! $Id: main.f90,v 1.15 2004/03/28 22:50:43 aake Exp $
!***********************************************************************
PROGRAM simplest
!
!  Main program for the simplest version of the stagger code
!
  use params
  use arrays
  use io
  implicit none

  t = 0.
  tsnap = 0.
  tscratch = 0.

  call read_parameters
  call allocate_arrays

  if (iread >= 0) then
    call read_snapshot
  else
    call initial_values
    call save
  end if

  step = 0
  do while (t < tstop)
    call timestep
    call save
    step = step+1
  end do
END

!***********************************************************************
SUBROUTINE read_parameters
  use params
  use io
  implicit none
  namelist /files/iread,iwrite,directory,from,file,scratch
  namelist /run/Cdt,t,dtsnap,dtscratch,tstop
  namelist /grid/mx,my,mz,sx,sy,sz
  namelist /pde/nu1,nu2,nur,do_mhd
  namelist /eos/gamma
!-----------------------------------------------------------------------
! Directory and file names
!-----------------------------------------------------------------------
  directory = 'data/'
  file = 'snapshot.dat'
  from = file
  scratch = 'scratch.dat'
  iread = -1
  iwrite = 0
  write (*,files)
  read  (*,files)
  write (*,files)
!-----------------------------------------------------------------------
! Run parameters
!-----------------------------------------------------------------------
  tstop = 1.
  Cdt = 0.45
  dtsnap = 0.2
  dtscratch = 1e35
  write (*,run)
  read  (*,run)
  write (*,run)
!-----------------------------------------------------------------------
! Grid parameters
!-----------------------------------------------------------------------
  mx=10; my=10; mz=10
  sx=1.; sy=1.; sz=1.
  write (*,grid)
  read  (*,grid)
  write (*,grid)
  dx=sx/mx; dy=sy/my; dz=sz/mz
  mw=mx*my*mz
!-----------------------------------------------------------------------
! PDE parameters
!-----------------------------------------------------------------------
  nu1=0.05; nu2=0.5; nur=0.
  do_mhd=.false.
  write (*,pde)
  read  (*,pde)
  write (*,pde)
!-----------------------------------------------------------------------
! Equation of state parameters
!-----------------------------------------------------------------------
  gamma = 5./3.
  write (*,eos)
  read  (*,eos)
  write (*,eos)
END
