! $Id: timestep.f90,v 1.13 2004/06/15 14:56:40 aake Exp $

!************************************************************************
  SUBROUTINE timestep
!
!  3rd order, 2-N storage Runge-Kutta.  Advances the eight MHD variables
!  forward in time, given time derivatives computed in the subroutine mhd.
!
  USE params
  USE arrays
  USE io
  implicit none

  integer n
  real alpha(3), beta(3)
  data alpha /     0., -0.641874, -1.31021/
  data beta  /0.46173,  0.924087, 0.390614/
!------------------------------------------------------------------------

!------------------------------------------------------------------------
!  Loop over three Runge-Kutta stages
!------------------------------------------------------------------------
  do n=1,3
    if (n .eq. 1) then
      drdt = 0.
      dedt = 0.
      dpxdt = 0.
      dpydt = 0.
      dpzdt = 0.
      if (do_mhd) then
        dBxdt = 0.
        dBydt = 0.
        dBzdt = 0.
      end if
    else
      drdt  = alpha(n)*drdt
      dedt  = alpha(n)*dedt
      dpxdt = alpha(n)*dpxdt
      dpydt = alpha(n)*dpydt
      dpzdt = alpha(n)*dpzdt
      nflop = nflop + 5
      if (do_mhd) then
        dBxdt = alpha(n)*dBxdt
        dBydt = alpha(n)*dBydt
        dBzdt = alpha(n)*dBzdt
        nflop = nflop + 3
      end if
    end if

    call pde
    if (n .eq.1) call Courant

    rho= rho+ (dt*beta(n))*drdt
    e  = e  + (dt*beta(n))*dedt
    px = px + (dt*beta(n))*dpxdt
    py = py + (dt*beta(n))*dpydt
    pz = pz + (dt*beta(n))*dpzdt
    nflop = nflop + 10
    if (do_mhd) then
      Bx = Bx + (dt*beta(n))*dBxdt
      By = By + (dt*beta(n))*dBydt
      Bz = Bz + (dt*beta(n))*dBzdt
      nflop = nflop + 6
    end if
  end do
  t = t + dt

  END
!***********************************************************************
  SUBROUTINE Courant
  USE params
  USE arrays
  USE io
  implicit none
  integer nprint, ix, iy, iz
  real cput(2), cpu, dtime, ds
  character(len=70):: id="$Id: timestep.f90,v 1.13 2004/06/15 14:56:40 aake Exp $"
!-----------------------------------------------------------------------

  if (id .ne. '') then 
    print *,id
    print *,''
    print *,'  step snap    t        dt           Courant numbers         mus/pt   U    cA   lnrho'
    id = ''
  end if

  ds = min(dx,dy,dz)
  Cu = 0.
  do iz=1,mz
  do iy=1,my
  do ix=1,mx
    Cu=max(Cu,(abs(Ux(ix,iy,iz))+cwave(ix,iy,iz))/dx, &
              (abs(Uy(ix,iy,iz))+cwave(ix,iy,iz))/dy, &
              (abs(Uz(ix,iy,iz))+cwave(ix,iy,iz))/dz)
  end do
  end do
  end do
!  Cu = max(maxval(Ux+cwave)/dx,maxval(Uy+cwave)/dy,maxval(Uz+cwave)/dz)
  Cn = 6.2*3.*maxval(nu)/2./ds
  Cr = maxval(abs(drdt/rho))
  Ce = max(Ce,maxval(abs(dedt/e)))
  dt = Cdt/max(Cu,Cn,Cr,Ce)
  
  nprint = min(1+30000/mw,10)
  if (mod(step,nprint) .eq. 0) then
    cpu = dtime(cput)/nprint
    write (*,'(i7,i5,f11.6,f9.6,2x,4f7.3,4f6.1)')&
      step,iwrite,t,dt,dt*Cu,dt*Cn,dt*Cr,dt*Ce,cpu*1e6/mw,maxval(U),maxval(cwave),maxval(lnr)
  end if

  END
