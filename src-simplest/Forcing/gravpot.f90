!***********************************************************************
  subroutine gravpot (rho, phi)
!
!  Solve for the gravitational potential phi, given the density rho,
!  from the equation
!
!       Laplace(phi) = grav*rho
!
!  In Fourier space, this becomes
!
!       k**2 phi = grav*rho
!
!  To avoid collapse to sizes below what can be resolved, phi is "softened"
!  by applying an exponential cut-off at the highest wavenumbers (smallest
!  scales):
!
!       k**2 phi = grav*rho*exp(-k**2/kcut**2)
!
  USE params
  USE forcing
!
  implicit none
  real, dimension(mx,my,mz):: rho, phi, green, k2
  real kcut
  character(len=70):: id = "$Id: gravpot.f90,v 1.5 2004/04/04 16:43:56 aake Exp $"
!-----------------------------------------------------------------------

  if (id .ne. '') then
    print *,id                                    ! identify
    call gravpot_test
    id = ''                                       ! but only once
  end if

  if (grav .eq. 0) then                           ! trap cheap case
    phi = 0.
    return
  endif

  call fft3d_k2 (k2, dx, dy, dz, mx, my, mz)      ! compute k**2
  green = -grav*4.*pi/(k2+1e-10)                  ! avoid division by zero
  green(1,1,1) = 0.0                              ! choose mean phi = 0

  kcut = pi/(soft*dx)                             ! cut-off wavenumber
  green = green*exp(-k2*(1./kcut**2))             ! softening

  call fft3df (rho, phi, mx, my, mz)              ! forward FFT
  phi = green*phi                                 ! Green's function
  call fft3db (phi, phi, mx, my, mz)              ! reverse FFT

  end

!***********************************************************************
  subroutine gravpot_test
!
  USE params
  USE forcing
!
  implicit none
  real, dimension(mx,my,mz):: phi, rho, k2
  integer ix, iy, iz
  real kx, ky, kz

  kx = 2.*pi/(mx*dx)*2.
  ky = 2.*pi/(my*dy)*3.
  kz = 2.*pi/(mz*dz)*4.

  do iz=1,mz
  do iy=1,my
  do ix=1,mx
    phi(ix,iy,iz) = cos((ix-1)*dx*kx) &
                  + sin((iy-1)*dx*ky) &
                  + cos((iz-1)*dx*kz)
    rho(ix,iy,iz) = cos((ix-1)*dx*kx)*kx**2 &
                  + sin((iy-1)*dx*ky)*ky**2 &
                  + cos((iz-1)*dx*kz)*kz**2
  end do
  end do
  end do

  call fft3d_k2 (k2, dx, dy, dz, mx, my, mz)      ! compute k**2
  call fft3df (phi, phi, mx, my, mz)              ! forward FFT
  !print *,phi(4:5,1,1)
  !print *,phi(1,6:7,1)
  !print *,phi(1,1,8:9)
  !print *,k2(4:5,1,1), kx**2
  !print *,k2(1,6:7,1), ky**2
  !print *,k2(1,1,8:9), kz**2
  phi = k2*phi                                    ! Green's function
  call fft3db (phi, phi, mx, my, mz)              ! reverse FFT

  eps = maxval(abs(phi-rho))/maxval(rho)
  print *,'eps =',eps
  if (eps.lt.3e-5) then
    print *,'the selfgravity routine appears to work correctly'
  else if (eps.lt.3e-4) then
    print *,'THE SELFGRAVITY ROUTINE APPEARS TO BE SOMEWHAT IMPRECISE'
  else
    print *,'THE SELFGRAVITY ROUTINE APPEARS TO NOT WORK CORRECTLY'
  end if

  end
