! $Id: planetary_disk.f90,v 1.6 2004/06/15 14:58:13 aake Exp $
!**********************************************************************
MODULE forcing

  implicit none
  logical do_force
  real            grav,M_sun,r0,r1,r2,r3,p3,eps,omega,soft,rho0,ee0,ee1,hy
  namelist /force/grav,M_sun,r0,r1,r2,r3,p3,eps,omega,soft,rho0,ee0,ee1,hy

END MODULE

!***********************************************************************
SUBROUTINE init_force
    USE params
    USE forcing
    integer iz
    character(len=80):: id='$Id: planetary_disk.f90,v 1.6 2004/06/15 14:58:13 aake Exp $'

    if (id .eq. ' ') return
    id = ' '

    do_force = .true.
    M_sun = 1.
    r0 = 0.5*sx
    r1 = dx
    r2 = 0.11*sx
    r3 = dx*2.
    p3 = 4.
    eps = 1e-4
    omega = 3.
    grav = 1.
    soft = 3.
    rho0 = 1.
    ee0 = 1.
    ee1 = 10.
    hy = 0.1

    call read_force

END SUBROUTINE

!***********************************************************************
SUBROUTINE read_force
    USE params
    USE forcing

    read (*,force)
    write (*,force)

!    call init_selfg
END SUBROUTINE

!***********************************************************************
SUBROUTINE forceit
!
  USE params
  USE arrays
  USE forcing
  USE stagger
!  USE selfgravity

  implicit none
  real, dimension(mx,my,mz):: dphi,pxz
  real rho_save,x,y,z,xz,r,exp1,omegar,dphir,taper
  integer ix,iy,iz
    
!  rho_save = rho(mx/2+1,my/2+1,mz/2+1)
!  rho(mx/2+1,my/2+1,mz/2+1) = rho(mx/2+1,my/2+1,mz/2+1) + M_sun/(dx*dy*dz)

  call gravpot(rho,phi)
  do iz=1,mz
    z = zm(iz)
    do iy=1,my
      y = ym(iy)
      do ix=1,mx
        x = xm(ix)
        r = sqrt(x**2+y**2+z**2+r3**2)
        phi(ix,iy,iz) = phi(ix,iy,iz)-grav*M_sun/r
      end do
    end do
  end do

!-----------------------------------------------------------------------
!  Compensate for the rotating coordinate system.  As shown by a test
!  with rigid rotation, it is the omega that needs to be a function of r,
!  not the whole acceleration term.  And as per a case with rotation,
!  selfgravity, and no motions in the rotating coordinate system near
!  the transition to the "exterior", the gravity term needs to be tapered
!  off in proportion to omegar**2.
!-----------------------------------------------------------------------

  dphi = ddxdn(phi)
  pxz = xdn(zup(Uz))
  do iz=1,mz
    z = (iz-1-mz/2)*dz
    do iy=1,my
      y = (iy-1-my/2)*dy
      do ix=1,mx
        x = (ix-1-mx/2-.5)*dx
        r = sqrt(x**2+y**2+z**2)
        xz = sqrt(x**2+z**2)
        x = x*min(xz,sx*0.5)/xz
        taper = exp1((r0-r)/r1)
        omegar = omega*taper
        dphir = dphi(ix,iy,iz)*taper**2
        dpxdt(ix,iy,iz) = dpxdt(ix,iy,iz) + &
           xdnr(ix,iy,iz)*(omegar*(omegar*x - 2.*pxz(ix,iy,iz)) - dphir)
      end do
    end do
  end do

  dphi = ddzdn(phi)
  pxz = zdn(xup(Ux))
  do iz=1,mz
    z = (iz-1-mz/2-.5)*dz
    do iy=1,my
      do ix=1,mx
        y = (iy-1-my/2)*dy
        x = (ix-1-mx/2)*dx
        r = sqrt(x**2+y**2+z**2)
        xz = sqrt(x**2+z**2)
        z = z*min(xz,sx*0.5)/xz
        taper = exp1((r0-r)/r1)
        omegar = omega*taper
        dphir = dphi(ix,iy,iz)*taper**2
        dpzdt(ix,iy,iz) = dpzdt(ix,iy,iz) + &
           zdnr(ix,iy,iz)*(omegar*(omegar*z + 2.*pxz(ix,iy,iz)) - dphir)
      end do
    end do
  end do

  dphi = ddydn(phi)
  do iz=1,mz
    z = (iz-1-mz/2)*dz
    do iy=1,my
      y = (iy-1-my/2-.5)*dy
      do ix=1,mx
        x = (ix-1-mx/2)*dx
        r = sqrt(x**2+y**2+z**2)
        taper = exp1((r0-r)/r1)
        dphir = dphi(ix,iy,iz)*taper**2
        dpydt(ix,iy,iz) = dpydt(ix,iy,iz)-ydnr(ix,iy,iz)*dphir
      end do
    end do
  end do
!  rho(mx/2+1,my/2+1,mz/2+1) = rho_save

END SUBROUTINE
