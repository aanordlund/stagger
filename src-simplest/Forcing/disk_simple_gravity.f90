! $Id: disk_simple_gravity.f90,v 1.7 2004/03/28 21:19:08 aake Exp $
!***********************************************************************
MODULE forcing
  real soft,grav
END MODULE

!-----------------------------------------------------------------------
SUBROUTINE init_forcing
  use forcing
  namelist /force/soft,grav

  write (*,force)
  read  (*,force)
  write (*,force)
END subroutine

!-----------------------------------------------------------------------
SUBROUTINE forceit
  use params
  use arrays
  use forcing
  implicit none
  integer ix,iy,iz
  real y,y0,ky
  character(len=72):: id="$Id"

  if (id .ne. ' ') then
    print *,id
    id = ' '
    call init_forcing
  end if

  ky = 2.*pi/sy*my/(my-1.)
  do iz=1,mz
  do iy=1,my
    y = dy*(iy-my/2-1)
    do ix=1,mx
      dpydt(ix,iy,iz) = dpydt(ix,iy,iz) - grav*ky*sin(ky*y)*ydnr(ix,iy,iz)
    end do
  end do
  end do
END
