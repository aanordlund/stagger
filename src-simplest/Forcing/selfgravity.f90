! $Id: selfgravity.f90,v 1.3 2004/04/03 12:17:00 rbjk Exp $
!-----------------------------------------------------------------------
MODULE forcing
  real soft,grav
END MODULE

!-----------------------------------------------------------------------
SUBROUTINE init_forcing
  use forcing
!
  namelist /force/soft,grav

  write (*,force)
  read  (*,force)
  write (*,force)
END

!-----------------------------------------------------------------------
SUBROUTINE forceit
!
!  Selfgravity (and only selfgravity)
!
  use params
  use arrays
  use stagger
  use forcing

  character(len=72) :: id="$Id"

  if (id .ne. ' ') then
    print *,id
    id = ' '
    call init_forcing
  end if

  call gravpot (rho, phi)

  dpxdt = dpxdt - xdnr*ddxdn(phi)
  dpydt = dpydt - ydnr*ddydn(phi)
  dpzdt = dpzdt - zdnr*ddzdn(phi)
END

