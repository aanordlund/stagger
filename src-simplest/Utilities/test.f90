! $Id: test.f90,v 1.3 2004/03/24 23:49:03 aake Exp $
!***********************************************************************
!
!  Main program for the MHD code.
!
  USE params
  USE arrays
  USE stagger

  implicit none
!-----------------------------------------------------------------------

  do_mhd = .true.
  gamma = 5./3.
  grav = 2e3                                ! gravity
  soft = 2.0                                ! softening

  nu1 = 0.05                                ! normal viscosity
  nu2 = 1.0                                 ! shock viscosity
  nur = 0.1                                 ! continuity diffusion

  Cdt = 0.5                                 ! Courant condition

  mx = 10
  my = 10
  mz = 10
  call allocate_arrays

  dx = 1./mx                                ! mesh size
  dy = 1./my                                ! mesh size
  dz = 1./mz                                ! mesh size
  print *,'----------------------- STAGGER ROUTINES TEST  -------------------------'
  call stagger_test
  print *,'----------------------- SELFGRAVITY TEST -------------------------------'
  call gravpot_test
  print *,'----------------------- MHD TEST ---------------------------------------'

  open (1, file='test/test_init.dat', &     ! open initial data file
   status='old', form='unformatted', &      ! old, unformatted
   access='direct', recl=4*mx*my*mz)        ! direct access
  read (1,rec=1) rho                        ! read density ..
  read (1,rec=2) px                         ! .. momentum ..
  read (1,rec=3) py                         ! ..
  read (1,rec=4) pz                         ! ..
  read (1,rec=5) e                          ! ..
  read (1,rec=6) Bx                         ! .. magnetic field ..
  read (1,rec=7) By                         ! ..
  read (1,rec=8) Bz                         ! ..
  close (1)
  print *,rho(1:5,1,1)
  print *,px(1:5,1,1)
  print *,py(1:5,1,1)
  print *,pz(1:5,1,1)
  print *,e (1:5,1,1)
  print *,bx(1:5,1,1)
  print *,by(1:5,1,1)
  print *,bz(1:5,1,1)

  t   = 0.                                  ! initial time
  dt  = 0.01*dx                             ! initial time step
  print *,dx,dy,dz,dt

  drdt  = 0.
  dpxdt = 0.
  dpydt = 0.
  dpzdt = 0.
  dedt  = 0.
  dBxdt = 0.
  dBydt = 0.
  dBzdt = 0.
  call pde
  print *,drdt(1:5,1,1)
  print *,dpxdt(1:5,1,1)
  print *,dpydt(1:5,1,1)
  print *,dpzdt(1:5,1,1)
  print *,dedt(1:5,1,1)
  print *,dbxdt(1:5,1,1)
  print *,dbydt(1:5,1,1)
  print *,dbzdt(1:5,1,1)

  open (2, file='test/test_dfdt.dat', &     ! open pde file
   status='old', form='unformatted', &      ! old, unformatted
   access='direct', recl=4*mx*my*mz)        ! direct access

  read (2,rec=1) u
  call test(' drdt:',u,drdt)
  read (2,rec=2) u
  call test('dpxdt:',u,dpxdt)
  read (2,rec=3) u
  call test('dpydt:',u,dpydt)
  read (2,rec=4) u
  call test('dpzdt:',u,dpzdt)
  read (2,rec=5) u
  call test('dBxdt:',u,dBxdt)
  read (2,rec=6) u
  call test('dBydt:',u,dBydt)
  read (2,rec=7) u
  call test('dBzdt:',u,dBzdt)
  close (2)

  print *,'----------------------- DONE -------------------------------------------'
  end
  
!***********************************************************************
  subroutine test (label, f, g)
  USE params

  real, dimension(mx,my,mz):: f, g
  character(len=*) label

  rms = sqrt(sum((f-g)**2)/mw)/sqrt(sum(f**2)/mw)
  if (rms .lt. 1e-6) then
    print *,label,' OK, rms error=', rms
  else
    print *,label,' NOK, rms error=', rms
  end if

  end
