! $Id: fft3d.f90,v 1.1 2004/03/22 01:02:24 aake Exp $
!----------------------------------------------------------------------
!
!  Three-dimensional Fourier transform routines, based on FFTPACK.
!  FFTPACK is transportable, and allows arbitrary dimensions (speed
!  will vary, of course).
!
!  CALL fft3df (f, ft, mx, my, mz)              ! forward transform
!  CALL fft3db (ft, f, mx, my, mz)              ! backward transform
!  CALL fft3d_k2 (k2, dx, dy, dz, mx, my, mz)   ! compute k2
!
!**********************************************************************
SUBROUTINE fft3d_k2(k2,dx,dy,dz,mx,my,mz)
  real, dimension(mx,my,mz):: k2
  real pi, dx, dy, dz, kx, ky, kz
  integer i, j, k, mx, my, mz
!----------------------------------------------------------------------

  pi=4.*atan(1.)

  do k=1,mz
    kz=2.*pi/(dz*mz)*(k/2)
    do j=1,my
      ky=2.*pi/(dy*my)*(j/2)
      do i=1,mx
        kx=2.*pi/(dx*mx)*(i/2)
        k2(i,j,k)=kx**2+ky**2+kz**2
      end do
    end do
  end do

END SUBROUTINE

!**********************************************************************
SUBROUTINE fft3df (r,rt,mx,my,mz)
  implicit none

  integer i,j,k,mx,my,mz
  integer, parameter:: mw=512, mwx=2*mw+15, mwy=2*mw+15, mwz=2*mw+15

  real, dimension(mx,my,mz):: r, rt
  real c, wx(mwx), wy(mwy), wz(mwz), fx(mx), fy(my), fz(mz,mx)
!----------------------------------------------------------------------

  call srffti (mx,wx)
  call srffti (my,wy)
  call srffti (mz,wz)

  do k=1,mz
  do j=1,my
    fx=r(:,j,k)
    call srfftf (mx,fx,wx)
    c=1./mx
    rt(:,j,k)=c*fx
  end do
  end do

  do k=1,mz
  do i=1,mx
    fy=rt(i,:,k)
    call srfftf (my,fy,wy)
    c=1./my
    rt(i,:,k)=c*fy
  end do
  end do

  do j=1,my
    do k=1,mz
      fz(k,:)=rt(:,j,k)
    end do
    do i=1,mx
      call srfftf (mz,fz(1,i),wz)
    end do
    c=1./mz
    do k=1,mz
      rt(:,j,k)=c*fz(k,:)
    end do
  end do

END SUBROUTINE

!**********************************************************************
SUBROUTINE fft3db (r,rt,mx,my,mz)
  implicit none

  integer i,j,k,mx,my,mz
  integer, parameter:: mw=512, mwx=2*mw+15, mwy=2*mw+15, mwz=2*mw+15

  real, dimension(mx,my,mz):: r, rt
  real wx(mwx), wy(mwy), wz(mwz), fx(mx), fy(my), fz(mz,mx)
!----------------------------------------------------------------------

  call srffti (mx,wx)
  call srffti (my,wy)
  call srffti (mz,wz)

  do k=1,mz
  do j=1,my
    fx=r(:,j,k)
    call srfftb (mx,fx,wx)
    rt(:,j,k)=fx
  end do
  end do

  do k=1,mz
  do i=1,mx
    fy=rt(i,:,k)
    call srfftb (my,fy,wy)
    rt(i,:,k)=fy
  end do
  end do

  do j=1,my
    do k=1,mz
      fz(k,:)=rt(:,j,k)
    end do
    do i=1,mx
      call srfftb (mz,fz(1,i),wz)
    end do
    do k=1,mz
      rt(:,j,k)=fz(k,:)
    end do
  end do

END SUBROUTINE
