! $Id: params.f90,v 1.8 2004/03/27 11:00:59 aake Exp $
MODULE params

! Parameters and scalars for the induction equation code

  real, parameter:: pi=3.1415926536
  integer:: mx, my, mz, mw, mvar
  integer:: it, isnap, nstep, nflop
  real:: sx, sy, sz, dx, dy, dz, nu1, nu2, nur
  real:: t, dt, dtold, Cdt, gamma
  real:: Cu, Cv, Cn, Cr, Ce
  real:: tscratch,tsnap,tstop,dtscratch,dtsnap
  logical:: do_mhd
  integer:: step, timeorder

END MODULE params
