! $Id: planetary_disk_isothermal.f90,v 1.3 2004/04/04 09:52:29 aake Exp $

!***********************************************************************
FUNCTION exp1(x)
  real :: x, exp1
  exp1 = exp(min(max(x,-70.),70.))
  exp1 = exp1/(1.+exp1)
END

!***********************************************************************
FUNCTION sqr1(x,a,b)
!
!  Implements a function that varies as (x/a)**2 for x<<b, and
!  then as (x/b)*(b/a)**2 for x>>b.
!
  real :: x, sqr1
  sqr1 = sqrt(1.+(x/b)**2)-1.       ! ~x/b for x>>b, ~0.5*(x/b)**2 for x<<b
  sqr1 = 2.*(b/a)**2*sqr1           ! ~(x/a)**2 for x<<b
END

!***********************************************************************
SUBROUTINE initial_values
  USE params
  USE arrays
  USE stagger
  USE forcing
  implicit none
  integer ix,iy,iz,ir
  real h,rho_save,cdx,fy,fr,vrot,T0,Tq,rho_min,smax
  real r,q,s,xz,x,y,z,xi,yi,zi,w,fxy,xy,exp1,pr,sqr1,dv,rm,rm1

  character(len=70):: id="$Id"

  if (id.ne.' ') print *,id ; id=' '

  call init_force

!-----------------------------------------------------------------------
!  Unperturbed density distribution, rotation
!-----------------------------------------------------------------------
  T0 = ee0*(gamma-1.)
  rho_min = 1e30
  do iz=1,mz
    do iy=1,my
      do ix=1,mx
        xz = sqrt(xm(ix)**2 + zm(iz)**2)
        q  = sqrt(xm(ix)**2 + zm(iz)**2 + r3**2)
        s  = sqrt(xm(ix)**2 + zm(iz)**2 + r3**2 + ym(iy)**2)

        phi(ix,iy,iz) = -grav*M_sun/s
        fr = exp1((q-r2)/r1)
!        fr = 1.

        if (q .gt. r2) then
          Tq = T0
          rho(ix,iy,iz) = rho0*fr*r3**3/q**3*exp(-grav*M_sun/Tq*(1./q-1./s))
          rho_min = min(rho(ix,iy,iz),rho_min)
        else 
          smax = sqrt(q**2+ym(1)**2)
          Tq = grav*M_sun*(1./q-1./smax)/alog(rho0*fr*r3**3/q**3/rho_min)
          rho(ix,iy,iz) = rho0*fr*r3**3/q**3*exp(-grav*M_sun/Tq*(1./q-1./s))
        end if
        if (iy.eq.1.and.iz.eq.51.and.ix.gt.50) then
          print*,ix,q,s,Tq,-grav*M_sun/Tq/q
        end if
        e(ix,iy,iz) = rho(ix,iy,iz)*Tq/(gamma-1.)
        temp(ix,iy,iz) = Tq

        w = grav*M_sun/Tq/q**3 + (-3./q + (1.-fr)/r1)/q
        w = sqrt(max(w,0.))
        w = w - omega
        w = max(w,0.0)
        px(ix,iy,iz) =  w*zm(iz)
        pz(ix,iy,iz) = -w*xm(ix)
        lnr(ix,iy,iz) = alog(rho(ix,iy,iz))
      end do
    end do
  end do

!-----------------------------------------------------------------------
!  Change from centered velocity to staggered momenta
!-----------------------------------------------------------------------
  px = exp(xdn(alog(rho)))*xdn(px)
  pz = exp(zdn(alog(rho)))*zdn(pz)
  py = 0.

END
