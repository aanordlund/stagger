! $Id: disk_isothermal.f90,v 1.11 2007/10/25 14:02:01 andersl Exp $
!***********************************************************************
MODULE initial
  real rho0,ux0,uy0,uz0,P0,Temp0
END MODULE

!-----------------------------------------------------------------------
SUBROUTINE initial_values
  use params
  use arrays
  use initial
  use forcing
  use radiation

  implicit none
  integer ix,iy,iz
  real y,ky,P1,tape,r2,r3,r,r0,r1,prospect,newtemp,hy,ctemp
  namelist /init/rho0,P0,ux0,uy0,uz0,grav,Temp0,ctemp

  ux0 = 0.1                                           ! default values
  uy0 = 0.1
  uz0 = 0.1
  rho0 = 1.                                           ! central density
  P0 = 1.                                             ! central pressure
  r0 = 0.05*mx*dx
  r2 = 0.04*mx*dx                                     ! start of hole
  r3 = 0.04*mx*dx                                     ! width of hole
  newtemp = 2000000.0                                                           
  prospect = 1.

  write (*,init)                                      ! prompt with default
  read  (*,init)                                      ! read new values
  write (*,init)                                      ! print final values

  ky = 2.*pi/sy*my/(my-1.)
  do iz=1,mz                                          ! loop to initialize ..
  do iy=1,my                                          ! .. perturbations ..
  do ix=1,mx

    y = dy*(iy-my/2-0.5)                              ! height
    P1 = P0*exp(-grav*(1-cos(ky*y)))                  ! sine gravity profile
!    e(ix,iy,iz)  = (1842.34/1.5151)*P1/(gamma-1.)     ! internal energy
    e(ix,iy,iz)  = (Temp0/ctemp)*P1                ! internal energy
    rho(ix,iy,iz) = rho0*P1/P0                        ! isothermal

    px(ix,iy,iz) = ux0*rho(ix,iy,iz)*sin(ix*2.*pi/mx)*sin(iy*2.*pi/my)
!    py(ix,iy,iz) = uy0*rho(ix,iy,iz)*sin(iz*2.*pi/mz)*sin(iy*2.*pi/my)
    py(ix,iy,iz) = uy0*rho(ix,iy,iz)*sin(iy*2.*pi/my)
    pz(ix,iy,iz) = uz0*rho(ix,iy,iz)*sin(iz*2.*pi/mz)*sin(iy*2.*pi/my)

!    px(ix,iy,iz)=0.
!    px(ix,iy,iz)=0.
!    px(ix,iy,iz)=0.
  end do
  end do
  end do

print *, ctemp*e(:,1,1)/rho(:,1,1)
print *, maxval(px), maxval(py), maxval(pz)
END
