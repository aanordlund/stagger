! $Id: disk_mixed.f90,v 1.1 2004/03/28 21:14:50 aake Exp $
!***********************************************************************
SUBROUTINE initial_values
  use params
  use arrays
  use forcing
  implicit none
  integer ix,iy,iz
  real y,ky,rho0,c0,P1,P2,Pmin,T0,T1,Tmin,ux0,uy0,uz0
  namelist /mixed/rho0,c0,ux0,uy0,uz0,T0,Tmin,P2

  ux0 = 0.                                                              ! default values
  uy0 = 0. 
  uz0 = 0. 
  rho0 = 1.                                                             ! central density
  c0 = 0.                                                               ! truncation param
  T0 = 1.                                                               ! central T
  Tmin = 1e-1                                                           ! min T
  P2 = 1.

  write (*,mixed)                                                       ! prompt with default
  read  (*,mixed)                                                       ! read new values
  write (*,mixed)                                                       ! print final values

  ky = 2.*pi/sy*my/(my-1.)                                              ! wavenumber
  grav = T0/(c0+1.)*gamma/(gamma-1.)                                    ! gravity parameter
  Pmin = 1e35
  do iz=1,mz                                                            ! loop to initialize ..
  do iy=1,my                                                            ! .. perturbations ..
  do ix=1,mx
    y = dy*(iy-my/2-0.5)                                                ! height
    T1 = max(Tmin,T0*(c0+cos(y*ky))/(c0+1.))                            ! sine gravity profile
    if (T1 > Tmin) then
      rho(ix,iy,iz) = rho0*(T1/T0)**(1./(gamma-1.))                     ! density
      P1 = T1*rho(ix,iy,iz)                                             ! pressure
      Pmin = min(Pmin,P1)                                               ! min isentropic pressure
    else
      P1 = P2*exp(grav/Tmin*cos(y*ky))
      rho(ix,iy,iz) = P1/Tmin
    end if
    e(ix,iy,iz)  = P1/(gamma-1.)                                        ! internal energy
    px(ix,iy,iz) = ux0*rho(ix,iy,iz)*sin(ix*2.*pi/mx)*sin(iy*2.*pi/my)
    py(ix,iy,iz) = uy0*rho(ix,iy,iz)*sin(iz*2.*pi/mz)*sin(iy*2.*pi/my)
    pz(ix,iy,iz) = uz0*rho(ix,iy,iz)*sin(iz*2.*pi/mz)*sin(iy*2.*pi/my)
  end do
  end do
  end do
  print *,'Pmin,grav=',Pmin,grav
END
