! $Id: disk_isothermal3.f90,v 1.2 2004/04/04 09:46:56 karreg Exp $
!***********************************************************************
MODULE initial
  real rho0,ux0,uy0,uz0,P0
END MODULE

!-----------------------------------------------------------------------
SUBROUTINE initial_values
  use params
  use arrays
  use initial
  use forcing
  implicit none
  integer ix,iy,iz
  real y,ky,P1,tape,r2,r3,r,r0,r1,prospect,newtemp,hy
  namelist /init/rho0,P0,ux0,uy0,uz0,grav

  ux0 = 0.1                                                             ! default values
  uy0 = 0.1
  uz0 = 0.1
  rho0 = 1.                                                             ! central density
  P0 = 1.                                                               ! central pressure
  r0 = 0.05*mx*dx
  r2 = 0.04*mx*dx                                                       ! start of hole
  r3 = 0.04*mx*dx                                                       ! width of hole
  newtemp = 2000000.0                                                           
  prospect = 1.

  write (*,init)                                                        ! prompt with default
  read  (*,init)                                                        ! read new values
  write (*,init)                                                        ! print final values

  ky = 2.*pi/sy*my/(my-1.)
  do iz=1,mz                                                            ! loop to initialize ..
  do iy=1,my                                                            ! .. perturbations ..
  do ix=1,mx
    y = dy*(iy-my/2-0.5)                                                ! height
    r = max(sqrt(((ix-mx/2-0.5)*dx)**2+((iz-mz/2-0.5)*dx)**2),0.1*dx)   ! radius
    tape = exp((r-r2)/r3)/(1.+exp((r-r2)/r3))                           ! doughnut-function
    hy = sqrt(2.*newtemp*r**3.)
    r1 = hy*sqrt(gamma/2.)
    
    P1 = P0*tape*exp(-(y/hy)**2)*((r1/r)**1.5)*((r0/r)**prospect)*exp(-grav*(1-cos(ky*y)))      ! isothermal

    rho(ix,iy,iz) = rho0*P1/P0

    e(ix,iy,iz)  = P1/(gamma-1.)        
    px(ix,iy,iz) = ux0*rho(ix,iy,iz)*sin(ix*2.*pi/mx)*sin(iy*2.*pi/my)
    py(ix,iy,iz) = uy0*rho(ix,iy,iz)*sin(iz*2.*pi/mz)*sin(iy*2.*pi/my)
    pz(ix,iy,iz) = uz0*rho(ix,iy,iz)*sin(iz*2.*pi/mz)*sin(iy*2.*pi/my)
  end do
  end do
  end do
END
