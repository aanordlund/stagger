! $Id: pde.f90,v 1.3 2004/03/24 23:48:27 aake Exp $

!***********************************************************************
  SUBROUTINE pde
!
!  Computes the time derivatives of the hydrodynamics equation
!  variables.  
!
!-----------------------------------------------------------------------
  USE params
  USE arrays
  USE stagger
  implicit none
  real ds

!-----------------------------------------------------------------------
!  Identify CVS version
!-----------------------------------------------------------------------
  character(len=70):: id='$Id: pde.f90,v 1.3 2004/03/24 23:48:27 aake Exp $'
  if (id.ne.' ') print *,id; id=' '

!-----------------------------------------------------------------------
!  Velocities
!-----------------------------------------------------------------------
  lnr = alog(rho)
  xdnr = exp(xdn(lnr))
  ydnr = exp(ydn(lnr))
  zdnr = exp(zdn(lnr))
  Ux = px/xdnr
  Uy = py/ydnr
  Uz = pz/zdnr
  U = sqrt(xup(Ux)**2 + yup(Uy)**2  + zup(Uz)**2)

!-----------------------------------------------------------------------
!  Pressure (P) and fast speed (cfast) -- isothermal conditions
!-----------------------------------------------------------------------
  P = (gamma-1.)*e
  if (do_mhd) then
    cfast = sqrt((P+xup(Bx)**2 + yup(By)**2 + zup(Bz)**2)*(1./rho)) &
          + sqrt(xup(Ux)**2 + yup(Uy)**2 + zup(Uz)**2)
  else
    cfast = sqrt(P/rho) + sqrt(xup(Ux)**2 + yup(Uy)**2 + zup(Uz)**2)
  end if

!-----------------------------------------------------------------------
!  Numerical diffusion, Richtmyer & Morton type
!-----------------------------------------------------------------------
  Sxx = ddxup(Ux); Syy = ddyup(Uy); Szz = ddzup(Uz)
  divU = Sxx + Syy + Szz
  ds = max(dx,dy,dz)
  nu = nu1*(U+cfast) + nu2*ds*smooth3(max3(-divU)) + 1e-35

!-----------------------------------------------------------------------
!  Viscous stress tensor, diagonal terms
!-----------------------------------------------------------------------
  ! dedt = dedt - rho*nu*(Sxx**2+Syy**2+Szz**2)
  Txx = rho*(xup(Ux)**2 - nu*dx*Sxx) + P
  Tyy = rho*(yup(Uy)**2 - nu*dy*Syy) + P
  Tzz = rho*(zup(Uz)**2 - nu*dz*Szz) + P

!-----------------------------------------------------------------------
!  Viscous stress tensor, off-diagonal terms
!-----------------------------------------------------------------------
  Sxy = (ddxdn(Uy)+ddydn(Ux))*0.5
  Syz = (ddydn(Uz)+ddzdn(Uy))*0.5
  Szx = (ddzdn(Ux)+ddxdn(Uz))*0.5
  ! dedt = dedt - rho*nu*(xup(yup(Sxy))**2 + yup(zup(Syz))**2 + zup(xup(Szx))**2)
  lnu = alog(nu)
  Txy = exp(xdn(ydn(lnr)))*(ydn(Ux)*xdn(Uy) - max(dx,dy)*exp(xdn(ydn(lnu)))*Sxy)
  Tyz = exp(ydn(zdn(lnr)))*(zdn(Uy)*ydn(Uz) - max(dy,dz)*exp(ydn(zdn(lnu)))*Syz)
  Tzx = exp(zdn(xdn(lnr)))*(xdn(Uz)*zdn(Ux) - max(dz,dx)*exp(zdn(xdn(lnu)))*Szx)

!-----------------------------------------------------------------------
!  Gravitational potential
!-----------------------------------------------------------------------
  call gravpot (rho, phi)

!-----------------------------------------------------------------------
!  Equations of motion, face centred
!-----------------------------------------------------------------------
  dpxdt = dpxdt - ddxdn(Txx)-ddyup(Txy)-ddzup(Tzx) - xdnr*ddxdn(phi)
  dpydt = dpydt - ddydn(Tyy)-ddzup(Tyz)-ddxup(Txy) - ydnr*ddydn(phi)
  dpzdt = dpzdt - ddzdn(Tzz)-ddxup(Tzx)-ddyup(Tyz) - zdnr*ddzdn(phi)

!-----------------------------------------------------------------------
!  Mass conservation, cell centered
!-----------------------------------------------------------------------
  drdt = drdt - ddxup1(px) - ddyup1(py) - ddzup1(pz)

!-----------------------------------------------------------------------
!  Energy conservation, cell centered
!-----------------------------------------------------------------------
  lne = alog(e)
  dedt = dedt - P*divU &
       - ddxup1(exp(xdn(lne-lnr))*px - dx*exp(xdn(lnu+lnr))*ddxdn(lne)) &
       - ddyup1(exp(ydn(lne-lnr))*py - dy*exp(ydn(lnu+lnr))*ddydn(lne)) &
       - ddzup1(exp(zdn(lne-lnr))*pz - dz*exp(zdn(lnu+lnr))*ddzdn(lne))

  if (do_mhd) then
!-----------------------------------------------------------------------
!  Electric current j = curl(B), edge centered
!-----------------------------------------------------------------------
    jx = ddydn(Bz) - ddzdn(By)
    jy = ddzdn(Bx) - ddxdn(Bz)
    jz = ddxdn(By) - ddydn(Bx)

!-----------------------------------------------------------------------
!  Lorentz force
!-----------------------------------------------------------------------
    dpxdt = dpxdt + zup(jy*xdn(Bz))-yup(jz*xdn(By))
    dpydt = dpydt + xup(jz*ydn(Bx))-zup(jx*ydn(Bz))
    dpzdt = dpzdt + yup(jx*zdn(By))-xup(jy*zdn(Bx))

!-----------------------------------------------------------------------
!  Electric field   E = eta j - (u x B), edge centered
!-----------------------------------------------------------------------
    Ex = max(dy,dz)*ydn(zdn(nu))*jx - zdn(Uy)*ydn(Bz) + ydn(Uz)*zdn(By)
    Ey = max(dz,dx)*zdn(xdn(nu))*jy - xdn(Uz)*zdn(Bx) + zdn(Ux)*xdn(Bz)
    Ez = max(dx,dy)*xdn(ydn(nu))*jz - ydn(Ux)*xdn(By) + xdn(Uy)*ydn(Bx)

!-----------------------------------------------------------------------
!  Magnetic field's time derivative, dBdt = - curl(E), face centered
!-----------------------------------------------------------------------
    dBxdt = dBxdt + ddzup(Ey) - ddyup(Ez)
    dBydt = dBydt + ddxup(Ez) - ddzup(Ex)
    dBzdt = dBzdt + ddyup(Ex) - ddxup(Ey)  
  end if

  END
