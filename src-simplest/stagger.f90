MODULE stagger
  real a, b, c
CONTAINS
!***********************************************************************
  function xup (f)
!
!  f is centered on (i-.5,j,k), fifth order stagger
!
  use params
  real, dimension(mx,my,mz):: f, xup
!-----------------------------------------------------------------------
!
  c = 3./256.
  b = -25./256.
  a = .5-b-c
!
!$omp parallel do private(i,j,k)
  do k=1,mz
   do j=1,my
    xup(mx-2,j,k) = ( &
                   a*(f(mx-2,j,k)+f(mx-1,j,k)) &
                 + b*(f(mx-3,j,k)+f(mx  ,j,k)) &
                 + c*(f(mx-4,j,k)+f(1   ,j,k)))
    xup(mx-1,j,k) = ( &
                   a*(f(mx-1,j,k)+f(mx  ,j,k)) &
                 + b*(f(mx-2,j,k)+f(1   ,j,k)) &
                 + c*(f(mx-3,j,k)+f(2   ,j,k)))
    xup(mx  ,j,k) = ( &
                   a*(f(mx  ,j,k)+f(1   ,j,k)) &
                 + b*(f(mx-1,j,k)+f(2   ,j,k)) &
                 + c*(f(mx-2,j,k)+f(3   ,j,k)))
    xup(1   ,j,k) = ( &
                   a*(f(1   ,j,k)+f(2   ,j,k)) &
                 + b*(f(mx  ,j,k)+f(3   ,j,k)) &
                 + c*(f(mx-1,j,k)+f(4   ,j,k)))
    xup(2   ,j,k) = ( &
                   a*(f(2   ,j,k)+f(3   ,j,k)) &
                 + b*(f(1   ,j,k)+f(4   ,j,k)) &
                 + c*(f(mx  ,j,k)+f(5   ,j,k)))
   end do
   do j=1,my
    do i=3,mx-3
      xup(i  ,j,k) = ( &
                    a*(f(i  ,j,k)+f(i+1 ,j,k)) &
                  + b*(f(i-1,j,k)+f(i+2 ,j,k)) &
                  + c*(f(i-2,j,k)+f(i+3 ,j,k)))
    end do
   end do
  end do
!
  nflop = nflop+8
  end function

!***********************************************************************
  function yup (f)
!
!  f is centered on (i,j-.5,k), fifth order stagger
!
  use params
  real, dimension(mx,my,mz):: f, yup
!-----------------------------------------------------------------------
!
  if (my.eq.1) then
    yup = f
    return
  end if
!
  c = 3./256.
  b = -25./256.
  a = .5-b-c
!
!$omp parallel do private(i,j,k)
  do k=1,mz
    do i=1,mx
      yup(i,my-2,k) = ( &
                     a*(f(i,my-2,k)+f(i,my-1,k)) + &
                     b*(f(i,my-3,k)+f(i,my  ,k)) + &
                     c*(f(i,my-4,k)+f(i,1   ,k)))
      yup(i,my-1,k) = ( &
                     a*(f(i,my-1,k)+f(i,my  ,k)) + &
                     b*(f(i,my-2,k)+f(i,1   ,k)) + &
                     c*(f(i,my-3,k)+f(i,2   ,k)))
      yup(i,my  ,k) = ( &
                     a*(f(i,my  ,k)+f(i,1   ,k)) + &
                     b*(f(i,my-1,k)+f(i,2   ,k)) + &
                     c*(f(i,my-2,k)+f(i,3   ,k)))
      yup(i,1   ,k) = ( &
                     a*(f(i,1   ,k)+f(i,2   ,k)) + &
                     b*(f(i,my  ,k)+f(i,3   ,k)) + &
                     c*(f(i,my-1,k)+f(i,4   ,k)))
      yup(i,2   ,k) = ( &
                     a*(f(i,2   ,k)+f(i,3   ,k)) + &
                     b*(f(i,1   ,k)+f(i,4   ,k)) + &
                     c*(f(i,my  ,k)+f(i,5   ,k)))
    end do
    do j=3,my-3
     do i=1,mx
      yup(i,j  ,k) = ( &
                     a*(f(i,j   ,k)+f(i,j+1 ,k)) + &
                     b*(f(i,j-1 ,k)+f(i,j+2 ,k)) + &
                     c*(f(i,j-2 ,k)+f(i,j+3 ,k)))
     end do
    end do
  end do
!
  nflop = nflop+8
  end function

!***********************************************************************
  function zup (f)
!
!  f is centered on (i,j,k-.5), fifth order stagger
!
  use params
  real, dimension(mx,my,mz):: f, zup
!-----------------------------------------------------------------------
!
  if (mz.eq.1) then
    zup = f
    return
  end if
!
  c = 3./256.
  b = -25./256.
  a = .5-b-c
!
!$omp parallel do private(i,j,k,km1,km2,kp1,kp2,kp3)
  do k=1,mz
    km2=mod(k-3+mz,mz)+1
    km1=mod(k-2+mz,mz)+1
    kp1=mod(k     ,mz)+1
    kp2=mod(k+1   ,mz)+1
    kp3=mod(k+2   ,mz)+1
    do j=1,my
      do i=1,mx
        zup(i,j,k) = ( &
                     a*(f(i,j,k  )+f(i,j,kp1)) + &
                     b*(f(i,j,km1)+f(i,j,kp2)) + &
                     c*(f(i,j,km2)+f(i,j,kp3)))
      end do
    end do
  end do
!
  nflop = nflop+8
  end function

!***********************************************************************
  function xdn (f)
!
!  f is centered on (i,j,k), fifth order stagger
!
  use params
  real, dimension(mx,my,mz):: f, xdn
!-----------------------------------------------------------------------
!
  c = 3./256.
  b = -25./256.
  a = .5-b-c
!
!$omp parallel do private(i,j,k)
  do k=1,mz
  do j=1,my
    xdn(mx-1,j,k) = ( &
                   a*(f(mx-2,j,k)+f(mx-1,j,k)) &
                 + b*(f(mx-3,j,k)+f(mx  ,j,k)) &
                 + c*(f(mx-4,j,k)+f(1   ,j,k)))
    xdn(mx  ,j,k) = ( &
                   a*(f(mx-1,j,k)+f(mx  ,j,k)) &
                 + b*(f(mx-2,j,k)+f(1   ,j,k)) &
                 + c*(f(mx-3,j,k)+f(2   ,j,k)))
    xdn(1  ,j,k) = ( &
                   a*(f(mx  ,j,k)+f(1   ,j,k)) &
                 + b*(f(mx-1,j,k)+f(2   ,j,k)) &
                 + c*(f(mx-2,j,k)+f(3   ,j,k)))
    xdn(2  ,j,k) = ( &
                   a*(f(1   ,j,k)+f(2   ,j,k)) &
                 + b*(f(mx  ,j,k)+f(3   ,j,k)) &
                 + c*(f(mx-1,j,k)+f(4   ,j,k)))
    xdn(3  ,j,k) = ( &
                   a*(f(2   ,j,k)+f(3   ,j,k)) &
                 + b*(f(1   ,j,k)+f(4   ,j,k)) &
                 + c*(f(mx  ,j,k)+f(5   ,j,k)))
  end do
  do j=1,my
    do i=3,mx-3
      xdn(i+1,j,k) = ( &
                   a*(f(i   ,j,k)+f(i+1 ,j,k)) &
                 + b*(f(i-1 ,j,k)+f(i+2 ,j,k)) &
                 + c*(f(i-2 ,j,k)+f(i+3 ,j,k)))
    end do
  end do
  end do
!
  nflop = nflop+8
  end function

!***********************************************************************
  function xdn1 (f)
!
!  f is centered on (i,j,k), fifth order stagger
!
  use params
  real, dimension(mx,my,mz):: f, xdn1
!-----------------------------------------------------------------------
!
  a = 0.5
!
!$omp parallel do private(i,j,k)
  do k=1,mz
  do j=1,my
    xdn1(1  ,j,k) = a*(f(mx  ,j,k)+f(1   ,j,k))
  end do
  do j=1,my
    do i=1,mx-1
      xdn1(i+1,j,k) = a*(f(i   ,j,k)+f(i+1 ,j,k))
    end do
  end do
  end do
!
  nflop = nflop+2
  end function

!***********************************************************************
  function ydn (f)
!
!  f is centered on (i,j,k), fifth order stagger
!
  use params
  real, dimension(mx,my,mz):: f, ydn
!-----------------------------------------------------------------------
!
  if (my.eq.1) then
    ydn = f
    return
  end if
!
  c = 3./256.
  b = -25./256.
  a = .5-b-c
!
!$omp parallel do private(i,j,k)
  do k=1,mz
    do i=1,mx
      ydn(i,my-1,k) = ( &
                     a*(f(i,my-2,k)+f(i,my-1,k)) + &
                     b*(f(i,my-3,k)+f(i,my  ,k)) + &
                     c*(f(i,my-4,k)+f(i,1   ,k)))
      ydn(i,my  ,k) = ( &
                     a*(f(i,my-1,k)+f(i,my  ,k)) + &
                     b*(f(i,my-2,k)+f(i,1   ,k)) + &
                     c*(f(i,my-3,k)+f(i,2   ,k)))
      ydn(i,1   ,k) = ( &
                     a*(f(i,my  ,k)+f(i,1   ,k)) + &
                     b*(f(i,my-1,k)+f(i,2   ,k)) + &
                     c*(f(i,my-2,k)+f(i,3   ,k)))
      ydn(i,2   ,k) = ( &
                     a*(f(i,1   ,k)+f(i,2   ,k)) + &
                     b*(f(i,my  ,k)+f(i,3   ,k)) + &
                     c*(f(i,my-1,k)+f(i,4   ,k)))
      ydn(i,3   ,k) = ( &
                     a*(f(i,2   ,k)+f(i,3   ,k)) + &
                     b*(f(i,1   ,k)+f(i,4   ,k)) + &
                     c*(f(i,my  ,k)+f(i,5   ,k)))
    end do
    do j=3,my-3
     do i=1,mx
      ydn(i,j+1,k) = ( &
                     a*(f(i,j   ,k)+f(i,j+1 ,k)) + &
                     b*(f(i,j-1 ,k)+f(i,j+2 ,k)) + &
                     c*(f(i,j-2 ,k)+f(i,j+3 ,k)))
     end do
    end do
  end do
!
  nflop = nflop+8
  end function

!***********************************************************************
  function ydn1 (f)
!
!  f is centered on (i,j,k), fifth order stagger
!
  use params
  real, dimension(mx,my,mz):: f, ydn1
!-----------------------------------------------------------------------
!
  if (my.eq.1) then
    ydn1 = f
    return
  end if
!
  a = 0.5
!
!$omp parallel do private(i,j,k)
  do k=1,mz
    do i=1,mx
      ydn1(i,1   ,k) = a*(f(i,my  ,k)+f(i,1   ,k))
    end do
    do j=1,my-1
     do i=1,mx
      ydn1(i,j+1,k) = a*(f(i,j   ,k)+f(i,j+1 ,k))
     end do
    end do
  end do
!
  nflop = nflop+2
  end function

!***********************************************************************
  function zdn (f)
!
!  f is centered on (i,j,k), fifth order stagger
!
  use params
  real, dimension(mx,my,mz):: f, zdn
!-----------------------------------------------------------------------
!
  if (mz.eq.1) then
    zdn = f
    return
  end if
!
  c = 3./256.
  b = -25./256.
  a = .5-b-c

!$omp parallel do private(i,j,k,km1,km2,km3,kp1,kp2)
  do k=1,mz
    km3=mod(k-4+mz,mz)+1
    km2=mod(k-3+mz,mz)+1
    km1=mod(k-2+mz,mz)+1
    kp1=mod(k     ,mz)+1
    kp2=mod(k+1   ,mz)+1
    do j=1,my
      do i=1,mx
        zdn(i,j,k) = ( &
                     a*(f(i,j,k  )+f(i,j,km1)) + &
                     b*(f(i,j,kp1)+f(i,j,km2)) + &
                     c*(f(i,j,kp2)+f(i,j,km3)))
      end do
    end do
  end do
!
  nflop = nflop+8
  end function

!***********************************************************************
  function zdn1 (f)
!
!  f is centered on (i,j,k), fifth order stagger
!
  use params
  real, dimension(mx,my,mz):: f, zdn1
!-----------------------------------------------------------------------
!
  if (mz.eq.1) then
    zdn1 = f
    return
  end if
!
  a = 0.5
!
!$omp parallel do private(i,j,k,km1,km2,km3,kp1,kp2)
  do k=1,mz
    km3=mod(k-4+mz,mz)+1
    km2=mod(k-3+mz,mz)+1
    km1=mod(k-2+mz,mz)+1
    kp1=mod(k     ,mz)+1
    kp2=mod(k+1   ,mz)+1
    do j=1,my
      do i=1,mx
        zdn1(i,j,k) = a*(f(i,j,k  )+f(i,j,km1))
      end do
    end do
  end do
!
  nflop = nflop+2
  end function

!***********************************************************************
  function ddxup (f)
  use params
  real, dimension(mx,my,mz):: f, ddxup
!-----------------------------------------------------------------------
!
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)/dx
  b = b/dx
  c = c/dx
!
!$omp parallel do private(i,j,k)
  do k=1,mz
  do j=1,my
    ddxup(mx-2,j,k) = ( &
                   a*(f(mx-1,j,k)-f(mx-2,j,k)) &
                 + b*(f(mx  ,j,k)-f(mx-3,j,k)) &
                 + c*(f(1   ,j,k)-f(mx-4,j,k)))
    ddxup(mx-1,j,k) = ( &
                   a*(f(mx  ,j,k)-f(mx-1,j,k)) &
                 + b*(f(1   ,j,k)-f(mx-2,j,k)) &
                 + c*(f(2   ,j,k)-f(mx-3,j,k)))
    ddxup(mx  ,j,k) = ( &
                   a*(f(1   ,j,k)-f(mx  ,j,k)) &
                 + b*(f(2   ,j,k)-f(mx-1,j,k)) &
                 + c*(f(3   ,j,k)-f(mx-2,j,k)))
    ddxup(1  ,j,k) = ( &
                   a*(f(2   ,j,k)-f(1   ,j,k)) &
                 + b*(f(3   ,j,k)-f(mx  ,j,k)) &
                 + c*(f(4   ,j,k)-f(mx-1,j,k)))
    ddxup(2  ,j,k) = ( &
                   a*(f(3   ,j,k)-f(2  ,j,k)) &
                 + b*(f(4   ,j,k)-f(1  ,j,k)) &
                 + c*(f(5   ,j,k)-f(mx ,j,k)))
  end do
  do j=1,my
    do i=3,mx-3
      ddxup(i  ,j,k) = ( &
                   a*(f(i+1 ,j,k)-f(i   ,j,k)) &
                 + b*(f(i+2 ,j,k)-f(i-1 ,j,k)) &
                 + c*(f(i+3 ,j,k)-f(i-2 ,j,k)))
    end do
  end do
  end do
!
  nflop = nflop+8
  end function

!***********************************************************************
  function ddxup1 (f)
  use params
  real, dimension(mx,my,mz):: f, ddxup1
!-----------------------------------------------------------------------
!
  a = 1./dx
!
!$omp parallel do private(i,j,k)
  do k=1,mz
  do j=1,my
    ddxup1(mx  ,j,k) = a*(f(1   ,j,k)-f(mx  ,j,k))
  end do
  do j=1,my
    do i=1,mx-1
      ddxup1(i  ,j,k) = a*(f(i+1 ,j,k)-f(i   ,j,k))
    end do
  end do
  end do
!
  nflop = nflop+2
  end function

!***********************************************************************
  function ddyup (f)
  use params
  real, dimension(mx,my,mz):: f, ddyup
!-----------------------------------------------------------------------
!
  if (my.eq.1) then
    ddyup = 0.
    return
  end if
!
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)/dy
  b = b/dy
  c = c/dy
!
!$omp parallel do private(i,j,k)
  do k=1,mz
    do i=1,mx
      ddyup(i,my-2,k) = ( &
                     a*(f(i,my-1,k)-f(i,my-2,k)) + &
                     b*(f(i,my  ,k)-f(i,my-3,k)) + &
                     c*(f(i,1   ,k)-f(i,my-4,k)))
      ddyup(i,my-1,k) = ( &
                     a*(f(i,my  ,k)-f(i,my-1,k)) + &
                     b*(f(i,1   ,k)-f(i,my-2,k)) + &
                     c*(f(i,2   ,k)-f(i,my-3,k)))
      ddyup(i,my  ,k) = ( &
                     a*(f(i,1   ,k)-f(i,my  ,k)) + &
                     b*(f(i,2   ,k)-f(i,my-1,k)) + &
                     c*(f(i,3   ,k)-f(i,my-2,k)))
      ddyup(i,1   ,k) = ( &
                     a*(f(i,2   ,k)-f(i,1   ,k)) + &
                     b*(f(i,3   ,k)-f(i,my  ,k)) + &
                     c*(f(i,4   ,k)-f(i,my-1,k)))
      ddyup(i,2   ,k) = ( &
                     a*(f(i,3   ,k)-f(i,2   ,k)) + &
                     b*(f(i,4   ,k)-f(i,1   ,k)) + &
                     c*(f(i,5   ,k)-f(i,my  ,k)))
    end do
    do j=3,my-3
     do i=1,mx
      ddyup(i,j  ,k) = ( &
                     a*(f(i,j+1 ,k)-f(i,j   ,k)) + &
                     b*(f(i,j+2 ,k)-f(i,j-1 ,k)) + &
                     c*(f(i,j+3 ,k)-f(i,j-2 ,k)))
     end do
    end do
  end do
!
  nflop = nflop+8
  end function

!***********************************************************************
  function ddyup1 (f)
  use params
  real, dimension(mx,my,mz):: f, ddyup1
!-----------------------------------------------------------------------
!
  if (my.eq.1) then
    ddyup1 = 0.
    return
  end if
!
  a = 1./dy
!
!$omp parallel do private(i,j,k)
  do k=1,mz
    do i=1,mx
      ddyup1(i,my  ,k) = a*(f(i,1   ,k)-f(i,my  ,k))
    end do
    do j=1,my-1
     do i=1,mx
      ddyup1(i,j  ,k) = a*(f(i,j+1 ,k)-f(i,j   ,k))
     end do
    end do
  end do
!
  nflop = nflop+2
  end function

!***********************************************************************
  function ddzup (f)
  use params
  real, dimension(mx,my,mz):: f, ddzup
!-----------------------------------------------------------------------
!
  if (mz.eq.1) then
    ddzup = 0.
    return
  end if
!
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)/dz
  b = b/dz
  c = c/dz
!
!$omp parallel do private(i,j,k,km1,km2,kp1,kp2,kp3)
  do k=1,mz
    km2=mod(k-3+mz,mz)+1
    km1=mod(k-2+mz,mz)+1
    kp1=mod(k     ,mz)+1
    kp2=mod(k+1   ,mz)+1
    kp3=mod(k+2   ,mz)+1
    do j=1,my
      do i=1,mx
        ddzup(i,j,k) = ( &
                       a*(f(i,j,kp1)-f(i,j,k  )) + &
                       b*(f(i,j,kp2)-f(i,j,km1)) + &
                       c*(f(i,j,kp3)-f(i,j,km2)))
      end do
    end do
  end do
!
  nflop = nflop+8
  end function

!***********************************************************************
  function ddzup1 (f)
  use params
  real, dimension(mx,my,mz):: f, ddzup1
!-----------------------------------------------------------------------
!
  if (mz.eq.1) then
    ddzup1 = 0.
    return
  end if
!
  a = 1./dz
!
!$omp parallel do private(i,j,k,kp1)
  do k=1,mz
    kp1=mod(k     ,mz)+1
    do j=1,my
      do i=1,mx
        ddzup1(i,j,k) = a*(f(i,j,kp1)-f(i,j,k  ))
      end do
    end do
  end do
!
  nflop = nflop+2
  end function

!***********************************************************************
  function ddxdn (f)
  use params
  real, dimension(mx,my,mz):: f, ddxdn
!-----------------------------------------------------------------------
!
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)/dx
  b = b/dx
  c = c/dx
!
!$omp parallel do private(i,j,k)
  do k=1,mz
   do j=1,my
    ddxdn(mx-1,j,k) = ( &
                   a*(f(mx-1,j,k)-f(mx-2,j,k)) &
                 + b*(f(mx  ,j,k)-f(mx-3,j,k)) &
                 + c*(f(1   ,j,k)-f(mx-4,j,k)))
    ddxdn(mx  ,j,k) = ( &
                   a*(f(mx  ,j,k)-f(mx-1,j,k)) &
                 + b*(f(1   ,j,k)-f(mx-2,j,k)) &
                 + c*(f(2   ,j,k)-f(mx-3,j,k)))
    ddxdn(1   ,j,k) = ( &
                   a*(f(1   ,j,k)-f(mx  ,j,k)) &
                 + b*(f(2   ,j,k)-f(mx-1,j,k)) &
                 + c*(f(3   ,j,k)-f(mx-2,j,k)))
    ddxdn(2  ,j,k) = ( &
                   a*(f(2   ,j,k)-f(1   ,j,k)) &
                 + b*(f(3   ,j,k)-f(mx  ,j,k)) &
                 + c*(f(4   ,j,k)-f(mx-1,j,k)))
    ddxdn(3  ,j,k) = ( &
                   a*(f(3   ,j,k)-f(2   ,j,k)) &
                 + b*(f(4   ,j,k)-f(1   ,j,k)) &
                 + c*(f(5   ,j,k)-f(mx  ,j,k)))
   end do
   do j=1,my
     do i=3,mx-3
      ddxdn(i+1,j,k) = ( &
                   a*(f(i+1 ,j,k)-f(i   ,j,k)) &
                 + b*(f(i+2 ,j,k)-f(i-1 ,j,k)) &
                 + c*(f(i+3 ,j,k)-f(i-2 ,j,k)))
    end do
   end do
  end do
!
  nflop = nflop+8
  end function

!***********************************************************************
  function ddxdn1 (f)
  use params
  real, dimension(mx,my,mz):: f, ddxdn1
!-----------------------------------------------------------------------
!
  a = 1./dx
!
!$omp parallel do private(i,j,k)
  do k=1,mz
   do j=1,my
    ddxdn1(1,j,k) = a*(f(1,j,k)-f(mx,j,k))
   end do
   do j=1,my
     do i=1,mx-1
      ddxdn1(i+1,j,k) = a*(f(i+1,j,k)-f(i,j,k))
    end do
   end do
  end do
!
  nflop = nflop+2
  end function

!***********************************************************************
  function ddydn (f)
  use params
  real, dimension(mx,my,mz):: f, ddydn
!-----------------------------------------------------------------------
!
  if (my.eq.1) then
    ddydn = 0.
    return
  end if
!
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)/dy
  b = b/dy
  c = c/dy
!
!$omp parallel do private(i,j,k)
  do k=1,mz
    do i=1,mx
      ddydn(i,my-1,k) = ( &
                     a*(f(i,my-1,k)-f(i,my-2,k)) + &
                     b*(f(i,my  ,k)-f(i,my-3,k)) + &
                     c*(f(i,1   ,k)-f(i,my-4,k)))
      ddydn(i,my  ,k) = ( &
                     a*(f(i,my  ,k)-f(i,my-1,k)) + &
                     b*(f(i,1   ,k)-f(i,my-2,k)) + &
                     c*(f(i,2   ,k)-f(i,my-3,k)))
      ddydn(i,1  ,k) = ( &
                     a*(f(i,1   ,k)-f(i,my  ,k)) + &
                     b*(f(i,2   ,k)-f(i,my-1,k)) + &
                     c*(f(i,3   ,k)-f(i,my-2,k)))
      ddydn(i,2   ,k) = ( &
                     a*(f(i,2   ,k)-f(i,1   ,k)) + &
                     b*(f(i,3   ,k)-f(i,my  ,k)) + &
                     c*(f(i,4   ,k)-f(i,my-1,k)))
      ddydn(i,3   ,k) = ( &
                     a*(f(i,3   ,k)-f(i,2   ,k)) + &
                     b*(f(i,4   ,k)-f(i,1   ,k)) + &
                     c*(f(i,5   ,k)-f(i,my  ,k)))
    end do
    do j=3,my-3
     do i=1,mx
      ddydn(i,j+1 ,k) = ( &
                     a*(f(i,j+1 ,k)-f(i,j   ,k)) + &
                     b*(f(i,j+2 ,k)-f(i,j-1 ,k)) + &
                     c*(f(i,j+3 ,k)-f(i,j-2 ,k)))
     end do
    end do
  end do
!
  nflop = nflop+8
  end function

!***********************************************************************
  function ddydn1 (f)
  use params
  real, dimension(mx,my,mz):: f, ddydn1
!-----------------------------------------------------------------------
!
  if (my.eq.1) then
    ddydn1 = 0.
    return
  end if
!
  a = 1./dy
!
!$omp parallel do private(i,j,k)
  do k=1,mz
    do i=1,mx
      ddydn1(i,1  ,k) = a*(f(i,1   ,k)-f(i,my  ,k))
    end do
    do j=1,my-1
     do i=1,mx
      ddydn1(i,j+1 ,k) = a*(f(i,j+1 ,k)-f(i,j   ,k))
     end do
    end do
  end do
!
  nflop = nflop+8
  end function

!***********************************************************************
  function ddzdn (f)
  use params
  real, dimension(mx,my,mz):: f, ddzdn
!-----------------------------------------------------------------------
!
  if (mz.eq.1) then
    ddzdn = 0.
    return
  end if
!
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)/dz
  b = b/dz
  c = c/dz

!$omp parallel do private(i,j,k,km1,km2,km3,kp1,kp2)
  do k=1,mz
    km3=mod(k-4+mz,mz)+1
    km2=mod(k-3+mz,mz)+1
    km1=mod(k-2+mz,mz)+1
    kp1=mod(k     ,mz)+1
    kp2=mod(k+1   ,mz)+1
    do j=1,my
      do i=1,mx
        ddzdn(i,j,k) = ( &
                       a*(f(i,j,k  )-f(i,j,km1)) + &
                       b*(f(i,j,kp1)-f(i,j,km2)) + &
                       c*(f(i,j,kp2)-f(i,j,km3)))
      end do
    end do
  end do
!
  nflop = nflop+8
  end function

!***********************************************************************
  function ddzdn1 (f)
  use params
  real, dimension(mx,my,mz):: f, ddzdn1
!-----------------------------------------------------------------------
!
  if (mz.eq.1) then
    ddzdn1 = 0.
    return
  end if
!
  a = 1./dz
!
!$omp parallel do private(i,j,k,km1,km2,km3,kp1,kp2)
  do k=1,mz
    km1=mod(k-2+mz,mz)+1
    do j=1,my
      do i=1,mx
        ddzdn1(i,j,k) = a*(f(i,j,k  )-f(i,j,km1))
      end do
    end do
  end do
!
  nflop = nflop+8
  end function

!***********************************************************************
  function smooth3 (f)
!
!  Three point max
!
  USE params
  real, dimension(mx,my,mz):: f, smooth3
  real, dimension(mx,my):: tmp
  integer ix,iy,iz,izm1,izp1,iym1,iyp1
  real c3
!-----------------------------------------------------------------------
!
   c3 = 1./3.
!$omp parallel private(ix,iy,iz,iym1,iyp1,izm1,izp1,tmp)
  if (mz.eq.1) then
    smooth3 = f
  else
!$omp do
    do iz=1,mz
      izm1 = iz-1 ; if (iz.eq.1 ) izm1=mz
      izp1 = iz+1 ; if (iz.eq.mz) izp1=1
      do ix=1,mx
      do iy=1,my
        smooth3(ix,iy,iz) = c3*(f(ix,iy,izm1)+f(ix,iy,iz)+f(ix,iy,izp1))
      end do
      end do
    end do
!$omp barrier
  end if
!$omp do
  do iz=1,mz
    if (my.eq.1) then
      tmp = smooth3(:,:,iz)
    else
      do iy=1,my
        iym1 = iy-1 ; if (iy.eq.1 ) iym1=my
        iyp1 = iy+1 ; if (iy.eq.my) iyp1=1
        do ix=1,mx
          tmp(ix,iy) = c3*(smooth3(ix,iym1,iz)+smooth3(ix,iy,iz)+smooth3(ix,iyp1,iz))
        end do
      end do
    end if
    do iy=1,my
      smooth3( 1,iy,iz) = c3*(tmp(mx  ,iy)+tmp(1 ,iy)+tmp(2,iy))
      smooth3(mx,iy,iz) = c3*(tmp(mx-1,iy)+tmp(mx,iy)+tmp(1,iy))
      do ix=2,mx-1
        smooth3(ix,iy,iz) = c3*(tmp(ix-1,iy)+tmp(ix,iy)+tmp(ix+1,iy))
      enddo
    enddo
  enddo
!$omp end parallel
  end function

!***********************************************************************
  function max3 (f)
!
!  Three point max
!
  USE params
  real, dimension(mx,my,mz):: f, max3
  real, dimension(mx,my):: tmp
  integer ix,iy,iz,izm1,izp1,iym1,iyp1
!-----------------------------------------------------------------------
!
!$omp parallel private(ix,iy,iz,iym1,iyp1,izm1,izp1,tmp)
  if (mz.eq.1) then
    max3 = max(f,0.)
  else
!$omp do
    do iz=1,mz
      izm1 = iz-1 ; if (iz.eq.1 ) izm1=mz
      izp1 = iz+1 ; if (iz.eq.mz) izp1=1
      do ix=1,mx
      do iy=1,my
        max3(ix,iy,iz) = max(f(ix,iy,izm1),f(ix,iy,iz),f(ix,iy,izp1),0.)
      end do
      end do
    end do
!$omp barrier
  end if
!$omp do
  do iz=1,mz
    if (my.eq.1) then
      tmp = max3(:,:,iz)
    else
      do iy=1,my
        iym1 = iy-1 ; if (iy.eq.1 ) iym1=my
        iyp1 = iy+1 ; if (iy.eq.my) iyp1=1
        do ix=1,mx
          tmp(ix,iy) = max(max3(ix,iym1,iz),max3(ix,iy,iz),max3(ix,iyp1,iz))
        end do
      end do
    end if
    do iy=1,my
      max3( 1,iy,iz) = max(tmp(mx  ,iy),tmp(1 ,iy),tmp(2,iy))
      max3(mx,iy,iz) = max(tmp(mx-1,iy),tmp(mx,iy),tmp(1,iy))
      do ix=2,mx-1
        max3(ix,iy,iz) = max(tmp(ix-1,iy),tmp(ix,iy),tmp(ix+1,iy))
      enddo
    enddo
  enddo
!$omp end parallel
  end function

!***********************************************************************
  function max5 (f)
!
!  Three point max
!
  USE params
  real, dimension(mx,my,mz):: f, max5
  real, dimension(mx,my):: tmp
  integer ix,iy,iz,izm2,izm1,izp1,izp2,iym2,iym1,iyp1,iyp2
!-----------------------------------------------------------------------
!
!$omp parallel private(ix,iy,iz,iym2,iym1,iyp1,iyp2,izm2,izm1,izp1,izp2,tmp)
  if (mz.eq.1) then
    max5 = max(f,0.)
  else
!$omp do
    do iz=1,mz
      izm2 = iz-2 ; if (iz.eq.1 ) izm2=mz-1 ; if (iz.eq.2) izm2=mz
      izm1 = iz-1 ; if (iz.eq.1 ) izm1=mz
      izp1 = iz+1 ; if (iz.eq.mz) izp1=1
      izp2 = iz+2 ; if (iz.eq.mz) izp2=2    ; if (iz.eq.mz-1) izp2=1
      do ix=1,mx
      do iy=1,my
        max5(ix,iy,iz) = max(f(ix,iy,izm2), &
                             f(ix,iy,izm1), &
                             f(ix,iy,iz  ), &
                             f(ix,iy,izp1), &
                             f(ix,iy,izp2),0.)
      end do
      end do
    end do
!$omp barrier
  end if
!$omp do
  do iz=1,mz
    if (my.eq.1) then
      tmp = max5(:,:,iz)
    else
      do iy=1,my
        iym2 = iy-2 ; if (iy.eq.1 ) iym2=my-1 ; if (iy.eq.2) iym2=my
        iym1 = iy-1 ; if (iy.eq.1 ) iym1=my
        iyp1 = iy+1 ; if (iy.eq.my) iyp1=1
        iyp2 = iy+2 ; if (iy.eq.my) iyp2=2    ; if (iy.eq.my-1) iyp2=1
        do ix=1,mx
          tmp(ix,iy) = max(max5(ix,iym2,iz), &
                           max5(ix,iym1,iz), &
                           max5(ix,iy  ,iz), &
                           max5(ix,iyp1,iz), &
                           max5(ix,iyp2,iz))
        end do
      end do
    end if
    do iy=1,my
      max5( 2  ,iy,iz) = max(tmp(mx  ,iy),tmp(1   ,iy),tmp(2   ,iy),tmp(3 ,iy),tmp(4,iy))
      max5( 1  ,iy,iz) = max(tmp(mx-1,iy),tmp(mx  ,iy),tmp(1   ,iy),tmp(2 ,iy),tmp(3,iy))
      max5(mx  ,iy,iz) = max(tmp(mx-2,iy),tmp(mx-1,iy),tmp(mx  ,iy),tmp(1 ,iy),tmp(2,iy))
      max5(mx-1,iy,iz) = max(tmp(mx-3,iy),tmp(mx-2,iy),tmp(mx-1,iy),tmp(mx,iy),tmp(1,iy))
      do ix=3,mx-2
        max5(ix,iy,iz) = max(tmp(ix-2,iy), &
                             tmp(ix-1,iy), &
                             tmp(ix  ,iy), &
                             tmp(ix+1,iy), &
                             tmp(ix+2,iy))
      enddo
    enddo
  enddo
!$omp end parallel
  end function

!-------------------------------------------------------------------------------
  subroutine stagger_test
!
!  Check the correctness of the stagger operators
!
  use params
  use arrays
  implicit none
  integer ix,iy,iz
  real epsx, epsy, epsz, eps, fx, fy, fz
  logical ok

  character(len=70):: id='$Id: stagger.f90,v 1.3 2004/03/28 21:51:02 aake Exp $'
  if (id .ne. ' ') print *,id; id=' '

  fx = 2.*pi/(mx*dx)
  fy = 2.*pi/(my*dy)
  fz = 2.*pi/(mz*dz)

  do iz=1,mz
  do iy=1,my
  do ix=1,mx
    ux(ix,iy,iz)=sin(2.*pi*real(ix)/real(mx))
    uy(ix,iy,iz)=sin(2.*pi*real(iy)/real(my))
    uz(ix,iy,iz)=sin(2.*pi*real(iz)/real(mz))
    px(ix,iy,iz)=sin(2.*pi*real(ix-0.5)/real(mx))
    py(ix,iy,iz)=sin(2.*pi*real(iy-0.5)/real(my))
    pz(ix,iy,iz)=sin(2.*pi*real(iz-0.5)/real(mz))
  end do
  end do
  end do
  dpxdt = xdn(ux); epsx=maxval(abs(dpxdt-px))
  dpydt = ydn(uy); epsy=maxval(abs(dpydt-py)); if (my.eq.1) epsy=0.
  dpzdt = zdn(uz); epsz=maxval(abs(dpzdt-pz)); if (mz.eq.1) epsz=0.
  eps = max(4.*fx**6*dx**6/(6.*5.*4.*3.*2.),1e-6)
  ok = (epsx < eps) .and. (epsy < eps) .and. (epsz < eps)
  print *,'  dn:', epsx, epsy, epsz, eps, ok

  do iz=1,mz
  do iy=1,my
  do ix=1,mx
    ux(ix,iy,iz)=sin(2.*pi*real(ix)/real(mx))
    uy(ix,iy,iz)=sin(2.*pi*real(iy)/real(my))
    uz(ix,iy,iz)=sin(2.*pi*real(iz)/real(mz))
    px(ix,iy,iz)=sin(2.*pi*real(ix+0.5)/real(mx))
    py(ix,iy,iz)=sin(2.*pi*real(iy+0.5)/real(my))
    pz(ix,iy,iz)=sin(2.*pi*real(iz+0.5)/real(mz))
  end do
  end do
  end do
  dpxdt = xup(ux); epsx=maxval(abs(dpxdt-px))
  dpydt = yup(uy); epsy=maxval(abs(dpydt-py)); if (my.eq.1) epsy=0.
  dpzdt = zup(uz); epsz=maxval(abs(dpzdt-pz)); if (mz.eq.1) epsz=0.
  eps = max(4.*fx**6*dx**6/(6.*5.*4.*3.*2.),1e-6)
  ok = ok .and. (epsx < eps) .and. (epsy < eps) .and. (epsz < eps)
  print *,'  up:', epsx, epsy, epsz, eps, ok

  do iz=1,mz
  do iy=1,my
  do ix=1,mx
    ux(ix,iy,iz)=sin(2.*pi*real(ix)/real(mx))
    uy(ix,iy,iz)=sin(2.*pi*real(iy)/real(my))
    uz(ix,iy,iz)=sin(2.*pi*real(iz)/real(mz))
    px(ix,iy,iz)=cos(2.*pi*real(ix-0.5)/real(mx))*fx
    py(ix,iy,iz)=cos(2.*pi*real(iy-0.5)/real(my))*fy
    pz(ix,iy,iz)=cos(2.*pi*real(iz-0.5)/real(mz))*fz
  end do
  end do
  end do
  dpxdt = ddxdn(ux); epsx=maxval(abs(dpxdt-px))
  dpydt = ddydn(uy); epsy=maxval(abs(dpydt-py)); if (my.eq.1) epsy=0.
  dpzdt = ddzdn(uz); epsz=maxval(abs(dpzdt-pz)); if (mz.eq.1) epsz=0.
  eps = max(4.*fx**7*dx**6/(7.*6.*5.*4.*3.*2.),1e-6/dx)
  ok = ok .and. (epsx < eps) .and. (epsy < eps) .and. (epsz < eps)
  print *,' ddn:', epsx, epsy, epsz, eps, ok

  do iz=1,mz
  do iy=1,my
  do ix=1,mx
    ux(ix,iy,iz)=sin(2.*pi*real(ix)/real(mx))
    uy(ix,iy,iz)=sin(2.*pi*real(iy)/real(my))
    uz(ix,iy,iz)=sin(2.*pi*real(iz)/real(mz))
    px(ix,iy,iz)=cos(2.*pi*real(ix+0.5)/real(mx))*fx
    py(ix,iy,iz)=cos(2.*pi*real(iy+0.5)/real(my))*fy
    pz(ix,iy,iz)=cos(2.*pi*real(iz+0.5)/real(mz))*fz
  end do
  end do
  end do
  dpxdt = ddxup(ux); epsx=maxval(abs(dpxdt-px))
  dpydt = ddyup(uy); epsy=maxval(abs(dpydt-py)); if (my.eq.1) epsy=0.
  dpzdt = ddzup(uz); epsz=maxval(abs(dpzdt-pz)); if (mz.eq.1) epsz=0.
  eps = max(4.*fx**7*dx**6/(7.*6.*5.*4.*3.*2.),1e-6/dx)
  ok = ok .and. (epsx < eps) .and. (epsy < eps) .and. (epsz < eps)
  print *,' dup:', epsx, epsy, epsz, eps, ok

  do iz=1,mz
  do iy=1,my
  do ix=1,mx
    ux(ix,iy,iz)=sin(2.*pi*real(ix)/real(mx))
    uy(ix,iy,iz)=sin(2.*pi*real(iy)/real(my))
    uz(ix,iy,iz)=sin(2.*pi*real(iz)/real(mz))
    px(ix,iy,iz)=sin(2.*pi*real(ix-0.5)/real(mx))
    py(ix,iy,iz)=sin(2.*pi*real(iy-0.5)/real(my))
    pz(ix,iy,iz)=sin(2.*pi*real(iz-0.5)/real(mz))
  end do
  end do
  end do
  dpxdt = xdn1(ux); epsx=maxval(abs(dpxdt-px))
  dpydt = ydn1(uy); epsy=maxval(abs(dpydt-py)); if (my.eq.1) epsy=0.
  dpzdt = zdn1(uz); epsz=maxval(abs(dpzdt-pz)); if (mz.eq.1) epsz=0.
  eps = max(0.5*fx**2*dx**2/2.,1e-6)
  ok = ok .and. (epsx < eps) .and. (epsy < eps) .and. (epsz < eps)
  print *,' dn1:', epsx, epsy, epsz, eps, ok

  do iz=1,mz
  do iy=1,my
  do ix=1,mx
    ux(ix,iy,iz)=sin(2.*pi*real(ix)/real(mx))
    uy(ix,iy,iz)=sin(2.*pi*real(iy)/real(my))
    uz(ix,iy,iz)=sin(2.*pi*real(iz)/real(mz))
    px(ix,iy,iz)=cos(2.*pi*real(ix-0.5)/real(mx))*fx
    py(ix,iy,iz)=cos(2.*pi*real(iy-0.5)/real(my))*fy
    pz(ix,iy,iz)=cos(2.*pi*real(iz-0.5)/real(mz))*fz
  end do
  end do
  end do
  dpxdt = ddxdn1(ux); epsx=maxval(abs(dpxdt-px))
  dpydt = ddydn1(uy); epsy=maxval(abs(dpydt-py)); if (my.eq.1) epsy=0.
  dpzdt = ddzdn1(uz); epsz=maxval(abs(dpzdt-pz)); if (mz.eq.1) epsz=0.
  eps = max(0.5*fx**3*dx**2/(3.*2.*1.),1e-6/dx)
  ok = ok .and. (epsx < eps) .and. (epsy < eps) .and. (epsz < eps)
  print *,'ddn1:', epsx, epsy, epsz, eps, ok

  do iz=1,mz
  do iy=1,my
  do ix=1,mx
    ux(ix,iy,iz)=sin(2.*pi*real(ix)/real(mx))
    uy(ix,iy,iz)=sin(2.*pi*real(iy)/real(my))
    uz(ix,iy,iz)=sin(2.*pi*real(iz)/real(mz))
    px(ix,iy,iz)=cos(2.*pi*real(ix+0.5)/real(mx))*fx
    py(ix,iy,iz)=cos(2.*pi*real(iy+0.5)/real(my))*fy
    pz(ix,iy,iz)=cos(2.*pi*real(iz+0.5)/real(mz))*fz
  end do
  end do
  end do
  dpxdt = ddxup1(ux); epsx=maxval(abs(dpxdt-px))
  dpydt = ddyup1(uy); epsy=maxval(abs(dpydt-py)); if (my.eq.1) epsy=0.
  dpzdt = ddzup1(uz); epsz=maxval(abs(dpzdt-pz)); if (mz.eq.1) epsz=0.
  eps = max(0.5*fx**3*dx**2/(3.*2.*1.),1e-6/dx)
  ok = ok .and. (epsx < eps) .and. (epsy < eps) .and. (epsz < eps)
  print *,'dup1:', epsx, epsy, epsz, eps, ok

  if (ok) then
    print *,'the stagger routines appear to be working correctly'
  else
    print *,'THERE APPEARS TO BE A PROBLEM WITH THE STAGGER ROUTINES'
  end if

  end subroutine

END MODULE
