! $Id: hd.f90,v 1.12 2004/03/28 21:53:10 aake Exp $

!***********************************************************************
  SUBROUTINE pde
!
!  Computes the time derivatives of the hydrodynamics equation variables.  
!
!-----------------------------------------------------------------------
  USE params
  USE arrays
  USE stagger
  implicit none
  real ds

!-----------------------------------------------------------------------
!  Identify CVS version
!-----------------------------------------------------------------------
  character(len=70):: id='$Id: hd.f90,v 1.12 2004/03/28 21:53:10 aake Exp $'
  if (id.ne.' ') print *,id; id=' '

!-----------------------------------------------------------------------
!  Velocities
!-----------------------------------------------------------------------
  lnr = alog(rho)
  xdnr = exp(xdn(lnr))
  ydnr = exp(ydn(lnr))
  zdnr = exp(zdn(lnr))
  Ux = px/xdnr
  Uy = py/ydnr
  Uz = pz/zdnr
  U = sqrt(xup(Ux)**2 + yup(Uy)**2  + zup(Uz)**2)

!-----------------------------------------------------------------------
!  Pressure (P) and sound speed (cwave)
!-----------------------------------------------------------------------
  if (gamma==1.) then; P=e; else; P=(gamma-1.)*e; endif
  temp = P/rho
  cwave = sqrt(gamma*temp)+sqrt(xup(Ux)**2 + yup(Uy)**2 + zup(Uz)**2)

!-----------------------------------------------------------------------
!  Numerical diffusion, Richtmyer & Morton type
!-----------------------------------------------------------------------
  Sxx = ddxup(Ux); Syy = ddyup(Uy); Szz = ddzup(Uz)
  divU = Sxx + Syy + Szz
  ds = max(dx,dy,dz)
  nu = nu1*(U+cwave) + nu2*ds*smooth3(max3(max(-divU,0.))) + 1e-35

!-----------------------------------------------------------------------
!  Viscous stress tensor, diagonal terms
!-----------------------------------------------------------------------
  ! dedt = dedt - rho*nu*(Sxx**2+Syy**2+Szz**2)
  Txx = rho*(xup(Ux)**2 - nu*dx*Sxx) + P
  Tyy = rho*(yup(Uy)**2 - nu*dy*Syy) + P
  Tzz = rho*(zup(Uz)**2 - nu*dz*Szz) + P

!-----------------------------------------------------------------------
!  Viscous stress tensor, off-diagonal terms
!-----------------------------------------------------------------------
  Sxy = (ddxdn(Uy)+ddydn(Ux))*0.5
  Syz = (ddydn(Uz)+ddzdn(Uy))*0.5
  Szx = (ddzdn(Ux)+ddxdn(Uz))*0.5
  ! dedt = dedt - rho*nu*(xup(yup(Sxy))**2 + yup(zup(Syz))**2 + zup(xup(Szx))**2)
  lnu = alog(nu)
  Txy = exp(xdn(ydn(lnr)))*(ydn(Ux)*xdn(Uy) - max(dx,dy)*exp(xdn(ydn(lnu)))*Sxy)
  Tyz = exp(ydn(zdn(lnr)))*(zdn(Uy)*ydn(Uz) - max(dy,dz)*exp(ydn(zdn(lnu)))*Syz)
  Tzx = exp(zdn(xdn(lnr)))*(xdn(Uz)*zdn(Ux) - max(dz,dx)*exp(zdn(xdn(lnu)))*Szx)

!-----------------------------------------------------------------------
!  Equations of motion, face centred
!-----------------------------------------------------------------------
  dpxdt = dpxdt - ddxdn(Txx) - ddyup(Txy) - ddzup(Tzx)
  dpydt = dpydt - ddydn(Tyy) - ddzup(Tyz) - ddxup(Txy)
  dpzdt = dpzdt - ddzdn(Tzz) - ddxup(Tzx) - ddyup(Tyz)
  call forceit

!-----------------------------------------------------------------------
!  Mass conservation, cell centered
!-----------------------------------------------------------------------
  drdt = drdt - ddxup1(px) - ddyup1(py) - ddzup1(pz)

!-----------------------------------------------------------------------
!  Energy conservation, cell centered
!-----------------------------------------------------------------------
  lne = alog(e)-lnr
  lnu = lnu+lnr+lne
  dedt = dedt - P*divU &
       - ddxup1(exp(xdn(lne))*px - dx*exp(xdn(lnu))*ddxdn(lne)) &
       - ddyup1(exp(ydn(lne))*py - dy*exp(ydn(lnu))*ddydn(lne)) &
       - ddzup1(exp(zdn(lne))*pz - dz*exp(zdn(lnu))*ddzdn(lne))
  call coolit
END
    