! $Id: mhd.f90,v 1.2 2004/03/24 23:48:27 aake Exp $

!***********************************************************************
  SUBROUTINE mhd
!
!  Computes the time derivatives of the magneto-hydrodynamics equation
!  variables.  
!
!  There are 66 dn() operators and 39 up() operators, for at total of
!  840 floating point operations per mesh point in stagger operators.
!  Of these 21 are redundant, in the sense that they occur twice, and
!  could be avoided by using scratch arrays -- for clarity this is not
!  done here.
!
!-----------------------------------------------------------------------
  USE params
  USE arrays
  USE stagger
  implicit none

!-----------------------------------------------------------------------
!  Identify CVS version
!-----------------------------------------------------------------------
  character(len=70):: id='$Id: mhd.f90,v 1.2 2004/03/24 23:48:27 aake Exp $'
  if (id.ne.' ') print *,id; id=' '

!-----------------------------------------------------------------------
!  Velocities
!-----------------------------------------------------------------------
  lnr = alog(rho)
  xdnr = exp(xdn(lnr))
  ydnr = exp(ydn(lnr))
  zdnr = exp(zdn(lnr))
  Ux = px/xdnr
  Uy = py/ydnr
  Uz = pz/zdnr
  U = sqrt(xup(Ux)**2 + yup(Uy)**2  + zup(Uz)**2)
!  print *,maxval(abs(Ux)),maxval(abs(Uy)),maxval(abs(Uz))

!-----------------------------------------------------------------------
!  Pressure (P) and fast speed (cfast) -- isothermal conditions
!-----------------------------------------------------------------------
  P = rho
  cfast = sqrt((P+xup(Bx)**2 + yup(By)**2 + zup(Bz)**2)*(1./rho))

!-----------------------------------------------------------------------
!  Numerical diffusion, Richtmyer & Morton type
!-----------------------------------------------------------------------
  divU = ddxup(Ux) + ddyup(Uy) + ddzup(Uz)
  nu = nu1*dx*(U+cfast) + nu2*dx**2*smooth3(max3(-divU))

!-----------------------------------------------------------------------
!  Mass conservation, cell centered
!-----------------------------------------------------------------------
!  drdt = drdt - ddxup(px) - ddyup(py) - ddzup(pz)
  drdt = drdt &
       - ddxup1(px - nur*xdn(nu)*xdnr*ddxdn(lnr)) &
       - ddyup1(py - nur*ydn(nu)*ydnr*ddydn(lnr)) &
       - ddzup1(pz - nur*zdn(nu)*zdnr*ddzdn(lnr))

!-----------------------------------------------------------------------
!  Viscous stres tensor, diagonal and off-diagonal elements
!-----------------------------------------------------------------------
  Txx = rho*(xup(Ux)**2 - nu*ddxup(Ux)) + P
  Tyy = rho*(yup(Uy)**2 - nu*ddyup(Uy)) + P
  Tzz = rho*(zup(Uz)**2 - nu*ddzup(Uz)) + P
  Txy = exp(xdn(ydn(lnr)))*(ydn(Ux)*xdn(Uy) - xdn(ydn(nu))*(ddxdn(Uy)+ddydn(Ux))*0.5)
  Tyz = exp(ydn(zdn(lnr)))*(zdn(Uy)*ydn(Uz) - ydn(zdn(nu))*(ddydn(Uz)+ddzdn(Uy))*0.5)
  Tzx = exp(zdn(xdn(lnr)))*(xdn(Uz)*zdn(Ux) - zdn(xdn(nu))*(ddzdn(Ux)+ddxdn(Uz))*0.5)

!-----------------------------------------------------------------------
!  Gravitational potential
!-----------------------------------------------------------------------
  call gravpot (rho, phi)

!-----------------------------------------------------------------------
!  Electric current j = curl(B), edge centered
!-----------------------------------------------------------------------
  jx = ddydn(Bz) - ddzdn(By)
  jy = ddzdn(Bx) - ddxdn(Bz)
  jz = ddxdn(By) - ddydn(Bx)

!-----------------------------------------------------------------------
!  Equations of motion, face centred
!-----------------------------------------------------------------------
  dpxdt = dpxdt - ddxdn(Txx)-ddyup(Txy)-ddzup(Tzx) + zup(jy*xdn(Bz))-yup(jz*xdn(By)) - xdnr*ddxdn(phi)
  dpydt = dpydt - ddydn(Tyy)-ddzup(Tyz)-ddxup(Txy) + xup(jz*ydn(Bx))-zup(jx*ydn(Bz)) - ydnr*ddydn(phi)
  dpzdt = dpzdt - ddzdn(Tzz)-ddxup(Tzx)-ddyup(Tyz) + yup(jx*zdn(By))-xup(jy*zdn(Bx)) - zdnr*ddzdn(phi)

!-----------------------------------------------------------------------
!  Electric field   E = eta j - (u x B), edge centered
!-----------------------------------------------------------------------
  Ex = ydn(zdn(nu))*jx*2. - zdn(Uy)*ydn(Bz) + ydn(Uz)*zdn(By)
  Ey = zdn(xdn(nu))*jy*2. - xdn(Uz)*zdn(Bx) + zdn(Ux)*xdn(Bz)
  Ez = xdn(ydn(nu))*jz*2. - ydn(Ux)*xdn(By) + xdn(Uy)*ydn(Bx)

!-----------------------------------------------------------------------
!  Magnetic field's time derivative, dBdt = - curl(E), face centered
!-----------------------------------------------------------------------
  dBxdt = dBxdt + ddzup(Ey) - ddyup(Ez)
  dBydt = dBydt + ddxup(Ez) - ddzup(Ex)
  dBzdt = dBzdt + ddyup(Ex) - ddxup(Ey)

  END
