#!/bin/sh
# @ job_name = demo
# @ job_type = parallel
# @ total_tasks = 4
# @ class = qexp
# @ input = /dev/null
# @ output = $(job_name).$(jobid).o
# @ error =  $(job_name).$(jobid).e
# @ notification = never
# @ queue

export OMP_NUM_THREADS=4

cd ComputerPhysics/13_StarFormation
../sleipner.x < in.dat

