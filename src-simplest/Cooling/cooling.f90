! $Id: cooling.f90,v 1.5 2004/05/19 13:45:51 antonio Exp $
!***********************************************************************
MODULE radiation
  real kappa,pkappa,stefan,ctemp,fric, Ftrick
END MODULE
!
!-----------------------------------------------------------------------
SUBROUTINE init_radiation
  use radiation
  namelist /rad/kappa,pkappa,stefan,ctemp,fric, Ftrick
  kappa=0.
  pkappa=1.
  stefan=0.
  fric=1.
  utemp=0.027139394  
  write (*,rad)
  read  (*,rad)
  write (*,rad)
END SUBROUTINE



  SUBROUTINE coolit

  USE params
  USE arrays
  USE radiation

  implicit none

  integer :: ix, iy, iz
  real, dimension(mx,my,mz):: s,dtau,q
  real, dimension(mx)      :: temper, ee
  real, dimension(mx,my)   :: cooling
  real rkq, maxtem, mintem
  character(len=72):: id="$Id"

!-----------------------------------------------------------------------
  if (id .ne. ' ') then
    print *,id
    id = ' '
    call init_radiation
  end if

maxtem=0.
mintem=1e7
!$omp parallel do private(iz,iy,ee,temper)
  do iz=1,mz
      do iy=1,my
            dtau(:,iy,iz) = dy*kappa*rho(:,iy,iz)
            ee(:)   = e(:,iy,iz)/rho(:,iy,iz)
            temper(:)= ctemp*ee(:)
            maxtem=max(maxtem,maxval(temper))
            mintem=min(mintem,minval(temper))
            s(:,iy,iz) = stefan*(temper(:)**2)**2
      end do
  end do

print *,'max s, temp, dtau  ', maxval(s),maxtem,maxval(dtau)
print *,'min temp', mintem
  call qtransfer(q,dtau,s)


!$omp parallel do private(iz,cooling)
!  do iz=1,mz
!    cooling      = q(:,:,iz)*rho(:,:,iz)*kappa
!    dedt(:,:,iz) = dedt(:,:,iz) + cooling
!  end do


  print *,'max q, rho', maxval(q),maxval(rho), maxval(rho*kappa*q)
  Ce = 0.
  do iz=1,mz
    do iy=1,my
    do ix=1,mx
      rkq = rho(ix,iy,iz)*kappa*q(ix,iy,iz)
      dedt(ix,iy,iz) = dedt(ix,iy,iz) + rkq
      Ce = max(Ce,abs(rkq)/e(ix,iy,iz))
    end do
    end do
  end do



END Subroutine
















