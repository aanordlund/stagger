! $Id: transfer_integral.f90,v 1.3 2004/06/15 14:56:13 aake Exp $
!***********************************************************************
SUBROUTINE radiative_transfer (dtau, s, q, i1, i2)                      ! radiative transfer solution
  use params
  real, dimension(mx,my,mz):: dtau, s, q, s1, s2
  real, dimension(mx,my):: dq2, q1, q2
  real(kind=8), dimension(mx,my):: exp1
  real i1, i2, f1, f2

  call deriv2 (dtau, s, s1, s2)                                         ! source function derivatives

!$omp do private(ix,iy,iz,q1,q2,exp1,f1,f2,dq1,dq2
  do iz=1,mz
    do ix=1,mx
      q1(ix,1) = i1-s(ix,1,iz)
    end do
    do iy=2,my
    do ix=1,mx
      exp1(ix,iy) = exp(dble(-dtau(ix,iy,iz)))                          ! exponential factor
      f1 = 1d0-exp1(ix,iy)                                              ! first derivative factor
      f2 = 1d0-exp1(ix,iy)*(1d0+dtau(ix,iy,iz))                         ! second derivative factor
      dq1          = -s1(ix,iy  ,iz)*f1 + s2(ix,iy  ,iz)*f2             ! local contributions
      dq2(ix,iy-1) =  s1(ix,iy-1,iz)*f1 + s2(ix,iy-1,iz)*f2             ! local contributions
      q1(ix,iy) = q1(ix,iy-1)*exp1(ix,iy) + dq1                         ! local contributions
    end do
    end do
    do ix=1,mx
      q2(ix,my) = i2-s(ix,my,iz)
      q(ix,my,iz) = 0.5*(q1(ix,my)+q2(ix,my))
    end do
    do iy=my-1,1,-1
    do ix=1,mx
      q2(ix,iy) = q2(ix,iy+1)*exp1(ix,iy+1) + dq2(ix,iy)                ! local contributions
      q(ix,iy,iz) = 0.5*(q1(ix,iy)+q2(ix,iy))                           ! average q
    end do
    end do
  end do
END SUBROUTINE

!-----------------------------------------------------------------------
SUBROUTINE deriv2 (ds, y, y1, y2)                                       ! first and second derivs
  use params
  real, dimension(mx,my,mz):: ds, y, y1, y2, cx, dyds

  do iz=1,mz
    do iy=2,my
      cx(:,iy-1,iz) =  1./(ds(:,iy,iz)+ds(:,iy-1,iz))                   ! inverse difference
      dyds(:,iy,iz) =  (y(:,iy,iz)-y(:,iy-1,iz))/ds(:,iy,iz)            ! first difference
    end do
    do iy=2,my-1
      y1(:,iy,iz) = (ds(:,iy+1,iz)*dyds(:,iy  ,iz) + &
                     ds(:,iy  ,iz)*dyds(:,iy+1,iz))*cx(:,iy,iz)         ! first derivative
      y2(:,iy,iz) = (dyds(:,iy+1,iz)-dyds(:,iy,iz))*2.*cx(:,iy,iz)      ! second derivative
    end do
    y1(:,1 ,iz) = (-ds(:,3   ,iz)*dyds(:,3   ,iz) + (2.*ds(:,3   ,iz)+ds(:,2 ,iz))*dyds(:,2 ,iz))*cx(:,2   ,iz)
    y1(:,my,iz) = (-ds(:,my-1,iz)*dyds(:,my-1,iz) + (2.*ds(:,my-1,iz)+ds(:,my,iz))*dyds(:,my,iz))*cx(:,my-1,iz)
    y2(:,1 ,iz) = y2(:,2   ,iz)
    y2(:,my,iz) = y2(:,my-1,iz)
  end do
END
