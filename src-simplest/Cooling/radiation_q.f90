! $Id: radiation_q.f90,v 1.8 2004/05/19 13:45:33 antonio Exp $
!***********************************************************************
! first and second derivs
! use cyclic derivatives
!
SUBROUTINE deriv2(dl, y, y1, y2)
  USE params
  
  Implicit None
  
  real, dimension(mx,my,mz), intent(in) :: y, dl
  real, dimension(mx,my,mz), intent(out):: y1, y2
  real, dimension(mx,my)                :: dydl, cl
  integer                               :: ix,iy,iz

!$omp parallel do private(iz,iy,yp,dydl,cl)
  do iz=1,mz
    ! Boundaries
    cl(:, 1)  = 1./(dl(:,1,iz)+dl(:,2,iz))
    cl(:,my)  = 0.
    dydl(:,1) = 0.
    dydl(:,my) = (y(:,my,iz) - y(:,my-1,iz))/dl(:,my,iz)
    do iy=2,my-1
      dydl(:,iy ) = (y(:,iy,iz) - y(:,iy-1,iz))/dl(:,iy,iz)
      cl(:,iy   ) = 1./(dl(:,iy,iz)+dl(:,iy+1,iz))
      y1(:,iy,iz) = (dl(:,iy,iz)*dydl(:,iy)+dl(:,iy,iz)*dydl(:,iy))*cl(:,iy)
      y2(:,iy,iz) = 2.*(dydl(:,iy) - dydl(:,iy))*cl(:,iy)
    end do
    y1(:,1 ,iz) = (-dl(:,3   ,iz)*dydl(:,3   ) + (2.*dl(:,3   ,iz)+dl(:,2 ,iz))*dydl(:,2 ))*cl(:,2   )
    y1(:,my,iz) = (-dl(:,my-1,iz)*dydl(:,my-1) + (2.*dl(:,my-1,iz)+dl(:,my,iz))*dydl(:,my))*cl(:,my-1)
    y2(:,1 ,iz) = y2(:,2   ,iz)
    y2(:,my,iz) = y2(:,my-1,iz)

  end do

END
!-----------------------------------------------------------------------
! radiative transfer solution
!
SUBROUTINE qtransfer(q, dtau, s)
 USE params
 USE radiation
 Implicit None
  
 integer :: ix,iy,iz
 real, dimension(mx,my,mz), intent(out) :: q
 real, dimension(mx,my,mz), intent(in)  :: dtau, s
 real, dimension(mx,my,mz)              :: s1, s2
 real, dimension(mx,my)                 :: dq, f1, f2, exp2
 double precision, dimension(mx,my)     :: exp1
 double precision                       :: dbl
  
 
  call deriv2(dtau, s, s1, s2)
!$omp parallel do private(dq,f1,f2,exp1,exp2,iz,iy,ix)
  do iz=1,mz
    exp1 = exp(dble(-dtau(:,:,iz)))         ! exponential factor
    exp2 = real(exp1)
    f1 = real(1d0-exp1)                      ! first derivative factor
    f2 = real(1d0-exp1*(1d0+dtau(:,:,iz)))   ! second derivative factor
    dq = -s1(:,:,iz)*f1 + s2(:,:,iz)*f2      ! local contributions
    q(:,1,iz) = - s(:,1,iz)
    do iy=2,my
      q(:,iy,iz)=q(:,iy-1,iz)*exp2(:,iy-1)+dq(:,iy-1)
    end do
    do iy=1,my
      exp2(:,my+1-iy) = q(:,iy,iz)
    end do
    q(:,:,iz) = 0.5*(q(:,:,iz) + exp2)
    q(:,:,iz) = q(:,:,iz)/Ftrick              !Trick!!!!!
  end do
END



