! $Id: transfer_feautrier.f90,v 1.1 2004/03/28 21:15:56 aake Exp $
!***********************************************************************
SUBROUTINE radiative_transfer (dtau,s,q,i1,i2)
!
  USE params
  implicit none
!
  real, dimension(mx,my,mz) :: dtau,s,q
  real, dimension(mx,my)    :: sp1,sp2,sp3
  real, dimension(mx,mz)    :: xi
  real, dimension(mx)       :: ex
  integer :: k, l, n, np, n1p, n1
  real :: a, taum, dinv, i1, i2
  logical doxi
!
  character(len=76):: id='$Id: transfer_feautrier.f90,v 1.1 2004/03/28 21:15:56 aake Exp $'

  np = my
  n1 = 20
  doxi = .false.

!$omp master
  if (id.ne.' ') print *,id ; id=' '
!$omp end master
!
!  Calculate 1-exp(-tau(l,1,n)) straight or by series expansion,
!  based on max tau(l,1,n).
!
!$omp do private(n,l,k,n1p,taum,ex,dinv,sp1,sp2,sp3)
  do n=1,mz
  taum=dtau(1,1,n)
  do l=1,mx
    taum=amax1(taum,dtau(l,1,n))
  end do
  if (taum.gt.0.1) then
    do l=1,mx
      ex(l)=1.-exp(-dtau(l,1,n))
    end do
  else
    do l=1,mx
      ex(l)=dtau(l,1,n)*(1.-.5*dtau(l,1,n)*(1.-.3333*dtau(l,1,n)))
    end do
  endif
!
!  Boundary condition at k=1.
!
  do l=1,mx
    sp2(l,1)=dtau(l,2,n)*(1.+.5*dtau(l,2,n))
    sp3(l,1)=-1.
    q(l,1,n)=s(l,1,n)*dtau(l,2,n)*(.5*dtau(l,2,n)+ex(l))
  end do
!
!  Matrix elements for k>2, k<np: [3d+3s]
!
  do k=2,np-1
  do l=1,mx
    sp2(l,k)=1.
    dinv=2./(dtau(l,k+1,n)+dtau(l,k,n))
    sp1(l,k)=-dinv/dtau(l,k,n)
    sp3(l,k)=-dinv/dtau(l,k+1,n)
  end do
  end do
!
!  Right hand sides, for k>2, k<n1
!
  n1p=min0(max0(n1,2),np-3)
  do k=2,n1p-1
  do l=1,mx
    q(l,k,n)=s(l,k,n)
  end do
  end do
!
!  RHS for k=n1p.  The equation is still in p(k), but q(k+1) is now
!  q(k+1)=p(k+1)-s(k+1), so to get p(k+1) we have to add s(k+1) to the
!  LHS, that is subtract it from the RHS.
!
  if (n1p.le.np) then
    do l=1,mx
      q(l,n1p,n)=s(l,n1p,n)-sp3(l,n1p)*s(l,n1p+1,n)
    end do
  endif
!
!  RHS for k=n1p+1.  The equation is now in Q: Q" = Q - S", but q(k-1)
!  is now p(k-1)=q(k-1)+s(k-1), so we must add sp1*s(l,k-1,n) to the RHS,
!  relative to the case below.
!
  if (n1p+1.le.np) then
    do l=1,mx
      q(l,n1p+1,n)=sp1(l,n1p+1)*s(l,n1p+1,n) &
                  +sp3(l,n1p+1)*(s(l,n1p+1,n)-s(l,n1p+2,n))
    end do
  endif
!
!  k>n1p+1,<np [2m+2a]
!
  do k=n1p+2,np-1
  do l=1,mx
    q(l,k,n)=sp1(l,k)*(s(l,k,n)-s(l,k-1,n)) &
            +sp3(l,k)*(s(l,k,n)-s(l,k+1,n))
  end do
  end do
!
!  k=n
!
  do l=1,mx
    sp2(l,np)=1.
    q(l,np,n)=-s(1,np,n)
  end do
!
!  Eliminate subdiagonal, save factors in sp1 [1d+2m+4a]
!
  do k=1,np-2
  do l=1,mx
    a=-sp1(l,k+1)/(sp2(l,k)-sp3(l,k))
    q(l,k+1,n)=q(l,k+1,n)+a*q(l,k,n)
    sp2(l,k+1)=sp2(l,k+1)+a*sp2(l,k)
    sp2(l,k)=sp2(l,k)-sp3(l,k)
  end do
  end do
  do l=1,mx
    sp2(l,np-1)=sp2(l,np-1)-sp3(l,np-1)
  end do
!
!  Backsubstitute [1d+1m+1a]
!
  do k=np-1,1,-1
  do l=1,mx
    q(l,k,n)=(q(l,k,n)-sp3(l,k)*q(l,k+1,n))/sp2(l,k)
  end do
  end do
!
!  Subtract S in the region where the equation is in P
!
  do k=1,n1p
    do l=1,mx
      q(l,k,n)=q(l,k,n)-s(l,k,n)
    end do
  end do
!
!  Surface intensity.
!
  if (doxi) then
    do l=1,mx
      xi(l,n)=(1.-ex(l))*(q(l,1,n)+s(l,1,n))+s(l,1,n)*0.5*ex(l)**2
    end do
  end if

  end do
!$omp end do
!
END
