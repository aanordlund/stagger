! $Id: radiation_cooling.f90,v 1.5 2004/06/15 14:56:57 aake Exp $
!***********************************************************************
MODULE radiation
  real kappa,pkappa,stefan
END MODULE

!-----------------------------------------------------------------------
SUBROUTINE init_radiation
  use radiation
  namelist /rad/kappa,pkappa,stefan
  kappa=0.
  pkappa=1.
  stefan=0.
  write (*,rad)
  read  (*,rad)
  write (*,rad)
END SUBROUTINE

!-----------------------------------------------------------------------
SUBROUTINE coolit
  use params
  use arrays
  use radiation
  implicit none
  real, dimension(mx,my,mz):: dtau, s, q, tau
  real rkq
  integer ix,iy,iz
  character(len=72):: id="$Id"

  if (id .ne. ' ') then
    print *,id
    id = ' '
    call init_radiation
  end if

  do iz=1,mz
    do ix=1,mx
      s(ix,1,iz) = stefan*(temp(ix,1,iz)**2)**2
      tau(ix,1,iz) = 0.
    end do
    do iy=2,my
    do ix=1,mx
      dtau(ix,iy,iz) = 0.5*(rho(ix,iy-1,iz)+rho(ix,iy,iz))**pkappa*kappa*dy ! optical depth difference
      tau(ix,iy,iz) = tau(ix,iy-1,iz)+dtau(ix,iy,iz)                        ! for debugging only
      s(ix,iy,iz) = stefan*(temp(ix,iy,iz)**2)**2                           ! source function
    end do
    end do
  end do

  call radiative_transfer (dtau, s, q, 0., 0.)

  Ce = 0.
  do iz=1,mz
    do iy=1,my
    do ix=1,mx
      rkq = rho(ix,iy,iz)*kappa*q(ix,iy,iz)
      dedt(ix,iy,iz) = dedt(ix,iy,iz) + rkq
      Ce = max(Ce,abs(rkq)/e(ix,iy,iz))
    end do
    end do
  end do
END
