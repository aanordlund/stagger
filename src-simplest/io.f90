! $Id: io.f90,v 1.8 2004/04/04 14:11:48 aake Exp $
!***********************************************************************
MODULE io
  integer:: iread, iwrite, nvar
  character(len=40):: directory, from, file, scratch
END

!***********************************************************************
SUBROUTINE save
  use params
  use arrays
  use io
  implicit none
  integer i

  nvar = 5; if (do_mhd) nvar = 8
!-----------------------------------------------------------------------
! Scratch file
!-----------------------------------------------------------------------
  if (t > tscratch) then
    tscratch = tscratch + dtscratch
    i = index(directory,' ')-1
    open (1,file=directory(1:i)//'scratch.dat',status='unknown', &
          access='direct',recl=4*mw)
    write (1,rec=1) rho
    write (1,rec=2) px
    write (1,rec=3) py
    write (1,rec=4) pz
    write (1,rec=5) e
    close (1)
    open (1,file=directory(1:I)//'time.dat',status='unknown', &
            access='direct',recl=4*3)
    write (1,rec=1) t
    write (1,rec=2) dt
    write (1,rec=3) dt
    close (1)
    print *,directory(1:i)//'scratch.dat'
  end if
!-----------------------------------------------------------------------
! Snapshot data and time files
!-----------------------------------------------------------------------
  if (t >= tsnap) then
    tsnap = tsnap + dtsnap
    i = index(directory,' ')-1
    open (1,file=directory(1:i)//'snapshot.dat',status='unknown', &
            access='direct',recl=4*mw)
    write (1,rec=1+iwrite*nvar) rho
    write (1,rec=2+iwrite*nvar) px
    write (1,rec=3+iwrite*nvar) py
    write (1,rec=4+iwrite*nvar) pz
    write (1,rec=5+iwrite*nvar) e
    close (1)
    open (1,file=directory(1:i)//'time.dat',status='unknown', &
            access='direct',recl=4*3)
    write (1,rec=1+iwrite*3) t
    write (1,rec=2+iwrite*3) dt
    write (1,rec=3+iwrite*3) dt
    close (1)
    print *,directory(1:i)//'snapshot.dat'
    iwrite = iwrite+1
  end if
END

!***********************************************************************
SUBROUTINE read_snapshot
  use params
  use arrays
  use io
  implicit none

  nvar = 5; if (do_mhd) nvar = 8
!-----------------------------------------------------------------------
! Snapshot data and time files
!-----------------------------------------------------------------------
  open (1,file=directory//from,status='old', &
          access='direct',recl=4*mw)
  read (1,rec=1+iread*nvar) rho
  read (1,rec=2+iread*nvar) px
  read (1,rec=3+iread*nvar) py
  read (1,rec=4+iread*nvar) pz
  read (1,rec=5+iread*nvar) e
  close (1)
  open (1,file=directory//'time.dat',status='unknown', &
          access='direct',recl=4*3)
  read (1,rec=1+iread*3) t
  read (1,rec=2+iread*3) dt
  read (1,rec=3+iread*3) dt
  close (1)
  
  if (iwrite == 0 .and. file == from) iwrite = iread+1
  tscratch = dtscratch*int(t/dtscratch+1.)
  tsnap = dtsnap*int(t/dtsnap+1.)

  print *,'iread,iwrite,t,file:',iread,iwrite,t,' snapshot.dat'
END
