! $Id: arrays.f90,v 1.11 2007/10/25 21:41:29 aake Exp $
MODULE arrays

! This module contains the arrays needed for solving the
! MHD equations

  USE params
  implicit none

  real, allocatable, dimension(:,:,:) :: &
    rho,lnr,P,temp,U,e,lne, &
    cwave,divU,nu,lnu,phi, &
    xdnr,ydnr,zdnr, &
    px,py,pz, &
    Bx,By,Bz, &
    Ux,Uy,Uz, &
    Ex,Ey,Ez, &
    jx,jy,jz, &
    Sxx,Syy,Szz, &
    Sxy,Szx,Syz, &
    Txx,Tyy,Tzz, &
    Txy,Tzx,Tyz, &
    drdt,dedt, &
    dpxdt,dpydt,dpzdt, &
    dBxdt,dBydt,dBzdt

   real, allocatable, dimension(:):: xm,ym,zm,xmdn,ymdn,zmdn

CONTAINS

SUBROUTINE allocate_arrays
  integer ix,iy,iz

  allocate (rho(mx,my,mz), lnr(mx,my,mz), P(mx,my,mz), U(mx,my,mz), e(mx,my,mz))
  allocate (cwave(mx,my,mz), divU(mx,my,mz), nu(mx,my,mz), temp(mx,my,mz))
  allocate (xdnr(mx,my,mz), ydnr(mx,my,mz), zdnr(mx,my,mz))
  allocate (lne(mx,my,mz), lnu(mx,my,mz), phi(mx,my,mz))
  allocate (px(mx,my,mz), py(mx,my,mz), pz(mx,my,mz))
  allocate (Ux(mx,my,mz), Uy(mx,my,mz), Uz(mx,my,mz))
  allocate (Sxx(mx,my,mz), Syy(mx,my,mz), Szz(mx,my,mz))
  allocate (Sxy(mx,my,mz), Szx(mx,my,mz), Syz(mx,my,mz))
  allocate (Txx(mx,my,mz), Tyy(mx,my,mz), Tzz(mx,my,mz))
  allocate (Txy(mx,my,mz), Tzx(mx,my,mz), Tyz(mx,my,mz))
  allocate (drdt(mx,my,mz), dedt(mx,my,mz))
  allocate (dpxdt(mx,my,mz), dpydt(mx,my,mz), dpzdt(mx,my,mz))

  !$omp do private(iz)                                                          ! loop parallel OpenMP
  do iz=1,mz                                                                    ! encourage memory distribution
    rho(:,:,iz) = 1.
    px (:,:,iz) = 0.
    py (:,:,iz) = 0.
    pz (:,:,iz) = 0.
    e  (:,:,iz) = 1.
  end do

  if (do_mhd) then                                                              ! MHD variables?
    allocate (Bx(mx,my,mz), By(mx,my,mz), Bz(mx,my,mz))
    allocate (dBxdt(mx,my,mz), dBydt(mx,my,mz), dBzdt(mx,my,mz))
    allocate (Ex(mx,my,mz), Ey(mx,my,mz), Ez(mx,my,mz))
    allocate (jx(mx,my,mz), jy(mx,my,mz), jz(mx,my,mz))
    !$omp do private(iz)
    do iz=1,mz
	  Bx(:,:,iz) = 0.
	  By(:,:,iz) = 0.
	  Bz(:,:,iz) = 0.
	end do
  end if

  allocate (xm(mx),ym(my),zm(mz),xmdn(mx),ymdn(my),zmdn(mz))                    ! initialize mesh arrays
  xm = (/(dx*(ix-mx/2-1), ix=1,mx)/); xmdn=xm-0.5*dx
  ym = (/(dy*(iy-my/2-1), iy=1,my)/); ymdn=ym-0.5*dy
  zm = (/(dz*(iz-mz/2-1), iz=1,mz)/); zmdn=zm-0.5*dz

END SUBROUTINE

END MODULE arrays
