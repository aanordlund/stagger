! $Id: divB_diffuse.f90,v 1.3 2013/11/15 13:38:40 aake Exp $
!***********************************************************************
SUBROUTINE divB_diffuse
!-----------------------------------------------------------------------
!  div(B) diffusion, using constant coefficients.  The CdivB parameter
!  should be less than 0.11 for stability.  The allowed values of the 
!  boundary indices lb1 and ub1 depend somewhat on the boundary condition
!  -- the values here are appropriate (tested) for the standard mconv
!  case.
!-----------------------------------------------------------------------
  USE params
  USE variables, only: Bx, By, Bz, dBxdt, dBydt, dBzdt
  USE arrays, only: scr1, scr2, scr3, scr4
  implicit none
  integer lb1, ub1, ub1y, ix, iy, iz
  real c, ds

  if (Cdivb == 0.0) return

  if (do_2nddiv) then
    call ddxup1_set (Bx, scr1)
  else
    call ddxup_set (Bx, scr1)
  end if
  call dumpn(scr1,'dBxdx','div.dmp',1)
  if (do_2nddiv) then
    call ddyup1_add (By, scr1)
  else
    call ddyup_add (By, scr1)
  end if
  call regularize (scr1)
  call dumpn(scr1,'dBydy','div.dmp',1)
  !$omp barrier
  if (do_2nddiv) then
    call ddzup1_add (Bz, scr1)
  else
    call ddzup_add (Bz, scr1)
  end if

  if (mpi_y == 0) then                                                  ! top boundary
    if (do_2nddiv) then                                                 ! 2nd order div() has footprint +-1
      lb1 = lb                                                          ! Bx & Bz modified inside the domain
    else                                                                ! 6th order div() has footprint +-3
      lb1 = lb+2                                                        ! two more buffer zones?
    end if
    do iy=1,lb1-1                                                       ! at ghostzones and bdry
      scr1(:,iy,:) = 0.                                                 ! zap div(B)
    enddo
  else                                                                  ! MPI boundary
    lb1 = 1                                                             ! include all points
  end if
  if (mpi_y == mpi_ny-1) then                                           ! bottom boundary
    if (do_2nddiv) then                                                 ! 2nd order
      ub1 = ub                                                          ! Bx & Bz modified inside the domain
      ub1y = ub+1
    else                                                                ! 6th order
      ub1 = ub                                                          ! two more buffer zones?
      ub1y = ub+1
    end if
    do iy=ub1+1,my                                                      ! at ghostzones and bdry
      scr1(:,iy,:) = 0.                                                 ! zap div(B)
    enddo
  else                                                                  ! MPI boundary
    ub1 = my                                                            ! include all points
    ub1y = my
  end if

  !$omp barrier
  call dumpn(scr1,'dBzdz','div.dmp',1)
  if (do_2nddiv) then
    call ddxdn1_set(scr1,scr2)
    call ddydn1_set(scr1,scr3)
    call ddzdn1_set(scr1,scr4)
  else
    call ddxdn_set(scr1,scr2)
    call ddydn_set(scr1,scr3)
    call ddzdn_set(scr1,scr4)
  endif

  !$omp barrier
  do iz=izs,ize
    do iy=lb1,ub1y; do ix=1,mx                                          ! By modified at half points inside Bx & Bz points
      ds = min(dxm(ix),dym(iy),dzm(iz))
      c=ds**2/dt*Cdivb                                                  ! CdivB < 0.11
      dBydt(ix,iy,iz) = dBydt(ix,iy,iz) + scr3(ix,iy,iz)*c
    end do; end do
    do iy=lb1,ub1; do ix=1,mx                                           ! Bx & Bz as per lb1 and ub1
      ds = min(dxm(ix),dym(iy),dzm(iz))
      c=ds**2/dt*Cdivb
      dBxdt(ix,iy,iz) = dBxdt(ix,iy,iz) + scr2(ix,iy,iz)*c
      dBzdt(ix,iy,iz) = dBzdt(ix,iy,iz) + scr4(ix,iy,iz)*c
    end do; end do
  end do
END
