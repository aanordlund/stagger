! $Id: gravpot_star.f90,v 1.1 2004/02/15 23:47:45 aake Exp $
!***********************************************************************
  subroutine gravpot (rho, phi)

  USE params
  USE forcing
  USE selfgravity
  USE stagger

  implicit none

  integer, parameter:: NF=256

  real, dimension(mx,my,mz):: rho, phi
!hpf$ align with TheCube:: rho, phi

  real, dimension(mx,my,mz):: scr, green
!hpf$ align with TheCube:: scr, green
  complex, dimension(mx/2+1,my,mz):: phicmplx
  real, dimension(mx+2,my,mz):: phi1

  real c,rhoa,k2,phi0,x,y,z,r
  integer ix,iy,iz
  real, dimension(mx+NF+2*my+NF+2*mz+NF) :: coeff
  real kx(mx),ky(my),kz(mz)

  REAL work (mx+4*mz)
  INTEGER ISYS(0:1)
  
  character:: id*72 = "$Id: gravpot_star.f90,v 1.1 2004/02/15 23:47:45 aake Exp $"
!-----------------------------------------------------------------------
  ISYS(0)=1

  if (id .ne. '') then
    print *,id
    print *,'grav,soft,dx=',grav,soft,dx
    id = ''
  end if

  if (grav .eq. 0) then
    phi = 0.
    return
  endif

  kx = (/(ix,ix=0,mx-1)/)
  ky = (/(iy,iy=0,my-1)/)
  kz = (/(iz,iz=0,mz-1)/)
  kx(mx/2+2:mx) = kx(mx/2+2:mx) - float(mx)
  ky(my/2+2:my) = ky(my/2+2:my) - float(my)
  kz(mz/2+2:mz) = kz(mz/2+2:mz) - float(mz)
  kx = (2.0*pi/(dx*mx)*kx)**2 + 1e-20
  ky = (2.0*pi/(dy*my)*ky)**2 + 1e-20
  kz = (2.0*pi/(dz*mz)*kz)**2 + 1e-20

  CALL SCFFT3D ( 0, mx, my, mz, 0.0, 0, 0, 0, 0, 0, 0, coeff, 0, 0)

  if (isolated) then
    do iz=1,mz
      z = (iz-1-mz/2)
      do iy=1,my
        y = (iy-1-my/2)
        do ix=1,mx
          x = (ix-1-mx/2)
          r = dx*sqrt(x**2+y**2+z**2)
          phi(ix,iy,iz) = -m_sun/(4.*pi*max(r,dx*1e-4))
        end do
      end do
    end do
    phi0 = phi(1,1,1)                             ! zero point
    scr = ddxup1(ddxdn1(phi))+ddyup1(ddydn1(phi))+ddzup1(ddzdn1(phi))
    scr(2:mx,2:my,2:mz) = rho(2:mx,2:my,2:mz)

    CALL SCFFT3D (+1, mx, my, mz, 1.0, scr, mx, my, phicmplx, &
                    mx/2+1, my, coeff, work, isys)
  else
    phi0 = 0.
    CALL SCFFT3D (+1, mx, my, mz, 1.0, rho, mx, my, phicmplx, &
                    mx/2+1, my, coeff, work, isys)
  end if

  c = 2.*soft**2/kx(mx/2+1)
!$omp parallel do private(ix,iy,iz,k2)
  do iz=1,mz
    do iy=1,my
      do ix=1,mx
        k2 = kx(ix) + ky(iy) + kz(iz)
        green(ix,iy,iz) = -(1./k2)*exp(-c*k2)
      enddo
    enddo
  enddo
  green(1,1,1) = 0.0
  
!$omp parallel do private(ix,iy,iz,k2)
  do iz=1,mz
    do iy=1,my
      do ix=1,mx
        phicmplx(ix,iy,iz) = grav*green(ix,iy,iz)*phicmplx(ix,iy,iz)
      end do
    end do
  end do

  c = 1./(mx*my*mz)
  CALL CSFFT3D (-1, mx, my, mz, c, phicmplx, mx/2+1, my, phi1, &
                    mx+2, my, coeff, work, isys)

  phi(1:mx,1:my,1:mz) = phi1(1:mx,1:my,1:mz)

  end
