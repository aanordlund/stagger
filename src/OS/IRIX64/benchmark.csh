#!/bin/csh


@ ncpu = 2
@ maxcpu = 16

while  ($ncpu <= $maxcpu)

  if ($ncpu ==   1) set m=100
  if ($ncpu ==   2) set m=125
  if ($ncpu ==   4) set m=160
  if ($ncpu ==   8) set m=200
  if ($ncpu ==  16) set m=250
  if ($ncpu ==  32) set m=300
  if ($ncpu ==  64) set m=400
  if ($ncpu == 128) set m=500

qsub -q linuxpar -S /bin/csh -j oe -o benchmark-$ncpu.log -l mem=${ncpu}gb,ncpus=$ncpu,cput=05:00:00 << XXX

set time = 1
cd \$PBS_O_WORKDIR

./benchmark.x << EOF
&io      nstep=10 nsnap=50 nscr=50 file="EXPERIMENTS/benchmark.dat" /
&run     Cdt=0.45 iseed=-77 /
&vars    do_pscalar=f do_energy=f do_mhd=t /
&grid    mx=$m, my=$m, mz=$m, sx=1, sy=1, sz=1 /
&pde     do_loginterp=t do_2nddiv=t nu1=0.1 nu2=0.6 nur=0.2 gamma=1. /
&eqofst  /
&force   do_force=t do_helmh=f ampl_turb=8 t_turn=.0625 k1=1 k2=2 pk=1.833 /
&selfg   grav=0. /
&init    b0=8 do_test=f /
&wave    type='y_alfven' k=1 ampl=0.1 /
&bdry    /
&expl    do_expl=f /
&cool    do_cool=f /
EOF

/bin/rm -f EXPERIMENTS/benchmark.dat EXPERIMENTS/benchmark.scr
XXX

  @ ncpu *= 2
end
