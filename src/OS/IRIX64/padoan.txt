
Speeds on sander with EXPERIMENTS/padoan.in:

	date/version	1 CPU		2 CPUs
	2003-04-17	164		
	2003-07-15	153				w fudge
	2003-08-15	153				w fudge
	2003-08-15	164				w/o fudge
	2003-08-15	164		334		w/o selfgravity
	2003-08-15	140		280		mhd_call -> mhd, w/o selfg
	2003-08-15	144		283		w selfgravity, SCFFT/CSFFT
	2003-08-15	142		234		mhd_call, fftpack, selfg

So, the FFTPACK does not scale as well with CPUs as the SCFFT/CSFFT.

14 400 MHZ IP35 Processors
CPU: MIPS R12000 Processor Chip Revision: 3.5
FPU: MIPS R12010 Floating Point Chip Revision: 3.5
Main memory size: 12288 Mbytes
Instruction cache size: 32 Kbytes
Data cache size: 32 Kbytes
Secondary unified instruction/data cache size: 8 Mbytes
