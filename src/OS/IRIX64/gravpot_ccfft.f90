! $Id: gravpot_ccfft.f90,v 1.1 2004/04/05 12:46:10 aake Exp $
!***********************************************************************
  subroutine gravpot (rho, phi)

  USE params
  USE selfgravity

  implicit none

  integer, parameter:: NF=256

  real, dimension(mx,my,mz):: rho, phi
!hpf$ align with TheCube:: rho, phi

  real, dimension(mx,my,mz):: green
  complex, dimension(mx+1,my+1,mz+1):: scr, phicmplx

  real c,rhoa,k2
  integer nperif,i,j,k
  real, dimension(mx+NF+2*my+NF+2*mz+NF) :: coeff
  real kx(mx),ky(my),kz(mz)

  real work (mx+4*mz)
  integer isys(0:1)
  
  character:: id*72 = "$Id: gravpot_ccfft.f90,v 1.1 2004/04/05 12:46:10 aake Exp $"
!-----------------------------------------------------------------------
  isys(0)=1

  if (id .ne. '') then
    print *,id
    print *,'grav,soft,dx=',grav,soft,dx
    id = ''
  end if

  if (grav .eq. 0) then
    phi = 0.
    return
  endif

  kx = (/(i,i=0,mx-1)/)
  ky = (/(i,i=0,my-1)/)
  kz = (/(i,i=0,mz-1)/)
  kx(mx/2+2:mx) = kx(mx/2+2:mx) - float(mx)
  ky(my/2+2:my) = ky(my/2+2:my) - float(my)
  kz(mz/2+2:mz) = kz(mz/2+2:mz) - float(mz)
  kx = (2.0*pi/(dx*mx)*kx)**2 + 1e-20
  ky = (2.0*pi/(dy*my)*ky)**2 + 1e-20
  kz = (2.0*pi/(dz*mz)*kz)**2 + 1e-20

  scr(1:mx,1:my,1:mz) = rho
  if (isolated) then
    scr(1,:,:) = 0.
    scr(:,1,:) = 0.
    scr(:,:,1) = 0.
    scr(mx,:,:) = 0.
    scr(:,my,:) = 0.
    scr(:,:,mz) = 0.
    rhoa = sum(scr(1:mx,1:my,1:mz))/mw
    nperif = 2*(mx*my+mx*mz+my*mz)-4*(mx+my+mz)+8
    rhoa = (rhoa*mx*my*mz)/nperif
    scr(1,:,:) = -rhoa
    scr(:,1,:) = -rhoa
    scr(:,:,1) = -rhoa
    scr(mx,:,:) = -rhoa
    scr(:,my,:) = -rhoa
    scr(:,:,mz) = -rhoa
  end if

  c = 2.*soft**2/kx(mx/2+1)
!$omp parallel do private(i,j,k,k2)
  do k=1,mz
    do j=1,my
      do i=1,mx
        k2 = kx(i) + ky(j) + kz(k)
        green(i,j,k) = -(1./k2)*exp(-c*k2)
      enddo
    enddo
  enddo
  green(1,1,1) = 0.0

  CALL CCFFT3D ( 0, mx, my, mz, 0.0, 0, 0, 0, 0, 0, 0, coeff, 0, 0)

  CALL CCFFT3D (+1, mx, my, mz, 1.0, scr, mx+1, my+1, phicmplx, &
 &                  mx+1, my+1, coeff, work, isys)
  
!$omp parallel do private(i,j,k,k2)
  do k=1,mz
    do j=1,my
      do i=1,mx
        phicmplx(i,j,k) = grav*green(i,j,k)*phicmplx(i,j,k)
      end do
    end do
  end do

  c = 1./(mx*my*mz)
  CALL CCFFT3D (-1, mx, my, mz, c, phicmplx, mx+1, my+1, scr, &
 &                  mx+1, my+1, coeff, work, isys)
  phi = scr(1:mx,1:my,1:mz)

  end

!***********************************************************************
  USE params
  mx = 64
  my = 64
  mz = 64
  dx = 1.
  dy = 1.
  dz = 1.
  call gravpot_test (mx,my,mz)
  END

  subroutine gravpot_test (nx,ny,nz)
!
  USE params
!
  implicit none
  integer nx,ny,nz
  integer, parameter:: NF=256
  real, dimension(nx,ny,nz):: phi, rho
  complex, dimension(nx+1,ny+1,nz+1):: scr, phicmplx
  real, dimension(nx+NF+2*ny+NF+2*nz+NF) :: coeff
  integer ix, iy, iz, i, j, k
  real kx, ky, kz, eps, k2
  real kx2(nx),ky2(ny),kz2(nz)
  real work (mx+4*mz), cpus(2), cpu, dtime
  integer isys(0:1)

  kx = 2.*pi/(mx*dx)*2.
  ky = 2.*pi/(my*dy)*3.
  kz = 2.*pi/(mz*dz)*4.

  do iz=1,mz
  do iy=1,my
  do ix=1,mx
    phi(ix,iy,iz) = cos((ix-1)*dx*kx) &
                  + sin((iy-1)*dx*ky) &
                  + cos((iz-1)*dx*kz)
    rho(ix,iy,iz) = cos((ix-1)*dx*kx)*kx**2 &
                  + sin((iy-1)*dx*ky)*ky**2 &
                  + cos((iz-1)*dx*kz)*kz**2
  end do
  end do
  end do

  cpu = dtime(cpus)
  do iz=1,mz
  do iy=1,my
  do ix=1,mx
    scr(ix,iy,iz) = cmplx(phi(ix,iy,iz),0.0)
  end do
  end do
  end do

  isys(0)=1
  CALL CCFFT3D ( 0, mx, my, mz, 0.0, scr, mx+1, my+1, phicmplx, 0, 0, coeff, work, isys)
  CALL CCFFT3D (+1, mx, my, mz, 1.0, scr, mx+1, my+1, phicmplx, &
 &                  mx+1, my+1, coeff, work, isys)

  kx2 = (/(i,i=0,mx-1)/)
  ky2 = (/(i,i=0,my-1)/)
  kz2 = (/(i,i=0,mz-1)/)
  kx2(mx/2+2:mx) = kx2(mx/2+2:mx) - float(mx)
  ky2(my/2+2:my) = ky2(my/2+2:my) - float(my)
  kz2(mz/2+2:mz) = kz2(mz/2+2:mz) - float(mz)
  kx2 = (2.0*pi/(dx*mx)*kx2)**2
  ky2 = (2.0*pi/(dy*my)*ky2)**2
  kz2 = (2.0*pi/(dz*mz)*kz2)**2

!$omp parallel do private(i,j,k,k2)
  do k=1,mz
    do j=1,my
      do i=1,mx
        k2 = kx2(i) + ky2(j) + kz2(k)
        phicmplx(i,j,k) = k2*phicmplx(i,j,k)
      enddo
    enddo
  enddo

  CALL CCFFT3D (-1, mx, my, mz, 1./(mx*my*mz), phicmplx, mx+1, my+1, scr, &
 &                  mx+1, my+1, coeff, work, isys)
  do iz=1,mz
  do iy=1,my
  do ix=1,mx
    phi(ix,iy,iz) = scr(ix,iy,iz)
  end do
  end do
  end do
  cpu = dtime(cpus)

  eps = maxval(abs(phi-rho))/maxval(rho)
  print *,'eps =',eps,cpu
  if (eps.lt.3e-5) then
    print *,'the selfgravity routine appears to work correctly'
  else if (eps.lt.3e-4) then
    print *,'THE SELFGRAVITY ROUTINE APPEARS TO BE SOMEWHAT IMPRECISE'
  else
    print *,'THE SELFGRAVITY ROUTINE APPEARS TO NOT WORK CORRECTLY'
  end if

  end

