! $Id: os.f90,v 1.2 2016/11/09 18:42:59 aake Exp $
!***********************************************************************
  function elapsed()

!  Return the elapsed cpu + system time since the previous call

  real cpu(2)
  elapsed = dtime(cpu)
  elapsed = cpu(1)+cpu(2)
  end
!***********************************************************************
  subroutine oflush
  USE params
  integer iostat
 
!  flush stdout (101) and stderr (102)
 
  stdin  = 100
  stdout = 101
  stderr = 102
  flush (stdout)
  flush (stderr)
  end
!***********************************************************************
  integer function lrec(m)

!  Return the RECL=LREC(M) value, for M words.

  lrec = 4*m
  end
!***********************************************************************
  integer*2 function icompare(a,b)

! compare two reals, for use in qsort

  if (a.gt.b) then
    icompare=1
  else if (a.lt.b) then
    icompare=-1
  else
    icompare=0
  endif
  end function

!***********************************************************************
  FUNCTION average (f)
  USE params

!  Return average of array.  Make sure precision is retained, by 
!  summing first individual rows, then columns, then planes.

  real:: f(mx,my,mz), fa(my,mz), fb(mz)
!-----------------------------------------------------------------------

!$ doacross local(k,j,fa), shared(fb)
  do k=1,mz
    do j=lb,ub
      fa(j,k) = sum(f(:,j,k))
    end do
    fb(k) = sum(fa(lb:ub,k))
  end do

  average = sum(fb)*(1./mx)*(1./(ub-lb+1))*(1./mz)

  END

!***********************************************************************
  FUNCTION average2 (nx,ny,nz,f)
  USE params

!  Return average of array.  Make sure precision is retained, by 
!  summing first individual rows, then columns, then planes.

  real:: f(mx,my,mz), fa(my,mz), fb(mz)
!-----------------------------------------------------------------------

!$ doacross local(k,j,fa), shared(fb)
  do k=1,nz
    do j=1,ny
      fa(j,k) = sum(f(1:nx,j,k))
    end do
    fb(k) = sum(fa(1:ny,k))
  end do

  average2 = sum(fb(1:nz))*(1./nx)*(1./ny)*(1./nz)

  END
!***********************************************************************
  subroutine aver (f, af, a1, a2)

  USE params
  real f(mx,my,mz), tmp(mx,my), av(mz), fmin(mz), fmax(mz)
!hpf$ align with TheCube:: f
!$ do across local(k,tmp)
!-----------------------------------------------------------------------

  do k=1,mz
    tmp=f(:,:,k)
    av(k)=sum(tmp(:,lb:ub))
    fmin(k)=minval(tmp(:,lb:ub))
    fmax(k)=maxval(tmp(:,lb:ub))
  end do
  af = sum(av)/(mx*(ub-lb+1)*mz)
  a1 = minval(fmin)
  a2 = maxval(fmax)
  end
