! $Id: fft3d.f90,v 1.1 2003/11/02 11:17:16 aake Exp $
!***********************************************************************
  subroutine fft3d (ut,nx,ny,nz)

  USE params
  implicit none
  integer nx,ny,nz
  real:: c
  complex:: ut(nx,ny,nz), coeff(nx+15+ny+15+nz+15), work(max(nx,ny,nz))
  character:: id*72 = "$Id: fft3d.f90,v 1.1 2003/11/02 11:17:16 aake Exp $"
!-----------------------------------------------------------------------

  if (id .ne. ' ') then
    print *,id
    id = ' '
  end if

  c = 1./(float(mx)*float(my)*float(mz))
  call ccfft3d( 0, mx, my, mz, c, ut, my, my, ut, mx, my, coeff, work, 0)
  call ccfft3d(-1, mx, my, mz, c, ut, my, my, ut, mx, my, coeff, work, 0)

  end
