! $Id: gravpot.f90,v 1.4 2003/08/15 11:40:37 aake Exp $
!***********************************************************************
  subroutine gravpot (rho, phi)

  USE params
  USE selfgravity

  implicit none

  integer, parameter:: NF=256

  real, dimension(mx,my,mz):: rho, phi
!hpf$ align with TheCube:: rho, phi

  real, dimension(mx,my,mz):: scr, green
!hpf$ align with TheCube:: scr, green
  complex, dimension(mx,my,mz):: phicmplx
!hpf$ align with TheCube:: phicmplx

  real c,rhoa,k2
  integer nperif,i,j,k
  real, dimension(mx+NF+2*my+NF+2*mz+NF) :: coeff
  real kx(mx),ky(my),kz(mz)

  REAL work (mx+4*mz)
  INTEGER ISYS(0:1)
  
  character:: id*72 = "$Id: gravpot.f90,v 1.4 2003/08/15 11:40:37 aake Exp $"
!-----------------------------------------------------------------------
  ISYS(0)=1

  if (id .ne. '') then
    print *,id
    print *,'grav,soft,dx=',grav,soft,dx
    id = ''
  end if

  if (grav .eq. 0) then
    phi = 0.
    return
  endif

  kx = (/(i,i=0,mx-1)/)
  ky = (/(i,i=0,my-1)/)
  kz = (/(i,i=0,mz-1)/)
  kx(mx/2+2:mx) = kx(mx/2+2:mx) - float(mx)
  ky(my/2+2:my) = ky(my/2+2:my) - float(my)
  kz(mz/2+2:mz) = kz(mz/2+2:mz) - float(mz)
  kx = (2.0*pi/(dx*mx)*kx)**2 + 1e-20
  ky = (2.0*pi/(dy*my)*ky)**2 + 1e-20
  kz = (2.0*pi/(dz*mz)*kz)**2 + 1e-20

  if (isolated) then
    scr = rho
    scr(1,:,:) = 0.
    scr(:,1,:) = 0.
    scr(:,:,1) = 0.
    scr(mx,:,:) = 0.
    scr(:,my,:) = 0.
    scr(:,:,mz) = 0.
    rhoa = sum(scr)/mw
    nperif = 2*(mx*my+mx*mz+my*mz)-4*(mx+my+mz)+8
    rhoa = (rhoa*mx*my*mz)/nperif
    scr(1,:,:) = -rhoa
    scr(:,1,:) = -rhoa
    scr(:,:,1) = -rhoa
    scr(mx,:,:) = -rhoa
    scr(:,my,:) = -rhoa
    scr(:,:,mz) = -rhoa
  else
    scr = rho
  end if

  c = 2.*soft**2/kx(mx/2+1)
!$omp parallel do private(i,j,k,k2)
  do k=1,mz
    do j=1,my
      do i=1,mx
        k2 = kx(i) + ky(j) + kz(k)
        green(i,j,k) = -(1./k2)*exp(-c*k2)
      enddo
    enddo
  enddo
  green(1,1,1) = 0.0

  CALL SCFFT3D ( 0, mx, my, mz, 0.0, 0, 0, 0, 0, 0, 0, coeff, 0, 0)

  CALL SCFFT3D (+1, mx, my, mz, 1.0, scr, mx, my, phicmplx, &
 &                  mx, my, coeff, work, isys)
  
!$omp parallel do private(i,j,k,k2)
  do k=1,mz
    do j=1,my
      do i=1,mx
        phicmplx(i,j,k) = grav*green(i,j,k)*phicmplx(i,j,k)
      end do
    end do
  end do

  c = 1./(mx*my*mz)
  CALL CSFFT3D (-1, mx, my, mz, c, phicmplx, mx, my, phi, &
 &                  mx, my, coeff, work, isys)

  end
