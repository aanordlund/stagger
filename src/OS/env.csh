# To be sourced, to set up environment variables

set h = `hostname`
set h = $h:r
set h = $h:r
set h = $h:r
set h = $h:r
setenv HOST $h
setenv MACHTYPE `uname -s`
if (-e EXPERIMENTS/DEFAULT) then
  setenv EXPERIMENT `sed -e '/^\#/d' -e 's/EXPERIMENT *=//' EXPERIMENTS/DEFAULT`
else if (-e EXPERIMENTS/DEFAULT.mkf) then
  setenv EXPERIMENT `sed -e '/^\#/d' -e 's/EXPERIMENT *=//' EXPERIMENTS/DEFAULT.mkf`
else
  setenv EXPERIMENT pde
endif

