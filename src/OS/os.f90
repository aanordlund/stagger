!................................................................................
MODULE validate_m
  implicit none
  logical, public, save:: validate=.false.
END MODULE validate_m

!................................................................................
SUBROUTINE set_validate (flag)
  USE validate_m
  implicit none
  logical:: flag
  validate = flag
END SUBROUTINE set_validate

!................................................................................
FUNCTION wallclock()
  USE validate_m
  implicit none
  real:: wallclock
  real, save:: previous=0.
  real(8), save:: offset=0d0
#if defined (_OPENMP)
  real(8):: omp_get_wtime
  if (validate) then
     wallclock = 0.0
     return
  end if
  if (offset==0d0) offset = omp_get_wtime()
  wallclock = omp_get_wtime() - offset
#else
  integer, save:: count, count_rate=0, count_max
  if (validate) then
     wallclock = 0.0
     return
  end if
  if (count_rate == 0) then
     call system_clock(count=count, count_rate=count_rate, count_max=count_max)
     offset = -count/real(count_rate,kind=8)
  else
     call system_clock(count=count)
  end if
  wallclock = count/real(count_rate,kind=8) + offset
  if (wallclock < previous) then
     offset = offset + real(count_max)/real(count_rate,kind=8)
     wallclock = count/real(count_rate,kind=8) + offset
  end if
#endif
  previous = wallclock
END FUNCTION wallclock

!................................................................................
FUNCTION wallclock8()
  USE validate_m
  implicit none
  real(kind=8):: wallclock8
  real(kind=8), save:: previous=0., offset=0.
#if defined (_OPENMP)
  real(8):: omp_get_wtime
  if (validate) then
     wallclock8 = 0d0
     return
  end if
  if (offset==0d0) offset = omp_get_wtime()
  wallclock8 = omp_get_wtime() - offset
#else
  integer, save:: count, count_rate=0, count_max
  if (validate) then
     wallclock8 = 0d0
     return
  end if
  if (count_rate == 0) then
     call system_clock(count=count, count_rate=count_rate, count_max=count_max)
     offset = -count/real(count_rate,kind=8)
     print '(a,f6.2,a,f6.2,a)', &
          ' wallclock started at', &
          count/(count_rate*3600.), &
          ' h, wraps at =', &
          count_max/(count_rate*3600.),' h'
  else
     call system_clock(count=count)
  end if
  wallclock8 = count/real(count_rate,kind=8) + offset
  if (wallclock8 < previous) then
     offset = offset + real(count_max)/real(count_rate,kind=8)
     wallclock8 = count/real(count_rate,kind=8) + offset
  end if
#endif
  previous = wallclock8
END FUNCTION wallclock8

!................................................................................
FUNCTION etime(cpu)
  implicit none
  real cpu(2), wallclock, etime
  etime = wallclock()
  cpu(1) = etime
  cpu(2) = 0.
END FUNCTION etime

!................................................................................
FUNCTION dtime(cpu)
  implicit none
  real cpu(2), wallclock, wc, dtime
  real,save:: otime = 0.
  wc = wallclock()
  dtime = wc-otime
  otime = wc
  cpu(1) = dtime
  cpu(2) = 0.
END FUNCTION dtime

!................................................................................
FUNCTION fdtime()
  implicit none
  !  Return the elapsed cpu + system time since the previous call
  real*4 cpu(2), dtime, fdtime
  fdtime = dtime(cpu)
END FUNCTION fdtime

!................................................................................
FUNCTION fetime()
  implicit none
  ! Return the elapsed cpu + system time since the program start
  real*4 cpu(2), etime, fetime
  Fetime = etime(cpu)
END FUNCTION fetime

!................................................................................
SUBROUTINE oflush
  implicit none
  !flush stdout (6) and stderr (?)
  !flush(stdout)
  !flush(stderr)
  !call flush_mpi
  return
END SUBROUTINE oflush

!................................................................................
!SUBROUTINE mysort (f,n)
!  implicit none
!  integer n
!  real f(n)
!  integer*2, external:: icompare
!  call qsort (f,n,4,icompare)
!END SUBROUTINE mysort

!................................................................................
INTEGER*2 FUNCTION icompare(a,b)
  implicit none
  real:: a,b
  if (a.gt.b) then
     icompare=1
  else if (a.lt.b) then
     icompare=-1
  else
     icompare=0
  endif
END FUNCTION icompare

!................................................................................
SUBROUTINE expn(n,a,b)
  implicit none
  integer i,n
  real, dimension(n)::a,b
  do i=1,n
     b(i) = exp(a(i))
  end do
END SUBROUTINE expn

!................................................................................
FUNCTION io_word ()
  use params
  implicit none
  integer io_word, word
  logical, save:: first_time=.true.
  inquire (iolength=io_word) word
  if (first_time) then
     first_time=.false.
     if (master) then
        print*,'INFO: this compiler uses recl units =', io_word
     end if
  end if
END FUNCTION io_word
!................................................................................
