
MODULE params
  integer mx,my,mz

END MODULE params

!***********************************************************************
PROGRAM test
  USE params
  mx = 250
  my = 250
  mz = 250
  read *,mx,my,mz
  call test_ifc ()
END

!***********************************************************************
SUBROUTINE test_ifc ()
  USE params
  real, dimension(mx,my,mz):: f1,f2,f3
  real cpu0, cpu, cpus(2)

  do iz=1,mz
    f1(:,:,iz) = 1.
    f2(:,:,iz) = 1.
    f3(:,:,iz) = 1.
  end do

  n = -1
  cpu0 = etime(cpus)
  cpu = cpu0
  do while (cpu-cpu0 .lt. 5.)
    call xup_set(f1,f2)
    call yup_set(f2,f3)
    call zup_set(f3,f1)
    n = n+1
    cpu = etime(cpus)
    if (n.eq.0) cpu0=cpu
  end do
  print '(i4,a,g11.3,a)',n,' iterations, ',(3.e-6*n*mx*my*mz)/(cpu-cpu0),' Mz/s'

  END

!***********************************************************************
SUBROUTINE xup_set (f, fp)
!
!  f is centered on (i-.5,j,k), fifth order stagger
!
  use params
  real, dimension(mx,my,mz):: f, fp
!-----------------------------------------------------------------------
!
  c = 3./256.
  b = -25./256.
  a = .5-b-c
  a1st = 0.5
!
  do k=1,mz
   do j=1,my
    fp(mx-2,j,k) = ( &
                   a*(f(mx-2,j,k)+f(mx-1,j,k)) &
                 + b*(f(mx-3,j,k)+f(mx  ,j,k)) &
                 + c*(f(mx-4,j,k)+f(1   ,j,k)))
    fp(mx-1,j,k) = ( &
                   a*(f(mx-1,j,k)+f(mx  ,j,k)) &
                 + b*(f(mx-2,j,k)+f(1   ,j,k)) &
                 + c*(f(mx-3,j,k)+f(2   ,j,k)))
    fp(mx  ,j,k) = ( &
                   a*(f(mx  ,j,k)+f(1   ,j,k)) &
                 + b*(f(mx-1,j,k)+f(2   ,j,k)) &
                 + c*(f(mx-2,j,k)+f(3   ,j,k)))
    fp(1   ,j,k) = ( &
                   a*(f(1   ,j,k)+f(2   ,j,k)) &
                 + b*(f(mx  ,j,k)+f(3   ,j,k)) &
                 + c*(f(mx-1,j,k)+f(4   ,j,k)))
    fp(2   ,j,k) = ( &
                   a*(f(2   ,j,k)+f(3   ,j,k)) &
                 + b*(f(1   ,j,k)+f(4   ,j,k)) &
                 + c*(f(mx  ,j,k)+f(5   ,j,k)))
   end do
   do j=1,my
    do i=3,mx-3
      fp(i  ,j,k) = ( &
                    a*(f(i  ,j,k)+f(i+1 ,j,k)) &
                  + b*(f(i-1,j,k)+f(i+2 ,j,k)) &
                  + c*(f(i-2,j,k)+f(i+3 ,j,k)))
    end do
   end do
  end do
!
  nstag = nstag+1
  nflop = nflop+8
END SUBROUTINE

!***********************************************************************
SUBROUTINE yup_set (f, fp)
!
!  f is centered on (i,j-.5,k), fifth order stagger
!
  use params
  real, intent(in) , dimension(mx,my,mz):: f
  real,              dimension(mx,my,mz):: fp
!-----------------------------------------------------------------------
!
  c = 3./256.
  b = -25./256.
  a = .5-b-c
  a1st = 0.5
!
  do k=1,mz
    do i=1,mx
      fp(i,my-2,k) = ( &
                     a*(f(i,my-2,k)+f(i,my-1,k)) + &
                     b*(f(i,my-3,k)+f(i,my  ,k)) + &
                     c*(f(i,my-4,k)+f(i,1   ,k)))
      fp(i,my-1,k) = ( &
                     a*(f(i,my-1,k)+f(i,my  ,k)) + &
                     b*(f(i,my-2,k)+f(i,1   ,k)) + &
                     c*(f(i,my-3,k)+f(i,2   ,k)))
      fp(i,my  ,k) = ( &
                     a*(f(i,my  ,k)+f(i,1   ,k)) + &
                     b*(f(i,my-1,k)+f(i,2   ,k)) + &
                     c*(f(i,my-2,k)+f(i,3   ,k)))
      fp(i,1   ,k) = ( &
                     a*(f(i,1   ,k)+f(i,2   ,k)) + &
                     b*(f(i,my  ,k)+f(i,3   ,k)) + &
                     c*(f(i,my-1,k)+f(i,4   ,k)))
      fp(i,2   ,k) = ( &
                     a*(f(i,2   ,k)+f(i,3   ,k)) + &
                     b*(f(i,1   ,k)+f(i,4   ,k)) + &
                     c*(f(i,my  ,k)+f(i,5   ,k)))
    end do
    do j=3,my-3
     do i=1,mx
      fp(i,j  ,k) = ( &
                     a*(f(i,j   ,k)+f(i,j+1 ,k)) + &
                     b*(f(i,j-1 ,k)+f(i,j+2 ,k)) + &
                     c*(f(i,j-2 ,k)+f(i,j+3 ,k)))
     end do
    end do
  end do
!
  nstag = nstag+1
  nflop = nflop+8
END SUBROUTINE

!***********************************************************************
SUBROUTINE zup_set (f, fp)
!
!  f is centered on (i,j,k-.5), fifth order stagger
!
  use params
  real, dimension(mx,my,mz):: f, fp
!-----------------------------------------------------------------------
!
  c = 3./256.
  b = -25./256.
  a = .5-b-c
  a1st = 0.5
!
  do k=3,mz-3
    do j=1,my
      do i=1,mx
        fp(i,j,k) = ( &
                     a*(f(i,j,k+1)+f(i,j,k  )) + &
                     b*(f(i,j,k+2)+f(i,j,k-1)) + &
                     c*(f(i,j,k+3)+f(i,j,k-2)))
      end do
    end do
  end do
  do j=1,my
    do i=1,mx
      fp(i,j,mz-2) = ( &
                   a*(f(i,j,mz-1)+f(i,j,mz-2)) + &
                   b*(f(i,j,mz  )+f(i,j,mz-3)) + &
                   c*(f(i,j,1   )+f(i,j,mz-4)))
      fp(i,j,mz-1) = ( &
                   a*(f(i,j,mz )+f(i,j,mz-1)) + &
                   b*(f(i,j,1  )+f(i,j,mz-2)) + &
                   c*(f(i,j,2  )+f(i,j,mz-3)))
      fp(i,j,mz) = ( &
                   a*(f(i,j,1  )+f(i,j,mz  )) + &
                   b*(f(i,j,1+1)+f(i,j,mz-1)) + &
                   c*(f(i,j,1+2)+f(i,j,mz-2)))
      fp(i,j,1) = ( &
                   a*(f(i,j,2  )+f(i,j,1   )) + &
                   b*(f(i,j,2+1)+f(i,j,mz  )) + &
                   c*(f(i,j,2+2)+f(i,j,mz-1)))
      fp(i,j,2) = ( &
                   a*(f(i,j,3  )+f(i,j,2   )) + &
                   b*(f(i,j,3+1)+f(i,j,1   )) + &
                   c*(f(i,j,3+2)+f(i,j,mz  )))
    end do
  end do
!
  nstag = nstag+1
  nflop = nflop+8
END SUBROUTINE

