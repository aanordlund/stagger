module load intel71

efc -stack_temps -O3         -fpp ifc_test1.f90 -o ifc7_test1.x -Vaxlib
efc -stack_temps -O3 -openmp -fpp ifc_test1.f90 -o ifc7_omp_test1.x -Vaxlib

efc -stack_temps -O3         -fpp ifc_test2.f90 -o ifc7_test2.x -Vaxlib
efc -stack_temps -O3 -openmp -fpp ifc_test2.f90 -o ifc7_omp_test2.x -Vaxlib

efc -stack_temps -O3         -fpp ifc_test3.f90 -o ifc7_test3.x -Vaxlib
efc -stack_temps -O3 -openmp -fpp ifc_test3.f90 -o ifc7_omp_test3.x -Vaxlib

module unload intel71
