
ifort -O3         -fpp ifc_test1.f90 -o ifc8_test1.x
ifort -O3 -openmp -fpp ifc_test1.f90 -o ifc8_omp_test1.x

ifort -O3         -fpp ifc_test2.f90 -o ifc8_test2.x
ifort -O3 -openmp -fpp ifc_test2.f90 -o ifc8_omp_test2.x

ifort -O3         -fpp ifc_test3.f90 -o ifc8_test3.x
ifort -O3 -openmp -fpp ifc_test3.f90 -o ifc8_omp_test3.x

