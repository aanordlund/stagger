xlf90_r -qsuffix=f=f90 -O3 -qtune=auto -qcache=auto -qhot etime.o ifc_test1.f90 -o ifc_test1.x
xlf90_r -qsuffix=cpp=f90 -O3 -qtune=auto -qcache=auto -qhot etime.o ifc_test2.f90 -o ifc_test2.x
xlf90_r -qsuffix=cpp=f90 -O3 -qtune=auto -qcache=auto -qhot etime.o ifc_test3.f90 -o ifc_test3.x

xlf90_r -qsuffix=f=f90 -O3 -qtune=auto -qcache=auto -qhot -qsmp=auto:omp etime.o ifc_test1.f90 -o ifc_test1_omp.x
xlf90_r -qsuffix=cpp=f90 -O3 -qtune=auto -qcache=auto -qhot -qsmp=auto:omp etime.o ifc_test2.f90 -o ifc_test2_omp.x
xlf90_r -qsuffix=cpp=f90 -O3 -qtune=auto -qcache=auto -qhot -qsmp=auto:omp etime.o ifc_test3.f90 -o ifc_test3_omp.x

