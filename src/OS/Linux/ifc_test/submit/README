
To run the test, do

	source compile.csh
	qsub ifc_test.csh

Depending on the local setup, the compile.csh and ifc_test.csh
scripts may need to be edited.

On brahe.theory.ki.ku.dk one gets:

echo 250,250,250 > in

setenv OMP_NUM_THREADS 1
dplace -x6 ifc7_omp_test.x < in
  75 iterations,    234.     Mz/s
dplace -x6 ifc8_omp_test.x < in
  67 iterations,    208.     Mz/s

setenv OMP_NUM_THREADS 4
dplace -x6 ifc7_omp_test.x < in
 221 iterations,    172.     Mz/s
dplace -x6 ifc8_omp_test.x < in
 217 iterations,    169.     Mz/s

dplace -x6 ifc7_test.x < in
  76 iterations,    237.     Mz/s
dplace -x6 ifc8_test.x < in
  75 iterations,    233.     Mz/s

echo 250,250,2500 > in

setenv OMP_NUM_THREADS 1
dplace -x6 ifc7_omp_test.x < in
   8 iterations,    239.     Mz/s
dplace -x6 ifc8_omp_test.x < in
   7 iterations,    211.     Mz/s

setenv OMP_NUM_THREADS 4
dplace -x6 ifc7_omp_test.x < in
  24 iterations,    180.     Mz/s
dplace -x6 ifc8_omp_test.x < in
  22 iterations,    171.     Mz/s

dplace -x6 ifc7_test.x < in
   8 iterations,    240.     Mz/s
dplace -x6 ifc8_test.x < in
   7 iterations,    213.     Mz/s


So, the ifc8 version is about 10% slower on a single CPU.  The trend is
the same with 4 CPUs, but the difference is smaller.

Compiling without the -openmp option demonstrates that the intrinsic
speed of the ifc8 is nearly the same as with ifc7, so the loss of speed
relative to ifc7 is in the OMP part.

Notice also the significant drop of speed per CPU (about 25%) with 4 CPUs.

Increasing the problem size so there are fewer parallel regions per unit
time does _not_ improve the situation; the reduction of speed with 4 CPUs
is about the same.  It may thus have to do with the slower memory access
when some mem references are non-local.


On the IBM sleipner.cscaa.dk (Regatta, power4, 1.? GHz), the corresponding
output is:

sleipner:ifc_test/submit> echo 250,250,250 | ./ifc_test.x
  33 iterations,    114.     Mz/s
sleipner:ifc_test/submit> echo 250,250,250 | ./ifc_test_omp.x
  35 iterations,    108.     Mz/s
sleipner:ifc_test/submit> setenv OMP_NUM_THREADS 4
sleipner:ifc_test/submit> echo 250,250,250 | ./ifc_test_omp.x
  35 iterations,    106.     Mz/s

So, the speed is lower (of course), but the speed loss due to OMP is not
as noticeable, and the speed per CPU with 4 CPUs is practically the same
as with 1 CPU.


June 14, 2004, Aake NordlunD
