# source this (dont script it)

ifort -stack_temps -O3         -fpp ifc_test.f90 -o ifc8_test.x
ifort -stack_temps -O3 -openmp -fpp ifc_test.f90 -o ifc8_omp_test.x

module load intel71
efc   -stack_temps -O3         -fpp ifc_test.f90 -o ifc7_test.x -Vaxlib
efc   -stack_temps -O3 -openmp -fpp ifc_test.f90 -o ifc7_omp_test.x -Vaxlib
module unload intel71
