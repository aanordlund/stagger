set verbose
unlimit stack

echo 250,250,250 > in

setenv OMP_NUM_THREADS 1
./ifc_test1.x < in
./ifc_test2.x < in
./ifc_test3.x < in

foreach n (1 2 4 8)
setenv OMP_NUM_THREADS $n
  ./ifc_test1_omp.x < in
  ./ifc_test2_omp.x < in
  ./ifc_test3_omp.x < in
end

echo 1000,250,250 > in

setenv OMP_NUM_THREADS 1
./ifc_test1.x < in
./ifc_test2.x < in
./ifc_test3.x < in

foreach n (1 2 4 8)
setenv OMP_NUM_THREADS $n
  ./ifc_test1_omp.x < in
  ./ifc_test2_omp.x < in
  ./ifc_test3_omp.x < in
end
