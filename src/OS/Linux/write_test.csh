#!/bin/csh

cd /scratch1/astro/aake

cat >write_test.f90 << EOF
  integer, parameter:: m=500
  real, dimension(m,m,m):: a,b,c,d
  real dtime, cpu, cpus(2)
  open(1,file='tmp.dat',access='direct',status='unknown',recl=m*m*m)
  cpu=dtime(cpus)
  write(1,rec=1) a
  write(1,rec=2) b
  write(1,rec=3) c
  write(1,rec=4) d
  close(1)
  print *,'write MB/s:',m*m*m*4*4/dtime(cpus)/1024.**2
  END
EOF

cat >read_test.f90 << EOF
  integer, parameter:: m=500
  real, dimension(m,m,m):: a,b,c,d
  real dtime, cpu, cpus(2)
  open(1,file='tmp.dat',access='direct',status='unknown',recl=m*m*m)
  cpu=dtime(cpus)
  read(1,rec=1) a
  read(1,rec=2) b
  read(1,rec=3) c
  read(1,rec=4) d
  close(1)
  print *,'read MB/s:',m*m*m*4*4/dtime(cpus)/1024.**2
  END
EOF

ifort -O3 write_test.f90
./a.out
ifort -O3 read_test.f90
./a.out
ifort -O3 -convert big_endian write_test.f90
./a.out
ifort -O3 -convert big_endian read_test.f90
./a.out

/bin/rm tmp.dat write_test.f90 read_test.f90 a.out
