! $Id: omp.f90,v 1.46 2016/11/09 22:42:42 aake Exp $
!***********************************************************************
MODULE barrier_m
  logical barrier
  character(len=8) label1
END MODULE


!***********************************************************************
SUBROUTINE test_barrier (label)
  USE omp_lib
  USE params
  implicit none
  character(len=*) label
#if defined (_OPENMP)
  if (.not. omp_in_parallel()) return
#endif
  !$omp barrier
  !$omp critical
  print *,'test_barrier:', label, omp_mythread
  !$omp end critical
  !$omp barrier
END

!***********************************************************************
SUBROUTINE barrier_omp (label)
!
!  This routine should be called whenever an "!$omp barrier" would 
!  otherwise be used.  It allows challenging the synchronization, by
!  delaying one thread at that point, instead of synchronizing.  If
!  this does not cause errors or differences, then the barrier is most
!  likely not needed.
!
!  Use the "dbg_barrier" input parameter to select the barrier to be
!  tested, and set "spin_barrier" to the number of thousands of times
!  to spin, as an active delay.  Set "dbg_select" to "dbg_omp" (=2)
!  for verbose output, which is helpful in identifying which value of
!  "dbg_barrier" belongs to which section of the code.
!
!  The idea behind this subroutine is due to Mats Carlsson, Oslo.
!
  USE omp_lib
  USE params
  USE barrier_m
  implicit none
  character(len=*) label
  logical debug
  integer i
  integer, save:: n_threads
  character(len=8) label2

  if (check_barrier) then
  end if

  if (do_trace .and. master) print*,'barrier_omp:',trim(label)
#if defined (_OPENMP)
  if (.not. omp_in_parallel() .or. omp_nthreads==1) return
#endif

  !$omp single                                                          ! need all threads to agree
    barrier = dbg_barrier .ne. (n_barrier+1)                            ! is this the one to test?
    n_barrier = n_barrier + 1                                           ! count the barriers
  !$omp end single                                                      ! implied barrier here!

  if (debug(dbg_omp)) then                                              ! make sure all threads pass here
    !$omp critical
    print 1, 'barrier_omp: rank, thread, n_barrier, dbg_barrier =', &
      label, mpi_rank, omp_mythread, n_barrier, dbg_barrier
1   format(2(1x,a),4i6)
    !$omp end critical
  end if

  if (check_barrier) then
    !$omp master
    label1 = label
    n_threads = 0
    !$omp end master
    !$omp barrier

    !$omp critical
    n_threads = n_threads+1
    !$omp end critical
    !$omp barrier

    label2 = label
    if (omp_master .and. n_threads /= omp_nthreads) then
      print *,'missing thread(s) at label '//trim(label)
    end if
    if (label1 /= label2) then
      print '(1x,a,i7,i4)','barrier_omp CHECK, local:'//label2// &
      ', master:'//label1//', rank, thread:', mpi_rank, omp_mythread
    end if
  end if
END

!***********************************************************************
SUBROUTINE set_omp
  USE omp_lib
  USE params
  implicit none
  integer iz
  real ram
  integer, parameter:: words_per_cell=60
  character(len=mid):: id="$Id: omp.f90,v 1.46 2016/11/09 22:42:42 aake Exp $"
!
#if defined (_OPENMP)
  if (omp_nz > 0) then
    call omp_set_num_threads(omp_nz)
  end if
!$omp parallel
  omp_nthreads = omp_get_num_threads()
  omp_mythread = omp_get_thread_num()
#else
  omp_mythread = 0
  omp_nthreads = 1
#endif
  omp_master = omp_mythread .eq. 0
  master = mpi_master .and. omp_master
!$omp end parallel
  omp_nz = omp_nthreads
  if (mpi_master) print *,'set_omp: omp_nz =',omp_nz

  ram = real(mx)*real(my)*real(mz)*words_per_cell*storage_size(ram)/4./1024.**3/mpi_size/max(omp_nz,1)
  if (master) then
    print *,'this run needs approximately'
    print '(f8.2,a)',ram,' GB per core',ram*mpi_size*max(omp_nz,1),' GB total'
  end if
  if (ram > maxram) then
    print '(a,2f8.2)',' ERROR: estimated memory per core larger than MAXRAM =',maxram, ram
    call end_mpi   
    stop
  end if
END

!***********************************************************************
SUBROUTINE limits_omp (n1,n2,i1,i2)
  USE omp_lib
  USE params
  implicit none
  integer n1,n2,i1,i2
!
#if defined (_OPENMP)
  if (omp_in_parallel()) then
    i1 = n1 + ((omp_get_thread_num()  )*n2)/omp_get_num_threads()
    i2 =      ((omp_get_thread_num()+1)*n2)/omp_get_num_threads()
  else
    i1 = max(1,n1)
    i2 = n2
  end if
#else
  i1 = max(1,n1)
  i2 = n2
#endif
END

!***********************************************************************
SUBROUTINE init_omp
  USE params
  implicit none
  integer omp_get_num_threads, omp_get_thread_num, iz
  character(len=mid):: id="$Id: omp.f90,v 1.46 2016/11/09 22:42:42 aake Exp $"

  call set_omp
!$omp parallel
  iys = 1 + ((omp_mythread  )*my)/omp_nthreads
  iye =     ((omp_mythread+1)*my)/omp_nthreads
  izs = 1 + ((omp_mythread  )*mz)/omp_nthreads
  ize =     ((omp_mythread+1)*mz)/omp_nthreads
  !$omp critical
  if (mpi_master) print *,'init_omp:', omp_mythread, izs, ize
  !$omp end critical
!$omp end parallel
  !call oflush

  allocate (scratch(mx,my,mz))                                          ! global scratch array
  !$omp parallel
  do iz=izs,ize
    scratch(:,:,iz) = 0.                                                ! encourage local memory
  end do
  !$omp end parallel

  n_barrier = 0

  if (check_barrier) then
    !$omp parallel
    call barrier_omp('all1')
    if (omp_master) then
      call barrier_omp('master')
    else
      call barrier_omp('local')
    end if
    !$omp end parallel
    if (master) then
      call barrier_mpi('master')
    else
      call barrier_mpi('slave')
    end if
  end if
END SUBROUTINE

!***********************************************************************
SUBROUTINE where_am_i (thread, jzs, jze)
  USE omp_lib
  USE params
  implicit none
  integer jzs, jze, thread, nthreads

#if defined (_OPENMP)
  thread = omp_get_thread_num()
  nthreads = omp_get_num_threads()
#else
  thread = 0
  nthreads = 1
#endif
  jzs = 1 + ((thread  )*mz)/nthreads
  jze =     ((thread+1)*mz)/nthreads
END SUBROUTINE

!***********************************************************************
#if ! defined (_OPENMP)
#if ! defined (__xlc__)
LOGICAL FUNCTION omp_in_parallel()
  USE params
  implicit none

  omp_in_parallel = .false.
END FUNCTION
#endif
#endif

!***********************************************************************
SUBROUTINE sum_2d_omp (f, ny, sum2d)
!
!  Return average of array in the local domain.  Make sure precision is
!  retained, by summing first individual rows, then columns, then planes.
!
!  NOTE: The return value should be thread-local variable!
!
  USE params
  implicit none
  integer ny
  real f(mx,mz,ny)
  real(kind=8) sum2d(ny)
  real(kind=8) sumx, sumz(ny)
  integer i, j, k
  character(len=mid):: id="sum_2d_omp, $Id: omp.f90,v 1.46 2016/11/09 22:42:42 aake Exp $"
!-----------------------------------------------------------------------
  call print_trace (id, dbg_io, 'BEGIN')

  !$omp single
  sum2d = 0.
  !$omp end single
  do j=1,ny
    sumz(j) = 0.
    do k=izs,ize
      sumx = 0.
      do i=1,mx
        sumx = sumx + f(i,k,j)
      end do
      sumz(j) = sumz(j) + sumx
    end do
  end do
  !$omp critical
  sum2d = sum2d + sumz
  !$omp end critical
  !$omp barrier
  !call barrier_omp('average')

  call print_trace (id, dbg_io, 'END')
END SUBROUTINE

!***********************************************************************
SUBROUTINE fmaxval_omp (label,f,fmax1)
!
!  Max value returned, and written to file
!
!-----------------------------------------------------------------------
  USE params
  USE omp_lib
  !USE io
  implicit none
  character(len=*) label
  real, dimension(mx,my,mz):: f
  integer iz, loc1(1), loc2(2), im, jm, km, lb1, ub1
  real fmax1
  logical do_io
  character(len=mid):: id='fmaxval_omp, $Id: omp.f90,v 1.46 2016/11/09 22:42:42 aake Exp $'
  integer, parameter:: lcache=32
  integer, allocatable, dimension(:,:,:) :: lmax, lmin
  real, allocatable, dimension(:,:) ::  fmax, fmin
!
  call print_id(id)

  fmax1 = 0.0
  !$omp do reduction(max:fmax1)
  do iz=1,mz
    fmax1 = max(fmax1,maxval(f(:,:,iz)))
  end do

  im=1; jm=1; km=1
  call fmax_mpi (im,jm,km,fmax1)

END SUBROUTINE fmaxval_omp

!***********************************************************************
SUBROUTINE fminval_omp (label,f,fmin1)
!
!  Max value returned, and written to file
!
!-----------------------------------------------------------------------
  USE params
  !USE io
  implicit none
  character(len=*) label
  real, dimension(mx,my,mz):: f
  integer iz, lmin1(2),im,jm,km(1),lb1,ub1
  real fmin1
  logical do_io
  character(len=mid):: id='fminval_omp, $Id: omp.f90,v 1.46 2016/11/09 22:42:42 aake Exp $'
  integer, parameter:: lcache=32
  integer, allocatable, dimension(:,:,:) :: lmax, lmin
  real, allocatable, dimension(:,:) ::  fmax, fmin
!
  call print_id(id)
!
  lb1 = max(min(lb,my),1)
  ub1 = max(min(ub,my),1)

!$omp single
  allocate (lmax(lcache,2,mz), fmax(lcache,mz))
  allocate (lmin(lcache,2,mz), fmin(lcache,mz))
!$omp end single

  do iz=izs,ize
    lmin1 = minloc(f(:,lb1:ub1,iz))
    lmin(1,:,iz) = lmin1
    lmin1(1) = max(1,min(mx       ,lmin1(1)))
    lmin1(2) = max(1,min(ub1-lb1+1,lmin1(2)))
    fmin(1,iz) = f(lmin1(1),lmin1(2)+lb1-1,iz)
  end do
  call barrier_omp('io1')
!$omp single
  km = minloc(fmin(1,:))
  if (km(1).eq.0) then
    print *,'fminval: minloc returned 0'
    print *,fmin(1,:)
    stop
  end if
  im = lmin(1,1,km(1))
  if (im.eq.0) then
    print *,'fminval: minloc returned 0'
    print *,f(:,lb1,km(1))
    stop
  end if
  jm = lmin(1,2,km(1))+lb1-1
  if (jm.eq.0) then
    print *,'fminval: minloc returned 0'
    print *,f(im,:,km(1))
    stop
  end if
  fmin1 = f(im,jm,km(1))

  call fmin_mpi (im,jm,km,fmin1)

  !if (mpi_master .and. (do_check .or. do_io(t,tscr,iscr+iscr0,nscr))) then
  !  i_check = i_check+1
  !  write (check_txt(i_check),'(1x,a8,3i5,1pg11.3)') label,im,jm,km(1),fmin1
  !end if
  if (mpi_master .and. do_logcheck .and. isubstep==1) &
    print '(1x,a,3i5,1pg11.3,2i5)',label,im,jm,km,fmin1,omp_mythread,mpi_rank
!$omp end single
END SUBROUTINE fminval_omp

!***********************************************************************
MODULE stats_m
  real fav, fa2, fmin, fmax
END MODULE

!***********************************************************************
  SUBROUTINE stats_omp (l, f)
  USE params
  USE stats_m
  implicit none
  real, dimension(mx,my,mz):: f
  real, pointer, dimension(:,:,:):: ff
  character(len=*) l
  real fmin1, fmax1, fminz(mz), fmaxz(mz), frms
  integer, dimension(3):: lmin, lmax
  integer lb1, ub1, ix, iy, iz
  real(kind=8) sumx, sumy, sumz
!-----------------------------------------------------------------------

  if (idbg.lt.2) return
  !if (master .and. do_trace) print *,mpi_rank,'stats: begin'

  call barrier_omp('stats1')
  lb1 = max(min(lb,my),1)
  ub1 = max(min(ub,my),1)

  !ff => scratch
  !!call average_omp (f, fav)
  !!$omp single
  !fav = 0.
  !!$omp end single
  !if (abs(fav).lt.1e-18) then
  !  do iz=izs,ize
  !    ff(:,:,iz) = (f(:,:,iz) - fav)**2
  !  end do
  !  call average_omp (ff, fa2)
  !  fa2 = fa2/(fav+1e-16)**2
  !else
  !  do iz=izs,ize
  !    ff(:,:,iz) = (f(:,:,iz)/(fav+1e-35)-1.)**2
  !  end do
  !  call average_omp (ff, fa2)
  !end if

  if (idbg.eq.2) then
    !$omp master
    sumz = 0.
    do iz=1,mz
      sumy = 0.
      do iy=1,my
        sumx = 0.
        do ix=1,mx
          sumx = sumx + f(ix,iy,iz)**2
        end do
        sumy = sumy + sumx
      end do
      sumz = sumz + sumy
    end do
    call sum_real8_mpi (sumz)
    frms = sumz/(real(mxtot)*real(mytot)*real(mztot))
    if (.not. isnan(frms)) frms=sqrt(frms)
    if (master) print'(1x,"stats:",1p,g12.3,2x,a)',frms,l
    !$omp end master
  else if (idbg.eq.3) then
    fmin = f(1,1,1)
    fmax = f(1,1,1)
    fmin1 = f(1,1,izs)
    fmax1 = f(1,1,izs)
    do iz=izs,ize
      do iy=1,my
      do ix=1,mx
        fmin1 = min(fmin1,f(ix,iy,iz))
        fmax1 = max(fmax1,f(ix,iy,iz))
      end do
      end do
    end do
    !$omp critical
    fmax = max(fmax,fmax1)
    fmin = max(fmin,fmin1)
    !$omp end critical
    call barrier_omp('io2')
    write (*,'(1x,a,i5,4(1pe11.3),2x,a)') 'min,av,max,rms:', &
      mpi_rank,fmin,fav,fmax,fav*sqrt(fa2),l
  else if (idbg.gt.3) then
    !$omp single
    lmin = minloc(f(:,lb1:ub1,:)) ; lmin(2)=lmin(2)+lb1
    lmax = maxloc(f(:,lb1:ub1,:)) ; lmax(2)=lmax(2)+lb1
    fmin = f(lmin(1),lmin(2),lmin(3))
    fmax = f(lmax(1),lmax(2),lmax(3))
    lmin = lmin + (/ixoff,iyoff,izoff/)
    lmax = lmax + (/ixoff,iyoff,izoff/)
    !$omp end single
    write (*,'(1x,a,i5,1pe11.3,3i5,1pe11.3,1pe11.3,3i5,1pe11.3,2x,a)') &
      'min,av,max,rms:', mpi_rank,fmin,lmin,fav,fmax,lmax,fav*sqrt(fa2),l
  end if

END SUBROUTINE stats_omp

FUNCTION elapsed ()
  USE omp_lib
  real(8) elapsed
#if defined (_OPENMP)
  elapsed = omp_get_wtime()
#else
  elapsed = 0
#endif
END FUNCTION elapsed
