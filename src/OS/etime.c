#include	<sys/types.h>
#include	<sys/times.h>

#define HZ 100.


long etime (tarray)
float tarray[];
/*
    etime:  Returns the sum of user and system CPU times in seconds.

    tarray(1)  <-  (real) user   CPU seconds elapsed since process start
    tarray(2)  <-  (real) system CPU seconds elapsed since process start
*/
{
	float tmp;
	long *ptmp = (long *) &tmp;
	static struct tms t1, t2;
	
	times (&t1);
	tarray[0] = t1.tms_utime / HZ;
	tarray[1] = t1.tms_stime / HZ;

	/* We have to return a long (32 bit), rather than a float
	  (which is a double in a function result).  Thus have to
	  copy with a pointer.
	*/
	tmp = tarray[0]+tarray[1];
	return (*ptmp);
}

long dtime (tarray)
float tarray[];
/*
    etime:  Returns the sum of user and system CPU times in seconds.

    tarray(1)  <-  (real) user   CPU seconds elapsed since process start
    tarray(2)  <-  (real) system CPU seconds elapsed since process start
*/
{
	float tmp;
	long *ptmp = (long *) &tmp;
	static struct tms t1, t2;
	
	times (&t2);
	tarray[0] = (t2.tms_utime - t1.tms_utime) / HZ;
	tarray[1] = (t2.tms_stime - t1.tms_stime) / HZ;
	t1 = t2;

	/* We have to return a long (32 bit), rather than a float
	  (which is a double in a function result).  Thus have to
	  copy with a pointer.
	*/
	tmp = tarray[0]+tarray[1];
	return (*ptmp);
}
