! $Id: ran1s.f90,v 1.1 2003/03/07 00:02:43 aake Exp $
!***********************************************************************
  FUNCTION ran1s(idum)
  INTEGER idum,IA,IM,IQ,IR,NTAB,NDIV
  REAL ran1s,AM,EPS,RNMX
  PARAMETER (IA=16807,IM=2147483647,AM=1./IM,IQ=127773,IR=2836, &
  NTAB=32,NDIV=1+(IM-1)/NTAB,EPS=1.2e-7,RNMX=1.-EPS)
  INTEGER k,iy
  if (idum.le.0) then
    idum=max(-idum,1)
  endif
  k=idum/IQ
  idum=IA*(idum-k*IQ)-IR*k
  if (idum.lt.0) idum=idum+IM
  iy=idum
  ran1s=min(AM*iy,RNMX)
  return
  END
