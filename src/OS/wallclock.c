/* $Id: wallclock.c,v 1.1 2005/01/10 16:50:21 aake Exp $

   This may need to be linked against -lrt (real time library)

*/
#include <fcntl.h>
#include <stdlib.h>
#include <time.h>
#include <sys/procfs.h>

double wallclock_(void)
{
   struct timespec t;
   clock_gettime(CLOCK_REALTIME, &t);
   /* clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &t); */
   return ((double)t.tv_sec+(double)t.tv_nsec*1.e-9);
}

