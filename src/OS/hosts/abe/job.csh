#!/bin/tcsh
#
# $HOME/.soft contains:
#
#  +mpichvmi-intel
#  @teragrid-basic
#  @globus-4.0
#  @teragrid-dev
#
#PBS -l walltime=08:00:00
#PBS -l nodes=128:ppn=4
#PBS -V
#PBS -N run1

setenv SCR /cfs/scratch/users/${USER}/STAGGER/1000/run1
mkdir -p ${SCR}
cd $SCR

setenv NP `wc -l ${PBS_NODEFILE} | cut -d' ' -f1`
#setenv F_UFMTENDIAN big

set x = padoan_cm_ifort.x
cp -p ~/bin/$MACHTYPE/$x .

mpirun -np ${NP} -machinefile ${PBS_NODEFILE} ./$x input.txt >> output.txt

