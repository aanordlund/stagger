#! /bin/csh
#PBS -S /bin/csh -j oe -v EXPERIMENT
#    -N myjob -l ncpus=04,cput=0800:00:00,walltime=168:00:00,mem=08gb -q parlinux
#    -N myjob -l ncpus=08,cput=1500:00:00,walltime=168:00:00,mem=16gb -q parlinux
#PBS -N myjob -l ncpus=16,cput=3000:00:00,walltime=168:00:00,mem=32gb -q parlinux
#    -N myjob -l ncpus=24,cput=5000:00:00,walltime=168:00:00,mem=48gb -q parlinux
#edit?  jobname       Ncpu     hours              hours (max)    RAM    queue

# $Id: lws.csh,v 1.1 2004/07/13 06:43:28 aake Exp $

# ------------------------------------------------------------------------------------
# Directory and environment setup
# ------------------------------------------------------------------------------------
cd $PBS_O_WORKDIR				# working directory
umask 026					# allow group to read
unlimit stack					# max stack size
limit core 0					# no core dump
setenv OMP_NUM_THREADS $NCPUS			# number of CPUs
setenv OMP_DYNAMIC False			# fixed number of CPUs
setenv KMP_STACKSIZE 200m			# slave CPU stack size
setenv KMP_LIBRARY turnaround                   # be selfish
setenv KMP_BLOCKTIME 10s                        # don't loose the threads during i/o
#setenv F_UFMTENDIAN big  			# big endian byte order (SLOW!!)

# ------------------------------------------------------------------------------------
# CVS stuff.  This assumes that the run dir is in a CVS repository.  For each run the
# input file is appended to runs.txt and both files are then committed to the CVS repos
# THIS PART MAY BE COMMENTED OUT IF NEEDED, BUT PLEASE KEEP IT IF AT ALL POSSIBLE!
# Using it normally only requires a "cvs add ..." of new dirs under the main run dir.
# ------------------------------------------------------------------------------------
if (! -e runs.txt) then				# if runs.txt does not exist
  ( echo ""; echo "===== $PBS_JOBID `date` =====" ; sed -e 's/Id://' input.txt ) >> runs.txt
  cvs add input.txt runs.txt			# add input and runs files
else						# or else
  cvs update runs.txt				# update runs.txt
  ( echo ""; echo "===== $PBS_JOBID `date` =====" ; sed -e 's/Id://' input.txt ) >> runs.txt
endif
cvs commit -m "$PBS_JOBID" input.txt runs.txt	# commit current versions

# ------------------------------------------------------------------------------------
# Execution.  This assumes that the $EXPERIMENT.x file has been installed ("make intall")
# to ~/bin/$MACHTYPE (to allow running under multiple architechtures).  The executable is
# copied to the run dir, to keep it with the data, and to allow the original file to be
# updated without killing running jobs.  
# ------------------------------------------------------------------------------------
set x = $EXPERIMENT.x				# edit this for special cases and tests
cp ~/bin/$MACHTYPE/$x .				# make a local copy to avoid killing jobs
dplace -x6 ./$x < input.txt >>& output.txt	# run, with output appended to output.txt

