#!/bin/sh
# @ error            = errors
# @ output           = errors
# @ job_name         = debug
# @ class            = debug
# @ job_type         = parallel
# @ node_usage       = not_shared
# @ notification     = never
# @ wall_clock_limit = 00:20:00
# @ node             = 4
# @ tasks_per_node   = 1
# @ queue

i=`date +%y%m%d`-`echo $LOADL_STEP_ID | sed -e 's/.[0-9]$//' -e 's/.*\.//'`
h=.hosts-$i
l=$i.log
ln -f -s $l log

exec >> $l 2>&1

( echo ""; echo "================= Thu Apr 27 18:38:33 CEST 2006 padoan_mpi.x ==================" )

/bin/rm -f mesh*dat
ulimit -s unlimited

/opt/ibmll/LoadL/full/bin/ll_get_machine_list ${LOADL_STEP_ID} | sed -e 's/.steno.dcsc.ku.dk//' > $h
n=`wc -l < $h`

x=lightbridge_omp.x
cp ~/bin/$MACHTYPE/$x ./
mpirun_ssh -np $n -hostfile $h ./$x
