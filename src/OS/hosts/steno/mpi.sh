#!/bin/sh
# @ error            = errors
# @ output           = errors
# @ job_name         = mpi
# @ job_type         = parallel
# @ class            = parallel
# @ node_usage       = not_shared
# @ notification     = never
# @ wall_clock_limit = 48:00:00
# @ node             = 16
# @ tasks_per_node   = 4
# @ queue

# executable name (edit)
x=padoan_cm_mpi.x

# useful names of hostfiles and logfiles
i=`~aake/bin/llid`
h=.hosts-$i
l=$i.log
ln -f -s $l log

# redirect output
exec >> $l 2>&1

( echo ""; echo "================= `date` $x ==================" )

# copy in a fresh version of the executable, and remove mesh files
cp -p $HOME/bin/x86_64/$x $x
/bin/rm -f *msh mesh*dat
ulimit -s unlimited

# get a host list and count the number of hosts
echo $LOADL_PROCESSOR_LIST | tr " " "\n"> $h
n=`wc -l < $h`

# run
mpirun_ssh -np $n -hostfile $h ./$x
