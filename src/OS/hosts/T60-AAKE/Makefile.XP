# $Id: Makefile.XP,v 1.1 2008/04/11 18:19:02 aake Exp $
#helpon
#
#  make benchmark	compiles and runs a benchmark
#  make			compiles with EXPERIMENT from EXPERIMENTS/DEFAULT
#  make trial		compiles and makes trial run
#  make install		compiles and copies the executable to ~/bin/$(MACHTYPE)/
#  make setup  		copies a template input file and jobs script to the "run" subdir
#  make check		check required environment and files
#  make timeit		determine basic operation speeds
#  make EXPERIMENT=pde	compiles with EXPERIMENT=pde
#
#helpoff

# Default actions
default: compile

help:
	@awk '/#helpon/,/#helpoff/' Makefile | grep -v '#help'

# Default compiler and linker, options, libraries, parallelization, hardware, debug
O     = o
CC    = cc
FC    = f90
F9    = f90
LC    = f90
FOPT  = -O
LIBS  =
PAR   =
HW    =
DEBUG =
MAKE  = make -j

# The compiler macro may be used to select compiler specific initialization --
# we do not set it here, to allow it to be set in the environment without -e opt.
#COMPILER = generic

# Choice of experiment
include EXPERIMENTS/DEFAULT.mkf
sinclude EXPERIMENTS/DEFAULT

# machine architecture
ifndef MACHTYPE
  MACHTYPE = $(shell uname -m)
endif
sinclude OS/$(MACHTYPE).mkf
sinclude OS/$(MACHTYPE)_$(MP).mkf
ifndef OSTYPE
  OSTYPE   = $(shell uname -o)
endif
sinclude OS/$(OSTYPE).mkf

# Method choices
MPI          = nompi.$O
PARAMS       = params.$O
FFTPACK      = UTILITIES/fftpack.$O UTILITIES/fft3d_OMP.$O
FFT          =
RESIST       = resist.$O
TIMESTEP     = timestep.$O
PARTICLES    = particles_fraction.$O
MPIRUN       = ./$(EXE) 

# Do NOT set OM here -- it overrides choices made in DEFAULT.mkf
#OM           =

# Choice of variable mesh or not
MESH         = mesh_
MESH         =

# Choice of array valued function calls or not
PDE          = pde.$O pde_lin.$O
MHD          = mhd.$O
PASSIVE      = passive.$O
STAGGER_SUBR = STAGGER/stagger_$(MESH)subr$(OM)$(MP).$O
STAGGER_FUNC = STAGGER/stagger_$(MESH)func$(OM)$(MP).$O

# These choices may be overriden, first by OS/$(MACHTYPE).mkf, then by EXPERIMENTS/$(EXPERIMENT).mkf
PDE          = pde_call.$O pde_call_lin.$O
MHD          = mhd_call.$O
PASSIVE      = passive_call.$O
STAGGER      =

STPAR        = STAGGER/stagger_par.$O
QUENCH       = STAGGER/quench.$O
RANDOM       = OS/ran1.$O
OMP          = OS/omp.$O
OS           = OS/$(MACHTYPE).$O
INITIAL      = INITIAL/generic.$O
TESTS        = INITIAL/tests.$O INITIAL/wave_tests.$O INITIAL/shock_tests.$O INITIAL/stone_tests.$O
FORCING      = FORCING/noforce.$O
EXPLODE      = FORCING/noexplode.$O
GRAVDIR      = FORCING
GRAVPOT      =
SELFGRAV     =
EOS          = EOS/ideal-gas.$O
UNITS        = EOS/ideal-gas-units.$O
COOLING      = COOLING/nocooling.$O
OPACITY      = COOLING/noopacity.$O
RADIATION    = COOLING/noradiation.$O
CONDUCTION   = COOLING/noconduction.$O
BOUNDARY     = BOUNDARIES/noboundary.$O
SYMMETRY     =
TRF          = feautrier
METRIC       = METRIC/nometric.$O
MSH          = mesh.$O
MATH         = math.$O
SLICE        = slice.$O
COURANT      = courant.$O
IO           = io.$O
INPUT        = OS/input.$O
CHECK        = check.$O
MAIN         = main.$O

# stagger loop methods (may differ btw directions)
X = do
Y = do
Z = do

# Read optional experiment, OS, compiler, and host specific makefiles
.SUFFIXES:
.SUFFIXES: .f90 .f .c .$O .obj .mkf .x

OS_COMPILER  = OS/compilers/$(COMPILER)/os.$O

sinclude OS/generic.mkf
sinclude OS/generic_$(MP).mkf
sinclude OS/compilers/$(COMPILER)/generic.mkf
sinclude OS/compilers/$(COMPILER)/$(MACHTYPE).mkf
sinclude OS/compilers/$(COMPILER)/$(CASE).mkf
sinclude OS/hosts/$(HOST).mkf
sinclude OS/hosts/$(HOST)/$(COMPILER).mkf
sinclude OS/hosts/$(HOST)/$(COMPILER)_$(MP).mkf
sinclude OS/hosts/$(HOST)/$(COMPILER)_$(CASE).mkf
sinclude EXPERIMENTS/$(EXPERIMENT).mkf
sinclude EXPERIMENTS/$(EXPERIMENT)/Makefile
sinclude EXPERIMENTS/DEFAULT

# Choices that are necessary, given other choices
SED          = if$(OM).sed
STAGGER_SUBR = STAGGER/stagger_$(MESH)subr$(OM)$(MP).$O
STAGGER_FUNC = STAGGER/stagger_$(MESH)func$(OM)$(MP).$O
STAGGER_TEST = STAGGER/stagger_test$(OM).$O

include  Makefile.dep

# Combined compiler and link options
FLAGS  = $(PAR) $(DEBUG) $(HW) $(FPE) $(CONVERT)
FFLAGS = $(FOPT) $(FLAGS) $(INC)
CFLAGS = $(COPT) $(FLAGS)
LFLAGS = $(FOPT) $(FLAGS) $(LOPT)
LDLIBS = $(LIBS) $(MPLIBS)

# Rules for making .$O of .f and .f90
.f90.o:
	$(F9) $(FFLAGS) -c -o $*.o $<
.f.o:
	$(FC) $(FFLAGS) -c -o $*.o $<
.f90.obj:
	$(F9) $(FFLAGS) /c /object:$*.obj $<
.f.obj:
	$(FC) $(FFLAGS) /c /object:$*.obj $<
.c.$O:
	$(CC) $(CFLAGS) -c -o $*.$O $<

# Check required environment and files
check:
	@if [ -z "$(HOST)" ]; then echo 'environment variable HOST undefined, please set it to `hostname -s`, or source OS/env.csh' ; fi
	@if [ -z "$(MACHTYPE)" ]; then echo 'environment variable MACHTYPE undefined, please set it to `uname -s`, or source OS/env.csh' ; fi
	@if [ -z "$(EXPERIMENT)" ]; then echo 'the default EXPERIMENT may be set with EXPERIMENT=xxxx in the file EXPERIMENTS/DEFAULT' ; fi
# @if [ ! -r OS/hosts/$(HOST).csh ]; then echo 'please put a template job script in OS/hosts/$(HOST).csh' ; fi

$(OS_COMPILER): $(PARAMS)

# store info about the last compilation
remember:
	@ echo " "; echo "     Macro values stored in EXPERIMENTS/DEFAULT:"
	@ ( echo "EXPERIMENT = $(EXPERIMENT)"; echo "FOPT  = $(FOPT)"; echo "DEBUG = $(DEBUG)"; \
	    echo "PAR   = $(PAR)"; echo "HW    = $(HW)"; echo "MP    = $(MP)"; echo COMPILER = $(COMPILER) ) \
	    | tee EXPERIMENTS/DEFAULT
forget:
	@ echo " "; echo "     Macro values stored in EXPERIMENTS/DEFAULT:"
	@ ( echo "EXPERIMENT = $(EXPERIMENT)" ) | tee EXPERIMENTS/DEFAULT

# Main program
EX1 = $(EXPERIMENT)_$(COMPILER)_$(CASE)$(MP).x
EX2 = $(subst __,_,$(EX1))
EXE = $(subst _.,.,$(EX2))
compile: check $(EXE)
OBJ = $(QUENCH) $(STAGGER_SUBR) $(STAGGER) $(STPAR) $(OSEXTRA) $(EXTRA) $(FFT) $(PARAMS) $(METRIC) $(UNITS) \
      $(EOS) $(STAGGER_TEST) $(FORCING) $(SELFGRAV) $(GRAVPOT) $(EXPLODE) $(SYMMETRY) $(BOUNDARY) $(OPACITY) $(RADIATION) \
      $(COOLING) $(CONDUCTION) $(PASSIVE) $(RESIST) $(MHD) $(PDE) $(TIMESTEP) $(INITIAL) $(PARTICLES) \
      $(TESTS) $(RANDOM) $(OS) $(OS_COMPILER) $(MPI) $(OMP) $(IO) $(MSH) $(MATH) $(COURANT) $(SLICE) \
      $(MAIN) $(CHECK) $(INPUT)
$(EXE): $(OBJ)
	$(LC) $(LFLAGS) $(OBJ) -o $(EXE) $(LDLIBS)
	@echo "BUILT WITH:" | tee $(EXE).opts
	@echo "make EXPERIMENT=$(EXPERIMENT) COMPILER=$(COMPILER) FOPT=$(FOPT) PAR=$(PAR) HW=$(HW) DEBUG=$(DEBUG) CASE=$(CASE)" | tee -a $(EXE).opts
	@echo "compiler: $(FC) $(FFLAGS)" | tee -a $(EXE).opts
	@echo "  linker: $(LC) $(LFLAGS)" | tee -a $(EXE).opts

experiments:
	for a in `ls -1 EXPERIMENTS/*mkf | sed -e 's:.*/::' -e 's:.mkf::'` ; do $(MAKE) reset; $(MAKE) FOPT="$(FOPT)" PAR="$(PAR)" HW="$(HW)" EXPERIMENT=$$a ; done

###############################################################################
# Trial run
trial: TRIAL/$(EXE) TRIAL/input
	sed -e 's:TRIAL/::' EXPERIMENTS/$(EXPERIMENT).in > TRIAL/input.txt
	\rm -f TRIAL/mesh.dat
	cd TRIAL/; $(MPIRUN)
TRIAL/$(EXE): $(EXE) 
	cp -p $(EXE) TRIAL/
TRIAL/input.txt: EXPERIMENTS/$(EXPERIMENT).in
	cp -p EXPERIMENTS/$(EXPERIMENT).in TRIAL/input.txt
TRIAL/input:
	-(s=`pwd`; cd ../input; i=`pwd`; cd $$s/TRIAL; ln -sf $$i .)

###############################################################################
# Demo run
demo: $(EXPERIMENT)$(MP).x
	time $(EXPERIMENT)$(MP).x < TESTS/demo.in | tee TESTS/demo.log
demo.log: $(EXPERIMENT)$(MP).x
	$(EXPERIMENT)$(MP).x < TESTS/demo.in | tee TESTS/demo.log

###############################################################################
# Directory for binaries
BIN = $(HOME)/bin
$(BIN):
	mkdir $(BIN)
BINDIR = $(BIN)/$(MACHTYPE)
$(BINDIR):
	mkdir $(BINDIR)
$(BINDIR)/$(EXE): $(BIN) $(BINDIR) $(EXE)
	-mv $(BINDIR)/$(EXE) $(BINDIR)/$(EXE)_prv 
	cp -p $(EXE) $(BINDIR)

###############################################################################
# Install actions
RUNDIR = run
install:: compile $(BINDIR)/$(EXE)
setup:: $(RUNDIR) $(RUNDIR)/$(HOST).tmp $(RUNDIR)/input.tmp $(RUNDIR)/input $(RUNDIR)/src
$(RUNDIR):
	@echo "#############################################################################"
	@echo "making local run directory -- consider making this a link to another filesys"
	mkdir $(RUNDIR)
	@echo "#############################################################################"
$(RUNDIR)/$(HOST).tmp: OS/hosts/$(HOST).csh
	cp -p OS/hosts/$(HOST).csh $(RUNDIR)/$(HOST).tmp
$(RUNDIR)/input.tmp: EXPERIMENTS/$(EXPERIMENT).in
	sed -e 's/ file=".*"//' EXPERIMENTS/$(EXPERIMENT).in > $(RUNDIR)/input.tmp
$(RUNDIR)/input:
	(s=`pwd`; cd ../input; i=`pwd`; cd $$s/$(RUNDIR); ln -sf $$i .)
$(RUNDIR)/src:
	(s=`pwd`; cd $$s/$(RUNDIR); ln -sf $$s src)

###############################################################################
# Stagger directory (can have alternatives!)
D = STAGGER/

# main targets
STGR_FUNC = $(D)stagger_$(MESH)func$(OM)$(MP).f90
STGR_SUBR = $(D)stagger_$(MESH)subr$(OM)$(MP).f90
CLEANER = $(STGR_FUNC) $(STGR_SUBR)
all-stagger: $(STGR_FUNC) $(STGR_SUBR)

# source files (MESH may be defined if needed)
STGR0 = \
 $(D)dif2$(MP).f90 \
 $(D)x_$(X).f90  $(D)ddi_x_$(X).f90 $(D)dd_x_$(MESH)$(X).f90 \
 $(D)y_$(Y).f90  $(D)ddi_y_$(Y).f90 $(D)dd_y_$(MESH)$(Y).f90 \
 $(D)z_$(Z).f90  $(D)ddi_z_$(Z).f90 $(D)dd_z_$(MESH)$(Z).f90

# extra functions
STGR1 = $(D)smooth_max_do$(MP).f90

# targets
$(STGR_FUNC): $(STGR0) $(STGR1) $(D)$(SED) $(D)stagger_head.f90 $(D)stagger_tail.f90
	sed -f $(D)$(SED) $(D)stagger_head.f90 $(STGR0) $(STGR1) $(D)stagger_tail.f90 > $(STGR_FUNC)
$(STGR_SUBR): $(STGR0) $(STGR1) $(D)$(SED) $(D)stagger_set.sed $(D)stagger_add.sed $(D)stagger_sub.sed $(D)stagger_neg.sed
	sed -f $(D)$(SED) $(STGR0) | sed -f $(D)stagger_set.sed > xxxx.f90
	cp xxxx.f90 $(STGR_SUBR)
	sed -f $(D)$(SED) $(STGR1) | sed -f $(D)stagger_set.sed >> $(STGR_SUBR)
	sed -f $(D)stagger_add.sed xxxx.f90 >> $(STGR_SUBR)
	sed -f $(D)stagger_sub.sed xxxx.f90 >> $(STGR_SUBR)
	sed -f $(D)stagger_neg.sed xxxx.f90 >> $(STGR_SUBR)
	rm xxxx.f90

# tests
stagger_test = $(OSEXTRA) $(PARAMS) $(STAGGER) STAGGER/stagger_test.$O
stagger_test.x: $(stagger_test)
	$(LC) $(LFLAGS) $(stagger_test) -o stagger_test.x $(LDLIBS)
stagger_test: stagger_test.x
	stagger_test.x

###############################################################################
# Divergence cleaning
bclean = $(PARAMS) $(OS) $(STAGGER) \
  UTILITIES/vectorpot.$O UTILITIES/fft3d.$O UTILITIES/fftpack.$O UTILITIES/main_bclean.$O
bclean.x: $(bclean)
	$(LC) $(LFLAGS) $(bclean) -o bclean.x $(LDLIBS)

###############################################################################
POWERC = UTILITIES/main_powerc.$O OS/fft3d_$(MACHTYPE).$O UTILITIES/powerc.$O UTILITIES/helmhc.$O \
	$(STAGGER) $(OS)
powerc.x: $(POWERC)
	$(FC) $(LFLAGS) $(POWERC) -o powerc.x $(LDLIBS)

###############################################################################
# Test
TEST = $(PARAMS) $(STAGGER).$O $(PDE).$O $(TIMESTEP).$O test.$O
test.x: $(TEST)
	$(LC) $(LFLAGS) $(TEST) -o test.x $(LDLIBS)

###############################################################################
# Verify
VERIFY = $(PARAMS) $(STAGGER) $(PDE) $(TIMESTEP) verify
verify.x: $(VERIFY)
	$(LC) $(LFLAGS) $(VERIFY) -o verify.x $(LDLIBS)
verify: compile
	csh TESTS/verify.csh $(BINDIR)/$(EXPERIMENT)$(MP).x ./$(EXPERIMENT)$(MP).x

###############################################################################
# Radiative transfor tests
REF = integral
TRANSFER = COOLING/transfer_$(TRF).$O
TESTS/transfer_$(REF).f90: COOLING/transfer_$(REF).f90
	sed -e 's/ transfer *(/ transfer1 (/' COOLING/transfer_$(REF).f90 > TESTS/transfer_$(REF).f90
test_radiation = $(PARAMS) $(OSEXTRA) $(TRANSFER) TESTS/test_radiation.$O TESTS/transfer_$(REF).$O io.$O $(OS) $(EOS) mesh.$O math.$O
test_radiation.x: $(test_radiation)
	$(LC) $(LFLAGS) $(test_radiation) -o test_radiation.x $(LDLIBS)
	mv test_radiation.x test_radiation_$(TRF).x
test_radiation: test_radiation.x
	echo 0,, | test_radiation_$(TRF).x | tee test_radiation.log
	(date; OS/$(MACHTYPE)/sysinfo ; echo "`grep ns/p test_radiation.log` $(FC) $(FFLAGS) -c radiation_$(TRF).f90") | tee -a OS/$(MACHTYPE)/radiation_$(HOST).log

test_transfer = $(PARAMS) $(OSEXTRA) $(FORCING) $(SELFGRAV) $(TRANSFER) TESTS/test_transfer.$O TESTS/transfer_$(REF).$O io.$O $(OS) $(EOS) mesh.$O math.$O
test_transfer.x: $(test_transfer)
	$(LC) $(LFLAGS) $(test_transfer) -o test_transfer.x $(LDLIBS)
	mv test_transfer.x test_transfer_$(TRF).x
test_transfer: test_transfer.x
	echo 0,, | test_transfer_$(TRF).x | tee test_transfer.log
	(date; OS/$(MACHTYPE)/sysinfo ; echo "`grep ns/p test_transfer.log` $(FC) $(FFLAGS) -c transfer_$(TRF).f90") | tee -a OS/$(MACHTYPE)/transfer_$(HOST).log

test_transx = $(PARAMS) transx.$O TESTS/test_transx.$O
test_transx.x: $(test_transx)
	$(LC) $(LFLAGS) $(test_transx) -o test_transx.x $(LDLIBS)
test_transx: test_transx.x
	test_transx.x

###############################################################################
# Hardware speed tests
timeit = $(OSEXTRA) TESTS/timeit.$O
timeit.x: $(timeit)
	$(LC) $(LFLAGS) $(timeit) -o timeit.x $(LDLIBS)
timeit: timeit-always timeit.x OS/$(MACHTYPE)/sysinfo
	(date; OS/$(MACHTYPE)/sysinfo ; echo "$(FC) $(FFLAGS) ..."; timeit.x) | tee -a OS/$(MACHTYPE)/timeit_$(HOST).log
timeit-always:
	\rm -f $(timeit)
OS/$(MACHTYPE)/sysinfo:
	cp OS/sysinfo OS/$(MACHTYPE)/sysinfo

###############################################################################
# Benchmark runs
benchmark:
	@echo " "
	@echo "................................................................"
	@echo ".  Set env variable COMPILER to choose compiler"
	@echo ".  Currently COMPILER=$(COMPILER)"
	@echo ".  Possible choices 'PGI', 'ifort', 'pathscale'"
	@echo ".  Use 'make clean' to clean up when changing"
	@echo "................................................................"
	@echo " "
	$(MAKE) EXPERIMENT=benchmark MAXCPU=$(MAXCPU) MAXSIZE=$(MAXSIZE)
	csh EXPERIMENTS/benchmark.csh

###############################################################################
# Basic test of the PDE routine
test: test.x
	test.x | tee test.log
test.log: test.x
	test.x | tee test.log

###############################################################################
# Make reference data set and comparison
reference:
	cat *.f90 */*.f90 | grep '\!.*$$Id' > TESTS/reference.log
	export MAKE=$(MAKE) ; csh TESTS/reference.csh | tee -a TESTS/reference.log
compare:
	cat *.f90 */*.f90 | grep '\!.*$$Id' > TESTS/compare.log
	export MAKE=$(MAKE) ; csh TESTS/compare.csh | tee -a TESTS/compare.log

###############################################################################
# Clean up tasks
cleanobj:
	rm -f $(OBJ)
clean:
	rm -f   *.$O   *.mod   *.M   core   core.*   ifc* $(CLEAN)
	rm -f  */*.$O */*.mod */*.M */core */core.* */ifc* 
cleaner: clean
	rm -rf   *.list   *.lst   *.il   a.out   rii_files spool* $(CLEANER)
	rm -rf */*.list */*.lst */*.il */a.out */rii_files S*/*.$O
cleandata:
	rm -f   *.dat   *.scr   *.log   *.tim   *.time   *.dx   *.txt
	rm -f */*.dat */*.scr */*.log */*.tim */*.time */*.dx */*.txt
CLEANEST = *.x */*.x
cleanest: cleaner cleandata
	rm -f $(CLEANEST)

###############################################################################
#  This is needed to force recompilation for a different experiment
reset:
	rm -f EOS/*unit*.$O FORCING/*.$O *mpi.$O UTILITIES/*mpi.$O
