#! /bin/csh
#PBS -S /bin/csh -j oe -m be -N a2a
#PBS -l nodes=504:ppn=32:xe,walltime=00:15:00

cd $PBS_O_WORKDIR			# working directory
unlimit stack				# max stack size
limit core 0				# no core dump

date
module list
printenv

# -----------------------------------------------------------------------------
# Env var to trim for scaling to large number of cores
# -----------------------------------------------------------------------------
setenv MPICH_ENV_DISPLAY
# Possibly outdated
#setenv MPICH_MSGS_PER_PROC 64512                # may need to be increased (def 16k)
#setenv MPI_MSGS_PER_PROC 64512                  # may need to be increased (def 16k)
# Cray presentation
#setenv MPICH_GNI_MAX_VSHORT_MSG_SIZE 216        # for large Nmpi
#setenv MPICH_GNI_RECV_CQ_SIZE 204800            # buffer for receive mshs (def 40k)
#setenv MPICH_ALLTOALL_SHORT_MSG 512

set b = ~/codes/stagger/src
aprun -n  1008 $b/TESTS/mpi_alltoall.x; date
unsetenv MPICH_ENV_DISPLAY
aprun -n  2016 $b/TESTS/mpi_alltoall.x; date
aprun -n  4032 $b/TESTS/mpi_alltoall.x; date
aprun -n  8064 $b/TESTS/mpi_alltoall.x; date
aprun -n 16128 $b/TESTS/mpi_alltoall.x; date
#aprun -n 32256 $b/TESTS/mpi_alltoall.x
