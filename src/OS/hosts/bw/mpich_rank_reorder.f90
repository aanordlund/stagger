! $Id: mpich_rank_reorder.f90,v 1.7 2015/01/07 14:37:42 aake Exp $
!-------------------------------------------------------------------------------
! Program to write out the file MPICH_RANK_ORDER, which should contain a list
! of MPI ranks given to each consecutive process.  The numbers 0-31 correspond
! to the first node with 32 cores, the next 32 numbers are in the next node, etc.
!
! In other words, the numbers in the file is rank as a function of process number, 
! where process number runs over consecutive cores on consecutive nodes. It is 
! important to not mix this up with a list of process number as a function of 
! rank. 
!
! An MPI program on BW uses this file to do rank reordering if the environment
! variable MPICH_RANK_REORDER_METHOD is set to 3.
!
! Ranks in a Fortran program run in z-y-x order; i.e., the z-dimension
! changes most rapidly, which is why BW with the default mapping runs fastest 
! when the domain z-dimension is as small as possible; since ranks are close 
! (often share memory) in the z-direction, MPI-operations are cheapest in that
! direction. 
!
! To find rank as a function of process number we can use that 
!   rank=(mpi_z-1)+(mpi_y-1)*mpi_nz+(mpi_x-1)*mpi_ny*mpi_nz
! and that we want the mapping from (mpi_x,mpi_y,mpi_z) to processor core to be 
!   core=mod(mpi_z-1,4)+mod(mpi_x-1,4)*4+mod(mpi_y,2)*8
! and the mapping from mpi_rank to node to be mpi_rank/32, so the complete 
! mapping is
!   mpi_x=rank/(mpi_ny*mpi_nz)
!   mpi_y=mod(rank/mpi_nz,mpi_ny)
!   mpi_z=mod(rank,mpi_nz)
!   proc=mod(mpi_z-1,4)+mod(mpi_x-1,4)*4+mod(mpi_y,2)*8+(rank/32)*32
!-------------------------------------------------------------------------------
PROGRAM mpich_rank_reorder
  !USE mpi
  implicit none
  include 'mpif.h'
  character(len=100):: meshfile                     ! redundant file name in the namelist
  real:: sx,sy,sz,xmin,ymin,zmin,omega,maxram       ! redundant reals in the namelist
  integer:: omp_nz,lb,ub                            ! redundant integers in the namelist
  integer:: mx,my,mz                                ! domain dimensions
  integer:: mpi_nx,mpi_ny,mpi_nz                    ! MPI dimensions
  integer:: mpi_err,mpi_size,mpi_rank               ! MPI variables
  integer:: nodes,node_ranks,mpi_nmax               ! number of nodes and ranks per node
  integer:: node_nx,node_ny,node_nz                 ! node dimensions
  integer:: x_nodes,y_nodes,z_nodes                 ! number of nodes in each direction
  integer:: node_x,node_y,node_z                    ! number of cells in each direction
  integer:: ix,iy,iz,n,core,rank,proc               ! loop indices
  integer:: mpi_x,mpi_y,mpi_z                       ! MPI coordinates
  integer:: x_incr,y_incr,z_incr,node_incr          ! increments
  namelist /grid/mx,my,mz,mpi_nx,mpi_ny,mpi_nz,mpi_nmax,omp_nz,maxram,sx,sy,sz,xmin,ymin,zmin,lb,ub,omega,meshfile
  namelist /node/node_ranks,node_nx,node_ny,node_nz,mpi_nx,mpi_ny,mpi_nz
    
  !-----------------------------------------------------------------------------
  ! Find out how many MPI processes there are, and keep only the master
  !-----------------------------------------------------------------------------
  call MPI_INIT (mpi_err)
  call MPI_COMM_SIZE (mpi_comm_world, mpi_size, mpi_err)
  call MPI_COMM_RANK (mpi_comm_world, mpi_rank, mpi_err)
  if (mpi_rank>0) then
    call MPI_Finalize(mpi_err)
    call exit
  end if

  !-----------------------------------------------------------------------------
  ! Read the input.txt file
  !-----------------------------------------------------------------------------
  print*,'mpi_size, mpi_rank =',mpi_size  
  open(1,file='input.txt',status='old',form='formatted')
  read(1,grid)
  rewind(1)
  read(1,node)
  close(1)
  open(1,file='MPICH_RANK_ORDER',status='unknown',form='formatted')

  !-----------------------------------------------------------------------------
  ! Compute and check number of ranks in each direction
  !-----------------------------------------------------------------------------
  mpi_nz=mpi_size/(mpi_nx*mpi_ny)
  if (mpi_nx*mpi_ny*mpi_nz  /= mpi_size) then
    print *,'ERROR: incompatible rank dimensions =', &
      mpi_nx,mpi_ny,mpi_nz
    write(1,*) '# ERROR: incompatible rank dimensions =', &
      mpi_nx,mpi_ny,mpi_nz
    call MPI_Finalize(mpi_err)
    call exit
  end if
  print*,'mpi_nx, mpi_ny, mpi_nz =', mpi_nx,mpi_ny,mpi_nz
  write(1,*) '# node_nx, node_ny, node_nz =', node_nx,node_ny,node_nz
  write(1,*) '# mpi_nx, mpi_ny, mpi_nz =', mpi_nx,mpi_ny,mpi_nz
  
  !-----------------------------------------------------------------------------
  ! Compute and check number of nodes in each direction
  !-----------------------------------------------------------------------------
  node_ranks=32
  nodes=mpi_size/node_ranks
  x_nodes=mpi_nx/node_nx
  y_nodes=mpi_ny/node_ny
  z_nodes=mpi_nz/node_nz
  if (x_nodes*y_nodes*z_nodes  /= nodes) then
    print *,'ERROR: incompatible node dimensions =', &
      x_nodes,y_nodes,z_nodes
    write(1,*) '# ERROR: incompatible node dimensions =', &
      x_nodes,y_nodes,z_nodes
    call MPI_Finalize(mpi_err)
    call exit
  end if
  print*,'x_nodes, y_nodes, z_nodes =',x_nodes,y_nodes,z_nodes
  write(1,*) '# x_nodes, y_nodes, z_nodes =',x_nodes,y_nodes,z_nodes
  node_x = (mx/mpi_nx)*node_nx
  node_y = (my/mpi_ny)*node_ny
  node_z = (mz/mpi_nz)*node_nz
  print*,'node_x, node_y, node_z =',node_x,node_y,node_z
  write(1,*) '# node_x, node_y, node_z =',node_x,node_y,node_z

  !-----------------------------------------------------------------------------
  ! Compute the rank increment in the three directions inside a node
  !-----------------------------------------------------------------------------
  z_incr=1
  y_incr=node_nz
  x_incr=y_incr*node_ny
  node_incr=node_ranks
  print*,'x_incr, y_incr, z_incr, node_incr =',x_incr,y_incr,z_incr,node_incr
  write(1,*) '# x_incr, y_incr, z_incr, node_incr =',x_incr,y_incr,z_incr,node_incr

  !-----------------------------------------------------------------------------
  ! Compute the process number for each rank, determining first the mpi_xyz
  ! coordinate, then the internal core number, then the node number.
  !-----------------------------------------------------------------------------
  open(2,file='mpich_rank_reorder.log',status='unknown',form='formatted')
  do rank=0,mpi_size-1
    !---------------------------------------------------------------------------
    ! Map the rank to an MPI coordinate, with z running fastest
    !---------------------------------------------------------------------------
    mpi_x=rank/(mpi_ny*mpi_nz)
    mpi_y=mod(rank/mpi_nz,mpi_ny)
    mpi_z=mod(rank,mpi_nz)
    !---------------------------------------------------------------------------
    ! Map MPI-coordinate to a core number in the range 0-31, with nearest
    ! neighbors in x,y,z at distances (8,4,1)
    !---------------------------------------------------------------------------
    core=mod(mpi_x,node_nx)*x_incr+ &
         mod(mpi_y,node_ny)*y_incr+ &
         mod(mpi_z,node_nz)*z_incr
    !---------------------------------------------------------------------------
    ! Map MPI-coordinate to a node number in the range 0-(mpi_size/32-1)
    !---------------------------------------------------------------------------
    n=(mpi_z/node_nz)+(mpi_y/node_ny)*z_nodes+(mpi_x/node_nx)*z_nodes*y_nodes
    !---------------------------------------------------------------------------
    ! Combine the core and node number to a process number
    !---------------------------------------------------------------------------
    proc=core+n*node_ranks
    write (1,'(i8)') proc
    write (2,'(i8,2x,3i5,2x,2i5,i8)') rank,mpi_x,mpi_y,mpi_z,core,n,proc
  end do
  close (1)
  close (2)

  call MPI_Finalize(mpi_err)
END
