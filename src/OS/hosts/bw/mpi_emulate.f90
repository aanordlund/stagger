
! MPI emulation

MODULE mpi
  implicit none
  integer, parameter:: mpi_comm_world=0  
CONTAINS

SUBROUTINE MPI_Init (mpi_err)
  integer mpi_err
  mpi_err=0
END SUBROUTINE MPI_Init

SUBROUTINE MPI_Comm_size (mpi_comm, mpi_size, mpi_err)
  integer mpi_err,mpi_comm,mpi_size
  mpi_size=16128
  mpi_err=0
END SUBROUTINE MPI_Comm_size

SUBROUTINE MPI_Comm_rank (mpi_comm, mpi_rank, mpi_err)
  integer mpi_err,mpi_comm,mpi_rank
  mpi_rank=0
  mpi_err=0
END SUBROUTINE MPI_Comm_rank

SUBROUTINE MPI_Finalize (mpi_err)
  integer mpi_err
  mpi_err=0
END SUBROUTINE MPI_Finalize

END MODULE mpi
