# $Id: mpi.csh,v 1.1 2014/08/16 14:09:02 aake Exp $

# -----------------------------------------------------------------------------
# Directory and environment setup
# -----------------------------------------------------------------------------
set echo
cd $PBS_O_WORKDIR			# working directory
umask 026				# allow group to read
unlimit stack				# max stack size
limit core 0				# no core dump
limit

setenv MPICH_ENV_DISPLAY
module list

# -----------------------------------------------------------------------------
# Env var to trim for scaling to large number of cores
# -----------------------------------------------------------------------------
#setenv MPICH_PTL_UNEX_EVENTS 64512              # at east twice Nmpi
#setenv MPICH_UNEX_BUFFER_SIZE 134217728         # default is 64M
setenv MPICH_MSGS_PER_PROC 64512                # may need to be increased (def 16k)
setenv MPI_MSGS_PER_PROC 64512                  # may need to be increased (def 16k)
# Cray presentation
setenv MPICH_GNI_MAX_VSHORT_MSG_SIZE 216        # for large Nmpi
#setenv MPICH_GNI_RECV_CQ_SIZE 204800            # buffer for receive mshs (def 40k)
#setenv MPICH_ALLTOALL_SHORT_MSG 512
#printenv

# -----------------------------------------------------------------------------
# Execution.  
# -----------------------------------------------------------------------------
set name=96x30Mm_4032_t777h
set name=`date +%y%m%d_%H%M`
set x = mconv_crayftn_rays_debug_mpi.x		# edit this for specific case 
set x = mconv_crayftn_opt_debug_mpi.x		# edit this for specific case 
set x = mconv_crayftn_opt_mpi.x		        # edit this for specific case 
set x = mconv_crayftn_rays_mpi.x		# edit this for specific case 
set b = ~/codes/stagger/src
cp -p $b/$x $name.x
#\rm $x
#ln -s ~/bin/$x .				# make a local copy 

set c = "$1"					# number of cpus 
#set c = 16128
#set c = 8064

cp input.txt $name.in				# preserve input file
cat $name.in >>! $name.ins
if (-e log) mv log prv
touch $name.log                         	# create log if doesn't exist
ln -sf $name.log log
date >> $name.log
ls -lL $b/$x | cut -d ' ' -f 5-10 >> $name.log	#executable name and date
which jobid
hostname >> $name.log
echo $c >> $name.log				# number threads

aprun -n $c ./$name.x $name.in >>& $name.log	# run, with output appended 
#if ($? != 0) then 
#   sleep 1
#   date >> $name.log
#   aprun -n $c ./$name.x $name.in >>& $name.log	# another run
#endif
date >> $name.log
#qsub job.csh

