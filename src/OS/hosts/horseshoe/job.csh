#! /bin/csh
#PBS -S /bin/csh -j oe
#PBS -l walltime=20:00
#PBS -l nodes=4

cd $PBS_O_WORKDIR; umask 026

source ~/.cshrc
source TESTS/speed.csh
