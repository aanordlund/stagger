#! /bin/bash
#PBS -j oe -o log.txt -N test
#PBS -l nodes=2:nehalem,walltime=00:01:00

# $Id: job.sh,v 1.2 2009/11/13 01:50:54 aake Exp $

cd $PBS_O_WORKDIR				# working directory
umask 026					# allow group to read
export id=${PBS_JOBID%.*}			# job id
export np=`wc -l < $PBS_NODEFILE`		# number of processes
ln -sf $id.log output.txt			# job output

exec >output.txt 2>&1 				# redirect output

#jobcontrol.csh mpi.csh text.x			# comprehensive job control
mpi.csh text.x					# simple job control
