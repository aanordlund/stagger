#! /bin/csh
#PBS -S /bin/csh -j oe -o log.txT -N test
#PBS -l nodes=8:nehalem,walltime=00:01:00

# $Id: job.csh,v 1.2 2009/11/13 01:50:54 aake Exp $

cd $PBS_O_WORKDIR				# working directory
umask 026					# allow group to read
@ np = `wc -l < $PBS_NODEFILE`			# number of nodes
setenv np $np					# export number of processes
setenv id $PBS_JOBID:r				# export job id
ln -sf $id.log output.txt			# sym link to output

#jobcontrol.csh mpi.csh test.x >& output.txt	# comprehensive job control
mpi.csh test.x >& output.txt			# simple job control
