#! /bin/csh
#PBS -S /bin/csh -j oe -N supergr
# BS -l ncpus=04,cput=0800:00:00,walltime=168:00:00,mem=8gb
# BS -l ncpus=08,cput=1500:00:00,walltime=168:00:00,mem=12gb
#PBS -l ncpus=16,cput=3000:00:00,walltime=168:00:00,mem=28gb
# BS -l ncpus=24,cput=5000:00:00,walltime=168:00:00,mem=44gb
# BS -l ncpus=24,cput=5000:00:00,walltime=168:00:00,mem=60gb
#edit?      Ncpu     hours               hours (max)   RAM

# $Id: job.csh,v 1.1 2007/01/14 23:55:31 aake Exp $

# ------------------------------------------------------------------------------------
# Directory and environment setup
# ------------------------------------------------------------------------------------
cd $PBS_O_WORKDIR				# working directory
umask 026					# allow group to read
unlimit stack					# max stack size
limit core 0					# no core dump
setenv OMP_NUM_THREADS $NCPUS			# number of CPUs
setenv OMP_DYNAMIC False			# fixed number of CPUs
setenv KMP_STACKSIZE 30m			# slave CPU stack size
#setenv KMP_LIBRARY throughput			# default, for shared environment
#setenv KMP_BLOCKTIME 0.3s			# near optimum, for throughput mode
setenv KMP_LIBRARY turnaround			# for minimum wall clock time batch
#setenv F_UFMTENDIAN big  			# big endian byte order (SLOW!!)
#setenv F_UFMTENDIAN big:10,12			# big endian old model(10) and table(12)
#setenv F_UFMTENDIAN big:11   			# big endian new model(11)

# ------------------------------------------------------------------------------------
# CVS stuff.  This assumes that the run dir is in a CVS repository.  For each run the
# input file is appended to runs.txt and both files are then committed to the CVS repos
# THIS PART MAY BE COMMENTED OUT IF NEEDED, BUT PLEASE KEEP IT IF AT ALL POSSIBLE!
# Using it normally only requires a "cvs add ..." of new dirs under the main run dir.
# ------------------------------------------------------------------------------------
#if (! -e runs.txt) then				# if runs.txt does not exist
#  ( echo ""; echo "===== $PBS_JOBID `date` =====" ; sed -e 's/Id://' input.txt ) >> runs.txt
#  cvs add input.txt runs.txt			# add input and runs files
#else						# or else
#  cvs update runs.txt				# update runs.txt
#  ( echo ""; echo "===== $PBS_JOBID `date` =====" ; sed -e 's/Id://' input.txt ) >> runs.txt
#endif
#cvs commit -m "$PBS_JOBID" input.txt runs.txt	# commit current versions

# ------------------------------------------------------------------------------------
# Execution.  This assumes that the $EXPERIMENT.x file has been installed ("make intall")
# to ~/bin/$MACHTYPE (to allow running under multiple architechtures).  The executable is
# copied to the run dir, to keep it with the data, and to allow the original file to be
# updated without killing running jobs.  
# ------------------------------------------------------------------------------------
set x = supergr.x				# edit this for special cases and tests
set xx = supergr_`date %y%m%d.%H%M`		# save executable
cp -p ~/bin/$MACHTYPE/$x $xx			# make a local copy to avoid killing jobs
dplace -x6 ./$xx < input.txt >>& output.txt	# run, with output appended to output.txt

