#! /bin/csh
#PBS -S /bin/csh -j oe -N bench
#PBS -l select=8:ncpus=8:mpiprocs=8,walltime=1:00:00

module load nas
module load comp/intel/11.0.081_64

# SGI mpi (fastest)
module load mpi/mpt.1.20
mpiexec -np 64 dplace -s1 -c0,1,4,5,2,3,6,7 ./benchmark_ifort_mpi.x

# INTEL mpi
#module load mpi-intel/3.1b
#mpiexec -np 64 dplace -c0,1,4,5,2,3,6,7 ./benchmark_ifort_mpi-intel_mpi.x

# MVAPICH mpi
#module load mpi/mvapich_intel
#mpiexec -np 64 dplace -c0,1,4,5,2,3,6,7 ./benchmark_ifort_mvapich_mpi.x
