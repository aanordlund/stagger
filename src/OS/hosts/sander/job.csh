#! /bin/csh
#PBS -l ncpus=16,cput=1000:00:00,mem=8gb -q parallel -S /bin/csh -j oe -o sander.out

cd $PBS_O_WORKDIR; umask 026; unlimit stack; limit core 0; limit
setenv OMP_DYNAMIC False; setenv _DSM_VERBOSE ON
setenv TRAP_FPE "ALL=COUNT; UNDERFL=FLUSH_ZERO; OVERFL=TRACE, ABORT(1); INVALID=TRACE, ABORT(1); DIVZERO=TRACE, ABORT(1)"

#MPI
mpirun -np $NCPUS ./padoan_mpi.x input.txt >& output.txt

## OMP
#setenv OMP_NUM_THREADS $NCPUS
#./padoan.x >& output.txt
