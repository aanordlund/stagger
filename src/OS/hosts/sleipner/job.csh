#!/bin/csh
# @ job_name = demo
# @ job_type = parallel
# @ total_tasks = 4
# @ class = qpar
# @ input = /dev/null
# @ output = $(job_name).$(jobid).o
# @ error =  $(job_name).$(jobid).e
# @ notification = never
# @ queue

# ------------------------------------------------------------------------------------
# Directory and environment setup
# ------------------------------------------------------------------------------------
umask 026					# allow group to read
unlimit stack					# max stack size
limit core 0					# no core dump
setenv OMP_DYNAMIC False			# fixed number of CPUs

# ------------------------------------------------------------------------------------
# CVS stuff.  This assumes that the run dir is in a CVS repository.  For each run the
# input file is appended to runs.txt and both files are then committed to the CVS repos
# ------------------------------------------------------------------------------------
if (! -e runs.txt) then				# if runs.txt does not exist
  ( echo ""; echo "===== $PBS_JOBID `date` =====" ; sed -e 's/Id://' input.txt ) >> runs.txt
  cvs add input.txt runs.txt			# add input and runs files
else						# or else
  cvs update runs.txt				# update runs.txt
  ( echo ""; echo "===== $PBS_JOBID `date` =====" ; sed -e 's/Id://' input.txt ) >> runs.txt
endif
cvs commit -m "$PBS_JOBID" input.txt runs.txt	# commit current versions

# ------------------------------------------------------------------------------------
# Execution.  This assumes that the $EXPERIMENT.x file has been installed ("make intall") to
# ../$MACHTYPE.x (to allow running under multiple architechtures).  The executable is saved
# to the run dir, to keep it with the data, and to allow the ../*.x file to be updated.
# ------------------------------------------------------------------------------------
cp ../$MACHTYPE.x ./				# copy the executable (to save with the data)
time ./$MACHTYPE.x < input.txt >>& output.txt	# run, with output appended to output.txt

