#!/bin/csh
# @ job_name = 200
# @ job_type = parallel
# @ total_tasks = 2
# @ class = qexp
# @ input = /dev/null
# @ output = log.$(jobid)
# @ error =  log.$(jobid)
# @ notification = never
# @ queue

set list = (  $LOADL_PROCESSOR_LIST )
setenv OMP_NUM_THREADS $#list

cat >input.txt <<EOF
&io      nstep=10 nsnap=50 file="snapshot.dat" /
&run     Cdt=0.50 /
&vars    do_pscalar=t do_energy=f do_mhd=f /
&grid    mx=100, my=100, mz=100, sx=1, sy=1, sz=1 /
&pde     do_loginterp=t do_2nddiv=f nu1=0.05 nu2=0.6 nur=0.1 gamma=1. /
&eqofst  /
&force   do_force=f /
&selfg   grav=45. isolated=t soft=3./
&init    w0=2. rho0=1.e3 eps2=3.0 pow=-2.0 pow2=6/
&wave    /
&bdry    /
&expl    do_expl=f /
&cool    do_cool=f /
EOF

( echo "======== `date` ========" ; cat input.txt ) >> runs.txt
time ./central.x < input.txt
