! $Id: pde_split.f90,v 1.20 2012/12/31 16:19:15 aake Exp $
!***********************************************************************
MODULE pde_mod

  USE params
  implicit none

  integer ix,iy,iz, j
  real gam1, tmp, tm(30), fCv
  
  real, allocatable, dimension(:,:,:) :: &
       dd,lnd,lnu,lnr,P,Cs,divU,nu,lne,ee,Ux,Uy,Uz, &
       Sxx,Syy,Szz,Sxy,Szx,Syz,xdnl,ydnl,zdnl,xdnr,ydnr,zdnr, &
       Txx,Tyy,Tzz,Txy,Tzx,Tyz,ddxd,ddyd,ddzd
  real, allocatable, dimension(:,:,:) :: &
    scr1, scr2, scr3, &
    xydnlnu, xzdnlnu, yzdnlnu, &
    TSxy, TSyz, TSzx, &
    xupUx, yupUy, zupUz, &
    xydnl, xdnUy, ydnUx, &
    yzdnl, ydnUz, zdnUy, &
    zxdnl, zdnUx, xdnUz, &
    ddxdnUy, ddzdnUy, &
    ddydnUz, ddxdnUz, &
    ddzdnUx, ddydnUx, &
    ddxdnTxx, ddyupTxy, ddzupTzx, &
    ddydnTyy, ddzupTyz, ddxupTxy, &
    ddzdnTzz, ddxupTzx, ddyupTyz, &
    eflux, &
    xdnlnu, xdnlne, ddxdnlne, &
    ydnlnu, ydnlne, ddydnlne, &
    zdnlnu, zdnlne, ddzdnlne, &
    ddxuppx, ddyuppy, ddzuppz
CONTAINS

  SUBROUTINE pde0(r,px,py,pz,e,d,drdt,dpxdt,dpydt,dpzdt,dedt,dddt,flag,Bx,By,Bz,dBxdt,dBydt,dBzdt)
  USE params
  implicit none
  real elapsed, average, fmaxval, f
  logical flag
  real, dimension(mx,my,mz) :: &
       r,px,py,pz,e,d,drdt,dpxdt,dpydt,dpzdt,dedt,dddt, &
       Bx,By,Bz,dBxdt,dBydt,dBzdt

  character (len=mid):: id
  data id/'$Id: pde_split.f90,v 1.20 2012/12/31 16:19:15 aake Exp $'/

  nstag=0; nstag1=0; nflop=0;

  if (id .ne. '') print *,id
  if (.not. do_trace) id = ''
  if (idbg>1) then; j=1;tm(j)=elapsed(); end if

!-----------------------------------------------------------------------
!  Velocities, pressure
!-----------------------------------------------------------------------
  allocate (lnr(mx,my,mz),xdnl(mx,my,mz),ydnl(mx,my,mz),zdnl(mx,my,mz)) !  4 chunks
  allocate (xdnr(mx,my,mz),ydnr(mx,my,mz),zdnr(mx,my,mz))               !  7 chunks
!$omp parallel do private(iz,iy,ix)
  do iz=1,mz
  do iy=1,my
  do ix=1,mx
    lnr(ix,iy,iz) = alog(r(ix,iy,iz))
  end do
  end do
  end do
  nflop = nflop+1
  call density_boundary_log(r,lnr)
  call xdn_set (lnr, xdnl)
  call ydn_set (lnr, ydnl)
  call zdn_set (lnr, zdnl)
  allocate (Ux(mx,my,mz),Uy(mx,my,mz),Uz(mx,my,mz))                     !  6 chunks        
!$omp parallel do private(iz,iy,ix)
  do iz=1,mz
  do iy=1,my
  do ix=1,mx
    xdnr(ix,iy,iz)=exp(xdnl(ix,iy,iz))
    ydnr(ix,iy,iz)=exp(ydnl(ix,iy,iz))
    zdnr(ix,iy,iz)=exp(zdnl(ix,iy,iz))
  end do
  end do
  end do
!$omp parallel do private(iz,iy,ix)
  do iz=1,mz
  do iy=1,my
  do ix=1,mx
    Ux(ix,iy,iz) = px(ix,iy,iz)/xdnr(ix,iy,iz)
    Uy(ix,iy,iz) = py(ix,iy,iz)/ydnr(ix,iy,iz)
    Uz(ix,iy,iz) = pz(ix,iy,iz)/zdnr(ix,iy,iz)
  end do
  end do
  end do
  nflop = nflop+6
  call velocity_boundary(r,xdnr,ydnr,zdnr,Ux,Uy,Uz,px,py,pz,e,Bx,By,Bz)
  if (idbg>1) then; j=j+1;tm(j)=elapsed(); print '(a4,2f7.3)',' ui',tm(j)-tm(j-1),tm(j)-tm(1); endif

  call forceit (r,Ux,Uy,Uz,xdnr,ydnr,zdnr,dpxdt,dpydt,dpzdt)
  if (idbg>1) then; j=j+1;tm(j)=elapsed(); print '(a4,2f7.3)','frc',tm(j)-tm(j-1),tm(j)-tm(1); endif
  deallocate (xdnr, ydnr, zdnr)

  allocate (Sxx(mx,my,mz),Syy(mx,my,mz),Szz(mx,my,mz))                  !  9 chunks
  call ddxup_set (Ux, Sxx)
  call ddyup_set (Uy, Syy)
  call ddzup_set (Uz, Szz)

!-----------------------------------------------------------------------
!  First part of viscosity
!-----------------------------------------------------------------------
  allocate (P(mx,my,mz),Cs(mx,my,mz),nu(mx,my,mz))                      ! 12 chunks
!$omp parallel do private(iz,iy,ix)
  do iz=1,mz
  do iy=1,my
  do ix=1,mx
    nu(ix,iy,iz) = Sxx(ix,iy,iz)+Syy(ix,iy,iz)+Szz(ix,iy,iz)
    nu(ix,iy,iz) = max(-nu(ix,iy,iz),0.)
  end do
  end do
  end do
  nflop = nflop+4
  if (gamma .eq. 1.) then
    gam1 = 1.
  else
    gam1 = gamma-1.
  end if
  allocate (scr1(mx,my,mz), scr2(mx,my,mz), scr3(mx,my,mz))
  call xup_set (Ux, scr1)
  call yup_set (Uy, scr2)
  call zup_set (Uz, scr3)
!$omp parallel do private(iz,iy,ix)
  do iz=1,mz
  do iy=1,my
  do ix=1,mx
    Cs(ix,iy,iz) = scr1(ix,iy,iz)**2 + scr2(ix,iy,iz)**2 + scr3(ix,iy,iz)**2
  end do
  end do
  end do
  nflop = nflop+5
  deallocate (scr1, scr2, scr3)
  if (idbg>1) then; j=j+1;tm(j)=elapsed(); print '(a4,2f7.3)','div',tm(j)-tm(j-1),tm(j)-tm(1); endif
  Urms = sqrt(average(Cs))
  if (idbg>1) then; j=j+1;tm(j)=elapsed(); print '(a4,2f7.3)','urm',tm(j)-tm(j-1),tm(j)-tm(1); endif

!-----------------------------------------------------------------------
!  Polytropic pressure, at given sound speed
!-----------------------------------------------------------------------
  allocate (ee(mx,my,mz))                                                !  6 chunks
  if (.not. do_energy) then
    f = (1./(gamma*gam1)*csound**2)
!$omp parallel do private(iz,iy,ix)
    do iz=1,mz
    do iy=1,my
    do ix=1,mx
      e(ix,iy,iz) = f*exp(alog(r(ix,iy,iz))*gamma)
    end do
    end do
    end do
    nflop = nflop+2
  end if
!-----------------------------------------------------------------------
!  Energy per unit mass
!-----------------------------------------------------------------------
!$omp parallel do private(iz,iy,ix)
  do iz=1,mz
  do iy=1,my
  do ix=1,mx
    ee(ix,iy,iz) = e(ix,iy,iz)/r(ix,iy,iz)
  end do
  end do
  end do
    nflop = nflop+1
!-----------------------------------------------------------------------
!  Boundary condition -- may need to calculate pressure internally
!-----------------------------------------------------------------------
  if (do_energy) call energy_boundary(r,lnr,Ux,Uy,Uz,e,ee,p,Bx,By,Bz)
!-----------------------------------------------------------------------
!  Pressure
!-----------------------------------------------------------------------
  if (do_ionization) then
    call pressure (r,ee,p)
  else
!$omp parallel do private(iz,iy,ix)
    do iz=1,mz
    do iy=1,my
    do ix=1,mx
      P(ix,iy,iz)  = gam1*e(ix,iy,iz)
    end do
    end do
    end do
    nflop = nflop+1
  end if

  if (idbg>1) then; j=j+1;tm(j)=elapsed(); print '(a4,2f7.3)','eos',tm(j)-tm(j-1),tm(j)-tm(1); endif

!-----------------------------------------------------------------------
!  Numerical viscosity, Richtmyer & Morton type.  For testing purposes,
!  nu1<0 signals the use of constant diffusivity.
!-----------------------------------------------------------------------
  allocate (scr1(mx,my,mz))
  if (nu1.lt.0.0) then
!$omp parallel do private(iz,iy,ix)
    do iz=1,mz
    do iy=1,my
    do ix=1,mx
      Cs(ix,iy,iz) = sqrt(gamma*P(ix,iy,iz)/r(ix,iy,iz)) + sqrt(Cs(ix,iy,iz))
      nu(ix,iy,iz) = abs(nu1)
    end do
    end do
    end do
    nflop = nflop+4+2*10
  else
!$omp parallel do private(iz,iy,ix), firstprivate(nu1,nu2,nu3)
    do iz=1,mz
    do iy=1,my
    do ix=1,mx
      scr1(ix,iy,iz) = sqrt(Cs(ix,iy,iz))                                               ! velocity
      Cs(ix,iy,iz) = sqrt(gamma*P(ix,iy,iz)/r(ix,iy,iz))                                ! sound speed
      nu(ix,iy,iz) = nu3*Cs(ix,iy,iz) +nu1*scr1(ix,iy,iz) + nu2*max(dxm(ix),dym(iy),dzm(iz))*nu(ix,iy,iz)       ! viscosity
      Cs(ix,iy,iz) = Cs(ix,iy,iz) + scr1(ix,iy,iz)                                      ! phase speed
    end do
    end do
    end do
    nflop = nflop+5+2*10
  end if

  allocate (lnu(mx,my,mz))
  if (lb.lt.6) call regularize(nu)

  !nu = smooth3(max3(nu))
  call max3_set (nu, scr1)
  call smooth3_set (scr1, nu)
  deallocate (scr1)

  call regularize(nu)
  call stats('nu',nu)
!$omp parallel do private(iz,iy,ix)
  do iz=1,mz
  do iy=1,my
  do ix=1,mx
    lnu(ix,iy,iz) = alog(nu(ix,iy,iz))
  end do
  end do
  end do
  nflop = nflop+1

  if (flag) then
!$omp parallel do private(iz,iy,ix)
    do iz=1,mz
    do iy=1,my
    do ix=1,mx
      Cs(ix,iy,iz) = Cs(ix,iy,iz)*(dt/min(dxm(ix),dym(iy),dzm(iz)))
    end do
    end do
    end do
    Cu = fmaxval('Cu',Cs)

    fCv = 2.*6.2*3.*dt/2.
!$omp parallel do private(iz,iy,ix)
    do iz=1,mz
    do iy=1,my
    do ix=1,mx
      Cs(ix,iy,iz) = nu(ix,iy,iz)*fCv/min(dxm(ix),dym(iy),dzm(iz))
    end do
    end do
    end do
    Cv = fmaxval('Cv',Cs)
  end if
  nflop = nflop+2
  deallocate (Cs)

!$omp parallel do private(iz,iy,ix)
  do iz=1,mz
  do iy=1,my
  do ix=1,mx
    lnu(ix,iy,iz) = lnu(ix,iy,iz)+lnr(ix,iy,iz)
    nu(ix,iy,iz) = nu(ix,iy,iz)*r(ix,iy,iz)
  end do
  end do
  end do
  nflop = nflop+2
  if (idbg>1) then; j=j+1;tm(j)=elapsed(); print '(a4,2f7.3)','dif',tm(j)-tm(j-1),tm(j)-tm(1); endif

  call stats ('nu', nu)

  END SUBROUTINE
  SUBROUTINE pde1(r,px,py,pz,e,d,drdt,dpxdt,dpydt,dpzdt,dedt,dddt,flag,Bx,By,Bz,dBxdt,dBydt,dBzdt)
  USE params
  implicit none
  real elapsed
  logical flag
  real, dimension(mx,my,mz) :: &
       r,px,py,pz,e,d,drdt,dpxdt,dpydt,dpzdt,dedt,dddt, &
       Bx,By,Bz,dBxdt,dBydt,dBzdt

!-----------------------------------------------------------------------
!  Mass conservation, cell centered
!-----------------------------------------------------------------------
if (do_density) then
  if (nur==0) then
    allocate (ddxuppx(mx,my,mz), ddyuppy(mx,my,mz), ddzuppz(mx,my,mz))
    if (do_2nddiv) then
      call ddxup1_set (px, ddxuppx)
      call ddyup1_set (py, ddyuppy)
      call ddzup1_set (pz, ddzuppz)
    else
      call ddxup_set (px, ddxuppx)
      call ddyup_set (py, ddyuppy)
      call ddzup_set (pz, ddzuppz)
    end if
!$omp parallel do private(iz,iy,ix)
    do iz=1,mz
    do iy=1,my
    do ix=1,mx
      drdt(ix,iy,iz) = drdt(ix,iy,iz) - ddxuppx(ix,iy,iz) - ddyuppy(ix,iy,iz) - ddzuppz(ix,iy,iz)
    end do
    end do
    end do
    nflop = nflop+3
    deallocate (ddxuppx, ddyuppy, ddzuppz)                                !  1 chunk
  else
    allocate (scr1(mx,my,mz), scr2(mx,my,mz))
!
    call ddxdn_set (lnr, scr1)
    call xdn_set (lnu, scr2)
!$omp parallel do private(iz,iy,ix)
    do iz=1,mz
    do iy=1,my
    do ix=1,mx
      scr1(ix,iy,iz) = -px(ix,iy,iz)+(nur*dxmdn(ix))*exp(scr2(ix,iy,iz))*scr1(ix,iy,iz)
    end do
    end do
    end do
    nflop = nflop+6
    if (do_2nddiv) then
      call ddxup1_add (scr1, drdt)
    else
      call ddxup_add (scr1, drdt)
    end if
!
    call ddydn_set (lnr, scr1)
    call ydn_set (lnu, scr2)
!$omp parallel do private(iz,iy,ix)
    do iz=1,mz
    do iy=1,my
    do ix=1,mx
      scr1(ix,iy,iz) = -py(ix,iy,iz)+(nur*dymdn(iy))*exp(scr2(ix,iy,iz))*scr1(ix,iy,iz)
    end do
    end do
    end do
    nflop = nflop+6
    if (do_2nddiv) then
      call ddyup1_add (scr1, drdt)
    else
      call ddyup_add (scr1, drdt)
    end if
!
    call ddzdn_set (lnr, scr1)
    call zdn_set (lnu, scr2)
!$omp parallel do private(iz,iy,ix)
    do iz=1,mz
    do iy=1,my
    do ix=1,mx
      scr1(ix,iy,iz) = -pz(ix,iy,iz)+(nur*dzmdn(iz))*exp(scr2(ix,iy,iz))*scr1(ix,iy,iz)
    end do
    end do
    end do
    nflop = nflop+6
    if (do_2nddiv) then
      call ddzup1_add (scr1, drdt)
    else
      call ddzup_add (scr1, drdt)
    end if
!
    deallocate (scr1, scr2)
  end if
end if
  deallocate (lnr)                                                      !  3 chunks
  if (idbg>1) then; j=j+1;tm(j)=elapsed(); print '(a4,2f7.3)','mas',tm(j)-tm(j-1),tm(j)-tm(1); endif

!-----------------------------------------------------------------------
!  Viscous stress, diagonal and off-diagonal elements
!-----------------------------------------------------------------------
  nstag=0
  allocate (Txx(mx,my,mz),Tyy(mx,my,mz),Tzz(mx,my,mz))                  ! 15 chunks
!$omp parallel do private(iz,iy,ix)
  do iz=1,mz
  do iy=1,my
    do ix=1,mx
      Txx(ix,iy,iz) = - (2.*dxm(ix)*nu(ix,iy,iz))*Sxx(ix,iy,iz) + P(ix,iy,iz)
      Tyy(ix,iy,iz) = - (2.*dym(iy)*nu(ix,iy,iz))*Syy(ix,iy,iz) + P(ix,iy,iz)
      Tzz(ix,iy,iz) = - (2.*dzm(iz)*nu(ix,iy,iz))*Szz(ix,iy,iz) + P(ix,iy,iz)
    end do
    if (do_energy) then
      if (do_dissipation) then 
        do ix=1,mx
          dedt(ix,iy,iz) = dedt(ix,iy,iz) &
                     - Txx(ix,iy,iz)*Sxx(ix,iy,iz) &
                     - Tyy(ix,iy,iz)*Syy(ix,iy,iz) &
                     - Tzz(ix,iy,iz)*Szz(ix,iy,iz)
        end do
      else
         do ix=1,mx
          dedt(ix,iy,iz) = dedt(ix,iy,iz) - P(ix,iy,iz)* &
                       (Sxx(ix,iy,iz) + Syy(ix,iy,iz) + Szz(ix,iy,iz))
        end do
      end if
    end if
  end do
  end do
  nflop = nflop+15
  deallocate (P,Sxx,Syy,Szz)                                            ! 10 chunks

  allocate (Sxy(mx,my,mz),Syz(mx,my,mz),Szx(mx,my,mz))                  ! 13 chunks
  allocate (ddxdnUy(mx,my,mz), ddzdnUy(mx,my,mz), ddydnUz(mx,my,mz))    ! 16 chunks
  allocate (ddxdnUz(mx,my,mz), ddzdnUx(mx,my,mz), ddydnUx(mx,my,mz))    ! 19 chunks
  call ddxdn_set(Uy,ddxdnUy)
  call ddzdn_set(Uy,ddzdnUy)
  call ddydn_set(Ux,ddydnUx)
  call ddzdn_set(Ux,ddzdnUx)
  call ddxdn_set(Uz,ddxdnUz)
  call ddydn_set(Uz,ddydnUz)
!$omp parallel do private(iz,iy,ix)
  do iz=1,mz
  do iy=1,my
  do ix=1,mx
    Sxy(ix,iy,iz) = (ddxdnUy(ix,iy,iz)+ddydnUx(ix,iy,iz))*0.5
    Syz(ix,iy,iz) = (ddydnUz(ix,iy,iz)+ddzdnUy(ix,iy,iz))*0.5
    Szx(ix,iy,iz) = (ddzdnUx(ix,iy,iz)+ddxdnUz(ix,iy,iz))*0.5
  end do
  end do
  end do
  nflop = nflop+6
  deallocate (ddxdnUy, ddzdnUy, ddydnUz, ddxdnUz, ddzdnUx, ddydnUx)     ! 13 chunks
  allocate (Txy(mx,my,mz),Tyz(mx,my,mz),Tzx(mx,my,mz))                  ! 16 chunks
  allocate (scr1(mx,my,mz), xydnlnu(mx,my,mz), xzdnlnu(mx,my,mz), yzdnlnu(mx,my,mz))
  call xdn_set (lnu,scr1)
  call ydn_set (scr1,xydnlnu)
  call zdn_set (scr1,xzdnlnu)
  call ydn_set (lnu,scr1)
  call zdn_set (scr1,yzdnlnu)
!$omp parallel do private(iz,iy,ix)
  do iz=1,mz
  do iy=1,my
  do ix=1,mx
    Txy(ix,iy,iz) = - (dxmdn(ix)+dymdn(iy))*exp(xydnlnu(ix,iy,iz))*Sxy(ix,iy,iz)
    Tyz(ix,iy,iz) = - (dymdn(iy)+dzmdn(iz))*exp(yzdnlnu(ix,iy,iz))*Syz(ix,iy,iz)
    Tzx(ix,iy,iz) = - (dzmdn(iz)+dxmdn(ix))*exp(xzdnlnu(ix,iy,iz))*Szx(ix,iy,iz)
  end do
  end do
  end do
  nflop = nflop+9
  deallocate (xydnlnu, xzdnlnu, yzdnlnu)

  if (do_energy .and. do_dissipation) then
    call xup_set (Sxy, scr1)
    call yup_set (scr1, Sxy)
    call yup_set (Syz, scr1)
    call zup_set (scr1, Syz)
    call zup_set (Szx, scr1)
    call xup_set (scr1, Szx)

!$omp parallel do private(iz,iy,ix)
    do iz=1,mz
    do iy=1,my
    do ix=1,mx
      dedt(ix,iy,iz) = dedt(ix,iy,iz) + 2.*( &
        (dxm(ix)+dym(iy))*nu(ix,iy,iz)*Sxy(ix,iy,iz)**2 &
      + (dym(iy)+dzm(iz))*nu(ix,iy,iz)*Syz(ix,iy,iz)**2 &
      + (dzm(iz)+dxm(ix))*nu(ix,iy,iz)*Szx(ix,iy,iz)**2)
    end do
    end do
    end do
    nflop = nflop+6
  end if
  deallocate (scr1)
  deallocate (Sxy,Syz,Szx)                                              ! 13 chunks
  if (idbg>1) then; j=j+1;tm(j)=elapsed(); print '(a4,2f7.3)','vst',tm(j)-tm(j-1),tm(j)-tm(1); endif

  END SUBROUTINE
  SUBROUTINE pde2(r,px,py,pz,e,d,drdt,dpxdt,dpydt,dpzdt,dedt,dddt,flag,Bx,By,Bz,dBxdt,dBydt,dBzdt)
  USE params
  implicit none
  real elapsed
  logical flag
  real, dimension(mx,my,mz) :: &
       r,px,py,pz,e,d,drdt,dpxdt,dpydt,dpzdt,dedt,dddt, &
       Bx,By,Bz,dBxdt,dBydt,dBzdt

!-----------------------------------------------------------------------
!  Add Reynolds stress
!-----------------------------------------------------------------------
  allocate (xupUx(mx,my,mz), yupUy(mx,my,mz), zupUz(mx,my,mz))
  call xup_set(Ux,xupUx)
  call yup_set(Uy,yupUy)
  call zup_set(Uz,zupUz)
!$omp parallel do private(iz,iy,ix)
  do iz=1,mz
  do iy=1,my
  do ix=1,mx
    Txx(ix,iy,iz) = Txx(ix,iy,iz) + r(ix,iy,iz)*xupUx(ix,iy,iz)**2
    Tyy(ix,iy,iz) = Tyy(ix,iy,iz) + r(ix,iy,iz)*yupUy(ix,iy,iz)**2
    Tzz(ix,iy,iz) = Tzz(ix,iy,iz) + r(ix,iy,iz)*zupUz(ix,iy,iz)**2
  end do
  end do
  end do
  nflop = nflop+9
  deallocate (xupUx, yupUy, zupUz)

  allocate (xydnl(mx,my,mz), xdnUy(mx,my,mz), ydnUx(mx,my,mz))
  call xdn_set (ydnl, xydnl)
  call xdn_set(Uy, xdnUy)
  call ydn_set(Ux, ydnUx)
!$omp parallel do private(iz,iy,ix)
  do iz=1,mz
  do iy=1,my
  do ix=1,mx
    Txy(ix,iy,iz) = Txy(ix,iy,iz) + exp(xydnl(ix,iy,iz))*ydnUx(ix,iy,iz)*xdnUy(ix,iy,iz)
  end do
  end do
  end do
  nflop = nflop+4
  deallocate (xydnl, xdnUy, ydnUx)

  allocate (yzdnl(mx,my,mz), ydnUz(mx,my,mz), zdnUy(mx,my,mz))
  call ydn_set (zdnl, yzdnl)
  call ydn_set(Uz, ydnUz)
  call zdn_set(Uy, zdnUy)
!$omp parallel do private(iz,iy,ix)
  do iz=1,mz
  do iy=1,my
  do ix=1,mx
    Tyz(ix,iy,iz) = Tyz(ix,iy,iz) + exp(yzdnl(ix,iy,iz))*zdnUy(ix,iy,iz)*ydnUz(ix,iy,iz)
  end do
  end do
  end do
  nflop = nflop+4
  deallocate (yzdnl, ydnUz, zdnUy)

  allocate (zxdnl(mx,my,mz), zdnUx(mx,my,mz), xdnUz(mx,my,mz))
  call zdn_set (xdnl, zxdnl)
  call zdn_set(Ux, zdnUx)
  call xdn_set(Uz, xdnUz)
!$omp parallel do private(iz,iy,ix)
  do iz=1,mz
  do iy=1,my
  do ix=1,mx
    Tzx(ix,iy,iz) = Tzx(ix,iy,iz) + exp(zxdnl(ix,iy,iz))*xdnUz(ix,iy,iz)*zdnUx(ix,iy,iz)
  end do
  end do
  end do
  nflop = nflop+4
  deallocate (zxdnl,zdnUx,xdnUz)
  deallocate (xdnl,ydnl,zdnl)                                           ! 10 chunks
  if (idbg>1) then; j=j+1;tm(j)=elapsed(); print '(a4,2f7.3)','rst',tm(j)-tm(j-1),tm(j)-tm(1); endif

!-----------------------------------------------------------------------
!  Equations of motion, face centred
!-----------------------------------------------------------------------
  allocate (ddxdnTxx(mx,my,mz), ddyupTxy(mx,my,mz), ddzupTzx(mx,my,mz))
  if (do_2nddiv) then
    call ddxdn1_set (Txx, ddxdnTxx)
    call ddyup1_set (Txy, ddyupTxy)
    call ddzup1_set (Tzx, ddzupTzx)
  else
    call ddxdn_set (Txx, ddxdnTxx)
    call ddyup_set (Txy, ddyupTxy)
    call ddzup_set (Tzx, ddzupTzx)
  end if
!$omp parallel do private(iz,iy,ix)
  do iz=1,mz
  do iy=1,my
  do ix=1,mx
    dpxdt(ix,iy,iz) = dpxdt(ix,iy,iz) - ddxdnTxx(ix,iy,iz) - ddyupTxy(ix,iy,iz) - ddzupTzx(ix,iy,iz)
  end do
  end do
  end do
  nflop = nflop+3
  deallocate (ddxdnTxx, ddyupTxy, ddzupTzx)

  allocate (ddydnTyy(mx,my,mz), ddzupTyz(mx,my,mz), ddxupTxy(mx,my,mz))
  if (do_2nddiv) then
    call ddydn1_set (Tyy, ddydnTyy)
    call ddzup1_set (Tyz, ddzupTyz)
    call ddxup1_set (Txy, ddxupTxy)
  else
    call ddydn_set (Tyy, ddydnTyy)
    call ddzup_set (Tyz, ddzupTyz)
    call ddxup_set (Txy, ddxupTxy)
  end if
!$omp parallel do private(iz,iy,ix)
  do iz=1,mz
  do iy=1,my
  do ix=1,mx
    dpydt(ix,iy,iz) = dpydt(ix,iy,iz) - ddydnTyy(ix,iy,iz) - ddzupTyz(ix,iy,iz) - ddxupTxy(ix,iy,iz)
  end do
  end do
  end do
  nflop = nflop+3
  deallocate (ddydnTyy, ddzupTyz, ddxupTxy)

  allocate (ddzdnTzz(mx,my,mz), ddxupTzx(mx,my,mz), ddyupTyz(mx,my,mz))
  if (do_2nddiv) then
    call ddzdn1_set (Tzz, ddzdnTzz)
    call ddxup1_set (Tzx, ddxupTzx)
    call ddyup1_set (Tyz, ddyupTyz)
  else
    call ddzdn_set (Tzz, ddzdnTzz)
    call ddxup_set (Tzx, ddxupTzx)
    call ddyup_set (Tyz, ddyupTyz)
  end if
!$omp parallel do private(iz,iy,ix)
  do iz=1,mz
  do iy=1,my
  do ix=1,mx
    dpzdt(ix,iy,iz) = dpzdt(ix,iy,iz) - ddzdnTzz(ix,iy,iz) - ddxupTzx(ix,iy,iz) - ddyupTyz(ix,iy,iz)
  end do
  end do
  end do
  nflop = nflop+3
  deallocate (ddzdnTzz, ddxupTzx, ddyupTyz)
  deallocate (Txy,Tyz,Tzx,Txx,Tyy,Tzz)                                  !  4 chunks
  if (idbg>1) then; j=j+1;tm(j)=elapsed(); print '(a4,2f7.3,i4)','eom',tm(j)-tm(j-1),tm(j)-tm(1),nstag; endif

  END SUBROUTINE
  SUBROUTINE pde3(r,px,py,pz,e,d,drdt,dpxdt,dpydt,dpzdt,dedt,dddt,flag,Bx,By,Bz,dBxdt,dBydt,dBzdt)
  USE params
  ! USE cooling
  implicit none
  real elapsed
  logical flag
  real, dimension(mx,my,mz) :: &
       r,px,py,pz,e,d,drdt,dpxdt,dpydt,dpzdt,dedt,dddt, &
       Bx,By,Bz,dBxdt,dBydt,dBzdt

!-----------------------------------------------------------------------
!  Abundance per unit mass
!-----------------------------------------------------------------------
  if (do_pscalar .or. do_cool) then
    allocate (dd(mx,my,mz))
    if (do_pscalar) then
!$omp parallel do private(iz,iy,ix)
      do iz=1,mz
      do iy=1,my
      do ix=1,mx
        dd(ix,iy,iz)=d(ix,iy,iz)/r(ix,iy,iz)
      end do
      end do
      end do
    end if
    nflop = nflop+1
  end if

!-----------------------------------------------------------------------
!  Passive scalar
!-----------------------------------------------------------------------
  call passive(nu,lnu,r,px,py,pz,Ux,Uy,Uz,dd,dddt)
  call trace_particles (Ux,Uy,Uz)
  deallocate (nu)
  if (idbg>1) then; j=j+1;tm(j)=elapsed(); print '(a4,2f7.3)','pas',tm(j)-tm(j-1),tm(j)-tm(1); endif

!-----------------------------------------------------------------------
!  Energy equation
!-----------------------------------------------------------------------
  allocate (lne(mx,my,mz))                                              !  6 chunks
!!  lnu = lnu+alog(2.)
!$omp parallel do private(iz,iy,ix)
  do iz=1,mz
  do iy=1,my
  do ix=1,mx
    lne(ix,iy,iz) = alog(ee(ix,iy,iz))
    lnu(ix,iy,iz) = lnu(ix,iy,iz)+lne(ix,iy,iz)
  end do
  end do
  end do
  nflop = nflop+2
  if (idbg>1) then; j=j+1;tm(j)=elapsed(); print '(a4,2f7.3)','lne',tm(j)-tm(j-1),tm(j)-tm(1); endif

!-----------------------------------------------------------------------
!  MHD
!-----------------------------------------------------------------------
  if (do_mhd) then
    call mhd(r,ee,Ux,Uy,Uz,Bx,By,Bz,dpxdt,dpydt,dpzdt,dedt,dBxdt,dBydt,dBzdt,flag)
  if (idbg>1) then; j=j+1;tm(j)=elapsed(); print '(a4,2f7.3)','mhd',tm(j)-tm(j-1),tm(j)-tm(1); endif
  end if
  deallocate (Ux,Uy,Uz)                                                 !  7 chunks

if (do_energy) then

!-----------------------------------------------------------------------
!  Energy advection and diffusion
!-----------------------------------------------------------------------
  allocate (eflux(mx,my,mz), xdnlnu(mx,my,mz), xdnlne(mx,my,mz), ddxdnlne(mx,my,mz))
  call xdn_set (lnu, xdnlnu)
  call xdn_set (lne, xdnlne)
  call ddxdn_set (lne, ddxdnlne)
!$omp parallel do private(iz,iy,ix)
  do iz=1,mz
  do iy=1,my
  do ix=1,mx
    eflux(ix,iy,iz) = exp(xdnlne(ix,iy,iz))*px(ix,iy,iz) - dxmdn(ix)*exp(xdnlnu(ix,iy,iz))*ddxdnlne(ix,iy,iz)
  end do
  end do
  end do
  nflop = nflop+6
  deallocate (xdnlnu, xdnlne, ddxdnlne)

  allocate (scr1(mx,my,mz))
  if (do_2nddiv) then
    call ddxup1_set (eflux, scr1)
  else
    call ddxup_set (eflux, scr1)
  end if
!$omp parallel do private(iz,iy,ix)
  do iz=1,mz
  do iy=1,my
  do ix=1,mx
    dedt(ix,iy,iz) = dedt(ix,iy,iz) - scr1(ix,iy,iz)
  end do
  end do
  end do
  nflop = nflop+1

  allocate (ydnlnu(mx,my,mz), ydnlne(mx,my,mz), ddydnlne(mx,my,mz))
  call ydn_set (lnu, ydnlnu)
  call ydn_set (lne, ydnlne)
  call ddydn_set (lne, ddydnlne)
!$omp parallel do private(iz,iy,ix)
  do iz=1,mz
  do iy=1,my
  do ix=1,mx
    eflux(ix,iy,iz) = exp(ydnlne(ix,iy,iz))*py(ix,iy,iz) - dymdn(iy)*exp(ydnlnu(ix,iy,iz))*ddydnlne(ix,iy,iz)
  end do
  end do
  end do
  nflop = nflop+6
  deallocate (ydnlnu, ydnlne, ddydnlne)

  if (do_2nddiv) then
    call ddyup1_set (eflux, scr1)
  else
    call ddyup_set (eflux, scr1)
  end if
!$omp parallel do private(iz,iy,ix)
  do iz=1,mz
  do iy=1,my
  do ix=1,mx
    dedt(ix,iy,iz) = dedt(ix,iy,iz) - scr1(ix,iy,iz)
  end do
  end do
  end do
  nflop = nflop+1

  allocate (zdnlnu(mx,my,mz), zdnlne(mx,my,mz), ddzdnlne(mx,my,mz))
  call zdn_set (lnu, zdnlnu)
  call zdn_set (lne, zdnlne)
  call ddzdn_set (lne, ddzdnlne)
!$omp parallel do private(iz,iy,ix)
  do iz=1,mz
  do iy=1,my
  do ix=1,mx
    eflux(ix,iy,iz) = exp(zdnlne(ix,iy,iz))*pz(ix,iy,iz) - dzmdn(iz)*exp(zdnlnu(ix,iy,iz))*ddzdnlne(ix,iy,iz)
  end do
  end do
  end do
  nflop = nflop+6
  deallocate (zdnlnu, zdnlne, ddzdnlne)

  if (do_2nddiv) then
    call ddzup1_set (eflux, scr1)
  else
    call ddzup_set (eflux, scr1)
  end if
!$omp parallel do private(iz,iy,ix)
  do iz=1,mz
  do iy=1,my
  do ix=1,mx
    dedt(ix,iy,iz) = dedt(ix,iy,iz) - scr1(ix,iy,iz)
  end do
  end do
  end do
  nflop = nflop+1
  deallocate (eflux,scr1)

!-----------------------------------------------------------------------
!  Cooling and conduction
!-----------------------------------------------------------------------
  if (do_cool) then
    call coolit (r,ee,lne,dd,dedt)
    call conduction (r,e,ee,Bx,By,Bz,dedt)
  end if
  if (idbg>1) then; j=j+1;tm(j)=elapsed(); print '(a4,2f7.3)','coo',tm(j)-tm(j-1),tm(j)-tm(1); endif

end if
  deallocate (ee,lne)                                                   !  6 chunks
  if (idbg>1) then; j=j+1;tm(j)=elapsed(); print '(a4,2f7.3)','eeq',tm(j)-tm(j-1),tm(j)-tm(1); endif

  if (do_pscalar .or. do_cool) deallocate(dd)
  deallocate (lnu)                                                      !  4 chunks

  call ddt_boundary (r,px,py,pz,e,Bx,By,Bz, &
                     drdt,dpxdt,dpydt,dpzdt, &
                     dedt,dBxdt,dBydt,dBzdt)

  END SUBROUTINE
END MODULE

  SUBROUTINE pde(r,px,py,pz,e,d,drdt,dpxdt,dpydt,dpzdt,dedt,dddt,flag,Bx,By,Bz,dBxdt,dBydt,dBzdt)
  USE params
  USE pde_mod
  implicit none
  logical flag
  real, dimension(mx,my,mz) :: &
       r,px,py,pz,e,d,drdt,dpxdt,dpydt,dpzdt,dedt,dddt, &
       Bx,By,Bz,dBxdt,dBydt,dBzdt

  if (.not. do_loginterp) then
    call pde_lin(r,px,py,pz,e,d,drdt,dpxdt,dpydt,dpzdt,dedt,dddt,flag, &
                 Bx,By,Bz,dBxdt,dBydt,dBzdt)
    return
  end if

  call pde0(r,px,py,pz,e,d,drdt,dpxdt,dpydt,dpzdt,dedt,dddt,flag,Bx,By,Bz,dBxdt,dBydt,dBzdt)
  call pde1(r,px,py,pz,e,d,drdt,dpxdt,dpydt,dpzdt,dedt,dddt,flag,Bx,By,Bz,dBxdt,dBydt,dBzdt)
  call pde2(r,px,py,pz,e,d,drdt,dpxdt,dpydt,dpzdt,dedt,dddt,flag,Bx,By,Bz,dBxdt,dBydt,dBzdt)
  call pde3(r,px,py,pz,e,d,drdt,dpxdt,dpydt,dpzdt,dedt,dddt,flag,Bx,By,Bz,dBxdt,dBydt,dBzdt)
  END SUBROUTINE
