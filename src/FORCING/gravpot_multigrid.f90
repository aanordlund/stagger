! $Id: gravpot_multigrid.f90,v 1.2 2006/04/05 16:16:23 aake Exp $
!**********************************************************************
MODULE selfgravity
  real grav, soft
  logical isolated
  real max_error, fft_error, error, filter(3), sor, rhav, resav, a_fft, a_dom
  logical verbose, debug, do_domain, do_fft, do_filter
  integer min_iter, max_iter, nlap
END MODULE

MODULE selfgravity_arrays
  real, allocatable, dimension(:,:,:):: phi, rho, phi0
END MODULE

!***********************************************************************
SUBROUTINE init_selfg
  USE params
  USE selfgravity
  USE selfgravity_arrays
  implicit none
  integer iz
  character(len=mid), save:: id="init_laplace: $Id: gravpot_multigrid.f90,v 1.2 2006/04/05 16:16:23 aake Exp $"

  call print_trace (id, dbg_force, 'BEGIN')

  grav = 0.
  soft = 2.
  isolated = .false.
  max_error = 1.0e-3
  fft_error = 1.5e-3
  min_iter = 1
  max_iter = 200
  filter = (/0.200,0.05,0.00/)
  a_fft = 8.
  a_dom = 1.
  nlap = 10
  sor = 1.0
  debug = .false.
  do_domain = .true.
  do_fft = .true.
  do_filter = .true.
  call read_selfg
  verbose = debug
  call init_multigrid (filter, min_iter, max_iter, max_error)

  allocate (phi(mx,my,mz))                                              ! need to keep this from step to step
  !$omp parallel
  do iz=izs,ize
    phi(:,:,iz) = 0.
  end do
  !$omp end parallel

  call test_gravpot
END SUBROUTINE

!***********************************************************************
SUBROUTINE read_selfg
  USE params
  USE selfgravity
  implicit none
  namelist /selfg/grav,soft,isolated,max_error,fft_error,max_iter,min_iter, &
    filter,sor,debug,nlap,a_fft,a_dom,do_fft,do_domain,do_filter

  rewind (stdin); read (stdin,selfg)
  if (master) write (*,selfg)
END SUBROUTINE

!***********************************************************************
SUBROUTINE selfgrav (r,xdnr,ydnr,zdnr,dpxdt,dpydt,dpzdt)
  USE params
  USE selfgravity, void => debug
  implicit none
  real, dimension(mx,my,mz):: r,xdnr,ydnr,zdnr,dpxdt,dpydt,dpzdt
  logical omp_in_parallel, debug

  if (debug(dbg_force)) print *,'selfgrav:',grav,omp_in_parallel(),omp_mythread
  if (grav == 0.) return
  if (omp_in_parallel()) then
    call selfgrav_omp (r,xdnr,ydnr,zdnr,dpxdt,dpydt,dpzdt)
  else
    !$omp parallel
    call selfgrav_omp (r,xdnr,ydnr,zdnr,dpxdt,dpydt,dpzdt)
    !$omp end parallel
  end if

END SUBROUTINE

!***********************************************************************
SUBROUTINE selfgrav_omp (r,xdnr,ydnr,zdnr,dpxdt,dpydt,dpzdt)
  USE params
  USE arrays, only: scr5, scr6
  USE selfgravity, only: grav
  USE selfgravity_arrays, only: phi
  implicit none
  real, dimension(mx,my,mz):: r,xdnr,ydnr,zdnr,dpxdt,dpydt,dpzdt
  integer iz
  character(len=mid):: id="$Id: gravpot_multigrid.f90,v 1.2 2006/04/05 16:16:23 aake Exp $"
  call print_trace (id, dbg_force, 'BEGIN')

!---------------------------------------------------------------------
!Gravity added RJ 8/2003
!---------------------------------------------------------------------  

  call poisson_multigrid (grav, dx, dy, dz, mx, my, mz, r, phi)
  call ddxdn_set (phi, scratch)
  do iz=izs,ize
    dpxdt(:,:,iz) = dpxdt(:,:,iz)-xdnr(:,:,iz)*scratch(:,:,iz)
  end do
  call ddydn_set (phi, scratch)
  do iz=izs,ize
    dpydt(:,:,iz) = dpydt(:,:,iz)-ydnr(:,:,iz)*scratch(:,:,iz)
  end do
  call ddzdn_set (phi, scratch)
  do iz=izs,ize
    dpzdt(:,:,iz) = dpzdt(:,:,iz)-zdnr(:,:,iz)*scratch(:,:,iz)
  end do

  call print_trace (id, dbg_force, 'END')
END SUBROUTINE

!***********************************************************************
SUBROUTINE test_gravpot
  USE params
  USE arrays, only: scr5, scr6
  USE selfgravity
  USE selfgravity_arrays
  implicit none
  real kx, ky, kz
  integer ix, iy, iz

  kx = 2.*pi/sx
  ky = 2.*pi/sy
  kz = 2.*pi/sz
  print *,'kx',kx

  allocate (rho(mx,my,mz), phi0(mx,my,mz))

  !$omp parallel
  do iz=izs,ize
  do iy=1,my
  do ix=1,mx
    rho(ix,iy,iz) = 1.+0.5*sin(kx*xm(ix))*sin(ky*ym(iy))*sin(kz*zm(iz))
  end do
  end do
  end do
  !$omp end parallel

  if (verbose) then
    rho(6,my/4+1,mz/4+1) = 10.
    print '(i3,1x,a,20f7.3)', mpi_rank, '  xm',  xm(1:20)
    print '(i3,1x,a,20f7.3)', mpi_rank, '  ph',  xm(1:20)*kx
    print '(i3,1x,a,20f7.3)', mpi_rank, ' rho', rho(1:20,my/4+1,mz/4+1)
  end if

  !$omp parallel
  call poisson_multigrid (grav, dx, dy, dz, mx, my, mz, rho, phi)
  !$omp end parallel

  deallocate (rho, phi0)
END
