! $Id: gravpot_star.f90,v 1.5 2012/12/31 16:19:12 aake Exp $
!***********************************************************************
FUNCTION perif(f)
  USE params
  real, dimension(mx,my,mz):: f
  perif = sum(f(1,:,:)) &
        + sum(f(:,1,:)) &
        + sum(f(:,:,1)) &
        + sum(f(mx,:,:)) &
        + sum(f(:,my,:)) &
        + sum(f(:,:,mz)) &
        - sum(f(1,1,:)) &
        - sum(f(1,:,1)) &
        - sum(f(:,1,1)) &
        - sum(f(mx,my,:)) &
        - sum(f(mx,:,mz)) &
        - sum(f(:,my,mz))
  do iz=1,mz,mz-1
  do iy=1,my,my-1
  do ix=1,mx,mx-1
    perif = perif + f(ix,iy,iz)
  end do
  end do
  end do
END

SUBROUTINE gravpot (rho, phi)

!  Solve for the gravitational potential phi, given the density rho,
!  from the equation Laplace(phi) = grav*rho.

  use params
  use selfgravity, void=>phi
  use forcing
  implicit none

  real, dimension(mx,my,mz):: rho, phi
!hpf$ align with TheCube:: rho, phi
  real, allocatable, dimension(:,:,:):: green, k2, scr, scr1
!hpf$ align with TheCube:: green, k2, scr, scr1

  real k2norm,rhoa,x,y,z,r,phi0
  integer ix,iy,iz

  character(len=mid):: id = "$Id: gravpot_star.f90,v 1.5 2012/12/31 16:19:12 aake Exp $"
!-----------------------------------------------------------------------

  if (id .ne. '') print *,id                      ! identify
  id = ''

  if (.not. isolated) then                        ! periodic case
    call fft3df (rho, phi, mx, my, mz)            ! forward FFT
    phi0 = 0.                                     ! zero point

  else                                            ! isolated case
    allocate (scr(mx,my,mz))

!$omp parallel do private(ix,iy,iz,x,y,z,r)
    do iz=1,mz
      z = (iz-1-mz/2)
      do iy=1,my
        y = (iy-1-my/2)
        do ix=1,mx
          x = (ix-1-mx/2)
          r = dx*sqrt(x**2+y**2+z**2)
          phi(ix,iy,iz) = -m_sun/(4.*pi*max(r,dx*1e-4))
        end do
      end do
    end do
    phi0 = phi(1,1,1)                             ! zero point

!  Set a negative mass density in the boundary zones (ix=iy=iz=1), to
!  provide the BC corresponding to the potential of an isolated central mass.

    allocate (scr1(mx,my,mz))
    call ddxdn1_set (phi,scr1)                    ! corresponding to
    call ddxup1_set (scr1,scr)                    ! negative mass at
    call ddydn1_set (phi,scr1)                    ! the boundaries
    call ddyup1_add (scr1,scr)
    call ddzdn1_set (phi,scr1)
    call ddzup1_add (scr1,scr)
    deallocate (scr1)

!  In principle, the normalization below, which says that the total mass
!  in the box should vanish, for minimum influence from neighboring boxes,
!  does NOT improve the result.  On the contrary, equipotential surfaces
!  are more circular if the normalization is not carried out.
!    scr(2:mx,2:my,2:mz) = 0.
!    scr = -scr*sum(rho(2:mx,2:my,2:mz))/sum(scr)
!    print *,'sum(scr)=',sum(scr)

!$omp parallel do private(iz)
    do iz=2,mz
      scr(2:mx,2:my,iz) = rho(2:mx,2:my,iz)
    end do
!    print *,'sum(scr)/sum(rho)=',sum(scr)/sum(rho)

    call fft3df (scr, phi, mx, my, mz)            ! forward FFT
    deallocate (scr)
  end if

  allocate (green(mx,my,mz),k2(mx,my,mz))
  call fft3d_k2 (k2, dx, dy, dz, mx, my, mz)      ! compute k**2
  k2norm = 2.*(soft*(sx/mx)/pi)**2                ! normalization factor

! In wavenumber space the transform is suppressed with exp(-k^2/(2.*k_0^2)), 
! where k0 = (\pi/ds).  The softening profile in real space is thus
! exp(-(x-x0)^2/(2.*(2.*ds)^2), with a 1/e width of 2\sqrt(2) grid zones.

!$omp parallel do private(iz)
  do iz=1,mz
    green(:,:,iz) = -grav/(k2(:,:,iz)+1e-10)*exp(-k2norm*k2(:,:,iz))        ! softening
  end do
  green(1,1,1) = 0.0                              ! no mean phi
  deallocate (k2)

!$omp parallel do private(iz)
  do iz=1,mz
    phi(:,:,iz) = green(:,:,iz)*phi(:,:,iz)       ! Green's function
  end do
  call fft3db (phi, phi, mx, my, mz)              ! reverse FFT
!$omp parallel do private(iz)
  do iz=1,mz
    phi(:,:,iz) = phi(:,:,iz) + (phi0-phi(1,1,1)) ! appply zero point
  end do
  deallocate (green)
END
