MODULE forcing
  real grav, soft
  logical, parameter:: conservative=.true.
END MODULE

SUBROUTINE init_force
  use params
  use forcing
  grav = 1.
  soft = 3.
  call read_force
END subroutine

SUBROUTINE read_force
  use params
  use forcing
  namelist /force/grav,soft
  write (*,force)
  read  (*,force)
  write (*,force)
END subroutine

SUBROUTINE forceit (r,Ux,Uy,Uz,xdnr,ydnr,zdnr,dpxdt,dpydt,dpzdt)
  USE params
  USE stagger
  use forcing
  implicit none

  real, dimension(mx,my,mz):: r,Ux,Uy,Uz,xdnr,ydnr,zdnr,dpxdt,dpydt,dpzdt
  integer ix,iy,iz
  real y,y0

  y0 = (my+1.)/2.
  do iz=1,mz
  do iy=1,my
    y = dy*iy
    do ix=1,mx
      dpydt(ix,iy,iz) = dpydt(ix,iy,iz) - grav*sin(2.*pi*(y-y0)/my)*ydnr(ix,iy,iz)
    end do
  end do
  end do
END
