!***********************************************************************
MODULE stellarwind

!-----------------------------------------------------------------------
!The module (along with stellar_wind subroutine) are used to calculate 
!the amount of energy and mass input to the ISM for all the massive stars
!(which are stored in hms) during 2 consecutive time steps.
!-----------------------------------------------------------------------


!REMEMBER:
!hms(i,14)=lifetime,
!hms(i,8)=x-component of star velocity, 
!hms(i,9)=y-component of star velocity,
!hms(i,10)=z-component of star velocity,
!hmspos(i,1)=x-component of star final position in the grid,
!hmspos(i,2)=y-component of star final position in the grid,
!hmspos(i,3)=z-component of star final position in the grid

!Common block for stellar wind quantities

implicit none

integer phase1row, phase1column,phase2row, phase2column, &
        vinfph1row,vinfph1column, vinfph2row, vinfph2column

logical do_wind

character (len=80) phase1file,phase2file, vinfph1file, vinfph2file
real pwind, wwind, windtemp,ww2,errw

namelist /wind/ do_wind, phase1row, phase1column,phase2row, phase2column, &
                vinfph1row,vinfph1column, vinfph2row, vinfph2column, &
                phase1file,phase2file, vinfph1file, vinfph2file,wwind, &
                windtemp,ww2,errw
    
real, allocatable, dimension(:,:) :: phase1tbl,phase2tbl,vinfph1tbl, &
                                     vinfph2tbl

real, public:: bbnsII

CONTAINS


!***********************************************************************
SUBROUTINE calculate_lostmass(i,Rti,Mi,phase,lostmass,Rmi,Mi2)

!-----------------------------------------------------------------------
!Calculate the amount of lost mass of the star placed in the 'i' position
!in hms in this time interval
!-----------------------------------------------------------------------

USE explosions

integer i,pos1,pos2

real Rti,Mi,Rmi,Mi2, lostmass
!double precision lostmass

logical phase

pos1=int(2*hms(i,13)-1)
pos2=int(2*(hms(i,13)+1)-1)

  if (phase) then
! print *,'holalostmass',hms(i,2)     
     call interpolation(i,Rti,phase1tbl,phase1row,phase1column,1, &
                       pos1,pos2,Rmi)
! print *,'Adiolostmass',hms(i,2)

     Mi2=Rmi*hms(i,17)        !total lost Mass (in solar masses) AFTER subr.
                              !in first phase 
     lostmass=Mi2-Mi          !Lost mass (in M_o) in this time interval
     hms(i,15)=hms(i,2)-Mi2   !stellar mass (in M_o) AFTER the subroutine.
  else
     call interpolation(i,Rti,phase2tbl,phase2row,phase2column,1, &
                       pos1,pos2,Rmi)

     Mi2=Rmi*(hms(i,12)-hms(i,17))  !Total lost mass AFTER subrutina
                                   !in second phase .
     lostmass=hms(i,17)+Mi2-Mi     !Lost mass in this interval of time
     hms(i,15)=hms(i,2)-(hms(i,17)+Mi2) !stellar mass AFTER the Subroutine
  end if

END subroutine


!*********************************************************************************
SUBROUTINE calculate_Vinf(i,Rti,phase,Vinf)

!---------------------------------------------------------------------------------
!Calculate Vinf of the star placed in the position 'i' at time t.
!This is done taking care of which stage of the evolution the star is.
!To simplify we consider 3 stages:
!First phase means O,B stars whereas second one refers to Red Giant or WR stage
!(depending on the initial mass)  

!i)For O and B stars, and for Red Giant stars Vinf is calculated as follows:
!First Vesc is found and after that it is multiply by a factor (which depends on T in the 
!case of O,B stars) to obtain Vinf.
!References for O,B stars -> "Intr. to Stellar winds", Lamers & Cassinelli 1999 or 
!"Winds from Hot Stars", Kudritzi 2000
!Justify this Vinf-Vesc relation for Red Giants stars is more complicated, altought it is not 
!bad aprox.

!ii)For WR stars Vin=2000km/s is given (Meynet 1994)
!---------------------------------------------------------------------------------
 
  USE units
  USE explosions

  implicit none
  
  logical phase

  integer i,pos1,pos2

  real, parameter :: sigmae=0.3             !electron scattering coefficient per unit mass

  real :: L,Teff,R,Factor,Vesc,Vinf,Rti,LogL,logT, M, gammaW
  real, allocatable, dimension(:) :: Rlife1

  pos1=int(2*hms(i,13)-1)              
  pos2=int(2*(hms(i,13)+1)-1)

  if (phase) then              !In Main Sequence
!     print *,'holaVinf',hms(i,2)
     call interpolation(i,Rti,vinfph1tbl,vinfph1row,vinfph1column,1, &
                       pos1,pos2,vinf) 
!     print *,'adiosVinf',hms(i,2)  
     vinf=1.e8*log10(vinf)             ![Vinf]=cm/s

  else
     if (hms(i,2).GE.40.) then  !Initial mass larger or equal than 40
                                !in WR phase
        vinf=2.*1.e8
        return
     else                       !Initial mass smaller than 40
                                !in cold phase
         call interpolation(i,Rti,vinfph2tbl,vinfph2row,vinfph2column,1, &
                       pos1,pos2,vinf)
      end if
  end if   




END subroutine




!*********************************************************************************
SUBROUTINE input_ISM(i,lostmass,Vinf,Energyinput,rho,e,totalRti,Relatmaxerror,ldist,mdist,ndist)


!---------------------------------------------------------------------------------
!First calculate the amount of (kinetic) energy to be added to the surroundings, 
!corresponding with the energy input in the ISM during this time interval.
!This amount, along with the lostmass, will be addedd  following the same profile 
!used to input the energy from SN. The energy will be modify in form of THERMAL 
!energy, altought will be transformed to mechanical (because of energy ecuation)
!---------------------------------------------------------------------------------


  USE units
  USE explosions
  implicit none
  
  logical phase
  integer i,j,ldist,mdist,ndist,ll,mm,nn,l,m,n,s,s2,smin
  real :: lastVinf,Vinf,rampl,eampl,totalRti, &
          dwidth,arg, ex,v_wind,e_0,sup_edge, &
          inf_edge,windm,err,wwind2,dwidth_wwind, &
          massprof,f_wwind,arg2,ex2,massprof2, &
          idwidth,iww2,idwidth_wwind,iwwind, &
          massprof2b

  real xx,yy,zz,x(mx),y(my),z(mz),lostmass,lim
  
  real, dimension(mx,my,mz) :: rho, e, prof_wind,prof_mass, &
       prof_mass2,prof_wind2
  double precision Energyinput,Maxerror,lostmassCGS,Relatmaxerror 


  lostmassCGS=lostmass*msun                                              !To cgs units

  ldist=45
  mdist=45
  ndist=45



!  write(*,*) 'lostmass  ',lostmass

!Aprox  Integral(Lw*dt)=Integral((Mdot*Vinf^2)/2*dt)
!in the time interval [ti-1,ti] guessing that Vinf is an straight line in this interval 

  lastVinf=hms(i,18)
!  write(*,*) 'Energy', (0.5*0.5*lostmassCGS)*Vinf**2.
  Energyinput=(0.5d0*lostmassCGS)*(Vinf**2+lastVinf**2)*0.5
  


!If we suposse that Vinf(ti-1) <= Vinf(t) <= Vinf(ti) for all
!t in [ti-1,ti] (almost all its life) then Max error will be:

  maxerror=(0.5d0*lostmassCGS)*(0.5d0*(Vinf**2.-lastVinf**2.))
!  write(*,*) 'Energyinput',Energyinput
!  write(*,*) 'Maxerror', maxerror
   
  Relatmaxerror=maxerror/Energyinput
!  write(*,*) 'Relative Max. error ', Relatmaxerror
  hms(i,19)=hms(i,19) + real(Energyinput/1d51)                        


!!!!!!!!!!!!!!!!!!!!!!!!!!!!  
! Calculate star position!!!  
!!!!!!!!!!!!!!!!!!!!!!!!!!!!

!Calculate the star position in the box assuming that star will have constant 
!velocity through all its life (and equal to the central velocity of the cube where
!the star was created) 
 

  ldist = modulo(nint(hms(i,8)*(totalRti-1)*hms(i,14)/dx) + hmspos(i,1),mx)  
  mdist = modulo(nint(hms(i,9)*(totalRti-1)*hms(i,14)/dy) + hmspos(i,2),my)
  ndist = modulo(nint(hms(i,10)*(totalRti-1)*hms(i,14)/dz) + hmspos(i,3),mz)
! Cambio para TEST!!!: el bueno es el de arriba! El de abajo (dejar las 
! estrellas fijas) es para test


!  ldist=hmspos(i,1)
!  mdist=hmspos(i,2)
!  ndist=hmspos(i,3)

  if (ldist .eq. 0) ldist = mx
  if (mdist .eq. 0) mdist = my
  if (ndist .eq. 0) ndist = mz




!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Input of energy and mass around the star!! 
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

!Same profile as SN explosion

  dwidth=wwind*dx
  lim=15./pwind+1               !consider 0. terms of order 1.d-15
  idwidth=1./dwidth
  iww2=1./(ww2*dx)  
  iwwind=1./wwind
  s2=ww2*(log(sqrt(1.d10))/pwind+1.)
  s=wwind*(log(sqrt(1.d10))/pwind+1.)             !If prof_wind is of order 1.d-10 then prof_wind=0.
!$omp parallel do private(n)
  do n=1,mx 
     prof_wind(:,:,n)=0.  
     prof_mass2(:,:,n)=0.
     prof_wind2(:,:,n)=0.
  enddo

  !$omp parallel do private(zz,n,nn,yy,m,mm,xx,l,ll,arg,arg2,ex,ex2)
  do n=-s-1,s+1
     nn=ndist+n
     nn=merge(nn,nn-mz,nn .le.mz)
     nn=merge(nn,nn+mz,nn .gt.0)
     zz=n*dz
     do m=-s-1,s+1
        mm=mdist+m
        mm=merge(mm,mm-mz,mm .le.mz)
        mm=merge(mm,mm+mz,mm .gt.0)
        yy=m*dy        
        do l=-s-1,s+1
           ll=ldist+l
           ll=merge(ll,ll-mz,ll.le.mz)
           ll=merge(ll,ll+mz,ll.gt.0)
           xx=l*dx
           arg=sqrt(xx**2+yy**2+zz**2)/dwidth
           if (arg .le.(s+1.)*1e0/wwind) then
              ex=exp(pwind*(arg-1.))               ! pwind=steepness
              prof_wind(ll,mm,nn)=1./(1.+ex**2)    
           endif   
           arg2=sqrt(xx**2+yy**2+zz**2)/(ww2*dx)       ! wwind=ww2
           if (arg2.le.(s2+1.)*1e0/ww2) then     
              ex2=exp(pwind*(arg2-1.))   
              prof_wind2(ll,mm,nn)=1./(1.+ex2**2)         ! tanh()-like profile in log e
           endif
           prof_mass2(ll,mm,nn)=prof_wind2(ll,mm,nn)*rho(ll,mm,nn)
        enddo
     enddo
  enddo
  massprof2=sum(prof_mass2)*dx**3
  windm=2.*mu*(mproton*(Energyinput/cmu))/kB/windtemp/3.


if (hms(i,2).lt.20. .or. massprof2.gt.windm) then

  v_wind=sum(prof_wind)*dx*dy*dz
  e_0=Energyinput/ceu
  e_0=e_0/v_wind
  rampl=lostmassCGS/cmu
  rampl=rampl/v_wind
!print *, 'rampl, e_0, Minitial',rampl,'  ', e_0,'  ',hms(i,2)
  rho=rho +  rampl*prof_wind
  e  =e   +  e_0*prof_wind

else
 
  rampl=lostmassCGS/cmu
  err=1.
  inf_edge=1.
  sup_edge=(mx*1.e0)
  wwind2=inf_edge+(sup_edge-inf_edge)/2.
  do while (err>errw)
     dwidth_wwind=wwind2*dx
     idwidth_wwind=1./dwidth_wwind
     s=wwind2*(log(sqrt(1.d10))/pwind+1.)             !If prof_wind is of order 1.d-10 then prof_wind=0.
     smin=min(s,mx/2)
     !$omp parallel do private(n)
     do n=1,mx 
        prof_wind(:,:,n)=0.  
        prof_mass(:,:,n)=0.
     enddo
     !$omp parallel do private(zz,n,nn,yy,m,mm,xx,l,ll,arg,ex)
     do n=-smin-1,smin+1
        nn=ndist+n
        nn=merge(nn,nn-mz,nn .le.mz)
        nn=merge(nn,nn+mz,nn .gt.0)
        zz=n*dz
        do m=-smin-1,smin+1
           mm=mdist+m
           mm=merge(mm,mm-mz,mm .le.mz)
           mm=merge(mm,mm+mz,mm .gt.0)
           yy=m*dy        
           do l=-smin-1,smin+1
              ll=ldist+l
              ll=merge(ll,ll-mz,ll.le.mz)
              ll=merge(ll,ll+mz,ll.gt.0)
              xx=l*dx
              arg=sqrt(xx**2+yy**2+zz**2)/dwidth_wwind
              if (arg .le.(s+1.)*1e0/wwind2) then
                 ex=exp(pwind*(arg-1.))               ! pwind=steepness
                 prof_wind(ll,mm,nn)=1./(1.+ex**2)    
              endif
              prof_mass(ll,mm,nn)=prof_wind(ll,mm,nn)*rho(ll,mm,nn)
           enddo
        enddo
     enddo
     massprof=sum(prof_mass)*dx*dy*dz
     f_wwind=massprof+rampl-windm
     if (f_wwind > 0) then
        sup_edge=wwind2
     else 
        inf_edge=wwind2
     endif
     err=abs(f_wwind)/windm
     wwind2=inf_edge+(sup_edge-inf_edge)/2.

!     print *,'EDGES of wwind2 ,wwind2  ',inf_edge,sup_edge, wwind2 
!     print *,'massprof, windm, f_wwind  ',massprof,windm,f_wwind  
!     print *,'windm en Mo,energy', windm*(cmu/msun), Energyinput
!     print *,'massprof en Mo   ', massprof*(cmu/msun)
!     print *,'rampl en Mo  y en code units ', rampl*(cmu/msun),rampl
!     print *,'error-->    ',err
 
  enddo   !of WHILE

  v_wind=sum(prof_wind)*dx*dy*dz
  rampl = rampl/(v_wind*1.d0)
  rho = rho*1.d0+rampl*(prof_wind*1.d0)
  e=e+1./utemp*prof_wind*rho*windtemp   

!  print *,'ENERGIA METIDA, SOBRE MASA, MASA ESTRELLA, wwind2'
!  print *,sum(1./utemp*prof_wind*rho*windtemp)*ceu*dx**3, sum(rho*prof_wind)*(cmu/msun)*dx**3,hms(i,2),wwind2

endif
 
   
END subroutine


!***********************************************************************
SUBROUTINE interpolation(i,Rti,tbl,row,column,columnsFromAge,pos1,pos2,x)

USE explosions

integer i,j,pos1,pos2,pos3,columnsFromAge,row,column

real Rti,m1,m2,m3,n1,n2,n3,interpol1,interpol2,x

real,allocatable, dimension(:) :: Rlife1,Rlife2,a
real, dimension(row,column) :: tbl




!columnsFromAge: 1 for lostMass Rate and Luminosity because they are 1 column right
!the age rate column in their matrix.
!It takes 2 for Temperature for the same reason.


!First interpolation in the columns that refer to normalized life and mass
!of the stars with initial mass just above the initial mass corresponding to 
!hms(i,13)

!life tbl column corresponding with initialmass between those (mass(itau),mass(itau+1)) 

                
  allocate(Rlife1(row))
  allocate(a(row))
  Rlife1=tbl(:,pos1)


  a=-1.
  where(Rlife1.le.Rti) a=Rlife1
  do j=1,row
     if (Rlife1(j).eq.maxval(a)) then
!       write(*,*) '1bien:',Rlife1(j),a
        exit
     endif
     if(j.eq.row) then
        write(*,*) '1fallo',Rlife1(j),a       
         write(*,*) 'What in the Earth1 are u doing HERE'
     endif      
  end do
  m1=(tbl(j+1,pos1+columnsFromAge)-tbl(j,pos1+columnsFromAge))/(tbl(j+1,pos1)&
     -tbl(j,pos1))
        
  n1=tbl(j,pos1+columnsFromAge)-m1*tbl(j,pos1)
  interpol1=m1*Rti+n1
  deallocate(Rlife1)

!Second interpolation in the table Normilized life Vs mass: Initial &
!mass of the star is just below of the initial mass corresponding with &
!hms(i,13)+1

  
  allocate(Rlife2(row))
  Rlife2=tbl(:,pos2)
!  write(*,*) 'POS2:    ',pos2
!  write(*,*) 'RLIFE2: ',Rlife2
  a=-1.  
  where(Rlife2.le.Rti) a=Rlife2
  do j=1,row
     if (Rlife2(j).eq.maxval(a))  then 
!       write(*,*) '2Bien:',Rlife2(j),a
        exit
     endif              
     if(j.eq.row) then 
       write(*,*) '2fallo:',Rlife2(j),a
        write(*,*) 'What in the Earth2 are u doing HERE'
     endif
  end do
  m2=(tbl(j+1,pos2+columnsFromAge)-tbl(j,pos2+columnsFromAge))/(tbl(j+1,pos2)&
     -tbl(j,pos2))
        
  n2=tbl(j,pos2+columnsFromAge)-m2*tbl(j,pos2)
  interpol2=m2*Rti+n2
  deallocate(Rlife2)
  deallocate(a)

!Final interpolation depending on the initial mass!
  
  pos3=int(hms(i,13))
  m3=(interpol2-interpol1)/(genevelifetbl(pos3+1,1)-genevelifetbl(pos3,1))
  n3=interpol1-m3*genevelifetbl(pos3,1)
  x=m3*hms(i,2) + n3    



END subroutine
END MODULE
!***********************************************************************
SUBROUTINE write_hms (i,j,k,r,mass,lifetime,explotime,totalm, &
     totallostM,Ph1lifetime,Ph1totallostM)
!-----------------------------------------------------------------------
!Write density, time, and position for the cell where 
!a high-mass star is born. Also its mass, lifetime and explosion time are recorded 
!-----------------------------------------------------------------------

USE params
implicit none

integer i,j,k
real, dimension(mx,my,mz) :: r
real lifetime, explotime, mass, totalm, totallostM,Ph1lifetime,Ph1totallostM


open(5,file=file(1:index(file,'.'))//'hms', position='append', status='unknown')
write(5,*) 'TMASS  ','   t      ','  Position   ','Density   ','Mass  ','Lifetime ', &
             'Expltime ', 'TotallostMass',' Ph1lifetime  ','Ph1totallostM'
write(5,100) totalm,t,i,j,k,r(i,j,k),mass,lifetime,explotime,totallostM,Ph1lifetime,Ph1totallostM
100 format(ES10.3,ES10.3,I4,I4,I4,ES11.3,ES10.3,ES10.3,ES10.3,ES10.3,ES10.3,ES10.3)
close(5)

END subroutine

!***********************************************************************
SUBROUTINE write_wind (Masa,totalRti,totalRlostmass,Vinf,Relatmaxerror, &
TotalEnergyinput,lostmass,l,m,n)
!-----------------------------------------------------------------------
!Write in a file the evolution of the lost mass rate (totalRlostmass), Vinf, largest relative
!error (Relatmaxerror),the total Energy input  and the position in the grid (i,j,m)
!-----------------------------------------------------------------------

USE params
implicit none


integer l,m,n
real Vinf,totalRti,totalRlostmass,Masa,TotalEnergyinput,lostmass  
double precision Relatmaxerror
close(6)
  if (abs(27.58-Masa).le.0.1 .or. abs(61.86-Masa).le.0.1) then
    
    open(6,file=file(1:index(file,'.'))//'wind', position='append', status='unknown')
    write(6,102) Masa    ,totalRti ,totalRlostmass  ,vinf    ,Relatmaxerror ,TotalEnergyinput ,lostmass,l  ,m  ,n
102 format     ( ES12.4  ,ES12.4   ,ES12.4          ,ES12.4  ,ES12.4        ,ES12.4           ,ES12.4  ,I4 ,I4 ,I4)
    close(6) 
endif


END subroutine




!***********************************************************************
SUBROUTINE init_wind
 
 USE params, only: stdin, mid
 USE stellarwind
 implicit none

 integer :: i,j,k
 character(len=mid):: id="$Id: winds_Schaller.f90,v 1.4 2005/10/08 23:39:48 aake Exp $"

 call print_id(id)

 do_wind= .true.
 pwind=5.
 wwind=4.
 windtemp=5.e6
 ww2=3.
 errw=0.1
 
phase1row=13
phase1column=20
phase2row=39
phase2column=20


vinfph1row=13
vinfph1column=20
vinfph2row=39
vinfph2column=14

!First 2 columns in phasefiles and ALTfiles correspond with M=7Mo from Schaller 1992
!The razon is that for M=15,20,25 Meynet and Schaller plots from their 
!tables have almost the same shape  



!Meynet
! phase1file='../src/FORCING/YIELDS/RAgeMassphase1.tbl'
! phase2file='../src/FORCING/YIELDS/RAgeMassphase2.tbl'
! ALTph1file='../src/FORCING/YIELDS/ALTphase1.tbl'
! ALTph2file='../src/FORCING/YIELDS/ALTphase2.tbl'


!Schaller
 phase1file ='../src/FORCING/YIELDS/RAgeMassphase1.sch'
 phase2file ='../src/FORCING/YIELDS/RAgeMassphase2.sch'
 vinfph1file='../src/FORCING/YIELDS/RAgeVinfphase1.sch'
 vinfph2file='../src/FORCING/YIELDS/RAgeVinfphase2.sch'


 read (stdin,wind)
 write(*,wind)

 if (.not. do_wind) return

 
!*************************************************
!Nota: phase1file and 2 (=../src/FORCING/YIELDS/phase1file.dat') contiene
!un array de dimensiones 51*16. 16 es 8 columnas (1,3,5...)
!que se corresponden con la tasa de vida de estrellas de masa 
!7,15,20.... y otras 8 columnas (2,4,6,....) que se corresponden con 
!la tasa de masa perdida (con respecto al total) en funcion de la 
!tasa de vida de estrellas para las estrellas con la misma masa inicial.
!Es decir las dos primeras columnas se refieren a 7Mo, las dos siguientes
!se asocian a 15Mo y asi sucesivamente...
 

 allocate(phase1tbl(phase1row,phase1column))
 open(1,file=phase1file,status='old', form='formatted')
 do k=1,phase1row
        read(1,*) ((phase1tbl(i,j), i=k,k), j=1,phase1column)
 end do
 close(1)

 allocate(phase2tbl(phase2row,phase2column))
 open(1,file=phase2file,status='old', form='formatted')
 do k=1,phase2row
        read(1,*) ((phase2tbl(i,j), i=k,k), j=1,phase2column)
 end do
 close(1)


!Here we fill the tables with Rate of time in phase, Log(L/Lo), 
!and log(T) for stars with M=7,15,20,25,40,60,85,120
!So 24 columns



 allocate(vinfph1tbl(vinfph1row,vinfph1column))
 open(1,file=vinfph1file,status='old', form='formatted')
 do k=1,vinfph1row
        read(1,*) ((vinfph1tbl(i,j), i=k,k), j=1,vinfph1column)
 end do
 close(1)

 allocate(vinfph2tbl(vinfph2row,vinfph2column))
 open(1,file=vinfph2file,status='old', form='formatted')
 do k=1,vinfph2row
        read(1,*) ((vinfph2tbl(i,j), i=k,k), j=1,vinfph2column)
 end do
 close(1)



!Aqui ira la inizializacion de la matriz 'normalized' i.e. aquella compuesta 
!los porcentajes de vida y masa perdida para estrellas de masa 15,20,25,40
!60,85,120 mas una de 8M artificial (en Meynet) que por extrapolacion sera igual 
!a la tabla de 15 (puesto que la de 15 y 20 son practicamente iguales.... 
!ademas hemos elegido que las estrellas menores de 12M pierdan muy poca 
!masa por lo el perfil no es realmente importante.   



END subroutine

!***********************************************************************
SUBROUTINE stellar_wind(rho,e)

!-----------------------------------------------------------------------
!Loop over the array where the star are stored. For each star, Lostmass,
!and Energy input on ISM are calculated and  


  USE explosions
  USE params
  USE units
  USE stellarwind

  implicit none
 
  integer  i,ldist,ndist,mdist
  real, dimension(mx,my,mz) :: rho,e
  real ti,Rti,Mi,Rmi,Vinf,totalRti,totalRlostmass,Mi2, lostmass
  double precision Energyinput, Relatmaxerror
  logical phase1

  if (.not. do_wind) return
   
  

! Only if massive stars exit
   if (mhms.eq.0) then
      return
   else 
      if (sum(hms(:,12)).eq.0.) return
   endif
      
 
!Bucle sobre las estrellas 
   do i=1,mhms
       ti =abs(t-(hms(i,1)-hms(i,14))) !how old the star is.
                                   !(ABS to avoid tiny negative numbers in the
                                   !first call to the subroutine
        if (ti.le.hms(i,16)) then  !it decides in which phase is the star
           Rti=ti/hms(i,16)        !and which rate of time has been expend there 
           phase1=.true.
        else
           Rti=(ti-hms(i,16))/&
                (hms(i,14)-hms(i,16))
           phase1=.false.
        end if   
       
        Mi = hms(i,2)-hms(i,15)    !Mass already ejected

 
!       write(*,*) 'AGE:  ',ti,'  TASA VIDA:   ',Rti,'  MASA  ',hms(i,2)

        call calculate_lostmass(i,Rti,Mi,phase1,lostmass,Rmi,Mi2)
        call calculate_vinf(i,Rti,phase1,Vinf)
        if (phase1) then
           totalRti=(Rti*hms(i,16))/hms(i,14)
        else
           totalRti=((1.-Rti)*hms(i,16)+Rti*hms(i,14))/hms(i,14)
        endif
        if (lostmass.gt.0.0) then     !Avoid problems with tiny numbers
           call input_ISM(i,lostmass,Vinf, Energyinput,rho,e,totalRti,Relatmaxerror,ldist,mdist,ndist)
        endif   
        totalRlostmass=(hms(i,2)-hms(i,15))/hms(i,12)
      if (mod(it,nsnap).eq.0) then   
           write(*,*) 'Masa', hms(i,2)
           write(*,*) 'Vinf   :',Vinf
           write(*,*) 'totalRlostmass  : ',totalRlostmass
           write(*,*) 'totalRti        : ',totalRti
           write(*,*) 'PHASE 1???      : ',Phase1
           write(*,*) 't               : ',t
           write(*,*) 'AGE:  ',ti
           write(*,*) 'Mass lost until now (in Mo): ',totalRlostmass*hms(i,12)
      end if
      if (mod(it,10).eq.0) then 
         call write_wind(hms(i,2),totalRti,totalRlostmass,Vinf,Relatmaxerror,hms(i,19),lostmass,ldist,mdist,ndist) 
      end if 
       hms(i,18)=vinf
   end do

END subroutine


















