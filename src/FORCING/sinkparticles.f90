! $Id: sinkparticles.f90,v 1.37 2009/09/09 14:59:15 aake Exp $
!***********************************************************************
MODULE sink_m
!
! The sink particle mass is added back into the density field before 
! computing the gravitational potential.  To avoid selfinteraction, the
! mass is always centered exactly on the nearest mesh point.
!
! MPI processes are assumed to update the particles that belong to their
! domain.  The number of sink particles is small enough for the data to
! be broadcast to all processes between time steps.
!
! sink particle array elements:
!  1: x-coordinate
!  2: y-coordinate
!  3: z-coordinate
!  4: x-momentum
!  5: y-momentum
!  6: z-momentum
!  7: mass
!  8: velocity
!  9: gas density
! 10: time of creation
! 11: accretion time
! 12: near neighbors
! 13: dx/dt
! 14: dy/dt
! 15: dz/dt
! 16: dpx/dt
! 17: dpy/dt
! 18: dpz/dt
!                     ---------------------                                               --------------
!                     |      explode      |                                               |  forceit   |
!                     ---------------------                                               --------------
!                    /          |          \                                                    |
!        --------------  ----------------  ------------                                   --------------
!        | extend_mpi |  | create_sinks |  |  accrete |                                   |  selfgrav  |
!        --------------  ----------------  ------------                                   --------------
!                         /            \           \                                    /       |        \
!                 ------------   ---------------  ----------------         -------------    -----------   ------------------
!                 | find_max |   | create_sink |--| accrete_sink |         | add_stars |    | gravpot |   | xyz_move_stars |
!                 ------------   ---------------  ----------------         -------------    -----------   ------------------
! Subroutines:
!
!  init_explode             initialize (name borrowed from explosion handling)
!  read_explode             read parameters
!  write_sink               write sink particles to file
!  read_sink                read sink particles from file
!  add_stars                add the sink particles masses to the density field
!  add_mass                 add one sink particle mass to the density field
!  [xyz]_move_stars         move stars
!  accrete                  accrete onto existing sink particles
!  create_sinks             create new sink particles
!  create_sink              create one new sink particles
!  find_max                 find the maximum in the local neighborhood
!-----------------------------------------------------------------------
  USE params
  implicit none
  integer, parameter :: lsink=18, mghost=2
  integer msink, verbose
  real rho_limit, phi_bh, rho_cut
  real, allocatable, dimension(:) :: sink1
  real, allocatable, dimension(:,:) :: sinks, osinks

  integer, parameter:: m=2
  real, save, dimension(-m:m,-m:m,-m:m):: prf, prfx, prfy, prfz
  logical, save:: first_time=.true.
  character(len=mfile) sink_file
  logical do_sink

CONTAINS

!***********************************************************************
SUBROUTINE accrete (rho, px, py, pz, xdnr, ydnr, zdnr)
!
! Look for points where the density is above the critical one
!
  USE params
  implicit none
  real, dimension(1-mghost:mx+mghost,1-mghost:my+mghost,1-mghost:mz+mghost):: &
    rho,px,py,pz,xdnr,ydnr,zdnr
  integer i,i1,i2
  real dm
!-----------------------------------------------------------------------
  if (do_trace .and. master) print *,'BEGIN accrete_sink_particles',msink

  sinks(:,12) = 0.                                                      ! count close encounters
  !$omp parallel private(i,i1,i2)
  call limits_omp(1,msink,i1,i2)
  do i=i1,i2
    call accrete_sink (i, rho, px, py, pz, xdnr, ydnr, zdnr, dm)
  end do
  !$omp end parallel

  if (do_trace .and. master) print *,'END   accrete_sink_particles'
END SUBROUTINE

!***********************************************************************
SUBROUTINE accrete_sink (i, rho, px, py, pz, xdnr, ydnr, zdnr, dm)
!
! Look for points where the density is above the critical one
!
  USE params
  USE forcing, only: conservative
  USE selfgravity
  implicit none
  real, dimension(1-mghost:mx+mghost,1-mghost:my+mghost,1-mghost:mz+mghost):: &
    rho,px,py,pz,xdnr,ydnr,zdnr
  integer i, ix, iy, iz, ix1, iy1, iz1, jx, jy, jz, n_aver, j
  real x, y, z, dv, dm, drho, dpi, dpx, dpy, dpz, ux_aver, uy_aver, uz_aver, &
    rho_aver, rho_new, u_aver, px_aver, py_aver, pz_aver, rho_max, rho_min
  real(kind=8) frho, frho_max, sum1
  logical reached_limit
!-----------------------------------------------------------------------
  if (do_trace .and. master) print *,'BEGIN accrete_sink_particles',msink

!-----------------------------------------------------------------------
! Overflow accretion; mass above the density limit is accreted
!-----------------------------------------------------------------------
  dv = dx*dy*dz
  reached_limit = .false.
  x = sinks(i,1)
  y = sinks(i,2)
  z = sinks(i,3)
  ix = (x-xm(1))/dx + 1.5
  iy = (y-ym(1))/dy + 1.5
  iz = (z-zm(1))/dz + 1.5
  ix1 = max(1,min(ix,mx+0))
  iy1 = max(1,min(iy,my+0))
  iz1 = max(1,min(iz,mz+0))

!-----------------------------------------------------------------------
! Check for close encounters
!-----------------------------------------------------------------------
  do j=i+1,msink
    jx = (sinks(j,1)-xm(1))/dx + 1.5
    jy = (sinks(j,2)-ym(1))/dy + 1.5
    jz = (sinks(j,3)-zm(1))/dz + 1.5
    if (max(abs(ix-jx),abs(iy-jy),abs(iz-jz)) <= mghost) then
      sinks(i,12) = sinks(i,12) + 1.
      !$omp critical
      sinks(j,12) = sinks(j,12) + 1.
      !$omp end critical
      if (verbose > 1) then
        print *,'WARNING: close encounter:'
    print '(a,i5,6f10.6,f5.0)','particle',i,sinks(i,1:3),sinks(i,4:6)/sinks(i,7),sinks(i,12)
    print '(a,i5,6f10.6,f5.0)','particle',j,sinks(j,1:3),sinks(j,4:6)/sinks(j,7),sinks(j,12)
      else if (verbose > 0) then
        print *,'WARNING: close encounter btw particles',i,j
      end if
    end if
  end do

!-----------------------------------------------------------------------
! Check for accretion
!-----------------------------------------------------------------------
  dm = 0.
  rho_aver = 0.
  rho_max = 0.
  do jz=iz1-m,iz1+m
  do jy=iy1-m,iy1+m
  do jx=ix1-m,ix1+m
    if (rho(jx,jy,jz) > rho_limit) then
      reached_limit = .true.
      rho_new = rho_cut*rho_limit
      dm = dm + (rho(jx,jy,jz)-rho_new)*dv
      !rho(jx,jy,jz) = rho_new
    end if
    rho_aver = rho_aver + prf(jx-ix1,jy-iy1,jz-iz1)*rho(jx,jy,jz)
    rho_max = max(rho_max,rho(jx,jy,jz))
  end do
  end do
  end do

  if (reached_limit) then
    px_aver = 0.
    do jz=iz1-m,iz1+m
    do jy=iy1-m,iy1+m
    do jx=ix1-m+1,ix1+m
      px_aver = px_aver + prfx(jx-ix1,jy-iy1,jz-iz1)*px(jx,jy,jz)               ! average momentum
    end do
    end do
    end do
    ux_aver = px_aver/rho_aver                                                  ! average core velocity
    dpx = 0.
    do jz=iz1-m,iz1+m
    do jy=iy1-m,iy1+m
    do jx=ix1-m,min(ix1+m,mx+m-1)
      if (rho(jx,jy,jz) > rho_limit) then
        rho_new = rho_cut*rho_limit
        dpi = (rho(jx,jy,jz)-rho_new)*ux_aver
        dpx = dpx + dpi
        px(jx  ,jy,jz) = px(jx  ,jy,jz) - 0.5*dpi                               ! subtract loss over profile
        px(jx+1,jy,jz) = px(jx+1,jy,jz) - 0.5*dpi                               ! subtract loss over profile
      end if
    end do
    end do
    end do
    sinks(i,4) = sinks(i,4) + dpx*dv                                            ! add total momentum to sink p

    py_aver = 0.
    do jz=iz1-m,iz1+m
    do jy=iy1-m+1,iy1+m
    do jx=ix1-m,ix1+m
      py_aver = py_aver + prfy(jx-ix1,jy-iy1,jz-iz1)*py(jx,jy,jz)               ! average momentum
    end do
    end do
    end do
    uy_aver = py_aver/rho_aver                                                  ! average core velocity
    dpy = 0.
    do jz=iz1-m,iz1+m
    do jy=iy1-m,min(iy1+m,my+m-1)
    do jx=ix1-m,ix1+m
      if (rho(jx,jy,jz) > rho_limit) then
        rho_new = rho_cut*rho_limit
        dpi = (rho(jx,jy,jz)-rho_new)*uy_aver
        dpy = dpy + dpi
        py(jx,jy  ,jz) = py(jx,jy  ,jz) - 0.5*dpi                               ! subtract loss over profile
        py(jx,jy+1,jz) = py(jx,jy+1,jz) - 0.5*dpi                               ! subtract loss over profile
      end if
    end do
    end do
    end do
    sinks(i,5) = sinks(i,5) + dpy*dv                                            ! add total momentum to sink p

    pz_aver = 0.
    do jz=iz1-m+1,iz1+m
    do jy=iy1-m,iy1+m
    do jx=ix1-m,ix1+m
      pz_aver = pz_aver + prfz(jx-ix1,jy-iy1,jz-iz1)*pz(jx,jy,jz)               ! average momentum
    end do
    end do
    end do
    uz_aver = pz_aver/rho_aver                                                  ! average core velocity
    dpz = 0.
    do jz=iz1-m,min(iz1+m,mz+m-1)
    do jy=iy1-m,iy1+m
    do jx=ix1-m,ix1+m
      if (rho(jx,jy,jz) > rho_limit) then
        rho_new = rho_cut*rho_limit
        dpi = (rho(jx,jy,jz)-rho_new)*uz_aver
        dpz = dpz + dpi
        pz(jx,jy,jz  ) = pz(jx,jy,jz  ) - 0.5*dpi                               ! subtract loss over profile
        pz(jx,jy,jz+1) = pz(jx,jy,jz+1) - 0.5*dpi                               ! subtract loss over profile
      end if
    end do
    end do
    end do
    sinks(i,6) = sinks(i,6) + dpz*dv                                            ! add total momentum to sink p

    do jz=iz1-m,iz1+m
    do jy=iy1-m,iy1+m
    do jx=ix1-m,ix1+m
      if (rho(jx,jy,jz) > rho_limit) then
        rho_new = rho_cut*rho_limit
        rho(jx,jy,jz) = rho_new
      end if
    end do
    end do
    end do
    sinks(i,7) = sinks(i,7) + dm
  end if

  ux_aver = sinks(i,4)/sinks(i,7)
  uy_aver = sinks(i,5)/sinks(i,7)
  uz_aver = sinks(i,6)/sinks(i,7)
  u_aver = sqrt(ux_aver**2+uy_aver**2+uz_aver**2)
  sinks(i,8) = u_aver
  sinks(i,9) = rho_aver
  !if (dm > 0) then
  !  sinks(i,11) = dt/dm*sinks(i,7)
  !else 
  !  sinks(i,11) = 0.
  !end if
  if ((reached_limit .and. verbose > 0) .or. verbose > 1) &
    print 1,i,reached_limit,sinks(i,7),ix1,iy1,iz1,ux_aver,uy_aver,uz_aver,rho_max,sinks(i,8:9),sinks(i,11)
1 format(1x,i4,l4,1g12.4,3i5,3f8.3,4g12.4)

  if (do_trace .and. master) print *,'END   accrete_sink_particles'
END SUBROUTINE

!***********************************************************************
SUBROUTINE create_sinks (rho, px, py, pz, xdnr, ydnr, zdnr)

! Look for points where the density is above the critical one
!
  USE params
  implicit none
  real, dimension(1-mghost:mx+mghost,1-mghost:my+mghost,1-mghost:mz+mghost):: rho, px, py, pz, xdnr, ydnr, zdnr
  real rhomax, rhotmp, dm
  integer ix, iy, iz, jx, jy, jz
!-----------------------------------------------------------------------
  if (do_trace .and. master) print *,'BEGIN create_sink_particles',msink

  rhomax=0.
  !$omp parallel private(rhotmp)
  rhotmp = maxval(rho(:,:,izs:ize))
  !$omp critical
  rhomax = max(rhomax,rhotmp)
  !$omp end critical
  !$omp end parallel
  if (rhomax < rho_limit) then
    if (verbose > 0 .and. master) print *,'create_sinks bail out, rhomax =',rhomax
    return
  end if
   
  do iz=1,mz
  do iy=1,my
  do ix=1,mx
    if (rho(ix,iy,iz) > rho_limit) then
      jx = ix
      jy = iy
      jz = iz
      call find_max (rho, jx, jy, jz)
      call create_sink (rho, px, py, pz, jx, jy, jz)
      call accrete_sink (msink, rho, px, py, pz, xdnr, ydnr, zdnr, dm)
    end if
  end do
  end do
  end do  
  if (do_trace .and. master) print *,'END   create_sink_particles',msink
END SUBROUTINE

!***********************************************************************
SUBROUTINE find_max (rho, jx, jy, jz)
!
! Look for points where the density is above the critical one
!
  USE params
  implicit none
  real, dimension(1-mghost:mx+mghost,1-mghost:my+mghost,1-mghost:mz+mghost):: rho
  real dv, rho_max
  integer j, jx, jy, jz, jx1, jy1, jz1, kx, ky, kz
!-----------------------------------------------------------------------
  
  rho_max = rho(jx,jy,jz)
  
  dv = dx*dy*dz
  do j=1,2
    jx1 = max(0,min(jx,mx+1))
    jy1 = max(0,min(jy,my+1))
    jz1 = max(0,min(jz,mz+1))
    do kz=jz1-1,jz1+1
    do ky=jy1-1,jy1+1
    do kx=jx1-1,jx1+1
      if (rho(kx,ky,kz) > rho_max) then
        jx = kx
        jy = ky
        jz = kz
        rho_max = rho(kx,ky,kz)
        if (verbose > 2) print *,'new max', jx, jy, jz
      end if
    end do
    end do
    end do
  end do
  if (verbose > 0) print *,'find_max:', jx, jy, jz, rho_max
END SUBROUTINE

!***********************************************************************
SUBROUTINE create_sink (rho, px, py, pz, jx, jy, jz)
!
! Look for points where the density is above the critical one
!
  USE params
  implicit none
  real, dimension(1-mghost:mx+mghost,1-mghost:my+mghost,1-mghost:mz+mghost):: rho,px,py,pz
  real uaver(3)
  integer i, jx, jy, jz, jx1, jy1, jz1, ix, iy, iz
!-----------------------------------------------------------------------

  do i=1,msink
    ix = (sinks(i,1)-xm(1))/dx + 1.5
    iy = (sinks(i,2)-ym(1))/dy + 1.5
    iz = (sinks(i,3)-zm(1))/dz + 1.5
    if (max(abs(ix-jx),abs(iy-jy),abs(iz-jz)) < mghost*2) then
      if (verbose > 0) print *,'abandoning, too close to',ix,iy,iz
      return
    end if
  end do

  if (msink == 0) then
    msink = msink+1
    allocate (sinks(msink,lsink))    
  else
    allocate (osinks(msink,lsink))
    osinks = sinks
    deallocate (sinks)
    msink = msink+1
    allocate (sinks(msink,lsink))
    sinks(1:msink,1:lsink) = osinks
    deallocate (osinks)
  end if

!-----------------------------------------------------------------------
! Initial position and time
!-----------------------------------------------------------------------

  sinks(msink,:) = 0.
  sinks(msink,1) = xm(1)+(jx-1)*dx
  sinks(msink,2) = ym(1)+(jy-1)*dy
  sinks(msink,3) = zm(1)+(jz-1)*dz
  sinks(msink,7) = 1e-30
  sinks(msink,10) = t
  if (verbose > 0) print 1,'create_sink_p:', msink, jx, jy, jz
1 format(1x,a,i5,3i4,4g12.4)
END SUBROUTINE

!***********************************************************************
SUBROUTINE write_sink
  USE params
  implicit none
  integer i, j
  character(len=mfile) fname, name
  character(len=mid), save:: id='write_sink: $Id: sinkparticles.f90,v 1.37 2009/09/09 14:59:15 aake Exp $'
!-----------------------------------------------------------------------
  call print_trace (id, dbg_expl, 'BEGIN')
  print *,'write_sink: isnap,msink,t =',isnap,msink,t

  if (msink > 0) then
    fname = name(sink_file,'sink',file)
    open (sink_unit,file=fname, position='append', form='unformatted', status='unknown')
    write (sink_unit) msink,lsink,t,size(sinks)
    write (sink_unit) sinks
    close(sink_unit)
    fname = name('sink.txt','snktxt',file)
    open (sink_unit,file=fname, form='formatted', status='unknown')
    write (sink_unit,*) msink, lsink, t
    write (sink_unit,'(a7,a8,11a12)') &
      'i', &
      'x ','y ','z ', &
      'vx','vy','vz', &
      'm ','u ','rho', &
      't ','t_acc'
    do i=1,msink
      sinks(i,4:6) = sinks(i,4:6)/sinks(i,7)
      write(sink_unit,'(i6,12g12.4)') i,(sinks(i,j), j=1,12)
      sinks(i,4:6) = sinks(i,4:6)*sinks(i,7)
    end do
    write (sink_unit,*) 'total mass =', sum(sinks(:,7))
    write (sink_unit,*) 'star  ekin =', sum((sinks(:,4)**2+sinks(:,5)**2+sinks(:,6)**2)/sinks(:,7))*0.5
    write (sink_unit,*) 'total ekin =', e_kin*sx*sy*sz
    write (sink_unit,*) 'total epot =', e_grav*sx*sy*sz
    close (sink_unit)

    open  (data_unit, file='energy.txt',form='formatted',position='append',status='unknown') 
    write (data_unit,'(3(1pg15.6))') t,e_kin,e_grav
    close (data_unit)
  end if

  call print_trace (id, dbg_expl, 'END')
  return
END subroutine

!***********************************************************************
SUBROUTINE read_sink
  USE params
  implicit none
  logical fexists, found
  integer sz_sink,lsink1,msink1
  real t0,t1
  character(len=mfile) fname, name
  character(len=mid), save:: id='read_sink: $Id: sinkparticles.f90,v 1.37 2009/09/09 14:59:15 aake Exp $'
!-----------------------------------------------------------------------
  call print_trace (id, dbg_expl, 'BEGIN')

  fname = name(sink_file,'sink',from)
  found = .false.
!-----------------------------------------------------------------------
! If sink.dat exits, then read sink particle dumps until either a time
! after the current time, or else end of file.  Keep the last set of
! sink particles, if any.
!-----------------------------------------------------------------------
  if (fexists(fname)) then 
    open (sink_unit, file=fname, status='old', form='unformatted')
    t1 = -1.
    do while (.true.)
      read (sink_unit,end=8,err=8) msink1,lsink1,t1
      if (verbose > 2) print *,"msink,t1,t,dt",msink1,t1,t,dt
      if (lsink1.ne.lsink) then
        print*,'WARNING: inconsistent sink particle data size', lsink1, lsink
        exit
      end if
      if (t1 > t) goto 8
      if (found) deallocate (sinks)
      msink = msink1
      allocate (sinks(msink,lsink))
      read (sink_unit,end=8) sinks
      t0 = t1
      found = .true.
    end do
  8 close(sink_unit)
    if (found) then
      write (*,*) 'Data of ',msink,' sink particles recovered from time ',t0
      close(sink_unit)
      return
    end if
  endif 
  print *,'WARNING: no sink particles found for time', t
  msink = 0
END subroutine 

END MODULE

!***********************************************************************
SUBROUTINE add_stars(rho)
  USE params
  USE units
  USE eos
  USE sink_m
  implicit none
  real, dimension(mx,my,mz):: rho
  integer i,i1,i2,ix,iy,iz
  real mass
!-----------------------------------------------------------------------
  call limits_omp (1,msink,i1,i2)
  do i=i1,i2
    mass=sinks(i,7)
    ix=(sinks(i,1)-xm(1))/dx+1.5
    iy=(sinks(i,2)-ym(1))/dy+1.5
    iz=(sinks(i,3)-zm(1))/dz+1.5
    call add_mass(mass,rho,ix,iy,iz)
  end do
  if (verbose > 4) then
    !$omp barrier
    !$omp master
    mass = sum(rho)/mw
    print *,'average density (including stars) =',mass
    !$omp end master
  end if
  call dumpl(rho,'add_stars','sink.dmp',0)
  
END SUBROUTINE

!***********************************************************************
SUBROUTINE add_mass(mass,rho,ix,iy,iz)
  USE params
  USE sink_m
  implicit none
  real mass,drho,sum
  real, dimension(mx,my,mz):: rho
  integer i,ix,iy,iz,jx,jy,jz,kx,ky,kz
!-----------------------------------------------------------------------
  drho=mass/(dx*dy*dz)
  do jz=-m,m
    kz=mod(iz-1+jz+mz,mz)+1
    do jy=-m,m
      ky=mod(iy-1+jy+my,my)+1
      do jx=-m,m
        kx=mod(ix-1+jx+mx,mx)+1
    !$omp critical
        rho(kx,ky,kz)=rho(kx,ky,kz)+drho*prf(jx,jy,jz)
    !$omp end critical
      end do
    end do
  end do
  if (verbose > 3) then
    print'(a,3i8,2(1pe12.3),3i5)','adding_mass',ix,iy,iz,mass,maxval(rho)
  end if
END SUBROUTINE

!***********************************************************************
SUBROUTINE x_move_stars(f)
  USE params
  USE timeintegration
  USE sink_m
  implicit none
  real, dimension(mx,my,mz):: f
  integer i,ix,iy,iz,i1,i2
!-----------------------------------------------------------------------
  if (msink==0) return
  call limits_omp (1,msink,i1,i2)
  do i=i1,i2
    ix=(sinks(i,1)-xm(1))/dx+1.5
    iy=(sinks(i,2)-ym(1))/dy+1.5
    iz=(sinks(i,3)-zm(1))/dz+1.5
    ix=mod(ix-1+mx,mx)+1
    iy=mod(iy-1+my,mx)+1
    iz=mod(iz-1+mz,mz)+1
    if (verbose > 3) print'(1x,a,3i4,3(1x,2f8.3),1pe11.3)', &
      'x_move',ix,iy,iz,sinks(i,1:3),sinks(i,4:6)/sinks(i,7),f(ix,iy,iz)
 
    sinks(i,13) = sinks(i,13)*alpha(isubstep) + sinks(i,4)/sinks(i,7)    ! dx/dt
    sinks(i,16) = sinks(i,16)*alpha(isubstep) - f(ix,iy,iz)*sinks(i,7)   ! dpx/dt
  end do
END SUBROUTINE

!***********************************************************************
SUBROUTINE y_move_stars(f)
  USE params
  USE timeintegration
  USE sink_m
  implicit none
  real, dimension(mx,my,mz):: f
  integer i,ix,iy,iz,i1,i2
!-----------------------------------------------------------------------
  if (msink==0) return
  call limits_omp (1,msink,i1,i2)
  do i=i1,i2
    ix=(sinks(i,1)-xm(1))/dx+1.5
    iy=(sinks(i,2)-ym(1))/dy+1.5
    iz=(sinks(i,3)-zm(1))/dz+1.5
    ix=mod(ix-1+mx,mx)+1
    iy=mod(iy-1+my,mx)+1
    iz=mod(iz-1+mz,mz)+1
    if (verbose > 3) print'(1x,a,3i4,2(1x,3f8.3),1pe11.3)', &
      'y_move',ix,iy,iz,sinks(i,1:3),sinks(i,4:6)/sinks(i,7),f(ix,iy,iz)

    sinks(i,14) = sinks(i,14)*alpha(isubstep) + sinks(i,5)/sinks(i,7)    ! dy/dt
    sinks(i,17) = sinks(i,17)*alpha(isubstep) - f(ix,iy,iz)*sinks(i,7)   ! dpy/dt
  end do
END SUBROUTINE

!***********************************************************************
SUBROUTINE z_move_stars(f,phi)
  USE params
  USE timeintegration
  USE sink_m
  implicit none
  real, dimension(mx,my,mz):: f,phi
  integer i,ix,iy,iz,i1,i2
!-----------------------------------------------------------------------
  if (msink==0) return
  call limits_omp (1,msink,i1,i2)
  do i=i1,i2
    ix=(sinks(i,1)-xm(1))/dx+1.5
    iy=(sinks(i,2)-ym(1))/dy+1.5
    iz=(sinks(i,3)-zm(1))/dz+1.5
    ix=mod(ix-1+mx,mx)+1
    iy=mod(iy-1+my,mx)+1
    iz=mod(iz-1+mz,mz)+1
    if (verbose > 2) print'(1x,a,3i4,2(1x,3f8.3),1pe11.3)', &
      'z_move',ix,iy,iz,sinks(i,1:3),sinks(i,4:6)/sinks(i,7),f(ix,iy,iz)
 
    sinks(i,15) = sinks(i,15)*alpha(isubstep) + sinks(i,6)/sinks(i,7)   ! dz/dt
    sinks(i,18) = sinks(i,18)*alpha(isubstep) - f(ix,iy,iz)*sinks(i,7)  ! dpz/dt

    sinks(i,1:3) = sinks(i,1:3) + dt*beta(isubstep)*sinks(i,13:15)      ! update positions
    sinks(i,4:6) = sinks(i,4:6) + dt*beta(isubstep)*sinks(i,16:18)      ! update momenta
    
    sinks(i,11) = phi(ix,iy,iz)

    if (isubstep==timeorder) then
      if (sinks(i,1) < xm(1)   -0.5*dx) sinks(i,1)=sinks(i,1)+sx        ! wrap around
      if (sinks(i,1) > xm(1)+sx+0.5*dx) sinks(i,1)=sinks(i,1)-sx        ! wrap around
      if (sinks(i,2) < ym(1)   -0.5*dy) sinks(i,2)=sinks(i,2)+sy        ! wrap around
      if (sinks(i,2) > ym(1)+sy+0.5*dy) sinks(i,2)=sinks(i,2)-sy        ! wrap around
      if (sinks(i,3) < zm(1)   -0.5*dz) sinks(i,3)=sinks(i,3)+sz        ! wrap around
      if (sinks(i,3) > zm(1)+sz+0.5*dz) sinks(i,3)=sinks(i,3)-sz        ! wrap around
    end if

    if ((verbose > 4 .and. i==1) .or. verbose > 5) then
      print*,'pos:',sinks(i,1:3)
      print*,'pos:',sinks(i,1:3)+dt*sinks(i,4:6)/sinks(i,7)
      print*,'vel:',sinks(i,4:6)/sinks(i,7)
      print*,'vel:',sinks(i,4:6)/sinks(i,7)+dt*sinks(i,16:18)/sinks(i,7)
      print*,'acc:',sinks(i,16:18)/sinks(i,7)
      print*,'acc:',sinks(i,16:18)/sinks(i,7)
    end if
  end do
END SUBROUTINE

!***********************************************************************
SUBROUTINE explode (rho,px,py,pz,e,d,Bx,By,Bz)
  USE params
  USE units
  USE forcing
  USE eos
  USE sink_m
  implicit none
  integer ix, iy, iz, msink0
  real rhomax, rhotmp
  logical do_io
  real, dimension(mx,my,mz):: rho,px,py,pz,e,d,Bx,By,Bz
  real, allocatable, dimension(:,:,:):: rho1,px1,py1,pz1,xdnr1,ydnr1,zdnr1, &
   xdnr,ydnr,zdnr,lnr

  if (.not. do_sink) return

  rhomax=0.
  !$omp parallel private(ix,iy,iz,rhotmp)
  rhotmp = maxval(rho(:,:,izs:ize))
  !$omp critical
  rhomax = max(rhomax,rhotmp)
  !$omp end critical
  !$omp end parallel
  if (rhomax < rho_limit) then
    if (verbose > 1 .and. master) print *,'no accretion, bail out, rhomax =',rhomax
    return
  end if
   
  allocate ( rho1(1-mghost:mx+mghost,1-mghost:my+mghost,1-mghost:mz+mghost))
  allocate (  px1(1-mghost:mx+mghost,1-mghost:my+mghost,1-mghost:mz+mghost))
  allocate (  py1(1-mghost:mx+mghost,1-mghost:my+mghost,1-mghost:mz+mghost))
  allocate (  pz1(1-mghost:mx+mghost,1-mghost:my+mghost,1-mghost:mz+mghost))
  allocate (xdnr1(1-mghost:mx+mghost,1-mghost:my+mghost,1-mghost:mz+mghost))
  allocate (ydnr1(1-mghost:mx+mghost,1-mghost:my+mghost,1-mghost:mz+mghost))
  allocate (zdnr1(1-mghost:mx+mghost,1-mghost:my+mghost,1-mghost:mz+mghost))
                                                                        call timer('sinks','alloc1')
  allocate (lnr(mx,my,mz))
  allocate (xdnr(mx,my,mz))
  allocate (ydnr(mx,my,mz))
  allocate (zdnr(mx,my,mz))
                                                                        call timer('sinks','alloc2')
  !$omp parallel private(iz)
  do iz=izs,ize
    lnr(:,:,iz) = alog(rho(:,:,iz))
  end do
  call xdn_set(lnr,xdnr)
  call ydn_set(lnr,ydnr)
  !$omp barrier
  call zdn_set(lnr,zdnr)
  do iz=izs,ize
    xdnr(:,:,iz) = exp(xdnr(:,:,iz))
    ydnr(:,:,iz) = exp(ydnr(:,:,iz))
    zdnr(:,:,iz) = exp(zdnr(:,:,iz))
  end do
  !$omp end parallel
                                                                        call timer('sinks','xyzdnr')
  !$omp parallel
  call extend_mpi (rho, mx, my, mz, rho1, mghost)
  call extend_mpi ( px, mx, my, mz,  px1, mghost)
  call extend_mpi ( py, mx, my, mz,  py1, mghost)
  call extend_mpi ( pz, mx, my, mz,  pz1, mghost)
  call extend_mpi (xdnr, mx, my, mz, xdnr1, mghost)
  call extend_mpi (ydnr, mx, my, mz, ydnr1, mghost)
  call extend_mpi (zdnr, mx, my, mz, zdnr1, mghost)
  !$omp end parallel
                                                                        call timer('sinks','extend')
  msink0 = msink
  call accrete (rho1,px1,py1,pz1,xdnr1,ydnr1,zdnr1)
                                                                        call timer('sinks','accrete')
  call create_sinks (rho1,px1,py1,pz1,xdnr1,ydnr1,zdnr1)
                                                                        call timer('sinks','create')
  !$omp parallel private(iz)
  do iz=izs,ize
    rho(:,:,iz) = rho1(1:mx,1:my,iz)
    px(:,:,iz)  = px1(1:mx,1:my,iz)
    py(:,:,iz)  = py1(1:mx,1:my,iz)
    pz(:,:,iz)  = pz1(1:mx,1:my,iz)
  end do
  !$omp end parallel
                                                                        call timer('sinks','rho')
  deallocate (rho1,px1,py1,pz1,xdnr1,ydnr1,zdnr1,lnr,xdnr,ydnr,zdnr)

  if (msink > msink0 .or. did_io) then                                  ! new sinks?
    call write_sink
  end if
                                                                        call timer('sinks','write')
END subroutine

!***********************************************************************
SUBROUTINE init_explode (rho)
  USE params
  USE units
  USE sink_m
  implicit none
  real, dimension(mx,my,mz):: rho
  integer jx, jy, jz
  character(len=mid):: id="$Id: sinkparticles.f90,v 1.37 2009/09/09 14:59:15 aake Exp $"
!-----------------------------------------------------------------------
  call print_id(id)
  msink = 0
  rho_limit = 300.
  phi_bh = 1.
  rho_cut = 0.90
  sink_file = 'sink.dat'
  do_sink = .false.

  call read_explode
  call read_sink

  first_time=.false.
  do jz=-m,m; do jy=-m,m; do jx=-m,m
    prf(jx,jy,jz)=exp(-0.75*real(jx**2+jy**2+jz**2))
  end do; end do; end do
  prf=prf/sum(prf)

  prfx = 0.
  do jz=-m,m; do jy=-m,m; do jx=-m+1,m
    prfx(jx,jy,jz)=exp(-0.75*real((jx-0.5)**2+jy**2+jz**2))
  end do; end do; end do
  prfx=prfx/sum(prfx)

  prfy = 0.
  do jz=-m,m; do jy=-m+1,m; do jx=-m,m
    prfy(jx,jy,jz)=exp(-0.75*real(jx**2+(jy-0.5)**2+jz**2))
  end do; end do; end do
  prfy=prfy/sum(prfy)

  prfz = 0.
  do jz=-m+1,m; do jy=-m,m; do jx=-m,m
    prfz(jx,jy,jz)=exp(-0.75*real(jx**2+jy**2+(jz-0.5)**2))
  end do; end do; end do
  prfz=prfz/sum(prfz)
  print *,'prf(0,0,0)=',prf(0,0,0)
  print *,'norm(prfxyz)=',sum(prfx),sum(prfy),sum(prfz)
END subroutine

!***********************************************************************
SUBROUTINE read_explode
  USE params
  USE units
  USE sink_m
  implicit none
  namelist /sink/do_sink,rho_limit,rho_cut,phi_bh,verbose, sink_file

  rewind (stdin); read (stdin,sink)
  if (master) write(*,sink)
END subroutine
