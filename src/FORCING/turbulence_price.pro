; $Id: turbulence_price.pro,v 1.3 2007/05/14 14:12:37 aake Exp $

;***********************************************************************
FUNCTION ddx, f, dx=dx
;
;  X partial derivative
;
;-----------------------------------------------------------------------
      s=size(f)
      if s(0) eq 2 then f=reform(f,s(1),s(2),1)
      if n_elements(dx) eq 0 then dx=1.
      a =  3./(4.*dx)
      b = -3./(20.*dx)
      c =  1./(60.*dx)
      if s(0) eq 1 then $
        return, a*(shift(f,-1) - shift(f,1)) $
              + b*(shift(f,-2) - shift(f,2)) $
              + c*(shift(f,-3) - shift(f,3))
      return, a*(shift(f,-1,0,0) - shift(f,1,0,0)) $
            + b*(shift(f,-2,0,0) - shift(f,2,0,0)) $
            + c*(shift(f,-3,0,0) - shift(f,3,0,0))
END
;***********************************************************************
FUNCTION ddy, f, dy=dy
;
;  Y partial derivative
;
;-----------------------------------------------------------------------
      s=size(f)
      if s(0) eq 1 then return, make_array(size=size(f))
      if s(0) eq 2 then f=reform(f,s(1),s(2),1)
      if n_elements(dy) eq 0 then dy=1.
      a =  3./(4.*dy)
      b = -3./(20.*dy)
      c =  1./(60.*dy)
      return, a*(shift(f,0,-1,0) - shift(f,0,1,0)) $
            + b*(shift(f,0,-2,0) - shift(f,0,2,0)) $
            + c*(shift(f,0,-3,0) - shift(f,0,3,0))
END
;***********************************************************************
FUNCTION ddz, f, dz=dz
;
;  Z partial derivative
;
;-----------------------------------------------------------------------
      s=size(f)
      if s(0) eq 1 then return, make_array(size=size(f))
      if s(0) eq 2 then f=reform(f,s(1),s(2),1)
      if n_elements(dz) eq 0 then dz=1.
      a =  3./(4.*dz)
      b = -3./(20.*dz)
      c =  1./(60.*dz)
      return, a*(shift(f,0,0,-1) - shift(f,0,0,1)) $
            + b*(shift(f,0,0,-2) - shift(f,0,0,2)) $
            + c*(shift(f,0,0,-3) - shift(f,0,0,3))
END

;***********************************************************************
; Display images of the force field, a time trace at one point, and
; print out the value of the normalized divergence of the force.
;***********************************************************************

close,1                                                                 ; make sure unit is closed
openr,1,'force.dat',/f77                                                ; open file
m=0L & nt=0L
readu,1,m,m,m,nt                                                        ; dimensions
ax=fltarr(m,m,m) & ay=ax & az=ax                                        ; arrays
a=fltarr(nt)
npix=160                                                                ; display size
npix=long(npix/m)*m                                                     ; round off
for i=0,nt-1 do begin
  readu,1,ax,ay,az
  tvscl,rebin(ax[*,*,0],npix,npix),npix*0,0                             ; image
  tvscl,rebin(ay[*,*,0],npix,npix),npix*1,0                             ; image
  tvscl,rebin(az[*,*,0],npix,npix),npix*2,0                             ; image
  a[i]=ax[0,0,0]                                                        ; one value
  wait,.01                                                              ; flush + delay
end
close,1
plot,a,psym=-1                                                          ; plot time dependence
diva=ddx(ax)+ddy(ay)+ddz(az)                                            ; check divergence
print,'divergence = ',sqrt(total(diva^2)/total(ddx(ax)^2))
END
