! $Id: stars_ones.f90,v 1.75 2012/09/26 15:06:00 aake Exp $
!***********************************************************************
! 
!  Overview of the main routines in this file:
!
!  init_explode         called by main, initializes values used later
!  init_wind            called by init_explode, initializes wind values
!  write_stars          called by initial_SNe and by explode
!  read_stars           called by initial_SNe and by explode
!  initial_SNe          called by init_explode, to make a few SNe
!  explode              called by main, once per time step
!  tracksn              called by find_star, stellarcluster, and by initial_SNe
!  lmsf                 called by explode, to make low mass stars
!  snpose               called by lmsf, to set the positions of new stars
!  find_star            called by explode, to determine position of cluster
!  SNexpl               called by explode
!  yield                called by SNexpl
!
!  explode      -> inital_SNe                           -> tracksn
!               -> find_star    -> stellarcluster       -> tracksn
!               -> lmsf         -> snpose
!               ->                                      -> SNexpl
!
!  'explode' first checks if more than 'masslim' of mass is available for
!  clusterformation, and if so calls 'find_star', which finds a position
!  based on equal probability per unit mass and then calls 'stellarcluster',
!  which generates stars distributed over mass as per an IMF.  Any SN
!  mass star is added to the list of massive stars that will explode
!  after a 10 MYr delay.  'stellarcluster' calls tracksn to actually
!  subtract the star out of the cloud.
!
!***********************************************************************
MODULE explosions
!
!  Common block for supernova quantities.
!
  USE params
  implicit none
  integer snseed,ctime,wtime,lbdist,mbdist,nbdist,turnon
  integer ylen,taulen,lmsid,plmsid,mhms,mlms, genevelifelen, genevemasslen, &
       genevelifePh1len, genevemassPh1len,nSN, cont, ntot,n_stars,ntot_io

  logical do_expl, do_delayexpl, do_lmsf, do_psft, do_asym,do_initialSNE, &
       do_cluster, do_conv, do_falgarone,do_ionizeclump, verbose

  real &
    dtsn,sntime,psftime,         &
    esn,dsn,wsn,psn,             &
    Tlimit,rholim,convlim,fvesc, &
    theta_eject, width_eject,    &
    totalstellarmass, tSN,SNtemp,&
    epsilon,rhoav,masslim,       &
    mass_cluster, bet,wstar,      &
    mass_min_cluster,convlim2

  character (len=70) yfile, taufile, genevelifefile, genevemassfile, &
       genevelifePh1file, genevemassPh1file, datadir

  integer, allocatable, dimension (:,:) :: hmspos
  integer, allocatable, dimension (:,:) :: lmspos
  integer, allocatable, dimension (:,:) :: dumpos
  real, allocatable, dimension(:,:) :: hms
  real, allocatable, dimension(:,:) :: lms
  real, allocatable, dimension(:,:) :: dum
  real, allocatable, dimension(:,:) :: tautbl
  real, allocatable, dimension(:,:) :: ytbl
  real, allocatable, dimension(:,:,:) :: temp
  real, allocatable, dimension(:,:) :: genevemasstbl
  real, allocatable, dimension(:,:) :: genevemassPh1tbl
  real, allocatable, dimension(:,:) :: genevelifetbl
  real, allocatable, dimension(:,:) :: genevelifePh1tbl
  integer,allocatable,dimension(:,:) :: postot
  integer,allocatable,dimension(:,:) :: pos_io
real, public:: bbnsII

CONTAINS

!***********************************************************************
SUBROUTINE write_stars
  USE params
  implicit none
  integer :: i,lrec   
  real,allocatable,dimension(:,:) :: hmstot
  character(len=mfile) fname, name
  character(len=mid), save:: id='write_stars: $Id: stars_ones.f90,v 1.75 2012/09/26 15:06:00 aake Exp $'

  call print_trace (id, dbg_expl, 'BEGIN')

 print *,'isnap',isnap

  if (mhms.gt.0) then
     allocate(hmstot(mhms,22))                 !19 from hms, 3 from hmspos
     hmstot(:,1:19)=hms
     hmstot(:,20:22)=hmspos
  endif
  fname = name('nstars.dat','nst',file)
  open (11, file=fname, status='unknown', form='unformatted', &
    access='direct', recl=lrec(2))
  write (11,rec=isnap+1) isnap+1, mhms    !isnap+1 because isnap must be change (isnap=isnap+1) later
  close(11)

  fname = name('hmstot.dat','hmst',file)
  if (mhms.gt.0) then
     open (11, file=fname, &
          status='unknown', form='unformatted', &
          access='direct', recl=lrec(mhms*22))
     print *,'isnap+1',isnap+1
     write (11,rec=isnap+1) hmstot
     close(11)
     deallocate(hmstot)
  else
     open (11, file=fname, &
          status='unknown', form='unformatted', &
          access='direct', recl=lrec(1))
     write (11,rec=isnap+1) 0.
     close(11)
  endif

! i = index(file,'.')                     
!  open(1,file=file(1:i)//'stars',status='replace')
!  write(1,*) mhms,t 
!  write(1,*) hms
!  write(1,*) hmspos
!  close(1)

  call print_trace (id, dbg_expl, 'END')
  return
END subroutine

!***********************************************************************
SUBROUTINE read_stars
   USE params
  implicit none

  logical exists
  integer :: i,lrec
  real :: t1
  real,allocatable,dimension(:,:) :: hmstot
  character(len=mfile) fname, name
  character(len=mid), save:: id='read_stars: $Id: stars_ones.f90,v 1.75 2012/09/26 15:06:00 aake Exp $'


  call print_trace (id, dbg_expl, 'BEGIN')

fname = name('nstars.dat','nst',from)
inquire (file=fname,exist=exists)
if (exists) then 
  open (11, file=fname, &
  status='old', form='unformatted', &
  access='direct', recl=lrec(2))
  print *,' isnap,mhms :',iread,mhms
!  if (iread.eq.0) then

!  if (iread.eq.-1) then
!     read(11,rec=iread+2) i, mhms
!  else
!     read(11,rec=iread) i, mhms
!  endif


  if (do_initialSNE) then
     if (iread.eq.-1) then
        read(11,rec=iread+2) i, mhms
     else
        read(11,rec=iread+1) i, mhms
     endif   
  else
     read(11,rec=iread) i, mhms
  endif
print * ,'iread,i,mhms',iread,i,mhms
  close(11)
  if (mhms.ge.1) then
     allocate(hmstot(mhms,22))
     fname = name('hmstot.dat','hmst',from)
     open (12, file=fname, &
     status='old', form='unformatted', &
     access='direct', recl=lrec(mhms*22))
     print *,'isnap+1',iread+1
!     if (iread.eq.0) then
!        read(12,rec=iread+1) hmstot
!     if (iread.eq.-1) then
!        read(12,rec=iread+2) hmstot
!     else
!        read(12,rec=iread) hmstot
!     endif   
     if (do_initialSNE) then
        if (iread.eq.-1) then
           read(12,rec=iread+2) hmstot
        else
           read(12,rec=iread+1) hmstot
        endif
     else
        read(12,rec=iread) hmstot
     endif

     close(12)
     allocate(hms(mhms,19))
     allocate(hmspos(mhms,3))
     hms=hmstot(:,1:19)
     hmspos=hmstot(:,20:22)  
     deallocate(hmstot)
  endif 
  write (*,*) 'Data of ',mhms,' massive stars recovered at snapshot ',i
else
  write (*,*) 'NO data of massive stars to be recovered'   

endif 

!  i = index(from,'.')                     
!  inquire (file=from(1:i)//'stars',exist=exists)
!  if (exists) then
!     open(1,file=from(1:i)//'stars',status='old')
!     read(1,*) mhms,t1
!     allocate(hms(mhms,19))
!     allocate(hmspos(mhms,3))
!     read(1,*) hms
!     read(1,*) hmspos
!     write (*,*) 'Data of ',mhms,' massive stars recovered at time ',t1  
!     close(1)
!  else     
!     write (*,*) 'NO data of massive stars to be recovered'  
!  endif

  call print_trace (id, dbg_expl, 'END')
  return
END subroutine 
!***********************************************************************
SUBROUTINE initial_SNE

  USE params
  implicit none
  
  integer i,iz,l,m,n
  integer, allocatable, dimension(:,:,:):: onesart
  real ran1s,rn1,rn2,rn3,alpha,p,imfk,imfm,pmass,rn 
  real, allocatable, dimension (:,:,:) :: rhoart, pxart, pyart, pzart, eart, dart
  character(len=mid), save:: id='initial_SNE: $Id: stars_ones.f90,v 1.75 2012/09/26 15:06:00 aake Exp $'


  call print_trace (id, dbg_expl, 'BEGIN')

  allocate (onesart(mx,my,mz), rhoart(mx,my,mz), pxart(mx,my,mz))
  allocate (pyart(mx,my,mz), pzart(mx,my,mz), eart(mx,my,mz), dart(mx,my,mz))

  if (do_trace) print *,'allocate done'
!XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
!$omp parallel do private(iz)
  do iz=1,mz
     rhoart(:,:,iz)=1.
     pxart(:,:,iz)=0.
     pyart(:,:,iz)=0.
     pzart(:,:,iz)=0.
     eart(:,:,iz)=0.
     dart(:,:,iz)=0.
     onesart(:,:,iz)=0
  enddo

  if (do_trace) print *,'init done'
  alpha = 2.35            ! parameters for calculating SN progenitor masses 
  p     = -1./(alpha-1.)
  imfk  = 0.058375757
  imfm  = 1.995262E-03
  do i=1,nSN                ! form nSN stars to be exploted (randomly)
                            ! in the time interval (0,tSN) million years
      if (do_trace) print *,'loop',i
!XXXXXXXXXXXXXXXXXXXXXXXXXXXXrandom numbers
      rn=ran1s(snseed)
      rn1=ran1s(snseed)
      rn2=ran1s(snseed)
      rn3=ran1s(snseed)
      if (rn1*mx.eq.0) rn1=1.
      if (rn2*mx.eq.0) rn2=1.
      if (rn3*mx.eq.0) rn3=1.
 
      pmass=(rn*imfk+imfm)**p
      l=int(rn1*mx)
      m=int(rn2*my)
      n=int(rn3*mz)  
      onesart(l,m,n)=2
      rhoart(l,m,n)=rhoart(l,m,n)*(1.+epsilon/(1.-epsilon))

      if (do_trace) print *,'call tracksn'
      call tracksn(rhoart,pxart,pyart,pzart,eart,dart,pmass,l,m,n,onesart)
!XXXXXXXXXXXXXXXXXXXXXXXXXXXXXrandom positioning
      l=int(rn1*mx)
      m=int(rn2*my)
      n=int(rn3*mz)  
      onesart(l,m,n)=0
      rn=ran1s(snseed) 
      hms(i,1)=t+rn*tSN       !Explosion in interval (t,tSN)
      hms(i,12)=0.
      hms(i,17)=0.
      hmspos(i,1)=rn1*mx
      rn=ran1s(snseed)
      hmspos(i,2)=rn2*my
      rn=ran1s(snseed)
      hmspos(i,3)=rn3*mz
      hms(i,8)=0.
      hms(i,9)=0.
      hms(i,10)=0.  
      print *,' STAT. FOR THE INITIAL SNe ',i
      print *, 'Explosion time, iexp,jexpl,zexpl, mass'
      print *, hms(i,1), hmspos(i,1),hmspos(i,2),hmspos(i,3), pmass 
      print *, 'velocidad',hms(i,8),hms(i,9),hms(i,10) 
 enddo
  if (do_trace) print *,'call write_stars'
  call write_stars 
  deallocate(hms)
  deallocate(hmspos)

  deallocate (onesart, rhoart, pxart)
  deallocate (pyart, pzart, eart, dart)

  call print_trace (id, dbg_expl, 'END')
  return
END SUBROUTINE  

!************************************************************
SUBROUTINE tracksn (rho,px,py,pz,e,d,progm,li,mi,ni,ones)

  USE params
  USE units
  !USE stagger
  implicit none

  integer :: i, j, l, m, n, ll, mm, nn, itau, maxtry, li, mi, ni, lf, mf, nf
  integer, dimension (mx,my,mz) :: ones 
  real, dimension (mx,my,mz) :: rho, px, py, pz, e, d
  real, allocatable, dimension (:,:,:) :: scr, eold, dold, prof, uxc, uyc, uzc
  double precision msub, cmass, cenergy, cmetal
  real :: elimit, lnmass, dwidth, arg, ampl, &
          prerho0, postrho0, lifetime, expltime, stau, a0, a1, xx, yy, zz, logph,   &
          x(mx), y(my), z(mz), progm,  ln10,a1M,a0M, &
          totallostM,remainingM,Ph1lifetime,Ph1totallostM, initialVinf, rho1, &
          dd2,xx0,yy2,zz2
  real, save:: totmass                                                  ! make sure global
  real, allocatable,dimension(:) :: a,b 
  real, dimension(mx):: arg1, exp1
  logical debug
  character(len=mid), save:: id='tracksn: $Id: stars_ones.f90,v 1.75 2012/09/26 15:06:00 aake Exp $'

  call print_trace (id, dbg_expl, 'BEGIN')

  allocate (scr(mx,my,mz), eold(mx,my,mz), dold(mx,my,mz), prof(mx,my,mz), &
            uxc(mx,my,mz), uyc(mx,my,mz), uzc(mx,my,mz))

  maxtry = 20 !dealing with spheres with radius basically between (wstar,wstar+maxtry.*0.5)
  elimit = Tlimit/utemp
  ln10 = 2.3025851

  !uxc = xup(px)/rho   ! u_x in pc/Myr
  !uyc = yup(py)/rho   ! u_y in pc/Myr
  !uzc = zup(pz)/rho   ! u_z in pc/Myr

  call xup_set_par(px,uxc)
  call yup_set_par(py,uyc)
  call zup_set_par(pz,uzc)
  !$omp parallel do private(l,m,n,rho1)
!XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
  do n=1,mz
  do m=1,my
  do l=1,mx
    rho1=1./rho(l,m,n)
    uxc(l,m,n)=uxc(l,m,n)*rho1
    uyc(l,m,n)=uyc(l,m,n)*rho1
    uzc(l,m,n)=uzc(l,m,n)*rho1
  end do
  end do
  end do
  call print_trace (id, dbg_expl, 'vel')


  ! stellar mass substraction: if successfully removed it will be re-inserted
  ! in the explosion stage in order to avoid too high central temperatures. 
  ! If stellarwind is present, then hms(i,15) has to be used instead  cmass because
  ! part of the stellar mass would be given back to the surrounding

  cmass   = 0.0
  cenergy = 0.0
  cmetal  = 0.0  

if (do_cluster .and. t.gt.0.) then !now progm refers to the mass of individual stars.
                                   ! t.gt.0. because of if there are artificial initial SNe 
                                    

 dwidth=1.5*wstar*dx !Let's begin to collect stellar mass using a sphere with small radius
                     !If the sphere is small the radius is enlarged.

!XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX???
    do j=1,mx
      x(j) = (j-mx/2)*dx
    end do
    do j=1,my
      y(j) = (j-my/2)*dy
    end do
    do j=1,mz
      z(j) = (j-mz/2)*dz
    end do


 do i=1,maxtry
    dd2=(1./dwidth)**2
    xx0=x(mx/2)
    !$omp parallel do private(l,m,n,ll,mm,nn,yy2,zz2,arg,arg1,exp1)
!XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
    do nn=1,mz
      n=mod(ni-(nn-mz/2)+mz-1,mz)+1
      zz2=(z(n)-z(mz/2))**2
      do mm=1,my
        m=mod(mi-(mm-my/2)+my-1,my)+1
        yy2=(y(m)-y(my/2))**2
        do ll=1,mx
          l=mod(li-(ll-mx/2)+mx-1,mx)+1
          arg=((x(l)-xx0)**2+yy2+zz2)*dd2
          arg1(ll)=-arg*arg*arg
        end do
        call expn(mx,arg1,exp1)
!        prof(:,mm,nn)=rho(:,mm,nn)*exp1            ! gaussian-like profile x rho         
        prof(:,mm,nn)=0.9*rho(:,mm,nn)*exp1             ! modified profile to avoid cells with very low density 
      end do
    end do

    call print_trace (id, dbg_expl, 'prof')
    call average_subr (prof,totmass)
    call print_trace (id, dbg_expl, 'aver')
    totmass=totmass*(mx*dx)*(my*dy)*(mz*dz)
    !totmass=sum(prof)*dx*dy*dz
    ampl=msun*progm/(cmu*totmass)
    if (debug(dbg_expl)) print *,i,dwidth,ampl
    if (ampl .lt. 1.0) then
      prerho0=rho(li,mi,ni)
      !$omp parallel do private(n)
!XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
      do n=1,mz
        eold(:,:,n)=e(:,:,n)
        scr(:,:,n)=rho(:,:,n)-ampl*prof(:,:,n)                 ! subtract the stellar mass
        e(:,:,n)=e(:,:,n)*(scr(:,:,n)/rho(:,:,n))              ! conserve local temperature
        if (do_pscalar) then
          dold(:,:,n)=d(:,:,n)
          d(:,:,n)=d(:,:,n)*(scr(:,:,n)/rho(:,:,n))            ! conserve local metallicity
        end if
      end do

      !$omp parallel do private(l,m,n), reduction(+:msub,cenergy,cmetal)
!XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
      do n=1,mz
      do m=1,my
        do l=1,mx
          msub=msub+(rho(l,m,n)-scr(l,m,n))
          cenergy=cenergy+(eold(l,m,n)-e(l,m,n))
          rho(l,m,n)=scr(l,m,n)                                ! finally, make it real
        end do
        if (do_pscalar) then
          do l=1,mx
            cmetal=cmetal+(dold(l,m,n)-d(l,m,n))
          end do
        end if
      end do
      end do

      msub=msub*(dx*dy*dz)
      cenergy=cenergy*(dx*dy*dz)
      if (do_pscalar) then
        cmetal=cmetal*(dx*dy*dz)
      end if
      !msub=sum(rho-scr)*dx*dy*dz                               ! msub sholud be equal to cmass
      cmass=msun*progm/cmu                                     ! total amount of mass
      !cenergy=sum(eold-e)*dx*dy*dz                             ! total amount of energy
      !cmetal=sum(dold-d)*dx*dy*dz                              ! total amount of metals
      postrho0=rho(li,mi,ni)
      call print_trace (id, dbg_expl, 'adjust')
      exit
    else if (i .lt. maxtry) then
      dwidth=1.5*wstar*10.**(i/real(maxtry))*dx     
    end if
  end do
  call print_trace (id, dbg_expl, 'ntry')

else  !form one massive star only in every clump
!XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
  do n=1,mz
    do m=1,my
      do l=1,mx
        if (ones(l,m,n).eq.2) then
           cmass  =cmass+epsilon*rho(l,m,n)*dx*dy*dz
           cenergy=cenergy+epsilon*e(l,m,n)*dx*dy*dz
           rho(l,m,n)=rho(l,m,n)*(1.-epsilon)
           e(l,m,n)=e(l,m,n)*(1.-epsilon)        ! conserve local temperature
           if (do_pscalar) then
             cmetal =cmetal +epsilon*d(l,m,n)*dx*dy*dz
             d(l,m,n)=d(l,m,n)*(1.-epsilon)        ! conserve local metalicity
           end if
        endif
      enddo
    enddo
  enddo

endif 

!print *,'COMPARACION DE MASAS (en cmu): ',msun*progm/cmu,cmass  
!print *,'COMPARACION DE MASAS (en Mo): ',progm,cmass*cmu/msun
 
    if (cmass .eq. 0.0) then
    msub=0.0
    ampl=0.0
    prerho0=rho(li,mi,ni)
    postrho0=prerho0
    print *,'Warning: Mass substraction failed!'
  end if
  
if (progm .ge. 8.) then   !Save data of only massive stars

  if (progm .ge. 10.) then                       ! estimating no. ionizing photons Myr-1 (x10^49)
    logph=0.65*alog(progm-9.25)+12.25            ! note: logph_real = logph + 49   
    logph=logph-0.188*alog(progm+29.905)+0.677   ! subtraction of He-ionizing photons
  else                                           ! bad fit below M = 10 M_o
    logph=12.        
  end if

  ! If using  Meynet table (don't calculate log(M) now!!)
  
  allocate(a(genevelifelen))
  a=0.
  where(genevelifetbl(:,1).le.progm) a=genevelifetbl(:,1)
  do itau=1,genevelifelen
     if (genevelifetbl(itau,1).eq.maxval(a)) exit
  end do
  deallocate(a)
  
  a1 = (genevelifetbl(itau+1,2)-genevelifetbl(itau,2))/ &
       (genevelifetbl(itau+1,1)-genevelifetbl(itau,1))
  a0 = genevelifetbl(itau,2)-a1*genevelifetbl(itau,1)

  lifetime = a1*progm+a0                 ! life time of star in Myrs
  expltime = t+lifetime                  ! time at explosion (Myrs)


  a1M = (genevemasstbl(itau+1,2)-genevemasstbl(itau,2))/ &
       (genevemasstbl(itau+1,1)-genevemasstbl(itau,1))
  a0M = genevemasstbl(itau,2)-a1M*genevemasstbl(itau,1)
  
  totallostM = a1M*progm+a0M                  ! expeled mass during all its life (solar masses)
  remainingM = progm - totallostM             ! expeled mass in the SN explosion (solar masses) 
 

! Calculate duration of first phase

  
  a1 = (genevelifePh1tbl(itau+1,2)-genevelifePh1tbl(itau,2))/ &
       (genevelifePh1tbl(itau+1,1)-genevelifePh1tbl(itau,1))
  a0 = genevelifePh1tbl(itau,2)-a1*genevelifePh1tbl(itau,1)

  Ph1lifetime = a1*progm+a0                 ! Phase 1 of the star in Myrs
  

! Calculate total lost mass during the life of the star. 


  a1M = (genevemassPh1tbl(itau+1,2)-genevemassPh1tbl(itau,2))/ &
       (genevemassPh1tbl(itau+1,1)-genevemassPh1tbl(itau,1))
  a0M = genevemassPh1tbl(itau,2)-a1M*genevemassPh1tbl(itau,1)
  
  Ph1totallostM = a1M*progm+a0M                  ! expeled mass during all its life (solar masses)


! calculate final cell position

  lf = modulo(nint(uxc(li,mi,ni)*lifetime/dx) + li,mx)
  mf = modulo(nint(uyc(li,mi,ni)*lifetime/dy) + mi,my)
  nf = modulo(nint(uzc(li,mi,ni)*lifetime/dz) + ni,mz)
  if (lf .eq. 0) lf = mx
  if (mf .eq. 0) mf = my
  if (nf .eq. 0) nf = mz

  ! place star in hms array
  allocate(dum(mhms+1,19))
  allocate(dumpos(mhms+1,3))
  if (mhms .gt. 0) then
    dum(1:mhms,:)=hms
    dumpos(1:mhms,:)=hmspos
    deallocate(hms,hmspos)
  end if 

  do i=mhms,1,-1
    if (expltime .ge. dum(i,1)) exit
    do j=1,19                         ! note: no. of entries in hms array (allocate)
      dum(i+1,j)=dum(i,j)
    end do
    do j=1,3
      dumpos(i+1,j)=dumpos(i,j)
    end do
  end do

  dum(i+1,1) = expltime
  dum(i+1,2) = progm
  dum(i+1,3) = cmass
  dum(i+1,4) = cenergy
  dum(i+1,5) = cmetal
  dum(i+1,6) = logph
  dum(i+1,7) = t-dtsn
  dum(i+1,8) = uxc(li,mi,ni)
  dum(i+1,9) = uyc(li,mi,ni)
  dum(i+1,10)= uzc(li,mi,ni)
  dum(i+1,11)= (3.99e-3*exp(ln10*logph/3.)*rhoav**(-0.6666667))**2.
  dum(i+1,12) = totallostM
  dum(i+1,13) = itau
  dum(i+1,14) = lifetime
  dum(i+1,15) = progm      !Here we'll place the mass in each time step
  dum(i+1,16) = Ph1lifetime
  dum(i+1,17) = Ph1totallostM
  dum(i+1,18) = 0.         !Here we'll place Vinf in each time step
  dum(i+1,19) = 0.         !Total Energy Input due to stellar wind

!  dumpos(i+1,1) = lf  !Para TEST ponemos inicial...CAMBIARLO LUEGO
!  dumpos(i+1,2) = mf  !el bueno es el de arriba!
!  dumpos(i+1,3) = nf  !El de abajo (dejar las estrellas fijas) es para test
  dumpos(i+1,1) = li
  dumpos(i+1,2) = mi
  dumpos(i+1,3) = ni 
 
 
  allocate(hms(mhms+1,19))
  allocate(hmspos(mhms+1,3))
  hms=dum
  hmspos=dumpos
  deallocate(dum,dumpos)
  mhms=mhms+1            ! a high-mass star has formed 
  totalstellarmass=totalstellarmass+progm

  if (t.gt.0)  call write_hms(li,mi,ni,rho,progm,lifetime,expltime, &
               totalstellarmass,totallostM,Ph1lifetime,Ph1totallostM)


  print *, 'Mass (M_o):.............................', progm
  print *, 'Total expeled mass (M_o):...............', totallostM
  print *, 'Life time (Myrs):.......................', lifetime
  print *, 'Time at explosion (Myrs):...............', expltime
  print *, 'Initial position:.......................', li,mi,ni 
  print *, 'Final position:.........................', lf,mf,nf
  print *, 'Velocity (pc/Myr):......................', uxc(li,mi,ni),uyc(li,mi,ni),uzc(li,mi,ni)
  print *, 'Pre-formation central density (cdu):....', prerho0 
  print *, 'Subtracted mass (cmu):..................', msub
  print *, 'Post-formation central density (cdu):...', postrho0 
  print *, 'Weighting function amplitude:...........', ampl 
  print *, 'No. existing high-mass stars:...........', mhms 
 
  call oflush
endif
  deallocate (scr, eold, dold, prof, uxc, uyc, uzc)
   
  call print_trace (id, dbg_expl, 'END')
  return
END SUBROUTINE



!************************************************************
subroutine lmsf (rho,px,py,pz,ee,d,f,elimit,rholim,convlim)

  USE params
  !USE stagger
  USE units
  implicit none

  integer i,j,ip,jp,kp,nos,li,ui,iz
  real, dimension(mx,my,mz) :: rho, px, py, pz, ee, d, scr
  real, allocatable, dimension(:,:,:) :: ux,uy,uz,lnr,conv
  real dlms(7), dpos(4), rotime
  real f,elimit,rholim,convlim,rest,ran1s,rnd,ftemp,tlim,elim,rlim,clim,expf
  real :: tdp, rdp, ddp
  character(len=mid), save:: id='lmsf: $Id: stars_ones.f90,v 1.75 2012/09/26 15:06:00 aake Exp $'


  call print_trace (id, dbg_expl, 'BEGIN')

  nos=floor(f)
  rest=f-real(nos)
  rnd=ran1s(snseed)
  if (rnd .le. rest) nos=nos+1

  if (.not. do_psft) then

    do i=1,nos
      call snpose(rho,px,py,pz,ee,ip,jp,kp,elimit,rholim,convlim)
      if (do_ionization) then
        ftemp=temp(ip,jp,kp)
      else
        ftemp=utemp*ee(ip,jp,kp)
      end if
      lmsid=lmsid+1
      write(*,*) 'Low-mass star: id time density metals temperature x y z'
      write(*,*) lmsid,t,rho(ip,jp,kp),d(ip,jp,kp),ftemp,ip,jp,kp
      tdp=t
      rdp=rho(ip,jp,kp)
      if (do_pscalar) then
        ddp=d(ip,jp,kp)
      else
        ddp=0.
      end if
      call write_lms(lmsid,ip,jp,kp,tdp,rdp,ddp,.false.)
    end do

  else

    if (mlms.eq.0 .and. nos.eq.0) then
      call print_trace (id, dbg_expl, 'END')
      return
    end if
    
    allocate(ux(mx,my,mz),  &
             uy(mx,my,mz),  &
             uz(mx,my,mz),  &
             lnr(mx,my,mz), &
             conv(mx,my,mz))
!-----------------------------------------------------------------------
    !ux = px/exp(xdn(lnr))
    !uy = py/exp(ydn(lnr))
    !uz = pz/exp(zdn(lnr))

    !$omp parallel do private(iz)
!XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
    do iz=1,mz
      lnr(:,:,iz) = alog(rho(:,:,iz))
    end do
    call xdn_set_par(lnr,ux)
    call ydn_set_par(lnr,uy)
    call zdn_set_par(lnr,uz)
    !$omp parallel do private(iz)
!XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
    do iz=1,mz
      ux(:,:,iz) = px(:,:,iz)/exp(ux(:,:,iz))
      uy(:,:,iz) = py(:,:,iz)/exp(uy(:,:,iz))
      uz(:,:,iz) = pz(:,:,iz)/exp(uz(:,:,iz))
    end do
    if (do_2nddiv) then
      !conv = -(ddxup1(ux) + ddyup1(uy) + ddzup1(uz))/sqrt(rho)
      call ddxup1_set_par(ux,conv)
      call ddyup1_add_par(uy,conv)
      call ddzup1_add_par(uz,conv)
    else
      !conv = -(ddxup (ux) + ddyup (uy) + ddzup (uz))/sqrt(rho) 
      call ddxup_set_par(ux,conv)
      call ddyup_add_par(uy,conv)
      call ddzup_add_par(uz,conv)
    end if
    !$omp parallel do private(iz)
!XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
    do iz=1,mz
      conv(:,:,iz) = -conv(:,:,iz)/sqrt(rho(:,:,iz))
    end do
!-----------------------------------------------------------------------

    deallocate(lnr)

    if (mlms .gt. 0) then               ! is there any existing proto-stars?
      rotime=5.*psftime
      do i=mlms,1,-1
        ip = modulo(nint(lms(i,3)*(t-lms(i,2))/dx) + lmspos(i,1),mx)
        jp = modulo(nint(lms(i,4)*(t-lms(i,2))/dy) + lmspos(i,2),my)
        kp = modulo(nint(lms(i,5)*(t-lms(i,2))/dz) + lmspos(i,3),mz)
        if (ip .eq. 0) ip = mx
        if (jp .eq. 0) jp = my
        if (kp .eq. 0) kp = mz
        expf=exp(lms(i,1)/psftime)      ! assume exponential time evolution for the sf-conditions 
        rlim=rholim/expf
        clim=expf*convlim
        if (do_ionization) then  
          tlim=expf*Tlimit
          if (temp(ip,jp,kp).lt.tlim.and.rho(ip,jp,kp).ge.rlim.and.conv(ip,jp,kp).gt.clim) then 
            lms(i,1)=lms(i,1)+2*dtsn     
          else
            lms(i,1)=-1.
            dlms=lms(i,:)
            lms(i:mlms-1,:)=lms(i+1:mlms,:)
            lms(mlms,:)=dlms
            dpos=lmspos(i,:)
            lmspos(i:mlms-1,:)=lmspos(i+1:mlms,:)
            lmspos(mlms,:)=dpos 
          end if
        else
          elim=expf*elimit
          if (ee(ip,jp,kp).lt.elim.and.rho(ip,jp,kp).ge.rlim.and.conv(ip,jp,kp).gt.clim) then
            lms(i,1)=lms(i,1)+2*dtsn     
          else
            lms(i,1)=-1.
            dlms=lms(i,:)
            lms(i:mlms-1,:)=lms(i+1:mlms,:)
            lms(mlms,:)=dlms
            dpos=lmspos(i,:)
            lmspos(i:mlms-1,:)=lmspos(i+1:mlms,:)
            lmspos(mlms,:)=dpos
          end if
        end if
      end do            
      
      do i=mlms,1,-1                    ! stars that failed to form
        if (lms(i,1) .gt. 0.) exit
        lmsid=lmsid+1
        ip=lmspos(i,1)
        jp=lmspos(i,2)
        kp=lmspos(i,3)                       
        call write_lms(lmsid,ip,jp,kp,lms(i,2),lms(i,6),lms(i,7),.false.)
      end do
  
      ui=i
   
      do i=1,mlms                       ! stars that succeded to form
        if (lms(i,1) .le. rotime) exit
        lmsid=lmsid+1
        ip=lmspos(i,1)
        jp=lmspos(i,2)
        kp=lmspos(i,3)
        call write_lms(lmsid,ip,jp,kp,lms(i,2),lms(i,6),lms(i,7),.true.)
      end do      
     
      li=i
      mlms=ui-li+1                      ! current number of existing proto-stars
       
      if (mlms .gt. 0) then
        allocate(dum(mlms,7),dumpos(mlms,3))
        dum=lms(li:ui,:)
        dumpos=lmspos(li:ui,:)
        deallocate(lms,lmspos)
        allocate(lms(mlms,7),lmspos(mlms,3)) 
        lms=dum
        lmspos=dumpos
        deallocate(dum,dumpos)
      else
        deallocate(lms,lmspos)
      end if    
    end if
    deallocate(conv)  
   
    if (nos .gt. 0) then                ! is there any newly formed proto-stars?
      allocate(dum(mlms+nos,7),dumpos(mlms+nos,3))
      if (mlms .gt. 0) then
        dum(1:mlms,:)=lms
        dumpos(1:mlms,:)=lmspos
        deallocate(lms,lmspos)
      end if
          
      do i=mlms,1,-1                    ! place newly formed proto-stars in lms array
        if (dum(i,1) .ge. dtsn) exit
        dum(i+nos,:)=dum(i,:)
        dumpos(i+nos,:)=dumpos(i,:)
      end do
    
      ui=i

      call xup_set_par(ux,scr)
      !$omp parallel do private(iz)
!XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
      do iz=1,mz
        ux(:,:,iz) = scr(:,:,iz)
      end do
      call yup_set_par(uy,scr)
      !$omp parallel do private(iz)
!XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
      do iz=1,mz
        uy(:,:,iz) = scr(:,:,iz)
      end do
      call zup_set_par(uz,scr)
      !$omp parallel do private(iz)
!XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
      do iz=1,mz
        uz(:,:,iz) = scr(:,:,iz)
      end do

      !ux=xup(ux)
      !uy=yup(uy)
      !uz=zup(uz)
    
      do i=1,nos
        call snpose(rho,px,py,pz,ee,ip,jp,kp,elimit,rholim,convlim)
        dum(i+ui,1)=dtsn
        dum(i+ui,2)=t
        dum(i+ui,3)=ux(ip,jp,kp)
        dum(i+ui,4)=uy(ip,jp,kp)
        dum(i+ui,5)=uz(ip,jp,kp)
        dum(i+ui,6)=rho(ip,jp,kp)
        if (do_pscalar) then
          dum(i+ui,7)=d(ip,jp,kp)
        else
          dum(i+ui,7)=0.
        end if
        dumpos(i+ui,1)=ip
        dumpos(i+ui,2)=jp
        dumpos(i+ui,3)=kp
        if (do_ionization) then
          ftemp=temp(ip,jp,kp)
        else
          ftemp=utemp*ee(ip,jp,kp)
        end if
        plmsid=plmsid+1
        write(*,*) 'Low-mass star: id time density metals temperature x y z'
        write(*,*) plmsid,t,rho(ip,jp,kp),dum(i+ui,7),ftemp,ip,jp,kp
      end do
    
      allocate(lms(mlms+nos,7),lmspos(mlms+nos,3))
      lms=dum
      lmspos=dumpos
      deallocate(dum,dumpos)
       
      mlms = mlms+nos                   ! nos proto-stars have formed 
    end if
    deallocate(ux,uy,uz) 
  end if  

  call print_trace (id, dbg_expl, 'END')
  return
end subroutine  
    


!************************************************************
  subroutine snpose (rho,px,py,pz,ee,ldist,mdist,ndist,elimit,rholim,convlim)

!  Find a SN position, with probability prop. to density, in
!  the subvolume with ee < elimit
!
!  980607/aake: Coded
!  981023/aake: Coded

  USE params
  !USE stagger
  USE units
  implicit none

  integer ldist,mdist,ndist
  real elimit, rholim, convlim, ran1s

  real, dimension(mx,my,mz):: rho, px, py, pz, ee
  real, allocatable, dimension(:,:,:) :: lnr, ux, uy, uz, conv
  real,dimension(mx*my*mz) :: scr1,scr2
  real, allocatable, dimension(:) :: scr3
  integer i, l, m, n,j,iz
  integer ii(1),ip,ml
  real rnd,a 

  integer nk,h,k
  integer*2, external:: icompare
  character(len=mid), save:: id='snpose: $Id: stars_ones.f90,v 1.75 2012/09/26 15:06:00 aake Exp $'


  call print_trace (id, dbg_expl, 'BEGIN')

  allocate(ux(mx,my,mz),  &
           uy(mx,my,mz),  &
           uz(mx,my,mz),  &
           lnr(mx,my,mz), &
           conv(mx,my,mz))

!-----------------------------------------------------------------------
  !lnr = alog(rho)
  !ux = px/exp(xdn(lnr))
  !uy = py/exp(ydn(lnr))
  !uz = pz/exp(zdn(lnr))
  !if (do_2nddiv) then
    !conv = -(ddxup1(ux) + ddyup1(uy) + ddzup1(uz))/sqrt(rho) 
  !else
    !conv = -(ddxup (ux) + ddyup (uy) + ddzup (uz))/sqrt(rho)
  !end if

    !$omp parallel do private(iz)
!XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
    do iz=1,mz
      lnr(:,:,iz) = alog(rho(:,:,iz))
    end do
    call xdn_set_par(lnr,ux)
    call ydn_set_par(lnr,uy)
    call zdn_set_par(lnr,uz)
    !$omp parallel do private(iz)
!XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
    do iz=1,mz
      ux(:,:,iz) = px(:,:,iz)/exp(ux(:,:,iz))
      uy(:,:,iz) = py(:,:,iz)/exp(uy(:,:,iz))
      uz(:,:,iz) = pz(:,:,iz)/exp(uz(:,:,iz))
    end do

    if (do_2nddiv) then
      !conv = -(ddxup1(ux) + ddyup1(uy) + ddzup1(uz))/sqrt(rho)
      call ddxup1_set_par(ux,conv)
      call ddyup1_add_par(uy,conv)
      call ddzup1_add_par(uz,conv)
    else
      !conv = -(ddxup (ux) + ddyup (uy) + ddzup (uz))/sqrt(rho) 
      call ddxup_set_par(ux,conv)
      call ddyup_add_par(uy,conv)
      call ddzup_add_par(uz,conv)
    end if
    !$omp parallel do private(iz)
!XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
    do iz=1,mz
      conv(:,:,iz) = -conv(:,:,iz)/sqrt(rho(:,:,iz))
    end do
!-----------------------------------------------------------------------

! make selection 

  k=1
  if (do_ionization) then
!XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
   do n=1,mz
    do m=1,my
      do l=1,mx
        if (temp(l,m,n).lt.Tlimit.and.rho(l,m,n).gt.rholim.and.conv(l,m,n).gt.convlim) then
          scr1(k)=rho(l,m,n)
          k=k+1
        endif
      enddo
    enddo
   enddo
  else
!XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
   do n=1,mz
    do m=1,my
      do l=1,mx
!        if (ee(l,m,n).lt.elimit.and.rho(l,m,n).gt.rholim.and.conv(l,m,n).gt.convlim) then
        if (ee(l,m,n).lt.elimit.and.rho(l,m,n).gt.rholim) then
          scr1(k)=rho(l,m,n)
          k=k+1
        endif
      enddo
    enddo
   enddo
  end if

  nk=k-1

!XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX????
  allocate(scr3(nk))
  do j=1,nk
     scr3(j)=scr1(j)
  enddo
   
  if (nk < 1) then
    print *,'WARNING, nk=0 in stars.f90'
    deallocate (lnr,ux,uy,uz,conv)
    ldist=1
    mdist=1
    ndist=1
    call print_trace (id, dbg_expl, 'END')
    return
  else if (idbg > 0) then
 !   print *,'sorting ',nk
  end if


! sort the density



!call qsort (scr3,nk,4,icompare)

! New sorting. The program used to crash (surprisingly) here when the mass substraction 
! failed in subroutine tracksn and snpose was called from there. 

  do ip=1,nk
    ii=minloc(scr3(ip:nk))
    ml=ii(1)
    a=scr3(ip)
    scr3(ip)=scr3(ml+ip-1)
    scr3(ml+ip-1)=a
  enddo 


! integrate the mass
!XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX??????
  scr2(1)=0.
  do k=2,nk
       scr2(k)=scr2(k-1)+scr3(k-1)
  enddo


! Choose a density with probability proportional to density
!XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXrandom positioning
  rnd=ran1s(snseed)
  rnd=ran1s(snseed)         ! roll a few extra times, to avoid
  rnd=ran1s(snseed)         ! peculiar first value.

  call binsearch (scr2,nk,rnd*scr2(nk),i)
  print *,'  #Fraction..........:',rnd,scr2(i)/scr2(nk)

! find the point (this is crude :-) and now also ugly!!!(bvg) 
! and it was incorrect several times over :-( / aake:
!  1. ee and scr1 must refer to the same point => sort after selection!
!  2. nk=k is wrong, since k=k+1 after last value computed
!  3. cannot just use double precision; exps() and qsort() expect real
!  4. finding the point was done incorrectly; scr1 was wiped out

  if (do_ionization) then
!XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
   do n=1,mz
    do m=1,my
      do l=1,mx
!        if (temp(l,m,n).lt.Tlimit.and.conv(l,m,n).gt.convlim .and. rho(l,m,n).eq.scr1(i)) then
!         if (temp(l,m,n).lt.Tlimit .and. rho(l,m,n).eq.scr1(i)) then
          if (temp(l,m,n).lt.Tlimit .and. rho(l,m,n).eq.scr3(i)) then
          ldist=l
          mdist=m
          ndist=n
          deallocate(lnr,ux,uy,uz,conv)
          deallocate(scr3)
          call print_trace (id, dbg_expl, 'END')
          return
        endif
      enddo
    enddo
   enddo
  else
!XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
   do n=1,mz
    do m=1,my
      do l=1,mx
!        if (ee(l,m,n).lt.elimit.and.conv(l,m,n).gt.convlim .and. rho(l,m,n).eq.scr1(i)) then
!        if (ee(l,m,n).lt.elimit .and. rho(l,m,n).eq.scr1(i)) then
         if (ee(l,m,n).lt.elimit .and. rho(l,m,n).eq.scr3(i)) then

          ldist=l
          mdist=m
          ndist=n
          deallocate(lnr,ux,uy,uz,conv)
          deallocate(scr3)
          call print_trace (id, dbg_expl, 'END')
          return
        endif
      enddo
    enddo
   enddo
  end if

  print *,'snpose: should never get here!'
  stop

  end subroutine

!**********************************************************************
  subroutine binsearch (f,n,f0,i)

!  Binary search in monotonically increasing f(1:n).  No checks.
!  aake/980923

  USE PARAMS
  USE units
  implicit none
  
  integer :: i1, i2, n, i
  real f(n), f0

  i1=1
  i2=n
100   continue
    i=(i1+i2)/2
    if (f(i) .lt. f0) then
      i1=i
    else
      i2=i
    endif
  if (i1 .lt. i2-1) goto 100
  i=i1

  end subroutine

!************************************************************************
  subroutine SNexpl (rho,e,d,sntype,progm,ldist,mdist,ndist,rampl,eampl,dampl,m_add)

  USE params
  USE units
  implicit none

  integer sntype, ldist, mdist, ndist, ll, mm, nn, l, m, n, i
  real, dimension(mx,my,mz) :: rho, e, d
  real, allocatable, dimension(:,:,:) :: lnrho, scr1, prof_expl, prof_shell, prof_met,prof_mass
  real, allocatable, dimension(:,:,:) :: prof_asym
  real esn1, dsn1, dpsn, arg, arg1, arg2, argd, ex, exd, ex1, ex2, prof1, prof2, profd
  real v_expl, v_shell, v_met, v_asym, e_0, d_0, cmass, SNe, dwidth, dwidth1, dwidth2,dwidth_wsn,wsn2,f_wsn,massprof,err
  real totm, totm2, totm3, tote, tote2, cee, totd, totd2, cdd, cutoff, dwidthd, progm, mu
  real xx, yy, zz, snlnr0, snlnr1, snrho0, snrho1, eemax, radius, omax, dcen,m_add,sum_r_exp
  real x(mx), y(my), z(mz), x0, y0, z0, theta, phi, omega_eject, ran1s,SNm,inf_edge,sup_edge
  real, optional :: rampl, eampl, dampl
  character(len=mid), save:: id='SNexpl: $Id: stars_ones.f90,v 1.75 2012/09/26 15:06:00 aake Exp $'


  call print_trace (id, dbg_expl, 'BEGIN')

  if (idbg.gt.5) print *,'pos=',ldist,mdist,ndist
  
  allocate (lnrho(mx,my,mz), scr1(mx,my,mz), prof_expl(mx,my,mz), &
            prof_shell(mx,my,mz), prof_met(mx,my,mz), prof_mass(mx,my,mz))

!XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX?????
  do i=1,mx
    x(i) = (i-mx/2)*dx
  end do
  do i=1,my
    y(i) = (i-my/2)*dy
  end do
  do i=1,mz
    z(i) = (i-mz/2)*dz
  end do
  
  
  print *,'M_ADD (In Mo), rampl  ',m_add,rampl
 
  m_add=m_add*(msun/cmu)                       !M_add in code units  
  rampl=m_add                                  !the mass given back to the surrounding 
  mu = 2.5                                     !WHERE DID THIS COME FROM BEFORE?? /Aake
  SNm=2.*mu*(mproton*(1.d51/cmu))/kB/SNtemp/3. !Mass (in code units) needed for distributing 1.d51 erg
                                               !and getting a constant Temperature of SNtemp Kelvin
  
  err=1.    

  if (do_asym) then
    allocate(prof_asym(mx,my,mz))
    omax  = 1.25*pi               ! if omax .eq. 0. no cavity in central cell
!XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXrandom positioning
    theta = pi*ran1s(snseed)
    phi   = 2*pi*ran1s(snseed)
    theta = theta+theta_eject 
    x0    = sin(theta)*cos(phi)
    y0    = sin(theta)*sin(phi)
    z0    = cos(theta)
  endif  

  dwidth = wsn*dx       ! generic width
  dwidth1= 1.5*dwidth   ! inner shell width 
  dwidth2= 2.0*dwidth   ! outer shell width
  cutoff = 1.0E-03      ! profile cutoff in order to avoid fictious ultra metal-poor stars
  dpsn   = 1.25*psn     ! decreasing the base line for the metallicity distr. (true when cutoff < 0.5) 
  dwidthd= (wsn-0.5)*dx ! further decreasing the base line 
  inf_edge=1.
  sup_edge=mx*1.e0
  wsn2=inf_edge+(sup_edge-inf_edge)/2.


do while (err>0.01)
  dwidth_wsn=wsn2*dx
!XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
  do n=1,mz
    zz=z(n)-z(mz/2)
    nn=ndist+(n-mz/2)
    if (nn .gt. mz) then
      nn=nn-mz
    else if (nn .le. 0) then
      nn=nn+mz
    endif
    do m=1,my
      yy=y(m)-y(my/2)
      mm=mdist+(m-my/2)
      if (mm .gt. my) then
        mm=mm-my
      else if (mm .le. 0) then
        mm=mm+my
      endif
      do l=1,mx
        xx=x(l)-x(mx/2)
        ll=ldist+(l-mx/2)
        if (ll .gt. mx) then
          ll=ll-mx
        else if (ll .le. 0) then
          ll=ll+mx
        endif
        radius=sqrt(xx**2+yy**2+zz**2)
        arg=radius/dwidth_wsn                       ! normalized radius
        ex=exp(max(-35.,min(35.,psn*(arg-1.))))     ! psn=steepness
        prof_expl(ll,mm,nn)=1./(1.+ex**2)           ! tanh()-like profile in log e 
        prof_mass(ll,mm,nn)=prof_expl(ll,mm,nn)*rho(ll,mm,nn)
      end do
    end do
  end do

  call average_subr (prof_mass,massprof)
  massprof=massprof*(mx*dx)*(my*dy)*(mz*dz)
!  massprof=sum(prof_mass)*dx*dy*dz
  f_wsn=massprof+m_add-SNm
  if (f_wsn > 0) then
     sup_edge=wsn2
  else 
     inf_edge=wsn2
  endif
  err=abs(f_wsn)
  wsn2=inf_edge+(sup_edge-inf_edge)/2.
  print *,'EDGES of wsn2 ,wsn2  ',inf_edge,sup_edge, wsn2 
  print *,'massprof, SNm, f_wsn  ',massprof,SNm,f_wsn  
  print *,'SNm en Mo   ', SNm*(cmu/msun)
  print *,'massprof en Mo   ', massprof*(cmu/msun)
  print *,'rampl en Mo  y en code units ', rampl*(cmu/msun),rampl
  print *,'error-->    ',err
enddo   !of WHILE
  print *,'WSN',wsn2
!XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
  do n=1,mz
    zz=z(n)-z(mz/2)
    nn=ndist+(n-mz/2)
    if (nn .gt. mz) then
      nn=nn-mz
    else if (nn .le. 0) then
      nn=nn+mz
    endif
    do m=1,my
      yy=y(m)-y(my/2)
      mm=mdist+(m-my/2)
      if (mm .gt. my) then
        mm=mm-my
      else if (mm .le. 0) then
        mm=mm+my
      endif
      do l=1,mx
        xx=x(l)-x(mx/2)
        ll=ldist+(l-mx/2)
        if (ll .gt. mx) then
          ll=ll-mx
        else if (ll .le. 0) then
          ll=ll+mx
        endif
        radius=sqrt(xx**2+yy**2+zz**2)
        arg=radius/dwidth                           ! normalized radius
        arg1=arg*(dwidth/dwidth1)
        arg2=arg*(dwidth/dwidth2)
        argd=arg*(dwidth/dwidthd)
        ex=exp(max(-35.,min(35.,psn*(arg-1.))))     ! psn=steepness
        exd=exp(max(-35.,min(35.,dpsn*(argd-1.))))
        ex1=exp(max(-35.,min(35.,psn*(arg1-1.))))
        ex2=exp(max(-35.,min(35.,psn*(arg2-1.))))
        prof1=1./(1.+ex1**2)
        prof2=1./(1.+ex2**2)
        profd=1./(1.+exd**2)
!        prof_expl(ll,mm,nn)=1./(1.+ex**2)           ! tanh()-like profile in log e 
!        prof_mass(ll,mm,nn)=prof_expl(ll,mm,nn)*rho(ll,mm,nn)
        prof_shell(ll,mm,nn)=prof2-prof1
        if (profd .lt. cutoff) then                  
          prof_met(ll,mm,nn)=0.
          if (do_asym) prof_asym(ll,mm,nn)=0.
        else
          prof_met(ll,mm,nn)=profd 
          if (do_asym) then
            if (radius .gt. 0.) then
              omega_eject=acos((xx*x0+yy*y0+zz*z0)/radius)  ! ang. between point - expl. center and ej. vector
            else 
              omega_eject=omax                              ! central cell is hollowed if omax .ne. 0. 
            end if
            prof_asym(ll,mm,nn)=profd*exp(-(omega_eject/width_eject)**2)
          end if
        end if        
      end do
    end do
  end do

  call average_subr (prof_met,v_met)
  call average_subr (prof_expl,v_expl)
  call average_subr (prof_shell,v_shell)
  v_met=v_met*(mx*dx)*(my*dy)*(mz*dz)
  v_expl=v_expl*(mx*dx)*(my*dy)*(mz*dz)
  v_shell=v_shell*(mx*dx)*(my*dy)*(mz*dz)

!  v_met=sum(prof_met)*dx*dy*dz                      ! normalization factor
!  v_expl=sum(prof_expl)*dx*dy*dz                    
!  v_shell=sum(prof_shell)*dx*dy*dz

  if (do_asym) then     
     call average_subr (prof_asym,v_asym)
     v_asym=v_asym*(mx*dx)*(my*dy)*(mz*dz)
  endif   
!  if (do_asym) v_asym=sum(prof_asym)*dx*dy*dz
                                                    ! Normalization factors
  SNe=1.d51/ceu                                     ! standard supernova energy
  e_0=(esn*SNe)/v_expl                              ! esn = expl. energy in foe

  scr1=rho                        ! save original rho distr. in scr1
  snrho0=rho(ldist,mdist,ndist)
  eemax=e_0/snrho0
  if (do_ionization) then
    print*,'  #Pre-expl temp.............:',temp(ldist,mdist,ndist)
  else
    print*,'  #Pre-expl temp.............:',utemp*e(ldist,mdist,ndist)/snrho0
  end if
  print*,'  #Pre-expl density..........:',snrho0
  if (do_pscalar) then
    print*,'  #Pre-expl metallicity......:',d(ldist,mdist,ndist)/snrho0
  end if

!Fix: if do_ionization = .true. re-calculate temperature for printed output!!!

  if (do_delayexpl) then
!   rampl = rampl/v_met
    
    call average_subr (rho*prof_expl,sum_r_exp)
    sum_r_exp=sum_r_exp*(mx*dx)*(my*dy)*(mz*dz)*(cmu/msun)
!    print *,'MASA    (en Mo)', sum(rho*prof_expl)*dx*dy*dz*(cmu/msun)
    print *,'Masa (en Mo)',sum_r_exp
    print *,'RAMPL',rampl
    rampl = rampl/(v_expl*1.d0)
    eampl = eampl/v_met
    dampl = dampl/v_met
    print *,'RHO+RAMPL',sum(rho),'+',sum(rampl*prof_expl)
!    rho = rho+rampl*prof_met      ! add back the stellar mass 
    
    rho = rho*1.d0+rampl*(prof_expl*1.d0)
    print *,'HA DE SER IGUAL A:  ', sum(rho), sum(rho)-sum(rampl*(prof_expl*1.d0))
    print *,'SUMA   ',sum(rampl*prof_expl)*dx*dy*dz*(cmu/msun),rampl*v_expl*(cmu/msun)
    print *,'MASA  despues de anadir  (en Mo)', sum(rho*prof_expl)*dx*dy*dz*(cmu/msun)
    e = e+eampl*prof_met          ! add back the energy
    if (do_pscalar) then
      d = d+dampl*prof_met        ! add back the metals
    end if
    scr1=rho                      ! save original (new) rho distr. in scr1
    snrho0=rho(ldist,mdist,ndist)
    eemax=e_0/snrho0     
    if (do_ionization) then
      print*,'  #Pre-expl SN temp..........:',temp(ldist,mdist,ndist)
    else
      print*,'  #Pre-expl SN temp..........:',utemp*e(ldist,mdist,ndist)/snrho0
    end if
    print*,'  #Pre-expl SN density.......:',snrho0
    if (do_pscalar) then
      print*,'  #Pre-expl SN metallicity...:',d(ldist,mdist,ndist)/snrho0
    end if
  end if

  

  ! If (and only if) the central temperature would be too small, make a cavity
  if (eemax.lt.1.e7/utemp) then   ! central temperature test
    eemax=1.e7/utemp
    snrho1=e_0/eemax
    snlnr1=alog(snrho1)
    snlnr0=alog(snrho0)
    call average_subr (rho,totm)
    totm=totm*(mx*dx)*(my*dy)*(mz*dz)
    call average_subr (e,tote)
    tote=tote*(mx*dx)*(my*dy)*(mz*dz)

!    totm=sum(rho)*dx*dy*dz
!    tote=sum(e)*dx*dy*dz
    if (do_pscalar) then
       call average_subr (d,totd)
       totd=totd*(mx*dx)*(my*dy)*(mz*dz)
!       totd=sum(d)*dx*dy*dz
    else
      totd=0.
    end if
    lnrho=alog(rho)
    lnrho=lnrho+(snlnr1-snlnr0)*prof_expl
    rho=exp(lnrho)
    e=e*(rho/scr1)               ! conserve local temperature

    call average_subr (rho,totm2)
    totm2=totm2*(mx*dx)*(my*dy)*(mz*dz)
    call average_subr (e,tote2)
    tote2=tote2*(mx*dx)*(my*dy)*(mz*dz)
    
!    totm2=sum(rho)*dx*dy*dz      ! compute new total mass
!    tote2=sum(e)*dx*dy*dz        ! copute new total energy 
    if (do_pscalar) then
      d=d*(rho/scr1)             ! conserve local metallicity
      call average_subr (d,totd2)
      totd2=totd2*(mx*dx)*(my*dy)*(mz*dz)
!      totd2=sum(d)*dx*dy*dz      ! compute new total abundance
      
    else
      totd2=0.
    end if
    cmass=(totm-totm2)/v_shell
    cee=(tote-tote2)/v_shell
    cdd=(totd-totd2)/v_shell
    rho=rho+cmass*prof_shell     ! Add the missing cavity mass back, in a shell.
    e=e+cee*prof_shell           ! Add the missing cavity energy back
    if (do_pscalar) then
      d=d+cdd*prof_shell         ! Add the missing cavity abundance back 
    end if
    totm3=cmass*v_shell

    if (idbg.gt.5) then
      print *,'totm:',totm-totm2,totm3
      print *,'tote:',tote,sum(e)*dx*dy*dz
      print *,'totd:',totd,sum(d)*dx*dy*dz
    end if

    esn1=0.                      ! ngrs: deal with e now that the rho profile is fixed
    e=e+e_0*prof_expl            ! finally add SN energy
    esn1=e_0*v_expl

    if (idbg.gt.5) then
      print *,'tote:',tote,sum(e)*dx*dy*dz
    end if

  else                           ! central temperature test    

    totm3=0.                     ! in case no cavity is needed
!    e=e+e_0*prof_expl
    e=e+1./utemp*prof_expl*rho*SNtemp
    print *,'ENERGIA TOTAL AL MEDIO (en erg) ',sum(1./utemp*prof_expl*rho*SNtemp)*ceu*dx**3
    print *,'MASA DONDE METEMOS ENERGIA AL MEDIO (en Mo) ',sum(prof_expl*rho)*dx**3*(cmu/msun)
    esn1=e_0*v_expl   

  endif                          ! central temperature test              

  if (do_pscalar) then               ! Finally, add also the newly synthesized metals
    if (do_asym) then                ! prof_met => symetric ejection; prof_asym => asymetric ejection 
      call yield(d_0,v_asym,progm) 
      dcen=d(ldist,mdist,ndist)+d_0  ! dcen is set to be d+d_0 both for sym. and asym. ej.
      d=d+d_0*prof_asym    
      dsn1=d_0*v_asym
      deallocate(prof_asym)
    else
      call yield(d_0,v_met,progm)
      dcen=d(ldist,mdist,ndist)+d_0  ! dcen is set to be d+d_0 both for sym. and asym. ej.
      d=d+d_0*prof_met    
      dsn1=d_0*v_met
    end if    
    if (idbg.gt.5) then
      print *,'totd:',totd,sum(d)*dx*dy*dz-totd,dsn1
    end if
  end if

  if (ndist.le.0.or.ndist.gt.mz) then
    print *,'ERROR: ndist=',ndist
    stop
  endif
  
  xx=x(ldist)                    !  Center of explosion, for printout
  yy=y(mdist)
  zz=z(ndist)
  
  print*,'  #SN-type...................:',sntype 
  if (do_pscalar) then
    print*,'  #SN mass (M_o).............:',progm
  end if
  print*,'  #Indices...................:',ldist,mdist,ndist
  print*,'  #Position..................:',xx,yy,zz
  print*,'  #Energy (foe)..............:',esn1/SNe
  if (do_pscalar) then
    print*,'  #Metals (M_o)..............:',dsn1*(cmu/msun)
  end if
  print*,'  #Ejection vector...........:',x0,y0,z0
  print*,'  #Shell mass (M_o)..........:',totm3*(cmu/msun)
  print*,'  #Time (Myrs)...............:',t
  print*,'  #Amplitude (ceu clu-3).....:',e_0
  if (do_ionization) then
    print*,'  #Post-expl temp (MK)........:',1.e-6*temp(ldist,mdist,ndist)
  else  
    print*,'  #Post-expl temp (MK)........:',1.e-6*utemp*e(ldist,mdist,ndist)/rho(ldist,mdist,ndist)
  end if
  print*,'  #Post-expl density.........:',rho(ldist,mdist,ndist)
  if (do_pscalar) then
    print*,'  #Post-expl metallicity.....:',dcen/rho(ldist,mdist,ndist) 
  end if
  print '(a,i2,1pe13.5,3i4,2(1pe10.2))','   #SN-Log.:',sntype,t,ldist,mdist,ndist,rho(ldist,mdist,ndist),totm3*(cmu/msun)
  
  deallocate (lnrho, scr1, prof_expl, prof_shell, prof_met, prof_mass)

  call print_trace (id, dbg_expl, 'END')
  return
END SUBROUTINE

!************************************************************************
SUBROUTINE yield (dnorm,volume,m)

! Given a certain supernova mass the yield is interpolated from a yield table.

USE params
USE units
implicit none

integer :: i
real :: dnorm, y_X, a0, a1, m, volume
  character(len=mid), save:: id='yield: $Id: stars_ones.f90,v 1.75 2012/09/26 15:06:00 aake Exp $'

  call print_trace (id, dbg_expl, 'BEGIN')
  
  if (m.lt.ytbl(1,1)) then
    y_X=ytbl(1,2)
  else if (m.ge.ytbl(1,1).and.m.lt.ytbl(ylen,1)) then
    yloop: do i=2,ylen
      if (m.ge.ytbl(i-1,1).and.m.lt.ytbl(i,1)) then
        a1=(ytbl(i,2)-ytbl(i-1,2))/(ytbl(i,1)-ytbl(i-1,1))
        a0=ytbl(i,2)-a1*ytbl(i,1)
        y_X=a1*m+a0
        exit yloop
      end if
    end do yloop
  else
    y_X=ytbl(ylen,2)
  end if
  
  dnorm=(y_X*msun/cmu)/volume          ! y_X = ejected yield in solar masses
!  dnorm=(dsn*msun/cmu)/volume          ! dsn = ejected yield in solar masses  
 
  call print_trace (id, dbg_expl, 'END')
  return
END SUBROUTINE

!***********************************************************************
SUBROUTINE write_lms (id,i,j,k,tf,rf,df,flag)
!-----------------------------------------------------------------------
!Write id, density, metal content, time, and position for the cell where 
!a low-mass star is born.
!-----------------------------------------------------------------------

  USE params
  USE units
  implicit none
  
  integer i,j,k,id
  logical flag
  real :: mass2num,tf,rf,df
  character(len=mfile) fname, name
  character(len=mid), save:: jd='write_lms: $Id: stars_ones.f90,v 1.75 2012/09/26 15:06:00 aake Exp $'

  call print_trace (jd, dbg_expl, 'BEGIN')
  
  mass2num=exp(lnfehsun) 
   
  fname = name('lowmstars.dat','lms',file)
  open(2,file=fname, position='append', status='unknown')
  if (flag) then
    write(2,110) id,tf,rf,df,mass2num,i,j,k
    110 format(I5,ES10.3,ES10.3,ES11.3,F8.3,I4,I4,I4)
  else
    write(2,120) id,tf,rf,df,mass2num,i,j,k
    120 format(I5,ES10.3,ES10.3,ES11.3,F8.3,I4,I4,I4)
  end if    
  close(2)

  call print_trace (jd, dbg_expl, 'END')
END SUBROUTINE


!*****************************************************************************
subroutine find_star(rho,px,py,pz,e,d,elimit,rholim,convlim,massconversion,ncl,ones)
  USE params
  !USE stagger
  USE units
  USE eos
  implicit none

  integer ldist,mdist,ndist,new_starting_cell
  real elimit, rholim, convlim, protomass,massconversion,rhomax,distan,dis, &
       Mrms_clump,meanM_clump,meanrho_clump,n0,cs_clump,L1,L2,L3,mag1,mag2,mag3
  real,dimension(mx,my,mz):: rho, px, py, pz,e,d
  integer,allocatable, dimension(:,:) :: posnew2,posa,posb,postotb,pos_dis
  integer,allocatable,dimension(:)::pos1,pos2,pos3 
  real,allocatable,dimension(:,:,:):: ee,lnr, ux, uy, uz, conv, ones_step
  integer i, j, k, l, m, n, li, mi, ni,iz,ik
  integer nk,h,new2,newk,k2,ncl,iflag,jflag,kflag
  integer,dimension(mx,my,mz):: ones
  character*10 strncl
  real,allocatable,dimension(:) :: v_clump,rho_clump
  character(len=mfile) fname, name
  character(len=mid), save:: id='find_star: $Id: stars_ones.f90,v 1.75 2012/09/26 15:06:00 aake Exp $'

  call print_trace (id, dbg_expl, 'BEGIN')
  iflag=0
  jflag=0
  kflag=0

allocate(ee(mx,my,mz))
 !$omp parallel do private(k)
!XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
  do k=1,mz
    ee(:,:,k) = e(:,:,k)/rho(:,:,k)
  end do

!-----------------------------------------------------------------------
    if (do_conv) then
      allocate(ux(mx,my,mz),  &
               uy(mx,my,mz),  &
               uz(mx,my,mz),  &
               lnr(mx,my,mz), &
               conv(mx,my,mz))
      !$omp parallel do private(iz)
!XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
      do iz=1,mz
        lnr(:,:,iz) = alog(rho(:,:,iz))
      end do
      call xdn_set_par(lnr,ux)
      call ydn_set_par(lnr,uy)
      call zdn_set_par(lnr,uz)
      !$omp parallel do private(iz)
!XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
      do iz=1,mz
        ux(:,:,iz) = px(:,:,iz)/exp(ux(:,:,iz))
        uy(:,:,iz) = py(:,:,iz)/exp(uy(:,:,iz))
        uz(:,:,iz) = pz(:,:,iz)/exp(uz(:,:,iz))
      end do
      if (do_2nddiv) then
        call ddxup1_set_par(ux,conv)
        call ddyup1_add_par(uy,conv)
        call ddzup1_add_par(uz,conv)
      else
        call ddxup_set_par(ux,conv)
        call ddyup_add_par(uy,conv)
        call ddzup_add_par(uz,conv)
      end if
      !$omp parallel do private(iz)
!XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
      do iz=1,mz
        conv(:,:,iz) = -conv(:,:,iz)/sqrt(rho(:,:,iz))
      end do
    end if
!-----------------------------------------------------------------------

! see which is the maximum rho value in the cells where 'ones' is equal
! 1. The idea is that we check which is the maximum value in the different 
! clumps. At first we have a lot of clumps with value of 1, but as soon we start
! deciding if these clumps are protostars/clusters or not we convert these 1 in 2 if 
! that clump will be a star/cluster or 0 if not...

  rhomax=0.
!XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
  do n=1,mz
    do m=1,my
      do l=1,mx
         if (do_conv) then 
            if (ee(l,m,n).lt.elimit.and.rho(l,m,n).ge.rholim.and.ones(l,m,n).eq.1.and.conv(i,j,k).gt.convlim) then 
               rhomax=amax1(rhomax,rho(l,m,n))
            endif
         else   
            if (ee(l,m,n).lt.elimit.and.rho(l,m,n).ge.rholim.and.ones(l,m,n).eq.1) then 
               rhomax=amax1(rhomax,rho(l,m,n))
            endif
         endif   
      enddo
    enddo
  enddo

if (rhomax.eq.0.)  print *,"estas jodido" 


!Value 2 in 'ones' to the cell that has the maximum rho and that satisfice
! the criterium. It would be the places where we start to find connected cells.


new_starting_cell=0
!XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
do n=1,mz
  do m=1,my
    do l=1,mx
       if (new_starting_cell .eq. 0) then 
          if (do_conv) then
             if (ee(l,m,n).lt.elimit.and.rho(l,m,n).eq.rhomax.and.ones(l,m,n).eq.1.and.conv(i,j,k).gt.convlim) then
                ones(l,m,n)=2
                new2=1       !The cell from which we start to search.
                if (new_starting_cell .gt. 0) deallocate(posnew2,postot)
                allocate(posnew2(1,3))
                allocate(postot(1,3))
                posnew2(1,1)=l
                posnew2(1,2)=m
                posnew2(1,3)=n
                ldist=l
                mdist=m
                ndist=n
                postot(1,:)=posnew2(1,:)
                new_starting_cell=1
             endif
          else   
             if (ee(l,m,n).lt.elimit.and.rho(l,m,n).eq.rhomax.and.ones(l,m,n).eq.1) then
                ones(l,m,n)=2
                new2=1       !The cell from which we start to search.
                if (new_starting_cell .gt. 0) deallocate(posnew2,postot)
                allocate(posnew2(1,3))
                allocate(postot(1,3))
                posnew2(1,1)=l
                posnew2(1,2)=m
                posnew2(1,3)=n
                ldist=l
                mdist=m
                ndist=n
                postot(1,:)=posnew2(1,:)
                new_starting_cell=1
             endif
          endif   
       endif   
    enddo
  enddo
enddo
ntot=1 
allocate(postotb(1,3))
postotb(1,1)=postot(1,1)
postotb(1,2)=postot(1,2)
postotb(1,3)=postot(1,3)

!As far as we continue finding connected cells.... (new2>0)

protomass=rhomax
do while (new2>0)
   newk=0
   do k2=1,new2     !Loop on the very recent found cells
!XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXFew positions
      li=posnew2(k2,1)
      mi=posnew2(k2,2)
      ni=posnew2(k2,3)
!XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXFew positions
      do n=-1,1
         do m=-1,1
            do l=-1,1
               i=li+l
               j=mi+m
               k=ni+n
               if (i.eq.0) then
                  i=mx        !If the sum lies on the edges of the box
                  iflag=1
               endif   
               if (i.eq.mx+1) i=1
               if (j.eq.0) then
                  j=mx
                  jflag=1
               endif   
               if (j.eq.mx+1) j=1
               if (k.eq.0) then
                  k=mx
                  kflag=1
               endif   
               if (k.eq.mx+1) k=1
               if (ones(i,j,k).eq.1) then  !Avoid to include cells with value 0 (not satisfice the criterium)
                                           !and 2 (cells already found) 
                  newk=newk+1              !Next connected cell
                  protomass=protomass+rho(i,j,k)
                  ones(i,j,k)=2
                  ntot=ntot+1              !Array with all the postions with value 2 in "ones"
                  deallocate(postot)
                  allocate(postot(ntot,3))
                  postot(ntot,1)=i
                  postot(ntot,2)=j
                  postot(ntot,3)=k
                  postot(1:ntot-1,:)=postotb(:,:)
                  deallocate(postotb)
                  allocate(postotb(ntot,3))
                  postotb(:,:)=postot(:,:)
                  if (newk.gt.1) deallocate(posa) !Array with the NEW positions with value 2 in "ones"
                  allocate(posa(newk,3))
                  posa(newk,1)=i
                  posa(newk,2)=j
                  posa(newk,3)=k
                  if (newk.gt.1) then 
                     posa(1:newk-1,:)=posb(:,:)
                     deallocate(posb)
                  endif
                  allocate(posb(newk,3))
                  posb(:,:)=posa(:,:)
               else
                  if (ones(i,j,k).eq.0) ones(i,j,k)=4 !For the 0's in 'ones' that are those that are 
                                                      !the edges or interiors cells 
                                                      !that not satisfices the R-T criterium 
               endif
            enddo
         enddo
      enddo
   enddo

!Assign the new posnew2 array with the new locations (in case there are...)

   new2=newk
   if (new2.gt.0) then
      deallocate(posnew2)
      allocate(posnew2(new2,3))
      posnew2(:,:)=posb(:,:)
      deallocate(posa)
      deallocate(posb)
   endif
enddo  !Enddo of WHILE

deallocate(posnew2)


!Calculate the total mass 

protomass=protomass*massconversion   

!Calculate size of the clump as the average of the maximum distance 
!in each direction L=(L1+L2+L3)/3. The algorithm might be wrong for structures 
!that have a direction larger than Sx/2. However, it shouldn't
!be the case of having such a large structrures.


      distan=0.
      allocate(pos_dis(ntot,3))
      allocate(pos1(ntot))
      allocate(pos2(ntot))
      allocate(pos3(ntot))
      pos_dis(:,:)=postot(:,:)
      if (iflag .eq. 1) then
!         print *,"iflag1"
         do ik=1,ntot 
!XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX????
            if (pos_dis(ik,1) .ge. 1 .and. pos_dis(ik,1) .le. mx/2) then
               pos_dis(ik,1)=pos_dis(ik,1)+mx
!               print *,pos_dis(ik,1)
            endif
         enddo
      endif
      if (jflag .eq. 1) then
!         print *,"iflag2"
!XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX????
         do ik=1,ntot
            if (pos_dis(ik,2) .ge. 1 .and. pos_dis(ik,2) .le. my/2)  pos_dis(ik,2)=pos_dis(ik,2)+my
         enddo
      endif
      if (kflag .eq. 1) then
!         print *,"iflag3"
!XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX??????
         do ik=1,ntot 
            if (pos_dis(ik,3) .ge. 1 .and. pos_dis(ik,3) .le. mz/2)  pos_dis(ik,3)=pos_dis(ik,3)+mz
         enddo
      endif
!XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX?????
      do ik=1,ntot 
         pos1(ik)=pos_dis(ik,1)
         pos2(ik)=pos_dis(ik,2)
         pos3(ik)=pos_dis(ik,3)
      enddo

      L1=0.
      L2=0.
      L3=0.

!XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX?????
      do ik=1,ntot 
         do j=ik+1,ntot                 
            mag1=pos1(ik)-pos1(j)               
            L1=max(abs(mag1)+1.,L1)
            mag2=pos2(ik)-pos2(j)       
            L2=max(abs(mag2)+1.,L2)
            mag3=pos3(ik)-pos3(j)               
            L3=max(abs(mag3)+1.,L3)
         enddo
      enddo

      distan=(L1+L2+L3)/3.*dx      

      deallocate (pos_dis, pos1, pos2, pos3)    

!******************************************************
!New version of the subroutine where we can form a cluster
!(do_cluster) from the clump or just create a massive star from
!the clump
!******************************************************

 
!ntot= number of cell in the clump
!postot= array with dimensions ntot x 3 that stores the 
!coordinates of all cells that are in the clump
 
 if (do_cluster) then 
    if (ntot .gt. 8) then        !Key point--> Only consider those clumps with at least 8 cells
       allocate(v_clump(ntot))
       allocate(rho_clump(ntot))
!XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX??????
       do i=1,ntot
          l=postot(i,1)
          m=postot(i,2)
          n=postot(i,3)
          v_clump(i)=px(l,m,n)/rho(l,m,n)
          cs_clump=0.25                           ! Key point-->Assume that sound speed in clump~0.25Km/s
         rho_clump(i)=rho(l,m,n)
       enddo
       meanM_clump=sum(v_clump(:)/cs_clump)/ntot
       Mrms_clump=sqrt(sum((v_clump(:)/cs_clump-meanM_clump)**2)/ntot)    
       meanrho_clump=sum(rho_clump(:))/ntot
       n0=meanrho_clump/2.5/1.6726        !mean number of H2 in clump assuming mu=2.5 
       print *,'Clump number,ntot,Mrms,n0,protomass,size'
       print *,ncl,ntot,Mrms_clump,n0,protomass,distan
       
       fname=name('cluster.dat','clus',file)
       open (12, file=fname,status='unknown',position='append')
       write (12,114) ntot*1.,protomass,distan,Mrms_clump,n0,t
114    format(ES10.3,ES10.3,ES10.3,ES10.3,ES10.3,ES10.3) 
       close(12)
       deallocate(v_clump,rho_clump)
       if ((.not. do_falgarone) .or. (n0*2. .le. 2.2e3*distan**(-1))) then   !Key point-->Only the clumps in falagarone region 
          print *,"call stellar cluster"
          call stellarcluster(rho,px,py,pz,e,d, protomass*epsilon, &
               ldist,mdist,ndist,ones,Mrms_clump,n0)
       else
          print *,"The clump is not inside the falagarone region"
       endif 
    endif
 else
    if (protomass*epsilon.gt.8. .and. protomass*epsilon.lt.100.) then 
       call tracksn(rho,px,py,pz,e,d, protomass*epsilon,ldist,mdist,ndist,ones)
    else 
       if (protomass*epsilon .ge. 100.) print *,'BIG STAR.... WHAT SHOULD I DO?'
    endif
 endif


!write(strncl,'(I10)'),ncl
!XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX?????
do i=1,ntot               ! Put 0 in the cells 
   l=postot(i,1)
   m=postot(i,2)
   n=postot(i,3)
!   close(5)
!   open(5,file=strncl, position='append', status='unknown')
!   write(5,105) l,m,n
!105 format (I,I,I)
!   close(5)
   ones(l,m,n)=0
enddo   

if (do_ionizeclump) then

   pos_io(ntot_io+1:ntot_io+ntot,1)=postot(:,1)
   pos_io(ntot_io+1:ntot_io+ntot,2)=postot(:,2)
   pos_io(ntot_io+1:ntot_io+ntot,3)=postot(:,3)
   ntot_io=ntot_io+ntot

endif
cont=cont-ntot            !Number of cells for the next search.

deallocate(postot,postotb)

!*******************************************************************************
!In case of cluster formation a new search of ones must be done to avoid some problems
!when creating stars because rho,e (thus ee) are modified inside when removing mass and energy to 
!form new stars. With the current method that uses more than one cell to take the mass 
!to form the star the original ones calculated before entering the first time in this 
!routine might be sligthly modify. As this routine is called very few times it´s not globally  
!"expensive"
!******************************************************************************

if (do_cluster) then
allocate(ones_step(mx,my,mz))
!$omp parallel do private(iz)
!XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
   do iz=1,mz                       
      ones_step (:,:,iz)=0
   enddo

    if (do_ionization) then
      allocate (temp(mx,my,mz))
      !$omp parallel do private(k,j,i), reduction(+:cont)
!XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
      do k=1,mz
        call eos2 (rho(:,:,k),ee(:,:,k),tt=temp(:,:,k))
        do j=1,my
          if (do_conv) then
            do i=1,mx
              if (temp(i,j,k).lt.Tlimit.and.rho(i,j,k).ge.rholim.and.conv(i,j,k).gt.convlim) then
                 ones_step(i,j,k)=1
              end if
            end do
          else
            do i=1,mx
              if (temp(i,j,k).lt.Tlimit.and.rho(i,j,k).ge.rholim) then
                 ones_step(i,j,k)=1
              end if
            end do
          end if
        end do
      end do  
    else
      !$omp parallel do private(k,j,i), reduction(+:cont)
!XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
      do k=1,mz
        do j=1,my
          if (do_conv) then
            do i=1,mx
              if (ee(i,j,k).lt.elimit.and.rho(i,j,k).ge.rholim.and.conv(i,j,k).gt.convlim) then
                 ones_step(i,j,k)=1
              end if
            end do
          else
            do i=1,mx
              if (ee(i,j,k).lt.elimit.and.rho(i,j,k).ge.rholim) then
                 ones_step(i,j,k)=1
              end if
            end do
          end if
        end do
      end do  
    end if
    if (do_conv) deallocate(lnr,ux,uy,uz,conv)

    !$omp parallel do private(k,j,i)
!XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
    do k=1,mz
       do j=1,my
          do i=1,mx
             if (ones(i,j,k).eq.1) then
                if (ones(i,j,k)-ones_step(i,j,k).ne.0) then
                   ones(i,j,k)=0
                   cont=cont-1
!                   print *,"ones has been reduced"
                endif
             endif
          enddo
       enddo
    enddo
    deallocate(ones_step)
    
endif
deallocate(ee)
if (cont.eq.0 .and. do_ionizeclump) then  !Input energy to get 10^4K
   print *,"ionize the original clump"
   do i=1,ntot_io
      l=pos_io(i,1)
      m=pos_io(i,2)
      n=pos_io(i,3)
      e(l,m,n)=e(l,m,n)+(1.e4*rho(l,m,n)/utemp-e(l,m,n))
   enddo
endif

      
  call print_trace (id, dbg_expl, 'END')
END SUBROUTINE
!************************************************************************
SUBROUTINE stellarcluster(rho,px,py,pz,e,d,mass_c, &
            ldist,mdist,ndist,ones,mrms,n0)


real :: mrms,n0,mass_c,c1,c2,int0,A_c, &
     rnd2,progmass,imfk,imfm,p,mass_stars_cluster,M_max,v_max,int1, &
     xrandom,m1,m2,m_add,ran1s
real, dimension(mx,my,mz):: rho,px,py,pz,e,d
integer, dimension(mx,my,mz):: ones
integer :: ldist,mdist,ndist,i,j,l,m,n,extrastar1,extrastar2
character(len=mfile) fname, name
character(len=mid), save:: id='stellarcluster: $Id: stars_ones.f90,v 1.75 2012/09/26 15:06:00 aake Exp $'
logical debug


  call print_trace (id, dbg_expl, 'BEGIN')


!Firstly, we want to get the number of stars to be created in the 
!chosen clump. To begin with, we have chosen the stellar efficency of the 
!clump. The sum of all stars that are created will be aprox to mass of the
!clump times the efficency--> protomass x epsilon=mass_c. 

!The stars are formed following the IMF:
!N(m)dm=A_c*m^(-3/(4-bet)-1)*Integral[P(mjean),{mjean,0,m}]dm 
!(Padoan & Nordlund 2002). 
!A priori, we don't know "A_c". 
!First thing to do is to calculate "A_c". Notice that A_c=mass_c/int0. Being 
!int0=Integrate(m*m^(-3/(4-bet)-1)*Integral[P(mjean),{mjean,0,m}]dm,
!{m,0.08,100.}) Note that we assume that there are only stars in the range 
! [0.08,100.]. Using Mathematica...

c1=-sqrt(2./alog(1.+0.25*mrms**2))

c2=(alog(8294400./(4.+mrms**2))-2.*alog(n0))/2./sqrt(2.)/ &
     sqrt(alog(1.+0.25*mrms**2))

int0=((-2**((2*(16 + bet**2))/(-4 + bet)**2) + 2**((16*bet)/ &
     (-4 + bet)**2)*bet)*(2**((23*bet)/(-4 + bet)**2)*25**((2* &
     (4 + bet**2))/(-4 + bet)**2)*Exp((1 + bet**2 + 20*bet*c1* &
     c2)/(4.*(-4 + bet)**2*c1**2))*(-3 + Erf((-1 + 8*c1*(c2 - &
     c1*alog(12.5)) + bet*(1 - 2*c1*c2 + c1**2*alog(156.25)))/ &
     (2.*(-4 + bet)*c1)) + Erfc((1 - bet)/(8*c1 - 2*bet*c1) - c2) + &
     Erfc((1 - bet)/(-8*c1 + 2*bet*c1) + c2) + Erfc((1 - bet)/(8* &
     c1 - 2*bet*c1) - c2 - c1*alog(100.))) + 2**((4 + bet*(13 + &
     bet))/(-4 + bet)**2)*25**(1 + 3/(-4 + bet))* (-(2**((5*bet)/ &
     (-4 + bet)**2)*25**((10*bet)/(-4 + bet)**2)*Erfc(c2 - &
     c1*alog(12.5))) + 2**((4 + bet**2)/(-4 + bet)**2)*25**((2*(4 +&
     bet**2))/(-4 + bet)**2)*Erfc(c2 + c1*alog(100.)))* &
     (Cosh((bet + 2*(4 + bet**2)*c1*c2)/(2.*(-4 + bet)**2*c1**2)) + &
     Sinh((bet + 2*(4 + bet**2)*c1*c2)/(2.*(-4 + bet)**2*c1**2)))))/ &
     (2**((16 + bet*(31 + bet))/(-4 + bet)**2)*25**((2*(4 + bet**2))/ &
     (-4 + bet)**2)*(-1 + bet)* (Cosh((bet + 2*(4 + bet**2)*c1*c2)/ &
     (2.*(-4 + bet)**2*c1**2)) +  Sinh((bet + 2*(4 + bet**2)*c1*c2)/&
     (2.*(-4 + bet)**2*c1**2))))

A_c=mass_c/int0

!Now, having the IMF=N(m)dm of the cluster what I do is to calculate the 
!number of  stars that are formed in the cluster. We split the search in two
!mass range. One between [0.08,M_max] and [M_max,100.] being M_max the 
!mass where the N(m)dm reach its maximum. That is,
!M_max is calculated searching the root of the first derivative of N(m)  
!Again, using magical Mathematica....

!v_max=brent(0.08,0.5,10.,1.e-7,M_max,mrms,n0,bet,A_c)
M_max=100.

!Calculate int1=number of stars with masses (in M_o) [0.08, M_Max] = integral
! of N(m)dN between [0.08Mo, M_Max] 

call qtrap(int1,0.08,M_max,mrms,n0,bet,A_c)

if (debug(dbg_expl)) print *,"A_c,int1",A_c,int1

mass_stars_cluster=0.

!For the interval between [0.08,M_max] --> we have to solve for 
!every star G1(m)-G1(0.08)-xrandom=0, being G1(m)=Integrate[N(m)dN,m]/int1-->
!nsint(m)/int1-nsint(0.08)/int1-xrandom=0.-->
!nsint(m)-nsint(0.08)- int1*xrandom=0--> nsint(m)+cte1=0.


extrastar1=0
xrandom=ran1s(iseed) 
xrandom=ran1s(iseed) 
if (xrandom.lt.int1-aint(int1)*1.) extrastar1=1 

fname=name('stellarmasses.dat','sms',file)
open (11, file=fname,status='unknown',position='append')
do i=1,int(int1+extrastar1)
   xrandom=ran1s(iseed) 
   m1=rtbis(0.08,M_max,1.e-7,mrms,n0,bet,A_c,-(nsint(0.08,mrms,n0,bet,A_c)+int1*xrandom))
   if (m1.lt.mass_c) then
      call starpose(rho,ldist,mdist,ndist) !Choose stellar birth place
      if (.not. do_delayexpl) then
         write (*,*) 'Super Nova stats'
         write (*,*) '  #Type II, tanh()-like profile distribution in log e' 
         m_add=3.
!      call SNexpl (rho,e,d,2,m1,ldist,mdist,ndist,m_add)
      else 
         call tracksn(rho,px,py,pz,e,d,m1,ldist,mdist,ndist,ones)
      end if
      mass_stars_cluster=mass_stars_cluster+m1
      if (debug(dbg_expl)) print *," int1-->Estrella numero",i, "y masa",m1
      write (11,102) m1
102  format(ES10.3) 
      n_stars=n_stars+1
   endif
enddo
close(11)


print *,"CLUSTER PROPERTIES "
print *,"c1,c2,A_c",c1,c2,A_c
print *,"n0,mrms,mass_c",n0,mrms,mass_c
print *,"Maximum of N(m)dm=M_max",M_max
print *,"Number of stars in cluster [0.08,M_max]:",aint(int1)+extrastar1
print *,"Total mass converted to massive stars (in Mo)",mass_stars_cluster
print *,"Final efficency (sum(Mstars)/Mass_clump),epsilon",mass_stars_cluster/mass_c*epsilon,epsilon
 
  call print_trace (id, dbg_expl, 'END')
END SUBROUTINE

!*************************************************************
real FUNCTION erf(x)

          ! Error function from Numerical Recipes.
          ! erf(x) = 1 - erfc(x)

          implicit none

      real dumerfc, x
      real t, z


      z = abs(x)
      t = 1.0 / ( 1.0 + 0.5 * z )

      dumerfc =       t * exp(-z * z - 1.26551223 + t *     &
                 ( 1.00002368 + t * ( 0.37409196 + t *          &
             ( 0.09678418 + t * (-0.18628806 + t *              &
                         ( 0.27886807 + t * (-1.13520398 + t *          &
             ( 1.48851587 + t * (-0.82215223 + t * 0.17087277 )))))))))

      if ( x.lt.0.0 ) dumerfc = 2.0 - dumerfc
     
          erf = 1.0 - dumerfc

END function erf
!************************************************************
SUBROUTINE starpose(rho,ldist,mdist,ndist)

!  Find a star position, with probability prop. to density, in
!  the clump
!
!  980607/aake: Coded
!  981023/aake: Coded

  USE params
  !USE stagger
  USE units
  implicit none

  integer ldist,mdist,ndist
  real elimit, rholim, convlim, ran1s
  real, dimension(mx,my,mz):: rho
  real,dimension(mx*my*mz) :: scr2
  real, allocatable, dimension(:) :: scr3
  integer i, l, m, n,j,iz
  integer ii(1),ip,ml
  real rnd,a

  integer nk,h,k
  integer*2, external:: icompare
  character(len=mid), save:: id='starpose: $Id: stars_ones.f90,v 1.75 2012/09/26 15:06:00 aake Exp $'

  call print_trace (id, dbg_expl, 'BEGIN')

  nk=ntot
  allocate(scr3(nk))
!XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX????
  do j=1,nk
     scr3(j)=rho(postot(j,1),postot(j,2),postot(j,3))
  enddo
     
!call qsort (scr3,nk,4,icompare)

! sort the density

!XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX????
  do ip=1,nk
    ii=minloc(scr3(ip:nk))
    ml=ii(1)
    a=scr3(ip)
    scr3(ip)=scr3(ml+ip-1)
    scr3(ml+ip-1)=a
  enddo 

! integrate the mass

  scr2(1)=0.
!XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX????
  do k=2,nk
       scr2(k)=scr2(k-1)+scr3(k-1)
  enddo

! Choose a density with probability proportional to density

  rnd=ran1s(snseed)
  rnd=ran1s(snseed)         ! roll a few extra times, to avoid
  rnd=ran1s(snseed)         ! peculiar first value.
!XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXrandom positioning
  call binsearch (scr2,nk,rnd*scr2(nk),i)

!Choose the location of the star
!XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX????
do n=1,ntot
   if (rho(postot(n,1),postot(n,2),postot(n,3)).eq.scr3(i)) then
      ldist=postot(n,1)
      mdist=postot(n,2)
      ndist=postot(n,3)
      deallocate(scr3)
      call print_trace (id, dbg_expl, 'END')
      return
   endif
enddo

  stop

  end subroutine

!**********************************************************************
real FUNCTION erfc(x)

          ! Error function from Numerical Recipes.
          ! erf(x) = 1 - erfc(x)

          implicit none

          real x
    
          erfc = 1.0 - erf(x)

END function erfc

!*************************************************************
real FUNCTION nsint(m,mrms,n0,bet,A)


          implicit none

          real m,mrms,n0,bet,A
    
          nsint=(A*(-4 + bet)*(Exp &
               (((-4 + bet)*Log(8294400./(4 + mrms**2)) + 3*Log((4 + mrms**2)/ &
               4.))**2/(8.*(-4 + bet)**2*Log((4 + mrms**2)/4.)))*n0** &
               (3/(8 - 2*bet))*Erf((-4*(-4 + bet)*Log(m) + (-4 + bet)* &
               Log(8294400./(4 + mrms**2)) + 3*Log((4 + mrms**2)/4.) + &
               8*Log(n0) - 2*bet*Log(n0))/(2.*Sqrt(2.)*(-4 + bet)*Sqrt( &
               Log((4 + mrms**2)/4.)))) + Exp(Log(8294400./(4 + mrms**2))**2 &
               /(8.*Log((4 + mrms**2)/4.)))*m**(3/(-4 + bet))*&
               Erfc((-4*Log(m) + Log(8294400./(4 + mrms**2)) - 2*Log(n0))/ &
               (2.*Sqrt(2.)*Sqrt(Log((4 + mrms**2)/4.))))))/ &
               (6.*Exp(Log(8294400./(4 + mrms**2))**2/(8.*Log((4 + mrms**2) &
               /4.))))

END function nsint

!*************************************************************
real FUNCTION ndn(m,mrms,n0,bet,A)

          implicit none
          
          real m,A,mrms,bet,n0 
          ndn =  (A*m**(-1 - 3/(4 - bet))*Erfc((-4*Log(m) + &
               Log(8294400./(4 + mrms**2)) - 2*Log(n0))/(2.*Sqrt(2.)* &
               
Sqrt(Log((4 + mrms**2)/4.)))))/2.

END function ndn
!*************************************************************
REAL*8 FUNCTION BRENT(AX,BX,CX,TOL,XMIN,mrms,n0,bet,A_c)
!Given a function F, and a bracketing triplet of abscissas
!AX,BX,CX (such that BX is between AX and CX, and F(BX) is less 
!than both F(AX) and F(CX)), this routine isolates the minimum 
!to a fractional precision of about TOL using Brent's method.
!The abscissa of the minimum is returned in XMIN, and the minimum
!function value is returned as BRENT, the returned function value.
implicit none
integer,PARAMETER::ITMAX=1000000
real,PARAMETER::CGOLD=.3819660,ZEPS=1.0E-10
!Maximum allowed number of iterations; golden ration; and a small

!number which protects against trying to achieve fractional accuracy

!for a minimum that happens to be exactly zero.

!IMPLICIT REAL*8 A-H,O-Z
integer:: iter
real:: A,AX,BX,CX,TOL,B,V,W,X,E,FX,FV,FW,XM,TOL1,TOL2,XMIN,R,Q,P , &
     ETEMP,D,U,FU
real::mrms,n0,bet,A_c

A=MIN(AX,CX)
B=MAX(AX,CX)
V=BX
W=V
X=V
E=0.
FX=-1.*ndn(X,mrms,n0,bet,A_c)
FV=FX
FW=FX
DO 11 ITER=1,ITMAX                                      !main loop
  XM=0.5*(A+B)
  TOL1=TOL*ABS(X)+ZEPS
  TOL2=2.*TOL1
  IF(ABS(X-XM).LE.(TOL2-.5*(B-A))) GOTO 3  !Test for done here
  IF(ABS(E).GT.TOL1) THEN     !Construct a trial parabolic fit
    R=(X-W)*(FX-FV)
        Q=(X-V)*(FX-FW)
    P=(X-V)*Q-(X-W)*R
        Q=.2*(Q-R)
        IF(Q.GT.0)  P=-P
        Q=ABS(Q)
        ETEMP=E
    E=D
        IF (ABS(P).GE.ABS(.5*Q*ETEMP).OR.P.LE.Q*(A-X).OR.  &
        P.GE.Q*(B-X))  GOTO 1
!   The above conditions determine the acceptability of the 
!   parabolic fit. Here it is o.k.:
    D=P/Q
        U=X+D
        IF(U-A.LT.TOL2.OR.B-U.LT.TOL2)  D=SIGN(TOL1,XM-X)
        GOTO 2
  ENDIF
1 IF(X.GE.XM) THEN
    E=A-X
  ELSE
    E=B-X
  ENDIF
  D=CGOLD*E
2 IF(ABS(D).GE.TOL1) THEN
    U=X+D
  ELSE
    U=X+SIGN(TOL1,D)
  ENDIF
  FU=-1.*ndn(U,mrms,n0,bet,A_c)  !This the one function evaluation per iteration
  IF(FU.LE.FX) THEN
    IF(U.GE.X) THEN
          A=X
    ELSE
          B=X
        ENDIF
        V=W
        FV=FW
        W=X
        FW=FX
        X=U
        FX=FU
  ELSE
    IF(U.LT.X) THEN
          A=U
        ELSE
          B=U
        ENDIF
        IF(FU.LE.FW.OR.W.EQ.X) THEN
          V=W
          FV=FW
          W=U
          FW=FU
    ELSE IF(FU.LE.FV.OR.V.EQ.X.OR.V.EQ.W) THEN
          V=U
          FV=FU
    ENDIF
  ENDIF
11 CONTINUE
Pause ' Brent exceed maximum iterations.'
3 XMIN=X   !exit section
  BRENT=FX
  RETURN
  END function


!*****************************************************************************
Subroutine qtrap(s,a,b,mrms,n0,bet,A_c)

IMPLICIT NONE
REAL :: a,b,s
INTEGER, PARAMETER :: JMAX=20
REAL, PARAMETER :: EPS=1.0e-6
REAL :: olds
real :: mrms,n0,bet,A_c
INTEGER :: j
olds=0.0               !Initial value of olds is arbitrary.
do j=1,JMAX
        call trapzd(a,b,s,j,mrms,n0,bet,A_c)
        if (j > 5) then 
           if (abs(s-olds) < EPS*abs(olds) .or. &
                (s == 0.0 .and. olds == 0.0)) RETURN
        end if
        olds=s
end do

END subroutine qtrap
!******************************************************
SUBROUTINE trapzd(a,b,s,n,mrms,n0,bet,A_c)

IMPLICIT NONE
REAL :: a,b
REAL :: s
INTEGER :: n
REAL :: del,sum,tnm,x
real :: mrms,n0,bet,A_c
INTEGER :: it,j

if (n.eq.1) then
   s=0.5*(b-a)*(ndn(a,mrms,n0,bet,A_c )+ndn(b,mrms,n0,bet,A_c ))
else
   it=2**(n-2)
   tnm=it
   del=(b-a)/tnm   !This is the spacing of the points to be added.
   x=a+0.5*del
   sum=0.
   do j=1,it
      sum=sum+ndn(x,mrms,n0,bet,A_c)
      x=x+del
   enddo
   s=0.5*(s+(b-a)*sum/tnm) !This replaces s by its refined value.
end if
END SUBROUTINE trapzd
!************************************************************

FUNCTION rtbis(x1,x2,xacc,mrms,n0,bet,A,cte)
INTEGER JMAX
REAL rtbis,x1,x2,xacc,mrms,n0,bet,A
!EXTERNAL func
PARAMETER (JMAX=600) !Maximum allowed number of bisections.
!Using bisection, find the root of a function func known to lie 
!between x1 and x2. The root, returned as rtbis, will be refined 
!until its accuracy is xacc.

INTEGER j
REAL dx,f,fmid,xmid,cte
fmid=nsint(x2,mrms,n0,bet,A)+cte
f=nsint(x1,mrms,n0,bet,A)+cte
if(f*fmid.ge.0.) then 
print *,"f,fmid,cte,x1,x2",f,fmid,cte,x1,x2
print *,"Error in bisection"
endif
if(f.lt.0.)then !Orient the search so that f>0 lies at x+dx.
   rtbis=x1
   dx=x2-x1
else
   rtbis=x2
   dx=x1-x2
endif
do j=1,JMAX     !Bisection loop.
   dx=dx*.5
   xmid=rtbis+dx
   fmid=nsint(xmid,mrms,n0,bet,A)+cte
   if(fmid.le.0.)rtbis=xmid
   if(abs(dx).lt.xacc .or. fmid.eq.0.) then
!      print *,j
      return
   endif   
enddo 
print *,"The chosen accuracy hasn't been reached"
END function
!***********************************************************************

END MODULE

!***********************************************************************
SUBROUTINE init_explode (rho)
  USE params
  USE units
  USE explosions
  implicit none

  integer :: i, j, k
  real    :: rlow, f 
  real, dimension(mx,my,mz):: rho
  character(len=mid), save:: id='init_explode: $Id: stars_ones.f90,v 1.75 2012/09/26 15:06:00 aake Exp $'


  call print_id(id)

  n_stars=0
  mass_min_cluster=3.
  Tlimit = 4000.     ! temperature criterion for star formation
  rholim = 1.0       ! density criterion
  fvesc = 3.         ! convergence criterion; escape velocity scaling factor - large value => no effect 
  ctime = 5
  wtime = 1
  sntime = 10.
  psftime = 0.01     ! proto-stellar formation time 
  ntot=1
  esn = 1.
  dsn = 1.0        
  wsn = 4.
  wstar=3.
  psn = 5.
  snseed = -1
  nSN=3
  tSN=1.
  bet=1.74           !Beta from turbulence theory
  do_initialSNE=.false.
  do_cluster=.true.
  do_conv=.false.
  do_falgarone=.true.                            ! insist on Falgarone density
  do_ionizeclump=.false.                         ! Ionize clump just after form cluster 
  mass_cluster=1000.
  turnon=-1
  do_expl = .true.                               ! supernova explosions?
  do_delayexpl = .false.                         ! tracking of high-mass stars?
  do_lmsf = .false.                              ! formation of low-mass stars? 
  do_psft = .false.                              ! tracking of low-mass proto-stars?
  do_asym = .false.                              ! assymetric ejection of metals?
  theta_eject = 0.                               ! ejection vector
  width_eject = pi/4.                            ! width of ejection cone
  yfile = '../src/FORCING/YIELDS/pFe_WW95.tbl'   
  ylen  = 8
  taufile = '../src/FORCING/YIELDS/tau.tbl'
  taulen = 28

!******************************************************************
! Decide between Meynet and Schaller tables
!******************************************************************

  genevelifefile = '../src/FORCING/YIELDS/Meynetlife.tbl'
  genevelifelen = 8
  genevemassfile = '../src/FORCING/YIELDS/Meynetmass.tbl'
  genevemasslen = 8
  genevelifePh1file = '../src/FORCING/YIELDS/MeynetlifePh1.tbl'
  genevelifePh1len = 8
  genevemassPh1file = '../src/FORCING/YIELDS/MeynetmassPh1.tbl'
  genevemassPh1len = 8

  genevelifefile = '../src/FORCING/YIELDS/Schallerlife.tbl'
  genevelifelen = 10
  genevemassfile = '../src/FORCING/YIELDS/Schallermass.tbl'
  genevemasslen = 10
  genevelifePh1file = '../src/FORCING/YIELDS/SchallerlPh1.tbl'
  genevelifePh1len = 10
  genevemassPh1file = '../src/FORCING/YIELDS/SchallermPh1.tbl'
  genevemassPh1len = 10


  SNtemp=1.e7
  dtsn = 0.
  epsilon=0.8
  rhoav=7. 
  masslim=100.
  
  if (do_trace) print *,'call read_explode'
  call read_explode
  if (do_trace) print *,'call init_wind'
  call init_wind 
  if (.not. do_expl) return

  if (do_trace) print *,'allocate'
  allocate (ytbl(ylen,2))
  if (do_trace) print *,'reading ',yfile
  open(1,file=yfile, status='old', form='formatted')
  do k=1,ylen
    read(1,*) ((ytbl(i,j), i=k,k), j=1,2)
  end do
  close(1)

  if (do_trace) print *,'do_delayed'
  if (do_delayexpl) then
    mhms = 0             ! no high-mass star exists from the beginning
    
    allocate(genevelifetbl(genevelifelen,2))
    if (do_trace) print *,'reading ',genevelifefile
    open(1,file=genevelifefile, status='old', form='formatted')
    do k=1,genevelifelen
      read(1,*) ((genevelifetbl(i,j), i=k,k), j=1,2)
    end do
    close(1)   
    
    allocate(genevemasstbl(genevemasslen,2))
    if (do_trace) print *,'reading ',genevemassfile
    open(1,file=genevemassfile, status='old', form='formatted')
    do k=1,genevemasslen
      read(1,*) ((genevemasstbl(i,j), i=k,k), j=1,2)
    end do
    close(1)

    allocate(genevelifePh1tbl(genevelifePh1len,2))
    if (do_trace) print *,'reading ',genevelifePh1file
    open(1,file=genevelifePh1file, status='old', form='formatted')
    do k=1,genevelifePh1len
      read(1,*) ((genevelifePh1tbl(i,j), i=k,k), j=1,2)
    end do
    close(1)

    if (do_trace) print *,'allocate5', genevemassPh1len
    allocate(genevemassPh1tbl(genevemassPh1len,2))
    if (do_trace) print *,'reading ',genevemassPh1file
    open(1,file=genevemassPh1file, status='old', form='formatted')
    do k=1,genevemassPh1len
      read(1,*) ((genevemassPh1tbl(i,j), i=k,k), j=1,2)
    end do
    close(1)
        
! form nSN stars to be exploted in the time interval (t,tSN) million years
    if (do_trace) print *,'call initial_SNE'
    if (do_initialSNE.and.turnon.lt.0) call initial_SNE              !artificial SNE??
   

    if (do_trace) print *,'call rad_stars'
    if (iread.ge.0) call read_stars
    if (iread.lt.0.and.turnon.lt.0.and.do_initialSNE) call read_stars
  endif   
  if (do_lmsf) then
    lmsid = 0
    if (do_psft) then
      mlms = 0
      plmsid = 0
    end if 
  end if  

  totalstellarmass=0.

  if (do_trace) print *,'init_explode done'

END subroutine

!***********************************************************************
SUBROUTINE read_explode
  USE params
  USE units
  USE explosions
  implicit none
  real f, rlow

  namelist /expl/ &
    do_expl,do_delayexpl,do_lmsf,do_psft,do_asym,    &
    snseed,sntime,psftime,theta_eject,width_eject,   &
    Tlimit,rholim,fvesc,ctime,esn,dsn,wsn,psn,       &
    ylen,yfile,taulen,taufile, genevelifefile,       &
    genevelifelen, genevemasslen, genevemassfile,    &
    nSN,tSN,do_initialSNE,turnon,SNtemp,epsilon,     &
    rhoav,masslim,wtime,do_cluster,mass_cluster,bet, &
    wstar,mass_min_cluster,do_conv,do_falgarone,     &
    do_ionizeclump,convlim2,datadir
  character(len=mid), save:: id='read_explode: $Id: stars_ones.f90,v 1.75 2012/09/26 15:06:00 aake Exp $'


  call print_id(id)

  f=0.8888889        ! tuning parameter for rlow (which sets a lower limit on rholim)  
  call rewind_stdin
  rewind (stdin); read (stdin,expl)
  convlim=convlim2
!  convlim = fvesc*vesc0   ! sqrt(rho) x convlim = escape velocity per unit length
  rlow=real(ceiling(10.*f*(100.*msun/cmu)/(0.6666667*(1.5*sqrt(pi)*wsn*dx)**3)))/10.
print *,"hola3"
!  if ((do_delayexpl) .and. (rholim .lt. rlow)) then
!    rholim=rlow
!    print *, 'Warning: rholim is set to',rholim,' by the mass substraction routine'
!  end if
  write (*,expl)
print *,"hola4"
  write(*,*) 'CONVLIM =',convlim
  write(*,*) 'Rlow=====    ',rlow
END subroutine

!***********************************************************************

SUBROUTINE explode (rho,px,py,pz,e,d,Bx,By,Bz)
  USE params
  USE explosions
  USE units
  USE eos
  implicit none

  integer ntime,i,j,k,ldist,mdist,ndist,nclump,iz
  integer, allocatable, dimension(:,:,:) :: ones_array
  real ran1s,etime,elap1(2),elap2(2),elapsed1,elapsed2  
  real rnd,rnd2,massconversion,elimit,alpha
  real p,imfk,imfm,starformationfraction,starMabove8Msun 
  real lowmasstarM,SNmass,lmmass,freq,lmsf_freq,volume
  real cloudlifetime, progmass,addmass
  real, save:: eemin
  
  double precision totmasse,totmassr,totmassc,totmass
  double precision tosolarM

  real, dimension(mx,my,mz) :: rho,px,py,pz,e,d,Bx,By,Bz
  real, allocatable, dimension(:,:,:) :: lnr, ux, uy, uz, conv,ee
  character(len=mfile) fname, name
  character(len=mid), save:: id='explode: $Id: stars_ones.f90,v 1.75 2012/09/26 15:06:00 aake Exp $'


  if (.not. do_expl) return

  if (id.ne.' ') then
    call print_trace (id, dbg_expl, 'BEGIN')
    print *,'Tlimit, rholim, convlim =', Tlimit, rholim, convlim
  endif

  if (it.eq.turnon) then       !Artificial SNE???
     call initial_SNE
     call read_stars
  endif

!Save hms every nsnap (at the same time as 'write_snap')

!TEST!!!!!!!!!!!!! Creo una estrella de 50 para seguirla el recorrido

!if (it.eq.1) then
!   call tracksn(rho,px,py,pz,e,d,22.,100,100,100)
!   print *,'ESTRELLA DE 22.Mo'
!endif
!print *,'mhms',mhms,nsnap,it
if (do_delayexpl) then
!   if(mod(it,nsnap).eq.0 .and. mhms.gt.0) call write_stars
   if(mod(it,nsnap).eq.0) call write_stars
endif
allocate(ee(mx,my,mz))

!XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
  !$omp parallel do private(k)
  do k=1,mz
    ee(:,:,k) = e(:,:,k)/rho(:,:,k)
  end do

  print *,"temp",utemp*ee(1,1,1)  
  volume = mx*dx*my*dy*mz*dz

  alpha = 2.35            ! parameters for calculating SN progenitor masses 
  p     = -1./(alpha-1.)
  imfk  = 0.058375757
  imfm  = 1.995262E-03

  elimit=Tlimit/utemp


!***********************************
!      parsec=3.09e18
!      solarM=1.99e33
!      tosolarM=1e-24/solarM
!      
!      print*,'tosolarM',tosolarM
!      cellvolume=(x4-x1)**2*(z4-z1)*(1000*parsec)**3/mw_LONG
!      massconversion=cellvolume*tosolarM_LONG

  tosolarM=cdu/msun
  massconversion=tosolarM*clu**3*(dx*dy*dz)

  cont=0  
  allocate (ones_array(mx,my,mz))
!XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
!$omp parallel do private(iz)
   do iz=1,mz                       
      ones_array (:,:,iz)=0
   enddo
  totmass=0.0
  totmasse=0.0
  totmassr=0.0
  totmassc=0.0
  eemin=ee(1,1,1)
  if (mod(it,ctime) .eq. ctime-1) then

!-----------------------------------------------------------------------
    if (do_conv) then
      allocate(ux(mx,my,mz),  &
               uy(mx,my,mz),  &
               uz(mx,my,mz),  &
               lnr(mx,my,mz), &
               conv(mx,my,mz))
      !$omp parallel do private(iz)
!XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
      do iz=1,mz
        lnr(:,:,iz) = alog(rho(:,:,iz))
      end do
      call xdn_set_par(lnr,ux)
      call ydn_set_par(lnr,uy)
      call zdn_set_par(lnr,uz)
      !$omp parallel do private(iz)
!XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
      do iz=1,mz
        ux(:,:,iz) = px(:,:,iz)/exp(ux(:,:,iz))
        uy(:,:,iz) = py(:,:,iz)/exp(uy(:,:,iz))
        uz(:,:,iz) = pz(:,:,iz)/exp(uz(:,:,iz))
          end do
          if (do_2nddiv) then
        call ddxup1_set_par(ux,conv)
        call ddyup1_add_par(uy,conv)
        call ddzup1_add_par(uz,conv)
      else
        call ddxup_set_par(ux,conv)
        call ddyup_add_par(uy,conv)
        call ddzup_add_par(uz,conv)
      end if
      !$omp parallel do private(iz)
!XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
      do iz=1,mz
        conv(:,:,iz) = -conv(:,:,iz)/sqrt(rho(:,:,iz))
      end do
    end if
!-----------------------------------------------------------------------

    if (do_ionization) then
      allocate (temp(mx,my,mz))
      !$omp parallel do private(k,j,i), reduction(+:totmass,cont), reduction(min:eemin)
!XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
      do k=1,mz
        call eos2 (rho(:,:,k),ee(:,:,k),tt=temp(:,:,k))
        do j=1,my
          if (do_conv) then
            do i=1,mx
              if (temp(i,j,k).lt.Tlimit.and.rho(i,j,k).ge.rholim.and.conv(i,j,k).gt.convlim) then
                 totmass=massconversion*rho(i,j,k)+totmass
                 cont=cont+1
                 ones_array(i,j,k)=1
              end if
              eemin = min(eemin,ee(i,j,k))
            end do
          else
            do i=1,mx
              if (temp(i,j,k).lt.Tlimit.and.rho(i,j,k).ge.rholim) then
                 totmass=massconversion*rho(i,j,k)+totmass
                 cont=cont+1
                 ones_array(i,j,k)=1
              end if
              eemin = min(eemin,ee(i,j,k))
            end do
          end if
        end do
      end do  
    else
      !$omp parallel do private(k,j,i), reduction(+:totmass,totmassc,totmasse,totmassr,cont), reduction(min:eemin)
!XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
      do k=1,mz
        do j=1,my
          if (do_conv) then
            do i=1,mx
              if (conv(i,j,k).lt.convlim) then
                 totmassc=massconversion*rho(i,j,k)+totmassc
              end if
              if (ee(i,j,k).lt.elimit.and.rho(i,j,k).ge.rholim.and.conv(i,j,k).gt.convlim) then
                 totmass=massconversion*rho(i,j,k)+totmass
                 cont=cont+1
                 ones_array(i,j,k)=1
              end if
            end do
          else
            do i=1,mx
              if (ee(i,j,k).lt.elimit.and.rho(i,j,k).ge.rholim) then
                 totmass=massconversion*rho(i,j,k)+totmass
                 cont=cont+1
                 ones_array(i,j,k)=1
              end if
            end do
          end if
          do i=1,mx
            if (ee(i,j,k).lt.elimit) then
               totmasse=massconversion*rho(i,j,k)+totmasse
            end if 
            if (rho(i,j,k).lt.rholim) then
               totmassr=massconversion*rho(i,j,k)+totmassr
            end if
            eemin = min(eemin,ee(i,j,k))
          end do
        end do
      end do  
    end if
    dtsn=dtsn+dt
    if (do_conv) deallocate(lnr,ux,uy,uz,conv)
  else 
    dtsn=dtsn+dt
    if(mod(it,wtime).eq.0) call stellar_wind(rho,e)         ! stellar wind every 'wtime' steps 
    deallocate(ee)  
    call print_trace (id, dbg_expl, 'END')
    return
  end if
  
  if (master) print '(1x,a,4(1pg12.4),2i7)', &
    'totmass',totmass,totmasse,totmassr,totmassc,cont,mhms

  rnd=ran1s(snseed)

  fname = name('supernovae.dat','sn',file)
  open (3,file=fname,form='formatted',status='unknown')
  write(3,100) '  #rnd,freq,mass,SNrate,Tlimit,Tmin,rhomax:',rnd,freq, &
                totmass,freq/dtsn/volume,Tlimit,eemin*utemp,maxval(rho)
  close (3) 
  100 format(1x,a,2f6.3,2(1pe11.3),2(0pf7.0),1pe10.2) 
  if (do_lmsf) then
    lmsf_freq=lowmasstarM*starformationfraction*totmass/lmmass
    lmsf_freq=lmsf_freq*dtsn/cloudlifetime
    call lmsf(rho,px,py,pz,ee,d,lmsf_freq,elimit,rholim,convlim)
  end if

  n_stars=0
  if (totmass.gt. masslim) then
     if (do_ionizeclump) then 
        ntot_io=0
        allocate(pos_io(cont,3))
     endif   
     nclump=1
     elapsed1=etime(elap1)
     do while (cont.gt.0)
        call find_star(rho,px,py,pz,e,d,elimit,rholim,convlim,massconversion,nclump,ones_array)
        nclump=nclump+1
     enddo
     if (do_ionizeclump) deallocate(pos_io)
     if (do_cluster) print *,'Total number of stars formed in this burst',n_stars 
     elapsed2=etime(elap2)
     print *,'Elapsed (s)',elapsed2-elapsed1,' user',elap2(1)-elap1(1),'system',elap2(2)-elap1(2)
  endif 
  deallocate (ones_array)

!  if (totmass.gt.100.) then
!    rnd2=ran1s(snseed)
!    progmass=(rnd2*imfk+imfm)**p
!    call snpose(rho,px,py,pz,ee,lbdist,mbdist,nbdist,elimit,rholim,convlim)
!    if (.not. do_delayexpl) then
!      write (*,*) 'Super Nova stats'
!      write (*,*) '  #Type II, tanh()-like profile distribution in log e' 
!      call SNexpl (rho,e,d,2,progmass,lbdist,mbdist,nbdist)
!    else 
!      write (*,*) 'High-mass star stats'   
!      call tracksn(rho,px,py,pz,e,d,progmass,lbdist,mbdist,nbdist)
!    end if
!  end if
  if (do_delayexpl) then
    if (mhms .gt. 0) then 
      do i=1,mhms
       if (t .ge. hms(i,1)) then
        write (*,*) 'Super Nova stats'
        write (*,*) '  #Type II, tanh()-like profile distribution in log e' 
!IMP-> when calling SNexpl, is it hms(i,15) (mass when SNe ocurrs) or hms(i,2) (initial stellar mass)??
        addmass=hms(i,2)-hms(i,12)
        print *,'hms(1,2), hms(i,12),hms(i,15)', hms(i,2), hms(i,12),hms(i,15) 
        call SNexpl(rho,e,d,2,hms(i,15),hmspos(i,1),hmspos(i,2),hmspos(i,3),hms(i,3),hms(i,4),hms(i,5),addmass)

        if (mhms .gt. 1) then
          allocate(dum(mhms-1,19))
          allocate(dumpos(mhms-1,3))
          if (i==1) then
            dum=hms(2:mhms,:)
            dumpos=hmspos(2:mhms,:)
          else if (i==mhms) then
            dum=hms(1:mhms-1,:)
            dumpos=hmspos(1:mhms-1,:)
          else
            dum(1:i-1,:)=hms(1:i-1,:)
            dumpos(1:i-1,:)=hmspos(1:i-1,:)
            dum(i:mhms-1,:)=hms(i+1:mhms,:)
            dumpos(i:mhms-1,:)=hmspos(i+1:mhms,:)
          end if
          deallocate(hms,hmspos)
          allocate(hms(mhms-1,19))
          allocate(hmspos(mhms-1,3))
          hms=dum
          hmspos=dumpos
          deallocate(dum,dumpos)
          mhms=mhms-1            ! a supernova has exploded
        else
          deallocate(hms,hmspos)
          mhms=0
        end if
       end if
      end do
    end if
  end if
  if (do_ionization) deallocate(temp)
  dtsn=0.
if(mod(it,wtime).eq.0) call stellar_wind(rho,e)  ! stellar wind every 'wtime' steps 
!  call stellar_wind(rho,e)                       ! stellar wind
  deallocate(ee) 

  call print_trace (id, dbg_expl, 'END')
  return
END subroutine
