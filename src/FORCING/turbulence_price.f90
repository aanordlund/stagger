! $Id: turbulence_price.f90,v 1.5 2007/05/14 14:04:21 aake Exp $
!***********************************************************************
!
! Input parameters are explained in the .in file.
!
! If/when applying this forcing in an MPI program you need to make sure
! that the mesh arrays (xm, ym, zm) give the global coordinates, and that
! the sizes sx, sy, sz also reflect the global sizes, while the variables
! dx, dy, dz and mx, my, mz should be the local values.
!
! Much of the structure of these subroutines is there for historical 
! reasons, and because they have been lifted out of a context.
!
! The pseudo-random number generator is intentionally very simple, to
! make it easier to restart.  Restarts must nevertheless be constructed
! carefully, with saving of iseed, and running through the same time
! sequence again from scratch.
!
!***********************************************************************
MODULE params
  integer, parameter:: mid=72, mfile=72
  integer mx,my,mz,mw
  integer stdin,stdout,mpi_rank
  real t
  real sx,sy,sz
  real dx,dy,dz
  real xmin,ymin,zmin
  real, allocatable, dimension(:):: xm,ym,zm
  real, parameter:: pi=3.14159265
  logical master,do_trace
  character(len=mfile) file
END MODULE

!**********************************************************************
MODULE arrays
  real, allocatable, dimension(:,:,:):: r,Ux,Uy,Uz,dUxdt,dUydt,dUzdt
END MODULE

!**********************************************************************
MODULE forcing
  logical, parameter:: conservative=.false.
  integer verbose, iseed
  logical do_force, do_helmh
  real:: t_turb, t_turn, ampl_turb, k1, k2, pk, a_helmh
  real, allocatable, dimension(:,:,:):: pranx,prany,pranz,franx,frany,franz
END MODULE

!***********************************************************************
SUBROUTINE init_params
  USE params
  implicit none
  integer ix,iy,iz
  namelist /in/mx,my,mz,sx,sy,sz
!
  mpi_rank=0
  stdin=5
  stdout=6
  master=.true.
!
  t=0.
  mx=32; my=32; mz=32
  sx=1.; sy=1.; sz=1.
  xmin=0.; ymin=0.; zmin=0.
!
  read(stdin,in);  write(*,in)
!
  mw=mx*my*mz
  dx=sx/mx; dy=sy/my; dz=sz/mz
  allocate(xm(mx),ym(my),zm(mz))
  xm=xmin+dx*(/(ix-1,ix=1,mx)/)
  ym=ymin+dy*(/(iy-1,iy=1,my)/)
  zm=zmin+dz*(/(iz-1,iz=1,mz)/)
END

!***********************************************************************
SUBROUTINE init_arrays
  USE params
  USE arrays
  integer k
  allocate(r(mx,my,mz))
  allocate(Ux(mx,my,mz))
  allocate(Uy(mx,my,mz))
  allocate(Uz(mx,my,mz))
  allocate(dUxdt(mx,my,mz))
  allocate(dUydt(mx,my,mz))
  allocate(dUzdt(mx,my,mz))
!$omp parallel do private(k)
  do k=1,mz
    r(:,:,k)=1.
    Ux(:,:,k)=0.
    Uy(:,:,k)=0.
    Uz(:,:,k)=0.
    dUxdt(:,:,k)=0.
    dUydt(:,:,k)=0.
    dUzdt(:,:,k)=0.
  end do
END

!***********************************************************************
SUBROUTINE dealloc
  USE params
  USE arrays
  USE forcing
  deallocate(xm,ym,zm)
  deallocate(r,Ux,Uy,Uz,dUxdt,dUydt,dUzdt)
  deallocate(pranx,prany,pranz,franx,frany,franz)
END

!***********************************************************************
SUBROUTINE init_force
  USE params
  USE forcing
  implicit none
  integer iz
  character(len=mid):: id='$Id: turbulence_price.f90,v 1.5 2007/05/14 14:04:21 aake Exp $'
  namelist /force/do_force,do_helmh,a_helmh,k1,k2,ampl_turb,t_turn,pk,iseed,verbose

  ampl_turb = 0.1
  t_turn = 10.
  do_force = .true.
  do_helmh = .true.
  a_helmh=1.
  k1=1.
  k2=1.5
  pk=7./6.
  verbose=0
  iseed=1

  rewind (stdin); read (stdin,force)
  if (master) write (*,force)

  t_turb = -1.5*t_turn                                                 ! initial value

  allocate (pranx(mx,my,mz), prany(mx,my,mz), pranz(mx,my,mz))
  allocate (franx(mx,my,mz), frany(mx,my,mz), franz(mx,my,mz))
!$omp parallel do private(iz)
  do iz=1,mz                                ! encourage distribution
    pranx(:,:,iz)=0. ; prany(:,:,iz)=0. ; pranz(:,:,iz)=0.
    franx(:,:,iz)=0. ; frany(:,:,iz)=0. ; franz(:,:,iz)=0.
  end do
END SUBROUTINE

!***********************************************************************
SUBROUTINE forceit (r,Ux,Uy,Uz,dUxdt,dUydt,dUzdt)
!
!  Calculate a random force, confined to a shell in k-space, at
!  regular intervals in time.  We assume rho, P, and the sound
!  speed to be near unity.  The velocity amplitude should be of the
!  order ampl_turb.  The size (scale) of the driving motions is of the
!  order of mx*dx/k1.  Thus the turnover time is given by
!  t_turb*ampl_turb = mx*dx/k1; and the acceleration is of the order
!  ampl_turb/t_turb.
!
!----------------------------------------------------------------------
  USE params
  USE forcing

  implicit none

  real, dimension(mx,my,mz):: r,Ux,Uy,Uz,dUxdt,dUydt,dUzdt
  real, allocatable, dimension(:,:,:):: scr1,scr2,scr3,scr

  integer i,j,k,jx,jy,jz,nrand,kmax

  complex fxx,fyy,fzz,kx,ky,kz,corr,expikr

  real x, y, z, xav, yav, zav, rav, w, fact, test, fpow, accel, ran1s
  real eps, fk, average, f

  real, save :: tprev=-1.
  logical, save :: first_time=.true.

  allocate(scr1(mx,my,mz), scr2(mx,my,mz), scr3(mx,my,mz), scr(mx,my,mz))

!----------------------------------------------------------------------
  if (t .ne. tprev) then    ! prevent repeat at same t
    tprev = t
    eps = 1e-5
    
    if (master.and.verbose>1) then
      print *,'forceit: t,t_turb,t_turn=',t,t_turb,t_turn
      if (t_turb .le. t) print *,'new force'
    endif
    
    if (first_time) then
!$omp parallel do private(k)
      do k=1,mz
        pranx(:,:,k)=0.
        prany(:,:,k)=0.
        pranz(:,:,k)=0.
      end do
    endif
!--------------------------------------------- new random numbers:
!  Loop until t_turb > t
    do while (t_turb .le. t)
!$omp parallel do private(k)
      do k=1,mz
        pranx(:,:,k)=franx(:,:,k)
        prany(:,:,k)=frany(:,:,k)
        pranz(:,:,k)=franz(:,:,k)
        franx(:,:,k) = 0.
        frany(:,:,k) = 0.
        franz(:,:,k) = 0.
      end do

    t_turb = t_turb + t_turn

    nrand = 0
    kmax=ifix(k2+1.)
    do jx=-kmax,kmax
      do jy=-kmax,kmax
        do jz=-kmax,kmax
          fk = sqrt(float(jx**2+jy**2+jz**2))
          if (fk .ge. k1-eps .and. fk .le. k2+eps) nrand=nrand+1
        enddo
      enddo
    enddo

!--------------------------------------------------------------------------------
    accel  = ampl_turb/t_turn/sqrt(float(nrand)/8.)    ! rms=1./8. per comp.
!--------------------------------------------------------------------------------

      if (master.and.verbose>0) print *,'random_force: t=',t,t_turb,nrand,iseed

! To obtain a Kolmogorov slope of the driving alone, one should have amplitudes
! a(k) that drop with k^(-11./6.), to have a(k)^2*k^2 = k^(-5./3.).  This ASSUMES
! that the amplitudes are proportional to the driving.  But the energy drain may
! be rather inversely proportional to the turnover time, which goes as k^(2./3.)

    ! pk = 11./6.
    ! pk = 7./6.

    fpow = 0.
    do jx=-kmax,kmax
      kx = cmplx (0., jx*2.*pi/sx)
      do jy=-kmax,kmax
        ky = cmplx (0., jy*2.*pi/sy)
        do jz=-kmax,kmax
          kz = cmplx (0., jz*2.*pi/sz)
          fk = sqrt(float(jx**2+jy**2+jz**2))
          if (fk .ge. k1-eps .and. fk .le. k2+eps) then
            fxx =   cexp(cmplx(0., 2.*pi*ran1s(iseed)))/fk**pk
            fyy =   cexp(cmplx(0., 2.*pi*ran1s(iseed)))/fk**pk
            if (mz.gt.1) then
              fzz = cexp(cmplx(0., 2.*pi*ran1s(iseed)))/fk**pk
            else
              fzz=0.
            end if

! solenoidal field:
!------------------
            if (do_helmh) then
               corr=(kx*fxx+ky*fyy+kz*fzz)/(kx*kx+ky*ky+kz*kz+1e-20) 
               if (jx.ne.0) fxx = fxx - a_helmh*corr*kx
               if (jy.ne.0) fyy = fyy - a_helmh*corr*ky
               if (jz.ne.0) fzz = fzz - a_helmh*corr*kz
            endif
!------------------

            fact=1.
            if (jx.gt.0) fact=fact*0.5
            if (jy.gt.0) fact=fact*0.5
            if (jz.gt.0) fact=fact*0.5
            fpow = fpow+fact*(cabs(fxx)**2+cabs(fyy)**2+cabs(fzz)**2)

!$omp parallel do private(k,i,j,x,y,z,expikr)
            do k=1,mz
              z = zm(k)-zmin
              do j=1,my
                y = ym(j)-ymin
                do i=1,mx
                  x = xm(i)-xmin
                  expikr = accel*cexp(kx*x+ky*y+kz*z)
                  franx(i,j,k) = franx(i,j,k) + real(fxx*expikr)
                  frany(i,j,k) = frany(i,j,k) + real(fyy*expikr)
                  franz(i,j,k) = franz(i,j,k) + real(fzz*expikr)
                end do
              end do
            end do
            if (master.and.verbose>2) then
              print '(1x,a,3i2,f6.3,4(f7.3,f6.3))', &
                    'random_force:k=',jx,jy,jz,fk,fxx,fyy,fzz,fact
            endif
          endif
        enddo
      enddo
    enddo

! TEST:
!--------------------------------------------------
    if (master.and.verbose>1) then
      test=sqrt(sum(franx**2+frany**2+franz**2)/mw)
      if (master) print *,'average acceleration = ',test,fpow,sqrt(fpow)*accel
    end if
!--------------------------------------------------
    enddo ! while
  endif   ! t > t_prev
!------------------------------------------------------------------
  if (master.and.verbose>2) print *,'interpolate force'

!  Time interpolation of the force
!
  !w = 1.-exp(-(t+t_turn-t_turb)/t_turn)
  w = (t_turn-(t_turb-t))/t_turn
  w = 0.5*(1.-cos(w*pi))

  if (master.and.verbose>2) &
    print *,'force: t-t_turb, t_turn, w:', t-t_turb, t_turn, w
  
!$omp parallel do private(k)
  do k=1,mz
    scr1(:,:,k) = pranx(:,:,k) + (franx(:,:,k)-pranx(:,:,k))*w
    scr2(:,:,k) = prany(:,:,k) + (frany(:,:,k)-prany(:,:,k))*w
    scr3(:,:,k) = pranz(:,:,k) + (franz(:,:,k)-pranz(:,:,k))*w
  end do
  if (master.and.verbose>3) then
    write(*,*) mpi_rank,' force: Ux'
    write(*,'(10e12.5)') scr1(1,1,:)
    write(*,*) mpi_rank,' force: pranx'
    write(*,'(10e12.5)') pranx(1,1,:)
    write(*,*) mpi_rank,' force: franx'
    write(*,'(10e12.5)') franx(1,1,:)
    write(*,*) mpi_rank,' force: r'
    write(*,'(10e12.5)') r(1,1,:)
  end if

! Force and velocity averages
!----------------------------

  if (do_trace) print *,'turbulence: average1'
  rav = 1./average(r)
!$omp parallel do private(k)
  do k=1,mz
    scr(:,:,k) = r(:,:,k)*(scr1(:,:,k)+Ux(:,:,k)*(1./t_turn))*rav
  end do
  if (do_trace) print *,'turbulence: average2'
  xav = average(scr)

!$omp parallel do private(k)
  do k=1,mz
    scr(:,:,k) = r(:,:,k)*(scr2(:,:,k)+Uy(:,:,k)*(1./t_turn))*rav
  end do
  if (do_trace) print *,'turbulence: average3'
  yav = average(scr)

!$omp parallel do private(k)
  do k=1,mz
    scr(:,:,k) = r(:,:,k)*(scr3(:,:,k)+Uz(:,:,k)*(1./t_turn))*rav
  end do
  if (do_trace) print *,'turbulence: average4'
  zav = average(scr)

  if (conservative) then
!$omp parallel do private(k)
    do k=1,mz
      dUxdt(:,:,k) = r(:,:,k)*(scr1(:,:,k) - xav)
      dUydt(:,:,k) = r(:,:,k)*(scr2(:,:,k) - yav)
      dUzdt(:,:,k) = r(:,:,k)*(scr3(:,:,k) - zav)
    end do
  else
!$omp parallel do private(k)
    do k=1,mz
      dUxdt(:,:,k) = (scr1(:,:,k) - xav)
      dUydt(:,:,k) = (scr2(:,:,k) - yav)
      dUzdt(:,:,k) = (scr3(:,:,k) - zav)
    end do
  end if

  deallocate (scr1,scr2,scr3,scr)

  first_time = .false.  ! don't come back
END SUBROUTINE

!***********************************************************************
FUNCTION ran1s(idum)
  INTEGER idum,IA,IM,IQ,IR,NTAB,NDIV
  REAL ran1s,AM,EPS,RNMX
  PARAMETER (IA=16807,IM=2147483647,AM=1./IM,IQ=127773,IR=2836, &
  NTAB=32,NDIV=1+(IM-1)/NTAB,EPS=1.2e-7,RNMX=1.-EPS)
  INTEGER k,iy
  if (idum.le.0) then
    idum=max(-idum,1)
  endif
  k=idum/IQ
  idum=IA*(idum-k*IQ)-IR*k
  if (idum.lt.0) idum=idum+IM
  iy=idum
  ran1s=min(AM*iy,RNMX)
  return
END

!***********************************************************************
FUNCTION average (f)
  USE params
  implicit none
!
!  Return average of array.  Make sure precision is retained, by 
!  summing first individual rows, then columns, then planes.
!
  integer i, j, k
  real:: f(mx,my,mz), sumx, sumy, sumz, average
!-----------------------------------------------------------------------
!
  sumz = 0.
!$omp parallel private(k,j,i,sumx,sumy), reduction(+:sumz)
  do k=1,mz
    sumy = 0.
    do j=1,my
      sumx = 0.
      do i=1,mx
        sumx = sumx + f(i,j,k)
      end do
      sumy = sumy + sumx
    end do
    sumz = sumz + sumy
  end do
!$omp end parallel
  average = sumz/(real(mx)*real(my)*real(mz))
  if (do_trace) print *,'average:',average
!
END

!***********************************************************************
PROGRAM price_test
  USE params
  USE arrays
  USE forcing
  integer i,nt
  call init_params
  call init_arrays
  call init_force
  t=0.
  dt=0.2*t_turn
  nt=50
  open (10,file='force.dat',form='unformatted',access='sequential')
  write (10) mx,my,mz,nt
  do i=1,nt
    call forceit(r,Ux,Uy,Uz,dUxdt,dUydt,dUzdt)
    write (10) dUxdt,dUydt,dUzdt
!$omp parallel do private(k)
    do k=1,mz
      Ux(:,:,k)=Ux(:,:,k)+dt*dUxdt(:,:,k)
      Uy(:,:,k)=Uy(:,:,k)+dt*dUydt(:,:,k)
      Uz(:,:,k)=Uz(:,:,k)+dt*dUzdt(:,:,k)
    end do
    t=t+dt
  end do
  close (10)
  call dealloc
  print *,'data written to force.dat; run turbulence_price.pro to check'
END
