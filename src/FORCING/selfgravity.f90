! $Id: selfgravity.f90,v 1.19 2009/12/10 21:31:29 aake Exp $
!**********************************************************************
MODULE selfgravity
  real grav, soft, psoft
  logical isolated
  real, allocatable, dimension(:,:,:):: phi, dphi
END MODULE

!***********************************************************************
SUBROUTINE init_selfg
  USE params
  USE selfgravity
  implicit none

  grav = 0.
  soft = 2.
  psoft = 2.
  isolated = .false.
  call read_selfg
  if (grav > 0.) call test_selfg
END SUBROUTINE

!***********************************************************************
SUBROUTINE read_selfg
  USE params
  USE selfgravity
  implicit none
  namelist /selfg/ grav,soft,psoft,isolated
  character(len=mid):: id="$Id: selfgravity.f90,v 1.19 2009/12/10 21:31:29 aake Exp $"

  call print_id(id)

  if (stdin.ge.0) then
    rewind (stdin); read (stdin,selfg)
  else
    read (*,selfg)
  end if
  if (master) write (*,selfg)
END SUBROUTINE

!***********************************************************************
SUBROUTINE selfgrav (r,xdnr,ydnr,zdnr,dpxdt,dpydt,dpzdt)
  USE params
  USE selfgravity
  implicit none
  real, dimension(mx,my,mz):: r,xdnr,ydnr,zdnr,dpxdt,dpydt,dpzdt
  logical omp_in_parallel, debug

  if (debug(dbg_force)) print *,'selfgrav:',grav,omp_in_parallel(),omp_mythread
  if (grav == 0.) return
  if (omp_in_parallel()) then
    !$omp master
    allocate (phi(mx,my,mz), dphi(mx,my,mz))
    !$omp end master
    !$omp barrier

    call selfgrav_omp (r,xdnr,ydnr,zdnr,dpxdt,dpydt,dpzdt,phi,dphi)

    !$omp barrier
    !$omp master
    deallocate (phi, dphi)
    !$omp end master
  else
    allocate (phi(mx,my,mz), dphi(mx,my,mz))

    !$omp parallel
    call selfgrav_omp (r,xdnr,ydnr,zdnr,dpxdt,dpydt,dpzdt,phi,dphi)
    !$omp end parallel

    deallocate (phi, dphi)
  end if

END SUBROUTINE

!***********************************************************************
SUBROUTINE selfgrav_omp (r,xdnr,ydnr,zdnr,dpxdt,dpydt,dpzdt,phi,dphi)
  USE params
  USE forcing, only: conservative
  implicit none
  real, dimension(mx,my,mz):: r,xdnr,ydnr,zdnr,dpxdt,dpydt,dpzdt,phi,dphi
  integer iz
  character(len=mid):: id="$Id: selfgravity.f90,v 1.19 2009/12/10 21:31:29 aake Exp $"
  call print_trace (id, dbg_force, 'BEGIN')

!---------------------------------------------------------------------
!Gravity added RJ 8/2003
!---------------------------------------------------------------------  

  call gravpot(r,phi)
  if (conservative) then
    call ddxdn_set (phi, dphi)
    do iz=izs,ize
      dpxdt(:,:,iz) = dpxdt(:,:,iz)-xdnr(:,:,iz)*dphi(:,:,iz)
    end do
    call ddydn_set (phi, dphi)
    do iz=izs,ize
      dpydt(:,:,iz) = dpydt(:,:,iz)-ydnr(:,:,iz)*dphi(:,:,iz)
    end do
    call ddzdn_set (phi, dphi)
    do iz=izs,ize
      dpzdt(:,:,iz) = dpzdt(:,:,iz)-zdnr(:,:,iz)*dphi(:,:,iz)
    end do
  else 
    call ddxdn_set (phi, dphi)
    do iz=izs,ize
      dpxdt(:,:,iz) = dpxdt(:,:,iz)-dphi(:,:,iz)
    end do
    call ddydn_set (phi, dphi)
    do iz=izs,ize
      dpydt(:,:,iz) = dpydt(:,:,iz)-dphi(:,:,iz)
    end do
    call ddzdn_set (phi, dphi)
    do iz=izs,ize
      dpzdt(:,:,iz) = dpzdt(:,:,iz)-dphi(:,:,iz)
    end do
  end if

  call print_trace (id, dbg_force, 'END')
END SUBROUTINE

!***********************************************************************
SUBROUTINE test_selfg
  call test_fft
END
