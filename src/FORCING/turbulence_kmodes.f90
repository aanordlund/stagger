! $Id: turbulence_kmodes.f90,v 1.2 2010/09/03 13:16:17 troels_h Exp $
!**********************************************************************
MODULE forcing

logical conservative
logical do_force, do_helmh
integer kmax
real tprev, t_turb, t_turn, ampl_turb, k1, k2, pk, a_helmh
real rnd1, rnd2, rnd3, rhoav, xav, yav, zav
real, allocatable, dimension(:):: rav
complex, allocatable, dimension(:,:,:,:) :: ak_prev, ak_next
END MODULE
!**********************************************************************
  SUBROUTINE init_force
    USE params
    USE forcing
    implicit none
    integer iz
    character(len=mid):: id='$Id: turbulence_kmodes.f90,v 1.2 2010/09/03 13:16:17 troels_h Exp $'

    call print_id(id)

    ampl_turb = 0.1
    do_force = .true.
    do_helmh = .true.
    conservative = .true.
    a_helmh=1.
    k1=1.
    k2=1.5
    pk=7./6.
    t_turn = 0.1
    t_turb = 0.
    call read_force
    if (t_turb == 0.) t_turb = -1.5*t_turn

    kmax = ifix(k2+1.)
    allocate (ak_prev(3,-kmax:kmax,-kmax:kmax,-kmax:kmax), &
              ak_next(3,-kmax:kmax,-kmax:kmax,-kmax:kmax))
    ak_prev=0.; ak_next=0.

    tprev = -1.

  END SUBROUTINE

!***********************************************************************
  SUBROUTINE read_force
    USE params
    USE forcing
    namelist /force/ do_force, do_helmh, a_helmh, k1, k2, ampl_turb, t_turn, t_turb, pk

    rewind (stdin); read (stdin,force)
    if (master) write (*,force)

    call init_selfg
  END SUBROUTINE

!***********************************************************************
  SUBROUTINE forceit (r,Ux,Uy,Uz,xdnr,ydnr,zdnr,dUxdt,dUydt,dUzdt)
!
!  Calculate a random force, confined to a shell in k-space, at
!  regular intervals in time.  We assume rho, P, and the sound
!  speed to be near unity.  The velocity amplitude should be of the
!  order ampl_turb.  The size (scale) of the driving motions is of the
!  order of mx*dx/k1.  Thus the turnover time is given by
!  t_turb*ampl_turb = mx*dx/k1; and the acceleration is of the order
!  ampl_turb/t_turb.
!
!  Note:  In order to restart properly, one would have to save the
!  previous value of iseed.
!
!  06-aug-93/aake:  bug corrected; iseed was not negative at start
!  09-aug-93/aake:  changed to allow restart w same random coeffs
!  21-feb-95/paolo: changed in order to use segments of constant
!                   time der. of acceleration (so continuous accel.)
!            franx(,y,z) are time deriv. of accel. (random, solenoidal);
!            pranx(,y,z) (and scr2(,3,4)) are accelerations. The variable
!            ``accel'' is now the time derivative of the acceleration.
!            ``t_turn'' has the same meaning as before (see comment above).
!  23-jan-96/aake:  bug corrected; ran1() -> 2.*ran1()-1. to span [-1,1]
!
!            Restart must be done AFTER t=t_turn.
!            ------------------------------------
!----------------------------------------------------------------------
  USE params
  USE forcing
  !USE selfgravity

  implicit none

  real, dimension(mx,my,mz):: r,Ux,Uy,Uz,xdnr,ydnr,zdnr,dUxdt,dUydt,dUzdt
  real, allocatable, dimension(:,:,:):: scr1,scr2,scr3,scr

  integer i,j,k,jx,jy,jz,nrand

  complex fxx,fyy,fzz,kx,ky,kz,corr,expikrx,expikry,expikrz

  real x, y, z, w, fact, test, fpow, accel, ran1s
  real eps, fk, average, f

  logical, save :: first_time=.true.
  logical debug

! Even if no external forcing, still do selfgravity (unless grav=0.)

  if (.not. do_force) then
    call selfgrav (r,xdnr,ydnr,zdnr,dUxdt,dUydt,dUzdt)
    return
  end if

!$omp master
  allocate(scr1(mx,my,mz), scr2(mx,my,mz), scr3(mx,my,mz), scr(mx,my,mz))
!$omp end master
!$omp barrier

!----------------------------------------------------------------------
  if (t .ne. tprev) then    ! prevent repeat at same t
    tprev = t
    eps = 1e-5
    
    if (master.and.idbg>0) then
      print *,'forceit: t,t_turb,first_time=',t,t_turb,first_time
      if (t_turb .le. t) print *,'new force'
    endif
    
    if (first_time) then
      ak_prev=0.; ak_next=0.
    endif
!--------------------------------------------- new random numbers:
!  Loop until t_turb > t
    do while (t_turb .le. t)
    ak_prev = ak_next                                                            ! advances a turnover time

    t_turb = t_turb + t_turn

    nrand = 0
    do jx=-kmax,kmax
      do jy=-kmax,kmax
        do jz=-kmax,kmax
          fk = sqrt(float(jx**2+jy**2+jz**2))
          if (fk .ge. k1-eps .and. fk .le. k2+eps) nrand=nrand+1
        enddo
      enddo
    enddo

!--------------------------------------------------------------------------------
    accel  = ampl_turb/t_turn/sqrt(float(nrand)/8.)    ! rms=1./8. per comp.
!--------------------------------------------------------------------------------

    if (master) print *,'random_force:t=',t,t_turb,nrand,iseed

! To obtain a Kolmogorov slope of the driving alone, one should have amplitudes
! a(k) that drop with k^(-11./6.), to have a(k)^2*k^2 = k^(-5./3.).  This ASSUMES
! that the amplitudes are proportional to the driving.  But the energy drain may
! be rather inversely proportional to the turnover time, which goes as k^(2./3.)

    ! pk = 11./6.
    ! pk = 7./6.

    fpow = 0.
    do jx=-kmax,kmax
      kx = cmplx (0., jx*2.*pi/sx)
      do jy=-kmax,kmax
        ky = cmplx (0., jy*2.*pi/sy)
        do jz=-kmax,kmax
          kz = cmplx (0., jz*2.*pi/sz)
          fk = sqrt(float(jx**2+jy**2+jz**2))
          if (fk .ge. k1-eps .and. fk .le. k2+eps) then
            fxx =   cexp(cmplx(0., 2.*pi*ran1s(iseed)))/fk**pk
            fyy =   cexp(cmplx(0., 2.*pi*ran1s(iseed)))/fk**pk
            if (mz.gt.1) then
              fzz = cexp(cmplx(0., 2.*pi*ran1s(iseed)))/fk**pk
            else
              fzz=0.
            end if

! solenoidal field:
!------------------
            if (do_helmh) then
               corr=(kx*fxx+ky*fyy+kz*fzz)/(kx*kx+ky*ky+kz*kz+1e-20) 

               if (jx.ne.0) fxx = fxx - a_helmh*corr*kx
               if (jy.ne.0) fyy = fyy - a_helmh*corr*ky
               if (jz.ne.0) fzz = fzz - a_helmh*corr*kz
            endif
!------------------
            ak_next(1,jx,jy,jz)=accel*fxx 
            ak_next(2,jx,jy,jz)=accel*fyy
            ak_next(3,jx,jy,jz)=accel*fzz 

            fact=1.
            if (jx.gt.0) fact=fact*0.5
            if (jy.gt.0) fact=fact*0.5
            if (jz.gt.0) fact=fact*0.5
            fpow = fpow+fact*(cabs(fxx)**2+cabs(fyy)**2+cabs(fzz)**2)

            if (master .and. debug(dbg_force)) then
              print '(1x,a,3i2,f6.3,3(f7.3,f6.3),5f6.3)', &
                    'random_force:k=',jx,jy,jz,fk,fxx,fyy,fzz,fact,zm(1),zmin
            endif
          endif
        enddo
      enddo
    enddo

    enddo ! while
  endif   ! t > t_prev
!------------------------------------------------------------------
  if (master.and.idbg>0) print *,'interpolate force'

!  Time interpolation of the force
!
  !w = 1.-exp(-(t+t_turn-t_turb)/t_turn)
  w = (t_turn-(t_turb-t))/t_turn
  w = 0.5*(1.-cos(w*pi))

  if (master.and.idbg>0.and.isubstep==1) &
    print *,'force: t-t_turb, t_turn, w:', t-t_turb, t_turn, w

  !$omp parallel do private(k)
  do k=1,mz
    scr1(:,:,k) = 0.
    scr2(:,:,k) = 0.
    scr3(:,:,k) = 0.
  end do

  do jx=-kmax,kmax
    kx = cmplx (0., jx*2.*pi/sx)
    do jy=-kmax,kmax
      ky = cmplx (0., jy*2.*pi/sy)
      do jz=-kmax,kmax
        kz = cmplx (0., jz*2.*pi/sz)
        fk = sqrt(float(jx**2+jy**2+jz**2))
        if (fk .ge. k1-eps .and. fk .le. k2+eps) then
          fact=1.
          if (jx.gt.0) fact=fact*0.5
          if (jy.gt.0) fact=fact*0.5
          if (jz.gt.0) fact=fact*0.5
          fxx = ak_prev(1,jx,jy,jz) * (1.0 - w) + ak_next(1,jx,jy,jz) * w
          fyy = ak_prev(2,jx,jy,jz) * (1.0 - w) + ak_next(2,jx,jy,jz) * w
          fzz = ak_prev(3,jx,jy,jz) * (1.0 - w) + ak_next(3,jx,jy,jz) * w
!$omp parallel do private(k,i,j,x,y,z,expikrx,expikry,expikrz)
          do k=1,mz
            z = zm(k)-zmin
            do j=1,my
              y = ym(j)-ymin
              do i=1,mx
                x = xm(i)-xmin
                expikrx = cexp(kx*(x-0.5*dx)+ky*y+kz*z)
                expikry = cexp(kx*x+ky*(y-0.5*dy)+kz*z)
                expikrz = cexp(kx*x+ky*y+kz*(z-0.5*dz))
                scr1(i,j,k) = scr1(i,j,k) + real(fxx*expikrx)
                scr2(i,j,k) = scr2(i,j,k) + real(fyy*expikry)
                scr3(i,j,k) = scr3(i,j,k) + real(fzz*expikrz)
              end do
            end do
          end do
        endif
      enddo
    enddo
  enddo
  
  if (debug(dbg_force)) then
    write(*,*) mpi_rank,' force: Ux'
    write(*,'(10e12.5)') scr1(1,1,:)
    write(*,*) mpi_rank,' force: xdnr'
    write(*,'(10e12.5)') xdnr(1,1,:)
  end if

! Force and velocity averages
!----------------------------

  if (do_trace) print *,'turbulence: average1'
  rhoav = 1./average(r)
!$omp parallel do private(k)
  do k=1,mz
    scr(:,:,k) = xdnr(:,:,k)*(scr1(:,:,k)+Ux(:,:,k)*(1./t_turn))*rhoav
  end do
  if (do_trace) print *,'turbulence: average2'
  xav = average(scr)

!$omp parallel do private(k)
  do k=1,mz
    scr(:,:,k) = ydnr(:,:,k)*(scr2(:,:,k)+Uy(:,:,k)*(1./t_turn))*rhoav
  end do
  if (do_trace) print *,'turbulence: average3'
  yav = average(scr)

!$omp parallel do private(k)
  do k=1,mz
    scr(:,:,k) = zdnr(:,:,k)*(scr3(:,:,k)+Uz(:,:,k)*(1./t_turn))*rhoav
  end do
  if (do_trace) print *,'turbulence: average4'
  zav = average(scr)

  if (first_time) print *,mpi_rank,'conservative =',conservative
  if (conservative) then
    !$omp parallel do private(k)
    do k=1,mz
      dUxdt(:,:,k) = dUxdt(:,:,k) + xdnr(:,:,k)*(scr1(:,:,k) - xav)
      dUydt(:,:,k) = dUydt(:,:,k) + ydnr(:,:,k)*(scr2(:,:,k) - yav)
      dUzdt(:,:,k) = dUzdt(:,:,k) + zdnr(:,:,k)*(scr3(:,:,k) - zav)
    end do
  else
    !$omp parallel do private(k)
    do k=1,mz
      dUxdt(:,:,k) = dUxdt(:,:,k) + (scr1(:,:,k) - xav)
      dUydt(:,:,k) = dUydt(:,:,k) + (scr2(:,:,k) - yav)
      dUzdt(:,:,k) = dUzdt(:,:,k) + (scr3(:,:,k) - zav)
    end do
  end if

  call dump(scr1,'force.dat',0)
  call dump(scr3,'force.dat',1)
  call dump(scr3,'force.dat',2)

  if (master.and.idbg>1) then
    call stats ('pranx', scr1)
    call stats ('prany', scr2)
    call stats ('pranz', scr3)
  end if

!$omp barrier
!$omp master
  deallocate (scr1,scr2,scr3,scr)
!$omp end master

  if (do_trace) print *,'selfg'
  call selfgrav (r,xdnr,ydnr,zdnr,dUxdt,dUydt,dUzdt)

  first_time = .false.  ! don't come back
END SUBROUTINE

