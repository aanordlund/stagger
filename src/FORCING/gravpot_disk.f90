! $Id: gravpot_disk.f90,v 1.14 2006/10/05 00:13:45 aake Exp $
!***********************************************************************
MODULE gravpot_m
  real(kind=8), allocatable, dimension(:,:):: rho_xy                    ! must be global
  logical,save:: first_time=.true.
END MODULE

!***********************************************************************
SUBROUTINE average_z (f, f_xy, mx, my, mz)
  USE params, only: izs, ize
  implicit none
  integer mx, my, mz
  real, dimension(mx,my,mz):: f
  real(kind=8), dimension(mx,my):: f_xy, scr
  real(kind=8) c
  integer ix, iy, iz

  !$omp single                                                          ! first to arrive ..
  do iy=1,my
  do ix=1,mx
    f_xy(ix,iy) = 0.                                                    ! .. initializes
  end do
  end do
  !$omp end single

  c = 1./mz
  do iy=1,my                                                            ! each OMP thread ..
  do ix=1,mx
    scr(ix,iy) = 0.
    do iz=izs,ize
      scr(ix,iy) = scr(ix,iy) + f(ix,iy,iz)*c                           ! .. adds up
    end do
  end do
  end do

  !$omp critical
  do iy=1,my
  do ix=1,mx
    f_xy(ix,iy) = f_xy(ix,iy) + scr(ix,iy)                              ! add thread contribs
  end do
  end do
  !$omp end critical
END

!***********************************************************************
SUBROUTINE gravpot (rho, phi)
  USE params
  implicit none
  real, dimension(mx,my,mz):: rho, phi
  logical omp_in_parallel

  if (omp_in_parallel()) then
    call gravpot_omp (rho, phi)
  else
    !$omp parallel
    call gravpot_omp (rho, phi)
    !$omp end parallel
  end if
END

!***********************************************************************
SUBROUTINE gravpot_omp (rho, phi)

!  Solve for the gravitational potential phi, given the density rho,
!  from the equation Laplace(phi) = grav*rho.

  USE params
  USE arrays, only: scr5, scr6
  USE forcing, only: grav, fbdry, isolated, soft
  USE gravpot_m
  implicit none

  real, dimension(mx,my,mz):: rho, phi

  real k2norm, rhoa, x, y, z, r, f, m_gas

  integer ix, iy, iz, nperif, iymax
  character(len=mid):: id = "$Id: gravpot_disk.f90,v 1.14 2006/10/05 00:13:45 aake Exp $"
!-----------------------------------------------------------------------
  call print_id (id)

  if (first_time) then
    call barrier_omp('gravpot1')
    !$omp master
    allocate (rho_xy(mx,my))
    first_time = .false.
    !$omp end master
    call barrier_omp('gravpot2')
  end if
  call dumpl(rho,'rho','phi.dmp',0)

  if (.not. isolated) then                                              ! periodic case
    call fft3df (rho, phi, mx, my, mz)                                  ! forward FFT

  else                                                                  ! isolated case
    call average_subr (rho, m_gas)                                      ! m_gas must NOT be global!
    m_gas = m_gas*sx*sy*sz                                              ! total gas mass
    !if (verbose) print *,'m_gas',m_gas

    do iz=izs,ize
    do iy=1,my
    do ix=1,mx
      scr6(ix,iy,iz) = rho(ix,iy,iz)
    end do
    end do
    end do
    !$omp barrier

    if (izs == 1) then                                                  ! at iz=1
      do iy=1,my
        do ix=1,mx
          scr6(ix,iy,1) = 0.                                            ! zap the density
        end do
      end do
    end if

    if (ize == mz) then                                                 ! at iz=mz
      do iy=1,my
        do ix=1,mx
          scr6(ix,iy,mz) = 0.                                           ! zap the density
        end do
      end do
    end if

    !$omp barrier
    call average_z (scr6, rho_xy, mx, my, mz)                           ! average over z
    !$omp barrier
 
    if (ize == mz) then
      do iy=1,my
        do ix=1,mx
          scr6(ix,iy,mz) = -fbdry*rho_xy(ix,iy)*(sz/dz)                 ! add projected negative mass
        end do
      end do
    end if
    !$omp barrier

    if (mpi_y == mpi_ny-1) then                                         ! on upper y-bdry xz-plane
      iymax = my-1
      do iz=izs,min(ize,mz-1)                                           ! avoid iz=mz edge
        z = zm(iz)
        do ix=1,mx
          x = xm(ix)
          r = sqrt(x**2+z**2+ym(my)**2)
          scr6(ix,my,iz) = -(2.*m_gas*dx*dz)*ym(my)/r**3                ! add central mass compensation
        end do
      end do
    else
      iymax = my
    end if
    !$omp barrier

    do iz=izs,min(ize,mz-1)                                             ! on upper x-bdry yz-plane
      z = zm(iz)
      do iy=1,iymax
        y = ym(iy)
        r = sqrt(xm(mx)**2+y**2+z**2)
        scr6(mx,iy,iz) = -(2.*m_gas*dz*dy)*xm(mx)/r**3                  ! add central mass compensation
      end do
    end do

    !$omp barrier
    call dumpl(scr6,'scr6','phi.dmp',1)
    call fft3df (scr6, phi, mx, my, mz)
    call dumpl(phi,'trf','phi.dmp',2)
  end if

  call fft3d_k2 (scr5, dx, dy, dz, mx, my, mz)                          ! compute k**2
  k2norm = 2.*(soft*(sx/mx)/pi)**2                                      ! normalization factor

  !$omp barrier
  call dumpl(scr5,'k2','phi.dmp',3)
  do iz=izs,ize
  do iy=1,my
  do ix=1,mx
    phi(ix,iy,iz) = -phi(ix,iy,iz) &
      *grav*4.*pi/(scr5(ix,iy,iz)+1e-10)*exp(-k2norm*scr5(ix,iy,iz))    ! softening
  end do
  end do
  end do

  if (mpi_y == 0) phi(1,1,1) = 0.0                                      ! no mean phi
  call fft3db (phi, phi, mx, my, mz)                                    ! reverse FFT
  call dumpl(phi,'phi','phi.dmp',4)
END
