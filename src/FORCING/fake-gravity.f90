! $Id: fake-gravity.f90,v 1.4 2005/11/23 05:31:39 aake Exp $
!**********************************************************************
MODULE forcing

logical do_force
real gx, gy,gz, ay
integer ky, ny
logical, parameter:: conservative=.true.

END MODULE

!***********************************************************************
SUBROUTINE init_force
  USE params
  USE forcing
  namelist /force/ do_force, gx, gy, gz, ay, ky, ny

  do_force = .true.
  gx = 0.
  gy = 0.
  gz = 0.
  ay = 1.                   ! acc. of gravity drop to half its normal value
  ny = 5                    ! when 'iy' varies with 'ny'
  ky = 15                   ! starting at iy=ky
  read (*,force)
  write (*,force)

END SUBROUTINE

!***********************************************************************
SUBROUTINE forceit (r,Ux,Uy,Uz,xdnr,ydnr,zdnr,dpxdt,dpydt,dpzdt)
  USE params
  USE forcing
  implicit none
  integer iy, iz
  real f
  real, dimension(mx,my,mz):: r,Ux,Uy,Uz,xdnr,ydnr,zdnr,dpxdt,dpydt,dpzdt
!hpf$ distribute(*,*,block):: r,Ux,Uy,Uz,xdnr,ydnr,zdnr,dpxdt,dpydt,dpzdt

  if (gx.ne.0.) then
!$omp parallel do private(iz)
    do iz=1,mz
      dpxdt(:,:,iz) = dpxdt(:,:,iz) + gx*xdnr(:,:,iz)
    end do
  end if

  if (gy.ne.0.) then
!$omp parallel do private(iz)
    do iz=1,mz
    do iy=1,my
      f = (ay+(1.-ay)*0.5*(1.+cos(min(max(ky-iy,0),ny)*pi/ny)))
      dpydt(:,iy,iz) = dpydt(:,iy,iz) + f*gy*ydnr(:,iy,iz)
    end do
    end do
  end if

  if (gz.ne.0.) then
!$omp parallel do private(iz)
    do iz=1,mz
      dpzdt(:,:,iz) = dpzdt(:,:,iz) + gz*zdnr(:,:,iz)
    end do
  end if

  return
END SUBROUTINE

