! $Id: kink_force.f90,v 1.3 2007/04/03 11:09:26 aake Exp $
!***********************************************************************
!  The experiment forcing file must contain the following modules and
!  subroutines:
!
!  forcing      module with forcing parameters
!  init_force   called only once, at the beginning of a job
!  read_force   reads paramters; called from init_force and check.f90
!  forceit      adds forces, if any, to the dp[xyz]dt variables
!
!***********************************************************************
MODULE forcing
  implicit none
  real:: ampl, beta, deltax, deltaz, p, a, ee_cool, q_cool
  logical, save:: conservative=.true.                                   ! MUST be included = true for linear momemnta, false for velocities
END MODULE

!***********************************************************************
SUBROUTINE init_force
  USE params
  USE forcing
  implicit none
  character(len=mid):: id="$Id: kink_force.f90,v 1.3 2007/04/03 11:09:26 aake Exp $"

  call print_id (id)

  ampl = 0.3                                                            ! driver amplitude
  beta = 0.1                                                            ! ratio of magnetic to gas pressure
  deltax = 0.                                                           ! driver y-shift (size units)
  deltaz = 0.                                                           ! driver z-shift (size units)
  p = 4.                                                                ! power exponent of profile
  a = 0.25                                                              ! radius of tube (size units)
  call read_force                                                       ! read params
  ee_cool = 0.5*beta/(gamma-1.)                                         ! initial energy per unit mass
  q_cool = 100.                                                         ! strong cooling is needed!
END SUBROUTINE

!***********************************************************************
SUBROUTINE read_force
  USE params
  USE forcing
  implicit none
  namelist /force/ ampl, beta, deltax, deltaz, p, a

  read  (stdin,force)                                                   ! read parameter namelist
  if (master) write (*,force)                                           ! print, but only in master thread
END SUBROUTINE

!***********************************************************************
SUBROUTINE forceit (r,Ux,Uy,Uz,scr1,scr2,scr3,dpxdt,dpydt,dpzdt)
  USE params
  implicit none
  real, dimension(mx,my,mz):: r,Ux,Uy,Uz,scr1,scr2,scr3,dpxdt,dpydt,dpzdt
                                                                        ! do nothing; we force through the E-field
END SUBROUTINE

!***********************************************************************
SUBROUTINE E_driver (Ex, Ez)
!
! Set electric fields in the first layer outside the model proper.
! Called from efield_boundary.  We use the mesh coordinates xm(),
! zm(), xmdn(), and zmdn() from the params module; these give absolute
! coordinates, that work also when running under MPI, when each
! process only has a sub-domain.
!
!-----------------------------------------------------------------------
  USE params
  USE forcing
  implicit none
  real, dimension(mx,my,mz) :: Ex, Ez
  real rx, rz, r2, f
  integer ix, iz

  do iz=izs,ize                                                         ! OpenMP z-loop limits
    do ix=1,mx
      rx = xm(ix) + deltax                                              ! xm(ix) is at cell center 
      rz = zmdn(iz) + deltaz                                            ! Ex is shifted half a mesh in z
      r2 = (rx**2 + rz**2)/a**2                                         ! normalized radius squared
      f = exp(-r2**(p/2.))                                              ! exponent power
      Ex(ix,lb,iz) = +ampl*rx*f                                         ! linear increase, with cut-off

      rx = xmdn(ix) + deltax                                            ! Ez is shifted half a mesh in x
      rz = zm(iz) + deltax
      r2 = (rx**2 + rz**2)/a**2
      f = exp(-r2**(p/2.))
      Ez(ix,lb,iz) = +ampl*rz*f                                         ! lb is half a mesh outside the box

      rx = xm(ix) - deltax
      rz = zmdn(iz) - deltaz                                            ! Ex is shifted half a mesh in z
      r2 = (rx**2 + rz**2)/a**2
      f = exp(-r2**(p/2.))
      Ex(ix,ub+1,iz) = -ampl*rx*f                                       ! ub+1 is half a mesh outside the box

      rx = xmdn(ix) - deltax                                            ! Ez is shifted half a mesh in x
      rz = zm(iz) - deltaz
      r2 = (rx**2 + rz**2)/a**2
      f = exp(-r2**(p/2.))
      Ez(ix,ub+1,iz) = -ampl*rz*f
    end do
  end do
END subroutine

