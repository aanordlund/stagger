! $Id: wengen4.f90,v 1.3 2012/12/31 16:19:12 aake Exp $
!**********************************************************************
MODULE forcing

  implicit none
  logical do_force, do_selfg, do_friction, isolated
  real grav,M_sun,r0,r1,r2,r3,p3,soft,rho0,ee0,rho_friction,fbdry,frho
  real rho_lim,e_lim,p_sigma,p_temp
  real, allocatable, dimension(:,:,:):: rh1
  real, allocatable, dimension(:,:):: sigm
  logical, parameter:: conservative=.true.

END MODULE

!***********************************************************************
SUBROUTINE init_force
    USE params
    USE forcing
    implicit none
    character(len=mid):: id='$Id: wengen4.f90,v 1.3 2012/12/31 16:19:12 aake Exp $'

    if (id .eq. ' ') return
    print *,id
    id = ' '

    do_force = .true.
    do_selfg = .false.
    isolated = .true.
    M_sun = 1.
    r0 = 0.5*sx
    r1 = 0.01*sx
    r2 = 0.11*sx
    r3 = 0.01*sx
    p3 = 4.
    omega = 13.
    grav = 20.
    soft = 3.
    rho0 = 1.
    ee0 = 0.8
    fbdry = 0.5
    frho = 1.
    rho_lim = 1e-4
    e_lim = 1e-6
    p_sigma = 2.0
    p_temp = 0.5

    call read_force

END SUBROUTINE

!***********************************************************************
SUBROUTINE read_force
    USE params
    USE forcing
    namelist /force/do_selfg,isolated,do_friction,grav,soft,M_sun,r0,r1,r2,r3, &
                    omega,rho0,ee0,rho_friction,fbdry,frho,rho_lim,e_lim,p_sigma,p_temp

    read (*,force)
    write (*,force)

!    call init_selfg
END SUBROUTINE

!***********************************************************************
SUBROUTINE forceit (rho,Ux,Uy,Uz,xdnr,ydnr,zdnr,dpxdt,dpydt,dpzdt)
  USE params
!
  USE params
  USE forcing

  implicit none
  real, dimension(mx,my,mz):: rho,Ux,Uy,Uz,xdnr,ydnr,zdnr,dpxdt,dpydt,dpzdt
  real, dimension(mx,my,mz):: dphi,pxz,phi
  real x,y,z,xz,r,exp1,omegar,dphir,taper,friction,ct
  integer ix,iy,iz

!-----------------------------------------------------------------------
!  Compute selfgravity from the gas, and add the potential from the
!  central star.
!-----------------------------------------------------------------------
  if (do_selfg) then
    call gravpot(rho,phi)
!$omp parallel do private (iz,iy,ix,z,y,x,r)
    do iz=1,mz
      z = zm(iz)
      do iy=1,my
        y = ym(iy)
        do ix=1,mx
          x = xm(ix)
          r = sqrt(x**2+y**2+z**2)
          r = sqrt(r**2+0.25*max(r3-r,0.)**2)
          phi(ix,iy,iz) = phi(ix,iy,iz) -grav*M_sun/r
        end do
      end do
    end do
  else
!$omp parallel do private (iz,iy,ix,z,y,x,r)
    do iz=1,mz
      z = zm(iz)
      do iy=1,my
        y = ym(iy)
        do ix=1,mx
          x = xm(ix)
          r = sqrt(x**2+y**2+z**2)
          r = sqrt(r**2+0.25*max(r3-r,0.)**2)
          phi(ix,iy,iz) = -grav*M_sun/r
        end do
      end do
    end do
  end if

!-----------------------------------------------------------------------
!  Compensate for the rotating coordinate system.  As shown by a test
!  with rigid rotation, it is the omega that needs to be a function of r,
!  not the whole acceleration term.  And as per a case with rotation,
!  selfgravity, and no motions in the rotating coordinate system near
!  the transition to the "exterior", the gravity term needs to be tapered
!  off in proportion to omegar**2.
!-----------------------------------------------------------------------

  call zup_set (Uz, dphi)
  call xdn_set (dphi, pxz)
  call ddxdn_set (phi, dphi)
!$omp parallel do private (iz,iy,ix,z,y,x,r,xz,taper,omegar,dphir)
  do iz=1,mz
    z = zm(iz)
    do iy=1,my
      y = ym(iy)
      do ix=1,mx
        x = xm(ix) - 0.5*(xm(mx/2+1)-xm(mx/2))
        r = sqrt(x**2+y**2+z**2)
        xz = sqrt(x**2+z**2)
        x = x*min(xz,sx*0.5)/max(xz,1e-6*sx)
        taper = exp1((r0-r)/r1)
        omegar = omega*taper
        dphir = dphi(ix,iy,iz)*taper**2
        dpxdt(ix,iy,iz) = dpxdt(ix,iy,iz) + &
           xdnr(ix,iy,iz)*(omegar*(omegar*x - 2.*pxz(ix,iy,iz)) - dphir)
      end do
    end do
  end do

  call xup_set (Ux, dphi)
  call zdn_set (dphi, pxz)
  call ddzdn_set (phi, dphi)
!$omp parallel do private (iz,iy,ix,z,y,x,r,xz,taper,omegar,dphir)
  do iz=1,mz
    do iy=1,my
      y = ym(iy)
      do ix=1,mx
        z = zm(iz) - 0.5*(zm(mz/2+1)-zm(mz/2))
        x = xm(ix)
        r = sqrt(x**2+y**2+z**2)
        xz = sqrt(x**2+z**2)
        z = z*min(xz,sx*0.5)/max(xz,1e-6*sx)
        taper = exp1((r0-r)/r1)
        omegar = omega*taper
        dphir = dphi(ix,iy,iz)*taper**2
        dpzdt(ix,iy,iz) = dpzdt(ix,iy,iz) + &
           zdnr(ix,iy,iz)*(omegar*(omegar*z + 2.*pxz(ix,iy,iz)) - dphir)
      end do
    end do
  end do

  call ddydn_set (phi, dphi)
!$omp parallel do private (iz,iy,ix,z,y,x,r,taper,dphir)
  do iz=1,mz
    do iy=1,my
      do ix=1,mx
        dpydt(ix,iy,iz) = dpydt(ix,iy,iz)-ydnr(ix,iy,iz)*dphi(ix,iy,iz)
      end do
    end do
  end do

  if (do_friction) then
    ct = 0.1/dt
!$omp parallel do private (iz,iy,ix,friction)
    do iz=1,mz
    do iy=1,my
    do ix=1,mx
      friction = ct*(rho_friction/(ydnr(ix,iy,iz)+rho_friction))**2
      dpydt(ix,iy,iz) = dpydt(ix,iy,iz) - friction*ydnr(ix,iy,iz)*Uy(ix,iy,iz)
    end do
    end do
    end do
  end if

END SUBROUTINE
