! $Id: planetary_disk.f90,v 1.11 2006/05/24 19:37:31 aake Exp $
!**********************************************************************
MODULE forcing

  implicit none
  logical do_force
  real M_sun,r0,r1,r2,eps,rho_min,rho_max
  logical, parameter:: conservative=.true.

END MODULE

!***********************************************************************
SUBROUTINE init_force
    USE params
    USE forcing
    integer iz
    character(len=80):: id='$Id: planetary_disk.f90,v 1.11 2006/05/24 19:37:31 aake Exp $'

    if (id.ne.'') print *,id
    id=''

    M_sun = 1.
    r0 = sx/2.
    r1 = 1.
    eps = 1e-4
    rho_max = 1e30
    rho_min = 1e2

    call read_force

END SUBROUTINE

!***********************************************************************
SUBROUTINE read_force
    USE params
    USE forcing
    namelist /force/do_force,M_sun,r0,r1,eps,rho_min,rho_max

    if (stdin.ge.0) then
      rewind (stdin); read (stdin,force)
    else
      read (*,force)
    end if
    write (*,force)
    r1 = r1*dx
    r2 = -r1*alog(eps)
    r0 = r0-r2/2.

    call init_selfg
END SUBROUTINE

!***********************************************************************
SUBROUTINE forceit (rho,Ux,Uy,Uz,xdnr,ydnr,zdnr,dpxdt,dpydt,dpzdt)
!
  USE params
  USE selfgravity
  USE forcing

  implicit none
  real, dimension(mx,my,mz):: rho,Ux,Uy,Uz,xdnr,ydnr,zdnr,dpxdt,dpydt,dpzdt
  real, allocatable, dimension(:,:,:):: pxy
  real rho_save,x,y,z,xy,r,exp1,omegar,dphir,taper
  integer ix,iy,iz

  rho_save = rho(mx/2+1,my/2+1,mz/2+1)
  rho(mx/2+1,my/2+1,mz/2+1) = rho(mx/2+1,my/2+1,mz/2+1) + M_sun/(dx*dy*dz)

  allocate (phi(mx,my,mz), dphi(mx,my,mz), pxy(mx,my,mz))
  call gravpot(rho,phi)

!-----------------------------------------------------------------------
!  Compensate for the rotating coordinate system.  As shown by the test
!  with rigid rotation, it is the omega that needs to be a function of r,
!  not the whole acceleration term.  And as per a case with rotation,
!  selfgravity, and no motions in the rotating coordinate system near
!  the transition to the "exterior", the gravity term needs to be tapered
!  off in proportion to omegar**2.
!-----------------------------------------------------------------------

  call ddxdn_set(phi,dphi)
  call yup_set(Uy,scratch)
  call xdn_set(scratch,pxy)

!$omp parallel do private(ix,iy,iz,x,y,z,xy,r,taper,omegar,dphir)
  do iz=1,mz
    z = (iz-1-mz/2)*dz
    do iy=1,my
      y = (iy-1-my/2)*dy
      do ix=1,mx
        x = (ix-1-mx/2-.5)*dx
        r = sqrt(x**2+y**2+z**2)
        xy = sqrt(x**2+y**2)
        x = x*min(xy,sx*0.5)/xy
        taper = exp1((r-r0)/r1)
        omegar = omega*taper
        dphir = dphi(ix,iy,iz)*taper**2
        dpxdt(ix,iy,iz) = dpxdt(ix,iy,iz) + &
           xdnr(ix,iy,iz)*(omegar*(omegar*x - 2.*pxy(ix,iy,iz)) - dphir)
      end do
    end do
  end do

  call ddydn_set(phi,dphi)
  call xup_set(Uy,scratch)
  call ydn_set(scratch,pxy)

!$omp parallel do private(ix,iy,iz,x,y,z,xy,r,taper,omegar,dphir)
  do iz=1,mz
    z = (iz-1-mz/2)*dz
    do iy=1,my
      do ix=1,mx
        y = (iy-1-my/2-.5)*dy
        x = (ix-1-mx/2)*dx
        r = sqrt(x**2+y**2+z**2)
        xy = sqrt(x**2+y**2)
        y = y*min(xy,sx*0.5)/xy
        taper = exp1((r-r0)/r1)
        omegar = omega*taper
        dphir = dphi(ix,iy,iz)*taper**2
        dpydt(ix,iy,iz) = dpydt(ix,iy,iz) + &
           ydnr(ix,iy,iz)*(omegar*(omegar*y + 2.*pxy(ix,iy,iz)) - dphir)
      end do
    end do
  end do

  call ddzdn_set(phi,dphi)

!$omp parallel do private(ix,iy,iz,x,y,z,r,taper,omegar,dphir)
  do iz=1,mz
    z = (iz-1-mz/2-.5)*dz
    do iy=1,my
      y = (iy-1-my/2)*dy
      do ix=1,mx
        x = (ix-1-mx/2)*dx
        r = sqrt(x**2+y**2+z**2)
        taper = exp1((r-r0)/r1)
        dphir = dphi(ix,iy,iz)*taper**2
        dpzdt(ix,iy,iz) = dpzdt(ix,iy,iz)-zdnr(ix,iy,iz)*dphir
      end do
    end do
  end do
  deallocate (phi, dphi, pxy)
  rho(mx/2+1,my/2+1,mz/2+1) = rho_save

END SUBROUTINE

