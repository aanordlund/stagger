! $Id: global_disk.f90,v 1.31 2006/11/14 14:13:12 rbjk Exp $
!**********************************************************************
MODULE forcing

  implicit none
  logical do_force, do_selfg, do_friction, isolated
  real grav,M_sun,r0,r1,r2,r3,p3,soft,rho0,ee0,rho_friction,fbdry,frho
  real rho_lim,e_lim,p_sigma,p_temp
  real, allocatable, dimension(:,:,:):: dphi,pxy,phi,vk
  real, allocatable, dimension(:,:):: sigm
  logical, parameter:: conservative=.true.

END MODULE

!***********************************************************************
SUBROUTINE init_force
    USE params
    USE forcing
    implicit none
    character(len=mid):: id='$Id: global_disk.f90,v 1.31 2006/11/14 14:13:12 rbjk Exp $'

    call print_id (id)

    do_force = .true.
    do_selfg = .false.
    isolated = .true.
    M_sun = 1.
    r0 = 0.5*sx
    r1 = 0.01*sx
    r2 = 0.11*sx
    r3 = 0.01*sx
    p3 = 4.
    omega = 13.
    grav = 20.
    soft = 3.
    rho0 = 1.
    ee0 = 0.8
    fbdry = 0.5
    frho = 1.
    rho_lim = 1e-4
    e_lim = 1e-6
    p_sigma = 2.0
    p_temp = 0.5

    call read_force
   
    allocate (dphi(mx,my,mz), phi(mx,my,mz), pxy(mx,my,mz), vk(mx,my,mz))

END SUBROUTINE

!***********************************************************************
SUBROUTINE read_force
    USE params
    USE forcing
    namelist /force/do_force,do_selfg,isolated,do_friction,grav,soft,M_sun,r0,r1,r2,r3, &
                    omega,rho0,ee0,rho_friction,fbdry,frho,rho_lim,e_lim,p_sigma,p_temp

    rewind (stdin); read (stdin,force)
    if (master) write (*,force)

!    call init_selfg
END SUBROUTINE

!***********************************************************************
SUBROUTINE forceit (rho,Ux,Uy,Uz,xdnr,ydnr,zdnr,dpxdt,dpydt,dpzdt)
  USE params
  implicit none
  real, dimension(mx,my,mz):: rho,Ux,Uy,Uz,xdnr,ydnr,zdnr,dpxdt,dpydt,dpzdt
  logical omp_in_parallel

  if (omp_in_parallel()) then
    call forceit_omp (rho,Ux,Uy,Uz,xdnr,ydnr,zdnr,dpxdt,dpydt,dpzdt)
  else
    !$omp parallel
    call forceit_omp (rho,Ux,Uy,Uz,xdnr,ydnr,zdnr,dpxdt,dpydt,dpzdt)
    !$omp end parallel
  end if
END

!***********************************************************************
SUBROUTINE forceit_omp (rho,Ux,Uy,Uz,xdnr,ydnr,zdnr,dpxdt,dpydt,dpzdt)
  USE params
  USE forcing

  implicit none
  real, dimension(mx,my,mz):: rho,Ux,Uy,Uz,xdnr,ydnr,zdnr,dpxdt,dpydt,dpzdt
  real x,y,z,xy,r,exp1,omegar,dphir,taper,friction,ct
  integer ix,iy,iz

  if (.not. do_force) return

!-----------------------------------------------------------------------
!  Compute selfgravity from the gas, and add the potential from the
!  central star.
!-----------------------------------------------------------------------
  if (do_selfg) then
    !$omp barrier
    call gravpot (rho, phi)
    !$omp barrier
    do iz=izs,ize
      z = zm(iz)
      do iy=1,my
        y = ym(iy)
        do ix=1,mx
          x = xm(ix)
          r = sqrt(x**2+y**2+z**2+r3**2)
          phi(ix,iy,iz) = phi(ix,iy,iz) -grav*M_sun/r
        end do
      end do
    end do
  else
    !$omp barrier
    do iz=izs,ize
      z = zm(iz)
      do iy=1,my
        y = ym(iy)
        do ix=1,mx
          x = xm(ix)
          r = sqrt(x**2+y**2+z**2+r3**2)
          phi(ix,iy,iz) = -grav*M_sun/r
        end do
      end do
    end do
  end if

!-----------------------------------------------------------------------
!  Compensate for the rotating coordinate system.  As shown by a test
!  with rigid rotation, it is the omega that needs to be a function of r,
!  not the whole acceleration term.  And as per a case with rotation,
!  selfgravity, and no motions in the rotating coordinate system near
!  the transition to the "exterior", the gravity term needs to be tapered
!  off in proportion to omegar**2.
!-----------------------------------------------------------------------

  !$omp barrier
  call yup_set (Uy, dphi)
  !$omp barrier
  call xdn_set (dphi, pxy)
  !$omp barrier
  call ddxdn_set (phi, dphi)
  call xup_set (dphi, scratch)
  !$omp barrier
  do iz=izs,ize
    z = zm(iz)
    do iy=1,my
      y = ym(iy)
      do ix=1,mx
        x = xm(ix) - 0.5*(xm(mx/2+1)-xm(mx/2))
        r = sqrt(x**2+y**2+z**2)
        xy = sqrt(x**2+y**2)
        x = x*min(xy,sx*0.5)/max(xy,1e-6*sx)
        taper = exp1((r0-r)/r1)
        omegar = omega*taper
        dphir = dphi(ix,iy,iz)*taper**2
        dpxdt(ix,iy,iz) = dpxdt(ix,iy,iz) + &
           xdnr(ix,iy,iz)*(omegar*(omegar*x - 2.*pxy(ix,iy,iz)) - dphir)
        vk(ix,iy,iz) = scratch(ix,iy,iz)**2
      end do
    end do
  end do

  !$omp barrier
  call xup_set (Ux, dphi)
  !$omp barrier
  call ydn_set (dphi, pxy)
  !$omp barrier
  call ddydn_set (phi, dphi)
  call yup_set (dphi, scratch)
  !$omp barrier
  do iz=izs,ize
    z = zm(iz)
    do iy=1,my
      do ix=1,mx
        y = ym(iy) - 0.5*(ym(my/2+1)-ym(my/2))
        x = xm(ix)
        r = sqrt(x**2+y**2+z**2)
        xy = sqrt(x**2+y**2)
        y = y*min(xy,sx*0.5)/max(xy,1e-6*sx)
        taper = exp1((r0-r)/r1)
        omegar = omega*taper
        dphir = dphi(ix,iy,iz)*taper**2
        dpydt(ix,iy,iz) = dpydt(ix,iy,iz) + &
           ydnr(ix,iy,iz)*(omegar*(omegar*y + 2.*pxy(ix,iy,iz)) - dphir)
        vk(ix,iy,iz) = sqrt(vk(ix,iy,iz) + scratch(ix,iy,iz)**2)
      end do
    end do
  end do

  !$omp barrier
  call ddzdn_set (phi, dphi)
  !$omp barrier
  do iz=izs,ize
    do iy=1,my
      do ix=1,mx
        dpzdt(ix,iy,iz) = dpzdt(ix,iy,iz)-zdnr(ix,iy,iz)*dphi(ix,iy,iz)
      end do
    end do
  end do

  !$omp barrier
  if (do_friction) then
    ct = 0.1/dt
    do iz=izs,ize
    do iy=1,my
    do ix=1,mx
      friction = ct*(rho_friction/(zdnr(ix,iy,iz)+rho_friction))**2
      dpzdt(ix,iy,iz) = dpzdt(ix,iy,iz) - friction*zdnr(ix,iy,iz)*Uz(ix,iy,iz)
    end do
    end do
    end do
  end if

  call xdn_set (vk, xdnr)
  call ydn_set (vk, ydnr)
  do iz=izs,ize
    z = zm(iz)
    do iy=1,my
      do ix=1,mx
        y = ym(iy)
        x = xmdn(ix)
        xy = sqrt(x**2+y**2)+1e-20
        y = y*min(xy,sx*0.5)/max(xy,1e-6*sx)
        r  = sqrt(x**2+y**2+z**2)
        taper = exp1((r0-r)/r1)
        xdnr(ix,iy,iz) = +y*(sqrt(xdnr(ix,iy,iz)/xy)-omega)*taper
      end do
    end do
    do iy=1,my   
      do ix=1,mx      
        y = ymdn(iy)  
        x = xm(ix)        
        xy = sqrt(x**2+y**2)+1e-20
        y = y*min(xy,sx*0.5)/max(xy,1e-6*sx)
        r = sqrt(x**2+y**2+z**2)
        taper = exp1((r0-r)/r1)
        ydnr(ix,iy,iz) = -x*(sqrt(ydnr(ix,iy,iz)/xy)-omega)*taper
      end do
    end do
  end do
  call dumpl(Ux,'Ux','kepler.dmp',0)
  call dumpl(Uy,'Uy','kepler.dmp',1)
  call dumpl(xdnr,'xdnr','kepler.dmp',2)
  call dumpl(ydnr,'ydnr','kepler.dmp',3)
  call dumpl(vk,'vk','kepler.dmp',4)
  call dumpl(phi,'phi','kepler.dmp',5)
  
  call gravpot (rho,phi)
  call dumpl(phi,'phi','phi.dat',0)
 
END SUBROUTINE
