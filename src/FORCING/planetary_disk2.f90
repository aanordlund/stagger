! $Id: planetary_disk2.f90,v 1.6 2005/11/23 05:31:40 aake Exp $
!**********************************************************************
MODULE forcing

  implicit none
  logical do_force, do_selfg, isolated
  real grav,M_sun,r0,r1,r2,r3,p3,eps,soft,rho0,ee0,ee1,hy
  logical, parameter:: conservative=.true.

END MODULE

!***********************************************************************
SUBROUTINE init_force
    USE params
    USE forcing
    integer iz
    character(len=80):: id='$Id: planetary_disk2.f90,v 1.6 2005/11/23 05:31:40 aake Exp $'

    if (id .eq. ' ') return
    id = ' '

    do_force = .true.
    do_selfg = .false.
    isolated = .true.
    M_sun = 1.
    r0 = 0.5*sx
    r1 = 0.01*sx
    r2 = 0.11*sx
    r3 = 0.01*sx
    p3 = 4.
    eps = 1e-4
    omega = 3.
    grav = 1.
    soft = 3.
    rho0 = 1.
    ee0 = 1.
    ee1 = 10.
    hy = 0.1

    call read_force

END SUBROUTINE

!***********************************************************************
SUBROUTINE read_force
    USE params
    USE forcing
    namelist /force/do_selfg,grav,M_sun,r0,r1,r2,r3,omega,soft,rho0,ee0,ee1

    read (*,force)
    write (*,force)

!    call init_selfg
END SUBROUTINE

!***********************************************************************
SUBROUTINE forceit (rho,Ux,Uy,Uz,xdnr,ydnr,zdnr,dpxdt,dpydt,dpzdt)
  USE params
!
  USE params
  USE forcing
  USE stagger
!  USE selfgravity

  implicit none
  real, dimension(mx,my,mz):: rho,Ux,Uy,Uz,xdnr,ydnr,zdnr,dpxdt,dpydt,dpzdt
  real, dimension(mx,my,mz):: dphi,pxz,phi
  real rho_save,x,y,z,xz,r,exp1,omegar,dphir,taper
  integer ix,iy,iz
    
!  rho_save = rho(mx/2+1,my/2+1,mz/2+1)
!  rho(mx/2+1,my/2+1,mz/2+1) = rho(mx/2+1,my/2+1,mz/2+1) + M_sun/(dx*dy*dz)

  if (do_selfg) then
    call gravpot(rho,phi)
!$omp parallel do private (iz,iy,ix,z,y,x,r)
    do iz=1,mz
      z = zm(iz)
      do iy=1,my
        y = ym(iy)
        do ix=1,mx
          x = xm(ix)
          r = sqrt(x**2+y**2+z**2+r3**2)
          phi(ix,iy,iz) = phi(ix,iy,iz) -grav*M_sun/r
        end do
      end do
    end do
  else
!$omp parallel do private (iz,iy,ix,z,y,x,r)
    do iz=1,mz
      z = zm(iz)
      do iy=1,my
        y = ym(iy)
        do ix=1,mx
          x = xm(ix)
          r = sqrt(x**2+y**2+z**2+r3**2)
          phi(ix,iy,iz) = -grav*M_sun/r
        end do
      end do
    end do
  end if

!-----------------------------------------------------------------------
!  Compensate for the rotating coordinate system.  As shown by a test
!  with rigid rotation, it is the omega that needs to be a function of r,
!  not the whole acceleration term.  And as per a case with rotation,
!  selfgravity, and no motions in the rotating coordinate system near
!  the transition to the "exterior", the gravity term needs to be tapered
!  off in proportion to omegar**2.
!-----------------------------------------------------------------------

  call zup_set (Uz, dphi)
  call xdn_set (dphi, pxz)
  call ddxdn_set (phi, dphi)
!$omp parallel do private (iz,iy,ix,z,y,x,r,xz,taper,omegar,dphir)
  do iz=1,mz
    z = zm(iz)
    do iy=1,my
      y = ym(iy)
      do ix=1,mx
        x = xm(ix) - 0.5*(xm(2)-xm(1))
        r = sqrt(x**2+y**2+z**2)
        xz = sqrt(x**2+z**2)
        x = x*min(xz,sx*0.5)/max(xz,1e-6*sx)
        taper = exp1((r0-r)/r1)
        omegar = omega*taper
        dphir = dphi(ix,iy,iz)*taper**2
        dpxdt(ix,iy,iz) = dpxdt(ix,iy,iz) + &
           xdnr(ix,iy,iz)*(omegar*(omegar*x - 2.*pxz(ix,iy,iz)) - dphir)
      end do
    end do
  end do

  call xup_set (Ux, dphi)
  call zdn_set (dphi, pxz)
  call ddzdn_set (phi, dphi)
!$omp parallel do private (iz,iy,ix,z,y,x,r,xz,taper,omegar,dphir)
  do iz=1,mz
    do iy=1,my
      y = ym(iy)
      do ix=1,mx
        z = zm(iz) - 0.5*(zm(2)-zm(1))
        x = xm(ix)
        r = sqrt(x**2+y**2+z**2)
        xz = sqrt(x**2+z**2)
        z = z*min(xz,sx*0.5)/xz
        taper = exp1((r0-r)/r1)
        omegar = omega*taper
        dphir = dphi(ix,iy,iz)*taper**2
        dpzdt(ix,iy,iz) = dpzdt(ix,iy,iz) + &
           zdnr(ix,iy,iz)*(omegar*(omegar*z + 2.*pxz(ix,iy,iz)) - dphir)
      end do
    end do
  end do

  call ddydn_set (phi, dphi)
!$omp parallel do private (iz,iy,ix,z,y,x,r,taper,dphir)
  do iz=1,mz
    z = zm(iz)
    do iy=1,my
      y = ym(iy) - 0.5*(ym(2)-ym(1))
      do ix=1,mx
        x = xm(ix)
        r = sqrt(x**2+y**2+z**2)
        taper = exp1((r0-r)/r1)
        dphir = dphi(ix,iy,iz)*taper**2
        dpydt(ix,iy,iz) = dpydt(ix,iy,iz)-ydnr(ix,iy,iz)*dphir
      end do
    end do
  end do
!  rho(mx/2+1,my/2+1,mz/2+1) = rho_save

END SUBROUTINE
