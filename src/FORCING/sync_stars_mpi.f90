! $Id: sync_stars_mpi.f90,v 1.1 2009/09/01 21:25:30 aake Exp $
!*******************************************************************************
SUBROUTINE sync_stars                                                           ! syncronize star info
  USE params
  USE mpi_mod, void=>verbose, xsend=>send, xrecv=>recv
  USE stars_m, only: star_t, star, pstars, nstars, verbose, need_sync
  implicit none
  real xmin_rank, ymin_rank, zmin_rank, xmax_rank, ymax_rank, zmax_rank
  integer i, rank, xyz(3)
  integer                                   :: owner                            ! star owner
  integer                                   :: mstars                           ! new number of stars
  integer                                   :: istars                           ! master incremental number of stars
  integer                                   :: newstars                         ! new stars from other domain
  integer                                   :: send(2), recv(2,0:mpi_size-1)    ! size info
  type(star_t), allocatable, dimension(:)   :: old
  type(star_t), allocatable, dimension(:,:) :: all
!...............................................................................
  if (verbose>2 .and. nstars>0) print*,mpi_rank,':f ',nstars,star(nstars)%x,star(nstars)%mass
  send(1) = nstars;  send(2) = 0                                                ! local number of stars
  if (need_sync)     send(2) = 1                                                ! we request a sync
  call MPI_Allgather (send,2,MPI_REAL,recv,2,MPI_REAL,mpi_comm_world,mpi_err)   ! gather the info
  ! if (sum(recv(2,:)) == 0) return                                               ! no need to sync

  if (verbose>2 .and. nstars>0) print*,mpi_rank,':g ',nstars,star(nstars)%x,star(nstars)%mass
  mstars = pstars + sum(recv(1,:)-pstars)                                       ! new number of stars
  if (mstars > pstars) then                                                     ! more star slots needed?
    if (nstars > 0) then
      allocate (old(nstars))                                                    ! make a copy
      old = star(1:nstars)                                                      ! copy out previous stars
      deallocate (star); allocate (star(mstars))                                ! new size
      star(1:nstars) = old                                                      ! copy in previous stars
      deallocate (old)
    else if (nstars == 0) then
      allocate (star(mstars))                                                   ! new size
    end if
  end if
  if (verbose>2 .and. nstars>0) print*,mpi_rank,':h ',nstars,star(nstars)%x,star(nstars)%mass
  nstars = mstars
  if (master .and. verbose > 1) print*,'sync_stars: new =', nstars-pstars

  allocate (all(nstars,0:mpi_size-1))                                           ! collect all star info
  call MPI_Allgather (star,sizeof(star),MPI_BYTE, &                             ! gather info from other ranks
                      all ,sizeof(star),MPI_BYTE,mpi_comm_world,mpi_err)

  istars = pstars                                                               ! index into stars
  do rank=0,mpi_size-1                                                          ! talk to other domains
    newstars = recv(1,rank)-pstars                                              ! new stars from rank
    if (newstars > 0) then
      star(istars+1:istars+newstars) = all(pstars+1:pstars+newstars,rank)       ! append new stars
      istars = istars+newstars                                                  ! increment count
    end if
  end do

  do rank=0,mpi_size-1
    do i=1,recv(1,rank)
      if (all(i,rank)%owner == rank) star(i)=all(i,rank)                        ! copy the authoritative info
    end do
  end do

  do i=1,nstars
    owner = -1                                                                  ! mark as not known yet
    do rank=0,mpi_size-1
      CALL MPI_CART_COORDS (new_com, rank, 3, xyz, mpi_err)
      xmin_rank = xmin+xyz(1)*mx*dx; xmax_rank = xmin+(xyz(1)+1)*mx*dx
      ymin_rank = ymin+xyz(2)*my*dy; ymax_rank = ymin+(xyz(2)+1)*my*dy
      zmin_rank = zmin+xyz(3)*mz*dz; zmax_rank = zmin+(xyz(3)+1)*mz*dz
      if (star(i)%x >= xmin_rank .and. star(i)%x < xmax_rank .and. &            ! all agree on coordinates
          star(i)%y >= ymin_rank .and. star(i)%y < ymax_rank .and. &
          star(i)%z >= zmin_rank .and. star(i)%z < zmax_rank) then
	 if (owner == -1) then
           owner = rank                                                         ! new owner
	 else
	   print*,mpi_rank,': owner conflict ERROR: i, rank, owner =', &        ! conflict
	     i, rank, owner
	 end if
      end if
      star(i)%owner = owner                                                     ! remember the new owner
      if (i <= recv(1,rank) .and. all(i,rank)%dmass > 0) then                   ! add increments only for valid slots
        star(i)%mass = star(i)%mass + all(i,rank)%dmass                         ! add mass increments from all ranks
        star(i)%px = star(i)%px + all(i,rank)%dpx                               ! add momentum increments from all ranks
        star(i)%py = star(i)%py + all(i,rank)%dpy
        star(i)%pz = star(i)%pz + all(i,rank)%dpz
        if (master .and. verbose > 0) &
          print'(a,3i4,1p(2e9.2),0p(3f7.3))','sync_stars: star,rank,own,m,dm =', &
	    i,rank,star(i)%owner,star(i)%mass,all(i,rank)%dmass, &
	    all(i,rank)%dpx/all(i,rank)%dmass, &
	    all(i,rank)%dpy/all(i,rank)%dmass, &
	    all(i,rank)%dpz/all(i,rank)%dmass
      end if
    end do
    star(i)%dmass = 0.                                                          ! avoid repeated summation

    if (owner >= 0) then
      star(i)%owner = owner                                                     ! set the new owner
    else
      print*,'sync_check EROOR: no owner', mpi_rank, i, nstars, star(i)%y       ! complain if there isn't one
    end if
    if (master .and. verbose > 2) print*,'sync_stars: star, owner =',i,owner
  end do
  pstars = nstars
  if (verbose>2 .and. nstars>0) print*,mpi_rank,':i ',nstars,star(nstars)%x,star(nstars)%mass

  if (verbose > -1) then
    call MPI_Allgather (star,sizeof(star),MPI_BYTE, & 
                        all ,sizeof(star),MPI_BYTE,mpi_comm_world,mpi_err)
    do i=1,nstars
      do rank=0,mpi_size-1 
        if (all(i,rank)%x .ne. star(i)%x .or. &
            all(i,rank)%y .ne. star(i)%y .or. &
            all(i,rank)%x .ne. star(i)%x .or. &
            all(i,rank)%mass .ne. star(i)%mass) then
          print*,mpi_rank,': ERROR out of sync; star, rank =',i,rank
	  print*,star(i)
	  print*,all(i,rank)
        end if
      end do
    end do
  end if

  deallocate (all)
END SUBROUTINE
