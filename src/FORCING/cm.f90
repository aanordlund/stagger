! $Id: cm.f90,v 1.20 2007/07/09 08:39:46 aake Exp $
!***********************************************************************
MODULE forcing

  logical conservative
  logical do_force, do_helmh
  real t_turb, t_turn, ampl_turb, k1, k2, pk, a_helmh
  real rnd1, rnd2, rnd3, rav, xav, yav, zav

  real, allocatable, dimension(:,:,:):: &
    franx, frany, franz, pranx, prany, pranz

END MODULE
!***********************************************************************
  SUBROUTINE init_force
    USE params
    USE forcing
    implicit none
    integer iz
    character(len=mid):: id='$Id: cm.f90,v 1.20 2007/07/09 08:39:46 aake Exp $'
    character(len=mfile):: name, fname

    call print_id (id)

    conservative = .false.
    ampl_turb = 0.1
    do_force = .true.
    do_helmh = .true.
    a_helmh = 1.
    k1=1.
    k2=1.5
    pk=7./6.
    t_turn = 0.1000
    t_turb = -1.001*t_turn
    call read_force

    allocate (franx(mx,my,mz), frany(mx,my,mz), franz(mx,my,mz))
    allocate (pranx(mx,my,mz), prany(mx,my,mz), pranz(mx,my,mz))

  END SUBROUTINE

!***********************************************************************
  SUBROUTINE read_force
    USE params
    USE forcing
    implicit none
    namelist /force/ do_force, do_helmh, k1, k2, ampl_turb, t_turn, t_turb, a_helmh, pk

    if (stdin.ge.0) then
      rewind (stdin); read (stdin,force)
    else
      read (*,force)
    end if
    if (master) write (*,force)

    call init_selfg
  END SUBROUTINE

!***********************************************************************
  SUBROUTINE forceit (r,Ux,Uy,Uz,xdnr,ydnr,zdnr,dUxdt,dUydt,dUzdt)
!
!  Calculate a random force, confined to a shell in k-space, at
!  regular intervals in time.  We assume rho, P, and the sound
!  speed to be near unity.  The velocity amplitude should be of the
!  order ampl_turb.  The size (scale) of the driving motions is of the
!  order of mx!dx/k1.  Thus the turnover time is given by
!  t_turb!ampl_turb = mx*dx/k1; and the acceleration is of the order
!  ampl_turb/t_turb.
!
!  Note:  In order to restart properly, one would have to save the
!  previous value of iseed.
!
!  06-aug-93/aake:  bug corrected; iseed was not negative at start
!  09-aug-93/aake:  changed to allow restart w same random coeffs
!  21-feb-95/paolo: changed in order to use segments of constant
!                   time der. of acceleration (so continuous accel.)
!            franx(,y,z) are time deriv. of accel. (random, solenoidal);
!            pranx(,y,z) (and scr2(,3,4)) are accelerations. The variable
!            ``accel'' is now the time derivative of the acceleration.
!            ``t_turn'' has the same meaning as before (see comment above).
!  23-jan-96/aake:  bug corrected; ran1s() -> 2.!ran1s()-1. to span [-1,1]
!
!            Restart must be done AFTER t=t_turn.
!            ------------------------------------
!----------------------------------------------------------------------
  USE params
  USE forcing

  implicit none
  real, dimension(mx,my,mz):: r,Ux,Uy,Uz,xdnr,ydnr,zdnr,dUxdt,dUydt,dUzdt

  complex fxx,fxy,fxz,fyx,fyy,fyz,fzx,fzy,fzz,kx,ky,kz,corr,expikrx,expikry,expikrz
  real x, y, z, eps, fk, fk1, fk2, accel, ran1s, test, t_tmp, fpow, fact, w, average
  integer flag, flag2, nrand, jxmax, jymax, jzmax, jx, jy, jz, i, j, k

  integer,save:: istart=1
  real, save:: tprev=-1.

  logical debug

!----------------------------------------------------------------------
! Even if no external forcing, still do selfgravity (unless grav=0.)
  if (.not. do_force) then
    call selfgrav (r,xdnr,ydnr,zdnr,dUxdt,dUydt,dUzdt)
    return
  end if

!----------------------------------------------------------------------
!   
                        if (t .ne. tprev) then    ! prevent repeat at same t
                        tprev=t

      flag=1
!--------------------------------------------- new random numbers:
!-----------------------------------------------------------------

      if (t .gt. t_turb .or. istart .ne. 0) then

        flag=0

        eps = 1e-5
        fk1 = k1 - eps
        fk2 = k2 + eps

!  Loop until t_turb > t
!
100     if (istart .ne. 0) then
          istart = 0
          iseed  = -iabs(iseed)
	  !$omp parallel do private(k)
          do k=1,mz
            pranx(:,:,k)=0.
            prany(:,:,k)=0.
            pranz(:,:,k)=0.
          end do
        else
	  !$omp parallel do private(k)
          do k=1,mz
            pranx(:,:,k)=franx(:,:,k)
            prany(:,:,k)=frany(:,:,k)
            pranz(:,:,k)=franz(:,:,k)
          end do
        endif

        t_turb = t_turb + t_turn

        nrand = 0
        jxmax=6
        jymax=min0(my-1,6)
        jzmax=min0(mz-1,6)
        if (my*mz .eq. 1) jxmax=128
        do jx=-jxmax,jxmax
          do jy=-jymax,jymax
            do jz=-jzmax,jzmax
              fk = sqrt(float(jx**2+jy**2+jz**2))
              if (fk .ge. fk1 .and. fk .le. fk2) nrand=nrand+1
            enddo
          enddo
        enddo

!--------------------------------------------------------------------------------
        accel  = ampl_turb/t_turn/sqrt(float(nrand)/8.)    ! rms=1./8. per comp.
!--------------------------------------------------------------------------------

        if (master) print *,'random_force:t=',t,t_turb,nrand,iseed

	  !$omp parallel do private(k)
          do k=1,mz
            franx(:,:,k) = 0.
            frany(:,:,k) = 0.
            franz(:,:,k) = 0.
          end do

! To obtain a Kolmogorov slope of the driving alone, one should have amplitudes
! a(k) that drop with k^(-11./6.), to have a(k)^2*k^2 = k^(-5./3.).

        ! pk = 1.5     ! previous value
        ! pk = 11./6.

        fpow = 0.
        do jx=-jxmax,jxmax
          kx = cmplx (0., jx*2.*pi/sx)
          do jy=-jymax,jymax
            ky = cmplx (0., jy*2.*pi/sy)
            do jz=-jzmax,jzmax
              kz = cmplx (0., jz*2.*pi/sz)
              fk = sqrt(float(jx**2+jy**2+jz**2))
              if (fk .ge. fk1 .and. fk .le. fk2) then
                fxx =   cexp(cmplx(0., 2.*pi*ran1s(iseed)))/fk**pk
                fyy =   cexp(cmplx(0., 2.*pi*ran1s(iseed)))/fk**pk
                if (mz.gt.1) then
                  fzz = cexp(cmplx(0., 2.*pi*ran1s(iseed)))/fk**pk
                else
                  fzz=0.
                end if

! solenoidal field:
!------------------
               if (do_helmh) then
                  corr=(kx*fxx+ky*fyy+kz*fzz)/(kx*kx+ky*ky+kz*kz+1e-20) 

                  if (jx.ne.0) fxx = fxx - corr*kx
                  if (jy.ne.0) fyy = fyy - corr*ky
                  if (jz.ne.0) fzz = fzz - corr*kz
               endif
!------------------

               fact=1.
               if (jx.ne.0) fact=fact*0.5
               if (jy.ne.0) fact=fact*0.5
               if (jz.ne.0) fact=fact*0.5
               fpow = fpow+fact*(cabs(fxx)**2+cabs(fyy)**2+cabs(fzz)**2)

	       !$omp parallel do private(i,j,k,x,y,z,expikrx,expikry,expikrz)
               do k=1,mz
                 z = zm(k)-zmin
                 do j=1,my
                   y = ym(j)-ymin
                   do i=1,mx
                     x = xm(i)-xmin
                     expikrx = accel*cexp(kx*(x-0.5*dx)+ky*y+kz*z)
                     expikry = accel*cexp(kx*x+ky*(y-0.5*dy)+kz*z)
                     expikrz = accel*cexp(kx*x+ky*y+kz*(z-0.5*dz))
                     franx(i,j,k) = franx(i,j,k) + real(fxx*expikrx)
                     frany(i,j,k) = frany(i,j,k) + real(fyy*expikry)
                     franz(i,j,k) = franz(i,j,k) + real(fzz*expikrz)
                   end do
                 end do
               end do
               if (master .and. debug(dbg_force)) then
	         print '(1x,a,3i2,f6.3,3(f7.3,f6.3),6f7.3)', &
                   'random_force:k=',jx,jy,jz,fk,fxx,fyy,fzz,fact,zm(1),zmin,kz,franx(1,1,1)
               endif
              endif
            enddo
          enddo
        enddo

!--------------------------------          

! TEST:
!--------------------------------------------------
        if (idbg.ge.1) then
          test=sqrt(average(franx**2+frany**2+franz**2))
          if (master) print *,'AVERAGE ACCELERATION = ',test,sqrt(fpow)*accel
        end if
!--------------------------------------------------
   
        if (t_turb .lt. t) then
           goto 100
        endif
      
      endif  ! new random numbers
!----------------------------------------end of new random numbers.
!------------------------------------------------------------------
                                endif                            
!      tm(1)=etime(cput)

!  Time interpolation of the force
!
      ! w = 1.-exp(-(t+t_turn-t_turb)/t_turn)
      w = (t_turn-(t_turb-t))/t_turn
      w = 0.5*(1.-cos(w*pi))
	   
      !$omp parallel do private(k)
      do k=1,mz
        pranx(:,:,k) = pranx(:,:,k) + (franx(:,:,k)-pranx(:,:,k))*w
        prany(:,:,k) = prany(:,:,k) + (frany(:,:,k)-prany(:,:,k))*w
        pranz(:,:,k) = pranz(:,:,k) + (franz(:,:,k)-pranz(:,:,k))*w
      end do
!      print *,'1:',etime(cput)-tm(1)


! Force and velocity averages
!----------------------------

      call average_subr (r, rav)
      !$omp parallel do private(k)
      do k=1,mz
        scratch(:,:,k) = r(:,:,k)*(pranx(:,:,k)+Ux(:,:,k)*(.5/t_turn))
      end do
      call average_subr (scratch, xav)
      xav = xav/rav
      !$omp parallel do private(k)
      do k=1,mz
        scratch(:,:,k) = r(:,:,k)*(prany(:,:,k)+Uy(:,:,k)*(.5/t_turn))
      end do
      call average_subr (scratch, yav)
      yav = yav/rav
      !$omp parallel do private(k)
      do k=1,mz
        scratch(:,:,k) = r(:,:,k)*(pranz(:,:,k)+Uz(:,:,k)*(.5/t_turn))
      end do
      call average_subr (scratch, zav)
      zav = zav/rav
!      print *,'2:',etime(cput)-tm(1)
      !$omp parallel do private(i,j,k)
      do k=1,mz
        do j=1,my
        do i=1,mx
          dUxdt(i,j,k) = dUxdt(i,j,k) + pranx(i,j,k) - xav
          dUydt(i,j,k) = dUydt(i,j,k) + prany(i,j,k) - yav
          dUzdt(i,j,k) = dUzdt(i,j,k) + pranz(i,j,k) - zav
        end do
        end do
      end do
!      print *,'3:',etime(cput)-tm(1)
      call stats ('pranx', pranx)
      call stats ('prany', prany)
      call stats ('pranz', pranz)

      call selfgrav (r,xdnr,ydnr,zdnr,dUxdt,dUydt,dUzdt)

END SUBROUTINE

