! $Id: noexplode.f90,v 1.15 2006/05/16 17:11:56 aake Exp $
MODULE explosions

  logical do_expl

END MODULE

SUBROUTINE init_explode (rho)
  USE params
  USE explosions
  real, dimension(mx,my,mz):: rho

  do_expl=.false.
  call read_explode
  return
END SUBROUTINE

SUBROUTINE read_explode
  USE params
  USE explosions
  namelist /expl/do_expl

  character (len=mid):: id="$Id: noexplode.f90,v 1.15 2006/05/16 17:11:56 aake Exp $"
  call print_id (id)

  return
END SUBROUTINE

SUBROUTINE explode (r,px,py,pz,e,d,Bx,By,Bz)
  USE params
  USE explosions
  implicit none
  real, dimension(mx,my,mz):: r,px,py,pz,e,d,Bx,By,Bz

  return
END SUBROUTINE
