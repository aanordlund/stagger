! $Id: turbulence_cm.f90,v 1.10 2007/07/12 08:50:36 aake Exp $
!**********************************************************************
MODULE forcing

logical do_force, do_helmh
real:: t_turb, t_turn, ampl_turb, k1, k2, pk, a_helmh, calib, enom, mach, edot
real, allocatable, dimension(:,:,:):: pranx,prany,pranz,franx,frany,franz
logical conservative

END MODULE

!***********************************************************************
  SUBROUTINE init_force
    USE params
    USE forcing
    implicit none
    integer iz
    character(len=mid):: id='$Id: turbulence_cm.f90,v 1.10 2007/07/12 08:50:36 aake Exp $'
    character(len=mfile):: name, fname

    call print_id (id)

    ampl_turb = 0.1
    t_turn = 10.
    do_force = .true.
    do_helmh = .true.
    conservative = .false.
    a_helmh=1.
    mach=0.
    calib=0.648
    k1=1.
    k2=1.5
    pk=7./6.

    call read_force

    t_turb = 0.
    allocate (pranx(mx,my,mz), prany(mx,my,mz), pranz(mx,my,mz))
    allocate (franx(mx,my,mz), frany(mx,my,mz), franz(mx,my,mz))
!$omp parallel do private(iz)
    do iz=1,mz                                ! encourage distribution
      pranx(:,:,iz)=0. ; prany(:,:,iz)=0. ; pranz(:,:,iz)=0.
      franx(:,:,iz)=0. ; frany(:,:,iz)=0. ; franz(:,:,iz)=0.
    end do

    if (master) then
      fname = name('force.txt','frc',file)
      open (22,file=fname,status='unknown',form='formatted')
    end if
  END SUBROUTINE

!***********************************************************************
  SUBROUTINE read_force
    USE params
    USE forcing
    implicit none
    namelist /force/ do_force, do_helmh, a_helmh, k1, k2, ampl_turb, mach, calib, t_turn, pk, conservative

    if (stdin.ge.0) then
      rewind (stdin); read (stdin,force)
    else
      read (*,force)
    end if
    if (master) write (*,force)
    enom = calib*mach**3

    call init_selfg
  END SUBROUTINE

!***********************************************************************
  SUBROUTINE forceit (r,Ux,Uy,Uz,xdnr,ydnr,zdnr,dUxdt,dUydt,dUzdt)
!
!  Calculate a random force, confined to a shell in k-space, at
!  regular intervals in time.  We assume rho, P, and the sound
!  speed to be near unity.  The velocity amplitude should be of the
!  order ampl_turb.  The size (scale) of the driving motions is of the
!  order of mx*dx/k1.  Thus the turnover time is given by
!  t_turb*ampl_turb = mx*dx/k1; and the acceleration is of the order
!  ampl_turb/t_turb.
!
!  Note:  In order to restart properly, one would have to save the
!  previous value of iseed.
!
!  06-aug-93/aake:  bug corrected; iseed was not negative at start
!  09-aug-93/aake:  changed to allow restart w same random coeffs
!  21-feb-95/paolo: changed in order to use segments of constant
!                   time der. of acceleration (so continuous accel.)
!            franx(,y,z) are time deriv. of accel. (random, solenoidal);
!            pranx(,y,z) (and scr2(,3,4)) are accelerations. The variable
!            ``accel'' is now the time derivative of the acceleration.
!            ``t_turn'' has the same meaning as before (see comment above).
!  23-jan-96/aake:  bug corrected; ran1() -> 2.*ran1()-1. to span [-1,1]
!
!            Restart must be done AFTER t=t_turn.
!            ------------------------------------
!----------------------------------------------------------------------
  USE params
  USE forcing

  implicit none

  real, dimension(mx,my,mz):: r,Ux,Uy,Uz,xdnr,ydnr,zdnr,dUxdt,dUydt,dUzdt
  real, allocatable, dimension(:,:,:):: scr1,scr2,scr3,scr

  integer i,j,k,jx,jy,jz,nrand,kmax

  complex fxx,fyy,fzz,kx,ky,kz,corr,expikrx,expikry,expikrz

  real x, y, z, xav, yav, zav, rav, w, fact, test, fpow, accel, ran1s
  real eps, fk, average, f

  real, save :: tprev=-1.
  logical, save :: first_time=.true.
  logical debug

! Even if no external forcing, still do selfgravity (unless grav=0.)

  if (.not. do_force) then
    call selfgrav (r,xdnr,ydnr,zdnr,dUxdt,dUydt,dUzdt)
    return
  end if

!$omp master
  allocate(scr1(mx,my,mz), scr2(mx,my,mz), scr3(mx,my,mz), scr(mx,my,mz))
!$omp end master
!$omp barrier

!----------------------------------------------------------------------
  if (t .ne. tprev) then    ! prevent repeat at same t
    tprev = t
    eps = 1e-5
    
    if (master.and.idbg>0) then
      print *,'forceit: t,t_turb,first_time=',t,t_turb,first_time
      if (t_turb .le. t) print *,'new force'
    endif
    
    if (first_time) then
      first_time = .false.  ! don't come back
      t_turb = -1.5*t_turn
!$omp parallel do private(k)
      do k=1,mz
        pranx(:,:,k)=0.
        prany(:,:,k)=0.
        pranz(:,:,k)=0.
      end do
    endif
!--------------------------------------------- new random numbers:
!  Loop until t_turb > t
    do while (t_turb .le. t)
!$omp parallel do private(k)
      do k=1,mz
        pranx(:,:,k)=franx(:,:,k)
        prany(:,:,k)=frany(:,:,k)
        pranz(:,:,k)=franz(:,:,k)
        franx(:,:,k) = 0.
        frany(:,:,k) = 0.
        franz(:,:,k) = 0.
      end do

    t_turb = t_turb + t_turn

    nrand = 0
    kmax=ifix(k2+1.)
    do jx=-kmax,kmax
      do jy=-kmax,kmax
        do jz=-kmax,kmax
          fk = sqrt(float(jx**2+jy**2+jz**2))
          if (fk .ge. k1-eps .and. fk .le. k2+eps) nrand=nrand+1
        enddo
      enddo
    enddo

!--------------------------------------------------------------------------------
    accel  = ampl_turb/t_turn/sqrt(float(nrand)/8.)    ! rms=1./8. per comp.
!--------------------------------------------------------------------------------

      if (master) print *,'random_force:t=',t,t_turb,nrand,iseed

! To obtain a Kolmogorov slope of the driving alone, one should have amplitudes
! a(k) that drop with k^(-11./6.), to have a(k)^2*k^2 = k^(-5./3.).  This ASSUMES
! that the amplitudes are proportional to the driving.  But the energy drain may
! be rather inversely proportional to the turnover time, which goes as k^(2./3.)

    ! pk = 11./6.
    ! pk = 7./6.

    fpow = 0.
    do jx=-kmax,kmax
      kx = cmplx (0., jx*2.*pi/sx)
      do jy=-kmax,kmax
        ky = cmplx (0., jy*2.*pi/sy)
        do jz=-kmax,kmax
          kz = cmplx (0., jz*2.*pi/sz)
          fk = sqrt(float(jx**2+jy**2+jz**2))
          if (fk .ge. k1-eps .and. fk .le. k2+eps) then
            fxx =   cexp(cmplx(0., 2.*pi*ran1s(iseed)))/fk**pk
            fyy =   cexp(cmplx(0., 2.*pi*ran1s(iseed)))/fk**pk
            if (mz.gt.1) then
              fzz = cexp(cmplx(0., 2.*pi*ran1s(iseed)))/fk**pk
            else
              fzz=0.
            end if

! solenoidal field:
!------------------
            if (do_helmh) then
               corr=(kx*fxx+ky*fyy+kz*fzz)/(kx*kx+ky*ky+kz*kz+1e-20) 

               if (jx.ne.0) fxx = fxx - a_helmh*corr*kx
               if (jy.ne.0) fyy = fyy - a_helmh*corr*ky
               if (jz.ne.0) fzz = fzz - a_helmh*corr*kz
            endif
!------------------

            fact=1.
            if (jx.gt.0) fact=fact*0.5
            if (jy.gt.0) fact=fact*0.5
            if (jz.gt.0) fact=fact*0.5
            fpow = fpow+fact*(cabs(fxx)**2+cabs(fyy)**2+cabs(fzz)**2)

!$omp parallel do private(k,i,j,x,y,z,expikrx,expikry,expikrz)
            do k=1,mz
              z = zm(k)-zmin
              do j=1,my
                y = ym(j)-ymin
                do i=1,mx
                  x = xm(i)-xmin
                  expikrx = accel*cexp(kx*(x-0.5*dx)+ky*y+kz*z)
                  expikry = accel*cexp(kx*x+ky*(y-0.5*dy)+kz*z)
                  expikrz = accel*cexp(kx*x+ky*y+kz*(z-0.5*dz))
                  franx(i,j,k) = franx(i,j,k) + real(fxx*expikrx)
                  frany(i,j,k) = frany(i,j,k) + real(fyy*expikry)
                  franz(i,j,k) = franz(i,j,k) + real(fzz*expikrz)
                end do
              end do
            end do
            if (master.and.idbg>0) then
              print '(1x,a,3i2,f6.3,4(f7.3,f6.3))', &
                    'random_force:k=',jx,jy,jz,fk,fxx,fyy,fzz,fact
            endif
          endif
        enddo
      enddo
    enddo

! TEST:
!--------------------------------------------------
    if (master.and.idbg>0) then
      test=sqrt(sum(franx**2+frany**2+franz**2)/mw)
      if (master) print *,'AVERAGE ACCELERATION = ',test,fpow,sqrt(fpow)*accel
    end if
!--------------------------------------------------
    enddo ! while
  endif   ! t > t_prev
!------------------------------------------------------------------
  if (master.and.idbg>0) print *,'interpolate force'

!  Time interpolation of the force
!
  !w = 1.-exp(-(t+t_turn-t_turb)/t_turn)
  w = (t_turn-(t_turb-t))/t_turn
  w = 0.5*(1.-cos(w*pi))

  if (master.and.idbg>0.and.isubstep==1) &
    print *,'force: t-t_turb, t_turn, w:', t-t_turb, t_turn, w
  
  !$omp parallel do private(k)
  do k=1,mz
    scr1(:,:,k) = pranx(:,:,k) + (franx(:,:,k)-pranx(:,:,k))*w
    scr2(:,:,k) = prany(:,:,k) + (frany(:,:,k)-prany(:,:,k))*w
    scr3(:,:,k) = pranz(:,:,k) + (franz(:,:,k)-pranz(:,:,k))*w
  end do
  if (debug(dbg_force)) then
    write(*,*) mpi_rank,' force: Ux'
    write(*,'(10e12.5)') scr1(1,1,:)
    write(*,*) mpi_rank,' force: pranx'
    write(*,'(10e12.5)') pranx(1,1,:)
    write(*,*) mpi_rank,' force: franx'
    write(*,'(10e12.5)') franx(1,1,:)
    write(*,*) mpi_rank,' force: xdnr'
    write(*,'(10e12.5)') xdnr(1,1,:)
  end if

  if (enom .gt. 0.) then
    !$omp parallel do private(k)
    do k=1,mz
      scr(:,:,k) = xdnr(:,:,k)*scr1(:,:,k)*Ux(:,:,k) &
                 + ydnr(:,:,k)*scr2(:,:,k)*Uy(:,:,k) &
                 + zdnr(:,:,k)*scr3(:,:,k)*Uz(:,:,k)
    end do

    if (debug(dbg_force)) then
      write(*,*) mpi_rank,' force: scr'
      write(*,'(10e12.5)') scr(1,1,:)
      write(*,*) mpi_rank,' force: Ux'
      write(*,'(10e12.5)') Ux(1,1,:)
    end if

    edot = average(scr)
    write (20,*) 'edot, enom, Urms =', edot, enom, Urms
    if (master .and. isubstep==2) write (22,'(1x,4g15.6)') t, edot, Urms, t_turn
    f = enom/edot
    !$omp parallel do private(k)
    do k=1,mz
      scr1(:,:,k) = f*scr1(:,:,k)
      scr2(:,:,k) = f*scr2(:,:,k)
      scr3(:,:,k) = f*scr3(:,:,k)
    end do
  end if

! Force and velocity averages
!----------------------------

  if (do_trace) print *,'turbulence: average1'
  rav = 1./average(r)
!$omp parallel do private(k)
  do k=1,mz
    scr(:,:,k) = xdnr(:,:,k)*(scr1(:,:,k)+Ux(:,:,k)*(1./t_turn))*rav
  end do
  if (do_trace) print *,'turbulence: average2'
  xav = average(scr)

!$omp parallel do private(k)
  do k=1,mz
    scr(:,:,k) = ydnr(:,:,k)*(scr2(:,:,k)+Uy(:,:,k)*(1./t_turn))*rav
  end do
  if (do_trace) print *,'turbulence: average3'
  yav = average(scr)

!$omp parallel do private(k)
  do k=1,mz
    scr(:,:,k) = zdnr(:,:,k)*(scr3(:,:,k)+Uz(:,:,k)*(1./t_turn))*rav
  end do
  if (do_trace) print *,'turbulence: average4'
  zav = average(scr)

  if (conservative) then
    !$omp parallel do private(k)
    do k=1,mz
      dUxdt(:,:,k) = dUxdt(:,:,k) + xdnr(:,:,k)*(scr1(:,:,k) - xav)
      dUydt(:,:,k) = dUydt(:,:,k) + ydnr(:,:,k)*(scr2(:,:,k) - yav)
      dUzdt(:,:,k) = dUzdt(:,:,k) + zdnr(:,:,k)*(scr3(:,:,k) - zav)
    end do
  else
    !$omp parallel do private(k)
    do k=1,mz
      dUxdt(:,:,k) = dUxdt(:,:,k) + (scr1(:,:,k) - xav)
      dUydt(:,:,k) = dUydt(:,:,k) + (scr2(:,:,k) - yav)
      dUzdt(:,:,k) = dUzdt(:,:,k) + (scr3(:,:,k) - zav)
    end do
  end if

  call dump(scr1,'force.dat',0)
  call dump(scr3,'force.dat',1)
  call dump(scr3,'force.dat',2)

  if (master.and.idbg>1) then
    call stats ('pranx', scr1)
    call stats ('prany', scr2)
    call stats ('pranz', scr3)
  end if

!$omp barrier
!$omp master
  deallocate (scr1,scr2,scr3,scr)
!$omp end master

  if (do_trace) print *,'selfg'
  call selfgrav (r,xdnr,ydnr,zdnr,dUxdt,dUydt,dUzdt)

END SUBROUTINE

