! $Id: friction.f90,v 1.2 2007/08/23 15:39:50 jon Exp $
!**********************************************************************
MODULE forcing

logical conservative
logical do_force
real t_friction 

END MODULE
!**********************************************************************
  SUBROUTINE init_force
    USE params
    USE forcing
    implicit none
    integer iz
    character(len=mid):: id='$Id: friction.f90,v 1.2 2007/08/23 15:39:50 jon Exp $'

    call print_id(id)

    do_force = .true.
    conservative = .true.
    t_friction = 0.

    call read_force

  END SUBROUTINE

!***********************************************************************
  SUBROUTINE read_force
    USE params
    USE forcing
    namelist /force/ do_force, t_friction

    rewind (stdin); read (stdin,force)
    if (master) write (*,force)

    call init_selfg
  END SUBROUTINE

!***********************************************************************
  SUBROUTINE forceit (r,Ux,Uy,Uz,xdnr,ydnr,zdnr,dpxdt,dpydt,dpzdt)
  USE params
  implicit none
  real, dimension(mx,my,mz):: r,Ux,Uy,Uz,xdnr,ydnr,zdnr,dpxdt,dpydt,dpzdt
  logical omp_in_parallel
!-----------------------------------------------------------------------
  if (omp_in_parallel()) then
    call forceit_omp (r,Ux,Uy,Uz,xdnr,ydnr,zdnr,dpxdt,dpydt,dpzdt)
  else
    !$omp parallel
    call forceit_omp (r,Ux,Uy,Uz,xdnr,ydnr,zdnr,dpxdt,dpydt,dpzdt)
    !$omp end parallel
  end if
  END SUBROUTINE

!***********************************************************************
  SUBROUTINE forceit_omp (r,Ux,Uy,Uz,xdnr,ydnr,zdnr,dpxdt,dpydt,dpzdt)
!
!----------------------------------------------------------------------
  USE params
  USE forcing
  !USE selfgravity

  implicit none

  real, dimension(mx,my,mz):: r,Ux,Uy,Uz,xdnr,ydnr,zdnr,dpxdt,dpydt,dpzdt
  integer iz

  logical debug

! Even if no external forcing, still do selfgravity (unless grav=0.)

  if ((.not. do_force) .or. t_friction==0.) then
    call selfgrav (r,xdnr,ydnr,zdnr,dpxdt,dpydt,dpzdt)
    return
  end if

  do iz=izs,ize
    dpxdt(:,:,iz) = dpxdt(:,:,iz) - (1./abs(t_friction))*xdnr(:,:,iz)*Ux(:,:,iz)
    dpydt(:,:,iz) = dpydt(:,:,iz) - (1./abs(t_friction))*ydnr(:,:,iz)*Uy(:,:,iz)
    dpzdt(:,:,iz) = dpzdt(:,:,iz) - (1./abs(t_friction))*zdnr(:,:,iz)*Uz(:,:,iz)
  end do

  if (master .and. do_trace) print *,'selfg'
  call selfgrav (r,xdnr,ydnr,zdnr,dpxdt,dpydt,dpzdt)

END SUBROUTINE
