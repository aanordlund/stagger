! $Id: nocosines.f90,v 1.7 2016/11/09 18:42:57 aake Exp $
!**********************************************************************
MODULE forcing
  USE params
  implicit none
  real:: a_f,t_f,u_f
  integer verbose,force_unit
  logical do_force,do_helmh
  logical, parameter:: conservative=.true.
  real t_turb,t_turn,a_helmh
END MODULE

!***********************************************************************
SUBROUTINE init_force
  USE params
  USE forcing
  implicit none
  character(len=80):: id='$Id: nocosines.f90,v 1.7 2016/11/09 18:42:57 aake Exp $'
  character(len=mfile):: name
!-----------------------------------------------------------------------
  call print_id(id)
  do_force = .true.
  verbose = 0
  force_unit = 77
  u_f = 0.2
  t_f = 1./u_f
  a_f = u_f*nu3*csound*dx*(2.*pi/sx)**2
  t_turn = u_f/a_f
  call read_force
  if (verbose.ge.1) then
    open(force_unit,file=name('nocosines.dat','noc',file),form='unformatted',status='unknown')
  end if
END SUBROUTINE

!***********************************************************************
SUBROUTINE read_force
  USE params
  USE forcing
  implicit none
  namelist /force/do_force,a_f,t_f,u_f,verbose
!-----------------------------------------------------------------------
  rewind (stdin); read (stdin,force)
  if (master) write (*,force)
END SUBROUTINE

!***********************************************************************
SUBROUTINE forceit (r,Ux,Uy,Uz,xdnr,ydnr,zdnr,dpxdt,dpydt,dpzdt)
  USE params
  USE forcing
  implicit none
  real, dimension(mx,my,mz):: r,Ux,Uy,Uz,xdnr,ydnr,zdnr,dpxdt,dpydt,dpzdt
  logical omp_in_parallel
!-----------------------------------------------------------------------
  if (.not. do_force) return
  if (omp_in_parallel()) then
    call forceit_omp (r,Ux,Uy,Uz,xdnr,ydnr,zdnr,dpxdt,dpydt,dpzdt)
  else
    !$omp parallel
    call forceit_omp (r,Ux,Uy,Uz,xdnr,ydnr,zdnr,dpxdt,dpydt,dpzdt)
    !$omp end parallel
  end if
END

!***********************************************************************
SUBROUTINE forceit_omp (r,Ux,Uy,Uz,xdnr,ydnr,zdnr,dpxdt,dpydt,dpzdt)
  USE params
  USE forcing
  implicit none
  real, dimension(mx,my,mz):: r,Ux,Uy,Uz,xdnr,ydnr,zdnr,dpxdt,dpydt,dpzdt
  integer ix,iy,iz
  real sinx,siny,sinz,u_rms,a_local,ratio
!-----------------------------------------------------------------------
  if (isubstep==1) then
    do iz=izs,ize
      do iy=1,my
        do ix=1,mx
          scratch(ix,iy,iz) = Ux(ix,iy,iz)**2 + Uy(ix,iy,iz)**2 + Uz(ix,iy,iz)**2
        end do
      end do
    end do
    a_local = a_f
    call average_subr(scratch,u_rms)                                    ! barrier here!
    u_rms=sqrt(u_rms)
    ratio=max(u_rms/(u_f*sqrt(1.5)),1e-3)
    a_local = a_local*(1. - (1.-1./ratio)*dt/t_f)
    !$omp master
      a_f = a_local
      if (verbose .ge. 1) then
        if (mod(it,10) == 2) then
          write (force_unit) t,u_rms,Brms,a_f
          flush(force_unit)
        end if
      end if
      if (verbose >= 2) then
        print *,'forceit: t,u_rms,a_f =',t,u_rms,a_f
      end if
    !$omp end master
  else
    a_local = a_f
  end if
  do iz=izs,ize
    sinz = sin(zm(iz)*2.*pi/sz)
    do iy=1,my
      siny = sin(ym(iy)*2.*pi/sy)
      do ix=1,mx
        sinx = sin(xm(ix)*2.*pi/sx)
        dpxdt(ix,iy,iz) = dpxdt(ix,iy,iz) + xdnr(ix,iy,iz)*a_local*sinz
        dpydt(ix,iy,iz) = dpydt(ix,iy,iz) + ydnr(ix,iy,iz)*a_local*sinx
        dpzdt(ix,iy,iz) = dpzdt(ix,iy,iz) + zdnr(ix,iy,iz)*a_local*siny
      end do
    end do
  end do
END SUBROUTINE

