! $Id: noselfgravity.f90,v 1.3 2004/01/19 00:11:02 aake Exp $
!**********************************************************************
MODULE selfgravity
  real grav, soft
  logical isolated
END MODULE

!***********************************************************************
SUBROUTINE init_selfg
  USE params
  USE selfgravity

  grav = 0.
  call read_selfg
END SUBROUTINE

!***********************************************************************
SUBROUTINE read_selfg
  USE params
  USE selfgravity

END SUBROUTINE

!***********************************************************************
SUBROUTINE selfgrav (r,xdnr,ydnr,zdnr,dpxdt,dpydt,dpzdt)
  USE params
  USE selfgravity
  implicit none
  real, dimension(mx,my,mz):: r,xdnr,ydnr,zdnr,dpxdt,dpydt,dpzdt
!hpf$ distribute(*,*,block):: r,xdnr,ydnr,zdnr,dpxdt,dpydt,dpzdt

END SUBROUTINE
