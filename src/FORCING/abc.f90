! $Id: abc.f90,v 1.4 2006/10/22 21:56:50 aake Exp $
!**********************************************************************
MODULE forcing
  USE params
  implicit none
  real:: a_f,b_f,c_f,t_f,u_f
  logical do_force,verbose,do_helmh
  logical, parameter:: conservative=.true.
  real t_turb,t_turn,a_helmh
END MODULE

!***********************************************************************
SUBROUTINE init_force
  USE params
  USE forcing
  implicit none
  character(len=80):: id='$Id: abc.f90,v 1.4 2006/10/22 21:56:50 aake Exp $'
!-----------------------------------------------------------------------
  call print_id(id)
  do_force = .true.
  verbose = .false.
  u_f = 0.2
  t_f = 1./u_f
  a_f = 8.*u_f*nu3*(dx*2.*pi/sx)**2
  b_f = a_f
  c_f = a_f
  t_turn = 0.6*u_f/a_f
  call read_force
END SUBROUTINE

!***********************************************************************
SUBROUTINE read_force
  USE params
  USE forcing
  implicit none
  namelist /force/do_force,a_f,b_f,c_f,t_f,u_f,verbose
!-----------------------------------------------------------------------
  rewind (stdin); read (stdin,force)
  if (master) write (*,force)
END SUBROUTINE

!***********************************************************************
SUBROUTINE forceit (r,Ux,Uy,Uz,xdnr,ydnr,zdnr,dpxdt,dpydt,dpzdt)
  USE params
  USE forcing
  implicit none
  real, dimension(mx,my,mz):: r,Ux,Uy,Uz,xdnr,ydnr,zdnr,dpxdt,dpydt,dpzdt
  logical omp_in_parallel
!-----------------------------------------------------------------------
  if (.not. do_force) return
  if (omp_in_parallel()) then
    call forceit_omp (r,Ux,Uy,Uz,xdnr,ydnr,zdnr,dpxdt,dpydt,dpzdt)
  else
    !$omp parallel
    call forceit_omp (r,Ux,Uy,Uz,xdnr,ydnr,zdnr,dpxdt,dpydt,dpzdt)
    !$omp end parallel
  end if
END

!***********************************************************************
SUBROUTINE forceit_omp (r,Ux,Uy,Uz,xdnr,ydnr,zdnr,dpxdt,dpydt,dpzdt)
  USE params
  USE forcing
  implicit none
  real, dimension(mx,my,mz):: r,Ux,Uy,Uz,xdnr,ydnr,zdnr,dpxdt,dpydt,dpzdt
  integer ix,iy,iz
  real cosx,cosy,cosz,sinx,siny,sinz,u_rms,f_local
!-----------------------------------------------------------------------
  if (isubstep==1) then
    do iz=izs,ize
      do iy=1,my
        do ix=1,mx
          scratch(ix,iy,iz) = Ux(ix,iy,iz)**2 + Uy(ix,iy,iz)**2 + Uz(ix,iy,iz)**2
        end do
      end do
    end do
    call average_subr(scratch,u_rms)
    u_rms=sqrt(u_rms)
    f_local = 1. + (1.-u_rms/u_f)*dt/t_f
  else
    f_local = 1.
  end if
  do iz=izs,ize
    sinz = sin(zm(iz)*2.*pi/sz)
    cosz = cos(zm(iz)*2.*pi/sz)
    do iy=1,my
      siny = sin(ym(iy)*2.*pi/sy)
      cosy = cos(ym(iy)*2.*pi/sy)
      do ix=1,mx
        sinx = sin(xm(ix)*2.*pi/sx)
        cosx = cos(xm(ix)*2.*pi/sx)
        dpxdt(ix,iy,iz) = dpxdt(ix,iy,iz) + xdnr(ix,iy,iz)*f_local*(c_f*sinz+b_f*cosy)
        dpydt(ix,iy,iz) = dpydt(ix,iy,iz) + ydnr(ix,iy,iz)*f_local*(a_f*sinx+c_f*cosz)
        dpzdt(ix,iy,iz) = dpzdt(ix,iy,iz) + zdnr(ix,iy,iz)*f_local*(b_f*siny+a_f*cosx)
      end do
    end do
  end do
  if (isubstep==1) then
    call barrier_omp('abc')                                             ! protect [abc]_f
    !$omp master
      a_f = f_local*a_f
      b_f = f_local*b_f
      c_f = f_local*c_f
      if (verbose) then
        print *,'forceit: u_f,u_rms,a_f =',u_f,u_rms,a_f
      end if
    !$omp end master
  end if
END SUBROUTINE

