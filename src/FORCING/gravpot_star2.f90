! $Id: gravpot_star2.f90,v 1.2 2012/12/31 16:19:12 aake Exp $
!***********************************************************************
SUBROUTINE gravpot (rho, phi)

!  Solve for the gravitational potential phi, given the density rho,
!  from the equation Laplace(phi) = grav*rho.

  use params
  use forcing
  use stagger
  implicit none

  real, dimension(mx,my,mz):: rho, phi
!hpf$ align with TheCube:: rho, phi
  real, allocatable, dimension(:,:,:):: green, k2, scr, scr1
!hpf$ align with TheCube:: green, k2, scr, scr1

  real zscr(mz)
  real k2norm, rhoa, m_gas, x, y, z, r, f, rho_save(2)
  integer ix, iy, iz, nperif

  character(len=mid):: id = "$Id: gravpot_star2.f90,v 1.2 2012/12/31 16:19:12 aake Exp $"
!-----------------------------------------------------------------------

  if (id .ne. '') print *,id                      ! identify
  id = ''

  if (mod(my,2) .eq. 0) then
    rho_save = rho(mx/2,my/2,mz/2)
    rho(mx/2,my/2,mz/2) = M_sun/(dx*dy*dz)
  else
    rho_save = rho(mx/2,my/2:my/2+1,mz/2)
    rho(mx/2,my/2  ,mz/2) = 0.5*M_sun/(dx*dy*dz)
    rho(mx/2,my/2+1,mz/2) = 0.5*M_sun/(dx*dy*dz)
  end if

  if (.not. isolated) then                        ! periodic case
    call fft3df (rho, phi, mx, my, mz)            ! forward FFT

  else                                            ! isolated case

!$omp parallel do
    do iz=1,mz
      zscr(iz) = dx*dy*sum(rho(2:mx,2:my,iz))     ! total mass in xy-planes
    end do
    m_gas = dz*sum(zscr(2:mz))                    ! total gas mass
    !print *,'m_gas',m_gas

    do iz=1,mz                                    ! for boundary condition ..
      z = zm(iz)                                  ! .. compute central potential
      do iy=1,my
        y = ym(iy)
        do ix=1,mx
          x = xm(ix)
          r = sqrt(x**2+y**2+z**2)
          phi(ix,iy,iz) = -m_gas/(4.*pi*max(r,dx*1e-4))
        end do
      end do
    end do

    allocate (scr(mx,my,mz),scr1(mx,my,mz))       ! boundary condition 
    call ddxdn1_set (phi,scr1)                    ! corresponding to
    call ddxup1_set (scr1,scr)                    ! negative mass at
    call ddydn1_set (phi,scr1)                    ! the boundaries
    call ddyup1_add (scr1,scr)
    call ddzdn1_set (phi,scr1)
    call ddzup1_add (scr1,scr)

    zscr(mz) = dx*dy*sum(scr(:,:,mz))             ! sum boundary mass
!$omp parallel do
    do iz=1,mz-1                                  ! assumes center at (1+mz+1)/2. = mz/2.+1
      scr(1:mx-1,1:my-1,iz) = 0.                  ! clear interior
      zscr(iz) = dx*dy*sum(scr(:,:,iz))           ! sum boundary mass
    end do
    !print *,'m_bdry',dz*sum(zscr)
    f = -fbdry*m_gas/(dz*sum(zscr))               ! boundary mass factor
    scr = scr*f
    !scr(1,1:my,1) = f*scr(1,1:my,1)
    !scr(2:mx,1,1) = f*scr(2:mx,1,1)
!$omp parallel do
    do iz=1,mz-1                                  ! assumes center at (1+mz+1)/2. = mz/2.+1
      !scr(1,1:my,iz) = f*scr(1,1:my,iz)
      !scr(2:mx,1,iz) = f*scr(2:mx,1,iz)
      scr(1:mx-1,1:my-1,iz)=rho(1:mx-1,1:my-1,iz) ! interior density
    end do
    !print *,'m_tot',dx*dy*dz*sum(scr)
    !print *,'f',f
    !scr(mx/2+1,my/2+1,mz/2+1) = m_gas/(dx*dy*dz)
    !print *,dx*dy*dz*sum(scr)
    call dump(scr,'phi.dat',1)

    call fft3df (scr, phi, mx, my, mz)            ! forward FFT
    deallocate (scr,scr1)
  end if

  allocate (k2(mx,my,mz))

  call fft3d_k2 (k2, dx, dy, dz, mx, my, mz)      ! compute k**2
  k2norm = 2.*(soft*(sx/mx)/pi)**2                ! normalization factor
!$omp parallel do private(ix,iy,iz)
  do iz=1,mz
  do iy=1,my
  do ix=1,mx
    phi(ix,iy,iz) = -phi(ix,iy,iz) &
      *grav*4.*pi/(k2(ix,iy,iz)+1e-10)*exp(-k2norm*k2(ix,iy,iz))  ! softening
  end do
  end do
  end do
  phi(1,1,1) = 0.0                                ! no mean phi
  call fft3db (phi, phi, mx, my, mz)              ! reverse FFT
  call dump(phi,'phi.dat',2)

  deallocate (k2)

  if (mod(my,2) .eq. 0) then
    rho(mx/2,my/2,mz/2) = rho_save(1)
  else
    rho(mx/2,my/2:my/2+1,mz/2) = rho_save
  end if
END
