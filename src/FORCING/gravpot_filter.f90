! $Id: gravpot_filter.f90,v 1.15 2006/11/03 01:41:20 aake Exp $
!**********************************************************************
MODULE selfgravity
  real grav, soft
  logical isolated
  real max_error, fft_error, error, filter(3), sor, rhav, resav, a_fft, a_dom
  logical verbose, do_domain, do_fft, do_filter
  integer min_iter, max_iter, nlap
END MODULE

MODULE selfgravity_arrays
  real, allocatable, dimension(:,:,:):: phi, rho, phi0
END MODULE

!***********************************************************************
SUBROUTINE init_selfg
  USE params
  USE selfgravity
  USE selfgravity_arrays
  implicit none
  integer iz
  character(len=mid), save:: id="init_laplace: $Id: gravpot_filter.f90,v 1.15 2006/11/03 01:41:20 aake Exp $"

  call print_trace (id, dbg_force, 'BEGIN')

  grav = 0.
  soft = 2.
  isolated = .false.
  max_error = 1.0e-3
  fft_error = 1.5e-3
  min_iter = 1
  max_iter = 50
  filter = (/0.202,0.050,0.000/)
  a_fft = 8.
  a_dom = 1.
  nlap = 20
  sor = 1.0
  verbose = .false.
  do_domain = .true.
  do_fft = .true.
  do_filter = .true.
  if (mpi_size == 1) then
    do_domain = .false.
    do_filter = .false.
  end if
  call read_selfg
  if (grav == 0) return

  allocate (phi(mx,my,mz))                                              ! need to keep this from step to step
  !$omp parallel
  do iz=izs,ize
    phi(:,:,iz) = 0.
  end do
  !$omp end parallel

  call test_gravpot
END SUBROUTINE

!***********************************************************************
SUBROUTINE read_selfg
  USE params
  USE selfgravity
  implicit none
  namelist /selfg/grav,soft,isolated,max_error,fft_error,max_iter,min_iter, &
    filter,sor,verbose,nlap,a_fft,a_dom,do_fft,do_domain,do_filter

  rewind (stdin); read (stdin,selfg)
  if (master) write (*,selfg)
END SUBROUTINE

!***********************************************************************
SUBROUTINE selfgrav (r,xdnr,ydnr,zdnr,dpxdt,dpydt,dpzdt)
  USE params
  USE selfgravity
  implicit none
  real, dimension(mx,my,mz):: r,xdnr,ydnr,zdnr,dpxdt,dpydt,dpzdt
  logical omp_in_parallel, debug

  if (debug(dbg_force)) print *,'selfgrav:',grav,omp_in_parallel(),omp_mythread
  if (grav == 0.) return
  if (omp_in_parallel()) then
    call selfgrav_omp (r,xdnr,ydnr,zdnr,dpxdt,dpydt,dpzdt)
  else
    !$omp parallel
    call selfgrav_omp (r,xdnr,ydnr,zdnr,dpxdt,dpydt,dpzdt)
    !$omp end parallel
  end if

END SUBROUTINE

!***********************************************************************
SUBROUTINE selfgrav_omp (r,xdnr,ydnr,zdnr,dpxdt,dpydt,dpzdt)
  USE params
  USE arrays, only: scr5, scr6
  USE selfgravity_arrays, only: phi
  implicit none
  real, dimension(mx,my,mz):: r,xdnr,ydnr,zdnr,dpxdt,dpydt,dpzdt
  integer iz
  character(len=mid):: id="$Id: gravpot_filter.f90,v 1.15 2006/11/03 01:41:20 aake Exp $"
  call print_trace (id, dbg_force, 'BEGIN')

!---------------------------------------------------------------------
!Gravity added RJ 8/2003
!---------------------------------------------------------------------  

  call gravpot (r, phi, scr5, scr6)
  call ddxdn_set (phi, scratch)
  do iz=izs,ize
    dpxdt(:,:,iz) = dpxdt(:,:,iz)-xdnr(:,:,iz)*scratch(:,:,iz)
  end do
  call ddydn_set (phi, scratch)
  do iz=izs,ize
    dpydt(:,:,iz) = dpydt(:,:,iz)-ydnr(:,:,iz)*scratch(:,:,iz)
  end do
  call ddzdn_set (phi, scratch)
  do iz=izs,ize
    dpzdt(:,:,iz) = dpzdt(:,:,iz)-zdnr(:,:,iz)*scratch(:,:,iz)
  end do

  call print_trace (id, dbg_force, 'END')
END SUBROUTINE

!***********************************************************************
SUBROUTINE laplace (phi, lphi)
  USE params, gy=>gscr, hy=>hscr
  !USE selfgravity
  implicit none
  real, dimension(mx,my,mz):: phi, lphi
  real, dimension(1,my,mz):: gx, hx
  real, dimension(mx,my,1):: gz, hz
  integer ix, iy, iz
  real a

  call mpi_send_x (phi, gx, 1, hx, 1)
  a = 1./dx**2
  do iz=izs,ize
  do iy=1,my
    do ix=2,mx-1
      lphi(ix,iy,iz) = a*(phi(ix+1,iy,iz)+phi(ix-1,iy,iz)-phi(ix,iy,iz)-phi(ix,iy,iz))
    end do
    lphi(1 ,iy,iz)   = a*(phi(2   ,iy,iz)+gx (1   ,iy,iz)-phi(1 ,iy,iz)-phi(1 ,iy,iz))
    lphi(mx,iy,iz)   = a*(hx (1   ,iy,iz)+phi(mx-1,iy,iz)-phi(mx,iy,iz)-phi(mx,iy,iz))
  end do
  end do

  !$omp master
  allocate (gy(mx,1,mz), hy(mx,1,mz))
  !$omp end master
  call mpi_send_y (phi, gy, 1, hy, 1)

  a = 1./dy**2
  do iz=izs,ize
    do iy=2,my-1
      do ix=1,mx
        lphi(ix,iy,iz) = lphi(ix,iy,iz) + a*(phi(ix,iy+1,iz)+phi(ix,iy-1,iz)-phi(ix,iy,iz)-phi(ix,iy,iz))
      end do
    end do
    do ix=1,mx
      lphi(ix,1 ,iz)   = lphi(ix, 1,iz) + a*(phi(ix,   2,iz)+gy (ix,   1,iz)-phi(ix,1 ,iz)-phi(ix,1 ,iz))
      lphi(ix,my,iz)   = lphi(ix,my,iz) + a*(hy (ix,   1,iz)+phi(ix,my-1,iz)-phi(ix,my,iz)-phi(ix,my,iz))
    end do
  end do

  !$omp barrier
  !$omp master
  deallocate (gy, hy)
  !$omp end master

  call mpi_send_z (phi, gz, 1, hz, 1)
  a = 1./dz**2
  do iz=max(2,izs),min(mz-1,ize)
    do iy=1,my
      do ix=1,mx
        lphi(ix,iy,iz) = lphi(ix,iy,iz) + a*(phi(ix,iy,iz+1)+phi(ix,iy,iz-1)-phi(ix,iy,iz)-phi(ix,iy,iz))
      end do
    end do
  end do
  if (izs == 1) then
    do iy=1,my
      do ix=1,mx
        lphi(ix,iy, 1) = lphi(ix,iy, 1) + a*(phi(ix,iy,   2)+ gz(ix,iy,   1)-phi(ix,iy, 1)-phi(ix,iy, 1))
      end do
    end do
  end if
  if (ize == mz) then
    do iy=1,my
      do ix=1,mx
        lphi(ix,iy,mz) = lphi(ix,iy,mz) + a*( hz(ix,iy,   1)+phi(ix,iy,mz-1)-phi(ix,iy,mz)-phi(ix,iy,mz))
      end do
    end do
  end if
END

!***********************************************************************
SUBROUTINE laplace_correct_filter (res, dphi, phi)
  USE params
  USE selfgravity
  implicit none
  real, dimension(mx,my,mz):: res, dphi, phi
  real, dimension(2,my,mz):: gx, hx
  real, dimension(mx,2,mz):: gy, hy
  real, dimension(mx,my,2):: gz, hz
  real tmpx(mx), tmpy(my), tmpz(mz)
  integer ix, iy, iz
  real a, b, c, d, norm, phiav
  character(len=mid):: id='laplace_correct_fft: $Id: gravpot_filter.f90,v 1.15 2006/11/03 01:41:20 aake Exp $'
!-----------------------------------------------------------------------
  call print_trace (id, dbg_force, 'BEGIN')

  norm = 0.25/(1./dx**2+1./dy**2+1./dz**2)/(3.-12*filter(1))
  a = (3.-6.*(filter(1)+filter(2)))*norm
  b = filter(1)*norm
  c = filter(2)*norm

  call mpi_send_x (res, gx, 2, hx, 2)
  do iz=izs,ize
  do iy=1,my
    do ix=4,mx-3
      dphi(ix,iy,iz) = a*res(ix,iy,iz) &
                     + b*(res(ix+1,iy,iz)+res(ix-1,iy,iz)) &
		     + c*(res(ix+2,iy,iz)+res(ix-2,iy,iz))
    end do
    dphi(1 ,iy,iz)   = a*res( 1,iy,iz) &
                     + b*(res(2   ,iy,iz)+gx (3   ,iy,iz)) &
                     + c*(res(3   ,iy,iz)+gx (2   ,iy,iz))
    dphi(2 ,iy,iz)   = a*res( 2,iy,iz) &
                     + b*(res(3   ,iy,iz)+res(1   ,iy,iz)) &
                     + c*(res(4   ,iy,iz)+gx (3   ,iy,iz))
    dphi(3 ,iy,iz)   = a*res( 3,iy,iz) &
                     + b*(res(4   ,iy,iz)+res(2   ,iy,iz)) &
                     + c*(res(5   ,iy,iz)+res(1   ,iy,iz))
    dphi(mx,iy,iz)   = a*res(mx,iy,iz) &
                     + b*(hx (1   ,iy,iz)+res(mx-1,iy,iz)) &
                     + c*(hx (2   ,iy,iz)+res(mx-2,iy,iz))
    dphi(mx-1,iy,iz) = a*res(mx-1,iy,iz) &
                     + b*(res(mx  ,iy,iz)+res(mx-2,iy,iz)) &
                     + c*(hx (1   ,iy,iz)+res(mx-3,iy,iz))
    dphi(mx-2,iy,iz) = a*res(mx-2,iy,iz) &
                     + b*(res(mx-1,iy,iz)+res(mx-3,iy,iz)) &
                     + c*(res(mx  ,iy,iz)+res(mx-4,iy,iz))
  end do
  end do

  call mpi_send_y (res, gy, 2, hy, 2)
  do iz=izs,ize
    do iy=4,my-3
      do ix=1,mx
        dphi(ix,iy,iz) = dphi(ix,iy,iz) &
	               + b*(res(ix,iy+1,iz)+res(ix,iy-1,iz)) &
	               + c*(res(ix,iy+2,iz)+res(ix,iy-2,iz))
      end do
    end do
    do ix=1,mx
      dphi(ix,1 ,iz)   = dphi(ix, 1,iz) &
                       + b*(res(ix,   2,iz)+gy (ix,   3,iz)) &
                       + c*(res(ix,   3,iz)+gy (ix,   2,iz))
      dphi(ix,2 ,iz)   = dphi(ix, 2,iz) &
                       + b*(res(ix,   3,iz)+res(ix,   1,iz)) &
                       + c*(res(ix,   4,iz)+gy (ix,   3,iz))
      dphi(ix,3 ,iz)   = dphi(ix, 3,iz) &
                       + b*(res(ix,   4,iz)+res(ix,   2,iz)) &
                       + c*(res(ix,   5,iz)+res(ix,   1,iz))
      dphi(ix,my,iz)   = dphi(ix,my,iz) &
                       + b*(hy (ix,   1,iz)+res(ix,my-1,iz)) &
                       + c*(hy (ix,   2,iz)+res(ix,my-2,iz))
      dphi(ix,my-1,iz) = dphi(ix,my-1,iz) &
                       + b*(res(ix,my  ,iz)+res(ix,my-2,iz)) &
                       + c*(hy (ix,   1,iz)+res(ix,my-3,iz))
      dphi(ix,my-2,iz) = dphi(ix,my-2,iz) &
                       + b*(res(ix,my-1,iz)+res(ix,my-3,iz)) &
                       + c*(res(ix,my  ,iz)+res(ix,my-4,iz))
    end do
  end do

  call mpi_send_z (res, gz, 2, hz, 2)
  do iz=max(4,izs),min(mz-3,ize)
    do iy=1,my
      do ix=1,mx
        dphi(ix,iy,iz) = dphi(ix,iy,iz) &
	               + b*(res(ix,iy,iz+1)+res(ix,iy,iz-1)) &
	               + c*(res(ix,iy,iz+2)+res(ix,iy,iz-2))
      end do
    end do
  end do
  if (izs == 1) then
    do iy=1,my
      do ix=1,mx
        dphi(ix,iy, 1) = dphi(ix,iy, 1) &
	               + b*(res(ix,iy,   2)+gz (ix,iy,   3)) &
	               + c*(res(ix,iy,   3)+gz (ix,iy,   2))
        dphi(ix,iy, 2) = dphi(ix,iy, 2) &
	               + b*(res(ix,iy,   3)+res(ix,iy,   1)) &
	               + c*(res(ix,iy,   4)+gz (ix,iy,   3))
        dphi(ix,iy, 3) = dphi(ix,iy, 3) &
	               + b*(res(ix,iy,   4)+res(ix,iy,   2)) &
	               + c*(res(ix,iy,   5)+res(ix,iy,   1))
      end do
    end do
  end if
  if (ize == mz) then
    do iy=1,my
      do ix=1,mx
        dphi(ix,iy,mz) = dphi(ix,iy,mz) &
	               + b*(hz (ix,iy,   1)+res(ix,iy,mz-1)) &
	               + c*(hz (ix,iy,   2)+res(ix,iy,mz-2))
        dphi(ix,iy,mz-1) = dphi(ix,iy,mz-1) &
	               + b*(res(ix,iy,  mz)+res(ix,iy,mz-2)) &
	               + c*(hz (ix,iy,   1)+res(ix,iy,mz-3))
        dphi(ix,iy,mz-2) = dphi(ix,iy,mz-2) &
	               + b*(res(ix,iy,mz-1)+res(ix,iy,mz-3)) &
	               + c*(res(ix,iy,mz  )+res(ix,iy,mz-4))
      end do
    end do
  end if
  phi(:,:,izs:ize) = phi(:,:,izs:ize) + sor*dphi(:,:,izs:ize)
  call print_trace (id, dbg_force, 'END')
END

!***********************************************************************
SUBROUTINE gravpot (rho, phi, residual, dphi)

!  Solve for the gravitational potential phi, given the density rho,
!  from the equation Laplace(phi) = grav*rho.

  USE params
  USE selfgravity
  implicit none
  real, dimension(mx,my,mz):: rho, phi, residual, dphi
  integer ix, iy, iz, iter, ilap
  real wt, wallclock
  real phiav
  logical debug
  character(len=mid):: id = "$Id: gravpot_filter.f90,v 1.15 2006/11/03 01:41:20 aake Exp $"
!-----------------------------------------------------------------------
  call print_trace (id, dbg_force, 'BEGIN')

  wt = wallclock()
  do iter=1,max_iter

!-----------------------------------------------------------------------
!  Compute the residual error in the Poisson equation -- this is cheap
!-----------------------------------------------------------------------
    call laplace (phi, residual)                                        ! Laplace operator
    call dumpl (residual, 'lapphi', 'gravpot.dmp', 0+(6+nlap)*(iter-1))
    call average_subr (rho, rhav)                                       ! average density
    residual(:,:,izs:ize) = residual(:,:,izs:ize) &
                          - grav*(rho(:,:,izs:ize)-rhav)                ! residual error
    call dumpl (residual, 'res', 'gravpot.dmp', 1+(6+nlap)*(iter-1))

!-----------------------------------------------------------------------
!  If the error is small enough, make min_iter corrections, exit
!-----------------------------------------------------------------------
    call fmaxval_subr ('resid', residual, error)
    error = error/(grav*rhav)
    if (verbose .and. master) print 1, mpi_rank, omp_mythread, 'iteration start', error
  1 format(2i4,2x,a,1pg12.3)
    if (error < max_error) then
      call laplace_correct_filter (residual, dphi, phi)
      do ilap=1,min_iter-1
        call average_subr (rho, rhav)                                   ! average density
        residual(:,:,izs:ize) = residual(:,:,izs:ize) &
                              - grav*(rho(:,:,izs:ize)-rhav)            ! residual error
        call laplace_correct_filter (residual, dphi, phi)
      end do
      exit
    end if

!-----------------------------------------------------------------------
!  Use an MPI domain iteration to correct inter-domain errors
!-----------------------------------------------------------------------
    if (do_domain) then
      call laplace_correct_domain (residual, dphi, phi)
      call dumpl (dphi, 'dphi', 'gravpot.dmp', 2+(6+nlap)*(iter-1))
      call dumpl (phi, 'phi', 'gravpot.dmp', 3+(6+nlap)*(iter-1))
      call laplace (phi, residual)                                      ! Laplace operator
      residual(:,:,izs:ize) = residual(:,:,izs:ize) &
                            - grav*(rho(:,:,izs:ize)-rhav)              ! residual error
      call dumpl (residual, 'res', 'gravpot.dmp', 4+(6+nlap)*(iter-1))
      if (verbose) then
        call fmaxval_subr ('resid', residual, error)
        error = error/(grav*rhav)
        if (master) print 1, mpi_rank, omp_mythread, 'after correct_domain ', error
      end if
    end if

!-----------------------------------------------------------------------
!  Use an FFT solver to correct local domain errors
!-----------------------------------------------------------------------
    if (do_fft) then
      call laplace_correct_fft (residual, dphi, phi)
      call dumpl (phi, 'phi', 'gravpot.dmp', 5+(6+nlap)*(iter-1))
      call laplace (phi, residual)                                      ! Laplace operator
      residual(:,:,izs:ize) = residual(:,:,izs:ize) &
                             - grav*(rho(:,:,izs:ize)-rhav)             ! residual error
      call dumpl (residual, 'res', 'gravpot.dmp', 6+(6+nlap)*(iter-1))
      if (verbose) then
        call fmaxval_subr ('resid', residual, error)
        error = error/(grav*rhav)
        if (master) print 1, mpi_rank, omp_mythread, 'after correct_fft ', error
      end if
    end if

!-----------------------------------------------------------------------
!  Use a filtered Jacobi solver to correct small scale (domain-bdry) errors
!-----------------------------------------------------------------------
    if (do_filter) then
      call laplace_correct_filter (residual, dphi, phi)                 ! filter the residual
      do ilap=1,nlap-1
        call laplace (phi, residual)                                    ! Laplace operator
        residual(:,:,izs:ize) = residual(:,:,izs:ize) &
                              - grav*(rho(:,:,izs:ize)-rhav)            ! residual error
        call laplace_correct_filter (residual, dphi, phi)               ! filter the residual
        call dumpl (residual, 'res', 'gravpot.dmp', 6+ilap+(6+nlap)*(iter-1))
        call fmaxval_subr ('resid', residual, error)
        error = error/(grav*rhav)
	if (error < max_error) exit
        if (verbose) then
          if (master) print 1, mpi_rank, omp_mythread, 'after correct_filter ', error
        end if
      end do
    end if
  end do

  call print_trace (id, dbg_force, 'END')
END

!***********************************************************************
SUBROUTINE laplace_correct_fft (res, dphi, phi)
!
!  Solve for the gravitational potential correction dphi, given the
!  residual from the equation res = Laplace(phi) = grav*rho.
!
  USE params
  USE selfgravity, only: soft, grav, a_fft
  implicit none
  real, dimension(mx,my,mz):: res, dphi, phi
  real k2norm, phiav
  integer iz
  character(len=mid):: id='laplace_correct_fft: $Id: gravpot_filter.f90,v 1.15 2006/11/03 01:41:20 aake Exp $'
!-----------------------------------------------------------------------
  call print_trace (id, dbg_force, 'BEGIN')
  call fft3df (res, scratch, mx, my, mz)                                ! forward FFT
  call fft3d_k2 (dphi, dx, dy, dz, mx, my, mz)                          ! compute k**2 => dphi
  if (izs.eq.1) dphi(1,1,1) = a_fft*(2.*pi/sx)**2                       ! no mean phi
  !$omp barrier                                                         ! make sure phi is complete
  k2norm = 2.*(soft*dx/pi)**2                                           ! normalization factor
  do iz=izs,ize
    scratch(:,:,iz) = scratch(:,:,iz)/(dphi(:,:,iz)+1e-10) &            ! 1/k^2
                   *exp(-k2norm*dphi(:,:,iz))                           ! softening
  end do
  if (izs.eq.1) scratch(1,1,1) = 0.0                                    ! no mean phi
  call fft3db (scratch, dphi, mx, my, mz)                               ! reverse FFT
  !$omp barrier                                                         ! make sure phi is complete
  phi(:,:,izs:ize) = phi(:,:,izs:ize) + dphi(:,:,izs:ize)
  call print_trace (id, dbg_force, 'END')
END

!***********************************************************************
SUBROUTINE laplace_correct_domain (res, dphi, phi)
!
!  Solve for the gravitational potential correction dphi, using Jacobi
!  iteration on the domain scale, given the residual from the equation
!  res = Laplace(phi) = grav*rho.
!
  USE params
  USE selfgravity, only: a_dom, verbose
  implicit none
  real, dimension(mx,my,mz):: res, dphi, phi
  real c, phiav, resav, dphia
  integer ix, iy, iz
  logical debug
  character(len=mid):: id='laplace_correct_local: $Id: gravpot_filter.f90,v 1.15 2006/11/03 01:41:20 aake Exp $'
!-----------------------------------------------------------------------
  call print_trace (id, dbg_force, 'BEGIN')
  call average_domain (res, resav)
  c = 1.178/(0.5*mx)**2/(0.5*my)**2/(0.5*mz)**2                         ! empirical, for best convergence
  dphia = a_dom*c*.25/(1./(dx*mz)**2+1./(dy*my)**2+1./(dz*mz)**2)*resav
  do iz=izs,ize; do iy=1,my; do ix=1,mx
    phi(ix,iy,iz) = phi(ix,iy,iz) + dphia &
                    *(ix*(mx-ix)) &
                    *(iy*(my-iy)) &
                    *(iz*(mz-iz))
  end do; end do; end do
  if (verbose .and. master) print *,' lap_cor_dom: rank, resav, dphia =', mpi_rank, resav, dphia
  call print_trace (id, dbg_force, 'END')
END

!***********************************************************************
SUBROUTINE test_gravpot
  USE params
  USE arrays, only: scr5, scr6
  USE selfgravity
  USE selfgravity_arrays
  implicit none
  real kx, ky, kz
  integer ix, iy, iz

  kx = 2.*pi/sx
  ky = 2.*pi/sy
  kz = 2.*pi/sz
  print *,'kx',kx

  allocate (rho(mx,my,mz), phi0(mx,my,mz))

  !$omp parallel
  do iz=izs,ize
  do iy=1,my
  do ix=1,mx
    rho(ix,iy,iz) = 1.+0.5*sin(kx*xm(ix))*sin(ky*ym(iy))*sin(kz*zm(iz))
  end do
  end do
  end do
  !$omp end parallel

  if (verbose) then
    !rho(6,my/4+1,mz/4+1) = 10.
    print '(i3,1x,a,20f7.3)', mpi_rank, '  xm',  xm(1:20)
    print '(i3,1x,a,20f7.3)', mpi_rank, '  ph',  xm(1:20)*kx
    print '(i3,1x,a,20f7.3)', mpi_rank, ' rho', rho(1:20,my/4+1,mz/4+1)
  end if

  !$omp parallel
  call gravpot (rho, phi, scr5, scr6)
  !$omp end parallel

  !$omp parallel
  do iz=izs,ize
  do iy=1,my
  do ix=1,mx
    phi(ix,iy,iz) = 0.
  end do
  end do
  end do
  !$omp end parallel

  deallocate (rho, phi0)
END
