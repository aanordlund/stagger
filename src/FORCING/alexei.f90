! $Id: alexei.f90,v 1.6 2007/07/06 17:21:27 aake Exp $
!**********************************************************************
MODULE forcing

  USE params

  real:: enom, t_damp, mach, calib
  logical do_force, do_helmh
  character(len=mfile) input
  real, allocatable, dimension(:,:,:):: franx,frany,franz
  logical, parameter:: conservative=.true.

END MODULE

!***********************************************************************
  SUBROUTINE init_force
    USE params
    USE forcing
    integer iz
    character(len=80):: id='$Id: alexei.f90,v 1.6 2007/07/06 17:21:27 aake Exp $'

    if (id.ne.'') print *,id
    id=''

    do_force = .true.
    mach = 6.
    calib = 0.648
    t_damp = 0.5/mach
    input = 'alexei.dat'

    allocate (franx(mx,my,mz), frany(mx,my,mz), franz(mx,my,mz))
!$omp parallel do private(iz)
    do iz=1,mz                                ! encourage distribution
      franx(:,:,iz)=0. ; frany(:,:,iz)=0. ; franz(:,:,iz)=0.
    end do

    call read_force
  END SUBROUTINE

!***********************************************************************
  SUBROUTINE read_force
    USE params
    USE forcing
    namelist /force/ do_force, input, mach, calib, t_damp
    real, dimension(mx,my,mz):: scr

    if (stdin.ge.0) then
      rewind (stdin); read (stdin,force)
    else
      read (*,force)
    end if
    write (*,force)
    enom = calib*mach**3

    open (12,file=input,status='old',form='unformatted',access='direct',recl=lrec(mw))
    read (12,rec=1) scr
    call xdn_set (scr, franx)
    read (12,rec=2) scr
    call ydn_set (scr, frany)
    read (12,rec=3) scr
    call zdn_set (scr, franz)
    close (12)

    call init_selfg
  END SUBROUTINE

!***********************************************************************
SUBROUTINE forceit (r,Ux,Uy,Uz,xdnr,ydnr,zdnr,dpxdt,dpydt,dpzdt)
!
  USE params
  USE forcing

  implicit none
  real rav, xav, yav, zav, edot, c_damp, f, average
  real, dimension(mx,my,mz):: r,Ux,Uy,Uz,xdnr,ydnr,zdnr,dpxdt,dpydt,dpzdt
  real, dimension(mx,my,mz):: scr

  integer i,j,k

  call selfgrav (r,xdnr,ydnr,zdnr,dpxdt,dpydt,dpzdt)

  if (.not. do_force) return

!$omp parallel do private(k)
  do k=1,mz
    scr(:,:,k) = xdnr(:,:,k)*franx(:,:,k)*Ux(:,:,k) &
               + ydnr(:,:,k)*frany(:,:,k)*Uy(:,:,k) &
               + zdnr(:,:,k)*franz(:,:,k)*Uz(:,:,k)
  end do
  edot = average(scr)
  f = enom/edot

  rav = 1./average(r)
  c_damp = 1./t_damp
!$omp parallel do private(k)
  do k=1,mz
    scr(:,:,k) = xdnr(:,:,k)*(f*franx(:,:,k)+Ux(:,:,k)*c_damp)*rav
  end do
  if (do_trace) print *,'turbulence: average2'
  xav = average(scr)

!$omp parallel do private(k)
  do k=1,mz
    scr(:,:,k) = ydnr(:,:,k)*(f*frany(:,:,k)+Uy(:,:,k)*c_damp)*rav
  end do
  if (do_trace) print *,'turbulence: average3'
  yav = average(scr)

!$omp parallel do private(k)
  do k=1,mz
    scr(:,:,k) = zdnr(:,:,k)*(f*franz(:,:,k)+Uz(:,:,k)*c_damp)*rav
  end do
  if (do_trace) print *,'turbulence: average4'
  zav = average(scr)

!$omp parallel do private(k)
  do k=1,mz
    dpxdt(:,:,k) = dpxdt(:,:,k) + xdnr(:,:,k)*(f*franx(:,:,k) - xav)
    dpydt(:,:,k) = dpydt(:,:,k) + ydnr(:,:,k)*(f*frany(:,:,k) - yav)
    dpzdt(:,:,k) = dpzdt(:,:,k) + zdnr(:,:,k)*(f*franz(:,:,k) - zav)
  end do

END SUBROUTINE

