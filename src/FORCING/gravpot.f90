! $Id: gravpot.f90,v 1.12 2009/09/30 05:30:35 aake Exp $
!***********************************************************************
SUBROUTINE gravpot (rho, phi)

!  Solve for the gravitational potential phi, given the density rho,
!  from the equation Laplace(phi) = grav*rho.

  use params
  implicit none
  real, dimension(mx,my,mz):: rho, phi
  logical omp_in_parallel, debug

  if (omp_in_parallel()) then
    call gravpot_omp (rho, phi)
  else
    !$omp parallel
    call gravpot_omp (rho, phi)
    !$omp end parallel
  end if
END

!***********************************************************************
SUBROUTINE gravpot_omp (rho, phi)

!  Solve for the gravitational potential phi, given the density rho,
!  from the equation Laplace(phi) = grav*rho.

  use params
  use arrays, only: scr5, scr6
  use selfgravity, only: grav, soft, isolated
  implicit none
  real, dimension(mx,my,mz):: rho, phi
  real k2norm, rhoa
  real, save:: av
  integer iz, nperif
  logical omp_in_parallel, debug

  character(len=mid):: id = "$Id: gravpot.f90,v 1.12 2009/09/30 05:30:35 aake Exp $"
!-----------------------------------------------------------------------

  call print_id(id)
  if (.not. (allocated(scr5).and.allocated(scr6))) then
    if (master) print *,'gravpot: scratch arrays not allocated;',allocated(scr5),allocated(scr6)
    allocate (scr5(mx,my,mz),scr6(mx,my,mz))
    !call end_mpi
    !stop
  end if

  if (.not. isolated) then                                              ! periodic case
    call fft3df (rho, phi, mx, my, mz)                                  ! forward FFT
    call dumpl (rho,'rh','selfgrav.dmp',0)

  else                                                                  ! isolated case
    !scr => scr5
    !k2  => scr6

    do iz=izs,ize
      scr5(:,:,iz) = rho(:,:,iz)
    end do
    scr5(1,:,izs:ize) = 0.
    scr5(:,1,izs:ize) = 0.
    if (izs.eq.1) scr5(:,:,1) = 0.
    scr5(mx,:,izs:ize) = 0.
    scr5(:,my,izs:ize) = 0.
    if (ize.eq.mz) scr5(:,:,mz) = 0.

    call average_subr (scr5, av)
    nperif = 2*(mx*my+mx*mz+my*mz)-4*(mx+my+mz)+8
    rhoa = (av*mw)/nperif
    if (debug(dbg_force)) print *,'gravpot: av=',av,omp_in_parallel(),omp_mythread

    scr5(1,:,izs:ize) = -rhoa
    scr5(:,1,izs:ize) = -rhoa
    if (izs.eq.1) scr5(:,:,1) = -rhoa
    scr5(mx,:,izs:ize) = -rhoa
    scr5(:,my,izs:ize) = -rhoa
    if (ize.eq.mz) scr5(:,:,mz) = -rhoa

    call fft3df (scr5, phi, mx, my, mz)                                 ! forward FFT
    call dumpl (scr5,'rh','selfgrav.dmp',0)
  end if

  call fft3d_k2 (scr6, dx, dy, dz, mx, my, mz)                          ! compute k**2
  call dumpl (scr6,'k2','selfgrav.dmp',1)
  call dumpl (phi,'rt','selfgrav.dmp',2)
  !$omp barrier                                                         ! make sure phi is complete

  k2norm = 2.*(soft*dx/pi)**2                                           ! normalization factor
  !do iz=1,mx/2
  !  print *,iz,exp(-(k2norm*scr6(iz,1,1))**2)
  !end do
  do iz=izs,ize
    !scr5(:,:,iz) = -phi(:,:,iz)*grav/(scr6(:,:,iz)+1e-10)*exp(-(k2norm*scr6(:,:,iz))**2)        ! softening
    scr5(:,:,iz) = -phi(:,:,iz)*grav/(scr6(:,:,iz)+1e-10)*exp(-k2norm*scr6(:,:,iz))        ! softening
  end do

  if (  izs == 1 .and. &
      mpi_x == 0 .and. &
      mpi_y == 0 .and. &
      mpi_z == 0) scr5(1,1,1) = 0.0     ! no mean phi

  call dumpl (scr5,'scr5','selfgrav.dmp',3)
  call fft3db (scr5, phi, mx, my, mz)                                   ! reverse FFT
  call dumpl (phi,'phi','selfgrav.dmp',4)
  !$omp barrier                                                         ! make sure phi is complete

END
