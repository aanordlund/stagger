! $Id: stars.f90,v 1.17 2005/03/01 16:11:40 antonio Exp $
!***********************************************************************
MODULE explosions

!
!  Common block for supernova quantities.
!
  implicit none
  integer snseed,ctime,lbdist,mbdist,nbdist,turnon
  integer ylen,taulen,lmsid,plmsid,mhms,mlms, Meynetlifelen, Meynetmasslen, &
       MeynetlifePh1len, MeynetmassPh1len,nSN

  logical do_expl, do_delayexpl, do_lmsf, do_psft, do_asym,do_initialSNE

  real &
    dtsn,sntime,psftime,         &
    esn,dsn,wsn,psn,             &
    Tlimit,rholim,convlim,fvesc, &
    theta_eject, width_eject,    &
    totalstellarmass, tSN 

  character (len=40) yfile, taufile, Meynetlifefile, Meynetmassfile, &
       MeynetlifePh1file, MeynetmassPh1file

  integer, allocatable, dimension (:,:) :: hmspos
  integer, allocatable, dimension (:,:) :: lmspos
  integer, allocatable, dimension (:,:) :: dumpos
  real, allocatable, dimension(:,:) :: hms
  real, allocatable, dimension(:,:) :: lms
  real, allocatable, dimension(:,:) :: dum
  real, allocatable, dimension(:,:) :: tautbl
  real, allocatable, dimension(:,:) :: ytbl
  real, allocatable, dimension(:,:,:) :: temp
  real, allocatable, dimension(:,:) :: Meynetmasstbl
  real, allocatable, dimension(:,:) :: MeynetmassPh1tbl
  real, allocatable, dimension(:,:) :: Meynetlifetbl
  real, allocatable, dimension(:,:) :: MeynetlifePh1tbl


real, public:: bbnsII

CONTAINS

!***********************************************************************
SUBROUTINE init_explode (rho)

  USE params
  USE units
  USE cooling
  implicit none

  integer :: i, j, k
  real    :: rlow, f 
  real, dimension(mx,my,mz):: rho

  Tlimit = 4000.     ! temperature criterion for star formation
  rholim = 1.0       ! density criterion
  fvesc = 3.         ! convergence criterion; escape velocity scaling factor - large value => no effect 
  ctime = 5
  sntime = 10.
  psftime = 0.01     ! proto-stellar formation time 
  esn = 1.
  dsn = 1.0        
  wsn = 4.
  psn = 5.
  snseed = -1
  nSN=3
  tSN=1.
  do_initialSNE=.false.
  turnon=-1
  do_expl = .true.                               ! supernova explosions?
  do_delayexpl = .false.                         ! tracking of high-mass stars?
  do_lmsf = .false.                              ! formation of low-mass stars? 
  do_psft = .false.                              ! tracking of low-mass proto-stars?
  do_asym = .false.                              ! assymetric ejection of metals?
  theta_eject = 0.                               ! ejection vector
  width_eject = pi/4.                            ! width of ejection cone
  yfile = '../src/FORCING/YIELDS/pFe_WW95.tbl'   
  ylen  = 8
  taufile = '../src/FORCING/YIELDS/tau.tbl'
  taulen = 28
  Meynetlifefile = '../src/FORCING/YIELDS/Meynetlife.tbl'
  Meynetlifelen = 8
  Meynetmassfile = '../src/FORCING/YIELDS/Meynetmass.tbl'
  Meynetmasslen = 8
  MeynetlifePh1file = '../src/FORCING/YIELDS/MeynetlifePh1.tbl'
  MeynetlifePh1len = 8
  MeynetmassPh1file = '../src/FORCING/YIELDS/MeynetmassPh1.tbl'
  MeynetmassPh1len = 8

  dtsn = 0.

  call read_explode
  call init_wind 
  if (.not. do_expl) return

  allocate (ytbl(ylen,2))
  if (do_trace) print *,'reading ',yfile
  open(1,file=yfile, status='old', form='formatted')
  do k=1,ylen
    read(1,*) ((ytbl(i,j), i=k,k), j=1,2)
  end do
  close(1)
  
  if (do_delayexpl) then
    mhms = 0             ! no high-mass star exists from the beginning
    
    allocate(Meynetlifetbl(Meynetlifelen,2))
    if (do_trace) print *,'reading ',Meynetlifefile
    open(1,file=Meynetlifefile, status='old', form='formatted')
    do k=1,Meynetlifelen
      read(1,*) ((Meynetlifetbl(i,j), i=k,k), j=1,2)
    end do
    close(1)   
    
    allocate(Meynetmasstbl(Meynetmasslen,2))
    if (do_trace) print *,'reading ',Meynetmassfile
    open(1,file=Meynetmassfile, status='old', form='formatted')
    do k=1,Meynetmasslen
      read(1,*) ((Meynetmasstbl(i,j), i=k,k), j=1,2)
    end do
    close(1)

    allocate(MeynetlifePh1tbl(MeynetlifePh1len,2))
    if (do_trace) print *,'reading ',MeynetlifePh1file
    open(1,file=MeynetlifePh1file, status='old', form='formatted')
    do k=1,MeynetlifePh1len
      read(1,*) ((MeynetlifePh1tbl(i,j), i=k,k), j=1,2)
    end do
    close(1)

    allocate(MeynetmassPh1tbl(MeynetmassPh1len,2))
    if (do_trace) print *,'reading ',MeynetmassPh1file
    open(1,file=MeynetmassPh1file, status='old', form='formatted')
    do k=1,MeynetmassPh1len
      read(1,*) ((MeynetmassPh1tbl(i,j), i=k,k), j=1,2)
    end do
    close(1)
        
! form nSN stars to be exploted in the time interval (t,tSN) million years
    if (do_initialSNE.and.turnon.lt.0) call initial_SNE              !artificial SNE??


    if (iread.ge.0) call read_stars
    if (iread.lt.0.and.turnon.lt.0.and.do_initialSNE) call read_stars
  endif   
   
  if (do_lmsf) then
    lmsid = 0
    if (do_psft) then
      mlms = 0
      plmsid = 0
    end if 
  end if  

  totalstellarmass=0.

!  call init_wind


END subroutine

!***********************************************************************
SUBROUTINE read_explode
  USE params
  USE units
  implicit none
  real f, rlow

  namelist /expl/ &
    do_expl,do_delayexpl,do_lmsf,do_psft,do_asym,    &
    snseed,sntime,psftime,theta_eject,width_eject,   &
    Tlimit,rholim,fvesc,ctime,esn,dsn,wsn,psn,       &
    ylen,yfile,taulen,taufile, Meynetlifefile,       &
    Meynetlifelen, Meynetmasslen, Meynetmassfile,    &
    nSN,tSN,do_initialSNE,turnon

  f=0.8888889        ! tuning parameter for rlow (which sets a lower limit on rholim)  
!  f=0.7
  read (*,expl)
  convlim = fvesc*vesc0   ! sqrt(rho) x convlim = escape velocity per unit length
  rlow=real(ceiling(10.*f*(100.*msun/cmu)/(0.6666667*(1.5*sqrt(pi)*wsn*dx)**3)))/10.
  if ((do_delayexpl) .and. (rholim .lt. rlow)) then
    rholim=rlow
    print *, 'Warning: rholim is set to',rholim,' by the mass substraction routine'
  end if
  write (*,expl)
  write(*,*) 'CONVLIM =',convlim
  write(*,*) 'Rlow=====    ',rlow
END subroutine

!***********************************************************************
SUBROUTINE write_stars
  USE params
  implicit none
  integer :: i    
  
  i = index(file,'.')                     
  open(1,file=file(1:i)//'stars',status='replace')
  write(1,*) mhms,t 
  write(1,*) hms
  write(1,*) hmspos
  close(1)

  
END subroutine

!***********************************************************************
SUBROUTINE read_stars
   USE params
  implicit none

  logical exists
  integer :: i
  real :: t1
  
  i = index(from,'.')                     
  inquire (file=from(1:i)//'stars',exist=exists)
  if (exists) then
     open(1,file=from(1:i)//'stars',status='old')
     read(1,*) mhms,t1
     allocate(hms(mhms,19))
     allocate(hmspos(mhms,3))
     read(1,*) hms
     read(1,*) hmspos
     write (*,*) 'Data of ',mhms,' massive stars recovered at time ',t1  
     close(1)
  else     
     write (*,*) 'NO data of massive stars to be recovered'  
  endif
END subroutine 
!***********************************************************************
SUBROUTINE initial_SNE

  USE params
  implicit none
  
  integer i,iz
  real ran1s,rn,alpha,p,imfk,imfm,pmass 
  real, dimension (mx,my,mz) :: rhoart, pxart, pyart, pzart, eart, dart

  do iz=1,mx
     rhoart(:,:,iz)=1.
     pxart(:,:,iz)=0.
     pyart(:,:,iz)=0.
     pzart(:,:,iz)=0.
     eart(:,:,iz)=0.
     dart(:,:,iz)=0.
  enddo
  alpha = 2.35            ! parameters for calculating SN progenitor masses 
  p     = -1./(alpha-1.)
  imfk  = 0.058375757
  imfm  = 1.995262E-03
  do i=1,nSN                ! form nSN stars to be exploted (randomly)
                            ! in the time interval (t,tSN) million years
      rn=ran1s(snseed)
      pmass=(rn*imfk+imfm)**p
      call tracksn(rhoart,pxart,pyart,pzart,eart,dart,pmass,1,1,1)
      rn=ran1s(snseed) 
      hms(i,1)=t+rn*tSN       !Explosion in interval (t,tSN)
      hms(i,12)=0.
      hms(i,17)=0.
      hmspos(i,1)=rn*mx
      rn=ran1s(snseed)
      hmspos(i,2)=rn*my
      rn=ran1s(snseed)
      hmspos(i,3)=rn*mz
      hms(i,8)=0.
      hms(i,9)=0.
      hms(i,10)=0.  
      print *,' STAT. FOR THE INITIAL SNe ',i
      print *, 'Explosion time, iexp,jexpl,zexpl, mass'
      print *, hms(i,1), hmspos(i,1),hmspos(i,2),hmspos(i,3), pmass 
      print *, 'velocidad',hms(i,8),hms(i,9),hms(i,10) 
 enddo
  call write_stars 
  deallocate(hms)
  deallocate(hmspos)


END SUBROUTINE  
!***********************************************************************

SUBROUTINE explode (rho,px,py,pz,e,d,Bx,By,Bz)

  USE params
  USE stagger
  USE units
  USE eos
  USE cooling
  implicit none

  integer lbdist,mbdist,nbdist,ntime,i,j,k,ldist,mdist,ndist

  real ran1s  
  real rnd,rnd2,massconversion,elimit,eemin,alpha
  real p,imfk,imfm,starformationfraction,starMabove8Msun 
  real lowmasstarM,SNmass,lmmass,freq,lmsf_freq,volume
  real cloudlifetime, progmass

  double precision totmasse,totmassr,totmassc,totmass
  double precision tosolarM

  real, dimension(mx,my,mz) :: rho,px,py,pz,e,d,Bx,By,Bz
!hpf$ distribute(*,*,block):: &
!hpf$   rho,px,py,pz,e,d,Bx,By,Bz
  real, allocatable, dimension(:,:,:) :: lnr, ux, uy, uz, conv,ee
!hpf$ distribute(*,*,block):: lnr, ux, uy, uz, conv, ee
  character (len=68) id
  data id &
  /'$Id: stars.f90,v 1.17 2005/03/01 16:11:40 antonio Exp $'/

  if (.not. do_expl) return
  if (it.eq.turnon) then       !Artificial SNE???
     call initial_SNE
     call read_stars
  endif
!Save hms every nsnap (at the same time as 'write_snap')

!TEST!!!!!!!!!!!!! Creo una estrella de 50 para seguirla el recorrido

!if (it.eq.1) then
!   call tracksn(rho,px,py,pz,e,d,22.,100,100,100)
!   print *,'ESTRELLA DE 22.Mo'
!endif

if (do_delayexpl) then
   if(mod(it,nsnap).eq.0 .and. mhms.gt.0) call write_stars
endif
allocate(ee(mx,my,mz))

  ee = e/rho
  
  volume = mx*dx*my*dy*mz*dz

  alpha = 2.35            ! parameters for calculating SN progenitor masses 
  p     = -1./(alpha-1.)
  imfk  = 0.058375757
  imfm  = 1.995262E-03

  if (id.ne.' ') then
    print *,id
    print *,'Tlimit, rholim, convlim =', Tlimit, rholim, convlim
  endif
  id=' '

  elimit=Tlimit/utemp


!***********************************
!      parsec=3.09e18
!      solarM=1.99e33
!      tosolarM=1e-24/solarM
!      
!      print*,'tosolarM',tosolarM
!      cellvolume=(x4-x1)**2*(z4-z1)*(1000*parsec)**3/mw_LONG
!      massconversion=cellvolume*tosolarM_LONG

  tosolarM=cdu/msun
  massconversion=tosolarM*clu**3*(dx*dy*dz)
  
  totmass=0.0
  totmasse=0.0
  totmassr=0.0
  totmassc=0.0
  eemin=ee(1,1,1)
  if (mod(it,ctime) .eq. ctime-1) then
    allocate(ux(mx,my,mz),  &
             uy(mx,my,mz),  &
             uz(mx,my,mz),  &
             lnr(mx,my,mz), &
             conv(mx,my,mz))
    lnr = alog(rho)
    ux = px/exp(xdn(lnr))
    uy = py/exp(ydn(lnr))
    uz = pz/exp(zdn(lnr))
    !if (do_2nddiv) then
      !conv = -(ddxup1(ux) + ddyup1(uy) + ddzup1(uz))/sqrt(rho)
    !else
      !conv = -(ddxup (ux) + ddyup (uy) + ddzup (uz))/sqrt(rho) 
    !end if
    if (do_ionization) then
      allocate (temp(mx,my,mz))
      do k=1,mz
        call eos2 (rho(:,:,k),ee(:,:,k),tt=temp(:,:,k))
        do j=1,my
          do i=1,mx
            eemin=amin1(eemin,ee(i,j,k))
            if (temp(i,j,k).lt.Tlimit.and.rho(i,j,k).ge.rholim.and.conv(i,j,k).gt.convlim) then
               totmass=massconversion*rho(i,j,k)+totmass
            end if
          end do
        end do
      end do  
    else
      do k=1,mz
        do j=1,my
          do i=1,mx
            eemin=amin1(eemin,ee(i,j,k))
            if (ee(i,j,k).lt.elimit) then
               totmasse=massconversion*rho(i,j,k)+totmasse
            end if 
            if (rho(i,j,k).lt.rholim) then
               totmassr=massconversion*rho(i,j,k)+totmassr
            end if
            !if (conv(i,j,k).lt.convlim) then
               !totmassc=massconversion*rho(i,j,k)+totmassc
            !end if
            !if (ee(i,j,k).lt.elimit.and.rho(i,j,k).ge.rholim.and.conv(i,j,k).gt.convlim) then
            if (ee(i,j,k).lt.elimit.and.rho(i,j,k).ge.rholim) then
               totmass=massconversion*rho(i,j,k)+totmass
            end if
          end do
        end do
      end do  
    end if
    dtsn=dtsn+dt
    deallocate(lnr,ux,uy,uz,conv)
  else 
    dtsn=dtsn+dt
    call stellar_wind(rho,e)                       ! stellar wind 
    deallocate(ee)  
    return
  end if


  write(*,*) 'totmass', totmass,totmasse,totmassr,totmassc

  rnd=ran1s(snseed)


  open (3,file=file(1:index(file,'.'))//'sn')
  write(3,100) '  #rnd,freq,mass,SNrate,Tlimit,Tmin,rhomax:',rnd,freq, &
                totmass,freq/dtsn/volume,Tlimit,eemin*utemp,maxval(rho)
  close (3)
 
  100 format(1x,a,2f6.3,2(1pe11.3),2(0pf7.0),1pe10.2)

  if (do_lmsf) then
    lmsf_freq=lowmasstarM*starformationfraction*totmass/lmmass
    lmsf_freq=lmsf_freq*dtsn/cloudlifetime
    call lmsf(rho,px,py,pz,ee,d,lmsf_freq,elimit,rholim,convlim)
  end if
 
  if (totmass.gt.100.) then 
    rnd2=ran1s(snseed)
    progmass=(rnd2*imfk+imfm)**p
    call snpose(rho,px,py,pz,ee,lbdist,mbdist,nbdist,elimit,rholim,convlim)
    if (.not. do_delayexpl) then
      write (*,*) 'Super Nova stats'
      write (*,*) '  #Type II, tanh()-like profile distribution in log e' 
      call SNexpl (rho,e,d,2,progmass,lbdist,mbdist,nbdist)
    else 
      write (*,*) 'High-mass star stats'   
      call tracksn(rho,px,py,pz,e,d,progmass,lbdist,mbdist,nbdist)
    end if
  end if
  if (do_delayexpl) then
    if (mhms .gt. 0) then 
      if (t .ge. hms(1,1)) then
        write (*,*) 'Super Nova stats'
        write (*,*) '  #Type II, tanh()-like profile distribution in log e' 
        call SNexpl(rho,e,d,2,hms(1,15),hmspos(1,1),hmspos(1,2),hmspos(1,3),hms(1,3),hms(1,4),hms(1,5))
        if (mhms .gt. 1) then
          allocate(dum(mhms-1,19))
          allocate(dumpos(mhms-1,3))
          dum=hms(2:mhms,:)
          dumpos=hmspos(2:mhms,:)
          deallocate(hms,hmspos)
          allocate(hms(mhms-1,19))
          allocate(hmspos(mhms-1,3))
          hms=dum
          hmspos=dumpos
          deallocate(dum,dumpos)
          mhms=mhms-1            ! a supernova has exploded
        else
          deallocate(hms,hmspos)
          mhms=0
        end if
      end if
    end if
  end if
  if (do_ionization) deallocate(temp)
  dtsn=0.
  call stellar_wind(rho,e)                       ! stellar wind
  deallocate(ee) 
END subroutine


!************************************************************
SUBROUTINE tracksn (rho,px,py,pz,e,d,progm,li,mi,ni)

  USE params
  USE units
  USE stagger
  implicit none

  integer :: i, j, l, m, n, ll, mm, nn, itau, maxtry, li, mi, ni, lf, mf, nf 
  real, dimension (mx,my,mz) :: rho, scr, px, py, pz, e, ee, d, eold, dold, prof,   &
                                uxc, uyc, uzc
  real :: elimit, lnmass, cmass, cenergy, cmetal, dwidth, arg, ampl, totmass, msub, &
          prerho0, postrho0, lifetime, expltime, stau, a0, a1, xx, yy, zz, logph,   &
          x(mx), y(my), z(mz), progm, rhoav, ln10,a1M,a0M, &
          totallostM,remainingM,Ph1lifetime,Ph1totallostM, initialVinf

  real, allocatable,dimension(:) :: a,b 


  maxtry = 3
  ee = e/rho
  elimit = Tlimit/utemp
  rhoav = 1.
  ln10 = 2.3025851

  uxc = xup(px)/rho   ! u_x in pc/Myr
  uyc = yup(py)/rho   ! u_y in pc/Myr
  uzc = zup(pz)/rho   ! u_z in pc/Myr

  ! stellar mass substraction: if successfully removed it will be re-inserted
  ! in the explosion stage in order to avoid too high central temperatures. 

  cmass   = 0.0
  cenergy = 0.0
  cmetal  = 0.0  
  
  dwidth=1.5*wsn*dx
  do i=1,maxtry
    do j=1,mx
      x(j) = (j-mx/2)*dx
    end do
    do j=1,my
      y(j) = (j-my/2)*dy
    end do
    do j=1,mz
      z(j) = (j-mz/2)*dz
    end do

    do n=1,mz
      zz=z(n)-z(mz/2)
      nn=ni+(n-mz/2)
      if (nn .gt. mz) then
        nn=nn-mz
      else if (nn .le. 0) then
        nn=nn+mz
      endif
      do m=1,my
        yy=y(m)-y(my/2)
        mm=mi+(m-my/2)
        if (mm .gt. my) then
          mm=mm-my
        else if (mm .le. 0) then
          mm=mm+my
        endif
        do l=1,mx
          xx=x(l)-x(mx/2)
          ll=li+(l-mx/2)
          if (ll .gt. mx) then
            ll=ll-mx
          else if (ll .le. 0) then
            ll=ll+mx
          endif
          arg=(xx**2+yy**2+zz**2)/dwidth**2 
          arg=arg**3                              
          prof(ll,mm,nn)=rho(ll,mm,nn)*exp(-arg)  ! gaussian-like profile x rho         
        end do
      end do
    end do
    totmass=sum(prof)*dx*dy*dz
    ampl=msun*progm/(cmu*totmass)
    if (ampl .lt. 1.0) then
      prerho0=rho(li,mi,ni)
      eold=e
      dold=d
      scr=rho-ampl*prof                  ! subtract the stellar mass
      e=e*(scr/rho)                      ! conserve local temperature
      d=d*(scr/rho)                      ! conserve local metallicity
      msub=sum(rho-scr)*dx*dy*dz         ! msub sholud be equal to cmass
      cmass=msun*progm/cmu               ! total amount of mass
      cenergy=sum(eold-e)*dx*dy*dz       ! total amount of energy
      cmetal=sum(dold-d)*dx*dy*dz        ! total amount of metals
      rho=scr                            ! finally, make it real
      postrho0=rho(li,mi,ni)
      exit
    else if (i .lt. maxtry) then
      print *, 'Mass substraction failed, trying a new formation site!'
      call snpose(rho,px,py,pz,ee,li,mi,ni,elimit,rholim,convlim)
    end if
  end do
  if (cmass .eq. 0.0) then
    msub=0.0
    ampl=0.0
    prerho0=rho(li,mi,ni)
    postrho0=prerho0
    print *,'Warning: Mass substraction failed!'
  end if
  

  if (progm .ge. 10.) then                       ! estimating no. ionizing photons Myr-1 (x10^49)
    logph=0.65*alog(progm-9.25)+12.25            ! note: logph_real = logph + 49   
    logph=logph-0.188*alog(progm+29.905)+0.677   ! subtraction of He-ionizing photons
  else                                           ! bad fit below M = 10 M_o
    logph=12.        
  end if

  ! If using  Meynet table (don't calculate log(M) now!!)
  
  allocate(a(Meynetlifelen))
  a=0.
  where(Meynetlifetbl(:,1).le.progm) a=Meynetlifetbl(:,1)
  do itau=1,Meynetlifelen
     if (Meynetlifetbl(itau,1).eq.maxval(a)) exit
  end do
  deallocate(a)
  
  a1 = (Meynetlifetbl(itau+1,2)-Meynetlifetbl(itau,2))/ &
       (Meynetlifetbl(itau+1,1)-Meynetlifetbl(itau,1))
  a0 = Meynetlifetbl(itau,2)-a1*Meynetlifetbl(itau,1)

  lifetime = a1*progm+a0                 ! life time of star in Myrs
  expltime = t+lifetime                  ! time at explosion (Myrs)


  a1M = (Meynetmasstbl(itau+1,2)-Meynetmasstbl(itau,2))/ &
       (Meynetmasstbl(itau+1,1)-Meynetmasstbl(itau,1))
  a0M = Meynetmasstbl(itau,2)-a1M*Meynetmasstbl(itau,1)
  
  totallostM = a1M*progm+a0M                  ! expeled mass during all its life (solar masses)
  remainingM = progm - totallostM             ! expeled mass in the SN explosion (solar masses) 
 

! Calculate duration of first phase

  
  a1 = (MeynetlifePh1tbl(itau+1,2)-MeynetlifePh1tbl(itau,2))/ &
       (MeynetlifePh1tbl(itau+1,1)-MeynetlifePh1tbl(itau,1))
  a0 = MeynetlifePh1tbl(itau,2)-a1*MeynetlifePh1tbl(itau,1)

  Ph1lifetime = a1*progm+a0                 ! Phase 1 of the star in Myrs
  


! Calculate total lost mass during the life of the star. 


  a1M = (MeynetmassPh1tbl(itau+1,2)-MeynetmassPh1tbl(itau,2))/ &
       (MeynetmassPh1tbl(itau+1,1)-MeynetmassPh1tbl(itau,1))
  a0M = MeynetmassPh1tbl(itau,2)-a1M*MeynetmassPh1tbl(itau,1)
  
  Ph1totallostM = a1M*progm+a0M                  ! expeled mass during all its life (solar masses)

  
  
  
  
  
  
  ! calculate final cell position
  lf = modulo(nint(uxc(li,mi,ni)*lifetime/dx) + li,mx)
  mf = modulo(nint(uyc(li,mi,ni)*lifetime/dy) + mi,my)
  nf = modulo(nint(uzc(li,mi,ni)*lifetime/dz) + ni,mz)
  if (lf .eq. 0) lf = mx
  if (mf .eq. 0) mf = my
  if (nf .eq. 0) nf = mz

  ! place star in hms array
  allocate(dum(mhms+1,19))
  allocate(dumpos(mhms+1,3))
  if (mhms .gt. 0) then
    dum(1:mhms,:)=hms
    dumpos(1:mhms,:)=hmspos
    deallocate(hms,hmspos)
  end if 

  do i=mhms,1,-1
    if (expltime .ge. dum(i,1)) exit
    do j=1,19                         ! note: no. of entries in hms array (allocate)
      dum(i+1,j)=dum(i,j)
    end do
    do j=1,3
      dumpos(i+1,j)=dumpos(i,j)
    end do
  end do

  dum(i+1,1) = expltime
  dum(i+1,2) = progm
  dum(i+1,3) = cmass
  dum(i+1,4) = cenergy
  dum(i+1,5) = cmetal
  dum(i+1,6) = logph
  dum(i+1,7) = t-dtsn
  dum(i+1,8) = uxc(li,mi,ni)
  dum(i+1,9) = uyc(li,mi,ni)
  dum(i+1,10)= uzc(li,mi,ni)
  dum(i+1,11)= (3.99e-3*exp(ln10*logph/3.)*rhoav**(-0.6666667))**2.
  dum(i+1,12) = totallostM
  dum(i+1,13) = itau
  dum(i+1,14) = lifetime
  dum(i+1,15) = progm      !Here we'll place the mass in each time step
  dum(i+1,16) = Ph1lifetime
  dum(i+1,17) = Ph1totallostM
  dum(i+1,18) = 0.         !Here we'll place Vinf in each time step
  dum(i+1,19) = 0.         !Total Energy Input due to stellar wind

  dumpos(i+1,1) = lf  !Para TEST ponemos inicial...CAMBIARLO LUEGO
  dumpos(i+1,2) = mf  !el bueno es el de arriba!
  dumpos(i+1,3) = nf  !El de abajo (dejar las estrellas fijas) es para test
!  dumpos(i+1,1) = li
!  dumpos(i+1,2) = mi
!  dumpos(i+1,3) = ni 
 
 
  allocate(hms(mhms+1,19))
  allocate(hmspos(mhms+1,3))
  hms=dum
  hmspos=dumpos
  deallocate(dum,dumpos)
  mhms=mhms+1            ! a high-mass star has formed 
  totalstellarmass=totalstellarmass+progm

  if (t.gt.0)  call write_hms(li,mi,ni,rho,progm,lifetime,expltime, &
               totalstellarmass,totallostM,Ph1lifetime,Ph1totallostM)


  print *, 'Mass (M_o):.............................', progm
  print *, 'Total expeled mass (M_o):...............', totallostM
  print *, 'Life time (Myrs):.......................', lifetime
  print *, 'Time at explosion (Myrs):...............', expltime
  print *, 'Initial position:.......................', li,mi,ni 
  print *, 'Final position:.........................', lf,mf,nf
  print *, 'Velocity (pc/Myr):......................', uxc(li,mi,ni),uyc(li,mi,ni),uzc(li,mi,ni)
  print *, 'Pre-formation central density (cdu):....', prerho0 
  print *, 'Subtracted mass (cmu):..................', msub
  print *, 'Post-formation central density (cdu):...', postrho0 
  print *, 'Weighting function amplitude:...........', ampl 
  print *, 'No. existing high-mass stars:...........', mhms 
 
  call oflush
   
END SUBROUTINE



!************************************************************
subroutine lmsf (rho,px,py,pz,ee,d,f,elimit,rholim,convlim)

  USE params
  USE stagger
  USE units
  implicit none

  integer i,j,ip,jp,kp,nos,li,ui
  real, dimension(mx,my,mz) :: rho, px, py, pz, ee, d
  real, allocatable, dimension(:,:,:) :: ux,uy,uz,lnr,conv
  real dlms(7), dpos(4), rotime
  real f,elimit,rholim,convlim,rest,ran1s,rnd,ftemp,tlim,elim,rlim,clim,expf
  real :: tdp, rdp, ddp

  nos=floor(f)
  rest=f-real(nos)
  rnd=ran1s(snseed)
  if (rnd .le. rest) nos=nos+1

  if (.not. do_psft) then

    do i=1,nos
      call snpose(rho,px,py,pz,ee,ip,jp,kp,elimit,rholim,convlim)
      if (do_ionization) then
        ftemp=temp(ip,jp,kp)
      else
        ftemp=utemp*ee(ip,jp,kp)
      end if
      lmsid=lmsid+1
      write(*,*) 'Low-mass star: id time density metals temperature x y z'
      write(*,*) lmsid,t,rho(ip,jp,kp),d(ip,jp,kp),ftemp,ip,jp,kp
      tdp=t
      rdp=rho(ip,jp,kp)
      ddp=d(ip,jp,kp)
      call write_lms(lmsid,ip,jp,kp,tdp,rdp,ddp,.false.)
    end do

  else

    if (mlms.eq.0 .and. nos.eq.0) return
    
    allocate(ux(mx,my,mz),  &
             uy(mx,my,mz),  &
             uz(mx,my,mz),  &
             lnr(mx,my,mz), &
             conv(mx,my,mz))
    lnr = alog(rho)
    ux = px/exp(xdn(lnr))
    uy = py/exp(ydn(lnr))
    uz = pz/exp(zdn(lnr))
    if (do_2nddiv) then
      conv = -(ddxup1(ux) + ddyup1(uy) + ddzup1(uz))/sqrt(rho)
    else
      conv = -(ddxup (ux) + ddyup (uy) + ddzup (uz))/sqrt(rho) 
    end if
    deallocate(lnr)
  
    if (mlms .gt. 0) then               ! is there any existing proto-stars?
      rotime=5.*psftime
      do i=mlms,1,-1
        ip = modulo(nint(lms(i,3)*(t-lms(i,2))/dx) + lmspos(i,1),mx)
        jp = modulo(nint(lms(i,4)*(t-lms(i,2))/dy) + lmspos(i,2),my)
        kp = modulo(nint(lms(i,5)*(t-lms(i,2))/dz) + lmspos(i,3),mz)
        if (ip .eq. 0) ip = mx
        if (jp .eq. 0) jp = my
        if (kp .eq. 0) kp = mz
        expf=exp(lms(i,1)/psftime)      ! assume exponential time evolution for the sf-conditions 
        rlim=rholim/expf
        clim=expf*convlim
        if (do_ionization) then  
          tlim=expf*Tlimit
          if (temp(ip,jp,kp).lt.tlim.and.rho(ip,jp,kp).ge.rlim.and.conv(ip,jp,kp).gt.clim) then 
            lms(i,1)=lms(i,1)+2*dtsn     
          else
            lms(i,1)=-1.
            dlms=lms(i,:)
            lms(i:mlms-1,:)=lms(i+1:mlms,:)
            lms(mlms,:)=dlms
            dpos=lmspos(i,:)
            lmspos(i:mlms-1,:)=lmspos(i+1:mlms,:)
            lmspos(mlms,:)=dpos 
          end if
        else
          elim=expf*elimit
          if (ee(ip,jp,kp).lt.elim.and.rho(ip,jp,kp).ge.rlim.and.conv(ip,jp,kp).gt.clim) then
            lms(i,1)=lms(i,1)+2*dtsn     
          else
            lms(i,1)=-1.
            dlms=lms(i,:)
            lms(i:mlms-1,:)=lms(i+1:mlms,:)
            lms(mlms,:)=dlms
            dpos=lmspos(i,:)
            lmspos(i:mlms-1,:)=lmspos(i+1:mlms,:)
            lmspos(mlms,:)=dpos
          end if
        end if
      end do            
      
      do i=mlms,1,-1                    ! stars that failed to form
        if (lms(i,1) .gt. 0.) exit
        lmsid=lmsid+1
        ip=lmspos(i,1)
        jp=lmspos(i,2)
        kp=lmspos(i,3)                       
        call write_lms(lmsid,ip,jp,kp,lms(i,2),lms(i,6),lms(i,7),.false.)
      end do
  
      ui=i
   
      do i=1,mlms                       ! stars that succeded to form
        if (lms(i,1) .le. rotime) exit
        lmsid=lmsid+1
        ip=lmspos(i,1)
        jp=lmspos(i,2)
        kp=lmspos(i,3)
        call write_lms(lmsid,ip,jp,kp,lms(i,2),lms(i,6),lms(i,7),.true.)
      end do      
     
      li=i
      mlms=ui-li+1                      ! current number of existing proto-stars
       
      if (mlms .gt. 0) then
        allocate(dum(mlms,7),dumpos(mlms,3))
        dum=lms(li:ui,:)
        dumpos=lmspos(li:ui,:)
        deallocate(lms,lmspos)
        allocate(lms(mlms,7),lmspos(mlms,3)) 
        lms=dum
        lmspos=dumpos
        deallocate(dum,dumpos)
      else
        deallocate(lms,lmspos)
      end if    
    end if
    deallocate(conv)  
   
    if (nos .gt. 0) then                ! is there any newly formed proto-stars?
      allocate(dum(mlms+nos,7),dumpos(mlms+nos,3))
      if (mlms .gt. 0) then
        dum(1:mlms,:)=lms
        dumpos(1:mlms,:)=lmspos
        deallocate(lms,lmspos)
      end if
          
      do i=mlms,1,-1                    ! place newly formed proto-stars in lms array
        if (dum(i,1) .ge. dtsn) exit
        dum(i+nos,:)=dum(i,:)
        dumpos(i+nos,:)=dumpos(i,:)
      end do
    
      ui=i
      ux=xup(ux)
      uy=yup(uy)
      uz=zup(uz)
    
      do i=1,nos
        call snpose(rho,px,py,pz,ee,ip,jp,kp,elimit,rholim,convlim)
        dum(i+ui,1)=dtsn
        dum(i+ui,2)=t
        dum(i+ui,3)=ux(ip,jp,kp)
        dum(i+ui,4)=uy(ip,jp,kp)
        dum(i+ui,5)=uz(ip,jp,kp)
        dum(i+ui,6)=rho(ip,jp,kp)
        dum(i+ui,7)=d(ip,jp,kp)
        dumpos(i+ui,1)=ip
        dumpos(i+ui,2)=jp
        dumpos(i+ui,3)=kp
        if (do_ionization) then
          ftemp=temp(ip,jp,kp)
        else
          ftemp=utemp*ee(ip,jp,kp)
        end if
        plmsid=plmsid+1
        write(*,*) 'Low-mass star: id time density metals temperature x y z'
        write(*,*) plmsid,t,rho(ip,jp,kp),d(ip,jp,kp),ftemp,ip,jp,kp
      end do
    
      allocate(lms(mlms+nos,7),lmspos(mlms+nos,3))
      lms=dum
      lmspos=dumpos
      deallocate(dum,dumpos)
       
      mlms = mlms+nos                   ! nos proto-stars have formed 
    end if
    deallocate(ux,uy,uz) 
  end if  

end subroutine  
    


!************************************************************
  subroutine snpose (rho,px,py,pz,ee,ldist,mdist,ndist,elimit,rholim,convlim)

!  Find a SN position, with probability prop. to density, in
!  the subvolume with ee < elimit
!
!  980607/aake: Coded
!  981023/aake: Coded

  USE params
  USE stagger
  USE units
  implicit none

  integer ldist,mdist,ndist
  real elimit, rholim, convlim, ran1s

  real, dimension(mx,my,mz):: rho, px, py, pz, ee
  real, allocatable, dimension(:,:,:) :: lnr, ux, uy, uz, conv
  real,dimension(mx*my*mz) :: scr1,scr2
  real, allocatable, dimension(:) :: scr3
  integer i, l, m, n,j
  integer ii(1),ip,ml
  real rnd,a 

  integer nk,h,k
  integer*2, external:: icompare

  allocate(ux(mx,my,mz),  &
           uy(mx,my,mz),  &
           uz(mx,my,mz),  &
           lnr(mx,my,mz), &
           conv(mx,my,mz))
  lnr = alog(rho)
  ux = px/exp(xdn(lnr))
  uy = py/exp(ydn(lnr))
  uz = pz/exp(zdn(lnr))
  if (do_2nddiv) then
    conv = -(ddxup1(ux) + ddyup1(uy) + ddzup1(uz))/sqrt(rho) 
  else
    conv = -(ddxup (ux) + ddyup (uy) + ddzup (uz))/sqrt(rho)
  end if

! make selection 

  k=1
  if (do_ionization) then
   do n=1,mz
    do m=1,my
      do l=1,mx
        if (temp(l,m,n).lt.Tlimit.and.rho(l,m,n).gt.rholim.and.conv(l,m,n).gt.convlim) then
          scr1(k)=rho(l,m,n)
          k=k+1
        endif
      enddo
    enddo
   enddo
  else
   do n=1,mz
    do m=1,my
      do l=1,mx
!        if (ee(l,m,n).lt.elimit.and.rho(l,m,n).gt.rholim.and.conv(l,m,n).gt.convlim) then
        if (ee(l,m,n).lt.elimit.and.rho(l,m,n).gt.rholim) then
          scr1(k)=rho(l,m,n)
          k=k+1
        endif
      enddo
    enddo
   enddo
  end if

  nk=k-1

  allocate(scr3(nk))
  do j=1,nk
     scr3(j)=scr1(j)
  enddo
   
  if (nk < 1) then
    print *,'WARNING, nk=0 in stars.f90'
    deallocate (lnr,ux,uy,uz,conv)
    ldist=1
    mdist=1
    ndist=1
    return
  else if (idbg > 0) then
 !   print *,'sorting ',nk
  end if


! sort the density
!print *, 'nk  ',nk
!  call mysort(scr3,nk)
!  call mysort(scr1,nk)

!call qsort (scr3,nk,4,icompare)

! New sorting. The program used to crash (surprisingly) here when the mass substraction 
! failed in subroutine tracksn and snpose was called from there. 

  do ip=1,nk
    ii=minloc(scr3(ip:nk))
    ml=ii(1)
    a=scr3(ip)
    scr3(ip)=scr3(ml+ip-1)
    scr3(ml+ip-1)=a
  enddo 
!print *,scr3


!print *, 'hola 113'

! integrate the mass

  scr2(1)=0.
  do k=2,nk
!      scr2(k)=scr2(k-1)+scr1(k-1)
       scr2(k)=scr2(k-1)+scr3(k-1)
  enddo
!print *, 'hola 114'

! Choose a density with probability proportional to density

  rnd=ran1s(snseed)
  rnd=ran1s(snseed)         ! roll a few extra times, to avoid
  rnd=ran1s(snseed)         ! peculiar first value.

  call binsearch (scr2,nk,rnd*scr2(nk),i)
  print *,'  #Fraction..........:',rnd,scr2(i)/scr2(nk)

! find the point (this is crude :-) and now also ugly!!!(bvg) 
! and it was incorrect several times over :-( / aake:
!  1. ee and scr1 must refer to the same point => sort after selection!
!  2. nk=k is wrong, since k=k+1 after last value computed
!  3. cannot just use double precision; exps() and qsort() expect real
!  4. finding the point was done incorrectly; scr1 was wiped out

  if (do_ionization) then
   do n=1,mz
    do m=1,my
      do l=1,mx
!        if (temp(l,m,n).lt.Tlimit.and.conv(l,m,n).gt.convlim .and. rho(l,m,n).eq.scr1(i)) then
!         if (temp(l,m,n).lt.Tlimit .and. rho(l,m,n).eq.scr1(i)) then
          if (temp(l,m,n).lt.Tlimit .and. rho(l,m,n).eq.scr3(i)) then
          ldist=l
          mdist=m
          ndist=n
          deallocate(lnr,ux,uy,uz,conv)
          deallocate(scr3)
          return
        endif
      enddo
    enddo
   enddo
  else
   do n=1,mz
    do m=1,my
      do l=1,mx
!        if (ee(l,m,n).lt.elimit.and.conv(l,m,n).gt.convlim .and. rho(l,m,n).eq.scr1(i)) then
!        if (ee(l,m,n).lt.elimit .and. rho(l,m,n).eq.scr1(i)) then
         if (ee(l,m,n).lt.elimit .and. rho(l,m,n).eq.scr3(i)) then

          ldist=l
          mdist=m
          ndist=n
          deallocate(lnr,ux,uy,uz,conv)
          deallocate(scr3)
          return
        endif
      enddo
    enddo
   enddo
  end if

  print *,'snpose: should never get here!'
  stop

  end subroutine

!**********************************************************************
  subroutine binsearch (f,n,f0,i)

!  Binary search in monotonically increasing f(1:n).  No checks.
!  aake/980923

  USE PARAMS
  USE units
  implicit none
  
  integer :: i1, i2, n, i
  real f(n), f0

  i1=1
  i2=n
100   continue
    i=(i1+i2)/2
    if (f(i) .lt. f0) then
      i1=i
    else
      i2=i
    endif
  if (i1 .lt. i2-1) goto 100
  i=i1

  end subroutine

!************************************************************************
  subroutine SNexpl (rho,e,d,sntype,progm,ldist,mdist,ndist,rampl,eampl,dampl)

  USE params
  USE units
  implicit none

  integer sntype, ldist, mdist, ndist, ll, mm, nn, l, m, n, i
  real, dimension(mx,my,mz) :: rho, lnrho, e, d, scr1, prof_expl, prof_shell, prof_met
  real, allocatable, dimension(:,:,:) :: prof_asym
  real esn1, dsn1, dpsn, arg, arg1, arg2, argd, ex, exd, ex1, ex2, prof1, prof2, profd
  real v_expl, v_shell, v_met, v_asym, e_0, d_0, cmass, SNe, dwidth, dwidth1, dwidth2
  real totm, totm2, totm3, tote, tote2, cee, totd, totd2, cdd, cutoff, dwidthd, progm
  real xx, yy, zz, snlnr0, snlnr1, snrho0, snrho1, eemax, radius, omax, dcen
  real x(mx), y(my), z(mz), x0, y0, z0, theta, phi, omega_eject, ran1s
  real, optional :: rampl, eampl, dampl

  if (idbg.gt.5) print *,'pos=',ldist,mdist,ndist

  do i=1,mx
    x(i) = (i-mx/2)*dx
  end do
  do i=1,my
    y(i) = (i-my/2)*dy
  end do
  do i=1,mz
    z(i) = (i-mz/2)*dz
  end do

  if (do_asym) then
    allocate(prof_asym(mx,my,mz))
    omax  = 1.25*pi               ! if omax .eq. 0. no cavity in central cell
    theta = pi*ran1s(snseed)
    phi   = 2*pi*ran1s(snseed)
    theta = theta+theta_eject 
    x0    = sin(theta)*cos(phi)
    y0    = sin(theta)*sin(phi)
    z0    = cos(theta)
  endif  

  dwidth = wsn*dx       ! generic width
  dwidth1= 1.5*dwidth   ! inner shell width 
  dwidth2= 2.0*dwidth   ! outer shell width
  cutoff = 1.0E-03      ! profile cutoff in order to avoid fictious ultra metal-poor stars
  dpsn   = 1.25*psn     ! decreasing the base line for the metallicity distr. (true when cutoff < 0.5) 
  dwidthd= (wsn-0.5)*dx ! further decreasing the base line 
  do n=1,mz
    zz=z(n)-z(mz/2)
    nn=ndist+(n-mz/2)
    if (nn .gt. mz) then
      nn=nn-mz
    else if (nn .le. 0) then
      nn=nn+mz
    endif
    do m=1,my
      yy=y(m)-y(my/2)
      mm=mdist+(m-my/2)
      if (mm .gt. my) then
        mm=mm-my
      else if (mm .le. 0) then
        mm=mm+my
      endif
      do l=1,mx
        xx=x(l)-x(mx/2)
        ll=ldist+(l-mx/2)
        if (ll .gt. mx) then
          ll=ll-mx
        else if (ll .le. 0) then
          ll=ll+mx
        endif
        radius=sqrt(xx**2+yy**2+zz**2)
        arg=radius/dwidth                           ! normalized radius
        arg1=arg*(dwidth/dwidth1)
        arg2=arg*(dwidth/dwidth2)
        argd=arg*(dwidth/dwidthd)
        ex=exp(max(-35.,min(35.,psn*(arg-1.))))     ! psn=steepness
        exd=exp(max(-35.,min(35.,dpsn*(argd-1.))))
        ex1=exp(max(-35.,min(35.,psn*(arg1-1.))))
        ex2=exp(max(-35.,min(35.,psn*(arg2-1.))))
        prof1=1./(1.+ex1**2)
        prof2=1./(1.+ex2**2)
        profd=1./(1.+exd**2)
        prof_expl(ll,mm,nn)=1./(1.+ex**2)           ! tanh()-like profile in log e 
        prof_shell(ll,mm,nn)=prof2-prof1
        if (profd .lt. cutoff) then                  
          prof_met(ll,mm,nn)=0.
          if (do_asym) prof_asym(ll,mm,nn)=0.
        else
          prof_met(ll,mm,nn)=profd 
          if (do_asym) then
            if (radius .gt. 0.) then
              omega_eject=acos((xx*x0+yy*y0+zz*z0)/radius)  ! ang. between point - expl. center and ej. vector
            else 
              omega_eject=omax                              ! central cell is hollowed if omax .ne. 0. 
            end if
            prof_asym(ll,mm,nn)=profd*exp(-(omega_eject/width_eject)**2)
          end if
        end if        
      end do
    end do
  end do
  v_met=sum(prof_met)*dx*dy*dz                      ! normalization factor
  v_expl=sum(prof_expl)*dx*dy*dz                    
  v_shell=sum(prof_shell)*dx*dy*dz
  if (do_asym) v_asym=sum(prof_asym)*dx*dy*dz
                                                    ! Normalization factors
  SNe=1.d51/ceu                                     ! standard supernova energy
  e_0=(esn*SNe)/v_expl                              ! esn = expl. energy in foe

  scr1=rho                        ! save original rho distr. in scr1
  snrho0=rho(ldist,mdist,ndist)
  eemax=e_0/snrho0
  if (do_ionization) then
    print*,'  #Pre-expl temp.............:',temp(ldist,mdist,ndist)
  else
    print*,'  #Pre-expl temp.............:',utemp*e(ldist,mdist,ndist)/snrho0
  end if
  print*,'  #Pre-expl density..........:',snrho0
  if (do_pscalar) then
    print*,'  #Pre-expl metallicity......:',d(ldist,mdist,ndist)/snrho0
  end if

!Fix: if do_ionization = .true. re-calculate temperature for printed output!!!

  if (do_delayexpl) then
    rampl = rampl/v_met
    eampl = eampl/v_met
    dampl = dampl/v_met
    rho = rho+rampl*prof_met      ! add back the stellar mass 
    e = e+eampl*prof_met          ! add back the energy
    d = d+dampl*prof_met          ! add back the metals
    scr1=rho                      ! save original (new) rho distr. in scr1
    snrho0=rho(ldist,mdist,ndist)
    eemax=e_0/snrho0     
    if (do_ionization) then
      print*,'  #Pre-expl SN temp..........:',temp(ldist,mdist,ndist)
    else
      print*,'  #Pre-expl SN temp..........:',utemp*e(ldist,mdist,ndist)/snrho0
    end if
    print*,'  #Pre-expl SN density.......:',snrho0
    if (do_pscalar) then
      print*,'  #Pre-expl SN metallicity...:',d(ldist,mdist,ndist)/snrho0
    end if
  end if

  ! If (and only if) the central temperature would be too small, make a cavity
  if (eemax.lt.1.e7/utemp) then   ! central temperature test
    eemax=1.e7/utemp
    snrho1=e_0/eemax
    snlnr1=alog(snrho1)
    snlnr0=alog(snrho0)
    totm=sum(rho)*dx*dy*dz
    tote=sum(e)*dx*dy*dz
    totd=sum(d)*dx*dy*dz
    lnrho=alog(rho)
    lnrho=lnrho+(snlnr1-snlnr0)*prof_expl
    rho=exp(lnrho)
    e=e*(rho/scr1)               ! conserve local temperature
    d=d*(rho/scr1)               ! conserve local metallicity
    totm2=sum(rho)*dx*dy*dz      ! compute new total mass
    tote2=sum(e)*dx*dy*dz        ! copute new total energy 
    totd2=sum(d)*dx*dy*dz        ! compute new total abundance
    cmass=(totm-totm2)/v_shell
    cee=(tote-tote2)/v_shell
    cdd=(totd-totd2)/v_shell
    rho=rho+cmass*prof_shell     ! Add the missing cavity mass back, in a shell.
    e=e+cee*prof_shell           ! Add the missing cavity energy back
    d=d+cdd*prof_shell           ! Add the missing cavity abundance back 
    totm3=cmass*v_shell

    if (idbg.gt.5) then
      print *,'totm:',totm-totm2,totm3
      print *,'tote:',tote,sum(e)*dx*dy*dz
      print *,'totd:',totd,sum(d)*dx*dy*dz
    end if

    esn1=0.                      ! ngrs: deal with e now that the rho profile is fixed
    e=e+e_0*prof_expl            ! finally add SN energy
    esn1=e_0*v_expl

    if (idbg.gt.5) then
      print *,'tote:',tote,sum(e)*dx*dy*dz
    end if

  else                           ! central temperature test    

    totm3=0.                     ! in case no cavity is needed
    e=e+e_0*prof_expl
    esn1=e_0*v_expl   

  endif                          ! central temperature test              

  if (do_pscalar) then               ! Finally, add also the newly synthesized metals
    if (do_asym) then                ! prof_met => symetric ejection; prof_asym => asymetric ejection 
      call yield(d_0,v_asym,progm) 
      dcen=d(ldist,mdist,ndist)+d_0  ! dcen is set to be d+d_0 both for sym. and asym. ej.
      d=d+d_0*prof_asym    
      dsn1=d_0*v_asym
      deallocate(prof_asym)
    else
      call yield(d_0,v_met,progm)
      dcen=d(ldist,mdist,ndist)+d_0  ! dcen is set to be d+d_0 both for sym. and asym. ej.
      d=d+d_0*prof_met    
      dsn1=d_0*v_met
    end if    
    if (idbg.gt.5) then
      print *,'totd:',totd,sum(d)*dx*dy*dz-totd,dsn1
    end if
  end if

  if (ndist.le.0.or.ndist.gt.mz) then
    print *,'ERROR: ndist=',ndist
    stop
  endif
  
  xx=x(ldist)                    !  Center of explosion, for printout
  yy=y(mdist)
  zz=z(ndist)
  
  print*,'  #SN-type...................:',sntype 
  if (do_pscalar) then
    print*,'  #SN mass (M_o).............:',progm
  end if
  print*,'  #Indices...................:',ldist,mdist,ndist
  print*,'  #Position..................:',xx,yy,zz
  print*,'  #Energy (foe)..............:',esn1/SNe
  if (do_pscalar) then
    print*,'  #Metals (M_o)..............:',dsn1*(cmu/msun)
  end if
  print*,'  #Ejection vector...........:',x0,y0,z0
  print*,'  #Shell mass (M_o)..........:',totm3*(cmu/msun)
  print*,'  #Time (Myrs)...............:',t
  print*,'  #Amplitude (ceu clu-3).....:',e_0
  if (do_ionization) then
    print*,'  #Post-expl temp (MK)........:',1.e-6*temp(ldist,mdist,ndist)
  else  
    print*,'  #Post-expl temp (MK)........:',1.e-6*utemp*e(ldist,mdist,ndist)/rho(ldist,mdist,ndist)
  end if
  print*,'  #Post-expl density.........:',rho(ldist,mdist,ndist)
  if (do_pscalar) then
    print*,'  #Post-expl metallicity.....:',dcen/rho(ldist,mdist,ndist) 
  end if
  print '(a,i2,1pe13.5,3i4,2(1pe10.2))','   #SN-Log.:',sntype,t,ldist,mdist,ndist,rho(ldist,mdist,ndist),totm3*(cmu/msun)
  
END SUBROUTINE

!************************************************************************
SUBROUTINE yield (dnorm,volume,m)

! Given a certain supernova mass the yield is interpolated from a yield table.

USE params
USE units
implicit none

integer :: i
real :: dnorm, y_X, a0, a1, m, volume
  
  if (m.lt.ytbl(1,1)) then
    y_X=ytbl(1,2)
  else if (m.ge.ytbl(1,1).and.m.lt.ytbl(ylen,1)) then
    yloop: do i=2,ylen
      if (m.ge.ytbl(i-1,1).and.m.lt.ytbl(i,1)) then
        a1=(ytbl(i,2)-ytbl(i-1,2))/(ytbl(i,1)-ytbl(i-1,1))
        a0=ytbl(i,2)-a1*ytbl(i,1)
        y_X=a1*m+a0
        exit yloop
      end if
    end do yloop
  else
    y_X=ytbl(ylen,2)
  end if
  
  dnorm=(y_X*msun/cmu)/volume          ! y_X = ejected yield in solar masses
!  dnorm=(dsn*msun/cmu)/volume          ! dsn = ejected yield in solar masses  
 
END SUBROUTINE

!***********************************************************************
SUBROUTINE write_lms (id,i,j,k,tf,rf,df,flag)
!-----------------------------------------------------------------------
!Write id, density, metal content, time, and position for the cell where 
!a low-mass star is born.
!-----------------------------------------------------------------------

  USE params
  USE units
  implicit none
  
  integer i,j,k,id
  logical flag
  real :: mass2num,tf,rf,df
  
  mass2num=exp(lnfehsun) 
   
  open(2,file=file(1:index(file,'.'))//'lms', position='append', status='unknown')
  if (flag) then
    write(2,110) id,tf,rf,df,mass2num,i,j,k
    110 format(I5,ES10.3,ES10.3,ES11.3,F8.3,I4,I4,I4)
  else
    write(2,120) id,tf,rf,df,mass2num,i,j,k
    120 format(I5,ES10.3,ES10.3,ES11.3,F8.3,I4,I4,I4)
  end if    
  close(2)
END SUBROUTINE

END MODULE
