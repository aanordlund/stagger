! $Id: gravpot_disk_bs.f90,v 1.8 2012/12/31 16:19:11 aake Exp $
!***********************************************************************
SUBROUTINE gravpot (rho, phi)

!  Solve for the gravitational potential phi, given the density rho,
!  from the equation Laplace(phi) = grav*rho.

  use params
  use forcing
  use stagger
  implicit none

  real, dimension(mx,my,mz):: rho, phi
!hpf$ align with TheCube:: rho, phi
  real, allocatable, dimension(:,:,:):: k2
!hpf$ align with TheCube:: k2

  real zscr(mz)
  real k2norm, rhoa, m_gas, m_tot, m_xz, x, y, z, r, f, cpu, fdtime
  integer ix, iy, iz, nperif, ix1, iz1

  character(len=mid):: id = "$Id: gravpot_disk_bs.f90,v 1.8 2012/12/31 16:19:11 aake Exp $"
!-----------------------------------------------------------------------

  if (id .ne. ' ') then
    print *,id                                    ! identify
    allocate (sigm(mx,mz))                        ! surface density
    allocate (rh1(mx,my,mz))                      ! boundary condition 
    id = ' '
  end if

  if (.not. isolated) then                        ! periodic case
    call fft3df (rho, phi, mx, my, mz)            ! forward FFT
  else                                            ! isolated case

!$omp parallel do private(iz)
    do iz=1,mz
      zscr(iz) = dx*dy*sum(rho(1:mx,1:my,iz))     ! total mass in xy-planes
    end do
    m_gas = dz*sum(zscr(1:mz))                    ! total gas mass
    if (idbg>0) print *,'m_gas',m_gas

!$omp parallel do private(ix,iz)
    do iz=1,mz
      do ix=1,mx
        rh1(ix,1:my-1,iz) = rho(ix,1:my-1,iz)
        sigm(ix,iz) = sum(rho(ix,1:my-1,iz))
      end do
    end do

!-----------------------------------------------------------------------
!  Compute boundary mass for the gravitational potential calculation.
!  Normalization: We add up the contributions to the gravitational force
!  at the boundary from the individual patches of mass sigm(ix,iz)*dx*dz.
!  The jump in the force is twice that, and should correspond to the
!  difference in grad(Phi), so the integral over dy of 4\pi\rho.  So,
!  dy*rho there is (1/2\pi)*(the summed force).
!-----------------------------------------------------------------------
    if (mod(it,100).eq.0 .and. isubstep.eq.1) then  ! every 100 steps
      cpu = fdtime()
      f = dx*dz/(2.*pi)
!$omp parallel do private(ix,ix1,iz,iz1,x,y,z,r)
      do iz=1,mz
        y = abs(ym(my))
        z = zm(iz)
        do ix=1,mx
          x = xm(ix)
          rh1 (ix,my,iz) = 0.
          do iz1=1,mz
          do ix1=1,mx
            r = sqrt((x-xm(ix1))**2+y**2+(z-zm(iz1))**2)
            rh1(ix,my,iz) = rh1(ix,my,iz) - f*sigm(ix1,iz1)*y/(r*r*r)
          end do
          end do
        end do
        zscr(iz) = dx*dy*sum(rh1(1:mx,1:my,iz))   ! total mass in xy-planes
      end do
      print *,'bdry time:',fdtime()
    end if

!$omp parallel do private(iz)
    do iz=1,mz
      zscr(iz) = dx*dy*sum(rh1(1:mx,1:my,iz))     ! total mass in xy-planes
    end do
    m_tot = dz*sum(zscr)
    if (idbg>0) print *,'m_tot',m_tot

    !rh1(:,:,mz) = -0.5*m_tot/(mx*my*dx*dy*dz)
    !rh1(mx,:,:) = -0.5*m_tot/(mz*my*dx*dy*dz)

    do iy=1,my-1
      do ix=1,mx-1
        r = sqrt(xm(ix)**2+ym(iy)**2+zm(mz)**2)
        rh1(ix,iy,mz) = -(m_gas/(2.*pi)/dy*zm(mz))/r**3
      end do
    end do

    do iz=1,mz-1
      do iy=1,my-1
        r = sqrt(xm(mx)**2+ym(iy)**2+zm(iz)**2)
        rh1(mx,iy,iz) = -(m_gas/(2.*pi)/dy*xm(mx))/r**3
      end do
    end do

    if (idbg>0) then
!$omp parallel do private(iz)
      do iz=1,mz
        zscr(iz) = dx*dy*sum(rh1(1:mx,1:my,iz))   ! total mass in xy-planes
      end do
      m_tot = dz*sum(zscr)
      m_xz = dx*dy*dz*sum(rh1(1:mx-1,1:my-1,mz))
      print *,'m_tot',m_tot,m_xz
      rh1(1:mx-1,1:my-1,mz) = rh1(1:mx-1,1:my-1,mz)*(1.-0.5*m_tot/m_xz)
      rh1(mx,1:my-1,1:mz-1) = rh1(mx,1:my-1,1:mz-1)*(1.-0.5*m_tot/m_xz)
    end if

    call dump(rh1,'phi.dat',1)

    call fft3df (rh1, phi, mx, my, mz)            ! forward FFT
  end if

  allocate (k2(mx,my,mz))

  call fft3d_k2 (k2, dx, dy, dz, mx, my, mz)      ! compute k**2
  k2norm = 2.*(soft*(sx/mx)/pi)**2                ! normalization factor
!$omp parallel do private(ix,iy,iz)
  do iz=1,mz
  do iy=1,my
  do ix=1,mx
    phi(ix,iy,iz) = -phi(ix,iy,iz) &
      *grav*4.*pi/(k2(ix,iy,iz)+1e-10)*exp(-k2norm*k2(ix,iy,iz))  ! softening
  end do
  end do
  end do
  phi(1,1,1) = 0.0                                ! no mean phi
  call fft3db (phi, phi, mx, my, mz)              ! reverse FFT
  call dump(phi,'phi.dat',2)

  deallocate (k2)
END
