! $Id: turbulence_jared.f90,v 1.1 2005/11/28 17:15:33 aake Exp $

!***********************************************************************
MODULE params
  integer, parameter:: mid=72, mfile=72
  integer mx,my,mz,mw
  integer stdin,stdout,idbg,iseed,isubstep,dbg_select,mpi_rank
  real t
  real sx,sy,sz
  real dx,dy,dz
  real xmin,ymin,zmin
  real, allocatable, dimension(:):: xm,ym,zm
  real, parameter:: pi=3.14159265
  logical master,do_trace
  character(len=mfile) file
END MODULE

!**********************************************************************
MODULE arrays
  real, allocatable, dimension(:,:,:):: r,Ux,Uy,Uz,dUxdt,dUydt,dUzdt
END MODULE

!**********************************************************************
MODULE forcing
  integer, parameter:: dbg_force=1
  logical, parameter:: conservative=.false.
  logical do_force, do_helmh
  real:: t_turb, t_turn, ampl_turb, k1, k2, pk, a_helmh, calib, enom, mach, edot
  real, allocatable, dimension(:,:,:):: pranx,prany,pranz,franx,frany,franz
END MODULE

!***********************************************************************
SUBROUTINE init_params
  USE params
  implicit none
  integer ix,iy,iz
  namelist /in/mx,my,mz,sx,sy,sz,iseed,idbg,dbg_select
!
  mpi_rank=0
  stdin=5
  stdout=6
  master=.true.
!
  mx=48; my=48; mz=48
  t=0.
  sx=1.; sy=1.; sz=1.
  xmin=0.; ymin=0.; zmin=0.
!
  read(stdin,in);  write(*,in)
!
  dx=dx/mx; dy=sy/my; dz=sz/mz
  allocate(xm(mx),ym(my),zm(mz))
  xm=xmin+dx*(/(ix-1,ix=1,mx)/)
  ym=ymin+dy*(/(iy-1,iy=1,my)/)
  zm=zmin+dz*(/(iz-1,iz=1,mz)/)
END

!***********************************************************************
SUBROUTINE init_arrays
  USE params
  USE arrays
  allocate(r(mx,my,mz))
  allocate(Ux(mx,my,mz))
  allocate(Uy(mx,my,mz))
  allocate(Uz(mx,my,mz))
  allocate(dUxdt(mx,my,mz))
  allocate(dUydt(mx,my,mz))
  allocate(dUzdt(mx,my,mz))
  r=1.
  Ux=0.
  Uy=0.
  Uz=0.
  dUxdt=0.
  dUydt=0.
  dUzdt=0.
END

!***********************************************************************
SUBROUTINE init_force
  USE params
  USE forcing
  implicit none
  integer iz
  character(len=mid):: id='$Id: turbulence_jared.f90,v 1.1 2005/11/28 17:15:33 aake Exp $'

  ampl_turb = 0.1
  t_turn = 10.
  do_force = .true.
  do_helmh = .true.
  a_helmh=1.
  mach=0.
  calib=0.648
  k1=1.
  k2=1.5
  pk=7./6.

  call read_force

  t_turb = 0.
  allocate (pranx(mx,my,mz), prany(mx,my,mz), pranz(mx,my,mz))
  allocate (franx(mx,my,mz), frany(mx,my,mz), franz(mx,my,mz))
!$omp parallel do private(iz)
  do iz=1,mz                                ! encourage distribution
    pranx(:,:,iz)=0. ; prany(:,:,iz)=0. ; pranz(:,:,iz)=0.
    franx(:,:,iz)=0. ; frany(:,:,iz)=0. ; franz(:,:,iz)=0.
  end do

END SUBROUTINE

!***********************************************************************
SUBROUTINE read_force
  USE params
  USE forcing
  implicit none
  namelist /force/ do_force, do_helmh, a_helmh, k1, k2, ampl_turb, mach, calib, t_turn, pk

  if (stdin.ge.0) then
    rewind (stdin); read (stdin,force)
  else
    read (*,force)
  end if
  if (master) write (*,force)
  enom = calib*mach**3

END SUBROUTINE

!***********************************************************************
SUBROUTINE forceit (r,Ux,Uy,Uz,dUxdt,dUydt,dUzdt)
!
!  Calculate a random force, confined to a shell in k-space, at
!  regular intervals in time.  We assume rho, P, and the sound
!  speed to be near unity.  The velocity amplitude should be of the
!  order ampl_turb.  The size (scale) of the driving motions is of the
!  order of mx*dx/k1.  Thus the turnover time is given by
!  t_turb*ampl_turb = mx*dx/k1; and the acceleration is of the order
!  ampl_turb/t_turb.
!
!  Note:  In order to restart properly, one would have to save the
!  previous value of iseed.
!
!  06-aug-93/aake:  bug corrected; iseed was not negative at start
!  09-aug-93/aake:  changed to allow restart w same random coeffs
!  21-feb-95/paolo: changed in order to use segments of constant
!                   time der. of acceleration (so continuous accel.)
!            franx(,y,z) are time deriv. of accel. (random, solenoidal);
!            pranx(,y,z) (and scr2(,3,4)) are accelerations. The variable
!            ``accel'' is now the time derivative of the acceleration.
!            ``t_turn'' has the same meaning as before (see comment above).
!  23-jan-96/aake:  bug corrected; ran1() -> 2.*ran1()-1. to span [-1,1]
!
!            Restart must be done AFTER t=t_turn.
!            ------------------------------------
!----------------------------------------------------------------------
  USE params
  USE forcing

  implicit none

  real, dimension(mx,my,mz):: r,Ux,Uy,Uz,dUxdt,dUydt,dUzdt
  real, allocatable, dimension(:,:,:):: scr1,scr2,scr3,scr

  integer i,j,k,jx,jy,jz,nrand,kmax

  complex fxx,fyy,fzz,kx,ky,kz,corr,expikrx,expikry,expikrz

  real x, y, z, xav, yav, zav, rav, w, fact, test, fpow, accel, ran1s
  real eps, fk, average, f

  real, save :: tprev=-1.
  logical, save :: first_time=.true.
  logical debug

!$omp master
  allocate(scr1(mx,my,mz), scr2(mx,my,mz), scr3(mx,my,mz), scr(mx,my,mz))
!$omp end master
!$omp barrier

!----------------------------------------------------------------------
  if (t .ne. tprev) then    ! prevent repeat at same t
    tprev = t
    eps = 1e-5
    
    if (master.and.idbg>0) then
      print *,'forceit: t,t_turb,first_time=',t,t_turb,first_time
      if (t_turb .le. t) print *,'new force'
    endif
    
    if (first_time) then
      t_turb = -1.5*t_turn
!$omp parallel do private(k)
      do k=1,mz
        pranx(:,:,k)=0.
        prany(:,:,k)=0.
        pranz(:,:,k)=0.
      end do
    endif
!--------------------------------------------- new random numbers:
!  Loop until t_turb > t
    do while (t_turb .le. t)
!$omp parallel do private(k)
      do k=1,mz
        pranx(:,:,k)=franx(:,:,k)
        prany(:,:,k)=frany(:,:,k)
        pranz(:,:,k)=franz(:,:,k)
        franx(:,:,k) = 0.
        frany(:,:,k) = 0.
        franz(:,:,k) = 0.
      end do

    t_turb = t_turb + t_turn

    nrand = 0
    kmax=ifix(k2+1.)
    do jx=-kmax,kmax
      do jy=-kmax,kmax
        do jz=-kmax,kmax
          fk = sqrt(float(jx**2+jy**2+jz**2))
          if (fk .ge. k1-eps .and. fk .le. k2+eps) nrand=nrand+1
        enddo
      enddo
    enddo

!--------------------------------------------------------------------------------
    accel  = ampl_turb/t_turn/sqrt(float(nrand)/8.)    ! rms=1./8. per comp.
!--------------------------------------------------------------------------------

      if (master) print *,'random_force:t=',t,t_turb,nrand,iseed

! To obtain a Kolmogorov slope of the driving alone, one should have amplitudes
! a(k) that drop with k^(-11./6.), to have a(k)^2*k^2 = k^(-5./3.).  This ASSUMES
! that the amplitudes are proportional to the driving.  But the energy drain may
! be rather inversely proportional to the turnover time, which goes as k^(2./3.)

    ! pk = 11./6.
    ! pk = 7./6.

    fpow = 0.
    do jx=-kmax,kmax
      kx = cmplx (0., jx*2.*pi/sx)
      do jy=-kmax,kmax
        ky = cmplx (0., jy*2.*pi/sy)
        do jz=-kmax,kmax
          kz = cmplx (0., jz*2.*pi/sz)
          fk = sqrt(float(jx**2+jy**2+jz**2))
          if (fk .ge. k1-eps .and. fk .le. k2+eps) then
            fxx =   cexp(cmplx(0., 2.*pi*ran1s(iseed)))/fk**pk
            fyy =   cexp(cmplx(0., 2.*pi*ran1s(iseed)))/fk**pk
            if (mz.gt.1) then
              fzz = cexp(cmplx(0., 2.*pi*ran1s(iseed)))/fk**pk
            else
              fzz=0.
            end if

! solenoidal field:
!------------------
            if (do_helmh) then
               corr=(kx*fxx+ky*fyy+kz*fzz)/(kx*kx+ky*ky+kz*kz+1e-20) 

               if (jx.ne.0) fxx = fxx - a_helmh*corr*kx
               if (jy.ne.0) fyy = fyy - a_helmh*corr*ky
               if (jz.ne.0) fzz = fzz - a_helmh*corr*kz
            endif
!------------------

            fact=1.
            if (jx.gt.0) fact=fact*0.5
            if (jy.gt.0) fact=fact*0.5
            if (jz.gt.0) fact=fact*0.5
            fpow = fpow+fact*(cabs(fxx)**2+cabs(fyy)**2+cabs(fzz)**2)

!$omp parallel do private(k,i,j,x,y,z,expikrx,expikry,expikrz)
            do k=1,mz
              z = zm(k)-zmin
              do j=1,my
                y = ym(j)-ymin
                do i=1,mx
                  x = xm(i)-xmin
                  expikrx = accel*cexp(kx*(x-0.5*dx)+ky*y+kz*z)
                  expikry = accel*cexp(kx*x+ky*(y-0.5*dy)+kz*z)
                  expikrz = accel*cexp(kx*x+ky*y+kz*(z-0.5*dz))
                  franx(i,j,k) = franx(i,j,k) + real(fxx*expikrx)
                  frany(i,j,k) = frany(i,j,k) + real(fyy*expikry)
                  franz(i,j,k) = franz(i,j,k) + real(fzz*expikrz)
                end do
              end do
            end do
            if (master.and.idbg>0) then
              print '(1x,a,3i2,f6.3,4(f7.3,f6.3))', &
                    'random_force:k=',jx,jy,jz,fk,fxx,fyy,fzz,fact
            endif
          endif
        enddo
      enddo
    enddo

! TEST:
!--------------------------------------------------
    if (master.and.idbg>0) then
      test=sqrt(sum(franx**2+frany**2+franz**2)/mw)
      if (master) print *,'AVERAGE ACCELERATION = ',test,fpow,sqrt(fpow)*accel
    end if
!--------------------------------------------------
    enddo ! while
  endif   ! t > t_prev
!------------------------------------------------------------------
  if (master.and.idbg>0) print *,'interpolate force'

!  Time interpolation of the force
!
  !w = 1.-exp(-(t+t_turn-t_turb)/t_turn)
  w = (t_turn-(t_turb-t))/t_turn
  w = 0.5*(1.-cos(w*pi))

  if (master.and.idbg>0.and.isubstep==1) &
    print *,'force: t-t_turb, t_turn, w:', t-t_turb, t_turn, w
  
  !$omp parallel do private(k)
  do k=1,mz
    scr1(:,:,k) = pranx(:,:,k) + (franx(:,:,k)-pranx(:,:,k))*w
    scr2(:,:,k) = prany(:,:,k) + (frany(:,:,k)-prany(:,:,k))*w
    scr3(:,:,k) = pranz(:,:,k) + (franz(:,:,k)-pranz(:,:,k))*w
  end do
  if (debug(dbg_force)) then
    write(*,*) mpi_rank,' force: Ux'
    write(*,'(10e12.5)') scr1(1,1,:)
    write(*,*) mpi_rank,' force: pranx'
    write(*,'(10e12.5)') pranx(1,1,:)
    write(*,*) mpi_rank,' force: franx'
    write(*,'(10e12.5)') franx(1,1,:)
    write(*,*) mpi_rank,' force: r'
    write(*,'(10e12.5)') r(1,1,:)
  end if

! If 'enom' is non-zero, then use constant energy input driving,
! normlized as per MacLow, astro-ph/9809177.
!---------------------------------------------------------------
  if (enom .gt. 0.) then
    !$omp parallel do private(k)
    do k=1,mz
      scr(:,:,k) = r(:,:,k)*scr1(:,:,k)*Ux(:,:,k) &
                 + r(:,:,k)*scr2(:,:,k)*Uy(:,:,k) &
                 + r(:,:,k)*scr3(:,:,k)*Uz(:,:,k)
    end do

    if (debug(dbg_force)) then
      write(*,*) mpi_rank,' force: scr'
      write(*,'(10e12.5)') scr(1,1,:)
      write(*,*) mpi_rank,' force: Ux'
      write(*,'(10e12.5)') Ux(1,1,:)
    end if

    edot = average(scr)
    write (20,*) 'edot, enom =', edot, enom
    if (master .and. isubstep==2) write (22,'(1x,4g15.6)') t, edot, t_turn
    f = enom/edot
    !$omp parallel do private(k)
    do k=1,mz
      scr1(:,:,k) = f*scr1(:,:,k)
      scr2(:,:,k) = f*scr2(:,:,k)
      scr3(:,:,k) = f*scr3(:,:,k)
    end do
  end if

! Force and velocity averages
!----------------------------

  if (do_trace) print *,'turbulence: average1'
  rav = 1./average(r)
!$omp parallel do private(k)
  do k=1,mz
    scr(:,:,k) = r(:,:,k)*(scr1(:,:,k)+Ux(:,:,k)*(1./t_turn))*rav
  end do
  if (do_trace) print *,'turbulence: average2'
  xav = average(scr)

!$omp parallel do private(k)
  do k=1,mz
    scr(:,:,k) = r(:,:,k)*(scr2(:,:,k)+Uy(:,:,k)*(1./t_turn))*rav
  end do
  if (do_trace) print *,'turbulence: average3'
  yav = average(scr)

!$omp parallel do private(k)
  do k=1,mz
    scr(:,:,k) = r(:,:,k)*(scr3(:,:,k)+Uz(:,:,k)*(1./t_turn))*rav
  end do
  if (do_trace) print *,'turbulence: average4'
  zav = average(scr)

  if (first_time) print *,mpi_rank,'conservative =',conservative
  if (conservative) then
    !$omp parallel do private(k)
    do k=1,mz
      dUxdt(:,:,k) = dUxdt(:,:,k) + r(:,:,k)*(scr1(:,:,k) - xav)
      dUydt(:,:,k) = dUydt(:,:,k) + r(:,:,k)*(scr2(:,:,k) - yav)
      dUzdt(:,:,k) = dUzdt(:,:,k) + r(:,:,k)*(scr3(:,:,k) - zav)
    end do
  else
    !$omp parallel do private(k)
    do k=1,mz
      dUxdt(:,:,k) = dUxdt(:,:,k) + (scr1(:,:,k) - xav)
      dUydt(:,:,k) = dUydt(:,:,k) + (scr2(:,:,k) - yav)
      dUzdt(:,:,k) = dUzdt(:,:,k) + (scr3(:,:,k) - zav)
    end do
  end if

!$omp barrier
!$omp master
  deallocate (scr1,scr2,scr3,scr)
!$omp end master

  first_time = .false.  ! don't come back
END SUBROUTINE

!***********************************************************************
FUNCTION debug (dbg_id)
  USE params
  implicit none
  logical debug
  integer dbg_id
  
  debug = iand(dbg_id,dbg_select).ne.0
END

!***********************************************************************
FUNCTION ran1s(idum)
  INTEGER idum,IA,IM,IQ,IR,NTAB,NDIV
  REAL ran1s,AM,EPS,RNMX
  PARAMETER (IA=16807,IM=2147483647,AM=1./IM,IQ=127773,IR=2836, &
  NTAB=32,NDIV=1+(IM-1)/NTAB,EPS=1.2e-7,RNMX=1.-EPS)
  INTEGER k,iy
  if (idum.le.0) then
    idum=max(-idum,1)
  endif
  k=idum/IQ
  idum=IA*(idum-k*IQ)-IR*k
  if (idum.lt.0) idum=idum+IM
  iy=idum
  ran1s=min(AM*iy,RNMX)
  return
END

!***********************************************************************
FUNCTION average (f)
  USE params
  implicit none
!
!  Return average of array.  Make sure precision is retained, by 
!  summing first individual rows, then columns, then planes.
!
  integer i, j, k
  real:: f(mx,my,mz), sumx, sumy, sumz, average
!-----------------------------------------------------------------------
!
  sumz = 0.
!$omp parallel private(k,j,i,sumx,sumy), reduction(+:sumz)
  do k=1,mz
    sumy = 0.
    do j=1,my
      sumx = 0.
      do i=1,mx
        sumx = sumx + f(i,j,k)
      end do
      sumy = sumy + sumx
    end do
    sumz = sumz + sumy
  end do
!$omp end parallel
  average = sumz/(real(mx)*real(my)*real(mz))
  if (do_trace) print *,'average:',average
!
END

!***********************************************************************
PROGRAM jared_test
  USE params
  USE arrays
  call init_params
  call init_arrays
  call init_force
  call forceit(r,Ux,Uy,Uz,dUxdt,dUydt,dUzdt)
  print *,dUxdt(1:10,1,1)
END
