! $Id: noforce.f90,v 1.11 2010/02/04 23:49:28 aake Exp $
!**********************************************************************
MODULE forcing

  logical do_force, conservative
  real gx, gy,gz, ay, t_friction
  real, allocatable, dimension(:):: rav

END MODULE

!***********************************************************************
SUBROUTINE init_force
  USE params
  USE forcing

  do_force = .false.
  conservative = .true.
  call read_force

END SUBROUTINE

!***********************************************************************
SUBROUTINE read_force
  USE params
  USE forcing
  namelist /force/ do_force, conservative

  if (stdin.ge.0) then
    rewind (stdin); read (stdin,force)
  else
    read (*,force)
  end if
  if (master) write (*,force)
END SUBROUTINE

!***********************************************************************
SUBROUTINE forceit (r,Ux,Uy,Uz,xdnr,ydnr,zdnr,dpxdt,dpydt,dpzdt)
  USE params
  USE forcing
  real, dimension(mx,my,mz):: r,Ux,Uy,Uz,xdnr,ydnr,zdnr,dpxdt,dpydt,dpzdt
!hpf$ distribute(*,*,block):: r,Ux,Uy,Uz,xdnr,ydnr,zdnr,dpxdt,dpydt,dpzdt

  return
END SUBROUTINE

