! $Id: sync_stars.f90,v 1.1 2009/09/01 21:25:29 aake Exp $
SUBROUTINE sync_stars
!*******************************************************************************
  USE stars_m
  implicit none
  integer i
  do i=1,nstars
    star(i)%mass = star(i)%mass + star(i)%dmass
    star(i)%px = star(i)%px + star(i)%dpx
    star(i)%py = star(i)%py + star(i)%dpy
    star(i)%pz = star(i)%pz + star(i)%dpz
  end do
END SUBROUTINE

