! $Id: math.f90,v 1.21 2014/08/28 14:45:14 aake Exp $
!***********************************************************************
MODULE math_m
  double precision dbl
END MODULE

!***********************************************************************
SUBROUTINE maxval_omp(f,fmax)
  USE params
  real, dimension(mx,my,mz):: f
  real fmax, fmax_thread
  integer iz
  fmax = -1e30
  do iz=izs,ize
    fmax_thread = maxval(f(:,lb:ub,iz))
    !$omp critical
    fmax = max(fmax,fmax_thread)
    !$omp end critical
  end do
END

!***********************************************************************
SUBROUTINE average_domain (f, av)
!
!  Return average of array in the local domain.  Make sure precision is
!  retained, by summing first individual rows, then columns, then planes.
!
!  NOTE: The return value should be thread-local variable!
!
  USE params
  USE math_m
  implicit none
  logical debug
  integer i, j, k, lb1, ub1
  real f(mx,my,mz), av
  real(kind=8) sumx, sumy, sumz
  character(len=mid):: id="average_local, $Id: math.f90,v 1.21 2014/08/28 14:45:14 aake Exp $"
!-----------------------------------------------------------------------
  call print_trace (id, dbg_force, 'BEGIN')

  !$omp single
  dbl = 0.
  !$omp end single
  lb1 = max(min(lb,my),1)
  ub1 = max(min(ub,my),1)
  sumz = 0.
  do k=izs,ize
    sumy = 0.
    do j=lb1,ub1
      sumx = 0.
      do i=1,mx
        sumx = sumx + f(i,j,k)
      end do
      sumy = sumy + sumx
    end do
    sumz = sumz + sumy
  end do
  !$omp critical
  dbl = dbl + sumz
  !$omp end critical
  !$omp barrier
  !call barrier_omp('average')

  av = dbl/(real(mx)*real(ub1-lb1+1)*real(mz))
  call print_trace (id, dbg_force, 'END')
END SUBROUTINE

!***********************************************************************
SUBROUTINE average_2d (f, ny, av2d)
!
!  Return average of array in the local domain.  Make sure precision is
!  retained, by summing first individual rows, then columns, then planes.
!
!  NOTE: The return value should be thread-local variable!
!
  USE params
  USE math_m
  implicit none
  integer ny
  real f(mx,mz,ny)
  real(kind=8) av2d(ny)
!-----------------------------------------------------------------------
  call sum_2d (f, ny, av2d)
  !$omp single
  av2d = (av2d/(mxtot))/(mztot)
  !$omp end single
END

!***********************************************************************
SUBROUTINE copy(a,b)
  USE params
  implicit none
  real, dimension(mx,my,mz):: a,b
  integer iz
!
  do iz=izs,ize
    b(:,:,iz) = a(:,:,iz)
  end do
END
