! $Id: grpde.f90,v 1.7 2012/12/31 16:19:12 aake Exp $
!***********************************************************************
 SUBROUTINE pde(r,px,py,pz,e,d,drdt,dpxdt,dpydt,dpzdt,dedt,dddt,flag, &
                 Bx,By,Bz,dBxdt,dBydt,dBzdt)
  USE metric
  USE stagger
  USE forcing
  USE eos
  USE polysolve
  USE visc
  
  implicit none

  real ds, gam1, average, fmaxval,nuimpl
  integer iy,iz,impl,impl_last

  logical flag
  real, dimension(mx,my,mz) :: &
       r,px,py,pz,e,d,drdt,dpxdt,dpydt,dpzdt,dedt,dddt, &
       Bx,By,Bz,dBxdt,dBydt,dBzdt
!hpf$ distribute (*,*,block) :: &
!hpf$  r,px,py,pz,e,d,drdt,dpxdt,dpydt,dpzdt,dedt,dddt, &
!hpf$  Bx,By,Bz,dBxdt,dBydt,dBzdt
  real, allocatable, dimension(:,:,:) :: &
       dd,lnr,P,Cs,divU,nu,lnu,lne,ee,Ux,Uy,Uz, &
       xdnl,ydnl,zdnl,xdnr,ydnr,zdnr, &
       eta,Txx,Tyy,Tzz,Txy,Tzx,Tyz,Ttx,Tty,Ttz,Ttt,ddxd,ddyd,ddzd
  real, allocatable, dimension(:,:,:) :: &
       alp,detg,adetg,idetg,ig11,ig22,ig33,g11,g22,g33,Palp
  real, allocatable, dimension(:,:,:) :: &
       pxup,pyup,pzup,SPD,rd,ed,hm1,vW,W,Dh,rh,DhW,lnDh,Uxup,Uyup,Uzup
  real, allocatable, dimension(:,:,:) :: &
       Sxx,Syy,Szz,Mtermt,Mtermx,Mtermy,Mtermz
!hpf$ distribute (*,*,block) :: &
!hpf$  dd,lnr,P,Cs,divU,nu,lnu,lne,ee,Ux,Uy,Uz, &
!hpf$  Sxx,Syy,Szz,Sxy,Szx,Syz,xdnl,ydnl,zdnl,xdnr,ydnr,zdnr, &
!hpf$  eta,Txx,Tyy,Tzz,Txy,Tzx,Tyz,ddxd,ddyd,ddzd
  character(len=mid) id
  data id/'$Id: grpde.f90,v 1.7 2012/12/31 16:19:12 aake Exp $'/

  if (id .ne. '') print *,id
  id = ''
  gam1 = gamma - 1.
!-----------------------------------------------------------------------
!  Correcting for metric factors, finding the primitive variables:
!  specific enthalpy minus one, Lorentz Boost
!-----------------------------------------------------------------------
  allocate( alp(mmx,mmy,mmz), &
           detg(mmx,mmy,mmz),idetg(mmx,mmy,mmz), &
           g11(mmx,mmy,mmz), g22(mmx,mmy,mmz), g33(mmx,mmy,mmz), &
           ig11(mmx,mmy,mmz),ig22(mmx,mmy,mmz),ig33(mmx,mmy,mmz))
  alp  = metricalpha(0)
  detg = determinantg(0)
  g11  = metricg(0,1,1)
  g22  = metricg(0,2,2)
  g33  = metricg(0,3,3)
  idetg = 1./detg
  ig11  = 1./g11
  ig22  = 1./g22
  ig33  = 1./g33
  allocate(pxup(mx,my,mz),pyup(mx,my,mz),pzup(mx,my,mz))
  pxup = xup(px)           !
  pxup = mmult(pxup,idetg) !
  pyup = yup(py)           !
  pyup = mmult(pyup,idetg) !
  pzup = zup(pz)           !
  pzup = mmult(pzup,idetg) ! Centered momenta
  ! Lets go find those primitive variables - Centered
  allocate(hm1(mx,my,mz),vW(mx,my,mz),SPD(mx,my,mz),rd(mx,my,mz),ed(mx,my,mz))
  ! FIXME! This expression is only valid for a diagonal metric
  ! Remember pxup = sqrt(gxx)*g*rho*h*W**2*V_x
  SPD = mmult(pxup**2,ig11) + mmult(pxup**2,ig22) + mmult(pxup**2,ig33)
  ! FIXME! END
  rd  = mmult(r,idetg)
  ed  = mmult(e,idetg)
  call rhdsolve(rd, ed, SPD, vW, hm1)
  deallocate(SPD)
  If (minval(hm1) .lt. 0) Then
    Print *, 'hm1 is negative - should not happen!!'
    Print *, minval(hm1)
  end if
!-----------------------------------------------------------------------
!  Velocities, Lorentz Boost
!-----------------------------------------------------------------------
  allocate(Uxup(mx,my,mz),Uyup(mx,my,mz),Uzup(mx,my,mz)) !  6 chunks
  allocate(W(mx,my,mz),Dh(mx,my,mz),DhW(mx,my,mz),lnDh(mx,my,mz))
!$omp parallel do private(iz)
  do iz=1,mz
      W(:,:,iz)   = Sqrt(1. + vW(:,:,iz)**2)            ! Gamma factor centered
      Dh(:,:,iz)  = (1. + hm1(:,:,iz))*r(:,:,iz)
      DhW(:,:,iz) = Dh(:,:,iz)*W(:,:,iz)
      lnDh(:,:,iz)= Alog(Dh(:,:,iz))
      Uzup(:,:,iz) = 1./DhW(:,:,iz)
      Uxup(:,:,iz) = pxup(:,:,iz)*Uzup(:,:,iz)          ! V_x - centered
      Uyup(:,:,iz) = pyup(:,:,iz)*Uzup(:,:,iz)          ! V_y - centered
      Uzup(:,:,iz) = pzup(:,:,iz)*Uzup(:,:,iz)          ! V_z - centered
  end do
  deallocate(pxup,pyup,pzup)                            ! 10 chunks
  ! Uiup == SFCS transport velocity = alpha/sqrt(gii) v^i - beta^i
  ! Uiup = sqrt(gii)*detg*V_i
  ! FIXME! This expression is only valid for a diagonal metric
  Uxup = mmult(Uxup,alp*ig11*idetg)                   ! V^x - centered - SFC system
  Uyup = mmult(Uyup,alp*ig22*idetg)                   ! V^y - centered - SFC system
  Uzup = mmult(Uzup,alp*ig33*idetg)                   ! V^z - centered - SFC system
  ! FIXME! END
!-----------------------------------------------------------------------
!  Velocities, face values
!-----------------------------------------------------------------------
  allocate(Ux(mx,my,mz),Uy(mx,my,mz),Uz(mx,my,mz))      !  6 chunks
  Ux = xdn(Uxup)                                        ! ...Face values
  Uy = ydn(Uyup)                                        ! ...Face values
  Uz = zdn(Uzup)                                        ! ...Face values
!  call velocity_boundary(r,xdnr,ydnr,zdnr,Ux,Uy,Uz,px,py,pz,e,Bx,By,Bz)
!  call forceit (r,Ux,Uy,Uz,xdnr,ydnr,zdnr,dpxdt,dpydt,dpzdt)
  allocate(Sxx(mx,my,mz),Syy(mx,my,mz),Szz(mx,my,mz))   !  9 chunks
! Divergence of SFC system three velocity.
  Sxx = ddxup(Ux)
  Syy = ddyup(Uy)
  Szz = ddzup(Uz)
!-----------------------------------------------------------------------
!  Numerical viscosity, Richtmyer & Morton type
!-----------------------------------------------------------------------
  allocate(nu(mx,my,mz),lnu(mx,my,mz))     ! 12 chunks
!$omp parallel do private(iz)
  do iz=1,mz
    nu(:,:,iz) = Sxx(:,:,iz)+Syy(:,:,iz)+Szz(:,:,iz)
    nu(:,:,iz) = max(-nu(:,:,iz),0.0)
  end do
  deallocate(Sxx,Syy,Szz)                                             ! 10 chunks
  if (lb.lt.6) call regularize(nu)
  nu = smooth3(max5(nu))
  call regularize(nu)
!
  allocate(Cs(mx,my,mz))
  ds = max(dx,dy,dz)
  gam1 = gamma - 1.
  ! This is SFCS transport velocity, is that correct ??
  Cs = mmult(Uxup**2 + Uyup**2  + Uzup**2,alp**2)
  Urms = sqrt(average(Cs))
!$omp parallel do private(iz), firstprivate(nu1,nu2,ds)
    do iz=1,mz
      Cs(:,:,iz) = sqrt(gam1*hm1(:,:,iz)/(1.+hm1(:,:,iz))) + sqrt(Cs(:,:,iz))
      nu(:,:,iz) = nu1*(Cs(:,:,iz)) + nu2*ds*nu(:,:,iz)
      lnu(:,:,iz) = alog(nu(:,:,iz))
    end do
!-----------------------------------------------------------------------
!  Viscous stress, diagonal and off-diagonal elements
!-----------------------------------------------------------------------
  call viscosity(Dh,lnDh,nu,lnu,vW,W,Ux,Uy,Uz,Uxup,Uyup,Uzup, &
                Ttt,Txx,Tyy,Tzz,Txy,Tzx,Tyz,Ttx,Tty,Ttz)
!-----------------------------------------------------------------------
! Multiply viscosities by metric factors
!-----------------------------------------------------------------------
  allocate(adetg(mmx,mmy,mmz))
  adetg = alp*detg
  Txx = mmult(Txx,adetg)
  Tyy = mmult(Tyy,adetg)
  Tzz = mmult(Tzz,adetg)
!-----------------------------------------------------------------------
!  Viscosity, energy density
!-----------------------------------------------------------------------
  If (do_2nddiv) then
    dedt = dedt - ddxup1(Ttx) - ddyup1(Tty) - ddzup1(Ttz)
  else
    dedt = dedt - ddxup(Ttx) - ddyup(Tty) - ddzup(Ttz)
  endif
! FIXME! Does only work for a diagonal metric
!-----------------------------------------------------------------------
!  Calculate "ln alpha" term in dedt
!-----------------------------------------------------------------------
  Ttx = mmult(Ttx,adetg*sqrt(ig11))
  Tty = mmult(Tty,adetg*sqrt(ig22))
  Ttz = mmult(Ttz,adetg*sqrt(ig33))
  dedt = dedt - lnalphaterm(DhW,Uxup,Uyup,Uzup,Ttx,Tty,Ttz,alp)
! END FIXME
  deallocate (Ttx,Tty,Ttz)
!-----------------------------------------------------------------------
!  Energy per unit mass
!-----------------------------------------------------------------------
  allocate(ee(mx,my,mz))
!  if (do_ionization) then
!    call pressure(drho,ee,p,vW,W)
!  else
!$omp parallel do private(iz)
  do iz=1,mz
    ee(:,:,iz)  = 1./gamma*hm1(:,:,iz)          ! internal energy
  end do
!-----------------------------------------------------------------------
!  Boundary condition -- may need to calculate pressure internally
!-----------------------------------------------------------------------
!  if (do_energy) call energy_boundary(r,lnr,Ux,Uy,Uz,e,ee,p,Bx,By,Bz)
!-----------------------------------------------------------------------
!  Pressure
!-----------------------------------------------------------------------
  allocate(P(mx,my,mz),rh(mx,my,mz))
  rh = mmult(r/W,idetg)
!$omp parallel do private(iz)
  do iz=1,mz
    P(:,:,iz)  = gam1*rh(:,:,iz)*ee(:,:,iz)
    rh(:,:,iz) = rd(:,:,iz)*(1. + hm1(:,:,iz))
  end do
!-----------------------------------------------------------------------
! Time stepping
!-----------------------------------------------------------------------
  if (flag) then
    ds = min(dx,dy,dz)
    Cs = Cs*(dt/ds)
    Cu = fmaxval('Cu',Cs)
    Cs = (6.2*3.*0.75/2.*dt/ds)*nu
    Cv = fmaxval('Cv',Cs)
  end if
  deallocate(Cs)
!-----------------------------------------------------------------------
!  Mass conservation, cell centered
!-----------------------------------------------------------------------
  allocate(lnr(mx,my,mz),xdnl(mx,my,mz),ydnl(mx,my,mz),zdnl(mx,my,mz))
!$omp parallel do private(iz)
  do iz=1,mz
    lnr(:,:,iz) = alog(r(:,:,iz))
  end do
  xdnl = xdn(lnr)
  ydnl = ydn(lnr)
  zdnl = zdn(lnr)
  if (do_2nddiv) then
    if (nur>0) then
      drdt = drdt &
         - ddxup1(Ux*exp(xdnl)-dx*nur*exp(xdn(lnu+lnr))*ddxdn(lnr)) &
         - ddyup1(Uy*exp(ydnl)-dy*nur*exp(ydn(lnu+lnr))*ddydn(lnr)) &
         - ddzup1(Uz*exp(zdnl)-dz*nur*exp(zdn(lnu+lnr))*ddzdn(lnr))
    else
      drdt = drdt - ddxup1(Ux*exp(xdnl)) - ddyup1(Uy*exp(ydnl)) &
                  - ddzup1(Uz*exp(zdnl))
    end if
  else
    if (nur>0) then
      drdt = drdt &
         - ddxup(Ux*exp(xdnl)-dx*nur*exp(xdn(lnu+lnr))*ddxdn(lnr)) &
         - ddyup(Uy*exp(ydnl)-dy*nur*exp(ydn(lnu+lnr))*ddydn(lnr)) &
         - ddzup(Uz*exp(zdnl)-dz*nur*exp(zdn(lnu+lnr))*ddzdn(lnr))
    else
      drdt = drdt - ddxup(Ux*exp(xdnl)) - ddyup(Uy*exp(ydnl)) - ddzup(Uz*exp(zdnl))
    end if
  end if
  deallocate(xdnl,ydnl,zdnl)                                            ! 10 chunks
! FIXME Does only work for a diagonal metric
  lnDh = mmult(DhW,detg/alp)
!*$* assert do (concurrent)
  Txx = Txx + mmult(lnDh*Uxup**2,g11)
!*$* assert do (concurrent)
  Tyy = Tyy + mmult(lnDh*Uyup**2,g22)
!*$* assert do (concurrent)
  Tzz = Tzz + mmult(lnDh*Uzup**2,g33)
  lnDh = Alog(lnDh)
  Txy = Txy + exp(xdn(ydn(madd(lnDh,Alog(g11)))))*ydn(Ux)*xdn(Uy)
  Tyz = Tyz + exp(ydn(zdn(madd(lnDh,Alog(g22)))))*zdn(Uy)*ydn(Uz)
  Tzx = Tzx + exp(zdn(xdn(madd(lnDh,Alog(g33)))))*xdn(Uz)*zdn(Ux)
!-----------------------------------------------------------------------
!  Calculate geometric terms: M_sigma = 1/2 detg T^munu \partial_sigma g_munu
!-----------------------------------------------------------------------
  Ttt = mmult((e + r),idetg) + Ttt
  allocate(Mtermt(mx,my,mz),Mtermx(mx,my,mz),Mtermy(mx,my,mz),Mtermz(mx,my,mz))
  call mterm(Mtermt,Mtermx,Mtermy,Mtermz,Ttt,Txx,Tyy,Tzz,Txy,Tyz,Tzx,detg) 
  deallocate(Ttt)
  If (.not. time_dependence) deallocate(Mtermt)
!-----------------------------------------------------------------------
!  Add Pressure
!-----------------------------------------------------------------------
  allocate(Palp(mx,my,mz))
  Palp = mmult(P,adetg)
  deallocate(adetg)
!$omp parallel do private(iz)
  do iz=1,mz
    Txx(:,:,iz) = Txx(:,:,iz) + Palp(:,:,iz)
    Tyy(:,:,iz) = Tyy(:,:,iz) + Palp(:,:,iz)
    Tzz(:,:,iz) = Tzz(:,:,iz) + Palp(:,:,iz)
  end do
  deallocate(Palp)
! END FIXME
  deallocate(lnDh)
!-----------------------------------------------------------------------
!  Equations of motion, face centred
!-----------------------------------------------------------------------
  if (do_2nddiv) then
    dpxdt = dpxdt - ddxdn1(Txx) - ddyup1(Txy) - ddzup1(Tzx) + Mtermx
    dpydt = dpydt - ddydn1(Tyy) - ddzup1(Tyz) - ddxup1(Txy) + Mtermy
    dpzdt = dpzdt - ddzdn1(Tzz) - ddxup1(Tzx) - ddyup1(Tyz) + Mtermz
  else
    dpxdt = dpxdt - ddxdn (Txx) - ddyup (Txy) - ddzup (Tzx) + Mtermx
    dpydt = dpydt - ddydn (Tyy) - ddzup (Tyz) - ddxup (Txy) + Mtermy
    dpzdt = dpzdt - ddzdn (Tzz) - ddxup (Tzx) - ddyup (Tyz) + Mtermz
  end if
  deallocate(Mtermx,Mtermy,Mtermz,Txy,Tyz,Tzx,Txx,Tyy,Tzz)                                   !  1 chunks
!-----------------------------------------------------------------------
!  MHD
!-----------------------------------------------------------------------
  if (do_mhd) then
    allocate (eta(mx,my,mz))
    call resist(eta,rh,P,Uxup,Uyup,Uzup,Bx,By,Bz,flag)
    call mhd(eta,Ux,Uy,Uz,Bx,By,Bz,dpxdt,dpydt,dpzdt,dedt,dBxdt,dBydt,dBzdt)
    deallocate (eta)
  end if
  deallocate(Uxup,Uyup,Uzup,rh) !  6 chunks
!-----------------------------------------------------------------------
!  Abundance per unit mass
!-----------------------------------------------------------------------
!  if (do_pscalar .or. do_cool) then
!    allocate (dd(mx,my,mz))
!    dd=d/r
!  end if

!-----------------------------------------------------------------------
!  Passive scalar
!-----------------------------------------------------------------------
!  call passive(nu,lnu,r,px,py,pz,Ux,Uy,Uz,dd,dddt)
  deallocate (nu)                                              !  7 chunks

  if (do_energy) then

  allocate(lne(mx,my,mz))                                             !  3 chunks
!$omp parallel do private(iz)
    do iz=1,mz
      lne(:,:,iz) = alog(e(:,:,iz) + P(:,:,iz))
      lnu(:,:,iz) = lnu(:,:,iz) + lnr(:,:,iz)
      lnr(:,:,iz) = alog(hm1(:,:,iz))
      lnu(:,:,iz) = lnu(:,:,iz) + lnr(:,:,iz)
    end do
!-----------------------------------------------------------------------
!  Energy advection and diffusion
!-----------------------------------------------------------------------
    if (do_2nddiv) then
      dedt = dedt &
       - ddxup1(exp(xdn(lne))*Ux - dx*exp(xdn(lnu))*ddxdn(lnr)) &
       - ddyup1(exp(ydn(lne))*Uy - dy*exp(ydn(lnu))*ddydn(lnr)) &
       - ddzup1(exp(zdn(lne))*Uz - dz*exp(zdn(lnu))*ddzdn(lnr))
    else
      dedt = dedt &
       - ddxup (exp(xdn(lne))*Ux - dx*exp(xdn(lnu))*ddxdn(lnr)) &
       - ddyup (exp(ydn(lne))*Uy - dy*exp(ydn(lnu))*ddydn(lnr)) &
       - ddzup (exp(zdn(lne))*Uz - dz*exp(zdn(lnu))*ddzdn(lnr))
    end if 
    deallocate(P,Ux,Uy,Uz)

!-----------------------------------------------------------------------
!  Cooling and conduction
!-----------------------------------------------------------------------
    if (do_pscalar .or. do_cool) then
      call conduction (r,e,ee,Bx,By,Bz,dedt)
      call coolit (r,ee,lne,dd,dedt)
    end if
    deallocate(lne)                                                     !  3 chunks         
  else
    deallocate(P)
  end if
  deallocate(ee,lnu)                                                    !  1 chunks
  deallocate(lnr)                                                       !  3 chunks
  if (do_pscalar .or. do_cool) deallocate(dd)                           !  0 chunks
! print **,5,dedt(16,106,38:40)/e(16,106,38:40),e(16,106,38:40)

  call ddt_boundary (r,px,py,pz,e,Bx,By,Bz,drdt,dpxdt,dpydt,dpzdt, &
                     dedt,dBxdt,dBydt,dBzdt)
  
  END
