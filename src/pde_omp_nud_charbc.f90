! $Id: pde_omp_nud_charbc.f90,v 1.4 2012/12/31 16:19:15 aake Exp $
!***********************************************************************
  SUBROUTINE pde (flag)
  USE params
  USE variables
  USE arrays
  implicit none
  logical flag, debug
  real void, etime
  real,save:: cpu0(2), cpu1(2)

  if (debug(dbg_hd) .and. omp_master) then; void = etime(cpu1)
  print '(1x,a9,2f7.3)','    pde0',(cpu1-cpu0)*omp_nthreads; cpu0=cpu1; endif
  call pde1 (flag,r,px,py,pz,e,d,Bx,By,Bz,drdt,dpxdt,dpydt,dpzdt,dedt,dddt,dBxdt,dBydt,dBzdt, &
             Ux,Uy,Uz,Ex,Ey,Ez, &
             wk01,wk02,wk03,wk04,wk05,wk06,wk07,wk08,wk09, &
             wk10,wk11,wk12,wk13,wk14,wk15,wk16,wk17, &
             scr1,scr2,scr3,scr4,scr5,scr6)
  if (debug(dbg_hd) .and. omp_master) then; void = etime(cpu1)
  print '(1x,a9,2f7.3)','    pde1',(cpu1-cpu0)*omp_nthreads; cpu0=cpu1; endif
  call pde2 (flag,r,px,py,pz,e,d,Bx,By,Bz,drdt,dpxdt,dpydt,dpzdt,dedt,dddt,dBxdt,dBydt,dBzdt, &
             Ux,Uy,Uz,Ex,Ey,Ez, &
             wk01,wk02,wk03,wk04,wk05,wk06,wk07,wk08,wk09, &
             wk10,wk11,wk12,wk13,wk14,wk15,wk16,wk17, &
             scr1,scr2,scr3,scr4,scr5,scr6)
  if (debug(dbg_hd) .and. omp_master) then ;void = etime(cpu1)
  print '(1x,a9,2f7.3)','    pde2',(cpu1-cpu0)*omp_nthreads; cpu0=cpu1; endif
  call pde3 (flag,r,px,py,pz,e,d,Bx,By,Bz,drdt,dpxdt,dpydt,dpzdt,dedt,dddt,dBxdt,dBydt,dBzdt, &
             Ux,Uy,Uz,Ex,Ey,Ez, &
             wk01,wk02,wk03,wk04,wk05,wk06,wk07,wk08,wk09, &
             wk10,wk11,wk12,wk13,wk14,wk15,wk16,wk17, &
             scr1,scr2,scr3,scr4,scr5,scr6)
  if (debug(dbg_hd) .and. omp_master) then; void = etime(cpu1)
  print '(1x,a9,2f7.3)','    pde3',(cpu1-cpu0)*omp_nthreads; cpu0=cpu1; endif
  END

!***********************************************************************
  SUBROUTINE pde1 (flag,r,px,py,pz,e,d,Bx,By,Bz,drdt,dpxdt,dpydt,dpzdt,dedt,dddt,dBxdt,dBydt,dBzdt, &
             Ux,Uy,Uz,Ex,Ey,Ez, &
             wk01,wk02,wk03,wk04,wk05,wk06,wk07,wk08,wk09, &
             wk10,wk11,wk12,wk13,wk14,wk15,wk16,wk17, &
             scr1,scr2,scr3,scr4,scr5,scr6)
  USE params
  !USE variables
  USE arrays, only: eeav, dpxav, dpzav
  USE quench_m, only: qlim
  implicit none
  real, dimension(mx,my,mz):: &
             r,px,py,pz,e,d,Bx,By,Bz,drdt,dpxdt,dpydt,dpzdt,dedt,dddt,dBxdt,dBydt,dBzdt, &
             Ux,Uy,Uz,Ex,Ey,Ez, &
             wk01,wk02,wk03,wk04,wk05,wk06,wk07,wk08,wk09, &
             wk10,wk11,wk12,wk13,wk14,wk15,wk16,wk17, &
             scr1,scr2,scr3,scr4,scr5,scr6

  logical flag, omp_in_parallel, debug
  integer iz, iy, ix

  real gam1, average, fCv, soundsp, velocity, sumy, sumz
  real cput(2), void, dtime
  real, dimension(mx):: exp1

  character (len=mid):: id
  data id/'$Id: pde_omp_nud_charbc.f90,v 1.4 2012/12/31 16:19:15 aake Exp $'/
!-----------------------------------------------------------------------

  call print_id(id)
  if (do_trace) print *,'pde1',omp_mythread

                       if (debug(dbg_hd) .and. omp_master) then; void = dtime(cput)
		       print '(1x,a5,2f7.3)','pde0 ',cput*omp_nthreads; endif

  if (gamma .eq. 1.) then
    gam1 = 1.
  else
    gam1 = gamma-1.
  end if

!ptr  dd => scr4
!ptr  lne => scr5
!ptr  xdnl => wk16
!ptr  ydnl => wk17
!ptr  zdnl => wk01
!ptr  lnu => wk03
!ptr  S2  => wk09
!ptr  p   => wk04; ee => wk05; nu => wk06
!ptr  Cs  => wk07; lnr => wk08; nud => wk02
!ptr  Sxy => wk10; Syz => wk11; Szx => wk12
!ptr  Sxx => wk13; Syy => wk14; Szz => wk15
!ptr  Txy => wk13; Tyz => wk14; Tzx => wk15
!ptr  Txx => wk07; Tyy => wk08; Tzz => wk09

!ptr  eta => wk16; fudge => wk17
!ptr  Jx => wk18; Jy => wk15; Jz => wk20
!ptr  Bx_y => wk10; By_z => wk11; Bz_x => wk12
!ptr  Bx_z => wk13; By_x => wk14; Bz_y => wk15
!ptr  Ux_y => scr1; Uy_z => scr2; Uz_x => scr3
!ptr  Ux_z => scr4; Uy_x => scr5; Uz_y => scr6

!-----------------------------------------------------------------------
!  Viscosity Courant condition factors:
!    qlim : max factor from the quenching operator (=1 when do_quench=f)
!      2. : the diagonal viscosity has a factor two in front of wk06
!     6.2 : the maximum value returned from ddxup(ddxdn(f)) when dx=1.
!      3. : for the worst case of a 3-D checker board
!      2. : normalize so Ctd=1. when we reach 1x overshoot
!-----------------------------------------------------------------------
  fCv = qlim*2.*6.2*3.*dt/2.

!-----------------------------------------------------------------------
!  Velocities, pressure
!-----------------------------------------------------------------------

  do iz=izs,ize
    wk08(:,:,iz) = alog(r(:,:,iz))
  end do
  call density_boundary(r,wk08,py,e)

                       if (debug(dbg_hd) .and. omp_master) then; void = dtime(cput)
		       print '(1x,a5,2f7.3)','dbdry',cput*omp_nthreads; endif
                       if (do_trace) print *,'dbdry ',omp_mythread

!-----------------------------------------------------------------------
!  Energy per unit mass
!-----------------------------------------------------------------------
  do iz=izs,ize
    wk05(:,:,iz) = e(:,:,iz)/r(:,:,iz)
  end do
                       if (debug(dbg_hd) .and. omp_master) then; void = dtime(cput)
		       print '(1x,a5,2f7.3)','pde1c',cput*omp_nthreads; endif
                       if (do_trace) print *,'ee ',omp_mythread

!-----------------------------------------------------------------------
!  Pressure
!-----------------------------------------------------------------------
  if (do_ionization) then
    call pressure (r,wk05,wk04)
  else
    do iz=izs,ize
      wk04(:,:,iz)  = gam1*e(:,:,iz)
    end do
  end if

                       if (debug(dbg_hd) .and. omp_master) then; void = dtime(cput)
		       print '(1x,a5,2f7.3)','eos  ',cput*omp_nthreads; endif
                       if (do_trace) print *,'eos ',omp_mythread

!-----------------------------------------------------------------------
!  Boundary condition -- may need to use pressure internally
!-----------------------------------------------------------------------
  call energy_boundary(r,wk08,Ux,Uy,Uz,e,wk05,wk04,Bx,By,Bz)

                       if (debug(dbg_hd) .and. omp_master) then; void = dtime(cput)
		       print '(1x,a5,2f7.3)','ebdry',cput*omp_nthreads; endif
                       if (do_trace) print *,'ebdry ',omp_mythread

  call xdn_set (wk08, wk16)
  call ydn_set (wk08, wk17)

  call barrier_omp('pde1')                                              ! barrier 1, needed because wk08 just computed
  call zdn_set (wk08, wk01)
  do iz=izs,ize
  do iy=1,my
    call expn (mx, wk16(:,iy,iz), scr1(:,iy,iz))
    Ux(:,iy,iz) = px(:,iy,iz)/scr1(:,iy,iz)
    call expn (mx, wk17(:,iy,iz), scr2(:,iy,iz))
    Uy(:,iy,iz) = py(:,iy,iz)/scr2(:,iy,iz)
    call expn (mx, wk01(:,iy,iz), scr3(:,iy,iz))
    Uz(:,iy,iz) = pz(:,iy,iz)/scr3(:,iy,iz)
  end do
  end do
                       if (debug(dbg_hd) .and. omp_master) then; void = dtime(cput)
		       print '(1x,a5,2f7.3)','pde1a',cput*omp_nthreads; endif
                       if (do_trace) print *,'Uxyz ',omp_mythread

  call velocity_boundary(r,scr1,scr2,scr3,Ux,Uy,Uz,px,py,pz,e,Bx,By,Bz)

                       if (debug(dbg_hd) .and. omp_master) then; void = dtime(cput)
		       print '(1x,a5,2f7.3)','vbdry',cput*omp_nthreads; endif
                       if (do_trace) print *,'vbdry ',omp_mythread

    call dump (dpxdt, 'dpxdt.dmp', 0)
  call forceit (r,Ux,Uy,Uz,scr1,scr2,scr3,dpxdt,dpydt,dpzdt)
    call dump (dpxdt, 'dpxdt.dmp', 1)

                       if (debug(dbg_hd) .and. omp_master) then; void = dtime(cput)
		       print '(1x,a5,2f7.3)','force',cput*omp_nthreads; endif
                       if (do_trace) print *,'force ',omp_mythread

  call barrier_omp('pde2')                                              ! barrier 2, needed because Uz computed since last barrier
  call ddzup_set (Uz, wk15)

  if (do_kepler) then                                                   ! subtract Kepler velocity?
    do iz=izs,ize
      scr5(:,:,iz) = Ux(:,:,iz) - scr1(:,:,iz)                          ! use scr5-6 to avoid need for barrier
      scr6(:,:,iz) = Uy(:,:,iz) - scr2(:,:,iz)
    end do
    call ddxup_set (scr5, wk13)
    call ddyup_set (scr6, wk14)
    call ddxdn_set (scr6,scr1)
    call ddydn_set (scr5,scr3)
  else
    call ddxup_set (Ux, wk13)
    call ddyup_set (Uy, wk14)
    call ddxdn_set (Uy,scr1)
    call ddydn_set (Ux,scr3)
  end if

  call ddzdn_set (Uy,scr2)
  call ddzdn_set (Ux,scr4)
  call ddxdn_set (Uz,scr5)
  call ddydn_set (Uz,scr6)
  do iz=izs,ize
    wk10(:,:,iz) = (scr1(:,:,iz)+scr3(:,:,iz))*0.5
    wk11(:,:,iz) = (scr6(:,:,iz)+scr2(:,:,iz))*0.5
    wk12(:,:,iz) = (scr4(:,:,iz)+scr5(:,:,iz))*0.5
  end do

  if (Csmag > 0) then
    do iz=izs,ize
      wk09(:,:,iz) = wk13(:,:,iz)**2 + wk14(:,:,iz)**2 + wk15(:,:,iz)**2
      scr1(:,:,iz) = wk10(:,:,iz)**2
      scr2(:,:,iz) = wk11(:,:,iz)**2
      scr3(:,:,iz) = wk12(:,:,iz)**2
    end do
    call xup1_set (scr1,scr4)
    call yup1_set (scr4,scr1)
    call yup1_set (scr2,scr4)
    call barrier_omp('pde3')                                            ! scr4 needs to be all done
    call zup1_set (scr4,scr2)
    call barrier_omp('pde4')                                            ! scr4 needs to be all done
    call zup1_set (scr3,scr5)
    call barrier_omp('pde5')                                            ! scr4 needs to be all done
    call xup1_set (scr5,scr3)
    do iz=izs,ize
      wk09(:,:,iz) = sqrt(2.*(wk09(:,:,iz) + 2.*(scr1(:,:,iz)+scr2(:,:,iz)+scr3(:,:,iz))))
    end do
  else
    do iz=izs,ize
      wk09(:,:,iz) = 0.
    end do
  end if
                       if (do_trace) print *,'Csmag ',omp_mythread

!-----------------------------------------------------------------------
!  First part of viscosity
!-----------------------------------------------------------------------
  if (nu4 > 0.) then
    call barrier_omp('pde6')                                            ! is this needed?
    call dif2a_set (wk08,wk02)
    do iz=izs,ize
    do iy=1,my
    do ix=1,mx
      wk02(ix,iy,iz) = max(1.,nu4*wk02(ix,iy,iz))* &
        (nu2*max(-wk13(ix,iy,iz)-wk14(ix,iy,iz)-wk15(ix,iy,iz),0.)+1e-20 + &
	 Csmag*wk09(ix,iy,iz))
    end do
    end do
    end do
  else
    do iz=izs,ize
    do iy=1,my
    do ix=1,mx
      wk02(ix,iy,iz) = wk13(ix,iy,iz)+wk14(ix,iy,iz)+wk15(ix,iy,iz)
      wk02(ix,iy,iz) = nu2*max(-wk02(ix,iy,iz),0.)+1e-20 + Csmag*wk09(ix,iy,iz)
    end do
    end do
    end do
  end if
    call dump (wk02, 'dpxdt.dmp', 2)
                       if (do_trace) print *,'nu2 ',omp_mythread

  call xup_set (Ux, scr1)
  call yup_set (Uy, scr2)
  call zup_set (Uz, scr3)
  do iz=izs,ize
    wk07(:,:,iz) = scr1(:,:,iz)**2 + scr2(:,:,iz)**2 + scr3(:,:,iz)**2
  end do
  call average_subr (wk07, Urms)

!-----------------------------------------------------------------------
!  Polytropic pressure, at given sound speed
!-----------------------------------------------------------------------
  if (.not. do_energy) then
    if (gamma .eq. 1.) then
      do iz=izs,ize
        e(:,:,iz) = csound**2*r(:,:,iz)
      end do
    else
      do iz=izs,ize
        e(:,:,iz) = (1./(gamma*gam1)*csound**2)*r(:,:,iz)**gamma
      end do
    end if
  end if

!-----------------------------------------------------------------------
!  Numerical viscosity, Richtmyer & Morton type.  For testing purposes,
!  nu1<0 signals the use of constant diffusivity.
!-----------------------------------------------------------------------
  if (nu1.lt.0.0) then
    do iz=izs,ize
      wk07(:,:,iz) = sqrt(gamma*wk04(:,:,iz)/r(:,:,iz)) + sqrt(wk07(:,:,iz))
      wk06(:,:,iz) = abs(nu1)
    end do
  else
    do iz=izs,ize
    do iy=1,my
    do ix=1,mx
      soundsp = sqrt(gamma*wk04(ix,iy,iz)/r(ix,iy,iz))
      velocity = sqrt(wk07(ix,iy,iz))
      wk07(ix,iy,iz) = soundsp + velocity
      wk06(ix,iy,iz) = nu1*velocity + nu3*soundsp
    end do
    end do
    end do
  end if

  call regularize(wk02)
  call regularize(wk06)
                       if (do_trace) print *,'nu1+3 ',omp_mythread

  call barrier_omp('pde7')                                              ! barrier needed only if removing "single"
  !$omp single
  Urms = sqrt(Urms)
  !$omp end single nowait

  if (do_max5_hd) then
    call smooth3max5_set (wk02, scr1)
  else
    call smooth3max3_set (wk02, scr1)
  endif
    call dump (wk02, 'dpxdt.dmp', 3)

                       if (debug(dbg_hd) .and. omp_master) then; void = dtime(cput)
		       print '(1x,a5,2f7.3)','smth3',cput*omp_nthreads; endif
                       if(do_trace) print *,'smoothmax ',omp_mythread

  call barrier_omp('pde8')                                              ! barrier 4, NOT NEEDED????
  call regularize(wk02)
                       if(do_trace) print *,'smoothmax ',omp_mythread

  do iz=izs,ize
    wk03(:,:,iz) = alog(wk06(:,:,iz))
  end do

  if (flag) then
    do iz=izs,ize
    do iy=1,my
    do ix=1,mx
      wk07(ix,iy,iz) = wk07(ix,iy,iz)*(dt/min(dxm(ix),dym(iy),dzm(iz)))
      scr1(ix,iy,iz) = fCv*(wk02(ix,iy,iz)+wk06(ix,iy,iz)/min(dxm(ix),dym(iy),dzm(iz)))
    end do
    end do
    end do
    call fmaxval_subr ('Cu',wk07,Cu)
    call fmaxval_subr ('Cv',scr1,Cv)
  end if
  do iz=izs,ize
    wk03(:,:,iz) = wk03(:,:,iz)+wk08(:,:,iz)
    wk06(:,:,iz) = wk06(:,:,iz)*r(:,:,iz)
    wk02(:,:,iz) = wk02(:,:,iz)*r(:,:,iz)
  end do

!-----------------------------------------------------------------------
!  Mass conservation, cell centered
!-----------------------------------------------------------------------
if (do_density) then

  if (nur==0) then
    if (do_2nddiv) then
      call ddxup1_sub (px, drdt)
      call ddyup1_sub (py, drdt)
      call ddzup1_sub (pz, drdt)
    else
      call ddxup_sub (px, drdt)
      call ddyup_sub (py, drdt)
      call ddzup_sub (pz, drdt)
    end if
    call dumpl (drdt, 'drdt', '2nddiv.dmp', 0)
  else
                       if(do_trace) print *,'drdt ',omp_mythread
!
    call ddxdn_set (wk08, scr1)
    call xdn_set (wk03, scr2)
    if (do_quench) then
      call quenchx (scr1, scr3)
      do iz=izs,ize
      do iy=1,my
      call expn (mx, scr2(:,iy,iz), exp1)
      do ix=1,mx
        scr1(ix,iy,iz) = -px(ix,iy,iz)+(nur*dxmdn(ix))*exp1(ix)*scr3(ix,iy,iz)
      end do
      end do
      end do
    else
      do iz=izs,ize
      do iy=1,my
      call expn (mx, scr2(:,iy,iz), exp1)
      do ix=1,mx
        scr1(ix,iy,iz) = -px(ix,iy,iz)+(nur*dxmdn(ix))*exp1(ix)*scr1(ix,iy,iz)
      end do
      end do
      end do
    end if

    if (do_2nddiv) then
      call ddxup1_add (scr1, drdt)
    else
      call ddxup_add (scr1, drdt)
    end if
    call dumpl (drdt, 'drdt', '2nddiv.dmp', 0)
!
    call ddydn_set (wk08, scr1)
    call ydn_set (wk03, scr2)
    if (do_quench) then
      call quenchy (scr1, scr3)
      do iz=izs,ize
      do iy=1,my
      call expn (mx, scr2(:,iy,iz), exp1)
      do ix=1,mx
        scr1(ix,iy,iz) = -py(ix,iy,iz)+(nur*dymdn(iy))*exp1(ix)*scr3(ix,iy,iz)
      end do
      end do
      end do
    else
      do iz=izs,ize
      do iy=1,my
      call expn (mx, scr2(:,iy,iz), exp1)
      do ix=1,mx
        scr1(ix,iy,iz) = -py(ix,iy,iz)+(nur*dymdn(iy))*exp1(ix)*scr1(ix,iy,iz)
      end do
      end do
      end do
    end if

    if (do_2nddiv) then
      call ddyup1_add (scr1, drdt)
    else
      call ddyup_add (scr1, drdt)
    end if
    call dumpl (drdt, 'drdt', '2nddiv.dmp', 1)
!
    call ddzdn_set (wk08, scr1)
    call barrier_omp('pde9')                                            ! barrier 5, needed because wk03 & scr1 just computed
    if (do_quench) then
      call quenchz (scr1, scr3, scr2)
      call barrier_omp('pde10')                                         ! barrier 6, needed only because scr2 overlaps (quench)
      call zdn_set (wk03, scr2)
      do iz=izs,ize
      do iy=1,my
      call expn (mx, scr2(:,iy,iz), exp1)
      do ix=1,mx
        scr1(ix,iy,iz) = -pz(ix,iy,iz)+(nur*dzmdn(iz))*exp1(ix)*scr3(ix,iy,iz)
      end do
      end do
      end do
    else
      call zdn_set (wk03, scr2)
      do iz=izs,ize
      do iy=1,my
      call expn (mx, scr2(:,iy,iz), exp1)
      do ix=1,mx
        scr1(ix,iy,iz) = -pz(ix,iy,iz)+(nur*dzmdn(iz))*exp1(ix)*scr1(ix,iy,iz)
      end do
      end do
      end do
    end if

    call barrier_omp('pde11')                                           ! barrier 7, needed because scr1 modified since last barrier
    if (do_2nddiv) then
      call ddzup1_add (scr1, drdt)
    else
      call ddzup_add (scr1, drdt)
    end if
    call dumpl (drdt, 'drdt', '2nddiv.dmp', 2)

  end if
  call barrier_omp('pde12')                                             ! barrier 7a, needed because scr1 modified later on!!!
end if ! (do_density)
                       if(do_trace) print *,'end drdt ',omp_mythread
  END

!***********************************************************************
  SUBROUTINE pde2 (flag,r,px,py,pz,e,d,Bx,By,Bz,drdt,dpxdt,dpydt,dpzdt,dedt,dddt,dBxdt,dBydt,dBzdt, &
             Ux,Uy,Uz,Ex,Ey,Ez, &
             wk01,wk02,wk03,wk04,wk05,wk06,wk07,wk08,wk09, &
             wk10,wk11,wk12,wk13,wk14,wk15,wk16,wk17, &
             scr1,scr2,scr3,scr4,scr5,scr6)
  USE params
  !USE variables
  USE arrays, only: eeav, dpxav, dpzav
  implicit none
  real, dimension(mx,my,mz):: &
             r,px,py,pz,e,d,Bx,By,Bz,drdt,dpxdt,dpydt,dpzdt,dedt,dddt,dBxdt,dBydt,dBzdt, &
             Ux,Uy,Uz,Ex,Ey,Ez, &
             wk01,wk02,wk03,wk04,wk05,wk06,wk07,wk08,wk09, &
             wk10,wk11,wk12,wk13,wk14,wk15,wk16,wk17, &
             scr1,scr2,scr3,scr4,scr5,scr6
  integer iz, iy, ix
  real void, dtime, cput(2)
  real, dimension(mx):: nuxy, nuyz, nuzx, exp1
  logical flag, debug

  if(do_trace) print *,'pde2',omp_mythread
!-----------------------------------------------------------------------
!  Quenching operator
!-----------------------------------------------------------------------

                       if (debug(dbg_hd) .and. omp_master) then; void = dtime(cput)
		       print '(1x,a5,2f7.3)','pde2a',cput*omp_nthreads; endif

  if (do_quench) then
    call barrier_omp('pde13')                                           ! barrier 8, needed because scr1 is input to ddzup above (quench)
    call quenchx (wk13, scr1)
    call quenchz (wk15, scr3, scr2)
    call barrier_omp('pde14')                                           ! barrier 9, needed only because scr2 overlaps (quench)
    call quenchy (wk14, scr2)
  end if
                       if (debug(dbg_hd) .and. omp_master) then; void = dtime(cput)
		       print '(1x,a5,2f7.3)','pde2b',cput*omp_nthreads; endif

!-----------------------------------------------------------------------
!  Viscous stress, diagonal and off-diagonal elements
!-----------------------------------------------------------------------
    call dump (wk02, 'dpxdt.dmp', 4)
    call dumpl (dedt,'begin','dedt.dmp',0)
  do iz=izs,ize
  do iy=1,my
    if (do_quench) then
      do ix=1,mx
        wk07(ix,iy,iz) = - (2.*dxm(ix))*wk06(ix,iy,iz)*scr1(ix,iy,iz) + wk04(ix,iy,iz)
        wk08(ix,iy,iz) = - (2.*dym(iy))*wk06(ix,iy,iz)*scr2(ix,iy,iz) + wk04(ix,iy,iz)
        wk09(ix,iy,iz) = - (2.*dzm(iz))*wk06(ix,iy,iz)*scr3(ix,iy,iz) + wk04(ix,iy,iz)
      end do
    else
      do ix=1,mx
        wk07(ix,iy,iz) = - (2.*dxm(ix))*(wk06(ix,iy,iz)+dxm(ix)*wk02(ix,iy,iz))*wk13(ix,iy,iz) + wk04(ix,iy,iz)
        wk08(ix,iy,iz) = - (2.*dym(iy))*(wk06(ix,iy,iz)+dym(iy)*wk02(ix,iy,iz))*wk14(ix,iy,iz) + wk04(ix,iy,iz)
        wk09(ix,iy,iz) = - (2.*dzm(iz))*(wk06(ix,iy,iz)+dzm(iz)*wk02(ix,iy,iz))*wk15(ix,iy,iz) + wk04(ix,iy,iz)
      end do
    end if
    if (do_energy) then
      if (do_dissipation) then 
        do ix=1,mx
          dedt(ix,iy,iz) = dedt(ix,iy,iz) &
                     - wk07(ix,iy,iz)*wk13(ix,iy,iz) &
                     - wk08(ix,iy,iz)*wk14(ix,iy,iz) &
                     - wk09(ix,iy,iz)*wk15(ix,iy,iz)
        end do
      else
        do ix=1,mx
          dedt(ix,iy,iz) = dedt(ix,iy,iz) - wk04(ix,iy,iz)* &
                       (wk13(ix,iy,iz) + wk14(ix,iy,iz) + wk15(ix,iy,iz))
        end do
      end if
    end if
  end do
  end do
    call dumpl (dedt,'visc','dedt.dmp',1)
    call dump (wk07, 'dpxdt.dmp', 5)
                       if (debug(dbg_hd) .and. omp_master) then; void = dtime(cput)
		       print '(1x,a5,2f7.3)','diag ',cput*omp_nthreads; endif
  do iz=izs,ize
  do iy=1,my
  do ix=1,mx
    wk03(ix,iy,iz) = alog(wk06(ix,iy,iz)+0.5*(dxm(ix)+dym(iy))*wk02(ix,iy,iz))
  end do
  end do
  end do
  call xdn_set (wk03,scr1)
  call ydn_set (scr1,scr2)

  do iz=izs,ize
  do iy=1,my
  do ix=1,mx
    wk03(ix,iy,iz) = alog(wk06(ix,iy,iz)+0.5*(dxm(ix)+dzm(iz))*wk02(ix,iy,iz))
  end do
  end do
  end do
  call xdn_set (wk03,scr1)

  call barrier_omp('pde15')                                             ! barrier 10, scr1
  call zdn_set (scr1,scr3)

  do iz=izs,ize
  do iy=1,my
  do ix=1,mx
    wk03(ix,iy,iz) = alog(wk06(ix,iy,iz)+0.5*(dym(iy)+dzm(iz))*wk02(ix,iy,iz))
  end do
  end do
  end do
  call ydn_set (wk03,scr5)
  call barrier_omp('pde16')                                             ! barrier 11, scr5
  call zdn_set (scr5,scr1)
                       if (debug(dbg_hd) .and. omp_master) then; void = dtime(cput)
		       print '(1x,a5,2f7.3)','pde2d',cput*omp_nthreads; endif

  if (do_quench) then
    call barrier_omp('pde17')                                           ! barrier 11, wk11 (quench)
    call quenchyz (wk11, scr5, scr4)
    call barrier_omp('pde18')                                           ! barrier 12, scr4 (quench)
    call quenchzx (wk12, scr6, scr4)
    call barrier_omp('pde19')                                           ! barrier 13, scr4 (quench)
    call quenchxy (wk10, scr4)
                       if (debug(dbg_hd) .and. omp_master) then; void = dtime(cput)
		       print '(1x,a5,2f7.3)','pde2e',cput*omp_nthreads; endif
    do iz=izs,ize
    do iy=1,my
      call expn (mx, scr2(:,iy,iz), nuxy)
      call expn (mx, scr1(:,iy,iz), nuyz)
      call expn (mx, scr3(:,iy,iz), nuzx)
      do ix=1,mx
        wk13(ix,iy,iz) = - (dxmdn(ix)+dymdn(iy))*nuxy(ix)*scr4(ix,iy,iz)
        wk14(ix,iy,iz) = - (dymdn(iy)+dzmdn(iz))*nuyz(ix)*scr5(ix,iy,iz)
        wk15(ix,iy,iz) = - (dzmdn(iz)+dxmdn(ix))*nuzx(ix)*scr6(ix,iy,iz)
      end do
    end do
    end do
  else
    do iz=izs,ize
    do iy=1,my
      call expn (mx, scr2(:,iy,iz), nuxy)
      call expn (mx, scr1(:,iy,iz), nuyz)
      call expn (mx, scr3(:,iy,iz), nuzx)
      do ix=1,mx
        wk13(ix,iy,iz) = - (dxmdn(ix)+dymdn(iy))*nuxy(ix)*wk10(ix,iy,iz)
        wk14(ix,iy,iz) = - (dymdn(iy)+dzmdn(iz))*nuyz(ix)*wk11(ix,iy,iz)
        wk15(ix,iy,iz) = - (dzmdn(iz)+dxmdn(ix))*nuzx(ix)*wk12(ix,iy,iz)
      end do
    end do
    end do
  end if

!-----------------------------------------------------------------------
!  Compensate for net horizontal viscous drag
!-----------------------------------------------------------------------
  if (do_nodrag .and. abs(omegax)+abs(omegay)+abs(omegaz) > 0.) then
    call ddyup_set (wk13, scr1)
    call ddyup_set (wk14, scr2)
    call haverage_subr (scr1, dpxav)
    call haverage_subr (scr2, dpzav)
    do iz=izs,ize
      do iy=1,my
        dpxdt(:,iy,iz) = dpxdt(:,iy,iz) + dpxav(iy)
        dpzdt(:,iy,iz) = dpzdt(:,iy,iz) + dpzav(iy)
      end do
    end do
  end if
                       if (debug(dbg_hd) .and. omp_master) then; void = dtime(cput)
		       print '(1x,a5,2f7.3)','pde2e',cput*omp_nthreads; endif

  if (do_energy .and. do_dissipation) then
    call xup_set (wk10, scr1)
    call yup_set (scr1, wk10)
    call yup_set (wk11, scr1)
    call barrier_omp('pde20')                                           ! barrier 14, scr1
    call zup_set (scr1, wk11)
    call zup_set (wk12, scr2)
    call barrier_omp('pde21')                                           ! barrier 15, scr2
    call xup_set (scr2, wk12)

    do iz=izs,ize
    do iy=1,my
    do ix=1,mx
      dedt(ix,iy,iz) = dedt(ix,iy,iz) &
      + (dxm(ix)+dym(iy))*wk06(ix,iy,iz)*wk10(ix,iy,iz)**2 &
      + (dym(iy)+dzm(iz))*wk06(ix,iy,iz)*wk11(ix,iy,iz)**2 &
      + (dzm(iz)+dxm(ix))*wk06(ix,iy,iz)*wk12(ix,iy,iz)**2 
    end do
    end do
    end do
      call dumpl (dedt,'offdiag','dedt.dmp',2)
  end if
                       if (debug(dbg_hd) .and. omp_master) then; void = dtime(cput)
		       print '(1x,a5,2f7.3)','pde2f',cput*omp_nthreads; endif
!-----------------------------------------------------------------------
!  Add Reynolds stress
!-----------------------------------------------------------------------
  call xup_set (Ux,scr1)
  call yup_set (Uy,scr2)
  call zup_set (Uz,scr3)
    call dump (wk07, 'dpxdt.dmp', 6)
  do iz=izs,ize
    wk07(:,:,iz) = wk07(:,:,iz) + r(:,:,iz)*scr1(:,:,iz)**2
    wk08(:,:,iz) = wk08(:,:,iz) + r(:,:,iz)*scr2(:,:,iz)**2
    wk09(:,:,iz) = wk09(:,:,iz) + r(:,:,iz)*scr3(:,:,iz)**2
  end do
    call dump (scr1, 'dpxdt.dmp', 7)
    call dump (wk07, 'dpxdt.dmp', 8)

  call xdn_set (wk17, scr1)
  call xdn_set (Uy, scr2)
  call ydn_set (Ux, scr3)
  do iz=izs,ize
  do iy=1,my
    call expn (mx, scr1(:,iy,iz), exp1)
    wk13(:,iy,iz) = wk13(:,iy,iz) + exp1*scr3(:,iy,iz)*scr2(:,iy,iz)
  end do
  end do


  call ydn_set (wk01, scr1)
  call ydn_set (Uz, scr2)
  call zdn_set (Uy, scr3)
  do iz=izs,ize
  do iy=1,my
    call expn (mx, scr1(:,iy,iz), exp1)
    wk14(:,iy,iz) = wk14(:,iy,iz) + exp1*scr3(:,iy,iz)*scr2(:,iy,iz)
  end do
  end do

  call zdn_set (wk16, scr1)
  call zdn_set (Ux, scr2)
  call xdn_set (Uz, scr3)
  do iz=izs,ize
  do iy=1,my
    call expn (mx, scr1(:,iy,iz), exp1)
    wk15(:,iy,iz) = wk15(:,iy,iz) + exp1*scr3(:,iy,iz)*scr2(:,iy,iz)
  end do
  end do
                       if (debug(dbg_hd) .and. omp_master) then; void = dtime(cput)
		       print '(1x,a5,2f7.3)','pde2a',cput*omp_nthreads; endif


!-----------------------------------------------------------------------
!  Equations of motion, face centred
!-----------------------------------------------------------------------
  call barrier_omp('pde22')                                             ! needed because wk15 just computed
  if (do_2nddiv) then
    call ddxdn1_set (wk07, scr1)
    call ddyup1_set (wk13, scr2)
    call ddzup1_set (wk15, scr3)
  else
    call ddxdn_set (wk07, scr1)
    call ddyup_set (wk13, scr2)
    call ddzup_set (wk15, scr3)
  end if
  do iz=izs,ize
    dpxdt(:,:,iz) = dpxdt(:,:,iz) - scr1(:,:,iz) - scr2(:,:,iz) - scr3(:,:,iz)
  end do
    call dump (scr1, 'dpxdt.dmp', 9)
    call dump (scr2, 'dpxdt.dmp', 10)
    call dump (scr3, 'dpxdt.dmp', 11)
    call dump (dpxdt, 'dpxdt.dmp', 12)

  if (do_2nddiv) then
    call ddydn1_set (wk08, scr1)
    call ddzup1_set (wk14, scr2)
    call ddxup1_set (wk13, scr3)
  else
    call ddydn_set (wk08, scr1)
    call ddzup_set (wk14, scr2)
    call ddxup_set (wk13, scr3)
  end if
    call dumpl(dpydt,'dpydt','dpydt.dmp',0)
  do iz=izs,ize
    dpydt(:,:,iz) = dpydt(:,:,iz) - scr1(:,:,iz) - scr2(:,:,iz) - scr3(:,:,iz)
  end do
    call dumpl(scr1,'dpydt','dpydt.dmp',1)
    call dumpl(scr2,'dpydt','dpydt.dmp',2)
    call dumpl(scr3,'dpydt','dpydt.dmp',3)

  if (do_2nddiv) then
    call ddzdn1_set (wk09, scr1)
    call ddxup1_set (wk15, scr2)
    call ddyup1_set (wk14, scr3)
  else
    call ddzdn_set (wk09, scr1)
    call ddxup_set (wk15, scr2)
    call ddyup_set (wk14, scr3)
  end if
  do iz=izs,ize
    dpzdt(:,:,iz) = dpzdt(:,:,iz) - scr1(:,:,iz) - scr2(:,:,iz) - scr3(:,:,iz)
    scr5(:,:,iz) = alog(wk05(:,:,iz))
  end do

                       if (debug(dbg_hd) .and. omp_master) then; void = dtime(cput)
		       print '(1x,a5,2f7.3)','eqofm',cput*omp_nthreads; endif
  END

!***********************************************************************
  SUBROUTINE pde3 (flag,r,px,py,pz,e,d,Bx,By,Bz,drdt,dpxdt,dpydt,dpzdt,dedt,dddt,dBxdt,dBydt,dBzdt, &
             Ux,Uy,Uz,Ex,Ey,Ez, &
             wk01,wk02,wk03,wk04,wk05,wk06,wk07,wk08,wk09, &
             wk10,wk11,wk12,wk13,wk14,wk15,wk16,wk17, &
             scr1,scr2,scr3,scr4,scr5,scr6)
  USE params
  !USE variables
  USE arrays, only: eeav, dpxav, dpzav
  implicit none
  real, dimension(mx,my,mz):: &
             r,px,py,pz,e,d,Bx,By,Bz,drdt,dpxdt,dpydt,dpzdt,dedt,dddt,dBxdt,dBydt,dBzdt, &
             Ux,Uy,Uz,Ex,Ey,Ez, &
             wk01,wk02,wk03,wk04,wk05,wk06,wk07,wk08,wk09, &
             wk10,wk11,wk12,wk13,wk14,wk15,wk16,wk17, &
             scr1,scr2,scr3,scr4,scr5,scr6
  integer iz, iy, ix
  real, dimension(mx):: exp1, exp2
  logical debug, flag
  real cput(2), void, dtime

  if(do_trace) print *,'pde3',omp_mythread

  do iz=izs,ize
  do iy=1,my
  do ix=1,mx
    wk03(ix,iy,iz) = alog(wk06(ix,iy,iz)+dxm(ix)*wk02(ix,iy,iz))
  end do
  end do
  end do

  call xdn_set (wk03, scr1)
  call xdn_set (scr5, scr6)
  call ddxdn_set (scr5, scr3)
  if (do_quench) then
    call quenchx (scr3, scr2)
    do iz=izs,ize
    do iy=1,my
    call expn (mx, scr6(:,iy,iz), exp1)
    call expn (mx, scr1(:,iy,iz), exp2)
    do ix=1,mx
      scr3(ix,iy,iz) = exp1(ix)*(px(ix,iy,iz) - dxmdn(ix)*exp2(ix)*scr2(ix,iy,iz))
    end do
    end do
    end do
  else
    do iz=izs,ize
    do iy=1,my
    call expn (mx, scr6(:,iy,iz), exp1)
    call expn (mx, scr1(:,iy,iz), exp2)
    do ix=1,mx
      scr3(ix,iy,iz) = exp1(ix)*(px(ix,iy,iz) - dxmdn(ix)*exp2(ix)*scr3(ix,iy,iz))
    end do
    end do
    end do
  end if

  if (do_2nddiv) then
    call ddxup1_set (scr3, scr1)
  else
    call ddxup_set (scr3, scr1)
  end if
  call dumpl (scr1, 'scr1', '2nddiv.dmp', 3)
  do iz=izs,ize
    dedt(:,:,iz) = dedt(:,:,iz) - scr1(:,:,iz)
  end do
    call dumpl (dedt,'x-transp','dedt.dmp',3)

  do iz=izs,ize
  do iy=1,my
  do ix=1,mx
    wk03(ix,iy,iz) = alog(wk06(ix,iy,iz)+dym(iy)*wk02(ix,iy,iz))
  end do
  end do
  end do

  call ydn_set (wk03, scr1)
  call ydn_set (scr5, scr2)

!-----------------------------------------------------------------------
!  In cases with boundaries, diffuse energy fluctuations relative to the
!  horizontal mean.
!-----------------------------------------------------------------------
  if (do_stratified) then
    call haverage_subr (wk05, eeav)
    do iz=izs,ize
      do iy=1,my
        scr6(:,iy,iz) = scr5(:,iy,iz)-log(eeav(iy))
      end do
    end do
    call ddydn_set (scr6, scr3)
  else
    call ddydn_set (scr5, scr3)
  end if

  if (do_quench) then
    call quenchy (scr3, scr6)
    do iz=izs,ize
    do iy=1,my
    call expn (mx, scr2(:,iy,iz), exp1)
    call expn (mx, scr1(:,iy,iz), exp2)
    do ix=1,mx
      scr3(ix,iy,iz) = exp1(ix)*(py(ix,iy,iz) - dymdn(iy)*exp2(ix)*scr6(ix,iy,iz))
    end do
    end do
    end do
  else
    do iz=izs,ize
    do iy=1,my
    call expn (mx, scr2(:,iy,iz), exp1)
    call expn (mx, scr1(:,iy,iz), exp2)
    do ix=1,mx
      scr3(ix,iy,iz) = exp1(ix)*(py(ix,iy,iz) - dymdn(iy)*exp2(ix)*scr3(ix,iy,iz))
    end do
    end do
    end do
  end if

  if (do_2nddiv) then
    call ddyup1_set (scr3, scr1)
  else
    call ddyup_set (scr3, scr1)
  end if
  call dumpl (scr1, 'scr1', '2nddiv.dmp', 4)
  do iz=izs,ize
    dedt(:,:,iz) = dedt(:,:,iz) - scr1(:,:,iz)
  end do
    call dumpl (dedt,'y-transp','dedt.dmp',4)

  do iz=izs,ize
  do iy=1,my
  do ix=1,mx
    wk03(ix,iy,iz) = alog(wk06(ix,iy,iz)+dzm(iz)*wk02(ix,iy,iz))
  end do
  end do
  end do

  call barrier_omp('pde23')                                             ! barrier 17, scr5??? (quench)
  call ddzdn_set (scr5, scr3)
  if (do_quench) then
    call barrier_omp('pde24')                                           ! barrier 18, src3 (quench)
    call quenchz (scr3, scr6, scr1)
    call barrier_omp('pde25')                                           ! barrier 19, scr1 (eliminated with more scratch)
    call zdn_set (wk03, scr1)
    call zdn_set (scr5, scr2)
    do iz=izs,ize
    do iy=1,my
    call expn (mx, scr2(:,iy,iz), exp1)
    call expn (mx, scr1(:,iy,iz), exp2)
    do ix=1,mx
      scr3(ix,iy,iz) = exp1(ix)*(pz(ix,iy,iz) - dzmdn(iz)*exp2(ix)*scr6(ix,iy,iz))
    end do
    end do
    end do
  else
    call zdn_set (wk03, scr1)
    call zdn_set (scr5, scr2)
    do iz=izs,ize
    do iy=1,my
    call expn (mx, scr2(:,iy,iz), exp1)
    call expn (mx, scr1(:,iy,iz), exp2)
    do ix=1,mx
      scr3(ix,iy,iz) = exp1(ix)*(pz(ix,iy,iz) - dzmdn(iz)*exp2(ix)*scr3(ix,iy,iz))
    end do
    end do
    end do
  end if

  call barrier_omp('pde26')                                             ! barrier 20, scr3 set above
  if (do_2nddiv) then
    call ddzup1_set (scr3, scr1)
  else
    call ddzup_set (scr3, scr1)
  end if
  call dumpl (scr1, 'scr1', '2nddiv.dmp', 5)

  do iz=izs,ize
    dedt(:,:,iz) = dedt(:,:,iz) - scr1(:,:,iz)
  end do
    call dumpl (dedt,'z-transp','dedt.dmp',5)

                       if (debug(dbg_hd) .and. omp_master) then; void = dtime(cput)
		       print '(1x,a5,2f7.3)','energ',cput*omp_nthreads; endif

!-----------------------------------------------------------------------
!  Cooling and conduction
!-----------------------------------------------------------------------
  call barrier_omp('pde27')                                             ! needed??
  call coolit (r, wk05, scr5, scr4, dedt)
    call dumpl (dedt,'coolit','dedt.dmp',6)

                       if (debug(dbg_hd) .and. omp_master) then; void = dtime(cput)
		       print '(1x,a5,2f7.3)','radtr',cput*omp_nthreads; endif

!-----------------------------------------------------------------------
!  Passive scalar
!-----------------------------------------------------------------------
  if (do_pscalar) then
    do iz=izs,ize
      wk07(:,:,iz) = d(:,:,iz)/r(:,:,iz)
    end do
    call passive (wk06,wk03,r,px,py,pz,Ux,Uy,Uz,wk07,dddt)
  end if
  call trace_particles (Ux,Uy,Uz)

!-----------------------------------------------------------------------
!  MHD
!-----------------------------------------------------------------------
  if (do_mhd) then
    call mhd (flag, &
            r,e,Ux,Uy,Uz,Bx,By,Bz,Ex,Ey,Ez, &
	    dpxdt,dpydt,dpzdt,dedt,dBxdt,dBydt,dBzdt)
    call dumpl(dpydt,'mhd+','dpydt.dmp',4)
    call dumpl (dedt,'mhd','dedt.dmp',7)
  end if
  !scr4-wk03 free

!-----------------------------------------------------------------------
!  Boundary conditions on time derivatives
!-----------------------------------------------------------------------
  call ddt_boundary (r,px,py,pz,e,Bx,By,Bz, &
                     drdt,dpxdt,dpydt,dpzdt, &
                     dedt,dBxdt,dBydt,dBzdt, &
                     Ux,Uy,Uz,wk08,wk04,wk05,wk07)
    call dump (dpxdt, 'dpxdt.dmp', 13)
    call dumpl(dpydt,'bdr+','dpydt.dmp',5)
    call dumpl(dedt,'ddt-bdry','dedt.dmp',8)

                       if (debug(dbg_hd) .and. omp_master) then; void = dtime(cput)
		       print '(1x,a5,2f7.3)','ddtbd',cput*omp_nthreads; endif

  END
