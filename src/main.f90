!................................................................................
PROGRAM stagger_code

  !  Main program for the stagger code

  USE params
  USE variables

  implicit none

  integer mxyz, iz, lrec, i, iv
  real(kind=8) t0,trun,nc
  real cput1(2), cput2(2), cputot(2,2)
  real etime, etm, kzs, wallclock, wct, void, utm
  real wct0, wct1, dwct, swct, nswct
  character(len=mid):: id = 'main'

  hl = repeat('-',72)

  call init_stdio
  call init_mpi
  call print_id(id)

  if (master) print *,hl; call init_params
  call open_checkfile                           ! open file for checks
  if (master) print *,hl; call init_omp
  if (master) print *,hl; call init_arrays
  if (master) print *,hl; call init_boundary
  if (master) print *,hl; call init_mesh
  if (master) print *,hl; call init_slice
  if (master) print *,hl; call init_metric
  if (master) print *,hl; call init_timestep
  if (master) print *,hl; call init_quench
  if (master) print *,hl; call init_eos
  if (master) print *,hl; call init_io
  !if (master) print *,hl; call test_stagger
  if (master) print *,hl; call init_particles
  if (master) print *,hl; call init_pde

  if (master) print *,hl
  if (iread .ge. 0) then
     call read_snap (r,px,py,pz,e,d,Bx,By,Bz)
     call read_particles (iread)
     if (master) print *,hl                     ! bar
     call init_force                            ! may need time info for force init
  else
     t=0.                                       ! initialize time before force
     call init_force                            ! may need force params for init vals
     call init_values (r,px,py,pz,e,d,Bx,By,Bz)
     isnap = isnap+1                            ! snapshot number
     isnap0  = 1                                ! snapshot offset
     iscr0  = 1                                 ! snapshot offset
     if (nsnap > 0 .or. tsnap > 0.) then        ! write initial state, if at all writing
        call write_snap (r,px,py,pz,e,d,Bx,By,Bz)
        call write_particles (isnap)
     end if
  end if
  t0 = t
  trun = 0.
  if (master) print *,'isnap:',isnap,isnap0,t,tsnap,tsnap*(isnap+isnap0)
  if (master) print *,hl; call init_cooling (r,e)
  if (master) print *,hl; call init_explode (r)
  if (master) print *,hl
  call close_checkfile                          ! open file for checks
  call oflush                                   ! flush stdout

  !.............................................................................
  !  Time step loop
  !.............................................................................
  it = 1
  dtold = 1e30
  td = t
  wct0 = wallclock()
  do while (it <= nstep .and. t < tstop)        ! loop ...
     call timer('PRINT','')
     call open_checkfile                        ! open file for checks
     if (it.le.2) etm = etime(cputot(:,1))      ! skip first step for average
     if (it.le.2) wct = wallclock()             ! skip first step for average

     void = etime(cput1)                        ! for speed measurement
     call explode (r,px,py,pz,e,d,Bx,By,Bz)     ! supernovae etc
     call timestep( &                           ! step the variables
          r,px,py,pz,e,d, &
          drdt,dpxdt,dpydt,dpzdt,dedt,dddt, &
          Bx,By,Bz,dBxdt,dBydt,dBzdt)

     void = etime(cput2)                        ! speed measurement
     cput2 = cput2-cput1
     call print_time (cput2)                    ! print time step and speed info

     call check_io (r,px,py,pz,e,d,Bx,By,Bz)    ! I/O check
     call timer('check_io','')
     call check_slice (r,px,py,pz,e,d,Bx,By,Bz) ! slice check
     call check_params                          ! possibly read new parameters
     call timer('check_params','')
     call oflush                                ! generic output flush
     call close_checkfile                       ! close file for checks
     call timer('close_checkfile','')
     trun = trun+dt                             ! increment time for this run
     t = t0 + trun                              ! make sure the total time remains exact

     wct1 = wallclock()
     dwct = wct1-wct0                           ! delta wall clock
     wct0 = wct1
     if (it <= 2) then                          ! the second time
        swct = dwct
        nswct = 1
     else if (dwct < 1.1*swct/nswct) then       ! and then if not excessive
        swct = swct + dwct
        nswct = nswct + 1
     end if

     it = it+1                                  ! increment time step
  end do
  call timer('END','')
  etm = etime(cputot(:,2))-etm                  ! total time for the run
  utm = sum(cputot(:,2)-cputot(:,1))            ! CPU time
  wct = wallclock()-wct                         ! total wall clock time


  !................................................................................
  !  Speed summary
  !................................................................................
  call barrier_mpi('summary')
  call sum_int_mpi (n_trnslt)
  call sum_int_mpi (n_reshape)
  call sum_int_mpi (n_deshape)
  call sum_int_mpi (n_lookup)
  call sum_int_mpi (n_transfer)

  if (master) then
     it = it-1
     print '(X,A)', &
          '        usr         sys        wallt        mus/pt' // &
          '    wmus/pt      smus/pt       barriers      stagger'
     print '(1x,f12.1,f12.2,f12.1,f12.2,f12.3,f12.3,4i10)', &
          cputot(1,2)-cputot(1,1), &
          cputot(2,2)-cputot(2,1), &
          wct, &
          (utm*1e6)/(max(it-1,1)*mw), &
          (wct*1e6)/(max(it-1,1)*mw), &
          (swct*1e6)/(nswct*mw), &
          n_barrier/(max(it,1)*timeorder), &
          nstag, nstag1, nflop

     nc = n_omega * real(mxtot) * real(mytot) * real(mztot)
     if (nc .eq. 0) then
        !nc information unavailable: flag with negative value!
        nc = -1
        n_trnslt = 1
        n_reshape = 1
        n_deshape = 1
        n_lookup = 1
        n_transfer = 1
     end if
     print '(X,A,X,F6.2)', 'n_trnslt   =', real(n_trnslt) / real(nc)
     print '(X,A,X,F6.2)', 'n_reshape  =', real(n_reshape) / real(nc)
     print '(X,A,X,F6.2)', 'n_deshape  =', real(n_deshape) / real(nc)
     print '(X,A,X,F6.2)', 'n_lookup   =', real(n_lookup)  / real(nc)
     print '(X,A,X,F6.2)', 'n_transfer =', real(n_transfer) / real(nc)

  end if

  call end_mpi

END PROGRAM stagger_code
