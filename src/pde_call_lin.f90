! $Id: pde_call_lin.f90,v 1.54 2012/12/31 16:19:14 aake Exp $
!***********************************************************************
  SUBROUTINE pde_lin(r,px,py,pz,e,d,drdt,dpxdt,dpydt,dpzdt,dedt,dddt,flag, &
                     Bx,By,Bz,dBxdt,dBydt,dBzdt)

  USE params
  implicit none

  real ds, gam1, average, fmaxval, soundsp, velocity, fCv
  integer ix, iy, iz

  integer j
  real elapsed

  logical flag
  real, dimension(mx,my,mz) :: &
       r,px,py,pz,e,d,drdt,dpxdt,dpydt,dpzdt,dedt,dddt, &
       Bx,By,Bz,dBxdt,dBydt,dBzdt
  real, allocatable, dimension(:,:,:) :: &
       lnr,dd,lnu,P,Cs,divU,nu,lne,ee,Ux,Uy,Uz, &
       Sxx,Syy,Szz,Sxy,Szx,Syz,xdnr,ydnr,zdnr, &
       Txx,Tyy,Tzz,Txy,Tzx,Tyz,ddxd,ddyd,ddzd
  real, allocatable, dimension(:,:,:) :: &
    scr1, scr2, scr3, &
    xydnnu, xzdnnu, yzdnnu, &
    TSxy, TSyz, TSzx, &
    xupUx, yupUy, zupUz, &
    xydnr, xdnUy, ydnUx, &
    yzdnr, ydnUz, zdnUy, &
    zxdnr, zdnUx, xdnUz, &
    ddxdnUy, ddzdnUy, &
    ddydnUz, ddxdnUz, &
    ddzdnUx, ddydnUx, &
    ddxdnTxx, ddyupTxy, ddzupTzx, &
    ddydnTyy, ddzupTyz, ddxupTxy, &
    ddzdnTzz, ddxupTzx, ddyupTyz, &
    eflux, &
    xdnnu, xdne, ddxdne, &
    ydnnu, ydne, ddydne, &
    zdnnu, zdne, ddzdne, &
    ddxuppx, ddyuppy, ddzuppz
  real tm(30), cput(2)
  character(len=mid) id
  data id/'$Id: pde_call_lin.f90,v 1.54 2012/12/31 16:19:14 aake Exp $'/

  if (id .ne. '') print *,id
  id = ''
  if (idbg>1) then; j=1;tm(j)=elapsed(); end if

!-----------------------------------------------------------------------
!  Velocities, pressure
!-----------------------------------------------------------------------
  allocate(xdnr(mx,my,mz),ydnr(mx,my,mz),zdnr(mx,my,mz))  !  4 chunks
  call density_boundary(r)
  call xdn_set (r, xdnr)
  call ydn_set (r, ydnr)
  call zdn_set (r, zdnr)
  allocate(Ux(mx,my,mz),Uy(mx,my,mz),Uz(mx,my,mz))                      !  6 chunks        
!$omp parallel do private(iz)
  do iz=1,mz
    Ux(:,:,iz) = px(:,:,iz)/xdnr(:,:,iz)
    Uy(:,:,iz) = py(:,:,iz)/ydnr(:,:,iz)
    Uz(:,:,iz) = pz(:,:,iz)/zdnr(:,:,iz)
  end do
  call velocity_boundary(r,xdnr,ydnr,zdnr,Ux,Uy,Uz,px,py,pz,e,Bx,By,Bz)
  if (idbg>1) then; j=j+1;tm(j)=elapsed(); print '(a4,2f7.3)',' Ui',tm(j)-tm(j-1),tm(j)-tm(1); endif
  call forceit (r,Ux,Uy,Uz,xdnr,ydnr,zdnr,dpxdt,dpydt,dpzdt)
  if (idbg>1) then; j=j+1;tm(j)=elapsed(); print '(a4,2f7.3)','frc',tm(j)-tm(j-1),tm(j)-tm(1); endif

  allocate(Sxx(mx,my,mz),Syy(mx,my,mz),Szz(mx,my,mz))                   !  9 chunks
  call ddxup_set (Ux, Sxx)
  call ddyup_set (Uy, Syy)
  call ddzup_set (Uz, Szz)

!-----------------------------------------------------------------------
!  First part of viscosity
!-----------------------------------------------------------------------
  allocate (P(mx,my,mz),Cs(mx,my,mz),nu(mx,my,mz))                      ! 12 chunks
!$omp parallel do private(iz)
  do iz=1,mz
    nu(:,:,iz) = Sxx(:,:,iz)+Syy(:,:,iz)+Szz(:,:,iz)
    nu(:,:,iz) = max(-nu(:,:,iz),0.)
  end do
  if (gamma .eq. 1.) then
    gam1 = 1.
  else
    gam1 = gamma-1.
  end if
  allocate (scr1(mx,my,mz), scr2(mx,my,mz), scr3(mx,my,mz))
  call xup_set (Ux, scr1)
  call yup_set (Uy, scr2)
  call zup_set (Uz, scr3)
!$omp parallel do private(iz)
  do iz=1,mz
    Cs(:,:,iz) = scr1(:,:,iz)*scr1(:,:,iz) + scr2(:,:,iz)*scr2(:,:,iz) + scr3(:,:,iz)*scr3(:,:,iz)
  end do
  deallocate (scr1, scr2, scr3)
  if (idbg>1) then; j=j+1;tm(j)=elapsed(); print '(a4,2f7.3)','div',tm(j)-tm(j-1),tm(j)-tm(1); endif
  if (flag) Urms = sqrt(average(Cs))
  if (idbg>1) then; j=j+1;tm(j)=elapsed(); print '(a4,2f7.3)','urm',tm(j)-tm(j-1),tm(j)-tm(1); endif

!-----------------------------------------------------------------------
!  Polytropic pressure, at given sound speed
!-----------------------------------------------------------------------
  allocate (ee(mx,my,mz))                                                !  6 chunks
  if (.not. do_energy) then
   if (gamma.eq.1.) then
!$omp parallel do private(iz)
    do iz=1,mz
      e(:,:,iz) = (1./(gamma*gam1)*csound**2)*r(:,:,iz)
    end do
   else
!$omp parallel do private(iz)
    do iz=1,mz
      e(:,:,iz) = (1./(gamma*gam1)*csound**2)*r(:,:,iz)**gamma
    end do
   end if
  end if
!-----------------------------------------------------------------------
!  Energy per unit mass
!-----------------------------------------------------------------------
!$omp parallel do private(iz)
  do iz=1,mz
    ee(:,:,iz) = e(:,:,iz)/r(:,:,iz)
  end do
!-----------------------------------------------------------------------
!  Boundary condition -- may need to calculate pressure internally
!-----------------------------------------------------------------------
  if (do_energy) call energy_boundary(r,lnr,Ux,Uy,Uz,e,ee,p,Bx,By,Bz)
!-----------------------------------------------------------------------
!  Pressure
!-----------------------------------------------------------------------
  if (do_ionization) then
    call pressure (r,ee,p)
  else
!$omp parallel do private(iz)
    do iz=1,mz
      P(:,:,iz)  = gam1*e(:,:,iz)
    end do
  end if

!-----------------------------------------------------------------------
!  Numerical viscosity, Richtmyer & Morton type
!-----------------------------------------------------------------------
  if (do_momenta) then
   if (nu1 .lt. 0) then
!$omp parallel do private(iz,iy,ix)
    do iz=1,mz
    do iy=1,my
    do ix=1,mx
      Cs(ix,iy,iz) = sqrt(gamma*P(ix,iy,iz)/r(ix,iy,iz))
      nu(ix,iy,iz) = abs(nu1)
    end do
    end do
    end do
   else
!$omp parallel do private(iz,iy,ix,soundsp,velocity)
    do iz=1,mz
    do iy=1,my
    do ix=1,mx
      velocity = sqrt(Cs(ix,iy,iz))
      soundsp = sqrt(gamma*P(ix,iy,iz)/r(ix,iy,iz))
      Cs(ix,iy,iz) = soundsp + velocity
      nu(ix,iy,iz) = nu1*velocity &
                   + nu2*max(dxm(ix),dym(iy),dzm(iz))*nu(ix,iy,iz) &
                   + nu3*soundsp
    end do
    end do
    end do
   end if
  else
!$omp parallel do private(iz,iy,ix)
    do iz=1,mz
    do iy=1,my
    do ix=1,mx
      Cs(ix,iy,iz) = sqrt(gamma*P(ix,iy,iz)/r(ix,iy,iz))
      nu(ix,iy,iz) = nu2*max(dxm(ix),dym(iy),dzm(iz))*nu(ix,iy,iz) &
                   + nu3*Cs(ix,iy,iz)
    end do
    end do
    end do
  end if

  if (lb.lt.6) call regularize(nu)

  allocate (scr1(mx,my,mz))
  call max3_set (nu, scr1)
  call smooth3_set (scr1, nu)
  deallocate (scr1)

  call regularize(nu)

  if (flag) then
!$omp parallel do private(iz,iy,ix)
    do iz=1,mz
    do iy=1,my
    do ix=1,mx
      Cs(ix,iy,iz) = Cs(ix,iy,iz)*(dt/min(dxm(ix),dym(iy),dzm(iz)))
    end do
    end do
    end do
    Cu = fmaxval('Cu',Cs)

    fCv = 2.*6.2*3.*dt/2.
!$omp parallel do private(iz,iy,ix)
    do iz=1,mz
    do iy=1,my
    do ix=1,mx
      Cs(ix,iy,iz) = nu(ix,iy,iz)*(fCv/min(dxm(ix),dym(iy),dzm(iz)))
    end do
    end do
    end do
    Cv = fmaxval('Cv',Cs)
  end if
  deallocate(Cs)                                                      ! 12 chunks

!$omp parallel do private(iz)
  do iz=1,mz
    nu(:,:,iz) = nu(:,:,iz)*r(:,:,iz)
  end do
  if (idbg>1) then; j=j+1;tm(j)=elapsed(); print '(a4,2f7.3)','vis',tm(j)-tm(j-1),tm(j)-tm(1); endif

!-----------------------------------------------------------------------
!  Mass conservation, cell centered
!-----------------------------------------------------------------------
if (do_density) then
  allocate (ddxuppx(mx,my,mz), ddyuppy(mx,my,mz), ddzuppz(mx,my,mz))
  call ddxup_set (px, ddxuppx)
  call ddyup_set (py, ddyuppy)
  call ddzup_set (pz, ddzuppz)
!$omp parallel do private(iz)
  do iz=1,mz
    drdt(:,:,iz) = drdt(:,:,iz) - ddxuppx(:,:,iz) - ddyuppy(:,:,iz) - ddzuppz(:,:,iz)
  end do
  deallocate (ddxuppx, ddyuppy, ddzuppz)
end if
  if (idbg>1) then; j=j+1;tm(j)=elapsed(); print '(a4,2f7.3)','mas',tm(j)-tm(j-1),tm(j)-tm(1); endif

!-----------------------------------------------------------------------
!  Viscous stress, diagonal and off-diagonal elements
!-----------------------------------------------------------------------
  nstag=0
if (do_momenta) then
  allocate(Txx(mx,my,mz),Tyy(mx,my,mz),Tzz(mx,my,mz))                   ! 15 chunks
!$omp parallel do private(iz,iy,ix)
  do iz=1,mz
    do iy=1,my
    do ix=1,mx
      Txx(ix,iy,iz) = - (2.*dxm(ix))*nu(ix,iy,iz)*Sxx(ix,iy,iz) + P(ix,iy,iz)
      Tyy(ix,iy,iz) = - (2.*dym(iy))*nu(ix,iy,iz)*Syy(ix,iy,iz) + P(ix,iy,iz)
      Tzz(ix,iy,iz) = - (2.*dzm(iz))*nu(ix,iy,iz)*Szz(ix,iy,iz) + P(ix,iy,iz)
    end do
    end do
    if (do_energy) then
      if (do_dissipation) then 
        dedt(:,:,iz) = dedt(:,:,iz) &
                     - Txx(:,:,iz)*Sxx(:,:,iz) &
                     - Tyy(:,:,iz)*Syy(:,:,iz) &
                     - Tzz(:,:,iz)*Szz(:,:,iz)
      else
        dedt(:,:,iz) = dedt(:,:,iz) - P(:,:,iz)* &
                       (Sxx(:,:,iz) + Syy(:,:,iz) + Szz(:,:,iz))
      end if
    end if
  end do
  deallocate(P,Sxx,Syy,Szz)                                             ! 10 chunks
  if (idbg>2) print *,'1:',elapsed()-tm(j)

  allocate(Sxy(mx,my,mz),Syz(mx,my,mz),Szx(mx,my,mz))                   ! 13 chunks
  allocate(ddxdnUy(mx,my,mz), ddzdnUy(mx,my,mz), ddydnUz(mx,my,mz))     ! 16 chunks
  allocate(ddxdnUz(mx,my,mz), ddzdnUx(mx,my,mz), ddydnUx(mx,my,mz))     ! 19 chunks
  if (idbg>2) print *,'2:',elapsed()-tm(j)
  call ddxdn_set(Uy,ddxdnUy)
  call ddzdn_set(Uy,ddzdnUy)
  call ddydn_set(Ux,ddydnUx)
  call ddzdn_set(Ux,ddzdnUx)
  call ddxdn_set(Uz,ddxdnUz)
  call ddydn_set(Uz,ddydnUz)
  if (idbg>2) print *,'3:',elapsed()-tm(j)
!$omp parallel do private(iz)
  do iz=1,mz
    Sxy(:,:,iz) = (ddxdnUy(:,:,iz)+ddydnUx(:,:,iz))*0.5
    Syz(:,:,iz) = (ddydnUz(:,:,iz)+ddzdnUy(:,:,iz))*0.5
    Szx(:,:,iz) = (ddzdnUx(:,:,iz)+ddxdnUz(:,:,iz))*0.5
  end do
  if (idbg>2) print *,'4:',elapsed()-tm(j)
  deallocate (ddxdnUy, ddzdnUy, ddydnUz, ddxdnUz, ddzdnUx, ddydnUx)     ! 13 chunks
  allocate(Txy(mx,my,mz),Tyz(mx,my,mz),Tzx(mx,my,mz))                   ! 16 chunks
  allocate(scr1(mx,my,mz), xydnnu(mx,my,mz), xzdnnu(mx,my,mz), yzdnnu(mx,my,mz))
  if (idbg>2) print *,'5:',elapsed()-tm(j)
  call zdn_set (nu,scr1)
  call ydn_set (scr1,yzdnnu)
  call xdn_set (scr1,xzdnnu)
  call ydn_set (nu,scr1)
  call xdn_set (scr1,xydnnu)
  if (idbg>2) print *,'6:',elapsed()-tm(j)
!$omp parallel do private(iz,iy,ix)
  do iz=1,mz
  do iy=1,my
  do ix=1,mx
    Txy(ix,iy,iz) = - (dxmdn(ix)+dymdn(iy))*xydnnu(ix,iy,iz)*Sxy(ix,iy,iz)
    Tyz(ix,iy,iz) = - (dymdn(iy)+dzmdn(iz))*yzdnnu(ix,iy,iz)*Syz(ix,iy,iz)
    Tzx(ix,iy,iz) = - (dzmdn(iz)+dxmdn(ix))*xzdnnu(ix,iy,iz)*Szx(ix,iy,iz)
  end do
  end do
  end do
  if (idbg>2) print *,'7:',elapsed()-tm(j)
  deallocate(xydnnu, xzdnnu, yzdnnu)

  if (do_energy .and. do_dissipation) then
    call xup_set (Sxy, scr1)
    call yup_set (scr1, Sxy)
    call yup_set (Syz, scr1)
    call zup_set (scr1, Syz)
    call zup_set (Szx, scr1)
    call xup_set (scr1, Szx)

!$omp parallel do private(iz,iy,ix)
    do iz=1,mz
    do iy=1,my
    do ix=1,mx
      dedt(ix,iy,iz) = dedt(ix,iy,iz) &
      + (2.*(dxm(ix)+dym(iy)))*nu(ix,iy,iz)*Sxy(ix,iy,iz)**2 &
      + (2.*(dym(iy)+dzm(iz)))*nu(ix,iy,iz)*Syz(ix,iy,iz)**2 &
      + (2.*(dzm(iz)+dxm(ix)))*nu(ix,iy,iz)*Szx(ix,iy,iz)**2 
    end do
    end do
    end do
    nflop = nflop+9
  end if

  deallocate(scr1)
  deallocate(Sxy,Syz,Szx)                                               ! 13 chunks
  if (idbg>2) print *,'9:',elapsed()-tm(j)

!-----------------------------------------------------------------------
!  Add Reynolds stress
!-----------------------------------------------------------------------
  allocate (xupUx(mx,my,mz), yupUy(mx,my,mz), zupUz(mx,my,mz))
  call xup_set(Ux,xupUx)
  call yup_set(Uy,yupUy)
  call zup_set(Uz,zupUz)
  if (idbg>2) print *,'10:',elapsed()-tm(j)
!$omp parallel do private(iz)
  do iz=1,mz
    Txx(:,:,iz) = Txx(:,:,iz) + r(:,:,iz)*xupUx(:,:,iz)*xupUx(:,:,iz)
    Tyy(:,:,iz) = Tyy(:,:,iz) + r(:,:,iz)*yupUy(:,:,iz)*yupUy(:,:,iz)
    Tzz(:,:,iz) = Tzz(:,:,iz) + r(:,:,iz)*zupUz(:,:,iz)*zupUz(:,:,iz)
  end do
  deallocate (xupUx, yupUy, zupUz)
  if (idbg>2) print *,'11:',elapsed()-tm(j)

  allocate (xydnr(mx,my,mz), xdnUy(mx,my,mz), ydnUx(mx,my,mz))
  call xdn_set (ydnr, xydnr)
  call xdn_set(Uy, xdnUy)
  call ydn_set(Ux, ydnUx)
!$omp parallel do private(iz)
  do iz=1,mz
    Txy(:,:,iz) = Txy(:,:,iz) + xydnr(:,:,iz)*ydnUx(:,:,iz)*xdnUy(:,:,iz)
  end do
  deallocate (xydnr, xdnUy, ydnUx)
  if (idbg>2) print *,'12:',elapsed()-tm(j)

  allocate (yzdnr(mx,my,mz), ydnUz(mx,my,mz), zdnUy(mx,my,mz))
  call ydn_set (zdnr, yzdnr)
  call ydn_set(Uz, ydnUz)
  call zdn_set(Uy, zdnUy)
!$omp parallel do private(iz)
  do iz=1,mz
    Tyz(:,:,iz) = Tyz(:,:,iz) + yzdnr(:,:,iz)*zdnUy(:,:,iz)*ydnUz(:,:,iz)
  end do
  deallocate (yzdnr, ydnUz, zdnUy)
  if (idbg>2) print *,'13:',elapsed()-tm(j)

  allocate (zxdnr(mx,my,mz), zdnUx(mx,my,mz), xdnUz(mx,my,mz))
  call xdn_set (zdnr, zxdnr)
  call zdn_set(Ux, zdnUx)
  call xdn_set(Uz, xdnUz)
!$omp parallel do private(iz)
  do iz=1,mz
    Tzx(:,:,iz) = Tzx(:,:,iz) + zxdnr(:,:,iz)*xdnUz(:,:,iz)*zdnUx(:,:,iz)
  end do
  deallocate (zxdnr,zdnUx,xdnUz)
  deallocate (xdnr,ydnr,zdnr)                                            ! 10 chunks
  if (idbg>2) print *,'14:',elapsed()-tm(j)

!-----------------------------------------------------------------------
!  Equations of motion, face centred
!-----------------------------------------------------------------------
  allocate (ddxdnTxx(mx,my,mz), ddyupTxy(mx,my,mz), ddzupTzx(mx,my,mz))
  call ddxdn_set (Txx, ddxdnTxx)
  call ddyup_set (Txy, ddyupTxy)
  call ddzup_set (Tzx, ddzupTzx)
!$omp parallel do private(iz)
  do iz=1,mz
    dpxdt(:,:,iz) = dpxdt(:,:,iz) - ddxdnTxx(:,:,iz) - ddyupTxy(:,:,iz) - ddzupTzx(:,:,iz)
  end do
  deallocate (ddxdnTxx, ddyupTxy, ddzupTzx)
  if (idbg>2) print *,'15:',elapsed()-tm(j)

  allocate (ddydnTyy(mx,my,mz), ddzupTyz(mx,my,mz), ddxupTxy(mx,my,mz))
  call ddydn_set (Tyy, ddydnTyy)
  call ddzup_set (Tyz, ddzupTyz)
  call ddxup_set (Txy, ddxupTxy)
!$omp parallel do private(iz)
  do iz=1,mz
    dpydt(:,:,iz) = dpydt(:,:,iz) - ddydnTyy(:,:,iz) - ddzupTyz(:,:,iz) - ddxupTxy(:,:,iz)
  end do
  deallocate (ddydnTyy, ddzupTyz, ddxupTxy)
  if (idbg>2) print *,'16:',elapsed()-tm(j)

  allocate (ddzdnTzz(mx,my,mz), ddxupTzx(mx,my,mz), ddyupTyz(mx,my,mz))
  call ddzdn_set (Tzz, ddzdnTzz)
  call ddxup_set (Tzx, ddxupTzx)
  call ddyup_set (Tyz, ddyupTyz)
!$omp parallel do private(iz)
  do iz=1,mz
    dpzdt(:,:,iz) = dpzdt(:,:,iz) - ddzdnTzz(:,:,iz) - ddxupTzx(:,:,iz) - ddyupTyz(:,:,iz)
  end do
  deallocate (ddzdnTzz, ddxupTzx, ddyupTyz)
  deallocate (Txy,Tyz,Tzx,Txx,Tyy,Tzz)                                   !  4 chunks
  if (idbg>2) print *,'17:',elapsed()-tm(j)

  if (idbg>1) then; j=j+1;tm(j)=elapsed(); print '(a4,2f7.3,i4)','mom',tm(j)-tm(j-1),tm(j)-tm(1),nstag; endif
else
  deallocate (P,Sxx,Syy,Szz,xdnr,ydnr,zdnr)
end if

!-----------------------------------------------------------------------
!  MHD
!-----------------------------------------------------------------------
  if (do_mhd) then
    call mhd(r,ee,Ux,Uy,Uz,Bx,By,Bz,dpxdt,dpydt,dpzdt,dedt,dBxdt,dBydt,dBzdt,flag)
  if (idbg>1) then; j=j+1;tm(j)=elapsed(); print '(a4,2f7.3)','mhd',tm(j)-tm(j-1),tm(j)-tm(1); endif
  end if

!-----------------------------------------------------------------------
!  Abundance per unit mass
!-----------------------------------------------------------------------
  if (do_pscalar .or. do_cool) then
    allocate (dd(mx,my,mz))
    dd=d/r
  end if

!-----------------------------------------------------------------------
!  Passive scalar
!-----------------------------------------------------------------------
  call passive(nu,lnu,r,px,py,pz,Ux,Uy,Uz,dd,dddt)
  deallocate (Ux,Uy,Uz)                                                 !  7 chunks

if (do_energy) then

!-----------------------------------------------------------------------
!  Energy advection and diffusion
!-----------------------------------------------------------------------
  allocate(eflux(mx,my,mz), xdnnu(mx,my,mz), xdne(mx,my,mz), ddxdne(mx,my,mz))
  call xdn_set (nu, xdnnu)
  call xdn_set (ee, xdne)
  call ddxdn_set (ee, ddxdne)
!$omp parallel do private(iz,iy,ix)
  do iz=1,mz
  do iy=1,my
  do ix=1,mx
    eflux(ix,iy,iz) = xdne(ix,iy,iz)*px(ix,iy,iz) - dxm(ix)*xdnnu(ix,iy,iz)*ddxdne(ix,iy,iz)
  end do
  end do
  end do
  deallocate(xdnnu, xdne, ddxdne)

  allocate (scr1(mx,my,mz))
  call ddxup_set (eflux, scr1)
!$omp parallel do private(iz)
  do iz=1,mz
    dedt(:,:,iz) = dedt(:,:,iz) - scr1(:,:,iz)
  end do

  allocate(ydnnu(mx,my,mz), ydne(mx,my,mz), ddydne(mx,my,mz))
  call ydn_set (nu, ydnnu)
  call ydn_set (ee, ydne)
  call ddydn_set (ee, ddydne)
!$omp parallel do private(iz,iy,ix)
  do iz=1,mz
  do iy=1,my
  do ix=1,mx
    eflux(ix,iy,iz) = ydne(ix,iy,iz)*py(ix,iy,iz) - dym(iy)*ydnnu(ix,iy,iz)*ddydne(ix,iy,iz)
  end do
  end do
  end do
  deallocate(ydnnu, ydne, ddydne)

  call ddyup_set (eflux, scr1)
!$omp parallel do private(iz)
  do iz=1,mz
    dedt(:,:,iz) = dedt(:,:,iz) - scr1(:,:,iz)
  end do

  allocate(zdnnu(mx,my,mz), zdne(mx,my,mz), ddzdne(mx,my,mz))
  call zdn_set (nu, zdnnu)
  call zdn_set (ee, zdne)
  call ddzdn_set (ee, ddzdne)
!$omp parallel do private(iz,iy,ix)
  do iz=1,mz
  do iy=1,my
  do ix=1,mx
    eflux(ix,iy,iz) = zdne(ix,iy,iz)*pz(ix,iy,iz) - dzm(iz)*zdnnu(ix,iy,iz)*ddzdne(ix,iy,iz)
  end do
  end do
  end do
  deallocate(zdnnu, zdne, ddzdne, nu)

  call ddzup_set (eflux, scr1)
!$omp parallel do private(iz)
  do iz=1,mz
    dedt(:,:,iz) = dedt(:,:,iz) - scr1(:,:,iz)
  end do
  deallocate (eflux,scr1)

!-----------------------------------------------------------------------
!  Cooling and conduction
!-----------------------------------------------------------------------
  if (do_cool) then
    allocate(lne(mx,my,mz))
!$omp parallel do private(iz)
    do iz=1,mz
      lne(:,:,iz) = alog(ee(:,:,iz))
    end do
    call coolit (r,ee,lne,dd,dedt)
    call conduction (r,e,ee,Bx,By,Bz,dedt)
    deallocate(lne)
  end if
  
 else
  deallocate (nu)
 end if
  if (idbg>1) then; j=j+1;tm(j)=elapsed(); print '(a4,2f7.3)','tot',tm(j)-tm(j-1),tm(j)-tm(1); endif

  if (do_pscalar .or. do_cool) deallocate(dd)
  deallocate(ee)                                                        !  4 chunks

  call ddt_boundary (r,px,py,pz,e,Bx,By,Bz, &
                     drdt,dpxdt,dpydt,dpzdt, &
                     dedt,dBxdt,dBydt,dBzdt)

  if (idbg>1) then; j=j+1;tm(j)=elapsed(); print '(a4,2f7.3)','ddt',tm(j)-tm(j-1),tm(j)-tm(1); endif

  END
