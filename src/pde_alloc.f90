! $Id: pde_alloc.f90,v 1.1 2010/01/09 09:34:52 aake Exp $
!***********************************************************************
MODULE pde_mod
  real ram
  integer n_d2lnr, narr, narrm
  real, allocatable, dimension(:,:,:) :: &
    xdnl, ydnl, zdnl, xdnr, ydnr, zdnr, &
    lnr, lne, lnu, S2, p, dd, ee, nu, nud, Cs, d2lnr, &
    Sxx, Syy, Szz, Sxy, Syz, Szx, &
    Txx, Tyy, Tzz, Txy, Tyz, Tzx, &
    Ex, Ey, Ez
END MODULE

!***********************************************************************
SUBROUTINE init_pde
  call init_arrays
END SUBROUTINE

!***********************************************************************
SUBROUTINE pde (flag)
  USE params
  USE variables
  USE arrays
  implicit none
  logical flag
  real d2lnr_max

  call pde1 (flag, mx, my, mz, &
                 r, px, py, pz, e, d, Bx, By, Bz, &
                 drdt, dpxdt, dpydt, dpzdt, dedt, &
                 dddt, dBxdt, dBydt, dBzdt, Ux, Uy, Uz, &
                 scr1, scr2, scr3, scr4, scr5, scr6, d2lnr_max)
END
!***********************************************************************
SUBROUTINE pde1 (flag, mx, my, mz, &
                 r, px, py, pz, e, d, Bx, By, Bz, &
                 drdt, dpxdt, dpydt, dpzdt, dedt, &
                 dddt, dBxdt, dBydt, dBzdt, Ux, Uy, Uz, &
                 scr1, scr2, scr3, scr4, scr5, scr6, d2lnr_max)
  USE params, mx_void=>mx, my_void=>my, mz_void=>mz
  USE quench_m, only: qlim
  USE arrays,   only: eeav, dpxav, dpzav
  USE forcing,  only: rav
  USE pde_mod
  implicit none
  logical flag
  integer mx, my, mz
  real d2lnr_max
  integer iz, iy, ix, n_thread
  logical, save:: first_time=.true.
  real, dimension(mx):: nuxy, nuyz, nuzx, exp1, exp2
  real, dimension(my):: frho
  real average, nuS1, nuS2, divu, nux, nuy, nuz
  real gam1, fCv, wavesp, velocity, sumy, sumz
  real, dimension(mx,my,mz):: &
                 r, px, py, pz, e, d, Bx, By, Bz, &
                 drdt, dpxdt, dpydt, dpzdt, dedt, &
                 dddt, dBxdt, dBydt, dBzdt, Ux, Uy, Uz, &
                 scr1, scr2, scr3, scr4, scr5, scr6

  character (len=mid):: id='$Id: pde_alloc.f90,v 1.1 2010/01/09 09:34:52 aake Exp $'
!-----------------------------------------------------------------------
  call print_id(id)
  narr=27; narrm=narr
                                                                        call timer('pde','begin')
!-----------------------------------------------------------------------
!  The parameter 'gamma' is used in cases with polytropic equations of
!  state.  In other cases it is only used in the estimate of the sound
!  speed (which does not need to be accurate)
!-----------------------------------------------------------------------
  if (gamma .eq. 1.) then
    gam1 = 1.
  else
    gam1 = gamma-1.
  end if

!-----------------------------------------------------------------------
!  Viscosity Courant condition factors:
!    qlim : max factor from the quenching operator (=1 when do_quench=f)
!     6.2 : the maximum value returned from ddxup(ddxdn(f)) when dx=1.
!      3. : for the worst case of a 3-D checker board
!      2. : normalize so Ctd=1. when we reach 1x overshoot
!-----------------------------------------------------------------------
  fCv = qlim*6.2*3.*dt/2.*max(1.,nuE)

!-----------------------------------------------------------------------
!  Velocities, pressure
!-----------------------------------------------------------------------
  !$omp master
  allocate (lnr(mx,my,mz)); narr=narr+1; narrm=max(narr,narrm)
  allocate (xdnl(mx,my,mz), ydnl(mx,my,mz), zdnl(mx,my,mz)); narr=narr+3; narrm=max(narr,narrm)
  allocate (xdnr(mx,my,mz), ydnr(mx,my,mz), zdnr(mx,my,mz)); narr=narr+3; narrm=max(narr,narrm)
  !$omp end master
  !$omp barrier

  do iz=izs,ize
    lnr(:,:,iz) = alog(amax1(r(:,:,iz),1e-30))
  end do
    call dumpn(r ,'r' ,'Ux.dmp',1)
  call density_boundary(r,lnr,py,e)
    call dumpn(r ,'r' ,'Ux.dmp',1)
                                                                        call timer('pde','dens_bdry')
  call xdn_set (lnr, xdnl)
  call ydn_set (lnr, ydnl)
  call barrier_omp('pde1')                                              ! barrier 1, needed because lnr just computed
  call zdn_set (lnr, zdnl)
  do iz=izs,ize
  do iy=1,my
    call expn (mx, xdnl(:,iy,iz), xdnr(:,iy,iz))
    Ux(:,iy,iz) = px(:,iy,iz)/xdnr(:,iy,iz)

    call expn (mx, ydnl(:,iy,iz), ydnr(:,iy,iz))
    Uy(:,iy,iz) = py(:,iy,iz)/ydnr(:,iy,iz)

    call expn (mx, zdnl(:,iy,iz), zdnr(:,iy,iz))
    Uz(:,iy,iz) = pz(:,iy,iz)/zdnr(:,iy,iz)
  end do
  end do
    call dumpn(Ux,'Ux','Ux.dmp',1)
    call dumpn(px,'px','Ux.dmp',1)
    call dumpn(xdnr,'xdnr','Ux.dmp',1)
                                                                        call timer('pde','velocities')
  call velocity_boundary(r,xdnr,ydnr,zdnr,Ux,Uy,Uz,px,py,pz,e,Bx,By,Bz)
    call dumpn(Ux,'Ux','Ux.dmp',1)
    call dumpn(px,'px','Ux.dmp',1)
    call dumpn(xdnr,'xdnr','Ux.dmp',1)
                                                                        call timer('pde','vel_bdry')
    call dumpn (dpxdt, 'dpxdt', 'dpxdt.dmp', 1)
  call forceit (r,Ux,Uy,Uz,xdnr,ydnr,zdnr,dpxdt,dpydt,dpzdt)
    call dumpn (dpxdt, 'dpxdt', 'dpxdt.dmp', 1)

  !$omp barrier
  !$omp master
  deallocate (xdnr, ydnr, zdnr); narr=narr-3
  !$omp end master
  !$omp barrier
                                                                        call timer('pde','force')
!-----------------------------------------------------------------------
!  Strain tensor components
!-----------------------------------------------------------------------
  !$omp master
  allocate (Sxx(mx,my,mz), Syy(mx,my,mz), Szz(mx,my,mz)); narr=narr+3; narrm=max(narr,narrm)
  allocate (Sxy(mx,my,mz), Syz(mx,my,mz), Szx(mx,my,mz)); narr=narr+3; narrm=max(narr,narrm)
  allocate (S2(mx,my,mz)); narr=narr+1; narrm=max(narr,narrm)
  allocate (d2lnr(mx,my,mz)); narr=narr+1; narrm=max(narr,narrm)
  allocate (nud(mx,my,mz)); narr=narr+1; narrm=max(narr,narrm)
  allocate (ee(mx,my,mz)); narr=narr+1; narrm=max(narr,narrm)
  allocate (p(mx,my,mz)); narr=narr+1; narrm=max(narr,narrm)
  allocate (nu(mx,my,mz)); narr=narr+1; narrm=max(narr,narrm)
  allocate (Cs(mx,my,mz)); narr=narr+1; narrm=max(narr,narrm)
  !$omp end master
  !$omp barrier

  call ddxup_set (Ux, Sxx)
  call ddyup_set (Uy, Syy)
  call barrier_omp('pde2')                                              ! barrier 2, needed because Uz computed since last barrier
  call ddzup_set (Uz, Szz)
  call ddzdn_set (Uy,scr2)
  call ddxdn_set (Uy,scr1)
  call ddydn_set (Ux,scr3)
  call ddzdn_set (Ux,scr4)
  call ddxdn_set (Uz,scr5)
  call ddydn_set (Uz,scr6)
  do iz=izs,ize
    Sxy(:,:,iz) = (scr1(:,:,iz)+scr3(:,:,iz))*0.5
    Syz(:,:,iz) = (scr6(:,:,iz)+scr2(:,:,iz))*0.5
    Szx(:,:,iz) = (scr4(:,:,iz)+scr5(:,:,iz))*0.5
  end do
  
  if (Csmag > 0) then
    do iz=izs,ize
      scr4(:,:,iz) = 0.333333*(Sxx(:,:,iz) + Syy(:,:,iz) + Szz(:,:,iz))
      S2(:,:,iz) = (Sxx(:,:,iz)-scr4(:,:,iz))**2 &
                 + (Syy(:,:,iz)-scr4(:,:,iz))**2 & 
                 + (Szz(:,:,iz)-scr4(:,:,iz))**2
      scr1(:,:,iz) = Sxy(:,:,iz)**2
      scr2(:,:,iz) = Syz(:,:,iz)**2
      scr3(:,:,iz) = Szx(:,:,iz)**2
    end do
    call xup1_set (scr1,scr4)
    call yup1_set (scr4,scr1)
    call yup1_set (scr2,scr4)
    call barrier_omp('pde3')                                            ! scr4 needs to be all done
    call zup1_set (scr4,scr2)
    call zup1_set (scr3,scr5)
    call barrier_omp('pde5')                                            ! scr4 needs to be all done
    call xup1_set (scr5,scr3)
    do iz=izs,ize
      S2(:,:,iz) = sqrt(2.*(S2(:,:,iz) + 2.*(scr1(:,:,iz)+scr2(:,:,iz)+scr3(:,:,iz))))
    end do
  else
    do iz=izs,ize
      S2(:,:,iz) = 0.
    end do
  end if
                                                                        call timer('pde','strain')
!-----------------------------------------------------------------------
!  First part of viscosity
!-----------------------------------------------------------------------
  !call dif2a_set (lnr,d2lnr)
  call d2abs_set (lnr,d2lnr)
  call regularize (d2lnr)
  call smooth3max3_set (d2lnr, scr1)
  call dumpn(d2lnr,'d2lnr','nu.dmp',0)
  if (do_stratified .and. nrho > 0) then
    do iy=1,my
      frho(iy)=1./(1.+(rav(iy)/rav(nrho))**.30)
    end do
  else
    frho(:)=1.
  endif
  if (nu4 < 0.) then
    call dif2_set (Ux, Uy, Uz, nud)
    do iz=izs,ize
    do iy=1,my
    do ix=1,mx
      nud(ix,iy,iz) = (nu2*nud(ix,iy,iz) + Csmag*S2(ix,iy,iz))*frho(iy)
    end do
    end do
    end do
    nflop = nflop+4
  else
    do iz=izs,ize
    do iy=1,my
    do ix=1,mx
      nud(ix,iy,iz) = Sxx(ix,iy,iz)+Syy(ix,iy,iz)+Szz(ix,iy,iz)
      nud(ix,iy,iz) = (nu2*max(-nud(ix,iy,iz),0.) + Csmag*S2(ix,iy,iz))*frho(iy)
    end do
    end do
    end do
    nflop = nflop+4
  end if
    call dumpn (nud, 'nud', 'nu.dmp', 1)
    call dumpn (nud, 'nud', 'xxx.dmp', 0)

    !call dumpn (Sxx, 'Sxx', 'nu.dmp', 1)
    !call dumpn (Syy, 'Syy', 'nu.dmp', 1)
    !call dumpn (Szz, 'Szz', 'nu.dmp', 1)
    !call dumpn (S2 , 'S2' , 'nu.dmp', 1)
    
  call xup_set (Ux, scr1)
  call yup_set (Uy, scr2)
  call zup_set (Uz, scr3)
  do iz=izs,ize
    S2(:,:,iz) = scr1(:,:,iz)**2 + scr2(:,:,iz)**2 + scr3(:,:,iz)**2
  end do
  call average_subr (S2, Urms)

  if (isubstep==1) then
    do iz=izs,ize
      scr1(:,:,iz) = 0.5*S2(:,:,iz)*r(:,:,iz)
    end do
    call average_subr (scr1, E_kin)
  end if

!-----------------------------------------------------------------------
!  Energy per unit mass
!-----------------------------------------------------------------------
  if (do_energy) then
    do iz=izs,ize
      ee(:,:,iz) = e(:,:,iz)/r(:,:,iz)
    end do
  end if
                                                                        call timer('pde','visc1')
!-----------------------------------------------------------------------
!  Boundary condition -- may need to calculate pressure internally
!-----------------------------------------------------------------------
  if (do_energy) call energy_boundary(r,lnr,Ux,Uy,Uz,e,ee,p,Bx,By,Bz)
                                                                        call timer('pde','e_bdry')
!-----------------------------------------------------------------------
!  Pressure
!-----------------------------------------------------------------------
  call pressure (r,ee,p)
      call dumpn (p, 'Pg', 'dpydt.dmp', 0)
                                                                        call timer('pde','pressure')
!-----------------------------------------------------------------------
!  Numerical viscosity, Richtmyer & Morton type.  For testing purposes,
!  nu1<0 signals the use of constant diffusivity.
!-----------------------------------------------------------------------
  if (nu1.lt.0.0) then
    do iz=izs,ize
      Cs(:,:,iz) = sqrt(gamma*p(:,:,iz)/r(:,:,iz)) + sqrt(S2(:,:,iz))
      nu(:,:,iz) = abs(nu1)
    end do
  else if (do_mhd) then
    call xup_set(Bx,scr1)
    call yup_set(By,scr2)
    call zup_set(Bz,scr3)
    do iz=izs,ize
    do iy=1,my
    do ix=1,mx
      wavesp = sqrt((scr1(ix,iy,iz)**2+scr2(ix,iy,iz)**2+scr3(ix,iy,iz)**2 &
                     + gamma*P(ix,iy,iz))/r(ix,iy,iz))
      scr1(ix,iy,iz) = wavesp
      velocity = sqrt(S2(ix,iy,iz))
      Cs(ix,iy,iz) = wavesp + velocity
      nu(ix,iy,iz) = nu1*velocity + nu3*wavesp
      ! scr2(ix,iy,iz) = nu(ix,iy,iz) + max(dxm(ix),dym(iy),dzm(iz))*nud(ix,iy,iz)
    end do
    end do
    end do
  else
    do iz=izs,ize
    do iy=1,my
    do ix=1,mx
      wavesp = sqrt(gamma*P(ix,iy,iz)/r(ix,iy,iz))
      scr1(ix,iy,iz) = wavesp
      velocity = sqrt(S2(ix,iy,iz))
      Cs(ix,iy,iz) = wavesp + velocity
      nu(ix,iy,iz) = nu1*velocity + nu3*wavesp
      ! scr2(ix,iy,iz) = nu(ix,iy,iz) + max(dxm(ix),dym(iy),dzm(iz))*nud(ix,iy,iz)
    end do
    end do
    end do
  end if
  call viscosity_boundary (nu, scr1)
    call dumpn (nu  , 'nu' , 'nu.dmp', 1)
    call dumpn (scr1, 'U'  , 'nu.dmp', 1)
    call dumpn (Cs  , 'csu', 'nu.dmp', 1)
    call dumpn (scr2,'nu','dpxdt.dmp', 1)

!-----------------------------------------------------------------------
!  Enhance where ln(rho) changes too rapidly
!-----------------------------------------------------------------------
  if (nu4 > 0) then
    do iz=izs,ize
    do iy=1,my
    do ix=1,mx
      ! nu (ix,iy,iz) = nu (ix,iy,iz)*max(1.,nu4*d2lnr(ix,iy,iz))
      nud(ix,iy,iz) = nud(ix,iy,iz)*max(1.,nu4*d2lnr(ix,iy,iz))
    end do
    end do
    end do
  end if
    call dumpn (nud, 'nud', 'xxx.dmp', 1)

!-----------------------------------------------------------------------
!  Make compatible with pde_omp, as an option
!-----------------------------------------------------------------------
  if (.not. do_aniso_nu) then
    do iz=izs,ize
    do iy=1,my
    do ix=1,mx
      nu(ix,iy,iz) = nu(ix,iy,iz) + max(dxm(ix),dym(iy),dzm(iz))*nud(ix,iy,iz)
      nud(ix,iy,iz) = 0.
    end do
    end do
    end do
  end if
    call dumpn (nu , 'nu', 'xxx.dmp', 1)
    call dumpn (nu , 'nu' , 'nu.dmp', 1)
    call dumpn (nud, 'nud', 'nu.dmp', 1)

!-----------------------------------------------------------------------
!  Spread the influence over a few zones, regularize
!-----------------------------------------------------------------------
  call regularize(nu)
  call regularize(nud)

  !call barrier_omp('pde7')                                              ! barrier needed only if removing "single"
!$omp single
  Urms = sqrt(Urms)
!$omp end single nowait

  if (do_max5_hd) then
    call smooth3max5_set (nud, scr1)
    call smooth3max5_set (nu , scr2)
  else
    call smooth3max3_set (nud, scr1)
    call smooth3max3_set (nu , scr2)
  end if

  !call barrier_omp('pde8')                                              ! barrier 4, NOT NEEDED????
  call regularize(nud)
  call regularize(nu)
    call dumpn (nu , 'nu'  , 'nu.dmp', 1)
    call dumpn (nud, 'nud' , 'nu.dmp', 1)

!-----------------------------------------------------------------------
!  Courant conditions
!-----------------------------------------------------------------------
  if (flag) then
    do iz=izs,ize
    do iy=1,my
    do ix=1,mx
      Cs(ix,iy,iz) = Cs(ix,iy,iz)*(dt/min(dxm(ix),dym(iy),dzm(iz)))
      scr1(ix,iy,iz) = fCv*(nud(ix,iy,iz)+nu(ix,iy,iz)/min(dxm(ix),dym(iy),dzm(iz)))
    end do
    end do
    end do
    call fmaxval_subr ('Cu',Cs,Cu)
    call fmaxval_subr ('Cv',scr1,Cv)
  end if
    call dumpn (scr1, 'Cv' , 'nu.dmp', 1)

  !$omp barrier
  !$omp master
  deallocate (S2); narr=narr-1
  deallocate (Cs); narr=narr-1
  allocate (lnu(mx,my,mz)); narr=narr+1; narrm=max(narr,narrm)
  !$omp end master

  do iz=izs,ize
    nu(:,:,iz) = nu(:,:,iz)*r(:,:,iz)
    nud(:,:,iz) = nud(:,:,iz)*r(:,:,iz)
    lnu(:,:,iz) = alog(max(nu(:,:,iz),1e-30))
  end do
    call dumpn (lnu, 'lnu', 'dpxdt.dmp', 1)
    call dumpn (lnu ,'lnu', 'xxx.dmp', 1)
                                                                        call timer('pde','visc2')

!-----------------------------------------------------------------------
!  Mass conservation, cell centered
!-----------------------------------------------------------------------
if (do_density) then
  if (nur==0) then
    if (do_2nddiv) then
      call ddxup_set (px, scr1)
      call ddyup_set (py, scr2)
      call ddzup_set (pz, scr3)
      call ddxup1_set (px, scr4)
      call ddyup1_set (py, scr5)
      call ddzup1_set (pz, scr6)
      do iz=izs,ize
        where (d2lnr(:,:,iz) > d2lnr_lim)
          drdt(:,:,iz) = drdt(:,:,iz) &
                       - scr4(:,:,iz) &
                       - scr5(:,:,iz) &
                       - scr6(:,:,iz)
        elsewhere
          drdt(:,:,iz) = drdt(:,:,iz) &
                       - scr1(:,:,iz) &
                       - scr2(:,:,iz) &
                       - scr3(:,:,iz)
        endwhere
      end do
    else
      call ddxup_sub (px, drdt)
      call ddyup_sub (py, drdt)
      call ddzup_sub (pz, drdt)
      call dumpn(drdt,'drdt','xxx.dmp',1)
    end if

!-----------------------------------------------------------------------
!  Mass conservation, with mass diffusion
!-----------------------------------------------------------------------
  else
    call ddxdn1_set (lnr, scr1)
    call xdn1_set (lnu, scr2)
    if (do_quench) then
      call quenchx (scr1, scr3)
      do iz=izs,ize
      do iy=1,my
      call expn (mx, scr2(:,iy,iz), exp1)
      do ix=1,mx
        scr1(ix,iy,iz) = -px(ix,iy,iz)+(nur*dxmdn(ix))*exp1(ix)*scr3(ix,iy,iz)
      end do
      end do
      end do
    else
      do iz=izs,ize
      do iy=1,my
      call expn (mx, scr2(:,iy,iz), exp1)
      where (d2lnr(:,iy,iz) < d2lnr_lim) 
        scr1(:,iy,iz) = -px(:,iy,iz)
      elsewhere
        scr1(:,iy,iz) = -px(:,iy,iz)+(nur*dxmdn(:))*exp1(:)*scr1(:,iy,iz)
      end where
      end do
      end do
    end if

    if (do_2nddiv) then
      call ddxup_set  (scr1, scr2)
      call ddxup1_set (scr1, scr3)
      do iz=izs,ize
        where (d2lnr(:,:,iz) > d2lnr_lim)
          drdt(:,:,iz) = drdt(:,:,iz) &
                       + scr3(:,:,iz)
        elsewhere
          drdt(:,:,iz) = drdt(:,:,iz) &
                       + scr2(:,:,iz)
        endwhere
      end do
    else
      call ddxup_add (scr1, drdt)
    end if
!
    if (do_stratified) then
      call haverage_subr (lnr, eeav)
      do iz=izs,ize
        do iy=1,my
          scr2(:,iy,iz) = lnr(:,iy,iz)-eeav(iy)
        end do
      end do
      call ddydn1_set (scr2, scr1)
    else
      call ddydn1_set (lnr, scr1)
    end if

    call ydn1_set (lnu, scr2)
    if (do_quench) then
      call quenchy (scr1, scr3)
      do iz=izs,ize
      do iy=1,my
      call expn (mx, scr2(:,iy,iz), exp1)
      do ix=1,mx
        scr1(ix,iy,iz) = -py(ix,iy,iz)+(nur*dymdn(iy))*exp1(ix)*scr3(ix,iy,iz)
      end do
      end do
      end do
    else
      do iz=izs,ize
      do iy=1,my
      call expn (mx, scr2(:,iy,iz), exp1)
      where (d2lnr(:,iy,iz) < d2lnr_lim) 
        scr1(:,iy,iz) = -py(:,iy,iz)
      elsewhere
        scr1(:,iy,iz) = -py(:,iy,iz)+(nur*dymdn(iy))*exp1(:)*scr1(:,iy,iz)
      end where
      end do
      end do
    end if

    if (do_2nddiv) then
      call ddyup_set  (scr1, scr2)
      call ddyup1_set (scr1, scr3)
      do iz=izs,ize
        where (d2lnr(:,:,iz) > d2lnr_lim)
          drdt(:,:,iz) = drdt(:,:,iz) &
                       + scr3(:,:,iz)
        elsewhere
          drdt(:,:,iz) = drdt(:,:,iz) &
                       + scr2(:,:,iz)
        endwhere
      end do
    else
      call ddyup_add (scr1, drdt)
    end if
!
    call ddzdn1_set (lnr, scr1)
    call barrier_omp('pde9')                                            ! barrier 5, needed because lnu & scr1 just computed
    if (do_quench) then
      call quenchz (scr1, scr3, scr2)
        call dumpn (scr3, 'qz', 'quench.dmp', 1)
      call zdn_set (lnu, scr2)
      do iz=izs,ize
      do iy=1,my
      call expn (mx, scr2(:,iy,iz), exp1)
      do ix=1,mx
        scr1(ix,iy,iz) = -pz(ix,iy,iz)+(nur*dzmdn(iz))*exp1(ix)*scr3(ix,iy,iz)
      end do
      end do
      end do
    else
      call zdn1_set (lnu, scr2)
      do iz=izs,ize
      do iy=1,my
      call expn (mx, scr2(:,iy,iz), exp1)
      where (d2lnr(:,iy,iz) < d2lnr_lim) 
        scr1(:,iy,iz) = -pz(:,iy,iz)
      elsewhere
        scr1(:,iy,iz) = -pz(:,iy,iz)+(nur*dzmdn(iz))*exp1(:)*scr1(:,iy,iz)
      end where
      end do
      end do
    end if

    call barrier_omp('pde11')                                           ! barrier 7, needed because scr1 modified since last barrier
    if (do_2nddiv) then
      call ddzup_set  (scr1, scr2)
      call ddzup1_set (scr1, scr3)
      do iz=izs,ize
        where (d2lnr(:,:,iz) > d2lnr_lim)
          drdt(:,:,iz) = drdt(:,:,iz) &
                       + scr3(:,:,iz)
        elsewhere
          drdt(:,:,iz) = drdt(:,:,iz) &
                       + scr2(:,:,iz)
        endwhere
      end do
    else
      call ddzup_add (scr1, drdt)
    end if
  end if
  call barrier_omp('pde12')                                             ! barrier 7a, needed because scr1 modified later on!!!
                                                                        call timer('pde','mass')
end if ! (do_density)


!-----------------------------------------------------------------------
!  Quenching operator
!-----------------------------------------------------------------------
  if (do_quench) then
    call quenchx (Sxx, scr1)
      call dumpn (scr1, 'qx', 'quench.dmp', 1)
    call quenchz (Szz, scr3, scr2)
      call dumpn (Szz, 'qz', 'quench.dmp', 1)
      call dumpn (scr3, 'qz', 'quench.dmp', 1)
    call quenchy (Syy, scr2)
      call dumpn (scr2, 'qy', 'quench.dmp', 1)
                                                                        call timer('pde','quench')
  end if

!-----------------------------------------------------------------------
!  Viscous stress, diagonal elements
!-----------------------------------------------------------------------
    if (do_energy) call dumpn (dedt, 'begin', 'dedt.dmp', 1)
    call dumpn (p, 'p', 'dpxdt.dmp', 1)
    call dumpn (dedt,'dedt','xxx.dmp',1)
  nuS1 = 3.*nuS/(1.+2.*nuS)
  nuS2 = (1.-nuS)/(1.+2.*nuS)

  !$omp barrier
  !$omp master
  deallocate (lnr); narr=narr-1
  allocate (Txx(mx,my,mz), Tyy(mx,my,mz), Tzz(mx,my,mz)); narr=narr+3; narrm=max(narr,narrm)
  !$omp end master
  !$omp barrier

  do iz=izs,ize
    if (do_quench) then
      do iy=1,my
      do ix=1,mx
        Txx(ix,iy,iz) = - dxm(ix)*nu(ix,iy,iz)*scr1(ix,iy,iz) + p(ix,iy,iz)
        Tyy(ix,iy,iz) = - dym(iy)*nu(ix,iy,iz)*scr2(ix,iy,iz) + p(ix,iy,iz)
        Tzz(ix,iy,iz) = - dzm(iz)*nu(ix,iy,iz)*scr3(ix,iy,iz) + p(ix,iy,iz)
      end do
      end do
    else
      do iy=1,my
      do ix=1,mx
        divu = Sxx(ix,iy,iz) + Syy(ix,iy,iz) + Szz(ix,iy,iz)
        nux = dxm(ix)*(nu(ix,iy,iz)+dxm(ix)*nud(ix,iy,iz))
        nuy = dym(iy)*(nu(ix,iy,iz)+dym(iy)*nud(ix,iy,iz))
        nuz = dzm(iz)*(nu(ix,iy,iz)+dzm(iz)*nud(ix,iy,iz))
        Txx(ix,iy,iz) = - nux*(nuS1*Sxx(ix,iy,iz)+nuS2*divu) + p(ix,iy,iz)
        Tyy(ix,iy,iz) = - nuy*(nuS1*Syy(ix,iy,iz)+nuS2*divu) + p(ix,iy,iz)
        Tzz(ix,iy,iz) = - nuz*(nuS1*Szz(ix,iy,iz)+nuS2*divu) + p(ix,iy,iz)
        ! scr2(ix,iy,iz) = nu(ix,iy,iz)+dxm(ix)*nud(ix,iy,iz)
     end do
     end do
    end if
    if (do_energy) then
      if (do_dissipation) then 
        do iy=1,my
        do ix=1,mx
          dedt(ix,iy,iz) = dedt(ix,iy,iz) &
                     - Txx(ix,iy,iz)*Sxx(ix,iy,iz) &
                     - Tyy(ix,iy,iz)*Syy(ix,iy,iz) &
                     - Tzz(ix,iy,iz)*Szz(ix,iy,iz)
        end do
        end do
        if (isubstep == 1) then
          do iy=1,my
          do ix=1,mx
            scr1(ix,iy,iz) = -(Txx(ix,iy,iz) - p(ix,iy,iz))*Sxx(ix,iy,iz) &
                             -(Tyy(ix,iy,iz) - p(ix,iy,iz))*Syy(ix,iy,iz) &
                             -(Tzz(ix,iy,iz) - p(ix,iy,iz))*Szz(ix,iy,iz)
          end  do
          end  do
        end if
      else
        do iy=1,my
        do ix=1,mx
          dedt(ix,iy,iz) = dedt(ix,iy,iz) - p(ix,iy,iz)* &
                       (Sxx(ix,iy,iz) + Syy(ix,iy,iz) + Szz(ix,iy,iz))
        end do
        end do
      end if
    end if
  end do
    call dumpn(dedt,'dedt','xxx.dmp',1)
    call dumpn(  p,  'p','xxx.dmp',1)
    call dumpn(Sxx,'Sxx','xxx.dmp',1)
    call dumpn(Syy,'Syy','xxx.dmp',1)
    call dumpn(Szz,'Szz','xxx.dmp',1)
    call dumpn(dedt, 'dedt', 'dpydt.dmp', 1)

  if (do_energy .and. do_dissipation .and. isubstep==1) then
    if (lb >  1) scr1(:, 1:lb+1,izs:ize) = 0.                           ! zap ghost-zone values
    if (ub < my) scr1(:,ub-1:my,izs:ize) = 0.
    call dumpn(scr1,'diagonal','diss.dmp',0)
    call barrier_omp ('Qk1')
    call average_subr (scr1, Q_kin)
    !print*,'Qk1',omp_mythread,Q_kin
  end if

    if (do_energy) call dumpn (dedt, 'visc', 'dedt.dmp', 1)
    call dumpn (scr2, 'nu', 'dpxdt.dmp', 1)
    call dumpn (Sxx, 'Sxx', 'dpxdt.dmp', 1)
    call dumpn (Txx, 'Txx', 'dpxdt.dmp', 1)
                                                                        call timer('pde','mom-diag')
!-----------------------------------------------------------------------
!  Interpolated log viscosity coefficients (already contain rho-factor)
!-----------------------------------------------------------------------
  do iz=izs,ize
  do iy=1,my
  do ix=1,mx
    lnu(ix,iy,iz) = alog(max(nu(ix,iy,iz)+0.5*(dxm(ix)+dym(iy))*nud(ix,iy,iz),1e-30))
  end do
  end do
  end do
  call xdn_set (lnu,scr1)
    call dumpn (lnu , 'lnu' , 'dpxdt.dmp', 1)
    call dumpn (scr1, 'scr1', 'dpxdt.dmp', 1)
  call ydn_set (scr1,scr2)
    call dumpn (scr2, 'scr2', 'dpxdt.dmp', 1)

  do iz=izs,ize
  do iy=1,my
  do ix=1,mx
    lnu(ix,iy,iz) = alog(max(nu(ix,iy,iz)+0.5*(dxm(ix)+dzm(iz))*nud(ix,iy,iz),1e-30))
  end do
  end do
  end do
  call xdn_set (lnu,scr1)

  call barrier_omp('pde15')                                             ! barrier 10, scr1
  call zdn_set (scr1,scr3)

  do iz=izs,ize
  do iy=1,my
  do ix=1,mx
    lnu(ix,iy,iz) = alog(max(nu(ix,iy,iz)+0.5*(dym(iy)+dzm(iz))*nud(ix,iy,iz),1e-30))
  end do
  end do
  end do
  call ydn_set (lnu,scr5)
  call barrier_omp('pde16')                                             ! barrier 11, scr5
  call zdn_set (scr5,scr1)
  call barrier_omp('pde16a')                                            ! barrier 11, scr5
                                                                        call timer('pde','log nu')
!-----------------------------------------------------------------------
!  Stress tensor, off-diagonal terms
!-----------------------------------------------------------------------
  !$omp barrier
  !$omp master
  deallocate (Sxx, Syy, Szz); narr=narr-3
  allocate (Txy(mx,my,mz), Tyz(mx,my,mz), Tzx(mx,my,mz)); narr=narr+3; narrm=max(narr,narrm)
  !$omp end master
  !$omp barrier

  if (do_quench) then
    call quenchyz (Syz, scr5, scr4)
      call dumpn (scr5, 'qyz', 'quench.dmp', 1)
    call quenchzx (Szx, scr6, scr4)
      call dumpn (scr6, 'qzx', 'quench.dmp', 1)
    call quenchxy (Sxy, scr4)
      call dumpn (scr4, 'qxy', 'quench.dmp', 1)

    do iz=izs,ize
    do iy=1,my
      call expn (mx, scr2(:,iy,iz), nuxy)
      call expn (mx, scr1(:,iy,iz), nuyz)
      call expn (mx, scr3(:,iy,iz), nuzx)
      do ix=1,mx
        Txy(ix,iy,iz) = - max(dxmdn(ix),dymdn(iy))*nuxy(ix)*scr4(ix,iy,iz)
        Tyz(ix,iy,iz) = - max(dymdn(iy),dzmdn(iz))*nuyz(ix)*scr5(ix,iy,iz)
        Tzx(ix,iy,iz) = - max(dzmdn(iz),dxmdn(ix))*nuzx(ix)*scr6(ix,iy,iz)
      end do
    end do
    end do
  else
    do iz=izs,ize
    do iy=1,my
      call expn (mx, scr2(:,iy,iz), nuxy)                               ! scr2 = ydn(xdn(alog(nu)))
      call expn (mx, scr1(:,iy,iz), nuyz)                               ! scr1 = zdn(ydn(alog(nu)))
      call expn (mx, scr3(:,iy,iz), nuzx)                               ! scr3 = zdn(xdn(alog(nu)))
      do ix=1,mx
        Txy(ix,iy,iz) = - nuS1*max(dxmdn(ix),dymdn(iy))*nuxy(ix)*Sxy(ix,iy,iz)
        Tyz(ix,iy,iz) = - nuS1*max(dymdn(iy),dzmdn(iz))*nuyz(ix)*Syz(ix,iy,iz)
        Tzx(ix,iy,iz) = - nuS1*max(dzmdn(iz),dxmdn(ix))*nuzx(ix)*Szx(ix,iy,iz)
      end do
    end do
    end do
  end if
    call dumpn (Sxy, 'Sxy', 'dpxdt.dmp', 1)
    call dumpn (Txy, 'Txy', 'dpxdt.dmp', 1)
                                                                        call timer('pde','mom_off-diag')

!-----------------------------------------------------------------------
!  Compensate for net horizontal viscous drag
!-----------------------------------------------------------------------
  if (do_nodrag .and. abs(omegax)+abs(omegay)+abs(omegaz) > 0.) then
    call ddyup_set (Txy, scr1)
    call ddyup_set (Tyz, scr2)
    call haverage_subr (scr1, dpxav)
    call haverage_subr (scr2, dpzav)
    do iz=izs,ize
      do iy=1,my
        dpxdt(:,iy,iz) = dpxdt(:,iy,iz) + dpxav(iy)
        dpzdt(:,iy,iz) = dpzdt(:,iy,iz) + dpzav(iy)
      end do
    end do
                                                                        call timer('pde','nodrag')
  end if

!-----------------------------------------------------------------------
!  Viscous dissipation
!-----------------------------------------------------------------------
  if (do_energy .and. do_dissipation) then
    do iz=izs,ize
      scr1(:,:,iz) = -2.*Txy(:,:,iz)*Sxy(:,:,iz)                        ! sub- and super-diagonal terms
      scr2(:,:,iz) = -2.*Tyz(:,:,iz)*Syz(:,:,iz)
      scr3(:,:,iz) = -2.*Tzx(:,:,iz)*Szx(:,:,iz)
    end do

    call xup1_set (scr1, scr4)                                           ! center the xy-dissip
    call yup1_set (scr4, scr1)

    call yup1_set (scr2, scr5)
    call barrier_omp('pde20')                                           ! barrier 14, scr5 and scr3
    call zup1_set (scr5, scr2)

    call zup1_set (scr3, scr6)
    call barrier_omp('pde21')                                           ! barrier 14, scr5 and scr3
    call xup1_set (scr6, scr3)

    do iz=izs,ize
      dedt(:,:,iz) = dedt(:,:,iz) &                                     ! add in centered dissipation
                   + scr1(:,:,iz) &
                   + scr2(:,:,iz) &
                   + scr3(:,:,iz)

      if (isubstep == 1) then
        scr1(:,:,iz) = scr1(:,:,iz) &                                   ! for diagnostics only
                     + scr2(:,:,iz) &
                     + scr3(:,:,iz)
      end if
    end do

    if (isubstep == 1) then
      if (lb >  1) scr1(:, 1:lb+1,izs:ize) = 0.                         ! zap ghost-zone values
      if (ub < my) scr1(:,ub-1:my,izs:ize) = 0.
      call dumpn(scr1,'offdiag','diss.dmp',1)
      call barrier_omp ('Q_kin')
      Q_kin = Q_kin + average(scr1)
      !print*,'Qk2',omp_mythread,Q_kin

    end if
    call dumpn (dedt, 'offdiag', 'dedt.dmp', 1)
    call dumpn (dedt, 'dedt', 'dpydt.dmp', 1)
                                                                        call timer('pde','visc.diss.')
  end if
  call hd_fluxes (r,Ux,Uy,Uz,e,p,py,Txy,Tyy,Tyz,scr1,scr2,scr3,scr4,scr5,scr6)

!-----------------------------------------------------------------------
!  Add Reynolds stress
!-----------------------------------------------------------------------
  call xup_set (Ux,scr1)
  call yup_set (Uy,scr2)
  call zup_set (Uz,scr3)
    call dumpn (Txx, 'Txx', 'dpxdt.dmp', 1)
  do iz=izs,ize
    Txx(:,:,iz) = Txx(:,:,iz) + r(:,:,iz)*scr1(:,:,iz)**2 - p(:,:,iz)
    Tyy(:,:,iz) = Tyy(:,:,iz) + r(:,:,iz)*scr2(:,:,iz)**2 - p(:,:,iz)
    Tzz(:,:,iz) = Tzz(:,:,iz) + r(:,:,iz)*scr3(:,:,iz)**2 - p(:,:,iz)
  end do
    call dumpn (scr1, 'Ux^2', 'dpxdt.dmp', 1)
    call dumpn (Txx, 'Txx', 'dpxdt.dmp', 1)

  call xdn_set (ydnl, scr1)
  call xdn_set (Uy, scr2)
  call ydn_set (Ux, scr3)
  do iz=izs,ize
  do iy=1,my
    call expn (mx, scr1(:,iy,iz), exp1)
    Txy(:,iy,iz) = Txy(:,iy,iz) + exp1*scr3(:,iy,iz)*scr2(:,iy,iz)
  end do
  end do

  call ydn_set (zdnl, scr1)
  call ydn_set (Uz, scr2)
  call zdn_set (Uy, scr3)
  do iz=izs,ize
  do iy=1,my
    call expn (mx, scr1(:,iy,iz), exp1)
    Tyz(:,iy,iz) = Tyz(:,iy,iz) + exp1*scr3(:,iy,iz)*scr2(:,iy,iz)
  end do
  end do

  call zdn_set (xdnl, scr1)
  call zdn_set (Ux, scr2)
  call xdn_set (Uz, scr3)
  do iz=izs,ize
  do iy=1,my
    call expn (mx, scr1(:,iy,iz), exp1)
    Tzx(:,iy,iz) = Tzx(:,iy,iz) + exp1*scr3(:,iy,iz)*scr2(:,iy,iz)
  end do
  end do

  !$omp barrier
  !$omp master
  deallocate (Sxy, Syz, Szx); narr=narr-3
  deallocate (xdnl, ydnl, zdnl); narr=narr-3
  !$omp end master
  !$omp barrier
                                                                        call timer('pde','add R-stress')

!-----------------------------------------------------------------------
!  Equations of motion, face centred
!-----------------------------------------------------------------------
  call barrier_omp('pde22')                                             ! needed because Tzx just computed
  if (do_2nddiv) then
    call ddxdn_set (Txx, scr1)
    call ddyup_set (Txy, scr2)
    call ddzup_set (Tzx, scr3)
    call ddxdn1_set (Txx, scr4)
    call ddyup1_set (Txy, scr5)
    call ddzup1_set (Tzx, scr6)
    do iz=izs,ize
      where (d2lnr(:,:,iz) > d2lnr_lim)
        scr1(:,:,iz) = scr4(:,:,iz)
        scr2(:,:,iz) = scr5(:,:,iz)
        scr3(:,:,iz) = scr6(:,:,iz)
      endwhere
    end do
  else
    call ddxdn_set (Txx, scr1)
    call ddyup_set (Txy, scr2)
    call ddzup_set (Tzx, scr3)
  end if
  do iz=izs,ize
    dpxdt(:,:,iz) = dpxdt(:,:,iz) - scr1(:,:,iz) - scr2(:,:,iz) - scr3(:,:,iz)
  end do
    call dumpn (scr1, 'scr1', 'dpxdt.dmp', 1)
    call dumpn (scr2, 'scr2', 'dpxdt.dmp', 1)
    call dumpn (scr3, 'scr3', 'dpxdt.dmp', 1)
    call dumpn (dpxdt, 'dpxdt', 'dpxdt.dmp', 1)

  if (do_2nddiv) then
    call ddydn_set (Tyy, scr1)
    call ddzup_set (Tyz, scr2)
    call ddxup_set (Txy, scr3)
    call ddydn1_set (Tyy, scr4)
    call ddzup1_set (Tyz, scr5)
    call ddxup1_set (Txy, scr6)
    do iz=izs,ize
      where (d2lnr(:,:,iz) > d2lnr_lim)
        scr1(:,:,iz) = scr4(:,:,iz)
        scr2(:,:,iz) = scr5(:,:,iz)
        scr3(:,:,iz) = scr6(:,:,iz)
      endwhere
    end do
  else
    call ddydn_set (Tyy, scr1)
    call ddzup_set (Tyz, scr2)
    call ddxup_set (Txy, scr3)
  end if
  do iz=izs,ize
    dpydt(:,:,iz) = dpydt(:,:,iz) - scr1(:,:,iz) - scr2(:,:,iz) - scr3(:,:,iz)
  end do
      call dumpn (dpydt, '-div(T)', 'dpydt.dmp', 1)

  if (do_2nddiv) then
    call ddzdn_set (Tzz, scr1)
    call ddxup_set (Tzx, scr2)
    call ddyup_set (Tyz, scr3)
    call ddzdn1_set (Tzz, scr4)
    call ddxup1_set (Tzx, scr5)
    call ddyup1_set (Tyz, scr6)
    do iz=izs,ize
      where (d2lnr(:,:,iz) > d2lnr_lim)
        scr1(:,:,iz) = scr4(:,:,iz)
        scr2(:,:,iz) = scr5(:,:,iz)
        scr3(:,:,iz) = scr6(:,:,iz)
      endwhere
    end do
  else
    call ddzdn_set (Tzz, scr1)
    call ddxup_set (Tzx, scr2)
    call ddyup_set (Tyz, scr3)
  end if

  !$omp barrier
  !$omp master
  deallocate (Txx, Tyy, Tzz); narr=narr-3
  deallocate (Txy, Tyz, Tzx); narr=narr-3
  allocate (lne(mx,my,mz)); narr=narr+1; narrm=max(narr,narrm)
  !$omp end master
  !$omp barrier

  do iz=izs,ize
    dpzdt(:,:,iz) = dpzdt(:,:,iz) - scr1(:,:,iz) - scr2(:,:,iz) - scr3(:,:,iz)
    lne(:,:,iz) = alog(ee(:,:,iz))
    scr1(:,:,iz) = alog(p(:,:,iz))
  end do

      call dumpn (dpydt, '-div(T)', 'dpydt.dmp', 1)
      call dumpn (scr4, 'lnPg', 'dpydt.dmp', 1)
  call xdn_set (scr1, scr2)
  call ddxdn_set (scr1, scr3)
  do iz=izs,ize
    dpxdt(:,:,iz) = dpxdt(:,:,iz) - exp(scr2(:,:,iz))*scr3(:,:,iz)
  end do
  call ydn_set (scr1, scr2)
  call ddydn_set (scr1, scr3)
      call dumpn (scr2, 'ydn(lnPg)', 'dpydt.dmp', 1)
      call dumpn (scr3, 'ddydn(lnPg)', 'dpydt.dmp', 1)
  do iz=izs,ize
    dpydt(:,:,iz) = dpydt(:,:,iz) - exp(scr2(:,:,iz))*scr3(:,:,iz)
  end do
  call zdn_set (scr1, scr2)
  call ddzdn_set (scr1, scr3)
  do iz=izs,ize
    dpzdt(:,:,iz) = dpzdt(:,:,iz) - exp(scr2(:,:,iz))*scr3(:,:,iz)
  end do
      call dumpn (dpydt, 'grad(lnp)', 'dpydt.dmp', 1)
                                                                        call timer('pde','face centered eom')
if (do_energy) then
  do iz=izs,ize
  do iy=1,my
  do ix=1,mx
    lnu(ix,iy,iz) = alog(nuE*max(nu(ix,iy,iz)+dxm(ix)*nud(ix,iy,iz),1e-30))
  end do
  end do
  end do
    call dumpn(dedt,'dedt_x','xxx.dmp',1)

!-----------------------------------------------------------------------
!  Energy equation; x-advection and diffusion
!-----------------------------------------------------------------------
  call xdn_set (lnu, scr1)                                              ! lnu = alog(rho*nu)
  call xdn_set (lne, scr6)                                              ! lne = alog(ee)
  call ddxdn_set (lne, scr3)
  if (do_quench) then
    call quenchx (scr3, scr2)
    do iz=izs,ize
    do iy=1,my
    call expn (mx, scr6(:,iy,iz), exp1)
    call expn (mx, scr1(:,iy,iz), exp2)
    do ix=1,mx
      scr3(ix,iy,iz) = exp1(ix)*(px(ix,iy,iz) - dxmdn(ix)*exp2(ix)*scr2(ix,iy,iz))
    end do
    end do
    end do
  else
    do iz=izs,ize
    do iy=1,my
    call expn (mx, scr6(:,iy,iz), exp1)                                 ! exp1 = xdn(ee)
    call expn (mx, scr1(:,iy,iz), exp2)                                 ! exp2 = xdn(rho*nu)
    do ix=1,mx                                                          ! xdn(ee)*(px - ds*xdn(rho*nu)*ddxdn(lnee))
      scr3(ix,iy,iz) = exp1(ix)*(px(ix,iy,iz) - dxmdn(ix)*exp2(ix)*scr3(ix,iy,iz))
    end do
    end do
    end do
  end if

  if (do_2nddiv) then
    call ddxup_set  (scr3, scr1)
    call ddxup1_set (scr3, scr6)
    do iz=izs,ize
      where (d2lnr(:,:,iz) > d2lnr_lim)
        scr1(:,:,iz) = scr6(:,:,iz)
      endwhere
    end do
  else
    call ddxup_set (scr3, scr1)
  end if
  do iz=izs,ize
    dedt(:,:,iz) = dedt(:,:,iz) - scr1(:,:,iz)
  end do
    if (do_energy) call dumpn (dedt, 'x-transp', 'dedt.dmp', 1)
    call dumpn(dedt,'dedt_x','xxx.dmp',1)
    call dumpn(dedt, 'dedt', 'dpydt.dmp', 1)

  do iz=izs,ize
  do iy=1,my
  do ix=1,mx
    lnu(ix,iy,iz) = alog(nuE*max(nu(ix,iy,iz)+dym(iy)*nud(ix,iy,iz),1e-30))
  end do
  end do
  end do

!-----------------------------------------------------------------------
!  In cases with y-stratification, diffuse energy fluctuations relative
!  to the horizontal mean.
!-----------------------------------------------------------------------
  if (do_stratified) then
    call haverage_subr (ee, eeav)
    do iz=izs,ize
      do iy=1,my
        scr6(:,iy,iz) = lne(:,iy,iz)-log(eeav(iy))
      end do
    end do
    call ddydn_set (scr6, scr3)
  else
    call ddydn_set (lne, scr3)
  end if
  call dumpn(lnu,'lnu','xxx.dmp',1)
  call dumpn(lne,'lne','xxx.dmp',1)

!-----------------------------------------------------------------------
!  Energy equation; y-advection and diffusion
!-----------------------------------------------------------------------
  call ydn_set (lnu, scr1)
  call ydn_set (lne, scr2)
  if (do_quench) then
    call quenchy (scr3, scr6)
    do iz=izs,ize
    do iy=1,my
    call expn (mx, scr2(:,iy,iz), exp1)
    call expn (mx, scr1(:,iy,iz), exp2)
    do ix=1,mx
      scr3(ix,iy,iz) = exp1(ix)*(py(ix,iy,iz) - dymdn(iy)*exp2(ix)*scr6(ix,iy,iz))
    end do
    end do
    end do
  else
    do iz=izs,ize
    do iy=1,my
    call expn (mx, scr2(:,iy,iz), exp1)
    call expn (mx, scr1(:,iy,iz), exp2)
    do ix=1,mx
      scr3(ix,iy,iz) = exp1(ix)*(py(ix,iy,iz) - dymdn(iy)*exp2(ix)*scr3(ix,iy,iz))
    end do
    end do
    end do
  end if

  if (do_2nddiv) then
    call ddyup_set  (scr3, scr1)
    call ddyup1_set (scr3, scr6)
    do iz=izs,ize
      where (d2lnr(:,:,iz) > d2lnr_lim)
        scr1(:,:,iz) = scr6(:,:,iz)
      endwhere
    end do
  else
    call ddyup_set (scr3, scr1)
  end if
  do iz=izs,ize
    dedt(:,:,iz) = dedt(:,:,iz) - scr1(:,:,iz)
  end do
    if (do_energy) call dumpn (dedt, 'y-transp', 'dedt.dmp', 1)
    call dumpn(dedt,'dedt_y','xxx.dmp',1)
    call dumpn(dedt, 'dedt', 'dpydt.dmp', 1)

  do iz=izs,ize
  do iy=1,my
  do ix=1,mx
    lnu(ix,iy,iz) = alog(nuE*max(nu(ix,iy,iz)+dzm(iz)*nud(ix,iy,iz),1e-30))
  end do
  end do
  end do

!-----------------------------------------------------------------------
!  Energy equation; z-advection and diffusion
!-----------------------------------------------------------------------
  call barrier_omp('pde23')                                             ! barrier 17, lne??? (quench)
  call ddzdn_set (lne, scr3)
  if (do_quench) then
    call quenchz (scr3, scr6, scr1)
    call zdn_set (lnu, scr1)
    call zdn_set (lne, scr2)
    do iz=izs,ize
    do iy=1,my
    call expn (mx, scr2(:,iy,iz), exp1)
    call expn (mx, scr1(:,iy,iz), exp2)
    do ix=1,mx
      scr3(ix,iy,iz) = exp1(ix)*(pz(ix,iy,iz) - dzmdn(iz)*exp2(ix)*scr6(ix,iy,iz))
    end do
    end do
    end do
  else
    call zdn_set (lnu, scr1)
    call zdn_set (lne, scr2)
    do iz=izs,ize
    do iy=1,my
    call expn (mx, scr2(:,iy,iz), exp1)
    call expn (mx, scr1(:,iy,iz), exp2)
    do ix=1,mx
      scr3(ix,iy,iz) = exp1(ix)*(pz(ix,iy,iz) - dzmdn(iz)*exp2(ix)*scr3(ix,iy,iz))
    end do
    end do
    end do
  end if

  call barrier_omp('pde26')                                             ! barrier 20, scr3 set above
  if (do_2nddiv) then
    call ddzup_set  (scr3, scr1)
    call ddzup1_set (scr3, scr6)
    do iz=izs,ize
      where (d2lnr(:,:,iz) > d2lnr_lim)
        scr1(:,:,iz) = scr6(:,:,iz)
      endwhere
    end do
  else
    call ddzup_set (scr3, scr1)
  end if

  !$omp barrier
  !$omp master
  deallocate (nu); narr=narr-1
  deallocate (nud); narr=narr-1
  deallocate (lnu); narr=narr-1
  deallocate (d2lnr); narr=narr-1
  allocate (dd(mx,my,mz)); narr=narr+1; narrm=max(narr,narrm)
  !$omp end master
  !$omp barrier

  do iz=izs,ize
    dedt(:,:,iz) = dedt(:,:,iz) - scr1(:,:,iz)
  end do
    call dumpn (dedt, 'z-transp', 'dedt.dmp', 1)
    Call dumpn(dedt,'dedt_z','xxx.dmp',1)
    call dumpn(dedt, 'dedt', 'dpydt.dmp', 1)
                                                                        call timer('pde','e_adv+diff')

!-----------------------------------------------------------------------
!  Cooling and conduction
!-----------------------------------------------------------------------
  call coolit (r, ee, lne, dd, dedt)

  !$omp end master
  !$omp master
  deallocate (ee); narr=narr-1
  deallocate (lne); narr=narr-1
  deallocate (dd); narr=narr-1
  !$omp end master
  !$omp barrier

    call dumpn (dedt, 'coolit', 'dedt.dmp', 1)
    call dumpn(dedt, 'dedt', 'dpydt.dmp', 1)
                                                                        call timer('pde','cooling')
end if  ! if (do_energy)

!-----------------------------------------------------------------------
!  MHD
!-----------------------------------------------------------------------
    call dumpn (dpxdt, 'dpxdt', 'dpxdt.dmp', 1)
  if (do_mhd) then
    !$omp master
    allocate (Ex(mx,my,mz), Ey(mx,my,mz), Ez(mx,my,mz)); narr=narr+3; narrm=max(narr,narrm)
    !$omp end master
    !$omp barrier

    call mhd (flag, &
            r,e,p,Ux,Uy,Uz,Bx,By,Bz,Ex,Ey,Ez, &
            dpxdt,dpydt,dpzdt,dedt,dBxdt,dBydt,dBzdt)

    !$omp barrier
    !$omp master
    deallocate (Ex, Ey, Ez); narr=narr-3
    !$omp end master
    !$omp barrier

      call dumpn (dpydt, 'mhd', 'dpydt.dmp', 1)
      call dumpn (dedt, 'dedt', 'dpydt.dmp', 1)
      call dumpn (dedt, 'mhd', 'dedt.dmp', 1)
  end if

!-----------------------------------------------------------------------
!  Boundary conditions applied to time derivatives
!-----------------------------------------------------------------------
    call dumpn (dpxdt, 'dpxdt', 'dpxdt.dmp', 1)
    call dumpn (drdt,'drdt','xxx.dmp',1)
    call ddt_boundary (r,px,py,pz,e,p,Bx,By,Bz, &
                     drdt,dpxdt,dpydt,dpzdt, &
                     dedt,dBxdt,dBydt,dBzdt)
    !$omp barrier
    !$omp master
    deallocate (p); narr=narr-1
    !$omp end master
    !$omp barrier

    call dumpn (dedt, 'ddt-bdry', 'dedt.dmp', 1)
    call dumpn (dpxdt, 'dpxdt', 'dpxdt.dmp', 1)
    call dumpn (drdt,'drdt','xxx.dmp',1)
    call dumpn (dedt,'dedt','xxx.dmp',1)

    call dumpn (drdt,'drdt','Ux.dmp',1)
    call dumpn (dpxdt,'dpxdt','Ux.dmp',1)
                                                                        call timer('pde','ddt_bdry')
  call output_fluxes
                                                                        call timer('pde','out_flux')
  if (first_time .and. master) then
    ram = 4.*narrm*mx*my*mz
    if (ram > 1e9) then
      print'(f7.3,a)',ram/1e9,' GB per thread'
    else
      print'(f7.3,a)',ram/1e6,' MB per thread'
    end if
  end if
  first_time = .false.
END
