!***********************************************************************
FUNCTION ddzupi (f)
  use params
  implicit none
  real a, b, c, a1st
  integer i, j, k, km2, km1, kp1, kp2, kp3
  real, dimension(mx,my,mz):: f, ddzupi
!hpf$ distribute(*,*,block):: f, ddzupi
!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    ddzupi=0.
    return
  endif
!
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)
  a1st = 1.
!
!Xomp barrier
!$omp parallel private(i,j,k,km2,km1,kp1,kp2,kp3)
  do k=izs,ize
    km2=mod(k-3+mz,mz)+1
    km1=mod(k-2+mz,mz)+1
    kp1=mod(k     ,mz)+1
    kp2=mod(k+1   ,mz)+1
    kp3=mod(k+2   ,mz)+1
    do j=1,my
      do i=1,mx
        ddzupi(i,j,k) = ( &
                     a*(f(i,j,kp1)-f(i,j,k  )) + &
                     b*(f(i,j,kp2)-f(i,j,km1)) + &
                     c*(f(i,j,kp3)-f(i,j,km2)))
      end do
    end do
  end do
!$omp end parallel
!
  nstag = nstag+1
  nflop = nflop+8
END FUNCTION

!***********************************************************************
FUNCTION ddzupi1 (f)
  use params
  implicit none
  real a, b, c, a1st
  integer i, j, k, kp1
  real, dimension(mx,my,mz):: f, ddzupi1
!hpf$ distribute(*,*,block):: f, ddzupi1
!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    ddzupi1=0.
    return
  endif
!
  a = 1.
!
!Xomp barrier
!$omp parallel private(i,j,k,kp1)
  do k=izs,ize
    kp1=mod(k     ,mz)+1
    do j=1,my
      do i=1,mx
        ddzupi1(i,j,k) = ( &
                     a*(f(i,j,kp1)-f(i,j,k  )))
      end do
    end do
  end do
!$omp end parallel
!
  nstag1 = nstag1+1
  nflop = nflop+2
END FUNCTION

!***********************************************************************
FUNCTION ddzdni (f)
  use params
  implicit none
  real a, b, c, a1st
  integer i, j, k, km3, km2, km1, kp1, kp2
  real, dimension(mx,my,mz):: f, ddzdni
!hpf$ distribute(*,*,block):: f, ddzdni
!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    ddzdni=0.
    return
  endif
!
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)
  a1st = 1.
!
!Xomp barrier
!$omp parallel private(i,j,k,km3,km2,km1,kp1,kp2)
  do k=izs,ize
    km3=mod(k-4+mz,mz)+1
    km2=mod(k-3+mz,mz)+1
    km1=mod(k-2+mz,mz)+1
    kp1=mod(k     ,mz)+1
    kp2=mod(k+1   ,mz)+1
    do j=1,my
      do i=1,mx
        ddzdni(i,j,k) = ( &
                     a*(f(i,j,k  )-f(i,j,km1)) + &
                     b*(f(i,j,kp1)-f(i,j,km2)) + &
                     c*(f(i,j,kp2)-f(i,j,km3)))
      end do
    end do
  end do
!$omp end parallel
!
  nstag = nstag+1
  nflop = nflop+8
END FUNCTION

!***********************************************************************
FUNCTION ddzdni1 (f)
  use params
  implicit none
  real a, b, c, a1st
  integer i, j, k, km1
  real, dimension(mx,my,mz):: f, ddzdni1
!hpf$ distribute(*,*,block):: f, ddzdni1
!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    ddzdni1=0.
    return
  endif
!
  a = 1.
!
!Xomp barrier
!$omp parallel private(i,j,k,km1)
  do k=izs,ize
    km1=mod(k-2+mz,mz)+1
    do j=1,my
      do i=1,mx
        ddzdni1(i,j,k) = ( &
                     a*(f(i,j,k  )-f(i,j,km1)))
      end do
    end do
  end do
!$omp end parallel
!
  nstag1 = nstag1+1
  nflop = nflop+2
END FUNCTION
!***********************************************************************
FUNCTION ddzupi32 (f,j)
  use params
  implicit none
  real a, b, c, a1st
  integer i, j, k, km2, km1, kp1, kp2, kp3
  real, dimension(mx,my,mz):: f
  real, dimension(mx,mz):: ddzupi32
!hpf$ distribute(*,*,block):: f, ddzupi
!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    ddzupi32=0.
    return
  endif
!
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)
  a1st = 1.
!
!Xomp barrier
!$omp parallel private(i,j,k,km2,km1,kp1,kp2,kp3)
  do k=izs,ize
    km2=mod(k-3+mz,mz)+1
    km1=mod(k-2+mz,mz)+1
    kp1=mod(k     ,mz)+1
    kp2=mod(k+1   ,mz)+1
    kp3=mod(k+2   ,mz)+1
      do i=1,mx
        ddzupi32(i,k) = ( &
                     a*(f(i,j,kp1)-f(i,j,k  )) + &
                     b*(f(i,j,kp2)-f(i,j,km1)) + &
                     c*(f(i,j,kp3)-f(i,j,km2)))
      end do
  end do
!$omp end parallel
!
  nstag = nstag+1
  nflop = nflop+8
END FUNCTION

!***********************************************************************
FUNCTION ddzupi132 (f,j)
  use params
  implicit none
  real a, b, c, a1st
  integer i, j, k, kp1
  real, dimension(mx,my,mz):: f
  real, dimension(mx,mz):: ddzupi132
!hpf$ distribute(*,*,block):: f, ddzupi1
!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    ddzupi132=0.
    return
  endif
!
  a = 1.
!
!Xomp barrier
!$omp parallel private(i,j,k,kp1)
  do k=izs,ize
    kp1=mod(k     ,mz)+1
      do i=1,mx
        ddzupi132(i,k) = ( &
                     a*(f(i,j,kp1)-f(i,j,k  )))
      end do
  end do
!$omp end parallel
!
  nstag1 = nstag1+1
  nflop = nflop+2
END FUNCTION

!***********************************************************************
FUNCTION ddzdni32 (f,j)
  use params
  implicit none
  real a, b, c, a1st
  integer i, j, k, km3, km2, km1, kp1, kp2
  real, dimension(mx,my,mz):: f
  real, dimension(mx,mz):: ddzdni32
!hpf$ distribute(*,*,block):: f, ddzdni
!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    ddzdni32=0.
    return
  endif
!
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)
  a1st = 1.
!
!Xomp barrier
!$omp parallel private(i,j,k,km3,km2,km1,kp1,kp2)
  do k=izs,ize
    km3=mod(k-4+mz,mz)+1
    km2=mod(k-3+mz,mz)+1
    km1=mod(k-2+mz,mz)+1
    kp1=mod(k     ,mz)+1
    kp2=mod(k+1   ,mz)+1
      do i=1,mx
        ddzdni32(i,k) = ( &
                     a*(f(i,j,k  )-f(i,j,km1)) + &
                     b*(f(i,j,kp1)-f(i,j,km2)) + &
                     c*(f(i,j,kp2)-f(i,j,km3)))
      end do
  end do
!$omp end parallel
!
  nstag = nstag+1
  nflop = nflop+8
END FUNCTION

!***********************************************************************
FUNCTION ddzdni132 (f,j)
  use params
  implicit none
  real a, b, c, a1st
  integer i, j, k, km1
  real, dimension(mx,my,mz):: f
  real, dimension(mx,mz):: ddzdni132
!hpf$ distribute(*,*,block):: f, ddzdni1
!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    ddzdni132=0.
    return
  endif
!
  a = 1.
!
!Xomp barrier
!$omp parallel private(i,j,k,km1)
  do k=izs,ize
    km1=mod(k-2+mz,mz)+1
      do i=1,mx
        ddzdni132(i,k) = ( &
                     a*(f(i,j,k  )-f(i,j,km1)))
      end do
  end do
!$omp end parallel
!
  nstag1 = nstag1+1
  nflop = nflop+2
END FUNCTION
!***********************************************************************
FUNCTION ddzupi22 (f)
  use params
  implicit none
  real a, b, c, a1st
  integer i, j, k, km2, km1, kp1, kp2, kp3
  real, dimension(mx,mz):: ddzupi22, f
!hpf$ distribute(*,*,block):: f, ddzupi
!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    ddzupi22=0.
    return
  endif
!
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)
  a1st = 1.
!
!Xomp barrier
!$omp parallel private(i,j,k,km2,km1,kp1,kp2,kp3)
  do k=izs,ize
    km2=mod(k-3+mz,mz)+1
    km1=mod(k-2+mz,mz)+1
    kp1=mod(k     ,mz)+1
    kp2=mod(k+1   ,mz)+1
    kp3=mod(k+2   ,mz)+1
      do i=1,mx
        ddzupi22(i,k) = ( &
                     a*(f(i,kp1)-f(i,k  )) + &
                     b*(f(i,kp2)-f(i,km1)) + &
                     c*(f(i,kp3)-f(i,km2)))
      end do
  end do
!$omp end parallel
!
  nstag = nstag+1
  nflop = nflop+8
END FUNCTION

!***********************************************************************
FUNCTION ddzupi122 (f)
  use params
  implicit none
  real a, b, c, a1st
  integer i, j, k, kp1
  real, dimension(mx,mz):: ddzupi122, f
!hpf$ distribute(*,*,block):: f, ddzupi1
!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    ddzupi122=0.
    return
  endif
!
  a = 1.
!
!Xomp barrier
!$omp parallel private(i,k,kp1)
  do k=izs,ize
    kp1=mod(k     ,mz)+1
      do i=1,mx
        ddzupi122(i,k) = ( &
                     a*(f(i,kp1)-f(i,k  )))
      end do
  end do
!$omp end parallel
!
  nstag1 = nstag1+1
  nflop = nflop+2
END FUNCTION

!***********************************************************************
FUNCTION ddzdni22 (f)
  use params
  implicit none
  real a, b, c, a1st
  integer i, j, k, km3, km2, km1, kp1, kp2
  real, dimension(mx,mz):: ddzdni22, f
!hpf$ distribute(*,*,block):: f, ddzdni
!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    ddzdni22=0.
    return
  endif
!
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)
  a1st = 1.
!
!Xomp barrier
!$omp parallel private(i,k,km3,km2,km1,kp1,kp2)
  do k=izs,ize
    km3=mod(k-4+mz,mz)+1
    km2=mod(k-3+mz,mz)+1
    km1=mod(k-2+mz,mz)+1
    kp1=mod(k     ,mz)+1
    kp2=mod(k+1   ,mz)+1
      do i=1,mx
        ddzdni22(i,k) = ( &
                     a*(f(i,k  )-f(i,km1)) + &
                     b*(f(i,kp1)-f(i,km2)) + &
                     c*(f(i,kp2)-f(i,km3)))
      end do
  end do
!$omp end parallel
!
  nstag = nstag+1
  nflop = nflop+8
END FUNCTION

!***********************************************************************
FUNCTION ddzdni122 (f)
  use params
  implicit none
  real a, b, c, a1st
  integer i, j, k, km1
  real, dimension(mx,mz):: ddzdni122, f
!hpf$ distribute(*,*,block):: f, ddzdni1
!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    ddzdni122=0.
    return
  endif
!
  a = 1.
!
!Xomp barrier
!$omp parallel private(i,k,km1)
  do k=izs,ize
    km1=mod(k-2+mz,mz)+1
      do i=1,mx
        ddzdni122(i,k) = ( &
                     a*(f(i,k  )-f(i,km1)))
      end do
  end do
!$omp end parallel
!
  nstag1 = nstag1+1
  nflop = nflop+2
END FUNCTION
