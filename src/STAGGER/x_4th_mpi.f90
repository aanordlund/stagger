!***********************************************************************
FUNCTION xup (f)
!
!  f is centered on (i-.5,j,k), fifth order stagger
!
  use params, g=>gx1, h=>hx2
  implicit none
  real a, b
  integer i, j, k
  real, dimension(mx,my,mz):: f, xup
!-----------------------------------------------------------------------
!
  character(len=mid):: id = "$Id: x_4th_mpi.f90,v 1.4 2012/08/05 11:50:43 aake Exp $" 
  call print_id (id)
!
  if (mx.le.5) then
    xup = f
    return
  end if
  b = -1./16.
  a = .5-b

!$omp parallel private(i,j,k)

  call mpi_send_x (f, g, 1, h, 2)

  do k=izs,ize
   do j=1,my
    xup(mx-1,j,k) = ( &
                   b*(f(mx-2,j,k)+h(1   ,j,k)) &
                 + a*(f(mx-1,j,k)+f(mx  ,j,k)))
    xup(mx  ,j,k) = ( &
                   b*(f(mx-1,j,k)+h(2   ,j,k)) &
                 + a*(f(mx  ,j,k)+h(1   ,j,k)))
    xup(1   ,j,k) = ( &
                   b*(g(1   ,j,k)+f(3   ,j,k)) &
                 + a*(f(1   ,j,k)+f(2   ,j,k)))
   end do
   do j=1,my
    do i=2,mx-2
      xup(i  ,j,k) = ( &
                    b*(f(i-1,j,k)+f(i+2 ,j,k)) &
                  + a*(f(i  ,j,k)+f(i+1 ,j,k)))
    end do
   end do
  end do
!$omp end parallel
!
  nstag = nstag+1
  nflop = nflop+8
END FUNCTION

!***********************************************************************
FUNCTION xup1 (f)
!
!  f is centered on (i-.5,j,k), first order stagger
!
  use params, h=>hx1
  implicit none
  real a
  integer i, j, k
  real, dimension(mx,my,mz):: f, xup1
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    xup1 = f
    return
  end if
!
  a = .5

!$omp parallel private(i,j,k)

  call mpi_send_x (f, h, 0, h, 1)
!
  do k=izs,ize
   do j=1,my
    xup1(mx  ,j,k) =  ( &
                    a*(f(mx,j,k)+h(  1,j,k)))
   end do
   do j=1,my
    do i=1,mx-1
      xup1(i  ,j,k) = ( &
                    a*(f(i ,j,k)+f(i+1,j,k)))
    end do
   end do
  end do
!$omp end parallel
!
  nstag1 = nstag1+1
  nflop = nflop+2
END FUNCTION

!***********************************************************************
FUNCTION xdn (f)
!
!  f is centered on (i,j,k), fifth order stagger
!
  use params, g=>gx2, h=>hx1
  implicit none
  real a, b
  integer i, j, k
  real, dimension(mx,my,mz):: f, xdn
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    xdn = f
    return
  end if
!
  b = -1./16.
  a = .5-b

!$omp parallel private(i,j,k)

  call mpi_send_x (f, g, 2, h, 1)
!
  do k=izs,ize
   do j=1,my
    xdn(mx  ,j,k) = b*(f(mx-2,j,k)+h(1   ,j,k)) &
                  + a*(f(mx-1,j,k)+f(mx  ,j,k))
    xdn(1   ,j,k) = b*(g(1   ,j,k)+f(2   ,j,k)) &
                  + a*(g(2   ,j,k)+f(1   ,j,k))
    xdn(2   ,j,k) = b*(g(2   ,j,k)+f(3   ,j,k)) &
                  + a*(f(1   ,j,k)+f(2   ,j,k))
   end do
   do j=1,my
    do i=3,mx-1
      xdn(i,j,k) = b*(f(i+1 ,j,k)+f(i-2 ,j,k)) &
                 + a*(f(i   ,j,k)+f(i-1 ,j,k))
    end do
   end do
  end do
!$omp end parallel
!
!
  nstag = nstag+1
  nflop = nflop+8
END FUNCTION

!***********************************************************************
FUNCTION xdn1 (f)
!
!  f is centered on (i,j,k), first order stagger
!
  use params, g=>gx1
  implicit none
  real a
  integer i, j, k
  real, dimension(mx,my,mz):: f, xdn1
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    xdn1 = f
    return
  end if
!
  a = 0.5

!$omp parallel private(i,j,k)

  call mpi_send_x (f, g, 1, g, 0)
!
  do k=izs,ize
   do j=1,my
    xdn1(1  ,j,k) = ( &
                   a*(g(1,j,k)+f(1   ,j,k)))
   end do
   do j=1,my
    do i=2,mx
      xdn1(i,j,k) = ( &
                   a*(f(i,j,k)+f(i-1,j,k)))
    end do
   end do
  end do
!$omp end parallel
!
  nstag1 = nstag1+1
  nflop = nflop+2
END FUNCTION

!***********************************************************************
FUNCTION xup32 (f,j)
!
!  f is centered on (i-.5,j,k), fifth order stagger
!
  use params, g=>gx1, h=>hx2
  implicit none
  real a, b
  integer i, j, k
  real, dimension(mx,my,mz):: f
  real, dimension(mx,mz):: xup32
!-----------------------------------------------------------------------
!
  character(len=mid):: id = "$Id: x_4th_mpi.f90,v 1.4 2012/08/05 11:50:43 aake Exp $" 
  call print_id (id)
!
  if (mx.le.5) then
    xup32 = f(:,j,:)
    return
  end if
  b = -1./16.
  a = .5-b
!
!$omp parallel private(i,j,k)

  call mpi_send_x (f, g, 1, h, 2)

  do k=izs,ize
    xup32(mx-1,k) = b*(f(mx-2,j,k)+h(1   ,j,k)) &
                  + a*(f(mx-1,j,k)+f(mx  ,j,k))
    xup32(mx  ,k) = b*(f(mx-1,j,k)+h(2   ,j,k)) &
                  + a*(f(mx  ,j,k)+h(1   ,j,k))
    xup32(1   ,k) = b*(g(1   ,j,k)+f(3   ,j,k)) &
                  + a*(f(1   ,j,k)+f(2   ,j,k))
    do i=2,mx-2
      xup32(i  ,k) = b*(f(i-1,j,k)+f(i+2 ,j,k)) &
                   + a*(f(i  ,j,k)+f(i+1 ,j,k))
    end do
  end do
!$omp end parallel
!
  if (omp_master) then
      nstag = nstag+1
      nflop = nflop+8
  end if
END FUNCTION

!***********************************************************************
FUNCTION xup132 (f,j)
!
!  f is centered on (i-.5,j,k), fifth order stagger
!
  use params, h=>hx1
  implicit none
  real a
  integer i, j, k
  real, dimension(mx,my,mz):: f
  real, dimension(mx,mz):: xup132
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    xup132 = f(:,j,:)
    return
  end if
!
  a = .5
!
!$omp parallel private(i,j,k)

  call mpi_send_x (f, h, 0, h, 1)

  do k=izs,ize
    xup132(mx  ,k) = ( &
                   a*(f(mx  ,j,k)+h(1   ,j,k)))
    do i=1,mx-1
      xup132(i  ,k) = ( &
                    a*(f(i  ,j,k)+f(i+1 ,j,k)))
    end do
  end do
!$omp end parallel
!
  if (omp_master) then
    nstag1 = nstag1+1
    nflop = nflop+2
  end if
END FUNCTION

!***********************************************************************
FUNCTION xdn32 (f,j)
!
!  f is centered on (i,j,k), fifth order stagger
!
  use params, g=>gx2, h=>hx1
  implicit none
  real a, b
  integer i, j, k
  real, dimension(mx,my,mz):: f
  real, dimension(mx,mz):: xdn32
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    xdn32 = f(:,j,:)
    return
  end if
!
  b = -1./16.
  a = .5-b
!
!$omp parallel private(i,j,k)

  call mpi_send_x (f, g, 2, h, 1)

  do k=izs,ize
    xdn32(mx  ,k) = b*(f(mx-2,j,k)+h(1   ,j,k)) &
                  + a*(f(mx-1,j,k)+f(mx  ,j,k))
    xdn32(1   ,k) = b*(g(1   ,j,k)+f(2   ,j,k)) &
                  + a*(g(2   ,j,k)+f(1   ,j,k))
    xdn32(2   ,k) = b*(g(2   ,j,k)+f(3   ,j,k)) &
                  + a*(f(1   ,j,k)+f(2   ,j,k))
    do i=3,mx-1
      xdn32(i,k) = b*(f(i+1 ,j,k)+f(i-2 ,j,k)) &
                 + a*(f(i   ,j,k)+f(i-1 ,j,k))
    end do
  end do
!$omp end parallel
!
!
  if (omp_master) then
    nstag = nstag+1
    nflop = nflop+8
  end if
END FUNCTION

!***********************************************************************
FUNCTION xdn132 (f,j)
!
!  f is centered on (i,j,k), fifth order stagger
!
  use params, g=>gx1
  implicit none
  real a
  integer i, j, k
  real, dimension(mx,my,mz):: f
  real, dimension(mx,mz):: xdn132
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    xdn132 = f(:,j,:)
    return
  end if
!
  a = 0.5
!
!$omp parallel private(i,j,k)

  call mpi_send_x (f, g, 1, g, 0)

  do k=izs,ize
    xdn132(1  ,k) = ( &
                   a*(g(1   ,j,k)+f(1   ,j,k)))
    do i=2,mx
      xdn132(i,k) = ( &
                   a*(f(i   ,j,k)+f(i-1 ,j,k)))
    end do
  end do
!$omp end parallel
!
  if (omp_master) then
    nstag1 = nstag1+1
    nflop = nflop+2
  end if
END FUNCTION

!***********************************************************************
FUNCTION xup22 (f)
!
!  f is centered on (i-.5,k), fifth order stagger
!
  use params, g=>gx1, h=> hx2
  implicit none
  real a, b
  integer i, j, k
  real, dimension(mx,mz):: xup22, f
!-----------------------------------------------------------------------
!
  character(len=mid):: id = "$Id: x_4th_mpi.f90,v 1.4 2012/08/05 11:50:43 aake Exp $" 
  call print_id (id)
!
  if (mx.le.5) then
    xup22 = f
    return
  end if
  b = -1./16.
  a = .5-b
!
!$omp parallel private(i,k)

  call mpi_send2_x (f, mx, 1, mz, g, 1, h, 2)

  do k=izs,ize
    xup22(mx-1,k) = b*(f(mx-2,k)+h(1 ,1,k)) &
                  + a*(f(mx-1,k)+f(mx  ,k))
    xup22(mx  ,k) = b*(f(mx-1,k)+h(2 ,1,k)) &
                  + a*(f(mx  ,k)+h(1 ,1,k))
    xup22(1   ,k) = b*(g(1 ,1,k)+f(3   ,k)) &
                  + a*(f(1   ,k)+f(2   ,k))
    do i=2,mx-2
      xup22(i  ,k) = b*(f(i-1,k)+f(i+2 ,k)) &
                   + a*(f(i  ,k)+f(i+1 ,k))
    end do
  end do
!$omp end parallel
!
  if (omp_master) then
      nstag = nstag+1
      nflop = nflop+8
  end if
END FUNCTION

!***********************************************************************
FUNCTION xup122 (f)
!
!  f is centered on (i-.5,k), fifth order stagger
!
  use params, h=> hx1
  implicit none
  real a
  integer i, j, k
  real, dimension(mx,mz):: xup122, f
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    xup122 = f
    return
  end if
!
  a = .5
!
!$omp parallel private(i,k)

  call mpi_send2_x (f, mx, 1, mz, h, 0, h, 1)

  do k=izs,ize
    xup122(mx  ,k) = ( &
                   a*(f(mx  ,k)+h(1 ,1,k)))
    do i=1,mx-1
      xup122(i  ,k) = ( &
                    a*(f(i  ,k)+f(i+1 ,k)))
    end do
  end do
!$omp end parallel
!
  if (omp_master) then
    nstag1 = nstag1+1
    nflop = nflop+2
  end if
END FUNCTION

!***********************************************************************
FUNCTION xdn22 (f)
!
!  f is centered on (i,k), fifth order stagger
!
  use params, g=>gx2, h=>hx1
  implicit none
  real a, b
  integer i, j, k
  real, dimension(mx,mz):: xdn22, f
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    xdn22 = f
    return
  end if
!
  b = -1./16.
  a = .5-b
!
!$omp parallel private(i,k)

  call mpi_send2_x (f, mx, 1, mz, g, 2, h, 1)

  do k=izs,ize
    xdn22(mx  ,k) = b*(f(mx-2,k)+h(1 ,1,k)) &
                  + a*(f(mx-1,k)+f(mx  ,k))
    xdn22(1   ,k) = b*(g(1 ,1,k)+f(2   ,k)) &
                  + a*(g(2 ,1,k)+f(1   ,k))
    xdn22(2   ,k) = b*(g(2 ,1,k)+f(3   ,k)) &
                  + a*(f(1   ,k)+f(2   ,k))
    do i=3,mx-1
      xdn22(i,k) = b*(f(i+1 ,k)+f(i-2 ,k)) &
                 + a*(f(i   ,k)+f(i-1 ,k))
    end do
  end do
!$omp end parallel
!
!
  if (omp_master) then
    nstag = nstag+1
    nflop = nflop+8
  end if
END FUNCTION

!***********************************************************************
FUNCTION xdn122 (f)
!
!  f is centered on (i,k), fifth order stagger
!
  use params, g=>gx1
  implicit none
  real a
  integer i, j, k
  real, dimension(mx,mz):: xdn122, f
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    xdn122 = f
    return
  end if
!
  a = 0.5
!
!$omp parallel private(i,k)

  call mpi_send2_x (f, mx, 1, mz, g, 1, g, 0)

  do k=izs,ize
    xdn122(1  ,k) = ( &
                   a*(g(1 ,1,k)+f(1   ,k)))
    do i=2,mx
      xdn122(i,k) = ( &
                   a*(f(i   ,k)+f(i-1 ,k)))
    end do
  end do
!$omp end parallel
!
  if (omp_master) then
    nstag1 = nstag1+1
    nflop = nflop+2
  end if
END FUNCTION
