
!***********************************************************************
SUBROUTINE d2abs_set (f, fp)
!
!  Max abs difference one zone left to one zones right.
!

  USE params
  implicit none
  logical omp_in_parallel
  real cx, cy, cz
  real, dimension(mx,my,mz):: f, fp
  real, dimension(mx,my,1):: gz, hz
!hpf$ align with TheCube:: f, fp
  integer i, j, k, km1, kp1
!-----------------------------------------------------------------------

!$omp parallel private(i,j,k,km1,kp1)

  if (mx.gt.1) then
    do k=izs,ize
      do j=1,my
        do i=2,mx-1
          fp(i,j,k) = abs(f(i-1 ,j,k)+f(i+1,j,k)-f(i,j,k)-f(i,j,k))
        end do
        fp(1 ,j,k)  = abs(gx1(1   ,j,k)+f (2,j,k)-f(1 ,j,k)-f(1 ,j,k))
        fp(mx,j,k)  = abs(f (mx-1,j,k)+hx1(1,j,k)-f(mx,j,k)-f(mx,j,k))
      end do
    end do
  else
    do k=izs,ize
      do j=1,my
        do i=1,mx
          fp(i,j,k) = 0.
        end do
      end do
    end do
  end if

 if (my .gt. 1) then
  do k=izs,ize
    do j=2,my-1
      do i=1,mx
        fp(i,j, k) = max(fp(i,j, k),abs(f(i,j -1,k)+f(i,j+1,k)-f(i,j,k)-f(i,j,k)))
      end do
    end do
    do i=1,mx
        fp(i,1 ,k) = max(fp(i,1 ,k),abs(gy1(i,   1,k)+f (i,2,k)-f(i, 1,k)-f(i, 1,k)))
        fp(i,my,k) = max(fp(i,my,k),abs(f (i,my-1,k)+hy1(i,1,k)-f(i,my,k)-f(i,my,k)))
    end do
  end do
 end if
 if (mz .gt. 1) then
!$omp barrier
  do k=izs,ize
    if (k.eq.1) then
      do j=1,my
      do i=1,mx
        fp(i,j,k ) = max(fp(i,j,k ),abs(gz(i,j,1)+f(i,j,k+1)-f(i,j,k)-f(i,j,k)))
      end do
      end do
    else if (k.eq.mz) then
      do j=1,my
      do i=1,mx
        fp(i,j,k ) = max(fp(i,j,k ),abs(hz(i,j,1)+f(i,j,k-1)-f(i,j,k)-f(i,j,k)))
      end do
      end do
    else
      do j=1,my
      do i=1,mx
        fp(i,j,k ) = max(fp(i,j,k ),abs(f(i,j,k-1)+f(i,j,k+1)-f(i,j,k)-f(i,j,k)))
      end do
      end do
    end if
  end do
 end if
!$omp end parallel

END SUBROUTINE
!***********************************************************************
SUBROUTINE dif2a_set (f, fp)
!
!  Max abs difference one zone left to one zones right.
!

  USE params
  implicit none
  logical omp_in_parallel
  real cx, cy, cz
  real, dimension(mx,my,mz):: f, fp
  real, dimension(mx,my,1):: gz, hz
!hpf$ align with TheCube:: f, fp
  integer i, j, k, km1, kp1
!-----------------------------------------------------------------------

!$omp parallel private(i,j,k,km1,kp1)

  if (mx.gt.1) then
    do k=izs,ize
      do j=1,my
        do i=2,mx-1
          fp(i,j,k) = abs(f(i-1 ,j,k)-f(i+1,j,k))
        end do
        fp(1 ,j,k)  = abs(gx1(1   ,j,k)-f (2,j,k))
        fp(mx,j,k)  = abs(f (mx-1,j,k)-hx1(1,j,k))
      end do
    end do
  else
    do k=izs,ize
      do j=1,my
        do i=1,mx
          fp(i,j,k) = 0.
        end do
      end do
    end do
  end if

 if (my .gt. 1) then
  do k=izs,ize
    do j=2,my-1
      do i=1,mx
        fp(i,j, k) = max(fp(i,j, k),abs(f(i,j -1,k)-f(i,j+1,k)))
      end do
    end do
    do i=1,mx
        fp(i,1 ,k) = max(fp(i,1 ,k),abs(gy1(i,   1,k)-f (i,2,k)))
        fp(i,my,k) = max(fp(i,my,k),abs(f (i,my-1,k)-hy1(i,1,k)))
    end do
  end do
 end if
 if (mz .gt. 1) then
!$omp barrier
  do k=izs,ize
    if (k.eq.1) then
      do j=1,my
      do i=1,mx
        fp(i,j,k ) = max(fp(i,j,k ),abs(gz(i,j,1)-f(i,j,k+1)))
      end do
      end do
    else if (k.eq.mz) then
      do j=1,my
      do i=1,mx
        fp(i,j,k ) = max(fp(i,j,k ),abs(hz(i,j,1)-f(i,j,k-1)))
      end do
      end do
    else
      do j=1,my
      do i=1,mx
        fp(i,j,k ) = max(fp(i,j,k ),abs(f(i,j,k-1)-f(i,j,k+1)))
      end do
      end do
    end if
  end do
 end if
!$omp end parallel

END SUBROUTINE

!***********************************************************************
SUBROUTINE dif2_set (fx, fy, fz, fp)
!
!  Finite difference one zone left to one zones right.
!

  USE params
  implicit none
  logical omp_in_parallel
  real cx(mx), cy(my), cz(mz)
  real, dimension(mx,my,mz):: fx, fy, fz, fp
  real, dimension(mx,my,1):: gz, hz
  integer i, j, k, km1, kp1
!-----------------------------------------------------------------------


!$omp parallel private(i,j,k,km1,kp1)


  if (mx.gt.1) then
    do k=izs,ize
      do j=1,my
        do i=2,mx-1
          fp(i,j,k) = cx(i)*(fx(i-1 ,j,k)-fx(i+1,j,k))
        end do
        fp(1 ,j,k)  = cx( 1)*(gx1(1   ,j,k)-fx(2  ,j,k))
        fp(mx,j,k)  = cx(mx)*(fx(mx-1,j,k)-hx1(1  ,j,k))
      end do
    end do
  else
    do k=izs,ize
      do j=1,my
        do i=1,mx
          fp(i,j,k) = 0.
        end do
      end do
    end do
  end if


  if (my .gt. 1) then
    do k=izs,ize
     do j=2,my-1
       do i=1,mx
       end do
     end do
     do i=1,mx
     end do
    end do
  end if


 if (mz .gt. 1) then
!$omp barrier
  do k=izs,ize
    if (k.eq.1) then
      do j=1,my
      do i=1,mx
      end do
      end do
    else if (k.eq.mz) then
      do j=1,my
      do i=1,mx
      end do
      end do
    else
      do j=1,my
      do i=1,mx
      end do
      end do
    end if
  end do
 end if

 do k=izs,ize
   do j=1,my
     do i=1,mx
       fp(i,j,k) = max (0., fp(i,j,k))
     end do
   end do
 end do
!$omp end parallel

END SUBROUTINE
!***********************************************************************
SUBROUTINE dif2d_set (fx, fy, fz, fp)
!
!  Finite difference one zone left to one zones right.
!

  USE params
  implicit none
  logical omp_in_parallel
  real, dimension(mx,my,mz):: fx, fy, fz, fp
  real, dimension(mx,my,1):: gz, hz
  integer i, j, k, km1, kp1
!-----------------------------------------------------------------------


!$omp parallel private(i,j,k,km1,kp1)

  if (mx.gt.1) then
    do k=izs,ize
      do j=1,my
        do i=2,mx-1
          fp(i,j,k) = (fx(i-1 ,j,k)-fx(i+1,j,k))
        end do
        fp(1 ,j,k)  = (gx1(1   ,j,k)-fx(2  ,j,k))
        fp(mx,j,k)  = (fx(mx-1,j,k)-hx1(1  ,j,k))
      end do
    end do
  else
    do k=izs,ize
      do j=1,my
        do i=1,mx
          fp(i,j,k) = 0.
        end do
      end do
    end do
  end if


  if (my .gt. 1) then
    do k=izs,ize
     do j=2,my-1
       do i=1,mx
         fp(i,j, k) = fp(i,j, k) + (fy(i,j -1,k)-fy(i,j+1,k))
       end do
     end do
     do i=1,mx
         fp(i,1 ,k) = fp(i,1 ,k) + (gy1(i,   1,k)-fy(i,  2,k))
         fp(i,my,k) = fp(i,my,k) + (fy(i,my-1,k)-hy1(i,  1,k))
     end do
    end do
  end if


 if (mz .gt. 1) then
!$omp barrier
  do k=izs,ize
    if (k.eq.1) then
      do j=1,my
      do i=1,mx
        fp(i,j,k ) = fp(i,j,k ) + (gz(i,j,  1)-fz(i,j,k+1))
      end do
      end do
    else if (k.eq.mz) then
      do j=1,my
      do i=1,mx
        fp(i,j,k ) = fp(i,j,k ) + (fz(i,j,k-1)-hz(i,j,1))
      end do
      end do
    else
      do j=1,my
      do i=1,mx
        fp(i,j,k ) = fp(i,j,k ) + (fz(i,j,k-1)-fz(i,j,k+1))
      end do
      end do
    end if
  end do
 end if

 do k=izs,ize
   do j=1,my
     do i=1,mx
       fp(i,j,k) = max (0., fp(i,j,k))
     end do
   end do
 end do
!$omp end parallel

END SUBROUTINE

!***********************************************************************
SUBROUTINE dif1xy_set (fx, fy, fp)
!
!  Finite difference curl one zone left to center
!
  USE params
  implicit none
  logical omp_in_parallel
  real, dimension(mx,my,mz):: fx, fy, fp
  real, dimension(mx):: cx
  real, dimension(my):: cy
  integer i, j, k, km1, kp1
!-----------------------------------------------------------------------
!$omp parallel private(i,j,k,km1,kp1)
  if (mx.gt.1) then
    do k=izs,ize
      do j=1,my
        do i=2,mx
          fp(i,j,k) = cx(i)*(fy(i ,j,k)-fy(i-1,j,k))
        end do
        fp(1,j,k)  = cx(1)*(fy(1,j,k)-gx1(  1,j,k))
      end do
    end do
  else
    do k=izs,ize
      do j=1,my
        do i=1,mx
          fp(i,j,k) = 0.
        end do
      end do
    end do
  end if
  if (my .gt. 1) then
    do k=izs,ize
     do j=2,my
       do i=1,mx
         fp(i,j, k) = fp(i, j,k) - cy(j)*(fx(i, j,k)-fx(i,j-1,k))
       end do
     end do
     do i=1,mx
         fp(i,1,k) = fp(i,1,k) - cy(1)*(fx(i,1,k)-gy1(i,  1,k))
     end do
    end do
  end if
  do k=izs,ize
    do j=1,my
      do i=1,mx
        fp(i,j,k) = fp(i,j,k)**2
      end do
    end do
  end do
!$omp end parallel
END SUBROUTINE

!***********************************************************************
SUBROUTINE dif1yz_set (fy, fz, fp)
!
!  Finite difference one zone left to center
!
  USE params
  implicit none
  logical omp_in_parallel
  real, dimension(mx,my,mz):: fy, fz, fp
  real, dimension(mx,my,1):: gz, hz
  real, dimension(my):: cy
  real, dimension(mz):: cz
  integer i, j, k, km1, kp1
!-----------------------------------------------------------------------
!$omp parallel private(i,j,k,km1,kp1)
  if (my .gt. 1) then
    do k=izs,ize
     do j=2,my
       do i=1,mx
         fp(i,j, k) = cy(j)*(fz(i, j,k)-fz(i,j-1,k))
       end do
     end do
     do i=1,mx
         fp(i,1,k) = cy(1)*(fz(i,1,k)-gy1(i,  1,k))
     end do
    end do
  end if
  if (mz .gt. 1) then
!$omp barrier
   do k=izs,ize
     if (k.eq.1) then
       do j=1,my
       do i=1,mx
         fp(i,j,k) = fp(i,j,k) - cz(k)*(fy(i,j,k)-gz(i,j,1))
       end do
       end do
     else
       do j=1,my
       do i=1,mx
         fp(i,j,k) = fp(i,j,k) - cz(k)*(fy(i,j,k)-fy(i,j,k-1))
       end do
       end do
     end if
   end do
  end if
  do k=izs,ize
    do j=1,my
      do i=1,mx
        fp(i,j,k) = fp(i,j,k)**2
      end do
    end do
  end do
 !$omp end parallel
END SUBROUTINE

!***********************************************************************
SUBROUTINE dif1zx_set (fz, fx, fp)
!
!  Finite difference one zone left to center
!
  USE params
  implicit none
  logical omp_in_parallel
  real, dimension(mx,my,mz):: fx, fz, fp
  real, dimension(mx,my,1):: gz, hz
  real, dimension(mx):: cx
  real, dimension(mz):: cz
  integer i, j, k, km1, kp1
!-----------------------------------------------------------------------
!$omp parallel private(i,j,k,km1,kp1)
  if (mx.gt.1) then
    do k=izs,ize
      do j=1,my
        do i=2,mx
          fp(i,j,k) = cx(i)*(fz(i ,j,k)-fz(i-1,j,k))
        end do
        fp(1,j,k)  = cx(1)*(fz(1,j,k)-gx1(  1,j,k))
      end do
    end do
  else
    do k=izs,ize
      do j=1,my
        do i=1,mx
          fp(i,j,k) = 0.
        end do
      end do
    end do
  end if
  if (mz .gt. 1) then
!$omp barrier
    do k=izs,ize
      if (k.eq.1) then
        do j=1,my
        do i=1,mx
          fp(i,j,k) = fp(i,j,k) - cz(k)*(fx(i,j,k)-gz(i,j,1))
        end do
        end do
      else
        do j=1,my
        do i=1,mx
          fp(i,j,k) = fp(i,j,k) - cz(k)*(fx(i,j,k)-fx(i,j,k-1))
        end do
        end do
      end if
    end do
   end if
   do k=izs,ize
     do j=1,my
       do i=1,mx
         fp(i,j,k) = fp(i,j,k)**2
       end do
     end do
   end do
!$omp end parallel
END SUBROUTINE

!***********************************************************************
SUBROUTINE dif1_set (fx, fy, fz, fp)
!
!  Finite difference one zone left to center
!

  USE params
  implicit none
  logical omp_in_parallel
  real, dimension(mx,my,mz):: fx, fy, fz, fp
  real, dimension(mx,my,1):: gz, hz
  real, dimension(mx):: cx
  real, dimension(my):: cy
  real, dimension(mz):: cz
  integer i, j, k, km1, kp1
!-----------------------------------------------------------------------



!$omp parallel private(i,j,k,km1,kp1)


  if (mx.gt.1) then
    do k=izs,ize
      do j=1,my
        do i=1,mx-1
          fp(i,j,k) = cx(i)*(fx(i ,j,k)-fx(i+1,j,k))
        end do
        fp(mx,j,k)  = cx(mx)*(fx(mx,j,k)-hx1(  1,j,k))
      end do
    end do
  else
    do k=izs,ize
      do j=1,my
        do i=1,mx
          fp(i,j,k) = 0.
        end do
      end do
    end do
  end if


  if (my .gt. 1) then
    do k=izs,ize
     do j=1,my-1
       do i=1,mx
       end do
     end do
     do i=1,mx
     end do
    end do
  end if


 if (mz .gt. 1) then
!$omp barrier
  do k=izs,ize
    if (k.eq.mz) then
      do j=1,my
      do i=1,mx
      end do
      end do
    else
      do j=1,my
      do i=1,mx
      end do
      end do
    end if
  end do
 end if

 do k=izs,ize
   do j=1,my
     do i=1,mx
       fp(i,j,k) = max (0., fp(i,j,k))
     end do
   end do
 end do
!$omp end parallel

END SUBROUTINE

!***********************************************************************
SUBROUTINE dif1d_set (fx, fy, fz, fp)
!
!  Finite difference one zone left to center
!

  USE params
  implicit none
  logical omp_in_parallel
  real, dimension(mx,my,mz):: fx, fy, fz, fp
  real, dimension(mx,my,1):: gz, hz
  integer i, j, k, km1, kp1
!-----------------------------------------------------------------------


!$omp parallel private(i,j,k,km1,kp1)


  if (mx.gt.1) then
    do k=izs,ize
      do j=1,my
        do i=1,mx-1
          fp(i,j,k) = (fx(i ,j,k)-fx(i+1,j,k))
        end do
        fp(mx,j,k)  = (fx(mx,j,k)-hx1(  1,j,k))
      end do
    end do
  else
    do k=izs,ize
      do j=1,my
        do i=1,mx
          fp(i,j,k) = 0.
        end do
      end do
    end do
  end if


  if (my .gt. 1) then
    do k=izs,ize
     do j=1,my-1
       do i=1,mx
         fp(i,j, k) = fp(i, j,k) + (fy(i, j,k)-fy(i,j+1,k))
       end do
     end do
     do i=1,mx
         fp(i,my,k) = fp(i,my,k) + (fy(i,my,k)-hy1(i,  1,k))
     end do
    end do
  end if


 if (mz .gt. 1) then
!$omp barrier
  do k=izs,ize
    if (k.eq.mz) then
      do j=1,my
      do i=1,mx
        fp(i,j,k) = fp(i,j,k) + (fz(i,j,k)-hz(i,j,1))
      end do
      end do
    else
      do j=1,my
      do i=1,mx
        fp(i,j,k) = fp(i,j,k) + (fz(i,j,k)-fz(i,j,k+1))
      end do
      end do
    end if
  end do
 end if

 do k=izs,ize
   do j=1,my
     do i=1,mx
       fp(i,j,k) = max (0., fp(i,j,k))
     end do
   end do
 end do
!$omp end parallel

END SUBROUTINE

!***********************************************************************
SUBROUTINE xup_set (f, fp)
!
!  f is centered on (i-.5,j,k), fifth order stagger
!
  use params, g=>gx2, h=>hx3
  implicit none
  logical omp_in_parallel
  real a, b, c
  integer i, j, k
  real, dimension(mx,my,mz):: f, fp
!-----------------------------------------------------------------------
!
!
  if (mx.le.5) then
    fp = f
    return
  end if
  b = -1./16.
  a = .5-b

!$omp parallel private(i,j,k)


  do k=izs,ize
   do j=1,my
    fp(mx-2,j,k) = ( &
                   a*(f(mx-2,j,k)+f(mx-1,j,k)) &
                 + b*(f(mx-3,j,k)+f(mx  ,j,k)))
    fp(mx-1,j,k) = ( &
                   a*(f(mx-1,j,k)+f(mx  ,j,k)) &
                 + b*(f(mx-2,j,k)+h(1   ,j,k)))
    fp(mx  ,j,k) = ( &
                   a*(f(mx  ,j,k)+h(1   ,j,k)) &
                 + b*(f(mx-1,j,k)+h(2   ,j,k)))
    fp(1   ,j,k) = ( &
                   a*(f(1   ,j,k)+f(2   ,j,k)) &
                 + b*(g(2   ,j,k)+f(3   ,j,k)))
    fp(2   ,j,k) = ( &
                   a*(f(2   ,j,k)+f(3   ,j,k)) &
                 + b*(f(1   ,j,k)+f(4   ,j,k)))
   end do
   do j=1,my
    do i=3,mx-3
      fp(i  ,j,k) = ( &
                    a*(f(i  ,j,k)+f(i+1 ,j,k)) &
                  + b*(f(i-1,j,k)+f(i+2 ,j,k)))
    end do
   end do
  end do
!$omp end parallel
!
  nstag = nstag+1
  nflop = nflop+8
END SUBROUTINE

!***********************************************************************
SUBROUTINE xup1_set (f, fp)
!
!  f is centered on (i-.5,j,k), first order stagger
!
  use params, h=>hx1
  implicit none
  logical omp_in_parallel
  real a, b, c
  integer i, j, k
  real, dimension(mx,my,mz):: f, fp
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = f
    return
  end if
!
  a = .5

!$omp parallel private(i,j,k)

!
  do k=izs,ize
   do j=1,my
    fp(mx  ,j,k) =  ( &
                    a*(f(mx,j,k)+h(  1,j,k)))
   end do
   do j=1,my
    do i=1,mx-1
      fp(i  ,j,k) = ( &
                    a*(f(i ,j,k)+f(i+1,j,k)))
    end do
   end do
  end do
!$omp end parallel
!
  nstag1 = nstag1+1
  nflop = nflop+2
END SUBROUTINE

!***********************************************************************
SUBROUTINE xdn_set (f, fp)
!
!  f is centered on (i,j,k), fifth order stagger
!
  use params, g=>gx3, h=>hx2
  implicit none
  logical omp_in_parallel
  real a, b, c
  integer i, j, k
  real, dimension(mx,my,mz):: f, fp
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = f
    return
  end if
!
  b = -1./16.
  a = .5-b

!$omp parallel private(i,j,k)

!
  do k=izs,ize
   do j=1,my
    fp(mx-1,j,k) = ( &
                   a*(f(mx-2,j,k)+f(mx-1,j,k)) &
                 + b*(f(mx-3,j,k)+f(mx  ,j,k)))
    fp(mx  ,j,k) = ( &
                   a*(f(mx-1,j,k)+f(mx  ,j,k)) &
                 + b*(f(mx-2,j,k)+h(1   ,j,k)))
    fp(1  ,j,k) = ( &
                   a*(g(3   ,j,k)+f(1   ,j,k)) &
                 + b*(g(2   ,j,k)+f(2   ,j,k)))
    fp(2  ,j,k) = ( &
                   a*(f(1   ,j,k)+f(2   ,j,k)) &
                 + b*(g(3   ,j,k)+f(3   ,j,k)))
    fp(3  ,j,k) = ( &
                   a*(f(2   ,j,k)+f(3   ,j,k)) &
                 + b*(f(1   ,j,k)+f(4   ,j,k)))
   end do
   do j=1,my
    do i=4,mx-2
      fp(i,j,k) = ( &
                   a*(f(i   ,j,k)+f(i-1 ,j,k)) &
                 + b*(f(i+1 ,j,k)+f(i-2 ,j,k)))
    end do
   end do
  end do
!$omp end parallel
!
!
  nstag = nstag+1
  nflop = nflop+8
END SUBROUTINE

!***********************************************************************
SUBROUTINE xdn1_set (f, fp)
!
!  f is centered on (i,j,k), first order stagger
!
  use params, g=>gx1
  implicit none
  logical omp_in_parallel
  real a, b, c
  integer i, j, k
  real, dimension(mx,my,mz):: f, fp
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = f
    return
  end if
!
  a = 0.5

!$omp parallel private(i,j,k)

!
  do k=izs,ize
   do j=1,my
    fp(1  ,j,k) = ( &
                   a*(g(1,j,k)+f(1   ,j,k)))
   end do
   do j=1,my
    do i=2,mx
      fp(i,j,k) = ( &
                   a*(f(i,j,k)+f(i-1,j,k)))
    end do
   end do
  end do
!$omp end parallel
!
  nstag1 = nstag1+1
  nflop = nflop+2
END SUBROUTINE

!***********************************************************************
SUBROUTINE xup32_set (f,j, fp)
!
!  f is centered on (i-.5,j,k), fifth order stagger
!
  use params, g=>gx2, h=>hx3
  implicit none
  logical omp_in_parallel
  real a, b, c
  integer i, j, k
  real, dimension(mx,my,mz):: f
  real, dimension(mx,mz):: fp
!-----------------------------------------------------------------------
!
!
  if (mx.le.5) then
    fp = f(:,j,:)
    return
  end if
  b = -1./16.
  a = .5-b
!
!$omp parallel private(i,j,k)


  do k=izs,ize
    fp(mx-2,k) = ( &
                   a*(f(mx-2,j,k)+f(mx-1,j,k)) &
                 + b*(f(mx-3,j,k)+f(mx  ,j,k)))
    fp(mx-1,k) = ( &
                   a*(f(mx-1,j,k)+f(mx  ,j,k)) &
                 + b*(f(mx-2,j,k)+h(1   ,j,k)))
    fp(mx  ,k) = ( &
                   a*(f(mx  ,j,k)+h(1   ,j,k)) &
                 + b*(f(mx-1,j,k)+h(2   ,j,k)))
    fp(1   ,k) = ( &
                   a*(f(1   ,j,k)+f(2   ,j,k)) &
                 + b*(g(2   ,j,k)+f(3   ,j,k)))
    fp(2   ,k) = ( &
                   a*(f(2   ,j,k)+f(3   ,j,k)) &
                 + b*(f(1   ,j,k)+f(4   ,j,k)))
    do i=3,mx-3
      fp(i  ,k) = ( &
                    a*(f(i  ,j,k)+f(i+1 ,j,k)) &
                  + b*(f(i-1,j,k)+f(i+2 ,j,k)))
    end do
  end do
!$omp end parallel
!
  if (omp_master) then
      nstag = nstag+1
      nflop = nflop+8
  end if
END SUBROUTINE

!***********************************************************************
SUBROUTINE xup132_set (f,j, fp)
!
!  f is centered on (i-.5,j,k), fifth order stagger
!
  use params, h=>hx1
  implicit none
  logical omp_in_parallel
  real a, b, c
  integer i, j, k
  real, dimension(mx,my,mz):: f
  real, dimension(mx,mz):: fp
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = f(:,j,:)
    return
  end if
!
  a = .5
!
!$omp parallel private(i,j,k)


  do k=izs,ize
    fp(mx  ,k) = ( &
                   a*(f(mx  ,j,k)+h(1   ,j,k)))
    do i=1,mx-1
      fp(i  ,k) = ( &
                    a*(f(i  ,j,k)+f(i+1 ,j,k)))
    end do
  end do
!$omp end parallel
!
  if (omp_master) then
    nstag1 = nstag1+1
    nflop = nflop+2
  end if
END SUBROUTINE

!***********************************************************************
SUBROUTINE xdn32_set (f,j, fp)
!
!  f is centered on (i,j,k), fifth order stagger
!
  use params, g=>gx3, h=>hx2
  implicit none
  logical omp_in_parallel
  real a, b, c
  integer i, j, k
  real, dimension(mx,my,mz):: f
  real, dimension(mx,mz):: fp
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = f(:,j,:)
    return
  end if
!
  b = -1./16.
  a = .5-b
!
!$omp parallel private(i,j,k)


  do k=izs,ize
    fp(mx-1,k) = ( &
                   a*(f(mx-2,j,k)+f(mx-1,j,k)) &
                 + b*(f(mx-3,j,k)+f(mx  ,j,k)))
    fp(mx  ,k) = ( &
                   a*(f(mx-1,j,k)+f(mx  ,j,k)) &
                 + b*(f(mx-2,j,k)+h(1   ,j,k)))
    fp(1  ,k) = ( &
                   a*(g(3   ,j,k)+f(1   ,j,k)) &
                 + b*(g(2   ,j,k)+f(2   ,j,k)))
    fp(2  ,k) = ( &
                   a*(f(1   ,j,k)+f(2   ,j,k)) &
                 + b*(g(3   ,j,k)+f(3   ,j,k)))
    fp(3  ,k) = ( &
                   a*(f(2   ,j,k)+f(3   ,j,k)) &
                 + b*(f(1   ,j,k)+f(4   ,j,k)))
    do i=4,mx-2
      fp(i,k) = ( &
                   a*(f(i   ,j,k)+f(i-1 ,j,k)) &
                 + b*(f(i+1 ,j,k)+f(i-2 ,j,k)))
    end do
  end do
!$omp end parallel
!
!
  if (omp_master) then
    nstag = nstag+1
    nflop = nflop+8
  end if
END SUBROUTINE

!***********************************************************************
SUBROUTINE xdn132_set (f,j, fp)
!
!  f is centered on (i,j,k), fifth order stagger
!
  use params, g=>gx1
  implicit none
  logical omp_in_parallel
  real a, b, c
  integer i, j, k
  real, dimension(mx,my,mz):: f
  real, dimension(mx,mz):: fp
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = f(:,j,:)
    return
  end if
!
  a = 0.5
!
!$omp parallel private(i,j,k)


  do k=izs,ize
    fp(1  ,k) = ( &
                   a*(g(1   ,j,k)+f(1   ,j,k)))
    do i=2,mx
      fp(i,k) = ( &
                   a*(f(i   ,j,k)+f(i-1 ,j,k)))
    end do
  end do
!$omp end parallel
!
  if (omp_master) then
    nstag1 = nstag1+1
    nflop = nflop+2
  end if
END SUBROUTINE

!***********************************************************************
SUBROUTINE xup22_set (f, fp)
!
!  f is centered on (i-.5,k), fifth order stagger
!
  use params, g=>gx2, h=> hx3
  implicit none
  logical omp_in_parallel
  real a, b, c
  integer i, j, k
  real, dimension(mx,mz):: fp, f
!-----------------------------------------------------------------------
!
!
  if (mx.le.5) then
    fp = f
    return
  end if
  b = -1./16.
  a = .5-b
!
!$omp parallel private(i,k)


  do k=izs,ize
    fp(mx-2,k) = ( &
                   a*(f(mx-2,k)+f(mx-1,k)) &
                 + b*(f(mx-3,k)+f(mx  ,k)))
    fp(mx-1,k) = ( &
                   a*(f(mx-1,k)+f(mx  ,k)) &
                 + b*(f(mx-2,k)+h(1 ,1,k)))
    fp(mx  ,k) = ( &
                   a*(f(mx  ,k)+h(1 ,1,k)) &
                 + b*(f(mx-1,k)+h(2 ,1,k)))
    fp(1   ,k) = ( &
                   a*(f(1   ,k)+f(2   ,k)) &
                 + b*(g(2 ,1,k)+f(3   ,k)))
    fp(2   ,k) = ( &
                   a*(f(2   ,k)+f(3   ,k)) &
                 + b*(f(1   ,k)+f(4   ,k)))
    do i=3,mx-3
      fp(i  ,k) = ( &
                    a*(f(i  ,k)+f(i+1 ,k)) &
                  + b*(f(i-1,k)+f(i+2 ,k)))
    end do
  end do
!$omp end parallel
!
  if (omp_master) then
      nstag = nstag+1
      nflop = nflop+8
  end if
END SUBROUTINE

!***********************************************************************
SUBROUTINE xup122_set (f, fp)
!
!  f is centered on (i-.5,k), fifth order stagger
!
  use params, h=> hx1
  implicit none
  logical omp_in_parallel
  real a, b, c
  integer i, j, k
  real, dimension(mx,mz):: fp, f
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = f
    return
  end if
!
  a = .5
!
!$omp parallel private(i,k)


  do k=izs,ize
    fp(mx  ,k) = ( &
                   a*(f(mx  ,k)+h(1 ,1,k)))
    do i=1,mx-1
      fp(i  ,k) = ( &
                    a*(f(i  ,k)+f(i+1 ,k)))
    end do
  end do
!$omp end parallel
!
  if (omp_master) then
    nstag1 = nstag1+1
    nflop = nflop+2
  end if
END SUBROUTINE

!***********************************************************************
SUBROUTINE xdn22_set (f, fp)
!
!  f is centered on (i,k), fifth order stagger
!
  use params, g=>gx3, h=>hx2
  implicit none
  logical omp_in_parallel
  real a, b, c
  integer i, j, k
  real, dimension(mx,mz):: fp, f
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = f
    return
  end if
!
  b = -1./16.
  a = .5-b
!
!$omp parallel private(i,k)


  do k=izs,ize
    fp(mx-1,k) = ( &
                   a*(f(mx-2,k)+f(mx-1,k)) &
                 + b*(f(mx-3,k)+f(mx  ,k)))
    fp(mx  ,k) = ( &
                   a*(f(mx-1,k)+f(mx  ,k)) &
                 + b*(f(mx-2,k)+h(1 ,1,k)))
    fp(1  ,k) = ( &
                   a*(g(3 ,1,k)+f(1   ,k)) &
                 + b*(g(2 ,1,k)+f(2   ,k)))
    fp(2  ,k) = ( &
                   a*(f(1   ,k)+f(2   ,k)) &
                 + b*(g(3 ,1,k)+f(3   ,k)))
    fp(3  ,k) = ( &
                   a*(f(2   ,k)+f(3   ,k)) &
                 + b*(f(1   ,k)+f(4   ,k)))
    do i=4,mx-2
      fp(i,k) = ( &
                   a*(f(i   ,k)+f(i-1 ,k)) &
                 + b*(f(i+1 ,k)+f(i-2 ,k)))
    end do
  end do
!$omp end parallel
!
!
  if (omp_master) then
    nstag = nstag+1
    nflop = nflop+8
  end if
END SUBROUTINE

!***********************************************************************
SUBROUTINE xdn122_set (f, fp)
!
!  f is centered on (i,k), fifth order stagger
!
  use params, g=>gx1
  implicit none
  logical omp_in_parallel
  real a, b, c
  integer i, j, k
  real, dimension(mx,mz):: fp, f
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = f
    return
  end if
!
  a = 0.5
!
!$omp parallel private(i,k)


  do k=izs,ize
    fp(1  ,k) = ( &
                   a*(g(1 ,1,k)+f(1   ,k)))
    do i=2,mx
      fp(i,k) = ( &
                   a*(f(i   ,k)+f(i-1 ,k)))
    end do
  end do
!$omp end parallel
!
  if (omp_master) then
    nstag1 = nstag1+1
    nflop = nflop+2
  end if
END SUBROUTINE
!***********************************************************************
SUBROUTINE ddxupi_set (f, fp)
  use params, g=>gx2, h=>hx3
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f, fp
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = 0.
    return
  end if
!
  b = -1./24.
  a = (1.-3.*b)
  a1st = 1.

!
!$omp parallel private(i,j,k)
  do k=izs,ize
   do j=1,my
    fp(mx-2,j,k) = ( &
                 + b*(f(mx  ,j,k)-f(mx-3,j,k)) &
                 + a*(f(mx-1,j,k)-f(mx-2,j,k)))
    fp(mx-1,j,k) = ( &
                 + b*(h(1   ,j,k)-f(mx-2,j,k)) &
                 + a*(f(mx  ,j,k)-f(mx-1,j,k)))
    fp(mx  ,j,k) = ( &
                 + b*(h(2   ,j,k)-f(mx-1,j,k)) &
                 + a*(h(1   ,j,k)-f(mx  ,j,k)))
    fp(1  ,j,k) = ( &
                 + b*(f(3   ,j,k)-g(2   ,j,k)) &
                 + a*(f(2   ,j,k)-f(1   ,j,k)))
    fp(2  ,j,k) = ( &
                 + b*(f(4   ,j,k)-f(1   ,j,k)) &
                 + a*(f(3   ,j,k)-f(2   ,j,k)))
   end do
   do j=1,my
    do i=3,mx-3
      fp(i  ,j,k) = ( &
                 + b*(f(i+2 ,j,k)-f(i-1 ,j,k)) &
                 + a*(f(i+1 ,j,k)-f(i   ,j,k)))
    end do
  end do
  end do
!$omp end parallel
!
  nstag = nstag+1
  nflop = nflop+8
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddxupi1_set (f, fp)
  use params, g=>gx1
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f, fp
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = 0.
    return
  end if
  a = 1.

!$omp parallel private(i,j,k)

!
  do k=izs,ize
   do j=1,my
    fp(mx  ,j,k) = ( &
                   a*(g(1  ,j,k)-f(mx,j,k)))
   end do
   do j=1,my
    do i=1,mx-1
      fp(i  ,j,k) = ( &
                   a*(f(i+1,j,k)-f(i ,j,k)))
    end do
  end do
  end do
!$omp end parallel
!
  nstag1 = nstag1+1
  nflop = nflop+2
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddxdni_set (f, fp)
  use params, g=>gx3, h=>hx2
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f, fp
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = 0.
    return
  end if
!
  b = -1./24.
  a = (1.-3.*b)
  a1st = 1.

!$omp parallel private(i,j,k)

!
  do k=izs,ize
   do j=1,my
    fp(mx-1,j,k) = ( &
                 + b*(f(mx  ,j,k)-f(mx-3,j,k)) &
                 + a*(f(mx-1,j,k)-f(mx-2,j,k)))
    fp(mx  ,j,k) = ( &
                 + b*(h(1   ,j,k)-f(mx-2,j,k)) &
                 + a*(f(mx  ,j,k)-f(mx-1,j,k)))
    fp(1   ,j,k) = ( &
                 + b*(f(2   ,j,k)-g(2   ,j,k)) &
                 + a*(f(1   ,j,k)-g(3   ,j,k)))
    fp(2  ,j,k) = ( &
                 + b*(f(3   ,j,k)-g(3   ,j,k)) &
                 + a*(f(2   ,j,k)-f(1   ,j,k)))
    fp(3  ,j,k) = ( &
                 + b*(f(4   ,j,k)-f(1   ,j,k)) &
                 + a*(f(3   ,j,k)-f(2   ,j,k)))
   end do
   do j=1,my
    do i=4,mx-2
      fp(i,j,k) = ( &
                 + b*(f(i+1 ,j,k)-f(i-2 ,j,k)) &
                 + a*(f(i   ,j,k)-f(i-1 ,j,k)))
    end do
   end do
  end do
!$omp end parallel
!
  nstag = nstag+1
  nflop = nflop+8
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddxdni1_set (f, fp)
  use params, g=>gx1
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f, fp
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = 0.
    return
  end if
  a = 1.

!$omp parallel private(i,j,k)

!
  do k=izs,ize
   do j=1,my
    fp(1   ,j,k) = ( &
                   a*(f(1,j,k)-g(1  ,j,k)))
   end do
   do j=1,my
    do i=2,mx
      fp(i,j,k) = ( &
                   a*(f(i,j,k)-f(i-1,j,k)))
    end do
   end do
  end do
!$omp end parallel
!
  nstag1 = nstag1+1
  nflop = nflop+2
END SUBROUTINE
!***********************************************************************
SUBROUTINE ddxup_set (f, fp)
  use params, h=>hx3, g=>gx2
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f, fp
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = 0.
    return
  end if
!
  b = -1./24.
  a = (1.-3.*b)/dx
  a1st = 1./dx
  b = b/dx

!$omp parallel private(i,j,k)
!
  do k=izs,ize
   do j=1,my
    fp(mx-2,j,k) = ( &
                 + b*(f(mx  ,j,k)-f(mx-3,j,k)) &
                 + a*(f(mx-1,j,k)-f(mx-2,j,k)))
    fp(mx-1,j,k) = ( &
                 + b*(h(1   ,j,k)-f(mx-2,j,k)) &
                 + a*(f(mx  ,j,k)-f(mx-1,j,k)))
    fp(mx  ,j,k) = ( &
                 + b*(h(2   ,j,k)-f(mx-1,j,k)) &
                 + a*(h(1   ,j,k)-f(mx  ,j,k)))
    fp(1  ,j,k) = ( &
                 + b*(f(3   ,j,k)-g(2   ,j,k)) &
                 + a*(f(2   ,j,k)-f(1   ,j,k)))
    fp(2  ,j,k) = ( &
                 + b*(f(4   ,j,k)-f(1   ,j,k)) &
                 + a*(f(3   ,j,k)-f(2   ,j,k)))
   end do
   do j=1,my
    do i=3,mx-3
      fp(i  ,j,k) = ( &
                 + b*(f(i+2 ,j,k)-f(i-1 ,j,k)) &
                 + a*(f(i+1 ,j,k)-f(i   ,j,k)))
    end do
  end do
  end do
!$omp end parallel
!
  nstag = nstag+1
  nflop = nflop+8
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddxup1_set (f, fp)
  use params, g=>gx1
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f, fp
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = 0.
    return
  end if
  a = 1./dx

!$omp parallel private(i,j,k)
!
  do k=izs,ize
   do j=1,my
    fp(mx  ,j,k) = ( &
                   a*(g(1  ,j,k)-f(mx,j,k)))
   end do
   do j=1,my
    do i=1,mx-1
      fp(i  ,j,k) = ( &
                   a*(f(i+1,j,k)-f(i ,j,k)))
    end do
  end do
  end do
!$omp end parallel
!
  nstag1 = nstag1+1
  nflop = nflop+2
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddxdn_set (f, fp)
  use params, g=>gx3, h=>hx2
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f, fp
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = 0.
    return
  end if
!
  b = -1./24.
  a = (1.-3.*b)/dx
  a1st = 1./dx
  b = b/dx

!$omp parallel private(i,j,k)
!
  do k=izs,ize
   do j=1,my
    fp(mx-1,j,k) = ( &
                 + b*(f(mx  ,j,k)-f(mx-3,j,k)) &
                 + a*(f(mx-1,j,k)-f(mx-2,j,k)))
    fp(mx  ,j,k) = ( &
                 + b*(h(1   ,j,k)-f(mx-2,j,k)) &
                 + a*(f(mx  ,j,k)-f(mx-1,j,k)))
    fp(1   ,j,k) = ( &
                 + b*(f(2   ,j,k)-g(2   ,j,k)) &
                 + a*(f(1   ,j,k)-g(3   ,j,k)))
    fp(2  ,j,k) = ( &
                 + b*(f(3   ,j,k)-g(3   ,j,k)) &
                 + a*(f(2   ,j,k)-f(1   ,j,k)))
    fp(3  ,j,k) = ( &
                 + b*(f(4   ,j,k)-f(1   ,j,k)) &
                 + a*(f(3   ,j,k)-f(2   ,j,k)))
   end do
   do j=1,my
    do i=4,mx-2
      fp(i,j,k) = ( &
                 + b*(f(i+1 ,j,k)-f(i-2 ,j,k)) &
                 + a*(f(i   ,j,k)-f(i-1 ,j,k)))
    end do
   end do
  end do
!$omp end parallel
!
  nstag = nstag+1
  nflop = nflop+8
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddxdn1_set (f, fp)
  use params, g=>gx1
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f, fp
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = 0.
    return
  end if
  a = 1./dx

!$omp parallel private(i,j,k)
!
  do k=izs,ize
   do j=1,my
    fp(1   ,j,k) = ( &
                   a*(f(1,j,k)-g(1  ,j,k)))
   end do
   do j=1,my
    do i=2,mx
      fp(i,j,k) = ( &
                   a*(f(i,j,k)-f(i-1,j,k)))
    end do
   end do
  end do
!$omp end parallel
!
  nstag1 = nstag1+1
  nflop = nflop+2
END SUBROUTINE
!***********************************************************************
SUBROUTINE yup_set (f, fp)
!
!  f is centered on (i,j-.5,k), fifth order stagger
!
  use params, g=>gy2, h=>gy3
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k
  real, intent(in) , dimension(mx,my,mz):: f
  real,              dimension(mx,my,mz):: fp
!-----------------------------------------------------------------------

  if (my .lt. 5) then
    !$omp parallel private(k)
    do k=izs,ize
      fp(:,:,k) = f(:,:,k)
    end do
    !$omp end parallel
    return
  end if
  b = -1./16.
  a = .5-b
  a1st = 0.5


  !$omp parallel private(i,j,k)
  do k=izs,ize
    do j=3,my-3
     do i=1,mx
      fp(i,j  ,k) = ( &
                     a*(f(i,j   ,k)+f(i,j+1 ,k)) + &
                     b*(f(i,j-1 ,k)+f(i,j+2 ,k)))
     end do
    end do
    do i=1,mx
      fp(i,my-2,k) = ( &
                     a*(f(i,my-2,k)+f(i,my-1,k)) + &
                     b*(f(i,my-3,k)+f(i,my  ,k)))
      fp(i,my-1,k) = ( &
                     a*(f(i,my-1,k)+f(i,my  ,k)) + &
                     b*(f(i,my-2,k)+h(i,1   ,k)))
      fp(i,my  ,k) = ( &
                     a*(f(i,my  ,k)+h(i,1   ,k)) + &
                     b*(f(i,my-1,k)+h(i,2   ,k)))
      fp(i,1   ,k) = ( &
                     a*(f(i,1   ,k)+f(i,2   ,k)) + &
                     b*(g(i,2   ,k)+f(i,3   ,k)))
      fp(i,2   ,k) = ( &
                     a*(f(i,2   ,k)+f(i,3   ,k)) + &
                     b*(f(i,1   ,k)+f(i,4   ,k)))
    end do
  end do
  !$omp end parallel

  !$omp master
  nstag = nstag+1
  nflop = nflop+8
  !$omp end master
END SUBROUTINE

!***********************************************************************
SUBROUTINE yup1_set (f, fp)
!
!  f is centered on (i,j-.5,k), fifth order stagger
!
  use params, h=>gy1
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k
  real, intent(in) , dimension(mx,my,mz):: f
  real,              dimension(mx,my,mz):: fp
!-----------------------------------------------------------------------
!
  if (my .lt. 5) then
    !$omp parallel private(k)
    do k=izs,ize
      fp(:,:,k) = f(:,:,k)
    end do
    !$omp end parallel
    return
  end if

  a = 0.5


  !$omp parallel private(i,j,k)
  do k=izs,ize
    do j=1,my-1
      do i=1,mx
        fp(i,j  ,k) = ( &
                     a*(f(i,j   ,k)+f(i,j+1 ,k)))
      end do
    end do
    do i=1,mx
      fp(i,my,k) = ( &
                     a*(h(i,1,k)+f(i,my,k)))
    end do
  end do
  !$omp end parallel

  !$omp master
  nstag1 = nstag1+1
  nflop = nflop+2
  !$omp end master
END SUBROUTINE

!***********************************************************************
SUBROUTINE ydn_set (f, fp)
!
!  f is centered on (i,j,k), fifth order stagger
!
  use params, g=>gy3, h=>gy2
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f, fp
!-----------------------------------------------------------------------

  if (my .lt. 5) then
    !$omp parallel private(k)
    do k=izs,ize
      fp(:,:,k) = f(:,:,k)
    end do
    !$omp end parallel
    return
  end if

  b = -1./16.
  a = .5-b
  a1st = 0.5


  !$omp parallel private(i,j,k)
  do k=izs,ize
    do j=4,my-2
     do i=1,mx
      fp(i,j,k) = ( &
                     a*(f(i,j   ,k)+f(i,j-1 ,k)) + &
                     b*(f(i,j+1 ,k)+f(i,j-2 ,k)))
     end do
    end do
    do i=1,mx
      fp(i,my-1,k) = ( &
                     a*(f(i,my-2,k)+f(i,my-1,k)) + &
                     b*(f(i,my-3,k)+f(i,my  ,k)))
      fp(i,my  ,k) = ( &
                     a*(f(i,my-1,k)+f(i,my  ,k)) + &
                     b*(f(i,my-2,k)+h(i,1   ,k)))
      fp(i,1   ,k) = ( &
                     a*(g(i,3   ,k)+f(i,1   ,k)) + &
                     b*(g(i,2   ,k)+f(i,2   ,k)))
      fp(i,2   ,k) = ( &
                     a*(f(i,1   ,k)+f(i,2   ,k)) + &
                     b*(g(i,3   ,k)+f(i,3   ,k)))
      fp(i,3   ,k) = ( &
                     a*(f(i,2   ,k)+f(i,3   ,k)) + &
                     b*(f(i,1   ,k)+f(i,4   ,k)))
    end do
  end do
  !$omp end parallel

  !$omp master
  nstag = nstag+1
  nflop = nflop+8
  !$omp end master
END SUBROUTINE

!***********************************************************************
SUBROUTINE ydn1_set (f, fp)
!
!  f is centered on (i,j,k), fifth order stagger
!
  use params, g=>gy1
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f, fp
!-----------------------------------------------------------------------
  if (my .lt. 5) then
    !$omp parallel private(k)
    do k=izs,ize
      fp(:,:,k) = f(:,:,k)
    end do
    !$omp end parallel
    return
  end if

  a = 0.5


  !$omp parallel private(i,j,k)
  do k=izs,ize
    do j=2,my
     do i=1,mx
      fp(i,j,k) = ( &
                     a*(f(i,j   ,k)+f(i,j-1 ,k)))
     end do
    end do
    do i=1,mx
      fp(i,1   ,k) = ( &
                     a*(g(i,1   ,k)+f(i,1   ,k)))
    end do
  end do
  !$omp end parallel

  !$omp master
  nstag1 = nstag1+1
  nflop = nflop+2
  !$omp end master
END SUBROUTINE
!***********************************************************************
SUBROUTINE ddyupi_set (f, fp)
  use params, g=>gy2, h=>gy3
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f, fp
!-----------------------------------------------------------------------
!
  if (my .lt. 3) then
    fp = 0.
    return
  end if
  b = -1./24.
  a = (1.-3.*b)
  a1st = 1.


  do k=izs,ize
    do i=1,mx
      fp(i,my-2,k) = ( &
                     a*(f(i,my-1,k)-f(i,my-2,k)) + &
                     b*(f(i,my  ,k)-f(i,my-3,k)))
      fp(i,my-1,k) = ( &
                     a*(f(i,my  ,k)-f(i,my-1,k)) + &
                     b*(h(i,1   ,k)-f(i,my-2,k)))
      fp(i,my  ,k) = ( &
                     a*(h(i,1   ,k)-f(i,my  ,k)) + &
                     b*(h(i,2   ,k)-f(i,my-1,k)))
      fp(i,1   ,k) = ( &
                     a*(f(i,2   ,k)-f(i,1   ,k)) + &
                     b*(f(i,3   ,k)-g(i,2   ,k)))
      fp(i,2   ,k) = ( &
                     a*(f(i,3   ,k)-f(i,2   ,k)) + &
                     b*(f(i,4   ,k)-f(i,1   ,k)))
    end do
    do j=3,my-3
     do i=1,mx
      fp(i,j  ,k) = ( &
                     a*(f(i,j+1 ,k)-f(i,j   ,k)) + &
                     b*(f(i,j+2 ,k)-f(i,j-1 ,k)))
     end do
    end do
  end do

  !$omp master
  nstag = nstag+1
  nflop = nflop+8
  !$omp end master
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddyupi1_set (f, fp)
  use params, g=>gy1
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f, fp
!-----------------------------------------------------------------------
!
  if (my .lt. 3) then
    fp = 0.
    return
  end if
  a = 1.


  do k=izs,ize
    do i=1,mx
      fp(i,my,k) = ( &
                     a*(g(i,1  ,k)-f(i,my,k)))
    end do
    do j=1,my-1
      do i=1,mx
        fp(i,j  ,k) = ( &
                     a*(f(i,j+1 ,k)-f(i,j   ,k)))
     end do
    end do
  end do

  !$omp master
  nstag1 = nstag1+1
  nflop = nflop+2
  !$omp end master
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddydni_set (f, fp)
  use params, g=>gy3, h=>gy2
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f, fp
!-----------------------------------------------------------------------
!
  if (my .lt. 3) then
    fp = 0.
    return
  end if
  b = -1./24.
  a = (1.-3.*b)
  a1st = 1.


  do k=izs,ize
    do i=1,mx
      fp(i,my-1,k) = ( &
                     a*(f(i,my-1,k)-f(i,my-2,k)) + &
                     b*(f(i,my  ,k)-f(i,my-3,k)))
      fp(i,my  ,k) = ( &
                     a*(f(i,my  ,k)-f(i,my-1,k)) + &
                     b*(h(i,1   ,k)-f(i,my-2,k)))
      fp(i,1  ,k) = ( &
                     a*(f(i,1   ,k)-g(i,3   ,k)) + &
                     b*(f(i,2   ,k)-g(i,2   ,k)))
      fp(i,2   ,k) = ( &
                     a*(f(i,2   ,k)-f(i,1   ,k)) + &
                     b*(f(i,3   ,k)-g(i,3   ,k)))
      fp(i,3   ,k) = ( &
                     a*(f(i,3   ,k)-f(i,2   ,k)) + &
                     b*(f(i,4   ,k)-f(i,1   ,k)))
    end do
    do j=4,my-2
     do i=1,mx
      fp(i,j ,k) = ( &
                     a*(f(i,j   ,k)-f(i,j-1 ,k)) + &
                     b*(f(i,j+1 ,k)-f(i,j-2 ,k)))
     end do
    end do
  end do

  !$omp master
  nstag = nstag+1
  nflop = nflop+8
  !$omp end master
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddydni1_set (f, fp)
  use params, g=>gy1
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f, fp
!-----------------------------------------------------------------------
!
  if (my .lt. 3) then
    fp = 0.
    return
  end if
  a = 1.


  do k=izs,ize
    do i=1,mx
      fp(i,1  ,k) = ( &
                     a*(f(i,1   ,k)-g(i,1   ,k)))
    end do
    do j=2,my
     do i=1,mx
      fp(i,j ,k) = ( &
                     a*(f(i,j   ,k)-f(i,j-1 ,k)))
     end do
    end do
  end do

  !$omp master
  nstag1 = nstag1+1
  nflop = nflop+2
  !$omp end master
END SUBROUTINE
!***********************************************************************
SUBROUTINE ddyup_set (f, fp)
  use params, g=>gy2, h=>gy3
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f, fp
!-----------------------------------------------------------------------

  if (my .lt. 3) then
    fp = 0.
    return
  end if
  b = -1./24.
  a = (1.-3.*b)/dy
  a1st = 1./dy
  b = b/dy


  !$omp parallel private(i,j,k)
  do k=izs,ize
    do i=1,mx
      fp(i,my-2,k) = ( &
                     b*(f(i,my  ,k)-f(i,my-3,k)) + &
                     a*(f(i,my-1,k)-f(i,my-2,k)))
      fp(i,my-1,k) = ( &
                     b*(h(i,1   ,k)-f(i,my-2,k)) + &
                     a*(f(i,my  ,k)-f(i,my-1,k)))
      fp(i,my  ,k) = ( &
                     b*(h(i,2   ,k)-f(i,my-1,k)) + &
                     a*(h(i,1   ,k)-f(i,my  ,k)))
      fp(i,1   ,k) = ( &
                     b*(f(i,3   ,k)-g(i,2   ,k)) + &
                     a*(f(i,2   ,k)-f(i,1   ,k)))
      fp(i,2   ,k) = ( &
                     b*(f(i,4   ,k)-f(i,1   ,k)) + &
                     a*(f(i,3   ,k)-f(i,2   ,k)))
    end do
    do j=3,my-3
     do i=1,mx
      fp(i,j  ,k) = ( &
                     b*(f(i,j+2 ,k)-f(i,j-1 ,k)) + &
                     a*(f(i,j+1 ,k)-f(i,j   ,k)))
     end do
    end do
  end do
  !$omp end parallel

  !$omp master
  nstag = nstag+1
  nflop = nflop+8
  !$omp end master
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddyup1_set (f, fp)
  use params, g=>gy1
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f, fp
!-----------------------------------------------------------------------

  if (my .lt. 3) then
    fp = 0.
    return
  end if
  a = 1./dy


  !$omp parallel private(i,j,k)
  do k=izs,ize
    do i=1,mx
      fp(i,my,k) = ( &
                     a*(g(i,1  ,k)-f(i,my,k)))
    end do
    do j=1,my-1
      do i=1,mx
        fp(i,j  ,k) = ( &
                     a*(f(i,j+1 ,k)-f(i,j   ,k)))
     end do
    end do
  end do
  !$omp end parallel

  !$omp master
  nstag1 = nstag1+1
  nflop = nflop+2
  !$omp end master
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddydn_set (f, fp)
  use params, g=>gy3, h=>gy2
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f, fp
!-----------------------------------------------------------------------

  if (my .lt. 3) then
    fp = 0.
    return
  end if
  b = -1./24.
  a = (1.-3.*b)/dy
  a1st = 1./dy
  b = b/dy


  !$omp parallel private(i,j,k)
  do k=izs,ize
    do i=1,mx
      fp(i,my-1,k) = ( &
                     b*(f(i,my  ,k)-f(i,my-3,k)) + &
                     a*(f(i,my-1,k)-f(i,my-2,k)) ) 
      fp(i,my  ,k) = ( &
                     b*(h(i,1   ,k)-f(i,my-2,k)) + &
                     a*(f(i,my  ,k)-f(i,my-1,k)) )
      fp(i,1  ,k) = ( &
                     b*(f(i,2   ,k)-g(i,2   ,k)) + &
                     a*(f(i,1   ,k)-g(i,3   ,k)) )
      fp(i,2   ,k) = ( &
                     b*(f(i,3   ,k)-g(i,3   ,k)) + &
                     a*(f(i,2   ,k)-f(i,1   ,k)) )
      fp(i,3   ,k) = ( &
                     b*(f(i,4   ,k)-f(i,1   ,k)) + &
                     a*(f(i,3   ,k)-f(i,2   ,k)) )
    end do
    do j=4,my-2
     do i=1,mx
      fp(i,j ,k) = ( &
                     b*(f(i,j+1 ,k)-f(i,j-2 ,k)) + &
                     a*(f(i,j   ,k)-f(i,j-1 ,k)) )
     end do
    end do
  end do
  !$omp end parallel

  !$omp master
  nstag = nstag+1
  nflop = nflop+8
  !$omp end master
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddydn1_set (f, fp)
  use params, g=>gy1
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f, fp
!-----------------------------------------------------------------------

  if (my .lt. 3) then
    fp = 0.
    return
  end if
  a = 1./dy


  !$omp parallel private(i,j,k)
  do k=izs,ize
    do i=1,mx
      fp(i,1  ,k) = ( &
                     a*(f(i,1   ,k)-g(i,1   ,k)))
    end do
    do j=2,my
     do i=1,mx
      fp(i,j ,k) = ( &
                     a*(f(i,j   ,k)-f(i,j-1 ,k)))
     end do
    end do
  end do
  !$omp end parallel

  !$omp master
  nstag1 = nstag1+1
  nflop = nflop+2
  !$omp end master
END SUBROUTINE
!***********************************************************************
SUBROUTINE zup_set (f, fp)
!
!  f is centered on (i,j,k-.5), fifth order stagger
!
  use params
  implicit none
  logical omp_in_parallel
  real a, b, c
  integer i, j, k
  real, intent(in) , dimension(mx,my,mz):: f
  real,              dimension(mx,my,mz):: fp
  real,              dimension(mx,my, 2):: g
  real,              dimension(mx,my, 3):: h
!-----------------------------------------------------------------------
!

  if (mz .lt. 5) then
    !$omp parallel private(k)
    do k=izs,ize
      fp(:,:,k) = f(:,:,k)
    end do
    !$omp end parallel
    return
  end if
  b = -1./16.
  a = .5-b

!
  !$omp parallel private(i,j,k)
  do k=max(3,izs),min(mz-3,ize)
    do j=1,my
      do i=1,mx
        fp(i,j,k) = ( &
                     a*(f(i,j,k   )+f(i,j,k+1 )) + &
                     b*(f(i,j,k-1 )+f(i,j,k+2 )))
     end do
    end do
  end do
  if (ize.eq.mz) then
   do j=1,my
    do i=1,mx
      fp(i,j,mz-2) = ( &
                     a*(f(i,j,mz-2)+f(i,j,mz-1)) + &
                     b*(f(i,j,mz-3)+f(i,j,mz  )))
      fp(i,j,mz-1) = ( &
                     a*(f(i,j,mz-1)+f(i,j,mz  )) + &
                     b*(f(i,j,mz-2)+h(i,j,1   )))
      fp(i,j,mz  ) = ( &
                     a*(f(i,j,mz  )+h(i,j,1   )) + &
                     b*(f(i,j,mz-1)+h(i,j,2   )))
    end do
   end do
  end if
  if (izs.eq.1) then
   do j=1,my
    do i=1,mx
      fp(i,j,1   ) = ( &
                     a*(f(i,j,1   )+f(i,j,2   )) + &
                     b*(g(i,j,2   )+f(i,j,3   )))
      fp(i,j,2   ) = ( &
                     a*(f(i,j,2   )+f(i,j,3   )) + &
                     b*(f(i,j,1   )+f(i,j,4   )))
    end do
   end do
  end if
  !$omp end parallel
!
  nstag = nstag+1
  nflop = nflop+8
END SUBROUTINE

!***********************************************************************
SUBROUTINE zup1_set (f, fp)
!
!  f is centered on (i,j,k-.5), fifth order stagger
!
  use params
  implicit none
  logical omp_in_parallel
  real a, b, c
  integer i, j, k
  real, intent(in) , dimension(mx,my,mz):: f
  real,              dimension(mx,my,mz):: fp
  real,              dimension(mx,my,1 ):: h
!-----------------------------------------------------------------------
!
  if (mz .lt. 5) then
    !$omp parallel private(k)
    do k=izs,ize
      fp(:,:,k) = f(:,:,k)
    end do
    !$omp end parallel
    return
  end if
!
  a = 0.5
!
  !$omp parallel private(i,j,k)
  do k=izs,min(mz-1,ize)
    do j=1,my
      do i=1,mx
        fp(i,j,k  ) = ( &
                     a*(f(i,j,k   )+f(i,j,k+1 )))
      end do
    end do
  end do
  if (ize.eq.mz) then
   do j=1,my
    do i=1,mx
      fp(i,j,mz) = ( &
                     a*(h(i,j,1)+f(i,j,mz)))
    end do
   end do
  end if
  !$omp end parallel
!
  nstag1 = nstag1+1
  nflop = nflop+2
END SUBROUTINE

!***********************************************************************
SUBROUTINE zdn_set (f, fp)
!
!  f is centered on (i,j), fifth order stagger
!
  use params
  implicit none
  logical omp_in_parallel
  real a, b, c
  integer i, j, k
  real, dimension(mx,my,mz):: f, fp
  real, dimension(mx,my, 3):: g
  real, dimension(mx,my, 2):: h
!-----------------------------------------------------------------------
!
  if (mz .lt. 5) then
    !$omp parallel private(k)
    do k=izs,ize
      fp(:,:,k) = f(:,:,k)
    end do
    !$omp end parallel
    return
  end if
  b = -1./16.
  a = .5-b

!
  !$omp parallel private(i,j,k)
  do k=max(4,izs),min(mz-2,ize)
    do j=1,my
     do i=1,mx
      fp(i,j,k) = ( &
                     a*(f(i,j,k   )+f(i,j,k-1 )) + &
                     b*(f(i,j,k+1 )+f(i,j,k-2 )))
     end do
    end do
  end do
  if (ize.eq.mz) then
   do j=1,my
    do i=1,mx
      fp(i,j,mz-1) = ( &
                     a*(f(i,j,mz-2)+f(i,j,mz-1)) + &
                     b*(f(i,j,mz-3)+f(i,j,mz  )))
      fp(i,j,mz  ) = ( &
                     a*(f(i,j,mz-1)+f(i,j,mz  )) + &
                     b*(f(i,j,mz-2)+h(i,j,1   )))
    end do
   end do
  end if
  if (izs.eq.1) then
   do j=1,my
    do i=1,mx
      fp(i,j,1   ) = ( &
                     a*(g(i,j,3   )+f(i,j,1   )) + &
                     b*(g(i,j,2   )+f(i,j,2   )))
      fp(i,j,2   ) = ( &
                     a*(f(i,j,1   )+f(i,j,2   )) + &
                     b*(g(i,j,3   )+f(i,j,3   )))
      fp(i,j,3   ) = ( &
                     a*(f(i,j,2   )+f(i,j,3   )) + &
                     b*(f(i,j,1   )+f(i,j,4   )))
    end do
   end do
  end if
  !$omp end parallel
!
  nstag = nstag+1
  nflop = nflop+8
END SUBROUTINE

!***********************************************************************
SUBROUTINE zdn1_set (f, fp)
!
!  f is centered on (i,j), fifth order stagger
!
  use params
  implicit none
  logical omp_in_parallel
  real a, b, c
  integer i, j, k
  real, dimension(mx,my,mz):: f, fp
  real, dimension(mx,my,1 ):: g
!-----------------------------------------------------------------------
!
  if (mz .lt. 5) then
    !$omp parallel private(k)
    do k=izs,ize
      fp(:,:,k) = f(:,:,k)
    end do
    !$omp end parallel
    return
  end if
!
  a = 0.5
!
  !$omp parallel private(i,j,k)
  do k=max(2,izs),ize
    do j=1,my
     do i=1,mx
      fp(i,j,k) = ( &
                     a*(f(i,j,k   )+f(i,j,k-1 )))
     end do
    end do
  end do
  if (izs.eq.1) then
   do j=1,my
    do i=1,mx
      fp(i,j,1   ) = ( &
                     a*(g(i,j,1   )+f(i,j,1   )))
    end do
   end do
  end if
  !$omp end parallel
!
  nstag1 = nstag1+1
  nflop = nflop+2
END SUBROUTINE

!***********************************************************************
SUBROUTINE zup32_set (f,j, fp)
!
!  f is centered on (i,j,k-.5), fifth order stagger
!
  use params
  implicit none
  logical omp_in_parallel
  real a, b, c
  integer i, j, k
  real,             dimension(mx,   mz):: fp
  real, intent(in), dimension(mx,my,mz):: f
  real,             dimension(mx,my, 2):: g
  real,             dimension(mx,my, 3):: h
!-----------------------------------------------------------------------
!
!
  if (mz.le.5) then
    !$omp parallel private(k)
    do k=izs,ize
      fp(:,k) = f(:,j,k)
    end do
    !$omp end parallel
    return
  end if

  b = -1./16.
  a = .5-b

!
!$omp parallel private(i,j,k)
  do k=max(3,izs),min(mz-3,ize)
      do i=1,mx
        fp(i,k) = ( &
                     a*(f(i,j,k+1)+f(i,j,k  )) + &
                     b*(f(i,j,k+2)+f(i,j,k-1)))
      end do
  end do
  if (izs.eq.1) then
    do i=1,mx
      fp(i,1   ) = ( &
                     a*(f(i,j,1   )+f(i,j,2   )) + &
                     b*(g(i,j,2   )+f(i,j,3   )))
      fp(i,2   ) = ( &
                     a*(f(i,j,2   )+f(i,j,3   )) + &
                     b*(f(i,j,1   )+f(i,j,4   )))
    end do
  end if
  if (ize.eq.mz) then
    do i=1,mx
      fp(i,mz-2) = ( &
                     a*(f(i,j,mz-2)+f(i,j,mz-1)) + &
                     b*(f(i,j,mz-3)+f(i,j,mz  )))
      fp(i,mz-1) = ( &
                     a*(f(i,j,mz-1)+f(i,j,mz  )) + &
                     b*(f(i,j,mz-2)+h(i,j,1   )))
      fp(i,mz  ) = ( &
                     a*(f(i,j,mz  )+h(i,j,1   )) + &
                     b*(f(i,j,mz-1)+h(i,j,2   )))
    end do
  end if

!$omp end parallel
!
  if (omp_master) then
    nstag = nstag+1
    nflop = nflop+8
  end if
END SUBROUTINE

!***********************************************************************
SUBROUTINE zup132_set (f,j, fp)
!
!  f is centered on (i,j,k-.5), fifth order stagger
!
  use params
  implicit none
  logical omp_in_parallel
  real a, b, c
  integer i, j, k
  real,              dimension(mx,   mz):: fp
  real, intent(in),  dimension(mx,my,mz):: f
  real,              dimension(mx,my,1 ):: h
!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    !$omp parallel private(k)
    do k=izs,ize
      fp(:,k) = f(:,j,k)
    end do
    !$omp end parallel
    return
  end if

  a = .5
!
!$omp parallel private(i,j,k)
  do k=izs,min(mz-1,ize)
      do i=1,mx
        fp(i,k) = ( &
                     a*(f(i,j,k+1)+f(i,j,k  )))
      end do
  end do
  if (ize.eq.mz) then
    do i=1,mx
      fp(i,mz) = ( &
                     a*(h(i,j,1)+f(i,j,mz)))
    end do
  end if

!$omp end parallel
!
  if (omp_master) then
    nstag1 = nstag1+1
    nflop = nflop+2
  end if
END SUBROUTINE

!***********************************************************************
SUBROUTINE zdn32_set (f,j, fp)
!
!  f is centered on (i,j,k), fifth order stagger
!
  use params
  implicit none
  logical omp_in_parallel
  real a, b, c
  integer i, j, k
  real,             dimension(mx,   mz):: fp
  real, intent(in), dimension(mx,my,mz):: f
  real,             dimension(mx,my, 3):: g
  real,             dimension(mx,my, 2):: h

!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    !$omp parallel private(k)
    do k=izs,ize
      fp(:,k) = f(:,j,k)
    end do
    !$omp end parallel
    return
  end if

  b = -1./16.
  a = .5-b

!
!$omp parallel private(i,j,k)
  do k=max(4,izs),min(mz-2,ize)
      do i=1,mx
        fp(i,k) = ( &
                     a*(f(i,j,k  )+f(i,j,k-1)) + &
                     b*(f(i,j,k+1)+f(i,j,k-2)))
      end do
  end do
  if (izs.eq.1) then
    do i=1,mx
      fp(i,1   ) = ( &
                     a*(g(i,j,3   )+f(i,j,1   )) + &
                     b*(g(i,j,2   )+f(i,j,2   )))
      fp(i,2   ) = ( &
                     a*(f(i,j,1   )+f(i,j,2   )) + &
                     b*(g(i,j,3   )+f(i,j,3   )))
      fp(i,3   ) = ( &
                     a*(f(i,j,2   )+f(i,j,3   )) + &
                     b*(f(i,j,1   )+f(i,j,4   )))
    end do
  end if
  if (ize.eq.mz) then
    do i=1,mx
      fp(i,mz-1) = ( &
                     a*(f(i,j,mz-2)+f(i,j,mz-1)) + &
                     b*(f(i,j,mz-3)+f(i,j,mz  )))
      fp(i,mz  ) = ( &
                     a*(f(i,j,mz-1)+f(i,j,mz  )) + &
                     b*(f(i,j,mz-2)+h(i,j,1   )))
    end do
  end if

!$omp end parallel
!
  if (omp_master) then
    nstag = nstag+1
    nflop = nflop+8
  end if
END SUBROUTINE

!***********************************************************************
SUBROUTINE zdn132_set (f,j, fp)
!
!  f is centered on (i,j,k), fifth order stagger
!
  use params
  implicit none
  logical omp_in_parallel
  real a, b, c
  integer i, j, k
  real,             dimension(mx,   mz):: fp
  real, intent(in), dimension(mx,my,mz):: f
  real,             dimension(mx,my,1 ):: g
!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    !$omp parallel private(k)
    do k=izs,ize
      fp(:,k) = f(:,j,k)
    end do
    !$omp end parallel
    return
  end if

  a = .5
!
!$omp parallel private(i,j,k)
  do k=max(2,izs),ize
      do i=1,mx
        fp(i,k) = ( &
                     a*(f(i,j,k  )+f(i,j,k-1)))
      end do
  end do
  if (izs.eq.1) then
    do i=1,mx
      fp(i,1   ) = ( &
                     a*(g(i,j,1   )+f(i,j,1   )))
    end do
  end if

!$omp end parallel
!
  if (omp_master) then
    nstag1 = nstag1+1
    nflop = nflop+2
  end if
END SUBROUTINE

!***********************************************************************
SUBROUTINE zup22_set (f, fp)
!
!  f is centered on (i,k-.5), fifth order stagger
!
  use params
  implicit none
  logical omp_in_parallel
  real a, b, c
  integer i, j, k
  real,             dimension(mx,  mz):: fp
  real, intent(in), dimension(mx,  mz):: f
  real,             dimension(mx,1, 2):: g
  real,             dimension(mx,1, 3):: h

!-----------------------------------------------------------------------
!
!
  if (mz.le.5) then
    !$omp parallel private(k)
    do k=izs,ize
      fp(:,k) = f(:,k)
    end do
    !$omp end parallel
    return
  end if
  b = -1./16.
  a = .5-b

!
!$omp parallel private(i,k)
  do k=max(3,izs),min(mz-3,ize)
      do i=1,mx
        fp(i,k) = ( &
                     a*(f(i,k+1)+f(i,k  )) + &
                     b*(f(i,k+2)+f(i,k-1)))
      end do
  end do
  if (izs.eq.1) then
    do i=1,mx
      fp(i,1   ) = ( &
                     a*(f(i,1   )+f(i,2   )) + &
                     b*(g(i,1,2   )+f(i,3   )))
      fp(i,2   ) = ( &
                     a*(f(i,2   )+f(i,3   )) + &
                     b*(f(i,1   )+f(i,4   )))
    end do
  end if
  if (ize.eq.mz) then
    do i=1,mx
      fp(i,mz-2) = ( &
                     a*(f(i,mz-2)+f(i,mz-1)) + &
                     b*(f(i,mz-3)+f(i,mz  )))
      fp(i,mz-1) = ( &
                     a*(f(i,mz-1)+f(i,mz  )) + &
                     b*(f(i,mz-2)+h(i,1,1   )))
      fp(i,mz  ) = ( &
                     a*(f(i,mz  )+h(i,1,1   )) + &
                     b*(f(i,mz-1)+h(i,1,2   )))
    end do
  end if

!$omp end parallel
!
  if (omp_master) then
    nstag = nstag+1
    nflop = nflop+8
  end if
END SUBROUTINE

!***********************************************************************
SUBROUTINE zup122_set (f, fp)
!
!  f is centered on (i,k-.5), fifth order stagger
!
  use params
  implicit none
  logical omp_in_parallel
  real a, b, c
  integer i, j, k
  real,             dimension(mx,   mz):: fp
  real, intent(in), dimension(mx,   mz):: f
  real,             dimension(mx,my,1 ):: h
!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    !$omp parallel private(k)
    do k=izs,ize
      fp(:,k) = f(:,k)
    end do
    !$omp end parallel
    return
  end if

  a = .5
!
!$omp parallel private(i,k)
  do k=izs,ize
      do i=1,mx
        fp(i,k) = ( &
                     a*(f(i,k+1)+f(i,k  )))
      end do
  end do
  if (ize.eq.mz) then
    do i=1,mx
      fp(i,mz) = ( &
                     a*(h(i,1,1)+f(i,mz)))
    end do
  end if

!$omp end parallel
!
  if (omp_master) then
    nstag1 = nstag1+1
    nflop = nflop+2
  end if
END SUBROUTINE

!***********************************************************************
SUBROUTINE zdn22_set (f, fp)
!
!  f is centered on (i,k), fifth order stagger
!
  use params
  implicit none
  logical omp_in_parallel
  real a, b, c
  integer i, j, k
  real,             dimension(mx,   mz):: fp
  real, intent(in), dimension(mx,   mz):: f
  real,             dimension(mx,1, 3 ):: g
  real,             dimension(mx,1, 2 ):: h

!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    !$omp parallel private(k)
    do k=izs,ize
      fp(:,k) = f(:,k)
    end do
    !$omp end parallel
    return
  end if
  b = -1./16.
  a = .5-b
!
!$omp parallel private(i,k)
  do k=max(4,izs),min(mz-2,ize)
      do i=1,mx
        fp(i,k) = ( &
                     a*(f(i,k  )+f(i,k-1)) + &
                     b*(f(i,k+1)+f(i,k-2)))
      end do
  end do
  if (izs.eq.1) then
    do i=1,mx
      fp(i,1   ) = ( &
                     a*(g(i,1,3 )+f(i,1   )) + &
                     b*(g(i,1,2 )+f(i,2   )))
      fp(i,2   ) = ( &
                     a*(f(i,1   )+f(i,2   )) + &
                     b*(g(i,1,3 )+f(i,3   )))
      fp(i,3   ) = ( &
                     a*(f(i,2   )+f(i,3   )) + &
                     b*(f(i,1   )+f(i,4   )))
    end do
  end if
  if (ize.eq.mz) then
    do i=1,mx
      fp(i,mz-1) = ( &
                     a*(f(i,mz-2)+f(i,mz-1)) + &
                     b*(f(i,mz-3)+f(i,mz  )))
      fp(i,mz  ) = ( &
                     a*(f(i,mz-1)+f(i,mz  )) + &
                     b*(f(i,mz-2)+h(i,1,1 )))
    end do
  end if

!$omp end parallel
!
  if (omp_master) then
    nstag = nstag+1
    nflop = nflop+8
  end if
END SUBROUTINE

!***********************************************************************
SUBROUTINE zdn122_set (f, fp)
!
!  f is centered on (i,k), fifth order stagger
!
  use params
  implicit none
  logical omp_in_parallel
  real a, b, c
  integer i, j, k
  real,             dimension(mx,   mz):: fp
  real, intent(in), dimension(mx,   mz):: f
  real,             dimension(mx,1 ,1 ):: g
!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    !$omp parallel private(k)
    do k=izs,ize
      fp(:,k) = f(:,k)
    end do
    !$omp end parallel
    return
  end if

  a = .5
!
!$omp parallel private(i,k)
  do k=max(2,izs),ize
      do i=1,mx
        fp(i,k) = ( &
                     a*(f(i,k  )+f(i,k-1)))
      end do
  end do
  if (izs.eq.1) then
    do i=1,mx
      fp(i,1   ) = ( &
                     a*(g(i,1,1  )+f(i,1  )))
    end do
  end if
 
!$omp end parallel
!
  if (omp_master) then
    nstag1 = nstag1+1
    nflop = nflop+2
  end if
END SUBROUTINE
!***********************************************************************
SUBROUTINE ddzupi_set (f, fp)
!
!  f is centered on (i,j,k-.5), fifth order stagger
!
  use params
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k
  real, intent(in) , dimension(mx,my,mz):: f
  real,              dimension(mx,my,mz):: fp
  real,              dimension(mx,my, 2):: g
  real,              dimension(mx,my, 3):: h
!-----------------------------------------------------------------------
!

  if (mz .lt. 5) then
    !$omp parallel private(k)
    do k=izs,ize
      fp(:,:,k) = f(:,:,k)
    end do
    !$omp end parallel
    return
  end if

  b = -1./24.
  a = (1.-3.*b)
  a1st = 1

!
  !$omp parallel private(i,j,k)
  do k=max(izs,3),min(ize,mz-3)
    do j=1,my
      do i=1,mx
        fp(i,j,k) = ( &
                     a*(-f(i,j,k   )+f(i,j,k+1 )) + &
                     b*(-f(i,j,k-1 )+f(i,j,k+2 )))
     end do
    end do
  end do
  !$omp end parallel
  do j=1,my
    do i=1,mx
      fp(i,j,mz-2) = ( &
                     a*(-f(i,j,mz-2)+f(i,j,mz-1)) + &
                     b*(-f(i,j,mz-3)+f(i,j,mz  )))
      fp(i,j,mz-1) = ( &
                     a*(-f(i,j,mz-1)+f(i,j,mz  )) + &
                     b*(-f(i,j,mz-2)+h(i,j,1   )))
      fp(i,j,mz  ) = ( &
                     a*(-f(i,j,mz  )+h(i,j,1   )) + &
                     b*(-f(i,j,mz-1)+h(i,j,2   )))
      fp(i,j,1   ) = ( &
                     a*(-f(i,j,1   )+f(i,j,2   )) + &
                     b*(-g(i,j,1   )+f(i,j,3   )))
      fp(i,j,2   ) = ( &
                     a*(-f(i,j,2   )+f(i,j,3   )) + &
                     b*(-f(i,j,1   )+f(i,j,4   )))
    end do
  end do
!
  nstag = nstag+1
  nflop = nflop+8
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddzupi1_set (f, fp)
!
!  f is centered on (i,j,k-.5), fifth order stagger
!
  use params
  implicit none
  logical omp_in_parallel
  integer i, j, k
  real, intent(in) , dimension(mx,my,mz):: f
  real,              dimension(mx,my,mz):: fp
  real,              dimension(mx,my, 1):: g
!-----------------------------------------------------------------------
!
  if (mz .lt. 5) then
    !$omp parallel private(k)
    do k=izs,ize
      fp(:,:,k) = f(:,:,k)
    end do
    !$omp end parallel
    return
  end if
!
!
  !$omp parallel private(i,j,k)
  do k=izs,min(ize,mz-1)
    do j=1,my
      do i=1,mx
        fp(i,j,k  ) = ( &
                     (f(i,j,k+1)-f(i,j,k)))
      end do
    end do
  end do
  !$omp end parallel
  do j=1,my
    do i=1,mx
      fp(i,j,mz) = ( &
                     (+g(i,j,1)-f(i,j,mz)))
    end do
  end do
!
  nstag1 = nstag1+1
  nflop = nflop+2
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddzdni_set (f, fp)
!
!  f is centered on (i,j), fifth order stagger
!
  use params
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f, fp
  real, dimension(mx,my, 3):: g
  real, dimension(mx,my, 2):: h
!-----------------------------------------------------------------------
!
  if (mz .lt. 5) then
    !$omp parallel private(k)
    do k=izs,ize
      fp(:,:,k) = f(:,:,k)
    end do
    !$omp end parallel
    return
  end if

  b = -1./24.
  a = (1.-3.*b)
  a1st = 1.

!
  !$omp parallel private(i,j,k)
  do k=max(izs,4),min(ize,mz-2)
    do j=1,my
     do i=1,mx
      fp(i,j,k) = ( &
                     a*(f(i,j,k   )-f(i,j,k-1)) + &
                     b*(f(i,j,k+1 )-f(i,j,k-2)))
     end do
    end do
  end do
  !$omp end parallel
  do j=1,my
    do i=1,mx
      fp(i,j,mz-1) = ( &
                     a*(-f(i,j,mz-2)+f(i,j,mz-1)) + &
                     b*(-f(i,j,mz-3)+f(i,j,mz  )))
      fp(i,j,mz  ) = ( &
                     a*(-f(i,j,mz-1)+f(i,j,mz  )) + &
                     b*(-f(i,j,mz-2)+h(i,j,1   )))
      fp(i,j,1   ) = ( &
                     a*(-g(i,j,3   )+f(i,j,1   )) + &
                     b*(-g(i,j,2   )+f(i,j,2   )))
      fp(i,j,2   ) = ( &
                     a*(-f(i,j,1   )+f(i,j,2   )) + &
                     b*(-g(i,j,3   )+f(i,j,3   )))
      fp(i,j,3   ) = ( &
                     a*(-f(i,j,2   )+f(i,j,3   )) + &
                     b*(-f(i,j,1   )+f(i,j,4   )))
    end do
  end do
!
  nstag = nstag+1
  nflop = nflop+8
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddzdni1_set (f, fp)
!
!  f is centered on (i,j), fifth order stagger
!
  use params
  implicit none
  logical omp_in_parallel
  integer i, j, k
  real, dimension(mx,my,mz):: f, fp
  real, dimension(mx,my, 1):: g
!-----------------------------------------------------------------------
!
  if (mz .lt. 5) then
    !$omp parallel private(k)
    do k=izs,ize
      fp(:,:,k) = f(:,:,k)
    end do
    !$omp end parallel
    return
  end if
!
!
  !$omp parallel private(i,j,k)
  do k=max(izs,2),ize
    do j=1,my
     do i=1,mx
      fp(i,j,k) = ( &
                     (f(i,j,k)-f(i,j,k-1)))
     end do
    end do
  end do
  !$omp end parallel
  do j=1,my
    do i=1,mx
      fp(i,j,1   ) = ( &
                     (f(i,j,1)-g(i,j,1)))
    end do
  end do
!
  nstag1 = nstag1+1
  nflop = nflop+2
END SUBROUTINE
!***********************************************************************
SUBROUTINE ddzup_set (f, fp)
!
!  f is centered on (i,j,k-.5), fifth order stagger
!
  use params
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k
  real, intent(in) , dimension(mx,my,mz):: f
  real,              dimension(mx,my,mz):: fp
  real,              dimension(mx,my, 2):: g
  real,              dimension(mx,my, 3):: h
!-----------------------------------------------------------------------
!

  if (mz .lt. 5) then
    !$omp parallel private(k)
    do k=izs,ize
      fp(:,:,k) = 0.
    end do
    !$omp end parallel
    return
  end if

  b = -1./24.
  a = (1.-3.*b)/dz
  a1st = 1./dz
  b = b/dz

!
  !$omp parallel private(i,j,k)
  do k=max(3,izs),min(mz-3,ize)
    do j=1,my
      do i=1,mx
        fp(i,j,k) = ( &
                     b*(f(i,j,k+2 )-f(i,j,k-1 )) + &
                     a*(f(i,j,k+1 )-f(i,j,k   )))
     end do
    end do
  end do

  if (ize.eq.mz) then
   do j=1,my
    do i=1,mx
      fp(i,j,mz-2) = ( &
                     b*(f(i,j,mz  )-f(i,j,mz-3)) + &
                     a*(f(i,j,mz-1)-f(i,j,mz-2)))
      fp(i,j,mz-1) = ( &
                     b*(h(i,j,1   )-f(i,j,mz-2)) + &
                     a*(f(i,j,mz  )-f(i,j,mz-1)))
      fp(i,j,mz  ) = ( &
                     b*(h(i,j,2   )-f(i,j,mz-1)) + &
                     a*(h(i,j,1   )-f(i,j,mz  )))
    end do
   end do
  end if
  if (izs.eq.1) then
   do j=1,my
    do i=1,mx
      fp(i,j,1   ) = ( &
                     b*(f(i,j,3   )-g(i,j,2   )) + &
                     a*(f(i,j,2   )-f(i,j,1   )))
      fp(i,j,2   ) = ( &
                     b*(f(i,j,4   )-f(i,j,1   )) + &
                     a*(f(i,j,3   )-f(i,j,2   )))
    end do
   end do
  end if
  !$omp end parallel
!
  nstag = nstag+1
  nflop = nflop+8
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddzup1_set (f, fp)
!
!  f is centered on (i,j,k-.5), fifth order stagger
!
  use params
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k
  real, intent(in) , dimension(mx,my,mz):: f
  real,              dimension(mx,my,mz):: fp
  real,              dimension(mx,my, 1):: g
!-----------------------------------------------------------------------
!
  if (mz .lt. 5) then
    !$omp parallel private(k)
    do k=izs,ize
      fp(:,:,k) = 0.
    end do
    !$omp end parallel
    return
  end if
!
  a = 1./dz
!
  !$omp parallel private(i,j,k)
  do k=izs,min(mz-1,ize)
    do j=1,my
      do i=1,mx
        fp(i,j,k  ) = ( &
                     a*(f(i,j,k+1)-f(i,j,k)))
      end do
    end do
  end do
  if (ize.eq.mz) then
   do j=1,my
    do i=1,mx
      fp(i,j,mz) = ( &
                     a*(g(i,j,1)-f(i,j,mz)))
    end do
   end do
  end if
  !$omp end parallel
!
  nstag1 = nstag1+1
  nflop = nflop+2
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddzdn_set (f, fp)
!
!  f is centered on (i,j), fifth order stagger
!
  use params
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f, fp
  real, dimension(mx,my, 3):: g
  real, dimension(mx,my, 2):: h
!-----------------------------------------------------------------------
!
  if (mz .lt. 5) then
    !$omp parallel private(k)
    do k=izs,ize
      fp(:,:,k) = 0.
    end do
    !$omp end parallel
    return
  end if

  b = -1./24.
  a = (1.-3.*b)/dz
  a1st = 1./dz
  b = b/dz

!
  !$omp parallel private(i,j,k)
  do k=max(4,izs),min(mz-2,ize)
    do j=1,my
     do i=1,mx
      fp(i,j,k) = ( &
                     b*(f(i,j,k+1 )-f(i,j,k-2 )) + &
                     a*(f(i,j,k   )-f(i,j,k-1 )))
     end do
    end do
  end do
  if (ize.eq.mz) then
   do j=1,my
    do i=1,mx
      fp(i,j,mz-1) = ( &
                     b*(f(i,j,mz  )-f(i,j,mz-3)) + &
                     a*(f(i,j,mz-1)-f(i,j,mz-2)))
      fp(i,j,mz  ) = ( &
                     b*(h(i,j,1   )-f(i,j,mz-2)) + &
                     a*(f(i,j,mz  )-f(i,j,mz-1)))
    end do
   end do
  end if
  if (izs.eq.1) then
   do j=1,my
    do i=1,mx
      fp(i,j,1   ) = ( &
                     b*(f(i,j,2   )-g(i,j,2   )) + &
                     a*(f(i,j,1   )-g(i,j,3   )))
      fp(i,j,2   ) = ( &
                     b*(f(i,j,3   )-g(i,j,3   )) + &
                     a*(f(i,j,2   )-f(i,j,1   )))
      fp(i,j,3   ) = ( &
                     b*(f(i,j,4   )-f(i,j,1   )) + &
                     a*(f(i,j,3   )-f(i,j,2   )))
    end do
   end do
  end if
  !$omp end parallel
!
  nstag = nstag+1
  nflop = nflop+8
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddzdn1_set (f, fp)
!
!  f is centered on (i,j), fifth order stagger
!
  use params
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f, fp
  real, dimension(mx,my, 1):: g
!-----------------------------------------------------------------------
!
  if (mz .lt. 5) then
    !$omp parallel private(k)
    do k=izs,ize
      fp(:,:,k) = 0.
    end do
    !$omp end parallel
    return
  end if
!
  a = 1./dz
!
  !$omp parallel private(i,j,k)
  do k=max(2,izs),ize
    do j=1,my
     do i=1,mx
      fp(i,j,k) = ( &
                     a*(f(i,j,k)-f(i,j,k-1)))
     end do
    end do
  end do
  if (izs.eq.1) then
   do j=1,my
    do i=1,mx
      fp(i,j,1   ) = ( &
                     a*(f(i,j,1)-g(i,j,1  )))
    end do
   end do
  end if
  !$omp end parallel
!
  nstag1 = nstag1+1
  nflop = nflop+2
END SUBROUTINE
! $Id: stagger_4th_mpi.f90,v 1.1 2012/07/13 00:11:51 aake Exp $
!***********************************************************************
SUBROUTINE crl1_set (Ux, Uy, Uz, out)
!
!  Add a lower order, centered approximation of abs(w) to the out array
!
  USE params
  implicit none
  logical omp_in_parallel
  integer iz
  real, dimension(mx,my,mz):: Ux, Uy, Uz, out
  real, allocatable, dimension(:,:,:):: w2, s
!
  allocate (w2(mx,my,mz), s(mx,my,mz))
  !$omp parallel do private(iz)
  do iz=1,mz
    out(:,:,iz) = sqrt(w2(:,:,iz))
  end do
  deallocate (w2, s)
END
!***********************************************************************
SUBROUTINE crl1_add (Ux, Uy, Uz, out)
!
!  Add a lower order, centered approximation of abs(w) to the out array
!
  USE params
  implicit none
  logical omp_in_parallel
  integer iz
  real, dimension(mx,my,mz):: Ux, Uy, Uz, out
  real, allocatable, dimension(:,:,:):: w2, s
!
  allocate (w2(mx,my,mz), s(mx,my,mz))
  !$omp parallel do private(iz)
  do iz=1,mz
    out(:,:,iz) = out(:,:,iz) + sqrt(w2(:,:,iz))
  end do
  deallocate (w2, s)
END

!***********************************************************************
SUBROUTINE smooth (f, dir)
!
!  Three point smooth
!
  USE params, gx=>gx1, hx=>hx1
  implicit none
  logical omp_in_parallel
  logical debug
  integer i, j, k, dir
  real, dimension(mx,my,mz):: f, fp
  real, dimension(0:mx+1):: fx
  real, dimension(0:my+1):: fy
  real, dimension(mx,0:mz+1):: fz
  real, dimension(mx,my,1):: gz, hz
  real c
!-----------------------------------------------------------------------
!

  if (dir==1) then
    do k=1,mz
     do j=1,my
      do i=1,mx
        fx(i)=f(i,j,k)
      end do
      fx(0)=gx(1,j,k)
      fx(mx+1)=hx(1,j,k)
      do i=1,mx
        f(i,j,k)=(1.-2*c)*fx(i)+c*(fx(i-1)+fx(i+1))
      end do
     end do
    end do

  else if (dir==2) then
    do k=1,mz
     do i=1,mx
      do j=1,my
        fy(j)=f(i,j,k)
      end do
      fy(0)=gy1(i,1,k)
      fy(my+1)=hy1(i,1,k)
      do j=merge(1,lb+1,lb.le.1),merge(my,ub-1,ub.ge.my)
        f(i,j,k)=(1.-2*c)*fy(j)+c*(fy(j-1)+fy(j+1))
      end do
     end do
    end do

  else if (mz > 1) then
    do j=1,my
     do k=1,mz
     do i=1,mx
       fz(i,k)=f(i,j,k)
     end do
     end do
     do i=1,mx
       fz(i,   0)=gz(i,j,1)
       fz(i,mz+1)=hz(i,j,1)
     end do
     do k=1,mz
      do i=1,mx
        f(i,j,k)=(1.-2*c)*fz(i,k)+c*(fz(i,k-1)+fz(i,k+1))
      end do
     end do
    end do
  end if
END SUBROUTINE

!***********************************************************************
SUBROUTINE smooth3_set (f, fp)
!
!  Three point smooth
!
  USE params, gx=>gx1, hx=>hx1
  implicit none
  logical omp_in_parallel
  logical debug
  integer i,j,k
  real, dimension(mx,my,mz):: f, fp
  real, dimension(0:mx+1):: fx
  real, dimension(0:my+1):: fy
  real, dimension(mx,my,1):: gz, hz
  real c3
!-----------------------------------------------------------------------
!


  do k=izs,ize
   do j=1,my
    do i=1,mx
      fx(i)=f(i,j,k)
    end do
    fx(0)=gx(1,j,k)
    fx(mx+1)=hx(1,j,k)
    do i=1,mx
      scratch(i,j,k)=c3*(fx(i-1)+fx(i)+fx(i+1))
    end do
   end do
  end do


  do k=izs,ize
   do i=1,mx
    do j=1,my
      fy(j)=scratch(i,j,k)
    end do
    fy(0)=gy1(i,1,k)
    fy(my+1)=hy1(i,1,k)
    do j=1,my
      scratch(i,j,k)=c3*(fy(j-1)+fy(j)+fy(j+1))
    end do
   end do
  end do

  if (mz < 3) then
    fp = scratch
    return
  end if

  do k=max(2,izs),min(mz-1,ize)
   do j=1,my
    do i=1,mx
      fp(i,j,k)=c3*(scratch(i,j,k)+scratch(i,j,k-1)+scratch(i,j,k+1))
    end do
   end do
  end do
  if (izs.eq. 1) then
   do j=1,my
    do i=1,mx
      fp(i,j,1)=c3*(scratch(i,j,1)+gz(i,j,1)+scratch(i,j,2))
    end do
   end do
  end if
  if (ize.eq.mz) then
   do j=1,my
    do i=1,mx
      fp(i,j,mz)=c3*(scratch(i,j,mz)+scratch(i,j,mz-1)+hz(i,j,1))
    end do
   end do
  end if

END SUBROUTINE

!***********************************************************************
SUBROUTINE smooth3t_set (f, fp)
!
!  Three point smooth
!
  USE params, gx=>gx1, hx=>hx1
  implicit none
  logical omp_in_parallel
  integer i,j,k
  real, dimension(mx,my,mz):: f, fp
  real, dimension(0:mx+1):: fx
  real, dimension(0:my+1):: fy
  real, dimension(mx,my,1):: gz, hz
  real c3
!-----------------------------------------------------------------------
!


  do k=izs,ize
   do j=1,my
    do i=1,mx
      fx(i)=f(i,j,k)
    end do
    fx(0)=gx(1,j,k)
    fx(mx+1)=hx(1,j,k)
    do i=1,mx
      scratch(i,j,k)=0.25*(fx(i-1)+fx(i)+fx(i)+fx(i+1))
    end do
   end do
  end do


  do k=izs,ize
   do i=1,mx
    do j=1,my
      fy(j)=scratch(i,j,k)
    end do
    fy(0)=gy1(i,1,k)
    fy(my+1)=hy1(i,1,k)
    do j=1,my
      scratch(i,j,k)=0.25*(fy(j-1)+fy(j)+fy(j)+fy(j+1))
    end do
   end do
  end do

  if (mz < 3) then
    fp = scratch
    return
  end if


  do k=max(2,izs),min(mz-1,ize)
   do j=1,my
    do i=1,mx
      fp(i,j,k)=0.25*(scratch(i,j,k-1)+scratch(i,j,k)+scratch(i,j,k)+scratch(i,j,k+1))
    end do
   end do
  end do
  if (izs.eq. 1) then
   do j=1,my
    do i=1,mx
      fp(i,j,1)=0.25*(gz(i,j,1)+scratch(i,j,1)+scratch(i,j,1)+scratch(i,j,2))
    end do
   end do
  end if
  if (ize.eq.mz) then
   do j=1,my
    do i=1,mx
      fp(i,j,mz)=0.25*(scratch(i,j,mz-1)+scratch(i,j,mz)+scratch(i,j,mz)+hz(i,j,1))
    end do
   end do
  end if

END SUBROUTINE

!***********************************************************************
SUBROUTINE smooth3th_set (f, fp)
!
!  Three point smooth horizontally
!
  USE params, gx=>gx1, hx=>hx1
  implicit none
  logical omp_in_parallel
  integer i,j,k
  real, dimension(mx,my,mz):: f, fp
  real, dimension(0:mx+1):: fx
  real, dimension(0:my+1):: fy
  real, dimension(mx,my,1):: gz, hz
  real c3
!-----------------------------------------------------------------------
!


  do k=izs,ize
   do j=1,my
    do i=1,mx
      fx(i)=f(i,j,k)
    end do
    fx(0)=gx(1,j,k)
    fx(mx+1)=hx(1,j,k)
    do i=1,mx
      scratch(i,j,k)=0.25*(fx(i-1)+fx(i)+fx(i)+fx(i+1))
    end do
   end do
  end do


  if (mz < 3) then
    fp = scratch
    return
  end if


  do k=max(2,izs),min(mz-1,ize)
   do j=1,my
    do i=1,mx
      fp(i,j,k)=0.25*(scratch(i,j,k-1)+scratch(i,j,k)+scratch(i,j,k)+scratch(i,j,k+1))
    end do
   end do
  end do
  if (izs.eq. 1) then
   do j=1,my
    do i=1,mx
      fp(i,j,1)=0.25*(gz(i,j,1)+scratch(i,j,1)+scratch(i,j,1)+scratch(i,j,2))
    end do
   end do
  end if
  if (ize.eq.mz) then
   do j=1,my
    do i=1,mx
      fp(i,j,mz)=0.25*(scratch(i,j,mz-1)+scratch(i,j,mz)+scratch(i,j,mz)+hz(i,j,1))
    end do
   end do
  end if

END SUBROUTINE

!***********************************************************************
SUBROUTINE max3_set (f, fp)
!
!  Three point max
!
  USE params, gx=>gx1, hx=>hx1
  implicit none
  logical omp_in_parallel
  logical debug
  integer i,j,k,km1,kp1
  real, dimension(mx,my,mz):: f, fp
  real, dimension(0:mx+1):: fx
  real, dimension(0:my+1):: fy
  real, dimension(mx,my,1):: gz, hz
!-----------------------------------------------------------------------



  do k=izs,ize
   do j=1,my
    do i=1,mx
      fx(i)=f(i,j,k)
    end do
    fx(0)=gx(1,j,k)
    fx(mx+1)=hx(1,j,k)
    do i=1,mx
      scratch(i,j,k)=amax1(fx(i-1),fx(i),fx(i+1))
    end do
   end do
  end do


  do k=izs,ize
   do i=1,mx
    do j=1,my
      fy(j)=scratch(i,j,k)
    end do
    fy(0)=gy1(i,1,k)
    fy(my+1)=hy1(i,1,k)
    do j=1,my
      scratch(i,j,k)=amax1(fy(j-1),fy(j),fy(j+1))
    end do
   end do
  end do
 

  if (mz < 3) then
    fp = scratch
    return
  end if


  do k=max(2,izs),min(mz-1,ize)
   do j=1,my
    do i=1,mx
      fp(i,j,k)=amax1(scratch(i,j,k),scratch(i,j,k-1),scratch(i,j,k+1))
    end do
   end do
  end do
  if (izs.eq.1) then
   do j=1,my
    do i=1,mx
      fp(i,j,1)=amax1(scratch(i,j,1),gz(i,j,1),scratch(i,j,2))
    end do
   end do
  end if
  if (ize.eq.mz) then
   do j=1,my
    do i=1,mx
      fp(i,j,mz)=amax1(scratch(i,j,mz),hz(i,j,1),scratch(i,j,mz-1))
    end do
   end do
  end if

END SUBROUTINE

!***********************************************************************
SUBROUTINE max5_set (f, fp)
!
!  Three point max
!
  USE params, gx=>gx2, hx=>hx2
  implicit none
  logical omp_in_parallel
  integer i,j,k
  real, dimension(mx,my,mz), intent(in) :: f
  real, dimension(mx,my,mz):: fp
  real, dimension(-1:mx+2):: fx
  real, dimension(-1:my+2):: fy
  real, dimension(mx,my,2):: gz, hz
!-----------------------------------------------------------------------



  do k=izs,ize
   do j=1,my
    do i=1,mx
      fx(i)=f(i,j,k)
    end do
    fx(  -1)=gx(1,j,k)
    fx(   0)=gx(2,j,k)
    fx(mx+1)=hx(1,j,k)
    fx(mx+2)=hx(2,j,k)
    do i=1,mx
      scratch(i,j,k)=amax1(fx(i-2),fx(i-1),fx(i),fx(i+1),fx(i+2))
    end do
   end do
  end do


  do k=izs,ize
   if (my.gt.1) then
     do i=1,mx
      do j=1,my
        fy(j)=scratch(i,j,k)
      end do
      fy(  -1)=gy2(i,1,k)
      fy(   0)=gy2(i,2,k)
      fy(my+1)=hy2(i,1,k)
      fy(my+2)=hy2(i,2,k)
      do j=1,my
        scratch(i,j,k)=amax1(fy(j-2),fy(j-1),fy(j),fy(j+1),fy(j+2))
      end do
     end do
    end if
  end do


  if (mz < 5) then
    fp = scratch
    return
  end if


  do k=max(3,izs),min(mz-2,ize)
   do j=1,my
    do i=1,mx
      fp(i,j,k)=amax1(scratch(i,j,k-2),scratch(i,j,k-1),scratch(i,j,k),scratch(i,j,k+1),scratch(i,j,k+2))
    end do
   end do
  end do
  if (izs.eq.1) then
   do j=1,my
    do i=1,mx
      fp(i,j,1)=amax1(gz(i,j,1),gz(i,j,2),scratch(i,j,1),scratch(i,j,2),scratch(i,j,3))
      fp(i,j,2)=amax1(gz(i,j,2),scratch(i,j,1),scratch(i,j,2),scratch(i,j,3),scratch(i,j,4))
    end do
   end do
  end if
  if (ize.eq.mz) then
   do j=1,my
    do i=1,mx
      fp(i,j,mz-1)=amax1(scratch(i,j,mz-3),scratch(i,j,mz-2),scratch(i,j,mz-1),scratch(i,j,mz),hz(i,j,1))
      fp(i,j,mz  )=amax1(scratch(i,j,mz-2),scratch(i,j,mz-1),scratch(i,j,mz  ),hz(i,j,1 ),hz(i,j,2))
    end do
   end do
  end if

END SUBROUTINE

!***********************************************************************
SUBROUTINE smooth3max5_set (f, fz)
!
!  Three point max
!
  USE params
  implicit none
  logical omp_in_parallel
  real, dimension(mx,my,mz) :: f, fz

!
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE smooth3max3_set (f, fz)
!
!  Three point max
!
  USE params
  implicit none
  logical omp_in_parallel
  real, dimension(mx,my,mz) :: f, fz

!
!
END SUBROUTINE


!***********************************************************************
SUBROUTINE d2abs_add (f, fp)
!
!  Max abs difference one zone left to one zones right.
!

  USE params
  implicit none
  logical omp_in_parallel
  real cx, cy, cz
  real, dimension(mx,my,mz):: f, fp
  real, dimension(mx,my,1):: gz, hz
!hpf$ align with TheCube:: f, fp
  integer i, j, k, km1, kp1
!-----------------------------------------------------------------------

!$omp parallel private(i,j,k,km1,kp1)

  if (mx.gt.1) then
    do k=izs,ize
      do j=1,my
        do i=2,mx-1
          fp(i,j,k) = fp(i,j,k) + abs(f(i-1 ,j,k)+f(i+1,j,k)-f(i,j,k)-f(i,j,k))
        end do
        fp(1 ,j,k) = fp(1 ,j,k) + abs(gx1(1   ,j,k)+f (2,j,k)-f(1 ,j,k)-f(1 ,j,k))
        fp(mx,j,k) = fp(mx,j,k) + abs(f (mx-1,j,k)+hx1(1,j,k)-f(mx,j,k)-f(mx,j,k))
      end do
    end do
  else
    do k=izs,ize
      do j=1,my
        do i=1,mx
          fp(i,j,k) = fp(i,j,k) + 0.
        end do
      end do
    end do
  end if

 if (my .gt. 1) then
  do k=izs,ize
    do j=2,my-1
      do i=1,mx
        fp(i,j, k) = fp(i,j, k) + max(fp(i,j, k),abs(f(i,j -1,k)+f(i,j+1,k)-f(i,j,k)-f(i,j,k)))
      end do
    end do
    do i=1,mx
        fp(i,1 ,k) = fp(i,1 ,k) + max(fp(i,1 ,k),abs(gy1(i,   1,k)+f (i,2,k)-f(i, 1,k)-f(i, 1,k)))
        fp(i,my,k) = fp(i,my,k) + max(fp(i,my,k),abs(f (i,my-1,k)+hy1(i,1,k)-f(i,my,k)-f(i,my,k)))
    end do
  end do
 end if
 if (mz .gt. 1) then
!$omp barrier
  do k=izs,ize
    if (k.eq.1) then
      do j=1,my
      do i=1,mx
        fp(i,j,k ) = fp(i,j,k ) + max(fp(i,j,k ),abs(gz(i,j,1)+f(i,j,k+1)-f(i,j,k)-f(i,j,k)))
      end do
      end do
    else if (k.eq.mz) then
      do j=1,my
      do i=1,mx
        fp(i,j,k ) = fp(i,j,k ) + max(fp(i,j,k ),abs(hz(i,j,1)+f(i,j,k-1)-f(i,j,k)-f(i,j,k)))
      end do
      end do
    else
      do j=1,my
      do i=1,mx
        fp(i,j,k ) = fp(i,j,k ) + max(fp(i,j,k ),abs(f(i,j,k-1)+f(i,j,k+1)-f(i,j,k)-f(i,j,k)))
      end do
      end do
    end if
  end do
 end if
!$omp end parallel

END SUBROUTINE
!***********************************************************************
SUBROUTINE dif2a_add (f, fp)
!
!  Max abs difference one zone left to one zones right.
!

  USE params
  implicit none
  logical omp_in_parallel
  real cx, cy, cz
  real, dimension(mx,my,mz):: f, fp
  real, dimension(mx,my,1):: gz, hz
!hpf$ align with TheCube:: f, fp
  integer i, j, k, km1, kp1
!-----------------------------------------------------------------------

!$omp parallel private(i,j,k,km1,kp1)

  if (mx.gt.1) then
    do k=izs,ize
      do j=1,my
        do i=2,mx-1
          fp(i,j,k) = fp(i,j,k) + abs(f(i-1 ,j,k)-f(i+1,j,k))
        end do
        fp(1 ,j,k) = fp(1 ,j,k) + abs(gx1(1   ,j,k)-f (2,j,k))
        fp(mx,j,k) = fp(mx,j,k) + abs(f (mx-1,j,k)-hx1(1,j,k))
      end do
    end do
  else
    do k=izs,ize
      do j=1,my
        do i=1,mx
          fp(i,j,k) = fp(i,j,k) + 0.
        end do
      end do
    end do
  end if

 if (my .gt. 1) then
  do k=izs,ize
    do j=2,my-1
      do i=1,mx
        fp(i,j, k) = fp(i,j, k) + max(fp(i,j, k),abs(f(i,j -1,k)-f(i,j+1,k)))
      end do
    end do
    do i=1,mx
        fp(i,1 ,k) = fp(i,1 ,k) + max(fp(i,1 ,k),abs(gy1(i,   1,k)-f (i,2,k)))
        fp(i,my,k) = fp(i,my,k) + max(fp(i,my,k),abs(f (i,my-1,k)-hy1(i,1,k)))
    end do
  end do
 end if
 if (mz .gt. 1) then
!$omp barrier
  do k=izs,ize
    if (k.eq.1) then
      do j=1,my
      do i=1,mx
        fp(i,j,k ) = fp(i,j,k ) + max(fp(i,j,k ),abs(gz(i,j,1)-f(i,j,k+1)))
      end do
      end do
    else if (k.eq.mz) then
      do j=1,my
      do i=1,mx
        fp(i,j,k ) = fp(i,j,k ) + max(fp(i,j,k ),abs(hz(i,j,1)-f(i,j,k-1)))
      end do
      end do
    else
      do j=1,my
      do i=1,mx
        fp(i,j,k ) = fp(i,j,k ) + max(fp(i,j,k ),abs(f(i,j,k-1)-f(i,j,k+1)))
      end do
      end do
    end if
  end do
 end if
!$omp end parallel

END SUBROUTINE

!***********************************************************************
SUBROUTINE dif2_add (fx, fy, fz, fp)
!
!  Finite difference one zone left to one zones right.
!

  USE params
  implicit none
  logical omp_in_parallel
  real cx(mx), cy(my), cz(mz)
  real, dimension(mx,my,mz):: fx, fy, fz, fp
  real, dimension(mx,my,1):: gz, hz
  integer i, j, k, km1, kp1
!-----------------------------------------------------------------------


!$omp parallel private(i,j,k,km1,kp1)


  if (mx.gt.1) then
    do k=izs,ize
      do j=1,my
        do i=2,mx-1
        end do
      end do
    end do
  else
    do k=izs,ize
      do j=1,my
        do i=1,mx
          fp(i,j,k) = fp(i,j,k) + 0.
        end do
      end do
    end do
  end if


  if (my .gt. 1) then
    do k=izs,ize
     do j=2,my-1
       do i=1,mx
       end do
     end do
     do i=1,mx
     end do
    end do
  end if


 if (mz .gt. 1) then
!$omp barrier
  do k=izs,ize
    if (k.eq.1) then
      do j=1,my
      do i=1,mx
      end do
      end do
    else if (k.eq.mz) then
      do j=1,my
      do i=1,mx
      end do
      end do
    else
      do j=1,my
      do i=1,mx
      end do
      end do
    end if
  end do
 end if

 do k=izs,ize
   do j=1,my
     do i=1,mx
       fp(i,j,k) = fp(i,j,k) + max (0., fp(i,j,k))
     end do
   end do
 end do
!$omp end parallel

END SUBROUTINE
!***********************************************************************
SUBROUTINE dif2d_add (fx, fy, fz, fp)
!
!  Finite difference one zone left to one zones right.
!

  USE params
  implicit none
  logical omp_in_parallel
  real, dimension(mx,my,mz):: fx, fy, fz, fp
  real, dimension(mx,my,1):: gz, hz
  integer i, j, k, km1, kp1
!-----------------------------------------------------------------------


!$omp parallel private(i,j,k,km1,kp1)

  if (mx.gt.1) then
    do k=izs,ize
      do j=1,my
        do i=2,mx-1
          fp(i,j,k) = fp(i,j,k) + (fx(i-1 ,j,k)-fx(i+1,j,k))
        end do
        fp(1 ,j,k) = fp(1 ,j,k) + (gx1(1   ,j,k)-fx(2  ,j,k))
        fp(mx,j,k) = fp(mx,j,k) + (fx(mx-1,j,k)-hx1(1  ,j,k))
      end do
    end do
  else
    do k=izs,ize
      do j=1,my
        do i=1,mx
          fp(i,j,k) = fp(i,j,k) + 0.
        end do
      end do
    end do
  end if


  if (my .gt. 1) then
    do k=izs,ize
     do j=2,my-1
       do i=1,mx
         fp(i,j, k) = fp(i,j, k) + fp(i,j, k) + (fy(i,j -1,k)-fy(i,j+1,k))
       end do
     end do
     do i=1,mx
         fp(i,1 ,k) = fp(i,1 ,k) + fp(i,1 ,k) + (gy1(i,   1,k)-fy(i,  2,k))
         fp(i,my,k) = fp(i,my,k) + fp(i,my,k) + (fy(i,my-1,k)-hy1(i,  1,k))
     end do
    end do
  end if


 if (mz .gt. 1) then
!$omp barrier
  do k=izs,ize
    if (k.eq.1) then
      do j=1,my
      do i=1,mx
        fp(i,j,k ) = fp(i,j,k ) + fp(i,j,k ) + (gz(i,j,  1)-fz(i,j,k+1))
      end do
      end do
    else if (k.eq.mz) then
      do j=1,my
      do i=1,mx
        fp(i,j,k ) = fp(i,j,k ) + fp(i,j,k ) + (fz(i,j,k-1)-hz(i,j,1))
      end do
      end do
    else
      do j=1,my
      do i=1,mx
        fp(i,j,k ) = fp(i,j,k ) + fp(i,j,k ) + (fz(i,j,k-1)-fz(i,j,k+1))
      end do
      end do
    end if
  end do
 end if

 do k=izs,ize
   do j=1,my
     do i=1,mx
       fp(i,j,k) = fp(i,j,k) + max (0., fp(i,j,k))
     end do
   end do
 end do
!$omp end parallel

END SUBROUTINE

!***********************************************************************
SUBROUTINE dif1xy_add (fx, fy, fp)
!
!  Finite difference curl one zone left to center
!
  USE params
  implicit none
  logical omp_in_parallel
  real, dimension(mx,my,mz):: fx, fy, fp
  real, dimension(mx):: cx
  real, dimension(my):: cy
  integer i, j, k, km1, kp1
!-----------------------------------------------------------------------
!$omp parallel private(i,j,k,km1,kp1)
  if (mx.gt.1) then
    do k=izs,ize
      do j=1,my
        do i=2,mx
        end do
      end do
    end do
  else
    do k=izs,ize
      do j=1,my
        do i=1,mx
          fp(i,j,k) = fp(i,j,k) + 0.
        end do
      end do
    end do
  end if
  if (my .gt. 1) then
    do k=izs,ize
     do j=2,my
       do i=1,mx
         fp(i,j, k) = fp(i,j, k) + fp(i, j,k) - cy(j)*(fx(i, j,k)-fx(i,j-1,k))
       end do
     end do
     do i=1,mx
         fp(i,1,k) = fp(i,1,k) + fp(i,1,k) - cy(1)*(fx(i,1,k)-gy1(i,  1,k))
     end do
    end do
  end if
  do k=izs,ize
    do j=1,my
      do i=1,mx
        fp(i,j,k) = fp(i,j,k) + fp(i,j,k)**2
      end do
    end do
  end do
!$omp end parallel
END SUBROUTINE

!***********************************************************************
SUBROUTINE dif1yz_add (fy, fz, fp)
!
!  Finite difference one zone left to center
!
  USE params
  implicit none
  logical omp_in_parallel
  real, dimension(mx,my,mz):: fy, fz, fp
  real, dimension(mx,my,1):: gz, hz
  real, dimension(my):: cy
  real, dimension(mz):: cz
  integer i, j, k, km1, kp1
!-----------------------------------------------------------------------
!$omp parallel private(i,j,k,km1,kp1)
  if (my .gt. 1) then
    do k=izs,ize
     do j=2,my
       do i=1,mx
       end do
     end do
     do i=1,mx
     end do
    end do
  end if
  if (mz .gt. 1) then
!$omp barrier
   do k=izs,ize
     if (k.eq.1) then
       do j=1,my
       do i=1,mx
         fp(i,j,k) = fp(i,j,k) + fp(i,j,k) - cz(k)*(fy(i,j,k)-gz(i,j,1))
       end do
       end do
     else
       do j=1,my
       do i=1,mx
         fp(i,j,k) = fp(i,j,k) + fp(i,j,k) - cz(k)*(fy(i,j,k)-fy(i,j,k-1))
       end do
       end do
     end if
   end do
  end if
  do k=izs,ize
    do j=1,my
      do i=1,mx
        fp(i,j,k) = fp(i,j,k) + fp(i,j,k)**2
      end do
    end do
  end do
 !$omp end parallel
END SUBROUTINE

!***********************************************************************
SUBROUTINE dif1zx_add (fz, fx, fp)
!
!  Finite difference one zone left to center
!
  USE params
  implicit none
  logical omp_in_parallel
  real, dimension(mx,my,mz):: fx, fz, fp
  real, dimension(mx,my,1):: gz, hz
  real, dimension(mx):: cx
  real, dimension(mz):: cz
  integer i, j, k, km1, kp1
!-----------------------------------------------------------------------
!$omp parallel private(i,j,k,km1,kp1)
  if (mx.gt.1) then
    do k=izs,ize
      do j=1,my
        do i=2,mx
        end do
      end do
    end do
  else
    do k=izs,ize
      do j=1,my
        do i=1,mx
          fp(i,j,k) = fp(i,j,k) + 0.
        end do
      end do
    end do
  end if
  if (mz .gt. 1) then
!$omp barrier
    do k=izs,ize
      if (k.eq.1) then
        do j=1,my
        do i=1,mx
          fp(i,j,k) = fp(i,j,k) + fp(i,j,k) - cz(k)*(fx(i,j,k)-gz(i,j,1))
        end do
        end do
      else
        do j=1,my
        do i=1,mx
          fp(i,j,k) = fp(i,j,k) + fp(i,j,k) - cz(k)*(fx(i,j,k)-fx(i,j,k-1))
        end do
        end do
      end if
    end do
   end if
   do k=izs,ize
     do j=1,my
       do i=1,mx
         fp(i,j,k) = fp(i,j,k) + fp(i,j,k)**2
       end do
     end do
   end do
!$omp end parallel
END SUBROUTINE

!***********************************************************************
SUBROUTINE dif1_add (fx, fy, fz, fp)
!
!  Finite difference one zone left to center
!

  USE params
  implicit none
  logical omp_in_parallel
  real, dimension(mx,my,mz):: fx, fy, fz, fp
  real, dimension(mx,my,1):: gz, hz
  real, dimension(mx):: cx
  real, dimension(my):: cy
  real, dimension(mz):: cz
  integer i, j, k, km1, kp1
!-----------------------------------------------------------------------



!$omp parallel private(i,j,k,km1,kp1)


  if (mx.gt.1) then
    do k=izs,ize
      do j=1,my
        do i=1,mx-1
        end do
      end do
    end do
  else
    do k=izs,ize
      do j=1,my
        do i=1,mx
          fp(i,j,k) = fp(i,j,k) + 0.
        end do
      end do
    end do
  end if


  if (my .gt. 1) then
    do k=izs,ize
     do j=1,my-1
       do i=1,mx
       end do
     end do
     do i=1,mx
     end do
    end do
  end if


 if (mz .gt. 1) then
!$omp barrier
  do k=izs,ize
    if (k.eq.mz) then
      do j=1,my
      do i=1,mx
      end do
      end do
    else
      do j=1,my
      do i=1,mx
      end do
      end do
    end if
  end do
 end if

 do k=izs,ize
   do j=1,my
     do i=1,mx
       fp(i,j,k) = fp(i,j,k) + max (0., fp(i,j,k))
     end do
   end do
 end do
!$omp end parallel

END SUBROUTINE

!***********************************************************************
SUBROUTINE dif1d_add (fx, fy, fz, fp)
!
!  Finite difference one zone left to center
!

  USE params
  implicit none
  logical omp_in_parallel
  real, dimension(mx,my,mz):: fx, fy, fz, fp
  real, dimension(mx,my,1):: gz, hz
  integer i, j, k, km1, kp1
!-----------------------------------------------------------------------


!$omp parallel private(i,j,k,km1,kp1)


  if (mx.gt.1) then
    do k=izs,ize
      do j=1,my
        do i=1,mx-1
          fp(i,j,k) = fp(i,j,k) + (fx(i ,j,k)-fx(i+1,j,k))
        end do
        fp(mx,j,k) = fp(mx,j,k) + (fx(mx,j,k)-hx1(  1,j,k))
      end do
    end do
  else
    do k=izs,ize
      do j=1,my
        do i=1,mx
          fp(i,j,k) = fp(i,j,k) + 0.
        end do
      end do
    end do
  end if


  if (my .gt. 1) then
    do k=izs,ize
     do j=1,my-1
       do i=1,mx
         fp(i,j, k) = fp(i,j, k) + fp(i, j,k) + (fy(i, j,k)-fy(i,j+1,k))
       end do
     end do
     do i=1,mx
         fp(i,my,k) = fp(i,my,k) + fp(i,my,k) + (fy(i,my,k)-hy1(i,  1,k))
     end do
    end do
  end if


 if (mz .gt. 1) then
!$omp barrier
  do k=izs,ize
    if (k.eq.mz) then
      do j=1,my
      do i=1,mx
        fp(i,j,k) = fp(i,j,k) + fp(i,j,k) + (fz(i,j,k)-hz(i,j,1))
      end do
      end do
    else
      do j=1,my
      do i=1,mx
        fp(i,j,k) = fp(i,j,k) + fp(i,j,k) + (fz(i,j,k)-fz(i,j,k+1))
      end do
      end do
    end if
  end do
 end if

 do k=izs,ize
   do j=1,my
     do i=1,mx
       fp(i,j,k) = fp(i,j,k) + max (0., fp(i,j,k))
     end do
   end do
 end do
!$omp end parallel

END SUBROUTINE

!***********************************************************************
SUBROUTINE xup_add (f, fp)
!
!  f is centered on (i-.5,j,k), fifth order stagger
!
  use params, g=>gx2, h=>hx3
  implicit none
  logical omp_in_parallel
  real a, b, c
  integer i, j, k
  real, dimension(mx,my,mz):: f, fp
!-----------------------------------------------------------------------
!
!
  if (mx.le.5) then
    fp = fp + f
    return
  end if
  b = -1./16.
  a = .5-b

!$omp parallel private(i,j,k)


  do k=izs,ize
   do j=1,my
    fp(mx-2,j,k) = fp(mx-2,j,k) + ( &
                   a*(f(mx-2,j,k)+f(mx-1,j,k)) &
                 + b*(f(mx-3,j,k)+f(mx  ,j,k)))
    fp(mx-1,j,k) = fp(mx-1,j,k) + ( &
                   a*(f(mx-1,j,k)+f(mx  ,j,k)) &
                 + b*(f(mx-2,j,k)+h(1   ,j,k)))
    fp(mx  ,j,k) = fp(mx  ,j,k) + ( &
                   a*(f(mx  ,j,k)+h(1   ,j,k)) &
                 + b*(f(mx-1,j,k)+h(2   ,j,k)))
    fp(1   ,j,k) = fp(1   ,j,k) + ( &
                   a*(f(1   ,j,k)+f(2   ,j,k)) &
                 + b*(g(2   ,j,k)+f(3   ,j,k)))
    fp(2   ,j,k) = fp(2   ,j,k) + ( &
                   a*(f(2   ,j,k)+f(3   ,j,k)) &
                 + b*(f(1   ,j,k)+f(4   ,j,k)))
   end do
   do j=1,my
    do i=3,mx-3
      fp(i  ,j,k) = fp(i  ,j,k) + ( &
                    a*(f(i  ,j,k)+f(i+1 ,j,k)) &
                  + b*(f(i-1,j,k)+f(i+2 ,j,k)))
    end do
   end do
  end do
!$omp end parallel
!
  nstag = nstag+1
  nflop = nflop+8
END SUBROUTINE

!***********************************************************************
SUBROUTINE xup1_add (f, fp)
!
!  f is centered on (i-.5,j,k), first order stagger
!
  use params, h=>hx1
  implicit none
  logical omp_in_parallel
  real a, b, c
  integer i, j, k
  real, dimension(mx,my,mz):: f, fp
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = fp + f
    return
  end if
!
  a = .5

!$omp parallel private(i,j,k)

!
  do k=izs,ize
   do j=1,my
    fp(mx  ,j,k) = fp(mx  ,j,k) +  ( &
                    a*(f(mx,j,k)+h(  1,j,k)))
   end do
   do j=1,my
    do i=1,mx-1
      fp(i  ,j,k) = fp(i  ,j,k) + ( &
                    a*(f(i ,j,k)+f(i+1,j,k)))
    end do
   end do
  end do
!$omp end parallel
!
  nstag1 = nstag1+1
  nflop = nflop+2
END SUBROUTINE

!***********************************************************************
SUBROUTINE xdn_add (f, fp)
!
!  f is centered on (i,j,k), fifth order stagger
!
  use params, g=>gx3, h=>hx2
  implicit none
  logical omp_in_parallel
  real a, b, c
  integer i, j, k
  real, dimension(mx,my,mz):: f, fp
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = fp + f
    return
  end if
!
  b = -1./16.
  a = .5-b

!$omp parallel private(i,j,k)

!
  do k=izs,ize
   do j=1,my
    fp(mx-1,j,k) = fp(mx-1,j,k) + ( &
                   a*(f(mx-2,j,k)+f(mx-1,j,k)) &
                 + b*(f(mx-3,j,k)+f(mx  ,j,k)))
    fp(mx  ,j,k) = fp(mx  ,j,k) + ( &
                   a*(f(mx-1,j,k)+f(mx  ,j,k)) &
                 + b*(f(mx-2,j,k)+h(1   ,j,k)))
    fp(1  ,j,k) = fp(1  ,j,k) + ( &
                   a*(g(3   ,j,k)+f(1   ,j,k)) &
                 + b*(g(2   ,j,k)+f(2   ,j,k)))
    fp(2  ,j,k) = fp(2  ,j,k) + ( &
                   a*(f(1   ,j,k)+f(2   ,j,k)) &
                 + b*(g(3   ,j,k)+f(3   ,j,k)))
    fp(3  ,j,k) = fp(3  ,j,k) + ( &
                   a*(f(2   ,j,k)+f(3   ,j,k)) &
                 + b*(f(1   ,j,k)+f(4   ,j,k)))
   end do
   do j=1,my
    do i=4,mx-2
      fp(i,j,k) = fp(i,j,k) + ( &
                   a*(f(i   ,j,k)+f(i-1 ,j,k)) &
                 + b*(f(i+1 ,j,k)+f(i-2 ,j,k)))
    end do
   end do
  end do
!$omp end parallel
!
!
  nstag = nstag+1
  nflop = nflop+8
END SUBROUTINE

!***********************************************************************
SUBROUTINE xdn1_add (f, fp)
!
!  f is centered on (i,j,k), first order stagger
!
  use params, g=>gx1
  implicit none
  logical omp_in_parallel
  real a, b, c
  integer i, j, k
  real, dimension(mx,my,mz):: f, fp
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = fp + f
    return
  end if
!
  a = 0.5

!$omp parallel private(i,j,k)

!
  do k=izs,ize
   do j=1,my
    fp(1  ,j,k) = fp(1  ,j,k) + ( &
                   a*(g(1,j,k)+f(1   ,j,k)))
   end do
   do j=1,my
    do i=2,mx
      fp(i,j,k) = fp(i,j,k) + ( &
                   a*(f(i,j,k)+f(i-1,j,k)))
    end do
   end do
  end do
!$omp end parallel
!
  nstag1 = nstag1+1
  nflop = nflop+2
END SUBROUTINE

!***********************************************************************
SUBROUTINE xup32_add (f,j, fp)
!
!  f is centered on (i-.5,j,k), fifth order stagger
!
  use params, g=>gx2, h=>hx3
  implicit none
  logical omp_in_parallel
  real a, b, c
  integer i, j, k
  real, dimension(mx,my,mz):: f
  real, dimension(mx,mz):: fp
!-----------------------------------------------------------------------
!
!
  if (mx.le.5) then
    fp = fp + f(:,j,:)
    return
  end if
  b = -1./16.
  a = .5-b
!
!$omp parallel private(i,j,k)


  do k=izs,ize
    fp(mx-2,k) = fp(mx-2,k) + ( &
                   a*(f(mx-2,j,k)+f(mx-1,j,k)) &
                 + b*(f(mx-3,j,k)+f(mx  ,j,k)))
    fp(mx-1,k) = fp(mx-1,k) + ( &
                   a*(f(mx-1,j,k)+f(mx  ,j,k)) &
                 + b*(f(mx-2,j,k)+h(1   ,j,k)))
    fp(mx  ,k) = fp(mx  ,k) + ( &
                   a*(f(mx  ,j,k)+h(1   ,j,k)) &
                 + b*(f(mx-1,j,k)+h(2   ,j,k)))
    fp(1   ,k) = fp(1   ,k) + ( &
                   a*(f(1   ,j,k)+f(2   ,j,k)) &
                 + b*(g(2   ,j,k)+f(3   ,j,k)))
    fp(2   ,k) = fp(2   ,k) + ( &
                   a*(f(2   ,j,k)+f(3   ,j,k)) &
                 + b*(f(1   ,j,k)+f(4   ,j,k)))
    do i=3,mx-3
      fp(i  ,k) = fp(i  ,k) + ( &
                    a*(f(i  ,j,k)+f(i+1 ,j,k)) &
                  + b*(f(i-1,j,k)+f(i+2 ,j,k)))
    end do
  end do
!$omp end parallel
!
  if (omp_master) then
      nstag = nstag+1
      nflop = nflop+8
  end if
END SUBROUTINE

!***********************************************************************
SUBROUTINE xup132_add (f,j, fp)
!
!  f is centered on (i-.5,j,k), fifth order stagger
!
  use params, h=>hx1
  implicit none
  logical omp_in_parallel
  real a, b, c
  integer i, j, k
  real, dimension(mx,my,mz):: f
  real, dimension(mx,mz):: fp
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = fp + f(:,j,:)
    return
  end if
!
  a = .5
!
!$omp parallel private(i,j,k)


  do k=izs,ize
    fp(mx  ,k) = fp(mx  ,k) + ( &
                   a*(f(mx  ,j,k)+h(1   ,j,k)))
    do i=1,mx-1
      fp(i  ,k) = fp(i  ,k) + ( &
                    a*(f(i  ,j,k)+f(i+1 ,j,k)))
    end do
  end do
!$omp end parallel
!
  if (omp_master) then
    nstag1 = nstag1+1
    nflop = nflop+2
  end if
END SUBROUTINE

!***********************************************************************
SUBROUTINE xdn32_add (f,j, fp)
!
!  f is centered on (i,j,k), fifth order stagger
!
  use params, g=>gx3, h=>hx2
  implicit none
  logical omp_in_parallel
  real a, b, c
  integer i, j, k
  real, dimension(mx,my,mz):: f
  real, dimension(mx,mz):: fp
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = fp + f(:,j,:)
    return
  end if
!
  b = -1./16.
  a = .5-b
!
!$omp parallel private(i,j,k)


  do k=izs,ize
    fp(mx-1,k) = fp(mx-1,k) + ( &
                   a*(f(mx-2,j,k)+f(mx-1,j,k)) &
                 + b*(f(mx-3,j,k)+f(mx  ,j,k)))
    fp(mx  ,k) = fp(mx  ,k) + ( &
                   a*(f(mx-1,j,k)+f(mx  ,j,k)) &
                 + b*(f(mx-2,j,k)+h(1   ,j,k)))
    fp(1  ,k) = fp(1  ,k) + ( &
                   a*(g(3   ,j,k)+f(1   ,j,k)) &
                 + b*(g(2   ,j,k)+f(2   ,j,k)))
    fp(2  ,k) = fp(2  ,k) + ( &
                   a*(f(1   ,j,k)+f(2   ,j,k)) &
                 + b*(g(3   ,j,k)+f(3   ,j,k)))
    fp(3  ,k) = fp(3  ,k) + ( &
                   a*(f(2   ,j,k)+f(3   ,j,k)) &
                 + b*(f(1   ,j,k)+f(4   ,j,k)))
    do i=4,mx-2
      fp(i,k) = fp(i,k) + ( &
                   a*(f(i   ,j,k)+f(i-1 ,j,k)) &
                 + b*(f(i+1 ,j,k)+f(i-2 ,j,k)))
    end do
  end do
!$omp end parallel
!
!
  if (omp_master) then
    nstag = nstag+1
    nflop = nflop+8
  end if
END SUBROUTINE

!***********************************************************************
SUBROUTINE xdn132_add (f,j, fp)
!
!  f is centered on (i,j,k), fifth order stagger
!
  use params, g=>gx1
  implicit none
  logical omp_in_parallel
  real a, b, c
  integer i, j, k
  real, dimension(mx,my,mz):: f
  real, dimension(mx,mz):: fp
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = fp + f(:,j,:)
    return
  end if
!
  a = 0.5
!
!$omp parallel private(i,j,k)


  do k=izs,ize
    fp(1  ,k) = fp(1  ,k) + ( &
                   a*(g(1   ,j,k)+f(1   ,j,k)))
    do i=2,mx
      fp(i,k) = fp(i,k) + ( &
                   a*(f(i   ,j,k)+f(i-1 ,j,k)))
    end do
  end do
!$omp end parallel
!
  if (omp_master) then
    nstag1 = nstag1+1
    nflop = nflop+2
  end if
END SUBROUTINE

!***********************************************************************
SUBROUTINE xup22_add (f, fp)
!
!  f is centered on (i-.5,k), fifth order stagger
!
  use params, g=>gx2, h=> hx3
  implicit none
  logical omp_in_parallel
  real a, b, c
  integer i, j, k
  real, dimension(mx,mz):: fp, f
!-----------------------------------------------------------------------
!
!
  if (mx.le.5) then
    fp = fp + f
    return
  end if
  b = -1./16.
  a = .5-b
!
!$omp parallel private(i,k)


  do k=izs,ize
    fp(mx-2,k) = fp(mx-2,k) + ( &
                   a*(f(mx-2,k)+f(mx-1,k)) &
                 + b*(f(mx-3,k)+f(mx  ,k)))
    fp(mx-1,k) = fp(mx-1,k) + ( &
                   a*(f(mx-1,k)+f(mx  ,k)) &
                 + b*(f(mx-2,k)+h(1 ,1,k)))
    fp(mx  ,k) = fp(mx  ,k) + ( &
                   a*(f(mx  ,k)+h(1 ,1,k)) &
                 + b*(f(mx-1,k)+h(2 ,1,k)))
    fp(1   ,k) = fp(1   ,k) + ( &
                   a*(f(1   ,k)+f(2   ,k)) &
                 + b*(g(2 ,1,k)+f(3   ,k)))
    fp(2   ,k) = fp(2   ,k) + ( &
                   a*(f(2   ,k)+f(3   ,k)) &
                 + b*(f(1   ,k)+f(4   ,k)))
    do i=3,mx-3
      fp(i  ,k) = fp(i  ,k) + ( &
                    a*(f(i  ,k)+f(i+1 ,k)) &
                  + b*(f(i-1,k)+f(i+2 ,k)))
    end do
  end do
!$omp end parallel
!
  if (omp_master) then
      nstag = nstag+1
      nflop = nflop+8
  end if
END SUBROUTINE

!***********************************************************************
SUBROUTINE xup122_add (f, fp)
!
!  f is centered on (i-.5,k), fifth order stagger
!
  use params, h=> hx1
  implicit none
  logical omp_in_parallel
  real a, b, c
  integer i, j, k
  real, dimension(mx,mz):: fp, f
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = fp + f
    return
  end if
!
  a = .5
!
!$omp parallel private(i,k)


  do k=izs,ize
    fp(mx  ,k) = fp(mx  ,k) + ( &
                   a*(f(mx  ,k)+h(1 ,1,k)))
    do i=1,mx-1
      fp(i  ,k) = fp(i  ,k) + ( &
                    a*(f(i  ,k)+f(i+1 ,k)))
    end do
  end do
!$omp end parallel
!
  if (omp_master) then
    nstag1 = nstag1+1
    nflop = nflop+2
  end if
END SUBROUTINE

!***********************************************************************
SUBROUTINE xdn22_add (f, fp)
!
!  f is centered on (i,k), fifth order stagger
!
  use params, g=>gx3, h=>hx2
  implicit none
  logical omp_in_parallel
  real a, b, c
  integer i, j, k
  real, dimension(mx,mz):: fp, f
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = fp + f
    return
  end if
!
  b = -1./16.
  a = .5-b
!
!$omp parallel private(i,k)


  do k=izs,ize
    fp(mx-1,k) = fp(mx-1,k) + ( &
                   a*(f(mx-2,k)+f(mx-1,k)) &
                 + b*(f(mx-3,k)+f(mx  ,k)))
    fp(mx  ,k) = fp(mx  ,k) + ( &
                   a*(f(mx-1,k)+f(mx  ,k)) &
                 + b*(f(mx-2,k)+h(1 ,1,k)))
    fp(1  ,k) = fp(1  ,k) + ( &
                   a*(g(3 ,1,k)+f(1   ,k)) &
                 + b*(g(2 ,1,k)+f(2   ,k)))
    fp(2  ,k) = fp(2  ,k) + ( &
                   a*(f(1   ,k)+f(2   ,k)) &
                 + b*(g(3 ,1,k)+f(3   ,k)))
    fp(3  ,k) = fp(3  ,k) + ( &
                   a*(f(2   ,k)+f(3   ,k)) &
                 + b*(f(1   ,k)+f(4   ,k)))
    do i=4,mx-2
      fp(i,k) = fp(i,k) + ( &
                   a*(f(i   ,k)+f(i-1 ,k)) &
                 + b*(f(i+1 ,k)+f(i-2 ,k)))
    end do
  end do
!$omp end parallel
!
!
  if (omp_master) then
    nstag = nstag+1
    nflop = nflop+8
  end if
END SUBROUTINE

!***********************************************************************
SUBROUTINE xdn122_add (f, fp)
!
!  f is centered on (i,k), fifth order stagger
!
  use params, g=>gx1
  implicit none
  logical omp_in_parallel
  real a, b, c
  integer i, j, k
  real, dimension(mx,mz):: fp, f
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = fp + f
    return
  end if
!
  a = 0.5
!
!$omp parallel private(i,k)


  do k=izs,ize
    fp(1  ,k) = fp(1  ,k) + ( &
                   a*(g(1 ,1,k)+f(1   ,k)))
    do i=2,mx
      fp(i,k) = fp(i,k) + ( &
                   a*(f(i   ,k)+f(i-1 ,k)))
    end do
  end do
!$omp end parallel
!
  if (omp_master) then
    nstag1 = nstag1+1
    nflop = nflop+2
  end if
END SUBROUTINE
!***********************************************************************
SUBROUTINE ddxupi_add (f, fp)
  use params, g=>gx2, h=>hx3
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f, fp
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = fp + 0.
    return
  end if
!
  b = -1./24.
  a = (1.-3.*b)
  a1st = 1.

!
!$omp parallel private(i,j,k)
  do k=izs,ize
   do j=1,my
    fp(mx-2,j,k) = fp(mx-2,j,k) + ( &
                 + b*(f(mx  ,j,k)-f(mx-3,j,k)) &
                 + a*(f(mx-1,j,k)-f(mx-2,j,k)))
    fp(mx-1,j,k) = fp(mx-1,j,k) + ( &
                 + b*(h(1   ,j,k)-f(mx-2,j,k)) &
                 + a*(f(mx  ,j,k)-f(mx-1,j,k)))
    fp(mx  ,j,k) = fp(mx  ,j,k) + ( &
                 + b*(h(2   ,j,k)-f(mx-1,j,k)) &
                 + a*(h(1   ,j,k)-f(mx  ,j,k)))
    fp(1  ,j,k) = fp(1  ,j,k) + ( &
                 + b*(f(3   ,j,k)-g(2   ,j,k)) &
                 + a*(f(2   ,j,k)-f(1   ,j,k)))
    fp(2  ,j,k) = fp(2  ,j,k) + ( &
                 + b*(f(4   ,j,k)-f(1   ,j,k)) &
                 + a*(f(3   ,j,k)-f(2   ,j,k)))
   end do
   do j=1,my
    do i=3,mx-3
      fp(i  ,j,k) = fp(i  ,j,k) + ( &
                 + b*(f(i+2 ,j,k)-f(i-1 ,j,k)) &
                 + a*(f(i+1 ,j,k)-f(i   ,j,k)))
    end do
  end do
  end do
!$omp end parallel
!
  nstag = nstag+1
  nflop = nflop+8
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddxupi1_add (f, fp)
  use params, g=>gx1
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f, fp
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = fp + 0.
    return
  end if
  a = 1.

!$omp parallel private(i,j,k)

!
  do k=izs,ize
   do j=1,my
    fp(mx  ,j,k) = fp(mx  ,j,k) + ( &
                   a*(g(1  ,j,k)-f(mx,j,k)))
   end do
   do j=1,my
    do i=1,mx-1
      fp(i  ,j,k) = fp(i  ,j,k) + ( &
                   a*(f(i+1,j,k)-f(i ,j,k)))
    end do
  end do
  end do
!$omp end parallel
!
  nstag1 = nstag1+1
  nflop = nflop+2
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddxdni_add (f, fp)
  use params, g=>gx3, h=>hx2
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f, fp
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = fp + 0.
    return
  end if
!
  b = -1./24.
  a = (1.-3.*b)
  a1st = 1.

!$omp parallel private(i,j,k)

!
  do k=izs,ize
   do j=1,my
    fp(mx-1,j,k) = fp(mx-1,j,k) + ( &
                 + b*(f(mx  ,j,k)-f(mx-3,j,k)) &
                 + a*(f(mx-1,j,k)-f(mx-2,j,k)))
    fp(mx  ,j,k) = fp(mx  ,j,k) + ( &
                 + b*(h(1   ,j,k)-f(mx-2,j,k)) &
                 + a*(f(mx  ,j,k)-f(mx-1,j,k)))
    fp(1   ,j,k) = fp(1   ,j,k) + ( &
                 + b*(f(2   ,j,k)-g(2   ,j,k)) &
                 + a*(f(1   ,j,k)-g(3   ,j,k)))
    fp(2  ,j,k) = fp(2  ,j,k) + ( &
                 + b*(f(3   ,j,k)-g(3   ,j,k)) &
                 + a*(f(2   ,j,k)-f(1   ,j,k)))
    fp(3  ,j,k) = fp(3  ,j,k) + ( &
                 + b*(f(4   ,j,k)-f(1   ,j,k)) &
                 + a*(f(3   ,j,k)-f(2   ,j,k)))
   end do
   do j=1,my
    do i=4,mx-2
      fp(i,j,k) = fp(i,j,k) + ( &
                 + b*(f(i+1 ,j,k)-f(i-2 ,j,k)) &
                 + a*(f(i   ,j,k)-f(i-1 ,j,k)))
    end do
   end do
  end do
!$omp end parallel
!
  nstag = nstag+1
  nflop = nflop+8
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddxdni1_add (f, fp)
  use params, g=>gx1
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f, fp
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = fp + 0.
    return
  end if
  a = 1.

!$omp parallel private(i,j,k)

!
  do k=izs,ize
   do j=1,my
    fp(1   ,j,k) = fp(1   ,j,k) + ( &
                   a*(f(1,j,k)-g(1  ,j,k)))
   end do
   do j=1,my
    do i=2,mx
      fp(i,j,k) = fp(i,j,k) + ( &
                   a*(f(i,j,k)-f(i-1,j,k)))
    end do
   end do
  end do
!$omp end parallel
!
  nstag1 = nstag1+1
  nflop = nflop+2
END SUBROUTINE
!***********************************************************************
SUBROUTINE ddxup_add (f, fp)
  use params, h=>hx3, g=>gx2
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f, fp
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = fp + 0.
    return
  end if
!
  b = -1./24.
  a = (1.-3.*b)/dx
  a1st = 1./dx
  b = b/dx

!$omp parallel private(i,j,k)
!
  do k=izs,ize
   do j=1,my
    fp(mx-2,j,k) = fp(mx-2,j,k) + ( &
                 + b*(f(mx  ,j,k)-f(mx-3,j,k)) &
                 + a*(f(mx-1,j,k)-f(mx-2,j,k)))
    fp(mx-1,j,k) = fp(mx-1,j,k) + ( &
                 + b*(h(1   ,j,k)-f(mx-2,j,k)) &
                 + a*(f(mx  ,j,k)-f(mx-1,j,k)))
    fp(mx  ,j,k) = fp(mx  ,j,k) + ( &
                 + b*(h(2   ,j,k)-f(mx-1,j,k)) &
                 + a*(h(1   ,j,k)-f(mx  ,j,k)))
    fp(1  ,j,k) = fp(1  ,j,k) + ( &
                 + b*(f(3   ,j,k)-g(2   ,j,k)) &
                 + a*(f(2   ,j,k)-f(1   ,j,k)))
    fp(2  ,j,k) = fp(2  ,j,k) + ( &
                 + b*(f(4   ,j,k)-f(1   ,j,k)) &
                 + a*(f(3   ,j,k)-f(2   ,j,k)))
   end do
   do j=1,my
    do i=3,mx-3
      fp(i  ,j,k) = fp(i  ,j,k) + ( &
                 + b*(f(i+2 ,j,k)-f(i-1 ,j,k)) &
                 + a*(f(i+1 ,j,k)-f(i   ,j,k)))
    end do
  end do
  end do
!$omp end parallel
!
  nstag = nstag+1
  nflop = nflop+8
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddxup1_add (f, fp)
  use params, g=>gx1
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f, fp
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = fp + 0.
    return
  end if
  a = 1./dx

!$omp parallel private(i,j,k)
!
  do k=izs,ize
   do j=1,my
    fp(mx  ,j,k) = fp(mx  ,j,k) + ( &
                   a*(g(1  ,j,k)-f(mx,j,k)))
   end do
   do j=1,my
    do i=1,mx-1
      fp(i  ,j,k) = fp(i  ,j,k) + ( &
                   a*(f(i+1,j,k)-f(i ,j,k)))
    end do
  end do
  end do
!$omp end parallel
!
  nstag1 = nstag1+1
  nflop = nflop+2
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddxdn_add (f, fp)
  use params, g=>gx3, h=>hx2
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f, fp
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = fp + 0.
    return
  end if
!
  b = -1./24.
  a = (1.-3.*b)/dx
  a1st = 1./dx
  b = b/dx

!$omp parallel private(i,j,k)
!
  do k=izs,ize
   do j=1,my
    fp(mx-1,j,k) = fp(mx-1,j,k) + ( &
                 + b*(f(mx  ,j,k)-f(mx-3,j,k)) &
                 + a*(f(mx-1,j,k)-f(mx-2,j,k)))
    fp(mx  ,j,k) = fp(mx  ,j,k) + ( &
                 + b*(h(1   ,j,k)-f(mx-2,j,k)) &
                 + a*(f(mx  ,j,k)-f(mx-1,j,k)))
    fp(1   ,j,k) = fp(1   ,j,k) + ( &
                 + b*(f(2   ,j,k)-g(2   ,j,k)) &
                 + a*(f(1   ,j,k)-g(3   ,j,k)))
    fp(2  ,j,k) = fp(2  ,j,k) + ( &
                 + b*(f(3   ,j,k)-g(3   ,j,k)) &
                 + a*(f(2   ,j,k)-f(1   ,j,k)))
    fp(3  ,j,k) = fp(3  ,j,k) + ( &
                 + b*(f(4   ,j,k)-f(1   ,j,k)) &
                 + a*(f(3   ,j,k)-f(2   ,j,k)))
   end do
   do j=1,my
    do i=4,mx-2
      fp(i,j,k) = fp(i,j,k) + ( &
                 + b*(f(i+1 ,j,k)-f(i-2 ,j,k)) &
                 + a*(f(i   ,j,k)-f(i-1 ,j,k)))
    end do
   end do
  end do
!$omp end parallel
!
  nstag = nstag+1
  nflop = nflop+8
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddxdn1_add (f, fp)
  use params, g=>gx1
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f, fp
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = fp + 0.
    return
  end if
  a = 1./dx

!$omp parallel private(i,j,k)
!
  do k=izs,ize
   do j=1,my
    fp(1   ,j,k) = fp(1   ,j,k) + ( &
                   a*(f(1,j,k)-g(1  ,j,k)))
   end do
   do j=1,my
    do i=2,mx
      fp(i,j,k) = fp(i,j,k) + ( &
                   a*(f(i,j,k)-f(i-1,j,k)))
    end do
   end do
  end do
!$omp end parallel
!
  nstag1 = nstag1+1
  nflop = nflop+2
END SUBROUTINE
!***********************************************************************
SUBROUTINE yup_add (f, fp)
!
!  f is centered on (i,j-.5,k), fifth order stagger
!
  use params, g=>gy2, h=>gy3
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k
  real, intent(in) , dimension(mx,my,mz):: f
  real,              dimension(mx,my,mz):: fp
!-----------------------------------------------------------------------

  if (my .lt. 5) then
    !$omp parallel private(k)
    do k=izs,ize
      fp(:,:,k) = fp(:,:,k) + f(:,:,k)
    end do
    !$omp end parallel
    return
  end if
  b = -1./16.
  a = .5-b
  a1st = 0.5


  !$omp parallel private(i,j,k)
  do k=izs,ize
    do j=3,my-3
     do i=1,mx
      fp(i,j  ,k) = fp(i,j  ,k) + ( &
                     a*(f(i,j   ,k)+f(i,j+1 ,k)) + &
                     b*(f(i,j-1 ,k)+f(i,j+2 ,k)))
     end do
    end do
    do i=1,mx
      fp(i,my-2,k) = fp(i,my-2,k) + ( &
                     a*(f(i,my-2,k)+f(i,my-1,k)) + &
                     b*(f(i,my-3,k)+f(i,my  ,k)))
      fp(i,my-1,k) = fp(i,my-1,k) + ( &
                     a*(f(i,my-1,k)+f(i,my  ,k)) + &
                     b*(f(i,my-2,k)+h(i,1   ,k)))
      fp(i,my  ,k) = fp(i,my  ,k) + ( &
                     a*(f(i,my  ,k)+h(i,1   ,k)) + &
                     b*(f(i,my-1,k)+h(i,2   ,k)))
      fp(i,1   ,k) = fp(i,1   ,k) + ( &
                     a*(f(i,1   ,k)+f(i,2   ,k)) + &
                     b*(g(i,2   ,k)+f(i,3   ,k)))
      fp(i,2   ,k) = fp(i,2   ,k) + ( &
                     a*(f(i,2   ,k)+f(i,3   ,k)) + &
                     b*(f(i,1   ,k)+f(i,4   ,k)))
    end do
  end do
  !$omp end parallel

  !$omp master
  nstag = nstag+1
  nflop = nflop+8
  !$omp end master
END SUBROUTINE

!***********************************************************************
SUBROUTINE yup1_add (f, fp)
!
!  f is centered on (i,j-.5,k), fifth order stagger
!
  use params, h=>gy1
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k
  real, intent(in) , dimension(mx,my,mz):: f
  real,              dimension(mx,my,mz):: fp
!-----------------------------------------------------------------------
!
  if (my .lt. 5) then
    !$omp parallel private(k)
    do k=izs,ize
      fp(:,:,k) = fp(:,:,k) + f(:,:,k)
    end do
    !$omp end parallel
    return
  end if

  a = 0.5


  !$omp parallel private(i,j,k)
  do k=izs,ize
    do j=1,my-1
      do i=1,mx
        fp(i,j  ,k) = fp(i,j  ,k) + ( &
                     a*(f(i,j   ,k)+f(i,j+1 ,k)))
      end do
    end do
    do i=1,mx
      fp(i,my,k) = fp(i,my,k) + ( &
                     a*(h(i,1,k)+f(i,my,k)))
    end do
  end do
  !$omp end parallel

  !$omp master
  nstag1 = nstag1+1
  nflop = nflop+2
  !$omp end master
END SUBROUTINE

!***********************************************************************
SUBROUTINE ydn_add (f, fp)
!
!  f is centered on (i,j,k), fifth order stagger
!
  use params, g=>gy3, h=>gy2
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f, fp
!-----------------------------------------------------------------------

  if (my .lt. 5) then
    !$omp parallel private(k)
    do k=izs,ize
      fp(:,:,k) = fp(:,:,k) + f(:,:,k)
    end do
    !$omp end parallel
    return
  end if

  b = -1./16.
  a = .5-b
  a1st = 0.5


  !$omp parallel private(i,j,k)
  do k=izs,ize
    do j=4,my-2
     do i=1,mx
      fp(i,j,k) = fp(i,j,k) + ( &
                     a*(f(i,j   ,k)+f(i,j-1 ,k)) + &
                     b*(f(i,j+1 ,k)+f(i,j-2 ,k)))
     end do
    end do
    do i=1,mx
      fp(i,my-1,k) = fp(i,my-1,k) + ( &
                     a*(f(i,my-2,k)+f(i,my-1,k)) + &
                     b*(f(i,my-3,k)+f(i,my  ,k)))
      fp(i,my  ,k) = fp(i,my  ,k) + ( &
                     a*(f(i,my-1,k)+f(i,my  ,k)) + &
                     b*(f(i,my-2,k)+h(i,1   ,k)))
      fp(i,1   ,k) = fp(i,1   ,k) + ( &
                     a*(g(i,3   ,k)+f(i,1   ,k)) + &
                     b*(g(i,2   ,k)+f(i,2   ,k)))
      fp(i,2   ,k) = fp(i,2   ,k) + ( &
                     a*(f(i,1   ,k)+f(i,2   ,k)) + &
                     b*(g(i,3   ,k)+f(i,3   ,k)))
      fp(i,3   ,k) = fp(i,3   ,k) + ( &
                     a*(f(i,2   ,k)+f(i,3   ,k)) + &
                     b*(f(i,1   ,k)+f(i,4   ,k)))
    end do
  end do
  !$omp end parallel

  !$omp master
  nstag = nstag+1
  nflop = nflop+8
  !$omp end master
END SUBROUTINE

!***********************************************************************
SUBROUTINE ydn1_add (f, fp)
!
!  f is centered on (i,j,k), fifth order stagger
!
  use params, g=>gy1
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f, fp
!-----------------------------------------------------------------------
  if (my .lt. 5) then
    !$omp parallel private(k)
    do k=izs,ize
      fp(:,:,k) = fp(:,:,k) + f(:,:,k)
    end do
    !$omp end parallel
    return
  end if

  a = 0.5


  !$omp parallel private(i,j,k)
  do k=izs,ize
    do j=2,my
     do i=1,mx
      fp(i,j,k) = fp(i,j,k) + ( &
                     a*(f(i,j   ,k)+f(i,j-1 ,k)))
     end do
    end do
    do i=1,mx
      fp(i,1   ,k) = fp(i,1   ,k) + ( &
                     a*(g(i,1   ,k)+f(i,1   ,k)))
    end do
  end do
  !$omp end parallel

  !$omp master
  nstag1 = nstag1+1
  nflop = nflop+2
  !$omp end master
END SUBROUTINE
!***********************************************************************
SUBROUTINE ddyupi_add (f, fp)
  use params, g=>gy2, h=>gy3
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f, fp
!-----------------------------------------------------------------------
!
  if (my .lt. 3) then
    fp = fp + 0.
    return
  end if
  b = -1./24.
  a = (1.-3.*b)
  a1st = 1.


  do k=izs,ize
    do i=1,mx
      fp(i,my-2,k) = fp(i,my-2,k) + ( &
                     a*(f(i,my-1,k)-f(i,my-2,k)) + &
                     b*(f(i,my  ,k)-f(i,my-3,k)))
      fp(i,my-1,k) = fp(i,my-1,k) + ( &
                     a*(f(i,my  ,k)-f(i,my-1,k)) + &
                     b*(h(i,1   ,k)-f(i,my-2,k)))
      fp(i,my  ,k) = fp(i,my  ,k) + ( &
                     a*(h(i,1   ,k)-f(i,my  ,k)) + &
                     b*(h(i,2   ,k)-f(i,my-1,k)))
      fp(i,1   ,k) = fp(i,1   ,k) + ( &
                     a*(f(i,2   ,k)-f(i,1   ,k)) + &
                     b*(f(i,3   ,k)-g(i,2   ,k)))
      fp(i,2   ,k) = fp(i,2   ,k) + ( &
                     a*(f(i,3   ,k)-f(i,2   ,k)) + &
                     b*(f(i,4   ,k)-f(i,1   ,k)))
    end do
    do j=3,my-3
     do i=1,mx
      fp(i,j  ,k) = fp(i,j  ,k) + ( &
                     a*(f(i,j+1 ,k)-f(i,j   ,k)) + &
                     b*(f(i,j+2 ,k)-f(i,j-1 ,k)))
     end do
    end do
  end do

  !$omp master
  nstag = nstag+1
  nflop = nflop+8
  !$omp end master
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddyupi1_add (f, fp)
  use params, g=>gy1
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f, fp
!-----------------------------------------------------------------------
!
  if (my .lt. 3) then
    fp = fp + 0.
    return
  end if
  a = 1.


  do k=izs,ize
    do i=1,mx
      fp(i,my,k) = fp(i,my,k) + ( &
                     a*(g(i,1  ,k)-f(i,my,k)))
    end do
    do j=1,my-1
      do i=1,mx
        fp(i,j  ,k) = fp(i,j  ,k) + ( &
                     a*(f(i,j+1 ,k)-f(i,j   ,k)))
     end do
    end do
  end do

  !$omp master
  nstag1 = nstag1+1
  nflop = nflop+2
  !$omp end master
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddydni_add (f, fp)
  use params, g=>gy3, h=>gy2
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f, fp
!-----------------------------------------------------------------------
!
  if (my .lt. 3) then
    fp = fp + 0.
    return
  end if
  b = -1./24.
  a = (1.-3.*b)
  a1st = 1.


  do k=izs,ize
    do i=1,mx
      fp(i,my-1,k) = fp(i,my-1,k) + ( &
                     a*(f(i,my-1,k)-f(i,my-2,k)) + &
                     b*(f(i,my  ,k)-f(i,my-3,k)))
      fp(i,my  ,k) = fp(i,my  ,k) + ( &
                     a*(f(i,my  ,k)-f(i,my-1,k)) + &
                     b*(h(i,1   ,k)-f(i,my-2,k)))
      fp(i,1  ,k) = fp(i,1  ,k) + ( &
                     a*(f(i,1   ,k)-g(i,3   ,k)) + &
                     b*(f(i,2   ,k)-g(i,2   ,k)))
      fp(i,2   ,k) = fp(i,2   ,k) + ( &
                     a*(f(i,2   ,k)-f(i,1   ,k)) + &
                     b*(f(i,3   ,k)-g(i,3   ,k)))
      fp(i,3   ,k) = fp(i,3   ,k) + ( &
                     a*(f(i,3   ,k)-f(i,2   ,k)) + &
                     b*(f(i,4   ,k)-f(i,1   ,k)))
    end do
    do j=4,my-2
     do i=1,mx
      fp(i,j ,k) = fp(i,j ,k) + ( &
                     a*(f(i,j   ,k)-f(i,j-1 ,k)) + &
                     b*(f(i,j+1 ,k)-f(i,j-2 ,k)))
     end do
    end do
  end do

  !$omp master
  nstag = nstag+1
  nflop = nflop+8
  !$omp end master
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddydni1_add (f, fp)
  use params, g=>gy1
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f, fp
!-----------------------------------------------------------------------
!
  if (my .lt. 3) then
    fp = fp + 0.
    return
  end if
  a = 1.


  do k=izs,ize
    do i=1,mx
      fp(i,1  ,k) = fp(i,1  ,k) + ( &
                     a*(f(i,1   ,k)-g(i,1   ,k)))
    end do
    do j=2,my
     do i=1,mx
      fp(i,j ,k) = fp(i,j ,k) + ( &
                     a*(f(i,j   ,k)-f(i,j-1 ,k)))
     end do
    end do
  end do

  !$omp master
  nstag1 = nstag1+1
  nflop = nflop+2
  !$omp end master
END SUBROUTINE
!***********************************************************************
SUBROUTINE ddyup_add (f, fp)
  use params, g=>gy2, h=>gy3
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f, fp
!-----------------------------------------------------------------------

  if (my .lt. 3) then
    fp = fp + 0.
    return
  end if
  b = -1./24.
  a = (1.-3.*b)/dy
  a1st = 1./dy
  b = b/dy


  !$omp parallel private(i,j,k)
  do k=izs,ize
    do i=1,mx
      fp(i,my-2,k) = fp(i,my-2,k) + ( &
                     b*(f(i,my  ,k)-f(i,my-3,k)) + &
                     a*(f(i,my-1,k)-f(i,my-2,k)) )
      fp(i,my-1,k) = fp(i,my-1,k) + ( &
                     b*(h(i,1   ,k)-f(i,my-2,k)) + &
                     a*(f(i,my  ,k)-f(i,my-1,k)) )
      fp(i,my  ,k) = fp(i,my  ,k) + ( &
                     b*(h(i,2   ,k)-f(i,my-1,k)) + &
                     a*(h(i,1   ,k)-f(i,my  ,k)) )
      fp(i,1   ,k) = fp(i,1   ,k) + ( &
                     b*(f(i,3   ,k)-g(i,2   ,k)) + &
                     a*(f(i,2   ,k)-f(i,1   ,k)) )
      fp(i,2   ,k) = fp(i,2   ,k) + ( &
                     b*(f(i,4   ,k)-f(i,1   ,k)) + &
                     a*(f(i,3   ,k)-f(i,2   ,k)) )
    end do
    do j=3,my-3
     do i=1,mx
      fp(i,j  ,k) = fp(i,j  ,k) + ( &
                     b*(f(i,j+2 ,k)-f(i,j-1 ,k)) + &
                     a*(f(i,j+1 ,k)-f(i,j   ,k)) )
     end do
    end do
  end do
  !$omp end parallel

  !$omp master
  nstag = nstag+1
  nflop = nflop+8
  !$omp end master
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddyup1_add (f, fp)
  use params, g=>gy1
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f, fp
!-----------------------------------------------------------------------

  if (my .lt. 3) then
    fp = fp + 0.
    return
  end if
  a = 1./dy


  !$omp parallel private(i,j,k)
  do k=izs,ize
    do i=1,mx
      fp(i,my,k) = fp(i,my,k) + ( &
                     a*(g(i,1  ,k)-f(i,my,k)))
    end do
    do j=1,my-1
      do i=1,mx
        fp(i,j  ,k) = fp(i,j  ,k) + ( &
                     a*(f(i,j+1 ,k)-f(i,j   ,k)))
     end do
    end do
  end do
  !$omp end parallel

  !$omp master
  nstag1 = nstag1+1
  nflop = nflop+2
  !$omp end master
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddydn_add (f, fp)
  use params, g=>gy3, h=>gy2
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f, fp
!-----------------------------------------------------------------------

  if (my .lt. 3) then
    fp = fp + 0.
    return
  end if
  b = -1./24.
  a = (1.-3.*b)/dy
  a1st = 1./dy
  b = b/dy


  !$omp parallel private(i,j,k)
  do k=izs,ize
    do i=1,mx
      fp(i,my-1,k) = fp(i,my-1,k) + ( &
                     b*(f(i,my  ,k)-f(i,my-3,k)) + &
                     a*(f(i,my-1,k)-f(i,my-2,k)) ) 
      fp(i,my  ,k) = fp(i,my  ,k) + ( &
                     b*(h(i,1   ,k)-f(i,my-2,k)) + &
                     a*(f(i,my  ,k)-f(i,my-1,k)) )
      fp(i,1  ,k) = fp(i,1  ,k) + ( &
                     b*(f(i,2   ,k)-g(i,2   ,k)) + &
                     a*(f(i,1   ,k)-g(i,3   ,k)) )
      fp(i,2   ,k) = fp(i,2   ,k) + ( &
                     b*(f(i,3   ,k)-g(i,3   ,k)) + &
                     a*(f(i,2   ,k)-f(i,1   ,k)) )
      fp(i,3   ,k) = fp(i,3   ,k) + ( &
                     b*(f(i,4   ,k)-f(i,1   ,k)) + &
                     a*(f(i,3   ,k)-f(i,2   ,k)) )
    end do
    do j=4,my-2
     do i=1,mx
      fp(i,j ,k) = fp(i,j ,k) + ( &
                     b*(f(i,j+1 ,k)-f(i,j-2 ,k)) + &
                     a*(f(i,j   ,k)-f(i,j-1 ,k)) )
     end do
    end do
  end do
  !$omp end parallel

  !$omp master
  nstag = nstag+1
  nflop = nflop+8
  !$omp end master
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddydn1_add (f, fp)
  use params, g=>gy1
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f, fp
!-----------------------------------------------------------------------

  if (my .lt. 3) then
    fp = fp + 0.
    return
  end if
  a = 1./dy


  !$omp parallel private(i,j,k)
  do k=izs,ize
    do i=1,mx
      fp(i,1  ,k) = fp(i,1  ,k) + ( &
                     a*(f(i,1   ,k)-g(i,1   ,k)))
    end do
    do j=2,my
     do i=1,mx
      fp(i,j ,k) = fp(i,j ,k) + ( &
                     a*(f(i,j   ,k)-f(i,j-1 ,k)))
     end do
    end do
  end do
  !$omp end parallel

  !$omp master
  nstag1 = nstag1+1
  nflop = nflop+2
  !$omp end master
END SUBROUTINE
!***********************************************************************
SUBROUTINE zup_add (f, fp)
!
!  f is centered on (i,j,k-.5), fifth order stagger
!
  use params
  implicit none
  logical omp_in_parallel
  real a, b, c
  integer i, j, k
  real, intent(in) , dimension(mx,my,mz):: f
  real,              dimension(mx,my,mz):: fp
  real,              dimension(mx,my, 2):: g
  real,              dimension(mx,my, 3):: h
!-----------------------------------------------------------------------
!

  if (mz .lt. 5) then
    !$omp parallel private(k)
    do k=izs,ize
      fp(:,:,k) = fp(:,:,k) + f(:,:,k)
    end do
    !$omp end parallel
    return
  end if
  b = -1./16.
  a = .5-b

!
  !$omp parallel private(i,j,k)
  do k=max(3,izs),min(mz-3,ize)
    do j=1,my
      do i=1,mx
        fp(i,j,k) = fp(i,j,k) + ( &
                     a*(f(i,j,k   )+f(i,j,k+1 )) + &
                     b*(f(i,j,k-1 )+f(i,j,k+2 )))
     end do
    end do
  end do
  if (ize.eq.mz) then
   do j=1,my
    do i=1,mx
      fp(i,j,mz-2) = fp(i,j,mz-2) + ( &
                     a*(f(i,j,mz-2)+f(i,j,mz-1)) + &
                     b*(f(i,j,mz-3)+f(i,j,mz  )))
      fp(i,j,mz-1) = fp(i,j,mz-1) + ( &
                     a*(f(i,j,mz-1)+f(i,j,mz  )) + &
                     b*(f(i,j,mz-2)+h(i,j,1   )))
      fp(i,j,mz  ) = fp(i,j,mz  ) + ( &
                     a*(f(i,j,mz  )+h(i,j,1   )) + &
                     b*(f(i,j,mz-1)+h(i,j,2   )))
    end do
   end do
  end if
  if (izs.eq.1) then
   do j=1,my
    do i=1,mx
      fp(i,j,1   ) = fp(i,j,1   ) + ( &
                     a*(f(i,j,1   )+f(i,j,2   )) + &
                     b*(g(i,j,2   )+f(i,j,3   )))
      fp(i,j,2   ) = fp(i,j,2   ) + ( &
                     a*(f(i,j,2   )+f(i,j,3   )) + &
                     b*(f(i,j,1   )+f(i,j,4   )))
    end do
   end do
  end if
  !$omp end parallel
!
  nstag = nstag+1
  nflop = nflop+8
END SUBROUTINE

!***********************************************************************
SUBROUTINE zup1_add (f, fp)
!
!  f is centered on (i,j,k-.5), fifth order stagger
!
  use params
  implicit none
  logical omp_in_parallel
  real a, b, c
  integer i, j, k
  real, intent(in) , dimension(mx,my,mz):: f
  real,              dimension(mx,my,mz):: fp
  real,              dimension(mx,my,1 ):: h
!-----------------------------------------------------------------------
!
  if (mz .lt. 5) then
    !$omp parallel private(k)
    do k=izs,ize
      fp(:,:,k) = fp(:,:,k) + f(:,:,k)
    end do
    !$omp end parallel
    return
  end if
!
  a = 0.5
!
  !$omp parallel private(i,j,k)
  do k=izs,min(mz-1,ize)
    do j=1,my
      do i=1,mx
        fp(i,j,k  ) = fp(i,j,k  ) + ( &
                     a*(f(i,j,k   )+f(i,j,k+1 )))
      end do
    end do
  end do
  if (ize.eq.mz) then
   do j=1,my
    do i=1,mx
      fp(i,j,mz) = fp(i,j,mz) + ( &
                     a*(h(i,j,1)+f(i,j,mz)))
    end do
   end do
  end if
  !$omp end parallel
!
  nstag1 = nstag1+1
  nflop = nflop+2
END SUBROUTINE

!***********************************************************************
SUBROUTINE zdn_add (f, fp)
!
!  f is centered on (i,j), fifth order stagger
!
  use params
  implicit none
  logical omp_in_parallel
  real a, b, c
  integer i, j, k
  real, dimension(mx,my,mz):: f, fp
  real, dimension(mx,my, 3):: g
  real, dimension(mx,my, 2):: h
!-----------------------------------------------------------------------
!
  if (mz .lt. 5) then
    !$omp parallel private(k)
    do k=izs,ize
      fp(:,:,k) = fp(:,:,k) + f(:,:,k)
    end do
    !$omp end parallel
    return
  end if
  b = -1./16.
  a = .5-b

!
  !$omp parallel private(i,j,k)
  do k=max(4,izs),min(mz-2,ize)
    do j=1,my
     do i=1,mx
      fp(i,j,k) = fp(i,j,k) + ( &
                     a*(f(i,j,k   )+f(i,j,k-1 )) + &
                     b*(f(i,j,k+1 )+f(i,j,k-2 )))
     end do
    end do
  end do
  if (ize.eq.mz) then
   do j=1,my
    do i=1,mx
      fp(i,j,mz-1) = fp(i,j,mz-1) + ( &
                     a*(f(i,j,mz-2)+f(i,j,mz-1)) + &
                     b*(f(i,j,mz-3)+f(i,j,mz  )))
      fp(i,j,mz  ) = fp(i,j,mz  ) + ( &
                     a*(f(i,j,mz-1)+f(i,j,mz  )) + &
                     b*(f(i,j,mz-2)+h(i,j,1   )))
    end do
   end do
  end if
  if (izs.eq.1) then
   do j=1,my
    do i=1,mx
      fp(i,j,1   ) = fp(i,j,1   ) + ( &
                     a*(g(i,j,3   )+f(i,j,1   )) + &
                     b*(g(i,j,2   )+f(i,j,2   )))
      fp(i,j,2   ) = fp(i,j,2   ) + ( &
                     a*(f(i,j,1   )+f(i,j,2   )) + &
                     b*(g(i,j,3   )+f(i,j,3   )))
      fp(i,j,3   ) = fp(i,j,3   ) + ( &
                     a*(f(i,j,2   )+f(i,j,3   )) + &
                     b*(f(i,j,1   )+f(i,j,4   )))
    end do
   end do
  end if
  !$omp end parallel
!
  nstag = nstag+1
  nflop = nflop+8
END SUBROUTINE

!***********************************************************************
SUBROUTINE zdn1_add (f, fp)
!
!  f is centered on (i,j), fifth order stagger
!
  use params
  implicit none
  logical omp_in_parallel
  real a, b, c
  integer i, j, k
  real, dimension(mx,my,mz):: f, fp
  real, dimension(mx,my,1 ):: g
!-----------------------------------------------------------------------
!
  if (mz .lt. 5) then
    !$omp parallel private(k)
    do k=izs,ize
      fp(:,:,k) = fp(:,:,k) + f(:,:,k)
    end do
    !$omp end parallel
    return
  end if
!
  a = 0.5
!
  !$omp parallel private(i,j,k)
  do k=max(2,izs),ize
    do j=1,my
     do i=1,mx
      fp(i,j,k) = fp(i,j,k) + ( &
                     a*(f(i,j,k   )+f(i,j,k-1 )))
     end do
    end do
  end do
  if (izs.eq.1) then
   do j=1,my
    do i=1,mx
      fp(i,j,1   ) = fp(i,j,1   ) + ( &
                     a*(g(i,j,1   )+f(i,j,1   )))
    end do
   end do
  end if
  !$omp end parallel
!
  nstag1 = nstag1+1
  nflop = nflop+2
END SUBROUTINE

!***********************************************************************
SUBROUTINE zup32_add (f,j, fp)
!
!  f is centered on (i,j,k-.5), fifth order stagger
!
  use params
  implicit none
  logical omp_in_parallel
  real a, b, c
  integer i, j, k
  real,             dimension(mx,   mz):: fp
  real, intent(in), dimension(mx,my,mz):: f
  real,             dimension(mx,my, 2):: g
  real,             dimension(mx,my, 3):: h
!-----------------------------------------------------------------------
!
!
  if (mz.le.5) then
    !$omp parallel private(k)
    do k=izs,ize
      fp(:,k) = fp(:,k) + f(:,j,k)
    end do
    !$omp end parallel
    return
  end if

  b = -1./16.
  a = .5-b

!
!$omp parallel private(i,j,k)
  do k=max(3,izs),min(mz-3,ize)
      do i=1,mx
        fp(i,k) = fp(i,k) + ( &
                     a*(f(i,j,k+1)+f(i,j,k  )) + &
                     b*(f(i,j,k+2)+f(i,j,k-1)))
      end do
  end do
  if (izs.eq.1) then
    do i=1,mx
      fp(i,1   ) = fp(i,1   ) + ( &
                     a*(f(i,j,1   )+f(i,j,2   )) + &
                     b*(g(i,j,2   )+f(i,j,3   )))
      fp(i,2   ) = fp(i,2   ) + ( &
                     a*(f(i,j,2   )+f(i,j,3   )) + &
                     b*(f(i,j,1   )+f(i,j,4   )))
    end do
  end if
  if (ize.eq.mz) then
    do i=1,mx
      fp(i,mz-2) = fp(i,mz-2) + ( &
                     a*(f(i,j,mz-2)+f(i,j,mz-1)) + &
                     b*(f(i,j,mz-3)+f(i,j,mz  )))
      fp(i,mz-1) = fp(i,mz-1) + ( &
                     a*(f(i,j,mz-1)+f(i,j,mz  )) + &
                     b*(f(i,j,mz-2)+h(i,j,1   )))
      fp(i,mz  ) = fp(i,mz  ) + ( &
                     a*(f(i,j,mz  )+h(i,j,1   )) + &
                     b*(f(i,j,mz-1)+h(i,j,2   )))
    end do
  end if

!$omp end parallel
!
  if (omp_master) then
    nstag = nstag+1
    nflop = nflop+8
  end if
END SUBROUTINE

!***********************************************************************
SUBROUTINE zup132_add (f,j, fp)
!
!  f is centered on (i,j,k-.5), fifth order stagger
!
  use params
  implicit none
  logical omp_in_parallel
  real a, b, c
  integer i, j, k
  real,              dimension(mx,   mz):: fp
  real, intent(in),  dimension(mx,my,mz):: f
  real,              dimension(mx,my,1 ):: h
!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    !$omp parallel private(k)
    do k=izs,ize
      fp(:,k) = fp(:,k) + f(:,j,k)
    end do
    !$omp end parallel
    return
  end if

  a = .5
!
!$omp parallel private(i,j,k)
  do k=izs,min(mz-1,ize)
      do i=1,mx
        fp(i,k) = fp(i,k) + ( &
                     a*(f(i,j,k+1)+f(i,j,k  )))
      end do
  end do
  if (ize.eq.mz) then
    do i=1,mx
      fp(i,mz) = fp(i,mz) + ( &
                     a*(h(i,j,1)+f(i,j,mz)))
    end do
  end if

!$omp end parallel
!
  if (omp_master) then
    nstag1 = nstag1+1
    nflop = nflop+2
  end if
END SUBROUTINE

!***********************************************************************
SUBROUTINE zdn32_add (f,j, fp)
!
!  f is centered on (i,j,k), fifth order stagger
!
  use params
  implicit none
  logical omp_in_parallel
  real a, b, c
  integer i, j, k
  real,             dimension(mx,   mz):: fp
  real, intent(in), dimension(mx,my,mz):: f
  real,             dimension(mx,my, 3):: g
  real,             dimension(mx,my, 2):: h

!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    !$omp parallel private(k)
    do k=izs,ize
      fp(:,k) = fp(:,k) + f(:,j,k)
    end do
    !$omp end parallel
    return
  end if

  b = -1./16.
  a = .5-b

!
!$omp parallel private(i,j,k)
  do k=max(4,izs),min(mz-2,ize)
      do i=1,mx
        fp(i,k) = fp(i,k) + ( &
                     a*(f(i,j,k  )+f(i,j,k-1)) + &
                     b*(f(i,j,k+1)+f(i,j,k-2)))
      end do
  end do
  if (izs.eq.1) then
    do i=1,mx
      fp(i,1   ) = fp(i,1   ) + ( &
                     a*(g(i,j,3   )+f(i,j,1   )) + &
                     b*(g(i,j,2   )+f(i,j,2   )))
      fp(i,2   ) = fp(i,2   ) + ( &
                     a*(f(i,j,1   )+f(i,j,2   )) + &
                     b*(g(i,j,3   )+f(i,j,3   )))
      fp(i,3   ) = fp(i,3   ) + ( &
                     a*(f(i,j,2   )+f(i,j,3   )) + &
                     b*(f(i,j,1   )+f(i,j,4   )))
    end do
  end if
  if (ize.eq.mz) then
    do i=1,mx
      fp(i,mz-1) = fp(i,mz-1) + ( &
                     a*(f(i,j,mz-2)+f(i,j,mz-1)) + &
                     b*(f(i,j,mz-3)+f(i,j,mz  )))
      fp(i,mz  ) = fp(i,mz  ) + ( &
                     a*(f(i,j,mz-1)+f(i,j,mz  )) + &
                     b*(f(i,j,mz-2)+h(i,j,1   )))
    end do
  end if

!$omp end parallel
!
  if (omp_master) then
    nstag = nstag+1
    nflop = nflop+8
  end if
END SUBROUTINE

!***********************************************************************
SUBROUTINE zdn132_add (f,j, fp)
!
!  f is centered on (i,j,k), fifth order stagger
!
  use params
  implicit none
  logical omp_in_parallel
  real a, b, c
  integer i, j, k
  real,             dimension(mx,   mz):: fp
  real, intent(in), dimension(mx,my,mz):: f
  real,             dimension(mx,my,1 ):: g
!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    !$omp parallel private(k)
    do k=izs,ize
      fp(:,k) = fp(:,k) + f(:,j,k)
    end do
    !$omp end parallel
    return
  end if

  a = .5
!
!$omp parallel private(i,j,k)
  do k=max(2,izs),ize
      do i=1,mx
        fp(i,k) = fp(i,k) + ( &
                     a*(f(i,j,k  )+f(i,j,k-1)))
      end do
  end do
  if (izs.eq.1) then
    do i=1,mx
      fp(i,1   ) = fp(i,1   ) + ( &
                     a*(g(i,j,1   )+f(i,j,1   )))
    end do
  end if

!$omp end parallel
!
  if (omp_master) then
    nstag1 = nstag1+1
    nflop = nflop+2
  end if
END SUBROUTINE

!***********************************************************************
SUBROUTINE zup22_add (f, fp)
!
!  f is centered on (i,k-.5), fifth order stagger
!
  use params
  implicit none
  logical omp_in_parallel
  real a, b, c
  integer i, j, k
  real,             dimension(mx,  mz):: fp
  real, intent(in), dimension(mx,  mz):: f
  real,             dimension(mx,1, 2):: g
  real,             dimension(mx,1, 3):: h

!-----------------------------------------------------------------------
!
!
  if (mz.le.5) then
    !$omp parallel private(k)
    do k=izs,ize
      fp(:,k) = fp(:,k) + f(:,k)
    end do
    !$omp end parallel
    return
  end if
  b = -1./16.
  a = .5-b

!
!$omp parallel private(i,k)
  do k=max(3,izs),min(mz-3,ize)
      do i=1,mx
        fp(i,k) = fp(i,k) + ( &
                     a*(f(i,k+1)+f(i,k  )) + &
                     b*(f(i,k+2)+f(i,k-1)))
      end do
  end do
  if (izs.eq.1) then
    do i=1,mx
      fp(i,1   ) = fp(i,1   ) + ( &
                     a*(f(i,1   )+f(i,2   )) + &
                     b*(g(i,1,2   )+f(i,3   )))
      fp(i,2   ) = fp(i,2   ) + ( &
                     a*(f(i,2   )+f(i,3   )) + &
                     b*(f(i,1   )+f(i,4   )))
    end do
  end if
  if (ize.eq.mz) then
    do i=1,mx
      fp(i,mz-2) = fp(i,mz-2) + ( &
                     a*(f(i,mz-2)+f(i,mz-1)) + &
                     b*(f(i,mz-3)+f(i,mz  )))
      fp(i,mz-1) = fp(i,mz-1) + ( &
                     a*(f(i,mz-1)+f(i,mz  )) + &
                     b*(f(i,mz-2)+h(i,1,1   )))
      fp(i,mz  ) = fp(i,mz  ) + ( &
                     a*(f(i,mz  )+h(i,1,1   )) + &
                     b*(f(i,mz-1)+h(i,1,2   )))
    end do
  end if

!$omp end parallel
!
  if (omp_master) then
    nstag = nstag+1
    nflop = nflop+8
  end if
END SUBROUTINE

!***********************************************************************
SUBROUTINE zup122_add (f, fp)
!
!  f is centered on (i,k-.5), fifth order stagger
!
  use params
  implicit none
  logical omp_in_parallel
  real a, b, c
  integer i, j, k
  real,             dimension(mx,   mz):: fp
  real, intent(in), dimension(mx,   mz):: f
  real,             dimension(mx,my,1 ):: h
!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    !$omp parallel private(k)
    do k=izs,ize
      fp(:,k) = fp(:,k) + f(:,k)
    end do
    !$omp end parallel
    return
  end if

  a = .5
!
!$omp parallel private(i,k)
  do k=izs,ize
      do i=1,mx
        fp(i,k) = fp(i,k) + ( &
                     a*(f(i,k+1)+f(i,k  )))
      end do
  end do
  if (ize.eq.mz) then
    do i=1,mx
      fp(i,mz) = fp(i,mz) + ( &
                     a*(h(i,1,1)+f(i,mz)))
    end do
  end if

!$omp end parallel
!
  if (omp_master) then
    nstag1 = nstag1+1
    nflop = nflop+2
  end if
END SUBROUTINE

!***********************************************************************
SUBROUTINE zdn22_add (f, fp)
!
!  f is centered on (i,k), fifth order stagger
!
  use params
  implicit none
  logical omp_in_parallel
  real a, b, c
  integer i, j, k
  real,             dimension(mx,   mz):: fp
  real, intent(in), dimension(mx,   mz):: f
  real,             dimension(mx,1, 3 ):: g
  real,             dimension(mx,1, 2 ):: h

!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    !$omp parallel private(k)
    do k=izs,ize
      fp(:,k) = fp(:,k) + f(:,k)
    end do
    !$omp end parallel
    return
  end if
  b = -1./16.
  a = .5-b
!
!$omp parallel private(i,k)
  do k=max(4,izs),min(mz-2,ize)
      do i=1,mx
        fp(i,k) = fp(i,k) + ( &
                     a*(f(i,k  )+f(i,k-1)) + &
                     b*(f(i,k+1)+f(i,k-2)))
      end do
  end do
  if (izs.eq.1) then
    do i=1,mx
      fp(i,1   ) = fp(i,1   ) + ( &
                     a*(g(i,1,3 )+f(i,1   )) + &
                     b*(g(i,1,2 )+f(i,2   )))
      fp(i,2   ) = fp(i,2   ) + ( &
                     a*(f(i,1   )+f(i,2   )) + &
                     b*(g(i,1,3 )+f(i,3   )))
      fp(i,3   ) = fp(i,3   ) + ( &
                     a*(f(i,2   )+f(i,3   )) + &
                     b*(f(i,1   )+f(i,4   )))
    end do
  end if
  if (ize.eq.mz) then
    do i=1,mx
      fp(i,mz-1) = fp(i,mz-1) + ( &
                     a*(f(i,mz-2)+f(i,mz-1)) + &
                     b*(f(i,mz-3)+f(i,mz  )))
      fp(i,mz  ) = fp(i,mz  ) + ( &
                     a*(f(i,mz-1)+f(i,mz  )) + &
                     b*(f(i,mz-2)+h(i,1,1   )))
    end do
  end if

!$omp end parallel
!
  if (omp_master) then
    nstag = nstag+1
    nflop = nflop+8
  end if
END SUBROUTINE

!***********************************************************************
SUBROUTINE zdn122_add (f, fp)
!
!  f is centered on (i,k), fifth order stagger
!
  use params
  implicit none
  logical omp_in_parallel
  real a, b, c
  integer i, j, k
  real,             dimension(mx,   mz):: fp
  real, intent(in), dimension(mx,   mz):: f
  real,             dimension(mx,1 ,1 ):: g
!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    !$omp parallel private(k)
    do k=izs,ize
      fp(:,k) = fp(:,k) + f(:,k)
    end do
    !$omp end parallel
    return
  end if

  a = .5
!
!$omp parallel private(i,k)
  do k=max(2,izs),ize
      do i=1,mx
        fp(i,k) = fp(i,k) + ( &
                     a*(f(i,k  )+f(i,k-1)))
      end do
  end do
  if (izs.eq.1) then
    do i=1,mx
      fp(i,1   ) = fp(i,1   ) + ( &
                     a*(g(i,1,1  )+f(i,1  )))
    end do
  end if
 
!$omp end parallel
!
  if (omp_master) then
    nstag1 = nstag1+1
    nflop = nflop+2
  end if
END SUBROUTINE
!***********************************************************************
SUBROUTINE ddzupi_add (f, fp)
!
!  f is centered on (i,j,k-.5), fifth order stagger
!
  use params
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k
  real, intent(in) , dimension(mx,my,mz):: f
  real,              dimension(mx,my,mz):: fp
  real,              dimension(mx,my, 2):: g
  real,              dimension(mx,my, 3):: h
!-----------------------------------------------------------------------
!

  if (mz .lt. 5) then
    !$omp parallel private(k)
    do k=izs,ize
      fp(:,:,k) = fp(:,:,k) + f(:,:,k)
    end do
    !$omp end parallel
    return
  end if

  b = -1./24.
  a = (1.-3.*b)
  a1st = 1

!
  !$omp parallel private(i,j,k)
  do k=max(izs,3),min(ize,mz-3)
    do j=1,my
      do i=1,mx
        fp(i,j,k) = fp(i,j,k) + ( &
                     a*(-f(i,j,k   )+f(i,j,k+1 )) + &
                     b*(-f(i,j,k-1 )+f(i,j,k+2 )))
     end do
    end do
  end do
  !$omp end parallel
  do j=1,my
    do i=1,mx
      fp(i,j,mz-2) = fp(i,j,mz-2) + ( &
                     a*(-f(i,j,mz-2)+f(i,j,mz-1)) + &
                     b*(-f(i,j,mz-3)+f(i,j,mz  )))
      fp(i,j,mz-1) = fp(i,j,mz-1) + ( &
                     a*(-f(i,j,mz-1)+f(i,j,mz  )) + &
                     b*(-f(i,j,mz-2)+h(i,j,1   )))
      fp(i,j,mz  ) = fp(i,j,mz  ) + ( &
                     a*(-f(i,j,mz  )+h(i,j,1   )) + &
                     b*(-f(i,j,mz-1)+h(i,j,2   )))
      fp(i,j,1   ) = fp(i,j,1   ) + ( &
                     a*(-f(i,j,1   )+f(i,j,2   )) + &
                     b*(-g(i,j,1   )+f(i,j,3   )))
      fp(i,j,2   ) = fp(i,j,2   ) + ( &
                     a*(-f(i,j,2   )+f(i,j,3   )) + &
                     b*(-f(i,j,1   )+f(i,j,4   )))
    end do
  end do
!
  nstag = nstag+1
  nflop = nflop+8
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddzupi1_add (f, fp)
!
!  f is centered on (i,j,k-.5), fifth order stagger
!
  use params
  implicit none
  logical omp_in_parallel
  integer i, j, k
  real, intent(in) , dimension(mx,my,mz):: f
  real,              dimension(mx,my,mz):: fp
  real,              dimension(mx,my, 1):: g
!-----------------------------------------------------------------------
!
  if (mz .lt. 5) then
    !$omp parallel private(k)
    do k=izs,ize
      fp(:,:,k) = fp(:,:,k) + f(:,:,k)
    end do
    !$omp end parallel
    return
  end if
!
!
  !$omp parallel private(i,j,k)
  do k=izs,min(ize,mz-1)
    do j=1,my
      do i=1,mx
        fp(i,j,k  ) = fp(i,j,k  ) + ( &
                     (f(i,j,k+1)-f(i,j,k)))
      end do
    end do
  end do
  !$omp end parallel
  do j=1,my
    do i=1,mx
      fp(i,j,mz) = fp(i,j,mz) + ( &
                     (+g(i,j,1)-f(i,j,mz)))
    end do
  end do
!
  nstag1 = nstag1+1
  nflop = nflop+2
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddzdni_add (f, fp)
!
!  f is centered on (i,j), fifth order stagger
!
  use params
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f, fp
  real, dimension(mx,my, 3):: g
  real, dimension(mx,my, 2):: h
!-----------------------------------------------------------------------
!
  if (mz .lt. 5) then
    !$omp parallel private(k)
    do k=izs,ize
      fp(:,:,k) = fp(:,:,k) + f(:,:,k)
    end do
    !$omp end parallel
    return
  end if

  b = -1./24.
  a = (1.-3.*b)
  a1st = 1.

!
  !$omp parallel private(i,j,k)
  do k=max(izs,4),min(ize,mz-2)
    do j=1,my
     do i=1,mx
      fp(i,j,k) = fp(i,j,k) + ( &
                     a*(f(i,j,k   )-f(i,j,k-1)) + &
                     b*(f(i,j,k+1 )-f(i,j,k-2)))
     end do
    end do
  end do
  !$omp end parallel
  do j=1,my
    do i=1,mx
      fp(i,j,mz-1) = fp(i,j,mz-1) + ( &
                     a*(-f(i,j,mz-2)+f(i,j,mz-1)) + &
                     b*(-f(i,j,mz-3)+f(i,j,mz  )))
      fp(i,j,mz  ) = fp(i,j,mz  ) + ( &
                     a*(-f(i,j,mz-1)+f(i,j,mz  )) + &
                     b*(-f(i,j,mz-2)+h(i,j,1   )))
      fp(i,j,1   ) = fp(i,j,1   ) + ( &
                     a*(-g(i,j,3   )+f(i,j,1   )) + &
                     b*(-g(i,j,2   )+f(i,j,2   )))
      fp(i,j,2   ) = fp(i,j,2   ) + ( &
                     a*(-f(i,j,1   )+f(i,j,2   )) + &
                     b*(-g(i,j,3   )+f(i,j,3   )))
      fp(i,j,3   ) = fp(i,j,3   ) + ( &
                     a*(-f(i,j,2   )+f(i,j,3   )) + &
                     b*(-f(i,j,1   )+f(i,j,4   )))
    end do
  end do
!
  nstag = nstag+1
  nflop = nflop+8
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddzdni1_add (f, fp)
!
!  f is centered on (i,j), fifth order stagger
!
  use params
  implicit none
  logical omp_in_parallel
  integer i, j, k
  real, dimension(mx,my,mz):: f, fp
  real, dimension(mx,my, 1):: g
!-----------------------------------------------------------------------
!
  if (mz .lt. 5) then
    !$omp parallel private(k)
    do k=izs,ize
      fp(:,:,k) = fp(:,:,k) + f(:,:,k)
    end do
    !$omp end parallel
    return
  end if
!
!
  !$omp parallel private(i,j,k)
  do k=max(izs,2),ize
    do j=1,my
     do i=1,mx
      fp(i,j,k) = fp(i,j,k) + ( &
                     (f(i,j,k)-f(i,j,k-1)))
     end do
    end do
  end do
  !$omp end parallel
  do j=1,my
    do i=1,mx
      fp(i,j,1   ) = fp(i,j,1   ) + ( &
                     (f(i,j,1)-g(i,j,1)))
    end do
  end do
!
  nstag1 = nstag1+1
  nflop = nflop+2
END SUBROUTINE
!***********************************************************************
SUBROUTINE ddzup_add (f, fp)
!
!  f is centered on (i,j,k-.5), fifth order stagger
!
  use params
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k
  real, intent(in) , dimension(mx,my,mz):: f
  real,              dimension(mx,my,mz):: fp
  real,              dimension(mx,my, 2):: g
  real,              dimension(mx,my, 3):: h
!-----------------------------------------------------------------------
!

  if (mz .lt. 5) then
    !$omp parallel private(k)
    do k=izs,ize
      fp(:,:,k) = fp(:,:,k) + 0.
    end do
    !$omp end parallel
    return
  end if

  b = -1./24.
  a = (1.-3.*b)/dz
  a1st = 1./dz
  b = b/dz

!
  !$omp parallel private(i,j,k)
  do k=max(3,izs),min(mz-3,ize)
    do j=1,my
      do i=1,mx
        fp(i,j,k) = fp(i,j,k) + ( &
                     b*(f(i,j,k+2 )-f(i,j,k-1 )) + &
                     a*(f(i,j,k+1 )-f(i,j,k   )))
     end do
    end do
  end do

  if (ize.eq.mz) then
   do j=1,my
    do i=1,mx
      fp(i,j,mz-2) = fp(i,j,mz-2) + ( &
                     b*(f(i,j,mz  )-f(i,j,mz-3)) + &
                     a*(f(i,j,mz-1)-f(i,j,mz-2)))
      fp(i,j,mz-1) = fp(i,j,mz-1) + ( &
                     b*(h(i,j,1   )-f(i,j,mz-2)) + &
                     a*(f(i,j,mz  )-f(i,j,mz-1)))
      fp(i,j,mz  ) = fp(i,j,mz  ) + ( &
                     b*(h(i,j,2   )-f(i,j,mz-1)) + &
                     a*(h(i,j,1   )-f(i,j,mz  )))
    end do
   end do
  end if
  if (izs.eq.1) then
   do j=1,my
    do i=1,mx
      fp(i,j,1   ) = fp(i,j,1   ) + ( &
                     b*(f(i,j,3   )-g(i,j,2   )) + &
                     a*(f(i,j,2   )-f(i,j,1   )))
      fp(i,j,2   ) = fp(i,j,2   ) + ( &
                     b*(f(i,j,4   )-f(i,j,1   )) + &
                     a*(f(i,j,3   )-f(i,j,2   )))
    end do
   end do
  end if
  !$omp end parallel
!
  nstag = nstag+1
  nflop = nflop+8
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddzup1_add (f, fp)
!
!  f is centered on (i,j,k-.5), fifth order stagger
!
  use params
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k
  real, intent(in) , dimension(mx,my,mz):: f
  real,              dimension(mx,my,mz):: fp
  real,              dimension(mx,my, 1):: g
!-----------------------------------------------------------------------
!
  if (mz .lt. 5) then
    !$omp parallel private(k)
    do k=izs,ize
      fp(:,:,k) = fp(:,:,k) + 0.
    end do
    !$omp end parallel
    return
  end if
!
  a = 1./dz
!
  !$omp parallel private(i,j,k)
  do k=izs,min(mz-1,ize)
    do j=1,my
      do i=1,mx
        fp(i,j,k  ) = fp(i,j,k  ) + ( &
                     a*(f(i,j,k+1)-f(i,j,k)))
      end do
    end do
  end do
  if (ize.eq.mz) then
   do j=1,my
    do i=1,mx
      fp(i,j,mz) = fp(i,j,mz) + ( &
                     a*(g(i,j,1)-f(i,j,mz)))
    end do
   end do
  end if
  !$omp end parallel
!
  nstag1 = nstag1+1
  nflop = nflop+2
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddzdn_add (f, fp)
!
!  f is centered on (i,j), fifth order stagger
!
  use params
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f, fp
  real, dimension(mx,my, 3):: g
  real, dimension(mx,my, 2):: h
!-----------------------------------------------------------------------
!
  if (mz .lt. 5) then
    !$omp parallel private(k)
    do k=izs,ize
      fp(:,:,k) = fp(:,:,k) + 0.
    end do
    !$omp end parallel
    return
  end if

  b = -1./24.
  a = (1.-3.*b)/dz
  a1st = 1./dz
  b = b/dz

!
  !$omp parallel private(i,j,k)
  do k=max(4,izs),min(mz-2,ize)
    do j=1,my
     do i=1,mx
      fp(i,j,k) = fp(i,j,k) + ( &
                     b*(f(i,j,k+1 )-f(i,j,k-2 )) + &
                     a*(f(i,j,k   )-f(i,j,k-1 )))
     end do
    end do
  end do
  if (ize.eq.mz) then
   do j=1,my
    do i=1,mx
      fp(i,j,mz-1) = fp(i,j,mz-1) + ( &
                     b*(f(i,j,mz  )-f(i,j,mz-3)) + &
                     a*(f(i,j,mz-1)-f(i,j,mz-2)))
      fp(i,j,mz  ) = fp(i,j,mz  ) + ( &
                     b*(h(i,j,1   )-f(i,j,mz-2)) + &
                     a*(f(i,j,mz  )-f(i,j,mz-1)))
    end do
   end do
  end if
  if (izs.eq.1) then
   do j=1,my
    do i=1,mx
      fp(i,j,1   ) = fp(i,j,1   ) + ( &
                     b*(f(i,j,2   )-g(i,j,2   )) + &
                     a*(f(i,j,1   )-g(i,j,3   )))
      fp(i,j,2   ) = fp(i,j,2   ) + ( &
                     b*(f(i,j,3   )-g(i,j,3   )) + &
                     a*(f(i,j,2   )-f(i,j,1   )))
      fp(i,j,3   ) = fp(i,j,3   ) + ( &
                     b*(f(i,j,4   )-f(i,j,1   )) + &
                     a*(f(i,j,3   )-f(i,j,2   )))
    end do
   end do
  end if
  !$omp end parallel
!
  nstag = nstag+1
  nflop = nflop+8
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddzdn1_add (f, fp)
!
!  f is centered on (i,j), fifth order stagger
!
  use params
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f, fp
  real, dimension(mx,my, 1):: g
!-----------------------------------------------------------------------
!
  if (mz .lt. 5) then
    !$omp parallel private(k)
    do k=izs,ize
      fp(:,:,k) = fp(:,:,k) + 0.
    end do
    !$omp end parallel
    return
  end if
!
  a = 1./dz
!
  !$omp parallel private(i,j,k)
  do k=max(2,izs),ize
    do j=1,my
     do i=1,mx
      fp(i,j,k) = fp(i,j,k) + ( &
                     a*(f(i,j,k)-f(i,j,k-1)))
     end do
    end do
  end do
  if (izs.eq.1) then
   do j=1,my
    do i=1,mx
      fp(i,j,1   ) = fp(i,j,1   ) + ( &
                     a*(f(i,j,1)-g(i,j,1  )))
    end do
   end do
  end if
  !$omp end parallel
!
  nstag1 = nstag1+1
  nflop = nflop+2
END SUBROUTINE

!***********************************************************************
SUBROUTINE d2abs_sub (f, fp)
!
!  Max abs difference one zone left to one zones right.
!

  USE params
  implicit none
  logical omp_in_parallel
  real cx, cy, cz
  real, dimension(mx,my,mz):: f, fp
  real, dimension(mx,my,1):: gz, hz
!hpf$ align with TheCube:: f, fp
  integer i, j, k, km1, kp1
!+---------------------------------------------------------------------

!$omp parallel private(i,j,k,km1,kp1)

  if (mx.gt.1) then
    do k=izs,ize
      do j=1,my
        do i=2,mx-1
          fp(i,j,k) = fp(i,j,k) - abs(f(i-1 ,j,k)+f(i+1,j,k)-f(i,j,k)-f(i,j,k))
        end do
        fp(1 ,j,k) = fp(1 ,j,k) - abs(gx1(1   ,j,k)+f (2,j,k)-f(1 ,j,k)-f(1 ,j,k))
        fp(mx,j,k) = fp(mx,j,k) - abs(f (mx-1,j,k)+hx1(1,j,k)-f(mx,j,k)-f(mx,j,k))
      end do
    end do
  else
    do k=izs,ize
      do j=1,my
        do i=1,mx
          fp(i,j,k) = fp(i,j,k) - 0.
        end do
      end do
    end do
  end if

 if (my .gt. 1) then
  do k=izs,ize
    do j=2,my-1
      do i=1,mx
        fp(i,j, k) = fp(i,j, k) - max(fp(i,j, k),abs(f(i,j -1,k)+f(i,j+1,k)-f(i,j,k)-f(i,j,k)))
      end do
    end do
    do i=1,mx
        fp(i,1 ,k) = fp(i,1 ,k) - max(fp(i,1 ,k),abs(gy1(i,   1,k)+f (i,2,k)-f(i, 1,k)-f(i, 1,k)))
        fp(i,my,k) = fp(i,my,k) - max(fp(i,my,k),abs(f (i,my-1,k)+hy1(i,1,k)-f(i,my,k)-f(i,my,k)))
    end do
  end do
 end if
 if (mz .gt. 1) then
!$omp barrier
  do k=izs,ize
    if (k.eq.1) then
      do j=1,my
      do i=1,mx
        fp(i,j,k ) = fp(i,j,k ) - max(fp(i,j,k ),abs(gz(i,j,1)+f(i,j,k+1)-f(i,j,k)-f(i,j,k)))
      end do
      end do
    else if (k.eq.mz) then
      do j=1,my
      do i=1,mx
        fp(i,j,k ) = fp(i,j,k ) - max(fp(i,j,k ),abs(hz(i,j,1)+f(i,j,k-1)-f(i,j,k)-f(i,j,k)))
      end do
      end do
    else
      do j=1,my
      do i=1,mx
        fp(i,j,k ) = fp(i,j,k ) - max(fp(i,j,k ),abs(f(i,j,k-1)+f(i,j,k+1)-f(i,j,k)-f(i,j,k)))
      end do
      end do
    end if
  end do
 end if
!$omp end parallel

END SUBROUTINE
!***********************************************************************
SUBROUTINE dif2a_sub (f, fp)
!
!  Max abs difference one zone left to one zones right.
!

  USE params
  implicit none
  logical omp_in_parallel
  real cx, cy, cz
  real, dimension(mx,my,mz):: f, fp
  real, dimension(mx,my,1):: gz, hz
!hpf$ align with TheCube:: f, fp
  integer i, j, k, km1, kp1
!+---------------------------------------------------------------------

!$omp parallel private(i,j,k,km1,kp1)

  if (mx.gt.1) then
    do k=izs,ize
      do j=1,my
        do i=2,mx-1
          fp(i,j,k) = fp(i,j,k) - abs(f(i-1 ,j,k)-f(i+1,j,k))
        end do
        fp(1 ,j,k) = fp(1 ,j,k) - abs(gx1(1   ,j,k)-f (2,j,k))
        fp(mx,j,k) = fp(mx,j,k) - abs(f (mx-1,j,k)-hx1(1,j,k))
      end do
    end do
  else
    do k=izs,ize
      do j=1,my
        do i=1,mx
          fp(i,j,k) = fp(i,j,k) - 0.
        end do
      end do
    end do
  end if

 if (my .gt. 1) then
  do k=izs,ize
    do j=2,my-1
      do i=1,mx
        fp(i,j, k) = fp(i,j, k) - max(fp(i,j, k),abs(f(i,j -1,k)-f(i,j+1,k)))
      end do
    end do
    do i=1,mx
        fp(i,1 ,k) = fp(i,1 ,k) - max(fp(i,1 ,k),abs(gy1(i,   1,k)-f (i,2,k)))
        fp(i,my,k) = fp(i,my,k) - max(fp(i,my,k),abs(f (i,my-1,k)-hy1(i,1,k)))
    end do
  end do
 end if
 if (mz .gt. 1) then
!$omp barrier
  do k=izs,ize
    if (k.eq.1) then
      do j=1,my
      do i=1,mx
        fp(i,j,k ) = fp(i,j,k ) - max(fp(i,j,k ),abs(gz(i,j,1)-f(i,j,k+1)))
      end do
      end do
    else if (k.eq.mz) then
      do j=1,my
      do i=1,mx
        fp(i,j,k ) = fp(i,j,k ) - max(fp(i,j,k ),abs(hz(i,j,1)-f(i,j,k-1)))
      end do
      end do
    else
      do j=1,my
      do i=1,mx
        fp(i,j,k ) = fp(i,j,k ) - max(fp(i,j,k ),abs(f(i,j,k-1)-f(i,j,k+1)))
      end do
      end do
    end if
  end do
 end if
!$omp end parallel

END SUBROUTINE

!***********************************************************************
SUBROUTINE dif2_sub (fx, fy, fz, fp)
!
!  Finite difference one zone left to one zones right.
!

  USE params
  implicit none
  logical omp_in_parallel
  real cx(mx), cy(my), cz(mz)
  real, dimension(mx,my,mz):: fx, fy, fz, fp
  real, dimension(mx,my,1):: gz, hz
  integer i, j, k, km1, kp1
!+---------------------------------------------------------------------


!$omp parallel private(i,j,k,km1,kp1)


  if (mx.gt.1) then
    do k=izs,ize
      do j=1,my
        do i=2,mx-1
          fp(i,j,k) = fp(i,j,k) - cx(i)*(fx(i-1 ,j,k)-fx(i+1,j,k))
        end do
        fp(1 ,j,k) = fp(1 ,j,k) - cx( 1)*(gx1(1   ,j,k)-fx(2  ,j,k))
        fp(mx,j,k) = fp(mx,j,k) - cx(mx)*(fx(mx-1,j,k)-hx1(1  ,j,k))
      end do
    end do
  else
    do k=izs,ize
      do j=1,my
        do i=1,mx
          fp(i,j,k) = fp(i,j,k) - 0.
        end do
      end do
    end do
  end if


  if (my .gt. 1) then
    do k=izs,ize
     do j=2,my-1
       do i=1,mx
       end do
     end do
     do i=1,mx
     end do
    end do
  end if


 if (mz .gt. 1) then
!$omp barrier
  do k=izs,ize
    if (k.eq.1) then
      do j=1,my
      do i=1,mx
      end do
      end do
    else if (k.eq.mz) then
      do j=1,my
      do i=1,mx
      end do
      end do
    else
      do j=1,my
      do i=1,mx
      end do
      end do
    end if
  end do
 end if

 do k=izs,ize
   do j=1,my
     do i=1,mx
       fp(i,j,k) = fp(i,j,k) - max (0., fp(i,j,k))
     end do
   end do
 end do
!$omp end parallel

END SUBROUTINE
!***********************************************************************
SUBROUTINE dif2d_sub (fx, fy, fz, fp)
!
!  Finite difference one zone left to one zones right.
!

  USE params
  implicit none
  logical omp_in_parallel
  real, dimension(mx,my,mz):: fx, fy, fz, fp
  real, dimension(mx,my,1):: gz, hz
  integer i, j, k, km1, kp1
!+---------------------------------------------------------------------


!$omp parallel private(i,j,k,km1,kp1)

  if (mx.gt.1) then
    do k=izs,ize
      do j=1,my
        do i=2,mx-1
          fp(i,j,k) = fp(i,j,k) - (fx(i-1 ,j,k)-fx(i+1,j,k))
        end do
        fp(1 ,j,k) = fp(1 ,j,k) - (gx1(1   ,j,k)-fx(2  ,j,k))
        fp(mx,j,k) = fp(mx,j,k) - (fx(mx-1,j,k)-hx1(1  ,j,k))
      end do
    end do
  else
    do k=izs,ize
      do j=1,my
        do i=1,mx
          fp(i,j,k) = fp(i,j,k) - 0.
        end do
      end do
    end do
  end if


  if (my .gt. 1) then
    do k=izs,ize
     do j=2,my-1
       do i=1,mx
         fp(i,j, k) = fp(i,j, k) - fp(i,j, k) + (fy(i,j -1,k)-fy(i,j+1,k))
       end do
     end do
     do i=1,mx
         fp(i,1 ,k) = fp(i,1 ,k) - fp(i,1 ,k) + (gy1(i,   1,k)-fy(i,  2,k))
         fp(i,my,k) = fp(i,my,k) - fp(i,my,k) + (fy(i,my-1,k)-hy1(i,  1,k))
     end do
    end do
  end if


 if (mz .gt. 1) then
!$omp barrier
  do k=izs,ize
    if (k.eq.1) then
      do j=1,my
      do i=1,mx
        fp(i,j,k ) = fp(i,j,k ) - fp(i,j,k ) + (gz(i,j,  1)-fz(i,j,k+1))
      end do
      end do
    else if (k.eq.mz) then
      do j=1,my
      do i=1,mx
        fp(i,j,k ) = fp(i,j,k ) - fp(i,j,k ) + (fz(i,j,k-1)-hz(i,j,1))
      end do
      end do
    else
      do j=1,my
      do i=1,mx
        fp(i,j,k ) = fp(i,j,k ) - fp(i,j,k ) + (fz(i,j,k-1)-fz(i,j,k+1))
      end do
      end do
    end if
  end do
 end if

 do k=izs,ize
   do j=1,my
     do i=1,mx
       fp(i,j,k) = fp(i,j,k) - max (0., fp(i,j,k))
     end do
   end do
 end do
!$omp end parallel

END SUBROUTINE

!***********************************************************************
SUBROUTINE dif1xy_sub (fx, fy, fp)
!
!  Finite difference curl one zone left to center
!
  USE params
  implicit none
  logical omp_in_parallel
  real, dimension(mx,my,mz):: fx, fy, fp
  real, dimension(mx):: cx
  real, dimension(my):: cy
  integer i, j, k, km1, kp1
!+---------------------------------------------------------------------
!$omp parallel private(i,j,k,km1,kp1)
  if (mx.gt.1) then
    do k=izs,ize
      do j=1,my
        do i=2,mx
          fp(i,j,k) = fp(i,j,k) - cx(i)*(fy(i ,j,k)-fy(i-1,j,k))
        end do
        fp(1,j,k) = fp(1,j,k) - cx(1)*(fy(1,j,k)-gx1(  1,j,k))
      end do
    end do
  else
    do k=izs,ize
      do j=1,my
        do i=1,mx
          fp(i,j,k) = fp(i,j,k) - 0.
        end do
      end do
    end do
  end if
  if (my .gt. 1) then
    do k=izs,ize
     do j=2,my
       do i=1,mx
         fp(i,j, k) = fp(i,j, k) - fp(i, j,k) - cy(j)*(fx(i, j,k)-fx(i,j-1,k))
       end do
     end do
     do i=1,mx
         fp(i,1,k) = fp(i,1,k) - fp(i,1,k) - cy(1)*(fx(i,1,k)-gy1(i,  1,k))
     end do
    end do
  end if
  do k=izs,ize
    do j=1,my
      do i=1,mx
        fp(i,j,k) = fp(i,j,k) - fp(i,j,k)**2
      end do
    end do
  end do
!$omp end parallel
END SUBROUTINE

!***********************************************************************
SUBROUTINE dif1yz_sub (fy, fz, fp)
!
!  Finite difference one zone left to center
!
  USE params
  implicit none
  logical omp_in_parallel
  real, dimension(mx,my,mz):: fy, fz, fp
  real, dimension(mx,my,1):: gz, hz
  real, dimension(my):: cy
  real, dimension(mz):: cz
  integer i, j, k, km1, kp1
!+---------------------------------------------------------------------
!$omp parallel private(i,j,k,km1,kp1)
  if (my .gt. 1) then
    do k=izs,ize
     do j=2,my
       do i=1,mx
         fp(i,j, k) = fp(i,j, k) - cy(j)*(fz(i, j,k)-fz(i,j-1,k))
       end do
     end do
     do i=1,mx
         fp(i,1,k) = fp(i,1,k) - cy(1)*(fz(i,1,k)-gy1(i,  1,k))
     end do
    end do
  end if
  if (mz .gt. 1) then
!$omp barrier
   do k=izs,ize
     if (k.eq.1) then
       do j=1,my
       do i=1,mx
         fp(i,j,k) = fp(i,j,k) - fp(i,j,k) - cz(k)*(fy(i,j,k)-gz(i,j,1))
       end do
       end do
     else
       do j=1,my
       do i=1,mx
         fp(i,j,k) = fp(i,j,k) - fp(i,j,k) - cz(k)*(fy(i,j,k)-fy(i,j,k-1))
       end do
       end do
     end if
   end do
  end if
  do k=izs,ize
    do j=1,my
      do i=1,mx
        fp(i,j,k) = fp(i,j,k) - fp(i,j,k)**2
      end do
    end do
  end do
 !$omp end parallel
END SUBROUTINE

!***********************************************************************
SUBROUTINE dif1zx_sub (fz, fx, fp)
!
!  Finite difference one zone left to center
!
  USE params
  implicit none
  logical omp_in_parallel
  real, dimension(mx,my,mz):: fx, fz, fp
  real, dimension(mx,my,1):: gz, hz
  real, dimension(mx):: cx
  real, dimension(mz):: cz
  integer i, j, k, km1, kp1
!+---------------------------------------------------------------------
!$omp parallel private(i,j,k,km1,kp1)
  if (mx.gt.1) then
    do k=izs,ize
      do j=1,my
        do i=2,mx
          fp(i,j,k) = fp(i,j,k) - cx(i)*(fz(i ,j,k)-fz(i-1,j,k))
        end do
        fp(1,j,k) = fp(1,j,k) - cx(1)*(fz(1,j,k)-gx1(  1,j,k))
      end do
    end do
  else
    do k=izs,ize
      do j=1,my
        do i=1,mx
          fp(i,j,k) = fp(i,j,k) - 0.
        end do
      end do
    end do
  end if
  if (mz .gt. 1) then
!$omp barrier
    do k=izs,ize
      if (k.eq.1) then
        do j=1,my
        do i=1,mx
          fp(i,j,k) = fp(i,j,k) - fp(i,j,k) - cz(k)*(fx(i,j,k)-gz(i,j,1))
        end do
        end do
      else
        do j=1,my
        do i=1,mx
          fp(i,j,k) = fp(i,j,k) - fp(i,j,k) - cz(k)*(fx(i,j,k)-fx(i,j,k-1))
        end do
        end do
      end if
    end do
   end if
   do k=izs,ize
     do j=1,my
       do i=1,mx
         fp(i,j,k) = fp(i,j,k) - fp(i,j,k)**2
       end do
     end do
   end do
!$omp end parallel
END SUBROUTINE

!***********************************************************************
SUBROUTINE dif1_sub (fx, fy, fz, fp)
!
!  Finite difference one zone left to center
!

  USE params
  implicit none
  logical omp_in_parallel
  real, dimension(mx,my,mz):: fx, fy, fz, fp
  real, dimension(mx,my,1):: gz, hz
  real, dimension(mx):: cx
  real, dimension(my):: cy
  real, dimension(mz):: cz
  integer i, j, k, km1, kp1
!+---------------------------------------------------------------------



!$omp parallel private(i,j,k,km1,kp1)


  if (mx.gt.1) then
    do k=izs,ize
      do j=1,my
        do i=1,mx-1
          fp(i,j,k) = fp(i,j,k) - cx(i)*(fx(i ,j,k)-fx(i+1,j,k))
        end do
        fp(mx,j,k) = fp(mx,j,k) - cx(mx)*(fx(mx,j,k)-hx1(  1,j,k))
      end do
    end do
  else
    do k=izs,ize
      do j=1,my
        do i=1,mx
          fp(i,j,k) = fp(i,j,k) - 0.
        end do
      end do
    end do
  end if


  if (my .gt. 1) then
    do k=izs,ize
     do j=1,my-1
       do i=1,mx
       end do
     end do
     do i=1,mx
     end do
    end do
  end if


 if (mz .gt. 1) then
!$omp barrier
  do k=izs,ize
    if (k.eq.mz) then
      do j=1,my
      do i=1,mx
      end do
      end do
    else
      do j=1,my
      do i=1,mx
      end do
      end do
    end if
  end do
 end if

 do k=izs,ize
   do j=1,my
     do i=1,mx
       fp(i,j,k) = fp(i,j,k) - max (0., fp(i,j,k))
     end do
   end do
 end do
!$omp end parallel

END SUBROUTINE

!***********************************************************************
SUBROUTINE dif1d_sub (fx, fy, fz, fp)
!
!  Finite difference one zone left to center
!

  USE params
  implicit none
  logical omp_in_parallel
  real, dimension(mx,my,mz):: fx, fy, fz, fp
  real, dimension(mx,my,1):: gz, hz
  integer i, j, k, km1, kp1
!+---------------------------------------------------------------------


!$omp parallel private(i,j,k,km1,kp1)


  if (mx.gt.1) then
    do k=izs,ize
      do j=1,my
        do i=1,mx-1
          fp(i,j,k) = fp(i,j,k) - (fx(i ,j,k)-fx(i+1,j,k))
        end do
        fp(mx,j,k) = fp(mx,j,k) - (fx(mx,j,k)-hx1(  1,j,k))
      end do
    end do
  else
    do k=izs,ize
      do j=1,my
        do i=1,mx
          fp(i,j,k) = fp(i,j,k) - 0.
        end do
      end do
    end do
  end if


  if (my .gt. 1) then
    do k=izs,ize
     do j=1,my-1
       do i=1,mx
         fp(i,j, k) = fp(i,j, k) - fp(i, j,k) + (fy(i, j,k)-fy(i,j+1,k))
       end do
     end do
     do i=1,mx
         fp(i,my,k) = fp(i,my,k) - fp(i,my,k) + (fy(i,my,k)-hy1(i,  1,k))
     end do
    end do
  end if


 if (mz .gt. 1) then
!$omp barrier
  do k=izs,ize
    if (k.eq.mz) then
      do j=1,my
      do i=1,mx
        fp(i,j,k) = fp(i,j,k) - fp(i,j,k) + (fz(i,j,k)-hz(i,j,1))
      end do
      end do
    else
      do j=1,my
      do i=1,mx
        fp(i,j,k) = fp(i,j,k) - fp(i,j,k) + (fz(i,j,k)-fz(i,j,k+1))
      end do
      end do
    end if
  end do
 end if

 do k=izs,ize
   do j=1,my
     do i=1,mx
       fp(i,j,k) = fp(i,j,k) - max (0., fp(i,j,k))
     end do
   end do
 end do
!$omp end parallel

END SUBROUTINE

!***********************************************************************
SUBROUTINE xup_sub (f, fp)
!
!  f is centered on (i-.5,j,k), fifth order stagger
!
  use params, g=>gx2, h=>hx3
  implicit none
  logical omp_in_parallel
  real a, b, c
  integer i, j, k
  real, dimension(mx,my,mz):: f, fp
!+---------------------------------------------------------------------
!
!
  if (mx.le.5) then
    fp = fp - f
    return
  end if
  b = -1./16.
  a = .5-b

!$omp parallel private(i,j,k)


  do k=izs,ize
   do j=1,my
    fp(mx-2,j,k) = fp(mx-2,j,k) - ( &
                   a*(f(mx-2,j,k)+f(mx-1,j,k)) &
                 + b*(f(mx-3,j,k)+f(mx  ,j,k)))
    fp(mx-1,j,k) = fp(mx-1,j,k) - ( &
                   a*(f(mx-1,j,k)+f(mx  ,j,k)) &
                 + b*(f(mx-2,j,k)+h(1   ,j,k)))
    fp(mx  ,j,k) = fp(mx  ,j,k) - ( &
                   a*(f(mx  ,j,k)+h(1   ,j,k)) &
                 + b*(f(mx-1,j,k)+h(2   ,j,k)))
    fp(1   ,j,k) = fp(1   ,j,k) - ( &
                   a*(f(1   ,j,k)+f(2   ,j,k)) &
                 + b*(g(2   ,j,k)+f(3   ,j,k)))
    fp(2   ,j,k) = fp(2   ,j,k) - ( &
                   a*(f(2   ,j,k)+f(3   ,j,k)) &
                 + b*(f(1   ,j,k)+f(4   ,j,k)))
   end do
   do j=1,my
    do i=3,mx-3
      fp(i  ,j,k) = fp(i  ,j,k) - ( &
                    a*(f(i  ,j,k)+f(i+1 ,j,k)) &
                  + b*(f(i-1,j,k)+f(i+2 ,j,k)))
    end do
   end do
  end do
!$omp end parallel
!
  nstag = nstag+1
  nflop = nflop+8
END SUBROUTINE

!***********************************************************************
SUBROUTINE xup1_sub (f, fp)
!
!  f is centered on (i-.5,j,k), first order stagger
!
  use params, h=>hx1
  implicit none
  logical omp_in_parallel
  real a, b, c
  integer i, j, k
  real, dimension(mx,my,mz):: f, fp
!+---------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = fp - f
    return
  end if
!
  a = .5

!$omp parallel private(i,j,k)

!
  do k=izs,ize
   do j=1,my
    fp(mx  ,j,k) = fp(mx  ,j,k) -  ( &
                    a*(f(mx,j,k)+h(  1,j,k)))
   end do
   do j=1,my
    do i=1,mx-1
      fp(i  ,j,k) = fp(i  ,j,k) - ( &
                    a*(f(i ,j,k)+f(i+1,j,k)))
    end do
   end do
  end do
!$omp end parallel
!
  nstag1 = nstag1+1
  nflop = nflop+2
END SUBROUTINE

!***********************************************************************
SUBROUTINE xdn_sub (f, fp)
!
!  f is centered on (i,j,k), fifth order stagger
!
  use params, g=>gx3, h=>hx2
  implicit none
  logical omp_in_parallel
  real a, b, c
  integer i, j, k
  real, dimension(mx,my,mz):: f, fp
!+---------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = fp - f
    return
  end if
!
  b = -1./16.
  a = .5-b

!$omp parallel private(i,j,k)

!
  do k=izs,ize
   do j=1,my
    fp(mx-1,j,k) = fp(mx-1,j,k) - ( &
                   a*(f(mx-2,j,k)+f(mx-1,j,k)) &
                 + b*(f(mx-3,j,k)+f(mx  ,j,k)))
    fp(mx  ,j,k) = fp(mx  ,j,k) - ( &
                   a*(f(mx-1,j,k)+f(mx  ,j,k)) &
                 + b*(f(mx-2,j,k)+h(1   ,j,k)))
    fp(1  ,j,k) = fp(1  ,j,k) - ( &
                   a*(g(3   ,j,k)+f(1   ,j,k)) &
                 + b*(g(2   ,j,k)+f(2   ,j,k)))
    fp(2  ,j,k) = fp(2  ,j,k) - ( &
                   a*(f(1   ,j,k)+f(2   ,j,k)) &
                 + b*(g(3   ,j,k)+f(3   ,j,k)))
    fp(3  ,j,k) = fp(3  ,j,k) - ( &
                   a*(f(2   ,j,k)+f(3   ,j,k)) &
                 + b*(f(1   ,j,k)+f(4   ,j,k)))
   end do
   do j=1,my
    do i=4,mx-2
      fp(i,j,k) = fp(i,j,k) - ( &
                   a*(f(i   ,j,k)+f(i-1 ,j,k)) &
                 + b*(f(i+1 ,j,k)+f(i-2 ,j,k)))
    end do
   end do
  end do
!$omp end parallel
!
!
  nstag = nstag+1
  nflop = nflop+8
END SUBROUTINE

!***********************************************************************
SUBROUTINE xdn1_sub (f, fp)
!
!  f is centered on (i,j,k), first order stagger
!
  use params, g=>gx1
  implicit none
  logical omp_in_parallel
  real a, b, c
  integer i, j, k
  real, dimension(mx,my,mz):: f, fp
!+---------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = fp - f
    return
  end if
!
  a = 0.5

!$omp parallel private(i,j,k)

!
  do k=izs,ize
   do j=1,my
    fp(1  ,j,k) = fp(1  ,j,k) - ( &
                   a*(g(1,j,k)+f(1   ,j,k)))
   end do
   do j=1,my
    do i=2,mx
      fp(i,j,k) = fp(i,j,k) - ( &
                   a*(f(i,j,k)+f(i-1,j,k)))
    end do
   end do
  end do
!$omp end parallel
!
  nstag1 = nstag1+1
  nflop = nflop+2
END SUBROUTINE

!***********************************************************************
SUBROUTINE xup32_sub (f,j, fp)
!
!  f is centered on (i-.5,j,k), fifth order stagger
!
  use params, g=>gx2, h=>hx3
  implicit none
  logical omp_in_parallel
  real a, b, c
  integer i, j, k
  real, dimension(mx,my,mz):: f
  real, dimension(mx,mz):: fp
!+---------------------------------------------------------------------
!
!
  if (mx.le.5) then
    fp = fp - f(:,j,:)
    return
  end if
  b = -1./16.
  a = .5-b
!
!$omp parallel private(i,j,k)


  do k=izs,ize
    fp(mx-2,k) = fp(mx-2,k) - ( &
                   a*(f(mx-2,j,k)+f(mx-1,j,k)) &
                 + b*(f(mx-3,j,k)+f(mx  ,j,k)))
    fp(mx-1,k) = fp(mx-1,k) - ( &
                   a*(f(mx-1,j,k)+f(mx  ,j,k)) &
                 + b*(f(mx-2,j,k)+h(1   ,j,k)))
    fp(mx  ,k) = fp(mx  ,k) - ( &
                   a*(f(mx  ,j,k)+h(1   ,j,k)) &
                 + b*(f(mx-1,j,k)+h(2   ,j,k)))
    fp(1   ,k) = fp(1   ,k) - ( &
                   a*(f(1   ,j,k)+f(2   ,j,k)) &
                 + b*(g(2   ,j,k)+f(3   ,j,k)))
    fp(2   ,k) = fp(2   ,k) - ( &
                   a*(f(2   ,j,k)+f(3   ,j,k)) &
                 + b*(f(1   ,j,k)+f(4   ,j,k)))
    do i=3,mx-3
      fp(i  ,k) = fp(i  ,k) - ( &
                    a*(f(i  ,j,k)+f(i+1 ,j,k)) &
                  + b*(f(i-1,j,k)+f(i+2 ,j,k)))
    end do
  end do
!$omp end parallel
!
  if (omp_master) then
      nstag = nstag+1
      nflop = nflop+8
  end if
END SUBROUTINE

!***********************************************************************
SUBROUTINE xup132_sub (f,j, fp)
!
!  f is centered on (i-.5,j,k), fifth order stagger
!
  use params, h=>hx1
  implicit none
  logical omp_in_parallel
  real a, b, c
  integer i, j, k
  real, dimension(mx,my,mz):: f
  real, dimension(mx,mz):: fp
!+---------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = fp - f(:,j,:)
    return
  end if
!
  a = .5
!
!$omp parallel private(i,j,k)


  do k=izs,ize
    fp(mx  ,k) = fp(mx  ,k) - ( &
                   a*(f(mx  ,j,k)+h(1   ,j,k)))
    do i=1,mx-1
      fp(i  ,k) = fp(i  ,k) - ( &
                    a*(f(i  ,j,k)+f(i+1 ,j,k)))
    end do
  end do
!$omp end parallel
!
  if (omp_master) then
    nstag1 = nstag1+1
    nflop = nflop+2
  end if
END SUBROUTINE

!***********************************************************************
SUBROUTINE xdn32_sub (f,j, fp)
!
!  f is centered on (i,j,k), fifth order stagger
!
  use params, g=>gx3, h=>hx2
  implicit none
  logical omp_in_parallel
  real a, b, c
  integer i, j, k
  real, dimension(mx,my,mz):: f
  real, dimension(mx,mz):: fp
!+---------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = fp - f(:,j,:)
    return
  end if
!
  b = -1./16.
  a = .5-b
!
!$omp parallel private(i,j,k)


  do k=izs,ize
    fp(mx-1,k) = fp(mx-1,k) - ( &
                   a*(f(mx-2,j,k)+f(mx-1,j,k)) &
                 + b*(f(mx-3,j,k)+f(mx  ,j,k)))
    fp(mx  ,k) = fp(mx  ,k) - ( &
                   a*(f(mx-1,j,k)+f(mx  ,j,k)) &
                 + b*(f(mx-2,j,k)+h(1   ,j,k)))
    fp(1  ,k) = fp(1  ,k) - ( &
                   a*(g(3   ,j,k)+f(1   ,j,k)) &
                 + b*(g(2   ,j,k)+f(2   ,j,k)))
    fp(2  ,k) = fp(2  ,k) - ( &
                   a*(f(1   ,j,k)+f(2   ,j,k)) &
                 + b*(g(3   ,j,k)+f(3   ,j,k)))
    fp(3  ,k) = fp(3  ,k) - ( &
                   a*(f(2   ,j,k)+f(3   ,j,k)) &
                 + b*(f(1   ,j,k)+f(4   ,j,k)))
    do i=4,mx-2
      fp(i,k) = fp(i,k) - ( &
                   a*(f(i   ,j,k)+f(i-1 ,j,k)) &
                 + b*(f(i+1 ,j,k)+f(i-2 ,j,k)))
    end do
  end do
!$omp end parallel
!
!
  if (omp_master) then
    nstag = nstag+1
    nflop = nflop+8
  end if
END SUBROUTINE

!***********************************************************************
SUBROUTINE xdn132_sub (f,j, fp)
!
!  f is centered on (i,j,k), fifth order stagger
!
  use params, g=>gx1
  implicit none
  logical omp_in_parallel
  real a, b, c
  integer i, j, k
  real, dimension(mx,my,mz):: f
  real, dimension(mx,mz):: fp
!+---------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = fp - f(:,j,:)
    return
  end if
!
  a = 0.5
!
!$omp parallel private(i,j,k)


  do k=izs,ize
    fp(1  ,k) = fp(1  ,k) - ( &
                   a*(g(1   ,j,k)+f(1   ,j,k)))
    do i=2,mx
      fp(i,k) = fp(i,k) - ( &
                   a*(f(i   ,j,k)+f(i-1 ,j,k)))
    end do
  end do
!$omp end parallel
!
  if (omp_master) then
    nstag1 = nstag1+1
    nflop = nflop+2
  end if
END SUBROUTINE

!***********************************************************************
SUBROUTINE xup22_sub (f, fp)
!
!  f is centered on (i-.5,k), fifth order stagger
!
  use params, g=>gx2, h=> hx3
  implicit none
  logical omp_in_parallel
  real a, b, c
  integer i, j, k
  real, dimension(mx,mz):: fp, f
!+---------------------------------------------------------------------
!
!
  if (mx.le.5) then
    fp = fp - f
    return
  end if
  b = -1./16.
  a = .5-b
!
!$omp parallel private(i,k)


  do k=izs,ize
    fp(mx-2,k) = fp(mx-2,k) - ( &
                   a*(f(mx-2,k)+f(mx-1,k)) &
                 + b*(f(mx-3,k)+f(mx  ,k)))
    fp(mx-1,k) = fp(mx-1,k) - ( &
                   a*(f(mx-1,k)+f(mx  ,k)) &
                 + b*(f(mx-2,k)+h(1 ,1,k)))
    fp(mx  ,k) = fp(mx  ,k) - ( &
                   a*(f(mx  ,k)+h(1 ,1,k)) &
                 + b*(f(mx-1,k)+h(2 ,1,k)))
    fp(1   ,k) = fp(1   ,k) - ( &
                   a*(f(1   ,k)+f(2   ,k)) &
                 + b*(g(2 ,1,k)+f(3   ,k)))
    fp(2   ,k) = fp(2   ,k) - ( &
                   a*(f(2   ,k)+f(3   ,k)) &
                 + b*(f(1   ,k)+f(4   ,k)))
    do i=3,mx-3
      fp(i  ,k) = fp(i  ,k) - ( &
                    a*(f(i  ,k)+f(i+1 ,k)) &
                  + b*(f(i-1,k)+f(i+2 ,k)))
    end do
  end do
!$omp end parallel
!
  if (omp_master) then
      nstag = nstag+1
      nflop = nflop+8
  end if
END SUBROUTINE

!***********************************************************************
SUBROUTINE xup122_sub (f, fp)
!
!  f is centered on (i-.5,k), fifth order stagger
!
  use params, h=> hx1
  implicit none
  logical omp_in_parallel
  real a, b, c
  integer i, j, k
  real, dimension(mx,mz):: fp, f
!+---------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = fp - f
    return
  end if
!
  a = .5
!
!$omp parallel private(i,k)


  do k=izs,ize
    fp(mx  ,k) = fp(mx  ,k) - ( &
                   a*(f(mx  ,k)+h(1 ,1,k)))
    do i=1,mx-1
      fp(i  ,k) = fp(i  ,k) - ( &
                    a*(f(i  ,k)+f(i+1 ,k)))
    end do
  end do
!$omp end parallel
!
  if (omp_master) then
    nstag1 = nstag1+1
    nflop = nflop+2
  end if
END SUBROUTINE

!***********************************************************************
SUBROUTINE xdn22_sub (f, fp)
!
!  f is centered on (i,k), fifth order stagger
!
  use params, g=>gx3, h=>hx2
  implicit none
  logical omp_in_parallel
  real a, b, c
  integer i, j, k
  real, dimension(mx,mz):: fp, f
!+---------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = fp - f
    return
  end if
!
  b = -1./16.
  a = .5-b
!
!$omp parallel private(i,k)


  do k=izs,ize
    fp(mx-1,k) = fp(mx-1,k) - ( &
                   a*(f(mx-2,k)+f(mx-1,k)) &
                 + b*(f(mx-3,k)+f(mx  ,k)))
    fp(mx  ,k) = fp(mx  ,k) - ( &
                   a*(f(mx-1,k)+f(mx  ,k)) &
                 + b*(f(mx-2,k)+h(1 ,1,k)))
    fp(1  ,k) = fp(1  ,k) - ( &
                   a*(g(3 ,1,k)+f(1   ,k)) &
                 + b*(g(2 ,1,k)+f(2   ,k)))
    fp(2  ,k) = fp(2  ,k) - ( &
                   a*(f(1   ,k)+f(2   ,k)) &
                 + b*(g(3 ,1,k)+f(3   ,k)))
    fp(3  ,k) = fp(3  ,k) - ( &
                   a*(f(2   ,k)+f(3   ,k)) &
                 + b*(f(1   ,k)+f(4   ,k)))
    do i=4,mx-2
      fp(i,k) = fp(i,k) - ( &
                   a*(f(i   ,k)+f(i-1 ,k)) &
                 + b*(f(i+1 ,k)+f(i-2 ,k)))
    end do
  end do
!$omp end parallel
!
!
  if (omp_master) then
    nstag = nstag+1
    nflop = nflop+8
  end if
END SUBROUTINE

!***********************************************************************
SUBROUTINE xdn122_sub (f, fp)
!
!  f is centered on (i,k), fifth order stagger
!
  use params, g=>gx1
  implicit none
  logical omp_in_parallel
  real a, b, c
  integer i, j, k
  real, dimension(mx,mz):: fp, f
!+---------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = fp - f
    return
  end if
!
  a = 0.5
!
!$omp parallel private(i,k)


  do k=izs,ize
    fp(1  ,k) = fp(1  ,k) - ( &
                   a*(g(1 ,1,k)+f(1   ,k)))
    do i=2,mx
      fp(i,k) = fp(i,k) - ( &
                   a*(f(i   ,k)+f(i-1 ,k)))
    end do
  end do
!$omp end parallel
!
  if (omp_master) then
    nstag1 = nstag1+1
    nflop = nflop+2
  end if
END SUBROUTINE
!***********************************************************************
SUBROUTINE ddxupi_sub (f, fp)
  use params, g=>gx2, h=>hx3
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f, fp
!+---------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = fp - 0.
    return
  end if
!
  b = -1./24.
  a = (1.-3.*b)
  a1st = 1.

!
!$omp parallel private(i,j,k)
  do k=izs,ize
   do j=1,my
    fp(mx-2,j,k) = fp(mx-2,j,k) - ( &
                 + b*(f(mx  ,j,k)-f(mx-3,j,k)) &
                 + a*(f(mx-1,j,k)-f(mx-2,j,k)))
    fp(mx-1,j,k) = fp(mx-1,j,k) - ( &
                 + b*(h(1   ,j,k)-f(mx-2,j,k)) &
                 + a*(f(mx  ,j,k)-f(mx-1,j,k)))
    fp(mx  ,j,k) = fp(mx  ,j,k) - ( &
                 + b*(h(2   ,j,k)-f(mx-1,j,k)) &
                 + a*(h(1   ,j,k)-f(mx  ,j,k)))
    fp(1  ,j,k) = fp(1  ,j,k) - ( &
                 + b*(f(3   ,j,k)-g(2   ,j,k)) &
                 + a*(f(2   ,j,k)-f(1   ,j,k)))
    fp(2  ,j,k) = fp(2  ,j,k) - ( &
                 + b*(f(4   ,j,k)-f(1   ,j,k)) &
                 + a*(f(3   ,j,k)-f(2   ,j,k)))
   end do
   do j=1,my
    do i=3,mx-3
      fp(i  ,j,k) = fp(i  ,j,k) - ( &
                 + b*(f(i+2 ,j,k)-f(i-1 ,j,k)) &
                 + a*(f(i+1 ,j,k)-f(i   ,j,k)))
    end do
  end do
  end do
!$omp end parallel
!
  nstag = nstag+1
  nflop = nflop+8
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddxupi1_sub (f, fp)
  use params, g=>gx1
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f, fp
!+---------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = fp - 0.
    return
  end if
  a = 1.

!$omp parallel private(i,j,k)

!
  do k=izs,ize
   do j=1,my
    fp(mx  ,j,k) = fp(mx  ,j,k) - ( &
                   a*(g(1  ,j,k)-f(mx,j,k)))
   end do
   do j=1,my
    do i=1,mx-1
      fp(i  ,j,k) = fp(i  ,j,k) - ( &
                   a*(f(i+1,j,k)-f(i ,j,k)))
    end do
  end do
  end do
!$omp end parallel
!
  nstag1 = nstag1+1
  nflop = nflop+2
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddxdni_sub (f, fp)
  use params, g=>gx3, h=>hx2
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f, fp
!+---------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = fp - 0.
    return
  end if
!
  b = -1./24.
  a = (1.-3.*b)
  a1st = 1.

!$omp parallel private(i,j,k)

!
  do k=izs,ize
   do j=1,my
    fp(mx-1,j,k) = fp(mx-1,j,k) - ( &
                 + b*(f(mx  ,j,k)-f(mx-3,j,k)) &
                 + a*(f(mx-1,j,k)-f(mx-2,j,k)))
    fp(mx  ,j,k) = fp(mx  ,j,k) - ( &
                 + b*(h(1   ,j,k)-f(mx-2,j,k)) &
                 + a*(f(mx  ,j,k)-f(mx-1,j,k)))
    fp(1   ,j,k) = fp(1   ,j,k) - ( &
                 + b*(f(2   ,j,k)-g(2   ,j,k)) &
                 + a*(f(1   ,j,k)-g(3   ,j,k)))
    fp(2  ,j,k) = fp(2  ,j,k) - ( &
                 + b*(f(3   ,j,k)-g(3   ,j,k)) &
                 + a*(f(2   ,j,k)-f(1   ,j,k)))
    fp(3  ,j,k) = fp(3  ,j,k) - ( &
                 + b*(f(4   ,j,k)-f(1   ,j,k)) &
                 + a*(f(3   ,j,k)-f(2   ,j,k)))
   end do
   do j=1,my
    do i=4,mx-2
      fp(i,j,k) = fp(i,j,k) - ( &
                 + b*(f(i+1 ,j,k)-f(i-2 ,j,k)) &
                 + a*(f(i   ,j,k)-f(i-1 ,j,k)))
    end do
   end do
  end do
!$omp end parallel
!
  nstag = nstag+1
  nflop = nflop+8
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddxdni1_sub (f, fp)
  use params, g=>gx1
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f, fp
!+---------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = fp - 0.
    return
  end if
  a = 1.

!$omp parallel private(i,j,k)

!
  do k=izs,ize
   do j=1,my
    fp(1   ,j,k) = fp(1   ,j,k) - ( &
                   a*(f(1,j,k)-g(1  ,j,k)))
   end do
   do j=1,my
    do i=2,mx
      fp(i,j,k) = fp(i,j,k) - ( &
                   a*(f(i,j,k)-f(i-1,j,k)))
    end do
   end do
  end do
!$omp end parallel
!
  nstag1 = nstag1+1
  nflop = nflop+2
END SUBROUTINE
!***********************************************************************
SUBROUTINE ddxup_sub (f, fp)
  use params, h=>hx3, g=>gx2
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f, fp
!+---------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = fp - 0.
    return
  end if
!
  b = -1./24.
  a = (1.-3.*b)/dx
  a1st = 1./dx
  b = b/dx

!$omp parallel private(i,j,k)
!
  do k=izs,ize
   do j=1,my
    fp(mx-2,j,k) = fp(mx-2,j,k) - ( &
                 + b*(f(mx  ,j,k)-f(mx-3,j,k)) &
                 + a*(f(mx-1,j,k)-f(mx-2,j,k)))
    fp(mx-1,j,k) = fp(mx-1,j,k) - ( &
                 + b*(h(1   ,j,k)-f(mx-2,j,k)) &
                 + a*(f(mx  ,j,k)-f(mx-1,j,k)))
    fp(mx  ,j,k) = fp(mx  ,j,k) - ( &
                 + b*(h(2   ,j,k)-f(mx-1,j,k)) &
                 + a*(h(1   ,j,k)-f(mx  ,j,k)))
    fp(1  ,j,k) = fp(1  ,j,k) - ( &
                 + b*(f(3   ,j,k)-g(2   ,j,k)) &
                 + a*(f(2   ,j,k)-f(1   ,j,k)))
    fp(2  ,j,k) = fp(2  ,j,k) - ( &
                 + b*(f(4   ,j,k)-f(1   ,j,k)) &
                 + a*(f(3   ,j,k)-f(2   ,j,k)))
   end do
   do j=1,my
    do i=3,mx-3
      fp(i  ,j,k) = fp(i  ,j,k) - ( &
                 + b*(f(i+2 ,j,k)-f(i-1 ,j,k)) &
                 + a*(f(i+1 ,j,k)-f(i   ,j,k)))
    end do
  end do
  end do
!$omp end parallel
!
  nstag = nstag+1
  nflop = nflop+8
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddxup1_sub (f, fp)
  use params, g=>gx1
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f, fp
!+---------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = fp - 0.
    return
  end if
  a = 1./dx

!$omp parallel private(i,j,k)
!
  do k=izs,ize
   do j=1,my
    fp(mx  ,j,k) = fp(mx  ,j,k) - ( &
                   a*(g(1  ,j,k)-f(mx,j,k)))
   end do
   do j=1,my
    do i=1,mx-1
      fp(i  ,j,k) = fp(i  ,j,k) - ( &
                   a*(f(i+1,j,k)-f(i ,j,k)))
    end do
  end do
  end do
!$omp end parallel
!
  nstag1 = nstag1+1
  nflop = nflop+2
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddxdn_sub (f, fp)
  use params, g=>gx3, h=>hx2
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f, fp
!+---------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = fp - 0.
    return
  end if
!
  b = -1./24.
  a = (1.-3.*b)/dx
  a1st = 1./dx
  b = b/dx

!$omp parallel private(i,j,k)
!
  do k=izs,ize
   do j=1,my
    fp(mx-1,j,k) = fp(mx-1,j,k) - ( &
                 + b*(f(mx  ,j,k)-f(mx-3,j,k)) &
                 + a*(f(mx-1,j,k)-f(mx-2,j,k)))
    fp(mx  ,j,k) = fp(mx  ,j,k) - ( &
                 + b*(h(1   ,j,k)-f(mx-2,j,k)) &
                 + a*(f(mx  ,j,k)-f(mx-1,j,k)))
    fp(1   ,j,k) = fp(1   ,j,k) - ( &
                 + b*(f(2   ,j,k)-g(2   ,j,k)) &
                 + a*(f(1   ,j,k)-g(3   ,j,k)))
    fp(2  ,j,k) = fp(2  ,j,k) - ( &
                 + b*(f(3   ,j,k)-g(3   ,j,k)) &
                 + a*(f(2   ,j,k)-f(1   ,j,k)))
    fp(3  ,j,k) = fp(3  ,j,k) - ( &
                 + b*(f(4   ,j,k)-f(1   ,j,k)) &
                 + a*(f(3   ,j,k)-f(2   ,j,k)))
   end do
   do j=1,my
    do i=4,mx-2
      fp(i,j,k) = fp(i,j,k) - ( &
                 + b*(f(i+1 ,j,k)-f(i-2 ,j,k)) &
                 + a*(f(i   ,j,k)-f(i-1 ,j,k)))
    end do
   end do
  end do
!$omp end parallel
!
  nstag = nstag+1
  nflop = nflop+8
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddxdn1_sub (f, fp)
  use params, g=>gx1
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f, fp
!+---------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = fp - 0.
    return
  end if
  a = 1./dx

!$omp parallel private(i,j,k)
!
  do k=izs,ize
   do j=1,my
    fp(1   ,j,k) = fp(1   ,j,k) - ( &
                   a*(f(1,j,k)-g(1  ,j,k)))
   end do
   do j=1,my
    do i=2,mx
      fp(i,j,k) = fp(i,j,k) - ( &
                   a*(f(i,j,k)-f(i-1,j,k)))
    end do
   end do
  end do
!$omp end parallel
!
  nstag1 = nstag1+1
  nflop = nflop+2
END SUBROUTINE
!***********************************************************************
SUBROUTINE yup_sub (f, fp)
!
!  f is centered on (i,j-.5,k), fifth order stagger
!
  use params, g=>gy2, h=>gy3
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k
  real, intent(in) , dimension(mx,my,mz):: f
  real,              dimension(mx,my,mz):: fp
!+---------------------------------------------------------------------

  if (my .lt. 5) then
    !$omp parallel private(k)
    do k=izs,ize
      fp(:,:,k) = fp(:,:,k) - f(:,:,k)
    end do
    !$omp end parallel
    return
  end if
  b = -1./16.
  a = .5-b
  a1st = 0.5


  !$omp parallel private(i,j,k)
  do k=izs,ize
    do j=3,my-3
     do i=1,mx
      fp(i,j  ,k) = fp(i,j  ,k) - ( &
                     a*(f(i,j   ,k)+f(i,j+1 ,k)) + &
                     b*(f(i,j-1 ,k)+f(i,j+2 ,k)))
     end do
    end do
    do i=1,mx
      fp(i,my-2,k) = fp(i,my-2,k) - ( &
                     a*(f(i,my-2,k)+f(i,my-1,k)) + &
                     b*(f(i,my-3,k)+f(i,my  ,k)))
      fp(i,my-1,k) = fp(i,my-1,k) - ( &
                     a*(f(i,my-1,k)+f(i,my  ,k)) + &
                     b*(f(i,my-2,k)+h(i,1   ,k)))
      fp(i,my  ,k) = fp(i,my  ,k) - ( &
                     a*(f(i,my  ,k)+h(i,1   ,k)) + &
                     b*(f(i,my-1,k)+h(i,2   ,k)))
      fp(i,1   ,k) = fp(i,1   ,k) - ( &
                     a*(f(i,1   ,k)+f(i,2   ,k)) + &
                     b*(g(i,2   ,k)+f(i,3   ,k)))
      fp(i,2   ,k) = fp(i,2   ,k) - ( &
                     a*(f(i,2   ,k)+f(i,3   ,k)) + &
                     b*(f(i,1   ,k)+f(i,4   ,k)))
    end do
  end do
  !$omp end parallel

  !$omp master
  nstag = nstag+1
  nflop = nflop+8
  !$omp end master
END SUBROUTINE

!***********************************************************************
SUBROUTINE yup1_sub (f, fp)
!
!  f is centered on (i,j-.5,k), fifth order stagger
!
  use params, h=>gy1
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k
  real, intent(in) , dimension(mx,my,mz):: f
  real,              dimension(mx,my,mz):: fp
!+---------------------------------------------------------------------
!
  if (my .lt. 5) then
    !$omp parallel private(k)
    do k=izs,ize
      fp(:,:,k) = fp(:,:,k) - f(:,:,k)
    end do
    !$omp end parallel
    return
  end if

  a = 0.5


  !$omp parallel private(i,j,k)
  do k=izs,ize
    do j=1,my-1
      do i=1,mx
        fp(i,j  ,k) = fp(i,j  ,k) - ( &
                     a*(f(i,j   ,k)+f(i,j+1 ,k)))
      end do
    end do
    do i=1,mx
      fp(i,my,k) = fp(i,my,k) - ( &
                     a*(h(i,1,k)+f(i,my,k)))
    end do
  end do
  !$omp end parallel

  !$omp master
  nstag1 = nstag1+1
  nflop = nflop+2
  !$omp end master
END SUBROUTINE

!***********************************************************************
SUBROUTINE ydn_sub (f, fp)
!
!  f is centered on (i,j,k), fifth order stagger
!
  use params, g=>gy3, h=>gy2
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f, fp
!+---------------------------------------------------------------------

  if (my .lt. 5) then
    !$omp parallel private(k)
    do k=izs,ize
      fp(:,:,k) = fp(:,:,k) - f(:,:,k)
    end do
    !$omp end parallel
    return
  end if

  b = -1./16.
  a = .5-b
  a1st = 0.5


  !$omp parallel private(i,j,k)
  do k=izs,ize
    do j=4,my-2
     do i=1,mx
      fp(i,j,k) = fp(i,j,k) - ( &
                     a*(f(i,j   ,k)+f(i,j-1 ,k)) + &
                     b*(f(i,j+1 ,k)+f(i,j-2 ,k)))
     end do
    end do
    do i=1,mx
      fp(i,my-1,k) = fp(i,my-1,k) - ( &
                     a*(f(i,my-2,k)+f(i,my-1,k)) + &
                     b*(f(i,my-3,k)+f(i,my  ,k)))
      fp(i,my  ,k) = fp(i,my  ,k) - ( &
                     a*(f(i,my-1,k)+f(i,my  ,k)) + &
                     b*(f(i,my-2,k)+h(i,1   ,k)))
      fp(i,1   ,k) = fp(i,1   ,k) - ( &
                     a*(g(i,3   ,k)+f(i,1   ,k)) + &
                     b*(g(i,2   ,k)+f(i,2   ,k)))
      fp(i,2   ,k) = fp(i,2   ,k) - ( &
                     a*(f(i,1   ,k)+f(i,2   ,k)) + &
                     b*(g(i,3   ,k)+f(i,3   ,k)))
      fp(i,3   ,k) = fp(i,3   ,k) - ( &
                     a*(f(i,2   ,k)+f(i,3   ,k)) + &
                     b*(f(i,1   ,k)+f(i,4   ,k)))
    end do
  end do
  !$omp end parallel

  !$omp master
  nstag = nstag+1
  nflop = nflop+8
  !$omp end master
END SUBROUTINE

!***********************************************************************
SUBROUTINE ydn1_sub (f, fp)
!
!  f is centered on (i,j,k), fifth order stagger
!
  use params, g=>gy1
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f, fp
!+---------------------------------------------------------------------
  if (my .lt. 5) then
    !$omp parallel private(k)
    do k=izs,ize
      fp(:,:,k) = fp(:,:,k) - f(:,:,k)
    end do
    !$omp end parallel
    return
  end if

  a = 0.5


  !$omp parallel private(i,j,k)
  do k=izs,ize
    do j=2,my
     do i=1,mx
      fp(i,j,k) = fp(i,j,k) - ( &
                     a*(f(i,j   ,k)+f(i,j-1 ,k)))
     end do
    end do
    do i=1,mx
      fp(i,1   ,k) = fp(i,1   ,k) - ( &
                     a*(g(i,1   ,k)+f(i,1   ,k)))
    end do
  end do
  !$omp end parallel

  !$omp master
  nstag1 = nstag1+1
  nflop = nflop+2
  !$omp end master
END SUBROUTINE
!***********************************************************************
SUBROUTINE ddyupi_sub (f, fp)
  use params, g=>gy2, h=>gy3
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f, fp
!+---------------------------------------------------------------------
!
  if (my .lt. 3) then
    fp = fp - 0.
    return
  end if
  b = -1./24.
  a = (1.-3.*b)
  a1st = 1.


  do k=izs,ize
    do i=1,mx
      fp(i,my-2,k) = fp(i,my-2,k) - ( &
                     a*(f(i,my-1,k)-f(i,my-2,k)) + &
                     b*(f(i,my  ,k)-f(i,my-3,k)))
      fp(i,my-1,k) = fp(i,my-1,k) - ( &
                     a*(f(i,my  ,k)-f(i,my-1,k)) + &
                     b*(h(i,1   ,k)-f(i,my-2,k)))
      fp(i,my  ,k) = fp(i,my  ,k) - ( &
                     a*(h(i,1   ,k)-f(i,my  ,k)) + &
                     b*(h(i,2   ,k)-f(i,my-1,k)))
      fp(i,1   ,k) = fp(i,1   ,k) - ( &
                     a*(f(i,2   ,k)-f(i,1   ,k)) + &
                     b*(f(i,3   ,k)-g(i,2   ,k)))
      fp(i,2   ,k) = fp(i,2   ,k) - ( &
                     a*(f(i,3   ,k)-f(i,2   ,k)) + &
                     b*(f(i,4   ,k)-f(i,1   ,k)))
    end do
    do j=3,my-3
     do i=1,mx
      fp(i,j  ,k) = fp(i,j  ,k) - ( &
                     a*(f(i,j+1 ,k)-f(i,j   ,k)) + &
                     b*(f(i,j+2 ,k)-f(i,j-1 ,k)))
     end do
    end do
  end do

  !$omp master
  nstag = nstag+1
  nflop = nflop+8
  !$omp end master
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddyupi1_sub (f, fp)
  use params, g=>gy1
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f, fp
!+---------------------------------------------------------------------
!
  if (my .lt. 3) then
    fp = fp - 0.
    return
  end if
  a = 1.


  do k=izs,ize
    do i=1,mx
      fp(i,my,k) = fp(i,my,k) - ( &
                     a*(g(i,1  ,k)-f(i,my,k)))
    end do
    do j=1,my-1
      do i=1,mx
        fp(i,j  ,k) = fp(i,j  ,k) - ( &
                     a*(f(i,j+1 ,k)-f(i,j   ,k)))
     end do
    end do
  end do

  !$omp master
  nstag1 = nstag1+1
  nflop = nflop+2
  !$omp end master
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddydni_sub (f, fp)
  use params, g=>gy3, h=>gy2
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f, fp
!+---------------------------------------------------------------------
!
  if (my .lt. 3) then
    fp = fp - 0.
    return
  end if
  b = -1./24.
  a = (1.-3.*b)
  a1st = 1.


  do k=izs,ize
    do i=1,mx
      fp(i,my-1,k) = fp(i,my-1,k) - ( &
                     a*(f(i,my-1,k)-f(i,my-2,k)) + &
                     b*(f(i,my  ,k)-f(i,my-3,k)))
      fp(i,my  ,k) = fp(i,my  ,k) - ( &
                     a*(f(i,my  ,k)-f(i,my-1,k)) + &
                     b*(h(i,1   ,k)-f(i,my-2,k)))
      fp(i,1  ,k) = fp(i,1  ,k) - ( &
                     a*(f(i,1   ,k)-g(i,3   ,k)) + &
                     b*(f(i,2   ,k)-g(i,2   ,k)))
      fp(i,2   ,k) = fp(i,2   ,k) - ( &
                     a*(f(i,2   ,k)-f(i,1   ,k)) + &
                     b*(f(i,3   ,k)-g(i,3   ,k)))
      fp(i,3   ,k) = fp(i,3   ,k) - ( &
                     a*(f(i,3   ,k)-f(i,2   ,k)) + &
                     b*(f(i,4   ,k)-f(i,1   ,k)))
    end do
    do j=4,my-2
     do i=1,mx
      fp(i,j ,k) = fp(i,j ,k) - ( &
                     a*(f(i,j   ,k)-f(i,j-1 ,k)) + &
                     b*(f(i,j+1 ,k)-f(i,j-2 ,k)))
     end do
    end do
  end do

  !$omp master
  nstag = nstag+1
  nflop = nflop+8
  !$omp end master
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddydni1_sub (f, fp)
  use params, g=>gy1
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f, fp
!+---------------------------------------------------------------------
!
  if (my .lt. 3) then
    fp = fp - 0.
    return
  end if
  a = 1.


  do k=izs,ize
    do i=1,mx
      fp(i,1  ,k) = fp(i,1  ,k) - ( &
                     a*(f(i,1   ,k)-g(i,1   ,k)))
    end do
    do j=2,my
     do i=1,mx
      fp(i,j ,k) = fp(i,j ,k) - ( &
                     a*(f(i,j   ,k)-f(i,j-1 ,k)))
     end do
    end do
  end do

  !$omp master
  nstag1 = nstag1+1
  nflop = nflop+2
  !$omp end master
END SUBROUTINE
!***********************************************************************
SUBROUTINE ddyup_sub (f, fp)
  use params, g=>gy2, h=>gy3
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f, fp
!+---------------------------------------------------------------------

  if (my .lt. 3) then
    fp = fp - 0.
    return
  end if
  b = -1./24.
  a = (1.-3.*b)/dy
  a1st = 1./dy
  b = b/dy


  !$omp parallel private(i,j,k)
  do k=izs,ize
    do i=1,mx
      fp(i,my-2,k) = fp(i,my-2,k) - ( &
                     b*(f(i,my  ,k)-f(i,my-3,k)) + &
                     a*(f(i,my-1,k)-f(i,my-2,k)) )
      fp(i,my-1,k) = fp(i,my-1,k) - ( &
                     b*(h(i,1   ,k)-f(i,my-2,k)) + &
                     a*(f(i,my  ,k)-f(i,my-1,k)) )
      fp(i,my  ,k) = fp(i,my  ,k) - ( &
                     b*(h(i,2   ,k)-f(i,my-1,k)) + &
                     a*(h(i,1   ,k)-f(i,my  ,k)) )
      fp(i,1   ,k) = fp(i,1   ,k) - ( &
                     b*(f(i,3   ,k)-g(i,2   ,k)) + &
                     a*(f(i,2   ,k)-f(i,1   ,k)) )
      fp(i,2   ,k) = fp(i,2   ,k) - ( &
                     b*(f(i,4   ,k)-f(i,1   ,k)) + &
                     a*(f(i,3   ,k)-f(i,2   ,k)) )
    end do
    do j=3,my-3
     do i=1,mx
      fp(i,j  ,k) = fp(i,j  ,k) - ( &
                     b*(f(i,j+2 ,k)-f(i,j-1 ,k)) + &
                     a*(f(i,j+1 ,k)-f(i,j   ,k)) )
     end do
    end do
  end do
  !$omp end parallel

  !$omp master
  nstag = nstag+1
  nflop = nflop+8
  !$omp end master
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddyup1_sub (f, fp)
  use params, g=>gy1
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f, fp
!+---------------------------------------------------------------------

  if (my .lt. 3) then
    fp = fp - 0.
    return
  end if
  a = 1./dy


  !$omp parallel private(i,j,k)
  do k=izs,ize
    do i=1,mx
      fp(i,my,k) = fp(i,my,k) - ( &
                     a*(g(i,1  ,k)-f(i,my,k)))
    end do
    do j=1,my-1
      do i=1,mx
        fp(i,j  ,k) = fp(i,j  ,k) - ( &
                     a*(f(i,j+1 ,k)-f(i,j   ,k)))
     end do
    end do
  end do
  !$omp end parallel

  !$omp master
  nstag1 = nstag1+1
  nflop = nflop+2
  !$omp end master
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddydn_sub (f, fp)
  use params, g=>gy3, h=>gy2
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f, fp
!+---------------------------------------------------------------------

  if (my .lt. 3) then
    fp = fp - 0.
    return
  end if
  b = -1./24.
  a = (1.-3.*b)/dy
  a1st = 1./dy
  b = b/dy


  !$omp parallel private(i,j,k)
  do k=izs,ize
    do i=1,mx
      fp(i,my-1,k) = fp(i,my-1,k) - ( &
                     b*(f(i,my  ,k)-f(i,my-3,k)) + &
                     a*(f(i,my-1,k)-f(i,my-2,k)) ) 
      fp(i,my  ,k) = fp(i,my  ,k) - ( &
                     b*(h(i,1   ,k)-f(i,my-2,k)) + &
                     a*(f(i,my  ,k)-f(i,my-1,k)) )
      fp(i,1  ,k) = fp(i,1  ,k) - ( &
                     b*(f(i,2   ,k)-g(i,2   ,k)) + &
                     a*(f(i,1   ,k)-g(i,3   ,k)) )
      fp(i,2   ,k) = fp(i,2   ,k) - ( &
                     b*(f(i,3   ,k)-g(i,3   ,k)) + &
                     a*(f(i,2   ,k)-f(i,1   ,k)) )
      fp(i,3   ,k) = fp(i,3   ,k) - ( &
                     b*(f(i,4   ,k)-f(i,1   ,k)) + &
                     a*(f(i,3   ,k)-f(i,2   ,k)) )
    end do
    do j=4,my-2
     do i=1,mx
      fp(i,j ,k) = fp(i,j ,k) - ( &
                     b*(f(i,j+1 ,k)-f(i,j-2 ,k)) + &
                     a*(f(i,j   ,k)-f(i,j-1 ,k)) )
     end do
    end do
  end do
  !$omp end parallel

  !$omp master
  nstag = nstag+1
  nflop = nflop+8
  !$omp end master
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddydn1_sub (f, fp)
  use params, g=>gy1
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f, fp
!+---------------------------------------------------------------------

  if (my .lt. 3) then
    fp = fp - 0.
    return
  end if
  a = 1./dy


  !$omp parallel private(i,j,k)
  do k=izs,ize
    do i=1,mx
      fp(i,1  ,k) = fp(i,1  ,k) - ( &
                     a*(f(i,1   ,k)-g(i,1   ,k)))
    end do
    do j=2,my
     do i=1,mx
      fp(i,j ,k) = fp(i,j ,k) - ( &
                     a*(f(i,j   ,k)-f(i,j-1 ,k)))
     end do
    end do
  end do
  !$omp end parallel

  !$omp master
  nstag1 = nstag1+1
  nflop = nflop+2
  !$omp end master
END SUBROUTINE
!***********************************************************************
SUBROUTINE zup_sub (f, fp)
!
!  f is centered on (i,j,k-.5), fifth order stagger
!
  use params
  implicit none
  logical omp_in_parallel
  real a, b, c
  integer i, j, k
  real, intent(in) , dimension(mx,my,mz):: f
  real,              dimension(mx,my,mz):: fp
  real,              dimension(mx,my, 2):: g
  real,              dimension(mx,my, 3):: h
!+---------------------------------------------------------------------
!

  if (mz .lt. 5) then
    !$omp parallel private(k)
    do k=izs,ize
      fp(:,:,k) = fp(:,:,k) - f(:,:,k)
    end do
    !$omp end parallel
    return
  end if
  b = -1./16.
  a = .5-b

!
  !$omp parallel private(i,j,k)
  do k=max(3,izs),min(mz-3,ize)
    do j=1,my
      do i=1,mx
        fp(i,j,k) = fp(i,j,k) - ( &
                     a*(f(i,j,k   )+f(i,j,k+1 )) + &
                     b*(f(i,j,k-1 )+f(i,j,k+2 )))
     end do
    end do
  end do
  if (ize.eq.mz) then
   do j=1,my
    do i=1,mx
      fp(i,j,mz-2) = fp(i,j,mz-2) - ( &
                     a*(f(i,j,mz-2)+f(i,j,mz-1)) + &
                     b*(f(i,j,mz-3)+f(i,j,mz  )))
      fp(i,j,mz-1) = fp(i,j,mz-1) - ( &
                     a*(f(i,j,mz-1)+f(i,j,mz  )) + &
                     b*(f(i,j,mz-2)+h(i,j,1   )))
      fp(i,j,mz  ) = fp(i,j,mz  ) - ( &
                     a*(f(i,j,mz  )+h(i,j,1   )) + &
                     b*(f(i,j,mz-1)+h(i,j,2   )))
    end do
   end do
  end if
  if (izs.eq.1) then
   do j=1,my
    do i=1,mx
      fp(i,j,1   ) = fp(i,j,1   ) - ( &
                     a*(f(i,j,1   )+f(i,j,2   )) + &
                     b*(g(i,j,2   )+f(i,j,3   )))
      fp(i,j,2   ) = fp(i,j,2   ) - ( &
                     a*(f(i,j,2   )+f(i,j,3   )) + &
                     b*(f(i,j,1   )+f(i,j,4   )))
    end do
   end do
  end if
  !$omp end parallel
!
  nstag = nstag+1
  nflop = nflop+8
END SUBROUTINE

!***********************************************************************
SUBROUTINE zup1_sub (f, fp)
!
!  f is centered on (i,j,k-.5), fifth order stagger
!
  use params
  implicit none
  logical omp_in_parallel
  real a, b, c
  integer i, j, k
  real, intent(in) , dimension(mx,my,mz):: f
  real,              dimension(mx,my,mz):: fp
  real,              dimension(mx,my,1 ):: h
!+---------------------------------------------------------------------
!
  if (mz .lt. 5) then
    !$omp parallel private(k)
    do k=izs,ize
      fp(:,:,k) = fp(:,:,k) - f(:,:,k)
    end do
    !$omp end parallel
    return
  end if
!
  a = 0.5
!
  !$omp parallel private(i,j,k)
  do k=izs,min(mz-1,ize)
    do j=1,my
      do i=1,mx
        fp(i,j,k  ) = fp(i,j,k  ) - ( &
                     a*(f(i,j,k   )+f(i,j,k+1 )))
      end do
    end do
  end do
  if (ize.eq.mz) then
   do j=1,my
    do i=1,mx
      fp(i,j,mz) = fp(i,j,mz) - ( &
                     a*(h(i,j,1)+f(i,j,mz)))
    end do
   end do
  end if
  !$omp end parallel
!
  nstag1 = nstag1+1
  nflop = nflop+2
END SUBROUTINE

!***********************************************************************
SUBROUTINE zdn_sub (f, fp)
!
!  f is centered on (i,j), fifth order stagger
!
  use params
  implicit none
  logical omp_in_parallel
  real a, b, c
  integer i, j, k
  real, dimension(mx,my,mz):: f, fp
  real, dimension(mx,my, 3):: g
  real, dimension(mx,my, 2):: h
!+---------------------------------------------------------------------
!
  if (mz .lt. 5) then
    !$omp parallel private(k)
    do k=izs,ize
      fp(:,:,k) = fp(:,:,k) - f(:,:,k)
    end do
    !$omp end parallel
    return
  end if
  b = -1./16.
  a = .5-b

!
  !$omp parallel private(i,j,k)
  do k=max(4,izs),min(mz-2,ize)
    do j=1,my
     do i=1,mx
      fp(i,j,k) = fp(i,j,k) - ( &
                     a*(f(i,j,k   )+f(i,j,k-1 )) + &
                     b*(f(i,j,k+1 )+f(i,j,k-2 )))
     end do
    end do
  end do
  if (ize.eq.mz) then
   do j=1,my
    do i=1,mx
      fp(i,j,mz-1) = fp(i,j,mz-1) - ( &
                     a*(f(i,j,mz-2)+f(i,j,mz-1)) + &
                     b*(f(i,j,mz-3)+f(i,j,mz  )))
      fp(i,j,mz  ) = fp(i,j,mz  ) - ( &
                     a*(f(i,j,mz-1)+f(i,j,mz  )) + &
                     b*(f(i,j,mz-2)+h(i,j,1   )))
    end do
   end do
  end if
  if (izs.eq.1) then
   do j=1,my
    do i=1,mx
      fp(i,j,1   ) = fp(i,j,1   ) - ( &
                     a*(g(i,j,3   )+f(i,j,1   )) + &
                     b*(g(i,j,2   )+f(i,j,2   )))
      fp(i,j,2   ) = fp(i,j,2   ) - ( &
                     a*(f(i,j,1   )+f(i,j,2   )) + &
                     b*(g(i,j,3   )+f(i,j,3   )))
      fp(i,j,3   ) = fp(i,j,3   ) - ( &
                     a*(f(i,j,2   )+f(i,j,3   )) + &
                     b*(f(i,j,1   )+f(i,j,4   )))
    end do
   end do
  end if
  !$omp end parallel
!
  nstag = nstag+1
  nflop = nflop+8
END SUBROUTINE

!***********************************************************************
SUBROUTINE zdn1_sub (f, fp)
!
!  f is centered on (i,j), fifth order stagger
!
  use params
  implicit none
  logical omp_in_parallel
  real a, b, c
  integer i, j, k
  real, dimension(mx,my,mz):: f, fp
  real, dimension(mx,my,1 ):: g
!+---------------------------------------------------------------------
!
  if (mz .lt. 5) then
    !$omp parallel private(k)
    do k=izs,ize
      fp(:,:,k) = fp(:,:,k) - f(:,:,k)
    end do
    !$omp end parallel
    return
  end if
!
  a = 0.5
!
  !$omp parallel private(i,j,k)
  do k=max(2,izs),ize
    do j=1,my
     do i=1,mx
      fp(i,j,k) = fp(i,j,k) - ( &
                     a*(f(i,j,k   )+f(i,j,k-1 )))
     end do
    end do
  end do
  if (izs.eq.1) then
   do j=1,my
    do i=1,mx
      fp(i,j,1   ) = fp(i,j,1   ) - ( &
                     a*(g(i,j,1   )+f(i,j,1   )))
    end do
   end do
  end if
  !$omp end parallel
!
  nstag1 = nstag1+1
  nflop = nflop+2
END SUBROUTINE

!***********************************************************************
SUBROUTINE zup32_sub (f,j, fp)
!
!  f is centered on (i,j,k-.5), fifth order stagger
!
  use params
  implicit none
  logical omp_in_parallel
  real a, b, c
  integer i, j, k
  real,             dimension(mx,   mz):: fp
  real, intent(in), dimension(mx,my,mz):: f
  real,             dimension(mx,my, 2):: g
  real,             dimension(mx,my, 3):: h
!+---------------------------------------------------------------------
!
!
  if (mz.le.5) then
    !$omp parallel private(k)
    do k=izs,ize
      fp(:,k) = fp(:,k) - f(:,j,k)
    end do
    !$omp end parallel
    return
  end if

  b = -1./16.
  a = .5-b

!
!$omp parallel private(i,j,k)
  do k=max(3,izs),min(mz-3,ize)
      do i=1,mx
        fp(i,k) = fp(i,k) - ( &
                     a*(f(i,j,k+1)+f(i,j,k  )) + &
                     b*(f(i,j,k+2)+f(i,j,k-1)))
      end do
  end do
  if (izs.eq.1) then
    do i=1,mx
      fp(i,1   ) = fp(i,1   ) - ( &
                     a*(f(i,j,1   )+f(i,j,2   )) + &
                     b*(g(i,j,2   )+f(i,j,3   )))
      fp(i,2   ) = fp(i,2   ) - ( &
                     a*(f(i,j,2   )+f(i,j,3   )) + &
                     b*(f(i,j,1   )+f(i,j,4   )))
    end do
  end if
  if (ize.eq.mz) then
    do i=1,mx
      fp(i,mz-2) = fp(i,mz-2) - ( &
                     a*(f(i,j,mz-2)+f(i,j,mz-1)) + &
                     b*(f(i,j,mz-3)+f(i,j,mz  )))
      fp(i,mz-1) = fp(i,mz-1) - ( &
                     a*(f(i,j,mz-1)+f(i,j,mz  )) + &
                     b*(f(i,j,mz-2)+h(i,j,1   )))
      fp(i,mz  ) = fp(i,mz  ) - ( &
                     a*(f(i,j,mz  )+h(i,j,1   )) + &
                     b*(f(i,j,mz-1)+h(i,j,2   )))
    end do
  end if

!$omp end parallel
!
  if (omp_master) then
    nstag = nstag+1
    nflop = nflop+8
  end if
END SUBROUTINE

!***********************************************************************
SUBROUTINE zup132_sub (f,j, fp)
!
!  f is centered on (i,j,k-.5), fifth order stagger
!
  use params
  implicit none
  logical omp_in_parallel
  real a, b, c
  integer i, j, k
  real,              dimension(mx,   mz):: fp
  real, intent(in),  dimension(mx,my,mz):: f
  real,              dimension(mx,my,1 ):: h
!+---------------------------------------------------------------------
!
  if (mz.le.5) then
    !$omp parallel private(k)
    do k=izs,ize
      fp(:,k) = fp(:,k) - f(:,j,k)
    end do
    !$omp end parallel
    return
  end if

  a = .5
!
!$omp parallel private(i,j,k)
  do k=izs,min(mz-1,ize)
      do i=1,mx
        fp(i,k) = fp(i,k) - ( &
                     a*(f(i,j,k+1)+f(i,j,k  )))
      end do
  end do
  if (ize.eq.mz) then
    do i=1,mx
      fp(i,mz) = fp(i,mz) - ( &
                     a*(h(i,j,1)+f(i,j,mz)))
    end do
  end if

!$omp end parallel
!
  if (omp_master) then
    nstag1 = nstag1+1
    nflop = nflop+2
  end if
END SUBROUTINE

!***********************************************************************
SUBROUTINE zdn32_sub (f,j, fp)
!
!  f is centered on (i,j,k), fifth order stagger
!
  use params
  implicit none
  logical omp_in_parallel
  real a, b, c
  integer i, j, k
  real,             dimension(mx,   mz):: fp
  real, intent(in), dimension(mx,my,mz):: f
  real,             dimension(mx,my, 3):: g
  real,             dimension(mx,my, 2):: h

!+---------------------------------------------------------------------
!
  if (mz.le.5) then
    !$omp parallel private(k)
    do k=izs,ize
      fp(:,k) = fp(:,k) - f(:,j,k)
    end do
    !$omp end parallel
    return
  end if

  b = -1./16.
  a = .5-b

!
!$omp parallel private(i,j,k)
  do k=max(4,izs),min(mz-2,ize)
      do i=1,mx
        fp(i,k) = fp(i,k) - ( &
                     a*(f(i,j,k  )+f(i,j,k-1)) + &
                     b*(f(i,j,k+1)+f(i,j,k-2)))
      end do
  end do
  if (izs.eq.1) then
    do i=1,mx
      fp(i,1   ) = fp(i,1   ) - ( &
                     a*(g(i,j,3   )+f(i,j,1   )) + &
                     b*(g(i,j,2   )+f(i,j,2   )))
      fp(i,2   ) = fp(i,2   ) - ( &
                     a*(f(i,j,1   )+f(i,j,2   )) + &
                     b*(g(i,j,3   )+f(i,j,3   )))
      fp(i,3   ) = fp(i,3   ) - ( &
                     a*(f(i,j,2   )+f(i,j,3   )) + &
                     b*(f(i,j,1   )+f(i,j,4   )))
    end do
  end if
  if (ize.eq.mz) then
    do i=1,mx
      fp(i,mz-1) = fp(i,mz-1) - ( &
                     a*(f(i,j,mz-2)+f(i,j,mz-1)) + &
                     b*(f(i,j,mz-3)+f(i,j,mz  )))
      fp(i,mz  ) = fp(i,mz  ) - ( &
                     a*(f(i,j,mz-1)+f(i,j,mz  )) + &
                     b*(f(i,j,mz-2)+h(i,j,1   )))
    end do
  end if

!$omp end parallel
!
  if (omp_master) then
    nstag = nstag+1
    nflop = nflop+8
  end if
END SUBROUTINE

!***********************************************************************
SUBROUTINE zdn132_sub (f,j, fp)
!
!  f is centered on (i,j,k), fifth order stagger
!
  use params
  implicit none
  logical omp_in_parallel
  real a, b, c
  integer i, j, k
  real,             dimension(mx,   mz):: fp
  real, intent(in), dimension(mx,my,mz):: f
  real,             dimension(mx,my,1 ):: g
!+---------------------------------------------------------------------
!
  if (mz.le.5) then
    !$omp parallel private(k)
    do k=izs,ize
      fp(:,k) = fp(:,k) - f(:,j,k)
    end do
    !$omp end parallel
    return
  end if

  a = .5
!
!$omp parallel private(i,j,k)
  do k=max(2,izs),ize
      do i=1,mx
        fp(i,k) = fp(i,k) - ( &
                     a*(f(i,j,k  )+f(i,j,k-1)))
      end do
  end do
  if (izs.eq.1) then
    do i=1,mx
      fp(i,1   ) = fp(i,1   ) - ( &
                     a*(g(i,j,1   )+f(i,j,1   )))
    end do
  end if

!$omp end parallel
!
  if (omp_master) then
    nstag1 = nstag1+1
    nflop = nflop+2
  end if
END SUBROUTINE

!***********************************************************************
SUBROUTINE zup22_sub (f, fp)
!
!  f is centered on (i,k-.5), fifth order stagger
!
  use params
  implicit none
  logical omp_in_parallel
  real a, b, c
  integer i, j, k
  real,             dimension(mx,  mz):: fp
  real, intent(in), dimension(mx,  mz):: f
  real,             dimension(mx,1, 2):: g
  real,             dimension(mx,1, 3):: h

!+---------------------------------------------------------------------
!
!
  if (mz.le.5) then
    !$omp parallel private(k)
    do k=izs,ize
      fp(:,k) = fp(:,k) - f(:,k)
    end do
    !$omp end parallel
    return
  end if
  b = -1./16.
  a = .5-b

!
!$omp parallel private(i,k)
  do k=max(3,izs),min(mz-3,ize)
      do i=1,mx
        fp(i,k) = fp(i,k) - ( &
                     a*(f(i,k+1)+f(i,k  )) + &
                     b*(f(i,k+2)+f(i,k-1)))
      end do
  end do
  if (izs.eq.1) then
    do i=1,mx
      fp(i,1   ) = fp(i,1   ) - ( &
                     a*(f(i,1   )+f(i,2   )) + &
                     b*(g(i,1,2   )+f(i,3   )))
      fp(i,2   ) = fp(i,2   ) - ( &
                     a*(f(i,2   )+f(i,3   )) + &
                     b*(f(i,1   )+f(i,4   )))
    end do
  end if
  if (ize.eq.mz) then
    do i=1,mx
      fp(i,mz-2) = fp(i,mz-2) - ( &
                     a*(f(i,mz-2)+f(i,mz-1)) + &
                     b*(f(i,mz-3)+f(i,mz  )))
      fp(i,mz-1) = fp(i,mz-1) - ( &
                     a*(f(i,mz-1)+f(i,mz  )) + &
                     b*(f(i,mz-2)+h(i,1,1   )))
      fp(i,mz  ) = fp(i,mz  ) - ( &
                     a*(f(i,mz  )+h(i,1,1   )) + &
                     b*(f(i,mz-1)+h(i,1,2   )))
    end do
  end if

!$omp end parallel
!
  if (omp_master) then
    nstag = nstag+1
    nflop = nflop+8
  end if
END SUBROUTINE

!***********************************************************************
SUBROUTINE zup122_sub (f, fp)
!
!  f is centered on (i,k-.5), fifth order stagger
!
  use params
  implicit none
  logical omp_in_parallel
  real a, b, c
  integer i, j, k
  real,             dimension(mx,   mz):: fp
  real, intent(in), dimension(mx,   mz):: f
  real,             dimension(mx,my,1 ):: h
!+---------------------------------------------------------------------
!
  if (mz.le.5) then
    !$omp parallel private(k)
    do k=izs,ize
      fp(:,k) = fp(:,k) - f(:,k)
    end do
    !$omp end parallel
    return
  end if

  a = .5
!
!$omp parallel private(i,k)
  do k=izs,ize
      do i=1,mx
        fp(i,k) = fp(i,k) - ( &
                     a*(f(i,k+1)+f(i,k  )))
      end do
  end do
  if (ize.eq.mz) then
    do i=1,mx
      fp(i,mz) = fp(i,mz) - ( &
                     a*(h(i,1,1)+f(i,mz)))
    end do
  end if

!$omp end parallel
!
  if (omp_master) then
    nstag1 = nstag1+1
    nflop = nflop+2
  end if
END SUBROUTINE

!***********************************************************************
SUBROUTINE zdn22_sub (f, fp)
!
!  f is centered on (i,k), fifth order stagger
!
  use params
  implicit none
  logical omp_in_parallel
  real a, b, c
  integer i, j, k
  real,             dimension(mx,   mz):: fp
  real, intent(in), dimension(mx,   mz):: f
  real,             dimension(mx,1, 3 ):: g
  real,             dimension(mx,1, 2 ):: h

!+---------------------------------------------------------------------
!
  if (mz.le.5) then
    !$omp parallel private(k)
    do k=izs,ize
      fp(:,k) = fp(:,k) - f(:,k)
    end do
    !$omp end parallel
    return
  end if
  b = -1./16.
  a = .5-b
!
!$omp parallel private(i,k)
  do k=max(4,izs),min(mz-2,ize)
      do i=1,mx
        fp(i,k) = fp(i,k) - ( &
                     a*(f(i,k  )+f(i,k-1)) + &
                     b*(f(i,k+1)+f(i,k-2)))
      end do
  end do
  if (izs.eq.1) then
    do i=1,mx
      fp(i,1   ) = fp(i,1   ) - ( &
                     a*(g(i,1,3 )+f(i,1   )) + &
                     b*(g(i,1,2 )+f(i,2   )))
      fp(i,2   ) = fp(i,2   ) - ( &
                     a*(f(i,1   )+f(i,2   )) + &
                     b*(g(i,1,3 )+f(i,3   )))
      fp(i,3   ) = fp(i,3   ) - ( &
                     a*(f(i,2   )+f(i,3   )) + &
                     b*(f(i,1   )+f(i,4   )))
    end do
  end if
  if (ize.eq.mz) then
    do i=1,mx
      fp(i,mz-1) = fp(i,mz-1) - ( &
                     a*(f(i,mz-2)+f(i,mz-1)) + &
                     b*(f(i,mz-3)+f(i,mz  )))
      fp(i,mz  ) = fp(i,mz  ) - ( &
                     a*(f(i,mz-1)+f(i,mz  )) + &
                     b*(f(i,mz-2)+h(i,1,1   )))
    end do
  end if

!$omp end parallel
!
  if (omp_master) then
    nstag = nstag+1
    nflop = nflop+8
  end if
END SUBROUTINE

!***********************************************************************
SUBROUTINE zdn122_sub (f, fp)
!
!  f is centered on (i,k), fifth order stagger
!
  use params
  implicit none
  logical omp_in_parallel
  real a, b, c
  integer i, j, k
  real,             dimension(mx,   mz):: fp
  real, intent(in), dimension(mx,   mz):: f
  real,             dimension(mx,1 ,1 ):: g
!+---------------------------------------------------------------------
!
  if (mz.le.5) then
    !$omp parallel private(k)
    do k=izs,ize
      fp(:,k) = fp(:,k) - f(:,k)
    end do
    !$omp end parallel
    return
  end if

  a = .5
!
!$omp parallel private(i,k)
  do k=max(2,izs),ize
      do i=1,mx
        fp(i,k) = fp(i,k) - ( &
                     a*(f(i,k  )+f(i,k-1)))
      end do
  end do
  if (izs.eq.1) then
    do i=1,mx
      fp(i,1   ) = fp(i,1   ) - ( &
                     a*(g(i,1,1  )+f(i,1  )))
    end do
  end if
 
!$omp end parallel
!
  if (omp_master) then
    nstag1 = nstag1+1
    nflop = nflop+2
  end if
END SUBROUTINE
!***********************************************************************
SUBROUTINE ddzupi_sub (f, fp)
!
!  f is centered on (i,j,k-.5), fifth order stagger
!
  use params
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k
  real, intent(in) , dimension(mx,my,mz):: f
  real,              dimension(mx,my,mz):: fp
  real,              dimension(mx,my, 2):: g
  real,              dimension(mx,my, 3):: h
!+---------------------------------------------------------------------
!

  if (mz .lt. 5) then
    !$omp parallel private(k)
    do k=izs,ize
      fp(:,:,k) = fp(:,:,k) - f(:,:,k)
    end do
    !$omp end parallel
    return
  end if

  b = -1./24.
  a = (1.-3.*b)
  a1st = 1

!
  !$omp parallel private(i,j,k)
  do k=max(izs,3),min(ize,mz-3)
    do j=1,my
      do i=1,mx
        fp(i,j,k) = fp(i,j,k) - ( &
                     a*(-f(i,j,k   )+f(i,j,k+1 )) + &
                     b*(-f(i,j,k-1 )+f(i,j,k+2 )))
     end do
    end do
  end do
  !$omp end parallel
  do j=1,my
    do i=1,mx
      fp(i,j,mz-2) = fp(i,j,mz-2) - ( &
                     a*(-f(i,j,mz-2)+f(i,j,mz-1)) + &
                     b*(-f(i,j,mz-3)+f(i,j,mz  )))
      fp(i,j,mz-1) = fp(i,j,mz-1) - ( &
                     a*(-f(i,j,mz-1)+f(i,j,mz  )) + &
                     b*(-f(i,j,mz-2)+h(i,j,1   )))
      fp(i,j,mz  ) = fp(i,j,mz  ) - ( &
                     a*(-f(i,j,mz  )+h(i,j,1   )) + &
                     b*(-f(i,j,mz-1)+h(i,j,2   )))
      fp(i,j,1   ) = fp(i,j,1   ) - ( &
                     a*(-f(i,j,1   )+f(i,j,2   )) + &
                     b*(-g(i,j,1   )+f(i,j,3   )))
      fp(i,j,2   ) = fp(i,j,2   ) - ( &
                     a*(-f(i,j,2   )+f(i,j,3   )) + &
                     b*(-f(i,j,1   )+f(i,j,4   )))
    end do
  end do
!
  nstag = nstag+1
  nflop = nflop+8
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddzupi1_sub (f, fp)
!
!  f is centered on (i,j,k-.5), fifth order stagger
!
  use params
  implicit none
  logical omp_in_parallel
  integer i, j, k
  real, intent(in) , dimension(mx,my,mz):: f
  real,              dimension(mx,my,mz):: fp
  real,              dimension(mx,my, 1):: g
!+---------------------------------------------------------------------
!
  if (mz .lt. 5) then
    !$omp parallel private(k)
    do k=izs,ize
      fp(:,:,k) = fp(:,:,k) - f(:,:,k)
    end do
    !$omp end parallel
    return
  end if
!
!
  !$omp parallel private(i,j,k)
  do k=izs,min(ize,mz-1)
    do j=1,my
      do i=1,mx
        fp(i,j,k  ) = fp(i,j,k  ) - ( &
                     (f(i,j,k+1)-f(i,j,k)))
      end do
    end do
  end do
  !$omp end parallel
  do j=1,my
    do i=1,mx
      fp(i,j,mz) = fp(i,j,mz) - ( &
                     (+g(i,j,1)-f(i,j,mz)))
    end do
  end do
!
  nstag1 = nstag1+1
  nflop = nflop+2
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddzdni_sub (f, fp)
!
!  f is centered on (i,j), fifth order stagger
!
  use params
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f, fp
  real, dimension(mx,my, 3):: g
  real, dimension(mx,my, 2):: h
!+---------------------------------------------------------------------
!
  if (mz .lt. 5) then
    !$omp parallel private(k)
    do k=izs,ize
      fp(:,:,k) = fp(:,:,k) - f(:,:,k)
    end do
    !$omp end parallel
    return
  end if

  b = -1./24.
  a = (1.-3.*b)
  a1st = 1.

!
  !$omp parallel private(i,j,k)
  do k=max(izs,4),min(ize,mz-2)
    do j=1,my
     do i=1,mx
      fp(i,j,k) = fp(i,j,k) - ( &
                     a*(f(i,j,k   )-f(i,j,k-1)) + &
                     b*(f(i,j,k+1 )-f(i,j,k-2)))
     end do
    end do
  end do
  !$omp end parallel
  do j=1,my
    do i=1,mx
      fp(i,j,mz-1) = fp(i,j,mz-1) - ( &
                     a*(-f(i,j,mz-2)+f(i,j,mz-1)) + &
                     b*(-f(i,j,mz-3)+f(i,j,mz  )))
      fp(i,j,mz  ) = fp(i,j,mz  ) - ( &
                     a*(-f(i,j,mz-1)+f(i,j,mz  )) + &
                     b*(-f(i,j,mz-2)+h(i,j,1   )))
      fp(i,j,1   ) = fp(i,j,1   ) - ( &
                     a*(-g(i,j,3   )+f(i,j,1   )) + &
                     b*(-g(i,j,2   )+f(i,j,2   )))
      fp(i,j,2   ) = fp(i,j,2   ) - ( &
                     a*(-f(i,j,1   )+f(i,j,2   )) + &
                     b*(-g(i,j,3   )+f(i,j,3   )))
      fp(i,j,3   ) = fp(i,j,3   ) - ( &
                     a*(-f(i,j,2   )+f(i,j,3   )) + &
                     b*(-f(i,j,1   )+f(i,j,4   )))
    end do
  end do
!
  nstag = nstag+1
  nflop = nflop+8
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddzdni1_sub (f, fp)
!
!  f is centered on (i,j), fifth order stagger
!
  use params
  implicit none
  logical omp_in_parallel
  integer i, j, k
  real, dimension(mx,my,mz):: f, fp
  real, dimension(mx,my, 1):: g
!+---------------------------------------------------------------------
!
  if (mz .lt. 5) then
    !$omp parallel private(k)
    do k=izs,ize
      fp(:,:,k) = fp(:,:,k) - f(:,:,k)
    end do
    !$omp end parallel
    return
  end if
!
!
  !$omp parallel private(i,j,k)
  do k=max(izs,2),ize
    do j=1,my
     do i=1,mx
      fp(i,j,k) = fp(i,j,k) - ( &
                     (f(i,j,k)-f(i,j,k-1)))
     end do
    end do
  end do
  !$omp end parallel
  do j=1,my
    do i=1,mx
      fp(i,j,1   ) = fp(i,j,1   ) - ( &
                     (f(i,j,1)-g(i,j,1)))
    end do
  end do
!
  nstag1 = nstag1+1
  nflop = nflop+2
END SUBROUTINE
!***********************************************************************
SUBROUTINE ddzup_sub (f, fp)
!
!  f is centered on (i,j,k-.5), fifth order stagger
!
  use params
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k
  real, intent(in) , dimension(mx,my,mz):: f
  real,              dimension(mx,my,mz):: fp
  real,              dimension(mx,my, 2):: g
  real,              dimension(mx,my, 3):: h
!+---------------------------------------------------------------------
!

  if (mz .lt. 5) then
    !$omp parallel private(k)
    do k=izs,ize
      fp(:,:,k) = fp(:,:,k) - 0.
    end do
    !$omp end parallel
    return
  end if

  b = -1./24.
  a = (1.-3.*b)/dz
  a1st = 1./dz
  b = b/dz

!
  !$omp parallel private(i,j,k)
  do k=max(3,izs),min(mz-3,ize)
    do j=1,my
      do i=1,mx
        fp(i,j,k) = fp(i,j,k) - ( &
                     b*(f(i,j,k+2 )-f(i,j,k-1 )) + &
                     a*(f(i,j,k+1 )-f(i,j,k   )))
     end do
    end do
  end do

  if (ize.eq.mz) then
   do j=1,my
    do i=1,mx
      fp(i,j,mz-2) = fp(i,j,mz-2) - ( &
                     b*(f(i,j,mz  )-f(i,j,mz-3)) + &
                     a*(f(i,j,mz-1)-f(i,j,mz-2)))
      fp(i,j,mz-1) = fp(i,j,mz-1) - ( &
                     b*(h(i,j,1   )-f(i,j,mz-2)) + &
                     a*(f(i,j,mz  )-f(i,j,mz-1)))
      fp(i,j,mz  ) = fp(i,j,mz  ) - ( &
                     b*(h(i,j,2   )-f(i,j,mz-1)) + &
                     a*(h(i,j,1   )-f(i,j,mz  )))
    end do
   end do
  end if
  if (izs.eq.1) then
   do j=1,my
    do i=1,mx
      fp(i,j,1   ) = fp(i,j,1   ) - ( &
                     b*(f(i,j,3   )-g(i,j,2   )) + &
                     a*(f(i,j,2   )-f(i,j,1   )))
      fp(i,j,2   ) = fp(i,j,2   ) - ( &
                     b*(f(i,j,4   )-f(i,j,1   )) + &
                     a*(f(i,j,3   )-f(i,j,2   )))
    end do
   end do
  end if
  !$omp end parallel
!
  nstag = nstag+1
  nflop = nflop+8
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddzup1_sub (f, fp)
!
!  f is centered on (i,j,k-.5), fifth order stagger
!
  use params
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k
  real, intent(in) , dimension(mx,my,mz):: f
  real,              dimension(mx,my,mz):: fp
  real,              dimension(mx,my, 1):: g
!+---------------------------------------------------------------------
!
  if (mz .lt. 5) then
    !$omp parallel private(k)
    do k=izs,ize
      fp(:,:,k) = fp(:,:,k) - 0.
    end do
    !$omp end parallel
    return
  end if
!
  a = 1./dz
!
  !$omp parallel private(i,j,k)
  do k=izs,min(mz-1,ize)
    do j=1,my
      do i=1,mx
        fp(i,j,k  ) = fp(i,j,k  ) - ( &
                     a*(f(i,j,k+1)-f(i,j,k)))
      end do
    end do
  end do
  if (ize.eq.mz) then
   do j=1,my
    do i=1,mx
      fp(i,j,mz) = fp(i,j,mz) - ( &
                     a*(g(i,j,1)-f(i,j,mz)))
    end do
   end do
  end if
  !$omp end parallel
!
  nstag1 = nstag1+1
  nflop = nflop+2
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddzdn_sub (f, fp)
!
!  f is centered on (i,j), fifth order stagger
!
  use params
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f, fp
  real, dimension(mx,my, 3):: g
  real, dimension(mx,my, 2):: h
!+---------------------------------------------------------------------
!
  if (mz .lt. 5) then
    !$omp parallel private(k)
    do k=izs,ize
      fp(:,:,k) = fp(:,:,k) - 0.
    end do
    !$omp end parallel
    return
  end if

  b = -1./24.
  a = (1.-3.*b)/dz
  a1st = 1./dz
  b = b/dz

!
  !$omp parallel private(i,j,k)
  do k=max(4,izs),min(mz-2,ize)
    do j=1,my
     do i=1,mx
      fp(i,j,k) = fp(i,j,k) - ( &
                     b*(f(i,j,k+1 )-f(i,j,k-2 )) + &
                     a*(f(i,j,k   )-f(i,j,k-1 )))
     end do
    end do
  end do
  if (ize.eq.mz) then
   do j=1,my
    do i=1,mx
      fp(i,j,mz-1) = fp(i,j,mz-1) - ( &
                     b*(f(i,j,mz  )-f(i,j,mz-3)) + &
                     a*(f(i,j,mz-1)-f(i,j,mz-2)))
      fp(i,j,mz  ) = fp(i,j,mz  ) - ( &
                     b*(h(i,j,1   )-f(i,j,mz-2)) + &
                     a*(f(i,j,mz  )-f(i,j,mz-1)))
    end do
   end do
  end if
  if (izs.eq.1) then
   do j=1,my
    do i=1,mx
      fp(i,j,1   ) = fp(i,j,1   ) - ( &
                     b*(f(i,j,2   )-g(i,j,2   )) + &
                     a*(f(i,j,1   )-g(i,j,3   )))
      fp(i,j,2   ) = fp(i,j,2   ) - ( &
                     b*(f(i,j,3   )-g(i,j,3   )) + &
                     a*(f(i,j,2   )-f(i,j,1   )))
      fp(i,j,3   ) = fp(i,j,3   ) - ( &
                     b*(f(i,j,4   )-f(i,j,1   )) + &
                     a*(f(i,j,3   )-f(i,j,2   )))
    end do
   end do
  end if
  !$omp end parallel
!
  nstag = nstag+1
  nflop = nflop+8
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddzdn1_sub (f, fp)
!
!  f is centered on (i,j), fifth order stagger
!
  use params
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f, fp
  real, dimension(mx,my, 1):: g
!+---------------------------------------------------------------------
!
  if (mz .lt. 5) then
    !$omp parallel private(k)
    do k=izs,ize
      fp(:,:,k) = fp(:,:,k) - 0.
    end do
    !$omp end parallel
    return
  end if
!
  a = 1./dz
!
  !$omp parallel private(i,j,k)
  do k=max(2,izs),ize
    do j=1,my
     do i=1,mx
      fp(i,j,k) = fp(i,j,k) - ( &
                     a*(f(i,j,k)-f(i,j,k-1)))
     end do
    end do
  end do
  if (izs.eq.1) then
   do j=1,my
    do i=1,mx
      fp(i,j,1   ) = fp(i,j,1   ) - ( &
                     a*(f(i,j,1)-g(i,j,1  )))
    end do
   end do
  end if
  !$omp end parallel
!
  nstag1 = nstag1+1
  nflop = nflop+2
END SUBROUTINE

!***********************************************************************
SUBROUTINE d2abs_neg (f, fp)
!
!  Max abs difference one zone left to one zones right.
!

  USE params
  implicit none
  logical omp_in_parallel
  real cx, cy, cz
  real, dimension(mx,my,mz):: f, fp
  real, dimension(mx,my,1):: gz, hz
!hpf$ align with TheCube:: f, fp
  integer i, j, k, km1, kp1
!+---------------------------------------------------------------------

!$omp parallel private(i,j,k,km1,kp1)

  if (mx.gt.1) then
    do k=izs,ize
      do j=1,my
        do i=2,mx-1
          fp(i,j,k) = - abs(f(i-1 ,j,k)+f(i+1,j,k)-f(i,j,k)-f(i,j,k))
        end do
        fp(1 ,j,k) = - abs(gx1(1   ,j,k)+f (2,j,k)-f(1 ,j,k)-f(1 ,j,k))
        fp(mx,j,k) = - abs(f (mx-1,j,k)+hx1(1,j,k)-f(mx,j,k)-f(mx,j,k))
      end do
    end do
  else
    do k=izs,ize
      do j=1,my
        do i=1,mx
          fp(i,j,k) = - 0.
        end do
      end do
    end do
  end if

 if (my .gt. 1) then
  do k=izs,ize
    do j=2,my-1
      do i=1,mx
        fp(i,j, k) = - max(fp(i,j, k),abs(f(i,j -1,k)+f(i,j+1,k)-f(i,j,k)-f(i,j,k)))
      end do
    end do
    do i=1,mx
        fp(i,1 ,k) = - max(fp(i,1 ,k),abs(gy1(i,   1,k)+f (i,2,k)-f(i, 1,k)-f(i, 1,k)))
        fp(i,my,k) = - max(fp(i,my,k),abs(f (i,my-1,k)+hy1(i,1,k)-f(i,my,k)-f(i,my,k)))
    end do
  end do
 end if
 if (mz .gt. 1) then
!$omp barrier
  do k=izs,ize
    if (k.eq.1) then
      do j=1,my
      do i=1,mx
        fp(i,j,k ) = - max(fp(i,j,k ),abs(gz(i,j,1)+f(i,j,k+1)-f(i,j,k)-f(i,j,k)))
      end do
      end do
    else if (k.eq.mz) then
      do j=1,my
      do i=1,mx
        fp(i,j,k ) = - max(fp(i,j,k ),abs(hz(i,j,1)+f(i,j,k-1)-f(i,j,k)-f(i,j,k)))
      end do
      end do
    else
      do j=1,my
      do i=1,mx
        fp(i,j,k ) = - max(fp(i,j,k ),abs(f(i,j,k-1)+f(i,j,k+1)-f(i,j,k)-f(i,j,k)))
      end do
      end do
    end if
  end do
 end if
!$omp end parallel

END SUBROUTINE
!***********************************************************************
SUBROUTINE dif2a_neg (f, fp)
!
!  Max abs difference one zone left to one zones right.
!

  USE params
  implicit none
  logical omp_in_parallel
  real cx, cy, cz
  real, dimension(mx,my,mz):: f, fp
  real, dimension(mx,my,1):: gz, hz
!hpf$ align with TheCube:: f, fp
  integer i, j, k, km1, kp1
!+---------------------------------------------------------------------

!$omp parallel private(i,j,k,km1,kp1)

  if (mx.gt.1) then
    do k=izs,ize
      do j=1,my
        do i=2,mx-1
          fp(i,j,k) = - abs(f(i-1 ,j,k)-f(i+1,j,k))
        end do
        fp(1 ,j,k) = - abs(gx1(1   ,j,k)-f (2,j,k))
        fp(mx,j,k) = - abs(f (mx-1,j,k)-hx1(1,j,k))
      end do
    end do
  else
    do k=izs,ize
      do j=1,my
        do i=1,mx
          fp(i,j,k) = - 0.
        end do
      end do
    end do
  end if

 if (my .gt. 1) then
  do k=izs,ize
    do j=2,my-1
      do i=1,mx
        fp(i,j, k) = - max(fp(i,j, k),abs(f(i,j -1,k)-f(i,j+1,k)))
      end do
    end do
    do i=1,mx
        fp(i,1 ,k) = - max(fp(i,1 ,k),abs(gy1(i,   1,k)-f (i,2,k)))
        fp(i,my,k) = - max(fp(i,my,k),abs(f (i,my-1,k)-hy1(i,1,k)))
    end do
  end do
 end if
 if (mz .gt. 1) then
!$omp barrier
  do k=izs,ize
    if (k.eq.1) then
      do j=1,my
      do i=1,mx
        fp(i,j,k ) = - max(fp(i,j,k ),abs(gz(i,j,1)-f(i,j,k+1)))
      end do
      end do
    else if (k.eq.mz) then
      do j=1,my
      do i=1,mx
        fp(i,j,k ) = - max(fp(i,j,k ),abs(hz(i,j,1)-f(i,j,k-1)))
      end do
      end do
    else
      do j=1,my
      do i=1,mx
        fp(i,j,k ) = - max(fp(i,j,k ),abs(f(i,j,k-1)-f(i,j,k+1)))
      end do
      end do
    end if
  end do
 end if
!$omp end parallel

END SUBROUTINE

!***********************************************************************
SUBROUTINE dif2_neg (fx, fy, fz, fp)
!
!  Finite difference one zone left to one zones right.
!

  USE params
  implicit none
  logical omp_in_parallel
  real cx(mx), cy(my), cz(mz)
  real, dimension(mx,my,mz):: fx, fy, fz, fp
  real, dimension(mx,my,1):: gz, hz
  integer i, j, k, km1, kp1
!+---------------------------------------------------------------------


!$omp parallel private(i,j,k,km1,kp1)


  if (mx.gt.1) then
    do k=izs,ize
      do j=1,my
        do i=2,mx-1
          fp(i,j,k) = - cx(i)*(fx(i-1 ,j,k)-fx(i+1,j,k))
        end do
        fp(1 ,j,k) = - cx( 1)*(gx1(1   ,j,k)-fx(2  ,j,k))
        fp(mx,j,k) = - cx(mx)*(fx(mx-1,j,k)-hx1(1  ,j,k))
      end do
    end do
  else
    do k=izs,ize
      do j=1,my
        do i=1,mx
          fp(i,j,k) = - 0.
        end do
      end do
    end do
  end if


  if (my .gt. 1) then
    do k=izs,ize
     do j=2,my-1
       do i=1,mx
       end do
     end do
     do i=1,mx
     end do
    end do
  end if


 if (mz .gt. 1) then
!$omp barrier
  do k=izs,ize
    if (k.eq.1) then
      do j=1,my
      do i=1,mx
      end do
      end do
    else if (k.eq.mz) then
      do j=1,my
      do i=1,mx
      end do
      end do
    else
      do j=1,my
      do i=1,mx
      end do
      end do
    end if
  end do
 end if

 do k=izs,ize
   do j=1,my
     do i=1,mx
       fp(i,j,k) = - max (0., fp(i,j,k))
     end do
   end do
 end do
!$omp end parallel

END SUBROUTINE
!***********************************************************************
SUBROUTINE dif2d_neg (fx, fy, fz, fp)
!
!  Finite difference one zone left to one zones right.
!

  USE params
  implicit none
  logical omp_in_parallel
  real, dimension(mx,my,mz):: fx, fy, fz, fp
  real, dimension(mx,my,1):: gz, hz
  integer i, j, k, km1, kp1
!+---------------------------------------------------------------------


!$omp parallel private(i,j,k,km1,kp1)

  if (mx.gt.1) then
    do k=izs,ize
      do j=1,my
        do i=2,mx-1
          fp(i,j,k) = - (fx(i-1 ,j,k)-fx(i+1,j,k))
        end do
        fp(1 ,j,k) = - (gx1(1   ,j,k)-fx(2  ,j,k))
        fp(mx,j,k) = - (fx(mx-1,j,k)-hx1(1  ,j,k))
      end do
    end do
  else
    do k=izs,ize
      do j=1,my
        do i=1,mx
          fp(i,j,k) = - 0.
        end do
      end do
    end do
  end if


  if (my .gt. 1) then
    do k=izs,ize
     do j=2,my-1
       do i=1,mx
         fp(i,j, k) = - fp(i,j, k) + (fy(i,j -1,k)-fy(i,j+1,k))
       end do
     end do
     do i=1,mx
         fp(i,1 ,k) = - fp(i,1 ,k) + (gy1(i,   1,k)-fy(i,  2,k))
         fp(i,my,k) = - fp(i,my,k) + (fy(i,my-1,k)-hy1(i,  1,k))
     end do
    end do
  end if


 if (mz .gt. 1) then
!$omp barrier
  do k=izs,ize
    if (k.eq.1) then
      do j=1,my
      do i=1,mx
        fp(i,j,k ) = - fp(i,j,k ) + (gz(i,j,  1)-fz(i,j,k+1))
      end do
      end do
    else if (k.eq.mz) then
      do j=1,my
      do i=1,mx
        fp(i,j,k ) = - fp(i,j,k ) + (fz(i,j,k-1)-hz(i,j,1))
      end do
      end do
    else
      do j=1,my
      do i=1,mx
        fp(i,j,k ) = - fp(i,j,k ) + (fz(i,j,k-1)-fz(i,j,k+1))
      end do
      end do
    end if
  end do
 end if

 do k=izs,ize
   do j=1,my
     do i=1,mx
       fp(i,j,k) = - max (0., fp(i,j,k))
     end do
   end do
 end do
!$omp end parallel

END SUBROUTINE

!***********************************************************************
SUBROUTINE dif1xy_neg (fx, fy, fp)
!
!  Finite difference curl one zone left to center
!
  USE params
  implicit none
  logical omp_in_parallel
  real, dimension(mx,my,mz):: fx, fy, fp
  real, dimension(mx):: cx
  real, dimension(my):: cy
  integer i, j, k, km1, kp1
!+---------------------------------------------------------------------
!$omp parallel private(i,j,k,km1,kp1)
  if (mx.gt.1) then
    do k=izs,ize
      do j=1,my
        do i=2,mx
          fp(i,j,k) = - cx(i)*(fy(i ,j,k)-fy(i-1,j,k))
        end do
        fp(1,j,k) = - cx(1)*(fy(1,j,k)-gx1(  1,j,k))
      end do
    end do
  else
    do k=izs,ize
      do j=1,my
        do i=1,mx
          fp(i,j,k) = - 0.
        end do
      end do
    end do
  end if
  if (my .gt. 1) then
    do k=izs,ize
     do j=2,my
       do i=1,mx
         fp(i,j, k) = - fp(i, j,k) - cy(j)*(fx(i, j,k)-fx(i,j-1,k))
       end do
     end do
     do i=1,mx
         fp(i,1,k) = - fp(i,1,k) - cy(1)*(fx(i,1,k)-gy1(i,  1,k))
     end do
    end do
  end if
  do k=izs,ize
    do j=1,my
      do i=1,mx
        fp(i,j,k) = - fp(i,j,k)**2
      end do
    end do
  end do
!$omp end parallel
END SUBROUTINE

!***********************************************************************
SUBROUTINE dif1yz_neg (fy, fz, fp)
!
!  Finite difference one zone left to center
!
  USE params
  implicit none
  logical omp_in_parallel
  real, dimension(mx,my,mz):: fy, fz, fp
  real, dimension(mx,my,1):: gz, hz
  real, dimension(my):: cy
  real, dimension(mz):: cz
  integer i, j, k, km1, kp1
!+---------------------------------------------------------------------
!$omp parallel private(i,j,k,km1,kp1)
  if (my .gt. 1) then
    do k=izs,ize
     do j=2,my
       do i=1,mx
         fp(i,j, k) = - cy(j)*(fz(i, j,k)-fz(i,j-1,k))
       end do
     end do
     do i=1,mx
         fp(i,1,k) = - cy(1)*(fz(i,1,k)-gy1(i,  1,k))
     end do
    end do
  end if
  if (mz .gt. 1) then
!$omp barrier
   do k=izs,ize
     if (k.eq.1) then
       do j=1,my
       do i=1,mx
         fp(i,j,k) = - fp(i,j,k) - cz(k)*(fy(i,j,k)-gz(i,j,1))
       end do
       end do
     else
       do j=1,my
       do i=1,mx
         fp(i,j,k) = - fp(i,j,k) - cz(k)*(fy(i,j,k)-fy(i,j,k-1))
       end do
       end do
     end if
   end do
  end if
  do k=izs,ize
    do j=1,my
      do i=1,mx
        fp(i,j,k) = - fp(i,j,k)**2
      end do
    end do
  end do
 !$omp end parallel
END SUBROUTINE

!***********************************************************************
SUBROUTINE dif1zx_neg (fz, fx, fp)
!
!  Finite difference one zone left to center
!
  USE params
  implicit none
  logical omp_in_parallel
  real, dimension(mx,my,mz):: fx, fz, fp
  real, dimension(mx,my,1):: gz, hz
  real, dimension(mx):: cx
  real, dimension(mz):: cz
  integer i, j, k, km1, kp1
!+---------------------------------------------------------------------
!$omp parallel private(i,j,k,km1,kp1)
  if (mx.gt.1) then
    do k=izs,ize
      do j=1,my
        do i=2,mx
          fp(i,j,k) = - cx(i)*(fz(i ,j,k)-fz(i-1,j,k))
        end do
        fp(1,j,k) = - cx(1)*(fz(1,j,k)-gx1(  1,j,k))
      end do
    end do
  else
    do k=izs,ize
      do j=1,my
        do i=1,mx
          fp(i,j,k) = - 0.
        end do
      end do
    end do
  end if
  if (mz .gt. 1) then
!$omp barrier
    do k=izs,ize
      if (k.eq.1) then
        do j=1,my
        do i=1,mx
          fp(i,j,k) = - fp(i,j,k) - cz(k)*(fx(i,j,k)-gz(i,j,1))
        end do
        end do
      else
        do j=1,my
        do i=1,mx
          fp(i,j,k) = - fp(i,j,k) - cz(k)*(fx(i,j,k)-fx(i,j,k-1))
        end do
        end do
      end if
    end do
   end if
   do k=izs,ize
     do j=1,my
       do i=1,mx
         fp(i,j,k) = - fp(i,j,k)**2
       end do
     end do
   end do
!$omp end parallel
END SUBROUTINE

!***********************************************************************
SUBROUTINE dif1_neg (fx, fy, fz, fp)
!
!  Finite difference one zone left to center
!

  USE params
  implicit none
  logical omp_in_parallel
  real, dimension(mx,my,mz):: fx, fy, fz, fp
  real, dimension(mx,my,1):: gz, hz
  real, dimension(mx):: cx
  real, dimension(my):: cy
  real, dimension(mz):: cz
  integer i, j, k, km1, kp1
!+---------------------------------------------------------------------



!$omp parallel private(i,j,k,km1,kp1)


  if (mx.gt.1) then
    do k=izs,ize
      do j=1,my
        do i=1,mx-1
          fp(i,j,k) = - cx(i)*(fx(i ,j,k)-fx(i+1,j,k))
        end do
        fp(mx,j,k) = - cx(mx)*(fx(mx,j,k)-hx1(  1,j,k))
      end do
    end do
  else
    do k=izs,ize
      do j=1,my
        do i=1,mx
          fp(i,j,k) = - 0.
        end do
      end do
    end do
  end if


  if (my .gt. 1) then
    do k=izs,ize
     do j=1,my-1
       do i=1,mx
       end do
     end do
     do i=1,mx
     end do
    end do
  end if


 if (mz .gt. 1) then
!$omp barrier
  do k=izs,ize
    if (k.eq.mz) then
      do j=1,my
      do i=1,mx
      end do
      end do
    else
      do j=1,my
      do i=1,mx
      end do
      end do
    end if
  end do
 end if

 do k=izs,ize
   do j=1,my
     do i=1,mx
       fp(i,j,k) = - max (0., fp(i,j,k))
     end do
   end do
 end do
!$omp end parallel

END SUBROUTINE

!***********************************************************************
SUBROUTINE dif1d_neg (fx, fy, fz, fp)
!
!  Finite difference one zone left to center
!

  USE params
  implicit none
  logical omp_in_parallel
  real, dimension(mx,my,mz):: fx, fy, fz, fp
  real, dimension(mx,my,1):: gz, hz
  integer i, j, k, km1, kp1
!+---------------------------------------------------------------------


!$omp parallel private(i,j,k,km1,kp1)


  if (mx.gt.1) then
    do k=izs,ize
      do j=1,my
        do i=1,mx-1
          fp(i,j,k) = - (fx(i ,j,k)-fx(i+1,j,k))
        end do
        fp(mx,j,k) = - (fx(mx,j,k)-hx1(  1,j,k))
      end do
    end do
  else
    do k=izs,ize
      do j=1,my
        do i=1,mx
          fp(i,j,k) = - 0.
        end do
      end do
    end do
  end if


  if (my .gt. 1) then
    do k=izs,ize
     do j=1,my-1
       do i=1,mx
         fp(i,j, k) = - fp(i, j,k) + (fy(i, j,k)-fy(i,j+1,k))
       end do
     end do
     do i=1,mx
         fp(i,my,k) = - fp(i,my,k) + (fy(i,my,k)-hy1(i,  1,k))
     end do
    end do
  end if


 if (mz .gt. 1) then
!$omp barrier
  do k=izs,ize
    if (k.eq.mz) then
      do j=1,my
      do i=1,mx
        fp(i,j,k) = - fp(i,j,k) + (fz(i,j,k)-hz(i,j,1))
      end do
      end do
    else
      do j=1,my
      do i=1,mx
        fp(i,j,k) = - fp(i,j,k) + (fz(i,j,k)-fz(i,j,k+1))
      end do
      end do
    end if
  end do
 end if

 do k=izs,ize
   do j=1,my
     do i=1,mx
       fp(i,j,k) = - max (0., fp(i,j,k))
     end do
   end do
 end do
!$omp end parallel

END SUBROUTINE

!***********************************************************************
SUBROUTINE xup_neg (f, fp)
!
!  f is centered on (i-.5,j,k), fifth order stagger
!
  use params, g=>gx2, h=>hx3
  implicit none
  logical omp_in_parallel
  real a, b, c
  integer i, j, k
  real, dimension(mx,my,mz):: f, fp
!+---------------------------------------------------------------------
!
!
  if (mx.le.5) then
    fp = - f
    return
  end if
  b = -1./16.
  a = .5-b

!$omp parallel private(i,j,k)


  do k=izs,ize
   do j=1,my
    fp(mx-2,j,k) = - ( &
                   a*(f(mx-2,j,k)+f(mx-1,j,k)) &
                 + b*(f(mx-3,j,k)+f(mx  ,j,k)))
    fp(mx-1,j,k) = - ( &
                   a*(f(mx-1,j,k)+f(mx  ,j,k)) &
                 + b*(f(mx-2,j,k)+h(1   ,j,k)))
    fp(mx  ,j,k) = - ( &
                   a*(f(mx  ,j,k)+h(1   ,j,k)) &
                 + b*(f(mx-1,j,k)+h(2   ,j,k)))
    fp(1   ,j,k) = - ( &
                   a*(f(1   ,j,k)+f(2   ,j,k)) &
                 + b*(g(2   ,j,k)+f(3   ,j,k)))
    fp(2   ,j,k) = - ( &
                   a*(f(2   ,j,k)+f(3   ,j,k)) &
                 + b*(f(1   ,j,k)+f(4   ,j,k)))
   end do
   do j=1,my
    do i=3,mx-3
      fp(i  ,j,k) = - ( &
                    a*(f(i  ,j,k)+f(i+1 ,j,k)) &
                  + b*(f(i-1,j,k)+f(i+2 ,j,k)))
    end do
   end do
  end do
!$omp end parallel
!
  nstag = nstag+1
  nflop = nflop+8
END SUBROUTINE

!***********************************************************************
SUBROUTINE xup1_neg (f, fp)
!
!  f is centered on (i-.5,j,k), first order stagger
!
  use params, h=>hx1
  implicit none
  logical omp_in_parallel
  real a, b, c
  integer i, j, k
  real, dimension(mx,my,mz):: f, fp
!+---------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = - f
    return
  end if
!
  a = .5

!$omp parallel private(i,j,k)

!
  do k=izs,ize
   do j=1,my
    fp(mx  ,j,k) = -  ( &
                    a*(f(mx,j,k)+h(  1,j,k)))
   end do
   do j=1,my
    do i=1,mx-1
      fp(i  ,j,k) = - ( &
                    a*(f(i ,j,k)+f(i+1,j,k)))
    end do
   end do
  end do
!$omp end parallel
!
  nstag1 = nstag1+1
  nflop = nflop+2
END SUBROUTINE

!***********************************************************************
SUBROUTINE xdn_neg (f, fp)
!
!  f is centered on (i,j,k), fifth order stagger
!
  use params, g=>gx3, h=>hx2
  implicit none
  logical omp_in_parallel
  real a, b, c
  integer i, j, k
  real, dimension(mx,my,mz):: f, fp
!+---------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = - f
    return
  end if
!
  b = -1./16.
  a = .5-b

!$omp parallel private(i,j,k)

!
  do k=izs,ize
   do j=1,my
    fp(mx-1,j,k) = - ( &
                   a*(f(mx-2,j,k)+f(mx-1,j,k)) &
                 + b*(f(mx-3,j,k)+f(mx  ,j,k)))
    fp(mx  ,j,k) = - ( &
                   a*(f(mx-1,j,k)+f(mx  ,j,k)) &
                 + b*(f(mx-2,j,k)+h(1   ,j,k)))
    fp(1  ,j,k) = - ( &
                   a*(g(3   ,j,k)+f(1   ,j,k)) &
                 + b*(g(2   ,j,k)+f(2   ,j,k)))
    fp(2  ,j,k) = - ( &
                   a*(f(1   ,j,k)+f(2   ,j,k)) &
                 + b*(g(3   ,j,k)+f(3   ,j,k)))
    fp(3  ,j,k) = - ( &
                   a*(f(2   ,j,k)+f(3   ,j,k)) &
                 + b*(f(1   ,j,k)+f(4   ,j,k)))
   end do
   do j=1,my
    do i=4,mx-2
      fp(i,j,k) = - ( &
                   a*(f(i   ,j,k)+f(i-1 ,j,k)) &
                 + b*(f(i+1 ,j,k)+f(i-2 ,j,k)))
    end do
   end do
  end do
!$omp end parallel
!
!
  nstag = nstag+1
  nflop = nflop+8
END SUBROUTINE

!***********************************************************************
SUBROUTINE xdn1_neg (f, fp)
!
!  f is centered on (i,j,k), first order stagger
!
  use params, g=>gx1
  implicit none
  logical omp_in_parallel
  real a, b, c
  integer i, j, k
  real, dimension(mx,my,mz):: f, fp
!+---------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = - f
    return
  end if
!
  a = 0.5

!$omp parallel private(i,j,k)

!
  do k=izs,ize
   do j=1,my
    fp(1  ,j,k) = - ( &
                   a*(g(1,j,k)+f(1   ,j,k)))
   end do
   do j=1,my
    do i=2,mx
      fp(i,j,k) = - ( &
                   a*(f(i,j,k)+f(i-1,j,k)))
    end do
   end do
  end do
!$omp end parallel
!
  nstag1 = nstag1+1
  nflop = nflop+2
END SUBROUTINE

!***********************************************************************
SUBROUTINE xup32_neg (f,j, fp)
!
!  f is centered on (i-.5,j,k), fifth order stagger
!
  use params, g=>gx2, h=>hx3
  implicit none
  logical omp_in_parallel
  real a, b, c
  integer i, j, k
  real, dimension(mx,my,mz):: f
  real, dimension(mx,mz):: fp
!+---------------------------------------------------------------------
!
!
  if (mx.le.5) then
    fp = - f(:,j,:)
    return
  end if
  b = -1./16.
  a = .5-b
!
!$omp parallel private(i,j,k)


  do k=izs,ize
    fp(mx-2,k) = - ( &
                   a*(f(mx-2,j,k)+f(mx-1,j,k)) &
                 + b*(f(mx-3,j,k)+f(mx  ,j,k)))
    fp(mx-1,k) = - ( &
                   a*(f(mx-1,j,k)+f(mx  ,j,k)) &
                 + b*(f(mx-2,j,k)+h(1   ,j,k)))
    fp(mx  ,k) = - ( &
                   a*(f(mx  ,j,k)+h(1   ,j,k)) &
                 + b*(f(mx-1,j,k)+h(2   ,j,k)))
    fp(1   ,k) = - ( &
                   a*(f(1   ,j,k)+f(2   ,j,k)) &
                 + b*(g(2   ,j,k)+f(3   ,j,k)))
    fp(2   ,k) = - ( &
                   a*(f(2   ,j,k)+f(3   ,j,k)) &
                 + b*(f(1   ,j,k)+f(4   ,j,k)))
    do i=3,mx-3
      fp(i  ,k) = - ( &
                    a*(f(i  ,j,k)+f(i+1 ,j,k)) &
                  + b*(f(i-1,j,k)+f(i+2 ,j,k)))
    end do
  end do
!$omp end parallel
!
  if (omp_master) then
      nstag = nstag+1
      nflop = nflop+8
  end if
END SUBROUTINE

!***********************************************************************
SUBROUTINE xup132_neg (f,j, fp)
!
!  f is centered on (i-.5,j,k), fifth order stagger
!
  use params, h=>hx1
  implicit none
  logical omp_in_parallel
  real a, b, c
  integer i, j, k
  real, dimension(mx,my,mz):: f
  real, dimension(mx,mz):: fp
!+---------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = - f(:,j,:)
    return
  end if
!
  a = .5
!
!$omp parallel private(i,j,k)


  do k=izs,ize
    fp(mx  ,k) = - ( &
                   a*(f(mx  ,j,k)+h(1   ,j,k)))
    do i=1,mx-1
      fp(i  ,k) = - ( &
                    a*(f(i  ,j,k)+f(i+1 ,j,k)))
    end do
  end do
!$omp end parallel
!
  if (omp_master) then
    nstag1 = nstag1+1
    nflop = nflop+2
  end if
END SUBROUTINE

!***********************************************************************
SUBROUTINE xdn32_neg (f,j, fp)
!
!  f is centered on (i,j,k), fifth order stagger
!
  use params, g=>gx3, h=>hx2
  implicit none
  logical omp_in_parallel
  real a, b, c
  integer i, j, k
  real, dimension(mx,my,mz):: f
  real, dimension(mx,mz):: fp
!+---------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = - f(:,j,:)
    return
  end if
!
  b = -1./16.
  a = .5-b
!
!$omp parallel private(i,j,k)


  do k=izs,ize
    fp(mx-1,k) = - ( &
                   a*(f(mx-2,j,k)+f(mx-1,j,k)) &
                 + b*(f(mx-3,j,k)+f(mx  ,j,k)))
    fp(mx  ,k) = - ( &
                   a*(f(mx-1,j,k)+f(mx  ,j,k)) &
                 + b*(f(mx-2,j,k)+h(1   ,j,k)))
    fp(1  ,k) = - ( &
                   a*(g(3   ,j,k)+f(1   ,j,k)) &
                 + b*(g(2   ,j,k)+f(2   ,j,k)))
    fp(2  ,k) = - ( &
                   a*(f(1   ,j,k)+f(2   ,j,k)) &
                 + b*(g(3   ,j,k)+f(3   ,j,k)))
    fp(3  ,k) = - ( &
                   a*(f(2   ,j,k)+f(3   ,j,k)) &
                 + b*(f(1   ,j,k)+f(4   ,j,k)))
    do i=4,mx-2
      fp(i,k) = - ( &
                   a*(f(i   ,j,k)+f(i-1 ,j,k)) &
                 + b*(f(i+1 ,j,k)+f(i-2 ,j,k)))
    end do
  end do
!$omp end parallel
!
!
  if (omp_master) then
    nstag = nstag+1
    nflop = nflop+8
  end if
END SUBROUTINE

!***********************************************************************
SUBROUTINE xdn132_neg (f,j, fp)
!
!  f is centered on (i,j,k), fifth order stagger
!
  use params, g=>gx1
  implicit none
  logical omp_in_parallel
  real a, b, c
  integer i, j, k
  real, dimension(mx,my,mz):: f
  real, dimension(mx,mz):: fp
!+---------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = - f(:,j,:)
    return
  end if
!
  a = 0.5
!
!$omp parallel private(i,j,k)


  do k=izs,ize
    fp(1  ,k) = - ( &
                   a*(g(1   ,j,k)+f(1   ,j,k)))
    do i=2,mx
      fp(i,k) = - ( &
                   a*(f(i   ,j,k)+f(i-1 ,j,k)))
    end do
  end do
!$omp end parallel
!
  if (omp_master) then
    nstag1 = nstag1+1
    nflop = nflop+2
  end if
END SUBROUTINE

!***********************************************************************
SUBROUTINE xup22_neg (f, fp)
!
!  f is centered on (i-.5,k), fifth order stagger
!
  use params, g=>gx2, h=> hx3
  implicit none
  logical omp_in_parallel
  real a, b, c
  integer i, j, k
  real, dimension(mx,mz):: fp, f
!+---------------------------------------------------------------------
!
!
  if (mx.le.5) then
    fp = - f
    return
  end if
  b = -1./16.
  a = .5-b
!
!$omp parallel private(i,k)


  do k=izs,ize
    fp(mx-2,k) = - ( &
                   a*(f(mx-2,k)+f(mx-1,k)) &
                 + b*(f(mx-3,k)+f(mx  ,k)))
    fp(mx-1,k) = - ( &
                   a*(f(mx-1,k)+f(mx  ,k)) &
                 + b*(f(mx-2,k)+h(1 ,1,k)))
    fp(mx  ,k) = - ( &
                   a*(f(mx  ,k)+h(1 ,1,k)) &
                 + b*(f(mx-1,k)+h(2 ,1,k)))
    fp(1   ,k) = - ( &
                   a*(f(1   ,k)+f(2   ,k)) &
                 + b*(g(2 ,1,k)+f(3   ,k)))
    fp(2   ,k) = - ( &
                   a*(f(2   ,k)+f(3   ,k)) &
                 + b*(f(1   ,k)+f(4   ,k)))
    do i=3,mx-3
      fp(i  ,k) = - ( &
                    a*(f(i  ,k)+f(i+1 ,k)) &
                  + b*(f(i-1,k)+f(i+2 ,k)))
    end do
  end do
!$omp end parallel
!
  if (omp_master) then
      nstag = nstag+1
      nflop = nflop+8
  end if
END SUBROUTINE

!***********************************************************************
SUBROUTINE xup122_neg (f, fp)
!
!  f is centered on (i-.5,k), fifth order stagger
!
  use params, h=> hx1
  implicit none
  logical omp_in_parallel
  real a, b, c
  integer i, j, k
  real, dimension(mx,mz):: fp, f
!+---------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = - f
    return
  end if
!
  a = .5
!
!$omp parallel private(i,k)


  do k=izs,ize
    fp(mx  ,k) = - ( &
                   a*(f(mx  ,k)+h(1 ,1,k)))
    do i=1,mx-1
      fp(i  ,k) = - ( &
                    a*(f(i  ,k)+f(i+1 ,k)))
    end do
  end do
!$omp end parallel
!
  if (omp_master) then
    nstag1 = nstag1+1
    nflop = nflop+2
  end if
END SUBROUTINE

!***********************************************************************
SUBROUTINE xdn22_neg (f, fp)
!
!  f is centered on (i,k), fifth order stagger
!
  use params, g=>gx3, h=>hx2
  implicit none
  logical omp_in_parallel
  real a, b, c
  integer i, j, k
  real, dimension(mx,mz):: fp, f
!+---------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = - f
    return
  end if
!
  b = -1./16.
  a = .5-b
!
!$omp parallel private(i,k)


  do k=izs,ize
    fp(mx-1,k) = - ( &
                   a*(f(mx-2,k)+f(mx-1,k)) &
                 + b*(f(mx-3,k)+f(mx  ,k)))
    fp(mx  ,k) = - ( &
                   a*(f(mx-1,k)+f(mx  ,k)) &
                 + b*(f(mx-2,k)+h(1 ,1,k)))
    fp(1  ,k) = - ( &
                   a*(g(3 ,1,k)+f(1   ,k)) &
                 + b*(g(2 ,1,k)+f(2   ,k)))
    fp(2  ,k) = - ( &
                   a*(f(1   ,k)+f(2   ,k)) &
                 + b*(g(3 ,1,k)+f(3   ,k)))
    fp(3  ,k) = - ( &
                   a*(f(2   ,k)+f(3   ,k)) &
                 + b*(f(1   ,k)+f(4   ,k)))
    do i=4,mx-2
      fp(i,k) = - ( &
                   a*(f(i   ,k)+f(i-1 ,k)) &
                 + b*(f(i+1 ,k)+f(i-2 ,k)))
    end do
  end do
!$omp end parallel
!
!
  if (omp_master) then
    nstag = nstag+1
    nflop = nflop+8
  end if
END SUBROUTINE

!***********************************************************************
SUBROUTINE xdn122_neg (f, fp)
!
!  f is centered on (i,k), fifth order stagger
!
  use params, g=>gx1
  implicit none
  logical omp_in_parallel
  real a, b, c
  integer i, j, k
  real, dimension(mx,mz):: fp, f
!+---------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = - f
    return
  end if
!
  a = 0.5
!
!$omp parallel private(i,k)


  do k=izs,ize
    fp(1  ,k) = - ( &
                   a*(g(1 ,1,k)+f(1   ,k)))
    do i=2,mx
      fp(i,k) = - ( &
                   a*(f(i   ,k)+f(i-1 ,k)))
    end do
  end do
!$omp end parallel
!
  if (omp_master) then
    nstag1 = nstag1+1
    nflop = nflop+2
  end if
END SUBROUTINE
!***********************************************************************
SUBROUTINE ddxupi_neg (f, fp)
  use params, g=>gx2, h=>hx3
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f, fp
!+---------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = - 0.
    return
  end if
!
  b = -1./24.
  a = (1.-3.*b)
  a1st = 1.

!
!$omp parallel private(i,j,k)
  do k=izs,ize
   do j=1,my
    fp(mx-2,j,k) = - ( &
                 + b*(f(mx  ,j,k)-f(mx-3,j,k)) &
                 + a*(f(mx-1,j,k)-f(mx-2,j,k)))
    fp(mx-1,j,k) = - ( &
                 + b*(h(1   ,j,k)-f(mx-2,j,k)) &
                 + a*(f(mx  ,j,k)-f(mx-1,j,k)))
    fp(mx  ,j,k) = - ( &
                 + b*(h(2   ,j,k)-f(mx-1,j,k)) &
                 + a*(h(1   ,j,k)-f(mx  ,j,k)))
    fp(1  ,j,k) = - ( &
                 + b*(f(3   ,j,k)-g(2   ,j,k)) &
                 + a*(f(2   ,j,k)-f(1   ,j,k)))
    fp(2  ,j,k) = - ( &
                 + b*(f(4   ,j,k)-f(1   ,j,k)) &
                 + a*(f(3   ,j,k)-f(2   ,j,k)))
   end do
   do j=1,my
    do i=3,mx-3
      fp(i  ,j,k) = - ( &
                 + b*(f(i+2 ,j,k)-f(i-1 ,j,k)) &
                 + a*(f(i+1 ,j,k)-f(i   ,j,k)))
    end do
  end do
  end do
!$omp end parallel
!
  nstag = nstag+1
  nflop = nflop+8
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddxupi1_neg (f, fp)
  use params, g=>gx1
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f, fp
!+---------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = - 0.
    return
  end if
  a = 1.

!$omp parallel private(i,j,k)

!
  do k=izs,ize
   do j=1,my
    fp(mx  ,j,k) = - ( &
                   a*(g(1  ,j,k)-f(mx,j,k)))
   end do
   do j=1,my
    do i=1,mx-1
      fp(i  ,j,k) = - ( &
                   a*(f(i+1,j,k)-f(i ,j,k)))
    end do
  end do
  end do
!$omp end parallel
!
  nstag1 = nstag1+1
  nflop = nflop+2
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddxdni_neg (f, fp)
  use params, g=>gx3, h=>hx2
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f, fp
!+---------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = - 0.
    return
  end if
!
  b = -1./24.
  a = (1.-3.*b)
  a1st = 1.

!$omp parallel private(i,j,k)

!
  do k=izs,ize
   do j=1,my
    fp(mx-1,j,k) = - ( &
                 + b*(f(mx  ,j,k)-f(mx-3,j,k)) &
                 + a*(f(mx-1,j,k)-f(mx-2,j,k)))
    fp(mx  ,j,k) = - ( &
                 + b*(h(1   ,j,k)-f(mx-2,j,k)) &
                 + a*(f(mx  ,j,k)-f(mx-1,j,k)))
    fp(1   ,j,k) = - ( &
                 + b*(f(2   ,j,k)-g(2   ,j,k)) &
                 + a*(f(1   ,j,k)-g(3   ,j,k)))
    fp(2  ,j,k) = - ( &
                 + b*(f(3   ,j,k)-g(3   ,j,k)) &
                 + a*(f(2   ,j,k)-f(1   ,j,k)))
    fp(3  ,j,k) = - ( &
                 + b*(f(4   ,j,k)-f(1   ,j,k)) &
                 + a*(f(3   ,j,k)-f(2   ,j,k)))
   end do
   do j=1,my
    do i=4,mx-2
      fp(i,j,k) = - ( &
                 + b*(f(i+1 ,j,k)-f(i-2 ,j,k)) &
                 + a*(f(i   ,j,k)-f(i-1 ,j,k)))
    end do
   end do
  end do
!$omp end parallel
!
  nstag = nstag+1
  nflop = nflop+8
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddxdni1_neg (f, fp)
  use params, g=>gx1
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f, fp
!+---------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = - 0.
    return
  end if
  a = 1.

!$omp parallel private(i,j,k)

!
  do k=izs,ize
   do j=1,my
    fp(1   ,j,k) = - ( &
                   a*(f(1,j,k)-g(1  ,j,k)))
   end do
   do j=1,my
    do i=2,mx
      fp(i,j,k) = - ( &
                   a*(f(i,j,k)-f(i-1,j,k)))
    end do
   end do
  end do
!$omp end parallel
!
  nstag1 = nstag1+1
  nflop = nflop+2
END SUBROUTINE
!***********************************************************************
SUBROUTINE ddxup_neg (f, fp)
  use params, h=>hx3, g=>gx2
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f, fp
!+---------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = - 0.
    return
  end if
!
  b = -1./24.
  a = (1.-3.*b)/dx
  a1st = 1./dx
  b = b/dx

!$omp parallel private(i,j,k)
!
  do k=izs,ize
   do j=1,my
    fp(mx-2,j,k) = - ( &
                 + b*(f(mx  ,j,k)-f(mx-3,j,k)) &
                 + a*(f(mx-1,j,k)-f(mx-2,j,k)))
    fp(mx-1,j,k) = - ( &
                 + b*(h(1   ,j,k)-f(mx-2,j,k)) &
                 + a*(f(mx  ,j,k)-f(mx-1,j,k)))
    fp(mx  ,j,k) = - ( &
                 + b*(h(2   ,j,k)-f(mx-1,j,k)) &
                 + a*(h(1   ,j,k)-f(mx  ,j,k)))
    fp(1  ,j,k) = - ( &
                 + b*(f(3   ,j,k)-g(2   ,j,k)) &
                 + a*(f(2   ,j,k)-f(1   ,j,k)))
    fp(2  ,j,k) = - ( &
                 + b*(f(4   ,j,k)-f(1   ,j,k)) &
                 + a*(f(3   ,j,k)-f(2   ,j,k)))
   end do
   do j=1,my
    do i=3,mx-3
      fp(i  ,j,k) = - ( &
                 + b*(f(i+2 ,j,k)-f(i-1 ,j,k)) &
                 + a*(f(i+1 ,j,k)-f(i   ,j,k)))
    end do
  end do
  end do
!$omp end parallel
!
  nstag = nstag+1
  nflop = nflop+8
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddxup1_neg (f, fp)
  use params, g=>gx1
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f, fp
!+---------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = - 0.
    return
  end if
  a = 1./dx

!$omp parallel private(i,j,k)
!
  do k=izs,ize
   do j=1,my
    fp(mx  ,j,k) = - ( &
                   a*(g(1  ,j,k)-f(mx,j,k)))
   end do
   do j=1,my
    do i=1,mx-1
      fp(i  ,j,k) = - ( &
                   a*(f(i+1,j,k)-f(i ,j,k)))
    end do
  end do
  end do
!$omp end parallel
!
  nstag1 = nstag1+1
  nflop = nflop+2
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddxdn_neg (f, fp)
  use params, g=>gx3, h=>hx2
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f, fp
!+---------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = - 0.
    return
  end if
!
  b = -1./24.
  a = (1.-3.*b)/dx
  a1st = 1./dx
  b = b/dx

!$omp parallel private(i,j,k)
!
  do k=izs,ize
   do j=1,my
    fp(mx-1,j,k) = - ( &
                 + b*(f(mx  ,j,k)-f(mx-3,j,k)) &
                 + a*(f(mx-1,j,k)-f(mx-2,j,k)))
    fp(mx  ,j,k) = - ( &
                 + b*(h(1   ,j,k)-f(mx-2,j,k)) &
                 + a*(f(mx  ,j,k)-f(mx-1,j,k)))
    fp(1   ,j,k) = - ( &
                 + b*(f(2   ,j,k)-g(2   ,j,k)) &
                 + a*(f(1   ,j,k)-g(3   ,j,k)))
    fp(2  ,j,k) = - ( &
                 + b*(f(3   ,j,k)-g(3   ,j,k)) &
                 + a*(f(2   ,j,k)-f(1   ,j,k)))
    fp(3  ,j,k) = - ( &
                 + b*(f(4   ,j,k)-f(1   ,j,k)) &
                 + a*(f(3   ,j,k)-f(2   ,j,k)))
   end do
   do j=1,my
    do i=4,mx-2
      fp(i,j,k) = - ( &
                 + b*(f(i+1 ,j,k)-f(i-2 ,j,k)) &
                 + a*(f(i   ,j,k)-f(i-1 ,j,k)))
    end do
   end do
  end do
!$omp end parallel
!
  nstag = nstag+1
  nflop = nflop+8
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddxdn1_neg (f, fp)
  use params, g=>gx1
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f, fp
!+---------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = - 0.
    return
  end if
  a = 1./dx

!$omp parallel private(i,j,k)
!
  do k=izs,ize
   do j=1,my
    fp(1   ,j,k) = - ( &
                   a*(f(1,j,k)-g(1  ,j,k)))
   end do
   do j=1,my
    do i=2,mx
      fp(i,j,k) = - ( &
                   a*(f(i,j,k)-f(i-1,j,k)))
    end do
   end do
  end do
!$omp end parallel
!
  nstag1 = nstag1+1
  nflop = nflop+2
END SUBROUTINE
!***********************************************************************
SUBROUTINE yup_neg (f, fp)
!
!  f is centered on (i,j-.5,k), fifth order stagger
!
  use params, g=>gy2, h=>gy3
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k
  real, intent(in) , dimension(mx,my,mz):: f
  real,              dimension(mx,my,mz):: fp
!+---------------------------------------------------------------------

  if (my .lt. 5) then
    !$omp parallel private(k)
    do k=izs,ize
      fp(:,:,k) = - f(:,:,k)
    end do
    !$omp end parallel
    return
  end if
  b = -1./16.
  a = .5-b
  a1st = 0.5


  !$omp parallel private(i,j,k)
  do k=izs,ize
    do j=3,my-3
     do i=1,mx
      fp(i,j  ,k) = - ( &
                     a*(f(i,j   ,k)+f(i,j+1 ,k)) + &
                     b*(f(i,j-1 ,k)+f(i,j+2 ,k)))
     end do
    end do
    do i=1,mx
      fp(i,my-2,k) = - ( &
                     a*(f(i,my-2,k)+f(i,my-1,k)) + &
                     b*(f(i,my-3,k)+f(i,my  ,k)))
      fp(i,my-1,k) = - ( &
                     a*(f(i,my-1,k)+f(i,my  ,k)) + &
                     b*(f(i,my-2,k)+h(i,1   ,k)))
      fp(i,my  ,k) = - ( &
                     a*(f(i,my  ,k)+h(i,1   ,k)) + &
                     b*(f(i,my-1,k)+h(i,2   ,k)))
      fp(i,1   ,k) = - ( &
                     a*(f(i,1   ,k)+f(i,2   ,k)) + &
                     b*(g(i,2   ,k)+f(i,3   ,k)))
      fp(i,2   ,k) = - ( &
                     a*(f(i,2   ,k)+f(i,3   ,k)) + &
                     b*(f(i,1   ,k)+f(i,4   ,k)))
    end do
  end do
  !$omp end parallel

  !$omp master
  nstag = nstag+1
  nflop = nflop+8
  !$omp end master
END SUBROUTINE

!***********************************************************************
SUBROUTINE yup1_neg (f, fp)
!
!  f is centered on (i,j-.5,k), fifth order stagger
!
  use params, h=>gy1
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k
  real, intent(in) , dimension(mx,my,mz):: f
  real,              dimension(mx,my,mz):: fp
!+---------------------------------------------------------------------
!
  if (my .lt. 5) then
    !$omp parallel private(k)
    do k=izs,ize
      fp(:,:,k) = - f(:,:,k)
    end do
    !$omp end parallel
    return
  end if

  a = 0.5


  !$omp parallel private(i,j,k)
  do k=izs,ize
    do j=1,my-1
      do i=1,mx
        fp(i,j  ,k) = - ( &
                     a*(f(i,j   ,k)+f(i,j+1 ,k)))
      end do
    end do
    do i=1,mx
      fp(i,my,k) = - ( &
                     a*(h(i,1,k)+f(i,my,k)))
    end do
  end do
  !$omp end parallel

  !$omp master
  nstag1 = nstag1+1
  nflop = nflop+2
  !$omp end master
END SUBROUTINE

!***********************************************************************
SUBROUTINE ydn_neg (f, fp)
!
!  f is centered on (i,j,k), fifth order stagger
!
  use params, g=>gy3, h=>gy2
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f, fp
!+---------------------------------------------------------------------

  if (my .lt. 5) then
    !$omp parallel private(k)
    do k=izs,ize
      fp(:,:,k) = - f(:,:,k)
    end do
    !$omp end parallel
    return
  end if

  b = -1./16.
  a = .5-b
  a1st = 0.5


  !$omp parallel private(i,j,k)
  do k=izs,ize
    do j=4,my-2
     do i=1,mx
      fp(i,j,k) = - ( &
                     a*(f(i,j   ,k)+f(i,j-1 ,k)) + &
                     b*(f(i,j+1 ,k)+f(i,j-2 ,k)))
     end do
    end do
    do i=1,mx
      fp(i,my-1,k) = - ( &
                     a*(f(i,my-2,k)+f(i,my-1,k)) + &
                     b*(f(i,my-3,k)+f(i,my  ,k)))
      fp(i,my  ,k) = - ( &
                     a*(f(i,my-1,k)+f(i,my  ,k)) + &
                     b*(f(i,my-2,k)+h(i,1   ,k)))
      fp(i,1   ,k) = - ( &
                     a*(g(i,3   ,k)+f(i,1   ,k)) + &
                     b*(g(i,2   ,k)+f(i,2   ,k)))
      fp(i,2   ,k) = - ( &
                     a*(f(i,1   ,k)+f(i,2   ,k)) + &
                     b*(g(i,3   ,k)+f(i,3   ,k)))
      fp(i,3   ,k) = - ( &
                     a*(f(i,2   ,k)+f(i,3   ,k)) + &
                     b*(f(i,1   ,k)+f(i,4   ,k)))
    end do
  end do
  !$omp end parallel

  !$omp master
  nstag = nstag+1
  nflop = nflop+8
  !$omp end master
END SUBROUTINE

!***********************************************************************
SUBROUTINE ydn1_neg (f, fp)
!
!  f is centered on (i,j,k), fifth order stagger
!
  use params, g=>gy1
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f, fp
!+---------------------------------------------------------------------
  if (my .lt. 5) then
    !$omp parallel private(k)
    do k=izs,ize
      fp(:,:,k) = - f(:,:,k)
    end do
    !$omp end parallel
    return
  end if

  a = 0.5


  !$omp parallel private(i,j,k)
  do k=izs,ize
    do j=2,my
     do i=1,mx
      fp(i,j,k) = - ( &
                     a*(f(i,j   ,k)+f(i,j-1 ,k)))
     end do
    end do
    do i=1,mx
      fp(i,1   ,k) = - ( &
                     a*(g(i,1   ,k)+f(i,1   ,k)))
    end do
  end do
  !$omp end parallel

  !$omp master
  nstag1 = nstag1+1
  nflop = nflop+2
  !$omp end master
END SUBROUTINE
!***********************************************************************
SUBROUTINE ddyupi_neg (f, fp)
  use params, g=>gy2, h=>gy3
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f, fp
!+---------------------------------------------------------------------
!
  if (my .lt. 3) then
    fp = - 0.
    return
  end if
  b = -1./24.
  a = (1.-3.*b)
  a1st = 1.


  do k=izs,ize
    do i=1,mx
      fp(i,my-2,k) = - ( &
                     a*(f(i,my-1,k)-f(i,my-2,k)) + &
                     b*(f(i,my  ,k)-f(i,my-3,k)))
      fp(i,my-1,k) = - ( &
                     a*(f(i,my  ,k)-f(i,my-1,k)) + &
                     b*(h(i,1   ,k)-f(i,my-2,k)))
      fp(i,my  ,k) = - ( &
                     a*(h(i,1   ,k)-f(i,my  ,k)) + &
                     b*(h(i,2   ,k)-f(i,my-1,k)))
      fp(i,1   ,k) = - ( &
                     a*(f(i,2   ,k)-f(i,1   ,k)) + &
                     b*(f(i,3   ,k)-g(i,2   ,k)))
      fp(i,2   ,k) = - ( &
                     a*(f(i,3   ,k)-f(i,2   ,k)) + &
                     b*(f(i,4   ,k)-f(i,1   ,k)))
    end do
    do j=3,my-3
     do i=1,mx
      fp(i,j  ,k) = - ( &
                     a*(f(i,j+1 ,k)-f(i,j   ,k)) + &
                     b*(f(i,j+2 ,k)-f(i,j-1 ,k)))
     end do
    end do
  end do

  !$omp master
  nstag = nstag+1
  nflop = nflop+8
  !$omp end master
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddyupi1_neg (f, fp)
  use params, g=>gy1
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f, fp
!+---------------------------------------------------------------------
!
  if (my .lt. 3) then
    fp = - 0.
    return
  end if
  a = 1.


  do k=izs,ize
    do i=1,mx
      fp(i,my,k) = - ( &
                     a*(g(i,1  ,k)-f(i,my,k)))
    end do
    do j=1,my-1
      do i=1,mx
        fp(i,j  ,k) = - ( &
                     a*(f(i,j+1 ,k)-f(i,j   ,k)))
     end do
    end do
  end do

  !$omp master
  nstag1 = nstag1+1
  nflop = nflop+2
  !$omp end master
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddydni_neg (f, fp)
  use params, g=>gy3, h=>gy2
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f, fp
!+---------------------------------------------------------------------
!
  if (my .lt. 3) then
    fp = - 0.
    return
  end if
  b = -1./24.
  a = (1.-3.*b)
  a1st = 1.


  do k=izs,ize
    do i=1,mx
      fp(i,my-1,k) = - ( &
                     a*(f(i,my-1,k)-f(i,my-2,k)) + &
                     b*(f(i,my  ,k)-f(i,my-3,k)))
      fp(i,my  ,k) = - ( &
                     a*(f(i,my  ,k)-f(i,my-1,k)) + &
                     b*(h(i,1   ,k)-f(i,my-2,k)))
      fp(i,1  ,k) = - ( &
                     a*(f(i,1   ,k)-g(i,3   ,k)) + &
                     b*(f(i,2   ,k)-g(i,2   ,k)))
      fp(i,2   ,k) = - ( &
                     a*(f(i,2   ,k)-f(i,1   ,k)) + &
                     b*(f(i,3   ,k)-g(i,3   ,k)))
      fp(i,3   ,k) = - ( &
                     a*(f(i,3   ,k)-f(i,2   ,k)) + &
                     b*(f(i,4   ,k)-f(i,1   ,k)))
    end do
    do j=4,my-2
     do i=1,mx
      fp(i,j ,k) = - ( &
                     a*(f(i,j   ,k)-f(i,j-1 ,k)) + &
                     b*(f(i,j+1 ,k)-f(i,j-2 ,k)))
     end do
    end do
  end do

  !$omp master
  nstag = nstag+1
  nflop = nflop+8
  !$omp end master
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddydni1_neg (f, fp)
  use params, g=>gy1
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f, fp
!+---------------------------------------------------------------------
!
  if (my .lt. 3) then
    fp = - 0.
    return
  end if
  a = 1.


  do k=izs,ize
    do i=1,mx
      fp(i,1  ,k) = - ( &
                     a*(f(i,1   ,k)-g(i,1   ,k)))
    end do
    do j=2,my
     do i=1,mx
      fp(i,j ,k) = - ( &
                     a*(f(i,j   ,k)-f(i,j-1 ,k)))
     end do
    end do
  end do

  !$omp master
  nstag1 = nstag1+1
  nflop = nflop+2
  !$omp end master
END SUBROUTINE
!***********************************************************************
SUBROUTINE ddyup_neg (f, fp)
  use params, g=>gy2, h=>gy3
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f, fp
!+---------------------------------------------------------------------

  if (my .lt. 3) then
    fp = - 0.
    return
  end if
  b = -1./24.
  a = (1.-3.*b)/dy
  a1st = 1./dy
  b = b/dy


  !$omp parallel private(i,j,k)
  do k=izs,ize
    do i=1,mx
      fp(i,my-2,k) = - ( &
                     b*(f(i,my  ,k)-f(i,my-3,k)) + &
                     a*(f(i,my-1,k)-f(i,my-2,k)) )
      fp(i,my-1,k) = - ( &
                     b*(h(i,1   ,k)-f(i,my-2,k)) + &
                     a*(f(i,my  ,k)-f(i,my-1,k)) )
      fp(i,my  ,k) = - ( &
                     b*(h(i,2   ,k)-f(i,my-1,k)) + &
                     a*(h(i,1   ,k)-f(i,my  ,k)) )
      fp(i,1   ,k) = - ( &
                     b*(f(i,3   ,k)-g(i,2   ,k)) + &
                     a*(f(i,2   ,k)-f(i,1   ,k)) )
      fp(i,2   ,k) = - ( &
                     b*(f(i,4   ,k)-f(i,1   ,k)) + &
                     a*(f(i,3   ,k)-f(i,2   ,k)) )
    end do
    do j=3,my-3
     do i=1,mx
      fp(i,j  ,k) = - ( &
                     b*(f(i,j+2 ,k)-f(i,j-1 ,k)) + &
                     a*(f(i,j+1 ,k)-f(i,j   ,k)) )
     end do
    end do
  end do
  !$omp end parallel

  !$omp master
  nstag = nstag+1
  nflop = nflop+8
  !$omp end master
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddyup1_neg (f, fp)
  use params, g=>gy1
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f, fp
!+---------------------------------------------------------------------

  if (my .lt. 3) then
    fp = - 0.
    return
  end if
  a = 1./dy


  !$omp parallel private(i,j,k)
  do k=izs,ize
    do i=1,mx
      fp(i,my,k) = - ( &
                     a*(g(i,1  ,k)-f(i,my,k)))
    end do
    do j=1,my-1
      do i=1,mx
        fp(i,j  ,k) = - ( &
                     a*(f(i,j+1 ,k)-f(i,j   ,k)))
     end do
    end do
  end do
  !$omp end parallel

  !$omp master
  nstag1 = nstag1+1
  nflop = nflop+2
  !$omp end master
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddydn_neg (f, fp)
  use params, g=>gy3, h=>gy2
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f, fp
!+---------------------------------------------------------------------

  if (my .lt. 3) then
    fp = - 0.
    return
  end if
  b = -1./24.
  a = (1.-3.*b)/dy
  a1st = 1./dy
  b = b/dy


  !$omp parallel private(i,j,k)
  do k=izs,ize
    do i=1,mx
      fp(i,my-1,k) = - ( &
                     b*(f(i,my  ,k)-f(i,my-3,k)) + &
                     a*(f(i,my-1,k)-f(i,my-2,k)) ) 
      fp(i,my  ,k) = - ( &
                     b*(h(i,1   ,k)-f(i,my-2,k)) + &
                     a*(f(i,my  ,k)-f(i,my-1,k)) )
      fp(i,1  ,k) = - ( &
                     b*(f(i,2   ,k)-g(i,2   ,k)) + &
                     a*(f(i,1   ,k)-g(i,3   ,k)) )
      fp(i,2   ,k) = - ( &
                     b*(f(i,3   ,k)-g(i,3   ,k)) + &
                     a*(f(i,2   ,k)-f(i,1   ,k)) )
      fp(i,3   ,k) = - ( &
                     b*(f(i,4   ,k)-f(i,1   ,k)) + &
                     a*(f(i,3   ,k)-f(i,2   ,k)) )
    end do
    do j=4,my-2
     do i=1,mx
      fp(i,j ,k) = - ( &
                     b*(f(i,j+1 ,k)-f(i,j-2 ,k)) + &
                     a*(f(i,j   ,k)-f(i,j-1 ,k)) )
     end do
    end do
  end do
  !$omp end parallel

  !$omp master
  nstag = nstag+1
  nflop = nflop+8
  !$omp end master
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddydn1_neg (f, fp)
  use params, g=>gy1
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f, fp
!+---------------------------------------------------------------------

  if (my .lt. 3) then
    fp = - 0.
    return
  end if
  a = 1./dy


  !$omp parallel private(i,j,k)
  do k=izs,ize
    do i=1,mx
      fp(i,1  ,k) = - ( &
                     a*(f(i,1   ,k)-g(i,1   ,k)))
    end do
    do j=2,my
     do i=1,mx
      fp(i,j ,k) = - ( &
                     a*(f(i,j   ,k)-f(i,j-1 ,k)))
     end do
    end do
  end do
  !$omp end parallel

  !$omp master
  nstag1 = nstag1+1
  nflop = nflop+2
  !$omp end master
END SUBROUTINE
!***********************************************************************
SUBROUTINE zup_neg (f, fp)
!
!  f is centered on (i,j,k-.5), fifth order stagger
!
  use params
  implicit none
  logical omp_in_parallel
  real a, b, c
  integer i, j, k
  real, intent(in) , dimension(mx,my,mz):: f
  real,              dimension(mx,my,mz):: fp
  real,              dimension(mx,my, 2):: g
  real,              dimension(mx,my, 3):: h
!+---------------------------------------------------------------------
!

  if (mz .lt. 5) then
    !$omp parallel private(k)
    do k=izs,ize
      fp(:,:,k) = - f(:,:,k)
    end do
    !$omp end parallel
    return
  end if
  b = -1./16.
  a = .5-b

!
  !$omp parallel private(i,j,k)
  do k=max(3,izs),min(mz-3,ize)
    do j=1,my
      do i=1,mx
        fp(i,j,k) = - ( &
                     a*(f(i,j,k   )+f(i,j,k+1 )) + &
                     b*(f(i,j,k-1 )+f(i,j,k+2 )))
     end do
    end do
  end do
  if (ize.eq.mz) then
   do j=1,my
    do i=1,mx
      fp(i,j,mz-2) = - ( &
                     a*(f(i,j,mz-2)+f(i,j,mz-1)) + &
                     b*(f(i,j,mz-3)+f(i,j,mz  )))
      fp(i,j,mz-1) = - ( &
                     a*(f(i,j,mz-1)+f(i,j,mz  )) + &
                     b*(f(i,j,mz-2)+h(i,j,1   )))
      fp(i,j,mz  ) = - ( &
                     a*(f(i,j,mz  )+h(i,j,1   )) + &
                     b*(f(i,j,mz-1)+h(i,j,2   )))
    end do
   end do
  end if
  if (izs.eq.1) then
   do j=1,my
    do i=1,mx
      fp(i,j,1   ) = - ( &
                     a*(f(i,j,1   )+f(i,j,2   )) + &
                     b*(g(i,j,2   )+f(i,j,3   )))
      fp(i,j,2   ) = - ( &
                     a*(f(i,j,2   )+f(i,j,3   )) + &
                     b*(f(i,j,1   )+f(i,j,4   )))
    end do
   end do
  end if
  !$omp end parallel
!
  nstag = nstag+1
  nflop = nflop+8
END SUBROUTINE

!***********************************************************************
SUBROUTINE zup1_neg (f, fp)
!
!  f is centered on (i,j,k-.5), fifth order stagger
!
  use params
  implicit none
  logical omp_in_parallel
  real a, b, c
  integer i, j, k
  real, intent(in) , dimension(mx,my,mz):: f
  real,              dimension(mx,my,mz):: fp
  real,              dimension(mx,my,1 ):: h
!+---------------------------------------------------------------------
!
  if (mz .lt. 5) then
    !$omp parallel private(k)
    do k=izs,ize
      fp(:,:,k) = - f(:,:,k)
    end do
    !$omp end parallel
    return
  end if
!
  a = 0.5
!
  !$omp parallel private(i,j,k)
  do k=izs,min(mz-1,ize)
    do j=1,my
      do i=1,mx
        fp(i,j,k  ) = - ( &
                     a*(f(i,j,k   )+f(i,j,k+1 )))
      end do
    end do
  end do
  if (ize.eq.mz) then
   do j=1,my
    do i=1,mx
      fp(i,j,mz) = - ( &
                     a*(h(i,j,1)+f(i,j,mz)))
    end do
   end do
  end if
  !$omp end parallel
!
  nstag1 = nstag1+1
  nflop = nflop+2
END SUBROUTINE

!***********************************************************************
SUBROUTINE zdn_neg (f, fp)
!
!  f is centered on (i,j), fifth order stagger
!
  use params
  implicit none
  logical omp_in_parallel
  real a, b, c
  integer i, j, k
  real, dimension(mx,my,mz):: f, fp
  real, dimension(mx,my, 3):: g
  real, dimension(mx,my, 2):: h
!+---------------------------------------------------------------------
!
  if (mz .lt. 5) then
    !$omp parallel private(k)
    do k=izs,ize
      fp(:,:,k) = - f(:,:,k)
    end do
    !$omp end parallel
    return
  end if
  b = -1./16.
  a = .5-b

!
  !$omp parallel private(i,j,k)
  do k=max(4,izs),min(mz-2,ize)
    do j=1,my
     do i=1,mx
      fp(i,j,k) = - ( &
                     a*(f(i,j,k   )+f(i,j,k-1 )) + &
                     b*(f(i,j,k+1 )+f(i,j,k-2 )))
     end do
    end do
  end do
  if (ize.eq.mz) then
   do j=1,my
    do i=1,mx
      fp(i,j,mz-1) = - ( &
                     a*(f(i,j,mz-2)+f(i,j,mz-1)) + &
                     b*(f(i,j,mz-3)+f(i,j,mz  )))
      fp(i,j,mz  ) = - ( &
                     a*(f(i,j,mz-1)+f(i,j,mz  )) + &
                     b*(f(i,j,mz-2)+h(i,j,1   )))
    end do
   end do
  end if
  if (izs.eq.1) then
   do j=1,my
    do i=1,mx
      fp(i,j,1   ) = - ( &
                     a*(g(i,j,3   )+f(i,j,1   )) + &
                     b*(g(i,j,2   )+f(i,j,2   )))
      fp(i,j,2   ) = - ( &
                     a*(f(i,j,1   )+f(i,j,2   )) + &
                     b*(g(i,j,3   )+f(i,j,3   )))
      fp(i,j,3   ) = - ( &
                     a*(f(i,j,2   )+f(i,j,3   )) + &
                     b*(f(i,j,1   )+f(i,j,4   )))
    end do
   end do
  end if
  !$omp end parallel
!
  nstag = nstag+1
  nflop = nflop+8
END SUBROUTINE

!***********************************************************************
SUBROUTINE zdn1_neg (f, fp)
!
!  f is centered on (i,j), fifth order stagger
!
  use params
  implicit none
  logical omp_in_parallel
  real a, b, c
  integer i, j, k
  real, dimension(mx,my,mz):: f, fp
  real, dimension(mx,my,1 ):: g
!+---------------------------------------------------------------------
!
  if (mz .lt. 5) then
    !$omp parallel private(k)
    do k=izs,ize
      fp(:,:,k) = - f(:,:,k)
    end do
    !$omp end parallel
    return
  end if
!
  a = 0.5
!
  !$omp parallel private(i,j,k)
  do k=max(2,izs),ize
    do j=1,my
     do i=1,mx
      fp(i,j,k) = - ( &
                     a*(f(i,j,k   )+f(i,j,k-1 )))
     end do
    end do
  end do
  if (izs.eq.1) then
   do j=1,my
    do i=1,mx
      fp(i,j,1   ) = - ( &
                     a*(g(i,j,1   )+f(i,j,1   )))
    end do
   end do
  end if
  !$omp end parallel
!
  nstag1 = nstag1+1
  nflop = nflop+2
END SUBROUTINE

!***********************************************************************
SUBROUTINE zup32_neg (f,j, fp)
!
!  f is centered on (i,j,k-.5), fifth order stagger
!
  use params
  implicit none
  logical omp_in_parallel
  real a, b, c
  integer i, j, k
  real,             dimension(mx,   mz):: fp
  real, intent(in), dimension(mx,my,mz):: f
  real,             dimension(mx,my, 2):: g
  real,             dimension(mx,my, 3):: h
!+---------------------------------------------------------------------
!
!
  if (mz.le.5) then
    !$omp parallel private(k)
    do k=izs,ize
      fp(:,k) = - f(:,j,k)
    end do
    !$omp end parallel
    return
  end if

  b = -1./16.
  a = .5-b

!
!$omp parallel private(i,j,k)
  do k=max(3,izs),min(mz-3,ize)
      do i=1,mx
        fp(i,k) = - ( &
                     a*(f(i,j,k+1)+f(i,j,k  )) + &
                     b*(f(i,j,k+2)+f(i,j,k-1)))
      end do
  end do
  if (izs.eq.1) then
    do i=1,mx
      fp(i,1   ) = - ( &
                     a*(f(i,j,1   )+f(i,j,2   )) + &
                     b*(g(i,j,2   )+f(i,j,3   )))
      fp(i,2   ) = - ( &
                     a*(f(i,j,2   )+f(i,j,3   )) + &
                     b*(f(i,j,1   )+f(i,j,4   )))
    end do
  end if
  if (ize.eq.mz) then
    do i=1,mx
      fp(i,mz-2) = - ( &
                     a*(f(i,j,mz-2)+f(i,j,mz-1)) + &
                     b*(f(i,j,mz-3)+f(i,j,mz  )))
      fp(i,mz-1) = - ( &
                     a*(f(i,j,mz-1)+f(i,j,mz  )) + &
                     b*(f(i,j,mz-2)+h(i,j,1   )))
      fp(i,mz  ) = - ( &
                     a*(f(i,j,mz  )+h(i,j,1   )) + &
                     b*(f(i,j,mz-1)+h(i,j,2   )))
    end do
  end if

!$omp end parallel
!
  if (omp_master) then
    nstag = nstag+1
    nflop = nflop+8
  end if
END SUBROUTINE

!***********************************************************************
SUBROUTINE zup132_neg (f,j, fp)
!
!  f is centered on (i,j,k-.5), fifth order stagger
!
  use params
  implicit none
  logical omp_in_parallel
  real a, b, c
  integer i, j, k
  real,              dimension(mx,   mz):: fp
  real, intent(in),  dimension(mx,my,mz):: f
  real,              dimension(mx,my,1 ):: h
!+---------------------------------------------------------------------
!
  if (mz.le.5) then
    !$omp parallel private(k)
    do k=izs,ize
      fp(:,k) = - f(:,j,k)
    end do
    !$omp end parallel
    return
  end if

  a = .5
!
!$omp parallel private(i,j,k)
  do k=izs,min(mz-1,ize)
      do i=1,mx
        fp(i,k) = - ( &
                     a*(f(i,j,k+1)+f(i,j,k  )))
      end do
  end do
  if (ize.eq.mz) then
    do i=1,mx
      fp(i,mz) = - ( &
                     a*(h(i,j,1)+f(i,j,mz)))
    end do
  end if

!$omp end parallel
!
  if (omp_master) then
    nstag1 = nstag1+1
    nflop = nflop+2
  end if
END SUBROUTINE

!***********************************************************************
SUBROUTINE zdn32_neg (f,j, fp)
!
!  f is centered on (i,j,k), fifth order stagger
!
  use params
  implicit none
  logical omp_in_parallel
  real a, b, c
  integer i, j, k
  real,             dimension(mx,   mz):: fp
  real, intent(in), dimension(mx,my,mz):: f
  real,             dimension(mx,my, 3):: g
  real,             dimension(mx,my, 2):: h

!+---------------------------------------------------------------------
!
  if (mz.le.5) then
    !$omp parallel private(k)
    do k=izs,ize
      fp(:,k) = - f(:,j,k)
    end do
    !$omp end parallel
    return
  end if

  b = -1./16.
  a = .5-b

!
!$omp parallel private(i,j,k)
  do k=max(4,izs),min(mz-2,ize)
      do i=1,mx
        fp(i,k) = - ( &
                     a*(f(i,j,k  )+f(i,j,k-1)) + &
                     b*(f(i,j,k+1)+f(i,j,k-2)))
      end do
  end do
  if (izs.eq.1) then
    do i=1,mx
      fp(i,1   ) = - ( &
                     a*(g(i,j,3   )+f(i,j,1   )) + &
                     b*(g(i,j,2   )+f(i,j,2   )))
      fp(i,2   ) = - ( &
                     a*(f(i,j,1   )+f(i,j,2   )) + &
                     b*(g(i,j,3   )+f(i,j,3   )))
      fp(i,3   ) = - ( &
                     a*(f(i,j,2   )+f(i,j,3   )) + &
                     b*(f(i,j,1   )+f(i,j,4   )))
    end do
  end if
  if (ize.eq.mz) then
    do i=1,mx
      fp(i,mz-1) = - ( &
                     a*(f(i,j,mz-2)+f(i,j,mz-1)) + &
                     b*(f(i,j,mz-3)+f(i,j,mz  )))
      fp(i,mz  ) = - ( &
                     a*(f(i,j,mz-1)+f(i,j,mz  )) + &
                     b*(f(i,j,mz-2)+h(i,j,1   )))
    end do
  end if

!$omp end parallel
!
  if (omp_master) then
    nstag = nstag+1
    nflop = nflop+8
  end if
END SUBROUTINE

!***********************************************************************
SUBROUTINE zdn132_neg (f,j, fp)
!
!  f is centered on (i,j,k), fifth order stagger
!
  use params
  implicit none
  logical omp_in_parallel
  real a, b, c
  integer i, j, k
  real,             dimension(mx,   mz):: fp
  real, intent(in), dimension(mx,my,mz):: f
  real,             dimension(mx,my,1 ):: g
!+---------------------------------------------------------------------
!
  if (mz.le.5) then
    !$omp parallel private(k)
    do k=izs,ize
      fp(:,k) = - f(:,j,k)
    end do
    !$omp end parallel
    return
  end if

  a = .5
!
!$omp parallel private(i,j,k)
  do k=max(2,izs),ize
      do i=1,mx
        fp(i,k) = - ( &
                     a*(f(i,j,k  )+f(i,j,k-1)))
      end do
  end do
  if (izs.eq.1) then
    do i=1,mx
      fp(i,1   ) = - ( &
                     a*(g(i,j,1   )+f(i,j,1   )))
    end do
  end if

!$omp end parallel
!
  if (omp_master) then
    nstag1 = nstag1+1
    nflop = nflop+2
  end if
END SUBROUTINE

!***********************************************************************
SUBROUTINE zup22_neg (f, fp)
!
!  f is centered on (i,k-.5), fifth order stagger
!
  use params
  implicit none
  logical omp_in_parallel
  real a, b, c
  integer i, j, k
  real,             dimension(mx,  mz):: fp
  real, intent(in), dimension(mx,  mz):: f
  real,             dimension(mx,1, 2):: g
  real,             dimension(mx,1, 3):: h

!+---------------------------------------------------------------------
!
!
  if (mz.le.5) then
    !$omp parallel private(k)
    do k=izs,ize
      fp(:,k) = - f(:,k)
    end do
    !$omp end parallel
    return
  end if
  b = -1./16.
  a = .5-b

!
!$omp parallel private(i,k)
  do k=max(3,izs),min(mz-3,ize)
      do i=1,mx
        fp(i,k) = - ( &
                     a*(f(i,k+1)+f(i,k  )) + &
                     b*(f(i,k+2)+f(i,k-1)))
      end do
  end do
  if (izs.eq.1) then
    do i=1,mx
      fp(i,1   ) = - ( &
                     a*(f(i,1   )+f(i,2   )) + &
                     b*(g(i,1,2   )+f(i,3   )))
      fp(i,2   ) = - ( &
                     a*(f(i,2   )+f(i,3   )) + &
                     b*(f(i,1   )+f(i,4   )))
    end do
  end if
  if (ize.eq.mz) then
    do i=1,mx
      fp(i,mz-2) = - ( &
                     a*(f(i,mz-2)+f(i,mz-1)) + &
                     b*(f(i,mz-3)+f(i,mz  )))
      fp(i,mz-1) = - ( &
                     a*(f(i,mz-1)+f(i,mz  )) + &
                     b*(f(i,mz-2)+h(i,1,1   )))
      fp(i,mz  ) = - ( &
                     a*(f(i,mz  )+h(i,1,1   )) + &
                     b*(f(i,mz-1)+h(i,1,2   )))
    end do
  end if

!$omp end parallel
!
  if (omp_master) then
    nstag = nstag+1
    nflop = nflop+8
  end if
END SUBROUTINE

!***********************************************************************
SUBROUTINE zup122_neg (f, fp)
!
!  f is centered on (i,k-.5), fifth order stagger
!
  use params
  implicit none
  logical omp_in_parallel
  real a, b, c
  integer i, j, k
  real,             dimension(mx,   mz):: fp
  real, intent(in), dimension(mx,   mz):: f
  real,             dimension(mx,my,1 ):: h
!+---------------------------------------------------------------------
!
  if (mz.le.5) then
    !$omp parallel private(k)
    do k=izs,ize
      fp(:,k) = - f(:,k)
    end do
    !$omp end parallel
    return
  end if

  a = .5
!
!$omp parallel private(i,k)
  do k=izs,ize
      do i=1,mx
        fp(i,k) = - ( &
                     a*(f(i,k+1)+f(i,k  )))
      end do
  end do
  if (ize.eq.mz) then
    do i=1,mx
      fp(i,mz) = - ( &
                     a*(h(i,1,1)+f(i,mz)))
    end do
  end if

!$omp end parallel
!
  if (omp_master) then
    nstag1 = nstag1+1
    nflop = nflop+2
  end if
END SUBROUTINE

!***********************************************************************
SUBROUTINE zdn22_neg (f, fp)
!
!  f is centered on (i,k), fifth order stagger
!
  use params
  implicit none
  logical omp_in_parallel
  real a, b, c
  integer i, j, k
  real,             dimension(mx,   mz):: fp
  real, intent(in), dimension(mx,   mz):: f
  real,             dimension(mx,1, 3 ):: g
  real,             dimension(mx,1, 2 ):: h

!+---------------------------------------------------------------------
!
  if (mz.le.5) then
    !$omp parallel private(k)
    do k=izs,ize
      fp(:,k) = - f(:,k)
    end do
    !$omp end parallel
    return
  end if
  b = -1./16.
  a = .5-b
!
!$omp parallel private(i,k)
  do k=max(4,izs),min(mz-2,ize)
      do i=1,mx
        fp(i,k) = - ( &
                     a*(f(i,k  )+f(i,k-1)) + &
                     b*(f(i,k+1)+f(i,k-2)))
      end do
  end do
  if (izs.eq.1) then
    do i=1,mx
      fp(i,1   ) = - ( &
                     a*(g(i,1,3 )+f(i,1   )) + &
                     b*(g(i,1,2 )+f(i,2   )))
      fp(i,2   ) = - ( &
                     a*(f(i,1   )+f(i,2   )) + &
                     b*(g(i,1,3 )+f(i,3   )))
      fp(i,3   ) = - ( &
                     a*(f(i,2   )+f(i,3   )) + &
                     b*(f(i,1   )+f(i,4   )))
    end do
  end if
  if (ize.eq.mz) then
    do i=1,mx
      fp(i,mz-1) = - ( &
                     a*(f(i,mz-2)+f(i,mz-1)) + &
                     b*(f(i,mz-3)+f(i,mz  )))
      fp(i,mz  ) = - ( &
                     a*(f(i,mz-1)+f(i,mz  )) + &
                     b*(f(i,mz-2)+h(i,1,1   )))
    end do
  end if

!$omp end parallel
!
  if (omp_master) then
    nstag = nstag+1
    nflop = nflop+8
  end if
END SUBROUTINE

!***********************************************************************
SUBROUTINE zdn122_neg (f, fp)
!
!  f is centered on (i,k), fifth order stagger
!
  use params
  implicit none
  logical omp_in_parallel
  real a, b, c
  integer i, j, k
  real,             dimension(mx,   mz):: fp
  real, intent(in), dimension(mx,   mz):: f
  real,             dimension(mx,1 ,1 ):: g
!+---------------------------------------------------------------------
!
  if (mz.le.5) then
    !$omp parallel private(k)
    do k=izs,ize
      fp(:,k) = - f(:,k)
    end do
    !$omp end parallel
    return
  end if

  a = .5
!
!$omp parallel private(i,k)
  do k=max(2,izs),ize
      do i=1,mx
        fp(i,k) = - ( &
                     a*(f(i,k  )+f(i,k-1)))
      end do
  end do
  if (izs.eq.1) then
    do i=1,mx
      fp(i,1   ) = - ( &
                     a*(g(i,1,1  )+f(i,1  )))
    end do
  end if
 
!$omp end parallel
!
  if (omp_master) then
    nstag1 = nstag1+1
    nflop = nflop+2
  end if
END SUBROUTINE
!***********************************************************************
SUBROUTINE ddzupi_neg (f, fp)
!
!  f is centered on (i,j,k-.5), fifth order stagger
!
  use params
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k
  real, intent(in) , dimension(mx,my,mz):: f
  real,              dimension(mx,my,mz):: fp
  real,              dimension(mx,my, 2):: g
  real,              dimension(mx,my, 3):: h
!+---------------------------------------------------------------------
!

  if (mz .lt. 5) then
    !$omp parallel private(k)
    do k=izs,ize
      fp(:,:,k) = - f(:,:,k)
    end do
    !$omp end parallel
    return
  end if

  b = -1./24.
  a = (1.-3.*b)
  a1st = 1

!
  !$omp parallel private(i,j,k)
  do k=max(izs,3),min(ize,mz-3)
    do j=1,my
      do i=1,mx
        fp(i,j,k) = - ( &
                     a*(-f(i,j,k   )+f(i,j,k+1 )) + &
                     b*(-f(i,j,k-1 )+f(i,j,k+2 )))
     end do
    end do
  end do
  !$omp end parallel
  do j=1,my
    do i=1,mx
      fp(i,j,mz-2) = - ( &
                     a*(-f(i,j,mz-2)+f(i,j,mz-1)) + &
                     b*(-f(i,j,mz-3)+f(i,j,mz  )))
      fp(i,j,mz-1) = - ( &
                     a*(-f(i,j,mz-1)+f(i,j,mz  )) + &
                     b*(-f(i,j,mz-2)+h(i,j,1   )))
      fp(i,j,mz  ) = - ( &
                     a*(-f(i,j,mz  )+h(i,j,1   )) + &
                     b*(-f(i,j,mz-1)+h(i,j,2   )))
      fp(i,j,1   ) = - ( &
                     a*(-f(i,j,1   )+f(i,j,2   )) + &
                     b*(-g(i,j,1   )+f(i,j,3   )))
      fp(i,j,2   ) = - ( &
                     a*(-f(i,j,2   )+f(i,j,3   )) + &
                     b*(-f(i,j,1   )+f(i,j,4   )))
    end do
  end do
!
  nstag = nstag+1
  nflop = nflop+8
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddzupi1_neg (f, fp)
!
!  f is centered on (i,j,k-.5), fifth order stagger
!
  use params
  implicit none
  logical omp_in_parallel
  integer i, j, k
  real, intent(in) , dimension(mx,my,mz):: f
  real,              dimension(mx,my,mz):: fp
  real,              dimension(mx,my, 1):: g
!+---------------------------------------------------------------------
!
  if (mz .lt. 5) then
    !$omp parallel private(k)
    do k=izs,ize
      fp(:,:,k) = - f(:,:,k)
    end do
    !$omp end parallel
    return
  end if
!
!
  !$omp parallel private(i,j,k)
  do k=izs,min(ize,mz-1)
    do j=1,my
      do i=1,mx
        fp(i,j,k  ) = - ( &
                     (f(i,j,k+1)-f(i,j,k)))
      end do
    end do
  end do
  !$omp end parallel
  do j=1,my
    do i=1,mx
      fp(i,j,mz) = - ( &
                     (+g(i,j,1)-f(i,j,mz)))
    end do
  end do
!
  nstag1 = nstag1+1
  nflop = nflop+2
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddzdni_neg (f, fp)
!
!  f is centered on (i,j), fifth order stagger
!
  use params
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f, fp
  real, dimension(mx,my, 3):: g
  real, dimension(mx,my, 2):: h
!+---------------------------------------------------------------------
!
  if (mz .lt. 5) then
    !$omp parallel private(k)
    do k=izs,ize
      fp(:,:,k) = - f(:,:,k)
    end do
    !$omp end parallel
    return
  end if

  b = -1./24.
  a = (1.-3.*b)
  a1st = 1.

!
  !$omp parallel private(i,j,k)
  do k=max(izs,4),min(ize,mz-2)
    do j=1,my
     do i=1,mx
      fp(i,j,k) = - ( &
                     a*(f(i,j,k   )-f(i,j,k-1)) + &
                     b*(f(i,j,k+1 )-f(i,j,k-2)))
     end do
    end do
  end do
  !$omp end parallel
  do j=1,my
    do i=1,mx
      fp(i,j,mz-1) = - ( &
                     a*(-f(i,j,mz-2)+f(i,j,mz-1)) + &
                     b*(-f(i,j,mz-3)+f(i,j,mz  )))
      fp(i,j,mz  ) = - ( &
                     a*(-f(i,j,mz-1)+f(i,j,mz  )) + &
                     b*(-f(i,j,mz-2)+h(i,j,1   )))
      fp(i,j,1   ) = - ( &
                     a*(-g(i,j,3   )+f(i,j,1   )) + &
                     b*(-g(i,j,2   )+f(i,j,2   )))
      fp(i,j,2   ) = - ( &
                     a*(-f(i,j,1   )+f(i,j,2   )) + &
                     b*(-g(i,j,3   )+f(i,j,3   )))
      fp(i,j,3   ) = - ( &
                     a*(-f(i,j,2   )+f(i,j,3   )) + &
                     b*(-f(i,j,1   )+f(i,j,4   )))
    end do
  end do
!
  nstag = nstag+1
  nflop = nflop+8
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddzdni1_neg (f, fp)
!
!  f is centered on (i,j), fifth order stagger
!
  use params
  implicit none
  logical omp_in_parallel
  integer i, j, k
  real, dimension(mx,my,mz):: f, fp
  real, dimension(mx,my, 1):: g
!+---------------------------------------------------------------------
!
  if (mz .lt. 5) then
    !$omp parallel private(k)
    do k=izs,ize
      fp(:,:,k) = - f(:,:,k)
    end do
    !$omp end parallel
    return
  end if
!
!
  !$omp parallel private(i,j,k)
  do k=max(izs,2),ize
    do j=1,my
     do i=1,mx
      fp(i,j,k) = - ( &
                     (f(i,j,k)-f(i,j,k-1)))
     end do
    end do
  end do
  !$omp end parallel
  do j=1,my
    do i=1,mx
      fp(i,j,1   ) = - ( &
                     (f(i,j,1)-g(i,j,1)))
    end do
  end do
!
  nstag1 = nstag1+1
  nflop = nflop+2
END SUBROUTINE
!***********************************************************************
SUBROUTINE ddzup_neg (f, fp)
!
!  f is centered on (i,j,k-.5), fifth order stagger
!
  use params
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k
  real, intent(in) , dimension(mx,my,mz):: f
  real,              dimension(mx,my,mz):: fp
  real,              dimension(mx,my, 2):: g
  real,              dimension(mx,my, 3):: h
!+---------------------------------------------------------------------
!

  if (mz .lt. 5) then
    !$omp parallel private(k)
    do k=izs,ize
      fp(:,:,k) = - 0.
    end do
    !$omp end parallel
    return
  end if

  b = -1./24.
  a = (1.-3.*b)/dz
  a1st = 1./dz
  b = b/dz

!
  !$omp parallel private(i,j,k)
  do k=max(3,izs),min(mz-3,ize)
    do j=1,my
      do i=1,mx
        fp(i,j,k) = - ( &
                     b*(f(i,j,k+2 )-f(i,j,k-1 )) + &
                     a*(f(i,j,k+1 )-f(i,j,k   )))
     end do
    end do
  end do

  if (ize.eq.mz) then
   do j=1,my
    do i=1,mx
      fp(i,j,mz-2) = - ( &
                     b*(f(i,j,mz  )-f(i,j,mz-3)) + &
                     a*(f(i,j,mz-1)-f(i,j,mz-2)))
      fp(i,j,mz-1) = - ( &
                     b*(h(i,j,1   )-f(i,j,mz-2)) + &
                     a*(f(i,j,mz  )-f(i,j,mz-1)))
      fp(i,j,mz  ) = - ( &
                     b*(h(i,j,2   )-f(i,j,mz-1)) + &
                     a*(h(i,j,1   )-f(i,j,mz  )))
    end do
   end do
  end if
  if (izs.eq.1) then
   do j=1,my
    do i=1,mx
      fp(i,j,1   ) = - ( &
                     b*(f(i,j,3   )-g(i,j,2   )) + &
                     a*(f(i,j,2   )-f(i,j,1   )))
      fp(i,j,2   ) = - ( &
                     b*(f(i,j,4   )-f(i,j,1   )) + &
                     a*(f(i,j,3   )-f(i,j,2   )))
    end do
   end do
  end if
  !$omp end parallel
!
  nstag = nstag+1
  nflop = nflop+8
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddzup1_neg (f, fp)
!
!  f is centered on (i,j,k-.5), fifth order stagger
!
  use params
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k
  real, intent(in) , dimension(mx,my,mz):: f
  real,              dimension(mx,my,mz):: fp
  real,              dimension(mx,my, 1):: g
!+---------------------------------------------------------------------
!
  if (mz .lt. 5) then
    !$omp parallel private(k)
    do k=izs,ize
      fp(:,:,k) = - 0.
    end do
    !$omp end parallel
    return
  end if
!
  a = 1./dz
!
  !$omp parallel private(i,j,k)
  do k=izs,min(mz-1,ize)
    do j=1,my
      do i=1,mx
        fp(i,j,k  ) = - ( &
                     a*(f(i,j,k+1)-f(i,j,k)))
      end do
    end do
  end do
  if (ize.eq.mz) then
   do j=1,my
    do i=1,mx
      fp(i,j,mz) = - ( &
                     a*(g(i,j,1)-f(i,j,mz)))
    end do
   end do
  end if
  !$omp end parallel
!
  nstag1 = nstag1+1
  nflop = nflop+2
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddzdn_neg (f, fp)
!
!  f is centered on (i,j), fifth order stagger
!
  use params
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f, fp
  real, dimension(mx,my, 3):: g
  real, dimension(mx,my, 2):: h
!+---------------------------------------------------------------------
!
  if (mz .lt. 5) then
    !$omp parallel private(k)
    do k=izs,ize
      fp(:,:,k) = - 0.
    end do
    !$omp end parallel
    return
  end if

  b = -1./24.
  a = (1.-3.*b)/dz
  a1st = 1./dz
  b = b/dz

!
  !$omp parallel private(i,j,k)
  do k=max(4,izs),min(mz-2,ize)
    do j=1,my
     do i=1,mx
      fp(i,j,k) = - ( &
                     b*(f(i,j,k+1 )-f(i,j,k-2 )) + &
                     a*(f(i,j,k   )-f(i,j,k-1 )))
     end do
    end do
  end do
  if (ize.eq.mz) then
   do j=1,my
    do i=1,mx
      fp(i,j,mz-1) = - ( &
                     b*(f(i,j,mz  )-f(i,j,mz-3)) + &
                     a*(f(i,j,mz-1)-f(i,j,mz-2)))
      fp(i,j,mz  ) = - ( &
                     b*(h(i,j,1   )-f(i,j,mz-2)) + &
                     a*(f(i,j,mz  )-f(i,j,mz-1)))
    end do
   end do
  end if
  if (izs.eq.1) then
   do j=1,my
    do i=1,mx
      fp(i,j,1   ) = - ( &
                     b*(f(i,j,2   )-g(i,j,2   )) + &
                     a*(f(i,j,1   )-g(i,j,3   )))
      fp(i,j,2   ) = - ( &
                     b*(f(i,j,3   )-g(i,j,3   )) + &
                     a*(f(i,j,2   )-f(i,j,1   )))
      fp(i,j,3   ) = - ( &
                     b*(f(i,j,4   )-f(i,j,1   )) + &
                     a*(f(i,j,3   )-f(i,j,2   )))
    end do
   end do
  end if
  !$omp end parallel
!
  nstag = nstag+1
  nflop = nflop+8
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddzdn1_neg (f, fp)
!
!  f is centered on (i,j), fifth order stagger
!
  use params
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f, fp
  real, dimension(mx,my, 1):: g
!+---------------------------------------------------------------------
!
  if (mz .lt. 5) then
    !$omp parallel private(k)
    do k=izs,ize
      fp(:,:,k) = - 0.
    end do
    !$omp end parallel
    return
  end if
!
  a = 1./dz
!
  !$omp parallel private(i,j,k)
  do k=max(2,izs),ize
    do j=1,my
     do i=1,mx
      fp(i,j,k) = - ( &
                     a*(f(i,j,k)-f(i,j,k-1)))
     end do
    end do
  end do
  if (izs.eq.1) then
   do j=1,my
    do i=1,mx
      fp(i,j,1   ) = - ( &
                     a*(f(i,j,1)-g(i,j,1  )))
    end do
   end do
  end if
  !$omp end parallel
!
  nstag1 = nstag1+1
  nflop = nflop+2
END SUBROUTINE
