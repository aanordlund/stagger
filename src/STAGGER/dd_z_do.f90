!***********************************************************************
FUNCTION ddzup (f)
  use params
  implicit none
  real a, b, c, a1st
  integer i, j, k, km2, km1, kp1, kp2, kp3
  real, dimension(mx,my,mz):: f, ddzup
!hpf$ distribute(*,*,block):: f, ddzup
!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    ddzup = 0.
    return
  end if
!
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)/dz
  a1st = 1./dz
  b = b/dz
  c = c/dz
!
!Xomp barrier
!$omp parallel private(i,j,k,km2,km1,kp1,kp2,kp3)
  do k=izs,ize
    km2=mod(k-3+mz,mz)+1
    km1=mod(k-2+mz,mz)+1
    kp1=mod(k     ,mz)+1
    kp2=mod(k+1   ,mz)+1
    kp3=mod(k+2   ,mz)+1
    do j=1,my
      do i=1,mx
        ddzup(i,j,k) = ( &
                     c*(f(i,j,kp3)-f(i,j,km2)) + &
                     b*(f(i,j,kp2)-f(i,j,km1)) + &
                     a*(f(i,j,kp1)-f(i,j,k  )) )
      end do
    end do
  end do
!$omp end parallel
!1omp barrier
!
  nstag = nstag+1
  nflop = nflop+8
END FUNCTION

!***********************************************************************
FUNCTION ddzup1 (f)
  use params
  implicit none
  real a, b, c, a1st
  integer i, j, k, kp1
  real, dimension(mx,my,mz):: f, ddzup1
!hpf$ distribute(*,*,block):: f, ddzup1
!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    ddzup1 = 0.
    return
  end if
!
  a = 1./dz
!
!Xomp barrier
!$omp parallel private(i,j,k,kp1)
  do k=izs,ize
    kp1=mod(k     ,mz)+1
    do j=1,my
      do i=1,mx
        ddzup1(i,j,k) = ( &
                     a*(f(i,j,kp1)-f(i,j,k  )))
      end do
    end do
  end do
!$omp end parallel
!1omp barrier
!
  nstag1 = nstag1+1
  nflop = nflop+2
END FUNCTION

!***********************************************************************
FUNCTION ddzdn (f)
  use params
  implicit none
  real a, b, c, a1st
  integer i, j, k, km3, km2, km1, kp1, kp2
  real, dimension(mx,my,mz):: f, ddzdn
!hpf$ distribute(*,*,block):: f, ddzdn
!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    ddzdn = 0.
    return
  end if
!
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)/dz
  a1st = 1./dz
  b = b/dz
  c = c/dz
!
!Xomp barrier
!$omp parallel private(i,j,k,km3,km2,km1,kp1,kp2)
  do k=izs,ize
    km3=mod(k-4+mz,mz)+1
    km2=mod(k-3+mz,mz)+1
    km1=mod(k-2+mz,mz)+1
    kp1=mod(k     ,mz)+1
    kp2=mod(k+1   ,mz)+1
    do j=1,my
      do i=1,mx
        ddzdn(i,j,k) = ( &
                     c*(f(i,j,kp2)-f(i,j,km3)) + &
                     b*(f(i,j,kp1)-f(i,j,km2)) + &
                     a*(f(i,j,k  )-f(i,j,km1)) )
      end do
    end do
  end do
!$omp end parallel
!1omp barrier
!
  nstag = nstag+1
  nflop = nflop+8
END FUNCTION

!***********************************************************************
FUNCTION ddzdn1 (f)
  use params
  implicit none
  real a, b, c, a1st
  integer i, j, k, km1
  real, dimension(mx,my,mz):: f, ddzdn1
!hpf$ distribute(*,*,block):: f, ddzdn1
!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    ddzdn1 = 0.
    return
  end if
!
  a = 1./dz
!
!Xomp barrier
!$omp parallel private(i,j,k,km1)
  do k=izs,ize
    km1=mod(k-2+mz,mz)+1
    do j=1,my
      do i=1,mx
        ddzdn1(i,j,k) = ( &
                     a*(f(i,j,k  )-f(i,j,km1)))
      end do
    end do
  end do
!$omp end parallel
!1omp barrier
!
  nstag1 = nstag1+1
  nflop = nflop+2
END FUNCTION
