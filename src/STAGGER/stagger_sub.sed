# $Id: stagger_sub.sed,v 1.5 2006/02/26 00:23:22 aake Exp $
s/fp(\([^)]*\)) *=/fp(\1) = fp(\1) -/
s/fp *=/fp = fp -/
s/_set/_sub/g
s/- *-/+/
/print .*id/d
/character.*id/d
/call print_id/d
/omp_in_parallel.*WARNING:/d
