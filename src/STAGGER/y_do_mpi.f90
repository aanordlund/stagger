!***********************************************************************
FUNCTION yup (f)
!
!  f is centered on (i,j-.5,k), fifth order stagger
!
  use params, g=>gy2, h=>gy3
  implicit none
  real a, b, c, a1st
  integer i, j, k
  real, intent(in) , dimension(mx,my,mz):: f
  real,              dimension(mx,my,mz):: yup
!-----------------------------------------------------------------------
  character(len=mid):: id = "$Id: y_do_mpi.f90,v 1.19 2015/11/01 21:33:45 aake Exp $"
  call print_id (id)

  if (my .lt. 5) then
    !$omp parallel private(k)
    do k=izs,ize
      yup(:,:,k) = f(:,:,k)
    end do
    !$omp end parallel
    return
  end if
  c = 3./256.
  b = -25./256.
  a = .5-b-c
  a1st = 0.5

  call mpi_send_y (f, g, 2, h, 3)

  !$omp parallel private(i,j,k)
  do k=izs,ize
    do j=3,my-3
     do i=1,mx
      yup(i,j  ,k) = ( &
                     c*(f(i,j-2 ,k)+f(i,j+3 ,k)) + &
                     b*(f(i,j-1 ,k)+f(i,j+2 ,k)) + &
                     a*(f(i,j   ,k)+f(i,j+1 ,k)))
     end do
    end do
    do i=1,mx
      yup(i,my-2,k) = ( &
                     c*(f(i,my-4,k)+h(i,1   ,k)) + &
                     b*(f(i,my-3,k)+f(i,my  ,k)) + &
                     a*(f(i,my-2,k)+f(i,my-1,k)))
      yup(i,my-1,k) = ( &
                     c*(f(i,my-3,k)+h(i,2   ,k)) + &
                     b*(f(i,my-2,k)+h(i,1   ,k)) + &
                     a*(f(i,my-1,k)+f(i,my  ,k)))
      yup(i,my  ,k) = ( &
                     c*(f(i,my-2,k)+h(i,3   ,k)) + &
                     b*(f(i,my-1,k)+h(i,2   ,k)) + &
                     a*(f(i,my  ,k)+h(i,1   ,k)))
      yup(i,1   ,k) = ( &
                     c*(g(i,1   ,k)+f(i,4   ,k)) + &
                     b*(g(i,2   ,k)+f(i,3   ,k)) + &
                     a*(f(i,1   ,k)+f(i,2   ,k)))
      yup(i,2   ,k) = ( &
                     c*(g(i,2   ,k)+f(i,5   ,k)) + &
                     b*(f(i,1   ,k)+f(i,4   ,k)) + &
                     a*(f(i,2   ,k)+f(i,3   ,k)))
    end do
  end do
  !$omp end parallel

  !$omp master
  nstag = nstag+1
  nflop = nflop+8
  !$omp end master
END FUNCTION

!***********************************************************************
FUNCTION yup1 (f)
!
!  f is centered on (i,j-.5,k), fifth order stagger
!
  use params, h=>gy1
  implicit none
  real a, b, c, a1st
  integer i, j, k
  real, intent(in) , dimension(mx,my,mz):: f
  real,              dimension(mx,my,mz):: yup1
!-----------------------------------------------------------------------
!
  if (my .lt. 5) then
    !$omp parallel private(k)
    do k=izs,ize
      yup1(:,:,k) = f(:,:,k)
    end do
    !$omp end parallel
    return
  end if

  a = 0.5

  call mpi_send_y (f, h, 0, h, 1)

  !$omp parallel private(i,j,k)
  do k=izs,ize
    do j=1,my-1
      do i=1,mx
        yup1(i,j  ,k) = ( &
                     a*(f(i,j   ,k)+f(i,j+1 ,k)))
      end do
    end do
    do i=1,mx
      yup1(i,my,k) = ( &
                     a*(h(i,1,k)+f(i,my,k)))
    end do
  end do
  !$omp end parallel

  !$omp master
  nstag1 = nstag1+1
  nflop = nflop+2
  !$omp end master
END FUNCTION

!***********************************************************************
FUNCTION ydn (f)
!
!  f is centered on (i,j,k), fifth order stagger
!
  use params, g=>gy3, h=>gy2
  implicit none
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f, ydn
!-----------------------------------------------------------------------
  if (my .lt. 5) then
    !$omp parallel private(k)
    do k=izs,ize
      ydn(:,:,k) = f(:,:,k)
    end do
    !$omp end parallel
    return
  end if
  c = 3./256.
  b = -25./256.
  a = .5-b-c
  a1st = 0.5
  call mpi_send_y (f, g, 3, h, 2)
  !$omp parallel private(i,j,k)
  do k=izs,ize
    do j=4,my-2
     do i=1,mx
      ydn(i,j,k) = ( &
                     c*(f(i,j+2 ,k)+f(i,j-3 ,k)) + &
                     b*(f(i,j+1 ,k)+f(i,j-2 ,k)) + &
                     a*(f(i,j   ,k)+f(i,j-1 ,k)))
     end do
    end do
    do i=1,mx
      ydn(i,my-1,k) = ( &
                     c*(f(i,my-4,k)+h(i,1   ,k)) + &
                     b*(f(i,my-3,k)+f(i,my  ,k)) + &
                     a*(f(i,my-2,k)+f(i,my-1,k)))
      ydn(i,my  ,k) = ( &
                     c*(f(i,my-3,k)+h(i,2   ,k)) + &
                     b*(f(i,my-2,k)+h(i,1   ,k)) + &
                     a*(f(i,my-1,k)+f(i,my  ,k)))
      ydn(i,1   ,k) = ( &
                     c*(g(i,1   ,k)+f(i,3   ,k)) + &
                     b*(g(i,2   ,k)+f(i,2   ,k)) + &
                     a*(g(i,3   ,k)+f(i,1   ,k)))
      ydn(i,2   ,k) = ( &
                     c*(g(i,2   ,k)+f(i,4   ,k)) + &
                     b*(g(i,3   ,k)+f(i,3   ,k)) + &
                     a*(f(i,1   ,k)+f(i,2   ,k)))
      ydn(i,3   ,k) = ( &
                     c*(g(i,3   ,k)+f(i,5   ,k)) + &
                     b*(f(i,1   ,k)+f(i,4   ,k)) + &
                     a*(f(i,2   ,k)+f(i,3   ,k)))
    end do
  end do
  !$omp end parallel
  !$omp master
  nstag = nstag+1
  nflop = nflop+8
  !$omp end master
END FUNCTION

!***********************************************************************
FUNCTION ydn_r8 (f) result(fp)
!
!  f is centered on (i,j,k), fifth order stagger
!
  use params
  implicit none
  real a, b, c, a1st
  integer i, j, k
  real(8), dimension(mx,my,mz):: f, fp
  real(8), allocatable, dimension(:,:,:):: g, h
!-----------------------------------------------------------------------
  if (my .lt. 5) then
    !$omp parallel private(k)
    do k=izs,ize
      fp(:,:,k) = f(:,:,k)
    end do
    !$omp end parallel
    return
  end if
  c = 3./256.
  b = -25./256.
  a = .5-b-c
  a1st = 0.5
  allocate (g(mx,3,mz), h(mx,2,mz))
  call mpi_send_y_r8 (f, g, 3, h, 2)
  !$omp parallel private(i,j,k)
  do k=izs,ize
    do j=4,my-2
     do i=1,mx
      fp(i,j,k) = ( &
                     c*(f(i,j+2 ,k)+f(i,j-3 ,k)) + &
                     b*(f(i,j+1 ,k)+f(i,j-2 ,k)) + &
                     a*(f(i,j   ,k)+f(i,j-1 ,k)))
     end do
    end do
    do i=1,mx
      fp(i,my-1,k) = ( &
                     c*(f(i,my-4,k)+h(i,1   ,k)) + &
                     b*(f(i,my-3,k)+f(i,my  ,k)) + &
                     a*(f(i,my-2,k)+f(i,my-1,k)))
      fp(i,my  ,k) = ( &
                     c*(f(i,my-3,k)+h(i,2   ,k)) + &
                     b*(f(i,my-2,k)+h(i,1   ,k)) + &
                     a*(f(i,my-1,k)+f(i,my  ,k)))
      fp(i,1   ,k) = ( &
                     c*(g(i,1   ,k)+f(i,3   ,k)) + &
                     b*(g(i,2   ,k)+f(i,2   ,k)) + &
                     a*(g(i,3   ,k)+f(i,1   ,k)))
      fp(i,2   ,k) = ( &
                     c*(g(i,2   ,k)+f(i,4   ,k)) + &
                     b*(g(i,3   ,k)+f(i,3   ,k)) + &
                     a*(f(i,1   ,k)+f(i,2   ,k)))
      fp(i,3   ,k) = ( &
                     c*(g(i,3   ,k)+f(i,5   ,k)) + &
                     b*(f(i,1   ,k)+f(i,4   ,k)) + &
                     a*(f(i,2   ,k)+f(i,3   ,k)))
    end do
  end do
  !$omp end parallel
  !$omp master
  deallocate (g, h)
  nstag = nstag+1
  nflop = nflop+8
  !$omp end master
END FUNCTION

!***********************************************************************
FUNCTION ydn1 (f)
!
!  f is centered on (i,j,k), fifth order stagger
!
  use params, g=>gy1
  implicit none
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f, ydn1
!-----------------------------------------------------------------------
  if (my .lt. 5) then
    !$omp parallel private(k)
    do k=izs,ize
      ydn1(:,:,k) = f(:,:,k)
    end do
    !$omp end parallel
    return
  end if

  a = 0.5

  call mpi_send_y (f, g, 1, g, 0)

  !$omp parallel private(i,j,k)
  do k=izs,ize
    do j=2,my
     do i=1,mx
      ydn1(i,j,k) = ( &
                     a*(f(i,j   ,k)+f(i,j-1 ,k)))
     end do
    end do
    do i=1,mx
      ydn1(i,1   ,k) = ( &
                     a*(g(i,1   ,k)+f(i,1   ,k)))
    end do
  end do
  !$omp end parallel

  !$omp master
  nstag1 = nstag1+1
  nflop = nflop+2
  !$omp end master
END FUNCTION

!***********************************************************************
FUNCTION ydn1_r8 (f) result (fp)
!
!  f is centered on (i,j,k), fifth order stagger
!
  use params
  implicit none
  logical omp_in_parallel
  real(8) a
  integer i, j, k
  real(8), dimension(mx,my,mz):: f, fp
  real(8), allocatable, dimension(:,:,:):: g
!-----------------------------------------------------------------------
  if (my .lt. 5) then
    do k=izs,ize
      fp(:,:,k) = f(:,:,k)
    end do
    return
  end if

  a = 0.5d0

  allocate (g(mx,1,mz))
  call mpi_send_y_r8 (f, g, 1, g, 0)

  do k=izs,ize
    do j=2,my
     do i=1,mx
      fp(i,j,k) = ( &
                     a*(f(i,j   ,k)+f(i,j-1 ,k)))
     end do
    end do
    do i=1,mx
      fp(i,1   ,k) = ( &
                     a*(g(i,1   ,k)+f(i,1   ,k)))
    end do
  end do
  deallocate (g)

  !$omp master
  nstag1 = nstag1+1
  nflop = nflop+2
  !$omp end master
END FUNCTION
