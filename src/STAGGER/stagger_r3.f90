
      MODULE stagger

! 6Th order staggered derivatives, 5th order interpolation. this version
! uses explicit loops, for increased speed and parallelization.

         INTEGER            :: nflop, nstag, nstag1

      CONTAINS

         SUBROUTINE xup_set (f, fp)

! F is centered on (i-.5,j,k), fifth order stagger

         USE params

         IMPLICIT NONE

!r3 #include "r3_info.h"
!r3 #include "r3_omp.h"

         REAL               :: f(mx,my,mz), fp(mx,my,mz)

!HPF$ DISTRIBUTE (*,*,block) :: f, fp

         INTEGER            :: i, j, k
         REAL               :: a, a1st, b, c


!r3          CALL r3_info_begin (r3_info_index_0, 'CPU_xup_set')

         IF (mx .LE. 5) THEN
            fp = f
!r3             CALL r3_info_end (r3_info_index_0)
            RETURN
         END IF
         c    = 3.0 / 256.0
         b    = -25.0 / 256.0
         a    = 0.5 - b - c
         a1st = 0.5

!$OMP PARALLEL PRIVATE (i,j,k)
         DO k = izs, ize
            DO j = 1, my
               fp(mx-2,j,k) = (a * (f(mx-2,j,k) + f(mx-1,j,k)) + b *
     .                        (f(mx-3,j,k) + f(mx,j,k)) + c *
     .                        (f(mx-4,j,k) + f(1,j,k)))
               fp(mx-1,j,k) = (a * (f(mx-1,j,k) + f(mx,j,k)) + b *
     .                        (f(mx-2,j,k) + f(1,j,k)) + c *
     .                        (f(mx-3,j,k) + f(2,j,k)))
               fp(mx,j,k)   = (a * (f(mx,j,k) + f(1,j,k)) + b *
     .                        (f(mx-1,j,k) + f(2,j,k)) + c *
     .                        (f(mx-2,j,k) + f(3,j,k)))
               fp(1,j,k)    = (a * (f(1,j,k) + f(2,j,k)) + b *
     .                        (f(mx,j,k) + f(3,j,k)) + c *
     .                        (f(mx-1,j,k) + f(4,j,k)))
               fp(2,j,k)    = (a * (f(2,j,k) + f(3,j,k)) + b *
     .                        (f(1,j,k) + f(4,j,k)) + c * (f(mx,j,k) +
     .                        f(5,j,k)))
            END DO
            DO j = 1, my
               DO i = 3, mx - 3
                  fp(i,j,k) = (a * (f(i,j,k) + f(i+1,j,k)) + b *
     .                        (f(i-1,j,k) + f(i+2,j,k)) + c *
     .                        (f(i-2,j,k) + f(i+3,j,k)))
               END DO
            END DO
         END DO
!$OMP END PARALLEL

         nstag = nstag + 1
         nflop = nflop + 8

!r3          CALL r3_info_end (r3_info_index_0)

         RETURN
         END SUBROUTINE

! 

         SUBROUTINE xup1_set (f, fp)

! F is centered on (i-.5,j,k), fifth order stagger

         USE params

         IMPLICIT NONE

!r3 #include "r3_info.h"
!r3 #include "r3_omp.h"

         REAL               :: f(mx,my,mz), fp(mx,my,mz)

!HPF$ DISTRIBUTE (*,*,block) :: f, fp

         INTEGER            :: i, j, k
         REAL               :: a


!r3          CALL r3_info_begin (r3_info_index_0, 'CPU_xup1_set')

         IF (mx .LE. 5) THEN
            fp = f
!r3             CALL r3_info_end (r3_info_index_0)
            RETURN
         END IF

         a = 0.5

!$OMP PARALLEL PRIVATE (i,j,k)
         DO k = izs, ize
            DO j = 1, my
               fp(mx,j,k) = (a * (f(mx,j,k) + f(1,j,k)))
            END DO
            DO j = 1, my
               DO i = 1, mx - 1
                  fp(i,j,k) = (a * (f(i,j,k) + f(i+1,j,k)))
               END DO
            END DO
         END DO
!$OMP END PARALLEL

         nstag1 = nstag1 + 1
         nflop  = nflop + 2

!r3          CALL r3_info_end (r3_info_index_0)

         RETURN
         END SUBROUTINE

! 

         SUBROUTINE xdn_set (f, fp)

! F is centered on (i,j,k), fifth order stagger

         USE params

         IMPLICIT NONE

!r3 #include "r3_info.h"
!r3 #include "r3_omp.h"

         REAL               :: f(mx,my,mz), fp(mx,my,mz)

!HPF$ DISTRIBUTE (*,*,block) :: f, fp

         INTEGER            :: i, j, k
         REAL               :: a, a1st, b, c


!r3          CALL r3_info_begin (r3_info_index_0, 'CPU_xdn_set')

         IF (mx .LE. 5) THEN
            fp = f
!r3             CALL r3_info_end (r3_info_index_0)
            RETURN
         END IF

         c    = 3.0 / 256.0
         b    = -25.0 / 256.0
         a    = 0.5 - b - c
         a1st = 0.5

!$OMP PARALLEL PRIVATE (i,j,k)
         DO k = izs, ize
            DO j = 1, my
               fp(mx-1,j,k) = (a * (f(mx-2,j,k) + f(mx-1,j,k)) + b *
     .                        (f(mx-3,j,k) + f(mx,j,k)) + c *
     .                        (f(mx-4,j,k) + f(1,j,k)))
               fp(mx,j,k)   = (a * (f(mx-1,j,k) + f(mx,j,k)) + b *
     .                        (f(mx-2,j,k) + f(1,j,k)) + c *
     .                        (f(mx-3,j,k) + f(2,j,k)))
               fp(1,j,k)    = (a * (f(mx,j,k) + f(1,j,k)) + b *
     .                        (f(mx-1,j,k) + f(2,j,k)) + c *
     .                        (f(mx-2,j,k) + f(3,j,k)))
               fp(2,j,k)    = (a * (f(1,j,k) + f(2,j,k)) + b *
     .                        (f(mx,j,k) + f(3,j,k)) + c *
     .                        (f(mx-1,j,k) + f(4,j,k)))
               fp(3,j,k)    = (a * (f(2,j,k) + f(3,j,k)) + b *
     .                        (f(1,j,k) + f(4,j,k)) + c * (f(mx,j,k) +
     .                        f(5,j,k)))
            END DO
            DO j = 1, my
               DO i = 4, mx - 2
                  fp(i,j,k) = (a * (f(i,j,k) + f(i-1,j,k)) + b *
     .                        (f(i+1,j,k) + f(i-2,j,k)) + c *
     .                        (f(i+2,j,k) + f(i-3,j,k)))
               END DO
            END DO
         END DO
!$OMP END PARALLEL

         nstag = nstag + 1
         nflop = nflop + 8

!r3          CALL r3_info_end (r3_info_index_0)

         RETURN
         END SUBROUTINE

! 

         SUBROUTINE xdn1_set (f, fp)

! F is centered on (i,j,k), fifth order stagger

         USE params

         IMPLICIT NONE

!r3 #include "r3_info.h"
!r3 #include "r3_omp.h"

         REAL               :: f(mx,my,mz), fp(mx,my,mz)

!HPF$ DISTRIBUTE (*,*,block) :: f, fp

         INTEGER            :: i, j, k
         REAL               :: a


!r3          CALL r3_info_begin (r3_info_index_0, 'CPU_xdn1_set')

         IF (mx .LE. 5) THEN
            fp = f
!r3             CALL r3_info_end (r3_info_index_0)
            RETURN
         END IF

         a = 0.5

!$OMP PARALLEL PRIVATE (i,j,k)
         DO k = izs, ize
            DO j = 1, my
               fp(1,j,k) = (a * (f(mx,j,k) + f(1,j,k)))
            END DO
            DO j = 1, my
               DO i = 2, mx
                  fp(i,j,k) = (a * (f(i,j,k) + f(i-1,j,k)))
               END DO
            END DO
         END DO
!$OMP END PARALLEL

         nstag1 = nstag1 + 1
         nflop  = nflop + 2

!r3          CALL r3_info_end (r3_info_index_0)

         RETURN
         END SUBROUTINE

! 

         SUBROUTINE ddxup_set (f, fp)

         USE params

         IMPLICIT NONE

!r3 #include "r3_info.h"
!r3 #include "r3_omp.h"

         REAL               :: f(mx,my,mz), fp(mx,my,mz)

!HPF$ DISTRIBUTE (*,*,block) :: f, fp

         INTEGER            :: i, j, k
         REAL               :: a, a1st, b, c


!r3          CALL r3_info_begin (r3_info_index_0, 'CPU_ddxup_set')

         IF (mx .LE. 5) THEN
            fp = 0.0
!r3             CALL r3_info_end (r3_info_index_0)
            RETURN
         END IF

         c    = (-1.0 + (3.0 ** 5 - 3.0) / (3.0 ** 3 - 3.0)) /
     .          (5.0 ** 5 - 5.0 - 5.0 * (3.0 ** 5 - 3))
         b    = (-1.0 - 120.0 * c) / 24.0
         a    = (1.0 - 3.0 * b - 5.0 * c) / dx
         a1st = 1.0 / dx
         b    = b / dx
         c    = c / dx

!$OMP PARALLEL PRIVATE (i,j,k)
         DO k = izs, ize
            DO j = 1, my
               fp(mx-2,j,k) = (a * (f(mx-1,j,k) - f(mx-2,j,k)) + b *
     .                        (f(mx,j,k) - f(mx-3,j,k)) + c *
     .                        (f(1,j,k) - f(mx-4,j,k)))
               fp(mx-1,j,k) = (a * (f(mx,j,k) - f(mx-1,j,k)) + b *
     .                        (f(1,j,k) - f(mx-2,j,k)) + c * (f(2,j,k) -
     .                        f(mx-3,j,k)))
               fp(mx,j,k)   = (a * (f(1,j,k) - f(mx,j,k)) + b *
     .                        (f(2,j,k) - f(mx-1,j,k)) + c * (f(3,j,k) -
     .                        f(mx-2,j,k)))
               fp(1,j,k)    = (a * (f(2,j,k) - f(1,j,k)) + b *
     .                        (f(3,j,k) - f(mx,j,k)) + c * (f(4,j,k) -
     .                        f(mx-1,j,k)))
               fp(2,j,k)    = (a * (f(3,j,k) - f(2,j,k)) + b *
     .                        (f(4,j,k) - f(1,j,k)) + c * (f(5,j,k) -
     .                        f(mx,j,k)))
            END DO
            DO j = 1, my
               DO i = 3, mx - 3
                  fp(i,j,k) = (a * (f(i+1,j,k) - f(i,j,k)) + b *
     .                        (f(i+2,j,k) - f(i-1,j,k)) + c *
     .                        (f(i+3,j,k) - f(i-2,j,k)))
               END DO
            END DO
         END DO
!$OMP END PARALLEL

         nstag = nstag + 1
         nflop = nflop + 8

!r3          CALL r3_info_end (r3_info_index_0)

         RETURN
         END SUBROUTINE

! 

         SUBROUTINE ddxup1_set (f, fp)

         USE params

         IMPLICIT NONE

!r3 #include "r3_info.h"
!r3 #include "r3_omp.h"

         REAL               :: f(mx,my,mz), fp(mx,my,mz)

!HPF$ DISTRIBUTE (*,*,block) :: f, fp

         INTEGER            :: i, j, k
         REAL               :: a


!r3          CALL r3_info_begin (r3_info_index_0, 'CPU_ddxup1_set')

         IF (mx .LE. 5) THEN
            fp = 0.0
!r3             CALL r3_info_end (r3_info_index_0)
            RETURN
         END IF

         a = 1.0 / dx

!$OMP PARALLEL PRIVATE (i,j,k)
         DO k = izs, ize
            DO j = 1, my
               fp(mx,j,k) = (a * (f(1,j,k) - f(mx,j,k)))
            END DO
            DO j = 1, my
               DO i = 1, mx - 1
                  fp(i,j,k) = (a * (f(i+1,j,k) - f(i,j,k)))
               END DO
            END DO
         END DO
!$OMP END PARALLEL

         nstag1 = nstag1 + 1
         nflop  = nflop + 2

!r3          CALL r3_info_end (r3_info_index_0)

         RETURN
         END SUBROUTINE

! 

         SUBROUTINE ddxdn_set (f, fp)

         USE params

         IMPLICIT NONE

!r3 #include "r3_info.h"
!r3 #include "r3_omp.h"

         REAL               :: f(mx,my,mz), fp(mx,my,mz)

!HPF$ DISTRIBUTE (*,*,block) :: f, fp

         INTEGER            :: i, j, k
         REAL               :: a, a1st, b, c


!r3          CALL r3_info_begin (r3_info_index_0, 'CPU_ddxdn_set')

         IF (mx .LE. 5) THEN
            fp = 0.0
!r3             CALL r3_info_end (r3_info_index_0)
            RETURN
         END IF

         c    = (-1.0 + (3.0 ** 5 - 3.0) / (3.0 ** 3 - 3.0)) /
     .          (5.0 ** 5 - 5.0 - 5.0 * (3.0 ** 5 - 3))
         b    = (-1.0 - 120.0 * c) / 24.0
         a    = (1.0 - 3.0 * b - 5.0 * c) / dx
         a1st = 1.0 / dx
         b    = b / dx
         c    = c / dx

!$OMP PARALLEL PRIVATE (i,j,k)
         DO k = izs, ize
            DO j = 1, my
               fp(mx-1,j,k) = (a * (f(mx-1,j,k) - f(mx-2,j,k)) + b *
     .                        (f(mx,j,k) - f(mx-3,j,k)) + c *
     .                        (f(1,j,k) - f(mx-4,j,k)))
               fp(mx,j,k)   = (a * (f(mx,j,k) - f(mx-1,j,k)) + b *
     .                        (f(1,j,k) - f(mx-2,j,k)) + c * (f(2,j,k) -
     .                        f(mx-3,j,k)))
               fp(1,j,k)    = (a * (f(1,j,k) - f(mx,j,k)) + b *
     .                        (f(2,j,k) - f(mx-1,j,k)) + c * (f(3,j,k) -
     .                        f(mx-2,j,k)))
               fp(2,j,k)    = (a * (f(2,j,k) - f(1,j,k)) + b *
     .                        (f(3,j,k) - f(mx,j,k)) + c * (f(4,j,k) -
     .                        f(mx-1,j,k)))
               fp(3,j,k)    = (a * (f(3,j,k) - f(2,j,k)) + b *
     .                        (f(4,j,k) - f(1,j,k)) + c * (f(5,j,k) -
     .                        f(mx,j,k)))
            END DO
            DO j = 1, my
               DO i = 4, mx - 2
                  fp(i,j,k) = (a * (f(i,j,k) - f(i-1,j,k)) + b *
     .                        (f(i+1,j,k) - f(i-2,j,k)) + c *
     .                        (f(i+2,j,k) - f(i-3,j,k)))
               END DO
            END DO
         END DO
!$OMP END PARALLEL

         nstag = nstag + 1
         nflop = nflop + 8

!r3          CALL r3_info_end (r3_info_index_0)

         RETURN
         END SUBROUTINE

! 

         SUBROUTINE ddxdn1_set (f, fp)

         USE params

         IMPLICIT NONE

!r3 #include "r3_info.h"
!r3 #include "r3_omp.h"

         REAL               :: f(mx,my,mz), fp(mx,my,mz)

!HPF$ DISTRIBUTE (*,*,block) :: f, fp

         INTEGER            :: i, j, k
         REAL               :: a


!r3          CALL r3_info_begin (r3_info_index_0, 'CPU_ddxdn1_set')

         IF (mx .LE. 5) THEN
            fp = 0.0
!r3             CALL r3_info_end (r3_info_index_0)
            RETURN
         END IF

         a = 1.0 / dx

!$OMP PARALLEL PRIVATE (i,j,k)
         DO k = izs, ize
            DO j = 1, my
               fp(1,j,k) = (a * (f(1,j,k) - f(mx,j,k)))
            END DO
            DO j = 1, my
               DO i = 2, mx
                  fp(i,j,k) = (a * (f(i,j,k) - f(i-1,j,k)))
               END DO
            END DO
         END DO
!$OMP END PARALLEL

         nstag1 = nstag1 + 1
         nflop  = nflop + 2

!r3          CALL r3_info_end (r3_info_index_0)

         RETURN
         END SUBROUTINE

! 

         SUBROUTINE ddxup_add (f, fp)

         USE params

         IMPLICIT NONE

!r3 #include "r3_info.h"
!r3 #include "r3_omp.h"

         REAL               :: f(mx,my,mz), fp(mx,my,mz)

!HPF$ DISTRIBUTE (*,*,block) :: f, fp

         INTEGER            :: i, j, k
         REAL               :: a, a1st, b, c


!r3          CALL r3_info_begin (r3_info_index_0, 'CPU_ddxup_add')

         IF (mx .LE. 5) THEN
            fp = fp + 0.0
!r3             CALL r3_info_end (r3_info_index_0)
            RETURN
         END IF

         c    = (-1.0 + (3.0 ** 5 - 3.0) / (3.0 ** 3 - 3.0)) /
     .          (5.0 ** 5 - 5.0 - 5.0 * (3.0 ** 5 - 3))
         b    = (-1.0 - 120.0 * c) / 24.0
         a    = (1.0 - 3.0 * b - 5.0 * c) / dx
         a1st = 1.0 / dx
         b    = b / dx
         c    = c / dx

!$OMP PARALLEL PRIVATE (i,j,k)
         DO k = izs, ize
            DO j = 1, my
               fp(mx-2,j,k) = fp(mx-2,j,k) + (a * (f(mx-1,j,k) -
     .                        f(mx-2,j,k)) + b * (f(mx,j,k) -
     .                        f(mx-3,j,k)) + c * (f(1,j,k) -
     .                        f(mx-4,j,k)))
               fp(mx-1,j,k) = fp(mx-1,j,k) + (a * (f(mx,j,k) -
     .                        f(mx-1,j,k)) + b * (f(1,j,k) -
     .                        f(mx-2,j,k)) + c * (f(2,j,k) -
     .                        f(mx-3,j,k)))
               fp(mx,j,k)   = fp(mx,j,k) + (a * (f(1,j,k) - f(mx,j,k)) +
     .                        b * (f(2,j,k) - f(mx-1,j,k)) + c *
     .                        (f(3,j,k) - f(mx-2,j,k)))
               fp(1,j,k)    = fp(1,j,k) + (a * (f(2,j,k) - f(1,j,k)) +
     .                        b * (f(3,j,k) - f(mx,j,k)) + c *
     .                        (f(4,j,k) - f(mx-1,j,k)))
               fp(2,j,k)    = fp(2,j,k) + (a * (f(3,j,k) - f(2,j,k)) +
     .                        b * (f(4,j,k) - f(1,j,k)) + c *
     .                        (f(5,j,k) - f(mx,j,k)))
            END DO
            DO j = 1, my
               DO i = 3, mx - 3
                  fp(i,j,k) = fp(i,j,k) + (a * (f(i+1,j,k) - f(i,j,k)) +
     .                        b * (f(i+2,j,k) - f(i-1,j,k)) + c *
     .                        (f(i+3,j,k) - f(i-2,j,k)))
               END DO
            END DO
         END DO
!$OMP END PARALLEL

         nstag = nstag + 1
         nflop = nflop + 8

!r3          CALL r3_info_end (r3_info_index_0)

         RETURN
         END SUBROUTINE

! 

         SUBROUTINE ddxup1_add (f, fp)

         USE params

         IMPLICIT NONE

!r3 #include "r3_info.h"
!r3 #include "r3_omp.h"

         REAL               :: f(mx,my,mz), fp(mx,my,mz)

!HPF$ DISTRIBUTE (*,*,block) :: f, fp

         INTEGER            :: i, j, k
         REAL               :: a


!r3          CALL r3_info_begin (r3_info_index_0, 'CPU_ddxup1_add')

         IF (mx .LE. 5) THEN
            fp = fp + 0.0
!r3             CALL r3_info_end (r3_info_index_0)
            RETURN
         END IF

         a = 1.0 / dx

!$OMP PARALLEL PRIVATE (i,j,k)
         DO k = izs, ize
            DO j = 1, my
               fp(mx,j,k) = fp(mx,j,k) + (a * (f(1,j,k) - f(mx,j,k)))
            END DO
            DO j = 1, my
               DO i = 1, mx - 1
                  fp(i,j,k) = fp(i,j,k) + (a * (f(i+1,j,k) - f(i,j,k)))
               END DO
            END DO
         END DO
!$OMP END PARALLEL

         nstag1 = nstag1 + 1
         nflop  = nflop + 2

!r3          CALL r3_info_end (r3_info_index_0)

         RETURN
         END SUBROUTINE

! 

         SUBROUTINE yup_set (f, fp)

! F is centered on (i,j-.5,k), fifth order stagger

         USE params

         IMPLICIT NONE

!r3 #include "r3_info.h"
!r3 #include "r3_omp.h"

         REAL, INTENT (in)  :: f(mx,my,mz)

         REAL               :: fp(mx,my,mz)

!HPF$ DISTRIBUTE (*,*,block) :: f, fp

         INTEGER            :: i, j, k
         REAL               :: a, a1st, b, c


!r3          CALL r3_info_begin (r3_info_index_0, 'CPU_yup_set')

         IF (my .LT. 3) THEN

!$OMP PARALLEL PRIVATE (k)
            DO k = izs, ize
               fp(:,:,k) = f(:,:,k)
            END DO
!$OMP END PARALLEL

!r3             CALL r3_info_end (r3_info_index_0)

            RETURN

         END IF

         c    = 3.0 / 256.0
         b    = -25.0 / 256.0
         a    = 0.5 - b - c
         a1st = 0.5

!$OMP PARALLEL PRIVATE (i,j,k)
         DO k = izs, ize
            DO i = 1, mx
               fp(i,my-2,k) = (a * (f(i,my-2,k) + f(i,my-1,k)) + b *
     .                        (f(i,my-3,k) + f(i,my,k)) + c *
     .                        (f(i,my-4,k) + f(i,1,k)))
               fp(i,my-1,k) = (a * (f(i,my-1,k) + f(i,my,k)) + b *
     .                        (f(i,my-2,k) + f(i,1,k)) + c *
     .                        (f(i,my-3,k) + f(i,2,k)))
               fp(i,my,k)   = (a * (f(i,my,k) + f(i,1,k)) + b *
     .                        (f(i,my-1,k) + f(i,2,k)) + c *
     .                        (f(i,my-2,k) + f(i,3,k)))
               fp(i,1,k)    = (a * (f(i,1,k) + f(i,2,k)) + b *
     .                        (f(i,my,k) + f(i,3,k)) + c *
     .                        (f(i,my-1,k) + f(i,4,k)))
               fp(i,2,k)    = (a * (f(i,2,k) + f(i,3,k)) + b *
     .                        (f(i,1,k) + f(i,4,k)) + c * (f(i,my,k) +
     .                        f(i,5,k)))
            END DO
            DO j = 3, my - 3
               DO i = 1, mx
                  fp(i,j,k) = (a * (f(i,j,k) + f(i,j+1,k)) + b *
     .                        (f(i,j-1,k) + f(i,j+2,k)) + c *
     .                        (f(i,j-2,k) + f(i,j+3,k)))
               END DO
            END DO
         END DO
!$OMP END PARALLEL

         nstag = nstag + 1
         nflop = nflop + 8

!r3          CALL r3_info_end (r3_info_index_0)

         RETURN
         END SUBROUTINE

! 

         SUBROUTINE yup1_set (f, fp)

! F is centered on (i,j-.5,k), fifth order stagger

         USE params

         IMPLICIT NONE

!r3 #include "r3_info.h"
!r3 #include "r3_omp.h"

         REAL, INTENT (in)  :: f(mx,my,mz)

         REAL               :: fp(mx,my,mz)

!HPF$ DISTRIBUTE (*,*,block) :: f, fp

         INTEGER            :: i, j, k
         REAL               :: a


!r3          CALL r3_info_begin (r3_info_index_0, 'CPU_yup1_set')

         IF (my .LT. 3) THEN

!$OMP PARALLEL PRIVATE (k)
            DO k = izs, ize
               fp(:,:,k) = f(:,:,k)
            END DO
!$OMP END PARALLEL

!r3             CALL r3_info_end (r3_info_index_0)

            RETURN

         END IF

         a = 0.5

!$OMP PARALLEL PRIVATE (i,j,k)
         DO k = izs, ize
            DO i = 1, mx
               fp(i,my,k) = (a * (f(i,my,k) + f(i,1,k)))
            END DO
            DO j = 1, my - 1
               DO i = 1, mx
                  fp(i,j,k) = (a * (f(i,j,k) + f(i,j+1,k)))
               END DO
            END DO
         END DO
!$OMP END PARALLEL

         nstag1 = nstag1 + 1
         nflop  = nflop + 2

!r3          CALL r3_info_end (r3_info_index_0)

         RETURN
         END SUBROUTINE

! 

         SUBROUTINE ydn_set (f, fp)

! F is centered on (i,j,k), fifth order stagger

         USE params

         IMPLICIT NONE

!r3 #include "r3_info.h"
!r3 #include "r3_omp.h"

         REAL               :: f(mx,my,mz), fp(mx,my,mz)

!HPF$ DISTRIBUTE (*,*,block) :: f, fp

         INTEGER            :: i, j, k
         REAL               :: a, a1st, b, c


!r3          CALL r3_info_begin (r3_info_index_0, 'CPU_ydn_set')

         IF (my .LT. 3) THEN

!$OMP PARALLEL PRIVATE (k)
            DO k = izs, ize
               fp(:,:,k) = f(:,:,k)
            END DO
!$OMP END PARALLEL

!r3             CALL r3_info_end (r3_info_index_0)

            RETURN

         END IF

         c    = 3.0 / 256.0
         b    = -25.0 / 256.0
         a    = 0.5 - b - c
         a1st = 0.5

!$OMP PARALLEL PRIVATE (i,j,k)
         DO k = izs, ize
            DO i = 1, mx
               fp(i,my-1,k) = (a * (f(i,my-2,k) + f(i,my-1,k)) + b *
     .                        (f(i,my-3,k) + f(i,my,k)) + c *
     .                        (f(i,my-4,k) + f(i,1,k)))
               fp(i,my,k)   = (a * (f(i,my-1,k) + f(i,my,k)) + b *
     .                        (f(i,my-2,k) + f(i,1,k)) + c *
     .                        (f(i,my-3,k) + f(i,2,k)))
               fp(i,1,k)    = (a * (f(i,my,k) + f(i,1,k)) + b *
     .                        (f(i,my-1,k) + f(i,2,k)) + c *
     .                        (f(i,my-2,k) + f(i,3,k)))
               fp(i,2,k)    = (a * (f(i,1,k) + f(i,2,k)) + b *
     .                        (f(i,my,k) + f(i,3,k)) + c *
     .                        (f(i,my-1,k) + f(i,4,k)))
               fp(i,3,k)    = (a * (f(i,2,k) + f(i,3,k)) + b *
     .                        (f(i,1,k) + f(i,4,k)) + c * (f(i,my,k) +
     .                        f(i,5,k)))
            END DO
            DO j = 4, my - 2
               DO i = 1, mx
                  fp(i,j,k) = (a * (f(i,j,k) + f(i,j-1,k)) + b *
     .                        (f(i,j+1,k) + f(i,j-2,k)) + c *
     .                        (f(i,j+2,k) + f(i,j-3,k)))
               END DO
            END DO
         END DO
!$OMP END PARALLEL

         nstag = nstag + 1
         nflop = nflop + 8

!r3          CALL r3_info_end (r3_info_index_0)

         RETURN
         END SUBROUTINE

! 

         SUBROUTINE ydn1_set (f, fp)

! F is centered on (i,j,k), fifth order stagger

         USE params

         IMPLICIT NONE

!r3 #include "r3_info.h"
!r3 #include "r3_omp.h"

         REAL               :: f(mx,my,mz), fp(mx,my,mz)

!HPF$ DISTRIBUTE (*,*,block) :: f, fp

         INTEGER            :: i, j, k
         REAL               :: a


!r3          CALL r3_info_begin (r3_info_index_0, 'CPU_ydn1_set')

         IF (my .LT. 3) THEN

!$OMP PARALLEL PRIVATE (k)
            DO k = izs, ize
               fp(:,:,k) = f(:,:,k)
            END DO
!$OMP END PARALLEL

!r3             CALL r3_info_end (r3_info_index_0)

            RETURN

         END IF

         a = 0.5

!$OMP PARALLEL PRIVATE (i,j,k)
         DO k = izs, ize
            DO i = 1, mx
               fp(i,1,k) = (a * (f(i,my,k) + f(i,1,k)))
            END DO
            DO j = 2, my
               DO i = 1, mx
                  fp(i,j,k) = (a * (f(i,j,k) + f(i,j-1,k)))
               END DO
            END DO
         END DO
!$OMP END PARALLEL

         nstag1 = nstag1 + 1
         nflop  = nflop + 2

!r3          CALL r3_info_end (r3_info_index_0)

         RETURN
         END SUBROUTINE

! 

         SUBROUTINE ddyup_set (f, fp)

         USE params

         IMPLICIT NONE

!r3 #include "r3_info.h"
!r3 #include "r3_omp.h"

         REAL               :: f(mx,my,mz), fp(mx,my,mz)

!HPF$ DISTRIBUTE (*,*,block) :: f, fp

         INTEGER            :: i, j, k
         REAL               :: a, a1st, b, c


!r3          CALL r3_info_begin (r3_info_index_0, 'CPU_ddyup_set')

         IF (my .LT. 3) THEN
            fp = 0.0
!r3             CALL r3_info_end (r3_info_index_0)
            RETURN
         END IF

         c    = (-1.0 + (3.0 ** 5 - 3.0) / (3.0 ** 3 - 3.0)) /
     .          (5.0 ** 5 - 5.0 - 5.0 * (3.0 ** 5 - 3))
         b    = (-1.0 - 120.0 * c) / 24.0
         a    = (1.0 - 3.0 * b - 5.0 * c) / dy
         a1st = 1.0 / dy
         b    = b / dy
         c    = c / dy

!$OMP PARALLEL PRIVATE (i,j,k)
         DO k = izs, ize
            DO i = 1, mx
               fp(i,my-2,k) = (a * (f(i,my-1,k) - f(i,my-2,k)) + b *
     .                        (f(i,my,k) - f(i,my-3,k)) + c *
     .                        (f(i,1,k) - f(i,my-4,k)))
               fp(i,my-1,k) = (a * (f(i,my,k) - f(i,my-1,k)) + b *
     .                        (f(i,1,k) - f(i,my-2,k)) + c * (f(i,2,k) -
     .                        f(i,my-3,k)))
               fp(i,my,k)   = (a * (f(i,1,k) - f(i,my,k)) + b *
     .                        (f(i,2,k) - f(i,my-1,k)) + c * (f(i,3,k) -
     .                        f(i,my-2,k)))
               fp(i,1,k)    = (a * (f(i,2,k) - f(i,1,k)) + b *
     .                        (f(i,3,k) - f(i,my,k)) + c * (f(i,4,k) -
     .                        f(i,my-1,k)))
               fp(i,2,k)    = (a * (f(i,3,k) - f(i,2,k)) + b *
     .                        (f(i,4,k) - f(i,1,k)) + c * (f(i,5,k) -
     .                        f(i,my,k)))
            END DO
            DO j = 3, my - 3
               DO i = 1, mx
                  fp(i,j,k) = (a * (f(i,j+1,k) - f(i,j,k)) + b *
     .                        (f(i,j+2,k) - f(i,j-1,k)) + c *
     .                        (f(i,j+3,k) - f(i,j-2,k)))
               END DO
            END DO
         END DO
!$OMP END PARALLEL

         nstag = nstag + 1
         nflop = nflop + 8

!r3          CALL r3_info_end (r3_info_index_0)

         RETURN
         END SUBROUTINE

! 

         SUBROUTINE ddyup1_set (f, fp)

         USE params

         IMPLICIT NONE

!r3 #include "r3_info.h"
!r3 #include "r3_omp.h"

         REAL               :: f(mx,my,mz), fp(mx,my,mz)

!HPF$ DISTRIBUTE (*,*,block) :: f, fp

         INTEGER            :: i, j, k
         REAL               :: a


!r3          CALL r3_info_begin (r3_info_index_0, 'CPU_ddyup1_set')

         IF (my .LT. 3) THEN
            fp = 0.0
!r3             CALL r3_info_end (r3_info_index_0)
            RETURN
         END IF
         a = 1.0 / dy

!$OMP PARALLEL PRIVATE (i,j,k)
         DO k = izs, ize
            DO i = 1, mx
               fp(i,my,k) = (a * (f(i,1,k) - f(i,my,k)))
            END DO
            DO j = 1, my - 1
               DO i = 1, mx
                  fp(i,j,k) = (a * (f(i,j+1,k) - f(i,j,k)))
               END DO
            END DO
         END DO
!$OMP END PARALLEL

         nstag1 = nstag1 + 1
         nflop  = nflop + 2

!r3          CALL r3_info_end (r3_info_index_0)

         RETURN
         END SUBROUTINE

! 

         SUBROUTINE ddydn_set (f, fp)

         USE params

         IMPLICIT NONE

!r3 #include "r3_info.h"
!r3 #include "r3_omp.h"

         REAL               :: f(mx,my,mz), fp(mx,my,mz)

!HPF$ DISTRIBUTE (*,*,block) :: f, fp

         INTEGER            :: i, j, k
         REAL               :: a, a1st, b, c


!r3          CALL r3_info_begin (r3_info_index_0, 'CPU_ddydn_set')

         IF (my .LT. 3) THEN
            fp = 0.0
!r3             CALL r3_info_end (r3_info_index_0)
            RETURN
         END IF
         c    = (-1.0 + (3.0 ** 5 - 3.0) / (3.0 ** 3 - 3.0)) /
     .          (5.0 ** 5 - 5.0 - 5.0 * (3.0 ** 5 - 3))
         b    = (-1.0 - 120.0 * c) / 24.0
         a    = (1.0 - 3.0 * b - 5.0 * c) / dy
         a1st = 1.0 / dy
         b    = b / dy
         c    = c / dy

!$OMP PARALLEL PRIVATE (i,j,k)
         DO k = izs, ize
            DO i = 1, mx
               fp(i,my-1,k) = (a * (f(i,my-1,k) - f(i,my-2,k)) + b *
     .                        (f(i,my,k) - f(i,my-3,k)) + c *
     .                        (f(i,1,k) - f(i,my-4,k)))
               fp(i,my,k)   = (a * (f(i,my,k) - f(i,my-1,k)) + b *
     .                        (f(i,1,k) - f(i,my-2,k)) + c * (f(i,2,k) -
     .                        f(i,my-3,k)))
               fp(i,1,k)    = (a * (f(i,1,k) - f(i,my,k)) + b *
     .                        (f(i,2,k) - f(i,my-1,k)) + c * (f(i,3,k) -
     .                        f(i,my-2,k)))
               fp(i,2,k)    = (a * (f(i,2,k) - f(i,1,k)) + b *
     .                        (f(i,3,k) - f(i,my,k)) + c * (f(i,4,k) -
     .                        f(i,my-1,k)))
               fp(i,3,k)    = (a * (f(i,3,k) - f(i,2,k)) + b *
     .                        (f(i,4,k) - f(i,1,k)) + c * (f(i,5,k) -
     .                        f(i,my,k)))
            END DO
            DO j = 4, my - 2
               DO i = 1, mx
                  fp(i,j,k) = (a * (f(i,j,k) - f(i,j-1,k)) + b *
     .                        (f(i,j+1,k) - f(i,j-2,k)) + c *
     .                        (f(i,j+2,k) - f(i,j-3,k)))
               END DO
            END DO
         END DO
!$OMP END PARALLEL

         nstag = nstag + 1
         nflop = nflop + 8

!r3          CALL r3_info_end (r3_info_index_0)

         RETURN
         END SUBROUTINE

! 

         SUBROUTINE ddydn1_set (f, fp)

         USE params

         IMPLICIT NONE

!r3 #include "r3_info.h"
!r3 #include "r3_omp.h"

         REAL               :: f(mx,my,mz), fp(mx,my,mz)

!HPF$ DISTRIBUTE (*,*,block) :: f, fp

         INTEGER            :: i, j, k
         REAL               :: a


!r3          CALL r3_info_begin (r3_info_index_0, 'CPU_ddydn1_set')

         IF (my .LT. 3) THEN
            fp = 0.0
!r3             CALL r3_info_end (r3_info_index_0)
            RETURN
         END IF
         a = 1.0 / dy

!$OMP PARALLEL PRIVATE (i,j,k)
         DO k = izs, ize
            DO i = 1, mx
               fp(i,1,k) = (a * (f(i,1,k) - f(i,my,k)))
            END DO
            DO j = 2, my
               DO i = 1, mx
                  fp(i,j,k) = (a * (f(i,j,k) - f(i,j-1,k)))
               END DO
            END DO
         END DO
!$OMP END PARALLEL

         nstag1 = nstag1 + 1
         nflop  = nflop + 2

!r3          CALL r3_info_end (r3_info_index_0)

         RETURN
         END SUBROUTINE

! 

         SUBROUTINE ddyup_add (f, fp)

         USE params

         IMPLICIT NONE

!r3 #include "r3_info.h"
!r3 #include "r3_omp.h"

         REAL               :: f(mx,my,mz), fp(mx,my,mz)

!HPF$ DISTRIBUTE (*,*,block) :: f, fp

         INTEGER            :: i, j, k
         REAL               :: a, a1st, b, c


!r3          CALL r3_info_begin (r3_info_index_0, 'CPU_ddyup_add')

         IF (my .LT. 3) THEN
            fp = fp + 0.0
!r3             CALL r3_info_end (r3_info_index_0)
            RETURN
         END IF
         c    = (-1.0 + (3.0 ** 5 - 3.0) / (3.0 ** 3 - 3.0)) /
     .          (5.0 ** 5 - 5.0 - 5.0 * (3.0 ** 5 - 3))
         b    = (-1.0 - 120.0 * c) / 24.0
         a    = (1.0 - 3.0 * b - 5.0 * c) / dy
         a1st = 1.0 / dy
         b    = b / dy
         c    = c / dy

!$OMP PARALLEL PRIVATE (i,j,k)
         DO k = izs, ize
            DO i = 1, mx
               fp(i,my-2,k) = fp(i,my-2,k) + (a * (f(i,my-1,k) -
     .                        f(i,my-2,k)) + b * (f(i,my,k) -
     .                        f(i,my-3,k)) + c * (f(i,1,k) -
     .                        f(i,my-4,k)))
               fp(i,my-1,k) = fp(i,my-1,k) + (a * (f(i,my,k) -
     .                        f(i,my-1,k)) + b * (f(i,1,k) -
     .                        f(i,my-2,k)) + c * (f(i,2,k) -
     .                        f(i,my-3,k)))
               fp(i,my,k)   = fp(i,my,k) + (a * (f(i,1,k) - f(i,my,k)) +
     .                        b * (f(i,2,k) - f(i,my-1,k)) + c *
     .                        (f(i,3,k) - f(i,my-2,k)))
               fp(i,1,k)    = fp(i,1,k) + (a * (f(i,2,k) - f(i,1,k)) +
     .                        b * (f(i,3,k) - f(i,my,k)) + c *
     .                        (f(i,4,k) - f(i,my-1,k)))
               fp(i,2,k)    = fp(i,2,k) + (a * (f(i,3,k) - f(i,2,k)) +
     .                        b * (f(i,4,k) - f(i,1,k)) + c *
     .                        (f(i,5,k) - f(i,my,k)))
            END DO
            DO j = 3, my - 3
               DO i = 1, mx
                  fp(i,j,k) = fp(i,j,k) + (a * (f(i,j+1,k) - f(i,j,k)) +
     .                        b * (f(i,j+2,k) - f(i,j-1,k)) + c *
     .                        (f(i,j+3,k) - f(i,j-2,k)))
               END DO
            END DO
         END DO
!$OMP END PARALLEL

         nstag = nstag + 1
         nflop = nflop + 8

!r3          CALL r3_info_end (r3_info_index_0)

         RETURN
         END SUBROUTINE

! 

         SUBROUTINE ddyup1_add (f, fp)

         USE params

         IMPLICIT NONE

!r3 #include "r3_info.h"
!r3 #include "r3_omp.h"

         REAL               :: f(mx,my,mz), fp(mx,my,mz)

!HPF$ DISTRIBUTE (*,*,block) :: f, fp

         INTEGER            :: i, j, k
         REAL               :: a


!r3          CALL r3_info_begin (r3_info_index_0, 'CPU_ddyup1_add')

         IF (my .LT. 3) THEN
            fp = fp + 0.0
!r3             CALL r3_info_end (r3_info_index_0)
            RETURN
         END IF
         a = 1.0 / dy

!$OMP PARALLEL PRIVATE (i,j,k)
         DO k = izs, ize
            DO i = 1, mx
               fp(i,my,k) = fp(i,my,k) + (a * (f(i,1,k) - f(i,my,k)))
            END DO
            DO j = 1, my - 1
               DO i = 1, mx
                  fp(i,j,k) = fp(i,j,k) + (a * (f(i,j+1,k) - f(i,j,k)))
               END DO
            END DO
         END DO
!$OMP END PARALLEL

         nstag1 = nstag1 + 1
         nflop  = nflop + 2

!r3          CALL r3_info_end (r3_info_index_0)

         RETURN
         END SUBROUTINE

! 

         SUBROUTINE zup_set (f, fp)

! F is centered on (i,j,k-.5), fifth order stagger

         USE params

         IMPLICIT NONE

!r3 #include "r3_info.h"
!r3 #include "r3_omp.h"

         REAL               :: f(mx,my,mz), fp(mx,my,mz)

!HPF$ DISTRIBUTE (*,*,block) :: f, fp

         INTEGER            :: i, j, k
         REAL               :: a, a1st, b, c


!r3          CALL r3_info_begin (r3_info_index_0, 'CPU_zup_set')

         c    = 3.0 / 256.0
         b    = -25.0 / 256.0
         a    = 0.5 - b - c
         a1st = 0.5

!$OMP PARALLEL PRIVATE (i,j,k)
         DO k = MAX (3, izs), MIN (mz - 3, ize)
            DO j = 1, my
               DO i = 1, mx
                  fp(i,j,k) = (a * (f(i,j,k+1) + f(i,j,k)) + b *
     .                        (f(i,j,k+2) + f(i,j,k-1)) + c *
     .                        (f(i,j,k+3) + f(i,j,k-2)))
               END DO
            END DO
         END DO

         DO k = MAX (mz - 2, izs), MIN (mz - 2, ize)
            DO j = 1, my
               DO i = 1, mx
                  fp(i,j,mz-2) = (a * (f(i,j,mz-1) + f(i,j,mz-2)) + b *
     .                           (f(i,j,mz) + f(i,j,mz-3)) + c *
     .                           (f(i,j,1) + f(i,j,mz-4)))
               END DO
            END DO
         END DO
         DO k = MAX (mz - 1, izs), MIN (mz - 1, ize)
            DO j = 1, my
               DO i = 1, mx
                  fp(i,j,mz-1) = (a * (f(i,j,mz) + f(i,j,mz-1)) + b *
     .                           (f(i,j,1) + f(i,j,mz-2)) + c *
     .                           (f(i,j,2) + f(i,j,mz-3)))
               END DO
            END DO
         END DO
         DO k = MAX (mz, izs), MIN (mz, ize)
            DO j = 1, my
               DO i = 1, mx
                  fp(i,j,mz) = (a * (f(i,j,1) + f(i,j,mz)) + b *
     .                         (f(i,j,1+1) + f(i,j,mz-1)) + c *
     .                         (f(i,j,1+2) + f(i,j,mz-2)))
               END DO
            END DO
         END DO
         DO k = MAX (1, izs), MIN (1, ize)
            DO j = 1, my
               DO i = 1, mx
                  fp(i,j,1) = (a * (f(i,j,2) + f(i,j,1)) + b *
     .                        (f(i,j,2+1) + f(i,j,mz)) + c *
     .                        (f(i,j,2+2) + f(i,j,mz-1)))
               END DO
            END DO
         END DO
         DO k = MAX (2, izs), MIN (2, ize)
            DO j = 1, my
               DO i = 1, mx
                  fp(i,j,2) = (a * (f(i,j,3) + f(i,j,2)) + b *
     .                        (f(i,j,3+1) + f(i,j,1)) + c *
     .                        (f(i,j,3+2) + f(i,j,mz)))
               END DO
            END DO
         END DO
!$OMP END PARALLEL

         nstag = nstag + 1
         nflop = nflop + 8

!r3          CALL r3_info_end (r3_info_index_0)

         RETURN
         END SUBROUTINE

! 

         SUBROUTINE zup1_set (f, fp)

! F is centered on (i,j,k-.5), fifth order stagger

         USE params

         IMPLICIT NONE

!r3 #include "r3_info.h"
!r3 #include "r3_omp.h"

         REAL               :: f(mx,my,mz), fp(mx,my,mz)

!HPF$ DISTRIBUTE (*,*,block) :: f, fp

         INTEGER            :: i, j, k
         REAL               :: a


!r3          CALL r3_info_begin (r3_info_index_0, 'CPU_zup1_set')

         a = 0.5

!$OMP PARALLEL PRIVATE (i,j,k)
         DO k = izs, MIN (mz - 1, ize)
            DO j = 1, my
               DO i = 1, mx
                  fp(i,j,k) = (a * (f(i,j,k+1) + f(i,j,k)))
               END DO
            END DO
         END DO
         DO k = MAX (mz, izs), MIN (mz, ize)
            DO j = 1, my
               DO i = 1, mx
                  fp(i,j,mz) = (a * (f(i,j,1) + f(i,j,mz)))
               END DO
            END DO
         END DO
!$OMP END PARALLEL

         nstag1 = nstag1 + 1
         nflop  = nflop + 2

!r3          CALL r3_info_end (r3_info_index_0)

         RETURN
         END SUBROUTINE

! 

         SUBROUTINE zdn_set (f, fp)

! F is centered on (i,j,k), fifth order stagger

         USE params

         IMPLICIT NONE

!r3 #include "r3_info.h"
!r3 #include "r3_omp.h"

         REAL               :: f(mx,my,mz), fp(mx,my,mz)

!HPF$ DISTRIBUTE (*,*,block) :: f, fp

         INTEGER            :: i, j, k
         REAL               :: a, a1st, b, c


!r3          CALL r3_info_begin (r3_info_index_0, 'CPU_zdn_set')

         c    = 3.0 / 256.0
         b    = -25.0 / 256.0
         a    = 0.5 - b - c
         a1st = 0.5

!$OMP PARALLEL PRIVATE (i,j,k)
         DO k = MAX (4, izs), MIN (mz - 2, ize)
            DO j = 1, my
               DO i = 1, mx
                  fp(i,j,k) = (a * (f(i,j,k) + f(i,j,k-1)) + b *
     .                        (f(i,j,k+1) + f(i,j,k-2)) + c *
     .                        (f(i,j,k+2) + f(i,j,k-3)))
               END DO
            END DO
         END DO

         DO k = MAX (mz - 1, izs), MIN (mz - 1, ize)
            DO j = 1, my
               DO i = 1, mx
                  fp(i,j,mz-1) = (a * (f(i,j,mz-1) + f(i,j,mz-2)) + b *
     .                           (f(i,j,mz) + f(i,j,mz-3)) + c *
     .                           (f(i,j,1) + f(i,j,mz-4)))
               END DO
            END DO
         END DO
         DO k = MAX (mz, izs), MIN (mz, ize)
            DO j = 1, my
               DO i = 1, mx
                  fp(i,j,mz) = (a * (f(i,j,mz) + f(i,j,mz-1)) + b *
     .                         (f(i,j,1) + f(i,j,mz-2)) + c *
     .                         (f(i,j,2) + f(i,j,mz-3)))
               END DO
            END DO
         END DO
         DO k = MAX (1, izs), MIN (1, ize)
            DO j = 1, my
               DO i = 1, mx
                  fp(i,j,1) = (a * (f(i,j,1) + f(i,j,mz)) + b *
     .                        (f(i,j,1+1) + f(i,j,mz-1)) + c *
     .                        (f(i,j,1+2) + f(i,j,mz-2)))
               END DO
            END DO
         END DO
         DO k = MAX (2, izs), MIN (2, ize)
            DO j = 1, my
               DO i = 1, mx
                  fp(i,j,2) = (a * (f(i,j,2) + f(i,j,1)) + b *
     .                        (f(i,j,2+1) + f(i,j,mz)) + c *
     .                        (f(i,j,2+2) + f(i,j,mz-1)))
               END DO
            END DO
         END DO
         DO k = MAX (3, izs), MIN (3, ize)
            DO j = 1, my
               DO i = 1, mx
                  fp(i,j,3) = (a * (f(i,j,3) + f(i,j,2)) + b *
     .                        (f(i,j,3+1) + f(i,j,1)) + c *
     .                        (f(i,j,3+2) + f(i,j,mz)))
               END DO
            END DO
         END DO
!$OMP END PARALLEL

         nstag = nstag + 1
         nflop = nflop + 8

!r3          CALL r3_info_end (r3_info_index_0)

         RETURN
         END SUBROUTINE

! 

         SUBROUTINE zdn1_set (f, fp)

! F is centered on (i,j,k), fifth order stagger

         USE params

         IMPLICIT NONE

!r3 #include "r3_info.h"
!r3 #include "r3_omp.h"

         REAL               :: f(mx,my,mz), fp(mx,my,mz)

!HPF$ DISTRIBUTE (*,*,block) :: f, fp

         INTEGER            :: i, j, k
         REAL               :: a


!r3          CALL r3_info_begin (r3_info_index_0, 'CPU_zdn1_set')

         a = 0.5

!$OMP PARALLEL PRIVATE (i,j,k)
         DO k = MAX (2, izs), ize
            DO j = 1, my
               DO i = 1, mx
                  fp(i,j,k) = (a * (f(i,j,k) + f(i,j,k-1)))
               END DO
            END DO
         END DO
         DO k = MAX (1, izs), MIN (1, ize)
            DO j = 1, my
               DO i = 1, mx
                  fp(i,j,1) = (a * (f(i,j,1) + f(i,j,mz)))
               END DO
            END DO
         END DO
!$OMP END PARALLEL

         nstag1 = nstag1 + 1
         nflop  = nflop + 2

!r3          CALL r3_info_end (r3_info_index_0)

         RETURN
         END SUBROUTINE

! 

         SUBROUTINE ddzup_set (f, fp)

         USE params

         IMPLICIT NONE

!r3 #include "r3_info.h"
!r3 #include "r3_omp.h"

         REAL               :: f(mx,my,mz), fp(mx,my,mz)

!HPF$ DISTRIBUTE (*,*,block) :: f, fp

         INTEGER            :: i, j, k
         REAL               :: a, a1st, b, c


!r3          CALL r3_info_begin (r3_info_index_0, 'CPU_ddzup_set')

         c    = (-1.0 + (3.0 ** 5 - 3.0) / (3.0 ** 3 - 3.0)) /
     .          (5.0 ** 5 - 5.0 - 5.0 * (3.0 ** 5 - 3))
         b    = (-1.0 - 120.0 * c) / 24.0
         a    = (1.0 - 3.0 * b - 5.0 * c) / dz
         a1st = 1.0 / dz
         b    = b / dz
         c    = c / dz

!$OMP PARALLEL PRIVATE (i,j,k)
         DO k = MAX (3, izs), MIN (mz - 3, ize)
            DO j = 1, my
               DO i = 1, mx
                  fp(i,j,k) = (a * (f(i,j,k+1) - f(i,j,k)) + b *
     .                        (f(i,j,k+2) - f(i,j,k-1)) + c *
     .                        (f(i,j,k+3) - f(i,j,k-2)))
               END DO
            END DO
         END DO

         DO k = MAX (mz - 2, izs), MIN (mz - 2, ize)
            DO j = 1, my
               DO i = 1, mx
                  fp(i,j,mz-2) = (a * (f(i,j,mz-1) - f(i,j,mz-2)) + b *
     .                           (f(i,j,mz) - f(i,j,mz-3)) + c *
     .                           (f(i,j,1) - f(i,j,mz-4)))
               END DO
            END DO
         END DO
         DO k = MAX (mz - 1, izs), MIN (mz - 1, ize)
            DO j = 1, my
               DO i = 1, mx
                  fp(i,j,mz-1) = (a * (f(i,j,mz) - f(i,j,mz-1)) + b *
     .                           (f(i,j,1) - f(i,j,mz-2)) + c *
     .                           (f(i,j,2) - f(i,j,mz-3)))
               END DO
            END DO
         END DO
         DO k = MAX (mz, izs), MIN (mz, ize)
            DO j = 1, my
               DO i = 1, mx
                  fp(i,j,mz) = (a * (f(i,j,1) - f(i,j,mz)) + b *
     .                         (f(i,j,1+1) - f(i,j,mz-1)) + c *
     .                         (f(i,j,1+2) - f(i,j,mz-2)))
               END DO
            END DO
         END DO
         DO k = MAX (1, izs), MIN (1, ize)
            DO j = 1, my
               DO i = 1, mx
                  fp(i,j,1) = (a * (f(i,j,2) - f(i,j,1)) + b *
     .                        (f(i,j,2+1) - f(i,j,mz)) + c *
     .                        (f(i,j,2+2) - f(i,j,mz-1)))
               END DO
            END DO
         END DO
         DO k = MAX (2, izs), MIN (2, ize)
            DO j = 1, my
               DO i = 1, mx
                  fp(i,j,2) = (a * (f(i,j,3) - f(i,j,2)) + b *
     .                        (f(i,j,3+1) - f(i,j,1)) + c *
     .                        (f(i,j,3+2) - f(i,j,mz)))
               END DO
            END DO
         END DO
!$OMP END PARALLEL

         nstag = nstag + 1
         nflop = nflop + 8

!r3          CALL r3_info_end (r3_info_index_0)

         RETURN
         END SUBROUTINE

! 

         SUBROUTINE ddzup1_set (f, fp)

         USE params

         IMPLICIT NONE

!r3 #include "r3_info.h"
!r3 #include "r3_omp.h"

         REAL               :: f(mx,my,mz), fp(mx,my,mz)

!HPF$ DISTRIBUTE (*,*,block) :: f, fp

         INTEGER            :: i, j, k
         REAL               :: a


!r3          CALL r3_info_begin (r3_info_index_0, 'CPU_ddzup1_set')

         a = 1.0 / dz

!$OMP PARALLEL PRIVATE (i,j,k)
         DO k = izs, MIN (mz - 1, ize)
            DO j = 1, my
               DO i = 1, mx
                  fp(i,j,k) = (a * (f(i,j,k+1) - f(i,j,k)))
               END DO
            END DO
         END DO
         DO k = MAX (mz, izs), MIN (mz, ize)
            DO j = 1, my
               DO i = 1, mx
                  fp(i,j,mz) = (a * (f(i,j,1) - f(i,j,mz)))
               END DO
            END DO
         END DO
!$OMP END PARALLEL

         nstag1 = nstag1 + 1
         nflop  = nflop + 2

!r3          CALL r3_info_end (r3_info_index_0)

         RETURN
         END SUBROUTINE

! 

         SUBROUTINE ddzdn_set (f, fp)

         USE params

         IMPLICIT NONE

!r3 #include "r3_info.h"
!r3 #include "r3_omp.h"

         REAL               :: f(mx,my,mz), fp(mx,my,mz)

!HPF$ DISTRIBUTE (*,*,block) :: f, fp

         INTEGER            :: i, j, k
         REAL               :: a, a1st, b, c


!r3          CALL r3_info_begin (r3_info_index_0, 'CPU_ddzdn_set')

         c    = (-1.0 + (3.0 ** 5 - 3.0) / (3.0 ** 3 - 3.0)) /
     .          (5.0 ** 5 - 5.0 - 5.0 * (3.0 ** 5 - 3))
         b    = (-1.0 - 120.0 * c) / 24.0
         a    = (1.0 - 3.0 * b - 5.0 * c) / dz
         a1st = 1.0 / dz
         b    = b / dz
         c    = c / dz

!$OMP PARALLEL PRIVATE (i,j,k)
         DO k = MAX (4, izs), MIN (mz - 2, ize)
            DO j = 1, my
               DO i = 1, mx
                  fp(i,j,k) = (a * (f(i,j,k) - f(i,j,k-1)) + b *
     .                        (f(i,j,k+1) - f(i,j,k-2)) + c *
     .                        (f(i,j,k+2) - f(i,j,k-3)))
               END DO
            END DO
         END DO
         DO k = MAX (mz - 1, izs), MIN (mz - 1, ize)
            DO j = 1, my
               DO i = 1, mx
                  fp(i,j,mz-1) = (a * (f(i,j,mz-1) - f(i,j,mz-2)) + b *
     .                           (f(i,j,mz) - f(i,j,mz-3)) + c *
     .                           (f(i,j,1) - f(i,j,mz-4)))
               END DO
            END DO
         END DO
         DO k = MAX (mz, izs), MIN (mz, ize)
            DO j = 1, my
               DO i = 1, mx
                  fp(i,j,mz) = (a * (f(i,j,mz) - f(i,j,mz-1)) + b *
     .                         (f(i,j,1) - f(i,j,mz-2)) + c *
     .                         (f(i,j,2) - f(i,j,mz-3)))
               END DO
            END DO
         END DO
         DO k = MAX (1, izs), MIN (1, ize)
            DO j = 1, my
               DO i = 1, mx
                  fp(i,j,1) = (a * (f(i,j,1) - f(i,j,mz)) + b *
     .                        (f(i,j,1+1) - f(i,j,mz-1)) + c *
     .                        (f(i,j,1+2) - f(i,j,mz-2)))
               END DO
            END DO
         END DO
         DO k = MAX (2, izs), MIN (2, ize)
            DO j = 1, my
               DO i = 1, mx
                  fp(i,j,2) = (a * (f(i,j,2) - f(i,j,1)) + b *
     .                        (f(i,j,2+1) - f(i,j,mz)) + c *
     .                        (f(i,j,2+2) - f(i,j,mz-1)))
               END DO
            END DO
         END DO
         DO k = MAX (3, izs), MIN (3, ize)
            DO j = 1, my
               DO i = 1, mx
                  fp(i,j,3) = (a * (f(i,j,3) - f(i,j,2)) + b *
     .                        (f(i,j,3+1) - f(i,j,1)) + c *
     .                        (f(i,j,3+2) - f(i,j,mz)))
               END DO
            END DO
         END DO
!$OMP END PARALLEL

         nstag = nstag + 1
         nflop = nflop + 8

!r3          CALL r3_info_end (r3_info_index_0)

         RETURN
         END SUBROUTINE

! 

         SUBROUTINE ddzdn1_set (f, fp)

         USE params

         IMPLICIT NONE

!r3 #include "r3_info.h"
!r3 #include "r3_omp.h"

         REAL               :: f(mx,my,mz), fp(mx,my,mz)

!HPF$ DISTRIBUTE (*,*,block) :: f, fp

         INTEGER            :: i, j, k
         REAL               :: a


!r3          CALL r3_info_begin (r3_info_index_0, 'CPU_ddzdn1_set')

         a = 1.0 / dz

!$OMP PARALLEL PRIVATE (i,j,k)
         DO k = MAX (2, izs), ize
            DO j = 1, my
               DO i = 1, mx
                  fp(i,j,k) = (a * (f(i,j,k) - f(i,j,k-1)))
               END DO
            END DO
         END DO
         DO k = MAX (1, izs), MIN (1, ize)
            DO j = 1, my
               DO i = 1, mx
                  fp(i,j,1) = (a * (f(i,j,1) - f(i,j,mz)))
               END DO
            END DO
         END DO
!$OMP END PARALLEL

         nstag1 = nstag1 + 1
         nflop  = nflop + 2

!r3          CALL r3_info_end (r3_info_index_0)

         RETURN
         END SUBROUTINE

! 

         SUBROUTINE ddzup_add (f, fp)

         USE params

         IMPLICIT NONE

!r3 #include "r3_info.h"
!r3 #include "r3_omp.h"

         REAL               :: f(mx,my,mz), fp(mx,my,mz)

!HPF$ DISTRIBUTE (*,*,block) :: f, fp

         INTEGER            :: i, j, k
         REAL               :: a, a1st, b, c


!r3          CALL r3_info_begin (r3_info_index_0, 'CPU_ddzup_add')

         c    = (-1.0 + (3.0 ** 5 - 3.0) / (3.0 ** 3 - 3.0)) /
     .          (5.0 ** 5 - 5.0 - 5.0 * (3.0 ** 5 - 3))
         b    = (-1.0 - 120.0 * c) / 24.0
         a    = (1.0 - 3.0 * b - 5.0 * c) / dz
         a1st = 1.0 / dz
         b    = b / dz
         c    = c / dz

!$OMP PARALLEL PRIVATE (i,j,k)
         DO k = MAX (3, izs), MIN (mz - 3, ize)
            DO j = 1, my
               DO i = 1, mx
                  fp(i,j,k) = fp(i,j,k) + (a * (f(i,j,k+1) - f(i,j,k)) +
     .                        b * (f(i,j,k+2) - f(i,j,k-1)) + c *
     .                        (f(i,j,k+3) - f(i,j,k-2)))
               END DO
            END DO
         END DO
         DO k = MAX (mz - 2, izs), MIN (mz - 2, ize)
            DO j = 1, my
               DO i = 1, mx
                  fp(i,j,mz-2) = fp(i,j,mz-2) + (a * (f(i,j,mz-1) -
     .                           f(i,j,mz-2)) + b * (f(i,j,mz) -
     .                           f(i,j,mz-3)) + c * (f(i,j,1) -
     .                           f(i,j,mz-4)))
               END DO
            END DO
         END DO
         DO k = MAX (mz - 1, izs), MIN (mz - 1, ize)
            DO j = 1, my
               DO i = 1, mx
                  fp(i,j,mz-1) = fp(i,j,mz-1) + (a * (f(i,j,mz) -
     .                           f(i,j,mz-1)) + b * (f(i,j,1) -
     .                           f(i,j,mz-2)) + c * (f(i,j,2) -
     .                           f(i,j,mz-3)))
               END DO
            END DO
         END DO
         DO k = MAX (mz, izs), MIN (mz, ize)
            DO j = 1, my
               DO i = 1, mx
                  fp(i,j,mz) = fp(i,j,mz) + (a * (f(i,j,1) -
     .                         f(i,j,mz)) + b * (f(i,j,1+1) -
     .                         f(i,j,mz-1)) + c * (f(i,j,1+2) -
     .                         f(i,j,mz-2)))
               END DO
            END DO
         END DO
         DO k = MAX (1, izs), MIN (1, ize)
            DO j = 1, my
               DO i = 1, mx
                  fp(i,j,1) = fp(i,j,1) + (a * (f(i,j,2) - f(i,j,1)) +
     .                        b * (f(i,j,2+1) - f(i,j,mz)) + c *
     .                        (f(i,j,2+2) - f(i,j,mz-1)))
               END DO
            END DO
         END DO
         DO k = MAX (2, izs), MIN (2, ize)
            DO j = 1, my
               DO i = 1, mx
                  fp(i,j,2) = fp(i,j,2) + (a * (f(i,j,3) - f(i,j,2)) +
     .                        b * (f(i,j,3+1) - f(i,j,1)) + c *
     .                        (f(i,j,3+2) - f(i,j,mz)))
               END DO
            END DO
         END DO
!$OMP END PARALLEL

         nstag = nstag + 1
         nflop = nflop + 8

!r3          CALL r3_info_end (r3_info_index_0)

         RETURN
         END SUBROUTINE

! 

         SUBROUTINE ddzup1_add (f, fp)

         USE params

         IMPLICIT NONE

!r3 #include "r3_info.h"
!r3 #include "r3_omp.h"

         REAL               :: f(mx,my,mz), fp(mx,my,mz)

!HPF$ DISTRIBUTE (*,*,block) :: f, fp

         INTEGER            :: i, j, k
         REAL               :: a


!r3          CALL r3_info_begin (r3_info_index_0, 'CPU_ddzup1_add')

         a = 1.0 / dz

!$OMP PARALLEL PRIVATE (i,j,k)
         DO k = izs, MIN (mz - 1, ize)
            DO j = 1, my
               DO i = 1, mx
                  fp(i,j,k) = fp(i,j,k) + (a * (f(i,j,k+1) - f(i,j,k)))
               END DO
            END DO
         END DO
         DO k = MAX (mz, izs), MIN (mz, ize)
            DO j = 1, my
               DO i = 1, mx
                  fp(i,j,mz) = fp(i,j,mz) + (a * (f(i,j,1) - f(i,j,mz)))
               END DO
            END DO
         END DO
!$OMP END PARALLEL

         nstag1 = nstag1 + 1
         nflop  = nflop + 2

!r3          CALL r3_info_end (r3_info_index_0)

         RETURN
         END SUBROUTINE

! 

         SUBROUTINE smooth3_set (f, smooth3)

! Three point smooth

         USE params

         IMPLICIT NONE

!r3 #include "r3_info.h"
!r3 #include "r3_omp.h"

         INTEGER            :: i, j, k, km1, kp1
         REAL               :: f(mx,my,mz), smooth3(mx,my,mz)

!HPF$ DISTRIBUTE (*,*,block) :: f, smooth3

         REAL               :: fx(0:mx+1), fy(0:my+1), fz(mx,my,mz)

!HPF$ DISTRIBUTE (*,*,block) :: fz

         REAL               :: c3


!r3          CALL r3_info_begin (r3_info_index_0, 'CPU_smooth3_set')

         c3 = 1.0 / 3.0

!$OMP PARALLEL PRIVATE (i,j,k,fx)
         DO k = izs, ize
            DO j = 1, my
               DO i = 1, mx
                  fx(i) = f(i,j,k)
               END DO
               fx(0)    = fx(mx)
               fx(mx+1) = fx(1)
               DO i = 1, mx
                  fz(i,j,k) = c3 * (fx(i-1) + fx(i) + fx(i+1))
               END DO
            END DO
         END DO
!$OMP END PARALLEL

!$OMP PARALLEL PRIVATE (i,j,k,fy)
         DO k = izs, ize
            DO i = 1, mx
               DO j = 1, my
                  fy(j) = fz(i,j,k)
               END DO
               fy(0)    = fy(my)
               fy(my+1) = fy(1)
               DO j = 1, my
                  fz(i,j,k) = c3 * (fy(j-1) + fy(j) + fy(j+1))
               END DO
            END DO
         END DO
!$OMP END PARALLEL

!$OMP PARALLEL PRIVATE (i,j,k,km1,kp1)
         DO k = izs, ize
            km1 = MOD (k + mz - 2, mz) + 1
            kp1 = MOD (k + mz, mz) + 1
            DO j = 1, my
               DO i = 1, mx
                  smooth3(i,j,k) = c3 * (fz(i,j,k) + fz(i,j,km1) +
     .                             fz(i,j,kp1))
               END DO
            END DO
         END DO
!$OMP END PARALLEL

!r3          CALL r3_info_end (r3_info_index_0)

         RETURN
         END SUBROUTINE

! 

         SUBROUTINE max5_set (f, max5)

! Three point max

         USE params

         IMPLICIT NONE

!r3 #include "r3_info.h"
!r3 #include "r3_omp.h"

         REAL, INTENT (in)  :: f(mx,my,mz)

         REAL               :: max5(mx,my,mz)

!HPF$ DISTRIBUTE (*,*,block) :: f, max5

         REAL               :: fx(-1:mx+2), fy(-1:my+2), fz(mx,my,mz)

!HPF$ DISTRIBUTE (*,*,block) :: fz

         INTEGER            :: i, j, k, km1, kp1, km2, kp2


!r3          CALL r3_info_begin (r3_info_index_0, 'CPU_max5_set')

!$OMP PARALLEL PRIVATE (i,j,k,fx)
         DO k = izs, ize
            DO j = 1, my
               DO i = 1, mx
                  fx(i) = f(i,j,k)
               END DO
               fx(-1)   = fx(mx-1)
               fx(0)    = fx(mx)
               fx(mx+1) = fx(1)
               fx(mx+2) = fx(2)
               DO i = 1, mx
                  fz(i,j,k) = AMAX1 (fx(i-2), fx(i-1), fx(i), fx(i+1),
     .                        fx(i+2))
               END DO
            END DO
         END DO
!$OMP END PARALLEL

!$OMP PARALLEL PRIVATE (i,j,k,fy)
         DO k = izs, ize
            DO i = 1, mx
               DO j = 1, my
                  fy(j) = fz(i,j,k)
               END DO
               fy(-1)   = fy(my-1)
               fy(0)    = fy(my)
               fy(my+1) = fy(1)
               fy(my+2) = fy(2)
               DO j = 1, my
                  fz(i,j,k) = AMAX1 (fy(j-2), fy(j-1), fy(j), fy(j+1),
     .                        fy(j+2))
               END DO
            END DO
         END DO
!$OMP END PARALLEL

!$OMP PARALLEL PRIVATE (i,j,k,km1,kp1,km2,kp2)
         DO k = izs, ize
            km2 = MOD (k + mz - 3, mz) + 1
            km1 = MOD (k + mz - 2, mz) + 1
            kp1 = MOD (k + mz, mz) + 1
            kp2 = MOD (k + mz + 1, mz) + 1
            DO j = 1, my
               DO i = 1, mx
                  max5(i,j,k) = AMAX1 (fz(i,j,km2), fz(i,j,km1),
     .                          fz(i,j,k), fz(i,j,kp1), fz(i,j,kp2))
               END DO
            END DO
         END DO
!$OMP END PARALLEL

!r3          CALL r3_info_end (r3_info_index_0)

         RETURN
         END SUBROUTINE

! 

         SUBROUTINE dif2_set (fx, fy, fz, difu)

! Finite difference one zone left to one zones right.

         USE params

         IMPLICIT NONE

!r3 #include "r3_info.h"
!r3 #include "r3_omp.h"

         REAL               :: cx, cy, cz, fx(mx,my,mz), fy(mx,my,mz),
     .                         fz(mx,my,mz), difu(mx,my,mz)

!HPF$ align with TheCube:: fx, fy, fz, difu

         INTEGER            :: i, j, k


!r3          CALL r3_info_begin (r3_info_index_0, 'CPU_dif2_set')

         cx = 0.5 / dx
         cy = 0.5 / dy
         cz = 0.5 / dz

!$OMP PARALLEL PRIVATE (i,j,k)
         DO k = izs, ize
            DO j = 1, my
               DO i = 2, mx - 1
                  difu(i,j,k) = cx * (fx(i-1,j,k) - fx(i+1,j,k))
               END DO
               difu(1,j,k)  = cx * (fx(mx,j,k) - fx(2,j,k))
               difu(mx,j,k) = cx * (fx(mx-1,j,k) - fx(1,j,k))
            END DO
         END DO
!$OMP END PARALLEL

!$OMP PARALLEL PRIVATE (i,j,k)
         DO k = izs, ize
            DO j = 2, my - 1
               DO i = 1, mx
                  difu(i,j,k) = difu(i,j,k) + cy * (fy(i,j-1,k) -
     .                          fy(i,j+1,k))
               END DO
            END DO
            DO i = 1, mx
               difu(i,1,k)  = difu(i,1,k) + cy * (fy(i,my,k) -
     .                        fy(i,2,k))
               difu(i,my,k) = difu(i,my,k) + cy * (fy(i,my-1,k) -
     .                        fy(i,1,k))
            END DO
         END DO

         DO k = MAX (1, izs), MIN (1, ize)
            DO j = 1, my
               DO i = 1, mx
                  difu(i,j,1) = difu(i,j,1) + cz * (fz(i,j,mz) -
     .                          fz(i,j,2))
               END DO
            END DO
         END DO
         DO k = MAX (mz, izs), MIN (mz, ize)
            DO j = 1, my
               DO i = 1, mx
                  difu(i,j,mz) = difu(i,j,mz) + cz * (fz(i,j,mz-1) -
     .                           fz(i,j,1))
               END DO
            END DO
         END DO
!$OMP END PARALLEL

!$OMP PARALLEL PRIVATE (i,j,k)
         DO k = MAX (2, izs), MIN (mz - 1, ize)
            DO j = 1, my
               DO i = 1, mx
                  difu(i,j,k) = difu(i,j,k) + cz * (fz(i,j,k-1) -
     .                          fz(i,j,k+1))
               END DO
            END DO
         END DO
!$OMP END PARALLEL

!r3          CALL r3_info_end (r3_info_index_0)

         RETURN
         END SUBROUTINE

      END MODULE
