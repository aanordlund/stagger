! $Id: stagger_par.f90,v 1.2 2006/08/07 11:17:34 aake Exp $
!-----------------------------------------------------------------------
SUBROUTINE xup_set_par (f, fp)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f, fp
  logical omp_in_parallel
 
  if (omp_in_parallel()) then
    call xup_set (f, fp)
  else
    !$omp parallel
    call xup_set (f, fp)
    !$omp end parallel
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE xdn_set_par (f, fp)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f, fp
  logical omp_in_parallel
 
  if (omp_in_parallel()) then
    call xdn_set (f, fp)
  else
    !$omp parallel
    call xdn_set (f, fp)
    !$omp end parallel
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE xup1_set_par (f, fp)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f, fp
  logical omp_in_parallel
 
  if (omp_in_parallel()) then
    call xup1_set (f, fp)
  else
    !$omp parallel
    call xup1_set (f, fp)
    !$omp end parallel
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE xdn1_set_par (f, fp)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f, fp
  logical omp_in_parallel
 
  if (omp_in_parallel()) then
    call xdn1_set (f, fp)
  else
    !$omp parallel
    call xdn1_set (f, fp)
    !$omp end parallel
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE xup_neg_par (f, fp)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f, fp
  logical omp_in_parallel
 
  if (omp_in_parallel()) then
    call xup_neg (f, fp)
  else
    !$omp parallel
    call xup_neg (f, fp)
    !$omp end parallel
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE xdn_neg_par (f, fp)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f, fp
  logical omp_in_parallel
 
  if (omp_in_parallel()) then
    call xdn_neg (f, fp)
  else
    !$omp parallel
    call xdn_neg (f, fp)
    !$omp end parallel
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE xup1_neg_par (f, fp)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f, fp
  logical omp_in_parallel
 
  if (omp_in_parallel()) then
    call xup1_neg (f, fp)
  else
    !$omp parallel
    call xup1_neg (f, fp)
    !$omp end parallel
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE xdn1_neg_par (f, fp)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f, fp
  logical omp_in_parallel
 
  if (omp_in_parallel()) then
    call xdn1_neg (f, fp)
  else
    !$omp parallel
    call xdn1_neg (f, fp)
    !$omp end parallel
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE xup_add_par (f, fp)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f, fp
  logical omp_in_parallel
 
  if (omp_in_parallel()) then
    call xup_add (f, fp)
  else
    !$omp parallel
    call xup_add (f, fp)
    !$omp end parallel
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE xdn_add_par (f, fp)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f, fp
  logical omp_in_parallel
 
  if (omp_in_parallel()) then
    call xdn_add (f, fp)
  else
    !$omp parallel
    call xdn_add (f, fp)
    !$omp end parallel
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE xup1_add_par (f, fp)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f, fp
  logical omp_in_parallel
 
  if (omp_in_parallel()) then
    call xup1_add (f, fp)
  else
    !$omp parallel
    call xup1_add (f, fp)
    !$omp end parallel
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE xdn1_add_par (f, fp)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f, fp
  logical omp_in_parallel
 
  if (omp_in_parallel()) then
    call xdn1_add (f, fp)
  else
    !$omp parallel
    call xdn1_add (f, fp)
    !$omp end parallel
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE xup_sub_par (f, fp)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f, fp
  logical omp_in_parallel
 
  if (omp_in_parallel()) then
    call xup_sub (f, fp)
  else
    !$omp parallel
    call xup_sub (f, fp)
    !$omp end parallel
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE xdn_sub_par (f, fp)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f, fp
  logical omp_in_parallel
 
  if (omp_in_parallel()) then
    call xdn_sub (f, fp)
  else
    !$omp parallel
    call xdn_sub (f, fp)
    !$omp end parallel
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE xup1_sub_par (f, fp)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f, fp
  logical omp_in_parallel
 
  if (omp_in_parallel()) then
    call xup1_sub (f, fp)
  else
    !$omp parallel
    call xup1_sub (f, fp)
    !$omp end parallel
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE xdn1_sub_par (f, fp)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f, fp
  logical omp_in_parallel
 
  if (omp_in_parallel()) then
    call xdn1_sub (f, fp)
  else
    !$omp parallel
    call xdn1_sub (f, fp)
    !$omp end parallel
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE yup_set_par (f, fp)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f, fp
  logical omp_in_parallel
 
  if (omp_in_parallel()) then
    call yup_set (f, fp)
  else
    !$omp parallel
    call yup_set (f, fp)
    !$omp end parallel
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE ydn_set_par (f, fp)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f, fp
  logical omp_in_parallel
 
  if (omp_in_parallel()) then
    call ydn_set (f, fp)
  else
    !$omp parallel
    call ydn_set (f, fp)
    !$omp end parallel
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE yup1_set_par (f, fp)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f, fp
  logical omp_in_parallel
 
  if (omp_in_parallel()) then
    call yup1_set (f, fp)
  else
    !$omp parallel
    call yup1_set (f, fp)
    !$omp end parallel
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE ydn1_set_par (f, fp)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f, fp
  logical omp_in_parallel
 
  if (omp_in_parallel()) then
    call ydn1_set (f, fp)
  else
    !$omp parallel
    call ydn1_set (f, fp)
    !$omp end parallel
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE yup_neg_par (f, fp)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f, fp
  logical omp_in_parallel
 
  if (omp_in_parallel()) then
    call yup_neg (f, fp)
  else
    !$omp parallel
    call yup_neg (f, fp)
    !$omp end parallel
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE ydn_neg_par (f, fp)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f, fp
  logical omp_in_parallel
 
  if (omp_in_parallel()) then
    call ydn_neg (f, fp)
  else
    !$omp parallel
    call ydn_neg (f, fp)
    !$omp end parallel
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE yup1_neg_par (f, fp)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f, fp
  logical omp_in_parallel
 
  if (omp_in_parallel()) then
    call yup1_neg (f, fp)
  else
    !$omp parallel
    call yup1_neg (f, fp)
    !$omp end parallel
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE ydn1_neg_par (f, fp)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f, fp
  logical omp_in_parallel
 
  if (omp_in_parallel()) then
    call ydn1_neg (f, fp)
  else
    !$omp parallel
    call ydn1_neg (f, fp)
    !$omp end parallel
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE yup_add_par (f, fp)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f, fp
  logical omp_in_parallel
 
  if (omp_in_parallel()) then
    call yup_add (f, fp)
  else
    !$omp parallel
    call yup_add (f, fp)
    !$omp end parallel
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE ydn_add_par (f, fp)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f, fp
  logical omp_in_parallel
 
  if (omp_in_parallel()) then
    call ydn_add (f, fp)
  else
    !$omp parallel
    call ydn_add (f, fp)
    !$omp end parallel
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE yup1_add_par (f, fp)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f, fp
  logical omp_in_parallel
 
  if (omp_in_parallel()) then
    call yup1_add (f, fp)
  else
    !$omp parallel
    call yup1_add (f, fp)
    !$omp end parallel
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE ydn1_add_par (f, fp)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f, fp
  logical omp_in_parallel
 
  if (omp_in_parallel()) then
    call ydn1_add (f, fp)
  else
    !$omp parallel
    call ydn1_add (f, fp)
    !$omp end parallel
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE yup_sub_par (f, fp)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f, fp
  logical omp_in_parallel
 
  if (omp_in_parallel()) then
    call yup_sub (f, fp)
  else
    !$omp parallel
    call yup_sub (f, fp)
    !$omp end parallel
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE ydn_sub_par (f, fp)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f, fp
  logical omp_in_parallel
 
  if (omp_in_parallel()) then
    call ydn_sub (f, fp)
  else
    !$omp parallel
    call ydn_sub (f, fp)
    !$omp end parallel
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE yup1_sub_par (f, fp)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f, fp
  logical omp_in_parallel
 
  if (omp_in_parallel()) then
    call yup1_sub (f, fp)
  else
    !$omp parallel
    call yup1_sub (f, fp)
    !$omp end parallel
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE ydn1_sub_par (f, fp)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f, fp
  logical omp_in_parallel
 
  if (omp_in_parallel()) then
    call ydn1_sub (f, fp)
  else
    !$omp parallel
    call ydn1_sub (f, fp)
    !$omp end parallel
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE zup_set_par (f, fp)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f, fp
  logical omp_in_parallel
 
  if (omp_in_parallel()) then
    call zup_set (f, fp)
  else
    !$omp parallel
    call zup_set (f, fp)
    !$omp end parallel
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE zdn_set_par (f, fp)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f, fp
  logical omp_in_parallel
 
  if (omp_in_parallel()) then
    call zdn_set (f, fp)
  else
    !$omp parallel
    call zdn_set (f, fp)
    !$omp end parallel
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE zup1_set_par (f, fp)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f, fp
  logical omp_in_parallel
 
  if (omp_in_parallel()) then
    call zup1_set (f, fp)
  else
    !$omp parallel
    call zup1_set (f, fp)
    !$omp end parallel
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE zdn1_set_par (f, fp)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f, fp
  logical omp_in_parallel
 
  if (omp_in_parallel()) then
    call zdn1_set (f, fp)
  else
    !$omp parallel
    call zdn1_set (f, fp)
    !$omp end parallel
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE zup_neg_par (f, fp)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f, fp
  logical omp_in_parallel
 
  if (omp_in_parallel()) then
    call zup_neg (f, fp)
  else
    !$omp parallel
    call zup_neg (f, fp)
    !$omp end parallel
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE zdn_neg_par (f, fp)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f, fp
  logical omp_in_parallel
 
  if (omp_in_parallel()) then
    call zdn_neg (f, fp)
  else
    !$omp parallel
    call zdn_neg (f, fp)
    !$omp end parallel
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE zup1_neg_par (f, fp)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f, fp
  logical omp_in_parallel
 
  if (omp_in_parallel()) then
    call zup1_neg (f, fp)
  else
    !$omp parallel
    call zup1_neg (f, fp)
    !$omp end parallel
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE zdn1_neg_par (f, fp)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f, fp
  logical omp_in_parallel
 
  if (omp_in_parallel()) then
    call zdn1_neg (f, fp)
  else
    !$omp parallel
    call zdn1_neg (f, fp)
    !$omp end parallel
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE zup_add_par (f, fp)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f, fp
  logical omp_in_parallel
 
  if (omp_in_parallel()) then
    call zup_add (f, fp)
  else
    !$omp parallel
    call zup_add (f, fp)
    !$omp end parallel
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE zdn_add_par (f, fp)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f, fp
  logical omp_in_parallel
 
  if (omp_in_parallel()) then
    call zdn_add (f, fp)
  else
    !$omp parallel
    call zdn_add (f, fp)
    !$omp end parallel
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE zup1_add_par (f, fp)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f, fp
  logical omp_in_parallel
 
  if (omp_in_parallel()) then
    call zup1_add (f, fp)
  else
    !$omp parallel
    call zup1_add (f, fp)
    !$omp end parallel
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE zdn1_add_par (f, fp)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f, fp
  logical omp_in_parallel
 
  if (omp_in_parallel()) then
    call zdn1_add (f, fp)
  else
    !$omp parallel
    call zdn1_add (f, fp)
    !$omp end parallel
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE zup_sub_par (f, fp)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f, fp
  logical omp_in_parallel
 
  if (omp_in_parallel()) then
    call zup_sub (f, fp)
  else
    !$omp parallel
    call zup_sub (f, fp)
    !$omp end parallel
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE zdn_sub_par (f, fp)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f, fp
  logical omp_in_parallel
 
  if (omp_in_parallel()) then
    call zdn_sub (f, fp)
  else
    !$omp parallel
    call zdn_sub (f, fp)
    !$omp end parallel
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE zup1_sub_par (f, fp)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f, fp
  logical omp_in_parallel
 
  if (omp_in_parallel()) then
    call zup1_sub (f, fp)
  else
    !$omp parallel
    call zup1_sub (f, fp)
    !$omp end parallel
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE zdn1_sub_par (f, fp)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f, fp
  logical omp_in_parallel
 
  if (omp_in_parallel()) then
    call zdn1_sub (f, fp)
  else
    !$omp parallel
    call zdn1_sub (f, fp)
    !$omp end parallel
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE ddxup_set_par (f, fp)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f, fp
  logical omp_in_parallel
 
  if (omp_in_parallel()) then
    call ddxup_set (f, fp)
  else
    !$omp parallel
    call ddxup_set (f, fp)
    !$omp end parallel
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE ddxdn_set_par (f, fp)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f, fp
  logical omp_in_parallel
 
  if (omp_in_parallel()) then
    call ddxdn_set (f, fp)
  else
    !$omp parallel
    call ddxdn_set (f, fp)
    !$omp end parallel
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE ddxup1_set_par (f, fp)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f, fp
  logical omp_in_parallel
 
  if (omp_in_parallel()) then
    call ddxup1_set (f, fp)
  else
    !$omp parallel
    call ddxup1_set (f, fp)
    !$omp end parallel
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE ddxdn1_set_par (f, fp)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f, fp
  logical omp_in_parallel
 
  if (omp_in_parallel()) then
    call ddxdn1_set (f, fp)
  else
    !$omp parallel
    call ddxdn1_set (f, fp)
    !$omp end parallel
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE ddxup_neg_par (f, fp)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f, fp
  logical omp_in_parallel
 
  if (omp_in_parallel()) then
    call ddxup_neg (f, fp)
  else
    !$omp parallel
    call ddxup_neg (f, fp)
    !$omp end parallel
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE ddxdn_neg_par (f, fp)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f, fp
  logical omp_in_parallel
 
  if (omp_in_parallel()) then
    call ddxdn_neg (f, fp)
  else
    !$omp parallel
    call ddxdn_neg (f, fp)
    !$omp end parallel
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE ddxup1_neg_par (f, fp)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f, fp
  logical omp_in_parallel
 
  if (omp_in_parallel()) then
    call ddxup1_neg (f, fp)
  else
    !$omp parallel
    call ddxup1_neg (f, fp)
    !$omp end parallel
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE ddxdn1_neg_par (f, fp)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f, fp
  logical omp_in_parallel
 
  if (omp_in_parallel()) then
    call ddxdn1_neg (f, fp)
  else
    !$omp parallel
    call ddxdn1_neg (f, fp)
    !$omp end parallel
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE ddxup_add_par (f, fp)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f, fp
  logical omp_in_parallel
 
  if (omp_in_parallel()) then
    call ddxup_add (f, fp)
  else
    !$omp parallel
    call ddxup_add (f, fp)
    !$omp end parallel
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE ddxdn_add_par (f, fp)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f, fp
  logical omp_in_parallel
 
  if (omp_in_parallel()) then
    call ddxdn_add (f, fp)
  else
    !$omp parallel
    call ddxdn_add (f, fp)
    !$omp end parallel
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE ddxup1_add_par (f, fp)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f, fp
  logical omp_in_parallel
 
  if (omp_in_parallel()) then
    call ddxup1_add (f, fp)
  else
    !$omp parallel
    call ddxup1_add (f, fp)
    !$omp end parallel
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE ddxdn1_add_par (f, fp)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f, fp
  logical omp_in_parallel
 
  if (omp_in_parallel()) then
    call ddxdn1_add (f, fp)
  else
    !$omp parallel
    call ddxdn1_add (f, fp)
    !$omp end parallel
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE ddxup_sub_par (f, fp)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f, fp
  logical omp_in_parallel
 
  if (omp_in_parallel()) then
    call ddxup_sub (f, fp)
  else
    !$omp parallel
    call ddxup_sub (f, fp)
    !$omp end parallel
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE ddxdn_sub_par (f, fp)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f, fp
  logical omp_in_parallel
 
  if (omp_in_parallel()) then
    call ddxdn_sub (f, fp)
  else
    !$omp parallel
    call ddxdn_sub (f, fp)
    !$omp end parallel
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE ddxup1_sub_par (f, fp)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f, fp
  logical omp_in_parallel
 
  if (omp_in_parallel()) then
    call ddxup1_sub (f, fp)
  else
    !$omp parallel
    call ddxup1_sub (f, fp)
    !$omp end parallel
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE ddxdn1_sub_par (f, fp)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f, fp
  logical omp_in_parallel
 
  if (omp_in_parallel()) then
    call ddxdn1_sub (f, fp)
  else
    !$omp parallel
    call ddxdn1_sub (f, fp)
    !$omp end parallel
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE ddyup_set_par (f, fp)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f, fp
  logical omp_in_parallel
 
  if (omp_in_parallel()) then
    call ddyup_set (f, fp)
  else
    !$omp parallel
    call ddyup_set (f, fp)
    !$omp end parallel
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE ddydn_set_par (f, fp)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f, fp
  logical omp_in_parallel
 
  if (omp_in_parallel()) then
    call ddydn_set (f, fp)
  else
    !$omp parallel
    call ddydn_set (f, fp)
    !$omp end parallel
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE ddyup1_set_par (f, fp)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f, fp
  logical omp_in_parallel
 
  if (omp_in_parallel()) then
    call ddyup1_set (f, fp)
  else
    !$omp parallel
    call ddyup1_set (f, fp)
    !$omp end parallel
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE ddydn1_set_par (f, fp)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f, fp
  logical omp_in_parallel
 
  if (omp_in_parallel()) then
    call ddydn1_set (f, fp)
  else
    !$omp parallel
    call ddydn1_set (f, fp)
    !$omp end parallel
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE ddyup_neg_par (f, fp)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f, fp
  logical omp_in_parallel
 
  if (omp_in_parallel()) then
    call ddyup_neg (f, fp)
  else
    !$omp parallel
    call ddyup_neg (f, fp)
    !$omp end parallel
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE ddydn_neg_par (f, fp)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f, fp
  logical omp_in_parallel
 
  if (omp_in_parallel()) then
    call ddydn_neg (f, fp)
  else
    !$omp parallel
    call ddydn_neg (f, fp)
    !$omp end parallel
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE ddyup1_neg_par (f, fp)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f, fp
  logical omp_in_parallel
 
  if (omp_in_parallel()) then
    call ddyup1_neg (f, fp)
  else
    !$omp parallel
    call ddyup1_neg (f, fp)
    !$omp end parallel
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE ddydn1_neg_par (f, fp)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f, fp
  logical omp_in_parallel
 
  if (omp_in_parallel()) then
    call ddydn1_neg (f, fp)
  else
    !$omp parallel
    call ddydn1_neg (f, fp)
    !$omp end parallel
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE ddyup_add_par (f, fp)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f, fp
  logical omp_in_parallel
 
  if (omp_in_parallel()) then
    call ddyup_add (f, fp)
  else
    !$omp parallel
    call ddyup_add (f, fp)
    !$omp end parallel
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE ddydn_add_par (f, fp)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f, fp
  logical omp_in_parallel
 
  if (omp_in_parallel()) then
    call ddydn_add (f, fp)
  else
    !$omp parallel
    call ddydn_add (f, fp)
    !$omp end parallel
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE ddyup1_add_par (f, fp)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f, fp
  logical omp_in_parallel
 
  if (omp_in_parallel()) then
    call ddyup1_add (f, fp)
  else
    !$omp parallel
    call ddyup1_add (f, fp)
    !$omp end parallel
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE ddydn1_add_par (f, fp)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f, fp
  logical omp_in_parallel
 
  if (omp_in_parallel()) then
    call ddydn1_add (f, fp)
  else
    !$omp parallel
    call ddydn1_add (f, fp)
    !$omp end parallel
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE ddyup_sub_par (f, fp)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f, fp
  logical omp_in_parallel
 
  if (omp_in_parallel()) then
    call ddyup_sub (f, fp)
  else
    !$omp parallel
    call ddyup_sub (f, fp)
    !$omp end parallel
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE ddydn_sub_par (f, fp)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f, fp
  logical omp_in_parallel
 
  if (omp_in_parallel()) then
    call ddydn_sub (f, fp)
  else
    !$omp parallel
    call ddydn_sub (f, fp)
    !$omp end parallel
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE ddyup1_sub_par (f, fp)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f, fp
  logical omp_in_parallel
 
  if (omp_in_parallel()) then
    call ddyup1_sub (f, fp)
  else
    !$omp parallel
    call ddyup1_sub (f, fp)
    !$omp end parallel
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE ddydn1_sub_par (f, fp)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f, fp
  logical omp_in_parallel
 
  if (omp_in_parallel()) then
    call ddydn1_sub (f, fp)
  else
    !$omp parallel
    call ddydn1_sub (f, fp)
    !$omp end parallel
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE ddzup_set_par (f, fp)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f, fp
  logical omp_in_parallel
 
  if (omp_in_parallel()) then
    call ddzup_set (f, fp)
  else
    !$omp parallel
    call ddzup_set (f, fp)
    !$omp end parallel
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE ddzdn_set_par (f, fp)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f, fp
  logical omp_in_parallel
 
  if (omp_in_parallel()) then
    call ddzdn_set (f, fp)
  else
    !$omp parallel
    call ddzdn_set (f, fp)
    !$omp end parallel
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE ddzup1_set_par (f, fp)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f, fp
  logical omp_in_parallel
 
  if (omp_in_parallel()) then
    call ddzup1_set (f, fp)
  else
    !$omp parallel
    call ddzup1_set (f, fp)
    !$omp end parallel
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE ddzdn1_set_par (f, fp)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f, fp
  logical omp_in_parallel
 
  if (omp_in_parallel()) then
    call ddzdn1_set (f, fp)
  else
    !$omp parallel
    call ddzdn1_set (f, fp)
    !$omp end parallel
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE ddzup_neg_par (f, fp)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f, fp
  logical omp_in_parallel
 
  if (omp_in_parallel()) then
    call ddzup_neg (f, fp)
  else
    !$omp parallel
    call ddzup_neg (f, fp)
    !$omp end parallel
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE ddzdn_neg_par (f, fp)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f, fp
  logical omp_in_parallel
 
  if (omp_in_parallel()) then
    call ddzdn_neg (f, fp)
  else
    !$omp parallel
    call ddzdn_neg (f, fp)
    !$omp end parallel
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE ddzup1_neg_par (f, fp)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f, fp
  logical omp_in_parallel
 
  if (omp_in_parallel()) then
    call ddzup1_neg (f, fp)
  else
    !$omp parallel
    call ddzup1_neg (f, fp)
    !$omp end parallel
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE ddzdn1_neg_par (f, fp)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f, fp
  logical omp_in_parallel
 
  if (omp_in_parallel()) then
    call ddzdn1_neg (f, fp)
  else
    !$omp parallel
    call ddzdn1_neg (f, fp)
    !$omp end parallel
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE ddzup_add_par (f, fp)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f, fp
  logical omp_in_parallel
 
  if (omp_in_parallel()) then
    call ddzup_add (f, fp)
  else
    !$omp parallel
    call ddzup_add (f, fp)
    !$omp end parallel
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE ddzdn_add_par (f, fp)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f, fp
  logical omp_in_parallel
 
  if (omp_in_parallel()) then
    call ddzdn_add (f, fp)
  else
    !$omp parallel
    call ddzdn_add (f, fp)
    !$omp end parallel
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE ddzup1_add_par (f, fp)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f, fp
  logical omp_in_parallel
 
  if (omp_in_parallel()) then
    call ddzup1_add (f, fp)
  else
    !$omp parallel
    call ddzup1_add (f, fp)
    !$omp end parallel
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE ddzdn1_add_par (f, fp)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f, fp
  logical omp_in_parallel
 
  if (omp_in_parallel()) then
    call ddzdn1_add (f, fp)
  else
    !$omp parallel
    call ddzdn1_add (f, fp)
    !$omp end parallel
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE ddzup_sub_par (f, fp)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f, fp
  logical omp_in_parallel
 
  if (omp_in_parallel()) then
    call ddzup_sub (f, fp)
  else
    !$omp parallel
    call ddzup_sub (f, fp)
    !$omp end parallel
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE ddzdn_sub_par (f, fp)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f, fp
  logical omp_in_parallel
 
  if (omp_in_parallel()) then
    call ddzdn_sub (f, fp)
  else
    !$omp parallel
    call ddzdn_sub (f, fp)
    !$omp end parallel
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE ddzup1_sub_par (f, fp)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f, fp
  logical omp_in_parallel
 
  if (omp_in_parallel()) then
    call ddzup1_sub (f, fp)
  else
    !$omp parallel
    call ddzup1_sub (f, fp)
    !$omp end parallel
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE ddzdn1_sub_par (f, fp)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f, fp
  logical omp_in_parallel
 
  if (omp_in_parallel()) then
    call ddzdn1_sub (f, fp)
  else
    !$omp parallel
    call ddzdn1_sub (f, fp)
    !$omp end parallel
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE dif1_set_par (Ux, Uy, Uz, scr1)
  USE params
  implicit none
  real, dimension(mx,my,mz):: Ux, Uy, Uz, scr1
  logical omp_in_parallel
! 
  if (omp_in_parallel()) then
    call dif1_set (Ux, Uy, Uz, scr1)
  else
    !$omp parallel
    call dif1_set (Ux, Uy, Uz, scr1)
    !$omp end parallel
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE max3_set_par  (scr1, scr2)
  USE params
  implicit none
  real, dimension(mx,my,mz):: scr1, scr2
  logical omp_in_parallel
! 
  if (omp_in_parallel()) then
    call max3_set (scr1, scr2)
  else
    !$omp parallel
    call max3_set (scr1, scr2)
    !$omp end parallel
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE smooth3_set_par (scr1, scr2)
  USE params
  implicit none
  real, dimension(mx,my,mz):: scr1, scr2
  logical omp_in_parallel
! 
  if (omp_in_parallel()) then
    call smooth3_set (scr1, scr2)
  else
    !$omp parallel
    call smooth3_set (scr1, scr2)
    !$omp end parallel
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE dif2d_set_par (scr1,scr2,scr3,scr4)
  USE params
  implicit none
  real, dimension(mx,my,mz):: scr1,scr2,scr3,scr4
  logical omp_in_parallel
! 
  if (omp_in_parallel()) then
    call dif2d_set (scr1,scr2,scr3,scr4)
  else
    !$omp parallel
    call dif2d_set (scr1,scr2,scr3,scr4)
    !$omp end parallel
  end if
END
