!***********************************************************************
FUNCTION xyup (f)
!
!  f is centered on (i-.5,j,k), fifth order stagger
!
  use params
  implicit none
  real a, b, c, a1st
  integer i, j, k, jm2, jm1, jp1, jp2, jp3
  real, dimension(mx,my,mz):: f, xyup
!hpf$ distribute(*,*,block):: f, xyup
!-----------------------------------------------------------------------
!
  character(len=mid):: id = "$Id: xy_do.f90,v 1.3 2012/12/31 16:10:46 aake Exp $" 
  if (id.ne.' '.and.izs.eq.1) print '(1x,a)',id; id=' '
!
  if (mx.le.5) then
    xyup = f
    return
  end if
  c = 3./256.
  b = -25./256.
  a = .5-b-c
  a1st = 0.5
!
!2omp barrier
!$omp parallel private(i,j,k,jm2,jm1,jp1,jp2,jp3)
  do k=izs,ize
   do j=1,my
    jm2 = mod(j-3+my,my)+1
    jm1 = mod(j-2+my,my)+1
    jp1 = mod(j     ,my)+1
    jp2 = mod(j+1   ,my)+1
    jp3 = mod(j+2   ,my)+1
    xyup(mx-2,j,k) = ( &
                   a*(f(mx-2,j  ,k)+f(mx-1,jp1,k)) &
                 + b*(f(mx-3,jm1,k)+f(mx  ,jp2,k)) &
                 + c*(f(mx-4,jm2,k)+f(1   ,jp3,k)))
    xyup(mx-1,j,k) = ( &
                   a*(f(mx-1,j  ,k)+f(mx  ,jp1,k)) &
                 + b*(f(mx-2,jm1,k)+f(1   ,jp2,k)) &
                 + c*(f(mx-3,jm2,k)+f(2   ,jp3,k)))
    xyup(mx  ,j,k) = ( &
                   a*(F(mx  ,j  ,k)+f(1   ,jp1,k)) &
                 + b*(f(mx-1,jm1,k)+f(2   ,jp2,k)) &
                 + c*(f(mx-2,jm2,k)+f(3   ,jp3,k)))
    xyup(1   ,j,k) = ( &
                   a*(f(1   ,j  ,k)+f(2   ,jp1,k)) &
                 + b*(f(mx  ,jm1,k)+f(3   ,jp2,k)) &
                 + c*(f(mx-1,jm2,k)+f(4   ,jp3,k)))
    xyup(2   ,j,k) = ( &
                   a*(f(2   ,j  ,k)+f(3   ,jp1,k)) &
                 + b*(f(1   ,jm1,k)+f(4   ,jp2,k)) &
                 + c*(f(mx  ,jm2,k)+f(5   ,jp3,k)))
    do i=3,mx-3
      xyup(i  ,j,k) = ( &
                    a*(f(i  ,j  ,k)+f(i+1 ,jp1,k)) &
                  + b*(f(i-1,jm1,k)+f(i+2 ,jp2,k)) &
                  + c*(f(i-2,jm2,k)+f(i+3 ,jp3,k)))
    end do
   end do
  end do
!$omp end parallel
!2omp barrier
!
  nstag = nstag+1
  nflop = nflop+8
END FUNCTION

!***********************************************************************
FUNCTION xyup1 (f)
!
!  f is centered on (i-.5,j,k), fifth order stagger
!
  use params
  implicit none
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f, xyup1
!hpf$ distribute(*,*,block):: f, xyup1
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    xyup1 = f
    return
  end if
!
  a = .5
!
!2omp barrier
!$omp parallel private(i,j,k)
  do k=izs,ize
   do j=1,my
    xyup1(mx  ,j,k) = ( &
                   a*(f(mx  ,j,k)+f(1   ,j,k)))
   end do
   do j=1,my
    do i=1,mx-1
      xyup1(i  ,j,k) = ( &
                    a*(f(i  ,j,k)+f(i+1 ,j,k)))
    end do
   end do
  end do
!$omp end parallel
!2omp barrier
!
  nstag1 = nstag1+1
  nflop = nflop+2
END FUNCTION

!***********************************************************************
FUNCTION xydn (f)
!
!  f is centered on (i,j,k), fifth order stagger
!
  use params
  implicit none
  real a, b, c, a1st
  integer i, j, k, jm3, jm2, jm1, jp1, jp2
  real, dimension(mx,my,mz):: f, xydn
!hpf$ distribute(*,*,block):: f, xydn
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    xydn = f
    return
  end if
!
  c = 3./256.
  b = -25./256.
  a = .5-b-c
  a1st = 0.5
!
!2omp barrier
!$omp parallel private(i,j,k,jm3,jm2,jm1,jp1,jp2)
  do k=izs,ize
   do j=1,my
    jm3 = mod(j-4+my,my)+1
    jm2 = mod(j-3+my,my)+1
    jm1 = mod(j-2+my,my)+1
    jp1 = mod(j     ,my)+1
    jp2 = mod(j+1   ,my)+1
    xydn(mx-1,j,k) = ( &
                   a*(f(mx-2,j  ,k)+f(mx-1,jm1,k)) &
                 + b*(f(mx-3,jp1,k)+f(mx  ,jm2,k)) &
                 + c*(f(mx-4,jp2,k)+f(1   ,jm3,k)))
    xydn(mx  ,j,k) = ( &
                   a*(f(mx-1,j  ,k)+f(mx  ,jm1,k)) &
                 + b*(f(mx-2,jp1,k)+f(1   ,jm2,k)) &
                 + c*(f(mx-3,jp2,k)+f(2   ,jm3,k)))
    xydn(1  ,j,k) = ( &
                   a*(f(mx  ,j  ,k)+f(1   ,jm1,k)) &
                 + b*(f(mx-1,jp1,k)+f(2   ,jm2,k)) &
                 + c*(f(mx-2,jp2,k)+f(3   ,jm3,k)))
    xydn(2  ,j,k) = ( &
                   a*(f(1   ,j  ,k)+f(2   ,jm1,k)) &
                 + b*(f(mx  ,jp1,k)+f(3   ,jm2,k)) &
                 + c*(f(mx-1,jp2,k)+f(4   ,jm3,k)))
    xydn(3  ,j,k) = ( &
                   a*(f(2   ,j  ,k)+f(3   ,jm1,k)) &
                 + b*(f(1   ,jp1,k)+f(4   ,jm2,k)) &
                 + c*(f(mx  ,jp2,k)+f(5   ,jm3,k)))
    do i=4,mx-2
      xydn(i,j,k) = ( &
                   a*(f(i   ,j  ,k)+f(i-1 ,jm1,k)) &
                 + b*(f(i+1 ,jp1,k)+f(i-2 ,jm2,k)) &
                 + c*(f(i+2 ,jp2,k)+f(i-3 ,jm3,k)))
    end do
   end do
  end do
!$omp end parallel
!2omp barrier
!
!
  nstag = nstag+1
  nflop = nflop+8
END FUNCTION

!***********************************************************************
FUNCTION xydn1 (f)
!
!  f is centered on (i,j,k), fifth order stagger
!
  use params
  implicit none
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f, xydn1
!hpf$ distribute(*,*,block):: f, xydn1
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    xydn1 = f
    return
  end if
!
  a = 0.5
!
!2omp barrier
!$omp parallel private(i,j,k)
  do k=izs,ize
   do j=1,my
    xydn1(1  ,j,k) = ( &
                   a*(f(mx  ,j,k)+f(1   ,j,k)))
   end do
   do j=1,my
    do i=2,mx
      xydn1(i,j,k) = ( &
                   a*(f(i   ,j,k)+f(i-1 ,j,k)))
    end do
   end do
  end do
!$omp end parallel
!2omp barrier
!
  nstag1 = nstag1+1
  nflop = nflop+2
END FUNCTION
