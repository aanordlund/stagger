! $Id: smooth_max_do_mpi.f90,v 1.33 2013/11/03 10:10:02 aake Exp $
!***********************************************************************
SUBROUTINE crl1_set (Ux, Uy, Uz, out)
!
!  Add a lower order, centered approximation of abs(w) to the out array
!
  USE params
  implicit none
  integer iz
  real, dimension(mx,my,mz):: Ux, Uy, Uz, out
!
  !$omp master
  allocate (tmp1(mx,my,mz), tmp2(mx,my,mz))
  !$omp end master
  !$omp barrier
  call dif1xy_set(Ux,Uy,tmp1); call xup1_set(tmp1,tmp2); call yup1_set(tmp2,tmp1)
  call dif1yz_set(Uy,Uz,tmp1); call yup1_set(tmp1,tmp2); call zup1_add(tmp2,tmp1)
  call dif1zx_set(Uz,Ux,tmp1); call zup1_set(tmp1,tmp2); call xup1_add(tmp2,tmp1)
  do iz=izs,ize
    out(:,:,iz) = sqrt(tmp1(:,:,iz))
  end do
  !$omp barrier
  !$omp master
  deallocate (tmp1, tmp2)
  !$omp end master
END
!***********************************************************************
SUBROUTINE crl1_add (Ux, Uy, Uz, out)
!
!  Add a lower order, centered approximation of abs(w) to the out array
!
  USE params
  implicit none
  integer iz
  real, dimension(mx,my,mz):: Ux, Uy, Uz, out
!
  !$omp master
  allocate (tmp1(mx,my,mz), tmp2(mx,my,mz))
  !$omp end master
  !$omp barrier
  call dif1xy_set(Ux,Uy,tmp1); call xup1_set(tmp1,tmp2); call yup1_set(tmp2,tmp1)
  call dif1yz_set(Uy,Uz,tmp1); call yup1_set(tmp1,tmp2); call zup1_add(tmp2,tmp1)
  call dif1zx_set(Uz,Ux,tmp1); call zup1_set(tmp1,tmp2); call xup1_add(tmp2,tmp1)
  do iz=izs,ize
    out(:,:,iz) = out(:,:,iz) + sqrt(tmp1(:,:,iz))
  end do
  !$omp barrier
  !$omp master
  deallocate (tmp1, tmp2)
  !$omp end master
END

!***********************************************************************
SUBROUTINE smooth (f, dir)
!
!  Three point smooth
!
  USE params, gx=>gx1, hx=>hx1, gz=>gz1, hz=>hz1
  implicit none
  logical debug
  integer i, j, k, dir
  real, dimension(mx,my,mz):: f
  real, dimension(0:mx+1):: fx
  real, dimension(0:my+1):: fy
  real, dimension(mx,0:mz+1):: fz
  real c
  logical omp_in_parallel
!-----------------------------------------------------------------------
  character(len=mid):: id = "smooth3: $Id: smooth_max_do_mpi.f90,v 1.33 2013/11/03 10:10:02 aake Exp $" 
  call print_id (id)
!
  c=0.25

  if (dir==1 .and. mx>2) then
    call mpi_send_x (f, gx, 1, hx, 1)
    do k=izs,ize
     do j=1,my
      do i=1,mx
        fx(i)=f(i,j,k)
      end do
      fx(0)=gx(1,j,k)
      fx(mx+1)=hx(1,j,k)
      do i=1,mx
        f(i,j,k)=(1.-2*c)*fx(i)+c*(fx(i-1)+fx(i+1))
      end do
     end do
    end do

  else if (dir==2 .and. my>2) then
    call mpi_send_y (f, gy1, 1, hy1, 1)
    do k=izs,ize
     do i=1,mx
      do j=1,my
        fy(j)=f(i,j,k)
      end do
      fy(0)=gy1(i,1,k)
      fy(my+1)=hy1(i,1,k)
      do j=merge(1,lb+1,lb.le.1),merge(my,ub-1,ub.ge.my)
        f(i,j,k)=(1.-2*c)*fy(j)+c*(fy(j-1)+fy(j+1))
      end do
     end do
    end do

  else if (mz > 1) then
    call mpi_send_z (f, gz, 1, hz, 1)
    do j=iys,iye
     do k=1,mz
     do i=1,mx
       fz(i,k)=f(i,j,k)
     end do
     end do
     do i=1,mx
       fz(i,   0)=gz(i,j,1)
       fz(i,mz+1)=hz(i,j,1)
     end do
     do k=1,mz
      do i=1,mx
        f(i,j,k)=(1.-2*c)*fz(i,k)+c*(fz(i,k-1)+fz(i,k+1))
      end do
     end do
    end do
  end if
  call barrier_omp('smooth end')
END SUBROUTINE

!***********************************************************************
FUNCTION smooth3 (f)
!
!  Three point smooth
!
  USE params, gx=>gx1, hx=>hx1, gz=>gz1, hz=>hz1
  implicit none
  logical debug
  integer i,j,k
  real, dimension(mx,my,mz):: f, smooth3
  real, dimension(0:mx+1):: fx
  real, dimension(0:my+1):: fy
  real c3
  logical omp_in_parallel
!-----------------------------------------------------------------------
  character(len=mid):: id = "smooth3: $Id: smooth_max_do_mpi.f90,v 1.33 2013/11/03 10:10:02 aake Exp $" 
  call print_id (id)
!
  c3=1./3.
  if (omp_in_parallel()) print *,'WARNING: smooth3 called from within parallel region!'

  call mpi_send_x (f, gx, 1, hx, 1)

  if (mx < 3) then
    scratch=f
  else
    do k=izs,ize
     do j=1,my
      do i=1,mx
        fx(i)=f(i,j,k)
      end do
      fx(0)=gx(1,j,k)
      fx(mx+1)=hx(1,j,k)
      do i=1,mx
        scratch(i,j,k)=c3*(fx(i-1)+fx(i)+fx(i+1))
      end do
     end do
    end do
  end if

  if (my > 2) then
 
    call mpi_send_y (scratch, gy1, 1, hy1, 1)
 
    do k=izs,ize
     do i=1,mx
      do j=1,my
        fy(j)=scratch(i,j,k)
      end do
      fy(0)=gy1(i,1,k)
      fy(my+1)=hy1(i,1,k)
      do j=1,my
        scratch(i,j,k)=c3*(fy(j-1)+fy(j)+fy(j+1))
      end do
     end do
    end do
    call barrier_omp('smooth3')
  endif

  if (mz < 3) then
    smooth3 = scratch
    return
  end if

  call mpi_send_z (scratch, gz, 1, hz, 1)
  do k=max(2,izs),min(mz-1,ize)
   do j=1,my
    do i=1,mx
      smooth3(i,j,k)=c3*(scratch(i,j,k-1)+scratch(i,j,k)+scratch(i,j,k+1))
    end do
   end do
  end do
  if (izs.eq. 1) then
   do j=1,my
    do i=1,mx
      smooth3(i,j,1)=c3*(gz(i,j,1)+scratch(i,j,1)+scratch(i,j,2))
    end do
   end do
  end if
  if (ize.eq.mz) then
   do j=1,my
    do i=1,mx
      smooth3(i,j,mz)=c3*(scratch(i,j,mz-1)+scratch(i,j,mz)+hz(i,j,1))
    end do
   end do
  end if
  call barrier_omp('smooth3')

END FUNCTION

!***********************************************************************
FUNCTION smooth3z (f)
!
!  Three point smooth
!
  USE params, gx=>gx1, hx=>hx1, gz=>gz1, hz=>hz1
  implicit none
  logical debug
  integer i,j,k
  real, dimension(mx,my,mz):: f, smooth3z
  real, dimension(0:mx+1):: fx
  real, dimension(0:my+1):: fy
  real c3
  logical omp_in_parallel
!-----------------------------------------------------------------------
  character(len=mid):: id = "smooth3: $Id: smooth_max_do_mpi.f90,v 1.33 2013/11/03 10:10:02 aake Exp $" 
  call print_id (id)
!
  c3=1./3.
  if (omp_in_parallel()) print *,'WARNING: smooth3 called from within parallel region!'

  if (mz < 3) then
    scratch = f
  else

    call mpi_send_z (f, gz, 1, hz, 1)

    do k=max(2,izs),min(mz-1,ize)
     do j=1,my
      do i=1,mx
        scratch(i,j,k)=c3*(f(i,j,k-1)+f(i,j,k)+f(i,j,k+1))
      end do
     end do
    end do
    if (izs.eq. 1) then
     do j=1,my
      do i=1,mx
        scratch(i,j,1)=c3*(gz(i,j,1)+f(i,j,1)+f(i,j,2))
      end do
     end do
    end if
    if (ize.eq.mz) then
     do j=1,my
      do i=1,mx
        scratch(i,j,mz)=c3*(f(i,j,mz-1)+f(i,j,mz)+hz(i,j,1))
      end do
     end do
    end if
    call barrier_omp('smooth3')
  end if

  if (mx > 2) then

    call mpi_send_x (scratch, gx, 1, hx, 1)

    do k=izs,ize
     do j=1,my
      do i=1,mx
        fx(i)=scratch(i,j,k)
      end do
      fx(0)=gx(1,j,k)
      fx(mx+1)=hx(1,j,k)
      do i=1,mx
        scratch(i,j,k)=c3*(fx(i-1)+fx(i)+fx(i+1))
        end do
     end do
    end do
  end if

  if (my < 3) then
    smooth3z = scratch
  else
    call mpi_send_y (scratch, gy1, 1, hy1, 1)
    do k=izs,ize
     do i=1,mx
      do j=1,my
        fy(j)=scratch(i,j,k)
      end do
      fy(0)=gy1(i,1,k)
      fy(my+1)=hy1(i,1,k)
      do j=1,my
        smooth3z(i,j,k)=c3*(fy(j-1)+fy(j)+fy(j+1))
      end do
     end do
    end do
    call barrier_omp('smooth3')
  endif

END FUNCTION

!***********************************************************************
FUNCTION smooth3t (f)
!
!  Three point smooth
!
  USE params, gx=>gx1, hx=>hx1, gz=>gz1, hz=>hz1
  implicit none
  integer i,j,k
  real, dimension(mx,my,mz):: f, smooth3t
  real, dimension(0:mx+1):: fx
  real, dimension(0:my+1):: fy
  real c3
  logical omp_in_parallel
!-----------------------------------------------------------------------
  character(len=mid):: id = "smooth3t: $Id: smooth_max_do_mpi.f90,v 1.33 2013/11/03 10:10:02 aake Exp $" 
  call print_id (id)
!
  c3=1./3.
  if (omp_in_parallel()) print *,'WARNING: smooth3t called from within parallel region!'

  if (mx < 3) then
    scratch=f
  else

    call mpi_send_x (f, gx, 1, hx, 1)

    do k=izs,ize
     do j=1,my
      do i=1,mx
        fx(i)=f(i,j,k)
      end do
      fx(0)=gx(1,j,k)
      fx(mx+1)=hx(1,j,k)
      do i=1,mx
        scratch(i,j,k)=0.25*(fx(i-1)+fx(i)+fx(i)+fx(i+1))
      end do
     end do
    end do
  end if

  if (my > 2) then

    call mpi_send_y (scratch, gy1, 1, hy1, 1)

    do k=izs,ize
     do i=1,mx
      do j=1,my
        fy(j)=scratch(i,j,k)
      end do
      fy(0)=gy1(i,1,k)
      fy(my+1)=hy1(i,1,k)
      do j=1,my
        scratch(i,j,k)=0.25*(fy(j-1)+fy(j)+fy(j)+fy(j+1))
      end do
     end do
    end do
    call barrier_omp('smooth3t')

  end if

  if (mz < 3) then
    smooth3t = scratch
    return
  end if

  call mpi_send_z (scratch(:,:,1:mz), gz, 1, hz, 1)

  do k=max(2,izs),min(mz-1,ize)
   do j=1,my
    do i=1,mx
      smooth3t(i,j,k)=0.25*(scratch(i,j,k-1)+scratch(i,j,k)+scratch(i,j,k)+scratch(i,j,k+1))
    end do
   end do
  end do
  if (izs.eq. 1) then
   do j=1,my
    do i=1,mx
      smooth3t(i,j,1)=0.25*(gz(i,j,1)+scratch(i,j,1)+scratch(i,j,1)+scratch(i,j,2))
    end do
   end do
  end if
  if (ize.eq.mz) then
   do j=1,my
    do i=1,mx
      smooth3t(i,j,mz)=0.25*(scratch(i,j,mz-1)+scratch(i,j,mz)+scratch(i,j,mz)+hz(i,j,1))
    end do
   end do
  end if
  call barrier_omp('smooth3t')

END FUNCTION

!***********************************************************************
FUNCTION smooth3th (f)
!
!  Three point smooth horizontally
!
  USE params, gx=>gx1, hx=>hx1, gz=>gz1, hz=>hz1
  implicit none
  integer i,j,k
  real, dimension(mx,my,mz):: f, smooth3th
  real, dimension(0:mx+1):: fx
  real, dimension(0:my+1):: fy
  real c3
  logical omp_in_parallel
!-----------------------------------------------------------------------
  character(len=mid):: id = "smooth3t: $Id: smooth_max_do_mpi.f90,v 1.33 2013/11/03 10:10:02 aake Exp $" 
  call print_id (id)
!
  c3=1./3.
  if (omp_in_parallel()) print *,'WARNING: smooth3t called from within parallel region!'

  if (mx < 3) then
    scratch=f
  else

    call mpi_send_x (f, gx, 1, hx, 1)

    do k=izs,ize
     do j=1,my
      do i=1,mx
        fx(i)=f(i,j,k)
      end do
      fx(0)=gx(1,j,k)
      fx(mx+1)=hx(1,j,k)
      do i=1,mx
        scratch(i,j,k)=0.25*(fx(i-1)+fx(i)+fx(i)+fx(i+1))
      end do
     end do
    end do
  end if

  call barrier_omp('smooth3th')

  if (mz < 3) then
    smooth3th = scratch
    return
  end if

  call mpi_send_z (scratch(:,:,1:mz), gz, 1, hz, 1)

  do k=max(2,izs),min(mz-1,ize)
   do j=1,my
    do i=1,mx
      smooth3th(i,j,k)=0.25*(scratch(i,j,k-1)+scratch(i,j,k)+scratch(i,j,k)+scratch(i,j,k+1))
    end do
   end do
  end do
  if (izs.eq. 1) then
   do j=1,my
    do i=1,mx
      smooth3th(i,j,1)=0.25*(gz(i,j,1)+scratch(i,j,1)+scratch(i,j,1)+scratch(i,j,2))
    end do
   end do
  end if
  if (ize.eq.mz) then
   do j=1,my
    do i=1,mx
      smooth3th(i,j,mz)=0.25*(scratch(i,j,mz-1)+scratch(i,j,mz)+scratch(i,j,mz)+hz(i,j,1))
    end do
   end do
  end if
  call barrier_omp('smooth3th')

END FUNCTION

!***********************************************************************
FUNCTION max3 (f)
!
!  Three point max
!
  USE params, gx=>gx1, hx=>hx1, gz=>gz1, hz=>hz1
  implicit none
  logical debug
  integer i,j,k,km1,kp1
  real, dimension(mx,my,mz):: f, max3
  real, dimension(0:mx+1):: fx
  real, dimension(0:my+1):: fy
  logical omp_in_parallel
!-----------------------------------------------------------------------
  character(len=mid):: id = "max3: $Id: smooth_max_do_mpi.f90,v 1.33 2013/11/03 10:10:02 aake Exp $" 
  call print_id (id)

  if (omp_in_parallel()) print *,'WARNING: max3 called from within parallel region!'

  if (mx < 3) then
    scratch=f
  else

    call mpi_send_x (f, gx, 1, hx, 1)

    do k=izs,ize
     do j=1,my
      do i=1,mx
        fx(i)=f(i,j,k)
      end do
      fx(0)=gx(1,j,k)
      fx(mx+1)=hx(1,j,k)
      do i=1,mx
        scratch(i,j,k)=amax1(fx(i-1),fx(i),fx(i+1))
      end do
     end do
    end do
  end if

  if (my > 2) then

    call mpi_send_y (scratch, gy1, 1, hy1, 1)

    do k=izs,ize
     do i=1,mx
      do j=1,my
        fy(j)=scratch(i,j,k)
      end do
      fy(0)=gy1(i,1,k)
      fy(my+1)=hy1(i,1,k)
      do j=1,my
        scratch(i,j,k)=amax1(fy(j-1),fy(j),fy(j+1))
      end do
     end do
    end do
  end if
 
  call barrier_omp('max3')

  if (mz < 3) then
    max3 = scratch
    return
  end if

  call mpi_send_z (scratch, gz, 1, hz, 1)

  do k=max(2,izs),min(mz-1,ize)
   do j=1,my
    do i=1,mx
      max3(i,j,k)=amax1(scratch(i,j,k),scratch(i,j,k-1),scratch(i,j,k+1))
    end do
   end do
  end do
  if (izs.eq.1) then
   do j=1,my
    do i=1,mx
      max3(i,j,1)=amax1(scratch(i,j,1),gz(i,j,1),scratch(i,j,2))
    end do
   end do
  end if
  if (ize.eq.mz) then
   do j=1,my
    do i=1,mx
      max3(i,j,mz)=amax1(scratch(i,j,mz),hz(i,j,1),scratch(i,j,mz-1))
    end do
   end do
  end if
  call barrier_omp('max3')

END FUNCTION

!***********************************************************************
FUNCTION max5 (f)
!
!  Three point max
!
  USE params, gx=>gx2, hx=>hx2
  implicit none
  integer i,j,k
  real, dimension(mx,my,mz), intent(in) :: f
  real, dimension(mx,my,mz):: max5
  real, dimension(-1:mx+2):: fx
  real, dimension(-1:my+2):: fy
  real, dimension(mx,my,2):: gz, hz
  logical omp_in_parallel
!-----------------------------------------------------------------------
  character(len=mid):: id = "max5: $Id: smooth_max_do_mpi.f90,v 1.33 2013/11/03 10:10:02 aake Exp $" 
  call print_id (id)

  if (omp_in_parallel()) print *,'WARNING: max5 called from within parallel region!'

  if (mx < 5) then
    scratch=f
  else

    call mpi_send_x (f, gx, 2, hx, 2)

    do k=izs,ize
     do j=1,my
      do i=1,mx
        fx(i)=f(i,j,k)
      end do
      fx(  -1)=gx(1,j,k)
      fx(   0)=gx(2,j,k)
      fx(mx+1)=hx(1,j,k)
      fx(mx+2)=hx(2,j,k)
      do i=1,mx
        scratch(i,j,k)=amax1(fx(i-2),fx(i-1),fx(i),fx(i+1),fx(i+2))
      end do
     end do
    end do
  end if

  call mpi_send_y (scratch, gy2, 2, hy2, 2)

  do k=izs,ize
   if (my.gt.1) then
     do i=1,mx
      do j=1,my
        fy(j)=scratch(i,j,k)
      end do
      fy(  -1)=gy2(i,1,k)
      fy(   0)=gy2(i,2,k)
      fy(my+1)=hy2(i,1,k)
      fy(my+2)=hy2(i,2,k)
      do j=1,my
        scratch(i,j,k)=amax1(fy(j-2),fy(j-1),fy(j),fy(j+1),fy(j+2))
      end do
     end do
    end if
  end do

  call barrier_omp('max5')

  if (mz < 5) then
    max5 = scratch
    return
  end if

  call mpi_send_z (scratch, gz, 2, hz, 2)

  do k=max(3,izs),min(mz-2,ize)
   do j=1,my
    do i=1,mx
      max5(i,j,k)=amax1(scratch(i,j,k-2),scratch(i,j,k-1),scratch(i,j,k),scratch(i,j,k+1),scratch(i,j,k+2))
    end do
   end do
  end do
  if (izs.eq.1) then
   do j=1,my
    do i=1,mx
      max5(i,j,1)=amax1(gz(i,j,1),gz(i,j,2),scratch(i,j,1),scratch(i,j,2),scratch(i,j,3))
      max5(i,j,2)=amax1(gz(i,j,2),scratch(i,j,1),scratch(i,j,2),scratch(i,j,3),scratch(i,j,4))
    end do
   end do
  end if
  if (ize.eq.mz) then
   do j=1,my
    do i=1,mx
      max5(i,j,mz-1)=amax1(scratch(i,j,mz-3),scratch(i,j,mz-2),scratch(i,j,mz-1),scratch(i,j,mz),hz(i,j,1))
      max5(i,j,mz  )=amax1(scratch(i,j,mz-2),scratch(i,j,mz-1),scratch(i,j,mz  ),hz(i,j,1 ),hz(i,j,2))
    end do
   end do
  end if
  call barrier_omp('max5')

END FUNCTION

!***********************************************************************
SUBROUTINE smooth3max5_set (f, fz)
!
!  Three point max
!
  USE params
  implicit none
  real, dimension(mx,my,mz) :: f, fz

  character(len=mid):: id = "sm3mx5: $Id: smooth_max_do_mpi.f90,v 1.33 2013/11/03 10:10:02 aake Exp $" 
  call print_id (id)
!
  call    max5_set (f ,fz)
  call smooth3_set (fz,f )
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE smooth3max3_set (f, fz)
!
!  Three point max
!
  USE params
  implicit none
  real, dimension(mx,my,mz) :: f, fz
  integer iz
  character(len=mid):: id = "sm3mx3: $Id: smooth_max_do_mpi.f90,v 1.33 2013/11/03 10:10:02 aake Exp $" 

  call print_id (id)
!
  call    max3_set (f ,fz)
  call smooth3_set (fz,f )
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE smooth3max3_xz (f, fz)
!
!  Three point max
!
  USE params
  implicit none
  real, dimension(mx,my,mz) :: f, fz
  integer iz

  character(len=mid):: id = "sm3mx3: $Id: smooth_max_do_mpi.f90,v 1.33 2013/11/03 10:10:02 aake Exp $" 
  call print_id (id)
!
  call    max3_set (f ,fz)
  !$omp master
  allocate (tmp1(mx,my,mz),tmp2(mx,my,mz))
  !$omp end master
  !$omp barrier
  call smooth3_set (fz,tmp1)
  call smooth3z_set (fz,tmp2)
  do iz=izs,ize
    f(:,:,iz) = 0.25*((tmp1(:,:,iz)+tmp2(:,:,iz))+(tmp2(:,:,iz)+tmp1(:,:,iz)))
  end do
  !$omp barrier
  !$omp master
  deallocate (tmp1,tmp2)
  !$omp end master
!
END SUBROUTINE

