#!/bin/csh

if (-d STAGGER) then
  set dir = STAGGER
else
  set dir = .
endif

foreach f ($*)
 ( cat $dir/$f;\
   sed -f $dir/stagger_set.sed $dir/$f; \
   sed -f $dir/stagger_set.sed $dir/$f | sed -f $dir/stagger_add.sed ; \
   sed -f $dir/stagger_set.sed $dir/$f | sed -f $dir/stagger_sub.sed ; \
   sed -f $dir/stagger_set.sed $dir/$f | sed -f $dir/stagger_neg.sed )
end
