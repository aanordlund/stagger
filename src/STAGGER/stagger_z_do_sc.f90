!***********************************************************************
FUNCTION zup (f)
!
!  f is centered on (i,j,k-.5), fifth order stagger
!
  use params
  real, dimension(mx,my,mz):: f, zup
!hpf$ distribute(*,*,block):: f, zup
!-----------------------------------------------------------------------
!
  character(len=80):: id = "$Id: stagger_z_do_sc.f90,v 1.3 2003/04/28 09:28:11 aake Exp $" 
  if (id.ne.' ') print '(1x,a)',id; id=' '
!
  c = 3./256.
  b = -25./256.
  a = .5-b-c
  a1st = 0.5
!
    do j=1,my
!$omp parallel do private(i,k)
  do k=3,mz-3
      do i=1,mx
        zup(i,j,k) = ( &
                     a*(f(i,j,k+1)+f(i,j,k  )) + &
                     b*(f(i,j,k+2)+f(i,j,k-1)) + &
                     c*(f(i,j,k+3)+f(i,j,k-2)))
      end do
    end do
  end do
  do j=1,my
    do i=1,mx
      zup(i,j,mz-2) = ( &
                   a*(f(i,j,mz-1)+f(i,j,mz-2)) + &
                   b*(f(i,j,mz  )+f(i,j,mz-3)) + &
                   c*(f(i,j,1   )+f(i,j,mz-4)))
      zup(i,j,mz-1) = ( &
                   a*(f(i,j,mz )+f(i,j,mz-1)) + &
                   b*(f(i,j,1  )+f(i,j,mz-2)) + &
                   c*(f(i,j,2  )+f(i,j,mz-3)))
      zup(i,j,mz) = ( &
                   a*(f(i,j,1  )+f(i,j,mz  )) + &
                   b*(f(i,j,1+1)+f(i,j,mz-1)) + &
                   c*(f(i,j,1+2)+f(i,j,mz-2)))
      zup(i,j,1) = ( &
                   a*(f(i,j,2  )+f(i,j,1   )) + &
                   b*(f(i,j,2+1)+f(i,j,mz  )) + &
                   c*(f(i,j,2+2)+f(i,j,mz-1)))
      zup(i,j,2) = ( &
                   a*(f(i,j,3  )+f(i,j,2   )) + &
                   b*(f(i,j,3+1)+f(i,j,1   )) + &
                   c*(f(i,j,3+2)+f(i,j,mz  )))
    end do
  end do
!
  nstag = nstag+1
  nflop = nflop+8
END FUNCTION

!***********************************************************************
FUNCTION zup1 (f)
!
!  f is centered on (i,j,k-.5), fifth order stagger
!
  use params
  real, dimension(mx,my,mz):: f, zup1
!hpf$ distribute(*,*,block):: f, zup1
!-----------------------------------------------------------------------
!
  a = .5
!
    do j=1,my
!$omp parallel do private(i,k)
  do k=1,mz-1
      do i=1,mx
        zup1(i,j,k) = ( &
                     a*(f(i,j,k+1)+f(i,j,k  )))
      end do
    end do
  end do
  do j=1,my
    do i=1,mx
      zup1(i,j,mz) = ( &
                   a*(f(i,j,1  )+f(i,j,mz  )))
    end do
  end do
!
  nstag1 = nstag1+1
  nflop = nflop+2
END FUNCTION

!***********************************************************************
FUNCTION zdn (f)
!
!  f is centered on (i,j,k), fifth order stagger
!
  use params
  real, dimension(mx,my,mz):: f, zdn
!hpf$ distribute(*,*,block):: f, zdn
!-----------------------------------------------------------------------
!
  c = 3./256.
  b = -25./256.
  a = .5-b-c
  a1st = 0.5
!
    do j=1,my
!$omp parallel do private(i,k)
  do k=4,mz-2
      do i=1,mx
        zdn(i,j,k) = ( &
                     a*(f(i,j,k  )+f(i,j,k-1)) + &
                     b*(f(i,j,k+1)+f(i,j,k-2)) + &
                     c*(f(i,j,k+2)+f(i,j,k-3)))
      end do
    end do
  end do
  do j=1,my
    do i=1,mx
      zdn(i,j,mz-1) = ( &
                   a*(f(i,j,mz-1)+f(i,j,mz-2)) + &
                   b*(f(i,j,mz  )+f(i,j,mz-3)) + &
                   c*(f(i,j,1   )+f(i,j,mz-4)))
      zdn(i,j,mz) = ( &
                   a*(f(i,j,mz )+f(i,j,mz-1)) + &
                   b*(f(i,j,1  )+f(i,j,mz-2)) + &
                   c*(f(i,j,2  )+f(i,j,mz-3)))
      zdn(i,j,1) = ( &
                   a*(f(i,j,1  )+f(i,j,mz  )) + &
                   b*(f(i,j,1+1)+f(i,j,mz-1)) + &
                   c*(f(i,j,1+2)+f(i,j,mz-2)))
      zdn(i,j,2) = ( &
                   a*(f(i,j,2  )+f(i,j,1   )) + &
                   b*(f(i,j,2+1)+f(i,j,mz  )) + &
                   c*(f(i,j,2+2)+f(i,j,mz-1)))
      zdn(i,j,3) = ( &
                   a*(f(i,j,3  )+f(i,j,2   )) + &
                   b*(f(i,j,3+1)+f(i,j,1   )) + &
                   c*(f(i,j,3+2)+f(i,j,mz  )))
    end do
  end do
!
  nstag = nstag+1
  nflop = nflop+8
END FUNCTION

!***********************************************************************
FUNCTION zdn1 (f)
!
!  f is centered on (i,j,k), fifth order stagger
!
  use params
  real, dimension(mx,my,mz):: f, zdn1
!hpf$ distribute(*,*,block):: f, zdn1
!-----------------------------------------------------------------------
!
  a = .5
!
    do j=1,my
!$omp parallel do private(i,k)
  do k=2,mz
      do i=1,mx
        zdn1(i,j,k) = ( &
                     a*(f(i,j,k  )+f(i,j,k-1)))
      end do
    end do
  end do
  do j=1,my
    do i=1,mx
      zdn1(i,j,1) = ( &
                   a*(f(i,j,1  )+f(i,j,mz  )))
    end do
  end do
!
  nstag1 = nstag1+1
  nflop = nflop+2
END FUNCTION

!***********************************************************************
FUNCTION ddzup (f)
  use params
  real, dimension(mx,my,mz):: f, ddzup
!hpf$ distribute(*,*,block):: f, ddzup
!-----------------------------------------------------------------------
!
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)/dz
  a1st = 1./dz
  b = b/dz
  c = c/dz
!
    do j=1,my
!$omp parallel do private(i,k)
  do k=3,mz-3
      do i=1,mx
        ddzup(i,j,k) = ( &
                     a*(f(i,j,k+1)-f(i,j,k  )) + &
                     b*(f(i,j,k+2)-f(i,j,k-1)) + &
                     c*(f(i,j,k+3)-f(i,j,k-2)))
      end do
    end do
  end do
  do j=1,my
    do i=1,mx
      ddzup(i,j,mz-2) = ( &
                   a*(f(i,j,mz-1)-f(i,j,mz-2)) + &
                   b*(f(i,j,mz  )-f(i,j,mz-3)) + &
                   c*(f(i,j,1   )-f(i,j,mz-4)))
      ddzup(i,j,mz-1) = ( &
                   a*(f(i,j,mz )-f(i,j,mz-1)) + &
                   b*(f(i,j,1  )-f(i,j,mz-2)) + &
                   c*(f(i,j,2  )-f(i,j,mz-3)))
      ddzup(i,j,mz) = ( &
                   a*(f(i,j,1  )-f(i,j,mz  )) + &
                   b*(f(i,j,1+1)-f(i,j,mz-1)) + &
                   c*(f(i,j,1+2)-f(i,j,mz-2)))
      ddzup(i,j,1) = ( &
                   a*(f(i,j,2  )-f(i,j,1   )) + &
                   b*(f(i,j,2+1)-f(i,j,mz  )) + &
                   c*(f(i,j,2+2)-f(i,j,mz-1)))
      ddzup(i,j,2) = ( &
                   a*(f(i,j,3  )-f(i,j,2   )) + &
                   b*(f(i,j,3+1)-f(i,j,1   )) + &
                   c*(f(i,j,3+2)-f(i,j,mz  )))
    end do
  end do
!
  nstag = nstag+1
  nflop = nflop+8
END FUNCTION

!***********************************************************************
FUNCTION ddzup1 (f)
  use params
  real, dimension(mx,my,mz):: f, ddzup1
!hpf$ distribute(*,*,block):: f, ddzup1
!-----------------------------------------------------------------------
!
  a = 1./dz
!
    do j=1,my
!$omp parallel do private(i,k)
  do k=1,mz-1
      do i=1,mx
        ddzup1(i,j,k) = ( &
                     a*(f(i,j,k+1)-f(i,j,k  )))
      end do
    end do
  end do
  do j=1,my
    do i=1,mx
      ddzup1(i,j,mz) = ( &
                   a*(f(i,j,1  )-f(i,j,mz  )))
    end do
  end do
!
  nstag1 = nstag1+1
  nflop = nflop+2
END FUNCTION

!***********************************************************************
FUNCTION ddzdn (f)
  use params
  real, dimension(mx,my,mz):: f, ddzdn
!hpf$ distribute(*,*,block):: f, ddzdn
!-----------------------------------------------------------------------
!
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)/dz
  a1st = 1./dz
  b = b/dz
  c = c/dz
!
    do j=1,my
!$omp parallel do private(i,k)
  do k=4,mz-2
      do i=1,mx
        ddzdn(i,j,k) = ( &
                     a*(f(i,j,k  )-f(i,j,k-1)) + &
                     b*(f(i,j,k+1)-f(i,j,k-2)) + &
                     c*(f(i,j,k+2)-f(i,j,k-3)))
      end do
    end do
  end do
  do j=1,my
    do i=1,mx
      ddzdn(i,j,mz-1) = ( &
                   a*(f(i,j,mz-1)-f(i,j,mz-2)) + &
                   b*(f(i,j,mz  )-f(i,j,mz-3)) + &
                   c*(f(i,j,1   )-f(i,j,mz-4)))
      ddzdn(i,j,mz) = ( &
                   a*(f(i,j,mz )-f(i,j,mz-1)) + &
                   b*(f(i,j,1  )-f(i,j,mz-2)) + &
                   c*(f(i,j,2  )-f(i,j,mz-3)))
      ddzdn(i,j,1) = ( &
                   a*(f(i,j,1  )-f(i,j,mz  )) + &
                   b*(f(i,j,1+1)-f(i,j,mz-1)) + &
                   c*(f(i,j,1+2)-f(i,j,mz-2)))
      ddzdn(i,j,2) = ( &
                   a*(f(i,j,2  )-f(i,j,1   )) + &
                   b*(f(i,j,2+1)-f(i,j,mz  )) + &
                   c*(f(i,j,2+2)-f(i,j,mz-1)))
      ddzdn(i,j,3) = ( &
                   a*(f(i,j,3  )-f(i,j,2   )) + &
                   b*(f(i,j,3+1)-f(i,j,1   )) + &
                   c*(f(i,j,3+2)-f(i,j,mz  )))
    end do
  end do
!
  nstag = nstag+1
  nflop = nflop+8
END FUNCTION

!***********************************************************************
FUNCTION ddzdn1 (f)
  use params
  real, dimension(mx,my,mz):: f, ddzdn1
!hpf$ distribute(*,*,block):: f, ddzdn1
!-----------------------------------------------------------------------
!
  a = 1./dz
!
    do j=1,my
!$omp parallel do private(i,k)
  do k=2,mz
      do i=1,mx
        ddzdn1(i,j,k) = ( &
                     a*(f(i,j,k  )-f(i,j,k-1)))
      end do
    end do
  end do
  do j=1,my
    do i=1,mx
      ddzdn1(i,j,1) = ( &
                   a*(f(i,j,1  )-f(i,j,mz  )))
    end do
  end do
!
  nstag1 = nstag1+1
  nflop = nflop+2
END FUNCTION
