# $Id: stagger_omp1.sed,v 1.4 2007/04/14 10:27:03 aake Exp $
/^ *FUNCTION/{
s/FUNCTION \([a-z1]*\)/SUBROUTINE \1_set/
s/(f *)/(f, fp)/
}
/^ *END/s/FUNCTION/SUBROUTINE/
/real,.*d*[xyz]up[i123]*$/s/d*[xyz]up[i123]*$/fp/
/::/s/\(d*[xyz]up[i123]*\)/fp/
/::/s/\(d*[xyz]dn[i123]*\)/fp/
s/d*[xyz]up[i123]* *= */fp = /
s/d*[xyz]dn[i123]* *= */fp = /
s/ d*[xyz]up[i123]* *(/ fp(/
s/ d*[xyz]dn[i123]* *(/ fp(/
s/omp parallel do/omp parallel/
s/= *1,mz/=izs,iZe/
s/Xomp/$omp/
s/1omp/$omp/
/2omp/d
