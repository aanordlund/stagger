!***********************************************************************
FUNCTION yup (f)
!
!  f is centered on (i,j-.5,k), fifth order stagger
!
  use params
  implicit none
  real a, b, c, a1st
  integer  j, k, jm2, jm1, jp1, jp2, jp3
  logical omp_in_parallel
  real, intent(in) , dimension(mx,my,mz):: f
  real,              dimension(mx,my,mz):: yup
!hpf$ distribute(*,*,block):: f, yup
!-----------------------------------------------------------------------
!
  character(len=mid):: id = "$Id: y_do_mod.f90,v 1.2 2012/12/31 16:10:46 aake Exp $" 
  if (id.ne.' '.and.izs.eq.1) print '(1x,a)',id; id=' '

  if (my .lt. 5) then
!$omp parallel private(k) if (.not. omp_in_parallel())
    do k=izs,ize
      yup(:,iys:iye,k) = f(:,iys:iye,k)
    end do
!$omp end parallel
    return
  end if
  c = 3./256.
  b = -25./256.
  a = .5-b-c
  a1st = 0.5
!
!$omp parallel private(i,j,k,jm2,jm1,jp1,jp2,jp3) if (.not. omp_in_parallel())
  do k=izs,ize
    do j=iys,iye
      jm2 = mod(j-3+my,my)+1
      jm1 = mod(j-2+my,my)+1
      jp1 = mod(j     ,my)+1
      jp2 = mod(j+1   ,my)+1
      jp3 = mod(j+2   ,my)+1
      yup(:,j,k) = ( &
                    a*(f(:,j  ,k)+f(:,jp1,k)) + &
                    b*(f(:,jm1,k)+f(:,jp2,k)) + &
                    c*(f(:,jm2,k)+f(:,jp3,k)))
    end do
  end do
!$omp end parallel
!
  nstag = nstag+1
  nflop = nflop+8
END FUNCTION

!***********************************************************************
FUNCTION yup1 (f)
!
!  f is centered on (i,j-.5,k), fifth order stagger
!
  use params
  implicit none
  real a, b, c, a1st
  integer j, k, jp1
  logical omp_in_parallel
  real, intent(in) , dimension(mx,my,mz):: f
  real,              dimension(mx,my,mz):: yup1
!hpf$ distribute(*,*,block):: f, yup1
!-----------------------------------------------------------------------
!
  if (my .lt. 5) then
!$omp parallel private(k) if (.not. omp_in_parallel())
    do k=izs,ize
      yup1(:,iys:iye,k) = f(:,iys:iye,k)
    end do
!$omp end parallel
    return
  end if
!
  a = 0.5
!
!$omp parallel private(i,j,k,jp1) if (.not. omp_in_parallel())
  do k=izs,ize
    do j=iys,iye
      jp1 = mod(j,my)+1
      yup1(:,j,k) = ( &
                     a*(f(:,j,k)+f(:,jp1,k)))
    end do
  end do
!$omp end parallel
!
  nstag1 = nstag1+1
  nflop = nflop+2
END FUNCTION

!***********************************************************************
FUNCTION ydn (f)
!
!  f is centered on (i,j,k), fifth order stagger
!
  use params
  implicit none
  real a, b, c, a1st
  integer j, k, jm3, jm2, jm1, jp1, jp2
  logical omp_in_parallel
  real, dimension(mx,my,mz):: f, ydn
!hpf$ distribute(*,*,block):: f, ydn
!
!-----------------------------------------------------------------------
  if (my .lt. 5) then
!$omp parallel private(k) if (.not. omp_in_parallel())
    do k=izs,ize
      ydn(:,iys:iye,k) = f(:,iys:iye,k)
    end do
!$omp end parallel
    return
  end if
  c = 3./256.
  b = -25./256.
  a = .5-b-c
  a1st = 0.5
!
!$omp parallel private(i,j,k,jm3,jm2,jm1,jp1,jp2) if (.not. omp_in_parallel())
  do k=izs,ize
    do j=iys,iye
      jm3 = mod(j-4+my,my)+1
      jm2 = mod(j-3+my,my)+1
      jm1 = mod(j-2+my,my)+1
      jp1 = mod(j     ,my)+1
      jp2 = mod(j+1   ,my)+1
      ydn(:,j,k) = ( &
                     a*(f(:,j  ,k)+f(:,jm1,k)) + &
                     b*(f(:,jp1,k)+f(:,jm2,k)) + &
                     c*(f(:,jp2,k)+f(:,jm3,k)))
    end do
  end do
!$omp end parallel
!
  nstag = nstag+1
  nflop = nflop+8
END FUNCTION

!***********************************************************************
FUNCTION ydn1 (f)
!
!  f is centered on (i,j,k), fifth order stagger
!
  use params
  implicit none
  real a, b, c, a1st
  integer j, k, jm1
  logical omp_in_parallel
  real, dimension(mx,my,mz):: f, ydn1
!hpf$ distribute(*,*,block):: f, ydn1
!
!-----------------------------------------------------------------------
  if (my .lt. 5) then
!$omp parallel private(k) if (.not. omp_in_parallel())
    do k=izs,ize
      ydn1(:,iys:iye,k) = f(:,iys:iye,k)
    end do
!$omp end parallel
    return
  end if
!
  a = 0.5
!
!$omp parallel private(i,j,k,jm1) if (.not. omp_in_parallel())
  do k=izs,ize
    do j=iys,iye
      jm1 = mod(j-2+my,my)+1
      ydn1(:,j,k) = ( &
                     a*(f(:,j,k)+f(:,jm1,k)))
    end do
  end do
!$omp end parallel
!
  nstag1 = nstag1+1
  nflop = nflop+2
END FUNCTION
