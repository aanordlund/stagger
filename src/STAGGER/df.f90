MODULE df
CONTAINS

!**********************************************************************
FUNCTION difx2 (f)
  USE params
  USE stagger
  real, dimension(mx,my,mz):: f, difx2
  difx2 = ddxdn(f)
END FUNCTION
!**********************************************************************
FUNCTION dify2 (f)
  USE params
  USE stagger
  real, dimension(mx,my,mz):: f, dify2
  dify2 = ddydn(f)
END FUNCTION
!**********************************************************************
FUNCTION difz2 (f)
  USE params
  USE stagger
  real, dimension(mx,my,mz):: f, difz2
  difz2 = ddzdn(f)
END FUNCTION

!**********************************************************************
FUNCTION difx1 (f)
  USE params
  USE stagger
  real, dimension(mx,my,mz):: f, difx1
  difx1 = ddxdn(f)
END FUNCTION
!**********************************************************************
FUNCTION dify1 (f)
  USE params
  USE stagger
  real, dimension(mx,my,mz):: f, dify1
  dify1 = ddydn(f)
END FUNCTION
!**********************************************************************
FUNCTION difz1 (f)
  USE params
  USE stagger
  real, dimension(mx,my,mz):: f, difz1
  difz1 = ddzdn(f)
END FUNCTION

!**********************************************************************
FUNCTION dix (f)
  USE params
  USE stagger
  real, dimension(mx,my,mz):: f, dix
  dix = ddxdn(f)
END FUNCTION
!**********************************************************************
FUNCTION diy (f)
  USE params
  USE stagger
  real, dimension(mx,my,mz):: f, diy
  diy = ddydn(f)
END FUNCTION
!**********************************************************************
FUNCTION diz (f)
  USE params
  USE stagger
  real, dimension(mx,my,mz):: f, diz
  diz = ddzdn(f)
END FUNCTION

!**********************************************************************
FUNCTION dfxdn (f)
  USE params
  USE stagger
  real, dimension(mx,my,mz):: f, dfxdn
  dfxdn = ddxdn(f)
END FUNCTION
!**********************************************************************
FUNCTION dfydn (f)
  USE params
  USE stagger
  real, dimension(mx,my,mz):: f, dfydn
  dfydn = ddydn(f)
END FUNCTION
!**********************************************************************
FUNCTION dfzdn (f)
  USE params
  USE stagger
  real, dimension(mx,my,mz):: f, dfzdn
  dfzdn = ddzdn(f)
END FUNCTION

!**********************************************************************
FUNCTION dfxup (f)
  USE params
  USE stagger
  real, dimension(mx,my,mz):: f, dfxup
  dfxup = ddxup(f)
END FUNCTION
!**********************************************************************
FUNCTION dfyup (f)
  USE params
  USE stagger
  real, dimension(mx,my,mz):: f, dfyup
  dfyup = ddyup(f)
END FUNCTION
!**********************************************************************
FUNCTION dfzup (f)
  USE params
  USE stagger
  real, dimension(mx,my,mz):: f, dfzup
  dfzup = ddzup(f)
END FUNCTION
END MODULE
