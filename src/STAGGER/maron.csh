foreach s ( x y z )
  sed -f maron.sed ${s}_do_mpi.f90 > ${s}_maron.f90
  sed -f maron.sed dd_${s}_do_mpi.f90 > dd_${s}_maron.f90
  sed -f maron.sed ddi_${s}_do_mpi.f90 > ddi_${s}_maron.f90
end
