!***********************************************************************
FUNCTION ddxupi (f)
  use params, g=>gx2, h=>hx3
  implicit none
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f, ddxupi
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    ddxupi = 0.
    return
  end if
!
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)
  a1st = 1.

  call mpi_send_x (f, g, 2, h, 3)
!
!2omp barrier
!$omp parallel private(i,j,k)
  do k=izs,ize
   do j=1,my
    ddxupi(mx-2,j,k) = ( &
                 + c*(h(1   ,j,k)-f(mx-4,j,k)) &
                 + b*(f(mx  ,j,k)-f(mx-3,j,k)) &
                 + a*(f(mx-1,j,k)-f(mx-2,j,k)))
    ddxupi(mx-1,j,k) = ( &
                 + c*(h(2   ,j,k)-f(mx-3,j,k)) &
                 + b*(h(1   ,j,k)-f(mx-2,j,k)) &
                 + a*(f(mx  ,j,k)-f(mx-1,j,k)))
    ddxupi(mx  ,j,k) = ( &
                 + c*(h(3   ,j,k)-f(mx-2,j,k)) &
                 + b*(h(2   ,j,k)-f(mx-1,j,k)) &
                 + a*(h(1   ,j,k)-f(mx  ,j,k)))
    ddxupi(1  ,j,k) = ( &
                 + c*(f(4   ,j,k)-g(1   ,j,k)) &
                 + b*(f(3   ,j,k)-g(2   ,j,k)) &
                 + a*(f(2   ,j,k)-f(1   ,j,k)))
    ddxupi(2  ,j,k) = ( &
                 + c*(f(5   ,j,k)-g(2   ,j,k)) &
                 + b*(f(4   ,j,k)-f(1   ,j,k)) &
                 + a*(f(3   ,j,k)-f(2   ,j,k)))
   end do
   do j=1,my
    do i=3,mx-3
      ddxupi(i  ,j,k) = ( &
                 + c*(f(i+3 ,j,k)-f(i-2 ,j,k)) &
                 + b*(f(i+2 ,j,k)-f(i-1 ,j,k)) &
                 + a*(f(i+1 ,j,k)-f(i   ,j,k)))
    end do
  end do
  end do
!$omp end parallel
!2omp barrier
!
  nstag = nstag+1
  nflop = nflop+8
END FUNCTION

!***********************************************************************
FUNCTION ddxupi1 (f)
  use params, g=>gx1
  implicit none
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f, ddxupi1
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    ddxupi1 = 0.
    return
  end if
  a = 1.

!$omp parallel private(i,j,k)

  call mpi_send_x (f, g, 0, g, 1)
!
!2omp barrier
  do k=izs,ize
   do j=1,my
    ddxupi1(mx  ,j,k) = ( &
                   a*(g(1  ,j,k)-f(mx,j,k)))
   end do
   do j=1,my
    do i=1,mx-1
      ddxupi1(i  ,j,k) = ( &
                   a*(f(i+1,j,k)-f(i ,j,k)))
    end do
  end do
  end do
!$omp end parallel
!2omp barrier
!
  nstag1 = nstag1+1
  nflop = nflop+2
END FUNCTION

!***********************************************************************
FUNCTION ddxdni (f)
  use params, g=>gx3, h=>hx2
  implicit none
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f, ddxdni
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    ddxdni = 0.
    return
  end if
!
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)
  a1st = 1.

!$omp parallel private(i,j,k)

  call mpi_send_x (f, g, 3, h, 2)
!
!2omp barrier
  do k=izs,ize
   do j=1,my
    ddxdni(mx-1,j,k) = ( &
                 + c*(h(1   ,j,k)-f(mx-4,j,k)) &
                 + b*(f(mx  ,j,k)-f(mx-3,j,k)) &
                 + a*(f(mx-1,j,k)-f(mx-2,j,k)))
    ddxdni(mx  ,j,k) = ( &
                 + c*(h(2   ,j,k)-f(mx-3,j,k)) &
                 + b*(h(1   ,j,k)-f(mx-2,j,k)) &
                 + a*(f(mx  ,j,k)-f(mx-1,j,k)))
    ddxdni(1   ,j,k) = ( &
                 + c*(f(3   ,j,k)-g(1   ,j,k)) &
                 + b*(f(2   ,j,k)-g(2   ,j,k)) &
                 + a*(f(1   ,j,k)-g(3   ,j,k)))
    ddxdni(2  ,j,k) = ( &
                 + c*(f(4   ,j,k)-g(2   ,j,k)) &
                 + b*(f(3   ,j,k)-g(3   ,j,k)) &
                 + a*(f(2   ,j,k)-f(1   ,j,k)))
    ddxdni(3  ,j,k) = ( &
                 + c*(f(5   ,j,k)-g(3   ,j,k)) &
                 + b*(f(4   ,j,k)-f(1   ,j,k)) &
                 + a*(f(3   ,j,k)-f(2   ,j,k)))
   end do
   do j=1,my
    do i=4,mx-2
      ddxdni(i,j,k) = ( &
                 + c*(f(i+2 ,j,k)-f(i-3 ,j,k)) &
                 + b*(f(i+1 ,j,k)-f(i-2 ,j,k)) &
                 + a*(f(i   ,j,k)-f(i-1 ,j,k)))
    end do
   end do
  end do
!$omp end parallel
!2omp barrier
!
  nstag = nstag+1
  nflop = nflop+8
END FUNCTION

!***********************************************************************
FUNCTION ddxdni1 (f)
  use params, g=>gx1
  implicit none
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f, ddxdni1
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    ddxdni1 = 0.
    return
  end if
  a = 1.

!$omp parallel private(i,j,k)

  call mpi_send_x (f, g, 1, g, 0)
!
!2omp barrier
  do k=izs,ize
   do j=1,my
    ddxdni1(1   ,j,k) = ( &
                   a*(f(1,j,k)-g(1  ,j,k)))
   end do
   do j=1,my
    do i=2,mx
      ddxdni1(i,j,k) = ( &
                   a*(f(i,j,k)-f(i-1,j,k)))
    end do
   end do
  end do
!$omp end parallel
!2omp barrier
!
  nstag1 = nstag1+1
  nflop = nflop+2
END FUNCTION
