!***********************************************************************
FUNCTION ddzup (f)
!
!  f is centered on (i,j,k-.5), fifth order stagger
!
  use params, g=>gz2, h=>hz3
  implicit none
  real a, b, c
  integer i, j, k
  real, intent(in) , dimension(mx,my,mz):: f
  real,              dimension(mx,my,mz):: ddzup
!-----------------------------------------------------------------------
!
  character(len=mid):: id = "$Id: dd_z_mesh_do_mpi.f90,v 1.9 2013/11/02 21:59:59 aake Exp $" 
  call print_id (id)

  if (mz .lt. 5) then
    !$omp parallel
    do k=izs,ize
      ddzup(:,:,k) = 0.
    end do
    !$omp end parallel
    return
  end if

  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)

  call mpi_send_z (f, g, 2, h, 3)
!
  !$omp parallel
  do k=max(izs,3),min(ize,mz-3)
    do j=1,my
      do i=1,mx
        ddzup(i,j,k) = dzidzup(k)*( &
                     c*(f(i,j,k+3 )-f(i,j,k-2 )) + &
                     b*(f(i,j,k+2 )-f(i,j,k-1 )) + &
                     a*(f(i,j,k+1 )-f(i,j,k   )))
     end do
    end do
  end do
  if (ize.eq.mz) then
   do j=1,my
    do i=1,mx
      ddzup(i,j,mz-2) = dzidzup(mz-2)*( &
                     c*(h(i,j,1   )-f(i,j,mz-4)) + &
                     b*(f(i,j,mz  )-f(i,j,mz-3)) + &
                     a*(f(i,j,mz-1)-f(i,j,mz-2)))
      ddzup(i,j,mz-1) = dzidzup(mz-1)*( &
                     c*(h(i,j,2   )-f(i,j,mz-3)) + &
                     b*(h(i,j,1   )-f(i,j,mz-2)) + &
                     a*(f(i,j,mz  )-f(i,j,mz-1)))
      ddzup(i,j,mz  ) = dzidzup(mz)*( &
                     c*(h(i,j,3   )-f(i,j,mz-2)) + &
                     b*(h(i,j,2   )-f(i,j,mz-1)) + &
                     a*(h(i,j,1   )-f(i,j,mz  )))
    end do
   end do
  end if
  if (izs.eq.1) then
   do j=1,my
    do i=1,mx
      ddzup(i,j,1   ) = dzidzup(1)*( &
                     c*(f(i,j,4   )-g(i,j,1   )) + &
                     b*(f(i,j,3   )-g(i,j,2   )) + &
                     a*(f(i,j,2   )-f(i,j,1   )))
      ddzup(i,j,2   ) = dzidzup(2)*( &
                     c*(f(i,j,5   )-g(i,j,2   )) + &
                     b*(f(i,j,4   )-f(i,j,1   )) + &
                     a*(f(i,j,3   )-f(i,j,2   )))
    end do
   end do
  end if
  !$omp end parallel
!
  if (omp_master) then
    nstag = nstag+1
    nflop = nflop+8
  end if
END FUNCTION

!***********************************************************************
FUNCTION ddzup1 (f)
!
!  f is centered on (i,j,k-.5), fifth order stagger
!
  use params, g=>gz1
  implicit none
  real a, b, c
  integer i, j, k
  real, intent(in) , dimension(mx,my,mz):: f
  real,              dimension(mx,my,mz):: ddzup1
!-----------------------------------------------------------------------
!
  if (mz .lt. 5) then
    !$omp parallel
    do k=izs,ize
      ddzup1(:,:,k) = 0.
    end do
    !$omp end parallel
    return
  end if
!
  a = 1.
  call mpi_send_z (f, g, 0, g, 1)
!
  !$omp parallel
  do k=max(izs,1),min(ize,mz-1)
    do j=1,my
      do i=1,mx
        ddzup1(i,j,k  ) = dzidzup(k)*( &
                     a*(f(i,j,k+1)-f(i,j,k)))
      end do
    end do
  end do
  if (ize.eq.mz) then
   do j=1,my
    do i=1,mx
      ddzup1(i,j,mz) = dzidzup(mz)*( &
                     a*(g(i,j,1)-f(i,j,mz)))
    end do
   end do
  end if
  !$omp end parallel
!
  if (omp_master) then
    nstag1 = nstag1+1
    nflop = nflop+2
  end if
END FUNCTION

!***********************************************************************
FUNCTION ddzdn (f)
!
!  f is centered on (i,j), fifth order stagger
!
  use params, g=>gz3, h=>hz2
  implicit none
  real a, b, c
  integer i, j, k
  real, dimension(mx,my,mz):: f, ddzdn
!-----------------------------------------------------------------------
!
  if (mz .lt. 5) then
    !$omp parallel
    do k=izs,ize
      ddzdn(:,:,k) = 0.
    end do
    !$omp end parallel
    return
  end if

  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)

  call mpi_send_z (f, g, 3, h, 2)
!
  !$omp parallel
  do k=max(izs,4),min(ize,mz-2)
    do j=1,my
     do i=1,mx
      ddzdn(i,j,k) = dzidzdn(k)*( &
                     c*(f(i,j,k+2 )-f(i,j,k-3 )) + &
                     b*(f(i,j,k+1 )-f(i,j,k-2 )) + &
                     a*(f(i,j,k   )-f(i,j,k-1 )))
     end do
    end do
  end do
  if (ize.eq.mz) then
   do j=1,my
    do i=1,mx
      ddzdn(i,j,mz-1) = dzidzdn(mz-1)*( &
                     c*(h(i,j,1   )-f(i,j,mz-4)) + &
                     b*(f(i,j,mz  )-f(i,j,mz-3)) + &
                     a*(f(i,j,mz-1)-f(i,j,mz-2)))
      ddzdn(i,j,mz  ) = dzidzdn(mz)*( &
                     c*(h(i,j,2   )-f(i,j,mz-3)) + &
                     b*(h(i,j,1   )-f(i,j,mz-2)) + &
                     a*(f(i,j,mz  )-f(i,j,mz-1)))
    end do
   end do
  end if
  if (izs.eq.1) then
   do j=1,my
    do i=1,mx
      ddzdn(i,j,1   ) =dzidzdn(1)* ( &
                     c*(f(i,j,3   )-g(i,j,1   )) + &
                     b*(f(i,j,2   )-g(i,j,2   )) + &
                     a*(f(i,j,1   )-g(i,j,3   )))
      ddzdn(i,j,2   ) = dzidzdn(2)*( &
                     c*(f(i,j,4   )-g(i,j,2   )) + &
                     b*(f(i,j,3   )-g(i,j,3   )) + &
                     a*(f(i,j,2   )-f(i,j,1   )))
      ddzdn(i,j,3   ) = dzidzdn(3)*( &
                     c*(f(i,j,5   )-g(i,j,3   )) + &
                     b*(f(i,j,4   )-f(i,j,1   )) + &
                     a*(f(i,j,3   )-f(i,j,2   )))
    end do
   end do
  end if
  !$omp end parallel
!
  if (omp_master) then
    nstag = nstag+1
    nflop = nflop+8
  end if
END FUNCTION

!***********************************************************************
FUNCTION ddzdn1 (f)
!
!  f is centered on (i,j), fifth order stagger
!
  use params, g=>gz1
  implicit none
  real a, b, c
  integer i, j, k
  real, dimension(mx,my,mz):: f, ddzdn1
!-----------------------------------------------------------------------
!
  if (mz .lt. 5) then
    !$omp parallel
    do k=izs,ize
      ddzdn1(:,:,k) = 0.
    end do
    !$omp end parallel
    return
  end if
!
  a = 1.
  call mpi_send_z (f, g, 1, g, 0)
!
  !$omp parallel
  do k=max(izs,2),min(ize,mz)
    do j=1,my
     do i=1,mx
      ddzdn1(i,j,k) = dzidzdn(k)*( &
                     a*(f(i,j,k)-f(i,j,k-1)))
     end do
    end do
  end do
  if (izs.eq.1) then
   do j=1,my
    do i=1,mx
      ddzdn1(i,j,1   ) = dzidzdn(1)*( &
                     a*(f(i,j,1)-g(i,j,1  )))
    end do
   end do
  end if
  !$omp end parallel
!
  if (omp_master) then
    nstag1 = nstag1+1
    nflop = nflop+2
  end if
END FUNCTION
!***********************************************************************
FUNCTION ddzup32 (f,j)
  use params, g=>gz2, h=>hz3
  implicit none
  real a, b, c
  integer i, j, k
  real,             dimension(mx,mz):: ddzup32
  real, intent(in), dimension(mx,my,mz):: f
!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    !$omp parallel
    do k=izs,ize
      ddzup32(:,k) = 0.
    end do
    !$omp end parallel
    return
  endif

  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)

  call mpi_send_z (f, g, 2, h, 3)
!
!$omp parallel private(i,j,k)
  do k=max(izs,3),min(ize,mz-3)
      do i=1,mx
        ddzup32(i,k) = dzidzup(k)*( &
                     c*(f(i,j,k+3)-f(i,j,k-2)) + &
                     b*(f(i,j,k+2)-f(i,j,k-1)) + &
                     a*(f(i,j,k+1)-f(i,j,k  )))
      end do
  end do
  if (izs.eq.1) then
    do i=1,mx
      ddzup32(i,1   ) = dzidzup(1)*( &
                     c*(f(i,j,4 )-g(i,1,1 )) + &
                     b*(f(i,j,3 )-g(i,1,2 )) + &
                     a*(f(i,j,2 )-f(i,j,1 )))
      ddzup32(i,2   ) = dzidzup(2)*( &
                     c*(f(i,j,5 )-g(i,1,2 )) + &
                     b*(f(i,j,4 )-f(i,j,1 )) + &
                     a*(f(i,j,3 )-f(i,j,2 )))
    end do
  end if
  if (ize.eq.mz) then
    do i=1,mx
      ddzup32(i,mz-2) = dzidzup(mz-2)*( &
                     c*(h(i,1,1   )-f(i,j,mz-4)) + &
                     b*(f(i,j,mz  )-f(i,j,mz-3)) + &
                     a*(f(i,j,mz-1)-f(i,j,mz-2)))
      ddzup32(i,mz-1) = dzidzup(mz-1)*( &
                     c*(h(i,1,2   )-f(i,j,mz-3)) + &
                     b*(h(i,1,1   )-f(i,j,mz-2)) + &
                     a*(f(i,j,mz  )-f(i,j,mz-1)))
      ddzup32(i,mz  ) = dzidzup(mz)*( &
                     c*(h(i,1,3   )-f(i,j,mz-2)) + &
                     b*(h(i,1,2   )-f(i,j,mz-1)) + &
                     a*(h(i,1,1   )-f(i,j,mz  )))
    end do
  end if

!$omp end parallel
!
  if (omp_master) then
    nstag = nstag+1
    nflop = nflop+8
  end if
END FUNCTION

!***********************************************************************
FUNCTION ddzup132 (f,j)
  use params, g=>gz1
  implicit none
  real a, b, c
  integer i, j, k
  real, dimension(mx,mz):: ddzup132
  real, intent(in), dimension(mx,my,mz):: f
!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    !$omp parallel
    do k=izs,ize
      ddzup132(:,k) = 0.
    end do
    !$omp end parallel
    return
  endif
  a = 1.
  call mpi_send_z (f, g, 0, g, 1)
!
!$omp parallel private(i,j,k)
  do k=max(izs,1),min(ize,mz-1)
      do i=1,mx
        ddzup132(i,k) = dzidzup(k)*( &
                     a*(f(i,j,k+1)-f(i,j,k  )))
      end do
  end do
  if (ize.eq.mz) then
    do i=1,mx
      ddzup132(i,mz) = dzidzup(mz)*( &
                     a*(g(i,j,1)-f(i,j,mz)))
    end do
  end if

!$omp end parallel
!
  if (omp_master) then
    nstag1 = nstag1+1
    nflop = nflop+2
  end if
END FUNCTION

!***********************************************************************
FUNCTION ddzdn32 (f,j)
  use params, g=>gz2, h=>hz3
  implicit none
  real a, b, c
  integer i, j, k
  real, dimension(mx,mz):: ddzdn32
  real, dimension(mx,my,mz):: f
!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    !$omp parallel
    do k=izs,ize
      ddzdn32(:,k) = 0.
    end do
    !$omp end parallel
    return
  endif
!
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)

  call mpi_send_z (f, g, 3, h, 2)
!
!$omp parallel private(i,j,k)
  do k=max(izs,4),min(ize,mz-2)
      do i=1,mx
        ddzdn32(i,k) = dzidzdn(k)*( &
                     c*(f(i,j,k+2)-f(i,j,k-3)) + &
                     b*(f(i,j,k+1)-f(i,j,k-2)) + &
                     a*(f(i,j,k  )-f(i,j,k-1)))
      end do
  end do
  if (izs.eq.1) then
    do i=1,mx
      ddzdn32(i,1   ) =dzidzdn(1)* ( &
                     c*(f(i,j,3   )-g(i,j,1   )) + &
                     b*(f(i,j,2   )-g(i,j,2   )) + &
                     a*(f(i,j,1   )-g(i,j,3   )))
      ddzdn32(i,2   ) = dzidzdn(2)*( &
                     c*(f(i,j,4   )-g(i,j,2   )) + &
                     b*(f(i,j,3   )-g(i,j,3   )) + &
                     a*(f(i,j,2   )-f(i,j,1   )))
      ddzdn32(i,3   ) = dzidzdn(3)*( &
                     c*(f(i,j,5   )-g(i,j,3   )) + &
                     b*(f(i,j,4   )-f(i,j,1   )) + &
                     a*(f(i,j,3   )-f(i,j,2   )))
    end do
  end if
  if (ize.eq.mz) then
    do i=1,mx
      ddzdn32(i,mz-1) = dzidzdn(mz-1)*( &
                     c*(h(i,j,1   )-f(i,j,mz-4)) + &
                     b*(f(i,j,mz  )-f(i,j,mz-3)) + &
                     a*(f(i,j,mz-1)-f(i,j,mz-2)))
      ddzdn32(i,mz  ) = dzidzdn(mz)*( &
                     c*(h(i,j,2   )-f(i,j,mz-3)) + &
                     b*(h(i,j,1   )-f(i,j,mz-2)) + &
                     a*(f(i,j,mz  )-f(i,j,mz-1)))
    end do
  end if

!$omp end parallel
!
  if (omp_master) then
    nstag = nstag+1
    nflop = nflop+8
  end if
END FUNCTION

!***********************************************************************
FUNCTION ddzdn132 (f,j)
  use params, g=>gz1
  implicit none
  real a, b, c
  integer i, j, k
  real, dimension(mx,mz):: ddzdn132
  real, dimension(mx,my,mz):: f
!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    !$omp parallel
    do k=izs,ize
      ddzdn132(:,k) = 0.
    end do
    !$omp end parallel
    return
  endif
!
  a = 1.
  call mpi_send_z (f, g, 1, g, 0)
!
!$omp parallel private(i,j,k,km1)
  do k=max(izs,2),min(ize,mz)
      do i=1,mx
        ddzdn132(i,k) = dzidzdn(k)*( &
                     a*(f(i,j,k  )-f(i,j,k-1)))
      end do
  end do
  if (izs.eq.1) then
    do i=1,mx
      ddzdn132(i,1   ) = dzidzdn(1)*( &
                     a*(f(i,j,1)-g(i,j,1  )))
    end do
  end if

!$omp end parallel
!
  if (omp_master) then
    nstag1 = nstag1+1
    nflop = nflop+2
  end if
END FUNCTION
!***********************************************************************
FUNCTION ddzup22 (f)
  use params
  implicit none
  real a, b, c
  integer i, j, k
  real,             dimension(mx,  mz):: ddzup22
  real, intent(in), dimension(mx,  mz):: f
  real,             dimension(mx,1, 2):: g
  real,             dimension(mx,1, 3):: h
!
!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    !$omp parallel
    do k=izs,ize
      ddzup22(:,k) = 0.
    end do
    !$omp end parallel
    return
  endif

  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)

  call mpi_send2_z (f, mx, 1, mz, g, 2, h, 3)
!
!$omp parallel private(i,j,k)
  do k=max(izs,3),min(ize,mz-3)
      do i=1,mx
        ddzup22(i,k) = dzidzup(k)*( &
                     c*(f(i,k+3)-f(i,k-2 )) + &
                     b*(f(i,k+2)-f(i,k-1 )) + &
                     a*(f(i,k+1)-f(i,k   )))
      end do
  end do
  if (izs.eq.1) then
    do i=1,mx
      ddzup22(i,1   ) = dzidzup(1)*( &
                     c*(f(i,4   )-g(i,1,1)) + &
                     b*(f(i,3   )-g(i,1,2)) + &
                     a*(f(i,2   )-f(i,  1)))
      ddzup22(i,2   ) = dzidzup(2)*( &
                     c*(f(i,5   )-g(i,1,2)) + &
                     b*(f(i,4   )-f(i,  1)) + &
                     a*(f(i,3   )-f(i,  2)))
    end do
  end if
  if (ize.eq.mz) then
    do i=1,mx
      ddzup22(i,mz-2) = dzidzup(mz-2)*( &
                     c*(h(i,1,1   )-f(i,  mz-4)) + &
                     b*(f(i,  mz  )-f(i,  mz-3)) + &
                     a*(f(i,  mz-1)-f(i,  mz-2)))
      ddzup22(i,mz-1) = dzidzup(mz-1)*( &
                     c*(h(i,1,2   )-f(i,  mz-3)) + &
                     b*(h(i,1,1   )-f(i,  mz-2)) + &
                     a*(f(i,  mz  )-f(i,  mz-1)))
      ddzup22(i,mz  ) = dzidzup(mz)*( &
                     c*(h(i,1,3   )-f(i,  mz-2)) + &
                     b*(h(i,1,2   )-f(i,  mz-1)) + &
                     a*(h(i,1,1   )-f(i,  mz  )))
    end do
  end if

!$omp end parallel
!
  if (omp_master) then
    nstag = nstag+1
    nflop = nflop+8
  end if
END FUNCTION

!***********************************************************************
FUNCTION ddzup122 (f)
  use params
  implicit none
  real a, b, c
  integer i, j, k
  real,             dimension(mx,   mz):: ddzup122
  real, intent(in), dimension(mx,   mz):: f
  real,             dimension(mx, 1, 1):: g
!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    !$omp parallel
    do k=izs,ize
      ddzup122(:,k) = 0.
    end do
    !$omp end parallel
    return
  endif
  a = 1.
  call mpi_send2_z (f, mx, 1, mz, g, 0, g, 1)
!
!$omp parallel private(i,k)
  do k=max(izs,1),min(ize,mz-1)
      do i=1,mx
        ddzup122(i,k) = dzidzup(k)*( &
                     a*(f(i,k+1)-f(i,  k )))
      end do
  end do
  if (ize.eq.mz) then
    do i=1,mx
      ddzup122(i,mz) = dzidzup(mz)*( &
                     a*(g(i,1,1)-f(i,  mz)))
    end do
  end if

!$omp end parallel
!
  if (omp_master) then
    nstag1 = nstag1+1
    nflop = nflop+2
  end if
END FUNCTION

!***********************************************************************
FUNCTION ddzdn22 (f)
  use params
  implicit none
  real a, b, c
  integer i, j, k
  real, dimension(mx,  mz):: ddzdn22
  real, dimension(mx,  mz):: f
  real, dimension(mx,1, 3):: g
  real, dimension(mx,1, 2):: h
!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    !$omp parallel
    do k=izs,ize
      ddzdn22(:,k) = 0.
    end do
    !$omp end parallel
    return
  endif
!
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)

  call mpi_send2_z (f, mx, 1, mz, g, 3, h, 2)
!
!$omp parallel private(i,k)
  do k=max(izs,4),min(ize,mz-2)
      do i=1,mx
        ddzdn22(i,k) = dzidzdn(k)*( &
                     c*(f(i,k+2)-f(i,k-3)) + &
                     b*(f(i,k+1)-f(i,k-2)) + &
                     a*(f(i,k  )-f(i,k-1)))
      end do
  end do
  if (izs.eq.1) then
    do i=1,mx
      ddzdn22(i,1   ) =dzidzdn(1)* ( &
                     c*(f(i,  3   )-g(i,1,1   )) + &
                     b*(f(i,  2   )-g(i,1,2   )) + &
                     a*(f(i,  1   )-g(i,1,3   )))
      ddzdn22(i,2   ) = dzidzdn(2)*( &
                     c*(f(i,  4   )-g(i,1,2   )) + &
                     b*(f(i,  3   )-g(i,1,3   )) + &
                     a*(f(i,  2   )-f(i,  1   )))
      ddzdn22(i,3   ) = dzidzdn(3)*( &
                     c*(f(i,  5   )-g(i,1,3   )) + &
                     b*(f(i,  4   )-f(i,  1   )) + &
                     a*(f(i,  3   )-f(i,  2   )))
    end do
  end if
  if (ize.eq.mz) then
    do i=1,mx
      ddzdn22(i,mz-1) = dzidzdn(mz-1)*( &
                     c*(h(i,1,1   )-f(i,  mz-4)) + &
                     b*(f(i,  mz  )-f(i,  mz-3)) + &
                     a*(f(i,  mz-1)-f(i,  mz-2)))
      ddzdn22(i,mz  ) = dzidzdn(mz)*( &
                     c*(h(i,1,2   )-f(i,  mz-3)) + &
                     b*(h(i,1,1   )-f(i,  mz-2)) + &
                     a*(f(i,  mz  )-f(i,  mz-1)))
    end do
  end if

!$omp end parallel
!
  if (omp_master) then
    nstag = nstag+1
    nflop = nflop+8
  end if
END FUNCTION

!***********************************************************************
FUNCTION ddzdn122 (f)
  use params
  implicit none
  real a, b, c
  integer i, j, k
  real, dimension(mx,   mz):: ddzdn122
  real, dimension(mx,   mz):: f
  real, dimension(mx, 1, 1):: g
!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    !$omp parallel
    do k=izs,ize
      ddzdn122(:,k) = 0.
    end do
    !$omp end parallel
    return
  endif
!
  a = 1.
  call mpi_send2_z (f, mx, 1, mz, g, 1, g, 0)
!
!$omp parallel private(i,k,km1)
  do k=max(izs,2),min(ize,mz)
      do i=1,mx
        ddzdn122(i,k) = dzidzdn(k)*( &
                     a*(f(i,k  )-f(i,k-1)))
      end do
  end do
  if (izs.eq.1) then
    do i=1,mx
      ddzdn122(i,1   ) = dzidzdn(1)*( &
                     a*(f(i,  1)-g(i,1,1  )))
    end do
  end if

!$omp end parallel
!
  if (omp_master) then
    nstag1 = nstag1+1
    nflop = nflop+2
  end if
END FUNCTION
