!***********************************************************************
FUNCTION ddzupi (f)
  use params
  real, dimension(mx,my,mz):: f, ddzupi
!hpf$ distribute(*,*,block):: f, ddzupi
!-----------------------------------------------------------------------
!
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)
  a1st = 1.
!
!$omp parallel do private(i,j,k)
  do k=3,mz-3
    do j=1,my
      do i=1,mx
        ddzupi(i,j,k) = ( &
                     a*(f(i,j,k+1)-f(i,j,k  )) + &
                     b*(f(i,j,k+2)-f(i,j,k-1)) + &
                     c*(f(i,j,k+3)-f(i,j,k-2)))
      end do
    end do
  end do
  do j=1,my
    do i=1,mx
      ddzupi(i,j,mz-2) = ( &
                   a*(f(i,j,mz-1)-f(i,j,mz-2)) + &
                   b*(f(i,j,mz  )-f(i,j,mz-3)) + &
                   c*(f(i,j,1   )-f(i,j,mz-4)))
      ddzupi(i,j,mz-1) = ( &
                   a*(f(i,j,mz )-f(i,j,mz-1)) + &
                   b*(f(i,j,1  )-f(i,j,mz-2)) + &
                   c*(f(i,j,2  )-f(i,j,mz-3)))
      ddzupi(i,j,mz) = ( &
                   a*(f(i,j,1  )-f(i,j,mz  )) + &
                   b*(f(i,j,1+1)-f(i,j,mz-1)) + &
                   c*(f(i,j,1+2)-f(i,j,mz-2)))
      ddzupi(i,j,1) = ( &
                   a*(f(i,j,2  )-f(i,j,1   )) + &
                   b*(f(i,j,2+1)-f(i,j,mz  )) + &
                   c*(f(i,j,2+2)-f(i,j,mz-1)))
      ddzupi(i,j,2) = ( &
                   a*(f(i,j,3  )-f(i,j,2   )) + &
                   b*(f(i,j,3+1)-f(i,j,1   )) + &
                   c*(f(i,j,3+2)-f(i,j,mz  )))
    end do
  end do
!
  nstag = nstag+1
  nflop = nflop+8
END FUNCTION

!***********************************************************************
FUNCTION ddzupi1 (f)
  use params
  real, dimension(mx,my,mz):: f, ddzupi1
!hpf$ distribute(*,*,block):: f, ddzupi1
!-----------------------------------------------------------------------
!
  a = 1.
!
!$omp parallel do private(i,j,k)
  do k=1,mz-1
    do j=1,my
      do i=1,mx
        ddzupi1(i,j,k) = ( &
                     a*(f(i,j,k+1)-f(i,j,k  )))
      end do
    end do
  end do
  do j=1,my
    do i=1,mx
      ddzupi1(i,j,mz) = ( &
                   a*(f(i,j,1  )-f(i,j,mz  )))
    end do
  end do
!
  nstag1 = nstag1+1
  nflop = nflop+2
END FUNCTION

!***********************************************************************
FUNCTION ddzdni (f)
  use params
  real, dimension(mx,my,mz):: f, ddzdni
!hpf$ distribute(*,*,block):: f, ddzdni
!-----------------------------------------------------------------------
!
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)
  a1st = 1.
!
!$omp parallel do private(i,j,k)
  do k=4,mz-2
    do j=1,my
      do i=1,mx
        ddzdni(i,j,k) = ( &
                     a*(f(i,j,k  )-f(i,j,k-1)) + &
                     b*(f(i,j,k+1)-f(i,j,k-2)) + &
                     c*(f(i,j,k+2)-f(i,j,k-3)))
      end do
    end do
  end do
  do j=1,my
    do i=1,mx
      ddzdni(i,j,mz-1) = ( &
                   a*(f(i,j,mz-1)-f(i,j,mz-2)) + &
                   b*(f(i,j,mz  )-f(i,j,mz-3)) + &
                   c*(f(i,j,1   )-f(i,j,mz-4)))
      ddzdni(i,j,mz) = ( &
                   a*(f(i,j,mz )-f(i,j,mz-1)) + &
                   b*(f(i,j,1  )-f(i,j,mz-2)) + &
                   c*(f(i,j,2  )-f(i,j,mz-3)))
      ddzdni(i,j,1) = ( &
                   a*(f(i,j,1  )-f(i,j,mz  )) + &
                   b*(f(i,j,1+1)-f(i,j,mz-1)) + &
                   c*(f(i,j,1+2)-f(i,j,mz-2)))
      ddzdni(i,j,2) = ( &
                   a*(f(i,j,2  )-f(i,j,1   )) + &
                   b*(f(i,j,2+1)-f(i,j,mz  )) + &
                   c*(f(i,j,2+2)-f(i,j,mz-1)))
      ddzdni(i,j,3) = ( &
                   a*(f(i,j,3  )-f(i,j,2   )) + &
                   b*(f(i,j,3+1)-f(i,j,1   )) + &
                   c*(f(i,j,3+2)-f(i,j,mz  )))
    end do
  end do
!
  nstag = nstag+1
  nflop = nflop+8
END FUNCTION

!***********************************************************************
FUNCTION ddzdni1 (f)
  use params
  real, dimension(mx,my,mz):: f, ddzdni1
!hpf$ distribute(*,*,block):: f, ddzdni1
!-----------------------------------------------------------------------
!
  a = 1.
!
!$omp parallel do private(i,j,k)
  do k=2,mz
    do j=1,my
      do i=1,mx
        ddzdni1(i,j,k) = ( &
                     a*(f(i,j,k  )-f(i,j,k-1)))
      end do
    end do
  end do
  do j=1,my
    do i=1,mx
      ddzdni1(i,j,1) = ( &
                   a*(f(i,j,1  )-f(i,j,mz  )))
    end do
  end do
!
  nstag1 = nstag1+1
  nflop = nflop+2
END FUNCTION
