!***********************************************************************
FUNCTION ddyup (f)
  use params, g=>gy2, h=>gy3
  implicit none
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f, ddyup
!-----------------------------------------------------------------------

  if (my .lt. 3) then
    ddyup = 0.
    return
  end if
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)/dy
  a1st = 1./dy
  b = b/dy
  c = c/dy

  call mpi_send_y (f, g, 2, h, 3)

  !$omp parallel private(i,j,k)
  do k=izs,ize
    do i=1,mx
      ddyup(i,my-2,k) = ( &
                     c*(h(i,1   ,k)-f(i,my-4,k)) + &
                     b*(f(i,my  ,k)-f(i,my-3,k)) + &
                     a*(f(i,my-1,k)-f(i,my-2,k)) )
      ddyup(i,my-1,k) = ( &
                     c*(h(i,2   ,k)-f(i,my-3,k)) + &
                     b*(h(i,1   ,k)-f(i,my-2,k)) + &
                     a*(f(i,my  ,k)-f(i,my-1,k)) )
      ddyup(i,my  ,k) = ( &
                     c*(h(i,3   ,k)-f(i,my-2,k)) + &
                     b*(h(i,2   ,k)-f(i,my-1,k)) + &
                     a*(h(i,1   ,k)-f(i,my  ,k)) )
      ddyup(i,1   ,k) = ( &
                     c*(f(i,4   ,k)-g(i,1   ,k)) + &
                     b*(f(i,3   ,k)-g(i,2   ,k)) + &
                     a*(f(i,2   ,k)-f(i,1   ,k)) )
      ddyup(i,2   ,k) = ( &
                     c*(f(i,5   ,k)-g(i,2   ,k)) + &
                     b*(f(i,4   ,k)-f(i,1   ,k)) + &
                     a*(f(i,3   ,k)-f(i,2   ,k)) )
    end do
    do j=3,my-3
     do i=1,mx
      ddyup(i,j  ,k) = ( &
                     c*(f(i,j+3 ,k)-f(i,j-2 ,k)) + &
                     b*(f(i,j+2 ,k)-f(i,j-1 ,k)) + &
                     a*(f(i,j+1 ,k)-f(i,j   ,k)) )
     end do
    end do
  end do
  !$omp end parallel

  !$omp master
  nstag = nstag+1
  nflop = nflop+8
  !$omp end master
END FUNCTION

!***********************************************************************
FUNCTION ddyup1 (f)
  use params, g=>gy1
  implicit none
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f, ddyup1
!-----------------------------------------------------------------------

  if (my .lt. 3) then
    ddyup1 = 0.
    return
  end if
  a = 1./dy

  call mpi_send_y (f, g, 0, g, 1)

  !$omp parallel private(i,j,k)
  do k=izs,ize
    do i=1,mx
      ddyup1(i,my,k) = ( &
                     a*(g(i,1  ,k)-f(i,my,k)))
    end do
    do j=1,my-1
      do i=1,mx
        ddyup1(i,j  ,k) = ( &
                     a*(f(i,j+1 ,k)-f(i,j   ,k)))
     end do
    end do
  end do
  !$omp end parallel

  !$omp master
  nstag1 = nstag1+1
  nflop = nflop+2
  !$omp end master
END FUNCTION

!***********************************************************************
FUNCTION ddydn (f)
  use params, g=>gy3, h=>gy2
  implicit none
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f, ddydn
!-----------------------------------------------------------------------

  if (my .lt. 3) then
    ddydn = 0.
    return
  end if
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)/dy
  a1st = 1./dy
  b = b/dy
  c = c/dy

  call mpi_send_y (f, g, 3, h, 2)

  !$omp parallel private(i,j,k)
  do k=izs,ize
    do i=1,mx
      ddydn(i,my-1,k) = ( &
                     c*(h(i,1   ,k)-f(i,my-4,k)) + &
                     b*(f(i,my  ,k)-f(i,my-3,k)) + &
                     a*(f(i,my-1,k)-f(i,my-2,k)) ) 
      ddydn(i,my  ,k) = ( &
                     c*(h(i,2   ,k)-f(i,my-3,k)) + &
                     b*(h(i,1   ,k)-f(i,my-2,k)) + &
                     a*(f(i,my  ,k)-f(i,my-1,k)) )
      ddydn(i,1  ,k) = ( &
                     c*(f(i,3   ,k)-g(i,1   ,k)) + &
                     b*(f(i,2   ,k)-g(i,2   ,k)) + &
                     a*(f(i,1   ,k)-g(i,3   ,k)) )
      ddydn(i,2   ,k) = ( &
                     c*(f(i,4   ,k)-g(i,2   ,k)) + &
                     b*(f(i,3   ,k)-g(i,3   ,k)) + &
                     a*(f(i,2   ,k)-f(i,1   ,k)) )
      ddydn(i,3   ,k) = ( &
                     c*(f(i,5   ,k)-g(i,3   ,k)) + &
                     b*(f(i,4   ,k)-f(i,1   ,k)) + &
                     a*(f(i,3   ,k)-f(i,2   ,k)) )
    end do
    do j=4,my-2
     do i=1,mx
      ddydn(i,j ,k) = ( &
                     c*(f(i,j+2 ,k)-f(i,j-3 ,k)) + &
                     b*(f(i,j+1 ,k)-f(i,j-2 ,k)) + &
                     a*(f(i,j   ,k)-f(i,j-1 ,k)) )
     end do
    end do
  end do
  !$omp end parallel

  !$omp master
  nstag = nstag+1
  nflop = nflop+8
  !$omp end master
END FUNCTION

!***********************************************************************
FUNCTION ddydn1 (f)
  use params, g=>gy1
  implicit none
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f, ddydn1
!-----------------------------------------------------------------------

  if (my .lt. 3) then
    ddydn1 = 0.
    return
  end if
  a = 1./dy

  call mpi_send_y (f, g, 1, g, 0)

  !$omp parallel private(i,j,k)
  do k=izs,ize
    do i=1,mx
      ddydn1(i,1  ,k) = ( &
                     a*(f(i,1   ,k)-g(i,1   ,k)))
    end do
    do j=2,my
     do i=1,mx
      ddydn1(i,j ,k) = ( &
                     a*(f(i,j   ,k)-f(i,j-1 ,k)))
     end do
    end do
  end do
  !$omp end parallel

  !$omp master
  nstag1 = nstag1+1
  nflop = nflop+2
  !$omp end master
END FUNCTION
