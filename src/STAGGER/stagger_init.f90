! $Id: stagger_init.f90,v 1.2 2005/06/04 15:23:23 aake Exp $

SUBROUTINE init_stagger
  call stagger_test_sub
END SUBROUTINE

SUBROUTINE init_derivs
END SUBROUTINE

!-------------------------------------------------------------------------------
 SUBROUTINE stagger_test_sub
!
!  Check the correctness of the stagger operators
!
  use params, only:mx,my,mz,dx,dy,dz,pi
  implicit none
  real, allocatable, dimension(:,:,:):: ux,uy,uz,px,py,pz,fx,fy,fz
  integer ix, iy, iz, ox, oy, oz
  real epsx, epsy, epsz, eps, kx, ky, kz
  logical ok, all

  character(len=70):: id='$Id: stagger_init.f90,v 1.2 2005/06/04 15:23:23 aake Exp $'
  print *,'------------------------------------------------------------------------'
  if (id .ne. ' ') print *,id; id=' '

  ox=mx
  oy=my
  oz=mz

  mx=40
  my=45
  mz=50

  dx=1./mx
  dy=1./my
  dz=1./mz

  kx = 2.*pi/(mx*dx)
  ky = 2.*pi/(my*dy)
  kz = 2.*pi/(mz*dz)

  allocate (ux(mx,my,mz), uy(mx,my,mz), uz(mx,my,mz))
  allocate (px(mx,my,mz), py(mx,my,mz), pz(mx,my,mz))
  allocate (fx(mx,my,mz), fy(mx,my,mz), fz(mx,my,mz))

  do iz=1,mz
  do iy=1,my
  do ix=1,mx
    ux(ix,iy,iz)=sin(2.*pi*real(ix)/real(mx))
    uy(ix,iy,iz)=sin(2.*pi*real(iy)/real(my))
    uz(ix,iy,iz)=sin(2.*pi*real(iz)/real(mz))
    px(ix,iy,iz)=sin(2.*pi*real(ix+0.5)/real(mx))
    py(ix,iy,iz)=sin(2.*pi*real(iy+0.5)/real(my))
    pz(ix,iy,iz)=sin(2.*pi*real(iz+0.5)/real(mz))
  end do
  end do
  end do
!$omp parallel
  call xup_set (ux, fx)
!$omp end parallel
  epsx=maxval(abs(fx-px))
!$omp parallel
  call yup_set (uy, fy)
!$omp end parallel
  epsy=maxval(abs(fy-py)); if (my.eq.1) epsy=0.
!$omp parallel
  call zup_set (uz, fz)
!$omp end parallel
  epsz=maxval(abs(fz-pz)); if (mz.eq.1) epsz=0.
  eps = max(4.*kx**6*dx**6/(6.*5.*4.*3.*2.),1e-6)
  ok = ok .and. (epsx < eps) .and. (epsy < eps) .and. (epsz < eps)
  all = ok
  print *,'  up:', epsx, epsy, epsz, eps, ok

  do iz=1,mz
  do iy=1,my
  do ix=1,mx
    ux(ix,iy,iz)=sin(2.*pi*real(ix)/real(mx))
    uy(ix,iy,iz)=sin(2.*pi*real(iy)/real(my))
    uz(ix,iy,iz)=sin(2.*pi*real(iz)/real(mz))
    px(ix,iy,iz)=cos(2.*pi*real(ix-0.5)/real(mx))*kx
    py(ix,iy,iz)=cos(2.*pi*real(iy-0.5)/real(my))*ky
    pz(ix,iy,iz)=cos(2.*pi*real(iz-0.5)/real(mz))*kz
  end do
  end do
  end do
!$omp parallel
  call ddxdn_set (ux, fx)
!$omp end parallel
  epsx=maxval(abs(fx-px))
!$omp parallel
  call ddydn_set (uy, fy)
!$omp end parallel
  epsy=maxval(abs(fy-py)); if (my.eq.1) epsy=0.
!$omp parallel
  call ddzdn_set (uz, fz)
!$omp end parallel
  epsz=maxval(abs(fz-pz)); if (mz.eq.1) epsz=0.
  eps = max(4.*kx**7*dx**6/(7.*6.*5.*4.*3.*2.),1e-6/dx)
  ok = (epsx < eps) .and. (epsy < eps) .and. (epsz < eps)
  all = all .and. ok
  print *,' ddn:', epsx, epsy, epsz, eps, ok

  do iz=1,mz
  do iy=1,my
  do ix=1,mx
    ux(ix,iy,iz)=sin(2.*pi*real(ix)/real(mx))
    uy(ix,iy,iz)=sin(2.*pi*real(iy)/real(my))
    uz(ix,iy,iz)=sin(2.*pi*real(iz)/real(mz))
    px(ix,iy,iz)=cos(2.*pi*real(ix+0.5)/real(mx))*kx
    py(ix,iy,iz)=cos(2.*pi*real(iy+0.5)/real(my))*ky
    pz(ix,iy,iz)=cos(2.*pi*real(iz+0.5)/real(mz))*kz
  end do
  end do
  end do
!$omp parallel
  call ddxup_set (ux, fx)
!$omp end parallel
  epsx=maxval(abs(fx-px))
!$omp parallel
  call ddyup_set (uy, fy)
!$omp end parallel
  epsy=maxval(abs(fy-py)); if (my.eq.1) epsy=0.
!$omp parallel
  call ddzup_set (uz, fz)
!$omp end parallel
  epsz=maxval(abs(fz-pz)); if (mz.eq.1) epsz=0.
  eps = max(4.*kx**7*dx**6/(7.*6.*5.*4.*3.*2.),1e-6/dx)
  ok = (epsx < eps) .and. (epsy < eps) .and. (epsz < eps)
  all = all .and. ok
  print *,' dup:', epsx, epsy, epsz, eps, ok

  do iz=1,mz
  do iy=1,my
  do ix=1,mx
    ux(ix,iy,iz)=sin(2.*pi*real(ix)/real(mx))
    uy(ix,iy,iz)=sin(2.*pi*real(iy)/real(my))
    uz(ix,iy,iz)=sin(2.*pi*real(iz)/real(mz))
    px(ix,iy,iz)=sin(2.*pi*real(ix-0.5)/real(mx))
    py(ix,iy,iz)=sin(2.*pi*real(iy-0.5)/real(my))
    pz(ix,iy,iz)=sin(2.*pi*real(iz-0.5)/real(mz))
  end do
  end do
  end do
!$omp parallel
  call xdn1_set (ux, fx)
!$omp end parallel
  epsx=maxval(abs(fx-px))
!$omp parallel
  call ydn1_set (uy, fy)
!$omp end parallel
  epsy=maxval(abs(fy-py)); if (my.eq.1) epsy=0.
!$omp parallel
  call zdn1_set (uz, fz)
!$omp end parallel
  epsz=maxval(abs(fz-pz)); if (mz.eq.1) epsz=0.
  eps = max(0.5*kx**2*dx**2/2.,1e-6)
  ok = (epsx < eps) .and. (epsy < eps) .and. (epsz < eps)
  all = all .and. ok
  print *,' dn1:', epsx, epsy, epsz, eps, ok

  do iz=1,mz
  do iy=1,my
  do ix=1,mx
    ux(ix,iy,iz)=sin(2.*pi*real(ix)/real(mx))
    uy(ix,iy,iz)=sin(2.*pi*real(iy)/real(my))
    uz(ix,iy,iz)=sin(2.*pi*real(iz)/real(mz))
    px(ix,iy,iz)=cos(2.*pi*real(ix-0.5)/real(mx))*kx
    py(ix,iy,iz)=cos(2.*pi*real(iy-0.5)/real(my))*ky
    pz(ix,iy,iz)=cos(2.*pi*real(iz-0.5)/real(mz))*kz
  end do
  end do
  end do
!$omp parallel
  call ddxdn1_set (ux, fx)
!$omp end parallel
  epsx=maxval(abs(fx-px))
!$omp parallel
  call ddydn1_set (uy, fy)
!$omp end parallel
  epsy=maxval(abs(fy-py)); if (my.eq.1) epsy=0.
!$omp parallel
  call ddzdn1_set (uz, fz)
!$omp end parallel
  epsz=maxval(abs(fz-pz)); if (mz.eq.1) epsz=0.
  eps = max(0.5*kx**3*dx**2/(3.*2.*1.),1e-6/dx)
  ok = (epsx < eps) .and. (epsy < eps) .and. (epsz < eps)
  all = all .and. ok
  print *,'ddn1:', epsx, epsy, epsz, eps, ok

  do iz=1,mz
  do iy=1,my
  do ix=1,mx
    ux(ix,iy,iz)=sin(2.*pi*real(ix)/real(mx))
    uy(ix,iy,iz)=sin(2.*pi*real(iy)/real(my))
    uz(ix,iy,iz)=sin(2.*pi*real(iz)/real(mz))
    px(ix,iy,iz)=cos(2.*pi*real(ix+0.5)/real(mx))*kx
    py(ix,iy,iz)=cos(2.*pi*real(iy+0.5)/real(my))*ky
    pz(ix,iy,iz)=cos(2.*pi*real(iz+0.5)/real(mz))*kz
  end do
  end do
  end do
!$omp parallel
  call ddxup1_set (ux, fx)
!$omp end parallel
  epsx=maxval(abs(fx-px))
!$omp parallel
  call ddyup1_set (uy, fy)
!$omp end parallel
  epsy=maxval(abs(fy-py)); if (my.eq.1) epsy=0.
!$omp parallel
  call ddzup1_set (uz, fz)
!$omp end parallel
  epsz=maxval(abs(fz-pz)); if (mz.eq.1) epsz=0.
  eps = max(0.5*kx**3*dx**2/(3.*2.*1.),1e-6/dx)
  ok = (epsx < eps) .and. (epsy < eps) .and. (epsz < eps)
  all = all .and. ok
  print *,'dup1:', epsx, epsy, epsz, eps, ok

  if (all) then
    print *,'the stagger routines appear to be working correctly'
  else
    print *,'THERE APPEARS TO BE A PROBLEM WITH THE STAGGER ROUTINES'
  end if
  print *,'------------------------------------------------------------------------'

  deallocate (ux,uy,uz,px,py,pz,fx,fy,fz)

  mx = ox
  my = oy
  mz = oz

END SUBROUTINE
