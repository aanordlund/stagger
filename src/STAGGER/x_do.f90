!***********************************************************************
FUNCTION xup (f)
!
!  f is centered on (i-.5,j,k), fifth order stagger
!
  use params
  implicit none
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f, xup
!hpf$ distribute(*,*,block):: f, xup
!-----------------------------------------------------------------------
!
  character(len=mid):: id = "$Id: x_do.f90,v 1.10 2007/04/10 15:32:12 aake Exp $" 
  call print_id (id)
!
  if (mx.le.5) then
    xup = f
    return
  end if
  c = 3./256.
  b = -25./256.
  a = .5-b-c
  a1st = 0.5
!
!2omp barrier
!$omp parallel private(i,j,k)
  do k=izs,ize
   do j=1,my
    xup(mx-2,j,k) = ( &
                   a*(f(mx-2,j,k)+f(mx-1,j,k)) &
                 + b*(f(mx-3,j,k)+f(mx  ,j,k)) &
                 + c*(f(mx-4,j,k)+f(1   ,j,k)))
    xup(mx-1,j,k) = ( &
                   a*(f(mx-1,j,k)+f(mx  ,j,k)) &
                 + b*(f(mx-2,j,k)+f(1   ,j,k)) &
                 + c*(f(mx-3,j,k)+f(2   ,j,k)))
    xup(mx  ,j,k) = ( &
                   a*(f(mx  ,j,k)+f(1   ,j,k)) &
                 + b*(f(mx-1,j,k)+f(2   ,j,k)) &
                 + c*(f(mx-2,j,k)+f(3   ,j,k)))
    xup(1   ,j,k) = ( &
                   a*(f(1   ,j,k)+f(2   ,j,k)) &
                 + b*(f(mx  ,j,k)+f(3   ,j,k)) &
                 + c*(f(mx-1,j,k)+f(4   ,j,k)))
    xup(2   ,j,k) = ( &
                   a*(f(2   ,j,k)+f(3   ,j,k)) &
                 + b*(f(1   ,j,k)+f(4   ,j,k)) &
                 + c*(f(mx  ,j,k)+f(5   ,j,k)))
   end do
   do j=1,my
    do i=3,mx-3
      xup(i  ,j,k) = ( &
                    a*(f(i  ,j,k)+f(i+1 ,j,k)) &
                  + b*(f(i-1,j,k)+f(i+2 ,j,k)) &
                  + c*(f(i-2,j,k)+f(i+3 ,j,k)))
    end do
   end do
  end do
!$omp end parallel
!2omp barrier
!
  if (omp_master) then
      nstag = nstag+1
      nflop = nflop+8
  end if
END FUNCTION

!***********************************************************************
FUNCTION xup1 (f)
!
!  f is centered on (i-.5,j,k), fifth order stagger
!
  use params
  implicit none
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f, xup1
!hpf$ distribute(*,*,block):: f, xup1
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    xup1 = f
    return
  end if
!
  a = .5
!
!2omp barrier
!$omp parallel private(i,j,k)
  do k=izs,ize
   do j=1,my
    xup1(mx  ,j,k) = ( &
                   a*(f(mx  ,j,k)+f(1   ,j,k)))
   end do
   do j=1,my
    do i=1,mx-1
      xup1(i  ,j,k) = ( &
                    a*(f(i  ,j,k)+f(i+1 ,j,k)))
    end do
   end do
  end do
!$omp end parallel
!2omp barrier
!
  if (omp_master) then
    nstag1 = nstag1+1
    nflop = nflop+2
  end if
END FUNCTION

!***********************************************************************
FUNCTION xdn (f)
!
!  f is centered on (i,j,k), fifth order stagger
!
  use params
  implicit none
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f, xdn
!hpf$ distribute(*,*,block):: f, xdn
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    xdn = f
    return
  end if
!
  c = 3./256.
  b = -25./256.
  a = .5-b-c
  a1st = 0.5
!
!2omp barrier
!$omp parallel private(i,j,k)
  do k=izs,ize
   do j=1,my
    xdn(mx-1,j,k) = ( &
                   a*(f(mx-2,j,k)+f(mx-1,j,k)) &
                 + b*(f(mx-3,j,k)+f(mx  ,j,k)) &
                 + c*(f(mx-4,j,k)+f(1   ,j,k)))
    xdn(mx  ,j,k) = ( &
                   a*(f(mx-1,j,k)+f(mx  ,j,k)) &
                 + b*(f(mx-2,j,k)+f(1   ,j,k)) &
                 + c*(f(mx-3,j,k)+f(2   ,j,k)))
    xdn(1  ,j,k) = ( &
                   a*(f(mx  ,j,k)+f(1   ,j,k)) &
                 + b*(f(mx-1,j,k)+f(2   ,j,k)) &
                 + c*(f(mx-2,j,k)+f(3   ,j,k)))
    xdn(2  ,j,k) = ( &
                   a*(f(1   ,j,k)+f(2   ,j,k)) &
                 + b*(f(mx  ,j,k)+f(3   ,j,k)) &
                 + c*(f(mx-1,j,k)+f(4   ,j,k)))
    xdn(3  ,j,k) = ( &
                   a*(f(2   ,j,k)+f(3   ,j,k)) &
                 + b*(f(1   ,j,k)+f(4   ,j,k)) &
                 + c*(f(mx  ,j,k)+f(5   ,j,k)))
   end do
   do j=1,my
    do i=4,mx-2
      xdn(i,j,k) = ( &
                   a*(f(i   ,j,k)+f(i-1 ,j,k)) &
                 + b*(f(i+1 ,j,k)+f(i-2 ,j,k)) &
                 + c*(f(i+2 ,j,k)+f(i-3 ,j,k)))
    end do
   end do
  end do
!$omp end parallel
!2omp barrier
!
!
  if (omp_master) then
    nstag = nstag+1
    nflop = nflop+8
  end if
END FUNCTION

!***********************************************************************
FUNCTION xdn1 (f)
!
!  f is centered on (i,j,k), fifth order stagger
!
  use params
  implicit none
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f, xdn1
!hpf$ distribute(*,*,block):: f, xdn1
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    xdn1 = f
    return
  end if
!
  a = 0.5
!
!2omp barrier
!$omp parallel private(i,j,k)
  do k=izs,ize
   do j=1,my
    xdn1(1  ,j,k) = ( &
                   a*(f(mx  ,j,k)+f(1   ,j,k)))
   end do
   do j=1,my
    do i=2,mx
      xdn1(i,j,k) = ( &
                   a*(f(i   ,j,k)+f(i-1 ,j,k)))
    end do
   end do
  end do
!$omp end parallel
!2omp barrier
!
  if (omp_master) then
    nstag1 = nstag1+1
    nflop = nflop+2
  end if
END FUNCTION
!***********************************************************************
FUNCTION xup32 (f,j)
!
!  f is centered on (i-.5,j,k), fifth order stagger
!
  use params
  implicit none
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f
  real, dimension(mx,mz):: xup32
!hpf$ distribute(*,*,block):: f, xup32
!-----------------------------------------------------------------------
!
  character(len=mid):: id = "$Id: x_do.f90,v 1.10 2007/04/10 15:32:12 aake Exp $" 
  call print_id (id)
!
  if (mx.le.5) then
    xup32 = f(:,j,:)
    return
  end if
  c = 3./256.
  b = -25./256.
  a = .5-b-c
  a1st = 0.5
!
!2omp barrier
!$omp parallel private(i,j,k)
  do k=izs,ize
    xup32(mx-2,k) = ( &
                   a*(f(mx-2,j,k)+f(mx-1,j,k)) &
                 + b*(f(mx-3,j,k)+f(mx  ,j,k)) &
                 + c*(f(mx-4,j,k)+f(1   ,j,k)))
    xup32(mx-1,k) = ( &
                   a*(f(mx-1,j,k)+f(mx  ,j,k)) &
                 + b*(f(mx-2,j,k)+f(1   ,j,k)) &
                 + c*(f(mx-3,j,k)+f(2   ,j,k)))
    xup32(mx  ,k) = ( &
                   a*(f(mx  ,j,k)+f(1   ,j,k)) &
                 + b*(f(mx-1,j,k)+f(2   ,j,k)) &
                 + c*(f(mx-2,j,k)+f(3   ,j,k)))
    xup32(1   ,k) = ( &
                   a*(f(1   ,j,k)+f(2   ,j,k)) &
                 + b*(f(mx  ,j,k)+f(3   ,j,k)) &
                 + c*(f(mx-1,j,k)+f(4   ,j,k)))
    xup32(2   ,k) = ( &
                   a*(f(2   ,j,k)+f(3   ,j,k)) &
                 + b*(f(1   ,j,k)+f(4   ,j,k)) &
                 + c*(f(mx  ,j,k)+f(5   ,j,k)))
    do i=3,mx-3
      xup32(i  ,k) = ( &
                    a*(f(i  ,j,k)+f(i+1 ,j,k)) &
                  + b*(f(i-1,j,k)+f(i+2 ,j,k)) &
                  + c*(f(i-2,j,k)+f(i+3 ,j,k)))
    end do
  end do
!$omp end parallel
!2omp barrier
!
  if (omp_master) then
      nstag = nstag+1
      nflop = nflop+8
  end if
END FUNCTION

!***********************************************************************
FUNCTION xup132 (f,j)
!
!  f is centered on (i-.5,j,k), fifth order stagger
!
  use params
  implicit none
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f
  real, dimension(mx,mz):: xup132
!hpf$ distribute(*,*,block):: f, xup132
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    xup132 = f(:,j,:)
    return
  end if
!
  a = .5
!
!2omp barrier
!$omp parallel private(i,j,k)
  do k=izs,ize
    xup132(mx  ,k) = ( &
                   a*(f(mx  ,j,k)+f(1   ,j,k)))
    do i=1,mx-1
      xup132(i  ,k) = ( &
                    a*(f(i  ,j,k)+f(i+1 ,j,k)))
    end do
  end do
!$omp end parallel
!2omp barrier
!
  if (omp_master) then
    nstag1 = nstag1+1
    nflop = nflop+2
  end if
END FUNCTION

!***********************************************************************
FUNCTION xdn32 (f,j)
!
!  f is centered on (i,j,k), fifth order stagger
!
  use params
  implicit none
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f
  real, dimension(mx,mz):: xdn32
!hpf$ distribute(*,*,block):: f, xdn32
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    xdn32 = f(:,j,:)
    return
  end if
!
  c = 3./256.
  b = -25./256.
  a = .5-b-c
  a1st = 0.5
!
!2omp barrier
!$omp parallel private(i,j,k)
  do k=izs,ize
    xdn32(mx-1,k) = ( &
                   a*(f(mx-2,j,k)+f(mx-1,j,k)) &
                 + b*(f(mx-3,j,k)+f(mx  ,j,k)) &
                 + c*(f(mx-4,j,k)+f(1   ,j,k)))
    xdn32(mx  ,k) = ( &
                   a*(f(mx-1,j,k)+f(mx  ,j,k)) &
                 + b*(f(mx-2,j,k)+f(1   ,j,k)) &
                 + c*(f(mx-3,j,k)+f(2   ,j,k)))
    xdn32(1  ,k) = ( &
                   a*(f(mx  ,j,k)+f(1   ,j,k)) &
                 + b*(f(mx-1,j,k)+f(2   ,j,k)) &
                 + c*(f(mx-2,j,k)+f(3   ,j,k)))
    xdn32(2  ,k) = ( &
                   a*(f(1   ,j,k)+f(2   ,j,k)) &
                 + b*(f(mx  ,j,k)+f(3   ,j,k)) &
                 + c*(f(mx-1,j,k)+f(4   ,j,k)))
    xdn32(3  ,k) = ( &
                   a*(f(2   ,j,k)+f(3   ,j,k)) &
                 + b*(f(1   ,j,k)+f(4   ,j,k)) &
                 + c*(f(mx  ,j,k)+f(5   ,j,k)))
    do i=4,mx-2
      xdn32(i,k) = ( &
                   a*(f(i   ,j,k)+f(i-1 ,j,k)) &
                 + b*(f(i+1 ,j,k)+f(i-2 ,j,k)) &
                 + c*(f(i+2 ,j,k)+f(i-3 ,j,k)))
    end do
  end do
!$omp end parallel
!2omp barrier
!
!
  if (omp_master) then
    nstag = nstag+1
    nflop = nflop+8
  end if
END FUNCTION

!***********************************************************************
FUNCTION xdn132 (f,j)
!
!  f is centered on (i,j,k), fifth order stagger
!
  use params
  implicit none
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f
  real, dimension(mx,mz):: xdn132
!hpf$ distribute(*,*,block):: f, xdn132
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    xdn132 = f(:,j,:)
    return
  end if
!
  a = 0.5
!
!2omp barrier
!$omp parallel private(i,j,k)
  do k=izs,ize
    xdn132(1  ,k) = ( &
                   a*(f(mx  ,j,k)+f(1   ,j,k)))
    do i=2,mx
      xdn132(i,k) = ( &
                   a*(f(i   ,j,k)+f(i-1 ,j,k)))
    end do
  end do
!$omp end parallel
!2omp barrier
!
  if (omp_master) then
    nstag1 = nstag1+1
    nflop = nflop+2
  end if
END FUNCTION
!***********************************************************************
FUNCTION xup22 (f)
!
!  f is centered on (i-.5,k), fifth order stagger
!
  use params
  implicit none
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,mz):: xup22, f
!hpf$ distribute(*,*,block):: f, xup22
!-----------------------------------------------------------------------
!
  character(len=mid):: id = "$Id: x_do.f90,v 1.10 2007/04/10 15:32:12 aake Exp $" 
  call print_id (id)
!
  if (mx.le.5) then
    xup22 = f
    return
  end if
  c = 3./256.
  b = -25./256.
  a = .5-b-c
  a1st = 0.5
!
!2omp barrier
!$omp parallel private(i,k)
  do k=izs,ize
    xup22(mx-2,k) = ( &
                   a*(f(mx-2,k)+f(mx-1,k)) &
                 + b*(f(mx-3,k)+f(mx  ,k)) &
                 + c*(f(mx-4,k)+f(1   ,k)))
    xup22(mx-1,k) = ( &
                   a*(f(mx-1,k)+f(mx  ,k)) &
                 + b*(f(mx-2,k)+f(1   ,k)) &
                 + c*(f(mx-3,k)+f(2   ,k)))
    xup22(mx  ,k) = ( &
                   a*(f(mx  ,k)+f(1   ,k)) &
                 + b*(f(mx-1,k)+f(2   ,k)) &
                 + c*(f(mx-2,k)+f(3   ,k)))
    xup22(1   ,k) = ( &
                   a*(f(1   ,k)+f(2   ,k)) &
                 + b*(f(mx  ,k)+f(3   ,k)) &
                 + c*(f(mx-1,k)+f(4   ,k)))
    xup22(2   ,k) = ( &
                   a*(f(2   ,k)+f(3   ,k)) &
                 + b*(f(1   ,k)+f(4   ,k)) &
                 + c*(f(mx  ,k)+f(5   ,k)))
    do i=3,mx-3
      xup22(i  ,k) = ( &
                    a*(f(i  ,k)+f(i+1 ,k)) &
                  + b*(f(i-1,k)+f(i+2 ,k)) &
                  + c*(f(i-2,k)+f(i+3 ,k)))
    end do
  end do
!$omp end parallel
!2omp barrier
!
  if (omp_master) then
      nstag = nstag+1
      nflop = nflop+8
  end if
END FUNCTION

!***********************************************************************
FUNCTION xup122 (f)
!
!  f is centered on (i-.5,k), fifth order stagger
!
  use params
  implicit none
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,mz):: xup122, f
!hpf$ distribute(*,*,block):: f, xup122
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    xup122 = f
    return
  end if
!
  a = .5
!
!2omp barrier
!$omp parallel private(i,k)
  do k=izs,ize
    xup122(mx  ,k) = ( &
                   a*(f(mx  ,k)+f(1   ,k)))
    do i=1,mx-1
      xup122(i  ,k) = ( &
                    a*(f(i  ,k)+f(i+1 ,k)))
    end do
  end do
!$omp end parallel
!2omp barrier
!
  if (omp_master) then
    nstag1 = nstag1+1
    nflop = nflop+2
  end if
END FUNCTION

!***********************************************************************
FUNCTION xdn22 (f)
!
!  f is centered on (i,k), fifth order stagger
!
  use params
  implicit none
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,mz):: xdn22, f
!hpf$ distribute(*,*,block):: f, xdn22
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    xdn22 = f
    return
  end if
!
  c = 3./256.
  b = -25./256.
  a = .5-b-c
  a1st = 0.5
!
!2omp barrier
!$omp parallel private(i,k)
  do k=izs,ize
    xdn22(mx-1,k) = ( &
                   a*(f(mx-2,k)+f(mx-1,k)) &
                 + b*(f(mx-3,k)+f(mx  ,k)) &
                 + c*(f(mx-4,k)+f(1   ,k)))
    xdn22(mx  ,k) = ( &
                   a*(f(mx-1,k)+f(mx  ,k)) &
                 + b*(f(mx-2,k)+f(1   ,k)) &
                 + c*(f(mx-3,k)+f(2   ,k)))
    xdn22(1  ,k) = ( &
                   a*(f(mx  ,k)+f(1   ,k)) &
                 + b*(f(mx-1,k)+f(2   ,k)) &
                 + c*(f(mx-2,k)+f(3   ,k)))
    xdn22(2  ,k) = ( &
                   a*(f(1   ,k)+f(2   ,k)) &
                 + b*(f(mx  ,k)+f(3   ,k)) &
                 + c*(f(mx-1,k)+f(4   ,k)))
    xdn22(3  ,k) = ( &
                   a*(f(2   ,k)+f(3   ,k)) &
                 + b*(f(1   ,k)+f(4   ,k)) &
                 + c*(f(mx  ,k)+f(5   ,k)))
    do i=4,mx-2
      xdn22(i,k) = ( &
                   a*(f(i   ,k)+f(i-1 ,k)) &
                 + b*(f(i+1 ,k)+f(i-2 ,k)) &
                 + c*(f(i+2 ,k)+f(i-3 ,k)))
    end do
  end do
!$omp end parallel
!2omp barrier
!
!
  if (omp_master) then
    nstag = nstag+1
    nflop = nflop+8
  end if
END FUNCTION

!***********************************************************************
FUNCTION xdn122 (f)
!
!  f is centered on (i,k), fifth order stagger
!
  use params
  implicit none
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,mz):: xdn122, f
!hpf$ distribute(*,*,block):: f, xdn122
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    xdn122 = f
    return
  end if
!
  a = 0.5
!
!2omp barrier
!$omp parallel private(i,k)
  do k=izs,ize
    xdn122(1  ,k) = ( &
                   a*(f(mx  ,k)+f(1   ,k)))
    do i=2,mx
      xdn122(i,k) = ( &
                   a*(f(i   ,k)+f(i-1 ,k)))
    end do
  end do
!$omp end parallel
!2omp barrier
!
  if (omp_master) then
    nstag1 = nstag1+1
    nflop = nflop+2
  end if
END FUNCTION
