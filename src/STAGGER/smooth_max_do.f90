! $Id: smooth_max_do.f90,v 1.33 2013/11/17 17:19:10 aake Exp $
!***********************************************************************
SUBROUTINE crl1_set (Ux, Uy, Uz, out)
!
!  Add a lower order, centered approximation of abs(w) to the out array
!
  USE params
  implicit none
  integer iz
  real, dimension(mx,my,mz):: Ux, Uy, Uz, out
  real, allocatable, dimension(:,:,:):: w2, s
!
  allocate (w2(mx,my,mz), s(mx,my,mz))
  call dif1xy_set(Ux,Uy,w2); call xup1_set(w2,s); call yup1_set(s,w2)
  call dif1yz_set(Uy,Uz,w2); call yup1_set(w2,s); call zup1_add(s,w2)
  call dif1zx_set(Uz,Ux,w2); call zup1_set(w2,s); call xup1_add(s,w2)
  !$omp parallel do private(iz)
  do iz=1,mz
    out(:,:,iz) = sqrt(w2(:,:,iz))
  end do
  deallocate (w2, s)
END
!***********************************************************************
SUBROUTINE crl1_add (Ux, Uy, Uz, out)
!
!  Add a lower order, centered approximation of abs(w) to the out array
!
  USE params
  implicit none
  integer iz
  real, dimension(mx,my,mz):: Ux, Uy, Uz, out
  real, allocatable, dimension(:,:,:):: w2, s
!
  allocate (w2(mx,my,mz), s(mx,my,mz))
  call dif1xy_set(Ux,Uy,w2); call xup1_set(w2,s); call yup1_set(s,w2)
  call dif1yz_set(Uy,Uz,w2); call yup1_set(w2,s); call zup1_add(s,w2)
  call dif1zx_set(Uz,Ux,w2); call zup1_set(w2,s); call xup1_add(s,w2)
  !$omp parallel do private(iz)
  do iz=1,mz
    out(:,:,iz) = out(:,:,iz) + sqrt(w2(:,:,iz))
  end do
  deallocate (w2, s)
END

!***********************************************************************
!  NOTE: Because smooth3 and max3 need an internal scratch array they
!  cannot easily be OpenMP parallel in an external parallel region.
!  It is therefore simplest to maintain them as loop parallel only,
!  and to only use smooth3max3 for external parallel region cases.
!***********************************************************************
FUNCTION smooth3t (f)
!
!  Three point smooth
!
  USE params
  implicit none
  integer i,j,k,km1,kp1
  real, dimension(mx,my,mz):: f, smooth3t
  real, dimension(0:mx+1):: fx
  real, dimension(0:my+1):: fy
  real c3
  logical omp_in_parallel
!-----------------------------------------------------------------------
  character(len=mid):: id = "$Id: smooth_max_do.f90,v 1.33 2013/11/17 17:19:10 aake Exp $" 
  call print_id (id)
!
  if (omp_in_parallel()) print *,'WARNING: smooth3 called from within parallel region!'
!$omp parallel private(i,j,k,fx,fy,km1,kp1)
  do k=izs,ize
   do j=1,my
    do i=1,mx
      fx(i)=f(i,j,k)
    end do
    fx(0)=fx(mx)
    fx(mx+1)=fx(1)
    do i=1,mx
      scratch(i,j,k)=0.25*(fx(i-1)+fx(i)+fx(i)+fx(i+1))
    end do
   end do
   do i=1,mx
    do j=1,my
      fy(j)=scratch(i,j,k)
    end do
    fy(0)=fy(my)
    fy(my+1)=fy(1)
    do j=1,my
      scratch(i,j,k)=0.25*(fy(j-1)+fy(j)+fy(j)+fy(j+1))
    end do
   end do
  end do
  call barrier_omp('smooth3t')
  do k=izs,ize
   km1=mod(k+mz-2,mz)+1
   kp1=mod(k+mz  ,mz)+1
   do j=1,my
    do i=1,mx
      smooth3t(i,j,k)=0.25*(scratch(i,j,km1)+scratch(i,j,k)+scratch(i,j,k)+scratch(i,j,kp1))
    end do
   end do
  end do
  call barrier_omp('smooth3t')
!$omp end parallel
END FUNCTION

!***********************************************************************
FUNCTION smooth3th (f)
!
!  Three point smooth
!
  USE params
  implicit none
  integer i,j,k,km1,kp1
  real, dimension(mx,my,mz):: f, smooth3th
  real, dimension(0:mx+1):: fx
  real, dimension(0:my+1):: fy
  real c3
  logical omp_in_parallel
!-----------------------------------------------------------------------
  character(len=mid):: id = "$Id: smooth_max_do.f90,v 1.33 2013/11/17 17:19:10 aake Exp $" 
  call print_id (id)
!
  if (omp_in_parallel()) print *,'WARNING: smooth3 called from within parallel region!'
!$omp parallel private(i,j,k,fx,fy,km1,kp1)
  do k=izs,ize
   do j=1,my
    do i=1,mx
      fx(i)=f(i,j,k)
    end do
    fx(0)=fx(mx)
    fx(mx+1)=fx(1)
    do i=1,mx
      scratch(i,j,k)=0.25*(fx(i-1)+fx(i)+fx(i)+fx(i+1))
    end do
   end do
  end do
  call barrier_omp('smooth3th')
  do k=izs,ize
   km1=mod(k+mz-2,mz)+1
   kp1=mod(k+mz  ,mz)+1
   do j=1,my
    do i=1,mx
      smooth3th(i,j,k)=0.25*(scratch(i,j,km1)+scratch(i,j,k)+scratch(i,j,k)+scratch(i,j,kp1))
    end do
   end do
  end do
  call barrier_omp('smooth3th')
!$omp end parallel
END FUNCTION

FUNCTION smooth3 (f)
!
!  Three point smooth
!
  USE params
  implicit none
  integer i,j,k,km1,kp1
  real, dimension(mx,my,mz):: f, smooth3
  real, dimension(0:mx+1):: fx
  real, dimension(0:my+1):: fy
  real c3
  logical omp_in_parallel
!-----------------------------------------------------------------------
  character(len=mid):: id = "$Id: smooth_max_do.f90,v 1.33 2013/11/17 17:19:10 aake Exp $" 
  call print_id (id)
!
  c3=1./3.
  if (omp_in_parallel()) print *,'WARNING: smooth3 called from within parallel region!'
!$omp parallel private(i,j,k,fx,fy,km1,kp1)
  do k=izs,ize
   do j=1,my
    do i=1,mx
      fx(i)=f(i,j,k)
    end do
    fx(0)=fx(mx)
    fx(mx+1)=fx(1)
    do i=1,mx
      scratch(i,j,k)=c3*(fx(i-1)+fx(i)+fx(i+1))
    end do
   end do
   do i=1,mx
    do j=1,my
      fy(j)=scratch(i,j,k)
    end do
    fy(0)=fy(my)
    fy(my+1)=fy(1)
    do j=1,my
      scratch(i,j,k)=c3*(fy(j-1)+fy(j)+fy(j+1))
    end do
   end do
  end do
  call barrier_omp('smooth3')
  do k=izs,ize
   km1=mod(k+mz-2,mz)+1
   kp1=mod(k+mz  ,mz)+1
   do j=1,my
    do i=1,mx
      smooth3(i,j,k)=c3*(scratch(i,j,k)+scratch(i,j,km1)+scratch(i,j,kp1))
    end do
   end do
  end do
  call barrier_omp('smooth3')
!$omp end parallel
END FUNCTION

!***********************************************************************
FUNCTION max3 (f)
!
!  Three point max
!
  USE params
  implicit none
  integer i,j,k,km1,kp1
  real, dimension(mx,my,mz):: f, max3
  real, dimension(0:mx+1):: fx
  real, dimension(0:my+1):: fy
  logical omp_in_parallel
!-----------------------------------------------------------------------
!
  if (omp_in_parallel()) print *,'WARNING: max3 called from within parallel region!'
!$omp parallel private(i,j,k,fx,fy,km1,kp1)
  do k=izs,ize
   do j=1,my
    do i=1,mx
      fx(i)=f(i,j,k)
    end do
    fx(0)=fx(mx)
    fx(mx+1)=fx(1)
    do i=1,mx
      scratch(i,j,k)=amax1(fx(i-1),fx(i),fx(i+1))
    end do
   end do
   do i=1,mx
    do j=1,my
      fy(j)=scratch(i,j,k)
    end do
    fy(0)=fy(my)
    fy(my+1)=fy(1)
    do j=1,my
      scratch(i,j,k)=amax1(fy(j-1),fy(j),fy(j+1))
    end do
   end do
  end do
  call barrier_omp('max3')
  do k=izs,ize
    km1=mod(k+mz-2,mz)+1
    kp1=mod(k+mz  ,mz)+1
    do j=1,my
    do i=1,mx
      max3(i,j,k)=amax1(scratch(i,j,k),scratch(i,j,km1),scratch(i,j,kp1))
    end do
   end do
  end do
  call barrier_omp('max3')
!$omp end parallel
END FUNCTION

!***********************************************************************
FUNCTION max5 (f)
!
!  Three point max
!
  USE params
  implicit none
  integer i,j,k,km1,kp1,km2,kp2
  real, dimension(mx,my,mz), intent(in) :: f
  real, dimension(mx,my,mz):: max5
  real, dimension(-1:mx+2):: fx
  real, dimension(-1:my+2):: fy
  logical omp_in_parallel
!-----------------------------------------------------------------------
!
  if (omp_in_parallel()) print *,'WARNING: max5 called from within parallel region!'
!$omp parallel private(i,j,k,fx,fy,km1,kp1,km2,kp2)
  do k=izs,ize
   do j=1,my
    do i=1,mx
      fx(i)=f(i,j,k)
    end do
    fx(  -1)=fx(mx-1)
    fx(   0)=fx(mx  )
    fx(mx+1)=fx(   1)
    fx(mx+2)=fx(   2)
    do i=1,mx
      scratch(i,j,k)=amax1(fx(i-2),fx(i-1),fx(i),fx(i+1),fx(i+2))
    end do
   end do
   if (my.gt.1) then
     do i=1,mx
      do j=1,my
        fy(j)=scratch(i,j,k)
      end do
      fy(  -1)=fy(my-1)
      fy(   0)=fy(my  )
      fy(my+1)=fy(   1)
      fy(my+2)=fy(   2)
      do j=1,my
        scratch(i,j,k)=amax1(fy(j-2),fy(j-1),fy(j),fy(j+1),fy(j+2))
      end do
     end do
    end if
  end do
  call barrier_omp('max5')
  do k=izs,ize
    km2=mod(k+mz-3,mz)+1
    km1=mod(k+mz-2,mz)+1
    kp1=mod(k+mz  ,mz)+1
    kp2=mod(k+mz+1,mz)+1
    do j=1,my
    do i=1,mx
      max5(i,j,k)=amax1(scratch(i,j,km2),scratch(i,j,km1),scratch(i,j,k),scratch(i,j,kp1),scratch(i,j,kp2))
    end do
   end do
  end do
  call barrier_omp('max5')
!$omp end parallel
END FUNCTION

!***********************************************************************
SUBROUTINE smooth3max5_set (f, fz)
!
!  Three point max
!
  USE params
  implicit none
  real c3
  integer i,j,k,km1,kp1,km2,kp2
  real, dimension(mx,my,mz) :: f, fz
  real, dimension(-1:mx+2):: fx
  real, dimension(-1:my+2):: fy
  logical omp_in_parallel
!-----------------------------------------------------------------------
!
  if (.not.omp_in_parallel().and.omp_nthreads.gt.1) print *,'WARNING: smooth3max5 called from outside parallel region!'
  c3=1./3.
  do k=izs,ize
   do j=1,my
    do i=1,mx
      fx(i)=f(i,j,k)
    end do
    fx(  -1)=fx(mx-1)
    fx(   0)=fx(mx  )
    fx(mx+1)=fx(   1)
    fx(mx+2)=fx(   2)
    do i=1,mx
      fz(i,j,k)=amax1(fx(i-2),fx(i-1),fx(i),fx(i+1),fx(i+2))
    end do
   end do
   if (my.gt.1) then
     do i=1,mx
      do j=1,my
        fy(j)=fz(i,j,k)
      end do
      fy(  -1)=fy(my-1)
      fy(   0)=fy(my  )
      fy(my+1)=fy(   1)
      fy(my+2)=fy(   2)
      do j=1,my
        f(i,j,k)=amax1(fy(j-2),fy(j-1),fy(j),fy(j+1),fy(j+2))
      end do
     end do
    end if
  end do
  call barrier_omp('smmx')
  do k=izs,ize
    km2=mod(k+mz-3,mz)+1
    km1=mod(k+mz-2,mz)+1
    kp1=mod(k+mz  ,mz)+1
    kp2=mod(k+mz+1,mz)+1
    do j=1,my
      do i=1,mx
        fx(i)=amax1(f(i,j,km2),f(i,j,km1),f(i,j,k),f(i,j,kp1),f(i,j,kp2))
      end do
      fx(0)=fx(mx)
      fx(mx+1)=fx(1)
      do i=1,mx
        fz(i,j,k)=c3*(fx(i-1)+fx(i)+fx(i+1))
      end do
    end do
    do i=1,mx
      do j=1,my
        fy(j)=fz(i,j,k)
      end do
      fy(0)=fy(my)
      fy(my+1)=fy(1)
      do j=1,my
        fz(i,j,k)=c3*(fy(j-1)+fy(j)+fy(j+1))
      end do
    end do
  end do
  call barrier_omp('smmx')
  do k=izs,ize
    km1=mod(k+mz-2,mz)+1
    kp1=mod(k+mz  ,mz)+1
    do j=1,my
      do i=1,mx
        f(i,j,k)=c3*(fz(i,j,k)+fz(i,j,km1)+fz(i,j,kp1))
      end do
    end do
  end do
  call barrier_omp('smmx')
END SUBROUTINE

!***********************************************************************
SUBROUTINE smooth3max3_set (f, fz)
!
!  Three point max
!
  USE params
  implicit none
  real c3
  integer i,j,k,km1,kp1,km2,kp2
  real, dimension(mx,my,mz) :: f, fz
  real, dimension(0:mx+1):: fx
  real, dimension(0:my+1):: fy
  logical omp_in_parallel
!-----------------------------------------------------------------------
!
  if (.not.omp_in_parallel().and.omp_nthreads.gt.1) print *,'WARNING: smooth3max5 called from outside parallel region!'
  c3=1./3.
  do k=izs,ize
   do j=1,my
    do i=1,mx
      fx(i)=f(i,j,k)
    end do
    fx(   0)=fx(mx  )
    fx(mx+1)=fx(   1)
    do i=1,mx
      fz(i,j,k)=amax1(fx(i-1),fx(i),fx(i+1))
    end do
   end do
   if (my.gt.1) then
     do i=1,mx
      do j=1,my
        fy(j)=fz(i,j,k)
      end do
      fy(   0)=fy(my  )
      fy(my+1)=fy(   1)
      do j=1,my
        f(i,j,k)=amax1(fy(j-1),fy(j),fy(j+1))
      end do
     end do
    end if
  end do
  call barrier_omp('smmx')
  do k=izs,ize
    km1=mod(k+mz-2,mz)+1
    kp1=mod(k+mz  ,mz)+1
    do j=1,my
      do i=1,mx
        fx(i)=amax1(f(i,j,km1),f(i,j,k),f(i,j,kp1))
      end do
      fx(0)=fx(mx)
      fx(mx+1)=fx(1)
      do i=1,mx
        fz(i,j,k)=c3*(fx(i-1)+fx(i)+fx(i+1))
      end do
    end do
    do i=1,mx
      do j=1,my
        fy(j)=fz(i,j,k)
      end do
      fy(0)=fy(my)
      fy(my+1)=fy(1)
      do j=1,my
        fz(i,j,k)=c3*(fy(j-1)+fy(j)+fy(j+1))
      end do
    end do
  end do
  call barrier_omp('smmx')
  do k=izs,ize
    km1=mod(k+mz-2,mz)+1
    kp1=mod(k+mz  ,mz)+1
    do j=1,my
      do i=1,mx
        f(i,j,k)=c3*(fz(i,j,k)+fz(i,j,km1)+fz(i,j,kp1))
      end do
    end do
  end do
  call barrier_omp('smmx')
END SUBROUTINE

!***********************************************************************
SUBROUTINE smooth (f, dir)
!
!  Three point smooth
!
  USE params, gx=>gx1, hx=>hx1, gz=>gz1, hz=>hz1
  implicit none
  logical debug
  integer i, j, k, dir
  real, dimension(mx,my,mz):: f, smooth3
  real, dimension(0:mx+1):: fx
  real, dimension(0:my+1):: fy
  real, dimension(mx,0:mz+1):: fz
  real c
  logical omp_in_parallel
!-----------------------------------------------------------------------
  character(len=mid):: id = "smooth3: $Id: smooth_max_do.f90,v 1.33 2013/11/17 17:19:10 aake Exp $" 
  call print_id (id)
!
  c=0.25

  if (dir==1 .and. mx>2) then
    call mpi_send_x (f, gx, 1, hx, 1)
    do k=1,mz
     do j=1,my
      do i=1,mx
        fx(i)=f(i,j,k)
      end do
      fx(0)=gx(1,j,k)
      fx(mx+1)=hx(1,j,k)
      do i=1,mx
        f(i,j,k)=(1.-2*c)*fx(i)+c*(fx(i-1)+fx(i+1))
      end do
     end do
    end do

  else if (dir==2 .and. my>2) then
    call mpi_send_y (f, gy1, 1, hy1, 1)
    do k=1,mz
     do i=1,mx
      do j=1,my
        fy(j)=f(i,j,k)
      end do
      fy(0)=gy1(i,1,k)
      fy(my+1)=hy1(i,1,k)
      do j=merge(1,lb+1,lb.le.1),merge(my,ub-1,ub.ge.my)
        f(i,j,k)=(1.-2*c)*fy(j)+c*(fy(j-1)+fy(j+1))
      end do
     end do
    end do

  else if (mz>2)
    call mpi_send_z (f, gz, 1, hz, 1)
    do j=1,my
     do k=1,mz
     do i=1,mx
       fz(i,k)=f(i,j,k)
     end do
     end do
     do i=1,mx
       fz(i,   0)=gz(i,j,1)
       fz(i,mz+1)=hz(i,j,1)
     end do
     do k=1,mz
      do i=1,mx
        f(i,j,k)=(1.-2*c)*fz(i,k)+c*(fz(i,k-1)+fz(i,k+1))
      end do
     end do
    end do
  end if
END SUBROUTINE

