!***********************************************************************
FUNCTION ddzupi (f)
!
!  f is centered on (i,j,k-.5), fifth order stagger
!
  use params, g=>gz2, h=>hz3
  implicit none
  real a, b, c, a1st
  integer i, j, k
  real, intent(in) , dimension(mx,my,mz):: f
  real,              dimension(mx,my,mz):: ddzupi
!-----------------------------------------------------------------------
!
  character(len=mid):: id = "$Id: ddi_z_do_mpi.f90,v 1.9 2013/11/03 10:10:01 aake Exp $" 
  call print_id (id)

  if (mz .lt. 5) then
    !$omp parallel private(k)
    do k=izs,ize
      ddzupi(:,:,k) = f(:,:,k)
    end do
    !$omp end parallel
    return
  end if

  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)
  a1st = 1

  call mpi_send_z (f, g, 2, h, 3)
!
  !$omp parallel private(i,j,k)
  do k=max(izs,3),min(ize,mz-3)
    do j=1,my
      do i=1,mx
        ddzupi(i,j,k) = ( &
                     a*(-f(i,j,k   )+f(i,j,k+1 )) + &
                     b*(-f(i,j,k-1 )+f(i,j,k+2 )) + &
                     c*(-f(i,j,k-2 )+f(i,j,k+3 )))
     end do
    end do
  end do
  !$omp end parallel
  do j=1,my
    do i=1,mx
      ddzupi(i,j,mz-2) = ( &
                     a*(-f(i,j,mz-2)+f(i,j,mz-1)) + &
                     b*(-f(i,j,mz-3)+f(i,j,mz  )) + &
                     c*(-f(i,j,mz-4)+h(i,j,1   )))
      ddzupi(i,j,mz-1) = ( &
                     a*(-f(i,j,mz-1)+f(i,j,mz  )) + &
                     b*(-f(i,j,mz-2)+h(i,j,1   )) + &
                     c*(-f(i,j,mz-3)+h(i,j,2   )))
      ddzupi(i,j,mz  ) = ( &
                     a*(-f(i,j,mz  )+h(i,j,1   )) + &
                     b*(-f(i,j,mz-1)+h(i,j,2   )) + &
                     c*(-f(i,j,mz-2)+h(i,j,3   )))
      ddzupi(i,j,1   ) = ( &
                     a*(-f(i,j,1   )+f(i,j,2   )) + &
                     b*(-g(i,j,1   )+f(i,j,3   )) + &
                     c*(-g(i,j,2   )+f(i,j,4   )))
      ddzupi(i,j,2   ) = ( &
                     a*(-f(i,j,2   )+f(i,j,3   )) + &
                     b*(-f(i,j,1   )+f(i,j,4   )) + &
                     c*(-g(i,j,2   )+f(i,j,5   )))
    end do
  end do
!
  nstag = nstag+1
  nflop = nflop+8
END FUNCTION

!***********************************************************************
FUNCTION ddzupi1 (f)
!
!  f is centered on (i,j,k-.5), fifth order stagger
!
  use params, g=>gz1
  implicit none
  integer i, j, k
  real, intent(in) , dimension(mx,my,mz):: f
  real,              dimension(mx,my,mz):: ddzupi1
!-----------------------------------------------------------------------
!
  if (mz .lt. 5) then
    !$omp parallel private(k)
    do k=izs,ize
      ddzupi1(:,:,k) = f(:,:,k)
    end do
    !$omp end parallel
    return
  end if
!
  call mpi_send_z (f, g, 0, g, 1)
!
  !$omp parallel private(i,j,k)
  do k=izs,min(ize,mz-1)
    do j=1,my
      do i=1,mx
        ddzupi1(i,j,k  ) = ( &
                     (f(i,j,k+1)-f(i,j,k)))
      end do
    end do
  end do
  !$omp end parallel
  do j=1,my
    do i=1,mx
      ddzupi1(i,j,mz) = ( &
                     (+g(i,j,1)-f(i,j,mz)))
    end do
  end do
!
  nstag1 = nstag1+1
  nflop = nflop+2
END FUNCTION

!***********************************************************************
FUNCTION ddzdni (f)
!
!  f is centered on (i,j), fifth order stagger
!
  use params, g=>gz3, h=>hz2
  implicit none
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f, ddzdni
!-----------------------------------------------------------------------
!
  if (mz .lt. 5) then
    !$omp parallel private(k)
    do k=izs,ize
      ddzdni(:,:,k) = f(:,:,k)
    end do
    !$omp end parallel
    return
  end if

  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)
  a1st = 1.

  call mpi_send_z (f, g, 3, h, 2)
!
  !$omp parallel private(i,j,k)
  do k=max(izs,4),min(ize,mz-2)
    do j=1,my
     do i=1,mx
      ddzdni(i,j,k) = ( &
                     a*(f(i,j,k   )-f(i,j,k-1)) + &
                     b*(f(i,j,k+1 )-f(i,j,k-2)) + &
                     c*(f(i,j,k+2 )-f(i,j,k-3)))
     end do
    end do
  end do
  !$omp end parallel
  do j=1,my
    do i=1,mx
      ddzdni(i,j,mz-1) = ( &
                     a*(-f(i,j,mz-2)+f(i,j,mz-1)) + &
                     b*(-f(i,j,mz-3)+f(i,j,mz  )) + &
                     c*(-f(i,j,mz-4)+h(i,j,1   )))
      ddzdni(i,j,mz  ) = ( &
                     a*(-f(i,j,mz-1)+f(i,j,mz  )) + &
                     b*(-f(i,j,mz-2)+h(i,j,1   )) + &
                     c*(-f(i,j,mz-3)+h(i,j,2   )))
      ddzdni(i,j,1   ) = ( &
                     a*(-g(i,j,3   )+f(i,j,1   )) + &
                     b*(-g(i,j,2   )+f(i,j,2   )) + &
                     c*(-g(i,j,1   )+f(i,j,3   )))
      ddzdni(i,j,2   ) = ( &
                     a*(-f(i,j,1   )+f(i,j,2   )) + &
                     b*(-g(i,j,3   )+f(i,j,3   )) + &
                     c*(-g(i,j,2   )+f(i,j,4   )))
      ddzdni(i,j,3   ) = ( &
                     a*(-f(i,j,2   )+f(i,j,3   )) + &
                     b*(-f(i,j,1   )+f(i,j,4   )) + &
                     c*(-g(i,j,3   )+f(i,j,5   )))
    end do
  end do
!
  nstag = nstag+1
  nflop = nflop+8
END FUNCTION

!***********************************************************************
FUNCTION ddzdni1 (f)
!
!  f is centered on (i,j), fifth order stagger
!
  use params, g=>gz1
  implicit none
  integer i, j, k
  real, dimension(mx,my,mz):: f, ddzdni1
!-----------------------------------------------------------------------
!
  if (mz .lt. 5) then
    !$omp parallel private(k)
    do k=izs,ize
      ddzdni1(:,:,k) = f(:,:,k)
    end do
    !$omp end parallel
    return
  end if
!
  call mpi_send_z (f, g, 1, g, 0)
!
  !$omp parallel private(i,j,k)
  do k=max(izs,2),ize
    do j=1,my
     do i=1,mx
      ddzdni1(i,j,k) = ( &
                     (f(i,j,k)-f(i,j,k-1)))
     end do
    end do
  end do
  !$omp end parallel
  do j=1,my
    do i=1,mx
      ddzdni1(i,j,1   ) = ( &
                     (f(i,j,1)-g(i,j,1)))
    end do
  end do
!
  nstag1 = nstag1+1
  nflop = nflop+2
END FUNCTION
