! $Id: quench.f90,v 1.17 2013/03/21 08:22:27 bob Exp $
!**********************************************************************
MODULE quench_m
  implicit none
  real qmax, qlim
  logical verbose
END module

!**********************************************************************
SUBROUTINE init_quench
  USE params
  USE quench_m
  implicit none
  character(len=mid):: id="$Id: quench.f90,v 1.17 2013/03/21 08:22:27 bob Exp $"

  call print_id(id)
  do_quench = .false.
  qmax = 4.
  qlim = 2.
  verbose = .false.
  call read_quench
  if (do_quench) call test_quench
END SUBROUTINE

!**********************************************************************
SUBROUTINE read_quench
  USE params
  USE quench_m
  implicit none
  namelist /quench/ do_quench, qmax, qlim
  read  (stdin,quench)
  if (master) write (*,quench)
  if (.not. do_quench) qlim=1.
END SUBROUTINE

!**********************************************************************
SUBROUTINE test_quench
  USE params
  USE quench_m
  USE arrays, ONLY: scr6
  implicit none
  integer mx0,my0,mz0
  real, allocatable, dimension(:,:,:):: f, g
  integer ix,iy,iz

  allocate (f(mx,my,mz), g(mx,my,mz))
  f=0.
  ix=11
  iy=12
  iz=13
  if (mx==1) ix=1
  if (my==1) iy=1
  if (mz==1) iz=1
  f(ix,iy,iz)=1.; f(1,iy,iz)=1.; f(ix,1,iz)=1.; f(ix,iy,1)=1.
  g=f
  !call max3_set (g, f, scratch)

  call quenchx1(f)
  if (mpi_master .and. verbose) print 1,'qx',f(:,iy,iz)
  f=g
  call quenchy1(f)
  if (mpi_master .and. verbose) print 1,'qy',f(ix,:,iz)
  f=g
  call quenchz1(f)
  if (mpi_master .and. verbose) print 1,'qz',f(ix,iy,:)

  f=g
  call quenchx(f,g)
  if (mpi_master .and. verbose) print 1,'qx',g(:,iy,iz)
  call quenchy(f,g)
  if (mpi_master .and. verbose) print 1,'qy',g(ix,:,iz)
  call quenchz(f,g,scr6)
  if (mpi_master .and. verbose) print 1,'qz',g(ix,iy,:)

1 format(a,/(7g11.3))
  deallocate (f, g)
END SUBROUTINE

!**********************************************************************
SUBROUTINE quenchx (f,g)
  USE params
  USE quench_m, only: qlim, qmax
  implicit none
!
!  This is one of the fundamental operators in the MHD simulation code,
!  and is used in the dissipation expressions.
!
!----------------------------------------------------------------------
!
  real, dimension(mx,my,mz) :: f, g
  intent(inout):: f
  intent(out):: g
  real :: d1(-1:mx+1), d2(0:mx+1), fa(0:mx+1), qq(0:mx+1)
  integer i,j,k
!-----------------------------------------------------------------------
!
  if (.not.do_quench .or. qmax.eq.1.) then
    do k=izs,ize
      g(:,:,k) = f(:,:,k)
    end do
    return
  end if
!
  call mpi_send_x (f,gx2,2,hx2,2)
!
  call smooth3_set(abs(f),g)
  do k=izs,ize
     do j=1,my
       d1(0   )=f (1,j,k)-gx2(2  ,j,k)
       do i=1,mx-1
         d1(i)=f(i+1,j,k)-f(i,j,k)
         fa(i)=g(i,j,k)
         d2(i)=abs(d1(i)-d1(i-1))
         qq(i)=d2(i)/(fa(i)+(1./qmax)*d2(i)+1e-20)
       enddo 
       d1(-1  )=gx2(2,j,k)-gx2(1  ,j,k)
       d2(0   )=abs(d1(0   )-d1(-1  ))
       d1(mx  )=hx2(1,j,k)-f (mx ,j,k)
       d2(mx  )=abs(d1(mx  )-d1(mx-1))
       d1(mx+1)=hx2(2,j,k)-hx2(1  ,j,k)
       d2(mx+1)=abs(d1(mx+1)-d1(mx  ))

       fa(0   )=abs(gx2(2,j,k))
       qq(0   )=d2(0   )/(fa(0   )+(1./qmax)*d2(0   )+1e-20)
       fa(mx  )=abs(f(mx,j,k))
       qq(mx  )=d2(mx  )/(fa(mx  )+(1./qmax)*d2(mx  )+1e-20)
       fa(mx+1)=abs(hx2(1,j,k))
       qq(mx+1)=d2(mx+1)/(fa(mx+1)+(1./qmax)*d2(mx+1)+1e-20)
       do i=1,mx
         g(i,j,k)=f(i,j,k)*min(max(qq(i-1),qq(i),qq(i+1)),qlim)
       enddo 
     enddo
  enddo
!
  nflop = nflop + 11
END

!**********************************************************************
SUBROUTINE quenchx1 (f)
  USE params
  USE quench_m, only: qlim, qmax
  implicit none
!
!  This is one of the fundamental operators in the MHD simulation code,
!  and is used in the dissipation expressions.
!
!----------------------------------------------------------------------
!
  real, dimension(mx,my,mz) :: f
  intent(inout):: f
  real :: d1(-1:mx+1), d2(0:mx+1), fa(0:mx+1), qq(0:mx+1)
  integer i,j,k
!-----------------------------------------------------------------------
!
  if (.not.do_quench .or. qmax.eq.1.) then
    return
  end if
!
  call mpi_send_x (f,gx2,2,hx2,2)
!
  do k=izs,ize
     do j=1,my
       d1(0   )=f (1,j,k)-gx2(2 ,j,k)
       do i=1,mx-1
         d1(i)=f(i+1,j,k)-f(i,j,k)					! +-2 for sawtooth
         fa(i)=abs(f(i,j,k))						! +1 for sawtooth
         d2(i)=abs(d1(i)-d1(i-1))					! +4 for sawtooth
         qq(i)=d2(i)/(fa(i)+(1./qmax)*d2(i)+1e-20)			! 4/(1+4/qmax) [ harmonic mean of 4 and qmax ]
       enddo 
       d1(-1  )=gx2(2,j,k)-gx2(1 ,j,k)
       d2(0   )=abs(d1(0   )-d1(-1  ))
       d1(mx  )=hx2(1,j,k)-f (mx,j,k)
       d2(mx  )=abs(d1(mx  )-d1(mx-1))
       d1(mx+1)=hx2(2,j,k)-hx2(1 ,j,k)
       d2(mx+1)=abs(d1(mx+1)-d1(mx  ))
       fa(0   )=abs(gx2(2,j,k))
       qq(0   )=d2(0   )/(fa(0   )+(1./qmax)*d2(0   )+1e-20)
       fa(mx  )=abs(f(mx,j,k))
       qq(mx  )=d2(mx  )/(fa(mx  )+(1./qmax)*d2(mx  )+1e-20)
       fa(mx+1)=abs(hx2(1,j,k))
       qq(mx+1)=d2(mx+1)/(fa(mx+1)+(1./qmax)*d2(mx+1)+1e-20)
       do i=1,mx
         f(i,j,k)=f(i,j,k)*min(max(qq(i-1),qq(i),qq(i+1)),qlim)
       enddo 
     enddo
  enddo
!
  nflop = nflop + 11
END

!**********************************************************************
SUBROUTINE quenchx2 (f,g)
  USE params
  USE quench_m, only: qlim, qmax
  implicit none
!
!  This is one of the fundamental operators in the MHD simulation code,
!  and is used in the dissipation expressions.
!
!----------------------------------------------------------------------
!
  real, dimension(mx,my,mz) :: f,g
  intent(inout):: f
  real :: d1(-1:mx+1), d2(0:mx+1), fa(0:mx+1), qq(0:mx+1)
  integer i,j,k
!-----------------------------------------------------------------------
!
  if (.not.do_quench .or. qmax.eq.1.) then
    return
  end if
!
  call mpi_send_x (f,gx2,2,hx2,2)
!
  call smooth3_set(abs(f),g)
  do k=izs,ize
     do j=1,my
       d1(0   )=f (1,j,k)-gx2(2 ,j,k)
       do i=1,mx-1
         d1(i)=f(i+1,j,k)-f(i,j,k)					! +-2 for sawtooth
         fa(i)=g(i,j,k)
         d2(i)=abs(d1(i)-d1(i-1))					! +4 for sawtooth
         qq(i)=d2(i)/(fa(i)+(1./qmax)*d2(i)+1e-20)			! 4/(1+4/qmax) [ harmonic mean of 4 and qmax ]
       enddo 
       d1(-1  )=gx2(2,j,k)-gx2(1 ,j,k)
       d2(0   )=abs(d1(0   )-d1(-1  ))
       d1(mx  )=hx2(1,j,k)-f (mx,j,k)
       d2(mx  )=abs(d1(mx  )-d1(mx-1))
       d1(mx+1)=hx2(2,j,k)-hx2(1 ,j,k)
       d2(mx+1)=abs(d1(mx+1)-d1(mx  ))
       fa(0   )=abs(gx2(2,j,k))
       qq(0   )=d2(0   )/(fa(0   )+(1./qmax)*d2(0   )+1e-20)
       fa(mx  )=abs(f(mx,j,k))
       qq(mx  )=d2(mx  )/(fa(mx  )+(1./qmax)*d2(mx  )+1e-20)
       fa(mx+1)=abs(hx2(1,j,k))
       qq(mx+1)=d2(mx+1)/(fa(mx+1)+(1./qmax)*d2(mx+1)+1e-20)
       do i=1,mx
         g(i,j,k)=min(max(qq(i-1),qq(i),qq(i+1)),qlim)
       enddo 
     enddo
  enddo
!
  nflop = nflop + 11
END

!**********************************************************************
SUBROUTINE quenchy (f, g)
  USE params
  USE quench_m, only: qlim, qmax
  implicit none
!
  real, dimension(mx,my,mz) :: f, g
  real :: d1(-1:my+1), d2(0:my+1), fa(0:my+1), qq(0:my+1)
  integer i,j,k
!----------------------------------------------------------------------
!
  if (.not.do_quench .or. qmax==1.0 .or. my==1) then
    do k=izs,ize
      g(:,:,k) = f(:,:,k)
    end do
    return
  end if

  call mpi_send_y (f,gy2,2,hy2,2)

  call smooth3_set(abs(f),g)
  do k=izs,ize
     do i=1,mx
       d1(0   )=f (i,1,k)-gy2(i, 2,k)
       do j=1,my-1
         d1(j)=f(i,j+1,k)-f(i,j,k)
         fa(j)=g(i,j,k)
         d2(j)=abs(d1(j)-d1(j-1))
         qq(j)=d2(j)/(fa(j)+(1./qmax)*d2(j)+1e-20)
       enddo 
       d1(-1  )=gy2(i,2,k)-gy2(i, 1,k)
       d2(0   )=abs(d1(0   )-d1(-1  ))
       d1(my  )=hy2(i,1,k)-f (i,my,k)
       d2(my  )=abs(d1(my  )-d1(my-1))
       d1(my+1)=hy2(i,2,k)-hy2(i, 1,k)
       d2(my+1)=abs(d1(my+1)-d1(my  ))

       fa(0   )=abs(gy2(i,2,k))
       qq(0   )=d2(0   )/(fa(0   )+(1./qmax)*d2(0   )+1e-20)
       fa(my  )=abs(f(i,my,k))
       qq(my  )=d2(my  )/(fa(my  )+(1./qmax)*d2(my  )+1e-20)
       fa(my+1)=abs(hy2(i,1,k))
       qq(my+1)=d2(my+1)/(fa(my+1)+(1./qmax)*d2(my+1)+1e-20)
       do j=1,my
         g(i,j,k)=f(i,j,k)*min(max(qq(j-1),qq(j),qq(j+1)),qlim)
       enddo
     enddo
     if (lb >  1) g(:, 1:lb  ,k) = f(:, 1:lb  ,k)
     if (ub < my) g(:,ub+1:my,k) = f(:,ub+1:my,k)
  enddo

  nflop = nflop + 11
END

!**********************************************************************
SUBROUTINE quenchy1 (f)
  USE params
  USE quench_m, only: qlim, qmax
  implicit none
!
  real, dimension(mx,my,mz) :: f
  real :: d1(-1:my+1), d2(0:my+1), fa(0:my+1), qq(0:my+1)
  integer i,j,k
!
!----------------------------------------------------------------------
!
  if (.not.do_quench .or. qmax==1. .or. my==1) then
    return
  end if
!
  call mpi_send_y (f,gy2,2,hy2,2)
!
  do k=izs,ize
     do i=1,mx
       d1(0   )=f (i,1,k)-gy2(i, 2,k)
       do j=1,my-1
         d1(j)=f(i,j+1,k)-f(i,j,k)
         fa(j)=abs(f(i,j,k))
         d2(j)=abs(d1(j)-d1(j-1))
         qq(j)=d2(j)/(fa(j)+(1./qmax)*d2(j)+1e-20)
       enddo 
       d1(-1  )=gy2(i,2,k)-gy2(i, 1,k)
       d2(0   )=abs(d1(0   )-d1(-1  ))
       d1(my  )=hy2(i,1,k)-f (i,my,k)
       d2(my  )=abs(d1(my  )-d1(my-1))
       d1(my+1)=hy2(i,2,k)-hy2(i, 1,k)
       d2(my+1)=abs(d1(my+1)-d1(my  ))
       fa(0   )=abs(gy2(i,2,k))
       qq(0   )=d2(0   )/(fa(0   )+(1./qmax)*d2(0   )+1e-20)
       fa(my  )=abs(f(i,my,k))
       qq(my  )=d2(my  )/(fa(my  )+(1./qmax)*d2(my  )+1e-20)
       fa(my+1)=abs(hy2(i,1,k))
       qq(my+1)=d2(my+1)/(fa(my+1)+(1./qmax)*d2(my+1)+1e-20)
       do j=lb,ub
         f(i,j,k)=f(i,j,k)*min(max(qq(j-1),qq(j),qq(j+1)),qlim)
       enddo
     enddo
  enddo
!
  nflop = nflop + 11
END

!**********************************************************************
SUBROUTINE quenchy2 (f,g)
  USE params
  USE quench_m, only: qlim, qmax
  implicit none
!
  real, dimension(mx,my,mz) :: f,g
  real :: d1(-1:my+1), d2(0:my+1), fa(0:my+1), qq(0:my+1)
  integer i,j,k
!
!----------------------------------------------------------------------
!
  if (.not.do_quench .or. qmax==1.0 .or. my==1) then
    g=1.
    return
  end if
!
  call mpi_send_y (f,gy2,2,hy2,2)
!
  call smooth3_set(abs(f),g)
  do k=izs,ize
     do i=1,mx
       d1(0   )=f (i,1,k)-gy2(i, 2,k)
       do j=1,my-1
         d1(j)=f(i,j+1,k)-f(i,j,k)
         fa(j)=g(i,j,k)
         d2(j)=abs(d1(j)-d1(j-1))
         qq(j)=d2(j)/(fa(j)+(1./qmax)*d2(j)+1e-20)
       enddo 
       d1(-1  )=gy2(i,2,k)-gy2(i, 1,k)
       d2(0   )=abs(d1(0   )-d1(-1  ))
       d1(my  )=hy2(i,1,k)-f (i,my,k)
       d2(my  )=abs(d1(my  )-d1(my-1))
       d1(my+1)=hy2(i,2,k)-hy2(i, 1,k)
       d2(my+1)=abs(d1(my+1)-d1(my  ))
       fa(0   )=abs(gy2(i,2,k))
       qq(0   )=d2(0   )/(fa(0   )+(1./qmax)*d2(0   )+1e-20)
       fa(my  )=abs(f(i,my,k))
       qq(my  )=d2(my  )/(fa(my  )+(1./qmax)*d2(my  )+1e-20)
       fa(my+1)=abs(hy2(i,1,k))
       qq(my+1)=d2(my+1)/(fa(my+1)+(1./qmax)*d2(my+1)+1e-20)
       do j=lb,ub
         g(i,j,k)=min(max(qq(j-1),qq(j),qq(j+1)),qlim)
       enddo
       do j=1,lb-1
         g(i,j,k)=1.
       enddo
       do j=ub+1,my
         g(i,j,k)=1.
       enddo
     enddo
  enddo
!
  nflop = nflop + 11
END

!**********************************************************************
SUBROUTINE quenchz (f, g, s)
  USE params
  USE quench_m
  implicit none
!
  real, intent(inout), dimension(mx,my,mz) :: f, g, s
  integer i,j,k,js,je
  real, dimension(mx,my,2):: gz, hz
  real, allocatable, dimension(:,:) :: fa, d1, d2, qq
!----------------------------------------------------------------------
!
  if (.not.do_quench .or. qmax.eq.1. .or. mz<4) then
    do k=izs,ize
      g(:,:,k) = f(:,:,k)
    end do
    return
  end if
!
  call mpi_send_z (f,gz,2,hz,2)
!
  allocate (fa(mx,-1:mz+2), d1(mx,-1:mz+1), d2(mx,0:mz+1), qq(mx,0:mz+1))
  call limits_omp(1,my,js,je)
  call smooth3_set(abs(f),g)
  do j=js,je
    do i=1,mx
      d1(i,-1)=gz(i,j,2)-gz(i,j,1)
      d1(i, 0)=f (i,j,1)-gz(i,j,2)
      d2(i, 0)=abs(d1(i,0   )-d1(i,-1  ))
      fa(i, 0)=abs(gz(i,j,2))
      qq(i, 0)=d2(i,0)/(fa(i,0)+(1./qmax)*d2(i,0)+1e-20)
    enddo
    do k=1,mz-1
      do i=1,mx
        d1(i,k)=f(i,j,k+1)-f(i,j,k)
        fa(i,k)=g(i,j,k)
        d2(i,k)=abs(d1(i,k)-d1(i,k-1))
        qq(i,k)=d2(i,k)/(fa(i,k)+(1./qmax)*d2(i,k)+1e-20)
      enddo 
    enddo 
    do i=1,mx
      d1(i,mz  )=hz(i,j,1)-f (i,j,mz)
      d1(i,mz+1)=hz(i,j,2)-hz(i,j,1)
      d2(i,mz  )=abs(d1(i,mz  )-d1(i,mz-1))
      d2(i,mz+1)=abs(d1(i,mz+1)-d1(i,mz  ))
      fa(i,mz  )=abs(f(i,j,mz))
      qq(i,mz  )=d2(i,mz  )/(fa(i,mz  )+(1./qmax)*d2(i,mz  )+1e-20)
      fa(i,mz+1)=abs(hz(i,j,1))
      qq(i,mz+1)=d2(i,mz+1)/(fa(i,mz+1)+(1./qmax)*d2(i,mz+1)+1e-20)
    enddo 
    do k=1,mz
      do i=1,mx
        g(i,j,k)=f(i,j,k)*min(max(qq(i,k-1),qq(i,k),qq(i,k+1)),qlim)
      enddo 
    enddo 
  enddo
  deallocate (fa, d1, d2, qq)
!
  nflop = nflop + 11
END

!**********************************************************************
SUBROUTINE quenchz1 (f)
  USE params
  USE quench_m
  implicit none
!
  real, intent(inout), dimension(mx,my,mz) :: f
  integer i,j,k
  real, dimension(mx,my,2):: gz, hz
  real, allocatable, dimension(:,:) :: fa, d1, d2, qq
!----------------------------------------------------------------------
!
  if (.not.do_quench .or. qmax.eq.1. .or. mz<4) then
    return
  end if
!
  call mpi_send_z (f,gz,2,hz,2)
!
  allocate (fa(mx,-1:mz+2), d1(mx,-1:mz+1), d2(mx,0:mz+1), qq(mx,0:mz+1))
  do j=1,my
     do i=1,mx
       d1(i,0   )=f (i,j,1)-gz(i,j,2)
     enddo
     do k=izs,min(ize,mz-1)
       do i=1,mx
         d1(i,k)=f(i,j,k+1)-f(i,j,k)
         fa(i,k)=abs(f(i,j,k))
         d2(i,k)=abs(d1(i,k)-d1(i,k-1))
         qq(i,k)=d2(i,k)/(fa(i,k)+(1./qmax)*d2(i,k)+1e-20)
       enddo 
     enddo 
     if (izs.eq.1) then
      do i=1,mx
       d1(i,-1  )=gz(i,j,2)-gz(i,j,1)
       d2(i,0   )=abs(d1(i,0   )-d1(i,-1  ))
       fa(i,0   )=abs(gz(i,j,2))
       qq(i,0   )=d2(i,0   )/(fa(i,0   )+(1./qmax)*d2(i,0   )+1e-20)
      enddo 
     end if
     if (ize.eq.mz) then
      do i=1,mx
       d1(i,mz  )=hz(i,j,1)-f (i,j,mz)
       d2(i,mz  )=abs(d1(i,mz  )-d1(i,mz-1))
       d1(i,mz+1)=hz(i,j,2)-hz(i,j,1)
       d2(i,mz+1)=abs(d1(i,mz+1)-d1(i,mz  ))
       fa(i,mz  )=abs(f(i,j,mz))
       qq(i,mz  )=d2(i,mz  )/(fa(i,mz  )+(1./qmax)*d2(i,mz  )+1e-20)
       fa(i,mz+1)=abs(hz(i,j,1))
       qq(i,mz+1)=d2(i,mz+1)/(fa(i,mz+1)+(1./qmax)*d2(i,mz+1)+1e-20)
      enddo 
     end if
     do k=izs,ize
       do i=1,mx
         f(i,j,k)=f(i,j,k)*min(max(qq(i,k-1),qq(i,k),qq(i,k+1)),qlim)
       enddo 
     enddo 
  enddo
  deallocate (fa, d1, d2, qq)
!
  nflop = nflop + 11
END

!**********************************************************************
SUBROUTINE quenchz2 (f,g)
  USE params
  USE quench_m
  implicit none
!
  real, intent(inout), dimension(mx,my,mz) :: f,g
  integer i,j,k
  real, dimension(mx,my,2):: gz, hz
  real, allocatable, dimension(:,:) :: fa, d1, d2, qq
!----------------------------------------------------------------------
!
  if (.not.do_quench .or. qmax.eq.1. .or. mz<4) then
    do k=izs,ize
      g(:,:,k) = 1.
    end do
    return
  end if
!
  call mpi_send_z (f,gz,2,hz,2)
!
  allocate (fa(mx,-1:mz+2), d1(mx,-1:mz+1), d2(mx,0:mz+1), qq(mx,0:mz+1))
  call smooth3_set(abs(f),g)
  do j=1,my
     do i=1,mx
       d1(i,0   )=f (i,j,1)-gz(i,j,2)
     enddo
     do k=izs,min(ize,mz-1)
       do i=1,mx
         d1(i,k)=f(i,j,k+1)-f(i,j,k)
         fa(i,k)=g(i,j,k)
         d2(i,k)=abs(d1(i,k)-d1(i,k-1))
         qq(i,k)=d2(i,k)/(fa(i,k)+(1./qmax)*d2(i,k)+1e-20)
       enddo 
     enddo 
     if (izs.eq.1) then
      do i=1,mx
       d1(i,-1  )=gz(i,j,2)-gz(i,j,1)
       d2(i,0   )=abs(d1(i,0   )-d1(i,-1  ))
       fa(i,0   )=abs(gz(i,j,2))
       qq(i,0   )=d2(i,0   )/(fa(i,0   )+(1./qmax)*d2(i,0   )+1e-20)
      enddo 
     end if
     if (ize.eq.mz) then
      do i=1,mx
       d1(i,mz  )=hz(i,j,1)-f (i,j,mz)
       d2(i,mz  )=abs(d1(i,mz  )-d1(i,mz-1))
       d1(i,mz+1)=hz(i,j,2)-hz(i,j,1)
       d2(i,mz+1)=abs(d1(i,mz+1)-d1(i,mz  ))
       fa(i,mz  )=abs(f(i,j,mz))
       qq(i,mz  )=d2(i,mz  )/(fa(i,mz  )+(1./qmax)*d2(i,mz  )+1e-20)
       fa(i,mz+1)=abs(hz(i,j,1))
       qq(i,mz+1)=d2(i,mz+1)/(fa(i,mz+1)+(1./qmax)*d2(i,mz+1)+1e-20)
      enddo 
     end if
     do k=izs,ize
       do i=1,mx
         g(i,j,k)=min(max(qq(i,k-1),qq(i,k),qq(i,k+1)),qlim)
       enddo 
     enddo 
  enddo
  deallocate (fa, d1, d2, qq)
!
  nflop = nflop + 11
END

!**********************************************************************
SUBROUTINE quenchxy (f, g)
  USE params, h=>scratch
  USE quench_m
  implicit none
  real, dimension(mx,my,mz):: f, g
  integer ix, iy, iz
!
  call quenchx(f,g)
  call quenchy(f,h)
  do iz=izs,ize
  do iy=1,my
  do ix=1,mx
    g(ix,iy,iz) = sign(max(abs(g(ix,iy,iz)), abs(h(ix,iy,iz))), f(ix,iy,iz))
  end do
  end do
  end do
!
END

!**********************************************************************
SUBROUTINE quenchyz (f, g, s)
  USE params
  USE quench_m
  implicit none
  real, dimension(mx,my,mz):: f, g, s
  integer ix, iy, iz
!
  call quenchz(f,g,s)
  call quenchy(f,s)
  do iz=izs,ize
  do iy=1,my
  do ix=1,mx
    g(ix,iy,iz) = sign(max(abs(g(ix,iy,iz)), abs(s(ix,iy,iz))), f(ix,iy,iz))
  end do
  end do
  end do
!
END

!**********************************************************************
SUBROUTINE quenchzx (f, g, s)
  USE params
  USE quench_m
  implicit none
  real, dimension(mx,my,mz):: f, g, s
  integer ix, iy, iz
!
  call quenchz(f,g,s)
  call quenchx(f,s)
  do iz=izs,ize
  do iy=1,my
  do ix=1,mx
    g(ix,iy,iz) = sign(max(abs(g(ix,iy,iz)), abs(s(ix,iy,iz))), f(ix,iy,iz))
  end do
  end do
  end do
!
END
