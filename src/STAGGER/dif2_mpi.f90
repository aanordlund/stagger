
!***********************************************************************
FUNCTION d2abs (f)
!
!  Max abs difference one zone left to one zones right.
!

  USE params, gz=>gz1, hz=>hz1
  implicit none
  logical omp_in_parallel
  real cx, cy, cz
  real, dimension(mx,my,mz):: f, d2abs
!hpf$ align with TheCube:: f, d2abs
  integer i, j, k, km1, kp1
!-----------------------------------------------------------------------

!$omp parallel private(i,j,k,km1,kp1)
  call mpi_send_x (f, gx1, 1, hx1, 1)
  call mpi_send_y (f, gy1, 1, hy1, 1)
  call mpi_send_z (f, gz, 1, hz, 1)

  if (mx.gt.1) then
    do k=izs,ize
      do j=1,my
        do i=2,mx-1
          d2abs(i,j,k) = abs(f(i-1 ,j,k)+f(i+1,j,k)-f(i,j,k)-f(i,j,k))
        end do
        d2abs(1 ,j,k)  = abs(gx1(1   ,j,k)+f (2,j,k)-f(1 ,j,k)-f(1 ,j,k))
        d2abs(mx,j,k)  = abs(f (mx-1,j,k)+hx1(1,j,k)-f(mx,j,k)-f(mx,j,k))
      end do
    end do
  else
    do k=izs,ize
      do j=1,my
        do i=1,mx
          d2abs(i,j,k) = 0.
        end do
      end do
    end do
  end if

 if (my .gt. 1) then
  do k=izs,ize
    do j=2,my-1
      do i=1,mx
        d2abs(i,j, k) = max(d2abs(i,j, k),abs(f(i,j -1,k)+f(i,j+1,k)-f(i,j,k)-f(i,j,k)))
      end do
    end do
    do i=1,mx
        d2abs(i,1 ,k) = max(d2abs(i,1 ,k),abs(gy1(i,   1,k)+f (i,2,k)-f(i, 1,k)-f(i, 1,k)))
        d2abs(i,my,k) = max(d2abs(i,my,k),abs(f (i,my-1,k)+hy1(i,1,k)-f(i,my,k)-f(i,my,k)))
    end do
  end do
 end if
 if (mz .gt. 1) then
!$omp barrier
  do k=izs,ize
    if (k.eq.1) then
      do j=1,my
      do i=1,mx
        d2abs(i,j,k ) = max(d2abs(i,j,k ),abs(gz(i,j,1)+f(i,j,k+1)-f(i,j,k)-f(i,j,k)))
      end do
      end do
    else if (k.eq.mz) then
      do j=1,my
      do i=1,mx
        d2abs(i,j,k ) = max(d2abs(i,j,k ),abs(hz(i,j,1)+f(i,j,k-1)-f(i,j,k)-f(i,j,k)))
      end do
      end do
    else
      do j=1,my
      do i=1,mx
        d2abs(i,j,k ) = max(d2abs(i,j,k ),abs(f(i,j,k-1)+f(i,j,k+1)-f(i,j,k)-f(i,j,k)))
      end do
      end do
    end if
  end do
 end if
!$omp end parallel

END FUNCTION
!***********************************************************************
FUNCTION dif2a (f)
!
!  Max abs difference one zone left to one zones right.
!

  USE params, gz=>gz1, hz=>hz1
  implicit none
  logical omp_in_parallel
  real cx, cy, cz
  real, dimension(mx,my,mz):: f, dif2a
!hpf$ align with TheCube:: f, dif2a
  integer i, j, k, km1, kp1
!-----------------------------------------------------------------------

!$omp parallel private(i,j,k,km1,kp1)
  call mpi_send_x (f, gx1, 1, hx1, 1)
  call mpi_send_y (f, gy1, 1, hy1, 1)
  call mpi_send_z (f, gz, 1, hz, 1)

  if (mx.gt.1) then
    do k=izs,ize
      do j=1,my
        do i=2,mx-1
          dif2a(i,j,k) = abs(f(i-1 ,j,k)-f(i+1,j,k))
        end do
        dif2a(1 ,j,k)  = abs(gx1(1   ,j,k)-f (2,j,k))
        dif2a(mx,j,k)  = abs(f (mx-1,j,k)-hx1(1,j,k))
      end do
    end do
  else
    do k=izs,ize
      do j=1,my
        do i=1,mx
          dif2a(i,j,k) = 0.
        end do
      end do
    end do
  end if

 if (my .gt. 1) then
  do k=izs,ize
    do j=2,my-1
      do i=1,mx
        dif2a(i,j, k) = max(dif2a(i,j, k),abs(f(i,j -1,k)-f(i,j+1,k)))
      end do
    end do
    do i=1,mx
        dif2a(i,1 ,k) = max(dif2a(i,1 ,k),abs(gy1(i,   1,k)-f (i,2,k)))
        dif2a(i,my,k) = max(dif2a(i,my,k),abs(f (i,my-1,k)-hy1(i,1,k)))
    end do
  end do
 end if
 if (mz .gt. 1) then
!$omp barrier
  do k=izs,ize
    if (k.eq.1) then
      do j=1,my
      do i=1,mx
        dif2a(i,j,k ) = max(dif2a(i,j,k ),abs(gz(i,j,1)-f(i,j,k+1)))
      end do
      end do
    else if (k.eq.mz) then
      do j=1,my
      do i=1,mx
        dif2a(i,j,k ) = max(dif2a(i,j,k ),abs(hz(i,j,1)-f(i,j,k-1)))
      end do
      end do
    else
      do j=1,my
      do i=1,mx
        dif2a(i,j,k ) = max(dif2a(i,j,k ),abs(f(i,j,k-1)-f(i,j,k+1)))
      end do
      end do
    end if
  end do
 end if
!$omp end parallel

END FUNCTION

!***********************************************************************
FUNCTION dif2 (fx, fy, fz)
!
!  Finite difference one zone left to one zones right.
!

  USE params, gz=>gz1, hz=>hz1
  implicit none
  real cx(mx), cy(my), cz(mz)
  real, dimension(mx,my,mz):: fx, fy, fz, dif2
  integer i, j, k, km1, kp1
  character(len=mid):: id="$Id: dif2_mpi.f90,v 1.21 2013/11/02 21:59:23 aake Exp $"
!-----------------------------------------------------------------------
  call print_id(id)

  cx=0.5/dxm
  cy=0.5/dym
  cz=0.5/dzm

!$omp parallel private(i,j,k,km1,kp1)

  call mpi_send_x (fx, gx1, 1, hx1, 1)

  if (mx.gt.1) then
    do k=izs,ize
      do j=1,my
        do i=2,mx-1
          dif2(i,j,k) = cx(i)*(fx(i-1 ,j,k)-fx(i+1,j,k))
        end do
        dif2(1 ,j,k)  = cx( 1)*(gx1(1   ,j,k)-fx(2  ,j,k))
        dif2(mx,j,k)  = cx(mx)*(fx(mx-1,j,k)-hx1(1  ,j,k))
      end do
    end do
  else
    do k=izs,ize
      do j=1,my
        do i=1,mx
          dif2(i,j,k) = 0.
        end do
      end do
    end do
  end if

  call mpi_send_y (fy, gy1, 1, hy1, 1)

  if (my .gt. 1) then
    do k=izs,ize
     do j=2,my-1
       do i=1,mx
         dif2(i,j, k) = dif2(i,j, k) + cy(j)*(fy(i,j -1,k)-fy(i,j+1,k))
       end do
     end do
     do i=1,mx
         dif2(i,1 ,k) = dif2(i,1 ,k) + cy( 1)*(gy1(i,   1,k)-fy(i,  2,k))
         dif2(i,my,k) = dif2(i,my,k) + cy(my)*(fy(i,my-1,k)-hy1(i,  1,k))
     end do
    end do
  end if

  call mpi_send_z (fz, gz, 1, hz, 1)

 if (mz .gt. 1) then
!$omp barrier
  do k=izs,ize
    if (k.eq.1) then
      do j=1,my
      do i=1,mx
        dif2(i,j,k ) = dif2(i,j,k ) + cz(k)*(gz(i,j,  1)-fz(i,j,k+1))
      end do
      end do
    else if (k.eq.mz) then
      do j=1,my
      do i=1,mx
        dif2(i,j,k ) = dif2(i,j,k ) + cz(k)*(fz(i,j,k-1)-hz(i,j,1))
      end do
      end do
    else
      do j=1,my
      do i=1,mx
        dif2(i,j,k ) = dif2(i,j,k ) + cz(k)*(fz(i,j,k-1)-fz(i,j,k+1))
      end do
      end do
    end if
  end do
 end if

 do k=izs,ize
   do j=1,my
     do i=1,mx
       dif2(i,j,k) = max (0., dif2(i,j,k))
     end do
   end do
 end do
!$omp end parallel

END FUNCTION
!***********************************************************************
FUNCTION dif2d (fx, fy, fz)
!
!  Finite difference one zone left to one zones right.
!

  USE params, gz=>gz1, hz=>hz1
  implicit none
  real, dimension(mx,my,mz):: fx, fy, fz, dif2d
  integer i, j, k, km1, kp1
  character(len=mid):: id="$Id: dif2_mpi.f90,v 1.21 2013/11/02 21:59:23 aake Exp $"
!-----------------------------------------------------------------------
  call print_id(id)


!$omp parallel private(i,j,k,km1,kp1)
  call mpi_send_x (fx, gx1, 1, hx1, 1)

  if (mx.gt.1) then
    do k=izs,ize
      do j=1,my
        do i=2,mx-1
          dif2d(i,j,k) = (fx(i-1 ,j,k)-fx(i+1,j,k))
        end do
        dif2d(1 ,j,k)  = (gx1(1   ,j,k)-fx(2  ,j,k))
        dif2d(mx,j,k)  = (fx(mx-1,j,k)-hx1(1  ,j,k))
      end do
    end do
  else
    do k=izs,ize
      do j=1,my
        do i=1,mx
          dif2d(i,j,k) = 0.
        end do
      end do
    end do
  end if

  call mpi_send_y (fy, gy1, 1, hy1, 1)

  if (my .gt. 1) then
    do k=izs,ize
     do j=2,my-1
       do i=1,mx
         dif2d(i,j, k) = dif2d(i,j, k) + (fy(i,j -1,k)-fy(i,j+1,k))
       end do
     end do
     do i=1,mx
         dif2d(i,1 ,k) = dif2d(i,1 ,k) + (gy1(i,   1,k)-fy(i,  2,k))
         dif2d(i,my,k) = dif2d(i,my,k) + (fy(i,my-1,k)-hy1(i,  1,k))
     end do
    end do
  end if

  call mpi_send_z (fz, gz, 1, hz, 1)

 if (mz .gt. 1) then
!$omp barrier
  do k=izs,ize
    if (k.eq.1) then
      do j=1,my
      do i=1,mx
        dif2d(i,j,k ) = dif2d(i,j,k ) + (gz(i,j,  1)-fz(i,j,k+1))
      end do
      end do
    else if (k.eq.mz) then
      do j=1,my
      do i=1,mx
        dif2d(i,j,k ) = dif2d(i,j,k ) + (fz(i,j,k-1)-hz(i,j,1))
      end do
      end do
    else
      do j=1,my
      do i=1,mx
        dif2d(i,j,k ) = dif2d(i,j,k ) + (fz(i,j,k-1)-fz(i,j,k+1))
      end do
      end do
    end if
  end do
 end if

 do k=izs,ize
   do j=1,my
     do i=1,mx
       dif2d(i,j,k) = max (0., dif2d(i,j,k))
     end do
   end do
 end do
!$omp end parallel

END FUNCTION

!***********************************************************************
FUNCTION dif1xy (fx, fy)
!
!  Finite difference curl one zone left to center
!
  USE params
  implicit none
  real, dimension(mx,my,mz):: fx, fy, dif1xy
  real, dimension(mx):: cx
  real, dimension(my):: cy
  integer i, j, k, km1, kp1
  character(len=mid):: id="dif1xy $Id: dif2_mpi.f90,v 1.21 2013/11/02 21:59:23 aake Exp $"
!-----------------------------------------------------------------------
  call print_id(id)
  cx = 1./dxmdn
  cy = 1./dymdn
!$omp parallel private(i,j,k,km1,kp1)
  call mpi_send_x (fy, gx1, 1, hx1, 1)
  if (mx.gt.1) then
    do k=izs,ize
      do j=1,my
        do i=2,mx
          dif1xy(i,j,k) = cx(i)*(fy(i ,j,k)-fy(i-1,j,k))
        end do
        dif1xy(1,j,k)  = cx(1)*(fy(1,j,k)-gx1(  1,j,k))
      end do
    end do
  else
    do k=izs,ize
      do j=1,my
        do i=1,mx
          dif1xy(i,j,k) = 0.
        end do
      end do
    end do
  end if
  call mpi_send_y (fx, gy1, 1, hy1, 1)
  if (my .gt. 1) then
    do k=izs,ize
     do j=2,my
       do i=1,mx
         dif1xy(i,j, k) = dif1xy(i, j,k) - cy(j)*(fx(i, j,k)-fx(i,j-1,k))
       end do
     end do
     do i=1,mx
         dif1xy(i,1,k) = dif1xy(i,1,k) - cy(1)*(fx(i,1,k)-gy1(i,  1,k))
     end do
    end do
  end if
  do k=izs,ize
    do j=1,my
      do i=1,mx
        dif1xy(i,j,k) = dif1xy(i,j,k)**2
      end do
    end do
  end do
!$omp end parallel
END FUNCTION

!***********************************************************************
FUNCTION dif1yz (fy, fz)
!
!  Finite difference one zone left to center
!
  USE params, gz=>gz1, hz=>hz1
  implicit none
  real, dimension(mx,my,mz):: fy, fz, dif1yz
  real, dimension(my):: cy
  real, dimension(mz):: cz
  integer i, j, k, km1, kp1
  character(len=mid):: id="difyz $Id: dif2_mpi.f90,v 1.21 2013/11/02 21:59:23 aake Exp $"
!-----------------------------------------------------------------------
  call print_id(id)
  cy = 1./dymdn
  cz = 1./dzmdn
!$omp parallel private(i,j,k,km1,kp1)
  call mpi_send_y (fz, gy1, 1, hy1, 1)
  if (my .gt. 1) then
    do k=izs,ize
     do j=2,my
       do i=1,mx
         dif1yz(i,j, k) = cy(j)*(fz(i, j,k)-fz(i,j-1,k))
       end do
     end do
     do i=1,mx
         dif1yz(i,1,k) = cy(1)*(fz(i,1,k)-gy1(i,  1,k))
     end do
    end do
  end if
  call mpi_send_z (fy, gz, 1, hz, 1)
  if (mz .gt. 1) then
!$omp barrier
   do k=izs,ize
     if (k.eq.1) then
       do j=1,my
       do i=1,mx
         dif1yz(i,j,k) = dif1yz(i,j,k) - cz(k)*(fy(i,j,k)-gz(i,j,1))
       end do
       end do
     else
       do j=1,my
       do i=1,mx
         dif1yz(i,j,k) = dif1yz(i,j,k) - cz(k)*(fy(i,j,k)-fy(i,j,k-1))
       end do
       end do
     end if
   end do
  end if
  do k=izs,ize
    do j=1,my
      do i=1,mx
        dif1yz(i,j,k) = dif1yz(i,j,k)**2
      end do
    end do
  end do
 !$omp end parallel
END FUNCTION

!***********************************************************************
FUNCTION dif1zx (fz, fx)
!
!  Finite difference one zone left to center
!
  USE params, gz=>gz1, hz=>hz1
  implicit none
  real, dimension(mx,my,mz):: fx, fz, dif1zx
  real, dimension(mx):: cx
  real, dimension(mz):: cz
  integer i, j, k, km1, kp1
  character(len=mid):: id="dif1zx $Id: dif2_mpi.f90,v 1.21 2013/11/02 21:59:23 aake Exp $"
!-----------------------------------------------------------------------
  call print_id(id)
  cx = 1./dxmdn
  cz = 1./dzmdn
!$omp parallel private(i,j,k,km1,kp1)
  call mpi_send_x (fz, gx1, 1, hx1, 1)
  if (mx.gt.1) then
    do k=izs,ize
      do j=1,my
        do i=2,mx
          dif1zx(i,j,k) = cx(i)*(fz(i ,j,k)-fz(i-1,j,k))
        end do
        dif1zx(1,j,k)  = cx(1)*(fz(1,j,k)-gx1(  1,j,k))
      end do
    end do
  else
    do k=izs,ize
      do j=1,my
        do i=1,mx
          dif1zx(i,j,k) = 0.
        end do
      end do
    end do
  end if
  call mpi_send_z (fx, gz, 1, hz, 1)
  if (mz .gt. 1) then
!$omp barrier
    do k=izs,ize
      if (k.eq.1) then
        do j=1,my
        do i=1,mx
          dif1zx(i,j,k) = dif1zx(i,j,k) - cz(k)*(fx(i,j,k)-gz(i,j,1))
        end do
        end do
      else
        do j=1,my
        do i=1,mx
          dif1zx(i,j,k) = dif1zx(i,j,k) - cz(k)*(fx(i,j,k)-fx(i,j,k-1))
        end do
        end do
      end if
    end do
   end if
   do k=izs,ize
     do j=1,my
       do i=1,mx
         dif1zx(i,j,k) = dif1zx(i,j,k)**2
       end do
     end do
   end do
!$omp end parallel
END FUNCTION

!***********************************************************************
FUNCTION dif1 (fx, fy, fz)
!
!  Finite difference one zone left to center
!

  USE params, gz=>gz1, hz=>hz1
  implicit none
  real, dimension(mx,my,mz):: fx, fy, fz, dif1
  real, dimension(mx):: cx
  real, dimension(my):: cy
  real, dimension(mz):: cz
  integer i, j, k, km1, kp1
  character(len=mid):: id="$Id: dif2_mpi.f90,v 1.21 2013/11/02 21:59:23 aake Exp $"
!-----------------------------------------------------------------------
  call print_id(id)


  cx = 1./dxmdn
  cy = 1./dymdn
  cz = 1./dzmdn

!$omp parallel private(i,j,k,km1,kp1)

  call mpi_send_x (fx, gx1, 1, hx1, 1)

  if (mx.gt.1) then
    do k=izs,ize
      do j=1,my
        do i=1,mx-1
          dif1(i,j,k) = cx(i)*(fx(i ,j,k)-fx(i+1,j,k))
        end do
        dif1(mx,j,k)  = cx(mx)*(fx(mx,j,k)-hx1(  1,j,k))
      end do
    end do
  else
    do k=izs,ize
      do j=1,my
        do i=1,mx
          dif1(i,j,k) = 0.
        end do
      end do
    end do
  end if

  call mpi_send_y (fy, gy1, 1, hy1, 1)

  if (my .gt. 1) then
    do k=izs,ize
     do j=1,my-1
       do i=1,mx
         dif1(i,j, k) = dif1(i, j,k) + cy(j)*(fy(i, j,k)-fy(i,j+1,k))
       end do
     end do
     do i=1,mx
         dif1(i,my,k) = dif1(i,my,k) + cy(my)*(fy(i,my,k)-hy1(i,  1,k))
     end do
    end do
  end if

  call mpi_send_z (fz, gz, 1, hz, 1)

 if (mz .gt. 1) then
!$omp barrier
  do k=izs,ize
    if (k.eq.mz) then
      do j=1,my
      do i=1,mx
        dif1(i,j,k) = dif1(i,j,k) + cz(k)*(fz(i,j,k)-hz(i,j,1))
      end do
      end do
    else
      do j=1,my
      do i=1,mx
        dif1(i,j,k) = dif1(i,j,k) + cz(k)*(fz(i,j,k)-fz(i,j,k+1))
      end do
      end do
    end if
  end do
 end if

 do k=izs,ize
   do j=1,my
     do i=1,mx
       dif1(i,j,k) = max (0., dif1(i,j,k))
     end do
   end do
 end do
!$omp end parallel

END FUNCTION

!***********************************************************************
FUNCTION dif1d (fx, fy, fz)
!
!  Finite difference one zone left to center
!

  USE params, gz=>gz1, hz=>hz1
  implicit none
  real, dimension(mx,my,mz):: fx, fy, fz, dif1d
  integer i, j, k, km1, kp1
  character(len=mid):: id="$Id: dif2_mpi.f90,v 1.21 2013/11/02 21:59:23 aake Exp $"
!-----------------------------------------------------------------------
  call print_id(id)


!$omp parallel private(i,j,k,km1,kp1)

  call mpi_send_x (fx, gx1, 1, hx1, 1)

  if (mx.gt.1) then
    do k=izs,ize
      do j=1,my
        do i=1,mx-1
          dif1d(i,j,k) = (fx(i ,j,k)-fx(i+1,j,k))
        end do
        dif1d(mx,j,k)  = (fx(mx,j,k)-hx1(  1,j,k))
      end do
    end do
  else
    do k=izs,ize
      do j=1,my
        do i=1,mx
          dif1d(i,j,k) = 0.
        end do
      end do
    end do
  end if

  call mpi_send_y (fy, gy1, 1, hy1, 1)

  if (my .gt. 1) then
    do k=izs,ize
     do j=1,my-1
       do i=1,mx
         dif1d(i,j, k) = dif1d(i, j,k) + (fy(i, j,k)-fy(i,j+1,k))
       end do
     end do
     do i=1,mx
         dif1d(i,my,k) = dif1d(i,my,k) + (fy(i,my,k)-hy1(i,  1,k))
     end do
    end do
  end if

  call mpi_send_z (fz, gz, 1, hz, 1)

 if (mz .gt. 1) then
!$omp barrier
  do k=izs,ize
    if (k.eq.mz) then
      do j=1,my
      do i=1,mx
        dif1d(i,j,k) = dif1d(i,j,k) + (fz(i,j,k)-hz(i,j,1))
      end do
      end do
    else
      do j=1,my
      do i=1,mx
        dif1d(i,j,k) = dif1d(i,j,k) + (fz(i,j,k)-fz(i,j,k+1))
      end do
      end do
    end if
  end do
 end if

 do k=izs,ize
   do j=1,my
     do i=1,mx
       dif1d(i,j,k) = max (0., dif1d(i,j,k))
     end do
   end do
 end do
!$omp end parallel

END FUNCTION

