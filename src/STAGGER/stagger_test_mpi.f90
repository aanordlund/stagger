!***********************************************************************
 !PROGRAM test
  !USE params
  !mx = 100
  !my = 100
  !mz = 100
  !call test_stagger 
 !END

!***********************************************************************
SUBROUTINE test_stagger
  USE params
  logical debug
  if (debug(dbg_stagger)) print *,'calling test_stagger_ops',mpi_rank
  call test_send_ops
  call test_stagger_ops
  if (debug(dbg_stagger)) print *,'calling test_smooth_max',mpi_rank
  call test_smooth_max
  if (debug(dbg_stagger)) print *,'calling test_divcurl',mpi_rank
  call test_divcurl
  if (debug(dbg_stagger)) print *,'calling test_del2',mpi_rank
  call test_del2
END 

!***********************************************************************
SUBROUTINE test_send_ops
  USE params
  USE arrays, only: scr1, scr2, scr3, scr4, scr5, scr6
  implicit none
  real, dimension(mx,1,mz):: g, h
  real, dimension(mz):: sumx
  integer iy, iz
  real avg, expect
  
  if (mpi_ny==1) return
  scr1(:,my,:)=mpi_y+2

  call mpi_send_y (scr1, g, 1, h, 0)
  do iz=1,mz
    sumx(iz)=sum(g(:,1,iz))
  end do
  avg = sum(sumx)/(mx*mz)
  expect = mpi_y+1
  if (mpi_y==0) expect=mpi_ny+1
  if (abs(1.-avg/expect) > 1e-6) then
    print'(a,2i4,1p,3g12.4)', 'test_send_y ERROR: rank, mpi_y, avg, expect', &
    mpi_rank, mpi_y, avg, expect
  else if (master) then
    print'(a,2i4,1p,3g12.4)', 'test_send_y: rank, mpi_y, avg, expect', &
        mpi_rank, mpi_y, avg, expect
  end if
END 

!***********************************************************************
SUBROUTINE test_stagger_ops
!
! Test some of the stagger routines.
!
!-----------------------------------------------------------------------
  USE params
  USE arrays, only: scr1, scr2, scr3, scr4, scr5, scr6
  implicit none
  logical omp_in_parallel, debug
  logical, save:: once=.true.
  integer ix, iy, iz, omp_get_thread_num, imax(3)
  real fmax1, fmax2, xx, yy, zz, ax, ay, az, kx, ky, kz, fmaxval, eps
  character(len=mid):: id="$Id: stagger_test_mpi.f90,v 1.19 2014/08/19 08:49:16 aake Exp $"
  character(len=mid):: fmt='(1x,a,4i5,4g12.4)'
  character(len=4) srank
  integer nprint, mprint, nwarn

  call print_id(id)
  !print *,'rank,thread,lb,ub:',mpi_rank,omp_mythread,lb,ub
  mprint = 50
  nprint = mprint

  if (debug(dbg_stagger)) &
    print '(i4,10e12.5)',mpi_rank,ym(1:2),ym(my-1:my),ymdn(1:2),ymdn(my-1:my)

!-----------------------------------------------------------------------
!  Test up-operators
!-----------------------------------------------------------------------
  nwarn = 0
  ax = 1.
  ay = 1.
  az = 1.
  kx = 2.*pi/sx
  ky = 2.*pi/sy
  kz = 2.*pi/sz
!$omp parallel private(iz,iy,ix)
  do iz=izs,ize
    do iy=1,my
      do ix=1,mx
        scr1(ix,iy,iz) = ax*sin(kx*xmdn(ix))
        scr2(ix,iy,iz) = ax*sin(kx*xm  (ix))
      end do
    end do
  end do
  call flush_mpi
  call xup_set (scr1, scr3)
    call dumpn(scr1,'xup','stagger.dmp',0)
    call dumpn(scr2,'xup','stagger.dmp',1)
    call dumpn(scr3,'xup','stagger.dmp',1)
  fmax1 = fmaxval('scr3',scr3)
  if (master .and. debug(dbg_stagger)) print *,'fmax1:a',mpi_rank, fmax1
  call flush_mpi
  do iz=izs,ize
    scr2(:,:,iz) = abs(scr2(:,:,iz)-scr3(:,:,iz))
  end do
!$omp end parallel
  fmax2 = maxval(scr2(:,lb:ub,:))
  eps = ax*max(5e-6,1e-7*(64./(sx/minval(dxm)))**5)
  if (debug(dbg_stagger)) &
    print '(1x,a,i4,3e12.5)','test_stagger: rank,fmax1,fmax2,eps=', mpi_rank, fmax1, fmax2, eps
  if (abs(fmax2) .gt. eps) then
    imax = maxloc(scr2(:,lb:ub,:))
    imax(2) = imax(2)+lb-1
    if (nprint>0) &
    print fmt,'WARNING: stagger xup appears to be incorrect at', mpi_rank, imax, fmax2, eps
    nwarn = nwarn + 1
  else if (master) then
    print *,'test_stagger: xup OK', abs(fmax2), eps
  end if
  call sum_int_mpi (nwarn); nprint = nprint - nwarn

  if (my > 1) then
!$omp parallel private(iz,iy,ix)
  do iz=izs,ize
    do iy=1,my
      do ix=1,mx
        scr1(ix,iy,iz) = ay*sin(ky*ymdn(iy))
        scr2(ix,iy,iz) = ay*sin(ky*ym  (iy))
      end do
    end do
  end do
  call flush_mpi
  call yup_set (scr1, scr3)
    call dumpn(scr1,'yup','stagger.dmp',1)
    call dumpn(scr2,'yup','stagger.dmp',1)
    call dumpn(scr3,'yup','stagger.dmp',1)
  fmax1 = fmaxval('scr1',scr1)
  if (master .and. debug(dbg_stagger)) print *,'fmax1:c',mpi_rank, fmax1
  do iz=izs,ize
    scr2(:,:,iz) = abs(scr2(:,:,iz)-scr3(:,:,iz))
  end do
!$omp end parallel
  fmax2 = maxval(scr2(:,lb:ub,:))
  eps = ay*max(5e-6,1e-7*(64./(sy/minval(dym)))**5)
  if (debug(dbg_stagger)) &
    print '(1x,a,i4,3e12.5)','test_stagger: rank,fmax1,fmax2,eps=', mpi_rank, fmax1, fmax2, eps
  if (abs(fmax2) .gt. eps) then
    imax = maxloc(scr2(:,lb:ub,:))
    imax(2) = imax(2)+lb-1
    if (nprint>0) &
    print fmt,'WARNING: stagger yup appears to be incorrect at', mpi_rank, imax, fmax2, eps
    nwarn = nwarn + 1
  else if (master) then
    print *,'test_stagger: yup OK', abs(fmax2), eps
  end if
  end if

  if (mz > 1) then
!$omp parallel private(iz,iy,ix)
  do iz=izs,ize
    do iy=1,my
      do ix=1,mx
        scr1(ix,iy,iz) = az*sin(kz*zmdn(iz))
        scr2(ix,iy,iz) = az*sin(kz*zm  (iz))
      end do
    end do
  end do
  call flush_mpi
  call zup_set (scr1, scr3)
    call dumpn(scr1,'zup','stagger.dmp',1)
    call dumpn(scr2,'zup','stagger.dmp',1)
    call dumpn(scr3,'zup','stagger.dmp',1)
  fmax1 = fmaxval('scr3',scr3)
  if (master .and. debug(dbg_stagger)) print *,'fmax1:c',mpi_rank, fmax1
  do iz=izs,ize
    scr2(:,:,iz) = abs(scr2(:,:,iz)-scr3(:,:,iz))
  end do
!$omp end parallel
  fmax2 = maxval(scr2(:,lb:ub,:))
  eps = az*max(5e-6,1e-7*(64./(sz/minval(dzm)))**5)
  if (debug(dbg_stagger)) &
    print '(1x,a,i4,3e12.5)','test_stagger: rank,fmax1,fmax2,eps=', mpi_rank, fmax1, fmax2, eps
  if (abs(fmax2) .gt. eps) then
    imax = maxloc(scr2(:,lb:ub,:))
    imax(2) = imax(2)+lb-1
    if (nprint>0) &
    print fmt,'WARNING: stagger zup appears to be incorrect at', mpi_rank, imax, fmax2, eps
    nwarn = nwarn + 1
  else if (master) then
    print *,'test_stagger: zup OK', abs(fmax2), eps
  end if
  end if

!-----------------------------------------------------------------------
!  Test dn-operators
!-----------------------------------------------------------------------
  ax = 1.
  ay = 1.
  az = 1.
  kx = 2.*pi/sx
  ky = 2.*pi/sy
  kz = 2.*pi/sz
!$omp parallel private(iz,iy,ix)
  do iz=izs,ize
    do iy=1,my
      do ix=1,mx
        scr1(ix,iy,iz) = ax*sin(kx*xm  (ix))
        scr2(ix,iy,iz) = ax*sin(kx*xmdn(ix))
      end do
    end do
  end do
  call flush_mpi
  call xdn_set (scr1, scr3)
    call dumpn(scr1,'xdn','stagger.dmp',1)
    call dumpn(scr2,'xdn','stagger.dmp',1)
    call dumpn(scr3,'xdn','stagger.dmp',1)
  fmax1 = fmaxval('scr3',scr3)
  if (master .and. debug(dbg_stagger)) print *,'fmax1:a',mpi_rank, fmax1
  do iz=izs,ize
    scr2(:,:,iz) = abs(scr2(:,:,iz)-scr3(:,:,iz))
  end do
!$omp end parallel
  fmax2 = maxval(scr2(:,lb:ub,:))
  eps = ax*max(5e-6,1e-7*(64./(sx/minval(dxm)))**5)
  if (debug(dbg_stagger)) &
    print '(1x,a,i4,3e12.5)','test_stagger: rank,fmax1,fmax2,eps=', mpi_rank, fmax1, fmax2, eps
  if (abs(fmax2) .gt. eps) then
    imax = maxloc(scr2(:,lb:ub,:))
    imax(2) = imax(2)+lb-1
    if (nprint>0) &
    print fmt,'WARNING: stagger xdn appears to be incorrect at', mpi_rank, imax, fmax2, eps
    nwarn = nwarn + 1
    if (debug(dbg_stagger)) then
      print '(1x,a,i5/(10g11.3))', 'x-dir', mpi_rank, scr2(:,imax(2),imax(3))
    end if
  else if (master) then
    print *,'test_stagger: xdn OK', abs(fmax2), eps
  end if

  if (my > 1) then
!$omp parallel private(iz,iy,ix)
  do iz=izs,ize
    do iy=1,my
      do ix=1,mx
        scr1(ix,iy,iz) = ay*sin(ky*ym  (iy))
        scr2(ix,iy,iz) = ay*sin(ky*ymdn(iy))
      end do
    end do
  end do
  call flush_mpi
  call ydn_set (scr1, scr3)
    call dumpn(scr1,'ydn','stagger.dmp',1)
    call dumpn(scr2,'ydn','stagger.dmp',1)
    call dumpn(scr3,'ydn','stagger.dmp',1)
  fmax1 = fmaxval('scr1',scr1)
  if (master .and. debug(dbg_stagger)) print *,'fmax1:c',mpi_rank, fmax1
  do iz=izs,ize
    scr2(:,:,iz) = abs(scr2(:,:,iz)-scr3(:,:,iz))
  end do
!$omp end parallel
  fmax2 = maxval(scr2(:,lb:ub,:))
  eps = ay*max(5e-6,1e-7*(64./(sy/minval(dym)))**5)
  if (debug(dbg_stagger)) &
    print '(1x,a,i4,3e12.5)','test_stagger: rank,fmax1,fmax2,eps=', mpi_rank, fmax1, fmax2, eps
  if (abs(fmax2) .gt. eps) then
    imax = maxloc(scr2(:,lb:ub,:))
    imax(2) = imax(2)+lb-1
    if (nprint>0) &
    print fmt,'WARNING: stagger ydn appears to be incorrect at', mpi_rank, imax, fmax2, eps
    nwarn = nwarn + 1
    if (debug(dbg_stagger)) then
      print '(1x,a,i5/(10g11.3))', 'y-dir', mpi_rank, scr2(imax(1),:,imax(3))
    end if
  else if (master) then
    print *,'test_stagger: ydn OK', abs(fmax2), eps
  end if
  end if

  if (mz > 1) then
!$omp parallel private(iz,iy,ix)
  do iz=izs,ize
    do iy=1,my
      do ix=1,mx
        scr1(ix,iy,iz) = az*sin(kz*zm  (iz))
        scr2(ix,iy,iz) = az*sin(kz*zmdn(iz))
      end do
    end do
  end do
  call flush_mpi
  call zdn_set (scr1, scr3)
    call dumpn(scr1,'zdn','stagger.dmp',1)
    call dumpn(scr2,'zdn','stagger.dmp',1)
    call dumpn(scr3,'zdn','stagger.dmp',1)
  fmax1 = fmaxval('scr3',scr3)
  !fmax1 = maxval(scr3(:,lb:ub,:))
  if (master .and. debug(dbg_stagger)) print *,'fmax1:c',mpi_rank, fmax1
  do iz=izs,ize
    scr2(:,:,iz) = abs(scr2(:,:,iz)-scr3(:,:,iz))
  end do
!$omp end parallel
  fmax2 = maxval(scr2(:,lb:ub,:))
  eps = az*max(5e-6,1e-7*(64./(sz/minval(dzm)))**5)
  if (debug(dbg_stagger)) &
    print '(1x,a,i4,3e12.5)','test_stagger: rank,fmax1,fmax2,eps=', mpi_rank, fmax1, fmax2, eps
  if (abs(fmax2) .gt. eps) then
    imax = maxloc(scr2(:,lb:ub,:))
    imax(2) = imax(2)+lb-1
    if (nprint>0) &
    print fmt,'WARNING: stagger zdn appears to be incorrect at', mpi_rank, imax, fmax2, eps
    nwarn = nwarn + 1
    if (debug(dbg_stagger)) then
      print '(1x,a,i5/(10g11.3))', 'z-dir', mpi_rank, scr2(imax(1),imax(2),:)
    end if
  else if (master) then
    print *,'test_stagger: zdn OK', abs(fmax2), eps
  end if
  end if

!-----------------------------------------------------------------------
!  Test dd.up-operators
!-----------------------------------------------------------------------
  if (master) print *,'mesh ratios:',mx*dx/sx,my*dy/sy,mz*dz/sz
  ax = 1.
  ay = 1.
  az = 1.
  kx = 2.*pi/sx
  ky = 2.*pi/sy
  kz = 2.*pi/sz
!$omp parallel private(iz,iy,ix)
  do iz=izs,ize
    do iy=1,my
      do ix=1,mx
        scr1(ix,iy,iz) = ax*sin(kx*xmdn(ix))
        scr2(ix,iy,iz) = ax*kx*cos(kx*xm(ix))
      end do
    end do
  end do
  call ddxup_set (scr1, scr3)
    call dumpn(scr1,'ddxup','stagger.dmp',1)
    call dumpn(scr2,'ddxup','stagger.dmp',1)
    call dumpn(scr3,'ddxup','stagger.dmp',1)
  do iz=izs,ize
    scr2(:,:,iz) = abs(scr2(:,:,iz)-scr3(:,:,iz))
  end do
  fmax2 = maxval(scr2)
!$omp end parallel
  eps = ax*kx*max(3e-5*(sx/dx)/100.,1e-6*(64./(sx/dx))**6)
  if (fmax2 .gt. eps) then
    imax = maxloc(scr2(:,lb:ub,:))
    imax(2) = imax(2)+lb-1
    if (nprint>0) &
    print fmt,'WARNING: stagger ddxup-ops appears to be incorrect at', mpi_rank, imax, fmax2, eps
    nwarn = nwarn + 1
    if (once) then
      once=.false.
      write(srank,'(i4.4)') mpi_rank
      print*,'DUMPING to','ddxup.dat_'//srank
      open(88,file='ddxup.dat_'//srank,status='unknown',form='unformatted')
      write(88) mx,imax
      write(88) scr1(:,imax(2),imax(3)),scr2(:,imax(2),imax(3)),dxm,dxmdn,dxidxup
      close(88)
    endif
  else if (master) then
    print *,'test_stagger: ddxup OK', fmax2, eps
  end if

  if (my > 1) then
!$omp parallel private(iz,iy,ix)
  do iz=izs,ize
    do iy=1,my
      do ix=1,mx
        scr1(ix,iy,iz) = ay*sin(ky*ymdn(iy))
        scr2(ix,iy,iz) = ay*ky*cos(ky*ym(iy))
      end do
    end do
  end do
  call ddyup_set (scr1, scr3)
    call dumpn(scr1,'ddyup','stagger.dmp',1)
    call dumpn(scr2,'ddyup','stagger.dmp',1)
    call dumpn(scr3,'ddyup','stagger.dmp',1)
  do iz=izs,ize
    scr2(:,:,iz) = abs(scr2(:,:,iz)-scr3(:,:,iz))
  end do
  fmax2 = maxval(scr2(:,lb:ub,:))
!$omp end parallel
  eps = ay*ky*max(3e-5*maxval(sy/dym)/100.,1e-6*(64./minval(sy/dym))**6)
  if (fmax2 .gt. eps) then
    imax = maxloc(scr2(:,lb:ub,:))
    imax(2) = imax(2)+lb-1
    if (nprint>0) &
    print fmt,'WARNING: stagger ddyup appears to be incorrect at', mpi_rank, imax, fmax2, eps, scr2(imax(1),my/2,imax(3))
    nwarn = nwarn + 1
  else if (master) then
    print *,'test_stagger: ddyup OK', fmax2, eps
  end if
  endif

  if (mz > 1) then
!$omp parallel private(iz,iy,ix)
  do iz=izs,ize
    do iy=1,my
      do ix=1,mx
        scr1(ix,iy,iz) = az*sin(kz*zmdn(iz))
        scr2(ix,iy,iz) = az*kz*cos(kz*zm(iz))
      end do
    end do
  end do
  call ddzup_set (scr1, scr3)
    call dumpn(scr1,'ddzup','stagger.dmp',1)
    call dumpn(scr2,'ddzup','stagger.dmp',1)
    call dumpn(scr3,'ddzup','stagger.dmp',1)
  do iz=izs,ize
    scr2(:,:,iz) = abs(scr2(:,:,iz)-scr3(:,:,iz))
  end do
  fmax2 = maxval(scr2)
!$omp end parallel
  eps = az*kz*max(3e-5*(sz/dz)/100.,1e-6*(64./(sz/dz))**6)
  if (fmax2 .gt. eps) then
    imax = maxloc(scr2(:,lb:ub,:))
    imax(2) = imax(2)+lb-1
    if (nprint>0) &
    print fmt,'WARNING: stagger ddzup appears to be incorrect at', mpi_rank, imax, fmax2, eps, scr2(imax(1),imax(2)-lb+1,mz/2)
    nwarn = nwarn + 1
  else if (master) then
    print *,'test_stagger: ddzup OK', fmax2, eps
  end if
  endif

!-----------------------------------------------------------------------
!  Test dd.dn-operators
!-----------------------------------------------------------------------
  ax = 1.
  ay = 1.
  az = 1.
  kx = 2.*pi/sx
  ky = 2.*pi/sy
  kz = 2.*pi/sz
!$omp parallel private(iz,iy,ix)
  do iz=izs,ize
    do iy=1,my
      do ix=1,mx
        scr1(ix,iy,iz) = ax*sin(kx*xm(ix))
        scr2(ix,iy,iz) = ax*kx*cos(kx*xmdn(ix))
      end do
    end do
  end do
  call ddxdn_set (scr1, scr3)
    call dumpn(scr1,'ddxdn','stagger.dmp',1)
    call dumpn(scr2,'ddxdn','stagger.dmp',1)
    call dumpn(scr3,'ddxdn','stagger.dmp',1)
  do iz=izs,ize
    scr3(:,:,iz) = abs(scr2(:,:,iz)-scr3(:,:,iz))
  end do
  call fmaxval_subr('ddx',scr3,fmax2)
!$omp end parallel
  eps = ax*kx*max(3e-5*(sx/dx)/100.,1e-6*(64./(sx/dx))**6)
  if (fmax2 .gt. eps) then
    imax = maxloc(scr2(:,lb:ub,:))
    imax(2) = imax(2)+lb-1
    if (nprint>0) &
    print fmt,'WARNING: stagger ddxdn appears to be incorrect at', mpi_rank, imax, fmax2, eps
    nwarn = nwarn + 1
  else if (master) then
    print *,'test_stagger: ddxdn OK', fmax2, eps
  end if

  if (my > 1) then
!$omp parallel private(iz,iy,ix)
  do iz=izs,ize
    do iy=1,my
      do ix=1,mx
        scr1(ix,iy,iz) = ay*sin(ky*ym(iy))
        scr2(ix,iy,iz) = ay*ky*cos(ky*ymdn(iy))
      end do
    end do
  end do
  call ddydn_set (scr1, scr3)
    call dumpn(scr1,'ddydn','stagger.dmp',1)
    call dumpn(scr2,'ddydn','stagger.dmp',1)
    call dumpn(scr3,'ddydn','stagger.dmp',1)
  do iz=izs,ize
    scr2(:,:,iz) = abs(scr2(:,:,iz)-scr3(:,:,iz))
  end do
  call fmaxval_subr('ddy',scr2,fmax2)
!$omp end parallel
  eps = ay*ky*max(3e-5*maxval(sy/dym)/100.,1e-6*(64./minval(sy/dym))**6)
  if (fmax2 .gt. eps) then
    imax = maxloc(scr2(:,lb:ub,:))
    imax(2) = imax(2)+lb-1
    if (nprint>0) &
    print fmt,'WARNING: stagger ddydn appears to be incorrect at', mpi_rank, imax, fmax2, eps
    nwarn = nwarn + 1
  else if (master) then
    print *,'test_stagger: ddydn OK', fmax2, eps
  end if
  endif

  if (mz > 1) then
!$omp parallel private(iz,iy,ix)
  do iz=izs,ize
    do iy=1,my
      do ix=1,mx
        scr1(ix,iy,iz) = az*sin(kz*zm(iz))
        scr2(ix,iy,iz) = az*kz*cos(kz*zmdn(iz))
      end do
    end do
  end do
  call ddzdn_set (scr1, scr3)
    call dumpn(scr1,'ddzdn','stagger.dmp',1)
    call dumpn(scr2,'ddzdn','stagger.dmp',1)
    call dumpn(scr3,'ddzup','stagger.dmp',1)
  do iz=izs,ize
    scr2(:,:,iz) = abs(scr2(:,:,iz)-scr3(:,:,iz))
  end do
  call fmaxval_subr('ddz',scr2,fmax2)
!$omp end parallel
  eps = az*kz*max(3e-5*(sz/dz)/100.,1e-6*(64./(sz/dz))**6)
  if (fmax2 .gt. eps) then
    imax = maxloc(scr2(:,lb:ub,:))
    imax(2) = imax(2)+lb-1
    if (nprint>0) &
    print fmt,'WARNING: stagger ddzdn appears to be incorrect at', mpi_rank, imax, fmax2, eps
    nwarn = nwarn + 1
  else if (master) then
    print *,'test_stagger: ddzdn OK', fmax2, eps
  end if
  endif

!-----------------------------------------------------------------------
!  Test up-operators
!-----------------------------------------------------------------------
  ax = 1.
  ay = 1.
  az = 1.
  kx = 2.*pi/sx
  ky = 2.*pi/sy
  kz = 2.*pi/sz
!$omp parallel private(iz,iy,ix)
  do iz=izs,ize
    do iy=1,my
      do ix=1,mx
        scr1(ix,iy,iz) = ax*sin(kx*xmdn(ix))
        scr2(ix,iy,iz) = ax*sin(kx*xm  (ix))
      end do
    end do
  end do
  call flush_mpi
  call xup1_set (scr1, scr3)
  fmax1 = fmaxval('scr3',scr3)
  if (master .and. debug(dbg_stagger)) print *,'fmax1:a',mpi_rank, fmax1
  call flush_mpi
  do iz=izs,ize
    scr2(:,:,iz) = abs(scr2(:,:,iz)-scr3(:,:,iz))
  end do
!$omp end parallel
  fmax2 = maxval(scr2(:,lb:ub,:))
  eps = ax*max(5e-6,2e-3*(64./(sx/minval(dxm)))**2)
  if (debug(dbg_stagger)) &
    print '(1x,a,i4,3e12.5)','test_stagger: rank,fmax1,fmax2,eps=', mpi_rank, fmax1, fmax2, eps
  if (abs(fmax2) .gt. eps) then
    imax = maxloc(scr2(:,lb:ub,:))
    imax(2) = imax(2)+lb-1
    if (nprint>0) &
    print fmt,'WARNING: stagger xup1 appears to be incorrect at', mpi_rank, imax, fmax2, eps
    nwarn = nwarn + 1
  else if (master) then
    print *,'test_stagger: xup1 OK', abs(fmax2), eps
  end if

  if (my > 1) then
!$omp parallel private(iz,iy,ix)
  do iz=izs,ize
    do iy=1,my
      do ix=1,mx
        scr1(ix,iy,iz) = ay*sin(ky*ymdn(iy))
        scr2(ix,iy,iz) = ay*sin(ky*ym  (iy))
      end do
    end do
  end do
  call flush_mpi
  call yup1_set (scr1, scr3)
  fmax1 = fmaxval('scr1',scr1)
  if (master .and. debug(dbg_stagger)) print *,'fmax1:c',mpi_rank, fmax1
  do iz=izs,ize
    scr2(:,:,iz) = abs(scr2(:,:,iz)-scr3(:,:,iz))
  end do
!$omp end parallel
  fmax2 = maxval(scr2(:,lb:ub,:))
  eps = ay*max(5e-6,2e-3*(64./(sy/minval(dym)))**2)
  if (debug(dbg_stagger)) &
    print '(1x,a,i4,3e12.5)','test_stagger: rank,fmax1,fmax2,eps=', mpi_rank, fmax1, fmax2, eps
  if (abs(fmax2) .gt. eps) then
    imax = maxloc(scr2(:,lb:ub,:))
    imax(2) = imax(2)+lb-1
    if (nprint>0) &
    print fmt,'WARNING: stagger yup1 appear to be incorrect at', mpi_rank, imax, fmax2, eps
    nwarn = nwarn + 1
  else if (master) then
    print *,'test_stagger: yup1 OK', abs(fmax2), eps
  end if
  end if

  if (mz > 1) then
!$omp parallel private(iz,iy,ix)
  do iz=izs,ize
    do iy=1,my
      do ix=1,mx
        scr1(ix,iy,iz) = az*sin(kz*zmdn(iz))
        scr2(ix,iy,iz) = az*sin(kz*zm  (iz))
      end do
    end do
  end do
  call flush_mpi
  call zup1_set (scr1, scr3)
  fmax1 = fmaxval('scr3',scr3)
  if (master .and. debug(dbg_stagger)) print *,'fmax1:c',mpi_rank, fmax1
  do iz=izs,ize
    scr2(:,:,iz) = abs(scr2(:,:,iz)-scr3(:,:,iz))
  end do
!$omp end parallel
  fmax2 = maxval(scr2(:,lb:ub,:))
  eps = az*max(5e-6,2e-3*(64./(sz/minval(dzm)))**2)
  if (debug(dbg_stagger)) &
    print '(1x,a,i4,3e12.5)','test_stagger: rank,fmax1,fmax2,eps=', mpi_rank, fmax1, fmax2, eps
  if (abs(fmax2) .gt. eps) then
    imax = maxloc(scr2(:,lb:ub,:))
    imax(2) = imax(2)+lb-1
    if (nprint>0) &
    print fmt,'WARNING: stagger zup1 appears to be incorrect at', mpi_rank, imax, fmax2, eps
    nwarn = nwarn + 1
  else if (master) then
    print *,'test_stagger: zup1 OK', abs(fmax2), eps
  end if
  end if

!-----------------------------------------------------------------------
!  Test dn-operators
!-----------------------------------------------------------------------
  ax = 1.
  ay = 1.
  az = 1.
  kx = 2.*pi/sx
  ky = 2.*pi/sy
  kz = 2.*pi/sz
!$omp parallel private(iz,iy,ix)
  do iz=izs,ize
    do iy=1,my
      do ix=1,mx
        scr1(ix,iy,iz) = ax*sin(kx*xm  (ix))
        scr2(ix,iy,iz) = ax*sin(kx*xmdn(ix))
      end do
    end do
  end do
  call flush_mpi
  call xdn1_set (scr1, scr3)
  fmax1 = fmaxval('scr3',scr3)
  if (master .and. debug(dbg_stagger)) print *,'fmax1:a',mpi_rank, fmax1
  do iz=izs,ize
    scr2(:,:,iz) = abs(scr2(:,:,iz)-scr3(:,:,iz))
  end do
!$omp end parallel
  fmax2 = maxval(scr2(:,lb:ub,:))
  eps = ax*max(5e-6,2e-3*(64./(sx/minval(dxm)))**2)
  if (debug(dbg_stagger)) &
    print '(1x,a,i4,3e12.5)','test_stagger: rank,fmax1,fmax2,eps=', mpi_rank, fmax1, fmax2, eps
  if (abs(fmax2) .gt. eps) then
    imax = maxloc(scr2(:,lb:ub,:))
    imax(2) = imax(2)+lb-1
    if (nprint>0) &
    print fmt,'WARNING: stagger xdn1 appear to be incorrect at', mpi_rank, imax, fmax2, eps
    nwarn = nwarn + 1
    if (debug(dbg_stagger)) then
      print '(1x,a,i5/(10g11.3))', 'x-dir', mpi_rank, scr2(:,imax(2),imax(3))
    end if
  else if (master) then
    print *,'test_stagger: xdn1 OK', abs(fmax2), eps
  end if

  if (my > 1) then
!$omp parallel private(iz,iy,ix)
  do iz=izs,ize
    do iy=1,my
      do ix=1,mx
        scr1(ix,iy,iz) = ay*sin(ky*ym  (iy))
        scr2(ix,iy,iz) = ay*sin(ky*ymdn(iy))
      end do
    end do
  end do
  call flush_mpi
  call ydn1_set (scr1, scr3)
  fmax1 = fmaxval('scr1',scr1)
  if (master .and. debug(dbg_stagger)) print *,'fmax1:c',mpi_rank, fmax1
  do iz=izs,ize
    scr2(:,:,iz) = abs(scr2(:,:,iz)-scr3(:,:,iz))
  end do
!$omp end parallel
  fmax2 = maxval(scr2(:,lb:ub,:))
  eps = ay*max(5e-6,2e-3*(64./(sy/minval(dym)))**2)
  if (debug(dbg_stagger)) &
    print '(1x,a,i4,3e12.5)','test_stagger: rank,fmax1,fmax2,eps=', mpi_rank, fmax1, fmax2, eps
  if (abs(fmax2) .gt. eps) then
    imax = maxloc(scr2(:,lb:ub,:))
    imax(2) = imax(2)+lb-1
    if (nprint>0) &
    print fmt,'WARNING: stagger ydn1 appear to be incorrect at', mpi_rank, imax, fmax2, eps
    nwarn = nwarn + 1
    if (debug(dbg_stagger)) then
      print '(1x,a,i5/(10g11.3))', 'y-dir', mpi_rank, scr2(imax(1),:,imax(3))
    end if
  else if (master) then
    print *,'test_stagger: ydn1 OK', abs(fmax2), eps
  end if
  end if

  if (mz > 1) then
!$omp parallel private(iz,iy,ix)
  do iz=izs,ize
    do iy=1,my
      do ix=1,mx
        scr1(ix,iy,iz) = az*sin(kz*zm  (iz))
        scr2(ix,iy,iz) = az*sin(kz*zmdn(iz))
      end do
    end do
  end do
  call flush_mpi
  call zdn1_set (scr1, scr3)
  fmax1 = fmaxval('scr3',scr3)
  !fmax1 = maxval(scr3(:,lb:ub,:))
  if (master .and. debug(dbg_stagger)) print *,'fmax1:c',mpi_rank, fmax1
  do iz=izs,ize
    scr2(:,:,iz) = abs(scr2(:,:,iz)-scr3(:,:,iz))
  end do
!$omp end parallel
  fmax2 = maxval(scr2(:,lb:ub,:))
  eps = az*max(5e-6,2e-3*(64./(sz/minval(dzm)))**2)
  if (debug(dbg_stagger)) &
    print '(1x,a,i4,3e12.5)','test_stagger: rank,fmax1,fmax2,eps=', mpi_rank, fmax1, fmax2, eps
  if (abs(fmax2) .gt. eps) then
    imax = maxloc(scr2(:,lb:ub,:))
    imax(2) = imax(2)+lb-1
    if (nprint>0) &
    print fmt,'WARNING: stagger zdn1 appear to be incorrect at', mpi_rank, imax, fmax2, eps
    nwarn = nwarn + 1
    if (debug(dbg_stagger)) then
      print '(1x,a,i5/(10g11.3))', 'z-dir', mpi_rank, scr2(imax(1),imax(2),:)
    end if
  else if (master) then
    print *,'test_stagger: zdn1 OK', abs(fmax2), eps
  end if
  end if

!-----------------------------------------------------------------------
!  Test dd.up-operators
!-----------------------------------------------------------------------
  if (master) print *,'mesh ratios:',mx*dx/sx,my*dy/sy,mz*dz/sz
  ax = 1.
  ay = 1.
  az = 1.
  kx = 2.*pi/sx
  ky = 2.*pi/sy
  kz = 2.*pi/sz
!$omp parallel private(iz,iy,ix)
  do iz=izs,ize
    do iy=1,my
      do ix=1,mx
        scr1(ix,iy,iz) = ax*sin(kx*xmdn(ix))
        scr2(ix,iy,iz) = ax*kx*cos(kx*xm(ix))
      end do
    end do
  end do
  call ddxup1_set (scr1, scr3)
    call dumpn(scr1,'scr1','stagger.dmp',1)
    call dumpn(scr2,'scr2','stagger.dmp',1)
    call dumpn(scr3,'scr3','stagger.dmp',1)
  do iz=izs,ize
    scr2(:,:,iz) = abs(scr2(:,:,iz)-scr3(:,:,iz))
  end do
  fmax2 = maxval(scr2)
!$omp end parallel
  eps = ax*kx*max(3e-5*(sx/dx)/100.,7e-4*(64./(sx/dx))**2)
  if (fmax2 .gt. eps) then
    imax = maxloc(scr2(:,lb:ub,:))
    imax(2) = imax(2)+lb-1
    if (nprint>0) &
    print fmt,'WARNING: stagger ddxup1 appears to be incorrect at', mpi_rank, imax, fmax2, eps
    nwarn = nwarn + 1
    if (once) then
      once=.false.
      write(srank,'(i4.4)') mpi_rank
      print*,'DUMPING to','ddxup.dat_'//srank
      open(88,file='ddxup.dat_'//srank,status='unknown',form='unformatted')
      write(88) mx,imax
      write(88) scr1(:,imax(2),imax(3)),scr2(:,imax(2),imax(3)),dxm,dxmdn,dxidxup
      close(88)
    endif
  else if (master) then
    print *,'test_stagger: ddxup1 OK', fmax2, eps
  end if

  if (my > 1) then
!$omp parallel private(iz,iy,ix)
  do iz=izs,ize
    do iy=1,my
      do ix=1,mx
        scr1(ix,iy,iz) = ay*sin(ky*ymdn(iy))
        scr2(ix,iy,iz) = ay*ky*cos(ky*ym(iy))
      end do
    end do
  end do
  call ddyup1_set (scr1, scr3)
  do iz=izs,ize
    scr2(:,:,iz) = abs(scr2(:,:,iz)-scr3(:,:,iz))
  end do
  fmax2 = maxval(scr2(:,lb:ub,:))
!$omp end parallel
  eps = ay*ky*max(3e-5*maxval(sy/dym)/100.,7e-4*(64./minval(sy/dym))**2)
  if (fmax2 .gt. eps) then
    imax = maxloc(scr2(:,lb:ub,:))
    imax(2) = imax(2)+lb-1
    if (nprint>0) &
    print fmt,'WARNING: stagger ddyup1 appears to be incorrect at', mpi_rank, imax, fmax2, eps, scr2(imax(1),my/2,imax(3))
    nwarn = nwarn + 1
  else if (master) then
    print *,'test_stagger: ddyup1 OK', fmax2, eps
  end if
  endif

  if (mz > 1) then
!$omp parallel private(iz,iy,ix)
  do iz=izs,ize
    do iy=1,my
      do ix=1,mx
        scr1(ix,iy,iz) = az*sin(kz*zmdn(iz))
        scr2(ix,iy,iz) = az*kz*cos(kz*zm(iz))
      end do
    end do
  end do
  call ddzup1_set (scr1, scr3)
  do iz=izs,ize
    scr2(:,:,iz) = abs(scr2(:,:,iz)-scr3(:,:,iz))
  end do
  fmax2 = maxval(scr2)
!$omp end parallel
  eps = az*kz*max(3e-5*(sz/dz)/100.,7e-4*(64./(sz/dz))**2)
  if (fmax2 .gt. eps) then
    imax = maxloc(scr2(:,lb:ub,:))
    imax(2) = imax(2)+lb-1
    if (nprint>0) &
    print fmt,'WARNING: stagger ddzup1 appears to be incorrect at', mpi_rank, imax, fmax2, eps, scr2(imax(1),imax(2)-lb+1,mz/2)
    nwarn = nwarn + 1
  else if (master) then
    print *,'test_stagger: ddzup1 OK', fmax2, eps
  end if
  endif

!-----------------------------------------------------------------------
!  Test dd.dn-operators
!-----------------------------------------------------------------------
  ax = 1.
  ay = 1.
  az = 1.
  kx = 2.*pi/sx
  ky = 2.*pi/sy
  kz = 2.*pi/sz
!$omp parallel private(iz,iy,ix)
  do iz=izs,ize
    do iy=1,my
      do ix=1,mx
        scr1(ix,iy,iz) = ax*sin(kx*xm(ix))
        scr2(ix,iy,iz) = ax*kx*cos(kx*xmdn(ix))
      end do
    end do
  end do
  call ddxdn1_set (scr1, scr3)
    call dumpn(scr1,'scr1','stagger.dmp',1)
    call dumpn(scr2,'scr2','stagger.dmp',1)
    call dumpn(scr3,'scr3','stagger.dmp',1)
  do iz=izs,ize
    scr3(:,:,iz) = abs(scr2(:,:,iz)-scr3(:,:,iz))
  end do
  call fmaxval_subr('ddx',scr3,fmax2)
!$omp end parallel
  eps = ax*kx*max(3e-5*(sx/dx)/100.,7e-4*(64./(sx/dx))**2)
  if (fmax2 .gt. eps) then
    imax = maxloc(scr2(:,lb:ub,:))
    imax(2) = imax(2)+lb-1
    if (nprint>0) &
    print fmt,'WARNING: stagger ddxdn1 appears to be incorrect at', mpi_rank, imax, fmax2, eps
    nwarn = nwarn + 1
  else if (master) then
    print *,'test_stagger: ddxdn1 OK', fmax2, eps
  end if

  if (my > 1) then
!$omp parallel private(iz,iy,ix)
  do iz=izs,ize
    do iy=1,my
      do ix=1,mx
        scr1(ix,iy,iz) = ay*sin(ky*ym(iy))
        scr2(ix,iy,iz) = ay*ky*cos(ky*ymdn(iy))
      end do
    end do
  end do
  call ddydn1_set (scr1, scr3)
  do iz=izs,ize
    scr2(:,:,iz) = abs(scr2(:,:,iz)-scr3(:,:,iz))
  end do
  call fmaxval_subr('ddy',scr2,fmax2)
!$omp end parallel
  eps = ay*ky*max(3e-5*maxval(sy/dym)/100.,7e-4*(64./minval(sy/dym))**2)
  if (fmax2 .gt. eps) then
    imax = maxloc(scr2(:,lb:ub,:))
    imax(2) = imax(2)+lb-1
    if (nprint>0) &
    print fmt,'WARNING: stagger ddydn1 appears to be incorrect at', mpi_rank, imax, fmax2, eps
    nwarn = nwarn + 1
  else if (master) then
    print *,'test_stagger: ddydn1 OK', fmax2, eps
  end if
  endif

  if (mz > 1) then
!$omp parallel private(iz,iy,ix)
  do iz=izs,ize
    do iy=1,my
      do ix=1,mx
        scr1(ix,iy,iz) = az*sin(kz*zm(iz))
        scr2(ix,iy,iz) = az*kz*cos(kz*zmdn(iz))
      end do
    end do
  end do
  call ddzdn1_set (scr1, scr3)
  do iz=izs,ize
    scr2(:,:,iz) = abs(scr2(:,:,iz)-scr3(:,:,iz))
  end do
  call fmaxval_subr('ddz',scr2,fmax2)
!$omp end parallel
  eps = az*kz*max(3e-5*(sz/dz)/100.,7e-4*(64./(sz/dz))**2)
  if (fmax2 .gt. eps) then
    imax = maxloc(scr2(:,lb:ub,:))
    imax(2) = imax(2)+lb-1
    if (nprint>0) &
    print fmt,'WARNING: stagger ddzdn1 appears to be incorrect at', mpi_rank, imax, fmax2, eps
    nwarn = nwarn + 1
  else if (master) then
    print *,'test_stagger: ddzdn1 OK', fmax2, eps
  end if
  endif
  !eps = ax*max(2e-6,1e-2*(64./(sx/dx))**2) &

  call sum_int_mpi (nwarn)
  nprint = nprint-nwarn

  if (master) print *,'Total number of warnings =',mprint-nprint
END SUBROUTINE test_stagger_ops

!***********************************************************************
 SUBROUTINE test_smooth_max
!
! Test the parallelization of smooth3(max3())
!
!-----------------------------------------------------------------------
  USE params
  USE arrays, only: scr1, scr2, scr3, scr4, scr5, scr6
  implicit none
  integer ix, iy, iz, i(3)
  integer omp_get_thread_num
  real fmax1, fmax2, fmaxval, ran1s
  logical omp_in_parallel, debug

!$omp parallel private(iz,iy,ix), shared(fmax1)
  do iz=izs,ize
    do iy=1,my
      do ix=1,mx
        scr1(ix,iy,iz) = mod(5*ix+3*iy+7*iz,mx/2)
      end do
    end do
  end do
!$omp barrier
  call fmaxval_omp ('scr1',scr1,fmax1)
!$omp end parallel
  call dump (scr1, 'smooth.dat', 0)

!  This part for debugging only -- it causes a segv on rs6000 with allocates
  !print *,mpi_rank,'trace1a'
  !scr4 = max(scr1,cshift(scr1,shift=-1,dim=1),cshift(scr1,shift=1,dim=1))
  !print *,mpi_rank,'trace1b'
  !scr5 = max(scr4,cshift(scr4,shift=-1,dim=2),cshift(scr4,shift=1,dim=2))
  !print *,mpi_rank,'trace1c'
  !scr4 = max(scr5,cshift(scr5,shift=-1,dim=3),cshift(scr5,shift=1,dim=3))
  !call dump (scr4, 'smooth.dat', 5)
  !print *,mpi_rank,'trace1d'
  !scr5 = (scr4+cshift(scr4,shift=-1,dim=1)+cshift(scr4,shift=1,dim=1))/3.
  !print *,mpi_rank,'trace1e'
  !scr4 = (scr5+cshift(scr5,shift=-1,dim=2)+cshift(scr5,shift=1,dim=2))/3.
  !print *,mpi_rank,'trace1f'
  !scr5 = (scr4+cshift(scr4,shift=-1,dim=3)+cshift(scr4,shift=1,dim=3))/3.
  !call dump (scr5, 'smooth.dat', 6)
  !print *,mpi_rank,'trace2'

  fmax2 = fmaxval('scr1',scr1)
  if (master .and. debug(dbg_stagger)) print *,'test fmaxval:', fmax1, fmax2
  if (abs(fmax1-fmax2) .gt. 1e-5) then
    if (master) print *,'ERROR: results from fmaxval and fmaxval_omp differ'
    call end_mpi
    stop
  end if

  if (do_trace.and.master) print *,'calling max3_set'
  call dump (scr1, 'smooth.dat', 1)
  scr4 = scr1
  call max3_set (scr1, scr2)
  call dump (scr2, 'smooth.dat', 2)

  if (do_trace.and.master) print *,'calling smooth3max3_set'
  call smooth3max3_set (scr4, scr3)
  call dump (scr3, 'smooth.dat', 3)

  if (do_trace.and.master) print *,'calling smooth3_set'
  call smooth3_set (scr2, scr1)
  call dump (scr1, 'smooth.dat', 4)

!$omp parallel private(iz), shared(fmax1)
  do iz=izs,ize
    scr2(:,:,iz) = abs(scr1(:,:,iz)-scr4(:,:,iz))
  end do
  call fmaxval_omp ('scr2',scr2,fmax1)
!$omp end parallel

  fmax2 = fmaxval('scr2',scr2)
  if (master .and. debug(dbg_stagger)) print *,'test_smooth_max:', fmax1, fmax2

  if (fmax1-fmax2 .gt. 1e-5) then
    i = maxloc(scr2(:,lb:ub,:))
    i(2) = i(2)+lb-1
    if (master) then
      print *,'ERROR: results from smooth3(max3()) and smooth3max3_set differ', fmax1, fmax2
      print *, i, scr1(i(1),i(2),i(3)), scr4(i(1),i(2),i(3))
    end if
    call end_mpi
    stop
  else if (master) then
    print *,'test_smooth_max: OK', fmax1, fmax2
  end if
END

!***********************************************************************
SUBROUTINE test_del2
!
! Test some of the stagger routines.
!
!-----------------------------------------------------------------------
  USE params
  USE arrays, ONLY: scr1, scr2, scr3, scr4, scr5
  implicit none
  real:: ax, ay, az, fx1, fx2, fy1, fy2, fz1, fz2, fmax, eps
  real xt(mx), yt(my), zt(mz)
  integer:: ix, iy, iz, i(3), rank, nx, ny, nz, jx, jy, jz
  logical debug

  ax = 1.
  ay = 1.
  az = 1.

  nx = mxtot
  ny = mytot
  nz = mztot

  jx = ixoff
  jy = iyoff
  jz = izoff

  xt = (/(ix+jx+0.5,ix=1,mx)/)-1
  yt = (/(iy+jy+0.5,iy=1,my)/)-1
  zt = (/(iz+jz+0.5,iz=1,mz)/)-1

!$omp parallel private(ix,iy,iz,fx1,fx2,fy1,fy2,fz1,fz2)
  do iz=izs,ize
  do iy=1,my
    do ix=1,mx
      scr1(ix,iy,iz) = ax*xt(ix)
      scr2(ix,iy,iz) = ay*yt(iy)
      scr3(ix,iy,iz) = az*zt(iz)
      fx1 = mod(xt(ix)+1.+nx,real(nx))
      fx2 = mod(xt(ix)-1.+nx,real(nx))
      fy1 = mod(yt(iy)+1.+ny,real(ny))
      fy2 = mod(yt(iy)-1.+ny,real(ny))
      fz1 = mod(zt(iz)+1.+nz,real(nz))
      fz2 = mod(zt(iz)-1.+nz,real(nz))
      scr4(ix,iy,iz) = max (0.0, 0.5*(ax*(fx2-fx1)/dxm(ix)+ay*(fy2-fy1)/dym(iy)+az*(fz2-fz1)/dzm(iz)))
    end do
  end do
  end do

  call dif2_set (scr1, scr2, scr3, scr5)

  do iz=izs,ize
  do iy=1,my
  do ix=1,mx
    scr1(ix,iy,iz) = abs(scr4(ix,iy,iz)-scr5(ix,iy,iz))
  end do
  end do
  end do
  call fmaxval_subr ('scr1',scr1, fmax)
!$omp end parallel

  eps = 1e-5*max(nx,ny,nz)/min(minval(dxm),minval(dym),minval(dzm))
  if (fmax > eps) then
    i = maxloc(scr1(:,lb:ub,:))
    i(2) = i(2)+lb-1
    do rank=0,mpi_size-1
      call flush_mpi
      if (rank == mpi_rank .and. scr1(i(1),i(2),i(3)) > eps) then
        print '(1x,a,4i5,7g14.6)','test_del2: dif too large at', mpi_rank, i, fmax, &
	  scr1(i(1),i(2),i(3)), scr4(i(1),i(2),i(3)), scr5(i(1),i(2),i(3)),dxm(i(1)),dym(i(2)),dzm(i(3))
        if (debug(dbg_stagger)) print '(i5,16f6.1/(5x,16f6.1))', mpi_rank, scr4(i(1),:,i(3))
        if (debug(dbg_stagger)) print '(i5,16f6.1/(5x,16f6.1))', mpi_rank, scr5(i(1),:,i(3))
        if (debug(dbg_stagger)) print '(i5,16f6.1/(5x,16f6.1))', mpi_rank, scr4(i(1),i(2),:)
        if (debug(dbg_stagger)) print '(i5,16f6.1/(5x,16f6.1))', mpi_rank, scr5(i(1),i(2),:)
        if (debug(dbg_stagger)) print '(i5,16f6.1/(5x,16f6.1))', mpi_rank, dym
        if (debug(dbg_stagger)) print '(i5,16f6.1/(5x,16f6.1))', mpi_rank, dzm
      end if
    end do
    call end_mpi
    stop
  else if (master) then
    print *,'test_del2: OK', fmax, eps
  end if

END

!***********************************************************************
SUBROUTINE test_divcurl
!
! Test that div(curl()) = 0
!
!-----------------------------------------------------------------------
  USE params
  USE arrays, only: scr1, scr2, scr3, scr4, scr5, scr6
  implicit none
  real:: kx,ky,kz,err,eps,fmaxval
  integer:: ix,iy,iz,i(3)
  logical debug

  kx = 2.*pi/sx
  ky = 2.*pi/sy
  kz = 2.*pi/sz

!$omp parallel private(ix,iy,iz)
  do iz=izs,ize
  do iy=1,my
  do ix=1,mx
    scr1(ix,iy,iz) = cos(2.*kx*xm  (ix)+4.*ky*ymdn(iy)+3.*kz*zmdn(iz))
    scr2(ix,iy,iz) = cos(3.*kx*xmdn(ix)+3.*ky*ym  (iy)+5.*kz*zmdn(iz))
    scr3(ix,iy,iz) = cos(4.*kx*xmdn(ix)+2.*ky*ymdn(iy)+4.*kz*zm  (iz))
  end do
  end do
  end do
!$omp barrier
  call ddxup_set(scr2,scr6)
  call ddyup_sub(scr1,scr6)
  call ddyup_set(scr3,scr4)
!$omp barrier                           ! scr2 and scr6 need to be consistent
  call ddzup_sub(scr2,scr4)
  call ddzup_set(scr1,scr5)
  call ddxup_sub(scr3,scr5)

  call ddxup_set(scr4,scr3)
  call ddyup_add(scr5,scr3)
  call ddzup_add(scr6,scr3)

  do iz=izs,ize
  do iy=1,my
  do ix=1,mx
    scr3(ix,iy,iz) = abs(scr3(ix,iy,iz))
  end do
  end do
  end do
  err = fmaxval('divcurl',scr3)
!$omp end parallel

  eps = maxval(max(4e-4*(sy/dym(lb:ub))/100.,2e-5*(64./(sy/dym(lb:ub)))**6))
  eps = max(eps,4e-4*(sx/dx)/100.,2e-5*(64./(sx/dx))**6)
  eps = max(eps,4e-4*(sz/dz)/100.,2e-5*(64./(sz/dz))**6)
  eps = eps*0.1*(kx**2+ky**2+kz**2)
  if (err > eps) then
    i = maxloc(scr3(:,lb:ub,:))
    i(2) = i(2)+lb-1
    print *,'ERROR: div(curl()) too large at',mpi_rank,i,err,eps
    print '(a,2i5/(10g12.5))','x-dir',i(2),i(3),scr3(:,i(2),i(3))
    print '(a,2i5/(10g12.5))','y-dir',i(1),i(3),scr3(i(1),:,i(3))
    print '(a,2i5/(10g12.5))','z-dir',i(1),i(2),scr3(i(1),i(2),:)
    call end_mpi
    stop
  else if (master) then
    print *,'test_divcurl: OK',err,eps
  end if

END
