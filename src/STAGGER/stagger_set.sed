# $Id: stagger_set.sed,v 1.21 2015/11/01 21:33:45 aake Exp $
/^ *FUNCTION/{
s/FUNCTION \([a-z1-9]*\)/SUBROUTINE \1_set/
s/result *(.*//
s/RESULT *(.*//
s/(\(f.*\))/(\1, fp)/
}
/^ *END/s/FUNCTION/SUBROUTINE/
/real,.*d*[xyz]*up[i123]*$/s/d*[xyz]*up[i123]*$/fp/
/::/s/\(d*[xyz]*up[i123]*\)/fp/
/::/s/\(d*[xyz]*dn[i123]*\)/fp/
s/d*[xyz]*up[i123]* *= */fp = /
s/d*[xyz]*dn[i123]* *= */fp = /
s/ d*[xyz]*up[i123]* *(/ fp(/
s/ d*[xyz]*dn[i123]* *(/ fp(/
/real,.*smooth3th* *$/s/smooth3th* *$/fp/
/real,.*smooth[35][tz]* *$/s/smooth[35][tz]* *$/fp/
/real,.*max[35] *$/s/max[35] *$/fp/
/real,.*dif[12]*$/s/dif[12][a-z]* *$/fp/
/real,.*dif[12]*d$/s/dif[12][a-z]* *$/fp/
/real,.*d2abs$/s/d2abs$/fp/
/real,.*laplace*$/s/laplace[a-z]* *$/fp/
/::/s/smooth[35]]tz]*/fp/
/::/s/max[35]/fp/
/::/s/dif[12][a-z]*/fp/
/::/s/d2abs/fp/
/::/s/laplace[a-z]*/fp/
/smooth3max5.*=/s/smooth3max5/fp/g
/smooth3th.*=/s/smooth3th*/fp/g
/smooth[35].*=/s/smooth[35][tz]*/fp/g
/max[35].*=/s/max[35]/fp/g
/dif[12].*=/s/dif[12][a-z]*/fp/g
/d2abs.*=/s/d2abs/fp/g
/laplace.*=/s/laplace[a-z]*/fp/g
/omp_in_parallel.*WARNING:/d
