! $Id: stagger_do.f90,v 1.3 2001/12/05 21:54:00 aake Exp &
MODULE stagger
!***********************************************************************
!
!  6th order staggered derivatives, 5th order interpolation. This version
!  uses explicit loops, for increased speed and parallelization.
!
!***********************************************************************

CONTAINS

