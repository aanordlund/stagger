!***********************************************************************
FUNCTION zup (f)
!
!  f is centered on (i,j,k-.5), fifth order stagger
!
  use params
  implicit none
  real a, b, c
  integer i, j, k
  real, intent(in) , dimension(mx,my,mz):: f
  real,              dimension(mx,my,mz):: zup
  real,              dimension(mx,my, 2):: g
  real,              dimension(mx,my, 3):: h
!-----------------------------------------------------------------------
!
  character(len=mid):: id = "$Id: z_maron.f90,v 1.2 2012/12/31 16:10:47 aake Exp $" 
  call print_id (id)

  if (mz .lt. 5) then
    !$omp parallel private(k)
    do k=izs,ize
      zup(:,:,k) = f(:,:,k)
    end do
    !$omp end parallel
    return
  end if
  c =  0.2214
  b = -0.12312
  a = .5-b-c

  call mpi_send_z (f, g, 2, h, 3)
!
  !$omp parallel private(i,j,k)
  do k=max(3,izs),min(mz-3,ize)
    do j=1,my
      do i=1,mx
        zup(i,j,k) = ( &
                     c*(f(i,j,k-2 )+f(i,j,k+3 )) + &
                     b*(f(i,j,k-1 )+f(i,j,k+2 )) + &
                     a*(f(i,j,k   )+f(i,j,k+1 )))
     end do
    end do
  end do
  if (ize.eq.mz) then
   do j=1,my
    do i=1,mx
      zup(i,j,mz-2) = ( &
                     c*(f(i,j,mz-4)+h(i,j,1   )) + &
                     b*(f(i,j,mz-3)+f(i,j,mz  )) + &
                     a*(f(i,j,mz-2)+f(i,j,mz-1)))
      zup(i,j,mz-1) = ( &
                     c*(f(i,j,mz-3)+h(i,j,2   )) + &
                     b*(f(i,j,mz-2)+h(i,j,1   )) + &
                     a*(f(i,j,mz-1)+f(i,j,mz  )))
      zup(i,j,mz  ) = ( &
                     c*(f(i,j,mz-2)+h(i,j,3   )) + &
                     b*(f(i,j,mz-1)+h(i,j,2   )) + &
                     a*(f(i,j,mz  )+h(i,j,1   )))
    end do
   end do
  end if
  if (izs.eq.1) then
   do j=1,my
    do i=1,mx
      zup(i,j,1   ) = ( &
                     c*(g(i,j,1   )+f(i,j,4   )) + &
                     b*(g(i,j,2   )+f(i,j,3   )) + &
                     a*(f(i,j,1   )+f(i,j,2   )))
      zup(i,j,2   ) = ( &
                     c*(g(i,j,2   )+f(i,j,5   )) + &
                     b*(f(i,j,1   )+f(i,j,4   )) + &
                     a*(f(i,j,2   )+f(i,j,3   )))
    end do
   end do
  end if
  !$omp end parallel
!
  nstag = nstag+1
  nflop = nflop+8
END FUNCTION

!***********************************************************************
FUNCTION zup1 (f)
!
!  f is centered on (i,j,k-.5), fifth order stagger
!
  use params
  implicit none
  real a, b, c
  integer i, j, k
  real, intent(in) , dimension(mx,my,mz):: f
  real,              dimension(mx,my,mz):: zup1
  real,              dimension(mx,my,1 ):: h
!-----------------------------------------------------------------------
!
  if (mz .lt. 5) then
    !$omp parallel private(k)
    do k=izs,ize
      zup1(:,:,k) = f(:,:,k)
    end do
    !$omp end parallel
    return
  end if
!
  a = 0.5
  call mpi_send_z (f, h, 0, h, 1)
!
  !$omp parallel private(i,j,k)
  do k=izs,min(mz-1,ize)
    do j=1,my
      do i=1,mx
        zup1(i,j,k  ) = ( &
                     a*(f(i,j,k   )+f(i,j,k+1 )))
      end do
    end do
  end do
  if (ize.eq.mz) then
   do j=1,my
    do i=1,mx
      zup1(i,j,mz) = ( &
                     a*(h(i,j,1)+f(i,j,mz)))
    end do
   end do
  end if
  !$omp end parallel
!
  nstag1 = nstag1+1
  nflop = nflop+2
END FUNCTION

!***********************************************************************
FUNCTION zdn (f)
!
!  f is centered on (i,j), fifth order stagger
!
  use params
  implicit none
  real a, b, c
  integer i, j, k
  real, dimension(mx,my,mz):: f, zdn
  real, dimension(mx,my, 3):: g
  real, dimension(mx,my, 2):: h
!-----------------------------------------------------------------------
!
  if (mz .lt. 5) then
    !$omp parallel private(k)
    do k=izs,ize
      zdn(:,:,k) = f(:,:,k)
    end do
    !$omp end parallel
    return
  end if
  c =  0.2214
  b = -0.12312
  a = .5-b-c

  call mpi_send_z (f, g, 3, h, 2)
!
  !$omp parallel private(i,j,k)
  do k=max(4,izs),min(mz-2,ize)
    do j=1,my
     do i=1,mx
      zdn(i,j,k) = ( &
                     c*(f(i,j,k+2 )+f(i,j,k-3 )) + &
                     b*(f(i,j,k+1 )+f(i,j,k-2 )) + &
                     a*(f(i,j,k   )+f(i,j,k-1 )))
     end do
    end do
  end do
  if (ize.eq.mz) then
   do j=1,my
    do i=1,mx
      zdn(i,j,mz-1) = ( &
                     c*(f(i,j,mz-4)+h(i,j,1   )) + &
                     b*(f(i,j,mz-3)+f(i,j,mz  )) + &
                     a*(f(i,j,mz-2)+f(i,j,mz-1)))
      zdn(i,j,mz  ) = ( &
                     c*(f(i,j,mz-3)+h(i,j,2   )) + &
                     b*(f(i,j,mz-2)+h(i,j,1   )) + &
                     a*(f(i,j,mz-1)+f(i,j,mz  )))
    end do
   end do
  end if
  if (izs.eq.1) then
   do j=1,my
    do i=1,mx
      zdn(i,j,1   ) = ( &
                     c*(g(i,j,1   )+f(i,j,3   )) + &
                     b*(g(i,j,2   )+f(i,j,2   )) + &
                     a*(g(i,j,3   )+f(i,j,1   )))
      zdn(i,j,2   ) = ( &
                     c*(g(i,j,2   )+f(i,j,4   )) + &
                     b*(g(i,j,3   )+f(i,j,3   )) + &
                     a*(f(i,j,1   )+f(i,j,2   )))
      zdn(i,j,3   ) = ( &
                     c*(g(i,j,3   )+f(i,j,5   )) + &
                     b*(f(i,j,1   )+f(i,j,4   )) + &
                     a*(f(i,j,2   )+f(i,j,3   )))
    end do
   end do
  end if
  !$omp end parallel
!
  nstag = nstag+1
  nflop = nflop+8
END FUNCTION

!***********************************************************************
FUNCTION zdn1 (f)
!
!  f is centered on (i,j), fifth order stagger
!
  use params
  implicit none
  real a, b, c
  integer i, j, k
  real, dimension(mx,my,mz):: f, zdn1
  real, dimension(mx,my,1 ):: g
!-----------------------------------------------------------------------
!
  if (mz .lt. 5) then
    !$omp parallel private(k)
    do k=izs,ize
      zdn1(:,:,k) = f(:,:,k)
    end do
    !$omp end parallel
    return
  end if
!
  a = 0.5
  call mpi_send_z (f, g, 1, g, 0)
!
  !$omp parallel private(i,j,k)
  do k=max(2,izs),ize
    do j=1,my
     do i=1,mx
      zdn1(i,j,k) = ( &
                     a*(f(i,j,k   )+f(i,j,k-1 )))
     end do
    end do
  end do
  if (izs.eq.1) then
   do j=1,my
    do i=1,mx
      zdn1(i,j,1   ) = ( &
                     a*(g(i,j,1   )+f(i,j,1   )))
    end do
   end do
  end if
  !$omp end parallel
!
  nstag1 = nstag1+1
  nflop = nflop+2
END FUNCTION

!***********************************************************************
FUNCTION zup32 (f,j)
!
!  f is centered on (i,j,k-.5), fifth order stagger
!
  use params
  implicit none
  real a, b, c
  integer i, j, k
  real,             dimension(mx,   mz):: zup32
  real, intent(in), dimension(mx,my,mz):: f
  real,             dimension(mx,my, 2):: g
  real,             dimension(mx,my, 3):: h
!-----------------------------------------------------------------------
!
  character(len=mid):: id = "$Id: z_maron.f90,v 1.2 2012/12/31 16:10:47 aake Exp $" 
  call print_id (id)
!
  if (mz.le.5) then
    !$omp parallel private(k)
    do k=izs,ize
      zup32(:,k) = f(:,j,k)
    end do
    !$omp end parallel
    return
  end if

  c =  0.2214
  b = -0.12312
  a = .5-b-c

  call mpi_send_z (f, g, 2, h, 3)
!
!$omp parallel private(i,j,k)
  do k=max(3,izs),min(mz-3,ize)
      do i=1,mx
        zup32(i,k) = ( &
                     c*(f(i,j,k+3)+f(i,j,k-2)) + &
                     b*(f(i,j,k+2)+f(i,j,k-1)) + &
                     a*(f(i,j,k+1)+f(i,j,k  )))
      end do
  end do
  if (izs.eq.1) then
    do i=1,mx
      zup32(i,1   ) = ( &
                     c*(g(i,j,1   )+f(i,j,4   )) + &
                     b*(g(i,j,2   )+f(i,j,3   )) + &
                     a*(f(i,j,1   )+f(i,j,2   )))
      zup32(i,2   ) = ( &
                     c*(g(i,j,2   )+f(i,j,5   )) + &
                     b*(f(i,j,1   )+f(i,j,4   )) + &
                     a*(f(i,j,2   )+f(i,j,3   )))
    end do
  end if
  if (ize.eq.mz) then
    do i=1,mx
      zup32(i,mz-2) = ( &
                     c*(f(i,j,mz-4)+h(i,j,1   )) + &
                     b*(f(i,j,mz-3)+f(i,j,mz  )) + &
                     a*(f(i,j,mz-2)+f(i,j,mz-1)))
      zup32(i,mz-1) = ( &
                     c*(f(i,j,mz-3)+h(i,j,2   )) + &
                     b*(f(i,j,mz-2)+h(i,j,1   )) + &
                     a*(f(i,j,mz-1)+f(i,j,mz  )))
      zup32(i,mz  ) = ( &
                     c*(f(i,j,mz-2)+h(i,j,3   )) + &
                     b*(f(i,j,mz-1)+h(i,j,2   )) + &
                     a*(f(i,j,mz  )+h(i,j,1   )))
    end do
  end if

!$omp end parallel
!
  if (omp_master) then
    nstag = nstag+1
    nflop = nflop+8
  end if
END FUNCTION

!***********************************************************************
FUNCTION zup132 (f,j)
!
!  f is centered on (i,j,k-.5), fifth order stagger
!
  use params
  implicit none
  real a, b, c
  integer i, j, k
  real,              dimension(mx,   mz):: zup132
  real, intent(in),  dimension(mx,my,mz):: f
  real,              dimension(mx,my,1 ):: h
!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    !$omp parallel private(k)
    do k=izs,ize
      zup132(:,k) = f(:,j,k)
    end do
    !$omp end parallel
    return
  end if

  a = .5
  call mpi_send_z (f, h, 0, h, 1)
!
!$omp parallel private(i,j,k)
  do k=izs,min(mz-1,ize)
      do i=1,mx
        zup132(i,k) = ( &
                     a*(f(i,j,k+1)+f(i,j,k  )))
      end do
  end do
  if (ize.eq.mz) then
    do i=1,mx
      zup132(i,mz) = ( &
                     a*(h(i,j,1)+f(i,j,mz)))
    end do
  end if

!$omp end parallel
!
  if (omp_master) then
    nstag1 = nstag1+1
    nflop = nflop+2
  end if
END FUNCTION

!***********************************************************************
FUNCTION zdn32 (f,j)
!
!  f is centered on (i,j,k), fifth order stagger
!
  use params
  implicit none
  real a, b, c
  integer i, j, k
  real,             dimension(mx,   mz):: zdn32
  real, intent(in), dimension(mx,my,mz):: f
  real,             dimension(mx,my, 3):: g
  real,             dimension(mx,my, 2):: h

!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    !$omp parallel private(k)
    do k=izs,ize
      zdn32(:,k) = f(:,j,k)
    end do
    !$omp end parallel
    return
  end if

  c =  0.2214
  b = -0.12312
  a = .5-b-c

  call mpi_send_z (f, g, 3, h, 2)
!
!$omp parallel private(i,j,k)
  do k=max(4,izs),min(mz-2,ize)
      do i=1,mx
        zdn32(i,k) = ( &
                     c*(f(i,j,k+2)+f(i,j,k-3)) + &
                     b*(f(i,j,k+1)+f(i,j,k-2)) + &
                     a*(f(i,j,k  )+f(i,j,k-1)))
      end do
  end do
  if (izs.eq.1) then
    do i=1,mx
      zdn32(i,1   ) = ( &
                     c*(g(i,j,1   )+f(i,j,3   )) + &
                     b*(g(i,j,2   )+f(i,j,2   )) + &
                     a*(g(i,j,3   )+f(i,j,1   )))
      zdn32(i,2   ) = ( &
                     c*(g(i,j,2   )+f(i,j,4   )) + &
                     b*(g(i,j,3   )+f(i,j,3   )) + &
                     a*(f(i,j,1   )+f(i,j,2   )))
      zdn32(i,3   ) = ( &
                     c*(g(i,j,3   )+f(i,j,5   )) + &
                     b*(f(i,j,1   )+f(i,j,4   )) + &
                     a*(f(i,j,2   )+f(i,j,3   )))
    end do
  end if
  if (ize.eq.mz) then
    do i=1,mx
      zdn32(i,mz-1) = ( &
                     c*(f(i,j,mz-4)+h(i,j,1   )) + &
                     b*(f(i,j,mz-3)+f(i,j,mz  )) + &
                     a*(f(i,j,mz-2)+f(i,j,mz-1)))
      zdn32(i,mz  ) = ( &
                     c*(f(i,j,mz-3)+h(i,j,2   )) + &
                     b*(f(i,j,mz-2)+h(i,j,1   )) + &
                     a*(f(i,j,mz-1)+f(i,j,mz  )))
    end do
  end if

!$omp end parallel
!
  if (omp_master) then
    nstag = nstag+1
    nflop = nflop+8
  end if
END FUNCTION

!***********************************************************************
FUNCTION zdn132 (f,j)
!
!  f is centered on (i,j,k), fifth order stagger
!
  use params
  implicit none
  real a, b, c
  integer i, j, k
  real,             dimension(mx,   mz):: zdn132
  real, intent(in), dimension(mx,my,mz):: f
  real,             dimension(mx,my,1 ):: g
!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    !$omp parallel private(k)
    do k=izs,ize
      zdn132(:,k) = f(:,j,k)
    end do
    !$omp end parallel
    return
  end if

  a = .5
  call mpi_send_z (f, g, 1, g, 0)
!
!$omp parallel private(i,j,k)
  do k=max(2,izs),ize
      do i=1,mx
        zdn132(i,k) = ( &
                     a*(f(i,j,k  )+f(i,j,k-1)))
      end do
  end do
  if (izs.eq.1) then
    do i=1,mx
      zdn132(i,1   ) = ( &
                     a*(g(i,j,1   )+f(i,j,1   )))
    end do
  end if

!$omp end parallel
!
  if (omp_master) then
    nstag1 = nstag1+1
    nflop = nflop+2
  end if
END FUNCTION

!***********************************************************************
FUNCTION zup22 (f)
!
!  f is centered on (i,k-.5), fifth order stagger
!
  use params
  implicit none
  real a, b, c
  integer i, j, k
  real,             dimension(mx,  mz):: zup22
  real, intent(in), dimension(mx,  mz):: f
  real,             dimension(mx,1, 2):: g
  real,             dimension(mx,1, 3):: h

!-----------------------------------------------------------------------
!
  character(len=mid):: id = "$Id: z_maron.f90,v 1.2 2012/12/31 16:10:47 aake Exp $" 
  call print_id (id)
!
  if (mz.le.5) then
    !$omp parallel private(k)
    do k=izs,ize
      zup22(:,k) = f(:,k)
    end do
    !$omp end parallel
    return
  end if
  c =  0.2214
  b = -0.12312
  a = .5-b-c

  call mpi_send2_z (f, mx, 1, my, g, 2, h, 3)
!
!$omp parallel private(i,k)
  do k=max(3,izs),min(mz-3,ize)
      do i=1,mx
        zup22(i,k) = ( &
                     c*(f(i,k+3)+f(i,k-2)) + &
                     b*(f(i,k+2)+f(i,k-1)) + &
                     a*(f(i,k+1)+f(i,k  )))
      end do
  end do
  if (izs.eq.1) then
    do i=1,mx
      zup32(i,1   ) = ( &
                     c*(g(i,1,1 )+f(i,4 )) + &
                     b*(g(i,1,2 )+f(i,3 )) + &
                     a*(f(i,1   )+f(i,2 )))
      zup32(i,2   ) = ( &
                     c*(g(i,1,2 )+f(i,5 )) + &
                     b*(f(i,1   )+f(i,4 )) + &
                     a*(f(i,2   )+f(i,3 )))
    end do
  end if
  if (ize.eq.mz) then
    do i=1,mx
      zup32(i,mz-2) = ( &
                     c*(f(i,mz-4)+h(i,1,1 )) + &
                     b*(f(i,mz-3)+f(i,mz  )) + &
                     a*(f(i,mz-2)+f(i,mz-1)))
      zup32(i,mz-1) = ( &
                     c*(f(i,mz-3)+h(i,1,2 )) + &
                     b*(f(i,mz-2)+h(i,1,1 )) + &
                     a*(f(i,mz-1)+f(i,mz  )))
      zup32(i,mz  ) = ( &
                     c*(f(i,mz-2)+h(i,1,3 )) + &
                     b*(f(i,mz-1)+h(i,1,2 )) + &
                     a*(f(i,mz  )+h(i,1,1 )))
    end do
  end if

!$omp end parallel
!
  if (omp_master) then
    nstag = nstag+1
    nflop = nflop+8
  end if
END FUNCTION

!***********************************************************************
FUNCTION zup122 (f)
!
!  f is centered on (i,k-.5), fifth order stagger
!
  use params
  implicit none
  real a, b, c
  integer i, j, k
  real,             dimension(mx,   mz):: zup122
  real, intent(in), dimension(mx,   mz):: f
  real,             dimension(mx,my,1 ):: h
!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    !$omp parallel private(k)
    do k=izs,ize
      zup122(:,k) = f(:,k)
    end do
    !$omp end parallel
    return
  end if

  a = .5
  call mpi_send2_z (f, mx, 1, mz, h, 0, h, 1)
!
!$omp parallel private(i,k)
  do k=izs,ize
      do i=1,mx
        zup122(i,k) = ( &
                     a*(f(i,k+1)+f(i,k  )))
      end do
  end do
  if (ize.eq.mz) then
    do i=1,mx
      zup122(i,mz) = ( &
                     a*(h(i,1,1)+f(i,mz)))
    end do
  end if

!$omp end parallel
!
  if (omp_master) then
    nstag1 = nstag1+1
    nflop = nflop+2
  end if
END FUNCTION

!***********************************************************************
FUNCTION zdn22 (f)
!
!  f is centered on (i,k), fifth order stagger
!
  use params
  implicit none
  real a, b, c
  integer i, j, k
  real,             dimension(mx,   mz):: zdn22
  real, intent(in), dimension(mx,   mz):: f
  real,             dimension(mx,1, 3 ):: g
  real,             dimension(mx,1, 2 ):: h

!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    !$omp parallel private(k)
    do k=izs,ize
      zdn22(:,k) = f(:,k)
    end do
    !$omp end parallel
    return
  end if
  c =  0.2214
  b = -0.12312
  a = .5-b-c
  call mpi_send2_z (f, mx, 1, mz, g, 3, h, 2)
!
!$omp parallel private(i,k)
  do k=max(4,izs),min(mz-2,ize)
      do i=1,mx
        zdn22(i,k) = ( &
                     c*(f(i,k+2)+f(i,k-3)) + &
                     b*(f(i,k+1)+f(i,k-2)) + &
                     a*(f(i,k  )+f(i,k-1)))
      end do
  end do
  if (izs.eq.1) then
    do i=1,mx
      zdn22(i,1   ) = ( &
                     c*(g(i,1,1 )+f(i,3   )) + &
                     b*(g(i,1,2 )+f(i,2   )) + &
                     a*(g(i,1,3 )+f(i,1   )))
      zdn22(i,2   ) = ( &
                     c*(g(i,1,2 )+f(i,4   )) + &
                     b*(g(i,1,3 )+f(i,3   )) + &
                     a*(f(i,1   )+f(i,2   )))
      zdn22(i,3   ) = ( &
                     c*(g(i,1,3 )+f(i,5   )) + &
                     b*(f(i,1   )+f(i,4   )) + &
                     a*(f(i,2   )+f(i,3   )))
    end do
  end if
  if (ize.eq.mz) then
    do i=1,mx
      zdn22(i,mz-1) = ( &
                     c*(f(i,mz-4)+h(i,1,1 )) + &
                     b*(f(i,mz-3)+f(i,mz  )) + &
                     a*(f(i,mz-2)+f(i,mz-1)))
      zdn22(i,mz  ) = ( &
                     c*(f(i,mz-3)+h(i,1,2 )) + &
                     b*(f(i,mz-2)+h(i,1,1 )) + &
                     a*(f(i,mz-1)+f(i,mz  )))
    end do
  end if

!$omp end parallel
!
  if (omp_master) then
    nstag = nstag+1
    nflop = nflop+8
  end if
END FUNCTION

!***********************************************************************
FUNCTION zdn122 (f)
!
!  f is centered on (i,k), fifth order stagger
!
  use params
  implicit none
  real a, b, c
  integer i, j, k
  real,             dimension(mx,   mz):: zdn122
  real, intent(in), dimension(mx,   mz):: f
  real,             dimension(mx,1 ,1 ):: g
!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    !$omp parallel private(k)
    do k=izs,ize
      zdn122(:,k) = f(:,k)
    end do
    !$omp end parallel
    return
  end if

  a = .5
  call mpi_send2_z (f, mx, 1, mz, g, 1, g, 0)
!
!$omp parallel private(i,k)
  do k=max(2,izs),ize
      do i=1,mx
        zdn122(i,k) = ( &
                     a*(f(i,k  )+f(i,k-1)))
      end do
  end do
  if (izs.eq.1) then
    do i=1,mx
      zdn122(i,1   ) = ( &
                     a*(g(i,1,1  )+f(i,1  )))
    end do
  end if
 
!$omp end parallel
!
  if (omp_master) then
    nstag1 = nstag1+1
    nflop = nflop+2
  end if
END FUNCTION
