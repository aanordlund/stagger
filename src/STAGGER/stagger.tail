!-----------------------------------------------------------------------

!***********************************************************************
SUBROUTINE dif2_set (fx, fy, fz, difu)
!
!  Finite difference one zone left to one zones right.
!

  USE params
  real cx, cy, cz
  real, dimension(mx,my,mz):: fx, fy, fz, difu
!hpf$ align with TheCube:: fx, fy, fz, difu
!-----------------------------------------------------------------------

  cx=0.5/dx
  cy=0.5/dy
  cz=0.5/dz

!$omp parallel private(i,j,k)
  if (mx.gt.1) then
    do k=izs,ize
      do j=1,my
        do i=2,mx-1
          difu(i,j,k) = cx*(fx(i-1 ,j,k)-fx(i+1,j,k))
        end do
        difu(1 ,j,k)  = cx*(fx(mx  ,j,k)-fx(2  ,j,k))
        difu(mx,j,k)  = cx*(fx(mx-1,j,k)-fx(1  ,j,k))
      end do
    end do
  else
    do k=izs,ize
      do j=1,my
        do i=1,mx
          difu(i,j,k) = 0.
        end do
      end do
    end do
  end if

 if (my .gt. 1) then
  do k=izs,ize
    do j=2,my-1
      do i=1,mx
        difu(i,j, k) = difu(i,j, k) + cy*(fy(i,j -1,k)-fy(i,j+1,k))
      end do
    end do
    do i=1,mx
        difu(i,1 ,k) = difu(i,1 ,k) + cy*(fy(i,my  ,k)-fy(i,  2,k))
        difu(i,my,k) = difu(i,my,k) + cy*(fy(i,my-1,k)-fy(i,  1,k))
    end do
  end do
 end if
!$omp end parallel

 if (mz .gt. 1) then
  do j=1,my
    do i=1,mx
        difu(i,j,1 ) = difu(i,j,1 ) + cz*(fz(i,j,mz  )-fz(i,  j,2))
        difu(i,j,mz) = difu(i,j,mz) + cz*(fz(i,j,mz-1)-fz(i,  j,1))
    end do
  end do
!$omp parallel private(i,j,k)
  do k=max(izs,2),min(ize,mz-1)
    do j=1,my
      do i=1,mx
        difu(i,j,k ) = difu(i,j,k ) + cz*(fz(i,j,k -1)-fz(i,j,k+1))
      end do
    end do
  end do
!$omp end parallel
 end if
END subroutine

!***********************************************************************
SUBROUTINE dif2_set_omp (fx, fy, fz, difu)
!
!  Finite difference one zone left to one zones right.
!

  USE params
  real cx, cy, cz
  real, dimension(mx,my,mz):: fx, fy, fz, difu
!hpf$ align with TheCube:: fx, fy, fz, difu
!-----------------------------------------------------------------------

  cx=0.5/dx
  cy=0.5/dy
  cz=0.5/dz

  if (mx.gt.1) then
    do k=izs,ize
      do j=1,my
        do i=2,mx-1
          difu(i,j,k) = cx*(fx(i-1 ,j,k)-fx(i+1,j,k))
        end do
        difu(1 ,j,k)  = cx*(fx(mx  ,j,k)-fx(2  ,j,k))
        difu(mx,j,k)  = cx*(fx(mx-1,j,k)-fx(1  ,j,k))
      end do
    end do
  else
    do k=izs,ize
      do j=1,my
        do i=1,mx
          difu(i,j,k) = 0.
        end do
      end do
    end do
  end if

 if (my .gt. 1) then
  do k=izs,ize
    do j=2,my-1
      do i=1,mx
        difu(i,j, k) = difu(i,j, k) + cy*(fy(i,j -1,k)-fy(i,j+1,k))
      end do
    end do
    do i=1,mx
        difu(i,1 ,k) = difu(i,1 ,k) + cy*(fy(i,my  ,k)-fy(i,  2,k))
        difu(i,my,k) = difu(i,my,k) + cy*(fy(i,my-1,k)-fy(i,  1,k))
    end do
  end do
 end if

 if (mz .gt. 1) then
!$omp barrier
!$omp master
  do j=1,my
    do i=1,mx
        difu(i,j,1 ) = difu(i,j,1 ) + cz*(fz(i,j,mz  )-fz(i,  j,2))
        difu(i,j,mz) = difu(i,j,mz) + cz*(fz(i,j,mz-1)-fz(i,  j,1))
    end do
  end do
!$omp end master
!$omp barrier

  do k=max(izs,2),min(ize,mz-1)
    do j=1,my
      do i=1,mx
        difu(i,j,k ) = difu(i,j,k ) + cz*(fz(i,j,k -1)-fz(i,j,k+1))
      end do
    end do
  end do
 end if
END subroutine

END MODULE
