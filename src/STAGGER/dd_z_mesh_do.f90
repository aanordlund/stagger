!***********************************************************************
FUNCTION ddzup (f)
  use params
  implicit none
  real a, b, c, a1st
  integer i, j, k, km2, km1, kp1, kp2, kp3
  real, dimension(mx,my,mz):: f, ddzup
!hpf$ distribute(*,*,block):: f, ddzup
!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    ddzup=0.
    return
  endif
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)
  a1st = 1.
!
!Xomp barrier
!$omp parallel private(i,j,k,km2,km1,kp1,kp2,kp3)
  do k=izs,ize
    km2=mod(k-3+mz,mz)+1
    km1=mod(k-2+mz,mz)+1
    kp1=mod(k     ,mz)+1
    kp2=mod(k+1   ,mz)+1
    kp3=mod(k+2   ,mz)+1
    do j=1,my
      do i=1,mx
        ddzup(i,j,k) = dzidzup(k)*( &
                     c*(f(i,j,kp3)-f(i,j,km2)) + &
                     b*(f(i,j,kp2)-f(i,j,km1)) + &
                     a*(f(i,j,kp1)-f(i,j,k  )))
      end do
    end do
  end do
!$omp end parallel
!
  if (omp_master) then
    nstag = nstag+1
    nflop = nflop+8
  end if
END FUNCTION

!***********************************************************************
FUNCTION ddzup1 (f)
  use params
  implicit none
  real a, b, c, a1st
  integer i, j, k, kp1
  real, dimension(mx,my,mz):: f, ddzup1
!hpf$ distribute(*,*,block):: f, ddzup1
!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    ddzup1=0.
    return
  endif
  a = 1.
!
!Xomp barrier
!$omp parallel private(i,j,k,kp1)
  do k=izs,ize
    kp1=mod(k     ,mz)+1
    do j=1,my
      do i=1,mx
        ddzup1(i,j,k) = dzidzup(k)*( &
                     a*(f(i,j,kp1)-f(i,j,k  )))
      end do
    end do
  end do
!$omp end parallel
!
  if (omp_master) then
    nstag1 = nstag1+1
    nflop = nflop+2
  end if
END FUNCTION

!***********************************************************************
FUNCTION ddzdn (f)
  use params
  implicit none
  real a, b, c, a1st
  integer i, j, k, km3, km2, km1, kp1, kp2
  real, dimension(mx,my,mz):: f, ddzdn
!hpf$ distribute(*,*,block):: f, ddzdn
!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    ddzdn=0.
    return
  endif
!
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)
  a1st = 1.
!
!Xomp barrier
!$omp parallel private(i,j,k,km3,km2,km1,kp1,kp2)
  do k=izs,ize
    km3=mod(k-4+mz,mz)+1
    km2=mod(k-3+mz,mz)+1
    km1=mod(k-2+mz,mz)+1
    kp1=mod(k     ,mz)+1
    kp2=mod(k+1   ,mz)+1
    do j=1,my
      do i=1,mx
        ddzdn(i,j,k) = dzidzdn(k)*( &
                     c*(f(i,j,kp2)-f(i,j,km3)) + &
                     b*(f(i,j,kp1)-f(i,j,km2)) + &
                     a*(f(i,j,k  )-f(i,j,km1)))
      end do
    end do
  end do
!$omp end parallel
!
  if (omp_master) then
    nstag = nstag+1
    nflop = nflop+8
  end if
END FUNCTION

!***********************************************************************
FUNCTION ddzdn1 (f)
  use params
  implicit none
  real a, b, c, a1st
  integer i, j, k, km1
  real, dimension(mx,my,mz):: f, ddzdn1
!hpf$ distribute(*,*,block):: f, ddzdn1
!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    ddzdn1=0.
    return
  endif
!
  a = 1.
!
!Xomp barrier
!$omp parallel private(i,j,k,km1)
  do k=izs,ize
    km1=mod(k-2+mz,mz)+1
    do j=1,my
      do i=1,mx
        ddzdn1(i,j,k) = dzidzdn(k)*( &
                     a*(f(i,j,k  )-f(i,j,km1)))
      end do
    end do
  end do
!$omp end parallel
!
  if (omp_master) then
    nstag1 = nstag1+1
    nflop = nflop+2
  end if
END FUNCTION
!***********************************************************************
FUNCTION ddzup32 (f,j)
  use params
  implicit none
  real a, b, c, a1st
  integer i, j, k, km2, km1, kp1, kp2, kp3
  real, dimension(mx,my,mz):: f
  real, dimension(mx,mz):: ddzup32
!hpf$ distribute(*,*,block):: f, ddzup
!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    ddzup32=0.
    return
  endif
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)
  a1st = 1.
!
!Xomp barrier
!$omp parallel private(i,j,k,km2,km1,kp1,kp2,kp3)
  do k=izs,ize
    km2=mod(k-3+mz,mz)+1
    km1=mod(k-2+mz,mz)+1
    kp1=mod(k     ,mz)+1
    kp2=mod(k+1   ,mz)+1
    kp3=mod(k+2   ,mz)+1
      do i=1,mx
        ddzup32(i,k) = dzidzup(k)*( &
                     c*(f(i,j,kp3)-f(i,j,km2)) + &
                     b*(f(i,j,kp2)-f(i,j,km1)) + &
                     a*(f(i,j,kp1)-f(i,j,k  )))
      end do
  end do
!$omp end parallel
!
  if (omp_master) then
    nstag = nstag+1
    nflop = nflop+8
  end if
END FUNCTION

!***********************************************************************
FUNCTION ddzup132 (f,j)
  use params
  implicit none
  real a, b, c, a1st
  integer i, j, k, kp1
  real, dimension(mx,my,mz):: f
  real, dimension(mx,mz):: ddzup132
!hpf$ distribute(*,*,block):: f, ddzup1
!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    ddzup132=0.
    return
  endif
  a = 1.
!
!Xomp barrier
!$omp parallel private(i,j,k,kp1)
  do k=izs,ize
    kp1=mod(k     ,mz)+1
      do i=1,mx
        ddzup132(i,k) = dzidzup(k)*( &
                     a*(f(i,j,kp1)-f(i,j,k  )))
      end do
  end do
!$omp end parallel
!
  if (omp_master) then
    nstag1 = nstag1+1
    nflop = nflop+2
  end if
END FUNCTION

!***********************************************************************
FUNCTION ddzdn32 (f,j)
  use params
  implicit none
  real a, b, c, a1st
  integer i, j, k, km3, km2, km1, kp1, kp2
  real, dimension(mx,my,mz):: f
  real, dimension(mx,mz):: ddzdn32
!hpf$ distribute(*,*,block):: f, ddzdn
!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    ddzdn32=0.
    return
  endif
!
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)
  a1st = 1.
!
!Xomp barrier
!$omp parallel private(i,j,k,km3,km2,km1,kp1,kp2)
  do k=izs,ize
    km3=mod(k-4+mz,mz)+1
    km2=mod(k-3+mz,mz)+1
    km1=mod(k-2+mz,mz)+1
    kp1=mod(k     ,mz)+1
    kp2=mod(k+1   ,mz)+1
      do i=1,mx
        ddzdn32(i,k) = dzidzdn(k)*( &
                     c*(f(i,j,kp2)-f(i,j,km3)) + &
                     b*(f(i,j,kp1)-f(i,j,km2)) + &
                     a*(f(i,j,k  )-f(i,j,km1)))
      end do
  end do
!$omp end parallel
!
  if (omp_master) then
    nstag = nstag+1
    nflop = nflop+8
  end if
END FUNCTION

!***********************************************************************
FUNCTION ddzdn132 (f,j)
  use params
  implicit none
  real a, b, c, a1st
  integer i, j, k, km1
  real, dimension(mx,my,mz):: f
  real, dimension(mx,mz):: ddzdn132
!hpf$ distribute(*,*,block):: f, ddzdn1
!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    ddzdn132=0.
    return
  endif
!
  a = 1.
!
!Xomp barrier
!$omp parallel private(i,j,k,km1)
  do k=izs,ize
    km1=mod(k-2+mz,mz)+1
      do i=1,mx
        ddzdn132(i,k) = dzidzdn(k)*( &
                     a*(f(i,j,k  )-f(i,j,km1)))
      end do
  end do
!$omp end parallel
!
  if (omp_master) then
    nstag1 = nstag1+1
    nflop = nflop+2
  end if
END FUNCTION
!***********************************************************************
FUNCTION ddzup22 (f)
  use params
  implicit none
  real a, b, c, a1st
  integer i, j, k, km2, km1, kp1, kp2, kp3
  real, dimension(mx,mz):: ddzup22, f
!hpf$ distribute(*,*,block):: f, ddzup
!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    ddzup22=0.
    return
  endif
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)
  a1st = 1.
!
!Xomp barrier
!$omp parallel private(i,j,k,km2,km1,kp1,kp2,kp3)
  do k=izs,ize
    km2=mod(k-3+mz,mz)+1
    km1=mod(k-2+mz,mz)+1
    kp1=mod(k     ,mz)+1
    kp2=mod(k+1   ,mz)+1
    kp3=mod(k+2   ,mz)+1
      do i=1,mx
        ddzup22(i,k) = dzidzup(k)*( &
                     c*(f(i,kp3)-f(i,km2)) + &
                     b*(f(i,kp2)-f(i,km1)) + &
                     a*(f(i,kp1)-f(i,k  )))
      end do
  end do
!$omp end parallel
!
  if (omp_master) then
    nstag = nstag+1
    nflop = nflop+8
  end if
END FUNCTION

!***********************************************************************
FUNCTION ddzup122 (f)
  use params
  implicit none
  real a, b, c, a1st
  integer i, j, k, kp1
  real, dimension(mx,mz):: ddzup122, f
!hpf$ distribute(*,*,block):: f, ddzup1
!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    ddzup122=0.
    return
  endif
  a = 1.
!
!Xomp barrier
!$omp parallel private(i,k,kp1)
  do k=izs,ize
    kp1=mod(k     ,mz)+1
      do i=1,mx
        ddzup122(i,k) = dzidzup(k)*( &
                     a*(f(i,kp1)-f(i,k  )))
      end do
  end do
!$omp end parallel
!
  if (omp_master) then
    nstag1 = nstag1+1
    nflop = nflop+2
  end if
END FUNCTION

!***********************************************************************
FUNCTION ddzdn22 (f)
  use params
  implicit none
  real a, b, c, a1st
  integer i, j, k, km3, km2, km1, kp1, kp2
  real, dimension(mx,mz):: ddzdn22, f
!hpf$ distribute(*,*,block):: f, ddzdn
!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    ddzdn22=0.
    return
  endif
!
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)
  a1st = 1.
!
!Xomp barrier
!$omp parallel private(i,k,km3,km2,km1,kp1,kp2)
  do k=izs,ize
    km3=mod(k-4+mz,mz)+1
    km2=mod(k-3+mz,mz)+1
    km1=mod(k-2+mz,mz)+1
    kp1=mod(k     ,mz)+1
    kp2=mod(k+1   ,mz)+1
      do i=1,mx
        ddzdn22(i,k) = dzidzdn(k)*( &
                     c*(f(i,kp2)-f(i,km3)) + &
                     b*(f(i,kp1)-f(i,km2)) + &
                     a*(f(i,k  )-f(i,km1)))
      end do
  end do
!$omp end parallel
!
  if (omp_master) then
    nstag = nstag+1
    nflop = nflop+8
  end if
END FUNCTION

!***********************************************************************
FUNCTION ddzdn122 (f)
  use params
  implicit none
  real a, b, c, a1st
  integer i, j, k, km1
  real, dimension(mx,mz):: ddzdn122, f
!hpf$ distribute(*,*,block):: f, ddzdn1
!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    ddzdn122=0.
    return
  endif
!
  a = 1.
!
!Xomp barrier
!$omp parallel private(i,k,km1)
  do k=izs,ize
    km1=mod(k-2+mz,mz)+1
      do i=1,mx
        ddzdn122(i,k) = dzidzdn(k)*( &
                     a*(f(i,k  )-f(i,km1)))
      end do
  end do
!$omp end parallel
!
  if (omp_master) then
    nstag1 = nstag1+1
    nflop = nflop+2
  end if
END FUNCTION
