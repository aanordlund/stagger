!  $Id: dif_call.f90,v 1.2 2007/07/09 09:37:37 aake Exp $
!-----------------------------------------------------------------------
      subroutine difx2_set (f, g)
!
!  Finite difference one zone left to one zones right.
!
      USE params
      implicit none
      integer i,j,k
      real, dimension(mx,my,mz):: f, g
!hpf$ align with TheCube:: f,g
!-----------------------------------------------------------------------
      do k=1,mz
        do j=1,my
          do i=2,mx-1
            g(i,j,k) = f(i-1,j,k)-f(i+1,j,k)
          end do
          g(1 ,j,k) = f(mx  ,j,k)-f(2,j,k)
          g(mx,j,k) = f(mx-1,j,k)-f(1,j,k)
        end do
      end do
      END
!-----------------------------------------------------------------------
      subroutine dify2_set (f, g)
!
!  Finite difference one zone left to one zones right.
!
      USE params
      implicit none
      integer i,j,k
      real, dimension(mx,my,mz):: f, g
!hpf$ align with TheCube:: f,g
!-----------------------------------------------------------------------
      if (my .eq. 1) then
        g = 0.0
        return
      endif
      do k=1,mz
        do j=2,my-1
          do i=1,mx
            g(i,j,k) = f(i,j-1,k)-f(i,j+1,k)
          end do
        end do
        do i=1,mx
          g(i,1 ,k) = f(i,my  ,k)-f(i,2,k)
          g(i,my,k) = f(i,my-1,k)-f(i,1,k)
        end do
      end do
      END
!-----------------------------------------------------------------------
      subroutine difz2_set (f, g)
!
!  Finite difference one zone left to one zones right.
!
      USE params
      implicit none
      integer i,j,k

      real, dimension(mx,my,mz):: f, g
!hpf$ align with TheCube:: f,g
!-----------------------------------------------------------------------
      if (mz .eq. 1) then
        g = 0.0
        return
      endif
      do j=1,my
        do i=1,mx
            g(i,j,1 ) = f(i,j,mz  )-f(i,j,2)
            g(i,j,mz) = f(i,j,mz-1)-f(i,j,1)
        end do
      end do
      do k=2,mz-1
        do j=1,my
          do i=1,mx
            g(i,j,k) = f(i,j,k-1)-f(i,j,k+1)
          end do
        end do
      end do
      END
!-----------------------------------------------------------------------
      subroutine difx1_set (f, g)
!
!  Finite difference one zone left to one zones right.
!
      USE params
      implicit none
      integer i,j,k
      real, dimension(mx,my,mz):: f, g
!hpf$ align with TheCube:: f,g
!-----------------------------------------------------------------------
      do k=1,mz
        do j=1,my
          do i=1,mx-1
            g(i,j,k) = f(i,j,k)-f(i+1,j,k)
          end do
          g(mx,j,k) = f(mx,j,k)-f(1,j,k)
        end do
      end do
      END
!-----------------------------------------------------------------------
      subroutine dify1_set (f, g)
!
!  Finite difference one zone left to one zones right.
!
      USE params
      implicit none
      integer i,j,k
      real, dimension(mx,my,mz):: f, g
!hpf$ align with TheCube:: f,g
!-----------------------------------------------------------------------
      if (my .eq. 1) then
        g = 0.0
        return
      endif
      do k=1,mz
        do j=1,my-1
          do i=1,mx
            g(i,j,k) = f(i,j,k)-f(i,j+1,k)
          end do
        end do
        do i=1,mx
          g(i,my,k) = f(i,my,k)-f(i,1,k)
        end do
      end do
      END
!-----------------------------------------------------------------------
      subroutine difz1_set (f, g)
!
!  Finite difference one zone left to one zones right.
!
      USE params
      implicit none
      integer i,j,k
      real, dimension(mx,my,mz):: f, g
!hpf$ align with TheCube:: f,g
!-----------------------------------------------------------------------
      if (mz .eq. 1) then
        g = 0.0
        return
      endif
      do j=1,my
        do i=1,mx
          g(i,j,mz) = f(i,j,mz)-f(i,j,1)
        end do
      end do
      do k=1,mz-1
        do j=1,my
          do i=1,mx
            g(i,j,k) = f(i,j,k)-f(i,j,k+1)
          end do
        end do
      end do
      END
!-----------------------------------------------------------------------
      subroutine difxyz1_set (fx, fy, fz, g, c)
!
!  Finite difference one zone left to one zones right.
!
      USE params
      implicit none
      real c
      integer i,j,k
      real, dimension(mx,my,mz):: fx, fy, fz, g
!hpf$ align with TheCube:: fx, fy, fz, g
!-----------------------------------------------------------------------

      do k=1,mz-1
        do j=1,my-1
          do i=1,mx-1
            g(i,j,k) = c*amax1(0., &
                       fx(i,j,k)-fx(i+1,j,k) &
                     + fy(i,j,k)-fy(i,j+1,k) &
                     + fz(i,j,k)-fz(i,j,k+1))
          end do
          g(mx,j,k) = c*amax1(0., &
                      fx(mx,j,k)-fx(1 ,j  ,k) &
                    + fy(mx,j,k)-fy(mx,j+1,k) &
                    + fz(mx,j,k)-fz(mx,j,k+1))
        end do
        do i=1,mx-1
          g(i,my,k) = c*amax1(0., &
                      fx(i,my,k)-fx(i+1,my,k  ) &
                    + fy(i,my,k)-fy(i  ,1 ,k  ) &
                    + fz(i,my,k)-fz(i  ,my,k+1))
        end do
        g(mx,my,k) = c*amax1(0., &
                     fx(mx,my,k)-fx(1 ,my,k  ) &
                   + fy(mx,my,k)-fy(mx,1 ,k  ) &
                   + fz(mx,my,k)-fz(mx,my,k+1))
      end do

      do j=1,my-1
        do i=1,mx-1
          g(i,j,mz) = c*amax1(0., &
                      fx(i,j,mz)-fx(i+1,j,mz) &
                    + fy(i,j,mz)-fy(i,j+1,mz) &
                    + fz(i,j,mz)-fz(i,j  ,1 ))
        end do
        g(mx,j,mz) = c*amax1(0., &
                     fx(mx,j,mz)-fx(1 ,j  ,mz) &
                   + fy(mx,j,mz)-fy(mx,j+1,mz) &
                   + fz(mx,j,mz)-fz(mx,j  ,1 ))
      end do
      do i=1,mx-1
        g(i,my,mz) = c*amax1(0., &
                     fx(i,my,mz)-fx(i+1,my,mz) &
                   + fy(i,my,mz)-fy(i  ,1 ,mz) &
                   + fz(i,my,mz)-fz(i  ,my,1 ))
      end do
      g(mx,my,mz) = c*amax1(0., &
                    fx(mx,my,mz)-fx(1 ,my,mz) &
                  + fy(mx,my,mz)-fy(mx,1 ,mz) &
                  + fz(mx,my,mz)-fz(mx,my,1 ))

      END
!-----------------------------------------------------------------------
      subroutine difxyz_u (fx, fy, fz, g, c)
!
!  Finite difference one zone left to one zones right.
!
      USE params
      implicit none
      integer i,j,k
      real c
      real, dimension(mx,my,mz):: fx, fy, fz, g
!hpf$ align with TheCube:: fx, fy, fz, g
!-----------------------------------------------------------------------

      do k=1,mz-1
        do j=1,my-1
          do i=1,mx-1
            g(i,j,k) = g(i,j,k) + c*amax1(0., &
                       fx(i,j,k)-fx(i+1,j,k) &
                     + fy(i,j,k)-fy(i,j+1,k) &
                     + fz(i,j,k)-fz(i,j,k+1))
          end do
          g(mx,j,k) = g(mx,j,k) + c*amax1(0., &
                      fx(mx,j,k)-fx(1 ,j  ,k) &
                    + fy(mx,j,k)-fy(mx,j+1,k) &
                    + fz(mx,j,k)-fz(mx,j,k+1))
        end do
        do i=1,mx-1
          g(i,my,k) = g(i,my,k) + c*amax1(0., &
                      fx(i,my,k)-fx(i+1,my,k  ) &
                    + fy(i,my,k)-fy(i  ,1 ,k  ) &
                    + fz(i,my,k)-fz(i  ,my,k+1))
        end do
        g(mx,my,k) = g(mx,my,k) + c*amax1(0., &
                     fx(mx,my,k)-fx(1 ,my,k  ) &
                   + fy(mx,my,k)-fy(mx,1 ,k  ) &
                   + fz(mx,my,k)-fz(mx,my,k+1))
      end do

      do j=1,my-1
        do i=1,mx-1
          g(i,j,mz) = g(i,j,mz) + c*amax1(0., &
                      fx(i,j,mz)-fx(i+1,j,mz) &
                    + fy(i,j,mz)-fy(i,j+1,mz) &
                    + fz(i,j,mz)-fz(i,j  ,1 ))
        end do
        g(mx,j,mz) = g(mx,j,mz) + c*amax1(0., &
                     fx(mx,j,mz)-fx(1 ,j  ,mz) &
                   + fy(mx,j,mz)-fy(mx,j+1,mz) &
                   + fz(mx,j,mz)-fz(mx,j  ,1 ))
      end do
      do i=1,mx-1
        g(i,my,mz) = g(i,my,mz) + c*amax1(0., &
                     fx(i,my,mz)-fx(i+1,my,mz) &
                   + fy(i,my,mz)-fy(i  ,1 ,mz) &
                   + fz(i,my,mz)-fz(i  ,my,1 ))
      end do
      g(mx,my,mz) = g(mx,my,mz) + c*amax1(0., &
                    fx(mx,my,mz)-fx(1 ,my,mz) &
                  + fy(mx,my,mz)-fy(mx,1 ,mz) &
                  + fz(mx,my,mz)-fz(mx,my,1 ))

      nflop = nflop + 8
      END
!-----------------------------------------------------------------------
      subroutine difxyz_b (bx, by, bz, ux, uy, uz, g, cc)
!
!  Finite difference one zone left to one zones right.
!
      USE params
      implicit none
      integer i,j,k,kp1,km1
      real c,cc,udotb,bdotb,ufact
      real, dimension(mx,my,mz):: bx, by, bz, ux, uy, uz, g
!hpf$ align with TheCube:: bx, by, bz, ux, uy, uz, g
!-----------------------------------------------------------------------

      c = cc*0.5
      do k=1,mz
        do j=1,my
          do i=1,mx
            udotb = ux(i,j,k)*bx(i,j,k) &
                  + uy(i,j,k)*by(i,j,k) &
                  + uz(i,j,k)*bz(i,j,k) 
            bdotb = bx(i,j,k)*bx(i,j,k) &
                  + by(i,j,k)*by(i,j,k) &
                  + bz(i,j,k)*bz(i,j,k) + 1e-30
            ufact = udotb/bdotb
            bx(i,j,k) = ux(i,j,k) - bx(i,j,k)*ufact
            by(i,j,k) = uy(i,j,k) - by(i,j,k)*ufact
            bz(i,j,k) = uz(i,j,k) - bz(i,j,k)*ufact
          end do
        end do
      end do
      nflop = nflop + 18

      do k=2,mz-1
        do j=2,my-1
          do i=2,mx-1
            g(i,j,k) = c*amax1(0., &
                       bx(i-1,j,k)-bx(i+1,j,k) &
                     + by(i,j-1,k)-by(i,j+1,k) &
                     + bz(i,j,k-1)-bz(i,j,k+1))
          end do
          g(1 ,j,k) = c*amax1(0., &
                      bx(mx,j  ,k)-bx(2 ,j  ,k) &
                    + by(1 ,j-1,k)-by(1 ,j+1,k) &
                    + bz(1 ,j,k-1)-bz(1 ,j,k+1))
          g(mx,j,k) = c*amax1(0., &
                      bx(mx-1,j,k)-bx(1 ,j  ,k) &
                    + by(mx,j-1,k)-by(mx,j+1,k) &
                    + bz(mx,j,k-1)-bz(mx,j,k+1))
        end do
        do i=2,mx-1
          g(i,1 ,k) = c*amax1(0., &
                      bx(i-1,1 ,k)-bx(i+1,1 ,k  ) &
                    + by(i,my  ,k)-by(i  ,2 ,k  ) &
                    + bz(i,1 ,k-1)-bz(i  ,1 ,k+1))
          g(i,my,k) = c*amax1(0., &
                      bx(i-1,my,k)-bx(i+1,my,k  ) &
                    + by(i,my-1,k)-by(i  ,1 ,k  ) &
                    + bz(i,my,k-1)-bz(i  ,my,k+1))
        end do
        g(1 ,my,k) = c*amax1(0., &
                     bx(mx-1,my,k)-bx(2 ,my,k  ) &
                   + by(1 ,my-1,k)-by(1 ,1 ,k  ) &
                   + bz(1 ,my,k-1)-bz(1 ,my,k+1))
        g(1 ,1 ,k) = c*amax1(0., &
                     bx(mx-1,1 ,k)-bx(2 ,1 ,k  ) &
                   + by(1 ,my  ,k)-by(1 ,2 ,k  ) &
                   + bz(1 ,1 ,k-1)-bz(1 ,1 ,k+1))
        g(mx,my,k) = c*amax1(0., &
                     bx(mx-1,my,k)-bx(1 ,my,k  ) &
                   + by(mx,my-1,k)-by(mx,1 ,k  ) &
                   + bz(mx,my,k-1)-bz(mx,my,k+1))
        g(mx,1 ,k) = c*amax1(0., &
                     bx(mx-1,1 ,k)-bx(1 ,1 ,k  ) &
                   + by(mx,my  ,k)-by(mx,2 ,k  ) &
                   + bz(mx,1 ,k-1)-bz(mx,1 ,k+1))
      end do

      do k=1,mz,mz-1
        kp1 = mod(k,mz)+1
        km1 = mod(k+mz-2,mz)+1
        do j=2,my-1
          do i=2,mx-1
            g(i,j,k) = c*amax1(0., &
                       bx(i-1,j,k)-bx(i+1,j,k) &
                     + by(i,j-1,k)-by(i,j+1,k) &
                     + bz(i,j,km1)-bz(i,j,kp1))
          end do
          g(1 ,j,k) = c*amax1(0., &
                      bx(mx  ,j,k)-bx(2 ,j  ,k) &
                    + by(1 ,j-1,k)-by(1 ,j+1,k) &
                    + bz(1 ,j,km1)-bz(1 ,j,kp1))
          g(mx,j,k) = c*amax1(0., &
                      bx(mx-1,j,k)-bx(1 ,j  ,k) &
                    + by(mx,j-1,k)-by(mx,j+1,k) &
                    + bz(mx,j,km1)-bz(mx,j,kp1))
        end do
        do i=2,mx-1
          g(i,1 ,k) = c*amax1(0., &
                      bx(i-1,1 ,k)-bx(i+1,1 ,k  ) &
                    + by(i,my  ,k)-by(i  ,2 ,k  ) &
                    + bz(i,1 ,km1)-bz(i  ,1 ,kp1))
          g(i,my,k) = c*amax1(0., &
                      bx(i-1,my,k)-bx(i+1,my,k  ) &
                    + by(i,my-1,k)-by(i  ,1 ,k  ) &
                    + bz(i,my,km1)-bz(i  ,my,kp1))
        end do
        g(1 ,my,k) = c*amax1(0., &
                     bx(mx-1,my,k)-bx(2 ,my,k  ) &
                   + by(1 ,my-1,k)-by(1 ,1 ,k  ) &
                   + bz(1 ,my,km1)-bz(1 ,my,kp1))
        g(1 ,1 ,k) = c*amax1(0., &
                     bx(mx-1,1 ,k)-bx(2 ,1 ,k  ) &
                   + by(1 ,my  ,k)-by(1 ,2 ,k  ) &
                   + bz(1 ,1 ,km1)-bz(1 ,1 ,kp1))
        g(mx,my,k) = c*amax1(0., &
                     bx(mx-1,my,k)-bx(1 ,my,k  ) &
                   + by(mx,my-1,k)-by(mx,1 ,k  ) &
                   + bz(mx,my,km1)-bz(mx,my,kp1))
        g(mx,1 ,k) = c*amax1(0., &
                     bx(mx-1,1 ,k)-bx(1 ,1 ,k  ) &
                   + by(mx,my  ,k)-by(mx,2 ,k  ) &
                   + bz(mx,1 ,km1)-bz(mx,1 ,kp1))
      end do
      nflop = nflop + 8

      END
!-----------------------------------------------------------------------
