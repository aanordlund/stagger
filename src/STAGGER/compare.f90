USE params
USE stagger0
USE stagger
!implicit none
real, allocatable, dimension(:,:,:):: f,f1,f2,f0
integer:: ix,iy,iz

mx=15
my=16
mz=17
dx=2.
dy=3.
dz=4.
allocate(f (mx,my,mz))
allocate(f0(mx,my,mz))
allocate(f1(mx,my,mz))
allocate(f2(mx,my,mz))

do iz=1,mz
do iy=1,my
do ix=1,mx
  f(ix,iy,iz)=sin(ix*2.*pi/mx) &
             *sin(iy*2.*pi/my) &
             *sin(iz*2.*pi/mz)
end do
end do
end do

f1 = xup0(f); f2 = xup(f); print *,'xup',maxval(abs(f1-f2))
f1 = yup0(f); f2 = yup(f); print *,'yup',maxval(abs(f1-f2))
f1 = zup0(f); f2 = zup(f); print *,'zup',maxval(abs(f1-f2))
f1 = xdn0(f); f2 = xdn(f); print *,'xdn',maxval(abs(f1-f2))
f1 = ydn0(f); f2 = ydn(f); print *,'ydn',maxval(abs(f1-f2))
f1 = zdn0(f); f2 = zdn(f); print *,'zdn',maxval(abs(f1-f2))

f1 = xup10(f); f2 = xup1(f); print *,'xup1',maxval(abs(f1-f2))
f1 = yup10(f); f2 = yup1(f); print *,'yup1',maxval(abs(f1-f2))
f1 = zup10(f); f2 = zup1(f); print *,'zup1',maxval(abs(f1-f2))
f1 = xdn10(f); f2 = xdn1(f); print *,'xdn1',maxval(abs(f1-f2))
f1 = ydn10(f); f2 = ydn1(f); print *,'ydn1',maxval(abs(f1-f2))
f1 = zdn10(f); f2 = zdn1(f); print *,'zdn1',maxval(abs(f1-f2))

f1 = ddxup0(f); f2 = ddxup(f); print *,'ddxup',maxval(abs(f1-f2))
f1 = ddyup0(f); f2 = ddyup(f); print *,'ddyup',maxval(abs(f1-f2))
f1 = ddzup0(f); f2 = ddzup(f); print *,'ddzup',maxval(abs(f1-f2))
f1 = ddxdn0(f); f2 = ddxdn(f); print *,'ddxdn',maxval(abs(f1-f2))
f1 = ddydn0(f); f2 = ddydn(f); print *,'ddydn',maxval(abs(f1-f2))
f1 = ddzdn0(f); f2 = ddzdn(f); print *,'ddzdn',maxval(abs(f1-f2))

f1 = ddxup10(f); f2 = ddxup1(f); print *,'ddxup1',maxval(abs(f1-f2))
f1 = ddyup10(f); f2 = ddyup1(f); print *,'ddyup1',maxval(abs(f1-f2))
f1 = ddzup10(f); f2 = ddzup1(f); print *,'ddzup1',maxval(abs(f1-f2))
f1 = ddxdn10(f); f2 = ddxdn1(f); print *,'ddxdn1',maxval(abs(f1-f2))
f1 = ddydn10(f); f2 = ddydn1(f); print *,'ddydn1',maxval(abs(f1-f2))
f1 = ddzdn10(f); f2 = ddzdn1(f); print *,'ddzdn1',maxval(abs(f1-f2))

f1 = max50(f); f2 = max5(f); print *,'max5',maxval(abs(f1-f2)),maxloc(abs(f1-f2))
f1 = smooth30(f); f2 = smooth3(f); print *,'smooth3',maxval(abs(f1-f2))

END
