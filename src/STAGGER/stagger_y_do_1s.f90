!***********************************************************************
FUNCTION yup (f)
!
!  f is centered on (i,j-.5,k), fifth order stagger
!
  use params
  real, intent(in) , dimension(mx,my,mz):: f
  real,              dimension(mx,my,mz):: yup
!hpf$ distribute(*,*,block):: f, yup
!-----------------------------------------------------------------------
!
  character(len=80):: id = "$Id: stagger_y_do_1s.f90,v 1.3 2003/04/27 10:11:36 aake Exp $" 
  if (id.ne.' ') print '(1x,a)',id; id=' '

  if (my .lt. 5) then
!$omp parallel do private(k)
    do k=1,mz
      yup(:,:,k) = f(:,:,k)
    end do
    return
  end if
!
  c20 = -2.734375E-2
  c21 = 0.41015625
  c22 = 0.8203125
  c23 = -0.2734375
  c24 = 8.203125E-2
  c25 = -1.171875E-2
!
  c = 3./256.
  b = -25./256.
  a = .5-b-c
  a1st = 0.5
!
!$omp parallel do private(i,j,k)
  do k=1,mz
    do i=1,mx
      yup(i,my-2,k) = ( &
                     a*(f(i,my-2,k)+f(i,my-1,k)) + &
                     b*(f(i,my-3,k)+f(i,my  ,k)) + &
                     c*(f(i,my-4,k)+f(i,1   ,k)))
      yup(i,my-1,k) = ( &
                     a*(f(i,my-1,k)+f(i,my  ,k)) + &
                     b*(f(i,my-2,k)+f(i,1   ,k)) + &
                     c*(f(i,my-3,k)+f(i,2   ,k)))
      yup(i,my  ,k) = ( &
                   c20*f(i,2   ,k)+c21*f(i,1   ,k) &
                 + c22*f(i,my  ,k)+c23*f(i,my-1,k) &
                 + c24*f(i,my-2,k)+c25*f(i,my-3,k))
      yup(i,1   ,k) = ( &
                   c20*f(i,my  ,k)+c21*f(i,1   ,k) &
                 + c22*f(i,2   ,k)+c23*f(i,3   ,k) &
                 + c24*f(i,4   ,k)+c25*f(i,5   ,k))
      yup(i,2   ,k) = ( &
                     a*(f(i,2   ,k)+f(i,3   ,k)) + &
                     b*(f(i,1   ,k)+f(i,4   ,k)) + &
                     c*(f(i,my  ,k)+f(i,5   ,k)))
    end do
    do j=3,my-3
     do i=1,mx
      yup(i,j  ,k) = ( &
                     a*(f(i,j   ,k)+f(i,j+1 ,k)) + &
                     b*(f(i,j-1 ,k)+f(i,j+2 ,k)) + &
                     c*(f(i,j-2 ,k)+f(i,j+3 ,k)))
     end do
    end do
  end do
!
  nstag = nstag+1
  nflop = nflop+8
END FUNCTION

!***********************************************************************
FUNCTION yup1 (f)
!
!  f is centered on (i,j-.5,k), fifth order stagger
!
  use params
  real, intent(in) , dimension(mx,my,mz):: f
  real,              dimension(mx,my,mz):: yup1
!hpf$ distribute(*,*,block):: f, yup1
!-----------------------------------------------------------------------
!
  if (my .lt. 5) then
!$omp parallel do private(k)
    do k=1,mz
      yup1(:,:,k) = f(:,:,k)
    end do
    return
  end if
!
  a = 0.5
!
!$omp parallel do private(i,j,k)
  do k=1,mz
    do i=1,mx
      yup1(i,my,k) = ( &
                     a*(f(i,my,k)+f(i,1,k)))
    end do
    do j=1,my-1
      do i=1,mx
        yup1(i,j  ,k) = ( &
                     a*(f(i,j   ,k)+f(i,j+1 ,k)))
      end do
    end do
  end do
!
  nstag1 = nstag1+1
  nflop = nflop+2
END FUNCTION

!***********************************************************************
FUNCTION ydn (f)
!
!  f is centered on (i,j,k), fifth order stagger
!
  use params
  real, dimension(mx,my,mz):: f, ydn
!hpf$ distribute(*,*,block):: f, ydn
!
!-----------------------------------------------------------------------
  if (my .lt. 5) then
!$omp parallel do private(k)
    do k=1,mz
      ydn(:,:,k) = f(:,:,k)
    end do
    return
  end if
!
  c20 = -2.734375E-2
  c21 = 0.41015625
  c22 = 0.8203125
  c23 = -0.2734375
  c24 = 8.203125E-2
  c25 = -1.171875E-2
!
  c = 3./256.
  b = -25./256.
  a = .5-b-c
  a1st = 0.5
!
!$omp parallel do private(i,j,k)
  do k=1,mz
    do i=1,mx
      ydn(i,my-1,k) = ( &
                     a*(f(i,my-2,k)+f(i,my-1,k)) + &
                     b*(f(i,my-3,k)+f(i,my  ,k)) + &
                     c*(f(i,my-4,k)+f(i,1   ,k)))
      ydn(i,my,k) = ( &
                    c20*f(i,1   ,k)+c21*f(i,my  ,k) &
                  + c22*f(i,my-1,k)+c23*f(i,my-2,k) &
                  + c24*f(i,my-3,k)+c25*f(i,my-4,k))
      ydn(i,1 ,k) = ( &
                    c20*f(i,my-1,k)+c21*f(i,my  ,k) &
                  + c22*f(i,1   ,k)+c23*f(i,2   ,k) &
                  + c24*f(i,3   ,k)+c25*f(i,4   ,k))
      ydn(i,2   ,k) = ( &
                     a*(f(i,1   ,k)+f(i,2   ,k)) + &
                     b*(f(i,my  ,k)+f(i,3   ,k)) + &
                     c*(f(i,my-1,k)+f(i,4   ,k)))
      ydn(i,3   ,k) = ( &
                     a*(f(i,2   ,k)+f(i,3   ,k)) + &
                     b*(f(i,1   ,k)+f(i,4   ,k)) + &
                     c*(f(i,my  ,k)+f(i,5   ,k)))
    end do
    do j=4,my-2
     do i=1,mx
      ydn(i,j,k) = ( &
                     a*(f(i,j   ,k)+f(i,j-1 ,k)) + &
                     b*(f(i,j+1 ,k)+f(i,j-2 ,k)) + &
                     c*(f(i,j+2 ,k)+f(i,j-3 ,k)))
     end do
    end do
  end do
!
  nstag = nstag+1
  nflop = nflop+8
END FUNCTION

!***********************************************************************
FUNCTION ydn1 (f)
!
!  f is centered on (i,j,k), fifth order stagger
!
  use params
  real, dimension(mx,my,mz):: f, ydn1
!hpf$ distribute(*,*,block):: f, ydn1
!
!-----------------------------------------------------------------------
  if (my .lt. 5) then
!$omp parallel do private(k)
    do k=1,mz
      ydn1(:,:,k) = f(:,:,k)
    end do
    return
  end if
!
  a = 0.5
!
!$omp parallel do private(i,j,k)
  do k=1,mz
    do i=1,mx
      ydn1(i,1   ,k) = ( &
                     a*(f(i,my  ,k)+f(i,1   ,k)))
    end do
    do j=2,my
     do i=1,mx
      ydn1(i,j,k) = ( &
                     a*(f(i,j   ,k)+f(i,j-1 ,k)))
     end do
    end do
  end do
!
  nstag1 = nstag1+1
  nflop = nflop+2
END FUNCTION

!***********************************************************************
FUNCTION ddyup (f)
  use params
  real, dimension(mx,my,mz):: f, ddyup
!hpf$ distribute(*,*,block):: f, ddyup
!-----------------------------------------------------------------------
!
  if (my .lt. 5) then
    ddyup = 0.
    return
  end if
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)/dy
  a1st = 1./dy
  b = b/dy
  c = c/dy
!
  c20=3.697916667e-2/dy
  c21=-1.1015625/dy
  c22=1.078125/dy
  c23=5.20833333e-3/dy
  c24=-2.34375E-2/dy
  c25=4.687500000e-3/dy
!
!$omp parallel do private(i,j,k)
  do k=1,mz
    do i=1,mx
      ddyup(i,my-2,k) = ( &
                     a*(f(i,my-1,k)-f(i,my-2,k)) + &
                     b*(f(i,my  ,k)-f(i,my-3,k)) + &
                     c*(f(i,1   ,k)-f(i,my-4,k)))
      ddyup(i,my-1,k) = ( &
                     a*(f(i,my  ,k)-f(i,my-1,k)) + &
                     b*(f(i,1   ,k)-f(i,my-2,k)) + &
                     c*(f(i,2   ,k)-f(i,my-3,k)))
      ddyup(i,my  ,k) = - ( &
                    c20*f(i,2   ,k)+c21*f(i,1   ,k) &
                  + c22*f(i,my  ,k)+c23*f(i,my-1,k) &
                  + c24*f(i,my-2,k)+c25*f(i,my-3,k))
      ddyup(i,1  ,k) = ( &
                    c20*f(i,my  ,k)+c21*f(i,1   ,k) &
                  + c22*f(i,2   ,k)+c23*f(i,3   ,k) &
                  + c24*f(i,4   ,k)+c25*f(i,5   ,k))
      ddyup(i,2   ,k) = ( &
                     a*(f(i,3   ,k)-f(i,2   ,k)) + &
                     b*(f(i,4   ,k)-f(i,1   ,k)) + &
                     c*(f(i,5   ,k)-f(i,my  ,k)))
    end do
    do j=3,my-3
     do i=1,mx
      ddyup(i,j  ,k) = ( &
                     a*(f(i,j+1 ,k)-f(i,j   ,k)) + &
                     b*(f(i,j+2 ,k)-f(i,j-1 ,k)) + &
                     c*(f(i,j+3 ,k)-f(i,j-2 ,k)))
     end do
    end do
  end do
!
  nstag = nstag+1
  nflop = nflop+8
END FUNCTION

!***********************************************************************
FUNCTION ddyup1 (f)
  use params
  real, dimension(mx,my,mz):: f, ddyup1
!hpf$ distribute(*,*,block):: f, ddyup1
!-----------------------------------------------------------------------
!
  if (my .lt. 5) then
    ddyup1 = 0.
    return
  end if
  a = 1./dy
!
!$omp parallel do private(i,j,k)
  do k=1,mz
    do i=1,mx
      ddyup1(i,my,k) = ( &
                     a*(f(i,1  ,k)-f(i,my,k)))
    end do
    do j=1,my-1
      do i=1,mx
        ddyup1(i,j  ,k) = ( &
                     a*(f(i,j+1 ,k)-f(i,j   ,k)))
     end do
    end do
  end do
!
  nstag1 = nstag1+1
  nflop = nflop+2
END FUNCTION

!***********************************************************************
FUNCTION ddydn (f)
  use params
  real, dimension(mx,my,mz):: f, ddydn
!hpf$ distribute(*,*,block):: f, ddydn
!-----------------------------------------------------------------------
!
  if (my .lt. 5) then
    ddydn = 0.
    return
  end if
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)/dy
  a1st = 1./dy
  b = b/dy
  c = c/dy
!
  c20=3.697916667e-2/dy
  c21=-1.1015625/dy
  c22=1.078125/dy
  c23=5.20833333e-3/dy
  c24=-2.34375E-2/dy
  c25=4.687500000e-3/dy
!
!$omp parallel do private(i,j,k)
  do k=1,mz
    do i=1,mx
      ddydn(i,my-1,k) = ( &
                     a*(f(i,my-1,k)-f(i,my-2,k)) + &
                     b*(f(i,my  ,k)-f(i,my-3,k)) + &
                     c*(f(i,1   ,k)-f(i,my-4,k)))
      ddydn(i,my  ,k) = - ( &
                    c20*f(i,1   ,k)+c21*f(i,my  ,k) &
                  + c22*f(i,my-1,k)+c23*f(i,my-2,k) &
                  + c24*f(i,my-3,k)+c25*f(i,my-4,k))
      ddydn(i,1   ,k) = ( &
                    c20*f(i,my-1,k)+c21*f(i,my  ,k) &
                  + c22*f(i,1   ,k)+c23*f(i,2   ,k) &
                  + c24*f(i,3   ,k)+c25*f(i,4   ,k))
      ddydn(i,2   ,k) = ( &
                     a*(f(i,2   ,k)-f(i,1   ,k)) + &
                     b*(f(i,3   ,k)-f(i,my  ,k)) + &
                     c*(f(i,4   ,k)-f(i,my-1,k)))
      ddydn(i,3   ,k) = ( &
                     a*(f(i,3   ,k)-f(i,2   ,k)) + &
                     b*(f(i,4   ,k)-f(i,1   ,k)) + &
                     c*(f(i,5   ,k)-f(i,my  ,k)))
    end do
    do j=4,my-2
     do i=1,mx
      ddydn(i,j ,k) = ( &
                     a*(f(i,j   ,k)-f(i,j-1 ,k)) + &
                     b*(f(i,j+1 ,k)-f(i,j-2 ,k)) + &
                     c*(f(i,j+2 ,k)-f(i,j-3 ,k)))
     end do
    end do
  end do
!
!
  nstag = nstag+1
  nflop = nflop+8
END FUNCTION

!***********************************************************************
FUNCTION ddydn1 (f)
  use params
  real, dimension(mx,my,mz):: f, ddydn1
!hpf$ distribute(*,*,block):: f, ddydn1
!-----------------------------------------------------------------------
!
  if (my .lt. 5) then
    ddydn1 = 0.
    return
  end if
  a = 1./dy
!
!$omp parallel do private(i,j,k)
  do k=1,mz
    do i=1,mx
      ddydn1(i,1  ,k) = ( &
                     a*(f(i,1   ,k)-f(i,my  ,k)))
    end do
    do j=2,my
     do i=1,mx
      ddydn1(i,j ,k) = ( &
                     a*(f(i,j   ,k)-f(i,j-1 ,k)))
     end do
    end do
  end do
!
  nstag1 = nstag1+1
  nflop = nflop+2
END FUNCTION
