!***********************************************************************
FUNCTION zup (f)
!
!  f is centered on (i,j,k-.5), fifth order stagger
!
  use params
  real, dimension(mx,my,mz):: f, zup
!hpf$ distribute(*,*,block):: f, zup
!
!-----------------------------------------------------------------------
  c = 3./256.
  b = -25./256.
  a = .5-b-c
  a1st = 0.5
!
!$omp parallel private(i,j,k,km1,km2,kp1,kp2,kp3)
  do j=1,my
!$omp do
    do k=1,mz
      km2=mod(k-3+mz,mz)+1
      km1=mod(k-2+mz,mz)+1
      kp1=mod(k     ,mz)+1
      kp2=mod(k+1   ,mz)+1
      kp3=mod(k+2   ,mz)+1
      do i=1,mx
        zup(i,j,k) = ( &
                     a*(f(i,j,k  )+f(i,j,kp1)) + &
                     b*(f(i,j,km1)+f(i,j,kp2)) + &
                     c*(f(i,j,km2)+f(i,j,kp3)))
      end do
    end do
!$omp enddo nowait
  end do
!$omp end parallel
!
  nstag = nstag+1
  nflop = nflop+8
END FUNCTION

!***********************************************************************
FUNCTION zup1 (f)
!
!  f is centered on (i,j,k-.5), fifth order stagger
!
  use params
  real, dimension(mx,my,mz):: f, zup1
!hpf$ distribute(*,*,block):: f, zup1
!
!-----------------------------------------------------------------------
  a = 0.5
!
!$omp parallel do private(i,j,k,km1,km2,kp1,kp2,kp3)
  do k=1,mz
    kp1=mod(k     ,mz)+1
    do j=1,my
      do i=1,mx
        zup1(i,j,k) = a*(f(i,j,k  )+f(i,j,kp1))
      end do
    end do
  end do
!
  nstag1 = nstag1+1
  nflop = nflop+2
END FUNCTION

!***********************************************************************
FUNCTION zdn (f)
!
!  f is centered on (i,j,k), fifth order stagger
!
  use params
  real, dimension(mx,my,mz):: f, zdn
!hpf$ distribute(*,*,block):: f, zdn
!
!-----------------------------------------------------------------------
  c = 3./256.
  b = -25./256.
  a = .5-b-c
  a1st = 0.5
!
!$omp parallel do private(i,j,k,km1,km2,km3,kp1,kp2)
  do k=1,mz
    km3=mod(k-4+mz,mz)+1
    km2=mod(k-3+mz,mz)+1
    km1=mod(k-2+mz,mz)+1
    kp1=mod(k     ,mz)+1
    kp2=mod(k+1   ,mz)+1
    do j=1,my
      do i=1,mx
        zdn(i,j,k) = ( &
                     a*(f(i,j,k  )+f(i,j,km1)) + &
                     b*(f(i,j,kp1)+f(i,j,km2)) + &
                     c*(f(i,j,kp2)+f(i,j,km3)))
      end do
    end do
  end do
!
  nstag = nstag+1
  nflop = nflop+8
END FUNCTION

!***********************************************************************
FUNCTION zdn1 (f)
!
!  f is centered on (i,j,k), fifth order stagger
!
  use params
  real, dimension(mx,my,mz):: f, zdn1
!hpf$ distribute(*,*,block):: f, zdn1
!
!-----------------------------------------------------------------------
  a = .5
!
!$omp parallel do private(i,j,k,km1,km2,km3,kp1,kp2)
  do k=1,mz
    km1=mod(k-2+mz,mz)+1
    do j=1,my
      do i=1,mx
        zdn1(i,j,k) = ( &
                     a*(f(i,j,k  )+f(i,j,km1)))
      end do
    end do
  end do
!
  nstag1 = nstag1+1
  nflop = nflop+2
END FUNCTION

!***********************************************************************
FUNCTION ddzup (f)
  use params
  real, dimension(mx,my,mz):: f, ddzup
!hpf$ distribute(*,*,block):: f, ddzup
!
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)/dz
  a1st = 1./dz
  b = b/dz
  c = c/dz
!
!$omp parallel do private(i,j,k,km1,km2,kp1,kp2,kp3)
  do k=1,mz
    km2=mod(k-3+mz,mz)+1
    km1=mod(k-2+mz,mz)+1
    kp1=mod(k     ,mz)+1
    kp2=mod(k+1   ,mz)+1
    kp3=mod(k+2   ,mz)+1
    do j=1,my
      do i=1,mx
        ddzup(i,j,k) = ( &
                       a*(f(i,j,kp1)-f(i,j,k  )) + &
                       b*(f(i,j,kp2)-f(i,j,km1)) + &
                       c*(f(i,j,kp3)-f(i,j,km2)))
      end do
    end do
  end do
!
  nstag = nstag+1
  nflop = nflop+8
END FUNCTION

!***********************************************************************
FUNCTION ddzup1 (f)
  use params
  real, dimension(mx,my,mz):: f, ddzup1
!hpf$ distribute(*,*,block):: f, ddzup1
!
  a = 1./dz
!
!$omp parallel do private(i,j,k,km1,km2,kp1,kp2,kp3)
  do k=1,mz
    km2=mod(k-3+mz,mz)+1
    km1=mod(k-2+mz,mz)+1
    kp1=mod(k     ,mz)+1
    kp2=mod(k+1   ,mz)+1
    kp3=mod(k+2   ,mz)+1
    do j=1,my
      do i=1,mx
        ddzup1(i,j,k) = a*(f(i,j,kp1)-f(i,j,k  ))
      end do
    end do
  end do
!
  nstag1 = nstag1+1
  nflop = nflop+2
END FUNCTION

!***********************************************************************
FUNCTION ddzdn (f)
  use params
  real, dimension(mx,my,mz):: f, ddzdn
!hpf$ distribute(*,*,block):: f, ddzdn
!
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)/dz
  a1st = 1./dz
  b = b/dz
  c = c/dz
!
!$omp parallel do private(i,j,k,km1,km2,km3,kp1,kp2)
  do k=1,mz
    km3=mod(k-4+mz,mz)+1
    km2=mod(k-3+mz,mz)+1
    km1=mod(k-2+mz,mz)+1
    kp1=mod(k     ,mz)+1
    kp2=mod(k+1   ,mz)+1
    do j=1,my
      do i=1,mx
        ddzdn(i,j,k) = ( &
                       a*(f(i,j,k  )-f(i,j,km1)) + &
                       b*(f(i,j,kp1)-f(i,j,km2)) + &
                       c*(f(i,j,kp2)-f(i,j,km3)))
      end do
    end do
  end do
!
  nstag = nstag+1
  nflop = nflop+8
END FUNCTION

!***********************************************************************
FUNCTION ddzdn1 (f)
  use params
  real, dimension(mx,my,mz):: f, ddzdn1
!hpf$ distribute(*,*,block):: f, ddzdn1
!
  a = 1./dz
!
!$omp parallel do private(i,j,k,km1,km2,km3,kp1,kp2)
  do k=1,mz
    km1=mod(k-2+mz,mz)+1
    do j=1,my
      do i=1,mx
        ddzdn1(i,j,k) = a*(f(i,j,k  )-f(i,j,km1))
      end do
    end do
  end do
!
  nstag1 = nstag1+1
  nflop = nflop+2
END FUNCTION

