  use params
  use stagger

  integer:: m
  real, allocatable, dimension(:,:,:):: ux,uy,uz
  real cpu, cpu0, time(2)

  read *,m
  mx=m
  my=m
  mz=m
  allocate (ux(mx,my,mz),uy(mx,my,mz),uz(mx,my,mz))

  ux=0.
  uy=0.
  uz=0.

  dx=2.*pi/mx
  dy=2.*pi/my
  dz=2.*pi/mz

  call init_stagger
  call init_derivs

!$omp parallel do private(ix,iy,iz)
  do iz=1,mz
  do iy=1,my
  do ix=1,mx
    ux(ix,iy,iz)=sin((2.*pi*ix)/mx)
    uy(ix,iy,iz)=cos((2.*pi*(ix-0.5))/mx)
  end do
  end do
  end do

  call ddxdn_set(ux,uz)
  print *,sum((uz-uy)**2)/m
  call ddxdn_set0(ux,uz)
  print *,sum((uz-uy)**2)/m

  END

!***********************************************************************
SUBROUTINE ddxdn_set (f, fp)
  use params
  real, dimension(mx,my,mz):: f, fp
!hpf$ distribute(*,*,block):: f, fp
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = 0.
    return
  end if
!
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)/dx
  a1st = 1./dx
  b = b/dx
  c = c/dx
!
!$omp parallel do private(i,j,k)
  do k=1,mz
   do j=1,my
    fp(mx-1,j,k) = ( &
                 + c*(f(1   ,j,k)-f(mx-4,j,k)) &
                 + b*(f(mx  ,j,k)-f(mx-3,j,k)) &
                 + a*(f(mx-1,j,k)-f(mx-2,j,k)))
    fp(mx  ,j,k) = ( &
                 + c*(f(2   ,j,k)-f(mx-3,j,k)) &
                 + b*(f(1   ,j,k)-f(mx-2,j,k)) &
                 + a*(f(mx  ,j,k)-f(mx-1,j,k)))
    fp(1   ,j,k) = ( &
                 + c*(f(3   ,j,k)-f(mx-2,j,k)) &
                 + b*(f(2   ,j,k)-f(mx-1,j,k)) &
                 + a*(f(1   ,j,k)-f(mx  ,j,k)))
    fp(2  ,j,k) = ( &
                 + c*(f(4   ,j,k)-f(mx-1,j,k)) &
                 + b*(f(3   ,j,k)-f(mx  ,j,k)) &
                 + a*(f(2   ,j,k)-f(1   ,j,k)))
    fp(3  ,j,k) = ( &
                 + c*(f(5   ,j,k)-f(mx  ,j,k)) &
                 + b*(f(4   ,j,k)-f(1   ,j,k)) &
                 + a*(f(3   ,j,k)-f(2   ,j,k)))
   end do
   do j=1,my
    do i=4,mx-2
      fp(i,j,k) = ( &
                 + c*(f(i+2 ,j,k)-f(i-3 ,j,k)) &
                 + b*(f(i+1 ,j,k)-f(i-2 ,j,k)) &
                 + a*(f(i   ,j,k)-f(i-1 ,j,k)))
    end do
   end do
  end do
!
  nstag = nstag+1
  nflop = nflop+8
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddxdn_set0 (f, fp)
  use params
  real, dimension(mx,my,mz):: f, fp
!hpf$ distribute(*,*,block):: f, fp
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = 0.
    return
  end if
!
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)/dx
  a1st = 1./dx
  b = b/dx
  c = c/dx
!
!$omp parallel do private(i,j,k)
  do k=1,mz
   do j=1,my
    fp(mx-1,j,k) = ( &
                 + a*(f(mx-1,j,k)-f(mx-2,j,k)) &
                 + b*(f(mx  ,j,k)-f(mx-3,j,k)) &
                 + c*(f(1   ,j,k)-f(mx-4,j,k)))
    fp(mx  ,j,k) = ( &
                 + a*(f(mx  ,j,k)-f(mx-1,j,k)) &
                 + b*(f(1   ,j,k)-f(mx-2,j,k)) &
                 + c*(f(2   ,j,k)-f(mx-3,j,k)))
    fp(1   ,j,k) = ( &
                 + a*(f(1   ,j,k)-f(mx  ,j,k)) &
                 + b*(f(2   ,j,k)-f(mx-1,j,k)) &
                 + c*(f(3   ,j,k)-f(mx-2,j,k)))
    fp(2  ,j,k) = ( &
                 + a*(f(2   ,j,k)-f(1   ,j,k)) &
                 + b*(f(3   ,j,k)-f(mx  ,j,k)) &
                 + c*(f(4   ,j,k)-f(mx-1,j,k)))
    fp(3  ,j,k) = ( &
                 + a*(f(3   ,j,k)-f(2   ,j,k)) &
                 + b*(f(4   ,j,k)-f(1   ,j,k)) &
                 + c*(f(5   ,j,k)-f(mx  ,j,k)))
   end do
   do j=1,my
    do i=4,mx-2
      fp(i,j,k) = ( &
                 + a*(f(i   ,j,k)-f(i-1 ,j,k)) &
                 + b*(f(i+1 ,j,k)-f(i-2 ,j,k)) &
                 + c*(f(i+2 ,j,k)-f(i-3 ,j,k)))
    end do
   end do
  end do
!
  nstag = nstag+1
  nflop = nflop+8
END SUBROUTINE
