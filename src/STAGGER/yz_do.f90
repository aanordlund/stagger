!***********************************************************************
FUNCTION yzup (f)
!
!  f is centered on (i,j-.5,k), fifth order stagger
!
  use params
  implicit none
  real a, b, c, a1st
  integer i, j, k, km2, km1, kp1, kp2, kp3
  real, intent(in) , dimension(mx,my,mz):: f
  real,              dimension(mx,my,mz):: yzup
!hpf$ distribute(*,*,block):: f, yzup
!-----------------------------------------------------------------------
!
  character(len=mid):: id = "$Id: yz_do.f90,v 1.2 2012/12/31 16:10:47 aake Exp $" 
  if (id.ne.' '.and.izs.eq.1) print '(1x,a)',id; id=' '

  if (my .lt. 5) then
!2omp barrier
!$omp parallel private(k)
    do k=izs,ize
      yzup(:,:,k) = f(:,:,k)
    end do
!$omp end parallel
!2omp barrier
    return
  end if
  c = 3./256.
  b = -25./256.
  a = .5-b-c
  a1st = 0.5
!
!2omp barrier
!$omp parallel private(i,j,k)
  do k=izs,ize
    km2 = mod(k-3+mz,mz)+1
    km1 = mod(k-2+mz,mz)+1
    kp1 = mod(k     ,mz)+1
    kp2 = mod(k+1   ,mz)+1
    kp3 = mod(k+2   ,mz)+1
    do i=1,mx
      yzup(i,my-2,k) = ( &
                     a*(f(i,my-2,k  )+f(i,my-1,kp1)) + &
                     b*(f(i,my-3,km1)+f(i,my  ,kp2)) + &
                     c*(f(i,my-4,km2)+f(i,1   ,kp3)))
      yzup(i,my-1,k) = ( &
                     a*(f(i,my-1,k  )+f(i,my  ,kp1)) + &
                     b*(f(i,my-2,km1)+f(i,1   ,kp2)) + &
                     c*(f(i,my-3,km2)+f(i,2   ,kp3)))
      yzup(i,my  ,k) = ( &
                     a*(f(i,my  ,k  )+f(i,1   ,kp1)) + &
                     b*(f(i,my-1,km1)+f(i,2   ,kp2)) + &
                     c*(f(i,my-2,km2)+f(i,3   ,kp3)))
      yzup(i,1   ,k) = ( &
                     a*(f(i,1   ,k  )+f(i,2   ,kp1)) + &
                     b*(f(i,my  ,km1)+f(i,3   ,kp2)) + &
                     c*(f(i,my-1,km2)+f(i,4   ,kp3)))
      yzup(i,2   ,k) = ( &
                     a*(f(i,2   ,k  )+f(i,3   ,kp1)) + &
                     b*(f(i,1   ,km1)+f(i,4   ,kp2)) + &
                     c*(f(i,my  ,km2)+f(i,5   ,kp3)))
    end do
    do j=3,my-3
     do i=1,mx
      yzup(i,j  ,k) = ( &
                     a*(f(i,j   ,k  )+f(i,j+1 ,kp1)) + &
                     b*(f(i,j-1 ,km1)+f(i,j+2 ,kp2)) + &
                     c*(f(i,j-2 ,km2)+f(i,j+3 ,kp3)))
     end do
    end do
  end do
!$omp end parallel
!2omp barrier
!
  nstag = nstag+1
  nflop = nflop+8
END FUNCTION

!***********************************************************************
FUNCTION yzup1 (f)
!
!  f is centered on (i,j-.5,k), fifth order stagger
!
  use params
  implicit none
  real a, b, c, a1st
  integer i, j, k
  real, intent(in) , dimension(mx,my,mz):: f
  real,              dimension(mx,my,mz):: yzup1
!hpf$ distribute(*,*,block):: f, yzup1
!-----------------------------------------------------------------------
!
  if (my .lt. 5) then
!2omp barrier
!$omp parallel private(k)
    do k=izs,ize
      yzup1(:,:,k) = f(:,:,k)
    end do
!$omp end parallel
!2omp barrier
    return
  end if
!
  a = 0.5
!
!2omp barrier
!$omp parallel private(i,j,k)
  do k=izs,ize
    do i=1,mx
      yzup1(i,my,k) = ( &
                     a*(f(i,my,k)+f(i,1,k)))
    end do
    do j=1,my-1
      do i=1,mx
        yzup1(i,j  ,k) = ( &
                     a*(f(i,j   ,k)+f(i,j+1 ,k)))
      end do
    end do
  end do
!$omp end parallel
!2omp barrier
!
  nstag1 = nstag1+1
  nflop = nflop+2
END FUNCTION

!***********************************************************************
FUNCTION yzdn (f)
!
!  f is centered on (i,j,k), fifth order stagger
!
  use params
  implicit none
  real a, b, c, a1st
  integer i, j, k, km3, km2, km1, kp1, kp2
  real, dimension(mx,my,mz):: f, yzdn
!hpf$ distribute(*,*,block):: f, yzdn
!
!-----------------------------------------------------------------------
  if (my .lt. 5) then
!2omp barrier
!$omp parallel private(k)
    do k=izs,ize
      yzdn(:,:,k) = f(:,:,k)
    end do
!$omp end parallel
!2omp barrier
    return
  end if
  c = 3./256.
  b = -25./256.
  a = .5-b-c
  a1st = 0.5
!
!2omp barrier
!$omp parallel private(i,j,k)
  do k=izs,ize
   km3 = mod(k-4+mz,mz)+1
   km2 = mod(k-3+mz,mz)+1
   km1 = mod(k-2+mz,mz)+1
   kp1 = mod(k     ,mz)+1
   kp2 = mod(k+1   ,mz)+1
    do i=1,mx
      yzdn(i,my-1,k) = ( &
                     a*(f(i,my-2,k)+f(i,my-1,km1)) + &
                     b*(f(i,my-3,k)+f(i,my  ,km2)) + &
                     c*(f(i,my-4,k)+f(i,1   ,km3)))
      yzdn(i,my  ,k) = ( &
                     a*(f(i,my-1,k)+f(i,my  ,km1)) + &
                     b*(f(i,my-2,k)+f(i,1   ,km2)) + &
                     c*(f(i,my-3,k)+f(i,2   ,km3)))
      yzdn(i,1   ,k) = ( &
                     a*(f(i,my  ,k)+f(i,1   ,km1)) + &
                     b*(f(i,my-1,k)+f(i,2   ,km2)) + &
                     c*(f(i,my-2,k)+f(i,3   ,km3)))
      yzdn(i,2   ,k) = ( &
                     a*(f(i,1   ,k)+f(i,2   ,km1)) + &
                     b*(f(i,my  ,k)+f(i,3   ,km2)) + &
                     c*(f(i,my-1,k)+f(i,4   ,km3)))
      yzdn(i,3   ,k) = ( &
                     a*(f(i,2   ,k)+f(i,3   ,km1)) + &
                     b*(f(i,1   ,k)+f(i,4   ,km2)) + &
                     c*(f(i,my  ,k)+f(i,5   ,km3)))
    end do
    do j=4,my-2
     do i=1,mx
      yzdn(i,j,k) = ( &
                     a*(f(i,j   ,k  )+f(i,j-1 ,km1)) + &
                     b*(f(i,j+1 ,kp1)+f(i,j-2 ,km2)) + &
                     c*(f(i,j+2 ,kp2)+f(i,j-3 ,km3)))
     end do
    end do
  end do
!$omp end parallel
!2omp barrier
!
  nstag = nstag+1
  nflop = nflop+8
END FUNCTION

!***********************************************************************
FUNCTION yzdn1 (f)
!
!  f is centered on (i,j,k), fifth order stagger
!
  use params
  implicit none
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f, yzdn1
!hpf$ distribute(*,*,block):: f, yzdn1
!
!-----------------------------------------------------------------------
  if (my .lt. 5) then
!2omp barrier
!$omp parallel private(k)
    do k=izs,ize
      yzdn1(:,:,k) = f(:,:,k)
    end do
!$omp end parallel
!2omp barrier
    return
  end if
!
  a = 0.5
!
!2omp barrier
!$omp parallel private(i,j,k)
  do k=izs,ize
    do i=1,mx
      yzdn1(i,1   ,k) = ( &
                     a*(f(i,my  ,k)+f(i,1   ,k)))
    end do
    do j=2,my
     do i=1,mx
      yzdn1(i,j,k) = ( &
                     a*(f(i,j   ,k)+f(i,j-1 ,k)))
     end do
    end do
  end do
!$omp end parallel
!2omp barrier
!
  nstag1 = nstag1+1
  nflop = nflop+2
END FUNCTION
