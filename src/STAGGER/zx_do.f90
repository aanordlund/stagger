!***********************************************************************
FUNCTION zxup (f)
!
!  f is centered on (i-.5,j,k), fifth order stagger
!
  use params
  implicit none
  real a, b, c, a1st
  integer i, j, k, km2, km1, kp1, kp2, kp3
  real, dimension(mx,my,mz):: f, zxup
!hpf$ distribute(*,*,block):: f, zxup
!-----------------------------------------------------------------------
!
  character(len=mid):: id = "$Id: zx_do.f90,v 1.3 2012/12/31 16:10:48 aake Exp $" 
  if (id.ne.' '.and.izs.eq.1) print '(1x,a)',id; id=' '
!
  if (mz.le.5) then
    zxup = f
    return
  end if
  c = 3./256.
  b = -25./256.
  a = .5-b-c
  a1st = 0.5
!
!2omp barrier
!$omp parallel private(i,j,k,km2,km1,kp1,kp2,kp3)
  do j=1,my
   do i=1,mx
    im2 = mod(i-3+mx,mx)+1
    im1 = mod(i-2+mx,mx)+1
    ip1 = mod(i     ,mx)+1
    ip2 = mod(i+1   ,mx)+1
    ip3 = mod(i+2   ,mx)+1
    zxup(i,j,mz-2) = ( &
                   a*(f(i  ,j,mz-2)+f(ip1,j,mz-1)) &
                 + b*(f(im1,j,mz-3)+f(ip2,j,mz  )) &
                 + c*(f(im2,j,mz-4)+f(ip3,j,1   )))
    zxup(i,j,mz-1) = ( &
                   a*(f(i  ,j,mz-1)+f(ip1,j,mz  )) &
                 + b*(f(im1,j,mz-2)+f(ip2,j,1   )) &
                 + c*(f(im2,j,mz-3)+f(ip3,j,2   )))
    zxup(i,j,mz  ) = ( &
                   a*(f(i  ,j,mz  )+f(ip1,j,1   )) &
                 + b*(f(im1,j,mz-1)+f(ip2,j,2   )) &
                 + c*(f(im2,j,mz-2)+f(ip3,j,3   )))
    zxup(i,j,1  ) = ( &
                   a*(f(i  ,j,1   )+f(ip1,j,2   )) &
                 + b*(f(im1,j,mz  )+f(ip2,j,3   )) &
                 + c*(f(im2,j,mz-1)+f(ip3,j,4   )))
    zxup(i,j,2  ) = ( &
                   a*(f(i  ,j,2   )+f(ip1,j,3   )) &
                 + b*(f(im1,j,1   )+f(ip2,j,4   )) &
                 + c*(f(im2,j,mz  )+f(ip3,j,5   )))
    do k=3,mz-3
      zxup(i  ,j,k) = ( &
                    a*(f(i  ,j,k  )+f(ip1,j,k+1)) &
                  + b*(f(im1,j,k-1)+f(ip2,j,k+2)) &
                  + c*(f(im2,j,k-2)+f(ip3,j,k+3)))
    end do
   end do
  end do
!$omp end parallel
!2omp barrier
!
  nstag = nstag+1
  nflop = nflop+8
END FUNCTION

!***********************************************************************
FUNCTION zxup1 (f)
!
!  f is centered on (i-.5,j,k), fifth order stagger
!
  use params
  implicit none
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f, zxup1
!hpf$ distribute(*,*,block):: f, zxup1
!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    zxup1 = f
    return
  end if
!
  a = .5
!
!2omp barrier
!$omp parallel private(i,j,k)
  do j=1,my
   do i=1,mx
    zxup1(i,j,mz) = ( &
                   a*(f(i,j,mz)+f(i,j,1)))
   end do
   do k=1,mz-1
    do i=1,mx
      zxup1(i,j,k) = ( &
                   a*(f(i,j,k)+f(i,j,k+1)))
    end do
   end do
  end do
!$omp end parallel
!2omp barrier
!
  nstag1 = nstag1+1
  nflop = nflop+2
END FUNCTION

!***********************************************************************
FUNCTION zxdn (f)
!
!  f is centered on (i,j,k), fifth order stagger
!
  use params
  implicit none
  real a, b, c, a1st
  integer i, j, k, km3, km2, km1, kp1, kp2
  real, dimension(mx,my,mz):: f, zxdn
!hpf$ distribute(*,*,block):: f, zxdn
!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    zxdn = f
    return
  end if
!
  c = 3./256.
  b = -25./256.
  a = .5-b-c
  a1st = 0.5
!
!2omp barrier
!$omp parallel private(i,j,k,km3,km2,km1,kp1,kp2)
  do j=1,my
   do i=1,mx
    im2 = mod(i-3+mx,mx)+1
    im1 = mod(i-2+mx,mx)+1
    ip1 = mod(i     ,mx)+1
    ip2 = mod(i+1   ,mx)+1
    ip3 = mod(i+2   ,mx)+1
    zxup(i,j,mz-2) = ( &
                   a*(f(i  ,j,mz-2)+f(im1,j,mz-1)) &
                 + b*(f(ip1,j,mz-3)+f(im2,j,mz  )) &
                 + c*(f(ip2,j,mz-4)+f(im3,j,1   )))
    zxup(i,j,mz-1) = ( &
                   a*(f(i  ,j,mz-1)+f(im1,j,mz  )) &
                 + b*(f(ip1,j,mz-2)+f(im2,j,1   )) &
                 + c*(f(ip2,j,mz-3)+f(im3,j,2  )))
    zxup(i,j,mz  ) = ( &
                   a*(f(i  ,j,m1  )+f(im1,j,1   )) &
                 + b*(f(ip1,j,mz-1)+f(im2,j,2   )) &
                 + c*(f(ip2,j,mz-2)+f(im3,j,3   )))
    zxup(i,j,1  ) = ( &
                  a*(f(i  ,j,1   )+f(im1,j,2   )) &
                + b*(f(ip1,j,mz  )+f(im2,j,3   )) &
                + c*(f(ip2,j,mz-1)+f(im3,j,4   )))
    zxup(i,j,2  ) = ( &
                  a*(f(i  ,j,2   )+f(im1,j,3   )) &
                + b*(f(ip1,j,1   )+f(im2,j,4   )) &
                + c*(f(ip2,j,mz  )+f(im3,j,5   )))
    do k=3,mz-3
      zxup(i,j,k) = ( &
                  a*(f(i  ,j,k   )+f(im1,j,k-1 )) &
                + b*(f(ip1,j,k+1 )+f(im2,j,k-2 )) &
                + c*(f(ip2,j,k+2 )+f(im3,j,k-3 )))
    end do
   end do
  end do
!$omp end parallel
!2omp barrier
!
!
  nstag = nstag+1
  nflop = nflop+8
END FUNCTION

!***********************************************************************
FUNCTION zxdn1 (f)
!
!  f is centered on (i,j,k), fifth order stagger
!
  use params
  implicit none
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f, zxdn1
!hpf$ distribute(*,*,block):: f, zxdn1
!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    zxdn1 = f
    return
  end if
!
  a = 0.5
!
!2omp barrier
!$omp parallel private(i,j,k)
  do j=1,my
   do i=1,mx
    zxdn1(1  ,j,k) = ( &
                   a*(f(mx  ,j,k)+f(1   ,j,k)))
   end do
   do k=2,mz
    do i=1,mx
      zxdn1(i,j,k) = ( &
                   a*(f(i,j,k)+f(i,j,k-1)))
    end do
   end do
  end do
!$omp end parallel
!2omp barrier
!
  nstag1 = nstag1+1
  nflop = nflop+2
END FUNCTION
