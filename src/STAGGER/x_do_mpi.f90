!***********************************************************************
FUNCTION xup (f)
!
!  f is centered on (i-.5,j,k), fifth order stagger
!
  use params, g=>gx2, h=>hx3
  implicit none
  real a, b, c
  integer i, j, k
  real, dimension(mx,my,mz):: f, xup
!-----------------------------------------------------------------------
!
  character(len=mid):: id = "$Id: x_do_mpi.f90,v 1.7 2014/08/26 21:51:48 aake Exp $" 
  call print_id (id)
!
  if (mx.le.5) then
    xup = f
    return
  end if
  c = 3./256.
  b = -25./256.
  a = .5-b-c

!$omp parallel private(i,j,k)

  call mpi_send_x_begin (f, g, 2, h, 3)

  do k=izs,ize
   do j=1,my
    do i=3,mx-3
      xup(i  ,j,k) = ( &
                    c*(f(i-2,j,k)+f(i+3 ,j,k)) + &
                    b*(f(i-1,j,k)+f(i+2 ,j,k)) + &
                    a*(f(i  ,j,k)+f(i+1 ,j,k)))
    end do
   end do
  end do

  call mpi_send_x_end (f, g, 2, h, 3)

  do k=izs,ize
   do j=1,my
    xup(mx-2,j,k) = ( &
                   c*(f(mx-4,j,k)+h(1   ,j,k)) + &
                   b*(f(mx-3,j,k)+f(mx  ,j,k)) + &
                   a*(f(mx-2,j,k)+f(mx-1,j,k)))
    xup(mx-1,j,k) = ( &
                   c*(f(mx-3,j,k)+h(2   ,j,k)) + &
                   b*(f(mx-2,j,k)+h(1   ,j,k)) + &
                   a*(f(mx-1,j,k)+f(mx  ,j,k)))
    xup(mx  ,j,k) = ( &
                   c*(f(mx-2,j,k)+h(3   ,j,k)) + &
                   b*(f(mx-1,j,k)+h(2   ,j,k)) + &
                   a*(f(mx  ,j,k)+h(1   ,j,k)))
    xup(1   ,j,k) = ( &
                   c*(g(1   ,j,k)+f(4   ,j,k)) + &
                   b*(g(2   ,j,k)+f(3   ,j,k)) + &
                   a*(f(1   ,j,k)+f(2   ,j,k)))
    xup(2   ,j,k) = ( &
                   c*(g(2   ,j,k)+f(5   ,j,k)) + &
                   b*(f(1   ,j,k)+f(4   ,j,k)) + &
                   a*(f(2   ,j,k)+f(3   ,j,k)))
   end do
  end do

  !$omp end parallel
!
  nstag = nstag+1
  nflop = nflop+8
END FUNCTION

!***********************************************************************
FUNCTION xup1 (f)
!
!  f is centered on (i-.5,j,k), first order stagger
!
  use params, h=>hx1
  implicit none
  real a, b, c
  integer i, j, k
  real, dimension(mx,my,mz):: f, xup1
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    xup1 = f
    return
  end if
!
  a = .5

!$omp parallel private(i,j,k)

  call mpi_send_x_begin (f, h, 0, h, 1)

  do k=izs,ize
   do j=1,my
    do i=1,mx-1
      xup1(i  ,j,k) = ( &
                    a*(f(i ,j,k)+f(i+1,j,k)))
    end do
   end do
  end do

  call mpi_send_x_end (f, h, 0, h, 1)

  do k=izs,ize
   do j=1,my
    xup1(mx  ,j,k) =  ( &
                    a*(f(mx,j,k)+h(  1,j,k)))
   end do
  end do
  
!$omp end parallel
!
  nstag1 = nstag1+1
  nflop = nflop+2
END FUNCTION

!***********************************************************************
FUNCTION xdn (f)
!
!  f is centered on (i,j,k), fifth order stagger
!
  use params, g=>gx3, h=>hx2
  implicit none
  real a, b, c
  integer i, j, k
  real, dimension(mx,my,mz):: f, xdn
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    xdn = f
    return
  end if
!
  c = 3./256.
  b = -25./256.
  a = .5-b-c

!$omp parallel private(i,j,k)

  call mpi_send_x_begin (f, g, 3, h, 2)

  do k=izs,ize
   do j=1,my
    do i=4,mx-2
      xdn(i,j,k) = ( &
                   c*(f(i+2 ,j,k)+f(i-3 ,j,k)) + &
                   b*(f(i+1 ,j,k)+f(i-2 ,j,k)) + &
                   a*(f(i   ,j,k)+f(i-1 ,j,k)))
    end do
   end do
  end do

  call mpi_send_x_end (f, g, 3, h, 2)
!
  do k=izs,ize
   do j=1,my
    xdn(mx-1,j,k) = ( &
                   c*(f(mx-4,j,k)+h(1   ,j,k)) + &
                   b*(f(mx-3,j,k)+f(mx  ,j,k)) + &
                   a*(f(mx-2,j,k)+f(mx-1,j,k)))
    xdn(mx  ,j,k) = ( &
                   c*(f(mx-3,j,k)+h(2   ,j,k)) + &
                   b*(f(mx-2,j,k)+h(1   ,j,k)) + &
                   a*(f(mx-1,j,k)+f(mx  ,j,k)))
    xdn(1  ,j,k) = ( &
                   c*(g(1   ,j,k)+f(3   ,j,k)) + &
                   b*(g(2   ,j,k)+f(2   ,j,k)) + &
                   a*(g(3   ,j,k)+f(1   ,j,k)))
    xdn(2  ,j,k) = ( &
                   c*(g(2   ,j,k)+f(4   ,j,k)) + &
                   b*(g(3   ,j,k)+f(3   ,j,k)) + &
                   a*(f(1   ,j,k)+f(2   ,j,k)))
    xdn(3  ,j,k) = ( &
                   c*(g(3   ,j,k)+f(5   ,j,k)) + &
                   b*(f(1   ,j,k)+f(4   ,j,k)) + &
                   a*(f(2   ,j,k)+f(3   ,j,k)))
   end do
  end do

  !$omp end parallel
!
!
  nstag = nstag+1
  nflop = nflop+8
END FUNCTION

!***********************************************************************
FUNCTION xdn1 (f)
!
!  f is centered on (i,j,k), first order stagger
!
  use params, g=>gx1
  implicit none
  real a, b, c
  integer i, j, k
  real, dimension(mx,my,mz):: f, xdn1
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    xdn1 = f
    return
  end if
!
  a = 0.5

!$omp parallel private(i,j,k)

  call mpi_send_x_begin (f, g, 1, g, 0)
  
  do k=izs,ize
   do j=1,my
    do i=2,mx
      xdn1(i,j,k) = ( &
                   a*(f(i,j,k)+f(i-1,j,k)))
    end do
   end do
  end do

  call mpi_send_x_end (f, g, 1, g, 0)

  do k=izs,ize
   do j=1,my
    xdn1(1  ,j,k) = ( &
                   a*(g(1,j,k)+f(1   ,j,k)))
   end do
  end do
!$omp end parallel
!
  nstag1 = nstag1+1
  nflop = nflop+2
END FUNCTION

!***********************************************************************
FUNCTION xup32 (f,j)
!
!  f is centered on (i-.5,j,k), fifth order stagger
!
  use params, g=>gx2, h=>hx3
  implicit none
  real a, b, c
  integer i, j, k
  real, dimension(mx,my,mz):: f
  real, dimension(mx,mz):: xup32
!-----------------------------------------------------------------------
!
  character(len=mid):: id = "$Id: x_do_mpi.f90,v 1.7 2014/08/26 21:51:48 aake Exp $" 
  call print_id (id)
!
  if (mx.le.5) then
    xup32 = f(:,j,:)
    return
  end if
  c = 3./256.
  b = -25./256.
  a = .5-b-c
!
!$omp parallel private(i,j,k)

  call mpi_send_x (f, g, 2, h, 3)

  do k=izs,ize
    xup32(mx-2,k) = ( &
                   c*(f(mx-4,j,k)+h(1   ,j,k)) + &
                   b*(f(mx-3,j,k)+f(mx  ,j,k)) + &
                   a*(f(mx-2,j,k)+f(mx-1,j,k)))
    xup32(mx-1,k) = ( &
                   c*(f(mx-3,j,k)+h(2   ,j,k)) + &
                   b*(f(mx-2,j,k)+h(1   ,j,k)) + &
                   a*(f(mx-1,j,k)+f(mx  ,j,k)))
    xup32(mx  ,k) = ( &
                   c*(f(mx-2,j,k)+h(3   ,j,k)) + &
                   b*(f(mx-1,j,k)+h(2   ,j,k)) + &
                   a*(f(mx  ,j,k)+h(1   ,j,k)))
    xup32(1   ,k) = ( &
                   c*(g(1   ,j,k)+f(4   ,j,k)) + &
                   b*(g(2   ,j,k)+f(3   ,j,k)) + &
                   a*(f(1   ,j,k)+f(2   ,j,k)))
    xup32(2   ,k) = ( &
                   c*(g(2   ,j,k)+f(5   ,j,k)) + &
                   b*(f(1   ,j,k)+f(4   ,j,k)) + &
                   a*(f(2   ,j,k)+f(3   ,j,k)))
    do i=3,mx-3
      xup32(i  ,k) = ( &
                    c*(f(i-2,j,k)+f(i+3 ,j,k)) + &
                    b*(f(i-1,j,k)+f(i+2 ,j,k)) + &
                    a*(f(i  ,j,k)+f(i+1 ,j,k)))
    end do
  end do
!$omp end parallel
!
  if (omp_master) then
      nstag = nstag+1
      nflop = nflop+8
  end if
END FUNCTION

!***********************************************************************
FUNCTION xup132 (f,j)
!
!  f is centered on (i-.5,j,k), fifth order stagger
!
  use params, h=>hx1
  implicit none
  real a, b, c
  integer i, j, k
  real, dimension(mx,my,mz):: f
  real, dimension(mx,mz):: xup132
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    xup132 = f(:,j,:)
    return
  end if
!
  a = .5
!
!$omp parallel private(i,j,k)

  call mpi_send_x (f, h, 0, h, 1)

  do k=izs,ize
    xup132(mx  ,k) = ( &
                   a*(f(mx  ,j,k)+h(1   ,j,k)))
    do i=1,mx-1
      xup132(i  ,k) = ( &
                    a*(f(i  ,j,k)+f(i+1 ,j,k)))
    end do
  end do
!$omp end parallel
!
  if (omp_master) then
    nstag1 = nstag1+1
    nflop = nflop+2
  end if
END FUNCTION

!***********************************************************************
FUNCTION xdn32 (f,j)
!
!  f is centered on (i,j,k), fifth order stagger
!
  use params, g=>gx3, h=>hx2
  implicit none
  real a, b, c
  integer i, j, k
  real, dimension(mx,my,mz):: f
  real, dimension(mx,mz):: xdn32
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    xdn32 = f(:,j,:)
    return
  end if
!
  c = 3./256.
  b = -25./256.
  a = .5-b-c
!
!$omp parallel private(i,j,k)

  call mpi_send_x (f, g, 3, h, 2)

  do k=izs,ize
    xdn32(mx-1,k) = ( &
                   c*(f(mx-4,j,k)+h(1   ,j,k)) + &
                   b*(f(mx-3,j,k)+f(mx  ,j,k)) + &
                   a*(f(mx-2,j,k)+f(mx-1,j,k)))
    xdn32(mx  ,k) = ( &
                   c*(f(mx-3,j,k)+h(2   ,j,k)) + &
                   b*(f(mx-2,j,k)+h(1   ,j,k)) + &
                   a*(f(mx-1,j,k)+f(mx  ,j,k)))
    xdn32(1  ,k) = ( &
                   c*(g(1   ,j,k)+f(3   ,j,k)) + &
                   b*(g(2   ,j,k)+f(2   ,j,k)) + &
                   a*(g(3   ,j,k)+f(1   ,j,k)))
    xdn32(2  ,k) = ( &
                   c*(g(2   ,j,k)+f(4   ,j,k)) + &
                   b*(g(3   ,j,k)+f(3   ,j,k)) + &
                   a*(f(1   ,j,k)+f(2   ,j,k)))
    xdn32(3  ,k) = ( &
                   c*(g(3   ,j,k)+f(5   ,j,k)) + &
                   b*(f(1   ,j,k)+f(4   ,j,k)) + &
                   a*(f(2   ,j,k)+f(3   ,j,k)))
    do i=4,mx-2
      xdn32(i,k) = ( &
                   c*(f(i+2 ,j,k)+f(i-3 ,j,k)) + &
                   b*(f(i+1 ,j,k)+f(i-2 ,j,k)) + &
                   a*(f(i   ,j,k)+f(i-1 ,j,k)))
    end do
  end do
!$omp end parallel
!
!
  if (omp_master) then
    nstag = nstag+1
    nflop = nflop+8
  end if
END FUNCTION

!***********************************************************************
FUNCTION xdn132 (f,j)
!
!  f is centered on (i,j,k), fifth order stagger
!
  use params, g=>gx1
  implicit none
  real a, b, c
  integer i, j, k
  real, dimension(mx,my,mz):: f
  real, dimension(mx,mz):: xdn132
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    xdn132 = f(:,j,:)
    return
  end if
!
  a = 0.5
!
!$omp parallel private(i,j,k)

  call mpi_send_x (f, g, 1, g, 0)

  do k=izs,ize
    xdn132(1  ,k) = ( &
                   a*(g(1   ,j,k)+f(1   ,j,k)))
    do i=2,mx
      xdn132(i,k) = ( &
                   a*(f(i   ,j,k)+f(i-1 ,j,k)))
    end do
  end do
!$omp end parallel
!
  if (omp_master) then
    nstag1 = nstag1+1
    nflop = nflop+2
  end if
END FUNCTION

!***********************************************************************
FUNCTION xup22 (f)
!
!  f is centered on (i-.5,k), fifth order stagger
!
  use params, g=>gx2, h=> hx3
  implicit none
  real a, b, c
  integer i, j, k
  real, dimension(mx,mz):: xup22, f
!-----------------------------------------------------------------------
!
  character(len=mid):: id = "$Id: x_do_mpi.f90,v 1.7 2014/08/26 21:51:48 aake Exp $" 
  call print_id (id)
!
  if (mx.le.5) then
    xup22 = f
    return
  end if
  c = 3./256.
  b = -25./256.
  a = .5-b-c
!
!$omp parallel private(i,k)

  call mpi_send2_x (f, mx, 1, mz, g, 2, h, 3)

  do k=izs,ize
    xup22(mx-2,k) = ( &
                   c*(f(mx-4,k)+h(1 ,1,k)) + &
                   b*(f(mx-3,k)+f(mx  ,k)) + &
                   a*(f(mx-2,k)+f(mx-1,k)))
    xup22(mx-1,k) = ( &
                   c*(f(mx-3,k)+h(2 ,1,k)) + &
                   b*(f(mx-2,k)+h(1 ,1,k)) + &
                   a*(f(mx-1,k)+f(mx  ,k)))
    xup22(mx  ,k) = ( &
                   c*(f(mx-2,k)+h(3 ,1,k)) + &
                   b*(f(mx-1,k)+h(2 ,1,k)) + &
                   a*(f(mx  ,k)+h(1 ,1,k)))
    xup22(1   ,k) = ( &
                   c*(g(1 ,1,k)+f(4   ,k)) + &
                   b*(g(2 ,1,k)+f(3   ,k)) + &
                   a*(f(1   ,k)+f(2   ,k)))
    xup22(2   ,k) = ( &
                   c*(g(2 ,1,k)+f(5   ,k)) + &
                   b*(f(1   ,k)+f(4   ,k)) + &
                   a*(f(2   ,k)+f(3   ,k)))
    do i=3,mx-3
      xup22(i  ,k) = ( &
                    c*(f(i-2,k)+f(i+3 ,k)) + &
                    b*(f(i-1,k)+f(i+2 ,k)) + &
                    a*(f(i  ,k)+f(i+1 ,k)))
    end do
  end do
!$omp end parallel
!
  if (omp_master) then
      nstag = nstag+1
      nflop = nflop+8
  end if
END FUNCTION

!***********************************************************************
FUNCTION xup122 (f)
!
!  f is centered on (i-.5,k), fifth order stagger
!
  use params, h=> hx1
  implicit none
  real a, b, c
  integer i, j, k
  real, dimension(mx,mz):: xup122, f
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    xup122 = f
    return
  end if
!
  a = .5
!
!$omp parallel private(i,k)

  call mpi_send2_x (f, mx, 1, mz, h, 0, h, 1)

  do k=izs,ize
    xup122(mx  ,k) = ( &
                   a*(f(mx  ,k)+h(1 ,1,k)))
    do i=1,mx-1
      xup122(i  ,k) = ( &
                    a*(f(i  ,k)+f(i+1 ,k)))
    end do
  end do
!$omp end parallel
!
  if (omp_master) then
    nstag1 = nstag1+1
    nflop = nflop+2
  end if
END FUNCTION

!***********************************************************************
FUNCTION xdn22 (f)
!
!  f is centered on (i,k), fifth order stagger
!
  use params, g=>gx3, h=>hx2
  implicit none
  real a, b, c
  integer i, j, k
  real, dimension(mx,mz):: xdn22, f
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    xdn22 = f
    return
  end if
!
  c = 3./256.
  b = -25./256.
  a = .5-b-c
!
!$omp parallel private(i,k)

  call mpi_send2_x (f, mx, 1, mz, g, 3, h, 2)

  do k=izs,ize
    xdn22(mx-1,k) = ( &
                   c*(f(mx-4,k)+h(1 ,1,k)) + &
                   b*(f(mx-3,k)+f(mx  ,k)) + &
                   a*(f(mx-2,k)+f(mx-1,k)))
    xdn22(mx  ,k) = ( &
                   c*(f(mx-3,k)+h(2 ,1,k)) + &
                   b*(f(mx-2,k)+h(1 ,1,k)) + &
                   a*(f(mx-1,k)+f(mx  ,k)))
    xdn22(1  ,k) = ( &
                   c*(g(1 ,1,k)+f(3   ,k)) + &
                   b*(g(2 ,1,k)+f(2   ,k)) + &
                   a*(g(3 ,1,k)+f(1   ,k)))
    xdn22(2  ,k) = ( &
                   c*(g(2 ,1,k)+f(4   ,k)) + &
                   b*(g(3 ,1,k)+f(3   ,k)) + &
                   a*(f(1   ,k)+f(2   ,k)))
    xdn22(3  ,k) = ( &
                   c*(g(3 ,1,k)+f(5   ,k)) + &
                   b*(f(1   ,k)+f(4   ,k)) + &
                   a*(f(2   ,k)+f(3   ,k)))
    do i=4,mx-2
      xdn22(i,k) = ( &
                   c*(f(i+2 ,k)+f(i-3 ,k)) + &
                   b*(f(i+1 ,k)+f(i-2 ,k)) + &
                   a*(f(i   ,k)+f(i-1 ,k)))
    end do
  end do
!$omp end parallel
!
!
  if (omp_master) then
    nstag = nstag+1
    nflop = nflop+8
  end if
END FUNCTION

!***********************************************************************
FUNCTION xdn122 (f)
!
!  f is centered on (i,k), fifth order stagger
!
  use params, g=>gx1
  implicit none
  real a, b, c
  integer i, j, k
  real, dimension(mx,mz):: xdn122, f
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    xdn122 = f
    return
  end if
!
  a = 0.5
!
!$omp parallel private(i,k)

  call mpi_send2_x (f, mx, 1, mz, g, 1, g, 0)

  do k=izs,ize
    xdn122(1  ,k) = ( &
                   a*(g(1 ,1,k)+f(1   ,k)))
    do i=2,mx
      xdn122(i,k) = ( &
                   a*(f(i   ,k)+f(i-1 ,k)))
    end do
  end do
!$omp end parallel
!
  if (omp_master) then
    nstag1 = nstag1+1
    nflop = nflop+2
  end if
END FUNCTION
