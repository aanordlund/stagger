!***********************************************************************
FUNCTION ddxup (f)
  use params, g=>gx2, h=>hx3
  implicit none
  real a, b, c
  integer i, j, k
  real, dimension(mx,my,mz):: f, ddxup
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    !$omp parallel
    ddxup(:,:,izs:ize) = 0.
    !$omp end parallel
    return
  end if
!
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)

!$omp parallel private(i,j,k)
  call mpi_send_x (f, g, 2, h, 3)
!
  do k=izs,ize
   do j=1,my
    ddxup(mx-2,j,k) = dxidxup(mx-2)*( &
                 + c*(h(1   ,j,k)-f(mx-4,j,k)) &
                 + b*(f(mx  ,j,k)-f(mx-3,j,k)) &
                 + a*(f(mx-1,j,k)-f(mx-2,j,k)))
    ddxup(mx-1,j,k) = dxidxup(mx-1)*( &
                 + c*(h(2   ,j,k)-f(mx-3,j,k)) &
                 + b*(h(1   ,j,k)-f(mx-2,j,k)) &
                 + a*(f(mx  ,j,k)-f(mx-1,j,k)))
    ddxup(mx  ,j,k) = dxidxup(mx)*( &
                 + c*(h(3   ,j,k)-f(mx-2,j,k)) &
                 + b*(h(2   ,j,k)-f(mx-1,j,k)) &
                 + a*(h(1   ,j,k)-f(mx  ,j,k)))
    ddxup(1  ,j,k) = dxidxup(1)*( &
                 + c*(f(4   ,j,k)-g(1   ,j,k)) &
                 + b*(f(3   ,j,k)-g(2   ,j,k)) &
                 + a*(f(2   ,j,k)-f(1   ,j,k)))
    ddxup(2  ,j,k) = dxidxup(2)*( &
                 + c*(f(5   ,j,k)-g(2   ,j,k)) &
                 + b*(f(4   ,j,k)-f(1   ,j,k)) &
                 + a*(f(3   ,j,k)-f(2   ,j,k)))
   end do
   do j=1,my
    do i=3,mx-3
      ddxup(i  ,j,k) = dxidxup(i)*( &
                 + c*(f(i+3 ,j,k)-f(i-2 ,j,k)) &
                 + b*(f(i+2 ,j,k)-f(i-1 ,j,k)) &
                 + a*(f(i+1 ,j,k)-f(i   ,j,k)))
    end do
  end do
  end do
!$omp end parallel
!
  if (omp_master) then
    nstag = nstag+1
    nflop = nflop+8
  end if
END FUNCTION

!***********************************************************************
FUNCTION ddxup1 (f)
  use params, g=>gx1
  implicit none
  real a, b, c
  integer i, j, k
  real, dimension(mx,my,mz):: f, ddxup1
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    !$omp parallel
    ddxup1(:,:,izs:ize) = 0.
    !$omp end parallel
    return
  end if
  a = 1.

!$omp parallel private(i,j,k)
  call mpi_send_x (f, g, 0, g, 1)
!
  do k=izs,ize
   do j=1,my
    ddxup1(mx  ,j,k) = dxidxup(mx)*( &
                   a*(g(1  ,j,k)-f(mx,j,k)))
   end do
   do j=1,my
    do i=1,mx-1
      ddxup1(i  ,j,k) = dxidxup(i)*( &
                   a*(f(i+1,j,k)-f(i ,j,k)))
    end do
  end do
  end do
!$omp end parallel
!
  if (omp_master) then
    nstag1 = nstag1+1
    nflop = nflop+2
  end if
END FUNCTION

!***********************************************************************
FUNCTION ddxdn (f)
  use params, g=>gx3, h=>hx2
  implicit none
  real a, b, c
  integer i, j, k
  real, dimension(mx,my,mz):: f, ddxdn
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    !$omp parallel
    ddxdn(:,:,izs:ize) = 0.
    !$omp end parallel
    return
  end if
!
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)

!$omp parallel private(i,j,k)
  call mpi_send_x (f, g, 3, h, 2)
!
  do k=izs,ize
   do j=1,my
    ddxdn(mx-1,j,k) = dxidxdn(mx-1)*( &
                 + c*(h(1   ,j,k)-f(mx-4,j,k)) &
                 + b*(f(mx  ,j,k)-f(mx-3,j,k)) &
                 + a*(f(mx-1,j,k)-f(mx-2,j,k)))
    ddxdn(mx  ,j,k) = dxidxdn(mx)*( &
                 + c*(h(2   ,j,k)-f(mx-3,j,k)) &
                 + b*(h(1   ,j,k)-f(mx-2,j,k)) &
                 + a*(f(mx  ,j,k)-f(mx-1,j,k)))
    ddxdn(1   ,j,k) = dxidxdn(1)*( &
                 + c*(f(3   ,j,k)-g(1   ,j,k)) &
                 + b*(f(2   ,j,k)-g(2   ,j,k)) &
                 + a*(f(1   ,j,k)-g(3   ,j,k)))
    ddxdn(2  ,j,k) = dxidxdn(2)*( &
                 + c*(f(4   ,j,k)-g(2   ,j,k)) &
                 + b*(f(3   ,j,k)-g(3   ,j,k)) &
                 + a*(f(2   ,j,k)-f(1   ,j,k)))
    ddxdn(3  ,j,k) = dxidxdn(3)*( &
                 + c*(f(5   ,j,k)-g(3   ,j,k)) &
                 + b*(f(4   ,j,k)-f(1   ,j,k)) &
                 + a*(f(3   ,j,k)-f(2   ,j,k)))
   end do
   do j=1,my
    do i=4,mx-2
      ddxdn(i,j,k) = dxidxdn(i)*( &
                 + c*(f(i+2 ,j,k)-f(i-3 ,j,k)) &
                 + b*(f(i+1 ,j,k)-f(i-2 ,j,k)) &
                 + a*(f(i   ,j,k)-f(i-1 ,j,k)))
    end do
   end do
  end do
!$omp end parallel
!
  if (omp_master) then
    nstag = nstag+1
    nflop = nflop+8
  end if
END FUNCTION

!***********************************************************************
FUNCTION ddxdn1 (f)
  use params, g=>gx1
  implicit none
  real a, b, c
  integer i, j, k
  real, dimension(mx,my,mz):: f, ddxdn1
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    !$omp parallel
    ddxdn1(:,:,izs:ize) = 0.
    !$omp end parallel
    return
  end if
  a = 1.

!$omp parallel private(i,j,k)
  call mpi_send_x (f, g, 1, g, 0)
!
  do k=izs,ize
   do j=1,my
    ddxdn1(1   ,j,k) = dxidxdn(1)*( &
                   a*(f(1,j,k)-g(1  ,j,k)))
   end do
   do j=1,my
    do i=2,mx
      ddxdn1(i,j,k) = dxidxdn(i)*( &
                   a*(f(i,j,k)-f(i-1,j,k)))
    end do
   end do
  end do
!$omp end parallel
!
  if (omp_master) then
    nstag1 = nstag1+1
    nflop = nflop+2
  end if
END FUNCTION
!***********************************************************************
FUNCTION ddxup32 (f,j)
  use params, g=>gx2, h=>hx3
  implicit none
  real a, b, c
  integer i, j, k
  real, dimension(mx,my,mz):: f
  real, dimension(mx,mz):: ddxup32
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    !$omp parallel
    ddxup32(:,izs:ize) = 0.
    !$omp end parallel
    return
  end if
!
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)

  call mpi_send_x (f, g, 2, h, 3)
!
!$omp parallel private(i,j,k)
  do k=izs,ize
    ddxup32(mx-2,k) = dxidxup(mx-2)*( &
                   c*(h(1   ,j,k)-f(mx-4,j,k)) &
                 + b*(f(mx  ,j,k)-f(mx-3,j,k)) &
                 + a*(f(mx-1,j,k)-f(mx-2,j,k)))
    ddxup32(mx-1,k) = dxidxup(mx-1)*( &
                   c*(h(2   ,j,k)-f(mx-3,j,k)) &
                 + b*(h(1   ,j,k)-f(mx-2,j,k)) &
                 + a*(f(mx  ,j,k)-f(mx-1,j,k)))
    ddxup32(mx  ,k) = dxidxup(mx  )*( &
                   c*(h(3   ,j,k)-f(mx-2,j,k)) &
                 + b*(h(2   ,j,k)-f(mx-1,j,k)) &
                 + a*(h(1   ,j,k)-f(mx  ,j,k)))
    ddxup32(1  ,k) = dxidxup(1   )*( &
                   c*(f(4   ,j,k)-g(1   ,j,k)) &
                 + b*(f(3   ,j,k)-g(2   ,j,k)) &
                 + a*(f(2   ,j,k)-f(1   ,j,k)))
    ddxup32(2  ,k) = dxidxup(2   )*( &
                   c*(f(5   ,j,k)-g(2  ,j,k)) &
                 + b*(f(4   ,j,k)-f(1  ,j,k)) &
                 + a*(f(3   ,j,k)-f(2  ,j,k)))
    do i=3,mx-3
      ddxup32(i  ,k) = dxidxup(i)*( &
                   c*(f(i+3 ,j,k)-f(i-2 ,j,k)) &
                 + b*(f(i+2 ,j,k)-f(i-1 ,j,k)) &
                 + a*(f(i+1 ,j,k)-f(i   ,j,k)))
    end do
  end do
!$omp end parallel
!
  if (omp_master) then
    nstag = nstag+1
    nflop = nflop+8
  end if
END FUNCTION

!***********************************************************************
FUNCTION ddxup132 (f,j)
  use params, g=>gx1
  implicit none
  real a, b, c
  integer i, j, k
  real, dimension(mx,my,mz):: f
  real, dimension(mx,mz):: ddxup132
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    !$omp parallel
    ddxup132(:,izs:ize) = 0.
    !$omp end parallel
    return
  end if
!
  a = 1.
  call mpi_send_x (f, g, 0, g, 1)
!
!$omp parallel private(i,j,k)
  do k=izs,ize
    ddxup132(mx  ,k) = dxidxup(mx)*( &
                   a*(g(1   ,j,k)-f(mx  ,j,k)))
    do i=1,mx-1
      ddxup132(i  ,k) = dxidxup(i)*( &
                   a*(f(i+1 ,j,k)-f(i   ,j,k)))
    end do
  end do
!$omp end parallel
!
  if (omp_master) then
    nstag1 = nstag1+1
    nflop = nflop+2
  end if
END FUNCTION

!***********************************************************************
FUNCTION ddxdn32 (f,j)
  use params, g=>gx3, h=>hx2
  implicit none
  real a, b, c
  integer i, j, k
  real, dimension(mx,my,mz):: f
  real, dimension(mx,mz):: ddxdn32
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    !$omp parallel
    ddxdn32(:,izs:ize) = 0.
    !$omp end parallel
    return
  end if
!
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)

  call mpi_send_x (f, g, 3, h, 2)
!
!$omp parallel private(i,j,k)
  do k=izs,ize
    ddxdn32(mx-1,k) = dxidxdn(mx-1)*( &
                   c*(h(1   ,j,k)-f(mx-4,j,k)) &
                 + b*(f(mx  ,j,k)-f(mx-3,j,k)) &
                 + a*(f(mx-1,j,k)-f(mx-2,j,k)))
    ddxdn32(mx  ,k) = dxidxdn(mx  )*( &
                   c*(h(2   ,j,k)-f(mx-3,j,k)) &
                 + b*(h(1   ,j,k)-f(mx-2,j,k)) &
                 + a*(f(mx  ,j,k)-f(mx-1,j,k)))
    ddxdn32(1   ,k) = dxidxdn(1   )*( &
                   c*(f(3   ,j,k)-g(1   ,j,k)) &
                 + b*(f(2   ,j,k)-g(2   ,j,k)) &
                 + a*(f(1   ,j,k)-g(3   ,j,k)))
    ddxdn32(2  ,k) = dxidxdn(2   )*( &
                   c*(f(4   ,j,k)-g(2   ,j,k)) &
                 + b*(f(3   ,j,k)-g(3   ,j,k)) &
                 + a*(f(2   ,j,k)-f(1   ,j,k)))
    ddxdn32(3  ,k) = dxidxdn(3   )*( &
                   c*(f(5   ,j,k)-g(3   ,j,k)) &
                 + b*(f(4   ,j,k)-f(1   ,j,k)) &
                 + a*(f(3   ,j,k)-f(2   ,j,k)))
    do i=4,mx-2
      ddxdn32(i,k) = dxidxdn(i)*( &
                   c*(f(i+2 ,j,k)-f(i-3 ,j,k)) &
                 + b*(f(i+1 ,j,k)-f(i-2 ,j,k)) &
                 + a*(f(i   ,j,k)-f(i-1 ,j,k)))
    end do
  end do
!$omp end parallel
!
  if (omp_master) then
    nstag = nstag+1
    nflop = nflop+8
  end if
END FUNCTION

!***********************************************************************
FUNCTION ddxdn132 (f,j)
  use params, g=>gx1
  implicit none
  real a, b, c
  integer i, j, k
  real, dimension(mx,my,mz):: f
  real, dimension(mx,mz):: ddxdn132
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    !$omp parallel
    ddxdn132(:,izs:ize) = 0.
    !$omp end parallel
    return
  end if
!
  a = 1.
  call mpi_send_x (f, g, 1, g, 0)
!
!$omp parallel private(i,j,k)
  do k=izs,ize
    ddxdn132(1   ,k) = dxidxdn(1)*( &
                   a*(f(1   ,j,k)-g(1   ,j,k)))
    do i=2,mx
      ddxdn132(i,k) = dxidxdn(i)*( &
                   a*(f(i   ,j,k)-f(i-1 ,j,k)))
    end do
  end do
!$omp end parallel
!
  if (omp_master) then
    nstag1 = nstag1+1
    nflop = nflop+2
  end if
END FUNCTION
!***********************************************************************
FUNCTION ddxup22 (f)
  use params, g=>gx2, h=>hx3
  implicit none
  real a, b, c
  integer i, j, k
  real, dimension(mx,mz):: ddxup22
  real, dimension(mx,mz):: f
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    !$omp parallel
    ddxup22(:,izs:ize) = 0.
    !$omp end parallel
    return
  end if
!
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)

  call mpi_send2_x (f, mx, 1, mz, g, 2, h, 3)
!
!$omp parallel private(i,k)
  do k=izs,ize
    ddxup22(mx-2,k) = dxidxup(mx-2)*( &
                   c*(h(1, 1,k)-f(mx-4,k)) &
                 + b*(f(mx  ,k)-f(mx-3,k)) &
                 + a*(f(mx-1,k)-f(mx-2,k)))
    ddxup22(mx-1,k) = dxidxup(mx-1)*( &
                   c*(h(2, 1,k)-f(mx-3,k)) &
                 + b*(h(1, 1,k)-f(mx-2,k)) &
                 + a*(f(mx  ,k)-f(mx-1,k)))
    ddxup22(mx  ,k) = dxidxup(mx  )*( &
                   c*(h(3, 1,k)-f(mx-2,k)) &
                 + b*(h(2, 1,k)-f(mx-1,k)) &
                 + a*(h(1, 1,k)-f(mx  ,k)))
    ddxup22(1  ,k) = dxidxup(1   )*( &
                   c*(f(4   ,k)-g(1, 1,k)) &
                 + b*(f(3   ,k)-g(2, 1,k)) &
                 + a*(f(2   ,k)-f(1   ,k)))
    ddxup22(2  ,k) = dxidxup(2   )*( &
                   c*(f(5   ,k)-g(2 ,1,k)) &
                 + b*(f(4   ,k)-f(1   ,k)) &
                 + a*(f(3   ,k)-f(2   ,k)))
    do i=3,mx-3
      ddxup22(i  ,k) = dxidxup(i)*( &
                   c*(f(i+3 ,k)-f(i-2 ,k)) &
                 + b*(f(i+2 ,k)-f(i-1 ,k)) &
                 + a*(f(i+1 ,k)-f(i   ,k)))
    end do
  end do
!$omp end parallel
!
  if (omp_master) then
    nstag = nstag+1
    nflop = nflop+8
  end if
END FUNCTION

!***********************************************************************
FUNCTION ddxup122 (f)
  use params, g=>gx1
  implicit none
  real a, b, c
  integer i, j, k
  real, dimension(mx,mz):: ddxup122
  real, dimension(mx,mz):: f
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    !$omp parallel
    ddxup122(:,izs:ize) = 0.
    !$omp end parallel
    return
  end if
!
  a = 1.
  call mpi_send2_x (f, mx, 1, mz, g, 0, g, 1)
!
!$omp parallel private(i,k)
  do k=izs,ize
    ddxup122(mx  ,k) = dxidxup(mx)*( &
                   a*(g(1 ,1,k)-f(mx  ,k)))
    do i=1,mx-1
      ddxup122(i  ,k) = dxidxup(i)*( &
                   a*(f(i+1 ,k)-f(i   ,k)))
    end do
  end do
!$omp end parallel
!
  if (omp_master) then
    nstag1 = nstag1+1
    nflop = nflop+2
  end if
END FUNCTION

!***********************************************************************
FUNCTION ddxdn22 (f)
  use params, g=>gx3, h=>hx2
  implicit none
  real a, b, c
  integer i, j, k
  real, dimension(mx,mz):: ddxdn22
  real, dimension(mx,mz):: f
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    !$omp parallel
    ddxdn22(:,izs:ize) = 0.
    !$omp end parallel
    return
  end if
!
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)

  call mpi_send2_x (f, mx, 1, mz, g, 3, h, 2)
!
!$omp parallel private(i,k)
  do k=izs,ize
    ddxdn22(mx-1,k) = dxidxdn(mx-1)*( &
                   c*(h(1 ,1,k)-f(mx-4,k)) &
                 + b*(f(mx  ,k)-f(mx-3,k)) &
                 + a*(f(mx-1,k)-f(mx-2,k)))
    ddxdn22(mx  ,k) = dxidxdn(mx  )*( &
                   c*(h(2 ,1,k)-f(mx-3,k)) &
                 + b*(h(1 ,1,k)-f(mx-2,k)) &
                 + a*(f(mx  ,k)-f(mx-1,k)))
    ddxdn22(1   ,k) = dxidxdn(1   )*( &
                   c*(f(3   ,k)-g(1 ,1,k)) &
                 + b*(f(2   ,k)-g(2 ,1,k)) &
                 + a*(f(1   ,k)-g(3 ,1,k)))
    ddxdn22(2  ,k) = dxidxdn(2   )*( &
                   c*(f(4   ,k)-g(2 ,1,k)) &
                 + b*(f(3   ,k)-g(3 ,1,k)) &
                 + a*(f(2   ,k)-f(1   ,k)))
    ddxdn22(3  ,k) = dxidxdn(3   )*( &
                   c*(f(5   ,k)-g(3 ,1,k)) &
                 + b*(f(4   ,k)-f(1   ,k)) &
                 + a*(f(3   ,k)-f(2   ,k)))
    do i=4,mx-2
      ddxdn22(i,k) = dxidxdn(i)*( &
                   c*(f(i+2 ,k)-f(i-3 ,k)) &
                 + b*(f(i+1 ,k)-f(i-2 ,k)) &
                 + a*(f(i   ,k)-f(i-1 ,k)))
    end do
  end do
!$omp end parallel
!
  if (omp_master) then
    nstag = nstag+1
    nflop = nflop+8
  end if
END FUNCTION

!***********************************************************************
FUNCTION ddxdn122 (f)
  use params,  g=>gx1
  implicit none
  real a, b, c
  integer i, j, k
  real, dimension(mx,mz):: ddxdn122
  real, dimension(mx,mz):: f
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    !$omp parallel
    ddxdn122(:,izs:ize) = 0.
    !$omp end parallel
    return
  end if
!
  a = 1.
  call mpi_send2_x (f, mx, 1, mz, g, 1, g, 0)
!
!$omp parallel private(i,k)
  do k=izs,ize
    ddxdn122(1   ,k) = dxidxdn(1)*( &
                   a*(f(1   ,k)-g(1 ,1,k)))
    do i=2,mx
      ddxdn122(i,k) = dxidxdn(i)*( &
                   a*(f(i   ,k)-f(i-1 ,k)))
    end do
  end do
!$omp end parallel
!
  if (omp_master) then
    nstag1 = nstag1+1
    nflop = nflop+2
  end if
END FUNCTION
