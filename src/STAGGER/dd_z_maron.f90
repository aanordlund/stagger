!***********************************************************************
FUNCTION ddzup (f)
!
!  f is centered on (i,j,k-.5), fifth order stagger
!
  use params, g=>gz2, h=>hz3
  implicit none
  real a, b, c, a1st
  integer i, j, k
  real, intent(in) , dimension(mx,my,mz):: f
  real,              dimension(mx,my,mz):: ddzup
!-----------------------------------------------------------------------
!
  character(len=mid):: id = "$Id: dd_z_maron.f90,v 1.3 2013/11/03 10:10:00 aake Exp $" 
  call print_id (id)

  if (mz .lt. 5) then
    !$omp parallel private(k)
    do k=izs,ize
      ddzup(:,:,k) = 0.
    end do
    !$omp end parallel
    return
  end if

  c =  0.00699
  b = -0.04056-5.*c
  a = (1.-3.*b-5.*c)/dz
  a1st = 1./dz
  b = b/dz
  c = c/dz

  call mpi_send_z (f, g, 2, h, 3)
!
  !$omp parallel private(i,j,k)
  do k=max(3,izs),min(mz-3,ize)
    do j=1,my
      do i=1,mx
        ddzup(i,j,k) = ( &
                     c*(f(i,j,k+3 )-f(i,j,k-2 )) + &
                     b*(f(i,j,k+2 )-f(i,j,k-1 )) + &
                     a*(f(i,j,k+1 )-f(i,j,k   )))
     end do
    end do
  end do

  if (ize.eq.mz) then
   do j=1,my
    do i=1,mx
      ddzup(i,j,mz-2) = ( &
                     c*(h(i,j,1   )-f(i,j,mz-4)) + &
                     b*(f(i,j,mz  )-f(i,j,mz-3)) + &
                     a*(f(i,j,mz-1)-f(i,j,mz-2)))
      ddzup(i,j,mz-1) = ( &
                     c*(h(i,j,2   )-f(i,j,mz-3)) + &
                     b*(h(i,j,1   )-f(i,j,mz-2)) + &
                     a*(f(i,j,mz  )-f(i,j,mz-1)))
      ddzup(i,j,mz  ) = ( &
                     c*(h(i,j,3   )-f(i,j,mz-2)) + &
                     b*(h(i,j,2   )-f(i,j,mz-1)) + &
                     a*(h(i,j,1   )-f(i,j,mz  )))
    end do
   end do
  end if
  if (izs.eq.1) then
   do j=1,my
    do i=1,mx
      ddzup(i,j,1   ) = ( &
                     c*(f(i,j,4   )-g(i,j,1   )) + &
                     b*(f(i,j,3   )-g(i,j,2   )) + &
                     a*(f(i,j,2   )-f(i,j,1   )))
      ddzup(i,j,2   ) = ( &
                     c*(f(i,j,5   )-g(i,j,2   )) + &
                     b*(f(i,j,4   )-f(i,j,1   )) + &
                     a*(f(i,j,3   )-f(i,j,2   )))
    end do
   end do
  end if
  !$omp end parallel
!
  nstag = nstag+1
  nflop = nflop+8
END FUNCTION

!***********************************************************************
FUNCTION ddzup1 (f)
!
!  f is centered on (i,j,k-.5), fifth order stagger
!
  use params, g=>gz1
  implicit none
  real a, b, c, a1st
  integer i, j, k
  real, intent(in) , dimension(mx,my,mz):: f
  real,              dimension(mx,my,mz):: ddzup1
!-----------------------------------------------------------------------
!
  if (mz .lt. 5) then
    !$omp parallel private(k)
    do k=izs,ize
      ddzup1(:,:,k) = 0.
    end do
    !$omp end parallel
    return
  end if
!
  a = 1./dz
  call mpi_send_z (f, g, 0, g, 1)
!
  !$omp parallel private(i,j,k)
  do k=izs,min(mz-1,ize)
    do j=1,my
      do i=1,mx
        ddzup1(i,j,k  ) = ( &
                     a*(f(i,j,k+1)-f(i,j,k)))
      end do
    end do
  end do
  if (ize.eq.mz) then
   do j=1,my
    do i=1,mx
      ddzup1(i,j,mz) = ( &
                     a*(g(i,j,1)-f(i,j,mz)))
    end do
   end do
  end if
  !$omp end parallel
!
  nstag1 = nstag1+1
  nflop = nflop+2
END FUNCTION

!***********************************************************************
FUNCTION ddzdn (f)
!
!  f is centered on (i,j), fifth order stagger
!
  use params, g=>gz3, h=>hz2
  implicit none
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f, ddzdn
!-----------------------------------------------------------------------
!
  if (mz .lt. 5) then
    !$omp parallel private(k)
    do k=izs,ize
      ddzdn(:,:,k) = 0.
    end do
    !$omp end parallel
    return
  end if

  c =  0.00699
  b = -0.04056-5.*c
  a = (1.-3.*b-5.*c)/dz
  a1st = 1./dz
  b = b/dz
  c = c/dz

  call mpi_send_z (f, g, 3, h, 2)
!
  !$omp parallel private(i,j,k)
  do k=max(4,izs),min(mz-2,ize)
    do j=1,my
     do i=1,mx
      ddzdn(i,j,k) = ( &
                     c*(f(i,j,k+2 )-f(i,j,k-3 )) + &
                     b*(f(i,j,k+1 )-f(i,j,k-2 )) + &
                     a*(f(i,j,k   )-f(i,j,k-1 )))
     end do
    end do
  end do
  if (ize.eq.mz) then
   do j=1,my
    do i=1,mx
      ddzdn(i,j,mz-1) = ( &
                     c*(h(i,j,1   )-f(i,j,mz-4)) + &
                     b*(f(i,j,mz  )-f(i,j,mz-3)) + &
                     a*(f(i,j,mz-1)-f(i,j,mz-2)))
      ddzdn(i,j,mz  ) = ( &
                     c*(h(i,j,2   )-f(i,j,mz-3)) + &
                     b*(h(i,j,1   )-f(i,j,mz-2)) + &
                     a*(f(i,j,mz  )-f(i,j,mz-1)))
    end do
   end do
  end if
  if (izs.eq.1) then
   do j=1,my
    do i=1,mx
      ddzdn(i,j,1   ) = ( &
                     c*(f(i,j,3   )-g(i,j,1   )) + &
                     b*(f(i,j,2   )-g(i,j,2   )) + &
                     a*(f(i,j,1   )-g(i,j,3   )))
      ddzdn(i,j,2   ) = ( &
                     c*(f(i,j,4   )-g(i,j,2   )) + &
                     b*(f(i,j,3   )-g(i,j,3   )) + &
                     a*(f(i,j,2   )-f(i,j,1   )))
      ddzdn(i,j,3   ) = ( &
                     c*(f(i,j,5   )-g(i,j,3   )) + &
                     b*(f(i,j,4   )-f(i,j,1   )) + &
                     a*(f(i,j,3   )-f(i,j,2   )))
    end do
   end do
  end if
  !$omp end parallel
!
  nstag = nstag+1
  nflop = nflop+8
END FUNCTION

!***********************************************************************
FUNCTION ddzdn1 (f)
!
!  f is centered on (i,j), fifth order stagger
!
  use params, g=>gz1
  implicit none
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f, ddzdn1
!-----------------------------------------------------------------------
!
  if (mz .lt. 5) then
    !$omp parallel private(k)
    do k=izs,ize
      ddzdn1(:,:,k) = 0.
    end do
    !$omp end parallel
    return
  end if
!
  a = 1./dz
  call mpi_send_z (f, g, 1, g, 0)
!
  !$omp parallel private(i,j,k)
  do k=max(2,izs),ize
    do j=1,my
     do i=1,mx
      ddzdn1(i,j,k) = ( &
                     a*(f(i,j,k)-f(i,j,k-1)))
     end do
    end do
  end do
  if (izs.eq.1) then
   do j=1,my
    do i=1,mx
      ddzdn1(i,j,1   ) = ( &
                     a*(f(i,j,1)-g(i,j,1  )))
    end do
   end do
  end if
  !$omp end parallel
!
  nstag1 = nstag1+1
  nflop = nflop+2
END FUNCTION
