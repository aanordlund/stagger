!***********************************************************************
FUNCTION ddyup (f)
  use params
  implicit none
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f, ddyup
!hpf$ distribute(*,*,block):: f, ddyup
!-----------------------------------------------------------------------
!
  if (my .lt. 3) then
    ddyup = 0.
    return
  end if
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)
  a1st = 1.
!
!$omp parallel private(i,j,k)
  do k=izs,ize
    do i=1,mx
      ddyup(i,my-2,k) = dyidyup(my-2)*( &
                     c*(f(i,1   ,k)-f(i,my-4,k)) + &
                     b*(f(i,my  ,k)-f(i,my-3,k)) + &
                     a*(f(i,my-1,k)-f(i,my-2,k)))
      ddyup(i,my-1,k) = dyidyup(my-1)*( &
                     c*(f(i,2   ,k)-f(i,my-3,k)) + &
                     b*(f(i,1   ,k)-f(i,my-2,k)) + &
                     a*(f(i,my  ,k)-f(i,my-1,k)))
      ddyup(i,my  ,k) = dyidyup(my  )*( &
                     c*(f(i,3   ,k)-f(i,my-2,k)) + &
                     b*(f(i,2   ,k)-f(i,my-1,k)) + &
                     a*(f(i,1   ,k)-f(i,my  ,k)))
      ddyup(i,1   ,k) = dyidyup(1   )*( &
                     c*(f(i,4   ,k)-f(i,my-1,k)) + &
                     b*(f(i,3   ,k)-f(i,my  ,k)) + &
                     a*(f(i,2   ,k)-f(i,1   ,k)))
      ddyup(i,2   ,k) = dyidyup(2   )*( &
                     c*(f(i,5   ,k)-f(i,my  ,k)) + &
                     b*(f(i,4   ,k)-f(i,1   ,k)) + &
                     a*(f(i,3   ,k)-f(i,2   ,k)))
    end do
    do j=3,my-3
     do i=1,mx
      ddyup(i,j  ,k) = dyidyup(j)*( &
                     c*(f(i,j+3 ,k)-f(i,j-2 ,k)) + &
                     b*(f(i,j+2 ,k)-f(i,j-1 ,k)) + &
                     a*(f(i,j+1 ,k)-f(i,j   ,k)))
     end do
    end do
  end do
!$omp end parallel
!
  if (omp_master) then
    nstag = nstag+1
    nflop = nflop+8
  end if
END FUNCTION

!***********************************************************************
FUNCTION ddyup1 (f)
  use params
  implicit none
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f, ddyup1
!hpf$ distribute(*,*,block):: f, ddyup1
!-----------------------------------------------------------------------
!
  if (my .lt. 3) then
    ddyup1 = 0.
    return
  end if
  a = 1.
!
!$omp parallel private(i,j,k)
  do k=izs,ize
    do i=1,mx
      ddyup1(i,my,k) = dyidyup(my)*( &
                     a*(f(i,1  ,k)-f(i,my,k)))
    end do
    do j=1,my-1
      do i=1,mx
        ddyup1(i,j  ,k) = dyidyup(j)*( &
                     a*(f(i,j+1 ,k)-f(i,j   ,k)))
     end do
    end do
  end do
!$omp end parallel
!
  if (omp_master) then
    nstag1 = nstag1+1
    nflop = nflop+2
  end if
END FUNCTION

!***********************************************************************
FUNCTION ddydn (f)
  use params
  implicit none
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f, ddydn
!hpf$ distribute(*,*,block):: f, ddydn
!-----------------------------------------------------------------------
!
  if (my .lt. 3) then
    ddydn = 0.
    return
  end if
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)
  a1st = 1.
!
!$omp parallel private(i,j,k)
  do k=izs,ize
    do i=1,mx
      ddydn(i,my-1,k) = dyidydn(my-1)*( &
                     c*(f(i,1   ,k)-f(i,my-4,k)) + &
                     b*(f(i,my  ,k)-f(i,my-3,k)) + &
                     a*(f(i,my-1,k)-f(i,my-2,k)))
      ddydn(i,my  ,k) = dyidydn(my  )*( &
                     c*(f(i,2   ,k)-f(i,my-3,k)) + &
                     b*(f(i,1   ,k)-f(i,my-2,k)) + &
                     a*(f(i,my  ,k)-f(i,my-1,k)))
      ddydn(i,1  ,k) = dyidydn(1   )*( &
                     c*(f(i,3   ,k)-f(i,my-2,k)) + &
                     b*(f(i,2   ,k)-f(i,my-1,k)) + &
                     a*(f(i,1   ,k)-f(i,my  ,k)))
      ddydn(i,2   ,k) = dyidydn(2   )*( &
                     c*(f(i,4   ,k)-f(i,my-1,k)) + &
                     b*(f(i,3   ,k)-f(i,my  ,k)) + &
                     a*(f(i,2   ,k)-f(i,1   ,k)))
      ddydn(i,3   ,k) = dyidydn(3   )*( &
                     c*(f(i,5   ,k)-f(i,my  ,k)) + &
                     b*(f(i,4   ,k)-f(i,1   ,k)) + &
                     a*(f(i,3   ,k)-f(i,2   ,k)))
    end do
    do j=4,my-2
     do i=1,mx
      ddydn(i,j ,k) = dyidydn(j   )*( &
                     c*(f(i,j+2 ,k)-f(i,j-3 ,k)) + &
                     b*(f(i,j+1 ,k)-f(i,j-2 ,k)) + &
                     a*(f(i,j   ,k)-f(i,j-1 ,k)))
     end do
    end do
  end do
!$omp end parallel
!
  if (omp_master) then
    nstag = nstag+1
    nflop = nflop+8
  end if
END FUNCTION

!***********************************************************************
FUNCTION ddydn1 (f)
  use params
  implicit none
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f, ddydn1
!hpf$ distribute(*,*,block):: f, ddydn1
!-----------------------------------------------------------------------
!
  if (my .lt. 3) then
    ddydn1 = 0.
    return
  end if
  a = 1.
!
!$omp parallel private(i,j,k)
  do k=izs,ize
    do i=1,mx
      ddydn1(i,1  ,k) = dyidydn(1)*( &
                     a*(f(i,1   ,k)-f(i,my  ,k)))
    end do
    do j=2,my
     do i=1,mx
      ddydn1(i,j ,k) = dyidydn(j)*( &
                     a*(f(i,j   ,k)-f(i,j-1 ,k)))
     end do
    end do
  end do
!$omp end parallel
!
  if (omp_master) then
    nstag1 = nstag1+1
    nflop = nflop+2
  end if
END FUNCTION
