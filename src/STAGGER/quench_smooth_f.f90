! $Id: quench_smooth_f.f90,v 1.5 2013/11/04 18:03:25 aake Exp $
!**********************************************************************
MODULE quench_m
  implicit none
  real qmax, qlim
END module

!**********************************************************************
SUBROUTINE init_quench
  USE params
  USE quench_m
  implicit none
  character(len=mid):: id="$Id: quench_smooth_f.f90,v 1.5 2013/11/04 18:03:25 aake Exp $"

  call print_id(id)
  do_quench = .false.
  qmax = 8.
  qlim = 2.
  call read_quench
  if (do_quench) call test_quench
END SUBROUTINE

!**********************************************************************
SUBROUTINE read_quench
  USE params
  USE quench_m
  implicit none
  namelist /quench/ do_quench, qmax, qlim
  rewind (stdin); read  (stdin,quench)
  if (master) write (*,quench)
  if (.not. do_quench) qlim=1.
END SUBROUTINE

!**********************************************************************
SUBROUTINE test_quench
  USE params
  implicit none
  integer mx0,my0,mz0
  real, allocatable, dimension(:,:,:):: f, g

  return
  mx0=mx
  my0=my
  mz0=mz
  mx=21
  my=25
  mz=39
  deallocate (scratch)
  call init_omp
  !$omp master
  allocate (f(mx,my,mz), g(mx,my,mz), scratch(mx,my,mz))
  !$omp end master
  !$omp barrier
  f = 0.
  g = 0.
  g(11,12,13)=1.
  g(12,12,13)=-1.
  call max3_set (g, f, scratch)

  call quenchx(f, g)  ; print 1,g(8:14,12,13)
  call quenchy(f, g)  ; print 1,g(11,9:15,13)
  call quenchz(f, g,scratch)  ; print 1,g(11,12,10:16)
1 format(/(7g11.3))
  !$omp master
  deallocate (f, g)
  mx=mx0
  my=my0
  mz=mz0
  deallocate (scratch)
  !$omp end master
  !$omp barrier
  call init_omp
END SUBROUTINE

!**********************************************************************
SUBROUTINE quenchx (f,g)
  USE params
  USE quench_m, only: qlim, qmax
  implicit none
!
!  This is one of the fundamental operators in the MHD simulation code,
!  and is used in the dissipation expressions.
!
!----------------------------------------------------------------------
!
  real, dimension(mx,my,mz) :: f, g
  intent(inout):: f
  intent(out):: g
  real :: d1(-1:mx+1), d2(0:mx+1), fa(0:mx+1), qq(0:mx+1)
  integer i,j,k
!-----------------------------------------------------------------------
!
  if (.not.do_quench .or. qmax.eq.1.) then
    do k=izs,ize
      g(:,:,k) = f(:,:,k)
    end do
    return
  end if
!
  call mpi_send_x (f,gx2,2,hx2,2)
!
!$omp barrier
  do k=izs,ize
     do j=1,my
       d1(0   )=f (1,j,k)-gx2(2  ,j,k)
       do i=1,mx-1
         d1(i)=f(i+1,j,k)-f(i,j,k)
         fa(i)=abs(f(i,j,k))
         d2(i)=abs(d1(i)-d1(i-1))
         qq(i)=d2(i)/(fa(i)+(1./qmax)*d2(i)+1e-20)
       enddo 
       d1(-1  )=gx2(2,j,k)-gx2(1  ,j,k)
       d2(0   )=abs(d1(0   )-d1(-1  ))
       d1(mx  )=hx2(1,j,k)-f (mx ,j,k)
       d2(mx  )=abs(d1(mx  )-d1(mx-1))
       d1(mx+1)=hx2(2,j,k)-hx2(1  ,j,k)
       d2(mx+1)=abs(d1(mx+1)-d1(mx  ))

       fa(0   )=abs(gx2(2,j,k))
       qq(0   )=d2(0   )/(fa(0   )+(1./qmax)*d2(0   )+1e-20)
       fa(mx  )=abs(f(mx,j,k))
       qq(mx  )=d2(mx  )/(fa(mx  )+(1./qmax)*d2(mx  )+1e-20)
       fa(mx+1)=abs(hx2(1,j,k))
       qq(mx+1)=d2(mx+1)/(fa(mx+1)+(1./qmax)*d2(mx+1)+1e-20)
       do i=1,mx
         g(i,j,k)=f(i,j,k)*min(max(qq(i-1),qq(i),qq(i+1)),qlim)
       enddo 
     enddo
  enddo
!
  nflop = nflop + 11
END

!**********************************************************************
SUBROUTINE quenchx1 (f)
  USE params
  USE quench_m, only: qlim, qmax
  implicit none
!
!  This is one of the fundamental operators in the MHD simulation code,
!  and is used in the dissipation expressions.
!
!----------------------------------------------------------------------
!
  real, dimension(mx,my,mz) :: f
  intent(inout):: f
  real :: d1(-1:mx+1), d2(0:mx+1), fa(0:mx+1), qq(0:mx+1)
  integer i,j,k
!-----------------------------------------------------------------------
!
  if (.not.do_quench .or. qmax.eq.1.) then
    return
  end if

  !$omp master
  allocate (tmp1(mx,my,mz), tmp2(mx,my,mz))
  !$omp end master
  !$omp barrier
!
  call regularize (f)
  do k=izs,ize
    tmp1(:,:,k) = abs(f(:,:,k))
  end do
  call dumpn(tmp1,'tmp1','qsmx.dmp',1)
  call smooth3_set (tmp1,tmp2)
  call dumpn(tmp2,'tmp2','qsmx.dmp',1)
  call mpi_send_x (f,gx1,1,hx1,1)
!
  !$omp barrier
  do k=izs,ize
     do j=1,my
       d1(0)=f(1,j,k)-gx1(1,j,k)
       do i=1,mx-1
         d1(i)=f(i+1,j,k)-f(i,j,k)
         fa(i)=tmp2(i,j,k)
         d2(i)=abs(d1(i)-d1(i-1))
         qq(i)=d2(i)/(fa(i)+(1./qmax)*d2(i)+1e-20)
       enddo 
       d1(mx)=hx1(1,j,k)-f (mx,j,k)
       d2(mx)=abs(d1(mx  )-d1(mx-1))
       fa(mx)=tmp2(mx,j,k)
       qq(mx)=d2(mx)/(fa(mx)+(1./qmax)*d2(mx)+1e-20)
       tmp1(1:mx,j,k) = qq(1:mx)
     enddo
  enddo
  !$omp barrier
  call regularize (tmp1)
  call dumpn(tmp1,'tmp3','qsmx.dmp',1)
  call max3_set (tmp1, tmp2)
  call dumpn(tmp2,'tmp4','qsmx.dmp',1)
  call smooth3_set (tmp2, tmp1)
  call dumpn(tmp1,'tmp4','qsmx.dmp',1)
!
  do k=izs,ize
    f(:,:,k) = f(:,:,k)*tmp1(:,:,k)
  end do
  call dumpn(f,'f','qsmx.dmp',1)
!
  !$omp barrier
  !$omp master
  deallocate (tmp1, tmp2)
  !$omp end master
!
  nflop = nflop + 11
END

!**********************************************************************
SUBROUTINE quenchx2 (f,g)
  USE params
  USE quench_m, only: qlim, qmax
  implicit none
!
!  This is one of the fundamental operators in the MHD simulation code,
!  and is used in the dissipation expressions.
!
!----------------------------------------------------------------------
!
  real, dimension(mx,my,mz) :: f,g
  intent(inout):: f
  real :: d1(-1:mx+1), d2(0:mx+1), fa(0:mx+1), qq(0:mx+1)
  integer i,j,k
!-----------------------------------------------------------------------
!
  if (.not.do_quench .or. qmax.eq.1.) then
    return
  end if
!
  !$omp master
  allocate (tmp1(mx,my,mz), tmp2(mx,my,mz))
  !$omp end master
  !$omp barrier
!
  call regularize (f)
  do k=izs,ize
    tmp1(:,:,k) = abs(f(:,:,k))
  end do
  call dumpn(tmp1,'tmp1','qsmx.dmp',1)
  call smooth3_set (tmp1,tmp2)
  call dumpn(tmp2,'tmp2','qsmx.dmp',1)
  call mpi_send_x (f,gx1,1,hx1,1)
!
  do k=izs,ize
     do j=1,my
       d1(0   )=f (1,j,k)-gx1(1 ,j,k)
       do i=1,mx-1
         d1(i)=f(i+1,j,k)-f(i,j,k)                                      ! +-2 for sawtooth
         fa(i)=tmp2(i,j,k)                                                ! +1 for sawtooth
         d2(i)=abs(d1(i)-d1(i-1))                                       ! +4 for sawtooth
         qq(i)=d2(i)/(fa(i)+(1./qmax)*d2(i)+1e-20)                      ! 4/(1+4/qmax) [ harmonic mean of 4 and qmax ]
       enddo 
       d1(mx)=hx1(1,j,k)-f (mx,j,k)
       d2(mx)=abs(d1(mx)-d1(mx-1))
       fa(mx)=tmp2(mx,j,k)
       qq(mx)=d2(mx)/(fa(mx)+(1./qmax)*d2(mx)+1e-20)
       do i=1,mx
         tmp1(i,j,k)=qq(i)
       enddo 
     enddo
  enddo
  call regularize (tmp1)
  call dumpn(tmp1,'tmp3','qsmx.dmp',1)
  call max3_set (tmp1, tmp2)
  call dumpn(tmp2,'tmp4','qsmx.dmp',1)
  call smooth3_set (tmp2, tmp1)
  call dumpn(tmp1,'tmp5','qsmx.dmp',1)
!
  do k=izs,ize
    g(:,:,k) = tmp1(:,:,k)
  end do
!
  !$omp barrier
  !$omp master
  deallocate (tmp1, tmp2)
  !$omp end master
!
  nflop = nflop + 11
END

!**********************************************************************
SUBROUTINE quenchy (f, g)
  USE params
  USE quench_m, only: qlim, qmax
  implicit none
!
  real, dimension(mx,my,mz) :: f, g
  real :: d1(-1:my+1), d2(0:my+1), fa(0:my+1), qq(0:my+1)
  integer i,j,k
!----------------------------------------------------------------------
!
  if (.not.do_quench .or. qmax.eq.1.) then
    do k=izs,ize
      g(:,:,k) = f(:,:,k)
    end do
    return
  end if

  call mpi_send_y (f,gy2,2,hy2,2)

  do k=izs,ize
     do i=1,mx
       d1(0   )=f (i,1,k)-gy2(i, 2,k)
       do j=1,my-1
         d1(j)=f(i,j+1,k)-f(i,j,k)
         fa(j)=abs(f(i,j,k))
         d2(j)=abs(d1(j)-d1(j-1))
         qq(j)=d2(j)/(fa(j)+(1./qmax)*d2(j)+1e-20)
       enddo 
       d1(-1  )=gy2(i,2,k)-gy2(i, 1,k)
       d2(0   )=abs(d1(0   )-d1(-1  ))
       d1(my  )=hy2(i,1,k)-f (i,my,k)
       d2(my  )=abs(d1(my  )-d1(my-1))
       d1(my+1)=hy2(i,2,k)-hy2(i, 1,k)
       d2(my+1)=abs(d1(my+1)-d1(my  ))

       fa(0   )=abs(gy2(i,2,k))
       qq(0   )=d2(0   )/(fa(0   )+(1./qmax)*d2(0   )+1e-20)
       fa(my  )=abs(f(i,my,k))
       qq(my  )=d2(my  )/(fa(my  )+(1./qmax)*d2(my  )+1e-20)
       fa(my+1)=abs(hy2(i,1,k))
       qq(my+1)=d2(my+1)/(fa(my+1)+(1./qmax)*d2(my+1)+1e-20)
       do j=1,my
         g(i,j,k)=f(i,j,k)*min(max(qq(j-1),qq(j),qq(j+1)),qlim)
       enddo
     enddo
     if (lb >  1) g(:, 1:lb  ,k) = f(:, 1:lb  ,k)
     if (ub < my) g(:,ub+1:my,k) = f(:,ub+1:my,k)
  enddo
!
  !$omp master
  nflop = nflop + 11
  !$omp end master
END


!**********************************************************************
SUBROUTINE quenchy1 (f)
  USE params
  USE quench_m, only: qlim, qmax
  implicit none
!
  real, dimension(mx,my,mz) :: f
  real :: d1(-1:my+1), d2(0:my+1), fa(0:my+1), qq(0:my+1)
  integer i,j,k
!-----------------------------------------------------------------------
!
  if (.not.do_quench .or. qmax.eq.1.) then
    return
  end if

  !$omp master
  allocate (tmp1(mx,my,mz), tmp2(mx,my,mz))
  !$omp end master
  !$omp barrier
!
  call regularize (f)
  do k=izs,ize
    tmp1(:,:,k) = abs(f(:,:,k))
  end do
  call dumpn(tmp1,'tmp1','qsmy.dmp',1)
  call smooth3_set (tmp1,tmp2)
  call dumpn(tmp2,'tmp2','qsmy.dmp',1)
  call mpi_send_y (f,gy1,1,hy1,1)

  !$omp barrier
  do k=izs,ize
     do i=1,mx
       d1(0   )=f (i,1,k)-gy1(i,1,k)
       do j=1,my-1
         d1(j)=f(i,j+1,k)-f(i,j,k)
         fa(j)=tmp2(i,j,k)
         d2(j)=abs(d1(j)-d1(j-1))
         qq(j)=d2(j)/(fa(j)+(1./qmax)*d2(j)+1e-20)
       enddo 
       d1(my)=hy1(i,1,k)-f (i,my,k)
       d2(my)=abs(d1(my)-d1(my-1))
       fa(my)=tmp2(i,my,k)
       qq(my)=d2(my)/(fa(my)+(1./qmax)*d2(my)+1e-20)
       do j=1,my
         tmp1(i,j,k)=qq(j)
       enddo
     enddo
  enddo
  !$omp barrier

  call dumpn(tmp1,'tmp3','qsmy.dmp',1)
  call regularize (tmp1)
  call dumpn(tmp1,'tmp3r','qsmy.dmp',1)
  call max3_set (tmp1, tmp2)
  call dumpn(tmp2,'tmp4','qsmy.dmp',1)
  call smooth3_set (tmp2, tmp1)
  call dumpn(tmp1,'tmp5','qsmy.dmp',1)
!
  do k=izs,ize
    f(:,:,k) = f(:,:,k)*tmp1(:,:,k)
  end do
  call dumpn(f,'f','qsmy.dmp',1)
!
  !$omp barrier
  !$omp master
  deallocate (tmp1, tmp2)
  !$omp end master
!
  !$omp master
  nflop = nflop + 11
  !$omp end master
END

!**********************************************************************
SUBROUTINE quenchy2 (f,g)
  USE params
  USE quench_m, only: qlim, qmax
  implicit none
!
  real, dimension(mx,my,mz) :: f,g
  real :: d1(-1:my+1), d2(0:my+1), fa(0:my+1), qq(0:my+1)
  integer i,j,k
!
!----------------------------------------------------------------------
!
  if (.not.do_quench .or. qmax.eq.1.) then
    return
  end if
!
  !$omp master
  allocate (tmp1(mx,my,mz), tmp2(mx,my,mz))
  !$omp end master
  !$omp barrier
!
  call regularize (f)
  do k=izs,ize
    tmp1(:,:,k) = abs(f(:,:,k))
  end do
  call smooth3_set (tmp1,tmp2)
  call mpi_send_y (f,gy1,1,hy1,1)

  do k=izs,ize
     do i=1,mx
       d1(0   )=f (i,1,k)-gy1(i,1,k)
       do j=1,my-1
         d1(j)=f(i,j+1,k)-f(i,j,k)
         fa(j)=tmp2(i,j,k)
         d2(j)=abs(d1(j)-d1(j-1))
         qq(j)=d2(j)/(fa(j)+(1./qmax)*d2(j)+1e-20)
       enddo 
       d1(my)=hy1(i,1,k)-f (i,my,k)
       d2(my)=abs(d1(my)-d1(my-1))
       fa(my)=tmp2(i,my,k)
       qq(my)=d2(my)/(fa(my)+(1./qmax)*d2(my)+1e-20)
       do j=1,my
         tmp1(i,j,k)=qq(j)
       enddo
     enddo
  enddo
  call regularize (tmp1)
  call max3_set (tmp1, tmp2)
  call smooth3_set (tmp2, tmp1)
!
  do k=izs,ize
    g(:,:,k) = tmp1(:,:,k)
  end do
!
  !$omp barrier
  !$omp master
  deallocate (tmp1, tmp2)
  !$omp end master
!
  nflop = nflop + 11
END

!**********************************************************************
SUBROUTINE quenchz (f, g, s)
  USE params, gz=> gz2, hz=>hz2
  USE quench_m
  implicit none
!
  real, intent(inout), dimension(mx,my,mz) :: f, g, s
  integer i,j,k,js,je
  real, allocatable, dimension(:,:) :: fa, d1, d2, qq
!----------------------------------------------------------------------
!
  if (.not.do_quench .or. qmax.eq.1.) then
    do k=izs,ize
      g(:,:,k) = f(:,:,k)
    end do
    return
  end if
!
!$omp barrier
  call mpi_send_z (f,gz,2,hz,2)
!
!$omp barrier
  allocate (fa(mx,-1:mz+2), d1(mx,-1:mz+1), d2(mx,0:mz+1), qq(mx,0:mz+1))
  call limits_omp(1,my,js,je)
  do j=js,je
    do i=1,mx
      d1(i,-1)=gz(i,j,2)-gz(i,j,1)
      d1(i, 0)=f (i,j,1)-gz(i,j,2)
      d2(i, 0)=abs(d1(i,0   )-d1(i,-1  ))
      fa(i, 0)=abs(gz(i,j,2))
      qq(i, 0)=d2(i,0)/(fa(i,0)+(1./qmax)*d2(i,0)+1e-20)
    enddo
    do k=1,mz-1
      do i=1,mx
        d1(i,k)=f(i,j,k+1)-f(i,j,k)
        fa(i,k)=abs(f(i,j,k))
        d2(i,k)=abs(d1(i,k)-d1(i,k-1))
        qq(i,k)=d2(i,k)/(fa(i,k)+(1./qmax)*d2(i,k)+1e-20)
      enddo 
    enddo 
    do i=1,mx
      d1(i,mz  )=hz(i,j,1)-f (i,j,mz)
      d1(i,mz+1)=hz(i,j,2)-hz(i,j,1)
      d2(i,mz  )=abs(d1(i,mz  )-d1(i,mz-1))
      d2(i,mz+1)=abs(d1(i,mz+1)-d1(i,mz  ))
      fa(i,mz  )=abs(f(i,j,mz))
      qq(i,mz  )=d2(i,mz  )/(fa(i,mz  )+(1./qmax)*d2(i,mz  )+1e-20)
      fa(i,mz+1)=abs(hz(i,j,1))
      qq(i,mz+1)=d2(i,mz+1)/(fa(i,mz+1)+(1./qmax)*d2(i,mz+1)+1e-20)
    enddo 
    do k=1,mz
      do i=1,mx
        g(i,j,k)=f(i,j,k)*min(max(qq(i,k-1),qq(i,k),qq(i,k+1)),qlim)
      enddo 
    enddo 
  enddo
  deallocate (fa, d1, d2, qq)
!$omp barrier
!
  !$omp master
  nflop = nflop + 11
  !$omp end master
END

!**********************************************************************
SUBROUTINE quenchz1 (f)
  USE params
  USE quench_m
  implicit none
!
  real, intent(inout), dimension(mx,my,mz) :: f
  integer i,j,k
  real, allocatable, dimension(:,:) :: fa, d1, d2, qq
!----------------------------------------------------------------------
!
  if (.not.do_quench .or. qmax.eq.1.) then
    return
  end if
!
  !$omp master
  allocate (tmp1(mx,my,mz), tmp2(mx,my,mz))
  !$omp end master
  !$omp barrier
!
  call regularize (f)
  do k=izs,ize
    tmp1(:,:,k) = abs(f(:,:,k))
  end do
  call dumpn(tmp1,'tmp1','qsm.dmp',1)
  call smooth3_set (tmp1,tmp2)
  call dumpn(tmp2,'tmp2','qsm.dmp',1)
  call mpi_send_z (f,gz1,1,hz1,1)
!
  allocate (fa(mx,-1:mz+2), d1(mx,-1:mz+1), d2(mx,0:mz+1), qq(mx,0:mz+1))
  !$omp barrier
  do j=iys,iye
     do i=1,mx
       d1(i,0)=f (i,j,1)-gz1(i,j,1)
     enddo
     do k=1,mz-1
       do i=1,mx
         d1(i,k)=f(i,j,k+1)-f(i,j,k)
         fa(i,k)=tmp2(i,j,k)
         d2(i,k)=abs(d1(i,k)-d1(i,k-1))
         qq(i,k)=d2(i,k)/(fa(i,k)+(1./qmax)*d2(i,k)+1e-20)
       enddo 
     enddo 
     do i=1,mx
       d1(i,mz)=hz1(i,j,1)-f (i,j,mz)
       d2(i,mz)=abs(d1(i,mz)-d1(i,mz-1))
       fa(i,mz)=tmp2(i,j,mz)
       qq(i,mz)=d2(i,mz)/(fa(i,mz)+(1./qmax)*d2(i,mz)+1e-20)
     enddo 
     do k=1,mz
       do i=1,mx
         tmp1(i,j,k)=qq(i,k)
       enddo 
     enddo 
  enddo
  !$omp barrier
  call dumpn(tmp1,'tmp3','qsm.dmp',1)
  call regularize (tmp1)
  call dumpn(tmp1,'tmp3r','qsm.dmp',1)
  call max3_set (tmp1, tmp2)
  call dumpn(tmp2,'tmp4','qsm.dmp',1)
  call smooth3_set (tmp2, tmp1)
  call dumpn(tmp1,'tmp5','qsm.dmp',1)
!
  do k=izs,ize
    f(:,:,k) = f(:,:,k)*tmp1(:,:,k)
  end do
  call dumpn(f,'f','qsm.dmp',1)
!
  !$omp barrier
  !$omp master
  deallocate (tmp1, tmp2)
  deallocate (fa, d1, d2, qq)
  !$omp end master
!
  nflop = nflop + 11
END

!**********************************************************************
SUBROUTINE quenchz2 (f,g)
  USE params
  USE quench_m
  implicit none
!
  real, intent(inout), dimension(mx,my,mz) :: f,g
  integer i,j,k
  real, allocatable, dimension(:,:) :: fa, d1, d2, qq
!----------------------------------------------------------------------
!
  if (.not.do_quench .or. qmax.eq.1.) then
    return
  end if
!
  !$omp master
  allocate (tmp1(mx,my,mz), tmp2(mx,my,mz))
  !$omp end master
  !$omp barrier
!
  call regularize (f)
  do k=izs,ize
    tmp1(:,:,k) = abs(f(:,:,k))
  end do
  call dumpn(tmp1,'tmp1','qsm.dmp',1)
  call smooth3_set (tmp1,tmp2)
  call dumpn(tmp2,'tmp2','qsm.dmp',1)
  call mpi_send_z (f,gz1,1,hz1,1)
!
  allocate (fa(mx,-1:mz+2), d1(mx,-1:mz+1), d2(mx,0:mz+1), qq(mx,0:mz+1))
  !$omp barrier
  do j=iys,iye
     do i=1,mx
       d1(i,0)=f (i,j,1)-gz1(i,j,1)
     enddo
     do k=1,mz-1
       do i=1,mx
         d1(i,k)=f(i,j,k+1)-f(i,j,k)
         fa(i,k)=tmp2(i,j,k)
         d2(i,k)=abs(d1(i,k)-d1(i,k-1))
         qq(i,k)=d2(i,k)/(fa(i,k)+(1./qmax)*d2(i,k)+1e-20)
       enddo 
     enddo 
     do i=1,mx
       d1(i,mz)=hz1(i,j,1)-f (i,j,mz)
       d2(i,mz)=abs(d1(i,mz)-d1(i,mz-1))
       fa(i,mz)=tmp2(i,j,mz)
       qq(i,mz)=d2(i,mz)/(fa(i,mz)+(1./qmax)*d2(i,mz)+1e-20)
     enddo 
     do k=1,mz
       do i=1,mx
         tmp1(i,j,k)=qq(i,k)
       enddo 
     enddo 
  enddo
  !$omp barrier
  call regularize (tmp1)
  call dumpn(tmp1,'tmp3','qsm.dmp',1)
  call max3_set (tmp1, tmp2)
  call dumpn(tmp2,'tmp4','qsm.dmp',1)
  call smooth3_set (tmp2, tmp1)
  call dumpn(tmp1,'tmp5','qsm.dmp',1)
!
  do k=izs,ize
    g(:,:,k) = tmp1(:,:,k)
  end do
  call dumpn(g,'g','qsm.dmp',1)
!
  !$omp barrier
  !$omp master
  deallocate (tmp1, tmp2)
  deallocate (fa, d1, d2, qq)
  !$omp end master
!
  nflop = nflop + 11
END

!**********************************************************************
SUBROUTINE quenchxy (f, g)
  USE params, h=>scratch
  USE quench_m
  implicit none
  real, dimension(mx,my,mz):: f, g
  integer ix, iy, iz
!
  call quenchx(f,g)
  call quenchy(f,h)
  do iz=izs,ize
  do iy=1,my
  do ix=1,mx
    g(ix,iy,iz) = sign(max(abs(g(ix,iy,iz)), abs(h(ix,iy,iz))), f(ix,iy,iz))
  end do
  end do
  end do
!
END

!**********************************************************************
SUBROUTINE quenchyz (f, g, s)
  USE params
  USE quench_m
  implicit none
  real, dimension(mx,my,mz):: f, g, s
  integer ix, iy, iz
!
!$omp barrier
  call quenchz(f,g,s)
!$omp barrier
  call quenchy(f,s)
!$omp barrier
  do iz=izs,ize
  do iy=1,my
  do ix=1,mx
    g(ix,iy,iz) = sign(max(abs(g(ix,iy,iz)), abs(s(ix,iy,iz))), f(ix,iy,iz))
  end do
  end do
  end do
!
END

!**********************************************************************
SUBROUTINE quenchzx (f, g, s)
  USE params
  USE quench_m
  implicit none
  real, dimension(mx,my,mz):: f, g, s
  integer ix, iy, iz
!
!$omp barrier
  call quenchz(f,g,s)
!$omp barrier
  call quenchx(f,s)
!$omp barrier
  do iz=izs,ize
  do iy=1,my
  do ix=1,mx
    g(ix,iy,iz) = sign(max(abs(g(ix,iy,iz)), abs(s(ix,iy,iz))), f(ix,iy,iz))
  end do
  end do
  end do
!$omp barrier
!
END
