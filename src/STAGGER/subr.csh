#!/bin/csh

if (-d STAGGER) then
  set dir = STAGGER
else
  set dir = .
endif

foreach f ($*)
  ( cat $dir/$f; sed -f subr_set.sed $dir/$f )
endif
