!***********************************************************************
FUNCTION ddxupi (f)
  use params
  implicit none
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f, ddxupi
!hpf$ distribute(*,*,block):: f, ddxupi
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    ddxupi = 0.
    return
  end if
!
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)
  a1st = 1.
!
!$omp parallel private(i,j,k)
  do k=izs,ize
   do j=1,my
    ddxupi(mx-2,j,k) = ( &
                   a*(f(mx-1,j,k)-f(mx-2,j,k)) &
                 + b*(f(mx  ,j,k)-f(mx-3,j,k)) &
                 + c*(f(1   ,j,k)-f(mx-4,j,k)))
    ddxupi(mx-1,j,k) = ( &
                   a*(f(mx  ,j,k)-f(mx-1,j,k)) &
                 + b*(f(1   ,j,k)-f(mx-2,j,k)) &
                 + c*(f(2   ,j,k)-f(mx-3,j,k)))
    ddxupi(mx  ,j,k) = ( &
                   a*(f(1   ,j,k)-f(mx  ,j,k)) &
                 + b*(f(2   ,j,k)-f(mx-1,j,k)) &
                 + c*(f(3   ,j,k)-f(mx-2,j,k)))
    ddxupi(1  ,j,k) = ( &
                   a*(f(2   ,j,k)-f(1   ,j,k)) &
                 + b*(f(3   ,j,k)-f(mx  ,j,k)) &
                 + c*(f(4   ,j,k)-f(mx-1,j,k)))
    ddxupi(2  ,j,k) = ( &
                   a*(f(3   ,j,k)-f(2  ,j,k)) &
                 + b*(f(4   ,j,k)-f(1  ,j,k)) &
                 + c*(f(5   ,j,k)-f(mx ,j,k)))
   end do
   do j=1,my
    do i=3,mx-3
      ddxupi(i  ,j,k) = ( &
                   a*(f(i+1 ,j,k)-f(i   ,j,k)) &
                 + b*(f(i+2 ,j,k)-f(i-1 ,j,k)) &
                 + c*(f(i+3 ,j,k)-f(i-2 ,j,k)))
    end do
   end do
  end do
!$omp end parallel
!
  nstag = nstag+1
  nflop = nflop+8
END FUNCTION

!***********************************************************************
FUNCTION ddxupi1 (f)
  use params
  implicit none
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f, ddxupi1
!hpf$ distribute(*,*,block):: f, ddxupi1
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    ddxupi1 = 0.
    return
  end if
!
  a = 1.
!
!$omp parallel private(i,j,k)
  do k=izs,ize
   do j=1,my
    ddxupi1(mx  ,j,k) = ( &
                   a*(f(1   ,j,k)-f(mx  ,j,k)))
   end do
   do j=1,my
    do i=1,mx-1
      ddxupi1(i  ,j,k) = ( &
                   a*(f(i+1 ,j,k)-f(i   ,j,k)))
    end do
   end do
  end do
!$omp end parallel
!
  nstag1 = nstag1+1
  nflop = nflop+2
END FUNCTION

!***********************************************************************
FUNCTION ddxdni (f)
  use params
  implicit none
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f, ddxdni
!hpf$ distribute(*,*,block):: f, ddxdni
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    ddxdni = 0.
    return
  end if
!
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)
  a1st = 1.
!
!$omp parallel private(i,j,k)
  do k=izs,ize
   do j=1,my
    ddxdni(mx-1,j,k) = ( &
                   a*(f(mx-1,j,k)-f(mx-2,j,k)) &
                 + b*(f(mx  ,j,k)-f(mx-3,j,k)) &
                 + c*(f(1   ,j,k)-f(mx-4,j,k)))
    ddxdni(mx  ,j,k) = ( &
                   a*(f(mx  ,j,k)-f(mx-1,j,k)) &
                 + b*(f(1   ,j,k)-f(mx-2,j,k)) &
                 + c*(f(2   ,j,k)-f(mx-3,j,k)))
    ddxdni(1   ,j,k) = ( &
                   a*(f(1   ,j,k)-f(mx  ,j,k)) &
                 + b*(f(2   ,j,k)-f(mx-1,j,k)) &
                 + c*(f(3   ,j,k)-f(mx-2,j,k)))
    ddxdni(2  ,j,k) = ( &
                   a*(f(2   ,j,k)-f(1   ,j,k)) &
                 + b*(f(3   ,j,k)-f(mx  ,j,k)) &
                 + c*(f(4   ,j,k)-f(mx-1,j,k)))
    ddxdni(3  ,j,k) = ( &
                   a*(f(3   ,j,k)-f(2   ,j,k)) &
                 + b*(f(4   ,j,k)-f(1   ,j,k)) &
                 + c*(f(5   ,j,k)-f(mx  ,j,k)))
   end do
   do j=1,my
    do i=4,mx-2
      ddxdni(i,j,k) = ( &
                   a*(f(i   ,j,k)-f(i-1 ,j,k)) &
                 + b*(f(i+1 ,j,k)-f(i-2 ,j,k)) &
                 + c*(f(i+2 ,j,k)-f(i-3 ,j,k)))
    end do
   end do
  end do
!$omp end parallel
!
  nstag = nstag+1
  nflop = nflop+8
END FUNCTION

!***********************************************************************
FUNCTION ddxdni1 (f)
  use params
  implicit none
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f, ddxdni1
!hpf$ distribute(*,*,block):: f, ddxdni1
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    ddxdni1 = 0.
    return
  end if
!
  a = 1.
!
!$omp parallel private(i,j,k)
  do k=izs,ize
   do j=1,my
    ddxdni1(1   ,j,k) = ( &
                   a*(f(1   ,j,k)-f(mx  ,j,k)))
   end do
   do j=1,my
    do i=2,mx
      ddxdni1(i,j,k) = ( &
                   a*(f(i   ,j,k)-f(i-1 ,j,k)))
    end do
   end do
  end do
!$omp end parallel
!
  nstag1 = nstag1+1
  nflop = nflop+2
END FUNCTION
!***********************************************************************
FUNCTION ddxupi32 (f,j)
  use params
  implicit none
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f
  real, dimension(mx,mz):: ddxupi32
!hpf$ distribute(*,*,block):: f, ddxupi32
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    ddxupi32 = 0.
    return
  end if
!
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)
  a1st = 1.
!
!$omp parallel private(i,j,k)
  do k=izs,ize
    ddxupi32(mx-2,k) = ( &
                   a*(f(mx-1,j,k)-f(mx-2,j,k)) &
                 + b*(f(mx  ,j,k)-f(mx-3,j,k)) &
                 + c*(f(1   ,j,k)-f(mx-4,j,k)))
    ddxupi32(mx-1,k) = ( &
                   a*(f(mx  ,j,k)-f(mx-1,j,k)) &
                 + b*(f(1   ,j,k)-f(mx-2,j,k)) &
                 + c*(f(2   ,j,k)-f(mx-3,j,k)))
    ddxupi32(mx  ,k) = ( &
                   a*(f(1   ,j,k)-f(mx  ,j,k)) &
                 + b*(f(2   ,j,k)-f(mx-1,j,k)) &
                 + c*(f(3   ,j,k)-f(mx-2,j,k)))
    ddxupi32(1  ,k) = ( &
                   a*(f(2   ,j,k)-f(1   ,j,k)) &
                 + b*(f(3   ,j,k)-f(mx  ,j,k)) &
                 + c*(f(4   ,j,k)-f(mx-1,j,k)))
    ddxupi32(2  ,k) = ( &
                   a*(f(3   ,j,k)-f(2  ,j,k)) &
                 + b*(f(4   ,j,k)-f(1  ,j,k)) &
                 + c*(f(5   ,j,k)-f(mx ,j,k)))
    do i=3,mx-3
      ddxupi32(i  ,k) = ( &
                   a*(f(i+1 ,j,k)-f(i   ,j,k)) &
                 + b*(f(i+2 ,j,k)-f(i-1 ,j,k)) &
                 + c*(f(i+3 ,j,k)-f(i-2 ,j,k)))
    end do
  end do
!$omp end parallel
!
  nstag = nstag+1
  nflop = nflop+8
END FUNCTION

!***********************************************************************
FUNCTION ddxupi132 (f,j)
  use params
  implicit none
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f
  real, dimension(mx,mz):: ddxupi132
!hpf$ distribute(*,*,block):: f, ddxupi132
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    ddxupi132 = 0.
    return
  end if
!
  a = 1.
!
!$omp parallel private(i,j,k)
  do k=izs,ize
    ddxupi132(mx  ,k) = ( &
                   a*(f(1   ,j,k)-f(mx  ,j,k)))
    do i=1,mx-1
      ddxupi132(i  ,k) = ( &
                   a*(f(i+1 ,j,k)-f(i   ,j,k)))
    end do
  end do
!$omp end parallel
!
  nstag1 = nstag1+1
  nflop = nflop+2
END FUNCTION

!***********************************************************************
FUNCTION ddxdni32 (f,j)
  use params
  implicit none
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f
  real, dimension(mx,mz):: ddxdni32
!hpf$ distribute(*,*,block):: f, ddxdni32
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    ddxdni32 = 0.
    return
  end if
!
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)
  a1st = 1.
!
!$omp parallel private(i,j,k)
  do k=izs,ize
    ddxdni32(mx-1,k) = ( &
                   a*(f(mx-1,j,k)-f(mx-2,j,k)) &
                 + b*(f(mx  ,j,k)-f(mx-3,j,k)) &
                 + c*(f(1   ,j,k)-f(mx-4,j,k)))
    ddxdni32(mx  ,k) = ( &
                   a*(f(mx  ,j,k)-f(mx-1,j,k)) &
                 + b*(f(1   ,j,k)-f(mx-2,j,k)) &
                 + c*(f(2   ,j,k)-f(mx-3,j,k)))
    ddxdni32(1   ,k) = ( &
                   a*(f(1   ,j,k)-f(mx  ,j,k)) &
                 + b*(f(2   ,j,k)-f(mx-1,j,k)) &
                 + c*(f(3   ,j,k)-f(mx-2,j,k)))
    ddxdni32(2  ,k) = ( &
                   a*(f(2   ,j,k)-f(1   ,j,k)) &
                 + b*(f(3   ,j,k)-f(mx  ,j,k)) &
                 + c*(f(4   ,j,k)-f(mx-1,j,k)))
    ddxdni32(3  ,k) = ( &
                   a*(f(3   ,j,k)-f(2   ,j,k)) &
                 + b*(f(4   ,j,k)-f(1   ,j,k)) &
                 + c*(f(5   ,j,k)-f(mx  ,j,k)))
    do i=4,mx-2
      ddxdni32(i,k) = ( &
                   a*(f(i   ,j,k)-f(i-1 ,j,k)) &
                 + b*(f(i+1 ,j,k)-f(i-2 ,j,k)) &
                 + c*(f(i+2 ,j,k)-f(i-3 ,j,k)))
    end do
  end do
!$omp end parallel
!
  nstag = nstag+1
  nflop = nflop+8
END FUNCTION

!***********************************************************************
FUNCTION ddxdni132 (f,j)
  use params
  implicit none
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f
  real, dimension(mx,mz):: ddxdni132
!hpf$ distribute(*,*,block):: f, ddxdni132
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    ddxdni132 = 0.
    return
  end if
!
  a = 1.
!
!$omp parallel private(i,j,k)
  do k=izs,ize
    ddxdni132(1   ,k) = ( &
                   a*(f(1   ,j,k)-f(mx  ,j,k)))
    do i=2,mx
      ddxdni132(i,k) = ( &
                   a*(f(i   ,j,k)-f(i-1 ,j,k)))
    end do
  end do
!$omp end parallel
!
  nstag1 = nstag1+1
  nflop = nflop+2
END FUNCTION
!***********************************************************************
FUNCTION ddxupi22 (f)
  use params
  implicit none
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,mz):: ddxupi22, f
!hpf$ distribute(*,*,block):: f, ddxupi22
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    ddxupi22 = 0.
    return
  end if
!
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)
  a1st = 1.
!
!$omp parallel private(i,k)
  do k=izs,ize
    ddxupi22(mx-2,k) = ( &
                   a*(f(mx-1,k)-f(mx-2,k)) &
                 + b*(f(mx  ,k)-f(mx-3,k)) &
                 + c*(f(1   ,k)-f(mx-4,k)))
    ddxupi22(mx-1,k) = ( &
                   a*(f(mx  ,k)-f(mx-1,k)) &
                 + b*(f(1   ,k)-f(mx-2,k)) &
                 + c*(f(2   ,k)-f(mx-3,k)))
    ddxupi22(mx  ,k) = ( &
                   a*(f(1   ,k)-f(mx  ,k)) &
                 + b*(f(2   ,k)-f(mx-1,k)) &
                 + c*(f(3   ,k)-f(mx-2,k)))
    ddxupi22(1  ,k) = ( &
                   a*(f(2   ,k)-f(1   ,k)) &
                 + b*(f(3   ,k)-f(mx  ,k)) &
                 + c*(f(4   ,k)-f(mx-1,k)))
    ddxupi22(2  ,k) = ( &
                   a*(f(3   ,k)-f(2  ,k)) &
                 + b*(f(4   ,k)-f(1  ,k)) &
                 + c*(f(5   ,k)-f(mx ,k)))
    do i=3,mx-3
      ddxupi22(i  ,k) = ( &
                   a*(f(i+1 ,k)-f(i   ,k)) &
                 + b*(f(i+2 ,k)-f(i-1 ,k)) &
                 + c*(f(i+3 ,k)-f(i-2 ,k)))
    end do
  end do
!$omp end parallel
!
  nstag = nstag+1
  nflop = nflop+8
END FUNCTION

!***********************************************************************
FUNCTION ddxupi122 (f)
  use params
  implicit none
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,mz):: ddxupi122, f
!hpf$ distribute(*,*,block):: f, ddxupi122
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    ddxupi122 = 0.
    return
  end if
!
  a = 1.
!
!$omp parallel private(i,k)
  do k=izs,ize
    ddxupi122(mx  ,k) = ( &
                   a*(f(1   ,k)-f(mx  ,k)))
    do i=1,mx-1
      ddxupi122(i  ,k) = ( &
                   a*(f(i+1 ,k)-f(i   ,k)))
    end do
  end do
!$omp end parallel
!
  nstag1 = nstag1+1
  nflop = nflop+2
END FUNCTION

!***********************************************************************
FUNCTION ddxdni22 (f)
  use params
  implicit none
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,mz):: ddxdni22, f
!hpf$ distribute(*,*,block):: f, ddxdni22
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    ddxdni22 = 0.
    return
  end if
!
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)
  a1st = 1.
!
!$omp parallel private(i,k)
  do k=izs,ize
    ddxdni22(mx-1,k) = ( &
                   a*(f(mx-1,k)-f(mx-2,k)) &
                 + b*(f(mx  ,k)-f(mx-3,k)) &
                 + c*(f(1   ,k)-f(mx-4,k)))
    ddxdni22(mx  ,k) = ( &
                   a*(f(mx  ,k)-f(mx-1,k)) &
                 + b*(f(1   ,k)-f(mx-2,k)) &
                 + c*(f(2   ,k)-f(mx-3,k)))
    ddxdni22(1   ,k) = ( &
                   a*(f(1   ,k)-f(mx  ,k)) &
                 + b*(f(2   ,k)-f(mx-1,k)) &
                 + c*(f(3   ,k)-f(mx-2,k)))
    ddxdni22(2  ,k) = ( &
                   a*(f(2   ,k)-f(1   ,k)) &
                 + b*(f(3   ,k)-f(mx  ,k)) &
                 + c*(f(4   ,k)-f(mx-1,k)))
    ddxdni22(3  ,k) = ( &
                   a*(f(3   ,k)-f(2   ,k)) &
                 + b*(f(4   ,k)-f(1   ,k)) &
                 + c*(f(5   ,k)-f(mx  ,k)))
    do i=4,mx-2
      ddxdni22(i,k) = ( &
                   a*(f(i   ,k)-f(i-1 ,k)) &
                 + b*(f(i+1 ,k)-f(i-2 ,k)) &
                 + c*(f(i+2 ,k)-f(i-3 ,k)))
    end do
  end do
!$omp end parallel
!
  nstag = nstag+1
  nflop = nflop+8
END FUNCTION

!***********************************************************************
FUNCTION ddxdni122 (f)
  use params
  implicit none
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,mz):: ddxdni122, f
!hpf$ distribute(*,*,block):: f, ddxdni122
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    ddxdni122 = 0.
    return
  end if
!
  a = 1.
!
!$omp parallel private(i,k)
  do k=izs,ize
    ddxdni122(1   ,k) = ( &
                   a*(f(1   ,k)-f(mx  ,k)))
    do i=2,mx
      ddxdni122(i,k) = ( &
                   a*(f(i   ,k)-f(i-1 ,k)))
    end do
  end do
!$omp end parallel
!
  nstag1 = nstag1+1
  nflop = nflop+2
END FUNCTION
