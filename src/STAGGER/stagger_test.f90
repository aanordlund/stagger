!***********************************************************************
 !PROGRAM test
  !USE params
  !mx = 100
  !my = 100
  !mz = 100
  !call test_stagger 
 !END

!***********************************************************************
SUBROUTINE test_stagger
  USE params
  USE arrays
  logical debug
  if (debug(dbg_stagger)) print *,'calling test_stagger_ops',mpi_rank
  call test_stagger_ops
  if (debug(dbg_stagger)) print *,'calling test_smooth_max',mpi_rank
  call test_smooth_max
  if (debug(dbg_stagger)) print *,'calling test_divcurl',mpi_rank
  call test_divcurl
  if (debug(dbg_stagger)) print *,'calling test_del2',mpi_rank
  call test_del2
END 

!***********************************************************************
SUBROUTINE test_stagger_ops
!
! Test some of the stagger routines.
!
!-----------------------------------------------------------------------
  USE params
  USE arrays
  implicit none
  logical omp_in_parallel, debug
  integer ix, iy, iz, omp_get_thread_num, imax(3)
  real fmax1, fmax2, xx, yy, zz, ax, ay, az, kx, ky, kz, fmaxval, eps
  character(len=mid):: id="$Id: stagger_test.f90,v 1.45 2011/06/16 09:52:26 aake Exp $"
  character(len=mid):: fmt='(1x,a,4i5,2g12.4)'

  call print_id(id)

  if (debug(dbg_stagger)) &
    print '(i4,10e12.5)',mpi_rank,ym(1:2),ym(my-1:my),ymdn(1:2),ymdn(my-1:my)

!-----------------------------------------------------------------------
!  Test up-operators
!-----------------------------------------------------------------------
  ax = 3.
  ay = 1.
  az = 2.
  kx = 2.*pi/sx
  ky = 2.*pi/sy
  kz = 2.*pi/sz
!$omp parallel private(iz,iy,ix)
  do iz=izs,ize
    do iy=1,my
      do ix=1,mx
        scr1(ix,iy,iz) = ax*sin(kx*xmdn(ix)) + ay*sin(ky*ymdn(iy)) + az*sin(kz*zmdn(iz))
        scr2(ix,iy,iz) = ax*sin(kx*xm  (ix)) + ay*sin(ky*ym  (iy)) + az*sin(kz*zm  (iz))
      end do
    end do
  end do
!$omp end parallel
  call flush_mpi
  !$omp parallel
  call xup_set (scr1, scr3)
                                                   call dumpn (scr1, 'xup', 'stagger.dmp', 1)
                                                   call dumpn (scr3, 'xup', 'stagger.dmp', 1)
  !$omp end parallel
  !fmax1 = fmaxval('scr3',scr3)
  fmax1 = maxval(scr3(:,lb:ub,:))
  if (debug(dbg_stagger)) print *,'fmax1:a',mpi_rank, fmax1
  call flush_mpi
  !$omp paralleL
  call yup_set (scr3, scr4)
                                                   call dumpn (scr4, 'yup', 'stagger.dmp', 1)
  !$omp end parallel
  !fmax1 = fmaxval('scr1',scr1)
  !print *,'maxval call:',mpi_rank,lb,ub
  fmax1 = maxval(scr4(:,lb:ub,:))
  if (debug(dbg_stagger)) print *,'fmax1:b',mpi_rank, fmax1
  call flush_mpi
  !$omp parallel
  call zup_set (scr4, scr3)
                                                   call dumpn (scr3, 'zup', 'stagger.dmp', 1)
                                                   call dumpn (scr2, 'zup', 'stagger.dmp', 1)
  !$omp end parallel
  !fmax1 = fmaxval('scr3',scr3)
  fmax1 = maxval(scr3(:,lb:ub,:))
  if (debug(dbg_stagger)) print *,'fmax1:c',mpi_rank, fmax1

!$omp parallel private(iz)
  do iz=izs,ize
    scr2(:,:,iz) = abs(scr2(:,:,iz)-scr3(:,:,iz))
  end do
!$omp end parallel

  fmax2 = maxval(scr2(:,lb:ub,:))
  eps = ax*max(5e-6,1e-7*(64./(sx/dx))**5) &
      + az*max(5e-6,1e-7*(64./(sz/dz))**5) &
      + ay*max(5e-6,1e-7*(64./minval(sy/dym))**5)

  if (debug(dbg_stagger)) &
    print '(1x,a,i4,3e12.5)','test_stagger: rank,fmax1,fmax2,eps=', mpi_rank, fmax1, fmax2, eps

  if (abs(fmax2) .gt. eps) then
    imax = maxloc(scr2(:,lb:ub,:))
    imax(2) = imax(2)+lb-1
    print fmt,'WARNING: stagger up-ops appear to be incorrect at', mpi_rank, imax, fmax2, eps
  else if (master) then
    print *,'test_stagger: up OK', abs(fmax2), eps
  end if

!-----------------------------------------------------------------------
!  Test dn-operators
!-----------------------------------------------------------------------
  ax = 3.
  ay = 1.
  az = 2.
  kx = 2.*pi/sx
  ky = 2.*pi/sy
  kz = 2.*pi/sz
!$omp parallel private(iz,iy,ix)
  do iz=izs,ize
    do iy=1,my
      do ix=1,mx
        scr1(ix,iy,iz) = ax*sin(kx*xm  (ix)) + ay*sin(ky*ym  (iy)) + az*sin(kz*zm  (iz))
        scr2(ix,iy,iz) = ax*sin(kx*xmdn(ix)) + ay*sin(ky*ymdn(iy)) + az*sin(kz*zmdn(iz))
      end do
    end do
  end do
!$omp end parallel
  call flush_mpi
  !$omp parallel
  call xdn_set (scr1, scr3)
                                                   call dumpn (scr3, 'xdn', 'stagger.dmp', 1)
  !$omp end parallel
  !fmax1 = fmaxval('scr3',scr3)
  fmax1 = maxval(scr3(:,lb:ub,:))
  if (debug(dbg_stagger)) print *,'fmax1:a',mpi_rank, fmax1
  call flush_mpi
  !$omp parallel
  call ydn_set (scr3, scr4)
                                                   call dumpn (scr4, 'ydn', 'stagger.dmp', 1)
  !$omp end parallel
  !fmax1 = fmaxval('scr1',scr1)
  fmax1 = maxval(scr4(:,lb:ub,:))
  if (debug(dbg_stagger)) print *,'fmax1:b',mpi_rank, fmax1
  call flush_mpi
  !$omp parallel
  call zdn_set (scr4, scr3)
                                                   call dumpn (scr3, 'zdn', 'stagger.dmp', 1)
  !$omp end parallel
  !fmax1 = fmaxval('scr3',scr3)
  fmax1 = maxval(scr3(:,lb:ub,:))
  if (debug(dbg_stagger)) print *,'fmax1:c',mpi_rank, fmax1

!$omp parallel private(iz)
  do iz=izs,ize
    scr4(:,:,iz) = abs(scr2(:,:,iz)-scr3(:,:,iz))
  end do
!$omp end parallel

  fmax2 = maxval(scr4(:,lb:ub,:))
  eps = ax*max(5e-6,1e-7*(64./(sx/dx))**5) &
      + az*max(5e-6,1e-7*(64./(sz/dz))**5) &
      + ay*max(5e-6,1e-7*(64./minval(sy/dym))**5)

  if (debug(dbg_stagger)) &
    print '(1x,a,i4,3e12.5)','test_stagger: rank,fmax1,fmax2,eps=', mpi_rank, fmax1, fmax2, eps

  if (abs(fmax2) .gt. eps) then
    imax = maxloc(scr4(:,lb:ub,:))
    imax(2) = imax(2)+lb-1
    print fmt,'WARNING: stagger dn-ops appear to be incorrect at', mpi_rank, imax, fmax2, eps
    if (debug(dbg_stagger)) then
      print '(1x,a,i5/(10g11.3))', 'x-dir', mpi_rank, scr4(:,imax(2),imax(3))
      print '(1x,a,i5/(10g11.3))', 'y-dir', mpi_rank, scr4(imax(1),:,imax(3))
      print '(1x,a,i5/(10g11.3))', 'z-dir', mpi_rank, scr4(imax(1),imax(2),:)
      print '(1x,a,i5/(10g11.3))', 'x-dir', mpi_rank, scr2(:,imax(2),imax(3))
      print '(1x,a,i5/(10g11.3))', 'y-dir', mpi_rank, scr2(imax(1),:,imax(3))
      print '(1x,a,i5/(10g11.3))', 'z-dir', mpi_rank, scr2(imax(1),imax(2),:)
    end if
  else if (master) then
    print *,'test_stagger: dn OK', abs(fmax2), eps
  end if

!-----------------------------------------------------------------------
!  Test dd.up-operators
!-----------------------------------------------------------------------
  ax = 3.
  ay = 1.
  az = 2.
  kx = 2.*2.*pi/sx
  ky = 2.*2.*pi/sy
  kz = 1.*2.*pi/sz
  if (debug(dbg_stagger)) print *,'ax,sx,kx:',ax,sx,kx
  if (debug(dbg_stagger)) print *,'ay,sy,ky:',ay,sy,ky,lb,ub
  if (debug(dbg_stagger)) print *,'az,sz,kz:',az,sz,kz
!$omp parallel private(iz,iy,ix)
  do iz=izs,ize
    do iy=1,my
      do ix=1,mx
        scr1(ix,iy,iz) = ax*sin(kx*xmdn(ix)) + ay*sin(ky*ymdn(iy)) + az*sin(kz*zmdn(iz))
        scr2(ix,iy,iz) = ax*kx*cos(kx*xm(ix)) + ay*ky*cos(ky*ym(iy)) + az*kz*cos(kz*zm(iz))
      end do
    end do
  end do
  call ddxup_set (scr1, scr3)
                                                   call dumpn (scr3, 'ddxup', 'stagger.dmp', 1)
!$omp end parallel
  fmax1 = maxval(scr3(:,lb:ub,:))
  if (debug(dbg_stagger)) print *,'fmax1:a',mpi_rank, fmax1
  !$omp parallel
  call ddyup_add (scr1, scr3)
                                                   call dumpn (scr3, 'ddyup', 'stagger.dmp', 1)
  !$omp end parallel
  fmax1 = maxval(scr3(:,lb:ub,:))
  if (debug(dbg_stagger)) print *,'fmax1:b',mpi_rank, fmax1
  !$omp parallel
  call ddzup_add (scr1, scr3)
                                                   call dumpn (scr3, 'ddzup', 'stagger.dmp', 1)
  !$omp end parallel
  !fmax1 = fmaxval('scr3',scr3)
  fmax1 = maxval(scr3(:,lb:ub,:))
  if (debug(dbg_stagger)) print *,'fmax1:c',mpi_rank, fmax1
!$omp parallel private(iz)
  do iz=izs,ize
    scr2(:,:,iz) = abs(scr2(:,:,iz)-scr3(:,:,iz))
  end do
!$omp end parallel
  fmax2 = maxval(scr2(:,lb:ub,:))
  eps = ax*kx*max(3e-5*(sx/dx)/100.,1e-6*(64./(sx/dx))**6) &
      + az*kz*max(3e-5*(sz/dz)/100.,1e-6*(64./(sz/dz))**6) &
      + ay*ky*max(3e-5*maxval(sy/dym)/100.,1e-6*(64./minval(sy/dym))**6)

  if (debug(dbg_stagger)) &
    print '(1x,a,i4,3e12.5)','test_stagger: rank,fmax1,fmax2,eps=', mpi_rank, fmax1, fmax2, eps
  if (fmax2 .gt. eps) then
    imax = maxloc(scr2(:,lb:ub,:))
    imax(2) = imax(2)+lb-1
    print fmt,'WARNING: stagger dd.up-ops appear to be incorrect at', mpi_rank, imax, fmax2, eps
    if (debug(dbg_stagger)) then
      print '(1x,a,i5/(10g11.3))', 'dxiup', mpi_rank, dxidxup
      print '(1x,a,i5/(10g11.3))', 'dyiup', mpi_rank, dyidyup
      print '(1x,a,i5/(10g11.3))', 'dziup', mpi_rank, dzidzup
      print '(1x,a,i5/(10g11.3))', 'x-dir', mpi_rank, scr2(:,imax(2),imax(3))
      print '(1x,a,i5/(10g11.3))', 'y-dir', mpi_rank, scr2(imax(1),:,imax(3))
      print '(1x,a,i5/(10g11.3))', 'z-dir', mpi_rank, scr2(imax(1),imax(2),:)
    end if
  else if (master) then
    print *,'test_stagger: dd.up OK', fmax2, eps
  end if

!-----------------------------------------------------------------------
!  Test dd.dn-operators
!-----------------------------------------------------------------------
  ax = 3.
  ay = 1.
  az = 2.
  kx = 2.*2.*pi/sx
  ky = 2.*2.*pi/sy
  kz = 1.*2.*pi/sz
!$omp parallel private(iz,iy,ix)
  do iz=izs,ize
    do iy=1,my
      do ix=1,mx
        scr1(ix,iy,iz) = ax*sin(kx*xm(ix)) + ay*sin(ky*ym(iy)) + az*sin(kz*zm(iz))
        scr2(ix,iy,iz) = ax*kx*cos(kx*xmdn(ix)) + ay*ky*cos(ky*ymdn(iy)) + az*kz*cos(kz*zmdn(iz))
      end do
    end do
  end do
  call ddxdn_set (scr1, scr3)
                                                   call dumpn (scr3, 'ddxdn', 'stagger.dmp', 1)
!$omp end parallel
  fmax1 = maxval(scr3(:,lb:ub,:))
  if (debug(dbg_stagger)) &
    print *,'fmax1:a',mpi_rank, fmax1
  !$omp parallel
  call ddydn_add (scr1, scr3)
                                                   call dumpn (scr3, 'ddydn', 'stagger.dmp', 1)
  !$omp end parallel
  fmax1 = maxval(scr3(:,lb:ub,:))
  if (debug(dbg_stagger)) &
    print *,'fmax1:b',mpi_rank, fmax1
  !$omp parallel
  call ddzdn_add (scr1, scr3)
                                                   call dumpn (scr3, 'ddzdn', 'stagger.dmp', 1)
  !$omp end parallel
  !fmax1 = fmaxval('scr3',scr3)
  fmax1 = maxval(scr3(:,lb:ub,:))
  if (debug(dbg_stagger)) &
    print *,'fmax1:c',mpi_rank, fmax1
!$omp parallel private(iz)
  do iz=izs,ize
    scr2(:,:,iz) = abs(scr2(:,:,iz)-scr3(:,:,iz))
  end do
!$omp end parallel
  fmax2 = maxval(scr2(:,lb:ub,:))
  eps = ax*kx*max(3e-5*(sx/dx)/100.,1e-6*(64./(sx/dx))**6) &
      + az*kz*max(3e-5*(sz/dz)/100.,1e-6*(64./(sz/dz))**6) &
      + ay*ky*max(3e-5*maxval(sy/dym)/100.,1e-6*(64./minval(sy/dym))**6)

  if (debug(dbg_stagger)) &
    print '(1x,a,i4,3e12.5)','test_stagger: rank,fmax1,fmax2,eps=', mpi_rank, fmax1, fmax2, eps
  if (fmax2 .gt. eps) then
    imax = maxloc(scr2(:,lb:ub,:))
    imax(2) = imax(2)+lb-1
    print fmt,'WARNING: stagger dd.dn-ops appear to be incorrect at', mpi_rank, imax, fmax2, eps
  else if (master) then
    print *,'test_stagger: dd.dn OK', fmax2, eps
  end if


!-----------------------------------------------------------------------
!  Test up-operators
!-----------------------------------------------------------------------
  ax = 3.
  ay = 1.
  az = 2.
  kx = 2.*pi/sx
  ky = 2.*pi/sy
  kz = 2.*pi/sz
!$omp parallel private(iz,iy,ix)
  do iz=izs,ize
    do iy=1,my
      do ix=1,mx
        scr1(ix,iy,iz) = ax*sin(kx*xmdn(ix)) + ay*sin(ky*ymdn(iy)) + az*sin(kz*zmdn(iz))
        scr2(ix,iy,iz) = ax*sin(kx*xm  (ix)) + ay*sin(ky*ym  (iy)) + az*sin(kz*zm  (iz))
      end do
    end do
  end do
  call flush_mpi
  call xup1_set (scr1, scr3)
                                                   call dumpn (scr3, 'xup1', 'stagger.dmp', 1)
!$omp end parallel
  !fmax1 = fmaxval('scr3',scr3)
  fmax1 = maxval(scr3(:,lb:ub,:))
  if (debug(dbg_stagger)) print *,'fmax1:a',mpi_rank, fmax1
  call flush_mpi
  !$omp parallel
  call yup1_set (scr3, scr4)
                                                   call dumpn (scr4, 'yup1', 'stagger.dmp', 1)
  !$omp end parallel
  !fmax1 = fmaxval('scr1',scr1)
  fmax1 = maxval(scr4(:,lb:ub,:))
  if (debug(dbg_stagger)) print *,'fmax1:b',mpi_rank, fmax1
  call flush_mpi
  !$omp parallel
  call zup1_set (scr4, scr3)
                                                   call dumpn (scr3, 'zup1', 'stagger.dmp', 1)
  !$omp end parallel
  !fmax1 = fmaxval('scr3',scr3)
  fmax1 = maxval(scr3(:,lb:ub,:))
  if (debug(dbg_stagger)) print *,'fmax1:c',mpi_rank, fmax1

!$omp parallel private(iz)
  do iz=izs,ize
    scr2(:,:,iz) = abs(scr2(:,:,iz)-scr3(:,:,iz))
  end do
!$omp end parallel

  fmax2 = maxval(scr2(:,lb:ub,:))
  eps = ax*max(2e-6,1e-2*(64./(sx/dx))**2) &
      + az*max(2e-6,1e-2*(64./(sz/dz))**2) &
      + ay*max(2e-6,1e-2*(64./minval(sy/dym))**2)

  if (debug(dbg_stagger)) &
    print '(1x,a,i4,3e12.5)','test_stagger: rank,fmax1,fmax2,eps=', mpi_rank, fmax1, fmax2, eps

  if (abs(fmax2) .gt. eps) then
    imax = maxloc(scr2(:,lb:ub,:))
    imax(2) = imax(2)+lb-1
    print fmt,'WARNING: stagger up1-ops appear to be incorrect at', mpi_rank, imax, fmax2, eps
  else if (master) then
    print *,'test_stagger: up1 OK', abs(fmax2), eps
  end if

!-----------------------------------------------------------------------
!  Test dn-operators
!-----------------------------------------------------------------------
  ax = 3.
  ay = 1.
  az = 2.
  kx = 2.*pi/sx
  ky = 2.*pi/sy
  kz = 2.*pi/sz
!$omp parallel private(iz,iy,ix)
  do iz=izs,ize
    do iy=1,my
      do ix=1,mx
        scr1(ix,iy,iz) = ax*sin(kx*xm  (ix)) + ay*sin(ky*ym  (iy)) + az*sin(kz*zm  (iz))
        scr2(ix,iy,iz) = ax*sin(kx*xmdn(ix)) + ay*sin(ky*ymdn(iy)) + az*sin(kz*zmdn(iz))
      end do
    end do
  end do
  call flush_mpi
  call xdn1_set (scr1, scr3)
                                                   call dumpn (scr3, 'xdn1', 'stagger.dmp', 1)
!$omp end parallel
  !fmax1 = fmaxval('scr3',scr3)
  fmax1 = maxval(scr3(:,lb:ub,:))
  if (debug(dbg_stagger)) print *,'fmax1:a',mpi_rank, fmax1
  call flush_mpi
  !$omp parallel
  call ydn1_set (scr3, scr4)
                                                   call dumpn (scr4, 'ydn1', 'stagger.dmp', 1)
  !$omp end parallel
  !fmax1 = fmaxval('scr1',scr1)
  fmax1 = maxval(scr4(:,lb:ub,:))
  if (debug(dbg_stagger)) print *,'fmax1:b',mpi_rank, fmax1
  call flush_mpi
  !$omp parallel
  call zdn1_set (scr4, scr3)
                                                   call dumpn (scr3, 'zdn1', 'stagger.dmp', 1)
  !$omp end parallel
  !fmax1 = fmaxval('scr3',scr3)
  fmax1 = maxval(scr3(:,lb:ub,:))
  if (debug(dbg_stagger)) print *,'fmax1:c',mpi_rank, fmax1

!$omp parallel private(iz)
  do iz=izs,ize
    scr2(:,:,iz) = abs(scr2(:,:,iz)-scr3(:,:,iz))
  end do
!$omp end parallel

  fmax2 = maxval(scr2(:,lb:ub,:))
  eps = ax*max(2e-6,1e-2*(64./(sx/dx))**2) &
      + az*max(2e-6,1e-2*(64./(sz/dz))**2) &
      + ay*max(2e-6,1e-2*(64./minval(sy/dym))**2)

  if (debug(dbg_stagger)) &
    print '(1x,a,i4,3e12.5)','test_stagger: rank,fmax1,fmax2,eps=', mpi_rank, fmax1, fmax2, eps

  if (abs(fmax2) .gt. eps) then
    imax = maxloc(scr2(:,lb:ub,:))
    imax(2) = imax(2)+lb-1
    print fmt,'WARNING: stagger dn1-ops appear to be incorrect at', mpi_rank, imax, fmax2, eps
  else if (master) then
    print *,'test_stagger: dn1 OK', abs(fmax2), eps
  end if

!-----------------------------------------------------------------------
!  Test dd.up-operators
!-----------------------------------------------------------------------
  ax = 3.
  ay = 1.
  az = 2.
  kx = 2.*2.*pi/sx
  ky = 2.*2.*pi/sy
  kz = 1.*2.*pi/sz
!$omp parallel private(iz,iy,ix)
  do iz=izs,ize
    do iy=1,my
      do ix=1,mx
        scr1(ix,iy,iz) = ax*sin(kx*xmdn(ix)) + ay*sin(ky*ymdn(iy)) + az*sin(kz*zmdn(iz))
        scr2(ix,iy,iz) = ax*kx*cos(kx*xm(ix)) + ay*ky*cos(ky*ym(iy)) + az*kz*cos(kz*zm(iz))
      end do
    end do
  end do
  call ddxup1_set (scr1, scr3)
!$omp end parallel
  fmax1 = maxval(scr3(:,lb:ub,:))
  if (debug(dbg_stagger)) &
    print *,'fmax1:a',mpi_rank, fmax1
  !$omp parallel
  call ddyup1_add (scr1, scr3)
                                                   call dumpn (scr3, 'ddyup1', 'stagger.dmp', 1)
  !$omp end parallel
  fmax1 = maxval(scr3(:,lb:ub,:))
  if (debug(dbg_stagger)) &
    print *,'fmax1:b',mpi_rank, fmax1
  !$omp parallel
  call ddzup1_add (scr1, scr3)
  !$omp end parallel
  !fmax1 = fmaxval('scr3',scr3)
  fmax1 = maxval(scr3(:,lb:ub,:))
  if (debug(dbg_stagger)) &
    print *,'fmax1:c',mpi_rank, fmax1
!$omp parallel private(iz)
  do iz=izs,ize
    scr2(:,:,iz) = abs(scr2(:,:,iz)-scr3(:,:,iz))
  end do
!$omp end parallel
  fmax2 = maxval(scr2(:,lb:ub,:))
  eps = ax*kx*max(3e-5*(sx/dx)/200.,1e-2*(64./(sx/dx))**2) &
      + az*kz*max(3e-5*(sz/dz)/200.,1e-2*(64./(sz/dz))**2) &
      + ay*ky*max(3e-5*maxval(sy/dym)/200.,1e-2*(64./minval(sy/dym))**2)

  if (debug(dbg_stagger)) &
    print '(1x,a,i4,3e12.5)','test_stagger: rank,fmax1,fmax2,eps=', mpi_rank, fmax1, fmax2, eps
  if (fmax2 .gt. eps) then
    imax = maxloc(scr2(:,lb:ub,:))
    imax(2) = imax(2)+lb-1
    print fmt,'WARNING: stagger dd.up1-ops appear to be incorrect at', mpi_rank, imax, fmax2, eps
  else if (master) then
    print *,'test_stagger: dd.up1 OK', fmax2, eps
  end if

!-----------------------------------------------------------------------
!  Test dd.dn-operators
!-----------------------------------------------------------------------
  ax = 3.
  ay = 1.
  az = 2.
  kx = 2.*2.*pi/sx
  ky = 2.*2.*pi/sy
  kz = 1.*2.*pi/sz
!$omp parallel private(iz,iy,ix)
  do iz=izs,ize
    do iy=1,my
      do ix=1,mx
        scr1(ix,iy,iz) = ax*sin(kx*xm(ix)) + ay*sin(ky*ym(iy)) + az*sin(kz*zm(iz))
        scr2(ix,iy,iz) = ax*kx*cos(kx*xmdn(ix)) + ay*ky*cos(ky*ymdn(iy)) + az*kz*cos(kz*zmdn(iz))
      end do
    end do
  end do
  call ddxdn1_set (scr1, scr3)
!$omp end parallel
  fmax1 = maxval(scr3(:,lb:ub,:))
  if (debug(dbg_stagger)) &
    print *,'fmax1:a',mpi_rank, fmax1
  !$omp parallel
  call ddydn1_add (scr1, scr3)
                                                   call dumpn (scr3, 'ddydn1', 'stagger.dmp', 1)
  !$omp end parallel
  fmax1 = maxval(scr3(:,lb:ub,:))
  if (debug(dbg_stagger)) &
    print *,'fmax1:b',mpi_rank, fmax1
  !$omp parallel
  call ddzdn1_add (scr1, scr3)
  !$omp end parallel
  !fmax1 = fmaxval('scr3',scr3)
  fmax1 = maxval(scr3(:,lb:ub,:))
  if (debug(dbg_stagger)) &
    print *,'fmax1:c',mpi_rank, fmax1
!$omp parallel private(iz)
  do iz=izs,ize
    scr2(:,:,iz) = abs(scr2(:,:,iz)-scr3(:,:,iz))
  end do
!$omp end parallel
  fmax2 = maxval(scr2(:,lb:ub,:))
  eps = ax*kx*max(3e-5*(sx/dx)/200.,1e-2*(64./(sx/dx))**2) &
      + az*kz*max(3e-5*(sz/dz)/200.,1e-2*(64./(sz/dz))**2) &
      + ay*ky*max(3e-5*maxval(sy/dym)/200.,1e-2*(64./minval(sy/dym))**2)

  if (debug(dbg_stagger)) &
    print '(1x,a,i4,3e12.5)','test_stagger: rank,fmax1,fmax2,eps=', mpi_rank, fmax1, fmax2, eps
  if (fmax2 .gt. eps) then
    imax = maxloc(scr2(:,lb:ub,:))
    imax(2) = imax(2)+lb-1
    print fmt,'WARNING: stagger dd.dn1-ops appear to be incorrect at', mpi_rank, imax, fmax2, eps
  else if (master) then
    print *,'test_stagger: dd.dn1 OK', fmax2, eps
  end if

END

!***********************************************************************
 SUBROUTINE test_smooth_max
!
! Test the parallelization of smooth3(max3())
!
!-----------------------------------------------------------------------
  USE params
  USE arrays
  implicit none
  integer ix, iy, iz, i(3)
  integer omp_get_thread_num
  real fmax1, fmax2, fmaxval, ran1s
  logical omp_in_parallel, debug

!$omp parallel private(iz,iy,ix), shared(fmax1)
  do iz=izs,ize
    do iy=1,my
      do ix=1,mx
        scr1(ix,iy,iz) = mod(5*(ix+ixoff)+3*(iy+iyoff)+7*(iz+izoff),mxtot/2)
      end do
    end do
  end do
!$omp barrier
  call fmaxval_omp ('scr1',scr1,fmax1)
!$omp end parallel
  call dumpn (scr1, 'input', 'smooth.dmp', 0)

!  This part for debugging only -- it causes a segv on rs6000 with allocates
  !print *,mpi_rank,'trace1a'
  !scr4 = max(scr1,cshift(scr1,shift=-1,dim=1),cshift(scr1,shift=1,dim=1))
  !print *,mpi_rank,'trace1b'
  !scr5 = max(scr4,cshift(scr4,shift=-1,dim=2),cshift(scr4,shift=1,dim=2))
  !print *,mpi_rank,'trace1c'
  !scr4 = max(scr5,cshift(scr5,shift=-1,dim=3),cshift(scr5,shift=1,dim=3))
  !call dumpn (scr4, 'xx', 'smooth.dmp', 1)
  !print *,mpi_rank,'trace1d'
  !scr5 = (scr4+cshift(scr4,shift=-1,dim=1)+cshift(scr4,shift=1,dim=1))/3.
  !print *,mpi_rank,'trace1e'
  !scr4 = (scr5+cshift(scr5,shift=-1,dim=2)+cshift(scr5,shift=1,dim=2))/3.
  !print *,mpi_rank,'trace1f'
  !scr5 = (scr4+cshift(scr4,shift=-1,dim=3)+cshift(scr4,shift=1,dim=3))/3.
  !call dumpn (scr5, 'xx', 'smooth.dmp', 1)
  !print *,mpi_rank,'trace2'

  fmax2 = fmaxval('scr1',scr1)
  if (debug(dbg_stagger)) print *,'test fmaxval:', fmax1, fmax2
  if (abs(fmax1-fmax2) .gt. 1e-5) then
    if (master) print *,'ERROR: results from fmaxval and fmaxval_omp differ'
    !call end_mpi
    !stop
  end if

  if (do_trace) print *,'calling max3_set'
  call dumpn (scr1, 'scr1', 'smooth.dmp', 1)
  scr4 = scr1
  !$omp parallel
  call max3_set (scr1, scr2)
                                                   call dumpn (scr2, 'max3', 'stagger.dmp', 1)
  !$omp end parallel
  call dumpn (scr2, 'scr2', 'smooth.dmp', 1)

  if (do_trace) print *,'calling smooth3max3_set'
  !$omp parallel
  call smooth3max3_set (scr4, scr3)
                                                   call dumpn (scr3, 'sm3max3', 'stagger.dmp', 1)
  !$omp end parallel
  call dumpn (scr3, 'scr3', 'smooth.dmp', 1)

  if (do_trace) print *,'calling smooth3_set'
  !$omp parallel
  call smooth3_set (scr2, scr1)
                                                   call dumpn (scr1, 'sm3', 'stagger.dmp', 1)
  !$omp end parallel
  call dumpn (scr1, 'scr1', 'smooth.dmp', 1)

!$omp parallel private(iz), shared(fmax1)
  do iz=izs,ize
    scr2(:,:,iz) = abs(scr1(:,:,iz)-scr4(:,:,iz))
  end do
  call fmaxval_omp ('scr2',scr2,fmax1)
!$omp end parallel

  fmax2 = fmaxval('scr2',scr2)
  if (debug(dbg_stagger)) print *,'test_smooth_max:', fmax1, fmax2

  if (fmax1-fmax2 .gt. 1e-5) then
    i = maxloc(scr2(:,lb:ub,:))
    i(2) = i(2)+lb-1
    if (master) then
      print *,'ERROR: results from smooth3(max3()) and smooth3max3_set differ', fmax1, fmax2
      print *, i, scr1(i(1),i(2),i(3)), scr4(i(1),i(2),i(3))
    end if
    !call end_mpi
    !stop
  else if (master) then
    print *,'test_smooth_max: OK', fmax1, fmax2
  end if
END

!***********************************************************************
SUBROUTINE test_del2
!
! Test some of the stagger routines.
!
!-----------------------------------------------------------------------
  USE params
  USE arrays
  implicit none
  real:: ax, ay, az, fx1, fx2, fy1, fy2, fz1, fz2, fmax, eps
  real xt(mx), yt(my), zt(mz)
  integer:: ix, iy, iz, i(3), rank
  logical debug

  ax = 1.
  ay = 1.
  az = 1.

  xt = (/(ix+ixoff,ix=1,mx)/)-1
  yt = (/(iy+iyoff,iy=1,my)/)-1
  zt = (/(iz+izoff,iz=1,mz)/)-1

!$omp parallel private(ix,iy,iz,fx1,fx2,fy1,fy2,fz1,fz2)
  do iz=izs,ize
  do iy=1,my
    do ix=1,mx
      scr1(ix,iy,iz) = ax*xt(ix)
      scr2(ix,iy,iz) = ay*yt(iy)
      scr3(ix,iy,iz) = az*zt(iz)
      fx1 = mod(xt(ix)+1.+mxtot+1e-5,real(mxtot))
      fx2 = mod(xt(ix)-1.+mxtot+1e-5,real(mxtot))
      fy1 = mod(yt(iy)+1.+mytot+1e-5,real(mytot))
      fy2 = mod(yt(iy)-1.+mytot+1e-5,real(mytot))
      fz1 = mod(zt(iz)+1.+mztot+1e-5,real(mztot))
      fz2 = mod(zt(iz)-1.+mztot+1e-5,real(mztot))
      scr4(ix,iy,iz) = max (0.0, 0.5*(ax*(fx2-fx1)/dxm(ix)+ay*(fy2-fy1)/dym(iy)+az*(fz2-fz1)/dzm(iz)))
    end do
  end do
  end do

  call dif2_set (scr1, scr2, scr3, scr5)
                                                   call dumpn (scr5, 'dif2', 'stagger.dmp', 1)
!$omp end parallel

!$omp parallel private(ix,iy,iz)
  do iz=izs,ize
  do iy=1,my
  do ix=1,mx
    scr1(ix,iy,iz) = abs(scr4(ix,iy,iz)-scr5(ix,iy,iz))
  end do
  end do
  end do
!$omp end parallel

  call fmaxval_subr ('scr1',scr1, fmax)
  call fmaxval_subr ('scr4',scr4, eps)
  eps = 1e-6*eps
  if (fmax > eps) then
    i = maxloc(scr1(:,lb:ub,:))
    i(2) = i(2)+lb-1
    do rank=0,mpi_size-1
      call flush_mpi
      if (rank == mpi_rank) then
        print '(1x,a,4i5,2g12.4)','test_del2: difference too large at', mpi_rank, i, fmax, scr1(i(1),i(2),i(3))
        print *, mpi_rank, scr4(i(1),i(2),i(3)), scr5(i(1),i(2),i(3))
        if (debug(dbg_stagger)) print '(i5,16f6.1/(5x,16f6.1))', mpi_rank, scr4(i(1),:,i(3))
        if (debug(dbg_stagger)) print '(i5,16f6.1/(5x,16f6.1))', mpi_rank, scr5(i(1),:,i(3))
        if (debug(dbg_stagger)) print '(i5,16f6.1/(5x,16f6.1))', mpi_rank, scr4(i(1),i(2),:)
        if (debug(dbg_stagger)) print '(i5,16f6.1/(5x,16f6.1))', mpi_rank, scr5(i(1),i(2),:)
        if (debug(dbg_stagger)) print '(i5,16f6.1/(5x,16f6.1))', mpi_rank, dym
        if (debug(dbg_stagger)) print '(i5,16f6.1/(5x,16f6.1))', mpi_rank, dzm
      end if
    end do
    !call end_mpi
    !stop
  else if (master) then
    print *,'test_del2: OK', fmax, eps
  end if

END

!***********************************************************************
SUBROUTINE test_divcurl
!
! Test that div(curl()) = 0
!
!-----------------------------------------------------------------------
  USE params
  USE arrays
  implicit none
  real:: kx,ky,kz,err,eps,fmaxval
  integer:: ix,iy,iz,i(3)
  logical debug

  kx = 2.*pi/sx
  ky = 2.*pi/sy
  kz = 2.*pi/sz

!$omp parallel private(ix,iy,iz)
  do iz=izs,ize
  do iy=1,my
  do ix=1,mx
    scr1(ix,iy,iz) = cos(2.*kx*xm  (ix)+4.*ky*ymdn(iy)+3.*kz*zmdn(iz))
    scr2(ix,iy,iz) = cos(3.*kx*xmdn(ix)+3.*ky*ym  (iy)+5.*kz*zmdn(iz))
    scr3(ix,iy,iz) = cos(4.*kx*xmdn(ix)+2.*ky*ymdn(iy)+4.*kz*zm  (iz))
  end do
  end do
  end do
  !$omp end parallel
  !$omp parallel
  call ddxup_set(scr2,scr6)
  call ddyup_sub(scr1,scr6)
  call ddyup_set(scr3,scr4)
  !$omp end parallel
!$omp barrier                           ! scr2 and scr6 need to be consistent
  !$omp parallel
  call ddzup_sub(scr2,scr4)
  !$omp end parallel
  !$omp parallel
  call ddzup_set(scr1,scr5)
  !$omp end parallel
  !$omp parallel
  call ddxup_sub(scr3,scr5)
  !$omp end parallel
  !$omp parallel

  call ddxup_set(scr4,scr3)
  !$omp end parallel
  !$omp parallel
  call ddyup_add(scr5,scr3)
  !$omp end parallel
  !$omp parallel
  call ddzup_add(scr6,scr3)
  !$omp end parallel

  !$omp parallel private(ix,iy,iz)
  do iz=izs,ize
  do iy=1,my
  do ix=1,mx
    scr3(ix,iy,iz) = abs(scr3(ix,iy,iz))
  end do
  end do
  end do
  !$omp end parallel

  err = fmaxval('divcurl',scr3)
  eps = maxval(max(4e-4*(sy/dym(lb:ub))/100.,2e-5*(64./(sy/dym(lb:ub)))**6))
  eps = max(eps,4e-4*(sx/dx)/100.,2e-5*(64./(sx/dx))**6)
  eps = max(eps,4e-4*(sz/dz)/100.,2e-5*(64./(sz/dz))**6)
  eps = eps*0.1*(kx**2+ky**2+kz**2)
  if (err > eps) then
    i = maxloc(scr3(:,lb:ub,:))
    i(2) = i(2)+lb-1
    print *,'ERROR: div(curl()) too large at',mpi_rank,i,err,eps
    print '(a,2i5/(10g12.5))','x-dir',i(2),i(3),scr3(:,i(2),i(3))
    print '(a,2i5/(10g12.5))','y-dir',i(1),i(3),scr3(i(1),:,i(3))
    print '(a,2i5/(10g12.5))','z-dir',i(1),i(2),scr3(i(1),i(2),:)
    !call end_mpi
    !stop
  else if (master) then
    print *,'test_divcurl: OK',err,eps
  end if

END
