
echo "" > stagger_par.f90
foreach f ( x y z ddx ddy ddz )
foreach o ( set neg add sub )
foreach d ( up dn up1 dn1 )
  cat >>stagger_par.f90 << XXX
!-----------------------------------------------------------------------
SUBROUTINE ${f}${d}_${o}_par (f, fp)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f, fp
  logical omp_in_parallel
 
  if (omp_in_parallel()) then
    call ${f}${d}_${o} (f, fp)
  else
    !\$omp parallel
    call ${f}${d}_${o} (f, fp)
    !\$omp end parallel
  end if
END

XXX
end
end
end
