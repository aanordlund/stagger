!!$Id: stagger_oslo.f,v 1.1 2008/05/23 14:35:44 bob Exp $
      subroutine xup_set(f,o)
      use vartype
      use cparam
      real(dp), dimension(mx,my,mz) :: f,o
      real(dp):: a,b,c
      integer(i4b):: i,j,k
      if (mx.eq.1) then
        do k=izs,ize
          do j=iys,iye
            do i=ixs,ixe
              o(i,j,k)=f(i,j,k)
            end do
          end do
        end do
        return
      endif

      c = 3.d0/256.d0
      b = -25.d0/256.d0
      a = .5d0-b-c

      do k=izs,ize
        do j=iys,iye
        if((mx-2 >= ixs).and.(mx-2 <= ixe)) o(mx-2,j,k) = (
     &                 a*(f(mx-2,j,k)+f(mx-1,j,k))
     &               + b*(f(mx-3,j,k)+f(mx  ,j,k))
     &               + c*(f(mx-4,j,k)+f(1   ,j,k)))
        if((mx-1 >= ixs).and.(mx-1 <= ixe)) o(mx-1,j,k) = (
     &                 a*(f(mx-1,j,k)+f(mx  ,j,k))
     &               + b*(f(mx-2,j,k)+f(1   ,j,k))
     &               + c*(f(mx-3,j,k)+f(2   ,j,k)))
        if((mx >= ixs).and.(mx <= ixe)) o(mx  ,j,k) = (
     &                 a*(f(mx  ,j,k)+f(1   ,j,k))
     &               + b*(f(mx-1,j,k)+f(2   ,j,k))
     &               + c*(f(mx-2,j,k)+f(3   ,j,k)))
        if((1 >= ixs).and.(1 <= ixe))o(1   ,j,k) = (
     &                 a*(f(1   ,j,k)+f(2   ,j,k))
     &               + b*(f(mx  ,j,k)+f(3   ,j,k))
     &               + c*(f(mx-1,j,k)+f(4   ,j,k)))
        if((2 >= ixs).and.(2 <= ixe))o(2   ,j,k) = (
     &                 a*(f(2   ,j,k)+f(3   ,j,k))
     &               + b*(f(1   ,j,k)+f(4   ,j,k))
     &               + c*(f(mx  ,j,k)+f(5   ,j,k)))
        end do
        do j=iys,iye
          do i=max(3,ixs),min(mx-3,ixe)
          o(i  ,j,k) = (
     &                  a*(f(i  ,j,k)+f(i+1 ,j,k))
     &                + b*(f(i-1,j,k)+f(i+2 ,j,k))
     &                + c*(f(i-2,j,k)+f(i+3 ,j,k)))
         end do
        end do
      end do

      end subroutine xup_set
      
! =======================================================================      
      
      subroutine yup_set(f,o)
      use vartype
      use cparam
      real(dp), dimension(mx,my,mz) :: f,o
      real(dp):: a,b,c
      integer(i4b):: i,j,k
!
!-----------------------------------------------------------------------
!
      if (my.eq.1) then
        do k=izs,ize
          do j=iys,iye
            do i=ixs,ixe
              o(i,j,k)=f(i,j,k)
            end do
          end do
        end do
        return
      endif

      c = 3.d0/256.d0
      b = -25.d0/256.d0
      a = .5d0-b-c

      do k=izs,ize
        do i=ixs,ixe
          if((my-2 >= iys).and.(my-2 <= iye))o(i,my-2,k) = (
     &                   a*(f(i,my-2,k)+f(i,my-1,k)) +
     &                   b*(f(i,my-3,k)+f(i,my  ,k)) + 
     &                   c*(f(i,my-4,k)+f(i,1   ,k)))
          if((my-1 >= iys).and.(my-1 <= iye))o(i,my-1,k) = (
     &                   a*(f(i,my-1,k)+f(i,my  ,k)) +
     &                   b*(f(i,my-2,k)+f(i,1   ,k)) + 
     &                   c*(f(i,my-3,k)+f(i,2   ,k)))
          if((my >= iys).and.(my <= iye))o(i,my  ,k) = (
     &                   a*(f(i,my  ,k)+f(i,1   ,k)) +
     &                   b*(f(i,my-1,k)+f(i,2   ,k)) + 
     &                   c*(f(i,my-2,k)+f(i,3   ,k)))
          if((1 >= iys).and.(1 <= iye))o(i,1   ,k) = (
     &                   a*(f(i,1   ,k)+f(i,2   ,k)) +
     &                   b*(f(i,my  ,k)+f(i,3   ,k)) + 
     &                   c*(f(i,my-1,k)+f(i,4   ,k)))
          if((2 >= iys).and.(2 <= iye))o(i,2   ,k) = (
     &                   a*(f(i,2   ,k)+f(i,3   ,k)) +
     &                   b*(f(i,1   ,k)+f(i,4   ,k)) + 
     &                   c*(f(i,my  ,k)+f(i,5   ,k)))
        end do
        do j=max(3,iys),min(my-3,iye)
          do i=ixs,ixe
            o(i,j  ,k) = (
     &                   a*(f(i,j   ,k)+f(i,j+1 ,k)) +
     &                   b*(f(i,j-1 ,k)+f(i,j+2 ,k)) + 
     &                   c*(f(i,j-2 ,k)+f(i,j+3 ,k)))
          end do
        end do
      end do
      end subroutine yup_set
      
      subroutine zup_set(f,o)
      use vartype
      use cparam
      use stagger
      
      real(dp), dimension(mx,my,mz) :: f,o
      integer(i4b):: i,j,k,l,kk
!
!-----------------------------------------------------------------------
!
      do k=izs,ize
         kk=k-2
         if (k.lt.4) kk=1
         if (k.gt.mz-3) kk=mz-5
         do j=iys,iye
            do i=ixs,ixe
               o(i,j,k) = 0.0d0
               do l=0,5
                  o(i,j,k) = o(i,j,k) + 
     &                      zupcoeff(k,l)*f(i,j,kk+l)
               end do
            end do
         end do
      end do
      end subroutine zup_set
      
!***********************************************************************

      subroutine xdn_set(f,o)
      use vartype
      use cparam
      real(dp), dimension(mx,my,mz) :: f,o
      real(dp):: a,b,c
      integer(i4b):: i,j,k

      if (mx.eq.1) then
        do k=izs,ize
          do j=iys,iye
            do i=ixs,ixe
              o(i,j,k)=f(i,j,k)
            end do
          end do
        end do
        return
      endif

      c = 3.d0/256.d0
      b = -25.d0/256.d0
      a = .5d0-b-c

      do k=izs,ize
      do j=iys,iye
        if((mx-1 >= ixs).and.(mx-1 <= ixe))o(mx-1,j,k) = (
     &                 a*(f(mx-2,j,k)+f(mx-1,j,k))
     &               + b*(f(mx-3,j,k)+f(mx  ,j,k))
     &               + c*(f(mx-4,j,k)+f(1   ,j,k)))
        if((mx >= ixs).and.(mx <= ixe))o(mx  ,j,k) = (
     &                 a*(f(mx-1,j,k)+f(mx  ,j,k))
     &               + b*(f(mx-2,j,k)+f(1   ,j,k))
     &               + c*(f(mx-3,j,k)+f(2   ,j,k)))
        if((1 >= ixs).and.(1 <= ixe))o(1  ,j,k) = (
     &                 a*(f(mx  ,j,k)+f(1   ,j,k))
     &               + b*(f(mx-1,j,k)+f(2   ,j,k))
     &               + c*(f(mx-2,j,k)+f(3   ,j,k)))
        if((2 >= ixs).and.(2 <= ixe))o(2  ,j,k) = (
     &                 a*(f(1   ,j,k)+f(2   ,j,k))
     &               + b*(f(mx  ,j,k)+f(3   ,j,k))
     &               + c*(f(mx-1,j,k)+f(4   ,j,k)))
        if((3 >= ixs).and.(3 <= ixe))o(3  ,j,k) = (
     &                 a*(f(2   ,j,k)+f(3   ,j,k))
     &               + b*(f(1   ,j,k)+f(4   ,j,k))
     &               + c*(f(mx  ,j,k)+f(5   ,j,k)))
      end do
      do j=iys,iye
        do i=max(4,ixs),min(mx-2,ixe)
          o(i  ,j,k) = (
     &                 a*(f(i-1 ,j,k)+f(i   ,j,k))
     &               + b*(f(i-2 ,j,k)+f(i+1 ,j,k))
     &               + c*(f(i-3 ,j,k)+f(i+2 ,j,k)))
        end do
      end do
      end do

!
      end subroutine xdn_set
      
!***********************************************************************

      subroutine ydn_set(f,o)
      use vartype
      use cparam
      real(dp), dimension(mx,my,mz) :: f,o
      real(dp):: a,b,c
      integer(i4b):: i,j,k
!
!-----------------------------------------------------------------------

      if (my.eq.1) then
        do k=izs,ize
          do j=iys,iye
            do i=ixs,ixe
              o(i,j,k)=f(i,j,k)
            end do
          end do
        end do
        return
      endif

      c = 3.d0/256.d0
      b = -25.d0/256.d0
      a = .5d0-b-c
      
      do k=izs,ize
        do i=ixs,ixe
          if((my-1 >= iys).and.(my-1 <= iye)) o(i,my-1,k) = (
     &                   a*(f(i,my-2,k)+f(i,my-1,k)) +
     &                   b*(f(i,my-3,k)+f(i,my  ,k)) + 
     &                   c*(f(i,my-4,k)+f(i,1   ,k)))
          if((my >= iys).and.(my <= iye)) o(i,my  ,k) = (
     &                   a*(f(i,my-1,k)+f(i,my  ,k)) +
     &                   b*(f(i,my-2,k)+f(i,1   ,k)) + 
     &                   c*(f(i,my-3,k)+f(i,2   ,k)))
          if((1 >= iys).and.(1 <= iye))o(i,1   ,k) = (
     &                   a*(f(i,my  ,k)+f(i,1   ,k)) +
     &                   b*(f(i,my-1,k)+f(i,2   ,k)) + 
     &                   c*(f(i,my-2,k)+f(i,3   ,k)))
          if((2 >= iys).and.(2 <= iye))o(i,2   ,k) = (
     &                   a*(f(i,1   ,k)+f(i,2   ,k)) +
     &                   b*(f(i,my  ,k)+f(i,3   ,k)) + 
     &                   c*(f(i,my-1,k)+f(i,4   ,k)))
          if((3 >= iys).and.(3 <= iye))o(i,3   ,k) = (
     &                   a*(f(i,2   ,k)+f(i,3   ,k)) +
     &                   b*(f(i,1   ,k)+f(i,4   ,k)) + 
     &                   c*(f(i,my  ,k)+f(i,5   ,k)))
        end do
        do j=max(4,iys),min(my-2,iye)
         do i=ixs,ixe
          o(i,j,k) = (
     &                   a*(f(i,j-1 ,k)+f(i,j   ,k)) +
     &                   b*(f(i,j-2 ,k)+f(i,j+1 ,k)) + 
     &                   c*(f(i,j-3 ,k)+f(i,j+2 ,k)))
         end do
        end do
      end do
!
      end subroutine ydn_set
      
!***********************************************************************

      subroutine zdn_set(f,o)
      use vartype
      use cparam
      use stagger
      real(dp), dimension(mx,my,mz) :: f,o
      
      integer(i4b) :: m
      integer(i4b):: i,j,k
!
!-----------------------------------------------------------------------

      m=mz
      do j=iys,iye
         do i=ixs,ixe
            if ((mz-1>=izs).and.(mz-1<=ize)) 
     &            o(i,j,m-1) = zdncoeff(m-1,0)*f(i,j,m-5) +
     &           zdncoeff(m-1,1)*f(i,j,m-4) +
     &           zdncoeff(m-1,2)*f(i,j,m-3) +
     &           zdncoeff(m-1,3)*f(i,j,m-2) +
     &           zdncoeff(m-1,4)*f(i,j,m-1) +
     &           zdncoeff(m-1,5)*f(i,j,m)
            if ((mz>=izs).and.(mz<=ize)) 
     &            o(i,j,m) = zdncoeff(m,0)*f(i,j,m-5) +
     &           zdncoeff(m,1)*f(i,j,m-4) +
     &           zdncoeff(m,2)*f(i,j,m-3) +
     &           zdncoeff(m,3)*f(i,j,m-2) +
     &           zdncoeff(m,4)*f(i,j,m-1) +
     &           zdncoeff(m,5)*f(i,j,m)
            if ((1>=izs).and.(1<=ize)) 
     &            o(i,j,1) = zdncoeff(1,0)*f(i,j,1) +
     &           zdncoeff(1,1)*f(i,j,2) +
     &           zdncoeff(1,2)*f(i,j,3) +
     &           zdncoeff(1,3)*f(i,j,4) +
     &           zdncoeff(1,4)*f(i,j,5) +
     &           zdncoeff(1,5)*f(i,j,6)
            if ((2>=izs).and.(2<=ize)) 
     &            o(i,j,2) = zdncoeff(2,0)*f(i,j,1) +
     &           zdncoeff(2,1)*f(i,j,2) +
     &           zdncoeff(2,2)*f(i,j,3) +
     &           zdncoeff(2,3)*f(i,j,4) +
     &           zdncoeff(2,4)*f(i,j,5) +
     &           zdncoeff(2,5)*f(i,j,6)
            if ((3>=izs).and.(3<=ize)) 
     &            o(i,j,3) = zdncoeff(3,0)*f(i,j,1) +
     &           zdncoeff(3,1)*f(i,j,2) +
     &           zdncoeff(3,2)*f(i,j,3) +
     &           zdncoeff(3,3)*f(i,j,4) +
     &           zdncoeff(3,4)*f(i,j,5) +
     &           zdncoeff(3,5)*f(i,j,6)
         end do
      end do
      do k=max(4,izs),min(m-2,ize)
        do j=iys,iye
           do i=ixs,ixe
            o(i,j,k) = zdncoeff(k,0)*f(i,j,k-3) +
     &           zdncoeff(k,1)*f(i,j,k-2) +
     &           zdncoeff(k,2)*f(i,j,k-1) +
     &           zdncoeff(k,3)*f(i,j,k) +
     &           zdncoeff(k,4)*f(i,j,k+1) +
     &           zdncoeff(k,5)*f(i,j,k+2)
           end do
        end do
      end do

      end subroutine zdn_set
      
!***********************************************************************

      subroutine ddxup_set(f,o)
      use vartype
      use cparam
      use cdata
      real(dp), dimension(mx,my,mz) :: f,o
      
      real(dp):: a,b,c
      integer(i4b):: i,j,k

      if (mx.eq.1) then
        do k=izs,ize
          do j=iys,iye
            do i=ixs,ixe
              o(i,j,k)=0.0d0
            end do
          end do
        end do
        return
      endif

      c = (-1.d0+(3.d0**5-3.d0)/(3.d0**3-3.d0))/(5.d0**5-5.d0-5.d0*
     & (3.d0**5-3))
      b = (-1.d0-120.d0*c)/24.d0
      a = (1.d0-3.d0*b-5.d0*c)/dx
      b = b/dx
      c = c/dx

      do k=izs,ize
      do j=iys,iye
        if((mx-2 >= ixs).and.(mx-2 <= ixe))o(mx-2,j,k) = (
     &                 a*(f(mx-1,j,k)-f(mx-2,j,k))
     &               + b*(f(mx  ,j,k)-f(mx-3,j,k))
     &               + c*(f(1   ,j,k)-f(mx-4,j,k)))
        if((mx-1 >= ixs).and.(mx-1 <= ixe))o(mx-1,j,k) = (
     &                 a*(f(mx  ,j,k)-f(mx-1,j,k))
     &               + b*(f(1   ,j,k)-f(mx-2,j,k))
     &               + c*(f(2   ,j,k)-f(mx-3,j,k)))
        if((mx >= ixs).and.(mx <= ixe))o(mx  ,j,k) = (
     &                 a*(f(1   ,j,k)-f(mx  ,j,k))
     &               + b*(f(2   ,j,k)-f(mx-1,j,k))
     &               + c*(f(3   ,j,k)-f(mx-2,j,k)))
        if((1 >= ixs).and.(1 <= ixe))o(1  ,j,k) = (
     &                 a*(f(2   ,j,k)-f(1   ,j,k))
     &               + b*(f(3   ,j,k)-f(mx  ,j,k))
     &               + c*(f(4   ,j,k)-f(mx-1,j,k)))
        if((2 >= ixs).and.(2 <= ixe))o(2  ,j,k) = (
     &                 a*(f(3   ,j,k)-f(2  ,j,k))
     &               + b*(f(4   ,j,k)-f(1  ,j,k))
     &               + c*(f(5   ,j,k)-f(mx ,j,k)))
      end do
      do j=iys,iye
        do i=max(3,ixs),min(mx-3,ixe)
          o(i  ,j,k) = (
     &                 a*(f(i+1 ,j,k)-f(i   ,j,k))
     &               + b*(f(i+2 ,j,k)-f(i-1 ,j,k))
     &               + c*(f(i+3 ,j,k)-f(i-2 ,j,k)))
        end do
      end do
      end do

      end subroutine ddxup_set
      
!***********************************************************************

      subroutine ddyup_set(f,o)
      use vartype
      use cparam
      use cdata
      real(dp), dimension(mx,my,mz) :: f,o
      real(dp):: a,b,c
      integer(i4b):: i,j,k

      if (my.eq.1) then
        do k=izs,ize
          do j=iys,iye
            do i=ixs,ixe
              o(i,j,k)=0.0d0
            end do
          end do
        end do
        return
      endif

      c = (-1.d0+(3.d0**5-3.d0)/(3.d0**3-3.d0))/(5.d0**5-5.d0-5.d0*
     & (3.d0**5-3))
      b = (-1.d0-120.d0*c)/24.d0
      a = (1.d0-3.d0*b-5.d0*c)/dy
      b = b/dy
      c = c/dy
 
      do k=izs,ize
        do i=ixs,ixe
          if((my-2 >= iys).and.(my-2 <= iye))o(i,my-2,k) = (
     &                   a*(f(i,my-1,k)-f(i,my-2,k)) +
     &                   b*(f(i,my  ,k)-f(i,my-3,k)) + 
     &                   c*(f(i,1   ,k)-f(i,my-4,k)))
          if((my-1 >= iys).and.(my-1 <= iye))o(i,my-1,k) = (
     &                   a*(f(i,my  ,k)-f(i,my-1,k)) +
     &                   b*(f(i,1   ,k)-f(i,my-2,k)) + 
     &                   c*(f(i,2   ,k)-f(i,my-3,k)))
          if((my >= iys).and.(my <= iye))o(i,my  ,k) = (
     &                   a*(f(i,1   ,k)-f(i,my  ,k)) +
     &                   b*(f(i,2   ,k)-f(i,my-1,k)) + 
     &                   c*(f(i,3   ,k)-f(i,my-2,k)))
          if((1 >= iys).and.(1 <= iye))o(i,1   ,k) = (
     &                   a*(f(i,2   ,k)-f(i,1   ,k)) +
     &                   b*(f(i,3   ,k)-f(i,my  ,k)) + 
     &                   c*(f(i,4   ,k)-f(i,my-1,k)))
          if((2 >= iys).and.(2 <= iye))o(i,2   ,k) = (
     &                   a*(f(i,3   ,k)-f(i,2   ,k)) +
     &                   b*(f(i,4   ,k)-f(i,1   ,k)) + 
     &                   c*(f(i,5   ,k)-f(i,my  ,k)))
        end do
        do j=max(3,iys),min(my-3,iye)
         do i=ixs,ixe
          o(i,j  ,k) = (
     &                   a*(f(i,j+1 ,k)-f(i,j   ,k)) +
     &                   b*(f(i,j+2 ,k)-f(i,j-1 ,k)) + 
     &                   c*(f(i,j+3 ,k)-f(i,j-2 ,k)))
         end do
        end do
      end do

      end subroutine ddyup_set
      
!***********************************************************************

      subroutine ddzup_set(f,o)
      use vartype
      use cparam
      use stagger
      real(dp), dimension(mx,my,mz) :: f,o
      integer(i4b):: i,j,k,l,kk

      do k=izs,ize
         kk=k-2
         if (k<4) kk=1
         if (k>mz-3) kk=mz-5
         do j=iys,iye
            do i=ixs,ixe
               o(i,j,k) = 0.0d0
               do l=0,5
                  o(i,j,k) = o(i,j,k) + 
     &             ddzupcoeff(k,l)*f(i,j,kk+l)
               end do            
            end do
         end do
      end do

      end subroutine ddzup_set
      
!***********************************************************************

      subroutine ddxdn_set(f,o)
      use vartype       
      use cparam
      use cdata
      real(dp), dimension(mx,my,mz) :: f,o
      real(dp):: a,b,c
      integer(i4b):: i,j,k
      if (mx.eq.1) then
        do k=izs,ize
          do j=iys,iye
            do i=ixs,ixe
              o(i,j,k)=0.0d0
            end do
          end do
        end do
        return
      endif

      c = (-1.d0+(3.d0**5-3.d0)/(3.d0**3-3.d0))/(5.d0**5-5.d0-5.d0*
     & (3.d0**5-3))
      b = (-1.d0-120.d0*c)/24.d0
      a = (1.d0-3.d0*b-5.d0*c)/dx
      b = b/dx
      c = c/dx
      do k=izs,ize
       do j=iys,iye
        if((mx-1 >= ixs).and.(mx-1 <= ixe))o(mx-1,j,k) = (
     &                 a*(f(mx-1,j,k)-f(mx-2,j,k))
     &               + b*(f(mx  ,j,k)-f(mx-3,j,k))
     &               + c*(f(1   ,j,k)-f(mx-4,j,k)))
        if((mx >= ixs).and.(mx <= ixe))o(mx  ,j,k) = (
     &                 a*(f(mx  ,j,k)-f(mx-1,j,k))
     &               + b*(f(1   ,j,k)-f(mx-2,j,k))
     &               + c*(f(2   ,j,k)-f(mx-3,j,k)))
        if((1 >= ixs).and.(1 <= ixe))o(1   ,j,k) = (
     &                 a*(f(1   ,j,k)-f(mx  ,j,k))
     &               + b*(f(2   ,j,k)-f(mx-1,j,k))
     &               + c*(f(3   ,j,k)-f(mx-2,j,k)))
        if((2 >= ixs).and.(2 <= ixe))o(2  ,j,k) = (
     &                 a*(f(2   ,j,k)-f(1   ,j,k))
     &               + b*(f(3   ,j,k)-f(mx  ,j,k))
     &               + c*(f(4   ,j,k)-f(mx-1,j,k)))
        if((3 >= ixs).and.(3 <= ixe))o(3  ,j,k) = (
     &                 a*(f(3   ,j,k)-f(2   ,j,k))
     &               + b*(f(4   ,j,k)-f(1   ,j,k))
     &               + c*(f(5   ,j,k)-f(mx  ,j,k)))
       end do
       do j=iys,iye
         do i=max(4,ixs),min(mx-2,ixe)
          o(i,j,k) = (
     &                 a*(f(i   ,j,k)-f(i-1  ,j,k))
     &               + b*(f(i+1 ,j,k)-f(i-2 ,j,k))
     &               + c*(f(i+2 ,j,k)-f(i-3 ,j,k)))
        end do
       end do
      end do

      end subroutine ddxdn_set
      
!***********************************************************************

      subroutine ddydn_set(f,o)
      use vartype
      use cparam
      use cdata
      real(dp), dimension(mx,my,mz) :: f,o
      real(dp):: a,b,c
      integer(i4b):: i,j,k
      
      if (my.eq.1) then
        do k=izs,ize
          do j=iys,iye
            do i=ixs,ixe
              o(i,j,k)=0.0d0
            end do
          end do
        end do
        return      
      endif

      c = (-1.d0+(3.d0**5-3.d0)/(3.d0**3-3.d0))/(5.d0**5-5.d0-5.d0*
     & (3.d0**5-3))
      b = (-1.d0-120.d0*c)/24.d0
      a = (1.d0-3.d0*b-5.d0*c)/dy
      b = b/dy
      c = c/dy

      do k=izs,ize
        do i=ixs,ixe
          if((my-1 >= iys).and.(my-1 <= iye))o(i,my-1,k) = (
     &                   a*(f(i,my-1,k)-f(i,my-2,k)) +
     &                   b*(f(i,my  ,k)-f(i,my-3,k)) + 
     &                   c*(f(i,1   ,k)-f(i,my-4,k)))
          if((my >= iys).and.(my <= iye))o(i,my  ,k) = (
     &                   a*(f(i,my  ,k)-f(i,my-1,k)) +
     &                   b*(f(i,1   ,k)-f(i,my-2,k)) + 
     &                   c*(f(i,2   ,k)-f(i,my-3,k)))
          if((1 >= iys).and.(1 <= iye))o(i,1  ,k) = (
     &                   a*(f(i,1   ,k)-f(i,my  ,k)) +
     &                   b*(f(i,2   ,k)-f(i,my-1,k)) + 
     &                   c*(f(i,3   ,k)-f(i,my-2,k)))
          if((2 >= iys).and.(2 <= iye))o(i,2   ,k) = (
     &                   a*(f(i,2   ,k)-f(i,1   ,k)) +
     &                   b*(f(i,3   ,k)-f(i,my  ,k)) + 
     &                   c*(f(i,4   ,k)-f(i,my-1,k)))
          if((3 >= iys).and.(3 <= iye))o(i,3   ,k) = (
     &                   a*(f(i,3   ,k)-f(i,2   ,k)) +
     &                   b*(f(i,4   ,k)-f(i,1   ,k)) + 
     &                   c*(f(i,5   ,k)-f(i,my  ,k)))
        end do
        do j=max(4,iys),min(my-2,iye)
         do i=ixs,ixe
          o(i,j   ,k) = (
     &                   a*(f(i,j   ,k)-f(i,j-1 ,k)) +
     &                   b*(f(i,j+1 ,k)-f(i,j-2 ,k)) + 
     &                   c*(f(i,j+2 ,k)-f(i,j-3 ,k)))
         end do
        end do
      end do

      end subroutine ddydn_set
      
!***********************************************************************

      subroutine ddzdn_set(f,o) 
      use vartype
      use cparam
      use stagger

      real(dp), dimension(mx,my,mz) :: f,o
      integer(i4b):: i,j,k,l,kk

      do k=izs,ize
         kk=k-3
         if (k.lt.4) kk=1
         if (k.gt.mz-3) kk=mz-5
         do j=iys,iye
            do i=ixs,ixe
               o(i,j,k) = 0.0d0
               do l=0,5
                 o(i,j,k) = o(i,j,k) +
     &                 ddzdncoeff(k,l)*f(i,j,kk+l)
              end do
           end do
        end do
      end do

      end subroutine ddzdn_set

!***********************************************************************

      subroutine xup1_set(f,o)
      use vartype
      use cparam
      real(dp), dimension(mx,my,mz) :: f,o
      
      real(dp):: a
      integer(i4b):: i,j,k
      if (mx.eq.1) then
        do k=izs,ize
          do j=iys,iye
            do i=ixs,ixe
              o(i,j,k)=f(i,j,k)
            end do
          end do
        end do
        return
      endif

      a = .5d0
      do k=izs,ize
        do j=iys,iye
          if((mx>=ixs).and.(mx<=ixe))o(mx  ,j,k) = (
     &                 a*(f(mx  ,j,k)+f(1   ,j,k)))
          end do
          do j=iys,iye
            do i=ixs,min(ixe,mx-1)
             o(i  ,j,k) = (
     &                  a*(f(i  ,j,k)+f(i+1 ,j,k)))
            end do
         end do
      end do

      end subroutine xup1_set
      
!***********************************************************************

      subroutine yup1_set(f,o)
      use vartype
      use cparam
      
      real(dp), dimension(mx,my,mz) :: f,o    
      real(dp):: a
      integer(i4b):: i,j,k

      a = .5d0
      if (my .eq. 1) then
        do k=izs,ize
          do j=iys,iye
            do i=ixs,ixe
              o(i,j,k)=f(i,j,k)
            end do
          end do
        end do
        return
      endif         

      do k=izs,ize
        do i=ixs,ixe
          if((my>=iys).and.(my<=iye))o(i,my  ,k) = (
     &                   a*(f(i,my  ,k)+f(i,1   ,k)))
        end do
        do j=iys,min(my-1,iye)
          do i=ixs,ixe
            o(i,j  ,k) = (
     &                   a*(f(i,j   ,k)+f(i,j+1 ,k)))
          end do
        end do
      end do

      end subroutine yup1_set
      
!***********************************************************************

      subroutine zup1_set(f,o)
      use vartype
      use cparam
      use stagger
      real(dp), dimension(mx,my,mz) :: f,o 
      integer(i4b):: i,j,k

      do j=iys,iye
         do i=ixs,ixe
           if((mz>=izs).and.(mz<=ize)) 
     &        o(i,j,mz ) = zup1coeff(mz,0) * 
     &           f(i,j,mz-1) + zup1coeff(mz,1) * f(i,j,mz)
         end do
      end do
      do k=izs,min(ize,mz-1)
        do j=iys,iye
          do i=ixs,ixe
               o(i,j,k) = zup1coeff(k,0) * f(i,j,k) + 
     &              zup1coeff(k,1) * f(i,j,k+1)
            end do
         end do
      end do

      end subroutine zup1_set
      
!***********************************************************************

      subroutine zup0_set(f,o)
      use vartype
      use cparam
      use stagger
      
      real(dp), dimension(mx,my,mz) :: f,o   
      integer(i4b):: i,j,k
      
      if((mz>=izs).and.(mz<=ize))then
        do j=iys,iye
          do i=ixs,ixe
            o(i,j,mz  ) = f(i,j,mz)
          end do
        end do
      endif
      do k=izs,min(ize,mz-1)
        do j=iys,iye
          do i=ixs,ixe
            o(i,j,k) = zup1coeff(k,0) * f(i,j,k) + 
     &              zup1coeff(k,1) * f(i,j,k+1)
          end do
        end do
      end do

      end subroutine zup0_set
      
!***********************************************************************

      subroutine xdn1_set(f,o)
      use vartype
      use cparam
       
      real(dp), dimension(mx,my,mz) :: f,o  
      real(dp):: a
      integer(i4b):: i,j,k

      if (mx.eq.1) then
        do k=izs,ize
          do j=iys,iye
            do i=ixs,ixe
              o(i,j,k)=f(i,j,k)
            end do
          end do
        end do
        return
      endif

      a = .5d0

      do k=izs,ize
        do j=iys,iye
          if((1>=ixs).and.(1<=ixe))o(1  ,j,k) = (
     &                 a*(f(mx  ,j,k)+f(1   ,j,k)))
          do i=max(2,ixs),ixe
            o(i,j,k) = (
     &                  a*(f(i-1  ,j,k)+f(i ,j,k)))
          end do
        end do
      end do

      end subroutine xdn1_set
      
!***********************************************************************

      subroutine ydn1_set(f,o)
      use vartype
      use cparam
      
      real(dp), dimension(mx,my,mz) :: f,o   
      
      real(dp):: a
      integer(i4b):: i,j,k
      
      a = .5d0
      if (my .eq. 1) then
        do k=izs,ize
          do j=iys,iye
            do i=ixs,ixe
              o(i,j,k)=f(i,j,k)
            end do
          end do
        end do
        return
      endif         

      do k=izs,ize
        do i=ixs,ixe
          if((1>=iys).and.(1<=iye))o(i,1   ,k) = (
     &                   a*(f(i,my  ,k)+f(i,1   ,k)))
          do j=max(2,iys),iye
            o(i,j,k) = (
     &                   a*(f(i,j-1   ,k)+f(i,j ,k)))
          end do
        end do
      end do

      end subroutine ydn1_set
      
!***********************************************************************

      subroutine zdn1_set(f,o)
      use vartype
      use cparam
      use stagger
      
      real(dp), dimension(mx,my,mz) :: f,o
      integer(i4b):: i,j,k
      
      do j=iys,iye
        do i=ixs,ixe
           if((1>=izs).and.(1<=ize))
     &        o(i,j,1) = zdn1coeff(1,0) * f(i,j,1) + 
     &          zdn1coeff(1,1) * f(i,j,2)
        end do
      end do
      do k=max(2,izs),ize
        do j=iys,iye
          do i=ixs,ixe
            o(i,j,k) = zdn1coeff(k,0) * f(i,j,k-1) + 
     &            zdn1coeff(k,1) * f(i,j,k)
          end do
        end do
      end do

      end subroutine zdn1_set
      
!***********************************************************************

      subroutine zdn0_set(f,o)
      use vartype
      use cparam
      use stagger
      
      real(dp), dimension(mx,my,mz) :: f,o
      integer(i4b):: i,j,k

      do j=iys,iye
        do i=ixs,ixe
          if((1>=izs).and.(1<=ize))
     &      o(i,j,1) = zdn1coeff(1,0) * f(i,j,1) + 
     &          zdn1coeff(1,1) * f(i,j,2)
        end do
      end do
      do k=max(2,izs),ize
        do j=iys,iye
          do i=ixs,ixe
             o(i,j,k) = zdn1coeff(k,0) * f(i,j,k-1) + 
     &            zdn1coeff(k,1) * f(i,j,k)
          end do
       end do
      end do

      end subroutine zdn0_set
      
!***********************************************************************

      subroutine ddxup1_set(f,o)
      use vartype
      use cparam
      use cdata
      real(dp), dimension(mx,my,mz) :: f,o 
      
      real(dp):: a
      integer(i4b):: i,j,k

      if (mx.eq.1) then
        do k=izs,ize
          do j=iys,iye
            do i=ixs,ixe
              o(i,j,k)=0.0d0
            end do
          end do
        end do
        return
      endif

      a = 1.d0/dx

      do k=izs,ize
        do j=iys,iye
          if((mx>=ixs).and.(mx<=ixe))o(mx  ,j,k) = (
     &                 a*(f(1   ,j,k)-f(mx  ,j,k)))
          do i=ixs,min(ixe,mx-1)
            o(i  ,j,k) = (
     &                 a*(f(i+1 ,j,k)-f(i   ,j,k)))
          end do
        end do
      end do

      end subroutine ddxup1_set
      
!***********************************************************************

      subroutine ddyup1_set(f,o)
      use vartype       
      use cparam
      use cdata
      real(dp), dimension(mx,my,mz) :: f,o    
      
      real(dp):: a
      integer(i4b):: i,j,k

      if (my.eq.1) then
        do k=izs,ize
          do j=iys,iye
            do i=ixs,ixe
              o(i,j,k)=0.0d0
            end do
          end do
        end do
        return
      endif

      a = 1.d0/dy
       
      do k=izs,ize
        do i=ixs,ixe
          if((my>=iys).and.(my<=iye))o(i,my  ,k) = (
     &                 a*(f(i,1   ,k)-f(i,my  ,k)))
          do j=iys,min(iye,my-1)
            o(i,j  ,k) = (
     &                 a*(f(i,j+1 ,k)-f(i,j   ,k)))
          end do
        end do
      end do

      end subroutine ddyup1_set
      
!***********************************************************************

      subroutine ddzup1_set(f,o)
      use vartype
      use cparam
      use cdata
      real(dp), dimension(mx,my,mz) :: f,o    
      
      integer(i4b):: i,j,k
      do k=izs,min(ize,mz-1)
        do j=iys,iye
          do i=ixs,ixe
            o(i,j,k) = (
     &                   (f(i,j,k+1 )-f(i,j,k   ))/dez(k))
         end do
       end do
      end do

      if((mz>=izs).and.(mz<=ize))then
        do j=iys,iye
          do i=ixs,ixe
            o(i,j,mz) = 2*((f(i,j,mz)-f(i,j,mz-1))/dez(mz-1))- 
     &           (f(i,j,mz-1)-f(i,j,mz-2))/dez(mz-2)
          end do
        end do      
      endif

      end subroutine ddzup1_set
      
!***********************************************************************

      subroutine ddxdn1_set(f,o)
      use vartype
      use cparam
      use cdata
      real(dp), dimension(mx,my,mz) :: f,o 
      
      real(dp):: a
      integer(i4b):: i,j,k

      if (mx.eq.1) then
        do k=izs,ize
          do j=iys,iye
            do i=ixs,ixe
              o(i,j,k)=0.0d0
            end do
          end do
        end do
        return
      endif

      a = 1.d0/dx

      do k=izs,ize
        do j=iys,iye
          if((1>=ixs).and.(1<=ixe))o(1   ,j,k) = (
     &                 a*(f(1   ,j,k)-f(mx  ,j,k)))
          do i=max(2,ixs),ixe
            o(i,j,k) = (
     &                 a*(f(i ,j,k)-f(i-1 ,j,k)))
          end do
        end do
      end do

      end subroutine ddxdn1_set
      
!***********************************************************************

      subroutine ddydn1_set(f,o)
      use vartype
      use cparam
      use cdata
      real(dp), dimension(mx,my,mz) :: f,o  
      
      real(dp):: a
      integer(i4b):: i,j,k
      if (my.eq.1) then
        do k=izs,ize
          do j=iys,iye
            do i=ixs,ixe
              o(i,j,k)=0.0d0
            end do
          end do
        end do
        return      
      endif

      a = 1.d0/dy     
      
      do k=izs,ize
        do i=ixs,ixe
          if((1>=iys).and.(1<=iye))o(i,1   ,k) = (
     &                   a*(f(i,1   ,k)-f(i,my  ,k)))
        end do
        do j=max(2,iys),iye
          do i=ixs,ixe
            o(i,j,k) = (
     &                   a*(f(i,j ,k)-f(i,j-1 ,k)))
          end do
        end do
      end do

      end subroutine ddydn1_set
      
!***********************************************************************

      subroutine ddzdn1_set(f,o)
      use vartype
      use cparam
      use cdata
      real(dp), dimension(mx,my,mz) :: f,o 
      
      integer(i4b) ::i,j,k

      do k=max(2,izs),ize
        do j=iys,iye
          do i=ixs,ixe
            o(i,j,k) = (
     &                   (f(i,j,k )-f(i,j,k-1 ))/dcz(k-1))
          end do
        end do
      end do
      do j=iys,iye
        do i=ixs,ixe
          if((1>=izs).and.(1<=ize))o(i,j,1) = 
     &                  2*o(i,j,2) - o(i,j,3)
        end do
      end do      
      end subroutine ddzdn1_set
      
!-----------------------------------------------------------------------

      subroutine difx1_set(f,o)
      use vartype
      use cparam
      real(dp), dimension(mx,my,mz) :: f,o 
      
      if (mx .eq. 1) then
        do k=izs,ize
          do j=iys,iye
            do i=ixs,ixe
              o(i,j,k)=0.0
            end do
          end do
        end do
        return
      endif
      do k=izs,ize
        do j=iys,iye
          if(ixe == mx)o(mx,j,k) = 
     &                 f(mx,j,k) - f(1,j,k)
          do i=ixs,min(ixe,mx-1)
            o(i,j,k) = f(i,j,k) - f(i+1,j,k)
          end do
        end do
      end do
      end subroutine difx1_set
      
!-----------------------------------------------------------------------

      subroutine dify1_set(f,o)
      use vartype
      use cparam
      real(dp), dimension(mx,my,mz) :: f,o     
      
!  Finite difference one zone left to two zones right.

      if (my .eq. 1) then
        do k=izs,ize
          do j=iys,iye
            do i=ixs,ixe
              o(i,j,k)=0.0d0
            end do
          end do
        end do
        return
      endif
      
      do k=izs,ize
        do i=ixs,ixe
          if(iye == my)o(i,my,k) = 
     &                 f(i,my,k) - f(i,1,k)
          do j=iys,min(iye,my-1)
            o(i,j,k) = f(i,j,k) - f(i,j+1,k)
          end do
        end do
      end do
      end subroutine dify1_set
      
!-----------------------------------------------------------------------

      subroutine difz1_set(f,o)
      use vartype
      use cparam
      use cwork
      use cdata
      
      real(dp), dimension(mx,my,mz) :: f,o  
      
      if (mz .eq. 1) then
        do k=izs,ize
          do j=iys,iye
            do i=ixs,ixe
              o(i,j,k)=0.0d0
            end do
          end do
        end do
        return
      endif
      
      call ddzdn1_set(f,scrs)
      do k=max(izs,2),ize
        do j=iys,iye
          do i=ixs,ixe
            o(i,j,k)=-scrs(i,j,k)*dcz(k-1)
          end do
        end do
      end do
      do j=iys,iye
        do i=ixs,ixe
          if((1>=izs).and.(1<=ize))o(i,j,1) = o(i,j,2)
        end do
      end do
      end subroutine difz1_set
      
!-----------------------------------------------------------------------

      subroutine init_stagger ()
      
      end subroutine init_stagger
      
!-----------------------------------------------------------------------

      subroutine init_derivs ()
      
      end subroutine init_derivs
      
!***********************************************************************

      subroutine max3_set(f,o)
      use vartype
      use cparam
      use cwork
!
      real(dp), dimension(mx,my,mz) :: f,o
      
      do k=izs,ize
        do j=iys,iye
           jp1=j+1
           jm1=j-1
           if (jp1 > my) jp1=jp1-my
           if (jm1 < 1 ) jm1=jm1+my
           do i=ixs,ixe
             scrs(i,j,k)=
     .           max(max(f(i,jm1,k),f(i,j,k)),f(i,jp1,k))
!             scrs(i,j,k)=maxval(f(i,jm1:jp1,k))
          end do
        end do
      end do

      if(parx) call omp_barrier

      do k=izs,ize
        do j=iys,iye
           do i=ixs,ixe
              ip1=i+1
              im1=i-1
              if (ip1 > mx) ip1=ip1-mx
              if (im1 < 1 ) im1=im1+mx
              scrs2(i,j,k)=
     .           max(max(scrs(im1,j,k),scrs(i,j,k)),scrs(ip1,j,k))
!              scrs2(i,j,k)=maxval(scrs(im1:ip1,j,k))
          end do
        end do
      end do

      if(parz) call omp_barrier

      do k=izs,ize
        kp1=k+1
        km1=k-1
        if (kp1 > mz) kp1=mz
        if (km1 < 1 ) km1=1
        do j=iys,iye
          do i=ixs,ixe
             o(i,j,k)=
     .           max(max(scrs2(i,j,km1),scrs2(i,j,k)),scrs2(i,j,kp1))
!             o(i,j,k)=maxval(scrs2(i,j,km1:kp1))
          end do
        end do
      end do

      end subroutine max3_set
      
!-----------------------------------------------------------------------

      subroutine smooth3_set(f,o)
      use vartype
      use cparam
      use cwork
!
      real(dp), dimension(mx,my,mz) :: f,o

      do k=izs,ize
        do j=iys,iye
          jm1=j-1
          jp1=j+1
          if(jm1.le.0) jm1=jm1+my
          if(jp1.gt.my) jp1=jp1-my
          do i=ixs,ixe
            scrs(i,j,k)=(1.d0/3.d0)*(f(i,jm1,k)+f(i,j,k)+f(i,jp1,k))
          end do
        end do
      end do

      if(parx) call omp_barrier

      do k=izs,ize
        do j=iys,iye
          do i=ixs,ixe
            im1=i-1
            ip1=i+1
            if(im1.le.0) im1=im1+mx
            if(ip1.gt.mx) ip1=ip1-mx
            scrs2(i,j,k)=(1.d0/3.d0)*(scrs(im1,j,k)+scrs(i,j,k)+
     &                               scrs(ip1,j,k))
          end do
        end do
      end do

      if(parz) call omp_barrier

      do k=izs,ize
        km1=k-1
        kp1=k+1
        if(km1.le.0) km1=1    ! double weight to boundary points
        if(kp1.gt.mz) kp1=mz
        do j=iys,iye
          do i=ixs,ixe
            o(i,j,k)=(1.d0/3.d0)*(scrs2(i,j,km1)+
     &       scrs2(i,j,k)+scrs2(i,j,kp1))
          end do
        end do
      end do

      end subroutine smooth3_set

!-----------------------------------------------------------------------

      subroutine difx2_set(f,o)
      
      use vartype
      use cparam
      
      implicit none
      real(dp), dimension(mx,my,mz) :: f,o 
      integer(i4b) :: i,j,k,ip1,im1
      
      if (mx .eq. 1) then
        do k=izs,ize
          do j=iys,iye
            do i=ixs,ixe
              o(i,j,k)=0.0d0
            end do
          end do
        end do
        return
      endif

      do k=izs,ize
         do j=iys,iye
            do i=ixs,ixe
               ip1=i+1
               im1=i-1
               if (ip1 > mx ) ip1=ip1-mx
               if (im1 < 1  ) im1=im1+mx
               o(i,j,k) = f(im1,j,k) - f(ip1,j,k) 
            end do
         end do
      end do

      end subroutine difx2_set
      
!-----------------------------------------------------------------------

      subroutine dify2_set(f,o)
      
      use vartype
      use cparam
      use cparam
      
      implicit none
      real(dp), dimension(mx,my,mz) :: f,o 
      integer(i4b) :: i,j,k,jm1,jp1
      
      if (my .eq. 1) then
        do k=izs,ize
          do j=iys,iye
            do i=ixs,ixe
              o(i,j,k)=0.0d0
            end do
          end do
        end do
        return
      endif

      do k=izs,ize
         do j=iys,iye
            jp1=j+1
            jm1=j-1
            if (jp1 > my ) jp1=jp1-my
            if (jm1 < 1  ) jm1=jm1+my
            do i=ixs,ixe
               o(i,j,k) = f(i,jm1,k) - f(i,jp1,k) 
            end do
         end do
      end do

      end subroutine dify2_set
      
!-----------------------------------------------------------------------

      subroutine difz2_set(f,o)
      
      use vartype
      use cparam
      
      implicit none
      real(dp), dimension(mx,my,mz) :: f,o  
      integer(i4b) :: i,j,k,km1,kp1
      
      if (mz .eq. 1) then
        do k=izs,ize
          do j=iys,iye
            do i=ixs,ixe
              o(i,j,k)=0.0d0
            end do
          end do
        end do
        return
      endif

      do k=izs,ize
         kp1=k+1
         km1=k-1
         if (kp1 > mz ) kp1=mz
         if (km1 < 1  ) km1=1
         do j=iys,iye
            do i=ixs,ixe
               o(i,j,k) = f(i,j,km1) - f(i,j,kp1) 
            end do
         end do
      end do
      
      end subroutine difz2_set
      
!***********************************************************************

      subroutine calc_stagger_coeffs
!
! Calculates the coefficients for z shifting and differentiating.
! Also initialises the dcz and dez arrays
!
! zh is an array of the positions of the cell centres and edges above the
!    base. zh(1) is 0 (cell edge 1), zh(2) is cell centre 1...
!
      use vartype
      use cparam
      use cdata
      use stagger

      real(dp), dimension(0:5):: a
      integer(i4b):: k,j
      integer(i4b), dimension(4), save :: iordl,iordu,dordl,dordu
      data iordu /1,3,5,5/
      data iordl /1,3,5,5/
      data dordu /2,3,5,5/
      data dordl /2,3,5,5/
!      data iordu /5,5,5,5/
!      data iordl /5,5,5,5/
!      data dordu /5,5,5,5/
!      data dordl /5,5,5,5/
      
      call omp_barrier      
!$omp single

      do k=1,3
         do j=0,5
            a(j) = zh(1+2*j) - zh(k*2)
         end do
         call calc_stagger_inv(a, iordl(k+1), 0, 0, zupcoeff(k,:))
         call calc_stagger_inv(a, dordl(k+1) , 1, 0, ddzupcoeff(k,:))
         do j=0,5
            a(j) = zh(2+2*j) - zh(k*2-1)
         end do
         call calc_stagger_inv(a, iordl(k), 0, 0, zdncoeff(k,:))
         call calc_stagger_inv(a, dordl(k), 1, 0, ddzdncoeff(k,:))
      end do
      do k=4,mz-3
         do j=0,5
            a(j) = zh(2*(k-3+j)) - zh(2*k-1)
         end do
         call calc_stagger_inv(a, 5, 0, 0, zdncoeff(k,:))
         call calc_stagger_inv(a, 5, 1, 0, ddzdncoeff(k,:))
         do j=0,5
            a(j) = zh(2*(k-2+j)-1) - zh(2*k)
         end do
         call calc_stagger_inv(a, 5, 0, 0, zupcoeff(k,:))
         call calc_stagger_inv(a, 5, 1, 0, ddzupcoeff(k,:))
      end do
      do k=mz-2,mz
         do j=0,iordu(mz-k+1)
            a(j) = zh(2*(mz+j-iordu(mz-k+1))-1) - zh(k*2)
         end do
         do j=iordu(mz-k+1)+1,5
           a(j)=0.d0
         end do
         call calc_stagger_inv(a, iordu(mz-k+1), 0,
     $        5-iordu(mz-k+1), zupcoeff(k,:))
         do j=0,dordu(mz-k+1)
            a(j) = zh(2*(mz+j-dordu(mz-k+1))-1) - zh(k*2)
         end do
         do j=dordu(mz-k+1)+1,5
           a(j)=0.d0
         end do
         call calc_stagger_inv(a, dordu(mz-k+1), 1,
     $        5-dordu(mz-k+1), ddzupcoeff(k,:))
         do j=0,iordu(mz-k+2)
            a(j) = zh(2*(mz+j-iordu(mz-k+2))) - zh(k*2-1)
         end do
         do j=iordu(mz-k+2)+1,5
           a(j)=0.d0
         end do
         call calc_stagger_inv(a, iordu(mz-k+2), 0,
     $        5-iordu(mz-k+2), zdncoeff(k,:))
         do j=0,dordu(mz-k+2)
            a(j) = zh(2*(mz+j-dordu(mz-k+2))) - zh(k*2-1)
         end do
         do j=dordu(mz-k+2)+1,5
           a(j)=0.d0
         end do
         call calc_stagger_inv(a, dordu(mz-k+2), 1,
     $        5-dordu(mz-k+2), ddzdncoeff(k,:))
      end do

      zup1coeff(1,0) = (zh(3)-zh(2))/(zh(3)-zh(1))
      zup1coeff(1,1) = (zh(1)-zh(2))/(zh(1)-zh(3))
      zdn1coeff(1,0) = (zh(4)-zh(1))/(zh(4)-zh(2))
      zdn1coeff(1,1) = (zh(2)-zh(1))/(zh(2)-zh(4))
      do k=2,mz-1
        zup1coeff(k,0) = (zh(2*k+1)-zh(2*k))/(zh(2*k+1)-zh(2*k-1))
        zup1coeff(k,1) = (zh(2*k-1)-zh(2*k))/(zh(2*k-1)-zh(2*k+1))
        zdn1coeff(k,0) = (zh(2*k)-zh(2*k-1))/(zh(2*k)-zh(2*k-2))
        zdn1coeff(k,1) = (zh(2*k-2)-zh(2*k-1))/(zh(2*k-2)-zh(2*k))
      end do
      zup1coeff(mz,0) = (zh(2*mz-1)-zh(2*mz))/
     $     (zh(2*mz-1)-zh(2*mz-3))
      zup1coeff(mz,1) = (zh(2*mz-3)-zh(2*mz))/
     $     (zh(2*mz-3)-zh(2*mz-1))
      zdn1coeff(mz,0) = (zh(2*mz)-zh(2*mz-1))/
     $     (zh(2*mz)-zh(2*mz-2))
      zdn1coeff(mz,1) = (zh(2*mz-2)-zh(2*mz-1))/
     $     (zh(2*mz-2)-zh(2*mz))

      do k=2,mz-1
         dcz(k) = zh(2*k) - zh(2*k-2)
         dez(k) = zh(2*k+1) - zh(2*k-1)
      end do
      dcz(1) = dcz(2)
      dez(1) = zh(3) - zh(1)
      dcz(mz) = zh(mz) - zh(mz-2)
      dez(mz) = dez(mz-1)
      do k=1,mz
        dcz3d(:,:,k) = dcz(k)
      enddo
!$omp end single
      end subroutine calc_stagger_coeffs

      subroutine calc_stagger_inv (x, n, y, o, result)
      use vartype
!
! Calculate the inverse of the Vandermonde matrix
!
      real(dp), dimension(0:5):: x, result
      integer(i4b):: n, y, o

      real(dp), dimension(0:5):: c, b
      real(dp):: t
      integer(i4b):: i,j

      do i=0,n
         c(i) = 0.0d0
      end do
      c(n) = -x(0)
      do i=1,n
         do j=n-i,n-1
            c(j) = c(j) - x(i)*c(j+1)
         end do
         c(n) = c(n) - x(i)
      end do
      do i=0,5
        result(i)=0.0d0
      end do
      do i=0,n
         t = 1.0d0
         b(n) = 1.0d0
         do j=n,1,-1
            b(j-1) = c(j) + x(i)*b(j)
            t = x(i)*t + b(j-1)
         end do
         result(i+o) = b(y)/t
      end do
      end subroutine calc_stagger_inv

