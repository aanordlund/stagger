! $Id: scale_mesh_do.f90,v 1.4 2004/06/24 15:38:21 aake Exp $

SUBROUTINE scale_ds (f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
  integer ix, iy, iz

!$omp parallel do private(ix,iy,iz)
  do iz=1,mz
  do iy=1,my
  do ix=1,mx
    f(ix,iy,iz) = max(dxm(ix),dym(iy),dzm(iz))*f(ix,iy,iz)
  end do
  end do
  end do
END SUBROUTINE

SUBROUTINE scale_ds1 (f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
  integer ix, iy, iz

!$omp parallel do private(ix,iy,iz)
  do iz=1,mz
  do iy=1,my
  do ix=1,mx
    f(ix,iy,iz) = f(ix,iy,iz)*(1./min(dxm(ix),dym(iy),dzm(iz)))
  end do
  end do
  end do
END SUBROUTINE

SUBROUTINE scale_dx (f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
  integer ix, iy, iz

!$omp parallel do private(ix,iy,iz)
  do iz=1,mz
  do iy=1,my
  do ix=1,mx
    f(ix,iy,iz) = dxmdn(ix)*f(ix,iy,iz)
  end do
  end do
  end do
END SUBROUTINE

SUBROUTINE scale_dy (f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
  integer iy, iz

!$omp parallel do private(iy,iz)
  do iz=1,mz
  do iy=1,my
    f(:,iy,iz) = dymdn(iy)*f(:,iy,iz)
  end do
  end do
END SUBROUTINE

SUBROUTINE scale_dz (f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
  integer iz

!$omp parallel do private(iz)
  do iz=1,mz
    f(:,:,iz) = dzmdn(iz)*f(:,:,iz)
  end do
END SUBROUTINE

SUBROUTINE scale_dxy (f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
  integer ix, iy, iz

!$omp parallel do private(ix,iy,iz)
  do iz=1,mz
  do iy=1,my
  do ix=1,mx
    f(ix,iy,iz) = 0.5*(dxmdn(ix)+dymdn(iy))*f(ix,iy,iz)
  end do
  end do
  end do
END SUBROUTINE

SUBROUTINE scale_dyz (f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
  integer ix, iy, iz

!$omp parallel do private(ix,iy,iz)
  do iz=1,mz
  do iy=1,my
  do ix=1,mx
    f(ix,iy,iz) = 0.5*(dymdn(iy)+dzmdn(iz))*f(ix,iy,iz)
  end do
  end do
  end do
END SUBROUTINE

SUBROUTINE scale_dzx (f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
  integer ix, iy, iz

!$omp parallel do private(ix,iy,iz)
  do iz=1,mz
  do iy=1,my
  do ix=1,mx
    f(ix,iy,iz) = 0.5*(dzmdn(iz)+dxmdn(ix))*f(ix,iy,iz)
  end do
  end do
  end do
END SUBROUTINE
