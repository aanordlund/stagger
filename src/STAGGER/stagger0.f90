MODULE stagger0
CONTAINS
! $Id: stagger.pro,v 1.1 2001/12/11 08:27:38 aake Exp &
!;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
!
!  6th order staggered derivatives, 5th order interpolation
!
!;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
FUNCTION xup0 (f)
!
!  f is centered on (i-.5,j,k), fifth order stagger
!
USE params
real, dimension(mx,my,mz):: f, xup0
!hpf$ distribute(*,*,block):: f, xup0
!-----------------------------------------------------------------------
  c = 3./256.
  b = -25./256.
  a = .5-b-c
  xup0 = a*(f + cshift(f,dim=1,shift=1)) &
      + b*(cshift(f,dim=1,shift=-1) + cshift(f,dim=1,shift=2)) &
      + c*(cshift(f,dim=1,shift=-2) + cshift(f,dim=1,shift=3))
END FUNCTION

FUNCTION xup10 (f)
!
!  f is centered on (i-.5,j,k), fifth order stagger
!
USE params
real, dimension(mx,my,mz):: f, xup10
!hpf$ distribute(*,*,block):: f, xup10
!-----------------------------------------------------------------------
  a = .5
  xup10 = a*(f + cshift(f,dim=1,shift=1))
END FUNCTION

!;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
FUNCTION yup0 (f)
!
!  f is centered on (i,j-.5,k)
!
USE params
real, dimension(mx,my,mz):: f, yup0
!hpf$ distribute(*,*,block):: f, yup0
!-----------------------------------------------------------------------
  c = 3./256.
  b = -25./256.
  a = .5-b-c
  yup0 = a*(f + cshift(f,dim=2,shift=1)) &
      + b*(cshift(f,dim=2,shift=-1) + cshift(f,dim=2,shift=2)) &
      + c*(cshift(f,dim=2,shift=-2) + cshift(f,dim=2,shift=3))
END FUNCTION
FUNCTION yup10 (f)
!
!  f is centered on (i,j-.5,k)
!
USE params
real, dimension(mx,my,mz):: f, yup10
!hpf$ distribute(*,*,block):: f, yup10
!-----------------------------------------------------------------------
  a = .5
  yup10 = a*(f + cshift(f,dim=2,shift=1))
END FUNCTION

!;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
FUNCTION zup0 (f)
!
!  f is centered on (i,j,k-.5)
!
USE params
real, dimension(mx,my,mz):: f, zup0
!hpf$ distribute(*,*,block):: f, zup0
!-----------------------------------------------------------------------
  c = 3./256.
  b = -25./256.
  a = .5-b-c
  zup0 = a*(f + cshift(f,dim=3,shift=1)) &
      + b*(cshift(f,dim=3,shift=-1) + cshift(f,dim=3,shift=2)) &
      + c*(cshift(f,dim=3,shift=-2) + cshift(f,dim=3,shift=3))
END FUNCTION
FUNCTION zup10 (f)
!
!  f is centered on (i,j,k-.5)
!
USE params
real, dimension(mx,my,mz):: f, zup10
!hpf$ distribute(*,*,block):: f, zup10
!-----------------------------------------------------------------------
  a = .5
  zup10 = a*(f + cshift(f,dim=3,shift=1))
END FUNCTION

!;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
FUNCTION xdn0 (f)
!
!  f is centered on (i-.5,j,k)
!
USE params
real, dimension(mx,my,mz):: f, xdn0
!hpf$ distribute(*,*,block):: f, xdn
!-----------------------------------------------------------------------
  c = 3./256.
  b = -25./256.
  a = .5-b-c
  xdn0 = a*(f + cshift(f,dim=1,shift=-1)) &
      + b*(cshift(f,dim=1,shift=1) + cshift(f,dim=1,shift=-2)) &
      + c*(cshift(f,dim=1,shift=2) + cshift(f,dim=1,shift=-3))
END FUNCTION
FUNCTION xdn10 (f)
!
!  f is centered on (i-.5,j,k)
!
USE params
real, dimension(mx,my,mz):: f, xdn10
!hpf$ distribute(*,*,block):: f, xdn10
!-----------------------------------------------------------------------
  a = .5
  xdn10 = a*(f + cshift(f,dim=1,shift=-1))
END FUNCTION

!;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
FUNCTION ydn0 (f)
!
!  f is centered on (i,j,k-.5)
!
USE params
real, dimension(mx,my,mz):: f, ydn0
!hpf$ distribute(*,*,block):: f, ydn
!-----------------------------------------------------------------------
  c = 3./256.
  b = -25./256.
  a = .5-b-c
  ydn0 = a*(f + cshift(f,dim=2,shift=-1)) &
      + b*(cshift(f,dim=2,shift=1) + cshift(f,dim=2,shift=-2)) &
      + c*(cshift(f,dim=2,shift=2) + cshift(f,dim=2,shift=-3))
END FUNCTION
FUNCTION ydn10 (f)
!
!  f is centered on (i,j,k-.5)
!
USE params
real, dimension(mx,my,mz):: f, ydn10
!hpf$ distribute(*,*,block):: f, ydn10
!-----------------------------------------------------------------------
  a = .5
  ydn10 = a*(f + cshift(f,dim=2,shift=-1))
END FUNCTION

!;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
FUNCTION zdn0 (f)
!
!  f is centered on (i,j,k-.5)
!
USE params
real, dimension(mx,my,mz):: f, zdn0
!hpf$ distribute(*,*,block):: f, zdn
!-----------------------------------------------------------------------
  c = 3./256.
  b = -25./256.
  a = .5-b-c
  zdn0 = a*(f + cshift(f,dim=3,shift=-1)) &
      + b*(cshift(f,dim=3,shift=1) + cshift(f,dim=3,shift=-2)) &
      + c*(cshift(f,dim=3,shift=2) + cshift(f,dim=3,shift=-3))
END FUNCTION
FUNCTION zdn10 (f)
!
!  f is centered on (i,j,k-.5)
!
USE params
real, dimension(mx,my,mz):: f, zdn10
!hpf$ distribute(*,*,block):: f, zdn10
!-----------------------------------------------------------------------
  a = .5
  zdn10 = a*(f + cshift(f,dim=3,shift=-1))
END FUNCTION

!;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
FUNCTION ddxup0 (f)
!
!  X partial derivative
!
USE params
real, dimension(mx,my,mz):: f, ddxup0
!hpf$ distribute(*,*,block):: f, ddxup
!-----------------------------------------------------------------------
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)/dx
  c = c/dx
  b = b/dx
  ddxup0 = a *(cshift(f,dim=1,shift=1) - f) &
  + b *(cshift(f,dim=1,shift=2) - cshift(f,dim=1,shift=-1)) &
  + c *(cshift(f,dim=1,shift=3) - cshift(f,dim=1,shift=-2))
END FUNCTION
!;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
FUNCTION ddyup0 (f)
!
!  Y partial derivative
!
USE params
real, dimension(mx,my,mz):: f, ddyup0
!hpf$ distribute(*,*,block):: f, ddyup
!-----------------------------------------------------------------------
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)/dy
  c = c/dy
  b = b/dy
  ddyup0 = a *(cshift(f,dim=2,shift=1) - f) &
  + b *(cshift(f,dim=2,shift=2) - cshift(f,dim=2,shift=-1)) &
  + c *(cshift(f,dim=2,shift=3) - cshift(f,dim=2,shift=-2))
END FUNCTION
!;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
FUNCTION ddzup0 (f)
!
!  Z partial derivative
!
USE params
real, dimension(mx,my,mz):: f, ddzup0
!hpf$ distribute(*,*,block):: f, ddzup
!-----------------------------------------------------------------------
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)/dz
  c = c/dz
  b = b/dz
  ddzup0 = a *(cshift(f,dim=3,shift=1) - f) &
  + b *(cshift(f,dim=3,shift=2) - cshift(f,dim=3,shift=-1)) &
  + c *(cshift(f,dim=3,shift=3) - cshift(f,dim=3,shift=-2))
END FUNCTION
!;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
FUNCTION ddxdn0 (f)
!
!  X partial derivative
!
USE params
real, dimension(mx,my,mz):: f, ddxdn0
!hpf$ distribute(*,*,block):: f, ddxdn
!-----------------------------------------------------------------------
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)/dx
  c = c/dx
  b = b/dx
  ddxdn0 = a*(f - cshift(f,dim=1,shift=-1)) &
      + b*(cshift(f,dim=1,shift=1) - cshift(f,dim=1,shift=-2)) &
      + c*(cshift(f,dim=1,shift=2) - cshift(f,dim=1,shift=-3))
END FUNCTION
!;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
FUNCTION ddydn0 (f)
!
!  Y partial derivative
!
USE params
real, dimension(mx,my,mz):: f, ddydn0
!hpf$ distribute(*,*,block):: f, ddydn
!-----------------------------------------------------------------------
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)/dy
  c = c/dy
  b = b/dy
  ddydn0 = a*(f - cshift(f,dim=2,shift=-1)) &
      + b*(cshift(f,dim=2,shift=1) - cshift(f,dim=2,shift=-2)) &
      + c*(cshift(f,dim=2,shift=2) - cshift(f,dim=2,shift=-3))
END FUNCTION
!;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
FUNCTION ddzdn0 (f)
!
!  Z partial derivative
!
USE params
real, dimension(mx,my,mz):: f, ddzdn0
!hpf$ distribute(*,*,block):: f, ddzdn
!-----------------------------------------------------------------------
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)/dz
  c = c/dz
  b = b/dz
  ddzdn0 = a*(f - cshift(f,dim=3,shift=-1)) &
      + b*(cshift(f,dim=3,shift=1) - cshift(f,dim=3,shift=-2)) &
      + c*(cshift(f,dim=3,shift=2) - cshift(f,dim=3,shift=-3))
END FUNCTION

!;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
FUNCTION ddxup10 (f)
!
!  X partial derivative
!
USE params
real, dimension(mx,my,mz):: f, ddxup10
!hpf$ distribute(*,*,block):: f, ddxup10
!-----------------------------------------------------------------------
  a = 1./dx
  ddxup10 = a *(cshift(f,dim=1,shift=1) - f)
END FUNCTION
!;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
FUNCTION ddyup10 (f)
!
!  Y partial derivative
!
USE params
real, dimension(mx,my,mz):: f, ddyup10
!hpf$ distribute(*,*,block):: f, ddyup10
!-----------------------------------------------------------------------
  a = 1./dy
  ddyup10 = a *(cshift(f,dim=2,shift=1) - f)
END FUNCTION
!;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
FUNCTION ddzup10 (f)
!
!  Z partial derivative
!
USE params
real, dimension(mx,my,mz):: f, ddzup10
!hpf$ distribute(*,*,block):: f, ddzup10
!-----------------------------------------------------------------------
  a=1./dz
  ddzup10 = a *(cshift(f,dim=3,shift=1) - f)
END FUNCTION
!;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
FUNCTION ddxdn10 (f)
!
!  X partial derivative
!
USE params
real, dimension(mx,my,mz):: f, ddxdn10
!hpf$ distribute(*,*,block):: f, ddxdn10
!-----------------------------------------------------------------------
  a=1./dx
  ddxdn10 = a*(f - cshift(f,dim=1,shift=-1))
END FUNCTION
!;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
FUNCTION ddydn10 (f)
!
!  Y partial derivative
!
USE params
real, dimension(mx,my,mz):: f, ddydn10
!hpf$ distribute(*,*,block):: f, ddydn10
!-----------------------------------------------------------------------
  a=1./dy
  ddydn10 = a*(f - cshift(f,dim=2,shift=-1))
END FUNCTION
!;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
FUNCTION ddzdn10 (f)
!
!  Z partial derivative
!
USE params
real, dimension(mx,my,mz):: f, ddzdn10
!hpf$ distribute(*,*,block):: f, ddzdn10
!-----------------------------------------------------------------------
  a=1./dz
  ddzdn10 = a*(f - cshift(f,dim=3,shift=-1))
END FUNCTION

!;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
FUNCTION smooth30 (f)
!
!  Three point smoothing
!
USE params
real, dimension(mx,my,mz):: f, smooth30
!hpf$ distribute(*,*,block):: f, smooth3
!-----------------------------------------------------------------------
  smooth30 = (f       + cshift(f      ,dim=1,shift=-1) + cshift(f      ,dim=1,shift=1))*(1./3.)
  smooth30 = (smooth30 + cshift(smooth30,dim=2,shift=-1) + cshift(smooth30,dim=2,shift=1))*(1./3.)
  smooth30 = (smooth30 + cshift(smooth30,dim=3,shift=-1) + cshift(smooth30,dim=3,shift=1))*(1./3.)
END FUNCTION

!***********************************************************************
FUNCTION max51 (f)
!
!  Three point max
!
  USE params
  implicit none
  integer i,j,k,km1,kp1,km2,kp2
  real, dimension(mx,my,mz), intent(in) :: f
  real, dimension(mx,my,mz):: max51
  real, dimension(-1:mx+2):: fx
  real, dimension(-1:my+2):: fy
  real, dimension(mx,my,mz):: fz
!-----------------------------------------------------------------------
!
  do k=1,mz
  do j=1,my
    do i=1,mx
      fx(i)=f(i,j,k)
    enddo
    fx(  -1)=fx(mx-1)
    fx(   0)=fx(mx  )
    fx(mx+1)=fx(   1)
    fx(mx+2)=fx(   2)
    do i=1,mx
      fz(i,j,k)=amax1(fx(i-2),fx(i-1),fx(i),fx(i+1),fx(i+2))
    end do
  end do
  end do
  do k=1,mz
  do i=1,mx
    do j=1,my
      fy(j)=fz(i,j,k)
    enddo
    fy(  -1)=fy(my-1)
    fy(   0)=fy(my  )
    fy(my+1)=fy(   1)
    fy(my+2)=fy(   2)
    do j=1,my
      fz(i,j,k)=amax1(fy(j-2),fy(j-1),fy(j),fy(j+1),fy(j+2))
    end do
   end do
  end do
  do k=1,mz
    km2=mod(k+mz-3,mz)+1
    km1=mod(k+mz-2,mz)+1
    kp1=mod(k+mz  ,mz)+1
    kp2=mod(k+mz+1,mz)+1
    do j=1,my
    do i=1,mx
      max51(i,j,k)=amax1(fz(i,j,km2),fz(i,j,km1),fz(i,j,k),fz(i,j,kp1),fz(i,j,kp2))
    enddo
    enddo
  enddo
END FUNCTION

!***********************************************************************
FUNCTION max50 (f)
!
!  Three point max
!
  USE params
  implicit none
  integer i,j,k,km1,kp1,km2,kp2
  real, dimension(mx,my,mz), intent(in) :: f
  real, dimension(mx,my,mz):: max50
  real, dimension(-1:mx+2):: fx
  real, dimension(-1:my+2):: fy
  real, dimension(mx,my,mz):: fz
!-----------------------------------------------------------------------
  max50 = amax1(f     , cshift(f    ,dim=1,shift=-1), &
                        cshift(f    ,dim=1,shift=+1), &
                        cshift(f    ,dim=1,shift=-2), &
                        cshift(f    ,dim=1,shift=+2))
  max50 = amax1(max50 , cshift(max50,dim=2,shift=-1), &
                        cshift(max50,dim=2,shift=+1), &
                        cshift(max50,dim=2,shift=-2), &
                        cshift(max50,dim=2,shift=+2))
  max50 = amax1(max50 , cshift(max50,dim=3,shift=-1), &
                        cshift(max50,dim=3,shift=+1), &
                        cshift(max50,dim=3,shift=-2), &
                        cshift(max50,dim=3,shift=+2))
END FUNCTION
!
!***********************************************************************
SUBROUTINE dif2_set0 (nx, ny, nz, fx, fy, fz, difu)
!
!  Finite difference one zone left to one zones right.
!

  USE params
  real, dimension(nx,ny,nz):: fx, fy, fz, difu
!hpf$ align with TheCube:: fx, fy, fz, difu
!-----------------------------------------------------------------------

   difu = cshift(fx,dim=1,shift=-1) - cshift(fx,dim=1,shift=1) &
        + cshift(fy,dim=2,shift=-1) - cshift(fy,dim=2,shift=1) &
        + cshift(fz,dim=3,shift=-1) - cshift(fz,dim=3,shift=1)
END subroutine

END MODULE
