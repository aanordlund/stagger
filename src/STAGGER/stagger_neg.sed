# $Id: stagger_neg.sed,v 1.5 2006/02/26 00:23:30 aake Exp $
s/fp(\([^)]*\)) *=/fp(\1) = -/
s/fp *=/fp = -/
s/_set/_neg/g
s/- *-/+/
/print .*id/d
/character.*id/d
/call print_id/d
/omp_in_parallel.*WARNING:/d
