!***********************************************************************
FUNCTION zup (f)
!
!  f is centered on (i,j,k-.5), fifth order stagger
!
  use params
  real, dimension(mx,my,mz):: f, zup
!hpf$ distribute(*,*,block):: f, zup
!-----------------------------------------------------------------------
!
  character(len=80):: id = "$Id: z_unroll.f90,v 1.1 2003/11/24 21:21:53 aake Exp $" 
  if (id.ne.' ') print '(1x,a)',id; id=' '
!
  c = 3./256.
  b = -25./256.
  a = .5-b-c
  a1st = 0.5
!
!$omp parallel do private(i,j,k)
  do k=1,mz,2
    km3=mod(k+mz-4,mz)+1
    km2=mod(k+mz-3,mz)+1
    km1=mod(k+mz-2,mz)+1
    kp1=mod(k+mz-1,mz)+1
    kp2=mod(k+mz+1,mz)+1
    kp3=mod(k+mz+2,mz)+1
    do j=1,my
      do i=1,mx
        zup(i,j,km1) = ( &
                     a*(f(i,j,k  )+f(i,j,km1)) + &
                     b*(f(i,j,kp1)+f(i,j,km2)) + &
                     c*(f(i,j,kp2)+f(i,j,km3)))
        zup(i,j,k) = ( &
                     a*(f(i,j,kp1)+f(i,j,k  )) + &
                     b*(f(i,j,kp2)+f(i,j,km1)) + &
                     c*(f(i,j,kp3)+f(i,j,km2)))
      end do
    end do
  end do
!
  nstag = nstag+1
  nflop = nflop+8
END FUNCTION

!***********************************************************************
FUNCTION zup1 (f)
!
!  f is centered on (i,j,k-.5), fifth order stagger
!
  use params
  real, dimension(mx,my,mz):: f, zup1
!hpf$ distribute(*,*,block):: f, zup1
!-----------------------------------------------------------------------
!
  a = .5
!
!$omp parallel do private(i,j,k)
  do k=1,mz-1
    do j=1,my
      do i=1,mx
        zup1(i,j,k) = ( &
                     a*(f(i,j,k+1)+f(i,j,k  )))
      end do
    end do
  end do
  do j=1,my
    do i=1,mx
      zup1(i,j,mz) = ( &
                   a*(f(i,j,1  )+f(i,j,mz  )))
    end do
  end do
!
  nstag1 = nstag1+1
  nflop = nflop+2
END FUNCTION

!***********************************************************************
FUNCTION zdn (f)
!
!  f is centered on (i,j,k), fifth order stagger
!
  use params
  real, dimension(mx,my,mz):: f, zdn
  intent(in):: f
!hpf$ distribute(*,*,block):: f, zdn
!-----------------------------------------------------------------------
!
  c = 3./256.
  b = -25./256.
  a = .5-b-c
  a1st = 0.5
!
!$omp parallel do private(i,j,k)
  do k=1,mz,3
    km4=mod(k+mz-5,mz)+1
    km3=mod(k+mz-4,mz)+1
    km2=mod(k+mz-3,mz)+1
    km1=mod(k+mz-2,mz)+1
    kp1=mod(k+mz-1,mz)+1
    kp2=mod(k+mz+1,mz)+1
    kp3=mod(k+mz+2,mz)+1
    do j=1,my
      do i=1,mx
        zdn(i,j,km1) = ( &
                     a*(f(i,j,km1)+f(i,j,km2)) + &
                     b*(f(i,j,k  )+f(i,j,km3)) + &
                     c*(f(i,j,kp1)+f(i,j,km4)))
        zdn(i,j,k) = ( &
                     a*(f(i,j,k  )+f(i,j,km1)) + &
                     b*(f(i,j,kp1)+f(i,j,km2)) + &
                     c*(f(i,j,kp2)+f(i,j,km3)))
        zdn(i,j,kp1) = ( &
                     a*(f(i,j,kp1)+f(i,j,k  )) + &
                     b*(f(i,j,kp2)+f(i,j,km1)) + &
                     c*(f(i,j,kp3)+f(i,j,km2)))
      end do
    end do
  end do
!
  nstag = nstag+1
  nflop = nflop+8
END FUNCTION

!***********************************************************************
FUNCTION zdn1 (f)
!
!  f is centered on (i,j,k), fifth order stagger
!
  use params
  real, dimension(mx,my,mz):: f, zdn1
!hpf$ distribute(*,*,block):: f, zdn1
!-----------------------------------------------------------------------
!
  a = .5
!
!$omp parallel do private(i,j,k)
  do k=2,mz
    do j=1,my
      do i=1,mx
        zdn1(i,j,k) = ( &
                     a*(f(i,j,k  )+f(i,j,k-1)))
      end do
    end do
  end do
  do j=1,my
    do i=1,mx
      zdn1(i,j,1) = ( &
                   a*(f(i,j,1  )+f(i,j,mz  )))
    end do
  end do
!
  nstag1 = nstag1+1
  nflop = nflop+2
END FUNCTION
