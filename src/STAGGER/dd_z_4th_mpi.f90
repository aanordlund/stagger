!***********************************************************************
FUNCTION ddzup (f)
!
!  f is centered on (i,j,k-.5), fifth order stagger
!
  use params, g=>gz1, h=>hz2
  implicit none
  real a, b
  integer i, j, k
  real, intent(in) , dimension(mx,my,mz):: f
  real,              dimension(mx,my,mz):: ddzup
!-----------------------------------------------------------------------
!
  character(len=mid):: id = "$Id: dd_z_4th_mpi.f90,v 1.5 2013/11/03 10:10:00 aake Exp $" 
  call print_id (id)

  if (mz .lt. 5) then
    !$omp parallel private(k)
    do k=izs,ize
      ddzup(:,:,k) = 0.
    end do
    !$omp end parallel
    return
  end if

  b = -1./(24.*dy)
  a = 1./dy-3.*b

  call mpi_send_z (f, g, 1, h, 2)
!
  !$omp parallel private(i,j,k)
  do k=max(2,izs),min(mz-2,ize)
    do j=1,my
      do i=1,mx
        ddzup(i,j,k) = ( &
                     b*(f(i,j,k+2 )-f(i,j,k-1 )) + &
                     a*(f(i,j,k+1 )-f(i,j,k   )))
     end do
    end do
  end do

  if (ize.ge.mz-1) then
   do j=1,my
    do i=1,mx
      ddzup(i,j,mz-1) = ( &
                     b*(h(i,j,1   )-f(i,j,mz-2)) + &
                     a*(f(i,j,mz  )-f(i,j,mz-1)))
      ddzup(i,j,mz  ) = ( &
                     b*(h(i,j,2   )-f(i,j,mz-1)) + &
                     a*(h(i,j,1   )-f(i,j,mz  )))
    end do
   end do
  end if
  if (izs.eq.1) then
   do j=1,my
    do i=1,mx
      ddzup(i,j,1   ) = ( &
                     b*(f(i,j,3   )-g(i,j,1   )) + &
                     a*(f(i,j,2   )-f(i,j,1   )))
    end do
   end do
  end if
  !$omp end parallel
!
  nstag = nstag+1
  nflop = nflop+8
END FUNCTION

!***********************************************************************
FUNCTION ddzup1 (f)
!
!  f is centered on (i,j,k-.5), fifth order stagger
!
  use params, g=>gz1
  implicit none
  real a
  integer i, j, k
  real, intent(in) , dimension(mx,my,mz):: f
  real,              dimension(mx,my,mz):: ddzup1
!-----------------------------------------------------------------------
!
  if (mz .lt. 5) then
    !$omp parallel private(k)
    do k=izs,ize
      ddzup1(:,:,k) = 0.
    end do
    !$omp end parallel
    return
  end if
!
  a = 1./dz
  call mpi_send_z (f, g, 0, g, 1)
!
  !$omp parallel private(i,j,k)
  do k=izs,min(mz-1,ize)
    do j=1,my
      do i=1,mx
        ddzup1(i,j,k  ) = ( &
                     a*(f(i,j,k+1)-f(i,j,k)))
      end do
    end do
  end do
  if (ize.eq.mz) then
   do j=1,my
    do i=1,mx
      ddzup1(i,j,mz) = ( &
                     a*(g(i,j,1)-f(i,j,mz)))
    end do
   end do
  end if
  !$omp end parallel
!
  nstag1 = nstag1+1
  nflop = nflop+2
END FUNCTION

!***********************************************************************
FUNCTION ddzdn (f)
!
!  f is centered on (i,j), fifth order stagger
!
  use params, g=>gz2, h=>hz1
  implicit none
  real a, b
  integer i, j, k
  real, dimension(mx,my,mz):: f, ddzdn
!-----------------------------------------------------------------------
!
  if (mz .lt. 5) then
    !$omp parallel private(k)
    do k=izs,ize
      ddzdn(:,:,k) = 0.
    end do
    !$omp end parallel
    return
  end if

  b = -1./(24.*dy)
  a = 1./dy-3.*b

  call mpi_send_z (f, g, 2, h, 1)
!
  !$omp parallel private(i,j,k)
  do k=max(3,izs),min(mz-1,ize)
    do j=1,my
     do i=1,mx
      ddzdn(i,j,k) = ( &
                     b*(f(i,j,k+1 )-f(i,j,k-2 )) + &
                     a*(f(i,j,k   )-f(i,j,k-1 )))
     end do
    end do
  end do
  if (ize.eq.mz) then
   do j=1,my
    do i=1,mx
      ddzdn(i,j,mz  ) = ( &
                     b*(h(i,j,1   )-f(i,j,mz-2)) + &
                     a*(f(i,j,mz  )-f(i,j,mz-1)))
    end do
   end do
  end if
  if (izs.le.2) then
   do j=1,my
    do i=1,mx
      ddzdn(i,j,1   ) = ( &
                     b*(f(i,j,2   )-g(i,j,1   )) + &
                     a*(f(i,j,1   )-g(i,j,2   )))
      ddzdn(i,j,2   ) = ( &
                     b*(f(i,j,3   )-g(i,j,2   )) + &
                     a*(f(i,j,2   )-f(i,j,1   )))
    end do
   end do
  end if
  !$omp end parallel
!
  nstag = nstag+1
  nflop = nflop+8
END FUNCTION

!***********************************************************************
FUNCTION ddzdn1 (f)
!
!  f is centered on (i,j), fifth order stagger
!
  use params, g=>gz1
  implicit none
  real a
  integer i, j, k
  real, dimension(mx,my,mz):: f, ddzdn1
!-----------------------------------------------------------------------
!
  if (mz .lt. 5) then
    !$omp parallel private(k)
    do k=izs,ize
      ddzdn1(:,:,k) = 0.
    end do
    !$omp end parallel
    return
  end if
!
  a = 1./dz
  call mpi_send_z (f, g, 1, g, 0)
!
  !$omp parallel private(i,j,k)
  do k=max(2,izs),ize
    do j=1,my
     do i=1,mx
      ddzdn1(i,j,k) = ( &
                     a*(f(i,j,k)-f(i,j,k-1)))
     end do
    end do
  end do
  if (izs.eq.1) then
   do j=1,my
    do i=1,mx
      ddzdn1(i,j,1   ) = ( &
                     a*(f(i,j,1)-g(i,j,1  )))
    end do
   end do
  end if
  !$omp end parallel
!
  nstag1 = nstag1+1
  nflop = nflop+2
END FUNCTION
