# $Id: stagger_omp.sed,v 1.6 2007/04/14 10:27:03 aake Exp $
/^ *FUNCTION/{
s/FUNCTION \([a-z1]*\)/SUBROUTINE \1_set_omp/
s/(f *)/(f, fp)/
}
/^ *END/s/FUNCTION/SUBROUTINE/
/real,.*d*[xyz]up[i123]*$/s/d*[xyz]up[i123]*$/fp/
/::/s/\(d*[xyz]up[i123]*\)/fp/
/::/s/\(d*[xyz]dn[i123]*\)/fp/
s/d*[xyz]up[i123]* *= */fp = /
s/d*[xyz]dn[i123]* *= */fp = /
s/ d*[xyz]up[i123]* *(/ fp(/
s/ d*[xyz]dn[i123]* *(/ fp(/
/real,.*smooth[35]t* *$/s/smooth[35]t* *$/fp/
/::/s/smooth[35]t*/fp/
s/smooth[35]t* *= */fp = /
/real,.*max[35] *$/s/max[35] *$/fp/
/::/s/max[35]/fp/
s/max[35] *= */fp = /
/nflop/d
/nstag/d
/omp/s/omp parallel do/omp parallel/
s/Xomp/\$omp/
/[1-4],mz-/s/\([1-4]\),\(.*\)/max(\1,izs),min(\2,ize)/
/[2-4],mz/s/\([2-4]\),\(.*\)/max(\1,izs),min(\2,ize)/
s/= *1,mz *$/=izs,ize/
/omp.*parallel/d
s/1,mz/izs,ize/
/[0-9]omp /d
