!***********************************************************************
FUNCTION zup (f)
!
!  f is centered on (i,j,k-.5), fifth order stagger
!
  use params
  implicit none
  real a, b, c, a1st
  integer i, j, k, km2, km1, kp1, kp2, kp3
  real, dimension(mx,my,mz):: f, zup
!hpf$ distribute(*,*,block):: f, zup
!-----------------------------------------------------------------------
!
  character(len=mid):: id = "$Id: z_do.f90,v 1.14 2007/04/14 12:57:16 bob Exp $" 
  call print_id (id)
!
  if (mz.le.5) then
    zup = f
    return
  end if
  c = 3./256.
  b = -25./256.
  a = .5-b-c
  a1st = 0.5
!
!Xomp barrier
!$omp parallel private(i,j,k,km2,km1,kp1,kp2,kp3)
  do k=izs,ize
    km2=mod(k-3+mz,mz)+1
    km1=mod(k-2+mz,mz)+1
    kp1=mod(k     ,mz)+1
    kp2=mod(k+1   ,mz)+1
    kp3=mod(k+2   ,mz)+1
    do j=1,my
      do i=1,mx
        zup(i,j,k) = ( &
                     a*(f(i,j,kp1)+f(i,j,k  )) + &
                     b*(f(i,j,kp2)+f(i,j,km1)) + &
                     c*(f(i,j,kp3)+f(i,j,km2)))
      end do
    end do
  end do
!$omp end parallel
!1omp barrier
!
  if (omp_master) then
    nstag = nstag+1
    nflop = nflop+8
  end if
END FUNCTION

!***********************************************************************
FUNCTION zup1 (f)
!
!  f is centered on (i,j,k-.5), fifth order stagger
!
  use params
  implicit none
  real a, b, c, a1st
  integer i, j, k, kp1
  real, dimension(mx,my,mz):: f, zup1
!hpf$ distribute(*,*,block):: f, zup1
!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    zup1 = f
    return
  end if
  a = .5
!
!Xomp barrier
!$omp parallel private(i,j,k,kp1)
  do k=izs,ize
    kp1=mod(k     ,mz)+1
    do j=1,my
      do i=1,mx
        zup1(i,j,k) = ( &
                     a*(f(i,j,kp1)+f(i,j,k  )))
      end do
    end do
  end do
!$omp end parallel
!1omp barrier
!
  if (omp_master) then
    nstag1 = nstag1+1
    nflop = nflop+2
  end if
END FUNCTION

!***********************************************************************
FUNCTION zdn (f)
!
!  f is centered on (i,j,k), fifth order stagger
!
  use params
  implicit none
  real a, b, c, a1st
  integer i, j, k, km3, km2, km1, kp1, kp2
  real, dimension(mx,my,mz):: f, zdn
!hpf$ distribute(*,*,block):: f, zdn
!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    zdn = f
    return
  end if
  c = 3./256.
  b = -25./256.
  a = .5-b-c
  a1st = 0.5
!
!Xomp barrier
!$omp parallel private(i,j,k,km3,km2,km1,kp1,kp2)
  do k=izs,ize
    km3=mod(k-4+mz,mz)+1
    km2=mod(k-3+mz,mz)+1
    km1=mod(k-2+mz,mz)+1
    kp1=mod(k     ,mz)+1
    kp2=mod(k+1   ,mz)+1
    do j=1,my
      do i=1,mx
        zdn(i,j,k) = ( &
                     a*(f(i,j,k  )+f(i,j,km1)) + &
                     b*(f(i,j,kp1)+f(i,j,km2)) + &
                     c*(f(i,j,kp2)+f(i,j,km3)))
      end do
    end do
  end do
!$omp end parallel
!1omp barrier
!
  if (omp_master) then
    nstag = nstag+1
    nflop = nflop+8
  end if
END FUNCTION

!***********************************************************************
FUNCTION zdn1 (f)
!
!  f is centered on (i,j,k), fifth order stagger
!
  use params
  implicit none
  real a, b, c, a1st
  integer i, j, k, km1
  real, dimension(mx,my,mz):: f, zdn1
!hpf$ distribute(*,*,block):: f, zdn1
!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    zdn1 = f
    return
  end if
  a = .5
!
!Xomp barrier
!$omp parallel private(i,j,k,km1)
  do k=izs,ize
    km1=mod(k-2+mz,mz)+1
    do j=1,my
      do i=1,mx
        zdn1(i,j,k) = ( &
                     a*(f(i,j,k  )+f(i,j,km1)))
      end do
    end do
  end do
!$omp end parallel
!1omp barrier
!
  if (omp_master) then
    nstag1 = nstag1+1
    nflop = nflop+2
  end if
END FUNCTION
!***********************************************************************
FUNCTION zup32 (f,j)
!
!  f is centered on (i,j,k-.5), fifth order stagger
!
  use params
  implicit none
  real a, b, c, a1st
  integer i, j, k, km2, km1, kp1, kp2, kp3
  real, dimension(mx,my,mz):: f
  real, dimension(mx,mz):: zup32
!hpf$ distribute(*,*,block):: f, zup32
!-----------------------------------------------------------------------
!
  character(len=mid):: id = "$Id: z_do.f90,v 1.14 2007/04/14 12:57:16 bob Exp $" 
  call print_id (id)
!
  if (mz.le.5) then
    zup32 = f(:,j,:)
    return
  end if
  c = 3./256.
  b = -25./256.
  a = .5-b-c
  a1st = 0.5
!
!Xomp barrier
!$omp parallel private(i,j,k,km2,km1,kp1,kp2,kp3)
  do k=izs,ize
    km2=mod(k-3+mz,mz)+1
    km1=mod(k-2+mz,mz)+1
    kp1=mod(k     ,mz)+1
    kp2=mod(k+1   ,mz)+1
    kp3=mod(k+2   ,mz)+1
      do i=1,mx
        zup32(i,k) = ( &
                     a*(f(i,j,kp1)+f(i,j,k  )) + &
                     b*(f(i,j,kp2)+f(i,j,km1)) + &
                     c*(f(i,j,kp3)+f(i,j,km2)))
      end do
  end do
!$omp end parallel
!1omp barrier
!
  if (omp_master) then
    nstag = nstag+1
    nflop = nflop+8
  end if
END FUNCTION

!***********************************************************************
FUNCTION zup132 (f,j)
!
!  f is centered on (i,j,k-.5), fifth order stagger
!
  use params
  implicit none
  real a, b, c, a1st
  integer i, j, k, kp1
  real, dimension(mx,my,mz):: f
  real, dimension(mx,mz):: zup132
!hpf$ distribute(*,*,block):: f, zup1
!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    zup132 = f(:,j,:)
    return
  end if
  a = .5
!
!Xomp barrier
!$omp parallel private(i,j,k,kp1)
  do k=izs,ize
    kp1=mod(k     ,mz)+1
      do i=1,mx
        zup132(i,k) = ( &
                     a*(f(i,j,kp1)+f(i,j,k  )))
      end do
  end do
!$omp end parallel
!1omp barrier
!
  if (omp_master) then
    nstag1 = nstag1+1
    nflop = nflop+2
  end if
END FUNCTION

!***********************************************************************
FUNCTION zdn32 (f,j)
!
!  f is centered on (i,j,k), fifth order stagger
!
  use params
  implicit none
  real a, b, c, a1st
  integer i, j, k, km3, km2, km1, kp1, kp2
  real, dimension(mx,my,mz):: f
  real, dimension(mx,mz):: zdn32
!hpf$ distribute(*,*,block):: f, zdn
!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    zdn32 = f(:,j,:)
    return
  end if
  c = 3./256.
  b = -25./256.
  a = .5-b-c
  a1st = 0.5
!
!Xomp barrier
!$omp parallel private(i,j,k,km3,km2,km1,kp1,kp2)
  do k=izs,ize
    km3=mod(k-4+mz,mz)+1
    km2=mod(k-3+mz,mz)+1
    km1=mod(k-2+mz,mz)+1
    kp1=mod(k     ,mz)+1
    kp2=mod(k+1   ,mz)+1
      do i=1,mx
        zdn32(i,k) = ( &
                     a*(f(i,j,k  )+f(i,j,km1)) + &
                     b*(f(i,j,kp1)+f(i,j,km2)) + &
                     c*(f(i,j,kp2)+f(i,j,km3)))
      end do
  end do
!$omp end parallel
!1omp barrier
!
  if (omp_master) then
    nstag = nstag+1
    nflop = nflop+8
  end if
END FUNCTION

!***********************************************************************
FUNCTION zdn132 (f,j)
!
!  f is centered on (i,j,k), fifth order stagger
!
  use params
  implicit none
  real a, b, c, a1st
  integer i, j, k, km1
  real, dimension(mx,my,mz):: f
  real, dimension(mx,mz):: zdn132
!hpf$ distribute(*,*,block):: f, zdn1
!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    zdn132 = f(:,j,:)
    return
  end if
  a = .5
!
!Xomp barrier
!$omp parallel private(i,j,k,km1)
  do k=izs,ize
    km1=mod(k-2+mz,mz)+1
      do i=1,mx
        zdn132(i,k) = ( &
                     a*(f(i,j,k  )+f(i,j,km1)))
      end do
  end do
!$omp end parallel
!1omp barrier
!
  if (omp_master) then
    nstag1 = nstag1+1
    nflop = nflop+2
  end if
END FUNCTION
!***********************************************************************
FUNCTION zup22 (f)
!
!  f is centered on (i,k-.5), fifth order stagger
!
  use params
  implicit none
  real a, b, c, a1st
  integer i, j, k, km2, km1, kp1, kp2, kp3
  real, dimension(mx,mz):: zup22, f
!hpf$ distribute(*,*,block):: f, zup
!-----------------------------------------------------------------------
!
  character(len=mid):: id = "$Id: z_do.f90,v 1.14 2007/04/14 12:57:16 bob Exp $" 
  call print_id (id)
!
  if (mz.le.5) then
    zup22 = f
    return
  end if
  c = 3./256.
  b = -25./256.
  a = .5-b-c
  a1st = 0.5
!
!Xomp barrier
!$omp parallel private(i,k,km2,km1,kp1,kp2,kp3)
  do k=izs,ize
    km2=mod(k-3+mz,mz)+1
    km1=mod(k-2+mz,mz)+1
    kp1=mod(k     ,mz)+1
    kp2=mod(k+1   ,mz)+1
    kp3=mod(k+2   ,mz)+1
      do i=1,mx
        zup22(i,k) = ( &
                     a*(f(i,kp1)+f(i,k  )) + &
                     b*(f(i,kp2)+f(i,km1)) + &
                     c*(f(i,kp3)+f(i,km2)))
      end do
  end do
!$omp end parallel
!1omp barrier
!
  if (omp_master) then
    nstag = nstag+1
    nflop = nflop+8
  end if
END FUNCTION

!***********************************************************************
FUNCTION zup122 (f)
!
!  f is centered on (i,k-.5), fifth order stagger
!
  use params
  implicit none
  real a, b, c, a1st
  integer i, j, k, kp1
  real, dimension(mx,mz):: zup122, f
!hpf$ distribute(*,*,block):: f, zup1
!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    zup122 = f
    return
  end if
  a = .5
!
!Xomp barrier
!$omp parallel private(i,k,kp1)
  do k=izs,ize
    kp1=mod(k     ,mz)+1
      do i=1,mx
        zup122(i,k) = ( &
                     a*(f(i,kp1)+f(i,k  )))
      end do
  end do
!$omp end parallel
!1omp barrier
!
  if (omp_master) then
    nstag1 = nstag1+1
    nflop = nflop+2
  end if
END FUNCTION

!***********************************************************************
FUNCTION zdn22 (f)
!
!  f is centered on (i,k), fifth order stagger
!
  use params
  implicit none
  real a, b, c, a1st
  integer i, j, k, km3, km2, km1, kp1, kp2
  real, dimension(mx,mz):: zdn22, f
!hpf$ distribute(*,*,block):: f, zdn
!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    zdn22 = f
    return
  end if
  c = 3./256.
  b = -25./256.
  a = .5-b-c
  a1st = 0.5
!
!Xomp barrier
!$omp parallel private(i,k,km3,km2,km1,kp1,kp2)
  do k=izs,ize
    km3=mod(k-4+mz,mz)+1
    km2=mod(k-3+mz,mz)+1
    km1=mod(k-2+mz,mz)+1
    kp1=mod(k     ,mz)+1
    kp2=mod(k+1   ,mz)+1
      do i=1,mx
        zdn22(i,k) = ( &
                     a*(f(i,k  )+f(i,km1)) + &
                     b*(f(i,kp1)+f(i,km2)) + &
                     c*(f(i,kp2)+f(i,km3)))
      end do
  end do
!$omp end parallel
!1omp barrier
!
  if (omp_master) then
    nstag = nstag+1
    nflop = nflop+8
  end if
END FUNCTION

!***********************************************************************
FUNCTION zdn122 (f)
!
!  f is centered on (i,k), fifth order stagger
!
  use params
  implicit none
  real a, b, c, a1st
  integer i, j, k, km1
  real, dimension(mx,mz):: zdn122, f
!hpf$ distribute(*,*,block):: f, zdn1
!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    zdn122 = f
    return
  end if
  a = .5
!
!Xomp barrier
!$omp parallel private(i,k,km1)
  do k=izs,ize
    km1=mod(k-2+mz,mz)+1
      do i=1,mx
        zdn122(i,k) = ( &
                     a*(f(i,k  )+f(i,km1)))
      end do
  end do
!$omp end parallel
!1omp barrier
!
  if (omp_master) then
    nstag1 = nstag1+1
    nflop = nflop+2
  end if
END FUNCTION
