!***********************************************************************
FUNCTION xup (f)
!
!  f is centered on (i-.5,j,k), fifth order stagger
!
  use params
  real, dimension(mx,my,mz):: f, xup
!hpf$ distribute(*,*,block):: f, xup
!-----------------------------------------------------------------------
!
  character(len=80):: id = "$Id: stagger_x_do.f90,v 1.7 2003/04/27 10:10:37 aake Exp $" 
  if (id.ne.' ') print '(1x,a)',id; id=' '
!
  if (mx.le.5) then
    xup = f
    return
  end if
  c = 3./256.
  b = -25./256.
  a = .5-b-c
  a1st = 0.5
!
!$omp parallel do private(i,j,k)
  do k=1,mz
   do j=1,my
    xup(mx-2,j,k) = ( &
                   a*(f(mx-2,j,k)+f(mx-1,j,k)) &
                 + b*(f(mx-3,j,k)+f(mx  ,j,k)) &
                 + c*(f(mx-4,j,k)+f(1   ,j,k)))
    xup(mx-1,j,k) = ( &
                   a*(f(mx-1,j,k)+f(mx  ,j,k)) &
                 + b*(f(mx-2,j,k)+f(1   ,j,k)) &
                 + c*(f(mx-3,j,k)+f(2   ,j,k)))
    xup(mx  ,j,k) = ( &
                   a*(f(mx  ,j,k)+f(1   ,j,k)) &
                 + b*(f(mx-1,j,k)+f(2   ,j,k)) &
                 + c*(f(mx-2,j,k)+f(3   ,j,k)))
    xup(1   ,j,k) = ( &
                   a*(f(1   ,j,k)+f(2   ,j,k)) &
                 + b*(f(mx  ,j,k)+f(3   ,j,k)) &
                 + c*(f(mx-1,j,k)+f(4   ,j,k)))
    xup(2   ,j,k) = ( &
                   a*(f(2   ,j,k)+f(3   ,j,k)) &
                 + b*(f(1   ,j,k)+f(4   ,j,k)) &
                 + c*(f(mx  ,j,k)+f(5   ,j,k)))
   end do
   do j=1,my
    do i=3,mx-3
      xup(i  ,j,k) = ( &
                    a*(f(i  ,j,k)+f(i+1 ,j,k)) &
                  + b*(f(i-1,j,k)+f(i+2 ,j,k)) &
                  + c*(f(i-2,j,k)+f(i+3 ,j,k)))
    end do
   end do
  end do
!
  nstag = nstag+1
  nflop = nflop+8
END FUNCTION

!***********************************************************************
FUNCTION xup1 (f)
!
!  f is centered on (i-.5,j,k), fifth order stagger
!
  use params
  real, dimension(mx,my,mz):: f, xup1
!hpf$ distribute(*,*,block):: f, xup1
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    xup1 = f
    return
  end if
!
  a = .5
!
!$omp parallel do private(i,j,k)
  do k=1,mz
   do j=1,my
    xup1(mx  ,j,k) = ( &
                   a*(f(mx  ,j,k)+f(1   ,j,k)))
   end do
   do j=1,my
    do i=1,mx-1
      xup1(i  ,j,k) = ( &
                    a*(f(i  ,j,k)+f(i+1 ,j,k)))
    end do
   end do
  end do
!
  nstag1 = nstag1+1
  nflop = nflop+2
END FUNCTION

!***********************************************************************
FUNCTION xdn (f)
!
!  f is centered on (i,j,k), fifth order stagger
!
  use params
  real, dimension(mx,my,mz):: f, xdn
!hpf$ distribute(*,*,block):: f, xdn
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    xdn = f
    return
  end if
!
  c = 3./256.
  b = -25./256.
  a = .5-b-c
  a1st = 0.5
!
!$omp parallel do private(i,j,k)
  do k=1,mz
   do j=1,my
    xdn(mx-1,j,k) = ( &
                   a*(f(mx-2,j,k)+f(mx-1,j,k)) &
                 + b*(f(mx-3,j,k)+f(mx  ,j,k)) &
                 + c*(f(mx-4,j,k)+f(1   ,j,k)))
    xdn(mx  ,j,k) = ( &
                   a*(f(mx-1,j,k)+f(mx  ,j,k)) &
                 + b*(f(mx-2,j,k)+f(1   ,j,k)) &
                 + c*(f(mx-3,j,k)+f(2   ,j,k)))
    xdn(1  ,j,k) = ( &
                   a*(f(mx  ,j,k)+f(1   ,j,k)) &
                 + b*(f(mx-1,j,k)+f(2   ,j,k)) &
                 + c*(f(mx-2,j,k)+f(3   ,j,k)))
    xdn(2  ,j,k) = ( &
                   a*(f(1   ,j,k)+f(2   ,j,k)) &
                 + b*(f(mx  ,j,k)+f(3   ,j,k)) &
                 + c*(f(mx-1,j,k)+f(4   ,j,k)))
    xdn(3  ,j,k) = ( &
                   a*(f(2   ,j,k)+f(3   ,j,k)) &
                 + b*(f(1   ,j,k)+f(4   ,j,k)) &
                 + c*(f(mx  ,j,k)+f(5   ,j,k)))
   end do
   do j=1,my
    do i=4,mx-2
      xdn(i,j,k) = ( &
                   a*(f(i   ,j,k)+f(i-1 ,j,k)) &
                 + b*(f(i+1 ,j,k)+f(i-2 ,j,k)) &
                 + c*(f(i+2 ,j,k)+f(i-3 ,j,k)))
    end do
   end do
  end do
!
!
  nstag = nstag+1
  nflop = nflop+8
END FUNCTION

!***********************************************************************
FUNCTION xdn1 (f)
!
!  f is centered on (i,j,k), fifth order stagger
!
  use params
  real, dimension(mx,my,mz):: f, xdn1
!hpf$ distribute(*,*,block):: f, xdn1
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    xdn1 = f
    return
  end if
!
  a = 0.5
!
!$omp parallel do private(i,j,k)
  do k=1,mz
   do j=1,my
    xdn1(1  ,j,k) = ( &
                   a*(f(mx  ,j,k)+f(1   ,j,k)))
   end do
   do j=1,my
    do i=2,mx
      xdn1(i,j,k) = ( &
                   a*(f(i   ,j,k)+f(i-1 ,j,k)))
    end do
   end do
  end do
!
  nstag1 = nstag1+1
  nflop = nflop+2
END FUNCTION

!***********************************************************************
FUNCTION ddxup (f)
  use params
  real, dimension(mx,my,mz):: f, ddxup
!hpf$ distribute(*,*,block):: f, ddxup
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    ddxup = 0.
    return
  end if
!
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)/dx
  a1st = 1./dx
  b = b/dx
  c = c/dx
!
!$omp parallel do private(i,j,k)
  do k=1,mz
   do j=1,my
    ddxup(mx-2,j,k) = ( &
                   a*(f(mx-1,j,k)-f(mx-2,j,k)) &
                 + b*(f(mx  ,j,k)-f(mx-3,j,k)) &
                 + c*(f(1   ,j,k)-f(mx-4,j,k)))
    ddxup(mx-1,j,k) = ( &
                   a*(f(mx  ,j,k)-f(mx-1,j,k)) &
                 + b*(f(1   ,j,k)-f(mx-2,j,k)) &
                 + c*(f(2   ,j,k)-f(mx-3,j,k)))
    ddxup(mx  ,j,k) = ( &
                   a*(f(1   ,j,k)-f(mx  ,j,k)) &
                 + b*(f(2   ,j,k)-f(mx-1,j,k)) &
                 + c*(f(3   ,j,k)-f(mx-2,j,k)))
    ddxup(1  ,j,k) = ( &
                   a*(f(2   ,j,k)-f(1   ,j,k)) &
                 + b*(f(3   ,j,k)-f(mx  ,j,k)) &
                 + c*(f(4   ,j,k)-f(mx-1,j,k)))
    ddxup(2  ,j,k) = ( &
                   a*(f(3   ,j,k)-f(2  ,j,k)) &
                 + b*(f(4   ,j,k)-f(1  ,j,k)) &
                 + c*(f(5   ,j,k)-f(mx ,j,k)))
   end do
   do j=1,my
    do i=3,mx-3
      ddxup(i  ,j,k) = ( &
                   a*(f(i+1 ,j,k)-f(i   ,j,k)) &
                 + b*(f(i+2 ,j,k)-f(i-1 ,j,k)) &
                 + c*(f(i+3 ,j,k)-f(i-2 ,j,k)))
    end do
  end do
  end do
!
  nstag = nstag+1
  nflop = nflop+8
END FUNCTION

!***********************************************************************
FUNCTION ddxup1 (f)
  use params
  real, dimension(mx,my,mz):: f, ddxup1
!hpf$ distribute(*,*,block):: f, ddxup1
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    ddxup1 = 0.
    return
  end if
!
  a = 1./dx
!
!$omp parallel do private(i,j,k)
  do k=1,mz
   do j=1,my
    ddxup1(mx  ,j,k) = ( &
                   a*(f(1   ,j,k)-f(mx  ,j,k)))
   end do
   do j=1,my
    do i=1,mx-1
      ddxup1(i  ,j,k) = ( &
                   a*(f(i+1 ,j,k)-f(i   ,j,k)))
    end do
  end do
  end do
!
  nstag1 = nstag1+1
  nflop = nflop+2
END FUNCTION

!***********************************************************************
FUNCTION ddxdn (f)
  use params
  real, dimension(mx,my,mz):: f, ddxdn
!hpf$ distribute(*,*,block):: f, ddxdn
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    ddxdn = 0.
    return
  end if
!
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)/dx
  a1st = 1./dx
  b = b/dx
  c = c/dx
!
!$omp parallel do private(i,j,k)
  do k=1,mz
   do j=1,my
    ddxdn(mx-1,j,k) = ( &
                   a*(f(mx-1,j,k)-f(mx-2,j,k)) &
                 + b*(f(mx  ,j,k)-f(mx-3,j,k)) &
                 + c*(f(1   ,j,k)-f(mx-4,j,k)))
    ddxdn(mx  ,j,k) = ( &
                   a*(f(mx  ,j,k)-f(mx-1,j,k)) &
                 + b*(f(1   ,j,k)-f(mx-2,j,k)) &
                 + c*(f(2   ,j,k)-f(mx-3,j,k)))
    ddxdn(1   ,j,k) = ( &
                   a*(f(1   ,j,k)-f(mx  ,j,k)) &
                 + b*(f(2   ,j,k)-f(mx-1,j,k)) &
                 + c*(f(3   ,j,k)-f(mx-2,j,k)))
    ddxdn(2  ,j,k) = ( &
                   a*(f(2   ,j,k)-f(1   ,j,k)) &
                 + b*(f(3   ,j,k)-f(mx  ,j,k)) &
                 + c*(f(4   ,j,k)-f(mx-1,j,k)))
    ddxdn(3  ,j,k) = ( &
                   a*(f(3   ,j,k)-f(2   ,j,k)) &
                 + b*(f(4   ,j,k)-f(1   ,j,k)) &
                 + c*(f(5   ,j,k)-f(mx  ,j,k)))
   end do
   do j=1,my
    do i=4,mx-2
      ddxdn(i,j,k) = ( &
                   a*(f(i   ,j,k)-f(i-1 ,j,k)) &
                 + b*(f(i+1 ,j,k)-f(i-2 ,j,k)) &
                 + c*(f(i+2 ,j,k)-f(i-3 ,j,k)))
    end do
   end do
  end do
!
  nstag = nstag+1
  nflop = nflop+8
END FUNCTION

!***********************************************************************
FUNCTION ddxdn1 (f)
  use params
  real, dimension(mx,my,mz):: f, ddxdn1
!hpf$ distribute(*,*,block):: f, ddxdn1
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    ddxdn1 = 0.
    return
  end if
!
  a = 1./dx
!
!$omp parallel do private(i,j,k)
  do k=1,mz
   do j=1,my
    ddxdn1(1   ,j,k) = ( &
                   a*(f(1   ,j,k)-f(mx  ,j,k)))
   end do
   do j=1,my
    do i=2,mx
      ddxdn1(i,j,k) = ( &
                   a*(f(i   ,j,k)-f(i-1 ,j,k)))
    end do
   end do
  end do
!
  nstag1 = nstag1+1
  nflop = nflop+2
END FUNCTION
