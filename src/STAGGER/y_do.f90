!***********************************************************************
FUNCTION yup (f)
!
!  f is centered on (i,j-.5,k), fifth order stagger
!
  use params
  implicit none
  real a, b, c, a1st
  integer i, j, k
  real, intent(in) , dimension(mx,my,mz):: f
  real,              dimension(mx,my,mz):: yup
!hpf$ distribute(*,*,block):: f, yup
!-----------------------------------------------------------------------
!
  character(len=mid):: id = "$Id: y_do.f90,v 1.7 2007/01/13 16:05:45 aake Exp $" 
  call print_id (id)

  if (my .lt. 5) then
!2omp barrier
!$omp parallel private(k)
    do k=izs,ize
      yup(:,:,k) = f(:,:,k)
    end do
!$omp end parallel
!2omp barrier
    return
  end if
  c = 3./256.
  b = -25./256.
  a = .5-b-c
  a1st = 0.5
!
!2omp barrier
!$omp parallel private(i,j,k)
  do k=izs,ize
    do i=1,mx
      yup(i,my-2,k) = ( &
                     a*(f(i,my-2,k)+f(i,my-1,k)) + &
                     b*(f(i,my-3,k)+f(i,my  ,k)) + &
                     c*(f(i,my-4,k)+f(i,1   ,k)))
      yup(i,my-1,k) = ( &
                     a*(f(i,my-1,k)+f(i,my  ,k)) + &
                     b*(f(i,my-2,k)+f(i,1   ,k)) + &
                     c*(f(i,my-3,k)+f(i,2   ,k)))
      yup(i,my  ,k) = ( &
                     a*(f(i,my  ,k)+f(i,1   ,k)) + &
                     b*(f(i,my-1,k)+f(i,2   ,k)) + &
                     c*(f(i,my-2,k)+f(i,3   ,k)))
      yup(i,1   ,k) = ( &
                     a*(f(i,1   ,k)+f(i,2   ,k)) + &
                     b*(f(i,my  ,k)+f(i,3   ,k)) + &
                     c*(f(i,my-1,k)+f(i,4   ,k)))
      yup(i,2   ,k) = ( &
                     a*(f(i,2   ,k)+f(i,3   ,k)) + &
                     b*(f(i,1   ,k)+f(i,4   ,k)) + &
                     c*(f(i,my  ,k)+f(i,5   ,k)))
    end do
    do j=3,my-3
     do i=1,mx
      yup(i,j  ,k) = ( &
                     a*(f(i,j   ,k)+f(i,j+1 ,k)) + &
                     b*(f(i,j-1 ,k)+f(i,j+2 ,k)) + &
                     c*(f(i,j-2 ,k)+f(i,j+3 ,k)))
     end do
    end do
  end do
!$omp end parallel
!2omp barrier
!
  if (omp_master) then
    nstag = nstag+1
    nflop = nflop+8
  end if
END FUNCTION

!***********************************************************************
FUNCTION yup1 (f)
!
!  f is centered on (i,j-.5,k), fifth order stagger
!
  use params
  implicit none
  real a, b, c, a1st
  integer i, j, k
  real, intent(in) , dimension(mx,my,mz):: f
  real,              dimension(mx,my,mz):: yup1
!hpf$ distribute(*,*,block):: f, yup1
!-----------------------------------------------------------------------
!
  if (my .lt. 5) then
!2omp barrier
!$omp parallel private(k)
    do k=izs,ize
      yup1(:,:,k) = f(:,:,k)
    end do
!$omp end parallel
!2omp barrier
    return
  end if
!
  a = 0.5
!
!2omp barrier
!$omp parallel private(i,j,k)
  do k=izs,ize
    do i=1,mx
      yup1(i,my,k) = ( &
                     a*(f(i,my,k)+f(i,1,k)))
    end do
    do j=1,my-1
      do i=1,mx
        yup1(i,j  ,k) = ( &
                     a*(f(i,j   ,k)+f(i,j+1 ,k)))
      end do
    end do
  end do
!$omp end parallel
!2omp barrier
!
  if (omp_master) then
    nstag1 = nstag1+1
    nflop = nflop+2
  end if
END FUNCTION

!***********************************************************************
FUNCTION ydn (f)
!
!  f is centered on (i,j,k), fifth order stagger
!
  use params
  implicit none
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f, ydn
!hpf$ distribute(*,*,block):: f, ydn
!
!-----------------------------------------------------------------------
  if (my .lt. 5) then
!2omp barrier
!$omp parallel private(k)
    do k=izs,ize
      ydn(:,:,k) = f(:,:,k)
    end do
!$omp end parallel
!2omp barrier
    return
  end if
  c = 3./256.
  b = -25./256.
  a = .5-b-c
  a1st = 0.5
!
!2omp barrier
!$omp parallel private(i,j,k)
  do k=izs,ize
    do i=1,mx
      ydn(i,my-1,k) = ( &
                     a*(f(i,my-2,k)+f(i,my-1,k)) + &
                     b*(f(i,my-3,k)+f(i,my  ,k)) + &
                     c*(f(i,my-4,k)+f(i,1   ,k)))
      ydn(i,my  ,k) = ( &
                     a*(f(i,my-1,k)+f(i,my  ,k)) + &
                     b*(f(i,my-2,k)+f(i,1   ,k)) + &
                     c*(f(i,my-3,k)+f(i,2   ,k)))
      ydn(i,1   ,k) = ( &
                     a*(f(i,my  ,k)+f(i,1   ,k)) + &
                     b*(f(i,my-1,k)+f(i,2   ,k)) + &
                     c*(f(i,my-2,k)+f(i,3   ,k)))
      ydn(i,2   ,k) = ( &
                     a*(f(i,1   ,k)+f(i,2   ,k)) + &
                     b*(f(i,my  ,k)+f(i,3   ,k)) + &
                     c*(f(i,my-1,k)+f(i,4   ,k)))
      ydn(i,3   ,k) = ( &
                     a*(f(i,2   ,k)+f(i,3   ,k)) + &
                     b*(f(i,1   ,k)+f(i,4   ,k)) + &
                     c*(f(i,my  ,k)+f(i,5   ,k)))
    end do
    do j=4,my-2
     do i=1,mx
      ydn(i,j,k) = ( &
                     a*(f(i,j   ,k)+f(i,j-1 ,k)) + &
                     b*(f(i,j+1 ,k)+f(i,j-2 ,k)) + &
                     c*(f(i,j+2 ,k)+f(i,j-3 ,k)))
     end do
    end do
  end do
!$omp end parallel
!2omp barrier
!
  if (omp_master) then
    nstag = nstag+1
    nflop = nflop+8
  end if
END FUNCTION

!***********************************************************************
FUNCTION ydn_r8 (f) result(fp)
!
!  f is centered on (i,j,k), fifth order stagger
!
  use params
  implicit none
  real a, b, c, a1st
  integer i, j, k
  real(8), dimension(mx,my,mz):: f, fp
!hpf$ distribute(*,*,block):: f, ydn
!
!-----------------------------------------------------------------------
  if (my .lt. 5) then
!2omp barrier
!$omp parallel private(k)
    do k=izs,ize
      fp(:,:,k) = f(:,:,k)
    end do
!$omp end parallel
!2omp barrier
    return
  end if
  c = 3./256.
  b = -25./256.
  a = .5-b-c
  a1st = 0.5
!
!2omp barrier
!$omp parallel private(i,j,k)
  do k=izs,ize
    do i=1,mx
      fp(i,my-1,k) = ( &
                     a*(f(i,my-2,k)+f(i,my-1,k)) + &
                     b*(f(i,my-3,k)+f(i,my  ,k)) + &
                     c*(f(i,my-4,k)+f(i,1   ,k)))
      fp(i,my  ,k) = ( &
                     a*(f(i,my-1,k)+f(i,my  ,k)) + &
                     b*(f(i,my-2,k)+f(i,1   ,k)) + &
                     c*(f(i,my-3,k)+f(i,2   ,k)))
      fp(i,1   ,k) = ( &
                     a*(f(i,my  ,k)+f(i,1   ,k)) + &
                     b*(f(i,my-1,k)+f(i,2   ,k)) + &
                     c*(f(i,my-2,k)+f(i,3   ,k)))
      fp(i,2   ,k) = ( &
                     a*(f(i,1   ,k)+f(i,2   ,k)) + &
                     b*(f(i,my  ,k)+f(i,3   ,k)) + &
                     c*(f(i,my-1,k)+f(i,4   ,k)))
      fp(i,3   ,k) = ( &
                     a*(f(i,2   ,k)+f(i,3   ,k)) + &
                     b*(f(i,1   ,k)+f(i,4   ,k)) + &
                     c*(f(i,my  ,k)+f(i,5   ,k)))
    end do
    do j=4,my-2
     do i=1,mx
      fp(i,j,k) = ( &
                     a*(f(i,j   ,k)+f(i,j-1 ,k)) + &
                     b*(f(i,j+1 ,k)+f(i,j-2 ,k)) + &
                     c*(f(i,j+2 ,k)+f(i,j-3 ,k)))
     end do
    end do
  end do
!$omp end parallel
!2omp barrier
!
  if (omp_master) then
    nstag = nstag+1
    nflop = nflop+8
  end if
END FUNCTION

!***********************************************************************
FUNCTION ydn1 (f)
!
!  f is centered on (i,j,k), fifth order stagger
!
  use params
  implicit none
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f, ydn1
!hpf$ distribute(*,*,block):: f, ydn1
!
!-----------------------------------------------------------------------
  if (my .lt. 5) then
!2omp barrier
!$omp parallel private(k)
    do k=izs,ize
      ydn1(:,:,k) = f(:,:,k)
    end do
!$omp end parallel
!2omp barrier
    return
  end if
!
  a = 0.5
!
!2omp barrier
!$omp parallel private(i,j,k)
  do k=izs,ize
    do i=1,mx
      ydn1(i,1   ,k) = ( &
                     a*(f(i,my  ,k)+f(i,1   ,k)))
    end do
    do j=2,my
     do i=1,mx
      ydn1(i,j,k) = ( &
                     a*(f(i,j   ,k)+f(i,j-1 ,k)))
     end do
    end do
  end do
!$omp end parallel
!2omp barrier
!
  if (omp_master) then
    nstag1 = nstag1+1
    nflop = nflop+2
  end if
END FUNCTION

!***********************************************************************
FUNCTION ydn1_r8 (f) result (fp)
!
!  f is centered on (i,j,k), fifth order stagger
!
  use params
  implicit none
  real(8) a, b, c, a1st
  integer i, j, k
  real(8), dimension(mx,my,mz):: f, fp
!hpf$ distribute(*,*,block):: f, fp
!
!-----------------------------------------------------------------------
  if (my .lt. 5) then
!2omp barrier
!$omp parallel private(k)
    do k=izs,ize
      fp(:,:,k) = f(:,:,k)
    end do
!$omp end parallel
!2omp barrier
    return
  end if
!
  a = 0.5
!
!2omp barrier
!$omp parallel private(i,j,k)
  do k=izs,ize
    do i=1,mx
      fp(i,1   ,k) = (a*(f(i,my  ,k)+f(i,1   ,k)))
    end do
    do j=2,my
     do i=1,mx
      fp(i,j,k) = (a*(f(i,j   ,k)+f(i,j-1 ,k)))
     end do
    end do
  end do
!$omp end parallel
!2omp barrier
!
  if (omp_master) then
    nstag1 = nstag1+1
    nflop = nflop+2
  end if
END FUNCTION
