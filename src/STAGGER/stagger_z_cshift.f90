!;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
FUNCTION zup (f)
!
!  f is centered on (i,j,k-.5)
!
USE params
real, dimension(mx,my,mz):: f, zup
!hpf$ distribute(*,*,block):: f, zup
!-----------------------------------------------------------------------
  character(len=80):: id = "$Id: stagger_z_cshift.f90,v 1.2 2003/04/23 20:20:40 aake Exp $" 
  if (id.ne.' ') print '(1x,a)',id; id=' '
  c = 3./256.
  b = -25./256.
  a = .5-b-c
  zup = a*(f + cshift(f,dim=3,shift=1)) &
      + b*(cshift(f,dim=3,shift=-1) + cshift(f,dim=3,shift=2)) &
      + c*(cshift(f,dim=3,shift=-2) + cshift(f,dim=3,shift=3))
END FUNCTION
!;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
FUNCTION zdn (f)
!
!  f is centered on (i,j,k-.5)
!
USE params
real, dimension(mx,my,mz):: f, zdn
!hpf$ distribute(*,*,block):: f, zdn
!-----------------------------------------------------------------------
  c = 3./256.
  b = -25./256.
  a = .5-b-c
  zdn = a*(f + cshift(f,dim=3,shift=-1)) &
      + b*(cshift(f,dim=3,shift=1) + cshift(f,dim=3,shift=-2)) &
      + c*(cshift(f,dim=3,shift=2) + cshift(f,dim=3,shift=-3))
END FUNCTION
!;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
FUNCTION ddzup (f)
!
!  Z partial derivative
!
USE params
real, dimension(mx,my,mz):: f, ddzup
!hpf$ distribute(*,*,block):: f, ddzup
!-----------------------------------------------------------------------
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)/dz
  c = c/dz
  b = b/dz
  ddzup = a *(cshift(f,dim=3,shift=1) - f) &
  + b *(cshift(f,dim=3,shift=2) - cshift(f,dim=3,shift=-1)) &
  + c *(cshift(f,dim=3,shift=3) - cshift(f,dim=3,shift=-2))
END FUNCTION
!;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
FUNCTION ddzdn (f)
!
!  Z partial derivative
!
USE params
real, dimension(mx,my,mz):: f, ddzdn
!hpf$ distribute(*,*,block):: f, ddzdn
!-----------------------------------------------------------------------
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)/dz
  c = c/dz
  b = b/dz
  ddzdn = a*(f - cshift(f,dim=3,shift=-1)) &
      + b*(cshift(f,dim=3,shift=1) - cshift(f,dim=3,shift=-2)) &
      + c*(cshift(f,dim=3,shift=2) - cshift(f,dim=3,shift=-3))
END FUNCTION
