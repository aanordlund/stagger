! $Id: scale_do.f90,v 1.3 2004/05/26 18:05:43 aake Exp $

SUBROUTINE scale_ds (f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
  real ds
  integer iz

  ds = max(dx,dy,dz)
!$omp parallel do private(iz)
  do iz=1,mz
    f(:,:,iz) = ds*f(:,:,iz)
  end do
!Xomp end parallel
END SUBROUTINE

SUBROUTINE scale_ds1 (f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
  real ds
  integer iz

  ds = 1./min(dx,dy,dz)
!$omp parallel do private(iz)
  do iz=1,mz
    f(:,:,iz) = ds*f(:,:,iz)
  end do
!Xomp end parallel
END SUBROUTINE

SUBROUTINE scale_dx (f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
  integer iz

!$omp parallel do private(iz)
  do iz=1,mz
    f(:,:,iz) = dx*f(:,:,iz)
  end do
!Xomp end parallel
END SUBROUTINE

SUBROUTINE scale_dy (f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
  integer iz

!$omp parallel do private(iz)
  do iz=1,mz
    f(:,:,iz) = dy*f(:,:,iz)
  end do
!Xomp end parallel
END SUBROUTINE

SUBROUTINE scale_dz (f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
  integer iz

!$omp parallel do private(iz)
  do iz=1,mz
    f(:,:,iz) = dz*f(:,:,iz)
  end do
!Xomp end parallel
END SUBROUTINE

SUBROUTINE scale_dxy (f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
  real ds
  integer iz

  ds = 0.5*(dx+dy)
!$omp parallel do private(iz)
  do iz=1,mz
    f(:,:,iz) = ds*f(:,:,iz)
  end do
!Xomp end parallel
END SUBROUTINE

SUBROUTINE scale_dyz (f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
  real ds
  integer iz

  ds = 0.5*(dy+dz)
!$omp parallel do private(iz)
  do iz=1,mz
    f(:,:,iz) = ds*f(:,:,iz)
  end do
!Xomp end parallel
END SUBROUTINE

SUBROUTINE scale_dzx (f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
  real ds
  integer iz

  ds = 0.5*(dz+dx)
!$omp parallel do private(iz)
  do iz=1,mz
    f(:,:,iz) = ds*f(:,:,iz)
  end do
!Xomp end parallel
END SUBROUTINE
