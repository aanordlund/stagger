!***********************************************************************
FUNCTION ddxup (f)
  use params, h=>hx3, g=>gx2
  implicit none
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f, ddxup
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    ddxup = 0.
    return
  end if
!
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)/dx
  a1st = 1./dx
  b = b/dx
  c = c/dx

!$omp parallel private(i,j,k)
  call mpi_send_x_begin (f, g, 2, h, 3)
!
  do k=izs,ize
   do j=1,my
    do i=3,mx-3
      ddxup(i  ,j,k) = ( &
                 + c*(f(i+3 ,j,k)-f(i-2 ,j,k)) &
                 + b*(f(i+2 ,j,k)-f(i-1 ,j,k)) &
                 + a*(f(i+1 ,j,k)-f(i   ,j,k)))
    end do
   end do
  end do

  call mpi_send_x_end (f, g, 2, h, 3)

  do k=izs,ize
   do j=1,my
    ddxup(mx-2,j,k) = ( &
                 + c*(h(1   ,j,k)-f(mx-4,j,k)) &
                 + b*(f(mx  ,j,k)-f(mx-3,j,k)) &
                 + a*(f(mx-1,j,k)-f(mx-2,j,k)))
    ddxup(mx-1,j,k) = ( &
                 + c*(h(2   ,j,k)-f(mx-3,j,k)) &
                 + b*(h(1   ,j,k)-f(mx-2,j,k)) &
                 + a*(f(mx  ,j,k)-f(mx-1,j,k)))
    ddxup(mx  ,j,k) = ( &
                 + c*(h(3   ,j,k)-f(mx-2,j,k)) &
                 + b*(h(2   ,j,k)-f(mx-1,j,k)) &
                 + a*(h(1   ,j,k)-f(mx  ,j,k)))
    ddxup(1  ,j,k) = ( &
                 + c*(f(4   ,j,k)-g(1   ,j,k)) &
                 + b*(f(3   ,j,k)-g(2   ,j,k)) &
                 + a*(f(2   ,j,k)-f(1   ,j,k)))
    ddxup(2  ,j,k) = ( &
                 + c*(f(5   ,j,k)-g(2   ,j,k)) &
                 + b*(f(4   ,j,k)-f(1   ,j,k)) &
                 + a*(f(3   ,j,k)-f(2   ,j,k)))
   end do
  end do

  !$omp end parallel
!
  nstag = nstag+1
  nflop = nflop+8
END FUNCTION

!***********************************************************************
FUNCTION ddxup1 (f)
  use params, g=>gx1, h=>hx1
  implicit none
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f, ddxup1
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    ddxup1 = 0.
    return
  end if
  a = 1./dx

!$omp parallel private(i,j,k)
  call mpi_send_x_begin (f, g, 0, h, 1)

  do k=izs,ize
   do j=1,my
    do i=1,mx-1
      ddxup1(i  ,j,k) = ( &
                   a*(f(i+1,j,k)-f(i ,j,k)))
    end do
   end do
  end do

  call mpi_send_x_end (f, g, 0, h, 1)

  do k=izs,ize
   do j=1,my
    ddxup1(mx  ,j,k) = ( &
                   a*(h(1  ,j,k)-f(mx,j,k)))
   end do
  end do

!$omp end parallel
!2omp barrier
!
  nstag1 = nstag1+1
  nflop = nflop+2
END FUNCTION

!***********************************************************************
FUNCTION ddxdn (f)
  use params, g=>gx3, h=>hx2
  implicit none
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f, ddxdn
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    ddxdn = 0.
    return
  end if
!
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)/dx
  a1st = 1./dx
  b = b/dx
  c = c/dx

!$omp parallel private(i,j,k)
  call mpi_send_x_begin (f, g, 3, h, 2)

  do k=izs,ize
   do j=1,my
    do i=4,mx-2
      ddxdn(i,j,k) = ( &
                 + c*(f(i+2 ,j,k)-f(i-3 ,j,k)) &
                 + b*(f(i+1 ,j,k)-f(i-2 ,j,k)) &
                 + a*(f(i   ,j,k)-f(i-1 ,j,k)))
    end do
   end do
  end do

  call mpi_send_x_end (f, g, 3, h, 2)

  do k=izs,ize
   do j=1,my
    ddxdn(mx-1,j,k) = ( &
                 + c*(h(1   ,j,k)-f(mx-4,j,k)) &
                 + b*(f(mx  ,j,k)-f(mx-3,j,k)) &
                 + a*(f(mx-1,j,k)-f(mx-2,j,k)))
    ddxdn(mx  ,j,k) = ( &
                 + c*(h(2   ,j,k)-f(mx-3,j,k)) &
                 + b*(h(1   ,j,k)-f(mx-2,j,k)) &
                 + a*(f(mx  ,j,k)-f(mx-1,j,k)))
    ddxdn(1   ,j,k) = ( &
                 + c*(f(3   ,j,k)-g(1   ,j,k)) &
                 + b*(f(2   ,j,k)-g(2   ,j,k)) &
                 + a*(f(1   ,j,k)-g(3   ,j,k)))
    ddxdn(2  ,j,k) = ( &
                 + c*(f(4   ,j,k)-g(2   ,j,k)) &
                 + b*(f(3   ,j,k)-g(3   ,j,k)) &
                 + a*(f(2   ,j,k)-f(1   ,j,k)))
    ddxdn(3  ,j,k) = ( &
                 + c*(f(5   ,j,k)-g(3   ,j,k)) &
                 + b*(f(4   ,j,k)-f(1   ,j,k)) &
                 + a*(f(3   ,j,k)-f(2   ,j,k)))
   end do
  end do

!$omp end parallel
!
  nstag = nstag+1
  nflop = nflop+8
END FUNCTION

!***********************************************************************
FUNCTION ddxdn1 (f)
  use params, g=>gx1, h=>hx1
  implicit none
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f, ddxdn1
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    ddxdn1 = 0.
    return
  end if
  a = 1./dx

!$omp parallel private(i,j,k)
  call mpi_send_x_begin (f, g, 1, h, 0)

  do k=izs,ize
   do j=1,my
    do i=2,mx
      ddxdn1(i,j,k) = ( &
                   a*(f(i,j,k)-f(i-1,j,k)))
    end do
   end do
  end do

  call mpi_send_x_end (f, g, 1, h, 0)

  do k=izs,ize
   do j=1,my
    ddxdn1(1   ,j,k) = ( &
                   a*(f(1,j,k)-g(1  ,j,k)))
   end do
  end do

!$omp end parallel
!
  nstag1 = nstag1+1
  nflop = nflop+2
END FUNCTION
