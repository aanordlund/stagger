! $Id: stagger_do.f90,v 1.3 2001/12/05 21:54:00 aake Exp &
MODULE stagger
!***********************************************************************
!
!  6th order staggered derivatives, 5th order interpolation. This version
!  uses explicit loops, for increased speed and parallelization.
!
!***********************************************************************

CONTAINS


!***********************************************************************
FUNCTION laplace (f)
!
!  Laplace operator
!

  USE params
  implicit none
  logical omp_in_parallel
  real cx, cy, cz
  real, dimension(mx,my,mz):: f, laplace
  integer i, j, k, km1, kp1
!-----------------------------------------------------------------------

  cx=1./dx**2
  cy=1./dy**2
  cz=1./dz**2
  cx=1.
  cy=1.
  cz=1.
!$omp parallel private(i,j,k,km1,kp1) if (.not. omp_in_parallel())
  if (mx.gt.1) then
    do k=izs,ize
      do j=1,my
        do i=2,mx-1
          laplace(i,j,k) = cx*(f(i-1 ,j,k)+f(i+1,j,k)-2.*f(i,j,k))
        end do
        laplace(1 ,j,k)  = cx*(f(2,j,k)+f(mx  ,j,k)-2.*f(1 ,j,k))
        laplace(mx,j,k)  = cx*(f(1,j,k)+f(mx-1,j,k)-2.*f(mx,j,k))
      end do
    end do
  else
    do k=izs,ize
      do j=1,my
        do i=1,mx
          laplace(i,j,k) = 0.
        end do
      end do
    end do
  end if

 if (my .gt. 1) then
  do k=izs,ize
    do j=2,my-1
      do i=1,mx
        laplace(i,j,k) = laplace(i,j,k) + cy*(f(i,j+1,k)+f(i,j-1,k)-2.*f(i,j,k))
      end do
    end do
    do i=1,mx
        laplace(i,1 ,k) = laplace(i,1 ,k) + cy*(f(i,2,k)+f(i,my  ,k)-2.*f(i,1 ,k))
        laplace(i,my,k) = laplace(i,my,k) + cy*(f(i,1,k)+f(i,my-1,k)-2.*f(i,my,k))
    end do
  end do
 end if

 if (mz .gt. 1) then
!$omp barrier
  do k=izs,ize
    km1=mod(k-2+mz,mz)+1
    kp1=mod(k     ,mz)+1
    do j=1,my
      do i=1,mx
        laplace(i,j,k) = laplace(i,j,k) + cz*(f(i,j,kp1)+f(i,j,km1)-2.*f(i,j,k))
      end do
    end do
  end do
 end if
!$omp end parallel
END FUNCTION

!***********************************************************************
FUNCTION dif2a (f)
!
!  Max abs difference one zone left to one zones right.
!

  USE params
  implicit none
  real, dimension(mx,my,mz):: f, dif2a
!-----------------------------------------------------------------------
  call dif2a_set (f, dif2a)
END FUNCTION

!***********************************************************************
FUNCTION dif2 (fx, fy, fz)
!
!  Finite difference one zone left to one zones right.
!

  USE params
  implicit none
  real, dimension(mx,my,mz):: fx, fy, fz, dif2
!-----------------------------------------------------------------------
  call dif2_set (fx, fy, fz, dif2)
END FUNCTION

!***********************************************************************
FUNCTION dif2d (fx, fy, fz)
!
!  Finite difference one zone left to one zones right.
!

  USE params
  implicit none
  real, dimension(mx,my,mz):: fx, fy, fz, dif2d
!-----------------------------------------------------------------------
  call dif2d_set (fx, fy, fz, dif2d)
END FUNCTION

!***********************************************************************
FUNCTION dif1 (fx, fy, fz)
!
!  Finite difference one zone left to one zones right.
!

  USE params
  implicit none
  real, dimension(mx,my,mz):: fx, fy, fz, dif1
!-----------------------------------------------------------------------
  call dif1_set (fx, fy, fz, dif1)
END FUNCTION
!***********************************************************************
FUNCTION dif1d (fx, fy, fz)
!
!  Finite difference one zone left to one zones right.
!

  USE params
  implicit none
  real, dimension(mx,my,mz):: fx, fy, fz, dif1d
!-----------------------------------------------------------------------
  call dif1d_set (fx, fy, fz, dif1d)
END FUNCTION

!***********************************************************************
FUNCTION xup (f)
!
!  f is centered on (i-.5,j,k), fifth order stagger
!
  use params
  implicit none
  real, dimension(mx,my,mz):: f, xup
!-----------------------------------------------------------------------
  call xup_set (f, xup)
END FUNCTION

!***********************************************************************
FUNCTION xup1 (f)
!
!  f is centered on (i-.5,j,k), fifth order stagger
!
  use params
  implicit none
  real, dimension(mx,my,mz):: f, xup1
!-----------------------------------------------------------------------
  call xup1_set (f, xup1)
END FUNCTION

!***********************************************************************
FUNCTION xdn (f)
!
!  f is centered on (i,j,k), fifth order stagger
!
  use params
  implicit none
  real, dimension(mx,my,mz):: f, xdn
!-----------------------------------------------------------------------
  call xdn_set (f, xdn)
END FUNCTION

!***********************************************************************
FUNCTION xdn1 (f)
!
!  f is centered on (i,j,k), fifth order stagger
!
  use params
  implicit none
  real, dimension(mx,my,mz):: f, xdn1
!-----------------------------------------------------------------------
  call xdn1_set (f, xdn1)
END FUNCTION
!***********************************************************************
FUNCTION xup32 (f,j)
!
!  f is centered on (i-.5,j,k), fifth order stagger
!
  use params
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f
  real, dimension(mx,mz):: xup32
!hpf$ distribute(*,*,block):: f, xup32
!-----------------------------------------------------------------------
!
  character(len=mid):: id = "$Id: stagger_func.f90,v 1.2 2010/01/07 09:11:02 aake Exp $" 
  call print_id (id)
!
  if (mx.le.5) then
    xup32 = f(:,j,:)
    return
  end if
  c = 3./256.
  b = -25./256.
  a = .5-b-c
  a1st = 0.5
!
!$omp parallel private(i,j,k)
  do k=izs,ize
    xup32(mx-2,k) = ( &
                   a*(f(mx-2,j,k)+f(mx-1,j,k)) &
                 + b*(f(mx-3,j,k)+f(mx  ,j,k)) &
                 + c*(f(mx-4,j,k)+f(1   ,j,k)))
    xup32(mx-1,k) = ( &
                   a*(f(mx-1,j,k)+f(mx  ,j,k)) &
                 + b*(f(mx-2,j,k)+f(1   ,j,k)) &
                 + c*(f(mx-3,j,k)+f(2   ,j,k)))
    xup32(mx  ,k) = ( &
                   a*(f(mx  ,j,k)+f(1   ,j,k)) &
                 + b*(f(mx-1,j,k)+f(2   ,j,k)) &
                 + c*(f(mx-2,j,k)+f(3   ,j,k)))
    xup32(1   ,k) = ( &
                   a*(f(1   ,j,k)+f(2   ,j,k)) &
                 + b*(f(mx  ,j,k)+f(3   ,j,k)) &
                 + c*(f(mx-1,j,k)+f(4   ,j,k)))
    xup32(2   ,k) = ( &
                   a*(f(2   ,j,k)+f(3   ,j,k)) &
                 + b*(f(1   ,j,k)+f(4   ,j,k)) &
                 + c*(f(mx  ,j,k)+f(5   ,j,k)))
    do i=3,mx-3
      xup32(i  ,k) = ( &
                    a*(f(i  ,j,k)+f(i+1 ,j,k)) &
                  + b*(f(i-1,j,k)+f(i+2 ,j,k)) &
                  + c*(f(i-2,j,k)+f(i+3 ,j,k)))
    end do
  end do
!$omp end parallel
!
  if (omp_master) then
      nstag = nstag+1
      nflop = nflop+8
  end if
END FUNCTION

!***********************************************************************
FUNCTION xup132 (f,j)
!
!  f is centered on (i-.5,j,k), fifth order stagger
!
  use params
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f
  real, dimension(mx,mz):: xup132
!hpf$ distribute(*,*,block):: f, xup132
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    xup132 = f(:,j,:)
    return
  end if
!
  a = .5
!
!$omp parallel private(i,j,k)
  do k=izs,ize
    xup132(mx  ,k) = ( &
                   a*(f(mx  ,j,k)+f(1   ,j,k)))
    do i=1,mx-1
      xup132(i  ,k) = ( &
                    a*(f(i  ,j,k)+f(i+1 ,j,k)))
    end do
  end do
!$omp end parallel
!
  if (omp_master) then
    nstag1 = nstag1+1
    nflop = nflop+2
  end if
END FUNCTION

!***********************************************************************
FUNCTION xdn32 (f,j)
!
!  f is centered on (i,j,k), fifth order stagger
!
  use params
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f
  real, dimension(mx,mz):: xdn32
!hpf$ distribute(*,*,block):: f, xdn32
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    xdn32 = f(:,j,:)
    return
  end if
!
  c = 3./256.
  b = -25./256.
  a = .5-b-c
  a1st = 0.5
!
!$omp parallel private(i,j,k)
  do k=izs,ize
    xdn32(mx-1,k) = ( &
                   a*(f(mx-2,j,k)+f(mx-1,j,k)) &
                 + b*(f(mx-3,j,k)+f(mx  ,j,k)) &
                 + c*(f(mx-4,j,k)+f(1   ,j,k)))
    xdn32(mx  ,k) = ( &
                   a*(f(mx-1,j,k)+f(mx  ,j,k)) &
                 + b*(f(mx-2,j,k)+f(1   ,j,k)) &
                 + c*(f(mx-3,j,k)+f(2   ,j,k)))
    xdn32(1  ,k) = ( &
                   a*(f(mx  ,j,k)+f(1   ,j,k)) &
                 + b*(f(mx-1,j,k)+f(2   ,j,k)) &
                 + c*(f(mx-2,j,k)+f(3   ,j,k)))
    xdn32(2  ,k) = ( &
                   a*(f(1   ,j,k)+f(2   ,j,k)) &
                 + b*(f(mx  ,j,k)+f(3   ,j,k)) &
                 + c*(f(mx-1,j,k)+f(4   ,j,k)))
    xdn32(3  ,k) = ( &
                   a*(f(2   ,j,k)+f(3   ,j,k)) &
                 + b*(f(1   ,j,k)+f(4   ,j,k)) &
                 + c*(f(mx  ,j,k)+f(5   ,j,k)))
    do i=4,mx-2
      xdn32(i,k) = ( &
                   a*(f(i   ,j,k)+f(i-1 ,j,k)) &
                 + b*(f(i+1 ,j,k)+f(i-2 ,j,k)) &
                 + c*(f(i+2 ,j,k)+f(i-3 ,j,k)))
    end do
  end do
!$omp end parallel
!
!
  if (omp_master) then
    nstag = nstag+1
    nflop = nflop+8
  end if
END FUNCTION

!***********************************************************************
FUNCTION xdn132 (f,j)
!
!  f is centered on (i,j,k), fifth order stagger
!
  use params
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f
  real, dimension(mx,mz):: xdn132
!hpf$ distribute(*,*,block):: f, xdn132
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    xdn132 = f(:,j,:)
    return
  end if
!
  a = 0.5
!
!$omp parallel private(i,j,k)
  do k=izs,ize
    xdn132(1  ,k) = ( &
                   a*(f(mx  ,j,k)+f(1   ,j,k)))
    do i=2,mx
      xdn132(i,k) = ( &
                   a*(f(i   ,j,k)+f(i-1 ,j,k)))
    end do
  end do
!$omp end parallel
!
  if (omp_master) then
    nstag1 = nstag1+1
    nflop = nflop+2
  end if
END FUNCTION
!***********************************************************************
FUNCTION xup22 (f)
!
!  f is centered on (i-.5,k), fifth order stagger
!
  use params
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,mz):: xup22, f
!hpf$ distribute(*,*,block):: f, xup22
!-----------------------------------------------------------------------
!
  character(len=mid):: id = "$Id: stagger_func.f90,v 1.2 2010/01/07 09:11:02 aake Exp $" 
  call print_id (id)
!
  if (mx.le.5) then
    xup22 = f
    return
  end if
  c = 3./256.
  b = -25./256.
  a = .5-b-c
  a1st = 0.5
!
!$omp parallel private(i,k)
  do k=izs,ize
    xup22(mx-2,k) = ( &
                   a*(f(mx-2,k)+f(mx-1,k)) &
                 + b*(f(mx-3,k)+f(mx  ,k)) &
                 + c*(f(mx-4,k)+f(1   ,k)))
    xup22(mx-1,k) = ( &
                   a*(f(mx-1,k)+f(mx  ,k)) &
                 + b*(f(mx-2,k)+f(1   ,k)) &
                 + c*(f(mx-3,k)+f(2   ,k)))
    xup22(mx  ,k) = ( &
                   a*(f(mx  ,k)+f(1   ,k)) &
                 + b*(f(mx-1,k)+f(2   ,k)) &
                 + c*(f(mx-2,k)+f(3   ,k)))
    xup22(1   ,k) = ( &
                   a*(f(1   ,k)+f(2   ,k)) &
                 + b*(f(mx  ,k)+f(3   ,k)) &
                 + c*(f(mx-1,k)+f(4   ,k)))
    xup22(2   ,k) = ( &
                   a*(f(2   ,k)+f(3   ,k)) &
                 + b*(f(1   ,k)+f(4   ,k)) &
                 + c*(f(mx  ,k)+f(5   ,k)))
    do i=3,mx-3
      xup22(i  ,k) = ( &
                    a*(f(i  ,k)+f(i+1 ,k)) &
                  + b*(f(i-1,k)+f(i+2 ,k)) &
                  + c*(f(i-2,k)+f(i+3 ,k)))
    end do
  end do
!$omp end parallel
!
  if (omp_master) then
      nstag = nstag+1
      nflop = nflop+8
  end if
END FUNCTION

!***********************************************************************
FUNCTION xup122 (f)
!
!  f is centered on (i-.5,k), fifth order stagger
!
  use params
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,mz):: xup122, f
!hpf$ distribute(*,*,block):: f, xup122
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    xup122 = f
    return
  end if
!
  a = .5
!
!$omp parallel private(i,k)
  do k=izs,ize
    xup122(mx  ,k) = ( &
                   a*(f(mx  ,k)+f(1   ,k)))
    do i=1,mx-1
      xup122(i  ,k) = ( &
                    a*(f(i  ,k)+f(i+1 ,k)))
    end do
  end do
!$omp end parallel
!
  if (omp_master) then
    nstag1 = nstag1+1
    nflop = nflop+2
  end if
END FUNCTION

!***********************************************************************
FUNCTION xdn22 (f)
!
!  f is centered on (i,k), fifth order stagger
!
  use params
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,mz):: xdn22, f
!hpf$ distribute(*,*,block):: f, xdn22
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    xdn22 = f
    return
  end if
!
  c = 3./256.
  b = -25./256.
  a = .5-b-c
  a1st = 0.5
!
!$omp parallel private(i,k)
  do k=izs,ize
    xdn22(mx-1,k) = ( &
                   a*(f(mx-2,k)+f(mx-1,k)) &
                 + b*(f(mx-3,k)+f(mx  ,k)) &
                 + c*(f(mx-4,k)+f(1   ,k)))
    xdn22(mx  ,k) = ( &
                   a*(f(mx-1,k)+f(mx  ,k)) &
                 + b*(f(mx-2,k)+f(1   ,k)) &
                 + c*(f(mx-3,k)+f(2   ,k)))
    xdn22(1  ,k) = ( &
                   a*(f(mx  ,k)+f(1   ,k)) &
                 + b*(f(mx-1,k)+f(2   ,k)) &
                 + c*(f(mx-2,k)+f(3   ,k)))
    xdn22(2  ,k) = ( &
                   a*(f(1   ,k)+f(2   ,k)) &
                 + b*(f(mx  ,k)+f(3   ,k)) &
                 + c*(f(mx-1,k)+f(4   ,k)))
    xdn22(3  ,k) = ( &
                   a*(f(2   ,k)+f(3   ,k)) &
                 + b*(f(1   ,k)+f(4   ,k)) &
                 + c*(f(mx  ,k)+f(5   ,k)))
    do i=4,mx-2
      xdn22(i,k) = ( &
                   a*(f(i   ,k)+f(i-1 ,k)) &
                 + b*(f(i+1 ,k)+f(i-2 ,k)) &
                 + c*(f(i+2 ,k)+f(i-3 ,k)))
    end do
  end do
!$omp end parallel
!
!
  if (omp_master) then
    nstag = nstag+1
    nflop = nflop+8
  end if
END FUNCTION

!***********************************************************************
FUNCTION xdn122 (f)
!
!  f is centered on (i,k), fifth order stagger
!
  use params
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,mz):: xdn122, f
!hpf$ distribute(*,*,block):: f, xdn122
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    xdn122 = f
    return
  end if
!
  a = 0.5
!
!$omp parallel private(i,k)
  do k=izs,ize
    xdn122(1  ,k) = ( &
                   a*(f(mx  ,k)+f(1   ,k)))
    do i=2,mx
      xdn122(i,k) = ( &
                   a*(f(i   ,k)+f(i-1 ,k)))
    end do
  end do
!$omp end parallel
!
  if (omp_master) then
    nstag1 = nstag1+1
    nflop = nflop+2
  end if
END FUNCTION
!***********************************************************************
FUNCTION ddxupi (f)
  use params
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f, ddxupi
!hpf$ distribute(*,*,block):: f, ddxupi
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    ddxupi = 0.
    return
  end if
!
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)
  a1st = 1.
!
!$omp parallel private(i,j,k)
  do k=izs,ize
   do j=1,my
    ddxupi(mx-2,j,k) = ( &
                   a*(f(mx-1,j,k)-f(mx-2,j,k)) &
                 + b*(f(mx  ,j,k)-f(mx-3,j,k)) &
                 + c*(f(1   ,j,k)-f(mx-4,j,k)))
    ddxupi(mx-1,j,k) = ( &
                   a*(f(mx  ,j,k)-f(mx-1,j,k)) &
                 + b*(f(1   ,j,k)-f(mx-2,j,k)) &
                 + c*(f(2   ,j,k)-f(mx-3,j,k)))
    ddxupi(mx  ,j,k) = ( &
                   a*(f(1   ,j,k)-f(mx  ,j,k)) &
                 + b*(f(2   ,j,k)-f(mx-1,j,k)) &
                 + c*(f(3   ,j,k)-f(mx-2,j,k)))
    ddxupi(1  ,j,k) = ( &
                   a*(f(2   ,j,k)-f(1   ,j,k)) &
                 + b*(f(3   ,j,k)-f(mx  ,j,k)) &
                 + c*(f(4   ,j,k)-f(mx-1,j,k)))
    ddxupi(2  ,j,k) = ( &
                   a*(f(3   ,j,k)-f(2  ,j,k)) &
                 + b*(f(4   ,j,k)-f(1  ,j,k)) &
                 + c*(f(5   ,j,k)-f(mx ,j,k)))
   end do
   do j=1,my
    do i=3,mx-3
      ddxupi(i  ,j,k) = ( &
                   a*(f(i+1 ,j,k)-f(i   ,j,k)) &
                 + b*(f(i+2 ,j,k)-f(i-1 ,j,k)) &
                 + c*(f(i+3 ,j,k)-f(i-2 ,j,k)))
    end do
   end do
  end do
!$omp end parallel
!
  nstag = nstag+1
  nflop = nflop+8
END FUNCTION

!***********************************************************************
FUNCTION ddxupi1 (f)
  use params
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f, ddxupi1
!hpf$ distribute(*,*,block):: f, ddxupi1
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    ddxupi1 = 0.
    return
  end if
!
  a = 1.
!
!$omp parallel private(i,j,k)
  do k=izs,ize
   do j=1,my
    ddxupi1(mx  ,j,k) = ( &
                   a*(f(1   ,j,k)-f(mx  ,j,k)))
   end do
   do j=1,my
    do i=1,mx-1
      ddxupi1(i  ,j,k) = ( &
                   a*(f(i+1 ,j,k)-f(i   ,j,k)))
    end do
   end do
  end do
!$omp end parallel
!
  nstag1 = nstag1+1
  nflop = nflop+2
END FUNCTION

!***********************************************************************
FUNCTION ddxdni (f)
  use params
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f, ddxdni
!hpf$ distribute(*,*,block):: f, ddxdni
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    ddxdni = 0.
    return
  end if
!
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)
  a1st = 1.
!
!$omp parallel private(i,j,k)
  do k=izs,ize
   do j=1,my
    ddxdni(mx-1,j,k) = ( &
                   a*(f(mx-1,j,k)-f(mx-2,j,k)) &
                 + b*(f(mx  ,j,k)-f(mx-3,j,k)) &
                 + c*(f(1   ,j,k)-f(mx-4,j,k)))
    ddxdni(mx  ,j,k) = ( &
                   a*(f(mx  ,j,k)-f(mx-1,j,k)) &
                 + b*(f(1   ,j,k)-f(mx-2,j,k)) &
                 + c*(f(2   ,j,k)-f(mx-3,j,k)))
    ddxdni(1   ,j,k) = ( &
                   a*(f(1   ,j,k)-f(mx  ,j,k)) &
                 + b*(f(2   ,j,k)-f(mx-1,j,k)) &
                 + c*(f(3   ,j,k)-f(mx-2,j,k)))
    ddxdni(2  ,j,k) = ( &
                   a*(f(2   ,j,k)-f(1   ,j,k)) &
                 + b*(f(3   ,j,k)-f(mx  ,j,k)) &
                 + c*(f(4   ,j,k)-f(mx-1,j,k)))
    ddxdni(3  ,j,k) = ( &
                   a*(f(3   ,j,k)-f(2   ,j,k)) &
                 + b*(f(4   ,j,k)-f(1   ,j,k)) &
                 + c*(f(5   ,j,k)-f(mx  ,j,k)))
   end do
   do j=1,my
    do i=4,mx-2
      ddxdni(i,j,k) = ( &
                   a*(f(i   ,j,k)-f(i-1 ,j,k)) &
                 + b*(f(i+1 ,j,k)-f(i-2 ,j,k)) &
                 + c*(f(i+2 ,j,k)-f(i-3 ,j,k)))
    end do
   end do
  end do
!$omp end parallel
!
  nstag = nstag+1
  nflop = nflop+8
END FUNCTION

!***********************************************************************
FUNCTION ddxdni1 (f)
  use params
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f, ddxdni1
!hpf$ distribute(*,*,block):: f, ddxdni1
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    ddxdni1 = 0.
    return
  end if
!
  a = 1.
!
!$omp parallel private(i,j,k)
  do k=izs,ize
   do j=1,my
    ddxdni1(1   ,j,k) = ( &
                   a*(f(1   ,j,k)-f(mx  ,j,k)))
   end do
   do j=1,my
    do i=2,mx
      ddxdni1(i,j,k) = ( &
                   a*(f(i   ,j,k)-f(i-1 ,j,k)))
    end do
   end do
  end do
!$omp end parallel
!
  nstag1 = nstag1+1
  nflop = nflop+2
END FUNCTION
!***********************************************************************
FUNCTION ddxupi32 (f,j)
  use params
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f
  real, dimension(mx,mz):: ddxupi32
!hpf$ distribute(*,*,block):: f, ddxupi32
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    ddxupi32 = 0.
    return
  end if
!
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)
  a1st = 1.
!
!$omp parallel private(i,j,k)
  do k=izs,ize
    ddxupi32(mx-2,k) = ( &
                   a*(f(mx-1,j,k)-f(mx-2,j,k)) &
                 + b*(f(mx  ,j,k)-f(mx-3,j,k)) &
                 + c*(f(1   ,j,k)-f(mx-4,j,k)))
    ddxupi32(mx-1,k) = ( &
                   a*(f(mx  ,j,k)-f(mx-1,j,k)) &
                 + b*(f(1   ,j,k)-f(mx-2,j,k)) &
                 + c*(f(2   ,j,k)-f(mx-3,j,k)))
    ddxupi32(mx  ,k) = ( &
                   a*(f(1   ,j,k)-f(mx  ,j,k)) &
                 + b*(f(2   ,j,k)-f(mx-1,j,k)) &
                 + c*(f(3   ,j,k)-f(mx-2,j,k)))
    ddxupi32(1  ,k) = ( &
                   a*(f(2   ,j,k)-f(1   ,j,k)) &
                 + b*(f(3   ,j,k)-f(mx  ,j,k)) &
                 + c*(f(4   ,j,k)-f(mx-1,j,k)))
    ddxupi32(2  ,k) = ( &
                   a*(f(3   ,j,k)-f(2  ,j,k)) &
                 + b*(f(4   ,j,k)-f(1  ,j,k)) &
                 + c*(f(5   ,j,k)-f(mx ,j,k)))
    do i=3,mx-3
      ddxupi32(i  ,k) = ( &
                   a*(f(i+1 ,j,k)-f(i   ,j,k)) &
                 + b*(f(i+2 ,j,k)-f(i-1 ,j,k)) &
                 + c*(f(i+3 ,j,k)-f(i-2 ,j,k)))
    end do
  end do
!$omp end parallel
!
  nstag = nstag+1
  nflop = nflop+8
END FUNCTION

!***********************************************************************
FUNCTION ddxupi132 (f,j)
  use params
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f
  real, dimension(mx,mz):: ddxupi132
!hpf$ distribute(*,*,block):: f, ddxupi132
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    ddxupi132 = 0.
    return
  end if
!
  a = 1.
!
!$omp parallel private(i,j,k)
  do k=izs,ize
    ddxupi132(mx  ,k) = ( &
                   a*(f(1   ,j,k)-f(mx  ,j,k)))
    do i=1,mx-1
      ddxupi132(i  ,k) = ( &
                   a*(f(i+1 ,j,k)-f(i   ,j,k)))
    end do
  end do
!$omp end parallel
!
  nstag1 = nstag1+1
  nflop = nflop+2
END FUNCTION

!***********************************************************************
FUNCTION ddxdni32 (f,j)
  use params
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f
  real, dimension(mx,mz):: ddxdni32
!hpf$ distribute(*,*,block):: f, ddxdni32
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    ddxdni32 = 0.
    return
  end if
!
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)
  a1st = 1.
!
!$omp parallel private(i,j,k)
  do k=izs,ize
    ddxdni32(mx-1,k) = ( &
                   a*(f(mx-1,j,k)-f(mx-2,j,k)) &
                 + b*(f(mx  ,j,k)-f(mx-3,j,k)) &
                 + c*(f(1   ,j,k)-f(mx-4,j,k)))
    ddxdni32(mx  ,k) = ( &
                   a*(f(mx  ,j,k)-f(mx-1,j,k)) &
                 + b*(f(1   ,j,k)-f(mx-2,j,k)) &
                 + c*(f(2   ,j,k)-f(mx-3,j,k)))
    ddxdni32(1   ,k) = ( &
                   a*(f(1   ,j,k)-f(mx  ,j,k)) &
                 + b*(f(2   ,j,k)-f(mx-1,j,k)) &
                 + c*(f(3   ,j,k)-f(mx-2,j,k)))
    ddxdni32(2  ,k) = ( &
                   a*(f(2   ,j,k)-f(1   ,j,k)) &
                 + b*(f(3   ,j,k)-f(mx  ,j,k)) &
                 + c*(f(4   ,j,k)-f(mx-1,j,k)))
    ddxdni32(3  ,k) = ( &
                   a*(f(3   ,j,k)-f(2   ,j,k)) &
                 + b*(f(4   ,j,k)-f(1   ,j,k)) &
                 + c*(f(5   ,j,k)-f(mx  ,j,k)))
    do i=4,mx-2
      ddxdni32(i,k) = ( &
                   a*(f(i   ,j,k)-f(i-1 ,j,k)) &
                 + b*(f(i+1 ,j,k)-f(i-2 ,j,k)) &
                 + c*(f(i+2 ,j,k)-f(i-3 ,j,k)))
    end do
  end do
!$omp end parallel
!
  nstag = nstag+1
  nflop = nflop+8
END FUNCTION

!***********************************************************************
FUNCTION ddxdni132 (f,j)
  use params
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f
  real, dimension(mx,mz):: ddxdni132
!hpf$ distribute(*,*,block):: f, ddxdni132
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    ddxdni132 = 0.
    return
  end if
!
  a = 1.
!
!$omp parallel private(i,j,k)
  do k=izs,ize
    ddxdni132(1   ,k) = ( &
                   a*(f(1   ,j,k)-f(mx  ,j,k)))
    do i=2,mx
      ddxdni132(i,k) = ( &
                   a*(f(i   ,j,k)-f(i-1 ,j,k)))
    end do
  end do
!$omp end parallel
!
  nstag1 = nstag1+1
  nflop = nflop+2
END FUNCTION
!***********************************************************************
FUNCTION ddxupi22 (f)
  use params
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,mz):: ddxupi22, f
!hpf$ distribute(*,*,block):: f, ddxupi22
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    ddxupi22 = 0.
    return
  end if
!
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)
  a1st = 1.
!
!$omp parallel private(i,k)
  do k=izs,ize
    ddxupi22(mx-2,k) = ( &
                   a*(f(mx-1,k)-f(mx-2,k)) &
                 + b*(f(mx  ,k)-f(mx-3,k)) &
                 + c*(f(1   ,k)-f(mx-4,k)))
    ddxupi22(mx-1,k) = ( &
                   a*(f(mx  ,k)-f(mx-1,k)) &
                 + b*(f(1   ,k)-f(mx-2,k)) &
                 + c*(f(2   ,k)-f(mx-3,k)))
    ddxupi22(mx  ,k) = ( &
                   a*(f(1   ,k)-f(mx  ,k)) &
                 + b*(f(2   ,k)-f(mx-1,k)) &
                 + c*(f(3   ,k)-f(mx-2,k)))
    ddxupi22(1  ,k) = ( &
                   a*(f(2   ,k)-f(1   ,k)) &
                 + b*(f(3   ,k)-f(mx  ,k)) &
                 + c*(f(4   ,k)-f(mx-1,k)))
    ddxupi22(2  ,k) = ( &
                   a*(f(3   ,k)-f(2  ,k)) &
                 + b*(f(4   ,k)-f(1  ,k)) &
                 + c*(f(5   ,k)-f(mx ,k)))
    do i=3,mx-3
      ddxupi22(i  ,k) = ( &
                   a*(f(i+1 ,k)-f(i   ,k)) &
                 + b*(f(i+2 ,k)-f(i-1 ,k)) &
                 + c*(f(i+3 ,k)-f(i-2 ,k)))
    end do
  end do
!$omp end parallel
!
  nstag = nstag+1
  nflop = nflop+8
END FUNCTION

!***********************************************************************
FUNCTION ddxupi122 (f)
  use params
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,mz):: ddxupi122, f
!hpf$ distribute(*,*,block):: f, ddxupi122
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    ddxupi122 = 0.
    return
  end if
!
  a = 1.
!
!$omp parallel private(i,k)
  do k=izs,ize
    ddxupi122(mx  ,k) = ( &
                   a*(f(1   ,k)-f(mx  ,k)))
    do i=1,mx-1
      ddxupi122(i  ,k) = ( &
                   a*(f(i+1 ,k)-f(i   ,k)))
    end do
  end do
!$omp end parallel
!
  nstag1 = nstag1+1
  nflop = nflop+2
END FUNCTION

!***********************************************************************
FUNCTION ddxdni22 (f)
  use params
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,mz):: ddxdni22, f
!hpf$ distribute(*,*,block):: f, ddxdni22
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    ddxdni22 = 0.
    return
  end if
!
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)
  a1st = 1.
!
!$omp parallel private(i,k)
  do k=izs,ize
    ddxdni22(mx-1,k) = ( &
                   a*(f(mx-1,k)-f(mx-2,k)) &
                 + b*(f(mx  ,k)-f(mx-3,k)) &
                 + c*(f(1   ,k)-f(mx-4,k)))
    ddxdni22(mx  ,k) = ( &
                   a*(f(mx  ,k)-f(mx-1,k)) &
                 + b*(f(1   ,k)-f(mx-2,k)) &
                 + c*(f(2   ,k)-f(mx-3,k)))
    ddxdni22(1   ,k) = ( &
                   a*(f(1   ,k)-f(mx  ,k)) &
                 + b*(f(2   ,k)-f(mx-1,k)) &
                 + c*(f(3   ,k)-f(mx-2,k)))
    ddxdni22(2  ,k) = ( &
                   a*(f(2   ,k)-f(1   ,k)) &
                 + b*(f(3   ,k)-f(mx  ,k)) &
                 + c*(f(4   ,k)-f(mx-1,k)))
    ddxdni22(3  ,k) = ( &
                   a*(f(3   ,k)-f(2   ,k)) &
                 + b*(f(4   ,k)-f(1   ,k)) &
                 + c*(f(5   ,k)-f(mx  ,k)))
    do i=4,mx-2
      ddxdni22(i,k) = ( &
                   a*(f(i   ,k)-f(i-1 ,k)) &
                 + b*(f(i+1 ,k)-f(i-2 ,k)) &
                 + c*(f(i+2 ,k)-f(i-3 ,k)))
    end do
  end do
!$omp end parallel
!
  nstag = nstag+1
  nflop = nflop+8
END FUNCTION

!***********************************************************************
FUNCTION ddxdni122 (f)
  use params
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,mz):: ddxdni122, f
!hpf$ distribute(*,*,block):: f, ddxdni122
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    ddxdni122 = 0.
    return
  end if
!
  a = 1.
!
!$omp parallel private(i,k)
  do k=izs,ize
    ddxdni122(1   ,k) = ( &
                   a*(f(1   ,k)-f(mx  ,k)))
    do i=2,mx
      ddxdni122(i,k) = ( &
                   a*(f(i   ,k)-f(i-1 ,k)))
    end do
  end do
!$omp end parallel
!
  nstag1 = nstag1+1
  nflop = nflop+2
END FUNCTION
!***********************************************************************
FUNCTION ddxup (f)
  use params
  implicit none
  real, dimension(mx,my,mz):: f, ddxup
!-----------------------------------------------------------------------!
  call ddxup_set(f,ddxup)
END FUNCTION

!***********************************************************************
FUNCTION ddxup1 (f)
  use params
  implicit none
  real, dimension(mx,my,mz):: f, ddxup1
!-----------------------------------------------------------------------
  call ddxup1_set(f,ddxup1)
END FUNCTION

!***********************************************************************
FUNCTION ddxdn (f)
  use params
  implicit none
  real, dimension(mx,my,mz):: f, ddxdn
!-----------------------------------------------------------------------
  call ddxdn_set(f,ddxdn)
END FUNCTION

!***********************************************************************
FUNCTION ddxdn1 (f)
  use params
  implicit none
  real, dimension(mx,my,mz):: f, ddxdn1
!-----------------------------------------------------------------------
  call ddxdn1_set(f,ddxdn1)
END FUNCTION
!***********************************************************************
FUNCTION yup (f)
!
!  f is centered on (i,j-.5,k), fifth order stagger
!
  use params
  implicit none
  real, intent(in) , dimension(mx,my,mz):: f
  real,              dimension(mx,my,mz):: yup
!-----------------------------------------------------------------------
  call yup_set (f, yup)
END FUNCTION

!***********************************************************************
FUNCTION yup1 (f)
!
!  f is centered on (i,j-.5,k), fifth order stagger
!
  use params
  implicit none
  real, intent(in) , dimension(mx,my,mz):: f
  real,              dimension(mx,my,mz):: yup1
!-----------------------------------------------------------------------
  call yup1_set (f, yup1)
END FUNCTION

!***********************************************************************
FUNCTION ydn (f)
!
!  f is centered on (i,j,k), fifth order stagger
!
  use params
  implicit none
  real, dimension(mx,my,mz):: f, ydn
!-----------------------------------------------------------------------
  call ydn_set (f, ydn)
END FUNCTION

!***********************************************************************
FUNCTION ydn1 (f)
!
!  f is centered on (i,j,k), fifth order stagger
!
  use params
  implicit none
  real, dimension(mx,my,mz):: f, ydn1
!-----------------------------------------------------------------------
  call ydn1_set (f, ydn1)
END FUNCTION
!***********************************************************************
FUNCTION ddyupi (f)
  use params
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f, ddyupi
!hpf$ distribute(*,*,block):: f, ddyupi
!-----------------------------------------------------------------------
!
  if (my .lt. 3) then
    ddyupi = 0.
    return
  end if
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)
  a1st = 1.
!
!$omp parallel private(i,j,k)
  do k=izs,ize
    do i=1,mx
      ddyupi(i,my-2,k) = ( &
                     a*(f(i,my-1,k)-f(i,my-2,k)) + &
                     b*(f(i,my  ,k)-f(i,my-3,k)) + &
                     c*(f(i,1   ,k)-f(i,my-4,k)))
      ddyupi(i,my-1,k) = ( &
                     a*(f(i,my  ,k)-f(i,my-1,k)) + &
                     b*(f(i,1   ,k)-f(i,my-2,k)) + &
                     c*(f(i,2   ,k)-f(i,my-3,k)))
      ddyupi(i,my  ,k) = ( &
                     a*(f(i,1   ,k)-f(i,my  ,k)) + &
                     b*(f(i,2   ,k)-f(i,my-1,k)) + &
                     c*(f(i,3   ,k)-f(i,my-2,k)))
      ddyupi(i,1   ,k) = ( &
                     a*(f(i,2   ,k)-f(i,1   ,k)) + &
                     b*(f(i,3   ,k)-f(i,my  ,k)) + &
                     c*(f(i,4   ,k)-f(i,my-1,k)))
      ddyupi(i,2   ,k) = ( &
                     a*(f(i,3   ,k)-f(i,2   ,k)) + &
                     b*(f(i,4   ,k)-f(i,1   ,k)) + &
                     c*(f(i,5   ,k)-f(i,my  ,k)))
    end do
    do j=3,my-3
     do i=1,mx
      ddyupi(i,j  ,k) = ( &
                     a*(f(i,j+1 ,k)-f(i,j   ,k)) + &
                     b*(f(i,j+2 ,k)-f(i,j-1 ,k)) + &
                     c*(f(i,j+3 ,k)-f(i,j-2 ,k)))
     end do
    end do
  end do
!$omp end parallel
!
  nstag = nstag+1
  nflop = nflop+8
END FUNCTION

!***********************************************************************
FUNCTION ddyupi1 (f)
  use params
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f, ddyupi1
!hpf$ distribute(*,*,block):: f, ddyupi1
!-----------------------------------------------------------------------
!
  if (my .lt. 3) then
    ddyupi1 = 0.
    return
  end if
  a = 1.
!
!$omp parallel private(i,j,k)
  do k=izs,ize
    do i=1,mx
      ddyupi1(i,my,k) = ( &
                     a*(f(i,1  ,k)-f(i,my,k)))
    end do
    do j=1,my-1
      do i=1,mx
        ddyupi1(i,j  ,k) = ( &
                     a*(f(i,j+1 ,k)-f(i,j   ,k)))
     end do
    end do
  end do
!$omp end parallel
!
  nstag1 = nstag1+1
  nflop = nflop+2
END FUNCTION

!***********************************************************************
FUNCTION ddydni (f)
  use params
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f, ddydni
!hpf$ distribute(*,*,block):: f, ddydni
!-----------------------------------------------------------------------
!
  if (my .lt. 3) then
    ddydni = 0.
    return
  end if
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)
  a1st = 1.
!
!$omp parallel private(i,j,k)
  do k=izs,ize
    do i=1,mx
      ddydni(i,my-1,k) = ( &
                     a*(f(i,my-1,k)-f(i,my-2,k)) + &
                     b*(f(i,my  ,k)-f(i,my-3,k)) + &
                     c*(f(i,1   ,k)-f(i,my-4,k)))
      ddydni(i,my  ,k) = ( &
                     a*(f(i,my  ,k)-f(i,my-1,k)) + &
                     b*(f(i,1   ,k)-f(i,my-2,k)) + &
                     c*(f(i,2   ,k)-f(i,my-3,k)))
      ddydni(i,1  ,k) = ( &
                     a*(f(i,1   ,k)-f(i,my  ,k)) + &
                     b*(f(i,2   ,k)-f(i,my-1,k)) + &
                     c*(f(i,3   ,k)-f(i,my-2,k)))
      ddydni(i,2   ,k) = ( &
                     a*(f(i,2   ,k)-f(i,1   ,k)) + &
                     b*(f(i,3   ,k)-f(i,my  ,k)) + &
                     c*(f(i,4   ,k)-f(i,my-1,k)))
      ddydni(i,3   ,k) = ( &
                     a*(f(i,3   ,k)-f(i,2   ,k)) + &
                     b*(f(i,4   ,k)-f(i,1   ,k)) + &
                     c*(f(i,5   ,k)-f(i,my  ,k)))
    end do
    do j=4,my-2
     do i=1,mx
      ddydni(i,j ,k) = ( &
                     a*(f(i,j   ,k)-f(i,j-1 ,k)) + &
                     b*(f(i,j+1 ,k)-f(i,j-2 ,k)) + &
                     c*(f(i,j+2 ,k)-f(i,j-3 ,k)))
     end do
    end do
  end do
!$omp end parallel
!
!
  nstag = nstag+1
  nflop = nflop+8
END FUNCTION

!***********************************************************************
FUNCTION ddydni1 (f)
  use params
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f, ddydni1
!hpf$ distribute(*,*,block):: f, ddydni1
!-----------------------------------------------------------------------
!
  if (my .lt. 3) then
    ddydni1 = 0.
    return
  end if
  a = 1.
!
!$omp parallel private(i,j,k)
  do k=izs,ize
    do i=1,mx
      ddydni1(i,1  ,k) = ( &
                     a*(f(i,1   ,k)-f(i,my  ,k)))
    end do
    do j=2,my
     do i=1,mx
      ddydni1(i,j ,k) = ( &
                     a*(f(i,j   ,k)-f(i,j-1 ,k)))
     end do
    end do
  end do
!$omp end parallel
!
  nstag1 = nstag1+1
  nflop = nflop+2
END FUNCTION
!***********************************************************************
FUNCTION ddyup (f)
  use params
  implicit none
  real, dimension(mx,my,mz):: f, ddyup
!-----------------------------------------------------------------------
  call ddyup_set(f,ddyup)
END FUNCTION

!***********************************************************************
FUNCTION ddyup1 (f)
  use params
  implicit none
  real, dimension(mx,my,mz):: f, ddyup1
!-----------------------------------------------------------------------
  call ddyup1_set(f,ddyup1)
END FUNCTION

!***********************************************************************
FUNCTION ddydn (f)
  use params
  implicit none
  real, dimension(mx,my,mz):: f, ddydn
!-----------------------------------------------------------------------
  call ddydn_set(f,ddydn)
END FUNCTION

!***********************************************************************
FUNCTION ddydn1 (f)
  use params
  implicit none
  real, dimension(mx,my,mz):: f, ddydn1
!-----------------------------------------------------------------------
  call ddydn1_set(f,ddydn1)
END FUNCTION
!***********************************************************************
FUNCTION zup (f)
!
!  f is centered on (i,j,k-.5), fifth order stagger
!
  use params
  implicit none
  real, dimension(mx,my,mz):: f, zup
!-----------------------------------------------------------------------
  call zup_set (f, zup)
END FUNCTION

!***********************************************************************
FUNCTION zup1 (f)
!
!  f is centered on (i,j,k-.5), fifth order stagger
!
  use params
  implicit none
  real, dimension(mx,my,mz):: f, zup1
!-----------------------------------------------------------------------
  call zup1_set (f, zup1)
END FUNCTION

!***********************************************************************
FUNCTION zdn (f)
!
!  f is centered on (i,j,k), fifth order stagger
!
  use params
  implicit none
  real, dimension(mx,my,mz):: f, zdn
!-----------------------------------------------------------------------
  call zdn_set (f, zdn)
END FUNCTION

!***********************************************************************
FUNCTION zdn1 (f)
!
!  f is centered on (i,j,k), fifth order stagger
!
  use params
  implicit none
  real, dimension(mx,my,mz):: f, zdn1
!-----------------------------------------------------------------------
  call zdn1_set (f, zdn1)
END FUNCTION
!***********************************************************************
FUNCTION zup32 (f,j)
!
!  f is centered on (i,j,k-.5), fifth order stagger
!
  use params
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k, km2, km1, kp1, kp2, kp3
  real, dimension(mx,my,mz):: f
  real, dimension(mx,mz):: zup32
!hpf$ distribute(*,*,block):: f, zup32
!-----------------------------------------------------------------------
!
  character(len=mid):: id = "$Id: stagger_func.f90,v 1.2 2010/01/07 09:11:02 aake Exp $" 
  call print_id (id)
!
  if (mz.le.5) then
    zup32 = f(:,j,:)
    return
  end if
  c = 3./256.
  b = -25./256.
  a = .5-b-c
  a1st = 0.5
!
!Xomp barrier
!$omp parallel private(i,j,k,km2,km1,kp1,kp2,kp3)
  do k=izs,ize
    km2=mod(k-3+mz,mz)+1
    km1=mod(k-2+mz,mz)+1
    kp1=mod(k     ,mz)+1
    kp2=mod(k+1   ,mz)+1
    kp3=mod(k+2   ,mz)+1
      do i=1,mx
        zup32(i,k) = ( &
                     a*(f(i,j,kp1)+f(i,j,k  )) + &
                     b*(f(i,j,kp2)+f(i,j,km1)) + &
                     c*(f(i,j,kp3)+f(i,j,km2)))
      end do
  end do
!$omp end parallel
!1omp barrier
!
  if (omp_master) then
    nstag = nstag+1
    nflop = nflop+8
  end if
END FUNCTION

!***********************************************************************
FUNCTION zup132 (f,j)
!
!  f is centered on (i,j,k-.5), fifth order stagger
!
  use params
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k, kp1
  real, dimension(mx,my,mz):: f
  real, dimension(mx,mz):: zup132
!hpf$ distribute(*,*,block):: f, zup1
!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    zup132 = f(:,j,:)
    return
  end if
  a = .5
!
!Xomp barrier
!$omp parallel private(i,j,k,kp1)
  do k=izs,ize
    kp1=mod(k     ,mz)+1
      do i=1,mx
        zup132(i,k) = ( &
                     a*(f(i,j,kp1)+f(i,j,k  )))
      end do
  end do
!$omp end parallel
!1omp barrier
!
  if (omp_master) then
    nstag1 = nstag1+1
    nflop = nflop+2
  end if
END FUNCTION

!***********************************************************************
FUNCTION zdn32 (f,j)
!
!  f is centered on (i,j,k), fifth order stagger
!
  use params
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k, km3, km2, km1, kp1, kp2
  real, dimension(mx,my,mz):: f
  real, dimension(mx,mz):: zdn32
!hpf$ distribute(*,*,block):: f, zdn
!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    zdn32 = f(:,j,:)
    return
  end if
  c = 3./256.
  b = -25./256.
  a = .5-b-c
  a1st = 0.5
!
!Xomp barrier
!$omp parallel private(i,j,k,km3,km2,km1,kp1,kp2)
  do k=izs,ize
    km3=mod(k-4+mz,mz)+1
    km2=mod(k-3+mz,mz)+1
    km1=mod(k-2+mz,mz)+1
    kp1=mod(k     ,mz)+1
    kp2=mod(k+1   ,mz)+1
      do i=1,mx
        zdn32(i,k) = ( &
                     a*(f(i,j,k  )+f(i,j,km1)) + &
                     b*(f(i,j,kp1)+f(i,j,km2)) + &
                     c*(f(i,j,kp2)+f(i,j,km3)))
      end do
  end do
!$omp end parallel
!1omp barrier
!
  if (omp_master) then
    nstag = nstag+1
    nflop = nflop+8
  end if
END FUNCTION

!***********************************************************************
FUNCTION zdn132 (f,j)
!
!  f is centered on (i,j,k), fifth order stagger
!
  use params
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k, km1
  real, dimension(mx,my,mz):: f
  real, dimension(mx,mz):: zdn132
!hpf$ distribute(*,*,block):: f, zdn1
!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    zdn132 = f(:,j,:)
    return
  end if
  a = .5
!
!Xomp barrier
!$omp parallel private(i,j,k,km1)
  do k=izs,ize
    km1=mod(k-2+mz,mz)+1
      do i=1,mx
        zdn132(i,k) = ( &
                     a*(f(i,j,k  )+f(i,j,km1)))
      end do
  end do
!$omp end parallel
!1omp barrier
!
  if (omp_master) then
    nstag1 = nstag1+1
    nflop = nflop+2
  end if
END FUNCTION
!***********************************************************************
FUNCTION zup22 (f)
!
!  f is centered on (i,k-.5), fifth order stagger
!
  use params
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k, km2, km1, kp1, kp2, kp3
  real, dimension(mx,mz):: zup22, f
!hpf$ distribute(*,*,block):: f, zup
!-----------------------------------------------------------------------
!
  character(len=mid):: id = "$Id: stagger_func.f90,v 1.2 2010/01/07 09:11:02 aake Exp $" 
  call print_id (id)
!
  if (mz.le.5) then
    zup22 = f
    return
  end if
  c = 3./256.
  b = -25./256.
  a = .5-b-c
  a1st = 0.5
!
!Xomp barrier
!$omp parallel private(i,k,km2,km1,kp1,kp2,kp3)
  do k=izs,ize
    km2=mod(k-3+mz,mz)+1
    km1=mod(k-2+mz,mz)+1
    kp1=mod(k     ,mz)+1
    kp2=mod(k+1   ,mz)+1
    kp3=mod(k+2   ,mz)+1
      do i=1,mx
        zup22(i,k) = ( &
                     a*(f(i,kp1)+f(i,k  )) + &
                     b*(f(i,kp2)+f(i,km1)) + &
                     c*(f(i,kp3)+f(i,km2)))
      end do
  end do
!$omp end parallel
!1omp barrier
!
  if (omp_master) then
    nstag = nstag+1
    nflop = nflop+8
  end if
END FUNCTION

!***********************************************************************
FUNCTION zup122 (f)
!
!  f is centered on (i,k-.5), fifth order stagger
!
  use params
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k, kp1
  real, dimension(mx,mz):: zup122, f
!hpf$ distribute(*,*,block):: f, zup1
!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    zup122 = f
    return
  end if
  a = .5
!
!Xomp barrier
!$omp parallel private(i,k,kp1)
  do k=izs,ize
    kp1=mod(k     ,mz)+1
      do i=1,mx
        zup122(i,k) = ( &
                     a*(f(i,kp1)+f(i,k  )))
      end do
  end do
!$omp end parallel
!1omp barrier
!
  if (omp_master) then
    nstag1 = nstag1+1
    nflop = nflop+2
  end if
END FUNCTION

!***********************************************************************
FUNCTION zdn22 (f)
!
!  f is centered on (i,k), fifth order stagger
!
  use params
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k, km3, km2, km1, kp1, kp2
  real, dimension(mx,mz):: zdn22, f
!hpf$ distribute(*,*,block):: f, zdn
!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    zdn22 = f
    return
  end if
  c = 3./256.
  b = -25./256.
  a = .5-b-c
  a1st = 0.5
!
!Xomp barrier
!$omp parallel private(i,k,km3,km2,km1,kp1,kp2)
  do k=izs,ize
    km3=mod(k-4+mz,mz)+1
    km2=mod(k-3+mz,mz)+1
    km1=mod(k-2+mz,mz)+1
    kp1=mod(k     ,mz)+1
    kp2=mod(k+1   ,mz)+1
      do i=1,mx
        zdn22(i,k) = ( &
                     a*(f(i,k  )+f(i,km1)) + &
                     b*(f(i,kp1)+f(i,km2)) + &
                     c*(f(i,kp2)+f(i,km3)))
      end do
  end do
!$omp end parallel
!1omp barrier
!
  if (omp_master) then
    nstag = nstag+1
    nflop = nflop+8
  end if
END FUNCTION

!***********************************************************************
FUNCTION zdn122 (f)
!
!  f is centered on (i,k), fifth order stagger
!
  use params
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k, km1
  real, dimension(mx,mz):: zdn122, f
!hpf$ distribute(*,*,block):: f, zdn1
!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    zdn122 = f
    return
  end if
  a = .5
!
!Xomp barrier
!$omp parallel private(i,k,km1)
  do k=izs,ize
    km1=mod(k-2+mz,mz)+1
      do i=1,mx
        zdn122(i,k) = ( &
                     a*(f(i,k  )+f(i,km1)))
      end do
  end do
!$omp end parallel
!1omp barrier
!
  if (omp_master) then
    nstag1 = nstag1+1
    nflop = nflop+2
  end if
END FUNCTION
!***********************************************************************
FUNCTION ddzupi (f)
  use params
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k, km2, km1, kp1, kp2, kp3
  real, dimension(mx,my,mz):: f, ddzupi
!hpf$ distribute(*,*,block):: f, ddzupi
!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    ddzupi=0.
    return
  endif
!
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)
  a1st = 1.
!
!Xomp barrier
!$omp parallel private(i,j,k,km2,km1,kp1,kp2,kp3)
  do k=izs,ize
    km2=mod(k-3+mz,mz)+1
    km1=mod(k-2+mz,mz)+1
    kp1=mod(k     ,mz)+1
    kp2=mod(k+1   ,mz)+1
    kp3=mod(k+2   ,mz)+1
    do j=1,my
      do i=1,mx
        ddzupi(i,j,k) = ( &
                     a*(f(i,j,kp1)-f(i,j,k  )) + &
                     b*(f(i,j,kp2)-f(i,j,km1)) + &
                     c*(f(i,j,kp3)-f(i,j,km2)))
      end do
    end do
  end do
!$omp end parallel
!
  nstag = nstag+1
  nflop = nflop+8
END FUNCTION

!***********************************************************************
FUNCTION ddzupi1 (f)
  use params
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k, kp1
  real, dimension(mx,my,mz):: f, ddzupi1
!hpf$ distribute(*,*,block):: f, ddzupi1
!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    ddzupi1=0.
    return
  endif
!
  a = 1.
!
!Xomp barrier
!$omp parallel private(i,j,k,kp1)
  do k=izs,ize
    kp1=mod(k     ,mz)+1
    do j=1,my
      do i=1,mx
        ddzupi1(i,j,k) = ( &
                     a*(f(i,j,kp1)-f(i,j,k  )))
      end do
    end do
  end do
!$omp end parallel
!
  nstag1 = nstag1+1
  nflop = nflop+2
END FUNCTION

!***********************************************************************
FUNCTION ddzdni (f)
  use params
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k, km3, km2, km1, kp1, kp2
  real, dimension(mx,my,mz):: f, ddzdni
!hpf$ distribute(*,*,block):: f, ddzdni
!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    ddzdni=0.
    return
  endif
!
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)
  a1st = 1.
!
!Xomp barrier
!$omp parallel private(i,j,k,km3,km2,km1,kp1,kp2)
  do k=izs,ize
    km3=mod(k-4+mz,mz)+1
    km2=mod(k-3+mz,mz)+1
    km1=mod(k-2+mz,mz)+1
    kp1=mod(k     ,mz)+1
    kp2=mod(k+1   ,mz)+1
    do j=1,my
      do i=1,mx
        ddzdni(i,j,k) = ( &
                     a*(f(i,j,k  )-f(i,j,km1)) + &
                     b*(f(i,j,kp1)-f(i,j,km2)) + &
                     c*(f(i,j,kp2)-f(i,j,km3)))
      end do
    end do
  end do
!$omp end parallel
!
  nstag = nstag+1
  nflop = nflop+8
END FUNCTION

!***********************************************************************
FUNCTION ddzdni1 (f)
  use params
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k, km1
  real, dimension(mx,my,mz):: f, ddzdni1
!hpf$ distribute(*,*,block):: f, ddzdni1
!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    ddzdni1=0.
    return
  endif
!
  a = 1.
!
!Xomp barrier
!$omp parallel private(i,j,k,km1)
  do k=izs,ize
    km1=mod(k-2+mz,mz)+1
    do j=1,my
      do i=1,mx
        ddzdni1(i,j,k) = ( &
                     a*(f(i,j,k  )-f(i,j,km1)))
      end do
    end do
  end do
!$omp end parallel
!
  nstag1 = nstag1+1
  nflop = nflop+2
END FUNCTION
!***********************************************************************
FUNCTION ddzupi32 (f,j)
  use params
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k, km2, km1, kp1, kp2, kp3
  real, dimension(mx,my,mz):: f
  real, dimension(mx,mz):: ddzupi32
!hpf$ distribute(*,*,block):: f, ddzupi
!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    ddzupi32=0.
    return
  endif
!
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)
  a1st = 1.
!
!Xomp barrier
!$omp parallel private(i,j,k,km2,km1,kp1,kp2,kp3)
  do k=izs,ize
    km2=mod(k-3+mz,mz)+1
    km1=mod(k-2+mz,mz)+1
    kp1=mod(k     ,mz)+1
    kp2=mod(k+1   ,mz)+1
    kp3=mod(k+2   ,mz)+1
      do i=1,mx
        ddzupi32(i,k) = ( &
                     a*(f(i,j,kp1)-f(i,j,k  )) + &
                     b*(f(i,j,kp2)-f(i,j,km1)) + &
                     c*(f(i,j,kp3)-f(i,j,km2)))
      end do
  end do
!$omp end parallel
!
  nstag = nstag+1
  nflop = nflop+8
END FUNCTION

!***********************************************************************
FUNCTION ddzupi132 (f,j)
  use params
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k, kp1
  real, dimension(mx,my,mz):: f
  real, dimension(mx,mz):: ddzupi132
!hpf$ distribute(*,*,block):: f, ddzupi1
!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    ddzupi132=0.
    return
  endif
!
  a = 1.
!
!Xomp barrier
!$omp parallel private(i,j,k,kp1)
  do k=izs,ize
    kp1=mod(k     ,mz)+1
      do i=1,mx
        ddzupi132(i,k) = ( &
                     a*(f(i,j,kp1)-f(i,j,k  )))
      end do
  end do
!$omp end parallel
!
  nstag1 = nstag1+1
  nflop = nflop+2
END FUNCTION

!***********************************************************************
FUNCTION ddzdni32 (f,j)
  use params
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k, km3, km2, km1, kp1, kp2
  real, dimension(mx,my,mz):: f
  real, dimension(mx,mz):: ddzdni32
!hpf$ distribute(*,*,block):: f, ddzdni
!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    ddzdni32=0.
    return
  endif
!
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)
  a1st = 1.
!
!Xomp barrier
!$omp parallel private(i,j,k,km3,km2,km1,kp1,kp2)
  do k=izs,ize
    km3=mod(k-4+mz,mz)+1
    km2=mod(k-3+mz,mz)+1
    km1=mod(k-2+mz,mz)+1
    kp1=mod(k     ,mz)+1
    kp2=mod(k+1   ,mz)+1
      do i=1,mx
        ddzdni32(i,k) = ( &
                     a*(f(i,j,k  )-f(i,j,km1)) + &
                     b*(f(i,j,kp1)-f(i,j,km2)) + &
                     c*(f(i,j,kp2)-f(i,j,km3)))
      end do
  end do
!$omp end parallel
!
  nstag = nstag+1
  nflop = nflop+8
END FUNCTION

!***********************************************************************
FUNCTION ddzdni132 (f,j)
  use params
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k, km1
  real, dimension(mx,my,mz):: f
  real, dimension(mx,mz):: ddzdni132
!hpf$ distribute(*,*,block):: f, ddzdni1
!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    ddzdni132=0.
    return
  endif
!
  a = 1.
!
!Xomp barrier
!$omp parallel private(i,j,k,km1)
  do k=izs,ize
    km1=mod(k-2+mz,mz)+1
      do i=1,mx
        ddzdni132(i,k) = ( &
                     a*(f(i,j,k  )-f(i,j,km1)))
      end do
  end do
!$omp end parallel
!
  nstag1 = nstag1+1
  nflop = nflop+2
END FUNCTION
!***********************************************************************
FUNCTION ddzupi22 (f)
  use params
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k, km2, km1, kp1, kp2, kp3
  real, dimension(mx,mz):: ddzupi22, f
!hpf$ distribute(*,*,block):: f, ddzupi
!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    ddzupi22=0.
    return
  endif
!
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)
  a1st = 1.
!
!Xomp barrier
!$omp parallel private(i,j,k,km2,km1,kp1,kp2,kp3)
  do k=izs,ize
    km2=mod(k-3+mz,mz)+1
    km1=mod(k-2+mz,mz)+1
    kp1=mod(k     ,mz)+1
    kp2=mod(k+1   ,mz)+1
    kp3=mod(k+2   ,mz)+1
      do i=1,mx
        ddzupi22(i,k) = ( &
                     a*(f(i,kp1)-f(i,k  )) + &
                     b*(f(i,kp2)-f(i,km1)) + &
                     c*(f(i,kp3)-f(i,km2)))
      end do
  end do
!$omp end parallel
!
  nstag = nstag+1
  nflop = nflop+8
END FUNCTION

!***********************************************************************
FUNCTION ddzupi122 (f)
  use params
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k, kp1
  real, dimension(mx,mz):: ddzupi122, f
!hpf$ distribute(*,*,block):: f, ddzupi1
!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    ddzupi122=0.
    return
  endif
!
  a = 1.
!
!Xomp barrier
!$omp parallel private(i,k,kp1)
  do k=izs,ize
    kp1=mod(k     ,mz)+1
      do i=1,mx
        ddzupi122(i,k) = ( &
                     a*(f(i,kp1)-f(i,k  )))
      end do
  end do
!$omp end parallel
!
  nstag1 = nstag1+1
  nflop = nflop+2
END FUNCTION

!***********************************************************************
FUNCTION ddzdni22 (f)
  use params
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k, km3, km2, km1, kp1, kp2
  real, dimension(mx,mz):: ddzdni22, f
!hpf$ distribute(*,*,block):: f, ddzdni
!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    ddzdni22=0.
    return
  endif
!
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)
  a1st = 1.
!
!Xomp barrier
!$omp parallel private(i,k,km3,km2,km1,kp1,kp2)
  do k=izs,ize
    km3=mod(k-4+mz,mz)+1
    km2=mod(k-3+mz,mz)+1
    km1=mod(k-2+mz,mz)+1
    kp1=mod(k     ,mz)+1
    kp2=mod(k+1   ,mz)+1
      do i=1,mx
        ddzdni22(i,k) = ( &
                     a*(f(i,k  )-f(i,km1)) + &
                     b*(f(i,kp1)-f(i,km2)) + &
                     c*(f(i,kp2)-f(i,km3)))
      end do
  end do
!$omp end parallel
!
  nstag = nstag+1
  nflop = nflop+8
END FUNCTION

!***********************************************************************
FUNCTION ddzdni122 (f)
  use params
  implicit none
  logical omp_in_parallel
  real a, b, c, a1st
  integer i, j, k, km1
  real, dimension(mx,mz):: ddzdni122, f
!hpf$ distribute(*,*,block):: f, ddzdni1
!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    ddzdni122=0.
    return
  endif
!
  a = 1.
!
!Xomp barrier
!$omp parallel private(i,k,km1)
  do k=izs,ize
    km1=mod(k-2+mz,mz)+1
      do i=1,mx
        ddzdni122(i,k) = ( &
                     a*(f(i,k  )-f(i,km1)))
      end do
  end do
!$omp end parallel
!
  nstag1 = nstag1+1
  nflop = nflop+2
END FUNCTION
!***********************************************************************
FUNCTION ddzup (f)
  use params
  implicit none
  real, dimension(mx,my,mz):: f, ddzup
!-----------------------------------------------------------------------
  call ddzup_set(f,ddzup)
END FUNCTION

!***********************************************************************
FUNCTION ddzup1 (f)
  use params
  implicit none
  real, dimension(mx,my,mz):: f, ddzup1
!-----------------------------------------------------------------------
  call ddzup1_set(f,ddzup1)
END FUNCTION

!***********************************************************************
FUNCTION ddzdn (f)
  use params
  implicit none
  real, dimension(mx,my,mz):: f, ddzdn
!-----------------------------------------------------------------------
  call ddzdn_set(f,ddzdn)
END FUNCTION

!***********************************************************************
FUNCTION ddzdn1 (f)
  use params
  implicit none
  real, dimension(mx,my,mz):: f, ddzdn1
!-----------------------------------------------------------------------
  call ddzdn1_set(f,ddzdn1)
END FUNCTION

!***********************************************************************
FUNCTION smooth3 (f)
!
!  Three point smooth
!
  USE params
  implicit none
  real, dimension(mx,my,mz):: f, smooth3
!-----------------------------------------------------------------------
  call smooth3_set (f, smooth3)
END FUNCTION

!***********************************************************************
FUNCTION max3 (f)
!
!  Three point max
!
  USE params
  implicit none
  real, dimension(mx,my,mz):: f, max3
!-----------------------------------------------------------------------
  call max3_set (f, max3)
END FUNCTION

!***********************************************************************
FUNCTION max5 (f)
!
!  Three point max
!
  USE params
  implicit none
  real, dimension(mx,my,mz), intent(in) :: f
  real, dimension(mx,my,mz):: max5
!-----------------------------------------------------------------------
  call max5_set (f, max5)
END FUNCTION

!***********************************************************************
FUNCTION smooth3max5 (f)
!
!  Three point max
!
  USE params
  implicit none
  real, dimension(mx,my,mz) :: f, smooth3max5
!-----------------------------------------------------------------------
  smooth3max5 = smooth3(max5(f))
END FUNCTION

!***********************************************************************
FUNCTION smooth3max3 (f)
!
!  Three point max
!
  USE params
  implicit none
  real, dimension(mx,my,mz) :: f, smooth3max3
!-----------------------------------------------------------------------
  smooth3max3 = smooth3(max3(f))
END FUNCTION

END MODULE
