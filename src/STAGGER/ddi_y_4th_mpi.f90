!***********************************************************************
FUNCTION ddyupi (f)
  use params, g=>gy1, h=>gy2
  implicit none
  real a, b
  integer i, j, k
  real, dimension(mx,my,mz):: f, ddyupi
!-----------------------------------------------------------------------
!
  if (my .lt. 3) then
    ddyupi = 0.
    return
  end if
  b = -1./(24.*dy)
  a = 1./dy-3.*b

  call mpi_send_y (f, g, 1, h, 2)

  do k=izs,ize
    do i=1,mx
      ddyupi(i,my-1,k) = b*(h(i,1   ,k)-f(i,my-2,k)) + &
                         a*(f(i,my  ,k)-f(i,my-1,k))
      ddyupi(i,my  ,k) = b*(h(i,2   ,k)-f(i,my-1,k)) + &
                         a*(h(i,1   ,k)-f(i,my  ,k))
      ddyupi(i,1   ,k) = b*(f(i,3   ,k)-g(i,1   ,k)) + &
                         a*(f(i,2   ,k)-f(i,1   ,k))
    end do
    do j=2,my-2
     do i=1,mx
      ddyupi(i,j  ,k) = b*(f(i,j+2 ,k)-f(i,j-1 ,k)) + &
                        a*(f(i,j+1 ,k)-f(i,j   ,k))
     end do
    end do
  end do

  !$omp master
  nstag = nstag+1
  nflop = nflop+8
  !$omp end master
END FUNCTION

!***********************************************************************
FUNCTION ddyupi1 (f)
  use params, g=>gy1
  implicit none
  real a
  integer i, j, k
  real, dimension(mx,my,mz):: f, ddyupi1
!-----------------------------------------------------------------------
!
  if (my .lt. 3) then
    ddyupi1 = 0.
    return
  end if
  a = 1.

  call mpi_send_y (f, g, 0, g, 1)

  do k=izs,ize
    do i=1,mx
      ddyupi1(i,my,k) = ( &
                     a*(g(i,1  ,k)-f(i,my,k)))
    end do
    do j=1,my-1
      do i=1,mx
        ddyupi1(i,j  ,k) = ( &
                     a*(f(i,j+1 ,k)-f(i,j   ,k)))
     end do
    end do
  end do

  !$omp master
  nstag1 = nstag1+1
  nflop = nflop+2
  !$omp end master
END FUNCTION

!***********************************************************************
FUNCTION ddydni (f)
  use params, g=>gy2, h=>gy1
  implicit none
  real a, b
  integer i, j, k
  real, dimension(mx,my,mz):: f, ddydni
!-----------------------------------------------------------------------
!
  if (my .lt. 3) then
    ddydni = 0.
    return
  end if
  b = -1./(24.*dy)
  a = 1./dy-3.*b

  call mpi_send_y (f, g, 2, h, 1)

  do k=izs,ize
    do i=1,mx
      ddydni(i,my  ,k) = b*(h(i,1   ,k)-f(i,my-2,k)) + &
                         a*(f(i,my  ,k)-f(i,my-1,k))
      ddydni(i,1  ,k) =  b*(f(i,2   ,k)-g(i,1   ,k)) + &
                         a*(f(i,1   ,k)-g(i,2   ,k))
      ddydni(i,2   ,k) = b*(f(i,3   ,k)-g(i,2   ,k)) + &
                         a*(f(i,2   ,k)-f(i,1   ,k))
    end do
    do j=3,my-1
     do i=1,mx
      ddydni(i,j ,k) = b*(f(i,j+1 ,k)-f(i,j-2 ,k)) + &
                       a*(f(i,j   ,k)-f(i,j-1 ,k))
     end do
    end do
  end do

  !$omp master
  nstag = nstag+1
  nflop = nflop+8
  !$omp end master
END FUNCTION

!***********************************************************************
FUNCTION ddydni1 (f)
  use params, g=>gy1
  implicit none
  real a
  integer i, j, k
  real, dimension(mx,my,mz):: f, ddydni1
!-----------------------------------------------------------------------
!
  if (my .lt. 3) then
    ddydni1 = 0.
    return
  end if
  a = 1.

  call mpi_send_y (f, g, 1, g, 0)

  do k=izs,ize
    do i=1,mx
      ddydni1(i,1  ,k) = ( &
                     a*(f(i,1   ,k)-g(i,1   ,k)))
    end do
    do j=2,my
     do i=1,mx
      ddydni1(i,j ,k) = ( &
                     a*(f(i,j   ,k)-f(i,j-1 ,k)))
     end do
    end do
  end do

  !$omp master
  nstag1 = nstag1+1
  nflop = nflop+2
  !$omp end master
END FUNCTION
