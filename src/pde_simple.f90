! $Id: pde_simple.f90,v 1.7 2007/10/21 22:55:21 aake Exp $
!***********************************************************************
MODULE pdescr
!
!  Scratch variables for the MHD equations.  Allocating these only once
!  is much faster than allocating / deallocating every time step (4.3
!  musec/pt vs. 7.3 musec/pt on steno)!
!
  implicit none
  real, allocatable, dimension(:,:,:) :: &
    lnr,pp,U,B,cA,divU,nu,Ux,Uy,Uz,UxB,UyB,UzB,divUp,Ex,Ey,Ez,Jx,Jy,Jz, &
    Fx,Fy,Fz,Sxx,Syy,Szz,Sxy,Szx,Syz,Txx,Tyy,Tzz,Txy,Tzx,Tyz,eta,lnu,nuC,etaC, &
    xdnr,ydnr,zdnr,ee
!-----------------------------------------------------------------------
CONTAINS
SUBROUTINE allocate_scratch
  USE params
  implicit none
  allocate (lnr(mx,my,mz), pp(mx,my,mz), U(mx,my,mz), B(mx,my,mz))
  allocate (cA(mx,my,mz), divU(mx,my,mz), nu(mx,my,mz))
  allocate (Ux(mx,my,mz), Uy(mx,my,mz), Uz(mx,my,mz))
  allocate (UxB(mx,my,mz), UyB(mx,my,mz), UzB(mx,my,mz), divUp(mx,my,mz))
  allocate (Ex(mx,my,mz), Ey(mx,my,mz), Ez(mx,my,mz))
  allocate (Jx(mx,my,mz), Jy(mx,my,mz), Jz(mx,my,mz))
  allocate (Fx(mx,my,mz), Fy(mx,my,mz), Fz(mx,my,mz))
  allocate (Sxx(mx,my,mz), Syy(mx,my,mz), Szz(mx,my,mz))
  allocate (Sxy(mx,my,mz), Szx(mx,my,mz), Syz(mx,my,mz))
  allocate (Txx(mx,my,mz), Tyy(mx,my,mz), Tzz(mx,my,mz))
  allocate (Txy(mx,my,mz), Tzx(mx,my,mz), Tyz(mx,my,mz))
  allocate (eta(mx,my,mz), lnu(mx,my,mz), ee(mx,my,mz))
  allocate (nuC(mx,my,mz), etaC(mx,my,mz))
  allocate (xdnr(mx,my,mz), ydnr(mx,my,mz), zdnr(mx,my,mz))
  print*,'scratch allocated'
END SUBROUTINE
END MODULE

!***********************************************************************
SUBROUTINE pde(rho,px,py,pz,e,d,drdt,dpxdt,dpydt,dpzdt,dedt,dddt,flag, &
               Bx,By,Bz,dBxdt,dBydt,dBzdt)
!
!  The magneto-hydrodynamics equations.
!
!-----------------------------------------------------------------------
  USE params
  USE pdescr
  USE stagger
  USE eos
  implicit none
  logical flag
  real, dimension(mx,my,mz) :: &
       rho,px,py,pz,e,d,drdt,dpxdt,dpydt,dpzdt,dedt,dddt, &
       Bx,By,Bz,dBxdt,dBydt,dBzdt

  if (.not. allocated(lnr)) call allocate_scratch
!-----------------------------------------------------------------------
!  Equation of state 
!-----------------------------------------------------------------------
  ee = e/rho
  call equation_of_state(rho,ee,pp)
  e = ee*rho

!-----------------------------------------------------------------------
!  Velocities, using logarithmic interpolation of density
!-----------------------------------------------------------------------
  lnr = alog(rho)
  xdnr = exp(xdn(lnr))
  ydnr = exp(ydn(lnr))
  zdnr = exp(zdn(lnr))
  Ux = px/xdnr
  Uy = py/ydnr
  Uz = pz/zdnr

  Sxx = ddxup(Ux)
  Syy = ddyup(Uy)
  Szz = ddzup(Uz)

  U = sqrt(xup1(Ux)**2 + yup1(Uy)**2  + zup1(Uz)**2)
  divU = Sxx + Syy + Szz
  call symmetric_center (U)
  call symmetric_center (divU)

  if (do_mhd) then
    B = sqrt(xup1(Bx)**2 + yup1(By)**2  + zup1(Bz)**2)
    call symmetric_center (B)
    cA = sqrt((B**2 + gamma*pp)/rho)
    UxB = Ux*Bx/xdn(B)
    UyB = Uy*By/ydn(B)
    UzB = Uz*Bz/zdn(B)
    divUp = divU - ddxup(UxB) - ddyup(UyB) - ddzup(UzB)
    call symmetric_center (divUp)
    eta = (nu1*max(dx,dy,dz))*(U+cA) + nu2*max(dx,dy,dz)**2*max(-divUp,0.0)
    eta = smooth3(max5(eta))
    call symmetric_center (eta)
    Cn = (3.*6.3/2.)*maxval(eta)*dt/min(dx,dy,dz)**2
  else
    cA = sqrt(gamma*pp/rho)
    Cn = 0.
  end if

!-----------------------------------------------------------------------
!  Numerical diffusion, Richtmyer & Morton type
!-----------------------------------------------------------------------
  nu  = max(dx,dy,dz)*(nu1*U+nu3*cA)
  Cu  = maxval(cA+U)*dt/min(dx,dy,dz)

  if (nu2 > 0) then
    if (do_mhd) then
      UxB = Ux*Bx/xdn1(B)
      UyB = Uy*By/ydn1(B)
      UzB = Uz*Bz/zdn1(B)
      divUp = divU - ddxup(UxB) - ddyup(UyB) - ddzup(UzB)
      call symmetric_center (divUp)
      etaC = eta + (nu2*max(dx,dy,dz)**2)*smooth3(max(-divUp,0.0))
      call symmetric_center (eta)
      call symmetric_center (etaC)
    end if

    !nuC  = nu  + (nu2*max(dx,dy,dz)**2)*smooth3(max(-divU ,0.0))
    nuC  = nu  + (nu2*max(dx,dy,dz)**2)*smooth3(max3(max(-divU ,0.0)))
    nu = nuC

    call symmetric_center (nu)
    call symmetric_center (nuC)
  end if
  Cv  = (6.2*3./2.)*maxval(nuC)*dt/min(dx,dy,dz)**2
  lnu = alog(max(nu,1e-35)) + lnr
  Cv = (3.*6.3/2.)*maxval(nu )*dt/min(dx,dy,dz)**2

!-----------------------------------------------------------------------
!  Mass conservation, cell centered
!-----------------------------------------------------------------------
  drdt = drdt - ddxup1(px) - ddyup1(py) - ddzup1(pz)

!-----------------------------------------------------------------------
!  Viscous stress, diagonal and off-diagonal elements
!-----------------------------------------------------------------------
  !Txx = - rho*(nu*(Sxx-0.3333333*divU) + nuC*divU)
  !Tyy = - rho*(nu*(Syy-0.3333333*divU) + nuC*divU)
  !Tzz = - rho*(nu*(Szz-0.3333333*divU) + nuC*divU)
  Txx = - rho*nuC*Sxx
  Tyy = - rho*nuC*Syy
  Tzz = - rho*nuC*Szz

  Sxy = (ddxdn(Uy)+ddydn(Ux))*0.5
  Syz = (ddydn(Uz)+ddzdn(Uy))*0.5
  Szx = (ddzdn(Ux)+ddxdn(Uz))*0.5
  Txy = - exp(xdn(ydn(lnu)))*Sxy
  Tyz = - exp(ydn(zdn(lnu)))*Syz
  Tzx = - exp(zdn(xdn(lnu)))*Szx

!-----------------------------------------------------------------------
!  Viscous dissipation
!-----------------------------------------------------------------------
  if (do_dissipation) dedt = dedt &
       - Txx*Sxx &
       - Tyy*Syy &
       - Tzz*Szz &
       - 2.*(xup1(yup1(Txy*Sxy))  &
           + yup1(zup1(Tyz*Syz))  &
           + zup1(xup1(Tzx*Szx)))

!-----------------------------------------------------------------------
!  Add pressure and Reynolds stress
!-----------------------------------------------------------------------
  Txx = Txx + pp + rho*xup(Ux)**2
  Tyy = Tyy + pp + rho*yup(Uy)**2
  Tzz = Tzz + pp + rho*zup(Uz)**2
  Txy = Txy + exp(xdn1(ydn1(lnr)))*ydn1(Ux)*xdn1(Uy)
  Tyz = Tyz + exp(ydn1(zdn1(lnr)))*zdn1(Uy)*ydn1(Uz)
  Tzx = Tzx + exp(zdn1(xdn1(lnr)))*xdn1(Uz)*zdn1(Ux)

  call symmetric_center (Txx)
  call symmetric_face (Txy)
  call symmetric_face (Tzx)

!-----------------------------------------------------------------------
!  Equations of motion, face centred
!-----------------------------------------------------------------------
  dpxdt = dpxdt - ddxdn1(Txx) - ddyup1(Txy) - ddzup1(Tzx)
  dpydt = dpydt - ddydn1(Tyy) - ddzup1(Tyz) - ddxup1(Txy)
  dpzdt = dpzdt - ddzdn1(Tzz) - ddxup1(Tzx) - ddyup1(Tyz)
  call forceit (rho,Ux,Uy,Uz,xdnr,ydnr,zdnr,dpxdt,dpydt,dpzdt)

  if (do_mhd) then
!-----------------------------------------------------------------------
!  Electric current j = curl(B), edge centered
!-----------------------------------------------------------------------
    Jx = ddydn(Bz) - ddzdn(By)
    Jy = ddzdn(Bx) - ddxdn(Bz)
    Jz = ddxdn(By) - ddydn(Bx)

    call symmetric_face (Jy)
    call symmetric_face (Jz)

!-----------------------------------------------------------------------
!  Lorentz force
!-----------------------------------------------------------------------
  dpxdt = dpxdt + Fx
  dpydt = dpydt + Fy
  dpzdt = dpzdt + Fz

!-----------------------------------------------------------------------
!  Electric field   E = eta j - (u x B), edge centered
!-----------------------------------------------------------------------
    Ex = ydn1(zdn1(eta))*Jx - zdn(Uy)*ydn(Bz) + ydn(Uz)*zdn(By)
    Ey = zdn1(xdn1(eta))*Jy - xdn(Uz)*zdn(Bx) + zdn(Ux)*xdn(Bz)
    Ez = xdn1(ydn1(eta))*Jz - ydn(Ux)*xdn(By) + xdn(Uy)*ydn(Bx)

  !call E_driver (Ex, Ez)

!-----------------------------------------------------------------------
!  Magnetic field's time derivative, dBdt = - curl(E), face centered
!-----------------------------------------------------------------------
  dBxdt = dBxdt + ddzup(Ey) - ddyup(Ez)
  dBydt = dBydt + ddxup(Ez) - ddzup(Ex)
  dBzdt = dBzdt + ddyup(Ex) - ddxup(Ey)

!-----------------------------------------------------------------------
!  Energy equation, including Joule dissipation
!-----------------------------------------------------------------------
if (do_energy) then
  dedt = dedt &
       + eta*(yup1(zup1(Jx**2))  &
            + zup1(xup1(Jy**2))  &
            + xup1(yup1(Jz**2)))
end if ! (do_energy)

end if ! (do_mhd)

!-----------------------------------------------------------------------
!  Energy equation, including Joule dissipation
!-----------------------------------------------------------------------
if (do_energy) then
  dedt = dedt &
       - ddxup (exp(xdn(alog(e)))*Ux) &
       - ddyup (exp(ydn(alog(e)))*Uy) &
       - ddzup (exp(zdn(alog(e)))*Uz) &
       - pp*divU
end if

  call ddt_boundary (rho,px,py,pz,e,Bx,By,Bz, &
                     drdt,dpxdt,dpydt,dpzdt, &
                     dedt,dBxdt,dBydt,dBzdt)
END
