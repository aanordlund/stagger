! $Id: hyman_mixed.f90,v 1.3 2012/01/20 14:59:08 aake Exp $
!************************************************************************
MODULE timeintegration
  implicit none

  real, dimension(3):: alpha, beta
  real, allocatable, dimension(:,:,:) :: rold, pxold, pyold, pzold, &
                                         eold, Bxold, Byold, Bzold, &
                                         dold
  real, allocatable, dimension(:,:,:) :: rsav, pxsav, pysav, pzsav, &
                                         esav, Bxsav, Bysav, Bzsav, &
                                         dsav
END MODULE timeintegration

!------------------------------------------------------------------------
SUBROUTINE init_timestep
  USE params
  USE timeintegration

  if (nstep<=3) timeorder=1
  if (master) print *,'init_timestep',do_density,do_momenta, do_energy, do_pscalar, do_mhd
  
  if (do_density) then
    allocate (rold(mx,my,mz))
    allocate (rsav(mx,my,mz))
  end if
  if (do_momenta) then
    allocate (pxold(mx,my,mz))
    allocate (pyold(mx,my,mz))
    allocate (pzold(mx,my,mz))
    allocate (pxsav(mx,my,mz))
    allocate (pysav(mx,my,mz))
    allocate (pzsav(mx,my,mz))
  end if
  if (do_energy) then
    allocate (eold(mx,my,mz))
    allocate (esav(mx,my,mz))
  end if
  if (do_pscalar) then
    allocate (dold(mx,my,mz))
    allocate (dsav(mx,my,mz))
  end if
  if (do_mhd) then
    allocate (Bxold(mx,my,mz))
    allocate (Byold(mx,my,mz))
    allocate (Bzold(mx,my,mz))
    allocate (Bxsav(mx,my,mz))
    allocate (Bysav(mx,my,mz))
    allocate (Bzsav(mx,my,mz))
  end if
END SUBROUTINE

!------------------------------------------------------------------------
SUBROUTINE timestep(r,px,py,pz,e,d,drdt,dpxdt,dpydt,dpzdt,dedt,dddt, &
                      Bx,By,Bz,dBxdt,dBydt,dBzdt)
!
!  3rd order, 2-N storage Runge-Kutta.  Advances the eight MHD variables
!  forward in time, given time derivatives computed in the subroutine pde.
!
  USE params
  USE timeintegration
  implicit none

  integer nx, ny, nz, iz, iy, ix
  real, dimension(mx,my,mz) :: &
        r,px,py,pz,e,d, &
        drdt,dpxdt,dpydt,dpzdt,dedt,dddt, &
        Bx,By,Bz,dBxdt,dBydt,dBzdt
  real :: drtdt
  real :: a1, a2, a3, b1, b2, c2, dtr, f
  logical debug
  character(len=mid):: id="$Id: hyman_mixed.f90,v 1.3 2012/01/20 14:59:08 aake Exp $"

  call print_id (id)

!------------------------------------------------------------------------

  f = 2./3.                                                              ! timestep reduction

  !$omp parallel private(iz)
  do iz=izs,ize
    if (do_density) drdt(:,:,iz)  = 0.
    if (do_energy)  dedt(:,:,iz)  = 0.
    if (do_pscalar) dddt(:,:,iz)  = 0.
    if (do_momenta) then
		    dpxdt(:,:,iz) = 0.
		    dpydt(:,:,iz) = 0.
		    dpzdt(:,:,iz) = 0.
    end if
    if (do_mhd) then
		    dBxdt(:,:,iz) = 0.
		    dBydt(:,:,iz) = 0.
		    dBzdt(:,:,iz) = 0.
    end if
  end do
  !$omp end parallel

  isubstep = 1
  call pde(.true.)

  !$omp parallel
  call courant
  !$omp end parallel

  if (it==1 .or. timeorder==1) then
    !$omp parallel private(iz)
    do iz=izs,ize
      if (do_density) rold(:,:,iz)  = r(:,:,iz)
      if (do_energy)  eold(:,:,iz)  = e(:,:,iz)
      if (do_pscalar) dold(:,:,iz)  = d(:,:,iz)
      if (do_momenta) then
		      pxold(:,:,iz) = px(:,:,iz)
		      pyold(:,:,iz) = py(:,:,iz)
		      pzold(:,:,iz) = pz(:,:,iz)
      end if
      if (do_mhd) then
		      Bxold(:,:,iz) = Bx(:,:,iz)
		      Byold(:,:,iz) = By(:,:,iz)
		      Bzold(:,:,iz) = Bz(:,:,iz)
      end if
      if (do_density) r(:,:,iz)  =  r(:,:,iz) + dt* drdt(:,:,iz)
      if (do_energy)  e(:,:,iz)  =  e(:,:,iz) + dt* dedt(:,:,iz)
      if (do_pscalar) d(:,:,iz)  =  d(:,:,iz) + dt* dddt(:,:,iz)
      if (do_momenta) then
		      px(:,:,iz) = px(:,:,iz) + dt*dpxdt(:,:,iz)
		      py(:,:,iz) = py(:,:,iz) + dt*dpydt(:,:,iz)
		      pz(:,:,iz) = pz(:,:,iz) + dt*dpzdt(:,:,iz)
      end if
      if (do_mhd) then
		      Bx(:,:,iz) = Bx(:,:,iz) + dt*dBxdt(:,:,iz)
		      By(:,:,iz) = By(:,:,iz) + dt*dBydt(:,:,iz)
		      Bz(:,:,iz) = Bz(:,:,iz) + dt*dBzdt(:,:,iz)
      end if
    end do
    !$omp end parallel
    td= td + dt
    dtold = dt

  else
    dtr = dt/dtold
    a1 = dtr**2
    a2 = 2.*(1.+dtr   )/(2.+3.*dtr)
    b1 = dt*(1.+dtr   )
    b2 = dt*(1.+dtr**2)/(2.+3.*dtr)
    c2 = dt*(1.+dtr   )/(2.+3.*dtr)

    !$omp parallel private(iz)
    do iz=izs,ize
      if (do_density) then
	  rsav(:,:,iz) =       r(:,:,iz)
	  r   (:,:,iz) = a1*rold(:,:,iz) + (1.-a1)*r(:,:,iz) + b1*drdt(:,:,iz)
	  rold(:,:,iz) =    rsav(:,:,iz)
	  rsav(:,:,iz) = a2*rold(:,:,iz) + (1.-a2)*r(:,:,iz) + b2*drdt(:,:,iz)
	  drdt(:,:,iz) = 0.
      end if
      if (do_energy) then
	  esav(:,:,iz) =       e(:,:,iz)
	  e   (:,:,iz) = a1*eold(:,:,iz) + (1.-a1)*e(:,:,iz) + b1*dedt(:,:,iz)
	  eold(:,:,iz) =    esav(:,:,iz)
	  esav(:,:,iz) = a2*eold(:,:,iz) + (1.-a2)*e(:,:,iz) + b2*dedt(:,:,iz)
	  dedt(:,:,iz) = 0.
      end if
      if (do_pscalar) then
	  dsav(:,:,iz) =       d(:,:,iz)
	  d   (:,:,iz) = a1*dold(:,:,iz) + (1.-a1)*d(:,:,iz) + b1*dddt(:,:,iz)
	  dold(:,:,iz) =    dsav(:,:,iz)
	  dsav(:,:,iz) = a2*dold(:,:,iz) + (1.-a2)*d(:,:,iz) + b2*dddt(:,:,iz)
	  dddt(:,:,iz) = 0.
      end if
      if (do_momenta) then
	  pxsav(:,:,iz) =       px(:,:,iz)
	  px   (:,:,iz) = a1*pxold(:,:,iz) + (1.-a1)*px(:,:,iz) + b1*dpxdt(:,:,iz)
	  pxold(:,:,iz) =    pxsav(:,:,iz)
	  pxsav(:,:,iz) = a2*pxold(:,:,iz) + (1.-a2)*px(:,:,iz) + b2*dpxdt(:,:,iz)
	  dpxdt(:,:,iz) = 0.

	  pysav(:,:,iz) =       py(:,:,iz)
	  py   (:,:,iz) = a1*pyold(:,:,iz) + (1.-a1)*py(:,:,iz) + b1*dpydt(:,:,iz)
	  pyold(:,:,iz) =    pysav(:,:,iz)
	  pysav(:,:,iz) = a2*pyold(:,:,iz) + (1.-a2)*py(:,:,iz) + b2*dpydt(:,:,iz)
	  dpydt(:,:,iz) = 0.

	  pzsav(:,:,iz) =       pz(:,:,iz)
	  pz   (:,:,iz) = a1*pzold(:,:,iz) + (1.-a1)*pz(:,:,iz) + b1*dpzdt(:,:,iz)
	  pzold(:,:,iz) =    pzsav(:,:,iz)
	  pzsav(:,:,iz) = a2*pzold(:,:,iz) + (1.-a2)*pz(:,:,iz) + b2*dpzdt(:,:,iz)
	  dpzdt(:,:,iz) = 0.
      end if
      if (do_mhd) then
	  Bxsav(:,:,iz) =       Bx(:,:,iz)
	  Bx   (:,:,iz) = a1*Bxold(:,:,iz) + (1.-a1)*Bx(:,:,iz) + b1*dBxdt(:,:,iz)
	  Bxold(:,:,iz) =    Bxsav(:,:,iz)
	  Bxsav(:,:,iz) = a2*Bxold(:,:,iz) + (1.-a2)*Bx(:,:,iz) + b2*dBxdt(:,:,iz)
	  dBxdt(:,:,iz) = 0.

	  Bysav(:,:,iz) =       By(:,:,iz)
	  By   (:,:,iz) = a1*Byold(:,:,iz) + (1.-a1)*By(:,:,iz) + b1*dBydt(:,:,iz)
	  Byold(:,:,iz) =    Bysav(:,:,iz)
	  Bysav(:,:,iz) = a2*Byold(:,:,iz) + (1.-a2)*By(:,:,iz) + b2*dBydt(:,:,iz)
	  dBydt(:,:,iz) = 0.

	  Bzsav(:,:,iz) =       Bz(:,:,iz)
	  Bz   (:,:,iz) = a1*Bzold(:,:,iz) + (1.-a1)*Bz(:,:,iz) + b1*dBzdt(:,:,iz)
	  Bzold(:,:,iz) =    Bzsav(:,:,iz)
	  Bzsav(:,:,iz) = a2*Bzold(:,:,iz) + (1.-a2)*Bz(:,:,iz) + b2*dBzdt(:,:,iz)
	  dBzdt(:,:,iz) = 0.
      end if
    end do
    !$omp end parallel

    isubstep = 2
    call pde(.false.)

    !$omp parallel private(iz)
    do iz=izs,ize
      if (do_density) r(:,lb:ub,iz)  = rsav(:,lb:ub,iz) + c2*drdt(:,lb:ub,iz)
      if (do_energy)  e(:,lb:ub,iz)  = esav(:,lb:ub,iz) + c2*dedt(:,lb:ub,iz)
      if (do_pscalar) d(:,lb:ub,iz)  = dsav(:,lb:ub,iz) + c2*dddt(:,lb:ub,iz)
      if (do_momenta) then
		      px(:,lb:ub,iz) = pxsav(:,lb:ub,iz) + c2*dpxdt(:,lb:ub,iz)                      
		      py(:,lb:ub,iz) = pysav(:,lb:ub,iz) + c2*dpydt(:,lb:ub,iz)
		      pz(:,lb:ub,iz) = pzsav(:,lb:ub,iz) + c2*dpzdt(:,lb:ub,iz)
      end if
      if (do_mhd) then
		      Bx(:,  :  ,iz) = Bxsav(:,  :  ,iz) + c2*dBxdt(:,  :  ,iz)                      
		      By(:,  :  ,iz) = Bysav(:,  :  ,iz) + c2*dBydt(:,  :  ,iz)
		      Bz(:,  :  ,iz) = Bzsav(:,  :  ,iz) + c2*dBzdt(:,  :  ,iz)
      end if
    end do
    !$omp end parallel
  end if

  call dumpn(drdt ,'drdt ','dfdt.dmp',0)
  call dumpn(dpxdt,'dpxdt','dfdt.dmp',1)
  call dumpn(dpydt,'dpydt','dfdt.dmp',1)
  call dumpn(dpzdt,'dpzdt','dfdt.dmp',1)
  if (do_energy) then
    call dumpn(dedt ,'dedt ','dfdt.dmp',1)
  end if
  if (do_pscalar) then
    call dumpn(dddt ,'dddt ','dfdt.dmp',1)
  end if
  if (do_mhd) then
    call dumpn(dBxdt,'dBxdt','dfdt.dmp',1)
    call dumpn(dBydt,'dBydt','dfdt.dmp',1)
    call dumpn(dBzdt,'dBzdt','dfdt.dmp',1)
  end if

  td = td + dt
  rt= td                                 ! rt is the "real time" computed also in substeps.
  t = td
  dtold = dt
END SUBROUTINE
