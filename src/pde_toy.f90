! $Id: pde_toy.f90,v 1.8 2012/08/05 13:25:33 aake Exp $
!***********************************************************************
SUBROUTINE pde (flag)
  USE params
  USE variables
  USE arrays
  implicit none
  logical flag
  real, allocatable, dimension(:,:,:):: &
                  wk22,wk23,wk24,wk25,wk26,wk27,wk28
!.......................................................................
  allocate (wk22(mx,my,mz),wk23(mx,my,mz))
  allocate (wk24(mx,my,mz),wk25(mx,my,mz),wk26(mx,my,mz))
  allocate (wk27(mx,my,mz),wk28(mx,my,mz))
  call pde1 (flag,r   ,px  ,py  ,pz  ,e   ,Bx  ,By  ,Bz  ,           &
                  drdt,dpxdt,dpydt,dpzdt,dedt,dBxdt,dBydt,dBzdt ,    &
                  Ux  ,Uy  ,Uz  ,Ex  ,Ey  ,Ez  ,                     &
                  wk01,wk02,wk03,wk04,wk05,wk06,wk07,wk08,wk09,wk10, &
                  wk11,wk12,wk13,wk14,wk15,wk16,wk17,wk18,wk19,wk20, &
                  wk21,wk22,wk23,wk24,wk25,wk26,wk27,wk28,scr1,scr2, &
                  scr3,scr4,scr5,scr6)
  deallocate (wk22,wk23,wk24,wk25,wk26,wk27,wk28)
END SUBROUTINE

!***********************************************************************
SUBROUTINE pde1 (flag,rho ,px  ,py  ,pz  ,e   ,Bx  ,By  ,Bz ,           &
                      drdt,dpxdt,dpydt,dpzdt,dedt,dBxdt,dBydt,dBzdt ,   &
                      Ux  ,Uy  ,Uz  ,Ex  ,Ey  ,Ez  ,                    &
                      lnr ,nu  ,divU,U   ,xdnr,ydnr,zdnr,Jx ,Jy  ,Jz  , &
                      ee  ,pg  ,Cs  ,lne ,lnu ,eta ,Sxx ,Syy,Szz ,Sxy , &
                      Syz ,Szx ,Txx ,Tyy ,Tzz ,Txy ,Tyz ,Tzx,scr1,scr2, &
                      scr3,scr4,scr5,scr6)
  USE params
  USE stagger
  implicit none
  logical flag
  real ds, nuS1, nuS2
  integer ix, iy, iz
  real, dimension(mx,my,mz):: &
                      rho ,px  ,py  ,pz  ,e   ,Bx  ,By  ,Bz ,           &
                      drdt,dpxdt,dpydt,dpzdt,dedt,dBxdt,dBydt,dBzdt ,   &
                      Ux  ,Uy  ,Uz  ,Ex  ,Ey  ,Ez  ,                    &
                      lnr ,nu  ,divU,U   ,xdnr,ydnr,zdnr,Jx ,Jy  ,Jz  , &
                      ee  ,pg  ,Cs  ,lne ,lnu ,eta ,Sxx ,Syy,Szz ,Sxy , &
                      Syz ,Szx ,Txx ,Tyy ,Tzz ,Txy ,Tyz ,Tzx,scr1,scr2, &
                      scr3,scr4,scr5,scr6

!-----------------------------------------------------------------------
!  Mass conservation, cell centered
!-----------------------------------------------------------------------
  drdt = drdt - ddxupq(px) - ddyupq(py) - ddzupq(pz)
  lnr = alog(rho)
  call density_boundary(rho,lnr,py,e)

!-----------------------------------------------------------------------
!  Velocities
!-----------------------------------------------------------------------
  xdnr = exp(xdn(lnr))
  ydnr = exp(ydn(lnr))
  zdnr = exp(zdn(lnr))
  Ux = px/xdnr
  Uy = py/ydnr
  Uz = pz/zdnr

!-----------------------------------------------------------------------
!  Equation of state 
!-----------------------------------------------------------------------
  ee = e/rho
  call pressure(rho,ee,pg)
  Cs = sqrt((xup(Bx)**2 + yup(By)**2  + zup(Bz)**2 + pg*gamma)/rho)

  call energy_boundary(rho,lnr,Ux,Uy,Uz,e,ee,pg,Bx,By,Bz)
  call velocity_boundary(rho,xdnr,ydnr,zdnr,Ux,Uy,Uz,px,py,pz,e,Bx,By,Bz)
  call forceit(rho,Ux,Uy,Uz,xdnr,ydnr,zdnr,dpxdt,dpydt,dpzdt)

!-----------------------------------------------------------------------
!  Numerical diffusion and resistivity, Richtmyer & Morton type
!-----------------------------------------------------------------------
  U = sqrt(xup(Ux)**2 + yup(Uy)**2  + zup(Uz)**2)
  divU = ddxup(Ux) + ddyup(Uy) + ddzup(Uz)
  nu  = smooth3(max3(max(-divU,0.0)))
!.nu  = smooth3(max3(dif1d(Ux,Uy,Uz)))
  do iz=1,mz; do iy=1,my; do ix=1,mx
    ds = max(dxm(ix),dym(iy),dzm(iz))
    nu(ix,iy,iz)  = (nu1*U(ix,iy,iz) + nu3*Cs(ix,iy,iz) + nu2*ds*nu(ix,iy,iz))
!...nu(ix,iy,iz)  = (nu1*U(ix,iy,iz) + nu3*Cs(ix,iy,iz) + nu2*nu(ix,iy,iz))
    eta(ix,iy,iz) = nuB*nu(ix,iy,iz)
  end do; end do; end do

!-----------------------------------------------------------------------
!  Courant conditions
!-----------------------------------------------------------------------
  if (flag) then
    do iz=1,mz; do iy=1,my; do ix=1,mx
      scr1(ix,iy,iz) = Cs(ix,iy,iz)*dt/min(dxm(ix),dym(iy),dzm(iz))
      scr2(ix,iy,iz) = nu(ix,iy,iz)*dt/min(dxm(ix),dym(iy),dzm(iz))*(6.2*3.)
      scr2(ix,iy,iz) = scr2(ix,iy,iz)*max( &
        0.5*(dxm(ix)+dym(iy))/min(dxm(ix),dym(iy)), &
        0.5*(dym(iy)+dzm(iz))/min(dym(iy),dzm(iz)), &
        0.5*(dzm(iz)+dxm(ix))/min(dzm(iz),dxm(ix)))
    end do; end do; end do
    call fmaxval_subr ('Cu',scr1,Cu)
    call fmaxval_subr ('Cv',scr2,Cv)
  end if

!-----------------------------------------------------------------------
!  Viscous stress, diagonal and off-diagonal elements
!-----------------------------------------------------------------------
  Sxx = ddxup(Ux)
  Syy = ddyup(Uy)
  Szz = ddzup(Uz)
  nuS1 = 2.*nuS
  nuS2 = 2.*(1.-nuS)
  Txx = - nuS1*nu*rho*Sxx - nuS2*nu*rho*divU
  Tyy = - nuS1*nu*rho*Syy - nuS2*nu*rho*divU
  Tzz = - nuS1*nu*rho*Szz - nuS2*nu*rho*divU
  Sxy = 0.5*(ddxdn(Uy)+ddydn(Ux))
  Syz = 0.5*(ddydn(Uz)+ddzdn(Uy))
  Szx = 0.5*(ddzdn(Ux)+ddxdn(Uz))
  lnu = alog(nu*rho)
  Txy = - nuS1*exp(xdnq(ydnq(lnu)))*Sxy
  Tyz = - nuS1*exp(ydnq(zdnq(lnu)))*Syz
  Tzx = - nuS1*exp(zdnq(xdnq(lnu)))*Szx
  do iz=1,mz; do iy=1,my; do ix=1,mx
    Txx(ix,iy,iz) = Txx(ix,iy,iz)*dxm(ix)
    Tyy(ix,iy,iz) = Tyy(ix,iy,iz)*dym(iy)
    Tzz(ix,iy,iz) = Tzz(ix,iy,iz)*dzm(iz)
    Txy(ix,iy,iz) = Txy(ix,iy,iz)*0.5*(dxm(ix)+dym(iy))
    Tyz(ix,iy,iz) = Tyz(ix,iy,iz)*0.5*(dym(iy)+dzm(iz))
    Tzx(ix,iy,iz) = Tzx(ix,iy,iz)*0.5*(dzm(iz)+dxm(ix))
  end do; end do; end do

!-----------------------------------------------------------------------
!  Electric field   E = eta j - (u x B), edge centered
!-----------------------------------------------------------------------
  call mfield_boundary (Ux,Uy,Uz,Bx,By,Bz)
  Jx = ddydnq(Bz) - ddzdnq(By)
  Jy = ddzdnq(Bx) - ddxdnq(Bz)
  Jz = ddxdnq(By) - ddydnq(Bx)

!-----------------------------------------------------------------------
!  Resistive electric field   E = eta j, edge centered
!-----------------------------------------------------------------------
  Ex = ydn(zdn(eta))*Jx
  Ey = zdn(xdn(eta))*Jy
  Ez = xdn(ydn(eta))*Jz
  do iz=1,mz; do iy=1,my; do ix=1,mx
    Ex(ix,iy,iz) = Ex(ix,iy,iz)*(dym(iy)+dzm(iz))*0.5
    Ey(ix,iy,iz) = Ey(ix,iy,iz)*(dzm(iz)+dxm(ix))*0.5
    Ez(ix,iy,iz) = Ez(ix,iy,iz)*(dxm(ix)+dym(iy))*0.5
  end do; end do; end do
  call ecurrent_boundary (Ex, Ey, Ez, Jx, Jy, Jz)
  dedt = dedt + zupq(yupq(Jx*Ex) + xupq(Jy*Ey)) + xupq(yupq(Jz*Ez))

!-----------------------------------------------------------------------
!  Advective electric field   E = E - (u x B), edge centered
!-----------------------------------------------------------------------
  Ex = Ex - zdn(Uy)*ydn(Bz) + ydn(Uz)*zdn(By)
  Ey = Ey - xdn(Uz)*zdn(Bx) + zdn(Ux)*xdn(Bz)
  Ez = Ez - ydn(Ux)*xdn(By) + xdn(Uy)*ydn(Bx)
  call efield_boundary (Ex, Ey, Ez, scr1, scr2, scr3, scr4, scr5, scr6, &
                        Bx, By, Bz, scr1, scr2, scr3, scr4, scr5, scr6, &
                        Ux, Uy, Uz)

!-----------------------------------------------------------------------
!  Magnetic field's time derivative, dBdt = - curl(E), face centered
!-----------------------------------------------------------------------
  dBxdt = dBxdt + ddzupq(Ey) - ddyupq(Ez)
  dBydt = dBydt + ddxupq(Ez) - ddzupq(Ex)
  dBzdt = dBzdt + ddyupq(Ex) - ddxupq(Ey)

!-----------------------------------------------------------------------
!  Energy eq, with P*dV work, difffusion, kinetic and Joule dissipation
!-----------------------------------------------------------------------
  lne = alog(ee)
  do iz=1,mz; do iy=1,my; do ix=1,mx
    scr1(ix,iy,iz) = dxmdn(ix)
    scr2(ix,iy,iz) = dymdn(iy)
    scr3(ix,iy,iz) = dzmdn(iz)
  end do; end do; end do
  dedt = dedt - pg*divU &
    - ddxupq (exp(xdn(lne))*(px - nuE*scr1*exp(xdn(lnu))*ddxdn(lne))) &
    - ddyupq (exp(ydn(lne))*(py - nuE*scr2*exp(ydn(lnu))*ddydn(lne))) &
    - ddzupq (exp(zdn(lne))*(pz - nuE*scr3*exp(zdn(lnu))*ddzdn(lne))) &
    - (Txx*Sxx + Tyy*Syy + Tzz*Szz) &
    - 2.*(xupq(yupq(Txy*Sxy) + zupq(Tzx*Szx)) + yupq(zupq(Tyz*Syz)))
  call coolit (rho, ee, lne, scr1, dedt)

!-----------------------------------------------------------------------
!  Pressure and Reynolds stress
!-----------------------------------------------------------------------
  Txx = Txx + rho*xup(Ux)**2
  Tyy = Tyy + rho*yup(Uy)**2
  Tzz = Tzz + rho*zup(Uz)**2

!-----------------------------------------------------------------------
!  Equations of motion, face centred, with Lorentz force jxB
!-----------------------------------------------------------------------
  Txy = Txy + exp(xdnq(ydnq(lnr)))*ydn(Ux)*xdn(Uy)
  Tyz = Tyz + exp(ydnq(zdnq(lnr)))*zdn(Uy)*ydn(Uz)
  Tzx = Tzx + exp(zdnq(xdnq(lnr)))*xdn(Uz)*zdn(Ux)
  dpxdt = dpxdt - ddxdnq(Txx) - ddyup(Txy) - ddzup(Tzx) + zupq(Jy*xdnq(Bz)) - yupq(Jz*xdnq(By))
  dpydt = dpydt - ddydnq(Tyy) - ddzup(Tyz) - ddxup(Txy) + xupq(Jz*ydnq(Bx)) - zupq(Jx*ydnq(Bz))
  dpzdt = dpzdt - ddzdnq(Tzz) - ddyup(Tyz) - ddxup(Tzx) + yupq(Jx*zdnq(By)) - xupq(Jy*zdnq(Bx))
  call ddt_boundary (rho,px,py,pz,e,pg,Bx,By,Bz, &
                     drdt,dpxdt,dpydt,dpzdt, &
                     dedt,dBxdt,dBydt,dBzdt)  
  END
!***********************************************************************
