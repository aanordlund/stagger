! $Id: mpi_io_aggregate.f90,v 1.14 2014/09/09 11:46:09 aake Exp $
!-------------------------------------------------------------------------------
! This version of file_read_mpi() and file_write_mpi() sends all chunks through
! the first rank (mpi_z==0) of a "beam" of ranks with the same mpi_x and mpi_y.
!-------------------------------------------------------------------------------

!*******************************************************************************
SUBROUTINE file_read_mpi (f, rec)
  USE params
  USE mpi_mod
  implicit none
  real(kind=4), dimension(mx,my,mz):: f
  real(kind=4), allocatable, dimension(:,:,:):: rcv
  integer rec, i_z, req(mpi_nz-1)
!-------------------------------------------------------------------------------
! Read a chunk at specified record number (rec) on ranks with mpi_z == 0 and
! distribute chunks to ranks with mpi_z > 0.
!-------------------------------------------------------------------------------
  allocate (rcv(mx,my,mztot))
  if (mpi_z == 0) then
    call file_read_chunk (rcv, rec)
    do i_z=1,mpi_nz-1
      call file_send_chunk (rcv(:,:,1+i_z*mz:mz+i_z*mz), i_z, req(i_z))
    end do
    call waitall_mpi (mpi_nz-1, req)
    f(:,:,:) = rcv(:,:,1:mz)
  else
    call file_recv_chunk (f, 0, req(1))
    call wait_mpi (req(1))
  end if
  deallocate (rcv)
END SUBROUTINE file_read_mpi

!*******************************************************************************
SUBROUTINE file_write_mpi (f, rec)
  USE params
  USE mpi_mod
  implicit none
  real(kind=4), dimension(mx,my,mz):: f
  real(kind=4), allocatable, dimension(:,:,:):: rcv
  integer rec, i_z, req(mpi_nz-1)
!-------------------------------------------------------------------------------
! Write a chunk at specified record number (rec), aggregating chunks from ranks
! with mpi_z > 0 to the ranks with mpi_z == 0.
!-------------------------------------------------------------------------------
  allocate (rcv(mx,my,mztot))
  if (mpi_z == 0) then
    do i_z=1,mpi_nz-1
      call file_recv_chunk (rcv(:,:,1+i_z*mz:mz+i_z*mz), i_z, req(i_z))
    end do
    rcv(:,:,1:mz) = f
    call waitall_mpi (mpi_nz-1, req)
    call file_write_chunk (rcv, rec)
  else
    call file_send_chunk (f, 0, req(1))
    call wait_mpi (req(1))
  end if
  deallocate (rcv)
END SUBROUTINE file_write_mpi

!*******************************************************************************
SUBROUTINE file_send_chunk (f, rank, req)
  USE params
  USE mpi_mod
  implicit none
  real(kind=4), dimension(mx,my,mz):: f
  integer rank, tag, req
!-------------------------------------------------------------------------------
! Send a chunk to another beam rank
!-------------------------------------------------------------------------------
  tag = rank
  call MPI_Isend (f, mw, MPI_REAL, rank, tag, mpi_comm_beam(3), req, mpi_err)
END SUBROUTINE file_send_chunk

!*******************************************************************************
SUBROUTINE file_recv_chunk (f, rank, req)
  USE params
  USE mpi_mod
  implicit none
  real(kind=4), dimension(mx,my,mz):: f
  integer rank, tag, req
!-------------------------------------------------------------------------------
! Recv a chunk from another beam rank
!-------------------------------------------------------------------------------
  tag = mpi_z
  call MPI_Irecv (f, mw, MPI_REAL, rank, tag, mpi_comm_beam(3), req, mpi_err)
END SUBROUTINE file_recv_chunk

!*******************************************************************************
MODULE aggregate_mod
  integer nio
END MODULE aggregate_mod

!*******************************************************************************
SUBROUTINE file_open_mpi (filem, mode)
  USE params
  USE mpi_mod
  USE aggregate_mod
  implicit none
  integer mode
  character(len=*) filem
  integer, dimension(3):: size, subsize, starts
  logical debug2
!-------------------------------------------------------------------------------
! Open a file for MPI parallel I/O
!-------------------------------------------------------------------------------
  nio = 0
  CALL MPI_FILE_OPEN (mpi_comm_plane(3), filem, mode, MPI_INFO_NULL, handle, mpi_err)    
  if (mpi_err .ne. 0) then
    if (master) print*,'MPI_FILE_OPEN ERROR: mpi_err =',mpi_err
    call abort_mpi
  endif
  size = (/mxtot, mytot, mztot/)
  subsize = (/mx, my, mztot/)
  starts = (/ixoff, iyoff, 0/)
  CALL MPI_TYPE_CREATE_SUBARRAY (3, size, subsize, starts, MPI_ORDER_FORTRAN, MPI_REAL, filetype, mpi_err)   
  CALL MPI_TYPE_COMMIT (filetype, mpi_err)
  if (debug2(dbg_mpi,1) .and. master) then
    print'(1x,a,2i8)','mpi_file_open: '//trim(filem),mpi_rank,mpi_err
  end if 
END SUBROUTINE file_open_mpi

!*******************************************************************************
SUBROUTINE file_openw_mpi (filem)
  USE mpi_mod
  USE params, only: master, dbg_io
  implicit none
  character(len=*) filem
  logical debug2
!...............................................................................
  if (master .and. debug2(dbg_io,1)) print*,'Opening '//trim(filem)//' for writing'
  call file_open_mpi (filem, MPI_MODE_CREATE + MPI_MODE_RDWR)
END SUBROUTINE file_openw_mpi

!*******************************************************************************
SUBROUTINE file_openr_mpi (filem)
  USE mpi_mod
  USE params, only: master, dbg_io
  implicit none
  character(len=*) filem
  logical exists, debug2
!...............................................................................
  inquire (file=filem, exist=exists)
  if (.not. exists) then
    if (master) print*,'file_open_mpi: ERROR, file '//trim(filem)//' does not exist'
    call end_mpi
  end if
  if (master .and. debug2(dbg_io,1)) print*,'Opening '//trim(filem)//' for reading'
  call file_open_mpi (filem, MPI_MODE_RDONLY)
END SUBROUTINE file_openr_mpi

!*******************************************************************************
SUBROUTINE file_close_mpi
  USE mpi_mod
  USE params, only: master, dbg_io
  USE aggregate_mod
  implicit none
  logical debug2
!...............................................................................
  CALL MPI_FILE_CLOSE(handle, mpi_err)
  if (master .and. debug2(dbg_io,1)) print*,'Number of I/O operations =', nio
END SUBROUTINE file_close_mpi

!*******************************************************************************
SUBROUTINE file_write_chunk (f, rec)
  USE params
  USE mpi_mod
  USE aggregate_mod
  implicit none
  real(kind=4), dimension(mx,my,mz):: f
  integer rec, count
  integer status(MPI_STATUS_SIZE)
  integer(kind=MPI_OFFSET_KIND) pos
  logical debug2
!-------------------------------------------------------------------------------
! Write record (physical variable number) rec of a raw data file
!-------------------------------------------------------------------------------
  pos = int(4*(rec-1)*mztot,kind=8)*int(mxtot*mytot,kind=8)
  count = mx*my*mztot
  CALL MPI_FILE_SET_VIEW (handle, pos, MPI_REAL, filetype, 'native', MPI_INFO_NULL, mpi_err)   
  if (mpi_err .ne. 0) then
    print*,'MPI_FILE_SET_VIEW: rank, mpi_err =',mpi_rank,mpi_err
    call abort_mpi
  endif
  CALL MPI_FILE_WRITE_ALL(handle, f , count, MPI_REAL, status, mpi_err)
  if (mpi_err .ne. 0) then
    print*,'MPI_FILE_WRITE_ALL: rank, mpi_err =',mpi_rank,mpi_err
    call abort_mpi
  endif
  if (debug2(dbg_mpi,1) .and. master) then
    print'(1x,a,2i6,1p,3e10.2,2i12)','mpi_file_write_all:',mpi_rank,rec,f(1:3,1,1),pos,count
  end if
  nio = nio+1
END SUBROUTINE file_write_chunk

!*******************************************************************************
SUBROUTINE file_read_chunk (f, rec)
  USE params
  USE mpi_mod
  USE aggregate_mod
  implicit none
  real(kind=4), dimension(mx,my,mz):: f
  integer rec, count
  integer status(MPI_STATUS_SIZE)
  integer(kind=MPI_OFFSET_KIND) pos
  logical debug2
!-------------------------------------------------------------------------------
! Read record (physical variable number) rec of a raw data file
!-------------------------------------------------------------------------------
  pos = int(4*(rec-1)*mztot,kind=8)*int(mxtot*mytot,kind=8)
  count = mx*my*mztot
  CALL MPI_FILE_SET_VIEW (handle, pos, MPI_REAL, filetype, 'native', MPI_INFO_NULL, mpi_err)   
  if (mpi_err .ne. 0) then
    print*,'MPI_FILE_SET_VIEW: rank, mpi_err =',mpi_rank,mpi_err
    call abort_mpi
  endif
  CALL MPI_FILE_READ_ALL(handle, f , count, MPI_REAL, status, mpi_err)
  if (mpi_err .ne. 0) then
    print*,'MPI_FILE_READ_ALL: rank, mpi_err =',mpi_rank,mpi_err
    call abort_mpi
  endif
  if (debug2(dbg_mpi,1) .and. master) then
    print'(1x,a,2i6,1p,3e10.2,2i12)',' mpi_file_read_all:',mpi_rank,rec,f(1:3,1,1),pos,count
  end if
  nio = nio+1
END SUBROUTINE file_read_chunk
