! $Id: passive.f90,v 1.14 2016/11/09 22:42:41 aake Exp $
!***********************************************************************
  SUBROUTINE passive(nu,lnu,r,px,py,pz,Ux,Uy,Uz,dd,dddt)

  USE params
  USE arrays, only: scr1, scr2, scr3
  implicit none

  integer :: ix,iy,iz
  real, dimension(mx,my,mz) :: &
       r,px,py,pz,Ux,Uy,Uz,dd,dddt, &
       nu,lnu,fx,fy,fz
  real, pointer, dimension(:,:,:) :: lnd
  real ddmin, c_decay
  integer loc(3)
  character(len=mid) id
  data id/'$Id: passive.f90,v 1.14 2016/11/09 22:42:41 aake Exp $'/

  if (.not. do_pscalar) then
    !dddt = 0.
    return
  end if

  call print_id(id)

!-----------------------------------------------------------------------
!  Passive scalar
!-----------------------------------------------------------------------
  if (do_loginterp) then
    lnd => scr3

    !$omp parallel do private(iz,iy,ix)
    do iz=1,mz
      ddmin = minval(dd(:,:,iz))
      if (ddmin .le. 0) then
        loc = minval(dd(:,:,iz))
        loc(3) = iz
        print *,'WARNING: dd negative at', mpi_rank,loc
        dd(:,:,iz) = abs(dd(:,:,iz))
      end if
      lnd(:,:,iz) = alog(dd(:,:,iz))
    end do

    call xdn_set(lnd,scr1)
    call xdn_set(lnu,scr2)
    if (do_2nddiv) then
      call ddxdn1_set(lnd,fx)
    else
      call ddxdn_set(lnd,fx)
    end if

    !$omp parallel do private(iz,iy,ix)
    do iz=1,mz
    do iy=1,my
    do ix=1,mx
      fx(ix,iy,iz) = exp(scr1(ix,iy,iz))*(px(ix,iy,iz) - (nur*dxmdn(ix))*exp(scr2(ix,iy,iz))*fx(ix,iy,iz))
    end do
    end do
    end do

    call ydn_set(lnd,scr1)
    call ydn_set(lnu,scr2)
    if (do_2nddiv) then
      call ddydn1_set(lnd,fy)
    else
      call ddydn_set(lnd,fy)
    end if

    !$omp parallel do private(iz,iy,ix)
    do iz=1,mz
    do iy=1,my
    do ix=1,mx
      fy(ix,iy,iz) = exp(scr1(ix,iy,iz))*(py(ix,iy,iz) - (nur*dymdn(iy))*exp(scr2(ix,iy,iz))*fy(ix,iy,iz))
    end do
    end do
    end do

    call zdn_set(lnd,scr1)
    call zdn_set(lnu,scr2)
    if (do_2nddiv) then
      call ddzdn1_set(lnd,fz)
    else
      call ddzdn_set(lnd,fz)
    end if

    !$omp parallel do private(iz,iy,ix)
    do iz=1,mz
    do iy=1,my
    do ix=1,mx
      fz(ix,iy,iz) = exp(scr1(ix,iy,iz))*(pz(ix,iy,iz) - (nur*dzmdn(iz))*exp(scr2(ix,iy,iz))*fz(ix,iy,iz))
    end do
    end do
    end do

  else

    call xdn_set(dd,scr1)
    call xdn_set(nu,scr2)
    if (do_2nddiv) then
      call ddxdn1_set(dd,fx)
    else
      call ddxdn_set(dd,fx)
    end if

    !$omp parallel do private(iz,iy,ix)
    do iz=1,mz
    do iy=1,my
    do ix=1,mx
      fx(ix,iy,iz) = px(ix,iy,iz)*scr1(ix,iy,iz) - (nur*dxmdn(ix))*scr2(ix,iy,iz)*fx(ix,iy,iz)
    end do
    end do
    end do

    call ydn_set(dd,scr1)
    call ydn_set(nu,scr2)
    if (do_2nddiv) then
      call ddydn1_set(dd,fy)
    else
      call ddydn_set(dd,fy)
    end if

    !$omp parallel do private(iz,iy,ix)
    do iz=1,mz
    do iy=1,my
    do ix=1,mx
      fy(ix,iy,iz) = py(ix,iy,iz)*scr1(ix,iy,iz) - (nur*dymdn(iy))*scr2(ix,iy,iz)*fy(ix,iy,iz)
    end do
    end do
    end do

    call zdn_set(dd,scr1)
    call zdn_set(nu,scr2)
    if (do_2nddiv) then
      call ddzdn1_set(dd,fz)
    else
      call ddzdn_set(dd,fz)
    end if

    !$omp parallel do private(iz,iy,ix)
    do iz=1,mz
    do iy=1,my
    do ix=1,mx
      fz(ix,iy,iz) = pz(ix,iy,iz)*scr1(ix,iy,iz) - (nur*dzmdn(iz))*scr2(ix,iy,iz)*fz(ix,iy,iz)
    end do
    end do
    end do
  end if

  if (do_2nddiv) then
    call ddxup1_sub (fx, dddt)
    call ddyup1_sub (fy, dddt)
    call ddzup1_sub (fz, dddt)
  else
    call ddxup_sub (fx, dddt)
    call ddyup_sub (fy, dddt)
    call ddzup_sub (fz, dddt)
  end if
 
  if (t_decay > 0.0) then
    c_decay = 1./t_decay
    do iz=izs,ize
      dddt(:,:,iz) = dddt(:,:,iz) - r(:,:,iz)*dd(:,:,iz)*c_decay
    end do
  endif

  END
