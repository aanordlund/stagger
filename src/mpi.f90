!-------------------------------------------------------------------------------
module mpi_mod

  !use mpi
  implicit none
  include 'mpif.h'

  integer               :: new_com, old_com, mpi_err, mpi_com
  integer               :: mpi_comm_plane(3), cart_mpi(3)
  integer               :: mpi_comm_beam(3)
  integer               :: mpi_color_beam(3), mpi_key_beam(3)
  integer               :: mpi_xup, mpi_xdn
  integer               :: mpi_yup, mpi_ydn
  integer               :: mpi_zdn, mpi_zup
  integer               :: mpi_xup_beam, mpi_xdn_beam
  integer               :: mpi_yup_beam, mpi_ydn_beam
  integer               :: mpi_zup_beam, mpi_zdn_beam
  integer, dimension(3) :: mpi_coords, mpi_dims
  logical               :: mpi_reorder, mpi_periodic(3)
  !integer               :: ix0, ix1, iy0, iy1, iz0, iz1
  integer(kind=8)       :: mpi_sends, mpi_sent, buffer_words, buffer_calls
  integer               :: handle, filetype, my_real, my_integer
  integer, allocatable, dimension(:) :: mx_mpi, my_mpi, mz_mpi
  integer, allocatable, dimension(:) :: ox_mpi, oy_mpi, oz_mpi
  integer, save         :: verbose=0
  real                  :: real_size
  integer               :: RT_size, RT_flag, RT_comm, RT_rank
  integer               :: xrequest(4), zrequest(4), nxreq, nzreq
  integer               :: my_size, my_rank

  real(kind=8) dbl
  real(kind=8), allocatable:: dbl1(:)

  real, dimension(:,:,:), allocatable:: send, recv, hxsnd, gxsnd, hzsnd, gzsnd
  real(kind=4), allocatable, dimension(:,:):: buffer

end module mpi_mod

!-------------------------------------------------------------------------------
subroutine init_mpi

  !  Run the stagger code under MPI.  All we do is modify the stagger operators
  !  so they use MPI to communicate overlap zones.  There is no change needed in
  !  the  code, only in initialization, driver and (possibly) boundary routines.
  !  There, changes are minimized by just changing the scales and coordinates,
  !  assuming that the MPI-boxes have the sizes given in the input param file.

  use params
  use mpi_mod
  implicit none

  real :: dummy
  integer :: iostat, mpi_tmp
  integer :: status(MPI_STATUS_SIZE)
  character(len=120) :: line
  character(len=mid) :: id = 'init_mpi'
  !namelist /split/ RT_size

  stdout = 6
  stderr = 6
  mpi_size = 0
  mpi_rank = 0


  !-------------------------------------------------------------------------------
  !  Start up MPI and get number of nodes and our node (mpi_rank)
  !-------------------------------------------------------------------------------

  call MPI_INIT (mpi_err)
  mpi_com = mpi_comm_world
  call MPI_COMM_SIZE (mpi_com, mpi_size, mpi_err)
  call MPI_COMM_RANK (mpi_com, mpi_rank, mpi_err)
  my_size = mpi_size
  my_rank = mpi_rank

  RT_size = 0
  !!read (stdin,split,iostat=iostat); if (master) write(stdout,split); rewind(stdin)

  if (mpi_rank+RT_size > mpi_size-1) then
     RT_flag = 1                                                                 ! we are the RT
  else
     RT_flag = 0                                                                 ! we are the MHD
  end if

  if (RT_size > 0) then
     call MPI_COMM_SPLIT (mpi_com, RT_flag, mpi_rank, mpi_tmp, mpi_err)          ! new mpi_com
     mpi_com = mpi_tmp
     call MPI_COMM_SIZE (mpi_com, mpi_size, mpi_err)                             ! new mpi_size
     call MPI_COMM_RANK (mpi_com, mpi_rank, mpi_err)                             ! new mpi_rank
  end if

  if (RT_flag==1) then
     call end_mpi
  end if

  dummy = 1.0
  if (storage_size(dummy) == 64) then
     my_real = MPI_REAL8
     real_size = 8.
  else
     my_real = MPI_REAL
     real_size = 4.
  endif
  if (storage_size(iostat) == 64) then
     my_integer = MPI_INTEGER8
  else
     my_integer = MPI_INTEGER
  endif

  !-------------------------------------------------------------------------------
  !  Allow printout only on the master node.  This assumes testing on stdio.
  !-------------------------------------------------------------------------------

  if (mpi_rank == 0) then
     mpi_master = .true.
  else
     mpi_master = .false.
     stdout = -1
  end if
  master = mpi_master

  call print_id(id)
  if (master) then
     print *,hl
     print *,'init_mpi: number of processes =', mpi_size
     !print *,'init_mpi: number of RT processes =', RT_size
     print *,'init_mpi: input file = ',trim(inputfile)
  end if

  mpi_sends = 0
  mpi_sent = 0

end subroutine init_mpi

!-------------------------------------------------------------------------------
subroutine mpi_name (name)

  use params
  use mpi_mod
  implicit none

  integer, parameter :: mseq=6
  integer :: i, k, idir, ierr
  integer :: index
  character(len=mfile) :: name
  character(len=mseq) :: sequence

  if (mpi_size .gt. 1) then
     if (do_subdir) then
        write (sequence,'("N",i4.4,"/")') mpi_rank
        i = index(name,'/',back=.true.)
        if (i > 0) then
           name = name(1:i)//sequence//name(i+1:mfile-mseq)
        else
           name = sequence//name(1:mfile-mseq)
        end if
     else
        write (sequence,'("-",i5.5)') mpi_rank
        i = index(name,'.',back=.true.)-1
        if (i > 0) then
           name = name(1:i)//sequence//name(i+1:mfile-mseq)
        else
           name = name(1:mfile-mseq)//sequence
        end if
     end if
  end if

end subroutine mpi_name


!-------------------------------------------------------------------------------
subroutine barrier_mpi (label)

  use params
  use mpi_mod
  implicit none

  character(len=*) :: label
  character(len=8) :: test1, test2
  logical :: omp_in_parallel, debug, any_mpi_nowait

  !if (debug(dbg_io)) print *,'barrier_mpi:', mpi_rank,omp_mythread,omp_in_parallel()
  if (omp_in_parallel()) then
     !$omp barrier
     !$omp single
     if (debug(dbg_mpi)) print *, mpi_rank, 'barrier_mpi: ', label
     call MPI_BARRIER (mpi_com, mpi_err)
     !$omp end single
  else
     if (debug(dbg_mpi)) print *, mpi_rank, 'barrier_mpi: ', label
     call MPI_BARRIER (mpi_com, mpi_err)
  end if
  if (check_barrier) then
     test1 = label
     test2 = test1
     call broadcast_chars_mpi (test2, 8)
     if (test1 /= test2) then
        print *,'barrier_mpi CHECK, local:'//test2//', master:'//test1 &
             //', rank:', mpi_rank
     end if
     if (any_mpi_nowait(test1 /= test2)) then
        if (master) print*,'barrier_mpi ERROR: out of sync for label = ', label
     end if
  end if

end subroutine barrier_mpi

!-------------------------------------------------------------------------------
subroutine waitall_mpi (nreq, req)
  use mpi_mod
  implicit none
  integer :: nreq, req(nreq)
  if (nreq > 0) call MPI_WAITALL (nreq, req, MPI_STATUSES_IGNORE, mpi_err)
end subroutine waitall_mpi

!-------------------------------------------------------------------------------
subroutine wait_mpi (req)
  use mpi_mod
  implicit none
  integer :: req
  call MPI_Wait (req, MPI_STATUS_IGNORE, mpi_err)
end subroutine wait_mpi

!-------------------------------------------------------------------------------
subroutine barrier_mpi_test (label)
  use params
  implicit none
  character(len=*) :: label

  call barrier_mpi (label)
  print *,'barrier_mpi_test:',label,mpi_rank
  call barrier_mpi (label)
end subroutine barrier_mpi_test

!-------------------------------------------------------------------------------
subroutine flush_mpi
  use mpi_mod
  implicit none
  call barrier_omp('flush')
  !$omp single
  call MPI_BARRIER (mpi_com, mpi_err)
  !$omp end single
end subroutine flush_mpi

!-------------------------------------------------------------------------------
subroutine barrier1_mpi
  use mpi_mod
  implicit none
  call barrier_omp('1')
  !! !$omp single
  !! call MPI_BARRIER (mpi_com, mpi_err)
  !! !$omp end single
end subroutine barrier1_mpi

!-------------------------------------------------------------------------------
subroutine barrier2_mpi
  use mpi_mod
  implicit none
  call barrier_omp('2')
  !! !$omp single
  !! call MPI_BARRIER (mpi_com, mpi_err)
  !! !$omp end single
end subroutine barrier2_mpi

!-------------------------------------------------------------------------------
subroutine barrier3_mpi
  use mpi_mod
  implicit none
  call barrier_omp('3')
  !! !$omp single
  !! call MPI_BARRIER (mpi_com, mpi_err)
  !! !$omp end single
end subroutine barrier3_mpi

!-------------------------------------------------------------------------------
subroutine init_geom_mpi

  use params
  implicit none

  real :: power
  integer :: np
  logical :: success
  character(len=mid) :: id = 'init_geom_mpi'

  call print_id(id)
  call set_omp

  !! either OMP or MPI in z-dir
  !if (omp_nz > 1) mpi_nz = 1

  ! case where mpi_nx is computed
  mpi_ny = max(1, mpi_ny)
  mpi_nz = max(1, mpi_nz)
  if (mpi_nx==0) mpi_nx = mpi_size/(mpi_ny*mpi_nz)
  call  mpi_search_geom (mpi_size, np, mx, my, mz, mpi_nx, mpi_ny, mpi_nz)

  ! let the z-dir take the rest
  mpi_nx = max(1, mpi_nx)
  mpi_ny = max(1, mpi_ny)
  mpi_nz = mpi_size/(mpi_nx*mpi_ny)
  if (mpi_size .ne. mpi_nx*mpi_ny*mpi_nz) then
     if (master) print*,'MPI mismatch: size, n[xyz] =', &
          mpi_size,mpi_nx,mpi_ny,mpi_nz
     call abort_mpi
  endif

  !call test_mpi
end subroutine init_geom_mpi

!-------------------------------------------------------------------------------
subroutine mesh_mpi

  use params
  use mpi_mod

  implicit none

  integer :: iz
  integer :: i_x, i_y, i_z
  integer :: mxrem, myrem, mzrem
  integer :: mpi_xup1, mpi_xdn1
  integer, dimension(1) :: buffer1
  logical :: debug
  character(len=mid) :: id = 'mesh_mpi'
  character(len=mid) :: fmt

  call print_id(id)

  ! Compute mpi dimensions, our place in the node space, and our neighbors

  mpi_reorder = .false.
  mpi_periodic = .true.
  mpi_dims(1) = mpi_nx
  mpi_dims(2) = mpi_ny
  mpi_dims(3) = mpi_nz
  !if (mpi_nz>1 .and. omp_nthreads>1) then
  !  print *,'cannot use both MPI and OMP in the z-direction'
  !  call MPI_FINALIZE (mpi_err)
  !  stop
  !end if

  ! Create New Cartesian Communicator

  call MPI_CART_CREATE (mpi_com, 3, mpi_dims, mpi_periodic, mpi_reorder, new_com,  mpi_err)
  call MPI_CART_COORDS (new_com, mpi_rank, 3, mpi_coords, mpi_err)
  call MPI_CART_SHIFT  (new_com, 0, 1, mpi_xdn, mpi_xup, mpi_err)
  call MPI_CART_SHIFT  (new_com, 1, 1, mpi_ydn, mpi_yup, mpi_err)
  call MPI_CART_SHIFT  (new_com, 2, 1, mpi_zdn, mpi_zup, mpi_err)


  ! Split main communicator into MPI_COMM_PLANE sub-communicators, one per MPI plane (YZ, XZ, XY)

  !!if (mpi_dims(1) > 1) then
  call MPI_COMM_SPLIT (mpi_com, mpi_coords(1), mpi_rank, mpi_comm_plane(1), mpi_err)
  call assert_code ('MPI_comm_split call 1', mpi_err == 0, mpi_err)
  call MPI_CART_CREATE (mpi_comm_plane(1), 2, (/mpi_ny,mpi_nz/), mpi_periodic, mpi_reorder, cart_mpi(1),  mpi_err)
  call assert_code ('MPI_Cart_create call 1', mpi_err == 0, mpi_err)
  !!end if

  !!if (mpi_dims(2) > 1) then
  call MPI_COMM_SPLIT (mpi_com, mpi_coords(2), mpi_rank, mpi_comm_plane(2), mpi_err)
  call assert_code ('MPI_comm_split call 2', mpi_err == 0, mpi_err)
  call MPI_CART_CREATE (mpi_comm_plane(2), 2, (/mpi_nx,mpi_nz/), mpi_periodic, mpi_reorder, cart_mpi(2),  mpi_err)
  call assert_code ('MPI_Cart_create call 2', mpi_err == 0, mpi_err)
  !!end if

  !!if (mpi_dims(3) > 1) then
  call MPI_COMM_SPLIT (mpi_com, mpi_coords(3), mpi_rank, mpi_comm_plane(3), mpi_err)
  call assert_code ('MPI_comm_split call 3', mpi_err == 0, mpi_err)
  call MPI_CART_CREATE (mpi_comm_plane(3), 2, (/mpi_nx,mpi_ny/), mpi_periodic, mpi_reorder, cart_mpi(3),  mpi_err)
  call assert_code ('MPI_Cart_create call 3', mpi_err == 0, mpi_err)
  !!end if


  ! Define MPI coordinates on Cartesian grid
  mpi_x = mpi_coords(1)
  mpi_y = mpi_coords(2)
  mpi_z = mpi_coords(3)
  !if (mpi_x==0 .or. mpi_x==(mpi_nx-1)) then
  !  call MPI_CART_SHIFT  (new_com, 0, -1, mpi_xdn1, mpi_xup1, mpi_err)
  !  print'(a,10i5)','MK:',mpi_rank,mpi_x,mpi_xdn,mpi_xup,mpi_xdn1,mpi_xup1
  !endif


  ! Allocate one-dimensional arrays ix_cart_mpi, iy_mpi_cart, iz_mpi_cart, and assign 
  ! them the x, y, and z coordinate indexes for the MPI Cartesian communicator as a 
  ! function of the MPI rank.
  ! NOTE: mpi_[x,y,z] is copied to buffer1(1) before passing the latter to
  ! mpi_cart_gather_int: the routine is expecting a vector and passing a scalar would
  ! cause certain compilers to issue a warning message.

  allocate( ix_mpi_cart( mpi_size), iy_mpi_cart( mpi_size), iz_mpi_cart( mpi_size) )
  buffer1(1) = mpi_x ; call mpi_cart_gather_int ( buffer1(1), 1, ix_mpi_cart )
  buffer1(1) = mpi_y ; call mpi_cart_gather_int ( buffer1(1), 1, iy_mpi_cart )
  buffer1(1) = mpi_z ; call mpi_cart_gather_int ( buffer1(1), 1, iz_mpi_cart )


  ! Split main communicator into MPI_COMM_BEAM sub-communicators, according to the 
  ! MPI coordinate pairs (Y,Z), (Z,X), or (X,Y)

  ! Identify subdomains with the same (mpi_y,mpi_z) coordinates by defining 
  ! mpi_color_beam(1) = mpi_ny * mpi_z + mpi_y;
  ! Within mpi_comm_beam(1), identify subdomains by the value of the mpi_x coordinate;

  ! Define labels for relative down- and up- ranks along beam(1) (assumed to be periodic).

  mpi_color_beam(1) = mpi_ny * mpi_z + mpi_y
  mpi_key_beam(1)   = mpi_x
  call MPI_COMM_SPLIT(mpi_com, mpi_color_beam(1), mpi_key_beam(1), mpi_comm_beam(1), mpi_err)

  mpi_xdn_beam = modulo( mpi_x - 1, mpi_nx )
  mpi_xup_beam = modulo( mpi_x + 1, mpi_nx )


  ! Identify subdomains with the same (mpi_x,mpi_z) coordinates by defining 
  ! mpi_color_beam(2) = mpi_nz * mpi_x + mpi_z;
  ! Within mpi_comm_beam(2), identify subdomains by the value of the mpi_y coordinate;

  ! Define labels for relative down- and up- ranks along beam(2) (assumed to be periodic).

  mpi_color_beam(2) = mpi_nz * mpi_x + mpi_z
  mpi_key_beam(2)   = mpi_y
  call MPI_COMM_SPLIT(mpi_com, mpi_color_beam(2), mpi_key_beam(2), mpi_comm_beam(2), mpi_err)

  mpi_ydn_beam = modulo( mpi_y - 1, mpi_ny )
  mpi_yup_beam = modulo( mpi_y + 1, mpi_ny )


  ! Identify subdomains with the same (mpi_y,mpi_z) coordinates by defining 
  ! mpi_color_beam(3) = mpi_nx * mpi_y + mpi_x;
  ! Within mpi_comm_beam(3), identify subdomains by the value of the mpi_z coordinate.  

  ! Define labels for relative down- and up- ranks along beam(3) (assumed to be periodic).

  mpi_color_beam(3) = mpi_nx * mpi_y + mpi_x
  mpi_key_beam(3)   = mpi_z
  call MPI_COMM_SPLIT(mpi_com, mpi_color_beam(3), mpi_key_beam(3), mpi_comm_beam(3), mpi_err)

  mpi_zdn_beam = modulo( mpi_z - 1, mpi_nz )
  mpi_zup_beam = modulo( mpi_z + 1, mpi_nz )


  ! Allocate mx_mpi, ox_mpi, etc., arrays of indexes
  allocate (mx_mpi(0:mpi_nx-1), my_mpi(0:mpi_ny-1), mz_mpi(0:mpi_nz-1))
  allocate (ox_mpi(0:mpi_nx-1), oy_mpi(0:mpi_ny-1), oz_mpi(0:mpi_nz-1))

  ! back to differential size
  ub = my-ub

  ! global dimensions of the grid, x, y, and z directions
  mxtot = mx
  mytot = my
  mztot = mz

  ! reduce grid for MPI
  mx    = mxtot / mpi_nx
  my    = mytot / mpi_ny
  mz    = mztot / mpi_nz

  if (master) then
     fmt='(a,2x,3i8)'
     print *,'====================================================='
     print fmt,' MPI:  mxtot  mytot  mztot =', mxtot,mytot,mztot
     print fmt,' MPI: mpi_nx mpi_ny mpi_nz =', mpi_nx,mpi_ny,mpi_nz
     print fmt,' MPI:     mx     my     mz =', mx,my,mz
     print *,'====================================================='
  end if

  mxrem = mxtot - mx * mpi_nx
  myrem = mytot - my * mpi_ny
  mzrem = mztot - mz * mpi_nz

  mx_mpi(0:mpi_nx-1) = mx
  my_mpi(0:mpi_ny-1) = my
  mz_mpi(0:mpi_nz-1) = mz


  if (mxrem .gt. 0) mx_mpi(0:mxrem) = mx+1
  if (myrem .gt. 0) my_mpi(0:myrem) = my+1
  if (mzrem .gt. 0) mz_mpi(0:mzrem) = mz+1


  ixoff = mpi_x*mx + min(mpi_x,mxrem)
  iyoff = mpi_y*my + min(mpi_y,myrem)
  izoff = mpi_z*mz + min(mpi_z,mzrem)


  do i_x=0,mpi_nx-1
     ox_mpi(i_x) = i_x*mx + min(i_x,mxrem)
  end do

  do i_y=0,mpi_ny-1
     oy_mpi(i_y) = i_y*my + min(i_y,myrem)
  end do

  do i_z=0,mpi_nz-1
     oz_mpi(i_z) = i_z*mz + min(i_z,mzrem)
  end do

  if (mpi_x < mxrem) mx=mx+1
  if (mpi_y < myrem) my=my+1
  if (mpi_z < mzrem) mz=mz+1

  if (debug(dbg_mpi)) print'(a,i5,3i4,2x,3i4)', &
       'rank,dims,offset:',mpi_rank,mx,my,mz,ixoff,iyoff,izoff

  ! absolute (local) index
  ub = my-ub

  ! make sure bdry zones only at bdries
  if (mpi_y .ne.        0) lb=1
  if (mpi_y .ne. mpi_ny-1) ub=my

  !call oflush
  if (debug(dbg_mpi)) print '(1x,a6,i5,4(i8,i5),2i5)','mpi:', &
       mpi_rank, mpi_xdn, mpi_xup, mpi_ydn, mpi_yup, mpi_zdn, mpi_zup, mpi_coords
  !call oflush

  if (master) then
     print'(a,1p,e11.3)', ' Total words per array: ',           float(mxtot)*float(mytot)*float(mztot)
     print'(a,    f7.3)', ' log2  words per array: ',      alog(float(mxtot)*float(mytot)*float(mztot))/alog(2.)
     print'(a,1p,e11.3)', ' Total bytes per array: ', real_size*float(mxtot)*float(mytot)*float(mztot)
     print'(a,1p,e11.3)', ' Local bytes per array: ', real_size*float(mx   )*float(my   )*float(mz   )
  endif


  !  Allocate buffer zones
  allocate (gx1(1,my,mz), hx1(1,my,mz), gx2(2,my,mz), hx2(2,my,mz), gx3(3,my,mz), hx3(3,my,mz))
  allocate (gy1(mx,1,mz), gy2(mx,2,mz), gy3(mx,3,mz), hy1(mx,1,mz), hy2(mx,2,mz), hy3(mx,3,mz))
  allocate (gz1(mx,my,1), gz2(mx,my,2), gz3(mx,my,3), hz1(mx,my,1), hz2(mx,my,2), hz3(mx,my,3))

end subroutine mesh_mpi

!-------------------------------------------------------------------------------
subroutine end_mpi
  use params
  use mpi_mod
  implicit none

  ! Wait for other ranks, then finalize

  !$omp master
  call MPI_BARRIER (mpi_comm_world, mpi_err)
  call MPI_FINALIZE (mpi_err)
  !$omp end master

  ! Avoid stop output
  call exit
end subroutine end_mpi

!-------------------------------------------------------------------------------
subroutine abort_mpi
  use params
  use mpi_mod
  implicit none

  print*,mpi_rank,' called abort_mpi'
  call MPI_ABORT (mpi_com, 127, mpi_err)
  call exit
end subroutine abort_mpi

!-------------------------------------------------------------------------------
subroutine mpi_send1_x (mf, f, g, h)
  
  !  Send the boundary layers from one domain over to the neighboring domains
  !  and receive the corresponding values into temporary arrays.

  use params
  use mpi_mod
  implicit none

  integer :: mf
  real, dimension(mf) :: f, g, h
  integer :: status(MPI_STATUS_SIZE)
  integer :: request1, request2, request3, request4

  !-------------------------------------------------------------------------------
  !  Send up and down
  !-------------------------------------------------------------------------------

  !$omp critical
  call MPI_ISEND (f, mf, my_real, mpi_xdn, 2*(mpi_rank*omp_nthreads + omp_mythread)    , mpi_com, request1, mpi_err)
  call MPI_IRECV (h, mf, my_real, mpi_xup, 2*(mpi_xup *omp_nthreads + omp_mythread)    , mpi_com, request3, mpi_err)
  call MPI_ISEND (f, mf, my_real, mpi_xup, 2*(mpi_rank*omp_nthreads + omp_mythread) + 1, mpi_com, request2, mpi_err)
  call MPI_IRECV (g, mf, my_real, mpi_xdn, 2*(mpi_xdn *omp_nthreads + omp_mythread) + 1, mpi_com, request4, mpi_err)
  !$omp end critical
  call barrier_omp('mpi_send1_x')
  !$omp critical
  call MPI_WAIT (request1, status, mpi_err)
  call MPI_WAIT (request3, status, mpi_err)
  call MPI_WAIT (request2, status, mpi_err)
  call MPI_WAIT (request4, status, mpi_err)
  !$omp end critical
  call barrier_omp('mpi_send1_x')

end subroutine mpi_send1_x

!-------------------------------------------------------------------------------
subroutine mpi_send1_y (mf, f, g, h)

  !  Send the boundary layers from one domain over to the neighboring domains
  !  and receive the corresponding values into temporary arrays.

  use params
  use mpi_mod
  implicit none

  integer :: mf
  real, dimension(mf) :: f, g, h
  integer :: status(MPI_STATUS_SIZE)
  integer :: request1, request2, request3, request4


  !  Send up and down

  !$omp critical
  call MPI_ISEND (f, mf, my_real, mpi_ydn, 2*(mpi_rank*omp_nthreads + omp_mythread)    , mpi_com, request1, mpi_err)
  call MPI_IRECV (h, mf, my_real, mpi_yup, 2*(mpi_yup *omp_nthreads + omp_mythread)    , mpi_com, request3, mpi_err)
  call MPI_ISEND (f, mf, my_real, mpi_yup, 2*(mpi_rank*omp_nthreads + omp_mythread) + 1, mpi_com, request2, mpi_err)
  call MPI_IRECV (g, mf, my_real, mpi_ydn, 2*(mpi_ydn *omp_nthreads + omp_mythread) + 1, mpi_com, request4, mpi_err)
  !$omp end critical
  call barrier_omp('mpi_send1_y')
  !$omp critical
  call MPI_WAIT (request1, status, mpi_err)
  call MPI_WAIT (request3, status, mpi_err)
  call MPI_WAIT (request2, status, mpi_err)
  call MPI_WAIT (request4, status, mpi_err)
  !$omp end critical
  call barrier_omp('mpi_send1_y')

end subroutine mpi_send1_y

!-------------------------------------------------------------------------------
subroutine mpi_send1_z (mf, f, g, h)

  !  Send the boundary layers from one domain over to the neighboring domains
  !  and receive the corresponding values into temporary arrays.

  use params
  use mpi_mod
  implicit none

  integer :: mf
  real, dimension(mf) :: f, g, h
  integer :: status(MPI_STATUS_SIZE)
  integer :: request1, request2, request3, request4

  !  Send up and down

  !$omp critical
  call MPI_ISEND (f, mf, my_real, mpi_zdn, 2*(mpi_rank*omp_nthreads + omp_mythread)    , mpi_com, request1, mpi_err)
  call MPI_IRECV (h, mf, my_real, mpi_zup, 2*(mpi_zup *omp_nthreads + omp_mythread)    , mpi_com, request3, mpi_err)
  call MPI_ISEND (f, mf, my_real, mpi_zup, 2*(mpi_rank*omp_nthreads + omp_mythread) + 1, mpi_com, request2, mpi_err)
  call MPI_IRECV (g, mf, my_real, mpi_zdn, 2*(mpi_zdn *omp_nthreads + omp_mythread) + 1, mpi_com, request4, mpi_err)
  !$omp end critical
  call barrier_omp('mpi_send1_z')
  !$omp critical
  call MPI_WAIT (request1, status, mpi_err)
  call MPI_WAIT (request3, status, mpi_err)
  call MPI_WAIT (request2, status, mpi_err)
  call MPI_WAIT (request4, status, mpi_err)
  !$omp end critical
  call barrier_omp('mpi_send1_z')

end subroutine mpi_send1_z


!-------------------------------------------------------------------------------
subroutine mpi_send2_x (f, nx, ny, nz, g, ng, h, nh)

  !  Send boundary layers from one domain over to neighboring domains (X direction);
  !  receive the corresponding layers from neighboring domains and store them
  !  into temporary arrays.

  use params
  use mpi_mod
  implicit none

  integer :: nx, ny, nz, ng, nh
  real, dimension(nx,ny,nz) :: f
  real, dimension(ng,ny,nz) :: g, g1
  real, dimension(nh,ny,nz) :: h, h1
  integer :: status(MPI_STATUS_SIZE)
  integer :: request1, request2, request3, request4

  call barrier_omp('mpi_send2_x')

  !$omp master

  h1 = f(1:nh,:,:)
  call MPI_ISEND (h1, nh*ny*nz, my_real, mpi_xdn, 2*(mpi_rank*omp_nthreads + omp_mythread)    , mpi_com, request1, mpi_err)
  call MPI_IRECV (h , nh*ny*nz, my_real, mpi_xup, 2*(mpi_xup *omp_nthreads + omp_mythread)    , mpi_com, request3, mpi_err)

  g1 = f(nx-ng+1:nx,:,:)
  call MPI_ISEND (g1, ng*ny*nz, my_real, mpi_xup, 2*(mpi_rank*omp_nthreads + omp_mythread) + 1, mpi_com, request2, mpi_err)
  call MPI_IRECV (g , ng*ny*nz, my_real, mpi_xdn, 2*(mpi_xdn *omp_nthreads + omp_mythread) + 1, mpi_com, request4, mpi_err)

  call MPI_WAIT (request1, status, mpi_err)
  call MPI_WAIT (request3, status, mpi_err)
  call MPI_WAIT (request2, status, mpi_err)
  call MPI_WAIT (request4, status, mpi_err)

  !$omp end master

  call barrier_omp('mpi_send2_x')

end subroutine mpi_send2_x


!-------------------------------------------------------------------------------
subroutine mpi_send2_y (f, nx, ny, nz, g, ng, h, nh)

  !  Send boundary layers from one domain over to neighboring domains (Y direction);
  !  receive the corresponding layers from neighboring domains and store them
  !  into temporary arrays.

  use params
  use mpi_mod
  implicit none

  integer :: nx, ny, nz, ng, nh
  real, dimension(nx,ny,nz) :: f
  real, dimension(nx,ng,nz) :: g, g1
  real, dimension(nx,nh,nz) :: h, h1
  integer :: status(MPI_STATUS_SIZE)
  integer :: request1, request2, request3, request4

  call barrier_omp('mpi_send2_y')

  !$omp master

  h1 = f(:,1:nh,:)
  call MPI_ISEND (h1, nx*nh*nz, my_real, mpi_ydn, 2*(mpi_rank*omp_nthreads + omp_mythread)    , mpi_com, request1, mpi_err)
  call MPI_IRECV (h , nx*nh*nz, my_real, mpi_yup, 2*(mpi_yup *omp_nthreads + omp_mythread)    , mpi_com, request3, mpi_err)

  g1 = f(:,ny-ng+1:ny,:)
  call MPI_ISEND (g1, nx*ng*nz, my_real, mpi_yup, 2*(mpi_rank*omp_nthreads + omp_mythread) + 1, mpi_com, request2, mpi_err)
  call MPI_IRECV (g , nx*ng*nz, my_real, mpi_ydn, 2*(mpi_ydn *omp_nthreads + omp_mythread) + 1, mpi_com, request4, mpi_err)

  call MPI_WAIT (request1, status, mpi_err)
  call MPI_WAIT (request3, status, mpi_err)
  call MPI_WAIT (request2, status, mpi_err)
  call MPI_WAIT (request4, status, mpi_err)

  !$omp end master

  call barrier_omp('mpi_send2_y')

end subroutine mpi_send2_y


!-------------------------------------------------------------------------------
subroutine mpi_send2_z (f, nx, ny, nz, g, ng, h, nh)

  !  Send boundary layers from one domain over to neighboring domains (Z direction);
  !  receive the corresponding layers from neighboring domains and store them
  !  into temporary arrays.

  use params
  use mpi_mod
  implicit none

  integer :: nx, ny, nz, ng, nh
  real, dimension(nx,ny,nz) :: f
  real, dimension(nx,ny,ng) :: g, g1
  real, dimension(nx,ny,nh) :: h, h1
  integer :: status(MPI_STATUS_SIZE)
  integer :: request1, request2, request3, request4

  call barrier_omp('mpi_send2_z')

  !$omp master

  h1 = f(:,:,1:nh)
  call MPI_ISEND (h1, nx*ny*nh, my_real, mpi_zdn, 2*(mpi_rank*omp_nthreads + omp_mythread)    , mpi_com, request1, mpi_err)
  call MPI_IRECV (h , nx*ny*nh, my_real, mpi_zup, 2*(mpi_zup *omp_nthreads + omp_mythread)    , mpi_com, request3, mpi_err)

  g1 = f(:,:,nz-ng+1:nz)
  call MPI_ISEND (g1, nx*ny*ng, my_real, mpi_zup, 2*(mpi_rank*omp_nthreads + omp_mythread) + 1, mpi_com, request2, mpi_err)
  call MPI_IRECV (g , nx*ny*ng, my_real, mpi_zdn, 2*(mpi_zdn *omp_nthreads + omp_mythread) + 1, mpi_com, request4, mpi_err)

  call MPI_WAIT (request1, status, mpi_err)
  call MPI_WAIT (request3, status, mpi_err)
  call MPI_WAIT (request2, status, mpi_err)
  call MPI_WAIT (request4, status, mpi_err)

  !$omp end master

  call barrier_omp('mpi_send2_z')

end subroutine mpi_send2_z


!-------------------------------------------------------------------------------
subroutine mpi_send_bdry_y_dn (f, n, g)

  ! Send lower-most (index-wise) n boundary layers to relative down-rank (i.e., from 
  ! mpi_y to mpi_ydn_beam) along sub-communicator mpi_comm_beam(2);
  ! Receive corresponding number of layers from relative up-rank (from mpi_yup_beam 
  ! to mpi_y) and store it in array g.

  use params
  use mpi_mod
  implicit none

  integer :: iy, iz, n
  integer :: mBuffer
  integer :: status(MPI_STATUS_SIZE)
  integer :: source, dest
  integer :: tag_send, tag_recv
  integer :: req_send, req_recv

  real, dimension(mx,my,mz) :: f
  real, dimension(mx, n,mz) :: fSend, g


  ! prepare buffer for sending
  do iz=izs,ize
     do iy=1,n
        fSend(:,iy,iz) = f(:,iy,iz)
     end do
  end do

  ! buffer size
  mBuffer  = mx *  n * mz

  ! dest and send tag
  dest     = mpi_ydn_beam
  tag_send = 2*mpi_y

  ! source and receive tag
  source   = mpi_yup_beam 
  tag_recv = 2*mpi_yup_beam

  ! send and receive
  call MPI_ISEND(fSend, mBuffer, my_real, dest,   tag_send, mpi_comm_beam(2), req_send, mpi_err)
  call MPI_IRECV(    g, mBuffer, my_real, source, tag_recv, mpi_comm_beam(2), req_recv, mpi_err)    
  call MPI_WAIT (req_send, status, mpi_err)
  call MPI_WAIT (req_recv, status, mpi_err)  

end subroutine mpi_send_bdry_y_dn


!-------------------------------------------------------------------------------
subroutine mpi_send_bdry_y_up (f, n, g)
 
  ! Send upper-most (index-wise) n boundary layers to relative up-rank (i.e., from 
  ! mpi_y to mpi_yup_beam) along sub-communicator mpi_comm_beam(2);
  ! Receive corresponding number of layers from relative down-rank (from mpi_ydn_beam 
  ! to mpi_y) and store it in array g.

  use params
  use mpi_mod
  implicit none

  integer :: j, iy, iz, n
  integer :: mBuffer
  integer :: status(MPI_STATUS_SIZE)
  integer :: source, dest
  integer :: tag_send, tag_recv
  integer :: req_send, req_recv

  real, dimension(mx,my,mz) :: f
  real, dimension(mx, n,mz) :: fSend, g


  ! prepare buffer for sending
  do iz=izs,ize
     do j=1,n
        iy = my+1 - j
        fSend(:,j,iz) = f(:,iy,iz)
     end do
  end do

  ! buffer size
  mBuffer  = mx *  n * mz

  ! dest and send tag
  dest     = mpi_yup_beam
  tag_send = 2*mpi_y

  ! source and receive tag
  source   = mpi_ydn_beam 
  tag_recv = 2*mpi_ydn_beam

  ! send and receive
  call MPI_ISEND(fSend, mBuffer, my_real, dest,   tag_send, mpi_comm_beam(2), req_send, mpi_err)
  call MPI_IRECV(    g, mBuffer, my_real, source, tag_recv, mpi_comm_beam(2), req_recv, mpi_err)    
  call MPI_WAIT (req_send, status, mpi_err)
  call MPI_WAIT (req_recv, status, mpi_err)  

end subroutine mpi_send_bdry_y_up


!-------------------------------------------------------------------------------
subroutine mpi_cart_gather_int(f, n, g) 
 
  ! Gather arrays of n integers (f) from all processes and store them in array g;
  ! MPI ranking: from Cartesian communicator (new_com)

  use params
  use mpi_mod
  implicit none

  integer :: n

  integer, dimension(n)           :: f
  integer, dimension(n, mpi_size) :: g

  call MPI_ALLGATHER (f, n, my_integer, g, n, my_integer, new_com, mpi_err)

end subroutine mpi_cart_gather_int


!-------------------------------------------------------------------------------
subroutine mpi_gather_slices_y(f, g) 

  ! Gather individual y-slices (dimension mx * mz) from all processes within the 
  ! same mpi_comm_beam(2) sub-communicator. 
  ! Stack all layers in array g; ranks within the sub-communicator are numbered
  ! from 0 (top sub-domain) to mpi_ny-1 (bottom sub-domain); layers in g are then
  ! stacked in the same order.

  use params
  use mpi_mod
  implicit none

  integer :: iy, iz
  integer :: mBuffer

  real, dimension(mx, 1, mz)         :: f
  real, dimension(mx, 1, mz, mpi_ny) :: g

  mBuffer = mx * mz

  call MPI_ALLGATHER(f, mBuffer, my_real, g, mBuffer, my_real, mpi_comm_beam(2), mpi_err)

end subroutine mpi_gather_slices_y


!-------------------------------------------------------------------------------
subroutine mpi_gather_single_layers_y(f, iy, g) 

  ! Gather single layers (number iy) from all processes within the 
  ! same mpi_comm_beam(2) sub-communicator. 
  ! Stack all layers in array g; Ranks within the sub-communicator are numbered
  ! from 0 (top sub-domain) to mpi_ny-1 (bottom sub-domain); layers in g are then
  ! stacked in the same order.

  use params
  use mpi_mod
  implicit none

  integer :: iy, iz
  integer :: mBuffer

  real, dimension(mx, my, mz)         :: f
  real, dimension(mx,  1, mz)         :: fSend
  real, dimension(mx,  1, mz, mpi_ny) :: g

  mBuffer = mx * mz

  do iz=izs,ize
     fSend(:,1,iz) = f(:,iy,iz)
  end do

  call MPI_ALLGATHER(fSend, mBuffer, my_real, g, mBuffer, my_real, mpi_comm_beam(2), mpi_err)

end subroutine mpi_gather_single_layers_y


!-------------------------------------------------------------------------------
subroutine mpi_gather_single_layers_y_dp(f, iy, g) 

  ! Gather single layers (number iy) from all processes within the 
  ! same mpi_comm_beam(2) sub-communicator. 
  ! Stack all layers in array g; Ranks within the sub-communicator are numbered
  ! from 0 (top sub-domain) to mpi_ny-1 (bottom sub-domain); layers in g are then
  ! stacked in the same order.

  use params
  use mpi_mod

  implicit none

  integer :: iy, iz
  integer :: mBuffer

  real(kind=8), dimension(mx, my, mz)         :: f
  real(kind=8), dimension(mx,  1, mz)         :: fSend
  real(kind=8), dimension(mx,  1, mz, mpi_ny) :: g

  mBuffer = mx * mz

  do iz=izs,ize
     fSend(:,1,iz) = f(:,iy,iz)
  end do

  call MPI_ALLGATHER(fSend, mBuffer, MPI_REAL8, g, mBuffer, MPI_REAL8, mpi_comm_beam(2), mpi_err)

end subroutine mpi_gather_single_layers_y_dp


!-------------------------------------------------------------------------------
subroutine mpi_send_x (f, g, mg, h, mh)

  !  Send the boundary layers from one domain over to the neighboring domains
  !  and receive the corresponding values into temporary arrays.
  !
  !  g is the RHS part of the f array, so is to be used on the LHS 
  !  h is the LHS part of the f array, so is to be used on the RHS

  use params
  use mpi_mod
  implicit none

  logical :: debug
  integer :: mg, mh, tag1, tag2, tag3, tag4
  integer :: status(MPI_STATUS_SIZE)
  integer :: request1, request2, request3, request4
  real, dimension(mx,my,mz) :: f
  real, dimension(mg,my,mz) :: g, gsend
  real, dimension(mh,my,mz) :: h, hsend

  !-------------------------------------------------------------------------------
  !  Single cpu runs, just wrap the array (assumes periodic y-conditions)
  !-------------------------------------------------------------------------------
  if (mpi_nx == 1) then
     if (mh .gt. 0) h(1:mh,:,izs:ize) = f(1:mh,:,izs:ize)
     if (mg .gt. 0) g(1:mg,:,izs:ize) = f(mx-mg+1:mx,:,izs:ize)
  else
     call barrier_omp('mpi_send_x begin')
     !$omp master

     !-------------------------------------------------------------------------------
     !  Send up and down
     !-------------------------------------------------------------------------------
     if (mh .gt. 0) then
        hsend = f(1:mh,:,:)
        tag1 = 2*mpi_x
        call MPI_ISEND (hsend, mh*my*mz, my_real, mpi_xdn_beam, tag1, mpi_comm_beam(1), request1, mpi_err)
        if (debug(dbg_mpi)) print *,'mpi_send_xdn:', mpi_rank, omp_mythread, mpi_xdn_beam, tag1, request1
        if (mpi_err .ne. 0) print *,'mpi_err:1',mpi_rank,mpi_err
        tag3= 2*mpi_xup_beam
        call MPI_IRECV (h    , mh*my*mz, my_real, mpi_xup_beam, tag3, mpi_comm_beam(1), request3, mpi_err)
        if (debug(dbg_mpi)) print *,'mpi_recv_xup:', mpi_rank, omp_mythread, mpi_xup_beam, tag3, request3
        if (mpi_err .ne. 0) print *,'mpi_err:2',mpi_rank,mpi_err
        mpi_sends = mpi_sends+1
        mpi_sent = mpi_sent+mh*my*mz
     end if
     if (mg .gt. 0) then
        gsend = f(mx-mg+1:mx,:,:)
        tag2 = 2*mpi_x + 1
        call MPI_ISEND (gsend, mg*my*mz, my_real, mpi_xup_beam, tag2, mpi_comm_beam(1), request2, mpi_err)
        if (debug(dbg_mpi)) print *,'mpi_send_xup:', mpi_rank, omp_mythread, mpi_xup_beam, tag2, request2
        if (mpi_err .ne. 0) print *,'mpi_err:3',mpi_rank,mpi_err
        tag4= 2*mpi_xdn_beam + 1
        call MPI_IRECV (g    , mg*my*mz, my_real, mpi_xdn_beam, tag4, mpi_comm_beam(1), request4, mpi_err)
        if (debug(dbg_mpi)) print *,'mpi_recv_xdn:', mpi_rank, omp_mythread, mpi_xdn_beam, tag4, request4
        if (mpi_err .ne. 0) print *,'mpi_err:4',mpi_rank,mpi_err
        mpi_sends = mpi_sends+1
        mpi_sent = mpi_sent+mg*my*mz
     end if

     !-------------------------------------------------------------------------------
     !  Wait as necessary (noop on many MPI implementations)
     !-------------------------------------------------------------------------------
     if (mh .gt. 0) then
        if (debug(dbg_mpi)) print *,'mpi_wait_send_xdn:', mpi_rank, omp_mythread, request1
        call MPI_WAIT (request1, status, mpi_err)
        if (debug(dbg_mpi)) print *,'mpi_wait_send_xdn:', mpi_rank, omp_mythread
        if (mpi_err .ne. 0) print *,'mpi_err:5',mpi_rank,mpi_err
        if (debug(dbg_mpi)) print *,'mpi_wait_recv_xup:', mpi_rank, omp_mythread, request3
        call MPI_WAIT (request3, status, mpi_err)
        if (debug(dbg_mpi)) print *,'mpi_wait_recv_xup:', mpi_rank, omp_mythread
        if (mpi_err .ne. 0) print *,'mpi_err:6',mpi_rank,mpi_err
        if (debug(dbg_mpi)) then
           print *,'mpi_send_x: sent', mpi_rank, mpi_xdn_beam, f(1:mh,my-6,mz)
           print *,'mpi_send_x: recv', mpi_rank, mpi_xup_beam, h(1:mh,my-6,mz)
        end if
     end if

     if (mg .gt. 0) then
        if (debug(dbg_mpi)) print *,'mpi_wait_send_xup:', mpi_rank, omp_mythread, request2
        call MPI_WAIT (request2, status, mpi_err)
        if (debug(dbg_mpi)) print *,'mpi_wait_send_xup:', mpi_rank, omp_mythread
        if (mpi_err .ne. 0) print *,'mpi_err:7',mpi_rank,mpi_err
        if (debug(dbg_mpi)) print *,'mpi_wait_recv_xdn:', mpi_rank, omp_mythread, request4
        call MPI_WAIT (request4, status, mpi_err)
        if (debug(dbg_mpi)) print *,'mpi_wait_recv_xdn:', mpi_rank, omp_mythread
        if (mpi_err .ne. 0) print *,'mpi_err:8',mpi_rank,mpi_err
        if (debug(dbg_mpi)) then
           print *,'mpi_send_x: sent', mpi_rank, mpi_yup_beam, f(mx-mg+1:mx,my-6,mz)
           print *,'mpi_send_x: recv', mpi_rank, mpi_ydn_beam, g(1:mg,my-6,mz)
        end if
     end if

     !$omp end master
     call barrier_omp('mpi_send_x end')
  end if
end subroutine mpi_send_x


!-------------------------------------------------------------------------------
subroutine mpi_send_x_begin (f, g, mg, h, mh)
  
  !  Send the boundary layers from one domain over to the neighboring domains
  !  and receive the corresponding values into temporary arrays.
  !
  !  g is the RHS part of the f array, so is to be used on the LHS 
  !  h is the LHS part of the f array, so is to be used on the RHS

  use params
  use mpi_mod
  implicit none

  logical :: debug
  integer :: mg, mh, tag1, tag2, tag3, tag4
  integer :: status(MPI_STATUS_SIZE)
  real, dimension(mx,my,mz) :: f
  real, dimension(mg,my,mz) :: g
  real, dimension(mh,my,mz) :: h

  !-------------------------------------------------------------------------------
  !  Single cpu runs, just wrap the array (assumes periodic y-conditions)
  !-------------------------------------------------------------------------------
  nxreq = 0
  if (mpi_nx > 1) then
     call barrier_omp('mpi_send_x begin')
     !$omp master
     if (mg > 0) allocate (gxsnd(mg,my,mz))
     if (mh > 0) allocate (hxsnd(mh,my,mz))

     !-------------------------------------------------------------------------------
     !  Post receives
     !-------------------------------------------------------------------------------
     if (mh .gt. 0) then
        nxreq = nxreq+1
        tag3= 2*mpi_xup_beam
        call MPI_IRECV (h, mh*my*mz, my_real, mpi_xup_beam, tag3, mpi_comm_beam(1), xrequest(nxreq), mpi_err)
        if (debug(dbg_mpi)) print *,'mpi_recv_xup:', mpi_rank, omp_mythread, mpi_xup_beam, tag3, xrequest(nxreq)
        if (mpi_err .ne. 0) print *,'mpi_err:2',mpi_rank,mpi_err
     end if
     if (mg .gt. 0) then
        nxreq = nxreq+1
        tag4= 2*mpi_xdn_beam + 1
        call MPI_IRECV (g, mg*my*mz, my_real, mpi_xdn_beam, tag4, mpi_comm_beam(1), xrequest(nxreq), mpi_err)
        if (debug(dbg_mpi)) print *,'mpi_recv_xdn:', mpi_rank, omp_mythread, mpi_xdn_beam, tag4, xrequest(nxreq)
        if (mpi_err .ne. 0) print *,'mpi_err:4',mpi_rank,mpi_err
     end if

     !-------------------------------------------------------------------------------
     !  Send up and down
     !-------------------------------------------------------------------------------
     if (mh .gt. 0) then
        hxsnd = f(1:mh,:,:)
        nxreq = nxreq+1
        tag1 = 2*mpi_x
        call MPI_ISEND (hxsnd, mh*my*mz, my_real, mpi_xdn_beam, tag1, mpi_comm_beam(1), xrequest(nxreq), mpi_err)
        if (debug(dbg_mpi)) print *,'mpi_send_xdn:', mpi_rank, omp_mythread, mpi_xdn_beam, tag1, xrequest(nxreq)
        if (mpi_err .ne. 0) print *,'mpi_err:1',mpi_rank,mpi_err
        mpi_sends = mpi_sends+1
        mpi_sent = mpi_sent+mh*my*mz
     end if
     if (mg .gt. 0) then
        gxsnd = f(mx-mg+1:mx,:,:)
        nxreq = nxreq+1
        tag2 = 2*mpi_x + 1
        call MPI_ISEND (gxsnd, mg*my*mz, my_real, mpi_xup_beam, tag2, mpi_comm_beam(1), xrequest(nxreq), mpi_err)
        if (debug(dbg_mpi)) print *,'mpi_send_xup:', mpi_rank, omp_mythread, mpi_xup_beam, tag2, xrequest(nxreq)
        if (mpi_err .ne. 0) print *,'mpi_err:3',mpi_rank,mpi_err
        mpi_sends = mpi_sends+1
        mpi_sent = mpi_sent+mg*my*mz
     end if

     !$omp end master
     call barrier_omp('mpi_send_x end')
  end if
end subroutine mpi_send_x_begin

!-------------------------------------------------------------------------------
subroutine mpi_send_x_end (f, g, mg, h, mh)
  
  !  Send the boundary layers from one domain over to the neighboring domains
  !  and receive the corresponding values into temporary arrays.
  !
  !  g is the RHS part of the f array, so is to be used on the LHS 
  !  h is the LHS part of the f array, so is to be used on the RHS
 
  use params
  use mpi_mod
  implicit none

  logical :: debug
  integer :: mg, mh, tag1, tag2, tag3, tag4
  integer :: status(MPI_STATUS_SIZE)
  integer :: request1, request2, request3, request4
  real, dimension(mx,my,mz) :: f
  real, dimension(mg,my,mz) :: g
  real, dimension(mh,my,mz) :: h

  !-------------------------------------------------------------------------------
  !  Single cpu runs, just wrap the array (assumes periodic y-conditions)
  !-------------------------------------------------------------------------------

  if (mpi_nx == 1) then
     if (mh .gt. 0) h(1:mh,:,izs:ize) = f(      1:mh,:,izs:ize)
     if (mg .gt. 0) g(1:mg,:,izs:ize) = f(mx-mg+1:mx,:,izs:ize)
  else

     !-------------------------------------------------------------------------------
     !  Wait as necessary (noop on many MPI implementations)
     !-------------------------------------------------------------------------------

     !$omp master
     call waitall_mpi (nxreq, xrequest)
     if (mg > 0) deallocate (gxsnd)
     if (mh > 0) deallocate (hxsnd)
     !$omp end master
  end if
end subroutine mpi_send_x_end


!-------------------------------------------------------------------------------
subroutine mpi_send_y (f, g, mg, h, mh)
  
  !  Send the boundary layers from one domain over to the neighboring domains
  !  and receive the corresponding values into temporary arrays.

  use params
  use mpi_mod
  implicit none

  logical :: debug
  integer :: mg, mh, tag1, tag2, tag3, tag4
  integer :: status(MPI_STATUS_SIZE)
  integer :: request1, request2, request3, request4
  real, dimension(mx,my,mz) :: f
  real, dimension(mx,mg,mz) :: g
  real, dimension(mx,mh,mz) :: h
  real, dimension(mx,mg,mz) :: gsend, grecv
  real, dimension(mx,mh,mz) :: hsend, hrecv

  !-------------------------------------------------------------------------------
  !  Single cpu runs, just wrap the array (assumes periodic y-conditions)
  !-------------------------------------------------------------------------------
  if (my == 1) then
     return
  else if (mpi_ny == 1) then
     !if (debug(dbg_mpi)) print *,'send_y:',izs,ize,omp_mythread
     if (mh .gt. 0) h(:,1:mh,izs:ize) = f(:,1:mh,izs:ize)
     if (mg .gt. 0) g(:,1:mg,izs:ize) = f(:,my-mg+1:my,izs:ize)
  else
     call barrier_omp('mpi_send_y begin')
     !$omp master

     !-------------------------------------------------------------------------------
     !  Send up and down
     !-------------------------------------------------------------------------------
     if (mh .gt. 0) then
        hsend = f(:,      1:mh,:)
        tag1 = 2*mpi_y
        call MPI_ISEND (hsend, mx*mh*mz, my_real, mpi_ydn_beam, tag1, mpi_comm_beam(2), request1, mpi_err)
        if (debug(dbg_mpi)) print 1,'mpi_send_ydn:', mpi_rank, omp_mythread, mpi_ydn_beam, tag1, request1
        if (mpi_err .ne. 0) print 1,'mpi_err:1',mpi_rank,mpi_err
        mpi_sends = mpi_sends+1
        mpi_sent = mpi_sent+mh*mx*mz
        tag3 = 2*mpi_yup_beam
        call MPI_IRECV (hrecv, mx*mh*mz, my_real, mpi_yup_beam, tag3, mpi_comm_beam(2), request3, mpi_err)
        if (debug(dbg_mpi)) print 1,'mpi_recv_ydn:', mpi_rank, omp_mythread, mpi_yup_beam, tag3, request3
        if (mpi_err .ne. 0) print 1,'mpi_err:2',mpi_rank,mpi_err
     end if

     if (mg .gt. 0) then
        gsend = f(:,my-mg+1:my,:)
        tag2 = 2*mpi_y + 1
        call MPI_ISEND (gsend, mx*mg*mz, my_real, mpi_yup_beam, tag2, mpi_comm_beam(2), request2, mpi_err)
        if (debug(dbg_mpi)) print 1,'mpi_send_yup:', mpi_rank, omp_mythread, mpi_yup_beam, tag2, request2
        if (mpi_err .ne. 0) print 1,'mpi_err:3',mpi_rank,mpi_err
        mpi_sends = mpi_sends+1
        mpi_sent = mpi_sent+mg*mx*mz
        tag4 = 2*mpi_ydn_beam + 1
        call MPI_IRECV (grecv, mx*mg*mz, my_real, mpi_ydn_beam, tag4, mpi_comm_beam(2), request4, mpi_err)
        if (debug(dbg_mpi)) print 1,'mpi_recv_yup:', mpi_rank, omp_mythread, mpi_ydn_beam, tag4, request4
        if (mpi_err .ne. 0) print 1,'mpi_err:4',mpi_rank,mpi_err
     end if

     !-------------------------------------------------------------------------------
     !  Wait as necessary (noop on many MPI implementations)
     !-------------------------------------------------------------------------------
     if (mh .gt. 0) then
        if (debug(dbg_mpi)) print 1,'mpi_wait_send_ydn:', mpi_rank, omp_mythread, request1
        call MPI_WAIT (request1, status, mpi_err)
        if (mpi_err .ne. 0) print 1,'mpi_err:5',mpi_rank,mpi_err
        if (debug(dbg_mpi)) print 1,'mpi_wait_recv_ydn:', mpi_rank, omp_mythread, request3
        call MPI_WAIT (request3, status, mpi_err)
        h = hrecv
        if (mpi_err .ne. 0) print 1,'mpi_err:6',mpi_rank,mpi_err
        if (debug(dbg_mpi)) then
           print 2,'mpi_send_ydn: sent', mpi_rank, omp_mythread, mpi_ydn_beam, f(mx-6,1:mh,1)
           print 2,'mpi_send_ydn: recv', mpi_rank, omp_mythread, mpi_yup_beam, h(mx-6,1:mh,1)
        end if
     end if

     if (mg .gt. 0) then
        if (debug(dbg_mpi)) print 1,'mpi_wait_send_yup:', mpi_rank, omp_mythread, request2
        call MPI_WAIT (request2, status, mpi_err)
        if (mpi_err .ne. 0) print 1,'mpi_err:7',mpi_rank,mpi_err
        if (debug(dbg_mpi)) print 1,'mpi_wait_recv_yup:', mpi_rank, omp_mythread, request4
        call MPI_WAIT (request4, status, mpi_err)
        g = grecv
        if (mpi_err .ne. 0) print 1,'mpi_err:8',mpi_rank,mpi_err
        if (debug(dbg_mpi)) then
           print 2,'mpi_send_yup: sent', mpi_rank, omp_mythread, mpi_yup_beam, f(mx-6,my-mg+1:my,1)
           print 2,'mpi_send_yup: recv', mpi_rank, omp_mythread, mpi_ydn_beam, g(mx-6,1:mg,1)
        end if
     end if

     !$omp end master
     call barrier_omp('mpi_send_y end')
  end if

1 format(1x,a,10i12)
2 format(1x,a,3i5,5f10.3)

end subroutine mpi_send_y


!-------------------------------------------------------------------------------
subroutine mpi_send_y_r8 (f, g, mg, h, mh)
  
  !  Send the boundary layers from one domain over to the neighboring domains
  !  and receive the corresponding values into temporary arrays.

  use params
  use mpi_mod
  implicit none

  logical :: debug
  integer :: mg, mh, tag1, tag2, tag3, tag4
  integer :: status(MPI_STATUS_SIZE)
  integer :: request1, request2, request3, request4

  real(8), dimension(mx,my,mz) :: f
  real(8), dimension(mx,mg,mz) :: g
  real(8), dimension(mx,mh,mz) :: h
  real(8), dimension(mx,mg,mz) :: gsend, grecv
  real(8), dimension(mx,mh,mz) :: hsend, hrecv

  !-------------------------------------------------------------------------------
  !  Single cpu runs, just wrap the array (assumes periodic y-conditions)
  !-------------------------------------------------------------------------------
  if (my == 1) then
     return
  else if (mpi_ny == 1) then
     !if (debug(dbg_mpi)) print *,'send_y:',izs,ize,omp_mythread
     if (mh .gt. 0) h(:,1:mh,izs:ize) = f(:,1:mh,izs:ize)
     if (mg .gt. 0) g(:,1:mg,izs:ize) = f(:,my-mg+1:my,izs:ize)
  else
     call barrier_omp('mpi_send_y begin')
     !$omp master

     !-------------------------------------------------------------------------------
     !  Send up and down
     !-------------------------------------------------------------------------------
     if (mh .gt. 0) then
        hsend = f(:,      1:mh,:)
        tag1 = 2*mpi_y
        call MPI_ISEND (hsend, mx*mh*mz, MPI_REAL8, mpi_ydn_beam, tag1, mpi_comm_beam(2), request1, mpi_err)
        if (debug(dbg_mpi)) print 1,'mpi_send_ydn:', mpi_rank, omp_mythread, mpi_ydn_beam, tag1, request1
        if (mpi_err .ne. 0) print 1,'mpi_err:1',mpi_rank,mpi_err
        mpi_sends = mpi_sends+1
        mpi_sent = mpi_sent+mh*mx*mz
        tag3 = 2*mpi_yup_beam
        call MPI_IRECV (hrecv, mx*mh*mz, MPI_REAL8, mpi_yup_beam, tag3, mpi_comm_beam(2), request3, mpi_err)
        if (debug(dbg_mpi)) print 1,'mpi_recv_ydn:', mpi_rank, omp_mythread, mpi_yup_beam, tag3, request3
        if (mpi_err .ne. 0) print 1,'mpi_err:2',mpi_rank,mpi_err
     end if

     if (mg .gt. 0) then
        gsend = f(:,my-mg+1:my,:)
        tag2 = 2*mpi_y + 1
        call MPI_ISEND (gsend, mx*mg*mz, MPI_REAL8, mpi_yup_beam, tag2, mpi_comm_beam(2), request2, mpi_err)
        if (debug(dbg_mpi)) print 1,'mpi_send_yup:', mpi_rank, omp_mythread, mpi_yup_beam, tag2, request2
        if (mpi_err .ne. 0) print 1,'mpi_err:3',mpi_rank,mpi_err
        mpi_sends = mpi_sends+1
        mpi_sent = mpi_sent+mg*mx*mz
        tag4 = 2*mpi_ydn_beam + 1
        call MPI_IRECV (grecv, mx*mg*mz, MPI_REAL8, mpi_ydn_beam, tag4, mpi_comm_beam(2), request4, mpi_err)
        if (debug(dbg_mpi)) print 1,'mpi_recv_yup:', mpi_rank, omp_mythread, mpi_ydn_beam, tag4, request4
        if (mpi_err .ne. 0) print 1,'mpi_err:4',mpi_rank,mpi_err
     end if

     !-------------------------------------------------------------------------------
     !  Wait as necessary (noop on many MPI implementations)
     !-------------------------------------------------------------------------------
     if (mh .gt. 0) then
        if (debug(dbg_mpi)) print 1,'mpi_wait_send_ydn:', mpi_rank, omp_mythread, request1
        call MPI_WAIT (request1, status, mpi_err)
        if (mpi_err .ne. 0) print 1,'mpi_err:5',mpi_rank,mpi_err
        if (debug(dbg_mpi)) print 1,'mpi_wait_recv_ydn:', mpi_rank, omp_mythread, request3
        call MPI_WAIT (request3, status, mpi_err)
        h = hrecv
        if (mpi_err .ne. 0) print 1,'mpi_err:6',mpi_rank,mpi_err
        if (debug(dbg_mpi)) then
           print 2,'mpi_send_ydn: sent', mpi_rank, omp_mythread, mpi_ydn_beam, f(mx-6,1:mh,1)
           print 2,'mpi_send_ydn: recv', mpi_rank, omp_mythread, mpi_yup_beam, h(mx-6,1:mh,1)
        end if
     end if

     if (mg .gt. 0) then
        if (debug(dbg_mpi)) print 1,'mpi_wait_send_yup:', mpi_rank, omp_mythread, request2
        call MPI_WAIT (request2, status, mpi_err)
        if (mpi_err .ne. 0) print 1,'mpi_err:7',mpi_rank,mpi_err
        if (debug(dbg_mpi)) print 1,'mpi_wait_recv_yup:', mpi_rank, omp_mythread, request4
        call MPI_WAIT (request4, status, mpi_err)
        g = grecv
        if (mpi_err .ne. 0) print 1,'mpi_err:8',mpi_rank,mpi_err
        if (debug(dbg_mpi)) then
           print 2,'mpi_send_yup: sent', mpi_rank, omp_mythread, mpi_yup_beam, f(mx-6,my-mg+1:my,1)
           print 2,'mpi_send_yup: recv', mpi_rank, omp_mythread, mpi_ydn_beam, g(mx-6,1:mg,1)
        end if
     end if

     !$omp end master
     call barrier_omp('mpi_send_y end')
  end if

1 format(1x,a,10i12)
2 format(1x,a,3i5,5f10.3)

end subroutine mpi_send_y_r8


!-------------------------------------------------------------------------------
subroutine mpi_send_z (f, g, mg, h, mh)
 
  !  Send the boundary layers from one domain over to the neighboring domains
  !  and receive the corresponding values into temporary arrays.

  use params
  use mpi_mod
  implicit none

  logical :: debug
  integer :: mg, mh
  integer :: status(MPI_STATUS_SIZE)
  integer :: request1, request2, request3, request4
  integer :: tag1, tag2, tag3, tag4, iz
  real, dimension(mx,my,mz) :: f
  real, dimension(mx,my,mg) :: g
  real, dimension(mx,my,mh) :: h
  real, dimension(mx,my,mg) :: gsend
  real, dimension(mx,my,mh) :: hsend

  !-------------------------------------------------------------------------------
  !  Single cpu runs, just wrap the array (assumes periodic y-conditions)
  !  NOTE: We need to assume that g and h are thread-local, so we must set
  !  them on all threads!
  !-------------------------------------------------------------------------------
  nzreq = 0
  if (mz == 1) then
     return
  else if (mpi_nz == 1) then
     call barrier_omp('send_z')
     !if (debug(dbg_mpi)) print *,'send_z:',izs,ize,omp_mythread
     if (mh .gt. 0) then
        h(:,:,1:mh) = f(:,:,1:mh)
     end if
     if (mg .gt. 0) then
        g(:,:,1:mg) = f(:,:,mz-mg+1:mz)
     end if
     call barrier_omp('send_z')
     !else if (omp_nthreads .gt. 1) then
     !  if (master) print *,'OMP cannot be combined with mpi_nz =',mpi_nz
     !  call end_mpi
     !  stop
  else
     call barrier_omp('send_z')
     !$omp master

     !-------------------------------------------------------------------------------
     !  Ship the low index layer to the next one down, receive same from next one up
     !-------------------------------------------------------------------------------
     if (mh .gt. 0) then
        do iz=1,mh
           hsend(:,:,iz) = f(:,:,iz)
        end do
        tag1 = 2*mpi_z
        nzreq = nzreq + 1
        call MPI_ISEND (hsend, mx*my*mh, my_real, mpi_zdn_beam, tag1, mpi_comm_beam(3), zrequest(nzreq), mpi_err)
        if (debug(dbg_mpi)) print 1,'mpi_send_zdn:',mpi_rank,omp_mythread,mpi_zdn_beam,tag1,zrequest(nzreq)
        tag2 = 2*mpi_zup_beam
        nzreq = nzreq + 1
        call MPI_IRECV (h    , mx*my*mh, my_real, mpi_zup_beam, tag2, mpi_comm_beam(3), zrequest(nzreq), mpi_err)
        if (debug(dbg_mpi)) print 1,'mpi_recv_zup:',mpi_rank,omp_mythread,mpi_zup_beam,tag2,zrequest(nzreq)
        mpi_sends = mpi_sends+1
        mpi_sent = mpi_sent+mh*mx*my
     end if

     !-------------------------------------------------------------------------------
     !  Ship the high index layer to the next one up, receive same from next one down
     !-------------------------------------------------------------------------------
     if (mg .gt. 0) then
        do iz=1,mg
           gsend(:,:,iz) = f(:,:,mz-mg+iz)
        end do
        tag3 = 2*mpi_z + 1
        nzreq = nzreq + 1
        call MPI_ISEND (gsend, mx*my*mg, my_real, mpi_zup_beam, tag3, mpi_comm_beam(3), zrequest(nzreq), mpi_err)
        if (debug(dbg_mpi)) print 1,'mpi_send_zup:',mpi_rank,omp_mythread,mpi_zup_beam,tag3,zrequest(nzreq)
        if (mpi_err .ne. 0) print 1,'ERROR',mpi_rank,omp_mythread
        tag4 = 2*mpi_zdn_beam + 1
        nzreq = nzreq + 1
        call MPI_IRECV (g    , mx*my*mg, my_real, mpi_zdn_beam, tag4, mpi_comm_beam(3), zrequest(nzreq), mpi_err)
        if (debug(dbg_mpi)) print 1,'mpi_recv_zdn:',mpi_rank,omp_mythread,mpi_zdn_beam,tag4,zrequest(nzreq)
        if (mpi_err .ne. 0) print 1,'ERROR',mpi_rank,omp_mythread
        mpi_sends = mpi_sends+1
        mpi_sent = mpi_sent+mg*mx*my
     end if

     !-------------------------------------------------------------------------------
     !  Wait as necessary (noop on many MPI implementations)
     !-------------------------------------------------------------------------------
     call waitall_mpi (nzreq, zrequest)

     !$omp end master
     call barrier_omp('mpi_send_z end')
  end if

1 format(1x,a,10i5)

end subroutine mpi_send_z


!-------------------------------------------------------------------------------
subroutine mpi_send_z_begin (f, g, mg, h, mh)

  !  Send the boundary layers from one domain over to the neighboring domains
  !  and receive the corresponding values into temporary arrays.

  use params
  use mpi_mod
  implicit none

  logical :: debug
  integer :: mg, mh
  integer :: status(MPI_STATUS_SIZE)
  integer :: request1, request2, request3, request4
  integer :: tag1, tag2, tag3, tag4, iz
  real, dimension(mx,my,mz) :: f
  real, dimension(mx,my,mg) :: g
  real, dimension(mx,my,mh) :: h

  !-------------------------------------------------------------------------------
  !  Single cpu runs, just wrap the array (assumes periodic y-conditions)
  !  NOTE: We need to assume that g and h are thread-local, so we must set
  !  them on all threads!
  !-------------------------------------------------------------------------------
  nzreq = 0
  if (mz == 1) then
     return
  else if (mpi_nz == 1) then
  else
     call barrier_omp('send_z')
     !$omp master
     if (mg > 0) allocate (gzsnd(mx,my,mg))
     if (mh > 0) allocate (hzsnd(mx,my,mh))

     !-------------------------------------------------------------------------------
     !  Ship the low index layer to the next one down, receive same from next one up
     !-------------------------------------------------------------------------------
     if (mh .gt. 0) then
        nzreq = nzreq + 1
        tag2 = 2*mpi_zup_beam
        call MPI_IRECV (h, mx*my*mh, my_real, mpi_zup_beam, tag2, mpi_comm_beam(3), zrequest(nzreq), mpi_err)
        if (debug(dbg_mpi)) print 1,'mpi_recv_zup:',mpi_rank,omp_mythread,mpi_zup_beam,tag2,zrequest(nzreq)
     end if

     !-------------------------------------------------------------------------------
     !  Ship the high index layer to the next one up, receive same from next one down
     !-------------------------------------------------------------------------------
     if (mg .gt. 0) then
        nzreq = nzreq + 1
        tag4 = 2*mpi_zdn_beam + 1
        call MPI_IRECV (g, mx*my*mg, my_real, mpi_zdn_beam, tag4, mpi_comm_beam(3), zrequest(nzreq), mpi_err)
        if (debug(dbg_mpi)) print 1,'mpi_recv_zdn:',mpi_rank,omp_mythread,mpi_zdn_beam,tag4,zrequest(nzreq)
        if (mpi_err .ne. 0) print 1,'ERROR',mpi_rank,omp_mythread
     end if

     !-------------------------------------------------------------------------------
     !  Ship the low index layer to the next one down, receive same from next one up
     !-------------------------------------------------------------------------------
     if (mh .gt. 0) then
        do iz=1,mh
           hzsnd(:,:,iz) = f(:,:,iz)
        end do
        nzreq = nzreq + 1
        tag1 = 2*mpi_z
        call MPI_ISEND (hzsnd, mx*my*mh, my_real, mpi_zdn_beam, tag1, mpi_comm_beam(3), zrequest(nzreq), mpi_err)
        if (debug(dbg_mpi)) print 1,'mpi_send_zdn:',mpi_rank,omp_mythread,mpi_zdn_beam,tag1,zrequest(nzreq)
        mpi_sends = mpi_sends+1
        mpi_sent = mpi_sent+mh*mx*my
     end if

     !-------------------------------------------------------------------------------
     !  Ship the high index layer to the next one up, receive same from next one down
     !-------------------------------------------------------------------------------
     if (mg .gt. 0) then
        do iz=1,mg
           gzsnd(:,:,iz) = f(:,:,mz-mg+iz)
        end do
        nzreq = nzreq + 1
        tag3 = 2*mpi_z + 1
        call MPI_ISEND (gzsnd, mx*my*mg, my_real, mpi_zup_beam, tag3, mpi_comm_beam(3), zrequest(nzreq), mpi_err)
        if (debug(dbg_mpi)) print 1,'mpi_send_zup:',mpi_rank,omp_mythread,mpi_zup_beam,tag3,zrequest(nzreq)
        if (mpi_err .ne. 0) print 1,'ERROR',mpi_rank,omp_mythread
        mpi_sends = mpi_sends+1
        mpi_sent = mpi_sent+mg*mx*my
     end if

     !$omp end master
     call barrier_omp('mpi_send_z end')
  end if

1 format(1x,a,10i5)

end subroutine mpi_send_z_begin


!-------------------------------------------------------------------------------
subroutine mpi_send_z_end (f, g, mg, h, mh)
 
  !  Send the boundary layers from one domain over to the neighboring domains
  !  and receive the corresponding values into temporary arrays.
  !

  use params
  use mpi_mod
  implicit none

  logical :: debug
  integer :: mg, mh
  integer :: status(MPI_STATUS_SIZE)
  integer :: request1, request2, request3, request4
  integer :: tag1, tag2, tag3, tag4, iz
  real, dimension(mx,my,mz) :: f
  real, dimension(mx,my,mg) :: g
  real, dimension(mx,my,mh) :: h

  !-------------------------------------------------------------------------------
  !  Single cpu runs, just wrap the array (assumes periodic y-conditions)
  !  NOTE: We need to assume that g and h are thread-local, so we must set
  !  them on all threads!
  !-------------------------------------------------------------------------------
  if (mz == 1) then
     return
  else if (mpi_nz == 1) then
     call barrier_omp('send_z')
     !if (debug(dbg_mpi)) print *,'send_z:',izs,ize,omp_mythread
     if (mh .gt. 0) then
        h(:,:,1:mh) = f(:,:,1:mh)
     end if
     if (mg .gt. 0) then
        g(:,:,1:mg) = f(:,:,mz-mg+1:mz)
     end if
     call barrier_omp('send_z')
     !else if (omp_nthreads .gt. 1) then
     !  if (master) print *,'OMP cannot be combined with mpi_nz =',mpi_nz
     !  call end_mpi
     !  stop
  else

     !-------------------------------------------------------------------------------
     !  Wait as necessary (noop on many MPI implementations)
     !-------------------------------------------------------------------------------
     !$omp master
     call waitall_mpi (nzreq, zrequest)
     if (mg > 0) deallocate (gzsnd)
     if (mh > 0) deallocate (hzsnd)
     !$omp end master
  end if

1 format(1x,a,10i5)

end subroutine mpi_send_z_end


!-------------------------------------------------------------------------------
subroutine zaverage_subr (f, av)

  use params
  implicit none

  real :: f(mx,my,mz)
  real(kind=8) :: av(mx,my)
  logical :: omp_in_parallel

  if (omp_in_parallel()) then
     call zaverage_omp (f, av)
  else
     !$omp parallel
     call zaverage_omp (f, av)
     !$omp end parallel
  end if

end subroutine zaverage_subr


!-------------------------------------------------------------------------------
subroutine zaverage_omp (f,av)

  use params
  use mpi_mod
  implicit none

  real, dimension(mx,my,mz), intent(in) :: f
  real(kind=8), dimension(mx,my) :: av
  integer :: i, j, k, size
  real(kind=8) :: sumz
  real(kind=8), allocatable, dimension(:,:,:) :: tmp8
  character(len=mid) :: id = 'zaverage_subr'
  !-------------------------------------------------------------------------------

  call print_id(id)

  !$omp master
  do i=1,mx
     do j=1,my
        sumz = 0.
        do k=1,mz
           sumz = sumz + f(i,j,k)
        end do
        av(i,j) = sumz
     end do
  end do

  if (mpi_nz > 1) then
     call MPI_COMM_SIZE (mpi_comm_beam(3), size, mpi_err)
     allocate (tmp8(mx,my,size))
     call MPI_ALLGATHER (av,mx*my,MPI_REAL8,tmp8,mx*my,MPI_REAL8,mpi_comm_beam(3),mpi_err) 
     do j=1,my
        do i=1,mx
           av(i,j) = sum(tmp8(i,j,1:size))
        end do
     end do
     deallocate (tmp8)
  end if
  av = av/dble(mztot)
  !$omp end master
  call barrier_omp('zaver')

end subroutine zaverage_omp


!-------------------------------------------------------------------------------
subroutine haverage_subr (f, av)
  use params
  implicit none

  real :: f(mx,my,mz)
  real(kind=8) :: av(my)
  logical :: omp_in_parallel

  if (omp_in_parallel()) then
     call haverage_omp (f, av)
  else
     !$omp parallel
     call haverage_omp (f, av)
     !$omp end parallel
  end if
end subroutine haverage_subr

!-------------------------------------------------------------------------------
subroutine haverage_omp (f,av)

  use params
  use mpi_mod
  implicit none

  real, dimension(mx,my,mz), intent(in) :: f
  real(kind=8), dimension(my) :: av
  real(kind=8), dimension(my) :: av1, sumy
  integer :: i, j, k, size
  real(kind=8) :: sumx
  real(kind=8), allocatable, dimension(:,:) :: tmp8
  character(len=mid) :: id = 'haverage_subr'

  
  call print_id(id)

  do k=izs,ize
     do j=1,my
        sumx = 0.
        do i=1,mx
           sumx = sumx + f(i,j,k)
        end do
        sumy(j) = sumx
     end do
     haver_tmp(1:my,k) = sumy(1:my)
  end do

  call barrier_omp('haver3')

  !$omp master
  do j=1,my
     av(j) = sum(haver_tmp(j,:))  ! just sum, average later
  end do
  !$omp end master

  call barrier_omp('haver4')

  !$omp master
  size = mpi_nx*mpi_nz
  if (size > 1) then
     av1 = av
     if (do_validate) then
        call MPI_COMM_SIZE(mpi_comm_plane(2), size, mpi_err)
        allocate (tmp8(my,size))
        call MPI_ALLGATHER(av1, my, MPI_REAL8, tmp8, my, MPI_REAL8, mpi_comm_plane(2), mpi_err) 
        do j=1,my
           av(j) = sum(tmp8(j,1:size))
        end do
        deallocate (tmp8)
     else
        call MPI_ALLREDUCE(av1, av, my, MPI_REAL8, MPI_SUM, mpi_comm_plane(2), mpi_err)
     end if
  end if
  av = av/dble(size*mx*mz)
  !$omp end master
  call barrier_omp('haver5')

end subroutine haverage_omp

!-------------------------------------------------------------------------------
subroutine haverage_bdry (f, av)

  use params
  use mpi_mod
  implicit none

  real :: f(mx,my,mz)
  real(kind=8) :: av(my)
  logical :: omp_in_parallel

  if (omp_in_parallel()) then
     call haverage_bdry_omp (f, av)
  else
     !$omp parallel
     call haverage_bdry_omp (f, av)
     !$omp end parallel
  end if

end subroutine haverage_bdry


!-------------------------------------------------------------------------------
subroutine haverage_bdry_omp (f, av)

  use params
  use mpi_mod
  implicit none

  !  Return horizontal average of array f, as in haverage_omp, but only
  !  for the boundary layers.

  real :: f(mx,my,mz)
  real(kind=8) :: av(my)
  real(kind=8) :: av1(my), sumy(my), sumx
  real(kind=8), allocatable, dimension(:,:) :: tmp8
  integer :: i, j, k, lb1, ub1, size

  
  do k=izs,ize
     do j=1,lb
        sumx = 0.
        do i=1,mx
           sumx = sumx + f(i,j,k)
        end do
        sumy(j) = sumx
     end do
     haver_tmp(1:lb,k) = sumy(1:lb)
     do j=ub,my
        sumx = 0.
        do i=1,mx
           sumx = sumx + f(i,j,k)
        end do
        sumy(j) = sumx
     end do
     haver_tmp(ub:my,k) = sumy(ub:my)
  end do
  call barrier_omp('haver_bdry')

  do j=1,lb
     av(j)=sum(haver_tmp(j,:))
  enddo
  do j=ub,my
     av(j)=sum(haver_tmp(j,:))
  enddo

  !$omp master
  size = mpi_nx*mpi_nz
  if (size > 1) then
     av1 = av
     if (do_validate) then
        call MPI_COMM_SIZE(mpi_comm_plane(2), size, mpi_err)
        allocate (tmp8(my,size))
        call MPI_ALLGATHER(av1, my, MPI_REAL8, tmp8, my, MPI_REAL8, mpi_comm_plane(2), mpi_err) 
        do j=1,my
           av(j) = sum(tmp8(j,1:size))
        end do
        deallocate (tmp8)
     else
        call MPI_ALLREDUCE(av1, av, my, MPI_REAL8, MPI_SUM, mpi_comm_plane(2), mpi_err)
     end if
  end if
  av = av/dble(size*mx*mz)
  !$omp end master
  call barrier_omp('haver')

  do j=1,lb
     av(j)=av(j)/dble(mxtot*mztot)
  enddo
  do j=ub,my
     av(j)=av(j)/dble(mxtot*mztot)
  enddo

  !if (do_trace) print *,mpi_rank,lb,ub,av(1)
end subroutine haverage_bdry_omp

!-------------------------------------------------------------------------------
subroutine haverage_mpi (av, m)

  use params
  use mpi_mod
  implicit none

  real(kind=8), dimension(m) :: av
  real(kind=8), dimension(m) :: av1
  integer :: m
  integer :: i, n1, n, size
  real :: w
  real(kind=8), allocatable, dimension(:,:) :: tmp8
  character(len=mid):: id = 'haverage_mpi'

  
  call print_id(id)

  call barrier_omp('haver1')
  !$omp master
  size = mpi_nx*mpi_nz
  if (size > 1) then
     av1 = av
     call MPI_COMM_SIZE(mpi_comm_plane(2), size, mpi_err)
     if (do_validate) then
        allocate (tmp8(m,size))
        call MPI_ALLGATHER(av1, m, MPI_REAL8, tmp8, m, MPI_REAL8, mpi_comm_plane(2), mpi_err) 
        do i=1,m
           av(i) = sum(tmp8(i,1:size))/dble(size)
        end do
        deallocate (tmp8)
     else
        call MPI_ALLREDUCE(av1, av, m, MPI_REAL8,MPI_SUM, mpi_comm_plane(2), mpi_err)
        av = av/dble(size)
     end if
  end if
  !$omp end master
  call barrier_omp('haver2')

end subroutine haverage_mpi


!-------------------------------------------------------------------------------
subroutine test_haver

  use params
  use arrays, only: scr1
  use mpi_mod
  implicit none

  real(kind=8), dimension(my) :: av
  real, parameter :: eps=1e-7
  integer :: ix,iy,iz
  real :: err

  do iz=1,mz
     do iy=1,my
        scr1(:,iy,iz) = iy
     end do
  end do
  !$omp parallel
  call haverage_bdry (scr1, av)
  !$omp end parallel

  err = 0.
  do iy=1,lb
     err = max(err, abs(real(av(iy)-iy)))
  end do
  do iy=ub,my
     err = max(err, abs(real(av(iy)-iy)))
  end do
  call assert_small ('haverage_bdry', err, eps*my)

  call haverage_subr (scr1, av)
  err = 0.
  do iy=1,my
     err = max(err, abs(real(av(iy)-iy)))
  end do
  call assert_small ('haverage_subr', err, eps*my)

end subroutine test_haver


!-------------------------------------------------------------------------------
subroutine test_zaver

  use params
  use arrays, only: scr1
  use mpi_mod
  implicit none

  real(kind=8), dimension(mx,my) :: av
  real, parameter :: eps=1e-7
  integer :: ix,iy,iz
  real :: err
  !
  do iz=1,mz
     do iy=1,my
        do ix=1,mx
           scr1(ix,iy,iz) = ix+1000*iy
        end do
     end do
  end do

  !$omp parallel
  call zaverage_subr (scr1, av)
  !$omp end parallel
  err = 0.
  do iy=1,my
     do ix=1,mx
        err = max(err, abs(real(av(ix,iy)-ix-1000*iy)))
     end do
  end do
  call assert_small ('zaverage_subr(OMP)', err, eps*my)

  call zaverage_subr (scr1, av)
  err = 0.
  do iy=1,my
     do ix=1,mx
        err = max(err, abs(real(av(ix,iy)-ix-1000*iy)))
     end do
  end do
  call assert_small ('zaverage_subr(SER)', err, eps*my)

end subroutine test_zaver

!-------------------------------------------------------------------------------
function average(f)

  use params
  implicit none

  real, dimension(mx,my,mz), intent(in) :: f
  real average, av

  call average_subr (f, av)
  average = av

end function average

!-------------------------------------------------------------------------------
subroutine average_subr (f,average)

  use params
  implicit none

  real :: f(mx,my,mz), average
  logical :: omp_in_parallel

  if (omp_in_parallel()) then
     call average_omp(f,average)
  else
     !$omp parallel shared(average)
     call average_omp(f,average)
     !$omp end parallel
  end if

end subroutine average_subr

!-------------------------------------------------------------------------------
subroutine average_omp (f, av)

  !  Return average of array.  Make sure precision is retained, by 
  !  summing first individual rows, then columns, then planes.
  
  use params
  use mpi_mod
  implicit none

  real :: f(mx,my,mz), av
  integer :: i, j, k, lb1, ub1, npt
  integer(kind=8) :: ntot, ntmp
  integer, allocatable, dimension(:,:) :: npts
  real(kind=8) :: sumx, sumy, sumz, av1, av2
  real(kind=8), allocatable, dimension(:,:) :: tmp8
  logical :: debug
  character(len=mid):: id = 'average_omp'

  call print_id(id)

  !$omp single
  dbl = 0.
  !$omp end single
  lb1 = max(min(lb,my),1)
  ub1 = max(min(ub,my),1)
  sumz = 0.
  do k=izs,ize
     sumy = 0.
     do j=lb1,ub1
        sumx = 0.
        do i=1,mx
           sumx = sumx + f(i,j,k)
        end do
        sumy = sumy + sumx
     end do
     sumz = sumz + sumy
  end do
  !$omp critical
  dbl = dbl + sumz
  !$omp end critical
  call barrier_omp('av_omp1')

  !$omp master
  av1 = dbl
  !if (do_validate) then
  !  allocate (tmp8(1,mpi_size), npts(1,mpi_size))
  !  npt = mx*(ub1-lb1+1)*mz
  !  call MPI_ALLGATHER (av1,1,MPI_REAL8,tmp8,1,MPI_REAL8,mpi_com,mpi_err) 
  !  call MPI_ALLGATHER (npt,1,my_integer,npts,1,my_integer,mpi_com,mpi_err) 
  !  dbl = sum(tmp8(1,1:mpi_size))/sum(int(npts(1,1:mpi_size),kind=8))
  !  deallocate (tmp8, npts)
  !else
  call MPI_ALLREDUCE(av1,dbl,1,MPI_REAL8,MPI_SUM,mpi_com,mpi_err)
  ntmp = mx*(ub1-lb1+1)*mz
  call MPI_ALLREDUCE(ntmp,ntot,1,MPI_INTEGER8,MPI_SUM,mpi_com,mpi_err)
  dbl = dbl/ntot
  !end if
  !$omp end master
  call barrier_omp('av_omp2e')

  if (debug(dbg_mpi)) &
       write(*,'(1x,a,i3,2e14.6,2i7)') trim(id)//': rank,av1 =',mpi_rank,av1
  !$omp critical
  av = dbl
  !$omp end critical

  call barrier_omp('av_omp3')

end subroutine average_omp

!-------------------------------------------------------------------------------
subroutine sum_int_mpi (a)

  use mpi_mod
  implicit none
  integer :: a, a1

  !$omp barrier
  !$omp master
  call MPI_ALLREDUCE(a,a1,1,MPI_INTEGER,MPI_SUM,mpi_com,mpi_err)
  a = a1
  !$omp end master
  !$omp barrier

end subroutine sum_int_mpi

!-------------------------------------------------------------------------------
subroutine sum_int8_mpi (a)

  use mpi_mod
  implicit none
  integer(kind=8) :: a, a1

  !$omp barrier
  !$omp master
  call MPI_ALLREDUCE(a,a1,1,MPI_INTEGER8,MPI_SUM,mpi_com,mpi_err)
  a = a1
  !$omp end master
  !$omp barrier

end subroutine sum_int8_mpi

!-------------------------------------------------------------------------------
subroutine decrement_mpi (i)

  use mpi_mod
  use params, only: master
  implicit none
  integer :: i, i1

  !$omp barrier
  !$omp master
  if (master) i=i-1                                           ! decrement master
  if (master) print*,'decrement_mpi:',i
  call MPI_BCAST (i, 1, MPI_INTEGER, 0, mpi_comm_world, mpi_err)     ! broadcast
  !$omp end master
  !$omp barrier

end subroutine decrement_mpi

!-------------------------------------------------------------------------------
subroutine min_real_mpi (a)

  use mpi_mod
  implicit none
  real :: a, a1

  !$omp barrier
  !$omp master
  call MPI_ALLREDUCE(a,a1,1,MPI_REAL,MPI_MIN,mpi_com,mpi_err)
  a = a1
  !$omp end master
  !$omp barrier

end subroutine min_real_mpi


!-------------------------------------------------------------------------------
subroutine max_real_mpi (a)

  use mpi_mod
  implicit none
  real :: a, a1

  !$omp barrier
  !$omp master
  call MPI_ALLREDUCE(a,a1,1,MPI_REAL,MPI_MAX,mpi_com,mpi_err)
  a = a1
  !$omp end master
  !$omp barrier

end subroutine max_real_mpi

!-------------------------------------------------------------------------------
subroutine min_int_mpi (a)

  use mpi_mod
  implicit none
  integer :: a, a1

  !$omp barrier
  !$omp master
  call MPI_ALLREDUCE(a,a1,1,MPI_INTEGER,MPI_MIN,mpi_com,mpi_err)
  a = a1
  !$omp end master
  !$omp barrier

end subroutine min_int_mpi

!-------------------------------------------------------------------------------
subroutine max_int_mpi (a)

  use mpi_mod
  implicit none
  integer :: a, a1

  !$omp barrier
  !$omp master
  call MPI_ALLREDUCE(a,a1,1,MPI_INTEGER,MPI_MAX,mpi_com,mpi_err)
  a = a1
  !$omp end master
  !$omp barrier

end subroutine max_int_mpi

!-------------------------------------------------------------------------------
subroutine max_int_mpi_comm (a, comm)

  use mpi_mod
  implicit none
  integer :: a, a1, comm

  !$omp barrier
  !$omp master
  call MPI_ALLREDUCE(a,a1,1,MPI_INTEGER,MPI_MAX,comm,mpi_err)
  a = a1
  !$omp end master
  !$omp barrier

end subroutine max_int_mpi_comm

!-------------------------------------------------------------------------------
subroutine max_int8_mpi (a)

  use mpi_mod
  implicit none
  integer(kind=8) :: a, a1

  !$omp barrier
  !$omp master
  call MPI_ALLREDUCE(a,a1,1,MPI_INTEGER8,MPI_MAX,mpi_com,mpi_err)
  a = a1
  !$omp end master
  !$omp barrier

end subroutine max_int8_mpi

!-------------------------------------------------------------------------------
subroutine sum_real_mpi (a)

  use mpi_mod
  implicit none
  real :: a
  real(kind=8) :: tmp, sum

  !$omp barrier
  !$omp master
  tmp = a
  call MPI_ALLREDUCE(tmp,sum,1,MPI_REAL8,MPI_SUM,mpi_com,mpi_err)
  a = sum
  !$omp end master
  !$omp barrier

end subroutine sum_real_mpi

!-------------------------------------------------------------------------------
subroutine sum_real8_mpi (a)

  use mpi_mod
  implicit none
  real(kind=8) :: a
  real(kind=8) :: tmp, sum

  !$omp barrier
  !$omp master
  tmp = a
  call MPI_ALLREDUCE(tmp,sum,1,MPI_REAL8,MPI_SUM,mpi_com,mpi_err)
  a = sum
  !$omp end master
  !$omp barrier

end subroutine sum_real8_mpi

!-------------------------------------------------------------------------------
subroutine sum_real8s_mpi (a, n)
  use mpi_mod
  implicit none
  integer :: n
  real(kind=8), dimension(n) :: a, sum

  !$omp barrier
  !$omp master
  call MPI_ALLREDUCE(a,sum,n,MPI_REAL8,MPI_SUM,mpi_com,mpi_err)
  a = sum
  !$omp end master
  !$omp barrier

end subroutine sum_real8s_mpi

!-------------------------------------------------------------------------------
subroutine fminval_mpi (label, f, fmin)

  use params
  use mpi_mod
  implicit none

  character(len=*) :: label
  real, dimension(mx,my,mz) :: f
  real :: fmin, fmin1

  !call fminval_omp(label,f,fmin)
  call barrier_omp('minval')
  !$omp single
  fmin = minval(f)
  fmin1 = fmin
  call MPI_ALLREDUCE(fmin1,fmin,1,my_real,MPI_MIN,mpi_com,mpi_err)
  !$omp end single

end subroutine fminval_mpi

!-------------------------------------------------------------------------------
subroutine fmaxval_mpi (label, f, fmax)

  use params
  use mpi_mod
  implicit none

  character(len=*) :: label
  real, dimension(mx,my,mz) :: f
  real :: fmax, fmax1

  !call fmaxval_omp(label,f,fmax)
  call barrier_omp('maxval')
  !$omp single
  fmax = maxval(f)
  fmax1 = fmax
  call MPI_ALLREDUCE(fmax1,fmax,1,my_real,MPI_MAX,mpi_com,mpi_err)
  !$omp end single

end subroutine fmaxval_mpi


!-------------------------------------------------------------------------------
integer function imaxval_mpi (i)

  use mpi_mod
  implicit none
  integer :: i, imax

  call barrier_omp('imaxval_mpi')
  !$omp master
  call MPI_ALLREDUCE(i,imax,1,my_integer,MPI_MAX,mpi_com,mpi_err)
  imaxval_mpi = imax
  !$omp end master
  call barrier_omp('imaxval_mpi')

end function imaxval_mpi

!-------------------------------------------------------------------------------
real function rmaxval_mpi (r)

  use mpi_mod
  implicit none
  real :: r, rmax

  call barrier_omp('imaxval_mpi')
  !$omp master
  call MPI_ALLREDUCE(r,rmax,1,my_real,MPI_MAX,mpi_com,mpi_err)
  rmaxval_mpi = rmax
  !$omp end master
  call barrier_omp('rmaxval_mpi')

end function rmaxval_mpi

!-------------------------------------------------------------------------------
subroutine imaxval_subr_mpi (i)

  use mpi_mod
  implicit none
  integer :: i, imax

  call barrier_omp('imaxval_subr_mpi')
  !$omp master
  call MPI_ALLREDUCE(i,imax,1,my_integer,MPI_MAX,mpi_com,mpi_err)
  i = imax
  !$omp end master
  call barrier_omp('imaxval_subr_mpi')

end subroutine imaxval_subr_mpi

!-------------------------------------------------------------------------------
subroutine imaxval2_subr_mpi (i1, i2)

  use mpi_mod
  implicit none

  integer :: i1, i2, l(2), g(2)
  call barrier_omp('imaxval_subr_mpi')
  !$omp master
  l = (/i1, i2/)
  call MPI_ALLREDUCE(l,g,2,my_integer,MPI_MAX,mpi_com,mpi_err)
  i1 = g(1)
  i2 = g(2)
  !$omp end master
  call barrier_omp('imaxval_subr_mpi')
end subroutine imaxval2_subr_mpi

!-------------------------------------------------------------------------------
function fmaxval(label,f)
  
  !  Max value returned, and written to file

  use params
  use mpi_mod
  implicit none

  character(len=*) :: label
  real, dimension(mx,my,mz) :: f
  real :: fmaxval, fmaxloc
  integer :: iz, loc1(1), loc2(2), im, jm, km, lb1, ub1
  logical :: debug, omp_in_parallel
  character(len=1) :: line
  character(len=mid) :: id = 'fmaxval'

  call print_id(id)
  !call fmaxval_subr (label, f, fmaxval)
  fmaxval = maxval(f)
  call max_real_mpi (fmaxval)

end function fmaxval

!-------------------------------------------------------------------------------
subroutine fmax_mpi (im, jm, km, fmax)

  use params
  use mpi_mod
  implicit none
  integer :: im, jm, km
  real :: fmax
  integer :: sendcnt, rank
  real :: sendbuf(4), recvbuf(4,mpi_size)

  sendcnt = 4
  sendbuf = (/im+ixoff+0.5,jm+iyoff+0.5,km+izoff+0.5,fmax /)
  call MPI_ALLGATHER (sendbuf,sendcnt,my_real, &
       recvbuf,sendcnt,my_real,mpi_com,mpi_err)
  fmax = maxval(recvbuf(4,:))
  do rank=1,mpi_size
     if (recvbuf(4,rank) == fmax) then
        im = recvbuf(1,rank)
        jm = recvbuf(2,rank)
        km = recvbuf(3,rank)
     end if
  end do

end subroutine fmax_mpi

!-------------------------------------------------------------------------------
subroutine fmin_mpi (im, jm, km, fmin)

  use params
  use mpi_mod
  implicit none

  integer :: im, jm, km
  real :: fmin
  integer :: sendcnt, rank
  real :: sendbuf(4), recvbuf(4,mpi_size)

  sendcnt = 4
  sendbuf = (/im+ixoff+0.5,jm+iyoff+0.5,km+izoff+0.5,fmin /)
  call MPI_ALLGATHER (sendbuf,sendcnt,my_real, &
       recvbuf,sendcnt,my_real,mpi_com,mpi_err)
  fmin = minval(recvbuf(4,:))
  do rank=1,mpi_size
     if (recvbuf(4,rank) == fmin) then
        im = recvbuf(1,rank)
        jm = recvbuf(2,rank)
        km = recvbuf(3,rank)
     end if
  end do

end subroutine fmin_mpi

!-------------------------------------------------------------------------------
subroutine horiz_minmax(f, fmin, fmax)
 
  ! Returns minima and maxima of f on horizontal planes

  use params
  use mpi_mod
  implicit none

  real,   dimension(mx,my,mz) :: f
  real(kind=8), dimension(my) :: fmin0, fmax0, fmin, fmax
  integer :: iy

  call barrier_omp('horiz_minmax')
  !$omp master

  do iy=1,my
     fmin0(iy) = dble( minval(f(:,iy,:)) )
     fmax0(iy) = dble( maxval(f(:,iy,:)) )
  end do

  call MPI_BARRIER(mpi_com, mpi_err)

  call MPI_ALLREDUCE(fmin0, fmin, my, MPI_DOUBLE_PRECISION, MPI_MIN, mpi_comm_plane(2), mpi_err)
  call MPI_ALLREDUCE(fmax0, fmax, my, MPI_DOUBLE_PRECISION, MPI_MAX, mpi_comm_plane(2), mpi_err)

  !$omp end master
  call barrier_omp('horiz_minmax')

end subroutine horiz_minmax

!-------------------------------------------------------------------------------
subroutine haver_updn(f, py, favUp, favDn, ffUp, ffDn)
 
  ! computes the horizontal averages of f in upflows 
  ! and downflows separately
  !
  ! f             : data cube
  ! py            : vertical momentum
  ! favUp (favDn) : horizontal average of f in upflows (downflows) only
  ! ffUp (ffDn)   : filling factor upflows (downflows)

  use params
  use mpi_mod
  implicit none

  real, dimension(mx,my,mz), intent(in) :: f, py
  real,    dimension(mx,mz) :: fSlice, pySlice, fSliceUp, fSliceDn
  integer, dimension(mx,mz) :: maskUp, maskDn

  real(kind=8), dimension(my), intent(out) :: favUp,  favDn,  ffUp,  ffDn
  real(kind=8), dimension(my) :: favUp0, favDn0, ffUp0, ffDn0
  integer,      dimension(my) :: numUp, numDn, numUp0, numDn0
  integer      :: iy, iz
  real(kind=8) :: size

  
  size = dble( mxtot * mztot ) 

  do iy=1,my

     do iz=1,mz
        fSlice(:,iz) =  f(:,iy,iz)
        pySlice(:,iz)= py(:,iy,iz)
     enddo

     ! py >  0: downflows, py <= 0: upflows
     where (pySlice .gt. 0.0)
        maskDn   = 1
        maskUp   = 0
        fSliceDn = fSlice
        fSliceUp = 0.0
     elsewhere
        maskDn   = 0
        maskUp   = 1
        fSliceDn = 0.0
        fSliceUp = fSlice        
     endwhere

     numUp0(iy) = sum(maskUp)
     numDn0(iy) = sum(maskDn)

     favUp0(iy) = sum( dble(fSliceUp) )
     favDn0(iy) = sum( dble(fSliceDn) ) 

  enddo

  call MPI_BARRIER(mpi_com, mpi_err)
  call MPI_ALLREDUCE(favUp0, favUp, my, MPI_DOUBLE_PRECISION, MPI_SUM, mpi_comm_plane(2), mpi_err)
  call MPI_ALLREDUCE(favDn0, favDn, my, MPI_DOUBLE_PRECISION, MPI_SUM, mpi_comm_plane(2), mpi_err)
  call MPI_ALLREDUCE(numUp0, numUp, my, my_integer, MPI_SUM, mpi_comm_plane(2), mpi_err)
  call MPI_ALLREDUCE(numDn0, numDn, my, my_integer, MPI_SUM, mpi_comm_plane(2), mpi_err)

  where ( numUp .gt. 0) 
     favUp = favUp / dble( numUp )
  elsewhere
     favUp = 0.0
  endwhere

  where ( numDn .gt. 0) 
     favDn = favDn / dble( numDn )
  elsewhere
     favDn = 0.0
  endwhere

  ffUp(:) = dble( numUp(:) ) / size
  ffDn(:) = dble( numDn(:) ) / size

end subroutine haver_updn

!-------------------------------------------------------------------------------
subroutine read_chunk (ff, io_type, iv, ird, unit)

  use params
  use variables, only: py
  use mpi_mod
  implicit none

  real, dimension(mx,my,mz) :: ff
  integer :: unit, iv, ird, rec, io_type
  integer :: status(MPI_STATUS_SIZE)
  integer :: ix, iy, iz, irec, mv
  integer :: i_x, i_y, i_z, mpi_to, rank, nx, ny, ix1, ix2, iy1, iy2
  integer :: request,coord(3)
  logical :: debug, omp_in_parallel
  real(kind=4), allocatable, dimension(:,:) :: piece
  real(kind=4), allocatable, dimension(:,:,:) :: f

  if (omp_in_parallel() .and. omp_mythread>0) then
     print*,'ERROR: read_chunk called from slave OMP thread'
     stop
  end if
  allocate (f(mx,my,mz))
  rec = iv + ird*mvar + 1

  !-------------------------------------------------------------------------------
  ! Single file, but handled by master (or last thread -- extra I/O node)
  !-------------------------------------------------------------------------------

  !print*,'read_chunk, io_type = ',io_type
  if (io_type==2) then
     !print*,'py_traceP',omp_mythread,py(1,1,21)
     if (mpi_rank == io_rank) then
        allocate (buffer(mxtot,mytot))                                            ! I/O buffer
        if (master .and. debug(dbg_io)) print*,'allocated buffer, words =',mxtot*mytot
     end if
     if (master .and. debug(dbg_io)) print*,'read_chunk: global slices with',mxtot*mytot*4,' bytes'
     !
     do i_z = 0,mpi_nz-1                                                                 ! loop over z-domains
        if (master .and. debug(dbg_io)) print*,'z-domain =',i_z
        do iz = 1,mz_mpi(i_z)                                                            ! loop over z-slices
           if (mpi_rank == io_rank) then
              irec = iz + oz_mpi(i_z) + (rec-1)*mztot                                    ! record number (izoff BUG corrected)
              read  (unit,rec=irec) buffer                                               ! read in
              !if (debug(dbg_io)) &
              !  print *, 'read_chunk: read; rank, iv =', mpi_rank, iv
           end if
           !call MPI_BARRIER (mpi_com, mpi_err)                                          ! avoid large send queue

           do i_y = 0,mpi_ny-1                                                           ! y-domains
              do i_x = 0,mpi_nx-1                                                        ! x-domains
                 coord = (/ i_x, i_y, i_z /)                                             ! sender MPI-coordinates
                 call MPI_CART_RANK (new_com, coord, mpi_to, mpi_err)                    ! convert to rank
                 nx = mx_mpi(i_x)
                 ny = my_mpi(i_y)
                 allocate (piece(nx,ny))
                 if (mpi_rank == io_rank) then                                           ! I/O node
                    ix1 = 1+ox_mpi(i_x); ix2=ix1-1+mx_mpi(i_x)
                    iy1 = 1+oy_mpi(i_y); iy2=iy1-1+my_mpi(i_y)
                    piece = buffer(ix1:ix2,iy1:iy2)                                      ! get piece from buffer
                    if (mpi_rank == mpi_to) then                                         ! if this one
                       !if (debug(dbg_io)) &
                       !  print *, 'read_chunk: copy; rank, iv =', mpi_rank, iv, iz, i_z
                       f(:,:,iz) = piece                                                 ! just copy the piece
                    else                                                                 ! if not ..
                       !if (debug(dbg_io)) &
                       !  print *, 'read_chunk: send; rank, iv =', mpi_rank, iv, iz, i_z
                       call MPI_ISEND (piece, nx*ny, MPI_REAL, mpi_to, mpi_to, &         ! receive piece
                            mpi_com, request, mpi_err)
                       call MPI_WAIT (request, status, mpi_err)                          ! wait for completion
                    end if
                 else if (mpi_rank == mpi_to) then                                       ! if this one
                    !if (debug(dbg_io)) &
                    !  print *, 'read_chunk: recv; rank, iv =', mpi_rank, iv, iz, i_z
                    call MPI_IRECV (piece, mx*my, MPI_REAL, io_rank, mpi_rank, &         ! send to io_rank
                         mpi_com, request, mpi_err)
                    call MPI_WAIT (request, status, mpi_err)
                    f(:,:,iz) = piece                                                    ! piece to recv
                 end if
                 deallocate (piece)
              end do
           end do
        end do
        call MPI_BARRIER (mpi_com, mpi_err)                                              ! avoid large send queue
     end do
     call MPI_BARRIER (mpi_com, mpi_err)
     if (mpi_rank == io_rank) then
        deallocate (buffer)
     end if
     !print*,'py_traceQ',omp_mythread,py(1,1,21)

     !-------------------------------------------------------------------------------
     ! MPI parallel i/o
     !-------------------------------------------------------------------------------

  else if (io_type==3) then
     !print*,'py_traceX',omp_mythread,py(1,1,21)
     call file_read_mpi (f, rec)
     !print*,'py_traceY',omp_mythread,py(1,1,21)
     !
     ! Each thread opens its own file
     !
  else if (io_type==1) then
     ! if (master) print*,'read_chunk: slices with',mx*my*4,' bytes'
     !print*,'py_traceV',omp_mythread,py(1,1,21),allocated(f)
     do iz=1,mz
        read (unit,rec=mz*(rec-1)+iz) f(:,:,iz)
        !print*,'py_trace ',omp_mythread,py(1,1,21),iz,rec
     end do
     !print*,'py_traceW',omp_mythread,py(1,1,21)
  else
     ! if (master) print*,'read_chunk: volume with',mx*my*mz*4,' bytes'
     read (unit,rec=rec) f
  end if

  ff = f
  deallocate (f)

end subroutine read_chunk


!-------------------------------------------------------------------------------
subroutine write_chunk (unit,rec,io_type,ff)

  use params
  use mpi_mod
  implicit none

  real, dimension(mx,my,mz) :: ff
  integer :: unit, rec, io_type
  integer :: status(MPI_STATUS_SIZE)
  integer :: ix, iy, iz, irec, mv, nx, ny, ix1, ix2, iy1, iy2
  integer :: i_x, i_y, i_z, mpi_fr, rank
  integer :: request,coord(3)
  logical :: debug, omp_in_parallel
  real(kind=4), allocatable, dimension(:,:) :: piece
  real(kind=4), allocatable, dimension(:,:,:) :: f

  if (omp_in_parallel() .and. omp_mythread>0) then
     print*,'ERROR: write_chunk called from parallel region'
     stop
  end if
  allocate (f(mx,my,mz))
  f = ff

  ! Single file, but handled by master (or last thread -- extra I/O node)
  if (io_type == 2) then
     if (mpi_rank == io_rank) then
        allocate (buffer(mxtot,mytot))                                                   ! I/O buffer
     end if
     !
     do i_z = 0,mpi_nz-1                                                                 ! loop over z-domains
        do iz = 1,mz_mpi(i_z)                                                            ! loop over z-slices
           do i_y = 0,mpi_ny-1                                                           ! y-domains
              do i_x = 0,mpi_nx-1                                                        ! x-domains
                 coord = (/ i_x, i_y, i_z /)                                             ! sender MPI-coordinates
                 call MPI_CART_RANK (new_com, coord, mpi_fr, mpi_err)                    ! convert to rank
                 nx = mx_mpi(i_x)
                 ny = my_mpi(i_y)
                 allocate (piece(nx,ny))
                 if (mpi_rank == io_rank) then                                           ! I/O node
                    if (mpi_rank == mpi_fr) then                                         ! if this one
                       if (debug(dbg_io)) &
                            print 1, 'write_chunk: copy; rank, rec =', mpi_rank, rec, iz, i_z, sum(piece**2)
                       piece = f(:,:,iz)                                                 ! just copy the piece
                    else                                                                 ! if not ..
                       call MPI_IRECV (piece, nx*ny, MPI_REAL, mpi_fr, mpi_fr, &         ! receive piece
                            mpi_com, request, mpi_err)
                       call MPI_WAIT (request, status, mpi_err)                          ! wait for completion
                       if (debug(dbg_io)) &
                            print 1, 'write_chunk: recv; rank, rec =', mpi_rank, rec, iz, i_z, sum(piece**2)
                    end if
                    ix1 = 1+ox_mpi(i_x); ix2=ix1-1+mx_mpi(i_x)
                    iy1 = 1+oy_mpi(i_y); iy2=iy1-1+my_mpi(i_y)
                    buffer(ix1:ix2,iy1:iy2) = piece                                      ! insert piece into buffer
                    if (debug(dbg_io)) &
                         print 1, 'write_chunk: buff; rank, rec =', ix1, ix2, iy1, iy2, sum(piece**2)
                 else if (mpi_rank == mpi_fr) then                                       ! if this one
                    piece = f(:,:,iz)                                                    ! piece to send
                    if (debug(dbg_io)) &
                         print 1, 'write_chunk: send; rank, rec =', mpi_rank, rec, iz, i_z, sum(piece**2)
                    call MPI_ISEND (piece, nx*ny, MPI_REAL, io_rank, mpi_rank, &         ! send to io_rank
                         mpi_com, request, mpi_err)
                    call MPI_WAIT (request, status, mpi_err)
                 end if
                 deallocate (piece)
              end do
           end do

           if (mpi_rank == io_rank) then
              irec = iz + oz_mpi(i_z) + (rec-1)*mztot                                    ! record number (izoff BUG corrected)
              if (debug(dbg_io)) &
                   print 1, 'write_chunk: wrte; rank, rec =', mpi_rank, rec, iz, i_z, sum(buffer**2)
              write  (unit,rec=irec) buffer                                              ! write out
           end if
           call MPI_BARRIER (mpi_com, mpi_err)                                           ! avoid large send queue
        end do
     end do
1    format(1x,a,4i8,1pg16.6)
     if (mpi_rank == io_rank) then
        deallocate (buffer)
     end if

  else if (io_type == 3) then
     ! MPI parallel i/o
     call file_write_mpi (f, rec)
     
  else if (io_type == 1) then
     ! Each thread opens its own file
     do iz=1,mz
        write (unit,rec=mz*(rec-1)+iz) f(:,:,iz)
     end do
  else
     write (unit,rec=rec) f
  end if

  deallocate (f)

end subroutine write_chunk

!-------------------------------------------------------------------------------
subroutine avg_2d (f, ny, sum2d)
  
  !  Return average of array in the local domain.  Make sure precision is
  !  retained, by summing first individual rows, then columns, then planes.
  !  --- NOTE: The return value should be thread-local variable!
  
  use params
  use mpi_mod
  implicit none

  integer :: i,ny,size
  real :: f(mx,mz,ny)
  real(kind=8) :: sum2d(ny)
  real(kind=8), allocatable, dimension(:,:) :: tmp8

  allocate (dbl1(ny))
  call sum_2d_omp (f, ny, dbl1)
  size = mpi_nx*mpi_nz
  if (size > 1) then
     !$omp master
     if (do_validate) then
        call MPI_COMM_SIZE (mpi_comm_plane(2), size, mpi_err)
        allocate (tmp8(ny,size))
        call MPI_ALLGATHER (dbl1,ny,MPI_REAL8,tmp8,ny,MPI_REAL8,mpi_comm_plane(2),mpi_err) 
        do i=1,ny
           sum2d(i) = sum(tmp8(i,1:size))/dble(size)
        end do
        deallocate (tmp8)
     else
        call MPI_ALLREDUCE(dbl1,sum2d,ny,MPI_REAL8,MPI_SUM,mpi_comm_plane(2),mpi_err)
        sum2d = sum2d/dble(size)
     endif
     !$omp end master
     call barrier_omp('avg_2d')
  else
     sum2d = dbl1
  end if
  deallocate (dbl1)

end subroutine avg_2d

!-------------------------------------------------------------------------------
subroutine sum_2d (f, sum2d)
  !  Return average of array in the local domain.  Make sure precision is
  !  retained, by summing first individual rows, then columns, then planes.
  !  --- NOTE: The return value should be thread-local variable!

  use params
  use mpi_mod
  implicit none

  integer :: i, size
  real :: f(mx,mz), ff(mx,mz,1)
  real :: sum2d
  real(kind=8) :: sum2d8
  real(kind=8), allocatable, dimension(:,:) :: tmp8

  allocate (dbl1(1))
  ff(:,:,1)=f
  call sum_2d_omp (f, 1, dbl1)
  size = mpi_nx*mpi_nz
  if (size > 1) then
     !$omp master
     if (do_validate) then
        call MPI_COMM_SIZE (mpi_comm_plane(2), size, mpi_err)
        allocate (tmp8(1,size))
        call MPI_ALLGATHER (dbl1,1,MPI_REAL8,tmp8,1,MPI_REAL8,mpi_comm_plane(2),mpi_err) 
        sum2d8 = sum(tmp8(1,1:size))
        deallocate (tmp8)
     else
        call MPI_ALLREDUCE(dbl1,sum2d8,1,MPI_REAL8,MPI_SUM,mpi_comm_plane(2),mpi_err)
     endif
     !$omp end master
  else
     sum2d8 = dbl1(1)
  end if
  sum2d=real(sum2d8)
  deallocate (dbl1)

end subroutine sum_2d

!-------------------------------------------------------------------------------
logical function any_mpi (l)
  use mpi_mod
  implicit none
  logical :: l
  integer :: one, sum
  !
  !$omp master
  one = merge(1,0,l)
  call MPI_Allreduce (one, sum, 1, my_integer, MPI_SUM, mpi_com, mpi_err) 
  any_mpi = (sum > 0)
  !$omp end master
end function any_mpi

!-------------------------------------------------------------------------------
logical function any_mpi_nowait (l)
  use mpi_mod
  implicit none
  logical :: l
  integer :: one, sum
  !
  !$omp master
  one = merge(1,0,l)
  call MPI_Allreduce (one, sum, 1, my_integer, MPI_SUM, mpi_com, mpi_err) 
  any_mpi_nowait = (sum > 0)
  !$omp end master
end function any_mpi_nowait

!-------------------------------------------------------------------------------
subroutine broadcast_mpi (a, n)
  use mpi_mod
  implicit none
  integer :: n
  real, dimension(n) :: a
  !
  !$omp barrier
  !$omp master
  call MPI_BCAST (a, n, my_real, 0, mpi_com, mpi_err) 
  !$omp end master
  !$omp barrier
end subroutine broadcast_mpi

!-------------------------------------------------------------------------------
subroutine broadcast4_mpi (a, n)
  use mpi_mod
  implicit none
  integer :: n
  real, dimension(n) :: a
  !
  !$omp barrier
  !$omp master
  call MPI_BCAST (a, n, MPI_REAL, 0, mpi_com, mpi_err) 
  !$omp end master
  !$omp barrier
end subroutine broadcast4_mpi

!-------------------------------------------------------------------------------
subroutine broadcast_chars_mpi (a, n)
  use mpi_mod
  implicit none
  integer :: n
  character(len=n) :: a
  !
  !$omp master
  call MPI_BCAST (a, n, MPI_CHAR, 0, mpi_com, mpi_err) 
  !$omp end master
end subroutine broadcast_chars_mpi

!-------------------------------------------------------------------------------
subroutine bcast_int_mpi (a, n)
  use mpi_mod
  implicit none
  integer :: n
  integer, dimension(n) :: a
  !
  !$omp barrier
  !$omp master
  call MPI_BCAST (a, n, MPI_INTEGER, 0, mpi_com, mpi_err) 
  !$omp end master
  !$omp barrier
end subroutine bcast_int_mpi

!-------------------------------------------------------------------------------
subroutine isend_int_mpi (a, n, rank, req)
  use mpi_mod
  implicit none
  integer :: n, rank, req
  integer, dimension(n) :: a
  !
  !$omp barrier
  !$omp master
  call MPI_ISEND (a, n, MPI_INTEGER, rank, 88888, mpi_com, req, mpi_err) 
  !$omp end master
  !$omp barrier
end subroutine isend_int_mpi

!-------------------------------------------------------------------------------
subroutine irecv_int_mpi (a, n, rank, req)
  use mpi_mod
  implicit none
  integer :: n, rank, req
  integer, dimension(n) :: a
  !
  !$omp barrier
  !$omp master
  call MPI_IRECV (a, n, MPI_INTEGER, rank, 88888, mpi_com, req, mpi_err) 
  !$omp end master
  !$omp barrier
end subroutine irecv_int_mpi

!-------------------------------------------------------------------------------
subroutine send_real_mpi (a, n, from, to)
  
  !  Send the boundary layers from one domain over to the neighboring domains
  !  and receive the corresponding values into temporary arrays.

  use mpi_mod
  implicit none

  integer :: n, from, to
  real, dimension(n) :: a
  integer :: status(MPI_STATUS_SIZE)
  integer :: request1, request2

  ! Send to and receive from

  !$omp master
  call MPI_ISEND (a, n, my_real,   to, 1000*to+from, mpi_com, request1, mpi_err)
  call MPI_IRECV (a, n, my_real, from, 1000*to+from, mpi_com, request2, mpi_err)
  call MPI_WAIT (request1, status, mpi_err)
  call MPI_WAIT (request2, status, mpi_err)
  !$omp end master

end subroutine send_real_mpi

!-------------------------------------------------------------------------------
function fexists(file)
  use mpi_mod
  use params, only: master, mpi_rank, mpi_size
  implicit none
  character(len=*) :: file
  logical :: fexists
  fexists = .false.
  if (mpi_rank == 0) inquire (file=file,exist=fexists)
  if (mpi_size > 0) call MPI_BCAST (fexists,1,MPI_LOGICAL,0,mpi_com,mpi_err)
end function fexists

!-------------------------------------------------------------------------------
function flag(file)
  use mpi_mod
  use params, only: flag_unit, master
  implicit none
  character(len=*) :: file
  logical :: flag
  integer :: iostat

  if (master) then
     inquire (file=file//'.flag',exist=flag)
     if (flag) then
        open (flag_unit,file=file//'.flag',status='old')
        close (flag_unit,status='delete',iostat=iostat)
     end if
  end if
  call MPI_BCAST (flag,1,MPI_LOGICAL,0,mpi_com,mpi_err)
end function flag

!-------------------------------------------------------------------------------
subroutine ghost_fill (mx,my,mz,ny,ma,f)

  use mpi_mod
  use params, only: mpi_rank
  implicit none
  integer :: mx,my,mz,ny,ma,m
  real :: f(1-ma:mx+ma,0:my+1,1-ma:mz+ma)
  real, dimension(ma,0:ny+1,1-ma:mz+ma) :: fx0,fx1
  real, dimension(1-ma:mx+ma,0:ny+1,ma) :: fz0,fz1
  integer :: status(MPI_STATUS_SIZE)
  integer :: request1, request2, request3, request4

  if (ma == 0) return
  call barrier_omp('ghost_fill')
  !$omp master
  fx0 = f(1:ma,0:ny+1,:)
  m = ma*(ny+2)*(mz+2*ma)
  call MPI_IRECV (fx1, m, my_real, mpi_xup, 2*mpi_xup     , mpi_com, request3, mpi_err)
  call MPI_ISEND (fx0, m, my_real, mpi_xdn, 2*mpi_rank    , mpi_com, request1, mpi_err)
  call MPI_WAIT (request1, status, mpi_err)
  call MPI_WAIT (request3, status, mpi_err)
  f(mx+1:mx+ma,0:ny+1,:) = fx1
  fx0 = f(mx-ma+1:mx,0:ny+1,:)
  call MPI_IRECV (fx1, m, my_real, mpi_xdn, 2*mpi_xdn  + 1, mpi_com, request4, mpi_err)
  call MPI_ISEND (fx0, m, my_real, mpi_xup, 2*mpi_rank + 1, mpi_com, request2, mpi_err)
  call MPI_WAIT (request2, status, mpi_err)
  call MPI_WAIT (request4, status, mpi_err)
  f(1-ma:0    ,0:ny+1,:) = fx1
  m = ma*(ny+2)*(mx+2*ma)
  fz0 = f(:,0:ny+1,      1:ma)
  call MPI_IRECV (fz1, m, my_real, mpi_zup, 2*mpi_zup     , mpi_com, request3, mpi_err)
  call MPI_ISEND (fz0, m, my_real, mpi_zdn, 2*mpi_rank    , mpi_com, request1, mpi_err)
  call MPI_WAIT (request1, status, mpi_err)
  call MPI_WAIT (request3, status, mpi_err)
  f(:,0:ny+1,mz+1:mz+ma) = fz1
  fz0 = f(:,0:ny+1,mz-ma+1:mz)
  call MPI_IRECV (fz1, m, my_real, mpi_zdn, 2*mpi_zdn  + 1, mpi_com, request4, mpi_err)
  call MPI_ISEND (fz0, m, my_real, mpi_zup, 2*mpi_rank + 1, mpi_com, request2, mpi_err)
  call MPI_WAIT (request2, status, mpi_err)
  call MPI_WAIT (request4, status, mpi_err)
  f(:,0:ny+1,1-ma:0    ) = fz1
  !$omp end master
  call barrier_omp('ghost_fill')

end subroutine ghost_fill

!-------------------------------------------------------------------------------
subroutine ghost_fill_x (mx,my,mz,f1,mg,mblocks,ny,izs,ize,f2)

  use mpi_mod
  use params, only: mpi_rank, mpi_nx
  implicit none

  integer :: mx,my,mz,ny,mg,ng,izs,ize,m
  real :: f1(mx,my,mz),f2(1-mg:mx+mg,ny,mz)
  integer :: status(MPI_STATUS_SIZE)
  integer :: request1, request2, request3, request4
  integer :: mpi_ixdn, mpi_ixup
  integer :: mblocks ! (DEPRECATED) max number of subdomains sent on either side to fill ghost zones
  integer :: nblocks, i
 
  if (mg > mblocks*mx) then
     print *,'ghost_fill_x: mg larger than mblocks*mx',mg,mblocks*mx
     stop
  end if
  if (mpi_nx == 1) then
     call ghost_fill_x_periodic (mx,my,mz,f1,mg,mblocks,ny,izs,ize,f2)
     return
  end if

  f2(1   :mx   ,1:ny,izs:ize) = f1(1      :mx,1:ny,izs:ize)

  call barrier_omp('ghost_fill_x')
  !$omp barrier
  !$omp master

  !number of neighbouring blocks to be communicated (per side)
  nblocks = (mg-1)/mx+1

  do i=1,nblocks
     ng   = min(mx,mg-(i-1)*mx)
     m    = ng*ny*mz
     allocate (send(1:ng,1:ny,1:mz), recv(1:ng,1:ny,1:mz))
     send = f1(1:ng,1:ny,1:mz)

     !   f2(mx+1:mx+ng,1:ny,1:mz) = f1(1      :ng,1:ny,1:mz)
     call MPI_CART_SHIFT(new_com, 0, i, mpi_ixdn, mpi_ixup, mpi_err)
     print*,'rank, send/recv, to, from:', mpi_rank, m, mpi_ixdn, mpi_ixup

     call MPI_IRECV (recv, m, my_real, mpi_ixup, 2*mpi_ixup    , mpi_com, request3, mpi_err)
     call MPI_ISEND (send, m, my_real, mpi_ixdn, 2*mpi_rank    , mpi_com, request1, mpi_err)
     call MPI_WAIT (request1, status, mpi_err)
     call MPI_WAIT (request3, status, mpi_err)

     !   f2(1-ng:0    ,1:ny,1:mz) = f1(mx-ng+1:mx,1:ny,izs:ize)
     f2(i*mx+1:i*mx+ng,1:ny,1:mz) = recv
     send = f1(mx-ng+1:mx,1:ny,1:mz)

     m = ng*ny*mz

     print*,'rank, send/recv, to, from:', mpi_rank, m, mpi_ixup, mpi_ixdn
     call MPI_IRECV (recv, m, my_real, mpi_ixdn, 2*mpi_ixdn + 1, mpi_com, request4, mpi_err)
     call MPI_ISEND (send, m, my_real, mpi_ixup, 2*mpi_rank + 1, mpi_com, request2, mpi_err)
     call MPI_WAIT (request2, status, mpi_err)
     call MPI_WAIT (request4, status, mpi_err)

     f2(1-ng+(1-i)*mx:(1-i)*mx,1:ny,1:mz) = recv

     deallocate (send, recv)
  end do

  !$omp end master
  !$omp barrier
  call barrier_omp('ghost_fill_x')

end subroutine ghost_fill_x

!-------------------------------------------------------------------------------
subroutine ghost_fill_z (mx,my,mz,f1,mg,mblocks,ny,izs,ize,f2)

  use mpi_mod
  use params, only: mpi_rank, mpi_nz
  implicit none

  integer :: mx,my,mz,ny,mg,ng,izs,ize,m
  real :: f1(mx,my,mz),f2(mx,ny,1-mg:mz+mg)
  integer :: status(MPI_STATUS_SIZE)
  integer :: request1, request2, request3, request4
  integer :: mpi_izdn, mpi_izup
  integer :: mblocks ! (DEPRECATED) max number of subdomains sent on either side to fill ghost zones
  integer :: nblocks, i

  if (mpi_nz == 1) then
     call ghost_fill_z_periodic (mx,my,mz,f1,mg,mblocks,ny,izs,ize,f2)
     return
  end if

  if (mg > mblocks*mz) then
     print *,'ghost_fill_z: mg larger than mblocks*mz',mg,mblocks*mz
     stop
  end if

  f2(1   :mx   ,1:ny,izs:ize) = f1(1      :mx,1:ny,izs:ize)

  call barrier_omp('ghost_fill_z')
  !$omp master

  nblocks = (mg-1)/mz+1     !number of neighbouring blocks to be communicated (per side)

  do i=1,nblocks

     ng = min(mz,mg-(i-1)*mz)
     m  = ng*ny*mx
     allocate (send(1:mx,1:ny,1:ng), recv(1:mx,1:ny,1:ng))
     send = f1(1:mx,1:ny,1:ng)

     ! f2(1:mx,1:ny,1-ng:mz-ng) = f1(1:mx,1:ny,1:mz)

     call MPI_CART_SHIFT(new_com, 2, i, mpi_izdn, mpi_izup, mpi_err)

     call MPI_IRECV (recv, m, my_real, mpi_izup, 2*mpi_izup    , mpi_com, request3, mpi_err)
     call MPI_ISEND (send, m, my_real, mpi_izdn, 2*mpi_rank    , mpi_com, request1, mpi_err)
     call MPI_WAIT (request1, status, mpi_err)
     call MPI_WAIT (request3, status, mpi_err)

     !   f2(1:mx,1:ny,1+ng:mz+ng) = f1(1:mx,1:ny,1:mz)

     f2(1:mx,1:ny,i*mz+1:i*mz+ng) = recv
     send = f1(1:mx,1:ny,mz-ng+1:mz)

     m = ng*ny*mx

     call MPI_IRECV (recv, m, my_real, mpi_izdn, 2*mpi_izdn + 1, mpi_com, request4, mpi_err)
     call MPI_ISEND (send, m, my_real, mpi_izup, 2*mpi_rank + 1, mpi_com, request2, mpi_err)
     call MPI_WAIT (request2, status, mpi_err)
     call MPI_WAIT (request4, status, mpi_err)

     f2(1:mx,1:ny,1-ng+(1-i)*mz:(1-i)*mz) = recv

     deallocate (send, recv)

  end do

  !$omp end master
  call barrier_omp('ghost_fill_z')

end subroutine ghost_fill_z

!-------------------------------------------------------------------------------
subroutine ghost_fill_x_periodic (mx,my,mz,f1,mg,mblocks,ny,izs,ize,f2)

  use params, only: mpi_rank
  implicit none
  integer :: mx,my,mz,ny,mg,ng,izs,ize
  real :: f1(mx,my,mz),f2(1-mg:mx+mg,ny,mz)
  integer :: mblocks ! (DEPRECATED) max number of subdomains sent on either side to fill ghost zones
  integer :: nblocks, i

  if (mg > mblocks*mx) then
     print *,'ghost_fill_x: mg larger than mblocks*mx',mg,mblocks,mx
     stop
  end if

  f2(1   :mx   ,1:ny,izs:ize) = f1(1      :mx,1:ny,izs:ize)

  !actual number of neighbouring blocks (per side)
  nblocks = (mg-1)/mx+1
  do i=1,nblocks
     ng = min(mx,mg-(i-1)*mx)

     !replaces f2(1-ng:0    ,1:ny,izs:ize) = f1(mx-ng+1:mx,1:ny,izs:ize)
     f2(1-ng+(1-i)*mx:(1-i)*mx,1:ny,izs:ize) = f1(mx-ng+1:mx,1:ny,izs:ize)

     !replaces f2(mx+1:mx+ng,1:ny,izs:ize) = f1(1      :ng,1:ny,izs:ize)
     f2(i*mx+1:i*mx+ng,        1:ny,izs:ize) = f1(1      :ng,1:ny,izs:ize)
  end do

end subroutine ghost_fill_x_periodic


!-------------------------------------------------------------------------------
subroutine ghost_fill_z_periodic (mx,my,mz,f1,mg,mblocks,ny,izs,ize,f2)

  use params, only: mpi_rank
  implicit none
  integer :: mx,my,mz,ny,mg,ng,izs,ize,iz
  real :: f1(mx,my,mz),f2(mx,ny,1-mg:mz+mg)
  integer :: mblocks ! (DEPRECATED) max number of subdomains sent on either side to fill ghost zones
  integer :: nblocks, i

  if (mg > mblocks*mz) then
     print *,'ghost_fill_z: mg larger than mblocks*mz',mg,mblocks*mz
     stop
  end if
  f2(1:mx,1:ny,izs:ize)       = f1(1:mx,1:ny,izs:ize)

  nblocks = (mg-1)/mz+1                                                         !actual number of neighbouring blocks (per side)
  do i=1,nblocks
     ng = min(mz,mg-(i-1)*mz)

     do iz=mz+1,mz+ng
        if ((iz-mz) >= izs .and. (iz-mz) <= ize) then                             ! each OMP thread handles its range
           f2(1:mx,1:ny,iz +(i-1)*mz) = f1(1:mx,1:ny,iz-mz)                        ! copy from iz-mz to iz
        end if
     end do
     do iz=1-ng,0
        if ((iz+mz) >= izs .and. (iz+mz) <= ize) then                             ! each OMP thread handles its range
           f2(1:mx,1:ny,iz +(1-i)*mz) = f1(1:mx,1:ny,iz+mz)                        ! copy from iz+mz to iz
        end if
     end do
  end do

end subroutine ghost_fill_z_periodic

!-------------------------------------------------------------------------------
subroutine buffer_reset (nsent)
  use mpi_mod, only: buffer_words, buffer_calls
  implicit none
  integer(kind=8) :: nsent

  !$omp single
  buffer_words = 0_8
  buffer_calls = 0_8
  nsent = 0_8
  !$omp end single
end subroutine buffer_reset

!-------------------------------------------------------------------------------
subroutine buffer_count
  use params, only: mxtot, mytot, mztot, master
  use mpi_mod, only: buffer_words, buffer_calls
  implicit none
  integer(kind=8) :: nwords, ncalls
  logical, save :: first_time=.true.

  if (.not. first_time) return
  first_time = .false.
  !$omp single
  nwords = buffer_words
  !$omp end single
  call sum_int8_mpi (nwords)
  !$omp single
  ncalls = buffer_calls
  !$omp end single
  call sum_int8_mpi (ncalls)
  if (master) print'(1x,a,1p,e12.2)', &
       'buffer_blocks:       words =', real(nwords), &
       'buffer_blocks:       calls =', real(ncalls), &
       'buffer_blocks:  words/call =', real(nwords)/max(1,ncalls), &
       'buffer_blocks: chunks/call =', nwords/(real(mxtot)*real(mytot)*real(mztot))/max(1,ncalls), &
       'buffer_blocks: chunks/step =', nwords/(real(mxtot)*real(mytot)*real(mztot))
end subroutine buffer_count

!-------------------------------------------------------------------------------
subroutine buffer_blocks_z(nx, ny, nz, nymax, nyBlocks, f1, f2, iyIniArr, iyEndArr, kBlockArr)

  use mpi_mod, only: send, recv, my_real, mpi_comm_beam, MPI_STATUS_SIZE, &
       mpi_err, buffer_words, buffer_calls
  use params, only: mpi_rank, mpi_nz, mpi_z, dbg_mpi

  implicit none

  integer,                intent(in)  :: nx, ny, nz, nymax, nyBlocks
  integer, dimension(ny), intent(in)  :: iyIniArr, iyEndArr, kBlockArr
  real,    dimension( nx, ny, nz ),         intent(in)   :: f1
  real,    dimension( nx, ny, -1:nz+nz+2 ), intent(out)  :: f2
  real,    dimension( :, :, : ), allocatable :: send1, recv1

  integer  :: request1, request2, request3, request4
  integer  :: request5, request6, request7, request8
  integer  :: tag_send, tag_recv
  integer  :: status(MPI_STATUS_SIZE)

  integer  :: mpi_izdnL,  mpi_izupL,  mpi_izdnR,  mpi_izupR
  integer  :: mpi_izdnL1, mpi_izupL1, mpi_izdnR1, mpi_izupR1
  integer  :: m, m1, nyb, j, iyIni, iyEnd
  integer  ::  kBlockL,  kBlockR,  kBlockL1,  kBlockR1
  integer  :: mkBlockL, mkBlockR, mkBlockL1, mkBlockR1
  logical  :: is_not_zero_L, is_not_zero_R, is_not_zero_L1, is_not_zero_R1  
  logical  :: debug2
  integer(8):: sent, sent1, calls


  ! Block topology:
  ! 
  ! - -------------------------------------  
  !    |    |           |           |    |
  !    | L1 |     L     |     R     | R1 |       
  !    |    |           |           |    |
  ! - -------------------------------------  
  !
  ! L, R  = left and right main blocks, nz grid-points wide
  ! L1,R1 = left and right thin layers, two grid-points thick


  ! case: no split in z direction

  if (mpi_nz == 1) then
     !$omp single
     f2( 1:nx, 1:nymax,       -1:0      )  =  f1( 1:nx, 1:nymax, nz-1:nz )
     f2( 1:nx, 1:nymax,       1:nz      )  =  f1( 1:nx, 1:nymax,    1:nz )
     f2( 1:nx, 1:nymax,    nz+1:nz+nz   )  =  f1( 1:nx, 1:nymax,    1:nz )
     f2( 1:nx, 1:nymax, nz+nz+1:nz+nz+2 )  =  f1( 1:nx, 1:nymax,    1:2  )
     !$omp end single
     return
  end if

  sent1 = buffer_words
  calls = buffer_calls

  ! case: mpi-split in z direction

  !$omp single
  do j=1,nyBlocks

     iyIni     = iyIniArr(j) 
     iyEnd     = iyEndArr(j)
     nyb       = iyEnd - iyIni + 1

     kBlockL   = kBlockArr(j)
     kBlockR   = kBlockArr(j) + 1
     kBlockR1  = kBlockArr(j) + 2
     kBlockL1  = kBlockArr(j) - 1

     mkBlockL  = modulo( kBlockL,  mpi_nz )
     mkBlockR  = modulo( kBlockR,  mpi_nz )
     mkBlockR1 = modulo( kBlockR1, mpi_nz )
     mkBlockL1 = modulo( kBlockL1, mpi_nz )

     is_not_zero_L  = mkBlockL  .ne. 0
     is_not_zero_R  = mkBlockR  .ne. 0
     is_not_zero_R1 = mkBlockR1 .ne. 0
     is_not_zero_L1 = mkBlockL1 .ne. 0

     m         = nx * nyb * nz
     m1        = nx * nyb * 2

     allocate ( send  (1:nx, 1:nyb, 1:nz), recv  (1:nx, 1:nyb, 1:nz) )
     allocate ( send1 (1:nx, 1:nyb, 1:2 ), recv1 (1:nx, 1:nyb, 1:2 ) )

     ! communicate L and R blocks

     send  = f1( 1:nx, iyIni:iyEnd, 1:nz )

     sent = buffer_words

     if (is_not_zero_L) then 
        mpi_izdnL = modulo( mpi_z - kBlockL, mpi_nz)
        mpi_izupL = modulo( mpi_z + kBlockL, mpi_nz)
        tag_send  = 4 * mpi_z
        tag_recv  = 4 * mpi_izupL
        call MPI_IRECV (recv, m, my_real, mpi_izupL, tag_recv, mpi_comm_beam(3), request5, mpi_err)
        call MPI_ISEND (send, m, my_real, mpi_izdnL, tag_send, mpi_comm_beam(3), request1, mpi_err)
        buffer_words = buffer_words + m
        buffer_calls = buffer_calls + 1
        call MPI_WAIT (request1, status, mpi_err)
        call MPI_WAIT (request5, status, mpi_err)
        f2( 1:nx, iyIni:iYEnd, 1:nz ) = recv
     else
        f2( 1:nx, iyIni:iYEnd, 1:nz ) = f1( 1:nx, iyIni:iYEnd, 1:nz )
     endif


     if (is_not_zero_R) then 
        mpi_izdnR = modulo( mpi_z - kBlockR, mpi_nz)
        mpi_izupR = modulo( mpi_z + kBlockR, mpi_nz)
        tag_send  = 4 * mpi_z + 1
        tag_recv  = 4 * mpi_izupR + 1
        call MPI_IRECV (recv, m, my_real, mpi_izupR, tag_recv, mpi_comm_beam(3), request6, mpi_err)
        call MPI_ISEND (send, m, my_real, mpi_izdnR, tag_send, mpi_comm_beam(3), request2, mpi_err)
        buffer_words = buffer_words + m
        buffer_calls = buffer_calls + 1
        call MPI_WAIT (request2, status, mpi_err)
        call MPI_WAIT (request6, status, mpi_err)
        f2( 1:nx, iyIni:iYEnd, nz+1:nz+nz ) = recv
     else
        f2( 1:nx, iyIni:iYEnd, nz+1:nz+nz ) = f1( 1:nx, iyIni:iYEnd, 1:nz )
     endif


     ! communicate L1 and R1 layers

     send1  = f1( 1:nx, iyIni:iyEnd, nz-1:nz )

     if (is_not_zero_L1) then 
        mpi_izdnL1 = modulo( mpi_z - kBlockL1, mpi_nz)
        mpi_izupL1 = modulo( mpi_z + kBlockL1, mpi_nz)
        tag_send  = 4 * mpi_z + 2
        tag_recv  = 4 * mpi_izupL1 + 2
        call MPI_IRECV (recv1, m1, my_real, mpi_izupL1, tag_recv, mpi_comm_beam(3), request7, mpi_err)
        call MPI_ISEND (send1, m1, my_real, mpi_izdnL1, tag_send, mpi_comm_beam(3), request3, mpi_err)
        buffer_words = buffer_words + m1
        buffer_calls = buffer_calls + 1
        call MPI_WAIT (request3, status, mpi_err)
        call MPI_WAIT (request7, status, mpi_err)
        f2( 1:nx, iyIni:iYEnd, -1:0 ) = recv1
     else
        f2( 1:nx, iyIni:iYEnd, -1:0 ) = send1
     endif


     send1  = f1( 1:nx, iyIni:iyEnd, 1:2 )

     if (is_not_zero_R1) then 
        mpi_izdnR1 = modulo( mpi_z - kBlockR1, mpi_nz)
        mpi_izupR1 = modulo( mpi_z + kBlockR1, mpi_nz)
        tag_send  = 4 * mpi_z + 3
        tag_recv  = 4 * mpi_izupR1 + 3
        call MPI_IRECV (recv1, m1, my_real, mpi_izupR1, tag_recv, mpi_comm_beam(3), request8, mpi_err)
        call MPI_ISEND (send1, m1, my_real, mpi_izdnR1, tag_send, mpi_comm_beam(3), request4, mpi_err)
        buffer_words = buffer_words + m1
        buffer_calls = buffer_calls + 1
        call MPI_WAIT (request4, status, mpi_err)
        call MPI_WAIT (request8, status, mpi_err)
        f2( 1:nx, iyIni:iYEnd, nz+nz+1:nz+nz+2 ) = recv1
     else
        f2( 1:nx, iyIni:iYEnd, nz+nz+1:nz+nz+2 ) = send1
     endif

     sent = buffer_words-sent+1
     !print'(a,2i5,2i8,i10,2x,4l2)','buffer_bl_z: rank, iyIni, sent, sent/nyb, words =', &
     !mpi_rank, iyIni, sent, sent/nyb, buffer_words, &
     !is_not_zero_L, is_not_zero_R, is_not_zero_L1, is_not_zero_R1

     deallocate ( send,  recv )
     deallocate ( send1, recv1 )

  end do

  if (debug2(dbg_mpi,3)) then
     sent1 = buffer_words-sent1+1
     calls = buffer_calls-calls
     print'(a,i5,i8,f8.2,i4,2x,4l2)','buffer_bl_z: rank, sent, sent/nxyz, calls =', &
          mpi_rank, sent1, sent1/real(nx*ny*nz), calls, &
          is_not_zero_L, is_not_zero_R, is_not_zero_L1, is_not_zero_R1
  end if

  !$omp end single

end subroutine buffer_blocks_z


!-------------------------------------------------------------------------------
subroutine buffer_blocks_x(nx, ny, nz, nymax, nyBlocks, f1, f2, iyIniArr, iyEndArr, kBlockArr)

  use mpi_mod, only: send, recv, my_real, mpi_comm_beam, MPI_STATUS_SIZE, &
       mpi_err, buffer_words, buffer_calls
  use params, only: mpi_rank, mpi_nx, mpi_x, dbg_mpi

  implicit none

  integer,                                  intent(in)  :: nx, ny, nz, nymax, nyBlocks
  integer, dimension(ny),                   intent(in)  :: iyIniArr, iyEndArr, kBlockArr
  real,    dimension(         nx, ny, nz ), intent(in)  :: f1
  real,    dimension( -1:nx+nx+2, ny, nz ), intent(out) :: f2

  real, dimension( :, :, : ), allocatable :: send1, recv1
  integer  :: request1, request2, request3, request4
  integer  :: request5, request6, request7, request8
  integer  :: tag_send, tag_recv
  integer  :: status(MPI_STATUS_SIZE)

  integer  :: mpi_ixdnL,  mpi_ixupL,  mpi_ixdnR,  mpi_ixupR
  integer  :: mpi_ixdnL1, mpi_ixupL1, mpi_ixdnR1, mpi_ixupR1
  integer  :: m, m1, nyb, j, iyIni, iyEnd
  integer  ::  kBlockL,  kBlockR,  kBlockL1,  kBlockR1
  integer  :: mkBlockL, mkBlockR, mkBlockL1, mkBlockR1
  logical  :: is_not_zero_L, is_not_zero_R, is_not_zero_L1, is_not_zero_R1  
  logical  :: debug2
  integer(8):: sent, sent1, calls

  ! Block topology:
  ! 
  ! - -------------------------------------  
  !    |    |           |           |    |
  !    | L1 |     L     |     R     | R1 |       
  !    |    |           |           |    |
  ! - -------------------------------------  
  !
  ! L, R  = left and right main blocks, nx grid-points wide
  ! L1,R1 = left and right thin layers, two grid-points thick


  ! case: no split in x direction

  if (mpi_nx == 1) then
     !$omp single
     f2(      -1:0      , 1:nymax, 1:nz )  =  f1( nx-1:nx, 1:nymax, 1:nz )
     f2(       1:nx     , 1:nymax, 1:nz )  =  f1(    1:nx, 1:nymax, 1:nz )
     f2(    nx+1:nx+nx  , 1:nymax, 1:nz )  =  f1(    1:nx, 1:nymax, 1:nz )
     f2( nx+nx+1:nx+nx+2, 1:nymax, 1:nz )  =  f1(    1:2 , 1:nymax, 1:nz )
     !$omp end single
     return
  end if

  ! case: mpi-split in x direction

  sent1 = buffer_words
  calls = buffer_calls

  !$omp single
  do j=1,nyBlocks

     iyIni     = iyIniArr(j) 
     iyEnd     = iyEndArr(j)
     nyb       = iyEnd - iyIni + 1

     kBlockL   = kBlockArr(j)
     kBlockR   = kBlockArr(j) + 1
     kBlockR1  = kBlockArr(j) + 2
     kBlockL1  = kBlockArr(j) - 1

     mkBlockL  = modulo( kBlockL,  mpi_nx )
     mkBlockR  = modulo( kBlockR,  mpi_nx )
     mkBlockR1 = modulo( kBlockR1, mpi_nx )
     mkBlockL1 = modulo( kBlockL1, mpi_nx )

     is_not_zero_L  = mkBlockL  .ne. 0
     is_not_zero_R  = mkBlockR  .ne. 0
     is_not_zero_R1 = mkBlockR1 .ne. 0
     is_not_zero_L1 = mkBlockL1 .ne. 0

     m         = nx * nyb * nz
     m1        =  2 * nyb * nz

     allocate ( send  (1:nx, 1:nyb, 1:nz), recv  (1:nx, 1:nyb, 1:nz) )
     allocate ( send1 (1:2 , 1:nyb, 1:nz), recv1 (1:2 , 1:nyb, 1:nz) )

     ! communicate L and R blocks
     send  = f1( 1:nx, iyIni:iyEnd, 1:nz )
     sent = buffer_words
     if (is_not_zero_L) then 
        mpi_ixdnL = modulo( mpi_x - kBlockL, mpi_nx)
        mpi_ixupL = modulo( mpi_x + kBlockL, mpi_nx)
        tag_send  = 4 * mpi_x
        tag_recv  = 4 * mpi_ixupL
        call MPI_IRECV (recv, m, my_real, mpi_ixupL, tag_recv, mpi_comm_beam(1), request5, mpi_err)
        call MPI_ISEND (send, m, my_real, mpi_ixdnL, tag_send, mpi_comm_beam(1), request1, mpi_err)
        buffer_words = buffer_words + m
        buffer_calls = buffer_calls + 1
        call MPI_WAIT (request1, status, mpi_err)
        call MPI_WAIT (request5, status, mpi_err)
        f2( 1:nx, iyIni:iYEnd, 1:nz ) = recv
     else
        f2( 1:nx, iyIni:iYEnd, 1:nz ) = f1( 1:nx, iyIni:iYEnd, 1:nz )
     endif
     
     if (is_not_zero_R) then 
        mpi_ixdnR = modulo( mpi_x - kBlockR, mpi_nx)
        mpi_ixupR = modulo( mpi_x + kBlockR, mpi_nx)
        tag_send  = 4 * mpi_x + 1
        tag_recv  = 4 * mpi_ixupR + 1
        call MPI_IRECV (recv, m, my_real, mpi_ixupR, tag_recv, mpi_comm_beam(1), request6, mpi_err)
        call MPI_ISEND (send, m, my_real, mpi_ixdnR, tag_send, mpi_comm_beam(1), request2, mpi_err)
        buffer_words = buffer_words + m
        buffer_calls = buffer_calls + 1
        call MPI_WAIT (request2, status, mpi_err)
        call MPI_WAIT (request6, status, mpi_err)
        f2( nx+1:nx+nx, iyIni:iYEnd, 1:nz ) = recv
     else
        f2( nx+1:nx+nx, iyIni:iYEnd, 1:nz ) = f1( 1:nx, iyIni:iYEnd, 1:nz )
     endif

     ! communicate L1 and R1 layers
     send1  = f1( nx-1:nx, iyIni:iyEnd, 1:nz )
     if (is_not_zero_L1) then 
        mpi_ixdnL1 = modulo( mpi_x - kBlockL1, mpi_nx)
        mpi_ixupL1 = modulo( mpi_x + kBlockL1, mpi_nx)
        tag_send  = 4 * mpi_x + 2
        tag_recv  = 4 * mpi_ixupL1 + 2
        call MPI_IRECV (recv1, m1, my_real, mpi_ixupL1, tag_recv, mpi_comm_beam(1), request7, mpi_err)
        call MPI_ISEND (send1, m1, my_real, mpi_ixdnL1, tag_send, mpi_comm_beam(1), request3, mpi_err)
        buffer_words = buffer_words + m1
        buffer_calls = buffer_calls + 1
        call MPI_WAIT (request3, status, mpi_err)
        call MPI_WAIT (request7, status, mpi_err)
        f2( -1:0, iyIni:iYEnd, 1:nz ) = recv1
     else
        f2( -1:0, iyIni:iYEnd, 1:nz ) = send1
     endif

     send1  = f1( 1:2, iyIni:iyEnd, 1:nz )
     if (is_not_zero_R1) then 
        mpi_ixdnR1 = modulo( mpi_x - kBlockR1, mpi_nx)
        mpi_ixupR1 = modulo( mpi_x + kBlockR1, mpi_nx)
        tag_send  = 4 * mpi_x + 3
        tag_recv  = 4 * mpi_ixupR1 + 3
        call MPI_IRECV (recv1, m1, my_real, mpi_ixupR1, tag_recv, mpi_comm_beam(1), request8, mpi_err)
        call MPI_ISEND (send1, m1, my_real, mpi_ixdnR1, tag_send, mpi_comm_beam(1), request4, mpi_err)
        buffer_words = buffer_words + m1
        buffer_calls = buffer_calls + 1
        call MPI_WAIT (request4, status, mpi_err)
        call MPI_WAIT (request8, status, mpi_err)
        f2( nx+nx+1:nx+nx+2, iyIni:iYEnd, 1:nz ) = recv1
     else
        f2( nx+nx+1:nx+nx+2, iyIni:iYEnd, 1:nz ) = send1
     endif
     
     sent = buffer_words-sent+1
     !print'(a,2i5,2i8,i10,2x,4l2)','buffer_bl_x: rank, iyIni, sent, sent/nyb, words =', &
     !mpi_rank, iyIni, sent, sent/nyb, buffer_words, &
     !is_not_zero_L, is_not_zero_R, is_not_zero_L1, is_not_zero_R1

     deallocate ( send,  recv )
     deallocate ( send1, recv1 )

  end do

  if (debug2(dbg_mpi,3)) then
     sent1 = buffer_words-sent1+1
     print'(a,i5,i8,f8.2,i4,2x,4l2)','buffer_bl_x: rank, sent, sent/nxyz, calls =', &
          mpi_rank, sent1, sent1/real(nx*ny*nz), buffer_calls-calls, &
          is_not_zero_L, is_not_zero_R, is_not_zero_L1, is_not_zero_R1
  end if

  !$omp end single

end subroutine buffer_blocks_x


!-------------------------------------------------------------------------------
subroutine buffer_blocks_z_bak ( mx, my, mz, ny, nyBlocks, f1, f2, iyIniArr, iyEndArr, kBlockArr)

  use mpi_mod
  use params, only: mpi_rank, mpi_nz

  implicit none
  integer mx, my, mz, m, m1
  real, dimension( mx, my, mz )         :: f1
  real, dimension( mx, my, -1:mz+mz+2 ) :: f2
  real, dimension( :, :, : ), allocatable :: send1, recv1
  integer status(MPI_STATUS_SIZE)
  integer request1, request2, request3, request4
  integer request5, request6, request7, request8

  integer,dimension(my):: iyIniArr, iyEndArr, kBlockArr
  integer mpi_izdnL,  mpi_izupL,  mpi_izdnR,  mpi_izupR
  integer mpi_izdnL1, mpi_izupL1, mpi_izdnR1, mpi_izupR1
  integer ny, nyBlocks, nyb, iyIni, iyEnd, j
  integer kBlockL , kBlockR
  integer kBlockL1, kBlockR1

  ! TO DO: make OpenMP safe?

  ! Block topology:
  ! 
  ! - -------------------------------------  
  !    |    |           |           |    |
  !    | L1 |     L     |     R     | R1 |       
  !    |    |           |           |    |
  ! - -------------------------------------  
  !
  ! L, R  = left and right main blocks, mz grid-points wide
  ! L1,R1 = left and right thin layers, two grid-points thick


  ! case: no split in z direction

  if (mpi_nz == 1) then
     call barrier_omp('buffer_blocks_z')
     !$omp master
     f2( 1:mx, 1:ny,       -1:0      )  =  f1( 1:mx, 1:ny, mz-1:mz )
     f2( 1:mx, 1:ny,       1:mz      )  =  f1( 1:mx, 1:ny,    1:mz )
     f2( 1:mx, 1:ny,    mz+1:mz+mz   )  =  f1( 1:mx, 1:ny,    1:mz )
     f2( 1:mx, 1:ny, mz+mz+1:mz+mz+2 )  =  f1( 1:mx, 1:ny,    1:2  )
     !$omp end master
     call barrier_omp('buffer_blocks_z')
     return
  end if


  ! case: mpi-split in z direction

  call barrier_omp('buffer_blocks_z')
  !$omp master

  do j=1,nyBlocks

     iyIni    = iyIniArr(j) 
     iyEnd    = iyEndArr(j)
     nyb      = iyEnd - iyIni + 1

     kBlockL  = kBlockArr(j)
     kBlockR  = kBlockL + 1
     kBlockR1 = kBlockR + 1
     kBlockL1 = kBlockL - 1

     m        = mx*nyb*mz
     m1       = mx*nyb*2

     allocate ( send  ( 1:mx, 1:nyb, 1:mz), recv  ( 1:mx, 1:nyb, 1:mz) )
     allocate ( send1 ( 1:mx, 1:nyb, 1:2 ), recv1 ( 1:mx, 1:nyb, 1:2 ) )

     ! communicate L and R blocks
     send  = f1( 1:mx, iyIni:iyEnd, 1:mz )
     if (kBlockL.ne.0) then 
        call MPI_CART_SHIFT(new_com, 2, kBlockL, mpi_izdnL, mpi_izupL, mpi_err)
        call MPI_IRECV (recv, m, my_real, mpi_izupL, 4*mpi_izupL  , mpi_com, request5, mpi_err)
        call MPI_ISEND (send, m, my_real, mpi_izdnL, 4*mpi_rank   , mpi_com, request1, mpi_err)
        call MPI_WAIT (request1, status, mpi_err)
        call MPI_WAIT (request5, status, mpi_err)
        f2( 1:mx, iyIni:iYEnd, 1:mz ) = recv
     else
        f2( 1:mx, iyIni:iYEnd, 1:mz ) = f1( 1:mx, iyIni:iYEnd, 1:mz )
     endif

     if (kBlockR.ne.0) then 
        call MPI_CART_SHIFT(new_com, 2, kBlockR, mpi_izdnR, mpi_izupR, mpi_err)
        call MPI_IRECV (recv, m, my_real, mpi_izupR, 4*mpi_izupR+1, mpi_com, request6, mpi_err)
        call MPI_ISEND (send, m, my_real, mpi_izdnR, 4*mpi_rank +1, mpi_com, request2, mpi_err)
        call MPI_WAIT (request2, status, mpi_err)
        call MPI_WAIT (request6, status, mpi_err)
        f2( 1:mx, iyIni:iYEnd, mz+1:mz+mz ) = recv
     else
        f2( 1:mx, iyIni:iYEnd, mz+1:mz+mz ) = f1( 1:mx, iyIni:iYEnd, 1:mz )
     endif

     ! communicate L1 and R1 layers
     send1  = f1( 1:mx, iyIni:iyEnd, mz-1:mz )
     if (kBlockL1.ne.0) then 
        call MPI_CART_SHIFT(new_com, 2, kBlockL1, mpi_izdnL1, mpi_izupL1, mpi_err)
        call MPI_IRECV (recv1, m1, my_real, mpi_izupL1, 4*mpi_izupL1+2, mpi_com, request7, mpi_err)
        call MPI_ISEND (send1, m1, my_real, mpi_izdnL1, 4*mpi_rank+2  , mpi_com, request3, mpi_err)
        call MPI_WAIT (request3, status, mpi_err)
        call MPI_WAIT (request7, status, mpi_err)
        f2( 1:mx, iyIni:iYEnd, -1:0 ) = recv1
     else
        f2( 1:mx, iyIni:iYEnd, -1:0 ) = send1
     endif

     send1  = f1( 1:mx, iyIni:iyEnd, 1:2 )
     if (kBlockR1.ne.0) then 
        call MPI_CART_SHIFT(new_com, 2, kBlockR1, mpi_izdnR1, mpi_izupR1, mpi_err)
        call MPI_IRECV (recv1, m1, my_real, mpi_izupR1, 4*mpi_izupR1+3, mpi_com, request8, mpi_err)
        call MPI_ISEND (send1, m1, my_real, mpi_izdnR1, 4*mpi_rank+3  , mpi_com, request4, mpi_err)
        call MPI_WAIT (request4, status, mpi_err)
        call MPI_WAIT (request8, status, mpi_err)
        f2( 1:mx, iyIni:iYEnd, mz+mz+1:mz+mz+2 ) = recv1
     else
        f2( 1:mx, iyIni:iYEnd, mz+mz+1:mz+mz+2 ) = send1
     endif

     deallocate ( send,  recv )
     deallocate ( send1, recv1 )

  end do

  !$omp end master
  call barrier_omp('buffer_blocks_z')

end subroutine buffer_blocks_z_bak


!-------------------------------------------------------------------------------
subroutine buffer_blocks_x_bak ( mx, my, mz, ny, nyBlocks, f1, f2, iyIniArr, iyEndArr, kBlockArr)

  use mpi_mod
  use params, only: mpi_rank, mpi_nx

  implicit none
  integer mx, my, mz, m, m1
  real, dimension(         mx, my, mz ) :: f1
  real, dimension( -1:mx+mx+2, my, mz ) :: f2
  real, dimension( :, :, : ), allocatable :: send1, recv1
  integer status(MPI_STATUS_SIZE)
  integer request1, request2, request3, request4
  integer request5, request6, request7, request8

  integer,dimension(my):: iyIniArr, iyEndArr, kBlockArr
  integer mpi_ixdnL,  mpi_ixupL,  mpi_ixdnR,  mpi_ixupR
  integer mpi_ixdnL1, mpi_ixupL1, mpi_ixdnR1, mpi_ixupR1
  integer ny, nyBlocks, nyb, iyIni, iyEnd, j
  integer kBlockL , kBlockR
  integer kBlockL1, kBlockR1

  ! TODO: make OpenMP safe?

  ! Block topology:
  ! 
  ! - -------------------------------------  
  !    |    |           |           |    |
  !    | L1 |     L     |     R     | R1 |       
  !    |    |           |           |    |
  ! - -------------------------------------  
  !
  ! L, R  = left and right main blocks, mx grid-points wide
  ! L1,R1 = left and right thin layers, two grid-points thick

  ! case: no split in x direction

  if (mpi_nx == 1) then
     call barrier_omp('buffer_blocks_x')
     !$omp master
     f2(      -1:0      , 1:ny, 1:mz )  =  f1( mx-1:mx, 1:ny, 1:mz )
     f2(       1:mx     , 1:ny, 1:mz )  =  f1(    1:mx, 1:ny, 1:mz )
     f2(    mx+1:mx+mx  , 1:ny, 1:mz )  =  f1(    1:mx, 1:ny, 1:mz )
     f2( mx+mx+1:mx+mx+2, 1:ny, 1:mz )  =  f1(    1:2 , 1:ny, 1:mz )
     !$omp end master
     call barrier_omp('buffer_blocks_x')
     return
  end if

  ! case: mpi-split in x direction

  call barrier_omp('buffer_blocks_x')
  !$omp master

  do j=1,nyBlocks

     iyIni    = iyIniArr(j) 
     iyEnd    = iyEndArr(j)
     nyb      = iyEnd - iyIni + 1

     kBlockL  = kBlockArr(j)
     kBlockR  = kBlockL + 1
     kBlockR1 = kBlockR + 1
     kBlockL1 = kBlockL - 1

     m        = mx*nyb*mz
     m1       =  2*nyb*mz

     allocate ( send  ( 1:mx, 1:nyb, 1:mz), recv  ( 1:mx, 1:nyb, 1:mz) )
     allocate ( send1 ( 1:2 , 1:nyb, 1:mz), recv1 ( 1:2 , 1:nyb, 1:mz) )

     ! communicate L and R blocks
     send  = f1( 1:mx, iyIni:iyEnd, 1:mz )
     if (kBlockL.ne.0) then 
        call MPI_CART_SHIFT(new_com, 0, kBlockL, mpi_ixdnL, mpi_ixupL, mpi_err)
        call MPI_IRECV (recv, m, my_real, mpi_ixupL, 4*mpi_ixupL  , mpi_com, request5, mpi_err)
        call MPI_ISEND (send, m, my_real, mpi_ixdnL, 4*mpi_rank   , mpi_com, request1, mpi_err)
        call MPI_WAIT (request1, status, mpi_err)
        call MPI_WAIT (request5, status, mpi_err)
        f2( 1:mx, iyIni:iYEnd, 1:mz ) = recv
     else
        f2( 1:mx, iyIni:iYEnd, 1:mz ) = f1( 1:mx, iyIni:iYEnd, 1:mz )
     endif

     if (kBlockR.ne.0) then 
        call MPI_CART_SHIFT(new_com, 0, kBlockR, mpi_ixdnR, mpi_ixupR, mpi_err)
        call MPI_IRECV (recv, m, my_real, mpi_ixupR, 4*mpi_ixupR+1, mpi_com, request6, mpi_err)
        call MPI_ISEND (send, m, my_real, mpi_ixdnR, 4*mpi_rank +1, mpi_com, request2, mpi_err)
        call MPI_WAIT (request2, status, mpi_err)
        call MPI_WAIT (request6, status, mpi_err)
        f2( mx+1:mx+mx, iyIni:iYEnd, 1:mz ) = recv
     else
        f2( mx+1:mx+mx, iyIni:iYEnd, 1:mz ) = f1( 1:mx, iyIni:iYEnd, 1:mz )
     endif

     ! communicate L1 and R1 layers
     send1  = f1( mx-1:mx, iyIni:iyEnd, 1:mz )
     if (kBlockL1.ne.0) then 
        call MPI_CART_SHIFT(new_com, 0, kBlockL1, mpi_ixdnL1, mpi_ixupL1, mpi_err)
        call MPI_IRECV (recv1, m1, my_real, mpi_ixupL1, 4*mpi_ixupL1+2, mpi_com, request7, mpi_err)
        call MPI_ISEND (send1, m1, my_real, mpi_ixdnL1, 4*mpi_rank+2  , mpi_com, request3, mpi_err)
        call MPI_WAIT (request3, status, mpi_err)
        call MPI_WAIT (request7, status, mpi_err)
        f2( -1:0, iyIni:iYEnd, 1:mz ) = recv1
     else
        f2( -1:0, iyIni:iYEnd, 1:mz ) = send1
     endif

     send1  = f1( 1:2, iyIni:iyEnd, 1:mz )
     if (kBlockR1.ne.0) then 
        call MPI_CART_SHIFT(new_com, 0, kBlockR1, mpi_ixdnR1, mpi_ixupR1, mpi_err)
        call MPI_IRECV (recv1, m1, my_real, mpi_ixupR1, 4*mpi_ixupR1+3, mpi_com, request8, mpi_err)
        call MPI_ISEND (send1, m1, my_real, mpi_ixdnR1, 4*mpi_rank+3  , mpi_com, request4, mpi_err)
        call MPI_WAIT (request4, status, mpi_err)
        call MPI_WAIT (request8, status, mpi_err)
        f2( mx+mx+1:mx+mx+2, iyIni:iYEnd, 1:mz ) = recv1
     else
        f2( mx+mx+1:mx+mx+2, iyIni:iYEnd, 1:mz ) = send1
     endif

     deallocate ( send,  recv )
     deallocate ( send1, recv1 )

  end do

  !$omp end master
  call barrier_omp('buffer_blocks_x')

end subroutine buffer_blocks_x_bak


!-------------------------------------------------------------------------------
subroutine sum_mpi_real (a, asum, n)

  use params, only: mpi_size, do_validate
  use mpi_mod
  implicit none
  integer :: n, i
  real :: a(n), asum(n)
  real, allocatable, dimension(:,:) :: tmp8
  !
  !$omp master
  if (do_validate) then
     allocate (tmp8(n,mpi_size))
     call MPI_ALLGATHER (a,n,my_real,tmp8,n,my_real,mpi_com,mpi_err) 
     do i=1,n
        asum(i) = sum(tmp8(i,1:mpi_size))
     end do
     deallocate (tmp8)
  else
     call MPI_ALLREDUCE(a,asum,n,MPI_REAL,MPI_SUM,mpi_com,mpi_err)
  end if
  !$omp end master

end subroutine sum_mpi_real

!-------------------------------------------------------------------------------
subroutine mpi_search_geom (mpi_size, np, mmx, mmy, mmz, mpi_nx, mpi_ny, mpi_nz)

  use params, only: master
  implicit none
  integer :: mmx, mmy, mmz
  integer :: mx, my, mz, nx, ny, nz, mpi_nx, mpi_ny, mpi_nz, nodes, np
  integer :: mpi_kx, mpi_ky, mpi_kz, nxyz, nmpi, mpi_size, mmpi
  integer :: nxmin, nxmax, nymin, nymax, nzmin, nzmax, power
  logical :: ok, mpi_search

  mx=mmx; my=mmy; mz=mmz

  do mmpi=mpi_size,mpi_size-16,-1
     nmpi = mmpi
     mpi_kx = mpi_nx
     mpi_ky = mpi_ny
     mpi_kz = mpi_nz
     power = 3
     !   nxyz = mx*my*mz
     if (mpi_kx > 0) then
        nmpi = nmpi/mpi_kx
        !     nxyz = nxyz/mx
        nxyz = my*mz
        power = power-1
     end if
     if (mpi_ky > 0) then
        nmpi = nmpi/mpi_ky
        !     nxyz = nxyz/my
        nxyz = mx*mz
        power = power-1
     end if
     if (mpi_kz > 0) then
        nmpi = nmpi/mpi_kz
        !     nxyz = nxyz/mz
        nxyz = mx*my
        power = power-1
     end if

     !print*,'x',nxyz,nmpi,power
     if (mpi_kx > 0) then
        nx = mx/mpi_kx
        ok = .true.
     else if (nmpi==0) then
        nz = mx
        ok = .false.
     else
        nx = (nxyz/nmpi)**(1./power)
        nxmax = max(nx*2,nxyz/(my*mz))
        nxmin = min(nx/2,nxyz/(my*mz))
        ok = mpi_search (nx, nxmax, +1, mx, nmpi, mpi_kx)
        if (.not. ok) ok = mpi_search (nx, nxmin, -1, mx, nmpi, mpi_kx)
        !     nxyz = nxyz/mx
        nxyz = my*mz
        power = power-1
     end if
     if (ok) then

        !print*,'y',nxyz,nmpi,power
        if (mpi_ky > 0) then
           ny = my/mpi_ky
           ok = .true.
        else if (nmpi==0) then
           ny = my
           ok = .false.
        else
           ny = (nxyz/nmpi)**(1./power)
           nymax = max(ny*2,nxyz/mx)
           nymin = min(ny/2,nxyz/mx)
           ok = mpi_search (ny, nymax, +1, my, nmpi, mpi_ky)
           if (.not. ok) ok = mpi_search (ny, nymin, -1, my, nmpi, mpi_ky)
           !       nxyz = nxyz/my
           nxyz = mx*mz
           power = power-1
        end if
        if (ok) then

           !print*,'z',nxyz,nmpi,power
           if (mpi_kz > 0) then
              nz = mz/mpi_kz
              ok = (mpi_kz==nmpi)
           else if (nmpi==0) then
              nz = mz
              ok = .false.
           else
              !print*,mz,nmpi,nz
              nz = mz/nmpi
              if (nz*nmpi == mz) then
                 mpi_kz = mz/nz
                 ok = .true.
              else
                 ok = .false.
              end if
           end if
           !print*,ok
           np = mpi_kx*mpi_ky*mpi_kz
           if (ok .and. np==mpi_size) then
              !if (master) then
              !print*,'mpi_search_geom:'
              !print'(3i6,3x,3i5,3x,2i6)',nx,ny,nz,mpi_kx,mpi_ky,mpi_kz,np,(np+7)/8
              !end if
              mpi_nx = mpi_kx
              mpi_ny = mpi_ky
              mpi_nz = mpi_kz
              return
           end if
        end if
     end if
  end do

end subroutine mpi_search_geom

!-------------------------------------------------------------------------------
logical function mpi_search (n1, n2, n3, ngrid, nmpi, mpi)

  implicit none
  integer :: n, n1, n2, n3, ngrid, nmpi, mpi

  !print*,''
  n1=max(n1,1)
  n2=max(n2,1)
  !print*,n1,n2,n3,ngrid,nmpi
  do n=n1,n2,n3
     !print*,n
     if (mod(ngrid,n)==0) then
        mpi=ngrid/n
        if (mod(nmpi,mpi)==0) then
           mpi_search=.true.
           n1=n
           nmpi = nmpi/mpi
           !print*,n,mpi,nmpi
           return
        endif
     endif
  end do
  mpi_search=.false.

end function mpi_search

!-------------------------------------------------------------------------------
subroutine alltoallv_mpi (send, nsend, send_count, send_offset, &
     recv, nrecv, recv_count, recv_offset, &
     mpi_type, mpi_rank, mpi_size, comm, &
     use_isend, verbose, word_count)
  use params, only: dump_unit
  use mpi_mod, only: buffer_words, buffer_calls, buffer_calls, buffer_words
  implicit none
  include 'mpif.h'
  real:: send(nsend), recv(nrecv)
  integer:: mpi_type, mpi_rank, mpi_size, comm, err, i, n, o, verbose
  integer:: nsend, nrecv, j
  integer:: use_isend
  integer(kind=MPI_OFFSET_KIND), dimension(0:mpi_size-1):: &
       send_offset, recv_offset
  integer, dimension(0:mpi_size-1):: snd_offset, rcv_offset
  integer, dimension(0:mpi_size-1):: send_count, send_req, &
       recv_count, recv_req
  integer(kind=8):: word_count
  integer:: status(MPI_STATUS_SIZE)
  character(len=32) dbg_file

  !$omp barrier
  !$omp master

  if (verbose == -3) then
     write (dbg_file,'(a,i3.3,a)') 'alltoallv_', mpi_rank,'.txt'
     open(dump_unit,file=dbg_file,form='formatted',status='unknown',position='append')
     write (dump_unit,*) 'use_isend =', use_isend
  end if

  if (any(send_count<0) .or. &
       any(recv_count<0) .or. &
       any(send_offset<0) .or. &
       any(recv_offset<0)) then
     print*,'negative count or offset'
     stop
  end if

  if (use_isend==0) then
     if (verbose == -3) then
        do i=0,mpi_size-1
           if (send_count(i) > 0) then
              n = send_count(i)
              o = send_offset(i) + 1
              write(dump_unit,1) 'sending block with',n,' real4 to rank', &
                   i,' with min, max =', minval(send(o:o+n-1)), maxval(send(o:o+n-1))
1             format(a,i7,a,i6,a,1p,2e12.3)
           end if
        end do
     end if
     if (all(send_offset < 2_8**31)) then
        snd_offset = send_offset
     else
        print*,'send_offset too large:', send_offset
        stop
     end if
     if (all(recv_offset < 2_8**31)) then
        rcv_offset = recv_offset
     else
        print*,'recv_offset too large:', recv_offset
        stop
     end if
     call MPI_ALLTOALLV (send, send_count, snd_offset, mpi_type, &
          recv, recv_count, rcv_offset, mpi_type, &
          comm, err)
     word_count = word_count + sum(send_count)
     buffer_words = buffer_words + sum(send_count)
     buffer_calls = buffer_calls + 1
     !do i=0,mpi_size-1
     !  if (recv_count(i) > 0) then
     !    n = recv_count(i)
     !    o = recv_offset(i) + 1
     !    print 2,mpi_rank,'expcts block with',n,' real4 from rank', &
     !      i,' with min, max =', minval(recv(o:o+n-1)), maxval(recv(o:o+n-1))
     !  2 format(i7,1x,a,i7,a,i6,a,1p,2e12.3)
     !  end if
     !end do

     if (verbose == -3) then
        do i=0,mpi_size-1
           if (recv_count(i) > 0) then
              n = recv_count(i)
              o = recv_offset(i) + 1
              write(dump_unit,1) 'recving block with',n,' real4 from rank', &
                   i,' with min, max =', minval(recv(o:o+n-1)), maxval(recv(o:o+n-1))
           end if
        end do
     end if

  else

     ! Post RECV from all senders that have something to send to us
     j = 0
     do i=0,mpi_size-1
        if (recv_count(i) > 0 .and. i /= mpi_rank) then
           n = recv_count(i)
           o = recv_offset(i) + 1
           !print 2,mpi_rank,'expects block with',n,' real4 from rank', i
           if (verbose == -3) then
              write(dump_unit,1) 'recving block with',n,' real4 from rank',i
           end if
           call MPI_IRECV (recv(o:o+n-1), n, mpi_type, i, i, comm, recv_req(j), err)
           j = j + 1
           if (err /= 0) print*,'alltoallv ERROR 1',mpi_rank,i,err
        end if
     end do
     nrecv = j

     !  Make sure all receivers are ready to receive
     if (use_isend==2) call MPI_BARRIER (comm, err)

     !  Send synchronously to all receivers that we should send something to
     j = 0
     do i=0,mpi_size-1
        if (send_count(i) > 0) then
           n = send_count(i)
           o = send_offset(i) + 1
           if (i == mpi_rank) then
              recv(recv_offset(i)+1:recv_offset(i)+n) = send(o:o+n-1)
           else
              !print 2,mpi_rank,'sending block with',n,' real4 to   rank', &
              !  i,' with min, max =', minval(send(o:o+n-1)), maxval(send(o:o+n-1))
              if (verbose == -3) &
                   write(dump_unit,1) 'sending block with',n,' real4 to rank', &
                   i,' with min, max =', minval(send(o:o+n-1)), maxval(send(o:o+n-1))
              if (use_isend>=2) then
                 call MPI_ISSEND (send(o:o+n-1), n, mpi_type, i, mpi_rank, comm, send_req(j), err)
              else
                 call MPI_ISEND (send(o:o+n-1), n, mpi_type, i, mpi_rank, comm, send_req(j), err)
              end if
              j = j + 1
              if (err /= 0) print*,'alltoallv ERROR 2',mpi_rank,i,err
              word_count = word_count + n
              buffer_words = buffer_words + n
              buffer_calls = buffer_calls + 1
           end if
        end if
     end do
     nsend = j

     !  Wait for all send to complete
     call MPI_WAITALL (nsend, send_req, MPI_STATUSES_IGNORE, err)

     !  Wait for all recv to complete
     call MPI_WAITALL (nrecv, recv_req, MPI_STATUSES_IGNORE, err)

     if (verbose == -3) then
        do i=0,mpi_size-1
           if (recv_count(i) > 0 .and. i /= mpi_rank) then
              n = recv_count(i)
              o = recv_offset(i) + 1
              write(dump_unit,1) 'recved block with',n,' real4 from rank', &
                   i,' with min, max =', minval(recv(o:o+n-1)), maxval(recv(o:o+n-1))
           end if
        end do
     end if

     !do i=0,mpi_size-1
     !  if (recv_count(i) > 0 .and. i /= mpi_rank) then
     !    n = recv_count(i)
     !    o = recv_offset(i) + 1
     !    print 2,mpi_rank,'recved  block with',n,' real4 from rank', &
     !      i,' with min, max =', minval(recv(o:o+n-1)), maxval(recv(o:o+n-1))
     !  end if
     !end do

  end if
  if (verbose == -3) &
       close(dump_unit)

  !$omp end master
  !$omp barrier

end subroutine alltoallv_mpi

!-------------------------------------------------------------------------------
subroutine alltoall_mpi (send, recv, count, mpi_type, comm, use_alltoall, err)

  use params, only: mpi_size, mpi_rank
  use mpi_mod, only: MPI_STATUSES_IGNORE, MPI_WTIME
  implicit none

  integer :: mpi_type, comm, err, i, count
  integer, dimension(count,0:mpi_size-1) :: send, recv
  integer, dimension(0:mpi_size-1) :: send_req, recv_req
  real(8) :: wt
  logical :: use_alltoall

  if (use_alltoall) then
     !$omp barrier
     !$omp master
     call MPI_Alltoall (send, count, mpi_type, recv, count, mpi_type, comm, err)
     !$omp end master
     !$omp barrier
     return
  end if
  !$omp barrier
  !$omp master

  ! Post RECV from all senders
  wt=MPI_WTIME()
  do i=0,mpi_size-1
     call MPI_IRECV (recv(:,i), count, mpi_type, i, i, comm, recv_req(i), err)
  end do

  ! Make sure all receivers are ready to receive
  call MPI_BARRIER (comm, err)
  !if (mpi_rank==0) print *,MPI_WTIME()-wt

  ! Send non-blocking synchronously to all receivers
  do i=0,mpi_size-1
     call MPI_ISSEND (send(:,i), count, mpi_type, i, mpi_rank, comm, send_req(i), err)
  end do
  !if (mpi_rank==0) print *,MPI_WTIME()-wt

  ! Wait for all send to complete
  call MPI_WAITALL (mpi_size, send_req, MPI_STATUSES_IGNORE, err)
  !if (mpi_rank==0) print *,MPI_WTIME()-wt

  ! Wait for all recv to complete
  call MPI_WAITALL (mpi_size, recv_req, MPI_STATUSES_IGNORE, err)
  !if (mpi_rank==0) print *,MPI_WTIME()-wt

  !$omp end master
  !$omp barrier

end subroutine alltoall_mpi

!-------------------------------------------------------------------------------
subroutine test_mpi

  use params, only: master
  implicit none
  integer :: i

  do i=0,2 
     if (master) then
        if (i==0) then
           print*,'Using MPI_Alltoall[v]'
        else if (i==1) then
           print*,'Using Mpi_Isend/Irecv'
        else
           print*,'Using Mpi_Issend/Irecv'
        end if
     end if
     call test_alltoallv(i)
     if (i==1) call test_alltoall(i>0)
  end do

end subroutine test_mpi

!-------------------------------------------------------------------------------
subroutine test_alltoall(use_alltoall)

  use params, only: mid
  use mpi_mod
  use params, only: mpi_size, mpi_rank, master
  implicit none

  integer :: nword, i, j, rank
  integer, allocatable :: local(:), global(:,:)
  real(8) :: wt
  real    :: eps, kB
  logical :: use_alltoall, ok
  character(len=mid) :: id = 'test_alltoall' 

  if (mpi_rank==0) print *, id

  do j=0,1
     nword = 10**j
     allocate (local(nword), global(nword,0:mpi_size-1))

     do i=1,nword
        local(i) = i + nword*mpi_rank
     end do

     wt = MPI_WTIME()
     call alltoall_mpi (local, global, nword, MPI_INTEGER, mpi_comm_world, use_alltoall, mpi_err)
     wt = MPI_WTIME()-wt

     if (mpi_rank==0) then
        ok = .true.
        do rank = 0,mpi_size-1
           do i=1,nword
              local(i) = i + nword*rank
           end do
           !print'(a,/,(10i8))', 'local',local(:)
           !print'(a,/,(10i8))', 'global',global(:,rank)
           eps = maxval(abs(local-global(:,rank)))
           kB = mpi_size*nword*4e-3
           ok = ok .and. (eps == 0)
        end do
        print'(a,2i7,1p,2e10.2,l3)', &
             ' alltoall: mpi_size, nword, wt, kB, ok =', &
             mpi_size, nword, wt, kB, ok
     end if
     deallocate (local, global)
  end do
end subroutine test_alltoall

!-------------------------------------------------------------------------------
subroutine test_alltoallv (use_isend)

  use params, only: mid
  use mpi_mod, only: MPI_OFFSET_KIND, MPI_WTIME, MPI_REAL, mpi_com
  use params, only: mpi_size, mpi_rank
  implicit none

  integer :: use_isend
  integer :: nword, i, j, isnd, ircv
  integer(kind=MPI_OFFSET_KIND), allocatable, dimension(:) :: snd_offset, rcv_offset
  integer, allocatable, dimension(:)   :: snd_count, rcv_count
  real,    allocatable, dimension(:,:) :: snd, rcv
  integer    :: ntry=10, verbose=0, nsnd, nrcv
  integer(8) :: nsent
  real(8) :: wt
  real    :: kB
  character(len=mid) :: id = 'test_alltoallv' 

  if (mpi_rank==0) print *, id

  allocate (snd_count(0:mpi_size-1), rcv_count(0:mpi_size-1))
  allocate (snd_offset(0:mpi_size-1), rcv_offset(0:mpi_size-1))
  ntry = min(ntry,mpi_size)
  do j=0,4
     nword = 10**j
     allocate (snd(nword,ntry), rcv(nword,ntry))
     snd=0.0; rcv=0.0


     ! Construct test data

     snd_count = 0
     rcv_count = 0
     snd_offset = 0
     rcv_offset = 0
     do i=0,ntry-1
        isnd=modulo(mpi_rank+i,mpi_size)             ! send to the next 10 ranks
        ircv=modulo(mpi_rank-i,mpi_size)             ! recv from the previous 10 ranks
        snd_count(isnd)=nword
        rcv_count(ircv)=nword
        snd_offset(isnd)=i*nword
        rcv_offset(ircv)=i*nword
     end do


     ! Time the test

     wt = MPI_WTIME()
     nsnd = nword*ntry
     nrcv = nword*ntry
     call alltoallv_mpi (snd, nsnd, snd_count, snd_offset, &
          rcv, nrcv, rcv_count, rcv_offset, &
          MPI_REAL, mpi_rank, mpi_size, mpi_com, &
          use_isend, verbose, nsent)
     wt = MPI_WTIME()-wt
     if (mpi_rank==0) then
        kB = ntry*nword*4e-3
        print'(a,2i7,1p,2e10.2,l3)', &
             ' alltoallv: mpi_size, nword, wt, kB =', &
             mpi_size, nword, wt, kB
     end if
     deallocate (snd, rcv)
  end do

  deallocate (snd_count, rcv_count, snd_offset, rcv_offset)

end subroutine test_alltoallv

!-------------------------------------------------------------------------------
subroutine send_yx_slice (my, mx, buf2, rank, req)

  use params, only: mpi_x
  use mpi_mod
  implicit none

  integer :: mx, my, myx, rank, req, comm, tag, ierr
  real :: buf2(my, mx)

  myx = my*mx
  tag  = mpi_x
  comm = mpi_comm_beam(1)
  call MPI_Isend (buf2, myx, MPI_REAL, rank, tag, comm, req, ierr)

end subroutine send_yx_slice

!-------------------------------------------------------------------------------
subroutine recv_yx_slice (my, mx, buf2, rank)

  use mpi_mod
  implicit none

  integer :: mx, my, myx, rank, comm, tag, ierr
  integer :: status(MPI_STATUS_SIZE)
  real :: buf2(my, mx)

  myx = my*mx
  tag  = rank
  comm = mpi_comm_beam(1)
  call MPI_Recv (buf2, myx, MPI_REAL, rank, tag, comm, status, ierr)

end subroutine recv_yx_slice

!-------------------------------------------------------------------------------
subroutine send_yz_slice (my, mz, buf2, tag, rank, req)

  use params, only: mpi_z
  use mpi_mod
  implicit none

  integer :: mz, my, myz, rank, req, comm, tag, ierr
  real :: buf2(my, mz)

  myz = my*mz
  comm = mpi_comm_beam(3)
  call MPI_Isend (buf2, myz, MPI_REAL, rank, tag, comm, req, ierr)

end subroutine send_yz_slice

!-------------------------------------------------------------------------------
subroutine recv_yz_slice (my, mz, buf2, tag, rank)

  use mpi_mod
  implicit none

  integer :: mz, my, myz, rank, comm, tag, ierr
  integer :: status(MPI_STATUS_SIZE)
  real :: buf2(my, mz)

  myz = my*mz
  comm = mpi_comm_beam(3)
  call MPI_Recv (buf2, myz, MPI_REAL, rank, tag, comm, status, ierr)

end subroutine recv_yz_slice

!-------------------------------------------------------------------------------
subroutine message_mpi (comm, txt)

  use params, only: master, do_trace
  use mpi_mod
  implicit none

  integer :: comm, ierr
  character(len=*) :: txt

  if (.not. do_trace) return
  print*,my_rank,' has arrived at message barrier'
  call MPI_Barrier (comm, ierr)
  call sleep(1)
  if (master) print *, txt
  call sleep(1)
  call MPI_Barrier (comm, ierr)

end subroutine message_mpi

!-------------------------------------------------------------------------------
subroutine gather_y_mpi (f, ff)

  use params
  use mpi_mod
  implicit none

  real :: f(mx,my,mz), ff(mx,mytot,mz)
  integer :: i_y, nrq, iy1, iy2, status(MPI_STATUS_SIZE)
  integer :: req(mpi_ny)
  real, allocatable, dimension(:,:,:) :: rcv
  logical :: debug2

  ! All ranks send to other ranks in he y-direction

  nrq = 0
  do i_y=0,mpi_ny-1 
     if (i_y == mpi_y) cycle
     nrq = nrq+1
     call MPI_Isend (f, mw, MPI_REAL, i_y, 0, mpi_comm_beam(2), req(nrq), mpi_err)
  end do

  
  ! All ranks receive from other ranks in the y-direction, and copies self

  allocate (rcv(mx,my,mz))
  do i_y=0,mpi_ny-1 
     iy1 = i_y*my + 1
     iy2 = i_y*my + my
     if (i_y == mpi_y) then
        ff(:,iy1:iy2,:) = f(:,:,:)
     else
        call MPI_Recv (rcv, mw, MPI_REAL, i_y, 0, mpi_comm_beam(2), status, mpi_err)
        ff(:,iy1:iy2,:) = rcv
     end if
  end do
  if (debug2(dbg_mpi,3)) print'(i6,a,1p,(63e10.2))', mpi_rank, 'f:', f(1,:,1)
  if (debug2(dbg_mpi,3)) print'(i6,a,1p,(63e10.2))', mpi_rank,'ff:',ff(1,:,1)
  call waitall_mpi (nrq, req)
  deallocate (rcv)

end subroutine gather_y_mpi
