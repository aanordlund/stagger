! $Id: params.f90,v 1.269 2016/11/09 22:32:07 aake Exp $
!***********************************************************************
MODULE params

! Parameters and scalars for the induction equation code

  implicit none
  real   , parameter :: pi=3.1415926536
  integer, parameter :: mfile=80, mid=120
  integer, save      :: data_unit=30, time_unit=31, dump_unit=32, &
                        check_unit=33, flag_unit=34, stdin_unit=35, &
                        limb_unit=36, slice_unit=37, sink_unit=38, &
                        stat_unit=39, surface_unit=40, bdry_unit=41, maxtry=5

  character(len=mfile):: file, from, head, tail, meshfile, scp_dir, scrfile, tmpdir
  character(len=mfile):: inputfile=' '
  character(len=mid):: hl=' '
  integer(kind=8):: mw
  integer :: mx, my, mz, mvar, timeorder, lb, ub, nflop, nstag, nstag1, &
             it, isubstep, nstep, nsnap, nscr, isnap, iread, iwrite, idbg, &
             iseed, iseedold, stdin, stdout, stderr, ihead, itail, npart, &
             mpi_nx, mpi_ny, mpi_nz, mpi_rank, mpi_x, mpi_y, mpi_z, mpi_size, &
             iscr, isnap0, iscr0, omp_nz, n_barrier, dbg_barrier, spin_barrier, &
             i_barrier, io_rank, divb_iter, nrho, mxtot, mytot, mztot, &
             ixoff, iyoff, izoff, nconcurrent, iconcurrent, iconcurrent0, use_isend, &
             stratified
  integer, allocatable, dimension(:) :: ix_mpi_cart, iy_mpi_cart, iz_mpi_cart
  logical :: periodic(3)
  logical :: do_density, do_momenta, do_energy, do_mhd, do_pscalar, do_cool, &
             do_loginterp, do_2nddiv, do_ionization, do_trace, do_dissipation, &
             do_metric, do_displacement, do_scp, do_subdir, do_master_io, do_nodrag, &
             do_compare, do_dump, do_stratified, do_max5, do_max5_hd, do_max5_mhd, &
             do_quench, do_timer, do_kepler, do_flags, do_check, do_massexact, &
             do_single, do_cylatmos, do_upping, do_logcheck, do_init_smooth, &
             do_divb_clean, do_aniso_eta, do_aniso_nu, do_parallel_io, did_io, &
             do_massflux, do_init_smooth_hd, do_lnpg, do_nu4_B, do_concurrent, &
             do_validate, rank_timer, check_barrier
  real(kind=8) :: td
  real :: sx, sy, sz, dx, dy, dz, xmin, ymin, zmin, rt, t, dt, dtold, dtdef, &
          Cdt, Cdtd, Cdtr, nu1, nu2, nu3, nu4, nur, nup, nuB, nuS, nuE, gamma, csound, cmax, &
          Cu, Cv, Cn, Cr, Ce, Cd, Cb, Urms, poff, t_Bgrowth, omega, Csmag, Ssmag, &
          omegax, omegay, omegaz, tsnap, tscr, tstop, Brms, e_grav, e_kin, &
          nu_atmos, nu_inner, nu_outer, t_prop, E_prop, E_mag, eetop, divb_eps, &
          Q_mag, Q_kin, ambi, Umax, d2lnr_lim, tconcurrent, t_decay, Cdivb
  real, allocatable, dimension(:,:,:):: gx1, gx2, gx3, hx1, hx2, hx3
  real, allocatable, dimension(:,:,:):: gy1, gy2, gy3, hy1, hy2, hy3
  real, allocatable, dimension(:,:,:):: gz1, gz2, gz3, hz1, hz2, hz3
  real, allocatable, dimension(:,:,:):: gscr, hscr
  real, allocatable, dimension(:,:):: pbub, surface_int
  real(kind=8), allocatable, dimension(:,:):: haver_tmp
  real,         allocatable, dimension(:):: xm,  ym,  zm,  xmdn,  ymdn,  zmdn, &
                                   dxm, dym, dzm, dxmdn, dymdn, dzmdn, &
                                   dxidxdn, dyidydn, dzidzdn, &
                                   dxidxup, dyidyup, dzidzup, &
                                   xmg, ymg, zmg
  real, allocatable, dimension(:):: sumxy
  real, allocatable, target, dimension(:,:,:):: scratch
  logical master, mpi_master, omp_master
  integer omp_nthreads, omp_mythread, iys, iye, izs, ize
#if defined (SICK_THREADPRIVATE)
!$omp threadprivate(omp_master, master, omp_nthreads, omp_mythread, iys, iye, izs, ize)
#else
  common /omp/omp_nthreads, omp_mythread, iys, iye, izs, ize, omp_master, master
!$omp threadprivate(/omp/)
#endif

!-------------------------------------------------------------------------------
! Allocatable temporary arrays for routines called in global OMP-regions.
! These need to adhere to the following rules:
!
! 1) the master thread allocates, followed by a barrier
! 2) the master thread deallocates, preceded by a barrier
! 3) each thread operates on a sub-section of these arrays
! 4) add a barrier if they need indices outside their subsection
!-------------------------------------------------------------------------------
  real, allocatable, dimension(:,:,:):: tmp1, tmp2, tmp3

  integer:: dbg_select
  integer, save:: dbg_level=0
  integer, parameter:: dbg_mpi     = 1
  integer, parameter:: dbg_omp     = 2
  integer, parameter:: dbg_io      = 4
  integer, parameter:: dbg_hd      = 8
  integer, parameter:: dbg_mhd     = 16
  integer, parameter:: dbg_stagger = 32
  integer, parameter:: dbg_force   = 64
  integer, parameter:: dbg_expl    = 128
  integer, parameter:: dbg_cool    = 256
  integer, parameter:: dbg_rad     = 512
  integer, parameter:: dbg_eos     = 1024
  integer, parameter:: dbg_pscal   = 2048
  integer, parameter:: dbg_fft     = 4096

  real, save:: maxram    = 2.8
  integer, save:: mpi_nmax = 18
  integer, save:: timer_verbose = 0
  integer(8), save:: n_trnslt=0, n_lookup=0, n_reshape=0, n_deshape=0, &
                     n_transfer=0, n_omega=0
CONTAINS

!***********************************************************************
SUBROUTINE init_params
  implicit none
  namelist /io/iread,iwrite,nstep,nsnap,tstop,tsnap,tscr,nscr,do_scp,do_subdir,do_master_io, do_parallel_io, &
               do_single,file,from,scp_dir,do_init_smooth,do_init_smooth_hd,do_divb_clean,divb_iter,divb_eps, &
               do_concurrent,nconcurrent,tconcurrent
  namelist /run/t,dt,Cdt,Cdtd,Cdtr,timeorder,iseed,idbg,do_trace,do_dump,do_compare,do_logcheck, &
                do_check,do_flags,do_timer,do_validate,rank_timer,dbg_select,dbg_barrier, &
                dbg_level,spin_barrier,t_prop,E_prop,E_mag,check_barrier,use_isend
  namelist /grid/mx,my,mz,mpi_nx,mpi_ny,mpi_nz,omp_nz,mpi_nmax,maxram,sx,sy,sz,xmin,ymin,zmin,lb,ub,omega,meshfile
  namelist /vars/do_density,do_momenta,do_energy,do_mhd,do_pscalar
  namelist /pde/do_loginterp,do_2nddiv,do_dissipation,do_metric,do_displacement,stratified, &
                do_massexact,do_kepler,do_nodrag,gamma,csound,nu1,nu2,nu3,nu4,nur,nup,nuB,nuS,nuE,poff,t_Bgrowth, &
                cmax,Csmag,Ssmag,do_max5,do_max5_hd,do_max5_mhd,nu_atmos,nu_inner,nu_outer,do_cylatmos, &
                do_upping,do_aniso_eta,ambi,d2lnr_lim,do_aniso_nu,do_massflux,nrho,do_lnpg,do_nu4_B,Cdivb
  integer i
  character(len=mid):: id='$Id: params.f90,v 1.269 2016/11/09 22:32:07 aake Exp $'

  if (master) print *,id


!-----------------------------------------------------------------------
!  I/O control
!-----------------------------------------------------------------------
  io_rank = 0                               ! I/O node
  iread = -1                                ! snap shot to read
  iwrite = -1                               ! snap shot to write
  isnap = -1                                ! snap shot counter
  nstep = 999999                            ! number of time steps
  nsnap = 20                                ! snapshot every nsnap
  nscr = 10                                 ! scratch file every nscr
  tsnap = 0.                                ! snapshot time interval
  tscr = 0.                                 ! scratch file time interval
  tstop = 999999.                           ! time to stop
  iscr = 0                                  ! scratch dump counter
  do_scp = .false.                          ! scp snapshots?
  do_subdir = .false.                       ! stor node data in subdir?
  do_master_io = .false.                    ! send io through the master?
  do_parallel_io = .false.                  ! use MPI parallel I/O
  do_concurrent = .false.
  nconcurrent = 0
  tconcurrent = 0.
  iconcurrent = 0
  tmpdir = ' '                              ! tmp dir
  scrfile = 'scratch.dat'                   ! scratch file
  file  = 'snapshot.dat'                    ! snapshot file
  from = ' '                                ! from file
  scp_dir = ' '                             ! directory to scp to
  do_init_smooth = .false.                  ! smooth3t initial data
  do_init_smooth_hd = .false.               ! smooth3t initial data
  do_divb_clean = .false.                   ! clean div(B) of initial state
  do_nu4_B = .true.                         ! use nu4 in resist
  Cdivb = 0.                                ! diffuse divB if gt 0
  divb_iter = 15                            ! max iterations
  divb_eps = 1e-4                           ! accuracy

  rewind(stdin); read  (stdin,io)           ! read namelist
  if (nsnap .eq. 0) nsnap=-9999999          ! guard against invalid nsnap

  if (from .eq. ' ') then                   ! check if from file given
    from = file                             ! if not, use the same file
  end if
  call split_name (file)                    ! make head and tail
  if (master) print *,hl
  if (master) write(stdout,io)              ! print namelist

!-----------------------------------------------------------------------
!  Run control
!-----------------------------------------------------------------------
  t     = -1.                               ! initial time
  dtdef = 0.0000111                         ! default time step value
  dt    = dtdef                             ! for checking if changed
  dtold = dtdef                             ! for checking if changed
  Cdt   = 0.5                               ! Courant condition
  Cdtd  = 0.8                               ! Courant condition on viscosity
  Cdtr  = 0.4                               ! Courant condition on viscosity
  idbg  = 0                                 ! debug printut?
  iseed = -77                               ! random number seed
  iseedold = 0                              ! impossible old value
  timeorder = 3                             ! time integration order
  if (nstep<=3) timeorder=1                 ! default for short runs
  do_trace = .false.                        ! trace calls
  dbg_select = 0                            ! select topic for debugging
  do_dump = .false.                         ! enable dumps
  do_compare = .false.                      ! compare results
  dbg_barrier = 0                           ! barrier number to test
  spin_barrier = 0                          ! spins to do at barrier
  do_timer = .false.                        ! timer calls
  rank_timer = .false.                      ! for each ranke
  do_flags = .false.                        ! control execution with .flag filEs
  do_check = .false.                        ! write check files or not
  do_logcheck = .false.                     ! arite check info to log file
  do_validate = .false.                     ! bitwise reduction precision etc
  check_barrier = .false.                   ! check that barriers are in sync
  t_prop  = 0.                              ! proper time, for field upping
  E_prop  = 0.                              ! proper energy, for field upping
  E_mag   = 0.                              ! actual energy
  use_isend = 2                             ! default: use MPI_Issend

  
  rewind (stdin); read  (stdin,run)         ! read namelist
  if (master) print *,hl
  if (master) write(stdout,run)             ! print namelist

  if (tsnap .gt. 0.) then                   ! snapshot every tsnap time interval
    isnap0 = t/tsnap - isnap + 1.00001      ! offset, in case tsnap differs
  end if
  if (tscr  .gt. 0) then                    ! scratch file every tscr time interval
    iscr0  = t/tscr  - iscr  + 1.00001      ! offset, in case tscr differs
  end if
  if (tconcurrent  .gt. 0) then                    ! concurrent file every tconcurrent time interval
    iconcurrent0  = t/tconcurrent  - iconcurrent  + 1.00001      ! offset, in case tconcurrent differs
  end if
  call set_validate (do_validate)           ! turn off timing when validating

!-----------------------------------------------------------------------
!  PDE Flags
!-----------------------------------------------------------------------
  do_pscalar = .true.                       ! passive scalar?
  do_density = .true.                       ! continuuty equation?
  do_momenta = .true.                       ! equations of motion?
  do_energy = .true.                        ! energy equation?
  do_mhd = .false.                          ! mhd equations?
  do_dissipation = .true.                   ! viscous dissipation?
  do_metric = .false.                       ! special/general relativity?
  do_displacement = .false.                 ! calculate displacement current?

  rewind (stdin); read  (stdin,vars)        ! read namelist
  if (master) print *,hl
  if (master) write (stdout,vars)           ! print namelist

!-----------------------------------------------------------------------
!  Grid parameters
!-----------------------------------------------------------------------
  periodic(:) = .true.                      ! default = 3-D periodic
  mx    = 33                                ! avoid powers of two
  my    = mx                                ! powers of two OK
  mz    = mx                                ! powers of two OK
  sx    = 1.                                ! grid size
  sy    = sx                                ! grid size
  sz    = sx                                ! grid size
  xmin  = 1e6                               ! minimum of [xyz]-axis.
  ymin  = 1e6                               ! if [xyz]min=1e6
  zmin  = 1e6                               ! then init_mesh determines value.
  lb    = 1                                 ! lower boundary
  ub    = 0                                 ! upper boundary
  omega = 0.                                ! coordinate system rotation
  omegax = 0.                               ! coordinate system rotation
  omegay = 0.                               ! coordinate system rotation
  omegaz = 0.                               ! coordinate system rotation
  meshfile = 'mesh.dat'                     ! default mesh file
  omp_nz = 0                                ! default: use OMP_NUM_THREADS
  mpi_nx = 1                                ! default from init_geom_mpi
  mpi_ny = 1                                ! default from init_geom_mpi
  mpi_nz = 1                                ! default from init_geom_mpi

  rewind (stdin); read  (stdin,grid)        ! read namelist
  call init_geom_mpi                        ! suggest mpi_n{x,y,z}

  if (ub .eq. 0) ub=my                      ! default ub

  dx=sx/mx ; dy=sy/my ; dz=sz/mz            ! mesh size
  if (mx.eq.1) dx=min(dy,dz)                ! allow 2-D w/o worries
  if (my.eq.1) dy=min(dz,dx)                ! allow 2-D w/o worries
  if (mz.eq.1) dz=min(dx,dy)                ! allow 2-D w/o worries

  if (xmin.eq.1e6) xmin = (1 - mx/2 - 0.5*mod(mx,2))*dx
  if (ymin.eq.1e6) ymin = (1 - my/2 - 0.5*mod(my,2))*dy
  if (zmin.eq.1e6) zmin = (1 - mz/2 - 0.5*mod(mz,2))*dz

  call mesh_mpi                             ! adjust for MPI

  if (master) print *,hl
  if (master) write(stdout,grid)            ! print namelist

  mw = mx*my*int(mz,kind=8)                 ! total words per chunk

!-----------------------------------------------------------------------
!  PDE parameters
!-----------------------------------------------------------------------
  do_stratified = .false.                   ! change in init_boundary
  stratified = 0                            ! subtract average energy (=1 with quench, =2 subtract flux, =3 with quench)
  do_loginterp = .true.                     ! conservative choice
  do_2nddiv = .false.                       ! 2nd order divergences
  do_lnpg = .false.                         ! gradient of ln(Pg)
  do_nodrag = .true.                        ! no net viscous drag
  do_kepler = .false.                       ! subtract Kepler velocity?
  do_massexact = .true.                     ! mass conservation in pde_local_cm
  do_max5 = .false.                         ! use max5 in HD and MHD
  do_max5_hd = .false.                      ! use max5 in HD
  do_max5_mhd = .false.                     ! use max5 in MHD
  gamma = 1.666667                          ! gas gamma
  nu1 = 0.02                                ! normal viscosity
  nu2 = 0.5                                 ! shock viscosity
  nu3 = 0.02                                ! sound speed coeff
  nu4 = 0.0                                 ! lnr gradient coeff
  nuS = 1.0                                 ! solenoidal fraction
  nur = 0.0                                 ! relative mass diffusion
  nup = 0.05                                ! relative passive scalar diffusion
  nuB = 1.0                                 ! relative B diffusion
  nuE = 1.0                                 ! relative energy diffusion
  nu_atmos = 0.                             ! atmospheric diffusion term
  nu_inner = 1.4                            ! inner boundary for atmospheric diffusion
  nu_outer = 1.6                            ! outer boundary for atmospheric diffusion
  do_cylatmos = .false.                     ! whether atmos.diff. term is outside a cylinder or a sphere
  do_upping = .false.                       ! artifical field-upping to keep magnetic energy constant
  do_aniso_eta = .true.                     ! anisotropic eta, for skew aspect ratios
  do_aniso_nu = .true.                      ! anisotropic nu, for skew aspect ratios
  do_massflux = .false.                     ! include viscous mass diffusion
  Csmag = 0.0                               ! Smagorinsky constant
  Ssmag = 0.0                               ! Smagorinsky constant
  csound = 1.0                              ! for polytropic EOS
  cmax = 0.                                 ! max Alfven speed
  poff = 1.0E-13                            ! offset in passive scalar
  t_Bgrowth = 0.                            ! optional growth of B
  ambi = 0.                                 ! ambipolar diffusion
  d2lnr_lim = 0.5                           ! 2nd order divergences when d2lnr larger
  nrho = 0                                  ! decrease compressive viscosity for depth > ym(nrho) [ off by default ]

  rewind (stdin); read  (stdin,pde)         ! read namelist
  if (nu3 .eq. 0.) nu3 = nu1                ! default, to be compatible
  if (do_max5) then                         ! cover both cases
    do_max5_hd = .true.
    do_max5_mhd = .true.
  end if
  if (master) print *,hl
  if (master) write(stdout,pde)             ! print namelist

!-----------------------------------------------------------------------
!  Courant parameters
!-----------------------------------------------------------------------
  Cu = 0.
  Cv = 0.
  Cr = 0.
  Ce = 0.
  Cd = 0.
  Cb = 0.
  Cn = 0.

!-----------------------------------------------------------------------
!  Allocates for all cases
!-----------------------------------------------------------------------
  allocate (pbub(mx,mz), surface_int(mx,mz), haver_tmp(my,mz))
  surface_int = 0.
  allocate (sumxy(mz))                                                  ! for OMP-sums over xy-planes
END SUBROUTINE

!***********************************************************************
SUBROUTINE read_params
  implicit none
  real tsnap0, tscr0, tconcurrent0
  namelist /io/iread,iwrite,nstep,nsnap,tstop,tsnap,tscr,nscr,do_scp,do_subdir,do_master_io,do_parallel_io,  &
               do_single,file,from,scp_dir,do_init_smooth,do_init_smooth_hd,do_divb_clean,divb_iter,divb_eps, &
               do_concurrent,nconcurrent,tconcurrent
  namelist /run/t,dt,Cdt,Cdtd,Cdtr,timeorder,iseed,idbg,do_trace,do_dump,do_compare,do_logcheck, &
                do_check,do_flags,do_timer,do_validate,rank_timer,dbg_select,dbg_barrier, &
                dbg_level,spin_barrier,t_prop,E_prop,E_mag,check_barrier,use_isend
  namelist /pde/do_loginterp,do_2nddiv,do_dissipation,do_metric,do_displacement,stratified, &
                do_massexact,do_kepler,do_nodrag,gamma,csound,nu1,nu2,nu3,nu4,nur,nup,nuB,nuS,nuE,poff,t_Bgrowth, &
                cmax,Csmag,Ssmag,do_max5,do_max5_hd,do_max5_mhd,nu_atmos,nu_inner,nu_outer,do_cylatmos, &
                do_upping,do_aniso_eta,ambi,d2lnr_lim,do_aniso_nu,do_massflux,nrho,do_lnpg,do_nu4_B,Cdivb
  character(len=mid) :: hl=repeat('-',72)

  tscr0  = tscr                             ! save, to check change
  tsnap0 = tsnap
  tconcurrent0 = tconcurrent

  if (stdin.ge.0) then
    rewind (stdin)
    read (stdin,io) ; if (master) print *,hl ; if (master) write(stdout,io )
    rewind (stdin)
    read (stdin,run); if (master) print *,hl ; if (master) write(stdout,run)
    rewind (stdin)
    read (stdin,pde); if (master) print *,hl ; if (master) write(stdout,pde)
  else
    rewind (stdin)
    read (*,io) ; if (master) print *,hl ; if (master) write(stdout,io )
    rewind (stdin)
    read (*,run); if (master) print *,hl ; if (master) write(stdout,run)
    rewind (stdin)
    read (*,pde); if (master) print *,hl ; if (master) write(stdout,pde)
  end if
  if (nu3 .eq. 0.) nu3 = nu1                ! default, to be compatible

  if (nsnap .eq. 0) nsnap=-9999999          ! guard against invalid nsnap
  if (tsnap .gt. 0. .and. tsnap .ne. tsnap0) then
    isnap0 = t/tsnap - isnap + 1            ! offset, in case tsnap differs
  end if
  if (tscr  .gt. 0. .and. tscr  .ne. tscr0 ) then
    iscr0  = t/tscr  - iscr  + 1            ! offset, in case tscr differs
  end if
  if (tconcurrent  .gt. 0. .and. tconcurrent  .ne. tconcurrent0 ) then
    iconcurrent0  = t/tconcurrent  - iconcurrent  + 1            ! offset, in case tscr differs
  end if

  call split_name (file)
END SUBROUTINE

END MODULE params

!***********************************************************************
MODULE variables
  real, allocatable, dimension(:,:,:) :: &
       r,px,py,pz,e,d,drdt,dpxdt,dpydt,dpzdt,dedt,dddt, &
       Bx,By,Bz,dBxdt,dBydt,dBzdt, &
       Ux,Uy,Uz,Ex,Ey,Ez,Fmx,Fmy,Fmz

END MODULE

!***********************************************************************
MODULE arrays
  real, allocatable, target, dimension(:,:,:) :: &
       wk01,wk02,wk03,wk04,wk05,wk06, &
       wk07,wk08,wk09,wk10,wk11,wk12, &
       wk13,wk14,wk15,wk16,wk17,wk18, &
       wk19,wk20,wk21,wk22,wk23, &
       scr1,scr2,scr3,scr4,scr5,scr6
  !real, allocatable, target, dimension(:,:,:,:) :: lns
  real, pointer, dimension(:,:,:) :: &
       etd,eta,fudge,divu, &
       xdnl,ydnl,zdnl,lnu, &
       dd,lne,Sxx,Syy,Szz, &
       Cs,lnr,p,nu,nud,ee, &
       Sxy,Szx,Syz,Txx,Tyy,Tzz,Txy,Tzx,Tyz
  real(kind=8), allocatable, dimension(:):: eeav, dpxav, dpzav, pxav, pyav, pzav, rhoav, lnrav
  real(kind=8), allocatable, dimension(:):: fvisc, fpoynt, frad, fkin, fconv, fei, facous, fedif
  real(kind=8), allocatable, dimension(:):: qqav, qqrav, qqja, wk_L, ekin, emag
  real(kind=8), allocatable, dimension(:):: eav, ttav, pav
  real(kind=8), allocatable, dimension(:):: Uxav, Uyav, Uzav
  real(kind=8), allocatable, dimension(:):: Bxav, Byav, Bzav
  real(kind=8), allocatable, dimension(:):: Exav, Eyav, Ezav
  real(kind=8), allocatable, dimension(:):: rhominh, rhomaxh, eeminh, eemaxh
  real(kind=8), allocatable, dimension(:):: ttminh, ttmaxh, pminh, pmaxh
  real(kind=8), allocatable, dimension(:):: pyavUp, pyavDn, ffpyavUp, ffpyavDn
  real(kind=8), allocatable, dimension(:):: Pelav, Rossav, Plckav, Stdav
END MODULE arrays


!***********************************************************************
SUBROUTINE init_arrays
  USE params
  USE variables
  USE arrays
  implicit none

  integer :: iz

  allocate (wk01 (mx,my,mz), wk02 (mx,my,mz), wk03 (mx,my,mz))          !  3 chunks
  allocate (wk04 (mx,my,mz), wk05 (mx,my,mz), wk06 (mx,my,mz))          !  6 chunks
  allocate (wk07 (mx,my,mz), wk08 (mx,my,mz), wk09 (mx,my,mz))          !  9 chunks
  allocate (wk10 (mx,my,mz), wk11 (mx,my,mz), wk12 (mx,my,mz))          ! 12 chunks
  allocate (wk13 (mx,my,mz), wk14 (mx,my,mz), wk15 (mx,my,mz))          ! 15 chunks
  allocate (wk16 (mx,my,mz), wk17 (mx,my,mz), wk18 (mx,my,mz))          ! 18 chunks
  allocate (wk19 (mx,my,mz), wk20 (mx,my,mz), wk21 (mx,my,mz))          ! 21 chunks
  allocate (wk22 (mx,my,mz), wk23 (mx,my,mz))                           ! 23 chunks

  allocate (scr1 (mx,my,mz), scr2 (mx,my,mz), scr3 (mx,my,mz))          ! 26 chunks
  allocate (scr4 (mx,my,mz), scr5 (mx,my,mz), scr6 (mx,my,mz))          ! 29 chunks

  allocate (r    (mx,my,mz), e    (mx,my,mz)                 )          ! 31 chunks
  allocate (px   (mx,my,mz), py   (mx,my,mz), pz   (mx,my,mz))          ! 34 chunks
  allocate (drdt (mx,my,mz), dedt (mx,my,mz)                 )          ! 36 chunks
  allocate (dpxdt(mx,my,mz), dpydt(mx,my,mz), dpzdt(mx,my,mz))          ! 39 chunks
  if (do_pscalar) then
    allocate (d    (mx,my,mz), dddt (mx,my,mz)                 )        ! 36 chunks
  else
    allocate (d    ( 1, 1, 1), dddt ( 1, 1, 1)                 )        ! 36 chunks
  end if
  if (do_mhd) then
    allocate (Bx   (mx,my,mz), By   (mx,my,mz), Bz   (mx,my,mz))        ! 42 chunks
    allocate (dBxdt(mx,my,mz), dBydt(mx,my,mz), dBzdt(mx,my,mz))        ! 45 chunks
    allocate (Ex   (mx,my,mz), Ey   (mx,my,mz), Ez   (mx,my,mz))        ! 48 chunks
  else
    allocate (Bx   ( 1, 1, 1), By   ( 1, 1, 1), Bz   ( 1, 1, 1))        ! 39 chunks
    allocate (dBxdt( 1, 1, 1), dBydt( 1, 1, 1), dBzdt( 1, 1, 1))        ! 39 chunks
    allocate (Ex   ( 1, 1, 1), Ey   ( 1, 1, 1), Ez   ( 1, 1, 1))        ! 39 chunks
  end if

  allocate (Ux   (mx,my,mz), Uy   (mx,my,mz), Uz   (mx,my,mz))          ! 51 chunks
  if (do_massflux) then
    allocate (Fmx   (mx,my,mz), Fmy   (mx,my,mz), Fmz   (mx,my,mz))     ! 54 chunks
  else
    allocate (Fmx   (1,1,1), Fmy   (1,1,1), Fmz   (1,1,1))              ! 51 chunks
  end if

  allocate ( eeav(my),  dpxav(my), dpzav(my), pyav(my), rhoav(my) )
  allocate ( lnrav(my), pxav(my), pzav(my) )
  allocate ( fvisc(my), fpoynt(my), frad(my), fkin(my), fconv(my), fei(my), facous(my) )
  allocate ( qqav(my), qqrav(my), qqja(my), wk_L(my), ekin(my), emag(my), fedif(my) )
  allocate ( eav(my) , ttav(my) , pav(my)  )
  allocate ( Uxav(my), Uyav(my) , Uzav(my) )
  allocate ( Bxav(my), Byav(my) , Bzav(my) )
  allocate ( Exav(my), Eyav(my) , Ezav(my) )
  allocate ( rhominh(my), rhomaxh(my), eeminh(my), eemaxh(my) )
  allocate ( ttminh(my), ttmaxh(my), pminh(my), pmaxh(my) )
  allocate ( pyavUp(my), pyavDn(my), ffpyavUp(my), ffpyavDn(my) )
  allocate ( Pelav(my), Rossav(my), Plckav(my), Stdav(my) )

!$omp parallel private(iz)
  do iz=izs,ize
    wk01(:,:,iz)=0.; wk02 (:,:,iz)=0.; wk03 (:,:,iz)=0.
    wk04(:,:,iz)=0.; wk05 (:,:,iz)=0.; wk06 (:,:,iz)=0.
    wk07(:,:,iz)=0.; wk08 (:,:,iz)=0.; wk09 (:,:,iz)=0.
    wk10(:,:,iz)=0.; wk11 (:,:,iz)=0.; wk12 (:,:,iz)=0.
    wk13(:,:,iz)=0.; wk14 (:,:,iz)=0.; wk15 (:,:,iz)=0.
    wk16(:,:,iz)=0.; wk17 (:,:,iz)=0.; wk18 (:,:,iz)=0.
    wk19(:,:,iz)=0.; wk20 (:,:,iz)=0.
    scr1(:,:,iz)=0.; scr2 (:,:,iz)=0.; scr3 (:,:,iz)=0.
    scr4(:,:,iz)=0.; scr5 (:,:,iz)=0.; scr6 (:,:,iz)=0.

    r   (:,:,iz)=1.; px   (:,:,iz)=0.; py   (:,:,iz)=0.; pz   (:,:,iz)=0.; e    (:,:,iz)=1.
    drdt(:,:,iz)=0.; dpxdt(:,:,iz)=0.; dpydt(:,:,iz)=0.; dpzdt(:,:,iz)=0.; dedt (:,:,iz)=0.
    Ux  (:,:,iz)=0.; Uy   (:,:,iz)=0.; Uz   (:,:,iz)=0.
    if (do_pscalar) then
      d (:,:,iz)=1e-15; dddt (:,:,iz)=0.
    end if
    if (do_mhd) then
      Bx   (:,:,iz)=0.; By   (:,:,iz)=0.; Bz   (:,:,iz)=0.
      dBxdt(:,:,iz)=0.; dBydt(:,:,iz)=0.; dBzdt(:,:,iz)=0.
      Ex   (:,:,iz)=0.; Ey   (:,:,iz)=0.; Ez   (:,:,iz)=0.
      pbub(:,iz) = 0.
    end if
    if (do_massflux) then
      Fmx  (:,:,iz)=0.; Fmy   (:,:,iz)=0.; Fmz   (:,:,iz)=0.
    end if
  end do
!$omp end parallel

  !call test_haver
  !call test_zaver
END SUBROUTINE init_arrays

!***********************************************************************
FUNCTION lrec(m)
  USE params
  implicit none
  integer lrec, m, io_word

!  Return the record length to use in OPEN(.., recl=lrec(m), ...) for m words
!  The multiplier io_word may be changed by OS/compilers/$(COMPILER).f90, and
!  may also be changed using the &io input namelist 

  lrec = io_word()*m
END FUNCTION lrec

!***********************************************************************
SUBROUTINE assert_small (message, error, tolerance)
  USE params
  implicit none
  character(len=*) message
  real error, tolerance

  if (error < tolerance) then
    if (master) print 1, message, ' OK, for error & tolerance =', error, tolerance
  else
    print 1, message, ' FAILED for error & tolerance = ', error, tolerance &
           , ' on processor & thread', mpi_rank, omp_mythread
  end if
1 format(1x,2a,1p,2e10.3,a,2i5)
END SUBROUTINE assert_small

!***********************************************************************
SUBROUTINE assert_code (message, test, code)
  USE params
  implicit none
  character(len=*) message
  logical test
  integer code

  if (test) then
    if (.not.master) return
    !if (do_trace) print 1, message, ' OK, code = ', code
  else
    print 1, message, ' FAILED, code, rank = ', code,mpi_rank
  end if
1 format(1x,2a,i5)
END SUBROUTINE assert_code

!***********************************************************************
SUBROUTINE print_id (id)
  USE params
  implicit none
  character(len=mid) id
  real wallclock
  logical debug
  
  if (do_trace) then
    if (master) print '(1x,f9.3,2(2x,i4),2x,a)',wallclock(),dbg_select,i_barrier,id
    return
  end if

  if (id .ne. ' ') then
    if (master) print '(1x,a,f12.2)', trim(id), wallclock()
    call barrier_omp('pr_id1')
    !$omp critical
    id = ' '
    !$omp end critical
    call barrier_omp('pr_id2')
  end if
END SUBROUTINE print_id

!***********************************************************************
SUBROUTINE split_name (ofile)
  USE params
  implicit none
  character(len=mfile) ofile

  ihead = index(ofile,'/',back=.true.)      ! index to slash
  ihead = max(ihead,0)
  itail = ihead+1
  tail = ofile(itail:mfile)                 ! file name only
  if (ihead.gt.0) then
    head = file(1:ihead)                    ! dir name
  else
    head = './'                             ! current dir
    ihead = 2
  end if
END SUBROUTINE split_name

!***********************************************************************
FUNCTION debug (dbg_id)
  USE params
  implicit none
  logical debug
  integer dbg_id
  
  debug = iand(dbg_id,dbg_select) /= 0 .and. dbg_level > 8
END FUNCTION debug

!***********************************************************************
FUNCTION debug2 (dbg_id, level)
  USE params
  implicit none
  logical debug2
  integer dbg_id, level
  
  debug2 = iand(dbg_id,dbg_select) /= 0 .and. dbg_level >= level
END FUNCTION debug2

!***********************************************************************
SUBROUTINE print_trace (id, dbg, label)
  USE params
  implicit none
  character(len=*) id
  character(len=*) label
  integer dbg
  real wallclock
  logical debug
  
  if (do_trace .or. debug(dbg)) then
    if (master .or. dbg_level>1) print '(1x,f9.3,2x,a,2x,a)',wallclock(),label,id
  end if
END SUBROUTINE print_trace

!*******************************************************************************
FUNCTION intarg (i, default)
  USE params
  implicit none
  integer i, intarg, default
  character(len=16) arg
!...............................................................................
  call getarg (i, arg)
  if (trim(arg) == '') then
    intarg = default
  else
    read (arg,*) intarg
  end if
END FUNCTION intarg

