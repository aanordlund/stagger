! $Id: corona_omp.f90,v 1.1 2006/03/29 21:28:59 aake Exp $

MODULE corona
  real t_Bbdry,v_Ampl,Umax
END MODULE

!-----------------------------------------------------------------------
SUBROUTINE init_boundary
  USE params
  USE corona
  USE points
  implicit none

  character(len=80):: id='$Id: corona_omp.f90,v 1.1 2006/03/29 21:28:59 aake Exp $'
  if (id.eq.' ') print *,id; id=' '

  t_Bbdry  = 1                                  ! bdry decay time for B
  call read_boundary

  write (*,*) 'set dx,dz to follwing',dx,dz,xm(2)-xm(1),zm(2)-zm(1)
  call init_points
END

!-----------------------------------------------------------------------
SUBROUTINE read_boundary
  USE params
  USE corona
  implicit none
 ! namelist /bdry/ t_Bbdry,v_ampl,Umax
  namelist /bdry/ v_ampl,Umax

  if (stdin.ge.0) then
    rewind (stdin); read (stdin,bdry)
  else
    read (*,bdry)
  end if
  write (*,bdry)
END

!-----------------------------------------------------------------------
SUBROUTINE density_boundary (r)
  USE params
  implicit none
  real, dimension(mx,my,mz):: r
!
!  Version for linear cases
!
  call extrapolate_center(r)
END

!-----------------------------------------------------------------------
SUBROUTINE density_boundary_log (r,lnr)
  USE params
  implicit none
  real, dimension(mx,my,mz):: r,lnr
  integer iy,iz
  real hrhomax, rhof(lb), rhoav, p
!
!  Density boundary condition for open boundaries -- extrapolate in the log
!
  call extrapolate_center(lnr)
  do iz=izs,ize
    do iy=1,lb-1
      r(:,iy,iz)=exp(lnr(:,iy,iz))
    end do
    do iy=ub+1,my
      r(:,iy,iz)=exp(lnr(:,iy,iz))
    end do
  end do
END

!-----------------------------------------------------------------------
SUBROUTINE energy_boundary (r,lnr,Ux,Uy,Uz,e,ee,p,Bx,By,Bz)
  USE params
  USE eos
  USE units
  implicit none
  real, dimension(mx,my,mz):: r,lnr,Ux,Uy,Uz,e,ee,p,Bx,By,Bz
  real, save:: rbot=-1., pbot, sbot, ebot, eetop
  real s, pb
  integer ix, iy, iz

  do iz=izs,ize
    ee(:,lb,iz)=6000./u_temp
  end do
  call symmetric_center (ee)
  do iz=izs,ize
    do iy=1,lb-1
      e(:,iy,iz)=r(:,iy,iz)*ee(:,iy,iz)
    end do
    do iy=ub+1,my
      e(:,iy,iz)=r(:,iy,iz)*ee(:,iy,iz)
    end do
  end do
END

!-----------------------------------------------------------------------
SUBROUTINE passive_boundary (r,Ux,Uy,Uz,d,dd)
  USE params
  implicit none
  real, dimension(mx,my,mz):: r,Ux,Uy,Uz,d,dd
  integer iy, iz

  call symmetric_center(dd)
  do iz=izs,ize
    do iy=1,lb-1
      d(:,iy,iz)=r(:,iy,iz)*dd(:,iy,iz)
    end do
    do iy=ub+1,my
      d(:,iy,iz)=r(:,iy,iz)*dd(:,iy,iz)
    end do
  end do
END

!-----------------------------------------------------------------------
SUBROUTINE velocity_boundary (r,xdnr,ydnr,zdnr,Ux,Uy,Uz,px,py,pz,e,Bx,By,Bz)
  USE params
  USE corona
  implicit none
  real, dimension(mx,my,mz):: r,xdnr,ydnr,zdnr,Ux,Uy,Uz,px,py,pz,e,ee,Bx,By,Bz
  real p, rin, rtot, fmout, fmtot, uyin, uytot
  integer ix, iy, iz


!call boundary_driving (v_ampl,Ux,Uy,Uz)

  do iz=izs,ize
    Uy(:,1:lb+1,iz) = max(min(Uy(:,1:lb+1,iz),Umax),-Umax)
    py(:,1:lb+1,iz) = Uy(:,1:lb+1,iz)*ydnr(:,1:lb+1,iz)
    px(:,1:lb+1,iz) = Ux(:,1:lb+1,iz)*xdnr(:,1:lb+1,iz)
    pz(:,1:lb+1,iz) = Uz(:,1:lb+1,iz)*zdnr(:,1:lb+1,iz)
  end do

  call extrapolate_center (Ux)
  call extrapolate_center (Uz)

  call antisymmetric_face_upper (Uy)
  call     symmetric_face_lower (Uy)
  do iz=izs,ize
    do iy=1,lb
      px(:,iy,iz) = xdnr(:,iy,iz)*Ux(:,iy,iz)
      py(:,iy,iz) = ydnr(:,iy,iz)*Uy(:,iy,iz)
      pz(:,iy,iz) = zdnr(:,iy,iz)*Uz(:,iy,iz)
    end do
    do iy=ub,my
      px(:,iy,iz) = xdnr(:,iy,iz)*Ux(:,iy,iz)
      py(:,iy,iz) = ydnr(:,iy,iz)*Uy(:,iy,iz)
      pz(:,iy,iz) = zdnr(:,iy,iz)*Uz(:,iy,iz)
    end do
  end do
END

!-----------------------------------------------------------------------
SUBROUTINE mfield_boundary (Ux,Uy,Uz,Bx,By,Bz)
  USE params
  implicit none
  real, dimension(mx,my,mz):: Ux,Uy,Uz,Bx,By,Bz

END

!-----------------------------------------------------------------------
SUBROUTINE efield_boundary (Ex, Ey, Ez, Bx_y, Bx_z, By_z, By_x, Bz_x, Bz_y, &
                            Rx, Ry, Rz, Ux_y, Ux_z, Uy_z, Uy_x, Uz_x, Uz_y, &
                            Ux, Uy, Uz)
  USE params
  USE corona
  implicit none
  real, dimension(mx,my,mz):: Ex, Ey, Ez, Bx_y, Bx_z, By_z, By_x, Bz_x, Bz_y, &
                              Rx, Ry, Rz, Ux_y, Ux_z, Uy_z, Uy_x, Uz_x, Uz_y, &
                              Ux, Uy, Uz, By_y
  real, dimension(mx,mz):: flb, fub

  call yup_set (By_z, By_y)
  if (izs.eq.1) then
    flb = +Uz(:,lb,:)*By_y(:,lb,:)
  end if
  call extrapolate_face_value_lower (Ex, flb)
  call yup_set (By_x, By_y)
  if (izs.eq.1) then
    flb = -Ux(:,lb,:)*By_y(:,lb,:)
  end if
  call extrapolate_face_value_lower (Ez, flb)
  call symmetric_face_upper (Ex)
  call symmetric_face_upper (Ez)
END

!-----------------------------------------------------------------------
SUBROUTINE regularize (f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
!
  call symmetric_center(f)
END

!-----------------------------------------------------------------------
SUBROUTINE regularize_face (f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
!
  call symmetric_face (f)
END

!-----------------------------------------------------------------------
SUBROUTINE ddt_boundary (r,px,py,pz,e,Bx,By,Bz, &
                         drdt,dpxdt,dpydt,dpzdt, &
                         dedt,dBxdt,dBydt,dBzdt)
  USE params
  implicit none
  real, dimension(mx,my,mz):: r,px,py,pz,e,Bx,By,Bz, &
                              drdt,dpxdt,dpydt,dpzdt, &
                              dedt,dBxdt,dBydt,dBzdt

END
