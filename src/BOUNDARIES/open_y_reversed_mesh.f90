! $Id: open_y_reversed_mesh.f90,v 1.7 2004/07/22 13:56:43 aake Exp $

MODULE boundary
  real t_Bbdry, Bscale
END module

!-----------------------------------------------------------------------
SUBROUTINE init_boundary
  USE params
  USE boundary
  implicit none

  character(len=80):: id='$Id: open_y_reversed_mesh.f90,v 1.7 2004/07/22 13:56:43 aake Exp $'
  if (id.ne.' ') print *,id; id=' '

  t_Bbdry  = 1                                  ! bdry decay time for B
  Bscale = dym(lb)*10.
  lb = 6
  ub = my-5
  call read_boundary
END

!-----------------------------------------------------------------------
SUBROUTINE read_boundary
  USE params
  USE boundary
  implicit none
  namelist /bdry/ lb, ub, t_Bbdry, Bscale

  if (stdin.ge.0) then
    rewind (stdin); read (stdin,bdry)
  else
    read (*,bdry)
  end if
  write (*,bdry)
END

!-----------------------------------------------------------------------
SUBROUTINE density_boundary (r)
  USE params
  implicit none
  real, dimension(mx,my,mz):: r
!
!  Version for linear cases
!
  call extrapolate_center(r)
END

!-----------------------------------------------------------------------
SUBROUTINE density_boundary_log (r,lnr)
  USE params
  implicit none
  real, dimension(mx,my,mz):: r,lnr
  integer iy,iz
  real hrhomax, rhof(lb), rhoav, p
!
!  Density boundary condition for open boundaries -- extrapolate in the log
!
  call extrapolate_center(lnr)
  hrhomax = dym(lb)*my/10.
  do iy=1,lb
!    rhof(iy) = (lb-iy)*dym(lb)/hrhomax         ! bracket density in top ghost zones
    rhof(iy) = (lb-iy)/real(lb)
  end do
  rhoav = sum(r(:,lb,:))/(mx*mz)
!$omp parallel do private(iz,iy,p)
  do iz=1,mz
    do iy=1,lb-1
      p=iy/real(lb)
!      lnr(:,iy,iz)=min(lnr(:,iy,iz),lnr(:,lb,iz)-rhof(iy)) ! avoid run away
!      r(:,iy,iz)=exp(lnr(:,iy,iz))
      r(:,iy,iz) = rhoav*(1-p)+r(:,lb,iz)*p
      lnr(:,iy,iz)=alog(r(:,iy,iz))
    end do
    do iy=ub+1,my
      r(:,iy,iz)=exp(lnr(:,iy,iz))
    end do
  end do
END

!-----------------------------------------------------------------------
SUBROUTINE energy_boundary (r,lnr,Ux,Uy,Uz,e,ee,p,Bx,By,Bz)
  USE params
  USE eos
  implicit none
  real, dimension(mx,my,mz):: r,lnr,Ux,Uy,Uz,e,ee,p,Bx,By,Bz
  real, save:: rbot=-1., pbot, sbot, ebot, eetop
  real s, pb, pbmax, efact
  integer ix, iy, iz

!!!$omp parallel do private(iz)
!!  call eos1(mx,mz,r(:,ub,:),r(:,ub,:),p=p(:,ub,:))

  if (rbot .lt. 0.) then                                                ! first time
    call pressure (r,ee,p=p)                                            ! single call
    rbot=sum(r(:,ub,:))/(mx*mz)                                         ! save density
    ebot=sum(e(:,ub,:))/(mx*mz)                                         ! save density
    pbot=sum(p(:,ub,:))/(mx*mz)                                         ! save pressure
!    sbot=sum(p(:,ub,:)/r(:,ub,:)**gamma)/(mx*mz)                        ! save entropy
    if (idbg.gt.0) print *,'ub, rbot, ebot =', ub, rbot, ebot
  end if
  eetop = sum(e(:,lb,:)/r(:,lb,:))/(mx*mz)

  if (do_mhd) then
    pbmax = 0.5*maxval(by(:,ub,:)**2)
  else
    pbmax = 0.
  end if
!$omp parallel do private(ix,iz,pb,efact)
  do iz=1,mz
    do ix=1,mx
      r(ix,ub,iz) = rbot
      e(ix,ub,iz) = ebot
      !efact = 1.+0.02*(cos((ix-97)*2.*pi/mx)+cos((iz-91)*2.*pi/mz))
      efact = 1.
      e(ix,ub,iz) = e(ix,ub,iz)*efact
      r(ix,ub,iz) = r(ix,ub,iz)*efact**(1./gamma)
      if (do_mhd) then
        pb = 0.5*by(ix,ub,iz)**2                                        ! should really be centered
        efact = (1.-0.4*pb/pbmax)                                       ! hardwire 40% max magnetic pressure
        e(ix,ub,iz) = e(ix,ub,iz)*efact                                 ! because of EOS this is crude
        r(ix,ub,iz) = r(ix,ub,iz)*efact**(1./gamma)                     ! use approximate gamma
        ee(ix,lb,iz) = eetop
        e(ix,lb,iz) = eetop*r(ix,lb,iz)                                 ! smooth top temperature
      end if
      ee(ix,ub,iz) = e(ix,ub,iz)/r(ix,ub,iz)                            ! redo energy per unit mass
    end do
  end do
  call extrapolate_center_upper(ee)                                     ! extrapolate at bottom
  call symmetric_center_lower(ee)                                       ! vanishing deriv at top
  if (do_loginterp) then
    call density_boundary_log (r,lnr)                                   ! readjust density
  else
    call density_boundary(r)                                            ! readjust density
  end if

!$omp parallel do private(iz,iy)
  do iz=1,mz
    do iy=1,lb-1
      e(:,iy,iz)=r(:,iy,iz)*ee(:,iy,iz)
    end do
    do iy=ub+1,my
      e(:,iy,iz)=r(:,iy,iz)*ee(:,iy,iz)
    end do
  end do
END

!-----------------------------------------------------------------------
SUBROUTINE passive_boundary (r,Ux,Uy,Uz,d,dd)
  USE params
  implicit none
  real, dimension(mx,my,mz):: r,Ux,Uy,Uz,d,dd
  integer iy, iz

  call symmetric_center(dd)
!$omp parallel do private(iz,iy)
  do iz=1,mz
    do iy=1,lb-1
      d(:,iy,iz)=r(:,iy,iz)*dd(:,iy,iz)
    end do
    do iy=ub+1,my
      d(:,iy,iz)=r(:,iy,iz)*dd(:,iy,iz)
    end do
  end do
END

!-----------------------------------------------------------------------
SUBROUTINE velocity_boundary (r,xdnr,ydnr,zdnr,Ux,Uy,Uz,px,py,pz,e,Bx,By,Bz)
  USE params
  implicit none
  real, dimension(mx,my,mz):: r,xdnr,ydnr,zdnr,Ux,Uy,Uz,px,py,pz,e,ee,Bx,By,Bz
  real p, rin, rtot, fmout, fmtot, uyin, uytot
  integer ix, iy, iz
!
!  Bottom boundary
!
  rin  = 1e-30
  rtot = 0.
  fmout = 0.
  fmtot = 0.
  do iz=1,mz
    do ix=1,mx
      fmtot = fmtot +    py(ix,ub,iz)                   ! mass flux
      rtot  = rtot  +  ydnr(ix,ub,iz)                   ! mass
      if (py(ix,ub,iz) .ge. 0.) then                    ! outgoing
        fmout = fmout +   py(ix,ub,iz)                  ! mass flux
      else                                              ! incoming
        rin   = rin   + ydnr(ix,ub,iz)                  ! mass
      end if
    end do
  end do
  uytot = fmtot/rtot
  uyin = -fmout/rin                                     ! target incoming velocity
  if (idbg.gt.0) print *,'uyin, uytot =', uyin, uytot

  p = 0.001
  do iz=1,mz
    do ix=1,mx
      if (py(ix,ub,iz) .lt. 0.) then                    ! incoming
!        Uy(ix,ub,iz) = Uy(ix,ub,iz)*(1.-p) + uyin*p     ! adjust towards target
!  The line above is kept for the case where vanishing mass flux is enforced,
!  rather than constant pressure.  For constant pressure the mass flux is non-zero.
        Ux(ix,ub,iz) = Ux(ix,ub,iz)*(1.-p)              ! adjust towards zero
        Uz(ix,ub,iz) = Uz(ix,ub,iz)*(1.-p)              ! adjust towards zero
      end if
    end do
  end do
  call symmetric_center_upper (Ux)
  call symmetric_face_upper (Uy)
  call symmetric_center_upper (Uz)
!
!  Top boundary, vanishing values
!
  call vanishing_center_lower (px)
!  call symmetric_face_lower   (Uy)
  call vanishing_center_lower (pz)
!
!  Vanishing vertical mass flux
!
!$omp parallel do private(iz,iy)
  do iz=1,mz
    do iy=1,lb
      py(:,iy,iz)=iy/real(lb+1)*py(:,lb+1,iz)
    end do
  end do  
!
!  Recompute mass fluxes
!
!$omp parallel do private(iz,iy)
  do iz=1,mz
    do iy=1,lb
!      px(:,iy,iz)=xdnr(:,iy,iz)*Ux(:,iy,iz)
      ux(:,iy,iz)=px(:,iy,iz)/xdnr(:,iy,iz)
      uy(:,iy,iz)=py(:,iy,iz)/ydnr(:,iy,iz)
      uz(:,iy,iz)=pz(:,iy,iz)/zdnr(:,iy,iz)
!      pz(:,iy,iz)=zdnr(:,iy,iz)*Uz(:,iy,iz)
    end do
    do iy=ub,my
      px(:,iy,iz)=xdnr(:,iy,iz)*Ux(:,iy,iz)
      py(:,iy,iz)=ydnr(:,iy,iz)*Uy(:,iy,iz)
      pz(:,iy,iz)=zdnr(:,iy,iz)*Uz(:,iy,iz)
    end do
  end do
END

!-----------------------------------------------------------------------
SUBROUTINE mfield_boundary (Ux,Uy,Uz,Bx,By,Bz)
  USE params
  USE boundary
  implicit none
  real, dimension(mx,my,mz):: Ux,Uy,Uz,Bx,By,Bz
!
!  Require the horizontal field components to vanish
!
!  call extrapolate_center_lower(Bx)             ! top boundary:
!  call extrapolate_center_lower(Bz)             ! extrapolate
!  call antisymmetric_center_upper(Bx)           ! bottom boundary:
!  call antisymmetric_center_upper(Bz)           ! vanishing horizontal

  call mixed_center(Bx,Bscale)
  call mixed_center(Bz,Bscale)
  call symmetric_face(By)   ! TEMPORARY HACK: to be replaced

!  call extrapolate_center_lower(Bx)             ! top boundary:
!  call extrapolate_center_lower(Bz)             ! extrapolate
!  call vanishing_center_upper(Bx)               ! bottom boundary
!  call vanishing_center_upper(Bz)               ! bottom boundary
!  call symmetric_face(By)   ! TEMPORARY HACK: to be replaced

END

!-----------------------------------------------------------------------
SUBROUTINE efield_boundary (Ex, Ey, Ez, Bx_y, Bx_z, By_z, By_x, Bz_x, Bz_y, &
                            Rx, Ry, Rz, Ux_y, Ux_z, Uy_z, Uy_x, Uz_x, Uz_y, &
                            Ux, Uy, Uz)
  USE params
  USE boundary
  implicit none
  real, dimension(mx,my,mz):: Ex, Ey, Ez, Bx_y, Bx_z, By_z, By_x, Bz_x, Bz_y, &
                              Ux_y, Ux_z, Uy_z, Uy_x, Uz_x, Uz_y, &
                              Ux, Uy, Uz
  real, dimension(mx,2,mz):: Rx, Ry, Rz
!
!  Allow the top B-field to evolve, bottom B-field -> zero
!
  call extrapolate_center_lower (Ex)                  ! allow extrap
  call extrapolate_center_lower (Ez)                  ! allow extrap
  call derivative_face_upper (Ex, -Bz_y*(1./t_Bbdry))   ! bottom boundary
  call derivative_face_upper (Ez, +Bx_y*(1./t_Bbdry))   ! damp out horizontal B
END

!-----------------------------------------------------------------------
SUBROUTINE regularize (f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
!
  call symmetric_center(f)
END

!-----------------------------------------------------------------------
SUBROUTINE regularize_face (f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
!
  call symmetric_face(f)
END

!-----------------------------------------------------------------------
SUBROUTINE ddt_boundary (r,px,py,pz,e,Bx,By,Bz, &
                         drdt,dpxdt,dpydt,dpzdt, &
                         dedt,dBxdt,dBydt,dBzdt)
  USE params
  USE boundary
  implicit none
  real, dimension(mx,my,mz):: r,px,py,pz,e,Bx,By,Bz, &
                              drdt,dpxdt,dpydt,dpzdt, &
                              dedt,dBxdt,dBydt,dBzdt
  integer ix,iz
  real rlb
!
  rlb = sum(r(:,lb,:))/(mx*mz)
!$omp parallel do private(iz,ix)
  do iz=1,mz
    do ix=1,mx
      drdt (ix,ub,iz) = 0.
      dedt (ix,ub,iz)=0.
      if (py(ix,ub,iz).lt.0.) then
        dpxdt(ix,ub,iz)=0.
!        dpydt(ix,ub,iz)=0.
!  The line above needs to be activated when controlling the inflow velocity
        dpzdt(ix,ub,iz)=0.
      endif
      if (py(ix,lb,iz).gt.0.) then
        drdt (ix,lb,iz) = drdt(ix,lb,iz) + (1./t_Bbdry)*(rlb-r(ix,lb,iz))
      end if
    end do
!    dpxdt(:,:,iz)=0.       ! for testing static conditions
!    dpydt(:,:,iz)=0.
!    dpzdt(:,:,iz)=0.
  end do
END
