! $Id: mhd_shock_bdry.f90,v 1.3 2005/01/13 13:24:04 aake Exp $

MODULE boundary
  real t_Bbdry, Bscale, lb_eampl, t_bdry
  real rbot, pbot, ebot, eetop, htop
  logical debug
END module

!-----------------------------------------------------------------------
SUBROUTINE init_boundary
  USE params
  USE boundary
  implicit none

  character(len=mid):: id='$Id: mhd_shock_bdry.f90,v 1.3 2005/01/13 13:24:04 aake Exp $'
  if (id.ne.' ') print *,id; id=' '

  t_bdry = 0.01
  t_Bbdry  = 1                                  ! bdry decay time for B
  Bscale = sy*10./my
  lb = 6
  ub = my-9
  lb_eampl = 0.
  rbot = -1.
  htop = 0.1
  debug = .false.
  call read_boundary
END

!-----------------------------------------------------------------------
SUBROUTINE read_boundary
  USE params
  USE boundary
  implicit none
  namelist /bdry/ lb, ub, t_bdry, t_Bbdry, Bscale, lb_eampl, htop, debug

  if (stdin.ge.0) then
    rewind (stdin); read (stdin,bdry)
  else
    read (*,bdry)
  end if
  write (*,bdry)
END

!-----------------------------------------------------------------------
SUBROUTINE density_boundary (r)
  USE params
  implicit none
  real, dimension(mx,my,mz):: r
!
!  Version for linear cases
!
  call extrapolate_center(r)
END

!-----------------------------------------------------------------------
SUBROUTINE density_boundary_open (r,lnr,py,Cs)
  USE params
  USE boundary
  USE forcing
  implicit none
  real, dimension(mx,my,mz):: r,lnr,py,Cs
  real rtop, rtop1, ftot, offset, sc
  integer ix,iy,iz
!
!  Density boundary condition for bottom
!
  call extrapolate_center_log_upper(r)
!
!  Density boundary condition for top open boundary 
!

  rtop=sum(r(:,lb,:))/(mx*mz)
  ftot = 0.
!$omp parallel do private(iz,ix), reduction(+:ftot)
  do iz=1,mz
    do ix=1,mx
        ftot=ftot+py(ix,lb+1,iz)
    enddo
  enddo
! test point
  rtop1 = rtop - dt*0.01*dyidyup(lb)*ftot/(mx*mz)
  offset = alog(rtop1/rtop)
  do iz=1,mz
    lnr(:,lb,iz)=lnr(:,lb,iz) + offset
      r(:,lb,iz)=exp(lnr(:,lb,iz))
!$omp parallel do private(iy)
    do iy=lb-1,1,-1
      lnr(:,iy,iz)=lnr(:,iy,iz)+(Cs(:,iy,iz)*dt*dyidyup(iy))* &
          (lnr(:,iy+1,iz)-gy-lnr(:,iy,iz))
      r(:,iy,iz)=exp(lnr(:,iy,iz))
    enddo
  enddo

  END
!-----------------------------------------------------------------------
SUBROUTINE density_boundary_log (r,lnr)
  USE params
  USE boundary
  implicit none
  real, dimension(mx,my,mz):: r,lnr
  integer ix,iy,iz
  real hrhomax
!
!  Density boundary condition for open boundaries -- extrapolate in the log.
!  Also, prevent the density from flipping over to drive input.
!
  call extrapolate_center(lnr)
!$omp parallel do private(iz,iy,ix,hrhomax)
  do iz=1,mz
    do ix=1,mx
      hrhomax=htop
      do iy=lb-1,1,-1
        lnr(:,iy,iz)=min(lnr(:,iy,iz),lnr(:,iy+1,iz)-(ym(iy+1)-ym(iy))/htop)
        r(:,iy,iz)=exp(lnr(:,iy,iz))
      end do
    end do
    do iy=ub+1,my
      r(:,iy,iz)=exp(lnr(:,iy,iz))
    end do
  end do
END

!-----------------------------------------------------------------------
SUBROUTINE energy_boundary (r,lnr,Ux,Uy,Uz,e,ee,p,Bx,By,Bz,py,Cs)
  USE params
  USE eos
  USE boundary
  implicit none
  real, dimension(mx,my,mz):: r,lnr,Ux,Uy,Uz,e,ee,p,Bx,By,Bz,py,Cs
  real s, pb, pbmax, efact
  integer ix, iy, iz

!!!$omp parallel do private(iz)
!!  call eos1(mx,mz,r(:,ub,:),r(:,ub,:),p=p(:,ub,:))

  if (rbot .lt. 0.) then                                ! first time
    call extrapolate_center_upper(ee)                   ! extrapolate at bottom
    call constant_center_lower(ee)                      ! vanishing deriv at top
    call pressure (r,ee,p)                              ! single call
    rbot=sum(r(:,ub,:))/(mx*mz)                         ! save density
    ebot=sum(e(:,ub,:))/(mx*mz)                         ! save density
    pbot=sum(p(:,ub,:))/(mx*mz)                         ! save pressure
    if (debug) print *,'ub, rbot, ebot =', ub, rbot, ebot
  end if

! eetop = sum(e(:,lb,:)/r(:,lb,:))/(mx*mz)
  eetop = sum(e(:,lb+1,:)/r(:,lb+1,:))/(mx*mz)

  if (do_mhd) then
    pbmax = 0.
!$omp parallel do private(iz,ix), reduction(max:pbmax)
    do iz=1,mz
      do ix=1,mx
        pbmax = max(pbmax,0.5*by(ix,ub,iz)**2)
      end do
    end do
  else
    pbmax = 0.
  end if

!$omp parallel do private(ix,iz,pb,efact)
  do iz=1,mz
    do ix=1,mx
      if (lb_eampl .ne. 0) then
        efact = 1.+lb_eampl*(cos((ix-97)*2.*pi/mx)+cos((iz-91)*2.*pi/mz))
        e(ix,ub,iz) = e(ix,ub,iz)*efact
        r(ix,ub,iz) = r(ix,ub,iz)*efact**(1./gamma)
      end if
      ee(ix,ub,iz) = e(ix,ub,iz)/r(ix,ub,iz)       ! redo energy per unit mass
      ee(ix,lb,iz) = eetop                      ! constant energy per unit mass
      e(ix,lb,iz) = eetop*r(ix,lb,iz)              ! smooth top temperature
    end do
  end do

  call extrapolate_center_upper(ee)                ! extrapolate at bottom

! call constant_center_lower(ee)                   ! vanishing deriv at top
!$omp parallel do private(iz,iy)
  do iz=1,mz
    do iy=lb-1,1,-1
      ee(:,iy,iz)=ee(:,iy,iz)+(Cs(:,iy,iz)*dt*dyidyup(iy))* &
        (ee(:,iy+1,iz)-ee(:,iy,iz))
    enddo
  enddo

  if (do_loginterp) then
    call density_boundary_open (r,lnr,py,Cs)                ! readjust density
  else
    call density_boundary(r)                                ! readjust density
  end if

! limits were lb-1 and ub+1 changed to lb and ub -- 9/29/04

!$omp parallel do private(iz,iy)
  do iz=1,mz
    do iy=1,lb
      e(:,iy,iz)=r(:,iy,iz)*ee(:,iy,iz)
    end do
    do iy=ub,my
      e(:,iy,iz)=r(:,iy,iz)*ee(:,iy,iz)
    end do
  end do

END

!-----------------------------------------------------------------------
SUBROUTINE passive_boundary (r,Ux,Uy,Uz,d,dd)
  USE params
  implicit none
  real, dimension(mx,my,mz):: r,Ux,Uy,Uz,d,dd
  integer iy, iz

  call symmetric_center(dd)
!$omp parallel do private(iz,iy)
  do iz=1,mz
    do iy=1,lb-1
      d(:,iy,iz)=r(:,iy,iz)*dd(:,iy,iz)
    end do
    do iy=ub+1,my
      d(:,iy,iz)=r(:,iy,iz)*dd(:,iy,iz)
    end do
  end do
END

!-----------------------------------------------------------------------
SUBROUTINE velocity_boundary (r,xdnr,ydnr,zdnr,Ux,Uy,Uz,px,py,pz,e,Bx,By,Bz,Cs)
  USE params
  USE boundary
  implicit none
  real, dimension(mx,my,mz):: r,xdnr,ydnr,zdnr,Ux,Uy,Uz,px,py,pz,e,ee,Bx,By,Bz,Cs
  real p, rin, rtot, fmout, fmtot, uyin, uytot
  integer ix, iy, iz
  real domega
!
!  Bottom boundary
!
  rin  = 1e-30
  rtot = 0.
  fmout = 0.
  fmtot = 0.
!$omp parallel do private(iz,ix), reduction(+:fmtot,rin,rtot,fmout)
  do iz=1,mz
    do ix=1,mx
      fmtot = fmtot +    py(ix,ub,iz)                   ! mass flux
      rtot  = rtot  +  ydnr(ix,ub,iz)                   ! mass
      if (py(ix,ub,iz) .ge. 0.) then                    ! outgoing
        fmout = fmout +   py(ix,ub,iz)                  ! mass flux
      else                                              ! incoming
        rin   = rin   + ydnr(ix,ub,iz)                  ! mass
      end if
    end do
  end do
  uytot = fmtot/rtot
  uyin = -fmout/rin                                  ! target incoming velocity
  if (debug) print *,'uyin, uytot =', uyin, uytot
!
!  Bottom Boundary, options (ub)
!
  call symmetric_center_upper (Ux)
! call constant_center_upper  (Ux)
! call symmetric_face_upper   (Uy)
  call constant_center_upper  (Uy)
  call symmetric_center_upper (Uz)
! call constant_center_upper  (Uz)
!
!  Top boundary, vanishing values (lb)
!
  call vanishing_center_lower (Ux)
! call symmetric_face_lower   (Uy)
! call constant_face_lower    (Uy)
! call extrapolate_face_lower (Uy)
  call vanishing_center_lower (Uz)

!$omp parallel do private(iz,iy)
  do iz=1,mz
    do iy=lb,1,-1
      Uy(:,iy,iz)=Uy(:,iy,iz)+(Cs(:,iy,iz)*dt*dyidyup(iy))* &
        (Uy(:,iy+1,iz)-Uy(:,iy,iz))
    enddo
  enddo

!
!  Recompute mass fluxes
!
!$omp parallel do private(iz,iy)
  do iz=1,mz
    do iy=1,lb
      px(:,iy,iz)=xdnr(:,iy,iz)*Ux(:,iy,iz)
      py(:,iy,iz)=ydnr(:,iy,iz)*Uy(:,iy,iz)
      pz(:,iy,iz)=zdnr(:,iy,iz)*Uz(:,iy,iz)
    end do
    do iy=ub,my
      px(:,iy,iz)=xdnr(:,iy,iz)*Ux(:,iy,iz)
      py(:,iy,iz)=ydnr(:,iy,iz)*Uy(:,iy,iz)
      pz(:,iy,iz)=zdnr(:,iy,iz)*Uz(:,iy,iz)
    end do
  end do

END

!-----------------------------------------------------------------------
SUBROUTINE mfield_boundary (Ux,Uy,Uz,Bx,By,Bz)
  USE params
  USE boundary
  implicit none
  real, dimension(mx,my,mz):: Ux,Uy,Uz,Bx,By,Bz
!
!  Require the horizontal field components to vanish
!
!  call extrapolate_center_lower(Bx)             ! top boundary:
!  call extrapolate_center_lower(Bz)             ! extrapolate
!  call antisymmetric_center_upper(Bx)           ! bottom boundary:
!  call antisymmetric_center_upper(Bz)           ! vanishing horizontal

!  call mixed_center(Bx,Bscale)
!  call mixed_center(Bz,Bscale)
!  call symmetric_face(By)   ! TEMPORARY HACK: to be replaced

!  call extrapolate_center_lower(Bx)             ! top boundary:
!  call extrapolate_center_lower(Bz)             ! extrapolate
!  call vanishing_center_upper(Bx)               ! bottom boundary
!  call vanishing_center_upper(Bz)               ! bottom boundary
!  call symmetric_face(By)   ! TEMPORARY HACK: to be replaced

!  call extrapolate_center_lower(Bx)
   call symmetric_center_lower(Bx)
   call extrapolate_face_lower(By)
!  call extrapolate_center_lower(Bz)
   call symmetric_center_lower(Bz)

!  call extrapolate_center_upper(Bx)
   call symmetric_center_upper(Bx)
   call extrapolate_face_upper(By)
!  call extrapolate_center_upper(Bz)
   call symmetric_center_upper(Bz)

END

!-----------------------------------------------------------------------
SUBROUTINE efield_boundary (Ex, Ey, Ez, Bx_y, Bx_z, By_z, By_x, Bz_x, Bz_y, &
                            Rx, Ry, Rz, Ux_y, Ux_z, Uy_z, Uy_x, Uz_x, Uz_y, &
                            Ux, Uy, Uz)
  USE params
  USE boundary
  implicit none
  real, dimension(mx,my,mz):: Ex, Ey, Ez, Bx_y, Bx_z, By_z, By_x, Bz_x, Bz_y, &
                              Ux_y, Ux_z, Uy_z, Uy_x, Uz_x, Uz_y, &
                              Ux, Uy, Uz
  real, dimension(mx,2,mz):: Rx, Ry, Rz
!
!  Allow the top B-field to evolve, bottom B-field -> zero
!
!  call extrapolate_center_lower (Ex)                  ! allow extrap
!  call extrapolate_center_lower (Ez)                  ! allow extrap
!  call derivative_face_upper (Ex, -Bz_y*(1./t_Bbdry)) ! bottom boundary
!  call derivative_face_upper (Ez, +Bx_y*(1./t_Bbdry)) ! damp out horizontal B

  call constant_center(Ex)
  call constant_center(Ez)

END

!-----------------------------------------------------------------------
SUBROUTINE regularize (f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
!
  call symmetric_center (f)
END

!-----------------------------------------------------------------------
SUBROUTINE regularize_face (f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
!
  call symmetric_face (f)
END

!-----------------------------------------------------------------------
SUBROUTINE ddt_boundary (r,px,py,pz,e,Bx,By,Bz, &
                         drdt,dpxdt,dpydt,dpzdt, &
                         dedt,dBxdt,dBydt,dBzdt)
  USE params
  USE eos
  USE boundary
  implicit none
  real, dimension(mx,my,mz):: r,px,py,pz,e,Bx,By,Bz, &
                              drdt,dpxdt,dpydt,dpzdt, &
                              dedt,dBxdt,dBydt,dBzdt
  integer ix,iz,i0,ixp1,izp1
  real rub,pyub,elb,eub,ct,pub,dpydtub,scr,pb
!
  rub = 0.
  eub = 0.
  pub = 0.
  pyub = 0.
  dpydtub = 0.
!$omp parallel do private(iz), reduction(+:rub,eub,pub,pyub,dpydtub)
  do iz=1,mz
    rub = rub + sum(r(:,ub,iz))
    eub = eub + sum(e(:,ub,iz))
    pub = pub + sum(pbot0(:,iz))
    pyub = pyub + sum(1.5*py(:,ub,iz)-0.5*py(:,ub-1,iz))                                        ! py is regular(?)
    dpydtub = dpydtub + sum(1.5*dpydt(:,ub,iz)-0.5*dpydt(:,ub-1,iz))                            ! extrapolate to bdry
  end do
  rub = rub/(mx*mz)
  eub = eub/(mx*mz)
  pub = pub/(mx*mz)
  pyub = pyub/(mx*mz)
  dpydtub = dpydtub/(mx*mz)
  if (debug) then
    print*,'rub',rub
    print*,'eub',eub
    print*,'pub',pub
    print*,'pyub',pyub
    print*,'dpydtub',dpydtub
  end if

  ct = 0.5/t_bdry                                                                               ! damping constant
  i0=0
  if (debug) print*,'ct',ct
!$omp parallel do private(iz,ix,scr), if (.not.debug)
  do iz=1,mz
    izp1 = mod(iz,mz)+1
    do ix=1,mx

      if (do_mhd) then
        ixp1 = mod(ix,mx)+1
        pb = 0.125*((bx(ix,ub,iz)+bx(ixp1,ub  ,iz  ))**2 + &
                    (by(ix,ub,iz)+by(ix  ,ub+1,iz  ))**2 + &
                    (bz(ix,ub,iz)+bz(ix  ,ub  ,izp1))**2)
      else
        pb = 0.
      end if

      if (debug.and.ix.eq.1.and.iz.eq.1) print*,'dpydt',dpydt(ix,ub,iz)

!     dpydt(ix,ub,iz) = dpydt(ix,ub,iz) - (dpydtub + pyub*ct)/1.5                               ! damp out average mass flux

      if (debug.and.ix.eq.1.and.iz.eq.1) print*,'dpydt',dpydt(ix,ub,iz)

!      dedt(ix,ub,iz) = -(pbot0(ix,iz)+pb-pub)*(e(ix,ub,iz)+pbot0(ix,iz))/(gamma*pbot0(iz,iz))*ct ! cancel pressure fluctuation

      if (debug.and.ix.eq.1.and.iz.eq.1) print*,'dedt',dedt(ix,ub,iz)

!     drdt(ix,ub,iz) = -(pbot0(ix,iz)+pb-pub)*r(ix,ub,iz)/(gamma*pbot0(iz,iz))*ct               ! at constant entropy

      if (debug.and.ix.eq.1.and.iz.eq.1) print*,'drdt',drdt(ix,ub,iz)

      if (py(ix,ub,iz).lt.0.) then                                                              ! for incoming flows..
        scr = ((e(ix,ub,iz)-ebot) - &
               (e(ix,ub,iz)+pbot0(ix,iz))*(r(ix,ub,iz)-rbot)/r(ix,ub,iz))/(gamma*t_bdry)        ! prop to entropy fluctuation
        if (debug.and.i0.eq.0) then
            print*,'scr',scr,ix,iz
          print*,'dedt',dedt(ix,ub,iz)
        end if
!       dedt(ix,ub,iz) = dedt(ix,ub,iz) - scr*(1.+e(ix,ub,iz)/r(ix,ub,iz)*dlnpdE_r(ix,iz))      ! cancel entropy fluctuation
        if (debug.and.i0.eq.0) then
          print*,'dedt',dedt(ix,ub,iz)
          print*,'drdt',drdt(ix,ub,iz)
        end if
!        drdt(ix,ub,iz) = drdt(ix,ub,iz) + scr*dlnpdE_r(ix,iz)                                  ! at constant pressure
          if (debug.and.i0.eq.0) print*,'drdt',drdt(ix,ub,iz)
        dpxdt(ix,ub,iz) = -px(ix,ub,iz)*ct                                                      ! damp horizontal motions
        dpzdt(ix,ub,iz) = -pz(ix,ub,iz)*ct
        i0=1
      endif

    end do
  end do
END

