!  $Id: potential_lower_mpi.f90,v 1.17 2014/08/31 14:00:08 aake Exp $
!***********************************************************************
MODULE potential_lower_m
  USE params
  implicit none
  real(kind=8), allocatable, dimension(:)     :: bxh, bzh
  real,         allocatable, dimension(:,:)   :: fy, fh, kh, byt
  real,         allocatable, dimension(:,:)   :: bt, bf
  logical, save:: first_time=.true.
END MODULE

!***********************************************************************
SUBROUTINE init_potential_lower
  USE potential_lower_m
  implicit none
!-----------------------------------------------------------------------
  if (.not.first_time) return
  !$omp barrier
  !$omp master
  first_time = .false.
  !$omp end master
  !$omp barrier
  call test_fft2d
  first_time = .false.
END

!***********************************************************************
SUBROUTINE dealloc_potential_lower
  USE potential_lower_m
  implicit none
!-----------------------------------------------------------------------
!$omp barrier
!$omp master
  deallocate (bxh, bzh)
  deallocate (fy, fh, kh, byt, bt, bf)
  first_time = .true.
!$omp end master
END

!----------------------------------------------------------------------
SUBROUTINE potential_lower (bx, by, bz, lb1, lb2, lb3)
  USE params
  implicit none
  real, dimension(mx,my,mz):: bx, by, bz
  integer lb1, lb2, lb3
  logical omp_in_parallel
!
  if (omp_in_parallel()) then
    call potential_lower_omp (bx, by, bz, lb1, lb2, lb3)
  else
    !$omp parallel shared(lb1)
    call potential_lower_omp (bx, by, bz, lb1, lb2, lb3)
    !$omp end parallel
  end if
END

!----------------------------------------------------------------------
SUBROUTINE potential_lower_omp (bx, by, bz, lb1, lb2, lb3)
!
!  Potential field extrapolation into the ghost zones.
!
!  A potential field, periodic in x and z, is exponentially
!  decreasing in the direction away from the boundary, with
!  amplitude factors exp(-y*k_xz) for each Fourier component.
!
!  Here we assume that 
!    Bx(i,j,iz) is centered at (i-1/2,j,iz),
!    By(i,j,iz) is centered at (i,j-1/2,iz),
!    Bz(i,j,iz) is centered at (i,j,iz-1/2).
!
!  If Bx and Bz were integer centered they would be exactly
!  pi/4 out of phase horizontally relative to By.  Since they
!  are half zone centered, a zero point that should be at j
!  is really at j+0.5, so we need to add that phase factor.
!
!  The extrapolation is based on By(:,lb1,:), which is centered 
!  at (ix,lb1+1/2,iz).  The first layer above this is the one
!  with Bx(:,lb,:), which is centered at (ix,lb,iz). 
!
!  lb1 is the By source layer.  1,lb2 is the By potential range.
!  1,lb3 is the Bx and Bz potential range.  Possoble choices:
!
!  lb1=lb+1 (the last layer inside)
!  lb2=lb   (from the first layer outside)
!  lb3=lb   (includes the boundary itself)
!
!  lb1=lb   (the first layer outside)
!  lb2=lb-1 (from the 2nd layer outside)
!  lb3=lb-1 (from the 1st lager outside)
!
  USE params
  USE arrays
  USE boundary, only: Bx0_top, Bz0_top
  USE potential_lower_m
  USE mpi_mod, only: mpi_comm_plane
  implicit none
  integer:: ix, iy, iz, lb1, lb2, lb3
  real:: asin, acos, kx(mx), kz(mz)
  real, dimension(mx,my,mz):: bx, by, bz
  real, allocatable, dimension(:,:,:):: bf1, bt1
  character(len=mid), save:: id='potential_lower: $Id: potential_lower_mpi.f90,v 1.17 2014/08/31 14:00:08 aake Exp $'

  call print_id(id)

!  Horizontal Fourier transform of the vertical boundary field
!  fft2df uses srfttf which return the sin transform in odd terms
!  and the cos transforms in the even terms.

  if (first_time) call init_potential_lower
  if (mpi_y > 0) return

  allocate (bxh(my), bzh(my))
  allocate (fy(mx,mz), fh(mx,mz), kh(mx,mz), byt(mx,mz))
  allocate (bt(mx,mz), bf(mx,mz))

  bf(:,izs:ize) = by(:,lb1,izs:ize)
  call fft2df (bf, byt, mx, mz)
                                                     call timer('mhd','pot_low')
  call haverage_subr (bx, bxh)
  call haverage_subr (bz, bzh)

  !$omp single
  if (Bx0_top .ne. 0.0) bxh(lb+1) = Bx0_top
  if (Bz0_top .ne. 0.0) bzh(lb+1) = Bz0_top
  do iy=1,lb
    bxh(iy) = (bxh(lb+1)*2.-bxh(lb+2+(lb-iy))) ! extrapolate average horizontal field
    bzh(iy) = (bzh(lb+1)*2.-bzh(lb+2+(lb-iy))) ! extrapolate average horizontal field
  end do
  !$omp end single

  do ix=1,mx
    kx(ix) = 2.*pi/sx*((ix+mpi_x*mx)/2)
  end do
  do iz=1,mz
    kz(iz) = 2.*pi/sz*((iz+mpi_z*mz)/2)
    do ix=1,mx
      kh(ix,iz) = sqrt(kx(ix)**2+kz(iz)**2) + 1e-30
    end do
  end do

!  Potential Y component

  allocate (bt1(mx,lb2,mz), bf1(mx,lb2,mz))
  do iy=1,lb2
    do iz=izs,ize
      bt1(:,iy,iz) = byt(:,iz)*exp((ymdn(iy)-ymdn(lb1))*kh(:,iz))
    end do
  end do
                                                     call timer('mhd','pot_low')
  call fft2dby (bt1, bf1, mx, lb2, mz)
  do iy=1,lb2
    by(:,iy,izs:ize) = bf1(:,iy,izs:ize)
  end do
  deallocate (bt1, bf1)
                                                     call timer('mhd','fft2dby')

!  Potential

  allocate (bt1(mx,lb3,mz), bf1(mx,lb3,mz))
  if(mpi_x==0.and.mpi_z==0) kh(1,1)=1.
  do iy=1,lb3
    do iz=izs,ize
      bt1(:,iy,iz) = byt(:,iz)*exp((ym(iy)-ymdn(lb1))*kh(:,iz))/kh(:,iz)
    end do
    if(mpi_x==0.and.mpi_z==0) bt1(1,iy,1)=0.
  end do
                                                     call timer('mhd','pot_low')
  call fft2dby (bt1, bf1, mx, lb3, mz)
  do iy=1,lb3
    scr1(:,iy,izs:ize) = bf1(:,iy,izs:ize)
  end do
  deallocate (bt1, bf1)
                                                     call timer('mhd','fft2dby')
  !call message_mpi (mpi_comm_plane(2), 'MK2')

!  Horizontal field components from the potential, with special treatment if the mean

  call ddxdn_set (scr1, scr2)
  call ddzdn_set (scr1, scr3)
  do iy=1,lb3
    bx(:,iy,izs:ize) = scr2(:,iy,izs:ize) + bxh(lb)
    bz(:,iy,izs:ize) = scr3(:,iy,izs:ize) + bzh(lb)
  end do
                                                     call timer('mhd','pot_low')
  deallocate (bxh, bzh)
  deallocate (fy, fh, kh, byt)
  deallocate (bt, bf)
END
