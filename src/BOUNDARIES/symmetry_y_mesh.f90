! $Id: symmetry_y_mesh.f90,v 1.7 2009/01/25 16:27:12 aake Exp $

! Boundary conditions.  The indices are assumed to be arranged like so:
!
!                                lb           ub=m-lb+1
! centers:    1   2   3   4   5   6 ...       -5  -4  -3  -2  -1   0
! faces  :  1   2   3   4   5   6   ... -6  -5  -4  -3  -2  -1   0 
!
! For face centered variables, the index ranges 1:lb and ub+1:my are
! external.  For cell centered variables, the ranges are 1:lb-1 and ub+1:my.
!
! Valid points for plotting:
!
!                 F90                   IDL                  total
! center:  lb  :ub = g+1:m-g    lb-1:m-lb = g  :m-g-1   ub-lb+1 = m-2*g
!   face:  lb+1:ub = g+2:m-g    lb  :m-lb = g+1:m-g-1   ub-lb   = m-2*g-1
!
!-----------------------------------------------------------------------
SUBROUTINE constant_face_lower(f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
  integer:: iy,iz
  logical omp_in_parallel

  if (mpi_y == 0) then
    do iz=izs,ize
    do iy=1,lb
      f(:,iy,iz) = f(:,lb+1,iz)
    end do
    end do
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE constant_center_lower(f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
  integer:: iy,iz
  logical omp_in_parallel

  if (mpi_y == 0) then
    do iz=izs,ize
    do iy=1,lb-1
      f(:,iy,iz) = f(:,lb,iz)
    end do
    end do
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE constant_center_upper(f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
  integer:: iy,iz
  logical omp_in_parallel

  if (mpi_y == mpi_ny-1) then
    do iz=izs,ize
    do iy=ub+1,my
      f(:,iy,iz) = f(:,ub,iz)
    end do
    end do
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE constant_center(f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f

  call constant_center_lower(f)
  call constant_center_upper(f)
END

!-----------------------------------------------------------------------
SUBROUTINE symmetric_center_lower(f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
  integer:: iy,iz
  logical omp_in_parallel

  if (mpi_y == 0) then
                                   !if (do_trace) print *,'sym_c_l:', omp_mythread
    do iz=izs,ize
    do iy=1,lb-1
      f(:,iy,iz) = f(:,lb+(lb-iy),iz)
    end do
    end do
                                   !if (do_trace) print *,'sym_c_l:', omp_mythread
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE symmetric_center_upper(f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
  integer:: iy,iz
  logical omp_in_parallel

  if (mpi_y == mpi_ny-1) then
                                   !if (do_trace) print *,'sym_c_u:', omp_mythread, ub, izs, ize
    do iz=izs,ize
    do iy=ub+1,my
      f(:,iy,iz) = f(:,ub-(iy-ub),iz)
    end do
    end do
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE symmetric_center(f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f

                                   !if (do_trace) print *,'sym_c:', omp_mythread
  call symmetric_center_lower(f)
  call symmetric_center_upper(f)
END

!-----------------------------------------------------------------------
SUBROUTINE symmetric_face_lower(f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
  integer:: iy,iz
  logical omp_in_parallel

  if (mpi_y == 0) then
    do iz=izs,ize
    do iy=1,lb
      f(:,iy,iz) = f(:,lb+1+(lb-iy),iz)
    end do
    end do
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE symmetric_face_upper(f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
  integer:: iy,iz
  logical omp_in_parallel

  if (mpi_y == mpi_ny-1) then
    do iz=izs,ize
    do iy=ub+1,my
      f(:,iy,iz) = f(:,ub+1-(iy-ub),iz)
    end do
    end do
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE symmetric_face(f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f

  call symmetric_face_lower(f)
  call symmetric_face_upper(f)
END

!-----------------------------------------------------------------------
SUBROUTINE antisymmetric_center_lower(f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
  integer:: iy,iz
  logical omp_in_parallel

  if (mpi_y == 0) then
    do iz=izs,ize
    do iy=1,lb-1
      f(:,iy,iz) = -f(:,lb+(lb-iy),iz)
    end do
    f(:,lb,iz) = 0.
    end do
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE antisymmetric_center_upper(f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
  integer:: iy,iz
  logical omp_in_parallel

  if (mpi_y == mpi_ny-1) then
    do iz=izs,ize
    do iy=ub+1,my
      f(:,iy,iz) = -f(:,ub-(iy-ub),iz)
    end do
    f(:,ub,iz) = 0.
    end do
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE antisymmetric_center(f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f

  call antisymmetric_center_lower(f)
  call antisymmetric_center_upper(f)
END

!-----------------------------------------------------------------------
SUBROUTINE antisymmetric_face_lower(f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
  integer:: iy,iz
  logical omp_in_parallel

  if (mpi_y == 0) then
    do iz=izs,ize
    do iy=1,lb
      f(:,iy,iz) = -f(:,lb+1+(lb-iy),iz)
    end do
    end do
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE antisymmetric_face_upper(f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
  integer:: iy,iz
  logical omp_in_parallel

  if (mpi_y == mpi_ny-1) then
    do iz=izs,ize
    do iy=ub+1,my
      f(:,iy,iz) = -f(:,ub+1-(iy-ub),iz)
    end do
    end do
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE antisymmetric_face(f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f

  call antisymmetric_face_lower(f)
  call antisymmetric_face_upper(f)
END

!-----------------------------------------------------------------------
SUBROUTINE derivative_face_lower (f, df)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f, df
  integer:: iy,iz
  logical omp_in_parallel

  if (mpi_y == 0) then
    do iz=izs,ize
    do iy=1,lb
      f(:,iy,iz) = f(:,lb+1,iz) - df(:,lb,iz)*dym(lb)*(lb+1-iy)
    end do
    end do
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE derivative_face_upper (f, df)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f, df
  integer:: iy,iz
  logical omp_in_parallel

  if (mpi_y == mpi_ny-1) then
    do iz=izs,ize
    do iy=ub+1,my
      f(:,iy,iz) = f(:,ub,iz) + df(:,ub,iz)*dym(ub)*(iy-ub)
    end do
    end do
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE derivative_face (f, df)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f, df

  call derivative_face_lower (f, df)
  call derivative_face_upper (f, df)
END

!-----------------------------------------------------------------------
SUBROUTINE derivative_center1_lower (f, df)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
  real df
  integer:: iy,iz

  if (mpi_y == 0) then
    do iz=izs,ize
    do iy=1,lb-1
      f(:,iy,iz) = df*(ym(iy)-ym(lb+(lb-iy))) + f(:,lb+(lb-iy),iz) 
    end do
    end do
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE derivative_center1_upper (f, df)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
  real df
  integer:: iy,iz

  if (mpi_y == mpi_ny-1) then
    do iz=izs,ize
    do iy=ub+1,my
      f(:,iy,iz) = df*(ym(iy)-ym(ub+(ub-iy))) + f(:,ub+(ub-iy),iz) 
    end do
    end do
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE derivative_center1 (f, df)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
  real df

  call derivative_center1_lower (f, df)
  call derivative_center1_upper (f, df)
END

!-----------------------------------------------------------------------
SUBROUTINE extrapolate_center_lower(f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
  integer:: iy,iz
  logical omp_in_parallel

  if (mpi_y == 0) then
    do iz=izs,ize
    do iy=1,lb-1
      f(:,iy,iz) = (f(:,lb,iz)-f(:,lb+(lb-iy),iz))+f(:,lb,iz)
    end do
    end do
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE extrapolate_center_upper(f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
  integer:: iy,iz
  logical omp_in_parallel

  if (mpi_y == mpi_ny-1) then
    do iz=izs,ize
    do iy=ub+1,my
      f(:,iy,iz) = (f(:,ub,iz)-f(:,ub+(ub-iy),iz))+f(:,ub,iz)
    end do
    end do
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE extrapolate_center(f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f

  call extrapolate_center_lower(f)
  call extrapolate_center_upper(f)
END

!-----------------------------------------------------------------------
SUBROUTINE extrapolate_constrained_center_lower(f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
  integer:: iy,iz
  logical omp_in_parallel
  real p

  if (mpi_y == 0) then
    do iz=izs,ize
    do iy=1,lb-1
      p = 1.-((lb-iy)/(lb-3.))**2
      f(:,iy,iz) = p*(f(:,lb,iz)-f(:,lb+(lb-iy),iz))+f(:,lb,iz)
    end do
    end do
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE extrapolate_constrained_center_upper(f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
  integer:: iy,iz
  logical omp_in_parallel
  real p

  if (mpi_y == mpi_ny-1) then
    do iz=izs,ize
    do iy=ub+1,my
      p = 1.-((ub-iy)/real(my-ub-2))**2
      f(:,iy,iz) = p*(f(:,ub,iz)-f(:,ub+(ub-iy),iz))+f(:,ub,iz)
    end do
    end do
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE extrapolate_constrained_center(f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f

  call extrapolate_constrained_center_lower(f)
  call extrapolate_constrained_center_upper(f)
END

!-----------------------------------------------------------------------
SUBROUTINE extrapolate_center_log_lower(f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
  integer:: iy,iz
  logical omp_in_parallel

  if (mpi_y == 0) then
    do iz=izs,ize
    do iy=1,lb-1
      f(:,iy,iz) = exp(2.*alog(f(:,lb,iz))-alog(f(:,lb+(lb-iy),iz)))
    end do
    end do
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE extrapolate_center_log_upper(f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
  integer:: iy,iz
  logical omp_in_parallel

  if (mpi_y == mpi_ny-1) then
    do iz=izs,ize
    do iy=ub+1,my
      f(:,iy,iz) = exp(2.*alog(f(:,ub,iz))-alog(f(:,ub-(iy-ub),iz)))
    end do
    end do
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE extrapolate_center_log(f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f

  call extrapolate_center_log_lower(f)
  call extrapolate_center_log_upper(f)
END

!-----------------------------------------------------------------------
SUBROUTINE vanishing_center_lower(f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
  integer:: iy,iz
  logical omp_in_parallel

                                   !if (do_trace) print *,'van_c_l:', omp_mythread
  if (mpi_y == 0) then
    do iz=izs,ize
    do iy=1,lb-1
      f(:,iy,iz) = iy/float(lb)*f(:,lb,iz)
    end do
    end do
                                   !if (do_trace) print *,'van_c_l:', omp_mythread
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE vanishing_face_lower(f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
  integer:: iy,iz
  logical omp_in_parallel

                                   !if (do_trace) print *,'van_c_l:', omp_mythread
  if (mpi_y == 0) then
    do iz=izs,ize
    do iy=1,lb
      f(:,iy,iz) = (iy-1)/float(lb)*f(:,lb+1,iz)
    end do
    end do
                                   !if (do_trace) print *,'van_c_l:', omp_mythread
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE vanishing_center_upper(f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
  integer:: iy,iz
  logical omp_in_parallel

                                   !if (do_trace) print *,'van_c_u:', omp_mythread
  if (mpi_y == mpi_ny-1) then
    do iz=izs,ize
    do iy=ub+1,my
      f(:,iy,iz) = (my+1-iy)/float(my+1-ub)*f(:,ub,iz)
    end do
    end do
                                   !if (do_trace) print *,'van_c_u:', omp_mythread
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE vanishing_center(f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f

  call vanishing_center_lower(f)
  call vanishing_center_upper(f)
END

!-----------------------------------------------------------------------
SUBROUTINE mixed_center_lower(f,a)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
  real a
  integer:: iy,iz
  logical omp_in_parallel

  if (mpi_y == 0) then
    do iz=izs,ize
    do iy=1,lb-1
      f(:,iy,iz) = (1.-(lb-iy)/a)*f(:,lb,iz)
    end do
    end do
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE mixed_center_upper(f,a)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
  real a
  integer:: iy,iz
  logical omp_in_parallel

  if (mpi_y == mpi_ny-1) then
    do iz=izs,ize
    do iy=ub+1,my
      f(:,iy,iz) = (1.-(iy-ub)/a)*f(:,ub,iz)
    end do
    end do
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE mixed_center(f,fl)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
  real fl

  call mixed_center_lower(f,fl)
  call mixed_center_upper(f,fl)
END

!-----------------------------------------------------------------------
SUBROUTINE mixed_center_value_lower(f,a,fbdry)
!
!  Let the value in the ghost zones tend gradually towards fbdry, with
!  an exponential weight factor measured in grid zones (not physical 
!  length units)
!
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
  integer:: iy,iz
  logical omp_in_parallel
  real fbdry, a, p

  if (mpi_y == 0) then
    do iz=izs,ize
    do iy=1,lb-1
      p = exp(-(lb-iy)/a)
      f(:,iy,iz) = p*f(:,lb,iz) + (1.-p)*fbdry
    end do
    end do
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE mixed_center_value_upper(f,a,fbdry)
!
!  Let the value in the ghost zones tend gradually towards fbdry, with
!  an exponential weight factor measured in grid zones (not physical 
!  length units)
!
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
  integer:: iy,iz
  logical omp_in_parallel
  real fbdry, a, p

  if (mpi_y == mpi_ny-1) then
    do iz=izs,ize
    do iy=ub+1,my
      p = exp(-(iy-ub)/a)
      f(:,iy,iz) = p*f(:,ub,iz) + (1.-p)*fbdry
    end do
    end do
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE mixed_center_value(f,a,fbdry)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
  real a, fbdry

  call mixed_center_value_lower(f,a,fbdry)
  call mixed_center_value_upper(f,a,fbdry)
END

!-----------------------------------------------------------------------
SUBROUTINE extrapolate_constrained_face_lower(f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
  real:: flb 
  integer:: ix,iy,iz
  logical omp_in_parallel
  real p

  if (mpi_y == 0) then
    do iz=izs,ize
    do ix=1,mx
      flb = 2./8.*(15.*f(ix,lb+1,iz)-10.*f(ix,lb+2,iz)+3.*f(ix,lb+3,iz))
      do iy=1,lb
        p = 1.-((lb+1.-iy)/real(lb-2))**2
        f(ix,iy,iz) = p*(flb-f(ix,lb+1+(lb-iy),iz))
      end do
    end do
    end do
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE extrapolate_constrained_face_upper(f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
  real fub
  integer:: ix,iy,iz
  logical omp_in_parallel
  real p

  if (mpi_y == mpi_ny-1) then
    do iz=izs,ize
    do ix=1,mx
      fub = 2./8.*(15.*f(ix,ub  ,iz)-10.*f(ix,ub-1,iz)+3.*f(ix,ub-2,iz))
      do iy=ub+1,my
	p = 1.-((iy-ub)/real(my-ub-2))**2
        f(ix,iy,iz) = p*(fub-f(ix,ub+1-(iy-ub),iz))
      end do
    end do
    end do
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE extrapolate_face_constrained(f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f

  call extrapolate_constrained_face_lower(f)
  call extrapolate_constrained_face_upper(f)
END

!-----------------------------------------------------------------------
SUBROUTINE extrapolate_face_lower(f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
  real:: flb 
  integer:: ix,iy,iz
  logical omp_in_parallel

  if (mpi_y == 0) then
    do iz=izs,ize
    do ix=1,mx
      flb = 2./8.*(15.*f(ix,lb+1,iz)-10.*f(ix,lb+2,iz)+3.*f(ix,lb+3,iz))
      do iy=1,lb
        f(ix,iy,iz) = flb-f(ix,lb+1+(lb-iy),iz)
      end do
    end do
    end do
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE extrapolate_face_upper(f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
  real fub
  integer:: ix,iy,iz
  logical omp_in_parallel

  if (mpi_y == mpi_ny-1) then
    do iz=izs,ize
    do ix=1,mx
      fub = 2./8.*(15.*f(ix,ub  ,iz)-10.*f(ix,ub-1,iz)+3.*f(ix,ub-2,iz))
      do iy=ub+1,my
        f(ix,iy,iz) = fub-f(ix,ub+1-(iy-ub),iz)
      end do
    end do
    end do
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE extrapolate_face(f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f

  call extrapolate_face_lower(f)
  call extrapolate_face_upper(f)
END

!-----------------------------------------------------------------------
SUBROUTINE extrapolate_face_value_lower(f,flb)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
  real, dimension(mx,mz):: flb
  integer:: ix,iy,iz
  logical omp_in_parallel

  if (mpi_y == 0) then
    do iz=izs,ize
    do ix=1,mx
      do iy=1,lb
        f(ix,iy,iz) = 2.*flb(ix,iz)-f(ix,lb+1+(lb-iy),iz)
      end do
    end do
    end do
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE extrapolate_face_value_upper(f,fub)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
  real, dimension(mx,mz):: fub
  integer:: ix,iy,iz
  logical omp_in_parallel

  if (mpi_y == mpi_ny-1) then
    do iz=izs,ize
    do ix=1,mx
      do iy=ub+1,my
        f(ix,iy,iz) = 2.*fub(ix,iz)-f(ix,ub-(iy-ub),iz)
      end do
    end do
    end do
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE extrapolate_value_face(f,flb,fub)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
  real, dimension(mx,mz):: flb,fub

  call extrapolate_face_value_lower(f,flb)
  call extrapolate_face_value_upper(f,fub)
END


!-----------------------------------------------------------------------
SUBROUTINE value_face_lower(f,fl)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
  real, dimension(mx,mz):: fl
  integer:: iy,iz
  logical omp_in_parallel

  if (mpi_y == 0) then
    do iz=izs,ize
    do iy=1,lb
      f(:,iy,iz) = fl(:,iz) + (lb+0.5-iy)*2.*(fl(:,iz)-f(:,lb+1,iz))
    end do
    end do
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE value_face_upper(f,fu)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
  real, dimension(mx,mz):: fu
  integer:: iy,iz
  logical omp_in_parallel

  if (mpi_y == mpi_ny-1) then
    do iz=izs,ize
    do iy=ub+1,my
      f(:,iy,iz) = fu(:,iz) + (iy-ub-0.5)*2.*(fu(:,iz)-f(:,ub,iz))
    end do
    end do
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE value_face(f,fl,fu)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
  real, dimension(mx,mz):: fl,fu

  call value_face_lower(f,fl)
  call value_face_upper(f,fu)
END

!-----------------------------------------------------------------------
SUBROUTINE value_scalar_lower(f,fl)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
  real:: fl
  integer:: iy,iz
  logical omp_in_parallel

  if (mpi_y == 0) then
    do iz=izs,ize
    do iy=1,lb-1
      f(:,iy,iz) = f(:,lb,iz) + (lb-iy)*(fl-f(:,lb,iz))/(lb-1.)
    end do
    end do
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE boundary_test
  USE params
  implicit none
  real, allocatable, dimension(:,:,:):: f
  integer iy

  mx=15
  my=mx
  mz=mx
  lb=6
  ub=my-5
  allocate (f(mx,my,mz))

  do iy=1,my
    f(:,iy,:)=cos((iy-lb)*2.*pi/(ub-lb))
  end do

  call symmetric_center(f)
  print '(a/15f8.4)','s_c',f(1,:,1)  
  call antisymmetric_center(f)
  print '(a/15f8.4)','a_c',f(1,:,1)  
  call symmetric_face(f)
  print '(a/15f8.4)','s_f',f(1,:,1)  
  call antisymmetric_face(f)
  print '(a/15f8.4)','a_f',f(1,:,1)
  
END
