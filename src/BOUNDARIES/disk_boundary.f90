! $Id: disk_boundary.f90,v 1.5 2004/07/22 13:56:43 aake Exp $

!-----------------------------------------------------------------------
SUBROUTINE init_boundary
  USE params
  implicit none
  lb = 1
  ub = my
!
END

!-----------------------------------------------------------------------
SUBROUTINE read_boundary
  USE params
  implicit none
!
END

!-----------------------------------------------------------------------
SUBROUTINE density_boundary (r)
  USE params
  implicit none
  real, dimension(mx,my,mz):: r
!
END

!-----------------------------------------------------------------------
SUBROUTINE density_boundary_log (lnr,r)
  USE params
  implicit none
  real, dimension(mx,my,mz):: lnr,r
!
END

!-----------------------------------------------------------------------
SUBROUTINE energy_boundary (r,lnr,Ux,Uy,Uz,e,ee,p,Bx,By,Bz)
  USE params
  implicit none
  real, dimension(mx,my,mz):: r,lnr,Ux,Uy,Uz,e,ee,p,Bx,By,Bz

END

!-----------------------------------------------------------------------
SUBROUTINE passive_boundary (r,Ux,Uy,Uz,d,dd)
  USE params
  implicit none
  real, dimension(mx,my,mz):: r,Ux,Uy,Uz,d,dd

END

!-----------------------------------------------------------------------
SUBROUTINE velocity_boundary (r,xdnr,ydnr,zdnr,Ux,Uy,Uz,px,py,pz,e,Bx,By,Bz)
  USE params
  implicit none
  real, dimension(mx,my,mz):: r,xdnr,ydnr,zdnr,Ux,Uy,Uz,px,py,pz,e,ee,Bx,By,Bz
!
END

!-----------------------------------------------------------------------
SUBROUTINE mfield_boundary (Ux,Uy,Uz,Bx,By,Bz)
  USE params
  implicit none
  real, dimension(mx,my,mz):: Ux,Uy,Uz,Bx,By,Bz
!
END

!-----------------------------------------------------------------------
SUBROUTINE efield_boundary (Ex, Ey, Ez, Bx_y, Bx_z, By_z, By_x, Bz_x, Bz_y, &
                                        Ux_y, Ux_z, Uy_z, Uy_x, Uz_x, Uz_y)
  USE params
  implicit none
  real, dimension(mx,my,mz):: Ex, Ey, Ez, Bx_y, Bx_z, By_z, By_x, Bz_x, Bz_y, &
                                          Ux_y, Ux_z, Uy_z, Uy_x, Uz_x, Uz_y
!
END

!-----------------------------------------------------------------------
SUBROUTINE ddt_boundary (r,px,py,pz,e,Bx,By,Bz, &
                         drdt,dpxdt,dpydt,dpzdt, &
                         dedt,dBxdt,dBydt,dBzdt)
  USE params
  USE forcing
  implicit none
  real, dimension(mx,my,mz):: r,px,py,pz,e,Bx,By,Bz, &
                              drdt,dpxdt,dpydt,dpzdt, &
                              dedt,dBxdt,dBydt,dBzdt
  real csink,dv,drdt1
  integer ix,iy,iz

  dv = dx*dy*dz                                                         ! volume element
  csink = 3.*Cu/dt                                                      ! rate constant = 3*(cs+max(u))/ds
  rho_max = max(0.5*maxval(r),rho_min)                                  ! minimum accretion density
  do iz=1,mz
  do iy=1,my
  do ix=1,mx
    drdt1 = max(r(ix,iy,iz)-rho_max,0.0)*csink                          ! fair fraction
    drdt(ix,iy,iz) = drdt(ix,iy,iz) - drdt1                             ! remove from rho
    m_sun = m_sun + dt*drdt1*(dv/3.)                                    ! add to m_sun
  end do
  end do
  end do
  if (isubstep .eq. 1) print *,'ddt_bdry:',maxval(r),rho_max,m_sun
!
END

!-----------------------------------------------------------------------
SUBROUTINE regularize (f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
!
END
