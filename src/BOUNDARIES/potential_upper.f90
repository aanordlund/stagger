!  $Id: potential_upper.f90,v 1.3 2003/09/12 17:49:07 aake Exp $
!----------------------------------------------------------------------
SUBROUTINE potential_upper (bx, by, bz, dx, dy, dz, mx, my, mz, ub)
!
!  Potential field extrapolation into the ghost zones.
!
!  A potential field, periodic in x and z, is exponentially
!  decreasing in the direction away from the boundary, with
!  amplitude factors exp(-y*k_xz) for each Fourier component.
!
!  Here we assume that 
!    Bx(i,j,iz) is centered at (i-1/2,j,iz),
!    By(i,j,iz) is centered at (i,j-1/2,iz),
!    Bz(i,j,iz) is centered at (i,j,iz-1/2).
!
!  If Bx and Bz were integer centered they would be exactly
!  pi/4 out of phase horizontally relative to By.  Since they
!  are half zone centered, a zero point that should be at j
!  is really at j+0.5, so we need to add that phase factor.
!
!  The extrapolation is based on By(:,ub,:), which is centered 
!  at (ix,ub-1/2,iz).  The first layer above this is the one
!  with Bx(:,ub,:), which is centered at (ix,ub,iz). 
!
  implicit none
  integer:: ix, iy, iz, mx, my, mz, ub
  real:: dx, dy, dz, pi, asin, acos, kx(mx), kz(mz)
  real, dimension(mx,mz):: fy, fh, kh, byt, bt, bf
  real, dimension(mx,my,mz):: bx, by, bz

!  Horizontal Fourier transform of the vertical boundary field

  bf = by(:,ub,:)
  call fft2df (bf, byt, mx, mz)

!  Potential extrapolation

  pi = 4.*atan(1.)
  do iy=ub,my

!  Y component

    do iz=1,mz
      kz(iz) = 2.*pi/(dz*mz)*(iz/2)
      do ix=1,mx
        kx(ix)   = 2.*pi/(dx*mx)*(ix/2)
        kh(ix,iz) = sqrt(kx(ix)**2+kz(iz)**2) + 1e-30
        bt(ix,iz) = byt(ix,iz)*exp(-dy*(iy-ub)*kh(ix,iz))
      end do
    end do
    if (iy .gt. ub) then
      call fft2db (bt, bf, mx, mz)
      do iz=1,mz
        do ix=1,mx
          by(ix,iy,iz) = bf(ix,iz)
        end do
      end do
    end if

!  X component

    do iz=1,mz
      bt(1 ,iz) = 0.
      bt(mx,iz) = 0.
      fh(1 ,iz) = exp(-dy*(iy-ub+0.5)*kh(1 ,iz))/kh(1 ,iz)
      fh(mx,iz) = exp(-dy*(iy-ub+0.5)*kh(mx,iz))/kh(mx,iz)
      do ix=3,mx,2
        acos = cos(0.5*kx(ix)*dx)
        asin = sin(0.5*kx(ix)*dx)
        fh(ix-1,iz) =  exp(-dy*(iy-ub+0.5)*kh(ix-1,iz))/kh(ix-1,iz)
        fh(ix  ,iz) =  exp(-dy*(iy-ub+0.5)*kh(ix  ,iz))/kh(ix  ,iz)
        bt(ix-1,iz) =  kx(ix)*fh(ix,iz)*(acos*byt(ix  ,iz) - asin*byt(ix-1,iz))
        bt(ix  ,iz) = -kx(ix)*fh(ix,iz)*(acos*byt(ix-1,iz) + asin*byt(ix  ,iz))
      end do
    end do
    call fft2db (bt, bf, mx, mz)
    do iz=1,mz
      do ix=1,mx
        bx(ix,iy,iz) = bf(ix,iz)
      end do
    end do

!  Z component

    acos = cos(2.*pi*0.5/mz)
    asin = sin(2.*pi*0.5/mz) 
    do ix=1,mx
      bt(ix, 1) = 0.
      bt(ix,mz) = 0.
      do iz=3,mz,2
        acos = cos(0.5*kz(iz)*dz)
        asin = sin(0.5*kz(iz)*dz)
        bt(ix,iz-1) =  kz(iz)*fh(ix,iz)*(acos*byt(ix,  iz) - asin*byt(ix,iz-1))
        bt(ix,iz  ) = -kz(iz)*fh(ix,iz)*(acos*byt(ix,iz-1) + asin*byt(ix,iz  ))
      end do
    end do
    call fft2db (bt, bf, mx, mz)
    do iz=1,mz
      do ix=1,mx
        bz(ix,iy,iz) = bf(ix,iz)
      end do
    end do

  end do
END

