! $Id: lightbridge.f90,v 1.7 2007/04/09 13:16:23 aake Exp $

MODULE boundary
  real t_Bbdry, Bscale, lb_eampl, t_bdry
  real rbot, pbot, ebot, eetop
  logical debug
END module

!-----------------------------------------------------------------------
SUBROUTINE init_boundary
  USE params
  USE boundary
  implicit none

  character(len=mid):: id='$Id: lightbridge.f90,v 1.7 2007/04/09 13:16:23 aake Exp $'
  if (id.ne.' ') print *,id; id=' '

  t_bdry = 0.01
  t_Bbdry  = 1                                  ! bdry decay time for B
  Bscale = sy*10./my
  lb = 6
  ub = my-5
  if (mpi_y > 0) lb = 1
  if (mpi_y < mpi_ny-1) ub = my
  lb_eampl = 0.
  rbot = -1.
  debug = .false.
  call read_boundary
END

!-----------------------------------------------------------------------
SUBROUTINE read_boundary
  USE params
  USE boundary
  implicit none
  namelist /bdry/ lb, ub, rbot, ebot, pbot, t_bdry, t_Bbdry, Bscale, lb_eampl, debug

  if (stdin.ge.0) then
    rewind (stdin); read (stdin,bdry)
  else
    read (*,bdry)
  end if
  if (master) write (*,bdry)
END

!-----------------------------------------------------------------------
SUBROUTINE density_boundary (r)
  USE params
  implicit none
  real, dimension(mx,my,mz):: r
!
!  Version for linear cases
!
  call extrapolate_center(r)
END

!-----------------------------------------------------------------------
SUBROUTINE density_boundary_log (r,lnr)
  USE params
  implicit none
  real, dimension(mx,my,mz):: r,lnr
  integer iy,iz
  real hrhomax
!
!  Density boundary condition for open boundaries -- extrapolate in the log.
!  Also, prevent the density from flipping over to drive input.
!
  call extrapolate_center(lnr)
  hrhomax=sy/10.
!$omp parallel do private(iz,iy)
  do iz=1,mz
    do iy=lb-1,1,-1
      lnr(:,iy,iz)=min(lnr(:,iy,iz),lnr(:,iy+1,iz)*(ym(iy+1)-ym(iy))/hrhomax)
      r(:,iy,iz)=exp(lnr(:,iy,iz))
    end do
    do iy=ub+1,my
      r(:,iy,iz)=exp(lnr(:,iy,iz))
    end do
  end do
END

!-----------------------------------------------------------------------
SUBROUTINE energy_boundary (r,lnr,Ux,Uy,Uz,e,ee,p,Bx,By,Bz)
  USE params
  USE eos
  USE boundary
  implicit none
  real, dimension(mx,my,mz):: r,lnr,Ux,Uy,Uz,e,ee,p,Bx,By,Bz
  real s, pb, pbmax, efact
  integer ix, iy, iz

!!!$omp parallel do private(iz)
!!  call eos1(mx,mz,r(:,ub,:),r(:,ub,:),p=p(:,ub,:))

  if (rbot .lt. 0.) then                                                ! first time
    call extrapolate_center_upper(ee)                                   ! extrapolate at bottom
    call constant_center_lower(ee)                                      ! vanishing deriv at top
    call pressure (r,ee,p)                                              ! single call
    rbot=sum(r(:,ub,:))/(mx*mz)                                         ! save density
    ebot=sum(e(:,ub,:))/(mx*mz)                                         ! save density
    pbot=sum(p(:,ub,:))/(mx*mz)                                         ! save pressure
    if (debug) print *,'ub, rbot, ebot =', ub, rbot, ebot
  end if
  eetop = sum(e(:,lb,:)/r(:,lb,:))/(mx*mz)

  if (do_mhd) then
    pbmax = 0.
!$omp parallel do private(iz,ix), reduction(max:pbmax)
    do iz=1,mz
      do ix=1,mx
        pbmax = max(pbmax,0.5*by(ix,ub,iz)**2)
      end do
    end do
  else
    pbmax = 0.
  end if

!$omp parallel do private(ix,iz,pb,efact)
  do iz=1,mz
    do ix=1,mx
      if (lb_eampl .ne. 0) then
        efact = 1.+lb_eampl*(cos((ix-97)*2.*pi/mx)+cos((iz-91)*2.*pi/mz))
        e(ix,ub,iz) = e(ix,ub,iz)*efact
        r(ix,ub,iz) = r(ix,ub,iz)*efact**(1./gamma)
      end if
      ee(ix,ub,iz) = e(ix,ub,iz)/r(ix,ub,iz)                            ! redo energy per unit mass
      ee(ix,lb,iz) = eetop						! constant energy per unit mass
      e(ix,lb,iz) = eetop*r(ix,lb,iz)                                   ! smooth top temperature
    end do
  end do
  call extrapolate_center_upper(ee)                                     ! extrapolate at bottom
  call constant_center_lower(ee)                                        ! vanishing deriv at top
  if (do_loginterp) then
    call density_boundary_log (r,lnr)                                   ! readjust density
  else
    call density_boundary(r)                                            ! readjust density
  end if

!$omp parallel do private(iz,iy)
  do iz=1,mz
    do iy=1,lb-1
      e(:,iy,iz)=r(:,iy,iz)*ee(:,iy,iz)
    end do
    do iy=ub+1,my
      e(:,iy,iz)=r(:,iy,iz)*ee(:,iy,iz)
    end do
  end do
END

!-----------------------------------------------------------------------
SUBROUTINE passive_boundary (r,Ux,Uy,Uz,d,dd)
  USE params
  implicit none
  real, dimension(mx,my,mz):: r,Ux,Uy,Uz,d,dd
  integer iy, iz

  call symmetric_center(dd)
!$omp parallel do private(iz,iy)
  do iz=1,mz
    do iy=1,lb-1
      d(:,iy,iz)=r(:,iy,iz)*dd(:,iy,iz)
    end do
    do iy=ub+1,my
      d(:,iy,iz)=r(:,iy,iz)*dd(:,iy,iz)
    end do
  end do
END

!-----------------------------------------------------------------------
SUBROUTINE velocity_boundary (r,xdnr,ydnr,zdnr,Ux,Uy,Uz,px,py,pz,e,Bx,By,Bz)
  USE params
  USE boundary
  implicit none
  real, dimension(mx,my,mz):: r,xdnr,ydnr,zdnr,Ux,Uy,Uz,px,py,pz,e,ee,Bx,By,Bz
  real p, uyin, uytot
  real(kind=8) rin, rtot, fmout, fmtot
  integer ix, iy, iz
!
!  Bottom boundary
!
  rin  = 1e-30
  rtot = 0.
  fmout = 0.
  fmtot = 0.
!$omp parallel do private(iz,ix), reduction(+:fmtot,rin,rtot,fmout)
  do iz=1,mz
    do ix=1,mx
      fmtot = fmtot +    py(ix,ub,iz)                   ! mass flux
      rtot  = rtot  +  ydnr(ix,ub,iz)                   ! mass
      if (py(ix,ub,iz) .ge. 0.) then                    ! outgoing
        fmout = fmout +   py(ix,ub,iz)                  ! mass flux
      else                                              ! incoming
        rin   = rin   + ydnr(ix,ub,iz)                  ! mass
      end if
    end do
  end do
  uytot = fmtot/rtot
  uyin = -fmout/rin                                     ! target incoming velocity
  if (debug) print *,'uyin, uytot =', uyin, uytot

  call symmetric_center_upper (Ux)
  call symmetric_face_upper   (Uy)
  call symmetric_center_upper (Uz)
!
!  Top boundary, vanishing values
!
  call vanishing_center_lower (Ux)
  call symmetric_face_lower   (Uy)
  call vanishing_center_lower (Uz)
!
!  Gradually vanishing vertical mass flux in upper ghost zone
!
!$omp parallel do private(iz,iy)
  do iz=1,mz
    do iy=1,lb
      py(:,iy,iz)=iy/real(lb+1)*py(:,lb+1,iz)
    end do
  end do  
!
!  Recompute mass fluxes
!
!$omp parallel do private(iz,iy)
  do iz=1,mz
    do iy=1,lb
      px(:,iy,iz)=xdnr(:,iy,iz)*Ux(:,iy,iz)
      py(:,iy,iz)=ydnr(:,iy,iz)*Uy(:,iy,iz)
      pz(:,iy,iz)=zdnr(:,iy,iz)*Uz(:,iy,iz)
    end do
    do iy=ub,my
      px(:,iy,iz)=xdnr(:,iy,iz)*Ux(:,iy,iz)
      py(:,iy,iz)=ydnr(:,iy,iz)*Uy(:,iy,iz)
      pz(:,iy,iz)=zdnr(:,iy,iz)*Uz(:,iy,iz)
    end do
  end do
END

!-----------------------------------------------------------------------
SUBROUTINE mfield_boundary (Ux,Uy,Uz,Bx,By,Bz)
  USE params
  USE boundary
  implicit none
  real, dimension(mx,my,mz):: Ux,Uy,Uz,Bx,By,Bz
!
!  Require the horizontal field components to vanish
!
!  call extrapolate_center_lower(Bx)             ! top boundary:
!  call extrapolate_center_lower(Bz)             ! extrapolate
!  call antisymmetric_center_upper(Bx)           ! bottom boundary:
!  call antisymmetric_center_upper(Bz)           ! vanishing horizontal

  call mixed_center(Bx,Bscale)
  call mixed_center(Bz,Bscale)
  call symmetric_face(By)   ! TEMPORARY HACK: to be replaced

!  call extrapolate_center_lower(Bx)             ! top boundary:
!  call extrapolate_center_lower(Bz)             ! extrapolate
!  call vanishing_center_upper(Bx)               ! bottom boundary
!  call vanishing_center_upper(Bz)               ! bottom boundary
!  call symmetric_face(By)   ! TEMPORARY HACK: to be replaced

END

!-----------------------------------------------------------------------
SUBROUTINE efield_boundary (Ex, Ey, Ez, Bx_y, Bx_z, By_z, By_x, Bz_x, Bz_y, &
                            Rx, Ry, Rz, Ux_y, Ux_z, Uy_z, Uy_x, Uz_x, Uz_y, &
                            Ux, Uy, Uz)
  USE params
  USE boundary
  implicit none
  real, dimension(mx,my,mz):: Ex, Ey, Ez, Bx_y, Bx_z, By_z, By_x, Bz_x, Bz_y, &
                              Ux_y, Ux_z, Uy_z, Uy_x, Uz_x, Uz_y, &
                              Ux, Uy, Uz
  real, dimension(mx,2,mz):: Rx, Ry, Rz
!
!  Allow the top B-field to evolve, bottom B-field -> zero
!
  call extrapolate_center_lower (Ex)                  ! allow extrap
  call extrapolate_center_lower (Ez)                  ! allow extrap
  call derivative_face_upper (Ex, -Bz_y*(1./t_Bbdry)) ! bottom boundary
  call derivative_face_upper (Ez, +Bx_y*(1./t_Bbdry)) ! damp out horizontal B
END

!-----------------------------------------------------------------------
SUBROUTINE regularize (f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
!
  call symmetric_center (f)
END

!-----------------------------------------------------------------------
SUBROUTINE regularize_face (f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
!
  call symmetric_face (f)
END

!-----------------------------------------------------------------------
SUBROUTINE ddt_boundary (r,px,py,pz,e,Bx,By,Bz, &
                         drdt,dpxdt,dpydt,dpzdt, &
                         dedt,dBxdt,dBydt,dBzdt)
  USE params
  USE eos
  USE boundary
  implicit none
  real, dimension(mx,my,mz):: r,px,py,pz,e,Bx,By,Bz, &
                              drdt,dpxdt,dpydt,dpzdt, &
                              dedt,dBxdt,dBydt,dBzdt
  integer ix,iz,ixp1,izp1
  real(kind=8) rub,pyub,elb,eub,ct,pub,dpydtub,scr,pb
  real(kind=8) rubx,pyubx,eubx,pubx,dpydtubx
!
  rub = 0.
  eub = 0.
  pub = 0.
  pyub = 0.
  dpydtub = 0.
!$omp parallel do private(iz,ix,rubx,pyubx,eubx,pubx,dpydtubx), reduction(+:rub,eub,pub,pyub,dpydtub)
  do iz=1,mz
    rubx = 0.
    eubx = 0.
    pubx = 0.
    pyubx = 0.
    dpydtubx = 0.
    do ix=1,mx
      rubx = rubx + r(ix,ub,iz)
      eubx = eubx + e(ix,ub,iz)
      pubx = pubx + pbot0(ix,iz)
      pyubx = pyubx + 1.5*py(ix,ub,iz)-0.5*py(ix,ub-1,iz)					        ! py is regular(?)
      dpydtubx = dpydtubx + 1.5*dpydt(ix,ub,iz)-0.5*dpydt(ix,ub-1,iz)				! extrapolate to bdry
    end do
    rub = rub + rubx
    eub = eub + eubx
    pub = pub + pubx
    pyub = pyub + pyubx
    dpydtub = dpydtub + dpydtubx
  end do
  rub = rub/(mx*mz)
  eub = eub/(mx*mz)
  pub = pub/(mx*mz)
  pyub = pyub/(mx*mz)
  dpydtub = dpydtub/(mx*mz)
  if (debug) then
    print*,'rub',rub
    print*,'eub',eub
    print*,'pub',pub
    print*,'pyub',pyub
    print*,'dpydtub',dpydtub
  end if

  ct = 0.5/t_bdry										! damping constant
  if (debug) print*,'ct',ct
!$omp parallel do private(iz,izp1,ix,ixp1,scr,pb)
  do iz=1,mz
    izp1 = mod(iz,mz)+1
    do ix=1,mx
      ixp1 = mod(ix,mx)+1
      pb = 0.125*((bx(ix,ub,iz)+bx(ixp1,ub  ,iz  ))**2 + &
                  (by(ix,ub,iz)+by(ix  ,ub+1,iz  ))**2 + &
                  (bz(ix,ub,iz)+bz(ix  ,ub  ,izp1))**2)
      dpydt(ix,ub,iz) = dpydt(ix,ub,iz) - (dpydtub + pyub*ct)/1.5                               ! damp out average mass flux
      dedt(ix,ub,iz) = -(pbot0(ix,iz)+pb-pub)*(e(ix,ub,iz)+pbot0(ix,iz))/(gamma*pbot0(ix,iz))*ct ! cancel pressure fluctuation
      drdt(ix,ub,iz) = -(pbot0(ix,iz)+pb-pub)*r(ix,ub,iz)/(gamma*pbot0(ix,iz))*ct               ! at constant entropy
      if (py(ix,ub,iz).lt.0.) then								! for incoming flows..
	scr = ((e(ix,ub,iz)-ebot) - &
	       (e(ix,ub,iz)+pbot0(ix,iz))*(r(ix,ub,iz)-rbot)/r(ix,ub,iz))/(gamma*t_bdry)        ! prop to entropy fluctuation
	dedt(ix,ub,iz) = dedt(ix,ub,iz) - scr*(1.+e(ix,ub,iz)/r(ix,ub,iz)*dlnpdE_r(ix,iz))	! cancel entropy fluctuation
        drdt(ix,ub,iz) = drdt(ix,ub,iz) + scr*dlnpdE_r(ix,iz)					! at constant pressure
        dpxdt(ix,ub,iz) = -px(ix,ub,iz)*ct							! damp horizontal motions
        dpzdt(ix,ub,iz) = -pz(ix,ub,iz)*ct
      endif
    end do
  end do
END

