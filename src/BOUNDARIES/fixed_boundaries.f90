! $Id: fixed_boundaries.f90,v 1.2 2006/08/29 22:15:08 aake Exp $

MODULE boundary
  integer, parameter:: mbdry=6
  real, allocatable, dimension(:,:,:):: rb,pxb,pyb,pzb,eb,db,bxb,byb,bzb
  logical load_boundaries
  logical:: do_bondary=.false.
  logical:: do_eta=.false.
  real:: eta_scl=1.0
  integer:: eta_width=1
  integer:: isub=1
END MODULE boundary

!-----------------------------------------------------------------------
SUBROUTINE init_boundary
  USE params
  USE variables
  USE boundary
  implicit none
  character(len=mid):: id="$Id: fixed_boundaries.f90,v 1.2 2006/08/29 22:15:08 aake Exp $"
  call print_id (id)
  lb = 1
  ub = my
  load_boundaries=.true.
  allocate (rb(mx,my,mz),pxb(mx,my,mz),pyb(mx,my,mz),pzb(mx,my,mz))
  allocate (eb(mx,my,mz),bxb(mx,my,mz),byb(mx,my,mz),bzb(mx,my,mz))
  pxb=0.; pyb=0.; pzb=0.
!
END

!-----------------------------------------------------------------------
SUBROUTINE read_boundary
  USE params
  implicit none
!
END

!-----------------------------------------------------------------------
SUBROUTINE set_boundary (f,fb)
  USE params
  USE boundary
  implicit none
  real, dimension(mx,my,mz):: f, fb
  integer ix,iy,iz
!
  do iz=1,mz
  do iy=1,mbdry
  do ix=1,mx
    f(ix,iy,iz)  = fb(ix,iy,iz)
  end do
  end do
  end do
  do iz=1,mz
  do iy=my-mbdry+1,my
  do ix=1,mx
    f(ix,iy,iz)  = fb(ix,iy,iz)
  end do
  end do
  end do
!
END

!-----------------------------------------------------------------------
SUBROUTINE density_boundary (r,lnr,py,e)
  USE params
  USE boundary
  implicit none
  real, dimension (mx,my,mz):: r,lnr,py,e
  if (do_trace) print *,'density_boundary'

  if (it.eq.1.and.isub.eq.1) then
    rb=r
    if(master) print *,'rbl= ',rb(1,1:3,1)
    if(master) print *,'rbr= ',rb(1,my-mbdry:my,1)
    if (do_energy) then
      eb=e
      if(master) print *,'ebl= ',eb(1,1:3,1)
      if(master) print *,'ebr= ',eb(1,my-mbdry:my,1)
    end if
    if(master) print *,'pyl= ',pyb(1,1:3,1)
    if(master) print *,'pyr= ',pyb(1,my-mbdry:my,1)
    if(master) print *,'pxl= ',pxb(1,1:3,1)
    if(master) print *,'pxr= ',pxb(1,my-mbdry:my,1)
  end if
!
!
  call set_boundary(r,rb)
  load_boundaries=.false.
END

!-----------------------------------------------------------------------
SUBROUTINE density_boundary_log (r,lnr)
  USE params
  USE boundary
  implicit none
  real, dimension(mx,my,mz):: lnr,r
!
! if (load_boundaries) call copy_bdry(r,r,r,r)

  if (it.eq.1.and.isub.eq.1) then
    rb=r
    if(master) print *,'rbl= ',rb(1,1:3,1)
    if(master) print *,'rbr= ',rb(1,my-mbdry:my,1)
    if(master) print *,'pyl= ',pyb(1,1:3,1)
    if(master) print *,'pyr= ',pyb(1,my-mbdry:my,1)
    if(master) print *,'pxl= ',pxb(1,1:3,1)
    if(master) print *,'pxr= ',pxb(1,my-mbdry:my,1)
  end if

  call set_boundary(r,rb)
  lnr = alog(r)
END

!-----------------------------------------------------------------------
SUBROUTINE energy_boundary (r,lnr,Ux,Uy,Uz,e,ee,p,Bx,By,Bz)
  USE params
  USE boundary
  implicit none
  real, dimension(mx,my,mz):: r,lnr,Ux,Uy,Uz,e,ee,p,Bx,By,Bz
!
  call set_boundary(e,eb)
END

!-----------------------------------------------------------------------
SUBROUTINE passive_boundary (r,Ux,Uy,Uz,d,dd)
  USE params
  USE boundary
  implicit none
  real, dimension(mx,my,mz):: r,Ux,Uy,Uz,d,dd
!
  call set_boundary(d,db)
END

!-----------------------------------------------------------------------
SUBROUTINE velocity_boundary (r,xdnr,ydnr,zdnr,Ux,Uy,Uz,px,py,pz,e,Bx,By,Bz)
  USE params
  USE boundary
  implicit none
  real, dimension(mx,my,mz):: r,xdnr,ydnr,zdnr,Ux,Uy,Uz,px,py,pz,e,Bx,By,Bz
  if (do_trace) print *,'velocity_boundary'
!
! if (load_boundaries) call copy_bdry(r,px,py,pz)

  load_boundaries = .false.
  call set_boundary(px,pxb)
  call set_boundary(py,pyb)
  call set_boundary(pz,pzb)
END

!-----------------------------------------------------------------------
SUBROUTINE mfield_boundary (Ux,Uy,Uz,Bx,By,Bz)
  USE params
  USE boundary
  implicit none
  real, dimension(mx,my,mz):: Ux,Uy,Uz,Bx,By,Bz
  if (do_trace) print *,'mfield_boundary'
!
!$omp parallel private (isub)
  if (do_mhd) then
    if (it.eq.1.and.isub.eq.1) then
      Bxb = Bx
      Byb = By
      Bzb = Bz
      if(master) print *,'bxl= ',bxb(1,1:3,1)
      if(master) print *,'bxr= ',bxb(1,my-mbdry:my,1)
      isub=2
    end if
  end if
!$omp end parallel

  if (do_mhd) then
    call set_boundary(Bx,Bxb)
    call set_boundary(By,Byb)
    call set_boundary(Bz,Bzb)
  end if

END

!-----------------------------------------------------------------------
SUBROUTINE efield_boundary (Ex, Ey, Ez, Bx_y, Bx_z, By_z, By_x, Bz_x, Bz_y, &
                            Rx, Ry, Rz, Ux_y, Ux_z, Uy_z, Uy_x, Uz_x, Uz_y, &
                            Ux, Uy, Uz, Bxl, Byl, Bzl, Bxu, Byu, Bzu)
  USE params
  implicit none
  real, dimension(mx,my,mz):: Ex, Ey, Ez, Bx_y, Bx_z, By_z, By_x, Bz_x, Bz_y, &
                                          Ux_y, Ux_z, Uy_z, Uy_x, Uz_x, Uz_y, &
                                          Ux, Uy, Uz, Bxl, Byl, Bzl, Bxu, Byu, Bzu
  real, dimension(mx,2 ,mz):: Rx, Ry, Rz
!
END

!-----------------------------------------------------------------------
SUBROUTINE ecurrent_boundary (Ex, Ey, Ez, Jx, Jy, Jz)
  USE params
  implicit none
  real, dimension(mx,my,mz):: Ex, Ey, Ez, Jx, Jy, Jz
!
END

!-----------------------------------------------------------------------
SUBROUTINE magnetic_pressure_boundary(BB)
USE params
  implicit none
  real, dimension(mx,my,mz)::BB
!
END

!-----------------------------------------------------------------------
SUBROUTINE viscosity_boundary(nu,scr)
  USE params
  implicit none
  real, dimension(mx,my,mz):: nu, scr
!
END
!-----------------------------------------------------------------------
SUBROUTINE ddt_boundary (r,px,py,pz,e,Bx,By,Bz, &
                         drdt,dpxdt,dpydt,dpzdt, &
                         dedt,dBxdt,dBydt,dBzdt)
  USE params
  implicit none
  real, dimension(mx,my,mz):: r,px,py,pz,e,Bx,By,Bz, &
                              drdt,dpxdt,dpydt,dpzdt, &
                              dedt,dBxdt,dBydt,dBzdt
  real, allocatable, dimension(:,:,:):: scr
!
  allocate (scr(mx,my,mz))
  scr = 0.
  call set_boundary(drdt,scr)
  call set_boundary(dpxdt,scr)
  call set_boundary(dpydt,scr)
  call set_boundary(dpzdt,scr)
  call set_boundary(dedt,scr)
  call set_boundary(dbxdt,scr)
  call set_boundary(dbydt,scr)
  deallocate (scr)
END

!-----------------------------------------------------------------------
SUBROUTINE regularize (f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
!
END

!-----------------------------------------------------------------------
SUBROUTINE regularize_face (f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
!
END
