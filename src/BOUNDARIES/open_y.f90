! $Id: open_y.f90,v 1.6 2004/07/22 13:56:43 aake Exp $

!-----------------------------------------------------------------------
SUBROUTINE density_boundary (r)
  USE params
  implicit none
  real, dimension(mx,my,mz):: r
!
  lb = 6
  ub = my-5
  call extrapolate_center(r)
END

!-----------------------------------------------------------------------
SUBROUTINE density_boundary_log (r,lnr)
  USE params
  implicit none
  real, dimension(mx,my,mz):: r,lnr
  integer iy,iz
!
  lb = 6
  ub = my-5
  call extrapolate_center(lnr)
!$omp parallel do private(iz,iy)
  do iz=1,mz
    do iy=1,lb-1
      r(:,iy,iz)=exp(lnr(:,iy,iz))
    end do
  end do
END

!-----------------------------------------------------------------------
SUBROUTINE energy_boundary (r,Ux,Uy,Uz,e,ee,p,Bx,By,Bz)
  USE params
  implicit none
  real, dimension(mx,my,mz):: r,Ux,Uy,Uz,e,ee,p,Bx,By,Bz
  real, save:: plower=-1., slower
  real s
  integer ix, iz

  if (plower .lt. 0.) then                                              ! first time
    plower=sum(p(:,lb,:))/(mx*mz)                                       ! save pressure
    slower=sum(p(:,lb,:)/r(:,lb,:)**gamma)/(mx*mz)                      ! save entropy
    print *,'plower, slower =', lb, plower, slower
  end if

  call extrapolate_center(ee)                                           ! extrapolate
  e(:,1:lb-1 ,:) = ee(:,1:lb-1 ,:)*r(:,1:lb-1 ,:)                       ! internal energy
  e(:,ub+1:my,:) = ee(:,ub+1:my,:)*r(:,ub+1:my,:)                       ! internal energy

  do iz=1,mz
    do ix=1,mx
      e(ix,lb,iz) = e(ix,lb,iz)* plower/p(ix,lb,iz)                     ! adjust pressure at constant S
      r(ix,lb,iz) = r(ix,lb,iz)*(plower/p(ix,lb,iz))**(1./gamma) 
      if (Uy(ix,lb,iz) .gt. 0.) then                                    ! incoming
        s = (p(ix,lb,iz)/r(ix,lb,iz)**gamma)                            ! (function of) entropy
        r(ix,lb,iz) =  r(ix,lb,iz)*(s/slower)**(1./gamma)               ! adjust S at constant pressure
      end if
    end do
  end do
END

!-----------------------------------------------------------------------
SUBROUTINE passive_boundary (r,Ux,Uy,Uz,d,dd)
  USE params
  implicit none
  real, dimension(mx,my,mz):: r,Ux,Uy,Uz,d,dd

  call symmetric_center(dd)
  d = r*dd
END

!-----------------------------------------------------------------------
SUBROUTINE velocity_boundary (r,xdnr,ydnr,zdnr,Ux,Uy,Uz,px,py,pz,e,Bx,By,Bz)
  USE params
  implicit none
  real, dimension(mx,my,mz):: r,xdnr,ydnr,zdnr,Ux,Uy,Uz,px,py,pz,e,ee,Bx,By,Bz
  real p, rlb, fmlb, uylb
  integer ix, iz
!
!  Lower boundary
!
  rlb  = 1e-30
  fmlb = 0.
  do iz=1,mz
    do ix=1,mx
      if (py(ix,lb,iz) .le. 0.) then    ! outgoing
        rlb  = rlb  + ydnr(ix,lb,iz)
        fmlb = fmlb +   py(ix,lb,iz)
      end if
    end do
  end do
  uylb = fmlb/rlb
  if (idbg.gt.0) print *,'uylb =', uylb

  p = 0.5
  do iz=1,mz
    do ix=1,mx
      if (py(ix,lb,iz) .gt. 0.) then    ! incoming
        Uy(ix,lb,iz) = -uylb*p + Uy(ix,lb,iz)*(1.-p)
        py(ix,lb,iz) = -Uy(ix,lb,iz)*ydnr(ix,lb,iz)
      end if
    end do
  end do
  call extrapolate_face_lower (Uy)
  py(:,1:lb-1,:) = Uy(:,1:lb-1,:)*ydnr(:,1:lb-1,:)
!
!  Upper boundary
!
  call symmetric_face_upper (Uy)
  py(:,ub+1:my,:) = Uy(:,ub+1:my,:)*ydnr(:,ub+1:my,:)
END

!-----------------------------------------------------------------------
SUBROUTINE mfield_boundary (Ux,Uy,Uz,Bx,By,Bz)
  USE params
  implicit none
  real, dimension(mx,my,mz):: Ux,Uy,Uz,Bx,By,Bz
!
  call antisymmetric_center_lower(Bx)
  call antisymmetric_center_lower(Bz)
  call extrapolate_center_upper(Bx)
  call extrapolate_center_upper(Bz)
END

!-----------------------------------------------------------------------
SUBROUTINE regularize (f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
!
  call symmetric_center(f)
END

!-----------------------------------------------------------------------
SUBROUTINE ddt_boundary (r,px,py,pz,e,Bx,By,Bz, &
                         drdt,dpxdt,dpydt,dpzdt, &
                         dedt,dBxdt,dBydt,dBzdt)
  USE params
  implicit none
  real, dimension(mx,my,mz):: r,px,py,pz,e,Bx,By,Bz, &
                              drdt,dpxdt,dpydt,dpzdt, &
                              dedt,dBxdt,dBydt,dBzdt

END
