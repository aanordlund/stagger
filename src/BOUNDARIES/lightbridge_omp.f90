! $Id: lightbridge_omp.f90,v 1.5 2007/07/06 17:14:32 aake Exp $

MODULE boundary
  real t_Bbdry, Bscale, lb_eampl, t_bdry
  real rbot, pbot, ebot, eetop
  logical debug
  real(kind=8) rin, rtot, fmout, fmtot
  real(kind=8), allocatable:: tmpa(:)
  real, allocatable, dimension(:,:,:) :: tmp,Bxl,Byl,Bzl,Bxu,Byu,Bzu
END module

!-----------------------------------------------------------------------
SUBROUTINE init_boundary
  USE params
  USE arrays
  USE boundary
  implicit none

  character(len=mid):: id='$Id: lightbridge_omp.f90,v 1.5 2007/07/06 17:14:32 aake Exp $'
  if (id.ne.' ') print *,id; id=' '

  t_bdry = 0.01
  t_Bbdry  = 1                                  ! bdry decay time for B
  Bscale = sy*10./my
  lb = 6
  ub = my-5
  lb_eampl = 0.
  if (mpi_y > 0) lb = 1
  if (mpi_y < mpi_ny-1) ub = my
  rbot = -1.
  debug = .false.
  call read_boundary

  allocate (tmp(mx,mz,5), tmpa(5))
  !allocate (Rx(mx,2,mz),Ry(mx,2,mz),Rz(mx,2,mz))
  if (lb >  1) allocate (Bxl(mx,lb,mz),Byl(mx,lb,mz),Bzl(mx,lb,mz))
  if (ub < my) allocate (Bxu(mx,my-ub+1,mz),Byu(mx,my-ub+1,mz),Bzu(mx,my-ub+1,mz))
END

!-----------------------------------------------------------------------
SUBROUTINE read_boundary
  USE params
  USE boundary
  implicit none
  namelist /bdry/ lb, ub, rbot, ebot, pbot, t_bdry, t_Bbdry, Bscale, lb_eampl, debug

  if (stdin.ge.0) then
    rewind (stdin); read (stdin,bdry)
  else
    read (*,bdry)
  end if
  write (*,bdry)
END

!-----------------------------------------------------------------------
SUBROUTINE density_boundary (r)
  USE params
  implicit none
  real, dimension(mx,my,mz):: r
!
!  Version for linear cases
!
  call extrapolate_center(r)
END

!-----------------------------------------------------------------------
SUBROUTINE density_boundary_log (r,lnr)
  USE params
  implicit none
  real, dimension(mx,my,mz):: r,lnr
  integer iy,iz
  real hrhomax
!
!  Density boundary condition for open boundaries -- extrapolate in the log.
!  Also, prevent the density from flipping over to drive input.
!
  call extrapolate_center(lnr)
  hrhomax=sy/10.
  do iz=izs,ize
    do iy=lb-1,1,-1
      lnr(:,iy,iz)=min(lnr(:,iy,iz),lnr(:,iy+1,iz)*(ym(iy+1)-ym(iy))/hrhomax)
      r(:,iy,iz)=exp(lnr(:,iy,iz))
    end do
    do iy=ub+1,my
      r(:,iy,iz)=exp(lnr(:,iy,iz))
    end do
  end do
END

!-----------------------------------------------------------------------
SUBROUTINE energy_boundary (r,lnr,Ux,Uy,Uz,e,ee,p,Bx,By,Bz)
  USE params
  USE eos
  USE boundary
  implicit none
  real, dimension(mx,my,mz):: r,lnr,Ux,Uy,Uz,e,ee,p,Bx,By,Bz
  real s, pb, efact
  integer ix, iy, iz

  if (rbot .lt. 0.) then                                                ! first time
    call extrapolate_center_upper(ee)                                   ! extrapolate at bottom
    call constant_center_lower(ee)                                      ! vanishing deriv at top
    call pressure (r,ee,p)                                              ! single call
    rbot=sum(r(:,ub,:))/(mx*mz)                                         ! save density
    ebot=sum(e(:,ub,:))/(mx*mz)                                         ! save density
    pbot=sum(p(:,ub,:))/(mx*mz)                                         ! save pressure
    if (debug) print *,'ub, rbot, ebot =', ub, rbot, ebot
  end if
  do iz=izs,ize
    tmp(:,iz,1) = e(:,lb,iz)/r(:,lb,iz)
  end do
  !eetop = sum(e(:,lb,:)/r(:,lb,:))/(mx*mz)
  call average_2d (tmp, 1, tmpa)
  eetop = tmpa(1)
  if (debug) print *, 'eetop =', eetop

  do iz=izs,ize
    do ix=1,mx
      if (lb_eampl .ne. 0) then
        efact = 1.+lb_eampl*(cos((ix-97)*2.*pi/mx)+cos((iz-91)*2.*pi/mz))
        e(ix,ub,iz) = e(ix,ub,iz)*efact
        r(ix,ub,iz) = r(ix,ub,iz)*efact**(1./gamma)
      end if
      ee(ix,ub,iz) = e(ix,ub,iz)/r(ix,ub,iz)                            ! redo energy per unit mass
      ee(ix,lb,iz) = eetop						! constant energy per unit mass
      e(ix,lb,iz) = eetop*r(ix,lb,iz)                                   ! smooth top temperature
    end do
  end do
  call extrapolate_center_upper(ee)                                     ! extrapolate at bottom
  call constant_center_lower(ee)                                        ! vanishing deriv at top
  if (do_loginterp) then
    call density_boundary_log (r,lnr)                                   ! readjust density
  else
    call density_boundary(r)                                            ! readjust density
  end if

  do iz=izs,ize
    do iy=1,lb-1
      e(:,iy,iz)=r(:,iy,iz)*ee(:,iy,iz)
    end do
    do iy=ub+1,my
      e(:,iy,iz)=r(:,iy,iz)*ee(:,iy,iz)
    end do
  end do
END

!-----------------------------------------------------------------------
SUBROUTINE passive_boundary (r,Ux,Uy,Uz,d,dd)
  USE params
  implicit none
  real, dimension(mx,my,mz):: r,Ux,Uy,Uz,d,dd
  integer iy, iz

  call symmetric_center(dd)
  do iz=izs,ize
    do iy=1,lb-1
      d(:,iy,iz)=r(:,iy,iz)*dd(:,iy,iz)
    end do
    do iy=ub+1,my
      d(:,iy,iz)=r(:,iy,iz)*dd(:,iy,iz)
    end do
  end do
END

!-----------------------------------------------------------------------
SUBROUTINE velocity_boundary (r,xdnr,ydnr,zdnr,Ux,Uy,Uz,px,py,pz,e,Bx,By,Bz)
  USE params
  USE boundary
  implicit none
  real, dimension(mx,my,mz):: r,xdnr,ydnr,zdnr,Ux,Uy,Uz,px,py,pz,e,ee,Bx,By,Bz
  real p, uyin, uytot
  integer ix, iy, iz
!
!  Bottom boundary
!
  do iz=izs,ize
    do ix=1,mx
      tmp(ix,iz,1) = ydnr(ix,ub,iz)
      tmp(ix,iz,2) = ydnr(ix,ub,iz)
      tmp(ix,iz,3) = py  (ix,ub,iz)
      tmp(ix,iz,4) = py  (ix,ub,iz)
      if (py(ix,ub,iz) < 0) then
        tmp(ix,iz,2) = 0.
        tmp(ix,iz,4) = 0.
      end if
    end do
  end do

  call average_2d (tmp, 4, tmpa)
  rtot  = tmpa(1)
  rin   = tmpa(2)
  fmtot = tmpa(3)
  fmout = tmpa(4)

  uytot = fmtot/rtot
  uyin = -fmout/rin                                     ! target incoming velocity
  if (debug) print *,'uyin, uytot =', uyin, uytot

  call symmetric_center_upper (Ux)
  call symmetric_face_upper   (Uy)
  call symmetric_center_upper (Uz)
!
!  Top boundary, vanishing values
!
  call vanishing_center_lower (Ux)
  call symmetric_face_lower   (Uy)
  call vanishing_center_lower (Uz)
!
!  Gradually vanishing vertical mass flux in upper ghost zone
!
  do iz=izs,ize
    do iy=1,lb
      py(:,iy,iz)=iy/real(lb+1)*py(:,lb+1,iz)
    end do
  end do  
!
!  Recompute mass fluxes
!
  do iz=izs,ize
    do iy=1,lb
      px(:,iy,iz)=xdnr(:,iy,iz)*Ux(:,iy,iz)
      py(:,iy,iz)=ydnr(:,iy,iz)*Uy(:,iy,iz)
      pz(:,iy,iz)=zdnr(:,iy,iz)*Uz(:,iy,iz)
    end do
    do iy=ub,my
      px(:,iy,iz)=xdnr(:,iy,iz)*Ux(:,iy,iz)
      py(:,iy,iz)=ydnr(:,iy,iz)*Uy(:,iy,iz)
      pz(:,iy,iz)=zdnr(:,iy,iz)*Uz(:,iy,iz)
    end do
  end do
END

!-----------------------------------------------------------------------
SUBROUTINE mfield_boundary (Ux,Uy,Uz,Bx,By,Bz)
  USE params
  USE boundary
  implicit none
  real, dimension(mx,my,mz):: Ux,Uy,Uz,Bx,By,Bz
!
!  Require the horizontal field components to vanish
!
!  call extrapolate_center_lower(Bx)             ! top boundary:
!  call extrapolate_center_lower(Bz)             ! extrapolate
!  call antisymmetric_center_upper(Bx)           ! bottom boundary:
!  call antisymmetric_center_upper(Bz)           ! vanishing horizontal

  call mixed_center(Bx,Bscale)
  call mixed_center(Bz,Bscale)
  call symmetric_face(By)   ! TEMPORARY HACK: to be replaced

!  call extrapolate_center_lower(Bx)             ! top boundary:
!  call extrapolate_center_lower(Bz)             ! extrapolate
!  call vanishing_center_upper(Bx)               ! bottom boundary
!  call vanishing_center_upper(Bz)               ! bottom boundary
!  call symmetric_face(By)   ! TEMPORARY HACK: to be replaced

END

!-----------------------------------------------------------------------
SUBROUTINE efield_boundary (Ex, Ey, Ez, Bx_y, Bx_z, By_z, By_x, Bz_x, Bz_y, &
                            Rx, Ry, Rz, Ux_y, Ux_z, Uy_z, Uy_x, Uz_x, Uz_y, &
                            Ux, Uy, Uz)
  USE params
  USE boundary
  implicit none
  real, dimension(mx,my,mz):: Ex, Ey, Ez, Bx_y, Bx_z, By_z, By_x, Bz_x, Bz_y, &
                              Ux_y, Ux_z, Uy_z, Uy_x, Uz_x, Uz_y, &
                              Ux, Uy, Uz
  real, dimension(mx,2,mz):: Rx, Ry, Rz
!
!  Allow the top B-field to evolve, bottom B-field -> zero
!
  call extrapolate_center_lower (Ex)                  ! allow extrap
  call extrapolate_center_lower (Ez)                  ! allow extrap
  call derivative_face_upper (Ex, -Bz_y*(1./t_Bbdry)) ! bottom boundary
  call derivative_face_upper (Ez, +Bx_y*(1./t_Bbdry)) ! damp out horizontal B
END

!-----------------------------------------------------------------------
SUBROUTINE regularize (f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
!
  call symmetric_center (f)
END

!-----------------------------------------------------------------------
SUBROUTINE regularize_face (f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
!
  call symmetric_face (f)
END

!-----------------------------------------------------------------------
SUBROUTINE ddt_boundary (r,px,py,pz,e,Bx,By,Bz, &
                         drdt,dpxdt,dpydt,dpzdt, &
                         dedt,dBxdt,dBydt,dBzdt)
  USE params
  USE eos
  USE boundary
  implicit none
  real, dimension(mx,my,mz):: r,px,py,pz,e,Bx,By,Bz, &
                              drdt,dpxdt,dpydt,dpzdt, &
                              dedt,dBxdt,dBydt,dBzdt
  integer ix,iz
  real(kind=8) rub,pyub,elb,eub,ct,pub,dpydtub,scr,pb
  real(kind=8) rubx,pyubx,eubx,pubx,dpydtubx
!
  do iz=izs,ize
    do ix=1,mx
      tmp(ix,iz,1) = r(ix,ub,iz)
      tmp(ix,iz,2) = e(ix,ub,iz)
      tmp(ix,iz,3) = pbot0(ix,iz)
      tmp(ix,iz,4) = 1.5*py(ix,ub,iz)-0.5*py(ix,ub-1,iz)
      tmp(ix,iz,5) = 1.5*dpydt(ix,ub,iz)-0.5*dpydt(ix,ub-1,iz)
    end do
  end do

  call average_2d (tmp, 5, tmpa)
  rub     = tmpa(1)
  eub     = tmpa(2)
  pub     = tmpa(3)
  pyub    = tmpa(4)
  dpydtub = tmpa(5)

  if (debug) then
    print*,'rub',rub
    print*,'eub',eub
    print*,'pub',pub
    print*,'pyub',pyub
    print*,'dpydtub',dpydtub
  end if

  ct = 0.5/t_bdry										! damping constant
  do iz=izs,ize
    do ix=1,mx
      dpydt(ix,ub,iz) = dpydt(ix,ub,iz) - (dpydtub + pyub*ct)/1.5                               ! damp out average mass flux
      dedt(ix,ub,iz) = -(pbot0(ix,iz)+pbub(ix,iz)-pub) &
                       *(e(ix,ub,iz)+pbot0(ix,iz))/(gamma*pbot0(ix,iz))*ct                      ! cancel pressure fluctuation
      drdt(ix,ub,iz) = -(pbot0(ix,iz)+pbub(ix,iz)-pub) &
                       *r(ix,ub,iz)/(gamma*pbot0(ix,iz))*ct                                     ! at constant entropy
      if (py(ix,ub,iz).lt.0.) then								! for incoming flows..
	scr = ((e(ix,ub,iz)-ebot) - &
	       (e(ix,ub,iz)+pbot0(ix,iz))*(r(ix,ub,iz)-rbot)/r(ix,ub,iz))/(gamma*t_bdry)        ! prop to entropy fluctuation
	dedt(ix,ub,iz) = dedt(ix,ub,iz) - scr*(1.+e(ix,ub,iz)/r(ix,ub,iz)*dlnpdE_r(ix,iz))	! cancel entropy fluctuation
        drdt(ix,ub,iz) = drdt(ix,ub,iz) + scr*dlnpdE_r(ix,iz)					! at constant pressure
        dpxdt(ix,ub,iz) = -px(ix,ub,iz)*ct							! damp horizontal motions
        dpzdt(ix,ub,iz) = -pz(ix,ub,iz)*ct
      endif
    end do
  end do
END

