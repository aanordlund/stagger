! $Id: alfvenwave.f90,v 1.4 2005/01/08 15:55:38 aake Exp $

MODULE boundary
  real u0
  character(len=16):: type
END MODULE

!-----------------------------------------------------------------------
SUBROUTINE init_boundary
  USE params
  USE boundary
  implicit none

  character(len=80):: id='$Id: alfvenwave.f90,v 1.4 2005/01/08 15:55:38 aake Exp $'
  if (id.eq.' ') print *,id; id=' '

  u0=0.5
  type='face_v'
  lb=6
  ub=my-5
  call read_boundary
END

!-----------------------------------------------------------------------
SUBROUTINE read_boundary
  USE params
  USE boundary
  implicit none
  namelist /bdry/ u0,type

  if (stdin.ge.0) then
    rewind (stdin); read (stdin,bdry)
  else
    read (*,bdry)
  end if
  write (*,bdry)
END

!-----------------------------------------------------------------------
SUBROUTINE density_boundary (r)
  USE params
  implicit none
  real, dimension(mx,my,mz):: r
!
!  Version for linear cases
!
  call extrapolate_center(r)
END

!-----------------------------------------------------------------------
SUBROUTINE density_boundary_log (r,lnr)
  USE params
  implicit none
  real, dimension(mx,my,mz):: r,lnr
  integer iy,iz
  real hrhomax, rhof(lb), rhoav, p
!
!  Density boundary condition for closed boundaries -- extrapolate in the log
!
  call extrapolate_center(lnr)
!$omp parallel do private(iz,iy,p)
  do iz=1,mz
    do iy=1,lb-1
      r(:,iy,iz)=exp(lnr(:,iy,iz))
    end do
    do iy=ub+1,my
      r(:,iy,iz)=exp(lnr(:,iy,iz))
    end do
  end do
END

!-----------------------------------------------------------------------
SUBROUTINE energy_boundary (r,lnr,Ux,Uy,Uz,e,ee,p,Bx,By,Bz)
  USE params
  USE eos
  implicit none
  real, dimension(mx,my,mz):: r,lnr,Ux,Uy,Uz,e,ee,p,Bx,By,Bz
  real, save:: rbot=-1., pbot, sbot, ebot, eetop
  real s, pb
  integer ix, iy, iz

  call symmetric_center (ee)
!$omp parallel do private(iz,iy)
  do iz=1,mz
    do iy=1,lb-1
      e(:,iy,iz)=r(:,iy,iz)*ee(:,iy,iz)
    end do
    do iy=ub+1,my
      e(:,iy,iz)=r(:,iy,iz)*ee(:,iy,iz)
    end do
  end do
END

!-----------------------------------------------------------------------
SUBROUTINE passive_boundary (r,Ux,Uy,Uz,d,dd)
  USE params
  implicit none
  real, dimension(mx,my,mz):: r,Ux,Uy,Uz,d,dd
  integer iy, iz

  call symmetric_center(dd)
!$omp parallel do private(iz,iy)
  do iz=1,mz
    do iy=1,lb-1
      d(:,iy,iz)=r(:,iy,iz)*dd(:,iy,iz)
    end do
    do iy=ub+1,my
      d(:,iy,iz)=r(:,iy,iz)*dd(:,iy,iz)
    end do
  end do
END

!-----------------------------------------------------------------------
SUBROUTINE velocity_boundary (r,xdnr,ydnr,zdnr,Ux,Uy,Uz,px,py,pz,e,Bx,By,Bz)
  USE params
  USE boundary
  implicit none
  real, dimension(mx,my,mz):: r,xdnr,ydnr,zdnr,Ux,Uy,Uz,px,py,pz,e,ee,Bx,By,Bz
  real p, rin, rtot, fmout, fmtot, uyin, uytot
  integer ix, iy, iz

  if (type.eq.'face_v_l') then
    do iz=1,mz
    do ix=1,mx
!      Ux(ix,1:lb,iz) = u0
!      Uz(ix,1:lb,iz) = 0.
!      Ux(ix,ub:my,iz) = 0.
!      Uz(ix,ub:my,iz) = 0.
      Ux(ix,lb,iz) = u0
      Uz(ix,lb,iz) = 0.
      Ux(ix,ub,iz) = 0.
      Uz(ix,ub,iz) = 0.
    end do
    end do
    call extrapolate_center (Ux)
    call extrapolate_center (Uz)
  else if (type.eq.'sym') then
    do iz=1,mz
    do ix=1,mx
      Ux(ix,1:lb,iz) = u0
      Uz(ix,1:lb,iz) = 0.
      Ux(ix,ub:my,iz) = u0
      Uz(ix,ub:my,iz) = 0.
      Ux(ix,lb+1,iz) = 0.5*(u0+ux(ix,lb+2,iz))
      Ux(ix,ub-1,iz) = 0.5*(u0+ux(ix,ub-2,iz))
    end do
    end do
  else
    do iz=1,mz
    do ix=1,mx
      Ux(ix,lb,iz) = u0
      Uz(ix,lb,iz) = 0.
      Ux(ix,ub,iz) = u0
      Uz(ix,ub,iz) = 0.
    end do
    end do
    call extrapolate_center (Ux)
    call extrapolate_center (Uz)
  end if

!  call     symmetric_center (Ux)
!  call     symmetric_center (Uz)
  call antisymmetric_face (Uy)

!$omp parallel do private(iz,iy)
  do iz=1,mz
      px(:,1:lb+1,iz) = xdnr(:,1:lb+1,iz)*Ux(:,1:lb+1,iz)
      pz(:,1:lb+1,iz) = zdnr(:,1:lb+1,iz)*Uz(:,1:lb+1,iz)
      py(:,1:lb,iz) = ydnr(:,1:lb,iz)*Uy(:,1:lb,iz)
      px(:,ub-1:my,iz) = xdnr(:,ub-1:my,iz)*Ux(:,ub-1:my,iz)
      pz(:,ub-1:my,iz) = zdnr(:,ub-1:my,iz)*Uz(:,ub-1:my,iz)
      py(:,ub:my,iz) = ydnr(:,ub:my,iz)*Uy(:,ub:my,iz)
  end do
END

!-----------------------------------------------------------------------
SUBROUTINE mfield_boundary (Ux,Uy,Uz,Bx,By,Bz)
  USE params
  implicit none
  real, dimension(mx,my,mz):: Ux,Uy,Uz,Bx,By,Bz

END

!-----------------------------------------------------------------------
SUBROUTINE efield_boundary (Ex, Ey, Ez, Bx_y, Bx_z, By_z, By_x, Bz_x, Bz_y, &
                            Rx, Ry, Rz, Ux_y, Ux_z, Uy_z, Uy_x, Uz_x, Uz_y, &
                            Ux, Uy, Uz)
  USE params
  USE boundary
  USE stagger
  implicit none
  real, dimension(mx,my,mz):: Ex, Ey, Ez, Bx_y, Bx_z, By_z, By_x, Bz_x, Bz_y, &
                              Rx, Ry, Rz, Ux_y, Ux_z, Uy_z, Uy_x, Uz_x, Uz_y, &
                              Ux, Uy, Uz, By_y
  real, dimension(mx,mz):: flb, fub
!
!  We need the horizontal velocity and the vertical magnetic field, both
!  shifted to positions (ix,iy,iz+1/2) for Ex and (ix+1/2,iy,iz) for Ez.
!
  select case (type)
  case('sym')
    call symmetric_face(Ex)
    call symmetric_face(Ez)
  case('extrap')
    call extrapolate_face(Ex)
    call extrapolate_face(Ez)
  case('face_v')
    call yup_set (By_z, By_y)
    flb = +Uz(:,lb,:)*By_y(:,lb,:)
    fub = +Uz(:,ub,:)*By_y(:,ub,:)
    call extrapolate_face_value (Ex, flb, fub)
    call yup_set (By_x, By_y)
    flb = -Ux(:,lb,:)*By_y(:,lb,:)
    fub = -Ux(:,ub,:)*By_y(:,ub,:)
    call extrapolate_face_value (Ez, flb, fub)
  case('face_v_l')
    call yup_set (By_z, By_y)
    flb = +Uz(:,lb,:)*By_y(:,lb,:)
    call extrapolate_face_value_lower (Ex, flb)
    call yup_set (By_x, By_y)
    flb = -Ux(:,lb,:)*By_y(:,lb,:)
    call extrapolate_face_value_lower (Ez, flb)
    call symmetric_face_upper (Ex)
    call symmetric_face_upper (Ez)
  end select
END

!-----------------------------------------------------------------------
SUBROUTINE regularize (f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
!
  call symmetric_center(f)
END

!-----------------------------------------------------------------------
SUBROUTINE regularize_face (f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
!
  call symmetric_face (f)
END

!-----------------------------------------------------------------------
SUBROUTINE ddt_boundary (r,px,py,pz,e,Bx,By,Bz, &
                         drdt,dpxdt,dpydt,dpzdt, &
                         dedt,dBxdt,dBydt,dBzdt)
  USE params
  USE boundary
  implicit none
  real, dimension(mx,my,mz):: r,px,py,pz,e,Bx,By,Bz, &
                              drdt,dpxdt,dpydt,dpzdt, &
                              dedt,dBxdt,dBydt,dBzdt
  integer ix,iz
  real rlb

 if (type.eq.'sym') then
  do iz=1,mz
  do ix=1,mx
    dpxdt(iz, 1:lb+1,iz) = 0.
    dpzdt(iz, 1:lb+1,iz) = 0.
    dpxdt(iz,ub-1:my,iz) = 0.
    dpzdt(iz,ub-1:my,iz) = 0.
    dBxdt(iz,   1:2 ,iz) = 0.
    dBxdt(iz,my-2:my,iz) = 0.
    dBydt(iz,   1:2 ,iz) = 0.
    dBydt(iz,my-2:my,iz) = 0.
    dBzdt(iz,   1:2 ,iz) = 0.
    dBzdt(iz,my-2:my,iz) = 0.
  end do
  end do
 else
  do iz=1,mz
  do ix=1,mx
    dpxdt(iz, 1:lb-1,iz) = 0.
    dpzdt(iz, 1:lb-1,iz) = 0.
    dpxdt(iz,ub+1:my,iz) = 0.
    dpzdt(iz,ub+1:my,iz) = 0.
    dBxdt(iz,   1:2 ,iz) = 0.
    dBxdt(iz,my-2:my,iz) = 0.
    dBydt(iz,   1:2 ,iz) = 0.
    dBydt(iz,my-2:my,iz) = 0.
    dBzdt(iz,   1:2 ,iz) = 0.
    dBzdt(iz,my-2:my,iz) = 0.
  end do
  end do
 end if

END
