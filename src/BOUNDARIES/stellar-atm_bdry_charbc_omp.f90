! $Id: stellar-atm_bdry_charbc_omp.f90,v 1.94 2010/01/07 10:04:04 aake Exp $
!***********************************************************************
MODULE boundary
  real t_Bbdry, Bscale, lb_eampl, t_bdry, eetop_frac, Uy_bdry 
  real rbot, pbot, ebot, eetop, htop
  real Bx0, By0, Bz0, rmin, pb_fact, uy_max
  real pdamp, rtop_frac, fmtop_frac, divb_max
  real ampxy, ampyz, period, omegap, akx, aky, akz, kxp, kyp, kzp
  real(kind=8) rub,pyub,elb,eub,pub,pubt,dpydtub
  real, allocatable, dimension(:,:,:):: Bxl, Byl, Bzl, Bxu, Byu, Bzu
  real, allocatable, dimension(:,:) :: drdt_bot, dpxdt_bot, dpydt_bot, &
                                       dpzdt_bot, dedt_bot
  real, allocatable, dimension(:,:) :: drdt_top, dpxdt_top, dpydt_top, &
                                       dpzdt_top, dedt_top


  real, allocatable, dimension(:,:,:):: Uxc, Uyc, Uzc, rdn, Csdn, &
                                        drdy, dpdy, dUxdy, dUydy, dUzdy, &
                                        d1, d2, d3, d4, d5, d6, d7, d8

  real, allocatable, dimension(:,:):: dpdx, dpdz, divh_ph, divh_uh, &
                                      divh_pxuh, divh_pyuh, divh_pzuh, divh_euh, &
                                      delta_p, rtop, lnrtop, etop

  real, allocatable, dimension(:,:):: scra, scrb, scrc, scrd, scre, scrf

  real(kind=8), allocatable, dimension(:):: pyav,Csav

  logical debug, bdry_first, Binflow
  integer verbose, idump, ndump

END module

!-----------------------------------------------------------------------
SUBROUTINE init_boundary
  USE params
  USE arrays
  USE boundary
  implicit none
  character(len=mid):: id='$Id: stellar-atm_bdry_charbc_omp.f90,v 1.94 2010/01/07 10:04:04 aake Exp $'

  call print_id(id)

  if (mpi_y == 0) then
    lb = 6
    allocate (Bxl(mx,lb,mz),Byl(mx,lb,mz),Bzl(mx,lb,mz))
    allocate (drdt_top(mx,mz),dpxdt_top(mx,mz),dpydt_top(mx,mz),dpzdt_top(mx,mz),dedt_top(mx,mz))
  else
    lb = 0
  end if
  if (mpi_y == mpi_ny-1) then
    ub = my-5
    allocate (Bxu(mx,my-ub,mz),Byu(mx,my-ub,mz),Bzu(mx,my-ub,mz))
    allocate (drdt_bot(mx,mz),dpxdt_bot(mx,mz),dpydt_bot(mx,mz),dpzdt_bot(mx,mz),dedt_bot(mx,mz))
  else
    ub = my
  end if

  allocate(Uxc(mx,my,mz),Uyc(mx,my,mz),Uzc(mx,my,mz))
  allocate(rdn(mx,my,mz),Csdn(mx,my,mz),drdy(mx,my,mz),dpdy(mx,my,mz))
  allocate(dUxdy(mx,my,mz),dUydy(mx,my,mz),dUzdy(mx,my,mz))
  allocate(d1(mx,my,mz),d2(mx,my,mz),d3(mx,my,mz),d4(mx,my,mz),d5(mx,my,mz),d6(mx,my,mz),d7(mx,my,mz),d8(mx,my,mz))

  allocate(dpdx(mx,mz),dpdz(mx,mz),divh_ph(mx,mz),divh_uh(mx,mz),divh_pxuh(mx,mz),divh_pyuh(mx,mz),divh_pzuh(mx,mz))
  allocate(divh_euh(mx,mz),delta_p(mx,mz),rtop(mx,mz),lnrtop(mx,mz),etop(mx,mz))
  allocate(scra(mx,mz),scrb(mx,mz),scrc(mx,mz),scrd(mx,mz),scre(mx,mz),scrf(mx,mz))
  allocate(pyav(my),Csav(my))

  t_bdry = 0.01                                                         ! bdry decay time
  t_Bbdry  = 0.01                                                       ! bdry decay time for B
  eetop_frac=0.05                                                       ! fraction of current eetop
  eetop = -1.
  rtop_frac=0.05                                                        ! fraction of current rtop
  fmtop_frac=1.0                                                        ! fraction fmass correction for rho(lb)
  pdamp = 0.
  Uy_bdry = 0.1
  lb_eampl = 0.
  rbot = -1.
  htop = 0.
  rmin = 1e-3
  pb_fact = 1.2
  uy_max = 2.                                                           ! 20 km/s with solar scaling
  Bscale = dym(ub)*10.
  Bx0 = 0.
  By0 = 0.
  Bz0 = 0.
  Binflow = .false.
  debug = .false.
  verbose = 0
  bdry_first = .true.
  do_stratified = .true.
  ampxy = 0.
  ampyz = 0.
  period = 0.
  akx = 1.
  aky = 1.
  akz = 1.

  call read_boundary

  kxp=akx*2.*pi/mx
  kyp=aky*2.*pi/(ub-lb+1)
  kzp=akz*2.*pi/mz
  if (period .gt. 0.) omegap=2.*pi/period

  call init_potential_lower
END

!-----------------------------------------------------------------------
SUBROUTINE read_boundary
  USE params
  USE boundary
  implicit none
  namelist /bdry/ lb, ub, t_bdry, t_Bbdry, Bscale, eetop_frac, lb_eampl, &
                  htop, rtop_frac, fmtop_frac, eetop, rbot, ebot, pdamp, &
                  Bx0, By0, Bz0, Binflow, Uy_bdry, &
		  rmin, pb_fact, uy_max, debug, verbose, &
                  ampxy, ampyz, period, akx, aky, akz

  rewind (stdin); read (stdin,bdry)
  if (master) write (*,bdry)
END

!-----------------------------------------------------------------------
SUBROUTINE density_boundary (r,lnr,py,e)
  USE params
  USE arrays, only: scr6, eeav
  USE forcing
  USE boundary
  USE eos
  implicit none
  real, dimension(mx,my,mz):: r,lnr,py,e
  integer ix,iy,iz
  real htopi, dlnrdy, lnrmin, lnrmax

if (do_trace .and. omp_master) print *, 'density_BC: mpi_rank,y,ny = ', mpi_rank, mpi_y, mpi_ny

! idump = 0
  ndump=30
  if (do_mhd) ndump=45

  call haverage_subr (r,rav)                                            ! horizontally averaged rho
  call haverage_subr (e,eav)                                            ! horizontally averaged e
  do iz=izs,ize
    scr6(:,:,iz) = e(:,:,iz)/r(:,:,iz)                                  ! ee = e/r
  end do

  if (pav(1).eq.0.) then                                                ! if first time
    idump = 0
    do iz = izs, ize
      rtop(:,iz)=rav(lb)
      etop(:,iz)=eav(lb)
    end do
    if (debug .and. omp_master) print *,'eetop =',eetop
  end if
  if (eetop.le.0.) then							! if eetop not read in
    call haverage_subr (scr6, eeav)					! ee horizontal average
!$omp single
    eetop=eeav(lb+1)
!$omp end single
  end if
  do iz=izs,ize
    scr6(:,lb,iz) = eetop
  end do
  call dumpl(r,'density1:r','bdry.dmp',0+ndump*idump)
  call dumpl(e,'density1:e','bdry.dmp',1+ndump*idump)

  if (lb > 1) then
    call constant_center_lower(scr6)					! vanishing deriv at top
    call extrapolate_center_lower(lnr)                                  ! basic default action
    do iz=izs,ize
      do iy=1,lb
        r(:,iy,iz) = exp(lnr(:,iy,iz))                              ! consistent density
        e(:,iy,iz)=r(:,iy,iz)*scr6(:,iy,iz)
      end do
    end do
  end if

  call extrapolate_center_upper(lnr)                                    ! extrapolate in the log

  if (ub < my) then
    do iz=izs,ize
    do iy=ub+1,my
    do ix=1,mx
      r(ix,iy,iz)=exp(lnr(ix,iy,iz))                                    ! consistent density
    end do 
    end do 
    end do
  end if
  call dumpl(r,'density2:r','bdry.dmp',2+ndump*idump)
  call dumpl(e,'density2:e','bdry.dmp',3+ndump*idump)
  call barrier_omp ('density')
END

!-----------------------------------------------------------------------
SUBROUTINE energy_boundary (r,lnr,Ux,Uy,Uz,e,ee,p,Bx,By,Bz)
  USE params
  USE arrays, ONLY: eeav
  USE eos
  USE boundary
  implicit none
  real, dimension(mx,my,mz):: r,lnr,Ux,Uy,Uz,e,ee,p,Bx,By,Bz
  real s, pb, efact
  real(kind=8) tmp(3)
  integer ix, iy, iz
  logical do_io

if (do_trace .and. omp_master) print *, 'energy_BC: mpi_rank,y,ny = ', mpi_rank, mpi_y, mpi_ny
  call dumpl(r,'energy1:r','bdry.dmp',4+ndump*idump)
  call dumpl(e,'energy1:e','bdry.dmp',5+ndump*idump)
  call dumpl(ee,'energy1:ee','bdry.dmp',6+ndump*idump)

  if (lb > 1) then                                                      ! top BC
!$omp single
    eetop = (1.-eetop_frac)*eetop + eetop_frac*eeav(lb+1)
!$omp end single
    if ((do_io (t, tsnap, isnap+isnap0, nsnap) .or. &
      do_io (t, tscr, iscr+iscr0, nscr) .or. debug) .and. omp_master) print *,'eetop =',eetop

    do iz=izs,ize
      ee(:,lb,iz) = eetop
    end do
!   call extrapolate_center_lower(ee)
    call constant_center_lower(ee)					! vanishing deriv at top
    call haverage_subr (ee, eeav)
    do iz=izs,ize
      do iy=1,lb
        e(:,iy,iz)=r(:,iy,iz)*ee(:,iy,iz)					! consistent energy
      end do
    end do
  end if

  if (mpi_y == mpi_ny-1 .and. rbot < 0.) then                           ! bottom BC
    call barrier_omp ('rbot')
!$omp single
    rbot=sum(r(:,ub,:))/(mx*mz)                                         ! save density
    ebot=sum(e(:,ub,:))/(mx*mz)                                         ! save density
    tmp(1) = rbot
    tmp(2) = ebot
    call haverage_mpi (tmp, 2)
    rbot = tmp(1)
    ebot = tmp(2)
!$omp end single
    if (master) print *,'ub, rbot, ebot =', ub, rbot, ebot
  end if

  if (mpi_y == mpi_ny-1 .and. lb_eampl .ne. 0) then
    do iz=izs,ize
      do ix=1,mx
!        efact = 1.+lb_eampl*(cos((ix-97)*2.*pi/mx)+cos((iz-91)*2.*pi/mz))
!        e(ix,ub,iz) = e(ix,ub,iz)*efact
!        r(ix,ub,iz) = r(ix,ub,iz)*efact**(1./gamma)
        ee(ix,ub,iz) = e(ix,ub,iz)/r(ix,ub,iz)                          ! redo energy per unit mass
      end do
    end do
    call extrapolate_center_log_upper(r)
    do iz=izs,ize
      do iy=ub+1,my
        lnr(:,iy,iz)=alog(r(:,iy,iz))
      end do
    end do
  end if

  call extrapolate_center_upper(ee)                                     ! extrapolate at bottom

  if (ub < my) then
    do iz=izs,ize
    do iy=ub+1,my
      e(:,iy,iz)=r(:,iy,iz)*ee(:,iy,iz)
    end do
    end do
  end if
  call dumpl(r,'energy2:r','bdry.dmp',7+ndump*idump)
  call dumpl(e,'energy2:e','bdry.dmp',8+ndump*idump)
  call dumpl(ee,'energy2:ee','bdry.dmp',9+ndump*idump)
END

!-----------------------------------------------------------------------
SUBROUTINE passive_boundary (r,Ux,Uy,Uz,d,dd)
  USE params
  implicit none
  real, dimension(mx,my,mz):: r,Ux,Uy,Uz,d,dd
  integer iy, iz

  call symmetric_center(dd)
  if (mpi_y == 0) then                                                  ! top BC
    do iz=izs,ize
    do iy=1,lb-1
      d(:,iy,iz)=r(:,iy,iz)*dd(:,iy,iz)
    end do
    end do
  end if

  if (mpi_y == mpi_ny-1) then                                           ! bottom BC
    do iz=izs,ize
    do iy=ub+1,my
      d(:,iy,iz)=r(:,iy,iz)*dd(:,iy,iz)
    end do
    end do
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE velocity_boundary (r,xdnr,ydnr,zdnr,Ux,Uy,Uz,px,py,pz,e,Bx,By,Bz)
  USE params
  USE boundary
  implicit none
  real, dimension(mx,my,mz):: r,xdnr,ydnr,zdnr,Ux,Uy,Uz,px,py,pz,e,Bx,By,Bz
  real p, rin, rtot, fmout, fmtot, uyin, uytot
  integer ix, iy, iz

    if (do_trace) print 1, 'velocity_BC 1: mpi_rank,y,ny = ', mpi_rank, mpi_y, mpi_ny, omp_mythread
1   format(1x,a,4i6)
    call dumpl(px,'velocity1;px','bdry.dmp',10+ndump*idump)
    call dumpl(py,'velocity1;py','bdry.dmp',11+ndump*idump)
    call dumpl(pz,'velocity1;pz','bdry.dmp',12+ndump*idump)
!
!  Top boundary, vanishing velocity derivatives
!
!  call constant_center_lower (Ux)
!  call constant_face_lower   (Uy)
!  call constant_center_lower (Uz)
   call extrapolate_center_lower(Ux)
   call extrapolate_face_lower  (Uy)
   call extrapolate_center_lower(Uz)
    if (do_trace) print 1,'velocity: 2', mpi_rank, mpi_y, mpi_ny, omp_mythread
!
!  Top boundary, impose maximum velocity -- relevant when the 
!  density is clambed and cannot become lower
!
  if (lb > 1) then
!   do iz=izs,ize
!   do ix=1,mx
!     if (Uy(ix,lb+1,iz) > 0. .or. Uy(ix,lb,iz) > 0.) then
!     do iy=1,lb+2
!       Ux(ix,iy,iz) = min(Ux(ix,iy,iz),+uy_max)
!       Uy(ix,iy,iz) = min(Uy(ix,iy,iz),+uy_max)
!       Uy(ix,iy,iz) = min(Uy(ix,iy,iz),Uy(ix,lb+1,iz))
!       Uz(ix,iy,iz) = min(Uz(ix,iy,iz),+uy_max)
!     end do
!     end if
!   end do
!   end do
!
!  Recompute mass fluxes
!
    do iz=izs,ize
      do iy=1,lb
        px(:,iy,iz)=xdnr(:,iy,iz)*Ux(:,iy,iz)
        py(:,iy,iz)=ydnr(:,iy,iz)*Uy(:,iy,iz)
        pz(:,iy,iz)=zdnr(:,iy,iz)*Uz(:,iy,iz)
      end do
!     py(:,lb+1,iz)=ydnr(:,lb+1,iz)*Uy(:,lb+1,iz)
    end do
  end if
    if(do_trace) print 1,'velocity: 3', mpi_rank, mpi_y, mpi_ny, omp_mythread

!  call symmetric_center_upper (Ux)
!  call symmetric_face_upper   (Uy)
!  call symmetric_center_upper (Uz)
   call extrapolate_center_upper(Ux)  
   call extrapolate_face_upper  (Uy)
   call extrapolate_center_upper(Uz)
!
!  Recompute mass fluxes
!
  if (mpi_y == mpi_ny-1) then
    do iz=izs,ize
    do iy=ub,my
      px(:,iy,iz)=xdnr(:,iy,iz)*Ux(:,iy,iz)
      py(:,iy,iz)=ydnr(:,iy,iz)*Uy(:,iy,iz)
      pz(:,iy,iz)=zdnr(:,iy,iz)*Uz(:,iy,iz)
    end do
    end do

  end if
    if(do_trace) print 1,'velocity: 4', mpi_rank, mpi_y, mpi_ny, omp_mythread
    call dumpl(px,'velocity2:px','bdry.dmp',13+ndump*idump)
    call dumpl(py,'velocity2:py','bdry.dmp',14+ndump*idump)
    call dumpl(pz,'velocity2:pz','bdry.dmp',15+ndump*idump)
END

!-----------------------------------------------------------------------
SUBROUTINE mfield_boundary (Ux,Uy,Uz,Bx,By,Bz)
  USE params
  USE boundary
  USE arrays, ONLY: scr1
  implicit none
  real, dimension(mx,my,mz):: Ux,Uy,Uz,Bx,By,Bz
  integer ix, iy, iz
  real f, divb_rms
  logical do_io
  character(len=mid), save:: id='mfield_boundary $Id: stellar-atm_bdry_charbc_omp.f90,v 1.94 2010/01/07 10:04:04 aake Exp $'

  call print_id (id)

  if (do_io (t, tsnap, isnap+isnap0, nsnap) .or. &
      do_io (t, tscr, iscr+iscr0, nscr)) then 
    call ddxup_set (Bx, scr1)
    call ddyup_add (By, scr1)
    !$omp barrier
    call ddzup_add (Bz, scr1)
    call dumpl(scr1,'mfield:divb','divb.dmp',idump)
    do iz=izs,ize
      scr1(:,:,iz)=scr1(:,:,iz)**2 
      do iy=1,lb-1
        scr1(:,iy,iz)=0.
      end do
      do iy=ub+1,my
        scr1(:,iy,iz)=0.
      end do
    end do
    call average_subr (scr1,divb_rms)
    call fmaxval_subr ('divb',scr1,divb_max)
    !$omp barrier
    if (master) then 
      print *,'RMS(divb) =',sqrt(divb_rms)
      print *,'MAX(divb) =',sqrt(divb_max)
    end if
  end if

  if (mpi_y == 0) then                                                  ! top BC
    do iz=izs,ize
      do iy=1,lb
        Bxl(:,iy,iz)=Bx(:,iy,iz)                                        ! store orig bdry B
        Byl(:,iy,iz)=By(:,iy,iz)
        Bzl(:,iy,iz)=Bz(:,iy,iz)
      end do
    end do

    if (omp_master.and.debug) then
      print *,'Bx (before)=',Bx(1,0:9,1)
      print *,'By (before)=',By(1,0:9,1)
      print *,'Bz (before)=',Bz(1,0:9,1)
    end if
    call dumpl(bx,'mfield1:Bx','bdry.dmp',30+ndump*idump)
    call dumpl(by,'mfield1:By','bdry.dmp',31+ndump*idump)
    call dumpl(bz,'mfield1:Bz','bdry.dmp',32+ndump*idump)

    call potential_lower(Bx,By,Bz,lb)                                   ! ->potential

    if (master.and.debug) then
      print *,'Bx (after)=',Bx(1,0:9,1)
      print *,'By (after)=',By(1,0:9,1)
      print *,'Bz (after)=',Bz(1,0:9,1)
    end if
  end if

!-----------------------------------------------------------------------
!
!  Lower boundary: Store & restore original ghost zone field, to avoid
!  destroying div(B).  But set up for computing the electric fiels as
!  if the field in inflows is constant.  This gets multiplied by the
!  inflow velocity field, which vanishes btw inflow and outflow, thus
!  avoiding creating a discontinuity in the electric field.
!
!-----------------------------------------------------------------------

  if (mpi_y == mpi_ny-1) then                                           ! bottom BC
    do iz=izs,ize
      do iy=1,my-ub
        Bxu(:,iy,iz)=Bx(:,ub+iy,iz)                                     ! store orig bdry B
        Byu(:,iy,iz)=By(:,ub+iy,iz)
        Bzu(:,iy,iz)=Bz(:,ub+iy,iz)
      end do
    end do

!   call mixed_center_upper(Bx,Bscale)
!   call mixed_center_upper(Bz,Bscale)
    call extrapolate_center_upper(Bx)
    call extrapolate_face_upper  (By)
    call extrapolate_center_upper(Bz)
    if (Binflow) then
      do iz=izs,ize
      do ix=1,mx
        f = 1./(1.+exp(Uy(ix,ub,iz)/Uy_bdry))                           ! -> 1 in inflows
        do iy=ub+1,my
          Bx(ix,iy,iz) = (1.-f)*Bx(ix,iy,iz) + f*Bx0
          Bz(ix,iy,iz) = (1.-f)*Bz(ix,iy,iz) + f*Bz0
        enddo
      enddo
      enddo
    end if
  end if
  call dumpl(bx,'mfield2:Bx','bdry.dmp',33+ndump*idump)
  call dumpl(by,'mfield2:By','bdry.dmp',34+ndump*idump)
  call dumpl(bz,'mfield2:Bz','bdry.dmp',35+ndump*idump)
END

!-----------------------------------------------------------------------
SUBROUTINE ecurrent_boundary (Ex, Ey, Ez, Jx, Jy, Jz)
  USE params
  USE boundary
  implicit none
  real, dimension(mx,my,mz):: Ex, Ey, Ez, Jx, Jy, Jz

END

!-----------------------------------------------------------------------
SUBROUTINE efield_boundary (Ex, Ey, Ez, Bx_y, Bx_z, By_z, By_x, Bz_x, Bz_y, &
                            Bx, By, Bz, Ux_y, Ux_z, Uy_z, Uy_x, Uz_x, Uz_y, &
                            Ux, Uy, Uz)
  USE params
  USE boundary
  USE arrays, only: scr1, scr2
  implicit none
  real, dimension(mx,my,mz):: Ex, Ey, Ez, Bx_y, Bx_z, By_z, By_x, Bz_x, Bz_y, &
                              Ux_y, Ux_z, Uy_z, Uy_x, Uz_x, Uz_y, &
                              Ux, Uy, Uz, Bx, By, Bz
  integer ix, iy, iz
  real f, ct

  character(len=mid), save:: id='efield_boundary $Id: stellar-atm_bdry_charbc_omp.f90,v 1.94 2010/01/07 10:04:04 aake Exp $'

  call print_id (id)

!-----------------------------------------------------------------------
!  Top boundary:  The template magnetic field is from a potential
!  extrapolation, and we construct an E-field that drives the real
!  B-field towards the template field.
!-----------------------------------------------------------------------
  ct = 1./t_Bbdry
  if (mpi_y == 0) then                                                  ! top BC
    do iz=izs,ize
     do iy=1,lb
      Ex(:,iy,iz) = Ex(:,lb+1,iz) + (Bzl(:,iy,iz)-Bz(:,iy,iz))* &       ! drive towards potential field
        ct*(ymdn(lb+1)-ymdn(iy))
      Ez(:,iy,iz) = Ez(:,lb+1,iz) - (Bxl(:,iy,iz)-Bx(:,iy,iz))* &       ! drive towards potential field
        ct*(ymdn(lb+1)-ymdn(iy))
      Bx(:,iy,iz)=Bxl(:,iy,iz)                                          ! restore upper bdry B
      By(:,iy,iz)=Byl(:,iy,iz)                                          ! restore upper bdry B
      Bz(:,iy,iz)=Bzl(:,iy,iz)                                          ! restore upper bdry B
     end do
    end do
  end if

!-----------------------------------------------------------------------
!  Bottom boundary:  Ex and Ez are already good, having been computed
!  from regularized velocity and magnetic field values.  However, the
!  values more than 3 grids outside the boundary are contaminated, due
!  to the ydn(Bxz) and ydn(Uxz) interpolations, so these need to be
!  regularized.
!-----------------------------------------------------------------------
  if (mpi_y == mpi_ny-1) then                                           ! bottom BC
    if (Binflow) then
      do iz=izs,ize
        do iy=ub+4,my
          Ex(:,iy,iz) = Ex(:,ub+3,iz)					! regularize
          Ey(:,iy,iz) = Ey(:,ub+3,iz)					! regularize
          Ez(:,iy,iz) = Ez(:,ub+3,iz)					! regularize
        enddo
      enddo
    else
      do iz=izs,ize
        scr1(:,ub,iz) = -(Bzu(:,1,iz)-Bz0)*ct
        scr2(:,ub,iz) = +(Bxu(:,1,iz)-Bx0)*ct
      enddo
      call derivative_face_upper (Ex, scr1) 
      call derivative_face_upper (Ez, scr2) 
    end if

    do iz=izs,ize
     do iy=1,my-ub
      Bx(:,ub+iy,iz)=Bxu(:,iy,iz)                                       ! restore lower bdry B
      By(:,ub+iy,iz)=Byu(:,iy,iz)                                       ! restore lower bdry B
      Bz(:,ub+iy,iz)=Bzu(:,iy,iz)                                       ! restore lower bdry B
     end do
    end do
  end if
  call dumpl(Ex,'efield2:Ex','bdry.dmp',36+ndump*idump)
  call dumpl(Ey,'efield2:Ey','bdry.dmp',37+ndump*idump)
  call dumpl(Ez,'efield2:Ez','bdry.dmp',38+ndump*idump)
END

!-----------------------------------------------------------------------
SUBROUTINE regularize (f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
!
  if (do_trace) print *,'regularize 1:',mpi_rank,omp_mythread,mpi_y
  if (mpi_y ==        0) call symmetric_center_lower (f)
  if (mpi_y == mpi_ny-1) call symmetric_center_upper (f)
  if (do_trace) print *,'regularize 2:',mpi_rank,omp_mythread,mpi_y
END

!-----------------------------------------------------------------------
SUBROUTINE regularize_face (f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
!
  if (mpi_y ==        0) call symmetric_face_lower (f)
  if (mpi_y == mpi_ny-1) call symmetric_face_upper (f)
END

!-----------------------------------------------------------------------
SUBROUTINE ddt_boundary (r,px,py,pz,e,Bx,By,Bz, &
                         drdt,dpxdt,dpydt,dpzdt, &
                         dedt,dBxdt,dBydt,dBzdt, &
                         Ux,Uy,Uz,lnr,p,ee,Cs)
  USE params
  USE arrays, ONLY: scr1,scr2,scr3,scr4,scr5,scr6,wk02,wk06,eeav
  USE forcing
  USE eos
  USE boundary
  USE timeintegration, ONLY: alpha
  implicit none
  real, dimension(mx,my,mz):: r,px,py,pz,e,Bx,By,Bz, &
                              drdt,dpxdt,dpydt,dpzdt, &
                              dedt,dBxdt,dBydt,dBzdt, &
                              Ux,Uy,Uz,lnr,p,ee,Cs
                              
  real ::                     trelaxb, trelaxt, htopi, gam1
  integer ix,iy,iz
  logical omp_in_parallel

  character(len=mid):: id='$Id: stellar-atm_bdry_charbc_omp.f90,v 1.94 2010/01/07 10:04:04 aake Exp $'
  call print_id (id)
  if (do_trace .and. omp_master) print *,'ddt_boundary: begin'

!-------------------------------------------------------------------------
! Characteristic Boundary Conditions
!   Cell (zone) centered variables are defined at j
!   Face centered variables are defined at j-1/2
!   1...2...3...4...5...6...  ... ....n-5...n-4...n-3...n-2...n-1..n  :cell
! 1...2...3...4...5...6...7.. ... .n-5...n-4...n-3...n-2...n-1...n    :face
!   Boundary is at middle of cell lb and ub
!   Bottom is at mx, Top is at 1: y=height (increases downward)
!-------------------------------------------------------------------------
    call barrier_omp ('ddt1')
    call dumpl(r,'ddt1:r','bdry.dmp',16+ndump*idump)
    call dumpl(px,'ddt1:px','bdry.dmp',17+ndump*idump)
    call dumpl(py,'ddt1:py','bdry.dmp',18+ndump*idump)
    call dumpl(pz,'ddt1:pz','bdry.dmp',19+ndump*idump)
    call dumpl(e,'ddt1:e','bdry.dmp',20+ndump*idump)
    call dumpl(dpxdt,'ddt1:dpxdt','bdry.dmp',21+ndump*idump)
    call dumpl(dpydt,'ddt1:dpydt','bdry.dmp',22+ndump*idump)
    call dumpl(dpzdt,'ddt1:dpzdt','bdry.dmp',23+ndump*idump)
    call dumpl(dedt,'ddt1:dedt','bdry.dmp',24+ndump*idump)
    if (do_mhd) then
      call dumpl(Bx,'ddt1:Bx','bdry.dmp',39+ndump*idump)
      call dumpl(By,'ddt1:By','bdry.dmp',40+ndump*idump)
      call dumpl(Bz,'ddt1:Bz','bdry.dmp',41+ndump*idump)
    end if
!-----------------------------------------------------------------------

  do iz = izs, ize
    lnr(:,:,iz)=alog(r(:,:,iz))					! overwritten in pde
    ee(:,:,iz)=e(:,:,iz)/r(:,:,iz)				! overwritten in mhd
  end do

!-----------------------------------------------------------------------
! Cell centered velocities
!-----------------------------------------------------------------------

  call xup_set(Ux, Uxc)
  call yup_set(Uy, Uyc)
  call zup_set(Uz, Uzc)

!-----------------------------------------------------------------------
! Vertical derivative
!-----------------------------------------------------------------------

  call ddydn_set(r, drdy)

  call ddydn_set(p, dpdy)

  call ydn_set(Uxc, scr5)
  call ddyup_set(scr5, dUxdy)

  call ddydn_set(Uyc, dUydy)

  call ydn_set(Uzc, scr6)
  call ddyup_set(scr6, dUzdy)

  call ydn_set(lnr, scr2)

  do iz=izs,ize
    rdn(:,:,iz) = exp(scr2(:,:,iz))
  end do

! call yup_set(drdy, scr3)
! call yup_set(dpdy, scr5)

!----------------------------------------------------------------------
! Wave speeds 
!----------------------------------------------------------------------

  do iz = izs, ize
    Cs(:,:,iz) = sqrt(p(:,:,iz) / r(:,:,iz) * (dlnPdlnr_E(:,:,iz) + &
                 p(:,:,iz) * dlnPdE_r(:,:,iz) / r(:,:,iz))) 
  end do

  call ydn_set(Cs, Csdn)

!----------------------------------------------------------------------
! Wave amplitudes for outgoing charcteristics
!----------------------------------------------------------------------

  do iz = izs, ize
    scr3(:,:,iz)=Csdn(:,:,iz)**2*drdy(:,:,iz)-dpdy(:,:,iz)
  end do
  call yup_set(scr3, scr5)

  do iz = izs, ize
    d1(:,:,iz) = (Uy(:,:,iz) - Csdn(:,:,iz)) * ( &
                 dpdy(:,:,iz) - &
                 rdn(:,:,iz) * Csdn(:,:,iz) * dUydy(:,:,iz))

!   d4(:,:,iz) = Uyc(:,:,iz) * (Cs(:,:,iz)**2 * scr3(:,:,iz) - &
!                scr5(:,:,iz))
    d4(:,:,iz) = Uyc(:,:,iz) * scr5(:,:,iz)

    d3(:,:,iz) = Uyc(:,:,iz) * dUxdy(:,:,iz)

    d6(:,:,iz) = Uyc(:,:,iz) * dUzdy(:,:,iz)

    d8(:,:,iz) = (Uy(:,:,iz) + Csdn(:,:,iz)) * ( &
                 dpdy(:,:,iz) + &
                 rdn(:,:,iz) * Csdn(:,:,iz) * dUydy(:,:,iz))

  end do

!----------------------------------------------------------------------
  if(mpi_y == 0) then
!-----------------------------------------------------------------------
! Top boundary
!-----------------------------------------------------------------------

  if(isubstep == 1) then
    do iz = izs, ize
      drdt_top(:,iz) = 0.0
      dpxdt_top(:,iz) = 0.0
      dpydt_top(:,iz) = 0.0
      dpzdt_top(:,iz) = 0.0
      dedt_top(:,iz) = 0.0
    end do
  else
    do iz = izs, ize
      drdt_top(:,iz) = alpha(isubstep) * drdt_top(:,iz)
      dpxdt_top(:,iz) = alpha(isubstep) * dpxdt_top(:,iz)
      dpydt_top(:,iz) = alpha(isubstep) * dpydt_top(:,iz)
      dpzdt_top(:,iz) = alpha(isubstep) * dpzdt_top(:,iz)
      dedt_top(:,iz) = alpha(isubstep) * dedt_top(:,iz)
    end do
  end if

  call barrier_omp ('ddt2')
  htopi = gy*rav(lb)/pav(lb)
  trelaxt = 1.0 / max((maxval(abs(Uyc(:,lb,:))) / dx + 1.0e-30) , &
            maxval(r(:,lb,:) * Cs(:,lb,:) * gy / p(:,lb,:)) )

!-----------------------------------------------------------------------
! Horizontal derivatives
!-----------------------------------------------------------------------

  call ddxdn32_set(p, lb, dpdx)
  call ddzdn32_set(p, lb, dpdz)
 
  call ddxup32_set(px, lb, divh_ph)
  call ddzup32_add(pz, lb, divh_ph)

  call ddxup32_set(Ux, lb, divh_uh)
  call ddzup32_add(Uz, lb, divh_uh)

  call xdn32_set(lnr, lb, scra)
  call barrier_omp ('ddt3')
  call zdn22_set(scra, scrb)

  call zdn32_set(Ux, lb, scrc)
  call xdn32_set(Uz, lb, scrd)

  call barrier_omp ('ddt4')
  do iz = izs, ize
    scra(:,iz) = exp(scrb(:,iz)) * scrc(:,iz) * scrd(:,iz)
    scre(:,iz) = r(:,lb,iz) * Uxc(:,lb,iz)**2 + p(:,lb,iz)
    scrf(:,iz) = r(:,lb,iz) * Uzc(:,lb,iz)**2 + p(:,lb,iz)
  end do

  call barrier_omp ('ddt5')
  call ddxdn22_set(scre, divh_pxuh)
  call ddzup22_add(scra, divh_pxuh)

  call ddzdn22_set(scrf, divh_pzuh)
  call barrier_omp ('ddt6')
  call ddxup22_add(scra, divh_pzuh)

  call zdn32_set(ee, lb, scrb)
  call xdn32_set(ee, lb, scra)

  do iz = izs, ize
    scra(:,iz) = scra(:,iz) * px(:,lb,iz)
    scrb(:,iz) = scrb(:,iz) * pz(:,lb,iz)
  end do

  call barrier_omp ('ddt7')
  call ddxup22_set(scra, divh_euh)
  call ddzup22_add(scrb, divh_euh)

  call ydn_set(lnr, scr1)
  call xdn32_set(scr1, lb+1, scra)

  call zdn_set(lnr, scr4)
  call barrier_omp ('ddt8')
  call ydn_set(scr4, scr3)

  call ydn_set(Ux, scr1)
  call xdn32_set(Uy, lb+1, scrc)

  call ydn_set(Uz, scr2)
  call zdn32_set(Uy, lb+1, scrd)

  do iz=izs, ize
    scre(:,iz) = exp(scra(:,iz)) * scr1(:,lb+1,iz) * scrc(:,iz)
    scrf(:,iz) = exp(scr3(:,lb+1,iz)) * scr2(:,lb+1,iz) * scrd(:,iz)
  end do

  call barrier_omp ('ddt9')
  call ddxup22_set(scre, divh_pyuh)
  call ddzup22_add(scrf, divh_pyuh)
  call barrier_omp ('ddt10')

!-----------------------------------------------------------------------
! Apply boundary conditions on ingoing characteristics
!-----------------------------------------------------------------------

  do iz = izs , ize
    lnrtop(:,iz)=alog(rtop(:,iz))
    lnrtop(:,iz)=lnrtop(:,iz)+alog(1.-fmtop_frac*dt*htopi*real(py(:,lb+1,iz))/(0.5*(r(:,lb+1,iz)+r(:,lb,iz))))
    rtop(:,iz) = (1.-rtop_frac)*rtop(:,iz) + rtop_frac*exp(lnrtop(:,iz))
!   rtop(:,iz) = (1.-rtop_frac)*rtop(:,iz) + rtop_frac*rav(lb)
!   etop(:,iz) = rtop(:,iz) * eetop
  end do

  do iz = izs, ize
    do iy = 1, my/2
      do ix = 1,mx

! Supersonic inflow, Tend toward local zero mass flux, time scale = H/Cs
      if ((Uy(ix,lb+1,iz) .gt. Csdn(ix,lb+1,iz)) .or. (Uy(ix,lb,iz) .gt. Csdn(ix,lb,iz))) &
        d1(ix,iy,iz) = -rdn(ix,iy,iz) * Csdn(ix,iy,iz) * gy - &
                       10.0 * py(ix,iy,iz) * gy

! All inflow, specify entropy, U_horiz -> 0
      if (Uy(ix,lb+1,iz) .gt. 0.0 .or. Uy(ix,lb,iz) .gt. 0.0) then 
        d4(ix,iy,iz) = - 1.*Cs(ix,iy,iz) * (p(ix,iy,iz)/r(ix,iy,iz)) * dlnPdE_r(ix,iy,iz) * &
                      ( (e(ix,iy,iz) + p(ix,iy,iz)) * &
                      (rtop(ix,iz) - r(ix,lb,iz))/r(ix,iy,iz) - &
                      r(ix,iy,iz)*(eetop - ee(ix,lb,iz)) ) * htopi
!                     (rav(iy) - r(ix,iy,iz))/r(ix,iy,iz) - &
!                     (eav(iy) - e(ix,iy,iz)) ) * htopi

        d3(ix,iy,iz) = Uxc(ix,iy,iz) / trelaxt

        d6(ix,iy,iz) = Uzc(ix,iy,iz) / trelaxt

      end if

! All inflow & subsonic outflow, Tend toward zero mass flux, time scale = H/Cs
      if (Uy(ix,lb+1,iz) .gt. -Csdn(ix,lb+1,iz)) &
!       d8(ix,iy,iz) = rdn(ix,iy,iz) * Csdn(ix,iy,iz) * gy * (1.0 + delta_p(ix,iz))
        d8(ix,iy,iz) = rdn(ix,iy,iz) * Csdn(ix,iy,iz) * gy + &
                      0.5 * py(ix,iy,iz) * gy
      end do
    end do
  end do


!-----------------------------------------------------------------------
! Time derivatives on the boundary
!-----------------------------------------------------------------------

   do iz = izs, ize
     scr1(:,:,iz) = 0.5 * (d1(:,:,iz) + d8(:,:,iz))
   end do

  call ydn_set(d4, scr2)
  call yup_set(scr1, scr3)

  do iz = izs, ize

    scra(:,iz) = (d4(:,lb,iz) + scr3(:,lb,iz)) / Cs(:,lb,iz)**2
    scrb(:,iz) = (scr2(:,lb+1,iz) + scr1(:,lb+1,iz)) / Csdn(:,lb+1,iz)**2
    scre(:,iz) = -r(:,lb,iz) * d3(:,lb,iz) - Uxc(:,lb,iz) * scra(:,iz)
    scrf(:,iz) = -r(:,lb,iz) * d6(:,lb,iz) - Uzc(:,lb,iz) * scra(:,iz)

  end do

  call barrier_omp ('ddt11')
  call xdn22_set(scre, scrc)
  call zdn22_set(scrf, scrd)

  do iz = izs, ize

    drdt_top(:,iz) = drdt_top(:,iz) - scra(:,iz) - divh_ph(:,iz)

    dedt_top(:,iz) = dedt_top(:,iz) + d4(:,lb,iz) * r(:,lb,iz) / (p(:,lb,iz) * dlnPdE_r(:,lb,iz)) - &
                     (e(:,lb,iz) + p(:,lb,iz)) * scra(:,iz) / r(:,lb,iz) - &
                     divh_euh(:,iz) - p(:,lb,iz) * divh_uh(:,iz)

    dpydt_top(:,iz) = dpydt_top(:,iz) - Uy(:,lb+1,iz) * scrb(:,iz) + &
                      0.5 * (d1(:,lb+1,iz) - d8(:,lb+1,iz)) / Csdn(:,lb+1,iz) + &
                      rdn(:,lb+1,iz) * gy - divh_pyuh(:,iz)

    dpxdt_top(:,iz) = dpxdt_top(:,iz) + scrc(:,iz) - divh_pxuh(:,iz)

    dpzdt_top(:,iz) = dpzdt_top(:,iz) + scrd(:,iz) - divh_pzuh(:,iz)

  end do

!-----------------------------------------------------------------------
! Diffusion - horizontal directions
!-----------------------------------------------------------------------
! S_ii = ddiup(U_i)
! S_ij = 0.5*(ddidn(U_j)+ddjdn(U_i))
! T_ii = 2*d_i*(nu+d_i*nud)*S_ii
! nu_ij = exp(idn(jdn(log(nu+.5*(d_i+d_j)*nud))))
! T_ij = (d_i+d_j)*nu_ij*S_ij
! dedt = dedt + sum_i [(T_ii*S_ii)]
! dedt = dedt + sum_ij [(d_i+d_j)*nu*iup(jup(S_ij))^2]
! dp_idt = dp_idt + ddjdn(T_ii) + sum_j [ddjup(T_ij)]
!-----------------------------------------------------------------------

  call barrier_omp ('ddt12')
  call ddxup32_set(Ux, lb, scra)

  do iz = izs, ize
    do ix = 1, mx
      scrb(ix,iz) = 2.0 * dxm(ix) * (wk06(ix,lb,iz) + dxm(ix) * wk02(ix,lb,iz)) * scra(ix,iz)
    end do
  end do

  call ddxdn22_set(scrb, scrc)

  do iz = izs, ize
    dpxdt_top(:,iz) = dpxdt_top(:,iz) + scrc(:,iz)
    dedt_top(:,iz) = dedt_top(:,iz) + scra(:,iz) * scrb(:,iz)
  end do
    
! call ddyup_set(Uy, scr1)

! do iz = izs, ize
!   do iy = 1, my
!     scr2(:,iy,iz) = 2.0 * dym(iy) * (wk06(:,iy,iz) + dym(iy) * wk02(:,iy,iz)) * scr1(:,iy,iz)
!   end do
! end do

! call ddydn_set(scr2, scr3)

! do iz = izs, ize
!   dpydt_top(:,iz) = dpydt_top(:,iz) + scr3(:,lb+1,iz)
!   dedt_top(:,iz) = dedt_top(:,iz) + scr1(:,lb,iz) * scr2(:,lb,iz)
! end do
    
  call ddzup32_set(Uz, lb, scra)

  do iz = izs, ize
      scrb(:,iz) = 2.0 * dzm(iz) * (wk06(:,lb,iz) + dzm(iz) * wk02(:,lb,iz)) * scra(:,iz)
  end do

  call barrier_omp ('ddt13')
  call ddzdn22_set(scrb, scrc)
  call barrier_omp ('ddt14')

  do iz = izs, ize
    dpzdt_top(:,iz) = dpzdt_top(:,iz) + scrc(:,iz)
    dedt_top(:,iz) = dedt_top(:,iz) + scra(:,iz) * scrb(:,iz)
  end do

  call ddxdn32_set(Uz, lb, scra)
  call ddzdn32_add(Ux, lb, scra)

  do iz = izs, ize
    do ix =1, mx
        scrb(ix,iz) = alog(wk06(ix,lb,iz) + 0.5 * (dxm(ix) + dzm(iz)) * wk02(ix,lb,iz))
    end do
  end do

  call xdn22_set(scrb, scrc)
  call barrier_omp ('ddt15')
  call zdn22_set(scrc, scrd)

  do iz = izs, ize
    do ix = 1, mx
      scrd(ix,iz) = 0.5 * (dxm(ix) + dzm(iz)) * exp(scrd(ix,iz)) * scra(ix,iz)
    end do
  end do

  call barrier_omp ('ddt16')
  call ddxup22_set(scrd, scre)
  call ddzup22_set(scrd, scrf)

  do iz = izs, ize
    dpxdt_top(:,iz) = dpxdt_top(:,iz) + scrf(:,iz)
    dpzdt_top(:,iz) = dpzdt_top(:,iz) + scre(:,iz)
  end do

  do iz = izs, ize
    scrb(:,iz) = 0.25 * scra(:,iz)**2
  end do

  call xup22_set(scrb, scrc)
  call barrier_omp ('ddt17')
  call zup22_set(scrc, scrd)

  do iz = izs, ize
    do ix = 1, mx
      dedt_top(ix,iz) = dedt_top(ix,iz) + 2.0 * (dxm(ix) + dzm(iz)) * wk06(ix,lb,iz) * scrd(ix,iz)
    end do
  end do

  call ddxdn_set(Uy, scr1)
! call ddydn_add(Ux, scr1)

  do iz = izs, ize
    do ix = 1, mx
      do iy = 1, my
        scr2(ix,iy,iz) = alog(wk06(ix,iy,iz) + 0.5 * (dxm(ix) + dym(iy)) * wk02(ix,iy,iz))
      end do
    end do
  end do

  call ydn_set(scr2, scr3)
  call xdn32_set(scr3,lb+1,scra)

  do iz = izs, ize
    do ix = 1, mx
      scra(ix,iz) = (dxm(ix) + dym(lb+1)) * exp(scra(ix,iz)) * scr1(ix,lb+1,iz)
    end do
  end do

  call ddxup22_set(scra, scrb)

  do iz = izs, ize
    dpydt_top(:,iz) = dpydt_top(:,iz) + scrb(:,iz)
  end do

  do iz = izs, ize
    scr2(:,:,iz) = scr1(:,:,iz)**2
  end do

  call yup_set(scr2, scr3)
  call xup32_set(scr3, lb, scra)

  do iz = izs, ize
    do ix = 1, mx
      dedt_top(ix,iz) = dedt_top(ix,iz) + 2.0 * (dxm(ix) + dym(lb)) * wk06(ix,lb,iz) * scra(ix,iz)
    end do
  end do

  call ddzdn_set(Uy, scr1)
! call barrier_omp ('ddt18')
! call ddydn_add(Uz, scr1)

  do iz = izs, ize
    do iy = 1, my
      scr2(:,iy,iz) = alog(wk06(:,iy,iz) + 0.5 * (dym(iy) + dzm(iz)) * wk02(:,iy,iz))
    end do
  end do

  call ydn_set(scr2, scr3)
  call barrier_omp ('ddt19')
  call zdn32_set(scr3, lb+1, scra)

  do iz = izs, ize
    scra(:,iz) = (dym(lb+1) + dzm(iz)) * exp(scra(:,iz)) * scr1(:,lb+1,iz)
  end do

  call barrier_omp ('ddt20')
  call ddzup22_set(scra, scrb)

  do iz = izs, ize
    dpydt_top(:,iz) = dpydt_top(:,iz) + scrb(:,iz)
  end do

  do iz = izs, ize
    scr2(:,:,iz) = scr1(:,:,iz)**2
  end do

  call yup_set(scr2, scr3)
  call barrier_omp ('ddt21')
  call zup32_set(scr3, lb, scra)

  do iz = izs, ize
    dedt_top(:,iz) = dedt_top(:,iz) + 2.0 * (dym(lb) + dzm(iz)) * wk06(:,lb,iz) * scra(:,iz)
  end do

!-----------------------------------------------------------------------
! Limit inflows to sonic
!-----------------------------------------------------------------------
  call haverage_subr (Cs,Csav)				! mass flux
  do iz = izs, ize
    do ix=1,mx
      if ((Uy(ix,lb+1,iz) .gt. Csav(lb+1) .or. Uy(ix,lb,iz) .gt. Csav(lb)) .and. pdamp .gt. 0.) &
        dpydt_top(ix,iz) = (min(Csav(lb+1)*rav(lb+1),py(ix,lb+2,iz)) - py(ix,lb+1,iz))/(pdamp*dt)				! limit inflows to sonic

!     if (py(ix,lb+1,iz) > 0. .or. py(ix,lb,iz) > 0.) &			! for inflows ..
!       dpydt_top(ix,iz) = dpydt_top(ix,iz) &				! let the velocity
!         - htopi*py(ix,lb+1,iz)*abs(py(ix,lb+1,iz))/(0.5*(r(ix,lb,iz)+r(ix,lb+1,iz)))   ! tend to zero --- but so
    end do
  end do

  do iz = izs, ize
    drdt(:,lb,iz) = drdt_top(:,iz)
    dpxdt(:,lb,iz) = dpxdt_top(:,iz)
    dpydt(:,lb+1,iz) = dpydt_top(:,iz)
    dpzdt(:,lb,iz) = dpzdt_top(:,iz)
    dedt(:,lb,iz) = dedt_top(:,iz)
  end do
  
!-----------------------------------------------------------------------
! Ghost zones
!-----------------------------------------------------------------------

  do iz=izs,ize
    do ix=1,mx
      drdt (ix,1:lb-1,iz) = 0.
      dpxdt(ix,1:lb-1,iz) = 0.
      dpydt(ix,1:lb  ,iz) = 0.
      dpzdt(ix,1:lb-1,iz) = 0.
      dedt (ix,1:lb-1,iz) = 0.
      if (do_mhd) then
        dbxdt(ix,1:2,iz)=0.
        dbydt(ix,1:2,iz)=0.
        dbzdt(ix,1:2,iz)=0.
      endif
    end do
  end do

  end if 

!----------------------------------------------------------------------
  if(mpi_y == mpi_ny - 1) then
!-----------------------------------------------------------------------
! Bottom boundary
!-----------------------------------------------------------------------

  if(isubstep == 1) then
    do iz = izs, ize
      drdt_bot(:,iz) = 0.0
      dpxdt_bot(:,iz) = 0.0
      dpydt_bot(:,iz) = 0.0
      dpzdt_bot(:,iz) = 0.0
      dedt_bot(:,iz) = 0.0
    end do
  else
    do iz = izs, ize
      drdt_bot(:,iz) = alpha(isubstep) * drdt_bot(:,iz)
      dpxdt_bot(:,iz) = alpha(isubstep) * dpxdt_bot(:,iz)
      dpydt_bot(:,iz) = alpha(isubstep) * dpydt_bot(:,iz)
      dpzdt_bot(:,iz) = alpha(isubstep) * dpzdt_bot(:,iz)
      dedt_bot(:,iz) = alpha(isubstep) * dedt_bot(:,iz)
    end do
  end if

  trelaxb = 1.0 / max(1.0 * (maxval(abs(Uyc(:,ub,:))) / dx + 1.0e-30) , &
            1.0 * maxval(r(:,ub,:) * Cs(:,ub,:) * gy / p(:,ub,:)) )

  do iz = izs, ize
    delta_p(:,iz) = 0.38 * (e(:,ub,iz) / ebot - 1.0)
!   delta_p(:,iz) = 5.00 * (e(:,ub,iz) / ebot - 1.0)
  end do
 
!-----------------------------------------------------------------------
! Horizontal derivatives
!-----------------------------------------------------------------------

  call ddxdn32_set(p, ub, dpdx)
  call ddzdn32_set(p, ub, dpdz)

  call ddxup32_set(px, ub, divh_ph)
  call ddzup32_add(pz, ub, divh_ph)

  call ddxup32_set(Ux, ub, divh_uh)
  call ddzup32_add(Uz, ub, divh_uh)

  call xdn32_set(lnr, ub, scra)
  call barrier_omp ('ddt22')
  call zdn22_set(scra, scrb)

  call zdn32_set(Ux, ub, scrc)
  call xdn32_set(Uz, ub, scrd)

  call barrier_omp ('ddt23')
  do iz = izs, ize
    scra(:,iz) = exp(scrb(:,iz)) * scrc(:,iz) * scrd(:,iz)
    scre(:,iz) = r(:,ub,iz) * Uxc(:,ub,iz)**2 + p(:,ub,iz)
    scrf(:,iz) = r(:,ub,iz) * Uzc(:,ub,iz)**2 + p(:,ub,iz)
  end do

  call barrier_omp ('ddt24')
  call ddxdn22_set(scre, divh_pxuh)
  call ddzup22_add(scra, divh_pxuh)

  call ddzdn22_set(scrf, divh_pzuh)
  call barrier_omp ('ddt25')
  call ddxup22_add(scra, divh_pzuh)

  call zdn32_set(ee, ub, scrb)
  call xdn32_set(ee, ub, scra)

  do iz = izs, ize
    scra(:,iz) = scra(:,iz) * px(:,ub,iz)
    scrb(:,iz) = scrb(:,iz) * pz(:,ub,iz)
  end do

  call barrier_omp ('ddt26')
  call ddxup22_set(scra, divh_euh)
  call ddzup22_add(scrb, divh_euh)

  call ydn_set(lnr, scr1)
  call xdn32_set(scr1, ub, scra)

  call zdn_set(lnr, scr4)
  call barrier_omp ('ddt27')
  call ydn_set(scr4, scr3)

  call ydn_set(Ux, scr1)
  call xdn32_set(Uy, ub, scrc)

  call ydn_set(Uz, scr2)
  call zdn32_set(Uy, ub, scrd)

  do iz=izs, ize
    scre(:,iz) = exp(scra(:,iz)) * scr1(:,ub,iz) * scrc(:,iz)
    scrf(:,iz) = exp(scr3(:,ub,iz)) * scr2(:,ub,iz) * scrd(:,iz)
  end do

  call barrier_omp ('ddt28')
  call ddxup22_set(scre, divh_pyuh)
  call ddzup22_add(scrf, divh_pyuh)
  call barrier_omp ('ddt29')

!-----------------------------------------------------------------------
! Apply boundary conditions on ingoing characteristics
! Assume always subsonic, not necessarily sub-Alfvenic
!-----------------------------------------------------------------------

  do iz = izs, ize
    do iy = my/2 + 1, my
      do ix = 1,mx

! Incoming (subsonic) characteristic, Tend toward initial pressure
        if(Uy(ix,ub,iz) .lt. Csdn(ix,ub,iz)) &
          d1(ix,iy,iz) = -rdn(ix,iy,iz) * Csdn(ix,iy,iz) * gy * (1.0 - delta_p(ix,iz)) !+ &
!                      5. * rdn(ix,iy,iz) * Uy(ix,iy,iz) * gy

! All inflow, fix entropy, U_horiz -> 0
        if(Uy(ix,ub,iz) .lt. 0.0) then

           d4(ix,iy,iz) = - Cs(ix,iy,iz) * (p(ix,iy,iz)/r(ix,iy,iz)) * dlnPdE_r(ix,iy,iz) * &
                          ( (e(ix,iy,iz)+p(ix,iy,iz)) * (rbot-r(ix,iy,iz))/r(ix,iy,iz) - &
                          (ebot-e(ix,iy,iz)) ) * (rav(iy)*gy/pav(iy))
!                         ( (e(ix,iy,iz)+p(ix,iy,iz)) * (rbot-r(ix,iy,iz))/r(ix,iy,iz) ) * &
!                         (rav(iy)*gy/pav(iy))

          d3(ix,iy,iz) = Uxc(ix,iy,iz) / trelaxb
          d6(ix,iy,iz) = Uzc(ix,iy,iz) / trelaxb

        end if

!        if(Uy(ix,ub,iz) .lt. -Csdn(ix,ub,iz)) &
!          d8(ix,iy,iz) = rdn(ix,iy,iz) * Csdn(ix,iy,iz) * gy * (1.0 + delta_p(ix,iz)) !- &
!                      5. * py(ix,iy,iz) * gy

      end do
    end do
  end do

!-----------------------------------------------------------------------
! Time derivatives on the boundary
!-----------------------------------------------------------------------

  do iz = izs, ize
    scr1(:,:,iz) = 0.5 * (d1(:,:,iz) + d8(:,:,iz))
  end do

  call ydn_set(d4, scr2)
  call yup_set(scr1, scr3)

  do iz = izs, ize

    scra(:,iz) = (d4(:,ub,iz) + scr3(:,ub,iz)) / Cs(:,ub,iz)**2
    scrb(:,iz) = (scr2(:,ub,iz) + scr1(:,ub,iz)) / Csdn(:,ub,iz)**2
    scre(:,iz) = -r(:,ub,iz) * d3(:,ub,iz) - Uxc(:,ub,iz) * scra(:,iz)
    scrf(:,iz) = -r(:,ub,iz) * d6(:,ub,iz) - Uzc(:,ub,iz) * scra(:,iz)

  end do

  call barrier_omp ('ddt30')
  call xdn22_set(scre, scrc)
  call zdn22_set(scrf, scrd)

  do iz = izs, ize

    drdt_bot(:,iz) = drdt_bot(:,iz) - scra(:,iz) - divh_ph(:,iz)

    dedt_bot(:,iz) = dedt_bot(:,iz) + d4(:,ub,iz) * r(:,ub,iz) / (p(:,ub,iz) * dlnPdE_r(:,ub,iz)) - &
                     (e(:,ub,iz) + p(:,ub,iz)) * scra(:,iz) / r(:,ub,iz) - &
                     divh_euh(:,iz) - p(:,ub,iz) * divh_uh(:,iz)

    dpydt_bot(:,iz) = dpydt_bot(:,iz) - Uy(:,ub,iz) * scrb(:,iz) + &
                      0.5 * (d1(:,ub,iz) - d8(:,ub,iz)) / Csdn(:,ub,iz) + &
                      rdn(:,ub,iz) * gy - divh_pyuh(:,iz)

    dpxdt_bot(:,iz) = dpxdt_bot(:,iz) + scrc(:,iz) - divh_pxuh(:,iz)

    dpzdt_bot(:,iz) = dpzdt_bot(:,iz) + scrd(:,iz) - divh_pzuh(:,iz)

  end do

!-----------------------------------------------------------------------
! Diffusion - horizontal directions
!-----------------------------------------------------------------------
! S_ii = ddiup(U_i)
! S_ij = 0.5*(ddidn(U_j)+ddjdn(U_i))
! T_ii = 2*d_i*(nu+d_i*nud)*S_ii
! nu_ij = exp(idn(jdn(log(nu+.5*(d_i+d_j)*nud))))
! T_ij = (d_i+d_j)*nu_ij*S_ij
! dedt = dedt + sum_i [(T_ii*S_ii)]
! dedt = dedt + sum_ij [(d_i+d_j)*nu*iup(jup(S_ij))^2]
! dp_idt = dp_idt + ddjdn(T_ii) + sum_j [ddjup(T_ij)]
!-----------------------------------------------------------------------

  call barrier_omp ('ddt31')
  call ddxup32_set(Ux, ub, scra)

  do iz = izs, ize
    do ix = 1, mx
      scrb(ix,iz) = 2.0 * dxm(ix) * (wk06(ix,ub,iz) + dxm(ix) * wk02(ix,ub,iz)) * scra(ix,iz)
    end do
  end do

  call ddxdn22_set(scrb, scrc)

  do iz = izs, ize
    dpxdt_bot(:,iz) = dpxdt_bot(:,iz) + scrc(:,iz)
    dedt_bot(:,iz) = dedt_bot(:,iz) + scra(:,iz) * scrb(:,iz)
  end do
    
  call ddzup32_set(Uz, ub, scra)

  do iz = izs, ize
      scrb(:,iz) = 2.0 * dzm(iz) * (wk06(:,ub,iz) + dzm(iz) * wk02(:,ub,iz)) * scra(:,iz)
  end do

  call barrier_omp ('ddt32')
  call ddzdn22_set(scrb, scrc)
  call barrier_omp ('ddt33')

  do iz = izs, ize
    dpzdt_bot(:,iz) = dpzdt_bot(:,iz) + scrc(:,iz)
    dedt_bot(:,iz) = dedt_bot(:,iz) + scra(:,iz) * scrb(:,iz)
  end do

  call ddxdn32_set(Uz, ub, scra)
  call ddzdn32_add(Ux, ub, scra)

  do iz = izs, ize
    do ix =1, mx
        scrb(ix,iz) = alog(wk06(ix,ub,iz) + 0.5 * (dxm(ix) + dzm(iz)) * wk02(ix,ub,iz))
    end do
  end do

  call xdn22_set(scrb, scrc)
  call barrier_omp ('ddt34')
  call zdn22_set(scrc, scrd)

  do iz = izs, ize
    do ix = 1, mx
      scrd(ix,iz) = 0.5 * (dxm(ix) + dzm(iz)) * exp(scrd(ix,iz)) * scra(ix,iz)
    end do
  end do

  call barrier_omp ('ddt35')
  call ddxup22_set(scrd, scre)
  call ddzup22_set(scrd, scrf)

  do iz = izs, ize
    dpxdt_bot(:,iz) = dpxdt_bot(:,iz) + scrf(:,iz)
    dpzdt_bot(:,iz) = dpzdt_bot(:,iz) + scre(:,iz)
  end do

  do iz = izs, ize
    scrb(:,iz) = 0.25 * scra(:,iz)**2
  end do

  call xup22_set(scrb, scrc)
  call barrier_omp ('ddt36')
  call zup22_set(scrc, scrd)

  do iz = izs, ize
    do ix = 1, mx
      dedt_bot(ix,iz) = dedt_bot(ix,iz) + 2.0 * (dxm(ix) + dzm(iz)) * wk06(ix,ub,iz) * scrd(ix,iz)
    end do
  end do

  call ddxdn_set(Uy, scr1)

  do iz = izs, ize
    do ix = 1, mx
      do iy = 1, my
        scr2(ix,iy,iz) = alog(wk06(ix,iy,iz) + 0.5 * (dxm(ix) + dym(iy)) * wk02(ix,iy,iz))
      end do
    end do
  end do

  call ydn_set(scr2, scr3)
  call xdn32_set(scr3, ub, scra)

  do iz = izs, ize
    do ix = 1, mx
      scra(ix,iz) = (dxm(ix) + dym(ub)) * exp(scra(ix,iz)) * scr1(ix,ub,iz)
    end do
  end do

  call ddxup22_set(scra, scrb)

  do iz = izs, ize
    dpydt_bot(:,iz) = dpydt_bot(:,iz) + scrb(:,iz)
  end do

  do iz = izs, ize
    scr2(:,:,iz) = scr1(:,:,iz)**2
  end do

  call yup_set(scr2, scr3)
  call xup32_set(scr3, ub, scra)

  do iz = izs, ize
    do ix = 1, mx
      dedt_bot(ix,iz) = dedt_bot(ix,iz) + 2.0 * (dxm(ix) + dym(ub)) * wk06(ix,ub,iz) * scra(ix,iz)
    end do
  end do

  call ddzdn_set(Uy, scr1)

  do iz = izs, ize
    do iy = 1, my
      scr2(:,iy,iz) = alog(wk06(:,iy,iz) + 0.5 * (dym(iy) + dzm(iz)) * wk02(:,iy,iz))
    end do
  end do

  call ydn_set(scr2, scr3)
  call barrier_omp ('ddt37')
  call zdn32_set(scr3, ub, scra)

  do iz = izs, ize
    scra(:,iz) = (dym(ub) + dzm(iz)) * exp(scra(:,iz)) * scr1(:,ub,iz)
  end do

  call barrier_omp ('ddt38')
  call ddzup22_set(scra, scrb)

  do iz = izs, ize
    dpydt_bot(:,iz) = dpydt_bot(:,iz) + scrb(:,iz)
  end do

  do iz = izs, ize
    scr2(:,:,iz) = scr1(:,:,iz)**2
  end do

  call yup_set(scr2, scr3)
  call barrier_omp ('ddt39')
  call zup32_set(scr3, ub, scra)

  do iz = izs, ize
    dedt_bot(:,iz) = dedt_bot(:,iz) + 2.0 * (dym(ub) + dzm(iz)) * wk06(:,ub,iz) * scra(:,iz)
  end do

  do iz = izs, ize
    drdt(:,ub,iz) = drdt_bot(:,iz)
    dpxdt(:,ub,iz) = dpxdt_bot(:,iz)
    dpydt(:,ub,iz) = dpydt_bot(:,iz)
    dpzdt(:,ub,iz) = dpzdt_bot(:,iz)
    dedt(:,ub,iz) = dedt_bot(:,iz)
  end do
  end if

!-----------------------------------------------------------------------
! damp vertical waves from bottom boundary
!-----------------------------------------------------------------------
  if (pdamp .gt. 0. .and. lb > 1) then
    call haverage_subr (py,pyav)				! mass flux
    do iz = izs, ize
!     do iy=1,my
!       dpydt(:,iy,iz)=dpydt(:,iy,iz)-pyav(iy)/(pdamp*trelaxt)
!     end do
      do iy=lb,min(2*lb-1,ub)
        dpxdt(:,iy,iz)=dpxdt(:,iy,iz)-px(:,iy,iz)*real(2*lb-iy)/(pdamp*trelaxt)
        dpydt(:,iy,iz)=dpydt(:,iy,iz)-py(:,iy,iz)*real(2*lb-iy)/(pdamp*trelaxt)
        dpzdt(:,iy,iz)=dpzdt(:,iy,iz)-pz(:,iy,iz)*real(2*lb-iy)/(pdamp*trelaxt)
      end do
    end do
  end if

!-----------------------------------------------------------------------

  call barrier_omp ('ddt40')
  call dumpl(drdt,'ddt2:drdt','bdry.dmp',25+ndump*idump)
  call dumpl(dpxdt,'ddt2:dpxdt','bdry.dmp',26+ndump*idump)
  call dumpl(dpydt,'ddt2:dpydt','bdry.dmp',27+ndump*idump)
  call dumpl(dpzdt,'ddt2:dpzdt','bdry.dmp',28+ndump*idump)
  call dumpl(dedt,'ddt2:dedt','bdry.dmp',29+ndump*idump)
  if (do_mhd) then
    call dumpl(dBxdt,'ddt2:dBxdt','bdry.dmp',42+ndump*idump)
    call dumpl(dBydt,'ddt2:dBydt','bdry.dmp',43+ndump*idump)
    call dumpl(dBzdt,'ddt2:dBzdt','bdry.dmp',44+ndump*idump)
  end if
!$omp single
    idump=idump+1
!$omp end single
END
