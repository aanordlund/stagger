! $Id: noboundary.f90,v 1.20 2009/08/03 10:50:23 aake Exp $
MODULE boundary
  logical:: do_bondary=.false.
  logical:: do_eta=.false.
  real:: eta_scl=1.0
  integer:: eta_width=1
END MODULE

!-----------------------------------------------------------------------
SUBROUTINE init_boundary
  USE params
  USE variables
  implicit none
  character(len=mid):: id="$Id: noboundary.f90,v 1.20 2009/08/03 10:50:23 aake Exp $"
  call print_id (id)
  lb = 1
  ub = my
  Uy(:,:,:)=1.0
  
!
END

!-----------------------------------------------------------------------
SUBROUTINE read_boundary
  USE params
  implicit none
!
END

!-----------------------------------------------------------------------
SUBROUTINE density_boundary (r,lnr,py,e)
  USE params
  implicit none
  real, dimension(mx,my,mz):: r,lnr,py,e
!
END

!-----------------------------------------------------------------------
SUBROUTINE density_boundary_log (lnr,r)
  USE params
  implicit none
  real, dimension(mx,my,mz):: lnr,r
!
END

!-----------------------------------------------------------------------
SUBROUTINE energy_boundary (r,lnr,Ux,Uy,Uz,e,ee,p,Bx,By,Bz)
  USE params
  implicit none
  real, dimension(mx,my,mz):: r,lnr,Ux,Uy,Uz,e,ee,p,Bx,By,Bz

END

!-----------------------------------------------------------------------
SUBROUTINE passive_boundary (r,Ux,Uy,Uz,d,dd)
  USE params
  implicit none
  real, dimension(mx,my,mz):: r,Ux,Uy,Uz,d,dd

END

!-----------------------------------------------------------------------
SUBROUTINE velocity_boundary (r,xdnr,ydnr,zdnr,Ux,Uy,Uz,px,py,pz,e,Bx,By,Bz)
  USE params
  implicit none
  real, dimension(mx,my,mz):: r,xdnr,ydnr,zdnr,Ux,Uy,Uz,px,py,pz,e,Bx,By,Bz
  real uymax,uymin
  Uy = 1.0
  py=Uy*ydnr
!
!call fminval_subr('uy',py/ydnr,uymin)
!call fmaxval_subr('uy',py/ydnr,uymax)
!print *,'vel_bdry: uy min max = ',uymin,uymax
END

!-----------------------------------------------------------------------
SUBROUTINE viscosity_boundary (nu, scr)
  USE params
  implicit none
  real, dimension(mx,my,mz):: nu, scr
!
END

!-----------------------------------------------------------------------
SUBROUTINE mfield_boundary (Ux,Uy,Uz,Bx,By,Bz)
  USE params
  implicit none
  real, dimension(mx,my,mz):: Ux,Uy,Uz,Bx,By,Bz
!
END

!-----------------------------------------------------------------------
SUBROUTINE ecurrent_boundary (Ex, Ey, Ez, Jx, Jy, Jz)
  USE params
  implicit none
  real, dimension(mx,my,mz):: Ex, Ey, Ez, Jx, Jy, Jz
!
END

!-----------------------------------------------------------------------
SUBROUTINE  efield_boundary (Ex, Ey, Ez, Bx_y, Bx_z, By_z, By_x, Bz_x, Bz_y, &
                             Rx, Ry, Rz, Ux_y, Ux_z, Uy_z, Uy_x, Uz_x, Uz_y, &
                             Ux, Uy, Uz)
  USE params
  implicit none
  real, dimension(mx,my,mz):: Ex, Ey, Ez, Bx_y, Bx_z, By_z, By_x, Bz_x, Bz_y, &
                              Rx, Ry, Rz, Ux_y, Ux_z, Uy_z, Uy_x, Uz_x, Uz_y, &
                              Ux, Uy, Uz
!
END

!-----------------------------------------------------------------------
SUBROUTINE ddt_boundary (r,px,py,pz,e,p,Bx,By,Bz, &
                         drdt,dpxdt,dpydt,dpzdt, &
                         dedt,dBxdt,dBydt,dBzdt)
  USE params
  implicit none
  real, dimension(mx,my,mz):: r,px,py,pz,e,p,Bx,By,Bz, &
                              drdt,dpxdt,dpydt,dpzdt, &
                              dedt,dBxdt,dBydt,dBzdt
real uymax,uymin
!
!call fminval_subr('uy',py/r,uymin)
!call fmaxval_subr('uy',py/r,uymax)
!print *,'ddt_bdry: uy min max = ',uymin,uymax
END

!-----------------------------------------------------------------------
SUBROUTINE E_driver (e1,e2)
  USE params
  implicit none
  real, dimension(mx,my,mz):: e1,e2
!
END

!-----------------------------------------------------------------------
SUBROUTINE regularize (f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
!
END

!-----------------------------------------------------------------------
SUBROUTINE regularize_face (f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
!
END

!-----------------------------------------------------------------------
SUBROUTINE magnetic_pressure_boundary (f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
!
END

