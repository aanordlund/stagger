! $Id: rigid_y.f90,v 1.8 2005/04/05 17:12:07 troels_h Exp $

!-----------------------------------------------------------------------
SUBROUTINE init_boundary
  USE params
  implicit none
  lb = 6
  ub = my-5
!
END

!-----------------------------------------------------------------------
SUBROUTINE read_boundary
  USE params
  implicit none
!
END

!-----------------------------------------------------------------------
SUBROUTINE density_boundary (r)
  USE params
  implicit none
  real, dimension(mx,my,mz):: r
!
END

!-----------------------------------------------------------------------
SUBROUTINE density_boundary_log (lnr,r)
  USE params
  implicit none
  real, dimension(mx,my,mz):: lnr,r
!
END

!-----------------------------------------------------------------------
SUBROUTINE energy_boundary (r,lnr,Ux,Uy,Uz,e,ee,p,Bx,By,Bz)
  USE params
  implicit none
  real, dimension(mx,my,mz):: r,lnr,Ux,Uy,Uz,e,ee,p,Bx,By,Bz

END

!-----------------------------------------------------------------------
SUBROUTINE passive_boundary (r,Ux,Uy,Uz,d,dd)
  USE params
  implicit none
  real, dimension(mx,my,mz):: r,Ux,Uy,Uz,d,dd

END

!-----------------------------------------------------------------------
SUBROUTINE velocity_boundary (r,xdnr,ydnr,zdnr,Ux,Uy,Uz,px,py,pz,e,Bx,By,Bz)
  USE params
  implicit none
  real, dimension(mx,my,mz):: r,xdnr,ydnr,zdnr,Ux,Uy,Uz,px,py,pz,e,ee,Bx,By,Bz
  integer :: iy,llb,uub,iz
!
  llb= max(lb,6)
  uub= min(ub,my-5)
  do iz=1,mz
  do iy=1,llb-1
    Ux(:,iy,iz) = Ux(:,llb,iz)
    Uy(:,iy,iz) = Uy(:,llb,iz)
    Uz(:,iy,iz) = Uz(:,llb,iz)
  end do
  do iy=uub+1,my
    Ux(:,iy,iz) = Ux(:,uub,iz)
    Uy(:,iy,iz) = Uy(:,uub,iz)
    Uz(:,iy,iz) = Uz(:,uub,iz)
  end do
  end do
!
END

!-----------------------------------------------------------------------
SUBROUTINE mfield_boundary (Ux,Uy,Uz,Bx,By,Bz)
  USE params
  implicit none
  real, dimension(mx,my,mz):: Ux,Uy,Uz,Bx,By,Bz
!
END

!-----------------------------------------------------------------------
SUBROUTINE efield_boundary (Ex, Ey, Ez, Bx_y, Bx_z, By_z, By_x, Bz_x, Bz_y, &
                            Rx, Ry, Rz, Ux_y, Ux_z, Uy_z, Uy_x, Uz_x, Uz_y, &
                            Ux, Uy, Uz)
  USE params
  implicit none
  real, dimension(mx,my,mz):: Ex, Ey, Ez, Bx_y, Bx_z, By_z, By_x, Bz_x, Bz_y, &
                              Ux_y, Ux_z, Uy_z, Uy_x, Uz_x, Uz_y, Ux, Uy, Uz
  real, dimension(mx,2, mz):: Rx, Ry, Rz
!
END

!-----------------------------------------------------------------------
SUBROUTINE ddt_boundary (r,px,py,pz,e,Bx,By,Bz, &
                         drdt,dpxdt,dpydt,dpzdt, &
                         dedt,dBxdt,dBydt,dBzdt)
  USE params
  implicit none
  real, dimension(mx,my,mz):: r,px,py,pz,e,Bx,By,Bz, &
                              drdt,dpxdt,dpydt,dpzdt, &
                              dedt,dBxdt,dBydt,dBzdt
  integer :: ll,lbb,ubb,iy,iz,zz,damp,damping
  real*8    :: lpyav,upyav,f
  lbb = lb
  ubb = ub
!
  do iz=1,mz
    do iy=1,lbb
      drdt(:,iy,iz)  = 0.
      dpxdt(:,iy,iz) = 0.
      dpydt(:,iy,iz) = 0.
      dpzdt(:,iy,iz) = 0.
      dedt(:,iy,iz)  = 0.
    enddo
    do iy=ubb,my
      drdt(:,iy,iz)  = 0.
      dpxdt(:,iy,iz) = 0.
      dpzdt(:,iy,iz) = 0.
      dedt(:,iy,iz)  = 0.
    enddo
    do iy=ubb+1,my          ! Remember we are on staggered grid!
      dpydt(:,iy,iz) = 0.
    enddo
  enddo
  if (do_mhd) then
    do iz=1,mz
      do iy=1,lbb
        dBxdt(:,iy,iz) = 0.
        dBydt(:,iy,iz) = 0.
        dBzdt(:,iy,iz) = 0.
      enddo
      do iy=ubb,my
        dBxdt(:,iy,iz) = 0.
        dBzdt(:,iy,iz) = 0.
      enddo
      do iy=ubb+1,my        ! Remember we are on staggered grid!
        dBydt(:,iy,iz) = 0.
      enddo
    enddo
  endif
END

!-----------------------------------------------------------------------
SUBROUTINE regularize (f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
  integer :: lbb,ubb,iz,iy
!
  lbb=lb
  ubb=ub
  do iz=1,mz
  do iy=1,lbb
    f(:,iy,iz) = f(:,lbb+1,iz)
  enddo
  do iy=ubb,my
    f(:,iy,iz) = f(:,ubb-1,iz)
  enddo
  enddo
!
END

!-----------------------------------------------------------------------
SUBROUTINE regularize_face (f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
!
END
