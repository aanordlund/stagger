!-----------------------------------------------------------------------
MODULE boundary_driving_m
  integer,parameter :: gg=3
  real,parameter :: ldif=2.0
  real :: tvr, vrms
  real,dimension(gg),save :: amplarr,granrarr,life_tarr
  integer,dimension(gg),save :: zrangearr,xrangearr
  integer :: k,ix,iz
!  type(point),pointer,save :: scrp1,scrp2,scrp3
END

!-----------------------------------------------------------------------
SUBROUTINE boundary_driving (v_ampl,Ux,Uy,Uz)

  USE params
  USE boundary_driving_m
  USE points
  implicit none
  real v_ampl
  real, dimension(mx,my,mz):: Ux,Uy,Uz

!!***********************************************************
! Written here for 3 levels, can in theory be expanded 
! to more levels.
!!***********************************************************

!$omp single
  if (.not.associated(firstlev)) then
     granrarr(1)=granr
     granrarr(2)=granr*ldif
     granrarr(3)=granr*ldif*ldif
     
     amplarr(1)=1.
     amplarr(2)=1./ldif
     amplarr(3)=1./(ldif*ldif)
     
     life_tarr(1)=life_t
     life_tarr(2)=ldif**2*life_t
     life_tarr(3)=ldif**4*life_t
     
     zrangearr(1)=zrange
     zrangearr(2)=min(nint(ldif*zrange),nint(mz/2.-1.))
     zrangearr(3)=min(nint(ldif*ldif*zrange),nint(mz/2.-1.))
     xrangearr(1)=xrange
     xrangearr(2)=min(nint(ldif*xrange),nint(mx/2-1.))
     xrangearr(3)=min(nint(ldif*ldif*xrange),nint(mx/2-1.))

     write (*,*) 'Upstart of multi_drive3'
     call oflush

     allocate(firstlev)
     if (associated(firstlev%next)) nullify(firstlev%next)
!     firstlev => scrp1
     write (*,*) 'allocated first'
        call oflush
     allocate(secondlev)
     if (associated(secondlev%next)) nullify(secondlev%next)
!     secondlev => scrp2 
     allocate(thirdlev)
!     thirdlev => scrp3
     if (associated(thirdlev%next)) nullify(thirdlev%next) 
     write (*,*) 'Creating ',gg,' levels with :'
     write (*,*) 'radii      ',granrarr
     write (*,*) 'amplitudes ',amplarr
     write (*,*) 'lifetimes  ',life_tarr
  endif
!$omp end single

  do iz=izs,ize
  do ix=1,mx
    Ux(ix,lb,iz) = 0.
    Uz(ix,lb,iz) = 0.
  end do
  end do

!$omp single
  do k=1,3
     select case (k)
     case (1)    
        if (associated(first)) nullify(first)
        first => firstlev
        current => firstlev
        if (associated(firstlev%next)) then
          first%next => firstlev%next
          current%next => firstlev%next
        endif
        previous => first
        last => first
     case (2)
        if (associated(first)) nullify(first)
        first => secondlev
        current => secondlev
        if (associated(secondlev%next)) then
          first%next => secondlev%next
          current%next => secondlev%next
        endif
        previous => first
        last => first
     case (3)
        if (associated(first)) nullify(first)
        first => thirdlev
        current => thirdlev
        if (associated(thirdlev%next)) then
          first%next => thirdlev%next
          current%next => thirdlev%next
        endif
        previous => first
        last => first
     end select
     
     ampl=amplarr(k)
     granr=granrarr(k)
     life_t=life_tarr(k)
     zrange=zrangearr(k)
     xrange=xrangearr(k)

     call drive3(k,Uz,Ux)

     select case (k)
     case (1)
       do 
       if (associated(firstlev,first)) then
          if (.NOT. associated(firstlev%next,first%next)) then
            firstlev%next=>first%next
            write (*,*) 'Next not aligned!!!, corrected'
          end if 
          exit
       else
         firstlev => first
         if (.NOT.associated(firstlev%next,first%next))firstlev%next=>first%next
         write (*,*) 'First point dissapeared, relocating ',k
       endif
      enddo
     
     case (2)
     do 
     if (associated(secondlev,first)) then
        if (.NOT. associated(secondlev%next,first%next)) then
          secondlev%next => first%next
          write (*,*) 'Next not aligned!!!, corrected'
        endif
        exit
     else
        secondlev => first
        if (.NOT.associated(secondlev%next,first%next)) secondlev%next => first%next
        write (*,*) 'First point dissapeared, relocating ',k
     endif
     enddo

     case (3)
     do
     if (associated(thirdlev,first)) then
       if (.NOT. associated(thirdlev%next,first%next)) then
         thirdlev%next=>first%next
         write (*,*) 'Next not aligned!!!, corrected'
       endif
       exit
     else
        thirdlev => first
        if (.NOT.associated(thirdlev%next,first%next)) thirdlev%next => first%next
        write (*,*) 'First point dissapeared, relocating ',k
     endif
     enddo
     end select
!     if (k.eq.1) firstlev%next => first%next
!     if (k.eq.2) secondlev%next => first%next
!     if (k.eq.3) thirdlev%next => first%next
     
     call resetarr
  enddo

!  Normalize to given total rms-velocity

  vrms=0. 

  do iz=1,mz
    vrms=vrms+sum(Ux(:,lb,iz)**2+Uz(:,lb,iz)**2)
  end do
  vrms=sqrt(vrms)/(mz*mx)+1e-30
!$omp end single

  do iz=izs,ize
    Ux(:,lb,iz) = Ux(:,lb,iz)*v_ampl/vrms
    Uz(:,lb,iz) = Uz(:,lb,iz)*v_ampl/vrms
    ! Ux(:,lb+1,iz) = 0.5*(Ux(:,lb,iz)+Ux(:,lb+2,iz))
    ! Uz(:,lb+1,iz) = 0.5*(Uz(:,lb,iz)+Uz(:,lb+2,iz))
  end do

END SUBROUTINE
