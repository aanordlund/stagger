! $Id: global_disk_bdry.f90,v 1.7 2006/06/10 20:17:22 aake Exp $

!-----------------------------------------------------------------------
SUBROUTINE init_boundary
  USE params
  implicit none
  lb = 1
  ub = my
!
END

!-----------------------------------------------------------------------
SUBROUTINE read_boundary
  USE params
  implicit none
!
END

!-----------------------------------------------------------------------
SUBROUTINE density_boundary (r)
  USE params
  implicit none
  real, dimension(mx,my,mz):: r
!
END

!-----------------------------------------------------------------------
SUBROUTINE density_boundary_log (lnr,r)
  USE params
  implicit none
  real, dimension(mx,my,mz):: lnr,r
!
END

!-----------------------------------------------------------------------
SUBROUTINE energy_boundary (r,lnr,Ux,Uy,Uz,e,ee,p,Bx,By,Bz)
  USE params
  USE forcing
  implicit none
  real, dimension(mx,my,mz):: r,lnr,Ux,Uy,Uz,e,ee,p,Bx,By,Bz

END

!-----------------------------------------------------------------------
SUBROUTINE passive_boundary (r,Ux,Uy,Uz,d,dd)
  USE params
  implicit none
  real, dimension(mx,my,mz):: r,Ux,Uy,Uz,d,dd

END

!-----------------------------------------------------------------------
SUBROUTINE velocity_boundary (r,xdnr,ydnr,zdnr,Ux,Uy,Uz,px,py,pz,e,Bx,By,Bz)
  USE params
  implicit none
  real, dimension(mx,my,mz):: r,xdnr,ydnr,zdnr,Ux,Uy,Uz,px,py,pz,e,ee,Bx,By,Bz
!
END

!-----------------------------------------------------------------------
SUBROUTINE mfield_boundary (Ux,Uy,Uz,Bx,By,Bz)
  USE params
  implicit none
  real, dimension(mx,my,mz):: Ux,Uy,Uz,Bx,By,Bz
!
END

!-----------------------------------------------------------------------
SUBROUTINE efield_boundary (Ex, Ey, Ez, Bx_y, Bx_z, By_z, By_x, Bz_x, Bz_y, &
                        Rx, Ry, Rz, Ux_y, Ux_z, Uy_z, Uy_x, Uz_x, Uz_y, &
                        Ux, Uy, Uz)
  USE params
  implicit none
  real, dimension(mx,my,mz):: Ex, Ey, Ez, Bx_y, Bx_z, By_z, By_x, Bz_x, Bz_y, &
                        Rx, Ry, Rz, Ux_y, Ux_z, Uy_z, Uy_x, Uz_x, Uz_y, &
                        Ux, Uy, Uz
END

!-----------------------------------------------------------------------
SUBROUTINE ddt_boundary (r,px,py,pz,e,Bx,By,Bz, &
                         drdt,dpxdt,dpydt,dpzdt, &
                         dedt,dBxdt,dBydt,dBzdt)
  USE params
  implicit none
  real, dimension(mx,my,mz):: r,px,py,pz,e,Bx,By,Bz, &
                              drdt,dpxdt,dpydt,dpzdt, &
                              dedt,dBxdt,dBydt,dBzdt
  integer ix0,iy,iy0,iz0

!-----------------------------------------------------------------------
!  Prevent the temperature from ever changing in the innermost central
!  3x3 points for even resolution and 2x2 points for odd resolution
!-----------------------------------------------------------------------
  ix0 = mx/2
  iy0 = (mpi_ny*my)/2-mpi_y*my
  if (mod(mx,2) .eq. 0) then
    do iy=max(1,iy0-1),min(my,iy0+1)
      dedt(ix0-1:ix0+1,iy,:) = drdt(ix0-1:ix0+1,iy,:)* &
                      e(ix0-1:ix0+1,iy,:)/r(ix0-1:ix0+1,iy,:)
    end do
  else
    do iy=max(1,iy0),min(my,iy0+1)
      dedt(ix0:ix0+1,iy,:) = drdt(ix0:ix0+1,iy,:)* &
                      e(ix0:ix0+1,iy,:)/r(ix0:ix0+1,iy,:)
    end do
  end if
!
END

!-----------------------------------------------------------------------
SUBROUTINE regularize (f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
!
END

!-----------------------------------------------------------------------
SUBROUTINE regularize_face (f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
!
END
