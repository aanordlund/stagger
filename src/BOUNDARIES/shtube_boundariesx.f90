! $Id: fixed_boundaries.f90,v 1.2 2006/08/29 22:15:08 aake Exp $

MODULE boundary
  integer, parameter:: mbdry=6
  real, allocatable, dimension(:,:,:):: rb,pxb,pyb,pzb,eb,db,bxb,byb,bzb
  real, allocatable, dimension(:,:,:):: Uxb,Uyb,Uzb
  logical load_boundaries
  logical:: do_bondary=.false.
  logical:: do_eta=.false.
  real:: eta_scl=1.0
  integer:: eta_width=1
  integer:: isub=1
END MODULE boundary

!-----------------------------------------------------------------------
SUBROUTINE init_boundary
  USE params
  USE variables
  USE boundary
  implicit none
  integer i,j,k
  character(len=mid):: id="$Id: fixed_boundaries.f90,v 1.2 2006/08/29 22:15:08 aake Exp $"
  call print_id (id)
  load_boundaries=.true.
  allocate (rb(mx,my,mz),pxb(mx,my,mz),pyb(mx,my,mz),pzb(mx,my,mz))
  allocate (eb(mx,my,mz),bxb(mx,my,mz),byb(mx,my,mz),bzb(mx,my,mz))
  allocate (Uxb(mx,my,mz),Uyb(mx,my,mz),Uzb(mx,my,mz))
  lb=6
  ub=my-5

  pxb=0.; pyb=0.; pzb=0.; bxb=0.; byb=0.; bzb=0.; Uxb=0.; Uyb=0.; Uzb=0.
!$omp parallel private (i,j,k)
  do i=1,mx/2
    do j=1,my
      do k=1,mz
        rb(i,j,k)=1.
        eb(i,j,k)=1.
        bxb(i,j,k)=0.75
!       byb(i,j,k)=1.
        bzb(i,j,k)=1.
      end do
    end do
  end do
  do i=mx/2+1,my
    do j=1,my
      do k=1,mz
        rb(i,j,k)=0.125
        eb(i,j,k)=0.1
        bxb(i,j,k)=0.75
!       byb(i,j,k)=-1.
        bzb(i,j,k)=-1.
      end do
    end do
  end do
!$omp end parallel
!
END

!-----------------------------------------------------------------------
SUBROUTINE read_boundary
  USE params
  implicit none
!
END

!-----------------------------------------------------------------------
SUBROUTINE set_boundary (f,fb)
  USE params
  USE boundary
  implicit none
  real, dimension(mx,my,mz):: f, fb
  integer ix,iy,iz
!
  do iz=1,mz
  do iy=1,my
  do ix=1,mbdry
    f(ix,iy,iz)  = fb(ix,iy,iz)
  end do
  end do
  end do
  do iz=1,mz
  do iy=1,my
  do ix=mx-mbdry+1,mx
    f(ix,iy,iz)  = fb(ix,iy,iz)
  end do
  end do
  end do
!
END

!-----------------------------------------------------------------------
SUBROUTINE density_boundary (r,lnr,py,e)
  USE params
  USE boundary
  implicit none
  real, dimension (mx,my,mz):: r,lnr,py,e
  if (do_trace) print *,'density_boundary'

  if (it.eq.1.and.isub.eq.1) then
    isub=2
    print *,'rbl= ',rb(1:3,1,1)
    print *,'rbr= ',rb(mx-mbdry:mx,1,1)
    if (do_energy) then
      print *,'ebl= ',eb(1:3,1,1)
      print *,'ebr= ',eb(mx-mbdry:mx,1,1)
    end if
    print *,'pyl= ',pyb(1:3,1,1)
    print *,'pyr= ',pyb(mx-mbdry:mx,1,1)
    print *,'pxl= ',pxb(1:3,1,1)
    print *,'pxr= ',pxb(mx-mbdry:mx,1,1)
    print *,'pzl= ',pzb(1:3,1,1)
    print *,'pxr= ',pzb(mx-mbdry:mx,1,1)
    if (do_mhd)  then
      print *,'bxl= ',bxb(1:3,1,1)
      print *,'bxr= ',bxb(mx-mbdry:mx,1,1)
      print *,'bzl= ',bzb(1:3,1,1)
      print *,'bzr= ',bzb(mx-mbdry:mx,1,1)
    end if
  end if
!
!
  call set_boundary(r,rb)
  load_boundaries=.false.
END

!-----------------------------------------------------------------------
SUBROUTINE density_boundary_log (r,lnr)
  USE params
  USE boundary
  implicit none
  real, dimension(mx,my,mz):: lnr,r
!
! if (load_boundaries) call copy_bdry(r,r,r,r)

  if (it.eq.1.and.isub.eq.1) then
    isub=2
    print *,'pyl= ',pyb(1:3,1,1)
    print *,'pyr= ',pyb(mx-mbdry:mx,1,1)
    print *,'pxl= ',pxb(1:3,1,1)
    print *,'pxr= ',pxb(mx-mbdry:mx,1,1)
    print *,'pzl= ',pzb(1:3,1,1)
    print *,'pxr= ',pzb(mx-mbdry:mx,1,1)
    if (do_mhd)  then
      print *,'bxl= ',bxb(1:3,1,1)
      print *,'bxr= ',bxb(mx-mbdry:mx,1,1)
      print *,'bzl= ',bzb(1:3,1,1)
      print *,'bzr= ',bzb(mx-mbdry:mx,1,1)
    end if
  end if

  call set_boundary(r,rb)
  lnr = alog(r)
END

!-----------------------------------------------------------------------
SUBROUTINE energy_boundary (r,lnr,Ux,Uy,Uz,e,ee,p,Bx,By,Bz)
  USE params
  USE boundary
  implicit none
  real, dimension(mx,my,mz):: r,lnr,Ux,Uy,Uz,e,ee,p,Bx,By,Bz
!
  call set_boundary(e,eb)
END

!-----------------------------------------------------------------------
SUBROUTINE passive_boundary (r,Ux,Uy,Uz,d,dd)
  USE params
  USE boundary
  implicit none
  real, dimension(mx,my,mz):: r,Ux,Uy,Uz,d,dd
!
  call set_boundary(d,db)
END

!-----------------------------------------------------------------------
SUBROUTINE velocity_boundary (r,xdnr,ydnr,zdnr,Ux,Uy,Uz,px,py,pz,e,Bx,By,Bz)
  USE params
  USE boundary
  implicit none
  real, dimension(mx,my,mz):: r,xdnr,ydnr,zdnr,Ux,Uy,Uz,px,py,pz,e,Bx,By,Bz
  if (do_trace) print *,'velocity_boundary'
!
! if (load_boundaries) call copy_bdry(r,px,py,pz)

  load_boundaries = .false.
  call set_boundary(px,pxb)
  call set_boundary(py,pyb)
  call set_boundary(pz,pzb)
  call set_boundary(Ux,Uxb)
  call set_boundary(Uy,Uyb)
  call set_boundary(Uz,Uzb)
END

!-----------------------------------------------------------------------
SUBROUTINE mfield_boundary (Ux,Uy,Uz,Bx,By,Bz)
  USE params
  USE boundary
  implicit none
  real, dimension(mx,my,mz):: Ux,Uy,Uz,Bx,By,Bz
  if (do_trace) print *,'mfield_boundary'
!
  if (do_mhd) then
    call set_boundary(Bx,Bxb)
    call set_boundary(By,Byb)
    call set_boundary(Bz,Bzb)
  end if

END

!-----------------------------------------------------------------------
SUBROUTINE efield_boundary (Ex, Ey, Ez, Bx_y, Bx_z, By_z, By_x, Bz_x, Bz_y, &
                            Rx, Ry, Rz, Ux_y, Ux_z, Uy_z, Uy_x, Uz_x, Uz_y, &
                            Ux, Uy, Uz, Bxl, Byl, Bzl, Bxu, Byu, Bzu)
  USE params
  implicit none
  real, dimension(mx,my,mz):: Ex, Ey, Ez, Bx_y, Bx_z, By_z, By_x, Bz_x, Bz_y, &
                                          Ux_y, Ux_z, Uy_z, Uy_x, Uz_x, Uz_y, &
                                          Ux, Uy, Uz, Bxl, Byl, Bzl, Bxu, Byu, Bzu
  real, dimension(mx,2 ,mz):: Rx, Ry, Rz
!
END

!-----------------------------------------------------------------------
SUBROUTINE ecurrent_boundary (Ex, Ey, Ez, Jx, Jy, Jz)
  USE params
  implicit none
  real, dimension(mx,my,mz):: Ex, Ey, Ez, Jx, Jy, Jz
!
END

!-----------------------------------------------------------------------
SUBROUTINE magnetic_pressure_boundary(BB)
USE params
  implicit none
  real, dimension(mx,my,mz)::BB
!
END

!-----------------------------------------------------------------------
SUBROUTINE viscosity_boundary(nu,scr)
  USE params
  implicit none
  real, dimension(mx,my,mz):: nu, scr
!
END
!-----------------------------------------------------------------------
SUBROUTINE ddt_boundary (r,px,py,pz,e,Bx,By,Bz, &
                         drdt,dpxdt,dpydt,dpzdt, &
                         dedt,dBxdt,dBydt,dBzdt)
  USE params
  implicit none
  real, dimension(mx,my,mz):: r,px,py,pz,e,Bx,By,Bz, &
                              drdt,dpxdt,dpydt,dpzdt, &
                              dedt,dBxdt,dBydt,dBzdt
  real, allocatable, dimension(:,:,:):: scr
!
  allocate (scr(mx,my,mz))
  scr = 0.
    call dumpn (drdt,'drdt<','bdry.dmp',1)
  call set_boundary(drdt,scr)
    call dumpn (drdt,'drdt>','bdry.dmp',1)
  call set_boundary(dpxdt,scr)
    call dumpn (dpydt,'dpydt<','bdry.dmp',1)
  call set_boundary(dpydt,scr)
    call dumpn (dpydt,'dpydt>','bdry.dmp',1)
  call set_boundary(dpzdt,scr)
    call dumpn (dedt,'dedt<','bdry.dmp',1)
  call set_boundary(dedt,scr)
    call dumpn (dedt,'dedt>','bdry.dmp',1)
  call set_boundary(dbxdt,scr)
    call dumpn (dbxdt,'dbxdt>','bdry.dmp',1)
  call set_boundary(dbydt,scr)
    call dumpn (dbydt,'dbydt>','bdry.dmp',1)
  call set_boundary(dbzdt,scr)
    call dumpn (dbzdt,'dbzdt>','bdry.dmp',1)
  deallocate (scr)
END

!-----------------------------------------------------------------------
SUBROUTINE regularize (f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
!
END

!-----------------------------------------------------------------------
SUBROUTINE regularize_face (f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
!
END


