! $Id: symmetry_y.f90,v 1.20 2009/01/25 16:27:19 aake Exp $

! Boundary conditions.  The indices are assumed to be arranged like so:
!
!                                lb           ub=m-lb+1
! centers:    1   2   3   4   5   6 ...       -5  -4  -3  -2  -1   0
! faces  :  1   2   3   4   5   6   ... -6  -5  -4  -3  -2  -1   0 
!
! For face centered variables, the index ranges 1:lb and ub+1:my are
! external.  For cell centered variables, the ranges are 1:lb-1 and ub+1:my.
!
! Valid points for plotting:
!
!                 F90                   IDL                  total
! center:  lb  :ub = g+1:m-g    lb-1:m-lb = g  :m-g-1   ub-lb+1 = m-2*g
!   face:  lb+1:ub = g+2:m-g    lb  :m-lb = g+1:m-g-1   ub-lb   = m-2*g-1
!
!-----------------------------------------------------------------------
SUBROUTINE constant_center_lower(f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
  integer:: iy,iz

  if (mpi_y > 0) return

  do iz=izs,ize
    do iy=1,lb-1
      f(:,iy,iz) = f(:,lb,iz)
    end do
  end do
END

!-----------------------------------------------------------------------
SUBROUTINE constant_center_upper(f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
  integer:: iy,iz

  if (mpi_y < mpi_ny-1) return

  do iz=izs,ize
    do iy=ub+1,my
      f(:,iy,iz) = f(:,ub,iz)
    end do
  end do
END

!-----------------------------------------------------------------------
SUBROUTINE constant_center(f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f

  logical omp_in_parallel
  if (omp_in_parallel()) then
    call constant_center_lower(f)
    call constant_center_upper(f)
  else
    !$omp parallel
    call constant_center_lower(f)
    call constant_center_upper(f)
    !$omp end parallel
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE symmetric_center_lower(f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
  integer:: iy,iz

  if (mpi_y > 0) return

  do iz=izs,ize
    do iy=1,lb-1
      f(:,iy,iz) = f(:,lb+(lb-iy),iz)
    end do
  end do
END

!-----------------------------------------------------------------------
SUBROUTINE symmetric_center_upper(f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
  integer:: iy,iz

  if (mpi_y < mpi_ny-1) return

  do iz=izs,ize
    do iy=ub+1,my
      f(:,iy,iz) = f(:,ub-(iy-ub),iz)
    end do
  end do
END

!-----------------------------------------------------------------------
SUBROUTINE symmetric_center(f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f

  logical omp_in_parallel
  if (omp_in_parallel()) then
    call symmetric_center_lower(f)
    call symmetric_center_upper(f)
  else
    !$omp parallel
    call symmetric_center_lower(f)
    call symmetric_center_upper(f)
    !$omp end parallel
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE symmetric_face_lower(f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
  integer:: iy,iz

  if (mpi_y > 0) return

  do iz=izs,ize
    do iy=1,lb
      f(:,iy,iz) = f(:,lb+1+(lb-iy),iz)
    end do
  end do
END

!-----------------------------------------------------------------------
SUBROUTINE symmetric_face_upper(f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
  integer:: iy,iz

  if (mpi_y < mpi_ny-1) return

  do iz=izs,ize
    do iy=ub+1,my
      f(:,iy,iz) = f(:,ub+1-(iy-ub),iz)
    end do
  end do
END

!-----------------------------------------------------------------------
SUBROUTINE symmetric_face(f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f

  logical omp_in_parallel
  if (omp_in_parallel()) then
    call symmetric_face_lower(f)
    call symmetric_face_upper(f)
  else
    !$omp parallel
    call symmetric_face_lower(f)
    call symmetric_face_upper(f)
    !$omp end parallel
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE antisymmetric_center_lower(f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
  integer:: iy,iz

  if (mpi_y > 0) return

  do iz=izs,ize
    do iy=1,lb-1
      f(:,iy,iz) = -f(:,lb+(lb-iy),iz)
    end do
    f(:,lb,iz) = 0.
  end do
END

!-----------------------------------------------------------------------
SUBROUTINE antisymmetric_center_upper(f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
  integer:: iy,iz

  if (mpi_y < mpi_ny-1) return

  do iz=izs,ize
    do iy=ub+1,my
      f(:,iy,iz) = -f(:,ub-(iy-ub),iz)
    end do
    f(:,ub,iz) = 0.
  end do
END

!-----------------------------------------------------------------------
SUBROUTINE antisymmetric_center(f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f

  logical omp_in_parallel
  if (omp_in_parallel()) then
    call antisymmetric_center_lower(f)
    call antisymmetric_center_upper(f)
  else
    !$omp parallel
    call antisymmetric_center_lower(f)
    call antisymmetric_center_upper(f)
    !$omp end parallel
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE antisymmetric_face_lower(f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
  integer:: iy,iz

  if (mpi_y > 0) return

  do iz=izs,ize
    do iy=1,lb
      f(:,iy,iz) = -f(:,lb+1+(lb-iy),iz)
    end do
  end do
END

!-----------------------------------------------------------------------
SUBROUTINE antisymmetric_face_upper(f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
  integer:: iy,iz

  if (mpi_y < mpi_ny-1) return

  do iz=izs,ize
    do iy=ub+1,my
      f(:,iy,iz) = -f(:,ub+1-(iy-ub),iz)
    end do
  end do
END

!-----------------------------------------------------------------------
SUBROUTINE antisymmetric_face(f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f

  logical omp_in_parallel
  if (omp_in_parallel()) then
    call antisymmetric_face_lower(f)
    call antisymmetric_face_upper(f)
  else
    !$omp parallel
    call antisymmetric_face_lower(f)
    call antisymmetric_face_upper(f)
    !$omp end parallel
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE derivative_face_lower (f, df)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f, df
  integer:: iy,iz

  if (mpi_y > 0) return

  do iz=izs,ize
    do iy=1,lb
      f(:,iy,iz) = f(:,lb+1,iz) - df(:,lb,iz)*(ym(lb)-ym(iy))
    end do
  end do
END

!-----------------------------------------------------------------------
SUBROUTINE derivative_face_upper (f, df)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f, df
  integer:: iy,iz

  if (mpi_y < mpi_ny-1) return

  do iz=izs,ize
    do iy=ub+1,my
      f(:,iy,iz) = f(:,ub,iz) + df(:,ub,iz)*(ym(iy)-ym(ub))
    end do
  end do
END

!-----------------------------------------------------------------------
SUBROUTINE derivative_face (f, df)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f, df

  logical omp_in_parallel
  if (omp_in_parallel()) then
    call derivative_face_lower (f, df)
    call derivative_face_upper (f, df)
  else
    !$omp parallel
    call derivative_face_lower (f, df)
    call derivative_face_upper (f, df)
    !$omp end parallel
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE extrapolate_center_lower(f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
  integer:: iy,iz

  if (mpi_y > 0) return

  do iz=izs,ize
    do iy=1,lb-1
      f(:,iy,iz) = 2.*f(:,lb,iz)-f(:,lb+(lb-iy),iz)
    end do
  end do
END

!-----------------------------------------------------------------------
SUBROUTINE extrapolate_center_upper(f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
  integer:: iy,iz

  if (mpi_y < mpi_ny-1) return

  do iz=izs,ize
    do iy=ub+1,my
      f(:,iy,iz) = 2.*f(:,ub,iz)-f(:,ub-(iy-ub),iz)
    end do
  end do
END

!-----------------------------------------------------------------------
SUBROUTINE extrapolate_center(f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f

  logical omp_in_parallel
  if (omp_in_parallel()) then
    call extrapolate_center_lower(f)
    call extrapolate_center_upper(f)
  else
    !$omp parallel
    call extrapolate_center_lower(f)
    call extrapolate_center_upper(f)
    !$omp end parallel
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE extrapolate_constrained_center_lower(f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
  integer:: iy,iz
  logical omp_in_parallel
  real p

  if (mpi_y == 0) then
    do iz=izs,ize
    do iy=1,lb-1
      p = 1.-((lb-iy)/(lb-3.))**2
      f(:,iy,iz) = p*(f(:,lb,iz)-f(:,lb+(lb-iy),iz))+f(:,lb,iz)
    end do
    end do
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE extrapolate_constrained_center_upper(f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
  integer:: iy,iz
  logical omp_in_parallel
  real p

  if (mpi_y == mpi_ny-1) then
    do iz=izs,ize
    do iy=ub+1,my
      p = 1.-((ub-iy)/real(my-ub-2))**2
      f(:,iy,iz) = p*(f(:,ub,iz)-f(:,ub+(ub-iy),iz))+f(:,ub,iz)
    end do
    end do
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE extrapolate_constrained_center(f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
  logical omp_in_parallel

  if (omp_in_parallel()) then
    call extrapolate_constrained_center_lower(f)
    call extrapolate_constrained_center_upper(f)
  else
    !$omp parallel
    call extrapolate_constrained_center_lower(f)
    call extrapolate_constrained_center_upper(f)
    !$omp end parallel
  end if
END


!-----------------------------------------------------------------------
SUBROUTINE extrapolate_center_log_lower(f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
  integer:: iy,iz

  if (mpi_y > 0) return

  do iz=izs,ize
    do iy=1,lb-1
      f(:,iy,iz) = exp(2.*alog(f(:,lb,iz))-alog(f(:,lb+(lb-iy),iz)))
    end do
  end do
END

!-----------------------------------------------------------------------
SUBROUTINE extrapolate_center_log_upper(f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
  integer:: iy,iz

  if (mpi_y < mpi_ny-1) return

  do iz=izs,ize
    do iy=ub+1,my
      f(:,iy,iz) = exp(2.*alog(f(:,ub,iz))-alog(f(:,ub-(iy-ub),iz)))
    end do
  end do
END

!-----------------------------------------------------------------------
SUBROUTINE extrapolate_center_log(f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f

  logical omp_in_parallel
  if (omp_in_parallel()) then
    call extrapolate_center_log_lower(f)
    call extrapolate_center_log_upper(f)
  else
    !$omp parallel
    call extrapolate_center_log_lower(f)
    call extrapolate_center_log_upper(f)
    !$omp end parallel
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE vanishing_face_lower(f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
  integer:: iy,iz

  if (mpi_y > 0) return

  do iz=izs,ize
    do iy=1,lb
      f(:,iy,iz) = (iy-1)/float(lb)*f(:,lb+1,iz)
    end do
  end do
END

!-----------------------------------------------------------------------
SUBROUTINE vanishing_face_upper(f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
  integer:: iy,iz

  if (mpi_y < mpi_ny-1) return

  do iz=izs,ize
    do iy=ub+1,my
      f(:,iy,iz) = (my-iy)/float(my-ub)*f(:,ub,iz)
    end do
  end do
END

!-----------------------------------------------------------------------
SUBROUTINE vanishing_face(f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f

  logical omp_in_parallel
  if (omp_in_parallel()) then
    call vanishing_face_lower(f)
    call vanishing_face_upper(f)
  else
    !$omp parallel
    call vanishing_face_lower(f)
    call vanishing_face_upper(f)
    !$omp end parallel
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE vanishing_center_lower(f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
  integer:: iy,iz

  if (mpi_y > 0) return

  do iz=izs,ize
    do iy=1,lb-1
      f(:,iy,iz) = iy/float(lb)*f(:,lb,iz)
    end do
  end do
END

!-----------------------------------------------------------------------
SUBROUTINE vanishing_center_upper(f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
  integer:: iy,iz

  if (mpi_y < mpi_ny-1) return

  do iz=izs,ize
    do iy=ub+1,my
      f(:,iy,iz) = (my+1-iy)/float(my+1-ub)*f(:,ub,iz)
    end do
  end do
END

!-----------------------------------------------------------------------
SUBROUTINE vanishing_center(f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f

  logical omp_in_parallel
  if (omp_in_parallel()) then
    call vanishing_center_lower(f)
    call vanishing_center_upper(f)
  else
    !$omp parallel
    call vanishing_center_lower(f)
    call vanishing_center_upper(f)
    !$omp end parallel
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE mixed_center_lower(f,fl)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
  integer:: iy,iz
  real fl

  if (mpi_y > 0) return

  do iz=izs,ize
    do iy=1,lb-1
      f(:,iy,iz) = (1.-(ym(lb)-ym(iy))/fl)*f(:,lb,iz)
    end do
  end do
END

!-----------------------------------------------------------------------
SUBROUTINE mixed_center_upper(f,fl)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
  integer:: iy,iz
  real fl

  if (mpi_y < mpi_ny-1) return

  do iz=izs,ize
    do iy=ub+1,my
      f(:,iy,iz) = (1.-(ym(iy)-ym(ub))/fl)*f(:,ub,iz)
    end do
  end do
END

!-----------------------------------------------------------------------
SUBROUTINE mixed_center(f,fl)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
  real fl

  logical omp_in_parallel
  if (omp_in_parallel()) then
    call mixed_center_lower(f,fl)
    call mixed_center_upper(f,fl)
  else
    !$omp parallel shared(f,fl)
    call mixed_center_lower(f,fl)
    call mixed_center_upper(f,fl)
    !$omp end parallel
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE extrapolate_constrained_face_lower(f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
  real:: flb 
  integer:: ix,iy,iz
  logical omp_in_parallel
  real p

  if (mpi_y == 0) then
    do iz=izs,ize
    do ix=1,mx
      flb = 2./8.*(15.*f(ix,lb+1,iz)-10.*f(ix,lb+2,iz)+3.*f(ix,lb+3,iz))
      do iy=1,lb
        p = 1.-((lb+1.-iy)/real(lb))**2
        f(ix,iy,iz) = p*(flb-f(ix,lb+1+(lb-iy),iz))
      end do
    end do
    end do
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE extrapolate_constrained_face_upper(f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
  real fub
  integer:: ix,iy,iz
  logical omp_in_parallel
  real p

  if (mpi_y == mpi_ny-1) then
    do iz=izs,ize
    do ix=1,mx
      fub = 2./8.*(15.*f(ix,ub  ,iz)-10.*f(ix,ub-1,iz)+3.*f(ix,ub-2,iz))
      do iy=ub+1,my
	p = 1.-((iy-ub)/real(my-ub))**2
        f(ix,iy,iz) = p*(fub-f(ix,ub+1-(iy-ub),iz))
      end do
    end do
    end do
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE extrapolate_face_constrained(f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
  logical omp_in_parallel

  if (omp_in_parallel()) then
    call extrapolate_constrained_face_lower(f)
    call extrapolate_constrained_face_upper(f)
  else
    !$omp parallel
    call extrapolate_constrained_face_lower(f)
    call extrapolate_constrained_face_upper(f)
    !$omp end parallel
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE extrapolate_face_lower(f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
  real:: flb 
  integer:: ix,iy,iz

  if (mpi_y > 0) return

  do iz=izs,ize
    do ix=1,mx
      flb = 2./8.*(15.*f(ix,lb+1,iz)-10.*f(ix,lb+2,iz)+3.*f(ix,lb+3,iz))
      do iy=1,lb
        f(ix,iy,iz) = flb-f(ix,lb+1+(lb-iy),iz)
      end do
    end do
  end do
END

!-----------------------------------------------------------------------
SUBROUTINE extrapolate_face_upper(f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
  real fub
  integer:: ix,iy,iz

  if (mpi_y < mpi_ny-1) return

  do iz=izs,ize
    do ix=1,mx
      fub = 2./8.*(15.*f(ix,ub  ,iz)-10.*f(ix,ub-1,iz)+3.*f(ix,ub-2,iz))
      do iy=ub+1,my
        f(ix,iy,iz) = fub-f(ix,ub+1-(iy-ub),iz)
      end do
    end do
  end do
END

!-----------------------------------------------------------------------
SUBROUTINE extrapolate_face(f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f

  logical omp_in_parallel
  if (omp_in_parallel()) then
    call extrapolate_face_lower(f)
    call extrapolate_face_upper(f)
  else
    !$omp parallel
    call extrapolate_face_lower(f)
    call extrapolate_face_upper(f)
    !$omp end parallel
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE extrapolate_face_value_lower(f,b)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
  real, dimension(mx,mz):: b
  integer:: ix,iy,iz

  if (mpi_y > 0) return

  do iz=izs,ize
    do iy=1,lb
      f(:,iy,iz) = 2.*b(:,iz)-f(:,lb+1+(lb-iy),iz)
    end do
  end do
END

!-----------------------------------------------------------------------
SUBROUTINE extrapolate_face_value_upper(f,b)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
  real, dimension(mx,mz):: b
  integer:: ix,iy,iz

  if (mpi_y < mpi_ny-1) return

  do iz=izs,ize
    do iy=ub+1,my
      f(:,iy,iz) = 2.*b(:,iz)-f(:,ub+1-(iy-ub),iz)
    end do
  end do
END

!-----------------------------------------------------------------------
SUBROUTINE extrapolate_face_value(f,flb,fub)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
  real, dimension(mx,mz):: flb,fub

  logical omp_in_parallel
  if (omp_in_parallel()) then
    call extrapolate_face_value_lower(f,flb)
    call extrapolate_face_value_upper(f,fub)
  else
    !$omp parallel
    call extrapolate_face_value_lower(f,flb)
    call extrapolate_face_value_upper(f,fub)
    !$omp end parallel
  end if
END


!-----------------------------------------------------------------------
SUBROUTINE value_face_lower(f,fl)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
  real, dimension(mx,mz):: fl
  integer:: iy,iz

  if (mpi_y > 0) return

  do iz=izs,ize
    do iy=1,lb
      f(:,iy,iz) = fl(:,iz) + (lb+0.5-iy)*2.*(fl(:,iz)-f(:,lb+1,iz))
    end do
  end do
END

!-----------------------------------------------------------------------
SUBROUTINE value_face_upper(f,fu)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
  real, dimension(mx,mz):: fu
  integer:: iy,iz

  if (mpi_y < mpi_ny-1) return

  do iz=izs,ize
    do iy=ub+1,my
      f(:,iy,iz) = fu(:,iz) + (iy-ub-0.5)*2.*(fu(:,iz)-f(:,ub,iz))
    end do
  end do
END

!-----------------------------------------------------------------------
SUBROUTINE value_face(f,fl,fu)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
  real, dimension(mx,mz):: fl,fu

  logical omp_in_parallel
  if (omp_in_parallel()) then
    call value_face_lower(f,fl)
    call value_face_upper(f,fu)
  else
    !$omp parallel
    call value_face_lower(f,fl)
    call value_face_upper(f,fu)
    !$omp end parallel
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE boundary_test
  USE params
  real, allocatable, dimension(:,:,:):: f
  integer  :: iy
  mx=15
  my=mx
  mz=mx
  lb=6
  ub=my-5
  allocate (f(mx,my,mz))

  do iy=1,my
    f(:,iy,:)=cos((iy-lb)*2.*pi/(ub-lb))
  end do

  call symmetric_center(f)
  print '(a/15f8.4)','s_c',f(1,:,1)  
  call antisymmetric_center(f)
  print '(a/15f8.4)','a_c',f(1,:,1)  
  call symmetric_face(f)
  print '(a/15f8.4)','s_f',f(1,:,1)  
  call antisymmetric_face(f)
  print '(a/15f8.4)','a_f',f(1,:,1)
  
END
