! $Id: stellar-atm_bdry_omp_charbotbc.f90,v 1.28 2008/01/16 21:53:10 bob Exp $
!***********************************************************************
MODULE boundary
  real t_Bbdry, Bscale, lb_eampl, t_bdry, eetop_frac, Uy_bdry, fmtop_frac
  real pdamp, rtop_frac
  real rbot, pbot, ebot, rtop, lnrtop, etop, eetop, htop, eetop1
  real ampxy, ampyz, period, omegap, akx, aky, akz, kxp, kyp, kzp
  real Bx0, By0, Bz0, rmin, pb_fact, uy_max
  real(kind=8) rub,pyub,elb,eub,pub,pubt,dpydtub
  real, allocatable, dimension(:,:,:):: Bxl, Byl, Bzl, Bxu, Byu, Bzu
  real, allocatable, dimension(:,:) :: drdt_bot, dpxdt_bot, dpydt_bot, &
                                       dpzdt_bot, dedt_bot
  real, allocatable, dimension(:,:) :: drdt_top, dpxdt_top, dpydt_top, &
                                       dpzdt_top, dedt_top


  real, allocatable, dimension(:,:,:):: Uxc, Uyc, Uzc, rdn, Csdn, &
                                        drdy, dpdy, dUxdy, dUydy, dUzdy, &
                                        d1, d2, d3, d4, d5, d6, d7, d8

  real, allocatable, dimension(:,:):: dpdx, dpdz, divh_ph, divh_uh, &
                                      divh_pxuh, divh_pyuh, divh_pzuh, divh_euh, &
                                      delta_p

  real, allocatable, dimension(:,:):: scra, scrb, scrc, scrd, scre, scrf

  real(kind=8), allocatable, dimension(:):: pyav

  logical debug, bdry_first, Binflow
  integer verbose, idump

END module

!-----------------------------------------------------------------------
SUBROUTINE init_boundary
  USE params
  USE arrays
  USE boundary
  implicit none
  character(len=mid):: id='$Id: stellar-atm_bdry_omp_charbotbc.f90,v 1.28 2008/01/16 21:53:10 bob Exp $'

  call print_id(id)

  if (mpi_y == 0) then
    lb = 6
    allocate (Bxl(mx,lb,mz),Byl(mx,lb,mz),Bzl(mx,lb,mz))
  else
    lb = 0
  end if
  if (mpi_y == mpi_ny-1) then
    ub = my-5
    allocate (Bxu(mx,my-ub,mz),Byu(mx,my-ub,mz),Bzu(mx,my-ub,mz))
    allocate (drdt_bot(mx,mz),dpxdt_bot(mx,mz),dpydt_bot(mx,mz),dpzdt_bot(mx,mz),dedt_bot(mx,mz))
  else
    ub = my
  end if

  allocate(Uxc(mx,my,mz),Uyc(mx,my,mz),Uzc(mx,my,mz))
  allocate(rdn(mx,my,mz),Csdn(mx,my,mz),drdy(mx,my,mz),dpdy(mx,my,mz))
  allocate(dUxdy(mx,my,mz),dUydy(mx,my,mz),dUzdy(mx,my,mz))
  allocate(d1(mx,my,mz),d2(mx,my,mz),d3(mx,my,mz),d4(mx,my,mz),d5(mx,my,mz),d6(mx,my,mz),d7(mx,my,mz),d8(mx,my,mz))

  allocate(dpdx(mx,mz),dpdz(mx,mz),divh_ph(mx,mz),divh_uh(mx,mz),divh_pxuh(mx,mz),divh_pyuh(mx,mz),divh_pzuh(mx,mz))
  allocate(divh_euh(mx,mz),delta_p(mx,mz))
  allocate(scra(mx,mz),scrb(mx,mz),scrc(mx,mz),scrd(mx,mz),scre(mx,mz),scrf(mx,mz))
  allocate(pyav(my))

  t_bdry = 0.01                                                         ! bdry decay time
  t_Bbdry  = 0.01                                                       ! bdry decay time for B
  eetop_frac=0.05                                                       ! fraction of current eetop
  rtop_frac=0.05                                                        ! fraction of current rtop
  fmtop_frac=1.0                                                        ! fraction fmass correction for rho(lb)
  lb_eampl = 0.
  pdamp = 0.
  rbot = -1.
  htop = 0.
  rmin = 1e-3
  pb_fact = 1.2
  uy_max = 2.                                                           ! 20 km/s with solar scaling
  ampxy = 0.
  ampyz = 0.
  period = 0.
  akx = 1.
  aky = 1.
  akz = 1.
  Bscale = dym(ub)*10.
  Bx0 = 0.
  By0 = 0.
  Bz0 = 0.
  Binflow = .false.
  debug = .false.
  verbose = 0
  bdry_first = .true.
  do_stratified = .true.
  call read_boundary

  kxp=akx*2.*pi/mx
  kyp=aky*2.*pi/(ub-lb+1)
  kzp=akz*2.*pi/mz
  if (period .gt. 0.) omegap=2.*pi/period

  call init_potential_lower
END

!-----------------------------------------------------------------------
SUBROUTINE read_boundary
  USE params
  USE boundary
  implicit none
  namelist /bdry/ lb, ub, t_bdry, t_Bbdry, Bscale, eetop_frac, rtop_frac, fmtop_frac, &
                  htop, rbot, pbot, ebot, Bx0, By0, Bz0, Binflow, Uy_bdry, &
                  rmin, pb_fact, uy_max, lb_eampl, pdamp, debug, verbose, &
                  ampxy, ampyz, period, akx, aky, akz

  rewind (stdin); read (stdin,bdry)
  if (master) write (*,bdry)
END

!-----------------------------------------------------------------------
SUBROUTINE density_boundary (r,lnr,py,e)
  USE params
  USE arrays, only: scr6, eeav
  USE forcing
  USE boundary
  USE eos
  implicit none
  real, dimension(mx,my,mz):: r,lnr,py,e
  integer ix,iy,iz
  real htopi, dlnrdy, lnrmin, lnrmax

if (do_trace) print *, 'density_BC: mpi_rank,y,ny = ', mpi_rank, mpi_y, mpi_ny
if (do_trace) print *, 'density_BC: mythread = ', omp_mythread,izs,ize
if (debug .and. omp_master) print *,'begin density'

  call haverage_subr (r,rav)                                            ! horizontally averaged rho
  call haverage_subr (e,eav)                                            ! horizontally averaged e
  do iz=izs,ize                                                       ! loop over z
    scr6(:,:,iz) = e(:,:,iz)/r(:,:,iz)                                ! ee = e/r
  end do

  idump = 0
  if (pav(1).eq.0.) then                                                ! if first time
    idump = 0
    call haverage_subr (scr6, eeav)					! haverage ee for initial eetop
!$omp single
    eetop=eeav(lb+1)
!$omp end single
  end if

  call dumpl(r,'r','bdry.dmp',0+34*idump)
  if (lb > 1) then
    call extrapolate_center_lower(lnr)                                  ! basic default action
    call barrier_omp ('pav')						! prevent read/write race
    call pressure (r, scr6, scratch)                                    ! compute the pressure
    htopi = gy*rav(lb)/pav(lb)                                          ! inverse pressure scale height
    lnrmin = alog(rmin*real(rav(lb)))                                   ! min lnrho value
    lnrmax = alog(3.*real(rav(lb)))                                     ! max lnrho value in inflows
    do iz=izs,ize
      do ix=1,mx
        lnr(ix,lb,iz) = max(lnr(ix,lb,iz),lnrmin)                       ! clamp extreme values
        r(ix,lb,iz) = exp(lnr(ix,lb,iz))                                ! consistent density
      end do
      do ix=1,mx
        if (py(ix,lb+1,iz) > 0.) then                                   ! inflow conditions
         do iy=1,lb-1
          dlnrdy = &
            (lnr(ix,lb+lb-iy,iz)-lnr(ix,lb,iz))/(ym(lb+lb-iy)-ym(lb))   ! log density gradient
          lnr(ix,iy,iz) = lnr(ix,lb,iz) - &                             ! extrapolate ..
            (ym(lb)-ym(iy))*max(dlnrdy,-2*dlnrdy,htopi)                 ! .. on the safe side
          lnr(ix,iy,iz) = max(lnr(ix,iy,iz),lnrmin)                     ! clamp extreme values
          r(ix,iy,iz) = exp(lnr(ix,iy,iz))                              ! consistent density
         end do
        end if
      end do
    end do
    do iz=izs,ize                                                       ! loop over z
    do iy=1,lb
      e(:,iy,iz) = r(:,iy,iz)*scr6(:,iy,iz)				! consistent e,ee
    end do
    end do
  end if
if (do_trace) print *, 'density_BC, end top: mythread = ', omp_mythread,izs,ize

  call extrapolate_center_upper(lnr)                                    ! extrapolate in the log
  if (ub < my) then
    do iz=izs,ize
    do iy=ub+1,my
    do ix=1,mx
      r(ix,iy,iz)=exp(lnr(ix,iy,iz))                                    ! consistent density
    end do 
    end do 
    end do
  end if
  call dumpl(r,'r','bdry.dmp',1+34*idump)
  call dumpl(e,'e','bdry.dmp',2+34*idump)
  call dumpl(scratch,'p','bdry.dmp',3+34*idump)
if (debug .and. omp_master) print *,'end density'
if (do_trace) print *, 'density_BC, end bottom: mythread = ', omp_mythread,izs,ize
    call barrier_omp ('density')						! prevent read/write race
END

!-----------------------------------------------------------------------
SUBROUTINE energy_boundary (r,lnr,Ux,Uy,Uz,e,ee,p,Bx,By,Bz)
  USE params
  USE arrays, ONLY: eeav
  USE eos
  USE boundary
  implicit none
  real, dimension(mx,my,mz):: r,lnr,Ux,Uy,Uz,e,ee,p,Bx,By,Bz
  real s, efact
  real(kind=8) tmp(3)
  integer ix, iy, iz

if (do_trace) print *, 'energy_BC: mpi_rank,y,ny = ', mpi_rank, mpi_y, mpi_ny
if (do_trace) print *, 'energy_BC: mythread = ', omp_mythread,izs,ize
if (debug .and. omp_master) print *,'begin energy'

  if (lb > 1) then                                                      ! top BC
!$omp single
    eetop = (1.-eetop_frac)*eetop + eetop_frac*eeav(lb+1)
!$omp end single

    do iz=izs,ize
      ee(:,lb,iz) = eetop						! constant energy per unit mass
    end do
    call constant_center_lower(ee)					! vanishing deriv at top
    call haverage_subr (ee, eeav)					! horizontal haverage
    do iz=izs,ize
    do iy=1,lb
      e(:,iy,iz) = r(:,iy,iz)*ee(:,iy,iz)				! smooth top temperature
    end do
    end do
  end if
  call dumpl(ee,'ee','bdry.dmp',4+34*idump)

  if (mpi_y == mpi_ny-1 .and. rbot < 0.) then                           ! bottom BC
!$omp barrier
!$omp single
    rbot=sum(r(:,ub,:))/(mx*mz)                                         ! save density
    ebot=sum(e(:,ub,:))/(mx*mz)                                         ! save density
    tmp(1) = rbot
    tmp(2) = ebot
    call haverage_mpi (tmp, 2)
    rbot = tmp(1)
    ebot = tmp(2)
!$omp end single
    if (debug .and. master) print *,'ub, rbot, ebot =', ub, rbot, ebot
  end if

  if (mpi_y == mpi_ny-1 .and. lb_eampl .ne. 0) then
    do iz=izs,ize
      do ix=1,mx
        efact = 1.+lb_eampl*(cos((ix-97)*2.*pi/mx)+cos((iz-91)*2.*pi/mz))
        e(ix,ub,iz) = e(ix,ub,iz)*efact
        r(ix,ub,iz) = r(ix,ub,iz)*efact**(1./gamma)
        ee(ix,ub,iz) = e(ix,ub,iz)/r(ix,ub,iz)                          ! redo energy per unit mass
      end do
    end do
    call extrapolate_center_log_upper(r)
    do iz=izs,ize
      do iy=ub+1,my
        lnr(:,iy,iz)=alog(r(:,iy,iz))
      end do
    end do
  end if

  call extrapolate_center_upper(ee)                                     ! extrapolate at bottom
  if (ub < my) then
    do iz=izs,ize
    do iy=ub+1,my
      e(:,iy,iz)=r(:,iy,iz)*ee(:,iy,iz)
    end do
    end do
  end if
  call dumpl(ee,'ee','bdry.dmp',5+34*idump)
  call dumpl(e,'e','bdry.dmp',6+34*idump)
if (debug .and. omp_master) print *,'end energy'
END

!-----------------------------------------------------------------------
SUBROUTINE passive_boundary (r,Ux,Uy,Uz,d,dd)
  USE params
  implicit none
  real, dimension(mx,my,mz):: r,Ux,Uy,Uz,d,dd
  integer iy, iz

  call symmetric_center(dd)
  if (mpi_y == 0) then                                                  ! top BC
    do iz=izs,ize
    do iy=1,lb-1
      d(:,iy,iz)=r(:,iy,iz)*dd(:,iy,iz)
    end do
    end do
  end if

  if (mpi_y == mpi_ny-1) then                                           ! bottom BC
    do iz=izs,ize
    do iy=ub+1,my
      d(:,iy,iz)=r(:,iy,iz)*dd(:,iy,iz)
    end do
    end do
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE velocity_boundary (r,xdnr,ydnr,zdnr,Ux,Uy,Uz,px,py,pz,e,Bx,By,Bz)
  USE params
  USE boundary
  implicit none
  real, dimension(mx,my,mz):: r,xdnr,ydnr,zdnr,Ux,Uy,Uz,px,py,pz,e,Bx,By,Bz
  real p, rin, rtot, fmout, fmtot, uyin, uytot
  integer ix, iy, iz

    if (do_trace) print 1, 'velocity_BC: mpi_rank,y,ny,thread = ', mpi_rank, mpi_y, mpi_ny, omp_mythread,izs,ize
1   format(1x,a,6i6)
if (debug .and. omp_master) print *,'begin velocity'
!
!  Top boundary, vanishing velocity derivatives
!
  call constant_center_lower (Ux)
  call constant_face_lower   (Uy)
  call constant_center_lower (Uz)
    if (do_trace) print 1,'velocity: 2', mpi_rank, mpi_y, mpi_ny, omp_mythread,izs,ize
!
!  Top boundary, impose maximum velocity -- relevant when the 
!  density is clambed and cannot become lower
!
  if (lb > 1) then
    do iz=izs,ize
    do ix=1,mx
      do iy=1,lb+2
        ux(ix,iy,iz) = max(min(ux(ix,iy,iz),+uy_max),-uy_max)
        uy(ix,iy,iz) = max(min(uy(ix,iy,iz),+uy_max),-uy_max)
        uz(ix,iy,iz) = max(min(uz(ix,iy,iz),+uy_max),-uy_max)
      end do
      if (uy(ix,lb+1,iz) > 0. .or. uy(ix,lb,iz) > 0.) then
        do iy=1,lb
          uy(ix,iy,iz) = min(uy(ix,iy,iz),uy(ix,lb+1,iz))
        end do
      end if
    end do
    end do
!
!  Recompute mass fluxes
!
    do iz=izs,ize
      do iy=1,lb
        px(:,iy,iz)=xdnr(:,iy,iz)*Ux(:,iy,iz)
        py(:,iy,iz)=ydnr(:,iy,iz)*Uy(:,iy,iz)
        pz(:,iy,iz)=zdnr(:,iy,iz)*Uz(:,iy,iz)
      end do
      py(:,lb+1,iz)=ydnr(:,lb+1,iz)*Uy(:,lb+1,iz)
    end do
  end if
    if(do_trace) print 1,'velocity: 4', mpi_rank, mpi_y, mpi_ny, omp_mythread,izs,ize

  call extrapolate_center_upper(Ux)
  call extrapolate_face_upper  (Uy)
  call extrapolate_center_upper(Uz)
! call constant_center_upper(Ux)
! call constant_center_upper(Uy)
! call constant_center_upper(Uz)
!
!  Recompute mass fluxes
!
  if (mpi_y == mpi_ny-1) then
    do iz=izs,ize
    do iy=ub,my
      px(:,iy,iz)=xdnr(:,iy,iz)*Ux(:,iy,iz)
      py(:,iy,iz)=ydnr(:,iy,iz)*Uy(:,iy,iz)
      pz(:,iy,iz)=zdnr(:,iy,iz)*Uz(:,iy,iz)
    end do
    end do
  end if
    if(do_trace) print 1,'velocity: 5', mpi_rank, mpi_y, mpi_ny, omp_mythread,izs,ize
if (debug .and. omp_master) print *,'end velocity'
END

!-----------------------------------------------------------------------
SUBROUTINE mfield_boundary (Ux,Uy,Uz,Bx,By,Bz)
  USE params
  USE boundary
  implicit none
  real, dimension(mx,my,mz):: Ux,Uy,Uz,Bx,By,Bz
  integer ix, iy, iz
  real f
  character(len=mid), save:: id='mfield_boundary $Id: stellar-atm_bdry_omp_charbotbc.f90,v 1.28 2008/01/16 21:53:10 bob Exp $'

  call print_id (id)
if (debug .and. omp_master) print *,'begin mfield'
if (do_trace) print *, 'mfield_BC: mythread,izs,ize = ', omp_mythread,izs,ize

  if (mpi_y == 0) then                                                  ! top BC
    do iz=izs,ize
      do iy=1,lb
        Bxl(:,iy,iz)=Bx(:,iy,iz)                                        ! store orig bdry B
        Byl(:,iy,iz)=By(:,iy,iz)
        Bzl(:,iy,iz)=Bz(:,iy,iz)
      end do
    end do

    if (omp_master.and.debug) then
      print *,'Bx (before)=',Bx(1,0:9,1)
      print *,'By (before)=',By(1,0:9,1)
      print *,'Bz (before)=',Bz(1,0:9,1)
    end if
    call dumpl(bx,'bx','bdry.dmp',7+34*idump)
    call dumpl(bz,'bz','bdry.dmp',8+34*idump)

    call potential_lower(Bx,By,Bz,lb)                                   ! ->potential

    call dumpl(bx,'bx','bdry.dmp',9+34*idump)
    call dumpl(bz,'bz','bdry.dmp',10+34*idump)
    if (master.and.debug) then
      print *,'Bx (after)=',Bx(1,0:9,1)
      print *,'By (after)=',By(1,0:9,1)
      print *,'Bz (after)=',Bz(1,0:9,1)
    end if
  end if

!-----------------------------------------------------------------------
!
!  Lower boundary: Store & restore original ghost zone field, to avoid
!  destroying div(B).  But set up for computing the electric fiels as
!  if the field in inflows is constant.  This gets multiplied by the
!  inflow velocity field, which vanishes btw inflow and outflow, thus
!  avoiding creating a discontinuity in the electric field.
!
!-----------------------------------------------------------------------

  if (mpi_y == mpi_ny-1) then                                           ! bottom BC
    do iz=izs,ize
      do iy=1,my-ub
        Bxu(:,iy,iz)=Bx(:,ub+iy,iz)                                     ! store orig bdry B
        Byu(:,iy,iz)=By(:,ub+iy,iz)
        Bzu(:,iy,iz)=Bz(:,ub+iy,iz)
      end do
    end do

!   call mixed_center_upper(Bx,Bscale)
!   call mixed_center_upper(Bz,Bscale)
    call extrapolate_center_upper(Bx)
    call extrapolate_face_upper  (By)
    call extrapolate_center_upper(Bz)
    if (Binflow) then
      do iz=izs,ize
      do ix=1,mx
        f = 1./(1.+exp(Uy(ix,ub,iz)/Uy_bdry))                           ! -> 1 in inflows
        do iy=ub+1,my
          Bx(ix,iy,iz) = (1.-f)*Bx(ix,iy,iz) + f*Bx0
          Bz(ix,iy,iz) = (1.-f)*Bz(ix,iy,iz) + f*Bz0
        enddo
      enddo
      enddo
    end if
  end if
if (debug .and. omp_master) print *,'end mfield'
END

!-----------------------------------------------------------------------
SUBROUTINE efield_boundary (Ex, Ey, Ez, Bx_y, Bx_z, By_z, By_x, Bz_x, Bz_y, &
                            Bx, By, Bz, Ux_y, Ux_z, Uy_z, Uy_x, Uz_x, Uz_y, &
                            Ux, Uy, Uz)
  USE params
  USE boundary
  USE arrays, only: scr1, scr2
  implicit none
  real, dimension(mx,my,mz):: Ex, Ey, Ez, Bx_y, Bx_z, By_z, By_x, Bz_x, Bz_y, &
                              Ux_y, Ux_z, Uy_z, Uy_x, Uz_x, Uz_y, &
                              Ux, Uy, Uz, Bx, By, Bz
  integer ix, iy, iz
  real f, ct

  character(len=mid), save:: id='efield_boundary $Id: stellar-atm_bdry_omp_charbotbc.f90,v 1.28 2008/01/16 21:53:10 bob Exp $'

  call print_id (id)
if (debug .and. omp_master) print *,'begin efield'
if (do_trace) print *, 'efield_BC: mythread,izs,ize = ', omp_mythread,izs,ize

!-----------------------------------------------------------------------
!  Top boundary:  The template magnetic field is from a potential
!  extrapolation, and we construct an E-field that drives the real
!  B-field towards the template field.
!-----------------------------------------------------------------------
  ct = 1./t_Bbdry
  if (mpi_y == 0) then                                                  ! top BC
    do iz=izs,ize
     do iy=1,lb
      Ex(:,iy,iz) = Ex(:,lb+1,iz) + (Bzl(:,iy,iz)-Bz(:,iy,iz))* &       ! drive towards potential field
        ct*(ymdn(lb+1)-ymdn(iy))
      Ez(:,iy,iz) = Ez(:,lb+1,iz) - (Bxl(:,iy,iz)-Bx(:,iy,iz))* &       ! drive towards potential field
        ct*(ymdn(lb+1)-ymdn(iy))
      Bx(:,iy,iz)=Bxl(:,iy,iz)                                          ! restore upper bdry B
      By(:,iy,iz)=Byl(:,iy,iz)                                          ! restore upper bdry B
      Bz(:,iy,iz)=Bzl(:,iy,iz)                                          ! restore upper bdry B
     end do
    end do
  end if

!-----------------------------------------------------------------------
!  Bottom boundary:  Ex and Ez are already good, having been computed
!  from regularized velocity and magnetic field values.  However, the
!  values more than 3 grids outside the boundary are contaminated, due
!  to the ydn(Bxz) and ydn(Uxz) interpolations, so these need to be
!  regularized.
!-----------------------------------------------------------------------
  if (mpi_y == mpi_ny-1) then                                           ! bottom BC
    if (Binflow) then
      do iz=izs,ize
        do iy=ub+4,my
          Ex(:,iy,iz) = Ex(:,ub+3,iz)					! regularize
          Ey(:,iy,iz) = Ey(:,ub+3,iz)					! regularize
          Ez(:,iy,iz) = Ez(:,ub+3,iz)					! regularize
        enddo
      enddo
    else
      do iz=izs,ize
        scr1(:,ub,iz) = -(Bzu(:,1,iz)-Bz0)*ct
        scr2(:,ub,iz) = +(Bxu(:,1,iz)-Bx0)*ct
      enddo
      call derivative_face_upper (Ex, scr1) 
      call derivative_face_upper (Ez, scr2) 
    end if

    do iz=izs,ize
     do iy=1,my-ub
      Bx(:,ub+iy,iz)=Bxu(:,iy,iz)                                       ! restore lower bdry B
      By(:,ub+iy,iz)=Byu(:,iy,iz)                                       ! restore lower bdry B
      Bz(:,ub+iy,iz)=Bzu(:,iy,iz)                                       ! restore lower bdry B
     end do
    end do
  end if
if (debug .and. omp_master) print *,'end efield'
END

!-----------------------------------------------------------------------
SUBROUTINE regularize (f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
!
  if (do_trace) print *,'regularize 1:',mpi_rank,omp_mythread,mpi_y
  if (mpi_y ==        0) call symmetric_center_lower (f)
  if (mpi_y == mpi_ny-1) call symmetric_center_upper (f)
  if (do_trace) print *,'regularize 2:',mpi_rank,omp_mythread,mpi_y
END

!-----------------------------------------------------------------------
SUBROUTINE regularize_face (f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
!
  if (mpi_y ==        0) call symmetric_face_lower (f)
  if (mpi_y == mpi_ny-1) call symmetric_face_upper (f)
END

!-----------------------------------------------------------------------
SUBROUTINE ddt_boundary (r,px,py,pz,e,Bx,By,Bz, &
                         drdt,dpxdt,dpydt,dpzdt, &
                         dedt,dBxdt,dBydt,dBzdt, &
                         Ux,Uy,Uz,lnr,p,ee,Cs)
  USE params
  USE arrays, ONLY: scr1,scr2,scr3,scr4,scr5,scr6,wk02,wk06,eeav
  USE forcing
  USE eos
  USE boundary
  USE timeintegration, ONLY: alpha
  implicit none
  real, dimension(mx,my,mz):: r,px,py,pz,e,Bx,By,Bz, &
                              drdt,dpxdt,dpydt,dpzdt, &
                              dedt,dBxdt,dBydt,dBzdt, &
                              Ux,Uy,Uz,lnr,p,ee,Cs
  integer ix,iy,iz,i0,ixp1,izp1
  real :: trelaxb, trelaxt, htopi, gam1, fmaxval
  integer, parameter:: mprint=10
  integer nprint

if (debug .and. omp_master) print *,'begin ddt_boundary'
if (do_trace) print *, 'ddt_BC: mythread,izs,ize = ', omp_mythread,izs,ize

  call barrier_omp ('ddt1')
  call dumpl(r,'r','bdry.dmp',11+34*idump)
  call dumpl(px,'px','bdry.dmp',12+34*idump)
  call dumpl(py,'py','bdry.dmp',13+34*idump)
  call dumpl(pz,'pz','bdry.dmp',14+34*idump)
  call dumpl(ee,'ee','bdry.dmp',15+34*idump)
  call dumpl(e,'e','bdry.dmp',16+34*idump)
  call dumpl(drdt,'drdt','bdry.dmp',17+34*idump)
  call dumpl(dpxdt,'dpxdt','bdry.dmp',18+34*idump)
  call dumpl(dpydt,'dpydt','bdry.dmp',19+34*idump)
  call dumpl(dpzdt,'dpzdt','bdry.dmp',20+34*idump)
  call dumpl(dedt,'dedt','bdry.dmp',21+34*idump)

!-------------------------------------------------------------------------
!  Top Boundary
!-------------------------------------------------------------------------
  if (mpi_y == 0) then                                                  ! top BC
    htopi = gy*rav(lb)/pav(lb)                                          ! inverse pressure scale height
    do iz=izs,ize
    do ix=1,mx
      if (py(ix,lb+1,iz) > 0. .or. py(ix,lb,iz) > 0.) then              ! for inflows ..
        drdt(ix,lb,iz) = drdt(ix,lb,iz) &                               ! let the density
          - htopi*py(ix,lb+1,iz)*(r(ix,lb,iz)/rav(lb)-1.)               ! tend towards the average
        dpydt(ix,lb+1,iz) = dpydt(ix,lb+1,iz) &                         ! and let the velocity
          - htopi*py(ix,lb+1,iz)*abs(py(ix,lb+1,iz))/(0.5*(r(ix,lb,iz)+r(ix,lb+1,iz)))   ! tend to zero --- but so
      end if                                                            ! gently it allows moderate flows

!-----------------------------------------------------------------------
! Ghost zones
!-----------------------------------------------------------------------
      drdt (ix,1:lb-1,iz) = 0.
      dpxdt(ix,1:lb-1,iz) = 0.
      dpydt(ix,1:lb  ,iz) = 0.
      dpzdt(ix,1:lb-1,iz) = 0.
      dedt (ix,1:lb-1,iz) = 0.
      if (do_mhd) then
        dbxdt(ix,1:2,iz)=0.
        dbydt(ix,1:2,iz)=0.
        dbzdt(ix,1:2,iz)=0.
      endif

    end do
    end do
  end if
  if (debug .and. omp_master) print *,'ddtbdry: end top'

!-------------------------------------------------------------------------
!  Bottom Boundary
!-------------------------------------------------------------------------
  call barrier_omp ('ddt2')
! if (debug) print *,'bdry',mpi_y,ym(1),rbot,ebot
  if (mpi_y == mpi_ny-1) then                                           ! bottom BC
    nprint = 0

    do iz=izs,ize
      izp1 = mod(iz,mz)+1
      do ix=1,mx
        if (do_mhd) then
          ixp1 = mod(ix,mx)+1
          pbub(ix,iz) = 0.125*((bx(ix,ub,iz)+bx(ixp1,ub ,iz  ))**2 + &
                            (by(ix,ub,iz)+by(ix  ,ub+1,iz  ))**2 + &
                            (bz(ix,ub,iz)+bz(ix  ,ub  ,izp1))**2)
        else
          pbub(ix,iz) = 0.
        end if
      end do
    end do
    if (master .and. verbose>0 .and. isubstep==1) &
      print *,'pb:',pbub(mx/2+1,mz/2+1),pbot0(mx/2+1,mz/2+1)            ! keep track of spot center
  end if

!-------------------------------------------------------------------------
! Characteristic Boundary Conditions
!   Cell (zone) centered variables are defined at j
!   Face centered variables are defined at j-1/2
!   1...2...3...4...5...6...  ... ....n-5...n-4...n-3...n-2...n-1..n  :cell
! 1...2...3...4...5...6...7.. ... .n-5...n-4...n-3...n-2...n-1...n    :face
!   Boundary is at middle of cell lb and ub
!   Bottom is at mx, Top is at 1: y=height (increases downward)
!-------------------------------------------------------------------------

  do iz = izs, ize
    lnr(:,:,iz)=alog(r(:,:,iz))					! overwritten in pde
    ee(:,:,iz)=e(:,:,iz)/r(:,:,iz)				! overwritten in mhd
  end do

!-----------------------------------------------------------------------
! Cell centered velocities
!-----------------------------------------------------------------------

  call xup_set(Ux, Uxc)
  call yup_set(Uy, Uyc)
  call zup_set(Uz, Uzc)
  call barrier_omp ('ddt3')

!-----------------------------------------------------------------------
! Vertical derivative
!-----------------------------------------------------------------------

  call ddydn_set(r, drdy)

  call ddydn_set(p, dpdy)

  call ydn_set(Uxc, scr5)
  call ddyup_set(scr5, dUxdy)

  call ddydn_set(Uyc, dUydy)

  call ydn_set(Uzc, scr6)
  call ddyup_set(scr6, dUzdy)

  call ydn_set(lnr, scr2)

  do iz=izs,ize
    rdn(:,:,iz) = exp(scr2(:,:,iz))
  end do

! call yup_set(drdy, scr3)
! call yup_set(dpdy, scr5)

!----------------------------------------------------------------------
! Wave speeds 
!----------------------------------------------------------------------

  do iz = izs, ize
    Cs(:,:,iz) = sqrt(p(:,:,iz) / r(:,:,iz) * (dlnPdlnr_E(:,:,iz) + &
                 p(:,:,iz) * dlnPdE_r(:,:,iz) / r(:,:,iz))) 
  end do

  call ydn_set(Cs, Csdn)

!----------------------------------------------------------------------
! Wave amplitudes for outgoing charcteristics
!----------------------------------------------------------------------

  do iz = izs, ize
    scr3(:,:,iz)=Csdn(:,:,iz)**2*drdy(:,:,iz)-dpdy(:,:,iz)
  end do
  call yup_set(scr3, scr5)

  do iz = izs, ize
    d1(:,:,iz) = (Uy(:,:,iz) - Csdn(:,:,iz)) * ( &
                 dpdy(:,:,iz) - &
                 rdn(:,:,iz) * Csdn(:,:,iz) * dUydy(:,:,iz))

!   d4(:,:,iz) = Uyc(:,:,iz) * (Cs(:,:,iz)**2 * scr3(:,:,iz) - &
!                scr5(:,:,iz))
    d4(:,:,iz) = Uyc(:,:,iz) * scr5(:,:,iz)

    d3(:,:,iz) = Uyc(:,:,iz) * dUxdy(:,:,iz)

    d6(:,:,iz) = Uyc(:,:,iz) * dUzdy(:,:,iz)

    d8(:,:,iz) = (Uy(:,:,iz) + Csdn(:,:,iz)) * ( &
                 dpdy(:,:,iz) + &
                 rdn(:,:,iz) * Csdn(:,:,iz) * dUydy(:,:,iz))

  end do

!----------------------------------------------------------------------
  if(mpi_y == mpi_ny - 1) then
!-----------------------------------------------------------------------
! Bottom boundary
!-----------------------------------------------------------------------

  if(isubstep == 1) then
    do iz = izs, ize
      drdt_bot(:,iz) = 0.0
      dpxdt_bot(:,iz) = 0.0
      dpydt_bot(:,iz) = 0.0
      dpzdt_bot(:,iz) = 0.0
      dedt_bot(:,iz) = 0.0
    end do
  else
    do iz = izs, ize
      drdt_bot(:,iz) = alpha(isubstep) * drdt_bot(:,iz)
      dpxdt_bot(:,iz) = alpha(isubstep) * dpxdt_bot(:,iz)
      dpydt_bot(:,iz) = alpha(isubstep) * dpydt_bot(:,iz)
      dpzdt_bot(:,iz) = alpha(isubstep) * dpzdt_bot(:,iz)
      dedt_bot(:,iz) = alpha(isubstep) * dedt_bot(:,iz)
    end do
  end if

  call barrier_omp('ddt36')
  trelaxb = 1.0 / max(1.0 * (maxval(abs(Uyc(:,ub,:))) / dx + 1.0e-30) , &
            1.0 * maxval(r(:,ub,:) * Cs(:,ub,:) * gy / p(:,ub,:)) )

  do iz = izs, ize
    delta_p(:,iz) = 0.38 * ((e(:,ub,iz) + pbub(:,iz))/ ebot - 1.0)
!   delta_p(:,iz) = 5.00 * (e(:,ub,iz) / ebot - 1.0)
  end do
 
!-----------------------------------------------------------------------
! Horizontal derivatives
!-----------------------------------------------------------------------

  call ddxdn32_set(p, ub, dpdx)
  call ddzdn32_set(p, ub, dpdz)

  call ddxup32_set(px, ub, divh_ph)
  call barrier_omp('ddt37')
  call ddzup32_add(pz, ub, divh_ph)

  call ddxup32_set(Ux, ub, divh_uh)
  call barrier_omp('ddt38')
  call ddzup32_add(Uz, ub, divh_uh)

  call xdn32_set(lnr, ub, scra)
  call barrier_omp('ddt39')
  call zdn22_set(scra, scrb)

  call zdn32_set(Ux, ub, scrc)
  call xdn32_set(Uz, ub, scrd)

  call barrier_omp('ddt40')
  do iz = izs, ize
    scra(:,iz) = exp(scrb(:,iz)) * scrc(:,iz) * scrd(:,iz)
    scre(:,iz) = r(:,ub,iz) * Uxc(:,ub,iz)**2 + p(:,ub,iz)
    scrf(:,iz) = r(:,ub,iz) * Uzc(:,ub,iz)**2 + p(:,ub,iz)
  end do

  call ddxdn22_set(scre, divh_pxuh)
  call barrier_omp ('ddt41')
  call ddzup22_add(scra, divh_pxuh)

  call ddzdn22_set(scrf, divh_pzuh)
  call barrier_omp ('ddt42')
  call ddxup22_add(scra, divh_pzuh)

  call zdn32_set(ee, ub, scrb)
  call xdn32_set(ee, ub, scra)

  call barrier_omp ('ddt43')
  do iz = izs, ize
    scra(:,iz) = scra(:,iz) * px(:,ub,iz)
    scrb(:,iz) = scrb(:,iz) * pz(:,ub,iz)
  end do

  call ddxup22_set(scra, divh_euh)
  call barrier_omp ('ddt44')
  call ddzup22_add(scrb, divh_euh)

  call ydn_set(lnr, scr1)
  call xdn32_set(scr1, ub, scra)

  call zdn_set(lnr, scr4)
  call barrier_omp ('ddt45')
  call ydn_set(scr4, scr3)

  call ydn_set(Ux, scr1)
  call xdn32_set(Uy, ub, scrc)

  call ydn_set(Uz, scr2)
  call zdn32_set(Uy, ub, scrd)

  call barrier_omp ('ddt46')
  do iz=izs, ize
    scre(:,iz) = exp(scra(:,iz)) * scr1(:,ub,iz) * scrc(:,iz)
    scrf(:,iz) = exp(scr3(:,ub,iz)) * scr2(:,ub,iz) * scrd(:,iz)
  end do

  call ddxup22_set(scre, divh_pyuh)
  call barrier_omp ('ddt47')
  call ddzup22_add(scrf, divh_pyuh)
  call barrier_omp ('ddt48')

!-----------------------------------------------------------------------
! Apply boundary conditions on ingoing characteristics
! Assume always subsonic, not necessarily sub-Alfvenic
!-----------------------------------------------------------------------

  do iz = izs, ize
    do iy = my/2 + 1, my
      do ix = 1,mx

! Incoming (subsonic) characteristic, Tend toward initial pressure
        if(Uy(ix,ub,iz) .lt. Csdn(ix,ub,iz)) &
          d1(ix,iy,iz) = -rdn(ix,iy,iz) * Csdn(ix,iy,iz) * gy * (1.0 - delta_p(ix,iz)) !+ &
!                      5. * rdn(ix,iy,iz) * Uy(ix,iy,iz) * gy

! All inflow, fix entropy, U_horiz -> 0
        if(Uy(ix,ub,iz) .lt. 0.0) then

           d4(ix,iy,iz) = - Cs(ix,iy,iz) * (p(ix,iy,iz)/r(ix,iy,iz)) * dlnPdE_r(ix,iy,iz) * &
                          ( (e(ix,iy,iz)+p(ix,iy,iz)) * (rbot-r(ix,iy,iz))/r(ix,iy,iz) - &
                          (ebot-e(ix,iy,iz)) ) * (rav(iy)*gy/pav(iy))
!                         ( (e(ix,iy,iz)+p(ix,iy,iz)) * (rbot-r(ix,iy,iz))/r(ix,iy,iz) ) * &
!                         (rav(iy)*gy/pav(iy))

          d3(ix,iy,iz) = Uxc(ix,iy,iz) / trelaxb
          d6(ix,iy,iz) = Uzc(ix,iy,iz) / trelaxb

        end if

!        if(Uy(ix,ub,iz) .lt. -Csdn(ix,ub,iz)) &
!          d8(ix,iy,iz) = rdn(ix,iy,iz) * Csdn(ix,iy,iz) * gy * (1.0 + delta_p(ix,iz)) !- &
!                      5. * py(ix,iy,iz) * gy

      end do
    end do
  end do

!-----------------------------------------------------------------------
! Time derivatives on the boundary
!-----------------------------------------------------------------------

  do iz = izs, ize
    scr1(:,:,iz) = 0.5 * (d1(:,:,iz) + d8(:,:,iz))
  end do

  call ydn_set(d4, scr2)
  call yup_set(scr1, scr3)

  do iz = izs, ize

    scra(:,iz) = (d4(:,ub,iz) + scr3(:,ub,iz)) / Cs(:,ub,iz)**2
    scrb(:,iz) = (scr2(:,ub,iz) + scr1(:,ub,iz)) / Csdn(:,ub,iz)**2
    scre(:,iz) = -r(:,ub,iz) * d3(:,ub,iz) - Uxc(:,ub,iz) * scra(:,iz)
    scrf(:,iz) = -r(:,ub,iz) * d6(:,ub,iz) - Uzc(:,ub,iz) * scra(:,iz)

  end do

  call barrier_omp ('ddt49')
  call xdn22_set(scre, scrc)
  call zdn22_set(scrf, scrd)
  call barrier_omp ('ddt50')

  do iz = izs, ize

    drdt_bot(:,iz) = drdt_bot(:,iz) - scra(:,iz) - divh_ph(:,iz)

    dedt_bot(:,iz) = dedt_bot(:,iz) + d4(:,ub,iz) * r(:,ub,iz) / (p(:,ub,iz) * dlnPdE_r(:,ub,iz)) - &
                     (e(:,ub,iz) + p(:,ub,iz)) * scra(:,iz) / r(:,ub,iz) - &
                     divh_euh(:,iz) - p(:,ub,iz) * divh_uh(:,iz)

    dpydt_bot(:,iz) = dpydt_bot(:,iz) - Uy(:,ub,iz) * scrb(:,iz) + &
                      0.5 * (d1(:,ub,iz) - d8(:,ub,iz)) / Csdn(:,ub,iz) + &
                      rdn(:,ub,iz) * gy - divh_pyuh(:,iz)

    dpxdt_bot(:,iz) = dpxdt_bot(:,iz) + scrc(:,iz) - divh_pxuh(:,iz)

    dpzdt_bot(:,iz) = dpzdt_bot(:,iz) + scrd(:,iz) - divh_pzuh(:,iz)

  end do

!-----------------------------------------------------------------------
! Diffusion - horizontal directions
!-----------------------------------------------------------------------
! S_ii = ddiup(U_i)
! S_ij = 0.5*(ddidn(U_j)+ddjdn(U_i))
! T_ii = 2*d_i*(nu+d_i*nud)*S_ii
! nu_ij = exp(idn(jdn(log(nu+.5*(d_i+d_j)*nud))))
! T_ij = (d_i+d_j)*nu_ij*S_ij
! dedt = dedt + sum_i [(T_ii*S_ii)]
! dedt = dedt + sum_ij [(d_i+d_j)*nu*iup(jup(S_ij))^2]
! dp_idt = dp_idt + ddjdn(T_ii) + sum_j [ddjup(T_ij)]
!-----------------------------------------------------------------------

  call barrier_omp ('ddt51')
  call ddxup32_set(Ux, ub, scra)

  do iz = izs, ize
    do ix = 1, mx
      scrb(ix,iz) = 2.0 * dxm(ix) * (wk06(ix,ub,iz) + dxm(ix) * wk02(ix,ub,iz)) * scra(ix,iz)
    end do
  end do

  call ddxdn22_set(scrb, scrc)

  call barrier_omp ('ddt52')
  do iz = izs, ize
    dpxdt_bot(:,iz) = dpxdt_bot(:,iz) + scrc(:,iz)
    dedt_bot(:,iz) = dedt_bot(:,iz) + scra(:,iz) * scrb(:,iz)
  end do
    
  call ddzup32_set(Uz, ub, scra)

  call barrier_omp ('ddt53')
  do iz = izs, ize
      scrb(:,iz) = 2.0 * dzm(iz) * (wk06(:,ub,iz) + dzm(iz) * wk02(:,ub,iz)) * scra(:,iz)
  end do

  call barrier_omp ('ddt54')
  call ddzdn22_set(scrb, scrc)
  call barrier_omp ('ddt55')

  do iz = izs, ize
    dpzdt_bot(:,iz) = dpzdt_bot(:,iz) + scrc(:,iz)
    dedt_bot(:,iz) = dedt_bot(:,iz) + scra(:,iz) * scrb(:,iz)
  end do

  call ddxdn32_set(Uz, ub, scra)
  call barrier_omp ('ddt56')
  call ddzdn32_add(Ux, ub, scra)

  do iz = izs, ize
    do ix =1, mx
        scrb(ix,iz) = alog(wk06(ix,ub,iz) + 0.5 * (dxm(ix) + dzm(iz)) * wk02(ix,ub,iz))
    end do
  end do

  call xdn22_set(scrb, scrc)
  call barrier_omp ('ddt57')
  call zdn22_set(scrc, scrd)

  call barrier_omp ('ddt58')
  do iz = izs, ize
    do ix = 1, mx
      scrd(ix,iz) = 0.5 * (dxm(ix) + dzm(iz)) * exp(scrd(ix,iz)) * scra(ix,iz)
    end do
  end do

  call barrier_omp ('ddt59')
  call ddxup22_set(scrd, scre)
  call ddzup22_set(scrd, scrf)

  call barrier_omp ('ddt60')
  do iz = izs, ize
    dpxdt_bot(:,iz) = dpxdt_bot(:,iz) + scrf(:,iz)
    dpzdt_bot(:,iz) = dpzdt_bot(:,iz) + scre(:,iz)
  end do

  do iz = izs, ize
    scrb(:,iz) = 0.25 * scra(:,iz)**2
  end do

  call xup22_set(scrb, scrc)
  call barrier_omp ('ddt61')
  call zup22_set(scrc, scrd)

  call barrier_omp ('ddt62')
  do iz = izs, ize
    do ix = 1, mx
      dedt_bot(ix,iz) = dedt_bot(ix,iz) + 2.0 * (dxm(ix) + dzm(iz)) * wk06(ix,ub,iz) * scrd(ix,iz)
    end do
  end do

  call ddxdn_set(Uy, scr1)

  do iz = izs, ize
    do ix = 1, mx
      do iy = 1, my
        scr2(ix,iy,iz) = alog(wk06(ix,iy,iz) + 0.5 * (dxm(ix) + dym(iy)) * wk02(ix,iy,iz))
      end do
    end do
  end do

  call ydn_set(scr2, scr3)
  call xdn32_set(scr3, ub, scra)

  do iz = izs, ize
    do ix = 1, mx
      scra(ix,iz) = (dxm(ix) + dym(ub)) * exp(scra(ix,iz)) * scr1(ix,ub,iz)
    end do
  end do

  call ddxup22_set(scra, scrb)

  do iz = izs, ize
    dpydt_bot(:,iz) = dpydt_bot(:,iz) + scrb(:,iz)
  end do

  do iz = izs, ize
    scr2(:,:,iz) = scr1(:,:,iz)**2
  end do

  call yup_set(scr2, scr3)
  call xup32_set(scr3, ub, scra)

  do iz = izs, ize
    do ix = 1, mx
      dedt_bot(ix,iz) = dedt_bot(ix,iz) + 2.0 * (dxm(ix) + dym(ub)) * wk06(ix,ub,iz) * scra(ix,iz)
    end do
  end do

  call ddzdn_set(Uy, scr1)

  do iz = izs, ize
    do iy = 1, my
      scr2(:,iy,iz) = alog(wk06(:,iy,iz) + 0.5 * (dym(iy) + dzm(iz)) * wk02(:,iy,iz))
    end do
  end do

  call ydn_set(scr2, scr3)
  call barrier_omp ('ddt63')
  call zdn32_set(scr3, ub, scra)

  call barrier_omp ('ddt64')
  do iz = izs, ize
    scra(:,iz) = (dym(ub) + dzm(iz)) * exp(scra(:,iz)) * scr1(:,ub,iz)
  end do

  call barrier_omp ('ddt65')
  call ddzup22_set(scra, scrb)

  call barrier_omp ('ddt66')
  do iz = izs, ize
    dpydt_bot(:,iz) = dpydt_bot(:,iz) + scrb(:,iz)
  end do

  do iz = izs, ize
    scr2(:,:,iz) = scr1(:,:,iz)**2
  end do

  call yup_set(scr2, scr3)
  call barrier_omp ('ddt67')
  call zup32_set(scr3, ub, scra)

  call barrier_omp ('ddt68')
  do iz = izs, ize
    dedt_bot(:,iz) = dedt_bot(:,iz) + 2.0 * (dym(ub) + dzm(iz)) * wk06(:,ub,iz) * scra(:,iz)
  end do

  do iz = izs, ize
    drdt(:,ub,iz) = drdt_bot(:,iz)
    dpxdt(:,ub,iz) = dpxdt_bot(:,iz)
    dpydt(:,ub,iz) = dpydt_bot(:,iz)
    dpzdt(:,ub,iz) = dpzdt_bot(:,iz)
    dedt(:,ub,iz) = dedt_bot(:,iz)
  end do

  end if                                                       ! END BOTTOM
if (debug .and. omp_master) print *,'end bottom'

!-----------------------------------------------------------------------

! damp vertical waves from bottom boundary
  if (pdamp .gt. 0. .and. lb > 1) then
    if (debug .and. omp_master) print *,'begin pdamp'
    call haverage_subr (py,pyav)                                            ! mass flux
    trelaxt = 1.0 / max((maxval(abs(Uyc(:,lb,:))) / dx + 1.0e-30) , &
            maxval(r(:,lb,:) * Cs(:,lb,:) * gy / p(:,lb,:)) )

    do iz = izs, ize
!     do iy=1,my
!       dpydt(:,iy,iz)=dpydt(:,iy,iz)-pyav(iy)/(pdamp*trelaxt)
!     end do
      do iy=lb,min(2*lb-1,ub)
        dpxdt(:,iy,iz)=dpxdt(:,iy,iz)-px(:,iy,iz)*real(2*lb-iy)/(pdamp*trelaxt)
        dpydt(:,iy,iz)=dpydt(:,iy,iz)-py(:,iy,iz)*real(2*lb-iy)/(pdamp*trelaxt)
        dpzdt(:,iy,iz)=dpzdt(:,iy,iz)-pz(:,iy,iz)*real(2*lb-iy)/(pdamp*trelaxt)
      end do
    end do
  end if

!-----------------------------------------------------------------------

  call barrier_omp ('ddt69')
  call dumpl(drdt,'drdt','bdry.dmp',22+34*idump)
  call dumpl(dpxdt,'dpxdt','bdry.dmp',23+34*idump)
  call dumpl(dpydt,'dpydt','bdry.dmp',24+34*idump)
  call dumpl(dpzdt,'dpzdt','bdry.dmp',25+34*idump)
  call dumpl(dedt,'dedt','bdry.dmp',26+34*idump)
  if (do_mhd) then
    call dumpl(Bx,'Bx','bdry.dmp',27+34*idump)
    call dumpl(By,'By','bdry.dmp',28+34*idump)
    call dumpl(Bz,'Bz','bdry.dmp',29+34*idump)
    call dumpl(dBxdt,'dBxdt','bdry.dmp',30+34*idump)
    call dumpl(dBydt,'dBydt','bdry.dmp',31+34*idump)
    call dumpl(dBzdt,'dBzdt','bdry.dmp',32+34*idump)
  end if
!$omp single
    idump=idump+1
!$omp end single

if (debug .and. omp_master) print *,'end ddt_boundary'

END

