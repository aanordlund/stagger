! $Id: stellar-atm_bdry_omp.f90,v 1.32 2008/04/16 22:11:35 aake Exp $
!***********************************************************************
MODULE boundary
  real t_Bbdry, Bscale, lb_eampl, t_bdry, eetop_frac, Uy_bdry
  real rbot, pbot, ebot, eetop, htop
  real Bx0, By0, Bz0, rmin, pb_fact, uy_max
  real(kind=8) rub,pyub,elb,eub,pub,pubt,dpydtub
  real, allocatable, dimension(:,:,:):: Bxl, Byl, Bzl, Bxu, Byu, Bzu
  logical debug, bdry_first, Binflow
  integer verbose

END module

!-----------------------------------------------------------------------
SUBROUTINE init_boundary
  USE params
  USE arrays
  USE boundary
  implicit none
  character(len=mid):: id='$Id: stellar-atm_bdry_omp.f90,v 1.32 2008/04/16 22:11:35 aake Exp $'

  call print_id(id)

  if (mpi_y == 0) then
    lb = 6
    allocate (Bxl(mx,lb,mz),Byl(mx,lb,mz),Bzl(mx,lb,mz))
  else
    lb = 0
  end if
  if (mpi_y == mpi_ny-1) then
    ub = my-5
    allocate (Bxu(mx,my-ub,mz),Byu(mx,my-ub,mz),Bzu(mx,my-ub,mz))
  else
    ub = my
  end if

  t_bdry = 0.01                                                         ! bdry decay time
  t_Bbdry  = 0.01                                                       ! bdry decay time for B
  eetop_frac=0.05                                                       ! fraction of current eetop
  Uy_bdry = 0.1
  lb_eampl = 0.
  rbot = -1.
  htop = 0.
  rmin = 1e-3
  pb_fact = 1.2
  uy_max = 2.                                                           ! 20 km/s with solar scaling
! Bscale = sy*10./my
  Bscale = dym(ub)*10.
  Bx0 = 0.
  By0 = 0.
  Bz0 = 0.
  Binflow = .false.
  debug = .false.
  verbose = 0
  bdry_first = .true.
  do_stratified = .true.
  call read_boundary

  call init_potential_lower
END

!-----------------------------------------------------------------------
SUBROUTINE read_boundary
  USE params
  USE boundary
  implicit none
  namelist /bdry/ lb, ub, t_bdry, t_Bbdry, Bscale, eetop_frac, lb_eampl, &
                  htop, rbot, pbot, ebot, Bx0, By0, Bz0, Binflow, Uy_bdry, &
		  rmin, pb_fact, uy_max, debug, verbose

  rewind (stdin); read (stdin,bdry)
  if (master) write (*,bdry)
END

!-----------------------------------------------------------------------
SUBROUTINE density_boundary (r,lnr,py,e)
  USE params
  USE boundary
  USE eos
  implicit none
  real, dimension(mx,my,mz):: r,lnr,py,e
  integer ix,iy,iz

if (do_trace) print *, 'density_BC: mpi_rank,y,ny = ', mpi_rank, mpi_y, mpi_ny

  call dumpl(r,'r','bdry.dmp',1)

  if (ub < my) then
    call extrapolate_center_upper(lnr)                                  ! extrapolate in the log
    do iz=izs,ize
    do iy=ub+1,my
    do ix=1,mx
      r(ix,iy,iz)=exp(lnr(ix,iy,iz))                                    ! consistent density
    end do 
    end do 
    end do
  end if
  call dumpl(r,'r','bdry.dmp',3)
  call dumpl(e,'e','bdry.dmp',4)
END

!-----------------------------------------------------------------------
SUBROUTINE energy_boundary (r,lnr,Ux,Uy,Uz,e,ee,p,Bx,By,Bz)
  USE params
  USE eos
  USE forcing
  USE boundary
  implicit none
  real, dimension(mx,my,mz):: r,lnr,Ux,Uy,Uz,e,ee,p,Bx,By,Bz
  real s, pb, efact
  real htopi, dlnrdy, lnrmin, lnrmax
  real(kind=8) tmp(3)
  integer ix, iy, iz

if (do_trace) print *, 'energy_BC: mpi_rank,y,ny = ', mpi_rank, mpi_y, mpi_ny

  call barrier_omp ('pav')                                              ! prevent read/write race
  call haverage_subr (r, rav)                                           ! horizontally averaged rho
  call haverage_subr (e, eav)                                           ! horizontally averaged e
  call haverage_subr (p, pav)                                           ! horizontally averaged pressure
  call dumpl(e,'e','bdry.dmp',2)
  call dumpl(p,'p','bdry.dmp',2)

  if (lb > 1) then                                                      ! top BC
    htopi = gy*rav(lb)/pav(lb)                                          ! inverse pressure scale height
    lnrmin = alog(rmin*real(rav(lb)))                                   ! min lnrho value
    lnrmax = alog(3.*real(rav(lb)))                                     ! max lnrho value in inflows
    do iz=izs,ize
      do ix=1,mx
        lnr(ix,lb,iz) = max(lnr(ix,lb,iz),lnrmin)                       ! clamp extreme values
        r(ix,lb,iz) = exp(lnr(ix,lb,iz))                                ! consistent density
      end do
      do ix=1,mx
        if (Uy(ix,lb+1,iz) > 0.) then                                   ! inflow conditions (sensed at lb+1)
         do iy=1,lb-1
          dlnrdy = &
            (lnr(ix,lb+lb-iy,iz)-lnr(ix,lb,iz))/(ym(lb+lb-iy)-ym(lb))   ! log density gradient
          lnr(ix,iy,iz) = lnr(ix,lb,iz) - &                             ! extrapolate ..
            (ym(lb)-ym(iy))*max(dlnrdy,-2*dlnrdy,htopi)                 ! .. on the safe side
          lnr(ix,iy,iz) = max(lnr(ix,iy,iz),lnrmin)                     ! clamp extreme values
          r(ix,iy,iz) = exp(lnr(ix,iy,iz))                              ! consistent density
         end do
        end if
      end do
    end do

    eetop=eav(lb)/rav(lb)                                               ! top boundary energy
    !eetop = (1.-eetop_frac)*eetop + eetop_frac*eav(lb)/rav(lb)          ! evolving (starting from what??)
    do iz=izs,ize
      ee(:,lb,iz) = eetop                                               ! constant energy per unit mass
    end do
    call constant_center_lower(ee)                                      ! vanishing deriv at top
    do iz=izs,ize
    do iy=1,lb
      e(:,iy,iz)=r(:,iy,iz)*ee(:,iy,iz)
    end do
    end do
  end if

  if (mpi_y == mpi_ny-1 .and. rbot < 0.) then                           ! bottom BC
!$omp barrier
!$omp single
    rbot=sum(r(:,ub,:))/(mx*mz)                                         ! save density
    ebot=sum(e(:,ub,:))/(mx*mz)                                         ! save density
    tmp(1) = rbot
    tmp(2) = ebot
    call haverage_mpi (tmp, 2)
    rbot = tmp(1)
    ebot = tmp(2)
!$omp end single
    if (master) print *,'ub, rbot, ebot =', ub, rbot, ebot
  end if

  if (mpi_y == mpi_ny-1 .and. lb_eampl .ne. 0) then                     ! artificial driving (for sunspot)
    do iz=izs,ize
      do ix=1,mx
        efact = 1.+lb_eampl*(cos((ix-97)*2.*pi/mx)+cos((iz-91)*2.*pi/mz))
        e(ix,ub,iz) = e(ix,ub,iz)*efact
        r(ix,ub,iz) = r(ix,ub,iz)*efact**(1./gamma)
        ee(ix,ub,iz) = e(ix,ub,iz)/r(ix,ub,iz)                          ! redo energy per unit mass
      end do
    end do
    call extrapolate_center_log_upper(r)
    do iz=izs,ize
      do iy=ub+1,my
        lnr(:,iy,iz)=alog(r(:,iy,iz))
      end do
    end do
  end if

  if (ub < my) then
    call extrapolate_center_upper(ee)                                   ! extrapolate at bottom
    do iz=izs,ize
    do iy=ub+1,my
      e(:,iy,iz)=r(:,iy,iz)*ee(:,iy,iz)
    end do
    end do
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE passive_boundary (r,Ux,Uy,Uz,d,dd)
  USE params
  implicit none
  real, dimension(mx,my,mz):: r,Ux,Uy,Uz,d,dd
  integer iy, iz

  call symmetric_center(dd)
  if (mpi_y == 0) then                                                  ! top BC
    do iz=izs,ize
    do iy=1,lb-1
      d(:,iy,iz)=r(:,iy,iz)*dd(:,iy,iz)
    end do
    end do
  end if

  if (mpi_y == mpi_ny-1) then                                           ! bottom BC
    do iz=izs,ize
    do iy=ub+1,my
      d(:,iy,iz)=r(:,iy,iz)*dd(:,iy,iz)
    end do
    end do
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE velocity_boundary (r,xdnr,ydnr,zdnr,Ux,Uy,Uz,px,py,pz,e,Bx,By,Bz)
  USE params
  USE boundary
  implicit none
  real, dimension(mx,my,mz):: r,xdnr,ydnr,zdnr,Ux,Uy,Uz,px,py,pz,e,Bx,By,Bz
  real p, rin, rtot, fmout, fmtot, uyin, uytot
  integer ix, iy, iz

    if (do_trace) print 1, 'velocity_BC: mpi_rank,y,ny = ', mpi_rank, mpi_y, mpi_ny, omp_mythread
1   format(1x,a,4i6)
!
!  Top boundary, vanishing velocity derivatives
!
  call constant_center_lower (Ux)
  call constant_face_lower   (Uy)
  call constant_center_lower (Uz)
    if (do_trace) print 1,'velocity: 2', mpi_rank, mpi_y, mpi_ny, omp_mythread
!
!  Top boundary, impose maximum velocity -- relevant when the 
!  density is clambed and cannot become lower
!
  if (lb > 1) then
    do iz=izs,ize
    do ix=1,mx
      do iy=1,lb+1
        ux(ix,iy,iz) = max(min(ux(ix,iy,iz),+uy_max),-uy_max)
        uy(ix,iy,iz) = max(min(uy(ix,iy,iz),+uy_max),-uy_max)
        uz(ix,iy,iz) = max(min(uz(ix,iy,iz),+uy_max),-uy_max)
      end do
      if (uy(ix,lb+1,iz) > 0. .or. uy(ix,lb,iz) > 0.) then
        do iy=1,lb
          uy(ix,iy,iz) = min(uy(ix,iy,iz),uy(ix,lb+1,iz))
        end do
      end if
    end do
    end do
!
!  Recompute mass fluxes
!
    do iz=izs,ize
      do iy=1,lb
        px(:,iy,iz)=xdnr(:,iy,iz)*Ux(:,iy,iz)
        py(:,iy,iz)=ydnr(:,iy,iz)*Uy(:,iy,iz)
        pz(:,iy,iz)=zdnr(:,iy,iz)*Uz(:,iy,iz)
      end do
      py(:,lb+1,iz)=ydnr(:,lb+1,iz)*Uy(:,lb+1,iz)
    end do
  end if
    if(do_trace) print 1,'velocity: 4', mpi_rank, mpi_y, mpi_ny, omp_mythread

  call symmetric_center_upper (Ux)
  call symmetric_face_upper   (Uy)
  call symmetric_center_upper (Uz)
!
!  Recompute mass fluxes
!
  if (mpi_y == mpi_ny-1) then
    do iz=izs,ize
    do iy=ub,my
      px(:,iy,iz)=xdnr(:,iy,iz)*Ux(:,iy,iz)
      py(:,iy,iz)=ydnr(:,iy,iz)*Uy(:,iy,iz)
      pz(:,iy,iz)=zdnr(:,iy,iz)*Uz(:,iy,iz)
    end do
    end do
  end if
    if(do_trace) print 1,'velocity: 5', mpi_rank, mpi_y, mpi_ny, omp_mythread
END

!-----------------------------------------------------------------------
SUBROUTINE mfield_boundary (Ux,Uy,Uz,Bx,By,Bz)
  USE params
  USE boundary
  implicit none
  real, dimension(mx,my,mz):: Ux,Uy,Uz,Bx,By,Bz
  integer ix, iy, iz
  real f
  character(len=mid), save:: id='mfield_boundary $Id: stellar-atm_bdry_omp.f90,v 1.32 2008/04/16 22:11:35 aake Exp $'

  call print_id (id)

  if (mpi_y == 0) then                                                  ! top BC
    do iz=izs,ize
      do iy=1,lb
        Bxl(:,iy,iz)=Bx(:,iy,iz)                                        ! store orig bdry B
        Byl(:,iy,iz)=By(:,iy,iz)
        Bzl(:,iy,iz)=Bz(:,iy,iz)
      end do
    end do

    if (omp_master.and.debug) then
      print *,'Bx (before)=',Bx(1,0:9,1)
      print *,'By (before)=',By(1,0:9,1)
      print *,'Bz (before)=',Bz(1,0:9,1)
    end if
    call potential_lower(Bx,By,Bz,lb)                                   ! ->potential
    if (master.and.debug) then
      print *,'Bx (after)=',Bx(1,0:9,1)
      print *,'By (after)=',By(1,0:9,1)
      print *,'Bz (after)=',Bz(1,0:9,1)
    end if
  end if

!-----------------------------------------------------------------------
!
!  Lower boundary: Store & restore original ghost zone field, to avoid
!  destroying div(B).  But set up for computing the electric fiels as
!  if the field in inflows is constant.  This gets multiplied by the
!  inflow velocity field, which vanishes btw inflow and outflow, thus
!  avoiding creating a discontinuity in the electric field.
!
!-----------------------------------------------------------------------

  if (mpi_y == mpi_ny-1) then                                           ! bottom BC
    do iz=izs,ize
      do iy=1,my-ub
        Bxu(:,iy,iz)=Bx(:,ub+iy,iz)                                     ! store orig bdry B
        Byu(:,iy,iz)=By(:,ub+iy,iz)
        Bzu(:,iy,iz)=Bz(:,ub+iy,iz)
      end do
    end do

    if (Binflow) then
      call symmetric_center_upper (Bx)
      call symmetric_center_upper (Bz)
      call symmetric_face_upper (By)
      do iz=izs,ize
      do ix=1,mx
        f = 1./(1.+exp(Uy(ix,ub,iz)/Uy_bdry))                           ! -> 1 in inflows
        do iy=ub+1,my
          Bx(ix,iy,iz) = (1.-f)*Bx(ix,iy,iz) + f*Bx0
          Bz(ix,iy,iz) = (1.-f)*Bz(ix,iy,iz) + f*Bz0
        enddo
      enddo
      enddo
    else
      call mixed_center_upper(Bx,Bscale)
      call mixed_center_upper(Bz,Bscale)
      call extrapolate_face_upper(By)
    end if
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE efield_boundary (Ex, Ey, Ez, Bx_y, Bx_z, By_z, By_x, Bz_x, Bz_y, &
                            Bx, By, Bz, Ux_y, Ux_z, Uy_z, Uy_x, Uz_x, Uz_y, &
                            Ux, Uy, Uz)
  USE params
  USE boundary
  USE arrays, only: scr1, scr2
  implicit none
  real, dimension(mx,my,mz):: Ex, Ey, Ez, Bx_y, Bx_z, By_z, By_x, Bz_x, Bz_y, &
                              Ux_y, Ux_z, Uy_z, Uy_x, Uz_x, Uz_y, &
                              Ux, Uy, Uz, Bx, By, Bz
  integer ix, iy, iz
  real f, ct

  character(len=mid), save:: id='efield_boundary $Id: stellar-atm_bdry_omp.f90,v 1.32 2008/04/16 22:11:35 aake Exp $'

  call print_id (id)

!-----------------------------------------------------------------------
!  Top boundary:  The template magnetic field is from a potential
!  extrapolation, and we construct an E-field that drives the real
!  B-field towards the template field.
!-----------------------------------------------------------------------
  ct = 1./t_Bbdry
  if (mpi_y == 0) then                                                  ! top BC
    do iz=izs,ize
     do iy=1,lb
      Ex(:,iy,iz) = Ex(:,lb+1,iz) + (Bzl(:,iy,iz)-Bz(:,iy,iz))* &       ! drive towards potential field
        ct*(ymdn(lb+1)-ymdn(iy))
      Ez(:,iy,iz) = Ez(:,lb+1,iz) - (Bxl(:,iy,iz)-Bx(:,iy,iz))* &       ! drive towards potential field
        ct*(ymdn(lb+1)-ymdn(iy))
      Bx(:,iy,iz)=Bxl(:,iy,iz)                                          ! restore upper bdry B
      By(:,iy,iz)=Byl(:,iy,iz)                                          ! restore upper bdry B
      Bz(:,iy,iz)=Bzl(:,iy,iz)                                          ! restore upper bdry B
     end do
    end do
  end if

!-----------------------------------------------------------------------
!  Bottom boundary:  Ex and Ez are already good, having been computed
!  from regularized velocity and magnetic field values.  However, the
!  values more than 3 grids outside the boundary are contaminated, due
!  to the ydn(Bxz) and ydn(Uxz) interpolations, so these need to be
!  regularized.
!-----------------------------------------------------------------------
  if (mpi_y == mpi_ny-1) then                                           ! bottom BC
    if (Binflow) then
      do iz=izs,ize
       do ix=1,mx
        do iy=ub+4,my
          Ex(ix,iy,iz) = Ex(ix,ub+3,iz)                                 ! regularize
          Ez(ix,iy,iz) = Ez(ix,ub+3,iz)                                 ! regularize
        enddo
       enddo
      enddo
    else
      do iz=izs,ize
        scr1(:,ub,iz) = -(Bz_y(:,ub,iz)-Bz0)*ct
        scr2(:,ub,iz) = +(Bx_y(:,ub,iz)-Bx0)*ct
      enddo
      call derivative_face_upper (Ex, scr1) 
      call derivative_face_upper (Ez, scr2) 
    end if

    do iz=izs,ize
     do iy=1,my-ub
      !Bx(:,ub+iy,iz)=Bxu(:,iy,iz)                                       ! restore lower bdry B
      By(:,ub+iy,iz)=Byu(:,iy,iz)                                       ! restore lower bdry B
      !Bz(:,ub+iy,iz)=Bzu(:,iy,iz)                                       ! restore lower bdry B
     end do
    end do
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE regularize (f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
!
  if (do_trace) print *,'regularize 1:',mpi_rank,omp_mythread,mpi_y
  if (mpi_y ==        0) call symmetric_center_lower (f)
  if (mpi_y == mpi_ny-1) call symmetric_center_upper (f)
  if (do_trace) print *,'regularize 2:',mpi_rank,omp_mythread,mpi_y
END

!-----------------------------------------------------------------------
SUBROUTINE regularize_face (f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
!
  if (mpi_y ==        0) call symmetric_face_lower (f)
  if (mpi_y == mpi_ny-1) call symmetric_face_upper (f)
END

!-----------------------------------------------------------------------
SUBROUTINE ddt_boundary (r,px,py,pz,e,Bx,By,Bz, &
                         drdt,dpxdt,dpydt,dpzdt, &
                         dedt,dBxdt,dBydt,dBzdt)
  USE params
  USE forcing
  USE eos
  USE boundary
  implicit none
  real, dimension(mx,my,mz):: r,px,py,pz,e,Bx,By,Bz, &
                              drdt,dpxdt,dpydt,dpzdt, &
                              dedt,dBxdt,dBydt,dBzdt
  integer ix,iz,i0,ixp1,izp1
  real(kind=8) rub1,pyub1,elb1,eub1,ct,pub1,pubt1,dpydtub1,scr,htopi
  integer, parameter:: mprint=10
  integer nprint
  real, dimension(mx,mz)::pb
  real, dimension(5):: tmp


!$omp barrier
  if (debug) print *,'bdry',mpi_y,ym(1),rbot,ebot
    call dumpl(drdt,'drdt','bdry.dmp',5)
    call dumpl(dedt,'dedt','bdry.dmp',6)

  if (mpi_y == mpi_ny-1) then                                           ! bottom BC
    nprint = 0
    if (master .and. verbose>0 .and. isubstep==1) &
      print *,'pb:',pbub(mx/2+1,mz/2+1),pbot0(mx/2+1,mz/2+1)            ! keep track of spot center

    ct = 1.0/t_bdry                                                     ! damping constant
    do iz=izs,ize
      izp1 = mod(iz,mz)+1
      do ix=1,mx
        if (do_mhd) then
          ixp1 = mod(ix,mx)+1
          pb(ix,iz) = 0.125*((bx(ix,ub,iz)+bx(ixp1,ub ,iz  ))**2 + &
                            (by(ix,ub,iz)+by(ix  ,ub+1,iz  ))**2 + &
                            (bz(ix,ub,iz)+bz(ix  ,ub  ,izp1))**2)
        else
          pb(ix,iz) = 0.
        end if
      end do
    end do
!$omp single
    rub = 0.
    eub = 0.
    pub = 0.
    pubt = 0.
    pyub = 0.
    dpydtub = 0.
!$omp end single
    rub1 = 0.
    eub1 = 0.
    pub1 = 0.
    pubt1 = 0.
    pyub1 = 0.
    dpydtub1 = 0.
    do iz=izs,ize
      rub1 = rub1 + sum(r(:,ub,iz))
      eub1 = eub1 + sum(e(:,ub,iz))
      pub1 = pub1 + sum(pbot0(:,iz))
      pubt1 = pubt1 + sum(pbot0(:,iz)+pb(:,iz))
      pyub1 = pyub1 + sum(1.5*py(:,ub,iz)-0.5*py(:,ub-1,iz))
       ! py is regular(?)
      dpydtub1 = dpydtub1 + sum(1.5*dpydt(:,ub,iz)-0.5*dpydt(:,ub-1,iz))
       ! extrapolate to bdry
    end do
!$omp critical
    rub = rub + rub1/(mx*mz)
    eub = eub + eub1/(mx*mz)
    pub = pub + pub1/(mx*mz)
    pubt = pubt + pubt1/(mx*mz)
    pyub = pyub + pyub1/(mx*mz)
    dpydtub = dpydtub + dpydtub1/(mx*mz)
!$omp end critical
!$omp barrier
    tmp(1) = rub
    tmp(2) = eub
    tmp(3) = pub
    tmp(4) = pubt
    tmp(5) = pyub
    call haverage_mpi (tmp, 5)
    rub = tmp(1)
    eub = tmp(2)
    pub = tmp(3)
    pubt = tmp(4)
    pyub = tmp(5)

    if (omp_master .and. debug) then
      print*,'rub',rub
      print*,'eub',eub
      print*,'pub',pub
      print*,'pubt',pubt
      print*,'pyub',pyub
      print*,'dpydtub',dpydtub
      print*,'pbot0(test)',pbot0(10,10)
      print*,'dlnpdE_r(test)',dlnpdE_r(10,10)
    end if

    ct = 0.5/t_bdry
     ! damping constant
    i0=0
    if (omp_master .and. debug) print*,'ct',ct
    do iz=izs,ize
    do ix=1,mx
        !if (omp_master .and. debug.and.ix.eq.1.and.iz.eq.1) print*,'dpydt',dpydt(ix,ub,iz)
      dpydt(ix,ub,iz) = dpydt(ix,ub,iz) - (dpydtub + pyub*ct)/1.5     ! damp out average mass flux
        !if (omp_master .and. debug.and.ix.eq.1.and.iz.eq.1) print*,'dpydt',dpydt(ix,ub,iz)
      dedt(ix,ub,iz) = -(pbot0(ix,iz)+pb(ix,iz)-pubt)*(e(ix,ub,iz)+pbot0(ix,iz))/(gamma*pbot0(ix,iz))*ct ! cancel pressure fluctuation
        !if (omp_master .and. debug.and.ix.eq.1.and.iz.eq.1) print*,'dedt',dedt(ix,ub,iz)
      drdt(ix,ub,iz) = -(pbot0(ix,iz)+pb(ix,iz)-pubt)*r(ix,ub,iz)/(gamma*pbot0(ix,iz))*ct             ! at constant entropy
        !if (omp_master .and. debug.and.ix.eq.1.and.iz.eq.1) print*,'drdt',drdt(ix,ub,iz)
      if (py(ix,ub,iz).lt.0.) then     ! for incoming flows..
        scr = ((e(ix,ub,iz)-ebot) - &
               (e(ix,ub,iz)+pbot0(ix,iz))*(r(ix,ub,iz)-rbot)/r(ix,ub,iz))/(gamma*t_bdry)     ! prop to entropy fluctuation
        !if (omp_master .and. debug.and.i0.eq.0) then
        !  print*,'scr',scr,ix,iz
        !  print*,'dedt',dedt(ix,ub,iz)
        !end if
        dedt(ix,ub,iz) = dedt(ix,ub,iz) - scr*(1.+e(ix,ub,iz)/r(ix,ub,iz)*dlnpdE_r(ix,iz))     ! cancel entropy fluctuation
        !if (omp_master .and. debug.and.i0.eq.0) then
        !  print*,'dedt',dedt(ix,ub,iz)
        !  print*,'drdt',drdt(ix,ub,iz)
        !  i0=1
        !end if
        drdt(ix,ub,iz) = drdt(ix,ub,iz) + scr*dlnpdE_r(ix,iz)     ! at constant pressure
        !  if (omp_master .and. debug.and.i0.eq.0) print*,'drdt',drdt(ix,ub,iz)
        dpxdt(ix,ub,iz) = -px(ix,ub,iz)*ct     ! damp horizontal motions
        dpzdt(ix,ub,iz) = -pz(ix,ub,iz)*ct
      endif
    end do
    end do
  end if

  if (mpi_y == 0) then                                                  ! top BC
    htopi = gy*rav(lb)/pav(lb)                                          ! inverse pressure scale height
    do iz=izs,ize
    do ix=1,mx
      if (py(ix,lb+1,iz) > 0. .or. py(ix,lb,iz) > 0.) then              ! for inflows ..
        drdt(ix,lb,iz) = drdt(ix,lb,iz) &                               ! let the density
          - htopi*py(ix,lb+1,iz)*(r(ix,lb,iz)/rav(lb)-1.)               ! tend towards the average
        dpydt(ix,lb+1,iz) = dpydt(ix,lb+1,iz) &                         ! and let the velocity
          - htopi*py(ix,lb+1,iz)**2/(0.5*(r(ix,lb,iz)+r(ix,lb+1,iz)))   ! tend to zero --- but so
      end if                                                            ! gently it allows moderate flows
      drdt (ix,1:lb-1,iz) = 0.
      dpxdt(ix,1:lb-1,iz) = 0.
      dpydt(ix,1:lb  ,iz) = 0.
      dpzdt(ix,1:lb-1,iz) = 0.
      dedt (ix,1:lb-1,iz) = 0.
      if (do_mhd) then
        dbxdt(ix,1:2,iz)=0.
        dbydt(ix,1:2,iz)=0.
        dbzdt(ix,1:2,iz)=0.
      endif
    end do
    end do
  end if
!$omp barrier
    call dumpl(drdt,'drdt','bdry.dmp',7)
    call dumpl(dpxdt,'dpxdt','bdry.dmp',8)
    call dumpl(dpydt,'dpydt','bdry.dmp',9)
    call dumpl(dpzdt,'dpzdt','bdry.dmp',10)
    call dumpl(dedt,'dedt','bdry.dmp',11)
END

