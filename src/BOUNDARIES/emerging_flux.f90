! $Id: emerging_flux.f90,v 1.38 2005/06/03 20:04:01 aake Exp $

MODULE boundary
  USE params
  real, allocatable, dimension(:,:,:):: B_tube_x, B_tube_y, B_tube_z
  real y_tube, U_tube, v_ampl, B_ampl
  character(len=32) tubefile
END MODULE

!-----------------------------------------------------------------------
SUBROUTINE init_boundary
  USE params
  USE boundary
  USE points
  implicit none
  integer iy,lrec
  real fy

  character(len=80):: id='$Id: emerging_flux.f90,v 1.38 2005/06/03 20:04:01 aake Exp $'
  if (id.ne.' ') print *,id; id=' '

  v_ampl = 0.
  B_ampl = 1.
  y_tube = 10.
  U_tube = 0.3
  tubefile = 'tube_B.dat'

  call read_boundary
  call init_points

  allocate (B_tube_x(mx,my,mz), B_tube_y(mx,my,mz), B_tube_z(mx,my,mz))
  if (tubefile .ne. ' ') then
    print *,'allocating and reading B_tube'
    open (10,file=tubefile,status='unknown',access='direct',recl=lrec(mw))
    read (10,rec=1) B_tube_x
    read (10,rec=2) B_tube_y
    read (10,rec=3) B_tube_z
    close (10)
  else
    print *,'allocating and generating B_tube test'
    do iy=1,my
      fy=min(max((iy-my/2.),-70.),70.)
      fy=exp(-fy)/(exp(fy)+exp(-fy))
      print *,iy,fy
      B_tube_x(:,iy,:)=3.*fy
      B_tube_y(:,iy,:)=0.
      B_tube_z(:,iy,:)=2.*fy
    end do
  end if
  print *,'  done'
END

!-----------------------------------------------------------------------
SUBROUTINE read_boundary
  USE params
  USE boundary
  implicit none
  namelist /bdry/ v_ampl, B_ampl, y_tube, U_tube, tubefile

  lb = 6
  ub = my-5
  if (stdin.ge.0) then
    rewind (stdin); read (stdin,bdry)
  else
    read (*,bdry)
  end if
  write (*,bdry)
END

!-----------------------------------------------------------------------
SUBROUTINE density_boundary (r)
  USE params
  implicit none
  real, dimension(mx,my,mz):: r
!
!  Version for linear cases
!
  call extrapolate_center(r)
END

!-----------------------------------------------------------------------
SUBROUTINE density_boundary_log (r,lnr)
  USE params
  implicit none
  real, dimension(mx,my,mz):: r,lnr
  integer iy,iz
  real ra
!
!  Density boundary condition for open boundaries -- extrapolate in the log
!
  ra=sum(r(:,lb,:))/(mx*mz)
!$omp parallel do private(iz)
  do iz=1,mz
    r(:,lb,iz)=max(r(:,lb,iz),0.1*ra)
  end do

  call extrapolate_center(lnr)
!$omp parallel do private(iz,iy)
  do iz=1,mz
    do iy=1,lb-1
      r(:,iy,iz)=exp(lnr(:,iy,iz))
    end do
    do iy=ub+1,my
      r(:,iy,iz)=exp(lnr(:,iy,iz))
    end do
  end do
END

!-----------------------------------------------------------------------
SUBROUTINE energy_boundary (r,lnr,Ux,Uy,Uz,e,ee,p,Bx,By,Bz)
  USE params
  USE eos
  USE cooling
  USE boundary
  implicit none
  real, dimension(mx,my,mz):: r,lnr,Ux,Uy,Uz,e,ee,p,Bx,By,Bz
  real, dimension(mx,mz):: rlb,elb
  real, save:: rbot=-1., pbot, sbot, ebot=-1.
  real s, pb, ea, eg, b2
  integer ix, iy, iz, ixp1, izp1

!  Compute average total pressure at the lower boundary, save and use
!  as lower boundary condition.  This is bit crude, and can drift when
!  restarting many times.

  ea = 0.
  eg = 0.
!$omp parallel do private(iz,izp1,ixp1,b2), reduction(+:ea,eg)
  do iz=1,mz
    izp1 = iz+1; if (iz.eq.mz) izp1=1
    do ix=1,mx
      ixp1 = ix+1; if (ix.eq.mx) ixp1=1
      b2=bx(ix,lb,iz)**2+bx(ixp1,lb,iz)**2 &
        +by(ix,lb,iz)**2+by(ix,lb+1,iz)**2 &
        +bz(ix,lb,iz)**2+bz(ix,lb,izp1)**2
      ea=ea+e(ix,lb,iz)+0.25*b2
      eg=eg+e(ix,lb,iz)
    end do
  end do
  ea=ea/(mx*mz)
  eg=eg/(mx*mz)
  if (ebot.lt.0.) then
    ebot=ea
    print *,'energy_boundary: lb,ea,eg,ebot=',lb,ea,eg,ebot
  end if

!  Clamp the pressure to prevent low pressure run away, and
!  set the density assuming constant temperature

!$omp parallel do private(iz,izp1,ixp1,b2)
  do iz=1,mz
    izp1 = iz+1; if (iz.eq.mz) izp1=1
    do ix=1,mx
      ixp1 = ix+1; if (ix.eq.mx) ixp1=1
      b2=bx(ix,lb,iz)**2+bx(ixp1,lb,iz)**2 &
        +by(ix,lb,iz)**2+by(ix,lb+1,iz)**2 &
        +bz(ix,lb,iz)**2+bz(ix,lb,izp1)**2
      e(ix,lb,iz)=ebot-0.25*b2
      e(ix,lb,iz)=max(e(ix,lb,iz),0.1*ebot)
      r(ix,lb,iz)=e(ix,lb,iz)/eeav(lb)
      ee(ix,lb,iz)=eeav(lb)
      rlb(ix,iz)=r(ix,lb,iz)
      elb(ix,iz)=e(ix,lb,iz)
    end do
  end do

!  Smooth the density and energy in the lower boundary plane

!$omp parallel do private(iz,izp1,ixp1)
  do iz=1,mz
    izp1 = iz+1; if (iz.eq.mz) izp1=1
    do ix=1,mx
      ixp1 = ix+1; if (ix.eq.mx) ixp1=1
      r(ix,lb,iz) = 0.25*(rlb(ix,iz)+rlb(ixp1,iz)+rlb(ix,izp1)+rlb(ixp1,izp1))
      e(ix,lb,iz) = 0.25*(elb(ix,iz)+elb(ixp1,iz)+elb(ix,izp1)+elb(ixp1,izp1))
    end do
  end do

!  Extrapolate the temperature and compute the ghost zone pressure

  call symmetric_center (ee)
!$omp parallel do private(iz,iy)
  do iz=1,mz
    do iy=1,lb-1
      e(:,iy,iz)=r(:,iy,iz)*ee(:,iy,iz)
    end do
    do iy=ub+1,my
      e(:,iy,iz)=r(:,iy,iz)*ee(:,iy,iz)
    end do
  end do
END

!-----------------------------------------------------------------------
SUBROUTINE passive_boundary (r,Ux,Uy,Uz,d,dd)
  USE params
  implicit none
  real, dimension(mx,my,mz):: r,Ux,Uy,Uz,d,dd
  integer iy, iz

  call symmetric_center(dd)
!$omp parallel do private(iz,iy)
  do iz=1,mz
    do iy=1,lb-1
      d(:,iy,iz)=r(:,iy,iz)*dd(:,iy,iz)
    end do
    do iy=ub+1,my
      d(:,iy,iz)=r(:,iy,iz)*dd(:,iy,iz)
    end do
  end do
END

!-----------------------------------------------------------------------
SUBROUTINE velocity_boundary (r,xdnr,ydnr,zdnr,Ux,Uy,Uz,px,py,pz,e,Bx,By,Bz)
  USE params
  USE boundary
  implicit none
  real, dimension(mx,my,mz):: r,xdnr,ydnr,zdnr,Ux,Uy,Uz,px,py,pz,e,ee,Bx,By,Bz
  real p, rin, rtot, fmout, fmtot, uyin, uytot, Umax
  integer ix, iy, iz

  call boundary_driving (v_ampl,Ux,Uy,Uz)
  Umax = U_tube

!$omp parallel do private(iz)
  do iz=1,mz
    Uy(:,1:lb+1,iz) = max(min(Uy(:,1:lb+1,iz),Umax),-Umax)
    py(:,1:lb+1,iz) = Uy(:,1:lb+1,iz)*ydnr(:,1:lb+1,iz)
    px(:,1:lb+1,iz) = Ux(:,1:lb+1,iz)*xdnr(:,1:lb+1,iz)
    pz(:,1:lb+1,iz) = Uz(:,1:lb+1,iz)*zdnr(:,1:lb+1,iz)
  end do

!  Horizontal velocity BCs:  This is called after the driver routine, so
!  if vanishing horizontal velocities are desired, that should be implemented
!  by setting zero driver amplitude.

  !call     symmetric_center (Ux)
  !call     symmetric_center (Uz)
  call     extrapolate_center (Ux)
  call     extrapolate_center (Uz)

!  Vertical velocity BCs:  At the lower boundary we allow flow through the
!  boundary, so mass can drain out.  The process is self-regulatory, since
!  a strong outflow reduces the gas pressure inside the model, while the
!  value at the boundary is kept constant.

  call antisymmetric_face_upper (Uy)
  call     symmetric_face_lower (Uy)

!  In practice the approach with an open boundary for Uy turned out to be too
!  unstable -- it works for long stretches of time but eventually situations 
!  develop where the runs seem to inevitably crash.  If we instead enforce
!  zero vertical velocity at the very boundary (antisymmetry) but keep the
!  density and pressure fixed then we still allow mass to drain (it can flow
!  to the boundary and it can drain from the point lb+1 with a flow at lb+1/2,
!  but the boundary cannot be the source of uncontrolled inflow.

  !call antisymmetric_face (Uy)

!$omp parallel do private(iz,iy)
  do iz=1,mz
    do iy=1,lb
      px(:,iy,iz) = xdnr(:,iy,iz)*Ux(:,iy,iz)
      py(:,iy,iz) = ydnr(:,iy,iz)*Uy(:,iy,iz)
      pz(:,iy,iz) = zdnr(:,iy,iz)*Uz(:,iy,iz)
    end do
    do iy=ub,my
      px(:,iy,iz) = xdnr(:,iy,iz)*Ux(:,iy,iz)
      py(:,iy,iz) = ydnr(:,iy,iz)*Uy(:,iy,iz)
      pz(:,iy,iz) = zdnr(:,iy,iz)*Uz(:,iy,iz)
    end do
  end do
END

!-----------------------------------------------------------------------
SUBROUTINE mfield_boundary (Ux,Uy,Uz,Bx,By,Bz)
!
!  Boundary conditions on the perpendicular components, Bx and Bz,
!  do not change the div(B)=0 property of the time stepping. 
!  Indirectly, bcs on Bx and Bz, together withg the dib(B)=0 property,
!  help constrain the perpendicular component, By.
!
!  Without a regularization of the B components in the ghost zones
!  the electric field in one ghost zone will "feel" the magnetic field
!  in the other ghost zone, thus creating an unphysical link between
!  the two.  The y-shifted parallel B-components influence the parallel 
!  E-components (as well as the perpendicular E component), over a distance
!  of three grids into the neighboring ghost zone.  The curl of the E-field
!  then spreads the influence another two grids, for a total of five grids.
!  IF the B-field is then regularized all the way through the ghost zone,
!  no further spreading (into the physical zones) occurs.  However, div(B)
!  is then influenced three grids into the physical zone.  An alternative
!  is to regularize the E-field before taking the curl; that will limit
!  the influence fromt the ghost zone at the other end to three grids,
!  keeping the first two grids OK.  One may then choose to regularize
!  B only on the three grids, thus keeping div(B) OK inside the physical
!  mesh.
!
  USE params
  implicit none
  real, dimension(mx,my,mz):: Ux,Uy,Uz,Bx,By,Bz

  !call antisymmetric_center_lower(Bx)
  !call antisymmetric_center_lower(Bz)
  !call extrapolate_center_lower(Bx)     !AA just for now
  !call extrapolate_center_lower(Bz)     !AA just for now

  !call extrapolate_center_upper(Bx)     !AA just for now
  !call extrapolate_center_upper(Bz)     !AA just for now
  call potential_upper (Bx,By,Bz,dx,dy,dz,mx,my,mz,ub-2)
END

!-----------------------------------------------------------------------
SUBROUTINE efield_boundary (Ex, Ey, Ez, Bx_y, Bx_z, By_z, By_x, Bz_x, Bz_y, &
                            Rx, Ry, Rz, Ux_y, Ux_z, Uy_z, Uy_x, Uz_x, Uz_y, &
                            Ux, Uy, Uz)
  USE params
  USE boundary
  !USE stagger
  implicit none
  real, dimension(mx,my,mz):: Ex, Ey, Ez, Bx_y, Bx_z, By_z, By_x, Bz_x, Bz_y, &
                                          Ux_y, Ux_z, Uy_z, Uy_x, Uz_x, Uz_y, &
                              Ux, Uy, Uz, By_y
  real, dimension(mx,2,mz):: Rx, Ry, Rz
  real, dimension(mx,mz):: flb, fub
  real:: Ex_tube, Ez_tube
  integer ix,iy,iz,i0,ib
  real py

  py = y_tube - t*U_tube    ! physical distance
  py = py/dy + 1.           ! index distance
  py = max(py,1.)           ! lower limit
  i0 = py                   ! index
  py = py-i0                ! fraction

  do iz=1,mz
    do iy=2,lb+1
      ib = max(i0+(iy-lb),1)
      do ix=1,mx

!  Set the ghostzone electric fields, first to a contribution from 
!  extrapolations of internal model values.  This contribution must 
!  not contain the cross-boundary advection terms, since they are 
!  computed separately below.  The essential part is the velocity 
!  parallel to the boundary times the magnetic field vertical to the
!  boundary.

        Ex(ix,iy,iz) = +Uz_y(ix,iy,iz)*By_z(ix,iy,iz)
        Ez(ix,iy,iz) = -Ux_y(ix,iy,iz)*By_x(ix,iy,iz)

!  Add the electric field from the y-advection

        Ex_tube = - U_tube*((1.-py)*B_tube_z(ix,ib,iz) + py*B_tube_z(ix,ib+1,iz))
        Ez_tube = + U_tube*((1.-py)*B_tube_x(ix,ib,iz) + py*B_tube_x(ix,ib+1,iz))
        Ex(ix,iy,iz) = B_ampl*Ex_tube + Ex(ix,iy,iz)
        Ez(ix,iy,iz) = B_ampl*Ez_tube + Ez(ix,iy,iz)
      end do
    end do
  end do

  ! Ex(:,lb:lb+1,:) = Ex(:,lb:lb+1,:) + Rx(:,1:2,:)
  ! Ez(:,lb:lb+1,:) = Ez(:,lb:lb+1,:) + Rz(:,1:2,:)

!  Allow only the terms involving parallel velocities at the upper boundary

  do iz=1,mz
    do iy=ub+1,my
      do ix=1,mx
        Ex(ix,iy,iz) = +Uz_y(ix,iy,iz)*By_z(ix,iy,iz)
        Ez(ix,iy,iz) = -Ux_y(ix,iy,iz)*By_x(ix,iy,iz)
      end do
    end do
  end do
!  if (isubstep.eq.1) print *,'efield_boundary: i0,py =',i0,py

END

!-----------------------------------------------------------------------
SUBROUTINE regularize (f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
!
  call symmetric_center(f)
END

!-----------------------------------------------------------------------
SUBROUTINE regularize_face (f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
!
  call symmetric_face(f)
END

!-----------------------------------------------------------------------
SUBROUTINE ddt_boundary (r,px,py,pz,e,Bx,By,Bz, &
                         drdt,dpxdt,dpydt,dpzdt, &
                         dedt,dBxdt,dBydt,dBzdt)
  USE params
  implicit none
  real, dimension(mx,my,mz):: r,px,py,pz,e,Bx,By,Bz, &
                              drdt,dpxdt,dpydt,dpzdt, &
                              dedt,dBxdt,dBydt,dBzdt
  real fvisc, tvisc
  integer iy,iz

  tvisc = 0.001
!$omp parallel do private(iy,iz,fvisc)
  do iz=1,mz
    drdt(:,lb,iz) = 0.
    dedt(:,lb,iz) = 0.
    do iy=1,lb
      dpxdt(:,iy,iz) = 0.
      dpzdt(:,iy,iz) = 0.
    end do
    do iy=1,lb+5
      fvisc=max(min(5+lb-iy,5),0)/5.
      dpxdt(:,iy,iz) = dpxdt(:,iy,iz) - dt*px(:,iy,iz)*(fvisc/tvisc)
      dpydt(:,iy,iy) = dpydt(:,iy,iz) - dt*py(:,iy,iz)*(fvisc/tvisc)
      dpzdt(:,iy,iz) = dpzdt(:,iy,iz) - dt*pz(:,iy,iz)*(fvisc/tvisc)
    end do
    do iy=ub-5,my
      fvisc=max(min(5+iy-ub,5),0)/5.
      dpxdt(:,iy,iz) = dpxdt(:,iy,iz) - dt*px(:,iy,iz)*(fvisc/tvisc)
      dpzdt(:,iy,iz) = dpzdt(:,iy,iz) - dt*pz(:,iy,iz)*(fvisc/tvisc)
    end do
  end do
END

