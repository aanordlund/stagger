! $Id: kink_bdry.f90,v 1.6 2007/04/14 12:43:34 aake Exp $

!-----------------------------------------------------------------------
SUBROUTINE init_boundary
  USE params
  implicit none
  character(len=mid):: id="$Id: kink_bdry.f90,v 1.6 2007/04/14 12:43:34 aake Exp $"

  call print_id (id)                                                    ! print id string
  lb = 6                                                                ! 5 boundary zones
  ub = my-5                                                             ! 5 boundary zones
END

!-----------------------------------------------------------------------
SUBROUTINE read_boundary
  USE params
  implicit none
END

!-----------------------------------------------------------------------
SUBROUTINE density_boundary (r, lnr)
  USE params
  implicit none
  real, dimension(mx,my,mz):: r, lnr

  call symmetric_center (r)
END

!-----------------------------------------------------------------------
SUBROUTINE density_boundary_log (r, lnr)
  USE params
  implicit none
  real, dimension(mx,my,mz):: lnr, r
  integer iy, iz

  call density_boundary (r, lnr)

  do iz=izs,ize
    do iy=1,lb
      lnr(:,iy,iz) = alog(r(:,iy,iz))
    end do
    do iy=ub,my
      lnr(:,iy,iz) = alog(r(:,iy,iz))
    end do
  end do
END

!-----------------------------------------------------------------------
SUBROUTINE energy_boundary (r,lnr,Ux,Uy,Uz,e,ee,p,Bx,By,Bz)
  USE params
  implicit none
  real, dimension(mx,my,mz):: r,lnr,Ux,Uy,Uz,e,ee,p,Bx,By,Bz

  call symmetric_center (e)
END

!-----------------------------------------------------------------------
SUBROUTINE passive_boundary (r,Ux,Uy,Uz,d,dd)
  USE params
  implicit none
  real, dimension(mx,my,mz):: r,Ux,Uy,Uz,d,dd

  call symmetric_center (d)
END

!-----------------------------------------------------------------------
SUBROUTINE velocity_boundary (r,xdnr,ydnr,zdnr,Ux,Uy,Uz,px,py,pz,e,Bx,By,Bz)
  USE params
  implicit none
  real, dimension(mx,my,mz):: r,xdnr,ydnr,zdnr,Ux,Uy,Uz,px,py,pz,e,Bx,By,Bz

  call symmetric_center (px)
  call symmetric_center (pz)
  call antisymmetric_face (py)
END

!-----------------------------------------------------------------------
SUBROUTINE mfield_boundary (Ux,Uy,Uz,Bx,By,Bz)
  USE params
  implicit none
  real, dimension(mx,my,mz):: Ux,Uy,Uz,Bx,By,Bz

  call extrapolate_center (Bx)                                          ! important to use extrapolate here, to make
  call extrapolate_center (Bz)                                          ! the bdry current symmetric across bdry!
END

!-----------------------------------------------------------------------
SUBROUTINE  efield_boundary (Ex, Ey, Ez, Bx_y, Bx_z, By_z, By_x, Bz_x, Bz_y, &
                             Rx, Ry, Rz, Ux_y, Ux_z, Uy_z, Uy_x, Uz_x, Uz_y, &
                             Ux, Uy, Uz, Bxl, Byl, Bzl, Bxu, Byu, Bzu)
  USE params
  implicit none
  real, dimension(mx,my,mz):: Ex, Ey, Ez, Bx_y, Bx_z, By_z, By_x, Bz_x, Bz_y, &
                              Rx, Ry, Rz, Ux_y, Ux_z, Uy_z, Uy_x, Uz_x, Uz_y, &
                              Ux, Uy, Uz, Bxl, Byl, Bzl, Bxu, Byu, Bzu
  call E_driver (Ex, Ez)
END

!-----------------------------------------------------------------------
SUBROUTINE ddt_boundary (r,px,py,pz,e,Bx,By,Bz, &
                         drdt,dpxdt,dpydt,dpzdt, &
                         dedt,dBxdt,dBydt,dBzdt)
  USE params
  implicit none
  real, dimension(mx,my,mz):: r,px,py,pz,e,Bx,By,Bz, &
                              drdt,dpxdt,dpydt,dpzdt, &
                              dedt,dBxdt,dBydt,dBzdt

  call symmetric_center (drdt)
  call symmetric_center (dpxdt)
  call antisymmetric_face (dpydt)
  call symmetric_center (dpzdt)
  call symmetric_center (dedt)
  call extrapolate_center (dBxdt)
  call extrapolate_center (dBzdt)
END

!-----------------------------------------------------------------------
SUBROUTINE regularize (f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f

  call symmetric_center (f)
END

!-----------------------------------------------------------------------
SUBROUTINE regularize_face (f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f

  call symmetric_face (f)
END
