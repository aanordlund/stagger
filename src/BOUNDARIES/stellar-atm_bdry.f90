! $Id: stellar-atm_bdry.f90,v 1.28 2006/05/04 22:36:21 aake Exp $

MODULE potential_lower_m
  USE params
  implicit none
  real(kind=8), allocatable, dimension(:)     :: bxh, bzh
  real,         allocatable, dimension(:,:)   :: fy, fh, kh, byt, bf0
  real,         allocatable, dimension(:,:,:) :: bt, bf
END MODULE

MODULE boundary
  real t_Bbdry, Bscale, lb_eampl, t_bdry
  real rbot, pbot, ebot, eetop, htop
  real Bx0, By0, Bz0
  logical debug
END module

!-----------------------------------------------------------------------
SUBROUTINE init_boundary
  USE params
  USE boundary
  USE potential_lower_m
  implicit none

  character(len=mid):: id='$Id: stellar-atm_bdry.f90,v 1.28 2006/05/04 22:36:21 aake Exp $'
  if (id.ne.' ') print *,id; id=' '

  t_bdry = 0.01
  t_Bbdry  = 1                                  ! bdry decay time for B
  Bscale = sy*10./my
  lb = 6
  ub = my-5
  lb_eampl = 0.
  rbot = -1.
  htop = 0.1
  debug = .false.
  Bx0 = 0.
  By0 = 0.
  Bz0 = 0.
  call read_boundary

  allocate (bxh(my), bzh(my))
  allocate (fy(mx,mz), fh(mx,mz), kh(mx,mz), byt(mx,mz), bf0(mx,mz))
  allocate (bt(mx,lb+1,mz), bf(mx,lb+1,mz))

  call test_fft2d
END

!-----------------------------------------------------------------------
SUBROUTINE read_boundary
  USE params
  USE boundary
  implicit none
  namelist /bdry/ lb, ub, t_bdry, t_Bbdry, Bscale, lb_eampl, htop, &
                  Bx0, By0, Bz0, debug

  if (stdin.ge.0) then
    rewind (stdin); read (stdin,bdry)
  else
    read (*,bdry)
  end if
  write (*,bdry)
  if (mpi_y >        1) lb =  1
  if (mpi_y < mpi_ny-1) ub = my
END

!-----------------------------------------------------------------------
SUBROUTINE density_boundary (r)
  USE params
  implicit none
  real, dimension(mx,my,mz):: r
!
!  Version for linear cases
!
  call extrapolate_center(r)
END

!-----------------------------------------------------------------------
SUBROUTINE density_boundary_log (r,lnr)
  USE params
  USE boundary
  implicit none
  real, dimension(mx,my,mz):: r,lnr
  integer iy,iz
  real hrhomax

  if (mpi_y > 1 .and. mpi_y < mpi_ny-1) return                          ! do nothing in MPI interior

!
!  Density boundary condition for open boundaries -- extrapolate in the log.
!  Also, prevent the density from flipping over to drive input.
!
!$omp parallel private(iz,iy,hrhomax)
  call extrapolate_center(lnr)
  do iz=izs,ize
    hrhomax=htop
    do iy=lb-1,1,-1
      lnr(:,iy,iz)=min(lnr(:,iy,iz),lnr(:,iy+1,iz)-(ym(iy+1)-ym(iy))/htop)
      r(:,iy,iz)=exp(lnr(:,iy,iz))
    end do
    do iy=ub+1,my
      r(:,iy,iz)=exp(lnr(:,iy,iz))
    end do
  end do
!$omp end parallel
END

!-----------------------------------------------------------------------
SUBROUTINE energy_boundary (r,lnr,Ux,Uy,Uz,e,ee,p,Bx,By,Bz)
  USE params
  USE eos
  USE boundary
  implicit none
  real, dimension(mx,my,mz):: r,lnr,Ux,Uy,Uz,e,ee,p,Bx,By,Bz
  real s, pb, pbmax, efact
  integer ix, iy, iz

  if (mpi_y > 1 .and. mpi_y < mpi_ny-1) return                          ! do nothing in MPI interior

!!  call eos1(mx,mz,r(:,ub,:),r(:,ub,:),p=p(:,ub,:))

  if (rbot .lt. 0.) then                                                ! first time
    call extrapolate_center_upper(ee)                                   ! extrapolate at bottom
    call constant_center_lower(ee)                                      ! vanishing deriv at top
    call pressure (r,ee,p)                                              ! single call
    rbot=sum(r(:,ub,:))/(mx*mz)                                         ! save density
    ebot=sum(e(:,ub,:))/(mx*mz)                                         ! save density
    pbot=sum(p(:,ub,:))/(mx*mz)                                         ! save pressure
    if (debug) print *,'ub, rbot, ebot =', ub, rbot, ebot
  end if

  eetop = 0.
  pbmax = 0.
!$omp parallel private(iz,ix,pb,efact), reduction(max:pbmax), reduction(+:eetop)
  do iz=izs,ize
    eetop = eetop + sum(e(:,lb,iz)/r(:,lb,iz))/(mx*mz)
  end do

  if (do_mhd) then
    do iz=izs,ize
      do ix=1,mx
        pbmax = max(pbmax,0.5*by(ix,ub,iz)**2)
      end do
    end do
  else
    pbmax = max(pbmax,0.)
  end if
!$omp end parallel
  if (debug) print *,'eetop =',eetop

!$omp parallel private(iz,ix,efact)
  do iz=izs,ize
    do ix=1,mx
      if (lb_eampl .ne. 0) then
        efact = 1.+lb_eampl*(cos((ix-97)*2.*pi/mx)+cos((iz-91)*2.*pi/mz))
        e(ix,ub,iz) = e(ix,ub,iz)*efact
        r(ix,ub,iz) = r(ix,ub,iz)*efact**(1./gamma)
      end if
      ee(ix,ub,iz) = e(ix,ub,iz)/r(ix,ub,iz)                            ! redo energy per unit mass
      ee(ix,lb,iz) = eetop                                              ! constant energy per unit mass
      e(ix,lb,iz) = eetop*r(ix,lb,iz)                                   ! smooth top temperature
    end do
  end do

  call extrapolate_center_upper(ee)                                     ! extrapolate at bottom
  call constant_center_lower(ee)                                        ! vanishing deriv at top
  if (do_loginterp) then
    call density_boundary_log (r,lnr)                                   ! readjust density
  else
    call density_boundary(r)                                            ! readjust density
  end if

  do iz=izs,ize
    do iy=1,lb-1
      e(:,iy,iz)=r(:,iy,iz)*ee(:,iy,iz)
    end do
    do iy=ub+1,my
      e(:,iy,iz)=r(:,iy,iz)*ee(:,iy,iz)
    end do
  end do
!$omp end parallel
END

!-----------------------------------------------------------------------
SUBROUTINE passive_boundary (r,Ux,Uy,Uz,d,dd)
  USE params
  implicit none
  real, dimension(mx,my,mz):: r,Ux,Uy,Uz,d,dd
  integer iy, iz

  if (mpi_y > 1 .and. mpi_y < mpi_ny-1) return                          ! do nothing in MPI interior

!$omp parallel private(iz,iy)
  call symmetric_center(dd)
  do iz=izs,ize
    do iy=1,lb-1
      d(:,iy,iz)=r(:,iy,iz)*dd(:,iy,iz)
    end do
    do iy=ub+1,my
      d(:,iy,iz)=r(:,iy,iz)*dd(:,iy,iz)
    end do
  end do
!$omp end parallel
END

!-----------------------------------------------------------------------
SUBROUTINE velocity_boundary (r,xdnr,ydnr,zdnr,Ux,Uy,Uz,px,py,pz,e,Bx,By,Bz)
  USE params
  USE boundary
  implicit none
  real, dimension(mx,my,mz):: r,xdnr,ydnr,zdnr,Ux,Uy,Uz,px,py,pz,e,Bx,By,Bz
  real p, rin, rtot, fmout, fmtot, uyin, uytot
  integer ix, iy, iz

  if (mpi_y > 1 .and. mpi_y < mpi_ny-1) return                          ! do nothing in MPI interior

!
!  Bottom boundary
!
  rin  = 1e-30
  rtot = 0.
  fmout = 0.
  fmtot = 0.
!$omp parallel private(iz,iy,ix), reduction(+:fmtot,rin,rtot,fmout)
  do iz=izs,ize
    do ix=1,mx
      fmtot = fmtot +    py(ix,ub,iz)                   ! mass flux
      rtot  = rtot  +  ydnr(ix,ub,iz)                   ! mass
      if (py(ix,ub,iz) .ge. 0.) then                    ! outgoing
        fmout = fmout +   py(ix,ub,iz)                  ! mass flux
      else                                              ! incoming
        rin   = rin   + ydnr(ix,ub,iz)                  ! mass
      end if
    end do
  end do

  call symmetric_center_upper (Ux)
  call symmetric_face_upper   (Uy)
  call symmetric_center_upper (Uz)
!
!  Top boundary, vanishing values
!
  call vanishing_center_lower (Ux)
  call symmetric_face_lower   (Uy)
  call vanishing_center_lower (Uz)
!
!  Gradually vanishing vertical mass flux in upper ghost zone
!
  do iz=izs,ize
    do iy=1,lb
      py(:,iy,iz)=iy/real(lb+1)*py(:,lb+1,iz)
    end do
  end do  
!
!  Recompute mass fluxes
!
  do iz=izs,ize
    do iy=1,lb
      px(:,iy,iz)=xdnr(:,iy,iz)*Ux(:,iy,iz)
      py(:,iy,iz)=ydnr(:,iy,iz)*Uy(:,iy,iz)
      pz(:,iy,iz)=zdnr(:,iy,iz)*Uz(:,iy,iz)
    end do
    do iy=ub,my
      px(:,iy,iz)=xdnr(:,iy,iz)*Ux(:,iy,iz)
      py(:,iy,iz)=ydnr(:,iy,iz)*Uy(:,iy,iz)
      pz(:,iy,iz)=zdnr(:,iy,iz)*Uz(:,iy,iz)
    end do
  end do
!$omp end parallel

  uytot = fmtot/rtot
  uyin = -fmout/rin                                     ! target incoming velocity
  if (debug) print *,'uyin, uytot =', uyin, uytot
END

!-----------------------------------------------------------------------
SUBROUTINE mfield_boundary (Ux,Uy,Uz,Bx,By,Bz)
  USE params
  USE boundary
  implicit none
  real, dimension(mx,my,mz):: Ux,Uy,Uz,Bx,By,Bz
  integer ix, iy, iz

  if (mpi_y > 1 .and. mpi_y < mpi_ny-1) return                          ! do nothing in MPI interior

!
! Require the horizontal field components to vanish
!
! Bottom boundary
!$omp parallel private(iz,iy), shared(Bscale)
!  call antisymmetric_center_upper(Bx)           ! bottom boundary:
!  call antisymmetric_center_upper(Bz)           ! vanishing horizontal

! call mixed_center_upper(Bx,Bscale)
! call mixed_center_upper(Bz,Bscale)
! call symmetric_face_upper(By)   ! TEMPORARY HACK: to be replaced

  call extrapolate_center_upper(Bx,Bscale)
  call extrapolate_center_upper(Bz,Bscale)
  call extrapolate_face_upper(By,Bscale)
  do iz=izs,ize
    do ix=1,mx
      if (Uy(ix,ub,iz) .lt. 0.) then
        do iy=ub+1,my
          Bx(ix,iy,iz) = Bx0
          By(ix,iy,iz) = By0
          Bz(ix,iy,iz) = Bz0
        enddo
      end if
    enddo
  enddo

! Top boundary
!  call extrapolate_center_lower(Bx)             ! top boundary:
!  call extrapolate_center_lower(Bz)             ! extrapolate
!$omp end parallel
  call potential_lower(Bx,By,Bz,lb+1)

END

!-----------------------------------------------------------------------
SUBROUTINE efield_boundary (Ex, Ey, Ez, Bx_y, Bx_z, By_z, By_x, Bz_x, Bz_y, &
                            Rx, Ry, Rz, Ux_y, Ux_z, Uy_z, Uy_x, Uz_x, Uz_y, &
                            Ux, Uy, Uz)
  USE params
  USE boundary
  implicit none
  real, dimension(mx,my,mz):: Ex, Ey, Ez, Bx_y, Bx_z, By_z, By_x, Bz_x, Bz_y, &
                              Ux_y, Ux_z, Uy_z, Uy_x, Uz_x, Uz_y, &
                              Ux, Uy, Uz
  real, dimension(mx,2,mz):: Rx, Ry, Rz
  integer ix, iy, iz

  if (mpi_y > 1 .and. mpi_y < mpi_ny-1) return                          ! do nothing in MPI interior

!
!  Allow the top B-field to evolve
!
!$omp parallel private(iz,iy)
! call extrapolate_center_lower (Ex)                  ! allow extrap
! call extrapolate_center_lower (Ez)                  ! allow extrap
  do iz=izs,ize
    do iy=1,lb
      Ex(:,iy,iz) = (Uz_y(:,iy,iz)*By_z(:,iy,iz)) - &
                    (Uy_z(:,iy,iz)*Bz_y(:,iy,iz))

      Ez(:,iy,iz) = (Uy_x(:,iy,iz)*Bx_y(:,iy,iz)) - &
                    (Ux_y(:,iy,iz)*By_x(:,iy,iz))
    end do
  end do
!
! Bottom boundary - B -> B0
!
  call derivative_face_upper (Ex, -(Bz_y-Bz0)*(1./t_Bbdry)) 
  call derivative_face_upper (Ez, +(Bx_y-Bx0)*(1./t_Bbdry)) 
!$omp end parallel
END

!-----------------------------------------------------------------------
SUBROUTINE regularize (f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f

  if (mpi_y > 1 .and. mpi_y < mpi_ny-1) return                          ! do nothing in MPI interior

  if (mpi_y ==      1) call symmetric_center_lower (f)
  if (mpi_y == mpi_ny) call symmetric_center_upper (f)
END

!-----------------------------------------------------------------------
SUBROUTINE regularize_face (f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f

  if (mpi_y > 1 .and. mpi_y < mpi_ny-1) return                          ! do nothing in MPI interior

  if (mpi_y ==      1) call symmetric_face_lower (f)
  if (mpi_y == mpi_ny) call symmetric_face_upper (f)
END

!-----------------------------------------------------------------------
SUBROUTINE ddt_boundary (r,px,py,pz,e,Bx,By,Bz, &
                         drdt,dpxdt,dpydt,dpzdt, &
                         dedt,dBxdt,dBydt,dBzdt)
  USE params
  USE eos
  USE boundary
  implicit none
  real, dimension(mx,my,mz):: r,px,py,pz,e,Bx,By,Bz, &
                              drdt,dpxdt,dpydt,dpzdt, &
                              dedt,dBxdt,dBydt,dBzdt
  integer ix,iz,i0,ixp1,izp1
  real, dimension(mx,mz)::pb
  real rub,pyub,elb,eub,ct,pub,pubt,dpydtub,scr

  if (mpi_y > 1 .and. mpi_y < mpi_ny-1) return                          ! do nothing in MPI interior

  rub = 0.
  eub = 0.
  pub = 0.
  pubt = 0.
  pyub = 0.
  dpydtub = 0.
!$omp parallel private(iz,ix,izp1,ixp1), reduction(+:rub,eub,pub,pubt,pyub,dpydtub)
  do iz=izs,ize
    izp1 = mod(iz,mz)+1
    do ix=1,mx
      if (do_mhd) then
        ixp1 = mod(ix,mx)+1
        pb(ix,iz) = 0.125*((bx(ix,ub,iz)+bx(ixp1,ub ,iz  ))**2 + &
                          (by(ix,ub,iz)+by(ix  ,ub+1,iz  ))**2 + &
                          (bz(ix,ub,iz)+bz(ix  ,ub  ,izp1))**2)
      else
        pb(ix,iz) = 0.
      end if
    end do
  end do
  do iz=izs,ize
    rub = rub + sum(r(:,ub,iz))
    eub = eub + sum(e(:,ub,iz))
    pub = pub + sum(pbot0(:,iz))
    pubt = pubt + sum(pbot0(:,iz)+pb(:,iz))
    pyub = pyub + sum(1.5*py(:,ub,iz)-0.5*py(:,ub-1,iz))                                        ! py is regular(?)
    dpydtub = dpydtub + sum(1.5*dpydt(:,ub,iz)-0.5*dpydt(:,ub-1,iz))                            ! extrapolate to bdry
  end do
!$omp end parallel
  rub = rub/(mx*mz)
  eub = eub/(mx*mz)
  pub = pub/(mx*mz)
  pubt = pubt/(mx*mz)
  pyub = pyub/(mx*mz)
  dpydtub = dpydtub/(mx*mz)
  if (debug) then
    print*,'rub',rub
    print*,'eub',eub
    print*,'pub',pub
    print*,'pubt',pubt
    print*,'pyub',pyub
    print*,'dpydtub',dpydtub
  end if

  ct = 0.5/t_bdry                                                                               ! damping constant
  if (debug) print*,'ct',ct
!$omp parallel private(iz,ix,scr)
  do iz=izs,ize
    do ix=1,mx
      dpydt(ix,ub,iz) = dpydt(ix,ub,iz) - (dpydtub + pyub*ct)/1.5                               ! damp out average mass flux
      dedt(ix,ub,iz) = -(pbot0(ix,iz)+pb(ix,iz)-pubt)*(e(ix,ub,iz)+pbot0(ix,iz))/(gamma*pbot0(ix,iz))*ct ! cancel pressure fluctuation
      drdt(ix,ub,iz) = -(pbot0(ix,iz)+pb(ix,iz)-pubt)*r(ix,ub,iz)/(gamma*pbot0(ix,iz))*ct               ! at constant entropy
      if (py(ix,ub,iz).lt.0.) then                                                              ! for incoming flows..
        scr = ((e(ix,ub,iz)-ebot) - &
               (e(ix,ub,iz)+pbot0(ix,iz))*(r(ix,ub,iz)-rbot)/r(ix,ub,iz))/(gamma*t_bdry)        ! prop to entropy fluctuation
        dedt(ix,ub,iz) = dedt(ix,ub,iz) - scr*(1.+e(ix,ub,iz)/r(ix,ub,iz)*dlnpdE_r(ix,iz))      ! cancel entropy fluctuation
        drdt(ix,ub,iz) = drdt(ix,ub,iz) + scr*dlnpdE_r(ix,iz)                                   ! at constant pressure
        dpxdt(ix,ub,iz) = -px(ix,ub,iz)*ct                                                      ! damp horizontal motions
        dpzdt(ix,ub,iz) = -pz(ix,ub,iz)*ct
      endif
      drdt(ix,1,iz)=0.
      dpxdt(ix,1,iz)=0.
      dpydt(ix,1,iz)=0.
      dpzdt(ix,1,iz)=0.
      dedt(ix,1,iz)=0.
      if (do_mhd) then
        dbxdt(ix,1,iz)=0.
        dbydt(ix,1,iz)=0.
        dbzdt(ix,1,iz)=0.
      endif
    end do
  end do
!$omp end parallel
END

