! $Id: points.f90,v 1.3 2006/03/31 02:44:27 aake Exp $
!-----------------------------------------------------------------------
MODULE points

  USE params
  USE units

  TYPE point
     integer,dimension(2) :: pos
     real,dimension(4) :: data
     type(point),pointer :: next
  end TYPE point

  integer :: zrange,xrange,p,nrpoints,ipsnap,writing
  real,allocatable,dimension(:,:) :: w,vvz,vvx
  real :: ampl,seed,dzdx2,ig,granr,pd,life_t,upd,avoid
  integer,allocatable,dimension(:,:) :: granlane,avoidarr

  Type(point), pointer :: first
  Type(point), pointer :: previous
  Type(point), pointer :: current
  Type(point), pointer :: last
  Type(point), pointer,save :: firstlev
  Type(point), pointer,save :: secondlev
  Type(point), pointer,save :: thirdlev
  
CONTAINS

!-----------------------------------------------------------------------
subroutine init_points

USE params

character(len=mid):: id='init_points $Id: points.f90,v 1.3 2006/03/31 02:44:27 aake Exp $'
call print_id(id)

allocate (w(mz,mx), vvz(mz,mx), vvx(mz,mx), granlane(mz,mx), avoidarr(mz,mx))

! Every point has 6 values associated with it: data(1-6). 
! These contain, y,z,current amplitude, amplitude at t=t_0, t_0, and life_time.

seed=53412

! Gives intergranular radius / (granular+intergranular radius)

ig=0.3

! Gives average radius of granule (no smaller than 6 grid points across)

granr=max(0.8*1.e8/u_l,4*maxval(dzm),4*maxval(dxm))

! Fractional difference in granule power

pd=0.1

! Gives exponential power of evolvement. Higher power faster growth/decay.

p=2

! Fractional distance, where after emergence, no granule can emerge 
! whithin this radius.(In order to not 'overproduce' granules). 
! This limit is unset when granule reaches t_0.

avoid=0.8

! Lifetime of granule
! Now also resolution dependant(5 min for granular scale)
life_t=(60*5/U_t)*(granr/(0.5*5e8/u_l))

dzdx2=maxval(dzm)**2+maxval(dxm)**2

print *,dzdx2

! Typical central velocity of granule(1.5e5 cm/s=1.5km/s)
! Has to be multiplied by the smallest distance, since velocity=ampl/dist
!should now also be dependant on smallest granluar scale resolvable. 

ampl=sqrt(dzdx2)*0.28e6/(u_u*granr)

zrange=min(nint(1.5*granr*(1+ig)/minval(dzm)),nint(mz/2.0)-1)
xrange=min(nint(1.5*granr*(1+ig)/minval(dxm)),nint(mx/2.0)-1)

granlane(:,:)=0
avoidarr(:,:)=0

call initpoints

end subroutine

!-----------------------------------------------------------------------
  subroutine addpoint

    type(point),pointer :: newpoint
    
    allocate(newpoint)
    if (associated(newpoint%next)) nullify(newpoint%next)
    previous => current
    
    current%next => newpoint
    current => newpoint
    last => current
 
  end subroutine addpoint

!-----------------------------------------------------------------------
  subroutine rmpoint

    if (associated(current%next)) then
       previous%next => current%next
       if (associated(first%next,current)) then
         first%next => current%next
         if (associated(firstlev,first)) firstlev%next => current%next
         if (associated(secondlev,first)) secondlev%next => current%next
         if (associated(thirdlev,first)) thirdlev%next => current%next
       endif
       deallocate(current)
       current => previous%next
    else
       last => previous
       nullify(previous%next)
       deallocate(current)
       current => previous
!!$      * BE AWARE THAT PREVIOUS IS NOT NOT ALLOCATED TO THE RIGHT POSITION
    endif
  end subroutine rmpoint

!-----------------------------------------------------------------------
  subroutine gtnextpoint

    previous => current
    current => current%next

  end subroutine gtnextpoint

!-----------------------------------------------------------------------
  subroutine reset

    current => first
    previous => first

  end subroutine reset

!-----------------------------------------------------------------------
!  subroutine wrpoints(issnap)
!
!    integer :: rn,issnap,isnap2,isnn,chnul
!    real,dimension(6) :: posdata
!    character(len=30) :: filename
!    rn=1
!    filename(1:6)="points"
!    chnul=ichar('0')
!    isnn=7
!    isnap2=issnap
!    write (*,*) 'Writing velocity field snap',issnap
!    call oflush
!    if (isnap2.ge.100) then 
!       filename(isnn:isnn)=char(floor(isnap/100.0)+chnul)
!       isnn=isnn+1
!       isnap2=isnap2-floor(isnap/100.0)*100
!       if (isnap2 .lt. 9) then
!          filename(isnn:isnn)='0'
!          isnn=isnn+1
!       endif
!    endif
!    if (isnap2.ge.10) then
!       filename(isnn:isnn)=char(floor(isnap2/10.0)+chnul)
!       isnn=isnn+1
!       isnap2=isnap2-floor(isnap2/10.0)*10
!    endif
!    filename(isnn:isnn)=char(chnul+isnap2)
!    filename(isnn+1:)=".dat"
!    OPEN(10,file=trim(filename),status="replace",access="direct",recl=lrec(6))
!    DO WHILE (associated(current%next))
!       posdata(1:2)=real(current%pos)
!       posdata(3:6)=current%data
!       write(10,rec=rn) posdata
!       call gtnextpoint
!       rn=rn+1
!    END DO
!
!    close (10)
!
!    call reset
!
!  end subroutine wrpoints

!-----------------------------------------------------------------------
  subroutine wrpointsscr

    integer :: rn
    real,dimension(6) :: posdata
    rn=1

    print*,'Writing Scratch velocity data'
    OPEN(10,file="points.scr",status="replace",access="direct",recl=lrec(6))
    DO WHILE (associated(current%next))
       posdata(1:2)=real(current%pos)
       posdata(3:6)=current%data
       write(10,rec=rn) posdata
       call gtnextpoint
       rn=rn+1
    END DO
    close (10)
    call reset

  end subroutine wrpointsscr

!-----------------------------------------------------------------------
!  subroutine rdpoints(isnap2)
!
!    real,dimension(6) :: tmppoint
!    integer :: iost,rn,isnap2,isnn,isnscr,chnul
!    logical :: ex
!    character(len=30) :: filename
!    rn=1
!    filename(1:6)="points"
!    chnul=ichar('0')
!    isnn=7
!    isnscr=isnap2
!    if (isnap2.ge.100) then 
!       filename(isnn:isnn)=char(floor(isnap2/100.0)+chnul)
!       isnn=isnn+1
!       isnap2=isnap2-floor(isnap2/100.0)*100
!       if (isnap2 .le. 9) then
!          filename(isnn:isnn)='0'
!          isnn=isnn+1
!       endif
!    endif
!    if (isnap2.ge.10) then
!       filename(isnn:isnn)=char(floor(isnap2/10.0)+chnul)
!       isnn=isnn+1
!       isnap2=isnap2-int(floor(isnap2/10.0)*10)
!    endif
!    filename(isnn:isnn)=char(isnap2+chnul)
!    filename(isnn+1:)=".dat"
!    inquire(file=trim(filename),exist=ex)
!
!    if (ex) then 
!       open(10,file=trim(filename),status="unknown",access="direct",recl=lrec(6))
!       print*,'reading velocity field nr',isnscr
!       iost=0
!
!       do 
!          if (.not.iost.eq.0) exit
!          read(10,iostat=iost,rec=rn) tmppoint
!          if (iost.eq.0) then
!             current%pos(:)=int(tmppoint(1:2))
!             current%data(:)=tmppoint(3:6)
!             call addpoint
!             rn=rn+1
!
!          else 
!             
!             last => previous
!             nullify(previous%next)
!             deallocate(current)
!             current => previous
!             
!          endif
!
!       end do
!       print*,'read ',rn-1,' points'
!       call reset
!    end if
!
!
!  END subroutine rdpoints

!-----------------------------------------------------------------------
  subroutine initpoints

    allocate(first)
    if (associated(first%next)) nullify(first%next)
    current => first
  
  end subroutine initpoints

!-----------------------------------------------------------------------
  subroutine wrpoints(issnap)
    USE params

    integer :: rn,isnn,chnul
    integer,intent(in) :: issnap
    real,dimension(6) :: posdata
    character(len=64) :: filename

    rn=1
    i=index(file,'.',back=.true.)-1
    write (filename,'(a,"-points",I3.3,".dat")') file(1:i),issnap 
    OPEN(10,file=filename,status="replace",access="direct",recl=lrec(6))
    DO WHILE (associated(current%next))
       posdata(1:2)=real(current%pos)
       posdata(3:6)=current%data
       write(10,rec=rn) posdata
       call gtnextpoint
       rn=rn+1
    END DO
    close (10)

    write (*,*) 'Wrote velocity field snap',issnap,'including ',rn,' points'
    call oflush

    call reset

  end subroutine wrpoints

!-----------------------------------------------------------------------
  subroutine rdpoints(isnap2)
    USE params

    real,dimension(6) :: tmppoint
    integer :: iost,rn,isnn,isnscr,chnul
    integer,intent(in) :: isnap2
    logical :: ex
    character(len=64) :: filename

    rn=1
    isnscr=isnap2
    i=index(file,'.',back=.true.)-1
    write (filename,'(a,"-points",I3.3,".dat")') file(1:i),isnap2 
    inquire(file=filename,exist=ex)

    if (ex) then 
       open(10,file=filename,status="unknown",access="direct",recl=lrec(6))
       print*,'reading velocity field nr',isnap2
       iost=0

       do 
          if (.not.iost.eq.0) exit
          read(10,iostat=iost,rec=rn) tmppoint
          if (iost.eq.0) then
             current%pos(:)=int(tmppoint(1:2))
             current%data(:)=tmppoint(3:6)
             call addpoint
             rn=rn+1

          else 
             
             last => previous
             nullify(previous%next)
             deallocate(current)
             current => previous
             
          endif

       end do
       print*,'read ',rn-1,' points'
       call reset
    end if

  end subroutine rdpoints
END module points

!-----------------------------------------------------------------------
      FUNCTION ran1(idum)
      INTEGER idum,IA,IM,IQ,IR,NTAB,NDIV
      REAL ran1,AM,EPS,RNMX
      PARAMETER (IA=16807,IM=2147483647,AM=1./IM,IQ=127773,IR=2836, &
      NTAB=32,NDIV=1+(IM-1)/NTAB,EPS=1.2e-7,RNMX=1.-EPS)
      INTEGER j,k,iv(NTAB),iy
      SAVE iv,iy
      DATA iv /NTAB*0/, iy /0/
      if (idum.le.0.or.iy.eq.0) then
        idum=max(-idum,1)
        do 11 j=NTAB+8,1,-1
          k=idum/IQ
          idum=IA*(idum-k*IQ)-IR*k
          if (idum.lt.0) idum=idum+IM
          if (j.le.NTAB) iv(j)=idum
11      continue
        iy=iv(1)
      endif
      k=idum/IQ
      idum=IA*(idum-k*IQ)-IR*k
      if (idum.lt.0) idum=idum+IM
      j=1+iy/NDIV
      iy=iv(j)
      iv(j)=idum
      ran1=min(AM*iy,RNMX)
      return
      END FUNCTION

!-----------------------------------------------------------------------
subroutine resetarr

  USE params
  USE points

  w(:,:)=0.0
  granlane(:,:)=0
  avoidarr(:,:)=0

end subroutine resetarr


