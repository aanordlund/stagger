! $Id: drive3_omp.f90,v 1.1 2006/03/29 21:29:33 aake Exp $
subroutine driveinit

USE params
USE points
implicit none

real :: timer,idum,elapsed,ran1

character(len=80):: id='$Id: drive3_omp.f90,v 1.1 2006/03/29 21:29:33 aake Exp $'

call print_id(id)

!$omp master

timer=elapsed()
call resetarr
call make_newpoint
nrpoints=1
do 
   if (minval(w/(1-avoidarr+1e-20)).ge.ampl/(granr*(1+ig))) exit

   call addpoint
   call make_newpoint
   nrpoints=nrpoints+1

! Initital t_0's different in initital drawing, must update   
   
  current%data(3)=t+(ran1(seed)*2-1)*current%data(4)*(-alog(ampl*sqrt(dzdx2)/  &
                  (current%data(2)*granr*(1-ig))))**(1./p)
  current%data(1)=current%data(2)*exp(-((t-current%data(3))/current%data(4))**p)

! Update arrays with new data

   call drawupdate

end do

! And reset

call reset

timer=elapsed()

write (*,*) 'startup: ',timer
call oflush

!$omp end master
!$omp barrier

end subroutine driveinit

!--------------------------------------------------------------

subroutine drive3(level,Uz,Ux)

USE params
USE points
!USE stagger
USE forcing
implicit none

real,dimension(mx,my,mz) :: Uz,Ux

real :: averU,nvor
real,dimension(mz,mx) :: wscr,wscr2
integer,intent(in) :: level
integer :: ix,iy,iz,nnrpoints

!$omp master

call resetarr

vvx=0.          ! make sure velocities are initialized everywhere
vvz=0.

nrpoints=0

if (.not.associated(current%next)) then

   call rdpoints((max(level-1,0))*100+isnap)

   if (.not.associated(current%next)) then
      call driveinit
      call wrpoints(isnap+100*max(level-1,0))
   endif
else
  call resetarr
  call updatepoints

  call drawupdate

  do 
     if (associated(current%next)) then
        call gtnextpoint
        call drawupdate
        nrpoints=nrpoints+1
     else
        exit
     endif
  end do
  nnrpoints=0
  do 
     if (minval(w(:,:)/(1-avoidarr(:,:)+1e-20)).ge.ampl/(granr*(1+ig)+sqrt(dzdx2))) then
        exit
     endif
     call addpoint
     call make_newpoint
     call drawupdate
     nrpoints=nrpoints+1
     nnrpoints=nnrpoints+1
  end do
!  write (*,*) 'placed ',nnrpoints,' on level',level
  call reset

endif

!Using lower boundary Uz,Ux as temp memory

!off$omp parallel do private(iz,ix)
do iz=1,mz
do ix=1,mx
  Uz(ix,lb,iz)= Uz(ix,lb,iz)+vvz(iz,ix)
  Ux(ix,lb,iz)= Ux(ix,lb,iz)+vvx(iz,ix)
end do
end do

! w(:,:) should now be free!!

if (level .eq. 3 .or. level .eq. 0) then

!Putting sum of velocities back into vz,vx

!off$omp parallel do private(iz,ix)
do iz=1,mz
do ix=1,mx 
   vvz(iz,ix)=Uz(ix,lb,iz)
   vvx(iz,ix)=Ux(ix,lb,iz)
end do
end do

!Calculating and enhancing rotational part

   call helmholtz(vvz,vvx,wscr,wscr2)
   nvor=5.0
   vvz=(vvz+nvor*wscr )
   vvx=(vvx+nvor*wscr2)

!calculating beta quenching

!   call xup_set(bx,scr2)
!   call yup_set(by,scr3)
!   call zup_set(bz,scr4)
!
!   scr1=scr2**2+scr3**2+scr4**2
!   wscr(:,:)=scr1(lb,:,:)
!   call aver2d3(wscr,w)
!
!   w=w/(2.*(gamma-1)*e(lb,:,:))  ! inverse beta = Pmag/Pgas
!   w=(1.+w**2)/(1.+3.*w**2)      ! velocity does NOT vanish in sunspots

    w=1.

!Reinserting rotationally enhanced and beta quenched velocity field
 
!off$omp parallel do private(iz,ix)
   do iz=1,mz
   do iy=1,lb
     do ix=1,mx
       Uz(ix,iy,iz)=vvz(iz,ix)*w(iz,ix)
       Ux(ix,iy,iz)=vvx(iz,ix)*w(iz,ix)
     enddo
   end do
   end do
endif


if (modulo(it,nsnap).eq.0) then
  writing=writing+1
  if ( (level.eq.0) .and. (writing.eq.3) ) then
    call wrpoints(isnap+1)
  else
    if (writing.ge.7) then
      call wrpoints(isnap+1+100*(level-1))
      if (writing.ge.9) writing=0
    endif
  endif
else
endif

!$omp end master
!$omp barrier

end subroutine drive3

!-----------------------------------------------------------

subroutine drawupdate

USE params
USE points

real :: zdist,xdist,dist2,dist,dzdx,vtmp
integer :: i,ii,j,jj

!$omp master
  dzdx=sqrt(dzdx2)

  ! Update weight and velocity for new granule

!off$omp parallel do private(i,ii,j,jj,zdist,xdist,dist2,dist,vtmp,vtmp2)
  do jj=current%pos(2)-xrange,current%pos(2)+xrange
     j = 1+mod(jj-1+mx,mx)
     do ii=current%pos(1)-zrange,current%pos(1)+zrange
        i = 1+mod(ii-1+mz,mz)
        zdist=dzm(i)*(ii-(current%pos(1)))
        xdist=dxm(j)*(jj-(current%pos(2)))
        dist2=max(zdist**2+xdist**2,dzdx2)
        dist=sqrt(dist2)
        if (dist.lt.avoid*granr.and.t.lt.current%data(3)) avoidarr(i,j)=1
        vtmp=current%data(1)/dist !*(dzdx/ampl)
        vtmp2=(1.6*2.*exp(1.0)/(0.53*granr)**2)*current%data(1)*dist**2*exp(-(dist/(0.53*granr))**2)
        if (vtmp.gt.w(i,j)*(1-ig)) then
           if (vtmp.gt.w(i,j)*(1+ig)) then
              vvz(i,j)=vtmp2*zdist/dist
              vvx(i,j)=vtmp2*xdist/dist
              w(i,j)=vtmp
              granlane(i,j)=0
           else
              vvz(i,j)=vvz(i,j)+vtmp2*zdist/dist
              vvx(i,j)=vvx(i,j)+vtmp2*xdist/dist
              w(i,j)=max(w(i,j),vtmp)
              granlane(i,j)=1
           end if
        endif
     enddo
  enddo
!$omp end master
!$omp barrier

end subroutine drawupdate

!-------------------------------------------------------

subroutine make_newpoint

  USE params
  USE points

  integer :: kfind,count,ipos,jpos
  integer,dimension(mz,mx) :: k
  real :: rand

!$omp master
  k(:,:)=0
  where (w/(1-avoidarr+1e-20).lt.ampl/(granr*(1+ig))) k=1

  ! Choose and find location of one of them
  rand=ran1(seed)
  kfind=int(rand*sum(k))+1
  count=0
  do i=1,mz
     do j=1,mx
        if (k(i,j).eq.1) then
           count=count+1
           if (count.eq.kfind) then 
              ipos=i
              jpos=j
           endif
        endif
     enddo
  enddo

! Create new data for new point

  current%pos(1)=ipos
  current%pos(2)=jpos
  current%data(2)=ampl*(1+(2*ran1(seed)-1)*pd)
  current%data(4)=life_t*(1+(2*ran1(seed)-1)/10.)
  current%data(3)=t+0.99*current%data(4)*(-alog(ampl*sqrt(dzdx2)/  &
                  (current%data(2)*granr*(1-ig))))**(1./p)
  current%data(1)=current%data(2)*exp(-((t-current%data(3))/current%data(4))**p)

!$omp end master
!$omp barrier

end subroutine make_newpoint

!----------------------------------------------------------------
subroutine updatepoints

USE params
USE points

real :: dzdx

dzdx=sqrt(dzdx2)

! MUST take care of case when first granule dissapears

!$omp master
current%data(1)=current%data(2)*exp(-((t-current%data(3))/current%data(4))**p)

do 
   if (current%data(1)/dzdx.ge.ampl/(granr*(1-ig))) exit
   first => current%next
   previous => first
   if (associated(firstlev,current).or. &
    &  associated(secondlev,current).or. & 
    &  associated(thirdlev,current)) then
     nullify(current)
   else
     if (associated(current,first).or.associated(current,firstlev).or. &
  &      associated(current,secondlev).or.associated(current,thirdlev)) then
       nullify(current)
     else
       deallocate(current)
     endif
   endif
   current => first
   if (.not.associated(first)) then
      allocate(first)
!      if (associated(first%next)) then
!        deallocate(first%next)
!      endif 
      current => first
      previous => first
      call make_newpoint
      exit
   endif
enddo

nrmpoints=0
do 
   if (associated(current%next)) then
      call gtnextpoint
      current%data(1)=current%data(2)*exp(-((t-current%data(3))/current%data(4))**p)
      if (current%data(1)/dzdx.lt.ampl/(granr*(1-ig))) then
         call rmpoint
         nrmpoints=nrmpoints+1
      end if
   else
      exit
   endif
end do
!write (*,*) 'removed',nrmpoints,' on level',level
call reset

!$omp end master
!$omp barrier

end subroutine updatepoints

!********************************************************

subroutine aver2d3(in,out)

USE params

real,dimension(mz,mx) :: in,out

!$omp barrier
do i=izs,ize
  ip1=mod(i,mz)+1
  im1=mod(i-2+mz,mz)+1
  do j=1,mx
    jp1=mod(j,mx)+1
    jm1=mod(j-2+mx,mx)+1
     out(i,j)=(in(im1,jm1)+in(im1,j)+in(im1,jp1)+ &
               in(i  ,jm1)+in(i,  j)+in(i  ,jp1)+ &
               in(ip1,jm1)+in(ip1,j)+in(ip1,jp1))/9.
  enddo
enddo

end subroutine aver2d3

!*******************************************************

subroutine helmholtz(vz,vx,rotz,rotx)

! extracts the rotational part of a 2d vector field 
! to increase vorticity for drive3.
! Uses cfft operators

USE params
implicit none
real,dimension(mz,mx) :: rotz,rotx,vz,vx
complex,dimension(mz,mx) :: fftrz,fftrx,fftvz,fftvx
complex corr,kz,kx,ci
integer j,k
real k2,k20,filter

!$omp master

fftvz=vz
fftvx=vx
call cfft2df(fftvz,fftvz,mz,mx)
call cfft2df(fftvx,fftvx,mz,mx)

ci=cmplx(0.,1.)
k20=(mz/4.)**2
do j=1,mz
   kz=ci*(mod(j-2+mz/2,mz)-mz/2+1)      ! =0,1,2,...,mz/2-1,mz/2,-(mz/2-1),...,-1
   if (j.eq.mz/2+1) kz=0.               ! =0,1,2,...,mz/2-1,   0,-(mz/2-1),...,-1
   do k=1,mx
      kx=ci*(mod(k-2+mx/2,mx)-mx/2+1)
      if (k.eq.mx/2+1) kx=0.
      k2=kz**2+kx**2+1e-30
      corr=(fftvz(j,k)*kz+fftvx(j,k)*kx)/k2
      fftrz(j,k)=fftvz(j,k)-corr*kz
      fftrx(j,k)=fftvx(j,k)-corr*kx
      filter=exp(-(k2/k20)**2)
      fftvz(j,k)=fftvz(j,k)*filter
      fftvx(j,k)=fftvx(j,k)*filter
      fftrz(j,k)=fftrz(j,k)*filter
      fftrx(j,k)=fftrx(j,k)*filter
   enddo
enddo

call cfft2db(fftvz,fftvz,mz,mx)
call cfft2db(fftvx,fftvx,mz,mx)
call cfft2db(fftrz,fftrz,mz,mx)
call cfft2db(fftrx,fftrx,mz,mx)
vz=real(fftvz)
vx=real(fftvx)
rotz=real(fftrz)
rotx=real(fftrx)

!$omp end master
!$omp barrier

end subroutine helmholtz

