! $Id: closed_y.f90,v 1.4 2004/07/22 13:56:43 aake Exp $

!-----------------------------------------------------------------------
SUBROUTINE density_boundary (r)
  USE params
  implicit none
  real, dimension(mx,my,mz):: r
!
  lb = 6
  ub = my-5
  call extrapolate_center(r)
END

!-----------------------------------------------------------------------
SUBROUTINE density_boundary_log (r,lnr)
  USE params
  implicit none
  real, dimension(mx,my,mz):: r,lnr
  integer iy,iz
!
  lb = 6
  ub = my-5
  call extrapolate_center(lnr)
!$omp parallel do private(iz,iy)
  do iz=1,mz
    do iy=1,lb-1
      r(:,iy,iz)=exp(lnr(:,iy,iz))
    end do
    do iy=ub+1,my
      r(:,iy,iz)=exp(lnr(:,iy,iz))
    end do
  end do
END

!-----------------------------------------------------------------------
SUBROUTINE energy_boundary (r,Ux,Uy,Uz,e,ee,p,Bx,By,Bz)
  USE params
  implicit none
  real, dimension(mx,my,mz):: r,Ux,Uy,Uz,e,ee,p,Bx,By,Bz
  integer iy,iz

  call extrapolate_center(ee)
!$omp parallel do private(iz,iy)
  do iz=1,mz
    do iy=1,lb-1
      e(:,iy,iz)=r(:,iy,iz)*ee(:,iy,iz)
    end do
    do iy=ub+1,my
      e(:,iy,iz)=r(:,iy,iz)*ee(:,iy,iz)
    end do
  end do
END

!-----------------------------------------------------------------------
SUBROUTINE passive_boundary (r,Ux,Uy,Uz,d,dd)
  USE params
  implicit none
  real, dimension(mx,my,mz):: r,Ux,Uy,Uz,d,dd

  call symmetric_center(dd)
  d = r*dd
END

!-----------------------------------------------------------------------
SUBROUTINE velocity_boundary (r,xdnr,ydnr,zdnr,Ux,Uy,Uz,px,py,pz,e,Bx,By,Bz)
  USE params
  implicit none
  real, dimension(mx,my,mz):: r,xdnr,ydnr,zdnr,Ux,Uy,Uz,px,py,pz,e,ee,Bx,By,Bz
  integer iy,iz
!
  call symmetric_center(Ux)
  call antisymmetric_face(Uy)
  call symmetric_center(Uz)
!$omp parallel do private(iz,iy)
  do iz=1,mz
    do iy=1,lb
      px(:,iy,iz) = xdnr(:,iy,iz)*Ux(:,iy,iz)
      py(:,iy,iz) = ydnr(:,iy,iz)*Uy(:,iy,iz)
      pz(:,iy,iz) = zdnr(:,iy,iz)*Uz(:,iy,iz)
    end do
    do iy=ub,my
      px(:,iy,iz) = xdnr(:,iy,iz)*Ux(:,iy,iz)
      py(:,iy,iz) = ydnr(:,iy,iz)*Uy(:,iy,iz)
      pz(:,iy,iz) = zdnr(:,iy,iz)*Uz(:,iy,iz)
    end do
  end do
END

!-----------------------------------------------------------------------
SUBROUTINE mfield_boundary (Ux,Uy,Uz,Bx,By,Bz)
  USE params
  implicit none
  real, dimension(mx,my,mz):: Ux,Uy,Uz,Bx,By,Bz
!
  call antisymmetric_center(Bx)
  call antisymmetric_center(Bz)
END

!-----------------------------------------------------------------------
SUBROUTINE regularize (f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
!
  call symmetric_center(f)
END

!-----------------------------------------------------------------------
SUBROUTINE ddt_boundary (r,px,py,pz,e,Bx,By,Bz, &
                         drdt,dpxdt,dpydt,dpzdt, &
                         dedt,dBxdt,dBydt,dBzdt)
  USE params
  implicit none
  real, dimension(mx,my,mz):: r,px,py,pz,e,Bx,By,Bz, &
                              drdt,dpxdt,dpydt,dpzdt, &
                              dedt,dBxdt,dBydt,dBzdt

END

