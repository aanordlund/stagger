! $Id: pde_cm.f90,v 1.2 2005/11/07 03:50:47 aake Exp $
!***********************************************************************
MODULE pde_cm
  USE params
  USE stagger
  USE df
  implicit none

  real ambi,tcool,eemean,chi,gamma1

  real, allocatable, dimension(:,:,:):: &
    r, px, py, pz, e, &
    xdnr, ydnr, zdnr, r1, d2, &
    lnP, cs, vx, vy, vz, &
    vx1, vy1, vz1, &
    pp, t1, bvx, bvy, bvz, &
    Ix, Iy, Iz, Ex, Ey, Ez, &
    scr1, scr2, scr3, scr4, scr5, scr6

CONTAINS

!***********************************************************************
SUBROUTINE alloc
  allocate( &
    r(mx,my,mz), px(mx,my,mz), py(mx,my,mz), pz(mx,my,mz), e(mx,my,mz), &
    xdnr(mx,my,mz), ydnr(mx,my,mz), zdnr(mx,my,mz), r1(mx,my,mz), d2(mx,my,mz), &
    lnP(mx,my,mz), cs(mx,my,mz), vx(mx,my,mz), vy(mx,my,mz), vz(mx,my,mz), &
    vx1(mx,my,mz), vy1(mx,my,mz), vz1(mx,my,mz), &
    pp(mx,my,mz), t1(mx,my,mz), bvx(mx,my,mz), bvy(mx,my,mz), bvz(mx,my,mz), &
    ix(mx,my,mz), iy(mx,my,mz), iz(mx,my,mz), ex(mx,my,mz), ey(mx,my,mz), ez(mx,my,mz), &
    scr1(mx,my,mz), scr2(mx,my,mz), scr3(mx,my,mz), scr4(mx,my,mz), scr5(mx,my,mz), scr6(mx,my,mz) &
  )
END SUBROUTINE

!***********************************************************************
SUBROUTINE dealloc
  deallocate( &
    r, px, py, pz, e, &
    xdnr, ydnr, zdnr, r1, d2, &
    lnP, cs, vx, vy, vz, &
    vx1, vy1, vz1, &
    pp, t1, bvx, bvy, bvz, &
    ix, iy, iz, ex, ey, ez, &
    scr1, scr2, scr3, scr4, scr5, scr6 &
  )
END SUBROUTINE

!***********************************************************************
SUBROUTINE stats (label, a)
      implicit none
      character*(*) label
      real, dimension (mx,my,mz):: a
!
      if ((.not.master) .or. (idbg.eq.0)) return
      write (*,'(1x,a,5(1pe11.3))') label,minval(a),sum(a)/mw,maxval(a)
END SUBROUTINE

!***********************************************************************
SUBROUTINE diffus (lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt)
  implicit none
  real, dimension(mx,my,mz):: lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt
!
!  Face centered velocities
!
      r = exp(lnr)
      xdnr = exp(xdn(lnr))
      ydnr = exp(ydn(lnr))
      zdnr = exp(zdn(lnr))
!
!  Mass fluxes consistent with velocities
!
      px = Ux*xdnr
      py = Uy*ydnr
      pz = Uz*zdnr
!
!  Temperature.  For Mach 10 or higher, even a single deedt*dt
!  can make the temperature negative!  Hence reset to constant.
!
      if (tcool .lt. 0.0) then
        if (gamma.eq.1.) then
          gamma1=1.
        else
          gamma1=(gamma-1.)
        end if
        eemean = csound**2/(gamma*gamma1)
        if (chi .eq. 1.) then
          ee = eemean
        else
          ee = eemean*r*exp(chi-1.)
        endif
      endif
!
!  Pressure.  For perfect gas, P = e_th * (gamma-1)
!
      r1 = ee*gamma1
      pp = r1*r
      lnP = alog(pp)
      call stats ('r', r)
      call stats ('ee', ee)
!
!  Sound speed and convergence
!
      cs = (gamma*gamma1)*ee
      cs = nu1*sqrt(cs)
      d2 = difx1(Ux) + dify1(Uy) + difz1(Uz)
      d2 = nu3*amax1(d2,0.0)
!
!  Smooth over 3x3x3 point, to broaden influence.  Use a low order shift
!  after smoothing, to ensure positive definite values.
!
      call max3_set (d2,t1)
!c====>      call smooth3 (d2)
      call smooth3t_set (t1,d2)
!
END SUBROUTINE
!************************************************************************
SUBROUTINE diffusivities (lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt)
  USE params
  implicit none
  real, dimension(mx,my,mz):: lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt
!
!  Diffusivities
!
      vx1 = dx*(xdn1(cs) + nu2*abs(Ux))
      vy1 = dy*(ydn1(cs) + nu2*abs(Uy))
      vz1 = dz*(zdn1(cs) + nu2*abs(Uz))
!c      vx = vx1 + dx*xdn1(d2)
!c      vy = vy1 + dy*ydn1(d2)
!c      vz = vz1 + dz*zdn1(d2)
      vx = dx*xdn1(d2)
      vy = dy*ydn1(d2)
      vz = dz*zdn1(d2)
      call stats ('vx', vx)
      call stats ('vy', vy)
      call stats ('vz', vz)
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE mass (lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt)
  USE params
  implicit none
  real, dimension(mx,my,mz):: lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt
!
!-----------------------------------------------------------------------
!
!  Mass conservation, dlnM/dt = - div (U)
!  ====================================
!
!  High order diffusive term necessary for low-beta plasmas, where a
!  saw-tooth density cannot drive velocity via gas pressure.  Does no
!  harm for high-beta plasmas.
!
      dlnrdt = - ddxup(Ux) - ddyup(Uy) - ddzup(Uz)
      deedt  = r1*dlnrdt
END SUBROUTINE

!***********************************************************************
SUBROUTINE mass1 (lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt)
  USE params
  implicit none
  real, dimension(mx,my,mz):: lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt
!
      scr1 = ddxdn(lnr)
      scr2 = ddydn(lnr)
      scr3 = ddzdn(lnr)
      dlnrdt = dlnrdt &
               - xup(Ux*scr1) &
               - yup(Uy*scr2) &
               - zup(Uz*scr3)
END SUBROUTINE

!***********************************************************************
SUBROUTINE mass2 (lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt)
  USE params
  implicit none
  real, dimension(mx,my,mz):: lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt
!
      dlnrdt = dlnrdt &
               +     ddxup(vx*scr1 + vx1*dfxdn(lnr)) &
               +     ddyup(vy*scr2 + vy1*dfydn(lnr)) &
               +     ddzup(vz*scr3 + vz1*dfzdn(lnr))
END SUBROUTINE
!***********************************************************************
SUBROUTINE mass3 (lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt)
  USE params
  implicit none
  real, dimension(mx,my,mz):: lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt
!
      dlnrdt = dlnrdt &
           + xup(scr1)*xup(vx*scr1 + vx1*dfxdn(lnr)) &
           + yup(scr2)*yup(vy*scr2 + vy1*dfydn(lnr)) &
           + zup(scr3)*zup(vz*scr3 + vz1*dfzdn(lnr))
END SUBROUTINE
!*********************************************************************
SUBROUTINE massexact (lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt)
  USE params
  implicit none
  real, dimension(mx,my,mz):: lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt
  real, save:: dlnrm=0.
  real rm, drdtm
  integer, save:: i_step=0
  integer n_step
!
!--------------------------------------------------------------------
!
!  Make mass conservation exact:  Add the smoothed difference btw
!  the per volume time derivative and the time derivative from
!  logarithmic vars.  Important to include diffusion also in
!  the formula below, to minimize the difference (should only
!  be truncation errors).

       n_step=10
       if (mod(i_step,n_step) .eq. 0) then
         rm = sum(r)/mw
         scr1 = r*dlnrdt
         drdtm = sum(scr1)/mw
         dlnrm = drdtm/rm
         print *,'rm,dlnrm=',rm,dlnrm
       endif
       i_step = i_step+1
       dlnrdt = dlnrdt-dlnrm

!c      scr1 =
!c     &      - ddxup(px - xdn(r)*vx*ddxdn(lnr))
!c     &      - ddyup(py - ydn(r)*vy*ddydn(lnr))
!c     &      - ddzup(pz - zdn(r)*vz*ddzdn(lnr))
!c      scr1 =
!c     &      - ddxup1(px - vx*ddxdn1(r))
!c     &      - ddyup1(py - vy*ddydn1(r))
!c     &      - ddzup1(pz - vz*ddzdn1(r))
!c      scr1 = scr1 - r*dlnrdt
!c      call smooth5 (scr1)
!c      dlnrdt = dlnrdt + scr1/r

END SUBROUTINE
!***********************************************************************
SUBROUTINE momx (lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt)
  USE params
  implicit none
  real, dimension(mx,my,mz):: lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt
!
      dUxdt = - xdn(r1)*ddxdn(lnP) &
            - Ux*xdn(ddxup(Ux)) &
            - yup(Uy*ddydn(Ux)) &
            - zup(Uz*ddzdn(Ux))
!
      scr1 = xup1(vx)*ddxup(Ux) + xup1(vx1)*dfxup(Ux)
END SUBROUTINE
!**********************************************************************
SUBROUTINE momxpartlysymm (lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt)
  USE params
  implicit none
  real, dimension(mx,my,mz):: lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt
!
!  Partly non-symmetrized
!c      scr2 = 0.5*(xdn1(vy)*ddydn(Ux)  +
!c     &            ydn1(vx)*ddxdn(Uy)) + xdn1(vy1)*dfydn(Ux)
!c      scr3 = 0.5*(xdn1(vz)*ddzdn(Ux)  +
!c     &            zdn1(vx)*ddxdn(Uz)) + xdn1(vz1)*dfzdn(Ux)
!
 END SUBROUTINE
!***********************************************************************
SUBROUTINE momxsymmetrized (lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt)
  USE params
  implicit none
  real, dimension(mx,my,mz):: lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt
!
!  Symmetrized
      scr2 = 0.5*(xdn1(vy)*ddydn(Ux) + xdn1(vy1)*dfydn(Ux) + &
                  ydn1(vx)*ddxdn(Uy) + ydn1(vx1)*dfxdn(Uy))
      scr3 = 0.5*(xdn1(vz)*ddzdn(Ux) + xdn1(vz1)*dfzdn(Ux) + &
                  zdn1(vx)*ddxdn(Uz) + zdn1(vx1)*dfxdn(Uz))
END SUBROUTINE
!***********************************************************************
SUBROUTINE momxduxdt (lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt)
  USE params
  implicit none
  real, dimension(mx,my,mz):: lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt
!
      dUxdt = dUxdt + ddxdn(scr1) + ddxdn(lnr)*xdn(scr1) &
                    + ddyup(scr2) + yup(ddydn(xdn(lnr))*scr2) &
                    + ddzup(scr3) + zup(ddzdn(xdn(lnr))*scr3)
END SUBROUTINE
!***********************************************************************
SUBROUTINE momy (lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt)
  USE params
  implicit none
  real, dimension(mx,my,mz):: lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt
!
      dUydt = - ydn(r1)*ddydn(lnP) &
            - Uy*ydn(ddyup(Uy))
      dUydt = dUydt &
            - zup(Uz*ddzdn(Uy))
      dUydt = dUydt &
            - xup(Ux*ddxdn(Uy))

      scr1 = yup1(vy)*ddyup(Uy) + yup1(vy1)*dfyup(Uy)
END SUBROUTINE
!**********************************************************************
SUBROUTINE momypartlysymm (lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt)
  USE params
  implicit none
  real, dimension(mx,my,mz):: lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt
!
!  Partly non-symmetrized
!c      scr2 = 0.5*(ydn1(vz)*ddzdn(Uy)  +
!c     &            zdn1(vy)*ddydn(Uz)) + ydn1(vz1)*dfzdn(Uy)
!c      scr3 = 0.5*(ydn1(vx)*ddxdn(Uy)  +
!c     &            xdn1(vy)*ddydn(Ux)) + ydn1(vx1)*dfxdn(Uy)
!
 END SUBROUTINE
!**********************************************************************
SUBROUTINE momysymmetrized (lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt)
  USE params
  implicit none
  real, dimension(mx,my,mz):: lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt
!
!  Symmetrized
      scr2 = 0.5*(ydn1(vz)*ddzdn(Uy) + ydn1(vz1)*dfzdn(Uy) + &
                  zdn1(vy)*ddydn(Uz) + zdn1(vy1)*dfydn(Uz))
      scr3 = 0.5*(ydn1(vx)*ddxdn(Uy) + ydn1(vx1)*dfxdn(Uy) + &
                  xdn1(vy)*ddydn(Ux) + xdn1(vy1)*dfydn(Ux))
END SUBROUTINE
!***********************************************************************
SUBROUTINE momyduydt (lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt)
  USE params
  implicit none
  real, dimension(mx,my,mz):: lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt
!
      dUydt = dUydt + ddydn(scr1) + ddydn(lnr)*ydn(scr1) &
                    + ddzup(scr2) + zup(ddzdn(ydn(lnr))*scr2) &
                    + ddxup(scr3) + xup(ddxdn(ydn(lnr))*scr3)
END SUBROUTINE
!***********************************************************************
SUBROUTINE momz (lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt)
  USE params
  implicit none
  real, dimension(mx,my,mz):: lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt
!
      dUzdt = - zdn(r1)*ddzdn(lnP) &
            - Uz*zdn(ddzup(Uz)) &
            - xup(Ux*ddxdn(Uz)) &
            - yup(Uy*ddydn(Uz))

      scr1 = zup1(vz)*ddzup(Uz) + zup1(vz1)*dfzup(Uz)
END SUBROUTINE
!***********************************************************************
SUBROUTINE momzpartlysymm (lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt)
  USE params
  implicit none
  real, dimension(mx,my,mz):: lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt
!
!  Partly non-symmetrized
!c      scr2 = 0.5*(zdn1(vx)*ddxdn(Uz)  +
!c     &            xdn1(vz)*ddzdn(Ux)) + zdn1(vx1)*dfxdn(Uz)
!c      scr3 = 0.5*(zdn1(vy)*ddydn(Uz)  +
!c     &            ydn1(vz)*ddzdn(Uy)) + zdn1(vy1)*dfydn(Uz)
!
 END SUBROUTINE
!***********************************************************************
SUBROUTINE momzsymmetrized (lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt)
  USE params
  implicit none
  real, dimension(mx,my,mz):: lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt
!
!  Symmetrized
      scr2 = 0.5*(zdn1(vx)*ddxdn(Uz) + zdn1(vx1)*dfxdn(Uz) + &
                  xdn1(vz)*ddzdn(Ux) + xdn1(vz1)*dfzdn(Ux))
      scr3 = 0.5*(zdn1(vy)*ddydn(Uz) + zdn1(vy1)*dfydn(Uz) + &
                  ydn1(vz)*ddzdn(Uy) + ydn1(vz1)*dfzdn(Uy))
END SUBROUTINE
!***********************************************************************
SUBROUTINE momzduzdt (lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt)
  USE params
  implicit none
  real, dimension(mx,my,mz):: lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt
!
      dUzdt = dUzdt + ddzdn(scr1) + ddzdn(lnr)*zdn(scr1) &
                    + ddxup(scr2) + xup(ddxdn(zdn(lnr))*scr2) &
                    + ddyup(scr3) + yup(ddydn(zdn(lnr))*scr3)
END SUBROUTINE
!***********************************************************************
SUBROUTINE energy (lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt)
  USE params
  implicit none
  real, dimension(mx,my,mz):: lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt
!
!  Cooling
!
      if (tcool .gt. 0.) then
        eemean = csound**2/(gamma*gamma1)
        deedt = deedt - (ee - eemean)/(tcool*(ee**0.5)*r**(-1))
      endif
!
!  Note: the P/r*div(U) in done in mass
!
      deedt = deedt &
           - xup(Ux*ddxdn(ee)) &
           - yup(Uy*ddydn(ee)) &
           - zup(Uz*ddzdn(ee)) &
           +(ddxup((vx1*dfxdn(ee) + (vx)*ddxdn(ee))) &
           + ddyup((vy1*dfydn(ee) + (vy)*ddydn(ee))) &
           + ddzup((vz1*dfzdn(ee) + (vz)*ddzdn(ee))) &
            )
!     &     +(ddxup( xdnr*(vx1*dfxdn(ee) + (vx+xdn(KK))*ddxdn(ee)))
!     &     + ddyup( ydnr*(vy1*dfydn(ee) + (vy+ydn(KK))*ddydn(ee)))
!     &     + ddzup( zdnr*(vz1*dfzdn(ee) + (vz+zdn(KK))*ddzdn(ee)))
!     &      )/r
!
END SUBROUTINE
!***********************************************************************
SUBROUTINE Lorentz (lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt)
  USE params
  implicit none
  real, dimension(mx,my,mz):: lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt
  real Umax
!
!  Current = curl(B)
!
      Ix = ddydn(Bz) - ddzdn(By)
      Iy = ddxdn(Bz)
      Iz = ddxdn(By)
      Iy = ddzdn(Bx) - Iy
      Iz = Iz - ddydn(Bx)
!
!  Lorentz force IxB.  Add ambipolar diffusion speed directly to
!  Ui, since Ui is only used in the induction equation after this.
!  Scale the coefficient with dx, to make the effect scale-indep.
!
      scr1 = zup(Iy*xdn(Bz)) - yup(Iz*xdn(By))
      scr2 = xup(Iz*ydn(Bx)) - zup(Ix*ydn(Bz))
      scr3 = yup(Ix*zdn(By)) - xup(Iy*zdn(Bx))
      dUxdt = dUxdt + scr1/xdnr
      dUydt = dUydt + scr2/ydnr
      dUzdt = dUzdt + scr3/zdnr
      if (ambi .ne. 0.0) then
        scr4=amax1(Ux,Uy,Uz)
        Umax=abs(maxval(scr4))
        t1 = ambi*dx*r**(-1.5)
        scr1=scr1*t1
        scr1=scr1*Umax/(abs(scr1)+Umax)
!!        where (abs(scr1) > Umax) scr1=Umax*scr1/abs(scr1)
        scr2=scr2*t1
        scr2=scr2*Umax/(abs(scr2)+Umax)
!!        where (abs(scr2) > Umax) scr2=Umax*scr2/abs(scr2)
        scr3=scr3*t1
        scr3=scr3*Umax/(abs(scr3)+Umax)
!!        where (abs(scr3) > Umax) scr3=Umax*scr3/abs(scr3)

!!        call smooth5p(scr1)   ! smooth5p does not work better than smooth3t
!!        call smooth5p(scr2)
!!        call smooth5p(scr3)

        call smooth3t_set(scr1,scr4)
        px = Ux + scr1
        call smooth3t_set(scr2,scr4)
        py = Uy + scr2
        call smooth3t_set(scr3,scr4)
        pz = Uz + scr3
      else
        px = Ux
        py = Uy
        pz = Uz
      endif
!
END SUBROUTINE
!***********************************************************************
SUBROUTINE induction (lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt)
  USE params
  implicit none
  real, dimension(mx,my,mz):: lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt
!
!  Magnetic convergence, in the plane perpendicular to the magnetic field.
!  r1 is used first for the normalization factor, then for the scalar
!  product U*Bhat.
!
      bvx = xup(Bx)
      bvy = yup(By)
      bvz = zup(Bz)
      r1 = amax1(bvx**2 + bvy**2 + bvz**2, 1.e-20)
      cs = (nu1*nuB)*sqrt((gamma*pp+r1)/r)
      r1 = 1./sqrt(r1)
      bvx = bvx*r1      ! bvi = unit B vector
      bvy = bvy*r1
      bvz = bvz*r1
      scr1 = xup(px)        ! scri = centered velocity
      scr2 = yup(py)
      scr3 = zup(pz)
      r1 = scr1*bvx + scr2*bvy + scr3*bvz
!c      d2 = (3./2.*nu3)*amax1(0.0,
      d2 = (0.5*nu3*nuB)*amax1(0.0, &
                   difx2(scr1-bvx*r1) &
                 + dify2(scr2-bvy*r1) &
                 + difz2(scr3-bvz*r1))
!
!  Smooth over 3x3x3 point, to broaden influence.  Use a low order shift
!  after smoothing, to ensure positive definite values.
!
!c      call smooth5 (d2)
      call max3_set (d2,t1)
!c====>      call smooth3 (d2)
      call smooth3t_set (t1,d2)
!
      scr1 = dx*((nu2*nuB)*abs(px) + xdn1(cs))
      scr2 = dy*((nu2*nuB)*abs(py) + ydn1(cs))
      scr3 = dz*((nu2*nuB)*abs(pz) + zdn1(cs))
      bvx = dx*xdn1(d2)
      bvy = dy*ydn1(d2)
      bvz = dz*zdn1(d2)
!
      call stats ('bvx', bvx)
      call stats ('bvy', bvy)
      call stats ('bvz', bvz)
!
END SUBROUTINE
!***********************************************************************
SUBROUTINE induction2 (lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt)
  USE params
  implicit none
  real, dimension(mx,my,mz):: lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt
!
!  Induction equation, d(B)/dt = - curl(E) = curl (u x B - eta I)
!  ==============================================================
!
!  The magnetic field is face centered.  The electric field and current
!  are centered on the cube edges.
!
!  eta I contribution to the Electric field
!
      Ex = (0.5)*((ydn1(scr3)+zdn1(scr2))*(diy(Ix)+diz(Ix)) &
                    + (ydn1(bvz )+zdn1(bvy ))*Ix)
      Ey = (0.5)*((zdn1(scr1)+xdn1(scr3))*(diz(Iy)+dix(Iy)) &
                    + (zdn1(bvx )+xdn1(bvz ))*Iy)
      Ez = (0.5)*((xdn1(scr2)+ydn1(scr1))*(dix(Iz)+diy(Iz)) &
                    + (xdn1(bvy )+ydn1(bvx ))*Iz)
!
END SUBROUTINE
!**********************************************************************
SUBROUTINE joulheating (lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt)
  USE params
  implicit none
  real, dimension(mx,my,mz):: lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt
  integer, save:: i_step=0
  real Emagn,Ekin,Qjoule
!
!  Joule heating added to the energy equation, eta I^2
!
      scr1 = zup(yup(Ex*Ix)) + xup(zup(Ey*Iy)+yup(Ez*Iz))
      if (mod(i_step,20) .eq. 1) then
        Qjoule = sum(scr1)/mw
        scr2 = Bx**2 + By**2 + Bz**2
        Emagn = 0.5*sum(scr2)/mw
        scr2 = r*(Ux**2+Uy**2+Uz**2)
        Ekin = 0.5*sum(scr2)/mw
        write(*,'(1x,a,f10.4,3(1pe12.5))') 'joule:',t,Qjoule,Emagn,Ekin
      endif
      i_step = i_step + 1
      deedt = deedt + scr1/r
!
END SUBROUTINE
!***********************************************************************
SUBROUTINE electricfield (lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt)
  USE params
  implicit none
  real, dimension(mx,my,mz):: lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt
!
!  Electric field   E = - uxB + eta I
!
      Ex = Ex - zdn(py)*ydn(Bz) + ydn(pz)*zdn(By)
      Ey = Ey - xdn(pz)*zdn(Bx) + zdn(px)*xdn(Bz)
      Ez = Ez - ydn(px)*xdn(By) + xdn(py)*ydn(Bx)
!
END SUBROUTINE
!***********************************************************************
SUBROUTINE magneticfield (lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt)
  USE params
  implicit none
  real, dimension(mx,my,mz):: lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt
!
!  Magnetic field 's time derivative, dBdt = - curl(E)
!
      dBxdt = ddzup (Ey) - ddyup (Ez)
      dBydt = ddxup (Ez) - ddzup (Ex)
      dBzdt = ddyup (Ex) - ddxup (Ey)
!
END SUBROUTINE
END MODULE

!***********************************************************************
SUBROUTINE pde ( &
        lnr,Ux,Uy,Uz,ee,d,dlnrdt,dUxdt,dUydt,dUzdt,deedt,dddt,flag, &
        Bx,By,Bz,dBxdt,dBydt,dBzdt)
      USE params
      USE pde_cm
      implicit none
      real, dimension(mx,my,mz):: &
        lnr,Ux,Uy,Uz,ee,d,dlnrdt,dUxdt,dUydt,dUzdt,deedt,dddt,flag, &
        Bx,By,Bz,dBxdt,dBydt,dBzdt
      character(len=mid):: id='$Id: pde_cm.f90,v 1.2 2005/11/07 03:50:47 aake Exp $'
!
      call print_id(id)
      tcool=0.
      chi=1.
      call stats ('lnr',lnr)
      call stats ('Ux',Ux)
      call stats ('Uy',Uy)
      call stats ('Uz',Uz)
      call stats ('ee',ee)
      call stats ('Bx',Bx)
      call stats ('By',By)
      call stats ('Bz',Bz)
      call alloc
      call diffus          (lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt)
      call diffusivities   (lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt)
      call mass            (lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt)
      call mass1           (lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt)
      call mass2           (lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt)
      call mass3           (lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt)
      call massexact       (lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt)
      call momx            (lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt)
      call momxpartlysymm  (lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt)
      call momxsymmetrized (lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt)
      call momxduxdt       (lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt)
      call momy            (lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt)
      call momypartlysymm  (lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt)
      call momysymmetrized (lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt)
      call momyduydt       (lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt)
      call momz            (lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt)
      call momzpartlysymm  (lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt)
      call momzsymmetrized (lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt)
      call momzduzdt       (lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt)
      if (do_mhd) &
        call Lorentz       (lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt)
      call energy          (lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt)
      call stats ('dlnrdt',dlnrdt)
      call stats ('dUxdt', dUxdt)
      call stats ('dUydt', dUydt)
      call stats ('dUzdt', dUzdt)
      call stats ('deedt', deedt)
      if (do_mhd) then
        call induction     (lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt)
        call induction2    (lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt)
        call joulheating   (lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt)
        call electricfield (lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt)
        call magneticfield (lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt)
        call stats ('Ix',Ix)
        call stats ('Iy',Iy)
        call stats ('Iz',Iz)
        call stats ('Ex',Ex)
        call stats ('Ey',Ey)
        call stats ('Ez',Ez)
        call stats ('dBxdt', dBxdt)
        call stats ('dBydt', dBydt)
        call stats ('dBzdt', dBzdt)
      endif
      call forceit (r,Ux,Uy,Uz,xdnr,ydnr,zdnr,dUxdt,dUydt,dUzdt)
      call dealloc
END SUBROUTINE

