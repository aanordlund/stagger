! $Id: pde_lin.f90,v 1.49 2012/12/31 16:19:14 aake Exp $
!***********************************************************************
  SUBROUTINE pde_lin(r,px,py,pz,e,d,drdt,dpxdt,dpydt,dpzdt,dedt,dddt,flag, &
                     Bx,By,Bz,dBxdt,dBydt,dBzdt)

  USE params
  USE stagger

  implicit none
  real gam1, ds, average, fmaxval, fCv
  integer iz
  logical flag
  real, dimension(mx,my,mz) :: &
       r,px,py,pz,e,d,drdt,dpxdt,dpydt,dpzdt,dedt,dddt, &
       Bx,By,Bz,dBxdt,dBydt,dBzdt
  real, allocatable, dimension(:,:,:) :: &
       lnr,dd,lnu,lne,P,Cs,divU,nu,ee,Ux,Uy,Uz, &
       Sxx,Syy,Szz,Sxy,Szx,Syz,xdnr,ydnr,zdnr, &
       Txx,Tyy,Tzz,Txy,Tzx,Tyz,ddxd,ddyd,ddzd
  character(len=mid) id
  data id/'$Id: pde_lin.f90,v 1.49 2012/12/31 16:19:14 aake Exp $'/

  if (id .ne. '') print *,id
  id = ''

!-----------------------------------------------------------------------
!  Velocities, pressure
!-----------------------------------------------------------------------
  allocate(xdnr(mx,my,mz),ydnr(mx,my,mz),zdnr(mx,my,mz))  !  4 chunks
  call density_boundary(r)
  xdnr = xdn(r)
  ydnr = ydn(r)
  zdnr = zdn(r)
  allocate(Ux(mx,my,mz),Uy(mx,my,mz),Uz(mx,my,mz))                      !  6 chunks        
!$omp parallel do private(iz)
  do iz=1,mz
    Ux(:,:,iz) = px(:,:,iz)/(xdnr(:,:,iz))
    Uy(:,:,iz) = py(:,:,iz)/(ydnr(:,:,iz))
    Uz(:,:,iz) = pz(:,:,iz)/(zdnr(:,:,iz))
  enddo
  call velocity_boundary(r,xdnr,ydnr,zdnr,Ux,Uy,Uz,px,py,pz,e,Bx,By,Bz)
  call forceit (r,Ux,Uy,Uz,xdnr,ydnr,zdnr,dpxdt,dpydt,dpzdt)

  allocate(Sxx(mx,my,mz),Syy(mx,my,mz),Szz(mx,my,mz))                   !  9 chunks
  Sxx = ddxup(Ux)
  Syy = ddyup(Uy)
  Szz = ddzup(Uz)

!-----------------------------------------------------------------------
!  Numerical viscosity, Richtmyer & Morton type
!-----------------------------------------------------------------------
  allocate(P(mx,my,mz),Cs(mx,my,mz),nu(mx,my,mz))                       ! 12 chunks
!$omp parallel do private(iz)
  do iz=1,mz
    nu(:,:,iz) = Sxx(:,:,iz)+Syy(:,:,iz)+Szz(:,:,iz)
    nu(:,:,iz) = max(-nu(:,:,iz),0.0)
  end do
  if (gamma .eq. 1) then
    gam1 = 1.
  else
    gam1 = gamma-1.
  end if
  Cs = xup(Ux)**2 + yup(Uy)**2  + zup(Uz)**2
  Urms = sqrt(average(Cs))

!-----------------------------------------------------------------------
!  Energy per unit mass
!-----------------------------------------------------------------------
  allocate(ee(mx,my,mz))
!$omp parallel do private(iz)
  do iz=1,mz
    if (.not. do_energy) then
      e(:,:,iz) = (csound**2/(gamma*gam1))*r(:,:,iz)**gamma
    end if
    ee(:,:,iz) = e(:,:,iz)/r(:,:,iz)
  end do
!-----------------------------------------------------------------------
!  Boundary condition -- may need to calculate pressure internally
!-----------------------------------------------------------------------
  if (do_energy) call energy_boundary(r,lnr,Ux,Uy,Uz,e,ee,p,Bx,By,Bz)

!-----------------------------------------------------------------------
!  Pressure
!-----------------------------------------------------------------------
  if (do_ionization) then
    call pressure(r,ee,p)
  else
!$omp parallel do private(iz)
    do iz=1,mz
      P(:,:,iz)  = gam1*e(:,:,iz)
    end do
  end if

!-----------------------------------------------------------------------
!  Numerical viscosity, Richtmyer & Morton type
!-----------------------------------------------------------------------
  ds = max(dx,dy,dz)
  if (do_momenta) then
!$omp parallel do private(iz), firstprivate(nu1,nu2,ds)
    do iz=1,mz
      Cs(:,:,iz) = sqrt(gamma*P(:,:,iz)/r(:,:,iz)) + sqrt(Cs(:,:,iz))
      nu(:,:,iz) = nu1*(Cs(:,:,iz)) + nu2*ds*nu(:,:,iz)
    end do
  else
!$omp parallel do private(iz), firstprivate(nu1,nu2,ds)
    do iz=1,mz
      Cs(:,:,iz) = sqrt(Cs(:,:,iz)) + 1e-30
      nu(:,:,iz) = nu1*(Cs(:,:,iz)) + nu2*ds*nu(:,:,iz)
    end do
  end if
  if (flag) then
    ds = min(dx,dy,dz)
    Cs = Cs*(dt/ds)
    Cu = fmaxval('Cu',Cs)
    fCv = 2.*6.2*3.*dt/2.
    Cs = fCv/ds*nu
    Cv = fmaxval('Cv',Cs)
  end if
  deallocate(Cs)                                                        ! 12 chunks
  if (lb.lt.6) call regularize(nu)
  nu = smooth3(max3(nu))
  call regularize(nu)
!$omp parallel do private(iz)
  do iz=1,mz
    nu(:,:,iz) = nu(:,:,iz)*r(:,:,iz)
  end do

!-----------------------------------------------------------------------
!  Viscous stress, diagonal and off-diagonal elements
!-----------------------------------------------------------------------
if (do_momenta) then
  allocate(Txx(mx,my,mz),Tyy(mx,my,mz),Tzz(mx,my,mz))                   ! 15 chunks
!$omp parallel do private(iz)
  do iz=1,mz
    Txx(:,:,iz) = - (2.*dx)*nu(:,:,iz)*Sxx(:,:,iz) + P(:,:,iz)
    Tyy(:,:,iz) = - (2.*dy)*nu(:,:,iz)*Syy(:,:,iz) + P(:,:,iz)
    Tzz(:,:,iz) = - (2.*dz)*nu(:,:,iz)*Szz(:,:,iz) + P(:,:,iz)
    if (do_energy) then
      if (do_dissipation) then 
        dedt(:,:,iz) = dedt(:,:,iz) &
                     - Txx(:,:,iz)*Sxx(:,:,iz) &
                     - Tyy(:,:,iz)*Syy(:,:,iz) &
                     - Tzz(:,:,iz)*Szz(:,:,iz)
      else
        dedt(:,:,iz) = dedt(:,:,iz) - P(:,:,iz)* &
                       (Sxx(:,:,iz) + Syy(:,:,iz) + Szz(:,:,iz))
      end if
    end if
  end do
  deallocate(P,Sxx,Syy,Szz)                                             ! 10 chunks

  allocate(Sxy(mx,my,mz),Syz(mx,my,mz),Szx(mx,my,mz))                   ! 13 chunks
  Sxy = (ddxdn(Uy)+ddydn(Ux))*0.5
  Syz = (ddydn(Uz)+ddzdn(Uy))*0.5
  Szx = (ddzdn(Ux)+ddxdn(Uz))*0.5
  allocate(Txy(mx,my,mz),Tyz(mx,my,mz),Tzx(mx,my,mz))                   ! 16 chunks
  Txy = - (dx+dy)*xdn(ydn(nu))*Sxy
  Tyz = - (dz+dz)*ydn(zdn(nu))*Syz
  Tzx = - (dy+dx)*zdn(xdn(nu))*Szx
  if (do_energy .and. do_dissipation) dedt = dedt &
      + (2.*(dx+dy))*nu*xup(yup(Sxy))**2 &
      + (2.*(dy+dz))*nu*yup(zup(Syz))**2 &
      + (2.*(dz+dx))*nu*zup(xup(Szx))**2 
  deallocate(Sxy,Syz,Szx)                                               ! 13 chunks

!-----------------------------------------------------------------------
!  Add Reynolds stress
!-----------------------------------------------------------------------
!*$* assert do (concurrent)
  Txx = Txx + r*xup(Ux)**2
!*$* assert do (concurrent)
  Tyy = Tyy + r*yup(Uy)**2
!*$* assert do (concurrent)
  Tzz = Tzz + r*zup(Uz)**2
  Txy = Txy + (xdn(ydnr))*ydn(Ux)*xdn(Uy)
  Tyz = Tyz + (ydn(zdnr))*zdn(Uy)*ydn(Uz)
  Tzx = Tzx + (zdn(xdnr))*xdn(Uz)*zdn(Ux)
  deallocate(xdnr,ydnr,zdnr)                                            !  7 chunks

!-----------------------------------------------------------------------
!  Equations of motion, face centred
!-----------------------------------------------------------------------
  dpxdt = dpxdt - ddxdn (Txx) - ddyup(Txy) - ddzup(Tzx)
  dpydt = dpydt - ddydn (Tyy) - ddzup(Tyz) - ddxup(Txy)
  dpzdt = dpzdt - ddzdn (Tzz) - ddxup(Tzx) - ddyup(Tyz)
  deallocate(Txy,Tyz,Tzx,Txx,Tyy,Tzz)                                   !  1 chunks
else
  deallocate(xdnr,ydnr,zdnr)
end if

!-----------------------------------------------------------------------
!  MHD
!-----------------------------------------------------------------------
  if (do_mhd) then
    call mhd(r,ee,Ux,Uy,Uz,Bx,By,Bz,dpxdt,dpydt,dpzdt,dedt,dBxdt,dBydt,dBzdt,flag)
  else
    Cb =0.
  end if

!-----------------------------------------------------------------------
!  Abundance per unit mass
!-----------------------------------------------------------------------
  if (do_pscalar .or. do_cool) then
    allocate (dd(mx,my,mz))
    dd=d/r
  end if

!-----------------------------------------------------------------------
!  Passive scalar
!-----------------------------------------------------------------------
  call passive(nu,lnu,r,px,py,pz,Ux,Uy,Uz,dd,dddt)
  deallocate (Ux,Uy,Uz)                                                 !  7 chunks

  if (do_energy) then

!-----------------------------------------------------------------------
!  Energy advection and diffusion
!-----------------------------------------------------------------------
    dedt = dedt &
       - ddxup (xdn(ee)*px - dx*xdn(nu)*ddxdn(ee)) &
       - ddyup (ydn(ee)*py - dy*ydn(nu)*ddydn(ee)) &
       - ddzup (zdn(ee)*pz - dz*zdn(nu)*ddzdn(ee))

!-----------------------------------------------------------------------
!  Cooling and conduction
!-----------------------------------------------------------------------
    if (do_cool) then
      allocate(lne(mx,my,mz))                                             !  3 chunks
!$omp parallel do private(iz)
      do iz=1,mz
        lne(:,:,iz) = alog(ee(:,:,iz))
      end do
      call conduction (r,e,ee,Bx,By,Bz,dedt)
      call coolit (r,ee,lne,dd,dedt)
      deallocate(lne)                                                     !  3 chunks
    end if

  end if
  deallocate(ee,nu)                                                     !  1 chunks
  if (do_pscalar .or. do_cool) deallocate(dd)                           !  0 chunks

!-----------------------------------------------------------------------
!  Mass conservation, cell centered
!-----------------------------------------------------------------------
  if (do_density) drdt = drdt - ddxup(px) - ddyup(py) - ddzup(pz)

  call ddt_boundary (r,px,py,pz,e,Bx,By,Bz, &
                     drdt,dpxdt,dpydt,dpzdt, &
                     dedt,dBxdt,dBydt,dBzdt)

  END
