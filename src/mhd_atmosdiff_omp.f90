! $Id: mhd_atmosdiff_omp.f90,v 1.4 2008/03/04 18:25:55 bob Exp $
!***********************************************************************
SUBROUTINE mhd (flag, &
            r,e,Ux,Uy,Uz,Bx,By,Bz,Ex,Ey,Ez, &
	    dpxdt,dpydt,dpzdt,dedt,dBxdt,dBydt,dBzdt)
  USE params
  USE arrays, ONLY: &
            wk04,wk05,wk06,wk10, &
            wk11,wk12,wk13,wk14,wk15,wk16,wk17, &
            scr1,scr2,scr3,scr4,scr5,scr6
  implicit none
  logical flag
  real, dimension(mx,my,mz):: &
            r,e,Ux,Uy,Uz,Bx,By,Bz,Ex,Ey,Ez, &
	    dpxdt,dpydt,dpzdt,dedt,dBxdt,dBydt,dBzdt

  call mhd1(flag, &
            r,e,Ux,Uy,Uz,Bx,By,Bz,Ex,Ey,Ez, &
	    dpxdt,dpydt,dpzdt,dedt,dBxdt,dBydt,dBzdt, &
            wk04,wk05,wk06,wk10, &
            wk11,wk12,wk13,wk14,wk15,wk16,wk17, &
            scr1,scr2,scr3,scr4,scr5,scr6)
END

!ptr  ee  => wk11
!ptr  eta => wk16; fudge => wk17
!ptr  Ux  => wk18; Uy => wk19; Uz => wk20

!ptr  Jx => wk04; Jy => wk05; Jz => wk06
!ptr  Bx_y => wk10; By_z => wk11; Bz_x => wk12
!ptr  Bx_z => wk13; By_x => wk14; Bz_y => wk15
!ptr  Ux_y => scr1; Uy_z => scr2; Uz_x => scr3
!ptr  Ux_z => scr4; Uy_x => scr5; Uz_y => scr6

!***********************************************************************
SUBROUTINE mhd1(flag, &
                r,e,Ux,Uy,Uz,Bx,By,Bz,Ex,Ey,Ez, &
	        dpxdt,dpydt,dpzdt,dedt,dBxdt,dBydt,dBzdt, &
                Jx  ,Jy  ,Jz  ,wk10, &
                wk11,wk12,wk13,wk14,wk15,eta,fudge, &
                scr1,scr2,scr3,scr4,scr5,scr6)
  USE params
  implicit none
  real, dimension(mx,my,mz):: &
    r,e,Ux,Uy,Uz,Bx,By,Bz,dpxdt,dpydt,dpzdt,dedt, &
    dBxdt,dBydt,dBzdt, &
    Jx,Jy,Jz,Ex,Ey,Ez,wk10, &
    wk11,wk12,wk13,wk14,wk15,eta,fudge, &
    scr1,scr2,scr3,scr4,scr5,scr6

  logical flag
  integer ix, iy, iz, j, imhd
  real:: elapsed, tm(30)

  logical debug
  real cput(2), void, dtime

  real thickness, radius, nu_atmos_var, safety !for atmospheric diffusion

  character(len=mid) id
  data id/'$Id: mhd_atmosdiff_omp.f90,v 1.4 2008/03/04 18:25:55 bob Exp $'/

  call print_trace (id, dbg_mhd, 'BEGIN')

                       if (debug(dbg_mhd) .and. omp_master) then; void = dtime(cput); print '(1x,a8,2f7.3)','mhd0   ',cput; endif

  imhd=0

!-----------------------------------------------------------------------

  call print_trace (id, dbg_mhd, 'bef. resist')
  call resist (flag, &                                                  ! uses wk01-wk09 internally
    eta,r,e,fudge,Ux,Uy,Uz,Bx,By,Bz)
  call print_trace (id, dbg_mhd, 'aft. resist')
    call dumpl(eta,'eta','mhd.dmp',imhd); imhd=imhd+1

                       if (debug(dbg_mhd) .and. omp_master) then; void = dtime(cput); print '(1x,a8,2f7.3)','resist ',cput; endif

!-----------------------------------------------------------------------
!  Electric current I = curl(B), resistive E, and dissipation
!-----------------------------------------------------------------------
  call mfield_boundary (Ux,Uy,Uz,Bx,By,Bz)

                       if (debug(dbg_mhd) .and. omp_master) then; void = dtime(cput); print '(1x,a8,2f7.3)','Bbdry  ',cput; endif

  call barrier_omp('mhd1')                                              ! barrier 1; By may have been modified
    call dumpl(Bx,'Bx','mhd.dmp',imhd); imhd=imhd+1
    call dumpl(By,'By','mhd.dmp',imhd); imhd=imhd+1
    call dumpl(Bz,'Bz','mhd.dmp',imhd); imhd=imhd+1

  call ddydn_set (Bz,Jx) ; call ddzdn_sub (By,Jx)
  call ddzdn_set (Bx,Jy) ; call ddxdn_sub (Bz,Jy)
  call ddxdn_set (By,Jz) ; call ddydn_sub (Bx,Jz)
    call dumpl(Jx,'Jx','mhd.dmp',imhd); imhd=imhd+1
    call dumpl(Jy,'Jy','mhd.dmp',imhd); imhd=imhd+1
    call dumpl(Jz,'Jz','mhd.dmp',imhd); imhd=imhd+1

!-----------------------------------------------------------------------
!  Alfven speed limiter / fudge factor
!-----------------------------------------------------------------------
  if (cmax .ne. 0) then
    do iz=izs,ize
      Jx(:,:,iz) = Jx(:,:,iz)*fudge(:,:,iz)
      Jy(:,:,iz) = Jy(:,:,iz)*fudge(:,:,iz)
      Jz(:,:,iz) = Jz(:,:,iz)*fudge(:,:,iz)
    end do
  end if
  call print_trace (id, dbg_mhd, 'J')

                       if (debug(dbg_mhd) .and. omp_master) then; void = dtime(cput); print '(1x,a8,2f7.3)','current',cput; endif

!-----------------------------------------------------------------------
!  Atmospheric diffusion term
!-----------------------------------------------------------------------
!
! This section contains an extra diffusion term for star-in-box
! simulations (see e.g. Braithwaite & Nordlund 2006).
! The idea is to add an extra magnetic diffusivity to the volume surrounding
! the star to force the field there into a curl-free state, thereby
! mimicking a vacuum but without the complication of a high Alfven speed
! that modelling a real low-density atmosphere would entail.
!
! This section finds the value of this diffusivity; at r<nu_inner it is zero,
! at r>nu_outer it is equal to the parameter nu_atmos, and it has intermediate
! value between the two. If nu_atmos is negative, the value of the diffusivity
! is set to the maximum permitted by the current value of dt.
!
! The next section is complicated by the need to allow for quenched diffusion
! in combination with this atmospheric diffusion.

  if (nu_atmos .ne. 0) then
    if (nu_atmos .gt. 0.) then
      nu_atmos_var = nu_atmos
    else
      safety = -nu_atmos
      nu_atmos_var = safety*Cdt**2*min(dx,dy,dz)/dt
      if (master .and. flag .and. mod(it,20) .eq. 0) print *,'nu_atmos_var=',nu_atmos_var
    endif
    thickness = nu_outer - nu_inner
    if (do_quench) then
      do iz=izs,ize
      do iy=1,my
      do ix=1,mx
        if (do_cylatmos) then
          radius = (xm(ix)**2+zmdn(iz)**2)**0.5
        else
          radius = (xm(ix)**2+ymdn(iy)**2+zmdn(iz)**2)**0.5
        end if
        scr5(ix,iy,iz) = nu_atmos_var*min(1.,max(0.,(radius-nu_inner)/thickness))
      end do
      end do
      end do
    else
      do iz=izs,ize
      do iy=1,my
      do ix=1,mx
        if (do_cylatmos) then
          radius = (xm(ix)**2+zm(iz)**2)**0.5
        else
          radius = (xm(ix)**2+ym(iy)**2+zm(iz)**2)**0.5
        end if
        eta(ix,iy,iz) = eta(ix,iy,iz) + nu_atmos_var*min(1.,max(0.,(radius-nu_inner)/thickness))
      end do
      end do
      end do
    endif
  end if

!-----------------------------------------------------------------------
!  Resistive part of the electric field
!-----------------------------------------------------------------------

  call barrier_omp('mhd2')                                              ! barrier 2;  NOT NEEDED?

  call zdn1_set (eta, scr1) ; call ydn1_set (scr1, scr2)
  if (do_quench) then
   call barrier_omp('mhd3')                                            ! barrier 3; Jx just computed
   call quenchyz (Jx, scr3, scr4)
   call regularize_face (scr3)
   if (nu_atmos .ne. 0) then
    do iz=izs,ize
    do iy=1,my
    do ix=1,mx
      Ex(ix,iy,iz)=(0.5*(dym(iy)+dzm(iz))) * (scr2(ix,iy,iz)*scr3(ix,iy,iz) + scr5(ix,iy,iz)*Jx(ix,iy,iz))  ! atmosdiff
    end do
    end do
    end do
   else
    do iz=izs,ize
    do iy=1,my
    do ix=1,mx
      Ex(ix,iy,iz) = scr2(ix,iy,iz)*(0.5*(dym(iy)+dzm(iz)))*scr3(ix,iy,iz)
    end do
    end do
    end do
   end if
  else
    do iz=izs,ize
    do iy=1,my
    do ix=1,mx
      Ex(ix,iy,iz) = scr2(ix,iy,iz)*(0.5*(dym(iy)+dzm(iz)))*Jx(ix,iy,iz)
    end do
    end do
    end do
  endif
  call print_trace (id, dbg_mhd, 'Eres')

  if (do_quench .and. nu_atmos.ne.0) then
    do iz=izs,ize
    do iy=1,my
    do ix=1,mx
      if (do_cylatmos) then
        radius = (xmdn(ix)**2+zmdn(iz)**2)**0.5
      else
        radius = (xmdn(ix)**2+ym(iy)**2+zmdn(iz)**2)**0.5
      end if
      scr5(ix,iy,iz) = nu_atmos_var*min(1.,max(0.,(radius-nu_inner)/thickness))
      if (do_cylatmos) then
        radius = (xmdn(ix)**2+zm(iz)**2)**0.5
      else
        radius = (xmdn(ix)**2+ymdn(iy)**2+zm(iz)**2)**0.5
      end if
      scr6(ix,iy,iz) = nu_atmos_var*min(1.,max(0.,(radius-nu_inner)/thickness))
    end do
    end do
    end do
  endif

  call xdn1_set (scr1, scr2)
  if (do_quench) then
   call barrier_omp('mhd4')                                            ! barrier 4; scr3 used above (NOT NEEDED?)
   call quenchzx (Jy, scr3, scr4)
   call regularize_face (scr3)
   if (nu_atmos .ne. 0) then
    do iz=izs,ize
    do iy=1,my
    do ix=1,mx
      Ey(ix,iy,iz)=(0.5*(dzm(iz)+dxm(ix))) * (scr2(ix,iy,iz)*scr3(ix,iy,iz) + scr5(ix,iy,iz)*Jy(ix,iy,iz))
    end do
    end do
    end do
   else
    do iz=izs,ize
    do iy=1,my
    do ix=1,mx
      Ey(ix,iy,iz) = scr2(ix,iy,iz)*(0.5*(dzm(iz)+dxm(ix)))*scr3(ix,iy,iz)
    end do
    end do
    end do
   end if
  else
    do iz=izs,ize
    do iy=1,my
    do ix=1,mx
      Ey(ix,iy,iz) = scr2(ix,iy,iz)*(0.5*(dzm(iz)+dxm(ix)))*Jy(ix,iy,iz)
    end do
    end do
    end do
  end if

  call ydn1_set (eta, scr1) ; call xdn1_set (scr1, scr2)
  if (do_quench) then
   call quenchxy (Jz, scr3)
   if (nu_atmos .ne. 0) then
    do iz=izs,ize
    do iy=1,my
    do ix=1,mx
      Ez(ix,iy,iz)=(0.5*(dxm(ix)+dym(iy))) * (scr2(ix,iy,iz)*scr3(ix,iy,iz) + scr6(ix,iy,iz)*Jz(ix,iy,iz))
    end do
    end do
    end do
   else
    do iz=izs,ize
    do iy=1,my
    do ix=1,mx
      Ez(ix,iy,iz) = scr2(ix,iy,iz)*(0.5*(dxm(ix)+dym(iy)))*scr3(ix,iy,iz)
    end do
    end do
    end do
   endif
  else
    do iz=izs,ize
    do iy=1,my
    do ix=1,mx
      Ez(ix,iy,iz) = scr2(ix,iy,iz)*(0.5*(dxm(ix)+dym(iy)))*Jz(ix,iy,iz)
    end do
    end do
    end do
  end if

  call print_trace (id, dbg_mhd, 'Eadv')
    call dumpl(Ex,'Ex','mhd.dmp',imhd); imhd=imhd+1
    call dumpl(Ey,'Ey','mhd.dmp',imhd); imhd=imhd+1
    call dumpl(Ez,'Ez','mhd.dmp',imhd); imhd=imhd+1

                       if (debug(dbg_mhd) .and. omp_master) then; void = dtime(cput); print '(1x,a8,2f7.3)','Eresist',cput; endif

  if (do_energy.and.do_dissipation) then
    do iz=izs,ize
      scr3(:,:,iz) = Jx(:,:,iz)*Ex(:,:,iz)
    end do
    call barrier_omp('mhd5')                                            ! barrier 5; scr3 just computed
    call zup1_set (scr3, scr1) ; call yup1_set (scr1 , scr2)
    do iz=izs,ize
      dedt(:,lb:ub,iz) = dedt(:,lb:ub,iz) + scr2(:,lb:ub,iz)
      scr2(:,:,iz) = Jy(:,:,iz)*Ey(:,:,iz)
    end do
    call xup1_set (scr2, scr1)
    call barrier_omp('mhd6')                                            ! barrier 6; make sure scr3 is not changed
    call zup1_set (scr1, scr3)
    do iz=izs,ize
      dedt(:,lb:ub,iz) = dedt(:,lb:ub,iz) + scr3(:,lb:ub,iz)
      scr3(:,:,iz) = Jz(:,:,iz)*Ez(:,:,iz)
    end do
    call barrier_omp('mhd6a')                                           ! barrier 6a; make sure scr3 is not changed
    call yup1_set (scr3, scr1) ; call xup1_set (scr1 , scr2)
    do iz=izs,ize
      dedt(:,lb:ub,iz) = dedt(:,lb:ub,iz) + scr2(:,lb:ub,iz)
    end do
  end if
  call print_trace (id, dbg_mhd, 'Qj')
    call dumpl(Ey,'Ey1','mhd.dmp',imhd); imhd=imhd+1
    call dumpl(Ez,'Ez1','mhd.dmp',imhd); imhd=imhd+1

  call print_trace (id, dbg_mhd, 'bdry')

                       if (debug(dbg_mhd) .and. omp_master) then; void = dtime(cput); print '(1x,a8,2f7.3)','Qjoule ',cput; endif

!-----------------------------------------------------------------------
!  Lorenz force = I x B
!-----------------------------------------------------------------------
  call ydn_set (Bx,wk10) ; call zdn_set (Bx,wk13)
  call zdn_set (By,wk11) ; call xdn_set (By,wk14)
  call xdn_set (Bz,wk12) ; call ydn_set (Bz,wk15)
  do iz=izs,ize
    scr1(:,:,iz) = Jy(:,:,iz)*wk12(:,:,iz)
  end do
  call barrier_omp('mhd7')                                              ! barrier 7; scr1 just computed
  call zup_add (scr1, dpxdt)
  do iz=izs,ize
    scr2(:,:,iz) = Jz(:,:,iz)*wk14(:,:,iz)
  end do
  call yup_sub (scr2, dpxdt)
  do iz=izs,ize
    scr2(:,:,iz) = Jz(:,:,iz)*wk10(:,:,iz)
  end do
  call xup_add (scr2, dpydt)
  do iz=izs,ize
    scr3(:,:,iz) = Jx(:,:,iz)*wk15(:,:,iz)
  end do
  call barrier_omp('mhd8')                                              ! barrier 8; scr3 just computed
  call zup_sub (scr3, dpydt)
  do iz=izs,ize
    scr1(:,:,iz) = Jx(:,:,iz)*wk11(:,:,iz)
  end do
  call yup_add (scr1, dpzdt)
  do iz=izs,ize
    scr1(:,:,iz) = Jy(:,:,iz)*wk13(:,:,iz)
  end do
  call xup_sub (scr1, dpzdt)
  call barrier_omp('mhd9')                                              ! barrier 9; NOT NEEDED??
  call print_trace (id, dbg_mhd, 'Lorentz')

                       if (debug(dbg_mhd) .and. omp_master) then; void = dtime(cput); print '(1x,a8,2f7.3)','Lforce ',cput; endif

!-----------------------------------------------------------------------
!  Electric field   E =  eta I - uxB
!-----------------------------------------------------------------------

  if (t_friction .lt. 0.) then                                          ! B fixed if t_friction is -ve
    if (.not. do_upping) then
    if (do_cylatmos) then
      do iz=izs,ize
       do iy=1,my
        do ix=1,mx
          radius = (xm(ix)**2+zmdn(iz)**2)**0.5
          if (radius .lt. 0.5*(nu_inner+nu_outer)) then Ex(ix,iy,iz)=0.
          radius = (xmdn(ix)**2+zmdn(iz)**2)**0.5
          if (radius .lt. 0.5*(nu_inner+nu_outer)) then Ey(ix,iy,iz)=0.
          radius = (xmdn(ix)**2+zm(iz)**2)**0.5
          if (radius .lt. 0.5*(nu_inner+nu_outer)) then Ez(ix,iy,iz)=0.
        end do
       end do
      end do
    else
      do iz=izs,ize
       do iy=1,my
        do ix=1,mx
          radius = (xm(ix)**2+ymdn(iy)**2+zmdn(iz)**2)**0.5
          if (radius .lt. 0.5*(nu_inner+nu_outer)) then Ex(ix,iy,iz)=0.
          radius = (xmdn(ix)**2+ym(iy)**2+zmdn(iz)**2)**0.5
          if (radius .lt. 0.5*(nu_inner+nu_outer)) then Ey(ix,iy,iz)=0.
          radius = (xmdn(ix)**2+ymdn(iy)**2+zm(iz)**2)**0.5
          if (radius .lt. 0.5*(nu_inner+nu_outer)) then Ez(ix,iy,iz)=0.
        end do
       end do
      end do
    end if
    end if
  else

  call zdn_set (Uy,scr2) ; call ydn_set (Uz,scr6)
  do iz=izs,ize
    Ex(:,:,iz) = Ex(:,:,iz) - scr2(:,:,iz)*wk15(:,:,iz) + scr6(:,:,iz)*wk11(:,:,iz)
  end do
  call xdn_set (Uz,scr3) ; call zdn_set (Ux,scr4)
  do iz=izs,ize
    Ey(:,:,iz) = Ey(:,:,iz) - scr3(:,:,iz)*wk13(:,:,iz) + scr4(:,:,iz)*wk12(:,:,iz)
  end do
  call ydn_set (Ux,scr1) ; call xdn_set (Uy,scr5)
  do iz=izs,ize
    Ez(:,:,iz) = Ez(:,:,iz) - scr1(:,:,iz)*wk14(:,:,iz) + scr5(:,:,iz)*wk10(:,:,iz)
  end do

  end if

  call print_trace (id, dbg_mhd, 'E')
    call dumpl(Ey,'Ey2','mhd.dmp',imhd); imhd=imhd+1
    call dumpl(Ez,'Ez2','mhd.dmp',imhd); imhd=imhd+1

                       if (debug(dbg_mhd) .and. omp_master) then; void = dtime(cput); print '(1x,a8,2f7.3)','Eadvect',cput; endif

  call efield_boundary (Ex, Ey, Ez, wk10, wk13, wk11, wk14, wk12, wk15, &
                        Bx, By, Bz, scr1, scr4, scr2, scr5, scr3, scr6, &
                        Ux, Uy, Uz)

                       if (debug(dbg_mhd) .and. omp_master) then; void = dtime(cput); print '(1x,a8,2f7.3)','Ebdry  ',cput; endif

!-----------------------------------------------------------------------
!  Magnetic field's time derivative, dBdt = - curl(E)
!-----------------------------------------------------------------------
  call barrier_omp('mhd10')                                             ! barrier 10; Ex, Ey just computed
    call dumpl(Ey,'Ey3','mhd.dmp',imhd); imhd=imhd+1
    call dumpl(Ez,'Ez3','mhd.dmp',imhd); imhd=imhd+1

  if (do_upping) then
    do iz=izs,ize
      scr1(:,:,iz) = dBxdt(:,:,iz)
      scr2(:,:,iz) = dBydt(:,:,iz)
      scr3(:,:,iz) = dBzdt(:,:,iz)
    end do
  end if

  call ddzup_add (Ey, dBxdt) ; call ddyup_sub (Ez, dBxdt)
  call ddxup_add (Ez, dBydt) ; call ddzup_sub (Ex, dBydt)
  call ddyup_add (Ex, dBzdt) ; call ddxup_sub (Ey, dBzdt)

!-----------------------------------------------------------------------
!  Field-upping, to keep magnetic energy constant
!  (to maintain healthy separation of Alfven and diffusive timescales)
!-----------------------------------------------------------------------
  if (do_upping) then
    do iz=izs,ize
      scr5(:,:,iz) = (Bx(:,:,iz)**2+By(:,:,iz)**2+Bz(:,:,iz)**2)
      scr4(:,:,iz) = (dBxdt(:,:,iz)-scr1(:,:,iz))*Bx(:,:,iz) + &
                     (dBydt(:,:,iz)-scr2(:,:,iz))*By(:,:,iz) + &
                     (dBzdt(:,:,iz)-scr3(:,:,iz))*Bz(:,:,iz)
    end do
    call barrier_omp('mhd11a')
    call average_subr (scr4,t_Bgrowth) !int(B.dBdt)dV
    call average_subr (scr5,E_mag)     !int(B.B)dV
    !$omp single
    E_mag     = E_mag*0.5*mx*my*mz*dx*dy*dz
    t_Bgrowth = t_Bgrowth*mx*my*mz*dx*dy*dz
    !print *,'E_mag, B.dBdt, t_Bgrowth',E_mag, t_Bgrowth, -E_mag/t_Bgrowth
    t_Bgrowth = -E_mag/t_Bgrowth 
    !$omp end single nowait
    call barrier_omp('mhd11b')
    do iz=izs,ize
      dBxdt(:,:,iz) = dBxdt(:,:,iz) + (1./(2.*t_Bgrowth))*Bx(:,:,iz)
      dBydt(:,:,iz) = dBydt(:,:,iz) + (1./(2.*t_Bgrowth))*By(:,:,iz)
      dBzdt(:,:,iz) = dBzdt(:,:,iz) + (1./(2.*t_Bgrowth))*Bz(:,:,iz)
    end do
    call barrier_omp('mhd11c')
    if (flag) then
      !$omp single
      E_prop = E_prop * (1.-dt/t_Bgrowth)
      t_prop = t_prop + dt*sqrt(E_mag/E_prop)
      !$omp end single nowait
    endif
    call barrier_omp('mhd11d')
  end if
  call print_trace (id, dbg_mhd, 'dBdt')
    call dumpl(dBxdt,'dBxdt','mhd.dmp',imhd); imhd=imhd+1
    call dumpl(dBydt,'dBydt','mhd.dmp',imhd); imhd=imhd+1

                       if (debug(dbg_mhd) .and. omp_master) then; void = dtime(cput); print '(1x,a8,2f7.3)','induct',cput; endif

  call print_trace (id, dbg_mhd, 'END')
  END
!**********************************************************************
