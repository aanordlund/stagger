! $Id: mhd_call.f90,v 1.33 2010/11/08 12:42:14 mactag Exp $
!***********************************************************************
  SUBROUTINE mhd(flag,r,ee,Ux,Uy,Uz,Bx,By,Bz,Ex,Ey,Ez,dpxdt,dpydt,dpzdt,dedt,dBxdt,dBydt,dBzdt)

  USE params
  implicit none

  logical flag
  real, dimension(mx,my,mz) :: &
       r,ee,Ux,Uy,Uz,Bx,By,Bz,Ex,Ey,Ez,dpxdt,dpydt,dpzdt,dedt,dBxdt,dBydt,dBzdt
  real, allocatable, dimension(:,:,:) :: &
       Jx,Jy,Jz,scr1,scr2,scr3, &
       Rx,Ry,Rz, &
       Bx_y,Bx_z,By_x,By_z,Bz_x,Bz_y,    &
       Ux_y,Ux_z,Uy_x,Uy_z,Uz_x,Uz_y,fudge,eta
  integer ix, iy, iz, j
  real ds
  real:: elapsed, tm(30)
  real, allocatable, dimension(:,:,:) :: &
         Bxl,Byl,Bzl,Bxu,Byu,Bzu

  character(len=mid) id
  data id/'$Id: mhd_call.f90,v 1.33 2010/11/08 12:42:14 mactag Exp $'/

  call print_trace (id, dbg_mhd, 'BEGIN')
  if (idbg>1) then; j=1;tm(j)=elapsed(); end if

  allocate (eta(mx,my,mz), fudge(mx,my,mz))
  if (do_trace) print *,'resist'
  call resist(eta,fudge,r,ee,Ux,Uy,Uz,Bx,By,Bz,flag)

!-----------------------------------------------------------------------
!  Store original boundary layer B
!-----------------------------------------------------------------------
  if (do_trace) print *,'Boundaray layer B'
  allocate (Bxl(mx,lb,mz), Byl(mx,lb,mz), Bzl(mx,lb,mz))
  allocate (Bxu(mx,my-ub,mz), Byu(mx,my-ub,mz), Bzu(mx,my-ub,mz))
!$omp parallel do private(iy,iz)
  do iz=1,mz
    do iy=1,lb
      Bxl(:,iy,iz)=Bx(:,iy,iz)
      Byl(:,iy,iz)=By(:,iy,iz)
      Bzl(:,iy,iz)=Bz(:,iy,iz)
    end do
    do iy=1,my-ub
      Bxu(:,iy,iz)=Bx(:,ub+iy,iz)
      Byu(:,iy,iz)=By(:,ub+iy,iz)
      Bzu(:,iy,iz)=Bz(:,ub+iy,iz)
    end do
  end do
!-----------------------------------------------------------------------
!  Electric current I = curl(B), resistive E, and dissipation
!-----------------------------------------------------------------------
  if (do_trace) print *,'current'
  call mfield_boundary (Ux,Uy,Uz,Bx,By,Bz)
  if (idbg>1) then; j=j+1;tm(j)=elapsed(); print '(a4,i3,2f7.3)','mhd',j,tm(j)-tm(j-1),tm(j)-tm(1); endif

  allocate (Jx(mx,my,mz), Jy(mx,my,mz), Jz(mx,my,mz))
  allocate (Bx_y(mx,my,mz), Bx_z(mx,my,mz))
  allocate (By_z(mx,my,mz), By_x(mx,my,mz))
  allocate (Bz_x(mx,my,mz), Bz_y(mx,my,mz))
  
  call ddydn_set(Bz,Bz_y) ; call ddzdn_set(By,By_z)
  call ddzdn_set(Bx,Bx_z) ; call ddxdn_set(Bz,Bz_x)
  call ddxdn_set(By,By_x) ; call ddydn_set(Bx,Bx_y)
!$omp parallel do private(iz)
  do iz=1,mz
    Jx(:,:,iz) = Bz_y(:,:,iz) - By_z(:,:,iz)
    Jy(:,:,iz) = Bx_z(:,:,iz) - Bz_x(:,:,iz)
    Jz(:,:,iz) = By_x(:,:,iz) - Bx_y(:,:,iz)
  end do
  nflop = nflop+3
  call regularize_face (Jx)
  call regularize_face (Jz)

!-----------------------------------------------------------------------
!  Alfven speed limiter / fudge factor
!-----------------------------------------------------------------------
  if (cmax .ne. 0) then
!$omp parallel do private(iz)
    do iz=1,mz
      Jx(:,:,iz) = Jx(:,:,iz)*fudge(:,:,iz)
      Jy(:,:,iz) = Jy(:,:,iz)*fudge(:,:,iz)
      Jz(:,:,iz) = Jz(:,:,iz)*fudge(:,:,iz)
    end do
    nflop = nflop+3
  end if
  deallocate (fudge)

!-----------------------------------------------------------------------
!  Resistive part of the electric field
!-----------------------------------------------------------------------
  allocate (scr1(mx,my,mz), scr2(mx,my,mz))
  if (do_trace) print *,'resistive E'
  call zdn1_set (eta, scr1) ; call ydn1_set (scr1, scr2)
!$omp parallel do private(iz)
  do iz=1,mz
  do iy=1,my
  do ix=1,mx
    Ex(ix,iy,iz) = scr2(ix,iy,iz)*(0.5*(dzmdn(iz)+dymdn(iy)))*Jx(ix,iy,iz)
  end do
  end do
  end do
  nflop = nflop+2
  call xdn1_set (scr1, scr2)
!$omp parallel do private(iz)
  do iz=1,mz
  do iy=1,my
  do ix=1,mx
    Ey(ix,iy,iz) = scr2(ix,iy,iz)*(0.5*(dzmdn(iz)+dxmdn(ix)))*Jy(ix,iy,iz)
  end do
  end do
  end do
  nflop = nflop+2
  call ydn1_set (eta, scr1) ; call xdn1_set (scr1, scr2)
  deallocate (eta)
!$omp parallel do private(iz)
  do iz=1,mz
  do iy=1,my
  do ix=1,mx
    Ez(ix,iy,iz) = scr2(ix,iy,iz)*(0.5*(dymdn(iy)+dxmdn(ix)))*Jz(ix,iy,iz)
  end do
  end do
  end do
  nflop = nflop+2
  if (idbg>1) then; j=j+1;tm(j)=elapsed(); print '(a4,i3,2f7.3)','mhd',j,tm(j)-tm(j-1),tm(j)-tm(1); endif

if (do_energy .and. do_dissipation) then
  allocate (scr3(mx,my,mz))
!$omp parallel do private(iz)
  do iz=1,mz
    scr3(:,:,iz) = Jx(:,:,iz)*Ex(:,:,iz)
  end do
  call zup1_set (scr3, scr1) ; call yup1_set (scr1 , scr2)
  call dumpn(scr2,'Jx*Ex','qjoule.dmp',1)
!$omp parallel do private(iz)
  do iz=1,mz
    dedt(:,:,iz) = dedt(:,:,iz) + scr2(:,:,iz)
    scr3(:,:,iz) = Jy(:,:,iz)*Ey(:,:,iz)
  end do
  nflop = nflop+1
  call xup1_set (scr3, scr1) ; call zup1_set (scr1 , scr2)
  call dumpn(scr2,'Jy*Ey','qjoule.dmp',2)
!$omp parallel do private(iz)
  do iz=1,mz
    dedt(:,:,iz) = dedt(:,:,iz) + scr2(:,:,iz)
    scr3(:,:,iz) = Jz(:,:,iz)*Ez(:,:,iz)
  end do
  nflop = nflop+1
  call yup1_set (scr3, scr1) ; call xup1_set (scr1 , scr2)
  call dumpn(scr2,'Jz*Ez','qjoule.dmp',3)
!$omp parallel do private(iz)
  do iz=1,mz
    dedt(:,:,iz) = dedt(:,:,iz) + scr2(:,:,iz)
  end do
  call dumpn(dedt,'dedt+Q','qjoule.dmp',4)
  nflop = nflop+1
  deallocate (scr3)
end if
  if (idbg>1) then; j=j+1;tm(j)=elapsed(); print '(a4,i3,2f7.3)','mhd',j,tm(j)-tm(j-1),tm(j)-tm(1); endif

! if (lb.gt.1) then
    allocate (Rx(mx,2,mz),Ry(mx,2,mz),Rz(mx,2,mz))
    Rx(:,:,:) = Ex(:,lb:lb+1,:)
    Ry(:,:,:) = Ey(:,lb:lb+1,:)
    Rz(:,:,:) = Ez(:,lb:lb+1,:)
! end if

!-----------------------------------------------------------------------
!  Lorenz force = I x B
!-----------------------------------------------------------------------
  if (do_trace) print *,'Lorentz force'
  call ydn_set(Bx,Bx_y) ; call zdn_set(Bx,Bx_z)
  call zdn_set(By,By_z) ; call xdn_set(By,By_x)
  call xdn_set(Bz,Bz_x) ; call ydn_set(Bz,Bz_y)

!$omp parallel do private(iz)
  do iz=1,mz
    scr2(:,:,iz) = Jy(:,:,iz)*Bz_x(:,:,iz)
  end do
  nflop = nflop+1
  call zup_set(scr2, scr1)
!$omp parallel do private(iz)
  do iz=1,mz
    dpxdt(:,:,iz) = dpxdt(:,:,iz) + scr1(:,:,iz)
    scr2(:,:,iz) = Jz(:,:,iz)*By_x(:,:,iz)
  end do
  nflop = nflop+2
  call yup_set(scr2, scr1)
!$omp parallel do private(iz)
  do iz=1,mz
    dpxdt(:,:,iz) = dpxdt(:,:,iz) - scr1(:,:,iz)
    scr2(:,:,iz) = Jz(:,:,iz)*Bx_y(:,:,iz)
  end do
  nflop = nflop+3
  call xup_set(scr2, scr1)
!$omp parallel do private(iz)
  do iz=1,mz
    dpydt(:,:,iz) = dpydt(:,:,iz) + scr1(:,:,iz)
    scr2(:,:,iz) = Jx(:,:,iz)*Bz_y(:,:,iz)
  end do
  nflop = nflop+2
  call zup_set(scr2, scr1)

!$omp parallel do private(iz)
  do iz=1,mz
    dpydt(:,:,iz) = dpydt(:,:,iz) - scr1(:,:,iz)
    scr2(:,:,iz) = Jx(:,:,iz)*By_z(:,:,iz)
  end do
  nflop = nflop+2
  call yup_set(scr2, scr1)
!$omp parallel do private(iz)
  do iz=1,mz
    dpzdt(:,:,iz) = dpzdt(:,:,iz) + scr1(:,:,iz)
    scr2(:,:,iz) = Jy(:,:,iz)*Bx_z(:,:,iz)
  end do
  nflop = nflop+2
  call xup_set(scr2, scr1)
!$omp parallel do private(iz)
  do iz=1,mz
    dpzdt(:,:,iz) = dpzdt(:,:,iz) - scr1(:,:,iz)
  end do
  nflop = nflop+1
  if (idbg>1) then; j=j+1;tm(j)=elapsed(); print '(a4,i3,2f7.3)','mhd',j,tm(j)-tm(j-1),tm(j)-tm(1); endif

  deallocate (Jx, Jy, Jz)

!-----------------------------------------------------------------------
!  Electric field   E =  eta I - uxB
!-----------------------------------------------------------------------
  if (do_trace) print *,'E <- UxB'
  deallocate (scr1,scr2)
  allocate (Ux_y(mx,my,mz),Ux_z(mx,my,mz),Uy_z(mx,my,mz),Uy_x(mx,my,mz),Uz_x(mx,my,mz),Uz_y(mx,my,mz))
  if (do_trace) print *,'Electric Field'
  call zdn_set(Uy,Uy_z) ; call ydn_set(Uz,Uz_y)
!$omp parallel do private(iz)
  do iz=1,mz
    Ex(:,:,iz) = Ex(:,:,iz) - Uy_z(:,:,iz)*Bz_y(:,:,iz) + Uz_y(:,:,iz)*By_z(:,:,iz)
  end do
  nflop = nflop+4
  call xdn_set(Uz,Uz_x) ; call zdn_set(Ux,Ux_z)
!$omp parallel do private(iz)
  do iz=1,mz
    Ey(:,:,iz) = Ey(:,:,iz) - Uz_x(:,:,iz)*Bx_z(:,:,iz) + Ux_z(:,:,iz)*Bz_x(:,:,iz)
  end do
  nflop = nflop+4
  call ydn_set(Ux,Ux_y) ; call xdn_set(Uy,Uy_x)
!$omp parallel do private(iz)
  do iz=1,mz
    Ez(:,:,iz) = Ez(:,:,iz) - Ux_y(:,:,iz)*By_x(:,:,iz) + Uy_x(:,:,iz)*Bx_y(:,:,iz)
  end do
  nflop = nflop+4
  if (idbg>1) then; j=j+1;tm(j)=elapsed(); print '(a4,i3,2f7.3)','mhd',j,tm(j)-tm(j-1),tm(j)-tm(1); endif

  if (do_trace) print *,'efield bdry'
  call efield_boundary (Ex, Ey, Ez, Bx_y, Bx_z, By_z, By_x, Bz_x, Bz_y, &
                        Rx, Ry, Rz, Ux_y, Ux_z, Uy_z, Uy_x, Uz_x, Uz_y, &
                        Ux, Uy, Uz, Bxl, Byl, Bzl, Bxu, Byu, Bzu)
  deallocate (Bx_y, Bx_z, By_z, By_x, Bz_x, Bz_y)
  deallocate (Ux_y, Ux_z, Uy_z, Uy_x, Uz_x, Uz_y)
  if (idbg>1) then; j=j+1;tm(j)=elapsed(); print '(a4,i3,2f7.3)','mhd',j,tm(j)-tm(j-1),tm(j)-tm(1); endif

  if (lb.gt.1) then
    deallocate (Rx, Ry, Rz)
  end if

!-----------------------------------------------------------------------
!  Magnetic field's time derivative, dBdt = - curl(E)
!-----------------------------------------------------------------------
  if (do_trace) print *,'dBdt'
  allocate (scr1(mx,my,mz),scr2(mx,my,mz))
  call ddzup_set(Ey, scr1) ; call ddyup_set(Ez, scr2)
!$omp parallel do private(iz)
  do iz=1,mz
    dBxdt(:,:,iz) = dBxdt(:,:,iz) + scr1(:,:,iz) - scr2(:,:,iz)
  end do
  call dumpn(dBxdt,'dBxdt','qjoule.dmp',5)
  nflop = nflop+2
  call ddxup_set(Ez, scr1) ; call ddzup_set(Ex, scr2)
!$omp parallel do private(iz)
  do iz=1,mz
    dBydt(:,:,iz) = dBydt(:,:,iz) + scr1(:,:,iz) - scr2(:,:,iz)
  end do
  call dumpn(dBydt,'dBydt','qjoule.dmp',6)
  nflop = nflop+2
  call ddyup_set(Ex, scr1) ; call ddxup_set(Ey, scr2)
!$omp parallel do private(iz)
  do iz=1,mz
    dBzdt(:,:,iz) = dBzdt(:,:,iz) + scr1(:,:,iz) - scr2(:,:,iz)
  end do
  call dumpn(dBzdt,'dBzdt','qjoule.dmp',7)
  nflop = nflop+2
  deallocate (scr1, scr2)

!-----------------------------------------------------------------------
!  Fictive magnetic field growth, e.g. to slowly increase AR fields
!-----------------------------------------------------------------------
  if (t_Bgrowth.ne.0.) then
!$omp parallel do private(iz)
    do iz=1,mz
      dBxdt(:,:,iz) = dBxdt(:,:,iz) + (1./t_Bgrowth)*Bx(:,:,iz)
      dBydt(:,:,iz) = dBydt(:,:,iz) + (1./t_Bgrowth)*By(:,:,iz)
      dBzdt(:,:,iz) = dBzdt(:,:,iz) + (1./t_Bgrowth)*Bz(:,:,iz)
    end do
    nflop = nflop+2
  end if
  if (idbg>1) then; j=j+1;tm(j)=elapsed(); print '(a4,i3,2f7.3)','mhd',j,tm(j)-tm(j-1),tm(j)-tm(1); endif
!-----------------------------------------------------------------------
!  Restore original boundary layer B
!-----------------------------------------------------------------------
!$omp parallel do private(iy,iz)
  do iz=1,mz
    do iy=1,lb
      Bx(:,iy,iz)=Bxl(:,iy,iz)
      By(:,iy,iz)=Byl(:,iy,iz)
      Bz(:,iy,iz)=Bzl(:,iy,iz)
    end do
    do iy=1,my-ub
      Bx(:,ub+iy,iz)=Bxu(:,iy,iz)
      By(:,ub+iy,iz)=Byu(:,iy,iz)
      Bz(:,ub+iy,iz)=Bzu(:,iy,iz)
    end do
  end do
  deallocate (Bxl, Byl, Bzl, Bxu, Byu, Bzu)
!-----------------------------------------------------------------------
  call print_trace (id, dbg_mhd, 'END')
  END
!**********************************************************************
