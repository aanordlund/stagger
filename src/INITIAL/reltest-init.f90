! $Id: reltest-init.f90,v 1.2 2005/04/05 17:08:03 troels_h Exp $
!-----------------------------------------------------------------------
SUBROUTINE test_values (r,px,py,pz,e,d,Bx,By,Bz)
  USE params
  USE stagger
  implicit none

  real, dimension(mx,my,mz):: r,px,py,pz,e,d,Bx,By,Bz
  character(len=16):: type
  real:: gam1,average,u1,u2,up1,up2,p1,p2,r1,r2,hm1,hm2,v1,v2,g1,g2,e0,B1,B2,Bp1,Bp2,ampl,fmaxval
  integer:: i,ix,iy,iz,smoothfac
  namelist /shock/type,smoothfac,r1,r2,p1,p2,B1,B2,Bp1,Bp2,v1,v2,u1,u2,up1,up2,e0,ampl

!-----------------------------------------------------------------------
!  Initial value parameters
!-----------------------------------------------------------------------
  type='shocktube1'
  ampl= 0.001
  r1  = 1.
  r2  = 1.
  e0  = .01
  hm1 = .1
  hm2 = .1
  g1  = 1.
  g2  = 1.
  u1  = 0.
  u2  = 0.
  up1 = 0.
  up2 = 0.
  v1  = 0.
  v2  = 0.
  B1  = 0.
  B2  = 0.
  Bp1 = 0.
  Bp2 = 0.
  smoothfac = 1
  
  read  (*,shock)
  write (*,shock)

  r  = 1.
  px = 0.
  py = 0.
  pz = 0.
  e  = 1.

  if (do_mhd) then
    bx = 0.
    by = 0.
    bz = 0.
  end if

  select case (type)
! Initialise problem according to name.
! Problems are taken from Komissarov paper MNRAS 303, p. 343 (1999)
! But there are errors. See astro-ph/0209213.
  case ('shocktube1')
    gam1 = gamma-1.
    px = 0.
    py = 0.
    pz = 0.
!    r1 = 1.
!    r2 = .1
!    p1 = 10.
!    p2 = 1.
    B1 = 1.
    B2 = 1.
    hm1 = p1/r1*gamma/gam1
    hm2 = p2/r2*gamma/gam1
    call SplitValue(r,r1,r2)          ! density r
    call SplitValue(py,hm1,hm2)       ! enthalpy h-1
    If (do_mhd) call SplitValue(By,B1,B2) ! Magnetic field
    do iy=1,smoothfac
      r  = exp(smooth3(alog(r)))      ! density r
      py = exp(smooth3(alog(py)))     ! enthalpy h-1
    end do
    e = r*py - gam1/gamma*py*r ! e = D(hW - 1) - A(h-1)D/W
    !px = 0.
    py = 0.
    !pz = 0.
    call clearedge(r,smoothfac)
    call clearedge(e,smoothfac)
    If (do_mhd) By = ydn(By)          ! Remember we are on a staggered mesh
    If (do_mhd) call clearedge(By,smoothfac)
  case ('shocktube2')
    gam1 = gamma-1.
    px = 0.
    py = 0.
    pz = 0.
!    r1 = 1.
!    r2 = .1
!    p1 = 30.
!    p2 = 1.
!    B1 = 20.
!    B2 = 0.
    hm1 = p1/r1*gamma/gam1
    hm2 = p2/r2*gamma/gam1
    call SplitValue(r,r1,r2)          ! density r
    call SplitValue(py,hm1,hm2)       ! enthalpy h-1
    if (do_mhd) call SplitValue(Bz,B1,B2) ! Magnetic field
    do iy=1,smoothfac
      r  = exp(smooth3(alog(r)))      ! density r
      py = exp(smooth3(alog(py)))     ! enthalpy h-1
      if (do_mhd) Bz = smooth3(Bz)
    end do
    e = r*py - gam1/gamma*py*r ! e = D(hW - 1) - A(h-1)D/W
    !px = 0.
    py = 0.
    !pz = 0.
    call clearedge(r,smoothfac)
    call clearedge(e,smoothfac)
    If (do_mhd) Bz = zdn(Bz)          ! Remember we are on a staggered mesh
    If (do_mhd) call clearedge(Bz,smoothfac)
  case ('collision')
    gam1 = (gamma-1.)/gamma
    px = 0.
    py = 0.
    pz = 0.
    g1 = sqrt(1. + u1**2 + up1**2)
    g2 = sqrt(1. + u2**2 + up2**2)
    v1 = u1/g1 
    v2 = u2/g2 
    hm1 = p1/r1/gam1
    hm2 = p2/r2/gam1
    call SplitValue(r,r1,r2)               ! density r
    call SplitValue(px,hm1,hm2)            ! enthalpy h-1
    call SplitValue(py,u1,u2)              ! four velocity in y-direction
    call SplitValue(pz,up1,up2)            ! four velocity in z-direction
    if (do_mhd) call SplitValue(By,Bp1,Bp2)! Magnetic field in y-direction
    if (do_mhd) call SplitValue(Bz,B1, B2) ! Magnetic field in z-direction
    do iy=1,smoothfac
      r  = exp(smooth3(alog(r)))           ! density r
      px = exp(smooth3(alog(px)))          ! enthalpy h-1
      py = smooth3(py)                     ! four velocity in y direction
      pz = smooth3(pz)                     ! four velocity in z direction
      if (do_mhd) By = smooth3(By)
      if (do_mhd) Bz = smooth3(Bz)
    end do
    e  = sqrt(1.+py**2 + pz**2)            ! e = W = sqrt(1. + uy**2 + uz**2)
    py = e*r*(px+1.)*py                    ! T0y = DhUy  = W*r*(hm1 + 1)*Uy = e*r*(px+1)*py
    pz = e*r*(px+1.)*pz                    ! T0z = DhUz  = W*r*(hm1 + 1)*Uz = e*r*(px+1)*pz
    py = ydn(py)                           ! Remember we are on a staggered mesh
    pz = zdn(pz)                           ! Remember we are on a staggered mesh
    call clearedge(py,smoothfac+5)
    e  = (yup(py)**2 + zup(pz)**2)/(r*(px+1.))**2  ! e = v**2 W**4
    e  = sqrt((1. + sqrt(1. + 4.*e))/2.)   ! e  = W 
    r  = r*e                               ! D  = rho*W = r*e
    call dump(r,'initial.dat',1)
    call dump(e,'initial.dat',2)
    ! vW = (zup(pz)**2 + yup(py)**2)/(r*(px+1.))**2 ! vW**2 = (T0y**2 + T0z**2)/(D*h)**2
    ! e  = T00p - D = D((h-1)(W-gam1/W) - vW**2/(W+1))
    e = r*(px*(e-gam1/e) + ((zup(pz)**2 + yup(py)**2)/(r*(px+1.))**2)/(e+1.))   
    px = 0
    call dump(e,'initial.dat',3)
    call dump(py,'initial.dat',4)
    call clearedge(r,smoothfac+5)
    call clearedge(py,smoothfac+5)
    call clearedge(e,smoothfac+5)
    if (do_mhd) then
      By = ydn(By)                         ! Remember we are on a staggered mesh
      Bz = zdn(Bz)                         ! Remember we are on a staggered mesh
      call clearedge(By,smoothfac+6)
      call clearedge(Bz,smoothfac+6)
    endif
  case ('alfvenpulse')
    ! B1 is the ambient beta parameter measured according to the article
    ! by De Villers & Hawley as:
    !      B1**2 = beta**2 = 2P/|b**2| = 2P*gamma/((1-Vy**2)Bx**2)
    ! e0 is the ambient internal energy (e0 = 1e-2)
    ! r1 is the ambient density (r1 =1.)
    ! v1 is the background flow velocity in the x direction
    ! ampl is the amplitude of the pulse (ampl = 1e-3)
    ! The box should have length sy=3 and be periodic
    ! initially there is a square pulse between y=1 and y=2
    gam1 = gamma-1.
    px = 0.
    py = 0.
    pz = 0.
    g1 = 1./sqrt(1. + v1**2)
    u1 = v1*g1
    hm1= gam1*e0
    if (.not. do_mhd) then
      print *, "To run the alfven pulse test you need do_mhd=t"
      stop
    endif
    ! B1 is here used to indicate the magnetic beta parameter
    By = sqrt(2.*gam1*gamma*r1*e0/B1**2)   ! By = sqrt(2*P*gamma/beta**2)
    r  = r1                                ! ambient density
    py = v1                                ! ambient velocity in y-direction
    call MakePulse(pz,0.,ampl,-ampl,0.)    ! Make a pulse in vz
    do iy=1,smoothfac
      pz = smooth3(pz)                     ! velocity in z direction
    end do
    By = By/sqrt(1. - pz**2)               ! By = By/sqrt(1-Vz**2) = sqrt(2*P*gamma/beta**2/(1-Vy**2))
    e  = 1./sqrt(1. - py**2 - pz**2)       ! e = W = 1./sqrt(1. - py**2 - pz**2)
    px = (py**2 + pz**2)*e**2              ! px= vW**2
    r  = r*e                               ! D = r*W
    py = r*(hm1+1.)*e*py                   ! T0y = DhUy  = W*r*(hm1 + 1)*Vy
    pz = r*(hm1+1.)*e*pz                   ! T0z = DhUz  = W*r*(hm1 + 1)*Vz
    ! e  = T00p - D = D((h-1)(W-(gamma-1)/(gamma*W)) - vW**2/(W+1))
    e = r*(hm1*(e-gam1/(gamma*e)) + px/(e+1.))
    px = 0.
    Bx = 0.
    Bz = 0.
  case default
    Print *, "Wrong test name. Possible names are:"
    Print *, "shocktube1, shocktube2, collision"
    stop
  end select
CONTAINS
  Subroutine SplitValue(f,f1,f2)
    Real, Dimension(mx,my,mz), intent(out):: f
    Real                     , intent(in) :: f1,f2
    integer                               :: iy
    do iy=1,my/2
      f(:,iy,:)= f1
    end do
    do iy=my/2+1,my
      f(:,iy,:)= f2
    end do    
  END Subroutine SplitValue
!
  Subroutine MakePulse(f,f0,f1,f2,f3)
    Real, Dimension(mx,my,mz), intent(out):: f
    Real                     , intent(in) :: f0,f1,f2,f3
    integer                               :: iy
    do iy=1,my/3
      f(:,iy,:)= f0
    end do
    do iy=my/3,my/2
      f(:,iy,:)= f1
    end do
    do iy=my/2+1,2*my/3
      f(:,iy,:)= f2
    end do    
    do iy=2*my/3+1,my
      f(:,iy,:)= f3
    end do    
  END Subroutine MakePulse

  Subroutine ClearEdge(f,sf)
    Real, Dimension(mx,my,mz), intent(out):: f
    integer,                   intent(in) :: sf
    integer                               :: iy
    do iy=1,sf+2
      f(:,iy,:)= f(:,sf+5,:)
    end do
    do iy=my-sf-1,my
      f(:,iy,:)= f(:,my-sf-4,:)
    end do    
  END Subroutine ClearEdge
END
