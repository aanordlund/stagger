! $Id: planetary_disk_init2.f90,v 1.6 2006/06/12 09:07:26 aake Exp $

!***********************************************************************
FUNCTION exp1(x)
  real :: x, exp1
  exp1 = exp(min(max(x,-70.),70.))
  exp1 = exp1/(1.+exp1)
END

!***********************************************************************
FUNCTION sqr1(x,a,b)
!
!  Implements a function that varies as (x/a)**2 for x<<b, and
!  then as (x/b)*(b/a)**2 for x>>b.
!
  real :: x, sqr1
  sqr1 = sqrt(1.+(x/b)**2)-1.       ! ~x/b for x>>b, ~0.5*(x/b)**2 for x<<b
  sqr1 = 2.*(b/a)**2*sqr1           ! ~(x/a)**2 for x<<b
END

!***********************************************************************
SUBROUTINE init_values (rho,px,py,pz,e,d,Bx,By,Bz)
  USE params
  USE stagger
  USE forcing
  implicit none
  real, dimension(mx,my,mz):: rho,px,py,pz,e,d,Bx,By,Bz,temp,scr,scr1
  integer ix,iy,iz,ir
  real h,rho_save,cdx,fy,fr,vrot,T0,Tq,rho_min,smax
  real r,q,s,xz,x,y,z,xi,yi,zi,w,fxy,xy,exp1,pr,sqr1,dv,rm,rm1

  character(len=70):: id="$Id"

  if (id.ne.' ') print *,id ; id=' '

!-----------------------------------------------------------------------
!  Unperturbed density distribution, rotation
!-----------------------------------------------------------------------
  T0 = ee0*(gamma-1.)
  rho_min = 1e30
  do iz=1,mz
    do iy=1,my
      do ix=1,mx
        xz = sqrt(xm(ix)**2 + zm(iz)**2)
        q  = sqrt(xm(ix)**2 + zm(iz)**2 + r3**2)
        s  = sqrt(xm(ix)**2 + zm(iz)**2 + r3**2 + ym(iy)**2)

        fr = exp1((q-r2)/r1)
!        fr = 1.

        if (q .gt. r2) then
          Tq = T0*(r0/q)**0.5
          rho(ix,iy,iz) = rho0*fr*r2**3/q**3*exp(-grav*M_sun/Tq*(1./q-1./s))
          rho_min = min(rho(ix,iy,iz),rho_min)
        else 
          smax = sqrt(q**2+ym(1)**2)
          Tq = grav*M_sun*(1./q-1./smax)/alog(rho0*fr*r2**3/q**3/(rho_min*fr))
          rho(ix,iy,iz) = rho0*fr*r2**3/q**3*exp(-grav*M_sun/Tq*(1./q-1./s))
        end if

        e(ix,iy,iz) = rho(ix,iy,iz)*Tq/(gamma-1.)
        temp(ix,iy,iz) = Tq
      end do
    end do
  end do

!  scr = xup(ddxdn(alog(e)))
  scr = alog(e)
  call ddxdn_set (scr, scr1)
  call xup_set (scr1, scr)
  do iz=1,mz
    do iy=1,my
      do ix=1,mx
        ir = ix
        if (ix.gt.mx/2+1) ir=mx-ix+2
        xz = sqrt(xm(ix)**2 + zm(iz)**2)
        q  = sqrt(xm(ix)**2 + zm(iz)**2 + r3**2)
        s  = sqrt(xm(ix)**2 + zm(iz)**2 + r3**2 + ym(iy)**2)
        w = grav*M_sun/s**3 - temp(ix,iy,iz)*scr(ir,iy,iz)/max(xz,xm(2)-xm(1))
        w = sqrt(max(w,0.))
        w = w - omega
        w = max(w,0.0)
        px(ix,iy,iz) =  w*zm(iz)
        pz(ix,iy,iz) = -w*xm(ix)
      end do
    end do
  end do

!-----------------------------------------------------------------------
!  Change from centered velocity to staggered momenta
!-----------------------------------------------------------------------
  scr1 = alog(rho)

  call xdn_set (scr1, scr)
  call xdn_set (px, temp)
  px = exp(scr)*temp

  call zdn_set (scr1, scr)
  call zdn_set (pz, temp)
  pz = exp(scr)*temp

  py = 0.

END
