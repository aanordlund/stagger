! $Id: abc_init.f90,v 1.4 2006/10/21 21:26:59 aake Exp $
!-----------------------------------------------------------------------
SUBROUTINE init_values (r,px,py,pz,e,d,Bx,By,Bz)
  USE params
  USE variables, ONLY: dpxdt,dpydt,dpzdt
  USE arrays
  USE forcing
  implicit none

  real, dimension(mx,my,mz):: r,px,py,pz,e,d,Bx,By,Bz
  logical do_init, do_test
  character(len=32):: test
  real r0,e0,d0,b0,b1,gam1,mrms,average,ti_turn
  real rbx,rby,rbz,ran1s
  integer iz,iy,ix,jseed
  namelist /init/do_init,do_test,test,r0,e0,d0,b0,b1,ti_turn

!-----------------------------------------------------------------------
!  Initial value parameters
!-----------------------------------------------------------------------
  r0 = 1.
  b0 = 1e-6
  b1 = 0.
  d0 = 1.
  e0 = 0.
  do_init = .true.
  do_test = .false.
  ti_turn = t_turn
  test = 'shear'

  if (master) write (*,*) hl
  read(stdin,init)
  if (master) write (*,init)

  gam1=gamma*(gamma-1.)
  if (gamma .eq. 1.) gam1=1.
  if (e0 .eq. 0.) e0 = r0*csound**2/gam1

  !$omp parallel private(iz)
    do iz=izs,ize
      r(:,:,iz) = r0
      e(:,:,iz) = e0
      px(:,:,iz) = 0.
      py(:,:,iz) = 0.
      pz(:,:,iz) = 0.
      if (do_pscalar) d(:,:,iz)=d0
    end do
  !$omp end parallel

  if (do_test) then
    call test_values (test,r,px,py,pz,e,d,Bx,By,Bz)
    return
  end if

  if (.not. do_init) return

!-----------------------------------------------------------------------
!  Initial linear acceleration
!-----------------------------------------------------------------------
  !$omp parallel private(ix,iy,iz,jseed,rbx,rby,rbz)
    call forceit (r,px,py,pz,r,r,r,dpxdt,dpydt,dpzdt)
    do iz=izs,ize
      jseed = (zm(iz)-zmin)/dz + 1.5
      jseed = -jseed
      rbz = ran1s(jseed)
      do iy=1,my
        rby = ran1s(jseed)
        do ix=1,mx
          rbx = ran1s(jseed)
          px(ix,iy,iz) = ti_turn*dpxdt(ix,iy,iz)
          py(ix,iy,iz) = ti_turn*dpydt(ix,iy,iz)
          pz(ix,iy,iz) = ti_turn*dpzdt(ix,iy,iz)
          if (do_mhd) then
	    bx(ix,iy,iz) = b0*(rby+rbz) + b1*px(ix,iy,iz)
	    by(ix,iy,iz) = b0*(rbz+rbx) + b1*py(ix,iy,iz)
	    bz(ix,iy,iz) = b0*(rbx+rby) + b1*pz(ix,iy,iz)
          end if
        end do
      end do
    end do
  !$omp end parallel
END
