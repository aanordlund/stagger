! $Id: wave_tests.f90,v 1.12 2010/02/04 23:52:48 aake Exp $
!-----------------------------------------------------------------------
SUBROUTINE wave_tests (testname,r,px,py,pz,e,d,Bx,By,Bz)
!
!  To keep the collection of tests as simple and compact as possible
!  we put them in category files.  Some extent of reuse of variable names
!  and name lists for input is thus likely.
!-----------------------------------------------------------------------
  USE params
  USE forcing, only: conservative
  implicit none
  real :: sy1,gy
!
  real, dimension(mx,my,mz):: r,px,py,pz,e,d,Bx,By,Bz,scr,lnr
  character(len=16):: testname
!
  real:: gam1,average,ax,ay,az
  integer:: i,ix,iy,iz,k,k0
!
  real:: ampl,r0,b0,e0,p0,f,npoly
  namelist /test/ampl,k,k0,r0,b0,npoly
!
  character(len=mid), save:: id='$Id'

!-----------------------------------------------------------------------
  gy = 0.
  call print_id(id)

!-----------------------------------------------------------------------
!  Initial value parameters
!-----------------------------------------------------------------------
  k=1; k0=0; ampl=0.01
  r0=1.; b0=1.; e0=1.; p0=1.; npoly=1.
  sy1=ymdn(ub)-ymdn(lb)
  print *,'wave_tests:sy1=',sy1
  print *,'testname=',testname

  rewind (stdin)
  select case (testname)
!-----------------------------------------------------------------------

!  MHD waves
!-----------------------------------------------------------------------
  case ('x_alfven')
    read (stdin,test); if (master) write (*,test)
    if (k0 == 0) k0=k
    bx = 1.; py = 0.; by = 0.
    do i=k0,k
    do ix=1,mx
      py(ix,:,:) = py(ix,:,:) + ampl*cos((2.*pi*i*xm(ix)/sx))
      by(ix,:,:) = by(ix,:,:) - ampl*cos((2.*pi*i*xm(ix)/sx))
    end do
    end do
!.......................................................................
  case ('y_alfven')
    read (stdin,test); if (master) write (*,test)
    by = 1.
    do iy=1,my
      px(:,iy,:) =  ampl*cos((2.*pi*k*ym(iy)/sy))
      bx(:,iy,:) = -ampl*cos((2.*pi*k*ym(iy)/sy))
    end do
!.......................................................................
  case ('xy_alfven')
    read (stdin,test); if (master) write (*,test)
    ax = 1./sqrt(2.)
    ay = ax
    bx = b0*ax
    by = b0*ay
    do iy=1,my
    do ix=1,mx
      px(ix,iy,:) =             ax*ampl*cos(2.*pi*k*(xm(ix)/sx+ym(iy)/sy))*b0
      bx(ix,iy,:) = bx(ix,iy,:)-ax*ampl*cos(2.*pi*k*(xm(ix)/sx+ym(iy)/sy))*b0
      py(ix,iy,:) =            -ax*ampl*cos(2.*pi*k*(xm(ix)/sx+ym(iy)/sy))*b0
      by(ix,iy,:) = by(ix,iy,:)+ax*ampl*cos(2.*pi*k*(xm(ix)/sx+ym(iy)/sy))*b0
    end do
    end do
!.......................................................................
  case ('xy16_alfven')
    read (stdin,test); if (master) write (*,test)
    ax= 1./sqrt(1.+6.**2)
    ay= 6./sqrt(1.+6.**2)
    bx = ax*b0
    by = ay*b0
    do iy=1,my
    do ix=1,mx
      px(ix,iy,:) =            +ay*ampl*cos(2.*pi*k*(xm(ix)/sx+6.*ym(iy)/sy))*b0
      bx(ix,iy,:) = bx(ix,iy,:)-ay*ampl*cos(2.*pi*k*(xm(ix)/sx+6.*ym(iy)/sy))*b0
      py(ix,iy,:) =            -ax*ampl*cos(2.*pi*k*(xm(ix)/sx+6.*ym(iy)/sy))*b0
      by(ix,iy,:) = by(ix,iy,:)+ax*ampl*cos(2.*pi*k*(xm(ix)/sx+6.*ym(iy)/sy))*b0
    end do
    end do
!.......................................................................
  case ('z_alfven')
    read (stdin,test); if (master) write (*,test)
    bz = 1.
    do iz=1,mz
      py(:,:,iz) = ampl*cos((2.*pi*k*zm(iz)/sz))
      by(:,:,iz) = ampl*cos((2.*pi*k*zm(iz)/sz))
    end do
!.......................................................................
  case ('x_alfven_stand')
    read (stdin,test); if (master) write (*,test)
    bx = 1.
    do ix=1,mx
      by(ix,:,:) = ampl*cos((2.*pi*k*xm(ix)/sx))
    end do

!-----------------------------------------------------------------------
!  HD waves
!-----------------------------------------------------------------------
  case ('x_sound')
    read (stdin,test); if (master) write (*,test)
    gam1 = gamma-1.
    if (gamma==1.) gam1 = 1.
    csound = 1.
    do ix=1,mx
      px(ix,:,:) = ampl*csound*r(ix,:,:)*sin((4.*pi*k*xmdn(ix)/sx))
      r (ix,:,:) = r(ix,:,:)*exp(ampl*sin((4.*pi*k*xm(ix)/sx)))
    end do
    e = e(1,1,1)*(r/r(1,1,1))**gamma
    print *,mpi_rank,pi,sx
    print *,mpi_rank,xm(1:4)
    print *,mpi_rank,xmdn(1:4)
    print *,mpi_rank,r (1:4,1,1)
    print *,mpi_rank,px(1:4,1,1)
!.......................................................................
  case ('y_sound')
    read (stdin,test); if (master) write (*,test)
    gam1 = gamma-1.
    if (gamma==1.) gam1 = 1.
    csound = 1.
    do iy=1,my
      py(:,iy,:) = ampl*csound*r(:,iy,:)*sin((4.*pi*k*ymdn(iy)/sy1))
      r (:,iy,:) = r(:,iy,:)*exp(-ampl*sin((4.*pi*k*ym(iy)/sy1)))
    end do
    e = e(1,1,1)*(r/r(1,1,1))**gamma
    print *,mpi_rank,r (1,1:4,1)
    print *,mpi_rank,py(1,1:4,1)
!.......................................................................
  case ('z_sound')
    read (stdin,test); if (master) write (*,test)
    gam1 = gamma-1.
    if (gamma==1.) gam1 = 1.
    csound = 1.
    do iz=1,mz
      pz(:,:,iz) = ampl*csound*r(:,:,iz)*sin((4.*pi*k*zmdn(iz)/sz))
      r (:,:,iz) = r(:,:,iz)*exp(ampl*sin((4.*pi*k*zm(iz)/sz)))
    end do
    e = e(1,1,1)*(r/r(1,1,1))**gamma
    print *,mpi_rank,r (1,1,1:4)
    print *,mpi_rank,pz(1,1,1:4)
!.......................................................................
  case ('xy_sound')
    read (stdin,test); if (master) write (*,test)
    gam1 = gamma-1.
    f = sqrt(0.5)
    if (gamma==1.) gam1 = 1.
    csound = 1.
    do ix=1,mx
    do iy=1,my
      px(ix,iy,:) = f*ampl*csound*r(ix,iy,1)*sin((4.*pi*k*(xmdn(ix)/sx+ymdn(iy)/sy1)))
      py(ix,iy,:) = f*ampl*csound*r(ix,iy,1)*sin((4.*pi*k*(xmdn(ix)/sx+ymdn(iy)/sy1)))
      r (ix,iy,:) = r(ix,iy,1)*exp(ampl*sin((4.*pi*k*(xm(ix)/sx+ym(iy)/sy1))))
    end do
    end do
    e = e(1,1,1)*(r/r(1,1,1))**gamma
    print *,mpi_rank,r (1,1:4,1)
    print *,mpi_rank,py(1,1:4,1)
!.......................................................................
  case ('yz_sound')
    read (stdin,test); if (master) write (*,test)
    gam1 = gamma-1.
    if (gamma==1.) gam1 = 1.
    f = sqrt(0.5)
    csound = 1.
    do iz=1,mz
    do iy=1,my
!   do iy=my/2,my
      py(1,iy,iz) = f*ampl*csound*r(1,iy,iz)*sin((4.*pi*k*(ymdn(iy)/sy1+zmdn(iz)/sz)))
      pz(1,iy,iz) = f*ampl*csound*r(1,iy,iz)*sin((4.*pi*k*(ymdn(iy)/sy1+zmdn(iz)/sz)))
      r (1,iy,iz) = r(1,iy,iz)*exp(-ampl*sin((4.*pi*k*(ym(iy)/sy1+zm(iz)/sz))))
    end do
    end do
    e = e(1,1,1)*(r/r(1,1,1))**gamma
    print *,mpi_rank,r (1,1:4,1)
    print *,mpi_rank,py(1,1:4,1)
!.......................................................................
  case ('yz_piston')
    read (stdin,test); if (master) write (*,test)
    gam1 = gamma-1.
    if (gamma==1.) gam1 = 1.
    r  = r(1,1,1)
    px = 0.
    py = 0.
    pz = 0.
    e = e(1,1,1)
    print *,mpi_rank,r (1,1:4,1)
    print *,mpi_rank,py(1,1:4,1)
!.......................................................................
  case ('xyz_conv')
    read (stdin,test); if (master) write (*,test)
    gam1 = gamma-1.
    if (gamma==1.) gam1 = 1.
    csound = 1.
    do iz=1,mz
    do iy=1,my
    do ix=1,mx
      r(ix,iy,iz) = 1.
      e(ix,iy,iz) = 1.
      px(ix,iy,iz) = ampl*sin((4.*pi*k*xmdn(ix)/sx))
      py(ix,iy,iz) = ampl*sin((4.*pi*k*xmdn(iy)/sy1))
      pz(ix,iy,iz) = ampl*sin((4.*pi*k*zmdn(iz)/sz))
    end do
    end do
    end do

!-----------------------------------------------------------------------
!  Hydrostatic equilibrium with constant gravity
!-----------------------------------------------------------------------
  case ('hydrostatic')
    gy = 1.
    read (stdin,test); if (master) write (*,test)
    gam1 = gamma-1.
    if (gamma==1.) gam1 = 1.
    r0=r(1,1,1)
    e0=e(1,1,1)
    p0=gam1*e0
    if(npoly .eq. 1.0) then
      do iy=1,my
        r (:,iy,:) = r0*exp(r0*gy*ym(iy)/p0)
        e (:,iy,:) = e0*exp(r0*gy*ym(iy)/p0)
      end do
    else
      do iy=1,my
        r (:,iy,:) = r0*(1.0+(1.0-1.0/npoly)*r0*gy*ym(iy)/p0)**(1.0/(npoly-1.0))
        e (:,iy,:) = e0*(1.0+(1.0-1.0/npoly)*r0*gy*ym(iy)/p0)**(npoly/(npoly-1.0))
      end do
    end if
    print *,mpi_rank,r (1,1:4,1)
    print *,mpi_rank,py(1,1:4,1)

!-----------------------------------------------------------------------
!  Checker board patters; these maximize the effects of diffusion
!-----------------------------------------------------------------------
  case ('r-checker')
    r0 = r(1,1,1)
    read (stdin,test); if (master) write (*,test)
    do iz=1,mz
      do iy=1,my
        do ix=1,mx
          r(ix,iy,iz)=r(ix,iy,iz)*exp(ampl*(2* &
	    mod(ifix((xm(ix)-xmin)/dx+(ym(iy)-ymin)/dy+(zm(iz)-zmin)/dz+0.5),2)-1))
          e(ix,iy,iz)=e(ix,iy,iz)*(r(ix,iy,iz)/r0)**gamma
        end do
      end do
    end do
!.......................................................................
  case ('e-checker')
    read (stdin,test); if (master) write (*,test)
    do iz=1,mz
      do iy=1,my
        do ix=1,mx
          e(ix,iy,iz)=e(ix,iy,iz)*exp(ampl*gamma*(2* &
	    mod(ifix((xm(ix)-xmin)/dx+(ym(iy)-ymin)/dy+(zm(iz)-zmin)/dz+0.5),2)-1))
          r(ix,iy,iz)=r(ix,iy,iz)*exp(ampl*(2* &
	    mod(ifix((xm(ix)-xmin)/dx+(ym(iy)-ymin)/dy+(zm(iz)-zmin)/dz+0.5),2)-1))
        end do
      end do
    end do
!.......................................................................
  case ('ee-checker')
    r0 = r(1,1,1)
    read (stdin,test); if (master) write (*,test)
    do iz=1,mz
      do iy=1,my
        do ix=1,mx
          r(ix,iy,iz)=r(ix,iy,iz)*exp(-ampl*(2* &
	    mod(ifix((xm(ix)-xmin)/dx+(ym(iy)-ymin)/dy+(zm(iz)-zmin)/dz+0.5),2)-1))
        end do
      end do
    end do
!.......................................................................
  case ('px-checker')
    read (stdin,test); if (master) write (*,test)
    do iz=1,mz
      do iy=1,my
        do ix=1,mx
          px(ix,iy,iz)=ampl*(2* &
	    mod(ifix((xm(ix)-xmin)/dx+(ym(iy)-ymin)/dy+(zm(iz)-zmin)/dz+0.5),2)-1)
        end do
      end do
    end do
!.......................................................................
  case ('pxy-checker')
    read (stdin,test); if (master) write (*,test)
    do iz=1,mz
      do iy=1,my
        do ix=1,mx
          px(ix,iy,iz)=+ampl*(2* &
	    mod(ifix((xm(ix)-xmin)/dx+(ym(iy)-ymin)/dy+(zm(iz)-zmin)/dz+0.5),2)-1)
          py(ix,iy,iz)=-ampl*(2* &
	    mod(ifix((xm(ix)-xmin)/dx+(ym(iy)-ymin)/dy+(zm(iz)-zmin)/dz+0.5),2)-1)
        end do
      end do
    end do
!.......................................................................
  case ('pxy2-checker')
    read (stdin,test); if (master) write (*,test)
    do iz=1,mz
      do iy=1,my
        do ix=1,mx
          px(ix,iy,iz)=+ampl*(2* &
	    mod(ifix((ym(iy)-ymin)/dy+0.5),2)-1)
          py(ix,iy,iz)=-ampl*(2* &
	    mod(ifix((xm(ix)-xmin)/dx+0.5),2)-1)
        end do
      end do
    end do

  end select
END
