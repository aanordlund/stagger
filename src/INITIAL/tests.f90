! $Id: tests.f90,v 1.12 2012/08/08 14:59:45 aake Exp $
!-----------------------------------------------------------------------
SUBROUTINE test_values (test,r,px,py,pz,e,d,Bx,By,Bz)
!
!  To keep the collection of tests as simple and compact as possible
!  we put them all in one file (although there may be be more than one
!  collection).  Some extent of reuse of variable names and name lists
!  for input is thus beneficial.
!-----------------------------------------------------------------------
  USE params
  USE forcing, only: conservative
  implicit none
  real, dimension(mx,my,mz):: r,px,py,pz,e,d,Bx,By,Bz,scr,lnr
  character(len=16):: test
  real r0,e0,gam1
  integer iz
  character(len=mid), save:: id='$Id: tests.f90,v 1.12 2012/08/08 14:59:45 aake Exp $'
!-----------------------------------------------------------------------
  call print_id(id)

!-----------------------------------------------------------------------
!  Default for all tests in this file, unless changed below
!-----------------------------------------------------------------------
  r0=1.; gam1 = gamma-1.; if (gamma.eq.1.) gam1 = 1.; e0 = r0/(gamma*gam1)
  !$omp parallel do private(iz)
  do iz=1,mz
    r(:,:,iz) = r0
    e(:,:,iz) = e0
    px(:,:,iz) = 0.
    py(:,:,iz) = 0.
    pz(:,:,iz) = 0.
    if (do_mhd) then
      Bx(:,:,iz) = 0.
      By(:,:,iz) = 0.
      Bz(:,:,iz) = 0.
    end if
  end do

!-----------------------------------------------------------------------
!  Find the test
!-----------------------------------------------------------------------
  call wave_tests   (test,r,px,py,pz,e,d,Bx,By,Bz)
  call shock_tests  (test,r,px,py,pz,e,d,Bx,By,Bz)
  call stone_tests  (test,r,px,py,pz,e,d,Bx,By,Bz)
! call shear_tests  (test,r,px,py,pz,e,d,Bx,By,Bz)
! call advect_tests (test,r,px,py,pz,e,d,Bx,By,Bz)

!-----------------------------------------------------------------------
!  Adjust for non-conservative calls
!-----------------------------------------------------------------------
  print*,conservative
  if (.not.conservative) then
    !$omp parallel do private(iz)
    do iz=1,mz
      e(:,:,iz) = e(:,:,iz)/r(:,:,iz)
      r(:,:,iz) = alog(r(:,:,iz))
    end do
  end if
END
