! $Id: stellaratm-init.f90,v 1.3 2010/02/05 00:00:34 aake Exp $
SUBROUTINE init_values (r,px,py,pz,e,d,Bx,By,Bz)

  USE params
  implicit none
  real, dimension(mx,my,mz):: r,px,py,pz,e,d,Bx,By,Bz
!hpf$ distribute(*,*,block):: r,px,py,pz,e,d,Bx,By,Bz
  logical do_init, do_test
  character(len=32):: test
  namelist /init/do_init, do_test, test

  do_init=.false.
  do_test=.false.
  test=''

  rewind (stdin); read (stdin,init)
  if (master) write (*,init)

  if (do_test) then
    call test_values (test,r,px,py,pz,e,d,Bx,By,Bz)
    return
  end if

  if (.not. do_init) return

  print *,'For now, stellaratm-init assumes the initial state is read from a file'
  stop
END
