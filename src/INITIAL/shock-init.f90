! $Id: shock-init.f90,v 1.5 2005/01/13 13:24:05 aake Exp $
!-----------------------------------------------------------------------
SUBROUTINE test_values (r,px,py,pz,e,d,Bx,By,Bz)
  USE params
  USE stagger
  implicit none

  real, dimension(mx,my,mz):: r,px,py,pz,e,d,Bx,By,Bz
  character(len=16):: type
  real:: gam1,average,r1,r2,hm1,hm2,v1,v2,g1,g2,e0,B1,B2, fmaxval
  integer:: i,ix,iy,iz,smoothfac
  namelist /shock/type,r1,r2,e0,hm1,hm2,g1,g2,B1,B2,smoothfac

!-----------------------------------------------------------------------
!  Initial value parameters
!-----------------------------------------------------------------------
  type='y_shock_rho'
  r1 = 1.0
  r2 = 1.0
  e0 = 0.1
  hm1 = 0.1
  hm2 = 0.1
  g1 = 1.0
  g2 = 1.0
  B1 = 1.0
  B2 = 1.0
  smoothfac = 2
  
  read  (*,shock)
  write (*,shock)

  r  = 1.
  px = 0.
  py = 0.
  pz = 0.
  e  = 1.

  if (do_mhd) then
    bx = 0.
    by = 0.
    bz = 0.
  end if

  select case (type)
! Set shock according to input parameters.
  case ('y_shock_set_rel')
    gam1 = gamma-1.
    v1 = sqrt(1.-1./g1**2)
    v2 = sqrt(1.-1./g2**2)
    do iy=1,my/2
      r(:,iy,:)  = r1
      pz(:,iy,:) = hm1
      py(:,iy,:) = r1*(hm1+1.)*g1*v1
    end do
    do iy=my/2+1,my
      r(:,iy,:)  = r2
      pz(:,iy,:) = hm2
      py(:,iy,:) = r2*hm2*g2*v2
    end do
    !a bit of smoothing to make things continous
    do iy=1,smoothfac
      r  = exp(smooth3(alog(r)))         ! density
      r(:,1:3,:) = r1                    ! No wrap-around in boundaries
      r(:,my-2:my,:)=r2
      pz  = exp(smooth3(alog(pz))) ! Enthalpy minus one
      pz(:,1:3,:) =hm1
      pz(:,my-2:my,:)=hm2
    end do
    ! We want to have flat momentum profile
    !py = r1*(1.+hm1)*g1*v1
    ! fix e according to smoothed r and h values
    ! Notice that v*gamma = py/(r*(1.+pz)),
    !               gamma = Sqrt(1+vW^2),
    ! and               e = T00 - D = D((h-1)W + vW/(W+1)) - P
    px = Sqrt(1 + (py/(r*(1.+pz)))**2)
    e  = r*(pz*px + py/(r*(1.+pz)*(px+1))) - gam1/gamma*pz*r/px
    px = 0.
    pz = 0.
! Approximately stationary shock. See A401 notes chap 12(?)
  case ('y_shock_stat_rel')
    gam1 = gamma-1.
    if (gamma==1.) gam1 = 1.
    hm1 = 1. + e0
    v1 = sqrt(1.-1./g1**2)
    v2 = gam1/gamma
    g2 = 1./sqrt(1.-v2**2)
    hm2 = hm1*g1/g2
    r2 = r1*v1/v2
    do iy=1,my/2
      r(:,iy,:)  = r1
      e(:,iy,:)  = r1*(hm1*g1 - 1.) - gam1/gamma*e0*r1/g1
      px(:,iy,:) = g1
      py(:,iy,:) = r1*hm1*g1*v1
      pz(:,iy,:) = hm1
    end do
    do iy=my/2+1,my
      r(:,iy,:)  = r2
      e(:,iy,:)  = r2*(hm2*g2 - 1.) - gam1/gamma*(hm2-1.)*r2/g2
      px(:,iy,:) = g2
      pz(:,iy,:) = hm2
    ! Mak:,iy,e sure pz is constant over the shock as it should be!
    ! See idl code and chap 12 from A401
      py(:,iy,:) = r1*hm1*g1*v1
    end do
    ! a bit of smoothing of gamma to make things continous
    do iy=1,smoothfac
      px = exp(smooth3(alog(px)))         ! gamma factor
      pz = exp(smooth3(alog(pz-1.))) + 1. ! enthalpy, h
    end do
    r  = py/(pz*sqrt(px**2-1.))
    e  = r*(pz*px-1.) - gam1/gamma*(pz-1.)*r/px
    px = 0.
    pz = 0.
! Exact stationary shock jump conditions.
! Calculated using mathematica.
  case ('y_shock_stat_set_rel')
    gam1 = gamma-1.
    ! case 1
    g1 = 3.
    hm1 = 0.03
    v1 = sqrt(1.-1./g1**2)
    r1 = 1.0
    g2 = 1.09069
    v2 = sqrt(1.-1./g2**2)
    hm2 = 1.83307
    r2 = 2.36159
    ! case 2
    g1 = 5.
    hm1 = 0.01
    v1 = sqrt(1.-1./g1**2)
    r1 = 1.0
    g2 = 1.03469
    v2 = sqrt(1.-1./g2**2)
    hm2 = 3.8807
    r2 = 3.81598
    do iy=1,my/2
      r(:,iy,:) = r1
      pz(:,iy,:) = hm1
    end do
    do iy=my/2+1,my
      r(:,iy,:) = r2
      pz(:,iy,:) = hm2
    end do
    ! a bit of smoothing to make things continous
    do iy=1,smoothfac
      r  = exp(smooth3(alog(r)))         ! density
      pz = exp(smooth3(alog(pz-1.))) + 1. ! enthalpy, h
    end do
    ! We want to have flat momentum profile
    py = r1*hm1*g1*v1
    ! fix e according to smoothed r and h values
    ! Notice gamma = Sqrt(1 + (py/(r*pz))**2)
    e  = r*(pz*Sqrt(1 + (py/(r*pz))**2)-1.) - gam1/gamma*(pz-1.)*r/Sqrt(1 + (py/(r*pz))**2)
    px = 0.
    pz = 0.
! Shock only in rho. Should give rise
! to a rarefaction wave, Contact discontinuity
! and shock front. (According to Shibata-san) (?)
  case ('y_shock_rho_rel')
    gam1 = gamma-1.
    do iy=1,my/2
      r(:,iy,:) = r1
    end do
    do iy=my/2+1,my
      r(:,iy,:) = r2
    end do
    ! a bit of smoothing to make things continous
    ! only necesary for high (>512 pt) resolutions
    !do iy=1,1
    !   r  = exp(smooth3(alog(r)))         ! density
    !end do
    ! constant energy density and zero momenta to start with.
    e  = e0
    px = 0.
    py = 0.
    pz = 0.
! Exact magnetic stationary shock jump conditions.
! Taken from Komissarov 1999 & 2002 papers.
!
! Shock tube 2:
  case ('komissarov_shock2')
    gam1 = gamma-1.
    ! case 1
    g1 = 1.1
    hm1 = 1.25
    v1 = sqrt(1.-1./g1**2)
    r1 = 1.0
    B1 = 1.0
    g2 = 1.1
    v2 = -sqrt(1.-1./g2**2)
    hm2 = 1.25
    r2 = 1.0
    B2 = -1.0
    do iy=1,my/2
      r(:,iy,:) = r1
      pz(:,iy,:) = hm1
      Bz(:,iy,:) = B1
    end do
    do iy=my/2+1,my
      r(:,iy,:) = r2
      pz(:,iy,:) = hm2
      Bz(:,iy,:) = B2
    end do
    ! a bit of smoothing to make things continous
    do iy=1,smoothfac
      r  = exp(smooth3(alog(r)))          ! density
      pz = exp(smooth3(alog(pz-1.))) + 1. ! enthalpy, h
      Bz = smooth3(Bz)                    ! Magnetic field
    end do
    ! We want to have as flat as possible a momentum profile
    py = r1*hm1*g1*v1
    ! fix e according to smoothed r, h and B values
    ! Notice gamma = Sqrt(1 + (py/(r*pz))**2)
    e  = r*(pz*Sqrt(1. + (py/(r*pz))**2) - 1.) - &
         gam1/gamma*(pz - 1.)*r/Sqrt(1. + (py/(r*pz))**2)
    px = 0.
    pz = 0.
    Bx = 0.
    By = 0.
  case ('pairplasma')
    gam1 = gamma-1.
    ! case 1
    v1 = sqrt(1.-1./g1**2)
    v2 = sqrt(1.-1./g2**2)
    do iy=1,my/2
      r(:,iy,:) = r1
      pz(:,iy,:) = hm1
      If (do_mhd) Bz(:,iy,:) = B1
    end do
    do iy=my/2+1,my
      r(:,iy,:) = r2
      pz(:,iy,:) = hm2
      If (do_mhd) Bz(:,iy,:) = B2
    end do
    ! a bit of smoothing to make things continous
    do iy=1,smoothfac
      r  = exp(smooth3(alog(r)))          ! density
      pz = exp(smooth3(alog(pz-1.))) + 1. ! enthalpy, h
      If (do_mhd) Bz = smooth3(Bz)                    ! Magnetic field
    end do
    do iy=1,smoothfac+1
      r(:,iy,:)  = r(:,smoothfac+2,:)   ! density
      pz(:,iy,:) = pz(:,smoothfac+2,:)  ! enthalpy, h
      If (do_mhd) Bz(:,iy,:) = Bz(:,smoothfac+2,:)  ! Magnetic field
    end do
    do iy=my-smoothfac,my
      r(:,iy,:)  = r(:,my-smoothfac-1,:)   ! density
      pz(:,iy,:) = pz(:,my-smoothfac-1,:)  ! enthalpy, h
      If (do_mhd) Bz(:,iy,:) = Bz(:,my-smoothfac-1,:)  ! Magnetic field
    end do
    ! We want to have as flat as possible a momentum profile
    do iy=1,my/2
      py(:,iy,:) = r(:,iy,:)*pz(:,iy,:)*g1*v1
      e(:,iy,:)= r(:,iy,:)*(pz(:,iy,:)*g1-1.)-gam1/gamma*(pz(:,iy,:)-1.)*r(:,iy,:)/g1
    end do
    do iy=my/2+1,my
      py(:,iy,:) = r(:,iy,:)*pz(:,iy,:)*g2*v2
      e(:,iy,:)= r(:,iy,:)*(pz(:,iy,:)*g2 - 1.)-gam1/gamma*(pz(:,iy,:)-1.)*r(:,iy,:)/g2
    end do
    py = smooth3(py)                      ! Momentum T0y
    e  = smooth3(e)
    do iy=1,smoothfac+1
      py(:,iy,:) = py(:,smoothfac+2,:)   ! density
      e(:,iy,:)  = e(:,smoothfac+2,:)    ! enthalpy, h
    end do
    do iy=my-smoothfac,my
      py(:,iy,:) = py(:,my-smoothfac-1,:)   ! density
      e(:,iy,:)  = e(:,my-smoothfac-1,:)    ! enthalpy, h
    end do
    ! fix e according to smoothed r and h values
    ! Notice gamma = Sqrt(1 + (py/(r*pz))**2)
!    e  = r*(pz*Sqrt(1. + (py/(r*pz))**2) - 1.) - &
!         gam1/gamma*(pz - 1.)*r/Sqrt(1. + (py/(r*pz))**2)
    px = 0.
    pz = 0.
    If (do_mhd) Bx = 0.
    If (do_mhd) By = 0.
  case ('pairplasma2')
    gam1 = gamma-1.
    ! case 1
    v1 = sqrt(1.-1./g1**2)
    v2 = sqrt(1.-1./g2**2)
    do iy=1,my/2
      r(:,iy,:) = r1*g1
      py(:,iy,:) = r1*hm1*g1**2*v1
      e(:,iy,:)= r1*g1*(hm1*g1-1.)-gam1/gamma*(hm1-1.)*r1
      If (do_mhd) Bz(:,iy,:) = B1
    end do
    do iy=my/2+1,my
      r(:,iy,:) = r2*g2
      py(:,iy,:) = r2*hm2*g2**2*v2
      e(:,iy,:)= r2*g2*(hm2*g2-1.)-gam1/gamma*(hm2-1.)*r2
      If (do_mhd) Bz(:,iy,:) = B2
    end do
    do iy=1,smoothfac
      r  = exp(smooth3(alog(r)))          ! density
      e  = exp(smooth3(alog(e)))          ! energy density e
      If (do_mhd) Bz = smooth3(Bz)                    ! Magnetic field
    end do
    do iy=1,smoothfac+1
      r(:,iy,:)  = r(:,smoothfac+2,:)    ! density
      e(:,iy,:)  = e(:,smoothfac+2,:)    ! enthalpy, h
      If (do_mhd) Bz(:,iy,:)=Bz(:,smoothfac+2,:) ! Magnetic field
    end do
    do iy=my-smoothfac,my
      r(:,iy,:)  = r(:,my-smoothfac-1,:)    ! density
      e(:,iy,:)  = e(:,my-smoothfac-1,:)    ! enthalpy, h
      If (do_mhd) Bz(:,iy,:)=Bz(:,my-smoothfac-1,:) ! Magnetic field
    end do
    ! We want to have as flat as possible a momentum profile
    ! T0y = r*h*W*vy
    px = 0.
    pz = 0.
    If (do_mhd) Bx = 0.
    If (do_mhd) By = 0.
    Print *, minval(py),maxval(py), "py"
  case ('st2')
    gam1 = gamma-1.
    ! case 1
    v1 = 0.
    v2 = 0.
    g1 = 0.
    g2 = 0.
    hm1 = 2500.
    r1 = 1.
    r2 = 1.
    r = r1
    py = 0.
    pz = 0.
    call SplitValue(px,hm1,hm2)
    do iy=1,smoothfac
      px  = exp(smooth3(alog(px)))          ! enthalpy h - 1
    end do
    e= r*px-gam1/gamma*px*r
    px = 0.
  case ('st3')
    gam1 = gamma-1.
    v1 = sqrt(1.-1./g1**2)
    v2 = sqrt(1.-1./g2**2)
    py = 0.
    pz = 0.
    call SplitValue(px,r1,r2)         ! density r
    call SplitValue(py,hm1,hm2)       ! enthalpy h-1
    call SplitValue(pz,g1,g2)         ! Lorentz boost W
    If (do_mhd) call SplitValue(Bz,B1,B2) ! Magnetic field
    r = px*pz                         ! D = r*W
    do iy=1,smoothfac
      r  = exp(smooth3(alog(r)))      ! density r
      py = exp(smooth3(alog(py)))     ! enthalpy h-1
    end do
    e = r*((py+1)*pz-1.) - gam1/gamma*py*r/pz ! e = D(hW - 1) - A(h-1)D/W
    v1 = v1*g1
    v2 = v2*g2
    call SplitValue(px,v1,v2)         ! four velocity
    pz = py
    py = r*(pz+1.)*px                 ! py = D h vW
    py = ydn(py)                      ! Remember we are on a staggered mesh
    px = 0.
    pz = 0.
    call clearedge(r,smoothfac)
    call clearedge(py,smoothfac)
    call clearedge(e,smoothfac)
  case ('st4')
    gam1 = gamma-1.
    v1 = sqrt(1.-1./g1**2)
    v2 = -sqrt(1.-1./g2**2)           ! This setup is good for colliding shocks
    py = 0.
    pz = 0.
    call SplitValue(px,r1,r2)         ! density r
    call SplitValue(py,hm1,hm2)       ! enthalpy h-1
    call SplitValue(pz,g1,g2)         ! Lorentz boost W
    If (do_mhd) call SplitValue(Bz,B1,B2) ! Magnetic field
    r = px*pz                         ! D = r*W
    do iy=1,smoothfac
      r  = exp(smooth3(alog(r)))      ! density r
      py = exp(smooth3(alog(py)))     ! enthalpy h-1
    end do
    e = r*((py+1)*pz-1.) - gam1/gamma*py*r/pz ! e = D(hW - 1) - A(h-1)D/W
    v1 = v1*g1
    v2 = v2*g2
    call SplitValue(px,v1,v2)         ! four velocity
    pz = py
    py = r*(pz+1.)*px                 ! py = D h vW
    py = ydn(py)                      ! Remember we are on a staggered mesh
    px = 0.
    pz = 0.
    call clearedge(r,smoothfac)
    call clearedge(py,smoothfac)
    call clearedge(e,smoothfac)
  case ('pphd')
    open (3,file='pphd.dat',form='unformatted',status='old')
    read (3) r,py,e
    px = 0.
    py = ydn(py)
    call ClearEdge(r,5)
    call ClearEdge(py,5)
    call ClearEdge(e,5)
    pz = 0.
    close (3)
  case ('ppmhd')
    open (3,file='ppmhd.dat',form='unformatted',status='old')
    read (3) r,py,e,bz,bx,by
    px = 0.
    py = ydn(py)
    pz = 0.
    Bx = xdn(Bx)
    By = ydn(By)
    Bz = zdn(Bz)
    call ClearEdge(r,lb-3)
    call ClearEdge(py,lb-3)
    call ClearEdge(e,lb-3)
    call ClearEdge(Bx,lb-3)
    call ClearEdge(By,lb-3)
    call ClearEdge(Bz,lb-3)
  end select
CONTAINS
  Subroutine SplitValue(f,f1,f2)
    Real, Dimension(mx,my,mz), intent(out):: f
    Real                     , intent(in) :: f1,f2
    integer                               :: iy
    do iy=1,my/2
      f(:,iy,:)= f1
    end do
    do iy=my/2+1,my
      f(:,iy,:)= f2
    end do    
  END Subroutine SplitValue
!
  Subroutine ClearEdge(f,sf)
    Real, Dimension(mx,my,mz), intent(out):: f
    integer,                   intent(in) :: sf
    integer                               :: iy
    do iy=1,sf+2
      f(:,iy,:)= f(:,sf+3,:)
    end do
    do iy=my-sf-1,my
      f(:,iy,:)= f(:,my-sf-2,:)
    end do    
  END Subroutine ClearEdge
END
