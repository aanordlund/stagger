! $Id: stone_tests.f90,v 1.5 2012/09/13 21:01:36 aake Exp $
!-----------------------------------------------------------------------
SUBROUTINE stone_tests (testname,r,px,py,pz,e,d,Bx,By,Bz)
!
!  To keep the collection of tests as simple and compact as possible
!  we put them in category files.  Some extent of reuse of variable names
!  and name lists for input is thus likely.
!-----------------------------------------------------------------------
  USE params
  USE forcing, only: conservative
  implicit none
  real, dimension(mx,my,mz):: r,px,py,pz,e,d,Bx,By,Bz,scr,lnr
  real, allocatable, dimension(:,:,:) :: Aaz
  character(len=16):: testname
  real gam1,average,ax,ay,az,x,y,z
  integer i,ix,iy,iz
  real ampl,k,r0,b0,e0,ux0,uy0,r1,ran1s
  namelist /OT/B0
  namelist /random/ampl,iseed
  namelist /test/ampl,k,r0,b0
  namelist /shock/ampl,k,ux0,uy0,r0,r1
  character(len=mid), save:: id='$Id: stone_tests.f90,v 1.5 2012/09/13 21:01:36 aake Exp $'
!-----------------------------------------------------------------------
  call print_id(id)

  select case (testname)
!-----------------------------------------------------------------------
!  Jim Stone's tests
!-----------------------------------------------------------------------
  case ('Stone-OT')
    B0 = 1./sqrt(4.*pi)
    rewind (stdin); read (stdin,OT); if (master) write(*,OT)
    gamma = 1.6666667
    allocate (Aaz(mx,my,mz))
    r = 25/(36.*pi)
    e = 5/(12.*pi)/(gamma-1.)
    do ix=1,mx
    do iy=1,my
      Aaz(ix,iy,:) = B0*(cos(4.*pi*(xmdn(ix)-xmin))/(4.*pi)+cos(2.*pi*(ymdn(iy)-ymin))/(2.*pi))
      px(ix,iy,:) = -r(ix,iy,:)*sin(2.*pi*(ym(iy)-ymin))
      py(ix,iy,:) =  r(ix,iy,:)*sin(2.*pi*(xm(ix)-xmin))
    end do
    end do
    pz = 0.
    call ddyup_set (Aaz, Bx)
    call ddxup_neg (Aaz, By)
    Bz = 0.
    deallocate (Aaz)
!.......................................................................
  case ('Stone-KH')
    gamma = 1.4
    ampl = 0.01
    rewind (stdin); read (stdin,random); write(*,random)
!$omp parallel do private(iz,iy,ix), shared(iseed)
    do iz=1,mz
    do iy=1,my
      if (abs(iz-mz/2+0.5) < mz/4) then
        do ix=1,mx
          r (ix,iy,iz) =  2.0
          px(ix,iy,iz) = -1.0 + ampl*r(ix,iy,iz)*(2.*ran1s(iseed)-1.)
          pz(ix,iy,iz) =        ampl*r(ix,iy,iz)*(2.*ran1s(iseed)-1.)
          e (ix,iy,iz) = 2.5/(gamma-1.)
        end do
      else
        do ix=1,mx
          r (ix,iy,iz) =  1.0
          px(ix,iy,iz) =  0.5 + ampl*r(ix,iy,iz)*ran1s(iseed)
          pz(ix,iy,iz) =        ampl*r(ix,iy,iz)*ran1s(iseed)
          e (ix,iy,iz) = 2.5/(gamma-1.)
        end do
      end if
    end do
    end do
!.......................................................................
  case ('Stone-implosion')
    gamma=1.4
    r0=1.
    e0=1./(gamma-1.)
    rewind (stdin); read (stdin,shock); write(*,shock)
    lb = 1
    !print*,sx,sz,xmin,zmin
    ub = mx
    do ix=1,mx
    do iz=1,mz
      x=xm(ix)-xmin-sx/2.
      z=zm(iz)-zmin-sz/2.
      if (abs(x)+abs(z) <= 0.25*sx+0.5*dx) then
        r(ix,1,iz) = 0.125*r0
        e(ix,1,iz) = 0.140*e0
      endif
    end do
    end do
    ix=0
    !r=cshift(r,dim=1,shift=ix)
    !e=cshift(e,dim=1,shift=ix)
    !r=cshift(r,dim=3,shift=ix)
    !e=cshift(e,dim=3,shift=ix)
!       r(lb,1,mz/2) = r(lb,1,mz/2+1)
!       e(lb,1,mz/2) = e(lb,1,mz/2+1)
!       call density_boundary_log (r, scr)
!       scr = e/r
!       call energy_boundary (r,scr,scr,scr,scr,e,scr,scr,scr,scr,scr)
!       call smooth3_set (r, scr)
!       r = scr
    !call smooth3_set (r, scr)
    !r = scr
    !call smooth3_set (e, scr)
    !e = scr

  end select
END
