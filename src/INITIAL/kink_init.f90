! $Id: kink_init.f90,v 1.2 2007/04/04 11:59:45 aake Exp $
!************************************************************************
SUBROUTINE init_values
  USE params
  USE forcing
  USE variables
  implicit none
  real B0, r0
  namelist /init/ B0, r0
  character(len=mid), save:: id="$Id: kink_init.f90,v 1.2 2007/04/04 11:59:45 aake Exp $"

  call print_id (id)

  B0 = 1.
  r0 = 1.
  rewind (stdin); read (stdin, init)
  if (master) write (*, init)

  Bx = 0.
  By = B0
  Bz = 0.
  px = 0.
  py = 0.
  pz = 0.
  r  = r0
  e  = r*ee_cool
END
