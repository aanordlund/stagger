! $Id: ISM.f90,v 1.12 2012/12/31 16:19:17 aake Exp $
!************************************************************************
SUBROUTINE init_values (r,px,py,pz,e,d,Bx,By,Bz)

USE params
USE units

implicit none

integer :: i,j,k,k1,k2
real, dimension(mx,my,mz) :: r,px,py,pz,e,d,Bx,By,Bz
real :: rhoav,tempav,emin,dmin,bmin,xb,rb,xc,yc,zc,x,y,z, &
        pr,rfl,mvir,bdr,average,by0
real*8 :: nr

logical :: do_init

namelist /init/ do_init, rhoav, tempav, rfl, pr, k1, k2, by0

character(len=mid) id
data id/'$Id: ISM.f90,v 1.12 2012/12/31 16:19:17 aake Exp $'/

print *,id

!-------------------------------------------------------------------------
! Parameter values for a homogeneous ISM

do_init = .true.
rhoav = 0.1
tempav = 1.0E+05 
rfl = 1.
pr = -1.
k1 = 1
k2 = 10
by0 = 1.

read (*,init)
write (*,init)

if (.not. do_init) return

emin=exp(lncV)*rhoav*1000.                ! emin*9.56E-15 erg cm^-3
dmin=rhoav*1.e-15                         ! to prevent underflow slow-down
bmin=rhoav*1.e-15                         ! to prevent underflow slow-down

!-------------------------------------------------------------------------
! Parameter values for a non-constant density profile. The dark 
! matter density profile is given by the phenomenological profile 
! of Burkert (1995, ApJ, 447, L25), determined from radial velocity 
! distributions of dwarf galaxies (also usable for calculating an external 
! graviational field).

  mvir=4.2E+08                          !virial mass (in solar masses) of object
  bdr=0.10                              !baryon-to-dark-matter ratio
  xb=(2.395E-02*mvir)**(0.4285714)      !characteristic radius (Burkert's profile)
  rb=4.5*(msun/cmu)*xb**(-0.6666667)    !central dark matter density (Burkert's profile)
 
  xc=0.35*(mx+1.)*dx                     !----------------------
  yc=0.25*(my+1.)*dy                     !position of the centre
  zc=0.70*(mz+1.)*dz                     !----------------------

! Initialization

  call randomf (r, 0.0, rfl, pr, k1, k2)
  r = exp(r)
  r = r*rhoav/average(r)
  print '(1x,a,1pe11.2)', 'Total mass (M_o) =', rhoav*sx*sy*sz*clu**3*cdu/msun

!  do k=1,mz
!    z = k*dz
!    do j=1,my
!      y = j*dy
!      do i=1,mx
!        x = i*dx
!        if ((x.eq.xc) .and. (y.eq.yc) .and. (z.eq.zc)) then
!          r(i,j,k)=r(i,j,k)+bdr*rb
!        else
!          nr = sqrt((x-xc)**2+(y-yc)**2+(z-zc)**2)/xb
!          r(i,j,k)=r(i,j,k)+bdr*rb/((1.+nr)*(1.+nr**2))
!        end if
!      end do
!    end do
!  end do

  px = 0.                               ! ignore underflow slow-down
  py = 0.                               ! ignore underflow slow-down
  pz = 0.                               ! ignore underflow slow-down

  e  = rhoav*exp(lncV)*tempav           ! add to energy; constant pressure
if (do_pscalar) d = poff*r              ! add to abundance; poff = passive scalar offset
if (do_mhd) then                        ! Magnetic field components
  Bx = bmin
  By = bmin+by0
  Bz = bmin
end if

END

!************************************************************************
SUBROUTINE randomf (r, rav, rfl, pr, k1, k2)

  USE params
  implicit none

  real, dimension(mx,my,mz):: r

  real:: rav,rfl,pr,pk,fk,x,y,z,ran1s,rms,rms1,average
  integer:: k1,k2,jx,jy,jz,i,j,k
  complex:: r1,kx,ky,kz,expikr

  r = 0.

  pk = (pr-2)/2
  rms = 0.
  do jx=-k2,k2
    kx = cmplx (0., jx*2.*pi/(mx*dx))
    do jy=-k2,k2
      ky = cmplx (0., jy*2.*pi/(my*dy))
      do jz=-k2,k2
        kz = cmplx (0., jz*2.*pi/(mz*dz))
        fk = sqrt(float(jx**2+jy**2+jz**2))
        if (fk .ge. k1 .and. fk .le. k2) then
          r1 = cexp(cmplx(0., 2.*pi*ran1s(iseed)))*fk**pk
          rms = rms + abs(r1)**2
!$omp parallel do private(i,j,k,x,y,z,expikr)
          do k=1,mz
            z = dz*(k-1)
            do j=1,my
              y = dy*(j-1)
              do i=1,mx
                x = dx*(i-1)
                expikr = cexp(kx*x+ky*y+kz*z)
                r(i,j,k) = r(i,j,k) + real(r1*expikr)
              end do
            end do
          end do
        endif
      enddo
    enddo
  enddo

  rms = sqrt(0.5*rms)
  rms1 = sqrt(average(r**2))
  print *,'rms:',rms,rms1

  r = rav + r*(rfl/rms1)

END SUBROUTINE

