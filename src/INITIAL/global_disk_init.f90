! $Id: global_disk_init.f90,v 1.31 2007/07/12 08:44:19 aake Exp $

!***********************************************************************
FUNCTION exp1(x)
  real :: x, exp1
  exp1 = exp(min(max(x,-70.),70.))
  exp1 = exp1/(1.+exp1)
END

!***********************************************************************
SUBROUTINE init_values (rho,px,py,pz,e,d,Bx,By,Bz)
  USE params
  USE forcing
  implicit none
  real, dimension(mx,my,mz):: rho,px,py,pz,e,d,Bx,By,Bz
  real, dimension(mx,my,mz):: temp,scr,scr1,scr2,phi0,phix,phiy
  integer ix,iy,iz
  real h,rho_save,cdx,fy,fr,vrot,T0,Tq,smax
  real r,q,s,x,y,z,xi,yi,zi,w,fxy,xy,exp1,pr,sqr1,dv,rm,rm1
  real sigma,sigma1,h_z,phi_z
  character(len=mid):: id="$Id: global_disk_init.f90,v 1.31 2007/07/12 08:44:19 aake Exp $"

  call print_id (id)

  if (mpi_nx > 1 .or. mpi_nz > 1) then
    if (master) print *,'ERROR, planetary_disk_init.f90: MPI not allowed in the x- and z-directions', mpi_nx, mpi_nz
    call end_mpi
    stop
  end if

  if (mx*mpi_nx .ne. my*mpi_ny) then
    if (master) print *,'ERROR, planetary_disk_init.f90: x- and y-dimensions must be the same', mx*mpi_nx, my*mpi_ny
    call end_mpi
    stop
  end if

  if (sx .ne. sy) then
    if (master) print *,'ERROR, planetary_disk_init.f90: x- and y-sizes must be the same', sx, sy
    call end_mpi
    stop
  end if

  if (master) print 1,'planetary_disk_init.f90: xmin, sx =', xmin, sx, xmin+sx-dx*2
  if (master) print 1,'planetary_disk_init.f90: ymin, sy =', ymin, sy, ymin+sy-dy*2
  if (master) print 1,'planetary_disk_init.f90: zmin, sz =', zmin, sz, zmin+sz-dz*2
1 format(1x,a,3f12.4)

!-----------------------------------------------------------------------
!  Unperturbed density distribution, rotation
!-----------------------------------------------------------------------
  T0 = ee0*(gamma-1.)
!$omp parallel do private(ix,iy,iz,xy,q,s,fr,Tq,sigma,h_z,phi_z,sigma1)
  do iy=1,my
    do ix=1,mx
      sigma1 = 0.
      do iz=1,mz
        xy = sqrt(xm(ix)**2 + ym(iy)**2)
        q  = sqrt(xm(ix)**2 + ym(iy)**2 + r3**2)
        s  = sqrt(xm(ix)**2 + ym(iy)**2 + r3**2 + zm(iz)**2)

        fr = exp1((q-r2)/r1)

        Tq = T0*(r0/q)**p_temp
        sigma = rho0*fr*(r2/q)**p_sigma
        h_z = Tq/(pi*grav*sigma)
        phi_z = -Tq*log((1d0-tanh(min(18d0,abs(zm(iz)*1d0/h_z)))**2)) + grav*M_sun*(1./q-1./s)
        rho(ix,iy,iz) = 0.5*sigma*(1./h_z+sqrt(grav*M_sun/Tq/q**3)) &
                        *exp(-min(phi_z/Tq,60.))

        rho(ix,iy,iz) = rho(ix,iy,iz) + rho_lim

        e(ix,iy,iz) = rho(ix,iy,iz)*Tq/(gamma-1.)
        e(ix,iy,iz) = e(ix,iy,iz) + e_lim

        temp(ix,iy,iz) = Tq
        sigma1 = sigma1+dz*rho(ix,iy,iz)
      end do
      sigma = sigma/sigma1
      do iz=1,mz
        rho(ix,iy,iz) = rho(ix,iy,iz)*sigma 
        e  (ix,iy,iz) = e  (ix,iy,iz)*sigma 
      end do
    end do
  end do

  !$omp parallel private(iz)
  do iz=izs,ize
    rho(:,:,iz) = alog(rho(:,:,iz))
  end do
  !$omp barrier
  call smooth3_set (rho,scr)
  !$omp barrier
  call smooth3_set (scr,rho)
  !$omp barrier
  do iz=izs,ize
    rho(:,:,iz) = exp(rho(:,:,iz))
    e(:,:,iz) = alog(e(:,:,iz))
  end do
  
  !$omp barrier
  call smooth3_set (e  ,scr)
  !$omp barrier
  call smooth3_set (scr,e  )
  !$omp barrier

  do iz=izs,ize
    e(:,:,iz) = exp(e(:,:,iz))
    scr1(:,:,iz) = alog(e(:,:,iz))
    scr2(:,:,iz) = scr1(:,:,iz)
  end do

  call ddxdn_set (scr1, scr)
  call xup_set (scr, scr1)                                              ! scr1 = dlnP/dx
  call ddydn_set (scr2, scr)
  call yup_set (scr, scr2)                                              ! scr2 = dlnP/dz

  if (do_selfg) then
    !$omp master
    it = 0
    isubstep = 1                                                        ! make sure BCs are evaluated
    !$omp end master
    call barrier_omp('init1')
    call gravpot (rho,phi0)
  !$omp barrier
    call ddxdn_set (phi0,scr)
    call xup_set (scr,phix)
    call ddydn_set (phi0,scr)
    call yup_set (scr,phiy)
  else
    do iz=izs,ize
      phix(:,:,iz) = 0.
      phiy(:,:,iz) = 0.
    end do
  end if
  !$omp end parallel

!-----------------------------------------------------------------------
!  Rotation.  Inward force if dlnP/dx is pos where xm is pos -> increase w
!-----------------------------------------------------------------------
  !$omp parallel private(ix,iy,iz,xy,q,s,w)
  do iz=izs,ize
    do iy=1,my
      do ix=1,mx
        xy = sqrt(xm(ix)**2 + ym(iy)**2)
        q  = sqrt(xm(ix)**2 + ym(iy)**2 + r3**2)
        s  = sqrt(xm(ix)**2 + ym(iy)**2 + r3**2 + zm(iz)**2)
! w is the angular frequency in radians per second, with the first term giving
! the Keplerian value, and the second term giving the correction for the radial
! pressure gradient.  If, for example, pressure drops with radius, then scr1
! is negative, which should lead to a drop in speed (pressure support), so the
! sign in front of temp*scr1 should be positive.
        w = grav*M_sun/s**3 + &
            (xm(ix)*(phix(ix,iy,iz)+temp(ix,iy,iz)*scr1(ix,iy,iz)) + &
             ym(iy)*(phiy(ix,iy,iz)+temp(ix,iy,iz)*scr2(ix,iy,iz)))/max(xy,xm(2)-xm(1))**2
        w = sqrt(max(w,0.))
        w = w - omega
        w = max(w,0.0)
        px(ix,iy,iz) =  w*ym(iy)
        py(ix,iy,iz) = -w*xm(ix)
        pz(ix,iy,iz) = 0.
        rho(ix,iy,iz) = frho*rho(ix,iy,iz)
      end do
    end do
  end do

!-----------------------------------------------------------------------
!  Change from centered velocity to staggered momenta
!-----------------------------------------------------------------------
  do iz=izs,ize
    scr1(:,:,iz) = alog(rho(:,:,iz))
  end do
  call xdn_set (scr1, scr)
  call xdn_set (px, temp)
  do iz=izs,ize
    px(:,:,iz) = exp(scr(:,:,iz))*temp(:,:,iz)
  end do

  call ydn_set (scr1, scr)
  call ydn_set (py, temp)
  do iz=izs,ize
    py(:,:,iz) = exp(scr(:,:,iz))*temp(:,:,iz)
  end do
  !$omp end parallel
 
END
