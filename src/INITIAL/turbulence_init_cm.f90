! $Id: turbulence_init_cm.f90,v 1.8 2006/07/16 21:59:03 aake Exp $
!-----------------------------------------------------------------------
SUBROUTINE init_values (lnr,Ux,Uy,Uz,ee,d,Bx,By,Bz)
  USE params
  USE arrays, ONLY: scr5, scr6
  USE forcing
  implicit none

  real, dimension(mx,my,mz):: lnr,Ux,Uy,Uz,ee,d,Bx,By,Bz
  real, allocatable, dimension(:,:,:):: Ui,dUx,dUy,dUz,r
  logical do_init, do_test, scr_alloc
  character(len=32):: test
  real r0,ee0,d0,b0,gam1,mrms,average,ti_turn
  integer iz,iy,ix
  namelist /init/do_init,do_test,test,r0,d0,b0,ti_turn
  character(len=mid), save:: id='$Id'

  call print_id(id)

!-----------------------------------------------------------------------
!  Initial value parameters
!-----------------------------------------------------------------------
  r0 = 1.
  b0 = 0.
  d0 = 1.
  do_init = .true.
  do_test = .false.
  ti_turn = t_turn
  test = 'shear'

  rewind (stdin); read (stdin,init)
  if (master) write (stdout,init)

  if (do_test) then
    call test_values (test,lnr,Ux,Uy,Uz,ee,d,Bx,By,Bz)
    return
  end if

  gam1=gamma*(gamma-1.)
  if (gamma .eq. 1.) gam1=1.
  ee0 = csound**2/gam1

  allocate (r(mx,my,mz))
!$omp parallel do private(iz)
  do iz=1,mz
    lnr(:,:,iz) = alog(r0)
    r(:,:,iz) = r0
    ee(:,:,iz) = ee0
    Ux(:,:,iz) = 0.
    Uy(:,:,iz) = 0.
    Uz(:,:,iz) = 0.
    if (do_mhd) then
      bx(:,:,iz) = 0.
      by(:,:,iz) = 0.
      bz(:,:,iz) = b0
    end if
    if (do_pscalar) d(:,:,iz)=d0
  end do

  if (.not. do_init) return

!-----------------------------------------------------------------------
!  Initial linear acceleration
!-----------------------------------------------------------------------
  allocate (Ui(mx,my,mz))
  allocate (dUx(mx,my,mz), dUy(mx,my,mz), dUz(mx,my,mz))
  if (allocated(scr5)) then
    scr_alloc = .false.
  else
    allocate (scr5(mx,my,mz), scr6(mx,my,mz))
    scr_alloc = .true.
  end if
  !$omp parallel do private(iz)
  do iz=1,mz
    Ui(:,:,iz)=0.
    dUx(:,:,iz)=0.
    dUy(:,:,iz)=0.
    dUz(:,:,iz)=0.
  end do
  call forceit (r,Ui,Ui,Ui,r,r,r,dUx,dUy,dUz)
  deallocate (r,Ui)
  if (scr_alloc) then
    deallocate (scr5, scr6)
  end if
  !$omp parallel do private(iz)
  do iz=1,mz
    if (do_helmh) then
      Ux(:,:,iz) = (0.25+0.25*a_helmh)*ti_turn*dUx(:,:,iz)
      Uy(:,:,iz) = (0.25+0.25*a_helmh)*ti_turn*dUy(:,:,iz)
      Uz(:,:,iz) = (0.25+0.25*a_helmh)*ti_turn*dUz(:,:,iz)
    else
      Ux(:,:,iz) = (0.25*ti_turn)*dUx(:,:,iz)
      Uy(:,:,iz) = (0.25*ti_turn)*dUy(:,:,iz)
      Uz(:,:,iz) = (0.25*ti_turn)*dUz(:,:,iz)
    end if
  end do
  deallocate (dUx,dUy,dUz)

END
