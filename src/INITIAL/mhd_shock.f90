! $Id: mhd_shock.f90,v 1.2 2004/12/22 02:14:50 aake Exp $

!************************************************************
! subroutine written as replacement for generic.f90
!    -- obj: write initial conditions- MHD tests of stagger
!************************************************************ 

SUBROUTINE init_values (r,px,py,pz,e,d,Bx,By,Bz)

  USE params
  implicit none
  real, dimension(mx,my,mz):: r,px,py,pz,e,d,Bx,By,Bz
!hpf$ distribute(*,*,block):: r,px,py,pz,e,d,Bx,By,Bz
  logical do_init, do_test
  real r0,e0,d0,b0,uy0
  real rr,rl,pr,pl,Bxr,Bxl,Byr,Byl,Bzr,Bzl
  integer i,j,k,disc
  character(len=16) test
  namelist /init/do_init,do_test,test,r0,e0,d0,b0,uy0
  do_test=.false.

  if (do_test) then
    call test_values (test,r,px,py,pz,e,d,Bx,By,Bz)
    return
  end if

! Discontinuity conditions as per Stone & Norman (ZEUS code)
!  x-dir is primary direction of propagation in ZEUS

  disc = int(mytot*.36)
  print *,'disc= ',disc

   rl = 1.0 
   pl = 1.0
  Bxl = 0.75
  Byl = 1.0
  
   rr = 0.125
   pr = 0.1
  Bxr = 0.75
  Byr = -1.0


! Set initial conditions for STAGGER
!   discontinuity is at cell face

  px=0.
  py=0.
  pz=0.
  Bx=0.
  Bz=0.

! Wtih gamma = 2,  pp = (gm1)*r*ee = (1)*e = e
  do k=1,mz
  do j=1,disc
         r(:,j,k) = rl
         e(:,j,k) = pl
        Bx(:,j,k) = Byl
        By(:,j,k) = Bxl
        Bz(:,j,k) = Byl
     end do
  do j=disc+1,my
         r(:,j,k) = rr
         e(:,j,k) = pr
        Bx(:,j,k) = Byr
        By(:,j,k) = Bxr
        Bz(:,j,k) = Byr
     end do 
  end do

! Revise B at discontinuity point (since B is defined at cell faces)

  Bx(:,disc+1,:) = 0.5*(Byl + Byr)
  By(:,disc+1,:) = 0.5*(Bxl + Bxr)
  Bz(:,disc+1,:) = 0.5*(Byl + Byr)

! Smear discontinutiy
    
  r=(cshift(r,-1,dim=2)+r+cshift(r,1,dim=2))/3.
  e=(cshift(e,-1,dim=2)+e+cshift(e,1,dim=2))/3.
  Bx=(cshift(Bx,-1,dim=2)+Bx+cshift(Bx,1,dim=2))/3.
  Bz=(cshift(Bz,-1,dim=2)+Bz+cshift(Bz,1,dim=2))/3.

  do k=1,mz
  do j=1,10
         r(:,j,k) = rl
         e(:,j,k) = pl
        Bx(:,j,k) = Byl
        By(:,j,k) = Bxl
        Bz(:,j,k) = Byl
     end do
  do j=my-10,my
         r(:,j,k) = rr
         e(:,j,k) = pr
        Bx(:,j,k) = Byr
        By(:,j,k) = Bxr
        Bz(:,j,k) = Byr
     end do 
  end do

! Case:

 if(do_dump) print *,'Bx= ',Bx(1,disc-1:disc+2,1)
 if(do_dump) print *,'Bz= ',Bz(1,disc-1:disc+2,1)

! Choose Perpandicular B direction = non-zero direction

!Bx = 0.
 Bz = 0.
 
END
