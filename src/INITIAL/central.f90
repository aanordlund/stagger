FUNCTION exp1(x)
  real :: x, exp1
  exp1 = exp(-x)
  exp1 = exp1/(1.+exp1)
END

SUBROUTINE init_values (rho,px,py,pz,e,d,Bx,By,Bz)
  USE params
  USE stagger
  implicit none

  real, dimension(mx,my,mz):: rho,px,py,pz,e,d,Bx,By,Bz
  logical do_init, do_test
  real rho0,ee0,d0,B0,w0,r0,r1,rr
  real r,x,y,z,w,eps,eps2,fxy,fr,xy,exp1,pow,pow2
  integer ix,iy,iz
  namelist /init/do_init,do_test,r0,r1,rho0,ee0,d0,B0,w0,eps2,pow,pow2
  character(len=70):: id="$Id"

  if (id.ne.' ') print *,id ; id=' '

!-----------------------------------------------------------------------
!  Initial value parameters
!-----------------------------------------------------------------------
  rho0=1.
  ee0=1.
  b0=0.
  d0=1.
  w0=4.
  r1=2.
  do_init=.true.
  do_test=.false.
  eps2 = 1.                                              ! Only for density
  pow = -2.
  pow2 = 5.

  read (*,init)
  write (*,init)

  if (do_test) then
    call test_values (r,px,py,pz,e,d,Bx,By,Bz)
    return
  end if

  if (.not. do_init) return

!-----------------------------------------------------------------------
!  Inner solid body rotation
!-----------------------------------------------------------------------
  eps = 1.e-2                                             ! Make it bigger
  r0 = (mx/2+r1*alog(eps))
  do iz=1,mz
    z = (iz-1-mz/2)
    do iy=1,my
      y = (iy-1-my/2)
      do ix=1,mx
        x = (ix-1-mx/2)
        r = sqrt(x**2+y**2+z**2)
       ! r = r/100.
        xy = sqrt(x**2+y**2)
        fr = exp1((r-r0)/r1)
        fxy = exp1((xy-r0)/r1)
        w = w0*fxy
        rr = (r**pow2+eps2**pow2)**(1./pow2)
        rho(ix,iy,iz) = rho0*rr**pow            ! Central density
        px(ix,iy,iz) =  w*y/r0
        py(ix,iy,iz) = -w*x/r0
        if (do_pscalar) d(ix,iy,iz) = (d0*xy/r0+eps)*rho(ix,iy,iz)
      end do
    end do
  end do

  px = exp(xdn(alog(rho)))*xdn(px)
  py = exp(ydn(alog(rho)))*ydn(py)
  pz = 0.
  e = rho*ee0

  if (do_mhd) then
    Bx = 0.
    By = 0.
    Bz = B0
  end if
END
