! $Id: mhd_shock.f90,v 1.2 2004/12/22 02:14:50 aake Exp $

!************************************************************
! subroutine written as replacement for generic.f90
!    -- obj: write initial conditions- MHD tests of stagger
!************************************************************ 

SUBROUTINE init_values (r,px,py,pz,e,d,Bx,By,Bz)

  USE params
  implicit none
  real, dimension(mx,my,mz):: r,px,py,pz,e,d,Bx,By,Bz
  real, dimension(mx,my,mz):: lnr,ydnl,ydnr
!hpf$ distribute(*,*,block):: r,px,py,pz,e,d,Bx,By,Bz
  logical do_init, do_test
  real r0,e0,d0,b0,uy0
  real rr,rl,er,el
  integer i,j,k,disc
  character(len=16) test
  namelist /init/do_init,do_test,test,r0,e0,d0,b0,uy0

! Discontinuity conditions as per Stone & Norman (ZEUS code)
!  x-dir is primary direction of propagation in ZEUS

  disc = int(mytot*.1)

   uy0 = 1.0

   rl = 1.0 
   el = 1.0
  
   rr = 2.0
   er = 2.0


! Set initial conditions for STAGGER
!   discontinuity is at cell face

  r(:,:,:)=rl
  e(:,:,:)=el
  px=0.
  pz=0.


! Wtih gamma = 2,  pp = (gm1)*r*ee = (1)*e = e
  do k=1,mz
  do j=disc,disc+50
         r(:,j,k) = rr
         e(:,j,k) = er
     end do 
  end do

! Smear discontinutiy
    
  r=(cshift(r,-2,dim=2)+cshift(r,-1,dim=2)+r+cshift(r,1,dim=2)+cshift(r,2,dim=2) &
        +cshift(r,-4,dim=2)+cshift(r,-3,dim=2)+cshift(r,3,dim=2)+cshift(r,4,dim=2))/9.
  r=(cshift(r,-2,dim=2)+cshift(r,-1,dim=2)+r+cshift(r,1,dim=2)+cshift(r,2,dim=2))/5.
  r=(cshift(r,-2,dim=2)+cshift(r,-1,dim=2)+r+cshift(r,1,dim=2)+cshift(r,2,dim=2))/5.
! r=(cshift(r,-1,dim=2)+r+cshift(r,1,dim=2))/3.
  e=(cshift(e,-2,dim=2)+cshift(e,-1,dim=2)+e+cshift(e,1,dim=2)+cshift(e,2,dim=2) &
        +cshift(e,-4,dim=2)+cshift(e,-3,dim=2)+cshift(e,3,dim=2)+cshift(e,4,dim=2))/9.
  e=(cshift(e,-2,dim=2)+cshift(e,-1,dim=2)+e+cshift(e,1,dim=2)+cshift(e,2,dim=2))/5.
  e=(cshift(e,-2,dim=2)+cshift(e,-1,dim=2)+e+cshift(e,1,dim=2)+cshift(e,2,dim=2))/5.

  do k=1,mz
    do j=1,10
         r(:,j,k) = rl
         e(:,j,k) = el
    end do
    do j=my-10,my
         r(:,j,k) = rl
         e(:,j,k) = el
    end do 
  end do

! py=uy0*r
  lnr=alog(r)
  call ydn_set (lnr, ydnl)
  ydnr=exp(ydnl)
  py= uy0*ydnr

END
