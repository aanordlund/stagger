! $Id: turbulence_tests.f90,v 1.1 2004/08/26 08:41:03 aake Exp $
!-----------------------------------------------------------------------
SUBROUTINE test_values (type,r,px,py,pz,e,d,Bx,By,Bz)
  USE params
  USE stagger
  implicit none

  real, dimension(mx,my,mz):: r,px,py,pz,e,d,Bx,By,Bz
  character(len=32):: type
  real:: k,ampl,bz0,r0,e0,ux0,uz0,ux1,f1,f2
  integer:: ix,iy,iz
  namelist /xshear/ux0,uz0,ux1,bz0,ampl,k

!-----------------------------------------------------------------------
!  Initial value parameters
!-----------------------------------------------------------------------
  r0=1. ; e0=1. ; k = 1. ; ampl=0.1 ; bz0=1.

!$omp parallel do private(iz)
  do iz=1,mz
    r (:,:,iz) = 1.
    px(:,:,iz) = 0.
    py(:,:,iz) = 0.
    pz(:,:,iz) = 0.
    e (:,:,iz) = e0
  end do

  if (do_mhd) then
!$omp parallel do private(iz)
    do iz=1,mz
      bx(:,:,iz) = 0.
      by(:,:,iz) = 0.
      bz(:,:,iz) = 0.
    end do
  end if

  select case (type)
  case ('x-shear')
    ux0 = 2.
    uz0 = 4.
    ux1 = 0.
    ampl = 0.1
    k = 1.
    read  (*,xshear)
    write (*,xshear)
!$omp parallel do private(iz,ix)
    do iz=1,mz
     do ix=1,mx/2
      f1 = 1.+ampl*cos(2.*pi*(iz*k/real(mz)-uz0/(ux0+1e-30)*ix/real(mx)))
      f2 =         cos(2.*pi*(iz*k/real(mz)))
      px(ix     ,:,iz) =  ux0*r0*f1+ux1*f2
      px(ix+mx/2,:,iz) = -ux0*r0*f1+ux1*f2
      pz(ix     ,:,iz) =  uz0*r0*f1
      pz(ix+mx/2,:,iz) = -uz0*r0*f1
      if (do_mhd) bz(:,:,iz) = bz0
     end do
     pz(1     ,:,iz) = 0.
     pz(1+mx/2,:,iz) = 0.
    end do
  case ('x-shear1')
    ux0 = 2.
    uz0 = 4.
    ampl = 0.1
    k = 1.
    read  (*,xshear)
    write (*,xshear)
!$omp parallel do private(iz)
    do iz=1,mz
      f1 = 1.+ampl*cos(iz*2.*pi*k/mz)
      px(2:mx/2  ,:,iz) =  ux0*r0
      px(mx/2+2: ,:,iz) = -ux0*r0
      pz(1:mx/2   ,:,iz) =  uz0*r0
      pz(mx/2+1:mx,:,iz) = -uz0*r0
      if (do_mhd) bz(:,:,iz) = bz0
    end do
  end select
END
