! $Id: BE_init.f90,v 1.6 2007/03/31 22:38:59 aake Exp $
!-----------------------------------------------------------------------
MODULE BE
  implicit none
  real r_BE,r_max,rho_max,rho_ext,mach_ext,f_BE
END MODULE

!-----------------------------------------------------------------------
SUBROUTINE init_values (rho,px,py,pz,e,d,Bx,By,Bz)
  USE params
  USE BE
  USE boundary
  USE selfgravity
  USE forcing, only: conservative
  implicit none
  real, dimension(mx,my,mz):: rho,px,py,pz,e,d,Bx,By,Bz
  real, allocatable, dimension(:,:,:):: scr
  integer ix,iy,iz,n
  real Bonnor_Ebert,f,r,xi,a1,a2,b1,b2
  namelist /init/r_BE,r_max,rho_max,rho_ext,mach_ext,a1,a2,b1,b2,f_BE
  character(len=mid):: id="$Id: BE_init.f90,v 1.6 2007/03/31 22:38:59 aake Exp $"
!
  call print_id(id)
!
  r_BE = 0.
  r_max = 6.
  rho_max = 1.
  rho_ext = 0.
  mach_ext = 0.
  a1 = 1.
  a2 = 1.
  b1 = 0.
  b2 = 0.
  f_BE=1.2
!
  rewind (stdin); read (stdin,init)
  if (r_BE .eq. 0.) r_BE=f_BE/sqrt(grav)
  if (rho_ext .eq. 0.) then
    rho_ext = Bonnor_Ebert(r_max)/mach_ext**2
  else
    rho_ext = Bonnor_Ebert(r_max)/rho_ext
  end if
  if (master) write (*,init)
  r_max = r_max*r_BE
! 
  do iz=1,mz
  do iy=1,my
  do ix=1,mz
    r = sqrt(xm(ix)**2 + ym(iy)**2 + zm(iz)**2)
    if (r < r_max+0.1*dx) then
      xi = r/r_BE
      rho(ix,iy,iz) = max (rho_ext, rho_max*Bonnor_Ebert(xi))
if (iy==my/2.and.iz==mz/2) print '(i4,6g12.4)',ix,rho_ext,rho_max,xi,Bonnor_Ebert(xi),rho(ix,iy,iz)
      px(ix,iy,iz) = 0.
      py(ix,iy,iz) = 0.
      pz(ix,iy,iz) = 0.
    else
      rho(ix,iy,iz) = rho_ext*(r_max/r)**2
      f = 1./(r+1e-30)
      if (conservative) then
        px(ix,iy,iz) = -rho(ix,iy,iz)*csound*mach_ext*(1.*xm(ix)+b1*0.5*ym(iy)+b2*0.7*zm(iz))*f
        py(ix,iy,iz) = -rho(ix,iy,iz)*csound*mach_ext*(a1*ym(iy)+b1*0.9*zm(iz)+b2*0.4*xm(ix))*f
        pz(ix,iy,iz) = -rho(ix,iy,iz)*csound*mach_ext*(a2*zm(iz)+b1*0.7*xm(ix)+b2*0.6*ym(iy))*f
      else
        rho(ix,iy,iz) = alog(rho(ix,iy,iz))
        px(ix,iy,iz) = -csound*mach_ext*(1.*xm(ix)+b1*0.5*ym(iy)+b2*0.7*zm(iz))*f
        py(ix,iy,iz) = -csound*mach_ext*(a1*ym(iy)+b1*0.9*zm(iz)+b2*0.4*xm(ix))*f
        pz(ix,iy,iz) = -csound*mach_ext*(a2*zm(iz)+b1*0.7*xm(ix)+b2*0.6*ym(iy))*f
      end if
    end if
  end do
  end do
  end do
!
  allocate (scr(mx,my,mz))
  call xdn1_set(px,scr); call copy(scr,px)
  call ydn1_set(py,scr); call copy(scr,py)
  call zdn1_set(pz,scr); call copy(scr,pz)
  deallocate (scr)
!
  call copy_bdry(rho,px,py,pz)
END

!-----------------------------------------------------------------------
SUBROUTINE copy_bdry(rho,px,py,pz)
  USE params
  USE boundary
  implicit none
  real, dimension(mx,my,mz):: rho,px,py,pz

  call copy(rho,rb)
  call copy(px,pxb)
  call copy(py,pyb)
  call copy(pz,pzb)
END

!-----------------------------------------------------------------------
SUBROUTINE copy1(a,b)
  USE params
  implicit none
  real, dimension(mx,my,mz):: a,b
  integer iz
!
  do iz=1,mz
    b(:,:,iz) = a(:,:,iz)
  end do
END

!-----------------------------------------------------------------------
REAL FUNCTION Bonnor_Ebert (r)
  implicit none
  real r, psir
  integer, parameter:: nxi=200
  real, save, dimension(nxi):: xi, psi
  real, save:: dxi=0.
!
  if (dxi == 0.) then 
    dxi = 0.05
    call init_BonnorEbert(nxi,dxi,xi,psi)
  end if
  call interp(nxi,xi,psi,1,r,psir,1,1)
!print *,'BE:',r,psir
  Bonnor_Ebert = exp(-psir)
END

!-----------------------------------------------------------------------
SUBROUTINE init_BonnorEbert(nxi,dxi,xi,psi)
  implicit none
  integer nxi,i
  real dxi,x
  real, dimension(nxi):: xi,psi
  real, dimension(2):: y
  external BE_ode
!
  xi(1) = 0.
  psi(1) = 0.
  x = dxi
  y(1) = x**3/3.
  y(2) = x**2/6.
  xi(2) = x
  psi(2) = y(2)
  do i=3,nxi
    call rk4(x,dxi,2,y,BE_ode)
    xi(i) = x
    psi(i) = y(2)
    print *,i,x,y(2),exp(-y(2))
  end do
END

!-----------------------------------------------------------------------
SUBROUTINE BE_ode(x,y,dydx)
  real x,y(2),dydx(2)
!
  dydx(1) = x**2*exp(-y(2))
  dydx(2) = y(1)/x**2
END

!-----------------------------------------------------------------------
SUBROUTINE rk4(x,dx,n,y,ode)
  implicit none
  integer n
  real x,dx,x0,y(n),y0(n),y1(n),dydx(n)
  external ode
!
  y0 = y
  x0 = x
  call ode(x,y,dydx)
  y1 = y0 + (dx/6.)*dydx
  y  = y0 + (dx/2.)*dydx
  x  = x0 + (dx/2.)
  call ode(x,y,dydx)
  y1 = y1 + (dx/3.)*dydx
  y  = y0 + (dx/2.)*dydx
  call ode(x,y,dydx)
  y1 = y1 + (dx/3.)*dydx
  x  = x0 + dx
  y  = y0 + dx*dydx
  call ode(x,y,dydx)
  y  = y1 + (dx/6.)*dydx
END
