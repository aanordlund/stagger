! $Id: blasts.f90,v 1.3 2012/12/31 16:19:16 aake Exp $
!************************************************************************
SUBROUTINE init_values (r,px,py,pz,e,d,Bx,By,Bz)

USE params
USE units

implicit none

integer, parameter:: mblast=3
integer :: nx,ny,nz,i,j,k,ix,iy,iz
real, dimension(mblast) :: x0, y0, z0
real, dimension(mx,my,mz) :: r,px,py,pz,e,d,Bx,By,Bz,profile,prof_met
real :: esn, dsn, wsn, psn, dpsn, proflim
real :: x,y,z,SNe,prof,nr,rr,ex,exd,distr
real :: rmin,emin,dmin,r_0,e_0,d_0,w,volume,v_met
character(len=mid) id

data id/'$Id: blasts.f90,v 1.3 2012/12/31 16:19:16 aake Exp $'/

print *,id

  esn = 1.
  dsn = 1.0                           ! maximum SN yield
  wsn = 4.
  psn = 5.

x0(1)=mx*dx*0.35                      ! first center
y0(1)=my*dy*0.35
z0(1)=mz*dz*0.35

x0(2)=mx*dx*0.65
y0(2)=my*dy*0.65
z0(2)=mz*dz*0.65

x0(3)=mx*dx*0.50
y0(3)=my*dy*0.50
z0(3)=mz*dz*0.75

w=wsn*dx                              ! width of the profile 
proflim=1.e-6/esn           

! Profile with approx linear slopes in the log
nr=5.
profile=0.
prof_met=0.
dpsn=2.0*psn      !-alog10(poff)*psn/4.
do i=1,mblast
  do iz=1,mz
    z = iz*dz
    do iy=1,my
      y=iy*dy
      do ix=1,mx
        x=ix*dx
        rr=sqrt((x-x0(i))**2+(y-y0(i))**2+(z-z0(i))**2)/w     ! normalized radius
        ex=exp(max(-35.,min(35.,psn*(rr-1.))))                ! psn=steepness 
        exd=exp(max(-35.,min(35.,dpsn*(rr-1.))))
        profile(ix,iy,iz)=profile(ix,iy,iz)+1./(1.+ex**2)     ! tanh()-like profile in log e
        distr=1./(1.+exd**2)
        if (distr .ge. proflim) then
          prof_met(ix,iy,iz)=prof_met(ix,iy,iz)+distr
        end if
      end do
    end do
  end do
end do
volume=sum(profile)*dx*dy*dz/mblast     ! normalization factor
v_met=sum(prof_met)*dx*dy*dz/mblast

! Normalization factors
SNe=1d51/ceu                            ! standard supernova energy
e_0=(esn*SNe)/volume                    ! esn = expl. energy in foe
d_0=(dsn*msun/cmu)/v_met                ! dsn = ejected yield in solar masses

! Parameter values for the ISM
rmin=1.                                 ! rmin*1.00E-24 g cm^-3
emin=exp(lncV)*rmin*1000.               ! emin*9.56E-15 erg cm^-3
dmin=rmin*poff                          ! to prevent underflow slow-down

! Initial conditions
r=rmin                                  ! density
px=rmin*1.e-15                          ! to prevent underflow slow-down
py=rmin*1.e-15                          ! to prevent underflow slow-down
pz=rmin*1.e-15                          ! to prevent underflow slow-down
e = emin+e_0*profile                    ! add to energy
if (do_pscalar) d = dmin+d_0*prof_met   ! add to abundance

if (do_mhd) then
  Bx = 1e-1
  By = 1e-1
  Bz = 1e-1
end if

print *,volume,pi,w
print '(1x,a,f6.1)'   , 'V/Vsph   = ', volume/(4.*pi*w**3/3.)
print '(1x,a,f6.1)'   , 'Tfact    = ', exp(-lncV)
print '(1x,a,1pg10.1)'   , 'Tmax[MK] = ', maxval(e/r)*exp(-lncV)*1e-6
print '(1x,a,1pd10.2)', 'SNe[erg] = ', esn*SNe*ceu
print '(1x,a,1pe10.2)', 'SNmass   = ', volume*rmin*(cmu/msun)

END
