! $Id: alexei-init.f90,v 1.1 2004/08/04 12:35:00 aake Exp $
!-----------------------------------------------------------------------
SUBROUTINE init_values (r,px,py,pz,e,d,Bx,By,Bz)
  USE params
  USE forcing
  implicit none

  real, dimension(mx,my,mz):: r,px,py,pz,e,d,Bx,By,Bz
  logical do_init
  real r0,e0,d0,b0
  integer iz
  namelist /init/do_init,r0,e0,d0,b0

!-----------------------------------------------------------------------
!  Initial value parameters
!-----------------------------------------------------------------------
  r0=1.
  e0=1.
  b0=0.
  d0=1.
  do_init=.true.

  read (*,init)
  write (*,init)

  if (.not. do_init) return

!-----------------------------------------------------------------------
!  Initial velocity
!-----------------------------------------------------------------------
!$omp parallel do private(iz)
  do iz=1,mz
    if (do_pscalar) d(:,:,iz)=d0
    px(:,:,iz) = r0*franx(:,:,iz)
    py(:,:,iz) = r0*frany(:,:,iz)
    pz(:,:,iz) = r0*franz(:,:,iz)
    if (do_mhd) then
      Bx(:,:,iz)=0.
      By(:,:,iz)=0.
      Bz(:,:,iz)=b0
    end if
  end do

END
