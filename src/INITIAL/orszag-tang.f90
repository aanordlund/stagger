!************************************************************
! Orszag-Tang Test
!************************************************************
 
SUBROUTINE init_values (r,px,py,pz,e,d,Bx,By,Bz)
  USE params
  USE stagger
  USE init
  implicit none
  real, dimension(mx,my,mz):: r,px,py,pz,e,d,Bx,By,Bz
  integer:: ix,iy,iz
  real:: x,z
  real:: d0,p0,v0,b0
  !---------------------------------------------------------------------
  ! Orszag & Tang (J. Fluid Mech., 90, 129, 1998) -- cf. Athena:
  ! www.astro.princeton.edu/~jstone/Athena/tests/orszag-tang/pagesource.html
  !---------------------------------------------------------------------
  py=0.
  By=0.
  v0 = 1.0
  d0 = 25.0/(36.0*pi)
  p0 = 5.0/(12.0*pi)
  b0 = 1.0/sqrt(4.0*pi)
  gamma = 5.0/3.0
  do iz=1,mz
    z = (iz - 0.5)/mz
    do iy=1,my
      do ix=1,mx
        x = (ix - 0.5)/mx
        r(ix,iy,iz) = d0
        e(ix,iy,iz) = p0/(gamma-1.0)
        px(ix,iy,iz) = -d0 * v0 * sin(2.0*pi*z)
        pz(ix,iy,iz) = +d0 * v0 * sin(2.0*pi*x)
        bx(ix,iy,iz) = -b0 * sin(2.0*pi*z)
        bz(ix,iy,iz) = +b0 * sin(4.0*pi*x)
      end do
    end do
  end do
END SUBROUTINE
