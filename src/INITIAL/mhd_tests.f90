! $Id: mhd_tests.f90,v 1.4 2004/12/20 06:51:06 aake Exp $
!-----------------------------------------------------------------------
MODULE init
  implicit none
  character (len=16) type
  real rl,rr,pl,pr,Uxl,Uxr,Uyl,Uyr,Uzl,Uzr,Bxl,Byl,Byr,Bzl,Bzr,gam1
END MODULE

SUBROUTINE test_values (test,r,px,py,pz,e,d,Bx,By,Bz)
  USE params
  implicit none
  real, dimension(mx,my,mz):: r,px,py,pz,e,d,Bx,By,Bz
  character(len=16):: test
  !---------------------------------------------------------------------
  select case (test)
  case ('mhd_tests')
    call mhd_shock_tube (r,px,py,pz,e,d,Bx,By,Bz)
  case ('orszag_tang')
    call orszag_tang (r,px,py,pz,e,d,Bx,By,Bz)
  case ('orszag_tang_yz')
    call orszag_tang_yz (r,px,py,pz,e,d,Bx,By,Bz)
  case ('orszag_tang_xz')
    call orszag_tang_xz (r,px,py,pz,e,d,Bx,By,Bz)
  case ('orszag_tang_zx')
    call orszag_tang_zx (r,px,py,pz,e,d,Bx,By,Bz)
  end select
END

!***********************************************************************
SUBROUTINE orszag_tang (r,px,py,pz,e,d,Bx,By,Bz)
  USE params
  USE stagger
  USE init
  implicit none
  real, dimension(mx,my,mz):: r,px,py,pz,e,d,Bx,By,Bz
  integer:: ix,iy,iz
  real:: x,y
  real:: d0,p0,v0,b0
  !---------------------------------------------------------------------
  ! Orszag & Tang (J. Fluid Mech., 90, 129, 1998) -- cf. Athena:
  ! www.astro.princeton.edu/~jstone/Athena/tests/orszag-tang/pagesource.html
  !---------------------------------------------------------------------
  v0 = 1.0
  d0 = 25.0/(36.0*pi)
  p0 = 5.0/(12.0*pi)
  b0 = 1.0/sqrt(4.0*pi)
  gamma = 5.0/3.0
  do iz=1,mz
    do iy=1,my
      y = (iy - 0.5)/my
      do ix=1,mx
        x = (ix - 0.5)/mx
        r(ix,iy,iz) = d0
        e(ix,iy,iz) = p0/(gamma-1.0)
        px(ix,iy,iz) = -d0 * v0 * sin(2.0*pi*y)
        py(ix,iy,iz) = +d0 * v0 * sin(2.0*pi*x)
        bx(ix,iy,iz) = -b0 * sin(2.0*pi*y)
        by(ix,iy,iz) = +b0 * sin(4.0*pi*x)
      end do
    end do
  end do
END SUBROUTINE

!***********************************************************************
SUBROUTINE orszag_tang_xz (r,px,py,pz,e,d,Bx,By,Bz)
  USE params
  USE stagger
  USE init
  implicit none
  real, dimension(mx,my,mz):: r,px,py,pz,e,d,Bx,By,Bz
  integer:: ix,iy,iz
  real:: x,z
  real:: d0,p0,v0,b0
  !---------------------------------------------------------------------
  ! Orszag & Tang (J. Fluid Mech., 90, 129, 1998) -- cf. Athena:
  ! www.astro.princeton.edu/~jstone/Athena/tests/orszag-tang/pagesource.html
  !---------------------------------------------------------------------
  v0 = 1.0
  d0 = 25.0/(36.0*pi)
  p0 = 5.0/(12.0*pi)
  b0 = 1.0/sqrt(4.0*pi)
  gamma = 5.0/3.0
  do iz=1,mz
    z = (iz - 0.5)/mz
    do iy=1,my
      do ix=1,mx
        x = (ix - 0.5)/mx
        r(ix,iy,iz) = d0
        e(ix,iy,iz) = p0/(gamma-1.0)
        px(ix,iy,iz) = -d0 * v0 * sin(2.0*pi*z)
        pz(ix,iy,iz) = +d0 * v0 * sin(2.0*pi*x)
        bx(ix,iy,iz) = -b0 * sin(2.0*pi*z)
        bz(ix,iy,iz) = +b0 * sin(4.0*pi*x)
      end do
    end do
  end do
END SUBROUTINE

!***********************************************************************
SUBROUTINE orszag_tang_yz (r,px,py,pz,e,d,Bx,By,Bz)
  USE params
  USE stagger
  USE init
  implicit none
  real, dimension(mx,my,mz):: r,px,py,pz,e,d,Bx,By,Bz
  integer:: ix,iy,iz
  real:: y,z
  real:: d0,p0,v0,b0
  !---------------------------------------------------------------------
  ! Orszag & Tang (J. Fluid Mech., 90, 129, 1998) -- cf. Athena:
  ! www.astro.princeton.edu/~jstone/Athena/tests/orszag-tang/pagesource.html
  !---------------------------------------------------------------------
  v0 = 1.0
  d0 = 25.0/(36.0*pi)
  p0 = 5.0/(12.0*pi)
  b0 = 1.0/sqrt(4.0*pi)
  gamma = 5.0/3.0
  do iz=1,mz
    z = (iz - 0.5)/mz
    do iy=1,my
      y = (iy - 0.5)/my
      do ix=1,mx
        r(ix,iy,iz) = d0
        e(ix,iy,iz) = p0/(gamma-1.0)
        py(ix,iy,iz) = -d0 * v0 * sin(2.0*pi*z)
        pz(ix,iy,iz) = +d0 * v0 * sin(2.0*pi*y)
        by(ix,iy,iz) = -b0 * sin(2.0*pi*z)
        bz(ix,iy,iz) = +b0 * sin(4.0*pi*y)
      end do
    end do
  end do
END SUBROUTINE

!***********************************************************************
SUBROUTINE orszag_tang_zx (r,px,py,pz,e,d,Bx,By,Bz)
  USE params
  USE stagger
  USE init
  implicit none
  real, dimension(mx,my,mz):: r,px,py,pz,e,d,Bx,By,Bz
  integer:: ix,iy,iz
  real:: x,z
  real:: d0,p0,v0,b0
  !---------------------------------------------------------------------
  ! Orszag & Tang (J. Fluid Mech., 90, 129, 1998) -- cf. Athena:
  ! www.astro.princeton.edu/~jstone/Athena/tests/orszag-tang/pagesource.html
  !---------------------------------------------------------------------
  v0 = 1.0
  d0 = 25.0/(36.0*pi)
  p0 = 5.0/(12.0*pi)
  b0 = 1.0/sqrt(4.0*pi)
  gamma = 5.0/3.0
  do iz=1,mz
    z = (iz - 0.5)/mz
    do iy=1,my
      do ix=1,mx
        x = (ix - 0.5)/mx
        r(ix,iy,iz) = d0
        e(ix,iy,iz) = p0/(gamma-1.0)
        pz(ix,iy,iz) = -d0 * v0 * sin(2.0*pi*x)
        px(ix,iy,iz) = +d0 * v0 * sin(2.0*pi*z)
        bz(ix,iy,iz) = -b0 * sin(2.0*pi*x)
        bx(ix,iy,iz) = +b0 * sin(4.0*pi*z)
      end do
    end do
  end do
END SUBROUTINE

!***********************************************************************
SUBROUTINE mhd_shock_tube (r,px,py,pz,e,d,Bx,By,Bz)
!
! Default:
!   Brio & Wu (JCP 75, 400, 1988) test, in units where P_B = B^2/2
! Other:
!   Kim et al (ApJ 514, 506, 1999), tests 1a, 1b
!   Stone & Normal, ZEUS code
!
  USE params
  USE stagger
  USE init
  implicit none
  integer iz
  real average
  real, dimension(mx,my,mz):: r,px,py,pz,e,d,Bx,By,Bz
  namelist /mhd/type,rr,rl,pr,pl,Uxl,Uxr,Uyl,Uyr,Uzl,Uzr,Bxl,Byl,Byr,Bzl,Bzr

  type = 'brio_wu'
  Uxl = 0.
  Uxr = 0.
  Uyl = 0.
  Uyr = 0.
  Uzl = 0.
  Uzr = 0.
  Bxl = 0.
  Byl = 0.
  Byr = 0.
  Bzl = 0.
  Bzr = 0.
  read  (*,mhd)

  select case (type)
  case ('brio_wu')
    gamma = 2.
    rl = 1.
    rr = 0.125
    pl = 1.
    pr = 0.1
    Bxl = 0.75
    Byl = 1.
    Byr = -1.
  case ('kim_etal_1a')
    gamma = 1.
    rl = 1.
    rr = 0.1
    pl = 1.
    pr = 0.1
    Bxl = 3./sqrt(4.*pi)
    Byl = 5./sqrt(4.*pi)
    Byr = 2./sqrt(4.*pi)
  case ('kim_etal_1b')
    gamma = 1.
    rl = 1.08
    pl = 1.08
    Uxl = 1.2
    Uyl = 0.01
    Uzl = 0.5
    Bxl = 2.0/sqrt(4.*pi)
    Byl = 3.6/sqrt(4.*pi)
    Bzl = 2.0/sqrt(4.*pi)
    rr = 1.
    pr = 1.
    Byr = 4.0/sqrt(4.*pi)
    Bzr = 2.0/sqrt(4.*pi)
  case ('kim_etal_1c')
    gamma = 1.
    rl = 0.12
    pl = 0.12
    Uxl = 24.
    Byl = 3./sqrt(4.*pi)
    rr = 0.3
    pr = 0.3
    Uxr = -15.
    Bzr = 3.0/sqrt(4.*pi)
  case ('kim_etal_1d')
    gamma = 1.
    rl = 1.0
    pl = 1.0
    Uxl = -1.
    Byl = 1.
    rr = 1.0
    pr = 1.0
    Uxr = 1.
    Byr = 1.
  end select

  write (*,mhd)

  gam1 = gamma-1.
  if (gam1.eq.0.) gam1=1.
  do iz=1,mz
    px(:,:,iz) = 0.                        ! common values
    py(:,:,iz) = 0.
    pz(:,:,iz) = 0.
    Bx(:,:,iz) = bxl

    r (1:mx/2,:,iz) = rl                   ! left state
    e (1:mx/2,:,iz) = pl/gam1
    px(1:mx/2,:,iz) = Uxl
    py(1:mx/2,:,iz) = Uyl
    pz(1:mx/2,:,iz) = Uzl
    By(1:mx/2,:,iz) = Byl
    Bz(1:mx/2,:,iz) = Bzl

    r (mx/2+1:mx,:,iz) = rr                ! right state
    e (mx/2+1:mx,:,iz) = pr/gam1
    px(mx/2+1:mx,:,iz) = Uxr
    py(mx/2+1:mx,:,iz) = Uyr
    pz(mx/2+1:mx,:,iz) = Uzr
    By(mx/2+1:mx,:,iz) = Byr
    Bz(mx/2+1:mx,:,iz) = Bzr
  end do
  sx = 2.
  dx = sx/mx
  dy = dx
  dz = dx
  sy = dy*my
  sz = dz*mz
  px = xdn(px)*exp(xdn(alog(r)))
  py = ydn(py)*exp(ydn(alog(r)))
  pz = zdn(pz)*exp(zdn(alog(r)))

  print *,' r',average(r) 
  print *,'px',average(px) 
  print *,'py',average(py) 
  print *,'pz',average(pz) 
  print *,' e',average(e) 
  print *,'Bx',average(Bx) 
  print *,'By',average(By) 
  print *,'Bz',average(Bz) 
END
