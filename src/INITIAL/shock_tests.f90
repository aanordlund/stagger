! $Id: shock_tests.f90,v 1.4 2012/09/13 21:00:12 aake Exp $
!-----------------------------------------------------------------------
SUBROUTINE shock_tests (test,r,px,py,pz,e,d,Bx,By,Bz)
!
!  To keep the collection of tests as simple and compact as possible
!  we put them in category files.  Some extent of reuse of variable names
!  and name lists for input is thus likely.
!-----------------------------------------------------------------------
  USE params
  USE forcing, only: conservative
  implicit none
  real, dimension(mx,my,mz):: r,px,py,pz,e,d,Bx,By,Bz,scr,lnr
  character(len=16):: test
  integer ix,iy,iz,ix1
  real ampl,ux0,uy0,r0,e0,k
  namelist /shock/ampl,ux0,uy0,r0,e0,k
  character(len=mid), save:: id='$Id: shock_tests.f90,v 1.4 2012/09/13 21:00:12 aake Exp $'
!-----------------------------------------------------------------------
  call print_id(id)

  k=1.
  r0=1.
  e0=(gamma-1.)

  select case (test)

!-----------------------------------------------------------------------
!  Supersonic flows
!-----------------------------------------------------------------------
  case ('x-shock')
    ux0=1.
    uy0=0.
    rewind (stdin); read (stdin,shock); if (master) write (*,shock)
    !$omp parallel private(iz)
    do iz=izs,ize
      r(:,:,iz) = r0
      e(:,:,iz) = e0
      px(1:mx/2,:,iz)=ux0
      px(mx/2+1:,:,iz)=0.
      py(:,:,iz) = 0.
      pz(:,:,iz) = 0.
      !e(mx/2,:,iz) = 1.5*e0
    end do
    !call xdn1_set_par(px,scr); call xup1_set_par(scr,px);
    call smooth (px,1)
    !call smooth (px,1)
    !call xdn1_set_par(px,scr)
    !px(:,:,izs:ize) = scr(:,:,izs:ize)
    !$omp end parallel

!-----------------------------------------------------------------------
!  Colliding supersonic flows
!-----------------------------------------------------------------------
  case ('x-collision')
    ux0=1.
    uy0=0.
    rewind (stdin); read (stdin,shock); if (master) write (*,shock)
    !$omp parallel private(iz)
    do iz=izs,ize
      r(:,:,iz) = r0
      e(:,:,iz) = e0
      px(1:mx/2,:,iz)=ux0
      px(mx/2+1:,:,iz)=-ux0
      py(:,:,iz) = 0.
      pz(:,:,iz) = 0.
    end do
    call xdn1_set_par(px,scr); call xup1_set_par(scr,px); 
    call xdn1_set_par(px,scr)
    px(:,:,izs:ize) = scr(:,:,izs:ize)
    !$omp end parallel

!-----------------------------------------------------------------------
!  Diagonal case
!-----------------------------------------------------------------------
  case ('xy-collision')
    ampl = 0.
    ux0=sqrt(0.5)
    uy0=sqrt(0.5)
    rewind (stdin); read (stdin,shock); if (master) write (*,shock)
    !$omp parallel private(iz,iy,ix,ix1)
    do iz=izs,ize
      do iy=1,my
        ix1 = (iy*mx)/my + ampl*sin(iy*2.*pi*k/my)
        do ix=1,ix1
          r(ix,iy,iz) = r0
          e(ix,iy,iz) = e0
          px(ix,iy,iz) = +ux0
          py(ix,iy,iz) = -uy0
          pz(ix,iy,iz) = 0.
        end do
        do ix=ix1+1,mx
          r(ix,iy,iz) = r0
          e(ix,iy,iz) = e0
          px(ix,iy,iz) = -ux0
          py(ix,iy,iz) = +uy0
          pz(ix,iy,iz) = 0.
        end do
      end do
    end do

    call xdn1_set_par(px,scr); call xup1_set_par(scr,px)
    call xdn1_set_par(px,scr)
    px(:,:,izs:ize)=scr(:,:,izs:ize)

    call ydn1_set_par(py,scr); call yup1_set_par(scr,py)
    call ydn1_set_par(py,scr)
    py(:,:,izs:ize)=scr(:,:,izs:ize)
    !$omp end parallel

  end select
!.......................................................................
END
