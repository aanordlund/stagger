! $Id: planetary_disk_init.f90,v 1.13 2006/06/10 20:18:37 aake Exp $

!-----------------------------------------------------------------------
FUNCTION exp1(x)
  real :: x, exp1
  exp1 = exp(-min(max(x,-70.),70.))
  exp1 = exp1/(1.+exp1)
END

!-----------------------------------------------------------------------
FUNCTION sqr1(x,a,b)
!
!  Implements a function that varies as (x/a)**2 for x<<b, and
!  then as (x/b)*(b/a)**2 for x>>b.
!
  real :: x, sqr1
  sqr1 = sqrt(1.+(x/b)**2)-1.       ! ~x/b for x>>b, ~0.5*(x/b)**2 for x<<b
  sqr1 = 2.*(b/a)**2*sqr1           ! ~(x/a)**2 for x<<b
END

!-----------------------------------------------------------------------
SUBROUTINE init_values (rho,px,py,pz,e,d,Bx,By,Bz)
  USE params
  USE stagger
  USE forcing
  USE selfgravity, void=>phi
  implicit none

  real, dimension(mx,my,mz):: rho,px,py,pz,e,d,Bx,By,Bz
  real, dimension(mx,my,mz):: phi,dphidx
  logical do_init,do_test
  real rho0,ee0,e1,d0,B0,ix0,iy0,iz0,soft_save,h,rho_save,cdx,phi0,fz,fr,dphidx1
  real delta4,r4,w4,a4
  real r,x,y,z,xi,yi,zi,w,fxy,xy,exp1,pr,hz,w0,sqr1,ee1,delta1,delta2,delta3,xy0,xy1
  integer ix,iy,iz,ir
  namelist /init/do_init,do_test,rho0,ee0,ee1,e1,d0,B0,hz,w0,delta1,delta2,delta3,xy0,xy1,delta4,r4,w4,a4
  character(len=mid):: id="$Id: planetary_disk_init.f90,v 1.13 2006/06/10 20:18:37 aake Exp $"

  call print_id (id)
  if (mpi_nx > 1 .or. mpi_nz > 1) then
    if (master) print *,'ERROR, planetary_disk_init.f90: MPI not allowed in the x- and z-directions', mpi_nx, mpi_nz
    call end_mpi
    stop
  end if

!-----------------------------------------------------------------------
!  Initial value parameters
!-----------------------------------------------------------------------
  rho0=1.
  ee0=1.
  ee1=100.
  e1=1.
  b0=0.
  d0=1.
  w0=1.
  delta1=0.
  delta2=0.
  delta3=0.
  delta4=0.
  r4=0.25
  w4=0.05
  a4=0.0
  xy0=mx/5.*dx
  xy1=mx/12.*dx
  do_init=.true.
  do_test=.false.

  rewind (stdin); read (stdin,init)
  if (master) write (*,init)

  if (do_test) then
    call uniform_rotation (rho,px,py,pz,e)
    return
  end if

  if (.not. do_init) return

!-----------------------------------------------------------------------
!  Gravitational potential
!-----------------------------------------------------------------------
  do iz=1,mz
    rho(:,:,iz) = 0.
  end do

  iy_mid = (my*mpi_ny)/2+1 - mpi_y*my                                   ! mid-plane index
  if (iy_mid > 0 .and. iy_mid <= my) then                               ! is it in our domain?
    rho(mx/2+1,iy_mid,mz/2+1) = M_sun/(dx*dy*dz)                        ! add central mass
  end if

  call gravpot (rho,phi)

!-----------------------------------------------------------------------
!  Unperturbed density distribution
!-----------------------------------------------------------------------
  cdx = sqrt(0.5)/dx
  if (iy_mid > 0 .and. iy_mid <= my) then
    phi0 = phi(mx/2+1,iy_mid,mz/2+1)
  else
    phi0 = 0.
  end if
  do iz=1,mz
    z = zm(iz)
    do iy=1,my
      y = ym(iy)
      do ix=1,mx
        x = xm(ix)
        r = max(sqrt(x**2+y**2+z**2),eps*dx)
!        r = 1./(1./max(r**20.,1e-10)+1./r0**20)**0.05
!        r = min(r,0.5*sx)
!        r = min(r,0.5*sx-3.*dx)
        r = min(r,r0-2.*dx)
        ir = mx/2+1-r*cdx
        ir = max(ir,1)
        pr = mx/2+1-r*cdx-ir
        xy = max(sqrt(x**2+y**2),eps*dx)
        h = hz*(2.*r)**1.3
        h = max(h,dz)
        fz = exp(-sqr1(z,h,2.*h))
        rho(ix,iy,iz) = rho0*(hz/h)*(max(r,10.*dx)/sx)**(-1.5)*max(fz*exp1((1.3*r2-r)/r1),eps)
        if (iz.eq.17.and.iy.eq.51.and.ix.lt.15) then
          print *,x,z,r,h,fz
        end if
        pr = 1e-2*rho0/rho(ix,iy,iz)
        e(ix,iy,iz) = rho(ix,iy,iz)*(ee0*(1.-pr)+ee1*pr)
!        e(ix,iy,iz) = rho(ix,iy,iz)*ee0*fz + e1*(1.-fz)
        if (do_pscalar) d(ix,iy,iz) = (d0*xy/r0+eps)*rho(ix,iy,iz)
        rho(ix,iy,iz) = rho(ix,iy,iz)*(1.+delta4*(1.+a4*y/r)*exp(-((r-r4)/w4)**2))
      end do
    end do
  end do
!        e(ix,iy,iz)=rho(ix,iy,iz)*exp(alog(ee0)*(1.-pr)+alog(ee1)*pr)

!-----------------------------------------------------------------------
!  Gravitational potential, including the initial disk.  The outer 
!  boundary condition, however, only includes the central star, which
!  thus appears to be isolated.
!-----------------------------------------------------------------------
  if (iy_mid > 0 .and. iy_mid <= my) then
    rho_save = rho(mx/2+1,iy_mid,mz/2+1)
    rho(mx/2+1,iy_mid,mz/2+1) = rho(mx/2+1,iy_mid,mz/2+1) + M_sun/(dx*dy*dz)
  end if
  call gravpot (rho,phi)
  call dump(phi,'phi.dat',1)
  if (iy_mid > 0 .and. iy_mid <= my) then
    rho(mx/2+1,iy_mid,mz/2+1) = rho_save
  end if
  dphidx = xup(ddxdn(phi)) + (gamma-1.)*xup(ddxdn(alog(e)))*e/rho

!-----------------------------------------------------------------------
!  Rotation.  We give each radius the rotation that just keeps it in 
!  balance with the sum of gravity and the pressure gradient.  To get
!  a grad(phi) more undisturbed by the sides, we sample it along the
!  diagonal, and then project it along radius (factor sqrt(2.)).
!-----------------------------------------------------------------------
  do iz=1,mz
    z = zm(iz)
    do iy=1,my
      y = ym(iy)
      do ix=1,mx
        x = xm(ix)
        r = max(sqrt(x**2+y**2+z**2),eps*dx)
        r = min(r,0.5*sx)
        ir = mx/2+1-r*cdx
        ir = max(ir,1)
        pr = mx/2+1-r*cdx-ir
        dphidx1 = sqrt(2.)*((1-pr)*dphidx(ir,ir,mz/2+1)+pr*dphidx(ir+1,ir+1,mz/2+1))
        w = dphidx1/r
        w = sqrt(abs(w))*w0 - omega
        xy = max(sqrt(x**2+y**2),eps*dx)
        fr = exp1((xy-r0)/r1)
        w = w*fr
        if (r.gt.0.25*sx) w = max(w,0.0)
        px(ix,iy,iz) =  w*y
        py(ix,iy,iz) = -w*x
        rho(ix,iy,iz) = rho(ix,iy,iz)*(1.+(delta1+delta2*(x+y)/r)*exp(-((xy-xy0)/xy1)**2))
      end do
    end do
  end do

!-----------------------------------------------------------------------
!  Local perturbation
!-----------------------------------------------------------------------
  if (iy_mid > 0 .and. iy_mid <= my) then
   if (delta3 .gt. 0.) then
    ix0 = mx/4+1
    iy0 = iy_mid
    iz0 = mz/2+1
    do iz=iz0-3,iz0+3
    do iy=iy0-3,iy0+3
    do ix=ix0-3,ix0+3
      rho(ix,iy,iz) = rho(ix,iy,iz)*(1.+delta3*exp(-(ix-ix0)**2-(iy-iy0)**2-(iz-iz0)**2))
    end do
    end do
    end do
   end if
  end if

!-----------------------------------------------------------------------
!  Change from centered velocity to staggered momenta
!-----------------------------------------------------------------------
  px = exp(xdn(alog(rho)))*xdn(px)
  py = exp(ydn(alog(rho)))*ydn(py)
  pz = 0.

  if (do_mhd) then
    Bx = 0.
    By = 0.
    Bz = B0
  end if
END

SUBROUTINE uniform_rotation (r,px,py,pz,e)  
!-----------------------------------------------------------------------
!  To test the rotating coordinate system, which normally is "attached" at
!  the outer perifery, so that the corners co-rotate with the outermost 
!  radius, this subroutine instead sets up a case with a uniformly rotating 
!  coordinate system and a stationary pattern (in the fixed coordinate system).
!  If all is OK, the pattern will appear to rotate backwards for positive 
!  omega, while the corners that are attached to the rotating coordinate 
!  system appear not to rotate.
!-----------------------------------------------------------------------
  USE params
  USE forcing
  USE selfgravity
  real, dimension(mx,my,mz):: r,px,py,pz,e
  real x,y,xy,omegar,exp1,phi1,ampl
  integer ix,iy,k
  namelist /test/ampl,k

  k = 10
  ampl=0.1
  rewind (stdin); read (stdin,test)
  if (master) write (*,test)

  print *,'uniform rotation test', r0, r1, r2
  grav = 0. ; nur=0.
  sx=1. ; sy=1. ; dx=sx/mx ; dy=sy/my
  do iy=1,my
  do ix=1,mx
    !x = dx*(ix-mx/2-1-0.5)
    !y = dy*(iy-my/2-1)
    x = xm(ix)-dx/2.
    y = ym(iy)
    xy = sqrt(x**2+y**2)
    omegar = omega*exp1((xy-r0)/r1)
    px(ix,iy,:) = -y*omegar
    !x = dx*(ix-mx/2-1)
    !y = dy*(iy-my/2-1-0.5)
    x = xm(ix)
    y = ym(iy)-dy/2.
    xy = sqrt(x**2+y**2)
    omegar = omega*exp1((xy-r0)/r1)
    py(ix,iy,:) = +x*omegar
    !x = dx*(ix-mx/2-1)
    !y = dy*(iy-my/2-1)
    x = xm(ix)
    y = ym(iy)
    phi1 = atan2(x,y)
    r(ix,iy,:) = 1.+ampl*cos(phi1*k)
    e(ix,iy,:) = 1.
  end do
  end do
END SUBROUTINE
