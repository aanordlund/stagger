! $Id: waves-init.f90,v 1.13 2005/04/13 17:39:08 aake Exp $
!-----------------------------------------------------------------------
SUBROUTINE test_values (test,r,px,py,pz,e,d,Bx,By,Bz)
  USE params
  implicit none

  real, dimension(mx,my,mz):: r,px,py,pz,e,d,Bx,By,Bz,scr
  character(len=16):: type, test
  real:: k,ampl,b0,r0,e0,gam1,average,ax,ay,va,x,y,rad
  integer:: i,ix,iy,iz
  namelist /wave/type,k,ampl,b0,e0,r0

!-----------------------------------------------------------------------
!  Initial value parameters
!-----------------------------------------------------------------------
  type='y_alfven'
  k = 1. ; ampl=0.1 ; b0=1.; r0=1.; e0=1.

  read  (*,wave)
  write (*,wave)

  r  = r0
  px = 0.
  py = 0.
  pz = 0.
  if (e0 .eq. 0.) then
    e0 = csound**2*r0/(gamma*(gamma-1.))
  end if
  e  = e0

  if (do_mhd) then
    bx = 0.
    by = 0.
    bz = 0.
  end if

  select case (type)
  case ('triangle_shock')
	lb = 6
	ub = mx-5
    do ix=lb,mx/2
    do iz=lb,mz/2-ix+lb
      r(ix,1,iz) = 0.140*r0
      e(ix,1,iz) = 0.125*e0
!	  scr(ix,1,iz) = alog(r(ix,1,iz))
    end do
    end do
!	r(lb,1,mz/2) = r(lb,1,mz/2+1)
!	e(lb,1,mz/2) = e(lb,1,mz/2+1)
!	call density_boundary_log (r, scr)
!	scr = e/r
!	call energy_boundary (r,scr,scr,scr,scr,e,scr,scr,scr,scr,scr)
!	call smooth3_set (r, scr)
!	r = scr
!   call smooth3_set (e, scr)
!   e = scr
  case ('balsara_blast')
    sx = 1.
    sy = 1.
    gamma = 1.4
    do iy=1,my
    do ix=1,mx
      r(ix,iy,:) = 1.
      bx(ix,iy,:) = 100./sqrt(4.*pi)
      x = (sx/mx)*(ix-mx/2-1)
      y = (sy/my)*(iy-my/2-1)
      rad = sqrt(x**2+y**2)
      if (rad.lt.0.1) then
        e(ix,iy,:) = 1000./(gamma-1.)
      else
        e(ix,iy,:) = 0.1/(gamma-1.)
      end if
    end do
    end do
  case ('x_alfven')
    bx = 1.
    py(:,1,1) =  ampl*cos((2.*pi*k/mx)*(/(i,i=0,mx-1)/))
    by(:,1,1) = -ampl*cos((2.*pi*k/mx)*(/(i,i=0,mx-1)/))
    py = spread(spread(py(:,1,1),dim=2,ncopies=my),dim=3,ncopies=mz)
    by = spread(spread(by(:,1,1),dim=2,ncopies=my),dim=3,ncopies=mz)
  case ('y_alfven')
    by = 1.
    px(1,:,1) =  ampl*cos((2.*pi*k/my)*(/(i,i=0,my-1)/))
    bx(1,:,1) = -ampl*cos((2.*pi*k/my)*(/(i,i=0,my-1)/))
    px = spread(spread(px(1,:,1),dim=1,ncopies=mx),dim=3,ncopies=mz)
    bx = spread(spread(bx(1,:,1),dim=1,ncopies=mx),dim=3,ncopies=mz)
  case ('xy_alfven')
    ax = 1./sqrt(2.)
    ay = ax
    bx = b0*ax
    by = b0*ay
    do iy=1,my
    do ix=1,mx
      px(ix,iy,:) =             ax*ampl*cos((2.*pi*(ix+iy))/my)*b0
      bx(ix,iy,:) = bx(ix,iy,:)-ax*ampl*cos((2.*pi*(ix+iy))/my)*b0
      py(ix,iy,:) =            -ax*ampl*cos((2.*pi*(ix+iy))/my)*b0
      by(ix,iy,:) = by(ix,iy,:)+ax*ampl*cos((2.*pi*(ix+iy))/my)*b0
    end do
    end do
  case ('xy16_alfven')
    ax= 1./sqrt(1.+6.**2)
    ay= 6./sqrt(1.+6.**2)
    bx = ax*b0
    by = ay*b0
    do iy=1,my
    do ix=1,mx
      px(ix,iy,:) =            +ay*ampl*cos((2.*pi*(6.*iy+ix))/my)*b0
      bx(ix,iy,:) = bx(ix,iy,:)-ay*ampl*cos((2.*pi*(6.*iy+ix))/my)*b0
      py(ix,iy,:) =            -ax*ampl*cos((2.*pi*(6.*iy+ix))/my)*b0
      by(ix,iy,:) = by(ix,iy,:)+ax*ampl*cos((2.*pi*(6.*iy+ix))/my)*b0
    end do
    end do
  case ('z_alfven')
    bz = 1.
    py(1,1,:) = ampl*cos((2.*pi*k/mx)*(/(i,i=0,mx-1)/))
    by(1,1,:) = ampl*cos((2.*pi*k/mz)*(/(i,i=0,mz-1)/))
    py = spread(spread(py(1,1,:),dim=1,ncopies=mx),dim=2,ncopies=my)
    by = spread(spread(by(1,1,:),dim=1,ncopies=mx),dim=2,ncopies=my)
  case ('x_alfven_stand')
    bx = 1.
    by(:,1,1) = ampl*cos((2.*pi*k/mx)*(/(i,i=0,mx-1)/))
    by = spread(spread(by(:,1,1),dim=2,ncopies=my),dim=3,ncopies=mz)
  case ('x_sound')
    gam1 = gamma-1.
    if (gamma==1.) gam1 = 1.
    csound = sqrt((gamma*gam1)*average(e)/average(r))
    px(:,1,1) = ampl*csound*r(:,1,1)*cos((2.*pi*k/mx)*(/(i-0.5,i=0,mx-1)/))
    r (:,1,1) = r(:,1,1)*(1.+ampl*cos((2.*pi*k/mx)*(/(i,i=0,mx-1)/)))
    px = spread(spread(px(:,1,1),dim=2,ncopies=my),dim=3,ncopies=mz)
    r  = spread(spread(r (:,1,1),dim=2,ncopies=my),dim=3,ncopies=mz)
    e = r**gamma
  case ('x_rel_sound')
    gam1 = gamma-1.
    if (gamma==1.) gam1 = 1.
    r(:,1,1) = r(:,1,1)*(1. + ampl*cos((2.*pi*k/mx)*(/(i-0.5,i=0,mx-1)/)))
    e(:,1,1) = e(:,1,1)*(r(:,1,1)/r(:,2,1))**gamma
    csound = sqrt((gamma*gam1)*average(e)/(1. + gamma*average(e)))
    px(:,1,1) = 1.0 + gamma*e(:,1,1)/r(:,1,1)  ! rel. enthalpy
    px(:,1,1) = ampl*cos((2.*pi*k/mx)*(/(i-0.5,i=0,mx-1)/))*px(:,1,1)*csound*r(:,1,1)
    px = spread(spread(px(:,1,1),dim=2,ncopies=my),dim=3,ncopies=mz)
    r  = spread(spread(r (:,1,1),dim=2,ncopies=my),dim=3,ncopies=mz)
    e  = spread(spread(e (:,1,1),dim=2,ncopies=my),dim=3,ncopies=mz)
  case ('y_rel_sound')
    gam1 = gamma-1.
    if (gamma==1.) gam1 = 1.
    do iy=1,my
      py(:,iy,:) = ampl*cos(2.*pi*k*(iy-1)/my)
    end do
    r = r0*(1. + py)
    e = e0*(r/r0)**gamma
    px = 0.
    scr = py*(gamma*e/r+1.)*r*sqrt(gamma*gam1*e/r/(1.+gamma*e/r))
    call ydn_set (scr,py)
    pz = 0.
  case ('z_rel_sound')
    gam1 = gamma-1.
    if (gamma==1.) gam1 = 1.
    do iz=1,mz
      pz(:,:,iz) = ampl*cos(2.*pi*k*(iz-1)/mz)
    end do
    r = r0*(1. + pz)
    e = e0*(r/r0)**gamma
    px = 0.
    py = 0.
    scr = pz*(gamma*e/r+1.)*r*sqrt(gamma*gam1*e/r/(1.+gamma*e/r))
    call zdn_set(scr,pz)
  case ('r-checker')
    do iz=1,mz
      do iy=1,my
        do ix=1,mx
          r(ix,iy,iz)=r0*exp(ampl*(2*mod(ix+iy+iz,2)-1))
          e(ix,iy,iz)=e0*(r(ix,iy,iz)/r0)**gamma
        end do
      end do
    end do
  case ('e-checker')
    do iz=1,mz
      do iy=1,my
        do ix=1,mx
          e(ix,iy,iz)=e(ix,iy,iz)*exp(ampl*(2*mod(ix+iy+iz,2)-1)*gamma)
          r(ix,iy,iz)=r(ix,iy,iz)*exp(ampl*(2*mod(ix+iy+iz,2)-1))
        end do
      end do
    end do
  case ('ee-checker')
    do iz=1,mz
      do iy=1,my
        do ix=1,mx
          r(ix,iy,iz)=r0*exp(-ampl*(2*mod(ix+iy+iz,2)-1))
        end do
      end do
    end do
  case ('px-checker')
    do iz=1,mz
      do iy=1,my
        do ix=1,mx
          px(ix,iy,iz)=ampl*(2.*mod(ix+iy+iz,2)-1)
        end do
      end do
    end do
  case ('pxy-checker')
    do iz=1,mz
      do iy=1,my
        do ix=1,mx
          px(ix,iy,iz)=+ampl*(2.*mod(ix+iy+iz,2)-1)
          py(ix,iy,iz)=-ampl*(2.*mod(ix+iy+iz,2)-1)
        end do
      end do
    end do
  case ('pxy2-checker')
    do iz=1,mz
      do iy=1,my
        do ix=1,mx
          px(ix,iy,iz)=+ampl*(2.*mod(iy,2)-1)
          py(ix,iy,iz)=-ampl*(2.*mod(ix,2)-1)
        end do
      end do
    end do
  case ('x-collision')
    px(1:mx/2,:,:)=ampl
    px(mx/2+1:,:,:)=-ampl
    call xdn1_set (px, scr)
    call xup1_set (scr, px)
    call xdn1_set (px, scr)
    px = scr
  case ('hawley-alfven')
    !
    ! Setup from astro-ph/0305142
    ! to test the S-R code with
    ! Alfven waves
    !
    ! ampl == V^y
    ! b0   == beta = Sqrt(2 P/ B^2)
    !
    rad = 0.01			! Internal energy e_int = 1e-2
    ay  = 1. + gamma*rad        ! Enthalpy h = 1 + e_int + P
    ax  = 0.001			! Velocity perturbation in z-direction
    pz  = 0.			! Pz = rho*h*W^2*vz = 0 when unperturbed
    px  = 1./sqrt(1. - ampl**2)	! Lorentz boost 
    py  = 1.*(1.+gamma*rad)*px**2*ampl	! Py = rho*h*W^2*vy
    do iy=my/3+1,my/2
      px(:,iy,:) = 1./sqrt(1. - (ampl**2 + ax**2))	! Lorentz boost 
      pz(:,iy,:) = 1.*(1.+gamma*rad)*px(:,iy,:)**2*ax	! Pz = rho*h*W^2*vz
      py(:,iy,:) = 1.*(1.+gamma*rad)*px(:,iy,:)**2*ampl	! Py = rho*h*W^2*vy
    end do
    do iy=my/2+1,2*my/3
      px(:,iy,:) = 1./sqrt(1. - (ampl**2 + ax**2))	! Lorentz boost 
      pz(:,iy,:) = -1.*(1.+gamma*rad)*px(:,iy,:)**2*ax	! Pz = rho*h*W^2*vz
      py(:,iy,:) = 1.*(1.+gamma*rad)*px(:,iy,:)**2*ampl	! Py = rho*h*W^2*vy
    end do
    r = 1.*px			! Rest mass density D = r*W = 1.*W
    ! We have e = DhW - D - P = (Py^2+Pz^2)/(Dh(W+1))*(1+gamma e_int/(hW)) + rho*e_int
    e = (py**2+pz**2)/(r*ay*(px+1))*(1. + gamma*rad/(ay*px)) + 1.*rad
    ! The field strength is initially zero transverse to the flow: Bx=Bz=0
    By = sqrt(2*(gamma-1.)*1.*rad)/b0			! By = Sqrt(2*P)/beta
    px = 0.						! Zero px again
  end select
END
