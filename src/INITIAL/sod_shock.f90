! $Id: mhd_shock.f90,v 1.2 2004/12/22 02:14:50 aake Exp $

!************************************************************
! subroutine written as replacement for generic.f90
!    -- obj: write initial conditions- MHD tests of stagger
!************************************************************ 

SUBROUTINE init_values (r,px,py,pz,e,d,Bx,By,Bz)

  USE params
  implicit none
  real, dimension(mx,my,mz):: r,px,py,pz,e,d,Bx,By,Bz
!hpf$ distribute(*,*,block):: r,px,py,pz,e,d,Bx,By,Bz
  logical do_init, do_test
  real r0,e0,d0,b0,uy0
  real rr,rl,pr,pl,Bxr,Bxl,Byr,Byl,Bzr,Bzl
  integer i,j,k,disc
  character(len=16) test
  namelist /init/do_init,do_test,test,r0,e0,d0,b0,uy0
  do_test=.false.

  if (do_test) then
    call test_values (test,r,px,py,pz,e,d,Bx,By,Bz)
    return
  end if

! Discontinuity conditions as per Stone & Norman (ZEUS code)
!  x-dir is primary direction of propagation in ZEUS


   rl = 1.0 
   pl = 1.0
  Bxl = 0.
  Byl = 0.
  
   rr = 0.125
   pr = 0.1
  Bxr = 0.
  Byr = 0.


! Set initial conditions for STAGGER
!   discontinuity is at cell face

  px=0.
  py=0.
  pz=0.

! Wtih gamma = 2,  pp = (gm1)*r*ee = (1)*e = e

!-----------------------------------------------------------------------------------
! Shock y-direction
!-----------------------------------------------------------------------------------
! disc = int(mytot*.36)
! print *,'disc= ',disc

! do k=1,mz
! do j=1,disc
!        r(:,j,k) = rl
!        e(:,j,k) = pl
!       Bx(:,j,k) = Byl
!       By(:,j,k) = Bxl
!       Bz(:,j,k) = 0.
!    end do
! do j=disc+1,my
!        r(:,j,k) = rr
!        e(:,j,k) = pr
!       Bx(:,j,k) = Byr
!       By(:,j,k) = Bxr
!       Bz(:,j,k) = 0.
!    end do 
! end do

! Revise B at discontinuity point (since B is defined at cell faces)

! Bx(:,disc+1,:) = 0.5*(Byl + Byr)
! By(:,disc+1,:) = 0.5*(Bxl + Bxr)

! Smear discontinutiy
    
! r=(cshift(r,-2,dim=2)+cshift(r,-1,dim=2)+r+cshift(r,1,dim=2)+cshift(r,2,dim=2) &
!       +cshift(r,-4,dim=2)+cshift(r,-3,dim=2)+cshift(r,3,dim=2)+cshift(r,4,dim=2))/9.
! r=(cshift(r,-2,dim=2)+cshift(r,-1,dim=2)+r+cshift(r,1,dim=2)+cshift(r,2,dim=2))/5.
! r=(cshift(r,-2,dim=2)+cshift(r,-1,dim=2)+r+cshift(r,1,dim=2)+cshift(r,2,dim=2))/5.
! e=(cshift(e,-2,dim=2)+cshift(e,-1,dim=2)+e+cshift(e,1,dim=2)+cshift(e,2,dim=2) &
!       +cshift(e,-4,dim=2)+cshift(e,-3,dim=2)+cshift(e,3,dim=2)+cshift(e,4,dim=2))/9.
! e=(cshift(e,-2,dim=2)+cshift(e,-1,dim=2)+e+cshift(e,1,dim=2)+cshift(e,2,dim=2))/5.
! e=(cshift(e,-2,dim=2)+cshift(e,-1,dim=2)+e+cshift(e,1,dim=2)+cshift(e,2,dim=2))/5.
! Bx=(cshift(Bx,-2,dim=2)+cshift(Bx,-1,dim=2)+Bx+cshift(Bx,1,dim=2)+cshift(Bx,2,dim=2) &
!       +cshift(Bx,-4,dim=2)+cshift(Bx,-3,dim=2)+cshift(Bx,3,dim=2)+cshift(Bx,4,dim=2))/9.
! Bx=(cshift(Bx,-2,dim=2)+cshift(Bx,-1,dim=2)+Bx+cshift(Bx,1,dim=2)+cshift(Bx,2,dim=2) )/5.
! Bx=(cshift(Bx,-2,dim=2)+cshift(Bx,-1,dim=2)+Bx+cshift(Bx,1,dim=2)+cshift(Bx,2,dim=2) )/5.

! do k=1,mz
! do j=1,10
!        r(:,j,k) = rl
!        e(:,j,k) = pl
!       Bx(:,j,k) = Byl
!       By(:,j,k) = Bxl
!       Bz(:,j,k) = 0.
!    end do
! do j=my-10,my
!        r(:,j,k) = rr
!        e(:,j,k) = pr
!       Bx(:,j,k) = Byr
!       By(:,j,k) = Bxr
!       Bz(:,j,k) = 0.
!    end do 
! end do

!-----------------------------------------------------------------------------------
! Shock z-direction
!-----------------------------------------------------------------------------------
  disc = int(mztot*.36)
  print *,'disc= ',disc
 
  do k=1,disc
         r(:,:,k) = rl
         e(:,:,k) = pl
        Bx(:,:,k) = Byl
        By(:,:,k) = Bxl
        Bz(:,:,k) = 0.
  end do
  do k=disc+1,my
         r(:,:,k) = rr
         e(:,:,k) = pr
        Bx(:,:,k) = Byr
        By(:,:,k) = Bxr
        Bz(:,:,k) = 0.
  end do 

! Revise B at discontinuity point (since B is defined at cell faces)

  Bx(:,:,disc+1) = 0.5*(Byl + Byr)
  By(:,:,disc+1) = 0.5*(Bxl + Bxr)

! Smear discontinutiy
    
  r=(cshift(r,-2,dim=3)+cshift(r,-1,dim=3)+r+cshift(r,1,dim=3)+cshift(r,2,dim=3) &
        +cshift(r,-4,dim=3)+cshift(r,-3,dim=3)+cshift(r,3,dim=3)+cshift(r,4,dim=3))/9.
  r=(cshift(r,-2,dim=3)+cshift(r,-1,dim=3)+r+cshift(r,1,dim=3)+cshift(r,2,dim=3))/5.
  r=(cshift(r,-2,dim=3)+cshift(r,-1,dim=3)+r+cshift(r,1,dim=3)+cshift(r,2,dim=3))/5.
  e=(cshift(e,-2,dim=3)+cshift(e,-1,dim=3)+e+cshift(e,1,dim=3)+cshift(e,2,dim=3) &
        +cshift(e,-4,dim=3)+cshift(e,-3,dim=3)+cshift(e,3,dim=3)+cshift(e,4,dim=3))/9.
  e=(cshift(e,-2,dim=3)+cshift(e,-1,dim=3)+e+cshift(e,1,dim=3)+cshift(e,2,dim=3))/5.
  e=(cshift(e,-2,dim=3)+cshift(e,-1,dim=3)+e+cshift(e,1,dim=3)+cshift(e,2,dim=3))/5.
  Bx=(cshift(Bx,-2,dim=3)+cshift(Bx,-1,dim=3)+Bx+cshift(Bx,1,dim=3)+cshift(Bx,2,dim=3) &
        +cshift(Bx,-4,dim=3)+cshift(Bx,-3,dim=3)+cshift(Bx,3,dim=3)+cshift(Bx,4,dim=3))/9.
  Bx=(cshift(Bx,-2,dim=3)+cshift(Bx,-1,dim=3)+Bx+cshift(Bx,1,dim=3)+cshift(Bx,2,dim=3) )/5.
  Bx=(cshift(Bx,-2,dim=3)+cshift(Bx,-1,dim=3)+Bx+cshift(Bx,1,dim=3)+cshift(Bx,2,dim=3) )/5.

  do k=1,10
         r(:,:,k) = rl
         e(:,:,k) = pl
        Bx(:,:,k) = Byl
        By(:,:,k) = Bxl
        Bz(:,:,k) = 0.
  end do
  do k=my-10,my
         r(:,:,k) = rr
         e(:,:,k) = pr
        Bx(:,:,k) = Byr
        By(:,:,k) = Bxr
        Bz(:,:,k) = 0.
  end do 

END
