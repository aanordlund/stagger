! $Id: generic.f90,v 1.11 2007/06/13 21:09:44 bob Exp $
SUBROUTINE init_values (r,px,py,pz,e,d,Bx,By,Bz)

  USE params
  implicit none
  real, dimension(mx,my,mz):: r,px,py,pz,e,d,Bx,By,Bz
!hpf$ distribute(*,*,block):: r,px,py,pz,e,d,Bx,By,Bz
  logical do_init, do_test
  real r0,e0,d0,b0,uy0
  character(len=16) test
  namelist /init/do_init,do_test,test,r0,e0,d0,b0,uy0

  r0=1.
  e0=1.
  b0=0.
  d0=1.
  uy0=0.
  do_init=.true.
  do_test=.false.
  if (stdin < 0) then
    read (*,init)
    write (*,init)
  else
    rewind (stdin); read (stdin,init)
    write (stdout,init)
  end if

  if (do_test) then
    call test_values (test,r,px,py,pz,e,d,Bx,By,Bz)
    return
  end if

  if (.not. do_init) return

  r=r0
  e=e0
  px=0.
  py=r*uy0
  pz=0.
  if (do_pscalar) d=d0
  if (do_mhd) then
    Bx=0.
    By=b0
    Bz=0.
  end if

END
