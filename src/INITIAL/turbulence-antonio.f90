! $Id: turbulence-antonio.f90,v 1.1 2005/01/15 12:31:06 antonio Exp $
!-----------------------------------------------------------------------
SUBROUTINE init_values (r,px,py,pz,e,d,Bx,By,Bz)
  USE params
  USE forcing
  implicit none

  real, dimension(mx,my,mz):: r,px,py,pz,e,d,Bx,By,Bz
  real, allocatable, dimension(:,:,:):: Ui,dpx,dpy,dpz
  logical do_init, do_test
  character(len=32):: test
  real r0,e0,d0,b0,gam1
  integer iz
  namelist /init/do_init,do_test,test,r0,d0,b0,e0

!-----------------------------------------------------------------------
!  Initial value parameters
!-----------------------------------------------------------------------
  r0=1.
  b0=0.
  d0=1.
  e0=1.
  do_init=.true.
  do_test=.false.
  test='shear'

  read (*,init)
  write (*,init)

  gam1=gamma*(gamma-1.)
  if (gamma .eq. 1.) gam1=1.
!  e0=r0*csound**2/gam1

  if (do_test) then
    call test_values (test,r,px,py,pz,e,d,Bx,By,Bz)
    return
  end if

  if (.not. do_init) return

!-----------------------------------------------------------------------
!  Initial linear acceleration
!-----------------------------------------------------------------------
  allocate (Ui(mx,my,mz))
  allocate (dpx(mx,my,mz), dpy(mx,my,mz), dpz(mx,my,mz))
!$omp parallel do private(iz)
  do iz=1,mz
    r(:,:,iz)=r0
    e(:,:,iz)=e0
    Ui(:,:,iz)=0.
    dpx(:,:,iz)=0.
    dpy(:,:,iz)=0.
    dpz(:,:,iz)=0.
  end do
  call forceit (r,Ui,Ui,Ui,r,r,r,dpx,dpy,dpz)
  deallocate (Ui)
!$omp parallel do private(iz)
  do iz=1,mz
    if (do_pscalar) d(:,:,iz)=d0
    if (do_helmh) then
      px(:,:,iz) = (0.50*t_turn)*dpx(:,:,iz)
      py(:,:,iz) = (0.50*t_turn)*dpy(:,:,iz)
      pz(:,:,iz) = (0.50*t_turn)*dpz(:,:,iz)
    else
      px(:,:,iz) = (0.25*t_turn)*dpx(:,:,iz)
      py(:,:,iz) = (0.25*t_turn)*dpy(:,:,iz)
      pz(:,:,iz) = (0.25*t_turn)*dpz(:,:,iz)
    end if
    if (do_mhd) then
      Bx(:,:,iz)=0.
      By(:,:,iz)=0.
      Bz(:,:,iz)=b0
    end if
  end do
  deallocate (dpx,dpy,dpz)

END
