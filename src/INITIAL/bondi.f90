! $Id: bondi.f90,v 1.2 2005/01/12 19:12:36 troels_h Exp $
!***********************************************************************
SUBROUTINE init_values (r,px,py,pz,e,d,Bx,By,Bz)

  USE metric
  USE stagger

  implicit none
  real, dimension(mx,my,mz):: r,px,py,pz,e,d,Bx,By,Bz
!hpf$ distribute(*,*,block):: r,px,py,pz,e,d,Bx,By,Bz
  logical do_init, do_test
  real    :: r0,e0,d0,b0,uy0,rlim
  integer :: iy
  namelist /init/do_init,do_test,r0,e0,d0,b0,uy0,rlim

  r0=1.
  e0=1.
  b0=0.
  d0=1.
  uy0=0.
  rlim=0.05
  do_init=.true.
  do_test=.false.
  read (*,init)
  write (*,init)
  ! Initialize metric
  call initialize_metric(rlim) ! Setup metric components

  if (do_test) then
    call test_values (r,px,py,pz,e,d,Bx,By,Bz)
    return
  end if

  if (.not. do_init) return
  
  do iy=1,my
    r(:,iy,:) = r0/(ym(iy)**2*sqrt(2.*mass/ym(iy)*(1.-2.*mass/ym(iy))))
    e(:,iy,:) = e0/((sqrt(2.*mass/ym(iy))*ym(iy)**2)**gamma* &
                    (1.-2.*mass/ym(iy))**(0.25*gamma + 0.25))
    px(:,iy,:)= sqrt(2.*mass/ym(iy))*(1. - 2.*mass/ym(iy)) 
    py(:,iy,:)= (r(:,iy,:)+gamma*e(:,iy,:))* &
                      sqrt(2.*mass/ym(iy))*(1.-2.*mass/ym(iy))
  enddo
  ! Calculate v*Gamma from Gamma
  pz = px/sqrt(1. - mmult(px**2,metricg(0,2,2))) 
  ! Classic formula. If D = rho Gamma, E = rho e_int Gamma and
  ! P = (gamma-1)rho e_int, then we have:
  ! T^tt - D = (D/(1+Gamma) + (gamma - 1)E/Gamma)v*Gamma + E Gamma
  ! where  all terms are positive, and never therefore well behaved.
  r  = 1. 
  e  = (r/(1. + px) + (gamma - 1.)*e/px)*pz**2 + e*px
  r  = mmult(r,determinantg(0))
  e  = mmult(e,determinantg(0))
  px = 0.
  py = ydn(mmult(py,determinantg(0)))
  pz = 0.
  
  if (do_pscalar) d=d0
  if (do_mhd) then
    Bx=0.
    By=0.
    Bz=0.
  end if

END
