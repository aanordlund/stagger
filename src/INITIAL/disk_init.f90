! $Id: disk_init.f90,v 1.1 2004/03/25 07:47:20 aake Exp $
!***********************************************************************
MODULE cooling
  real rho0, hscale
END MODULE

!***********************************************************************
SUBROUTINE initial_values
  use params
  use arrays
  use cooling

  implicit none
  integer ix,iy,iz
  real y,ux0,uy0,ee0
  namelist /init/rho0,ee0,ux0,uy0,hscale

  ux0 = 0.1
  uy0 = 0.1
  rho0 = 1.
  ee0 = 1.
  hscale = 0.1
  write (*,init)
  read  (*,init)
  write (*,init)

  px = 0.
  py = 0.

!$omp do
  do iz=1,mz
  do iy=1,my
  do ix=1,mx
    y = dy*(iy-(my+1)/2)
    rho(ix,iy,iz) = rho0*exp(-(y/hscale)**2)
    e(ix,iy,iz) = ee0*(rho(ix,iy,iz)/rho0)**gamma
    px(ix,iy,iz) = ux0*rho(ix,iy,iz)*sin(ix*2.*pi/mx)*sin(iy*2.*pi/my)
    py(ix,iy,iz) = uy0*rho(ix,iy,iz)*sin(iz*2.*pi/mz)*sin(iy*2.*pi/my)
  end do
  end do
  end do
END
