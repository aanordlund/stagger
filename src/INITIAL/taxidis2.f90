! $Id: taxidis2.f90,v 1.1 2004/03/21 15:18:31 aake Exp $
!-----------------------------------------------------------------------

FUNCTION exp1(x)
  real :: x, exp1
  exp1 = exp(-x)
  exp1 = exp1/(1.+exp1)
END

SUBROUTINE init_values (rho,px,py,pz,e,d,Bx,By,Bz)
  USE params
  USE stagger
  implicit none

  real, dimension(mx,my,mz):: rho,px,py,pz,e,d,Bx,By,Bz
  logical do_init, do_test
  real rho0,rho1,ee0,d0,B0,w0,w1,r0,r1
  real r,x,y,z,w,eps,fxy,fxz,fr,xy,xz,exp1
  integer ix,iy,iz
  namelist /init/do_init,do_test,r0,r1,rho0,rho1,ee0,d0,B0,w0,w1
  character(len=70):: id="$Id"

  if (id.ne.' ') print *,id ; id=' '

!-----------------------------------------------------------------------
!  Initial value parameters
!-----------------------------------------------------------------------
  rho0=1.
  ee0=1.
  b0=0.
  d0=1.
  w0=2.
  w1=4.
  r0=sz/3.
  r1=1.
  do_init=.true.
  do_test=.false.

  read (*,init)
  write (*,init)
  r1 = r1*dx

  if (do_test) then
    call test_values (r,px,py,pz,e,d,Bx,By,Bz)
    return
  end if

  if (.not. do_init) return

!-----------------------------------------------------------------------
!  Outer solid body rotation
!-----------------------------------------------------------------------
  eps = 1e-4
  r0 = (sz/2.+r1*alog(eps))
  do iz=1,mz
    z = dz*(iz-1-mz/2)
    do iy=1,my
      y = dy*(iy-1-my/2)
      do ix=1,mx
        x = dx*(ix-1-mx/2)
        r = sqrt(x**2+y**2+z**2)
        fr = exp1((r-r0)/r1)
        xy = sqrt(x**2+y**2)
        fxy = exp1((xy-r0)/r1)
        w = w0*fxy
        rho(ix,iy,iz) = rho0*(fr+eps)
        px(ix,iy,iz) =  w*y/r0
        py(ix,iy,iz) = -w*x/r0
        if (do_pscalar) d(ix,iy,iz) = (d0*xy/r0+eps)*rho(ix,iy,iz)
      end do
    end do
  end do

!-----------------------------------------------------------------------
!  Inner solid body rotation
!-----------------------------------------------------------------------
  eps = 1e-4
  r0 = sz/4.
  do iz=1,mz
    z = dz*(iz-1-mz/2)
    do iy=1,my
      y = dy*(iy-1-my/2)
      do ix=1,mx
        x = dx*(ix-1-mx/2)
        r = sqrt(x**2+y**2+z**2)
        fr = exp1((r-r0)/r1)
        xz = sqrt(x**2+z**2)
        fxz = exp1((xz-r0)/r1)
        w = w1*fr
        rho(ix,iy,iz) = rho(ix,iy,iz) + rho1*(fr+eps)
        px(ix,iy,iz) = px(ix,iy,iz) + w*z/r0
        pz(ix,iy,iz) = pz(ix,iy,iz) - w*x/r0
      end do
    end do
  end do

  px = exp(xdn(alog(rho)))*xdn(px)
  py = exp(ydn(alog(rho)))*ydn(py)
  pz = exp(zdn(alog(rho)))*zdn(pz)
  e = rho*ee0

  if (do_mhd) then
    Bx = 0.
    By = 0.
    Bz = B0
  end if
END
