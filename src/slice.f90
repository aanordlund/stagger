! $Id: slice.f90,v 1.37 2016/11/09 20:31:29 aake Exp $
!***********************************************************************
MODULE slices
  implicit none
  integer, parameter:: mslice=20
  integer, dimension(mslice):: jdir, jx, jy, jz, jtmp
  integer ntslice, islice, jslice, islice0, nslice, nvar, var(10), m1, m2
  real(kind=4) tslice
  logical do_slice, do_vrms, verbose
END module

!***********************************************************************
SUBROUTINE init_slice
!
!  Main program for the stagger code
!
  USE params
  USE slices
  implicit none
  character(len=mid) :: id="$Id: slice.f90,v 1.37 2016/11/09 20:31:29 aake Exp $"

  call print_id (id)

  do_slice = .false.
  do_vrms = .false.
  ntslice = 10
  nslice  = 1
  tslice = 1.
  islice = 1
  islice0 = -1
  jdir = 3
  jx = (mx+1)/2
  jy = (my+1)/2
  jz = (mz+1)/2
  nvar = 1
  var = 0
  call read_slice

END

!***********************************************************************
SUBROUTINE read_slice
!
!  Main program for the stagger code
!
  USE params
  USE slices
  implicit none
  namelist /slice/do_slice,do_vrms,tslice,ntslice,nslice,jdir,jx,jy,jz,nvar,var,verbose
  character(len=mfile) :: name, fname

  rewind (stdin); read (stdin,slice)
  if (master.and.do_slice) write(stdout,slice)
  if (.not. do_slice) return

  fname = name('slice.dat','slice',file)
  call mpi_name (fname)
  open (slice_unit, file=fname, form='unformatted', status='unknown')
  write (slice_unit) mx,my,mz,tslice,nslice,nvar,mpi_x,mpi_y,mpi_z
  write (slice_unit) jdir,jx,jy,jz
  write (slice_unit) var
END

!***********************************************************************
SUBROUTINE check_slice (r,px,py,pz,e,d,Bx,By,Bz)
!
!  Main program for the stagger code
!
  USE params
  USE slices
  implicit none
  real, dimension(mx,my,mz):: r,px,py,pz,e,d,Bx,By,Bz
  integer incr
   
  if (.not. do_slice) return

  if (islice0 < 0) islice0=t/tslice

  if (tslice  > 0.) then                          ! save by time interval
    if (t > tslice *(islice +islice0)) then       ! slice dump every tslice
      if (verbose .and. master) print *,t,tslice,islice,islice0
      incr = 1+(t/tslice -(islice+islice0))       ! in case large dt
      islice  = islice+incr                       ! increment counter
      call write_slice (r,px,py,pz,e,d,Bx,By,Bz)  ! write slice shot
    end if
  else if (nslice  > 0) then                      ! save by time step
    if (mod(it,nslice ) .eq. 0) then              ! slice every nslice 
      call write_slice (r,px,py,pz,e,d,Bx,By,Bz)  ! write slice
    end if
  end if

END

!***********************************************************************
SUBROUTINE write_slice (r,px,py,pz,e,d,Bx,By,Bz)
!
  USE params
  USE slices
  implicit none
  real, dimension(mx,my,mz):: r,px,py,pz,e,d,Bx,By,Bz
  real, dimension(:,:,:), allocatable:: lnr,ux,uy,uz,scr,ee,tt
  real(kind=4), dimension(:,:), allocatable:: slice
  real(kind=4) t4
  integer ivar, jvar, iz, jp

  do jslice=1,nslice
  if      (jdir(jslice) == 1) then
    allocate (slice(my,mz))
    m1 = my
    m2 = mz
  else if (jdir(jslice) == 2) Then
    allocate (slice(mx,mz))
    m1 = mx
    m2 = mz
  else if (jdir(jslice) == 3) then
    allocate (slice(mx,my))
    m1 = mx
    m2 = my
  end if

  do ivar=1,nvar
    jvar = var(ivar)

    if      (jvar == 0) then
      call get_slice (r, slice)

    else if (jvar == 1) then
      allocate (lnr(mx,my,mz), scr(mx,my,mz), ux(mx,my,mz))
!$omp parallel private(iz)
      do iz=izs,ize
        lnr(:,:,iz) = alog(r(:,:,iz))
      end do
      call xdn_set (lnr, scr)
      do iz=izs,ize
        ux(:,:,iz) = px(:,:,iz)/exp(scr(:,:,iz))
      end do
      call xup_set (ux, scr)
!$omp end parallel
      if (do_vrms) then
        call get_vrms (scr, slice)
      else
        call get_slice (scr, slice)
      end if
      deallocate (lnr, scr, ux)

    else if (jvar == 2) then
      allocate (lnr(mx,my,mz), scr(mx,my,mz), uy(mx,my,mz))
!$omp parallel private(iz)
      do iz=izs,ize
        lnr(:,:,iz) = alog(r(:,:,iz))
      end do
      call ydn_set (lnr, scr)
      do iz=izs,ize
        uy(:,:,iz) = py(:,:,iz)/exp(scr(:,:,iz))
      end do
      call yup_set (uy, scr)
!$omp end parallel
      if (do_vrms) then
        call get_vrms (scr, slice)
      else
        call get_slice (scr, slice)
      end if
      deallocate (lnr, scr, uy)

    else if (jvar == 3) then
      allocate (lnr(mx,my,mz), scr(mx,my,mz), uz(mx,my,mz))
!$omp parallel private(iz)
      do iz=izs,ize
        lnr(:,:,iz) = alog(r(:,:,iz))
      end do
!$omp barrier
      call zdn_set (lnr, scr)
      do iz=izs,ize
        uz(:,:,iz) = pz(:,:,iz)/exp(scr(:,:,iz))
      end do
      call zup_set (uz, scr)
!$omp end parallel
      if (do_vrms) then
        call get_vrms (scr, slice)
      else
        call get_slice (scr, slice)
      end if
      deallocate (lnr, scr, uz)

    else if (jvar == 4) then
      call get_slice (e, slice)

    else if (jvar == 5) then
      call get_slice (d, slice)

    else if (jvar == 6) then
      call get_slice (bx, slice)

    else if (jvar == 7) then
      call get_slice (by, slice)

    else if (jvar == 8) then
      call get_slice (bz, slice)

    else if (jvar == 9) then
      allocate (ee(mx,my,mz),tt(mx,my,mz))
!$omp parallel private(iz)
      do iz=izs,ize
        ee(:,:,iz) = e(:,:,iz)/r(:,:,iz)
      end do
!$omp end parallel
      call temperature (r, ee, tt)
      if (sum(surface_int) .ne. 0.) then
         do iz=izs,ize
            tt(:,1,iz) = surface_int(:,iz)
         enddo
      endif
      call get_slice (tt, slice)
      deallocate (ee, tt)

    end if

    t4 = t
    if (master .and. verbose) then
      if      (jdir(jslice) == 1) then
        jp = jx(jslice)
      else if (jdir(jslice) == 2) then
        jp = jy(jslice)
      else if (jdir(jslice) == 3) then
        jp = jz(jslice)
      end if
      print *,'slice:',jdir(jslice),jp,jvar,t4
    end if
    write (slice_unit) t4,jdir(jslice),jvar,jp
    write (slice_unit) slice
  end do
  deallocate (slice)
  end do

  flush (slice_unit)
END

!***********************************************************************
SUBROUTINE get_slice (f, slice)
  USE params
  USE slices
  implicit none
  real, dimension(mx,my,mz):: f
  real(kind=4), dimension(m1,m2):: slice
  integer ix, iy, iz

  if      (jdir(jslice) == 1) then
    if (jx(jslice) > 0) then
      slice = f(jx(jslice),:,:)
    else
      slice = 0.
      do ix=1,mx
        slice = slice+f(ix,:,:)
      end do
      slice = slice/mx
    end if
  else if (jdir(jslice) == 2) then
    if (jy(jslice) > 0) then
      slice = f(:,jy(jslice),:)
    else
      slice = 0.
      do iy=1,my
        slice = slice+f(:,iy,:)
      end do
      slice = slice/my
    end if
  else if (jdir(jslice) == 3) then
    if (jz(jslice) > 0) then
      slice = f(:,:,jz(jslice))
    else
      slice = 0.
      do iz=1,mz
        slice = slice+f(:,:,iz)
      end do
      slice = slice/mz
    end if
  end if
END
!***********************************************************************
SUBROUTINE get_vrms (f, slice)
  USE params
  USE slices
  implicit none
  real, dimension(mx,my,mz):: f
  real(kind=4), dimension(m1,m2):: slice
  integer ix, iy, iz
  real(kind=4) vmean, vrms  

  if      (jdir(jslice) == 1) then
    if (jx(jslice) > 0) then
      slice = f(jx(jslice),:,:)
    else
      slice = 0.
      do iy=1,my
         do iz=1,mz
            vmean=sum(f(:,iy,iz))/mx
            vrms=sqrt(sum((f(:,iy,iz)-vmean)**2)/mx)
            slice(iy,iz)=vrms
         end do   
      end do    
    end if
  else if (jdir(jslice) == 2) then
    if (jy(jslice) > 0) then
      slice = f(:,jy(jslice),:)
    else
      slice = 0.
      do ix=1,mx
         do iz=1,mz
            vmean=sum(f(ix,:,iz))/my
            vrms=sqrt(sum((f(ix,:,iz)-vmean)**2)/my)
            slice(ix,iz)=vrms
         end do   
      end do    

    end if
  else if (jdir(jslice) == 3) then
    if (jz(jslice) > 0) then
      slice = f(:,:,jz(jslice))
    else
      slice = 0.
      do ix=1,mx
         do iy=1,my
            vmean=sum(f(ix,iy,:))/mz
            vrms=sqrt(sum((f(ix,iy,:)-vmean)**2)/mz)
            slice(ix,iy)=vrms
!            print *,"vrms,vmean,sum",vrms,vmean,sum((f(ix,iy,:)-vmean)**2)/mz
         end do   
      end do
   
    end if
  end if

END
