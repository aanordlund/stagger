!***********************************************************************
SUBROUTINE mhd (flag, &
            r,e,pg,Ux,Uy,Uz,Bx,By,Bz,Ex,Ey,Ez, &
            dpxdt,dpydt,dpzdt,dedt,dBxdt,dBydt,dBzdt,frho)
!-----------------------------------------------------------------------
!  To omptimize for speed, pointer assignments are done in this first
!  wrapper routines, which then passes all arrays on in argument lists.
!-----------------------------------------------------------------------
  USE params
  USE arrays, ONLY: &
            eta, fudge, &
            wk01,wk02,wk03, &
            wk06,wk10,wk11,wk12, &
            wk13,wk14,wk15,wk16,wk17,wk18,wk19,wk20, &
            scr1,scr2,scr3,scr4,scr5,scr6
  logical flag
  real, dimension(mx,my,mz):: &
            r,e,pg,Ux,Uy,Uz,Bx,By,Bz,Ex,Ey,Ez, &
            dpxdt,dpydt,dpzdt,dedt,dBxdt,dBydt,dBzdt
  real, dimension(my):: frho
  real, pointer, dimension(:,:,:) ::  &
                 Jx, Jy, Jz, Fx, Fy, Fz, &
                 Bx_y, Bx_z, By_x, By_z, Bz_x, Bz_y, &
                 Ux_y, Ux_z, Uy_x, Uy_z, Uz_x, Uz_y
!-----------------------------------------------------------------------
  Jx   => wk18; Jy   => wk19; Jz   => wk20
  Bx_y => wk10; By_z => wk11; Bz_x => wk12
  Bx_z => wk13; By_x => wk14; Bz_y => wk15
  eta  => wk16; fudge=> wk17
  Ux_y => scr1; Uy_z => scr2; Uz_x => scr3
  Ux_z => scr4; Uy_x => scr5; Uz_y => scr6
  Fx   => wk01; Fy   => wk02; Fz   => wk03
  
  call mhd1     (flag, mx, my, mz, &
                 r, e, pg, &
                 Jx, Jy, Jz, Ex, Ey, Ez, Fx, Fy, Fz, &
                 Bx_y, Bx_z, By_x, By_z, Bz_x, Bz_y, &
                 Ux_y, Ux_z, Uy_x, Uy_z, Uz_x, Uz_y, &
                 Ux, Uy, Uz, Bx, By, Bz, fudge, eta, wk06, &
                 dpxdt, dpydt, dpzdt, dBxdt, dBydt, dBzdt, &
                 dedt, scr1, scr2, scr3, scr4, scr5, scr6,frho)
END

!***********************************************************************
SUBROUTINE mhd1 (flag, mx, my, mz, &
                 r, e, pg, &
                 Jx, Jy, Jz, Ex, Ey, Ez, Fx, Fy, Fz, &
                 Bx_y, Bx_z, By_x, By_z, Bz_x, Bz_y, &
                 Ux_y, Ux_z, Uy_x, Uy_z, Uz_x, Uz_y, &
                 Ux, Uy, Uz, Bx, By, Bz, fudge, eta, etd, &
                 dpxdt, dpydt, dpzdt, dBxdt, dBydt, dBzdt, &
                 dedt, scr1, scr2, scr3, scr4, scr5, scr6,frho)
  USE params, mx_void=>mx, my_void=>my, mz_void=>mz
  USE arrays, ONLY: wk_L, qqja
  USE pde_mod, ONLY: do_fluxes, qbdry
  implicit none
  logical flag
  integer mx, my, mz
  real, dimension(mx,my,mz) ::  &
                 r, e, pg, &
                 Jx, Jy, Jz, Ex, Ey, Ez, Fx, Fy, Fz, &
                 Bx_y, Bx_z, By_x, By_z, Bz_x, Bz_y, &
                 Ux_y, Ux_z, Uy_x, Uy_z, Uz_x, Uz_y, &
                 Ux, Uy, Uz, Bx, By, Bz, fudge, eta, etd, &
                 dpxdt, dpydt, dpzdt, dBxdt, dBydt, dBzdt, &
                 dedt, scr1, scr2, scr3, scr4, scr5, scr6
  integer ix, iy, iz, j
  logical debug, foutput
  integer lb1, ub1
  real ds, c, Qsum, Ua_x, Ua_y, Ua_z, x_i
  real(kind=8) sumy
  real, dimension(my):: frho
  integer, save:: nprint_ambi=10
  character(len=mid):: id = '$Id: mhd_bob.f90 $'
!-----------------------------------------------------------------------
  call print_id (id)
  do_fluxes = foutput('fluxes.txt')

  call mfield_boundary (Ux,Uy,Uz,Bx,By,Bz)
                                                                        call timer('mhd','mf_bdry')
  call resist (flag, &                                                  ! uses wk06-wk15 internally
    eta,r,e,pg,fudge,Ux,Uy,Uz,Bx,By,Bz,frho)
    call dumpn(eta,'eta','mhd.dmp',1)
    call dumpn(etd,'etd','mhd.dmp',1)
                                                                        call timer('mhd','begin')
!-----------------------------------------------------------------------
!  Electric current I = curl(B), resistive E, and dissipation
!-----------------------------------------------------------------------

  call barrier_omp('mhd1')                                              ! barrier 1; By may have been modified
    call dumpn(Bx,'Bx','mhd.dmp',1)
    call dumpn(By,'By','mhd.dmp',1)
    call dumpn(Bz,'Bz','mhd.dmp',1)

  if (do_2nddiv) then
    call ddydn1_set (Bz,Jx) ; call ddzdn1_sub (By,Jx)
    call ddzdn1_set (Bx,Jy) ; call ddxdn1_sub (Bz,Jy)
    call ddxdn1_set (By,Jz) ; call ddydn1_sub (Bx,Jz)
  else
    call ddydn_set (Bz,Jx) ; call ddzdn_sub (By,Jx)
    call ddzdn_set (Bx,Jy) ; call ddxdn_sub (Bz,Jy)
    call ddxdn_set (By,Jz) ; call ddydn_sub (Bx,Jz)
  end if
    call dumpn(Jx,'Jx','mhd.dmp',1)
    call dumpn(Jy,'Jy','mhd.dmp',1)
    call dumpn(Jz,'Jz','mhd.dmp',1)

!-----------------------------------------------------------------------
!  Alfven speed limiter / fudge factor
!-----------------------------------------------------------------------
  if (cmax .ne. 0) then
    do iz=izs,ize
      Jx(:,:,iz) = Jx(:,:,iz)*fudge(:,:,iz)
      Jy(:,:,iz) = Jy(:,:,iz)*fudge(:,:,iz)
      Jz(:,:,iz) = Jz(:,:,iz)*fudge(:,:,iz)
    end do
  end if
                                                                        call timer('mhd','current')
!-----------------------------------------------------------------------
!  Resistive part of Ex
!-----------------------------------------------------------------------
  call barrier_omp('mhd2')                                              ! barrier 2;  NOT NEEDED?
  call zdn1_set (eta, scr1) ; call ydn1_set (scr1, scr2)
  call zdn1_set (etd, scr3) ; call ydn1_set (scr3, scr4)

  call ddydn_set (Bz,scr5)
  do iz=izs,ize; do iy=1,my
    ds = 0.5*(dym(iy)+dzm(iz))
    if (do_aniso_eta) ds = dym(iy)
    do ix=1,mx
      Ex(ix,iy,iz) = ds*(scr2(ix,iy,iz)+ds*scr4(ix,iy,iz))*scr5(ix,iy,iz)
    end do
  end do; end do
    call dumpn(scr2,'etaydn','mhd.dmp',1)
    call dumpn(scr4,'etdydn','mhd.dmp',1)
    call dumpn(scr5,'dBzdydn','mhd.dmp',1)
    call dumpn(Ex,'Ex_res','mhd.dmp',1)

  call ddzdn_set (By,scr5)
  do iz=izs,ize; do iy=1,my
    ds = 0.5*(dym(iy)+dzm(iz))
    if (do_aniso_eta) ds = dzm(iz)
    do ix=1,mx
      Ex(ix,iy,iz) = Ex(ix,iy,iz) - ds*(scr2(ix,iy,iz)+ds*scr4(ix,iy,iz))*scr5(ix,iy,iz)
    end do
  end do; end do
    call dumpn(scr5,'dBydzdn','mhd.dmp',1)
    call dumpn(Ex,'Ex_res2','mhd.dmp',1)
  if (do_quench) then
    call barrier_omp('Ex')
    do iz=izs,ize                                          ! store unquenched bdry values
      do iy=1,2*(my-ub)
        qbdry(:,iy,iz)=Ex(:,ub-(my-ub)+iy,iz)
      end do
    end do
    call quenchy2 (Ex, scr4)
    call quenchz2 (Ex, scr5)
    do iz=izs,ize
      Ex(:,:,iz) = 0.5*(scr4(:,:,iz)+scr5(:,:,iz))
    end do
    do iz=izs,ize                                          ! restore unquenched bdry values
      Ex(:,ub-(my-ub)+1:my,iz)=qbdry(:,1:2*(my-ub),iz)
    end do
    call dumpn(Ex,'Ex>quench','mhd.dmp',1)
  endif

!-----------------------------------------------------------------------
!  Resistive part of Ey
!-----------------------------------------------------------------------
  call xdn1_set (scr1, scr2)
  call xdn1_set (scr3, scr4)

  call ddzdn_set (Bx,scr5)
  if (do_aniso_eta) then
    do iz=izs,ize; do iy=1,my; do ix=1,mx
      ds = dzm(iz)
      Ey(ix,iy,iz) = ds*(scr2(ix,iy,iz)+ds*scr4(ix,iy,iz))*scr5(ix,iy,iz)
    end do; end do; end do
  else
    do iz=izs,ize; do iy=1,my; do ix=1,mx
      ds = 0.5*(dzm(iz)+dxm(ix))
      Ey(ix,iy,iz) = ds*(scr2(ix,iy,iz)+ds*scr4(ix,iy,iz))*scr5(ix,iy,iz)
    end do; end do; end do
  end if

  call ddxdn_set (Bz,scr5)
  if (do_aniso_eta) then
    do iz=izs,ize; do iy=1,my; do ix=1,mx
      ds = dxm(ix)
      Ey(ix,iy,iz) = Ey(ix,iy,iz) - ds*(scr2(ix,iy,iz)+ds*scr4(ix,iy,iz))*scr5(ix,iy,iz)
    end do; end do; end do
  else
    do iz=izs,ize; do iy=1,my; do ix=1,mx
      ds = 0.5*(dzm(iz)+dxm(ix))
      Ey(ix,iy,iz) = Ey(ix,iy,iz) - ds*(scr2(ix,iy,iz)+ds*scr4(ix,iy,iz))*scr5(ix,iy,iz)
    end do; end do; end do
  end if
  if (do_quench) then
    call barrier_mpi('Ey')
    call dumpn(Ey,'Ey<quench','mhd.dmp',1)
    do iz=izs,ize                                          ! store unquenched bdry values
      do iy=1,2*(my-ub)
        qbdry(:,iy,iz)=Ey(:,ub-(my-ub)+iy,iz)
      end do
    end do
    call quenchx2 (Ey, scr4)
    call quenchz2 (Ey, scr5)
    do iz=izs,ize
      Ey(:,:,iz) = 0.5*(scr4(:,:,iz)+scr5(:,:,iz))
    end do
    do iz=izs,ize                                          ! restore unquenched bdry values
      Ey(:,ub-(my-ub)+1:my,iz)=qbdry(:,1:2*(my-ub),iz)
    end do
    call dumpn(Ey,'Ey>quench','mhd.dmp',1)
  endif

!-----------------------------------------------------------------------
!  Resistive part of Ez
!-----------------------------------------------------------------------
  call ydn1_set (eta, scr1) ; call xdn1_set (scr1, scr2)
  call ydn1_set (etd, scr3) ; call xdn1_set (scr3, scr4)

  call ddxdn_set (By,scr5)
  if (do_aniso_eta) then
    do iz=izs,ize; do iy=1,my; do ix=1,mx
      ds = dxm(ix)
      Ez(ix,iy,iz) = ds*(scr2(ix,iy,iz)+ds*scr4(ix,iy,iz))*scr5(ix,iy,iz)
    end do; end do; end do
  else
    do iz=izs,ize; do iy=1,my; do ix=1,mx
      ds = 0.5*(dxm(ix)+dym(iy))
      Ez(ix,iy,iz) = ds*(scr2(ix,iy,iz)+ds*scr4(ix,iy,iz))*scr5(ix,iy,iz)
    end do; end do; end do
  end if
    call dumpn(scr2,'etaydn','mhd.dmp',1)
    call dumpn(scr4,'etdydn','mhd.dmp',1)
    call dumpn(scr5,'dBydxdn','mhd.dmp',1)
    call dumpn(Ez,'Ez_res','mhd.dmp',1)


  call ddydn_set (Bx,scr5)
  if (do_aniso_eta) then
    do iz=izs,ize; do iy=1,my; do ix=1,mx
      ds = dym(iy)
      Ez(ix,iy,iz) = Ez(ix,iy,iz) - ds*(scr2(ix,iy,iz)+ds*scr4(ix,iy,iz))*scr5(ix,iy,iz)
    end do; end do; end do
  else
    do iz=izs,ize; do iy=1,my; do ix=1,mx
      ds = 0.5*(dxm(ix)+dym(iy))
      Ez(ix,iy,iz) = Ez(ix,iy,iz) - ds*(scr2(ix,iy,iz)+ds*scr4(ix,iy,iz))*scr5(ix,iy,iz)
    end do; end do; end do
  end if
    call dumpn(scr5,'dBxdydn','mhd.dmp',1)
    call dumpn(Ez,'Ez_res2','mhd.dmp',1)
  if (do_quench) then
    do iz=izs,ize                                          ! store unquenched bdry values
      do iy=1,2*(my-ub)
        qbdry(:,iy,iz)=Ez(:,ub-(my-ub)+iy,iz)
      end do
    end do
    call quenchx2 (Ez, scr4)
    call quenchy2 (Ez, scr5)
    do iz=izs,ize
      Ez(:,:,iz) = 0.5*(scr4(:,:,iz)+scr5(:,:,iz))
    end do
    do iz=izs,ize                                          ! restore unquenched bdry values
      Ez(:,ub-(my-ub)+1:my,iz)=qbdry(:,1:2*(my-ub),iz)
    end do
  endif
    call dumpn(Ex,'Ex>res','mhd.dmp',1)
    call dumpn(Ey,'Ey>res','mhd.dmp',1)
    call dumpn(Ez,'Ez>res','mhd.dmp',1)
  call ecurrent_boundary (Ex, Ey, Ez, Jx, Jy, Jz)
                                                                        call timer('mhd','E_resist')
!-----------------------------------------------------------------------
!  Joule dissipation
!-----------------------------------------------------------------------
  if (do_energy .and. do_dissipation) then
      call dumpn(dedt,'dedt<Qjoule','mhd.dmp',1)
    do iz=izs,ize
      scr3(:,:,iz) = Jx(:,:,iz)*Ex(:,:,iz)
    end do
    call barrier_omp('mhd5')                                            ! barrier 5; scr3 just computed
    call zup1_set (scr3, scr1) ; call yup1_set (scr1 , scr2)
    call barrier_omp('mhd5a')                                           ! barrier 5a; make sure scr3 is not changed
      call dumpn(scr3,'Jx*Ex','mhd.dmp',1)

    do iz=izs,ize
      scr3(:,:,iz) = Jy(:,:,iz)*Ey(:,:,iz)
    end do
    call xup1_set (scr3, scr1)
    call barrier_omp('mhd6')                                            ! barrier 6; make sure scr3 is not changed
    call zup1_add (scr1, scr2)
      call dumpn(scr3,'Jy*Ey','mhd.dmp',1)

    do iz=izs,ize
      scr3(:,:,iz) = Jz(:,:,iz)*Ez(:,:,iz)
    end do
    call barrier_omp('mhd6a')                                           ! barrier 6a; make sure scr3 is not changed
    call yup1_set (scr3, scr1) ; call xup1_add (scr1 , scr2)
    lb1 = max(min(lb,my),1)
    ub1 = max(min(ub,my),1)
      call dumpn(scr3,'Jz*Ez','mhd.dmp',1)
    do iz=izs,ize
      dedt(:,lb1:ub1,iz) = dedt(:,lb1:ub1,iz) + scr2(:,lb1:ub1,iz)
    end do
      call dumpn(scr2,'Qjoule','mhd.dmp',1)
      call dumpn(dedt,'dedt>Qjoule','mhd.dmp',1)

    if (do_fluxes) then
      call haverage_subr (scr2, qqja)
      call dumpn(scr2,'qqja','mhdfluxes.dmp',1)
    end if
  end if
    call dumpn(Ex,'Ex>joule','mhd.dmp',1)
    call dumpn(Ey,'Ey>joule','mhd.dmp',1)
    call dumpn(Ez,'Ez>joule','mhd.dmp',1)
                                                                        call timer('mhd','Q_joule')
!-----------------------------------------------------------------------
!  Lorenz force = I x B
!-----------------------------------------------------------------------
  if (do_2nddiv) then
    call ydn1_set (Bx,Bx_y) ; call zdn1_set (Bx,Bx_z)
    call zdn1_set (By,By_z) ; call xdn1_set (By,By_x)
    call xdn1_set (Bz,Bz_x) ; call ydn1_set (Bz,Bz_y)
    call smooth (Bx_y, 3)  ; call smooth (Bx_z, 2)
    call smooth (By_z, 1)  ; call smooth (By_x, 3)
    call smooth (Bz_x, 2)  ; call smooth (Bz_y, 1)
  else
    call ydn_set (Bx,Bx_y) ; call zdn_set (Bx,Bx_z)
    call zdn_set (By,By_z) ; call xdn_set (By,By_x)
    call xdn_set (Bz,Bz_x) ; call ydn_set (Bz,Bz_y)
    call smooth (Bx_y, 3)  ; call smooth (Bx_z, 2)
    call smooth (By_z, 1)  ; call smooth (By_x, 3)
    call smooth (Bz_x, 2)  ; call smooth (Bz_y, 1)
  end if
  do iz=izs,ize
    scr1(:,:,iz) = Jy(:,:,iz)*Bz_x(:,:,iz)
  end do
  call barrier_omp('mhd7')                                              ! barrier 7; scr1 just computed
  if (do_2nddiv) then
    call zup1_set (scr1, Fx)
  else
    call zup_set (scr1, Fx)
  end if
  do iz=izs,ize
    scr2(:,:,iz) = Jz(:,:,iz)*By_x(:,:,iz)
  end do
  if (do_2nddiv) then
    call yup1_sub (scr2, Fx)
  else
    call yup_sub (scr2, Fx)
  end if
    call dumpn(Jy,'Jy','mhd.dmp',1)
    call dumpn(Bz_x,'Bz_x','mhd.dmp',1)
    call dumpn(Jz,'Jz','mhd.dmp',1)
    call dumpn(By_x,'By_x','mhd.dmp',1)
    call dumpn(Fx,'Fx','mhd.dmp',1)
  do iz=izs,ize
    scr2(:,:,iz) = Jz(:,:,iz)*Bx_y(:,:,iz)
  end do
  if (do_2nddiv) then
    call xup1_set (scr2, Fy)
  else
    call xup_set (scr2, Fy)
  end if
  do iz=izs,ize
    scr3(:,:,iz) = Jx(:,:,iz)*Bz_y(:,:,iz)
  end do
  call barrier_omp('mhd8')                                              ! barrier 8; scr3 just computed
  if (do_2nddiv) then
    call zup1_sub (scr3, Fy)
  else
    call zup_sub (scr3, Fy)
  end if
    call dumpn(Jx,'Jx','mhd.dmp',1)
    call dumpn(Bx_y,'Bz_y','mhd.dmp',1)
    call dumpn(Jz,'Jz','mhd.dmp',1)
    call dumpn(Bz_y,'Bz_y','mhd.dmp',1)
    call dumpn(Fy,'Fy','mhd.dmp',1)
  do iz=izs,ize
    scr1(:,:,iz) = Jx(:,:,iz)*By_z(:,:,iz)
  end do
  if (do_2nddiv) then
    call yup1_set (scr1, Fz)
  else
    call yup_set (scr1, Fz)
  end if
  do iz=izs,ize
    scr1(:,:,iz) = Jy(:,:,iz)*Bx_z(:,:,iz)
  end do
  if (do_2nddiv) then
    call xup1_sub (scr1, Fz)
  else
    call xup_sub (scr1, Fz)
  end if
    call dumpn(Jx,'Jx','mhd.dmp',1)
    call dumpn(By_z,'By_z','mhd.dmp',1)
    call dumpn(Jy,'Jy','mhd.dmp',1)
    call dumpn(Bx_z,'Bx_z','mhd.dmp',1)
    call dumpn(Fz,'Fz','mhd.dmp',1)
  do iz=izs,ize
    dpxdt(:,:,iz) = dpxdt(:,:,iz) + Fx(:,:,iz)
    dpydt(:,:,iz) = dpydt(:,:,iz) + Fy(:,:,iz)
    dpzdt(:,:,iz) = dpzdt(:,:,iz) + Fz(:,:,iz)
  end do
  if (do_fluxes) then
    do iz=izs,ize
      scr1(:,:,iz) = Fx(:,:,iz)*ux(:,:,iz)
      scr2(:,:,iz) = Fy(:,:,iz)*uy(:,:,iz)
      scr3(:,:,iz) = Fz(:,:,iz)*uz(:,:,iz)
    end do
    call xup1_set (scr1, scr4)
    call yup1_add (scr2, scr4)
    call zup1_add (scr3, scr4)
    call haverage_subr (scr4, wk_L)
    call dumpn(scr4,'wk_L','mhdfluxes.dmp',1)
  end if
  call barrier_omp('mhd9')                                              ! barrier 9; NOT NEEDED??

                                                                        call timer('mhd','Lorentz')
!-----------------------------------------------------------------------
!  Ambipolar diffusion
!-----------------------------------------------------------------------
  if (ambi .ne. 0.) then
    do iz=izs,ize
      do iy=1,my
      do ix=1,mx
        scratch(ix,iy,iz) = max(Ux(ix,iy,iz),Uy(ix,iy,iz),Uz(ix,iy,iz)) ! max absolute velocity
      end do
      end do
    end do
    !call fmaxval_subr ('Umax',scratch,Umax)                             ! MPI-safe global max
    Umax = maxval(scratch); call max_real_mpi (Umax)
    if (ambi > 0) then
      do iz=izs,ize
        do iy=1,my
        do ix=1,mx
          ds = max(sx,sy,sz)
          c  = ambi*ds/(r(ix,iy,iz)*sqrt(r(ix,iy,iz)))
          Fx(ix,iy,iz) = Fx(ix,iy,iz)*c
          Fy(ix,iy,iz) = Fy(ix,iy,iz)*c
          Fz(ix,iy,iz) = Fz(ix,iy,iz)*c
          Fx(ix,iy,iz) = Fx(ix,iy,iz)*Umax/(Umax+abs(Fx(ix,iy,iz)))
          Fy(ix,iy,iz) = Fy(ix,iy,iz)*Umax/(Umax+abs(Fy(ix,iy,iz)))
          Fz(ix,iy,iz) = Fz(ix,iy,iz)*Umax/(Umax+abs(Fz(ix,iy,iz)))
        end do
        end do
      end do
    else
      do iz=izs,ize
        do iy=1,my
        do ix=1,mx
          x_i = 1.43/r(ix,iy,iz)*exp(-1.5*r(ix,iy,iz)**0.15)
          if (r(ix,iy,iz) < 0.39) x_i = 1.
          ds = max(sx,sy,sz)
          c  = abs(ambi)*ds/(x_i*r(ix,iy,iz)**2)
          Fx(ix,iy,iz) = Fx(ix,iy,iz)*c
          Fy(ix,iy,iz) = Fy(ix,iy,iz)*c
          Fz(ix,iy,iz) = Fz(ix,iy,iz)*c
          Fx(ix,iy,iz) = Fx(ix,iy,iz)*Umax/(Umax+abs(Fx(ix,iy,iz)))
          Fy(ix,iy,iz) = Fy(ix,iy,iz)*Umax/(Umax+abs(Fy(ix,iy,iz)))
          Fz(ix,iy,iz) = Fz(ix,iy,iz)*Umax/(Umax+abs(Fz(ix,iy,iz)))
        end do
        end do
      end do
    end if
    call dumps(Fx,'vADx','ambi.dat')
    call dumps(Fy,'vADy','ambi.dat')
    call dumps(Fz,'vADz','ambi.dat')
    do iz=izs,ize
      do iy=1,my
      do ix=1,mx
        Fx(ix,iy,iz) = Ux(ix,iy,iz) + Fx(ix,iy,iz)
        Fy(ix,iy,iz) = Uy(ix,iy,iz) + Fy(ix,iy,iz)
        Fz(ix,iy,iz) = Uz(ix,iy,iz) + Fz(ix,iy,iz)
      end do
      end do
    end do

    call barrier_omp('ambi1')
    do iz=izs,ize
      scratch(:,:,iz) = abs(Fx(:,:,iz) - Ux(:,:,iz))
    end do
    call fmaxval_subr ('Uambi_x',scratch,Umax)                             ! MPI-safe global max
    if (master) Ua_x = Umax
    do iz=izs,ize
      scratch(:,:,iz) = abs(Fy(:,:,iz) - Uy(:,:,iz))
    end do
    call fmaxval_subr ('Uambi_y',scratch,Umax)                             ! MPI-safe global max
    if (master) Ua_y = Umax
    do iz=izs,ize
      scratch(:,:,iz) = abs(Fz(:,:,iz) - Uz(:,:,iz))
    end do
    call fmaxval_subr ('Uambi_z',scratch,Umax)                             ! MPI-safe global max
    if (master.and.mod(it,nprint_ambi)==0.and.isubstep==1) &
      print*,'max(U_ambi):',Ua_x,Ua_y,Umax

    call barrier_omp('ambi2')
    call zdn_set (Fy,Uy_z) ; call ydn_set (Fz,Uz_y)
    call xdn_set (Fz,Uz_x) ; call zdn_set (Fx,Ux_z)
    call ydn_set (Fx,Ux_y) ; call xdn_set (Fy,Uy_x)
      call dumpn(Fx,'Ux_ambi','mhd.dmp',1)
      call dumpn(Fy,'Uy_ambi','mhd.dmp',1)
      call dumpn(Fz,'Uz_ambi','mhd.dmp',1)
  else
    call zdn1_set (Uy,Uy_z) ; call ydn1_set (Uz,Uz_y)
    call xdn1_set (Uz,Uz_x) ; call zdn1_set (Ux,Ux_z)
    call ydn1_set (Ux,Ux_y) ; call xdn1_set (Uy,Uy_x)
  end if
    call dumpn(Ux,'Ux','mhd.dmp',1)
    call dumpn(Uy,'Uy','mhd.dmp',1)
    call dumpn(Uz,'Uz','mhd.dmp',1)

!-----------------------------------------------------------------------
!  Advective part of the electric field;  E -> E - UxB
!-----------------------------------------------------------------------
  do iz=izs,ize
    Ex(:,:,iz) = Ex(:,:,iz) - Uy_z(:,:,iz)*Bz_y(:,:,iz) + Uz_y(:,:,iz)*By_z(:,:,iz)
    Ey(:,:,iz) = Ey(:,:,iz) - Uz_x(:,:,iz)*Bx_z(:,:,iz) + Ux_z(:,:,iz)*Bz_x(:,:,iz)
    Ez(:,:,iz) = Ez(:,:,iz) - Ux_y(:,:,iz)*By_x(:,:,iz) + Uy_x(:,:,iz)*Bx_y(:,:,iz)
  end do
    call dumpn(Ex,'Ex>UxB','mhd.dmp',1)
    call dumpn(Ey,'Ey>UxB','mhd.dmp',1)
    call dumpn(Ez,'Ez>UxB','mhd.dmp',1)
                                                                        call timer('mhd','E_advect')
!-----------------------------------------------------------------------
!  E-field boundary conditions
!-----------------------------------------------------------------------
  call efield_boundary (Ex, Ey, Ez, Bx_y, Bx_z, By_z, By_x, Bz_x, Bz_y, &
                        Bx, By, Bz, Ux_y, Ux_z, Uy_z, Uy_x, Uz_x, Uz_y, &
                        Ux, Uy, Uz)
                                                                        call timer('mhd','ef_bdry')
!-----------------------------------------------------------------------
!  Magnetic field's time derivative, dBdt = - curl(E)
!-----------------------------------------------------------------------
  call barrier_omp('mhd10')                                             ! barrier 10; Ex, Ey just computed
    call dumpn(Ex,'Ex>bdry','mhd.dmp',1)
    call dumpn(Ey,'Ey>bdry','mhd.dmp',1)
    call dumpn(Ez,'Ez>bdry','mhd.dmp',1)
  if (do_2nddiv) then
    call ddzup1_add (Ey, dBxdt) ; call ddyup1_sub (Ez, dBxdt)
    call ddxup1_add (Ez, dBydt) ; call ddzup1_sub (Ex, dBydt)
    call ddyup1_add (Ex, dBzdt) ; call ddxup1_sub (Ey, dBzdt)
  else
    call ddzup_add (Ey, dBxdt) ; call ddyup_sub (Ez, dBxdt)
    call ddxup_add (Ez, dBydt) ; call ddzup_sub (Ex, dBydt)
    call ddyup_add (Ex, dBzdt) ; call ddxup_sub (Ey, dBzdt)
  end if
  if (do_fluxes) then
    call Poynting_flux (Ex,Ey,Ez,Bx,By,Bz,scr1,scr2,scr3,scr4,scr5,scr6)
  end if
                                                                        call timer('mhd','induct')
!-----------------------------------------------------------------------
!  Fictive magnetic field growth, e.g. to slowly increase AR fields
!-----------------------------------------------------------------------
  if (t_Bgrowth.ne.0.) then
    do iz=izs,ize
      dBxdt(:,:,iz) = dBxdt(:,:,iz) + (1./t_Bgrowth)*Bx(:,:,iz)
      dBydt(:,:,iz) = dBydt(:,:,iz) + (1./t_Bgrowth)*By(:,:,iz)
      dBzdt(:,:,iz) = dBzdt(:,:,iz) + (1./t_Bgrowth)*Bz(:,:,iz)
    end do
                                                                        call timer('mhd','B-growth')
  end if
    call dumpn(dBxdt,'dBxdt','mhd.dmp',1)
    call dumpn(dBydt,'dBydt','mhd.dmp',1)
    call dumpn(dBzdt,'dBzdt','mhd.dmp',1)

  call divB_diffuse
END
