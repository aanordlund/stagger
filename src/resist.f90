! $Id: resist.f90,v 1.41 2006/05/05 02:54:09 aake Exp $
!-----------------------------------------------------------------------
SUBROUTINE resist (eta,fudge,r,ee,Ux,Uy,Uz,Bx,By,Bz,flag)

  USE params

  implicit none

  integer ix, iy, iz
  logical flag
  real gg1, cx, cy, cz, cmax2, fmaxval
  real, dimension(mx,my,mz) :: &
       eta,fudge,r,ee,Ux,Uy,Uz,Bx,By,Bz
  real, allocatable, dimension(:,:,:) :: &
       Uxc,Uyc,Uzc,Bxc,Byc,Bzc,BB,UdB,cA
  integer j
  real elapsed, tm(30)
  character(len=mid):: id='$Id: resist.f90,v 1.41 2006/05/05 02:54:09 aake Exp $'

  call print_id (id)
  if (idbg>1) then; j=1;tm(j)=elapsed(); end if


!-----------------------------------------------------------------------
!  Centered fields
!-----------------------------------------------------------------------
  allocate (Bxc(mx,my,mz), Byc(mx,my,mz), Bzc(mx,my,mz), BB(mx,my,mz))
  call xup_set(Bx,Bxc)
  call yup_set(By,Byc)
  call zup_set(Bz,Bzc)
  allocate (Uxc(mx,my,mz), Uyc(mx,my,mz), Uzc(mx,my,mz))
  call xup_set(Ux,Uxc)
  call yup_set(Uy,Uyc)
  call zup_set(Uz,Uzc)
  allocate (cA(mx,my,mz))

  if (idbg>1) then; j=j+1; tm(j)=elapsed(); print '(a16,2f7.3)','resist: center ',tm(j)-tm(j-1),tm(j)-tm(1); endif

!-----------------------------------------------------------------------
!  Alfven speed
!-----------------------------------------------------------------------
  if (do_trace) print *,'Alfven speed'
  gg1 = gamma*(gamma-1)
  if (gamma .eq. 1.) gg1=1.
  if (cmax .eq. 0.) then
!$omp parallel do private(iz)
    do iz=1,mz
      BB(:,:,iz) = max(Bxc(:,:,iz)**2 + Byc(:,:,iz)**2 + Bzc(:,:,iz)**2, 1.e-20)
      cA(:,:,iz) = sqrt(gg1*ee(:,:,iz)+BB(:,:,iz)/r(:,:,iz))
      cA(:,:,iz) = max((cA(:,:,iz)+abs(Uxc(:,:,iz))), &
                       (cA(:,:,iz)+abs(Uyc(:,:,iz))), &
                       (cA(:,:,iz)+abs(Uzc(:,:,iz))))
    end do
  else
    cmax2 = cmax**2
!$omp parallel do private(iz)
    do iz=1,mz
      BB(:,:,iz) = max(Bxc(:,:,iz)**2 + Byc(:,:,iz)**2 + Bzc(:,:,iz)**2, 1.e-20)
      cA(:,:,iz) = r(:,:,iz)/(gg1*r(:,:,iz)*ee(:,:,iz)+BB(:,:,iz))
      fudge(:,:,iz) = min(1.,cmax2*cA(:,:,iz))
      cA(:,:,iz) = min(cmax,1./sqrt(cA(:,:,iz)))
      cA(:,:,iz) = max((cA(:,:,iz)+abs(Uxc(:,:,iz))), &
                       (cA(:,:,iz)+abs(Uyc(:,:,iz))), &
                       (cA(:,:,iz)+abs(Uzc(:,:,iz))))
    end do
  end if
  nflop = nflop+18

  if (ub < my) then
    !$omp parallel do private(iz)
    do iz=1,mz
      pbub(:,iz) = 0.5*(BB(:,ub,iz) - 1e-20)
    end do
  end if

  if (idbg>1) then; j=j+1; tm(j)=elapsed(); print '(a16,2f7.3)','resist: A-speed',tm(j)-tm(j-1),tm(j)-tm(1); endif

!-----------------------------------------------------------------------
!  Perpendicular velocity convergence
!-----------------------------------------------------------------------
  if (do_trace) print *,'U perp B'
  if (nu2 .gt. 0.) then
    allocate (UdB(mx,my,mz))
!$omp parallel do private(iz)
    do iz=1,mz
      BB(:,:,iz) = 1./sqrt(BB(:,:,iz))
      Bxc(:,:,iz) = Bxc(:,:,iz)*BB(:,:,iz)
      Byc(:,:,iz) = Byc(:,:,iz)*BB(:,:,iz)
      Bzc(:,:,iz) = Bzc(:,:,iz)*BB(:,:,iz)
      UdB(:,:,iz) = Uxc(:,:,iz)*Bxc(:,:,iz) &
                  + Uyc(:,:,iz)*Byc(:,:,iz) &
                  + Uzc(:,:,iz)*Bzc(:,:,iz)
      Uxc(:,:,iz) = Uxc(:,:,iz)-Bxc(:,:,iz)*UdB(:,:,iz)
      Uyc(:,:,iz) = Uyc(:,:,iz)-Byc(:,:,iz)*UdB(:,:,iz)
      Uzc(:,:,iz) = Uzc(:,:,iz)-Bzc(:,:,iz)*UdB(:,:,iz)
    end do
    nflop = nflop+9
    call dif2_set (Uxc, Uyc, Uzc, UdB)
  end if
  deallocate (BB, Bxc, Byc, Bzc)
  deallocate (Uxc, Uyc, Uzc)

  if (idbg>1) then; j=j+1; tm(j)=elapsed(); print '(a16,2f7.3)','resist: perp   ',tm(j)-tm(j-1),tm(j)-tm(1); endif

!-----------------------------------------------------------------------
!  Combine the contributions, and evaluate the time step constraints
!  from advection and diffusion
!-----------------------------------------------------------------------
  if (do_trace) print *,'eta, cA'
  if (nu2 .gt. 0.) then
!$omp parallel do private(iz)
    do iz=1,mz
    do iy=1,my
    do ix=1,mx
      eta(ix,iy,iz) = (nuB*nu3)*cA(ix,iy,iz) + (nuB*nu2*max(dxm(ix),dym(iy),dzm(iz)))*max(UdB(ix,iy,iz),1e-30)
    end do
    end do
    end do
    nflop = nflop+4
    call dump (UdB,'eta.dat',2)
    deallocate (UdB)
  else
!$omp parallel do private(iz)
    do iz=1,mz
      eta(:,:,iz) = (nuB*nu3)*cA(:,:,iz)
    end do
    nflop = nflop+1
  end if

  if (idbg>1) then; j=j+1; tm(j)=elapsed(); print '(a16,2f7.3)','resist: combine',tm(j)-tm(j-1),tm(j)-tm(1); endif

!$omp parallel do private(iz)
  do iz=1,mz
  do iy=1,my
  do ix=1,mx
    cA(ix,iy,iz) = cA(ix,iy,iz)*(dt/min(dxm(ix),dym(iy),dzm(iz)))
  end do
  end do
  end do
  if (flag) Cb = fmaxval('Cb',cA)
!$omp parallel do private(iz)
  do iz=1,mz
  do iy=1,my
  do ix=1,mx
    cA(ix,iy,iz) = eta(ix,iy,iz)*(6.2*3.*0.75/2.*dt/min(dxm(ix),dym(iy),dzm(iz)))
  end do
  end do
  end do
  if (flag) Cn = fmaxval('Cn',cA)
  nflop = nflop+2

  if (idbg>1) then; j=j+1; tm(j)=elapsed(); print '(a16,2f7.3)','resist: maxval ',tm(j)-tm(j-1),tm(j)-tm(1); endif

!-----------------------------------------------------------------------
!  Smooth over 3x3x3 point, to broaden influence.  Use low order shifts
!  when using eta in mhd, to ensure positive definite values.
!-----------------------------------------------------------------------
  call regularize(eta)
  call max3_set(eta,cA)
  call smooth3_set(cA,eta)
  call regularize(eta)
  call dump(eta,'eta.dat',1)
  deallocate (cA)

  if (idbg>1) then; j=j+1; tm(j)=elapsed(); print '(a16,2f7.3)','resist: smooth ',tm(j)-tm(j-1),tm(j)-tm(1); endif

END
