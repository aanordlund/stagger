! $Id: resist_bob.f90,v 1.48 2016/04/25 12:49:07 remo Exp $
!***********************************************************************
SUBROUTINE resist (flag, &
    eta,r,e,pg,fudge,Ux,Uy,Uz,Bx,By,Bz,frho)
!-----------------------------------------------------------------------
!  To omptimize for speed, pointer assignments are done in this first
!  wrapper routines, which then passes all arrays on in argument lists.
!-----------------------------------------------------------------------
  USE params
  USE arrays, ONLY: wk04,wk05,wk06,wk07,wk08,wk09,wk10,wk11,wk12,wk13,wk14, &
                    wk15,wk16,wk17,wk21,scr1,scr2
  USE boundary, ONLY: do_eta,eta_scl,eta_width
  implicit none
  logical flag
  real, dimension(mx,my,mz):: &
    eta,r,e,pg,fudge,Ux,Uy,Uz,Bx,By,Bz
  real, dimension(my):: frho
!-----------------------------------------------------------------------
  !Uxc => wk07; Uyc => wk08; Uzc => wk09
  !Bxc => wk10; Byc => wk11; Bzc => wk12
  !cA  => wk13; UdB => wk14; BB  => wk15
  call resist1 (flag,mx,my,mz, &
                eta,r,e,pg,fudge,Ux,Uy,Uz,Bx,By,Bz, &
                wk05,wk07,wk08,wk09,wk10,wk11,wk12,wk13,wk14,wk15,wk06,wk21,scr1,scr2,frho)
END

!***********************************************************************
SUBROUTINE resist1 (flag,mx,my,mz, &
                    eta,r,e,p,fudge,Ux,Uy,Uz,Bx,By,Bz, &
                    ee,Bxc,Byc,Bzc,Uxc,Uyc,Uzc,cA,UdB,BB,etd,d2lnr,scr1,scr2,frho)
  USE params, mx_void=>mx, my_void=>my, mz_void=>mz
  USE quench_m, only: qlim
  USE arrays, only: emag
  USE boundary, ONLY: do_eta,eta_scl,eta_width
  implicit none
  integer mx,my,mz
  logical flag
  real, dimension(mx,my,mz):: &
                    eta,r,e,p,fudge,Ux,Uy,Uz,Bx,By,Bz, &
                    ee,Bxc,Byc,Bzc,Uxc,Uyc,Uzc,cA,UdB,BB,etd,d2lnr,scr1,scr2
  real, dimension(my):: frho
  integer ix,iy,iz
  real gg1,cmax2,fCv,Ceta,dsmin,dsmax,average,f
  logical omp_in_parallel,debug
  character(len=mid):: id='$Id: resist_bob.f90,v 1.48 2016/04/25 12:49:07 remo Exp $'
!-----------------------------------------------------------------------
  call print_id (id)
                                                                        call timer('mhd','begin')
!-----------------------------------------------------------------------
!  Centered fields
!-----------------------------------------------------------------------
  gg1 = gamma*(gamma-1)
  if (gamma .eq. 1.) gg1=1.
  cmax2 = cmax**2

    call dumpn(Bx,'Bx','res.dmp',1)
    call dumpn(By,'By','res.dmp',1)
    call dumpn(Bz,'Bz','res.dmp',1)
  call xup1_set (Bx,Bxc)
  call yup1_set (By,Byc)
  call zup1_set (Bz,Bzc)
    call dumpn(Bxc,'Bxc','res.dmp',1)
    call dumpn(Byc,'Byc','res.dmp',1)
    call dumpn(Bzc,'Bzc','res.dmp',1)
  call xup1_set (Ux,Uxc)
  call yup1_set (Uy,Uyc)
  call zup1_set (Uz,Uzc)
    call dumpn(Uxc,'Uxc','res.dmp',1)
    call dumpn(Uyc,'Uyc','res.dmp',1)
    call dumpn(Uzc,'Uzc','res.dmp',1)

  if (isubstep == 1) then
    do iz=izs,ize
      BB(:,:,iz) = Bxc(:,:,iz)**2 + Byc(:,:,iz)**2 + Bzc(:,:,iz)**2
    end do
     call haverage_subr (BB, emag)
     emag = 0.5*emag
     call average_subr (BB, Brms)
!$omp master
     Brms = sqrt(Brms)
!$omp end master
  end if

!-----------------------------------------------------------------------
!  Alfven speed
!-----------------------------------------------------------------------
  call dumpn(r,'r','res.dmp',1)
  call dumpn(p,'pg','res.dmp',1)
  if (cmax .eq. 0.) then
    do iz=izs,ize
      BB(:,:,iz) = Bxc(:,:,iz)**2 + Byc(:,:,iz)**2 + Bzc(:,:,iz)**2 + 1e-20
      cA(:,:,iz) = sqrt((gamma*p(:,:,iz)+BB(:,:,iz))/r(:,:,iz))
    end do
  else
    do iz=izs,ize
      BB(:,:,iz) = Bxc(:,:,iz)**2 + Byc(:,:,iz)**2 + Bzc(:,:,iz)**2 + 1e-20
      cA(:,:,iz) = BB(:,:,iz)/r(:,:,iz)
      fudge(:,:,iz) = cmax2/(cmax2+cA(:,:,iz))
      cA(:,:,iz) = sqrt(cA(:,:,iz)*fudge(:,:,iz) + gamma*p(:,:,iz)/r(:,:,iz))
      call dumpn(fudge,'fudge','res.dmp',1)
    end do
  end if
  !if (isubstep==1) E_mag = 0.5*average(BB)
    call dumpn(BB,'BB','res.dmp',1)
    call dumpn(cA,'cA','res.dmp',1)

  if (ub < my) then
    call magnetic_pressure_boundary (BB)
  end if
                                                                        call timer('mhd','A-speed')
!-----------------------------------------------------------------------
!  Perpendicular velocity convergence
!-----------------------------------------------------------------------
  Ceta = nuB*(nu2+Csmag)
  do iz=izs,ize
    BB(:,:,iz) = 1./sqrt(BB(:,:,iz))
    Bxc(:,:,iz) = Bxc(:,:,iz)*BB(:,:,iz)
    Byc(:,:,iz) = Byc(:,:,iz)*BB(:,:,iz)
    Bzc(:,:,iz) = Bzc(:,:,iz)*BB(:,:,iz)
    UdB(:,:,iz) = Uxc(:,:,iz)*Bxc(:,:,iz) &
                + Uyc(:,:,iz)*Byc(:,:,iz) &
                + Uzc(:,:,iz)*Bzc(:,:,iz)
    Uxc(:,:,iz) = Uxc(:,:,iz)-Bxc(:,:,iz)*UdB(:,:,iz)
    Uyc(:,:,iz) = Uyc(:,:,iz)-Byc(:,:,iz)*UdB(:,:,iz)
    Uzc(:,:,iz) = Uzc(:,:,iz)-Bzc(:,:,iz)*UdB(:,:,iz)
  end do
                                                                        call timer('mhd','Uperp')
    call dumpn(Uxc,'Uxc','res.dmp',1)
    call dumpn(Uyc,'Uyc','res.dmp',1)
    call dumpn(Uzc,'Uzc','res.dmp',1)
    call dumpn(UdB,'UdB','res.dmp',1)

  call barrier_omp('resist1')                                         ! barrier 1; input values from above
  call dif2_set (Uxc, Uyc, Uzc, UdB)
    call dumpn(UdB,'dif2','res.dmp',1)
                                                                        call timer('mhd','dif2')
!-----------------------------------------------------------------------
!  Resistivity
!-----------------------------------------------------------------------
  if (nu1 < 0.) then
    do iz=izs,ize
      eta(:,:,iz) = nuB*abs(nu1)
      etd(:,:,iz) = 0.0
    end do
  else
    do iz=izs,ize
      do iy=1,my
         eta(:,iy,iz) = (nuB*nu1)*sqrt(Uxc(:,iy,iz)**2+Uyc(:,iy,iz)**2+Uzc(:,iy,iz)**2) &
                       + (nuB*nu3)*cA(:,iy,iz)
         etd(:,iy,iz) = (Ceta   )*UdB(:,iy,iz)*frho(iy)
      end do
    end do
  endif
    call dumpn(eta,'eta','res.dmp',1)
    call dumpn(etd,'etd','res.dmp',1)

  if (nu4 > 0 .and. do_nu4_B) then
    do iz=izs,ize
      scr2(:,:,iz) = alog(ee(:,:,iz))
    end do
    call d2abs_set (scr2,scr1)
    call regularize (scr1)
    do iz=izs,ize
    do iy=1,my
    do ix=1,mx
      eta(ix,iy,iz) = eta(ix,iy,iz)*max(1.,nu4*d2lnr(ix,iy,iz),nu4*scr1(ix,iy,iz))
      etd(ix,iy,iz) = etd(ix,iy,iz)*max(1.,nu4*d2lnr(ix,iy,iz),nu4*scr1(ix,iy,iz))
    end do
    end do
    end do
      call dumpn(d2lnr,'d2lnr','res.dmp',1)
      call dumpn(eta,'eta','res.dmp',1)
      call dumpn(etd,'etd','res.dmp',1)
  end if

!-----------------------------------------------------------------------
! Ramp up eta,etd toward bottom boundary
!-----------------------------------------------------------------------
  if (do_eta) then
    do iz=izs,ize
    do iy=max(ub-eta_width,lb),my
!     f=1.+(iy-(ub-eta_width))/eta_scl
      f=exp(min((iy-(ub-eta_width))/eta_scl,eta_width/eta_scl))
      eta(:,iy,iz)=eta(:,iy,iz)*f
      etd(:,iy,iz)=etd(:,iy,iz)*f
    end do
    end do
  end if
                                                                        call timer('mhd','eta')
  call regularize (etd)
                                                                        call timer('mhd','regular')
!-----------------------------------------------------------------------
!  Smooth over 3x3x3 point, to broaden influence.  Use low order shifts
!  when using eta in mhd, to ensure positive definite values.
!-----------------------------------------------------------------------
  call barrier_omp('resist2')                                           ! barrier 2; etd from above (NOT NEEDED?)
  if (do_max5_mhd) then
    call smooth3max5_set (etd, Bxc)
    call smooth3max5_set (eta, Byc)
  else
    call smooth3max3_set (etd, Bxc)
    call smooth3max3_set (eta, Byc)
  end if
  call barrier_omp('resist3')                                           ! barrier 3; SHOULD NOT BE NEEDED!
  call regularize (etd)
    call dumpn(eta,'eta','res.dmp',1)
    call dumpn(etd,'etd','res.dmp',1)
                                                                        call timer('mhd','smooth')
!-----------------------------------------------------------------------
!  Magnetic diffusion Courant condition factors:
!    qlim : max factor from the quenching operator (=1 when do_quench=f)
!     6.2 : the maximum value returned from ddxup(ddxdn(f)) when dx=1.
!      3. : for the worst case of a 3-D checker board
!      2. : normalize so Ctd=1. when we reach 1x overshoot
!-----------------------------------------------------------------------
  fCv = qlim*6.2*3./2.*dt

!-----------------------------------------------------------------------
!  Evaluate the time step constraints from advection and diffusion
!-----------------------------------------------------------------------
  if (flag) then
    do iz=izs,ize
    if (do_aniso_eta) then 
     do iy=1,my
     do ix=1,mx
      cA(ix,iy,iz) = max((cA(ix,iy,iz)+abs(Uxc(ix,iy,iz))), &
                         (cA(ix,iy,iz)+abs(Uyc(ix,iy,iz))), &
                         (cA(ix,iy,iz)+abs(Uzc(ix,iy,iz))))
      dsmin = min(dxm(ix),dym(iy),dzm(iz))
      cA(ix,iy,iz) = cA(ix,iy,iz)*dt/dsmin
      BB(ix,iy,iz) = (eta(ix,iy,iz)/dsmin+etd(ix,iy,iz))*fCv
     end do
     end do
    else
     do iy=1,my
     do ix=1,mx
      cA(ix,iy,iz) = max((cA(ix,iy,iz)+abs(Uxc(ix,iy,iz))), &
                         (cA(ix,iy,iz)+abs(Uyc(ix,iy,iz))), &
                         (cA(ix,iy,iz)+abs(Uzc(ix,iy,iz))))
      dsmin = min(dxm(ix),dym(iy),dzm(iz))
      dsmax = max(0.5*(dxm(ix)+dym(iy)), &
                  0.5*(dym(iy)+dzm(iz)), &
                  0.5*(dzm(iz)+dxm(ix)))
      cA(ix,iy,iz) = cA(ix,iy,iz)*dt/dsmin
      BB(ix,iy,iz) = (eta(ix,iy,iz)*dsmax+etd(ix,iy,iz)*dsmax**2)*(fCv/dsmin**2)
     end do
     end do
    end if
    end do
    call fmaxval_subr ('Cb',cA,Cb)
    call fmaxval_subr ('Cn',BB,Cn)
    call dumpn(cA,'cA','res.dmp',1)
    call dumpn(BB,'BB','res.dmp',1)
  end if
                                                                        call timer('mhd','Courant')
END
