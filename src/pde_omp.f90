! $Id: pde_omp.f90,v 1.111 2010/07/31 15:28:15 aake Exp $
!***********************************************************************
  SUBROUTINE pde (flag)
  USE params
  logical flag
  real cput(2), void, dtime

  if (do_trace .and. omp_master) then; void = dtime(cput); print '(1x,a5,2f7.3)','pde0 ',cput; endif
  call pde1 (flag)
  if (do_trace .and. omp_master) then; void = dtime(cput); print '(1x,a5,2f7.3)','pde1 ',cput; endif
  call pde2
  if (do_trace .and. omp_master) then ;void = dtime(cput); print '(1x,a5,2f7.3)','pde2 ',cput; endif
  call pde3 (flag)
  if (do_trace .and. omp_master) then; void = dtime(cput); print '(1x,a5,2f7.3)','pde3 ',cput; endif
  END

!***********************************************************************
  SUBROUTINE pde1 (flag)
  USE params
  USE variables
  USE arrays
  USE quench_m
  implicit none

  logical flag, omp_in_parallel
  integer iz, iy, ix

  real gam1, average, fCv, wavesp, velocity, sumy, sumz
  real cput(2), void, dtime
  real, dimension(mx):: exp1
  real, pointer, dimension(:,:,:):: d2lnr, xdnr, ydnr, zdnr

  character(len=mid):: id='$Id: pde_omp.f90,v 1.111 2010/07/31 15:28:15 aake Exp $'
!-----------------------------------------------------------------------

  call print_id(id)
  call dumpn(dedt,'dedt','dedt.dmp',1)

  if (gamma .eq. 1.) then
    gam1 = 1.
  else
    gam1 = gamma-1.
  end if

  Sxx => scr4
  Syy => scr5
  Szz => scr6

  dd  => scr4
  lne => scr5

  xdnl => wk01; ydnl => wk02; zdnl => wk03
  xdnr => wk16; ydnr => wk17; zdnr => wk18
  p    => wk04; ee   => wk05; nu   => wk06
  Cs   => wk07; lnr  => wk08; lnu  => wk09
  Sxy  => wk10; Syz  => wk11; Szx  => wk12
  Txy  => wk13; Tyz  => wk14; Tzx  => wk15
  Txx  => wk16; Tyy  => wk17; Tzz  => wk18
  divu => wk20
  d2lnr => wk21

  fCv = qlim*6.2*3.*dt/2.*max(1.,nuE)
  call print_trace (id, dbg_hd, 'pde1a')

!-----------------------------------------------------------------------
!  Velocities, pressure
!-----------------------------------------------------------------------
  do iz=izs,ize
    lnr(:,:,iz) = alog(r(:,:,iz))
  end do
  !call density_boundary_log(r,lnr)
  call density_boundary(r,lnr,py,e)
  call print_trace (id, dbg_hd, 'pde1b')
  call xdn_set (lnr, xdnl)
  call ydn_set (lnr, ydnl)
                                                                        ! barrier 1, needed because lnr just computed
  call barrier_omp ('pde01')
  call zdn_set (lnr, zdnl)
  call print_trace (id, dbg_hd, 'pde1c1')
  do iz=izs,ize
  do iy=1,my
    call expn (mx, xdnl(:,iy,iz), xdnr(:,iy,iz))
    Ux(:,iy,iz) = px(:,iy,iz)/xdnr(:,iy,iz)
    call expn (mx, ydnl(:,iy,iz), ydnr(:,iy,iz))
    Uy(:,iy,iz) = py(:,iy,iz)/ydnr(:,iy,iz)
    call expn (mx, zdnl(:,iy,iz), zdnr(:,iy,iz))
    Uz(:,iy,iz) = pz(:,iy,iz)/zdnr(:,iy,iz)
  end do
  end do
    call dumpn(Ux,'Ux','dpxdt.dmp',0)
  call print_trace (id, dbg_hd, 'pde1c')

                       if (do_trace .and. omp_master) then; void = dtime(cput); print '(1x,a5,2f7.3)','pde1a',cput; endif

  call velocity_boundary(r,xdnr,ydnr,zdnr,Ux,Uy,Uz,px,py,pz,e,Bx,By,Bz)
    call dumpn (py, 'py', 'drdt.dmp', 0)
    call dumpn (dpxdt, 'dpxdt', 'dpxdt.dmp', 1)

                       if (do_trace .and. omp_master) then; void = dtime(cput); print '(1x,a5,2f7.3)','pde1b',cput; endif
  call print_trace (id, dbg_hd, 'pde1d')

  call forceit (r,Ux,Uy,Uz,xdnr,ydnr,zdnr,dpxdt,dpydt,dpzdt)
    call dumpn (dpxdt, 'dpxdt', 'dpxdt.dmp', 1)
    call dumpn(dedt,'dedt','dedt.dmp',1)
  call print_trace (id, dbg_hd, 'pde1e')

  call ddxup_set (Ux, Sxx)
  call ddyup_set (Uy, Syy)
                                                                        ! barrier 2, needed because Uz computed since last barrier
  call barrier_omp ('pde02')
  call ddzup_set (Uz, Szz)

!-----------------------------------------------------------------------
!  First part of viscosity
!-----------------------------------------------------------------------
  !call dif2a_set (lnr,d2lnr)
  call d2abs_set (lnr,d2lnr)
  call regularize (d2lnr)
  call smooth3max3_set (d2lnr, scr1)
  call dumpn(d2lnr,'pde','d2lnr.dmp',1)

  call dif1_set (Ux, Uy, Uz, nu)
  if (nu4 > 0.) then
    call barrier_omp ('pde03')
    ! call dif2a_set (lnr,nu)
    do iz=izs,ize
    do iy=1,my
    do ix=1,mx
      divu(ix,iy,iz) = Sxx(ix,iy,iz)+Syy(ix,iy,iz)+Szz(ix,iy,iz)
      nu(ix,iy,iz) = nu2*nu(ix,iy,iz)*max(1.,nu4*d2lnr(ix,iy,iz))
    end do
    end do
    end do
    nflop = nflop+7
  else
    do iz=izs,ize
    do iy=1,my
    do ix=1,mx
      divu(ix,iy,iz) = Sxx(ix,iy,iz)+Syy(ix,iy,iz)+Szz(ix,iy,iz)
      nu(ix,iy,iz) = nu2*nu(ix,iy,iz)
    end do
    end do
    end do
    nflop = nflop+4
  end if
    call dumpn (nu,'nud','xxx.dmp',0)
    nu(:,:,izs:ize) = nu(:,:,izs:ize)*max(1.,nu4*d2lnr(:,:,izs:ize))
    call dumpn (nu,'nud','xxx.dmp',1)
  call print_trace (id, dbg_hd, 'pde1f')
    call dumpn (Sxx, 'Sxx', 'dpxdt.dmp', 1)
    call dumpn (Syy, 'Syy', 'dpxdt.dmp', 1)
    call dumpn (Szz, 'Szz', 'dpxdt.dmp', 1)
    call dumpn (nu, 'nu', 'dpxdt.dmp', 1)
    call dumpn (nu, 'nu', 'nu.dmp', 0)

  call xup_set (Ux, scr1)
  call yup_set (Uy, scr2)
  call zup_set (Uz, scr3)
  sumz = 0.
  do iz=izs,ize
    Cs(:,:,iz) = scr1(:,:,iz)**2 + scr2(:,:,iz)**2 + scr3(:,:,iz)**2
  end do
  call average_subr (Cs, Urms)

  if (isubstep==1) then
    do iz=izs,ize
      scr1(:,:,iz) = 0.5*Cs(:,:,iz)*r(:,:,iz)
    end do
    call average_subr (scr1, E_kin)
  end if

!-----------------------------------------------------------------------
!  Polytropic pressure, at given sound speed
!-----------------------------------------------------------------------
  if (.not. do_energy) then
    if (gamma .eq. 1.) then
      do iz=izs,ize
        e(:,:,iz) = csound**2*r(:,:,iz)
      end do
    else
      do iz=izs,ize
        e(:,:,iz) = (1./(gamma*gam1)*csound**2)*r(:,:,iz)**gamma
      end do
    end if
  end if
  call print_trace (id, dbg_hd, 'pde1g')
!-----------------------------------------------------------------------
!  Energy per unit mass
!-----------------------------------------------------------------------
  do iz=izs,ize
    ee(:,:,iz) = e(:,:,iz)/r(:,:,iz)
  end do
                       if (do_trace .and. omp_master) then; void = dtime(cput); print '(1x,a5,2f7.3)','pde1c',cput; endif

!-----------------------------------------------------------------------
!  Boundary condition -- may need to calculate pressure internally
!-----------------------------------------------------------------------

  call print_trace (id, dbg_hd, 'pde1h')
  call energy_boundary(r,lnr,Ux,Uy,Uz,e,ee,p,Bx,By,Bz)
                       if (do_trace .and. omp_master) then; void = dtime(cput); print '(1x,a5,2f7.3)','pde1d',cput; endif
    call dumpn(dedt,'dedt','dedt.dmp',1)

!-----------------------------------------------------------------------
!  Pressure
!-----------------------------------------------------------------------
  if (do_ionization) then
    call pressure (r,ee,p)
  else
    do iz=izs,ize
      p(:,:,iz)  = gam1*e(:,:,iz)
    end do
  end if
                       if (do_trace .and. omp_master) then; void = dtime(cput); print '(1x,a5,2f7.3)','pde1e',cput; endif

!-----------------------------------------------------------------------
!  Numerical viscosity, Richtmyer & Morton type.  For testing purposes,
!  nu1<0 signals the use of constant diffusivity.
!-----------------------------------------------------------------------
  if (nu1.lt.0.0) then
    do iz=izs,ize
      Cs(:,:,iz) = sqrt(gamma*p(:,:,iz)/r(:,:,iz)) + sqrt(Cs(:,:,iz))
      nu(:,:,iz) = abs(nu1)
    end do
  else if (do_mhd) then
    call xup_set(Bx,scr1)
    call yup_set(By,scr2)
    call zup_set(Bz,scr3)
    if (cmax.eq.0) then
      do iz=izs,ize
      do iy=1,my
      do ix=1,mx
        wavesp = sqrt((scr1(ix,iy,iz)**2+scr2(ix,iy,iz)**2+scr3(ix,iy,iz)**2 &
                     + gamma*P(ix,iy,iz))/r(ix,iy,iz))
        scr1(ix,iy,iz) = wavesp
        velocity = sqrt(Cs(ix,iy,iz))
        Cs(ix,iy,iz) = wavesp + velocity
        nu(ix,iy,iz) = nu1*velocity + nu3*wavesp
      ! scr2(ix,iy,iz) = nu(ix,iy,iz) + max(dxm(ix),dym(iy),dzm(iz))*nud(ix,iy,iz)
      end do
      end do
      end do
    else
      do iz=izs,ize
      do iy=1,my
      do ix=1,mx
        wavesp = sqrt(min(cmax**2,(scr1(ix,iy,iz)**2+scr2(ix,iy,iz)**2+scr3(ix,iy,iz)**2)/r(ix,iy,iz)) &
                     + gamma*P(ix,iy,iz)/r(ix,iy,iz))
        scr1(ix,iy,iz) = wavesp
        velocity = sqrt(Cs(ix,iy,iz))
        Cs(ix,iy,iz) = wavesp + velocity
        nu(ix,iy,iz) = nu1*velocity + nu3*wavesp
      ! scr2(ix,iy,iz) = nu(ix,iy,iz) + max(dxm(ix),dym(iy),dzm(iz))*nud(ix,iy,iz)
      end do
      end do
      end do
    end if
  else
    do iz=izs,ize
    do iy=1,my
    do ix=1,mx
      wavesp = sqrt(gamma*P(ix,iy,iz)/r(ix,iy,iz))
      velocity = sqrt(Cs(ix,iy,iz))
      Cs(ix,iy,iz) = wavesp + velocity
      nu(ix,iy,iz) = nu1*velocity + nu3*wavesp + max(dxm(ix),dym(iy),dzm(iz))*nu(ix,iy,iz)
    end do
    end do
    end do
  end if
    call dumpn (nu,'nu','xxx.dmp',1)
    call dumpn (nu  , 'nu' , 'nu.dmp', 1)
    call dumpn (Cs  , 'csu', 'nu.dmp', 1)
    call dumpn (nu  , 'nu' , 'dpxdt.dmp', 1)

  call barrier_omp('pde03')                                             ! barrier 3, needed because nu just modified
  call regularize(nu)

!$omp single
  Urms = sqrt(Urms)
!$omp end single

  call smooth3max3_set (nu, scr1)

  !call max3_set (nu, scr1); do iz=izs,ize; nu(:,:,iz)=scr1(:,:,iz); end do
  !call smooth3_set (nu, scr1); do iz=izs,ize; nu(:,:,iz)=scr1(:,:,iz); end do
  !call max3_set (nu, scr1); !call barrier_omp('pde03'); !call smooth3_set (scr1, nu)

    call dumpn (nu,'sm3mx3','drdt.dmp',1)

  call regularize(nu)
    call dumpn(nu,'reg','drdt.dmp',1)

  if (flag) then
    do iz=izs,ize
    do iy=1,my
    do ix=1,mx
      Cs(ix,iy,iz) = Cs(ix,iy,iz)*(dt/min(dxm(ix),dym(iy),dzm(iz)))
      scr1(ix,iy,iz) = nu(ix,iy,iz)*fCv/min(dxm(ix),dym(iy),dzm(iz))
    end do
    end do
    end do
    call fmaxval_subr ('Cu',Cs,Cu)
    call fmaxval_subr ('Cv',scr1,Cv)
  end if

    call dumpn (nu  , 'nu' , 'nu.dmp', 1)
    call dumpn (scr1, 'Cv' , 'nu.dmp', 1)
    call dumpn (nu, 'nu', 'dpxdt.dmp', 1)
  do iz=izs,ize
    nu(:,:,iz) = nu(:,:,iz)*r(:,:,iz)
    lnu(:,:,iz) = alog(max(nu(:,:,iz),1e-30))
  end do
    call dumpn (lnu, 'lnu', 'dpxdt.dmp', 1)
    call dumpn (lnu,'lnu','xxx.dmp',1)

!-----------------------------------------------------------------------
!  Mass conservation, cell centered
!-----------------------------------------------------------------------
if (do_density) then

  if (nur==0) then
    if (do_2nddiv) then
      call ddxup1_sub (px, drdt)
      call ddyup1_sub (py, drdt)
      call ddzup1_sub (pz, drdt)
    else
      call ddxup_sub (px, drdt)
      call ddyup_sub (py, drdt)
      call ddzup_sub (pz, drdt)
      call dumpn(drdt,'drdt','xxx.dmp',1)
    end if
  else
!
    call ddxdn_set (lnr, scr1)
    call xdn_set (lnu, scr2)
    if (do_quench) then
      call quenchx (scr1, scr3)
      do iz=izs,ize
      do iy=1,my
      call expn (mx, scr2(:,iy,iz), exp1)
      do ix=1,mx
        scr1(ix,iy,iz) = -px(ix,iy,iz)+(nur*dxmdn(ix))*exp1(ix)*scr3(ix,iy,iz)
      end do
      end do
      end do
    else
      do iz=izs,ize
      do iy=1,my
      call expn (mx, scr2(:,iy,iz), exp1)
      do ix=1,mx
        scr1(ix,iy,iz) = -px(ix,iy,iz)+(nur*dxmdn(ix))*exp1(ix)*scr1(ix,iy,iz)
      end do
      end do
      end do
    end if

    if (do_2nddiv) then
      call ddxup1_add (scr1, drdt)
    else
      call ddxup_add (scr1, drdt)
    end if
!
!-----------------------------------------------------------------------
!  In cases with y-stratification, diffuse mass fluctuations relative
!  to the horizontal mean.
!-----------------------------------------------------------------------
    if (do_stratified) then
      call haverage_subr (lnr, eeav)
      do iz=izs,ize
        do iy=1,my
          scr2(:,iy,iz) = lnr(:,iy,iz)-eeav(iy)
        end do
      end do
      call ddydn_set (scr2, scr1)
    else
      call ddydn_set (lnr, scr1)
    end if

    call ydn_set (lnu, scr2)
      call dumpn(  py,'  py','drdt.dmp',1)
      call dumpn(scr1,'scr1','drdt.dmp',1)
      call dumpn(scr2,'scr2','drdt.dmp',1)
    if (do_quench) then
      call quenchy (scr1, scr3)
      do iz=izs,ize
      do iy=1,my
      call expn (mx, scr2(:,iy,iz), exp1)
      do ix=1,mx
        scr1(ix,iy,iz) = -py(ix,iy,iz)+(nur*dymdn(iy))*exp1(ix)*scr3(ix,iy,iz)
      end do
      end do
      end do
    else
      do iz=izs,ize
      do iy=1,my
      call expn (mx, scr2(:,iy,iz), exp1)
      do ix=1,mx
        scr1(ix,iy,iz) = -py(ix,iy,iz)+(nur*dymdn(iy))*exp1(ix)*scr1(ix,iy,iz)
      end do
      end do
      end do
    end if

    if (do_2nddiv) then
      call ddyup1_add (scr1, drdt)
    else
      call ddyup_add (scr1, drdt)
    end if
      call dumpn(scr1,'scr1','drdt.dmp',1)
      call dumpn(drdt,'drdt','drdt.dmp',1)
!
    call ddzdn_set (lnr, scr1)
                                                                        ! barrier 5, needed because lnu & scr1 just computed
    call barrier_omp ('pde05')
    if (do_quench) then
      call quenchz (scr1, scr3, scr2)
                                                                        ! barrier 6, needed only because scr2 overlaps (quench)
      call barrier_omp ('pde06')
      call zdn_set (lnu, scr2)
      do iz=izs,ize
      do iy=1,my
      call expn (mx, scr2(:,iy,iz), exp1)
      do ix=1,mx
        scr1(ix,iy,iz) = -pz(ix,iy,iz)+(nur*dzmdn(iz))*exp1(ix)*scr3(ix,iy,iz)
      end do
      end do
      end do
    else
      call zdn_set (lnu, scr2)
      do iz=izs,ize
      do iy=1,my
      call expn (mx, scr2(:,iy,iz), exp1)
      do ix=1,mx
        scr1(ix,iy,iz) = -pz(ix,iy,iz)+(nur*dzmdn(iz))*exp1(ix)*scr1(ix,iy,iz)
      end do
      end do
      end do
    end if

                                                                        ! barrier 7, needed because scr1 modified since last barrier
    call barrier_omp ('pde07')
    if (do_2nddiv) then
      call ddzup1_add (scr1, drdt)
    else
      call ddzup_add (scr1, drdt)
    end if
  end if
                                                                        ! barrier 7a, needed because scr1 modified later on!!!
  call barrier_omp ('pde08')
end if
  END

!***********************************************************************
  SUBROUTINE pde2
  USE params
  USE variables
  USE arrays
  USE quench_m
  implicit none
  integer iz, iy, ix, lb1, ub1
  real void, dtime, cput(2), average, nuS1, nuS2
  real, dimension(mx):: nuxy, nuyz, nuzx, exp1

    call dumpn(dedt,'dedt','dedt.dmp',1)

!-----------------------------------------------------------------------
!  Quenching operator
!-----------------------------------------------------------------------
                       if (do_trace .and. omp_master) then; void = dtime(cput); print '(1x,a5,2f7.3)','pde2a',cput; endif
  if (do_quench) then
                                                                        ! barrier 8, needed because scr1 is input to ddzup above (quench)
    call barrier_omp ('pde09')
    call quenchx (Sxx, scr1)
    call quenchz (Szz, scr3, scr2)
                                                                        ! barrier 9, needed only because scr2 overlaps (quench)
    call barrier_omp ('pde10')
    call quenchy (Syy, scr2)
  end if
                       if (do_trace .and. omp_master) then; void = dtime(cput); print '(1x,a5,2f7.3)','pde2b',cput; endif

!-----------------------------------------------------------------------
!  Viscous stress, diagonal and off-diagonal elements
!-----------------------------------------------------------------------
  nuS1 = 3.*nuS/(1.+2.*nuS)
  nuS2 = (1.-nuS)/(1.+2.*nuS)
    call dumpn(dedt,'dedt','dedt.dmp',1)
    call dumpn (dedt,'dedt','xxx.dmp',1)
  do iz=izs,ize
    if (do_quench) then
      do iy=1,my
      do ix=1,mx
        Txx(ix,iy,iz) = - dxm(ix)*nu(ix,iy,iz)*scr1(ix,iy,iz) + p(ix,iy,iz)
        Tyy(ix,iy,iz) = - dym(iy)*nu(ix,iy,iz)*scr2(ix,iy,iz) + p(ix,iy,iz)
        Tzz(ix,iy,iz) = - dzm(iz)*nu(ix,iy,iz)*scr3(ix,iy,iz) + p(ix,iy,iz)
      end do
      end do
    else
      do iy=1,my
      do ix=1,mx
        ! Txx(ix,iy,iz) = - (dxm(ix))*nu(ix,iy,iz)*Sxx(ix,iy,iz) + p(ix,iy,iz)
        ! Tyy(ix,iy,iz) = - (dym(iy))*nu(ix,iy,iz)*Syy(ix,iy,iz) + p(ix,iy,iz)
        ! Tzz(ix,iy,iz) = - (dzm(iz))*nu(ix,iy,iz)*Szz(ix,iy,iz) + p(ix,iy,iz)
        Txx(ix,iy,iz) = - dxm(ix)*nu(ix,iy,iz)*(nuS1*Sxx(ix,iy,iz) + nuS2*divu(ix,iy,iz)) + p(ix,iy,iz)
        Tyy(ix,iy,iz) = - dym(iy)*nu(ix,iy,iz)*(nuS1*Syy(ix,iy,iz) + nuS2*divu(ix,iy,iz)) + p(ix,iy,iz)
        Tzz(ix,iy,iz) = - dzm(iz)*nu(ix,iy,iz)*(nuS1*Szz(ix,iy,iz) + nuS2*divu(ix,iy,iz)) + p(ix,iy,iz)
      end do
      end do
    end if
    if (do_energy) then
      if (do_dissipation) then 
        lb1 = lb
        ub1 = ub
        lb1 = max(min(lb1,my),1)
        ub1 = max(min(ub1,my),1)
        if (isubstep == 1) then
	  do iy=lb1,ub1
          do ix=1,mx
            scr1(ix,iy,iz) = (dxm(ix))*nu(ix,iy,iz)*Sxx(ix,iy,iz)**2 &
                           + (dym(iy))*nu(ix,iy,iz)*Syy(ix,iy,iz)**2 &
                           + (dzm(iz))*nu(ix,iy,iz)*Szz(ix,iy,iz)**2
          end  do
          end  do
        end if
        do iy=lb1,ub1
        do ix=1,mx
          dedt(ix,iy,iz) = dedt(ix,iy,iz) &
                     - Txx(ix,iy,iz)*Sxx(ix,iy,iz) &
                     - Tyy(ix,iy,iz)*Syy(ix,iy,iz) &
                     - Tzz(ix,iy,iz)*Szz(ix,iy,iz)
        end do
        end do
      else
        do iy=1,my
        do ix=1,mx
          dedt(ix,iy,iz) = dedt(ix,iy,iz) - p(ix,iy,iz)* &
                       (Sxx(ix,iy,iz) + Syy(ix,iy,iz) + Szz(ix,iy,iz))
        end do
        end do
      end if
    end if
  end do
    call dumpn(dedt,'dedt','xxx.dmp',1)
    call dumpn(  p,  'p','xxx.dmp',1)
    call dumpn(Sxx,'Sxx','xxx.dmp',1)
    call dumpn(Syy,'Syy','xxx.dmp',1)
    call dumpn(Szz,'Szz','xxx.dmp',1)

  if (do_energy .and. do_dissipation .and. isubstep==1) then
    !$omp barrier
    call average_subr (scr1, Q_kin)
    !print*,'Qk1',omp_mythread,Q_kin
  end if
    call dumpn (nu, 'nu', 'dpxdt.dmp', 1)
    call dumpn (Sxx, 'Sxx', 'dpxdt.dmp', 1)
    call dumpn (Txx, 'Txx', 'dpxdt.dmp', 1)
    call dumpn (nu , ' nu', 'dedt.dmp', 1)
    call dumpn (divu,'div', 'dedt.dmp', 1)
    call dumpn (p  , '  o', 'dedt.dmp', 1)
    call dumpn (Sxx, 'Sxx', 'dedt.dmp', 1)
    call dumpn (Syy, 'Syy', 'dedt.dmp', 1)
    call dumpn (Szz, 'Szz', 'dedt.dmp', 1)
    call dumpn (Txx, 'Txx', 'dedt.dmp', 1)
    call dumpn (Tyy, 'Tyy', 'dedt.dmp', 1)
    call dumpn (Tzz, 'Tzz', 'dedt.dmp', 1)
    call dumpn (dedt, 'diag', 'dedt.dmp', 1)
                       if (do_trace .and. omp_master) then; void = dtime(cput); print '(1x,a5,2f7.3)','pde2c',cput; endif

  call ddzdn_set (Uy,scr2)
  call ddxdn_set (Uy,scr1)
  call ddydn_set (Ux,scr3)
  call ddzdn_set (Ux,scr4)
  call ddxdn_set (Uz,scr5)
  call ddydn_set (Uz,scr6)
  do iz=izs,ize
    Sxy(:,:,iz) = (scr1(:,:,iz)+scr3(:,:,iz))*0.5
    Syz(:,:,iz) = (scr6(:,:,iz)+scr2(:,:,iz))*0.5
    Szx(:,:,iz) = (scr4(:,:,iz)+scr5(:,:,iz))*0.5
  end do

  call xdn_set (lnu,scr1)
    call dumpn (lnu , 'lnu' , 'dpxdt.dmp', 1)
    call dumpn (scr1, 'scr1', 'dpxdt.dmp', 1)
  call ydn_set (scr1,scr2)
    call dumpn (scr2, 'scr2', 'dpxdt.dmp', 1)
                                                                        ! barrier 10, scr1
  call barrier_omp ('pde11')
  call zdn_set (scr1,scr3)
  call ydn_set (lnu,scr5)
                                                                        ! barrier 11, scr5
  call barrier_omp ('pde12')
  call zdn_set (scr5,scr1)
                       if (do_trace .and. omp_master) then; void = dtime(cput); print '(1x,a5,2f7.3)','pde2d',cput; endif

  if (do_quench) then
                                                                        ! barrier 11, Syz (quench)
    call barrier_omp ('pde13')
    call quenchyz (Syz, scr5, scr4)
                                                                        ! barrier 12, scr4 (quench)
    call barrier_omp ('pde14')
    call quenchzx (Szx, scr6, scr4)
                                                                        ! barrier 13, scr4 (quench)
    call barrier_omp ('pde15')
    call quenchxy (Sxy, scr4)
                       if (do_trace .and. omp_master) then; void = dtime(cput); print '(1x,a5,2f7.3)','pde2e',cput; endif
    do iz=izs,ize
    do iy=1,my
      call expn (mx, scr2(:,iy,iz), nuxy)
      call expn (mx, scr1(:,iy,iz), nuyz)
      call expn (mx, scr3(:,iy,iz), nuzx)
      do ix=1,mx
        Txy(ix,iy,iz) = - max(dxmdn(ix),dymdn(iy))*nuxy(ix)*scr4(ix,iy,iz)
        Tyz(ix,iy,iz) = - max(dymdn(iy),dzmdn(iz))*nuyz(ix)*scr5(ix,iy,iz)
        Tzx(ix,iy,iz) = - max(dzmdn(iz),dxmdn(ix))*nuzx(ix)*scr6(ix,iy,iz)
      end do
    end do
    end do
  else
    do iz=izs,ize
    do iy=1,my
      call expn (mx, scr2(:,iy,iz), nuxy)                               ! scr2 = ydn(xdn(alog(nu)))
      call expn (mx, scr1(:,iy,iz), nuyz)                               ! scr1 = ydm(zdn(alog(nu)))
      call expn (mx, scr3(:,iy,iz), nuzx)                               ! scr3 = zdn(xdn(alog(nu)))
      do ix=1,mx
        Txy(ix,iy,iz) = - nuS1*max(dxmdn(ix),dymdn(iy))*nuxy(ix)*Sxy(ix,iy,iz)
        Tyz(ix,iy,iz) = - nuS1*max(dymdn(iy),dzmdn(iz))*nuyz(ix)*Syz(ix,iy,iz)
        Tzx(ix,iy,iz) = - nuS1*max(dzmdn(iz),dxmdn(ix))*nuzx(ix)*Szx(ix,iy,iz)
      end do
    end do
    end do
  end if
    call dumpn (Sxy, 'Sxy', 'dpxdt.dmp', 1)
    call dumpn (Txy, 'Txy', 'dpxdt.dmp', 1)
                       if (do_trace .and. omp_master) then; void = dtime(cput); print '(1x,a5,2f7.3)','pde2e',cput; endif

  if (do_energy .and. do_dissipation) then
    call xup_set (Sxy, scr1)
    call yup_set (scr1, Sxy)
    call yup_set (Syz, scr1)
                                                                        ! barrier 14, scr1
    call barrier_omp ('pde16')
    call zup_set (scr1, Syz)
    call zup_set (Szx, scr2)
                                                                        ! barrier 15, scr2
    call barrier_omp ('pde17')
    call xup_set (scr2, Szx)

    do iz=izs,ize
      do iy=lb1,ub1
      do ix=1,mx
        dedt(ix,iy,iz) = dedt(ix,iy,iz) + 2.* &
          ((dxm(ix)+dym(iy))*nu(ix,iy,iz)*Sxy(ix,iy,iz)**2 &
        +  (dym(iy)+dzm(iz))*nu(ix,iy,iz)*Syz(ix,iy,iz)**2 &
        +  (dzm(iz)+dxm(ix))*nu(ix,iy,iz)*Szx(ix,iy,iz)**2)
      end do
      end do
      if (isubstep == 1) then
        do iy=lb1,ub1
        do ix=1,mx
          scr1(ix,iy,iz) = 2.* &
              ((dxm(ix)+dym(iy))*nu(ix,iy,iz)*Sxy(ix,iy,iz)**2 &
            +  (dym(iy)+dzm(iz))*nu(ix,iy,iz)*Syz(ix,iy,iz)**2 &
            +  (dzm(iz)+dxm(ix))*nu(ix,iy,iz)*Szx(ix,iy,iz)**2)
        end  do
        end  do
      end if
    end do
    if (isubstep == 1) then
      !$omp barrier
      Q_kin = Q_kin + average(scr1)
      !print*,'Qk2',omp_mythread,Q_kin
    end if
    call dumpn (dedt, 'offdiag', 'dedt.dmp', 1)
  end if
                       if (do_trace .and. omp_master) then; void = dtime(cput); print '(1x,a5,2f7.3)','pde2f',cput; endif
!-----------------------------------------------------------------------
!  Add Reynolds stress
!-----------------------------------------------------------------------
  call xup_set (Ux,scr1)
  call yup_set (Uy,scr2)
  call zup_set (Uz,scr3)
    call dumpn (Txx, 'Txx', 'dpxdt.dmp', 1)
  do iz=izs,ize
    Txx(:,:,iz) = Txx(:,:,iz) + r(:,:,iz)*scr1(:,:,iz)**2
    Tyy(:,:,iz) = Tyy(:,:,iz) + r(:,:,iz)*scr2(:,:,iz)**2
    Tzz(:,:,iz) = Tzz(:,:,iz) + r(:,:,iz)*scr3(:,:,iz)**2
  end do
    call dumpn (scr1, 'Ux^2', 'dpxdt.dmp', 1)
    call dumpn (Txx, 'Txx', 'dpxdt.dmp', 1)

  call xdn_set (ydnl, scr1)
  call xdn_set (Uy, scr2)
  call ydn_set (Ux, scr3)
  do iz=izs,ize
  do iy=1,my
    call expn (mx, scr1(:,iy,iz), exp1)
    Txy(:,iy,iz) = Txy(:,iy,iz) + exp1*scr3(:,iy,iz)*scr2(:,iy,iz)
  end do
  end do

  call ydn_set (zdnl, scr1)
  call ydn_set (Uz, scr2)
  call zdn_set (Uy, scr3)
  do iz=izs,ize
  do iy=1,my
    call expn (mx, scr1(:,iy,iz), exp1)
    Tyz(:,iy,iz) = Tyz(:,iy,iz) + exp1*scr3(:,iy,iz)*scr2(:,iy,iz)
  end do
  end do

  call zdn_set (xdnl, scr1)
  call zdn_set (Ux, scr2)
  call xdn_set (Uz, scr3)
    call dumpn (scr1, 'scr1', 'dpxdt.dmp', 1)
    call dumpn (scr2, 'scr2', 'dpxdt.dmp', 1)
    call dumpn (scr3, 'scr3', 'dpxdt.dmp', 1)
    call dumpn (Tzx, 'Tzx', 'dpxdt.dmp', 1)
  do iz=izs,ize
  do iy=1,my
    call expn (mx, scr1(:,iy,iz), exp1)
    Tzx(:,iy,iz) = Tzx(:,iy,iz) + exp1*scr3(:,iy,iz)*scr2(:,iy,iz)
  end do
  end do
    call dumpn (Tzx, 'Tzx', 'dpxdt.dmp', 1)
                       if (do_trace .and. omp_master) then; void = dtime(cput); print '(1x,a5,2f7.3)','pde2a',cput; endif


!-----------------------------------------------------------------------
!  Equations of motion, face centred
!-----------------------------------------------------------------------
                                                                        ! barrier 16, scr1
  call barrier_omp ('pde18')
  if (do_2nddiv) then
    call ddxdn1_set (Txx, scr1)
    call ddyup1_set (Txy, scr2)
    call ddzup1_set (Tzx, scr3)
  else
    call ddxdn_set (Txx, scr1)
    call ddyup_set (Txy, scr2)
    call ddzup_set (Tzx, scr3)
  end if
  do iz=izs,ize
    dpxdt(:,:,iz) = dpxdt(:,:,iz) - scr1(:,:,iz) - scr2(:,:,iz) - scr3(:,:,iz)
  end do
    call dumpn (scr1, 'scr1', 'dpxdt.dmp', 1)
    call dumpn (scr2, 'scr2', 'dpxdt.dmp', 1)
    call dumpn (scr3, 'scr3', 'dpxdt.dmp', 1)
    call dumpn (dpxdt, 'dpxdt', 'dpxdt.dmp', 1)

  if (do_2nddiv) then
    call ddydn1_set (Tyy, scr1)
    call ddzup1_set (Tyz, scr2)
    call ddxup1_set (Txy, scr3)
  else
    call ddydn_set (Tyy, scr1)
    call ddzup_set (Tyz, scr2)
    call ddxup_set (Txy, scr3)
  end if
  do iz=izs,ize
    dpydt(:,:,iz) = dpydt(:,:,iz) - scr1(:,:,iz) - scr2(:,:,iz) - scr3(:,:,iz)
  end do

  if (do_2nddiv) then
    call ddzdn1_set (Tzz, scr1)
    call ddxup1_set (Tzx, scr2)
    call ddyup1_set (Tyz, scr3)
  else
    call ddzdn_set (Tzz, scr1)
    call ddxup_set (Tzx, scr2)
    call ddyup_set (Tyz, scr3)
  end if
  do iz=izs,ize
    dpzdt(:,:,iz) = dpzdt(:,:,iz) - scr1(:,:,iz) - scr2(:,:,iz) - scr3(:,:,iz)
    lne(:,:,iz) = alog(ee(:,:,iz))
  end do

  do iz=izs,ize
    lnu(:,:,iz) = lnu(:,:,iz)+lne(:,:,iz)+alog(nuE)
  end do
                       if (do_trace .and. omp_master) then; void = dtime(cput); print '(1x,a5,2f7.3)','pde2a',cput; endif
  END

!***********************************************************************
  SUBROUTINE pde3 (flag)
  USE params
  USE variables
  USE arrays
  USE quench_m
  implicit none
  logical flag
  integer iz, iy, ix
  real void, dtime, cput(2)
  real, dimension(mx):: exp1, exp2, exp3
                       if (do_trace .and. omp_master) then; void = dtime(cput); print '(1x,a5,2f7.3)','pde3a',cput; endif
    call dumpn(dedt,'dedt_0','xxx.dmp',1)

  call xdn_set (lnu, scr1)                                              ! lnu = alog(rho*nu*ee)
  call xdn_set (lne, scr6)                                              ! lne = alog(ee)
  call ddxdn_set (lne, scr3)
  if (do_quench) then
    call quenchx (scr3, scr2)
    do iz=izs,ize
    do iy=1,my
    call expn (mx, scr6(:,iy,iz), exp1)
    call expn (mx, scr1(:,iy,iz), exp2)
    do ix=1,mx
      scr3(ix,iy,iz) = exp1(ix)*px(ix,iy,iz) - dxmdn(ix)*exp2(ix)*scr2(ix,iy,iz)
    end do
    end do
    end do
  else
    do iz=izs,ize
    do iy=1,my
    call expn (mx, scr6(:,iy,iz), exp1)                                 ! exp1 = xdn(ee)
    call expn (mx, scr1(:,iy,iz), exp2)                                 ! exp2 = xdn(rho*nu*ee)
    do ix=1,mx                                                          ! xdn(ee)*px - xdn(rho*nu*ee)*ddxdn(lne)
      scr3(ix,iy,iz) = exp1(ix)*px(ix,iy,iz) - dxmdn(ix)*exp2(ix)*scr3(ix,iy,iz)
    end do
    end do
    end do
  end if
                       if (do_trace .and. omp_master) then; void = dtime(cput); print '(1x,a5,2f7.3)','pde3b',cput; endif

  if (do_2nddiv) then
    call ddxup1_set (scr3, scr1)
  else
    call ddxup_set (scr3, scr1)
  end if
  do iz=izs,ize
    dedt(:,:,iz) = dedt(:,:,iz) - scr1(:,:,iz)
  end do
    call dumpn (dedt, 'x-deriv', 'dedt.dmp', 1)
    call dumpn (dedt,'dedt_x','xxx.dmp',1)
    call dumpn(lnu,'lnu','xxx.dmp',1)
    call dumpn(lne,'lne','xxx.dmp',1)

  call ydn_set (lnu, scr1)
  call ydn_set (lne, scr2)

!-----------------------------------------------------------------------
!  In cases with y-stratification, diffuse energy fluctuations relative
!  to the horizontal mean.
!-----------------------------------------------------------------------
  if (do_stratified) then
    call haverage_subr (ee, eeav)
    do iz=izs,ize
      do iy=1,my
        scr6(:,iy,iz) = lne(:,iy,iz)-log(eeav(iy))
      end do
    end do
    call ddydn_set (scr6, scr3)
  else
    call ddydn_set (lne, scr3)
  end if

  if (do_quench) then
    call quenchy (scr3, scr6)
    do iz=izs,ize
    do iy=1,my
    call expn (mx, scr2(:,iy,iz), exp1)
    call expn (mx, scr1(:,iy,iz), exp2)
    call expn (mx, scr6(:,iy,iz), exp3)
    do ix=1,mx
      scr3(ix,iy,iz) = exp1(ix)*py(ix,iy,iz) - dymdn(iy)*exp2(ix)*scr6(ix,iy,iz)
    end do
    end do
    end do
  else
    do iz=izs,ize
    do iy=1,my
    call expn (mx, scr2(:,iy,iz), exp1)
    call expn (mx, scr1(:,iy,iz), exp2)
    call expn (mx, scr6(:,iy,iz), exp3)
    do ix=1,mx
      scr3(ix,iy,iz) = exp1(ix)*py(ix,iy,iz) - dymdn(iy)*exp2(ix)*scr3(ix,iy,iz)
    end do
    end do
    end do
  end if
                       if (do_trace .and. omp_master) then; void = dtime(cput); print '(1x,a5,2f7.3)','pde3c',cput; endif

  if (do_2nddiv) then
    call ddyup1_set (scr3, scr1)
  else
    call ddyup_set (scr3, scr1)
  end if
  do iz=izs,ize
    dedt(:,:,iz) = dedt(:,:,iz) - scr1(:,:,iz)
  end do
    call dumpn (dedt, 'y-deriv', 'dedt.dmp', 1)
    call dumpn (dedt,'dedt_y','xxx.dmp',1)

                                                                        ! barrier 17, lne??? (quench)
  call barrier_omp ('pde19')
  call ddzdn_set (lne, scr3)
  if (do_quench) then
                                                                        ! barrier 18, src3 (quench)
    call barrier_omp ('pde20')
    call quenchz (scr3, scr6, scr1)
                                                                        ! barrier 19, scr1 (eliminated with more scratch)
    call barrier_omp ('pde21')
    call zdn_set (lnu, scr1)
    call zdn_set (lne, scr2)
    do iz=izs,ize
    do iy=1,my
    call expn (mx, scr2(:,iy,iz), exp1)
    call expn (mx, scr1(:,iy,iz), exp2)
    do ix=1,mx
      scr3(ix,iy,iz) = exp1(ix)*pz(ix,iy,iz) - dzmdn(iz)*exp2(ix)*scr6(ix,iy,iz)
    end do
    end do
    end do
  else
    call zdn_set (lnu, scr1)
    call zdn_set (lne, scr2)
    do iz=izs,ize
    do iy=1,my
    call expn (mx, scr2(:,iy,iz), exp1)
    call expn (mx, scr1(:,iy,iz), exp2)
    do ix=1,mx
      scr3(ix,iy,iz) = exp1(ix)*pz(ix,iy,iz) - dzmdn(iz)*exp2(ix)*scr3(ix,iy,iz)
    end do
    end do
    end do
  end if
                       if (do_trace .and. omp_master) then; void = dtime(cput); print '(1x,a5,2f7.3)','pde3d',cput; endif

                                                                        ! barrier 20, scr3 set above
  call barrier_omp ('pde22')
  if (do_2nddiv) then
    call ddzup1_set (scr3, scr1)
  else
    call ddzup_set (scr3, scr1)
  end if

  do iz=izs,ize
    dedt(:,:,iz) = dedt(:,:,iz) - scr1(:,:,iz)
  end do
    call dumpn (dedt,'dedt_z','xxx.dmp',1)
    call dumpn (dedt, 'z-deriv', 'dedt.dmp', 1)

!-----------------------------------------------------------------------
!  Cooling and conduction
!-----------------------------------------------------------------------
  call barrier_omp ('pde23')
  call coolit (r,ee,lne,dd,dedt)
    call dumpn (dedt, 'cooling', 'dedt.dmp', 1)
                       if (do_trace .and. omp_master) then; void = dtime(cput); print '(1x,a5,2f7.3)','pde3e',cput; endif

!-----------------------------------------------------------------------
!  Passive scalar
!-----------------------------------------------------------------------
  if (do_pscalar) then
    do iz=izs,ize
      dd(:,:,iz) = d(:,:,iz)/r(:,:,iz)
    end do
    call passive (nu,lnu,r,px,py,pz,Ux,Uy,Uz,dd,dddt)
  end if
  call trace_particles (Ux,Uy,Uz)
                       if (do_trace .and. omp_master) then; void = dtime(cput); print '(1x,a5,2f7.3)','pde3f',cput; endif

!-----------------------------------------------------------------------
!  MHD
!-----------------------------------------------------------------------
    call dumpn (dpxdt, 'dpxdt', 'dpxdt.dmp', 1)
  if (do_mhd) then
    call mhd (flag, &
            r,e,p,Ux,Uy,Uz,Bx,By,Bz,Ex,Ey,Ez, &
            dpxdt,dpydt,dpzdt,dedt,dBxdt,dBydt,dBzdt)
    call dumpn (dedt, 'joule', 'dedt.dmp', 1)
  end if
                       if (do_trace .and. omp_master) then; void = dtime(cput); print '(1x,a5,2f7.3)','pde3g',cput; endif

!-----------------------------------------------------------------------
!  Boundary conditions on time derivatives
!-----------------------------------------------------------------------
    call dumpn (dpxdt, 'dpxdt', 'dpxdt.dmp', 1)
    call dumpn(drdt,'drdt','xxx.dmp',1)
  call ddt_boundary (r,px,py,pz,e,p,Bx,By,Bz, &
                     drdt,dpxdt,dpydt,dpzdt, &
                     dedt,dBxdt,dBydt,dBzdt)
    call dumpn (dedt, 'ddt-bdry', 'dedt.dmp', 1)
    call dumpn (dpxdt, 'dpxdt', 'dpxdt.dmp', 1)
    call dumpn (dpydt, 'dpydt', 'drdt.dmp', 1)
    call dumpn(drdt,'drdt','xxx.dmp',1)
    call dumpn (dedt,'dedt','xxx.dmp',1)
                       if (do_trace .and. omp_master) then; void = dtime(cput); print '(1x,a5,2f7.3)','pde3h',cput; endif

  END
