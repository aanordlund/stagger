! $Id: timestep.f90,v 1.30 2015/06/22 03:36:51 aake Exp $
!************************************************************************
MODULE timeintegration
  implicit none

  real, dimension(3) ::  alpha, beta
  
END MODULE timeintegration

!------------------------------------------------------------------------
SUBROUTINE init_timestep
  USE params, only : timeorder,nstep,mid
  USE timeintegration

  if      (timeorder==1) then
    alpha=(/ 0.   ,  0.   , 0. /)
    beta =(/ 1.   ,  0.   , 0. /)
  else if (timeorder==2) then
    alpha=(/ 0.   , -1./2., 0. /)
    beta =(/ 1./2.,  1.   , 0. /)
  else if (timeorder==3) then
    alpha=(/0.    , -0.641874, -1.31021 /)
    beta =(/0.46173,  0.924087, 0.390614/)
!    alpha=(/   0. ,  -5./9., -153./128. /)     !  coefficients of Williamson (1980)
!    beta= (/ 1./3., 15./16.,     8./15. /)
  end if
END SUBROUTINE init_timestep

!------------------------------------------------------------------------
SUBROUTINE timestep (r,px,py,pz,e,d, &
                 drdt,dpxdt,dpydt,dpzdt,dedt,dddt, &
                   Bx,By,Bz,dBxdt,dBydt,dBzdt)
!
!  3rd order, 2-N storage Runge-Kutta.  Advances the eight MHD variables
!  forward in time, given time derivatives computed in the subroutine pde.
!
  USE params
  USE timeintegration
  implicit none
  real, dimension(mx,my,mz):: r,px,py,pz,e,d, &
                           drdt,dpxdt,dpydt,dpzdt,dedt,dddt, &
                             Bx,By,Bz,dBxdt,dBydt,dBzdt
  integer, save:: idump=0
  integer nx, ny, nz, iz, isub
  real :: drtdt
  character(len=mid):: id="$Id: timestep.f90,v 1.30 2015/06/22 03:36:51 aake Exp $"

  call print_id (id)

!------------------------------------------------------------------------
!  Loop over three Runge-Kutta stages
!------------------------------------------------------------------------
  rt= td                                                                ! rt is the "real time" computed also in substeps.
  !$omp parallel private(isub,iz,drtdt), shared(isubstep)
  do isub=1,timeorder
   call barrier_omp('timestep')
   !$omp single
   isubstep = isub
   !$omp end single
   if (isubstep .eq. 1) then
    drtdt = 1.
    do iz=izs,ize
      if (do_density) drdt(:,:,iz)  = 0.
      if (do_momenta) then
                      dpxdt(:,:,iz) = 0.
                      dpydt(:,:,iz) = 0.
                      dpzdt(:,:,iz) = 0.
      end if
      if (do_energy)  dedt(:,:,iz)  = 0.
      if (do_mhd) then
                      dBxdt(:,:,iz) = 0.
                      dBydt(:,:,iz) = 0.
                      dBzdt(:,:,iz) = 0.
      end if
      if (do_pscalar) dddt(:,:,iz)  = alpha(isubstep)*dddt(:,:,iz)
    end do
   else
    drtdt = alpha(isubstep)*drtdt + 1.
    do iz=izs,ize
      if (do_density) drdt(:,:,iz)  = alpha(isubstep)*drdt(:,:,iz)
      if (do_momenta) then
                      dpxdt(:,:,iz) = alpha(isubstep)*dpxdt(:,:,iz)
                      dpydt(:,:,iz) = alpha(isubstep)*dpydt(:,:,iz)
                      dpzdt(:,:,iz) = alpha(isubstep)*dpzdt(:,:,iz)
      end if
      if (do_energy)  dedt(:,:,iz)  = alpha(isubstep)*dedt(:,:,iz)
      if (do_mhd) then
                      dBxdt(:,  :  ,iz) = alpha(isubstep)*dBxdt(:,  :  ,iz)
                      dBydt(:,  :  ,iz) = alpha(isubstep)*dBydt(:,  :  ,iz)
                      dBzdt(:,  :  ,iz) = alpha(isubstep)*dBzdt(:,  :  ,iz)
      end if
      if (do_pscalar) dddt(:,:,iz)  = alpha(isubstep)*dddt(:,:,iz)
    end do
   end if

    call pde(isubstep.eq.1)
    if (isubstep.eq.1) call courant

    !$omp single
    rt = rt + dt*beta(isubstep)*drtdt
    !$omp end single
    do iz=izs,ize
      if (do_density) r(:,:,iz)  = r(:,:,iz)  + (dt*beta(isubstep))*drdt(:,:,iz)
      if (do_momenta) then
                      px(:,:,iz) = px(:,:,iz) + (dt*beta(isubstep))*dpxdt(:,:,iz)                      
                      py(:,:,iz) = py(:,:,iz) + (dt*beta(isubstep))*dpydt(:,:,iz)
                      pz(:,:,iz) = pz(:,:,iz) + (dt*beta(isubstep))*dpzdt(:,:,iz)
      end if
      if (do_energy)  e(:,:,iz)  = e(:,:,iz)  + (dt*beta(isubstep))*dedt(:,:,iz)
      if (do_mhd) then
                      Bx(:,  :  ,iz) = Bx(:,  :  ,iz) + (dt*beta(isubstep))*dBxdt(:,  :  ,iz)
                      By(:,  :  ,iz) = By(:,  :  ,iz) + (dt*beta(isubstep))*dBydt(:,  :  ,iz)
                      Bz(:,  :  ,iz) = Bz(:,  :  ,iz) + (dt*beta(isubstep))*dBzdt(:,  :  ,iz)
      end if
      if (do_pscalar) d(:,:,iz)  = d(:,:,iz)  + (dt*beta(isubstep))*dddt(:,:,iz)
    end do

    call dumpn(drdt ,'drdt ','dfdt.dmp',1)
    call dumpn(dpxdt,'dpxdt','dfdt.dmp',1)
    call dumpn(dpydt,'dpydt','dfdt.dmp',1)
    call dumpn(dpzdt,'dpzdt','dfdt.dmp',1)
    if (do_energy) then
      call dumpn(dedt ,'dedt ','dfdt.dmp',1)
    end if
    if (do_pscalar) then
      call dumpn(dddt ,'dddt ','dfdt.dmp',1)
    end if
    if (do_mhd) then
      call dumpn(dBxdt,'dBxdt','dfdt.dmp',1)
      call dumpn(dBydt,'dBydt','dfdt.dmp',1)
      call dumpn(dBzdt,'dBzdt','dfdt.dmp',1)
    end if
  end do
  !$omp end parallel

  if (idbg > 1) then
                   call stats('drdt' ,drdt)
    if (do_momenta) then
                   call stats('dpxdt',dpxdt)
                   call stats('dpydt',dpydt)
                   call stats('dpzdt',dpzdt)
    end if
    if (do_energy) call stats('dedt' ,dedt)
    if (do_mhd) then
                   call stats('dBxdt',dBxdt)
                   call stats('dBydt',dBydt)
                   call stats('dBzdt',dBzdt)
    end if
    if (do_pscalar)call stats('dddt' ,dddt)
  end if

  if (idbg > 0) then
                   call stats('r' ,r)
    if (do_momenta) then
                   call stats('px',px)
                   call stats('py',py)
                   call stats('pz',pz)
    end if
    if (do_energy) call stats('e' ,e)
    if (do_mhd) then
                   call stats('Bx',Bx)
                   call stats('By',By)
                   call stats('Bz',Bz)
    end if
    if (do_pscalar)call stats('d' ,d)
  end if

  if (dtold == dt) then                                                 ! courant normally sets dtold=dt ..
    td = td + dt                                                        ! for a normal time update ..
  else                                                                  ! except for reduced time steps ...
    td = tsnap*(isnap+isnap0)                                           ! that should result in exact save times
    if (master) print *,'setting exact save time'
  end if
  t = td
END SUBROUTINE
