! $Id: srresist.f90,v 1.6 2012/12/31 16:19:16 aake Exp $
!-----------------------------------------------------------------------
SUBROUTINE resist (eta,rh,P,Uxup,Uyup,Uzup,Bx,By,Bz,flag)

  USE params

  implicit none

  logical, intent(in) :: flag
  real, dimension(mx,my,mz), intent(out) :: eta
  real, dimension(mx,my,mz), intent(in)  :: &
       rh,P,Uxup,Uyup,Uzup,Bx,By,Bz
!hpf$ distribute (*,*,block) :: &
!hpf$  eta,r,ee,Ux,Uy,Uz,Bx,By,Bz
  real, allocatable, dimension(:,:,:) :: &
       Bxc,Byc,Bzc,BB,UdB,cA
!hpf$ distribute (*,*,block) :: &
!hpf$  Uxc,Uyc,Uzc,Bxc,Byc,Bzc,BB,UdB,cA
  integer j
  integer ix, iy, iz
  real fmaxval
  real elapsed, tm(30)
  character(len=mid):: id='$Id: srresist.f90,v 1.6 2012/12/31 16:19:16 aake Exp $'

  if (id.ne.' ') print *,id; id=' '
  if (idbg>1) then; j=1;tm(j)=elapsed(); end if


!-----------------------------------------------------------------------
!  Centered fields
!-----------------------------------------------------------------------
  allocate (Bxc(mx,my,mz), Byc(mx,my,mz), Bzc(mx,my,mz), BB(mx,my,mz))
  call xup_set(Bx,Bxc)
  call yup_set(By,Byc)
  call zup_set(Bz,Bzc)
  allocate (cA(mx,my,mz))

  if (idbg>1) then; j=j+1; tm(j)=elapsed(); print '(a16,2f7.3)','resist: center ',tm(j)-tm(j-1),tm(j)-tm(1); endif

!-----------------------------------------------------------------------
!  Relativistic fast mode speed: Sqrt((B^2 + Gamma P)/(B^2 + rho*h))
!-----------------------------------------------------------------------
!$omp parallel do private(iz)
  do iz=1,mz
    BB(:,:,iz) = max(Bxc(:,:,iz)**2 + Byc(:,:,iz)**2 + Bzc(:,:,iz)**2, 1.e-20)
    cA(:,:,iz) = sqrt((gamma*P(:,:,iz)+BB(:,:,iz))/(BB(:,:,iz) + rh(:,:,iz)))
    cA(:,:,iz) = max((cA(:,:,iz)+abs(Uxup(:,:,iz))), &
                     (cA(:,:,iz)+abs(Uyup(:,:,iz))), &
                     (cA(:,:,iz)+abs(Uzup(:,:,iz))))
  end do
  nflop = nflop+18

  if (idbg>1) then; j=j+1; tm(j)=elapsed(); print '(a16,2f7.3)','resist: A-speed',tm(j)-tm(j-1),tm(j)-tm(1); endif

!-----------------------------------------------------------------------
!  Perpendicular velocity convergence
!-----------------------------------------------------------------------
  if (nu2 .gt. 0.) then
    allocate (UdB(mx,my,mz))
!$omp parallel do private(iz)
    do iz=1,mz
      BB(:,:,iz) = 1./sqrt(BB(:,:,iz))
      Bxc(:,:,iz) = Bxc(:,:,iz)*BB(:,:,iz)
      Byc(:,:,iz) = Byc(:,:,iz)*BB(:,:,iz)
      Bzc(:,:,iz) = Bzc(:,:,iz)*BB(:,:,iz)
      UdB(:,:,iz) = Uxup(:,:,iz)*Bxc(:,:,iz) &
                  + Uyup(:,:,iz)*Byc(:,:,iz) &
                  + Uzup(:,:,iz)*Bzc(:,:,iz)
      Bxc(:,:,iz) = Uxup(:,:,iz)-Bxc(:,:,iz)*UdB(:,:,iz)
      Byc(:,:,iz) = Uyup(:,:,iz)-Byc(:,:,iz)*UdB(:,:,iz)
      Bzc(:,:,iz) = Uzup(:,:,iz)-Bzc(:,:,iz)*UdB(:,:,iz)
    end do
    nflop = nflop+9
    call dif2_set (Bxc, Byc, Bzc, UdB)
  end if
  deallocate (BB, Bxc, Byc, Bzc)

  if (idbg>1) then; j=j+1; tm(j)=elapsed(); print '(a16,2f7.3)','resist: perp   ',tm(j)-tm(j-1),tm(j)-tm(1); endif

!-----------------------------------------------------------------------
!  Combine the contributions, and evaluate the time step constraints
!  from advection and diffusion
!-----------------------------------------------------------------------
  if (nu2 .gt. 0.) then
!$omp parallel do private(iz)
    do iz=1,mz
    do iy=1,my
    do ix=1,mx
      eta(ix,iy,iz) = (nuB*nu3)*cA(ix,iy,iz) + (nuB*nu2*max(dxm(ix),dym(iy),dzm(iz)))*max(UdB(ix,iy,iz),1e-30)
    end do
    end do
    end do
    nflop = nflop+4
    call dump (UdB,'eta.dat',2)
    deallocate (UdB)
  else
!$omp parallel do private(iz)
    do iz=1,mz
      eta(:,:,iz) = (nuB*nu3)*cA(:,:,iz)
    end do
    nflop = nflop+1
  end if

  if (idbg>1) then; j=j+1; tm(j)=elapsed(); print '(a16,2f7.3)','resist: combine',tm(j)-tm(j-1),tm(j)-tm(1); endif

!$omp parallel do private(iz)
  do iz=1,mz
  do iy=1,my
  do ix=1,mx
    cA(ix,iy,iz) = cA(ix,iy,iz)*(dt/min(dxm(ix),dym(iy),dzm(iz)))
  end do
  end do
  end do
  if (flag) Cb = fmaxval('Cb',cA)
!$omp parallel do private(iz)
  do iz=1,mz
  do iy=1,my
  do ix=1,mx
    cA(ix,iy,iz) = eta(ix,iy,iz)*(6.2*3.*0.75/2.*dt/min(dxm(ix),dym(iy),dzm(iz)))
  end do
  end do
  end do
  if (flag) Cn = fmaxval('Cn',cA)
  nflop = nflop+2

  if (idbg>1) then; j=j+1; tm(j)=elapsed(); print '(a16,2f7.3)','resist: maxval ',tm(j)-tm(j-1),tm(j)-tm(1); endif

!-----------------------------------------------------------------------
!  Smooth over 3x3x3 point, to broaden influence.  Use low order shifts
!  when using eta in mhd, to ensure positive definite values.
!-----------------------------------------------------------------------
  call regularize(eta)
  call max5_set(eta,cA)
  call smooth3_set(cA,eta)
  call regularize(eta)
  call dump(eta,'eta.dat',1)
  deallocate (cA)

  if (idbg>1) then; j=j+1; tm(j)=elapsed(); print '(a16,2f7.3)','resist: smooth ',tm(j)-tm(j-1),tm(j)-tm(1); endif

END
