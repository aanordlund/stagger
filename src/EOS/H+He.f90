! $Id: H+He.f90,v 1.12 2005/01/12 12:15:04 troels_h Exp $
!***********************************************************************
module eos
!
!  EOS for hydrogren + neutral Helium plasma
!
  use params
  use units
  implicit none

  real, parameter:: twothirds=2./3.
  real:: ee0, ee1, rho0, rho1, TT0, f_He, mu, m_e, m_H, k_B, eV, chiH, hbar, eosymin
  integer:: calls, totiter

end module

!***********************************************************************
  subroutine pressure(r,ee,p,TT,ne)
    use units
    use eos
    implicit none
    real, dimension(mx,my,mz), intent(in)            :: r,ee
    real, dimension(mx,my,mz), intent(out), optional :: p,ne
    real, dimension(mx,my,mz), intent(out), optional :: TT
    real, dimension(mx,my,mz)                        :: y
    real:: rtsafe
!hpf$ distribute (*,*,block) :: r,ee,P,TT,y
    integer                                          :: ix,iy,iz
    real:: ylast, sec, elapsed, gam1

    if (do_trace) then
      print *,'pressure: begin, P,T,Ne=',present(tt),present(p),present(ne)
      sec = elapsed()
    end if

    if (do_ionization) then

!$omp parallel do private(ix,iy,iz,ylast)
    do iz=1,mz
      ylast=0.5
      do iy=1,my
        do ix=1,mx
          y(ix,iy,iz)=rtsafe(ylast,r(ix,iy,iz),ee(ix,iy,iz))
          ylast=y(ix,iy,iz)
        end do
      end do
    end do
    if (present(ne)) then
!$omp parallel do private(iz)
      do iz=1,mz
        ne(:,:,iz)=y(:,:,iz)*r(:,:,iz)*(cdu/(m_H*mu))
      end do
    end if
    if (present(P)) then
!$omp parallel do private(iz)
      do iz=1,mz
        P(:,:,iz)=twothirds*r(:,:,iz)*(ee(:,:,iz)-y(:,:,iz)*ee0)
      end do
    end if
    if (present(TT)) then
!$omp parallel do private(iz)
      do iz=1,mz
        TT(:,:,iz)=TT0*(twothirds*(ee(:,:,iz)*ee1-y(:,:,iz))/(1.+f_He+y(:,:,iz)))
      end do
    end if

    else
      gam1 = gamma-1.
      if (gamma.eq.1.) gam1=1.
      P = r*ee*gam1
    end if

    if (do_trace) then
      sec = elapsed()
      print *,'pressure: end, sec, ns/z =',sec,sec*1e9/(mx*my*mz)
    end if
  end subroutine pressure

!***********************************************************************
  subroutine eos2(r,ee,P,TT)
    use params
    use eos
    implicit none
    real, dimension(mx,my), intent(in)            :: r,ee
    real, dimension(mx,my), intent(out), optional :: P, TT
    real, dimension(mx,my)                        :: y
    real ionfrac

    y=ionfrac(r,ee)
    if (present(P )) P = twothirds*r*(ee-y*ee0)
    if (present(TT)) TT= twothirds*(ee*ee1-y)/(1.+f_He+y)*TT0
  end subroutine eos2

!***********************************************************************
  subroutine eos1(n1,n2,r,ee,P,TT)
    use params
    use eos
    implicit none
    integer i1,i2,n1,n2
    real, dimension(n1,n2), intent(in)            :: r,ee
    real, dimension(n1,n2), intent(out), optional :: P,TT
    real, dimension(n1,n2)                        :: y
    real:: rtsafe
    real yy

    yy=0.5
    do i2=1,n2
      do i1=1,n1
        yy=rtsafe(yy,r(i1,i2),ee(i1,i2))
        y(i1,i2)=yy
      end do
    end do
    if (present(P )) P  = twothirds*r*(ee-y*ee0)
    if (present(TT)) TT = twothirds*(ee*ee1-y)/(1.+f_He+y)*TT0

  end subroutine

!***********************************************************************
  function ionfrac(r,ee)
!----------------------------------------------------------------
! root-finding loop
!----------------------------------------------------------------
    use params
    use eos
    implicit none
    real, dimension(mx,my)       :: r,ee
    real, dimension(mx,my)       :: ionfrac
    real                         :: ylast
    integer                      :: i,j,k
    real:: rtsafe

    ylast=0.5
    do j=1,my
       do i=1,mx
          ylast=rtsafe(ylast,r(i,j),ee(i,j))
          ionfrac(i,j)=ylast
       end do
    end do
    return
  end function ionfrac

!***********************************************************************
  function rtsafe(ylast,r,ee)
!----------------------------------------------------------------
! safe newton-raphson algorithm
!----------------------------------------------------------------
    real, intent(in)   :: ylast,r,ee
    real               :: rtsafe
    real, parameter    :: yacc=1e-5, yrel=1e-3
    integer, parameter :: maxit=100
    real               :: yh,yl,fh,fl,f,df,dy,dyold,temp
    integer            :: i

!----------------------------------------------------------------------
! The temperature at ionization is around 0.3 eV, or about 40 times
! less than the ionization energy.  We thus loose a factor 40 in 
! precision of the total energy, so we need a low limiting yl value.
! At the temperature where ionization is nearly complete we need 
! a correspondingly less precise limiting yh value.
!----------------------------------------------------------------------

    yl=eosymin                           ! lower bound
    yh=1.-4e-6                        ! upper bound
    calls = calls+1

    if (yh.ge.ee*ee1) yh=ee*ee1-yacc  ! values gt ee make no sense
                                      !   (negative temperatures)
    rtsafe=yh
    call saha(r,ee,rtsafe,fh)         ! check the high range first
    if (fh.ge.0.) return              ! ... it is most common
    rtsafe=yl
    call saha(r,ee,rtsafe,fl)
    if (fl.le.0.) return

    dyold=yh-yl
    dy=dyold
!    if (ylast.ge.yh) ylast=.5*yh      ! check that last value
    rtsafe=ylast                      !   is not out of bounds
    if (rtsafe.ge.yh) rtsafe=.5*yh    ! check that last value
    call saha(r,ee,rtsafe,f,df)

    do i=1,maxit
       if (((rtsafe-yl)*df-f)*((rtsafe-yh)*df-f).gt.0. &
            .or. abs(2.*f).gt.abs(dyold*df)) then
          dyold=dy
          dy=.5*(yh-yl)
          rtsafe=yh-dy
          if (yh.eq.rtsafe) return
       else
          dyold=dy
          dy=f/df
          temp=rtsafe
          rtsafe=rtsafe-dy
          if (temp.eq.rtsafe) return
       end if
       if (abs(dy).lt.yacc.and.abs(f).lt.1.) return
!       if (abs(dy).lt.yacc) return
       call saha(r,ee,rtsafe,f,df)
       if (f.lt.0.) then
          yh=rtsafe
       else
          yl=rtsafe
       end if
    end do
    rtsafe = -1.
!    print *,'error: rtsafe exceeding maximum iterations'
!    print *,'ylast,r,ee',ylast,r,ee
!    stop
  end function rtsafe

!****************************************************************
  subroutine saha(r,ee,y,f,df)
!----------------------------------------------------------------
! we want to find the root of f
!----------------------------------------------------------------
    use params
    use eos
    implicit none
    real, intent(in)               :: r,ee,y
    real, intent(out)              :: f
    real, intent(out), optional    :: df
    real                           :: T1,dT1

    T1=1.5*(1.+f_He+y)/(ee*ee1-y)               ! inverse temperature
    f=-1.5*log(T1)-T1-log(r*rho1*y**2/(1.d0-y)) ! combine logs
    if (present(df)) then                       ! optional derivative
      dT1=(T1+1.5)/(ee*ee1-y)                   !  no need for this in ...
      df=-(dT1/T1)*(1.5+T1)-1./(1d0-y)-2d0/y    !  the many cases with y=0
    end if                                      !  or y=1.
    totiter = totiter+1
  end subroutine saha

!***********************************************************************
  real function eos_iterations (clear)
    integer clear
    eos_iterations=totiter/(calls+1e-6)
    if (clear.eq.0) then
      totiter=0
      calls=0
    end if
  end function


!***********************************************************************
  subroutine init_eos
    USE params
    USE eos
    namelist /eqofst/ do_ionization, eosymin

    m_e =9.109390e-28                                   ! [g]
    m_H =1.673525e-24                                   ! [g]
    k_B =1.380658e-16                                   ! [erg/K]
    hbar=1.054573e-27                                   ! [erg*s]
    eV  =1.602177e-12                                   ! [erg]
    chiH=13.6*eV                                        ! [erg]

    f_He=0.1
    mu  =1.+4*f_He

    rho0 = m_H*mu*((chiH/hbar)*(m_e/(2.*pi*hbar)))**1.5 ! [g/cm^3]
    ee0 = chiH/(m_H*mu)                                 ! [erg/g]
    TT0 = chiH/k_B                                       ! [K]

    rho0 = rho0/cdu
    ee0  = ee0/cku
    print *,'rho0,ee0,TT0',rho0,ee0,TT0

    rho1 = 1./rho0
    ee1  = 1./ee0

    do_ionization = .true.
    eosymin = 1e-4

    call read_eos
  end subroutine init_eos

!***********************************************************************
  subroutine read_eos
    USE params
    USE eos
    namelist /eqofst/ do_ionization, eosymin
    if (stdin.ge.0) then
      rewind (stdin); read (stdin,eqofst)
    else
      read (*,eqofst)
    end if
    write (*,eqofst)
  end subroutine
