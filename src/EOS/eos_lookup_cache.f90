! $Id: eos_lookup_cache.f90,v 1.5 2012/01/22 00:26:02 aake Exp $ 

!*******************************************************************************
MODULE table_m
  integer iupdte,nvar,mrho,mtable,njon,mee
  real(kind=4) eosxmin,ur,ul,ut,eps,tff,grv,abnd,ee1,ee2,dee
  real(kind=4), allocatable, dimension(:):: tmean,tamp,rhm,xcorr,thmin,thmax, &
    dth,eemin,eemax,deetab,tab
  integer, allocatable, dimension(:):: itab,mtab
  integer, parameter:: maxtab = 40000000
END

!*******************************************************************************
MODULE eos
  USE params
  implicit none
  integer mbox
  real(kind=4) dbox
  character(len=mfile):: tablefile
  logical do_eos, do_table, do_log
  real, allocatable, dimension(:,:):: pbot0, dlnpdE_r, dlnpdlnr_E
  logical, allocatable, dimension(:,:,:):: panic
  real, allocatable, dimension(:,:,:,:,:):: table
CONTAINS

!-------------------------------------------------------------------------------
SUBROUTINE eos_generic (lnr,ee,iv,nv,var)
  USE params
  implicit none
  real, dimension(mx,my,mz):: lnr,ee
  real, dimension(nv,mx,my,mz):: var
  integer iv,nv
  logical omp_in_parallel
!...............................................................................
  if (omp_in_parallel()) then
    call eos_omp (lnr,ee,iv,nv,var)
  else
    !$omp parallel
    call eos_omp (lnr,ee,iv,nv,var)
    !$omp end parallel
  end if
END SUBROUTINE

!-------------------------------------------------------------------------------
SUBROUTINE eos_omp (lnr,ee,iv,nv,var)
  USE params
  USE table_m
  implicit none
  real, dimension(mx,my,mz):: lnr,ee
  integer iv,nv
  real, dimension(nv,mx,my,mz):: var
  real lnr1,lnr2,dlnr,pr,qr,pe,qe
  integer ix,iy,iz,ir,ie
!...............................................................................
  call print_trace('EOS',dbg_eos,'BEGIN')

  lnr1=alog(rhm(1))
  lnr2=alog(rhm(mrho))
  dlnr=(lnr2-lnr1)/(mrho-1)

  if (nv == 1) then
    do iz=izs,ize
    do iy=1,my
    do ix=1,mx
      pr = 1.+(lnr(ix,iy,iz)-lnr1)/dlnr
      ir = max(1,min(mrho-1,int(pr)))
      pr = pr-ir
      qr = 1.-pr
      pe = 1.+(ee(ix,iy,iz)-ee1)/dee
      ie = max(1,min(mee-1,int(pe)))
      pe = pe-ie
      qe = 1.-pe
      panic(ix,iy,iz) = (pr > 1.1) .or. (qr > 1.1) .or. (pe > 1.1) .or. (qe > 1.1) 
      var(1,ix,iy,iz) = qr*(qe*table(1,1,iv,ie,ir)+pe*table(2,1,iv,ie,ir)) &
                      + pr*(qe*table(1,2,iv,ie,ir)+pe*table(2,2,iv,ie,ir))
    end do
    end do
    end do
  else if (nv == 2) then
    do iz=izs,ize
    do iy=1,my
    do ix=1,mx
      pr = 1.+(lnr(ix,iy,iz)-lnr1)/dlnr
      ir = max(1,min(mrho-1,int(pr)))
      pr = pr-ir
      qr = 1.-pr
      pe = 1.+(ee(ix,iy,iz)-ee1)/dee
      ie = max(1,min(mee-1,int(pe)))
      pe = pe-ie
      qe = 1.-pe
      panic(ix,iy,iz) = (pr > 1.1) .or. (qr > 1.1) .or. (pe > 1.1) .or. (qe > 1.1) 
      var(1,ix,iy,iz) = qr*(qe*table(1,1,iv  ,ie,ir)+pe*table(2,1,iv  ,ie,ir)) &
                      + pr*(qe*table(1,2,iv  ,ie,ir)+pe*table(2,2,iv  ,ie,ir))
      var(2,ix,iy,iz) = qr*(qe*table(1,1,iv+1,ie,ir)+pe*table(2,1,iv+1,ie,ir)) &
                      + pr*(qe*table(1,2,iv+1,ie,ir)+pe*table(2,2,iv+1,ie,ir))
    end do
    end do
    end do
  else
    if (master) print*,'eos does not support more than 2 variables at a time',nv
    call abort_mpi
    stop
  end if
  call print_trace('EOS',dbg_eos,'END')
END SUBROUTINE
!-------------------------------------------------------------------------------
END MODULE eos

!*******************************************************************************
SUBROUTINE radiation_eos (lnr,ee,ibin,lns,lnk)
  USE params
  USE table_m
  USE eos
  implicit none
  real, dimension(mx,my,mz), intent(in):: lnr,ee
  real, dimension(mx,my,mz), intent(out):: lnk,lns
  integer ibin
  real, allocatable, dimension(:,:,:,:):: var
  logical omp_in_parallel
!...............................................................................
  if (omp_in_parallel()) then
    !$omp master
    allocate (var(2,mx,my,mz))
    !$omp end master
    !$omp barrier
    call eos_omp (lnr,ee,njon+ibin*2-1,2,var)
    lns(:,:,izs:ize)=var(1,:,:,izs:ize)
    lnk(:,:,izs:ize)=var(2,:,:,izs:ize)
    !$omp barrier
    !$omp master
    deallocate (var)
    !$omp end master
  else
    allocate (var(2,mx,my,mz))
    !$omp parallel
    call eos_omp (lnr,ee,njon+ibin*2-1,2,var)
    lns(:,:,izs:ize)=var(1,:,:,izs:ize)
    lnk(:,:,izs:ize)=var(2,:,:,izs:ize)
    !$omp end parallel
    deallocate (var)
  end if
END

!*******************************************************************************
SUBROUTINE pressure (r,ee,pp)
  USE params
  USE arrays, only: lnr=>wk08
  USE eos
  implicit none
  real, dimension(mx,my,mz), intent(in):: r,ee
  real, dimension(mx,my,mz), intent(out):: pp
  logical omp_in_parallel
  integer iz
!...............................................................................
  if (omp_in_parallel()) then
    call eos_omp(lnr,ee,1,1,pp)
    do iz=izs,ize
      pp(:,:,iz) = exp(pp(:,:,iz))
    end do
    dlnpdE_r(:,izs:ize) = 0.
    dlnpdlnr_E(:,izs:ize) = 1.
    if ((izs <= ub) .and. (ize >= ub)) then
      pbot0(:,:) = pp(:,:,ub)
    end if
  else
    !$omp parallel
    call eos_omp(lnr,ee,1,1,pp)
    do iz=izs,ize
      pp(:,:,iz) = exp(pp(:,:,iz))
    end do
    dlnpdE_r(:,izs:ize) = 0.
    dlnpdlnr_E(:,izs:ize) = 1.
    if ((izs <= ub) .and. (ize >= ub)) then
      pbot0(:,:) = pp(:,:,ub)
    end if
    !$omp end parallel
  end if
  call dumpn(r  ,'r'  ,'eos.dmp',0)
  call dumpn(lnr,'lnr','eos.dmp',1)
  call dumpn(pp ,'pp' ,'eos.dmp',1)
END

!*******************************************************************************
SUBROUTINE temperature (rho,ee,tt)
  USE params
  USE arrays, only: lnr=>wk08
  USE eos
  implicit none
  real, dimension(mx,my,mz), intent(in):: rho,ee
  real, dimension(mx,my,mz), intent(out):: tt
  logical omp_in_parallel
!...............................................................................
  if (omp_in_parallel()) then
    lnr(:,:,izs:ize) = alog(rho(:,:,izs:ize))
    call eos_omp(lnr,ee,3,1,tt)
  else
    !$omp parallel
    lnr(:,:,izs:ize) = alog(rho(:,:,izs:ize))
    call eos_omp(lnr,ee,3,1,tt)
    !$omp end parallel
  end if
END

!*******************************************************************************
SUBROUTINE opacity (lnr,ee,rk)
  USE params
  USE eos
  implicit none
  real, dimension(mx,my,mz), intent(in):: lnr,ee
  real, dimension(mx,my,mz), intent(out):: rk
  logical omp_in_parallel
!...............................................................................
  if (omp_in_parallel()) then
    call eos_omp(lnr,ee,1,1,rk)
    rk(:,:,izs:ize) = exp(rk(:,:,izs:ize))
  else
    !$omp parallel
    call eos_omp(lnr,ee,1,1,rk)
    rk(:,:,izs:ize) = exp(rk(:,:,izs:ize))
    !$omp end parallel
  end if
END

!*******************************************************************************
SUBROUTINE read_eos
  USE eos
  USE table_m
  implicit none
  namelist /eqofst/ do_log, do_eos, do_ionization, do_table, tablefile
!...............................................................................
  do_eos = .true.
  do_log = .true.
  do_ionization = .true.
  do_table = .false.
  tablefile = 'table.dat'
  rewind (stdin); read (stdin,eqofst)
  if (master) write (*,eqofst)
END SUBROUTINE

!*******************************************************************************
SUBROUTINE init_eos
  USE params
  USE arrays
  USE eos
  USE table_m
  implicit none
  integer i, rank, iz, ir, ie, iv, jv, kv, jr, ntab, ik
  character(len=mid):: id="$Id: eos_lookup_cache.f90,v 1.5 2012/01/22 00:26:02 aake Exp $"
!...............................................................................
  call print_id(id)
  call read_eos
!-------------------------------------------------------------------------------
! Let the nodes read the table one at a time, to avoid swamping cluster file 
! systems with simultaneous I/O from the same file
!-------------------------------------------------------------------------------
  do rank=0,mpi_size-1
    call barrier_mpi ('init_eos')
    if (rank .eq. mpi_rank) then
      open (12,file=tablefile,form='unformatted',status='old')
      read (12) mrho,iupdte,nvar,mbox,eosxmin,dbox,ul,ut,ur,eps,tff,grv,abnd
      if (do_trace.and.master) print *,mpi_rank,mrho,iupdte,nvar,mbox,eosxmin,dbox,ul,ut,ur,eps,tff,grv,abnd
      njon = nvar-mbox*2
      allocate (tmean(mrho),tamp(mrho),rhm(mrho),xcorr(mrho),thmin(mrho),thmax(mrho))
      allocate (dth(mrho),eemin(mrho),eemax(mrho),deetab(mrho),itab(mrho),mtab(mrho))
      if (do_trace.and.master) print *,mpi_rank, 'reading 12 times', mrho, ' values'
      read (12) tmean,tamp,rhm,xcorr,thmin,thmax,dth,eemin,eemax,deetab,itab,mtab
      read (12) mtable
      allocate (tab(mtable))
      if (master) print *, mpi_rank, 'reading', mtable, ' table values'
      read (12) (tab(i), i=1,mtable)
      if (master) print *, mpi_rank, 'read   ', mtable, ' table values'
      close (12)
    end if
  end do

  ee1 = minval(eemin)                                                           ! lowest energy considered
  ee2 = maxval(eemax)                                                           ! highest energy considered
  dee = deetab(1)                                                               ! assumed to be constant!
  if (any(deetab .ne. dee)) then                                                ! check that they really are
    if (master) print*,'EOS: all energy increments must be equal'
    call abort_mpi
  end if
  mee = floor((ee2-ee1)/dee+1.1)                                                ! number of energy levels
  if (master) print'(1x,a,f5.0,a)', &
    'internal table size =',4.*4.*nvar*(mee-1)*(mrho-1)/1024.**2,' MB'
  allocate (table(2,2,nvar,mee-1,mrho-1))                                       ! create a square table (wastes space but is faster)

  do ir=1,mrho-1                                                                ! for all densities
    do jr=1,2
      ntab = mtab(ir+jr-1)                                                      ! table length for lower density
      ie = floor((eemin(ir+jr-1)-ee1)/dee+1.1)                                  ! first energy index to fill
      if ((ie < 1) .or. (ie+ntab-2) > (mee-1)) then
        if (master) print*,'EOS: error in energy index range',ie,ntab,mee
        call abort_mpi
      end if
      do iv=1,njon                                                              ! for all thermodynamic vars
        i = itab(ir+jr-1)+(iv-1)*3*ntab                                         ! table index
        table(1,jr,iv,ie:ie+ntab-2,ir) = tab(i  :i+ntab-2)                      ! fill with values for lower energy
        table(2,jr,iv,ie:ie+ntab-2,ir) = tab(i+1:i+ntab-1)                      ! fill with values for higher energy
        !print'(3i4,3i6,i9)',ir,jr,iv,ie,ie+ntab-2,mee-(ie+ntab-2),i
      end do
    end do

    do jr=1,2
      ntab = mtab(ir+jr-1)                                                      ! table length for lower density
      ie = floor((eemin(ir+jr-1)-ee1)/dee+1.1)                                  ! first energy index to fill
      jv = njon+1                                                               ! incremental slot in table
      do iv=njon+1,njon+mbox
      do kv=0,1                                                                 ! source and opacity, respectively
        i = itab(ir+jr-1)+(iv-1+kv*mbox)*3*ntab                                 ! alternating source and opacity
        if (i+ntab-2 > mtable) then
          if (master) print*,'EOS: error in table index',i,ntab,mtable
          call abort_mpi
        end if
        table(1,jr,jv,ie:ie+ntab-2,ir) = tab(i  :i+ntab-2)                      ! fill with values for lower energy
        table(2,jr,jv,ie:ie+ntab-2,ir) = tab(i+1:i+ntab-1)                      ! fill with values for higher energy
        if (kv == 1) then                                                       ! multiply opacities with std opacity
          i = itab(ir+jr-1)+3*ntab                                              ! address of ln(rhokap)
          table(1,jr,jv,ie:ie+ntab-2,ir) = table(1,jr,jv,ie:ie+ntab-2,ir)+tab(i  :i+ntab-2)
          table(2,jr,jv,ie:ie+ntab-2,ir) = table(2,jr,jv,ie:ie+ntab-2,ir)+tab(i+1:i+ntab-1)
        end if
	if (.not. do_log) &
          table(:,jr,jv,ie:ie+ntab-2,ir) = exp(table(:,jr,jv,ie:ie+ntab-2,ir))
        !print'(3i4,3i6,i9)',ir,jr,jv,ie,ie+ntab-2,mee-(ie+ntab-2),i
        jv = jv+1
      end do
      end do
    end do
  end do

  allocate (pbot0(mx,mz), dlnpdE_r(mx,mz), dlnpdlnr_E(mx,mz))
  allocate (panic(mx,my,mz))

  if (mtable .gt. maxtab) then
    if (master) print*,'init_table: table size larger than maxtab', mtable, maxtab
    call abort_mpi
    stop
  end if
END SUBROUTINE
