module units
!
!  Ideal gas units
!
  use params
  implicit none

  real, parameter:: cdu=1, ctu=1, clu=1, &
                    cvu=clu/ctu, cku=cvu**2, &
                    cpu=cdu*cku, utemp=1

  real, parameter:: u_r=1, u_t=1, u_l=1, &
                    u_u=u_l/u_t, u_ee=u_u**2, &
                    u_e=u_r*u_ee
  real u_temp

end module units
