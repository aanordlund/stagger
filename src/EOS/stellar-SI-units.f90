module units
!
!  Stellar CGS units
!
  use params
  implicit none

  real, parameter:: cdu=1e-4, ctu=1e2, clu=1e6
  real, parameter:: cvu=clu/ctu, cku=cvu**2, cpu=cdu*cku

  double precision, parameter:: kB=1.38e-23, mproton=1.6726e-27, melectron=9.1094e-31, &
                    echarge=1.602e-19, hplanck=6.6261e-34

end module units
