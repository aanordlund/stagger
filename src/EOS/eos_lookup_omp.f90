! $Id: eos_lookup_omp.f90,v 1.16 2014/12/30 21:28:19 aake Exp $

!**********************************************************************
MODULE table
  integer iupdte,nvar,mrho,mtable,njon
  real(kind=4) eosxmin,ur,ul,ut,eps,tff,grv,abnd
  real(kind=4), allocatable, dimension(:):: tmean,tamp,rhm,xcorr,thmin,thmax, &
    dth,eemin,eemax,deetab,ttab
  integer, allocatable, dimension(:):: itab,mtab
  integer, parameter:: maxtab=4000000
  real(kind=4) tab(maxtab)
  common /ctable/ tab
!$omp threadprivate(/ctable/)
END

!**********************************************************************
MODULE eos
  USE params
  integer mbox
  real(kind=4) dbox
  character(len=mfile):: tablefile
  logical do_eos, do_table
  real, allocatable, dimension(:,:):: pbot0, dlnpdE_r, dlnpdlnr_E
  real, allocatable, dimension(:,:,:,:):: s0, lns
  logical, allocatable, dimension(:,:,:):: panic
CONTAINS

!**********************************************************************
SUBROUTINE equation_of_state (rho,ee,mask,pp,tt,ne,lnrk,lnsource)
!
!  Lookup equation of state table
!----------------------------------------------------------------------
!
  USE params
  USE table
  implicit none
  real, dimension(mx,my,mz):: rho,ee
  real, dimension(mx,my,mz), optional:: pp,tt,ne,lnrk
  real, dimension(mx,my,mz,mbox), optional:: lnsource
  integer mask
!
  real, dimension(mx):: px,py,f00,f01,f10,f11, &
    fx00,fx01,fx10,fx11,fy00,fy01,fy10,fy11
  integer, dimension(mx):: np,np1,ik,ntab,ik1,ntab1
!
  logical newtable
  real rhm1,rhm2,drhm,algrk,eek,eek1,py1,qx,qy,pxqx,pxpx,qxqx,pypy,pyqy,qyqy,pxpy,qxqy,pxqy,qxpy
  integer ix,iy,iz,kee,kee1,j

  if (do_trace) print *,'equation_of_state:',present(pp),present(tt),present(ne),present(lnrk)

1 continue
!
  rhm1=alog(rhm(1))
  rhm2=alog(rhm(mrho))
  drhm=(rhm2-rhm1)/(mrho-1)
!
!  assume no panic
!
  newtable = .false.
  do iz=izs,ize
    panic(:,:,iz)=.false.
  end do
!
!  x,y loop
!
  do iz=izs,ize
  do iy=1,my
    do ix=1,mx
!
!  Density index
!  6 flops
!
      algrk=1.+(alog(rho(ix,iy,iz))-rhm1)/drhm
      np(ix)=max0(1,min0(mrho-1,int(algrk)))
      px(ix)=algrk-np(ix)
      panic(ix,iy,iz)=panic(ix,iy,iz).or.px(ix).lt.-0.01.or.px(ix).gt.1.01
!
!  Internal energy index.  This loop has indirect referencing.
!
! +6 = 12 flops
      ntab(ix)   = mtab(np(ix))
      eek       = 1.+(ee(ix,iy,iz)-eemin(np(ix)))/deetab(np(ix))
      kee       = min(ntab(ix)-1,max(1,ifix(eek)))
      py(ix)     = eek-kee
      panic(ix,iy,iz) = panic(ix,iy,iz).or.py(ix).lt.-0.01.or.py(ix).gt.1.01
      ik(ix)     = itab(np(ix))+kee-1
      ntab1(ix)  = mtab(np(ix)+1)
! +3 = 15 flops
      kee1      = kee - nint((eemin(np(ix)+1)-eemin(np(ix))) &
                                            /deetab(np(ix)))
      kee1      = min0(ntab1(ix)-1,max0(1,kee1))
      ik1(ix)    = itab(np(ix)+1)+kee1-1
!
!  These three lines may be commented out when we are satisfied it never panics
!
      eek1      = 1.+(ee(ix,iy,iz)-eemin(np(ix)+1))/deetab(np(ix)+1)
      py1       = eek1-kee1
      panic(ix,iy,iz) = panic(ix,iy,iz) .or. abs(py(ix)-py1).gt.0.001
      ! if (panic(ix,iy,iz)) then
      !   print*,ix,iy,iz
      ! end if
    end do
!
!  Separate loop for weights, vectorizes well
!
    do ix=1,mx
! +5 = 20 flops
      qx   = 1. - px(ix)
      pxqx = px(ix) * qx
      pxpx = px(ix) * px(ix)
      qxqx = qx * qx
! +4 = 24 flops
      qy   = 1. - py(ix)
      pyqy = py(ix) * qy
      pypy = py(ix) * py(ix)
      qyqy = qy     * qy
! +4 = 28 flops
      pxqy = px(ix) * qy
      pxpy = px(ix) * py(ix)
      qxqy = qx     * qy
      qxpy = qx     * py(ix)
! +5 = 33 flops
      f00(ix) = qxqy * (1. + pxqx - pxpx + pyqy - pypy)
      f01(ix) = qxpy * (1. + pxqx - pxpx + pyqy - qyqy)
      f10(ix) = pxqy * (1. - qxqx + pxqx + pyqy - pypy)
      f11(ix) = pxpy * (1. - qxqx + pxqx + pyqy - qyqy)
! +4 = 37 flops
      fx00(ix) =    qxqy * pxqx
      fx01(ix) =    qxpy * pxqx
      fx10(ix) =  - pxqy * pxqx
      fx11(ix) =  - pxpy * pxqx
! +4 = 41 flops
      fy00(ix) =    qxqy * pyqy
      fy01(ix) =  - qxpy * pyqy
      fy10(ix) =    pxqy * pyqy
      fy11(ix) =  - pxpy * pyqy
    end do
!
!  Loop over table entries, nvar = 4.  Would this be more efficent
!  if split into one gather loop and one calculation loop?
!
    if (iand(mask,1).eq.1) then
     do ix=1,mx
      pp(ix,iy,iz) = exp( &
         f00(ix) * tab(ik (ix))+  f01(ix) * tab(ik (ix) + 1) &
      +  f10(ix) * tab(ik1(ix))+  f11(ix) * tab(ik1(ix) + 1) &
      + fx00(ix) * tab(ik (ix) + 2 * ntab (ix)    ) &
      + fx01(ix) * tab(ik (ix) + 2 * ntab (ix) + 1) &
      + fx10(ix) * tab(ik1(ix) + 2 * ntab1(ix)    ) &
      + fx11(ix) * tab(ik1(ix) + 2 * ntab1(ix) + 1) &
      + fy00(ix) * tab(ik (ix) +     ntab (ix)    ) &
      + fy01(ix) * tab(ik (ix) +     ntab (ix) + 1) &
      + fy10(ix) * tab(ik1(ix) +     ntab1(ix)    ) &
      + fy11(ix) * tab(ik1(ix) +     ntab1(ix) + 1) &
        )
     end do
    end if
    do ix=1,mx
      ik (ix) = ik (ix) + 3*ntab (ix)
      ik1(ix) = ik1(ix) + 3*ntab1(ix)
    end do
    if (iand(mask,2).eq.2) then
     do ix=1,mx
      lnrk(ix,iy,iz) = ( &
         f00(ix) * tab(ik (ix))+  f01(ix) * tab(ik (ix) + 1) &
      +  f10(ix) * tab(ik1(ix))+  f11(ix) * tab(ik1(ix) + 1) &
      + fx00(ix) * tab(ik (ix) + 2 * ntab (ix)    ) &
      + fx01(ix) * tab(ik (ix) + 2 * ntab (ix) + 1) &
      + fx10(ix) * tab(ik1(ix) + 2 * ntab1(ix)    ) &
      + fx11(ix) * tab(ik1(ix) + 2 * ntab1(ix) + 1) &
      + fy00(ix) * tab(ik (ix) +     ntab (ix)    ) &
      + fy01(ix) * tab(ik (ix) +     ntab (ix) + 1) &
      + fy10(ix) * tab(ik1(ix) +     ntab1(ix)    ) &
      + fy11(ix) * tab(ik1(ix) +     ntab1(ix) + 1) &
        )
     end do
    end if
    do ix=1,mx
      ik (ix) = ik (ix) + 3*ntab (ix)
      ik1(ix) = ik1(ix) + 3*ntab1(ix)
    end do
    if (iand(mask,4).eq.4) then
     do ix=1,mx
      tt(ix,iy,iz) = ( &
         f00(ix) * tab(ik (ix))+  f01(ix) * tab(ik (ix) + 1) &
      +  f10(ix) * tab(ik1(ix))+  f11(ix) * tab(ik1(ix) + 1) &
      + fx00(ix) * tab(ik (ix) + 2 * ntab (ix)    ) &
      + fx01(ix) * tab(ik (ix) + 2 * ntab (ix) + 1) &
      + fx10(ix) * tab(ik1(ix) + 2 * ntab1(ix)    ) &
      + fx11(ix) * tab(ik1(ix) + 2 * ntab1(ix) + 1) &
      + fy00(ix) * tab(ik (ix) +     ntab (ix)    ) &
      + fy01(ix) * tab(ik (ix) +     ntab (ix) + 1) &
      + fy10(ix) * tab(ik1(ix) +     ntab1(ix)    ) &
      + fy11(ix) * tab(ik1(ix) +     ntab1(ix) + 1) &
        )
     end do
    end if
    do ix=1,mx
      ik (ix) = ik (ix) + 3*ntab (ix)
      ik1(ix) = ik1(ix) + 3*ntab1(ix)
    end do
    if (iand(mask,8).eq.8) then
     do ix=1,mx
      ne(ix,iy,iz) = exp( &
         f00(ix) * tab(ik (ix))+  f01(ix) * tab(ik (ix) + 1) &
      +  f10(ix) * tab(ik1(ix))+  f11(ix) * tab(ik1(ix) + 1) &
      + fx00(ix) * tab(ik (ix) + 2 * ntab (ix)    ) &
      + fx01(ix) * tab(ik (ix) + 2 * ntab (ix) + 1) &
      + fx10(ix) * tab(ik1(ix) + 2 * ntab1(ix)    ) &
      + fx11(ix) * tab(ik1(ix) + 2 * ntab1(ix) + 1) &
      + fy00(ix) * tab(ik (ix) +     ntab (ix)    ) &
      + fy01(ix) * tab(ik (ix) +     ntab (ix) + 1) &
      + fy10(ix) * tab(ik1(ix) +     ntab1(ix)    ) &
      + fy11(ix) * tab(ik1(ix) +     ntab1(ix) + 1) &
        )
     end do
    end if
    if (iand(mask,16).eq.16) then
     do j=1,mbox
     do ix=1,mx
      ik (ix) = ik (ix) + 3*ntab (ix)
      ik1(ix) = ik1(ix) + 3*ntab1(ix)
      lnsource(ix,iy,iz,j) = ( &
         f00(ix) * tab(ik (ix))+  f01(ix) * tab(ik (ix) + 1) &
      +  f10(ix) * tab(ik1(ix))+  f11(ix) * tab(ik1(ix) + 1) &
      + fx00(ix) * tab(ik (ix) + 2 * ntab (ix)    ) &
      + fx01(ix) * tab(ik (ix) + 2 * ntab (ix) + 1) &
      + fx10(ix) * tab(ik1(ix) + 2 * ntab1(ix)    ) &
      + fx11(ix) * tab(ik1(ix) + 2 * ntab1(ix) + 1) &
      + fy00(ix) * tab(ik (ix) +     ntab (ix)    ) &
      + fy01(ix) * tab(ik (ix) +     ntab (ix) + 1) &
      + fy10(ix) * tab(ik1(ix) +     ntab1(ix)    ) &
      + fy11(ix) * tab(ik1(ix) +     ntab1(ix) + 1) &
        )
     end do
     end do
    end if
!
! End outer loops
!
  end do
  end do
!
!  Any cause for panic ?
!
  do iz=izs,ize
  do iy=1,my
  do ix=1,mx
    if (panic(ix,iy,iz)) then
      algrk=1.+(alog(rho(ix,iy,iz))-rhm1)/drhm
      np(ix)=max0(1,min0(mrho-1,int(algrk)))
      ntab(ix) = mtab(np(ix))
      ntab1(ix)= mtab(np(ix)+1)
      eek= 1.+(ee(ix,iy,iz)-eemin(np(ix)))/deetab(np(ix))
      kee= min0(ntab(ix)-1,max0(1,ifix(eek)))
      kee1= kee - nint((eemin(np(ix)+1)-eemin(np(ix)))/deetab(np(ix)))
      kee1= min0(ntab1(ix)-1,max0(1,kee1))
      py(ix)= eek-kee
      eek1= 1.+(ee(ix,iy,iz)-eemin(np(ix)+1))/deetab(np(ix)+1)
      py1  = eek1-kee1
      print '(a)','ix,iy,iz,lnrho,rhm1,rhm2,np(ix),algrk:'
      print '(3i4,3f8.3,i4,f8.3)',ix,iy,iz,alog(rho(ix,iy,iz)),rhm1,rhm2,np(ix),algrk
      print '(a)','ee(ix,iy,iz),eemin,eemax,eek,kee,py(ix),ntab:'
      print '(a)','                         eek1,kee1,py1,ntab1:'
      print '(4f8.3,i4,f8.3,i4/24x,f8.3,i4,f8.3,i4)',ee(ix,iy,iz), &
        eemin(np(ix)),eemax(np(ix)),eek,kee,py(ix),ntab(ix),eek1,kee1,py1,ntab1(ix)
      newtable = .true.
    endif
  end do
  end do
  end do

!  Make a new table?

  if (newtable) then
!   call newtab
    print *,'Outside table in equation_of_state!'
    call end_mpi
    stop
!    mtable=0
!    goto 1
  end if
END SUBROUTINE

END MODULE eos

!**********************************************************************
SUBROUTINE init_eos
  USE params
  USE arrays
  USE eos
  USE table
  implicit none
  integer i, rank, iz
  character(len=mid):: id="$Id: eos_lookup_omp.f90,v 1.16 2014/12/30 21:28:19 aake Exp $"

  call print_id(id)
  call read_eos

  do rank=0,mpi_size-1
   call barrier_mpi ('init_eos')
   if (rank .eq. mpi_rank) then
    open (12,file=tablefile,form='unformatted',status='old')
    read (12) mrho,iupdte,nvar,mbox,eosxmin,dbox,ul,ut,ur,eps,tff,grv,abnd
    if (do_trace) print *,mpi_rank,mrho,iupdte,nvar,mbox,eosxmin,dbox,ul,ut,ur,eps,tff,grv,abnd
    njon = nvar-mbox
    allocate (tmean(mrho),tamp(mrho),rhm(mrho),xcorr(mrho),thmin(mrho),thmax(mrho))
    allocate (dth(mrho),eemin(mrho),eemax(mrho),deetab(mrho),itab(mrho),mtab(mrho))
    if (do_trace) print *,mpi_rank, 'reading 12 times', mrho, ' values'
    read (12) tmean,tamp,rhm,xcorr,thmin,thmax,dth,eemin,eemax,deetab,itab,mtab
    read (12) mtable
    allocate (ttab(mtable))
    if (master .or. do_trace) print *, mpi_rank, 'reading', mtable, ' table values'
    read (12) (ttab(i), i=1,mtable)
    if (master .or. do_trace) print *, mpi_rank, 'read   ', mtable, ' table values'
    close (12)
   end if
  end do

  allocate (pbot0(mx,mz), dlnpdE_r(mx,mz), dlnpdlnr_E(mx,mz))
  allocate (panic(mx,my,mz))
  allocate (lns(mx,my,mz,mbox))

!
!  Private copy of the table
!
!$omp parallel
  if (mtable .gt. maxtab) then
    call end_mpi
    stop
  end if
  tab(1:mtable) = ttab
  do iz=izs,ize
    panic(:,:,iz) = .false.
    lns(:,:,iz,:) = 0.
  end do
!$omp end parallel
END SUBROUTINE

!**********************************************************************
SUBROUTINE read_eos
  USE eos
  USE table
  implicit none
  namelist /eqofst/ do_eos, do_ionization, do_table, tablefile

  do_eos = .true.
  do_ionization = .true.
  do_table = .false.
  tablefile = 'table.dat'
  if (do_trace) write(*,eqofst)
  rewind (stdin); read (stdin,eqofst)
  if (master) write (*,eqofst)
END SUBROUTINE

!**********************************************************************
SUBROUTINE lookup (rho,ee,lnrk,lnsource)
!
!  Lookup equation of state table
!----------------------------------------------------------------------
!
  USE params
  USE eos
  USE table
  implicit none
  real, dimension(mx,my,mz), intent(in):: rho,ee
  real, dimension(mx,my,mz), intent(out):: lnrk
  real, dimension(mx,my,mz,mbox), intent(out):: lnsource
!
  real, dimension(mx):: px,py,f00,f01,f10,f11, &
    fx00,fx01,fx10,fx11,fy00,fy01,fy10,fy11
  integer, dimension(mx):: np,np1,ik,ntab,ik1,ntab1
!
  real rhm1,rhm2,drhm,algrk,eek,qx,qy,pxqx,pxpx,qxqx,pypy,pyqy,qyqy,pxpy,qxqy,pxqy,qxpy
  integer ix,iy,iz,kee,kee1,j
  logical omp_in_parallel
!
  if (do_trace .and. omp_master) print *,'lookup: begin'
  rhm1=alog(rhm(1))
  rhm2=alog(rhm(mrho))
  drhm=(rhm2-rhm1)/(mrho-1)
!
!  x,y loop
!
  do iz=izs,ize
  do iy=1,my
    do ix=1,mx
!
!  Density index
!  6 flops
!
      algrk=1.+(alog(rho(ix,iy,iz))-rhm1)/drhm
      np(ix)=max0(1,min0(mrho-1,int(algrk)))
      px(ix)=algrk-np(ix)
!
!  Internal energy index.  This loop has indirect referencing.
!
! +6 = 12 flops
      ntab(ix)   = mtab(np(ix))
      eek       = 1.+(ee(ix,iy,iz)-eemin(np(ix)))/deetab(np(ix))
      kee       = min(ntab(ix)-1,max(1,ifix(eek)))
      py(ix)     = eek-kee
      ik(ix)     = itab(np(ix))+kee-1
      ntab1(ix)  = mtab(np(ix)+1)
! +3 = 15 flops
      kee1      = kee - nint((eemin(np(ix)+1)-eemin(np(ix))) &
                                            /deetab(np(ix)))
      kee1      = min0(ntab1(ix)-1,max0(1,kee1))
      ik1(ix)    = itab(np(ix)+1)+kee1-1
    end do
!
!  Loop over table entries, nvar = 4.  Would this be more efficent
!  if split into one gather loop and one calculation loop?
!
    do ix=1,mx
      ik (ix) = ik (ix) + 3*ntab (ix)
      ik1(ix) = ik1(ix) + 3*ntab1(ix)
      lnrk(ix,iy,iz) = &
        (1.-px(ix))*((1.-py(ix))*tab(ik (ix)) +  py(ix)*tab(ik (ix)+1)) &
      +     px(ix) *((1.-py(ix))*tab(ik1(ix)) +  py(ix)*tab(ik1(ix)+1))
      ik (ix) = ik (ix) + 3*ntab (ix)*(njon-2)
      ik1(ix) = ik1(ix) + 3*ntab1(ix)*(njon-2)
    end do
    do j=1,mbox
     do ix=1,mx
      ik (ix) = ik (ix) + 3*ntab (ix)
      ik1(ix) = ik1(ix) + 3*ntab1(ix)
      lnsource(ix,iy,iz,j) = &
        (1.-px(ix))*((1.-py(ix))*tab(ik (ix)) +  py(ix)*tab(ik (ix)+1)) &
      +     px(ix) *((1.-py(ix))*tab(ik1(ix)) +  py(ix)*tab(ik1(ix)+1))
     end do
    end do
!
! End outer loops
!
  end do
  end do
  if (do_trace .and. omp_master) print *,'lookup: end'

END SUBROUTINE

!**********************************************************************
SUBROUTINE pressure (rho,ee,pp)
!
!  Lookup equation of state table
!----------------------------------------------------------------------
!
  USE params
  USE eos
  USE table
  implicit none
  real, dimension(mx,my,mz), intent(in):: rho,ee
  real, dimension(mx,my,mz), intent(out):: pp
!
  real, dimension(mx):: px,py,lnpp
  integer, dimension(mx):: np,np1,ik,ntab,ik1,ntab1
!
  logical newtable
  real rhm1,rhm2,drhm,algrk,eek,eek1,py1,qx,qy,pxqx,pxpx,qxqx,pypy,pyqy,qyqy,pxpy,qxqy,pxqy,qxpy
  integer ix,iy,iz,kee,kee1,j

1 continue
!
  if (do_trace .and. omp_master) print *,'pressure: begin', omp_mythread
  rhm1=alog(rhm(1))
  rhm2=alog(rhm(mrho))
  drhm=(rhm2-rhm1)/(mrho-1)
!
!  assume no panic
!
  newtable = .false.
!
!  x,y loop
!
  do iz=izs,ize
  do iy=1,my
    do ix=1,mx
!
!  Density index
!  6 flops
!
      algrk=1.+(alog(rho(ix,iy,iz))-rhm1)/drhm
      np(ix)=max0(1,min0(mrho-1,int(algrk)))
      px(ix)=algrk-np(ix)
      panic(ix,iy,iz)=px(ix).lt.-0.01.or.px(ix).gt.1.01
!
!  Internal energy index.  This loop has indirect referencing.
!
! +6 = 12 flops
      ntab(ix)   = mtab(np(ix))
      eek       = 1.+(ee(ix,iy,iz)-eemin(np(ix)))/deetab(np(ix))
      kee       = min(ntab(ix)-1,max(1,ifix(eek)))
      py(ix)     = eek-kee
      panic(ix,iy,iz) = panic(ix,iy,iz).or.py(ix).lt.-0.01.or.py(ix).gt.1.01
      ik(ix)     = itab(np(ix))+kee-1
      ntab1(ix)  = mtab(np(ix)+1)
! +3 = 15 flops
      kee1      = kee - nint((eemin(np(ix)+1)-eemin(np(ix))) &
                                            /deetab(np(ix)))
      kee1      = min0(ntab1(ix)-1,max0(1,kee1))
      ik1(ix)    = itab(np(ix)+1)+kee1-1
!
!  These three lines may be commented out when we are satisfied it never panics
!
      eek1      = 1.+(ee(ix,iy,iz)-eemin(np(ix)+1))/deetab(np(ix)+1)
      py1       = eek1-kee1
      panic(ix,iy,iz) = panic(ix,iy,iz) .or. abs(py(ix)-py1).gt.0.001
      ! if (panic(ix,iy,iz)) then
      !   print*,ix,iy,iz
      ! end if
    end do
!
!  Loop over table entries, nvar = 4.  Would this be more efficent
!  if split into one gather loop and one calculation loop?
!
    do ix=1,mx
      lnpp(ix) =  &
        (1.-px(ix))*((1.-py(ix))*tab(ik (ix)) +  py(ix)*tab(ik (ix)+1)) &
      +     px(ix) *((1.-py(ix))*tab(ik1(ix)) +  py(ix)*tab(ik1(ix)+1))
    end do
    call expn (mx, lnpp, pp(:,iy,iz))
    if (iy.eq.ub) then
!                               if (do_trace) print *,'pressure: crit1', omp_mythread
      do ix=1,mx
        pbot0(ix,iz) = pp(ix,ub,iz)
        dlnpdE_r(ix,iz) = ( &
          (1.-px(ix)) * (1.-py(ix)) * tab(ik(ix)  + ntab(ix)     ) &
      +   (1.-px(ix)) *  py(ix)     * tab(ik(ix)  + ntab(ix)  + 1) &
      +    px(ix)     * (1.-py(ix)) * tab(ik1(ix) + ntab1(ix)    ) &
      +    px(ix)     *  py(ix)     * tab(ik1(ix) + ntab1(ix) + 1) )/deetab(np(ix))
        dlnpdlnr_E(ix,iz) = ( &
          (1.-px(ix)) * (1.-py(ix)) * tab(ik(ix)  + 2*ntab(ix)     ) &
      +   (1.-px(ix)) *  py(ix)     * tab(ik(ix)  + 2*ntab(ix)  + 1) &
      +    px(ix)     * (1.-py(ix)) * tab(ik1(ix) + 2*ntab1(ix)    ) &
      +    px(ix)     *  py(ix)     * tab(ik1(ix) + 2*ntab1(ix) + 1) )/drhm
      end do
!                               if (do_trace) print *,'pressure: crit2', omp_mythread
    end if
!
! End outer loops
!
  end do
  end do

!
!  Any cause for panic ?
!
   do iz=izs,ize
   do iy=1,my
   do ix=1,mx
     if (panic(ix,iy,iz)) then
       algrk=1.+(alog(rho(ix,iy,iz))-rhm1)/drhm
       np(ix)=max0(1,min0(mrho-1,int(algrk)))
       ntab(ix) = mtab(np(ix))
       ntab1(ix)= mtab(np(ix)+1)
       eek= 1.+(ee(ix,iy,iz)-eemin(np(ix)))/deetab(np(ix))
       kee= min0(ntab(ix)-1,max0(1,ifix(eek)))
       kee1= kee - nint((eemin(np(ix)+1)-eemin(np(ix)))/deetab(np(ix)))
       kee1= min0(ntab1(ix)-1,max0(1,kee1))
       py(ix)= eek-kee
       eek1= 1.+(ee(ix,iy,iz)-eemin(np(ix)+1))/deetab(np(ix)+1)
       py1 = eek1-kee1
       print '(a)','ix,iy,iz,lnrho,rhm1,rhm2,np(ix),algrk:'
       print '(3i4,3f8.3,i4,f8.3)',ix,iy,iz,alog(rho(ix,iy,iz)),rhm1,rhm2,np(ix),algrk
       print '(a)','ee(ix,iy,iz),eemin,eemax,eek,kee,py(ix),ntab:'
       print '(a)','                         eek1,kee1,py1,ntab1:'
       print '(4f8.3,i4,f8.3,i4/24x,f8.3,i4,f8.3,i4)',ee(ix,iy,iz), &
         eemin(np(ix)),eemax(np(ix)),eek,kee,py(ix),ntab(ix),eek1,kee1,py1,ntab1(ix)
        newtable = .true.
     endif
   end do
   end do
   end do

   if (newtable) then
!!   call newtab
     print *,'Outside table in pressure!'
     call end_mpi
     stop
!!    mtable=0
!!    goto 1
   end if

  if (do_trace .and. omp_master) print *,'pressure: end', omp_mythread
END SUBROUTINE

!**********************************************************************
SUBROUTINE temperature (rho,ee,tt)
  USE params
  implicit none
  real, dimension(mx,my,mz), intent(in):: rho,ee
  real, dimension(mx,my,mz), intent(out):: tt
  logical omp_in_parallel
!----------------------------------------------------------------------
  if (omp_in_parallel()) then
    call temperature_omp (rho,ee,tt)
  else
    !$omp parallel
    call temperature_omp (rho,ee,tt)
    !$omp end parallel
  end if
END

!**********************************************************************
SUBROUTINE temperature_omp (rho,ee,tt)
!
!  Lookup equation of state table
!----------------------------------------------------------------------
!
  USE params
  USE eos
  USE table
  implicit none
  real, dimension(mx,my,mz), intent(in):: rho,ee
  real, dimension(mx,my,mz), intent(out):: tt
!
  real, dimension(mx):: px,py,f00,f01,f10,f11, &
    fx00,fx01,fx10,fx11,fy00,fy01,fy10,fy11
  integer, dimension(mx):: np,np1,ik,ntab,ik1,ntab1
!
  logical newtable
  real rhm1,rhm2,drhm,algrk,eek,eek1,py1,qx,qy,pxqx,pxpx,qxqx,pypy,pyqy,qyqy,pxpy,qxqy,pxqy,qxpy
  integer ix,iy,iz,kee,kee1,j

1 continue
  if (do_trace .and. omp_master) print *,'temperature: begin', omp_mythread
!
  rhm1=alog(rhm(1))
  rhm2=alog(rhm(mrho))
  drhm=(rhm2-rhm1)/(mrho-1)
!
!  assume no panic
!
  newtable = .false.
!
!  x,y loop
!
  do iz=izs,ize
  do iy=1,my
    do ix=1,mx
!
!  Density index
!  6 flops
!
      algrk=1.+(alog(rho(ix,iy,iz))-rhm1)/drhm
      np(ix)=max0(1,min0(mrho-1,int(algrk)))
      px(ix)=algrk-np(ix)
      panic(ix,iy,iz)=px(ix).lt.-0.01.or.px(ix).gt.1.01
!
!  Internal energy index.  This loop has indirect referencing.
!
! +6 = 12 flops
      ntab(ix)   = mtab(np(ix))
      eek       = 1.+(ee(ix,iy,iz)-eemin(np(ix)))/deetab(np(ix))
      kee       = min(ntab(ix)-1,max(1,ifix(eek)))
      py(ix)     = eek-kee
      panic(ix,iy,iz) = panic(ix,iy,iz).or.py(ix).lt.-0.01.or.py(ix).gt.1.01
      ik(ix)     = itab(np(ix))+kee-1
      ntab1(ix)  = mtab(np(ix)+1)
! +3 = 15 flops
      kee1      = kee - nint((eemin(np(ix)+1)-eemin(np(ix))) &
                                            /deetab(np(ix)))
      kee1      = min0(ntab1(ix)-1,max0(1,kee1))
      ik1(ix)    = itab(np(ix)+1)+kee1-1
!
!  These three lines may be commented out when we are satisfied it never panics
!
      eek1      = 1.+(ee(ix,iy,iz)-eemin(np(ix)+1))/deetab(np(ix)+1)
      py1       = eek1-kee1
      panic(ix,iy,iz) = panic(ix,iy,iz) .or. abs(py(ix)-py1).gt.0.001
      ! if (panic(ix,iy,iz)) then
      !   print*,ix,iy,iz
      ! end if
    end do
!
!  Separate loop for weights, vectorizes well
!
    do ix=1,mx
! +5 = 20 flops
      qx   = 1. - px(ix)
      pxqx = px(ix) * qx
      pxpx = px(ix) * px(ix)
      qxqx = qx * qx
! +4 = 24 flops
      qy   = 1. - py(ix)
      pyqy = py(ix) * qy
      pypy = py(ix) * py(ix)
      qyqy = qy     * qy
! +4 = 28 flops
      pxqy = px(ix) * qy
      pxpy = px(ix) * py(ix)
      qxqy = qx     * qy
      qxpy = qx     * py(ix)
! +5 = 33 flops
      f00(ix) = qxqy * (1. + pxqx - pxpx + pyqy - pypy)
      f01(ix) = qxpy * (1. + pxqx - pxpx + pyqy - qyqy)
      f10(ix) = pxqy * (1. - qxqx + pxqx + pyqy - pypy)
      f11(ix) = pxpy * (1. - qxqx + pxqx + pyqy - qyqy)
! +4 = 37 flops
      fx00(ix) =    qxqy * pxqx
      fx01(ix) =    qxpy * pxqx
      fx10(ix) =  - pxqy * pxqx
      fx11(ix) =  - pxpy * pxqx
! +4 = 41 flops
      fy00(ix) =    qxqy * pyqy
      fy01(ix) =  - qxpy * pyqy
      fy10(ix) =    pxqy * pyqy
      fy11(ix) =  - pxpy * pyqy
    end do
!
!  Loop over table entries, nvar = 4.  Would this be more efficent
!  if split into one gather loop and one calculation loop?
!
    do ix=1,mx
      ik (ix) = ik (ix) + 3*ntab (ix)*2
      ik1(ix) = ik1(ix) + 3*ntab1(ix)*2
      tt(ix,iy,iz) = ( &
         f00(ix) * tab(ik (ix))+  f01(ix) * tab(ik (ix) + 1) &
      +  f10(ix) * tab(ik1(ix))+  f11(ix) * tab(ik1(ix) + 1) &
      + fx00(ix) * tab(ik (ix) + 2 * ntab (ix)    ) &
      + fx01(ix) * tab(ik (ix) + 2 * ntab (ix) + 1) &
      + fx10(ix) * tab(ik1(ix) + 2 * ntab1(ix)    ) &
      + fx11(ix) * tab(ik1(ix) + 2 * ntab1(ix) + 1) &
      + fy00(ix) * tab(ik (ix) +     ntab (ix)    ) &
      + fy01(ix) * tab(ik (ix) +     ntab (ix) + 1) &
      + fy10(ix) * tab(ik1(ix) +     ntab1(ix)    ) &
      + fy11(ix) * tab(ik1(ix) +     ntab1(ix) + 1) &
        )
    end do
!
! End outer loops
!
  end do
  end do
!
!  Any cause for panic ?
!
  do iz=izs,ize
  do iy=1,my
  do ix=1,mx
    if (panic(ix,iy,iz)) then
      algrk=1.+(alog(rho(ix,iy,iz))-rhm1)/drhm
      np(ix)=max0(1,min0(mrho-1,int(algrk)))
      ntab(ix) = mtab(np(ix))
      ntab1(ix)= mtab(np(ix)+1)
      eek= 1.+(ee(ix,iy,iz)-eemin(np(ix)))/deetab(np(ix))
      kee= min0(ntab(ix)-1,max0(1,ifix(eek)))
      kee1= kee - nint((eemin(np(ix)+1)-eemin(np(ix)))/deetab(np(ix)))
      kee1= min0(ntab1(ix)-1,max0(1,kee1))
      py(ix)= eek-kee
      eek1= 1.+(ee(ix,iy,iz)-eemin(np(ix)+1))/deetab(np(ix)+1)
      py1  = eek1-kee1
      print '(a)','ix,iy,iz,lnrho,rhm1,rhm2,np(ix),algrk:'
      print '(3i4,3f8.3,i4,f8.3)',ix,iy,iz,alog(rho(ix,iy,iz)),rhm1,rhm2,np(ix),algrk
      print '(a)','ee(ix,iy,iz),eemin,eemax,eek,kee,py(ix),ntab:'
      print '(a)','                         eek1,kee1,py1,ntab1:'
      print '(4f8.3,i4,f8.3,i4/24x,f8.3,i4,f8.3,i4)',ee(ix,iy,iz), &
        eemin(np(ix)),eemax(np(ix)),eek,kee,py(ix),ntab(ix),eek1,kee1,py1,ntab1(ix)
      newtable = .true.
    endif
  end do
  end do
  end do

!  Make a new table?

  if (newtable) then
!   call newtab
    print *,'Outside table in temperature!'
    call end_mpi
    stop
!    mtable=0
!    goto 1
  end if
  if (do_trace .and. omp_master) print *,'temperature: end', omp_mythread
END SUBROUTINE

SUBROUTINE temperature1 (rho,ee,tt)
  USE params
  USE eos
  implicit none
  real, dimension(mx,my,mz), intent(in):: rho,ee
  real, dimension(mx,my,mz), intent(out):: tt
  call equation_of_state (rho,ee,4,tt=tt)
END SUBROUTINE
