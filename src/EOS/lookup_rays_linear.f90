! $Id: lookup_rays_linear.f90,v 1.2 2014/12/31 13:36:33 aake Exp $ 
!=======================================================================
SUBROUTINE lookup_rays (mray, mtau, lnrho, ee, rhokap, sources)
!
!  Lookup opacity and source function.  To speed up, all source functions
!  are computed and cashed in the 1st call.
!-----------------------------------------------------------------------
  USE params
  USE eos
  USE table
  implicit none
  integer:: mray, mtau, ibin
  real, dimension(mray,mtau), intent(in):: lnrho, ee
  real, dimension(mray,mtau), intent(out):: rhokap
  real, dimension(mray,mtau,mbox), intent(out):: sources
!
  integer, parameter:: nvect=32
  integer:: np,kee1,kee2,ir,itau,jbin,iv,ivmax
  real   :: rhm1,rhm2,drhm,algrk,eek
  integer, dimension(nvect):: ik1,ik2,ntab1,ntab2
  real   , dimension(nvect):: px,py,tab1,tab2,tab3,tab4,tmp
  integer(8), save:: ntot=0
  logical debug2
!.......................................................................
  call print_trace('lookup_rays',dbg_eos,'BEGIN')
  
  rhm1 = log(rhm(1))
  rhm2 = log(rhm(mrho))
  drhm = (rhm2-rhm1)/(mrho-1)
!
  !$omp do private(itau,ir,ivmax,iv,algrk,np,px,ntab1,ntab2,eek,kee1,py,kee2,ik1,ik2,jbin)
  do itau=1,mtau
    do ir=0,mray-1,nvect
    ivmax=min(nvect,mray-ir)
    do iv=1,ivmax
      algrk     = 1. + (lnrho(ir+iv,itau)-rhm1)/drhm
      np        = max0(1,min0(mrho-1,floor(algrk)))
      px(iv)    = algrk - np
      ntab1(iv) = mtab(np)
      ntab2(iv) = mtab(np+1)
      eek       = 1. + (ee(ir+iv,itau)-eemin(np))/deetab(np)
      kee1      = min(ntab1(iv)-1,max(1,floor(eek)))
      py(iv)    = eek - kee1
      kee2      = kee1 - nint((eemin(np+1)-eemin(np))/deetab(np))
      kee2      = min0(ntab2(iv)-1,max0(1,kee2))
      ik1(iv)   = itab(np  ) + kee1 - 1 + ntab1(iv) 
      ik2(iv)   = itab(np+1) + kee2 - 1 + ntab2(iv)
    end do
    !-------------------------------------------------------------------
    ! Cache table values in separate loop, which doesn't vectorize
    !-------------------------------------------------------------------
    do iv=1,ivmax
      tab1(iv) = tab(ik1(iv))
      tab2(iv) = tab(ik1(iv)+1)
      tab3(iv) = tab(ik2(iv))
      tab4(iv) = tab(ik2(iv)+1)
    end do
    !-------------------------------------------------------------------
    ! This loop vectorizes
    !-------------------------------------------------------------------
    do iv=1,ivmax
      rhokap(ir+iv,itau) = &
        (1-px(iv))*((1.-py(iv))*tab1(iv) +  py(iv)*tab2(iv)) &
      +    px(iv) *((1.-py(iv))*tab3(iv) +  py(iv)*tab4(iv))
    end do
    do iv=1,ivmax
      ik1(iv) = ik1(iv) + ntab1(iv)*(njon-1)
      ik2(iv) = ik2(iv) + ntab2(iv)*(njon-1)
    end do
    do jbin=1,mbox
      !-----------------------------------------------------------------
      ! Cache table values in separate loop, which doesn't vectorize
      !-----------------------------------------------------------------
      do iv=1,ivmax                                                     ! indirect address loop
        tab1(iv) = tab(ik1(iv))
        tab2(iv) = tab(ik1(iv)+1)
        tab3(iv) = tab(ik2(iv))
        tab4(iv) = tab(ik2(iv)+1)
      end do
      !-----------------------------------------------------------------
      ! Return S
      !-----------------------------------------------------------------
      do iv=1,ivmax
        sources(ir+iv,itau,jbin) = &
          (1-px(iv))*((1.-py(iv))*tab1(iv) +  py(iv)*tab2(iv)) &
        +    px(iv) *((1.-py(iv))*tab3(iv) +  py(iv)*tab4(iv))
      end do
      if (jbin==mbox) cycle
      do iv=1,ivmax                                                     ! indirect address loop
        ik1(iv) = ik1(iv) + ntab1(iv)
        ik2(iv) = ik2(iv) + ntab2(iv)
      end do
    end do
    end do
  end do
  n_lookup = n_lookup + mtau*mray

  if (debug2(dbg_rad,4)) then
    ntot = ntot + mtau*mray
    print *,'MK',mpi_rank,mtau,ntot
  end if

  call print_trace('lookup_rays',dbg_eos,'END')
END SUBROUTINE
