PROGRAM eos_test
  USE params
  USE eos

  implicit none

  integer :: mxyz, iz, lrec, i, iv
  real :: cput1(2), cput2(2), cputot(2,2), dtime, etime, fetime, etm, kzs, void
  real, allocatable, dimension(:,:,:) :: &
        r,px,py,pz,e,d,drdt,dpxdt,dpydt,dpzdt,dedt,dddt, &
        Bx,By,Bz,dBxdt,dBydt,dBzdt
  real, allocatable, dimension(:,:,:):: lnr,P,ee,rk,lnrk,s
  real, allocatable, dimension(:,:,:,:):: lns
!hpf$ dynamic, distribute(*,*,block):: &
!hpf$   r,px,py,pz,e,d,drdt,dpxdt,dpydt,dpzdt,dedt,dddt, &
!hpf$   Bx,By,Bz,dBxdt,dBydt,dBzdt

  print *,'-----------------------------------------------------------------------'
  call oflush
  call init_params

  allocate (r(mx,my,mz), &
         drdt(mx,my,mz), &
           px(mx,my,mz), &
           py(mx,my,mz), &
           pz(mx,my,mz), &
            e(mx,my,mz), &
           ee(mx,my,mz), &
            p(mx,my,mz), &
         lnrk(mx,my,mz), &
          lns(mx,my,mz,4) )

  call init_eos
  call init_io
  call read_snap (r,px,py,pz,e,d,Bx,By,Bz)
  ee=e/r

  print *,'r=',r(1,1:10,1)
  print *,'e=',e(1,1:10,1)
  print *,'ee=',ee(1,1:10,1)

  call pressure (r,ee,p)
  print *,'p=',p(1,1:10,1)
  call lookup (r,ee,lnrk,lns)
  print *,'rk=',exp(lnrk(1,1:10,1))
  print *,'s=',exp(lns(1,1:10,1,1))

END
