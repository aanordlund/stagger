! $Id: eos_lookup_linear.f90,v 1.7 2015/01/01 21:33:11 aake Exp $ 

!**********************************************************************
MODULE table
  integer iupdte,nvar,mrho,mtable,njon
  real(kind=4) eosxmin,ur,ul,ut,eps,tff,grv,abnd
  real(kind=4), allocatable, dimension(:):: tmean,tamp,rhm,xcorr,thmin,thmax, &
    dth,eemin,eemax,deetab,tab
  integer, allocatable, dimension(:):: itab,mtab
END

!**********************************************************************
MODULE eos
  USE params
  implicit none
  integer mbox
  real(kind=4) dbox
  character(len=mfile):: tablefile
  logical do_eos, do_table
  real, allocatable, dimension(:,:):: pbot0, dlnpdE_r, dlnpdlnr_E
  logical, allocatable, dimension(:,:,:):: panic
CONTAINS

!**********************************************************************
SUBROUTINE equation_of_state (rho,ee,mask,pp,tt,ne,rk,source)
!
!  Lookup equation of state table
!----------------------------------------------------------------------
!
  USE params
  implicit none
  real, dimension(mx,my,mz):: rho,ee
  real, dimension(mx,my,mz), optional:: pp,tt,ne,rk
  real, dimension(mx,my,mz,mbox), optional:: source
  integer mask
  logical omp_in_parallel

  if (omp_in_parallel()) then
    call equation_of_state_omp (rho,ee,mask,pp,tt,ne,rk,source)
  else
    !$omp parallel
    call equation_of_state_omp (rho,ee,mask,pp,tt,ne,rk,source)
    !$omp end parallel
  end if
END SUBROUTINE

!**********************************************************************
SUBROUTINE equation_of_state_omp (rho,ee,mask,pp,tt,ne,rk,source)
!
!  Lookup equation of state table
!----------------------------------------------------------------------
!
  USE params
  USE variables, ONLY: Ux,Uy,Uz,Bx,By,Bz
  USE table
  implicit none
  real, dimension(mx,my,mz):: rho,ee
  real, dimension(mx,my,mz), optional:: pp,tt,ne,rk
  real, dimension(mx,my,mz,mbox), optional:: source
  integer mask
!
  real(kind=4), dimension(mx):: px,py,f00,f01,f10,f11
  integer, dimension(mx):: np,np1,ik,ntab,ik1,ntab1
!
  logical newtable, any_mpi
  real(kind=4)::rhm1,rhm2,drhm,algrk,eek,eek1,py1,qx,qy,pxqx,pxpx,qxqx,pypy,pyqy,qyqy,pxpy,qxqy,pxqy,qxpy
  integer ix,iy,iz,kee,kee1,j

  call print_trace('EOS',dbg_eos,'BEGIN')

1 continue
!
  rhm1=log(rhm(1))
  rhm2=log(rhm(mrho))
  drhm=(rhm2-rhm1)/(mrho-1)
!
!  assume no panic
!
  newtable = .false.
  do iz=izs,ize
    panic(:,:,iz)=.false.
  end do
!
!  x,y loop
!
  do iz=izs,ize
  do iy=1,my
    do ix=1,mx
!
!  Density index
!  6 flops
!
      algrk=1.+(log(rho(ix,iy,iz))-rhm1)/drhm
      np(ix)=max0(1,min0(mrho-1,int(algrk)))
      px(ix)=algrk-np(ix)
      panic(ix,iy,iz)=panic(ix,iy,iz).or.px(ix).lt.-0.01.or.px(ix).gt.1.01
!
!  Internal energy index.  This loop has indirect referencing.
!
! +6 = 12 flops
      ntab(ix)   = mtab(np(ix))
      eek       = 1.+(ee(ix,iy,iz)-eemin(np(ix)))/deetab(np(ix))
      kee       = min(ntab(ix)-1,max(1,int(eek)))
      py(ix)     = eek-kee
      panic(ix,iy,iz) = panic(ix,iy,iz).or.py(ix).lt.-0.01.or.py(ix).gt.1.01
      ik(ix)     = itab(np(ix))+kee-1
      ntab1(ix)  = mtab(np(ix)+1)
! +3 = 15 flops
      kee1      = kee - nint((eemin(np(ix)+1)-eemin(np(ix))) &
                                            /deetab(np(ix)))
      kee1      = min0(ntab1(ix)-1,max0(1,kee1))
      ik1(ix)    = itab(np(ix)+1)+kee1-1
!
!  These three lines may be commented out when we are satisfied it never panics
!
      eek1      = 1.+(ee(ix,iy,iz)-eemin(np(ix)+1))/deetab(np(ix)+1)
      py1       = eek1-kee1
      panic(ix,iy,iz) = panic(ix,iy,iz) .or. abs(py(ix)-py1).gt.0.001
      !if (panic(ix,iy,iz)) then
      !  print*,ix,iy,iz
      !end if
    end do
!
!  Separate loop for weights, vectorizes well
!
    do ix=1,mx
! +5 = 20 flops
      qx   = 1. - px(ix)
      pxqx = px(ix) * qx
      pxpx = px(ix) * px(ix)
      qxqx = qx * qx
! +4 = 24 flops
      qy   = 1. - py(ix)
      pyqy = py(ix) * qy
      pypy = py(ix) * py(ix)
      qyqy = qy     * qy
! +4 = 28 flops
      pxqy = px(ix) * qy
      pxpy = px(ix) * py(ix)
      qxqy = qx     * qy
      qxpy = qx     * py(ix)
! +5 = 33 flops
      f00(ix) = qxqy * (1. + pxqx - pxpx + pyqy - pypy)
      f01(ix) = qxpy * (1. + pxqx - pxpx + pyqy - qyqy)
      f10(ix) = pxqy * (1. - qxqx + pxqx + pyqy - pypy)
      f11(ix) = pxpy * (1. - qxqx + pxqx + pyqy - qyqy)
    end do
!
!  Loop over table entries, nvar = 4.  Would this be more efficent
!  if split into one gather loop and one calculation loop?
!
    if (iand(mask,1).eq.1) then
     do ix=1,mx
      pp(ix,iy,iz) = ( &
         f00(ix) * tab(ik (ix))+  f01(ix) * tab(ik (ix) + 1) &
      +  f10(ix) * tab(ik1(ix))+  f11(ix) * tab(ik1(ix) + 1) )
     end do
    end if
    do ix=1,mx
      ik (ix) = ik (ix) + ntab (ix)
      ik1(ix) = ik1(ix) + ntab1(ix)
    end do
    if (iand(mask,2).eq.2) then
     do ix=1,mx
      rk(ix,iy,iz) = ( &
         f00(ix) * tab(ik (ix))+  f01(ix) * tab(ik (ix) + 1) &
      +  f10(ix) * tab(ik1(ix))+  f11(ix) * tab(ik1(ix) + 1) )
     end do
    end if
    do ix=1,mx
      ik (ix) = ik (ix) + ntab (ix)
      ik1(ix) = ik1(ix) + ntab1(ix)
    end do
    if (iand(mask,4).eq.4) then
     do ix=1,mx
      tt(ix,iy,iz) = ( &
         f00(ix) * tab(ik (ix))+  f01(ix) * tab(ik (ix) + 1) &
      +  f10(ix) * tab(ik1(ix))+  f11(ix) * tab(ik1(ix) + 1) )
     end do
    end if
    do ix=1,mx
      ik (ix) = ik (ix) + ntab (ix)
      ik1(ix) = ik1(ix) + ntab1(ix)
    end do
    if (iand(mask,8).eq.8) then
     do ix=1,mx
      ne(ix,iy,iz) = exp( &
         f00(ix) * tab(ik (ix))+  f01(ix) * tab(ik (ix) + 1) &
      +  f10(ix) * tab(ik1(ix))+  f11(ix) * tab(ik1(ix) + 1) )
     end do
    end if
    if (iand(mask,16).eq.16) then
     do j=1,mbox
     do ix=1,mx
      ik (ix) = ik (ix) + ntab (ix)
      ik1(ix) = ik1(ix) + ntab1(ix)
      source(ix,iy,iz,j) = ( &
         f00(ix) * tab(ik (ix))+  f01(ix) * tab(ik (ix) + 1) &
      +  f10(ix) * tab(ik1(ix))+  f11(ix) * tab(ik1(ix) + 1) )
     end do
     end do
    end if
!
! End outer loops
!
  end do
  end do
!
!  Any cause for panic ?
!
  !$omp barrier
  !$omp master
  if (any_mpi(any(panic))) then
    do_dump = .true.
    call dumpn(rho,'rho','lookup.dmp',1)
    call dumpn(ee,'ee','lookup.dmp',1)
    call dumpn(Ux,'Ux','lookup.dmp',1)
    call dumpn(Uy,'Uy','lookup.dmp',1)
    call dumpn(Uz,'Uz','lookup.dmp',1)
    if (do_mhd) then
      call dumpn(Bx,'Bx','lookup.dmp',1)
      call dumpn(By,'By','lookup.dmp',1)
      call dumpn(Bz,'Bz','lookup.dmp',1)
    end if

    do iz=1,mz
    do iy=1,my
    do ix=1,mx
      if (panic(ix,iy,iz)) then
        algrk=1.+(log(rho(ix,iy,iz))-rhm1)/drhm
        np(ix)=max0(1,min0(mrho-1,int(algrk)))
        ntab(ix) = mtab(np(ix))
        ntab1(ix)= mtab(np(ix)+1)
        eek= 1.+(ee(ix,iy,iz)-eemin(np(ix)))/deetab(np(ix))
        kee= min0(ntab(ix)-1,max0(1,int(eek)))
        kee1= kee - nint((eemin(np(ix)+1)-eemin(np(ix)))/deetab(np(ix)))
        kee1= min0(ntab1(ix)-1,max0(1,kee1))
        py(ix)= eek-kee
        eek1= 1.+(ee(ix,iy,iz)-eemin(np(ix)+1))/deetab(np(ix)+1)
        py1  = eek1-kee1
        print '(a)','ix,iy,iz,rho,rhm1,rhm2,np(ix),algrk:'
        print '(3i4,3f8.3,i4,f8.3)',ix,iy,iz,log(rho(ix,iy,iz)),rhm1,rhm2,np(ix),algrk
        print '(a)','ee(ix,iy,iz),eemin,eemax,eek,kee,py(ix),ntab:'
        print '(a)','                         eek1,kee1,py1,ntab1:'
        print '(4f8.3,i4,f8.3,i4/24x,f8.3,i4,f8.3,i4)',ee(ix,iy,iz), &
          eemin(np(ix)),eemax(np(ix)),eek,kee,py(ix),ntab(ix),eek1,kee1,py1,ntab1(ix)
        call abort_mpi
        stop
      endif
    end do
    end do
    end do

    call abort_mpi
    stop
  end if
  !$omp end master
  !$omp barrier
END SUBROUTINE

END MODULE eos

!**********************************************************************
SUBROUTINE init_eos
  USE params
  USE arrays
  USE eos
  USE table
  implicit none
  integer i, rank, iz
  character(len=mid):: id="$Id: eos_lookup_linear.f90,v 1.7 2015/01/01 21:33:11 aake Exp $"

  call print_id(id)
  call read_eos

  if (master) open (12,file=tablefile,form='unformatted',status='old',action='read')
  if (master) read (12) mrho,iupdte,nvar,mbox,eosxmin,dbox,ul,ut,ur,eps,tff,grv,abnd
  call broadcast_mpi (mrho   , 1)
  call broadcast_mpi (iupdte , 1)
  call broadcast_mpi (nvar   , 1)
  call broadcast_mpi (mbox   , 1)
  call broadcast_mpi (eosxmin, 1)
  call broadcast_mpi (dbox   , 1)
  call broadcast_mpi (ul     , 1)
  call broadcast_mpi (ut     , 1)
  call broadcast_mpi (ur     , 1)
  call broadcast_mpi (eps    , 1)
  call broadcast_mpi (tff    , 1)
  call broadcast_mpi (grv    , 1)
  call broadcast_mpi (abnd   , 1)
  if (do_trace.and.master) print *,mpi_rank,mrho,iupdte,nvar,mbox,eosxmin,dbox,ul,ut,ur,eps,tff,grv,abnd
  njon = nvar-mbox
  allocate (tmean(mrho),tamp(mrho),rhm(mrho),xcorr(mrho),thmin(mrho),thmax(mrho))
  allocate (dth(mrho),eemin(mrho),eemax(mrho),deetab(mrho),itab(mrho),mtab(mrho))
  if (do_trace.and.master) print *,mpi_rank, 'reading 12 times', mrho, ' values'
  if (master) read (12) tmean,tamp,rhm,xcorr,thmin,thmax,dth,eemin,eemax,deetab,itab,mtab
  call broadcast_mpi (tmean , mrho)
  call broadcast_mpi (tamp  , mrho)
  call broadcast_mpi (rhm   , mrho)
  call broadcast_mpi (xcorr , mrho)
  call broadcast_mpi (thmin , mrho)
  call broadcast_mpi (thmax , mrho)
  call broadcast_mpi (dth   , mrho)
  call broadcast_mpi (eemin , mrho)
  call broadcast_mpi (eemax , mrho)
  call broadcast_mpi (deetab, mrho)
  call broadcast_mpi (itab  , mrho)
  call broadcast_mpi (mtab  , mrho)
  if (master) read (12) mtable
  call broadcast_mpi (mtable, 1)
  allocate (tab(mtable))
  if (master) print *, mpi_rank, 'reading', mtable, ' table values'
  if (master) read (12) tab
  call broadcast_mpi (tab, mtable)
  if (master) print *, mpi_rank, 'read   ', mtable, ' table values'
  if (master) close (12)

  allocate (pbot0(mx,mz), dlnpdE_r(mx,mz), dlnpdlnr_E(mx,mz))
  allocate (panic(mx,my,mz))
!
!  Initialize on OMP threads
!
!$omp parallel
  do iz=izs,ize
    panic(:,:,iz) = .false.
  end do
!$omp end parallel
END SUBROUTINE

!**********************************************************************
SUBROUTINE read_eos
  USE eos
  USE table
  implicit none
  namelist /eqofst/ do_eos, do_ionization, do_table, tablefile

  do_eos = .true.
  do_ionization = .true.
  do_table = .false.
  tablefile = 'table_linear.dat'
  rewind (stdin); read (stdin,eqofst)
  if (master) write (*,eqofst)
END SUBROUTINE

!**********************************************************************
SUBROUTINE lookup (lnr,ee,rk,source, ny)
!
!  Lookup equation of state table
!----------------------------------------------------------------------
!
  USE params
  USE eos
  implicit none
  integer ny
  real, dimension(mx,my,mz), intent(in):: lnr,ee
  real, dimension(mx,my,mz), intent(out):: rk
  real, dimension(mx,my,mz,mbox), intent(out):: source
  logical omp_in_parallel

  if (omp_in_parallel()) then
    call lookup_omp (lnr,ee,rk,source,ny)
  else
    !$omp parallel
    call lookup_omp (lnr,ee,rk,source,ny)
    !$omp end parallel
  end if
  call dumpn(rk,'rk','eos2.dmp',1)
  call dumpn(source,'source','eos2.dmp',1)
  n_lookup = n_lookup + mx*ny*mz
END

!**********************************************************************
SUBROUTINE lookup_omp (lnr,ee,rk,source,ny)
!
!  Lookup equation of state table
!----------------------------------------------------------------------
!
  USE params
  USE eos
  USE table
  implicit none
  integer ny
  real, dimension(mx,my,mz), intent(in):: lnr,ee
  real, dimension(mx,my,mz), intent(out):: rk
  real, dimension(mx,my,mz,mbox), intent(out):: source
!
  real(kind=4), dimension(mx):: px,py
  integer, dimension(mx):: np,np1,ik,ntab,ik1,ntab1
!
  real(kind=4)::rhm1,rhm2,drhm,algrk,eek,qx,qy,pxqx,pxpx,qxqx,pypy,pyqy,qyqy,pxpy,qxqy,pxqy,qxpy
  integer ix,iy,iz,kee,kee1,j
  logical omp_in_parallel
!
  call print_trace('lookup',dbg_eos,'BEGIN')
  rhm1=log(rhm(1))
  rhm2=log(rhm(mrho))
  drhm=(rhm2-rhm1)/(mrho-1)
!
!  x,y loop
!
  do iz=izs,ize
  do iy=1,ny
    do ix=1,mx
!
!  Density index
!  6 flops
!
      algrk=1.+(lnr(ix,iy,iz)-rhm1)/drhm
      np(ix)=max0(1,min0(mrho-1,int(algrk)))
      px(ix)=algrk-np(ix)
!
!  Internal energy index.  This loop has indirect referencing.
!
! +6 = 12 flops
      ntab(ix)   = mtab(np(ix))
      eek       = 1.+(ee(ix,iy,iz)-eemin(np(ix)))/deetab(np(ix))
      kee       = min(ntab(ix)-1,max(1,int(eek)))
      py(ix)     = eek-kee
      ik(ix)     = itab(np(ix))+kee-1
      ntab1(ix)  = mtab(np(ix)+1)
! +3 = 15 flops
      kee1      = kee - nint((eemin(np(ix)+1)-eemin(np(ix))) &
                                            /deetab(np(ix)))
      kee1      = min0(ntab1(ix)-1,max0(1,kee1))
      ik1(ix)    = itab(np(ix)+1)+kee1-1
    end do
!
!  Loop over table entries, nvar = 4.  Would this be more efficent
!  if split into one gather loop and one calculation loop?
!
    do ix=1,mx
      ik (ix) = ik (ix) + ntab (ix)
      ik1(ix) = ik1(ix) + ntab1(ix)
      rk(ix,iy,iz) = &
        (1.-px(ix))*((1.-py(ix))*tab(ik (ix)) +  py(ix)*tab(ik (ix)+1)) &
      +     px(ix) *((1.-py(ix))*tab(ik1(ix)) +  py(ix)*tab(ik1(ix)+1))
      ik (ix) = ik (ix) + ntab (ix)*(njon-2)
      ik1(ix) = ik1(ix) + ntab1(ix)*(njon-2)
    end do
    do j=1,mbox
     do ix=1,mx
      ik (ix) = ik (ix) + ntab (ix)
      ik1(ix) = ik1(ix) + ntab1(ix)
      source(ix,iy,iz,j) = &
        (1.-px(ix))*((1.-py(ix))*tab(ik (ix)) +  py(ix)*tab(ik (ix)+1)) &
      +     px(ix) *((1.-py(ix))*tab(ik1(ix)) +  py(ix)*tab(ik1(ix)+1))
     end do
    end do
!
! End outer loops
!
  end do
  end do
  call print_trace('lookup',dbg_eos,'END')
END SUBROUTINE lookup_omp

!**********************************************************************
SUBROUTINE lookup_reshape (nx, ny, nz, lnr, ee, rk, source, ly, lz)
!
!  Lookup equation of state table
!----------------------------------------------------------------------
!
  USE params
  USE eos
  implicit none
  integer nx,ny,nz,ly,lz
  real, dimension(nx,ny,nz), intent(in):: lnr,ee
  real, dimension(nx,ny,nz), intent(out):: rk
  real, dimension(nx,ny,nz,mbox), intent(out):: source
  logical omp_in_parallel

  if (omp_in_parallel()) then
    call lookup_reshape_omp (nx,ny,nz,lnr,ee,rk,source,ly,lz)
  else
    !$omp parallel
    call lookup_reshape_omp (nx,ny,nz,lnr,ee,rk,source,ly,lz)
    !$omp end parallel
  end if
  call dumpn(rk,'rk','eos2.dmp',1)
  call dumpn(source,'source','eos2.dmp',1)
  n_lookup = n_lookup + nx*ly*nz
END

!**********************************************************************
SUBROUTINE lookup_reshape_omp (nx,ny,nz,lnr,ee,rk,source,ly,lz)
!
!  Lookup equation of state table
!----------------------------------------------------------------------
!
  USE params
  USE eos
  USE table
  implicit none
  integer nx,ny,nz,ly,lz
  real, dimension(nx,nz,ny), intent(in):: lnr,ee
  real, dimension(nx,nz,ny), intent(out):: rk
  real, dimension(nx,nz,ny,mbox), intent(out):: source
!
  real(kind=4), dimension(nx):: px,py,tab1,tab2,tab3,tab4
  integer, dimension(nx):: ntab1,ntab2,ik1,ik2
!
  real rhm1,rhm2,drhm,algrk,eek
  integer jbin,ix,iz,iy,kee,kee1,kee2,j,np
  logical omp_in_parallel, debug2
  integer(8), save:: ntot=0
!
  if (ny <= 0) return
  call print_trace('lookup',dbg_eos,'BEGIN')
  rhm1=log(rhm(1))
  rhm2=log(rhm(mrho))
  drhm=(rhm2-rhm1)/(mrho-1)
!
  !$omp do private(iz,iy,ix,algrk,np,px,ntab1,ntab2,eek,kee1,kee2,ik1,ik2,jbin)
  do iy=1,ly
  do iz=1,lz
    do ix=1,nx
      algrk     = 1. + (lnr(ix,iz,iy)-rhm1)/drhm
      np        = max0(1,min0(mrho-1,int(algrk)))
      px(ix)    = algrk - np
      ntab1(ix) = mtab(np)
      ntab2(ix) = mtab(np+1)
      eek       = 1. + (ee(ix,iz,iy)-eemin(np))/deetab(np)
      kee1      = min(ntab1(ix)-1,max(1,int(eek)))
      py(ix)    = eek - kee1
      kee2      = kee1 - nint((eemin(np+1)-eemin(np))/deetab(np))
      kee2      = min0(ntab2(ix)-1,max0(1,kee2))
      ik1(ix)   = itab(np  ) + kee1 - 1 + (ntab1(ix)) 
      ik2(ix)   = itab(np+1) + kee2 - 1 + (ntab2(ix))
    end do
    !-------------------------------------------------------------------
    ! Cache table values in separate loop, which doesn't vectorize
    !-------------------------------------------------------------------
    do ix=1,nx
      tab1(ix) = tab(ik1(ix))
      tab2(ix) = tab(ik1(ix)+1)
      tab3(ix) = tab(ik2(ix))
      tab4(ix) = tab(ik2(ix)+1)
    end do
    !-------------------------------------------------------------------
    ! This loop vectorizes
    !-------------------------------------------------------------------
    do ix=1,nx
      rk(ix,iz,iy) = &
        (1-px(ix))*((1.-py(ix))*tab1(ix) +  py(ix)*tab2(ix)) &
      +    px(ix) *((1.-py(ix))*tab3(ix) +  py(ix)*tab4(ix))
    end do
    !-------------------------------------------------------------------
    ! Increment addresses
    !-------------------------------------------------------------------
    do ix=1,nx
      ik1(ix) = ik1(ix) + (ntab1(ix))*(njon-1)
      ik2(ix) = ik2(ix) + (ntab2(ix))*(njon-1)
    end do
    do jbin=1,mbox
      !-----------------------------------------------------------------
      ! Cache table values in separate loop, which doesn't vectorize
      !-----------------------------------------------------------------
      do ix=1,nx
        tab1(ix) = tab(ik1(ix))
        tab2(ix) = tab(ik1(ix)+1)
        tab3(ix) = tab(ik2(ix))
        tab4(ix) = tab(ik2(ix)+1)
      end do
      !-----------------------------------------------------------------
      ! Return (S), to use a minimal number of exp() later, based on n2
      !-----------------------------------------------------------------
      do ix=1,nx
        source(ix,iz,iy,jbin) = &
          (1-px(ix))*((1.-py(ix))*tab1(ix) +  py(ix)*tab2(ix)) &
        +    px(ix) *((1.-py(ix))*tab3(ix) +  py(ix)*tab4(ix))
      end do
      if (jbin < mbox) then
        do ix=1,nx
          ik1(ix) = ik1(ix) + (ntab1(ix))
          ik2(ix) = ik2(ix) + (ntab2(ix))
        end do
      end if
    end do ! jbin
  end do ! iz
  end do ! iy

  if ((master.and.debug2(dbg_rad,3)) .or. debug2(dbg_rad,6)) then
    ntot = ntot + nx*ny*lz
    print*,'lookup_reshape:',mpi_rank,ny,ntot
  end if

  call print_trace('lookup',dbg_eos,'END')
END SUBROUTINE lookup_reshape_omp

!**********************************************************************
SUBROUTINE pressure (rho,ee,pp)
!
!  Lookup equation of state table
!----------------------------------------------------------------------
!
  USE params
  implicit none
  real, dimension(mx,my,mz), intent(in):: rho,ee
  real, dimension(mx,my,mz), intent(out):: pp
  logical omp_in_parallel

  if (omp_in_parallel()) then
    call pressure_omp (rho,ee,pp)
  else
    !$omp parallel
    call pressure_omp (rho,ee,pp)
    !$omp end parallel
  end if
  call dumpn(rho,'rho','eos2.dmp',1)
  call dumpn(pp,'pp','eos2.dmp',1)
END

!**********************************************************************
SUBROUTINE pressure_omp (rho,ee,pp)
!
!  Lookup equation of state table
!----------------------------------------------------------------------
!
  USE params
  USE eos
  USE variables, ONLY:Ux,Uy,Uz,Bx,By,Bz
  USE table
  implicit none
  real, dimension(mx,my,mz), intent(in):: rho,ee
  real, dimension(mx,my,mz), intent(out):: pp
!
  real(kind=8), dimension(mx):: px,py,py1
  integer, dimension(mx):: np,np1,ik,ntab,ik1,ntab1
!
  logical newtable, any_mpi
  real(kind=8)::rhm1,rhm2,drhm,algrk,eek,eek1
  integer ix,iy,iz,kee,kee1,j
  logical, save:: first_time=.true.

1 continue
!
  call barrier_omp('press1')
  rhm1=log(rhm(1))
  rhm2=log(rhm(mrho))
  drhm=(rhm2-rhm1)/(mrho-1)
!
!  assume no panic
!
  newtable = .false.
!
!  x,y loop
!
  do iz=izs,ize
  do iy=1,my
    do ix=1,mx
!
!  Density index
!  6 flops
!
      algrk=1.+(log(rho(ix,iy,iz))-rhm1)/drhm
      np(ix)=max0(1,min0(mrho-1,int(algrk)))
      px(ix)=algrk-np(ix)
      panic(ix,iy,iz)=px(ix).lt.-0.01.or.px(ix).gt.1.01
!
!  Internal energy index.  This loop has indirect referencing.
!
! +6 = 12 flops
      ntab(ix)  = mtab(np(ix))
      eek       = 1.+(ee(ix,iy,iz)-eemin(np(ix)))/deetab(np(ix))
      kee       = min(ntab(ix)-1,max(1,int(eek)))
      py(ix)    = eek-kee
      panic(ix,iy,iz) = panic(ix,iy,iz).or.py(ix).lt.-0.01.or.py(ix).gt.1.01
      ik(ix)    = itab(np(ix))+kee-1
      ntab1(ix) = mtab(np(ix)+1)
! +3 = 15 flops
      kee1      = kee - nint((eemin(np(ix)+1)-eemin(np(ix))) &
                                            /deetab(np(ix)))
      kee1      = min0(ntab1(ix)-1,max0(1,kee1))
      ik1(ix)   = itab(np(ix)+1)+kee1-1
!
!  These three lines may be commented out when we are satisfied it never panics
!
      eek1      = 1.+(ee(ix,iy,iz)-eemin(np(ix)+1))/deetab(np(ix)+1)
      py1(ix)   = eek1-kee1
      panic(ix,iy,iz) = panic(ix,iy,iz) .or. abs(py(ix)-py1(ix)).gt.0.001
      !if (panic(ix,iy,iz)) then
      !  print*,ix,iy,iz
      !end if
    end do
!
!  Loop over table entries, nvar = 4.  Would this be more efficent
!  if split into one gather loop and one calculation loop?
!
    do ix=1,mx
      pp(ix,iy,iz) = &
        (1d0-px(ix))*((1d0-py (ix))*tab(ik (ix)) +  py (ix)*tab(ik (ix)+1)) &
       +     px(ix) *((1d0-py1(ix))*tab(ik1(ix)) +  py1(ix)*tab(ik1(ix)+1))
    end do
    if (iy.eq.ub) then
!                               if (do_trace) print *,'pressure: crit1', omp_mythread
      do ix=1,mx
        pbot0(ix,iz) = pp(ix,ub,iz)
          dlnpdE_r(ix,iz) = (tab(ik(ix)+1) - tab(ik(ix)))/deetab(np(ix))/pbot0(ix,iz)
        dlnpdlnr_E(ix,iz) = (tab(ik1(ix) ) - tab(ik(ix)))/drhm          /pbot0(ix,iz)
      end do
!                               if (do_trace) print *,'pressure: crit2', omp_mythread
    end if
!
! End outer loops
!
  end do
  end do
  call barrier_omp('panic1')
!
!  Verify BC derivatives
!
  !$omp master
  if (first_time.and.master) print*,'dlnpdE_r, dlnpdlnr_E =', &
                             sum(dlnpdE_r)/(mx*mz),sum(dlnpdlnr_E)/(mx*mz)
  first_time=.false.
!
!  Any cause for panic ?
!
  if (any_mpi(any(panic))) then
    do_dump = .true.
    call dumpn(rho,'rho','lookup.dmp',1)
    call dumpn(ee,'ee','lookup.dmp',1)
    call dumpn(Ux,'Ux','lookup.dmp',1)
    call dumpn(Uy,'Uy','lookup.dmp',1)
    call dumpn(Uz,'Uz','lookup.dmp',1)
    if(do_mhd) then
      call dumpn(Bx,'Bx','lookup.dmp',1)
      call dumpn(By,'By','lookup.dmp',1)
      call dumpn(Bz,'Bz','lookup.dmp',1)
    end if

    do iz=1,mz
    do iy=1,my
    do ix=1,mx
      if (panic(ix,iy,iz)) then
        algrk=1.+(log(rho(ix,iy,iz))-rhm1)/drhm
        np(ix)=max0(1,min0(mrho-1,int(algrk)))
        ntab(ix) = mtab(np(ix))
        ntab1(ix)= mtab(np(ix)+1)
        eek= 1.+(ee(ix,iy,iz)-eemin(np(ix)))/deetab(np(ix))
        kee= min0(ntab(ix)-1,max0(1,int(eek)))
        kee1= kee - nint((eemin(np(ix)+1)-eemin(np(ix)))/deetab(np(ix)))
        kee1= min0(ntab1(ix)-1,max0(1,kee1))
        py(ix)= eek-kee
        eek1= 1.+(ee(ix,iy,iz)-eemin(np(ix)+1))/deetab(np(ix)+1)
        py1  = eek1-kee1
        print '(a)','ix,iy,iz,rho,rhm1,rhm2,np(ix),algrk:'
        print '(3i4,3f8.3,i4,f8.3)',ix+ixoff,iy+iyoff,iz+izoff,log(rho(ix,iy,iz)),rhm1,rhm2,np(ix),algrk
        print '(a)','ee(ix,iy,iz),eemin,eemax,eek,kee,py(ix),ntab:'
        print '(a)','                         eek1,kee1,py1,ntab1:'
        print '(4f8.3,i4,f8.3,i4/24x,f8.3,i4,f8.3,i4)',ee(ix,iy,iz), &
          eemin(np(ix)),eemax(np(ix)),eek,kee,py(ix),ntab(ix),eek1,kee1,py1,ntab1(ix)
        call abort_mpi
        stop
      endif
    end do
    end do
    end do

    call abort_mpi
    stop
  end if
  !$omp end master
  call barrier_omp('panic2')

END SUBROUTINE

!**********************************************************************
SUBROUTINE temperature (rho,ee,tt)
!----------------------------------------------------------------------
!
  USE params
  implicit none
  real, dimension(mx,my,mz), intent(in):: rho,ee
  real, dimension(mx,my,mz), intent(out):: tt
  logical omp_in_parallel

  if (omp_in_parallel()) then
    call temperature_omp (rho,ee,tt)
  else
    !$omp parallel
    call temperature_omp (rho,ee,tt)
    !$omp end parallel
  end if
END

!**********************************************************************
SUBROUTINE temperature_omp (rho,ee,tt)
!
!  Lookup equation of state table
!----------------------------------------------------------------------
!
  USE params
  USE eos
  USE variables, ONLY: Ux,Uy,Uz,Bx,By,Bz
  USE table
  implicit none
  real, dimension(mx,my,mz), intent(in):: rho,ee
  real, dimension(mx,my,mz), intent(out):: tt
!
  real, dimension(mx):: px,py,f00,f01,f10,f11
  integer, dimension(mx):: np,np1,ik,ntab,ik1,ntab1
!
  logical newtable, any_mpi
  real(kind=8)::rhm1,rhm2,drhm,algrk,eek,eek1,py1,qx,qy,pxqx,pxpx,qxqx,pypy,pyqy,qyqy,pxpy,qxqy,pxqy,qxpy
  integer ix,iy,iz,kee,kee1,j

1 continue
  call print_trace ('temperature',dbg_eos,'BEGIN')
!
  rhm1=log(rhm(1))
  rhm2=log(rhm(mrho))
  drhm=(rhm2-rhm1)/(mrho-1)
!
!  assume no panic
!
  newtable = .false.
!
!  x,y loop
!
  do iz=izs,ize
  do iy=1,my
    do ix=1,mx
!
!  Density index
!  6 flops
!
      algrk=1.+(log(rho(ix,iy,iz))-rhm1)/drhm
      np(ix)=max0(1,min0(mrho-1,int(algrk)))
      px(ix)=algrk-np(ix)
      panic(ix,iy,iz)=px(ix).lt.-0.01.or.px(ix).gt.1.01
!
!  Internal energy index.  This loop has indirect referencing.
!
! +6 = 12 flops
      ntab(ix)   = mtab(np(ix))
      eek       = 1.+(ee(ix,iy,iz)-eemin(np(ix)))/deetab(np(ix))
      kee       = min(ntab(ix)-1,max(1,int(eek)))
      py(ix)     = eek-kee
      panic(ix,iy,iz) = panic(ix,iy,iz).or.py(ix).lt.-0.01.or.py(ix).gt.1.01
      ik(ix)     = itab(np(ix))+kee-1
      ntab1(ix)  = mtab(np(ix)+1)
! +3 = 15 flops
      kee1      = kee - nint((eemin(np(ix)+1)-eemin(np(ix))) &
                                            /deetab(np(ix)))
      kee1      = min0(ntab1(ix)-1,max0(1,kee1))
      ik1(ix)    = itab(np(ix)+1)+kee1-1
!
!  These three lines may be commented out when we are satisfied it never panics
!
      eek1      = 1.+(ee(ix,iy,iz)-eemin(np(ix)+1))/deetab(np(ix)+1)
      py1       = eek1-kee1
      panic(ix,iy,iz) = panic(ix,iy,iz) .or. abs(py(ix)-py1).gt.0.001
      !if (panic(ix,iy,iz)) then
      !  print*,ix,iy,iz
      !end if
    end do
!
!  Separate loop for weights, vectorizes well
!
    do ix=1,mx
! +5 = 20 flops
      qx   = 1. - px(ix)
      pxqx = px(ix) * qx
      pxpx = px(ix) * px(ix)
      qxqx = qx * qx
! +4 = 24 flops
      qy   = 1. - py(ix)
      pyqy = py(ix) * qy
      pypy = py(ix) * py(ix)
      qyqy = qy     * qy
! +4 = 28 flops
      pxqy = px(ix) * qy
      pxpy = px(ix) * py(ix)
      qxqy = qx     * qy
      qxpy = qx     * py(ix)
! +5 = 33 flops
      f00(ix) = qxqy * (1. + pxqx - pxpx + pyqy - pypy)
      f01(ix) = qxpy * (1. + pxqx - pxpx + pyqy - qyqy)
      f10(ix) = pxqy * (1. - qxqx + pxqx + pyqy - pypy)
      f11(ix) = pxpy * (1. - qxqx + pxqx + pyqy - qyqy)
    end do
!
!  Loop over table entries, nvar = 4.  Would this be more efficent
!  if split into one gather loop and one calculation loop?
!
    do ix=1,mx
      ik (ix) = ik (ix) + ntab (ix)*2
      ik1(ix) = ik1(ix) + ntab1(ix)*2
      tt(ix,iy,iz) = ( &
         f00(ix) * tab(ik (ix))+  f01(ix) * tab(ik (ix) + 1) &
      +  f10(ix) * tab(ik1(ix))+  f11(ix) * tab(ik1(ix) + 1) )
    end do
!
! End outer loops
!
  end do
  end do
!
!  Any cause for panic ?
!
  !$omp barrier
  !$omp master
  if (any_mpi(any(panic))) then
    do_dump = .true.
    call dumpn(rho,'rho','lookup.dmp',1)
    call dumpn(ee,'ee','lookup.dmp',1)
    call dumpn(Ux,'Ux','lookup.dmp',1)
    call dumpn(Uy,'Uy','lookup.dmp',1)
    call dumpn(Uz,'Uz','lookup.dmp',1)
    if (do_mhd) then
      call dumpn(Bx,'Bx','lookup.dmp',1)
      call dumpn(By,'By','lookup.dmp',1)
      call dumpn(Bz,'Bz','lookup.dmp',1)
    end if

    do iz=1,mz
    do iy=1,my
    do ix=1,mx
      if (panic(ix,iy,iz)) then
        algrk=1.+(log(rho(ix,iy,iz))-rhm1)/drhm
        np(ix)=max0(1,min0(mrho-1,int(algrk)))
        ntab(ix) = mtab(np(ix))
        ntab1(ix)= mtab(np(ix)+1)
        eek= 1.+(ee(ix,iy,iz)-eemin(np(ix)))/deetab(np(ix))
        kee= min0(ntab(ix)-1,max0(1,int(eek)))
        kee1= kee - nint((eemin(np(ix)+1)-eemin(np(ix)))/deetab(np(ix)))
        kee1= min0(ntab1(ix)-1,max0(1,kee1))
        py(ix)= eek-kee
        eek1= 1.+(ee(ix,iy,iz)-eemin(np(ix)+1))/deetab(np(ix)+1)
        py1  = eek1-kee1
        print '(a)','ix,iy,iz,rho,rhm1,rhm2,np(ix),algrk:'
        print '(3i4,3f8.3,i4,f8.3)',ix+ixoff,iy+iyoff,iz+izoff,log(rho(ix,iy,iz)),rhm1,rhm2,np(ix),algrk
        print '(a)','ee(ix,iy,iz),eemin,eemax,eek,kee,py(ix),ntab:'
        print '(a)','                         eek1,kee1,py1,ntab1:'
        print '(4f8.3,i4,f8.3,i4/24x,f8.3,i4,f8.3,i4)',ee(ix,iy,iz), &
          eemin(np(ix)),eemax(np(ix)),eek,kee,py(ix),ntab(ix),eek1,kee1,py1,ntab1(ix)
        call abort_mpi
        stop
      endif
    end do
    end do
    end do

    call abort_mpi
    stop
  end if
  !$omp end master
  !$omp barrier

END SUBROUTINE

SUBROUTINE temperature1 (rho,ee,tt)
  USE params
  USE eos
  implicit none
  real, dimension(mx,my,mz), intent(in):: rho,ee
  real, dimension(mx,my,mz), intent(out):: tt
  call equation_of_state (rho,ee,4,tt=tt)
END SUBROUTINE
