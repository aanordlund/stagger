!***********************************************************************
MODULE eos
!
!  EOS for simple hydrogren plasma
!
  use params
  use units

  implicit none

  real, parameter:: chi=13.6, ee0=chi*echarge/mproton, tt0=chi*echarge/kB
! real, parameter:: rho0=mproton*(2.*pi*melectron*chi*echarge/hplanck**2)**1.5
  real, parameter:: rho0=0.25317730
  real, parameter:: twothirds=0.66666667
  real:: rho1,ee1,rc,eec,y
  integer:: calls, totiter, niter

CONTAINS

!***********************************************************************
  subroutine eos_test
    implicit integer (i-n)
    implicit real (a-h,o-z)
    nt=50
    t0=1e3
    t1=1e6
    dlnt=log(t1/t0)/nt
    utt=46.
    rho1=cdu/rho0
    ee1=cku/ee0
    r=1.
    ee=10.
    call eos1(r,ee,tt,pp)
    do i=-40,15
      z=i
      y=z2y(z)
      dydz=y*(1.-y)/(2.-y)
      f=fsaha(z)
      print '(6(1pg12.3))',z,y,y2z(y),dydz,f
    end do
    do i=0,nt
      tta=exp(log(t0)+dlnt*i)
      ee=tta/utemp
      call eos1(r,ee,tt,pp)
      print '(7(1pg12.3),i5)',r,ee,y,tta,tt,tt-tta,tt/tta,niter
    end do
  end subroutine

!***********************************************************************
  function pressure(r,ee,tt)
    implicit none
    real, dimension(mx,my,mz):: r,ee,yy,pressure
    real, dimension(mx,my,mz), optional:: tt
    integer:: iz

    rho1=cdu/rho0
    ee1=cku/ee0

    do iz=1,mz
      yy(:,:,iz) = ionfrac(r(:,:,iz),ee(:,:,iz))
    end do
    if (present(tt)) then
      tt=(twothirds*tt0)*(ee*ee1-yy)
      pressure=(cdu/cpu*ee0/tt0)*tt*r
      tt=tt/(1.+yy)
    else
      pressure=(twothirds*cdu/cpu*ee0)*r*(ee*ee1-yy)
    end if
    return
  end function

!***********************************************************************
  function pressure2(r,ee,tt)
    implicit none
    real, dimension(mx,my):: r,ee,yy,pressure2
    real, dimension(mx,my), optional:: tt

    rho1=cdu/rho0
    ee1=cku/ee0

    yy = ionfrac(r,ee)
    if (present(tt)) then
      tt=(twothirds*tt0)*(ee*ee1-yy)
      pressure2=(cdu/cpu*ee0/tt0)*tt*r
      tt=tt/(1.+yy)
    else
      pressure2=(twothirds*cdu/cpu*ee0)*r*(ee*ee1-yy)
    end if
    return
  end function

!***********************************************************************
  subroutine eos2(r,ee,tt,pp)
    implicit none
    real, dimension(mx,my):: r,ee,yy
    real, dimension(mx,my), optional:: tt,pp

    rho1=cdu/rho0
    ee1=cku/ee0

    yy = ionfrac(r,ee)
    if (present(tt)) then
      tt=(twothirds*tt0)*(ee*ee1-yy)
      if (present(pp)) pp=(cdu/cpu*ee0/tt0)*tt*r
      tt=tt/(1.+yy)
    else if (present(pp)) then
      pp=(twothirds*cdu/cpu*ee0)*r*(ee*ee1-yy)
    end if
    return
  end subroutine

!***********************************************************************
  function ionfrac(r,ee)
!----------------------------------------------------------------
! root-finding loop
!----------------------------------------------------------------
    implicit none
    real, dimension(mx,my) :: r,ee
    real, dimension(mx,my) :: ionfrac
    integer                   :: i,j

    do j=1,my
      do i=1,mx
        call eos1(r(i,j),ee(i,j))
        ionfrac(i,j)=y
     end do
    end do
  end function ionfrac

!***********************************************************************
  subroutine eos1(r,ee,tt,pp)
    real:: r,ee,z,z1,z2,E1,E2,Ehalf,T1,T2,T,Thalf
    real, optional:: tt,pp
    real, parameter:: tol=1e-4
!
    rho1=cdu/rho0
    ee1=cku/ee0

    rc=r*rho1
    eec=ee*ee1

    Thalf = 1./(-alog(rc)-y2z(0.5))
    Ehalf = 0.5 + 2.25*Thalf
!
!  E1 and E2 are estimates of the "corner" energies, at temperatures just 
!  below and just above Thalf.
!
    E1 = 1.5*Thalf
    E2 = 1.+3.*Thalf
!
!  For energies less than E1, estimate the temperature by assuming no ionization
!
    if (eec.lt.E1) then
      T2 = min(eec/1.5,Thalf)
      T2 = T2*1.1
      T1 = 0.5*T2
    else if (eec.gt.E2) then
      T1 = (eec-1.)/3.
      T1 = T1*0.5
      T2 = T1*4.
    else
      T1 = Thalf*0.7
      T2 = Thalf*1.5
    endif
!
    call zbrent(fsahaE,T1,T2,T,tol)
!
    if (present(tt)) tt=T*tt0
    if (present(pp)) pp=(cdu/cpu*ee0/tt0)*T*r*(1.+y)
!
  end subroutine

!***********************************************************************
  subroutine eos1a(r,ee,tt,pp)
    real:: r,ee,z,z1,z2
    real, optional:: tt,pp
    real, parameter:: tol=1e-4
!
    rho1=cdu/rho0
    ee1=cku/ee0
    rc=r*rho1
    eec=ee*ee1
    print *,'rc,eec=',rc,eec
    z1=-30.
    z2=15.
    if (eec < 1) then
      z2=y2z(eec-1e-6)
    end if
    call zbrent (fsaha,z1,z2,z,tol)
    !tt=(twothirds*tt0)*(ee*ee1-y)/(1.+y)
    tt=(twothirds*tt0)*(ee*ee1-y)
    pp=(cdu/cpu*ee0/tt0)*tt*r
    tt=tt/(1.+y)
    print *,'r,ee,z,y,tt,niter:', r,ee,z,y,tt,niter
!
  end subroutine

!***********************************************************************
  real function y2z(y)
!
! Tranformation of variables: z=alog(y^2/(1-y))
!
    real y
    y2z=alog(y**2/((1.-y)+1e-7))
  end function

!***********************************************************************
  real function z2y(zl)
!
! Reverse transformation
!
    real zl,z
    z=exp(zl)
    z2y=2.*z/(z+sqrt(z**2+4.*z+1E-35))
  end function

!***********************************************************************
  real function T2E(T)
!
! Reverse transformation
!
    real T,z
    z = exp(-alog(rho1)-1./T+1.5*alog(T))
    y = 2.*z/(z+sqrt(z**2+4.*z+1E-35))
    T2E = 1.5*T*(1.+y) + y
  end function

!***********************************************************************
  REAL FUNCTION fsahaE(T)
    real T,z,E
    z = exp(-alog(rho1)-1./T+1.5*alog(T))
    y = 2.*z/(z+sqrt(z**2+4.*z+1E-35))
    fsahaE = 1.5*T*(1.+y) + y - eec
    niter=niter+1
  END FUNCTION

!***********************************************************************
  subroutine saha(r,ee,z,f,df)
!----------------------------------------------------------------
! we want to find the root of f
!----------------------------------------------------------------
    implicit none
    real, intent(in)  :: r,ee,z
    real, intent(out) :: f,df
    real              :: T,dT,rho,y,dydz
!
    y=z2y(z)
    dydz=y*(1.-y)/(2.-y)
!
    T=(ee*ee1-y)/(1.+y)
    rho=r*rho1
    dT=-(1.+T)/(1.+y)
    f=1.5*log(T)-log(rho)-1./T+log(1.-y)-2.*log(y)
    df=(dT/T)*(1.5+1./T)-1./(1.-y)-2./y
    df=df*dydz
    !print '(3(1pe12.3))', y, f, df
  end subroutine saha

!***********************************************************************
  function fsaha(z)
!----------------------------------------------------------------
! we want to find the root of f
!----------------------------------------------------------------
    implicit none
    real, intent(in)  :: z
    real              :: T1,fsaha
!
    y=z2y(z)
    if (y >= eec) then
      y=eec
      fsaha=-1e20
      return
    end if
    T1=(1.+y)/(eec-y)
    fsaha=-1.5*log(T1)-log(rc)-T1-z
    niter=niter+1
    print '(1x,a,3(1pe12.3))', 'fsaha:',z,y, fsaha
  end function

!***********************************************************************
SUBROUTINE zbrent(func,x1,x2,x,tol)
  implicit real(a-h,o-z)
  implicit integer(i-n)
  real, parameter:: itmax=100, eps=3.e-8
  niter=0
  a=x1
  b=x2
  fb=func(b)
  fa=func(a)
  if(fb*fa.gt.0.) then
    print *, 'root must be bracketed for zbrent.'
    stop
  endif 
  fc=fb
  do iter=1,itmax
    if(fb*fc.gt.0.) then
      c=a
      fc=fa
      d=b-a
      e=d
    endif
    Nflops=Nflops+2
    if(abs(fc).lt.abs(fb)) then
      a=b
      b=c
      c=a
      fa=fb
      fb=fc
      fc=fa
    endif
    tol1=2.*eps*abs(b)+0.5*tol
    xm=.5*(c-b)
    Nflops=Nflops+7
    if(abs(xm).le.tol1 .or. fb.eq.0.)then
      x=b
      return
    endif
    if(abs(e).ge.tol1 .and. abs(fa).gt.abs(fb)) then
      s=fb/fa
      if(a.eq.c) then
        p=2.*xm*s
        q=1.-s
        Nflops=Nflops+4
      else
        q=fa/fc
        r=fb/fc
        p=s*(2.*xm*q*(q-r)-(b-a)*(r-1.))
        q=(q-1.)*(r-1.)*(s-1.)
        Nflops=Nflops+17
      endif
      if(p.gt.0.) q=-q
      p=abs(p)
      if(2.*p .lt. min(3.*xm*q-abs(tol1*q),abs(e*q))) then
        e=d
        d=p/q
        Nflops=Nflops+2
      else
        d=xm
        e=d
      endif
      Nflops=Nflops+10
    else
      d=xm
      e=d
    endif
    a=b
    fa=fb
    if(abs(d) .gt. tol1) then
      b=b+d
    else
      b=b+sign(tol1,xm)
    endif
    Nflops=Nflops+3
    fb=func(b)
  end do
  print *, 'zbrent exceeding maximum iterations.'
  x=b
  return
END SUBROUTINE

END MODULE
