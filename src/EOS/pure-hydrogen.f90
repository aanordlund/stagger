! $Id: pure-hydrogen.f90,v 1.13 2004/06/25 20:54:06 aake Exp $
!***********************************************************************
MODULE eos
!
!  EOS for simple hydrogren plasma
!
  use params
  use units

  implicit none

  real, parameter:: chi=13.6, ee0=chi*echarge/mproton, tt0=chi*echarge/kB
! real, parameter:: rho0=mproton*(2.*pi*melectron*chi*echarge/hplanck**2)**1.5
  real, parameter:: rho0=0.25317730
  real, parameter:: twothirds=0.66666667
  real:: rho1,ee1
  integer:: calls, totiter

CONTAINS


!***********************************************************************
  subroutine eos_test
    implicit integer (i-n)
    implicit real (a-h,o-z)
    nt=51
    t0=1e1
    t1=1e6
    dlnt=log(t1/t0)/nt
    utt=46.
    rho1=cdu/rho0
    ee1=cku/ee0
    r=1
    print *,'rho1,ee1=',rho1,ee1
    open (7,file='temp.dat',form='formatted',status='unknown')
    do i=0,nt
      tta=exp(log(t0)+dlnt*i)
      ee=tta/utemp
      yy=rtsafe(r,ee,iterations=it)
      tt=(twothirds*tt0)*(ee*ee1-yy)/(1.+yy)
      print '(7(1pg12.3),i5)',r,ee*ee1,yy,(ee*ee1-yy)/(1.+yy),tta,tt,tt/tta,it
      write(7,'(7(1pg12.3),i5)') r,ee*ee1,yy,(ee*ee1-yy)/(1.+yy),tta,tt,tt/tta,it
    end do
    close(7)
  end subroutine

!***********************************************************************
  subroutine equation_of_state (r,ee,pp,tt)
    implicit none
    real, dimension(mx,my,mz):: r,ee,yy
    real, dimension(mx,my,mz), optional:: tt,pp
    integer:: iz

    rho1=cdu/rho0
    ee1=cku/ee0

    do iz=1,mz
      yy(:,:,iz) = ionfrac(r(:,:,iz),ee(:,:,iz))
    end do
    if (present(tt)) then
      tt=(twothirds*tt0)*(ee*ee1-yy)
      pp=(cdu/cpu*ee0/tt0)*tt*r
      tt=tt/(1.+yy)
    else
      pp=(twothirds*cdu/cpu*ee0)*r*(ee*ee1-yy)
    end if
    return
  end subroutine

!***********************************************************************
  function pressure2(r,ee,tt)
    implicit none
    real, dimension(mx,my):: r,ee,yy,pressure2
    real, dimension(mx,my), optional:: tt

    rho1=cdu/rho0
    ee1=cku/ee0

    yy = ionfrac(r,ee)
    if (present(tt)) then
      tt=(twothirds*tt0)*(ee*ee1-yy)
      pressure2=(cdu/cpu*ee0/tt0)*tt*r
      tt=tt/(1.+yy)
    else
      pressure2=(twothirds*cdu/cpu*ee0)*r*(ee*ee1-yy)
    end if
    return
  end function

!***********************************************************************
  subroutine eos2(r,ee,tt,pp)
    implicit none
    real, dimension(mx,my):: r,ee,yy
    real, dimension(mx,my), optional:: tt,pp

    rho1=cdu/rho0
    ee1=cku/ee0

    yy = ionfrac(r,ee)
    if (present(tt)) then
      tt=(twothirds*tt0)*(ee*ee1-yy)
      if (present(pp)) pp=(cdu/cpu*ee0/tt0)*tt*r
      tt=tt/(1.+yy)
    else if (present(pp)) then
      pp=(twothirds*cdu/cpu*ee0)*r*(ee*ee1-yy)
    end if
    return
  end subroutine

!***********************************************************************
  function ionfrac(r,ee)
!----------------------------------------------------------------
! root-finding loop
!----------------------------------------------------------------
    implicit none
    real, dimension(mx,my) :: r,ee
    real, dimension(mx,my) :: ionfrac
    integer                   :: i,j,k

    do j=1,my
       do i=1,mx
          ionfrac(i,j)=rtsafe(r(i,j),ee(i,j))
       end do
    end do
  end function ionfrac

!***********************************************************************
  function rtsafe(r,ee,iterations)
!----------------------------------------------------------------
! safe newton-raphson algorithm
!----------------------------------------------------------------
    implicit none
    real, intent(in)   :: r,ee
    real               :: rtsafe
    real, parameter    :: yacc=1.e-5
    integer, parameter :: maxit=100
    real               :: yh,yl,fh,fl,f,df,dy,dyold,temp
    integer            :: i, startiter
    integer, optional  :: iterations
    real, save         :: ylast=.5

    calls=calls+1
    startiter = totiter
!----------------------------------------------------------------------
! The temperature at ionization is around 0.3 eV, or about 40 times
! less than the ionization energy.  We thus loose a factor 40 in 
! precision of the total energy, so we need a low limiting yl value.
! At the temperature where ionization is nearly complete we need 
! a correspondingly less precise limiting yh value.
!----------------------------------------------------------------------
    yl=1e-7                           ! lower bound
    yh=1.-4e-6                        ! upper bound

    if (yh.ge.ee*ee1) yh=ee*ee1-yacc  ! values gt ee make no sense
                                      !   (negative temperatures)
    call saha(r,ee,yh,fh)             ! check the high range first
    if (fh.ge.0.) then                ! ... it is most common
       rtsafe=1.                      ! ionization fraction equal to one...
       ylast=yh
       if (present(iterations)) iterations=totiter-startiter
       return
    endif
    call saha(r,ee,yl,fl)
    if (fl.le.0.) then
       rtsafe=yl                      ! ionization fraction very small
       ylast=yl                       ! within specified accuracy, because
       if (present(iterations)) iterations=totiter-startiter
       return                         ! f is monotonically decreasing
    endif

    dyold=yh-yl
    dy=dyold
    if (ylast.ge.yh) ylast=.5*yh      ! check that last value
    rtsafe=ylast                      !   is not out of bounds
    call saha(r,ee,rtsafe,f,df)

    do i=1,maxit
       if (((rtsafe-yl)*df-f)*((rtsafe-yh)*df-f).gt.0. &
            .or. abs(2.*f).gt.abs(dyold*df)) then
          dyold=dy
          dy=.5*(yh-yl)
          rtsafe=yh-dy
          if (yh.eq.rtsafe) then
             ylast=rtsafe
             if (present(iterations)) iterations=totiter-startiter
             return
          end if
       else
          dyold=dy
          dy=f/df
          temp=rtsafe
          rtsafe=rtsafe-dy
          if (temp.eq.rtsafe) then
             ylast=rtsafe
             if (present(iterations)) iterations=totiter-startiter
             return
          end if
       end if
       if (abs(dy).lt.yacc) then
          ylast=rtsafe
          if (present(iterations)) iterations=totiter-startiter
          return
       end if
       call saha(r,ee,rtsafe,f,df)
       if (f.lt.0.) then
          yh=rtsafe
       else
          yl=rtsafe
       end if
    end do
    print *,'error: rtsafe exceeding maximum iterations'
    return
  end function rtsafe

!****************************************************************
  subroutine saha(r,ee,y,f,df)
!----------------------------------------------------------------
! we want to find the root of f
!----------------------------------------------------------------
    implicit none
    real, intent(in)               :: r,ee,y
    real, intent(out)              :: f
    real, intent(out), optional    :: df
    real                           :: T1,dT,rho

    T1=1.5*(1.+y)/(ee*ee1-y)                               ! inverse temperature
    f=-1.5*log(T1)-T1-real(log(r*rho1*dble(y)**2/(1.-y)))  ! combine logs for speed
    if (present(df)) then                                  ! optional derivative
      dT=-(1.+T1)/(T1*(1.+y))                              ! no need for this in ...
      df=(dT*T1)*(1.5+T1)-1./(1.-y)-2./y                   ! the many cases with y=0
    end if                                                 ! or y=1.
    totiter=totiter+1
  end subroutine saha

END MODULE

!***********************************************************************
  subroutine init_eos
    USE params
    USE eos
    do_ionization = .false.
    call read_eos
!    call eos_test
  end subroutine

!***********************************************************************
  subroutine read_eos
    USE params
    USE eos
    namelist /eqofst/ do_ionization
    if (stdin.ge.0) then
      rewind (stdin); rewind (stdin); read (stdin,eqofst)
    else
      read (*,eqofst)
    end if
    write (*,eqofst)
  end subroutine

!***********************************************************************
  real function eos_iterations (clear)
    USE params
    USE eos
    integer clear
    eos_iterations=totiter/(calls+1e-6)
    if (clear.eq.0) then
      totiter=0
      calls=0
    end if
  end function
