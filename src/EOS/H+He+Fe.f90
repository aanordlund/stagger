! $Id: H+He+Fe.f90,v 1.3 2006/07/05 03:24:45 aake Exp $
!***********************************************************************
MODULE eos
!
!  Under construction...
!
  real, parameter:: h     = 6.626e-34           ! Planck
  real, parameter:: m     = 9.109e-31           ! electron mass
  real, parameter:: mH    = 1.679e-27           ! proton mass
  real, parameter:: k_B   = 1.38054e-23         ! Boltzmann
  real, parameter:: qe    = 1.602e-19           ! electron charge
  real, parameter:: Xi_H  = 13.6                ! Hydrogen ion.pot.
  real, parameter:: Xi_He = 22.4                ! Helium ion.pot.
  real, parameter:: Xi_Fe = 7.9                 ! Iron ion.pot.
  real, parameter:: pi = 3.1415926536
  real(kind=8):: A_H, A_He, A_Fe, C_H, C_He, C_Fe, N, rhoc, N_atom, N_e, P
  real(kind=8):: N_H, N_He, N_Fe, N_HII, N_HeII, N_FeII

CONTAINS

!***********************************************************************
SUBROUTINE NewT (T)
  implicit none
  real(kind=8) T,a,c
!
!  Calculate temperature dependent factors for the ionization balance
!-----------------------------------------------------------------------
  a    = qe/(k_B*T)                             ! one ev = t_ev kelvin
  c    = 0.5*((h/k_B)*(h/(2.*pi*m*T)))**1.5     ! avoid under/overflow
  C_H  = exp(-Xi_H *a)/c                        ! hydrogen
  C_He = exp(-Xi_He*a)/c                        ! helium
  C_Fe = exp(-Xi_Fe*a)/c                        ! iron
END SUBROUTINE

!***********************************************************************
FUNCTION Ne_over_N (N_e)
  implicit none
!
!  Calculate the particle density ratio N/N_e, given the electron number
!  density N_e.  Temperature dependent C_ factors from common block.
!
  real(kind=8):: Ne_over_N, N_e, y
!-----------------------------------------------------------------------
  y = (1. + (A_H + A_He + A_Fe)/ &
    (A_H *C_H /(C_H +N_e) + &
     A_He*C_He/(C_He+N_e) + &
     A_Fe*C_Fe/(C_Fe+N_e)))
  Ne_over_N = 1./y
END FUNCTION

!***********************************************************************
FUNCTION Energy (Temp, rho)
  implicit none
!
!  Calculate the particle density ratio N/N_e, given the electron number
!  density N_e.  Temperature dependent C_ factors from common block.
!
  real(kind=8):: Energy, Temp, rho, y, ne_min, ne_max, a, b, lnNe, N_e
  real(kind=8), parameter:: tol= 1.e-5
  integer Nit
!-----------------------------------------------------------------------
  call NewT (Temp)
  Ne_max = 0.501*N                          ! trivial bounds   |  |
  Ne_min = 1e-10                            ! lower bound      |  |
  Ne_max = 1.0001*N*sqrt(Ne_over_N(N))      ! smart upper bound|  |
  a = N/C_H*(1.+A_He)                       ! H ionization     |  |
  b = (2.+A_He)                             ! (Section 2)      |  |
  Ne_min = N*2/(b+sqrt(b**2+4.*a))          ! smart lower bound|  |
  call zbrent (error, log(Ne_min), &        !                  |  |
      log(Ne_max), lnNe, tol, Nit)          ! call root finder |  |
  N_e = exp(lnNe)                           !                  |  |
  rhoc = rho
  N_atom = rho*(A_H+A_He+A_Fe)/(mH*(1.+4.*A_He+56.*A_Fe))
  y = (1. + (A_H + A_He + A_Fe)/ &
    (A_H *C_H /(C_H +N_e) + &
     A_He*C_He/(C_He+N_e) + &
     A_Fe*C_Fe/(C_Fe+N_e)))
  N_e = N_atom/y
  N_H  = N_atom*A_H /(A_H + A_He + A_Fe)
  N_He = N_atom*A_He/(A_H + A_He + A_Fe)
  N_Fe = N_atom*A_Fe/(A_H + A_He + A_Fe)
  N_HII  = N_H *(C_H /(C_H  + N_e))
  N_HeII = N_He*(C_He/(C_He + N_e))
  N_FeII = N_Fe*(C_Fe/(C_Fe + N_e))
  P = (N_atom+N_e)*k_B*Temp
  Energy = (1.5*P + qe*(N_HII*Xi_H+N_HeII*Xi_He+N_FeII*Xi_Fe))/rho
  print *,N_atom,y,N_e,N_H,P

END FUNCTION

!***********************************************************************
FUNCTION error (ln_N_e)
  implicit none
  real(kind=8):: error, ln_N_e, N_e, f
!-----------------------------------------------------------------------
  N_e = exp(ln_N_e)
  f = (N_e/N)/Ne_over_N(N_e)                    ! =1 when N(N_e)=N
  error = log(f)                               ! =0 when N(N_e)=N
  print '(2f12.6)',ln_N_e,error
END FUNCTION

!***********************************************************************
SUBROUTINE eos_test
  implicit none
!
  real, parameter:: min_T   =  2000.
  real, parameter:: max_T   = 15000.
  real, parameter:: delta_T =   500.            ! T values
  real, parameter:: min_lg_P   = -2.0
  real, parameter:: max_lg_P   =  8.0
  real, parameter:: delta_lg_P =  5.0           ! P values
!
  integer, parameter:: nT = (max_T    - min_T   )/delta_T    + 1     ! T dimension
  integer, parameter:: nP = (max_lg_P - min_lg_P)/delta_lg_P + 1     ! T dimension
  real(kind=8), parameter:: tol= 1.e-5                               ! tolerance
!
  real(kind=8):: f_e_max, T, TT(nT), lgP(nP)
  real(kind=8):: P, Ne_max, a, b, N_e, f_e(nT, nP), Ntot, Ne_min, Ne_Pmin, lnNe
  integer:: iT, iP, Nit
!
!-----------------------------------------------------------------------
!
  f_e_max = 0.501                               ! if all ionized
  A_H  = 1.0                                    ! Hydrogen normalized
  A_He = 0.1                                    ! Helium relative H
  A_Fe = 1e-4                                   ! Iron relative H
                                                !
  Ntot = 0                                      !
  do iT=1,nT                                    ! temperature loop-----
    T  = min_T + delta_T*iT                     ! temperature         |
    TT(iT) = T                                  ! array for plot      |
    call NewT(T)                                ! T dept factors      |
                                                !                     |
    do iP=1,nP                                  ! pressure loop ----  |
      lgP(iP) = min_lg_P + (iP-1)*delta_lg_P    ! for plot         |  |
      P = 10.**lgP(iP)                          ! pressure         |  |
      N = P/(k_B*T)                             ! number dens      |  |
                                                !                  |  |
      ne_max = f_e_max*N                        ! trivial bounds   |  |
      Ne_min = 1e-10                            ! lower bound      |  |
      Ne_max = 1.0001*N*sqrt(Ne_over_N(N))      ! smart upper bound|  |
      a = N/C_H*(1.+A_He)                       ! H ionization     |  |
      b = (2.+A_He)                             ! (Section 2)      |  |
      Ne_min = N*2/(b+sqrt(b**2+4.*a))          ! smart lower bound|  |
      call zbrent (error, log(Ne_min), &       !                  |  |
         log(Ne_max), lnNe, tol, Nit)          ! call root finder |  |
      N_e = exp(lnNe)                           !                  |  |
                                                !                  |  |
      f_e(it,iP) = N_e/N                        ! store ratio      |  |
      Ntot = Ntot + Nit                         ! count iterations |  |
      Ne_min = N_e
      if (iP.eq.0) then
        Ne_Pmin = Ne_min*T/(T+delta_T)*0.99
      endif
     end do                                     ! end pressure loop-  |
    print *,'T =',T,'      Ntot =',Ntot
  end do                                        ! end temperature loop- 
  print *,'Average no of iterations =',real(Ntot)/(nP*nT)
END SUBROUTINE

!***********************************************************************
SUBROUTINE zbrent(func,x1,x2,x,tol,niter)
  implicit real(kind=8) (a-h,o-z)
  implicit integer(i-n)
  real, parameter:: itmax=100, eps=3.e-8
  a=x1
  b=x2
  fa=func(a)
  fb=func(b)
  if(fb*fa.gt.0.) then
    print *, 'root must be bracketed for zbrent.'
    stop
  endif 
  fc=fb
  do iter=1,itmax
    if(fb*fc.gt.0.) then
      c=a
      fc=fa
      d=b-a
      e=d
    endif
    Nflops=Nflops+2
    if(abs(fc).lt.abs(fb)) then
      a=b
      b=c
      c=a
      fa=fb
      fb=fc
      fc=fa
    endif
    tol1=2.*eps*abs(b)+0.5*tol
    xm=.5*(c-b)
    Nflops=Nflops+7
    if(abs(xm).le.tol1 .or. fb.eq.0.)then
      x=b
      niter=iter
      return
    endif
    if(abs(e).ge.tol1 .and. abs(fa).gt.abs(fb)) then
      s=fb/fa
      if(a.eq.c) then
        p=2.*xm*s
        q=1.-s
        Nflops=Nflops+4
      else
        q=fa/fc
        r=fb/fc
        p=s*(2.*xm*q*(q-r)-(b-a)*(r-1.))
        q=(q-1.)*(r-1.)*(s-1.)
        Nflops=Nflops+17
      endif
      if(p.gt.0.) q=-q
      p=abs(p)
      if(2.*p .lt. min(3.*xm*q-abs(tol1*q),abs(e*q))) then
        e=d
        d=p/q
        Nflops=Nflops+2
      else
        d=xm
        e=d
      endif
      Nflops=Nflops+10
    else
      d=xm
      e=d
    endif
    a=b
    fa=fb
    if(abs(d) .gt. tol1) then
      b=b+d
    else
      b=b+sign(tol1,xm)
    endif
    Nflops=Nflops+3
    fb=func(b)
  end do
  print *, 'zbrent exceeding maximum iterations.'
  x=b
  niter=iter
  return
END SUBROUTINE

END MODULE

  USE eos
  real(kind=8) T, rho, E

  call eos_test

  print *,'T,rho ?'
  read *,T,rho
  E = Energy(T,rho)
  print *,T,rho,E

END
