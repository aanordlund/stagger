! $Id: H+He-table.f90,v 1.18 2006/07/05 23:25:51 aake Exp $
!***********************************************************************
MODULE eos
!
!  EOS for hydrogren + neutral Helium plasma
!
  use units
  implicit none

  real, parameter:: twothirds=2./3.

  real:: eezero, ee0, ee1, rho0, rho1, TT0, f_He, mu, m_e, m_H, k_B, eV, chiH, hbar, eosymin
  integer:: calls, totiter

  integer ma, mb
  real a0, a1, b0, b1, amin, amax, bmin, bmax, aamin, aamax, bbmin, bbmax
  character(len=40) tablefile
  logical do_table
  real, allocatable, dimension(:,:):: table
  real, allocatable, dimension(:,:,:):: yy, lnrho

  real eoscpu
CONTAINS

!****************************************************************
  subroutine saha(lnr,ee,y,f,df)
!----------------------------------------------------------------
! we want to find the root of f
!----------------------------------------------------------------
    use params
    implicit none
    real, intent(in)               :: lnr,ee,y
    real, intent(out)              :: f
    real, intent(out), optional    :: df
    real                           :: T1,dT1

    T1=1.5*(1.+f_He+y)/(ee*ee1-y)               ! inverse temperature
    f=-1.5*log(T1)-T1-lnr-log(rho1*y**2/(1.d0-y)) ! combine logs
    if (present(df)) then                       ! optional derivative
      dT1=(T1+1.5)/(ee*ee1-y)                   !  no need for this in ...
      df=-(dT1/T1)*(1.5+T1)-1./(1d0-y)-2d0/y    !  the many cases with y=0
    end if                                      !  or y=1.
  end subroutine saha

!***********************************************************************
! subroutine eos2(r,ee,pp,TT)
!   USE params
!   implicit none
!   real, dimension(mx,my), intent(in)            :: r,ee
!   real, dimension(mx,my), intent(out), optional :: pp, TT
!
!   if (present(pp)) call pressure (r, ee, pp)
!   if (present(tt)) call temperature (r, ee, tt)
! end subroutine eos2
END MODULE

!***********************************************************************
SUBROUTINE pressure (r, ee, p)
  use params
  use eos
  real, dimension(mx,my,mz), intent(in)  :: r,ee
  real, dimension(mx,my,mz), intent(out) :: p
  if (do_trace) print *,'pressure:',minval(ee),maxval(ee)
  call equation_of_state (r, ee, p, .true., p, .false., p, .false.)
END SUBROUTINE

!***********************************************************************
SUBROUTINE temperature (r, ee, tt)
  use params
  use eos
  real, dimension(mx,my,mz), intent(in)  :: r,ee
  real, dimension(mx,my,mz), intent(out) :: tt
  call equation_of_state (r, ee, p, .false., tt, .true., p, .false.)
END SUBROUTINE

!***********************************************************************
SUBROUTINE lyman_opacity (r, ee, rk)
  use params
  use eos
  use units
  real, dimension(mx,my,mz), intent(in)  :: r,ee
  real, dimension(mx,my,mz), intent(out) :: rk
  integer iz
  real, parameter:: lyman_sigma=6e-18                                   ! Lyman edge cross section (cgs)
  real c
  character(len=mid):: id='lyman_opacity $Id: H+He-table.f90,v 1.18 2006/07/05 23:25:51 aake Exp $'
!-----------------------------------------------------------------------
  call print_trace (id, dbg_cool, 'BEGIN')
  c = lyman_sigma*clu*(cdu/mproton)                                     ! conversion to 1/clu units
  do iz=izs,ize
    rk(:,:,iz) = c*r(:,:,iz)*yy(:,:,iz)
  end do
  call print_trace (id, dbg_cool, 'END')
END SUBROUTINE

!***********************************************************************
  subroutine equation_of_state (r,ee,p,do_p,TT,do_tt,ne,do_ne)
    use params
    implicit none
    real, dimension(mx,my,mz), intent(in)  :: r,ee
    real, dimension(mx,my,mz), intent(out) :: p,ne,tt
    logical do_p, do_tt, do_ne
    logical omp_in_parallel

    if (omp_in_parallel()) then
      call equation_of_state_omp (r,ee,p,do_p,TT,do_tt,ne,do_ne)
    else
      !$omp parallel
      call equation_of_state_omp (r,ee,p,do_p,TT,do_tt,ne,do_ne)
      !$omp end parallel
    end if
  end subroutine

!***********************************************************************
  subroutine equation_of_state_omp (r,ee,p,do_p,TT,do_tt,ne,do_ne)
    use params
    use units
    use eos
    implicit none
    real, dimension(mx,my,mz), intent(in)  :: r,ee
    real, dimension(mx,my,mz), intent(out) :: p,ne,tt
    integer  :: ix,iy,iz,ncall
    real:: ylast,elapsed
    logical do_p, do_tt, do_ne

    if (do_trace) print *,'eq_of_st: begin', omp_mythread

    call ionfrac(r,ee,yy)
    call dumpl(yy,'yy','eos.dmp',0)
    if (do_trace .and. eoscpu.eq.0.) then
      ncall = 0
      eoscpu = elapsed()
      eoscpu = 0.
      do while(eoscpu < 1.)
        ncall = ncall+1
        call ionfrac(r,ee,yy)
        eoscpu = eoscpu+elapsed()
      end do
      print *,'eos: ns/pt/sst =',eoscpu*1e9/mw/ncall
    end if

    if (do_ne) then
      do iz=izs,ize
        ne(:,:,iz)=yy(:,:,iz)*r(:,:,iz)*(cdu/(m_H*mu))
      end do
      call dumpl(ne,'ne','eos.dmp',3)
    end if
    if (do_p) then
      do iz=izs,ize
        P(:,:,iz)=twothirds*r(:,:,iz)*((ee(:,:,iz)-eezero)-yy(:,:,iz)*ee0)
      end do
      call dumpl(P,'pp','eos.dmp',1)
    end if
    if (do_tt) then
      do iz=izs,ize
        TT(:,:,iz)=TT0*(twothirds*((ee(:,:,iz)-eezero)*ee1-yy(:,:,iz))/(1.+f_He+yy(:,:,iz)))
      end do
      call dumpl(TT,'TT','eos.dmp',2)
    end if
    if (do_trace) print *,'eq_of_st: end', omp_mythread
  end subroutine

!***********************************************************************
  subroutine ionfrac(r,ee,y)
!----------------------------------------------------------------
! root-finding loop
!----------------------------------------------------------------
    use eos
    implicit none
    real, dimension(mx,my,mz), intent(in)  :: r,ee
    real, dimension(mx,my,mz), intent(out) :: y
    integer :: ix,iy,iz
    real :: ylast, rtsafe

    if (do_trace) print *,'ionfrac: begin'
!
    if (do_table) then
      !$omp master
      allocate (lnrho(mx,my,mz))
      !$omp end master
      !$omp barrier
      lnrho(:,:,izs:ize) = alog(r(:,:,izs:ize))
      !$omp barrier
      call eos_table (lnrho, ee, y)
      !$omp barrier
      !$omp master
      deallocate (lnrho)
      !$omp end master
    else 
      ylast=0.5
      do iz=izs,ize
      do iy=1,my
        do ix=1,mx
	  if (y(ix,iy,iz) > 0) ylast=y(ix,iy,iz)
          ylast=rtsafe(ylast,r(ix,iy,iz),ee(ix,iy,iz)-eezero)
          y(ix,iy,iz)=ylast
         end do
      end do
      end do
    end if
    if (do_trace) print *,'ionfrac: end'
!
  end subroutine

!***********************************************************************
  function rtsafe(ylast,lnr,ee)
!----------------------------------------------------------------
! safe newton-raphson algorithm
!----------------------------------------------------------------
    use eos
    real, intent(in)   :: ylast,lnr,ee
    real               :: rtsafe
    real, parameter    :: yacc=1e-5, yrel=1e-3
    integer, parameter :: maxit=100
    real               :: yh,yl,fh,fl,f,df,dynew,dyold,temp
    integer            :: i

!----------------------------------------------------------------------
! The temperature at ionization is around 0.3 eV, or about 40 times
! less than the ionization energy.  We thus loose a factor 40 in 
! precision of the total energy, so we need a low limiting yl value.
! At the temperature where ionization is nearly complete we need 
! a correspondingly less precise limiting yh value.
!----------------------------------------------------------------------

    yl=eosymin                                                          ! lower bound
    yh=1.-4e-6                                                          ! upper bound

    if (yh.ge.ee*ee1) yh=ee*ee1-yacc                                    ! values gt ee make no sense
                                                                        !   (negative temperatures)
    rtsafe=yh
    call saha(lnr,ee,rtsafe,fh)                                         ! check the high range first
    if (fh.ge.0.) return                                                ! ... it is most common
    rtsafe=yl
    call saha(lnr,ee,rtsafe,fl)
    if (fl.le.0.) return

    dyold=yh-yl
    dynew=dyold
    rtsafe=ylast                                                        !   is not out of bounds
    if (rtsafe.ge.yh) rtsafe=.5*yh                                      ! check that last value
    call saha(lnr,ee,rtsafe,f,df)

    do i=1,maxit
       if (((rtsafe-yl)*df-f)*((rtsafe-yh)*df-f).gt.0. &
            .or. abs(2.*f).gt.abs(dyold*df)) then
          dyold=dynew
          dynew=.5*(yh-yl)
          rtsafe=yh-dynew
          if (yh.eq.rtsafe) return
       else
          dyold=dynew
          dynew=f/df
          temp=rtsafe
          rtsafe=rtsafe-dynew
          if (temp.eq.rtsafe) return
       end if
       if (abs(dynew).lt.yacc.and.abs(f).lt.1.) return
!       if (abs(dynew).lt.yacc) return
       call saha(lnr,ee,rtsafe,f,df)
       if (f.lt.0.) then
          yh=rtsafe
       else
          yl=rtsafe
       end if
    end do
    rtsafe = 1.
    print *,'error: rtsafe exceeding maximum iterations'
    print *,'ylast,lnr,ee',ylast,lnr,ee
    stop
  end function rtsafe

!***********************************************************************
  real function eos_iterations (clear)
    use eos
    integer clear
    eos_iterations=totiter/(calls+1e-6)
    if (clear.eq.0) then
      totiter=0
      calls=0
    end if
  end function

!***********************************************************************
SUBROUTINE eos_table (aa,bb,y)
!
  USE params
  USE eos
  implicit none
  real, dimension(mx,my,mz), intent(in):: aa,bb
  real, dimension(mx,my,mz), intent(out):: y
  integer:: ix,iy,iz,ia,ib
  real:: a,b,pa,pb,qa,qb
!
  if (do_trace) print *,'eos_table: begin'
!
  amin=minval(aa) ; amax=maxval(aa)
  bmin=minval(bb) ; bmax=maxval(bb)
!
  if (amin.lt.a0 .or. amax.gt.(a0+(ma-1)/a1) .or. &
      bmin.lt.b0 .or. bmax.gt.(b0+(mb-1)/b1)) then
        call new_eos_table
  end if
!
  do iz=izs,ize
    do iy=1,my
      do ix=1,mx
        a = 1.+(aa(ix,iy,iz) - a0)*a1
        b = 1.+(bb(ix,iy,iz) - b0)*b1
        ia = min(max(1,ifix(a)),ma-1)
        ib = min(max(1,ifix(b)),mb-1)
        pa = min(max(a-ia,0.),1.) ; qa=1.-pa
        pb = min(max(b-ib,0.),1.) ; qb=1.-pb
        y(ix,iy,iz) = exp( &
          qb*(qa*table(ia  ,ib  ) + pa*table(ia+1,ib  ))  + &
          pb*(qa*table(ia  ,ib+1) + pa*table(ia+1,ib+1)))
      end do
    end do
  end do
  if (do_trace) print *,'eos_table: end'
END SUBROUTINE

!***********************************************************************
SUBROUTINE new_eos_table
  USE params
  USE eos
  implicit none
  real ylast,lnr,r,ee,rtsafe
  integer ia,ib

  aamin = min(aamin,amin-1.)
  aamax = max(aamax,amax+1.)
  bbmin = min(bbmin,bmin-.1)
  bbmax = max(bbmax,bmax+1.)
  a1 = (ma-1)/(aamax-aamin)
  b1 = (mb-1)/(bbmax-bbmin)
  a0 = aamin
  b0 = bbmin
  print *,'generating new table:'
  print *,'amin,da,amax=',aamin,1./a1,aamax
  print *,'bmin,db,bmax=',bbmin,1./b1,bbmax

  ylast=0.5
  do ib=1,mb
   do ia=1,ma
    lnr = amin+(ia-1)/a1
    ee  = bmin+(ib-1)/b1
    r = exp(lnr)
    ylast = rtsafe(ylast,r,ee-eezero)
    table(ia,ib) = alog(ylast)
   end do
  end do
END SUBROUTINE

!***********************************************************************
  subroutine init_eos
    USE params
    USE eos

    m_e =9.109390e-28                                   ! [g]
    m_H =1.673525e-24                                   ! [g]
    k_B =1.380658e-16                                   ! [erg/K]
    hbar=1.054573e-27                                   ! [erg*s]
    eV  =1.602177e-12                                   ! [erg]
    chiH=13.6*eV                                        ! [erg]

    f_He=0.1
    mu  =1.+4*f_He

    rho0 = m_H*mu*((chiH/hbar)*(m_e/(2.*pi*hbar)))**1.5 ! [g/cm^3]
    ee0 = chiH/(m_H*mu)                                 ! [erg/g]
    TT0 = chiH/k_B                                       ! [K]

    rho0 = rho0/cdu
    ee0  = ee0/cku
    print *,'rho0,ee0,TT0',rho0,ee0,TT0

    rho1 = 1./rho0
    ee1  = 1./ee0

    do_ionization = .true.
    do_table = .true.
    eosymin = 1e-5
    eezero = 0.

    ma = 300
    mb = 300

    amin = 1e30
    bmin = 1e30
    amax = -1e30
    bmax = -1e30

    aamin = amin
    aamax = amax
    bbmin = bmin
    aamax = bmax

    eoscpu = 0.

    call read_eos

    if (do_table) then
      allocate (table(ma,mb))
      a0 = 1e30
      b0 = 1e30
    end if
    allocate (yy(mx,my,mz))

    !$omp parallel
    do iz=izs,ize
      yy(:,:,iz)=0.
    end do
    !$omp end parallel

  end subroutine init_eos

!***********************************************************************
  subroutine read_eos
    USE params
    USE eos
    namelist /eqofst/ do_ionization, eosymin, do_table, eezero, ma, mb

    if (stdin.ge.0) then
      rewind (stdin); read (stdin,eqofst)
    else
      read (*,eqofst)
    end if
    write (*,eqofst)
  end subroutine
