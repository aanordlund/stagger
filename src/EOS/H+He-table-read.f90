! $Id: H+He-table-read.f90,v 1.15 2005/01/12 12:15:03 troels_h Exp $
!***********************************************************************
MODULE eos
!
!  USE from table
!
  use params
  use units
  implicit none

  real, parameter:: twothirds=2./3.

  real:: ee0, ee1, TT0, f_He, mu, m_H, k_B, eV, chiH, eosymin
  integer:: calls, totiter

  integer ma, mb
  real a0, a1, b0, b1
  character(len=40) tablefile
  logical do_table
  real, allocatable, dimension(:,:):: table
END MODULE

!***********************************************************************
SUBROUTINE equation_of_state (r,ee,p,do_p,TT,do_tt,ne,do_ne)
  use units
  use eos
  implicit none
  real, dimension(mx,my,mz), intent(in)  :: r,ee
  real, dimension(mx,my,mz), intent(out) :: p,ne
  real, dimension(mx,my,mz), intent(out) :: TT
  real, dimension(mx,my,mz)              :: y
!hpf$ distribute (*,*,block) :: r,ee,P,TT,y
  integer                                :: ix,iy,iz
  real:: ylast, gam1
  logical do_p, do_tt, do_ne

  if (.not. do_ionization) then
    gam1 = gamma-1.
    if (gamma.eq.1.) gam1=1.
    P = r*ee*gam1
    return
  end if

  call ionfrac(r,ee,y)
  call stats('y',y)

  if (do_ne) then
!$omp parallel do private(iz)
    do iz=1,mz
      ne(:,:,iz)=(y(:,:,iz)+eosymin)*r(:,:,iz)*(cdu/(m_H*mu))
    end do
    call stats('ne',ne)
  end if
  if (do_p) then
!$omp parallel do private(iz)
    do iz=1,mz
      P(:,:,iz)=twothirds*r(:,:,iz)*(ee(:,:,iz)-y(:,:,iz)*ee0)
    end do
    call stats('P',P)
  end if
  if (do_tt) then
!$omp parallel do private(iz)
    do iz=1,mz
      TT(:,:,iz)=TT0*(twothirds*(ee(:,:,iz)*ee1-y(:,:,iz))/(1.+f_He+y(:,:,iz)))
    end do
    call stats('tt',tt)
  end if

  if (do_trace) print *,'pressure: end'
END SUBROUTINE 


!***********************************************************************
SUBROUTINE init_eos
  USE eos

  do_ionization = .true.
  do_table = .true.
  eosymin = 1e-4

  m_H =1.673525e-24                                   ! [g]
  k_B =1.380658e-16                                   ! [erg/K]
  eV  =1.602177e-12                                   ! [erg]
  chiH=13.6*eV                                        ! [erg]

  f_He=0.1
  mu  =1.+4*f_He

  ee0 = chiH/(m_H*mu)                                 ! [erg/g]
  TT0 = chiH/k_B                                      ! [K]

  print *,'ee0,cku,chiH,m_H,mu:',ee0,cku,chiH,m_H,mu
  ee0  = ee0/cku
  ee1  = 1./ee0

  tablefile = '../input/USE/H+He.dat'

  call read_eos
  if (do_ionization) call read_table

END SUBROUTINE

!***********************************************************************
SUBROUTINE read_eos
  USE params
  USE eos
  namelist /eqofst/ do_ionization, eosymin, do_table, tablefile

  if (stdin.ge.0) then
    rewind (stdin); read (stdin,eqofst)
  else
    read (*,eqofst)
  end if
  write (*,eqofst)
END SUBROUTINE

!***********************************************************************
SUBROUTINE read_table
!
  USE eos
  if (do_trace) print *,'read_eos_table: ', tablefile
  open (3,file=tablefile,form='unformatted',status='old')
  read (3) ma,mb,a0,b0,a1,b1
  print *,'USE table params: ',ma,mb,a0,b0,a1,b1
  allocate (table(ma,mb))
  read (3) table
  close (3)
END SUBROUTINE

!***********************************************************************
SUBROUTINE pressure(r,ee,p)
  USE eos
  use units
  implicit none
  real, dimension(mx,my,mz), intent(in)  :: r,ee
  real, dimension(mx,my,mz), intent(out) :: p
  real, dimension(mx,my,mz) :: TT, ne
  real, dimension(mx,my,mz)                        :: y
!hpf$ distribute (*,*,block) :: r,ee,P,TT,y
  integer                                          :: ix,iy,iz
  real:: ylast, gam1

  if (do_trace) print *,'pressure'

  if (.not. do_ionization) then
    gam1 = gamma-1.
    if (gamma.eq.1.) gam1=1.
    P = r*ee*gam1
    return
  end if

  call ionfrac(r,ee,y)
  call stats('y',y)

!$omp parallel do private(iz)
    do iz=1,mz
      P(:,:,iz)=twothirds*r(:,:,iz)*(ee(:,:,iz)-y(:,:,iz)*ee0)
    end do
    call stats('P',P)

  if (do_trace) print *,'pressure: end'
END SUBROUTINE 

!***********************************************************************
SUBROUTINE ionfrac (r,ee,y)
!
  USE params
  implicit none
  real, dimension(mx,my,mz), intent(in)  :: r,ee
  real, dimension(mx,my,mz), intent(out) :: y
  real, dimension(mx,my,mz)              :: lnr
  real                                :: ylast
  integer                             :: ix,iy,iz
!
  if (do_trace) print *,'ionfrac: begin'
!
  lnr = alog(r)
  call eos_table (lnr, ee, y)
!
  if (do_trace) print *,'ionfrac: end'
END SUBROUTINE

!***********************************************************************
SUBROUTINE eos_table (aa,bb,y)
!
  USE params
  USE eos
  implicit none
  real, dimension(mx,my,mz), intent(in):: aa,bb
  real, dimension(mx,my,mz), intent(out):: y
  integer:: ix,iy,iz,ia,ib
  real:: a,b,pa,pb,qa,qb
  real:: amin,amax,bmin,bmax
!
  if (do_trace) print *,'eos_table: begin',lb,ub
!
  amin=minval(aa(:,lb:ub,:)) ; amax=maxval(aa(:,lb:ub,:))
  bmin=minval(bb(:,lb:ub,:)) ; bmax=maxval(bb(:,lb:ub,:))
!
  if (amin.lt.a0 .or. amax.gt.(a0+(ma-1)/a1) .or. &
      bmin.lt.b0 .or. bmax.gt.(b0+(mb-1)/b1)) then
        print *, 'warning: outside USE table'
        print *, 'a0,amin,amax,a2', a0, amin, amax, a0+(ma-1)/a1
        print *, 'b0,bmin,bmax,b2', b0, bmin, bmax, b0+(mb-1)/b1
  end if
!
!$omp parallel do private(ix,iy,iz,a,b,ia,ib,pa,pb,qa,qb)
  do iz=1,mz
    do iy=1,my
      do ix=1,mx
        a = 1.+(aa(ix,iy,iz) - a0)*a1
        b = 1.+(bb(ix,iy,iz) - b0)*b1
        ia = min(max(1,ifix(a)),ma-1)
        ib = min(max(1,ifix(b)),mb-1)
        pa = min(max(a-ia,0.),1.) ; qa=1.-pa
        pb = min(max(b-ib,0.),1.) ; qb=1.-pb
        y(ix,iy,iz) = exp( &
          qb*(qa*table(ia  ,ib  ) + pa*table(ia+1,ib  ))  + &
          pb*(qa*table(ia  ,ib+1) + pa*table(ia+1,ib+1)))
      end do
    end do
  end do
!
  if (do_trace) print *,'eos_table: end'
END SUBROUTINE

FUNCTION eos_iterations(clear)
  USE eos
  real eos_iterations
  integer clear
!
  eos_iterations = 0.
END FUNCTION


!***********************************************************************
SUBROUTINE temperature (r,ee,TT)
  USE units
  USE eos
  implicit none
  real, dimension(mx,my,mz), intent(in)  :: r,ee
  real, dimension(mx,my,mz)              :: p,ne
  real, dimension(mx,my,mz), intent(out) :: TT
  real, dimension(mx,my,mz)              :: y
!hpf$ distribute (*,*,block) :: r,ee,P,TT,y
  integer                                          :: ix,iy,iz
  real:: ylast, gam1

  if (.not. do_ionization) then
    gam1 = gamma-1.
    if (gamma.eq.1.) gam1=1.
    P = r*ee*gam1
    return
  end if

  call ionfrac(r,ee,y)
  call stats('y',y)

!$omp parallel do private(iz)
    do iz=1,mz
      ne(:,:,iz)=(y(:,:,iz)+eosymin)*r(:,:,iz)*(cdu/(m_H*mu))
    end do
    call stats('ne',ne)
!$omp parallel do private(iz)
    do iz=1,mz
      P(:,:,iz)=twothirds*r(:,:,iz)*(ee(:,:,iz)-y(:,:,iz)*ee0)
    end do
    call stats('P',P)
!$omp parallel do private(iz)
    do iz=1,mz
      TT(:,:,iz)=TT0*(twothirds*(ee(:,:,iz)*ee1-y(:,:,iz))/(1.+f_He+y(:,:,iz)))
    end do
    call stats('tt',tt)

  if (do_trace) print *,'pressure: end'
END SUBROUTINE 
