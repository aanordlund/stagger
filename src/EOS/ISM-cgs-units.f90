module units
!
!  ISM cgs units
!
  use params
  implicit none

  double precision, parameter:: kB=1.38e-16, mproton=1.6726e-24, melectron=9.1094e-28, &
                                echarge=1.602e-12, hplanck=6.6261e-27, stefan=5.675e-5

  double precision, parameter:: cdu=1.d-24, clu=3.0857d18, ctu=3.1557d13, &
                                cvu=clu/ctu, cmu=((cdu*clu)*clu)*clu, msun=1.989e33, &
                                cku=cvu**2, cpu=cdu*cku, ceu=cpu*clu**3, mu0=0.6, gam=5./3., &
                                cte=cku*mu0*mproton/kB

  real, parameter:: lncV=-3.8286        ! mu=0.60 is used; [cV]=ceu cmu^-1 K^-1
  real, parameter:: utemp=cte*(gam-1.)  ! ee->K
  real, parameter:: vesc0=-0.02         ! -(8*pi*G/5)^1/2; [vesc0]=cdu^-1/2 ctu^-1  
  real, parameter:: lnfehsun=6.6280     ! (m_H/m_Fe)*10^(H-Fe_sun)*X_p^-1
  real           :: u_temp

end module units
