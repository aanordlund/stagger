*
*  Common for the lookup subroutine and table program
*
      integer mtabs,mtabv,iupdte,nvar,nbox,itab,mtab
      real xmin,dbox,ul,ut,ur,ps,tff,grv,abnd
     & tmean,tamp,rhm,xcorr,thmin,thmax,dth,eemin,eemax,deetab,tab
     & table
      parameter (mtabs=12, mtabv=12*my)
      common /cscr/tabpad(mw),
     & iupdte,nvar,nbox,xmin,dbox,ul,ut,ur,eps,tff,grv,abnd,
     & tmean(my),tamp(my),rhm(my),xcorr(my),
     & thmin(my),thmax(my),dth(my),
     & eemin(my),eemax(my),deetab(my),
     & itab(my),mtab(my),
     & tab(mtaba)
      real table(mtabs+mtabv+mtaba)
      equivalence (table,iupdte),(iel,xmin),(ion,dbox)
