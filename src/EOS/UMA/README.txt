
To compile the table generation code and prepare for running,
edit the dimensions in "cparam.inc" and do

make

The table generation needs a few files from this directory, 
some of which are made by "make".  Theretore it is easier to
link the two files "scratch.dat" and "mesh.dat" from the data
directory to hear, and then run

./tabcmp.x < tabcmp.in | tee tabcmp.log
./tabinv.x < tabinv.in | tee tabinv.log

The file "table.dat" should then be copied/moved back to the
data directory (keep the previous one for the record).
