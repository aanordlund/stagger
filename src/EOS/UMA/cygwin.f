! $Id: cygwin.f,v 1.1 2007/10/19 16:58:16 aake Exp $
!***********************************************************************
      function fdtime()

!  Return the elapsed cpu + system time since the previous call

      real cpu(2)
      fdtime = dtime(cpu)
      end
!***********************************************************************
      function fetime()

!  Return the elapsed cpu + system time since the program start

      real cpu(2)
      fetime = etime(cpu)
      end
!***********************************************************************
      subroutine fflush(lun)
      integer lun
      call flush(lun)
      end
!***********************************************************************
      integer function lrec(m)

!  Return the RECL=LREC(M) value, for M words.

      lrec = 4*m
      end
