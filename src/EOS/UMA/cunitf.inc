*
*  cunitf
*
*  Include file for output.f
*
*     mf          : max number of open files
*     mrec        : max number of user records per file
*     debug       : debug flag
*     i           : index into tables
*     lunp        : last used logical unit
*     nunit       : number of units open
*     lunit(i)    : logical unit numbers
*     inext(i)    : next record
*     ipos(irec,i): record pointer
*     lbuf(i)     : record length
*     nwords(i)   : number of words transferred
*     iunit(i)    : channel number returned from C-interface
*
      parameter (mf=5,mrec=1000)
      common /copen/debug
        logical debug
      common /cunitf/i,lunp,nunit,lunit(mf),inext(mf),lbuf(mf),
     &  nwords(mf),iunit(mf),ipos(mrec,mf)
