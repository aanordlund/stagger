#!/bin/csh

# Check that mtau is at least as large as mz, and update subs.inc
# only if it needs to be updated.

set my = `grep 'my=' cparam.inc | sed -e 's/.*my=\([0-9]*\).*/\1/'`
set mtau = `grep 'mtau=' subs.inc | sed -e 's/.*mtau=\([0-9]*\).*/\1/'`

if ($mtau < $my) then
ed subs.inc <<EOF
/mtau=/s/mtau=[0-9]*/mtau=$my/
w
q
EOF
endif
