*
*  cmtab
*
*  mtable:  max number of words for the lookup table common
*  mtabla:  max number od words for the lookup table
*  minter:  max number of interpolation intervals, should belong
*           to the series n(i+1)=n(i)*2-1, n(1)=4; i.e., 4, 7, 13,
*           25, 49, 97, 193, 385, 769, 1537, 3073, ...
*
      integer mtable,mtaba,minter,mbyte
      parameter (mbyte=1024*256)
      parameter (mtable=200*mbyte,mtaba=mtable-12*my-12,minter=3073)
