**********************************************************************
*
*  Various math routines.
*
*  Update history:
*                       .
*    -jul-86:  Coded by Ake Nordlund and Bob Stein.
*  14-aug-87:  Split up in subroutines, to get separate gprof-timings.
*  15-sep-87:  Added subz() and dividz() routines.
*  16-sep-87:  Added subavz() routine.
*  18-oct-87:  Added muladd() routine.
*  13-nov-87/aake&bob:  Added multz() routine.
*  14-nov-87/aake:  Added extrem() routine.
*  18-dec-87/aake:  Added crsadd() routine.
*  01-jan-88/aake:  Added absv() and absh() routines.
*  01-jan-88/aake:  Changed cross, crsadd for same source and target
*  04-jan-88/aake:  z-loops in addv,mulvs,scalev to improve concurrency
*  08-aug-89/aake:  added mulsub(), divneg(), dotsub() routines
*  09-aug-89/aake:  addc(), subc(), averh(), averh1() from mhd version
*  21-aug-89/aake:  magn(), magn2(), magnh() added
*  05-mar-90/aake:  scaldn() for no boundary update
*  13-jun-90/aake:  added subavz back in, for raster convenience
*  10-jun-91/aake:  averh loops over lm instead of n; much faster
*
************************************************************************
      subroutine add (a,b,c)
      include 'cparam.inc'
      dimension a(mw),b(mw),c(mw)
*
*  add scalars
*
      do 110 lmn=1,mw
110     c(lmn)=a(lmn)+b(lmn)
      end
*
************************************************************************
      subroutine addc (a,b,c)
      include 'cparam.inc'
      dimension a(mw),c(mw)
*
*  add constant to scalar
*
      do 110 lmn=1,mw
110     c(lmn)=a(lmn)+b
      end
*
************************************************************************
      subroutine scladd (s,a,b,c)
      include 'cparam.inc'
      dimension a(mw),b(mw),c(mw)
*
*  add constant*scalar to scalar
*
      do 115 lmn=1,mw
115     c(lmn)=s*a(lmn)+b(lmn)
      end
*
************************************************************************
      subroutine sclad1 (s,a,b,c)
      include 'cparam.inc'
      dimension a(mp,mz),b(mp,mz),c(mp,mz)
*
*  add scalars, for n=2,mz
*
      do 115 n=2,mz
      do 115 lm=1,mp
115   c(lm,n)=s*a(lm,n)+b(lm,n)
      end
*
************************************************************************
      subroutine scladn (s,a,b,c)
      include 'cparam.inc'
      dimension a(mp,mz),b(mp,mz),c(mp,mz)
*
*  add scalars, for n=2,mz-1
*
      do 115 n=2,mz-1
      do 115 lm=1,mp
115   c(lm,n)=s*a(lm,n)+b(lm,n)
      end
*
************************************************************************
      subroutine addv (av,bv,cv)
      include 'cparam.inc'
      dimension av(mw,3),bv(mw,3),cv(mw,3)
*
*  add vectors
*
      do 120 i=1,3
        do 120 lmn=1,mw
120       cv(lmn,i)=av(lmn,i)+bv(lmn,i)
      end
*
************************************************************************
      subroutine addscv (s,av,bv,cv)
      include 'cparam.inc'
      dimension av(mw,3),bv(mw,3),cv(mw,3)
*
*  add scaled vectors
*
      do 125 i=1,3
        do 125 lmn=1,mw
125   cv(lmn,i)=s*av(lmn,i)+bv(lmn,i)
      end
*
************************************************************************
      subroutine sub (a,b,c)
      include 'cparam.inc'
      dimension a(mw),b(mw),c(mw)
*
*  sub scalars
*
      do 111 lmn=1,mw
111     c(lmn)=a(lmn)-b(lmn)
      end
*
************************************************************************
      subroutine subc (a,b,c)
      include 'cparam.inc'
      dimension a(mw),c(mw)
*
*  sub constant from scalar
*
      do 110 lmn=1,mw
110     c(lmn)=a(lmn)-b
      end
*
************************************************************************
      subroutine subz (a,b,c)
      include 'cparam.inc'
      dimension a(mp,mz),b(mz),c(mp,mz)
*
*  subtract fct of z from scalar
*
      do 112 n=1,mz
      do 112 lm=1,mp
112     c(lm,n)=a(lm,n)-b(n)
      end
*
************************************************************************
      subroutine subavz (a,c)
      include 'cparam.inc'
      dimension a(mp,mz),b(mz),c(mp,mz)
*
      call averh(a,b)
      call subz (a,b,c)
      end
*
************************************************************************
      subroutine subv (av,bv,cv)
      include 'cparam.inc'
      dimension av(mw,3),bv(mw,3),cv(mw,3)
*
*  sub vectors
*
      do 121 i=1,3
        do 121 lmn=1,mw
121       cv(lmn,i)=av(lmn,i)-bv(lmn,i)
      end
*
************************************************************************
      subroutine subscv (s,av,bv,cv)
      include 'cparam.inc'
      dimension av(mw,3),bv(mw,3),cv(mw,3)
*
*  subtract scaled vectors
*
      do 122 i=1,3
        do 122 lmn=1,mw
122   cv(lmn,i)=s*av(lmn,i)-bv(lmn,i)
      end
*
************************************************************************
      subroutine mult (a,b,c)
      include 'cparam.inc'
      dimension a(mw),b(mw),c(mw)
*
*  multiply scalars
*
      do 130 lmn=1,mw
130     c(lmn)=a(lmn)*b(lmn)
      end
*
************************************************************************
      subroutine multv (av,bv,cv)
      include 'cparam.inc'
      dimension av(mw,3),bv(mw,3),cv(mw,3)
*
*  multiply vectors
*
      do 140 i=1,3
        do 140 lmn=1,mw
140       cv(lmn,i)=av(lmn,i)*bv(lmn,i)
      end
*
************************************************************************
      subroutine mulvs (av,b,cv)
      include 'cparam.inc'
      dimension b(mw),av(mw,3),cv(mw,3)
*
*  mult vector with scalar
*
      do 240 i=1,3
        do 240 lmn=1,mw
240       cv(lmn,i)=av(lmn,i)*b(lmn)
      end
*
************************************************************************
      subroutine muladd (a,b,c)
      include 'cparam.inc'
      dimension a(mw),b(mw),c(mw)
*
*  multiply scalars, add result
*
      do 100 lmn=1,mw
100     c(lmn)=c(lmn)+a(lmn)*b(lmn)
      end
************************************************************************
      subroutine mulsub (a,b,c)
      include 'cparam.inc'
      dimension a(mw),b(mw),c(mw)
*
*  multiply scalars, subtract result
*
      do 100 lmn=1,mw
100     c(lmn)=c(lmn)-a(lmn)*b(lmn)
      end
************************************************************************
      subroutine mulneg (a,b,c)
      include 'cparam.inc'
      dimension a(mw),b(mw),c(mw)
*
*  multiply scalars, negate result
*
      do 100 lmn=1,mw
100     c(lmn)=-a(lmn)*b(lmn)
      end
************************************************************************
      subroutine multz (a,b,c)
      include 'cparam.inc'
      dimension a(mp,mz),b(mz),c(mp,mz)
*
*  multiply scalar by function of z
*
      do 131 n=1,mz
      do 131 lm=1,mp
131     c(lm,n)=a(lm,n)*b(n)
      end
*
************************************************************************
      subroutine divide (a,b,c)
      include 'cparam.inc'
      dimension a(mw),b(mw),c(mw)
*
*  divide scalars
*
      do 150 lmn=1,mw
150     c(lmn)=a(lmn)/b(lmn)
      end
*
************************************************************************
      subroutine divneg (a,b,c)
      include 'cparam.inc'
      dimension a(mw),b(mw),c(mw)
*
*  (-) divide scalars
*
      do 150 lmn=1,mw
150   c(lmn)=-a(lmn)/b(lmn)
      end
*
************************************************************************
      subroutine dividv (av,bv,cv)
      include 'cparam.inc'
      dimension av(mw,3),bv(mw,3),cv(mw,3)
*
*  divide vectors
*
      do 160 i=1,3
        do 160 lmn=1,mw
160       cv(lmn,i)=av(lmn,i)/bv(lmn,i)
      end
*
************************************************************************
      subroutine divvs (av,a,bv)
      include 'cparam.inc'
      dimension a(mw),av(mw,3),bv(mw,3)
*
*  divide vector with scalar
*
      do 230 i=1,3
        do 230 lmn=1,mw
230       bv(lmn,i)=av(lmn,i)/a(lmn)
      end
*
************************************************************************
      subroutine dividz (a,b,c)
      include 'cparam.inc'
      dimension a(mp,mz),b(mz),c(mp,mz)
*
*  divide scalar by fct of z
*
      do 151 n=1,mz
      do 151 lm=1,mp
151     c(lm,n)=a(lm,n)/b(n)
      end
*
************************************************************************
      subroutine scale (s,a,b)
      include 'cparam.inc'
      dimension a(mw),b(mw)
*
*  scale scalar
*
      do 180 lmn=1,mw
180     b(lmn)=s*a(lmn)
      end
*
************************************************************************
      subroutine scalev (s,av,bv)
      include 'cparam.inc'
      dimension av(mw,3),bv(mw,3)
*
*  scale vector
*
      do 190 i=1,3
        do 190 lmn=1,mw
190       bv(lmn,i)=s*av(lmn,i)
      end
*
************************************************************************
      subroutine dot (av,bv,c)
      include 'cparam.inc'
      dimension av(mw,3),bv(mw,3),c(mw)
*
*   dot product of av and bv
*
      do 170 lmn=1,mw
        c(lmn) = av(lmn,1)*bv(lmn,1)+
     &    av(lmn,2)*bv(lmn,2)+av(lmn,3)*bv(lmn,3)
170   continue
      end
*
************************************************************************
      subroutine dotadd (av,bv,c)
      include 'cparam.inc'
      dimension av(mw,3),bv(mw,3),c(mw)
*
*   add dot product of av and bv
*
      do 171 lmn=1,mw
        c(lmn) = c(lmn)+av(lmn,1)*bv(lmn,1)+
     &    av(lmn,2)*bv(lmn,2)+av(lmn,3)*bv(lmn,3)
171   continue
      end
*
************************************************************************
      subroutine dotneg (av,bv,c)
      include 'cparam.inc'
      dimension av(mw,3),bv(mw,3),c(mw)
*
*   (-) dot product of av and bv
*
      do 172 lmn=1,mw
        c(lmn) = -av(lmn,1)*bv(lmn,1)-
     &    av(lmn,2)*bv(lmn,2)-av(lmn,3)*bv(lmn,3)
172   continue
      end
*
************************************************************************
      subroutine dotsub (av,bv,c)
      include 'cparam.inc'
      dimension av(mw,3),bv(mw,3),c(mw)
*
*   dot product of av and bv subtracted from c
*
      do 170 lmn=1,mw
         c(lmn) = c(lmn)-av(lmn,1)*bv(lmn,1)-
     &      av(lmn,2)*bv(lmn,2)-av(lmn,3)*bv(lmn,3)
170   continue
      end
*
**********************************************************************
      subroutine cross (av,bv,cv)
*
      include 'cparam.inc'
      dimension av(mw,3),bv(mw,3),cv(mw,3)
*
*  Cross product
*
      do 100 lmn=1,mw
        av1=av(lmn,1)
        av2=av(lmn,2)
        bv1=bv(lmn,1)
        bv2=bv(lmn,2)
        cv(lmn,1)=av2      *bv(lmn,3)-av(lmn,3)*bv2
        cv(lmn,2)=av(lmn,3)*bv1      -av1      *bv(lmn,3)
        cv(lmn,3)=av1      *bv2      -av2      *bv1
100   continue
      end
*
**********************************************************************
      subroutine crsadd (av,bv,cv)
*
      include 'cparam.inc'
      dimension av(mw,3),bv(mw,3),cv(mw,3)
*
*  Cross product, add result
*
      do 100 lmn=1,mw
        av1=av(lmn,1)
        av2=av(lmn,2)
        bv1=bv(lmn,1)
        bv2=bv(lmn,2)
        cv(lmn,1)=cv(lmn,1)+av2      *bv(lmn,3)-av(lmn,3)*bv2
        cv(lmn,2)=cv(lmn,2)+av(lmn,3)*bv1      -av1      *bv(lmn,3)
        cv(lmn,3)=cv(lmn,3)+av1      *bv2      -av2      *bv1
100   continue
      end
*
************************************************************************
      subroutine crossi (av,bv,c,i)
*
      include 'cparam.inc'
      dimension av(mw,3),bv(mw,3),c(mw)
*
*  i(th) component of cross product
*  NOTE!  Target (c) must be different from sources (av,bv).
*
      if (i.eq.1) then
        do 100 lmn=1,mw
          c(lmn)=av(lmn,2)*bv(lmn,3)-av(lmn,3)*bv(lmn,2)
100     continue
      else if (i.eq.2) then
        do 110 lmn=1,mw
          c(lmn)=av(lmn,3)*bv(lmn,1)-av(lmn,1)*bv(lmn,3)
110     continue
      else if (i.eq.3) then
        do 120 lmn=1,mw
          c(lmn)=av(lmn,1)*bv(lmn,2)-av(lmn,2)*bv(lmn,1)
120     continue
      else
        stop 'crossi'
      endif
      end
*
************************************************************************
      subroutine sqr (a)
      include 'cparam.inc'
      dimension a(mw)
*
*  square root scalar
*
      do 220 lmn=1,mw
220     a(lmn)=sqrt(amax1(a(lmn),1.e-30))
      end
*
************************************************************************
      subroutine exps (a,b)
      include 'cparam.inc'
      dimension a(mw),b(mw)
*
*  exponential scalar
*
      do 260 lmn=1,mw
260     b(lmn)=exp(a(lmn))
      end
*
************************************************************************
      subroutine alogs (a,b)
      include 'cparam.inc'
      dimension a(mw),b(mw)
*
*  log scalar
*
      do 270 lmn=1,mw
270     b(lmn)=alog(a(lmn))
      end
*
************************************************************************
      subroutine set (s,a)
      include 'cparam.inc'
      dimension a(mw)
*
*  set scalar
*
      do 250 lmn=1,mw
250     a(lmn)=s
      end
*
************************************************************************
      subroutine copy (a,b)
      include 'cparam.inc'
      dimension a(mw),b(mw)
*
*  copy scalar
*
      do 280 lmn=1,mw
280     b(lmn)=a(lmn)
      end
*
************************************************************************
      subroutine move (a,b,n)
      include 'cparam.inc'
      dimension a(mw),b(mw)
      do 290 i=1,n
290     b(i)=a(i)
      end
************************************************************************
      function smax (a)
*
*  max of abs of scalar
*
      include 'cparam.inc'
      dimension a(mw)
*
      tmp=abs(a(1))
      do 300 i=2,mw
300     tmp=amax1(tmp,abs(a(i)))
      smax=tmp
      end
*
************************************************************************
      subroutine magn (av,b)
*
*  Calculate the magnitude of a vector
*
************************************************************************
*
      include 'cparam.inc'
*
      real av(mw,3),b(mw)
      do 100 lmn=1,mw
        b(lmn)=sqrt(av(lmn,1)**2+av(lmn,2)**2+av(lmn,3)**2)
100   continue
      end
************************************************************************
      subroutine magn2 (av,b)
*
*  Calculate the square magnitude of a vector
*
************************************************************************
*
      include 'cparam.inc'
*
      real av(mw,3),b(mw)
      do 100 lmn=1,mw
        b(lmn)=av(lmn,1)**2+av(lmn,2)**2+av(lmn,3)**2
100   continue
      end
*
************************************************************************
      subroutine magnh (av,b)
*
*  Calculate the magnitude of the horizontal components of a vector
*
************************************************************************
*
      include 'cparam.inc'
*
      real av(mw,3),b(mw)
      do 100 lmn=1,mw
        b(lmn)=sqrt(av(lmn,1)**2+av(lmn,2)**2)
100   continue
      end
*
************************************************************************
      subroutine averh (a,av) 
*
*  Horizontal average.
*
*  NOTE!  Unlike most other 'math' routines, this one assumes mp=nx*ny.
*
************************************************************************
*
      include 'cparam.inc'
      dimension a(mp,mz),av(mz)
*
      do 100 n=1,mz
        sum=0.0
        do 101 lm=1,mp
          sum=sum+a(lm,n)
101     continue
        av(n)=sum/mp
100   continue
*
      end
*
************************************************************************
      subroutine averh1 (a,am)
*
*  Calculate the horizontal average of a variable at single height.
*  Vectorize by using partial sums.
*
************************************************************************
*
      include 'cparam.inc'
*
      real a(mx,my),b(mx)
      do 101 l=1,mx
        b(l)=0.
101   continue
      do 102 m=1,my
      do 102 l=1,mx
        b(l)=b(l)+a(l,m)
102   continue
      am=0.
      do 103 l=1,mx
        am=am+b(l)
103   continue
      am=am/mp
      end
