*
*  Read a formatted file with giant line (opacity distribution
*  function) informattion, and write it out unformatted, for
*  the subs.f package to read in opac().
*
*  Update history:
*
*  890831/aake:  removed status='new' in open (to make similar
*                to cnoin.f
*
c     program gib(tape5,output,tape11)  
      common nssg(5),xl(20),ylin(200)
c
      common /cline1/linun,xlinlo,xlinup,ntskal,tskal(30),npskal,
     *peskal(30),ipebeg(110),ipeend(110),npex(110)  
      common /cvaagl/nlb,xlb(500),w(500)
      common /cline4/iline  
      data dum/0.0/ 
c
c write giant line heading information  
      iline=1
      linun=3
      open (unit=10,file='gibin.dat',status='old')
      rewind 10
      read (10,102) ny,kr,nl,n,(nssg(i),i=1,n),nr,npskal,ntskal 
     *,(ipebeg(i),i=1,ntskal),(ipeend(i),i=1,ntskal)
102   format(15i5)  
      read (10,101) wave,dwave,peskal(1),pstep,(tskal(i),i=1,ntskal)
      read (10,101) (ylin(i),i=1,ny)
      open (unit=3,file='odf.dat',form='unformatted')
      iwr=1 
      write (3) ny,(ylin(i),i=1,ny),kr,wave,nl,(dum,i=1,nl)
     *,n,(nssg(i),i=1,n)
     *,nr,dwave,npskal,peskal(1),pstep,ntskal,(tskal(i),i=1,ntskal) 
     *,(ipebeg(i),i=1,ntskal),(ipeend(i),i=1,ntskal)
      print 50,(nssg(i),i=1,n)  
50    format(1h0,19x,'giant lines from gib file',5i4)
c
c calculate giant line wavelength region
      xlinlo=wave
      xlinup=wave+nr*dwave  
      print 51,nr,dwave,nl,xlinlo,xlinup
51    format(19x,i3,' giant lines of width',f5.0,' with',i3,
     *' points in each, covering',f6.0,' to',f7.0,' angstroms') 
      print 52,ntskal,(tskal(i),i=1,ntskal) 
52    format(' nt,tsk=',i3,10f7.0)  
      print 53,peskal(1),pstep,(ipebeg(i),ipeend(i),i=1,ntskal) 
53    format(' pe,pst=',3x,2f7.4/' beg,end=',20i3)  
c
100   read (10,101,end=900) (ylin(i),i=1,ny)
      if (ylin(1).eq.0.0) go to 900 
      write (3) ny,(ylin(i),i=1,ny)
      iwr=iwr+1 
101   format(5e15.8)
      goto 100  
900   print 901,iwr 
901   format(' number of wavelenghts =',i4) 
      end
