************************************************************************
      program tabprg
*
*  Update history:
*
*  10-mar-85:  ntable=mtable, to make sure xcorr written
*  10-mar-85:  xcorr(k)=xcorrk
*  13-nov-87:  take alog of source functions
*  13-nov-87:  rhm1=alog(rhom(1)) (was alog(0.1*rhom(1))
*  02-dec-87:  rhm1=alog(0.5*rhomin)
*  11-jan-88:  force monotonous tmean, for interpolation of xcorr
*  18-jan-88:  incorporate xcorr() into output absorption coefficient
*  19-jan-88:  new envelop construction
*  18-feb-88:  m[xy] -> n[xy] in code; allows n[xy] < m[xy]
*  18-feb-88:  testing nz.eq.mz; must be equal
*  03-mar-88:  pow(7) (was 5)
*  18-apr-88:  xcorr stuff bracketed by if (nbox.gt.0) then
*  05-may-88:  try testing on k<n0 for Planck mean
*  05-may-88:  adjust xcorr with xmin
*  07-may-88:  final stop changed to continue, to avoid makefile exit
*  15-jul-88:  amax1(1.0,xxj()), for k's with too few xxj() contribs
*  19-jul-88:  amin1(alog(rhotab(1)),alog(0.5*rhomin)) when init=0
*  10-mar-89:  log-log interpolation for tmin, tmax
*  10-mar-89:  forcing tmean monotonous with 50 K instead of 200 K 
*  21-aug-89:  reading file1,file2 names, removed read of unit numbers
*  24-aug-89:  changed parameter mtau to mz
*  26-aug-89:  parameter minter for max intervals
*  26-aug-89:  new tmp() inst of w() (w() now twodimensional)
*  28-aug-89:  added margins on n-1 and n+1 (1000 K) 
*  28-aug-89:  changed 1.5 -> 1.0 in calculation of np
*  30-aug-89:  set pe before loop 190
*  04-nov-89:  ttmin, ttmax includes +-2 to handle evolution of rho
*  19-nov-89:  constant extrap downwards i1=1 (was 2) in alog(xcorr(T))
*  19-nov-89:  forcing tmean monotonous with 100 K instead of 50 K
*  27-nov-89:  moved mbox(ibox).gt.0 test to guard against empty last bin
*  27-nov-89:  alog(amax1(1.e-30,tab())) to guard against other empty bins
*  28-nov-89:  check rhomin rhomax over 3 levels near boundaries
*  30-nov-89:  added epsros, epspg printed tests of lookup precision
*  10-jan-90:  bug: goto 241 -> goto 242 in mbox test fixed
*  13-mar-90:  extending with npmin+2, avoids straddling few points
*  13-mar-90:  take both max and min over a range n+-2 for ttmax ttmin
*  22-mar-90:  lowered temperature limit from 2200 to 2000 K
*  22-aug-90:  calculate exact length of table (mtabla)
*  04-apr-91:  only read mtabla words from input file when reopening
*
      include 'cparam.inc'
      include 'cmtab.inc'
      include 'cfiles.inc'
      include 'cdata.inc'
      include 'ctable.inc'
      include 'subs.inc'
*
      common /cscr/w(minter,3),tmin(mz),tmax(mz),mbox(4),pow(7)
     & ,qheat(mz,4),qbox(mz,4),qcorr(mz),xross(mz),xnorm(mz)
     & ,dbplan(mz),xxj(mz),xjnorm(mz),dxcorr(mz),dtmp(mz)
     & ,lnrhom(mz),tmp(mz),rhoml(mz),tmeanl(mz)
      common /utput/iread,iwrit
      common /statec/ppr(mtau),ppt(mtau),ppx(mtau),gg(mtau),zz(mtau)
     & ,dd(mtau),vv(mtau),ffc(mtau),ppe(mtau),tt(mtau),tauln(mtau)
     & ,ntau,iter
      common /tauc/tau(mtau),dtauln(mtau),jtau
      common /rossc/ross(mtau)
      common /ci1/dumci1(239),xiong(16,5),dm1ci1(20),elem(16)
     * ,dm2ci1(2)
      common /ci6/iqfix(16,5),nqtemp,tp
      common /ci8/pgc,rhoc,ec
      common /ci9/ai(16)
      common /cxmax/xmax
      common /cpartp/partco(mtau),partcn(mtau)
      common /cxlset/nset,nl(10),xl(20,10)
      common /cvaagl/nlb,xlb(500),wlb(500)
      common /ctran/x(mtau),s(mtau),bplan(mtau),xj(mtau),xh(mtau)
     & ,xk(mtau)
      common /cangle/mmy,xmy(6),xmy2(6),wmy(6)
*
*
      character*32 file1,file2                                           univac
*
      common /cwork/ttq(mx,my,mz),rho(mx,my,mz),rossav(mz),pgsav(mz)
      real rhotab(mz)
      equivalence (w1,ttq),(w2,rho),(rhm,rhotab)
*
      common /tg01ba/i1,in
      common /ci5/abu(16),anjon(16,5),ci51(5),part(16,5),dumi5(9)
      logical accur,acc1(4),acc2(4)
*
      data abund,htau/1.e-4,0.06/
      data clight,stefan/2.997925e10,5.675e-5/
*
      temp(k)=5040./(thmin(n)+(k-1.)*dth(n))
*
* load model state
      print 50
50    format('0main/newtab': '20 jan 81 at 23:29:07')
      iwrit=6
      iread=9
      call cpu
      call initjn(0)
      call cpu
      call initab(0)
      call cpu
      iread=5
*
* mg, si, fe detailed partition functions
      iqfix(8,1)=2
      iqfix(8,2)=2
      iqfix(10,1)=2
      iqfix(10,2)=2
      iqfix(15,1)=2
      iqfix(15,2)=2
*
* units
      read (iread,*) file1,file2
      read (iread,*) init,iptab
      lun1=21
      lun2=22
      if (init.ne.1) call openr(lun2,file2,ncfile,mw)
      call openr(lun1,file1,ncfile,mw)
      if (init.eq.1) call openw(lun2,file2,ncfile,mw)
      ns0=(idata-1)/incr+1
      ns0=max0(ns0,1)
      print *,'ns0=',ns0
      ndata=mdata
* *************************************
      call input(lun1,data,mdata,idata)
      call input(lun1,rho,mw,ilnrho)
      call exps (rho,rho)
      call input(lun1,ttq,mw,itt)
      if (nx.gt.mx) stop 'nx greater than mx'
      if (ny.gt.my) stop 'ny greater than my'
      if (nz.ne.mz) stop 'nz not equal to mz'
      mtabla=12+12*mz
      if (init.ne.1) call input(lun2,table,mtabla,itable)
* *************************************
      print *,'ndata,mtable=',ndata,ntable
      ip=iptab
* *************************************
      if (init.eq.1) iupdte=1
      print 105,iupdte,nst1,t
105   format('0iupdte,nst1,t =',2i4,f9.2)
*
      ns1=1
      ns2=1
      if (ip.le.3) print *,ttq(1,1,1),ttq(nx,ny,nz)
*
*  Check min and max density
*
      rhomin=rho(1,1,1)
      rhomax=rho(1,1,nz)
      do 104 n=1,3
	do 104 lm=1,mp
	  rhomin=amin1(rhomin,rho(lm,1,n))
	  rhomax=amax1(rhomax,rho(lm,1,nz-n+1))
104   continue
      if (init.eq.0) then
        rhm1=amin1(alog(rhotab(1)),alog(rhomin*0.05))
        rhm2=amax1(alog(rhotab(nz)),alog(rhomax*2.))
      else
        rhm1=alog(rhomin*0.05)
        rhm2=alog(rhomax*2.)
      endif
      drhm=(rhm2-rhm1)/(nz-1)
      if (ip.le.6) print *,'rhm1,rhm2=',rhm1,rhm2
*
*  Initial thmin and thmax.  Interpolate the mean model to the tabel rho.
*
      do 108 n=1,nz
        tmean(n)=0.
        do 109 m=1,ny
        do 109 l=1,nx
          tmean(n)=tmean(n)+ttq(l,m,n)
109     continue
        tmean(n)=tmean(n)/(nx*ny)
        tmeanl(n)=alog(tmean(n))
        rhoml(n)=alog(rhom(n))
        tt(n)=tmean(n)
108   continue
*
*  Do log-log interpolation since the bends may be sharp
*
      call tb04a (nz,rhoml,tmeanl,dtmp,w)
      i1=1
      in=1
      do 102 n=1,nz
        rhotbl=rhm1+(n-1)*drhm
        rhotab(n)=exp(rhotbl)
        tttab=exp(tg01b(-1,nz,rhoml,tmeanl,dtmp,rhotbl))
        tmin(n)=tttab
        tmax(n)=tttab
        if (init.gt.0) then
          thmin(n)=5040./tmax(n)
          thmax(n)=5040./tmin(n)
        endif
102   continue
*
* depth loop
      npmin=nz
      do 101 n=1,nz
      do 101 m=1,ny
      do 101 l=1,nx
*
* pressure index, adjust min and max temperatures
        alogr=alog(rho(l,m,n))
        np=1.0+(alogr-rhm1)/drhm
        np=max0(1,min0(nz,np))
        npmin=min0(np,npmin)
        tmax(np)=amax1(tmax(np),ttq(l,m,n))
        tmin(np)=amin1(tmin(np),ttq(l,m,n))
101   continue
      do 106 np=1,npmin-1
        tmax(np)=tmax(npmin+2)
        tmin(np)=tmin(npmin+2)
106   continue
*
* depth loop, add margins, compare with previous
      do 100 n=1,nz
        tamp(n)=tmax(n)-tmin(n)
	ttmax1=tmax(n)
	ttmin1=tmin(n)
	do 103 k=-2,2
	  n0=max0(1,min0(nz,n+k))
          ttmax1=amax1(ttmax1,tmax(n0)+1000.)
          ttmin1=amin1(ttmin1,tmin(n0)-1000.)
103     continue
        thmin(n)=amin1(5040./ttmax1,thmin(n))
        thmax(n)=amax1(5040./ttmin1,thmax(n))
        thmax(n)=amin1(5040./2000.,thmax(n))
100   continue
*
* print results
      if (ip.le.5) print 110
      if (ip.le.5) print 111,(n,z(n),rhotab(n),ppm(n),thmin(n),thmax(n)
     * ,tmean(n),tamp(n),tmin(n),tmax(n),n=1,nz)
111   format(1x,i3,f7.3,2e12.5,2f7.3,4f7.0)
110   format('1 n',5x,'z',10x,'rho',9x,'ppm',4x,'thmin',2x,'thmax'
     * ,2x,'tmean',2x,'tamp',3x,'tmin',3x,'tmax')
*
* initiate table
      ur=uur
      ul=uul
      ut=uut
      tff=teff
      grv=grav
      abnd=abund
      i=1
      read (iread,*) eps,njon,nbox
      if (nbox.gt.0) then
         read (iread,*) xmin,dbox
         do 120 j=1,nbox
         mbox(j)=0
         tab(i)=10.**(xmin+(j-1)*dbox)
120      i=i+1
      else
         read (iread,*) iel,ion
         if (iel.gt.0) then
           tab(i)=xiong(iel,ion)
           i=i+1
           tab(i)=ai(iel)
           i=i+1
           tab(i)=xl(1,1)
           i=i+1
           tab(i)=elem(iel)
           i=i+1
         else
           tab(i)=20.
           i=i+1
           tab(i)=28.
           i=i+1
           tab(i)=xl(1,1)
           i=i+1
*           tab(i)='co'
           i=i+1
         end if
      end if
      nvar=nbox+njon
      if (ip.le.6) print 122,ul,ut,ur,eps,teff,grav,abund
122   format('0ul,ut,ur,eps,teff,grav,abund=',7g12.3)
*
* z loop
      call cpu
      pe=ppm(1)*abund
      if (ip.le.4) print 181
181   format('0  n ntab    i   dth   sec')
      do 180 n=1,nz
*
* initiate table
      ntab=4
      accur=.false.
      ltab=3*ntab
      dth(n)=(thmax(n)-thmin(n))/(ntab-1.)
      do 130 k=1,ntab
      do 130 j=1,njon
      inew=i+(k-1)+(j-1)*ltab
      tempk=temp(k)
130   call phys(tempk,rhotab(n),pe,j,tab(inew),err)
*
* compute derivatives
140   do 141 j=1,njon
      inew=i+(j-1)*ltab
141   call tb04a1(ntab,tab(inew),tab(inew+ntab),w)
      if(accur.or.ntab.gt.(minter+1)/2) goto 170
      accur=.true.
*
* increase resolution
      ntab1=2*ntab-1
      ltab1=3*ntab1
      if(i+nvar*ltab1.le.mtaba) goto 151
      print 152
152   format('0too many points in table')
      stop
151   dth(n)=(thmax(n)-thmin(n))/(ntab1-1.)
*
* point to last elements in derivative tables
      inew=i+njon*ltab1-ntab1-1
      iold=i+njon*ltab-ntab-1
*
* loop over tables
      do 153 j=1,njon
*
* loop over table derivative elements
      do 150 k=1,ntab
      tab(inew)=tab(iold)
      iold=iold-1
150   inew=inew-2
*
* adjust to point at last elements of value tables
      inew=inew+1
*
* loop over table value elements
      do 155 k=1,ntab
      tab(inew)=tab(iold)
      iold=iold-1
155   inew=inew-2
*
* skip over ntab empty places, adjust
      inew=inew-ntab1+1
      iold=iold-ntab
153   continue
*
* calculate new function values
      do 162 j=1,njon
      acc2(j)=acc1(j)
162   acc1(j)=.true.
      do 160 k=2,ntab1,2
      if (k.eq.4) then
         pe1=pe
         tt1=temp(2)
      else if (k.ge.4) then
         pe2=pe1
         tt2=tt1
         pe1=pe
         tt1=temp(k-2)
         dpe=alog(pe1/pe2)/alog(tt1/tt2)
         pe=pe*(temp(k)/tt1)**dpe
      end if
      do 160 j=1,njon
      inew=i+(k-1)+(j-1)*ltab1
      call phys(temp(k),rhotab(n),pe,j,tab(inew),err)
      if=i+(j-1)*ltab1
      tabf=tg01b3(ntab,tab(if),tab(if+ntab1),0.5*(k+1))
      acc1(j)=acc1(j).and.abs(tab(inew)-tabf).lt.err
160   accur=accur.and.(abs(tab(inew)-tabf).lt.err)
      ntab=ntab1
      ltab=ltab1
      goto 140
*
* resolution good enough
170   ntab=(ntab+1)/2
      ltab=3*ntab
      dth(n)=(thmax(n)-thmin(n))/(ntab-1.)
      inew=i
      iold=i
      do 171 j=1,njon
      do 172 k=1,ntab
      tab(inew)=tab(iold)
      inew=inew+1
172   iold=iold+2
      iold=iold-1
      do 173 k=1,ntab
      tab(inew)=2.*tab(iold)
      inew=inew+1
173   iold=iold+2
      inew=inew+ntab
      iold=iold+ntab1-1
171   continue
*
* end z loop
      mtab(n)=ntab
      itab(n)=i
      call secnd(sec)
      if (ip.le.4) print 201,n,ntab,i,dth(n),sec,(acc2(j),j=1,njon)
201   format(1x,i3,i4,i6,f7.4,f5.1,4l4)
180   i=i+njon*3*mtab(n)+nbox*3*mtab(n)
      call rhocnt
      call phystm
      call cpu
*
* calculate pressure derivative
      pe=ppm(1)*abund
      do 190 n=1,nz
      n1=max0(n-1,1)
      n2=min0(n+1,nz)
      alogr=alog(rhotab(n2)/rhotab(n1))
      do 190 j=1,njon
      ntabn=mtab(n)
      do 190 k=1,ntabn
      do 191 m=n1,n2,(n2-n1)
      ntab=mtab(m)
      i=itab(m)+(j-1)*3*ntab
      thk=1.+(thmin(n)+(k-1)*dth(n)-thmin(m))/dth(m)
*...  tmp(m)=tg01b1(ntab,tab(i),tab(i+ntab),thk)
*  force recalculation of electron pressure .....|
      if (j.ne.1) call phys(temp(k),rhotab(m),pe,1,dum1,dum2)
      call phys (temp(k),rhotab(m),pe,j,tmp(m),err)
*  phys is better than possible exptrapolating the table
191   continue
      i=itab(n)+(3*j-1)*mtab(n)
      tab(i+k-1)=(tmp(n2)-tmp(n1))/alogr
      if (n.eq.29.and.j.eq.3.and.ip.le.3)
     &  print *,'k,e1,e2,dl,tab=',
     &  k,tmp(n1),tmp(n2),alogr,tab(i+k-1)
195   format(' n,j,k,pow=',3i3,g10.3)
190   continue
*
* dump table values, 3*njon*nz=3*4*nz=386 lines
*..      do 196 j=1,njon
*..      do 196 n=1,nz
*..      ntab=mtab(n)
*..      i=itab(n)+(j-1)*3*ntab
*..      do 196 l=1,3
*..      print 197,(tab(i+(l-1)*ntab+k-1),k=1,ntab)
*..197   format(1x,13g10.3e1)
*..196   continue
*
*  Force monotonous tmean, for xcorr interpolation.  Empirically, 200 forced
*  too low temperature sometimes, 50 produced too much variation in the grid
*  spacing in temperature, which caused misbehaved splines occasionally.
*  This should be replaced by a procedure which uses the same temperature
*  structure always, for a given stellar model.
*
      do 214 n=nz-1,1,-1
        tt(n)=amin1(tt(n),tt(n+1)-100.)
        tmean(n)=tt(n)
214   continue
*
* pseudo blanketing
*
      ntau=nz
      jtau=ntau
      up=ur*(ul/ut)**2
      ue=up*(ul/ut)
      pe=ppm(1)*abund
      if (ip.le.4) print 212
212   format('0  n    tt(n)     ppe(n)    pgc       rhoc     ross'
     & ,'     anjon      part      ')
      if (nbox.eq.0) then
        ielpr=15
      else
        ielpr=2
      endif
      do 213 n=1,nz
      call rhomke(tt(n),pe,rhom(n)*ur,pe)
      ppe(n)=pe
      ross(n)=rossop(tt(n),pe)
      if(n.eq.1) tau(1)=ross(1)*rhoc*htau*ul
      n2=min0(n+1,nz)
      n1=max0(n-1,1)
      tau(n)=tau(n)+.5*(z(n)-z(n1))*rhoc*ross(n)*ul
      tau(n2)=tau(n)+.5*(z(n2)-z(n))*rhoc*ross(n)*ul
      prd=1.333*stefan*tt(n)**4/clight
      call termon(tt(n),ppe(n),prd,pg,dpgt,dpgp,ro,drot,drop,cp,tgrad,q)
      if (ip.le.4) print 211,n,tt(n),ppe(n),pg,ro,ross(n)
     & ,anjon(ielpr,1),part(ielpr,1)
      pgsav(n)=pg/up
      rossav(n)=ross(n)*ul*ur
211   format(1x,i3,12(1pe10.3))
213   continue
*
* skip if not blanketing
      if (nbox.eq.0) go to 98
      do 210 n=1,nz
      ltab=3*mtab(n)
      i=itab(n)+njon*ltab
      ntot=nbox*3*mtab(n)
      do 210 k=1,ntot
210   tab(i+k-1)=0.
*
* lambda loop
      xmax=1.e10
      if (ip.le.3) print 224
224   format('1  j    xlb     wlb  n0  x1      xn0     s1      sn0',
     & 3x,'ibox',4x,'qheat')
      do 233 j=1,nbox
      do 233 k=1,nz
      qheat(k,j)=0.0
233   continue
      do 234 k=1,nz
      qcorr(k)=0.0
      xross(k)=0.0
      xnorm(k)=0.0
      xxj(k)=0.0
      xjnorm(k)=0.0
234   continue
      mmy=4
      call gausi(mmy,0.,1.,wmy,xmy)
      do 256 k=1,mmy
256   xmy2(k)=xmy(k)**2
      jtau=ntau
      do 220 j=1,nlb
      call opac(j,x,s)
      tauj=tau(1)*(x(1)+s(1))
      do 221 n=2,nz
      tauj=tauj+.5*(x(n)+s(n)+x(n-1)+s(n-1))*(tau(n)-tau(n-1))
      n0=n-1
      if(tauj.gt.1.) goto 222
221   continue
222   ibox=1.5+(alog10(x(n0)+s(n0))-xmin)/dbox
      ibox=min0(nbox,max0(1,ibox))
      mbox(ibox)=mbox(ibox)+1
*
* sum in boxes
      do 230 n=1,nz
      ntab=mtab(n)
      i=itab(n)+(njon*3+ibox*3-3)*ntab
      do 230 k=1,ntab
230   tab(i+k-1)=tab(i+k-1)+wlb(j)*bpl(temp(k),xlb(j))/ue
*
* test transfer, with matains extra uv opacity
      call dbplv(ntau,tt,xlb(j),bplan,dbplan)
      if (xlb(j).lt.5000.) then
        do 231 k=1,nz
        xmag=1.1e-3*(5000.-xlb(j))*10.**((5040./tt(k)-1.)*4.)
        x(k)=x(k)+xmag
231     continue
      end if
      call traneq
      do 232 k=1,nz
      qheat(k,ibox)=qheat(k,ibox)+x(k)*(xj(k)-bplan(k))*wlb(j)
      xross(k)=xross(k)+wlb(j)/(x(k)+s(k))*dbplan(k)
      xnorm(k)=xnorm(k)+wlb(j)*dbplan(k)
*---      if (ibox.eq.1) then
      if (ibox.eq.1.and.k.lt.n0) then
        xxj(k)=xxj(k)+x(k)*xj(k)*wlb(j)
        xjnorm(k)=xjnorm(k)+xj(k)*wlb(j)
      end if
232   continue
      if (ip.le.3) print 223,j,xlb(j),wlb(j),n0,x(1),x(n0),s(1),s(n0)
     & ,ibox,(qheat(1,ibx)/ue,ibx=1,nbox)
223   format(1x,i3,2f8.0,i3,4(1pe8.1e1),i4,2x,4e8.1e1)
*
* end lambda loop, take alog, calculate splines
220   continue
      call cpu
242   if (mbox(nbox).eq.0) then
        nbox=nbox-1
	go to 242
      endif
      do 240 n=1,nz
      i=itab(n)
      ntab=mtab(n)
      ltab=3*ntab
      do 240 ibox=1,nbox
        if=i+(njon*3+ibox*3-3)*ntab
        do 241 k=1,ntab
          tab(if+k-1)=alog(amax1(1.e-30,tab(if+k-1)))
241     continue
        call tb04a1(ntab,tab(if),tab(if+ntab),w)
240   continue
*
* set up and test transfer equation for the pseudo blanketing
      print *,'mbox=',mbox
      nvar=njon+nbox
      do 250 j=1,nbox
      xbox=10.**(xmin+(j-1)*dbox)
      do 251 k=1,ntau
      s(k)=0.
      x(k)=xbox
      thk=1.+(5040./tt(k)-thmin(k))/dth(k)
      ntab=mtab(k)
      i=itab(k)+(njon*3+j*3-3)*ntab
251   bplan(k)=exp(tg01b1(ntab,tab(i),tab(i+ntab),thk))
*
* contribution to radiative exchange
      call traneq
      if (ip.le.4) print 253,xbox
253   format('0 n  bplan',5x,'xj',8x,'qbox',6x,'qheat',5x,'qcorr'
     & ,5x,'xcorr',5x,'xbox=',g9.3)
      do 254 k=1,ntau
      qbox(k,j)=xbox*(xj(k)-bplan(k))
      qheat(k,j)=qheat(k,j)/ue
      if (z(k).lt.-1.3e7/ul*2.75e4*ut**2/ul/grav) then
        qcorr(k)=qcorr(k)+qheat(k,j)-qbox(k,j)
        xcorr(k)=1.+qcorr(k)/qbox(k,1)
        xcorr(k)=amax1(1.,xcorr(k))
      else
        qcorr(k)=0.0
        xcorr(k)=1.
*...    print *,'k,xcorr(k)=',k,xcorr(k)
      end if
      if (ip.le.4)
     & print 255,k,bplan(k),xj(k),qbox(k,j),
     &  qheat(k,j),qcorr(k),xcorr(k)
254   continue
255   format(1x,i3,10g10.3)
250   continue
      call cpu
*
* print co/cn info
      if (ip.le.5) print 260
260   format('0  n',4x,'partco',4x,'partcn',4x,'xross',5x,'xxjk',6x
     & ,'xcorrk',4x,'xcorr')
      do 262 k=1,ntau
      expt2=exp(-2.*tau(k))
      if (xjnorm(k).gt.0.0) then
        xxjk=amax1(1.0,xxj(k)/xjnorm(k))
      else
        xxjk=1.0
      endif
      xrossk=xnorm(k)/xross(k)
      xcorrk=expt2*xxjk+(1.-expt2)*xrossk
      if (ip.le.5) print 255,k,partco(k),partcn(k),xrossk
     &  ,xxjk,xcorrk,xcorr(k)
        xcorr(k)=xcorrk
262   continue
*
* dump table
98    continue
      if (ip.le.4) print 202
202   format('0  n',3x,'z',9x,'tt',8x,'rho',9x,'pg'
     & ,7x,'ross',3x,'ee/anjon')
      epspg=0.0
      epsros=0.0
      do 200 n=1,nz
      alogr=alog(rhom(n))
      np=1.5+(alogr-rhm1)/drhm
      np=max0(1,min0(nz,np))
      alogr=alogr-rhm1-(np-1)*drhm
      ntab=mtab(np)
      i=itab(np)-1
      thk=1.+(5040./tt(n)-thmin(np))/dth(np)
      kth=max0(1,min0(ntab,ifix(thk)))
      p=thk-kth
      q=1.-p
      qf=q+q*p*(q-p)
      pf=p-q*p*(q-p)
      qd=q*(q*p)
      pd=-p*(q*p)
      do 203 j=1,nvar
      tmp(j)=qf*tab(i+kth)+pf*tab(i+kth+1)
     & +qd*tab(i+kth+ntab)+pd*tab(i+kth+ntab+1)
         ltab=3*ntab
         pow(j)=(1.-p)*tab(i+2*ntab+kth)+p*tab(i+2*ntab+kth+1)
         tmp(j)=tmp(j)+alogr*pow(j)
203   i=i+ltab
      tmp(1)=exp(tmp(1))
      tmp(2)=exp(tmp(2))
      if (nbox.eq.0) then
         tmp(3)=exp(tmp(3))
         tmp(4)=exp(tmp(4))
      else 
         tmp(4)=0.0
      end if
      ros=tmp(2)/rhom(n)
      epsros=amax1(epsros,abs(1.-ros/rossav(n)))
      epspg=amax1(epspg,abs(1.-tmp(1)/pgsav(n)))
      if (ip.le.3) then
        print 204,n,z(n),tt(n),rhom(n),tmp(1),
     &    ros,tmp(3),(pow(j),j=1,njon)
      else if (ip.le.4) then
        print 204,n,z(n),tt(n),rhom(n),tmp(1),
     &    ros,tmp(3)
      endif
204   format(1x,i3,12g10.3) 
200   continue 
      print *,'simple lnrho interpolation: epspg,epsros =',
     & epspg,epsros
*
*  adjust absorption with xcorr
*
      if (nbox.gt.0) then
      xmin1=xmin*alog(10.)
      do 302 n=1,nz
*---    xcorr(n)=alog(xcorr(n))
        xcorr(n)=alog(xcorr(n))+xmin1
302   continue
      call tb04a(nz,tmean,xcorr,dxcorr,w)
*
*  use constant extrapolation below and above, to avoid weird extrapolations
*  when things conspire to yield a funny end point derivative 891119/aake
*
      i1=1
      in=1
      do 300 n=1,nz
        ntab=mtab(n)
        i=itab(n)+3*ntab
        if (n.eq.1.and.ip.le.5) then
          write (*,303) 'temp =',(temp(k),k=1,ntab)
          write (*,303) 'tab (bef xcorr)=',(tab(i+k-1),k=1,ntab)
303       format(1x,a/(1x,8f9.2))
        endif
        do 301 k=1,ntab
          tab(i+k-1)=tab(i+k-1)+tg01b(-1,nz,tmean,xcorr,dxcorr,temp(k))
301     continue
        if (n.eq.1.and.ip.le.5) then
          write (*,303) 'tab (aft xcorr)=',(tab(i+k-1),k=1,ntab)
          write (*,303) 'dtab (bef xcorr)=',(tab(i+ntab+k-1),k=1,ntab)
        endif
        call tb04a1(ntab,tab(i),tab(i+ntab),w)
        if (n.eq.1.and.ip.le.5) then
          write (*,303) 'dtab (aft xcorr)=',(tab(i+ntab+k-1),k=1,ntab)
        endif
300   continue
      endif
*
* write
      iupdte=iupdte+1
      call setrec(lun2,1)
      mtabla=12+12*mz+itab(mz)+nvar*3*mtab(mz)
      call output(lun2,table,mtabla,itable)
      call closef(lun2,ncfile)
*--      call closef(lun1,ncfile)
      call cpu
99    continue
*---- debug subchk
      end
************************************************************************
      subroutine phys(temp,rh,pe,j,res,err)
      include 'cparam.inc'
      include 'cmtab.inc'
*dc      external *sups
      common /ci5/abu(16),anjon(16,5),ci51(5),part(16,5),dumi5(9)
      common /ci8/pgc,rhoc,ec
      common/carc3/f1p,f3p,f4p,f5p,hnic,presmo(30)
*
      include 'ctable.inc'
      save
*
      data boltz/1.3805e-16/
*
* select variable
      if(j.le.1) goto 110
      if(j.eq.2) goto 120
      if(j.eq.3) goto 130
      if(j.ge.4) goto 140
*
* first call, gas pressure
110   up=ur*(ul/ut)**2
      ppe=pe*up
      rhou=rh*ur
      call rhomke(temp,ppe,rhou,ppe)
      pe=ppe/up
      res=alog(pgc/up)
      err=eps
      return
*
* second call, rho*ross
120   rossc=rossop(temp,ppe)
      res=alog(rossc*rhoc*ul)
      err=eps
      return
*
* third call, anj or internal energy
130   if (nbox.eq.0) then
         if (iel.gt.0) then
*..         res=alog(anjon(iel,ion)/part(iel,ion))
           res=alog(anjon(iel,ion)/((1.-anjon(iel,ion))*part(iel,ion)))
         else
           res=presmo(7)/pgc
         end if
         err=eps
         return
      else
         res=(ec+13.6*1.6e-12/1.67e-24/1.4)*(ut/ul)**2
*                ^^^^^^^^^^^^^^^^^^^^^^^^^ add hydrogen ionization energy
*                                          just to make it positive
         err=eps*pgc/rhoc*(ut/ul)**2
         return
      end if
*
* fourth call, cp
140   if (nbox.eq.0) then
        res=alog(ppe/(boltz*temp))
        err=eps
      else
        call termon(temp,ppe,pr,pgd,pgt,pgpe,ro,rot,rope,cp,tgrad,q)
        res=alog(cp*(ut/ul)**2)
        err=eps
      endif
      return
*
      entry phystm
150   format(1x,4i6)
*---- debug subchk
      end
