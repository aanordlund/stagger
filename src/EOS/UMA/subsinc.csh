#
#  Apply this csh script to the big ascii input file for the absorption
#  coefficient subroutines (subs.dat) to determine
#  the number of absorption coefficient components (nkomp),
#  and how many of those that are temperature independent (kompr).
#  The answer is 25 and 16 at present.
#  
set kompr = `grep 'NTETB= 1' $1 | wc -l`
set a = `grep KOMP= $1 | sed -e 's/=/ /'`
set nkomp = $a[2]
echo "nkomp=$nkomp kompr=$kompr" 

#set nkompa = `grep nkomp= subs.inc | sed -e 's/nkomp=\([0-9]*\)/\1/'`
#set kompra = `grep kompr= subs.inc | sed -e 's/kompr=\([0-9]*\)/\1/'`
#echo nkompa = $nkompa
#echo kompra = $kompra

#  Edit subs.inc to set mkomp and mkompr

ed $2 << EOF
/mkomp=/s/mkomp=[0-9]*,/mkomp=$nkomp,/
/mkompr=/s/mkompr=[0-9]*,/mkompr=$kompr,/
p
w
q
EOF
