       ! $Id: x86_64.f,v 1.1 2008/06/21 15:56:43 aake Exp $
       !***********************************************************************
         function elapsed()
       
       !  Return the elapsed cpu + system time since the previous call
       
         real cpu(2)
         elapsed = dtime(cpu)
         elapsed = cpu(1)+cpu(2)
         end
       !***********************************************************************
         function fdtime()
       
       !  Return the elapsed cpu + system time since the previous call
       
         real cpu(2)
         fdtime = dtime(cpu)
         end
       !***********************************************************************
         function fetime()
       
       !  Return the elapsed cpu + system time since the program start
       
         real cpu(2)
         fetime = etime(cpu)
         end
       !***********************************************************************
         subroutine fflush(lun)
         integer lun
         call flush(lun)
         end
       !***********************************************************************
         function lrec(i)
	 integer i, lrec
	 lrec = i
	 end
       !***********************************************************************
