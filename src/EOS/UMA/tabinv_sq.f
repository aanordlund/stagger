************************************************************************
      PROGRAM TABINV
*
*  Invert the roles of internal energy and temperature in the table.
*
*  In the incoming table, THETA=5040/T is the independent variable,
*  and ln Pgas, ln (rho kappa), and the internal energy E are the
*  dependent variables in tables with 3*NTAB values.  The first NTAB
*  values are the function values, the next NTAB values are the
*  derivative values, and the last NTAB values are d(f)/d(ln rho).
*
*  To make everything a function of E instead of THETA, use the relation
*
*             dE               dE
*     dE =  ------ dTHETA + --------  d ln(rho) = 0   when
*           dTHETA          d ln(rho)
*
*      dTHETA       (dE/dlnrho)
*     --------- = - -----------
*     d ln(rho)     (dE/dTHETA)
*
*  Thus
*
*       df                        df                      df
*     ------ at constant E is = ------ at const THETA - ------ * above
*     dlnrho                    dlnrho                  dTHETA
*
*  Indexing:
*  mtab=#temperature entries for this density
*  itab=#table entries preceeding 1st location for this density=sum(mtab*nvar*3)
*  Update history:
*
*  891103/aake:  one more in ee1max
*  891130/aake:  ditto, first time (not important)
*  900325/aake:  limit deemin to minimum 0.002 times largest span
*
************************************************************************
*
      include 'cparam.inc'
      include 'cmtab.inc'
      parameter (mtabl=5000)
!!      include 'cfiles.inc'
      include 'ctable.inc'
*
      common
     & x(mtabl),f(mtabl),d(mtabl),w(mtabl,3),g(mtabl),h(mtabl),
     & th(mtabl),ee(mtabl),dthdee(mtabl),deedlr(mtabl),
     & itab1(my),mtab1(my),dee(my),tab1(mtabl,my,3,7)
*
      common /copen/debug
        logical debug
      character*16 file1,file2
*
*  The internal energy data is in table no 3
*
      data je/3/
*
*  Read in the old table, open a new file for the new table
*
      read (*,*) file1,file2
      read *,ip,idn
      debug=.true.
!!      call openr(22,file1,ncfile,mw)
*
*  Zero new table
*
      do 10 j=1,mtabl
      do 10 k=1,my
      do 10 l=1,3
      do 10 m=1,nvar
        tab1(j,k,l,m)=0.
10    continue
*
*  Establish how much of table file to read 
*
      open (lun2,file=file2,form='unformatted',status='old')
      read (lun2) ny
      if (ny .ne. my) then
	print *,'ny .ne. my',ny,my
	stop
      end if
      read (lun2) mtab3
      read (lun2) (table(i),i=1,mtab3)
      read (lun2) mtab3
      read (lun2) (tab(i),i=1,mtab3)
      close (lun2)
*
*  Find the smallest dee
* 
      deemin=100.
      eespan=0.0
      eetmin=1.e3
      eetmax=0.
      do 200 nr=1,my
        ntab=mtab(nr)
        ltab=3*ntab
        ik=itab(nr)+(je-1)*ltab
        eemax1=tab(ik)
        eemin1=tab(ik+ntab-1)
        eespan=amax1(eespan,eemax1-eemin1)
        deenr=(eemax1-eemin1)/(ntab-1) 
        deemin=amin1(deemin,deenr)
        eetmin=amin1(eetmin,eemin1)
        eetmax=amax1(eetmax,eemax1)
200   continue 
*
*  Limit to max 500 points per density.  This is more than sufficient,
*  and prevents a narrow interval near the surface to enforce a very
*  small deemin.  900325/aake
*
      deemin=amax1(deemin,eespan*0.002)
      if (ip.le.5) print *,'deemin=',deemin
*
      if (ip.le.5)
     & print *,' n  ttmin  ttmax  eemin1 eemax1 deenr  ee1min ee1max'
      do 201 nr=1,my
        ntab=mtab(nr)
        ltab=3*ntab
        ik=itab(nr)+(je-1)*ltab
        eemax1=tab(ik)
        eemin1=tab(ik+ntab-1)
        deenr=(eemax1-eemin1)/(ntab-1) 
        ee1min=int(eemin1/deemin+1.)*deemin
        ee1max=int(eemax1/deemin)*deemin
        ntab1=(ee1max-ee1min)/deemin+1.5
        if (ntab1.gt.mtabl) then
          print *, 'tabinv: mtabl too small'
          call exit(2)
        endif
        mtab1(nr)=ntab1
        if (ip.le.6) write (*,'(i3,2f7.0,5f7.3,2i4)')
     &    nr,5040./thmax(nr),5040./thmin(nr),
     &    eemin1,eemax1,deenr,ee1min,ee1max,ntab,ntab1
201   continue 
      eetmin=int(eetmin/deemin+1.)*deemin
      eetmax=int(eetmax/deemin)*deemin
      ntab1=(eetmax-eetmin)/deemin+1.5
      if (ip.le.5) print *,'eetmax, eetmin, deemin, ntab1 = ',
     &  eetmax,eetmin,deemin,ntab1
      if (ntab1.ge.mtabl) then
	print *,'table overflow, ntab1, eetmax, eetmin, deemin = ',
     &  ntab1,eetmax,eetmin,deemin
        call exit(3)
      endif
*
*  Loop over density index
*   
c     ik1=itab(1)
      dlnr=alog(rhm(my)/rhm(1))/(my-1)
      do 100 nr=1,my
        if (ip.le.3) print 105,nr,rhm(nr)
105     format('0nr,rhm =',i3,g12.5)
*
*  Table lengths and pointers. ik is the index of the start of the table.
*
        ntab=mtab(nr)
        ltab=3*ntab
        ik=itab(nr)+(je-1)*ltab
*
*  Pick up the internal energy table in inverse form:  x is the energy,
*  f is theta=5040./temp, d is dfdx, g is df/dlnrh
*
        if (ip.le.3.and.nr.eq.idn)
     &    print *,'1: k,ee,th,dth/dee,dee/dlnr|th'
        do 110 k=1,ntab
          ee(k)=tab(ik+ntab-k)
          d(k)=dth(nr)/tab(ik+ntab-k+ntab)
          f(k)=thmax(nr)-(k-1)*dth(nr)
          g(k)=tab(ik+ntab-k+ntab*2)
          if (ip.le.3.and.nr.eq.idn)
     &      print 111,k,ee(k),f(k),d(k),g(k)
111       format(1x,i3,10g12.4)
110     continue
        call tb04a(ntab,ee,g,h,w)
*
*  Interpolate to equal values of x (new indep var = internal energy)
*
        if (ip.le.2.and.nr.eq.idn)
     &    print *,'2: k,eek,ff,dd,dd*dee,gg'
        ee1min=int(ee(1)/deemin+1.)*deemin
        ee1max=int(ee(ntab)/deemin)*deemin
        ntab2=(ee1max-ee1min)/deemin+1.5
        mtab1(nr)=ntab2
        dee(nr)=deemin
        do 120 k=1,ntab2
          eek=ee1min+(k-1)*dee(nr)
          call inter2(ntab,ee,f,d,eek,ff,dd)
          call inter2(ntab,ee,g,h,eek,gg,hh)
          th(k)=ff
          dthdee(k)=dd
          deedlr(k)=gg
          if (ip.le.2.and.nr.eq.idn)
     &       print 111,k,eek,ff,dd,gg
120     continue
        eemin(nr)=ee1min
        eemax(nr)=ee1max
	ke1=int((eemin(nr)-eetmin)/deemin)
        if (ip.le.2.and.nr.eq.idn)
     &     print *,'nr,ke1,eemin,eemax=',nr,ke1,ee1min,ee1max
*
*  Now interpolate the other variables to the same grid
*
        do 130 j=1,nvar
          ik=itab(nr)+(j-1)*ltab
          if (ip.le.1) print *,'j,ik=',j,ik
          do 131 k=1,ntab
            x(k)=thmin(nr)+(k-1)*dth(nr)
            f(k)=tab(ik+k-1)
            d(k)=tab(ik+k-1+ntab)/dth(nr)
            g(k)=tab(ik+k-1+ntab*2)
            if (ip.le.5.and.nr.eq.idn)
     &        print 111,k,x(k),ee(ntab-k+1),f(k),d(k),g(k)
131       continue
          call tb04a(ntab,x,g,h,w)
          if (ip.le.0.and.j.eq.je)
     &      print *,'4: k,th,eek,tab,dtab,dfdlnr,gg'
          do 132 k=1,ntab2
            xx=th(k)
            eek=eemin(nr)+(k-1)*dee(nr)
            call inter2(ntab,x,f,d,xx,ff,dd)
            call inter2(ntab,x,g,h,xx,gg,hh)
            tab1(k+ke1,nr,1,j)=ff
            tab1(k+ke1,nr,2,j)=dd*dthdee(k)*dee(nr)
            tab1(k+ke1,nr,3,j)=dlnr*(gg-dd*deedlr(k)*dthdee(k))
            if (j.eq.je) then
              ff=5040./th(k)
              dd=-1./th(k)
              gg=0.0
              tab1(k+ke1,nr,1,j)=alog(ff)
              tab1(k+ke1,nr,2,j)=dd*dthdee(k)*dee(nr)
              tab1(k+ke1,nr,3,j)=dlnr*(gg-dd*deedlr(k)*dthdee(k))
	      if (k.ge.ntab1-1) print *,k,th(k),ff
              if (ip.le.0.and.nr.eq.idn.and.j.eq.je.and.k.eq.1)
     &          print *,'debug: dd,deedlr,dthdee='
     &          ,dd,deedlr(k),dthdee(k)
            endif
            if (ip.le.0.and.(nr.eq.idn.or.j.eq.je))
     &         print 111,k,th(k),eek,tab1(k+ke1,nr,1,j),
     &         tab1(k+ke1,nr,2,j),tab1(k+ke1,nr,3,j),gg
132       continue
130     continue
*
*  Redefine indep variable limits
*
        thmin(nr)=eemin(nr)
        thmax(nr)=eemax(nr)
        dth(nr)=dee(nr)
        deetab(nr)=dee(nr)
*
*  End of density loop
*
100   continue
*
      open (24,file=file2,form='unformatted',status='unknown')
      print *,'table params: ',ntab1,my,3,nvar,eetmin,alog(rhm(1)),
     &  1./deemin,1./dlnr
      write (24) ntab1,my,3,nvar,eetmin,alog(rhm(1)),1./deemin,1./dlnr
      write (24) 
     &  (((tab1(i,j,k,1),i=1,ntab1),j=1,my),k=1,3),
     &  (((tab1(i,j,k,3),i=1,ntab1),j=1,my),k=1,3),
     &  (((tab1(i,j,k,2),i=1,ntab1),j=1,my),k=1,3),
     & ((((tab1(i,j,k,l),i=1,ntab1),j=1,my),k=1,3),l=4,nvar)
      close (24)
      end

*******************************************************************
      subroutine inter2(n,x,f,d,xx,ff,dd)
*
*  Interpolate the function and first derivative of a cubic spline
*
*******************************************************************
*
      real x(*),f(*),d(*)
*
*  Find index, assume increasing x
*
      do 100 k=2,n
        if (xx.lt.x(k)) goto 101
100   continue
      k=n
101   continue
*
*  Interpolate
*
      dx=x(k)-x(k-1)
      p=(xx-x(k-1))/dx
      q=1.-p
      qp=q*p
      qmp=q-p
      pf=p-qp*qmp
      pd=-p*qp
      qd=q*qp
      pdf=1.-qmp*qmp+2.*qp
      pdd=p*(-qmp-q)
      qdd=q*(qmp-p)
      ff=f(k-1)+pf*(f(k)-f(k-1))+qd*d(k-1)*dx+pd*d(k)*dx
      dd=pdf*(f(k)-f(k-1))/dx+qdd*d(k-1)+pdd*d(k)
*
      end
