************************************************************************
      subroutine tb04a(n,x,f,d,w) 
* 
*  Find the derivatives at the knots (x) of a cubic spline with
*  continuous second order derivatives, passsing through the function
*  values (f).  The boundary conditions on the spline are continuous
*  third derivatives at k=2 and k=2n-1.
*
*  The routine needs a work array w(n,3).
*
*  This version of tb04a is compatible with rku*harwell.tb04a, but
*  for unknown reasons is about 30 % faster.  770815/aake
*
************************************************************************
c
      dimension x(n),f(n),d(n),w(3,n)
c
c first point
      if (n.eq.1) then
        d(1)=0.0
        return
      endif
      cxb=1./(x(2)-x(1))
      cxc=1./(x(3)-x(2))
      dfb=f(2)-f(1)
      dfc=f(3)-f(2)
      w(1,1)=cxb*cxb
      w(3,1)=-cxc*cxc
      w(2,1)=w(1,1)+w(3,1)
      d(1)=2.*(dfb*cxb*cxb*cxb-dfc*cxc*cxc*cxc)
c
c interior points
      n1=n-1
      do 100 k=2,n1
      cxa=1./(x(k)-x(k-1))
      cxb=1./(x(k+1)-x(k))
      dfa=f(k)-f(k-1)
      dfb=f(k+1)-f(k)
      w(1,k)=cxa
      w(3,k)=cxb
      w(2,k)=2.*(cxa+cxb)
      d(k)=3.*(dfb*cxb*cxb+dfa*cxa*cxa)
100   continue
c
c last point
      w(1,n)=cxa*cxa
      w(3,n)=-cxb*cxb
      w(2,n)=w(1,n)+w(3,n)
      d(n)=2.*(dfa*cxa*cxa*cxa-dfb*cxb*cxb*cxb)
c
c eliminate at first point
      c=-w(3,1)/w(3,2)
      w(1,1)=w(1,1)+c*w(1,2)
      w(2,1)=w(2,1)+c*w(2,2)
      d(1)=d(1)+c*d(2)
      w(3,1)=w(2,1)
      w(2,1)=w(1,1)
c
c eliminate at last point
      c=-w(1,n)/w(1,n-1)
      w(2,n)=w(2,n)+c*w(2,n-1)
      w(3,n)=w(3,n)+c*w(3,n-1)
      d(n)=d(n)+c*d(n-1)
      w(1,n)=w(2,n)
      w(2,n)=w(3,n)
c
c eliminate subdiagonal
      do 110 k=2,n
      c=-w(1,k)/w(2,k-1)
      w(2,k)=w(2,k)+c*w(3,k-1)
      d(k)=d(k)+c*d(k-1)
110   continue
c
c backsubstitute
      d(n)=d(n)/w(2,n)
      do 120 kk=2,n
      k=(n+1)-kk
      d(k)=(d(k)-w(3,k)*d(k+1))/w(2,k)
120   continue
c
      return
      end
