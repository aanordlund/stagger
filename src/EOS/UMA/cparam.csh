#
#  Find the number of words before the dimensioned variables

set n = `sed  -e '1,/common/d' -e '/(mz)/,$d' -e 's/  *$//' -e 's/^.* //' $1 | tr ',' '\012' | wc -w`

# increment n by 2; there are two character*8 variables

@ n += 2
echo mpar=$n

#  Find the number of (mz) dimensioned variables

set m = `grep '(mz)' $1| tr ',' '\012'| sed -e '/^ *$/d'| wc -l`
echo mav=$m
set lm = `grep '(mp)' $1| tr ')' '\012'| sed -e '/^ *$/d'| wc -l`
echo mxy=$lm

ed $2 << EOF
/mpar=/s/mpar=[0-9]*,/mpar=$n,/
/mav=/s/mav=[0-9]*,/mav=$m,/
/mxy=/s/mxy=[0-9]*,/mxy=$lm,/
p
w
q
EOF
