*
*  cfiles
*
*  common block with logical record pointers to stored variables.
*
      common /cfile5/ncfil5,ndata5,ntabl5,nw5,idata5,itabl5,incr5,
     &  ilnrh5,iu5(3),iee5,ibb5(3),itt5,ipp5
      common /cfile3/ncfil3,ndata3,ntabl3,nw3,idata3,itabl3,incr3,
     &  ilnrh3,iu3(3),iee3,ibb3(3),itt3
      common /cfiles/ncfile,ndata,ntable,nw,idata,itable,incr,
     &  ilnrho,iu(3),iee,ibb(3),itt,
     &  idlrho,idu(3),idee,idbb(3),
     &  ilrold,iuold(3),ieeold,ibbold(3),
     &  idlrld,iduold(3),ideold,idbold(3),
     &  ijj(3),iffl(3),ijoule,
     &  itu(3),ituold(3),
     &  iab,irho,ib(4),idiss,ilnpp,ipp,idivu
      integer ivar(mvar+1),ivrold(mvar),idvar(mvar),idvold(mvar)
      equivalence (ivar,ilnrho),(ivrold,ilrold),
     &  (idvar,idlrho),(idvold,idlrld)
      integer ivar3(mvar+1)
      equivalence (ivar3,ilnrh3)
      integer ivar5(mvar+1)
      equivalence (ivar5,ilnrh5)
      common /clun/lun1,lun2,lun3,lun4,lun5
