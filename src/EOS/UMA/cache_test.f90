  implicit none
  integer, parameter:: mj=4
  integer, parameter:: mjon=3,mbin=9,mv=mjon+2*mbin,mr=500,me=100

  integer:: m(mj)=(/33,63,100,127/)

  real, dimension(2,2,mv,mr,me):: tab
  real, dimension(2,mv,mr,me,2):: tab1
  real, dimension(mv,mr,me,2,2):: tab2

  real, allocatable, dimension(:,:,:,:):: res0,res1,res2,res3

  real wc(12), wallclock, ran1, pr, qr, pe, qe
  integer ir,ie,ix,iy,iz,ii,iv,seed,i,n,mx,my,mz,j
  character(len=100) txt(12)
!.......................................................................

  print*,'cache test results, with number of table entries =',mv

  do j=1,mj
    mx=m(j)
    my=m(j)
    mz=m(j)

    allocate(res0(mx,my,mz,3))
    allocate(res1(3,mx,my,mz))
    allocate(res2(mv,mx,my,mz))
    allocate(res3(mx,my,mz,mv))

    seed = 13
    tab = 1.
    res0 = 0.
    res1 = 0.
    res2 = 0.
    res3 = 0.
    pr = .1
    pe = .1
    qr = 1.-pr
    qe = 1.-pe

    print*,' '
    print*,'dimensions =',m(j)
    print*,' musec musec/var'

    i = 1
    txt(i) = ' two at a time, with table corners far apart'
    wc(i) = wallclock()
    do ii=1,mv-1,2
    do iz=1,mz
    do iy=1,my
    do ix=1,mx
      ir = mod(iz*3+iy*17+ix*12345,mr)+1
      ie = mod(iz*3+iy*17+ix*54321,me)+1
      do iv=1,2
	res0(ix,iy,iz,iv) = &
	  qr*(qe*tab2(iv-1+ii,ir,ie,1,1)+pe*tab2(iv-1+ii,ir,ie,2,1)) &
	+ pr*(qe*tab2(iv-1+ii,ir,ie,1,2)+pe*tab2(iv-1+ii,ir,ie,2,2))
      end do
    end do
    end do
    end do
    end do

    i=i+1; wc(2) = wallclock()
    txt(i) = ' two at a time, with two table corners close'
    do ii=1,mv-1,2
    do iz=1,mz
    do iy=1,my
    do ix=1,mx
      ir = mod(iz*3+iy*17+ix*12345,mr)+1
      ie = mod(iz*3+iy*17+ix*54321,me)+1
      do iv=1,2
	res0(ix,iy,iz,iv) = &
	  qr*(qe*tab1(1,iv-1+ii,ir,ie,1)+pe*tab1(2,iv-1+ii,ir,ie,1)) &
	+ pr*(qe*tab1(1,iv-1+ii,ir,ie,2)+pe*tab1(2,iv-1+ii,ir,ie,2))
      end do
    end do
    end do
    end do
    end do

    i=i+1; wc(i) = wallclock()
    txt(i) = ' two at a time, with table corners close'
    do ii=1,mv-1,2
    do iz=1,mz
    do iy=1,my
    do ix=1,mx
      ir = mod(iz*3+iy*17+ix*12345,mr)+1
      ie = mod(iz*3+iy*17+ix*54321,me)+1
      do iv=1,2
	res0(ix,iy,iz,iv) = &
	  qr*(qe*tab(1,1,iv-1+ii,ir,ie)+pe*tab(2,1,iv-1+ii,ir,ie)) &
	+ pr*(qe*tab(1,2,iv-1+ii,ir,ie)+pe*tab(2,2,iv-1+ii,ir,ie))
      end do
    end do
    end do
    end do
    end do

    i=i+1; wc(i) = wallclock()
    txt(i) = ' one at a time, with table corners close'
    do ii=1,mv-1,2
    do iz=1,mz
    do iy=1,my
    do ix=1,mx
      ir = mod(iz*3+iy*17+ix*12345,mr)+1
      ie = mod(iz*3+iy*17+ix*54321,me)+1
      !ir = min(mr,max(1,1.+ran1(seed)*(mr-1.)))
      !ie = min(me,max(1,1.+ran1(seed)*(me-1.)))
      do iv=1,2
	res1(iv,ix,iy,iz) = &
	  qr*(qe*tab(1,1,iv-1+ii,ir,ie)+pe*tab(2,1,iv-1+ii,ir,ie)) &
	+ pr*(qe*tab(1,2,iv-1+ii,ir,ie)+pe*tab(2,2,iv-1+ii,ir,ie))
      end do
    end do
    end do
    end do
    end do

    i=i+1; wc(i) = wallclock()
    txt(i) = ' two at a time, with table corners close and results close'
    do iv=1,mv
    do iz=1,mz
    do iy=1,my
    do ix=1,mx
      ir = mod(iz*3+iy*17+ix*12345,mr)+1
      ie = mod(iz*3+iy*17+ix*54321,me)+1
      !ir = min(mr,max(1,1.+ran1(seed)*(mr-1.)))
      !ie = min(me,max(1,1.+ran1(seed)*(me-1.)))
	res2(iv,ix,iy,iz) = &
	  qr*(qe*tab(1,1,iv,ir,ie)+pe*tab(2,1,iv,ir,ie)) &
	+ pr*(qe*tab(1,2,iv,ir,ie)+pe*tab(2,2,iv,ir,ie))
    end do
    end do
    end do
    end do

    i=i+1; wc(i) = wallclock()
    txt(i) = ' all at once, with results far apart'
    do iz=1,mz
    do iy=1,my
    do ix=1,mx
      ir = mod(iz*3+iy*17+ix*12345,mr)+1
      ie = mod(iz*3+iy*17+ix*54321,me)+1
      do iv=1,mv
	res3(ix,iy,iz,iv) = &
	  qr*(qe*tab(1,1,iv,ir,ie)+pe*tab(2,1,iv,ir,ie)) &
	+ pr*(qe*tab(1,2,iv,ir,ie)+pe*tab(2,2,iv,ir,ie))
      end do
    end do
    end do
    end do

    i=i+1; wc(i) = wallclock()
    txt(i) = ' all at once, with results close'
    do iz=1,mz
    do iy=1,my
    do ix=1,mx
      ir = mod(iz*3+iy*17+ix*12345,mr)+1
      ie = mod(iz*3+iy*17+ix*54321,me)+1
      do iv=1,mv
	res2(iv,ix,iy,iz) = &
	  qr*(qe*tab(1,1,iv,ir,ie)+pe*tab(2,1,iv,ir,ie)) &
	+ pr*(qe*tab(1,2,iv,ir,ie)+pe*tab(2,2,iv,ir,ie))
      end do
    end do
    end do
    end do

    i=i+1; wc(i) = wallclock()
    txt(i) = ' all at once, with results close, table corners far apart'
    do iz=1,mz
    do iy=1,my
    do ix=1,mx
      ir = mod(iz*3+iy*17+ix*12345,mr)+1
      ie = mod(iz*3+iy*17+ix*54321,me)+1
      do iv=1,mv
	res2(iv,ix,iy,iz) = &
	  qr*(qe*tab2(iv,ir,ie,1,1)+pe*tab2(iv,ir,ie,2,1)) &
	+ pr*(qe*tab2(iv,ir,ie,1,2)+pe*tab2(iv,ir,ie,2,2))
      end do
    end do
    end do
    end do

    n=i
    i=i+1; wc(i) = wallclock()
    
    do i=1,n
      wc(i) = (wc(i+1)-wc(i))*1e6/(mx*my*mz)
      print 1,wc(i),wc(i)/mv,trim(txt(i))
    end do

    deallocate (res0,res1,res2,res3)
  end do

1 format(1x,2f6.3,2x,a)
END 
! $Id: cache_test.f90,v 1.3 2009/08/07 20:35:56 aake Exp $
!***********************************************************************
FUNCTION ran1(idum)
  INTEGER idum,IA,IM,IQ,IR,NTAB,NDIV
  REAL ran1,AM,EPS,RNMX
  PARAMETER (IA=16807,IM=2147483647,AM=1./IM,IQ=127773,IR=2836, &
  NTAB=32,NDIV=1+(IM-1)/NTAB,EPS=1.2e-7,RNMX=1.-EPS)
  INTEGER j,k,iv(NTAB),iy
  SAVE iv,iy
  DATA iv /NTAB*0/, iy /0/
!
  if (idum.le.0.or.iy.eq.0) then
    idum=max(-idum,1)
    do j=NTAB+8,1,-1
      k=idum/IQ
      idum=IA*(idum-k*IQ)-IR*k
      if (idum.lt.0) idum=idum+IM
      if (j.le.NTAB) iv(j)=idum
    end do
    iy=iv(1)
  endif
  k=idum/IQ
  idum=IA*(idum-k*IQ)-IR*k
  if (idum.lt.0) idum=idum+IM
  j=1+iy/NDIV
  iy=iv(j)
  iv(j)=idum
  ran1=min(AM*iy,RNMX)
  return
END
!***********************************************************************
REAL FUNCTION wallclock()
  implicit none
  integer, save:: count, count_rate=0, count_max
  real, save:: previous=0., offset=0.

  if (count_rate == 0) then
    call system_clock(count=count, count_rate=count_rate, count_max=count_max)
    offset = -count/real(count_rate)
  else
    call system_clock(count=count)
  end if
  wallclock = count/real(count_rate) + offset
  if (wallclock < previous) then
    offset = offset + real(count_max)/real(count_rate)
    wallclock = count/real(count_rate) + offset
  end if
  previous = wallclock
END
