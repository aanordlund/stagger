*
*  subs.inc:  Include files for the equation of state subroutines
*             in subs.f 
*
*  mtau  :  the maximum number of depth points. The subs.csh
*           script makes sure mtau >= mz.
*  mkomp :  the maximum number of absorbtion coeffient components
*           in subs.dat
*  mkompr:  the maximu number of temperature independent compenents
*
      parameter (mtau=1024,mkomp=25,mkompr=16,
     &  ifadim=(mkomp-mkompr)*mtau,kfadim=ifadim*3+mkompr*mtau)
