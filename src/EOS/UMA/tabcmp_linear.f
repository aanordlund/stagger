! $Id: tabcmp_linear.f,v 1.1 2014/12/29 02:10:08 aake Exp $ 
************************************************************************
      program tabprg
*
*  Update history:
*
*  10-mar-85:  ntable=mtable, to make sure xcorr written
*  10-mar-85:  xcorr(k)=xcorrk
*  13-nov-87:  take alog of source functions
*  13-nov-87:  rhm1=alog(rhom(1)) (was alog(0.1*rhom(1))
*  02-dec-87:  rhm1=alog(0.5*rhomin)
*  11-jan-88:  force monotonous tmean, for interpolation of xcorr
*  18-jan-88:  incorporate xcorr() into output absorption coefficient
*  19-jan-88:  new envelop construction
*  18-feb-88:  m[xy] -> n[xy] in code; allows n[xy] < m[xy]
*  18-feb-88:  testing nz.eq.mz; must be equal
*  03-mar-88:  pow(7) (was 5)
*  18-apr-88:  xcorr stuff bracketed by if (nbox.gt.0) then
*  05-may-88:  try testing on k<n0 for Planck mean
*  05-may-88:  adjust xcorr with xmin
*  07-may-88:  final stop changed to continue, to avoid makefile exit
*  15-jul-88:  amax1(1.0,xxj()), for k's with too few xxj() contribs
*  19-jul-88:  amin1(alog(rhotab(1)),alog(0.5*rhomin)) when init=0
*  10-mar-89:  log-log interpolation for tmin, tmax
*  10-mar-89:  forcing tmean monotonous with 50 K instead of 200 K 
*  21-aug-89:  reading file1,file2 names, removed read of unit numbers
*  24-aug-89:  changed parameter mtau to mz
*  26-aug-89:  parameter minter for max intervals
*  26-aug-89:  new tmp() inst of w() (w() now twodimensional)
*  28-aug-89:  added margins on n-1 and n+1 (1000 K) 
*  28-aug-89:  changed 1.5 -> 1.0 in calculation of np
*  30-aug-89:  set pe before loop 190
*  04-nov-89:  ttmin, ttmax includes +-2 to handle evolution of rho
*  19-nov-89:  constant extrap downwards i1=1 (was 2) in alog(xcorr(T))
*  19-nov-89:  forcing tmean monotonous with 100 K instead of 50 K
*  27-nov-89:  moved mbox(ibox).gt.0 test to guard against empty last bin
*  27-nov-89:  alog(amax1(1.e-30,tab())) to guard against other empty bins
*  28-nov-89:  check rhomin rhomax over 3 levels near boundaries
*  30-nov-89:  added epsros, epspg printed tests of lookup precision
*  10-jan-90:  bug: goto 241 -> goto 242 in mbox test fixed
*  13-mar-90:  extending with npmin+2, avoids straddling few points
*  13-mar-90:  take both max and min over a range n+-2 for ttmax ttmin
*  22-mar-90:  lowered temperature limit from 2200 to 2000 K
*  22-aug-90:  calculate exact length of table (mtabla)
*  04-apr-91:  only read mtabla words from input file when reopening
*  13-jan-07/regner:reading mean stratification from file 'meanrhoT_f77.dat'
*
      include 'cparam.inc'
      include 'cmtab.inc'
!!      include 'cfiles.inc'
!!      include 'cdata.inc'
      include 'ctable.inc'
      include 'subs.inc'
*
      common /cscr/w(minter,3),tmin(my),tmax(my),mbox(4),pow(9)
     & ,qheat(my,4),qbox(my,4),qcorr(my),xross(my),xnorm(my)
     & ,dbplan(my),xxj(my),xjnorm(my),dxcorr(my),dtmp(my)
     & ,lnrhom(my),tmp(my),rhoml(my),tmeanl(my),y(my),rhom(my)
     & ,tttab(my)
      common /utput/iread,iwrit
      common /statec/ppr(mtau),ppt(mtau),ppx(mtau),gg(mtau),zz(mtau)
     & ,dd(mtau),vv(mtau),ffc(mtau),ppe(mtau),tt(mtau),tauln(mtau)
     & ,ntau,iter
      common /tauc/tau(mtau),dtauln(mtau),jtau
      common /rossc/ross(mtau)
      common /ci1/dumci1(239),xiong(16,5),dm1ci1(20),elem(16)
     * ,dm2ci1(2)
      common /ci6/iqfix(16,5),nqtemp,tp
      common /ci8/pgc,rhoc,ec
      common /ci9/ai(16)
      common /cxmax/xmax
      common /cpartp/partco(mtau),partcn(mtau)
      common /cxlset/nset,nl(10),xl(20,10)
      common /cvaagl/nlb,xlb(500),wlb(500)
      common /ctran/x(mtau),s(mtau),bplan(mtau),xj(mtau),xh(mtau)
     & ,xk(mtau)
      common /cangle/mmy,xmy(6),xmy2(6),wmy(6)
*
*
      character*32 file1,file2                                           univac
*
      common /cwork/ttq(mx,my,mz),rho(mx,my,mz),rossav(my),pgsav(my)
      real rhotab(my)
      equivalence (w1,ttq),(w2,rho),(rhm,rhotab)
      integer*4 ftell, statf(13), bytesleft, ier
*
      common /tg01ba/i1,in
      common /ci5/abu(16),anjon(16,5),ci51(5),part(16,5),dumi5(9)
      logical accur,acc1(5),acc2(5)
*
      data abund,htau/1.e-4,0.06/
      data clight,stefan/2.997925e10,5.675e-5/
*
      temp(k)=5040./(thmin(m)+(k-1.)*dth(m))
*
* load model state
      print 50
50    format(' main/newtab': '20 jan 81 at 23:29:07')
      iwrit=6
      iread=9
      call initjn(0)
      call initab(0)
      iread=5
*
* mg, si, fe detailed partition functions
      iqfix(8,1)=2
      iqfix(8,2)=2
      iqfix(10,1)=2
      iqfix(10,2)=2
      iqfix(15,1)=2
      iqfix(15,2)=2
*
* units
      read (iread,*) file1,file2
      print *,'input: ',trim(file1)
      print *,'output: ',trim(file2)
      read (iread,*) init,iptab
      print *,'init,iptab ',init,iptab
      lun1=21
      lun2=22

      open (lun1,file=file1,form='unformatted',status='old',
     & access='direct',recl=lrec(mx*my))
      print *,'dimensions:',mx,my,mz,lrec(1)
      if (mtau.lt.my) then
        print*,'ERROR: mtau =',mtau,' is less than my =',my
	stop
      end if
      print *,'reading rho'
      do iz=1,mz
        read (lun1,rec=mz*0+iz) rho(:,:,iz)
      end do
      print *,mz,rho(1,1,1),rho(1,my,1),rho(mx,1,mz),rho(mx,my,mz)
      if (init.ne.0) call extrapolate_center_log(rho)
      print *,'reading T'
      do iz=1,mz
        read (lun1,rec=mz*5+iz) ttq(:,:,iz)
      end do
      print *,mz,ttq(1,1,1),ttq(1,my,1),ttq(mx,1,mz),ttq(mx,my,mz)
      if (init.ne.0) call extrapolate_center_log(ttq)
      close (lun1)

      call read_mesh(file1,mx,my,mz,y)
      nx=mx
      ny=my
      nz=mz

      if (init.eq.0) then
        open (lun2,file=file2,form='unformatted',status='old')
        read (lun2) ny
        if (ny.ne.my) then
          print *, 'tabcmp.f: ny from file not equal to my', ny, my
        end if
        read (lun2) mtab3
        read (lun2) (table(i),i=1,mtab3)
        read (lun2) mtab3
        read (lun2) (tab(i),i=1,mtab3)
        close (lun2)
      end if

* *************************************
      ip=iptab
* *************************************
      if (init.eq.1) then
        iupdte=1
	print*,'new table file '//trim(file2)
      else
	print*,'extending old table from '//trim(file2)//', revision',iupdte
      end if
*
      ns1=1
      ns2=1
      if (ip.le.3) print *,ttq(1,1,1),ttq(nx,ny,nz)
*
*  Check min and max density
*
      rhomin=rho(1,1,1)
      rhomax=rho(1,ny,1)
      do m=1,ny
        rhom(m)=0.
        do n=1,nz
        do l=1,nx
          rhomin=amin1(rhomin,rho(l,m,n))
          rhomax=amax1(rhomax,rho(l,m,n))
          rhom(m)=rhom(m)+rho(l,m,n)/(mx*mz)
        end do
        end do
      end do
      if (init.eq.0) then
        rhm1=amin1(alog(rhotab(1)),alog(rhomin*0.001))
        rhm2=amax1(alog(rhotab(ny)),alog(rhomax*2.))
      else
        rhm1=alog(rhomin*0.001)
        rhm2=alog(rhomax*2.)
      endif
      drhm=(rhm2-rhm1)/(ny-1)
      if (ip.le.7) print *,'delta(lnrho) =',drhm
*
*  Initial thmin and thmax.  Interpolate the mean model to the table rho.
*
      do 108 m=1,ny
        tmean(m)=0.
        do 109 n=1,nz
        do 109 l=1,nx
          tmean(m)=tmean(m)+ttq(l,m,n)
109     continue
        tmean(m)=tmean(m)/(nx*nz)
        tmeanl(m)=alog(tmean(m))
        rhoml(m)=alog(rhom(m))
        tt(m)=tmean(m)
108   continue
*
*  Do log-log interpolation since the bends may be sharp
*
      call tb04a (ny,rhoml,tmeanl,dtmp,w)
      i1=1        ! constant extrapolation below
      in=2        ! linear extrapolation above
      do 102 m=1,ny
        rhotbl=rhm1+(m-1)*drhm
        rhotab(m)=exp(rhotbl)
        tttab(m)=exp(tg01b(-1,ny,rhoml,tmeanl,dtmp,rhotbl))
        tmin(m)=tttab(m)
        tmax(m)=tttab(m)
        if (init.gt.0) then
          thmin(m)=5040./tmax(m)
          thmax(m)=5040./tmin(m)
        endif
102   continue
*
* depth loop
      npmin=ny
      rhomax=0.
      rhomin=1e30
      do m=1,my
      do n=1,mz
      do l=1,mx
*
* pressure index, adjust min and max temperatures
        alogr=alog(rho(l,m,n))
	rhomin=amin1(rhomin,rho(l,m,n))
	rhomax=amax1(rhomax,rho(l,m,n))
        np=1.0+(alogr-rhm1)/drhm
        np=max0(1,min0(ny,np))
        npmin=min0(np,npmin)
        tmax(np)=amax1(tmax(np),ttq(l,m,n))
        tmin(np)=amin1(tmin(np),ttq(l,m,n))
      end do
      end do
      end do
      print*,'alog10(tabmin) =',rhm1/alog(10.)
      print*,'alog10(rhomin) =',alog10(rhomin)
      print*,'alog10(rhomax) =',alog10(rhomax)
      print*,'alog10(tabmax) =',(rhm1+(ny-1)*drhm)/alog(10.)
      do 106 np=1,npmin-1
        tmax(np)=tmax(npmin+2)
        tmin(np)=tmin(npmin+2)
106   continue
*
* depth loop, add margins, compare with previous
      ttmarg=6000.                                                      ! margin is 6000 K
      do 100 m=1,ny
        tamp(m)=tmax(m)-tmin(m)
        ttmin1=tmin(m)
        ttmax1=tmax(m)
        do 103 k=-10,10
          n0=max0(1,min0(ny,m+k))
          ttmax1=max(ttmax1,tmax(n0)-tttab(n0)+tttab(m))
          ttmin1=min(ttmin1,tmin(n0)-tttab(n0)+tttab(m))
103     continue
        ttmin1=max(ttmin1-ttmarg,1600.)
        ttmax1=ttmax1+ttmarg
        ttmax1=max(ttmax1,1e4+ttmarg)                                   ! is at least 1e4 plus the common margin
        thmin(m)=min(5040./ttmax1,thmin(m))
        thmax(m)=max(5040./ttmin1,thmax(m))
        thmax(m)=min(5040./1600.,thmax(m))
100   continue
*
* print results
      if (ip.le.6) print 110
      if (ip.le.6) print 111,(m,y(m),rhotab(m)
     * ,5040./thmax(m),5040./thmin(m)
     * ,tttab(m),tamp(m),tmin(m),tmax(m),m=1,ny)
111   format(1x,i3,f8.3,1pe12.4,0p6f9.0)
110   format('1 m',6x,'y',10x,'rho',4x,'ttmin',4x,'ttmax'
     * ,4x,'tttab',5x,'tamp',5x,'tmin',5x,'tmax')
*
* initiate table
      uur=1e-7
      uul=1e8
      uut=1e2
      ur=uur
      ul=uul
      ut=uut
      teff=5800.
      grav=2.75
      abund=1e-4
      tff=teff
      grv=grav
      print *,'teff,grav',teff,grav
      abnd=abund
      i=1
      read (iread,*) eps,njon,nbox
      if (nbox.gt.0) then
         read (iread,*) xmin,dbox
         do 120 j=1,nbox
         mbox(j)=0
         tab(i)=10.**(xmin+(j-1)*dbox)
120      i=i+1
      else
         read (iread,*) iel,ion
         if (iel.gt.0) then
           tab(i)=xiong(iel,ion)
           i=i+1
           tab(i)=ai(iel)
           i=i+1
           tab(i)=xl(1,1)
           i=i+1
           tab(i)=elem(iel)
           i=i+1
         else
           tab(i)=20.
           i=i+1
           tab(i)=28.
           i=i+1
           tab(i)=xl(1,1)
           i=i+1
*           tab(i)='co'
           i=i+1
         end if
      end if
      nvar=nbox+njon
      if (ip.le.4) print 122,ul,ut,ur,eps,teff,grav,abund
122   format(' ul,ut,ur,eps,teff,grav,abund=',7g12.3)
*
* y loop
      pe=1e-4
      if (ip.le.4) print 181
181   format('   m  ntab        i    dth   sec')
      do 180 m=1,ny
*
* initiate table
      ntab=4
      accur=.false.
      ltab=3*ntab
      dth(m)=(thmax(m)-thmin(m))/(ntab-1.)
      do k=1,ntab
      do j=1,njon
        inew=i+(k-1)+(j-1)*ltab
        tempk=temp(k)
        call phys(tempk,rhotab(m),pe,j,tab(inew),err)
      end do
      end do
*
* compute temperature derivatives
140   do 141 j=1,njon
      inew=i+(j-1)*ltab
141   call tb04a1(ntab,tab(inew),tab(inew+ntab),w)
      if(accur) goto 170
      if(ntab.ge.minter) goto 182
      accur=.true.
*
* increase resolution
      ntab1=2*ntab-1
      ltab1=3*ntab1
      if(i+nvar*ltab1.le.mtaba) goto 151
      print 152
152   format(' too many points in table')
      stop
151   dth(m)=(thmax(m)-thmin(m))/(ntab1-1.)
*
* point to last elements in derivative tables
      inew=i+njon*ltab1-ntab1-1
      iold=i+njon*ltab-ntab-1
*
* loop over tables
      do 153 j=1,njon
*
* loop over table derivative elements
      do 150 k=1,ntab
      tab(inew)=tab(iold)
      iold=iold-1
150   inew=inew-2
*
* adjust to point at last elements of value tables
      inew=inew+1
*
* loop over table value elements
      do 155 k=1,ntab
      tab(inew)=tab(iold)
      iold=iold-1
155   inew=inew-2
*
* skip over ntab empty places, adjust
      inew=inew-ntab1+1
      iold=iold-ntab
153   continue
*
* calculate new function values
      do 162 j=1,njon
      acc2(j)=acc1(j)
162   acc1(j)=.true.
      do k=2,ntab1,2
*
* starting guess for new electron pressures
        if (k.eq.4) then
           pe1=pe
           tt1=temp(2)
        else if (k.ge.4) then
           pe2=pe1
           tt2=tt1
           pe1=pe
           tt1=temp(k-2)
           dpe=alog(pe1/pe2)/alog(tt1/tt2)
           pe=pe*(temp(k)/tt1)**dpe
        end if
        do j=1,njon
          inew=i+(k-1)+(j-1)*ltab1
          call phys(temp(k),rhotab(m),pe,j,tab(inew),err)
          if=i+(j-1)*ltab1
          tabf=tg01b4(ntab,tab(if),tab(if+ntab1),0.5*(k+1))
          acc1(j)=acc1(j).and.abs(tab(inew)-tabf).lt.err
          accur=accur.and.(abs(tab(inew)-tabf).lt.err)
        enddo
      enddo
      ntab=ntab1
      ltab=ltab1
      goto 140
*
* resolution good enough
170   ntab=(ntab+1)/2
      ltab=3*ntab
      dth(m)=(thmax(m)-thmin(m))/(ntab-1.)
      inew=i
      iold=i
      do 171 j=1,njon
      do 172 k=1,ntab
      tab(inew)=tab(iold)
      inew=inew+1
172   iold=iold+2
      iold=iold-1
      do 173 k=1,ntab
      tab(inew)=2.*tab(iold)
      inew=inew+1
173   iold=iold+2
      inew=inew+ntab
      iold=iold+ntab1-1
171   continue
*
* end y loop
182   mtab(m)=ntab
      itab(m)=i
      sec=fdtime()
      if (ip.le.4) print 201,m,ntab,i,dth(m),sec,(acc2(j),j=1,njon)
201   format(1x,i4,i6,i9,f7.4,f6.2,5l4)
180   i=i+njon*3*mtab(m)+nbox*3*mtab(m)
      call rhocnt
      call phystm
*
* calculate density derivative
      pe=1e-4
      do 190 m=1,ny
        m1=max0(m-1,1)
        m2=min0(m+1,ny)
        alogr=alog(rhotab(m2)/rhotab(m1))
        do 190 j=1,njon
          ntabn=mtab(m)
          do 190 k=1,ntabn
          do 191 mm=m1,m2,(m2-m1)
            ntab=mtab(mm)
            i=itab(mm)+(j-1)*3*ntab
            thk=1.+(thmin(mm)+(k-1)*dth(mm)-thmin(mm))/dth(mm)
*...        tmp(mm)=tg01b1(ntab,tab(i),tab(i+ntab),thk)
*  force recalculation of electron pressure .....|
        if (j.ne.1) call phys(temp(k),rhotab(mm),pe,1,dum1,dum2)
            call phys (temp(k),rhotab(mm),pe,j,tmp(mm),err)
*  phys is better than possible exptrapolating the table
191       continue
          i=itab(m)+(3*j-1)*mtab(m)
          tab(i+k-1)=(tmp(m2)-tmp(m1))/alogr
          if (m.eq.29.and.j.eq.3.and.ip.le.3)
     &      print *,'k,e1,e2,dl,tab=',
     &      k,tmp(m1),tmp(m2),alogr,tab(i+k-1)
190   continue
*..195   format(' m,j,k,pow=',3i3,g10.3)
*
* dump table values, 3*njon*ny=3*4*ny=386 lines
*..      do 196 j=1,njon
*..      do 196 m=1,ny
*..      ntab=mtab(m)
*..      i=itab(m)+(j-1)*3*ntab
*..      do 196 l=1,3
*..      print 197,(tab(i+(l-1)*ntab+k-1),k=1,ntab)
*..197   format(1x,13g10.3e1)
*..196   continue
*
* skip if not blanketing
      if (nbox.eq.0) go to 98
*
*  Open and read the horizontally and temporally averaged simulation
      open (37,file="meanrhoT_f77.dat",iostat=ier,
     &                             status='old',form='unformatted')
      if (ier.le.0) then
        print *,'  *** Reading meanrhoT_f77.dat ***'
        read (37) rhom, tmean
        ierr = fstat(37, statf)
        bytesleft = statf(8) - ftell(37)
        if (bytesleft.ge.(my*4+8)) then
*
*  The file seems to be long enough to also contain a y-scale
          read (37) y
        else if (bytesleft.gt.0) then
          print *," WARNING: ", bytesleft,
     &       "bytes left in meanrhoT_f77.dat after reading rhom, tmean."
          print *," This indicates that meanrhoT_f77.dat assumes my="
     &       ,my+bytesleft/8
        endif
        close(37)
      else
        print *,"  *** Couldn't find meanrhoT_f77.dat ***"
        print *,"  Using mean structure from ",trim(file1)," instead."
      endif
*
*  Sanity check of the meanrhoT stratification
      Tmmin = 1e5
      rmmin = 1e5
      do n=1,ny
        Tmmin = min(Tmmin, tmean(n))
        rmmin = min(rmmin, rhom(n))
      enddo
      if (Tmmin.LT.5e2 .or. rmmin.gt.1e3 .or. rmmin.lt.0.0) then
        print *,' Something is wrong with the meanrhoT_f77.dat-file.'
        print '(" min(Tmean)=",1pe11.3," and min(rhom)=",1pe11.3)',
     &                                                   Tmmin, rmmin
        print *,' Bailing out!'
        call flush(6)
        stop
      endif
*
*  Force monotonous tmean, for xcorr interpolation.  Empirically, about 100K 
*  per scale height is a good number.
*
      tt(ny)=tmean(ny)
      pe=1.
      up=ur*(ul/ut)**2
      dthp=100.
      do 214 m=ny-1,1,-1
        call rhomke(tt(m),pe,rhom(m)*ur,pe)
        prd=1.333*stefan*tt(m)**4/clight
        call termon(tt(m),pe,prd,pg,dpgt,dpgp,ro,drot,drop,cp,tgrad,q)   
	hp=(pg/up)/(rhom(m)*grav)
        tmean(m)=amin1(tmean(m),tmean(m+1)-dthp*(y(m+1)-y(m))/hp)
        tt(m)=tmean(m)
	!print*,m,hp,y(m),tt(m),dthp*(y(m+1)-y(m))/hp
214   continue
*
* pseudo blanketing; loop 213 establishes the variables in common /statec/,
* needed in the calls to opac. The structure tt(), ppe(), .. is the mean model.
*
      ntau=ny
      jtau=ntau
      ue=up*(ul/ut)
      pe=1e-4
      if (ip.le.5) print 212
212   format('   m    tt(m)     ppe(m)    pgc       rhoc     ross'
     & ,'     anjon      part      ')
      if (nbox.eq.0) then
        ielpr=15
      else
        ielpr=2
      endif
      do 213 m=1,ny
      call rhomke(tt(m),pe,rhom(m)*ur,pe)
      ppe(m)=pe
      ross(m)=rossop(tt(m),pe)
      if(m.eq.1) tau(1)=ross(1)*rhoc*htau*ul
      m2=min0(m+1,ny)
      m1=max0(m-1,1)
      tau(m)=tau(m)+.5*(y(m)-y(m1))*rhoc*ross(m)*ul
      tau(m2)=tau(m)+.5*(y(m2)-y(m))*rhoc*ross(m)*ul
      prd=1.333*stefan*tt(m)**4/clight
      call termon(tt(m),ppe(m),prd,pg,dpgt,dpgp,ro,drot,drop,cp,tgrad,q)   
      if (ip.le.5) print 211,m,tt(m),ppe(m),pg,ro,ross(m)
     & ,anjon(ielpr,1),part(ielpr,1)
      pgsav(m)=pg/up
      rossav(m)=ross(m)*ul*ur
211   format(1x,i3,12(1pe10.3))
213   continue
*
*  Loop over height/optical depth
      do 210 m=1,ny
      ltab=3*mtab(m)
      i=itab(m)+njon*ltab
      ntot=nbox*3*mtab(m)
      do 210 k=1,ntot
210   tab(i+k-1)=0.
*
* lambda loop
      xmax=1.e10
      if (ip.le.3) print 224
224   format('1  j    xlb     wlb  n0  x1      xn0     s1      sn0',
     & 3x,'ibox',4x,'qheat')
      do 233 j=1,nbox
      do 233 k=1,ny
      qheat(k,j)=0.0
233   continue
      do 234 k=1,ny
      qcorr(k)=0.0
      xross(k)=0.0
      xnorm(k)=0.0
      xxj(k)=0.0
      xjnorm(k)=0.0
234   continue
      mmy=4
      call gausi(mmy,0.,1.,wmy,xmy)
      do 256 k=1,mmy
256   xmy2(k)=xmy(k)**2
      jtau=ntau
      do 220 j=1,nlb
      call opac(j,x,s)
      tauj=tau(1)*(x(1)+s(1))
      do 221 m=2,ny
      tauj=tauj+.5*(x(m)+s(m)+x(m-1)+s(m-1))*(tau(m)-tau(m-1))
      n0=m-1
      if(tauj.gt.1.) goto 222
221   continue
222   ibox=1.5+(alog10(x(n0)+s(n0))-xmin)/dbox
      ibox=min0(nbox,max0(1,ibox))
      mbox(ibox)=mbox(ibox)+1
*
* sum in boxes
      do m=1,ny
       ntab=mtab(m)
       i=itab(m)+(njon*3+ibox*3-3)*ntab
       if (init.eq.2) then
        do k=1,ntab
          tab(i+k-1)=tab(i+k-1)+wlb(j)*bpl(temp(k),xl(1,1))/ue
        end do
       else
        do k=1,ntab
          tab(i+k-1)=tab(i+k-1)+wlb(j)*bpl(temp(k),xlb(j))/ue
        end do
       end if
      end do
*
* test transfer, with matains extra uv opacity
      call dbplv(ntau,tt,xlb(j),bplan,dbplan)
      if (xlb(j).lt.5000.) then
        do 231 k=1,ny
        xmag=1.1e-3*(5000.-xlb(j))*10.**((5040./tt(k)-1.)*4.)
        x(k)=x(k)+xmag
231     continue
      end if
      call traneq
      do 232 k=1,ny
      qheat(k,ibox)=qheat(k,ibox)+x(k)*(xj(k)-bplan(k))*wlb(j)
      xross(k)=xross(k)+wlb(j)/(x(k)+s(k))*dbplan(k)
      xnorm(k)=xnorm(k)+wlb(j)*dbplan(k)
*---      if (ibox.eq.1) then
      if (ibox.eq.1.and.k.lt.n0) then
        xxj(k)=xxj(k)+x(k)*xj(k)*wlb(j)
        xjnorm(k)=xjnorm(k)+xj(k)*wlb(j)
      end if
232   continue
      if (ip.le.3) print 223,j,xlb(j),wlb(j),n0,x(1),x(n0),s(1),s(n0)
     & ,ibox,(qheat(1,ibx)/ue,ibx=1,nbox)
223   format(1x,i3,2f8.0,i3,4(1pe8.1e1),i4,2x,4e8.1e1)
*
* end lambda loop, take alog, calculate splines
220   continue
242   if (mbox(nbox).eq.0) then
        nbox=nbox-1
        go to 242
      endif
      do 240 m=1,ny
      i=itab(m)
      ntab=mtab(m)
      ltab=3*ntab
      do 240 ibox=1,nbox
        if=i+(njon*3+ibox*3-3)*ntab
        do 241 k=1,ntab
          tab(if+k-1)=amax1(1.e-30,tab(if+k-1))
241     continue
        call tb04a1(ntab,tab(if),tab(if+ntab),w)
240   continue
*
* set up and test transfer equation for the pseudo blanketing
      print *,'mbox=',mbox
      nvar=njon+nbox
      do 250 j=1,nbox
      xbox=10.**(xmin+(j-1)*dbox)
      do 251 k=1,ntau
      s(k)=0.
      x(k)=xbox
      np=1.5+(alog(rhom(k))-rhm1)/drhm
      np=max0(1,min0(ny,np))
      thk=1.+(5040./tt(k)-thmin(np))/dth(np)
      ntab=mtab(np)
      i=itab(np)+(njon*3+j*3-3)*ntab
      if (i+2*ntab > mtable) then
        print*,'address overrun in loop 250',k,np,i,ntab,mtable
        stop
      end if
      bplan(k)=tg01b1(ntab,tab(i),tab(i+ntab),thk)
      print'(4i7,2(1pe12.3))',k,np,ntab,i,thk,bplan(k)
251   continue
*
* contribution to radiative exchange
      call traneq
      if (ip.le.4) print 253,xbox
253   format('  m  bplan',5x,'xj',8x,'qbox',6x,'qheat',5x,'qcorr'
     & ,5x,'xcorr',5x,'xbox=',g9.3)
      do 254 k=1,ntau
      qbox(k,j)=xbox*(xj(k)-bplan(k))
      qheat(k,j)=qheat(k,j)/ue
      if (y(k).lt.-1.3e7/ul*2.75e4*ut**2/ul/grav) then
        qcorr(k)=qcorr(k)+qheat(k,j)-qbox(k,j)
        xcorr(k)=1.+qcorr(k)/qbox(k,1)
        xcorr(k)=amax1(1.,xcorr(k))
      else
        qcorr(k)=0.0
        xcorr(k)=1.
*...    print *,'k,xcorr(k)=',k,xcorr(k)
      end if
      if (ip.le.4)
     & print 255,k,bplan(k),xj(k),qbox(k,j),
     &  qheat(k,j),qcorr(k),xcorr(k)
254   continue
255   format(1x,i3,10g10.3)
250   continue
*
* print co/cn info
      if (ip.le.5) print 260
260   format('   m',4x,'partco',4x,'partcn',4x,'xross',5x,'xxjk',6x
     & ,'xcorrk',4x,'xcorr')
      do 262 k=1,ntau
      expt2=exp(-2.*tau(k))
      if (xjnorm(k).gt.0.0) then
        xxjk=amax1(1.0,xxj(k)/xjnorm(k))
      else
        xxjk=1.0
      endif
      xrossk=xnorm(k)/xross(k)
      xcorrk=expt2*xxjk+(1.-expt2)*xrossk
      if (ip.le.5) print 255,k,partco(k),partcn(k),xrossk
     &  ,xxjk,xcorrk,xcorr(k)
        xcorr(k)=xcorrk
        if (init.eq.2) xcorr(k)=1.
262   continue
*
* dump table
98    continue
      if (ip.le.4) print 202
202   format('   m',3x,'y',9x,'tt',8x,'rho',9x,'pg'
     & ,7x,'ross',3x,'ee/anjon')
      epspg=0.0
      epsros=0.0
      do 200 m=1,ny
      alogr=alog(rhom(m))
      np=1.5+(alogr-rhm1)/drhm
      np=max0(1,min0(ny,np))
      alogr=alogr-rhm1-(np-1)*drhm
      ntab=mtab(np)
      i=itab(np)-1
      thk=1.+(5040./tt(m)-thmin(np))/dth(np)
      kth=max0(1,min0(ntab,ifix(thk)))
      p=thk-kth
      q=1.-p
      qf=q+q*p*(q-p)
      pf=p-q*p*(q-p)
      qd=q*(q*p)
      pd=-p*(q*p)
* Simple lnrho interpolation of all njon+nbox variables, - just for comparison.
      do 203 j=1,nvar
      tmp(j)=qf*tab(i+kth)+pf*tab(i+kth+1)
     & +qd*tab(i+kth+ntab)+pd*tab(i+kth+ntab+1)
         ltab=3*ntab
         pow(j)=(1.-p)*tab(i+2*ntab+kth)+p*tab(i+2*ntab+kth+1)
         tmp(j)=tmp(j)+alogr*pow(j)
203   i=i+ltab
      tmp(1)=(tmp(1))
      tmp(2)=(tmp(2))
      if (nbox.eq.0) then
         tmp(3)=(tmp(3))
         tmp(4)=(tmp(4))
      else 
         tmp(4)=0.0
      end if
      ros=tmp(2)/rhom(m)
      epsros=amax1(epsros,abs(1.-ros/rossav(m)))
      epspg=amax1(epspg,abs(1.-tmp(1)/pgsav(m)))
      if (ip.le.3) then
        print 204,m,y(m),tt(m),rhom(m),tmp(1),
     &    ros,tmp(3),(pow(j),j=1,njon)
      else if (ip.le.4) then
        print 204,m,y(m),tt(m),rhom(m),tmp(1),
     &    ros,tmp(3)
      endif
204   format(1x,i3,12g10.3) 
200   continue 
      print *,'simple lnrho interpolation: epspg,epsros =',
     & epspg,epsros
*
*  adjust absorption with xcorr
*
      if (nbox.gt.0) then
      xmin1=10.**xmin
      do 302 m=1,ny
        xcorr(m)=xcorr(m)*xmin1
302   continue
      call tb04a(ny,tmean,xcorr,dxcorr,w)
*
*  use constant extrapolation below and above, to avoid weird extrapolations
*  when things conspire to yield a funny end point derivative 891119/aake
*
      i1=1
      in=1
      do 300 m=1,ny
        ntab=mtab(m)
        i=itab(m)+3*ntab
        if (m.eq.1.and.ip.le.5) then
          write (*,303) 'temp =',(temp(k),k=1,ntab)
          write (*,303) 'tab (bef xcorr)=',(tab(i+k-1),k=1,ntab)
303       format(1x,a/(1x,8f9.2))
        endif
        do 301 k=1,ntab
          tab(i+k-1)=tab(i+k-1)*tg01b(-1,ny,tmean,xcorr,dxcorr,temp(k))
301     continue
        if (m.eq.1.and.ip.le.5) then
          write (*,303) 'tab (aft xcorr)=',(tab(i+k-1),k=1,ntab)
          write (*,303) 'dtab (bef xcorr)=',(tab(i+ntab+k-1),k=1,ntab)
        endif
        call tb04a1(ntab,tab(i),tab(i+ntab),w)
        if (m.eq.1.and.ip.le.5) then
          write (*,303) 'dtab (aft xcorr)=',(tab(i+ntab+k-1),k=1,ntab)
        endif
300   continue
      endif
*
* write
      iupdte=iupdte+1

      if (init.gt.0) then
        open (lun2,file=file2,form='unformatted',status='unknown')
      else
        open (lun2,file=file2,form='unformatted',status='old')
      end if
      write (lun2) ny
      mtab3=mtabs+mtabv
      write (lun2) mtab3
      write (lun2) (table(i),i=1,mtab3)
      mtab3=itab(ny)+nvar*3*mtab(ny)
      if (mtab3 .gt. mtable) then
          print *,'table array too small:',mtable,mtab3
        stop
        else
        print *,'size of table array:',mtab3*4e-6,' MB '
     & ,(mtab3*100./mtable),' %'
      end if
      write (lun2) mtab3
      write (lun2) (tab(i),i=1,mtab3)
      close (lun2)

99    continue
*---- debug subchk
      end
************************************************************************
      subroutine phys(temp,rh,pe,j,res,err)
      include 'cparam.inc'
      include 'cmtab.inc'
*dc      external *sups
      common /ci5/abu(16),anjon(16,5),ci51(5),part(16,5),dumi5(9)
      common /ci8/pgc,rhoc,ec
      common/carc3/f1p,f3p,f4p,f5p,hnic,presmo(30)
*
      include 'ctable.inc'
      save
*
      data boltz/1.3805e-16/
*
* select variable
      if(j.le.1) goto 110
      if(j.eq.2) goto 120
      if(j.eq.3) goto 130
      if(j.eq.4) goto 140
      if(j.eq.5) goto 150
*
* first call, gas pressure
110   up=ur*(ul/ut)**2
      ppe=pe*up
      rhou=rh*ur
      call rhomke(temp,ppe,rhou,ppe)
      pe=ppe/up
      res=pgc/up
      err=eps*res
      return
*
* second call, rho*ross
120   rossc=rossop(temp,ppe)
      res=rossc*rhoc*ul
      err=eps*res
      return
*
* third call, anj or internal energy
130   if (nbox.eq.0) then
         if (iel.gt.0) then
*..         res=alog(anjon(iel,ion)/part(iel,ion))
           res=anjon(iel,ion)/((1.-anjon(iel,ion))*part(iel,ion))
         else
           res=presmo(7)/pgc
         end if
         err=eps*res
         return
      else
         res=(ec+1.5*13.6*1.6e-12/1.67e-24/1.4)*(ut/ul)**2
*                ^^^^^^^^^^^^^^^^^^^^^^^^^ add 1.5 times hydrogen ionization energy
*                                          just to make it positive
         err=eps*pgc/rhoc*(ut/ul)**2
         return
      end if
*
* fourth call, ne/[cgs]
140   res=pe*up/(boltz*temp)
      err=eps*res
      return
*
* fifth call, cp
150   call termon(temp,ppe,pr,pgd,pgt,pgpe,ro,rot,rope,cp,tgrad,q)
      res=cp*(ut/ul)**2
      err=10.*eps*res
      return
*
      entry phystm
*---- debug subchk
      end
!========================================================================
      SUBROUTINE read_mesh (file,mx,my,mz,ym)
      implicit none
      logical exists
      integer dot, lrec, iz, mx, my, mz, mx1, my1, mz1, iy
      real dx1, dy1, dz1
      character*(*) file
      character(len=72) f
      real dxm(mx), dxdn(mx), xm(mx), xmdn(mx), dxidxup(mx), dxidxdn(mx)
      real dym(my), dydn(my), ym(my), ymdn(my), dyidyup(my), dyidydn(my)
      real dzm(mz), dzdn(mz), zm(mz), zmdn(mz), dzidzup(mz), dzidzdn(mz)

      dot = index(file,'.',back=.true.)
      f=file(1:dot)//'msh'
      inquire (file=f,exist=exists)
      if (.not.exists) then
        f='mesh.dat'
        inquire (file=f,exist=exists)
      end if
      if (exists) then
        print *,'reading mesh data: ', trim(f)
        open(2, file=f,
     &     status='old', form='unformatted')
        read (2) mx1,my1,mz1
        read (2) dxm, dxdn, xm, xmdn, dxidxup, dxidxdn
        read (2) dym, dydn, ym, ymdn, dyidyup, dyidydn
        read (2) dzm, dzdn, zm, zmdn, dzidzup, dzidzdn
        close(2)
      else
        f = file(1:dot)//'dx'
        inquire (file=f,exist=exists)
        if (exists) then
          print *,'reading mesh data: ', f
          open(2, file=file(1:dot)//'dx',
     &     status='old', form='formatted')
          read(2,*) mx1,my1,mz1
          read(2,*) dx1,dy1,dz1
          ym = dy1*(/(iy-1,iy=1,my)/)
          dym = dy1
          close(2)
        else
          print *,'tabcmp.f ERROR: no mesh file!'
          stop
        end if
      end if
      print *,'dimensions:',mx1,my1,mz1
      if (mx1.ne.mx) stop 'tabcmp.f: inconsistent mx'
      if (my1.ne.my) stop 'tabcmp.f: inconsistent my'
      if (mz1.ne.mz) stop 'tabcmp.f: inconsistent mz'

      END
!-----------------------------------------------------------------------
      SUBROUTINE extrapolate_center_log(f)
      include 'cparam.inc'
      integer lb,ub
      parameter (lb=6,ub=my-lb)
      real, dimension(mx,my,mz):: f
      integer:: iy,iz

      do iz=1,mz
        do iy=1,lb-1
          f(:,iy,iz) = exp(2.*alog(f(:,lb,iz))-alog(f(:,lb+(lb-iy),iz)))
        end do
      end do
      do iz=1,mz
        do iy=ub+1,my
          f(:,iy,iz) = exp(2.*alog(f(:,ub,iz))-alog(f(:,ub-(iy-ub),iz)))
        end do
      end do
      END
