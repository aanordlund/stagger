*
*  cdata
*
*  Common block with parameters and average values.  NOTE!  Must not
*  have any variables on the // line, to enable counting with the
*  cparam.csh script.
*
*  Update:  Keep order at least up to dato,time to avoid the need for
*  more than one version of howfar.x and scan.x.
*  10-jan-90: major reorganization
*  23-may-90: added npot. removed spare1. 
*  23-may-90: ekzm -> emag, emag -> ptm, removed rhoim
*  24-may-90: eelowr->tmass0, eeuppr->tee0, spares[1-3] -> bfm, bfke, bfe
*  11-jul-90: added rhoim, removed scrc
*  08-may-91: hydro version, mhd stripped, few spare added
*
      common /cdata/
     &  nx,ny,nz,height,width,n0,dx,dy,dz,bzone,npot,
     &  teff,grav,abund,fnom,tmass0,tee0,
     &  t,dt,tstop,dtold,fdtc,ntime,nst1,nst3,nst5,
     &  dtsave,dtscr,dtprnt,dtsmpl,dtsmth,ipack,nsmpl,
     &  ip,idl,idm,idn,ldl,ldm,ldn,ldebug,idebug,
     &  dato,time,
     &  uur,uul,uut,uub,mu0,
     &  nmu,nph,taumax,dabmax,
     &  cnu1,cnu2,cnu3,cnu4,cnu5,
     &  eetop,eebot,rhotop,rhobot,pptop,ppbot,
     &  period,nwavel,amp0,bfm,bfke,bfe,
     &  spare1,spare2,spare3,spare4,
     &  z(mz),zrad(mz),rhom(mz),uzm(mz),eem(mz),ppm(mz),
     &  ttm(mz),rhoim(mz),eim(mz),ekm(mz),ptm(mz),rho0(mz),
     &  ee0(mz),pp0(mz),ppmin(mz),ppmax(mz),ttmin(mz),ttmax(mz),
     &  qqrm(mz),rqqrm(mz),qqpdvm(mz),dissm(mz),ugrdpm(mz),
     &  fmass(mz),frad(mz),fconv(mz),fei(mz),fkin(mz),fke(mz),fpu(mz),
     &  scra(mz),scrb(mz),scrc(mz),scrd(mz),ismpl(mz),
     &  xi(mp)
      character*8 dato,time                                             vax,sun
*     integer dato(2),time(2)                                              cray
      dimension data(mdata)
      equivalence (nx,data)
*
*  Debug bitmaps
*
      parameter (
     & ilookp = 8,
     & imass  = 16,
     & imom   = 32,
     & ienrgy = 64,
     & irad   = 128,
     & ibound = 256,
     & idifcf = 512,
     & idiffs = 1024,
     & iupdat = 2048,
     & idskio = 4096)
*
