*
*  cparam (Parameters.)
*
*  Parameters.  NOTE:  on a non-UNIX system, these have to be updated
*  manually whenever cdata.inc is changed.  In particular, the following
*  should be checked:
*
*  mpar  :  the number of undimensiones variables in /cdata/
*  mav   :  the number of (mz) dimensioned variables in /cdata/
*  mxy   :  the number of (mx,my) dimensioned variables in /cdata/
*
      integer mx,my,mz,mp,mq,mw,mpar,mav,mxy,mdata,mvarx,mvar,mpoint,
     &  mcfile,mcfil3
      parameter (mx=2016,my=600,mz=2016,mp=mx*my,mq=my*mz,mw=mp*mz,
     &  mpar=76,mav=35,mxy=1,mdata=mpar+mav*mz+mxy*mp,mvarx=8,mvar=5,
     &  mpoint=8,mcfile=mpoint+4*mvarx,mcfil3=mpoint+mvarx)
