! $Id: odfs.f90,v 1.4 2009/03/31 14:43:54 aake Exp $
!*******************************************************************************
MODULE odf_mod
  integer n,kr,nl,nr,npskal,ntskal,ny,itab
  real wave,dwave,lgpemin,lgpstep
  integer, allocatable, dimension(:):: nssg,ipebeg,ipeend,istart
  real, allocatable, dimension(:):: peskal,tskal,kap,xlb
  real, allocatable, dimension(:,:):: ylin
END MODULE

!*******************************************************************************
SUBROUTINE init_odfs
  USE odf_mod
  implicit none
  real dum,x(4),w(4)
  integer idum,i,j,k
  namelist /odf/n,nl,nr,ntskal,npskal,wave,lgpemin,lgpstep
!-------------------------------------------------------------------------------
  open (20,file='odf.dat',form='unformatted',status='old')
  read (20) n,(dum,i=1,n),kr,wave,nl,(dum,i=1,nl),n,(idum,i=1,n) &
   ,nr,dwave,npskal,lgpemin,lgpstep,ntskal
  read (20) ny
  write (*,odf)
  rewind (20)

  allocate (nssg(n),tskal(ntskal),peskal(npskal),xlb(nr*nl),kap(nr*nl))
  allocate (ipebeg(ntskal),ipeend(ntskal),istart(ntskal))
  read (20) n,(dum,i=1,n),kr,wave,nl,(dum,i=1,nl),n,(nssg(i),i=1,n) &
   ,nr,dwave,npskal,peskal(1),lgpstep,ntskal,(tskal(i),i=1,ntskal) &
   ,(ipebeg(i),i=1,ntskal),(ipeend(i),i=1,ntskal)
  write (*,*) nssg
  write (*,*) tskal
  write (*,*) ipebeg
  write (*,*) ipeend

  istart(1)=1
  do i=2,ntskal
    istart(i)=istart(i-1)+ipeend(i)-ipebeg(i)+1
  end do
  write (*,*) istart

  rewind (20)
  allocate (ylin(ny,nr*nl))
  call gausi (4,0.,1.,w,x)
  do j=1,nr
   do i=1,nl
    xlb((j-1)*nl+i)=wave+dwave*x(i)
    read (20) ny,(ylin(k,(j-1)*nl+i),k=1,ny)
    print*,i,j,xlb((j-1)*nl+i)
   end do
   wave=wave+dwave
  end do
  close (20)
END

!*******************************************************************************
SUBROUTINE end_odfs
  USE odf_mod
  implicit none
!-------------------------------------------------------------------------------
  deallocate (nssg,tskal,peskal,ipebeg,ipeend,ylin,istart,kap,xlb)
END

!*******************************************************************************
SUBROUTINE odfs (temp,lgpe)
  USE odf_mod
  implicit none
  real temp,lgpe
  real pt,qt,pp,qp,qtqp,qtpp,ptqp,ptpp
  integer i,it,ip,ip1,ip2,j,nlgpe
!-------------------------------------------------------------------------------
  it=1
  do i=2,ntskal
   if (tskal(i) < temp) it=i
  end do
  pt = (temp-tskal(it))/(tskal(it+1)-tskal(it))
  qt = 1.-pt
  print*,'it,pt=',it,pt

  pp = (lgpe-lgpemin)/lgpstep
  ip = floor(pp)
  ip = min(max(ip,ipebeg(it)),ipeend(it)-1)
  pp = pp-ip
  qp = 1.-pp
  print*,'ip,pp=',ip,pp
  ip1 = istart(it  )+(ip-ipebeg(it))
  ip2 = istart(it+1)+(ip-ipebeg(it+1))
  qtqp = qt*qp
  qtpp = qt*pp
  ptqp = pt*qp
  ptpp = pt*pp
  print*,ip1,ip2
  do j=1,nr*nl
    kap(j) = 10.**(qtqp*ylin(ip1,j)+qtpp*ylin(ip1+1,j) &
                 + ptqp*ylin(ip2,j)+ptpp*ylin(ip2+1,j))
  end do
END

!*******************************************************************************
SUBROUTINE kappa_cont (t,pe,nlb,xlb,abs,scat)
  implicit none
  real t,pe
  integer nlb
  real,dimension(nlb):: xlb,abs,scat
  integer nset,nl
  real xl
  common /cxlset/nset,nl(10),xl(20,10)
  integer i1,i2,j1,j2,k,newt
  real abs1,abs2,scat1,scat2,p

  newt=1
  do k=1,nlb
    i2=2
    j2=1
    do while (xl(j2,i2) < xlb(k))
      i1=i2
      j1=j2
      if (j2 < nl(i2)) then
        j2=j2+1
      elseif (i2 < nset) then
        j2=1
    i2=i2+1
      else
        exit
      end if
    end do
    call absko(newt,1,t,pe,i1,j1,abs1,scat1)
    newt=0
    call absko(newt,1,t,pe,i2,j2,abs2,scat2)
    p=(xlb(k)-xl(j1,i1))/(xl(j2,i2)-xl(j1,i1))
    abs(k) =exp(alog( abs1)*(1.-p)+alog( abs2)*p)
    scat(k)=exp(alog(scat1)*(1.-p)+alog(scat2)*p)
  end do
END

!*******************************************************************************
PROGRAM test_odfs
  USE odf_mod
  implicit none
  real temp,lgpe,x(4),w(4)
  real, allocatable, dimension(:):: absc,scat
  integer j,k
!-------------------------------------------------------------------------------
  call init_odfs
  allocate (absc(nr*nl),scat(nr*nl))
  temp = 5800.
  lgpe = 1.1
  call init_subs
  call odfs (temp,lgpe)
  call kappa_cont (temp,10.**lgpe,nr*nl,xlb,absc,scat)
  call gausi (4,0.,1.,w,x)
  k=1
  do j=1,nr*nl
    print '(i4,f8.0,f6.3,4g14.6)',j,xlb(j),w(k),1.+kap(j)/absc(j),scat(j)/absc(j),kap(j),absc(j)
    k=mod(k,4)+1
  end do
  deallocate (absc,scat)
  call end_odfs
END PROGRAM
