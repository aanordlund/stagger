! $Id: Linux.f,v 1.1 2004/05/18 19:59:20 aake Exp $
!***********************************************************************
       function elapsed()

!  Return the elapsed cpu + system time since the previous call

       real cpu(2)

       elapsed = etime(cpu)

       end
!***********************************************************************
       function fdtime()

!  Return the elapsed cpu + system time since the previous call

       real cpu(2)
       fdtime = dtime(cpu)
       end
!***********************************************************************
       function fetime()

!  Return the elapsed cpu + system time since the program start

       real cpu(2)
       !integer omp_get_max_threads,omp_get_num_procs,omp_get_thread_num
       fetime = etime(cpu)
       !print *,omp_get_thread_num(),fetime
       end
!***********************************************************************
       subroutine fflush(lun)
       integer lun
       call flush(lun)
       end
!***********************************************************************
       subroutine oflush
      
!  flush stdout (6) and stderr (?)

       end
!***********************************************************************
       integer function lrec(m)

!  Return the RECL=LREC(M) value, for M words.

       lrec = m
       end
