*
*  Read a formatted file with CO and CN molecular giant line
*  (opacity distribution function) informattion, and write it
*  out unformatted, for the subs.f package to read in conabs().
*
c     program cno(tape5,output,tape19)  
      dimension absa(2,6)
      data nmol,ntemp/2,6/,nreg/0/  
c
      open (unit=12,file='cnoin.dat')
      rewind 12
100   read (12,101,end=900) wave,dum,ndum,(idum,(absa(imol,itemp)
     & ,itemp=1,ntemp),imol=1,nmol) 
101   format(2f15.1,i1/2(i3,6f6.2)) 
      if (wave.eq.0.0) go to 900
      nreg=nreg+1
      open (19,file='cno.dat',form='unformatted')
      write (19) wave,dum,nmol,(imol,(absa(imol,itemp)  
     & ,itemp=1,ntemp),imol=1,nmol) 
      goto 100  
900   print 901,nreg
901   format('0number of wavelengths =',i4) 
      end
