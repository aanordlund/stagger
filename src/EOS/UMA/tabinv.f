************************************************************************
      PROGRAM TABINV
*
*  Invert the roles of internal energy and temperature in the table.
*
*  In the incoming table, THETA=5040/T is the independent variable,
*  and ln Pgas, ln (rho kappa), and the internal energy E are the
*  dependent variables in tables with 3*NTAB values.  The first NTAB
*  values are the function values, the next NTAB values are the
*  derivative values, and the last NTAB values are d(f)/d(ln rho).
*
*  To make everything a function of E instead of THETA, use the relation
*
*             dE               dE
*     dE =  ------ dTHETA + --------  d ln(rho) = 0   when
*           dTHETA          d ln(rho)
*
*      dTHETA       (dE/dlnrho)
*     --------- = - -----------
*     d ln(rho)     (dE/dTHETA)
*
*  Thus
*
*       df                        df                      df
*     ------ at constant E is = ------ at const THETA - ------ * above
*     dlnrho                    dlnrho                  dTHETA
*
*  Update history:
*
*  891103/aake:  one more in ee1max
*  891130/aake:  ditto, first time (not important)
*  900325/aake:  limit deemin to minimum 0.002 times largest span
*
************************************************************************
*
!      implicit none
      include 'cparam.inc'
      include 'cmtab.inc'
      integer mtabl
      parameter (mtabl=250000)
      include 'cfiles.inc'
      include 'ctable.inc'
*
      common
     & x(mtabl),f(mtabl),d(mtabl),w(mtabl,3),g(mtabl),h(mtabl),
     & th(mtabl),ee(mtabl),dthdee(mtabl),deedlr(mtabl),
     & itab1(my),mtab1(my),dee(my),tab1(mtaba)
*
      common /copen/debug
        logical debug
      character*64 file1,file2
*
*  The internal energy data is in table no 3
*
      data je/3/
*
*  Read in the old table, open a new file for the new table
*
      read (*,*) file1,file2
      print *,'input: '//trim(file1)
      print *,'output: '//trim(file2)
      read *,ip,idn
      debug=.true.
      open (21,file=file1,form='unformatted',status='old')
*
*  Establish how much of table file to read 
*
      read (21) ny
      read (21) mtab3
      read (21) (table(i),i=1,mtab3)
      read (21) mtab3
      read (21) (tab(i),i=1,mtab3)
      close (21)
*
*  Find the smallest dee
* 
      deemin=100.
      eespan=0.0
      do 200 nr=1,my
        ntab=mtab(nr)
        ltab=3*ntab
        ik=itab(nr)+(je-1)*ltab
        eemax1=tab(ik)
        eemin1=tab(ik+ntab-1)
        eespan=amax1(eespan,eemax1-eemin1)
        deenr=(eemax1-eemin1)/(ntab-1) 
        deemin=amin1(deemin,deenr)
200   continue 
*
*  Limit to max 2000 points per density.  This is more than sufficient,
*  and prevents a narrow interval near the surface to enforce a very
*  small deemin.  
*
      if (ip.le.6) print *,'nvar,nbox=',nvar,nbox
      if (ip.le.6) print *,'deemin =',deemin
      deemin=amax1(deemin,eespan/2000.)
      if (ip.le.6) print *,'deemin (limited) =',deemin
*
      if (ip.le.5) 
     & print *,
     & '  n   ttmin    ttmax   eemin1  eemax1   deenr  ee1min'//
     & '  ee1max ntab ntab1'
      do 201 nr=1,my
        ntab=mtab(nr)
        ltab=3*ntab
        ik=itab(nr)+(je-1)*ltab
        eemax1=tab(ik)
        eemin1=tab(ik+ntab-1)
        deenr=(eemax1-eemin1)/(ntab-1) 
        ee1min=int(eemin1/deemin+1.)*deemin
        ee1max=int(eemax1/deemin)*deemin
        ntab1=(ee1max-ee1min)/deemin+1.5
        if (ntab1.gt.mtabl) then
          print *, 'tabinv: mtabl too small'
          call exit(2)
        endif
        mtab1(nr)=ntab1
        if (ip.le.5) write (*,'(i4,2f9.0,5f8.2,2i5)')
     &    nr,5040./thmax(nr),5040./thmin(nr),
     &    eemin1,eemax1,deenr,ee1min,ee1max,ntab,ntab1
201   continue 
*
*  Loop over density index
*   
      ik1=itab(1)
      dlnr=alog(rhm(my)/rhm(1))/(my-1)
      do 100 nr=1,my
*
*  Table lengths and pointers. ik is the index of the start of the table.
*
        ntab=mtab(nr)
        ltab=3*ntab
        ik=itab(nr)+(je-1)*ltab
        if (ip.le.4) print 105,nr,ik,ltab,ik1,rhm(nr)
105     format(' nr,ik1,rhm =',i4,3i8,g12.5)
*
*  Pick up the internal energy table in inverse form:  x is the energy,
*  f is theta=5040./temp, d is dfdx, g is df/dlnrh
*
        if (ip.le.3.and.nr.eq.idn)
     &    print *,'1: k,ee,th,dth/dee,dee/dlnr|th'
        do 110 k=1,ntab
          ee(k)=tab(ik+ntab-k)
          d(k)=dth(nr)/tab(ik+ntab-k+ntab)
          f(k)=thmax(nr)-(k-1)*dth(nr)
          g(k)=tab(ik+ntab-k+ntab*2)
          if (ip.le.3.and.nr.eq.idn)
     &      print 111,k,ee(k),f(k),d(k),g(k)
111       format(1x,i3,10g12.4)
110     continue
        call tb04a(ntab,ee,g,h,w)
*
*  Interpolate to equal values of x (new indep var = internal energy)
*
        if (ip.le.2.and.nr.eq.idn)
     &    print *,'2: k,eek,ff,dd,dd*dee,gg'
        ee1min=int(ee(1)/deemin+1.)*deemin
        ee1max=int(ee(ntab)/deemin)*deemin
***        ntab1=(ee1max-ee1min)/deemin+0.5
        ntab1=(ee1max-ee1min)/deemin+1.5
        mtab1(nr)=ntab1
        dee(nr)=deemin
        do 120 k=1,ntab1
          eek=ee1min+(k-1)*dee(nr)
          call inter2(ntab,ee,f,d,eek,ff,dd)
          call inter2(ntab,ee,g,h,eek,gg,hh)
          th(k)=ff
          dthdee(k)=dd
          deedlr(k)=gg
          if (ip.le.2.and.nr.eq.idn)
     &       print 111,k,eek,ff,dd,gg
120     continue
        eemin(nr)=ee1min
        eemax(nr)=ee1max
*
*  Now interpolate the other variables to the same grid
*
        itab1(nr)=ik1
        do 130 j=1,nvar
          ik=itab(nr)+(j-1)*ltab
          if (ip.le.4) print *,'j,ik,ik1,ntab1=',j,ik,ik1,ntab1
          do 131 k=1,ntab
            x(k)=thmin(nr)+(k-1)*dth(nr)
            f(k)=tab(ik+k-1)
            d(k)=tab(ik+k-1+ntab)/dth(nr)
            g(k)=tab(ik+k-1+ntab*2)
            if (ip.le.1.and.nr.eq.idn)
     &        print 111,k,x(k),ee(ntab-k+1),f(k),d(k),g(k)
131       continue
          call tb04a(ntab,x,g,h,w)
          if (ip.le.0.and.j.eq.je)
     &      print *,'4: k,th,eek,tab,dtab,dfdlnr,gg'
          do 132 k=1,ntab1
            xx=th(k)
            eek=eemin(nr)+(k-1)*dee(nr)
            call inter2(ntab,x,f,d,xx,ff,dd)
            call inter2(ntab,x,g,h,xx,gg,hh)
            tab1(ik1+k-1)=ff
            tab1(ik1+k-1+ntab1)=dd*dthdee(k)*dee(nr)
            tab1(ik1+k-1+ntab1*2)=dlnr*(gg-dd*deedlr(k)*dthdee(k))
            if (j.eq.je) then
              ff=5040./th(k)
              dd=-ff/th(k)
              gg=0.0
              tab1(ik1+k-1)=ff
              tab1(ik1+k-1+ntab1)=dd*dthdee(k)*dee(nr)
              tab1(ik1+k-1+ntab1*2)=dlnr*(gg-dd*deedlr(k)*dthdee(k))
              if (ip.le.0.and.nr.eq.idn.and.j.eq.je.and.k.eq.1)
     &          print *,'debug: dd,deedlr,dthdee='
     &          ,dd,deedlr(k),dthdee(k)
            endif
            if (ip.le.0.and.(nr.eq.idn.or.j.eq.je))
     &         print 111,k,th(k),eek,tab1(ik1+k-1),tab1(ik1+k-1+ntab1),
     &         tab1(ik1+k-1+ntab1*2),gg
132       continue
          ik1=ik1+3*ntab1
          if (ik1.ge.mtaba) then
            print *,'table overflow'
            call exit(3)
          endif
130     continue
*
*  Redefine indep variable limits
*
        thmin(nr)=eemin(nr)
        thmax(nr)=eemax(nr)
        dth(nr)=dee(nr)
        deetab(nr)=dee(nr)
*
*  End of density loop
*
100   continue
*
      call move (itab1,itab,my)
      call move (mtab1,mtab,my)
      call move (tab1,tab,ik1)

      open (22,file=file2,form='unformatted',status='unknown')
      write (22) ny,iupdte,nvar,nbox,xmin,dbox,ul,ut,ur,
     & eps,tff,grv,abnd
      write (22) tmean,tamp,rhm,xcorr,thmin,thmax,dth,
     & eemin,eemax,deetab,itab,mtab
      mtab3=ik1
      write (22) mtab3
      write (22) (tab1(i),i=1,mtab3)
      close (22)
      print'(a,f5.1,a,f5.1,a)', ' table size:',
     & mtab3*4./1024.**2,' MB,',(100.*mtab3)/mtaba,' %'

      end
*******************************************************************
      subroutine inter2(n,x,f,d,xx,ff,dd)
*
*  Interpolate the function and first derivative of a cubic spline
*
*******************************************************************
*
      real x(*),f(*),d(*)
*
*  Find index, assume increasing x
*
      do 100 k=2,n
        if (xx.lt.x(k)) goto 101
100   continue
      k=n
101   continue
*
*  Interpolate
*
      dx=x(k)-x(k-1)
      p=(xx-x(k-1))/dx
      q=1.-p
      qp=q*p
      qmp=q-p
      pf=p-qp*qmp
      pd=-p*qp
      qd=q*qp
      pdf=1.-qmp*qmp+2.*qp
      pdd=p*(-qmp-q)
      qdd=q*(qmp-p)
      ff=f(k-1)+pf*(f(k)-f(k-1))+qd*d(k-1)*dx+pd*d(k)*dx
      dd=pdf*(f(k)-f(k-1))/dx+qdd*d(k-1)+pdd*d(k)
*
      end
