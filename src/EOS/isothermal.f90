! $Id: isothermal.f90,v 1.1 2007/06/13 09:18:27 aake Exp $
!***********************************************************************
module eos
!
!  EOS for ideal gas
!
  use params

  implicit none
  real, allocatable, dimension(:,:):: pbot0, dlnpdE_r
  logical do_table
contains

!***********************************************************************
  subroutine equation_of_state (r,ee,pp,tt)
    USE params
    USE units
    implicit none
    real, dimension(mx,my,mz):: r,ee
    real, dimension(mx,my,mz), optional:: tt,pp
    real c
    integer iz

    c = csound**2
    do iz=izs,ize
      ee(:,:,iz) = c
    end do

    if (present(pp)) call pressure (r,ee,pp)
    if (present(tt)) call temperature (r,ee,tt)
  end subroutine

!***********************************************************************
  subroutine eos2(r,ee,pp,TT)
    USE params
    USE units
    implicit none
    real, dimension(mx,my), intent(in)            :: r,ee
    real, dimension(mx,my), intent(out), optional :: pp, TT
    real, dimension(mx,my)                        :: y
    real:: gam1

    if (gamma .eq. 1.) then
      gam1 = gamma
    else
      gam1 = gamma-1.
    endif
    if (present(pp)) pp = gam1*r*ee
    if (present(tt)) then
      tt = u_temp*ee
    end if

  end subroutine eos2
end module eos

!***********************************************************************
  subroutine init_eos
    USE params
    USE units
    USE eos
    character(len=mid):: id="$Id: isothermal.f90,v 1.1 2007/06/13 09:18:27 aake Exp $"
    call print_id (id)
    u_temp = 1.
    do_ionization = .false.
    call read_eos
    allocate (pbot0(mx,mz), dlnpdE_r(mx,mz))
  end subroutine

!***********************************************************************
  subroutine read_eos
    USE params
    USE units
    USE eos
    namelist /eqofst/ do_ionization
    if (stdin.ge.0) then
      rewind (stdin); read (stdin,eqofst)
    else
      read (*,eqofst)
    end if
    if (master) write (*,eqofst)
  end subroutine

!***********************************************************************
  real function eos_iterations(clear)
  USE params
  USE units
  USE eos
  integer clear
!
  eos_iterations=0.0
  end function

!***********************************************************************
  subroutine pressure (r,ee,pp)
    USE params
    USE eos
    USE units
    implicit none
    real, dimension(mx,my,mz):: r,ee
    real, dimension(mx,my,mz):: pp
    logical omp_in_parallel

    if (omp_in_parallel()) then
      call pressure_omp (r,ee,pp)
    else
      !$omp parallel
      call pressure_omp (r,ee,pp)
      !$omp end parallel
    end if
  end subroutine

!***********************************************************************
  subroutine pressure_omp (r,ee,pp)
    USE params
    USE eos
    USE units
    implicit none
    real, dimension(mx,my,mz):: r,ee
    real, dimension(mx,my,mz):: pp
    real:: gam1,c
    integer:: iz

    if (gamma .eq. 1.) then
      gam1 = gamma
    else
      gam1 = gamma-1.
    endif

    call equation_of_state (r,ee)
    do iz=izs,ize
      pp(:,:,iz) = gam1*r(:,:,iz)*ee(:,:,iz)
    end do

  end subroutine

!***********************************************************************
  subroutine temperature (r,ee,tt)
    USE params
    USE eos
    USE units
    implicit none
    real, dimension(mx,my,mz):: r,ee
    real, dimension(mx,my,mz):: tt
    logical omp_in_parallel
!
    if (omp_in_parallel()) then
      call temperature_omp (r,ee,tt)
    else
      !$omp parallel
      call temperature_omp (r,ee,tt)
      !$omp end parallel
    end if
  end subroutine

!***********************************************************************
  subroutine temperature_omp (r,ee,tt)
    USE params
    USE eos
    USE units
    implicit none
    real, dimension(mx,my,mz):: r,ee
    real, dimension(mx,my,mz):: tt
    real:: gam1,c
    integer:: iz

    if (gamma .eq. 1.) then
      gam1 = gamma
    else
      gam1 = gamma-1.
    endif

    call equation_of_state (r,ee)
    do iz=izs,ize
      tt(:,:,iz) = gam1*ee(:,:,iz)
    end do

  end subroutine

