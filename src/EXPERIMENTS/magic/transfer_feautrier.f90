!***********************************************************************
SUBROUTINE transfer (np,n1,dtau,s,q,xi,doxi)
  USE params
  implicit none
  integer np,n1
  real, dimension(mx,my,mz) :: dtau,s,q
  real, dimension(mx,mz)    :: xi
  logical doxi, omp_in_parallel

  if (omp_in_parallel()) then
    call transfer_omp (np,n1,dtau,s,q,xi,doxi)
  else
    !$omp parallel
    call transfer_omp (np,n1,dtau,s,q,xi,doxi)
    !$omp end parallel
  end if
END

!***********************************************************************
SUBROUTINE transfer_omp (np,n1,dtau,s,q,xi,doxi)
!
!  Solve the transfer equation, given optical depth and source function.
!
!  "Steins trick" is used; storing the sum of the three elements in each
!  row of the tridiagonal equation system, instead of the diagonal ele-
!  ment.  This allows accurate solutions to be otained for arbitrarily
!  small optical depths.  Without this trick, round off errors become
!  noticeable already when the smallest optical depth is less than the
!  square root of the machine precisions.
!
!  This version works with P as a variable for k <= n1, and with
!  Q = P - S for k > n1.  P is Feautriers P and S is the source function.
!  The equation solved is  Q" = P - S for k<=n1, and Q" = Q - S" for k>n1.
!  The answer returned is Q = P - S for all k.
!
!  This choice of equations produces a high precision in Q at large optical
!  depths (where P - S would have round off), while at the same time
!  retaining the precision at small optical depths (where S" becomes very
!  large and eventually would cause round off errors in the back
!  substitution).
!
!  This is a second order version.  For simulations, with rapidly
!  varying absorption coefficients and source functions, this is to be
!  preferred over spline and Hermitean versions because it is positive
!  definite, in the sense that a positive source function is guaranteed
!  to result in a positive average intensity p.  Also, the flux
!  divergence is exactly equal to q, for the conventional definition
!  of the flux.
!
!  Operation count: 5d+5m+11a = 21 flops
!
!  Timings:
!            Alliant: 0.58 * 21 / 1.25 = 9.7 Mfl @ 31*31*31
!
!  Update history:  This routine is based on transq, with updates
!
!***********************************************************************
!
  USE params
  USE cooling, only: iphot
  implicit none
!
  real, dimension(mx,my,mz) :: dtau,s,q
  real, dimension(mx,my)    :: sp1,sp2,sp3
  real, dimension(mx,mz)    :: xi
  real, dimension(mx)       :: ex
  integer :: k, l, iz, np, n1p, n1, omp_get_thread_num
  real :: a, taum, dinv
  logical doxi
!
  character(len=mid):: id='$Id: transfer_feautrier.f90,v 1.1 2015/04/16 15:33:11 aake Exp $'

  call print_id (id)
!
!  Calculate 1-exp(-tau(l,iphot,iz)) straight or by series expansion,
!  based on max tau(l,iphot,iz).
!
  do iz=izs,ize
    taum=dtau(1,iphot,iz)
    do l=1,mx
      taum=amax1(taum,dtau(l,iphot,iz))
    end do
    if (taum.gt.0.1) then
      do l=1,mx
        ex(l)=1.-exp(-dtau(l,iphot,iz))
      end do
    else
      do l=1,mx
        ex(l)=dtau(l,iphot,iz)*(1.-.5*dtau(l,iphot,iz)*(1.-.3333*dtau(l,iphot,iz)))
      end do
    endif

	
!
!  Boundary condition at k=1.
!
    do l=1,mx
      sp2(l,iphot)=dtau(l,iphot+1,iz)*(1.+.5*dtau(l,iphot+1,iz))
      sp3(l,iphot)=-1.
      q(l,iphot,iz)=s(l,iphot,iz)*dtau(l,iphot+1,iz)*(.5*dtau(l,iphot+1,iz)+ex(l))
    end do



!
!  Matrix elements for k>2, k<np: [3d+3s]
!
    do k=iphot+1,np-1
    do l=1,mx
      sp2(l,k)=1.
      dinv=2./(dtau(l,k+1,iz)+dtau(l,k,iz))
      sp1(l,k)=-dinv/dtau(l,k,iz)
      sp3(l,k)=-dinv/dtau(l,k+1,iz)	
    end do
    end do
!
!  Right hand sides, for k>2, k<n1
!
    n1p=min0(max0(n1,iphot+1),np-3)
    do k=iphot+1,n1p-1
    do l=1,mx
      q(l,k,iz)=s(l,k,iz)
    end do
    end do
!
!  RHS for k=n1p.  The equation is still in p(k), but q(k+1) is now
!  q(k+1)=p(k+1)-s(k+1), so to get p(k+1) we have to add s(k+1) to the
!  LHS, that is subtract it from the RHS.
!
    if (n1p.le.np) then
      do l=1,mx
        q(l,n1p,iz)=s(l,n1p,iz)-sp3(l,n1p)*s(l,n1p+1,iz)
      end do
    endif
!
!  RHS for k=n1p+1.  The equation is now in Q: Q" = Q - S", but q(k-1)
!  is now p(k-1)=q(k-1)+s(k-1), so we must add sp1*s(l,k-1,iz) to the RHS,
!  relative to the case below.
!
    if (n1p+1.le.np) then
      do l=1,mx
        q(l,n1p+1,iz)=sp1(l,n1p+1)*s(l,n1p+1,iz) &
                    +sp3(l,n1p+1)*(s(l,n1p+1,iz)-s(l,n1p+2,iz))
      end do
    endif
!
!  k>n1p+1,<np [2m+2a]
!
    do k=n1p+2,np-1
    do l=1,mx
      q(l,k,iz)=sp1(l,k)*(s(l,k,iz)-s(l,k-1,iz)) &
              +sp3(l,k)*(s(l,k,iz)-s(l,k+1,iz))
    end do
    end do
!
!  k=np
!
    do l=1,mx
      sp2(l,np)=1.
      q(l,np,iz)=0.0
    end do
!
!  Eliminate subdiagonal, save factors in sp1 [1d+2m+4a]
!
    do k=iphot,np-2
    do l=1,mx
      a=-sp1(l,k+1)/(sp2(l,k)-sp3(l,k))
      q(l,k+1,iz)=q(l,k+1,iz)+a*q(l,k,iz)
      sp2(l,k+1)=sp2(l,k+1)+a*sp2(l,k)
      sp2(l,k)=sp2(l,k)-sp3(l,k)
    end do
    end do
    do l=1,mx
      sp2(l,np-1)=sp2(l,np-1)-sp3(l,np-1)
    end do
!
!  Backsubstitute [1d+1m+1a]
!
    do k=np-1,iphot,-1
    do l=1,mx
      q(l,k,iz)=(q(l,k,iz)-sp3(l,k)*q(l,k+1,iz))/sp2(l,k)
    end do
    end do
!
!  Subtract S in the region where the equation is in P
!
    do k=iphot,n1p
    do l=1,mx
      q(l,k,iz)=q(l,k,iz)-s(l,k,iz)
    end do
    end do
!
!  Surface intensity.
!
    if (doxi) then
      do l=1,mx
	xi(l,iz)=2.*(1.-ex(l))*(q(l,iphot,iz)+s(l,iphot,iz))+s(l,iphot,iz)*ex(l)**2
      end do
    end if

  end do    ! iz
!
  END
