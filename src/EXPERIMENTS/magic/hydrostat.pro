  ul=1e8                     ; length unit (1 Mm)
  ut=1e2                     ; time unit (100 sec)
  ur=1e-7                    ; density (1e-7 is used in photospheric models)
  uv=ul/ut                   ; velocity (10 km/s)
  up=ur*uv^2                 ; pressure (1e3 cgs)

  g=2.75e4*ut^2/ul           ; acceleration of gravity
  kb=1.38d-16                ; Boltzmann (cgs)
  mp=1.67d-24                ; mass unit (cgs)

  open,a
  readmesh,ym=ym

  rho1=a[0]
  e1  =a[4]
  tt1 =a[5]

  i0=301                     ; reference level
  h=ym                       ; increasing downwards

  rho=haver(rho1,/yv)
  e  =haver(  e1,/yv)
  T  =haver( tt1,/yv)

  i=i0
  p  =make_array(size=size(e))
  p[i]=exp(lookup(alog(rho[i]),e[i]/rho[i]))

  gamma=5./3.
  e0=(e[i]-p[i]/(gamma-1.))/rho[i]               ; offset

  mu0=rho[i]*ur/(mp*p[i]*up/(kb*tt[i]))          ; for continuity
  mu1=0.6
  print,'mu, e0 =',mu, e0

  print,'       i            P          rho            h            T'
  print,i,P[i],rho[i],h[i],T[i]
  while (i gt 0) do begin
    i=i-1
    dh=h[i+1]-h[i]
    w=exp(-(T[i]-T[i0])/1e4) < 1                 ; approx mu
    mu=mu0*w+mu1*(1.-w)                          ; approx mu
    for k=1,5 do begin
      if k eq 1 then begin
        rho[i]=rho[i+1]                          ; initial guess
      end
      P[i]=P[i+1]-g*0.5*(rho[i+1]+rho[i])*dh     ; predict P[i]
      rho[i]=mu*mh*(P[i]*up/ur)/(kb*T[i])        ; improve rho
      ;print,i,P[i],rho[i]
    end
    e[i]=p[i]/(gamma-1.)+e0*rho[i]
    print,i,P[i],rho[i],h[i],T[i]
  end

  !p.multi=[0,2,2]
  !x.title='Mm'
  !p.psym=-1
  !y.style=3
  plot,h,rho,/ylog,yst=3,xr=[-10,5],symsize=.3
  plot,h,T,/ylog,yst=3,xr=[-10,5],symsize=.3
  plot,h,P[0:i0],/ylog,yst=3,xr=[-10,5],symsize=.3
  plot,h,e/rho,/ylog,yst=3,xr=[-10,5],symsize=.3

  for i=0,i0-1 do begin
    rho1[*,i,*]=rho[i]
    e1[*,i,*]=e[i]
  end
ENd
