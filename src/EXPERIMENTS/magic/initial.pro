pro initial, input, output, h=h, lnp=lnp, ee=ee, lnr=lnr, tt=th, debug=debug, ny=ny, my1=my1

;  This program produces the initial condition for an experiment combining
;  magnetoconvection and the corona.

; This file needs a bit of a tidy - it's the combination of several codes.


; Read in a mesh file (cell centres)

openr,lun,'mhd.mshtxt',/get_lun
mx = 0L & my = 0L & mz = 0L
readf,lun,mx & xc=fltarr(mx) & readf,lun,xc
readf,lun,my & yc=fltarr(my) & readf,lun,yc
readf,lun,mz & zc=fltarr(mz) & readf,lun,zc
free_lun,lun

; Add atmosphere grid

default, ny, 200      ; Will tidy this up at a later date

dxc = (xc[mx-1]-xc[0])/(mx-1)
dyc = yc(1)-yc(0)
yc2 = fltarr(ny)
yc2(0) = yc(0)-dyc

for i = 1, ny-1 do begin
      dyc = dyc*1.03     ; for stretching (will improve later)
      dyc = dyc < (2.*dxc)
      yc2(i) = yc2(i-1) - dyc
endfor

; Flip yc2

h = -yc2 ; for hydrostatic atmosphere later

fill = fltarr(ny)
for i = ny-1,0,-1 do begin
      fill(ny-1-i) = yc2(i)
endfor
yc2 = fill

ym = fltarr(my+ny)
ym = [yc2,yc]

; Write out a new mesh file

default, my1, ny+300

openw,lun,'mesh.txt',/get_lun
printf,lun,mx
printf,lun,xc
printf,lun,my1
printf,lun,ym[0:my1-1]
printf,lun,mz
printf,lun,zc
free_lun,lun

ymdn=ym

; Read in snapshot data

default,input,'mhd.dat'
open,a,input,dim=[mx,my,mz]

rho = a(0)
px = a(1)
py = a(2)
pz = a(3)
e = a(4)
t = a(5)
bx = a(6)
by = a(7)
bz = a(8)

; Correct t
t(*,0,*) = t(*,1,*)
print, t(mx/2,0,mz/2), t(mx/2,1,mz/2)

; Calculate potential field in the atmosphere

atmos = fltarr(mx,ny,mz)

bx = [[atmos],[bx]]
by = [[atmos],[by]]
bz = [[atmos],[bz]]

ymdn=0.5*(ym+shift(ym,1))
ymdn[0]=ymdn[1]*2.-ymdn[2]
print,'ym  :',ym[0:4]
print,'ymdn:',ymdn[0:4]

print,'potential field ...'
potential_lower,bx,by,bz,lb=ny+5,ymin=ym,ymdn=ymdn,dx=dxc,dz=dxc

print,'hydrostatic ...'
; Calculate hydrostatic equilibrium

  ul=1e8                     ; length unit (1 Mm)
  ut=1e2                     ; time unit (100 sec)
  ur=1e-7                    ; density (1e-7 is used in photospheric models)
  uv=ul/ut                   ; velocity (10 km/s)
  up=ur*uv^2                 ; pressure (1e3 cgs)

  g=2.75e4*ut^2/ul           ; acceleration of gravity
  kb=1.38d-16                ; Boltzmann (cgs)
  mp=1.67d-24                ; mass unit (cgs)
  mh=mp

rho1 = [[atmos],[rho]]
e1 = [[atmos],[e]]

y_cor = 2.2 ; start of the corona
y_chr = 1.4 ; start of the TR

lgT=alog10(aver(t[*,1,*]))
dlgTdh_tr=2./(y_cor-y_chr)
dlgTdh_chr=(alog10(1e4)-lgT)/y_chr

i0=ny-1
i=i0
t1=fltarr(ny)
while (abs(ym[i]-ym[i0]) lt y_chr) do begin
  t1[i]=10.^(lgT)
  lgT=lgT+abs(ym[i]-ym[i-1])*dlgTdh_chr
  i=i-1
end
while (abs(ym[i]-ym[i0]) lt y_cor) do begin
  t1[i]=10.^(lgT)
  lgT=lgT+abs(ym[i]-ym[i-1])*dlgTdh_tr
  i=i-1
end
while (i ge 0) do begin
  t1[i]=1e6
  i=i-1
end

for i=0,ny-1 do begin
      atmos(*,i,*) = t1(i)
endfor

T_unit=2./3.*(ul/ut)^2*1.433*1.67e-24/1.38e-16
print,2./3.,(ul/ut)^2,1.433,1.67e-24,1.38e-16
print,T_unit

tt1 = [[atmos],[t]]

i0=ny                     ; reference level
;  readmesh,ym=ym
  h=ym                       ; increasing downwards

  rhoh=haver(rho1,/yv)
  eh  =haver(  e1,/yv)
  Th  =haver( tt1,/yv)

  eos_table
  i=i0
  p  =make_array(size=size(eh))
  p[i:*]=exp(lookup(alog(rhoh[i:*]),eh[i:*]/rhoh[i:*]))

  print,'       i            h            P          rho            T        geff'
  print,i,P[i],rhoh[i],h[i],Th[i]
  while (i gt 0) do begin
  ;while (i gt i0-1) do begin
    i=i-1
    dh=h[i+1]-h[i]
    for k=1,5 do begin
      if k eq 1 then begin
        rhoh[i]=rhoh[i+1]                          ; initial guess
        P[i]=P[i+1]
      end
      dlnPdh=0.5*g*(rhoh[i]/P[i]+rhoh[i+1]/P[i+1])
      P[i]=P[i+1]*exp(-dh*dlnPdh)
      eeh=eh[i+1]/rhoh[i+1]+(Th[i]-Th[i+1])/T_unit
      lnr=alog(rhoh[i])
      tpmake,Th[i],alog(P[i]),lnr,eeh              ; iterate rho, ee
      rhoh[i]=exp(lnr)
      eh[i]=rhoh[i]*eeh
    end
    ;print,i,h[i],P[i],rhoh[i],Th[i],(P[i+1]-P[i])/(dh*g*0.5*(rhoh[i]+rhoh[i+1]))
  end
  if keyword_set(debug) then stop

  !p.multi=[0,2,2]
  !x.title='Mm'
  !p.psym=-1
  !y.style=3
  plot,h,rhoh,/ylog,yst=3,xst=3,symsize=.3,title='density'
  plot,h,Th,/ylog,yst=3,xst=3,symsize=.3,title='temperature'
  ;plot,h,P[0:i0],/ylog,yst=3,xst=3,symsize=.3,title='density'
  plot,h,P,/ylog,yst=3,xst=3,symsize=.3,title='pressure'
  plot,h,eh/rhoh,/ylog,yst=3,xst=3,symsize=.3,title='e/rho'
  !p.multi=0
  !x.title=''
  !p.psym=0
  if keyword_set(debug) then stop

  for i=0,i0-1 do begin
    rho1[*,i,*]=rhoh[i]
    e1[*,i,*]=eh[i]
  endfor
  ee=eh/rhoh
  lnr=alog(rhoh)
  lnp=alog(P)
  if keyword_set(debug) then stop

  ; Write out initial condition of combined corona and convection zone

; Momemnta in the intial atmosphere

; Correct py (will depend on data file)

; need to tidy

py(*,4,*) = py(*,5,*)*0.5
py(*,3,*) = py(*,4,*)*0.5
py(*,2,*) = py(*,3,*)*0.5
py(*,1,*) = py(*,2,*)*0.5
py(*,0,*) = py(*,1,*)*0.5

px1 = fltarr(mx,ny,mz)
py1 = fltarr(mx,ny,mz)
pz1 = fltarr(mx,ny,mz)

for i = 0, mx-1 do begin
for j = ny-1, 0, -1 do begin
for k = 0, mz-1 do begin
      px1(i,j,k) = px(i,0,k)*exp(-(ny-1-j)/10.0)*rhoh[j]/rhoh[ny]
      py1(i,j,k) = py(i,0,k)*exp(-(ny-1-j)/10.0)*rhoh[j]/rhoh[ny]
      pz1(i,j,k) = pz(i,0,k)*exp(-(ny-1-j)/10.0)*rhoh[j]/rhoh[ny]
end
end
end

print, py1(mx/2,ny-1,mz/2), py(mx/2,0,mz/2)

px = [[px1],[px]]
py = [[py1],[py]]
pz = [[pz1],[pz]]

print,'SET THESE IN THE INPUT FILE:'
print,'rbot =',most(rho1[*,my1-1,*])
print,'ebot =',most(  e1[*,my1-1,*])

default, output, 'snapshot_00000.dat'
openw,lun,output,/get_lun

b = assoc(lun,fltarr(mx,my1,mz))
b(0) = rho1[*,0:my1-1,*]
b(1) = px[*,0:my1-1,*]
b(2) = py[*,0:my1-1,*]
b(3) = pz[*,0:my1-1,*]
b(4) = e1[*,0:my1-1,*]
b(5) = tt1[*,0:my1-1,*]
b(6) = bx[*,0:my1-1,*]
b(7) = by[*,0:my1-1,*]
b(8) = bz[*,0:my1-1,*]
free_lun,lun

if keyword_set(debug) then stop

end
