! $Id: corona_cooling.f90,v 1.1 2015/04/16 15:33:11 aake Exp $
!***********************************************************************
SUBROUTINE mactag_coolit(r,ee,dEdt)
  USE params
  USE units
  USE cooling
  USE arrays, only: eeav, rhoav
  USE variables, only: drdt, px, py, pz, e, dpxdt, dpydt, dpzdt
  implicit none
  real, dimension(mx,my,mz):: r,ee,dEdt
  real, allocatable, dimension(:,:,:):: scr, drain
  real:: rcool, ccool
  integer:: ix, iy, iz, iw
  logical, save:: first_time=.true.

  if (.not. do_cool) return
  allocate (scr(mx,my,mz), drain(mx,my,mz))
  scr=0.

!-----------------------------------------------------------------------
!  Cooling of the chromosphere -- insignificant in the corona.  At the
!  corona base density, which is about six orders of magnitude below
!  the photospheric density, the cooling time should have increased 
!  to about 1000 seconds, so by 1e4.  The exponent thus needs to be
!  about 1+4./6.=1.67 (should be 1+4./8. if we go down to rho=3 (e-7).
!-----------------------------------------------------------------------

  if (do_chromos) then
    call haverage_subr (r , rhoav)
    rcool = rhoav(iphot)
    ccool = 1./t_chromos                				! chromospheric equlibration time
!$omp parallel do private(iy,iz)
    do iz=1,mz
    do iy=ichrom,iphot-1
      scr(:,iy,iz) = - (rhoav(iy)/rcool)**1.67 &
                     *r(:,iy,iz)*(ee(:,iy,iz)-ee_chromos)*ccool
      dEdt(:,iy,iz) = dEdt(:,iy,iz) + scr(:,iy,iz)
    end do
    end do
    call dumpn(scr,'Qchromos','corona.dmp',1)
  end if

!-----------------------------------------------------------------------
!  Corona cooling and heat conduction, after initial wind-up
!-----------------------------------------------------------------------
  if (do_corona) then
    call corona_cooling (r,ee,dEdt,scr)
    call haverage_subr (ee, eeav)
    eetop= eeav(lb)

!-----------------------------------------------------------------------
!  Keep horizontal temperature fluctuations small during wind-up
!-----------------------------------------------------------------------
  else if (do_damp) then
    scr = ee
    call smooth (scr, 2)
    call haverage_subr (scr, eeav)
!$omp parallel do private(ix,iy,iz)
    do iz=1,mz
    do iy=lb,ichrom-1
      ccool = (1./t_damp)*min(ichrom-iy,5)/5.
      scr(:,iy,iz) = -r(:,iy,iz)*(ee(:,iy,iz)-eeav(iy))*ccool
      dEdt(:,iy,iz) = dEdt(:,iy,iz) + scr(:,iy,iz)
    end do
    end do
    call dumpn(scr,'Qdamp','corona.dmp',1)
  end if

!-----------------------------------------------------------------------
!  Temperature fluctuation damping at the top boundary
!-----------------------------------------------------------------------
  iw=6
  if (do_top) then
    if (.not. (do_corona.or.do_damp)) then
      scr = ee
      call smooth (scr, 2)
      call haverage_subr (scr, eeav)
    endif
    drain(:,:,:) = 0.
    iy = lb
    do while ((h_top-ym(iy)) > 0.)
      ccool = min(1.0, (h_top-ym(iy))/d_top)
      ccool = 0.5*(1.-cos(pi*ccool))
      ccool = (1./t_top) * ccool
      do iz=1,mz
      do ix=1,mx
        scr(ix,iy,iz) = - r(ix,iy,iz)*(ee(ix,iy,iz)-ee_top)*ccool
        dedt(ix,iy,iz) = dedt(ix,iy,iz) + scr(ix,iy,iz)
	drain(ix,iy,iz) = (1.-r_top/r(ix,iy,iz))*ccool
        drdt(ix,iy,iz) = drdt(ix,iy,iz) - drain(ix,iy,iz)*r(ix,iy,iz)
        dedt(ix,iy,iz) = dedt(ix,iy,iz) - drain(ix,iy,iz)*e(ix,iy,iz)
      end do
      end do
      iy = iy+1
    end do
    call dumpn(scr,'Qtop','corona.dmp',1)
    call xdn1_set (drain, scr)
    dpxdt(:,:,:) = dpxdt(:,:,:) - scr(:,:,:)*px(:,:,:)
    call ydn1_set (drain, scr)
    dpydt(:,:,:) = dpydt(:,:,:) - scr(:,:,:)*py(:,:,:)
    call zdn1_set (drain, scr)
    dpzdt(:,:,:) = dpzdt(:,:,:) - scr(:,:,:)*pz(:,:,:)
  end if
  first_time=.false.
  deallocate(scr, drain)

END SUBROUTINE

!***********************************************************************
SUBROUTINE corona_cooling (r,ee,dEdt,scr)
  USE params
  USE cooling
  USE units
  implicit none
  real, dimension(mx,my,mz):: r,ee,dEdt,scr
  real, allocatable, dimension(:,:,:):: tt
  real:: u_cool, Tquench, quench1, quench2, quench, power, scale
  real:: climit, enhance
  integer:: ix, iy, iz
  character*64:: id= &
   "$Id: corona_cooling.f90,v 1.1 2015/04/16 15:33:11 aake Exp $"

!-----------------------------------------------------------------------
!  Corona cooling, from Schrijver & Zwaan 1999:
!
!  Q = 1.5e-18 T^{-2/3} n_e n_H erg cm^{-3} s^{-1}
!
!  Assume n_e = n_H = rho/(m_atom) = rho/(2e-24)  [Kahn, 1976]
!
!  Q [sim units] = 1.5e-18 (e/rho uTemp)^{-2/3} ((rho urho)/mp)^2 ut/ue
!                = 1.5e-18 ut/ue (urho/mp)^2 
!                  * Temp^{-2/3} rho^2
!-----------------------------------------------------------------------

  scale = c_scale*u_t/u_ee*u_r						! really u_t/u_e*u_r^2
  u_cool = 1.5d-18/2d-24**2			             		! Schrijver
  power = -2./3.

!  u_cool = 1.33e-19*(u_t/u_e)*(u_r/2d-24)**2            		! Kahn 1976
!  power = -0.5

  Tquench = 4e4								! Kill the cooling below 40,000 K

  if (verbose>0 .and. master) print*, 'starting corona cooling'
  allocate (tt(mx,my,mz))

  if (verbose==1 .and. master) print*, 'call temperature'
  call temperature (r, ee, tt)
  if (verbose==1 .and. master) print*, 'done temperature'

  if (do_casiana) then
    call coolit_casiana (tt, scr)
  else
    do iz=1,mz
    do iy=lb,iphot-1
    do ix=1,mx
      scr(ix,iy,iz) = u_cool*exp(power*alog(tt(ix,iy,iz)))
    end do
    end do
    end do
  end if

  climit = 1./tt_limit
  do iz=1,mz
  do iy=lb,iphot-1
  do ix=1,mx
    enhance = max(1.0, tt(ix,iy,iz)*climit)**4
    enhance = merge (enhance, 1., do_limit)
    scr(ix,iy,iz) = -enhance*scale*exp(-(Tquench/tt(ix,iy,iz))**2) &
                    *scr(ix,iy,iz)*r(ix,iy,iz)**2
    dEdt(ix,iy,iz) = dEdt(ix,iy,iz) + scr(ix,iy,iz)	
  end do
  end do
  end do
  call dumpn(scr,'Qcorona','corona.dmp',1)

  do iy=lb,iphot-1
    scr(:,iy,:) = scr(:,iy,:)/(r(:,iy,:)*ee(:,iy,:))			! measure rate of change
  end do
  call dumpn(scr,'Qrate','corona.dmp',1)

  deallocate (tt)
!  call fstat1 ('Qcool',scr2,lb,ub)
  if (verbose==1 .and. master) print*, 'ending corona cooling' 
  if (verbose>1) print*, 'ending corona cooling',mpi_rank
END SUBROUTINE


!***********************************************************************
SUBROUTINE coolit_Casiana (tt,lambda)

  USE params
  USE eos
  USE units
  USE cooling
  implicit none
  integer ix,iy,iz
  real, intent(in), dimension(mx,my,mz):: tt
  real, intent(out), dimension(mx,my,mz):: lambda
  real temp, lambda1
  
  logical, save:: first_time=.true.
  real, parameter:: &

    TE0=1.e3, &
    TE1=10964.781e0, &
    TE2=19952.632e0, & 
    TE3=29512.078e0, & 
    TE4=39810.708e0, & 
    TE5=69183.121e0, &
    TE6=125892.51e0, &
    TE7=251188.7e0, &
    TE8=501187.01e0, &
    TE9=891250.55e0, &
    TE10=1.412537e6, & 
    TE11=2.511887e6, &
    TE12=6.309576e6, &
    TE13=1.e7, &
    TE14=1.5848925e7, &
    TE15=1.e9, &

    C0=2.596,   &
    C1=3.846, &
    C2=-0.1764, &
    C3=1.00001, &
    C4=1.6666, &
    C5=1.15384, &
    C6=0.1, &
    C7= -3.933, &
    C8= 0.72, &
    C9= -0.65,  &
    C10= 0.4, &
    C11= -1.25, &
    C12= 0., &
    C13= -0.4,  &
    C14= 0.43333, &
    
    H0=3.2473989e-14/4.e-28, &						! The coeffs have been increased with 1e20,
    H1=2.894e-19/4.e-28,  &						! and are then divided by 4e-28, for at total
    H2=5.739e-2/4.e-28,  &						! increase of 1/4d-48 = 1/2d-24^2.
    H3=3.1620277e-07/4.e-28,  &
    H4=2.7123742e-10/4.e-28,  &
    H5=8.2298862e-08/4.e-28,  &   
    H6=1.9497e-02/4.e-28,  &
    H8=3.5155e-7/4.e-28,  &
    H9=4.982757e+1/4.e-28,  &
    H10=1.7379e-5/4.e-28,  &   
    H11=6.309e+5/4.e-28,  &
    H12=1.99525e-03/4.e-28,  &
    H13=1.2589e-00/4.e-28,  &
    H14=1.2589e-06/4.e-28 

  real(kind=8), parameter:: &
    H7=1.1749d+20/4d-28

  character(len=mid):: id= &
    'coolit_Casiana: $Id: corona_cooling.f90,v 1.1 2015/04/16 15:33:11 aake Exp $'

  call print_id (id)

  do iz=1,mz
  do iy=lb,iphot-1
  do ix=1,mx
    temp = tt(ix,iy,iz)
    if      (temp .le. TE0) then
      lambda1 = 0.
    else if (temp .le. TE1) then
      lambda1 = H0*temp**C0
    else if (temp .le. TE2) then
      lambda1 = H1*temp**C1
    else if (temp .le. TE3) then
      lambda1 = H2*temp**C2
    else if (temp .le. TE4) then
      lambda1 = H3*temp**C3
    else if (temp .le. TE5) then
      lambda1 = H4*temp**C4
    else if (temp .le. TE6) then
      lambda1 = H5*temp**C5
    else if (temp .le. TE7) then
      lambda1 = H6*temp**C6
    else if (temp .le. TE8) then
      lambda1 = H7*temp**C7
    else if (temp .le. TE9) then
      lambda1 = H8*temp**C8
    else if (temp .le. TE10) then
      lambda1 = H9*temp**C9
    else if (temp .le. TE11) then
      lambda1 = H10*temp**C10
    else if (temp .le. TE12) then
      lambda1 = H11*temp**C11
    else if (temp .le. TE13) then
      lambda1 = H12*temp**C12
    else if (temp .le. TE14) then
      lambda1 = H13*temp**C13
    else
      lambda1 = H14*temp**C14
    end if
    lambda(ix,iy,iz)=lambda1
  end do
  end do
  end do
END SUBROUTINE
