; Setup for Longcope-Strauss benchmark

@stagger_6th                                                                  ; pick up stagger operators

device,decomp=0
loadct,5

  mx = 720                                                                    ; grid parameters 
  my = 720                                                                    ; 
  mz = 1                                                                      ; 2D (xy-plane)
  sx = 1d0                                                                    ; Length in x
  sy = 1d0                                                                    ; do. y
  dx = sx/mx                                                                  ; differential element in x
  dy = sy/my                                                                  ; do y
  init_derivs, dx=dx, dy=dy

  x = dx*(indgen(mx)-mx/2)                                                    ; make x-coord
  y = dy*(indgen(my)-my/2)                                                    ; make y-coord

  xx = rebin(reform(x,mx,1,1),mx,my,mz)                                       ; spread x-coord to entire plane
  yy = rebin(reform(y,1,my,1),mx,my,mz)                                       ; spread y-coord to entire plane

  R      = 1.                                                                 ; gas constant
  p0     = 1.0                                                                ; (mean value) pressure
  t0     = 1.0                                                                ; (mean value) temperature
  rho0   = 1.                                                                 ; (mean value) density
  a0     = 1./(!dpi*sqrt(2d0))                                                ; normalization of vector potential
  phi0   = 1.e-4                                                              ; perturbation should be 'small' (notes by Ed Lee)
  gamma  = 5./3.                                                              ; adiabtic index
  iseed  = 25                                                                 ; the usual seed 

;-----------------------------------------------------------------------------;
;Construct flux function A(x,y,t=0)
  axyt0 = a0*(sin(2d0*!dpi*xx)*sin(2d0*!dpi*yy))                              ; convolve the sine-functions on XY-plane.
  axyt0 = reform(axyt0,mx,my,mz)                                              ; reform for use with stagger operators
  help,axyt0

;-----------------------------------------------------------------------------;
;Get the B-field from flux function, B == grad(A) .X. zhat
  bx  = reform(+ddyup(xdn(ydn(axyt0))),mx,my,mz)                              ; grad(A) .X. zhat, x-compon.
  by  = reform(-ddxup(xdn(ydn(axyt0))),mx,my,mz)                              ; do. y-compon.
  bz  = reform(replicate(0.,mx,my,mz),mx,my,mz)                               ; no Bz
  help,bx,by,bz

;-----------------------------------------------------------------------------;
;Construct velocity perturbation
 ;phi = fltarr(mx,my,mz)                                                      ; array for potential
  phi = reform(phi0*(cos(2d0*!dpi*xx)-cos(2d0*!dpi*yy)),mx,my,mz)             ; velo. perturb. potential, don't worry too much about normalization as long as "small"
  vx  = reform(+ddyup(xdn(ydn(phi))),mx,my,mz)                                ; grad(phi) .X. zhat, x-compon.
  vy  = reform(-ddxup(xdn(ydn(phi))),mx,my,mz)                                ; do. y-compon.
  pz  = reform(replicate(0.,mx,my,mz),mx,my,mz)                               ; no z-velo.
  help,vx,vy,pz

;-----------------------------------------------------------------------------;
;Construct pressure
  p   = reform(p0+(2d0*!dpi*axyt0)^2,mx,my,mz)

;-----------------------------------------------------------------------------;
;Construct temperature (isothermal == const)
  t   = reform(replicate(t0,mx,my,mz),mx,my,mz)

;-----------------------------------------------------------------------------;
;Construct density (according to Ed Lee, w/isothermal)
  rho = reform((p/(R*t)),mx,my,mz)
  lnr = reform(alog(rho),mx,my,mz)
  px = exp(xdn(lnr))*vx
  py = exp(ydn(lnr))*vy

;-----------------------------------------------------------------------------;
;Construct internal energy
  e   = reform((1d0/(gamma-1d0))*p,mx,my,mz)                                  ; check this!

window,1,xsize=180*3,ysize=180*3

  imsz,180
  image,rho,0,tit='rho'
  image,px,1,tit='px'
  image,py,2,tit='py'
  image,pz,3,tit='pz'
  image,e,4,tit='e'
  image,bx,5,tit='bx'
  image,by,6,tit='by'
  image,bz,7,tit='bz'
  image,p,8,tit='p'
  image,t,9,tit='t'
  
window,2,xsize=360,ysize=360 & imsz,360,360 & image,ddxup(bx)+ddyup(by)+ddzup(bz),/stat,/minm
  
;STOP
;-----------------------------------------------------------------------------;
;Write out variables to initial MHD snapshot
;Nb: must run subsequent procedure to make room for passive scalar 
  close,1
  openw,1,'data/init.dat'
  writeu,1,float(rho)
  writeu,1,float(px)
  writeu,1,float(py)
  writeu,1,float(pz)
  writeu,1,float(e)
  writeu,1,float(bx)
  writeu,1,float(by)
  writeu,1,float(bz)
  close,1
END
