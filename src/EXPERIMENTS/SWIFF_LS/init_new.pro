; Setup for Longcope-Strauss benchmark

@stagger_6th                                                                  ; pick up stagger operators

device,decomp=0
loadct,39
imsz,200,200

  mx = 1024                                                                   ; grid parameters 
  my = 1024                                                                   ; 
  mz = 1                                                                      ; 2D (xy-plane)
  sx = 1.                                                                     ; Length in x
  sy = 1.                                                                     ; do. y
  dx = sx/mx                                                                  ; differential element in x
  dy = sy/my                                                                  ; do y

  R      = 1.                                                                 ; gas constant
  p0     = 0.1                                                                ; (mean value) pressure
  t0     = 1.0                                                                ; (mean value) temperature
  b0     = sqrt(2.)                                                           ; (mean value) b
  v0     = 2.*!pi*0.1                                                         ; velocity perturbation strength

  gamma  = 5./3.                                                              ; adiabtic index
  iseed  = 25                                                                 ; the usual seed 

  x = dx*findgen(mx)                                                          ; make x-coord
  y = dy*findgen(my)                                                          ; make y-coords

  xx = rebin(reform(x,mx,1,1),mx,my,mz)                                       ; spread x-coord to entire plane
  yy = rebin(reform(y,my,1,1),mx,my,mz)                                       ; spread y-coord to entire plane

  rho = fltarr(mx,my,mz)                                                      ; define arrays for MHD variables
  px  = fltarr(mx,my,mz)
  py  = fltarr(mx,my,mz)
  pz  = fltarr(mx,my,mz)
  bx  = fltarr(mx,my,mz)
  by  = fltarr(mx,my,mz)
  bz  = fltarr(mx,my,mz)
  e   = fltarr(mx,my,mz)
  sc  = fltarr(mx,my,mz)                                                      ; passive scalar (optional)

;-----------------------------------------------------------------------------;
;Construct temperature (isothermal == const)
  t   = reform(replicate(t0,mx,my,mz),mx,my,mz)

;-----------------------------------------------------------------------------;
;Construct the pressure (scalar)                                                    ;
  p = p0 + 2.*(sin(2.*!pi*xx)*transpose(sin(2.*!pi*yy)))^2                    ;

;-----------------------------------------------------------------------------;
;Construct the density                                                             ;
  rho = p/(R*t)                                                               ; 

;-----------------------------------------------------------------------------;
;Construct the B-field 
  bx  = reform(+ b0*(sin(2.*!pi*xx)*transpose(cos(2.*!pi*yy))),mx,my,mz)      ; produce the b-field components
  by  = reform(- b0*(cos(2.*!pi*xx)*transpose(sin(2.*!pi*yy))),mx,my,mz)      ;
  bz  = reform(replicate(0.,mx,my,mz),mx,my,mz)                               ;
  bx = xdn(bx)                                                                ; stagger field components
  by = ydn(by)                                                                ;
 ;bz = zdn(bz)                                                                ;
  print,'-------------------------------'
  print,'div(b): ' & stat,ddxup(bx)+ddyup(by)+ddzup(bz)                       ; check div(B)
  print,'-------------------------------'
  image,alog10(ddxup(bx)+ddyup(by)+ddzup(bz)),tit='log10(div(B))',0                          ; check div(B) visually
  image,bx,tit='Bx',1                                                         ; check components visually
  image,by,tit='By',2                                                         ;
  image,bz,tit='Bz',3                                                         ;

;-----------------------------------------------------------------------------;
;Construct velocity (momentum) perturbation
  px = reform(+ v0 * transpose(sin(2.*!pi*yy)),mx,my,mz)                      ; construct the perturbation velocity components
  py = reform(  v0 *           sin(2.*!pi*xx) ,mx,my,mz)                      ;
  pz = reform(replicate(0.,mx,my,mz)          ,mx,my,mz)                      ; no z-velo.
  px = reform(px*rho,mx,my,mz)
  py = reform(py*rho,mx,my,mz)
  pz = reform(pz*rho,mx,my,mz)
  px = xdn(px)                                                                ; stagger field components
  py = ydn(py)                                                                ;
 ;pz = zdn(pz)                                                                ; 1 layer only, no staggering

  print,'-------------------------------'
  print,'div(v): ' & stat,ddxup(bx)+ddyup(by)+ddzup(bz)                       ; check div(B)
  print,'-------------------------------'
  image,ddxup(px)+ddyup(py)+ddzup(pz),tit='div(momentum)',4                          ; check div(B) visually
  image,px,tit='px',5                                                         ; check components visually
  image,py,tit='py',6                                                         ;
  image,pz,tit='pz',7                                                         ;
  help,px,py,pz                                                               ; check by visual inspection

;-----------------------------------------------------------------------------;
;Construct internal energy
  e   = reform((1./(gamma-1.))*p,mx,my,mz)                                    ; check this!
  
;-----------------------------------------------------------------------------;
;Write out variables to initial MHD snapshot
;Nb: must run subsequent procedure to make room for passive scalar 
  close,1
  openw,1,'init.dat'
  writeu,1,rho
  writeu,1,px
  writeu,1,py
  writeu,1,pz
  writeu,1,e
  writeu,1,bx
  writeu,1,by
  writeu,1,bz
  close,1
END
