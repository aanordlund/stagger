
g,'63'                                  ; pick up 63x63x63 model
open,a63,'mhd63.dat',ym=y63

g,'mhd014'                              ; pick up 252x500x252 model, for averages
file=file_search('snapshot_*.dat')
open,a14,file[0],ym=y14

g,'spot'                                ; experiment location
restore,'mesh_bob.save',/verbose        ; pick up Bob's vertical meshes
ym=ym1
w=where(ym ge 10)                       ; 10 Mm plus ghost zones
ny=w[0]+6
ym=ym[0:ny-1]

; set horizontal averages
nz=63
mx=8
nx=nz*mx
dir='data/hd/504x348x63/'
spawn,'mkdir -p '+dir
openw,u,/get,dir+'snapshot_00000.dat'
tmp=fltarr(nx,ny,nz)
a=assoc(u,tmp)
ym=ym1[0:ny-1]
f14=haver(a14[0],/yvertical)
fm=interp(y14,ym,f14)
a[0]=rebin(reform(fm,1,ny,1),nx,ny,nz)
a[1]=tmp
a[2]=tmp
a[3]=tmp
a[4]=rebin(reform(interp(y14,ym,haver(a14[4],/yvertical)),1,ny,1),nx,ny,nz)
a[5]=rebin(reform(interp(y14,ym,haver(a14[5],/yvertical)),1,ny,1),nx,ny,nz)

; insert values from mhd63.dat
ny1=(where(ym gt y63[57]))[0]
for i=0,4 do begin
  f=a[i]
  g=intrp3y(y63,ym[0:ny1-1],a63[i])
  if (i eq 0) or (i eq 4) then begin
    fm=haver(f)
    gm=haver(g)
    for iy=0,ny1-1 do g[*,iy,*]=g[*,iy,*]*fm[iy]/gm[iy]
  end
  for j=0,mx-1 do f[j*63:j*63+62,0:ny1-1,*]=g
  a[i]=f
end

; break the repetitive symmetry with random perturbations
e=a[4]
perturb=lowpass(randomu(seed,nx))
perturb=rebin(reform(perturb,nx,1),nx,nz)*0.1
for iy=0,ny-1 do begin
  e[*,iy,*]=e[*,iy,*]+rms(e[*,iy,*])*perturb
end
a[4]=e
;free_lun,u

sz=6.
sx=mx*sz
dx=sx/nx
dz=sz/nz
xm=dx*findgen(nx)
zm=dz*findgen(nz)
openw,u1,/get,dir+'mesh.txt'
printf,u1,nx & printf,u1,xm,format='(8e15.7)'
printf,u1,ny & printf,u1,ym,format='(8e15.7)'
printf,u1,nz & printf,u1,zm,format='(8e15.7)'
free_lun,u1

END