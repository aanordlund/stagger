@stagger_jacobi

; $Id: init.pro,v 1.8 2011/10/05 18:20:30 aake Exp $
PRO init,a,bx,by,tt_int,y=y,B0=B0,w0=w0,beta=beta,pg=pg,yw=yw,nx=nx,nz=nz, $
  tablefile=tablefile,dryrun=dryrun
;
;    y: the depth scale
;   yw: the Wilson depression
;   dw: the transition scale at the Wilson depression
;   B0: the field strength at y=0
;   w0: the width of the sunspot at y=0
; beta: the beta values at the surface and at the bottom
; pext: the external gas pressure
;

default,yw,0.7
default,dw,0.25
default,beta,[0.1,20]
default,nx,63*32
default,nz,nx/16
default,sz,6.                         ; box thickness
default,sx,(sz*nx)/nz                 ; box width
default,w0,20.                        ; width in Mm

g,'spot'
eos_table,tablefile
dir='data/hd/504x348x63/'
dim=[504,348,63]
open,c,dir+'snapshot_00630.dat',dim=dim   ; open HD file
readmesh,dir+'mesh.dat',ym=ym
y=ym

rho=c[0]
pg=exp(lookup(alog(rho),c[4]/rho))
rhom=haver(rho,/yvertical)
pgm=haver(pg)
ttm=haver(c[5])
ttm[0]=ttm[1]
rho=0 

dx=sx/nx
dz=sz/nz
print,'dx,dz = ',dx,dz

i=where(y ge yw)
i0=i[0]                               ; the index where y=yw

u_pg=1e5                              ; pressure unit (cgs)
B=sqrt(8.*!pi*pgm*u_pg)               ; the B for beta=0 (Gauss)
p=0.5*(1+tanh((y-yw)/dw))             ; transition profile
beta_y=beta[0]*(1-p)+beta[1]*p        ; beta(y)
B=B/sqrt(1.+beta_y)                   ; the actual B(y)
B0=B[i0]                              ; normalized at i0
B0w0=B0*w0
w=w0*B[i0]/B                          ; the width w(y)
w1=sx*0.5
w=w*w1/(w+w1)

B0=B0w0/w[i0]                         ; renormalize because of new w profile
B=B0w0/w                              ; new depth profile
B1=B0-B0w0*2./sx                      ; adjust for zero point

window,6
plot,y,B,xr=[-0.5,1]

x=dx*(findgen(nx)-nx/2)
z=dz*(findgen(nz)-nz/2)

ny=n_elements(y)
az=fltarr(nx,ny,nz)
bz=fltarr(nx,ny,nz)
f=fltarr(nx)
for iy=0,ny-1 do begin
  i=where(    x  le -w[iy]) & f[i]=+B1*w[i0]/(sx/2.-w[i0])*(x[i]+sx/2.)
  i=where(    x  ge +w[iy]) & f[i]=+B1*w[i0]/(sx/2.-w[i0])*(x[i]-sx/2.)
  i=where(abs(x) lt +w[iy]) & f[i]=-B1*w[i0]/(sx/2.-w[i0])*(sx/2.-w[iy])/w[iy]*x[i]
  f=smooth(smooth(f,3),3)
  for iz=0,nz-1 do az[*,iy,iz]=f
end

out=str(nx)+'x'+str(ny)+'x'+str(nz)
mhddir='data/mhd/'+out
spawn,'mkdir -p '+mhddir
mhd=mhddir+'/snapshot_00000.dat'

if exists(mhddir+'/mesh.dat') then begin
  print,'reading mesh from '+mhddir+'/mesh.dat'
  readmesh,mhddir+'/mesh.dat',xm=x,ym=y,zm=z
end else begin
  print,'WARNING: using new mesh with init_derivs'
  init_derivs,x,y,z
end

by=-ddxup(az)
;stop
bx=+ddyup(az)
by=by-by[0,ny/2-1,0]
flux=aver(by[*,0,*])*sx*sz
up=1e5                                                  ; unit of pressure
ub=sqrt(up*4.*!pi)                                      ; unit of B
print,'flux (code, Mxw) = ',flux/ub,flux*1e16

ny=348
bx=bx[*,0:ny-1,*]/ub
by=by[*,0:ny-1,*]/ub

window,2,xsize=nx,ysize=ny & imsz,nx,ny
!order=1
image,by[*,*,0],sign=0.3

ttm[0]=ttm[1]
pg_int=pgm*beta_y/(1.+beta_y)
hp_int=1./deriv(y,alog(pg_int))
hp_ext=1./deriv(y,alog(pgm))
tt_int=hp_int/hp_ext*ttm

window,1,xsize=800,ysize=600
!p.multi=[0,2,3]
plot,y,ttm,xr=[-0.5,1],yr=[0,16e3]
oplot,y,tt_int,line=2
plot,x,by[*,ny-1,0],xr=[-1,1]*1.0,psym=-1
plot,y,by[nx/2,*,0],xr=[-0.5,1],yst=0
oplot,y,b/ub,line=2,psym=-1
print,'<By> =',aver(by[*,i0,*])
print,'flux =',B0w0
plot,x,by[*,i0,0],xr=[-1,1]*30,psym=-1
plot,x,bx[*,5,0],psym=-1
plot,x,bx[*,i0,0],xr=[-1,1]*30,psym=-1
!p.multi=[0,1,1]

if keyword_set(dryrun) then return

close,1
openw,1,mhd                                             ; open for writing
for i=0,5 do begin
  f=c[i]
  writeu,1,rebin([f,f],nx,ny,nz)                        ; copy HD data
end
writeu,1,bx                                             ; add B-field
writeu,1,by
writeu,1,bz
close,1

; init_derivs,x,y,z
window,3
plot,hrms(ddxup(bx)+ddyup(by),/yvertical),xrange=[5,ny-6], $
  title='div(B)'


openw,1,mhddir+'/mesh.txt'                              ; write mesh file
y=y[0:ny-1]
printf,1,nx & printf,1,x,format='(10f12.7)'
printf,1,ny & printf,1,y,format='(10f12.7)'
printf,1,nz & printf,1,z,format='(10f12.7)'
close,1

open,a,mhd,/update,dim=[nx,ny,nz]                       ; open for updating
wdelete,1,2,3
rh=a[0]
ee=a[4]/rh
ux=fux(a,0)
uy=fuy(a,0)
uz=fuz(a,0)
tt=a[5]
tt[*,0,*]=tt[*,1,*]
tt_ext=ttm
pg_ext=pgm
rh_ext=rhom
rh_int=rh_ext*pg_int/pg_ext*tt_ext/tt_int
for iy=0,ny-1 do begin
  p=by[*,iy,*]/max(by[*,iy,*])
  tt[*,iy,*]= tt[*,iy,*]*(1-p+p*tt_int[iy]/tt_ext[iy])
  rh[*,iy,*]=(rh[*,iy,*]*(1-p+p*rh_int[iy]/rh_ext[iy])) > (0.03*rh_ext[iy])
end
eos_table,'data/table_mactag.dat'
tmake,tt,alog(rh),ee,safe=20,eps=3e-4,itmax=25,/debug
a[0]=rh
a[1]=exp(xdn(alog(rh)))*ux
a[2]=exp(ydn(alog(rh)))*uy
a[3]=exp(zdn(alog(rh)))*uz
a[4]=rh*ee
a[5]=tt
open,a,mhd,dim=dim

END
