! $Id: forcing.f90,v 1.1 2011/11/07 08:12:05 aake Exp $
!**********************************************************************
MODULE forcing

  logical, parameter:: conservative=.true.
  logical do_force
  real gx, gy,gz, ay, t_friction, t_damp, ff, yg, dyg, yf, dyf
  real, allocatable, dimension(:) :: gya
  real(kind=8), allocatable, dimension(:) :: rav, pav, eav
  real(kind=8), allocatable, dimension(:,:) :: pyav, rzav
  integer ky, ny

END MODULE

!***********************************************************************
SUBROUTINE init_force
  USE params
  USE forcing
  implicit none
  integer iy
  
  !if (mpi_ny > 1) then
  !  if (master) print *,'MPI in y-direction not enabled in constant_gravity.f90'
  !  call end_mpi
  !end if

  do_force = .true.
  gx = 0.
  gy = 0.
  gz = 0.
  omegax = 0.
  omegay = 0.
  omegaz = 0.
  ay = 1.                   ! acc. of gravity drops to this fraction ...
  ny = 5                    ! as 'iy' varies over the range 'ny' ...
  ky = 15                   ! starting at iy=ky
  t_friction = 0.           ! default, no friction
  t_damp = 0.               ! default, no damping
  yf = -0.35                ! friction boundary
  yg = -0.35                ! reduced gravity boundary
  dyf = 0.05                ! friction transition
  dyg = 0.05                ! reduced gravity transition
  call read_force

  allocate (gya(my),pyav(mx,my),rav(my),rzav(mx,my),pav(my),eav(my))
  pav = 0.
  do iy=1,my
    ff = (ay+(1.-ay)*0.5*(1.+cos(min(max(yg-ym(iy),0.),dyg)*pi/dyg)))          ! smooth transition from yg-dyg to yg
    gya(iy) = ff*gy
  end do
END SUBROUTINE

!***********************************************************************
SUBROUTINE read_force
  USE params
  USE forcing
  implicit none
  namelist /force/ do_force, gx, gy, gz, omegax, omegay, omegaz, ay, &
    ky, ny, yf, dyf, yg, dyg, t_friction, t_damp
  character(len=mid):: id="$Id: forcing.f90,v 1.1 2011/11/07 08:12:05 aake Exp $"

  call print_id(id)

  rewind (stdin); read (stdin,force)
  if (master) write (*,force)
END SUBROUTINE

!***********************************************************************
SUBROUTINE forceit (r,Ux,Uy,Uz,xdnr,ydnr,zdnr,dpxdt,dpydt,dpzdt)
  USE params
  implicit none
  real, dimension(mx,my,mz):: r,Ux,Uy,Uz,xdnr,ydnr,zdnr,dpxdt,dpydt,dpzdt
  logical omp_in_parallel
!-----------------------------------------------------------------------
  if (omp_in_parallel()) then
    call forceit_omp (r,Ux,Uy,Uz,xdnr,ydnr,zdnr,dpxdt,dpydt,dpzdt)
  else
    !$omp parallel
    call forceit_omp (r,Ux,Uy,Uz,xdnr,ydnr,zdnr,dpxdt,dpydt,dpzdt)
    !$omp end parallel
  end if
END

!***********************************************************************
SUBROUTINE forceit_omp (r,Ux,Uy,Uz,xdnr,ydnr,zdnr,dpxdt,dpydt,dpzdt)
  USE params
  USE variables, only: py
  USE forcing
  USE arrays, ONLY: scr4, scr5
  implicit none
  real, dimension(mx,my,mz):: r,Ux,Uy,Uz,xdnr,ydnr,zdnr,dpxdt,dpydt,dpzdt
  integer iy, iz
  real f
  character(len=mid):: id="$Id: forcing.f90,v 1.1 2011/11/07 08:12:05 aake Exp $"
!-----------------------------------------------------------------------
  call print_id(id)

  if (gx.ne.0.) then
    do iz=izs,ize
      dpxdt(:,:,iz) = dpxdt(:,:,iz) + gx*xdnr(:,:,iz)
    end do
  end if

  if (gy.ne.0.) then
    do iz=izs,ize
    do iy=1,my
      dpydt(:,iy,iz) = dpydt(:,iy,iz) + gya(iy)*ydnr(:,iy,iz)
    end do
    end do
  end if

  if (gz.ne.0.) then
    do iz=izs,ize
      dpzdt(:,:,iz) = dpzdt(:,:,iz) + gz*zdnr(:,:,iz)
    end do
  end if

  if (t_damp.ne.0.) then
    call zaverage_subr (py  , pyav)                                       ! horizontally averaged py
    call zaverage_subr (ydnr, rzav)                                       ! horizontally averaged rho
    do iz=izs,ize
      do iy=lb,ub
        dpydt(:,iy,iz) = dpydt(:,iy,iz) - xdnr(:,iy,iz)*(pyav(:,iy)/(rzav(:,iy)*t_damp))
      end do
    end do
  end if
  
  if (t_friction.ne.0.) then
    do iz=izs,ize
      do iy=lb,ub
        f = (1./t_friction)*(1.-cos(min(max(yf-ym(iy),0.),dyf)*pi/dyf))*0.5
        dpxdt(:,iy,iz) = dpxdt(:,iy,iz) - f*xdnr(:,iy,iz)*Ux(:,iy,iz)
        dpydt(:,iy,iz) = dpydt(:,iy,iz) - f*ydnr(:,iy,iz)*Uy(:,iy,iz)
        dpzdt(:,iy,iz) = dpzdt(:,iy,iz) - f*zdnr(:,iy,iz)*Uz(:,iy,iz)
      end do
    end do
  end if

  if (omegax.ne.0 .or. omegay.ne.0 .or. omegaz.ne.0) then
    if (omegay.ne.0 .or. omegaz.ne.0) then
      do iz=izs,ize
        scr5(:,:,iz) = xdnr(:,:,iz)*Ux(:,:,iz)
      end do
      call xup_set(scr5,scr4)
      if (omegaz .ne. 0) then
        call ydn_set(scr4,scr5)
        do iz=izs,ize
          do iy=lb,ub
            dpydt(:,iy,iz) = dpydt(:,iy,iz) - (2.*omegaz)*scr5(:,iy,iz)
          end do
        end do
      end if
      if (omegay .ne. 0) then
!$omp barrier
        call zdn_set(scr4,scr5)
        do iz=izs,ize
          do iy=lb,ub
            dpzdt(:,iy,iz) = dpzdt(:,iy,iz) + (2.*omegay)*scr5(:,iy,iz)
          end do
        end do
      end if
    end if
    if (omegaz.ne.0 .or. omegax.ne.0) then
      do iz=izs,ize
        scr5(:,:,iz) = ydnr(:,:,iz)*Uy(:,:,iz)
      end do
      call yup_set(scr5,scratch)
      if (omegax .ne. 0) then
!$omp barrier
        call zdn_set(scratch,scr5)
        do iz=izs,ize
          do iy=lb,ub
            dpzdt(:,iy,iz) = dpzdt(:,iy,iz) - (2.*omegax)*scr5(:,iy,iz)
          end do
        end do
      end if
      if (omegaz .ne. 0) then
        call xdn_set(scratch,scr5)
        do iz=izs,ize
          do iy=lb,ub
            dpxdt(:,iy,iz) = dpxdt(:,iy,iz) + (2.*omegaz)*scr5(:,iy,iz)
          end do
        end do
      end if
    end if
    if (omegax.ne.0 .or. omegay .ne.0) then
      do iz=izs,ize
        scr5(:,:,iz) = zdnr(:,:,iz)*Uz(:,:,iz)
      end do
!$omp barrier
      call zup_set(scr5,scr4)
      if (omegay .ne. 0) then
        call xdn_set(scr4,scratch)
        do iz=izs,ize
          do iy=lb,ub
            dpxdt(:,iy,iz) = dpxdt(:,iy,iz) - (2.*omegay)*scratch(:,iy,iz)
          end do
        end do
      end if
      if (omegax .ne. 0) then
        call ydn_set(scr4,scratch)
        do iz=izs,ize
          do iy=lb,ub
            dpydt(:,iy,iz) = dpydt(:,iy,iz) + (2.*omegax)*scratch(:,iy,iz)
          end do
        end do
      end if
    end if
  end if

  return
END SUBROUTINE
