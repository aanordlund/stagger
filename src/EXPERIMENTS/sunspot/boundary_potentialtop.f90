! $Id: boundary_potentialtop.f90,v 1.8 2011/09/05 06:57:23 aake Exp $

!-----------------------------------------------------------------------
SUBROUTINE viscosity_boundary_top (nu, cs)
  USE params
  implicit none
  real, dimension(mx,my,mz):: nu, cs
  integer iz

END

!-----------------------------------------------------------------------
SUBROUTINE density_boundary_top (r,lnr,py,e)
  USE params
  USE boundary
  USE arrays, only: scr1
  USE forcing, only: rav, eav
  implicit none
  real, dimension(mx,my,mz):: r,lnr,py,e
  integer ix,iy,iz
  real efact, lnrmin, lnrmax, r_prv, f
  real(kind=8) tmp(3)
  logical do_io

!if (do_trace) print *, 'density_BC: mpi_rank,y,ny = ', mpi_rank, mpi_y, mpi_ny

  call dumpn(r,'r0','bdry.dmp',1)

  call haverage_subr (r, rav)                                           ! horizontally averaged rho
  call haverage_subr (e, eav)                                           ! horizontally averaged e

  if (lb > 1) then                                                      ! top BC
    call extrapolate_center_lower(lnr)                                  ! extrapolate in the log
    do iz=izs,ize
      do iy=1,my
      do ix=1,mx
        lnrmin = alog(rmin*real(rav(iy)))                               ! min lnrho value
        lnrmax = alog(rmax*real(rav(iy)))                               ! max lnrho value in inflows
        lnr(ix,iy,iz) = max(lnr(ix,iy,iz),lnrmin)                       ! clamp extreme values
        lnr(ix,iy,iz) = min(lnr(ix,iy,iz),lnrmax)                       ! clamp extreme values
	f = exp(lnr(ix,iy,iz))/r(ix,iy,iz)
	if (abs(f-1.) > .01) then
          r(ix,iy,iz) = r(ix,iy,iz)*f                                   ! consistent density
          e(ix,iy,iz) = e(ix,iy,iz)*f                                   ! consistent energy
	end if
      end do
      end do
    end do

    if (eetop_frac == 1.) then
      eetop = eav(lb+1)/rav(lb+1)                                       ! set
    else
      !$omp master
      if (eetop.lt.0) eetop=eav(lb+1)/rav(lb+1)                         ! top boundary energy
      eetop = (1.-eetop_frac)*eetop + eetop_frac*eav(lb+1)/rav(lb+1)    ! gradual change (optional)
      !$omp end master
      !$omp barrier
    end if
    !e(:,1:lb,izs:ize)=r(:,1:lb,izs:ize)*eetop                           ! ee=eetop in ghostzone and first physical layer
    do iy=1,lb
      e(:,iy,izs:ize)=e(:,lb+1,izs:ize)/r(:,lb+1,izs:ize)*r(:,iy,izs:ize)! ee pattern propagated
    end do

  end if

  if ((do_io (t, tsnap, isnap+isnap0, nsnap) &
  .or. do_io (t, tscr, iscr+iscr0, nscr) &
  .or. debug) .and. master .and. isubstep==timeorder) print *,'eetop =',eetop

END

!-----------------------------------------------------------------------
SUBROUTINE energy_boundary_top (r,lnr,Ux,Uy,Uz,e,ee,p,Bx,By,Bz)
  USE params
  implicit none
  real, dimension(mx,my,mz):: r,lnr,Ux,Uy,Uz,e,ee,p,Bx,By,Bz

END

!-----------------------------------------------------------------------
SUBROUTINE passive_boundary_top (r,Ux,Uy,Uz,d,dd)
  USE params
  implicit none
  real, dimension(mx,my,mz):: r,Ux,Uy,Uz,d,dd
  integer iy, iz

  call symmetric_center(dd)
  if (mpi_y == 0) then                                                  ! top BC
    do iz=izs,ize
    do iy=1,lb-1
      d(:,iy,iz)=r(:,iy,iz)*dd(:,iy,iz)
    end do
    end do
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE velocity_boundary_top (r,xdnr,ydnr,zdnr,Ux,Uy,Uz,px,py,pz,e,Bx,By,Bz)
  USE params
  USE boundary
  implicit none
  real, dimension(mx,my,mz):: r,xdnr,ydnr,zdnr,Ux,Uy,Uz,px,py,pz,e,Bx,By,Bz
  real p, rin, rtot, fmout, fmtot, uyin, uytot
  integer ix, iy, iz

    !if (do_trace) print 1, 'velocity_BC: mpi_rank,y,ny = ', mpi_rank, mpi_y, mpi_ny, omp_mythread
1   format(1x,a,4i6)
!
!  Top boundary, vanishing velocity derivatives
!

  if (lb > 1) then
    do iz=izs,ize
    do ix=1,mx
      Ux(ix,lb,iz) = Ux(ix,lb+1,iz)
      Uz(ix,lb,iz) = Uz(ix,lb+1,iz)
    end do
    end do
  end if

  call constant_center_lower (Ux)
! call extrapolate_constrained_face_lower  (Uy)
  call constant_face_lower  (Uy)
  call constant_center_lower (Uz)
    !if (do_trace) print 1,'velocity: 2', mpi_rank, mpi_y, mpi_ny, omp_mythread
!
!  Top boundary, impose maximum velocity -- relevant when the 
!  density is clamped and cannot become lower
!
  if (lb > 1) then
    do iz=izs,ize
    do ix=1,mx
      do iy=1,lb+1
        ux(ix,iy,iz) = max(min(ux(ix,iy,iz),+uy_max),-uy_max)
        uy(ix,iy,iz) = max(min(uy(ix,iy,iz),+uy_max),-uy_max)
        uz(ix,iy,iz) = max(min(uz(ix,iy,iz),+uy_max),-uy_max)
      end do
      if (uy(ix,lb+1,iz) > 0. .or. uy(ix,lb,iz) > 0.) then
        do iy=1,lb
          uy(ix,iy,iz) = min(uy(ix,iy,iz),uy(ix,lb+1,iz))
        end do
      end if
    end do
    end do
!
!  Recompute mass fluxes
!
    do iz=izs,ize
      do iy=1,lb+1
        px(:,iy,iz)=xdnr(:,iy,iz)*Ux(:,iy,iz)
        py(:,iy,iz)=ydnr(:,iy,iz)*Uy(:,iy,iz)
        pz(:,iy,iz)=zdnr(:,iy,iz)*Uz(:,iy,iz)
      end do
    end do
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE mfield_boundary_top (Ux,Uy,Uz,Bx,By,Bz)
  USE params
  USE boundary
  implicit none
  real, dimension(mx,my,mz):: Ux,Uy,Uz,Bx,By,Bz
  integer ix, iy, iz
  real f
  character(len=mid), save:: id='mfield_boundary $Id: boundary_potentialtop.f90,v 1.8 2011/09/05 06:57:23 aake Exp $'

  call print_id(id)
!-----------------------------------------------------------------------
!  Top boundary:  Potential extrapolation is used as a guiding field,
!  not directly enforced, but controls the E-field.
!-----------------------------------------------------------------------

  call potential_lower(Bx,By,Bz,lb+1,lb,lb)                             ! ->potential
  call dumpn(Bx,'Bx','bdry.dmp',1)
  call dumpn(By,'By','bdry.dmp',1)
  call dumpn(Bz,'Bz','bdry.dmp',1)

END

!-----------------------------------------------------------------------
SUBROUTINE ecurrent_boundary_top (Ex, Ey, Ez, Jx, Jy, Jz)
  USE params
  USE boundary
  implicit none
  real, dimension(mx,my,mz):: Ex, Ey, Ez, Jx, Jy, Jz

END

!-----------------------------------------------------------------------
SUBROUTINE efield_boundary_top (Ex, Ey, Ez, Bx_y, Bx_z, By_z, By_x, Bz_x, Bz_y, &
                                Bx, By, Bz, Ux_y, Ux_z, Uy_z, Uy_x, Uz_x, Uz_y, &
                                Ux, Uy, Uz)
  USE params
  USE boundary
  USE arrays, only: scr1, scr2
  implicit none
  real, dimension(mx,my,mz):: Ex, Ey, Ez, Bx_y, Bx_z, By_z, By_x, Bz_x, Bz_y, &
                              Ux_y, Ux_z, Uy_z, Uy_x, Uz_x, Uz_y, &
                              Ux, Uy, Uz, Bx, By, Bz
  character(len=mid), save:: id=&
  'efield_boundary $Id: boundary_potentialtop.f90,v 1.8 2011/09/05 06:57:23 aake Exp $'

  call print_id (id)

!-----------------------------------------------------------------------
!  Top boundary: Do nothing, since the two contributions -- resistive 
!  and advective are only contaminated beyond three points from the bdry.
!-----------------------------------------------------------------------

END

!-----------------------------------------------------------------------
SUBROUTINE ddt_boundary_top (r,px,py,pz,e,p,Bx,By,Bz, &
                             drdt,dpxdt,dpydt,dpzdt, &
                             dedt,dBxdt,dBydt,dBzdt)
  USE params
  USE forcing, only: gya, rav, pav
  USE boundary
  implicit none
  real, dimension(mx,my,mz):: r,px,py,pz,e,p,Bx,By,Bz, &
                              drdt,dpxdt,dpydt,dpzdt, &
                              dedt,dBxdt,dBydt,dBzdt
  integer ix,iz,ixp1,izp1
  real(kind=8) rub1,pyub1,elb1,eub1,ct,pub1,pubt1,dpydtub1,scr,htopi
  character(len=mid), save:: id=&
  'ddt_boundary_top 0.05 fraction smoothing $Id: boundary_potentialtop.f90,v 1.8 2011/09/05 06:57:23 aake Exp $'
!.......................................................................
  call print_id (id)

!$omp barrier
  if (debug) print *,'bdry',mpi_y,ym(1),rbot,ebot
    call dumpn(drdt,'drdt','bdry.dmp',1)
    call dumpn(dedt,'dedt','bdry.dmp',1)

  if (debug .and. omp_master) print *,'ddt:bdry: Top boundary'
  call haverage_subr (p, pav)                                           ! needed below
  if (mpi_y == 0) then                                                  ! top BC
    htopi = gya(lb)*rav(lb)/pav(lb)                                     ! inverse pressure scale height
    do iz=izs,ize
    do ix=1,mx
      if (py(ix,lb+1,iz) > 0. .or. py(ix,lb,iz) > 0.) then              ! for inflows ..
        drdt(ix,lb,iz) = drdt(ix,lb,iz) &                               ! let the density
          + (py(ix,lb+1,iz)-py(ix,lb,iz))/dym(lb) &                     ! remove the -ddyup(py) part!
          - htopi*py(ix,lb+1,iz)                                        ! replace with -py/H term
      end if
      drdt (ix,1:lb-1,iz) = 0.                                          ! zap the ghost zone dfdt's
      dpxdt(ix,1:lb-1,iz) = 0.
      dpydt(ix,1:lb  ,iz) = 0.
      dpzdt(ix,1:lb-1,iz) = 0.
      dedt (ix,1:lb-1,iz) = 0.
      
      !dedt (ix,lb,iz) = drdt(ix,lb,iz)*eetop                            ! set dedt in first physical layer
      dedt (ix,lb,iz) = 0.

      if (do_mhd) then                                                  ! MHD case
        dBxdt(ix,1:3,iz)=0.                                             ! the E-field is contaminated up to iy=3
        dBzdt(ix,1:3,iz)=0.
        dBydt(ix,1:3,iz)=0.
      endif
    end do
    end do
  end if
END
