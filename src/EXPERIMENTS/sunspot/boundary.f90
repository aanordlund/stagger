! $Id: boundary.f90,v 1.4 2015/03/14 22:19:02 aake Exp $
!***********************************************************************
MODULE boundary
  use params
  implicit none
  real t_Bbdry, Bscale_top, Bscale_bot, lb_eampl, t_bdry, eetop_frac, Uy_bdry, t_vbdry
  real rbot, pbot, ebot, htop
  real Bx0, By0, Bz0, Bx0_top, Bz0_top, rmin, rmax, pb_fact, uy_max
  real dBx0dt, dBy0dt, dBz0dt
  real pdamp, rtop_frac, fmtop_frac, divb_max
  real ampxy, ampyz, period, omegap, akx, aky, akz, kxp, kyp, kzp
  real:: e_lb_pert=0.0, e_lb_width=0.2
  real(kind=8) rub,pyub,elb,eub,pub,pubt,dpydtub
  real pyubin,pyubout,iin,iout
  real, allocatable, dimension(:,:,:):: Bxl, Byl, Bzl, Bxu, Byu, Bzu
  real, allocatable, dimension(:,:,:):: Exl, Eyl, Ezl, Exu, Eyu, Ezu
  real eta_scl
  logical debug, bdry_first, Binflow, do_eta
  integer verbose, lb1, eta_width

END module

!-----------------------------------------------------------------------
SUBROUTINE init_boundary
  USE params
  USE arrays
  USE boundary
  implicit none
  logical fexists
  character(len=mfile) :: name
  character(len=mid):: id='$Id: boundary.f90,v 1.4 2015/03/14 22:19:02 aake Exp $'

  call print_id(id)

  if (mpi_y == 0) then
    lb = 6
    allocate (Bxl(mx,lb,mz),Byl(mx,lb,mz),Bzl(mx,lb,mz))
  else
    lb = 1
  end if
  if (mpi_y == mpi_ny-1) then
    ub = my-5
    allocate (Bxu(mx,my-ub,mz),Byu(mx,my-ub,mz),Bzu(mx,my-ub,mz))
  else
    ub = my
  end if

  periodic(2) = .false.                                                 ! non-periodic y
  t_bdry = 0.01                                                         ! bdry decay time
  t_Bbdry  = 0.01                                                       ! bdry decay time for B
  t_vbdry = t_bdry*20							! bdry decay time for Vinflow
  eetop_frac = 1.                                                       ! fraction of current eetop
  eetop = -1.                                                           ! signal setting eetop
  rtop_frac=0.05                                                        ! fraction of current rtop
  fmtop_frac=1.0                                                        ! fraction fmass correction for rho(lb)
  pdamp = 0.
  Uy_bdry = 0.1
  lb_eampl = 0.
  rbot = -1.
  htop = 0.
  rmin = 1e-3
  rmax = 1e1
  pb_fact = 1.0
  uy_max = 2.                                                           ! 20 km/s with solar scaling
  Bscale_top = 10
  Bscale_bot = 10
  Bx0 = 0.
  By0 = 0.
  Bz0 = 0.
  Bx0_top = 0.
  Bz0_top = 0.
  Binflow = .true.
  debug = .false.
  verbose = 0
  bdry_first = .true.
  do_stratified = .true.
  ampxy = 0.
  ampyz = 0.
  period = 0.
  akx = 1.
  aky = 1.
  akz = 1.
  do_eta = .false.
  eta_width = 9
  eta_scl = 3.

  call read_boundary

  if (.not. master) debug=.false.
END

!-----------------------------------------------------------------------
SUBROUTINE read_boundary
  USE params
  USE boundary
  implicit none
  namelist /bdry/ lb, ub, t_bdry, t_vbdry, t_Bbdry, Bscale_top, Bscale_bot, eetop_frac, lb_eampl, &
                  htop, rtop_frac, fmtop_frac, eetop, rbot, ebot, pdamp, &
                  Bx0, By0, Bz0, Bx0_top, Bz0_top, Binflow, Uy_bdry, do_stratified, &
                  rmin, rmax, pb_fact, uy_max, debug, verbose, &
                  ampxy, ampyz, period, akx, aky, akz, &
                  do_eta, eta_width, eta_scl, &
                  e_lb_pert, e_lb_width

  rewind (stdin); read (stdin,bdry)
  if (master) write (*,bdry)
END

!-----------------------------------------------------------------------
SUBROUTINE regularize (f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
!
  !if (do_trace) print *,'regularize 1:',mpi_rank,omp_mythread,mpi_y
  if (mpi_y ==        0) call symmetric_center_lower (f)
  if (mpi_y == mpi_ny-1) call symmetric_center_upper (f)
  !if (do_trace) print *,'regularize 2:',mpi_rank,omp_mythread,mpi_y
END

!-----------------------------------------------------------------------
SUBROUTINE regularize_face (f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
!
  if (mpi_y ==        0) call symmetric_face_lower (f)
  if (mpi_y == mpi_ny-1) call symmetric_face_upper (f)
END

!-----------------------------------------------------------------------
SUBROUTINE viscosity_boundary (nu, cs)
  USE params
  implicit none
  real, dimension(mx,my,mz):: nu, cs
  integer iz

  call viscosity_boundary_top (nu, cs)
  call viscosity_boundary_bot (nu, cs)
END

!-----------------------------------------------------------------------
SUBROUTINE density_boundary (r,lnr,py,e,ee)
  USE params
  implicit none
  real, dimension(mx,my,mz):: r,lnr,py,e,ee

  call density_boundary_top (r,lnr,py,e)
  call density_boundary_bot (r,lnr,py,e,ee)
END

!-----------------------------------------------------------------------
SUBROUTINE energy_boundary (r,lnr,Ux,Uy,Uz,e,ee,p,Bx,By,Bz)
  USE params
  implicit none
  real, dimension(mx,my,mz):: r,lnr,Ux,Uy,Uz,e,ee,p,Bx,By,Bz

  call energy_boundary_top (r,lnr,Ux,Uy,Uz,e,ee,p,Bx,By,Bz)
  call energy_boundary_bot (r,lnr,Ux,Uy,Uz,e,ee,p,Bx,By,Bz)
END

!-----------------------------------------------------------------------
SUBROUTINE passive_boundary (r,Ux,Uy,Uz,d,dd)
  USE params
  implicit none
  real, dimension(mx,my,mz):: r,Ux,Uy,Uz,d,dd

  call passive_boundary_top (r,Ux,Uy,Uz,d,dd)
  call passive_boundary_bot (r,Ux,Uy,Uz,d,dd)
END

!-----------------------------------------------------------------------
SUBROUTINE velocity_boundary (r,xdnr,ydnr,zdnr,Ux,Uy,Uz,px,py,pz,e,Bx,By,Bz)
  USE params
  implicit none
  real, dimension(mx,my,mz):: r,xdnr,ydnr,zdnr,Ux,Uy,Uz,px,py,pz,e,Bx,By,Bz

  call velocity_boundary_top (r,xdnr,ydnr,zdnr,Ux,Uy,Uz,px,py,pz,e,Bx,By,Bz)
  call velocity_boundary_bot (r,xdnr,ydnr,zdnr,Ux,Uy,Uz,px,py,pz,e,Bx,By,Bz)
END

!-----------------------------------------------------------------------
SUBROUTINE magnetic_pressure_boundary (BB)
  USE params
  implicit none
  real, dimension(mx,my,mz):: BB

  !call magnetic_pressure_boundary_top (BB)
  call magnetic_pressure_boundary_bot (BB)
END

!-----------------------------------------------------------------------
SUBROUTINE mfield_boundary (Ux,Uy,Uz,Bx,By,Bz)
  USE params
  implicit none
  real, dimension(mx,my,mz):: Ux,Uy,Uz,Bx,By,Bz

  call mfield_boundary_top (Ux,Uy,Uz,Bx,By,Bz)
  call mfield_boundary_bot (Ux,Uy,Uz,Bx,By,Bz)
END

!-----------------------------------------------------------------------
SUBROUTINE ecurrent_boundary (Ex, Ey, Ez, Jx, Jy, Jz)
  USE params
  implicit none
  real, dimension(mx,my,mz):: Ex, Ey, Ez, Jx, Jy, Jz

  call ecurrent_boundary_top (Ex, Ey, Ez, Jx, Jy, Jz)
  call ecurrent_boundary_bot (Ex, Ey, Ez, Jx, Jy, Jz)
END

!-----------------------------------------------------------------------
SUBROUTINE efield_boundary (Ex, Ey, Ez, Bx_y, Bx_z, By_z, By_x, Bz_x, Bz_y, &
                            Bx, By, Bz, Ux_y, Ux_z, Uy_z, Uy_x, Uz_x, Uz_y, &
                            Ux, Uy, Uz)
  USE params
  implicit none
  real, dimension(mx,my,mz):: Ex, Ey, Ez, Bx_y, Bx_z, By_z, By_x, Bz_x, Bz_y, &
                              Ux_y, Ux_z, Uy_z, Uy_x, Uz_x, Uz_y, &
                              Ux, Uy, Uz, Bx, By, Bz

  call efield_boundary_top (Ex, Ey, Ez, Bx_y, Bx_z, By_z, By_x, Bz_x, Bz_y, &
                            Bx, By, Bz, Ux_y, Ux_z, Uy_z, Uy_x, Uz_x, Uz_y, &
                            Ux, Uy, Uz)
  call efield_boundary_bot (Ex, Ey, Ez, Bx_y, Bx_z, By_z, By_x, Bz_x, Bz_y, &
                            Bx, By, Bz, Ux_y, Ux_z, Uy_z, Uy_x, Uz_x, Uz_y, &
                            Ux, Uy, Uz)
END

!-----------------------------------------------------------------------
SUBROUTINE ddt_boundary (r,px,py,pz,e,p,Bx,By,Bz, &
                         drdt,dpxdt,dpydt,dpzdt, &
                         dedt,dBxdt,dBydt,dBzdt)
  USE params
  implicit none
  real, dimension(mx,my,mz):: r,px,py,pz,e,p,Bx,By,Bz, &
                              drdt,dpxdt,dpydt,dpzdt, &
                              dedt,dBxdt,dBydt,dBzdt

  call ddt_boundary_top (r,px,py,pz,e,p,Bx,By,Bz, &
                         drdt,dpxdt,dpydt,dpzdt, &
                         dedt,dBxdt,dBydt,dBzdt)
  call ddt_boundary_bot (r,px,py,pz,e,p,Bx,By,Bz, &
                         drdt,dpxdt,dpydt,dpzdt, &
                         dedt,dBxdt,dBydt,dBzdt)
END
