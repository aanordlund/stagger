#!/bin/csh

# job control. Syntax: job.csh [ nthreads [inputfile] ]

set x = mconv_ifort_mpi.x				# magnetoconvection 
set x = mactag_ifort_mpi.x				# corona + magnetoconvection
set x = sunspot_ifort_mpi.x				# sunspot special

set i = input.txt					# default input file

mkdir -p LOG						# make sure LOG/ directory exists

# extract the job number and copy the hosts file (SYSTEM DEPENDENT)
set l = LOG/`echo $LOADL_STEP_ID | sed -e 's/\.[0-9]$//' -e 's/.*\.//'`
cp -p  $LOADL_HOSTFILE $l.hosts

if (-l output.txt) mv output.txt previous.txt		# shortcut name for previous output
set o = $l.out.$$					# job step output, changes on restart
ln -sf $o output.txt					# shortcut name for output

\cp -p ~/bin/x86_64/$x $l.$x				# make a local copy to avoid killing jobs
\cp -p input.txt $l.in.$$				# save a copy of the input file

(echo "BEGIN `date` `ls -l $l.$x`"; echo '') >> $o	# date and executable logged

set c = `wc -l < $LOADL_HOSTFILE`			# number of threads in hosts file
set c = `np.csh $c`					# rounded to useful number (can be commented out)

if ("$1" != "") set c = $1				# non-default number of threads
if ("$2" != "") set a = $2				# non-default input file

setenv OMPI_MCA_mpi_paffinity_alone 1			# OpenMPI env var to lock threads on cores
mpirun -np $c --hostfile $l.hosts ./$l.$x $i >>$o	# execute with redirected output
