#!/bin/csh

# job control. Syntax: job.csh [ nthreads ]

set c = `wc -l < $LOADL_HOSTFILE`
mpirun -np $c --hostfile $LOADL_HOSTFILE ./mactag_ifort_mpi.x >> output.txt

#set echo

set x = mconv_ifort_mpi.x
set x = mactag_ifort_mpi.x
set a = input.txt

source /software/astro/bin/LLsetup.csh		# std LL setup

set i = $l.in
set o = $l.out

set o = $o.$$
if (-l output.txt) mv output.txt previous.txt
ln -sf $o output.txt

#\cp -p ~/bin/nehalem/$x $l.$x			# make a local copy to avoid killing jobs
#\cp -p ~/bin/x86_64/$x $l.$x			# make a local copy to avoid killing jobs
\cp -p $x $l.$x					# make a local copy to avoid killing jobs
set x = $l.$x
\cp -p input.txt $l.in.$$			# save a copy of the input file

(echo "BEGIN `date` `ls -l $x`"; echo '') >> $o	# date and executable logged

set c = `wc -l < $LOADL_HOSTFILE`
set c = `np.csh $c`

if ("$1" != "") set c = $1			# non-default number of threads
if ("$2" != "") set a = $2			# non-default input file

mpirun -np $c --hostfile $h ./$x $a >>$o
