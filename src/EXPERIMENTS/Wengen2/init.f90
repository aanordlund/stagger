!************************************************************************
MODULE init_m
  real ux1, ux2, uy1, uy2, dy1, lnr1, k1, k2
END MODULE

!-----------------------------------------------------------------------
FUNCTION input_file()
  USE params
  implicit none
  character(len=mfile) input_file
  input_file='EXPERIMENTS/Wengen2/input.txt'
END

!-----------------------------------------------------------------------
SUBROUTINE init_values (r,px,py,pz,e,d,Bx,By,Bz)
  USE params
  USE init_m
  implicit none
  real, dimension(mx,my,mz):: r,px,py,pz,e,d,Bx,By,Bz
  real fx, fy, k0
  integer ix, iy, iz
  character(len=mid):: id="$Id: init.f90,v 1.5 2011/04/07 08:42:07 aake Exp $"
  call print_id (id)

  k0 = 2.*pi/sx
  do iz=1,mz; do iy=1,my; do ix=1,mx
    fx = exp(-10.*abs(ym(iy)))*cos(k1*xmdn(ix)*k0)*cos(k2*xmdn(ix)*k0)
    fy = 1./(1+exp(-2.*ym(iy)/dy1))
    r(ix,iy,iz) = 1.
    px(ix,iy,iz) = fx*(ux1*(1-fy) + ux2*fy)
    fy = 1./(1+exp(-2.*ymdn(iy)/dy1))
    py(ix,iy,iz) = uy1*(1-fy) + uy2*fy
    pz(ix,iy,iz) = 0.
    e(ix,iy,iz) = 1./(gamma-1.)
  end do; end do; end do
END SUBROUTINE

!-----------------------------------------------------------------------
SUBROUTINE init_boundary
  USE params
  USE init_m
  implicit none
  namelist /init/ ux1, ux2, uy1, uy2, dy1, k1, k2
!
  k1=10.; k2=3.; ux1=-.3; ux2=.2; uy1=0.4; uy2=-0.4; dy1=0.2
  rewind (stdin); read (stdin,init)
  if (master) write(*,init)
  lb = 5
  ub = my-5
END

!-----------------------------------------------------------------------
SUBROUTINE read_boundary
  USE params
  implicit none
END

!-----------------------------------------------------------------------
SUBROUTINE density_boundary (r,lnr,py,e)
  USE params
  USE init_m
  implicit none
  real, dimension(mx,my,mz):: r, lnr, py, e
  if (mpi_y == 0) r(:,1:lb,:) = 1.
  if (mpi_y == mpi_ny-1) r(:,ub:my,:) = 1.
END SUBROUTINE

!-----------------------------------------------------------------------
SUBROUTINE velocity_boundary (r,xdnr,ydnr,zdnr,Ux,Uy,Uz,px,py,pz,e,Bx,By,Bz)
  USE params
  USE init_m
  implicit none
  real, dimension(mx,my,mz):: r,xdnr,ydnr,zdnr,Ux,Uy,Uz,px,py,pz,e,Bx,By,Bz
  integer ix, iy, iz
  real fx, fy, k0
!
  k0 = 2.*pi/sx
  if (mpi_y == 0) then
    do iz=1,mz; do iy=1,lb; do ix=1,mx
      fx = exp(-10.*abs(ym(iy)))*cos(k1*xmdn(ix)*k0)*cos(k2*xmdn(ix)*k0)
      fy = 1./(1+exp(-2.*ym(iy)/dy1))
      px(ix,iy,iz) = fx*(ux1*(1-fy) + ux2*fy)
      fy = 1./(1+exp(-2.*ymdn(iy)/dy1))
      py(ix,iy,iz) = uy1*(1-fy) + uy2*fy
      pz(ix,iy,iz) = 0.
    end do; end do; end do
  end if

  if (mpi_y == mpi_ny-1) then
   do iz=1,mz; do iy=ub,my; do ix=1,mx
     fx = exp(-10.*abs(ym(iy)))*cos(k1*xmdn(ix)*k0)*cos(k2*xmdn(ix)*k0)
     fy = 1./(1+exp(-2.*ym(iy)/dy1))
     px(ix,iy,iz) = fx*(ux1*(1-fy) + ux2*fy)
      fy = 1./(1+exp(-2.*ymdn(iy)/dy1))
     py(ix,iy,iz) = uy1*(1-fy) + uy2*fy
     pz(ix,iy,iz) = 0.
   end do; end do; end do
  end if
END SUBROUTINE

!-----------------------------------------------------------------------
SUBROUTINE density_boundary_log (lnr,r)
  USE params
  implicit none
  real, dimension(mx,my,mz):: lnr,r
!
  if (mpi_y == 0) lnr(:, 1:lb,:) = alog(r(:, 1:lb,:))
  if (mpi_y == mpi_ny-1) lnr(:,ub:my,:) = alog(r(:,ub:my,:))
END

!-----------------------------------------------------------------------
SUBROUTINE energy_boundary (r,lnr,Ux,Uy,Uz,e,ee,p,Bx,By,Bz)
  USE params
  implicit none
  real, dimension(mx,my,mz):: r,lnr,Ux,Uy,Uz,e,ee,p,Bx,By,Bz

  if (mpi_y == 0) e (:, 1:lb,:) = 1./(gamma-1.)
  if (mpi_y == mpi_ny-1) e (:,ub:my,:) = 1./(gamma-1.)
END

!-----------------------------------------------------------------------
SUBROUTINE passive_boundary (r,Ux,Uy,Uz,d,dd)
  USE params
  implicit none
  real, dimension(mx,my,mz):: r,Ux,Uy,Uz,d,dd
END

!-----------------------------------------------------------------------
SUBROUTINE viscosity_boundary (nu, scr)
  USE params
  implicit none
  real, dimension(mx,my,mz):: nu, scr
END

!-----------------------------------------------------------------------
SUBROUTINE mfield_boundary (Ux,Uy,Uz,Bx,By,Bz)
  USE params
  implicit none
  real, dimension(mx,my,mz):: Ux,Uy,Uz,Bx,By,Bz
END

!-----------------------------------------------------------------------
SUBROUTINE ecurrent_boundary (Ex, Ey, Ez, Jx, Jy, Jz)
  USE params
  implicit none
  real, dimension(mx,my,mz):: Ex, Ey, Ez, Jx, Jy, Jz
END

!-----------------------------------------------------------------------
SUBROUTINE  efield_boundary (Ex, Ey, Ez, Bx_y, Bx_z, By_z, By_x, Bz_x, Bz_y, &
                             Rx, Ry, Rz, Ux_y, Ux_z, Uy_z, Uy_x, Uz_x, Uz_y, &
                             Ux, Uy, Uz)
  USE params
  implicit none
  real, dimension(mx,my,mz):: Ex, Ey, Ez, Bx_y, Bx_z, By_z, By_x, Bz_x, Bz_y, &
                              Rx, Ry, Rz, Ux_y, Ux_z, Uy_z, Uy_x, Uz_x, Uz_y, &
                              Ux, Uy, Uz
END

!-----------------------------------------------------------------------
SUBROUTINE ddt_boundary (r,px,py,pz,e,p,Bx,By,Bz, &
                         drdt,dpxdt,dpydt,dpzdt, &
                         dedt,dBxdt,dBydt,dBzdt)
  USE params
  implicit none
  real, dimension(mx,my,mz):: r,px,py,pz,e,p,Bx,By,Bz, &
                              drdt,dpxdt,dpydt,dpzdt, &
                              dedt,dBxdt,dBydt,dBzdt
  if (mpi_y == 0) then
    drdt(:,1:lb,:) = 0.
    dedt(:,1:lb,:) = 0.
    dpxdt(:,1:lb,:) = 0.
    dpzdt(:,1:lb,:) = 0.
    dpydt(:,1:lb,:) = 0.
  end if
  if (mpi_y == mpi_ny-1) then
    drdt(:,ub:my,:) = 0.
    dedt(:,ub:my,:) = 0.
    dpxdt(:,ub:my,:) = 0.
    dpzdt(:,ub:my,:) = 0.
    dpydt(:,ub+1:my,:) = 0.
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE regularize (f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
!
  call symmetric_center (f)
END

!-----------------------------------------------------------------------
SUBROUTINE regularize_face (f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
!
  call symmetric_face (f)
END
