! $Id: eos.f90,v 1.1 2010/10/08 09:28:53 aake Exp $
!***********************************************************************
module eos
  use params
  implicit none
  real, allocatable, dimension(:,:):: pbot0, dlnpdE_r
  logical do_table
  real frac
end module eos

!***********************************************************************
  subroutine init_eos
    USE params
    USE units
    USE eos
    character(len=mid):: id="$Id: eos.f90,v 1.1 2010/10/08 09:28:53 aake Exp $"
    call print_id (id)
    u_temp = 1.
    do_energy = .false.
    do_ionization = .false.
    frac = 0.1
    call read_eos
    allocate (pbot0(mx,mz), dlnpdE_r(mx,mz))
  end subroutine

!***********************************************************************
  subroutine read_eos
    USE params
    USE units
    USE eos
    namelist /eqofst/ frac
    if (stdin.ge.0) then
      rewind (stdin); read (stdin,eqofst)
    else
      read (*,eqofst)
    end if
    if (master) write (*,eqofst)
  end subroutine

!***********************************************************************
!  real function eos_iterations(clear)
!  USE params
!  USE units
!  USE eos
!  integer clear
!!
!  eos_iterations=0.0
!  end function
!
!***********************************************************************
  subroutine pressure (r,ee,pp)
    USE params
    USE eos
    USE units
    implicit none
    real, dimension(mx,my,mz):: r,ee
    real, dimension(mx,my,mz):: pp
    integer:: iz
!
    pp = r*0.01*(frac+1./(1.+r**1.5))
  end subroutine

!***********************************************************************
  subroutine temperature (r,ee,tt)
    USE params
    USE eos
    USE units
    implicit none
    real, dimension(mx,my,mz):: r,ee
    real, dimension(mx,my,mz):: tt
!
    tt = 0.01*(frac+1./(1.+r**1.5))*u_temp
  end subroutine

!***********************************************************************
  subroutine lookup (rho,ee,lnrk,lnsource)
  USE params
  implicit none
  real, dimension(mx,my,mz):: rho,ee,lnrk,lnsource

  end subroutine
