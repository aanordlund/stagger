!  $Id: potential_lower.f90,v 1.1 2009/03/02 14:10:48 aake Exp $
!***********************************************************************
MODULE potential_lower_m
  USE params
  implicit none
  real(kind=8), allocatable, dimension(:)     :: bxh, bzh
  real,         allocatable, dimension(:,:)   :: fy, fh, kh, byt
  real,         allocatable, dimension(:,:)   :: bt, bf
  logical, save:: first_time=.true.
END MODULE

!***********************************************************************
SUBROUTINE init_potential_lower
  USE potential_lower_m
  implicit none
!-----------------------------------------------------------------------
!$omp barrier
!$omp master
  allocate (bxh(my), bzh(my))
  allocate (fy(mx,mz), fh(mx,mz), kh(mx,mz), byt(mx,mz))
  allocate (bt(mx,mz), bf(mx,mz))
  first_time = .false.
!$omp end master
!$omp barrier
  !call test_fft2d
END

!***********************************************************************
SUBROUTINE dealloc_potential_lower
  USE potential_lower_m
  implicit none
!-----------------------------------------------------------------------
!$omp barrier
!$omp master
  deallocate (bxh, bzh)
  deallocate (fy, fh, kh, byt, bt, bf)
  first_time = .true.
!$omp end master
END

!----------------------------------------------------------------------
SUBROUTINE potential_lower (bx, by, bz, lb1)
  USE params
  implicit none
  real, dimension(mx,my,mz):: bx, by, bz
  integer lb1
  logical omp_in_parallel
!
  if (omp_in_parallel()) then
    call potential_lower_omp (bx, by, bz, lb1)
  else
    !$omp parallel shared(lb1)
    call potential_lower_omp (bx, by, bz, lb1)
    !$omp end parallel
  end if
END

!----------------------------------------------------------------------
SUBROUTINE potential_lower_omp (bx, by, bz, lb1)
!
!  Potential field extrapolation into the ghost zones.
!
!  A potential field, periodic in x and z, is exponentially
!  decreasing in the direction away from the boundary, with
!  amplitude factors exp(-y*k_xz) for each Fourier component.
!
!  Here we assume that 
!    Bx(i,j,iz) is centered at (i-1/2,j,iz),
!    By(i,j,iz) is centered at (i,j-1/2,iz),
!    Bz(i,j,iz) is centered at (i,j,iz-1/2).
!
!  If Bx and Bz were integer centered they would be exactly
!  pi/4 out of phase horizontally relative to By.  Since they
!  are half zone centered, a zero point that should be at j
!  is really at j+0.5, so we need to add that phase factor.
!
!  The extrapolation is based on By(:,lb+1,:), which is centered 
!  at (ix,lb+1/2,iz).  The first layer above this is the one
!  with Bx(:,lb,:), which is centered at (ix,lb,iz). 
!
  USE params
  USE potential_lower_m
  implicit none
  integer:: ix, iy, iz, lb1
  real:: asin, acos, kx(mx), kz(mz)
  real, dimension(mx,my,mz):: bx, by, bz

!  Horizontal Fourier transform of the vertical boundary field
!  fft2df uses srfttf which return the sin transform in odd terms
!  and the cos transforms in the even terms.

  if (mpi_y > 0) return
  if (first_time) call init_potential_lower

  bf(:,izs:ize) = by(:,lb1+1,izs:ize)
  call fft2df (bf, byt, mx, mz)

  call haverage_bdry (bx, bxh)
  call haverage_bdry (bz, bzh)
  do iz=1,mz
    kz(iz) = 2.*pi/sz*((iz+mpi_z*mz)/2)
  end do

!  Potential extrapolation

  do iy=lb1,1,-1

!  Y component

    do iz=izs,ize
      do ix=1,mx
        kx(ix)   = 2.*pi/sx*((ix+mpi_x*mx)/2)
        kh(ix,iz) = sqrt(kx(ix)**2+kz(iz)**2) + 1e-30
        bt(ix,iz) = byt(ix,iz)*exp((ymdn(iy)-ymdn(lb1+1))*kh(ix,iz))
      end do
    end do
    call fft2db (bt, bf, mx, mz)
    do iz=izs,ize
      do ix=1,mx
        by(ix,iy,iz) = bf(ix,iz)
      end do
    end do

!  X component

   if (iy <= lb1) then
    do iz=izs,ize
      bt(1 ,iz) = 0.
      bt(mx,iz) = 0.
      fh(1 ,iz) = exp((ym(iy)-ymdn(lb1+1))*kh(1 ,iz))/kh(1 ,iz)
      fh(mx,iz) = exp((ym(iy)-ymdn(lb1+1))*kh(mx,iz))/kh(mx,iz)
      do ix=3,mx,2
        acos = cos(0.5*kx(ix)*dx)
        asin = sin(0.5*kx(ix)*dx)
        fh(ix-1,iz) = exp((ym(iy)-ymdn(lb1+1))*kh(ix-1,iz))/kh(ix-1,iz)
        fh(ix  ,iz) = exp((ym(iy)-ymdn(lb1+1))*kh(ix  ,iz))/kh(ix  ,iz)
        bt(ix-1,iz) = -kx(ix)*fh(ix,iz)*(acos*byt(ix  ,iz)-asin*byt(ix-1,iz))
        bt(ix  ,iz) =  kx(ix)*fh(ix,iz)*(acos*byt(ix-1,iz)+asin*byt(ix  ,iz))
      end do
    end do
    call fft2db (bt, bf, mx, mz)
    do iz=izs,ize
      do ix=1,mx
        bx(ix,iy,iz) = bf(ix,iz) + bxh(lb1)
      end do
    end do

!  Z component

    do iz=izs,ize
      if (iz.eq.1) then
        bt(:, 1) = 0.
      else if (iz.eq.mz) then
        bt(:,mz) = 0.
      else if (mod(iz,2).eq.0) then
        acos = cos(0.5*kz(iz+1)*dz)
        asin = sin(0.5*kz(iz+1)*dz)
        bt(:,iz) = -kz(iz+1)*fh(:,iz+1)*(acos*byt(:,iz+1)-asin*byt(:,iz))
      else if (mod(iz,2).eq.1) then
        acos = cos(0.5*kz(iz)*dz)
        asin = sin(0.5*kz(iz)*dz)
        bt(:,iz) =  kz(iz  )*fh(:,iz  )*(acos*byt(:,iz-1)+asin*byt(:,iz))
      end if
    end do
    call fft2db (bt, bf, mx, mz)
    do iz=izs,ize
      do ix=1,mx
        bz(ix,iy,iz) = bf(ix,iz) + bzh(lb1)
      end do
    end do
   end if
  end do
  !print *,'bxh=',bxh(1:lb1)
  !print *,'bzh=',bzh(1:lb1)

  !call dealloc_potential_lower
END
