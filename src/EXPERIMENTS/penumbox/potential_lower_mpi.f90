!  $Id: potential_lower_mpi.f90,v 1.1 2009/03/02 14:10:48 aake Exp $
!***********************************************************************
MODULE potential_lower_m
  USE params
  implicit none
  real(kind=8), allocatable, dimension(:)     :: bxh, bzh
  real,         allocatable, dimension(:,:)   :: fy, fh, kh, byt
  real,         allocatable, dimension(:,:)   :: bt, bf
  logical, save:: first_time=.true.
END MODULE

!***********************************************************************
SUBROUTINE init_potential_lower
  USE potential_lower_m
  implicit none
!-----------------------------------------------------------------------
!$omp barrier
!$omp master
  allocate (bxh(my), bzh(my))
  allocate (fy(mx,mz), fh(mx,mz), kh(mx,mz), byt(mx,mz))
  allocate (bt(mx,mz), bf(mx,mz))
  first_time = .false.
!$omp end master
!$omp barrier
  call test_fft2d
END

!***********************************************************************
SUBROUTINE dealloc_potential_lower
  USE potential_lower_m
  implicit none
!-----------------------------------------------------------------------
!$omp barrier
!$omp master
  deallocate (bxh, bzh)
  deallocate (fy, fh, kh, byt, bt, bf)
  first_time = .true.
!$omp end master
END

!----------------------------------------------------------------------
SUBROUTINE potential_lower (bx, by, bz, lb1)
  USE params
  implicit none
  real, dimension(mx,my,mz):: bx, by, bz
  integer lb1
  logical omp_in_parallel
!
  if (omp_in_parallel()) then
    call potential_lower_omp (bx, by, bz, lb1)
  else
    !$omp parallel shared(lb1)
    call potential_lower_omp (bx, by, bz, lb1)
    !$omp end parallel
  end if
END

!----------------------------------------------------------------------
SUBROUTINE potential_lower_omp (bx, by, bz, lb1)
!
!  Potential field extrapolation into the ghost zones.
!
!  A potential field, periodic in x and z, is exponentially
!  decreasing in the direction away from the boundary, with
!  amplitude factors exp(-y*k_xz) for each Fourier component.
!
!  Here we assume that 
!    Bx(i,j,iz) is centered at (i-1/2,j,iz),
!    By(i,j,iz) is centered at (i,j-1/2,iz),
!    Bz(i,j,iz) is centered at (i,j,iz-1/2).
!
!  If Bx and Bz were integer centered they would be exactly
!  pi/4 out of phase horizontally relative to By.  Since they
!  are half zone centered, a zero point that should be at j
!  is really at j+0.5, so we need to add that phase factor.
!
!  The extrapolation is based on By(:,lb+1,:), which is centered 
!  at (ix,lb+1/2,iz).  The first layer above this is the one
!  with Bx(:,lb,:), which is centered at (ix,lb,iz). 
!
  USE params
  USE arrays
  USE potential_lower_m
  implicit none
  integer:: ix, iy, iz, lb1
  real:: asin, acos, kx(mx), kz(mz)
  real, dimension(mx,my,mz):: bx, by, bz

!  Horizontal Fourier transform of the vertical boundary field
!  fft2df uses srfttf which return the sin transform in odd terms
!  and the cos transforms in the even terms.

  if (mpi_y > 0) return
  if (first_time) call init_potential_lower

  bf(:,izs:ize) = by(:,lb1+1,izs:ize)
  call fft2df (bf, byt, mx, mz)

  call haverage_bdry (bx, bxh)
  call haverage_bdry (bz, bzh)

  do ix=1,mx
    kx(ix) = 2.*pi/sx*((ix+mpi_x*mx)/2)
  end do
  do iz=1,mz
    kz(iz) = 2.*pi/sz*((iz+mpi_z*mz)/2)
    do ix=1,mx
      kh(ix,iz) = sqrt(kx(ix)**2+kz(iz)**2) + 1e-30
    end do
  end do

!  Potential Y component

  do iy=1,lb1
    do iz=izs,ize
    do ix=1,mx
      bt(ix,iz) = byt(ix,iz)*exp((ymdn(iy)-ymdn(lb1+1))*kh(ix,iz))
    end do
    end do
    call fft2db (bt, bf, mx, mz)
    by(:,iy,izs:ize) = bf(:,izs:ize)
  end do

!  Potential

  if(mpi_x==0.and.mpi_z==0) kh(1,1)=1.
  do iy=1,lb1
    do iz=izs,ize
    do ix=1,mx
      bt(ix,iz) = byt(ix,iz)*exp((ym(iy)-ymdn(lb1+1))*kh(ix,iz))/kh(ix,iz)
    end do
    end do
    if(mpi_x==0.and.mpi_z==0) bt(1,1)=0.
    call fft2db (bt, bf, mx, mz)
    scr1(:,iy,izs:ize) = bf(:,izs:ize)
  end do

!  Horizontal field components from the potential

  call ddxdn_set (scr1, scr2)
  call ddzdn_set (scr1, scr3)
  do iy=1,lb1-1
    bx(:,iy,izs:ize) = scr2(:,iy,izs:ize) + bxh(lb1)
    bz(:,iy,izs:ize) = scr3(:,iy,izs:ize) + bzh(lb1)
  end do
END
