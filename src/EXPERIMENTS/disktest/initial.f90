! $Id: initial.f90,v 1.3 2007/06/22 21:34:23 aake Exp $
!************************************************************************
FUNCTION input_file ()
  USE params
  character(len=mfile):: input_file
  input_file = 'EXPERIMENTS/disktest/input.txt'
END

!************************************************************************
SUBROUTINE init_values (r,px,py,pz,e,d,Bx,By,Bz)
  USE params
  USE forcing
  implicit none
  real, dimension(mx,my,mz):: r,px,py,pz,e,d,Bx,By,Bz
  real, dimension(3):: B0, v0, v2
  real r0, kx, ky, kz, expz, expp, xp, yp, zp, v1, theta, xpdn, ypdn, zpdn
  integer ix, iy, iz
  namelist /init/ r0, B0, v0, v1, v2, theta

  r0 = 1.
  B0 = 0.
  v0 = 0.
  v1 = 0.5
  v2 = 0.5
  theta = 0.

  write(*,*) hl
  rewind (stdin); read (stdin,init)
  if (master) write(*,init)

  kx = 2.*pi/sx
  ky = 2.*pi/sy
  kz = 2.*pi/sz

  do iz=1,mz
  do iy=1,my
  do ix=1,mx
    if (conservative) then
      r(ix,iy,iz) = r0
      e(ix,iy,iz) = r0
    else
      r(ix,iy,iz) = alog(r0)
      e(ix,iy,iz) = 1.
    endif
    xp = xm(ix)*cos(theta)+zm(iz)*sin(theta)
    yp = ym(iy)
    zp = zm(iz)*cos(theta)-xm(ix)*sin(theta)
    xpdn = xmdn(ix)*cos(theta)+zmdn(iz)*sin(theta)
    ypdn = ymdn(iy)
    zpdn = zmdn(iz)*cos(theta)-xmdn(ix)*sin(theta)
    expz=exp(-6.*zp**2)
    expp=exp(-6.*(xpdn**2+yp**2+zp**2))
    px(ix,iy,iz) = +v1*cos(theta)*sin(ky*yp)*expz - v2(1)*sin(kx*xpdn)*expp + v0(1)
    expp=exp(-6.*(xp**2+ypdn**2+zp**2))
    py(ix,iy,iz) = -v1*           sin(kx*xp)*expz - v2(2)*sin(ky*ypdn)*expp + v0(2)
    expp=exp(-6.*(xp**2+yp**2+zpdn**2))
    pz(ix,iy,iz) = +v1*sin(theta)*sin(ky*yp)*expz - v2(3)*sin(kz*zpdn)*expp + v0(3)
    if (do_mhd) then
      bx(ix,iy,iz) = b0(1)
      by(ix,iy,iz) = b0(2)
      bz(ix,iy,iz) = b0(3)
    end if
  end do
  end do
  end do

END
