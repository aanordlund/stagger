!-----------------------------------------------------------------------
subroutine calc_hav(r,Ux,Uy,Uz,e,p,px,py,pz,Bx,By,Bz,Ex,Ey,Ez)
  
  ! calculates horizontal averages of various variables

  use params
  use arrays, only: rhoav, eav, lnrav, eeav, pav, ttav, &
       pxav, pyav, pzav, Uxav, Uyav, Uzav, &
       Exav, Eyav, Ezav, Bxav, Byav, Bzav, &
       pyavUp, pyavDn, ffpyavUp, ffpyavDn, &
       rhominh, rhomaxh, eeminh, eemaxh, ttminh, ttmaxh, pminh, pmaxh, &
       fvisc, fpoynt, frad, fkin, fconv, fei, facous, qqav, qqrav, &
       Pelav, Rossav, Plckav, Stdav

  implicit none

  character(len=mid), save:: id='$Id: hav.f90,v 1.17 2015/11/02 04:54:57 remo Exp $'
  real, dimension(mx,my,mz):: r,Ux,Uy,Uz,e,p,px,py,pz,Bx,By,Bz,Ex,Ey,Ez
  real, allocatable, dimension(:,:,:) :: lnr,ee,tt,Pel,lnRoss,lnPlck,lnStd
  logical foutput
  integer iz

  call print_id(id)

  if ( .not.foutput('fluxes.txt') ) return


  ! allocate arrays for auxiliary variables
  allocate ( lnr(mx,my,mz), ee(mx,my,mz), tt(mx,my,mz), Pel(mx,my,mz) )
  allocate ( lnRoss(mx,my,mz), lnPlck(mx,my,mz), lnStd(mx,my,mz) )
  
  ! calculate internal energy per unit mass
  do iz=izs,ize
     lnr(:,:,iz) = alog( r(:,:,iz) )
     ee (:,:,iz) = e(:,:,iz) / r(:,:,iz)
  end do

  ! look up auxiliary quantities
  call temperature( r, ee, tt )
  call elpressure ( r, ee, Pel )
  call rosseland  ( r, tt, lnRoss )
  call chiplanck  ( r, tt, lnPlck )
  call chiStd     ( r, tt, lnStd  )


  call haverage_subr ( r,  rhoav)
  call haverage_subr ( e,  eav  )
  call haverage_subr ( lnr,lnrav)
  call haverage_subr ( ee, eeav )
  call haverage_subr ( p,  pav  )
  call haverage_subr ( px, pxav )
  call haverage_subr ( py, pyav )
  call haverage_subr ( pz, pzav )
  call haverage_subr ( Ux, Uxav )
  call haverage_subr ( Uy, Uyav )
  call haverage_subr ( Uz, Uzav )
  call haverage_subr ( tt, ttav )
  call haverage_subr ( Pel,Pelav)
  call haverage_subr ( exp(lnRoss), Rossav )
  call haverage_subr ( exp(lnPlck), Plckav )
  call haverage_subr ( exp(lnStd),  Stdav  )

  if (do_mhd) then
     call haverage_subr ( Bx, Bxav )
     call haverage_subr ( By, Byav )
     call haverage_subr ( Bz, Bzav )
     call haverage_subr ( Ex, Exav )
     call haverage_subr ( Ey, Eyav )
     call haverage_subr ( Ez, Ezav )
  end if


  ! find minimum and maximum of density, energy per unit mass, temperature,
  ! and pressure on horizontal planes

  call horiz_minmax( r,  rhominh, rhomaxh )
  call horiz_minmax( ee, eeminh,  eemaxh  )
  call horiz_minmax( p,  pminh,   pmaxh   )
  call horiz_minmax( tt, ttminh,  ttmaxh  )

  call haver_updn( py, py, pyavUp, pyavDn, ffpyavUp, ffpyavDn)

  deallocate( lnRoss, lnPlck, lnStd )
  deallocate( lnr, ee, tt, Pel)

end subroutine calc_hav



!-----------------------------------------------------------------------
subroutine output_hav

! writes horizontal averages to output file

  use params, only: ym, ymdn, my, lb, ub, isnap, t, dt, do_scp, do_mhd, &
       mfile, mid, master, data_unit, file
  use arrays, only: rhoav, eav, lnrav, eeav, pav, ttav, &
       pxav, pyav, pzav, Uxav, Uyav, Uzav, &
       Exav, Eyav, Ezav, Bxav, Byav, Bzav, &
       pyavUp, pyavDn, ffpyavUp, ffpyavDn, &
       rhominh, rhomaxh, eeminh, eemaxh, ttminh, ttmaxh, pminh, pmaxh, &
       fvisc, fpoynt, frad, fkin, fconv, fei, facous, qqav, qqrav, &
       Pelav, Rossav, Plckav, Stdav
  use forcing,  only: gy
  use boundary, only: rbot, ebot
  use table, only: uur, uul, uut, uuv, uuee, uup, uuk, uubp

  implicit none

  character(len=mid), save :: id='$Id: hav.f90,v 1.17 2015/11/02 04:54:57 remo Exp $'
  character(len=mfile)     :: name, havfile

  ! Stefan-Boltzmann constant, cgs units
  real(kind=8), parameter  :: stefboltz = 5.6705d-5

  real(kind=8) :: teff, lnravbot, eeavbot, lnrbot, eebot
  real(kind=8) :: rhoavSum
  real(kind=8) :: pxavSum, uxcm
  real(kind=8) :: pyavSum, uycm
  real(kind=8) :: pzavSum, uzcm

  logical      :: foutput
  integer      :: iy, ksnap, nghost


  call print_id(id)

  ! if fluxes.txt not present, simply exit
  if (.not.foutput('fluxes.txt'))  return



  ! master process
  if (master) then

     ! define hav filename
     havfile = name('fluxes.txt','hav',file)

     ! call scp_name with ksnap = isnap; note that ksnap is then set to 0 by scp_name.
     ksnap = isnap
     call scp_name(havfile, ksnap)

     ! number of ghost-zones
     nghost = lb - 1


     ! effective temperature
     teff = ( -frad(lb)/stefboltz * uup * uuv ) ** 0.25

     ! lnr and ee, bottom boundary condition
     lnrbot = alog(rbot)
     eebot  = ebot/rbot
     
     ! lnr and ee, average value at bottom boundary
     lnravbot = lnrav(ub)
     eeavbot  =  eeav(ub)

     ! compute center of mass velocity
     rhoavSum = 0.0
     pxavSum  = 0.0
     pyavSum  = 0.0
     pzavSum  = 0.0
     do iy=lb,ub
        rhoavSum = rhoavSum + rhoav(iy)
        pxavSum  = pxavSum  +  pxav(iy)
        pyavSum  = pyavSum  +  pyav(iy)
        pzavSum  = pzavSum  +  pzav(iy)
     enddo
     uxcm = pxavSum / rhoavSum    ! center of mass velocity, x component
     uycm = pyavSum / rhoavSum    ! center of mass velocity, y component
     uzcm = pzavSum / rhoavSum    ! center of mass velocity, z component

     ! open hav file for writing
     print '(A32,A,A14,I5)', ' writing horizontal averages to ', trim( havfile ), ' for snapshot ', isnap
     if (do_scp) then
        open (data_unit, file=havfile, status='replace')
     else
        open (data_unit, file=havfile, status='unknown', position='append')
     endif

     ! write header in namelist format
     write (data_unit, fmt='(A)') '&haverages'
     write (data_unit, fmt='(3X,A10,I14,  A1)')  '   isnap =',  isnap,    ','
     write (data_unit, fmt='(3X,A10,I14,  A1)')  '    ndep =',  my,       ','
     write (data_unit, fmt='(3X,A10,F14.6,A1)')  '       t =',  t,        ','
     write (data_unit, fmt='(3X,A10,F14.6,A1)')  '      dt =',  dt,       ','	  
     write (data_unit, fmt='(3X,A10,I14,  A1)')  '  nghost =',  nghost,   ','
     write (data_unit, fmt='(3X,A10,E14.4,A1)')  '     uur =',  uur,      ','
     write (data_unit, fmt='(3X,A10,E14.4,A1)')  '     uul =',  uul,      ','
     write (data_unit, fmt='(3X,A10,E14.4,A1)')  '     uut =',  uut,      ','
     write (data_unit, fmt='(3X,A10,F14.4,A1)')  '    teff =',  teff,     ','
     write (data_unit, fmt='(3X,A10,F14.6,A1)')  '      gy =',  gy,       ','
     write (data_unit, fmt='(3X,A10,F14.6,A1)')  '  lnrbot =',  lnrbot,   ','
     write (data_unit, fmt='(3X,A10,F14.6,A1)')  '   eebot =',  eebot,    ','
     write (data_unit, fmt='(3X,A10,F14.6,A1)')  'lnravbot =',  lnravbot, ','
     write (data_unit, fmt='(3X,A10,F14.6,A1)')  ' eeavbot =',  eeavbot,  ','     
     write (data_unit, fmt='(3X,A10,F14.6,A1)')  '    uxcm =',  uxcm,     ','
     write (data_unit, fmt='(3X,A10,F14.6,A1)')  '    uycm =',  uycm,     ','
     write (data_unit, fmt='(3X,A10,F14.6,A1)')  '    uzcm =',  uzcm,     ' '
     write (data_unit, fmt='(A)') '/'


     ! write horizontal averages
     write (data_unit,fmt='(A6,8(X,A13))') &
          'k', 'ym', 'ymdn', 'rho', 'e', 'lnr', 'ee', 'tt', 'P'
     do iy=1,my
        write (data_unit,fmt='(I6,8(X,E13.5))') &
             iy, &
             ym(iy), ymdn(iy), rhoav(iy), eav(iy), &
             lnrav(iy), eeav(iy), ttav(iy), pav(iy)
     end do

     write (data_unit,'(A6,8(X,A13))') &
          'k', 'rhomin', 'rhomax', 'eemin', 'eemax', 'ttmin', 'ttmax', 'Pmin', 'Pmax'
     do iy=1,my
        write (data_unit,'(I6,8(X,E13.5))') &
             iy, &
             rhominh(iy), rhomaxh(iy), eeminh(iy), eemaxh(iy), &
             ttminh(iy), ttmaxh(iy), pminh(iy), pmaxh(iy)
     end do

     write (data_unit,'(A6,4(X,A13))') &
          'k', 'Pel', 'alphaR', 'alphaP', 'alphaS'
     do iy=1,my
        write (data_unit,'(I6,4(X,E13.5))') &
             iy,  &
             pelav(iy), Rossav(iy), Plckav(iy), Stdav(iy)
     end do

     write (data_unit,'(A6,10(X,A13))') &
          'k', 'px', 'py', 'pz', 'ux', 'uy', 'uz', 'pyup', 'pydn', 'ffpyup', 'ffpydn'
     do iy=1,my
        write (data_unit,'(I6,10(X,E13.5))') &
             iy, &
             pxav(iy), pyav(iy), pzav(iy), &
             uxav(iy), uyav(iy), uzav(iy), &
             pyavUp(iy), pyavDn(iy), &
             ffpyavUp(iy), ffpyavDn(iy)
     end do


     if (do_mhd) then
       write (data_unit,'(A6,9(X,A13))') &
             'k', 'fvisc', 'fkin', 'fconv', 'fei', 'facous', 'frad', 'qq', 'qqr','fpoynt'
        do iy=1,my
           write (data_unit,'(I6,9(X,E13.5))') &
                iy, &
                fvisc(iy), fkin(iy), fconv(iy), fei(iy), facous(iy), &
                frad(iy), qqav(iy), qqrav(iy), fpoynt(iy)
        end do
        write (data_unit,'(A6,6(X,A13))') &
             'k', 'Bx', 'By', 'Bz', 'Ex', 'Ey', 'Ez'
        do iy=1,my
           write (data_unit,'(I6,6(X,E13.5))') &
                iy, &
                Bxav(iy), Byav(iy), Bzav(iy), &
                Exav(iy), Eyav(iy), Ezav(iy)
        end do       
     else
        write (data_unit,'(A6,8(X,A13))') &
             'k', 'fvisc', 'fkin', 'fconv', 'fei', 'facous', 'frad', 'qq', 'qqr'
        do iy=1,my
           write (data_unit,'(I6,8(X,E13.5))') &
                iy, &
                fvisc(iy), fkin(iy), fconv(iy), fei(iy), facous(iy), &
                frad(iy), qqav(iy), qqrav(iy)
        end do
     endif
     
  end if

  return
  
end subroutine output_hav
