!-----------------------------------------------------------------------
module boundary

  use params
  implicit none

  real :: t_bdry, t_vbdry, t_Bbdry
  real :: Uy_bdry
  real :: Bscale_top, Bscale_bot
  real :: lb_eampl
  real :: eetop_frac
  real :: rbot
  real :: ebot
  real :: pbot
  real :: htop

  ! internal energy per unit mass, bottom bdry;
  ! ln pressure deriv with respect to ln energy, bottom bdry;
  ! ln pressure deriv with respect to ln density, bottom bdry
  real :: eebot
  real :: dlnpdlnee_r_bot 
  real :: dlnpdlnr_ee_bot

  ! p-mode driving: logical switch, relative amplitude, frequency, and phase
  logical :: do_pdrive
  real :: p_ampl, p_omega, p_phase
  
  real :: Bx0, By0, Bz0
  real :: Bx0_top, Bz0_top
  real :: rmin, rmax
  real :: pb_fact, uy_max
  real :: dBx0dt, dBy0dt, dBz0dt
  real :: pdamp
  real :: rtop_frac
  real :: fmtop_frac
  real :: divb_max
  real :: ampxy, ampyz, period, omegap, akx, aky, akz, kxp, kyp, kzp
  real(kind=8) :: rub, pyub, elb, eub, pub, pubt, dpydtub
  real(kind=8) :: pyubin, pyubout, iin, iout
  real, allocatable, dimension(:,:,:) :: Bxl, Byl, Bzl, Bxu, Byu, Bzu
  real, allocatable, dimension(:,:,:) :: Exl, Eyl, Ezl, Exu, Eyu, Ezu
  real :: eta_scl
  logical :: debug
  logical :: bdry_first
  logical :: Binflow
  logical :: do_eta
  integer :: verbose
  integer :: lb1
  integer :: eta_width

end module boundary

!-----------------------------------------------------------------------
subroutine init_boundary

  use params
  use arrays
  use boundary

  implicit none

  logical :: fexists

  character(len=mfile) :: name
  character(len=mid) :: IdStr = 'init_boundary'

  call print_id(IdStr)

  if (mpi_y == 0) then
     lb = 6
  else
     lb = 1
  end if
  if (mpi_y == mpi_ny-1) then
     ub = my-5
  else
     ub = my
  end if

  periodic(2) = .false.                                                 ! non-periodic y
  t_bdry = 0.05                                                         ! bdry decay time
  t_Bbdry  = 0.01                                                       ! bdry decay time for B
  t_vbdry = 0.                                                          ! bdry decay time for Vinflow
  eetop_frac = 1.                                                       ! fraction of current eetop
  eetop = -1.                                                           ! signal setting eetop
  rtop_frac=0.05                                                        ! fraction of current rtop
  fmtop_frac=1.0                                                        ! fraction fmass correction for rho(lb)
  pdamp = 0.
  Uy_bdry = 0.1
  lb_eampl = 0.
  rbot = -1.
  ebot = -1.

  do_pdrive = .false.                                                   ! p-mode driving, logical switch
  p_ampl = 0.                                                           ! amplitude of pressure drive, relative to pbot
  p_omega = 0.                                                          ! pulsational frequency of p-mode driving  
  p_phase = 0.                                                          ! phase of p-mode driving ( sin(pomega*t+pphase) )

  htop = 0.
  rmin = 1e-3
  rmax = 1e1
  pb_fact = 1.0
  uy_max = 2.                                                           ! 20 km/s with solar scaling
  Bscale_top = 10
  Bscale_bot = 10
  Bx0 = 0.
  By0 = 0.
  Bz0 = 0.
  Bx0_top = 0.
  Bz0_top = 0.
  Binflow = .true.
  debug = .false.
  verbose = 0
  bdry_first = .true.
  do_stratified = .true.
  ampxy = 0.
  ampyz = 0.
  period = 0.
  akx = 1.
  aky = 1.
  akz = 1.
  do_eta = .false.
  eta_width = 9
  eta_scl = 3.

  call read_boundary
    
  if (mpi_y == 0) then
     allocate (Bxl(mx,lb,mz),Byl(mx,lb,mz),Bzl(mx,lb,mz))
  end if

  if (mpi_y == mpi_ny-1) then
     allocate (Bxu(mx,my-ub,mz),Byu(mx,my-ub,mz),Bzu(mx,my-ub,mz))
  end if

  if (.not. master) debug=.false.
end subroutine init_boundary

!-----------------------------------------------------------------------
subroutine read_boundary
  use params
  use boundary
  implicit none
  namelist /bdry/ lb, ub, t_bdry, t_vbdry, t_Bbdry, &
       Bscale_top, Bscale_bot, eetop_frac, lb_eampl, htop, &
       rtop_frac, fmtop_frac, eetop, pdamp, rbot, ebot, &
       do_pdrive, p_ampl, p_omega, p_phase, Bx0, By0, Bz0, &
       Bx0_top, Bz0_top, Binflow, Uy_bdry, do_stratified, &
       rmin, rmax, pb_fact, uy_max, debug, verbose, &
       ampxy, ampyz, period, akx, aky, akz, &
       do_eta, eta_width, eta_scl

  rewind (stdin); read (stdin,bdry)
  if (master) write (*,bdry)
end subroutine read_boundary

!-----------------------------------------------------------------------
subroutine regularize (f)
  use params
  implicit none
  real, dimension(mx,my,mz):: f
  !
  !if (do_trace) print *,'regularize 1:',mpi_rank,omp_mythread,mpi_y
  if (mpi_y ==        0) call symmetric_center_lower (f)
  if (mpi_y == mpi_ny-1) call symmetric_center_upper (f)
  !if (do_trace) print *,'regularize 2:',mpi_rank,omp_mythread,mpi_y
end subroutine regularize

!-----------------------------------------------------------------------
subroutine regularize_face (f)
  use params
  implicit none
  real, dimension(mx,my,mz):: f
  !
  if (mpi_y ==        0) call symmetric_face_lower (f)
  if (mpi_y == mpi_ny-1) call symmetric_face_upper (f)
end subroutine regularize_face

!-----------------------------------------------------------------------
subroutine viscosity_boundary (nu, cs)
  use params
  implicit none
  real, dimension(mx,my,mz):: nu, cs
  integer iz

  call viscosity_boundary_top (nu, cs)
  call viscosity_boundary_bot (nu, cs)
end subroutine viscosity_boundary

!-----------------------------------------------------------------------
subroutine density_boundary (r,lnr,py,e)
  use params
  implicit none
  real, dimension(mx,my,mz):: r,lnr,py,e

  call density_boundary_top (r,lnr,py,e)
  call density_boundary_bot (r,lnr,py,e)
end subroutine density_boundary

!-----------------------------------------------------------------------
subroutine energy_boundary (r,lnr,Ux,Uy,Uz,e,ee,p,Bx,By,Bz)
  use params
  implicit none
  real, dimension(mx,my,mz):: r,lnr,Ux,Uy,Uz,e,ee,p,Bx,By,Bz

  call energy_boundary_top (r,lnr,Ux,Uy,Uz,e,ee,p,Bx,By,Bz)
  call energy_boundary_bot (r,lnr,Ux,Uy,Uz,e,ee,p,Bx,By,Bz)
end subroutine energy_boundary

!-----------------------------------------------------------------------
subroutine passive_boundary (r,Ux,Uy,Uz,d,dd)
  use params
  implicit none
  real, dimension(mx,my,mz):: r,Ux,Uy,Uz,d,dd

  call passive_boundary_top (r,Ux,Uy,Uz,d,dd)
  call passive_boundary_bot (r,Ux,Uy,Uz,d,dd)
end subroutine passive_boundary

!-----------------------------------------------------------------------
subroutine velocity_boundary (r,xdnr,ydnr,zdnr,Ux,Uy,Uz,px,py,pz,e,Bx,By,Bz)
  use params
  implicit none
  real, dimension(mx,my,mz):: r,xdnr,ydnr,zdnr,Ux,Uy,Uz,px,py,pz,e,Bx,By,Bz

  call velocity_boundary_top (r,xdnr,ydnr,zdnr,Ux,Uy,Uz,px,py,pz,e,Bx,By,Bz)
  call velocity_boundary_bot (r,xdnr,ydnr,zdnr,Ux,Uy,Uz,px,py,pz,e,Bx,By,Bz)
end subroutine velocity_boundary

!-----------------------------------------------------------------------
subroutine magnetic_pressure_boundary (BB)
  use params
  implicit none
  real, dimension(mx,my,mz):: BB

  !call magnetic_pressure_boundary_top (BB)
  call magnetic_pressure_boundary_bot (BB)
end subroutine magnetic_pressure_boundary

!-----------------------------------------------------------------------
subroutine mfield_boundary (Ux,Uy,Uz,Bx,By,Bz)
  use params
  implicit none
  real, dimension(mx,my,mz):: Ux,Uy,Uz,Bx,By,Bz

  call mfield_boundary_top (Ux,Uy,Uz,Bx,By,Bz)
  call mfield_boundary_bot (Ux,Uy,Uz,Bx,By,Bz)
end subroutine mfield_boundary

!-----------------------------------------------------------------------
subroutine ecurrent_boundary (Ex, Ey, Ez, Jx, Jy, Jz)
  use params
  implicit none
  real, dimension(mx,my,mz):: Ex, Ey, Ez, Jx, Jy, Jz

  call ecurrent_boundary_top (Ex, Ey, Ez, Jx, Jy, Jz)
  call ecurrent_boundary_bot (Ex, Ey, Ez, Jx, Jy, Jz)
end subroutine ecurrent_boundary

!-----------------------------------------------------------------------
subroutine efield_boundary (Ex, Ey, Ez, Bx_y, Bx_z, By_z, By_x, Bz_x, Bz_y, &
     Bx, By, Bz, Ux_y, Ux_z, Uy_z, Uy_x, Uz_x, Uz_y, &
     Ux, Uy, Uz)
  use params
  implicit none
  real, dimension(mx,my,mz):: Ex, Ey, Ez, Bx_y, Bx_z, By_z, By_x, Bz_x, Bz_y, &
       Ux_y, Ux_z, Uy_z, Uy_x, Uz_x, Uz_y, &
       Ux, Uy, Uz, Bx, By, Bz

  call efield_boundary_top (Ex, Ey, Ez, Bx_y, Bx_z, By_z, By_x, Bz_x, Bz_y, &
       Bx, By, Bz, Ux_y, Ux_z, Uy_z, Uy_x, Uz_x, Uz_y, &
       Ux, Uy, Uz)
  call efield_boundary_bot (Ex, Ey, Ez, Bx_y, Bx_z, By_z, By_x, Bz_x, Bz_y, &
       Bx, By, Bz, Ux_y, Ux_z, Uy_z, Uy_x, Uz_x, Uz_y, &
       Ux, Uy, Uz)
end subroutine efield_boundary

!-----------------------------------------------------------------------
subroutine ddt_boundary (r,px,py,pz,e,p,Bx,By,Bz, &
     drdt,dpxdt,dpydt,dpzdt, &
     dedt,dBxdt,dBydt,dBzdt)
  use params
  implicit none
  real, dimension(mx,my,mz):: r,px,py,pz,e,p,Bx,By,Bz, &
       drdt,dpxdt,dpydt,dpzdt, &
       dedt,dBxdt,dBydt,dBzdt

  call ddt_boundary_top (r,px,py,pz,e,p,Bx,By,Bz, &
       drdt,dpxdt,dpydt,dpzdt, &
       dedt,dBxdt,dBydt,dBzdt)
  call ddt_boundary_bot (r,px,py,pz,e,p,Bx,By,Bz, &
       drdt,dpxdt,dpydt,dpzdt, &
       dedt,dBxdt,dBydt,dBzdt)
end subroutine ddt_boundary
