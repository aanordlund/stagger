!***********************************************************************
SUBROUTINE transfer( np, n1, dtau, s, q, surfi, do_surfi )

  USE params, only: mid, mx, my, mz
  IMPLICIT none

  character(len=mid):: id='$Id: transfer_feautrier_mpi.f90,v 1.1 2016/10/12 13:08:48 remo Exp $'

  integer  np, n1
  real, dimension(mx,my,mz), intent(in)  :: dtau, s
  real, dimension(mx,my,mz), intent(out) :: q
  real, dimension(mx,mz),    intent(out) :: surfi

  logical do_surfi, omp_in_parallel

  if (omp_in_parallel()) then
     call transq_feautrier_mpi (dtau, s, q, surfi, do_surfi )
  else
     !$omp parallel
     call transq_feautrier_mpi (dtau, s, q, surfi, do_surfi )
     !$omp end parallel
  end if

END SUBROUTINE transfer


!***********************************************************************
SUBROUTINE transq_feautrier_mpi( dtau, s, q, surfi, do_surfi )

  ! Solve the transfer equation, given optical depth and source function.
  !
  ! This routine uses an MPI implementation of the 'transq' second-order
  ! Feautrier solver; 
  ! the radiative transfer equation is solved for Q=P-S at all depths
  ! (P corresponds to the Feautrier's variable for (IPlus+IMinus) / 2 .)
  ! 
  ! "Steins trick" is used; storing the sum of the three elements in each
  ! row of the tridiagonal equation system, instead of the diagonal ele-
  ! ment.  This allows accurate solutions to be otained for arbitrarily
  ! small optical depths.  Without this trick, round off errors become
  ! noticeable already when the smallest optical depth is less than the
  ! square root of the machine precisions.
  !
  ! This is a second order version.  For simulations, with rapidly
  ! varying absorption coefficients and source functions, this is to be
  ! preferred over spline and Hermitean versions because it is positive
  ! definite, in the sense that a positive source function is guaranteed
  ! to result in a positive average intensity p.  Also, the flux
  ! divergence is exactly equal to q, for the conventional definition
  ! of the flux.

  use params, only: mid, mx, my, mz, mpi_y, mpi_ny, izs, ize, isubstep

  implicit none

  character(len=mid):: id='$Id: transfer_feautrier_mpi.f90,v 1.1 2016/10/12 13:08:48 remo Exp $'

  real, dimension(mx,my,mz), intent(in)          :: dtau, s
  real, dimension(mx,my,mz), intent(out), target :: q
  real, dimension(mx,mz),    intent(out)         :: surfi
  logical                                        :: do_surfi


  real        , allocatable, target, dimension(:,:,:)   :: scratch0, scratch8
  real(kind=8), allocatable, target, dimension(:,:,:)   :: scratch1, scratch2, scratch3, scratch4, scratch5, scratch6
  real        , allocatable, target, dimension(:,:,:,:) :: scratch7

  real        , pointer,     dimension(:,:,:)   :: dsdtau, d2sdtau2, yterm, zterm
  real(kind=8), pointer,     dimension(:,:,:)   :: sp1, sp2, sp3, qcorr, dside, fside, gside
  real        , pointer,     dimension(:,:,:,:) :: buffer

  real, dimension(mx,1,mz) :: dtauN1          ! buffer containing dtau(n+1)=tau(n+1)-tau(n)
  real, dimension(mx,1,mz) :: s0, sN1         ! buffer containing source function for layers 0 and n+1

  real, dimension(mx)      :: dinv            ! buffer used to store 1 / (dtau(n+1)+dtau(n))
  real                     :: taum

  real, dimension(mx,mz)   :: ex0, ex1

  integer   :: ix, iy, iz 
  integer   ::  j, j1, j2
  integer   :: omp_get_thread_num


  call print_id (id)


  ! Allocate temporary arrays;
 
  ! To make a more efficient usage of resources, some arrays share the same memory area

  allocate( scratch0(mx,my,mz) )
  allocate( scratch8(mx,my,mz) )

  allocate( scratch1(mx,my,mz) )
  allocate( scratch2(mx,my,mz) )
  allocate( scratch3(mx,my,mz) )
  allocate( scratch4(mx,my,mz) )
  allocate( scratch5(mx,my,mz) )
  allocate( scratch6(mx,my,mz) )

  ! To make a more efficient usage of resources, some arrays share the same memory area

  d2sdtau2 => scratch0
  dsdtau   => scratch8

  yterm    => scratch0
  zterm    => scratch8

  sp1      => scratch3
  sp2      => scratch4
  sp3      => scratch5

  dside    => scratch6
  fside    => scratch1
  gside    => scratch2

  ! Compute derivatives of source function
  call ddtau_calc( dtau, s, dsdtau, d2sdtau2, dtauN1, s0, sN1 )
  ! source function derivatives: done.



  ! Assemble matrix and right-hand-side terms

  ! Default tridiagonal matrix elements

  do iz=izs,ize
     do iy=1,my-1
        dinv(:) = -2.0 / ( dtau(:,iy+1,iz) + dtau(:,iy,iz) )
        sp1(:,iy,iz) = dinv(:) / dtau(:,iy,iz)
        sp3(:,iy,iz) = dinv(:) / dtau(:,iy+1,iz)
        sp2(:,iy,iz) = 1.0
     end do
  end do

  do iz=izs,ize
     dinv(:) = -2.0 / ( dtauN1(:,1,iz) + dtau(:,my,iz) )
     sp1(:,my,iz) = dinv(:) / dtau(:,my,iz)
     sp3(:,my,iz) = dinv(:) / dtauN1(:,1,iz)
     sp2(:,my,iz) = 1.0
  end do

  ! Default d right-hand-side: d = d2sdtau2:
  do iz=izs,ize
     do iy=1,my
        dside(:,iy,iz) = d2sdtau2(:,iy,iz)
     end do
  end do
  


  ! Top boundary, uppermost subdomain ( mpi_y = 0 )
  if ( mpi_y .eq. 0 ) then

     ! Tridiagonal matrix elements, top boundary 
     do iz=izs,ize
        sp1(:,1,iz) = 0.0
        sp2(:,1,iz) = dtau(:,2,iz)*(1.0 + 0.5*dtau(:,2,iz))
        sp3(:,1,iz) = -1.0
     end do

     ! Calculate ex0 = exp(-tau(ix,1,iz)) and ex1 = 1-ex0 terms straight or by series expansion, based on max tau(ix,1,iz).
     do iz=izs,ize
        do ix=1,mx
           taum = dtau(ix,1,iz)
           if (taum .gt. 0.1) then
              ex0(ix,iz) = exp(-dtau(ix,1,iz))
              ex1(ix,iz) = 1.0 -ex0(ix,iz)
           else
              ex1(ix,iz) = dtau(ix,1,iz) * (1.0 - 0.5*dtau(ix,1,iz)*(1.0 - 0.333333 * dtau(ix,1,iz)))
              ex0(ix,iz) = 1.0 - ex1(ix,iz)
           end if
        end do
     end do

     ! Update top layer of dside array
     do iz=izs,ize
        dside(:,1,iz) = s(:,2,iz) - s(:,1,iz) - s(:,1,iz) * dtau(:,2,iz) * ex0(:,iz)
     end do

  end if


  ! Bottom boundary, lowermost subdomain (mpi_y = mpi_ny-1)
  if  ( mpi_y .eq. mpi_ny-1 ) then

     ! Tridiagonal matrix elements, bottom boundary 
     do iz=izs,ize
        sp1(:,my,iz) = -1.0
        sp2(:,my,iz) = dtau(:,my,iz)*(1.0 + 0.5*dtau(:,my,iz))
        sp3(:,my,iz) = 0.0
     end do

     ! Update bottom layer of dside array
     do iz=izs,ize
        dside(:,my,iz) = s(:,my-1,iz) - s(:,my,iz) +  dtau(:,my,iz) * (dsdtau(:,my,iz) + d2sdtau2(:,my,iz))
     end do
  end if


  ! f and g right-hand-sides
  do iz=izs,ize
     fside(:, 1,iz) = sp1(:,1,iz)    ! = 0. at top boundary, uppermost subdomain 
     gside(:, 1,iz) = 0.0
     do iy=2,my-1
        fside(:,iy,iz) = 0.0
        gside(:,iy,iz) = 0.0
     end do
     fside(:,my,iz) = 0.0
     gside(:,my,iz) = sp3(:,my,iz)    ! = 0. at bottom boundary, lowermost domain
  end do
  ! assembling tridiagonal system terms: done. 

  if (isubstep.eq.1) call dumpn (s,'s', 'feau.dmp', 1)
  if (isubstep.eq.1) call dumpn (dtau,'dtau', 'feau.dmp', 1)
  if (isubstep.eq.1) call dumpn (d2sdtau2,'d2sdtau2', 'feau.dmp', 1)
  if (isubstep.eq.1) call dumpn (dsdtau,'dsdtau', 'feau.dmp', 1)

  if (isubstep.eq.1) call dumpn (sp1,'sp1', 'feau.dmp', 1)
  if (isubstep.eq.1) call dumpn (sp2,'sp2', 'feau.dmp', 1)
  if (isubstep.eq.1) call dumpn (sp3,'sp3', 'feau.dmp', 1)
  if (isubstep.eq.1) call dumpn (dside,'d', 'feau.dmp', 1)
  if (isubstep.eq.1) call dumpn (fside,'f', 'feau.dmp', 1)
  if (isubstep.eq.1) call dumpn (gside,'g', 'feau.dmp', 1)
  


  ! Solve local tridiagonal systems
  do iz=izs,ize

     ! eliminate subdiagonal, save factors in sp1 array
     sp2(:,1,iz) = sp2(:,1,iz) - sp1(:,1,iz) ! this needs to be added explicitly for the MPI case to work
     do iy=1,my-1
        sp1(:,iy,  iz)   = -sp1(:,iy+1,iz) /(sp2(:,iy,iz) - sp3(:,iy,iz))
        sp2(:,iy+1,iz)   =  sp2(:,iy+1,iz) + sp1(:,iy,iz) * sp2(:,iy,iz)
        sp2(:,iy,  iz)   =  sp2(:,iy,iz)   - sp3(:,iy,iz)
        dside(:,iy+1,iz) =  dside(:,iy+1,iz) + sp1(:,iy,iz) * dside(:,iy,iz)
        fside(:,iy+1,iz) =  fside(:,iy+1,iz) + sp1(:,iy,iz) * fside(:,iy,iz)
        gside(:,iy+1,iz) =  gside(:,iy+1,iz) + sp1(:,iy,iz) * gside(:,iy,iz)
     end do
     sp2(:,my,iz) = sp2(:,my,iz) - sp3(:,my,iz) ! this needs to be added explicitly for the MPI case to work

  end do  



  ! backsubstitute
  do iz=izs,ize
     dside (:,my,iz) = dside(:,my,iz) / sp2(:,my,iz)
     fside (:,my,iz) = fside(:,my,iz) / sp2(:,my,iz)
     gside (:,my,iz) = gside(:,my,iz) / sp2(:,my,iz)
     do iy=my-1,1,-1
        dside (:,iy,iz) = (dside(:,iy,iz) - sp3(:,iy,iz) * dside(:,iy+1,iz)) / sp2(:,iy,iz)
        fside (:,iy,iz) = (fside(:,iy,iz) - sp3(:,iy,iz) * fside(:,iy+1,iz)) / sp2(:,iy,iz)
        gside (:,iy,iz) = (gside(:,iy,iz) - sp3(:,iy,iz) * gside(:,iy+1,iz)) / sp2(:,iy,iz)
     end do
  end do
  do iz=izs,ize
     q(:,:,iz) = dside(:,:,iz)
     yterm(:,:,iz) = fside(:,:,iz)
     zterm(:,:,iz) = gside(:,:,iz)
  end do
  ! local solution of tridiagonal systems: done.

  if (isubstep.eq.1) call dumpn (q    ,'q', 'feau.dmp', 1)
  if (isubstep.eq.1) call dumpn (yterm,'y', 'feau.dmp', 1)
  if (isubstep.eq.1) call dumpn (zterm,'z', 'feau.dmp', 1)


  ! deallocate temporary arrays containing sp1 and sp3 coefficients scratch3 ... scratch5
  deallocate( scratch6 )
  deallocate( scratch5 )
  deallocate( scratch4 )
  deallocate( scratch3 )


  ! Solution to global tridiagonal system (in case of domain decomposition along the y-direction)

  if (mpi_ny .gt. 1 ) then

     ! re-allocate arrays for global tridiagonal system
     allocate( scratch3(mx, 2*mpi_ny-2, mz) )
     allocate( scratch4(mx, 2*mpi_ny-2, mz) )
     allocate( scratch5(mx, 2*mpi_ny-2, mz) )
     allocate( scratch6(mx, 2*mpi_ny-2, mz) )

     sp1     => scratch3
     sp2     => scratch4
     sp3     => scratch5
     qcorr   => scratch6

     ! allocate buffer for MPI_allgather communication
     allocate( scratch7(mx, 1, mz, 0:mpi_ny-1) )
     buffer  => scratch7



     ! assembling the global tridiagonal system 

     ! main diagonal:

     ! gather last layers of zterm arrays from all subdomains; 
     ! copy data to sp2
     call mpi_gather_single_layers_y(zterm, my, buffer)
     do iz=izs,ize
        do j=0,mpi_ny-2
           j2 = 2*j+1
           sp2(:,j2,iz) = buffer(:,1,iz,j) 
        end do
     end do

     ! gather first layers of yterm arrays from all subdomains; 
     ! copy data to sp2
     call mpi_gather_single_layers_y(yterm, 1, buffer)
     do iz=izs,ize
        do j=1,mpi_ny-1
           j1 = 2*j
           sp2(:,j1,iz) = buffer(:,1,iz,j) 
        end do
     end do


     ! sub-diagonal elements:

     ! gather last layers of yterm arrays from all subdomains; 
     ! copy data to sp1
     do iz=izs,ize
        sp1(:,1,iz) = 0.0
     end do
     if (mpi_ny.gt.2) then
        call mpi_gather_single_layers_y(yterm, my, buffer)
        do iz=izs,ize
           do j=1,mpi_ny-2
              j1 = 2*j
              j2 = j1+1
              sp1(:,j1,iz) = 1.0 
              sp1(:,j2,iz) = buffer(:,1,iz,j) 
           end do
        end do
     end if
     do iz=izs,ize
        sp1(:,2*mpi_ny-2,iz) = 1.0
     end do

     ! super-diagonal elements:

     ! gather first layers of zterm arrays from all subdomains; 
     ! copy data to sp3
     do iz=izs,ize
        sp3(:,1,iz) = 1.0
     end do
     if (mpi_ny.gt.2) then
        call mpi_gather_single_layers_y(zterm, 1, buffer)
        do iz=izs,ize
           do j=1,mpi_ny-2
              j1 = 2*j
              j2 = j1+1
              sp3(:,j1,iz) = buffer(:,1,iz,j)
              sp3(:,j2,iz) = 1.0
           end do
        end do
     end if
     do iz=izs,ize
        sp3(:,2*mpi_ny-2,iz) = 0.0
     end do

     ! right-hand side (6corr):
     call mpi_gather_single_layers_y(q, 1, buffer)
     do iz=izs,ize
        do j=1,mpi_ny-1
           j1 = 2*j
           qcorr(:,j1,iz) = buffer(:,1,iz,j)
        end do
     end do

     call mpi_gather_single_layers_y(q, my, buffer)
     do iz=izs,ize
        do j=0,mpi_ny-2
           j2 = 2*j+1
           qcorr(:,j2,iz) = buffer(:,1,iz,j)
        end do
     end do


     ! Solve global system
     do iz=izs,ize

        ! eliminate subdiagonal, save factors in sp1 array
        do j=1,2*mpi_ny-3   ! do j=1,2*(mpi_ny-1)-1
           sp1(:,j  ,iz)   =  - sp1(:,j+1,iz) / sp2(:,j,iz)
           sp2(:,j+1,iz)   =    sp2(:,j+1,iz) + sp1(:,j,iz) * sp3(:,j,iz)
           qcorr(:,j+1,iz) =  qcorr(:,j+1,iz) + sp1(:,j,iz) * qcorr(:,j,iz)
        end do

     end do! isolate loop for debugging
     do iz=izs,ize


        ! backsubstitute
        qcorr(:, 2*mpi_ny-2, iz) = qcorr(:, 2*mpi_ny-2, iz) / sp2(:, 2*mpi_ny-2, iz)
        do j=2*mpi_ny-3,1,-1
           qcorr(:,j,iz) = (qcorr(:,j,iz) - sp3(:,j,iz) * qcorr(:,j+1,iz)) / sp2(:,j,iz)
        end do

     end do


     ! Add corrections to local solution
     ! zterm:
     if (mpi_y .ne. mpi_ny-1) then
        j = 2*mpi_y+1
        do iz=izs,ize
           do iy=1,my
              q(:,iy,iz) = q(:,iy,iz) - qcorr(:, j, iz) * zterm(:,iy,iz)
           end do
        end do
     end if

     !yterm:
     if (mpi_y .ne. 0) then
        j = 2*mpi_y
        do iz=izs,ize
           do iy=1,my
              q(:,iy,iz) = q(:,iy,iz) - qcorr(:, j, iz) * yterm(:,iy,iz)
           end do
        end do
     end if


     ! Finally, deallocate the temporary arrays used for buffer and for computing the global correction 
     deallocate( scratch7 )
     deallocate( scratch6 )
     deallocate( scratch5 )
     deallocate( scratch4 )
     deallocate( scratch3 )

  end if !(mpi_ny.gt.1)
  ! if no decomposition along y-direction (mpi_ny=1), then q remains unchanged


  ! Deallocate all remaining temporary arrays
  deallocate( scratch2 )
  deallocate( scratch1 )

  deallocate( scratch8 )
  deallocate( scratch0 )



  !  Surface intensity
  if ( do_surfi .and. mpi_y.eq.0 ) then
     do iz=izs,ize
        surfi(:,iz) = 2.0 * ex0(:,iz) * ( q(:,1,iz) + s(:,1,iz) ) + s(:,1,iz)*ex1(:,iz)**2
     end do
  end if

END SUBROUTINE transq_feautrier_mpi
