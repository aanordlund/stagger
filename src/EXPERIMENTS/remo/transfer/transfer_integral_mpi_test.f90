!***********************************************************************
SUBROUTINE transfer( np, n1, dtau, s, q, surfi, do_surfi )

  USE params, only: mid, mx, my, mz
  IMPLICIT none

  character(len=mid):: id='$Id: transfer_integral_mpi_test.f90,v 1.1 2016/10/12 13:08:48 remo Exp $'

  integer np, n1
  real, dimension(mx,my,mz), intent(in)  :: dtau, s
  real, dimension(mx,my,mz), intent(out) :: q
  real, dimension(mx,mz),    intent(out) :: surfi

  logical do_surfi, omp_in_parallel


  if (omp_in_parallel()) then
     call transq_integral_mpi ( dtau, s, q, surfi, do_surfi )
  else
     !$omp parallel
     call transq_integral_mpi ( dtau, s, q, surfi, do_surfi )
     !$omp end parallel
  end if

END SUBROUTINE transfer


!***********************************************************************
SUBROUTINE transq_integral_mpi( dtau, s, q, surfi, do_surfi )

  ! Solve the transfer equation, given optical depth and source function.
  !
  ! This routine uses an MPI implementation of integral solver for the 
  ! radiative transfer equation.
  !
  ! The routine solves for Q = P-S, where P is the Feautrier variable
  ! (IPlus + IMinus) / 2  = ( Irev + Ifwd ) / 2 (according to the notation
  ! used in this subroutine.)
  !
  ! KNOWN OPEN ISSUES: the accuracy of the implemented method decreases dramatically
  ! if large gradients in dtau occur near the surface


  USE params, only: mid, mx, my, mz, mpi_y, mpi_ny, izs, ize

  IMPLICIT none

  character(len=mid)  :: id = '$Id: transfer_integral_mpi_test.f90,v 1.1 2016/10/12 13:08:48 remo Exp $'

  real, dimension(mx,my,mz), intent(in)  :: dtau, s
  real, dimension(mx,my,mz), intent(out) :: q
  real, dimension(mx,my,mz)              :: qfw, qrv          ! forward and reverse q=I-S
  real, dimension(mx,mz)                 :: surfi
  logical                                :: do_surfi

  real, parameter                        :: dtau_thin = 0.05, dtau_thick = 100.0
  logical                                :: do_linear_transqint

  real, allocatable, target, dimension(:,:,:) :: scratch0, scratch1, scratch2
  real, allocatable, target, dimension(:,:,:) :: scratch3, scratch4, scratch5, scratch6
  real, pointer,             dimension(:,:,:) :: ex0, ex1, ex2
  real, pointer,             dimension(:,:,:) :: dsdtau, d2sdtau2 
  real, pointer,             dimension(:,:,:) :: d2sdtau2fw, d2sdtau2rv 

  real, dimension(mx,1,mz) :: dtauN1          ! buffer containing dtau(n+1)=tau(n+1)-tau(n)
  real, dimension(mx,1,mz) :: s0, sN1         ! buffers containing source function for layers 0 and n+1
  real, dimension(mx,1,mz) :: dsdtau0, dsdtauN1 ! buffers containing derivative of source function for layers 0 and n+1

  real, dimension(mx)      :: dinv            ! 1 / (dtau(n+1)+dtau(n))
  real                     :: dtau1           ! used to store dtau(ix,iy,iz), for convenience

  real, dimension(mx,1,mz)            :: qfwUp, qrvDn            ! Q=I-S, forward and reverse, bottom (up) and top (dn) layers
  real, dimension(mx,1,mz,0:mpi_ny-1) :: qfwUpAll, qrvDnAll      ! intrinsic solutions, all subdomains

  real, dimension(mx,1,mz)            :: exTauFw,    exTauRv     ! attenuation factors, forward and reverse, local subdomain
  real, dimension(mx,1,mz,0:mpi_ny-1) :: exTauFwAll, exTauRvAll  ! attenuation factors, all subdomains

  integer                             :: ix, iy, iz, j


  call print_id (id)


  ! allocate temporary arrays
  allocate( scratch0(mx,1:my+1,mz) )
  allocate( scratch1(mx,1:my+1,mz) )
  allocate( scratch2(mx,1:my+1,mz) )
  allocate( scratch3(mx,my,mz) )
  allocate( scratch4(mx,my,mz) )
  allocate( scratch5(mx,my,mz) )
  allocate( scratch6(mx,my,mz) )

  ex0        => scratch0
  ex1        => scratch1
  ex2        => scratch2
  dsdtau     => scratch3
  d2sdtau2   => scratch4
  d2sdtau2fw => scratch5
  d2sdtau2rv => scratch6


  ! Compute derivatives of source function
  call ddtau_calc( dtau, s, dsdtau, d2sdtau2, dtauN1, s0, sN1 )

  ! numerical second derivatives: use linear approximation or higher-order finite differences?
  do_linear_transqint = .true.

  if (do_linear_transqint) then

     ! boundary layers: source function's first derivative, top (0)  and bottom (N1)
     call mpi_send_bdry_y_up(dsdtau, 1, dsdtau0)
     call mpi_send_bdry_y_dn(dsdtau, 1, dsdtauN1)

     ! forward derivative
     ! if top domain, simply copy layer 2 to layer 1
     if (mpi_y.eq.0) then
        do iz=izs,ize
           d2sdtau2fw(:,1,iz) = d2sdtau2fw(:,2,iz)
        end do
     else
        do iz=izs,ize
           d2sdtau2fw(:,1,iz) = (dsdtau(:,1,iz) - dsdtau0(:,1,iz)) /dtau(:,1,iz)
        end do
     end if
     do iz=izs,ize
        do iy=2,my
           d2sdtau2fw(:,iy,iz) = (dsdtau(:,iy,iz) - dsdtau(:,iy-1,iz))/dtau(:,iy,iz)
        end do
     end do

     ! reverse derivative
     ! if bottom domain, simply copy layer my-1 to layer my
     if (mpi_y.eq.mpi_ny-1) then
        do iz=izs,ize
           d2sdtau2rv(:,my,iz) = d2sdtau2rv(:,my-1,iz)
        end do
     else
        do iz=izs,ize
           d2sdtau2rv(:,1,iz) = (dsdtauN1(:,1,iz) - dsdtau(:,my,iz)) / dtauN1(:,1,iz)
        end do
     end if
     do iz=izs,ize
        do iy=my-1,1,-1
           d2sdtau2rv(:,iy,iz) = (dsdtau(:,iy+1,iz) - dsdtau(:,iy,iz))/dtau(:,iy+1,iz)
        end do
     end do

else

   d2sdtau2fw = d2sdtau2
   d2sdtau2rv = d2sdtau2

end if


! Compute exponential coefficients:

! ex0 = exp(-dtau)
! ex1 = 1 - exp(-dtau)
! ex2 = 1 - exp(-dtau) * (1 + dtau)

do iz=izs,ize
   ! Compute ex0, ex1, and ex2
   do iy=1,my
      do ix=1,mx
         dtau1 = dtau(ix,iy,iz)
         if ( dtau1 .gt. dtau_thick ) then
            ex0(ix,iy,iz) = 0.0
            ex1(ix,iy,iz) = 1.0
            ex2(ix,iy,iz) = 1.0
         else if ( dtau1 .gt. dtau_thin ) then
            ex0(ix,iy,iz) = exp( -dtau1 )
            ex1(ix,iy,iz) = 1.0 - ex0(ix,iy,iz)
            ex2(ix,iy,iz) = ex1(ix,iy,iz) - dtau1 * ex0(ix,iy,iz)
         else 
            ex1(ix,iy,iz) = dtau1 * (1.0 - 0.5 * dtau1 * (1.0 - 0.3333333 * dtau1))
            ex0(ix,iy,iz) = 1.0 - ex1(ix,iy,iz)
            ex2(ix,iy,iz) = dtau1**2 * (0.5 - 0.3333333 * dtau1)
         end if
      end do
   end do
end do

! exponential coefficients for layer "n+1" ("N1")
do iz=izs,ize
   do ix=1,mx
      dtau1 = dtauN1(ix,1,iz)
      if ( dtau1 .gt. dtau_thick ) then
         ex0(ix,my+1,iz) = 0.0
         ex1(ix,my+1,iz) = 1.0
         ex2(ix,my+1,iz) = 1.0
      else if ( dtau1 .gt. dtau_thin ) then
         ex0(ix,my+1,iz) = exp( -dtau1 )
         ex1(ix,my+1,iz) = 1.0 - ex0(ix,my+1,iz)
         ex2(ix,my+1,iz) = ex1(ix,my+1,iz) - dtau1 * ex0(ix,my+1,iz)
      else 
         ex1(ix,my+1,iz) = dtau1 * (1.0 - 0.5 * dtau1 * (1.0 -0.3333333*dtau1))
         ex0(ix,my+1,iz) = 1.0 - ex1(ix,my+1,iz)
         ex2(ix,my+1,iz) = dtau1**2 * (0.5 - 0.3333333 * dtau1)
      end if
   end do
end do
! exponential coefficients: done.


! Compute global attenuation factors, forward and reverse

do iz=izs,ize
   exTauFw(:,1,iz) = 1.0
   do iy=2,my
      exTauFw(:,1,iz) = exTauFw(:,1,iz) * ex0(:,iy,iz)
   end do
   ! Note that the attenuation factor for the forward solution accounts
   ! for the attenuation in cell number 1,
   ! while the attenuation factor in the reverse direction for the attenuation 
   ! in cell my+1
   exTauRv(:,1,iz) = exTauFw(:,1,iz) * ex0(:,my+1,iz)         
   exTauFw(:,1,iz) = exTauFw(:,1,iz) * ex0(:,1,iz) 
end do
! attenuation factors, forward and reverse: done.



! Intrinsic solution of radiative-transfer equation

! Qfw: Forward direction

! first layer: assume qfw(0) = 0 for intrinsic solution;
! first layer of top domain: use zeroth-order approximation for the source function
!                            ( s = const = s(0) = s(1) )

if (mpi_y .eq. 0) then
   do iz=izs,ize
      qfw(:, 1,iz) =  - s(:, 1,iz) * ex0(:, 1,iz)
   end do
else
   do iz=izs,ize
      qfw(:, 1,iz) = - dsdtau(:, 1,iz) * ex1(:, 1,iz) + d2sdtau2fw(:, 1,iz) * ex2(:, 1,iz)
   end do
end if

do iz=izs,ize
   do iy=2,my
      qfw(:,iy,iz) = qfw(:,iy-1,iz) * ex0(:,iy,iz) - dsdtau(:,iy,iz) * ex1(:,iy,iz) + d2sdtau2fw(:,iy,iz) * ex2(:,iy,iz)
   end do
end do


! Qrv: reverse direction

! last layer: assume assume qrv(n+1) = 0 for intrinsic solution;
! bottom layer of bottom domain: use diffusion approximation (assume a optically thick medium)

if (mpi_y .eq. mpi_ny-1) then
   do iz=izs,ize
      ! use original d2sdtau2 as second derivative for diffusion approximation
      qrv(:,my,iz) = dsdtau(:,my,iz) + d2sdtau2(:,my,iz)
   end do
else
   do iz=izs,ize
      qrv(:,my,iz) = dsdtau(:,my,iz) * ex1(:,my+1,iz) + d2sdtau2rv(:,my,iz) * ex2(:,my+1,iz)
   end do
end if

do iz=izs,ize
   do iy=my-1,1,-1
      qrv(:,iy,iz) = qrv(:,iy+1,iz) * ex0(:,iy+1,iz) + dsdtau(:,iy,iz) * ex1(:,iy+1,iz) + d2sdtau2rv(:,iy,iz) * ex2(:,iy+1,iz)
   end do
end do

! Intrinsic solution: done.


! Global solution = intrinsic + external

! If the computational domain is split in the vertical direction (mpi_ny > 1),
! compute attenuation factors for local subdomains and gather them afterwards.
!
! Also, gather intrinsic Q=I-S RT solutions in both directions at the boundary layers 
! for all subdomains;
!
!  forward direction: gather qfw values from last  (iy=my) layer of all subdomains; 
!  reverse direction: gather qrv values from first (iy=1) layer of all subdomains.

if (mpi_ny .gt. 1) then

   do iz=izs,ize

      ! qfwUp and qrvDn contain the values of Qfw and Qrv at the bottom ("upper", 
      ! index-wise) and top ("lower") boundaries that will be communicated to the other
      ! subdomains

      qfwUp(:,1,iz) = qfw(:, my, iz)
      qrvDn(:,1,iz) = qrv(:,  1, iz)

   end do

   call mpi_gather_slices_y( exTauFw, exTauFwAll )
   call mpi_gather_slices_y( exTauRv, exTauRvAll )
   call mpi_gather_slices_y(   qfwUp,   qfwUpAll )
   call mpi_gather_slices_y(   qrvDn,   qrvDnAll )


   if (mpi_y .gt. 0) then

      ! Compute value of attenuated global Q forward solution at boundary 
      ! (note: centred in last layer of cells of previous subdomain);
      ! The value of the attenuated global Q forward solution AT BOUNDARY is stored in *** qfwUp ***

      ! This value is eventually used to compute the attenuated "external" component which is then added
      ! later to the intrinsic to obtain the global solution


      ! initialize Q starting with value of qfw solution at iy=my in subdomain mpi_y = 0
      do iz=izs,ize
         qfwUp(:,1,iz) = qfwUpAll(:,1,iz,0)
      end do

      ! cumulative attenuation, forward direction
      if (mpi_y .gt. 1) then
         do j=1,mpi_y-1
            do iz=izs,ize
               qfwUp(:,1,iz) = qfwUpAll(:,1,iz,j-1) + exTauFwAll(:,1,iz,j-1) * qfwUp(:,1,iz)
            end do
         end do
      end if

      ! compute global Q forward solution by adding intrinsic solution and "external" 
      ! component attenuated through local subdomain
      do iz=izs,ize
         do iy=1,my
            ! first attenuate external component
            ! then add it to intrinsic solution to obtain global solution
            qfwUp(:, 1,iz) = qfwUp(:, 1,iz) * ex0  (:,iy,iz)
            qfw  (:,iy,iz) = qfw  (:,iy,iz) + qfwUp(:, 1,iz)
         end do
      end do

   end if


   if (mpi_y .lt. mpi_ny-1) then

      ! initialize Q starting with value of qrv solution at iy=0 in subdomain mpi_y = mpi_ny-1
      do iz=izs,ize
         qrvDn(:,1,iz) = qrvDnAll(:,1,iz,mpi_ny-1)
      end do

      ! cumulative attenuation, reverse direction
      if (mpi_y .lt. mpi_ny-2) then
         do j=mpi_ny-2,mpi_y+1,-1
            do iz=izs,ize
               qrvDn(:,1,iz) = qrvDnAll(:,1,iz,j) + exTauRvAll(:,1,iz,j) * qrvDn(:,1,iz)
            end do
         end do
      end if

      ! compute global Q reverse solution by adding intrinsic solution and "external" attenuated component
      do iz=izs,ize
         do iy=my,1,-1
            ! first attenuate external component
            ! then add it to intrinsic solution to obtain global solution
            qrvDn(:, 1,iz) = qrvDn(:, 1,iz) * ex0  (:,iy+1,iz)
            qrv  (:,iy,iz) = qrv  (:,iy,iz) + qrvDn(:, 1,iz)
         end do
      end do

   end if

end if  ! mpi_ny.gt.1 ?

! Global solution: done.


! Finally, compute average of Qfw and Qrv, that is P = ((Ifw + Irv) - S ) / 2

do iz=izs,ize
   do iy=1,my
      q(:,iy,iz) = 0.5 * (qfw(:,iy,iz)+qrv(:,iy,iz))
   end do
end do



! Surface intensity

if (do_surfi .and. mpi_y .eq. 0) then
   do iz=izs,ize
      surfi(:,iz) = 2.0 * ex0(:,1,iz) * ( q(:,1,iz) + s(:,1,iz) )  + s(:,1,iz) * ex1(:,1,iz)**2
   end do
end if


! deallocate scratch arrays
deallocate( scratch0, scratch1, scratch2, scratch3, scratch4, scratch5, scratch6)


END SUBROUTINE transq_integral_mpi
