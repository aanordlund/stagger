!...............................................................................
subroutine quad_init

  use params, only: master, mid, pi
  use cooling, only: quad, mray, nray, mu_ray, phi_ray, wt_ray, &
       mmu, nmu, nphi, form_factor

  implicit none

  character(len=mid) :: Id = 'quad_init'
  character(len=32) :: default_quad = 'radau'
  integer :: lmu, lphi
  integer :: imu, iphi, iray
  real, dimension(mmu) :: xmu, wmu
  real :: wphi

  !theta: acos(mu); units: degrees
  real :: theta

  ! Carlson quadrature A
  ! number of rays per quadrant
  integer, parameter   :: mcar=36
  integer :: ncar
  ! imu[x,y,z]: indexes of directional cosines
  integer, dimension(mcar) :: imux, imuy, imuz
  ! weights
  real,    dimension(mcar) :: wtcarl

  integer :: i
  logical :: is_quad_valid
  logical :: is_quad_undefined
  logical, save :: first_time = .true.

  ! calculate quadrature angles only the first time the routine is called
  if (first_time) then
     if (master) then
        print '(x,a)', repeat("-",72)
        print '(x,a)', trim(Id)//': initialise'
     end if
  else
     return
  end if

  is_quad_undefined = (quad == 'undefined')

  ! if quadrature undefined, use old notation for angular quadrature:
  !
  ! nmu > 0: Use Gauss quadrature (mu=1 excluded), accuracy: 2*nmu order
  ! nmu = 0: One vertical ray with weight 0.5
  ! nmu < 0: Use Radau quadrature (mu=1 included), accuracy: 2*nmu-1 order

  if (is_quad_undefined) then
     if (nmu > 0) then
        quad = 'gauss'
     else if (nmu < 0) then
        quad = 'radau'
     else if (nmu == 0) then
        quad = 'vertical'
     end if
     is_quad_undefined = .false.
     if (master) then
        print '(1x,a)', trim(Id) // ': quadrature changed from undefined to ' // quad
     end if
  endif

  ! check if quadrature type is valid
  select case (quad)
  case ('gauss')
     is_quad_valid = .true.
  case ('radau')
     is_quad_valid = .true.
  case ('vertical')
     is_quad_valid = .true.
  case ('carlson')
     is_quad_valid = .true.
  case default
     is_quad_valid = .false.
  end select

  ! if quadrature type not valid, set to default
  if (.not.is_quad_valid) then
     quad = default_quad
     is_quad_valid = .true.
     if (master) then
        print '(1x,a)', trim(Id) // ': warning: quadrature unavailable, set quad = ' // quad
     end if
  end if

  ! check consistency with old notation: quad has precedence over nmu!
  select case (quad)
  case ('gauss')
     if (nmu .le. 0) then
        nmu = max(abs(nmu),1)
        if (master) then
           print '(1x,a,x,i3)', trim(Id)//': warning: quad = gauss, set nmu = ', nmu
        end if
     end if
  case ('radau')
     if (nmu .ge. 0) then
        nmu = -max(abs(nmu),1)
        if (master) then
           print '(1x,a,x,i3)', trim(Id)//': warning: quad = radau, set nmu = ', nmu
        end if
     end if
  case ('vertical')
     if (nmu .ne. 0) then
        nmu = 0
        if (master) then
           print '(1x,a)', trim(Id)//': warning: quad = vertical, set nmu = 0'
        end if
     end if
  end select

  ! check: nphi should be greater than or equal to 1 for 'radau' and 'gauss'
  if (quad .eq. 'gauss' .or. quad .eq. 'radau') then
     if (nphi .lt. 1) then
        nphi = 1
        if (master) then
           print '(1x,a)', trim(Id)//': warning: quad = gauss or quad = radau, set nphi = 1'
        end if
     end if
  end if


  ! define mu and phi values and associated weights

  if (quad .eq. 'gauss' .or. quad .eq. 'radau') then

     ! case: Gauss or Radau quadrature

     ! mu values and associated weights
     lmu = abs(nmu)
     if (quad .eq. 'gauss') then
        call gausi (lmu,0.,1.,wmu,xmu)
        nray = lmu * nphi
     else
        call radaui (lmu,0.,1.,wmu,xmu)
        nray = (lmu-1) * nphi + 1
     end if

     ! print mu values and weights
     if (master) then
        print '(x,a)', repeat("-",72)
        print '(x,a10,x,a3,x,a10,x,a8,x,a10)', trim(Id)//':', 'imu', 'xmu', 'theta','weight'
        do imu=1,lmu
           ! theta = acos(mu), units: degrees
           theta = acos(xmu(imu))*180./pi
           print '(x,10x,x,i3,x,f10.3,x,f8.1,x,f10.3)',imu,xmu(imu),theta,wmu(imu)
        end do
     end if

     ! initialise ray index
     iray = 1

     ! loop over mu values
     do imu=1,lmu
        ! how many phi values ?
        if (xmu(imu) .eq. 1.0) then
           lphi = 1
           wphi = 1.0
        else
           lphi = nphi
           wphi = 1.0 / real(nphi)
        end if
        ! loop over phi angles
        do iphi=1,lphi
           ! assign mu, phi and weight to ray
           mu_ray(iray) = xmu(imu)
           wt_ray(iray) = wmu(imu) * wphi
           phi_ray(iray)  = (iphi-1)*2.*pi / real(lphi)
           iray = iray + 1
        end do
     end do

  else if (quad .eq. 'vertical') then

     ! legacy case: only vertical ray with weight = 0.5
     ! TO DO: check why weight is set to 0.5
     nray = 1
     mu_ray(1)  = 1.0
     phi_ray(1) = 0.0
     wt_ray(1)  = 0.5

     ! legacy case: change to form_factor = 0.4 ?
     ! TO DO: check why form_factor is set to 0.4
     if (form_factor == 1.0) form_factor = 0.4

  else if (quad .eq. 'carlson' ) then

     ! Carlson quadrature A
     call carlson (nmu, mcar, ncar, xmu, imux, imuy, imuz, wtcarl)

     if (master) then
        print '(1x,a)', trim(Id)//': quad = carlson, directional cosines and weights:'
        do i=1,ncar
           print '(x,i3,x,3(x,F11.8),2x,F11.8)',&
                i,xmu(imux(i)),xmu(imuy(i)),xmu(imuz(i)),wtcarl(i)
        end do
     end if

     ! Carlson quadrature: nphi is here the number of quadrants (4)
     if (nphi.ne.4) then
        if (master) then
           print '(1x,a)', trim(Id)//': warning: quad = carlson, set nphi = 4'
        end if
        nphi = 4
     end if

     ! weight per quadrant
     wphi = 0.25

     ! number of rays
     nray = ncar * nphi

     ! assign mu values and phi angles and associated weights
     iray = 1
     do i=1,ncar
        do iphi=1,nphi
           mu_ray(iray)  = xmu(imuz(i))
           phi_ray(iray) = acos(xmu(imux(i))) + (iphi-1)*2.*pi / real(nphi)
           wt_ray(iray)  = wtcarl(i) * wphi
           iray = iray + 1
        end do
     end do

  end if

  return

end subroutine quad_init

!...............................................................................
subroutine limb_init

  ! limb_init: iniitialises the mu-values and phi-angles for limb darkening
  ! output.
  ! The present version simply uses the same values as for the main quadrature,
  ! with the addition of the vertical direction, if that is not already
  ! included.
  !
  ! note: before 2016/12/08, limb arkening angles used to be handled by
  ! quad_init subroutine

  use params, only: master, mid
  use cooling, only: mray, nray, mu_ray, phi_ray, nlimb, mu_limb, phi_limb

  implicit none

  character(len=mid) :: Id = 'limb_init'

  integer :: i

  real, parameter :: eps = 1.0e-6

  logical :: has_vertical
  logical, save :: first_time = .true.


  ! first call?
  if (first_time) then
     if (master) then
        print '(x,a)', repeat("-",72)
        print '(x,a)', trim(Id)//': initialise'
     end if
  else
     return
  end if

  ! initialise ray angles for limb darkening output
  ! default: use same mu and phi values as for quadrature set
  has_vertical = .false.
  do i=1,nray
     mu_limb(i) = mu_ray(i)
     phi_limb(i)= phi_ray(i)
     if (abs(mu_ray(i)-1.0).le.eps) has_vertical = .true.
  end do

  ! add vertical ray if not included in quadrature set
  if (has_vertical) then
     nlimb = nray
  else
     nlimb = nray + 1
     mu_limb(nlimb) = 1.0
     phi_limb(nlimb)= 0.0
     has_vertical = .true.
  end if

  ! print ray angles for limb darkening output
  if (first_time) then
     first_time = .false.
     if (master) then
        print '(x,a)', trim(Id)//': limb darkening mu-values and phi-angles:'
        print '(x,a4,x,a9,x,a6)','ilimb','mu','phi'
        do i=1,nlimb
           print '(x,i4,x,f9.6,x,f6.3)',i,mu_limb(i),phi_limb(i)
        end do
      end if
  end if

  return

end subroutine limb_init
