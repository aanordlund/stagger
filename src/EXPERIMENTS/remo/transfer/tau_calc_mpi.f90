!***********************************************************************
subroutine tau_calc( dydep, nydep, costh, tauIntr, nDTauTop, nDTauBot, do_tau )

  ! Compute optical depth scale increments (dtau) and, optionally (if do_tau=.true.),
  ! full optical depth scale tau

  use params,  only : mid, mx, my, mz, mpi_y, mpi_ny, izs, ize
  use cooling, only : rk, lnrk, dtau, tau, dtauMin, dtauMax

  implicit none

  character(len=mid):: id='$Id: tau_calc_mpi.f90,v 1.1 2016/10/12 13:08:48 remo Exp $'

  ! nydep: present only for historical reasons, for backward compatibility
  integer, intent(in)                      :: nydep
  real,    intent(in)                      :: costh
  real,    intent(in), dimension(my)       :: dydep
  integer, intent(out)                     :: nDTauTop, nDTauBot
  ! do_tau: compute global optical depth scale ?
  logical, intent(in)                      :: do_tau 

  real    :: costhInv, f1
  integer :: nTop, nBot
  integer :: ix, iy, iz, j

  ! lnrk0, rk0: (ln) absorption coefficient in layer immediately above local domain
  ! tauIntr:    intrinsic tau scale
  real, dimension(mx, 1,mz)        :: lnrk0, rk0 
  real, dimension(mx,my,mz)        :: tauIntr
  real, dimension(mx, 1,mz,mpi_ny) :: delTauAll
  real, dimension(mx, 1,mz)        :: delTau  


  call print_id (id)
  

  ! Send information about absorption coefficient in the last layer of 
  ! local domain to zero-th layer of domain below (mpi_yup);
  ! Receive information about absorption coefficient in the last layer
  ! from domain above (mpi_ydn) and copy it to zero-th layer of local domain

  ! If no subdivisions in the vertical direction are present (mpi_ny=1) then 
  ! skip MPI-send/receive.

  if ( mpi_ny .le. 1 ) then

     do iz=izs,ize
        lnrk0(:,1,iz) = lnrk(:,1,iz)
     end do

  else

     ! send extinction coefficient from boundary layer up
     call mpi_send_bdry_y_up(lnrk, 1, lnrk0)

     ! The mesh is not periodic in the y-direction: in the top domain, overwrite 
     ! lnrk0 with information from the first layer

     if (mpi_y .eq. 0) then
        do iz=izs,ize
           lnrk0(:,1,iz) = lnrk(:,1,iz)
        end do
     end if

  end if


  ! compute dtau and intrinsic optical depth scale tauIntr
  ! Note: dtau(i) = tau(i) - tau(i-1) !!

  nDtauTop = 0
  nDtauBot = 0
  nTop     = 0
  nBot     = 0

  costhInv = 1.0 / costh
  f1 = 0.5 * costhInv

  do iz=izs,ize
     rk( :,1,iz)     = exp(lnrk( :,1,iz))
     rk0(:,1,iz)     = exp(lnrk0(:,1,iz))
     dtau(:,1,iz)    = dydep(1) * ( rk0(:,1,iz) + rk(:,1,iz) ) *f1
     tauIntr(:,1,iz) = dtau(:,1,iz)
     do iy=2,my
        rk(:,iy,iz) = exp(lnrk(:,iy,iz))
        do ix=1,mx
           dtau(ix,iy,iz) = dydep(iy) * ( rk(ix,iy-1,iz) + rk(ix,iy,iz) ) *f1
           tauIntr(ix,iy,iz)  = tauIntr(ix,iy-1,iz)+dtau(ix,iy,iz) 
           if (dtau(ix,iy,iz) < dTauMin)  nTop = max(nTop,iy)
           if (dtau(ix,iy,iz) < dTauMax)  nBot = max(nBot,iy)
        end do
     end do
  end do

  nDTauTop  = max(nTop,nDTauTop)
  nDtauBot  = max(nBot,nDtauBot)


  if (do_tau) then

     ! Compute global optical depth (tau). Initialize: tau = tauIntr

     do iz=izs,ize
        tau(:,:,iz) = tauIntr(:,:,iz)
     end do

     if (mpi_ny .gt. 1) then 

        ! if multiple divisions along the vertical axis are present, then gather the values of
        ! tau intrinsic in the last layer (uppermost iy index) from all sub-domains in the same
        ! (mpi_x,mpi_z) column and add them incrementally

        ! select last slice (iy=my) from tauIntr and broadcast to all processes 
        do iz=izs,ize
           delTau(:,1,iz) = tauIntr(:,my,iz)
        end do
        call mpi_gather_slices_y( delTau, delTauAll)


        ! add tau offsets (for subdomains with mpi_y = 1 or larger)

        if (mpi_y .ge. 1) then

           ! reset delTau
           do iz=izs,ize
              delTau(:,1,iz) = 0.0
           end do

           ! add tau offsets (delTauAll) starting from the top-most domain (mpi_y=0, or j=1) to
           ! domain immediately above the current one (mpi_y-1, i.e. j = mpi_y)

           do j=1,mpi_y
              do iz=izs,ize
                 delTau(:,1,iz) = delTau(:,1,iz) + delTauAll(:,1,iz,j) 
              end do
           end do

           do iz=izs,ize
              do iy=1,my
                 tau(:,iy,iz) = tau(:,iy,iz) + delTau(:,1,iz)
              end do
           end do

        end if  ! is mpi_y .ge. 1 ?

     end if ! is mpi_ny .gt. 1 ?

  end if ! do_tau ?


end subroutine tau_calc
