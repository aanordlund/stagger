! $Id: trnslt_mpi.f90,v 1.1 2016/10/11 14:49:08 remo Exp $
!-------------------------------------------------------------------------------

module trnslt_m
  implicit none
  real, dimension(:,:,:), allocatable:: fin2
  integer, save:: nvertical=0, nxtrnslt=0, nztrnslt=0, nxztrnslt=0
  integer(kind=8):: ninterp=0

end module trnslt_m

subroutine trnslt_count
  use params, only: master
  use trnslt_m
  implicit none
  if (master) print 1,nvertical,nxtrnslt,nztrnslt,nxztrnslt
1 format(' trnslt: nvertical, nx, nz, nxz =',4i10)
  if (master) print 2, ninterp, real(ninterp)
  call sum_int8_mpi (ninterp)
  if (master) print 2, ninterp, real(ninterp)
2 format(' trnslt: ninterp =', 1p, i15, e12.3)
  ninterp = 0
end subroutine trnslt_count

!-------------------------------------------------------------------------------
subroutine trnslt (fin,fout,dxdy,dzdy,do_exp,nymax,mblocks,verbose)

  use params, only: mx,my,mz,mid,mpi_rank,dbg_rad,n_trnslt

  implicit none

  real, dimension(mx,my,mz), intent(in)  :: fin
  real, dimension(mx,my,mz), intent(out) :: fout
  real,                      intent(in)  :: dxdy, dzdy
  logical,                   intent(in)  :: do_exp
  integer,                   intent(in)  :: nymax, verbose
  integer,                   intent(in)  :: mblocks !deprecated
  ! mblocks: max number of subdomains communicated to fill ghost zones (deprecated)

  logical            :: omp_in_parallel, debug2
  character(len=mid) :: id="$Id: trnslt_mpi.f90,v 1.1 2016/10/11 14:49:08 remo Exp $"

  call print_id (id)
  if (nymax < 1) then
    if (debug2(dbg_rad,4)) &
      print*,mpi_rank,' trsnlt: returns with nymax =', nymax
    return
  end if
  n_trnslt = n_trnslt + mx*nymax*mz

  if (omp_in_parallel()) then
     call trnslt_omp (fin,fout,dxdy,dzdy,do_exp,nymax,mblocks,verbose)
  else
     !$omp parallel shared(dxdy,dzdy,do_exp,nymax)
     call trnslt_omp (fin,fout,dxdy,dzdy,do_exp,nymax,mblocks,verbose)
     !$omp end parallel
  end if
end subroutine trnslt


!-------------------------------------------------------------------------------
subroutine trnslt_omp (fin,fout,dxdy,dzdy,do_exp,nymax,mblocks,verbose)

  !  Translate a scalar field to an inclined coordinate system.
  !
  !  Operation count:  8m+10a = 18 flops/pnt
  !                    8m+6a  = 14 flops/pnt

  use params, only: mx, my, mz, izs, ize, dx, dz, mid, master, ym, mpi_rank, &
                    timer_verbose, omp_mythread
  use trnslt_m

  implicit none

  real, dimension(mx,my,mz), intent(in)  :: fin
  real, dimension(mx,my,mz), intent(out) :: fout
  real,                      intent(in)  :: dxdy, dzdy
  logical,                   intent(in)  :: do_exp
  integer,                   intent(in)  :: nymax, verbose
  integer,                   intent(in)  :: mblocks !deprecated
  ! mblocks: max number of subdomains communicated to fill ghost zones (deprecated)

  real, dimension(mx)    :: ftmp
  real, dimension(mx,mz) :: f, g

  integer  :: ix, iy, iz, k
  integer  :: lm1, lp0, lp1, lp2
  integer  :: mm1, mp0, mp1, mp2
  real     :: p, q, ad, bd, ac, bc, af, bf, xk, zk

  integer  :: jBlock, nyBlocks, kb 
  real     :: zkb
  real     :: zk1, zkNy
  integer, dimension(my) :: iyIniArr, iyEndArr, kBlockArr, kbArr, kArr
  real,    dimension(my) :: adArr, bdArr, acArr, bcArr

  real          :: eps = 1e-6
  logical, save :: mbwarning = .true.

  character(len=mid) :: id="$Id: trnslt_mpi.f90,v 1.1 2016/10/11 14:49:08 remo Exp $"

          if (timer_verbose>1) call timer('radiation','trnslt-begin')

  ! trap vertical rays

  if ( abs(dxdy) .lt. eps .and. abs(dzdy) .lt. eps ) then
     nvertical=nvertical+1

     if (do_exp) then
        do iz=izs,ize
           fout(:,:,iz) = exp(fin(:,:,iz))
        end do
     else
        do iz=izs,ize
           fout(:,:,iz) = fin(:,:,iz)
        end do
     end if

     return
  end if


  ! inclined rays
  if ( abs(dxdy).lt.eps ) then

     ! trnslt only in z direction
     call identify_blocks (my, nymax, mz, ym, dzdy, dz, nyBlocks, &
          iyIniArr, iyEndArr, kBlockArr, kbArr, kArr, adArr, bdArr, acArr, bcArr)

     !$omp master
     nztrnslt=nztrnslt+1
     allocate ( fin2 (mx, my, -1:mz+mz+2) )
     !$omp end master
     !$omp barrier

          if (timer_verbose>1) call timer('radiation','identify_z')
     call buffer_blocks_z (mx,my,mz,nymax,nyBlocks,fin,fin2,iyIniArr,iyEndArr,kBlockArr)
          if (timer_verbose>0) call timer('radiation','buffer_z')

     do iz=izs,ize
        do iy=1,nymax

           k  =  kArr(iy)
           kb = kbArr(iy)
           ad = adArr(iy)
           bd = bdArr(iy)
           ac = acArr(iy)
           bc = bcArr(iy)

           !  Interpolate using cubic splines
           mp0 = iz  + k - kb * mz
           mm1 = mp0 - 1
           mp1 = mp0 + 1 
           mp2 = mp0 + 2

           if (do_exp) then
              do ix=1,mx
                 ftmp(ix) = &
                      ac * fin2( ix, iy, mp0 ) + bc * fin2( ix, iy, mp1 ) - &
                      ad * fin2( ix, iy, mm1 ) + bd * fin2( ix, iy, mp2 )
              end do
              call expn (mx,ftmp,fout(1,iy,iz))
           else 
              do ix=1,mx
                 fout(ix,iy,iz) = &
                      ac * fin2( ix, iy, mp0 ) + bc * fin2( ix, iy, mp1 ) - &
                      ad * fin2( ix, iy, mm1 ) + bd * fin2( ix, iy, mp2 )
              end do
           end if

        end do
     end do
     ninterp = ninterp + (ize-izs+1)*nymax*mx

     !$omp barrier
     !$omp master
     deallocate ( fin2 )
     !$omp end master

          if (timer_verbose>0) call timer('radiation','interp_z')
     return

  else   if (abs(dzdy).lt.1e-6) then
     ! trnslt only in x direction
     call identify_blocks (my, nymax, mx, ym, dxdy, dx, nyBlocks, &
          iyIniArr, iyEndArr, kBlockArr, kbArr, kArr, adArr, bdArr, acArr, bcArr)

     !$omp master
     nxtrnslt=nxtrnslt+1
     allocate ( fin2( -1:mx+mx+2, my, mz ) )
     !$omp end master
     !$omp barrier

          if (timer_verbose>1) call timer('radiation','identify_x')
     call buffer_blocks_x (mx,my,mz,nymax,nyBlocks,fin,fin2,iyIniArr,iyEndArr,kBlockArr)
          if (timer_verbose>0) call timer('radiation','buffer_x')

     do iz=izs,ize
        do iy=1,nymax

           k  = kArr(iy)
           kb = kbArr(iy)
           ad = adArr(iy)
           bd = bdArr(iy)
           ac = acArr(iy)
           bc = bcArr(iy)

           !  Interpolate using cubic splines [4m+5a] [4m+3a]

           if (do_exp) then
              do ix=1,mx

                 lp0 = ix  + k - kb * mx
                 lm1 = lp0 - 1
                 lp1 = lp0 + 1 
                 lp2 = lp0 + 2

                 ftmp(ix) = &
                      ac * fin2( lp0, iy, iz ) + bc * fin2( lp1, iy, iz ) -  &
                      ad * fin2( lm1, iy, iz ) + bd * fin2( lp2, iy, iz )
              end do
              call expn ( mx, ftmp, fout(1,iy,iz) )
           else
              do ix=1,mx

                 lp0 = ix  + k - kb * mx
                 lm1 = lp0 - 1
                 lp1 = lp0 + 1 
                 lp2 = lp0 + 2

                 fout(ix,iy,iz) = &
                      ac * fin2( lp0, iy, iz ) + bc * fin2( lp1, iy, iz ) -  &
                      ad * fin2( lm1, iy, iz ) + bd * fin2( lp2, iy, iz )
              end do
           end if

        end do
     end do
     ninterp = ninterp + (ize-izs+1)*nymax*mx

     !$omp barrier
     !$omp master
     deallocate ( fin2 )
     !$omp end master

          if (timer_verbose>0) call timer('radiation','interp_x')
     return

  else         

     ! if ( abs(dxdy).ge.eps and abs(dzdy).ge.eps ) then

     ! part 1: trsnlt in z direction
     call identify_blocks (my, nymax, mz, ym, dzdy, dz, nyBlocks, &
          iyIniArr, iyEndArr, kBlockArr, kbArr, kArr, adArr, bdArr, acArr, bcArr)

     !$omp master
     nxztrnslt=nxztrnslt+1
     allocate ( fin2 (mx, my, -1:mz+mz+2) )
     !$omp end master
     !$omp barrier

          if (timer_verbose>1) call timer('radiation','identify_z')
     call buffer_blocks_z (mx,my,mz,nymax,nyBlocks,fin,fin2,iyIniArr,iyEndArr,kBlockArr)
          if (timer_verbose>0) call timer('radiation','buffer_z')

     do iz=izs,ize
        do iy=1,nymax

           k  =  kArr(iy)
           kb = kbArr(iy)
           ad = adArr(iy)
           bd = bdArr(iy)
           ac = acArr(iy)
           bc = bcArr(iy)

           !  Interpolate using cubic splines
           mp0 = iz  + k - kb * mz
           mm1 = mp0 - 1
           mp1 = mp0 + 1 
           mp2 = mp0 + 2

           ! note: do not exponentiate here! Exponentiation only at the end of part 2!
           do ix=1,mx
              fout(ix,iy,iz) = &
                   ac * fin2( ix, iy, mp0 ) + bc * fin2( ix, iy, mp1 ) - &
                   ad * fin2( ix, iy, mm1 ) + bd * fin2( ix, iy, mp2 )
           end do

        end do
     end do
     ninterp = ninterp + (ize-izs+1)*nymax*mx
          if (timer_verbose>0) call timer('radiation','interp_z')

     !$omp barrier
     !$omp master
     deallocate ( fin2 )
     !$omp end master

     ! part 2: trsnlt in x direction
     call identify_blocks (my, nymax, mx, ym, dxdy, dx, nyBlocks, &
          iyIniArr, iyEndArr, kBlockArr, kbArr, kArr, adArr, bdArr, acArr, bcArr)

     !$omp master
     allocate ( fin2( -1:mx+mx+2, my, mz ) )
     !$omp end master
     !$omp barrier

          if (timer_verbose>1) call timer('radiation','identify_x')
     call buffer_blocks_x (mx,my,mz,nymax,nyBlocks,fout,fin2,iyIniArr,iyEndArr,kBlockArr)
          if (timer_verbose>0) call timer('radiation','buffer_x')

     do iz=izs,ize
        do iy=1,nymax

           k  =  kArr(iy)
           kb = kbArr(iy)
           ad = adArr(iy)
           bd = bdArr(iy)
           ac = acArr(iy)
           bc = bcArr(iy)

           !  Interpolate using cubic splines

           if (do_exp) then
              do ix=1,mx

                 lp0 = ix  + k - kb * mx
                 lm1 = lp0 - 1
                 lp1 = lp0 + 1 
                 lp2 = lp0 + 2

                 ftmp(ix) = &
                      ac * fin2( lp0, iy, iz ) + bc * fin2( lp1, iy, iz ) -  &
                      ad * fin2( lm1, iy, iz ) + bd * fin2( lp2, iy, iz )
              end do
              call expn ( mx, ftmp, fout(1,iy,iz) )
           else
              do ix=1,mx

                 lp0 = ix  + k - kb * mx
                 lm1 = lp0 - 1
                 lp1 = lp0 + 1 
                 lp2 = lp0 + 2

                 fout(ix,iy,iz) = &
                      ac * fin2( lp0, iy, iz ) + bc * fin2( lp1, iy, iz ) -  &
                      ad * fin2( lm1, iy, iz ) + bd * fin2( lp2, iy, iz )
              end do
           end if

        end do
     end do
     ninterp = ninterp + (ize-izs+1)*nymax*mx

     !$omp barrier
     !$omp master
     deallocate ( fin2 )
     !$omp end master

          if (timer_verbose>0) call timer('radiation','interp_x')
  end if

end subroutine trnslt_omp


!-------------------------------------------------------------------------------
subroutine trnslt_test

  use params
  implicit none

  real, allocatable, dimension(:,:,:) :: fin,fout,fcmp
  integer  :: ix,iy,iz
  integer  :: count
  real     :: cpux,dxdy,dzdy,fdtime,dsdy,error,rmaxval_mpi
  integer  :: mblocks,verbose

  mx  = 100
  my  = mx
  mz  = mx
  do_trace = .true.
  mblocks  = 1
  verbose  = 0

  allocate (dxm(0:mx),dym(0:my),dzm(0:mz))
  allocate (xm(0:mx),ym(0:my),zm(0:mz))

  dxm = 1.
  dym = 1.
  dzm = 1.

  do ix=0,mx 
     xm(ix)=ix*dxm(1) 
  enddo
  do iy=0,my 
     ym(iy)=iy*dym(1) 
  enddo
  do iz=0,mz 
     zm(iz)=iz*dzm(1) 
  enddo

  call test_trnslt

  return

end subroutine trnslt_test


!-------------------------------------------------------------------------------
subroutine test_trnslt

  use params
  implicit none

  real, allocatable, dimension(:,:,:) :: fin,fout,fcmp
  integer  :: ix,iy,iz
  integer  :: count
  real     :: cpux,dxdy,dzdy,fdtime,kx,ky,kz,dsdy,error,rmaxval_mpi
  integer  :: mblocks,verbose

  return

  allocate (fin(mx,my,mz),fout(mx,my,mz),fcmp(mx,my,mz))

  return

  kx = 2.*pi/sx
  ky = 2.*pi/sy
  kz = 2.*pi/sz

  !  Testing x-direction

  dsdy = 1.
  dxdy = dsdy
  dzdy = 0.
  !$omp parallel private(iz)
  do iz=izs,ize
     do iy=1,my
        do ix=1,mx
           fin (ix,iy,iz) = cos(xm(ix)*kx               )+cos(ym(iy)*ky)+cos(zm(iz)*kz               )
           fcmp(ix,iy,iz) = cos(xm(ix)*kx+dxdy*ym(iy)*kx)+cos(ym(iy)*ky)+cos(zm(iz)*kz+dzdy*ym(iy)*kz)
        end do
     end do
  end do
  !$omp end parallel
  cpux = fdtime()
  cpux = 0.
  count = 0
  do while (cpux .lt. 0.5)
     count = count+1
     call trnslt(fin,fout,dxdy,dzdy,.false.,my,mblocks,verbose)
     cpux = cpux+fdtime()
     call broadcast_mpi (cpux, 1)
  end do
  error=rmaxval_mpi(maxval(abs(fout-fcmp)))
  if (master) print *,'trnslt_x : count, ns/pt, error =',count,1e9*cpux/real(mx*my*mz)/count,error

  !  Testing z-direction

  dxdy = 0.
  dzdy = dsdy
  !$omp parallel private(iz)
  do iz=izs,ize
     do iy=1,my
        do ix=1,mx
           fcmp(ix,iy,iz) = cos(xm(ix)*kx+dxdy*ym(iy)*kx)+cos(ym(iy)*ky)+cos(zm(iz)*kz+dzdy*ym(iy)*kz)
        end do
     end do
  end do
  !$omp end parallel
  cpux = fdtime()
  cpux = 0.
  count = 0
  do while (cpux .lt. 0.5)
     count = count+1
     call trnslt(fin,fout,dxdy,dzdy,.false.,my,mblocks,verbose)
     cpux = cpux+fdtime()
     call broadcast_mpi (cpux, 1)
  end do
  error=rmaxval_mpi(maxval(abs(fout-fcmp)))
  if (master) print *,'trnslt_z : count, ns/pt, error =',count,1e9*cpux/real(mx*my*mz)/count,error

  !  Testing xz-direction

  dxdy = dsdy
  dzdy = dsdy
  !$omp parallel private(iz)
  do iz=izs,ize
     do iy=1,my
        do ix=1,mx
           fcmp(ix,iy,iz) = cos(xm(ix)*kx+dxdy*ym(iy)*kx)+cos(ym(iy)*ky)+cos(zm(iz)*kz+dzdy*ym(iy)*kz)
        end do
     end do
  end do
  !$omp end parallel
  cpux = fdtime()
  cpux = 0.
  count = 0
  do while (cpux .lt. 0.5)
     count = count+1
     call trnslt(fin,fout,dxdy,dzdy,.false.,my,mblocks,verbose)
     cpux = cpux+fdtime()
     call broadcast_mpi (cpux, 1)
  end do
  error=rmaxval_mpi(maxval(abs(fout-fcmp)))
  if (master) print *,'trnslt_xz: count, ns/pt, error =',count,1e9*cpux/real(mx*my*mz)/count,error

  deallocate (fin,fout,fcmp)

end subroutine test_trnslt


!-------------------------------------------------------------------------------
subroutine identify_blocks (ny, nymax, ns, ydep, dsdy, ds, nyBlocks, &
     iyIniArr, iyEndArr, kBlockArr, kbArr, kArr, adArr, bdArr, acArr, bcArr)

  implicit none

  integer,                intent(in)  :: ny, ns, nymax
  real,    dimension(ny), intent(in)  :: ydep
  real,                   intent(in)  :: dsdy, ds

  integer,                intent(out) :: nyBlocks
  integer, dimension(ny), intent(out) :: iyIniArr, iyEndArr, kBlockArr, kbArr, kArr
  real,    dimension(ny), intent(out) :: adArr, bdArr, acArr, bcArr

  real     :: sk, skb, p, q, af, bf, ad, bd, ac, bc
  integer  :: iy, k, kb, jBlock

  ! The subroutine determines where (i.e., in which subdomains) rays with inclination
  ! dsdy cross the planes ydep=ydep(iy).
  !
  ! The "s" in ds, dsdy, ns, sk, skb  stands for either "x" or "z".
  !
  ! The subdomains intersected by the rays are identified by an integer (later
  ! stored in the kbArr array ) that indicates their position (distance) relative 
  ! to the local subdomain ("0"). 
  !
  ! Subdomains are divided in "blocks": each block is made up by consecutive
  ! ydep(iy) layers of a given subdomain that are intersected by the same ray. 
  ! The array kBlockArr contains the subdomain number corresponding to a given block; 
  ! iyIniArr and iyEndArr contain the top and bottom boundary of the block; 
  ! nyBlocks is the total number of blocks intersected by a ray.
  !
  ! adArr, bdArr, acArr, bcArr contain the cubic interpolation coefficients (so 
  ! that they don't need to be recomputed each time later on).


  ! NOTE: this version is NOT OpenMP-safe yet


  do iy=1,nymax

     sk  =  dsdy * ydep(iy) / ds
     k   =  floor(sk)
     skb =  sk / ns
     kb  =  floor(skb)

     p   =  sk  - k
     q   =  1.0 - p

     af  =  q+p*q*(q-p)
     bf  =  p-p*q*(q-p)
     ad  =  p*q*q*0.5
     bd  = -p*q*p*0.5
     ac  =  af - bd
     bc  =  bf + ad

     kArr (iy) = k 
     kbArr(iy) = kb

     adArr(iy) = ad
     bdArr(iy) = bd
     acArr(iy) = ac
     bcArr(iy) = bc

  end do


  jBlock       = 1

  iyIniArr(1)  = 1
  iyEndArr(1)  = 1
  kBlockArr(1) = kbArr(1)

  do iy=2,nymax

     if ( kbArr(iy).eq.kBlockArr(jBlock) ) then 

        iyEndArr(jBlock) = iy

     else

        jBlock  = jBlock  + 1
        kBlockArr(jBlock) = kbArr(iy) 
        iyIniArr(jBlock)  = iy
        iyEndArr(jBlock)  = iy

     end if

  end do


  nyBlocks = jBlock 

  return

end subroutine identify_blocks

!-------------------------------------------------------------------------------
