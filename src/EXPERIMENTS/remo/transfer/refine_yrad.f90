! $Id: refine_yrad.f90,v 1.1 2016/10/11 14:49:07 remo Exp $
!***********************************************************************
SUBROUTINE radiation_scale(ydep, dydep, yrad, dyrad, mtop, mbot, do_smooth)

  use params, only: my
  use cooling, only: lnrk
  
  implicit none

  real, dimension(my), intent(in)  :: ydep, dydep
  real, dimension(my), intent(out) :: yrad, dyrad
  logical, intent(in)              :: do_smooth
  integer, intent(out)             :: mtop, mbot

  integer :: ndtauTop, ndtauBot, iy
  real    :: dlnrkmax, drkmax
  real, dimension(my) :: indx, fndx, sfndx


  ! calculate tau and dtau on hydro depth scale
  call tau_calc(dydep, my, 1.0, mtop, mbot, ndtauTop, ndtauBot)


  fndx(:)    = 0.
  fndx(mtop) = 0.
  do iy = mtop+1, mbot
     dlnrkmax = maxval(abs(lnrk(:,iy,:)-lnrk(:,iy-1,:)))
     fndx(iy) = fndx(iy-1) + sqrt(dlnrkmax)
  end do


  ! Array with indexes for radiation scale yrad
  indx(1) = 1.
  do iy = 2, my
     indx(iy) = indx(iy-1) + 1.
  end do


  ! Smooth fndx
  sfndx(:)    = 0.
  sfndx(mtop) = fndx(mtop)
  if (do_smooth) then
     do iy = mtop+1, mbot-1
        sfndx(iy) = 0.5 * fndx(iy) + &
             0.5 * ((fndx(iy+1)-fndx(iy-1))/(ydep(iy+1)-ydep(iy-1)) * &
             (ydep(iy)-ydep(iy-1)) + fndx(iy-1))
     end do
  else
     do iy = mtop+1, mbot-1
        sfndx(iy) = fndx(iy)
     end do
  end if
  sfndx(mbot) = fndx(mbot)


  ! Normalize fndx array to span 1...my range
  drkmax = real(my-1) / fndx(mbot)
  do iy = mtop, mbot
     fndx(iy) = 1. + drkmax * sfndx(iy)
  end do


  ! Compute radiation scale yrad and dyrad(i)=yrad(i)-yrad(i-i) array
  call interp(mbot-mtop+1, fndx(mtop:mbot), ydep(mtop:mbot), my, indx, yrad, 2, 2)
  dyrad(1) = yrad(2) - yrad(1)
  do iy = 2, my
     dyrad(iy) = yrad(iy) - yrad(iy-1)
  end do

END SUBROUTINE radiation_scale


!***********************************************************************
SUBROUTINE yinter (ny,y,nt,yt,nx,nz,f,g)

  !  Interpolate to a new y-grid.
  !
  !  Update history:
  !
  !  22-dec-87/aake:  /czintr/ added to control extrapolation (not used
  !                   in simulation code, to be used in model resizing.
  !  11-jan-89/aake:  nx,ny -> mx,my in set to zero loop (nx,ny undefined)
  !  09-aug-89/aake;  added block data routine
  !  09-aug-89/aake;  /cscr/ instead of blank common
  !  22-aug-89/bob:   (l,m,n)->(lm,n)
  !
  !***********************************************************************

  implicit none
  integer                   :: nx,ny,nz,nt
  real, dimension(ny)       :: y
  real, dimension(nt)       :: yt
  real, dimension(nx,ny,nz) :: f,d
  real, dimension(nx,nt,nz) :: g
  real, parameter           :: iextr1=0
  real, parameter           :: iextrn=0
  real, parameter           :: eps=1.0e-4          
  ! If closer than eps to grid point, don't interpolate

  real    :: dy,py,qy, pyf,pyd,qyd
  integer :: n,k,iz


  !  Get derivatives of f
  call ydery(ny,y,nx,nz,f,d)


  !  Depth loop

  n=2
  do k=1,nt

     !  Find n such that  y(n-1) < yt(k) < y(n)
101  if (yt(k).gt.y(n)) then
        n=n+1
        if (n.gt.ny) then
           !nt=k-1
           goto 100
        endif
        goto 101
     endif


     !  Relative distances, check extrapolations
     dy=(y(n)-y(n-1))
     py=(yt(k)-y(n-1))/dy
     qy=1.-py

     if (py.lt.-eps) then

        if (iextr1.eq.0) then                ! Set to zero outside
           do iz=1,nz
              g(:,k,iz)=0.0
           enddo
           goto 100
        else if (iextr1.eq.1) then           ! Set to constant outside
           py=0.0
           qy=1.0
        endif

     else if (qy.lt.-eps) then

        if (iextrn.eq.0) then                ! Set to zero outside
           do iz=1,nz
              g(:,k,iz)=0.0
           enddo
           goto 100
        else if (iextrn.eq.1) then           ! Set to constant outside
           py=1.0
           qy=0.0
        endif

     endif


     if (abs(py).lt.eps) then                !  Close to one end ?
        do iz=1,nz
           g(:,k,iz)=f(:,n-1,iz)
        enddo
     else if (abs(qy).lt.eps) then
        do iz=1,nz
           g(:,k,iz)=f(:,n,iz)
        enddo

     else                                   !  Interpolate
        pyf=py-(qy*py)*(qy-py)
        pyd=-py*(qy*py)*dy
        qyd=qy*(qy*py)*dy
        do iz=1,nz
           g(:,k,iz)=f(:,n-1,iz)+pyf*(f(:,n,iz)-f(:,n-1,iz))+qyd*d(:,n-1,iz)+pyd*d(:,n,iz)
        enddo
     endif

  enddo   !END Y-LOOP

100 continue

END SUBROUTINE yinter


!***********************************************************************
SUBROUTINE ydery (ny,y,nx,nz,f,d)
  !
  !  The following LU decomposition of the problem was calculated by the
  !  startup routine:
  !
  !  | w2(1) w3(1)              |   |d|   |  1                    |   |e|
  !  |       w2(2) w3(2)        |   |d|   | w1(2)  1              |   |e|
  !  |          .     .         | * |.| = |       w1(3)  1        | * |.|
  !  |                .      .  |   |.|   |              .  .     |   |.|
  !  |                    w2(N) |   |d|   |                 w1  1 |   |e|
  !
  !  The solutions of all nx*ny equations are carried out in paralell.
  !
  !  4m+1d+4a = 9 flops per grid point, running at approx 1M grid pts/sec
  !  on the Univ of Colo Alliant FX-8.
  ! 
  !  Update history:
  !                      .
  !  20-aug-87: Coded by Ake Nordlund, Copenhagen University Observatory
  !  09-nov-87: zderz0() split off as separate routine
  !  27-sep-87: Removed bdry[123] parameters.
  !  02-oct-87: Moved factor 3 from zder loop to zder0 loop.
  !  15-oct-87: Factors a and b moved out of lm-loops 121. 13->9 flops.
  !  03-nov-87: zt parameter with subroutine zderz
  !  04-nov-87: inverted w2(), to turn divides into multiplications
  !  14-sep-88: combined loops l,m->lm
  !  20-jun-90: cubic-spline version
  !  spring-91: cubic at lower, finite diff at upper boundary
  !  08-may-91: combined all zder routines in one file
  !
  !***********************************************************************
  
  implicit none
  integer                   :: nx,ny,nz
  real, dimension(ny)       :: y
  real, dimension(3,ny)     :: w
  real, dimension(nx,ny,nz) :: f,d
  integer                   :: k,iz
  real                      :: a,b,cya,cyb
  real, dimension(nx)       :: dfa,dfb


  !  Check if we have a degenerate case (no z-extension)
  if (ny.eq.1) then
     do iz=1,nz
        d(:,1,nz)=0.0
     enddo
     return
  endif

  call ydery0(ny,y,w)


  !  First point
  do iz=1,nz
     d(:,1,iz)=(f(:,2,iz)-f(:,1,iz))/(y(2)-y(1))
  enddo


  !  Interior points [2m+2s]
  do k=2,ny-1
     a=1./(y(k+1)-y(k))**2
     b=1./(y(k)-y(k-1))**2
     do iz=1,nz
        d(:,k,iz)=(f(:,k+1,iz)-f(:,k,iz))*a+(f(:,k,iz)-f(:,k-1,iz))*b
     enddo
  enddo


  !  Last point
  cya=1./(y(ny-1)-y(ny-2))
  cyb=1./(y(ny)-y(ny-1))
  do iz=1,nz
     dfa(:)=f(:,ny-1,iz)-f(:,ny-2,iz)
     dfb(:)=f(:,ny,iz)-f(:,ny-1,iz)
     d(:,ny,iz)=2.*(dfa(:)*cya*cya*cya-dfb(:)*cyb*cyb*cyb)
  enddo


  !  Do the forward substitution [1m+1a]
  do iz=1,nz
     d(:,ny,iz)=d(:,ny,iz)+w(3,ny)*d(:,ny-1,iz)
  enddo

  do k=2,ny
     do iz=1,nz
        d(:,k,iz)=d(:,k,iz)+w(1,k)*d(:,k-1,iz)
     enddo
  enddo

  !  Do the backward substitution [1m+1d+1s]
  do iz=1,nz
     d(:,ny,iz)=d(:,ny,iz)*w(2,ny)
  enddo

  do k=ny-1,1,-1
     do iz=1,nz 
        d(:,k,iz)=(d(:,k,iz)-w(3,k)*d(:,k+1,iz))*w(2,k)
     enddo
  enddo

END SUBROUTINE ydery


!***********************************************************************
SUBROUTINE ydery0 (ny,y,w)
  !
  !  Calculates the matrix elements and an LU decomposition of the problem
  !  of finding the derivatives of a cubic spline with continuous second
  !  derivatives.
  !
  !  This particular version (to be used with the hd/mhd code with fixed
  !  boundary values at a fiducial extra boundary layer) has end point
  !  derivatives equal to one sided first order derivatives.  These are
  !  not used directly, and influence the solution only through the con-
  !  dition of a continuous second derivative at the next point in.
  !
  !  To make a "natural spline" (zero second derivative at the end pts),
  !  change the weights to
  !
  !     w2(1)=2./3.
  !     w3(1)=1./3.
  !     w2(nz)=2./3.
  !     w1(nz)=1./3.
  !
  !  For a general cubic spline, we have an equation system of the form:
  !
  !     | w2 w3          |   | d |     | e |
  !     | w1 w2 w3       |   | d |     | e |
  !     |       .  .     | * | . |  =  | . |
  !     |       .  .  .  |   | . |     | . |
  !     |          w1 w2 |   | d |     | e |
  !
  !  This may be solved by adding a fraction (w1) of each row to the
  !  following row.
  !
  !  This corresponds to an LU decomposition of the problem:
  !
  !     | w2 w3          |   | d |     | 1               |   | e |
  !     |    w2 w3       |   | d |     | w1  1           |   | e |
  !     |       .  .     | * | . |  =  |    w1  1        | * | . |
  !     |          .  .  |   | . |     |        .  .     |   | . |
  !     |             w2 |   | d |     |           w1  1 |   | e |
  !
  !  In this startup routine, we calculate the fractions w1.
  !  These are the same for all splines with the same z-scale,
  !  and only have to be calculated once for all nx*ny splines.
  !
  !  In principle, this startup routine only has to be called once,
  !  but to make the zder entry selfcontained, zder0 is called from
  !  within zder.  In practice, the zder0 time is insignificant.
  ! 
  !  Update history:
  !                      .
  !  20-aug-87: coded by Ake Nordlund, Copenhagen University Observatory
  !  27-sep-87: Removed bdry[123] parameters.
  !  02-oct-87: Moved factor 3 from zder loop to zder0 loop.
  !  04-nov-87: inverted w2(), to turn divides into multiplications
  !  09-nov-87: zderz0() split off as separate routine
  !  spring-91: cubic at lower, finite diff at upper boundary
  !  08-may-91: combined all zder routines in one file
  !
  !***********************************************************************

  implicit none
  integer               :: ny
  real, dimension(ny)   :: y
  real, dimension(3,ny) :: w 
  integer               :: k
  real                  :: a3,cya,cyb,c


  !  First point
  w(1,1)=0.0
  w(2,1)=1.0
  w(3,1)=0.0


  !  Interior points
  a3=1./3.
  do k=2,ny-1
     w(1,k)=a3/(y(k)-y(k-1))
     w(3,k)=a3/(y(k+1)-y(k))
     w(2,k)=2.*(w(1,k)+w(3,k))
  enddo


  !  Last point
  cya=1./(y(ny-1)-y(ny-2))
  cyb=1./(y(ny)-y(ny-1))
  w(1,ny)=cya*cya
  w(3,ny)=-cyb*cyb
  w(2,ny)=w(1,ny)+w(3,ny)


  ! eliminate at last point
  c=-w(1,ny)/w(1,ny-1)
  w(2,ny)=w(2,ny)+c*w(2,ny-1)
  w(3,ny)=w(3,ny)+c*w(3,ny-1)
  w(1,ny)=w(2,ny)
  w(2,ny)=w(3,ny)
  w(3,ny)=c


  !  Eliminate subdiagonal elements of rows 2 to n:
  !
  !     |     w2 w3       | * | . |  =  | . |
  !     |     w1 w2 w3    |   | . |     | . |

  do k=2,ny
     w(1,k)=-w(1,k)/w(2,k-1)
     w(2,k)=w(2,k)+w(1,k)*w(3,k-1)
  enddo


  !  Invert w(2,:), turns divides into mults
  do k=1,ny
     w(2,k)=1./w(2,k)
  enddo

END SUBROUTINE ydery0


!***********************************************************************
SUBROUTINE ydistr (nt,yt,ny,y,nx,nz,f,g)
  !
  !  Distribute to a new z-grid.
  !
  !  Update history:
  !
  !  22-dec-87/aake:  start at n=2 (was n=3)
  !  15-jan-88/aake:  factor 2 bug corrected
  !  09-aug-89/aake:  /cscr/ instead of blank common
  !  22-aug-89/bob:   (l,m,n)->(lm,n)
  !  24-apr-90/aake:  0.5*(z(n+1)-z(n-1)) inst. of z(n)-z(n-1)
  !
  !***********************************************************************

  implicit none
  integer                   :: nx,ny,nz,nt
  real, dimension(ny)       :: y
  real, dimension(nt)       :: yt
  real, dimension(nx,ny,nz) :: f
  real, dimension(nx,nt,nz) :: g

  real    :: dy,py,qy,dyt
  integer :: n,k,ix,iz,k1,k2,nm1,nm2,np1


  ! Find n such that z(n-1) < zt(k) < z(n)

  n=2

  ! reset g
  do iz=1,nz
     g(:,:,iz)=0.0
  enddo

  do k=1,nt
101  if (yt(k).gt.y(n)) then
        n=n+1
        if (n.gt.ny) then
           nt=k-1
           goto 100
        endif
        goto 101
     endif


     !  Weight factors proportional to distance from n-pts.
     dy=y(n)-y(n-1)
     py=(yt(k)-y(n-1))/dy
     qy=1.-py


     !  Input is energy per unit volume, z-integral should be conserved.
     !  We require the sum over g(n)*0.5*(z(n+1)-z(n-1)) to be the same
     !  as the sum over f(k)*0.5*(zt(k+1)-zt(k-1)).  Used to assume that
     !  0.5*(z(n+1)-z(n-1)) was equal to z(n)-z(n-1).  Although this was
     !  in practice so in the layers where radiation matters, it need not
     !  be, when we allow a non-equidistant mesh.

     k1=max0(k-1,1)
     k2=min0(k+1,nt)
     nm2=max0(n-2,1)
     nm1=max0(n-1,1)
     np1=min0(n+1,ny)
     dyt=yt(k2)-yt(k1)
     py=py*dyt/(y(np1)-y(nm1))
     qy=qy*dyt/(y(n)-y(nm2))


     !  Do for all x,z points
     do iz=1,nz
        do ix=1,nx
           g(ix,n-1,iz)=g(ix,n-1,iz)+qy*f(ix,k,iz)
           g(ix,n  ,iz)=g(ix,n  ,iz)+py*f(ix,k,iz)
        enddo
     enddo
  enddo

100 continue

END SUBROUTINE ydistr
