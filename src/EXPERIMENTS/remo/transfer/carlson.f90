!...............................................................................
subroutine carlson(nmu, mr, nr, xmu, imux, imuy, imuz, weights)

! Carlson quadrature A

  use params, only: mid, master
  implicit none

  character(len=mid) :: Id = 'carlson'

  ! number of distinct directional cosines
  integer, intent(in) :: nmu
  
  ! max number of rays per quadrant
  integer, intent(in) :: mr

  ! actual number of rays
  integer, intent(out) :: nr

  ! indexes of directional cosines for ray
  integer, dimension(mr), intent(out) :: imux, imuy, imuz

  ! directional cosines
  real, dimension(nmu),  intent(out) :: xmu
  
  ! ray weights (sum per quadrant normalised to 1)
  real, dimension(mr), intent(out) :: weights

  logical :: is_valid

  ! is number of rays per quadrant available in tabulated data?
  is_valid = (nmu.ge.2 .and. nmu.le.4)

  if (.not.is_valid) then
     if (master) then
        print '(x,a)', repeat("-",72)
        print '(x,a)', trim(Id) // &
             ': number of rays not available in tabulated data. Stop.'
        stop
     end if
  end if

  select case (nmu)
  case (2)
     nr = 3
     xmu  = (/ 0.33333333, 0.88191710 /)
     imux = (/ 1, 2, 1 /)
     imuy = (/ 2, 1, 1 /)
     imuz = (/ 1, 1, 2 /)
     weights = (/ &
          0.33333333, 0.33333333, 0.33333333 /)
  case (3)
     nr = 6
     xmu  = (/ 0.25819889, 0.68313005, 0.93094934 /)
     imux = (/ 2, 1, 3, 1, 2, 1 /)
     imuy = (/ 2, 3, 1, 2, 1, 1 /)
     imuz = (/ 1, 1, 1, 2, 2, 3 /)
     weights = (/ &
          0.15000001, 0.18333334, 0.18333334, 0.15000001, &
          0.15000001, 0.18333334 /)     
  case (4)
     nr = 10
     xmu  = (/ 0.21821789, 0.57735027, 0.78679579, 0.95118973 /)
     imux = (/ 3, 2, 1, 4, 2, 1, 3, 2, 1, 1 /)
     imuy = (/ 2, 3, 4, 1, 2, 3, 1, 1, 3, 1 /)
     imuz = (/ 1, 1, 1, 1, 2, 2, 2, 3, 3, 4 /)
     weights = (/ &
          0.09138352, 0.09138352, 0.12698138, 0.12698138, 0.07075469, &
          0.09138352, 0.09138352, 0.09138352, 0.09138352, 0.12698138 /)
  end select

  return
end subroutine carlson

!...............................................................................
