!$Id: transfer_feautrier_alt.f90,v 1.1 2016/10/11 14:49:07 remo Exp $
subroutine transfer (nx,ny,nz,nthick,nthin,dtau,s,q,surfi,do_surfi)

  ! note: this version is NOT OpenMP-safe (yet)

  use params, only: mid
  implicit none

  integer,                   intent(in)  :: nx, ny, nz, nthick, nthin
  real, dimension(nx,ny,nz), intent(in)  :: dtau, s
  real, dimension(nx,ny,nz), intent(out) :: q
  real, dimension(nx,nz),    intent(out) :: surfi
  logical,                   intent(in)  :: do_surfi

  call transfer_feautrier_alt (nx,ny,nz,nthick,nthin,dtau,s,q,surfi,do_surfi)

end subroutine transfer


!-------------------------------------------------------------------------------
subroutine transfer_feautrier_alt (nx,ny,nz,np,n1,dtau,s,q,xi,doxi)

  !  Solve the transfer equation, given optical depth and source function.
  !
  !  "Steins trick" is used; storing the sum of the three elements in each
  !  row of the tridiagonal equation system, instead of the diagonal ele-
  !  ment.  This allows accurate solutions to be otained for arbitrarily
  !  small optical depths.  Without this trick, round off errors become
  !  noticeable already when the smallest optical depth is less than the
  !  square root of the machine precisions.
  !
  !  This version works with P as a variable for k <= n1, and with
  !  Q = P - S for k > n1.  P is Feautriers P and S is the source function.
  !  The equation solved is  Q" = P - S for k<=n1, and Q" = Q - S" for k>n1.
  !  The answer returned is Q = P - S for all k.
  !
  !  This choice of equations produces a high precision in Q at large optical
  !  depths (where P - S would have round off), while at the same time
  !  retaining the precision at small optical depths (where S" becomes very
  !  large and eventually would cause round off errors in the back
  !  substitution).
  !
  !  This is a second order version.  For simulations, with rapidly
  !  varying absorption coefficients and source functions, this is to be
  !  preferred over spline and Hermitean versions because it is positive
  !  definite, in the sense that a positive source function is guaranteed
  !  to result in a positive average intensity p.  Also, the flux
  !  divergence is exactly equal to q, for the conventional definition
  !  of the flux.
  !
  !  Operation count: 5d+5m+11a = 21 flops
  !
  !  Timings:
  !            Alliant: 0.58 * 21 / 1.25 = 9.7 Mfl @ 31*31*31
  !
  !  Update history:  This routine is based on transq, with updates

  use params, only: mid
  implicit none

  integer,                   intent(in)  :: nx, ny, nz, np, n1
  real, dimension(nx,ny,nz), intent(in)  :: dtau,s
  real, dimension(nx,ny,nz), intent(out) :: q
  real, dimension(nx,nz),    intent(out) :: xi
  logical,                   intent(in)  :: doxi

  real, dimension(nx,ny)    :: sp1,sp2,sp3
  real, dimension(nx)       :: ex
  integer :: k, l, iz, n1p
  real    :: a, taum, dinv

  character(len=mid) :: Id = 'transfer_feautrier_alt'


  call print_id (Id)

  !  Calculate 1-exp(-tau(l,1,iz)) straight or by series expansion,
  !  based on max tau(l,1,iz).

  do iz=1,nz

     do l=1,nx
        if (dtau(l,1,iz).gt.0.1) then
           ex(l)=1.-exp(-dtau(l,1,iz))
        else
           ex(l)=dtau(l,1,iz)*(1.-.5*dtau(l,1,iz)*(1.-.3333*dtau(l,1,iz)))
        end if
     end do

     !  Boundary condition at k=1.     
     do l=1,nx
        sp2(l,1)=dtau(l,2,iz)*(1.+.5*dtau(l,2,iz))
        sp3(l,1)=-1.
        q(l,1,iz)=s(l,1,iz)*dtau(l,2,iz)*(.5*dtau(l,2,iz)+ex(l))
     end do

     !  Matrix elements for k>2, k<np: [3d+3s]
     do k=2,np-1
        do l=1,nx
           sp2(l,k)=1.
           dinv=2./(dtau(l,k+1,iz)+dtau(l,k,iz))
           sp1(l,k)=-dinv/dtau(l,k,iz)
           sp3(l,k)=-dinv/dtau(l,k+1,iz)
        end do
     end do

     !  Right hand sides, for k>2, k<n1     
     n1p=min0(max0(n1,2),np-3)
     do k=2,n1p-1
        do l=1,nx
           q(l,k,iz)=s(l,k,iz)
        end do
     end do

     !  RHS for k=n1p.  The equation is still in p(k), but q(k+1) is now
     !  q(k+1)=p(k+1)-s(k+1), so to get p(k+1) we have to add s(k+1) to the
     !  LHS, that is subtract it from the RHS.
     if (n1p.le.np) then
        do l=1,nx
           q(l,n1p,iz)=s(l,n1p,iz)-sp3(l,n1p)*s(l,n1p+1,iz)
        end do
     end if

     !  RHS for k=n1p+1.  The equation is now in Q: Q" = Q - S", but q(k-1)
     !  is now p(k-1)=q(k-1)+s(k-1), so we must add sp1*s(l,k-1,iz) to the RHS,
     !  relative to the case below.
     if (n1p+1.le.np) then
        do l=1,nx
           q(l,n1p+1,iz)=sp1(l,n1p+1)*s(l,n1p+1,iz) &
                +sp3(l,n1p+1)*(s(l,n1p+1,iz)-s(l,n1p+2,iz))
        end do
     end if

     ! k>n1p+1,<np [2m+2a]
     do k=n1p+2,np-1
        do l=1,nx
           q(l,k,iz)=sp1(l,k)*(s(l,k,iz)-s(l,k-1,iz)) &
                +sp3(l,k)*(s(l,k,iz)-s(l,k+1,iz))
        end do
     end do

     !  k=np     
     do l=1,nx
        sp2(l,np)=1.
        q(l,np,iz)=0.0
     end do

     !  Eliminate subdiagonal, save factors in sp1 [1d+2m+4a]
     do k=1,np-2
        do l=1,nx
           a=-sp1(l,k+1)/(sp2(l,k)-sp3(l,k))
           q(l,k+1,iz)=q(l,k+1,iz)+a*q(l,k,iz)
           sp2(l,k+1)=sp2(l,k+1)+a*sp2(l,k)
           sp2(l,k)=sp2(l,k)-sp3(l,k)
        end do
     end do
     do l=1,nx
        sp2(l,np-1)=sp2(l,np-1)-sp3(l,np-1)
     end do

     !  Backsubstitute [1d+1m+1a]
     do k=np-1,1,-1
        do l=1,nx
           q(l,k,iz)=(q(l,k,iz)-sp3(l,k)*q(l,k+1,iz))/sp2(l,k)
        end do
     end do

     !  Subtract S in the region where the equation is in P
     do k=1,n1p
        do l=1,nx
           q(l,k,iz)=q(l,k,iz)-s(l,k,iz)
        end do
     end do

     !  Surface intensity.     
     if (doxi) then
        do l=1,nx
           !if (l<10 .and. iz==1 .and. master) print '(a,5g12.3)','tr:',xi(l,iz),ex(l),q(l,1,iz),s(l,1,iz)
           xi(l,iz)=2.*(1.-ex(l))*(q(l,1,iz)+s(l,1,iz))+s(l,1,iz)*ex(l)**2
        end do
     end if

  end do  !iz

end subroutine transfer_feautrier_alt
