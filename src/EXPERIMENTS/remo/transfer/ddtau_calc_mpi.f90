
!***********************************************************************
subroutine ddtau_calc( dtau, s, dsdtau, d2sdtau2, dtauN1, s0, sN1)
  
  ! Compute first and second derivative of function s (i.e., source function)
  ! with respect to tau
  !
  ! The routine also returns dtauN1, the value of dtau in the "N+1" layer)
  ! and s0 and sN1 ( the values of the source function at layers 0 and N+1, 
  ! respectively)
  !
  !  ds1dtau  = first derivative at half-points (first-order accuracy);
  !             Note that ds1dtau(i) = derivative at half point i-1/2;
  !  dsdtau   = first derivatives, centred; second-order accuracy;
  !  d2sdtau2 = second derivatives, centred; second-order accuracy.

  use params, only: mid, mx, my, mz, mpi_y, mpi_ny, izs, ize

  implicit none

  character(len=mid):: id='$Id: ddtau_calc_mpi.f90,v 1.1 2016/10/12 13:08:47 remo Exp $'

  real, dimension(mx,my,mz), intent(in)  :: dtau, s
  real, dimension(mx,my,mz), intent(out) :: dsdtau, d2sdtau2

  real, dimension(mx,1,mz),  intent(out) :: dtauN1   ! buffer containing dtau(n+1)=tau(n+1)-tau(n)
  real, dimension(mx,1,mz),  intent(out) :: s0, sN1  ! buffer containing source function for layers 0 and n+1

  real, dimension(mx,my,mz)       :: ds1dtau

  real, dimension(mx,1,mz)        :: ds1dtauN1       ! buffer containing value of ds1dtau derivative at n+1
  real, dimension(mx)             :: dinv            ! buffer used to store 1 / (dtau(n+1)+dtau(n))

  integer                         :: ix, iy, iz 
  

  call print_id (id)


  ! Send and receive information at boundary layers

  ! boundary layers: source function, top (s0)  and bottom (sN1)
  call mpi_send_bdry_y_up(s, 1, s0)
  call mpi_send_bdry_y_dn(s, 1, sN1)

  ! boundary layers: dtau, bottom only
  call mpi_send_bdry_y_dn(dtau, 1, dtauN1)

  ! update source function at top boundary, top domain
  ! NOTE: this step is not strictly necessary: the derivatives at the top-boundary are 
  ! anyway re-conditioned later (extrapolated); it just ensures that the source function
  ! is not discontinuous at the boundary

  if (mpi_y .eq. 0) then
     do iz=izs,ize
        s0(:, 1,iz) = s(:, 1,iz)
     end do
  end if

  ! update source function and dtau at bottom boundary, bottom domain: set equal to my
  ! NOTE: this step is not strictly necessary: the derivatives at the top-boundary are 
  ! anyway re-conditioned later (extrapolated); it just ensures that the source function
  ! is not discontinuous at the boundary

  if (mpi_y .eq. mpi_ny-1 ) then
     do iz=izs,ize
        sN1   (:, 1,iz) = s   (:,my,iz)
        dtauN1(:, 1,iz) = dtau(:,my,iz)
     end do
  end if


  ! Calculate ds1dtau

  ! Note that dtau(i) = tau(i)-tau(i-1)
  do iz=izs,ize
     ds1dtau(:, 1,iz) = (s(:, 1,iz) - s0(:, 1,iz)) / dtau(:, 1,iz)
     do iy=2,my
        ds1dtau(:,iy,iz) = (s(:,iy,iz) - s(:,iy-1,iz)) / dtau(:,iy,iz)
     end do
     ds1dtauN1(:,1,iz) = (sN1(:,1,iz) - s(:,my,iz)) / dtauN1(:, 1,iz)
  end do

  ! Calculate dsdtau and ds2dtau2
  do iz=izs,ize
     do iy=1,my-1
        dinv  (:)         = 1.0 / (dtau(:,iy+1,iz) + dtau(:,iy,iz))       
        dsdtau(:,iy,iz)   = (ds1dtau(:,iy+1,iz)*dtau(:,iy,iz) + ds1dtau(:,iy,iz)*dtau(:,iy+1,iz)) * dinv(:)
        d2sdtau2(:,iy,iz) = (ds1dtau(:,iy+1,iz) - ds1dtau(:,iy,iz)) * dinv(:) * 2.0
     end do
     dinv(:)           = 1.0 / (dtauN1(:,1,iz) + dtau(:,my,iz))  
     dsdtau  (:,my,iz) = (ds1dtauN1(:,1,iz)*dtau(:,my,iz) + ds1dtau(:,my,iz)*dtauN1(:, 1,iz)) * dinv(:)
     d2sdtau2(:,my,iz) = (ds1dtauN1(:,1,iz) - ds1dtau(:,my,iz)) * dinv(:) * 2.0
  end do


  ! Finally, linearly extrapolate dsdtau and d2sdtau2 at top and bottom boundary
  ! (top and bottom subdomains only, respectively)

  ! top boundary:
  if (mpi_y .eq. 0) then
     do iz=izs,ize
        dsdtau  (:, 1,iz) = 2. * ds1dtau(:, 2,iz) -  dsdtau(:, 2,iz)
        d2sdtau2(:, 1,iz) = 2. * (dsdtau(:, 2,iz) - ds1dtau(:, 2,iz)) / dtau(:, 2,iz)
     end do
  end if

  ! bottom boundary:
  if (mpi_y .eq. mpi_ny-1) then
     do iz=izs,ize
        dsdtau  (:, my,iz) = 2. *  ds1dtau(:, my,iz) -  dsdtau(:, my-1,iz)
        d2sdtau2(:, my,iz) = 2. *  (dsdtau(:, my,iz) - ds1dtau(:, my-1,iz)) / dtau(:, my,iz)
     end do
  end if

  ! dsdtau and d2sdtau2 derivatives: done.


end subroutine ddtau_calc
