! $Id: tau_calc.f90,v 1.1 2016/10/11 14:49:07 remo Exp $
!***********************************************************************
subroutine tau_calc(dydep, nydep, costh,mTauTop,mTauBot,nDTauTop,nDTauBot)

  use params, only: izs, ize, mx, my, mz, lb
  use cooling, only: lnrk, rk, dtau, tau, tauMin, tauMax, dtauMin, dtauMax
  
  implicit none

  integer, intent(in)                      :: nydep
  real,    intent(in)                      :: costh
  real,    intent(in), dimension(my)       :: dydep
  integer, intent(out)                     :: mtauTop, mtauBot, ndtauTop, ndtauBot


  real     :: f0, f1
  real     :: tauTop, tauBot
  integer  :: nTop, nBot
  integer  :: ix, iy, iz


  !$omp barrier

  !$omp single
  mTauTop  = 1
  mTauBot  = 2
  nDtauTop = 2
  nDtauBot = 2
  !$omp end single

  nTop     = 2
  nBot     = 2


  f0 = 1.0 / costh
  f1 = 0.5 / costh
  do iz=izs,ize
     rk(:,1,iz)   = exp(lnrk(:,1,iz))
     dtau(:,1,iz) = dydep(1) * rk(:,1,iz) * f0
     tau(:,1,iz)  = dtau(:,1,iz) 
     do iy=2,nydep
        rk(:,iy,iz) = exp(lnrk(:,iy,iz))
        do ix=1,mx
           dtau(ix,iy,iz) = dydep(iy) * (rk(ix,iy-1,iz)+rk(ix,iy,iz)) * f1
           tau(ix,iy,iz)  = tau(ix,iy-1,iz) + dtau(ix,iy,iz) 
           if (dtau(ix,iy,iz) < dTauMin)  nTop = max(nTop,iy)
           if (dtau(ix,iy,iz) < dTauMax)  nBot = max(nBot,iy)
        end do
     end do
  end do

  !$omp critical

  nDTauTop  = max(nTop,nDTauTop)
  nDtauBot  = max(nBot,nDtauBot)

  do iy=1,nydep
     tauTop = minval(tau(:,iy,:))
     tauBot = maxval(tau(:,iy,:))
     if (tauBot < tauMin)  mTauTop=iy
     if (tauTop < tauMax)  mTauBot=iy
  enddo
  mTauBot = max(lb+2,mTauBot)
  mTauTop = max(lb,  mTauTop)

  !$omp end critical

  !$omp barrier

end subroutine tau_calc
