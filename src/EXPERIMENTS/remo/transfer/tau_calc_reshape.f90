! $Id: tau_calc_reshape.f90,v 1.1 2016/10/11 14:49:07 remo Exp $
!***********************************************************************
subroutine tau_calc(dydep, nydep, costh, mtauTop, mtauBot, ndtauTop, ndtauBot)

  use cooling, only: lnrk, rk, dtau, tau, tauMin, tauMax, dtauMin, dtauMax
  use reshape_mod, only: dim_re

  implicit none

  integer, intent(in)                      :: nydep
  real,    intent(in)                      :: costh
  real,    intent(in), dimension(dim_re(2)):: dydep
  integer, intent(out)                     :: mtauTop, mtauBot, ndtauTop, ndtauBot

  real     :: f0, f1
  real     :: tauTop, tauBot
  integer  :: nTop, nBot
  integer  :: ix, iy, iz
  integer  :: nx, ny, nz

  ! dimensions of data cubes (lnrk, etc.)
  nx  = dim_re(1)
  ny  = dim_re(2)
  nz  = dim_re(3)

  ! reset 
  mTauTop  = 1
  mTauBot  = 2
  nDtauTop = 2
  nDtauBot = 2

  nTop     = 2
  nBot     = 2

  f0 = 1.0 / costh
  f1 = 0.5 * f0
  do iz=1,nz
     rk  (:,1,iz) = exp(lnrk(:,1,iz))
     dtau(:,1,iz) = dydep(1) * rk(:,1,iz) * f0
     tau (:,1,iz) = dtau(:,1,iz) 
     do iy=2,nydep
        rk(:,iy,iz) = exp(lnrk(:,iy,iz))
        dtau(:,iy,iz)  = dydep(iy) * (rk(:,iy-1,iz) + rk(:,iy,iz)) * f1
     end do !iy
  end do !iz
                                                           call timer('tau_calc','dtau')
  do iy=3,nydep
    if (minval(dtau(:,iy,:)) < dtauMax) nBot=max(nBot,iy)
    if (maxval(dtau(:,iy,:)) < dtauMin) nTop=max(nTop,iy)
  end do

  ndtauTop  = max(nTop,ndtauTop)
  ndtauBot  = max(nBot,ndtauBot)
                                                           call timer('tau_calc','nBot')

  do iy=2,nydep
     tau(:,iy,:) = tau(:,iy-1,:) + dtau(:,iy,:)
     tauTop = minval(tau(:,iy,:))
     tauBot = maxval(tau(:,iy,:))
     if (tauBot < tauMin)  mtauTop = iy
     if (tauTop < tauMax)  mtauBot = iy
  end do
                                                           call timer('tau_calc','mtauBot')

end subroutine tau_calc
