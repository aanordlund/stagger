# README

## Stagger: Equation of state and opacity binning tables

The equation of state (EOS) and opacity binning data are tabulated and stored in two separate files, “EOSrhoe.tab” and “kaprhoT.tab”, respectively.

Both table files are in F77-unformatted big-endian binary format. 
The routines to read and look up the tables are given in the enclosed “eosrad_lookup.f90” source file.

## Content and structure of the table files

The table files are given in the “tables” folder. A description of the contents and structure of the EOS and opacity binning files can be found here below.

The “tables/aux” folder contains some additional files that are not necessary for running the simulations, but that included information about the adopted chemical composition and opacity binning specification in human readable format.

The file “Xsubs_AGSS09+0.00.dat” contains a list of the 17 most abundant elements in the Sun which are considered in the EOS. Their abundances are given in the usual logarithmic scale where the abundance of H is normalised to 12. Their atomic masses are also given at the end.

The file “radtab.in.bin.e11” is the input file used by the program that generates the opacity binning table. The upper part of the file contains a list of options for the code; the remaining part contains the binning specifications for the particular table provided here. (This information is also contained in the header of the opacity binning file too, but it is included here for convenience.)

The binning specification is given as follows in the input file:

	-9.00, 0.00, 1.46,     0.,   3809., .x1, xx, x2, lmbd1, lmbd2
	1.46, 6.00, 3.81,     0.,   3809., .x1, xx, x2, lmbd1, lmbd2
	3.81, 6.00,15.00,     0.,   3809., .x1, xx, x2, lmbd1, lmbd2
	…

Each line contains the information about the bin boundaries in wavelength - “opacity strength” parameter space.
The second column can be disregarded for the present purposes: it’s meaningful only when the table code is run with a different set of options than the ones considered here.
The first and third columns list the values lower and upper boundaries for the “opacity strength” for each bin, the fourth and fifth columns provide the wavelength range (in ångstroms) covered by the bin. The “opacity strength” is defined as the inverse of the Rosseland optical depth of formation for a given wavelength \lambda as referred to the mean atmospheric stratification of the simulation. More precisely, the opacity strength is computed by evaluating -\log\tau\_Ross(\tau_\lambda=1) on the mean stratification. The mean stratification is typically based on a previous simulation run. (We normally have several iterations before the final production run to ensure consistency of the mean stratification between table and simulation.)

### EOS file

The EOS table file contains data for the following variables, tabulated as a function of density (r) and internal energy per unit mass (ee):

1. Pressure
2. Rosseland extinction coefficient
3. Temperature
4. Electron number density
5. Planck-averaged extinction coefficient
6. Extinction coefficient at \lambda=500 nm

Notes:
- The pressure is actually the sum of the contributions from gas and radiation. The contribution from the latter is estimated from radiative transfer calculations using a mean temperature stratification. The ratio of radiation to gas pressure is in any case small in the photosphere and upper part of the solar convection zone.
- The extinction coefficient has the dimensions of an inverse length.
- All variables in the present EOS table are in cgs units.

The numerical values stored in the tables are actually the **natural logarithms (ln)** of the above quantities. The variables are tabulated on regular rectangular grids as a function of ln density (lnr) and ln energy per unit mass (lnee). The ln density and ln energy per unit mass are tabulated on a uniform grid with constant stepping dlnr and dlnee, respectively.

The definition of internal energy depends on the adopted equation of state. In particular, the zero point of the internal energy is arbitrary, so it is paramount that the internal energy stored in the 3D snapshots is consistent with that.

The table file includes a header with information about adopted elemental composition, number of variables, number of ln density and ln energy per unit mass points, lower and upper limits for the ln density and ln energy per unit mass, and conversion factors  from table units to cgs units.

The EOS table file **also contains the values of the partial derivatives** of the ln of the above variables with respect to lnee and with respect to lnr. The derivatives are used by the look-up routines for interpolating between tabulated values using bi-cubic interpolation.

**Note that the values of the derivatives with respect to lnee and lnr stored in the table file have been multiplied by dlnee and dlnr, respectively. In other words, in order to retrieve the actual derivatives, it is necessary to divide the corresponding entries in the table by dlnee and dlnr, respectively.**


### Opacity binning file

The opacity binning table file contains Nlam sets of the following variables, with Nlam being the number of opacity bins:

1. Ratio of the mean (bin-avarage) extinction coefficient and Rosseland extinction coefficients.
2. \epsilon\_\lambda B(T)\lambda d\lambda integrated over all wavelengths in the bin; \epsilon\_\lambda is the destruction probability and B(T)\_\lambda is the Planck function at temperature T.
3. Mean destruction probability \epsilon (bin average).

All of the above variables are assumed in cgs units and stored in ln scale; they are tabulated on regular rectangular grids as a function of ln density (ln r)  and ln temperature (ln T); the ln density and ln temperature values are tabulated on uniform grids with constant stepping in ln r and ln T. Contrary to the EOS table, no derivatives of the above variables are stored; the associated look-up routines use a simple linear interpolation.

In the tables provided here, we make the assumption of LTE with complete thermalisation of the radiation field so that epsilon is identically equal to one for all wavelengths, so that variable nr. 2 becomes simply the ln of the integrated Planck function and variable nr. 3 is, obviously, ln 1 = 0 for all density-temperature pairs.

At the end of the table file, three additional variables are stored:

1. ln Rosseland extinction coefficient;
2. ln Planck-averaged extinction coefficient;
3. ln extinction coefficient at 500 nm.

These last three variables are also tabulated for the same ln density and ln temperature values as the other quantities.

The opacity binning file contains a header with information about the size of the rectangular tables, upper and lower limits for the ln density and ln temperature scales, ln density and ln temperature steps, number of opacity bins, and the actual definition of the opacity binning realisation (i.e. binning boundaries in terms of wavelength and opacity strength ranges).

## Look-up routines
Fortran look-up routines for the various tables can be found in the “eosrad_lookup.f90” source code. The basic routines are “lookup_eos” and “lookup_rad” for the EOS and opacity binning tables, respectively. There are a number of routines that act as wrappers to the above two routines (e.g. “pressure”, “temperature”, “rosseland”) and are simply there for convenience and for improving code legibility. As mentioned above, “lookup_rad” uses a simple bi-linear interpolation scheme, while “lookup_eos” relies on a bi-cubic interpolation scheme. (Note: technically, it is not a full bi-cubic interpolation because we do not consider terms dependent on second derivatives of the variables with respect to density and energy per unit mass, but it is not a significant issue here.)

## Implementation of the tables within the code
The EOS and opacity binning are loaded only once at the beginning of the workflow and stored locally in memory by each MPI process for the entire duration of the run.

In the code, the EOS table is primarily used to look up the gas pressure and its derivatives for defining the boundary conditions and calculating the pressure-dependent terms in the Navier-Stokes equations.

The opacity binning table is looked up together with the EOS table exclusively by the radiative transfer module.
First, temperature and Rosseland mean extinction coefficient are looked up from the EOS table (as a function of ln density and ln energy per unit mass). The temperature is needed for looking up variables from the opacity binning tables, which are tabulated as a function of ln density and ln temperature. 

Note that we look up the Rosseland extinction coefficient from the EOS table because it has higher accuracy than the opacity binning table (allows for bi-cubic interpolation instead of just bi-linear). Having said that, the Rosseland extinction coefficient stored in the opacity binning table is fully consistent with the one stored in the EOS table.

We then loop over all available bins to solve the radiative transfer equation and compute the heating rates (and outgoing intensities at the surface).
For each bin, we look up the bin-integrated source function and the ratio of the bin-averaged mean extinction and Rosseland extinction coefficients and the bin-integrated source (Planck) function. The extinction coefficient for the bin is computed simply by multiplying the Rosseland extinction coefficient times the above ratio.
With the source function and extinction coefficient in place we can then solve the radiative transfer for various inclined rays and calculate the necessary heating rates.

## Simulation snapshot (IDL)
A snapshot of solar simulation computed with the Stagger code with the above EOS and opacity binning tables is provided in the “data” folder. The original snapshot has been processed so that all physical variables are defined at the centre of grid-cells; only the physical layers are included (i.e., ghost zones have been removed); z is the vertical direction. 
The file is in IDL “save” format and contains an IDL data structure with the following quantities:

	mx   = number of grid points in x direction
	my   = number of grid points in y direction
	mz   = number of grid points in z direction
	x    = x coordinates of the cell centres
	y    = y coordinates
	z    = z coordinates
	rho  = density 
	ux   = x velocity component 
	uy   = y velocity component
	uz   = z velocity component
	e    = internal energy per unit **volume**
	temp = temperature
 
**All variables are in cgs units!**

## IDL structures with EOS and opacity binning table data
For convenience, IDL data structures with the contents of the EOS and opacity binning tables. The IDL data structures are stored in the “tables/EOSrhoe_kaprhoT.idl” file. A summary of the most important variables stored therein is given below. 
Please **note that the names of the variables in the IDL data structures are slightly different from the ones in the Fortran routines.**
Also, note that the IDL version of the EOS table also include

**EOSDATA** structure contents:

	Netab = number of tabulated ln energy per unit mass points
	Nrtab = number of tabulated ln density points
	Ma = first dimension of auxiliary array arr
	Na = second dimension of arr
	Ntbvar = number of variables in IDL EOS table 
	Nelem  = number of chemical elements
	lnrmin = lower boundary for tabulated ln density values
	lnrmax = upper boundary for ln density
	lnemin = lower boundary for tabulated ln energy per unit mass
	lnemax = upper boundary for ln energy per unit mass
	ul  = conversion factor from table to cgs units for length
	ur  = conversion factor for denisty
	ut  = conversion factor for time
	iel   = list of elements considered in the composition
	abund = elemental abundances (log scale with abund(H)=12)
	arr   = auxiliary array (currently empty)
	tab   = actual table
	labels = array variable names
  
The dimensions of the “tab” array are Netab x Nrtab x 3 x Ntbvar. The third index indicates whether the values stored in the particular entry in the table are the variable itself (index =0 in IDL), its derivative with respect to energy per unit mass (index=1), or its derivative with respect to density (index=2).

**KAPDATA** structure contents:

	NTtab = number of tabulated ln temperature points
	Nrtab = number of tabulated ln density points
	Ma = first dimension of auxiliary array arr
	Na = second dimension of arr
	Nlam  = number of opacity bins
	Nelem = number of chemical elements
	lnrmin = lower boundary for tabulated ln density values
	lnrmax = upper boundary for ln density
	lntmin = lower boundary for tabulated ln temperature 
	lntmax = upper boundary for ln temperature
	rul  = conversion factor from table to cgs units for length
	rur  = conversion factor for denisty
	rut  = conversion factor for time
	iel   = list of elements considered in the composition
	abund = elemental abundances (log scale with abund(H)=12)
	arr   = auxiliary array (currently empty)
	tab   = actual table
	labels = array variable names
  
The dimensions of the “tab” array are NTtab x Nrtab x 3 x (Nlam+1). The first Nlam values of the fourth index refer to the opacity bins (Nlam in total). The third index indicates whether the values stored for a particular bin  are the bin-averaged extinction coefficient relative to Rosseland extinction (index =0 in IDL), the bin-integrated Planck function (index=1) or the bin-averaged destruction probability (index=2). The last value of the fourth index is reserved to storing the Rosseland mean extinction coefficient, the Planck-averaged mean extinction coefficient and the standard extinction coefficient at 500nm.