README
======

# Stagger IDL tutorial
This guide is meant as a quick introduction to the analysis of data Stagger simulations using IDL. The following instructions assume that the user has access to R. Collet's IDL library installed under `~/idl`:

To begin, launch IDL from the command line:

	idl

From within IDL, launch the IDL Stagger startup script to include all relevant IDL libraries in the main path (`!path` environment variable):

	@~/idl/start/idl_start_stagger

## Open a scratch/snapshot file
Go to simulation's directory, e.g.:

	dir = '~/stagger_sims/120x120x120/sun/2011-01-20'
	cd, dir

Open scratch file for reading (swap endianness, if necessary):

	fname = 'sun03.scr'
	fmesh = 'sun03.msh'
	open, a, fname, mesh=fmesh, /swap_endian

You can also retrieve the full list of `.dat` snapshots using the following command:

	flist = file_search('sun03_*.dat', count=nflist)

This will result in an array of strings `flist` with `nflist` entries, one per  `sun03_*.dat` snapshot found by the `file_search` routine. If no snapshots are exist with filenames corresponding to the pattern passed as argument, the routine will return an empty array and `nflist=-1`.

Assuming `flist` is not empty, you can load any snapshot from the list (e.g. snapshot number `i=10`) by typing:

	i=10
	open, a, flist[i], mesh=fmesh, /swap_endian


More in detail, the routine `open.pro` opens the scratch/snapshot file `fname` and assigns a pointer `a` to its contents. You can display the type of variable `a` by typing:

	help, a

This command will produce an output like the following:

	A               FLOAT     = File<sun02.scr> Array[120, 120, 120]

This indicates that `a` is a pointer to 3D floating-point data cubes with dimensions 120x120x120 in file `sun03.scr`; `a[0]` points (corresponds) to the first data cube in the file, `a[1]` to the second, etc.
The dimensions of the data cubes are accessible via common-block variables (`nx`, `ny`, and `nz`):

	help,nx,ny,nz

which should yield the following:

	NX (CDATA)      LONG      =          120
	NY (CDATA)      LONG      =          120
	NZ (CDATA)      LONG      =          120


A standard Stagger scratch/snapshot file contains the following data cubes, in the following order:

- 0: density
- 1, 2, 3: x-, y-, and z- components of momentum per unit volume
- 4: internal energy per unit volume
- 5: temperature

Additionally, scratch/snapshot files from magnetohydrodynamic (MHD) simulations also contain the following variables:

- 6, 7, 8: x-, y-, and z- components of magnetic field B

You can easily load the data cubes into variables (3D arrays) as follows:

	r = a[0]
	px = a[1]
	py = a[2]
	pz = a[3]
	e = a[4]
	tt = a[5]

The second index in each Stagger data cube refers to the vertical direction. Also, the y-component of the momentum (`py`) is the vertical one.

Note: the top layer in the temperature data cube `tt` (`a[5]`) is used to store the surface intensity in the vertical direction, integrated over all bins (full spectrum). The surface intensity pattern can be retrieved as follows:

	tt = a[5]
	im = reform(tt[*,0,*])

or, simply:

	im = reform(a[*,0,*,5])

It is generally convenient for the analysis to assign the following variables for ln density and for energy density per unit mass:

	lnr = alog(r)
	ee = e/r

## Units
All variables are in internal Stagger units. Conversion factors from internal to cgs units are loaded by the IDL Stagger start-up script and made available via a common block. They are:

- `uur`: density (=1.0e-7 g cm^-3)
- `uul`: length (=1.0e8 cm)
- `uut`: time (=1.0e2 s)

Derived units:
- `uuv`: velocity (`= uul/uut`)
- `uue`: energy per unit mass (`= uuv^2`)
- `uup`: pressure (`= uur*uuv^2`)
- `uua`: extinction per unit length (`=1.0/uul`)
- `uub`: magnetic field B (`= sqrt(4.*!pi*uup)`)

For example, to convert density, ln density, and internal energy per unit mass (as defined above) to cgs, one can do:

	rho = r * uur
	lnrho = lnr + alog(uur)
	emass = ee * uue

Note that temperatures are already assumed to be in kelvin, no conversion factor needed there.

## Ghost zones
By default, all data cubes include the so-called ghost layers (five layers at the top and five at the bottom, used by Stagger to control values of physical quantities and derivatives at the boundaries).

It is often desirable to remove the ghost layers when analysing simulation data. It is convenient to define the following variables after opening a scratch/snapshot file:

	lb = 5L
	ub = ny-6L

`lb` and `ub` define the range of indexes for the physical domain.

To trim a data cube, e.g. the density, of the ghost zones, you can do the following:

	r = a[0]
	r = r[*,lb:ub,*]

or, alternatively:

	r = a[*,lb:ub,*,0]

## Mesh

Calling `open.pro` with `mesh=fmesh` option also loads the mesh file. Alternatively, the mesh file can be read using the `read_mesh.pro` routine:

	fmesh = 'sun03.msh'
	read_mesh, fmesh, xm=xm, ym=ym, zm=zm, /swap_endian

The information about the mesh can then be accessed via a common block. The following arrays contain the coordinates of the centres of the individual cells in the numerical domain:

- `xm`: x coordinates
- `ym`: y coordinates
- `zm`: z coordinates

Also, very useful are the coordinates of the faces of the cells, or, more exactly, of the face located half a point down index-wise with respect to the cell centres:

- `xmdn`: x coordinates of faces half a point down with respect to cell centre
- `ymdn`: same as above for y coordinates
- `zmdn`: same as above for z coordinates

In other words, `xmdn[i]` is the x coordinate of the cell-face half-way between `xm[i-1]` and `xm[i]`.

Note that the x-, y-, and z- components of the momentum (data cubes targeted by a[1], ..., a[3]) are staggered half a index down in the respective directions. This means that px[i,j,k] is the x-component of the momentum at coordinates xmdn[i], ym[j], zm[k], while py[i,j,k] is the y-component of the momentum at coordinates xm[i], ymdn[j], zm[k], etc.

## Plotting
To plot the depth scale (as function of depth index), including ghost layers:

	cgplot, ym, ps=-7

Scatter plot of ln density vs temperature:

	r = a[0]
	tt = a[5]
	lnr = alog(r)
	cgplot, lnr, tt, ps=3, xtitle='ln density', ytitle='temperature', title='sun03'

Nicer version of the same plot (coloured histogram):

	plot_hist2d, lnr, tt

Visualisation of the surface intensity pattern with `image.pro` subroutine:

	im = reform( a[*,0,*,5] )
	image, im	

# EOS table
Basic thermodynamic quantities as well as important extinction coefficients are tabulated in the EOS table. The values of these quantities for a particular snapshot can be computed using dedicated lookup routines.

Load the table:

	tabfile = 'EOSrhoe.tab'
	eostab

The EOS lookup tables contains tabulated values of the following variables as function of ln density and ln internal energy per unit mass:

- `iv=0`:  ln gas+rad pressure
- `iv=1`:  ln Rosseland extinction coefficient
- `iv=2`:  ln temperature 
- `iv=3`:  ln electron number density
- `iv=4`:  ln Planck-averaged extinction coefficient
- `iv=5`:  ln standard extinction coefficient at 500nm

All quantities in the EOS table are in cgs units!

Example: look up pressure and Rosseland extinction coefficient:

	r = a[0]
	e = a[4]
	lnr = alog(r)
	ee = e/r
	lnpp = lookup_eos(lnr+alog(uur),alog(ee*uue),0)
	lnabross = lookup_eos(lnr+alog(uur),alog(ee*uue),1)

Convert to internal units:

	lnpp = lnpp - alog(uup)
	lnabross = lnabross - alog(uua)

Compute Rosseland optical depth

	tau = tau_calc(lnabross, ym, vertical=1, tau_min=0.)

(Note: the above example includes the ghost zones in the calculation, but it would be in general preferable to remove them beforehand).

When inspecting a simulation, it is very often useful to look at the temperature distribution as function of (log10) optical depth:

	tt = a[5]
	lgtau = alog10(tau)
	plot_hist2d, lgtau, tt



