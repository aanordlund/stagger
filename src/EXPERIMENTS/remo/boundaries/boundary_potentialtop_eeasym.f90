!...............................................................................
MODULE boundary_top_m
  real, allocatable, dimension(:,:,:) :: BxPot, ByPot, BzPot
END MODULE boundary_top_m

!...............................................................................
SUBROUTINE viscosity_boundary_top (nu, cs)
  USE params
  implicit none
  real, dimension(mx,my,mz) :: nu, cs
  integer :: iz

END SUBROUTINE viscosity_boundary_top

!...............................................................................
SUBROUTINE density_boundary_top (r,lnr,py,e)

  USE params
  USE boundary
  USE arrays,  only: scr1
  USE forcing, only: rav, eav
  implicit none

  real, dimension(mx,my,mz) :: r, lnr, py, e, ee
  real, parameter :: clampF=0.1
  integer :: ix, iy, iz
  real :: efact, lnrmin, lnrmax, r_prv, f
  logical :: do_io
  logical :: flag_snap, flag_scr
  real(kind=8) :: tmp(3)

  !if (do_trace) print *, 'density_BC: mpi_rank,y,ny = ', mpi_rank, mpi_y, mpi_ny

  call dumpn(r,'r0','bdry.dmp',1)

  ! horizontally averaged density and internal energy per volume
  call haverage_subr (r, rav)
  call haverage_subr (e, eav)

  
  if (lb > 1) then
     ! top BC
     
     ! extrapolate in the log
     call extrapolate_center_lower(lnr)

     do iz=izs,ize
        do iy=1,my
           do ix=1,mx
              ! min and max ln rho in inflows
              lnrmin = alog(rmin*real(rav(iy)))
              lnrmax = alog(rmax*real(rav(iy)))
              ! clamp extreme values
              lnr(ix,iy,iz) = max(lnr(ix,iy,iz),lnrmin)
              lnr(ix,iy,iz) = min(lnr(ix,iy,iz),lnrmax)
              f = exp(lnr(ix,iy,iz))/r(ix,iy,iz)
              if (abs(f-1.) > .01) then
                 ! consistent density and energy
                 r(ix,iy,iz) = r(ix,iy,iz)*f
                 e(ix,iy,iz) = e(ix,iy,iz)*f
              end if
              ! consistent energy per unit mass
              ee(ix,iy,iz)=e(ix,iy,iz)/r(ix,iy,iz)
           end do
        end do
     end do
     
     ! copy energy per unit mass from lb+1 to lb
     ! Note: setting ee(:,lb,:) = ee(:,lb+1,:) seems necessary to avoid 
     ! the development of an artificial temperature minimum at the top
     do iz=izs,ize
        ee(:,lb,iz) = ee(:,lb+1,iz)
     end do

     ! extrapolate ee antisymmetrically
     call extrapolate_center_lower(ee) 

     ! clamp energy per unit mass in ghost-zone
     ! update e = ee * r
     do ix=1,mx
        do iy=1,lb
           do iz=izs,ize
              ee(ix,iy,iz) = min( ee(ix,iy,iz), ee(ix,lb,iz) * (1.0 + clampF * real(lb-iy)/real(lb-1)) )
              ee(ix,iy,iz) = max( ee(ix,iy,iz), ee(ix,lb,iz) * (1.0 - clampF * real(lb-iy)/real(lb-1)) )
              e (ix,iy,iz) = ee(ix,iy,iz) * r(ix,iy,iz)
           end do
        end do
     end do

  end if

  flag_scr = do_io(t, tsnap, isnap+isnap0, nsnap)
  flag_snap= do_io(t, tscr, iscr+iscr0, nscr)
  if (flag_scr.or.flag_snap.or.debug.and.isubstep.eq.timeorder) then
     if (master) then
        print *,' eetop = ',eetop
     end if
  end if
  
END SUBROUTINE density_boundary_top

!...............................................................................
SUBROUTINE energy_boundary_top (r,lnr,Ux,Uy,Uz,e,ee,p,Bx,By,Bz)
  USE params
  implicit none
  real, dimension(mx,my,mz):: r,lnr,Ux,Uy,Uz,e,ee,p,Bx,By,Bz

END SUBROUTINE energy_boundary_top

!...............................................................................
SUBROUTINE passive_boundary_top (r,Ux,Uy,Uz,d,dd)
  USE params
  implicit none
  real, dimension(mx,my,mz):: r,Ux,Uy,Uz,d,dd
  integer iy, iz

  call symmetric_center(dd)

  ! top BC
  if (mpi_y == 0) then
     do iz=izs,ize
        do iy=1,lb-1
           d(:,iy,iz)=r(:,iy,iz)*dd(:,iy,iz)
        end do
     end do
  end if
END SUBROUTINE passive_boundary_top

!...............................................................................
SUBROUTINE velocity_boundary_top (r,xdnr,ydnr,zdnr,Ux,Uy,Uz,px,py,pz,e,Bx,By,Bz)
  USE params
  USE boundary
  implicit none
  real, dimension(mx,my,mz) :: r,xdnr,ydnr,zdnr,Ux,Uy,Uz,px,py,pz,e,Bx,By,Bz
  real :: p, rin, rtot, fmout, fmtot, uyin, uytot
  integer :: ix, iy, iz

  !!if (do_trace) &
  !!print 1, 'velocity_BC: mpi_rank,y,ny = ', mpi_rank, mpi_y, mpi_ny, omp_mythread
  !! 1 format(1x,a,4i6)

  ! Top boundary, vanishing velocity derivatives
  if (lb > 1) then
     do iz=izs,ize
        do ix=1,mx
           Ux(ix,lb,iz) = Ux(ix,lb+1,iz)
           Uz(ix,lb,iz) = Uz(ix,lb+1,iz)
        end do
     end do
  end if

  call constant_center_lower (Ux)
  !! call extrapolate_constrained_face_lower  (Uy)
  call constant_face_lower  (Uy)
  call constant_center_lower (Uz)
  !!if (do_trace) print 1,'velocity: 2', mpi_rank, mpi_y, mpi_ny, omp_mythread

  ! Top boundary, impose maximum velocity -- relevant when the 
  ! density is clamped and cannot become lower
  if (lb > 1) then
     do iz=izs,ize
        do ix=1,mx
           do iy=1,lb+1
              ux(ix,iy,iz) = max(min(ux(ix,iy,iz),+uy_max),-uy_max)
              uy(ix,iy,iz) = max(min(uy(ix,iy,iz),+uy_max),-uy_max)
              uz(ix,iy,iz) = max(min(uz(ix,iy,iz),+uy_max),-uy_max)
           end do
           if (uy(ix,lb+1,iz) > 0. .or. uy(ix,lb,iz) > 0.) then
              do iy=1,lb
                 uy(ix,iy,iz) = min(uy(ix,iy,iz),uy(ix,lb+1,iz))
              end do
           end if
        end do
     end do

     !  Recompute mass fluxes
     do iz=izs,ize
        do iy=1,lb+1
           px(:,iy,iz)=xdnr(:,iy,iz)*Ux(:,iy,iz)
           py(:,iy,iz)=ydnr(:,iy,iz)*Uy(:,iy,iz)
           pz(:,iy,iz)=zdnr(:,iy,iz)*Uz(:,iy,iz)
        end do
     end do

  end if

END SUBROUTINE velocity_boundary_top

!...............................................................................
SUBROUTINE mfield_boundary_top (Ux,Uy,Uz,Bx,By,Bz)
  USE params
  USE boundary_top_m
  implicit none
  real, dimension(mx,my,mz):: Ux,Uy,Uz,Bx,By,Bz
  integer ix, iy, iz, lb1, lb2, lb3
  real f
  character(len=mid), save:: Id = 'mfield_boundary_top'

  call print_id(id)
  call init_potential_lower
  if (mpi_y > 0) return

  !  Top boundary:  A potential extrapolation is used as a guiding field,
  !  not directly enforced, but controls the E-field.  In the call below,
  !  lb1 = By source layer for the extrapolation
  !  lb2 = last layer for imposing Bx & Bz
  !  lb3 = last layer for imposing By

  lb1 = lb+1
  lb2 = lb
  lb3 = lb

  !$omp master
  allocate (BxPot(mx,my,mz), ByPot(mx,my,mz), BzPot(mx,my,mz))
  !$omp end master
  !$omp barrier
  do iz=izs,ize
     BxPot(:,1:lb1+1,iz) = Bx(:,1:lb1+1,iz)
     ByPot(:,1:lb1+1,iz) = By(:,1:lb1+1,iz)
     BzPot(:,1:lb1+1,iz) = Bz(:,1:lb1+1,iz)
  end do
  call potential_lower(BxPot,ByPot,BzPot,lb1,lb2,lb3)           ! ->  potential
  !call dumpn(BxPot,'BxPot','bdry.dmp',1)
  !call dumpn(ByPot,'ByPot','bdry.dmp',1)
  !call dumpn(BzPot,'BzPot','bdry.dmp',1)
END SUBROUTINE mfield_boundary_top

!...............................................................................
SUBROUTINE ecurrent_boundary_top (Ex, Ey, Ez, Jx, Jy, Jz)
  USE params
  USE boundary
  implicit none
  real, dimension(mx,my,mz):: Ex, Ey, Ez, Jx, Jy, Jz

END SUBROUTINE ecurrent_boundary_top

!...............................................................................
SUBROUTINE efield_boundary_top (Ex, Ey, Ez, Bx_y, Bx_z, By_z, By_x, Bz_x, Bz_y, &
     Bx, By, Bz, Ux_y, Ux_z, Uy_z, Uy_x, Uz_x, Uz_y, &
     Ux, Uy, Uz)
  USE params
  USE boundary
  USE boundary_top_m
  USE arrays, only: scr1, scr2
  implicit none

  character(len=mid), save :: Id = 'efield_boundary_top'
  real, dimension(mx,my,mz) :: Ex, Ey, Ez
  real, dimension(mx,my,mz) :: Bx_y, Bx_z, By_z, By_x, Bz_x, Bz_y
  real, dimension(mx,my,mz) :: Ux_y, Ux_z, Uy_z, Uy_x, Uz_x, Uz_y
  real, dimension(mx,my,mz) :: Ux, Uy, Uz, Bx, By, Bz
  real :: c
  integer :: iy

  if (mpi_y > 0) return
  call print_id (id)

  !  Impose a BC on the electric field vertical derivative such that 
  !  dBxdy makes Bz tend towards BzPot and dBzdy makes Bx tend towards BzPot.
  !  dBxdt = -ddyup(Ez) = 0.3/dt * (BxPot - Bx)
  !  dBzdt = +ddyup(Ex) = 0.3/dt * (BxPot - Bx)

  c = 0.1/dt
  do iy=lb,1,-1
     Ex(:,iy,izs:ize) = Ex(:,iy+1,izs:ize) - c*(ymdn(iy+1)-ymdn(iy))*(BzPot(:,iy,izs:ize)-Bz(:,iy,izs:ize))
     Ez(:,iy,izs:ize) = Ez(:,iy+1,izs:ize) + c*(ymdn(iy+1)-ymdn(iy))*(BxPot(:,iy,izs:ize)-Bx(:,iy,izs:ize))
  end do
  do iy=1,lb
     Ey(:,iy,izs:ize) = 0.0
  end do
  
  !$omp barrier
  !$omp master
  deallocate (BxPot, ByPot, BzPot)
  !$omp end master
END SUBROUTINE efield_boundary_top

!...............................................................................
SUBROUTINE ddt_boundary_top (r,px,py,pz,e,p,Bx,By,Bz, &
     drdt,dpxdt,dpydt,dpzdt, &
     dedt,dBxdt,dBydt,dBzdt)

  USE params
  USE forcing, only: gya, rav, pav
  USE boundary
  implicit none

  character(len=mid), save :: Id = 'ddt_boundary_top'
  real, dimension(mx,my,mz) :: r, px, py, pz, e, p, Bx, By, Bz
  real, dimension(mx,my,mz) :: drdt, dpxdt, dpydt, dpzdt
  real, dimension(mx,my,mz) :: dedt, dBxdt, dBydt, dBzdt
  integer :: ix, iz, ixp1, izp1
  real(kind=8) :: rub1, pyub1, elb1, eub1, ct
  real(kind=8) :: pub1, pubt1, dpydtub1, scr, htopi
  !!'ddt_boundary_top 0.05 fraction smoothing'

  call print_id (Id)

  !$omp barrier
  if (debug) print *,'bdry',mpi_y,ym(1),rbot,ebot
  call dumpn(drdt,'drdt','bdry.dmp',1)
  call dumpn(dedt,'dedt','bdry.dmp',1)

  if (debug .and. omp_master) print *,'ddt:bdry: Top boundary'

  ! horizontally averaged pressure, needed below
  call haverage_subr (p, pav)
  ! top BC
  if (mpi_y == 0) then
     ! inverse pressure scale height
     htopi = gya(lb)*rav(lb)/pav(lb)
     do iz=izs,ize
        do ix=1,mx
           ! inflows: let density remove the -ddyup(py) part
           ! replace with -py/H term
           if (py(ix,lb+1,iz) > 0. .or. py(ix,lb,iz) > 0.) then
              drdt(ix,lb,iz) = drdt(ix,lb,iz) &
                   + (py(ix,lb+1,iz)-py(ix,lb,iz))/dym(lb) &
                   - htopi*py(ix,lb+1,iz)
           end if
           ! zap the ghost zone dfdt's
           drdt (ix,1:lb-1,iz) = 0.
           dpxdt(ix,1:lb-1,iz) = 0.
           dpydt(ix,1:lb  ,iz) = 0.
           dpzdt(ix,1:lb-1,iz) = 0.
           dedt (ix,1:lb-1,iz) = 0.

           ! set dedt in first physical layer
           !!dedt (ix,lb,iz) = drdt(ix,lb,iz)*eetop
           dedt (ix,lb,iz) = drdt(ix,lb,iz)*e(ix,lb,iz)/r(ix,lb,iz)

           if (do_mhd) then
              ! B field is contaminated up to iy=2 from taking ddyup(E.)
              dBxdt(ix,1:3,iz)=0.
              dBzdt(ix,1:3,iz)=0.
              dBydt(ix,1:3,iz)=0.
           endif
        end do
     end do
  end if
END SUBROUTINE ddt_boundary_top

!----------------------------------------------------------------------
SUBROUTINE potential_top (bx, by, bz, lb1, lb2, lb3)
  USE params
  implicit none
  real, dimension(mx,my,mz):: bx, by, bz
  integer lb1, lb2, lb3
  logical omp_in_parallel
  !
  if (omp_in_parallel()) then
     call potential_top_omp (bx, by, bz, lb1, lb2, lb3)
  else
     !$omp parallel shared(lb1)
     call potential_top_omp (bx, by, bz, lb1, lb2, lb3)
     !$omp end parallel
  end if
END SUBROUTINE potential_top

!----------------------------------------------------------------------
SUBROUTINE potential_top_omp (Bx, By, Bz, lb1, lb2, lb3)
  !
  !  Potential field extrapolation into the ghost zones.
  !
  !  A potential field, periodic in x and z, is exponentially
  !  decreasing in the direction away from the boundary, with
  !  amplitude factors exp(-y*k_xz) for each Fourier component.
  !
  !  Here we assume that 
  !    Bx(i,j,iz) is centered at (i-1/2,j,iz),
  !    By(i,j,iz) is centered at (i,j-1/2,iz),
  !    Bz(i,j,iz) is centered at (i,j,iz-1/2).
  !
  !  If Bx and Bz were integer centered they would be exactly
  !  pi/4 out of phase horizontally relative to By.  Since they
  !  are half zone centered, a zero point that should be at j
  !  is really at j+0.5, so we need to add that phase factor.
  !
  !  The extrapolation is based on By(:,lb1,:), which is centered 
  !  at (ix,lb1+1/2,iz).  The first layer above this is the one
  !  with Bx(:,lb,:), which is centered at (ix,lb,iz). 
  !
  !  lb1 is the By source layer.  1,lb2 is the By potential range.
  !  1,lb3 is the Bx and Bz potential range.  Possoble choices:
  !
  !  lb1=lb+1 (the last layer inside)
  !  lb2=lb   (from the first layer outside)
  !  lb3=lb   (includes the boundary itself)
  !
  !  lb1=lb   (the first layer outside)
  !  lb2=lb-1 (from the 2nd layer outside)
  !  lb3=lb-1 (from the 1st lager outside)
  !
  USE params
  USE arrays, only: scr1, scr2, scr3
  implicit none
  integer :: ix, iy, iz, lb1, lb2, lb3
  real :: asin, acos, kx(mx), kz(mz)
  real, dimension(mx,my,mz):: Bx, By, Bz
  real(kind=8), allocatable, dimension(:) :: bxh, bzh
  real,         allocatable, dimension(:,:) :: fy, fh, kh, byt
  real,         allocatable, dimension(:,:) :: bt, bf

  if (mpi_y > 0) return

  allocate (bxh(my), bzh(my))
  allocate (fy(mx,mz), fh(mx,mz), kh(mx,mz), byt(mx,mz))
  allocate (bt(mx,mz), bf(mx,mz))

  !  Horizontal Fourier transform of the vertical boundary field
  !  fft2df uses srfttf which return the sin transform in odd terms
  !  and the cos transforms in the even terms.

  bf(:,izs:ize) = by(:,lb1,izs:ize)
  call fft2df (bf, byt, mx, mz)

  call haverage_subr (bx, bxh)
  call haverage_subr (bz, bzh)

  do ix=1,mx
     kx(ix) = 2.*pi/sx*((ix+mpi_x*mx)/2)
  end do
  do iz=1,mz
     kz(iz) = 2.*pi/sz*((iz+mpi_z*mz)/2)
     do ix=1,mx
        kh(ix,iz) = sqrt(kx(ix)**2+kz(iz)**2) + 1e-30
     end do
  end do

  !  Potential Y component

  do iy=1,lb2
     do iz=izs,ize
        do ix=1,mx
           bt(ix,iz) = byt(ix,iz)*exp((ymdn(iy)-ymdn(lb1))*kh(ix,iz))
        end do
     end do
     call fft2db (bt, bf, mx, mz)
     by(:,iy,izs:ize) = bf(:,izs:ize)
  end do

  !  Potential

  if(mpi_x==0.and.mpi_z==0) kh(1,1)=1.
  do iy=1,lb3
     do iz=izs,ize
        do ix=1,mx
           bt(ix,iz) = byt(ix,iz)*exp((ym(iy)-ymdn(lb1))*kh(ix,iz))/kh(ix,iz)
        end do
     end do
     if(mpi_x==0.and.mpi_z==0) bt(1,1)=0.
     call fft2db (bt, bf, mx, mz)
     scr1(:,iy,izs:ize) = bf(:,izs:ize)
  end do

  !  Horizontal field components from the potential, with special treatment if the mean

  call ddxdn_set (scr1, scr2)
  call ddzdn_set (scr1, scr3)
  do iy=1,lb3
     bx(:,iy,izs:ize) = scr2(:,iy,izs:ize) + bxh(lb)
     bz(:,iy,izs:ize) = scr3(:,iy,izs:ize) + bzh(lb)
  end do

  deallocate (bxh, bzh, fy, fh, kh, byt, bt, bf)
  
END SUBROUTINE potential_top_omp
