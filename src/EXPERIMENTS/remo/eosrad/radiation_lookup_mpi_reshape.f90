! $Id: radiation_lookup_mpi_reshape.f90,v 1.1 2016/10/12 09:36:51 remo Exp $
!***********************************************************************
MODULE cooling

  USE params
  USE reshape_mod, only: sub_nx, sub_nz, reshape_mode

  integer, parameter:: mmu=9, mphi=10
  real    dphi, form_factor, dtaumin, dtaumax, taumin, taumax
  integer nmu, nphi, verbose, ny0
  integer mblocks
  integer n1, n2

  real y_newton, dy_newton, t_newton, ee_newton, ee_min, t_ee_min

  real, dimension(mmu):: mu0

  real, pointer, dimension(:,:,:) :: rk, lnrk, s, q, dtau, qq
  real, pointer, dimension(:,:,:) :: tau

  ! lnrkap: used to store untilted ln absorption coefficient (lnrk)
  real, pointer, dimension(:,:,:) :: lnrkap

  real, allocatable, target, dimension(:,:,:) :: scr7, scr8

  logical do_table, do_newton, do_limb, do_fld
  character(len=mfile):: intfile

END MODULE cooling


!***********************************************************************
SUBROUTINE init_cooling (r,e)

  USE params
  USE arrays
  USE cooling

  implicit none

  real, dimension(mx,my,mz):: r,e

  integer i
  character(len=mfile):: name

  !if (mpi_ny > 1) then
  !  if (master) print*,'ERROR: this RT version needs mpi_ny=1'
  !  call abort_mpi
  !end if

  do_cool     = .true.
  i           = index(file,'.')
  nmu         = 0
  nphi        = 4
  dphi        = 0.
  form_factor = 1.
  dtaumax     = 100.
  dtaumin     = 0.1
  intfile     = name('intensity.dat','int',file)

  do_newton   = .false.
  y_newton    = -0.3
  dy_newton   = 0.05
  t_newton    = 0.01
  t_ee_min    = 0.005
  ee_newton   = -1.
  ee_min      = 3.6
  do_limb     = .false.
  verbose     = 0
  ny0         = 0
  sub_nx      = 0
  sub_nz      = 0
  reshape_mode = 'ymerg_xzsplit'

  ! mblocks = number of subdomains communicated per side in old trnslt routine (deprecated!)
  mblocks     = 1
  do_fld      = .false.

  call read_cooling

  rk   => scr1
  lnrk => scr2
  s    => scr3
  q    => scr4
  dtau => scr5
  qq   => scr6

  allocate( scr7(mx,my,mz) )
  allocate( scr8(mx,my,mz) )
  tau    => scr7
  lnrkap => scr8

END SUBROUTINE init_cooling


!***********************************************************************
SUBROUTINE read_cooling

  USE params
  USE eos
  USE cooling

  namelist /cool/do_cool, nmu, nphi, dphi, form_factor, dtaumin, dtaumax, intfile, &
       do_newton, y_newton, dy_newton, t_newton, ee_newton, ee_min, t_ee_min, do_limb, mu0, &
       ny0, verbose, mblocks, do_fld, sub_nx, sub_nz, reshape_mode

  character(len=mfile):: name, fname
  character(len=mid):: id='$Id: radiation_lookup_mpi_reshape.f90,v 1.1 2016/10/12 09:36:51 remo Exp $'

  call print_id(id)

  rewind (stdin); read (stdin,cool)

  if (nmu==0 .and. form_factor==1.) form_factor=0.4

  if (master) write (*,cool)

  if (master) write (*,*) 'mbox, dbox =', mbox, dbox

  if (do_limb) then
     fname = name('limb.dat','lmb',file)
     call mpi_name(fname)
     open (limb_unit, file=trim(fname), &
          form='unformatted', status='unknown')
     write (limb_unit) mx, mz,nmu,nphi
  end if

END SUBROUTINE read_cooling


!***********************************************************************
SUBROUTINE coolit (r, ee, lne, dd, dedt)

  USE params
  USE arrays, only: lns
  USE units
  USE eos
  USE cooling

  implicit none

  character(len=mid)  :: id='$Id: radiation_lookup_mpi_reshape.f90,v 1.1 2016/10/12 09:36:51 remo Exp $'

  real, dimension(mx,my,mz) :: r, ee, lne, dd, dedt
  real, dimension(mx,mz)    :: surfi
 
  ! ym0, ymN1: values of ym depth in the layers immediately above and below local MPI subdomain
  ! dtau0, dtauN1: values of dtau immediately above and below local subdomain
  real                      :: ym0,   ymN1          
  real, dimension(mx,1,mz)  :: dtau0, dtauN1

  ! tauIntr: intrinsic optical depth scale
  real, dimension(mx,my,mz) :: tauIntr

  real    xmu(mmu), wmu(mmu)
  real    sec, fdtime, dxdy, dzdy, tanth, qqTmp, f1, f2
  real    phi, wphi, womega

  integer ix, iy, iz, lmu, lphi, imu, iphi, ibox, n1p, n2p, ny
  integer lrec, imaxval_mpi, nyr

  integer  mTauTop, mTauBot
  integer  nDTauTop, nDTauBot

  logical, save :: first=.true.
  logical       :: flag_scr, flag_snap, flag_limb, flag_surf, do_io, do_surfi, io_flag
  logical       :: debug
  real          :: cput(2), void, dtime, prof
  integer, save :: nrad=0, nang=0

  character(len=mfile) :: name, fname

  real, allocatable, dimension(:,:,:):: ii,jj,hhx,hhy,hhz

  external transfer


  ! print subroutine's ID
  call print_id(id)

  ! skip ratiative transfer ?
  if (.not. do_cool) return
                                                                                call timer('radiation','init')

  if (debug(dbg_cool) .and. omp_master) then
     void = dtime(cput)
     print '(1x,a8,2f7.3)','rad0   ',cput*omp_nthreads
  end if

  ! reset dedt in ghost zones, if present (lb > 1)
  if (lb > 1) then
     dedt(:,1:lb-1,izs:ize) = 0.
  end if


  !  Angular quadrature
  !  nmu > 0: Use Gauss integration (mu=1 excluded) with 2*nmu   order accuracy
  !  nmu = 0: One vertical ray with weight 0.5
  !  nmu < 0: Use Radau integration (mu=1 included) with 2*nmu-1 order accuracy

  if      (nmu .gt. 0) then
     lmu = nmu
     call gausi (lmu,0.,1.,wmu,xmu)
  else if (nmu .eq. 0) then
     lmu = 1
     xmu(1) = 1.
     wmu(1) = 0.5
  else 
     lmu =-nmu
     call radaui(lmu,0.,1.,wmu,xmu)
  end if
  if (mu0(1) .ne. 0.) xmu=mu0

  if (first) then
     call barrier_omp ('rad-first')
     if (omp_master) first = .false.
     if (master) then
        print *,' imu       xmu    theta'
        !           1.....0.000.....00.0
        do imu=1,lmu 
           print '(i4,f10.3,f8.1,f10.3)',imu,xmu(imu),acos(xmu(imu))*180./pi,wmu(imu)
        end do
     end if
  end if

  ! look up opacity and source function
  call lookup (r,ee,lnrk,lns)
                                                                                call timer('radiation','lookup')
  ! copy untilted lnrk to lnrkap
  do iz=izs,ize
     do iy=1,my
        lnrkap(:,iy,iz) = lnrk(:,iy,iz)
     end do
  end do


  ! solve radiative transfer for the entire subdomain
  ny = my


  !  !$omp barrier
  !  !$omp single
  !  n2 = 2
  !  !$omp end single
  !  n2p = 2

  !  do iz=izs,ize
  !    do iy=2,my
  !    do ix=1,mx
  !      dtau2 = 0.5*dymdn(iy)*(exp(lnrk(ix,iy-1,iz))+exp(lnrk(ix,iy,iz)))
  !      if (dtau2 < dtaumax) n2p = max(n2p,iy)
  !    end do
  !    end do
  !  end do

  !  !$omp critical
  !  n2 = max(n2p,n2)
  !  !$omp end critical
  !  !$omp barrier

  !  if (ny0==0) then
  !    n2 = imaxval_mpi (n2)
  !    ny  = n2
  !  else
  !    ny = ny0
  !  end if

  call dumpn (r,'r','rad.dmp',0)
  call dumpn (ee,'ee','rad.dmp',1)
  call dumpn (lnrk,'lnrk','rad.dmp',1)
  do ibox=1,mbox
     call dumpn (lns(:,:,:,ibox),'lns','rad.dmp',1)
  end do

  if (debug(dbg_cool) .and. omp_master) then
     void = dtime(cput)
     print '(1x,a8,2f7.3)','lookup ',cput*omp_nthreads
  end if

  if (do_fld .and. do_dump) then
     !$omp master
     allocate (ii(mx,my,mz),jj(mx,my,mz))
     !allocate (hhx(mx,my,mz),hhy(mx,my,mz),hhz(mx,my,mz))
     !$omp end master
     !$omp barrier
     ii(:,:,izs:ize) = 0.
     jj(:,:,izs:ize) = 0.
     call dumpn(lnrk,'lnrk','fld.dmp',1)
  end if
  call dumpn (lnrk,'lnrk','trf.dmp',0)


  ! reset heating rate
  do iz=izs,ize
     do iy=1,my
        qq(:,iy,iz) = 0.
     end do
  end do


  ! loop over angles
  do imu=1,lmu

     if (nmu .eq. 0 .and. imu .eq. 1) then
        lphi = 1 
     else if (nmu .lt. 0 .and. imu .eq. lmu) then
        lphi = 1 
     else
        lphi = nphi
     end if

     tanth = tan(acos(xmu(imu)))
     wphi  = 4.*pi/lphi

     do iphi=1,lphi

        phi = (iphi-1)*2.*pi/lphi + dphi*t
        dxdy = tanth*cos(phi)
        dzdy = tanth*sin(phi)

        if (master .and. verbose==1 .and. isubstep==1) then
           print '(a,3i5,2e15.7)',' imu,iphi,ny,dxdy,dzdy=', imu,iphi,ny,dxdy,dzdy
        end if


        ! solve radiative transfer

        ! trnslt: first tilt lnrkap -> lnrk
        ! do not exponentiate lnrk! exponentiation is done in tau_calc 

        call trnslt (lnrkap, lnrk, dxdy, dzdy, .false., my, mblocks, verbose)
                                                                                call timer('radiation','trnslt(lnrk)')
        call dumpn (lnrk,'lnrk','trf.dmp',1)

        do ibox=1,mbox

           !        !$omp barrier
           !        !$omp single
           !        n1 = 2
           !        n2 = 2
           !        !$omp end single
           !        n1p = 2
           !        n2p = 2

           if (ibox.eq.1) then

              ! flags for data output 
              flag_scr  = do_io(t+dt, tscr, iscr+iscr0,   nscr)
              flag_snap = do_io(t+dt, tsnap,isnap+isnap0, nsnap)
              io_flag   = flag_scr .or. flag_snap

              ! flag_limb: limb darkening every scratch 
              ! or else every nsnap and only mu=1
              ! and only for first substep and bin:
              flag_limb = do_limb   .and. flag_scr
              flag_limb = flag_limb .or.  (xmu(imu)==1. .and. flag_snap)
              flag_limb = flag_limb .and. isubstep.eq.1 .and. ibox.eq.1

              ! compute surface intensity ?
              flag_surf = flag_scr .or. flag_snap
              flag_surf = flag_surf .and. xmu(imu)==1. .and. isubstep.eq.1 

              do_surfi  = flag_surf.or.flag_limb

              ! compute optical depth increments
              ! compute intrinsic optical depth tauIntr
              ! tau_calc: rk = exp(lnrk)
              call tau_calc( dym, my, xmu(imu), tauIntr, nDTauTop, nDTauBot, .false. )
                                                                                call timer('radiation','tau_calc')

              !f1 = (0.5/xmu(imu))
              !do iz=izs,ize
              !   dtau(:,1,iz) = dym(1)*rk(:,1,iz)/xmu(imu)
              !   do iy=2,ny
              !      do ix=1,mx
              !         dtau(ix,iy,iz) = f1*dym(iy)*(rk(ix,iy-1,iz)+rk(ix,iy,iz))
              !         !dtau(ix,iy,iz) = f1*dymdn(iy)*(rk(ix,iy-1,iz)+rk(ix,iy,iz))
              !         !if (dtau(ix,iy,iz) < dtaumin) n1p = max(n1p,iy)
              !         !if (dtau(ix,iy,iz) < dtaumax) n2p = max(n2p,iy)
              !      end do
              !   end do
              !end do

           else

              f1 = 10.**dbox
              do iz=izs,ize
                 do iy=1,my
                    do ix=1,mx
                       dtau(ix,iy,iz) = f1 * dtau(ix,iy,iz)
                       !if (dtau(ix,iy,iz) < dtaumin) n1p = max(n1p,iy)
                       !if (dtau(ix,iy,iz) < dtaumax) n2p = max(n2p,iy)
                    end do
                 end do
              end do

           end if
           call dumpn (dtau,'dtau','trf.dmp',1)


           !        if (ny0==0) then
           !          !$omp critical
           !          n1 = max(n1p,n1)
           !          n2 = max(n2p,n2)
           !          !$omp end critical
           !          !$omp barrier
           !          n2 = imaxval_mpi (n2)
           !        else
           !          n2 = ny0
           !        end if
           !
           !        nrad = nrad + n2
           !        nang = nang + 1
           !
           !        if (master .and. verbose==2 .and. isubstep==1) then
           !           nyr = (nrad+0.5)/nang
           !           print '(a,7i5,1p(2e15.7))',' imu,iphi,ibox,ny,n1,n2,nyr,dxdy,dzdy=', &
           !                imu,iphi,ibox,ny,n1,n2,nyr,dxdy,dzdy
           !        end if


           ! trnslt: tilt source function, and exponentiate: lnsrc -> s
           ! transfer: solve radiative transfer equation
           call trnslt (lns(:,:,:,ibox), s, dxdy, dzdy, .true., my, mblocks, verbose)
                                                                                call timer('radiation','trnslt(s)')
           call dumpn (s,'s','trf.dmp',1)
           call transfer_reshape (dtau, s, q, surfi, do_surfi, xmu(imu), dtaumin, dtaumax, n2)
           n2 = min(my,n2-iyoff)
                                                                                call timer('radiation','transfer')
           if ( flag_limb ) then
              call barrier_omp('limb')
              if (do_limb .and. omp_master) then
                 write(limb_unit) t,imu,iphi,xmu(imu),phi,surfi
                 print *,omp_mythread,isubstep,ibox,imu,iphi
              end if
           end if
           call dumpn(q,'q0','trf.dmp',1)

           if ( do_fld .and. do_dump ) then
              ii(:,1:n2,izs:ize) = q(:,1:n2,izs:ize)+s(:,1:n2,izs:ize)
              call trnslt (ii, s, -dxdy, -dzdy, .false., my, mblocks, verbose)
              jj(:,1:n2,izs:ize) = jj(:,1:n2,izs:ize) &
                   + s(:,1:n2,izs:ize)*(wmu(imu)*wphi*form_factor)
              !call trnslt (rr, s, -dxdy, -dzdy, .false., my, mblocks, verbose)
              !hhx(:,1:n2,izs:ize) = hhx(:,1:n2,izs:ize) &
              !                    + s(:,1:n2,izs:ize)*(wmu(imu)*wphi*form_factor*sin(acos(xmu(imu)))*cos(phi))
              !hhz(:,1:n2,izs:ize) = hhz(:,1:n2,izs:ize) &
              !                    + s(:,1:n2,izs:ize)*(wmu(imu)*wphi*form_factor*sin(acos(xmu(imu)))*sin(phi))
              !hhy(:,1:n2,izs:ize) = hhy(:,1:n2,izs:ize) &
              !                    + s(:,1:n2,izs:ize)*(wmu(imu)*wphi*form_factor*xmu(imu))
           end if

           ! set s to zero explicitly
           ! trnslt: tilt q back to vertical grid, q -> s
           do iz=izs,ize
              s(:,:,iz) = 0.0
           end do
           call trnslt(q, s, -dxdy, -dzdy, .false., my, mblocks, verbose)
                                                                                call timer('radiation','trnslt(q)')
           call dumpn (s,'q2','trf.dmp',1)

           ! energy equation
           f1 = wmu(imu)*wphi*form_factor
           do iz=izs,ize
              do iy=1,my
                 do ix=1,mx
                    qqTmp = f1 * s(ix,iy,iz)
                    qq(ix,iy,iz)   =   qq(ix,iy,iz) + qqTmp
                    dedt(ix,iy,iz) = dedt(ix,iy,iz) + qqTmp
                 end do
              end do
           end do


           ! integrated surface intensity
           if (flag_surf) then
              if (ibox .eq. 1) then      ! overwrite surface_int
                 do iz=izs,ize
                    surface_int(:,iz) = surfi(:,iz)
                 end do
              else                       ! add to surface_int
                 do iz=izs,ize
                    surface_int(:,iz) = surface_int(:,iz)+surfi(:,iz)
                 end do
              end if
           end if
                                                                                call timer('radiation','sum(q)')
        end do ! ibox=1,nbox
     end do ! iphi=1,lphi
  end do ! imu=1,lmu


  call print_trace('radiation',0,'dump1')
  call dumpn(qq,'qq','rad.dmp',1)
  call dumpn(qq,'qq','qq.dmp',1)
  call print_trace('radiation',0,'dump2')
  ! call dumps(qq,'Qrad','fluxes.dat',1)

  if (do_fld .and. do_dump) then
     call dumpn(jj,'jj','fld.dmp',1)
     !$omp barrier
     !$omp master
     deallocate (ii,jj)
     !deallocate (hhx,hhy,hhz)
     !$omp end master
  end if

  call print_trace('radiation',0,'radiative_flux')
  call radiative_flux (qq,q)

  call print_trace('radiation',0,'newton')
  if (do_newton) then
     if (ee_newton > 0) eetop = ee_newton
     do iz=izs,ize
        do iy=1,my
           prof = exp(min((y_newton-ym(iy))/dy_newton,70.))
           prof = prof/(1.+prof)
           do ix=1,mx
              dedt(ix,iy,iz) = dedt(ix,iy,iz) - r(ix,iy,iz)*(ee(ix,iy,iz)-eetop)/t_newton*prof &
                   - r(ix,iy,iz)*min(0.,ee(ix,iy,iz)-ee_min)/t_ee_min
           end do
        end do
     end do
  end if
                                                                                call timer('radiation','dedt')
  if (debug(dbg_cool) .and. omp_master) then
     void = dtime(cput)
     print '(1x,a8,2f7.3)','transfr',cput*omp_nthreads
  end if

  call print_trace('radiation',0,'END')

END SUBROUTINE coolit
