!...............................................................................
module cooling

  use params, only: mfile

  integer, parameter:: mmu=9, mphi=10
  real    :: dphi, form_factor, dtaumin, dtaumax, tauMin, tauMax
  integer :: nmu, nphi, verbose, ny0
  integer :: mblocks
  integer :: n1, n2

  real :: y_newton, dy_newton, t_newton, ee_newton, ee_min, t_ee_min

  real, dimension(mmu) :: mu0

  real, pointer, dimension(:,:,:) :: rk, lnrk, s, q, dtau, qq, qqr
  real, pointer, dimension(:,:,:) :: tau, lnrkap, lnsrc, lnxx, lnrkRoss
  real, pointer, dimension(:,:,:) :: lnrk1, lnsrc1, s1

  real, allocatable, target, dimension(:,:,:) :: scr7,  scr8,  scr9,  scr10, scr11
  real, allocatable, target, dimension(:,:,:) :: scr12, scr13, scr14, scr15


  logical do_table, do_newton, do_limb, do_yrad, do_tsc
  character(len=mfile) :: intfile

end module cooling


!...............................................................................
subroutine init_cooling (r,e)

  use params
  use arrays
  use cooling

  implicit none

  character(len=mid) :: Id = 'init_cooling'

  real, dimension(mx,my,mz) :: r,e
  integer :: i
  character(len=mfile) :: name

  if (mpi_ny > 1) then
     if (master) print '(A)', trim(Id)//': this version requires mpi_ny=1. STOP.'
     call abort_mpi
     stop
  end if

  i = index(file,'.')

  do_cool     = .true.
  nmu         =  0
  nphi        =  4
  dphi        =  0.
  form_factor =  1.
  dtaumax     =  200.
  dtaumin     =  0.1
  intfile     =  name('intensity.dat','int',file)
  do_newton   = .false.
  y_newton    = -0.3
  dy_newton   =  0.05
  t_newton    =  0.01
  t_ee_min    =  0.005
  ee_newton   = -1.
  ee_min      =  3.6
  do_limb     = .false.
  verbose     =  0
  ny0         =  0

  ! do triangular shaped cloud interpolation in trsnlt? Default: false
  do_tsc      =  .false.

  ! do adaptive radiation depth scale?
  do_yrad     =  .false.

  ! mblocks: number of subdomains communicated per side in trnslt routine (DEPRECATED)
  ! default: communicate only nearest neighbour
  ! taumin, taumax: limits for adaptive radiation scale (radiation_scale routine);
  mblocks     =  1
  tauMin      =  1.0e-6
  tauMax      =  2.0e2

  call read_cooling

  rk   => scr1
  lnrk => scr2
  s    => scr3
  q    => scr4
  dtau => scr5
  qq   => scr6

  allocate(  scr7(mx,my,mz),  scr8(mx,my,mz),  scr9(mx,my,mz) )
  allocate( scr10(mx,my,mz), scr11(mx,my,mz), scr12(mx,my,mz) )
  allocate( scr13(mx,my,mz), scr14(mx,my,mz), scr15(mx,my,mz) )

  tau      => scr7
  lnrkap   => scr8
  lnsrc    => scr9
  lnxx     => scr10
  lnrkRoss => scr11
  lnrk1    => scr12
  lnsrc1   => scr13
  s1       => scr14
  qqr      => scr15

end subroutine init_cooling


!...............................................................................
subroutine read_cooling

  use params
  use eos
  use cooling

  namelist /cool/ &
       do_cool, &
       nmu, &
       nphi, &
       dphi, &
       form_factor, &
       dtaumin, &
       dtaumax, &
       intfile, &
       do_newton, &
       y_newton, &
       dy_newton, &
       t_newton, &
       ee_newton, &
       ee_min, &
       t_ee_min, &
       do_limb, &
       mu0, &
       ny0, &
       do_yrad, &
       verbose, &
       mblocks, &
       tauMin, &
       tauMax

  character(len=mid) :: Id = 'read_cooling'
  character(len=mfile) :: name, fname

  call print_id(Id)

  rewind (stdin); read (stdin, cool)

  if (nmu==0 .and. form_factor==1.)  form_factor=0.4

  if (master) write (*,cool)

  if (do_limb) then
     fname = name('limb.dat','lmb',file)
     call mpi_name(fname)
     open (limb_unit, file=trim(fname), form='unformatted', status='unknown')
     write (limb_unit) mx, mz, nmu, nphi
  end if

end subroutine read_cooling


!...............................................................................
subroutine coolit (r, ee, lne, dd, dedt)

  use params
  use arrays, only: qqav, qqrav
  use units
  use eos
  use cooling
  use variables, only: e
  use table, only: Nlam

  implicit none

  character(len=mid) :: Id = 'coolit'

  real, dimension(mx,my,mz) :: r, ee, lne, dd, dedt
  real, dimension(mx,my,mz) :: tt
  real, dimension(mx,mz)    :: surfi
  real, dimension(my)       :: yrad, dyrad

  real          :: xmu(mmu), wmu(mmu)
  real          :: sec, fdtime, dxdy, dzdy, tanth, f1, f2
  real          :: phi, wphi, womega

  integer       :: ix, iy, iz, lmu, lphi, imu, iphi, ilam, ny
  integer       :: lrec, imaxval_mpi
  integer       :: mTauTop, mTauBot, nDTauTop, nDTauBot

  logical       :: debug
  logical       :: flag_scr, flag_snap, flag_limb, flag_surf, do_io, do_surfi
  logical, save :: first=.true.

  real          :: cput(2), void, dtime, prof
  integer, save :: nrad=0, nang=0

  character(len=mfile) :: name, fname

  external transfer

  ! do_cool = .false.  ! no radiative transfer
  if (.not. do_cool)  return

  ! consistent treatment of scattering ?
  if (flag_scat) then
     if (master) print '(A)', &
          trim(Id)//': iterative solution for scattering case not available. STOP.'
     call abort_mpi
     stop
  end if

  ! debug statements?
  if (debug(dbg_cool) .and. omp_master) then
     void = dtime(cput)
     print '(1x,a8,2f7.3)','rad0   ', cput*omp_nthreads
  end if

  ! reset dedt in ghost zones, if present (lb > 1)
  if (lb > 1) then 
     dedt(:,1:lb-1,izs:ize) = 0.
  end if

  !  Angular quadrature:
  !  nmu > 0: Use Gauss quadrature (mu=1 excluded), accuracy: 2*nmu   order
  !  nmu = 0: One vertical ray with weight 0.5
  !  nmu < 0: Use Radau quadrature (mu=1 included), accuracy: 2*nmu-1 order
  if      (nmu .gt. 0) then
     lmu = nmu
     call gausi (lmu,0.,1.,wmu,xmu)
  else if (nmu .eq. 0) then
     lmu = 1
     xmu(1) = 1.
     wmu(1) = 0.5
  else 
     lmu =-nmu
     call radaui(lmu,0.,1.,wmu,xmu)
  end if
  if (mu0(1) .ne. 0.) xmu=mu0

  if (first) then
     call barrier_omp ('rad-first')
     if (omp_master) first = .false.
     if (master) then
        print '(a4,a10,a8,a10)', 'imu', 'xmu', 'theta'
        do imu=1,lmu 
           print '(i4,f10.3,f8.1,f10.3)',imu,xmu(imu),acos(xmu(imu))*180./pi,wmu(imu)
        end do
     end if
  end if


  ! lookup temperature and Rosseland extinction coefficient
  call temperature (r, ee, tt   )
  call rosseland   (r, tt, lnrkRoss )

  ! copy lnrkRoss to lnrk, required by radiation_scale;
  ! initialize rk and heating rates (qq)
  do iz=izs,ize
     lnrk(:,:,iz) = lnrkRoss(:,:,iz)
     rk(:,:,iz)   = 0. 
     qq(:,:,iz)   = 0.
  enddo

  if (do_yrad) then 
     ! adaptive depth scale for radiative transfer
     call radiation_scale(ym, dym, yrad, dyrad, mTauTop, mTauBot, .true.)
     n2 = mTauBot
  else
     ! radiation depth scale same as hydro depth scale
     call tau_calc(dym, my, 1.0, mTauTop, mTauBot, nDTauTop, nDTauBot)
     n2 = nDTauBot
  endif

  if (ny0==0) then
     ny = n2
  else
     ny = ny0
  end if

  call dumpn (r,  'r',  'rad.dmp', 0)
  call dumpn (ee, 'ee', 'rad.dmp', 1)
  call dumpn (lnrkRoss,'lnrkRoss','rad.dmp', 1)
  call dumpn (lnrkRoss,'lnrkRoss','trf.dmp', 0)

  if (debug(dbg_cool) .and. omp_master) then
     void = dtime(cput)
     print '(1x,a8,2f7.3)','lookup ',cput*omp_nthreads
  end if


  ! loop over bins or wavelengths
  do ilam=1,Nlam

     !lookup lnxx (ln ratio of bin/monochromatic and Rosseland opacity)
     !lnsrc (ln source function * weight; simulation units)
     call lookup_rad (r, tt, 1, ilam, lnxx,  .false., .false.)
     call lookup_rad (r, tt, 2, ilam, lnsrc, .false., .false.)

     !compute ln extinction coefficient
     do iz=izs,ize
        lnrkap(:,:,iz) =lnrkRoss(:,:,iz) + lnxx(:,:,iz)
     enddo

     ! loop over angles
     do imu=1,lmu

        if (nmu .eq. 0 .and. imu .eq. 1) then
           lphi = 1 
        else if (nmu .lt. 0 .and. imu .eq. lmu) then
           lphi = 1 
        else
           lphi = nphi
        end if
        tanth = tan(acos(xmu(imu)))
        wphi = 4.*pi/lphi

        do iphi=1,lphi

           phi = (iphi-1)*2.*pi/lphi + dphi*t
           dxdy = tanth*cos(phi)
           dzdy = tanth*sin(phi)

           if (master .and. verbose==1 .and. isubstep==1) then
              print '(a,3i5,2e15.7)',' imu,iphi,ny,dxdy,dzdy=', &
                   imu,iphi,ny,dxdy,dzdy
           end if

           ! flags for data output 
           flag_scr  = do_io(t+dt, tscr, iscr+iscr0,   nscr)
           flag_snap = do_io(t+dt, tsnap,isnap+isnap0, nsnap)

           ! flag_limb: limb darkening every scratch 
           ! or else every nsnap and only mu=1
           ! and only for first substep and bin:
           flag_limb = do_limb   .and. flag_scr
           flag_limb = flag_limb .or.  (xmu(imu)==1. .and. flag_snap)
           flag_limb = flag_limb .and. isubstep.eq.1 .and. ilam.eq.1

           ! compute surface intensity ?
           flag_surf = flag_scr .or. flag_snap
           flag_surf = flag_surf .and. xmu(imu)==1. .and. isubstep.eq.1 

           do_surfi  = flag_surf.or.flag_limb

           if (do_yrad) then

              ! solve radiative transfer on radiation depth scale

              ! trnslt: tilt lnrkap -> lnrk1
              ! yinter: interpolate to radiation depth scale; lnrk1 -> lnrk
              ! tau_calc: compute optical depth; lnrk -> rk=exp(lnrk) 
              ! tau_calc: return nDTau (-> n1) used by transfer routine 

              call trnslt(lnrkap, lnrk1, dxdy, dzdy, .false., ny, 1, verbose, do_tsc)
              call dumpn (lnrk1,'lnrk1','trf.dmp',1)
              call yinter(my,ym,my,yrad,mx,mz,lnrk1,lnrk)
              call tau_calc(dyrad, my, xmu(imu), mTauTop, mTauBot, nDTauTop, nDTauBot)
              n1 = nDTauTop

              ! trnslt: tilt source function lnsrc -> lnsrc1
              ! yinter: interpolate to radiation depth scale; lnsrc1 -> s1
              ! exponentiate: s1 -> s=exp(s1)

              call trnslt(lnsrc, lnsrc1, dxdy, dzdy, .false., ny, 1, verbose, do_tsc)
              call yinter(my,ym,my,yrad,mx,mz,lnsrc1,s1)
              do iz=izs,ize
                 s(:,:,iz) = exp(s1(:,:,iz))
              end do
              call dumpn (s,'s','trf.dmp',1)

              ! transfer: solve raditative transfer equation
              ! dtau= optical depth step
              ! s   = source function
              ! q   = J-S split

              call transfer (my, n1, dtau, s, q, surfi, do_surfi )
              call dumpn(q,'q0','trf.dmp',1)

              do iz=izs,ize
                 q(:,1,iz) = q(:,1,iz) * rk(:,1,iz)
                 do iy=2,my-1
                    f1 = 1. / (yrad(iy+1)-yrad(iy-1))
                    f2 = xmu(imu) * f1
                    q(:,iy,iz) = q(:,iy,iz) * (dtau(:,iy+1,iz)+dtau(:,iy,iz)) * f2
                 end do
                 q(:,my,iz) = q(:,my,iz) * rk(:,my,iz)
              end do

              call dumpn(q,'q1','trf.dmp',1)

              ! ydistr: reset s1, distribute q back to hydro depth scale, q -> s1 
              ! zap s explicitly
              ! trnslt: tilt s1 back to vertical grid, s1 -> s

              call ydistr(my,yrad,my,ym,mx,mz,q,s1)              
              do iz=izs,ize
                 s(:,:,iz) = 0.0
              end do
              call trnslt(s1, s, -dxdy, -dzdy, .false., ny, 1, verbose, do_tsc)
              call dumpn (s,'q2','trf.dmp',1)

           else

              ! solve radiative transfer on hydro depth scale

              ! trnslt: tilt lnrkap -> lnrk
              ! tau_calc: compute optical depth; lnrk -> rk=exp(lnrk) 
              ! tau_calc: return nDTauTop (-> n1) used by transfer routine 
              ! find n2, used by trnslt routine

              call trnslt(lnrkap, lnrk, dxdy, dzdy, .false., ny, mblocks, verbose)
              call dumpn (lnrk,'lnrk','trf.dmp',1)
              call tau_calc(dym, ny, xmu(imu), mTauTop, mTauBot, nDTauTop, nDTauBot)

              n1 = nDTauTop
              if (ny0==0) then
                 n2 = imaxval_mpi (nDTauBot)
              else
                 n2 = ny0
              end if

              ! trnslt: tilt source function, exponentiate; lnsrc -> s
              call trnslt(lnsrc, s, dxdy, dzdy, .true., n2, 1, verbose, do_tsc)
              call dumpn (s,'s','trf.dmp',1)

              ! transfer: solve raditative transfer equation
              ! dtau= optical depth step
              ! s   = source function
              ! q   = J-S split

              call transfer (n2, n1, dtau, s, q, surfi, do_surfi )
              call dumpn (q,'q0','trf.dmp',1)

              do iz=izs,ize
                 q(:,1,iz) = q(:,1,iz)*rk(:,1,iz)
                 do iy=2,n2-1
                    f1 = 1. / (ym(iy+1)-ym(iy-1))
                    f2 = xmu(imu) * f1
                    q(:,iy,iz) = q(:,iy,iz) * (dtau(:,iy+1,iz)+dtau(:,iy,iz)) * f2
                 end do
                 q(:,n2,iz) = q(:,n2,iz) * rk(:,n2,iz)
              end do
              call dumpn(q,'q1','trf.dmp',1)

              ! zap s explicitly
              ! trnslt: tilt q back to vertical grid, q -> s

              do iz=izs,ize
                 s(:,:,iz) = 0.0
              end do
              call trnslt(q, s, -dxdy, -dzdy, .false., n2, mblocks, verbose)
              call dumpn (s,'q2','trf.dmp',1)

           endif

           ! energy equation: radiative heating contribution
           f1 = wmu(imu) * wphi * form_factor
           do iz=izs,ize
              do iy=1,n2
                 do ix=1,mx
                    qq(ix,iy,iz) = qq(ix,iy,iz) + s(ix,iy,iz) * f1
                 end do
              end do
           end do

           ! limb darkening, output
           if (flag_limb) then
              call barrier_omp('limb')
              if (do_limb .and. omp_master) then
                 write(limb_unit) t,imu,iphi,xmu(imu),phi,surfi
                 print *,omp_mythread,isubstep,ilam,imu,iphi
              end if
           end if

           ! integrated surface intensity
           if (flag_surf) then
              if (ilam .eq. 1) then      ! overwrite surface_int
                 do iz=izs,ize
                    surface_int(:,iz) = surfi(:,iz)
                 end do
              else                       ! add to surface_int
                 do iz=izs,ize
                    surface_int(:,iz) = surface_int(:,iz)+surfi(:,iz)
                 end do
              end if
           end if

        end do ! iphi=1,lphi
     end do ! imu=1,lmu
  enddo ! ilam=1,nlam

  call dumpn(qq,'qq','qq.dmp',1)

  ! make energy per unit volume e consistent with r and ee ?
  ! no: handled internally by lookup_eos subroutine (invoked by temperature subroutine)
  !
  do iz=izs,ize
     e(:,:,iz) = ee(:,:,iz) * r(:,:,iz)
  end do

  ! energy equation: add contribution of radiative heating rate per unit volume
  do iz=izs,ize
     dedt(:,:,iz) = dedt(:,:,iz) + qq(:,:,iz)
  end do

  ! compute (average) radiative flux
  call radiative_flux (qq,q)

  ! compute radiative heating rates per unit mass
  do iz=izs,ize
     qqr(:,:,iz) = qq(:,:,iz) / r(:,:,iz)
  end do

  ! compute horizontally averaged radiative heating rates 
  ! per unit volume and per unit mass
  call haverage_subr(qq,  qqav)
  call haverage_subr(qqr, qqrav)

  if (do_newton) then
     if (ee_newton > 0) eetop = ee_newton
     do iz=izs,ize
        do iy=1,my
           prof = exp(min((y_newton-ym(iy))/dy_newton,70.))
           prof = prof/(1.+prof)
           do ix=1,mx
              dedt(ix,iy,iz) = dedt(ix,iy,iz) &
                   - r(ix,iy,iz)*(ee(ix,iy,iz)-eetop)/t_newton*prof   &
                   - r(ix,iy,iz)*min(0.,ee(ix,iy,iz)-ee_min)/t_ee_min
           end do
        end do
     end do
  end if

  if (debug(dbg_cool) .and. omp_master) then
     void = dtime(cput)
     print '(1x,a8,2f7.3)','transfr',cput*omp_nthreads
  end if

end subroutine coolit

!...............................................................................
