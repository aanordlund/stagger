!...............................................................................
module eos

  use params, only: mfile

  implicit none

  character(len=mfile) :: eostabfile
  character(len=mfile) :: radtabfile
  logical              :: do_eos, do_table, do_cubic_eos
  logical              :: flag_scat

  !! integer Nlam

  ! allocatable arrays for pressure and derivatives at bottom boundary   
  real(kind=4), allocatable, dimension(:,:)   :: pbot0
  real(kind=4), allocatable, dimension(:,:)   :: dlnpdE_r
  real(kind=4), allocatable, dimension(:,:)   :: dlnpdlnr_E

  ! allocatable arrays for interpolation indexes and coefficients
  integer,      allocatable, dimension(:,:,:) :: irarr_e, iearr_e
  real,         allocatable, dimension(:,:,:) :: pxarr_e, pyarr_e
  integer,      allocatable, dimension(:,:,:) :: irarr_r, itarr_r
  real,         allocatable, dimension(:,:,:) :: pxarr_r, pyarr_r


  ! similar to above, but for alternative version of lookup routines
  ! (for use with reshaped arrays)
  real(kind=4), allocatable, dimension(:,:)   :: pbot0_alt
  real(kind=4), allocatable, dimension(:,:)   :: dlnpdE_r_alt
  real(kind=4), allocatable, dimension(:,:)   :: dlnpdlnr_E_alt
  integer,      allocatable, dimension(:,:,:) :: irarr_e_alt, iearr_e_alt
  real,         allocatable, dimension(:,:,:) :: pxarr_e_alt, pyarr_e_alt
  integer,      allocatable, dimension(:,:,:) :: irarr_r_alt, itarr_r_alt
  real,         allocatable, dimension(:,:,:) :: pxarr_r_alt, pyarr_r_alt

end module eos

!...............................................................................
module table

  implicit none

  ! EOS table: parameters
  character(len=8)       :: dateeos, timeeos
  integer                :: NetabE, NrtabE, MaE, NaE, Ntbvar, NelemE
  integer, dimension(20) :: NxoptE, NxrevE
  real(kind=4)           :: lnrMinE, lnrMaxE, lneMinE, lneMaxE
  real(kind=4)           :: ulE, urE, utE
  real(kind=4),     allocatable, dimension(:)       :: abundE
  real(kind=4),     allocatable, dimension(:,:)     :: auxE
  real(kind=4),     allocatable, dimension(:,:,:,:) :: eostab
  character(len=4), allocatable, dimension(:)       :: ielE

  ! Radiation table: parameters
  character(len=8)       :: daterad,timerad
  integer                :: Nlam
  integer                :: NTtabR, NrtabR, MaR, NaR, NelemR
  integer, dimension(20) :: NxoptR, NxrevR
  real(kind=4)           :: lnrMinR, lnrMaxR, lntMinR, lntMaxR
  real(kind=4)           :: ulR, urR, utR
  real(kind=4),     allocatable, dimension(:)       :: abundR
  real(kind=4),     allocatable, dimension(:,:)     :: auxR
  real(kind=4),     allocatable, dimension(:,:,:,:) :: radtab
  character(len=4), allocatable, dimension(:)       :: ielR

  ! Maximum size for EOS and radiation tables
  integer, parameter :: maxtabE = 10000000
  integer, parameter :: maxtabR = 10000000
  integer            :: mtableE
  integer            :: mtableR

  ! Simulation units (cgs): length, time, density
  ! Simulation units (cgs): velocity, energy/mass, pressure, opacity,
  ! integrated Planck function (B*\Delta\lambda)
  real(kind=4) :: uur, uul, uut
  real(kind=4) :: uuv, uuee, uup, uuk, uubp

  ! EOS table units (cgs): velocity, energy/mass, pressure, opacity
  real(kind=4) :: uvE, ueeE, upE, ukE

  ! Radiation table units (cgs): velocity, energy/mass, pressure,
  ! opacity, integrated source function
  real(kind=4) :: uvR, ueeR, upR, ukR, ubpR

  ! EOS table: ln density and ln energy/mass increments
  ! Radiation table: ln density and ln temperature increments
  real(kind=4) :: dlnrE, dlneE
  real(kind=4) :: dlnrR, dlntR

  ! Switch: verbose output for debugging
  ! Switch: restrain density, temperature and energy/mass to within table limits
  logical :: verbose_tab
  logical :: do_limit_tab

  ! EOS and radiation tables: limits to density, energy/mass, and temperature
  real(kind=4) :: eelimMinE, eelimMaxE, rholimMinE, rholimMaxE
  real(kind=4) :: ttlimMinR, ttlimMaxR, rholimMinR, rholimMaxR

  ! Limits to density and internal energy (DEPRECATED)
  real(kind=4) :: eelimE, rholimE 

  ! Constants: Boltzmann constant (cgs)
  real(kind=4), parameter :: kBoltz = 1.3806504e-16
  real(kind=4), parameter :: lnkBoltz = alog(kBoltz)

end module table

!...............................................................................
subroutine read_eos

  use params, only: do_ionization, stdin, master, mid
  use eos
  use table
  implicit none

  character(len=mid) :: Id = 'read_eos'

  namelist /eqofst/ &
       do_eos, &
       do_ionization, &
       do_table, &
       eostabfile, &
       radtabfile, &
       do_cubic_eos, &
       do_limit_tab, &
       verbose_tab, &
       uur, &
       uul, &
       uut, &
       eelimMinE, &
       eelimMaxE, &
       rholimMinE, &
       rholimMaxE, &
       ttlimMinR, &
       ttlimMaxR, &
       rholimMinR, &
       rholimMaxR, &
       eelimE, &
       rholimE

  do_eos        = .true.
  do_ionization = .true.
  do_table      = .false.
  eostabfile    = 'EOSrhoe.tab'
  radtabfile    = 'kaprhoT.tab'
  do_cubic_eos  = .true.
  do_limit_tab  = .false.
  verbose_tab   = .false.
  ! Conversion factors, simulation to cgs units
  uur           = 1.0e-7
  uul           = 1.0e8
  uut           = 1.0e2
  ! Limits 
  eelimMinE     = 1.0e-4
  eelimMaxE     = 1.0e4
  rholimMinE    = 1.0e-9
  rholimMaxE    = 1.0e9
  ttlimMinR     = 1.0e1
  ttlimMaxR     = 1.0e7
  rholimMinR    = 1.0e-9
  rholimMaxR    = 1.0e9
  ! (DEPRECATED) eelimE, rholimE, kept for backward compatibility
  eelimE        = 0.0
  rholimE       = 0.0

  ! Read namelist
  call print_id(Id)
  rewind (stdin); read (stdin, eqofst)

  if (master) write (*,eqofst)

end subroutine read_eos

!...............................................................................
subroutine init_eos

  ! This routine reads the EOS and radiation tables.
  !
  ! The EOS table contains the following variables stored in the
  ! following order:
  !
  ! - Pressure (gas plus radiation)
  ! - Rosseland exctinction coefficient (per unit length)
  ! - Temperature
  ! - Electron number density (OPTIONAL)
  ! - Planck-averaged extinction coefficient
  ! - Standard extinction coefficient at 500 nm
  !
  ! The variables are stored in ln scale and tabulated as a function of
  ! ln energy/mass and ln density
  ! 
  ! The EOS table also includes the derivatives of the ln variables with
  ! respect to ln energy/mass and ln density.
  !
  !
  ! The radiation table contains the following variables:
  !
  ! - 1st wavelength (bin):  ratio of monochromatic (bin) and
  !                          Rosseland opacities
  ! - 1st wavelength (bin):  integrated thermal emission
  !                          (epsilon*B*w_lambda)
  ! - 1st wavelength (bin):  destruction probability (epsilon)
  !
  ! - 2nd wavelength (bin):  same as above...
  ! ...
  ! ...
  ! last 3 entries: Rosseland extinction coefficient (per unit length)
  !                 Planck-averaged extinction coefficient,
  !                 extinction coefficient at 500 nm
  !
  ! All variables are stored in ln scale and tabulated as a function of
  ! ln temperature and ln density

  use params, only: mid, mpi_size, mpi_rank, master, mx, my, mz, mid, hl
  use arrays
  use eos
  use table
  implicit none

  character(len=mid) :: Id = 'init_eos'

  ! EOS and radiation table file units
  integer :: lunE = 12
  integer :: lunR = 13

  integer :: i, j, rank

  character(len=16) :: jtype
  character(len=8)  :: linedat

  ! String arrays for printout (logging)
  character(len=32), dimension (10) :: sopctype = (/ ('', i=1,10) /)
  character(len=32), dimension (4)  :: ssrctype = (/ ('', i=1, 4) /)
  character(len=32), dimension (4)  :: stautype = (/ ('', i=1, 4) /)
  character(len=32), dimension (4)  :: sxtrtype = (/ ('', i=1, 4) /)

  logical :: flag_vanreg
  logical :: flag_bins
  logical :: flag_odf

  call print_id(Id)

  ! Read namelist from input file
  call read_eos


  ! Derived conversion factors, simulation to cgs units
  uuv  = uul / uut         ! velocity 
  uuee = uuv * uuv         ! energy/mass
  uup  = uur * uuee        ! pressure
  uuk  = 1.  / uul / uur   ! opacity
  uubp = uur * uuee * uuv  ! integrated Planck function ! [density] * [velocity]^3


  ! Open and read EOS and rad tables
  ! Note: ensure that only one process at the time opens and reads
  ! the table files to avoid excessive I/O load

  do rank=0,mpi_size-1

     call barrier_mpi ('init_eos')
     if (rank .eq. mpi_rank) then       

        ! Read EOS table
        if (master) then
           print '(A)', hl
           print '(A)', trim(Id)//': read EOS table'
        end if

        open (lunE, file=eostabfile, form='UNFORMATTED', status='OLD')
        read (lunE) NetabE, NrtabE, MaE, NaE, Ntbvar, NelemE, NxoptE, NxrevE
        allocate ( auxE(MaE, NaE) )
        allocate ( ielE(NelemE) )
        allocate ( abundE(NelemE) )
        allocate ( eostab(NetabE, NrtabE, 3, Ntbvar) )
        read (lunE) &
             dateeos, &
             timeeos, &
             lnrMinE, &
             lnrMaxE, &
             lneMinE, &
             lnEmaxE, &
             ulE, &
             urE, &
             utE, &
             iele(1:NelemE), &
             abundE(1:NelemE), &
             auxE(1:MaE,1:NaE)
        read (lunE) eostab(1:Netabe,1:Nrtabe,1:3,1:Ntbvar)
        close(lunE)

        ! EOS table units, derived:
        uvE  = ulE / utE         ! velocity
        ueeE = uvE * uvE         ! energy/mass
        upE  = urE * ueeE        ! pressure
        ukE  = 1.  / ulE / urE   ! opacity

        ! EOS table boundaries: convert to simulation units
        lneMaxE = lneMaxE + log(ueeE/uuee)
        lneMinE = lneMinE + log(ueeE/uuee)
        lnrMaxE = lnrMaxE + log(urE/uur)
        lnrMinE = lnrMinE + log(urE/uur)

        ! EOS table: density and internal energy increments
        dlnrE = (lnrMaxE-lnrMinE)/(real(NrtabE)-1.0)
        dlneE = (lneMaxE-lneMinE)/(real(NetabE)-1.0)

        ! EOS table: lower and upper limits to ln density and ln energy/mass
        eelimMinE  = max(eelimMinE, exp(lneMinE))
        eelimMaxE  = min(eelimMaxE, exp(lneMaxE))
        rholimMinE = max(rholimMinE,exp(lnrMinE))
        rholimMaxE = min(rholimMaxE,exp(lnrMaxE))

        ! DEPRECATED: eelimE and rholimE: lower limits to energy/mass and density
        if (master.and.(eelimE.ne.0.0.or.rholimE.ne.0.0))  then
           print '(A)', &
                trim(Id)//': WARNING: eelimE and rholimE DEPRECATED. '// &
                'Please use eelimMinE and rholimMinE instead.'
           eelimMinE  = max(eelimMinE,  eelimE )
           rholimMinE = max(rholimMinE, rholimE)
        end if

        ! EOS table: print effective limits:
        if (master.and.do_limit_tab) then
           print '(A)', hl
           print '(A)', trim(Id)//&
                ': EOS table, effective lower and upper limits for ee and rho: '
           print '(3X,A,G10.2)', 'EELIMMINE  = ', eelimMinE
           print '(3X,A,G10.2)', 'EELIMMAXE  = ', eelimMaxE
           print '(3X,A,G10.2)', 'RHOLIMMINE = ', rholimMinE
           print '(3X,A,G10.2)', 'RHOLIMMAXE = ', rholimMaxE
           print '(A)', hl
        end if


        ! Read radiation table
        if (master) then
           print '(A)', hl
           print '(A)',trim(Id)//': read radiation table'
        end if

        open (lunR, file=radtabfile, form='UNFORMATTED', status='OLD')
        read (lunR) NTtabr, Nrtabr, Mar, Nar, Nlam, Nelemr, Nxoptr, Nxrevr
        allocate ( auxR(MaR, NaR) )
        allocate ( ielR(NelemR) )
        allocate ( abundR(NelemR) )
        allocate ( radtab(NTtabR,NrtabR,3,Nlam+1) )
        read (lunR) &
             daterad, &
             timerad, &
             lnrMinR, &
             lnrMaxR, &
             lntMinR, &
             lntMaxR, &
             ulR, &
             urR, &
             utR, &
             ielR(1:NelemR), &
             abundR(1:NelemR), &
             auxR(1:MaR,1:NaR)
        do j=1,Nlam+1
           read (lunR) radtab( 1:NTtabR, 1:NrtabR, 1:3, j )
        enddo
        close(lunR)

        ! Radiation table, derived units:
        uvR  = ulR / utR           ! velocity
        ueeR = uvR * uvR           ! energy/mass
        upR  = urR * ueeR          ! pressure
        ukR  = 1.  / ulR  / urR    ! opacity
        ubpR = urR * ueeR * uvR    ! integrated Planck (B*\Delta\lambda) [density] * [velocity^3]

        ! Radiation table: ln density boundaries, convert to simulation units
        lnrMaxR = lnrMaxR + log(urR/uur)
        lnrMinR = lnrMinR + log(urR/uur)

        ! Radiation table: ln density and ln temperature steps
        dlnrR = (lnrMaxR-lnrMinR)/(real(NrtabR)-1.0)
        dlntR = (lntMaxR-lntMinR)/(real(NttabR)-1.0)


        ! EOS table: convert variables from table to simulation units
        ! 1: Pressure
        ! 2: Rosseland extinction coefficient:
        eostab(1:NetabE,1:NrtabE,1,1) = eostab(1:NetabE,1:NrtabE,1,1) + log(upE/uup)
        eostab(1:NetabE,1:NrtabE,1,2) = eostab(1:NetabE,1:NrtabE,1,2) - log(ulE/uul)

        select case(Ntbvar)
        case(5)
           ! 4: Planck mean extinction coefficient
           ! 5: Standard extinction coefficient at 500 nm
           eostab(1:NetabE,1:NrtabE,1,4) = eostab(1:NetabE,1:NrtabE,1,4) - log(ulE/uul)
           eostab(1:NetabE,1:NrtabE,1,5) = eostab(1:NetabE,1:NrtabE,1,5) - log(ulE/uul)
        case(6)
           ! 4: Electron number density
           ! 5: Planck mean extinction coefficient
           ! 6: Standard extinction coefficient at 500 nm
           eostab(1:NetabE,1:NrtabE,1,4) = eostab(1:NetabE,1:NrtabE,1,4) - log(ulE/uul)*3.0
           eostab(1:NetabE,1:NrtabE,1,5) = eostab(1:NetabE,1:NrtabE,1,5) - log(ulE/uul)
           eostab(1:NetabE,1:NrtabE,1,6) = eostab(1:NetabE,1:NrtabE,1,6) - log(ulE/uul)       
           ! TO DO: ensure that the ln electron number density
           ! does not exceed MAXEXPONENT in simulation units!
        case default
           ! Exit if table does not comply with any of the above cases
           if (master) print '(A)', trim(Id)//': Unsupported table format!'
           call abort_mpi
           stop
        end select

        ! Radiation table, convert from table to simulation units

        ! Integrated thermal emissions (\epsilon*B*\Delta\lambda)
        do j=1,Nlam
           radtab(1:NTtabR,1:NrtabR,2,j) = radtab(1:NTtabR,1:NrtabR,2,j) + log(ubpR/uubp)
        end do

        ! Rosseland, Planck-averaged, and 500 nm extinction coefficients
        ! NOTE: from revision 73 onwards, the table contains the extinction coefficients,
        ! not the opacities
        radtab(1:NTtabR,1:NrtabR,1:3,Nlam+1) = radtab(1:NTtabR,1:NrtabR,1:3,Nlam+1) - log(ulR/uul) 

        ! EOS and radiation tables: Check table size
        mtableE = NetabE * NrtabE * 3 * Ntbvar
        mtableR = NTtabR * NrtabR * 3 * (Nlam+1)

        if (mtableE.gt.maxTabE) then
           if (master) print '(A)',trim(Id)//': EOS table exceeds size limits.'
           call abort_mpi
           stop
        end if

        if ( mtableR .gt. maxTabR ) then
           print '(A)',trim(Id)//': Radiation table exceeds size limits.'
           stop
        end if


        ! EOS and radiation tables: basic information
        !
        ! flag_scat: consistent treatment of scattering (t) ?
        ! flag_bins: bins (t) or wavelengths (f) ?
        ! flag_odf:  ODFs (t) or opacity sampling (f) ?
        ! flag_vanreg: van Regemerter approximation for line scattering (t) ?

        select case(Nxoptr(2))
        case(1)
           flag_scat = .false.
        case(2)
           flag_scat = .true.
        case default
           if (master) print '(A)', trim(Id)//': Unsupported source function type. STOP.'
           call abort_mpi
           stop
        end select
        flag_odf    = Nxrevr(4).lt.70
        flag_bins   = Nxoptr(1).lt.6
        flag_vanreg = flag_scat.and.(Nxoptr(3).ne.0)

        if (flag_odf) then 
           linedat = 'odf.dat'
        else
           linedat = 'OS.dat'
        end if

        if (flag_bins) then 
           jtype='bins'
        else
           jtype='wavelengths'
        end if

        sopctype(1) = 'Opacity binning, scaled'
        sopctype(2) = 'Opacity binning'
        sopctype(6) = 'Opacity sampling'

        ssrctype(1) = 'Pure Planck function'
        ssrctype(2) = 'Scattering term included'

        stautype(1) = 'monochromatic opacity'
        stautype(2) = 'Rosseland optical depth'

        sxtrtype(1) = 'xcorr'
        sxtrtype(2) = 'Jlambda'
        sxtrtype(3) = 'T-scaled Jlambda'

        ! Print out information about EOS and radiation tables
        if (master) then
           print '(A)', hl
           print '(2A)', 'init_eos: basic information about EOS and RAD tables'
           print '(2A)',       ' (eos) table file           = ', eostabfile
           print '(A,2A10)',   ' (eos) date and time        = ', dateeos,timeeos
           print '(A,I4)',     ' (eos) SVN Rev              = ', NxrevE(2)
           print '(A,I2)',     ' (eos) Number of entries    = ', Ntbvar
           print '(A)', hl
           print '(2A)',       ' (rad) table file           = ', radtabfile
           print '(A,2A10)',   ' (rad) date and time        = ', daterad, timerad
           print '(A,I4)',     ' (rad) SVN Rev              = ', Nxrevr(2)
           print '(2A)',       ' (rad) opacity table type   = ', sopctype(Nxoptr(1))
           print '(2A)',       ' (rad) source function type = ', ssrctype(Nxoptr(2))
           print '(A,A10,A,I6)',' (rad) number of ', jtype,' = ', Nlam
           if (flag_bins) then     
              print '(A,A24,A)', &
                   ' (rad) bin membership according to value of ', &
                   stautype(Nxoptr(4)), ' where tau_lambda=1'
              print '(2A)',&
                   ' (rad) type of extrapolation from 1D rad transf solution = ', &
                   sxtrtype(Nxoptr(5))
              print '(A,F6.2,A)', &
                   ' (rad) Bridging function    = exp(',&
                   (-real(Nxoptr(6))*0.01),'*tau).'
           end if
           if (flag_scat) then
              if (flag_vanreg) then
                 print '(A,F6.2)', &
                      ' (rad) van Regemorter approx of line scattering; factor=', &
                      (real(Nxoptr(3))*0.01)
              else
                 print '(A)',' (rad) no van Regemorter approx of line scattering'
              end if
           end if
           print '(A,I4)',       ' (rad) EOS.tab version      = ',Nxrevr(3)
           print '(A,A10,A,I4)', ' (rad)',linedat,' version    = ',Nxrevr(4)
           print '(A,I4)',       ' (rad) subs.dat version     = ',Nxrevr(5)
           print '(A)', hl
        end if

     end if ! rank == mpi_rank ?

  end do ! rank

  ! allocate arrays for lookup routines
  allocate(pbot0(mx,mz), dlnpdE_r(mx,mz), dlnpdlnr_E(mx,mz))
  allocate(iearr_e(mx,my,mz),irarr_e(mx,my,mz)) 
  allocate(itarr_r(mx,my,mz),irarr_r(mx,my,mz))
  allocate(pxarr_e(mx,my,mz),pyarr_e(mx,my,mz))
  allocate(pxarr_r(mx,my,mz),pyarr_r(mx,my,mz))

end subroutine init_eos

!...............................................................................
subroutine close_eos

  use eos
  use table

  deallocate(pxarr_e,pyarr_e,pxarr_r,pyarr_r)  
  deallocate(iearr_e,irarr_e,itarr_r,irarr_r)  

  deallocate(auxe, iele, abunde)
  deallocate(eostab)

  deallocate(auxr, ielr, abundr)
  deallocate(radtab)

  deallocate(pbot0, dlnpdE_r, dlnpdlnr_E)

end subroutine close_eos

!...............................................................................
subroutine lookup_eos(rho, ee, ivar, var, do_indx, do_exp, do_cubic)

  use params, only: mx, my, mz, izs, ize, ub, mpi_rank, mid, hl
  use eos
  use table
  use variables, only: e
  implicit none

  character(len=mid):: Id = 'lookup_eos'

  real, dimension(mx,my,mz)              :: rho, ee
  real, dimension(mx,my,mz), intent(out) :: var
  integer,                   intent(in)  :: ivar
  logical,                   intent(in)  :: do_indx, do_exp, do_cubic

  integer  :: ie, ir
  real     :: px, py, qx, qy, pxqx, pyqy, dfx1, dfx2, dfy1, dfy2
  real     :: rmin, rmax, emin, emax, ri, ei

  integer  :: ix, iy, iz
  logical  :: is_outside


  if (do_indx) then

     rmin = minval(rho(:,:,:))
     rmax = maxval(rho(:,:,:))
     emin = minval(ee(:,:,:))
     emax = maxval(ee(:,:,:))

     is_outside = &
          rmin.lt.rholimMinE.or.rmax.gt.rholimMaxE.or. &
          emin.lt. eelimMinE.or.emax.gt. eelimMaxE

     if (is_outside) then 
        if (do_limit_tab) then
           ! Force rho and ee (and e) to witihin table limits
           do iz=izs,ize
              do iy=1,my
                 do ix=1,mx
                    rho(ix,iy,iz) = min(max(rho(ix,iy,iz),rholimMinE),rholimMaxE)
                    ee(ix,iy,iz)  = min( max(ee(ix,iy,iz), eelimMinE), eelimMaxE)
                    e (ix,iy,iz)  = ee(ix,iy,iz) * rho(ix,iy,iz)
                 end do
              end do
           end do
           ! Print log entry
           if (verbose_tab) then
              print '(A)', hl
              print '(A,X,I5)', &
                   trim(Id)//&
                   ': Density and energy forced to be within limits. mpi_rank =', &
                   mpi_rank
           end if
        else
           ! Error message and exit
           print '(A)', hl
           print '(A,X,I5)', &
                trim(Id)//&
                ': Outside table boundaries. mpi_rank =', &
                mpi_rank
           print '(A)', trim(Id)//': lnrmin, lnrmax, lnemin, lnemax ='
           print '(A,X,4(X,E13.6))',' (TABLE) ', lnrMinE, lnrMaxE, lneMinE, lneMaxE
           print '(A,X,4(X,E13.6))',' (SIM)   ', log(rMin), log(rMax), log(eMin), log(eMax)
           print '(a)', hl
           call abort_mpi
           stop
        end if
     end if

     do iz=izs,ize
        do iy=1,my
           do ix=1,mx
              ri = (log(rho(ix,iy,iz))-lnrMinE)/dlnrE + 1.0
              ir = max0(1,min0(Nrtabe-1,int(ri)))
              pxarr_e(ix,iy,iz) = ri-ir
              irarr_e(ix,iy,iz) = ir

              ei = (log(ee(ix,iy,iz))-lneMinE)/dlneE + 1.0
              ie = max0(1,min0(Netabe-1,int(ei)))
              pyarr_e(ix,iy,iz) = ei-ie
              iearr_e(ix,iy,iz) = ie
           end do
        end do
     end do

  endif


  if (do_cubic) then  

     do iz=izs,ize
        do iy=1,my
           do ix=1,mx

              ir = irarr_e(ix,iy,iz)
              ie = iearr_e(ix,iy,iz)
              px = pxarr_e(ix,iy,iz)
              py = pyarr_e(ix,iy,iz)
              qx   = 1. - px
              qy   = 1. - py
              pxqx = px*qx
              pyqy = py*qy

              dfx1 = eostab(ie  ,ir+1,1,ivar)-eostab(ie  ,ir  ,1,ivar)
              dfx2 = eostab(ie+1,ir+1,1,ivar)-eostab(ie+1,ir  ,1,ivar)
              dfy1 = eostab(ie+1,ir  ,1,ivar)-eostab(ie  ,ir  ,1,ivar)
              dfy2 = eostab(ie+1,ir+1,1,ivar)-eostab(ie  ,ir+1,1,ivar)

              var(ix,iy,iz) = qy*(                                           &
                   &             qx*(      eostab(ie  ,ir  ,1,ivar)          &
                   &               + pxqx*(eostab(ie  ,ir  ,3,ivar)-dfx1)    &
                   &               + pyqy*(eostab(ie  ,ir  ,2,ivar)-dfy1))   &
                   &           + px*(      eostab(ie  ,ir+1,1,ivar)          &
                   &               - pxqx*(eostab(ie  ,ir+1,3,ivar)-dfx1)    &
                   &               + pyqy*(eostab(ie  ,ir+1,2,ivar)-dfy2))   &
                   &                                                      )  &
                   &        + py*(                                           &
                   &             px*(      eostab(ie+1,ir+1,1,ivar)          &
                   &               - pxqx*(eostab(ie+1,ir+1,3,ivar)-dfx2)    &
                   &               - pyqy*(eostab(ie+1,ir+1,2,ivar)-dfy2))   &
                   &           + qx*(      eostab(ie+1,ir  ,1,ivar)          &
                   &               + pxqx*(eostab(ie+1,ir  ,3,ivar)-dfx2)    &
                   &               - pyqy*(eostab(ie+1,ir  ,2,ivar)-dfy1))   &
                   &                                                      )

           end do
        end do
     end do

  else

     do iz=izs,ize
        do iy=1,my
           do ix=1,mx
              ir = irarr_e(ix,iy,iz)
              ie = iearr_e(ix,iy,iz)
              px = pxarr_e(ix,iy,iz)
              py = pyarr_e(ix,iy,iz)
              qx   = 1. - px
              qy   = 1. - py
              var(ix,iy,iz) =qy*(qx*eostab(ie  ,ir,1,ivar)+px*eostab(ie  ,ir+1,1,ivar))+ &
                   &         py*(qx*eostab(ie+1,ir,1,ivar)+px*eostab(ie+1,ir+1,1,ivar))
           end do
        end do
     end do
  end if


  ! Pressure: calculate auxiliary arrays needed by boundary routines
  if (ivar.eq.1) then

     do iz=izs,ize
        do ix=1,mx

           pbot0(ix,iz) = var(ix,ub,iz)
           ir = irarr_e(ix,ub,iz)
           ie = iearr_e(ix,ub,iz)
           px = pxarr_e(ix,ub,iz)
           py = pyarr_e(ix,ub,iz)
           qx   = 1. - px
           qy   = 1. - py

           ! lookup dlnpdlne: note that the derivatives stored 
           ! in the tables are multiplied by dlne
           dlnpdE_r(ix,iz)   =  &
                qy*(qx*eostab(ie  ,ir,2,ivar)+px*eostab(ie  ,ir+1,2,ivar)) + &
                py*(qx*eostab(ie+1,ir,2,ivar)+px*eostab(ie+1,ir+1,2,ivar))

           ! dlnpde = dlnpdlne / ee   (in simulation units!)
           dlnpdE_r(ix,iz)   =  dlnpdE_r(ix,iz) / ee(ix,ub,iz) / dlneE

           ! lookup dlnpdlnr:  note that the derivatives stored 
           ! in the tables are multiplied by dlnr
           dlnpdlnr_E(ix,iz) =  &
                qy*(qx*eostab(ie  ,ir,3,ivar)+px*eostab(ie  ,ir+1,3,ivar)) + &
                py*(qx*eostab(ie+1,ir,3,ivar)+px*eostab(ie+1,ir+1,3,ivar))
           dlnpdlnr_E(ix,iz) = dlnpdlnr_E(ix,iz) / dlnrE

        end do
     end do

     if (do_exp) then
        pbot0(:,:) = exp(pbot0(:,:))
     end if

  end if

  if (do_exp) then
     do iz=izs,ize
        var(:,:,iz) = exp(var(:,:,iz))
     end do
  end if

end subroutine lookup_eos

!...............................................................................
subroutine lookup_rad(rho, tt, ivar, ilam, var, do_indx, do_exp)

  use params, only: mx, my, mz, izs, ize, mpi_rank, mid, hl
  use eos
  use table
  implicit none

  character(len=mid) :: Id = 'lookup_rad'

  real, dimension(mx,my,mz)              :: rho, tt
  real, dimension(mx,my,mz), intent(out) :: var
  integer,                   intent(in)  :: ivar, ilam
  logical,                   intent(in)  :: do_indx, do_exp

  integer  :: ir, itt
  real     :: px, py, qx, qy
  real     :: rmin,rmax,tmin,tmax,ri,ti
  integer  :: ix,iy,iz
  logical  :: is_outside

  if (do_indx) then

     rmin = minval(rho(:,:,:))
     rmax = maxval(rho(:,:,:))
     tmin = minval(tt(:,:,:))
     tmax = maxval(tt(:,:,:))

     is_outside = &
          rmin.lt.rholimMinR.or.rmax.gt.rholimMaxR.or.&
          tmin.lt. ttlimMinR.or.tmax.gt. ttlimMaxR

     if (is_outside) then
        if (do_limit_tab) then
           ! Force rho and tt to be witihin table limits
           do iz=izs,ize
              do iy=1,my
                 do ix=1,mx
                    rho(ix,iy,iz) = min(max(rho(ix,iy,iz),rholimMinR),rholimMaxR)
                    tt(ix,iy,iz)  = min( max(tt(ix,iy,iz), ttlimMinR), ttlimMaxR)
                 end do
              end do
           end do
           ! Print log entry
           if (verbose_tab) then
              print '(A)', hl
              print '(A,X,I5)', &
                   trim(Id)//&
                   ': Density and temperature forced to be within limits. mpi_rank =', &
                   mpi_rank
           end if
        else
           ! Error message and exit
           print '(A)', hl
           print '(A,X,I5)', &
                trim(Id)//&
                ': Outside table boundaries. mpi_rank =', &
                mpi_rank
           print '(A)', trim(Id)//': lnrmin, lnrmax, lntmin, lntmax ='
           print '(A,X,4(X,E13.6))',' (TABLE) ', lnrMinR, lnrMaxR, lntMinR, lntMaxR
           print '(A,X,4(X,E13.6))',' (SIM)   ', log(rMin), log(rMax), log(tMin), log(tMax)
           print '(a)', hl
           call abort_mpi
           stop
        end if
     end if


     do iz=izs,ize
        do iy=1,my
           do ix=1,mx
              ri = (log(rho(ix,iy,iz))-lnrMinR)/dlnrR + 1.0
              ir = max0(1,min0(Nrtabr-1,int(ri)))
              pxarr_r(ix,iy,iz) = ri-ir
              irarr_r(ix,iy,iz) = ir

              ti = (log(tt(ix,iy,iz))-lntMinR)/dlntR + 1.0
              itt = max0(1,min0(Nttabr-1,int(ti)))
              pyarr_r(ix,iy,iz) = ti-itt
              itarr_r(ix,iy,iz) = itt
           end do
        end do
     end do

  end if

  do iz=izs,ize
     do iy=1,my
        do ix=1,mx
           ir = irarr_r(ix,iy,iz)
           itt= itarr_r(ix,iy,iz)
           px = pxarr_r(ix,iy,iz)
           py = pyarr_r(ix,iy,iz)
           qx   = 1. - px
           qy   = 1. - py
           var(ix,iy,iz) = &
                qy*(qx*radtab(itt  ,ir,ivar,ilam)+px*radtab(itt  ,ir+1,ivar,ilam)) + &
                py*(qx*radtab(itt+1,ir,ivar,ilam)+px*radtab(itt+1,ir+1,ivar,ilam))
        enddo
     enddo
  enddo

  if (do_exp) then
     do iz=izs,ize
        var(:,:,iz) = exp(var(:,:,iz))
     enddo
  endif

end subroutine lookup_rad

!...............................................................................
subroutine pressure (rho, ee, pp)

  use params, only: mx, my, mz, dbg_eos, mid
  use eos, only: do_cubic_eos
  implicit none

  character(len=mid):: Id = 'pressure'

  real, dimension(mx,my,mz), intent(in)  :: rho,ee
  real, dimension(mx,my,mz), intent(out) :: pp
  logical :: omp_in_parallel
  logical :: do_indx, do_exp

  do_indx = .true.
  do_exp  = .true.

  if (omp_in_parallel()) then
     call lookup_eos(rho, ee, 1, pp, do_indx, do_exp, do_cubic_eos)
  else
     !$omp parallel
     call lookup_eos(rho, ee, 1, pp, do_indx, do_exp, do_cubic_eos)
     !$omp end parallel
  end if

  call print_trace (trim(Id), dbg_eos, 'END')

end subroutine pressure

!...............................................................................
subroutine temperature (rho, ee, tt)

  use params, only: mx, my, mz, dbg_eos, mid
  use eos, only: do_cubic_eos
  implicit none

  character(len=mid):: Id = 'temperature'

  real, dimension(mx,my,mz), intent(in)  :: rho, ee
  real, dimension(mx,my,mz), intent(out) :: tt
  logical :: omp_in_parallel
  logical :: do_indx, do_exp

  do_indx = .true.
  do_exp  = .true.

  if (omp_in_parallel()) then
     call lookup_eos(rho, ee, 3, tt, do_indx, do_exp, do_cubic_eos)
  else
     !$omp parallel
     call lookup_eos(rho, ee, 3, tt, do_indx, do_exp, do_cubic_eos)
     !$omp end parallel
  end if

  call print_trace (trim(Id), dbg_eos, 'END')

end subroutine temperature

!...............................................................................
subroutine rosseland (rho, tt, lnrkRoss)

  use params, only: mx, my, mz, dbg_eos, mid
  use eos
  use table, only: Nlam
  implicit none

  character(len=mid):: Id = 'rosseland'

  real, dimension(mx,my,mz), intent(in)  :: rho, tt
  real, dimension(mx,my,mz), intent(out) :: lnrkRoss
  logical :: omp_in_parallel
  logical :: do_indx, do_exp

  do_indx = .true.
  do_exp  = .false.

  if (omp_in_parallel()) then
     call lookup_rad(rho, tt, 1, Nlam+1, lnrkRoss, do_indx, do_exp)
  else
     !$omp parallel
     call lookup_rad(rho, tt, 1, Nlam+1, lnrkRoss, do_indx, do_exp)
     !$omp end parallel
  end if

  call print_trace (trim(Id), dbg_eos, 'END')

end subroutine rosseland

!...............................................................................
subroutine chiPlanck (rho, tt, lnrkPlanck)

  ! returns ln of Planck mean of extinction coefficient

  use params, only: mx, my, mz, dbg_eos, mid
  use eos
  use table, only: Nlam

  implicit none

  ! ID String
  character(len=mid):: Id = 'chiPlanck'

  real, dimension(mx,my,mz), intent(in)  :: rho, tt
  real, dimension(mx,my,mz), intent(out) :: lnrkPlanck
  logical :: omp_in_parallel
  logical :: do_indx, do_exp

  do_indx = .true.
  do_exp  = .false.

  if (omp_in_parallel()) then
     call lookup_rad(rho, tt, 2, Nlam+1, lnrkPlanck, do_indx, do_exp)
  else
     !$omp parallel
     call lookup_rad(rho, tt, 2, Nlam+1, lnrkPlanck, do_indx, do_exp)
     !$omp end parallel
  end if

  call print_trace (trim(Id), dbg_eos, 'END')

end subroutine chiPlanck


!...............................................................................
subroutine chiStd (rho, tt, lnrkStd)

  ! returns ln of extinction coefficient at standard wavelength
  ! (default: 500 nm)

  use params, only: mx, my, mz, dbg_eos, mid
  use eos
  use table, only: Nlam
  implicit none

  character(len=mid):: Id = 'chiStd'

  real, dimension(mx,my,mz), intent(in)  :: rho, tt
  real, dimension(mx,my,mz), intent(out) :: lnrkStd
  logical :: omp_in_parallel
  logical :: do_indx, do_exp

  do_indx = .true.
  do_exp  = .false.


  if (omp_in_parallel()) then
     call lookup_rad(rho, tt, 3, Nlam+1, lnrkStd, do_indx, do_exp)
  else
     !$omp parallel
     call lookup_rad(rho, tt, 3, Nlam+1, lnrkStd, do_indx, do_exp)
     !$omp end parallel
  end if

  call print_trace (trim(Id), dbg_eos, 'END')

end subroutine chiStd

!...............................................................................
subroutine elnumdens (rho, ee, lnNel)

  ! returns ln of electron number density (in internal units)'

  use params, only: mx, my, mz, dbg_eos, master, mid, hl
  use eos,    only: do_cubic_eos
  use table,  only: Ntbvar
  implicit none

  character(len=mid):: Id = 'elnumdens'

  real, dimension(mx,my,mz), intent(in)  :: rho, ee
  real, dimension(mx,my,mz), intent(out) :: lnNel
  logical :: omp_in_parallel
  logical :: do_indx, do_exp
  logical :: has_elnumdens

  do_indx = .true.
  do_exp  = .false.

  has_elnumdens = Ntbvar.eq.6

  if (.not.has_elnumdens) then
     if (master) then
        print '(A)', hl
        print '(A)', &
             trim(Id)//': EOS table does not contain electron number density. STOP.'
        call abort_mpi
        stop
     end if
  end if

  if (omp_in_parallel()) then
     call lookup_eos(rho, ee, 4, lnNel, do_indx, do_exp, do_cubic_eos)
  else
     !$omp parallel
     call lookup_eos(rho, ee, 4, lnNel, do_indx, do_exp, do_cubic_eos)
     !$omp end parallel
  end if

  call print_trace (trim(Id), dbg_eos, 'END')

end subroutine elnumdens


!...............................................................................
subroutine elpressure (rho, ee, Pel)

  ! returns electron pressure in internal units

  use params, only: mx, my, mz, izs, ize, dbg_eos, master, mid, hl
  use eos,    only: do_cubic_eos
  use table,  only: Ntbvar, lnkBoltz, uup, uul
  implicit none

  character(len=mid):: Id = 'elpressure'

  real, dimension(mx,my,mz), intent(in)  :: rho,ee
  real, dimension(mx,my,mz), intent(out) :: Pel
  real, dimension(mx,my,mz)              :: lnT
  ! lncfact: electron pressure, ln of conversion factor -> sim units
  real    :: lncfact
  integer :: iz
  logical :: omp_in_parallel
  logical :: do_indx, do_exp
  logical :: has_elnumdens

  do_indx = .true.
  do_exp  = .true.

  has_elnumdens = Ntbvar.eq.6

  if (.not.has_elnumdens) then
     if (master) then
        print '(A)', hl
        print '(A)', &
             trim(Id)//': EOS table does not contain electron number density. STOP.'
        call abort_mpi
        stop
     end if
  end if

  if (omp_in_parallel()) then
     call lookup_eos(rho, ee, 4, Pel, do_indx, .false., do_cubic_eos)
     call lookup_eos(rho, ee, 3, lnT, do_indx, .false., do_cubic_eos)
  else
     !$omp parallel
     call lookup_eos(rho, ee, 4, Pel, do_indx, .false., do_cubic_eos)
     call lookup_eos(rho, ee, 3, lnT, do_indx, .false., do_cubic_eos)
     !$omp end parallel
  end if

  ! electron pressure, conversion factor to sim units:
  ! lnPel and lnNel in internal units, lnkBoltz in cgs units

  lncfact = lnkboltz - log(uup) - 3.*log(uul)
  do iz=izs,ize
     Pel(:,:,iz) = exp( Pel(:,:,iz) + lnT(:,:,iz) + lncfact )
  enddo

  call print_trace (trim(Id), dbg_eos, 'END')

end subroutine elpressure

!...............................................................................
subroutine lookup_eos_dln_slice(rhoS, eeS, ivar, DlnVarDlne, DlnVarDlnr)

  use params, only: mx, mz, izs, ize
  use eos
  use table
  implicit none

  ! rhos = density, horizontal slice
  ! ees  = internal energy per unit mass, horizontal slice

  real, dimension(mx,mz)              :: rhoS, eeS
  real, dimension(mx,mz), intent(out) :: DlnVarDlne, DlnVarDlnr

  integer :: ie, ir
  real    :: px, py, qx, qy
  real    :: ri, ei

  integer :: ivar,ix,iz

  do iz=izs,ize
     do ix=1,mx

        ! force rhoS and eeS to within table limits
        rhoS(ix,iz) = min(max(rhoS(ix,iz),rholimMinE), rholimMaxE)
        eeS(ix,iz)  = min(max(eeS(ix,iz),  eelimMinE),  eelimMaxE)

        ri = (log(rhoS(ix,iz))-lnrMinE)/dlnrE + 1.0
        ir = max0(1,min0(Nrtabe-1,int(ri)))
        px = ri-ir

        ei = (log(eeS(ix,iz))-lnemine)/dlneE + 1.0
        ie = max0(1,min0(Netabe-1,int(ei)))
        py = ei-ie

        qx   = 1. - px
        qy   = 1. - py

        ! lookup DlnVarDlne: note that the derivatives 
        !stored in the tables are multiplied by dlne
        DlnVarDlne(ix,iz) = &
             qy*(qx*eostab(ie  ,ir,2,ivar)+px*eostab(ie  ,ir+1,2,ivar))+ &
             py*(qx*eostab(ie+1,ir,2,ivar)+px*eostab(ie+1,ir+1,2,ivar))
        DlnVarDlne(ix,iz) = DlnVarDlne(ix,iz) / dlneE


        ! lookup dlnpdlnr:  note that the derivatives 
        ! stored in the tables are multiplied by dlnr
        DlnVarDlnr(ix,iz) = &
             qy*(qx*eostab(ie  ,ir,3,ivar)+px*eostab(ie  ,ir+1,3,ivar))+ &
             py*(qx*eostab(ie+1,ir,3,ivar)+px*eostab(ie+1,ir+1,3,ivar))
        DlnVarDlnr(ix,iz)   =  DlnVarDlnr(ix,iz) / dlnrE

     end do
  end do

end subroutine lookup_eos_dln_slice


!...............................................................................
subroutine lookup_eos_slice(rhoS, eeS, ivar, var, do_exp)

  use params, only: mx, mz, izs, ize, mpi_rank
  use eos
  use table
  implicit none

  ! rhoS = density, horizontal slice
  ! eeS  = internal energy (per unit mass), horizontal slice

  real, dimension(mx,mz)  :: rhoS, eeS
  real, dimension(mx,mz), intent(out) :: var

  integer :: ie, ir
  real :: ri, ei
  real :: px, py, qx, qy, pxqx, pyqy
  real :: dfx1, dfx2, dfy1, dfy2
  real :: rmin, rmax, emin, emax

  integer :: ivar, ix, iz
  logical :: do_exp, is_outside_tab


  rmin  = minval( rhoS(:,:) )
  rmax  = maxval( rhoS(:,:) )
  emin  = minval(  eeS(:,:) )
  emax  = maxval(  eeS(:,:) )

  is_outside_tab = &
       rmin.lt.rholimMinE .or. rmax.gt.rholimMaxE   .or. &
       emin.lt.eelimMinE  .or. emax.gt.eelimMaxE

  if (verbose_tab .and. is_outside_tab) then
     print '(a,a,i4)', &
          'lookup_eos_slice: density and energy restrained within limits, ', &
          'mpi_rank ',mpi_rank
  endif

  if (do_cubic_eos) then  

     do iz=izs,ize
        do ix=1,mx

           ! force rhoS and eeS to within table limits

           rhoS(ix,iz) = min(max(rhoS(ix,iz),rholimMinE),rholimMaxE)
           eeS(ix,iz)  = min(max( eeS(ix,iz), eelimMinE), eelimMaxE)

           ri = (log(rhoS(ix,iz))-lnrMinE)/dlnrE + 1.0
           ir = max0(1,min0(Nrtabe-1,int(ri)))
           px = ri-ir

           ei = (log(eeS(ix,iz))-lnemine)/dlneE + 1.0
           ie = max0(1,min0(Netabe-1,int(ei)))
           py = ei-ie

           qx   = 1. - px
           qy   = 1. - py
           pxqx = px*qx
           pyqy = py*qy

           dfx1 = eostab(ie  ,ir+1,1,ivar)-eostab(ie  ,ir  ,1,ivar)
           dfx2 = eostab(ie+1,ir+1,1,ivar)-eostab(ie+1,ir  ,1,ivar)
           dfy1 = eostab(ie+1,ir  ,1,ivar)-eostab(ie  ,ir  ,1,ivar)
           dfy2 = eostab(ie+1,ir+1,1,ivar)-eostab(ie  ,ir+1,1,ivar)

           var(ix,iz) = qy*(                                           &
                &             qx*(      eostab(ie  ,ir  ,1,ivar)          &
                &               + pxqx*(eostab(ie  ,ir  ,3,ivar)-dfx1)    &
                &               + pyqy*(eostab(ie  ,ir  ,2,ivar)-dfy1))   &
                &           + px*(      eostab(ie  ,ir+1,1,ivar)          &
                &               - pxqx*(eostab(ie  ,ir+1,3,ivar)-dfx1)    &
                &               + pyqy*(eostab(ie  ,ir+1,2,ivar)-dfy2))   &
                &                                                      )  &
                &        + py*(                                           &
                &             px*(      eostab(ie+1,ir+1,1,ivar)          &
                &               - pxqx*(eostab(ie+1,ir+1,3,ivar)-dfx2)    &
                &               - pyqy*(eostab(ie+1,ir+1,2,ivar)-dfy2))   &
                &           + qx*(      eostab(ie+1,ir  ,1,ivar)          &
                &               + pxqx*(eostab(ie+1,ir  ,3,ivar)-dfx2)    &
                &               - pyqy*(eostab(ie+1,ir  ,2,ivar)-dfy1))   &
                &                                                      )

        end do
     end do

  else

     do iz=izs,ize
        do ix=1,mx

           rhoS(ix,iz) = max(rhoS(ix,iz),rholimE)
           eeS(ix,iz)  = min(max( eeS(ix,iz),eelimE ),eelimMaxE)

           ri = (log(rhoS(ix,iz))-lnrMinE)/dlnrE + 1.0
           ir = max0(1,min0(Nrtabe-1,int(ri)))
           px = ri-ir

           ei = (log(eeS(ix,iz))-lneMinE)/dlneE + 1.0
           ie = max0(1,min0(Netabe-1,int(ei)))
           py = ei-ie

           qx   = 1. - px
           qy   = 1. - py

           var(ix,iz) = &
                qy*(qx*eostab(ie  ,ir,1,ivar)+px*eostab(ie  ,ir+1,1,ivar))+ &
                py*(qx*eostab(ie+1,ir,1,ivar)+px*eostab(ie+1,ir+1,1,ivar))

        end do
     end do
  end if

  if (do_exp) then
     do iz=izs,ize
        var(:,iz) = exp(var(:,iz))
     end do
  end if

end subroutine lookup_eos_slice


!...............................................................................
subroutine tmake_eos_slice(rhoS, ttS, eeS)

  use params, only: mx, mz, izs, ize, master, hl
  use eos
  use table
  implicit none

  ! rhos = density, horizontal slice
  ! tts  = temperature, horizontal slice
  ! ees  = internal energy (per unit mass), horizontal slice

  real, dimension(mx,mz)  :: rhoS, ttS, eeS
  real, dimension(mx,mz)  :: ttS1, Dtt, Dee, DlnTDlne, DlnTDlnr

  integer :: i
  real    :: err
  integer, parameter :: imax=15
  real,    parameter :: eps=1.0e-4, DeeMax=1.0
  logical :: converged, do_iter

  i         =  0
  err       =  1.0
  converged = .false.
  do_iter   = .true.

  do while ( do_iter )

     call lookup_eos_slice(rhoS,eeS,3,ttS1,.true.)
     call lookup_eos_dln_slice(rhoS,eeS,3,DlnTDlne,DlnTDlnr)

     Dtt     =   ttS - ttS1
     Dee     =   Dtt / DlnTDlne / ttS1 * eeS
     Dee     =   Dee / (1.+abs(Dee/DeeMax))
     eeS     =   eeS + Dee

     err     = maxval( abs( Dtt / ttS ) ) 
     converged = (err < eps)
     do_iter = ( .not.converged .and. (i < imax) )
     i       = i+1

  end do

  if ( .not.converged .and. ( i > imax ) ) then
     if (master) then
        print '(A)', hl
        print '(A)','tmake_eos_slice: Warning! Not converged!'
        print '(A)', hl
     end if
  end if

end subroutine tmake_eos_slice

!...............................................................................
subroutine lookup_eos_alt(nx, ny, nz, rho, ee, ivar, var, do_indx, do_exp, do_cubic)

  use params, only: ub, mid, mpi_rank, hl
  use eos
  use table
  implicit none

  character(len=mid):: Id = 'lookup_eos_alt'

  integer, intent(in) :: nx, ny, nz
  integer, intent(in) :: ivar
  logical, intent(in) :: do_indx, do_exp, do_cubic

  real, dimension(nx,ny,nz)              :: rho, ee
  real, dimension(nx,ny,nz), intent(out) :: var

  integer  :: nx_chk, ny_chk, nz_chk
  integer  :: ie, ir
  real     :: px, py, qx, qy, pxqx, pyqy, dfx1, dfx2, dfy1, dfy2
  real     :: rmin, rmax, emin, emax, ri, ei

  integer  :: ix, iy, iz
  logical  :: is_outside, is_alloc, do_alloc


  is_alloc = allocated(iearr_e_alt)
  if (is_alloc) then
     nx_chk = size(iearr_e_alt,dim=1)
     ny_chk = size(iearr_e_alt,dim=2)
     nz_chk = size(iearr_e_alt,dim=3)
     do_alloc = nx_chk.ne.nx .and. ny_chk.ne.ny .and. nz_chk.ne.nz
  else
     do_alloc = .true.
  end if

  if (do_alloc) then
     if (is_alloc) then
        deallocate( iearr_e_alt, irarr_e_alt, pxarr_e_alt, pyarr_e_alt )
        deallocate( pbot0_alt, dlnpdE_r_alt, dlnpdlnr_E_alt )
     end if
     allocate( iearr_e_alt(nx,ny,nz), irarr_e_alt(nx,ny,nz) ) 
     allocate( pxarr_e_alt(nx,ny,nz), pyarr_e_alt(nx,ny,nz) )
     allocate( pbot0_alt(nx,nz), dlnpdE_r_alt(nx,nz), dlnpdlnr_E_alt(nx,nz) )
  end if


  if (do_indx) then

     rmin = minval(rho(:,:,:))
     rmax = maxval(rho(:,:,:))
     emin = minval(ee(:,:,:))
     emax = maxval(ee(:,:,:))

     is_outside = &
          rmin.lt.rholimMinE.or.rmax.gt.rholimMaxE.or. &
          emin.lt. eelimMinE.or.emax.gt. eelimMaxE

     if (is_outside) then 
        if (do_limit_tab) then
           ! Force rho and ee to witihin table limits
           do iz=1,nz
              do iy=1,ny
                 do ix=1,nx
                    rho(ix,iy,iz) = min(max(rho(ix,iy,iz),rholimMinE),rholimMaxE)
                    ee(ix,iy,iz)  = min( max(ee(ix,iy,iz), eelimMinE), eelimMaxE)
                 enddo
              enddo
           enddo
           ! Print log entry
           if (verbose_tab) then
              print '(A)', hl
              print '(A,X,I5)', &
                   trim(Id)//&
                   ': Density and energy forced to be within limits. mpi_rank =', &
                   mpi_rank
           end if
        else
           ! Error message and exit
           print '(A)', hl
           print '(A,X,I5)', &
                trim(Id)//&
                ': Outside table boundaries. mpi_rank =', &
                mpi_rank
           print '(A)', trim(Id)//': lnrmin, lnrmax, lnemin, lnemax ='
           print '(A,X,4(X,E13.6))',' (TABLE) ', lnrMinE, lnrMaxE, lneMinE, lneMaxE
           print '(A,X,4(X,E13.6))',' (SIM)   ', log(rMin), log(rMax), log(eMin), log(eMax)
           print '(a)', hl
           call abort_mpi
           stop
        end if
     end if

     do iz=1,nz
        do iy=1,ny
           do ix=1,nx
              ri = (log(rho(ix,iy,iz))-lnrMinE)/dlnrE + 1.0
              ir = max0(1,min0(Nrtabe-1,int(ri)))
              pxarr_e_alt(ix,iy,iz) = ri-ir
              irarr_e_alt(ix,iy,iz) = ir

              ei = (log(ee(ix,iy,iz))-lneMinE)/dlneE + 1.0
              ie = max0(1,min0(Netabe-1,int(ei)))
              pyarr_e_alt(ix,iy,iz) = ei-ie
              iearr_e_alt(ix,iy,iz) = ie
           end do
        end do
     end do

  endif

  if (do_cubic) then  

     do iz=1,nz
        do iy=1,ny
           do ix=1,nx

              ir = irarr_e_alt(ix,iy,iz)
              ie = iearr_e_alt(ix,iy,iz)
              px = pxarr_e_alt(ix,iy,iz)
              py = pyarr_e_alt(ix,iy,iz)
              qx   = 1. - px
              qy   = 1. - py
              pxqx = px*qx
              pyqy = py*qy

              dfx1 = eostab(ie  ,ir+1,1,ivar)-eostab(ie  ,ir  ,1,ivar)
              dfx2 = eostab(ie+1,ir+1,1,ivar)-eostab(ie+1,ir  ,1,ivar)
              dfy1 = eostab(ie+1,ir  ,1,ivar)-eostab(ie  ,ir  ,1,ivar)
              dfy2 = eostab(ie+1,ir+1,1,ivar)-eostab(ie  ,ir+1,1,ivar)

              var(ix,iy,iz) = qy*(                                           &
                   &             qx*(      eostab(ie  ,ir  ,1,ivar)          &
                   &               + pxqx*(eostab(ie  ,ir  ,3,ivar)-dfx1)    &
                   &               + pyqy*(eostab(ie  ,ir  ,2,ivar)-dfy1))   &
                   &           + px*(      eostab(ie  ,ir+1,1,ivar)          &
                   &               - pxqx*(eostab(ie  ,ir+1,3,ivar)-dfx1)    &
                   &               + pyqy*(eostab(ie  ,ir+1,2,ivar)-dfy2))   &
                   &                                                      )  &
                   &        + py*(                                           &
                   &             px*(      eostab(ie+1,ir+1,1,ivar)          &
                   &               - pxqx*(eostab(ie+1,ir+1,3,ivar)-dfx2)    &
                   &               - pyqy*(eostab(ie+1,ir+1,2,ivar)-dfy2))   &
                   &           + qx*(      eostab(ie+1,ir  ,1,ivar)          &
                   &               + pxqx*(eostab(ie+1,ir  ,3,ivar)-dfx2)    &
                   &               - pyqy*(eostab(ie+1,ir  ,2,ivar)-dfy1))   &
                   &                                                      )

           end do
        end do
     end do

  else

     do iz=1,nz
        do iy=1,ny
           do ix=1,nx
              ir = irarr_e_alt(ix,iy,iz)
              ie = iearr_e_alt(ix,iy,iz)
              px = pxarr_e_alt(ix,iy,iz)
              py = pyarr_e_alt(ix,iy,iz)
              qx   = 1. - px
              qy   = 1. - py
              var(ix,iy,iz) =qy*(qx*eostab(ie  ,ir,1,ivar)+px*eostab(ie  ,ir+1,1,ivar))+ &
                   &         py*(qx*eostab(ie+1,ir,1,ivar)+px*eostab(ie+1,ir+1,1,ivar))
           end do
        end do
     end do
  end if


  ! Pressure: calculate auxiliary arrays needed by boundary routines
  if (ivar.eq.1) then

     do iz=1,nz
        do ix=1,nx

           pbot0_alt(ix,iz) = var(ix,ub,iz)
           ir = irarr_e_alt(ix,ub,iz)
           ie = iearr_e_alt(ix,ub,iz)
           px = pxarr_e_alt(ix,ub,iz)
           py = pyarr_e_alt(ix,ub,iz)
           qx   = 1. - px
           qy   = 1. - py

           ! lookup dlnpdlne: note that the derivatives stored 
           ! in the tables are multiplied by dlne
           dlnpdE_r_alt(ix,iz)   =  &
                qy*(qx*eostab(ie  ,ir,2,ivar)+px*eostab(ie  ,ir+1,2,ivar)) + &
                py*(qx*eostab(ie+1,ir,2,ivar)+px*eostab(ie+1,ir+1,2,ivar))

           ! dlnpde = dlnpdlne / ee   (in simulation units!)
           dlnpdE_r_alt(ix,iz)   =  dlnpdE_r_alt(ix,iz) / ee(ix,ub,iz) / dlneE

           ! lookup dlnpdlnr:  note that the derivatives stored 
           ! in the tables are multiplied by dlnr
           dlnpdlnr_E_alt(ix,iz) =  &
                qy*(qx*eostab(ie  ,ir,3,ivar)+px*eostab(ie  ,ir+1,3,ivar)) + &
                py*(qx*eostab(ie+1,ir,3,ivar)+px*eostab(ie+1,ir+1,3,ivar))
           dlnpdlnr_E_alt(ix,iz) = dlnpdlnr_E_alt(ix,iz) / dlnrE

        end do
     end do

     if (do_exp) then
        pbot0_alt(:,:) = exp(pbot0_alt(:,:))
     end if
  end if

  if (do_exp) then
     do iz=1,nz
        var(:,:,iz) = exp(var(:,:,iz))
     end do
  end if

end subroutine lookup_eos_alt


!...............................................................................
subroutine lookup_rad_alt( nx, ny, nz, rho, tt, ivar, ilam, var, do_indx, do_exp)

  use params, only: mid, mpi_rank, hl
  use eos
  use table
  implicit none

  character(len=mid) :: Id = 'lookup_rad_alt'

  integer, intent(in) :: nx,ny,nz
  integer, intent(in) :: ivar, ilam
  logical, intent(in) :: do_indx, do_exp

  real, dimension(nx,ny,nz)              :: rho,tt
  real, dimension(nx,ny,nz), intent(out) :: var

  integer  :: nx_chk, ny_chk, nz_chk

  integer  :: ir, itt
  real     :: ri, ti
  real     :: px, py, qx, qy
  real     :: rmin, rmax, tmin, tmax

  integer  :: ix, iy, iz
  logical  :: is_outside, is_alloc, do_alloc

  is_alloc = allocated(irarr_r_alt)
  if (is_alloc) then
     nx_chk = size(irarr_r_alt,dim=1)
     ny_chk = size(irarr_r_alt,dim=2)
     nz_chk = size(irarr_r_alt,dim=3)
     do_alloc = nx_chk.ne.nx .and. ny_chk.ne.ny .and. nz_chk.ne.nz
  else
     do_alloc = .true.
  end if

  if (do_alloc) then
     if (is_alloc) then
        deallocate( irarr_r_alt, itarr_r_alt, pxarr_r_alt, pyarr_r_alt )
     end if
     allocate( irarr_r_alt(nx,ny,nz), itarr_r_alt(nx,ny,nz) ) 
     allocate( pxarr_r_alt(nx,ny,nz), pyarr_r_alt(nx,ny,nz) )
  end if

  if (do_indx) then

     rmin = minval(rho(:,:,:))
     rmax = maxval(rho(:,:,:))
     tmin = minval(tt(:,:,:))
     tmax = maxval(tt(:,:,:))

     is_outside = &
          rmin.lt.rholimMinR.or.rmax.gt.rholimMaxR.or.&
          tmin.lt. ttlimMinR.or.tmax.gt. ttlimMaxR

     if (is_outside) then
        if (do_limit_tab) then
           ! Force rho and tt to be witihin table limits
           do iz=1,nz
              do iy=1,ny
                 do ix=1,nx
                    rho(ix,iy,iz) = min(max(rho(ix,iy,iz),rholimMinR),rholimMaxR)
                    tt(ix,iy,iz)  = min( max(tt(ix,iy,iz), ttlimMinR), ttlimMaxR)
                 end do
              end do
           end do
           ! Print log entry
           if (verbose_tab) then
              print '(A)', hl
              print '(A,X,I5)', &
                   trim(Id)//&
                   ': Density and temperature forced to be within limits. mpi_rank =', &
                   mpi_rank
           end if
        else
           ! Error message and exit
           print '(A)', hl
           print '(A,X,I5)', &
                trim(Id)//&
                ': Outside table boundaries. mpi_rank =', &
                mpi_rank
           print '(A)', trim(Id)//': lnrmin, lnrmax, lntmin, lntmax ='
           print '(A,X,4(X,E13.6))',' (TABLE) ', lnrMinR, lnrMaxR, lntMinR, lntMaxR
           print '(A,X,4(X,E13.6))',' (SIM)   ', log(rMin), log(rMax), log(tMin), log(tMax)
           print '(a)', hl
           call abort_mpi
           stop
        end if
     end if

     do iz=1,nz
        do iy=1,ny
           do ix=1,nx
              ri = (log(rho(ix,iy,iz))-lnrMinR)/dlnrR + 1.0
              ir = max0(1,min0(Nrtabr-1,int(ri)))
              pxarr_r_alt(ix,iy,iz) = ri-ir
              irarr_r_alt(ix,iy,iz) = ir

              ti = (log(tt(ix,iy,iz))-lntMinR)/dlntR + 1.0
              itt = max0(1,min0(Nttabr-1,int(ti)))
              pyarr_r_alt(ix,iy,iz) = ti-itt
              itarr_r_alt(ix,iy,iz) = itt
           end do
        end do
     end do
  end if


  do iz=1,nz
     do iy=1,ny
        do ix=1,nx
           ir = irarr_r_alt(ix,iy,iz)
           itt= itarr_r_alt(ix,iy,iz)
           px = pxarr_r_alt(ix,iy,iz)
           py = pyarr_r_alt(ix,iy,iz)
           qx   = 1. - px
           qy   = 1. - py
           var(ix,iy,iz) = &
                qy*(qx*radtab(itt  ,ir,ivar,ilam)+px*radtab(itt  ,ir+1,ivar,ilam)) + &
                py*(qx*radtab(itt+1,ir,ivar,ilam)+px*radtab(itt+1,ir+1,ivar,ilam))
        end do
     end do
  end do

  if (do_exp) then
     do iz=1,nz
        var(:,:,iz) = exp(var(:,:,iz))
     end do
  end if

end subroutine lookup_rad_alt


!...............................................................................
subroutine temperature_alt (nx, ny, nz, rho, ee, tt)

  use params, only: dbg_eos, mid
  use eos, only: do_cubic_eos
  implicit none

  character(len=mid):: Id = 'temperature_alt'

  integer, intent(in)                    :: nx, ny, nz
  real, dimension(nx,ny,nz)              :: rho, ee
  real, dimension(nx,ny,nz), intent(out) :: tt

  logical :: omp_in_parallel
  logical :: do_indx, do_exp

  do_indx = .true.
  do_exp  = .true.

  if (omp_in_parallel()) then
     call lookup_eos_alt(nx, ny, nz, rho, ee, 3, tt, do_indx, do_exp, do_cubic_eos)
  else
     !$omp parallel
     call lookup_eos_alt(nx, ny, nz, rho, ee, 3, tt, do_indx, do_exp, do_cubic_eos)
     !$omp end parallel
  end if

  call print_trace (trim(Id), dbg_eos, 'END')

end subroutine temperature_alt

!...............................................................................
subroutine rosseland_alt (nx, ny, nz, rho, tt, lnrkRoss)

  use params, only: dbg_eos, mid
  use eos
  use table, only: Nlam
  implicit none

  character(len=mid):: Id = 'rosseland_alt'

  integer,                   intent(in)    :: nx,ny,nz
  real, dimension(nx,ny,nz)                :: rho,tt
  real, dimension(nx,ny,nz), intent(out)   :: lnrkRoss

  logical :: omp_in_parallel
  logical :: do_indx, do_exp

  do_indx = .true.
  do_exp  = .false.

  if (omp_in_parallel()) then
     call lookup_rad_alt(nx, ny, nz, rho, tt, 1, Nlam+1, lnrkRoss, do_indx, do_exp)
  else
     !$omp parallel
     call lookup_rad_alt(nx, ny, nz, rho, tt, 1, Nlam+1, lnrkRoss, do_indx, do_exp)
     !$omp end parallel
  end if

  call print_trace (trim(Id), dbg_eos, 'END')

end subroutine rosseland_alt

!...............................................................................
subroutine lookup_eos_test(nx, ny, nz, rho, ee, ivar, ider, var, &
     do_indx, do_exp, do_cubic)

  ! This is a modified and more general version of lookup_eos;
  ! The routine allows to look up table variables as well as their
  ! derivatives

  use params, only: ub, mid, mpi_rank, hl
  use eos
  use table
  implicit none

  character(len=mid):: Id = 'lookup_eos_test'

  integer, intent(in) :: nx, ny, nz
  integer, intent(in) :: ivar
  integer, intent(in) :: ider

  logical, intent(in) :: do_indx, do_exp, do_cubic

  real, dimension(nx,ny,nz)              :: rho, ee
  real, dimension(nx,ny,nz), intent(out) :: var

  integer  :: nx_chk, ny_chk, nz_chk
  integer  :: ie, ir
  real     :: px, py, qx, qy, pxqx, pyqy
  real     :: dfx1, dfx2, dfy1, dfy2
  real     :: rmin, rmax, emin, emax, ri, ei

  integer, allocatable, dimension(:,:,:), save :: iearr_e1, irarr_e1
  real,    allocatable, dimension(:,:,:), save :: pxarr_e1, pyarr_e1

  integer  :: ix, iy, iz
  logical  :: is_outside, is_alloc, do_alloc


  is_alloc = allocated(iearr_e1)
  if (is_alloc) then
     nx_chk = size(iearr_e1,dim=1)
     ny_chk = size(iearr_e1,dim=2)
     nz_chk = size(iearr_e1,dim=3)
     do_alloc = nx_chk.ne.nx .and. ny_chk.ne.ny .and. nz_chk.ne.nz
  else
     do_alloc = .true.
  end if

  if (do_alloc) then
     if (is_alloc) then
        deallocate( iearr_e1, irarr_e1 )
        deallocate( pxarr_e1, pyarr_e1 )
     end if
     allocate( iearr_e1(nx,ny,nz), irarr_e1(nx,ny,nz) ) 
     allocate( pxarr_e1(nx,ny,nz), pyarr_e1(nx,ny,nz) )
  end if


  if (do_indx) then

     rmin = minval(rho(:,:,:))
     rmax = maxval(rho(:,:,:))
     emin = minval(ee(:,:,:))
     emax = maxval(ee(:,:,:))

     is_outside = &
          rmin.lt.rholimMinE.or.rmax.gt.rholimMaxE.or. &
          emin.lt. eelimMinE.or.emax.gt. eelimMaxE

     if (is_outside) then 
        if (do_limit_tab) then
           ! Force rho and ee to witihin table limits
           do iz=1,nz
              do iy=1,ny
                 do ix=1,nx
                    rho(ix,iy,iz) = min(max(rho(ix,iy,iz),rholimMinE),rholimMaxE)
                    ee(ix,iy,iz)  = min( max(ee(ix,iy,iz), eelimMinE), eelimMaxE)
                 enddo
              enddo
           enddo
           ! Print log entry
           if (verbose_tab) then
              print '(A)', hl
              print '(A,X,I5)', &
                   trim(Id)//&
                   ': Density and energy forced to be within limits. mpi_rank =', &
                   mpi_rank
           end if
        else
           ! Error message and exit
           print '(A)', hl
           print '(A,X,I5)', &
                trim(Id)//&
                ': Outside table boundaries. mpi_rank =', &
                mpi_rank
           print '(A)', trim(Id)//': lnrmin, lnrmax, lnemin, lnemax ='
           print '(A,X,4(X,E13.6))',' (TABLE) ', lnrMinE, lnrMaxE, lneMinE, lneMaxE
           print '(A,X,4(X,E13.6))',' (SIM)   ', log(rMin), log(rMax), log(eMin), log(eMax)
           print '(a)', hl
           call abort_mpi
           stop
        end if
     end if

     do iz=1,nz
        do iy=1,ny
           do ix=1,nx
              ri = (log(rho(ix,iy,iz))-lnrMinE)/dlnrE + 1.0
              ir = max0(1,min0(Nrtabe-1,int(ri)))
              pxarr_e1(ix,iy,iz) = ri-ir
              irarr_e1(ix,iy,iz) = ir

              ei = (log(ee(ix,iy,iz))-lneMinE)/dlneE + 1.0
              ie = max0(1,min0(Netabe-1,int(ei)))
              pyarr_e1(ix,iy,iz) = ei-ie
              iearr_e1(ix,iy,iz) = ie
           end do
        end do
     end do

  endif

  ! cubic interpolation only for variable
  if (ider.eq.1 .and. do_cubic) then  

     do iz=1,nz
        do iy=1,ny
           do ix=1,nx

              ir = irarr_e1(ix,iy,iz)
              ie = iearr_e1(ix,iy,iz)
              px = pxarr_e1(ix,iy,iz)
              py = pyarr_e1(ix,iy,iz)
              qx   = 1. - px
              qy   = 1. - py
              pxqx = px*qx
              pyqy = py*qy

              dfx1 = eostab(ie  ,ir+1,1,ivar)-eostab(ie  ,ir  ,1,ivar)
              dfx2 = eostab(ie+1,ir+1,1,ivar)-eostab(ie+1,ir  ,1,ivar)
              dfy1 = eostab(ie+1,ir  ,1,ivar)-eostab(ie  ,ir  ,1,ivar)
              dfy2 = eostab(ie+1,ir+1,1,ivar)-eostab(ie  ,ir+1,1,ivar)

              var(ix,iy,iz) = qy*(                                           &
                   &             qx*(      eostab(ie  ,ir  ,1,ivar)          &
                   &               + pxqx*(eostab(ie  ,ir  ,3,ivar)-dfx1)    &
                   &               + pyqy*(eostab(ie  ,ir  ,2,ivar)-dfy1))   &
                   &           + px*(      eostab(ie  ,ir+1,1,ivar)          &
                   &               - pxqx*(eostab(ie  ,ir+1,3,ivar)-dfx1)    &
                   &               + pyqy*(eostab(ie  ,ir+1,2,ivar)-dfy2))   &
                   &                                                      )  &
                   &        + py*(                                           &
                   &             px*(      eostab(ie+1,ir+1,1,ivar)          &
                   &               - pxqx*(eostab(ie+1,ir+1,3,ivar)-dfx2)    &
                   &               - pyqy*(eostab(ie+1,ir+1,2,ivar)-dfy2))   &
                   &           + qx*(      eostab(ie+1,ir  ,1,ivar)          &
                   &               + pxqx*(eostab(ie+1,ir  ,3,ivar)-dfx2)    &
                   &               - pyqy*(eostab(ie+1,ir  ,2,ivar)-dfy1))   &
                   &                                                      )

           end do
        end do
     end do

  else

     ! look up derivative or variable, linear interpolation only
     do iz=1,nz
        do iy=1,ny
           do ix=1,nx
              ir = irarr_e1(ix,iy,iz)
              ie = iearr_e1(ix,iy,iz)
              px = pxarr_e1(ix,iy,iz)
              py = pyarr_e1(ix,iy,iz)
              qx   = 1. - px
              qy   = 1. - py
              var(ix,iy,iz) =qy*(qx*eostab(ie  ,ir,ider,ivar)+px*eostab(ie  ,ir+1,ider,ivar))+ &
                   &         py*(qx*eostab(ie+1,ir,ider,ivar)+px*eostab(ie+1,ir+1,ider,ivar))
           end do
        end do
     end do

  end if

  ! if derivative, apply scaling factor: derivatives stored in the tables
  ! are multiplied by dlne or dlnr

  ! dln f / dln E
  if (ider.eq.2) then
     do iz=1,nz
        var(:,:,iz) = var(:,:,iz) / dlneE
     end do
  end if

  ! dln f / dln r
  if (ider.eq.3) then
     do iz=1,nz
        var(:,:,iz) = var(:,:,iz) / dlnrE
     end do
  end if

  ! if variable and do_exp=.true. then exponeniate
  if (ider.eq.1.and.do_exp) then
     do iz=1,nz
        var(:,:,iz) = exp(var(:,:,iz))
     end do
  end if

end subroutine lookup_eos_test
!...............................................................................
