!...............................................................................
module cooling

  use params, only: mfile

  integer, parameter:: mmu=9, mphi=10
  real    :: dphi, form_factor, dtauMin, dtauMax, tauMin, tauMax
  integer :: nmu, nphi, verbose, ny0
  integer :: mblocks
  integer :: n1, n2

  real :: y_newton, dy_newton, t_newton, ee_newton, ee_min, t_ee_min

  real, dimension(mmu):: mu0

  real, pointer, dimension(:,:,:) :: rk, lnrk, s, q, dtau, qq, qqr
  real, pointer, dimension(:,:,:) :: tau, lnrkap, lnsrc, lnxx, lnrkRoss

  real, allocatable, target, dimension(:,:,:) :: scr7, scr8, scr9, scr10, scr11
  real, allocatable, target, dimension(:,:,:) :: scr12


  logical do_table, do_newton, do_limb, do_yrad
  character(len=mfile):: intfile

end module cooling

!...............................................................................
subroutine init_cooling (r,e)

  use params
  use arrays
  use cooling

  implicit none

  real, dimension(mx,my,mz):: r,e

  integer i
  character(len=mfile):: name


  do_cool     = .true.

  i           = index(file,'.')

  nmu         = 0
  nphi        = 4
  dphi        = 0.
  form_factor = 1.
  dtaumax     = 200.
  dtaumin     = 0.1
  intfile     = name('intensity.dat','int',file)

  do_newton   = .false.
  y_newton    = -0.3
  dy_newton   =  0.05
  t_newton    =  0.01
  t_ee_min    =  0.005
  ee_newton   = -1.
  ee_min      =  3.6
  do_limb     = .false.
  verbose     =  0
  ny0         =  0

  ! do adaptive radiation depth scale?
  do_yrad     =  .false.


  ! mblocks= number of subdomains communicated per side in trnslt routine;
  ! default: communicate only nearest neighbour
  ! taumin, taumax: limits for adaptive radiation scale (radiation_scale routine);
  mblocks     =  1
  tauMin      =  1.0e-6
  tauMax      =  2.0e2


  call read_cooling

  rk    => scr1
  lnrk  => scr2
  s     => scr3
  q     => scr4
  dtau  => scr5
  qq    => scr6

  allocate(  scr7(mx,my,mz),  scr8(mx,my,mz),  scr9(mx,my,mz) )
  allocate(  scr10(mx,my,mz), scr11(mx,my,mz), scr12(mx,my,mz) )

  tau      => scr7
  lnrkap   => scr8
  lnsrc    => scr9
  lnxx     => scr10
  lnrkRoss => scr11
  qqr      => scr12
 
end subroutine init_cooling

!...............................................................................
subroutine read_cooling

  use params
  use eos
  use cooling

  namelist /cool/ do_cool, nmu, nphi, dphi, form_factor, dtaumin, dtaumax, intfile, &
       do_newton, y_newton, dy_newton, t_newton, ee_newton, ee_min, t_ee_min, do_limb, mu0, &
       ny0, do_yrad, verbose, mblocks, tauMin, tauMax

  character(len=mfile):: name, fname
  character(len=mid)  :: id='$Id: radiation_eosrad_mpi_ympi.f90,v 1.1 2016/10/12 13:08:47 remo Exp $'

  call print_id(id)

  rewind (stdin); read (stdin, cool)

  if (nmu==0 .and. form_factor==1.)  form_factor=0.4

  if (master) write (*,cool)

  if (do_limb) then
     fname = name('limb.dat','lmb',file)
     call mpi_name(fname)
     open (limb_unit, file=trim(fname), form='unformatted', status='unknown')
     write (limb_unit) mx, mz, nmu, nphi
  end if

end subroutine read_cooling

!...............................................................................
subroutine coolit (r, ee, lne, dd, dedt)

  use params
  use arrays, only: qqav, qqrav
  use units
  use eos
  use cooling

  implicit none

  character(len=mid)  :: id='$Id: radiation_eosrad_mpi_ympi.f90,v 1.1 2016/10/12 13:08:47 remo Exp $'

  real, dimension(mx,my,mz) :: r, ee, lne, dd, dedt
  real, dimension(mx,my,mz) :: tt
  real, dimension(mx,mz)    :: surfi
  !real, dimension(my)       :: yrad, dyraddn

  ! ym0, ymN1: values of ym depth in the layers immediately above and below local MPI subdomain
  ! dtau0, dtauN1: values of dtau immediately above and below local subdomain
  real                      :: ym0,   ymN1          
  real, dimension(mx,1,mz)  :: dtau0, dtauN1

  ! tauIntr: intrinsic optical depth scale
  real, dimension(mx,my,mz) :: tauIntr

  real          :: xmu(mmu), wmu(mmu)
  real          :: sec, fdtime, dxdy, dzdy, tanth, qqTmp, f1, f2
  real          :: phi, wphi, womega

  integer       :: ix, iy, iz, lmu, lphi, imu, iphi, ilam, ny
  integer       :: lrec, imaxval_mpi
  integer       :: mTauTop, mTauBot, nDTauTop, nDTauBot
  
  logical       :: debug
  logical       :: flag_scr, flag_snap, flag_limb, flag_surf, do_io, do_surfi
  logical, save :: first=.true.

  real          :: cput(2), void, dtime, prof
  integer, save :: nrad=0, nang=0

  integer, dimension( 0:mpi_nx-1, 0:mpi_ny-1, 0:mpi_nz-1 ) :: nTop_mpi_cart, nBot_mpi_cart
  integer, dimension( mpi_size )   :: nTop_buffer, nBot_buffer
  integer, dimension( 0:mpi_ny-1 ) :: nTop_maxval_xz, nBot_maxval_xz
  integer  :: ix_mpi, iy_mpi, iz_mpi
  integer  :: irank

  character(len=mfile) :: name, fname
  
  ! compute global tau scale?
  logical       :: do_tau

  external transfer


  ! print subroutine's ID
  call print_id(id)
  
  ! skip ratiative transfer ?
  if (.not. do_cool)  return

  ! consistent treatment of scattering ?
  if (flag_scat) then

     if (master) then
        print '(x,a)', repeat("-",72)
        print '(x,a)', id 
        print '(4x,a)', 'warning: iterative solver for scattering case not available. STOP.'
        print '(a)', ''
     endif

     call abort_mpi
     stop

  end if


  if (debug(dbg_cool) .and. omp_master) then
     void = dtime(cput)
     print '(1x,a8,2f7.3)','rad0   ', cput*omp_nthreads
  end if


  ! reset dedt in ghost zones, if present (lb > 1)
  if (lb > 1) then 
     dedt(:,1:lb-1,izs:ize) = 0.
  end if


  !  Angular quadrature:
  !  nmu > 0: Use Gauss quadrature (mu=1 excluded), accuracy: 2*nmu   order
  !  nmu = 0: One vertical ray with weight 0.5
  !  nmu < 0: Use Radau quadrature (mu=1 included), accuracy: 2*nmu-1 order

  if      (nmu .gt. 0) then
     lmu = nmu
     call gausi (lmu,0.,1.,wmu,xmu)
  else if (nmu .eq. 0) then
     lmu = 1
     xmu(1) = 1.
     wmu(1) = 0.5
  else 
     lmu =-nmu
     call radaui(lmu,0.,1.,wmu,xmu)
  end if
  if (mu0(1) .ne. 0.) xmu=mu0

  if (first) then
     call barrier_omp ('rad-first')
     if (omp_master) first = .false.
     if (master) then
        print '(a4,a10,a8,a10)', 'imu', 'xmu', 'theta'
        do imu=1,lmu 
           print '(i4,f10.3,f8.1,f10.3)',imu,xmu(imu),acos(xmu(imu))*180./pi,wmu(imu)
        end do
     end if
  end if

  ! lookup temperature and Rosseland extinction coefficient

  call temperature (r, ee, tt   )
  call rosseland   (r, tt, lnrkRoss )

  ! copy lnrkRoss to lnrk, required by radiation_scale;
  ! initialize rk and heating rates (qq)

  do iz=izs,ize
     lnrk(:,:,iz) = lnrkRoss(:,:,iz)
     rk(:,:,iz)   = 0. 
     qq(:,:,iz)   = 0.
  enddo

  ! compute dtau, do not compute tau scale
  do_tau = .false.
  call tau_calc( dym, my, 1.0, tauIntr, nDTauTop, nDTauBot, do_tau )

  ! set do_yrad to .false. for current version of coolit
  if ( do_yrad ) then
     if ( master ) then
        print '(x,a)', repeat("-",72)
        print '(x,a)', id 
        print '(4x,a)', 'warning: this version of the subroutine does not use the yrad scale,'
        print '(4x,a)', '         set do_yrad to .false.'
        print '(a)', ''
     end if
     do_yrad = .false.
  end if


  ! solve radiative transfer for the entire subdomain
  ny = my

  call dumpn (r,   'r',   'rad.dmp',0)
  call dumpn (ee,  'ee',  'rad.dmp',1)
  call dumpn (lnrkRoss,'lnrkRoss','rad.dmp',1)
  call dumpn (lnrkRoss,'lnrkRoss','trf.dmp',0)

  if (debug(dbg_cool) .and. omp_master) then
     void = dtime(cput)
     print '(1x,a8,2f7.3)','lookup ',cput*omp_nthreads
  end if


  ! loop over bins or wavelengths
  do ilam=1,nlam

     !lookup lnxx (ln ratio of bin/monochromatic and Rosseland opacity)
     !lnsrc (ln source function * weight; simulation units)

     call lookup_rad (r, tt, 1, ilam, lnxx,  .false., .false.)
     call lookup_rad (r, tt, 2, ilam, lnsrc, .false., .false.)

     !compute ln extinction coefficient
     do iz=izs,ize
        lnrkap(:,:,iz) =lnrkRoss(:,:,iz) + lnxx(:,:,iz)
     enddo


     ! loop over angles

     do imu=1,lmu

        if (nmu .eq. 0 .and. imu .eq. 1) then
           lphi = 1 
        else if (nmu .lt. 0 .and. imu .eq. lmu) then
           lphi = 1 
        else
           lphi = nphi
        end if
        tanth = tan(acos(xmu(imu)))
        wphi = 4.*pi/lphi


        do iphi=1,lphi

           phi = (iphi-1)*2.*pi/lphi + dphi*t
           dxdy = tanth*cos(phi)
           dzdy = tanth*sin(phi)

           if (master .and. verbose==1 .and. isubstep==1) then
              print '(a,3i5,2e15.7)',' imu,iphi,ny,dxdy,dzdy=', &
                   imu,iphi,ny,dxdy,dzdy
           end if


           ! flags for data output 

           flag_scr  = do_io(t+dt, tscr, iscr+iscr0,   nscr)
           flag_snap = do_io(t+dt, tsnap,isnap+isnap0, nsnap)

           ! flag_limb: limb darkening every scratch 
           ! or else every nsnap and only mu=1
           ! and only for first substep and bin:

           flag_limb = do_limb   .and. flag_scr
           flag_limb = flag_limb .or.  (xmu(imu)==1. .and. flag_snap)
           flag_limb = flag_limb .and. isubstep.eq.1 .and. ilam.eq.1

           ! compute surface intensity ?

           flag_surf = flag_scr  .or.  flag_snap
           flag_surf = flag_surf .and. xmu(imu)==1. .and. isubstep.eq.1 

           do_surfi  = flag_surf .or.  flag_limb


           ! solve radiative transfer on hydro depth scale

           ! trnslt: tilt lnrkap -> lnrk
           ! tau_calc: compute optical depth; lnrk -> rk=exp(lnrk) 
           ! tau_calc: return nDTauTop (-> n1) used by transfer routine 
           ! find n2, used by trnslt routine

           call trnslt(lnrkap, lnrk, dxdy, dzdy, .false., my, mblocks, verbose)
           call dumpn (lnrk,'lnrk','trf.dmp',1)
           do_tau = .false.
           call tau_calc(dym, my, xmu(imu), tauIntr, nDTauTop, nDTauBot, do_tau )


           !! Gather nDTauTop and nDTauBot from all processes and store them in a 3D array
           !! with the same geometry as the MPI Cartesian communicator
           !call mpi_cart_gather_int ( nDTauTop, 1, nTop_buffer )
           !call mpi_cart_gather_int ( nDTauBot, 1, nBot_buffer )
           !do irank=0,mpi_size-1
           !   ix_mpi = ix_mpi_cart( irank+1 )
           !   iy_mpi = iy_mpi_cart( irank+1 )
           !   iz_mpi = iz_mpi_cart( irank+1 )
           !   nTop_mpi_cart( ix_mpi, iy_mpi, iz_mpi ) = nTop_buffer( irank+1 ) 
           !   nBot_mpi_cart( ix_mpi, iy_mpi, iz_mpi ) = nBot_buffer( irank+1 ) 
           !end do

           !! find maximum of nTop and nBot on horizontal MPI planes
           !do iy_mpi=0,mpi_ny
           !   nTop_maxval_xz( iy_mpi ) = maxval( nTop_mpi_cart( :, iy_mpi, :) )
           !   nBot_maxval_xz( iy_mpi ) = maxval( nBot_mpi_cart( :, iy_mpi, :) )
           !end do        

           ! !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
           !! The following portion of code possibly needs fixing
           ! !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
           !n1 = nTop_maxval_xz( mpi_y )
           !n2 = nBot_maxval_xz( mpi_y )
           !if ( mpi_y .lt. mpi_ny-1 ) then             
           !   if ( maxval(nTop_maxval_xz( mpi_y+1:mpi_ny)) .gt. 0) then
           !      n1 = my 
           !   end if
           !   if ( maxval(nBot_maxval_xz( mpi_y+1:mpi_ny)) .gt. 0) then 
           !      n2 = my+1
           !   end if
           !end if
           ! !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

           !n1 = nDTauTop
           !if (ny0==0) then
           !   n2 = imaxval_mpi (nDTauBot)
           !else
           !   n2 = ny0
           !end if

           ! trnslt: tilt source function, exponentiate; lnsrc -> s
           call trnslt(lnsrc, s, dxdy, dzdy, .true., my, mblocks, verbose)
           call dumpn (s,'s','trf.dmp',1)


           ! transfer: solve raditative transfer equation
           ! dtau= optical depth step
           ! s   = source function
           ! q   = J-S split

           call transfer (my, 1, dtau, s, q, surfi, do_surfi )
           call dumpn (q,'q0','trf.dmp',1)

           if (isubstep.eq.1) then
              call dumpn(q,'q','remo.dmp',1)
           end if

           ! Compute partial heating rate (along present ray and bin):

           ! First, communicate the values of the geometrical depth ym and dtau immediately 
           ! above and below local subdomain; 
           ! Note: dtau0 is actually not used, it is just shipped together with dtauN1
           call mpi_send2_y ( ym,    1, my,  1,   ym0, 1,   ymN1, 1 )
           call mpi_send2_y ( dtau, mx, my, mz, dtau0, 1, dtauN1, 1 )

           ! Then, compute partial heating rate at the top:
           if (mpi_y .eq. 0) then
              do iz=izs,ize
                 q(:,1,iz) = q(:,1,iz) * rk(:,1,iz)
              end do
           else
              do iz=izs,ize
                 f2 = xmu(imu)/(ym(2)-ym0)
                 q(:,1,iz) = q(:,1,iz)*(dtau(:,2,iz)+dtau(:,1,iz))*f2
              end do
           end if

           ! Compute partial heating rate in the middle of the domain:
           do iz=izs,ize
              do iy=2,my-1
                 f2 = xmu(imu)/(ym(iy+1)-ym(iy-1))
                 q(:,iy,iz) = q(:,iy,iz)*(dtau(:,iy+1,iz)+dtau(:,iy,iz))*f2
              end do
           end do

           ! Finally, compute partial heating rate at the bottom:
           if (mpi_y .eq. mpi_ny-1) then
              do iz=izs,ize
                 q(:,my,iz) = q(:,my,iz) * rk(:,my,iz)
              end do
           else
              do iz=izs,ize
                 f2 = xmu(imu)/(ymN1-ym(my-1))
                 q(:,my,iz) = q(:,my,iz)*(dtauN1(:,1,iz)+dtau(:,my,iz))*f2
              end do
           end if
           call dumpn(q,'q1','trf.dmp',1)


           ! set s to zero explicitly
           ! trnslt: tilt q back to vertical grid, q -> s
           do iz=izs,ize
              s(:,:,iz) = 0.0
           end do
           call trnslt(q, s, -dxdy, -dzdy, .false., my, mblocks, verbose)
           call dumpn (s,'q2','trf.dmp',1)



           ! energy equation: radiative heating contribution

           f1 = wmu(imu)*wphi*form_factor
           do iz=izs,ize
              do iy=1,my
                 do ix=1,mx
                    qqTmp = f1*s(ix,iy,iz)
                    qq(ix,iy,iz)   =   qq(ix,iy,iz) + qqTmp
                    dedt(ix,iy,iz) = dedt(ix,iy,iz) + qqTmp
                 end do
              end do
           end do


           ! limb darkening, output

           if (flag_limb) then
              call barrier_omp('limb')
              if (do_limb .and. omp_master) then
                 write ( limb_unit ) t, imu, iphi, xmu(imu), phi, surfi
                 print *, omp_mythread, isubstep, ilam, imu, iphi
              end if
           end if

           ! integrated surface intensity

           if (flag_surf) then
              if (ilam .eq. 1) then      ! overwrite surface_int
                 do iz=izs,ize
                    surface_int(:,iz) = surfi(:,iz)
                 end do
              else                       ! add to surface_int
                 do iz=izs,ize
                    surface_int(:,iz) = surface_int(:,iz)+surfi(:,iz)
                 end do
              end if
           end if


        end do ! iphi=1,lphi
     end do ! imu=1,lmu
  enddo ! ilam=1,nlam

  call dumpn(qq,'qq','qq.dmp',1)


  ! compute (average) radiative flux
  call radiative_flux (qq,q)


  ! compute radiative heating rates per unit mass
  do iz=izs,ize
     qqr(:,:,iz) = qq(:,:,iz) / r(:,:,iz)
  end do


  ! compute horizontally averaged radiative heating rates 
  ! per unit volume and per unit mass

  call haverage_subr(qq,  qqav)
  call haverage_subr(qqr, qqrav)


  if (do_newton) then
     if (ee_newton > 0) eetop = ee_newton
     do iz=izs,ize
        do iy=1,my
           prof = exp(min((y_newton-ym(iy))/dy_newton,70.))
           prof = prof/(1.+prof)
           do ix=1,mx
              dedt(ix,iy,iz) = dedt(ix,iy,iz) &
                   - r(ix,iy,iz)*(ee(ix,iy,iz)-eetop)/t_newton*prof   &
                   - r(ix,iy,iz)*min(0.,ee(ix,iy,iz)-ee_min)/t_ee_min
           end do
        end do
     end do
  end if

  if (debug(dbg_cool) .and. omp_master) then
     void = dtime(cput)
     print '(1x,a8,2f7.3)','transfr',cput*omp_nthreads
  end if

end subroutine coolit

!...............................................................................
