README
======

Test code with alternative version of (eosrad) lookup routines using some basic object oriented programming structures.

In order to compile and run, simply invoke:

	make
	export GFORTRAN_CONVERT_UNIT='big_endian'
	./test_eos_tab.x
