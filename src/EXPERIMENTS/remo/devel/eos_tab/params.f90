!...............................................................................
module params

  implicit none

  integer, parameter :: mfile = 255
  integer, parameter :: mid = 127
  integer :: mx, my, mz
  integer :: lb, ub
  integer, parameter :: dbg_eos = 1024
  integer, parameter :: stdin = 35
  !integer :: mpi_size
  !integer :: mpi_rank
  logical :: master
  logical :: do_ionization
  character(len=mid):: hl = repeat('-',72)
  ! save
  
end module params

!...............................................................................
subroutine print_id( Id )

  use params
  implicit none

  character(len=mid) :: Id
  if (master) print '(X,A)', trim(Id)

end subroutine print_id

!...............................................................................
