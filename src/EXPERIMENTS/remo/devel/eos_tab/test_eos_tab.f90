!...............................................................................
program test_eos_tab

  use params
  use eos

  implicit none

  integer :: dim(3)
  real, allocatable, dimension(:,:,:) :: r, e, tt, lnr, ee
  real, allocatable, dimension(:,:,:) :: var

  character(len=mfile) :: dir, eostfile, radtfile, simfile, outfile, inputfile
  integer :: lun, recl

  
  ! master process? (set to true in this sample serial code)
  master = .true.

  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  ! define dimensions of data cubes (__necessary__!)
  ! note: hardcoded here for simplicity
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  mx = 120
  my = 120
  mz = 120

  ! indexes for top and bottom boundaries (i.e. excluding ghost zones)
  lb = 5
  ub = my-5

  ! dim array with data cube dimensions
  dim = (/ mx, my, mz /)

  ! allocate arrays
  allocate( r (mx,my,mz) )
  allocate( e (mx,my,mz) )
  allocate( tt (mx,my,mz) )
  allocate( lnr(mx,my,mz) )
  allocate( ee (mx,my,mz) )
  allocate( var(mx,my,mz) )


  
  ! input/output files
  inputfile = 'input.txt'
  dir = '/Users/remo/mconv/120x120x120/sun/2011-01-20'
  simfile = 'sun02.scr'
  outfile = 'output.dat'
  
  ! read eqofst namelist from input file, initialise EOS
  open (stdin, file=trim(inputfile), status='old')
  call init_eos
  close(stdin)

  ! open and read data cube
  ! note: recl = record length for each data cube
  lun = 23
  recl = 4*mx*my*mz
  open(lun, file=trim(dir)//'/'//trim(simfile), access='direct', &
       recl=recl, status='unknown')
  read(lun, rec=1) r
  read(lun, rec=5) e
  read(lun, rec=6) tt
  close(lun)
  ! ee  = energy density per unit mass
  ! lnr = ln density
  ee = e/r
  lnr = alog(r)

  
  ! open output file
  lun = 23
  recl = 4* mx * my * mz
  !!open(lun, file=trim(dir)//'/'//trim(outfile), access='direct', &
  open(lun, file=trim(outfile), access='direct', &
       recl=recl, status='unknown')

  ! example: lookup temperature using eostab%lookup method:
  !
  !  call eostab%lookup(dim, ee, r, var, ivar, ider, do_exp, eos_indxs)
  !  
  !  dim --> dimensions of input/output arrays
  !
  !  ee --> internal energy per unit mass
  !  r  --> density
  
  !  ivar = 1 --> ln Rosseland extinction coeff. per unit length
  !  ivar = 2 --> ln Pressure (gas + rad)
  !  ivar = 3 --> ln Temperature
  !  ivar = 4 --> ln electron number density
  !  ivar = 5 --> ln extinction coeff., Planck mean
  !  ivar = 6 --> ln extinction ceoff. at 500 nm
  !
  !  do_exp = .true. --> exponentiate variable
  !
  !  ider = 1 --> variable
  !  ider = 2 --> d ln variable / d ln ee
  !  ider = 3 --> d ln variable / d ln r
  !
  !  eos_indxs --> indexes arrays for speeding up subsequent lookup calls (optional)
  !
  call eostab%lookup(dim, ee, r, var, 3, 1, .true., eos_indxs)
  write (lun,rec=1) var

  ! example: lookup derivative of ln temperature with resepect to ln ee
  ! note: for ider=1 and =2, do_exp (set to false here) is actually ignored
  call eostab%lookup(dim, ee, r, var, 3, 2, .false., eos_indxs)
  write (lun,rec=2) var

  ! example: lookup derivative of ln temperature with resepect to ln density
  call eostab%lookup(dim, ee, r, var, 3, 3, .false., eos_indxs)
  write (lun,rec=3) var

  ! example: lookup temperature using wrapper subroutine
  call temperature(r,ee,var)
  write (lun,rec=4) var

  ! example: lookup pressure using wrapper subroutine
  call pressure(r,ee,var)
  write (lun,rec=5) var

  ! example: lookup electron pressure using wrapper subroutine
  call elpressure(r,ee,var)
  write (lun,rec=6) var

  ! example: lookup ln Rosseland extinction coefficient
  call eostab%lookup(dim, ee, r, var, 1, 1, .false., eos_indxs)
  write (lun,rec=7) var

  ! close output file
  close(lun)


  ! deallocate arrays
  deallocate( r, e, tt, lnr, ee)
  deallocate( var )
  
end program test_eos_tab

!...............................................................................
