NOTES
=====

--------------------------------------------------------------------------------
`eosrad_lookup_devel_f90`:

Lookup interpolation, cubic (12 terms), variants:

Current variant:

 var(i,j,k) = qx*(                                            &
      &         qy*(      table%tab(ix  ,iy  ,1,ivar)         &
      &           + pyqy*(table%tab(ix  ,iy  ,3,ivar)-dfy1)   &
      &           + pxqx*(table%tab(ix  ,iy  ,2,ivar)-dfx1))  &
      &       + py*(      table%tab(ix  ,iy+1,1,ivar)         &
      &          - pyqy*(table%tab(ix  ,iy+1,3,ivar)-dfy1)    &
      &          + pxqx*(table%tab(ix  ,iy+1,2,ivar)-dfx2))   &
      &                                                    )  &
      &     + px*(                                            &
      &         py*(      table%tab(ix+1,iy+1,1,ivar)         &
      &           - pyqy*(table%tab(ix+1,iy+1,3,ivar)-dfy2)   &
      &           - pxqx*(table%tab(ix+1,iy+1,2,ivar)-dfx2))  &
      &       + qy*(      table%tab(ix+1,iy  ,1,ivar)         &
      &           + pyqy*(table%tab(ix+1,iy  ,3,ivar)-dfy2)   &
      &           - pxqx*(table%tab(ix+1,iy  ,2,ivar)-dfx1))  &
      &                                                     )


Alternative:

 var(i,j,k) = qy*(                                               &
      &           qx*(      table%tab(ix  ,iy  ,1,ivar)          &
      &             + pxqx*(table%tab(ix  ,iy  ,2,ivar)-dfx1)    &
      &             + pyqy*(table%tab(ix  ,iy  ,3,ivar)-dfy1))   &
      &         + px*(      table%tab(ix+1,iy  ,1,ivar)          &
      &             - pxqx*(table%tab(ix+1,iy  ,2,ivar)-dfx1)    &
      &             + pxqx*(table%tab(ix+1,iy  ,3,ivar)-dfy2))   &
      &                                                       )  &
      &     + py*(                                               &
      &           qx*(      table%tab(ix  ,iy+1,1,ivar)          &
      &             + pxqx*(table%tab(ix  ,iy+1,2,ivar)-dfx2)    &
      &             - pyqy*(table%tab(ix  ,iy+1,3,ivar)-dfy1) )  &
      &        +  px*(      table%tab(ix+1,iy+1,1,ivar)          &
      &             - pxqx*(table%tab(ix+1,iy+1,2,ivar)-dfx2)    &
      &             - pyqy*(table%tab(ix+1,iy+1,3,ivar)-dfy2))   &
      &                                                       )

Note: the two variants are different approximations (the full cubic
version requires 16 terms) and are not identical, but give
sufficiently accurate results.


--------------------------------------------------------------------------------
Namelists: neater print out:

    print '(A)', '&EQOFST'
    print '(X,A14,X,A1,X,L)', 'do_eos', '=', do_eos
    print '(X,A14,X,A1,X,L)', 'do_ionization', '=', do_ionization
    print '(X,A14,X,A1,X,L)', 'do_table', '=', do_table
    print '(X,A14,X,A1,X,A)', 'eostabfile', '=', eostabfile
    print '(X,A14,X,A1,X,A)', 'radtabfile', '=', radtabfile
    print '(X,A14,X,A1,X,L)', 'do_cubic_eos', '=', do_cubic_eos
    print '(X,A14,X,A1,X,L)', 'do_limit_eos', '=', do_limit_eos
    print '(X,A14,X,A1,X,L)', 'do_limit_rad', '=', do_limit_rad
    print '(X,A14,X,A1,X,L)', 'verbose_tab', '=', verbose_tab
    print '(X,A14,X,A1,X,ES15.8)', 'uur', '=', uur
    print '(X,A14,X,A1,X,ES15.8)', 'uul', '=', uul
    print '(X,A14,X,A1,X,ES15.8)', 'uut', '=', uut
    print '(X,A14,X,A1,X,ES15.8)', 'rmin_lim_eos', '=', rmin_lim_eos
    print '(X,A14,X,A1,X,ES15.8)', 'rmax_lim_eos', '=', rmax_lim_eos
    print '(X,A14,X,A1,X,ES15.8)', 'eemin_lim_eos', '=', eemin_lim_eos
    print '(X,A14,X,A1,X,ES15.8)', 'eemax_lim_eos', '=', eemax_lim_eos
    print '(X,A14,X,A1,X,ES15.8)', 'rmin_lim_rad', '=', rmin_lim_rad
    print '(X,A14,X,A1,X,ES15.8)', 'rmax_lim_rad', '=', rmax_lim_rad
    print '(X,A14,X,A1,X,ES15.8)', 'ttmin_lim_rad', '=', ttmin_lim_rad
    print '(X,A14,X,A1,X,ES15.8)', 'ttmax_lim_rad', '=', ttmax_lim_rad
    print '(X,A)', '/'

