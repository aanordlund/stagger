!...............................................................................
module cooling

  use params, only: mfile
  use table

  integer, parameter :: mmu = 9
  integer, parameter :: mphi = 10
  integer :: nmu
  integer :: nphi
  integer :: verbose
  integer :: ny0
  integer :: lb0, ub0
  integer :: mblocks ! DEPRECATED
  real :: dphi
  real :: form_factor
  real :: dtaumin, dtaumax
  real :: tauMin, tauMax
  real :: y_newton, dy_newton, t_newton, ee_newton, ee_min, t_ee_min
  logical :: do_table
  logical :: do_newton
  logical :: do_limb
  logical :: do_yrad
  character(len=mfile) :: intfile
  real, dimension(mmu) :: mu0

  ! cooling: number of opacity bins (or wavelengths) in radtab
  integer :: nlam

  ! cooling:
  ! - quadrature type
  ! - number of ray directions
  ! - mu and phi values, weights
  integer, parameter   :: mlen = 63
  character(len=mlen)  :: quad
  integer, parameter   :: mray = 200
  integer              :: nray
  real, dimension(mray):: mu_ray, phi_ray, wt_ray

  ! limb darkening:
  ! - quadrature type
  ! - table and output file names
  ! - number of mu and phi values
  ! - ray directions
  ! - number of opacity bins / wavelengths
  character(len=mlen)  :: quad_limb
  character(len=mfile) :: tabfile_limb
  character(len=mfile) :: fname_limb
  type(rad_tab) :: limbtab
  type(tab_indx):: limb_indxs
  integer :: nmul
  integer :: nphil
  integer, parameter :: mlimb = mray
  integer            :: nlimb
  real, dimension(mlimb) :: mu_limb, phi_limb, wt_limb
  integer :: nlam_limb

  real, pointer, dimension(:,:,:) :: dens, emass, temp, lnrkRoss
  real, pointer, dimension(:,:,:) :: rk, lnrk, lnrkap, lnsrc, lnxx
  real, pointer, dimension(:,:,:) :: dtau, tau, s, q, qqvol
  real, pointer, dimension(:,:,:) :: tmpdata
  real, pointer, dimension(:,:,:) :: qq, qqr, qqscr

  real,allocatable,target,dimension(:,:,:) :: scr_01,scr_02,scr_03,scr_04,scr_05
  real,allocatable,target,dimension(:,:,:) :: scr_06,scr_07,scr_08,scr_09,scr_10
  real,allocatable,target,dimension(:,:,:) :: scr_11,scr_12,scr_13,scr_14,scr_15

  real, allocatable, dimension(:) :: yhydro, dyhydro, yrad, dyrad

  ! surface intensity arrays (single-bin and total)
  real, allocatable, dimension(:,:) :: surfi, surfi_tot

end module cooling

!...............................................................................
subroutine init_cooling (r,e)

  use params, only: master, file, do_cool, mx, my, mz, mfile, mid, limb_unit
  use arrays
  use cooling
  use eos, only: radtab
  use reshape_mod, only: reshape_mode, sub_nx, sub_nz, dim_re

  implicit none

  character(len=mid) :: Id = 'init_cooling'

  real, dimension(mx,my,mz) :: r, e
  integer :: nx, ny, nz
  !integer :: i
  character(len=mfile) :: name

  if (master) print '(X,A)', trim(Id)//': initialize'

  ! initialise cooling parameters
  !i =  index(file,'.')
  do_cool     = .true.
  quad        = 'undefined'
  nmu         =  0
  nphi        =  4
  dphi        =  0.
  form_factor =  1.
  dtaumax     =  500.
  dtaumin     =  0.1
  intfile     =  name('intensity.dat','int',file)
  do_newton   = .false.
  y_newton    = -0.3
  dy_newton   =  0.05
  t_newton    =  0.01
  ee_newton   = -1.
  ee_min      =  3.6
  t_ee_min    =  0.005
  do_limb     = .false.
  tabfile_limb= 'kaprhoT.tab'
  quad_limb   = 'undefined'
  nmul        =  2
  nphil       =  4
  verbose     =  0
  ny0         =  0

  ! do_yrad: adaptive radiation depth scale?
  do_yrad     = .false.

  ! mblocks= number of subdomains communicated per side in trnslt routine;
  ! default: communicate only nearest neighbour
  ! taumin, taumax: limits for adaptive radiation scale (radiation_scale routine);
  mblocks     =  1
  tauMin      =  1.0e-6
  tauMax      =  5.0e2

  ! reshape_mod control variables: default values
  ! reshape_mode: mode of array reshaping
  ! sub_n[x,z]: number of subdivisions in the [x,z] directions
  reshape_mode = 'ymrg_xzspl'
  sub_nx = 0
  sub_nz = 0

  ! initialize cooling, quadrature, limb darkening, and reshape
  call read_cooling
  call quad_init
  call limb_init
  call reshape_init

  ! nlam = number of opacity bins (or wavelengths)
  nlam = radtab%nlam

  ! dimensions of reshaped arrays
  nx = dim_re(1)
  ny = dim_re(2)
  nz = dim_re(3)

  ! allocate reshaped scratch arrays
  allocate( scr_01(nx,ny,nz), scr_02(nx,ny,nz), scr_03(nx,ny,nz) )
  allocate( scr_04(nx,ny,nz), scr_05(nx,ny,nz), scr_06(nx,ny,nz) )
  allocate( scr_07(nx,ny,nz), scr_08(nx,ny,nz), scr_09(nx,ny,nz) )
  allocate( scr_10(nx,ny,nz), scr_11(nx,ny,nz), scr_12(nx,ny,nz) )
  allocate( scr_13(nx,ny,nz), scr_14(nx,ny,nz), scr_15(nx,ny,nz) )

  ! radiative transfer variables, regular arrays:
  !  qq:  heating rate per unit volume
  !  qqr: heating rate per unit mass
  !  qqscr: scratch array
  qq       => scr1
  qqr      => scr2
  qqscr    => scr3

  ! radiative transfer variables, reshaped arrays:
  dens     => scr_01
  emass    => scr_02
  temp     => scr_03
  rk       => scr_04
  lnrk     => scr_05
  lnrkap   => scr_06
  lnrkRoss => scr_07
  lnsrc    => scr_08
  lnxx     => scr_09
  dtau     => scr_10
  tau      => scr_11
  s        => scr_12
  q        => scr_13
  qqvol    => scr_14

  ! scratch array for temporary data
  tmpdata  => scr_15

  ! allocate hydrodynamical and geometrical depth scales
  allocate( yhydro(ny), dyhydro(ny), yrad(ny), dyrad(ny) )

  ! allocate arrays for storing vertical outgoing intensities at the surface,
  ! partial and total
  allocate( surfi(nx,nz), surfi_tot(nx,nz) )

  ! define lb0 and ub0: limits of physical domain for reshaped arrays
  lb0 = 6
  ub0 = ny-5

  !! limb darkening:
  !! - open output files
  !! - write header with limb-darkening data dimensions
  !if (do_limb) then
  !   fname = name('limb.dat','lmb',file)
  !   call mpi_name(fname)
  !   open (limb_unit, file=trim(fname), form='unformatted', status='unknown')
  !   write (limb_unit) nx, nz, nlam_limb, nlimb
  !end if

end subroutine init_cooling

!...............................................................................
subroutine read_cooling

  use params, only: mid, master, stdin, do_cool
  use eos
  use cooling
  use reshape_mod, only: reshape_mode, sub_nx, sub_nz

  character(len=mid) :: Id = 'read_cooling'

  namelist /cool/ &
       do_cool, &
       quad, &
       nmu, &
       nphi, &
       dphi, &
       form_factor, &
       dtaumin, &
       dtaumax, &
       intfile, &
       do_newton, &
       y_newton, &
       dy_newton, &
       t_newton, &
       ee_newton, &
       ee_min, &
       t_ee_min, &
       do_limb, &
       tabfile_limb, &
       quad_limb, &
       nmul, &
       nphil, &
       mu0, &
       ny0, &
       do_yrad, &
       verbose, &
       mblocks, &
       tauMin, &
       tauMax, &
       reshape_mode, &
       sub_nx, &
       sub_nz

  ! read cool namelist, write to log
  rewind (stdin); read (stdin, cool)
  if (master) then
     print '(X,A)', trim(Id)//': read namelist:'
     write (*,nml=cool)
  end if

end subroutine read_cooling

!...............................................................................
subroutine coolit (r, ee, lne, dd, dedt)

  ! This subroutine computes the radiative heating rate, integrated over
  ! wavelength and solid angle;
  ! the radiative transfer equation is solved along a number of discrete 
  ! rays at various inclinations and for all opacity bins; 
  ! surface intensities for the limb darkening output are also computed 
  ! in a separate loop;
  !
  ! In this version, the calculation of heating rates and surface intensities
  ! is organised in two nested loops: 
  !
  !  - outer loop over opacity bins
  !  - inner loop over angles
  !
  !
  ! Surface intensities are computed and stored only when a snapshot is taken;
  ! limb darkening calculations are performed in a separate block of code
  ! to allow for more flexibility, e.g. solving for different sets of 
  ! diagnostic bins or angles.

  use params
  use arrays, only: qqav, qqrav
  use units
  use eos, only: eostab, radtab, eos_indxs, rad_indxs, flag_scat
  use cooling
  use variables, only: e
  use reshape_mod, only: dim_re

  implicit none

  character(len=mid) :: Id = 'coolit'
  character(len=mfile) :: name

  real, dimension(mx,my,mz) :: r, ee, lne, dd, dedt
  real, dimension(mxtot,mztot) :: surfi_full
  real :: xmu(mmu), wmu(mmu)
  real :: sec, fdtime, dxdy, dzdy, tanth, f1, f2
  real :: phi, wphi, womega
  real :: mu, weight
  real :: cput(2), void, dtime, prof
  real, parameter :: eps = 1.0e-6

  integer :: nx, ny, nz, lmu, lphi
  integer :: ix, iy, iz, imu, iphi, ilam, nymax
  integer :: lrec, imaxval_mpi
  integer :: mtauTop, mtauBot, ndtauTop, ndtauBot, ntop, nbot
  integer :: iray
  integer :: ilimb
  integer, save :: nrad = 0
  integer, save :: nang = 0
  integer :: ksnap

  logical :: debug
  logical :: flag_scr, flag_snap, flag_limb, flag_surf
  logical :: do_io
  logical :: do_surfi
  logical :: is_vertical

  logical, save :: first_time = .true.

  external transfer

  ! do_cool=.false. : no radiative transfer
  if (.not.do_cool)  return

  call timer('radiation','begin')

  ! consistent treatment of scattering?
  if (flag_scat) then
     if (master) print '(X,A)', &
          trim(Id)//': solver for scattering case not implemented yet. STOP.'
     call abort_mpi
     stop
  end if

  ! debug statements?
  if (debug(dbg_cool) .and. omp_master) then
     void = dtime(cput)
     print '(X,1(X,A8),2(X,F7.3))', 'rad0   ', cput * omp_nthreads
  end if


  ! reset dedt in ghost zones, if present (lb > 1)
  if (lb > 1) then
     dedt(:,1:lb-1,izs:ize) = 0.
  end if

  ! reset dedt in ghost zones, if present (ub < my)
  if (ub < my) then
     dedt(:,ub+1:my,izs:ize) = 0.
  end if

  ! number of opacity bins
  nlam = radtab%nlam

  ! dimensions of reshaped data cubes for radiative transfer calculations
  nx  = dim_re(1)
  ny  = dim_re(2)
  nz  = dim_re(3)

  ! reshape r (density) and ee (int energy per unit mass) arrays -> dens, emass
  call reshape_cube(r, dens)
  call reshape_cube(ee, emass)

  ! reshape (reconstruct) hydro depth scale yhydro and associated dyhydro
  call reshape_ymesh( ym,  yhydro)
  call reshape_ymesh(dym, dyhydro)

  ! lookup temperature, reshaped arrays
  call eostab%lookup(dim_re, emass, dens, temp, 3, 1, .true.)
  ! lookup Rosseland extinction coefficient - initialize indexes
  rad_indxs%do_indx = .true.
  call radtab%lookup(dim_re,temp,dens,lnrkRoss,nlam+1,1,.false.,rad_indxs)


  ! copy lnrkRoss to lnrk, required by radiation_scale;
  ! initialize rk and heating rate per unit volume qqvol (reshaped)
  do iz=1,nz
     lnrk (:,:,iz) = lnrkRoss(:,:,iz)
     rk   (:,:,iz) = 0.
     qqvol(:,:,iz) = 0.
  end do

  ! initialize radiative heating rate per unit volume qq (regular)
  do iz=izs,ize
     qq   (:,:,iz) = 0.
  end do


  ! depth scale
  if (do_yrad) then
     ! adaptive depth scale for radiative transfer
     call radiation_scale(yhydro, dyhydro, yrad, dyrad, mtauTop, mtauBot, .true.)
     nbot = mtauBot
  else
     ! radiation depth scale same as hydro depth scale
     call tau_calc(dyhydro, ny, 1.0, mtauTop, mtauBot, ndtauTop, ndtauBot)
     nbot = ndtauBot
  end if

  ! define nymax, max depth for trnslt and radiative transfer
  if (ny0==0) then
     nbot = imaxval_mpi (nbot)
     nymax = nbot
  else
     nymax = ny0
  end if


  if (debug(dbg_cool) .and. omp_master) then
     void = dtime(cput)
     print '(X,1(X,A8),2(X,F7.3))', 'lookup  ', cput * omp_nthreads
  end if


  ! flags for data output
  flag_scr  = do_io(t+dt, tscr, iscr+iscr0,   nscr)
  flag_snap = do_io(t+dt, tsnap,isnap+isnap0, nsnap)

  ! flags for surface intensity, vertical direction
  flag_surf = flag_scr .or. flag_snap .and. isubstep.eq.3

  ! flags for limb darkening output
  flag_limb = do_limb .and. flag_snap .and. isubstep.eq.3


  ! compute radiative heating rates

  ! loop over bins or wavelengths
  do ilam=1,nlam

     ! lookup lnxx (ln ratio of bin/monochromatic and Rosseland opacity)
     ! lnsrc (ln source function * weight; simulation units)
     ! note: recycle the p and q indexes determined by the previous lookup call
     !       (do_indx=.false.)
     call radtab%lookup(dim_re, temp, dens, lnxx, ilam, 1, .false., rad_indxs)
     call radtab%lookup(dim_re, temp, dens, lnsrc,ilam, 2, .false., rad_indxs)

     !compute ln extinction coefficient
     do iz=1,nz
        lnrkap(:,:,iz) = lnrkRoss(:,:,iz) + lnxx(:,:,iz)
     end do

     ! loop over rays
     do iray=1,nray

        mu    = mu_ray(iray)
        tanth = tan(acos(mu))
        phi   = phi_ray(iray) + dphi*t
        dxdy  = tanth*cos(phi)
        dzdy  = tanth*sin(phi)

        ! weight (including whole solid angle factor)
        weight = wt_ray(iray) * 4.0 * pi

        if (master) then
           if (verbose.eq.1 .and. isubstep.eq.1) then
              print '(X,1(X,A),2(X,I5),2(X,E15.7))', &
                   trim(Id)//': iray, nymax, dxdy, dzdy = ', &
                   iray, nymax, dxdy, dzdy
           end if
        end if

        ! compute surface intensity only if vertical ray and
        ! scratch file or snapshot file is due
        is_vertical = (abs(mu-1.0).le.eps)
        do_surfi = flag_surf.and.is_vertical

        if (do_yrad) then

           ! solve radiative transfer on radiation depth scale

           ! trnslt: tilt lnrkap -> tmpdata
           ! yinter: interpolate to radiation depth scale; tmpdata -> lnrk
           ! tau_calc: compute optical depth; lnrk -> rk=exp(lnrk)
           ! tau_calc: return ndtauTop (-> ntop) used by transfer routine
           call trnslt_alt(nx, ny, nz, yhydro, lnrkap, tmpdata, dxdy, dzdy, &
                .false., ny, mblocks, verbose)
           call yinter(ny, yhydro, ny, yrad, nx, nz, tmpdata, lnrk)
           call tau_calc(dyrad, ny, mu, mtauTop, mtauBot, ndtauTop, ndtauBot)
           ntop = ndtauTop
           nbot = ny

           ! trnslt: tilt source function lnsrc -> tmpdata
           ! yinter: interpolate to radiation depth scale; tmpdata -> s
           ! exponentiate: s -> exp(s)
           call trnslt_alt(nx, ny, nz, yhydro, lnsrc, tmpdata, dxdy, dzdy, &
                .false., ny, mblocks, verbose)
           call yinter(ny, yhydro, ny, yrad, nx, nz, tmpdata, s)
           do iz=1,nz
              s(:,:,iz) = exp( s(:,:,iz) )
           end do

           ! transfer: solve raditative transfer equation
           ! dtau= optical depth step
           ! s   = source function
           ! q   = J-S split
           !
           ! note: nbot = ny
           call transfer (nx, ny, nz, nbot, ntop, dtau, s, q, surfi, do_surfi )

           ! compute partial heating rate per ray per opacity bin
           ! note: nbot = ny
           do iz=1,nz
              q(:,1,iz) = q(:,1,iz) * rk(:,1,iz)
              do iy=2,nbot-1
                 f1 = 1. / (yrad(iy+1) - yrad(iy-1))
                 f2 = mu * f1
                 q(:,iy,iz) = q(:,iy,iz) * (dtau(:,iy+1,iz)+dtau(:,iy,iz)) * f2
              end do
              q(:,nbot,iz) = q(:,nbot,iz) * rk(:,nbot,iz)
           end do

           ! ydistr: reset tmpdata, distribute q back to hydro depth scale
           !         (q -> tmpdata)
           ! reset s to zero
           ! trnslt: tilt tmpdata back to vertical grid, tmpdata -> s
           ! note: nbot = ny
           call ydistr(ny, yrad, ny, yhydro, nx, nz, q, tmpdata)
           do iz=1,nz
              s(:,:,iz) = 0.
           end do
           call trnslt_alt(nx, ny, nz, yhydro, tmpdata, s, -dxdy, -dzdy, &
                .false., ny, mblocks, verbose)

        else

           ! solve radiative transfer on hydro depth scale

           ! trnslt: tilt lnrkap -> lnrk
           ! tau_calc: compute optical depth; lnrk -> rk=exp(lnrk)
           ! tau_calc: return ndtauTop (-> ntop) used by transfer routine
           ! find nbot, used by trnslt routine
           call trnslt_alt(nx, ny, nz, yhydro, lnrkap, lnrk, dxdy, dzdy, &
                .false., nymax, mblocks, verbose)
           call tau_calc(dyhydro,nymax,mu,mtauTop,mtauBot,ndtauTop,ndtauBot)
           ntop = ndtauTop
           if (ny0==0) then
              nbot = imaxval_mpi (ndtauBot)
           else
              nbot = ny0
           end if

           ! trnslt: tilt source function, exponentiate; lnsrc -> s
           call trnslt_alt(nx, ny, nz, yhydro, lnsrc, s, dxdy, dzdy, &
                .true., nbot, mblocks, verbose)
           call timer('radiation','trnslt')

           ! transfer: solve raditative transfer equation
           ! dtau= optical depth step
           ! s   = source function
           ! q   = J-S split
           call transfer (nx, ny, nz, nbot, ntop, dtau, s, q, surfi, do_surfi)

           ! compute partial heating rate per ray per opacity bin
           do iz=1,nz
              q(:,1,iz) = q(:,1,iz) * rk(:,1,iz)
              do iy=2,nbot-1
                 f1 = 1. / (yhydro(iy+1) - yhydro(iy-1))
                 f2 = mu * f1
                 q(:,iy,iz) = q(:,iy,iz) * (dtau(:,iy+1,iz)+dtau(:,iy,iz)) * f2
              end do
              q(:,nbot,iz) = q(:,nbot,iz) * rk(:,nbot,iz)
           end do
           ! set q to zero below iy=nbot
           ! note: TO DO: transfer should handle this internally
           !       with diffusion approximation

           if (nbot < ny) then
              do iz=1,nz
                 do iy=nbot+1,ny
                    q(:,iy,iz) = 0.
                 end do
              end do
           end if

           ! reset s to zero
           ! trnslt: tilt q back to vertical grid, q -> s
           do iz=1,nz
              s(:,:,iz) = 0.
           end do
           call trnslt_alt(nx, ny, nz, yhydro, q, s, -dxdy, -dzdy, &
                .false., nbot, mblocks, verbose)

        end if  ! do_yrad

        ! add contribution to radiative heating rate
        f1 = weight * form_factor
        do iz=1,nz
           do iy=1,nbot
              qqvol(:,iy,iz) = qqvol(:,iy,iz) + s(:,iy,iz) * f1
           end do
        end do

        ! surface intensity, vertical direction
        if (do_surfi) then
           if (ilam .eq. 1) then      ! overwrite surface intensity array
              do iz=1,nz
                 surfi_tot(:,iz) = surfi(:,iz)
              end do
           else                       ! add to surface intensity array
              do iz=1,nz
                 surfi_tot(:,iz) = surfi_tot(:,iz) + surfi(:,iz)
              end do
           end if
        end if

     end do  !iray
  end do  !ilam

  ! reconstruct surface intensity array: surfi_tot -> surface_int (params module)
  call merge_surf(surfi_tot, surface_int)

  ! deshape density and energy per unit mass, for consistency
  call deshape_cube(dens, r)
  call deshape_cube(emass, ee)

  ! make energy per unit volume e consistent with r and ee
  do iz=izs,ize
     e(:,:,iz) = ee(:,:,iz) * r(:,:,iz)
  end do

  ! deshape heating rate per unit volume: qqvol -> qq
  call deshape_cube(qqvol, qq)

  ! energy equation: add contribution of radiative heating rate per unit volume
  do iz=izs,ize
     dedt(:,:,iz) = dedt(:,:,iz) + qq(:,:,iz)
  end do

  ! compute (average) radiative flux
  call radiative_flux (qq, qqscr)

  ! compute radiative heating rates per unit mass
  do iz=izs,ize
     qqr(:,:,iz) = qq(:,:,iz) / r(:,:,iz)
  end do

  ! compute horizontally averaged radiative heating rates
  ! per unit volume and per unit mass
  call haverage_subr(qq,  qqav)
  call haverage_subr(qqr, qqrav)


  ! limb darkening diagnostics (only when a snapshot is taken)
  if (flag_limb) then

     ! open file for limb darkening output
     if (master) then
        ! define file name for full-surface intensity output
        ! append snapshot number ksnap (=isnap+1) to file name
        ! write header with dimensions
        fname_limb = name('limb.dat', 'lmb', file)
        ksnap = isnap+1
        call scp_name(fname_limb, ksnap)
        open (limb_unit, file=trim(fname_limb), &
             form='unformatted', status='unknown')
        write (limb_unit) t, mxtot, mztot, nlam_limb, nlimb
        print '(2(X,A))', trim(Id)//': limb darkening output to file:', &
             trim(fname_limb)
     end if

     ! compute surface intensity
     do_surfi = .true.

     ! lookup lnrkRoss from limbtab, for consistency
     call limbtab%lookup(dim_re, temp, dens, lnrkRoss, nlam_limb+1, 1, &
          .false., limb_indxs)

     ! loop over bins or wavelengths
     do ilam=1,nlam_limb

        ! lookup lnxx (ln ratio of bin/monochromatic and Rosseland opacity)
        ! lnsrc (ln source function * weight; simulation units)
        ! note: recycle the p and q indexes from previous lookup call
        !       (do_indx=.false.)
        call limbtab%lookup(dim_re,temp,dens,lnxx, ilam,1,.false.,limb_indxs)
        call limbtab%lookup(dim_re,temp,dens,lnsrc,ilam,2,.false.,limb_indxs)

        !compute ln extinction coefficient
        do iz=1,nz
           lnrkap(:,:,iz) = lnrkRoss(:,:,iz) + lnxx(:,:,iz)
        end do

        ! loop over mu angles for limb darkening
        do ilimb=1,nlimb

           mu    = mu_limb(ilimb)
           tanth = tan(acos(mu))
           phi   = phi_limb(ilimb)
           dxdy  = tanth*cos(phi)
           dzdy  = tanth*sin(phi)

           if (do_yrad) then

              ! solve radiative transfer on radiation depth scale

              ! trnslt: tilt lnrkap -> tmpdata
              ! yinter: interpolate to radiation depth scale; tmpdata -> lnrk
              ! tau_calc: compute optical depth; lnrk -> rk=exp(lnrk)
              ! tau_calc: return ndtauTop (-> ntop) used by transfer routine
              call trnslt_alt(nx, ny, nz, yhydro, lnrkap, tmpdata, dxdy, dzdy, &
                   .false., ny, mblocks, verbose)
              call yinter(ny, yhydro, ny, yrad, nx, nz, tmpdata, lnrk)
              call tau_calc(dyrad, ny, mu, mtauTop, mtauBot, ndtauTop, ndtauBot)
              ntop = ndtauTop
              nbot = ny

              ! trnslt: tilt source function lnsrc -> tmpdata
              ! yinter: interpolate to radiation depth scale; tmpdata -> s
              ! exponentiate: s -> exp(s)
              call trnslt_alt(nx, ny, nz, yhydro, lnsrc, tmpdata, dxdy, dzdy, &
                   .false., ny, mblocks, verbose)
              call yinter(ny, yhydro, ny, yrad, nx, nz, tmpdata, s)
              do iz=1,nz
                 s(:,:,iz) = exp( s(:,:,iz) )
              end do

              ! transfer: solve raditative transfer equation
              ! dtau= optical depth step
              ! s   = source function
              ! q   = J-S split
              ! note: nbot = ny
              call transfer (nx, ny, nz, nbot, ntop, dtau, s, q, surfi, do_surfi)

           else

              ! solve radiative transfer on hydro depth scale

              ! trnslt: tilt lnrkap -> lnrk
              ! tau_calc: compute optical depth; lnrk -> rk=exp(lnrk)
              ! tau_calc: return ndtauTop (-> ntop) used by transfer routine
              ! find nbot, used by trnslt routine
              call trnslt_alt(nx, ny, nz, yhydro, lnrkap, lnrk, dxdy, dzdy, &
                   .false., nymax, mblocks, verbose)
              call tau_calc(dyhydro,nymax,mu,mtauTop,mtauBot,ndtauTop,ndtauBot)
              ntop = ndtauTop
              if (ny0==0) then
                 nbot = imaxval_mpi (ndtauBot)
              else
                 nbot = ny0
              end if

              ! trnslt: tilt source function, exponentiate; lnsrc -> s
              call trnslt_alt(nx, ny, nz, yhydro, lnsrc, s, dxdy, dzdy, &
                   .true., nbot, mblocks, verbose)

              ! transfer: solve raditative transfer equation
              ! dtau= optical depth step
              ! s   = source function
              ! q   = J-S split
              call transfer (nx, ny, nz, nbot, ntop, dtau, s, q, surfi, do_surfi)

           end if  ! do_yrad

           ! master task only: gather surfi arrays from all subdomains
           ! and merge them into full-surface array
           call merge_surf_full(surfi, surfi_full)

           ! limb darkening: main output
           if (master) then
              write(limb_unit) ilam, ilimb, mu, phi, surfi_full
           end if

        end do  !ilimb

     end do  !ilam

     ! reconstruct surface intensity array: surfi_tot -> surface_int
     ! note: params module
     ! note: dimensions = mx * mz
     call merge_surf(surfi_tot, surface_int)

     ! close limb darkening file
     if (master) then
        close(limb_unit)
     end if

  end if  !flag_limb


  ! Newtonian cooling?
  if (do_newton) then
     if (ee_newton > 0) eetop = ee_newton
     do iz=izs,ize
        do iy=1,my
           prof = exp(min((y_newton-ym(iy))/dy_newton,70.))
           prof = prof/(1.+prof)
           do ix=1,mx
              dedt(ix,iy,iz) = dedt(ix,iy,iz) &
                   - r(ix,iy,iz)*(ee(ix,iy,iz)-eetop)/t_newton*prof   &
                   - r(ix,iy,iz)*min(0.,ee(ix,iy,iz)-ee_min)/t_ee_min
           end do
        end do
     end do
  end if

  if (debug(dbg_cool) .and. omp_master) then
     void = dtime(cput)
     print '(X,1(X,A),2(X,F7.3))', trim(Id), cput * omp_nthreads
  end if

  call timer('radiation','end')

end subroutine coolit

!...............................................................................
