!...............................................................................
module quad_mod

  use params, only: mid, master, pi
  implicit none

  integer, parameter :: mlen = 63

  ! quadrature angles (derived type)
  type, public :: quad_angles
     character(len=mlen) :: type
     integer :: n
     real, allocatable :: mu(:)
     real, allocatable :: phi(:)
     real, allocatable :: weight(:)
   contains
     procedure :: dealloc => dealloc_angles
     procedure :: alloc => alloc_angles
     procedure :: add => add_angles
     procedure :: set => set_angles
  end type quad_angles

contains

  !.............................................................................
  subroutine dealloc_angles(angles)
    implicit none
    character(len=mid) :: Id = 'dealloc_angles'
    class(quad_angles) :: angles
    logical :: is_alloc

    ! check if allocated
    is_alloc = allocated(angles%mu)
    if (is_alloc) then
       deallocate(angles%mu)
       deallocate(angles%phi)
       deallocate(angles%weight)
    end if

  end subroutine dealloc_angles

  !.............................................................................
  subroutine alloc_angles(angles, n)
    implicit none
    character(len=mid) :: Id = 'alloc_angles'
    class(quad_angles) :: angles
    integer :: n
    logical :: is_alloc

    ! n = number of directions
    angles%n = n

    ! (re)allocate angles%mu, phi, and weights arrays
    call angles%dealloc
    allocate(angles%mu(n))
    allocate(angles%phi(n))
    allocate(angles%weight(n))

  end subroutine alloc_angles

  !.............................................................................
  subroutine add_angles(angles, nadd, mu_add, phi_add, wt_add)
    implicit none
    character(len=mid) :: Id = 'add_angles'
    class(quad_angles) :: angles
    integer :: nadd
    real, dimension(nadd) :: mu_add, phi_add, wt_add
    integer :: nang, n, i, j
    real, dimension(:), allocatable :: mu_tmp, phi_tmp, wt_tmp
    logical :: is_alloc

    ! initialize nang
    nang = 0

    ! check if allocated
    is_alloc = allocated(angles%mu)

    if (is_alloc) nang = angles%n
    n = nang + nadd       

    ! allocate temporary arrays
    allocate(mu_tmp(n),phi_tmp(n),wt_tmp(n))

    ! copy existing angles to temporary arrays
    if (is_alloc.and.nang.gt.0) then
       do i=1,nang
          mu_tmp(i) = angles%mu(i)
          phi_tmp(i)= angles%phi(i)
          wt_tmp(i) = angles%weight(i)
       end do
    end if
    ! copy additional angles to temporary arrays
    do j=1,nadd
       i = j + nang
       mu_tmp(i) = mu_add(j)
       phi_tmp(i)= phi_add(j)
       wt_tmp(i) = wt_add(j)
    end do

    ! re-allocate angles
    call angles%alloc(n)

    ! copy temporary arrays to angles
    angles%mu = mu_tmp
    angles%phi= phi_tmp
    angles%weight = wt_tmp

    ! deallocate temporary arrays
    deallocate(mu_tmp,phi_tmp,wt_tmp)

  end subroutine add_angles

  !.............................................................................
  subroutine set_angles(angles, quad_type, nmu, nphi)
    implicit none
    character(len=mid) :: Id = 'set_angles'
    class(quad_angles) :: angles
    character(len=mlen) :: quad_type
    character(len=mlen) :: default_quad = 'radau'
    integer :: n
    integer :: i
    integer :: nmu, nphi
    integer :: lmu, lphi
    integer :: imu, iphi
    real, allocatable, dimension(:) :: xmu, wmu
    real :: theta
    real :: wphi
    real, parameter :: eps = 1.0e-6
    logical :: is_valid, is_alloc

    ! Carlson quadrature
    integer, parameter :: mcar = 36
    integer :: ncar
    integer :: icar
    integer, dimension(mcar) :: imux, imuy, imuz
    real, dimension(mcar) :: wcar


    ! check quadrature type; if not valid, set to default
    select case(quad_type)
    case('gauss', 'radau', 'vertical', 'carlson', 'ld')
       is_valid = .true.
    case default
       is_valid = .false.
    end select
    if (.not.is_valid) then
       quad_type = default_quad
       is_valid = .true.
       if (master) then
          print '(2(X,A))', &
               trim(Id)//': quad_type unavailable: set to', &
               trim(default_quad)
       end if
    end if

    ! allocate arrays for mu values and weights (xmu and wmu)
    select case(quad_type)
    case('gauss', 'radau', 'carlson')
       lmu = abs(nmu)
    case('vertical')
       lmu = 1
    case('ld')
       lmu = 17
       if (nmu.ne.lmu) then
          if (master) then
             print '(X,A,X,I2)',trim(Id)//': set nmu =',lmu
          end if
          nmu = lmu
       end if
    end select
    allocate(xmu(lmu),wmu(lmu))


    ! set angles based on quadrature type
    select case(quad_type)
    case ('gauss', 'radau', 'ld')

       ! compute mu values and weights
       ! n = number of directions
       if (quad_type.eq.'gauss') then
          call gausi (lmu,0.,1.,wmu,xmu)
          n = lmu * nphi
       else if (quad_type.eq.'radau') then
          call radaui(lmu,0.,1.,wmu,xmu)
          n = (lmu-1) * nphi + 1
       else if (quad_type.eq.'ld') then
          ! set of fixed angles for limb darkening calculations
          xmu = (/ 0.0100, 0.0250, 0.0500, 0.0750, &
               &   0.1000, 0.1250, 0.1500, 0.2000, &
               &   0.2500, 0.3000, 0.4000, 0.5000, &
               &   0.6000, 0.7000, 0.8000, 0.9000, &
               &   1.0000 /)
          ! weights: trapezoidal rule
          wmu = (/ 0.0125, 0.0200, 0.0250, 0.0250, &
               &   0.0250, 0.0250, 0.0375, 0.0500, &
               &   0.0500, 0.0750, 0.1000, 0.1000, &
               &   0.1000, 0.1000, 0.1000, 0.1000, &
               &   0.0550 /)
          n = (lmu-1) * nphi + 1
       end if

       ! print mu values and weights
       if (master) then
          print '(X,A11,X,A3,X,A10,X,A8,X,A10)', &
               trim(Id)//':', 'imu', 'xmu', 'theta','weight'
          do imu=1,lmu
             ! theta = acos(mu), units: degrees
             theta = acos(xmu(imu))*180./pi
             print '(X,11X,X,I3,X,F10.3,X,F8.1,X,F10.3)', &
                  imu, xmu(imu), theta, wmu(imu)
          end do
       end if

       ! allocate angles (derived data type)
       call angles%alloc(n)

       ! set directions: mu and phi values
       ! loop over mu values
       i = 1
       do imu=1,lmu
          ! how many phi values ?
          if (abs(xmu(imu)-1.0).le.eps) then
             lphi = 1
             wphi = 1.0
          else
             lphi = nphi
             wphi = 1.0 / real(nphi)
          end if
          ! loop over phi angles
          do iphi=1,lphi
             ! assign mu, phi and weight to ray
             angles%mu(i) = xmu(imu)
             angles%weight(i) = wmu(imu) * wphi
             angles%phi(i) = (iphi-1)*2.*pi / real(lphi)
             i=i+1
          end do
       end do

    case('vertical')
       ! TO DO: check why weight is set to 0.5
       ! allocate angles
       n = 1
       call angles%alloc(n)
       angles%mu(1) = 1.0
       angles%phi(1) = 0.0
       angles%weight(1) = 0.5

    case('carlson')
       ! Carlson A quadrature

       ! ncar = number of directions per quadrant
       call carlson(lmu, mcar, ncar, xmu, imux, imuy, imuz, wcar)

       ! print directional cosines and weights
       if (master) then
          print '(X,A)', trim(Id)//': Carlson A quadrature:'
          print '(X,1(X,A))', 'Directional cosines and weights:'
          print '(X,1(X,A3),X,3(X,A11),X,1(X,A11))', &
               'i','mux','muy','muz','weight'
          do i=1,ncar
             print '(X,1(X,I3),X,3(X,F11.8),X,1(X,F11.8))', &
                  i, &
                  xmu(imux(i)), &
                  xmu(imuy(i)), &
                  xmu(imuz(i)), &
                  wcar(i)
          end do
       end if

       ! number of quadrants = 4
       lphi = 4
       if (nphi.ne.4) then
          if (master) then
             print '(X,A)', &
                  trim(Id)//': Carlson A quadrature: set nphi = 4'
          end if
          nphi = 4
       end if
       ! weight per quadrant
       wphi = 0.25

       ! allocate angles
       n = ncar * lphi
       call angles%alloc(n)

       ! set directions: mu and phi values
       i = 1
       do icar=1,ncar
          do iphi=1,lphi
             angles%mu(i) = xmu(imuz(i))
             angles%phi(i) = acos(xmu(imux(i))) + (iphi-1)*2.*pi / real(lphi)
             angles%weight(i) = wcar(i) * wphi
             i = i + 1
          end do
       end do

    end select

    ! deallocate xmu and wmu arrays
    deallocate(xmu,wmu)
    return

  end subroutine set_angles

end module quad_mod

!...............................................................................
subroutine quad_init
  use params, only: master, mid, hl
  use cooling, only: quad, mmu, nmu, nphi, form_factor, &
       mray, nray, mu_ray, phi_ray, wt_ray
  use quad_mod
  implicit none

  character(len=mid) :: Id = 'quad_init'
  type(quad_angles) :: angles
  integer :: i
  logical :: is_undef, is_check
  logical, save :: first_time = .true.

  ! quadrature angles only calculated the first time the routine is called
  if (first_time) then

     if (master) then
        print '(X,A)', hl
        print '(X,A)', trim(Id)//': initialize'
     end if

     first_time = .false.

     ! initialize nray, mu_ray, phi_ray, wt_ray
     nray = 0
     mu_ray(:) = 0.0
     phi_ray(:)= 0.0
     wt_ray(:) = 0.0

     ! if quadrature undefined, use old convention:
     !  nmu > 0: Gauss (mu=1 excluded); accuracy: 2*nmu order
     !  nmu = 0: only vertical ray with weight 0.5
     !  nmu < 0: Radau (mu=1 included); accuracy: 2*nmu-1 order
     is_undef = (quad.eq.'undefined')
     if (is_undef) then
        if (nmu > 0) then
           quad = 'gauss'
        else if (nmu < 0) then
           quad = 'radau'
        else if (nmu == 0) then
           quad = 'vertical'
        end if
        if (master) then
           print '(X,A)', trim(Id)//': undefined quadrature'
           print '(X,A)', trim(Id)//': set quad = ' // quad
        end if
        is_undef = .false.
     else
        ! check consistency between quad and nmu:
        ! when defined, quad has precedence over nmu
        is_check = .false.
        select case (quad)
        case ('gauss')
           if (nmu.le.0) then
              nmu = max(abs(nmu),1)
              is_check = .true.
           end if
        case ('radau')
           if (nmu.ge.0) then
              nmu = -max(abs(nmu),1)
              is_check = .true.
           end if
        case ('vertical')
           if (nmu.ne.0) then
              nmu = 0
              is_check = .true.
           end if
        end select
        if (is_check.and.master) then
           print '(X,A,X,I3)', &
                trim(Id)//': quad = '//trim(quad)//', set nmu = ', nmu
        end if
     end if

     ! legacy code: form factor for vertical case
     ! TO DO: check why form_factor is set to 0.4
     if (quad.eq.'vertical') then
        if (form_factor == 1.0) form_factor = 0.4
     end if

     ! set angles (directions)
     call angles%set(quad, nmu, nphi)

     ! copy mu and phi values and weights to _ray arrays
     nray = angles%n
     do i=1,nray
        mu_ray(i) = angles%mu(i)
        phi_ray(i)= angles%phi(i)
        wt_ray(i) = angles%weight(i)
     end do

     if (master) then
        print '(X,A)', trim(Id)//': ray directions and weights'
        print '(2X,A4,2(X,A9),X,1(X,A11))','i','mu','phi','weight'
        do i=1,nray
           print '(2X,I4,2(X,F9.6),X,1(X,F11.8))', &
                i,mu_ray(i),phi_ray(i),wt_ray(i)
        end do
     end if

     !! deallocate angles
     !call angles%dealloc

  end if ! first_time

  return

end subroutine quad_init

!...............................................................................
subroutine limb_init
  use params, only: hl, master, mid, mpi_rank, mpi_size
  use cooling, only: quad_limb, tabfile_limb, limbtab, nlam_limb, &
       nmul, nphil, nlimb, mu_limb, phi_limb, wt_limb
  use table
  use eos, only: radtab, radtabfile, rad_indxs
  use quad_mod
  implicit none

  character(len=mid) :: Id = 'limb_init'
  type(quad_angles) :: limb
  integer :: i
  integer :: rank
  real, dimension(1) :: mu_add, phi_add, wt_add
  real, parameter :: eps = 1.0e-6
  logical :: is_not_vertical, is_not_last
  logical :: is_check
  logical, save :: first_time = .true.


  if (first_time) then

     if (master) then
        print '(X,A)', hl
        print '(X,A)', trim(Id)//': initialize'
     end if

     first_time = .false.

     ! initialize mu_ray, phi_ray, wt_ray
     nlimb = 0
     mu_limb(:) = 0.0
     phi_limb(:)= 0.0
     wt_limb(:) = 0.0

     ! limb darkening opacity table:
     ! check: use same opacity binning table as cooling module?
     is_check = tabfile_limb.eq.radtabfile
     if (is_check) then
        limbtab = radtab
     else
        ! load table
        if (master) then
           print '(X,A)',trim(Id)//': read limb darkening opacity table'
        end if
        do rank=0,mpi_size-1
           call barrier_mpi('limb_init')
           if (rank.eq.mpi_rank) then
              call limbtab%init(tabfile_limb)
           end if
        end do
        ! convert table to simulation units
        call limbtab%convert
     end if !is_check

     ! number of opacity bins / wavelengths
     nlam_limb = limbtab%nlam
     if (master) then
        print '(X,A,X,I6)', &
             trim(Id)//': limb darkening opacity table, nlam =', nlam_limb
     end if

     ! set limb darkening angles
     ! note: 'ld' quadrature has fixed set of 17 mu angles plus vertical
     if (quad_limb.eq.'ld'.and.nmul.ne.17) then
        !fixed set of 17 angles (including vertical) for 'ld' case
        nmul = 17
        if (master) then
           print '(X,A,X,I2)',trim(Id)//': set nmul =', nmul
        end if
     end if
     call limb%set(quad_limb, nmul, nphil)


     ! check if vertical direction is missing
     is_not_vertical = .true.
     is_not_last = .true.
     i = 1
     do while (is_not_vertical.and.is_not_last)
        if (abs(limb%mu(i)-1.0).le.eps) is_not_vertical = .false.
        if (i.eq.limb%n) is_not_last = .false.
        i = i + 1
     end do
     ! if vertical direction is missing, then add it
     ! note: zero weight for vertical (TO DO: check whether to improve this) 
     if (is_not_vertical) then
        mu_add(1) = 1.0
        phi_add(1)= 0.0
        wt_add(1) = 0.0
        call limb%add(1, mu_add, phi_add, wt_add)
        is_not_vertical = .false.
     end if

     ! copy mu and phi values, weights to _limb arrays
     nlimb = limb%n
     do i=1,nlimb
        mu_limb(i) = limb%mu(i)
        phi_limb(i)= limb%phi(i)
        wt_limb(i) = limb%weight(i)
     end do

     if (master) then
        print '(X,A)', trim(Id)//': limb darkening directions'
        print '(2X,A4,2(X,A9))','i','mu','phi'
        do i=1,nlimb
           print '(2X,I4,2(X,F9.6))',i,mu_limb(i),phi_limb(i)
        end do
     end if

     !! deallocate limb darkening angles
     !call limb%dealloc

  end if !first_time

  return

end subroutine limb_init

!...............................................................................
