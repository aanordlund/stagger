!................................................................................
module table

  use params, only: mfile, mid, master

  implicit none

  ! derived types: declaration
  type, public :: basic_tab
     character(len=mfile) :: filename
     character(len=8) :: date
     character(len=8) :: time
     integer :: nx, ny
     integer :: ma, na
     integer :: nvar, nsub
     integer :: nelem
     integer, dimension(20) :: nxopt, nxrev
     real :: lnx_min, lnx_max
     real :: lny_min, lny_max
     real :: dlnx, dlny
     real :: xmin_lim, xmax_lim
     real :: ymin_lim, ymax_lim
     character(len=4), allocatable, dimension(:) :: iel
     real, allocatable, dimension(:) :: abund
     real, allocatable, dimension(:,:) :: aux
     real, allocatable, dimension(:,:,:,:) :: tab
     real :: u_r, u_l, u_t
     logical :: do_limit = .false.
   contains
     procedure :: init => init_tab
     procedure :: convert
     procedure :: lookup
     procedure :: limit
  end type basic_tab

  type, extends(basic_tab), public :: eos_tab
     logical :: do_cubic = .false.
  end type eos_tab

  type, extends(basic_tab), public :: rad_tab
     integer :: nlam
     logical :: flag_scat = .false.
  end type rad_tab

  ! tab_indxs: derived type containing table indexes and interpolation
  ! weights for lookup procedure
  type, public :: tab_indx
     logical :: do_indx = .false.
     integer :: nx, ny, nz
     integer, allocatable, dimension(:,:,:) :: ix, iy
     real,    allocatable, dimension(:,:,:) :: px, py
   contains
     procedure :: set => set_indx
     procedure :: unset => unset_indx
     procedure :: check => check_indx
  end type tab_indx


  ! module variables, declarations:

  ! basic simulation units (cgs):
  ! - length, time, density
  real :: uur, uul, uut

  ! derived simulation units (cgs):
  ! - velocity, energy/mass, pressure)
  ! - opacity, extinction coeff., source function times wavelength
  real :: uuv, uuee, uup
  real :: uuk, uua, uubp

  ! EOS table, limits on density and energy/mass:
  real :: rmin_lim_eos
  real :: rmax_lim_eos
  real :: eemin_lim_eos
  real :: eemax_lim_eos

  ! rad table, limits on density and temperature:
  real :: rmin_lim_rad
  real :: rmax_lim_rad
  real :: ttmin_lim_rad
  real :: ttmax_lim_rad

  ! logical switches
  logical :: do_cubic_eos
  logical :: do_limit_eos
  logical :: do_limit_rad
  logical :: flag_scat

contains

  !..............................................................................
  subroutine init_tab(table, tabfile)

    implicit none

    character(len=mid) :: Id = 'init_tab'
    class(basic_tab) :: table
    character(len=mfile) :: tabfile
    character(len=8) :: date
    character(len=8) :: time
    integer :: nx, ny
    integer :: ma, na
    integer :: nvar
    integer, parameter :: nsub = 3
    integer :: nlam
    integer :: nelem
    integer, dimension(20) :: nxopt, nxrev
    real :: lnx_min, lnx_max
    real :: lny_min, lny_max
    real :: xmin_lim, xmax_lim
    real :: ymin_lim, ymax_lim
    character(len=4), allocatable, dimension(:) :: iel
    real, allocatable, dimension(:) :: abund
    real, allocatable, dimension(:,:) :: aux
    real, allocatable, dimension(:,:,:,:) :: tab
    real :: u_r, u_l, u_t
    real :: u_v, u_ee
    real :: ln_u_rat
    integer :: lun
    logical :: is_open
    integer :: i

    ! assign free unit to file
    lun = 12
    inquire(unit=lun,opened=is_open)
    do while(is_open)
       lun = lun + 1
       inquire(unit=lun,opened=is_open)
    end do

    ! open and read file
    table%filename = trim(tabfile)
    open (lun, file=trim(tabfile), form='unformatted', status='old')
    read (lun) nx, ny, ma, na, nvar, nelem, nxopt, nxrev

    if (nx.gt.1.and.ny.gt.1.and.nvar.ge.1) then
       allocate( iel(nelem) )
       allocate( abund(nelem) )
       allocate( aux(ma,na) )
       read (lun) &
            date, &
            time, &
            lny_min, &
            lny_max, &
            lnx_min, &
            lnx_max, &
            u_l, &
            u_r, &
            u_t, &
            iel, &
            abund, &
            aux
       ! derived units
       u_v  = u_l/u_t
       u_ee = u_v * u_v

       ! type-dependent part
       select type (table)
       type is (basic_tab)
          table%do_limit = .false.
          class is (eos_tab)
          table%do_cubic = do_cubic_eos
          table%do_limit = do_limit_eos
          allocate( tab(nx,ny,nsub,nvar) )
          read (lun) tab
          ! external limits on x and y: update, convert to table units
          ! y = density
          ln_u_rat = log(uur/u_r)
          ymin_lim = exp(max(log(rmin_lim_eos)+ln_u_rat, lny_min))
          ymax_lim = exp(min(log(rmax_lim_eos)+ln_u_rat, lny_max))
          ! x = energy/mass
          ln_u_rat = log(uuee/u_ee)
          xmin_lim = exp(max(log(eemin_lim_eos)+ln_u_rat, lnx_min))
          xmax_lim = exp(min(log(eemax_lim_eos)+ln_u_rat, lnx_max))
          class is (rad_tab)
          table%flag_scat = flag_scat
          table%do_limit = do_limit_rad
          ! number of opacity bins
          nlam = nvar
          table%nlam = nlam
          ! update number of variables in table (nlam+1)
          nvar = nvar + 1
          allocate( tab(nx,ny,nsub,nvar) )
          do i=1,nvar
             read (lun) tab(:,:,:,i)
          end do
          ! external limits on x and y: update, convert to table units
          ! y = density
          ln_u_rat = log(uur/u_r)
          ymin_lim = exp(max(log(rmin_lim_rad)+ln_u_rat, lny_min))
          ymax_lim = exp(min(log(rmax_lim_rad)+ln_u_rat, lny_max))
          ! x = temperature
          ln_u_rat = 0.
          xmin_lim = exp(max(log(ttmin_lim_rad)+ln_u_rat, lnx_min))
          xmax_lim = exp(min(log(ttmax_lim_rad)+ln_u_rat, lnx_max))
          class default
             ! unexpected/unsupported type
          if (master) print '(X,A)', trim(Id)//': unexpected type for table object!'
          call abort_mpi
          stop
       end select

       ! close table file
       close(lun)

       ! fill table components
       table%nx = nx
       table%ny = ny
       table%ma = ma
       table%na = na
       table%nvar = nvar
       table%nsub = 3
       table%nelem = nelem
       table%nxopt = nxopt
       table%nxrev = nxrev
       table%date = date
       table%time = time
       table%lny_min = lny_min
       table%lny_max = lny_max
       table%lnx_min = lnx_min
       table%lnx_max = lnx_max
       table%dlny = (lny_max - lny_min) / real(ny-1)
       table%dlnx = (lnx_max - lnx_min) / real(nx-1)
       table%xmin_lim = xmin_lim
       table%xmax_lim = xmax_lim
       table%ymin_lim = ymin_lim
       table%ymax_lim = ymax_lim
       table%u_l = u_l
       table%u_r = u_r
       table%u_t = u_t
       allocate( table%iel(nelem) )
       allocate( table%abund(nelem) )
       allocate( table%aux(ma,na) )
       allocate( table%tab(nx,ny,nsub,nvar) )
       table%aux = aux
       table%iel = iel
       table%abund = abund
       table%tab = tab
    else
       if (master) print '(X,A)', trim(Id)//': table dimensions are too small!'
       call abort_mpi
       stop
    end if

    deallocate( iel, abund, aux, tab )

  end subroutine init_tab

  !..............................................................................
  subroutine convert(table)
    ! convert table values from table to simulation units
    implicit none

    character(len=mid) :: Id = 'convert'
    class(basic_tab) :: table
    real :: u_r, u_l, u_t
    real :: u_v, u_ee, u_p, u_a, u_k, u_bp
    real :: u_rat, ln_u_rat
    integer :: nvar, nlam
    integer :: ivar, ilam, isub

    ! number of main variables in table
    nvar = table%nvar

    ! table units, basic
    u_r = table%u_r
    u_l = table%u_l
    u_t = table%u_t

    ! table units, derived
    u_v  = u_l/u_t
    u_ee = u_v * u_v
    u_p  = u_r * u_ee
    u_a  = 1. / u_l
    u_k  = u_a/u_r
    u_bp = u_p * u_v

    select type (table)
    type is (basic_tab)
       ! note: nothing to be done
       class is (eos_tab)
          ! EOS table:
          ! x = energy/mass
          ! note: nothing to be done for dlnx: independent from units!
       u_rat = u_ee/uuee
       ln_u_rat = alog(u_rat)
       table%lnx_min = table%lnx_min + ln_u_rat
       table%lnx_max = table%lnx_max + ln_u_rat
       ! table limits, x:
       table%xmin_lim = table%xmin_lim * u_rat
       table%xmax_lim = table%xmax_lim * u_rat
       ! y = density
       ! note: nothing to be done for dlny: independent from units!
       u_rat = u_r/uur
       ln_u_rat = alog(u_rat)
       table%lny_min = table%lny_min + ln_u_rat
       table%lny_max = table%lny_max + ln_u_rat
       ! table limits, y:
       table%ymin_lim = table%ymin_lim * u_rat
       table%ymax_lim = table%ymax_lim * u_rat
       !
       ! table variables (ln of):
       ! ivar = 1: pressure (gas+rad)
       ! ivar = 2: extinction coeff., Rosseland mean
       ! ivar = 3: temperature
       ! ivar = 4: electron number density
       ! ivar = 5: extinction coeff., Planck mean
       ! ivar = 6: extinction coeff. at 500 nm
       !
       ! note: older versions of EOS table: no electron num. density!
       !
       ! isub = 1: variable
       ! isub = 2: d ln variable / d ln energy/mass * dlnx
       ! isub = 3: d ln variable / d ln density     * dlny
       !
       table%tab(:,:,1,1) = table%tab(:,:,1,1) + alog(u_p/uup)
       table%tab(:,:,1,2) = table%tab(:,:,1,2) + alog(u_a/uua)
       select case(nvar)
       case(6) !current version of tables
          table%tab(:,:,1,4) = table%tab(:,:,1,4) + alog(uul/u_l)*3
          table%tab(:,:,1,5:6) = table%tab(:,:,1,5:6) + alog(u_a/uua)
       case(5) !older version
          table%tab(:,:,1,4:5) = table%tab(:,:,1,4:5) + alog(u_a/uua)
       case default
          if (master) print '(X,A)', trim(Id)//': unsupported table format!'
          call abort_mpi
          stop
       end select
       class is (rad_tab)
          ! rad table:
          ! x = temperature
          ! note: unit ratio = 1, nothing to be done here
          !
          ! y = density
          ! note: nothing to be done for dlny: independent from units!
       u_rat = u_r/uur
       ln_u_rat = alog(u_rat)
       table%lny_min = table%lny_min + ln_u_rat
       table%lny_max = table%lny_max + ln_u_rat
       ! table limits, y:
       table%ymin_lim = table%ymin_lim * u_rat
       table%ymax_lim = table%ymax_lim * u_rat
       !
       ! table variables (ln of):
       ! - ivar = 1,nlam:
       !   - isub=1: ratio bin/Rosseland opacities
       !   - isub=2: bin-integrated thermal emission (epsilon*B*w_lambda)
       !   - isub=3: bin destruction probability (eps)
       ! note: nothing to be done for isub = 1 and 3
       nlam = table%nlam
       ln_u_rat = alog(u_bp/uubp)
       do ilam=1,nlam
          table%tab(:,:,2,ilam) = table%tab(:,:,2,ilam) + ln_u_rat
       end do
       ! table variables (ln of):
       ! - ivar = nlam + 1
       !   - isub=1: extinction coeff. ([1/length]), Rosseland mean
       !   - isub=2: extinction coeff., Planck mean
       !   - isub=3: extinction coeff. at 500 nm
       ln_u_rat = alog(u_a/uua)
       table%tab(:,:,:,nlam+1) = table%tab(:,:,:,nlam+1) + ln_u_rat
    end select

  end subroutine convert

  !..............................................................................
  subroutine limit(table, x, y, do_indx)
    ! force x and y to within table limits
    implicit none

    character(len=mid) :: Id = 'limit'
    class(basic_tab) :: table
    real, dimension(:,:,:), intent(inout) :: x, y
    logical, intent(in) :: do_indx

    integer :: dim(3)
    integer :: i, j, k
    real :: xmin, xmax
    real :: ymin, ymax
    real :: xmin_lim, xmax_lim
    real :: ymin_lim, ymax_lim
    logical :: is_check
    logical :: is_outside
    logical :: do_limit


    ! dimensions of x array:
    dim(1) = size(x,dim=1)
    dim(2) = size(x,dim=2)
    dim(3) = size(x,dim=3)

    ! check dimensions of x and y:
    is_check = &
         dim(1).eq.size(y,dim=1).and. &
         dim(2).eq.size(y,dim=2).and. &
         dim(3).eq.size(y,dim=3)

    if (is_check) then

       ! if x and y array dimensions match,
       ! check if indexes need to be recalculated (do_indx)
       if (do_indx) then

          ! do_limit switch: force x and y to within table boundaries
          do_limit = table%do_limit

          ! table limits
          xmin_lim = table%xmin_lim
          xmax_lim = table%xmax_lim
          ymin_lim = table%ymin_lim
          ymax_lim = table%ymax_lim

          ! x and y arrays, min and max
          xmin = minval(x(:,:,:))
          xmax = maxval(x(:,:,:))
          ymin = minval(y(:,:,:))
          ymax = maxval(y(:,:,:))

          ! check if outside table boundaries
          is_outside = &
               xmin.lt.xmin_lim.or. &
               xmax.gt.xmax_lim.or. &
               ymin.lt.ymin_lim.or. &
               ymax.gt.ymax_lim

          if (is_outside) then
             if (master) then
                print '(X,A)', trim(Id)//': table file = '//trim(table%filename)
                print '(X,A)', trim(Id)//': x and/or y outside table'
             end if
             if (do_limit) then
                if (master) print '(X,A)', trim(Id)//': enforce limits'
                ! force x and y to within table limits
                do k=1,dim(3)
                   do j=1,dim(2)
                      do i=1,dim(1)
                         ! apply limits here
                         x(i,j,k) = min(max(x(i,j,k),xmin_lim),xmax_lim)
                         y(i,j,k) = min(max(y(i,j,k),ymin_lim),ymax_lim)
                      end do
                   end do
                end do
             else
                if (master) print '(X,A)', &
                     trim(Id)//': no enforced limits, outside table boundaries!'
             end if !do_limit
          end if !is_outside

       end if !do_indx

    else

       if (master) then
          print '(X,A)', trim(Id)//': table file = '//trim(table%filename)
          print '(X,A)', trim(id)//': dimensions of x and y arguments do not match!'
       end if
       call abort_mpi
       stop

    end if !is_check

  end subroutine limit

  !..............................................................................
  subroutine lookup(table, dim, x, y, var, ivar, isub, do_exp, indexes)
    implicit none

    character(len=mid) :: Id = 'lookup'
    class(basic_tab), intent(in) :: table
    integer, intent(in) :: dim(3)
    real, dimension(dim(1),dim(2),dim(3)), intent(inout) :: x, y
    real, dimension(dim(1),dim(2),dim(3)), intent(out)   :: var
    integer, intent(in) :: ivar, isub
    logical, intent(in) :: do_exp
    class(tab_indx), optional, target :: indexes

    integer :: i, j, k
    integer :: ix, iy
    real :: xi, yi
    real :: px, py, qx, qy
    real :: pxqx, pyqy, dfx1, dfx2, dfy1, dfy2
    logical :: is_indexes
    logical :: is_check
    logical :: do_indx
    logical :: do_linear
    logical :: do_deriv

    ! pointers to arrays with table indexes and interpolation weights
    integer,dimension(:,:,:), pointer :: ix_arr, iy_arr
    real,   dimension(:,:,:), pointer :: px_arr, py_arr
    ! tab_indx object to be used in case optional indexes argument is not passed
    type(tab_indx), target :: tmp_indx


    ! check if optional indexes argument has been passed
    is_indexes = present(indexes)  
    if (is_indexes) then
       ! indexes present:
       ! check if indexes arrays are allocated and have dimensions dim
       is_check = indexes%check(dim)
       if (is_check) then
          ! indexes arrays already allocated
          ! assign bundled do_indx value to local do_indx
          do_indx = indexes%do_indx
       else
          ! indexes not passed or not allocated: reset
          do_indx = .true.
          call indexes%set(dim, do_indx)
       end if
       ! pointers to indexes arrays
       ix_arr => indexes%ix
       iy_arr => indexes%iy
       px_arr => indexes%px
       py_arr => indexes%py
    else
       ! indexes not present: allocate temporary object
       ! set do_indx = t (i.e. force recalculating indexes)
       do_indx = .true.
       call tmp_indx%set(dim, do_indx)
       ! pointers to temporary object arrays
       ix_arr => tmp_indx%ix
       iy_arr => tmp_indx%iy
       px_arr => tmp_indx%px
       py_arr => tmp_indx%py
    end if

    ! check if x and y are within table boundaries, apply limits
    call table%limit(x, y, do_indx)

    ! calculate indexes and weights
    ! note: x and y linear arrays, table independent variables in log
    if (do_indx) then
       do k=1,dim(3)
          do j=1,dim(2)
             do i=1,dim(1)
                xi = (log(x(i,j,k))-table%lnx_min)/table%dlnx + 1.0
                ix = max0(1,min0(table%nx-1,int(xi)))
                px_arr(i,j,k) = xi-ix
                ix_arr(i,j,k) = ix

                yi = (log(y(i,j,k))-table%lny_min)/table%dlny + 1.0
                iy = max0(1,min0(table%ny-1,int(yi)))
                py_arr(i,j,k) = yi-iy
                iy_arr(i,j,k) = iy
             end do
          end do
       end do
       do_indx = .false.
    end if


    ! do_linear: linear table lookup interpolation?
    ! do_deriv: calculate derivative of variable (for EOS table only)
    select type (table)
    type is (basic_tab)
       do_linear = .true.
       do_deriv  = .false.
       class is (eos_tab)
       do_deriv  = (isub.gt.1)
       do_linear = .not.table%do_cubic
       class is (rad_tab)
       do_linear = .true.
       do_deriv  = .false.
       class default
       if (master) print '(X,A)', trim(Id)//': unsupported table format!'
       call abort_mpi
       stop
    end select


    ! main table lookup
    if (do_linear) then
       ! linear interpolation
       do k=1,dim(3)
          do j=1,dim(2)
             do i=1,dim(1)
                ix = ix_arr(i,j,k)
                iy = iy_arr(i,j,k)
                px = px_arr(i,j,k)
                py = py_arr(i,j,k)
                qx = 1. - px
                qy = 1. - py
                var(i,j,k) = &
                     & qy * ( qx * table%tab(ix,  iy,  isub,ivar)   + &
                     &        px * table%tab(ix+1,iy,  isub,ivar) ) + &
                     & py * ( qx * table%tab(ix,  iy+1,isub,ivar)   + &
                     &        px * table%tab(ix+1,iy+1,isub,ivar) )
             end do
          end do
       end do
    else
       ! cubic interpolation
       if (do_deriv) then
          ! interpolate derivatives
          ! note: linear, for now
          do k=1,dim(3)
             do j=1,dim(2)
                do i=1,dim(1)
                   ix = ix_arr(i,j,k)
                   iy = iy_arr(i,j,k)
                   px = px_arr(i,j,k)
                   py = py_arr(i,j,k)
                   qx = 1. - px
                   qy = 1. - py
                   var(i,j,k) = &
                        & qy * ( qx * table%tab(ix,  iy,  isub,ivar)   + &
                        &        px * table%tab(ix+1,iy,  isub,ivar) ) + &
                        & py * ( qx * table%tab(ix,  iy+1,isub,ivar)   + &
                        &        px * table%tab(ix+1,iy+1,isub,ivar) )
                end do
             end do
          end do
       else
          ! interpolate variable
          do k=1,dim(3)
             do j=1,dim(2)
                do i=1,dim(1)
                   ix = ix_arr(i,j,k)
                   iy = iy_arr(i,j,k)
                   px = px_arr(i,j,k)
                   py = py_arr(i,j,k)
                   qx = 1. - px
                   qy = 1. - py
                   pxqx = px*qx
                   pyqy = py*qy

                   dfx1 = table%tab(ix+1,iy  ,1,ivar)-table%tab(ix  ,iy  ,1,ivar)
                   dfx2 = table%tab(ix+1,iy+1,1,ivar)-table%tab(ix  ,iy+1,1,ivar)
                   dfy1 = table%tab(ix  ,iy+1,1,ivar)-table%tab(ix  ,iy  ,1,ivar)
                   dfy2 = table%tab(ix+1,iy+1,1,ivar)-table%tab(ix+1,iy  ,1,ivar)

                   var(i,j,k) = qx*(                                            &
                        &         qy*(      table%tab(ix  ,iy  ,1,ivar)         &
                        &           + pyqy*(table%tab(ix  ,iy  ,3,ivar)-dfy1)   &
                        &           + pxqx*(table%tab(ix  ,iy  ,2,ivar)-dfx1))  &
                        &       + py*(      table%tab(ix  ,iy+1,1,ivar)         &
                        &          - pyqy*(table%tab(ix  ,iy+1,3,ivar)-dfy1)    &
                        &          + pxqx*(table%tab(ix  ,iy+1,2,ivar)-dfx2))   &
                        &                                                    )  &
                        &     + px*(                                            &
                        &         py*(      table%tab(ix+1,iy+1,1,ivar)         &
                        &           - pyqy*(table%tab(ix+1,iy+1,3,ivar)-dfy2)   &
                        &           - pxqx*(table%tab(ix+1,iy+1,2,ivar)-dfx2))  &
                        &       + qy*(      table%tab(ix+1,iy  ,1,ivar)         &
                        &           + pyqy*(table%tab(ix+1,iy  ,3,ivar)-dfy2)   &
                        &           - pxqx*(table%tab(ix+1,iy  ,2,ivar)-dfx1))  &
                        &                                                     )

                end do
             end do
          end do
       end if !do_deriv
    end if !do_linear

    ! derivatives (EOS table only)
    if (do_deriv) then
       ! The values of x- and y- derivatives stored in table have been multiplied
       ! by dlnx and dlny, respectively: normalise before outputting
       if (isub.eq.2) then 
          var(:,:,:) = var(:,:,:) / table%dlnx
       end if
       if (isub.eq.3) then 
          var(:,:,:) = var(:,:,:) / table%dlny
       end if
    end if

    ! exponentiate?
    if (do_exp) then
       if (do_deriv) then
          ! to be implemented
          if (master) print '(X,A)', trim(Id)//': not yet implemented'
       else
          do k=1,dim(3)
             var(:,:,k) = exp(var(:,:,k))
          end do
       end if
    end if

    ! before returning, nullify pointers
    nullify(ix_arr)
    nullify(iy_arr)
    nullify(px_arr)
    nullify(py_arr)

    ! before returning, set do_indx = f (no need to recompute indexes
    ! and weights next call, if x and y do not change) or,
    ! if indexes was not passed, deallocate tmp_indx
    if (is_indexes) then
       indexes%do_indx = .false.
    else
       call tmp_indx%unset()
    end if

    return

  end subroutine lookup

  !..............................................................................
  subroutine set_indx(indexes, dim, do_indx)
    ! (re)allocate indexes arrays
    implicit none

    character(len=mid) :: Id = 'set_indx'
    class(tab_indx) :: indexes
    integer, intent(in) :: dim(3)
    logical, intent(in), optional :: do_indx
    logical :: is_alloc

    ! default: set do_indx = t
    if (present(do_indx)) then
       indexes%do_indx = do_indx
    else
       indexes%do_indx = .true.
    end if

    ! set dimensions of arrays
    indexes%nx = dim(1)
    indexes%ny = dim(2)
    indexes%nz = dim(3)

    ! (re)allocate arrays
    is_alloc = allocated(indexes%ix)
    if (is_alloc) then
       deallocate(indexes%ix)
       deallocate(indexes%iy)
       deallocate(indexes%px)
       deallocate(indexes%py)
    end if
    allocate( indexes%ix(dim(1),dim(2),dim(3)) )
    allocate( indexes%iy(dim(1),dim(2),dim(3)) )
    allocate( indexes%px(dim(1),dim(2),dim(3)) )
    allocate( indexes%py(dim(1),dim(2),dim(3)) )

  end subroutine set_indx

  !..............................................................................
  function check_indx(indexes, dim)
    ! check if indexes array are allocated and have dimensions dim
    implicit none

    character(len=mid) :: Id = 'check_indx'
    class(tab_indx) :: indexes
    integer, intent(in) :: dim(3)
    logical :: check_indx
    logical :: is_alloc

    ! check if indexes array components allocated
    is_alloc = &
         allocated(indexes%ix).and.  &
         allocated(indexes%iy).and.  &
         allocated(indexes%px).and.  &
         allocated(indexes%py)

    if (is_alloc) then
       ! check dimensions of alocated arrays: are they compatible with dim ?
       check_indx = &
            size(indexes%ix,dim=1).eq.dim(1).and.&
            size(indexes%ix,dim=2).eq.dim(2).and.&
            size(indexes%ix,dim=3).eq.dim(3)

       check_indx = check_indx.and. &
            size(indexes%iy,dim=1).eq.dim(1).and.&
            size(indexes%iy,dim=2).eq.dim(2).and.&
            size(indexes%iy,dim=3).eq.dim(3)

       check_indx = check_indx.and. &
            size(indexes%px,dim=1).eq.dim(1).and.&
            size(indexes%px,dim=2).eq.dim(2).and.&
            size(indexes%px,dim=3).eq.dim(3)

       check_indx = check_indx.and. &
            size(indexes%py,dim=1).eq.dim(1).and.&
            size(indexes%py,dim=2).eq.dim(2).and.&
            size(indexes%py,dim=3).eq.dim(3)
    else
       check_indx = .false.
    end if

  end function check_indx

  !..............................................................................
  subroutine unset_indx(indexes)
    ! deallocate and reset indexes arrays and components
    implicit none

    character(len=mid) :: Id = 'unset_indx'
    class(tab_indx) :: indexes
    logical :: is_alloc

    ! deallocate arrays
    is_alloc = allocated(indexes%ix)
    if (is_alloc) then
       deallocate(indexes%ix)
       deallocate(indexes%iy)
       deallocate(indexes%px)
       deallocate(indexes%py)
    end if

    ! set dimensions of arrays to zero
    indexes%nx = 0
    indexes%ny = 0
    indexes%nz = 0

    ! set do_indx switch to false
    indexes%do_indx = .false.

  end subroutine unset_indx

end module table

!..............................................................................
module eos

  use params, only: mfile, mid
  use table
  implicit none

  ! logical switches:
  logical :: do_eos
  logical :: do_table
  logical :: verbose_tab

  ! EOS and rad table file names:
  character(len=mfile) :: eostabfile
  character(len=mfile) :: radtabfile

  ! EOS and rad tables:
  type(eos_tab) :: eostab
  type(rad_tab) :: radtab

  ! EOS and rad indexes:
  type(tab_indx) :: eos_indxs
  type(tab_indx) :: rad_indxs

  ! Constants: Boltzmann constant (cgs)
  real, parameter :: kboltz = 1.3806504e-16
  real, parameter :: lnkboltz = log(kboltz)

  ! allocatable arrays for pressure and derivatives at bottom boundary
  real, allocatable, dimension(:,:) :: pbot0
  real, allocatable, dimension(:,:) :: dlnpdE_r
  real, allocatable, dimension(:,:) :: dlnpdlnr_E

end module eos

!..............................................................................
subroutine init_eos

  use params, only: hl, mx, my, mz, mid, master, mpi_size, mpi_rank
  use eos
  implicit none

  character(len=mid) :: Id = 'init_eos'
  ! String arrays for printout (logging)
  character(len=16) :: jtype
  character(len=8)  :: linedat
  integer :: i
  integer :: rank
  character(len=32), dimension (10) :: sopctype = (/ ('', i=1,10) /)
  character(len=32), dimension (4)  :: ssrctype = (/ ('', i=1, 4) /)
  character(len=32), dimension (4)  :: stautype = (/ ('', i=1, 4) /)
  character(len=32), dimension (4)  :: sxtrtype = (/ ('', i=1, 4) /)
  ! rad table: logical switches for table variant
  logical :: flag_vanreg
  logical :: flag_bins
  logical :: flag_odf

  if (master) then
     print '(X,A)', hl
     print '(X,A)', trim(Id)//': initialize'
  end if

  ! namelist: initialize variables and read from input file
  call read_eos

  ! read EOS and rad tables
  if (master) print '(X,A)',trim(Id)//': read EOS and rad tables'
  do rank=0,mpi_size-1
     call barrier_mpi('init_eos')
     if (rank.eq.mpi_rank) then
        call eostab%init(eostabfile)
        call radtab%init(radtabfile)
     end if
  end do

  ! convert table to simulation units
  call eostab%convert
  call radtab%convert

  ! EOS and rad tables: print out basic information:
  !   flag_scat: consistent treatment of scattering (t) ?
  !   flag_bins: bins (t) or wavelengths (f) ?
  !   flag_odf:  ODFs (t) or opacity sampling (f) ?
  !   flag_vanreg: van Regemerter approximation for line scattering (t) ?

  ! flag_scat (note: declared in table module)
  select case(radtab%nxopt(2))
  case(1)
     flag_scat = .false.
  case(2)
     flag_scat = .true.
  case default
     if (master) print '(X,A)', trim(Id)//': unsupported source function type!'
     call abort_mpi
     stop
  end select

  flag_odf    = radtab%nxrev(4).lt.70
  flag_bins   = radtab%nxopt(1).lt.6
  flag_vanreg = radtab%nxopt(3).ne.0

  if (flag_odf) then 
     linedat = 'odf.dat'
  else
     linedat = 'OS.dat'
  end if

  if (flag_bins) then 
     jtype = 'bins'
  else
     jtype = 'wavelengths'
  end if

  sopctype(1) = 'Opacity binning, scaled'
  sopctype(2) = 'Opacity binning'
  sopctype(6) = 'Opacity sampling'
  ssrctype(1) = 'Pure Planck function'
  ssrctype(2) = 'Scattering term included'
  stautype(1) = 'monochromatic opacity'
  stautype(2) = 'Rosseland optical depth'
  sxtrtype(1) = 'xcorr'
  sxtrtype(2) = 'Jlambda'
  sxtrtype(3) = 'T-scaled Jlambda'

  if (master) then
     print '(X,A)', hl
     print '(X,A)', trim(Id)//': EOS table basic info:'

     print '(X,2(X,A))',     '(eos) table file =', trim(eostabfile)
     print '(X,3(X,A))',     '(eos) date and time =', &
          trim(eostab%date), &
          trim(eostab%time)

     print '(X,1(X,A),X,I4)','(eos) SVN Rev =', eostab%nxrev(2)

     print '(X,1(X,A),X,I2)','(eos) Number of entries =', eostab%nvar

     print '(X,1(X,A))', &
          '(eos) energy and density ranges (simulation units):'
     print '(X,1(X,A),X,ES13.6)', '(eos)   lnemin =', eostab%lnx_min
     print '(X,1(X,A),X,ES13.6)', '(eos)   lnemax =', eostab%lnx_max
     print '(X,1(X,A),X,ES13.6)', '(eos)   lnrmin =', eostab%lny_min
     print '(X,1(X,A),X,ES13.6)', '(eos)   lnrmax =', eostab%lny_max


     print '(X,A)', hl
     print '(X,A)', trim(Id)//': rad table basic info:'

     print '(X,2(X,A))',     '(rad) table file =', trim(radtabfile)
     print '(X,3(X,A))',     '(rad) date and time =', &
          trim(radtab%date), &
          trim(radtab%time)

     print '(X,1(X,A),X,I4)','(rad) SVN Rev =', radtab%nxrev(2)

     print '(X,2(X,A))',     '(rad) opacity table type =', &
          trim(sopctype(radtab%nxopt(1)))

     print '(X,2(X,A))',     '(rad) source function type =', &
          trim(ssrctype(radtab%nxopt(2)))

     print '(X,3(X,A),X,I6)','(rad) number of', trim(jtype), '=', &
          radtab%nlam

     print '(X,1(X,A))', &
          '(rad) temperature and density ranges (simulation units):'
     print '(X,1(X,A),X,ES13.6)', '(rad)   lntmin =', radtab%lnx_min
     print '(X,1(X,A),X,ES13.6)', '(rad)   lntmax =', radtab%lnx_max
     print '(X,1(X,A),X,ES13.6)', '(rad)   lnrmin =', radtab%lny_min
     print '(X,1(X,A),X,ES13.6)', '(rad)   lnrmax =', radtab%lny_max

     if (flag_bins) then
        print '(X,1(X,A))', '(rad) bin membership according to value of '// &
        !print '(X,5X,2(X,A))', &
             trim(stautype(radtab%nxopt(4))), 'at tau_lambda=1'

        print '(X,2(X,A))',&
             '(rad) type of extrapolation from 1D rad. transf. solution =', &
             trim(sxtrtype(radtab%nxopt(5)))

        print '(X,1(X,A),X,F6.2,X,A)', &
             '(rad) bridging function = exp(', &
             -real(radtab%nxopt(6)) * 0.01, &
             '*tau ).'
     end if

     if (flag_scat) then
        if (flag_vanreg) then
           print '(X,1(X,A),X,F6.2)', &
                '(rad) van Regemorter approximation for line scattering, '// &
                'factor =', &
                real(radtab%nxopt(3)) * 0.01
        else
           print '(X,1(X,A))', &
                '(rad) no van Regemorter approximation for line scattering'
        end if
     end if

     print '(X,1(X,A),X,I4)', '(rad) EOS.tab version =', &
          radtab%nxrev(3)

     print '(X,3(X,A),X,I4)', '(rad)', trim(linedat), &
          'version =', &
          radtab%nxrev(4)

     print '(X,1(X,A),X,I4)', '(rad) subs.dat version =', &
          radtab%nxrev(5)

     !print '(X,A)', trim(hl)
  end if

  ! allocate arrays for pressure at the bottom boundary and derivatives
  allocate(pbot0(mx,mz), dlnpdE_r(mx,mz), dlnpdlnr_E(mx,mz))

end subroutine init_eos

!..............................................................................
subroutine read_eos

  use params, only: do_ionization, stdin, master
  use eos
  implicit none

  character(len=mid) :: Id = 'read_eos'
  namelist /eqofst/ &
       do_eos, &
       do_ionization, &
       do_table, &
       eostabfile, &
       radtabfile, &
       do_cubic_eos, &
       do_limit_eos, &
       do_limit_rad, &
       verbose_tab, &
       uur, &
       uul, &
       uut, &
       rmin_lim_eos, &
       rmax_lim_eos, &
       eemin_lim_eos, &
       eemax_lim_eos, &
       rmin_lim_rad, &
       rmax_lim_rad, &
       ttmin_lim_rad, &
       ttmax_lim_rad

  ! default values:

  ! logical switches
  do_eos = .false.
  do_ionization = .true.
  do_table = .false.

  verbose_tab = .false.

  ! (note: part of table module)
  do_cubic_eos = .false.
  do_limit_eos = .false.
  do_limit_rad = .false.

  ! file names:
  eostabfile = 'EOSrhoe.tab'
  radtabfile = 'kaprhoT.tab'

  ! basic simulation units (cgs units):
  uur = 1.0e-7
  uul = 1.0e8
  uut = 1.0e2

  ! EOS table, limits on density and energy/mass:
  ! (note: simulation units)
  rmin_lim_eos  = 1.00e-10
  rmax_lim_eos  = 1.00e10
  eemin_lim_eos = 1.00e-4
  eemax_lim_eos = 1.00e4

  ! rad table, limits on density and temperature:
  ! (note: simulation units)
  rmin_lim_rad  = 1.0e-10
  rmax_lim_rad  = 1.0e10
  ttmin_lim_rad = 1.0e1
  ttmax_lim_rad = 1.0e7

  ! Read namelist from input file and print it
  rewind (stdin); read (stdin, eqofst)
  if (master) then
     print '(X,A)', trim(Id)//': read namelist:'
     write (*,eqofst)
  end if

  ! derived simulation units (cgs units):
  ! (note: [source function times wavelength] = [density] * [velocity]^3)
  uuv  = uul / uut
  uuee = uuv * uuv
  uup  = uur * uuee
  uua  =  1. / uul
  uuk  = uua / uur
  uubp = uup * uuv

end subroutine read_eos

!................................................................................
subroutine lookup_eos(nx, ny, nz, r, ee, ivar, ider, var, do_indx, do_exp)

  use params, only: mid
  use eos, only: eostab, eos_indxs
  implicit none

  character(len=mid) :: Id = 'lookup_eos'

  integer, intent(in) :: nx, ny, nz
  real, dimension(nx,ny,nz), intent(inout) :: r, ee
  real, dimension(nx,ny,nz), intent(out)   :: var
  integer, intent(in) :: ivar, ider
  !logical, optional, intent(in) :: do_indx
  !logical, optional, intent(in) :: do_exp
  logical, intent(in) :: do_indx
  logical, intent(in) :: do_exp

  integer :: dim(3)

  dim = (/ nx, ny, nz /)

  !! override eos_indxs%do_indx ?
  !if (do_indx) eos_indxs%do_indx = do_indx
  eos_indxs%do_indx = do_indx

  ! look up variable from rad table
  call eostab%lookup(dim, ee, r, var, ivar, ider, do_exp, eos_indxs)

end subroutine lookup_eos

!................................................................................
subroutine lookup_rad(nx, ny, nz, r, tt, isub, ilam, var, do_indx, do_exp)

  use params, only: mid
  use eos, only: radtab, rad_indxs
  implicit none

  character(len=mid) :: Id = 'lookup_rad'

  integer, intent(in) :: nx, ny, nz
  real, dimension(nx,ny,nz), intent(inout) :: r, tt
  real, dimension(nx,ny,nz), intent(out)   :: var
  integer, intent(in) :: isub, ilam
  !logical, optional, intent(in) :: do_indx
  !logical, optional, intent(in) :: do_exp
  logical, intent(in) :: do_indx
  logical, intent(in) :: do_exp

  integer :: dim(3)

  dim = (/ nx, ny, nz /)

  !! override rad_indxs%do_indx ?
  !if (do_indx) rad_indxs%do_indx = do_indx
  rad_indxs%do_indx = do_indx

  ! look up variable from rad table
  call radtab%lookup(dim, tt, r, var, ilam, isub, do_exp, rad_indxs)

end subroutine lookup_rad

!................................................................................
subroutine pressure(r, ee, pp)

  use params, only: mx, my, mz, mid, dbg_eos, ub
  use eos, only: eostab, eos_indxs, pbot0, dlnpdE_r, dlnpdlnr_E
  implicit none

  character(len=mid) :: Id = 'pressure'

  real, dimension(mx,my,mz), intent(inout) :: r, ee
  real, dimension(mx,my,mz), intent(out)   :: pp
  !logical, optional, intent(in) :: do_indx
  logical :: do_indx
  integer :: dim(3)

  real, dimension(mx,1,mz) :: ee_slice, r_slice, var

  call print_trace (trim(Id), dbg_eos, 'BEGIN')

  dim = (/ mx, my, mz /)

  ! force do_indx = t
  do_indx = .true.
  eos_indxs%do_indx = do_indx

  ! look up pressure
  call eostab%lookup(dim, ee, r, pp, 1, 1, .true., eos_indxs)

  ! auxiliary arrays for boundary routines
  ! note: only used for subdomains at bottom boundary (ub)

  ! pressure at bottom boundary
  pbot0(:,:) = pp(:,ub,:)

  ! pressure at bottom boundary: partial derivatives
  ! (d lnP / d ee)_r:
  dim = (/ mx, 1, mz /)

  ee_slice(:,1,:) = ee(:,ub,:)
  r_slice (:,1,:) = r (:,ub,:)

  call eostab%lookup(dim, ee_slice, r_slice, var, 1, 2, .false.)
  dlnpdE_r(:,:) = var(:,1,:) / ee_slice(:,1,:)

  call eostab%lookup(dim, ee_slice, r_slice, var, 1, 3, .false.)
  dlnpdlnr_E(:,:) = var(:,1,:)

  call print_trace (trim(Id), dbg_eos, 'END')

end subroutine pressure

!................................................................................
subroutine temperature(r, ee, tt)

  use params, only: mx, my, mz, mid, dbg_eos
  use eos, only: eostab, eos_indxs
  implicit none

  character(len=mid) :: Id = 'temperature'

  real, dimension(mx,my,mz), intent(inout) :: r, ee
  real, dimension(mx,my,mz), intent(out)   :: tt
  !logical, optional, intent(in) :: do_indx
  logical :: do_indx
  integer :: dim(3)

  call print_trace (trim(Id), dbg_eos, 'BEGIN')

  dim = (/ mx, my, mz /)

  ! force do_indx = t
  do_indx = .true.
  eos_indxs%do_indx = do_indx

  ! look up temperature
  call eostab%lookup(dim, ee, r, tt, 3, 1, .true., eos_indxs)

  call print_trace (trim(Id), dbg_eos, 'END')

end subroutine temperature

!................................................................................
subroutine elpressure(r, ee, pel)

  use params, only: mx, my, mz, mid, master, dbg_eos
  use eos, only: eostab, eos_indxs, lnkboltz, uup, uul
  implicit none

  character(len=mid) :: Id = 'elpressure'

  real, dimension(mx,my,mz), intent(inout) :: r, ee
  real, dimension(mx,my,mz), intent(out)   :: pel
  real, dimension(mx,my,mz) :: lntt
  !logical, optional, intent(in) :: do_indx
  logical :: do_indx
  integer :: dim(3)
  integer :: nvar

  call print_trace (trim(Id), dbg_eos, 'BEGIN')

  dim = (/ mx, my, mz /)

  nvar = eostab%nvar
  if (nvar.lt.6) then
     if (master) print '(X,A)', &
          trim(Id)//': EOS table does not contain electron number density!'
     call abort_mpi
     stop
  end if

  ! force do_indx = t
  do_indx = .true.
  eos_indxs%do_indx = do_indx

  ! look up log temperature;
  ! log electron number density;
  ! log electron pressure, simulation units;
  ! electron pressure, simulation units
  call eostab%lookup(dim, ee, r, lntt, 3, 1, .false., eos_indxs)
  call eostab%lookup(dim, ee, r, pel, 4, 1, .false., eos_indxs)
  pel = pel + lntt + lnkboltz - log(uup) - 3.*log(uul)
  pel(:,:,:) = exp(pel(:,:,:))

  call print_trace (trim(Id), dbg_eos, 'END')

end subroutine elpressure

!................................................................................
subroutine rosseland (r, tt, lnrk)

  use params, only: mx, my, mz, mid, dbg_eos
  use eos, only: radtab, rad_indxs
  implicit none

  character(len=mid):: Id = 'rosseland'

  real, dimension(mx,my,mz), intent(inout) :: r, tt
  real, dimension(mx,my,mz), intent(out)   :: lnrk
  !logical, optional, intent(in) :: do_indx
  logical :: do_indx
  integer :: dim(3)
  integer :: nlam

  call print_trace (trim(Id), dbg_eos, 'BEGIN')

  dim = (/ mx, my, mz /)

  ! force do_indx = t
  do_indx = .true.
  rad_indxs%do_indx = do_indx

  ! look up ln Rosseland extinction coefficient
  nlam = radtab%nlam
  call radtab%lookup(dim, tt, r, lnrk, nlam+1, 1, .false., rad_indxs)

  call print_trace (trim(Id), dbg_eos, 'END')

end subroutine rosseland

!................................................................................
subroutine chiplanck (r, tt, lnrk)

  use params, only: mx, my, mz, mid, dbg_eos
  use eos, only: radtab, rad_indxs
  implicit none

  character(len=mid):: Id = 'chiplanck'

  real, dimension(mx,my,mz), intent(inout) :: r, tt
  real, dimension(mx,my,mz), intent(out)   :: lnrk
  !logical, optional, intent(in) :: do_indx
  logical :: do_indx
  integer :: dim(3)
  integer :: nlam

  call print_trace (trim(Id), dbg_eos, 'BEGIN')

  dim = (/ mx, my, mz /)

  ! force do_indx = t
  do_indx = .true.
  rad_indxs%do_indx = do_indx

  ! look up ln Planck-weighted mean extinction coefficient
  nlam = radtab%nlam
  call radtab%lookup(dim, tt, r, lnrk, nlam+1, 2, .false., rad_indxs)

  call print_trace (trim(Id), dbg_eos, 'END')

end subroutine chiplanck

!................................................................................
subroutine chistd (r, tt, lnrk)

  use params, only: mx, my, mz, mid, dbg_eos
  use eos, only: radtab, rad_indxs
  implicit none

  character(len=mid):: Id = 'chistd'

  real, dimension(mx,my,mz), intent(inout) :: r, tt
  real, dimension(mx,my,mz), intent(out)   :: lnrk
  !logical, optional, intent(in) :: do_indx
  logical :: do_indx
  integer :: dim(3)
  integer :: nlam

  call print_trace (trim(Id), dbg_eos, 'BEGIN')

  dim = (/ mx, my, mz /)

  ! force do_indx = t
  do_indx = .true.
  rad_indxs%do_indx = do_indx

  ! look up ln standard extinction coefficient
  nlam = radtab%nlam
  call radtab%lookup(dim, tt, r, lnrk, nlam+1, 3, .false., rad_indxs)

  call print_trace (trim(Id), dbg_eos, 'END')

end subroutine chistd

!................................................................................
