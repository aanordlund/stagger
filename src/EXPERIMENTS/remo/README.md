Notes on EXPERIMENT=remo
========================

Quickstart
----------
To compile standard version:

	cd stagger/src
	make EXPERIMENT=remo CASE=reshape

To compile version for driving p-mode-like oscillations:

	make EXPERIMENT=remo CASE=pdrive


Experimental (development) versions of the code are also available; to compile current (development) version:

	make EXPERIMENT=remo CASE=reshape_devel

To compile test version in which loops over bins and rays are swapped compared to standard version:

	make EXPERIMENT=remo CASE=reshape_devel_test

***Please note that the development versions are experimental and will occasionally undergo rapid changes and should therefore not be used for production runs!***



NOTES:
======

2018-08-09:
-----------
Updated standard CASE=reshape and CASE=pdrive version using current development version of the radiation module. Moved some older files to bak directory (legacy code or alternative version of reshape that were rarely used). 


2018-07-12:
-----------
BUG: Corrected bug in conversion factor from cgs to internal units for electron pressure in subroutine `elpressure` in `EXPERIMENTS/remo/eosrad/eosrad_lookup.f90`.

	
2018-05-11:
-----------
Merged `pdrive` branch into `master`. The "pdrive" version adds an option to drive p-mode-like oscillations by introducing a sinusoidally varying pressure perturbation at the bottom boundary.

The option is activated by switching on `do_pdrive = t` in the `&bdry` namelist in the input file. In addition, it is necessary to provide the amplitude of the perturbations relative to the average pressure at the bottom boundary (`p_ampl`), the oscillation frequency and phase of the p-mode driving (`p_omega` and `p_phase`). The average pressure is set by the usual boundary conditions for density and internal energy per unit volume in upflows at the bottom boundary (`rbot` and `ebot`).



2017-09-28:
-----------
Cleaned up and reorganised `eosrad/eosrad_lookup.f90`; moved `Nlam` variable from `eos` to `table` module for better internal consistency, added a `use table` statement in the other routines that require `Nlam`. Changes should not affect in any way results compared with previous version.


2017-05-18:
-----------
Moved `quad_init.f90`, `carlson.f90`, and `limb.f90` from `devel/` to `transfer/`: present version of routines may be considered stable enough for production runs.

Edited dependencies in relevant `.mkf` files.


2016-12-23:
-----------
The Stagger repository has moved from CVS to git!


2016-12-21:
-----------
Files in `devel/` directory: added routines for saving limb darkening output.

Added new test version of radiation module:

`devel/radiation_eosrad_mpi_reshape_devel_test.f90`

where the loops over bins and rays are interchanged; in test version, the outer loop is over rays, the inner loop is over bins. For a given ray direction, the (reshaped) density and internal energy data volumes are tilted, then extinction coefficients and source functions are looked up for each bin. 

The purpose is to test whether performance can be increased by this modification.


2016-10-17:
-----------
Added routine with Carlson quadrature A angles and weights tabulated for three cases (3, 6, and 10 rays per quadrant).


2016-10-14:
-----------
Added the following "development" files:

-_Introducing a new routine to manage quadrature types for the radiative transfer module. To start with, it is a wrapper to the `gausi` and `radaui` routines, but it can be easily extended by adding other quadrature types.

The main radiative transfer routine now loops over rays directly rather than mu and phi separately, which is a much cleaner and more flexible way of handling the radiative transfer and integration over rays.

---

Verification test: carried out comparison of standard reshape case and development version with new `quad_init' routine and new ray loop: perfect agreement!



2016-10-12:
-----------

### Tag: remo-16-10-12

Moved `radiation_lookup_mpi_devel.f90` --> `eosrad/radiation_lookup_mpi_reshape.f90`
Temporarily restored `radiation_lookup_mpi_devel.f90` to allow compilation of `feautrier` and `integral` cases.

Moved legacy routines `radiation_yrad_mpi.f90` `radiation_yradmon_eosrad_mpi.f90` to tests directory to reduce clutter.

---
Created directories

* **bak/** where legacy code is stored for reference purpose or to reduce clutter
* **cobold/** containing some routines for using Stagger code with CO5BOLD EOS and opacity tables (testing purpose);
* **devel/** for hosting files for development versions


NOTE 1: the CO5BOLD-related code is no longer maintained and some clean-up work is needed in order to make the routines work again. However, the files are provided here to serve as documentation for how to implement the CO5BOLD tables.

NOTE 2: files in bak are in principle safe to remove without compromising the current working code in any way.

---
Moved the following files to **bak/**:

- `Makefile_devel`
- `io_eosrad.f90`
- `io_limitee.f90`
- `limitee.f90`
- `main_eosrad.f90`
- `pde_bob_limitee.f90`
- `pde_eosrad.f90`
- `radiation_lookup_mpi_remo.f90`
- `transfer_feautrier_mpi_alt.f90`
- `transfer_mpi_devel_alt.f90`


Moved the following files to **eosrad/**:

- `radiation_eosrad.f90`
- `radiation_eosrad_mpi.f90`
- `radiation_eosrad_mpi_devel.f90` --> `radiation_eosrad_mpi_ympi.f90`
- `radiation_lookup_mpi_devel.f90` --> `radiation_lookup_mpi_ympi.f90`

**NOTE:** `radiation_..._devel.f90` files have been renamed with `_ympi` label in place of `_devel` label to reflect the fact they are compatible with the transfer routines that use MPI splitting of rays in the y direction.

Moved the following files to **transfer/**:

- `ddtau_calc_mpi.f90`
- `ddtau_calc_mpi_mono.f90`
- `tau_calc_mpi.f90`
- `transfer_feautrier_mpi.f90`
- `transfer_integral_mpi.f90`
- `transfer_integral_mpi_test.f90`


2016-10-11:
-----------

### Tag: remo-16-10

Created tag remo-16-10 before undertaking reorganisation of the directory structure.

As a part of the operation of reducing clutter, new directories have been created:

* **aux/**  for additional files such as EOS and opacity tables and associated input files;
* **boundaries/** containing subroutines for boundary conditions;
* **eosrad/** for EOS and opacity binning look-up routines
* **transfer/** for radiative transfer solvers and related routines;
* **tests/** for tests and experiments



2014-09-24:
-----------

Currently supported version is compiled with:

	cd stagger-code/src
	make EXPERIMENT=remo CASE=reshape

In particular, the only maintained version of the boundary conditions
at the top is  EXPERIMENTS/remo/boundary\_potentialtop\_eeasym.f90
which is the one more consistent with the version used by mconv.


2010-11-26:
-----------

`radiation_eosrad.f90`: "no mpi" version is now consistent with "mpi" version; `mblocks` paramter not needed in "no mpi" version; added keyword `do_tsc` for compatibility with "no mpi" `trnslt.f90`;

`radiation_eosrad_mpi.f90`: corrected bug in `do_yrad=f` case: add contribution to heating rate only down to n2 rather than my or ny);

`eosrad_lookup.f90`: added internal unit parameters in `eqofst` namelist; cleaned up; added print out with basic information about tables.


2010-11-24:
-----------

**Changes:**

`radiation_yrad_eosrad_mpi.f90` merged with `radiation_eosrad_mpi.f90` --> `radiation_eosrad_mpi.f90` .

Introduced keyword `do_yrad` in input file to activate/deactivate 
adaptive radiation scale.

Updated `Makefile`.

`radiation_yrad_eosrad_mpi.f90` no longer necessary: deleted from repository.


2010-11-19:
-----------

**Changes:**

The `eosrad_lookup_limitee.f90` has now been merged into `eosrad_lookup.f90`; the keyword `do_limit_tab` has been introduced in the `eqofst` namelist in the input file to switch between the two versions. (See input_eosrad.txt sample file.)

In the new Makefile, the LIMITEE option has been removed because it's now obsolete. Old backup copies of the Makefile have been removed from the repository because obsolete.

The `radiation_yrad_eosrad_mpi.f90` routine has been cleaned up and made more efficient with the introduction of the `tau_calc` routine and some auxiliary arrays.


**To compile:**

	cd stagger-code/src/
	make EXPERIMENT=remo MP=_mpi YRAD=_yrad BCTOP=_eeasym


**Important! Warnings!**

OpenMP is broken and not yet supported in this version: OpenMP calls throughout the code are not "safe" and work needs to be done in this respect. In other words, make sure that OpenMP is "turned off":

	setenv OMP_NUM_THREADS 1

The "no MPI" version needs an update and needs to be made consistent
with the MPI version. Please avoid using it.

The `radiation_eosrad_mpi.f90` routine also needs cleaning and will be soon merged with the `radiation_yrad_eosrad_mpi.f90` one for the sake of simplicity. Also, a revision of the "no MPI" version is needed.cd 
