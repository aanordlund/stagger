!...............................................................................
module hav_mod

  implicit none

  real, allocatable, dimension(:,:,:) :: lnr, ee, tt, Pel
  real, allocatable, dimension(:,:,:) :: lnRoss, lnPlck, lnStd

end module hav_mod

!...............................................................................
subroutine calc_hav(r,Ux,Uy,Uz,e,p,px,py,pz,Bx,By,Bz,Ex,Ey,Ez)
  
  ! calculates horizontal averages of various variables

  use params
  use hav_mod
  use arrays, only: &
       rhoav, eav, lnrav, eeav, pav, ttav, &
       pxav, pyav, pzav, Uxav, Uyav, Uzav, &
       Exav, Eyav, Ezav, Bxav, Byav, Bzav, &
       pyavUp, pyavDn, ffpyavUp, ffpyavDn, &
       rhominh, rhomaxh, eeminh, eemaxh, &
       ttminh, ttmaxh, pminh, pmaxh, &
       fvisc, fpoynt, frad, fkin, fconv, &
       fei, facous, qqav, qqrav, &
       Pelav, Rossav, Plckav, Stdav
  
  implicit none

  character(len=mid) :: Id = 'calc_hav'
  real, dimension(mx,my,mz):: r, Ux, Uy, Uz, e, p, px, py, pz
  real, dimension(mx,my,mz):: Bx, By, Bz, Ex, Ey, Ez
  logical :: foutput
  integer :: iz

  call print_id(Id)

  if ( .not.foutput('fluxes.txt') ) return

  ! allocate arrays for auxiliary variables
  !$omp master
  allocate( lnr(mx,my,mz), ee(mx,my,mz), tt(mx,my,mz), Pel(mx,my,mz) )
  allocate( lnRoss(mx,my,mz), lnPlck(mx,my,mz), lnStd(mx,my,mz) )
  !$omp end master
  !$omp barrier
  
  ! calculate internal energy per unit mass
  do iz=izs,ize
     lnr(:,:,iz) = alog( r(:,:,iz) )
     ee (:,:,iz) = e(:,:,iz) / r(:,:,iz)
  end do

  ! look up auxiliary quantities
  call temperature( r, ee, tt )
  call elpressure ( r, ee, Pel )
  call rosseland  ( r, tt, lnRoss )
  call chiplanck  ( r, tt, lnPlck )
  call chiStd     ( r, tt, lnStd  )

  call haverage_subr ( r,  rhoav)
  call haverage_subr ( e,  eav  )
  call haverage_subr ( lnr,lnrav)
  call haverage_subr ( ee, eeav )
  call haverage_subr ( p,  pav  )
  call haverage_subr ( px, pxav )
  call haverage_subr ( py, pyav )
  call haverage_subr ( pz, pzav )
  call haverage_subr ( Ux, Uxav )
  call haverage_subr ( Uy, Uyav )
  call haverage_subr ( Uz, Uzav )
  call haverage_subr ( tt, ttav )
  call haverage_subr ( Pel,Pelav)
  call haverage_subr ( exp(lnRoss), Rossav )
  call haverage_subr ( exp(lnPlck), Plckav )
  call haverage_subr ( exp(lnStd),  Stdav  )

  if (do_mhd) then
     call haverage_subr ( Bx, Bxav )
     call haverage_subr ( By, Byav )
     call haverage_subr ( Bz, Bzav )
     call haverage_subr ( Ex, Exav )
     call haverage_subr ( Ey, Eyav )
     call haverage_subr ( Ez, Ezav )
  end if


  ! find minimum and maximum of density, energy per unit mass, temperature,
  ! and pressure on horizontal planes

  call horiz_minmax( r,  rhominh, rhomaxh )
  call horiz_minmax( ee, eeminh,  eemaxh  )
  call horiz_minmax( p,  pminh,   pmaxh   )
  call horiz_minmax( tt, ttminh,  ttmaxh  )

  !$omp barrier
  !$omp master
  call haver_updn( py, py, pyavUp, pyavDn, ffpyavUp, ffpyavDn)

  deallocate( lnRoss, lnPlck, lnStd )
  deallocate( lnr, ee, tt, Pel)
  !$omp end master

end subroutine calc_hav

!...............................................................................
subroutine output_hav

! writes horizontal averages to output file

  use params, only: ym, ymdn, my, lb, ub, isnap, t, dt, do_scp, do_mhd, &
       mfile, mid, mpi_ny, mpi_y, master, data_unit, file
  use mpi_mod, only: mpi_comm_beam, MPI_REAL8, my_real, my_integer, mpi_err
  use arrays, only: &
       rhoav, eav, lnrav, eeav, pav, ttav, &
       pxav, pyav, pzav, Uxav, Uyav, Uzav, &
       Exav, Eyav, Ezav, Bxav, Byav, Bzav, &
       pyavUp, pyavDn, ffpyavUp, ffpyavDn, &
       rhominh, rhomaxh, eeminh, eemaxh, &
       ttminh, ttmaxh, pminh, pmaxh, &
       fvisc, fpoynt, frad, fkin, fconv, fei, facous, &
       qqav, qqrav, Pelav, Rossav, Plckav, Stdav
  use forcing,  only: gy
  use boundary, only: rbot, ebot
  use table, only: uur, uul, uut, uuv, uuee, uup, uuk, uubp

  implicit none

  character(len=mid) :: Id = 'output_hav'
  character(len=mfile) :: name, havfile

  ! Stefan-Boltzmann constant, cgs units
  real(kind=8), parameter  :: stefboltz = 5.6705d-5

  real(kind=8) :: teff, lnravbot, eeavbot, lnrbot, eebot
  real(kind=8) :: rhoavSum
  real(kind=8) :: pxavSum, uxcm
  real(kind=8) :: pyavSum, uycm
  real(kind=8) :: pzavSum, uzcm

  logical      :: foutput
  integer      :: mpi_iy, iy, ksnap, nghost, ndep, lowb, upb
  integer      :: iy_up, iy_low
  real(kind=8) :: frad_offset

  ! allocatable _all arrays for storage of the horizontal averages, full depth scale
  real,         allocatable, dimension(:) :: ym_all, ymdn_all
  real(kind=8), allocatable, dimension(:) :: rhoav_all, eav_all
  real(kind=8), allocatable, dimension(:) :: lnrav_all, eeav_all
  real(kind=8), allocatable, dimension(:) :: pav_all, ttav_all
  real(kind=8), allocatable, dimension(:) :: pxav_all, pyav_all, pzav_all
  real(kind=8), allocatable, dimension(:) :: Uxav_all, Uyav_all, Uzav_all
  real(kind=8), allocatable, dimension(:) :: Exav_all, Eyav_all, Ezav_all
  real(kind=8), allocatable, dimension(:) :: Bxav_all, Byav_all, Bzav_all
  real(kind=8), allocatable, dimension(:) :: pyavUp_all, pyavDn_all
  real(kind=8), allocatable, dimension(:) :: ffpyavUp_all, ffpyavDn_all
  real(kind=8), allocatable, dimension(:) :: rhominh_all, rhomaxh_all
  real(kind=8), allocatable, dimension(:) :: eeminh_all, eemaxh_all
  real(kind=8), allocatable, dimension(:) :: ttminh_all, ttmaxh_all
  real(kind=8), allocatable, dimension(:) :: pminh_all, pmaxh_all
  real(kind=8), allocatable, dimension(:) :: fvisc_all, fkin_all, fconv_all
  real(kind=8), allocatable, dimension(:) :: fei_all, facous_all, fpoynt_all
  real(kind=8), allocatable, dimension(:) :: frad_all, qqav_all, qqrav_all
  real(kind=8), allocatable, dimension(:) :: Pelav_all, Rossav_all
  real(kind=8), allocatable, dimension(:) :: Plckav_all, Stdav_all

  call print_id(Id)

  ! if fluxes.txt not present, simply exit
  if (.not.foutput('fluxes.txt'))  return


  ! number of depth points of entire simulation domain
  ndep = mpi_ny * my

  ! broadcast value of lb from mpi_y=0 and of ub from mpi_y=mpi_ny-1
  ! to all other subdomains in local vertical beam
  lowb = lb
  upb  = ub
  call MPI_BCAST(lowb, 1, my_integer, 0,        mpi_comm_beam(2), mpi_err)
  call MPI_BCAST(upb,  1, my_integer, mpi_ny-1, mpi_comm_beam(2), mpi_err)
  ! add (mpi_ny-1) * my layers to get the value of the upb index on the full depth scale 
  upb  = upb + (mpi_ny-1)*my 

  ! allocate _all arrays
  allocate( ym_all(ndep), ymdn_all(ndep) )
  allocate( rhoav_all(ndep), eav_all(ndep) )
  allocate( lnrav_all(ndep), eeav_all(ndep) )
  allocate( pav_all(ndep), ttav_all(ndep) )
  allocate( pxav_all(ndep), pyav_all(ndep), pzav_all(ndep) )
  allocate( Uxav_all(ndep), Uyav_all(ndep), Uzav_all(ndep) )
  allocate( Exav_all(ndep), Eyav_all(ndep), Ezav_all(ndep) )
  allocate( Bxav_all(ndep), Byav_all(ndep), Bzav_all(ndep) )
  allocate( pyavUp_all(ndep), pyavDn_all(ndep) )
  allocate( ffpyavUp_all(ndep), ffpyavDn_all(ndep) )
  allocate( rhominh_all(ndep), rhomaxh_all(ndep) )
  allocate( eeminh_all(ndep), eemaxh_all(ndep) )
  allocate( ttminh_all(ndep), ttmaxh_all(ndep) )
  allocate( pminh_all(ndep), pmaxh_all(ndep) ) 
  allocate( fvisc_all(ndep), fkin_all(ndep), fconv_all(ndep) )
  allocate( fei_all(ndep), facous_all(ndep), fpoynt_all(ndep) )
  allocate( frad_all(ndep), qqav_all(ndep), qqrav_all(ndep) )
  allocate( Pelav_all(ndep), Rossav_all(ndep) )
  allocate( Plckav_all(ndep), Stdav_all(ndep) )

  ! gather all vertical depth scale segments (ym and ymdn) to mpi_y=0 task
  !  of local vertical beam (mpi_y=0)
  call MPI_GATHER( ym,   my, my_real, ym_all,   my, my_real, 0, mpi_comm_beam(2), mpi_err)
  call MPI_GATHER( ymdn, my, my_real, ymdn_all, my, my_real, 0, mpi_comm_beam(2), mpi_err)

  ! gather all segments of horizontally averaged quantities to mpi_y=0 task
  !  of local vertical beam (mpi_y=0)
  call MPI_GATHER(rhoav, my, MPI_REAL8, rhoav_all, my, MPI_REAL8, 0, mpi_comm_beam(2), mpi_err)
  call MPI_GATHER(eav,   my, MPI_REAL8, eav_all,   my, MPI_REAL8, 0, mpi_comm_beam(2), mpi_err)
  call MPI_GATHER(lnrav, my, MPI_REAL8, lnrav_all, my, MPI_REAL8, 0, mpi_comm_beam(2), mpi_err)
  call MPI_GATHER(rhoav, my, MPI_REAL8, rhoav_all, my, MPI_REAL8, 0, mpi_comm_beam(2), mpi_err)
  call MPI_GATHER(eav,   my, MPI_REAL8, eav_all,   my, MPI_REAL8, 0, mpi_comm_beam(2), mpi_err)
  call MPI_GATHER(lnrav, my, MPI_REAL8, lnrav_all, my, MPI_REAL8, 0, mpi_comm_beam(2), mpi_err)
  call MPI_GATHER(eeav,  my, MPI_REAL8, eeav_all,  my, MPI_REAL8, 0, mpi_comm_beam(2), mpi_err)
  call MPI_GATHER(pav,   my, MPI_REAL8, pav_all,   my, MPI_REAL8, 0, mpi_comm_beam(2), mpi_err)
  call MPI_GATHER(ttav,  my, MPI_REAL8, ttav_all,  my, MPI_REAL8, 0, mpi_comm_beam(2), mpi_err)
  call MPI_GATHER(pxav,  my, MPI_REAL8, pxav_all,  my, MPI_REAL8, 0, mpi_comm_beam(2), mpi_err)
  call MPI_GATHER(pyav,  my, MPI_REAL8, pyav_all,  my, MPI_REAL8, 0, mpi_comm_beam(2), mpi_err)
  call MPI_GATHER(pzav,  my, MPI_REAL8, pzav_all,  my, MPI_REAL8, 0, mpi_comm_beam(2), mpi_err)
  call MPI_GATHER(Uxav,  my, MPI_REAL8, Uxav_all,  my, MPI_REAL8, 0, mpi_comm_beam(2), mpi_err)
  call MPI_GATHER(Uyav,  my, MPI_REAL8, Uyav_all,  my, MPI_REAL8, 0, mpi_comm_beam(2), mpi_err)
  call MPI_GATHER(Uzav,  my, MPI_REAL8, Uzav_all,  my, MPI_REAL8, 0, mpi_comm_beam(2), mpi_err)
  call MPI_GATHER(Exav,  my, MPI_REAL8, Exav_all,  my, MPI_REAL8, 0, mpi_comm_beam(2), mpi_err)
  call MPI_GATHER(Eyav,  my, MPI_REAL8, Eyav_all,  my, MPI_REAL8, 0, mpi_comm_beam(2), mpi_err)
  call MPI_GATHER(Ezav,  my, MPI_REAL8, Ezav_all,  my, MPI_REAL8, 0, mpi_comm_beam(2), mpi_err)
  call MPI_GATHER(Bxav,  my, MPI_REAL8, Bxav_all,  my, MPI_REAL8, 0, mpi_comm_beam(2), mpi_err)
  call MPI_GATHER(Byav,  my, MPI_REAL8, Byav_all,  my, MPI_REAL8, 0, mpi_comm_beam(2), mpi_err)
  call MPI_GATHER(Bzav,  my, MPI_REAL8, Bzav_all,  my, MPI_REAL8, 0, mpi_comm_beam(2), mpi_err)
  call MPI_GATHER(pyavUp,my, MPI_REAL8, pyavUp_all,my, MPI_REAL8, 0, mpi_comm_beam(2), mpi_err)
  call MPI_GATHER(pyavDn,my, MPI_REAL8, pyavDn_all,my, MPI_REAL8, 0, mpi_comm_beam(2), mpi_err)
  call MPI_GATHER(ffpyavUp,my,MPI_REAL8,ffpyavUp_all,my,MPI_REAL8,0, mpi_comm_beam(2), mpi_err)
  call MPI_GATHER(ffpyavDn,my,MPI_REAL8,ffpyavDn_all,my,MPI_REAL8,0, mpi_comm_beam(2), mpi_err)
  call MPI_GATHER(rhominh, my,MPI_REAL8,rhominh_all, my,MPI_REAL8,0, mpi_comm_beam(2), mpi_err)
  call MPI_GATHER(rhomaxh, my,MPI_REAL8,rhomaxh_all, my,MPI_REAL8,0, mpi_comm_beam(2), mpi_err)
  call MPI_GATHER(eeminh, my, MPI_REAL8, eeminh_all, my, MPI_REAL8, 0, mpi_comm_beam(2), mpi_err)
  call MPI_GATHER(eemaxh, my, MPI_REAL8, eemaxh_all, my, MPI_REAL8, 0, mpi_comm_beam(2), mpi_err)
  call MPI_GATHER(ttminh, my, MPI_REAL8, ttminh_all, my, MPI_REAL8, 0, mpi_comm_beam(2), mpi_err)
  call MPI_GATHER(ttmaxh, my, MPI_REAL8, ttmaxh_all, my, MPI_REAL8, 0, mpi_comm_beam(2), mpi_err)
  call MPI_GATHER(pminh,  my, MPI_REAL8, pminh_all,  my, MPI_REAL8, 0, mpi_comm_beam(2), mpi_err)
  call MPI_GATHER(pmaxh,  my, MPI_REAL8, pmaxh_all,  my, MPI_REAL8, 0, mpi_comm_beam(2), mpi_err)
  call MPI_GATHER(fvisc,  my, MPI_REAL8, fvisc_all,  my, MPI_REAL8, 0, mpi_comm_beam(2), mpi_err)
  call MPI_GATHER(fpoynt, my, MPI_REAL8, fpoynt_all, my, MPI_REAL8, 0, mpi_comm_beam(2), mpi_err)
  call MPI_GATHER(frad,   my, MPI_REAL8, frad_all,   my, MPI_REAL8, 0, mpi_comm_beam(2), mpi_err)
  call MPI_GATHER(fkin,   my, MPI_REAL8, fkin_all,   my, MPI_REAL8, 0, mpi_comm_beam(2), mpi_err)
  call MPI_GATHER(fconv,  my, MPI_REAL8, fconv_all,  my, MPI_REAL8, 0, mpi_comm_beam(2), mpi_err)
  call MPI_GATHER(fei,    my, MPI_REAL8, fei_all,    my, MPI_REAL8, 0, mpi_comm_beam(2), mpi_err)
  call MPI_GATHER(facous, my, MPI_REAL8, facous_all, my, MPI_REAL8, 0, mpi_comm_beam(2), mpi_err)
  call MPI_GATHER(qqav,   my, MPI_REAL8, qqav_all,   my, MPI_REAL8, 0, mpi_comm_beam(2), mpi_err)
  call MPI_GATHER(qqrav,  my, MPI_REAL8, qqrav_all,  my, MPI_REAL8, 0, mpi_comm_beam(2), mpi_err)
  call MPI_GATHER(Pelav,  my, MPI_REAL8, Pelav_all,  my, MPI_REAL8, 0, mpi_comm_beam(2), mpi_err)
  call MPI_GATHER(Rossav, my, MPI_REAL8, Rossav_all, my, MPI_REAL8, 0, mpi_comm_beam(2), mpi_err)
  call MPI_GATHER(Plckav, my, MPI_REAL8, Plckav_all, my, MPI_REAL8, 0, mpi_comm_beam(2), mpi_err)
  call MPI_GATHER(Stdav,  my, MPI_REAL8, Stdav_all,  my, MPI_REAL8, 0, mpi_comm_beam(2), mpi_err)


  ! master process: mpi_x.eq.0 .and. mpi_y.eq.0 .and. mpi_z.eq.0
  if (master) then

     ! define hav filename
     havfile = name('fluxes.txt','hav',file)

     ! call scp_name with ksnap = isnap; note that ksnap is then set to 0 by scp_name.
     ksnap = isnap
     call scp_name(havfile, ksnap)

     ! number of ghost-zones
     nghost = lowb - 1

     ! if mpi_ny > 1: add appropriate offsets to frad segments to recover 
     ! full frad on global depth scale
     if (mpi_ny.gt.1) then
        do mpi_iy = mpi_ny-2, 0, -1
           iy_up  = (mpi_iy+1) * my
           iy_low = (mpi_iy  ) * my + 1
           frad_offset = frad_all( iy_up + 1 )
           do iy = iy_up,iy_low,-1
              frad_all( iy ) = frad_all( iy ) + frad_offset
           end do
        end do
     end if

     ! effective temperature
     teff = ( -frad_all(lowb)/stefboltz * uup * uuv ) ** 0.25

     ! lnr and ee, bottom boundary condition
     lnrbot = alog(rbot)
     eebot  = ebot/rbot
     
     ! lnr and ee, average value at bottom boundary
     lnravbot = lnrav_all(upb)
     eeavbot  =  eeav_all(upb)

     ! compute center of mass velocity
     rhoavSum = 0.0
     pxavSum  = 0.0
     pyavSum  = 0.0
     pzavSum  = 0.0
     do iy=lowb,upb
        rhoavSum = rhoavSum + rhoav_all(iy)
        pxavSum  = pxavSum  +  pxav_all(iy)
        pyavSum  = pyavSum  +  pyav_all(iy)
        pzavSum  = pzavSum  +  pzav_all(iy)
     enddo
     uxcm = pxavSum / rhoavSum    ! center of mass velocity, x component
     uycm = pyavSum / rhoavSum    ! center of mass velocity, y component
     uzcm = pzavSum / rhoavSum    ! center of mass velocity, z component

     ! open hav file for writing
     print '(A32,A,A14,I5)', ' writing horizontal averages to ', trim( havfile ), ' for snapshot ', isnap
     if (do_scp) then
        open (data_unit, file=havfile, status='replace')
     else
        open (data_unit, file=havfile, status='unknown', position='append')
     endif

     ! write header in namelist format
     write (data_unit, fmt='(A)') '&haverages'
     write (data_unit, fmt='(3X,A10,I14,  A1)')  '   isnap =',  isnap,    ','
     write (data_unit, fmt='(3X,A10,I14,  A1)')  '    ndep =',  ndep,     ','
     write (data_unit, fmt='(3X,A10,F14.6,A1)')  '       t =',  t,        ','
     write (data_unit, fmt='(3X,A10,F14.6,A1)')  '      dt =',  dt,       ','	  
     write (data_unit, fmt='(3X,A10,I14,  A1)')  '  nghost =',  nghost,   ','
     write (data_unit, fmt='(3X,A10,E14.4,A1)')  '     uur =',  uur,      ','
     write (data_unit, fmt='(3X,A10,E14.4,A1)')  '     uul =',  uul,      ','
     write (data_unit, fmt='(3X,A10,E14.4,A1)')  '     uut =',  uut,      ','
     write (data_unit, fmt='(3X,A10,F14.4,A1)')  '    teff =',  teff,     ','
     write (data_unit, fmt='(3X,A10,F14.6,A1)')  '      gy =',  gy,       ','
     write (data_unit, fmt='(3X,A10,F14.6,A1)')  '  lnrbot =',  lnrbot,   ','
     write (data_unit, fmt='(3X,A10,F14.6,A1)')  '   eebot =',  eebot,    ','
     write (data_unit, fmt='(3X,A10,F14.6,A1)')  'lnravbot =',  lnravbot, ','
     write (data_unit, fmt='(3X,A10,F14.6,A1)')  ' eeavbot =',  eeavbot,  ','     
     write (data_unit, fmt='(3X,A10,F14.6,A1)')  '    uxcm =',  uxcm,     ','
     write (data_unit, fmt='(3X,A10,F14.6,A1)')  '    uycm =',  uycm,     ','
     write (data_unit, fmt='(3X,A10,F14.6,A1)')  '    uzcm =',  uzcm,     ' '
     write (data_unit, fmt='(A)') '/'


     ! write horizontal averages
     write (data_unit,fmt='(A6,8(X,A13))') &
          'k', 'ym', 'ymdn', 'rho', 'e', 'lnr', 'ee', 'tt', 'P'
     do iy=1,ndep
        write (data_unit,fmt='(I6,8(X,E13.5))') &
             iy, &
             ym_all(iy), ymdn_all(iy), rhoav_all(iy), eav_all(iy), &
             lnrav_all(iy), eeav_all(iy), ttav_all(iy), pav_all(iy)
     end do

     write (data_unit,'(A6,8(X,A13))') &
          'k', 'rhomin', 'rhomax', 'eemin', 'eemax', 'ttmin', 'ttmax', 'Pmin', 'Pmax'
     do iy=1,ndep
        write (data_unit,'(I6,8(X,E13.5))') &
             iy, &
             rhominh_all(iy), rhomaxh_all(iy), eeminh_all(iy), eemaxh_all(iy), &
             ttminh_all(iy), ttmaxh_all(iy), pminh_all(iy), pmaxh_all(iy)
     end do

     write (data_unit,'(A6,4(X,A13))') &
          'k', 'Pel', 'alphaR', 'alphaP', 'alphaS'
     do iy=1,ndep
        write (data_unit,'(I6,4(X,E13.5))') &
             iy,  &
             pelav_all(iy), Rossav_all(iy), Plckav_all(iy), Stdav_all(iy)
     end do

     write (data_unit,'(A6,10(X,A13))') &
          'k', 'px', 'py', 'pz', 'ux', 'uy', 'uz', 'pyup', 'pydn', 'ffpyup', 'ffpydn'
     do iy=1,ndep
        write (data_unit,'(I6,10(X,E13.5))') &
             iy, &
             pxav_all(iy), pyav_all(iy), pzav_all(iy), &
             uxav_all(iy), uyav_all(iy), uzav_all(iy), &
             pyavUp_all(iy), pyavDn_all(iy), &
             ffpyavUp_all(iy), ffpyavDn_all(iy)
     end do


     if (do_mhd) then
       write (data_unit,'(A6,9(X,A13))') &
             'k', 'fvisc', 'fkin', 'fconv', 'fei', 'facous', 'frad', 'qq', 'qqr','fpoynt'
        do iy=1,ndep
           write (data_unit,'(I6,9(X,E13.5))') &
                iy, &
                fvisc_all(iy), fkin_all(iy), fconv_all(iy), fei_all(iy), facous_all(iy), &
                frad_all(iy), qqav_all(iy), qqrav_all(iy), fpoynt_all(iy)
        end do
        write (data_unit,'(A6,6(X,A13))') &
             'k', 'Bx', 'By', 'Bz', 'Ex', 'Ey', 'Ez'
        do iy=1,ndep
           write (data_unit,'(I6,6(X,E13.5))') &
                iy, &
                Bxav_all(iy), Byav_all(iy), Bzav_all(iy), &
                Exav_all(iy), Eyav_all(iy), Ezav_all(iy)
        end do       
     else
        write (data_unit,'(A6,8(X,A13))') &
             'k', 'fvisc', 'fkin', 'fconv', 'fei', 'facous', 'frad', 'qq', 'qqr'
        do iy=1,ndep
           write (data_unit,'(I6,8(X,E13.5))') &
                iy, &
                fvisc_all(iy), fkin_all(iy), fconv_all(iy), fei_all(iy), facous_all(iy), &
                frad_all(iy), qqav_all(iy), qqrav_all(iy)
        end do
     endif
     
  end if

  ! deallocate _all arrays
  deallocate( ym_all, ymdn_all )
  deallocate( rhoav_all, eav_all, lnrav_all, eeav_all, pav_all, ttav_all )
  deallocate( pxav_all, pyav_all, pzav_all, Uxav_all, Uyav_all, Uzav_all )
  deallocate( Exav_all, Eyav_all, Ezav_all, Bxav_all, Byav_all, Bzav_all )
  deallocate( pyavUp_all, pyavDn_all, ffpyavUp_all, ffpyavDn_all )
  deallocate( rhominh_all, rhomaxh_all, eeminh_all, eemaxh_all )
  deallocate( ttminh_all, ttmaxh_all, pminh_all, pmaxh_all )
  deallocate( fvisc_all, fkin_all, fconv_all, fei_all, facous_all )
  deallocate( fpoynt_all, frad_all, qqav_all, qqrav_all )
  deallocate( Pelav_all, Rossav_all, Plckav_all, Stdav_all )

  return
  
end subroutine output_hav

!...............................................................................
