!------------*************---------------------------------------------+-------
      MODULE opta_par_module

      IMPLICIT NONE
!
!--- Definition of Array Dimensions  -----------------------------------
!    mt = maximum number of temperatures in opacity table
!    mp = maximum number of pressures in opacity table
!    mfreq = maximum number of frequency points for opacity table
!            generation, including substeps
!    mband = maximum number of bands in opacity table
!
!      INTEGER, PARAMETER :: mt=61, mp=49
!      INTEGER, PARAMETER :: mt=181, mp=181
      INTEGER, PARAMETER :: mt=211, mp=211
      INTEGER, PARAMETER :: mfreq=120000, mband=20
!
!--- Definition of Array Dimensions finished  --------------------------
      END MODULE opta_par_module
!----------------*************-----------------------------------------+-------
