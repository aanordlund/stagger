!******************************************************************************
!
!     ####   #####   #####    ##
!    #    #  #    #    #     #  #
!    #    #  #    #    #    #    #
!    #    #  #####     #    ######
!    #    #  #         #    #    #
!     ####   #         #    #    # _dust
!
!    Opacity table routines
!
!******************************************************************************
! With contributions from
!   Matthias Steffen
!   Hans-Guenter Ludwig
!   Bernd Freytag
!   Susanne Hoefner
!******************************************************************************
! Modification history:
!   2009-07-17 (B.F.) Dust opacity tables
!******************************************************************************
! Modules:
!   kappadust_module: Dust opacity routine
!******************************************************************************
!
!------****************--------------------------------------------------------
module kappadust_module
!------------------------------------------------------------------------------
! NAME:
!   kappadust_module ('kappa_dust_module')
!
! PURPOSE:
!   Dust opacity routine
!
! CATEGORY:
!   Radiation transport,opacities
!
! CALLING SEQUENCE:
!   use kappad_module
!
! VARIABLES:
!   None
!
! ROUTINES: (contained)
!   kappadust: Compute Rosseland dust opacity in Small Particle Limit (SPL) in cm^2/g
!
! MODIFICATION HISTORY:
!   1992-08-31 (S.H.) Original file KAPPAD_SPL44.FOR
!   2004-06-04 (B.F.) Put into module
!   2008-06-17 (B.F.) Put into current version again
!------------------------------------------------------------------------------
implicit none
!
contains

subroutine kappadust(N, RHO, TRAD, DK3, OPADUS)
!------------------------------------------------------------------------------
! NAME:
!   kappadust ('kappa_dust')
!
! PURPOSE:
!   Computation of Rosseland dust opacity in Small Particle Limit (SPL) for N-dim vector
!
! CATEGORY:
!   Opacities
!
! CALLING SEQUENCE:
!   call kappadust(N, RHO, TRAD, DK3, R0D, OPADUS)
!
! INPUT:
!   N:      ('Number') integer with total number of (1D) grid points
!   RHO:    ('rho') density vector
!   TRAD:   ('Temperature_RADiation') vector with radiation temperature
!   DK3     ('D_K_3') vector with 3rd dust moment: K3
!
! OUTPUT:
!   OPADUS: ('OPAcity_DUSt') vector with dust opacity (SPL) in cm^2/g
!
! LOCAL VARIABLES
!   PI   =3.1415926535898  ; pi
!   PMASS=1.6726E-24       ; proton mass
!   AGW  =12.01115         ; atomic weight of dust forming material
!   RHOD =2.25             ; density of condensed material
!   R0D3                   : 3rd power of Monomer-Radius, R0D3=R0D**3
!
! ROUTINES:
!   None
!
! COMMENTS:
!   TXKAPD  : Identifikationstext 'KAPPAD_SPL   : Staubkappa im SPL, Q/a nach JMW94PC '
!
! MODIFICATION HISTORY:
!   1992-08-31 (Susanne Hoefner) Written, original file: KAPPAD_SPL44.FOR
!   1994-06-30 (S.H.) Wert fuer frequenzgemitteltes Q'(= Q/a) nach Winters (1994, priv.comm.)
!   2004-06-04 (B.F.) Put into module, Fortran90fied,
!              output of derivatives removed, output of TXKAPD removed
!              Input paramter: SENE -> TRAD,
!              INIT removed, R0D3 instead of R0D
!------------------------------------------------------------------------------
IMPLICIT NONE
!
! --- I/O parameters ---
!integer, parameter                     :: srk=kind(0.0E+00), drk=kind(0.0D+00)
integer, parameter                      :: rk=kind(0.0E+00)
integer,                   intent(in)   :: N
real (kind=rk),            intent(in)   :: RHO(N), TRAD(N), DK3(N)
real (kind=rk),            intent(out)  :: OPADUS(N)
!
! --- Local variables ---
integer                                 :: L
real (kind=rk)                          :: QEXTR, XKAPR
real (kind=rk), parameter               :: PI   = 3.1415926535898_rk, &
                                           PMASS=1.6726E-24_rk, &
                                           AGW  =12.01115_rk, &
                                           RHOD =2.25_rk, &
                                           R0D3 = 3.0_rk * AGW * PMASS / (4.0_rk * PI * RHOD), &
                                           H3V04= PI * R0D3
!                                           PIST = PI / 5.66961E-05_rk
!------------------------------------------------------------------------------
!
! === Dust kappa (Rosseland mean) ===
!
do L=1, N
  if (DK3(L) > 0.0_rk) then
    !
    ! --- Q' (Rosselandmittel) nach Winters (1994, priv. comm.) ---
    QEXTR    =4.4_rk * TRAD(L)
    !
    ! --- Staubkappa (Rosselandmittel) [1/cm] ---
    XKAPR    =H3V04 * DK3(L) * QEXTR
    !
    ! --- Staubkappa (Rosselandmittel) [cm^2/g] ---
    OPADUS(L)=XKAPR / RHO(L)
    !
  else
    !
    OPADUS(L)=0.0_rk
    !
  end if
end do ! L
!
end subroutine kappadust

end module kappadust_module
!
!------------*************---------------------------------------------+-------
!
!------**************----------------------------------------------------------
! module op_dust_module
! !------------------------------------------------------------------------------
! implicit none
! !
! contains
! 
! !-------------*****----------------------------------------------------+-------
!    SUBROUTINE op_dust(T, Plog, rho, quc, &
!                       nu, m1,n1, m2,n2, m3,n3, nquc, opclam)
! 
! ! --- This routine must be called in a scalar environment ---
! !
!   USE rhd_gl_module
!   USE rhd_prop_module
! !
!   IMPLICIT NONE
! !
! !--- Dummy arguments:
! !
!    INTEGER, INTENT(IN)    :: nu, m1,n1, m2,n2, m3,n3, nquc
! 
!    REAL, DIMENSION(m1:n1,m2:n2,m3:n3),        INTENT(IN)    :: T, Plog, rho
!    REAL, DIMENSION(m1:n1,m2:n2,m3:n3),        INTENT(INOUT) :: opclam
!    REAL, DIMENSION(m1:,m2:,m3:,1:),           INTENT(IN)    :: quc
! !
! !
! !--- Local scalar variables:
! !
!    REAL, PARAMETER :: PI=3.1415926536, SIGMA=5.6705E-5, C5=SIGMA/PI
!    INTEGER         :: i1, i2, i3, iquc
!    REAL, DIMENSION(:,:,:), ALLOCATABLE :: addata
! !
!    write(6,'(A)') 'prop%quc_prop=' // trim(prop%quc_prop) // &
!                   '  par%dustopamode=' // par%dustopamode
! !
!      ALLOCATE(addata(m1:n1,m2:n2,m3:n3))
! !
!    if (prop%quc_prop == 'dust_bins_01') then
!      ! --- Grey dust opacities from added geometrical cross section ---
!      ! --- Opacity of single grain is 2*pi*r_grain^2 (2*geometrical cross section) ---
!      !
!      !$OMP PARALLEL DEFAULT(NONE) &
!      !$OMP SHARED(m1,n1,m2,n2,m3,n3,par,prop,quc,addata)
!      !
!      ! --- Reset addata array  ---
!      !$OMP DO PRIVATE(i1,i2)
!      do i3=m3,n3
!        do i2=m2,n2
!          do i1=m1,n1
!            addata(i1,i2,i3)=0.0
!          end do ! i1
!        end do ! i2
!      end do ! i3
!      !$OMP END DO
!      !
!      ! --- Add bin opacities (use constant mass density per monomer) ---
!      !$OMP DO PRIVATE(i1,i2,iquc)
!      do i3=m3,n3
!        do iquc=2,prop%nquc
!          do i2=m2,n2
!            do i1=m1,n1
!              addata(i1,i2,i3)=addata(i1,i2,i3) + &
!                               1.5/(par%C_dust01*par%ar_dustGrainRadius(iquc)) * &
! !                              min(par%ar_dustGrainRadius(iquc)*1.0E+04, 1.0) * &
!                               min(max( par%ar_dustGrainRadius(iquc)/6.0E-03, &
!                                       (par%ar_dustGrainRadius(iquc)/6.0E-05)**3), 1.0) * &
!                               max(quc(i1,i2,i3,iquc), 0.0)
!            end do ! i1
!          end do ! i2
!        end do ! iquc
!      end do ! i3
!      !$OMP END DO
!      !
!      !$OMP END PARALLEL
!      !
!    else if (prop%quc_prop == 'dust_k3mon_03') then
!      ! --- Dust opacity [1/cm] (bin independent) ---
!      ! --- Opacity of single grain is 2*pi*r_grain^2 (2*geometrical cross section) ---
!      !
!      if (par%dustopamode == 'largeparticlelimit') then
!        ! --- Large particle limit ---
!        !
!        !$OMP PARALLEL DEFAULT(NONE) &
!        !$OMP SHARED(m1,n1,m2,n2,m3,n3,par,quc,addata)
!        !
!        !$OMP DO PRIVATE(i1,i2)
!        do i3=m3,n3
!          do i2=m2,n2
!            do i1=m1,n1
!              addata(i1,i2,i3)=1.5/(par%C_dust01*par%ar_dustGrainRadius(2)) * &
!                                 ( quc(i1,i2,i3,2)**2 * &
!                                  (quc(i1,i2,i3,1)+quc(i1,i2,i3,2)) )**(1.0/3.0)
!            end do ! i1
!          end do ! i2
!        end do ! i3
!        !$OMP END DO
!        !
!        !$OMP END PARALLEL
!      else if (par%dustopamode == 'dualslope01') then
!        ! --- Large particle limit ---
!        !
!        !$OMP PARALLEL DEFAULT(NONE) &
!        !$OMP SHARED(m1,n1,m2,n2,m3,n3,par,quc,addata)
!        !
!        !$OMP DO PRIVATE(i1,i2)
!        do i3=m3,n3
!          do i2=m2,n2
!            do i1=m1,n1
!              ! --- Particle radius ---
!              addata(i1,i2,i3)=par%ar_dustGrainRadius(2) * &
!                               ( quc(i1,i2,i3,2)         / &
!                                (quc(i1,i2,i3,1)+quc(i1,i2,i3,2)) )**(1.0/3.0)
!              ! --- Limiter function (fit by eye to Rosseland means of Mie opacities ---
!              ! ---                   of Forsterite; fit paramters in [cm])          ---
!              addata(i1,i2,i3)=min(max( addata(i1,i2,i3)/6.0E-03, &
!                                       (addata(i1,i2,i3)/6.0E-05)**3), 1.0)
!              ! --- Opacity ---
!              addata(i1,i2,i3)=1.5/(par%C_dust01*par%ar_dustGrainRadius(2)) * &
!                                 addata(i1,i2,i3) * &
!                                 ( quc(i1,i2,i3,2)**2 * &
!                                  (quc(i1,i2,i3,1)+quc(i1,i2,i3,2)) )**(1.0/3.0)
!            end do ! i1
!          end do ! i2
!        end do ! i3
!        !$OMP END DO
!        !
!        !$OMP END PARALLEL
!      else if (par%dustopamode == '') then
!        ! --- Old default (before 2008-10-31) ---
!        !
!        !$OMP PARALLEL DEFAULT(NONE) &
!        !$OMP SHARED(m1,n1,m2,n2,m3,n3,par,quc,addata)
!        !
!        !$OMP DO PRIVATE(i1,i2)
!        do i3=m3,n3
!          do i2=m2,n2
!            do i1=m1,n1
!              addata(i1,i2,i3)=1.5/(par%C_dust01*par%ar_dustGrainRadius(2)) * &
!                                 min(par%ar_dustGrainRadius(2)*1.0E+04, 1.0) * &
!                                 ( quc(i1,i2,i3,2)**2 * &
!                                  (quc(i1,i2,i3,1)+quc(i1,i2,i3,2)) )**(1.0/3.0)
!            end do ! i1
!          end do ! i2
!        end do ! i3
!        !$OMP END DO
!        !
!        !$OMP END PARALLEL
!      else
!        stop 'Unknown dustopamode'
!      endif
!    else
!      ! --- dust opacity [1/cm] (bin independent) ---
!      !
!      !$OMP PARALLEL DEFAULT(NONE) &
!      !$OMP SHARED(m3,n3,par,rho,quc,addata)
!      !
!      !$OMP DO
!      do i3=m3,n3
!        addata(:,:,i3) = (quc(:,:,i3,1)*par%C_dust02)/ &
!                         (quc(:,:,i3,1)+quc(:,:,i3,2)+rho(:,:,i3))
!        addata(:,:,i3) = par%C_dust05*addata(:,:,i3)**(2.0/3.0) * & 
!                         ((quc(:,:,i3,1)+quc(:,:,i3,2))/par%C_dust02)
!      end do ! i3
!      !$OMP END DO
!      !
!      !$OMP END PARALLEL
!      !
! !!$  do i3=m3,n3
! !!$    do i2=1,1
! !!$      do i1=1,1
! !!$        write(6,'(A,3I4,6(1X, 1PE11.4))') 'i123,quc1,quc2,dustopac=', &
! !!$          i1,i2,i3, quc(i1,i2,i3,1), quc(i1,i2,i3,2), addata(i1,i2,i3)
! !!$      end do ! i1
! !!$    end do ! i2
! !!$  end do ! i3
!    endif
! !
!    !$OMP PARALLEL DEFAULT(NONE) &
!    !$OMP SHARED(m1,n1,m2,n2,m3,n3,opclam,addata)
! !
!    !$OMP DO PRIVATE(i1,i2)
!    do i3=m3,n3
!      do i2=m2,n2
!        do i1=m1,n1
!          opclam(i1,i2,i3) = opclam(i1,i2,i3) + addata(i1,i2,i3)
!        end do ! i1
!      end do ! i2
!    end do ! i3
!    !$OMP END DO
! !
!    !$OMP END PARALLEL
! !
!    DEALLOCATE(addata)
! !
!    END SUBROUTINE op_dust
! !-----------------*****------------------------------------------------+-------
! 
!       END MODULE op_dust_module
! !----------------*************-----------------------------------------+-------
