!******************************************************************************
!
!     ####   #####   #####    ##
!    #    #  #    #    #     #  #
!    #    #  #    #    #    #    #
!    #    #  #####     #    ######
!    #    #  #         #    #    #
!     ####   #         #    #    # _routines
!
!    Opacity table routines
!
!******************************************************************************
! With contributions from
!   Matthias Steffen
!   Hans-Guenter Ludwig
!   Bernd Freytag
!   Susanne Hoefner
!******************************************************************************
! Modification history:
!   1991-01-21 (HGL) First version
!   2002-12-05 (M.S.) F90
!   2003-02-26 (B.F.)
!   2004-06-04 (S.H, B.F.) Dust opacities
!   ... Further updates
!   2008-06-17 (B.F. Uppsala) Merged with old version that contained kappadust
!   2008-08-12 (B.F., M.S.) Merge (to have kappadust and Tinci, blinci)
!   2008-10-10 (B.F.) Re-merge
!   2009-01-03 (M.S.) Allocatable array addata introduced for storing 
!                     additional external opacities (CO, dust)
!   2009-07-17 (B.F.) Dust opacity tables
!   2009-10-21 (B.F.) Check for too large nP and nT
!******************************************************************************
! Modules:
!   xopta_def:        Contains the opacity table data read by DFOPTA, used by XKAROS
!   xoptad_def:       Contains the dust opacity table data read by DFOPTADUST, used by XKAROSDUST
!   xopta1_def:       Contains the interpolation coefficients for wtbbd
!   opta_module:      Opacity table routines
!   co_module:        Carbon Monoxide (CO) opacity routines
!******************************************************************************


!------------*********-------------------------------------------------+-------
      MODULE xopta_def
!
      USE opta_par_module
!
      IMPLICIT NONE
      SAVE
!
!**   XOPTA contains the opacity table data read by DFOPTA
      INTEGER :: IDVERS, IDMEAN, NT, NTBN, NP, NPBN, NBAND, NSTEPX, &
                 NULOX, NUHIX, NUMNUX, INTMDX
      INTEGER, DIMENSION(MT)            :: IDXTBN
      INTEGER, DIMENSION(MP)            :: IDXPBN
      INTEGER, DIMENSION(MFREQ)         :: INBAND

      REAL,    DIMENSION(MT)            :: TABT, TABTBN, TABDTB
      REAL,    DIMENSION(MP)            :: TABP, TABPBN, TABDPB
      REAL,    DIMENSION(MT,0:MBAND)    :: WTBBD
      REAL,    DIMENSION(MT,MP,0:MBAND) :: TABKAP
!
      END MODULE xopta_def
!----------------*********---------------------------------------------+-------


!------------**********----------------------------------------------------------------------------
      MODULE xoptad_def
!--------------------------------------------------------------------------------------------------
! NAME:
!   x_optad_def ('x_opacity_table_dust_definition')
!
! PURPOSE:
!   Store dust opacity tables.
!
! MODIFICATION HISTORY:
!   2009-07-17 (B.F.) Derived from xopta_def
!--------------------------------------------------------------------------------------------------
USE opta_par_module
!
IMPLICIT NONE
!
type optad_type
  INTEGER                               :: IDVERS, IDMEAN, NT, NTBN, NR, NRBN, NBAND, NSTEPX, &
                                           NULOX, NUHIX, NUMNUX, INTMDX
  INTEGER, DIMENSION(MT)                :: IDXTBN
  INTEGER, DIMENSION(MP)                :: IDXRBN
  INTEGER, DIMENSION(MFREQ)             :: INBAND
  REAL,    DIMENSION(MT)                :: TABT, TABTBN, TABDTB
  REAL,    DIMENSION(MP)                :: TABR, TABRBN, TABDRB
  REAL,    DIMENSION(MT,0:MBAND)        :: WTBBD
  REAL,    DIMENSION(MT,MP,0:MBAND)     :: TABKAP
end type optad_type
!
type(optad_type), save                  :: optad
!
      END MODULE xoptad_def
!----------------**********------------------------------------------------------------------------


!------------**********------------------------------------------------+-------
      MODULE xopta1_def
!
      USE opta_par_module
!
      IMPLICIT NONE
      SAVE
!
!     --- xopta1 contains the interpolation coefficients for wtbbd ---
      REAL, DIMENSION(4,MT,0:MBAND) ::  wtbbdc
!
      END MODULE xopta1_def
!----------------**********--------------------------------------------+-------


!------------*********-------------------------------------------------+-------
      MODULE opta_chem
!
!
      IMPLICIT NONE
      SAVE
!
      INTEGER                              :: ispn, ispco
      INTEGER, DIMENSION(:), ALLOCATABLE   :: ispc, nspcmult
!
      END MODULE opta_chem
!----------------*********---------------------------------------------+-------

!------------***********-----------------------------------------------+-------
      MODULE opta_module
!*******************************************************************************
! Radiation Transport Routines
!*******************************************************************************
! SUBROUTINES:
!   cubitc:      Performs an 1D-interpolation using cubic polynomials
!                and gives out the interpolation coefficients.
!   dfopta:      Reads opacity table data and stores it in common blocks
!                XOPTA, xopta1, (and XOPTAN) (character data).
!   opta_info:   Gives stored information about current opacity table
!   opta_modify: Manipulate opacity data
!   xkaros0:     Computes opacities for given pressure P, temperature T
!                and frequency bin IBAND. Scalar version (0D)
!   xkaros1:     Computes opacities for given pressure P, temperature T
!                and frequency bin IBAND. Vector version (1D)
!   xkaros2:     Computes opacities for given pressure P, temperature T
!                and frequency bin IBAND. Array version  (2D)
!   xkaros3:     Computes opacities for given pressure P, temperature T
!                and frequency bin IBAND. Array version  (3D)
!   xbnu0:       Compute the fraction of the Planck function B_nu 
!                Scalar version.
!   xbnu1:       Compute the fraction of the Planck function B_nu 
!                Vector version.
!   xbnu2:       Compute the fraction of the Planck function B_nu 
!                Array version (2D).
!   xbnu3:       Compute the fraction of the Planck function B_nu 
!                Array version (3D).
!   xbnuwT0:     Compute the fraction of the Planck function B_nu 
!                and the fraction of the temperature derivative dB_nu/dT. 
!                Scalar version.
!   xbnuwT1:     Compute the fraction of the Planck function B_nu 
!                and the fraction of the temperature derivative dB_nu/dT. 
!                Vector version.
!   xbnuwT2:     Compute the fraction of the Planck function B_nu 
!                and the fraction of the temperature derivative dB_nu/dT. 
!                Array version (2D).
!   xbnuwT3:     Compute the fraction of the Planck function B_nu 
!                and the fraction of the temperature derivative dB_nu/dT. 
!                Array version (3D).
!*******************************************************************************
      ! --- 20-Mar-1998 16:36:07: (B.F.) Interface xkaros introduced ---
      !     26.11.00 (B.F. Uppsala)
      !     20. 2.01 xkaros3, xkaros_logPT2kappa
      !
      interface xkaros
        module procedure xkaros0, xkaros1, xkaros2, xkaros3
      end interface ! xkaros
      !
      interface sub_xkaros_logPT2kappa
        module procedure sub_xkaros2_logPT2kappa, sub_xkaros3_logPT2kappa
      end interface ! sub_xkaros_logPT2kappa
      !
      interface sub_xkaros_logPT2rhokap
        module procedure sub_xkaros2_logPT2rhokap, sub_xkaros3_logPT2rhokap
      end interface ! sub_xkaros_logPT2rhokap
      !
      interface xkaros_logPT2kappa
        module procedure xkaros1_logPT2kappa, xkaros2_logPT2kappa, &
                         xkaros3_logPT2kappa
      end interface ! xkaros_logPT2kappa
      !
      interface xkaros_logPT42kappa
        module procedure xkaros1_logPT42kappa
      end interface ! xkaros_logPT42kappa
      !
      interface xbnu
        module procedure xbnu0, xbnu1, xbnu2, xbnu3
      end interface ! xbnu
      !
      interface xbnuwT
        module procedure xbnuwT0, xbnuwT1, xbnuwT2, xbnuwT3
      end interface ! xbnuwT

      CONTAINS

!----------------******------------------------------------------------+-------
      SUBROUTINE CUBITc(XX, YY, NXY, INTMOD, I, X, Y, YS, a, b, c, d)
!----------------------------------------------------------------------+-------
!
! Version: 16-DEC-1990 17:33:09 HGL
!          30-May-1997 13:25:13 (B.F.) cubit -> cubitc
!          26.11.00 (B.F. Uppsala) cubitc and cubitic in separate subroutines
!                                  ENTRY statements outcommented
!
! Author : HGL
!
! Description: CUBITc performs an 1D-interpolation using cubic polynomials.
!   It is an extension of cubit: the interpolation coefficients are given out.
!   The following scheme shows the adopted model:
!      
!     YY
!     ^
!     |
!     |
!     |                                                            +
!     |
!     |                        +                  +
!     |                             +
!   Y +                                *    +
!     |        +
!     |    +
!     |
!     |----|---|-----/.../-----|----|--+----|-----|-----/.../------|----> XX
!          1   2              I-1   I      I+1   I+2              NXY
!                                   |  X    |
!
!   NXY (NXY >= 3!) data points Y(X) are given by the arrays XX and YY. 
!   The XX-values must be ordered monotonically ( For all i: X(i+1)>X(i), 
!   or X(i+1) < X(i)). CUBIT works in the interval specified by i 
!   (1 <= i <= NXY-1). The function Y(X) described by XX and YY is
!   approximated in interval i by a third order polynomial of the form
!
!   Y_i(X) = A_i*(X-X_i)**3 + B_i*(X-X_i)**2 + C_i*(X-X_i) + D_i .
!
!   The coefficients are determined by the values Y_i and Y_(i+1) at the 
!   interval boundaries and the derivatives YS_i and YS_(i+1). These
!   derivatives are computed in various ways for different values of the
!   parameter INTMOD. Y and YS give the value resp. the derivative of the 
!   interpolating polynomial at point X. X may lie outside interval i.
!   For every value of INTMOD the slope at the boundaries of the XX-range
!   is set to the slope of the secant through the nearest neighbour point.
!   There are two more ENTRY points in this subroutine. CUBITI is used when
!   the interval index I is already known. In this case it has to be specified
!   and is not determined in the subroutine. CUBITP is used when the same 
!   interpolating polynomial as in the last call should be regarded.
!
!   ENTRY CUBITIc( XX, YY, NXY, INTMOD, I, X, Y, YS, a, b, c, d)
!   ENTRY CUBITPc( X, Y, YS, a, b, c, d)
!
!   For a detailed treatment of the problem see
!   M. Steffen: "A simple method for monotonic interpolation in one dimension"
!                Astron. & Astrophys., vol. 239, pp. 443-450, 1990
!              
!
! Arguments, access, type, dimension, description:  (Entry CUBIT!)
!   XX     R R1: X-values of data points, ordered!
!   YY     R R1: Y-values of data points
!   NXY    R I : Number of data points, NXY>=3!
!   INTMOD R I  : Mode of interpolation:
!                 INTMOD = 1: Derivatives at the interval boundaries are given
!                             by the slope of the secant through the 
!                             neighbouring points.
!                 INTMOD = 2: Derivatives at the interval boundaries are given
!                             by a the slope of a parabola through the boundary
!                             point and its neighbours. If the XX-values are
!                             equidistant INTMOD=1 and INTMOD=2 work identical.
!                 INTMOD = 3: The derivatives of INTMOD = 2 are modified to
!                             assure monotonic behaviour of the interpolating
!                             polynomial in interval i.
!   X      R R : X-value for which the interpolating polynomial is evaluated
!   Y      W R : Value of interpolating polynomial at point X
!   YS     W R : Derivative of interpolating polynomial at point X
!   I      W I : Index of interval for which the polynomial was computed.
!                See CUBITI! (for cubiti: access=R/W)
!   a      w r : Interpolation coefficient
!   b      w r : Interpolation coefficient
!   c      w r : Interpolation coefficient
!   d      w r : Interpolation coefficient
!
! Common blocks (access): None
!
! External procedures   : None
!
! File units (access)   : None
!
! Comments: In order to minimize the number of if-then-else blocks a kind of
!   spaghetti coding is adopted. So some piece of code may appear multiple
!   times. For the same reason this version is restricted to NXY >=3. 
!   Otherwise it would be necessary to regard separately the case NXY=2.
!
! Legend:
!   Access: (R)ead only, (W)rite only, (*)read/write
!   Type  : (C)omplex, (D)ouble precision, (E)xternal, (I)nteger, 
!           (R)eal, (S)tring, (*)label
!
!----------------------------------------------------------------------+-------
!
      IMPLICIT NONE
!
!--- Dummy arguments:
      INTEGER,       INTENT(IN)        :: NXY, INTMOD, I
      REAL, DIMENSION(NXY), INTENT(IN) :: XX, YY
      REAL,          INTENT(IN)        :: X
      REAL,          INTENT(OUT)       :: Y, YS, a, b, c, d
!
!--- Local Variables ---
      INTEGER    :: IM1, IP1, IP2
      REAL       :: DX, HI, HIM1, HIP1, SI, SIM1, SIP1, YSI, YSIP1, YYI
      REAL, SAVE :: AI, BI, CI, DI, ASI, BSI, CSI, XXI
      EQUIVALENCE ( CI, CSI, YSI), ( DI, YYI)
      DATA AI , BI , CI , DI , ASI, BSI, XXI / 0.0,0.0,0.0,0.0,0.0,0.0,0.0 /
!----------------------------------------------------------------------+-------
!
!     --- The interval index I is known. ---
!      ENTRY CUBITIc(XX, YY, NXY, INTMOD, I, X, Y, YS, a, b, c, d)

!     --- Set auxiliary parameters ---
30    IM1 = I - 1
      IP1 = I + 1
      IP2 = I + 2
!     --- Set parameters for all cases ---
      XXI = XX( I)
      YYI = YY( I)
      HI  = XX( IP1) - XXI
      SI  = ( YY( IP1) - YYI) / HI

      if (I .EQ. 1) THEN
!       --- Interpolation in first interval ---
        HIM1 = HI
        SIM1 = SI
        HIP1 = XX( IP2) - XX( IP1)
        SIP1 = ( YY( IP2) - YY( IP1)) / HIP1
      ELSEif (I .EQ. NXY-1) THEN
!       --- Interpolation in last interval ---
        HIM1 = XXI - XX( IM1)
        SIM1 = ( YYI - YY( IM1)) / HIM1
        HIP1 = HI
        SIP1 = SI
      ELSE
!       --- Interpolation in inner interval ---
        HIM1 = XXI - XX( IM1)
        SIM1 = ( YYI - YY( IM1)) / HIM1
        HIP1 = XX( IP2) - XX( IP1)
        SIP1 = ( YY( IP2) - YY( IP1)) / HIP1
      ENDIF

      if (INTMOD .EQ. 3) THEN
!       --- Monotonic interpolation ---
!
        YSI   = ( SIM1 * HI   + SI   * HIM1) / ( HIM1 + HI  )
        YSIP1 = ( SI   * HIP1 + SIP1 * HI  ) / ( HI   + HIP1)
!       --- So far the same like in the case INTMOD = 2, but now the ---
!       --- magic statements come ...                                ---
        YSI   = ( SIGN( 1.0, SIM1) + SIGN( 1.0, SI  )) * &
                MIN( ABS( SIM1), ABS( SI  ), 0.5*ABS( YSI  ))
        YSIP1 = ( SIGN( 1.0, SI  ) + SIGN( 1.0, SIP1)) * &
                MIN( ABS( SI  ), ABS( SIP1), 0.5*ABS( YSIP1))
!-------------------------------------------------------------------------------
      ELSEif (INTMOD .EQ. 2) THEN
!       --- Derivatives given by parabolas ---
!
        YSI   = ( SIM1 * HI   + SI   * HIM1) / ( HIM1 + HI  )
        YSIP1 = ( SI   * HIP1 + SIP1 * HI  ) / ( HI   + HIP1)
!-------------------------------------------------------------------------------
      ELSE
!       --- Derivatives given by slopes of secants through neighbouring points -
!
        YSI   = ( SI * HI + SIM1 * HIM1) / ( HI + HIM1)
        YSIP1 = ( SI * HI + SIP1 * HIP1) / ( HI + HIP1)
!-----------------------------------------------------------------------
      ENDIF

!     --- Compute interpolation coefficients ---
!     --- Polynomial: ---
      AI = ( YSI + YSIP1 - 2.0 * SI) / ( HI * HI)
      BI = ( 3.0 * SI - 2.0 * YSI - YSIP1) / HI
!     --- The assignments CI = YSI and DI = YYI are missing since they are done 
!     --- by an EQUIVALENCE statement.
!     --- Derivative of the polynomial: ---
      ASI = 3.0 * AI
      BSI = BI + BI
!     --- The assignment CSI = CI is done by EQUIVALENCE. ---

!     --- The interpolating polynomial is known. ---
!      ENTRY CUBITPc(X, Y, YS, a, b, c, d)

!     --- Evaluate polynomials ---
      DX = X - XXI
      Y  = DX * ( DX * ( DX * AI + BI) + CI) + DI
      YS = DX * ( DX * ASI + BSI) + CSI
!
!     --- Output of interpolation coefficients ---
      a=ai
      b=bi
      c=ci
      d=di
!
      END SUBROUTINE CUBITC
!--------------------******--------------------------------------------+-------


!----------------*******-----------------------------------------------+-------
      SUBROUTINE CUBITIc(XX, YY, NXY, INTMOD, I, X, Y, YS, a, b, c, d)
!----------------------------------------------------------------------+-------
!
! Version: 16-DEC-1990 17:33:09 HGL
!          30-May-1997 13:25:13 (B.F.) cubit -> cubitc
!          26.11.00 (B.F. Uppsala) cubitc and cubitic in separate subroutines
!                                  ENTRY statements outcommented
!
! Author : HGL
!
! Description: CUBITc performs an 1D-interpolation using cubic polynomials.
!   It is an extension of cubit: the interpolation coefficients are given out.
!   The following scheme shows the adopted model:
!      
!     YY
!     ^
!     |
!     |
!     |                                                            +
!     |
!     |                        +                  +
!     |                             +
!   Y +                                *    +
!     |        +
!     |    +
!     |
!     |----|---|-----/.../-----|----|--+----|-----|-----/.../------|----> XX
!          1   2              I-1   I      I+1   I+2              NXY
!                                   |  X    |
!
!   NXY (NXY >= 3!) data points Y(X) are given by the arrays XX and YY. 
!   The XX-values must be ordered monotonically ( For all i: X(i+1)>X(i), 
!   or X(i+1) < X(i)). CUBIT works in the interval specified by i 
!   (1 <= i <= NXY-1). The function Y(X) described by XX and YY is
!   approximated in interval i by a third order polynomial of the form
!
!   Y_i(X) = A_i*(X-X_i)**3 + B_i*(X-X_i)**2 + C_i*(X-X_i) + D_i .
!
!   The coefficients are determined by the values Y_i and Y_(i+1) at the 
!   interval boundaries and the derivatives YS_i and YS_(i+1). These
!   derivatives are computed in various ways for different values of the
!   parameter INTMOD. Y and YS give the value resp. the derivative of the 
!   interpolating polynomial at point X. X may lie outside interval i.
!   For every value of INTMOD the slope at the boundaries of the XX-range
!   is set to the slope of the secant through the nearest neighbour point.
!   There are two more ENTRY points in this subroutine. CUBITI is used when
!   the interval index I is already known. In this case it has to be specified
!   and is not determined in the subroutine. CUBITP is used when the same 
!   interpolating polynomial as in the last call should be regarded.
!
!   ENTRY CUBITIc( XX, YY, NXY, INTMOD, I, X, Y, YS, a, b, c, d)
!   ENTRY CUBITPc( X, Y, YS, a, b, c, d)
!
!   For a detailed treatment of the problem see
!   M. Steffen: "A simple method for monotonic interpolation in one dimension"
!                Astron. & Astrophys., vol. 239, pp. 443-450, 1990
!              
!
! Arguments, access, type, dimension, description:  (Entry CUBIT!)
!   XX     R R1: X-values of data points, ordered!
!   YY     R R1: Y-values of data points
!   NXY    R I : Number of data points, NXY>=3!
!   INTMOD R I  : Mode of interpolation:
!                 INTMOD = 1: Derivatives at the interval boundaries are given
!                             by the slope of the secant through the 
!                             neighbouring points.
!                 INTMOD = 2: Derivatives at the interval boundaries are given
!                             by a the slope of a parabola through the boundary
!                             point and its neighbours. If the XX-values are
!                             equidistant INTMOD=1 and INTMOD=2 work identical.
!                 INTMOD = 3: The derivatives of INTMOD = 2 are modified to
!                             assure monotonic behaviour of the interpolating
!                             polynomial in interval i.
!   X      R R : X-value for which the interpolating polynomial is evaluated
!   Y      W R : Value of interpolating polynomial at point X
!   YS     W R : Derivative of interpolating polynomial at point X
!   I      W I : Index of interval for which the polynomial was computed.
!                See CUBITI! (for cubiti: access=R/W)
!   a      w r : Interpolation coefficient
!   b      w r : Interpolation coefficient
!   c      w r : Interpolation coefficient
!   d      w r : Interpolation coefficient
!
! Common blocks (access): None
!
! External procedures   : None
!
! File units (access)   : None
!
! Comments: In order to minimize the number of if-then-else blocks a kind of
!   spaghetti coding is adopted. So some piece of code may appear multiple
!   times. For the same reason this version is restricted to NXY >=3. 
!   Otherwise it would be necessary to regard separately the case NXY=2.
!
! Legend:
!   Access: (R)ead only, (W)rite only, (*)read/write
!   Type  : (C)omplex, (D)ouble precision, (E)xternal, (I)nteger, 
!           (R)eal, (S)tring, (*)label
!
!----------------------------------------------------------------------+-------
!
      IMPLICIT NONE
!
!--- Dummy arguments:
      INTEGER,       INTENT(IN)        :: NXY, INTMOD
      INTEGER,       INTENT(OUT)       :: I
      REAL, DIMENSION(NXY), INTENT(IN) :: XX, YY
      REAL,          INTENT(IN)        :: X
      REAL,          INTENT(OUT)       :: Y, YS, a, b, c, d
!
!--- Local Variables ---
      INTEGER    :: IM1, IP1, IP2
      REAL       :: DX, HI, HIM1, HIP1, SI, SIM1, SIP1, YSI, YSIP1, YYI
      REAL, SAVE :: AI, BI, CI, DI, ASI, BSI, CSI, XXI
      EQUIVALENCE ( CI, CSI, YSI), ( DI, YYI)
      DATA AI , BI , CI , DI , ASI, BSI, XXI / 0.0,0.0,0.0,0.0,0.0,0.0,0.0 /
!
!----------------------------------------------------------------------+-------
!
!     --- Determine interval index ---
      if (XX( 1) .LT. XX(NXY)) THEN
!       --- XX ordered low - up ---
        DO 10 I = NXY-1, 2, -1
          if (X .GE. XX(I)) GOTO 30
10      CONTINUE
!       --- If I is not 2 one has to use I=1 anyway. The DO-loop is left with
!       --- I=1 if the GOTO statement was not executed and the compiler works
!       --- really according the FORTRAN 77 standard! But to guarantee this in
!       --- any case ...
        I = 1
      ELSE
!       --- XX ordered up - low ---
        DO 20 I = NXY-1, 2, -1
          if (X .LE. XX(I)) GOTO 30
20      CONTINUE
!       --- See comment in THEN-block ---
        I = 1
      ENDIF

!     --- The interval index I is known. ---
!      ENTRY CUBITIc(XX, YY, NXY, INTMOD, I, X, Y, YS, a, b, c, d)

!     --- Set auxiliary parameters ---
30    IM1 = I - 1
      IP1 = I + 1
      IP2 = I + 2
!     --- Set parameters for all cases ---
      XXI = XX( I)
      YYI = YY( I)
      HI  = XX( IP1) - XXI
      SI  = ( YY( IP1) - YYI) / HI

      if (I .EQ. 1) THEN
!       --- Interpolation in first interval ---
        HIM1 = HI
        SIM1 = SI
        HIP1 = XX( IP2) - XX( IP1)
        SIP1 = ( YY( IP2) - YY( IP1)) / HIP1
      ELSEif (I .EQ. NXY-1) THEN
!       --- Interpolation in last interval ---
        HIM1 = XXI - XX( IM1)
        SIM1 = ( YYI - YY( IM1)) / HIM1
        HIP1 = HI
        SIP1 = SI
      ELSE
!       --- Interpolation in inner interval ---
        HIM1 = XXI - XX( IM1)
        SIM1 = ( YYI - YY( IM1)) / HIM1
        HIP1 = XX( IP2) - XX( IP1)
        SIP1 = ( YY( IP2) - YY( IP1)) / HIP1
      ENDIF

      if (INTMOD .EQ. 3) THEN
!       --- Monotonic interpolation ---
!
        YSI   = ( SIM1 * HI   + SI   * HIM1) / ( HIM1 + HI  )
        YSIP1 = ( SI   * HIP1 + SIP1 * HI  ) / ( HI   + HIP1)
!       --- So far the same like in the case INTMOD = 2, but now the ---
!       --- magic statements come ...                                ---
        YSI   = ( SIGN( 1.0, SIM1) + SIGN( 1.0, SI  )) * &
                MIN( ABS( SIM1), ABS( SI  ), 0.5*ABS( YSI  ))
        YSIP1 = ( SIGN( 1.0, SI  ) + SIGN( 1.0, SIP1)) * &
                MIN( ABS( SI  ), ABS( SIP1), 0.5*ABS( YSIP1))
!-------------------------------------------------------------------------------
      ELSEif (INTMOD .EQ. 2) THEN
!       --- Derivatives given by parabolas ---
!
        YSI   = ( SIM1 * HI   + SI   * HIM1) / ( HIM1 + HI  )
        YSIP1 = ( SI   * HIP1 + SIP1 * HI  ) / ( HI   + HIP1)
!-------------------------------------------------------------------------------
      ELSE
!       --- Derivatives given by slopes of secants through neighbouring points -
!
        YSI   = ( SI * HI + SIM1 * HIM1) / ( HI + HIM1)
        YSIP1 = ( SI * HI + SIP1 * HIP1) / ( HI + HIP1)
!-----------------------------------------------------------------------
      ENDIF

!     --- Compute interpolation coefficients ---
!     --- Polynomial: ---
      AI = ( YSI + YSIP1 - 2.0 * SI) / ( HI * HI)
      BI = ( 3.0 * SI - 2.0 * YSI - YSIP1) / HI
!     --- The assignments CI = YSI and DI = YYI are missing since they are done 
!     --- by an EQUIVALENCE statement.
!     --- Derivative of the polynomial: ---
      ASI = 3.0 * AI
      BSI = BI + BI
!     --- The assignment CSI = CI is done by EQUIVALENCE. ---

!     --- The interpolating polynomial is known. ---
!      ENTRY CUBITPc(X, Y, YS, a, b, c, d)

!     --- Evaluate polynomials ---
      DX = X - XXI
      Y  = DX * ( DX * ( DX * AI + BI) + CI) + DI
      YS = DX * ( DX * ASI + BSI) + CSI
!
!     --- Output of interpolation coefficients ---
      a=ai
      b=bi
      c=ci
      d=di
!
      END SUBROUTINE CUBITIC
!--------------------*******-------------------------------------------+-------


!----------------******------------------------------------------------+-------
      SUBROUTINE DFOPTA(MESLEV, OUTUNT, KPXUNT, NBDUM)
!----------------------------------------------------------------------+-------
!
! Version: 23. 2.93 Written by HGL
!          30. 5.97 (B.F.) Compute wtbbdc in xopta1
!           2.10.97 (M.S.) Additional dummy argument NBDUM (=NBAND)
!          28.01.02 (M.S.) call cubitic --> call cubitc
!          21.10.09 (B.F.) Check for too large nP and nT
!
! Description: DFOPTA reads opacity table data and stores it in common blocks
!   XOPTA, xopta1, (and XOPTAN) (character data).
!   It distinguishes 3 different entities: Heading information, comments and
!   numerical data. Heading information is copied to the output ( MESLEV<=2),
!   comments are ignored. The order of variables read has to correspond with
!   the order of writing in KAPPAX! The meslev parameter determines the 
!   messages which DFOPTA issues:
!   MESLEV = 0 : All messages, journaling the data read
!          = 1 : No journaling
!          = 2 : No detailed information which variable was read
!          = 3 : Warnings and heading information only
!          = 4 : No output
!   IDVERS should be read as the first variable. It identifies the opacity 
!   table data structure; not all variables are available in older file
!   versions.
!
! Arguments, access, type, dimension, description:
!   MESLEV R I : Message level
!   OUTUNT R I : Unit to which messages were written
!   KPXUNT R I : Unit from which opacity table data wre read
!
! Common blocks (access): XOPTA (W), xopta1 (w), (XOPTAN (W))
!
! External procedures   : None
!
! File units (access)   : KPXUNT (R), OUTUNT (W)
!
!
! Comments: In this version the subroutine's parameter list was modified!
!           12.12.91 B_nu fraction normalization included,
!                    defaults corrected
!           29. 5.97 (B.F.) TABDTB(1:4), TABDPB(1:4) initialized to
!                    non-zero values
!           30. 5.97 (B.F.) 
!
! Legend:
!   Access: (R)ead only, (W)rite only, (*)read/write
!   Type  : (C)omplex, (D)ouble precision, (E)xternal, (I)nteger, 
!           (R)eal, (S)tring, (*)label
!
!----------------------------------------------------------------------+-------
!
!**   XOPTA contains the opacity table data read by DFOPTA
      USE xopta_def
!     --- xopta1 contains the interpolation coefficients for wtbbd ---
      USE xopta1_def
!
      IMPLICIT NONE
!
!--- Dummy arguments:
      INTEGER,       INTENT(IN)        :: MESLEV, OUTUNT, KPXUNT
      INTEGER,       INTENT(OUT)       :: NBDUM
!
!--- Local Variables ---
      INTEGER    :: I, I1, I2, INTMOD, IBAND, IIBAND, I0BAND, ISTEP, &
                    IP, IIP, IT, NU
      REAL       :: DUMMY, WTB, WTDBDT
!
!
      CHARACTER(LEN=30) :: OPTANA
      CHARACTER(LEN=20) :: DATEX
      CHARACTER(LEN=72) :: CARD
      LOGICAL           :: ISHEAD
      LOGICAL           :: RDIDVS, RDOTN, RDATEX, RDIDMN, RDNT, RDNTBN, &
                           RDNP, RDNPBN, RDNBND, RDNSTP, RDNULO, RDNUHI, &
                           RDNUMN, RDINTM, RDTABT, RDTBN, RDITBN, RDDTB, &
                           RDTABP, RDPBN, RDIPBN, RDDPB, RDTBKP, RDWTB, RDINBD

!----------------------------------------------------------------------+-------
!**   Set initial values
      ISHEAD = .FALSE.
      RDIDVS = .FALSE.
      RDOTN  = .FALSE.
      RDATEX = .FALSE.
      RDIDMN = .FALSE.
      RDNT   = .FALSE.
      RDNTBN = .FALSE.
      RDNP   = .FALSE.
      RDNPBN = .FALSE.
      RDNBND = .FALSE.
      RDNSTP = .FALSE.
      RDNULO = .FALSE.
      RDNUHI = .FALSE.
      RDNUMN = .FALSE.
      RDINTM = .FALSE.
      RDTABT = .FALSE.
      RDTBN  = .FALSE.
      RDITBN = .FALSE.
      RDDTB  = .FALSE.
      RDTABP = .FALSE.
      RDPBN  = .FALSE.
      RDIPBN = .FALSE.
      RDDPB  = .FALSE.
      RDTBKP = .FALSE.
      RDWTB  = .FALSE.
      RDINBD = .FALSE.
!**   Parameters of old TABT, TABP structure
!**   Temperatures
      NT     = 36
      NTBN   = 4
      TABTBN(1) = 3.5
      TABTBN(2) = 4.0
      TABTBN(3) = 4.5
      TABTBN(4) = 5.0
      IDXTBN(1) = 1
      IDXTBN(2) = 21
      IDXTBN(3) = 31
      IDXTBN(4) = 36
!
! --- 29. 5.97 ---
      TABDTB(1) = 0.025
      TABDTB(2) = 0.05
      TABDTB(3) = 0.05
      TABDTB(4) = 0.05
! ---          ---
!
!**   Pressures
      NP     = 30
      NPBN   = 4
      TABPBN(1) = -2.0
      TABPBN(2) = 1.0
      TABPBN(3) = 3.0
      TABPBN(4) = 6.0
      IDXPBN(1) = 1
      IDXPBN(2) = 7
      IDXPBN(3) = 15
      IDXPBN(4) = 30
      TABDPB(1) = 0.5
      TABDPB(2) = 0.25
!
! --- 29. 5.97 ---
      TABDPB(3) = 0.2
      TABDPB(4) = 0.1
! ---          ---
!
!**   Default interpolation mode!
      INTMDX = 2
!----------------------------------------------------------------------+-------

      REWIND( KPXUNT)

!**   New page
      if (MESLEV .LE. 2) WRITE( OUTUNT, '(1H1)')

!**   DFOPTA greetings
      if (MESLEV .LE. 2) WRITE( OUTUNT, 10)
10    FORMAT( ///, 1X, '      DFOPTA> Start reading opacity table data', /)

      REWIND( KPXUNT)
20    READ( KPXUNT, '(A)', END = 200) CARD

      if (CARD(1:4) .EQ. '****') THEN
!**     Heading information enable / disable and copy
        ISHEAD = .NOT. ISHEAD
        if (MESLEV .LE. 2) WRITE( OUTUNT, '( 1X, (A))') CARD
        GOTO 20
      ENDIF

      if (ISHEAD) THEN
!**     Heading information is copied to stdout
        if (MESLEV .LE. 2) WRITE( OUTUNT, '( 1X, (A))') CARD
        GOTO 20
      ENDIF

      if (CARD(1:1) .EQ. '*') GOTO 20
!**   Comments are ignored if they are not part of heading information
              
!----------------------------------------------------------------------+-------
!**   Data encountered!

!**   Do it again, Sam, since you can use the *-format this way.
      BACKSPACE( KPXUNT)

!**   IDVERS
      if (.NOT. RDIDVS) THEN
        READ( KPXUNT, *) IDVERS
        RDIDVS = .TRUE.
!**     The following if-block reflects the fact that in older file versions
!**     the idvers parameter was missing. So in this files actually NT was read
!**     which was always 36 in this files.
        if (IDVERS .EQ. 36) THEN
          IDVERS = 1
          BACKSPACE( KPXUNT)
        ENDIF
        if (MESLEV .EQ. 0) WRITE( OUTUNT, *) '      IDVERS = ', IDVERS
        GOTO 20
      ENDIF

!**   OPTANA
      if (.NOT. RDOTN .AND. ( IDVERS .GE. 2)) THEN
        READ( KPXUNT, '(A)') OPTANA
        RDOTN = .TRUE.
        if (MESLEV .EQ. 0) WRITE( OUTUNT, *) '      OPTANA = ', OPTANA
        GOTO 20
      ENDIF

!**   DATEX
      if (.NOT. RDATEX .AND. ( IDVERS .GE. 2)) THEN
        READ( KPXUNT, '(A)') DATEX
        RDATEX = .TRUE.
        if (MESLEV .EQ. 0) WRITE( OUTUNT, *) '      DATEX  = ', DATEX
        GOTO 20
      ENDIF

!**   IDMEAN
      if (.NOT. RDIDMN .AND. ( IDVERS .GE. 2)) THEN
        READ( KPXUNT, *) IDMEAN
        RDIDMN = .TRUE.
        if (MESLEV .EQ. 0) WRITE( OUTUNT, *) '      IDMEAN = ', IDMEAN
        GOTO 20
      ENDIF

!**   NT
      if (.NOT. RDNT) THEN
        READ( KPXUNT, *) NT
        RDNT = .TRUE.
        if (MESLEV .EQ. 0) WRITE( OUTUNT, *) '      NT     = ', NT
        if (NT .gt. MT) stop 'Too many temperature points in table.'
        GOTO 20
      ENDIF

!**   NTBN
      if (.NOT. RDNTBN .AND. ( IDVERS .GE. 3)) THEN
        READ( KPXUNT, *) NTBN
        RDNTBN = .TRUE.
        if (MESLEV .EQ. 0) WRITE( OUTUNT, *) '      NTBN   = ', NTBN
        if (NTBN .gt. 4) stop 'More temperature interval changes than allowed.'
        GOTO 20
      ENDIF

!**   NP
      if (.NOT. RDNP) THEN
        READ( KPXUNT, *) NP
        RDNP = .TRUE.
        if (MESLEV .EQ. 0) WRITE( OUTUNT, *) '      NP     = ', NP
        if (NP .gt. MP) stop 'Too many pressure points in table.'
        GOTO 20
      ENDIF

!**   NPBN
      if (.NOT. RDNPBN .AND. ( IDVERS .GE. 3)) THEN
        READ( KPXUNT, *) NPBN
        RDNPBN = .TRUE.
        if (MESLEV .EQ. 0) WRITE( OUTUNT, *) '      NPBN   = ', NPBN
        if (NPBN .gt. 4) stop 'More pressure interval changes than allowed.'
        GOTO 20
      ENDIF

!**   NBAND
      if (.NOT. RDNBND) THEN
        READ( KPXUNT, *) NBAND
        if (MESLEV .EQ. 0) WRITE( OUTUNT, *) '      NBAND  = ', NBAND
        RDNBND = .TRUE.
        GOTO 20
      ENDIF

!**   NSTEPX
      if (.NOT. RDNSTP) THEN
        READ( KPXUNT, *) NSTEPX
        RDNSTP = .TRUE.
        if (MESLEV .EQ. 0) WRITE( OUTUNT, *) '      NSTEPX = ', NSTEPX
        GOTO 20
      ENDIF

!**   NULOX
      if (.NOT. RDNULO) THEN
        READ( KPXUNT, *) NULOX
        RDNULO = .TRUE.
        if (MESLEV .EQ. 0) WRITE( OUTUNT, *) '      NULOX  = ', NULOX
        GOTO 20
      ENDIF

!**   NUHIX
      if (.NOT. RDNUHI) THEN
        READ( KPXUNT, *) NUHIX
        RDNUHI = .TRUE.
        if (MESLEV .EQ. 0) WRITE( OUTUNT, *) '      NUHIX  = ', NUHIX
        GOTO 20
      ENDIF

!**   NUMNUX
      if (.NOT. RDNUMN) THEN
        READ( KPXUNT, *) NUMNUX
        RDNUMN = .TRUE.
        if (MESLEV .EQ. 0) WRITE( OUTUNT, *) '      NUMNUX = ', NUMNUX
        GOTO 20
      ENDIF

!**   INTMDX
      if (.NOT. RDINTM .AND. ( IDVERS .GE. 4)) THEN
        READ( KPXUNT, *) INTMDX
        RDINTM = .TRUE.
        if (MESLEV .EQ. 0) WRITE( OUTUNT, *) '      INTMDX = ', INTMDX
        GOTO 20
      ENDIF

!**   TABT
      if (.NOT. RDTABT) THEN
        READ( KPXUNT, *) ( TABT(I), I = 1, NT)
        RDTABT = .TRUE.
        if (MESLEV .EQ. 0) WRITE( OUTUNT, *) &
        '      TABT   = ', TABT( 1), ' ... ', TABT( NT)
        GOTO 20
      ENDIF

!**   TABTBN
      if (.NOT. RDTBN .AND. ( IDVERS .GE. 3)) THEN
        READ( KPXUNT, *) ( TABTBN(I), I = 1, NTBN)
        RDTBN = .TRUE.
        if (MESLEV .EQ. 0) WRITE( OUTUNT, *) &
        '      TABTBN = ', TABTBN( 1), ' ... ', TABTBN( NTBN)
        GOTO 20
      ENDIF

!**   IDXTBN
      if (.NOT. RDITBN .AND. ( IDVERS .GE. 3)) THEN
        READ( KPXUNT, *) ( IDXTBN(I), I = 1, NTBN)
        RDITBN = .TRUE.
        if (MESLEV .EQ. 0) WRITE( OUTUNT, *) &
        '      IDXTBN = ', IDXTBN( 1), ' ... ', IDXTBN( NTBN)
        GOTO 20
      ENDIF

!**   TABDTB
      if (.NOT. RDDTB .AND. ( IDVERS .GE. 3)) THEN
        READ( KPXUNT, *) ( TABDTB(I), I = 1, NTBN-1)
        RDDTB = .TRUE.
        if (MESLEV .EQ. 0) WRITE( OUTUNT, *) &
        '      TABDTB = ', TABDTB( 1), ' ... ', TABDTB( NTBN-1)
        GOTO 20
      ENDIF

!**   TABP
      if (.NOT. RDTABP) THEN
        READ( KPXUNT, *) ( TABP(I), I = 1, NP)
        if (MESLEV .EQ. 0) WRITE( OUTUNT, *) &
        '      TABP   = ', TABP( 1), ' ... ', TABP( NP)
        RDTABP = .TRUE.
        GOTO 20
      ENDIF

!**   TABPBN
      if (.NOT. RDPBN .AND. ( IDVERS .GE. 3)) THEN
        READ( KPXUNT, *) ( TABPBN(I), I = 1, NPBN)
        RDPBN = .TRUE.
        if (MESLEV .EQ. 0) WRITE( OUTUNT, *) &
        '      TABPBN = ', TABPBN( 1), ' ... ', TABPBN( NPBN)
        GOTO 20
      ENDIF

!**   IDXPBN
      if (.NOT. RDIPBN .AND. ( IDVERS .GE. 3)) THEN
        READ( KPXUNT, *) ( IDXPBN(I), I = 1, NPBN)
        RDIPBN = .TRUE.
        if (MESLEV .EQ. 0) WRITE( OUTUNT, *) &
        '      IDXPBN = ', IDXPBN( 1), ' ... ', IDXPBN( NPBN)
        GOTO 20
      ENDIF

!**   TABDPB
      if (.NOT. RDDPB .AND. ( IDVERS .GE. 3)) THEN
        READ( KPXUNT, *) ( TABDPB(I), I = 1, NPBN-1)
        RDDPB = .TRUE.
        if (MESLEV .EQ. 0) WRITE( OUTUNT, *) &
        '      TABDPB = ', TABDPB( 1), ' ... ', TABDPB( NPBN-1)
        GOTO 20
      ENDIF

!**   TABKAP
!**   To improve the performance, here the logic of DFOPTA is somewhat
!**   disturbed since more than only one data block is read.
      if (.NOT. RDTBKP) THEN
!**     Does the file contain grey Rosseland opacities?
        if (IDVERS .GE. 3) THEN
          I0BAND = 0
        ELSE
          I0BAND = 1
        ENDIF
!**     To ensure that every data block is preceded by comments ...
        BACKSPACE( KPXUNT)
        BACKSPACE( KPXUNT)
        DO IIP = 1, NP
!**       Skip pressure comment
          READ( KPXUNT, '(A)', END = 200) CARD
          DO IIBAND = I0BAND, NBAND
!**         Skip band comment
            READ( KPXUNT, '(A)', END = 200) CARD
            READ( KPXUNT, *) IP, IBAND
            READ( KPXUNT, *) ( TABKAP( IT, IP, IBAND), IT = 1, NT)
            if (MESLEV .EQ. 0) THEN
              WRITE( OUTUNT, *)
              WRITE( OUTUNT, *) '      IP     = ', IP, &
                                '      IBAND  = ', IBAND
              WRITE( OUTUNT, *) '      TABKAP = ', &
              TABKAP( 1, IP, IBAND), ' ... ', TABKAP( NT, IP, IBAND)
            ENDIF
          ENDDO
        ENDDO
        RDTBKP = .TRUE.
        GOTO 20
      ENDIF

!**   WTBBD
      if (.NOT. RDWTB) THEN
        READ( KPXUNT, *) IBAND
        READ( KPXUNT, *) (WTBBD( IT, IBAND), IT = 1, NT)
        if (MESLEV .EQ. 0) THEN
          WRITE( OUTUNT, *)
          WRITE( OUTUNT, *) '      IBAND  = ', IBAND
          WRITE( OUTUNT, *) '      WTBBD  = ', &
          WTBBD( 1, IBAND), ' ... ', WTBBD( NT, IBAND)
        ENDIF
        RDWTB = IBAND .EQ. NBAND
        GOTO 20
      ENDIF

!**   INBAND
      if (.NOT. RDINBD) THEN
        I1 =  NSTEPX *(NULOX-1) + 1
        I2 =  NSTEPX * NUHIX
        READ( KPXUNT, *) &
        ( INBAND( I), I = I1,I2 )
        if (MESLEV .EQ. 0) THEN
          WRITE( OUTUNT, *)
          WRITE( OUTUNT, *) '      INBAND = ', INBAND( I1), ' ... ', INBAND( I2)
        ENDIF
        RDINBD = .TRUE.
        GOTO 20
      ENDIF

!----------------------------------------------------------------------+-------

!**   Nothing to read anymore ...
      if (MESLEV .LE. 3) WRITE( OUTUNT, '( /, 7X, (A))') &
      'DFOPTA> Warning: File contains more data than expected!'
      if (MESLEV .LE. 3) WRITE( OUTUNT, '(    7X, (A))') &
      'DFOPTA>          Data ignored!'

200   CONTINUE
      if (MESLEV .LE. 1 .AND. RDIDVS) &
        WRITE( OUTUNT, *) '      DFOPTA> Read IDVERS'
      if (MESLEV .LE. 1 .AND. RDOTN ) &
        WRITE( OUTUNT, *) '      DFOPTA> Read OPTANA'
      if (MESLEV .LE. 1 .AND. RDATEX) &
        WRITE( OUTUNT, *) '      DFOPTA> Read DATEX'
      if (MESLEV .LE. 1 .AND. RDIDMN) &
        WRITE( OUTUNT, *) '      DFOPTA> Read IDMEAN'
      if (MESLEV .LE. 1 .AND. RDNT  ) &
        WRITE( OUTUNT, *) '      DFOPTA> Read NT'
      if (MESLEV .LE. 1 .AND. RDNTBN) &
        WRITE( OUTUNT, *) '      DFOPTA> Read NTBN'
      if (MESLEV .LE. 1 .AND. RDNP  ) &
        WRITE( OUTUNT, *) '      DFOPTA> Read NP'
      if (MESLEV .LE. 1 .AND. RDNPBN) &
        WRITE( OUTUNT, *) '      DFOPTA> Read NPBN'
      if (MESLEV .LE. 1 .AND. RDNBND) &
        WRITE( OUTUNT, *) '      DFOPTA> Read NBAND'
      if (MESLEV .LE. 1 .AND. RDNSTP) &
        WRITE( OUTUNT, *) '      DFOPTA> Read NSTEPX'
      if (MESLEV .LE. 1 .AND. RDNULO) &
        WRITE( OUTUNT, *) '      DFOPTA> Read NULOX'
      if (MESLEV .LE. 1 .AND. RDNUHI) &
        WRITE( OUTUNT, *) '      DFOPTA> Read NUHIX'
      if (MESLEV .LE. 1 .AND. RDNUMN) &
        WRITE( OUTUNT, *) '      DFOPTA> Read NUMNUX'
      if (MESLEV .LE. 1 .AND. RDINTM) &
        WRITE( OUTUNT, *) '      DFOPTA> Read INTMDX'
      if (MESLEV .LE. 1 .AND. RDTABT) &
        WRITE( OUTUNT, *) '      DFOPTA> Read TABT'
      if (MESLEV .LE. 1 .AND. RDTBN) &
        WRITE( OUTUNT, *) '      DFOPTA> Read TABTBN'
      if (MESLEV .LE. 1 .AND. RDITBN) &
        WRITE( OUTUNT, *) '      DFOPTA> Read IDXTBN'
      if (MESLEV .LE. 1 .AND. RDDTB ) &
        WRITE( OUTUNT, *) '      DFOPTA> Read TABDTB'
      if (MESLEV .LE. 1 .AND. RDTABP) &
        WRITE( OUTUNT, *) '      DFOPTA> Read TABP'
      if (MESLEV .LE. 1 .AND. RDTBN) &
        WRITE( OUTUNT, *) '      DFOPTA> Read TABPBN'
      if (MESLEV .LE. 1 .AND. RDIPBN) &
        WRITE( OUTUNT, *) '      DFOPTA> Read IDXPBN'
      if (MESLEV .LE. 1 .AND. RDDPB ) &
        WRITE( OUTUNT, *) '      DFOPTA> Read TABDPB'
      if (MESLEV .LE. 1 .AND. RDTBKP) &
        WRITE( OUTUNT, *) '      DFOPTA> Read TABKAP'
      if (MESLEV .LE. 1 .AND. RDWTB ) &
        WRITE( OUTUNT, *) '      DFOPTA> Read WTBBD'
      if (MESLEV .LE. 1 .AND. RDINBD) &
        WRITE( OUTUNT, *) '      DFOPTA> Read INBAND'

      if (MESLEV .LE. 3 .AND. .NOT. RDIDVS) &
        WRITE( OUTUNT, *) '      DFOPTA> Warning: Read not IDVERS'
      if (MESLEV .LE. 3 .AND. .NOT. RDOTN ) &
        WRITE( OUTUNT, *) '      DFOPTA> Warning: Read not OPTANA'
      if (MESLEV .LE. 3 .AND. .NOT. RDATEX) &
        WRITE( OUTUNT, *) '      DFOPTA> Warning: Read not DATEX'
      if (MESLEV .LE. 3 .AND. .NOT. RDIDMN) &
        WRITE( OUTUNT, *) '      DFOPTA> Warning: Read not IDMEAN'
      if (MESLEV .LE. 3 .AND. .NOT. RDNT  ) &
        WRITE( OUTUNT, *) '      DFOPTA> Warning: Read not NT'
      if (MESLEV .LE. 3 .AND. .NOT. RDNTBN) &
        WRITE( OUTUNT, *) '      DFOPTA> Warning: Read not NTBN, ', &
                          'default adopted!'  
      if (MESLEV .LE. 3 .AND. .NOT. RDNP  ) &
        WRITE( OUTUNT, *) '      DFOPTA> Warning: Read not NP'  
      if (MESLEV .LE. 3 .AND. .NOT. RDNPBN) &
        WRITE( OUTUNT, *) '      DFOPTA> Warning: Read not NPBN, ', &
                          'default adopted!'
      if (MESLEV .LE. 3 .AND. .NOT. RDNBND) &
        WRITE( OUTUNT, *) '      DFOPTA> Warning: Read not NBAND'
      if (MESLEV .LE. 3 .AND. .NOT. RDNSTP) &
        WRITE( OUTUNT, *) '      DFOPTA> Warning: Read not NSTEPX'
      if (MESLEV .LE. 3 .AND. .NOT. RDNULO) &
        WRITE( OUTUNT, *) '      DFOPTA> Warning: Read not NULOX'
      if (MESLEV .LE. 3 .AND. .NOT. RDNUHI) &
        WRITE( OUTUNT, *) '      DFOPTA> Warning: Read not NUHIX'
      if (MESLEV .LE. 3 .AND. .NOT. RDNUMN) &
        WRITE( OUTUNT, *) '      DFOPTA> Warning: Read not NUMNUX'
      if (MESLEV .LE. 3 .AND. .NOT. RDINTM) &
        WRITE( OUTUNT, *) '      DFOPTA> Warning: Read not INTMDX, ', &
                          'default adopted!'  
      if (MESLEV .LE. 3 .AND. .NOT. RDTABT) &
        WRITE( OUTUNT, *) '      DFOPTA> Warning: Read not TABT'
      if (MESLEV .LE. 3 .AND. .NOT. RDTBN) &
        WRITE( OUTUNT, *) '      DFOPTA> Warning: Read not TABTBN, ', &
                          'default adopted!'
      if (MESLEV .LE. 3 .AND. .NOT. RDITBN) &
        WRITE( OUTUNT, *) '      DFOPTA> Warning: Read not IDXTBN, ', &
                          'default adopted!'
      if (MESLEV .LE. 3 .AND. .NOT. RDDTB) &
        WRITE( OUTUNT, *) '      DFOPTA> Warning: Read not TABDTB, ', &
                          'default adopted!'
      if (MESLEV .LE. 3 .AND. .NOT. RDTABP) &
        WRITE( OUTUNT, *) '      DFOPTA> Warning: Read not TABP'
      if (MESLEV .LE. 3 .AND. .NOT. RDPBN) &
        WRITE( OUTUNT, *) '      DFOPTA> Warning: Read not TABPBN, ', &
                          'default adopted!'
      if (MESLEV .LE. 3 .AND. .NOT. RDIPBN) &
        WRITE( OUTUNT, *) '      DFOPTA> Warning: Read not IDXPBN, ', &
                          'default adopted!'
      if (MESLEV .LE. 3 .AND. .NOT. RDDPB ) &
        WRITE( OUTUNT, *) '      DFOPTA> Warning: Read not TABDPB, ', &
                          'default adopted!'
      if (MESLEV .LE. 3 .AND. .NOT. RDTBKP) &
        WRITE( OUTUNT, *) '      DFOPTA> Warning: Read not TABKAP'
      if (MESLEV .LE. 3 .AND. .NOT. RDWTB ) &
        WRITE( OUTUNT, *) '      DFOPTA> Warning: Read not WTBBD'
      if (MESLEV .LE. 3 .AND. .NOT. RDINBD) &
        WRITE( OUTUNT, *) '      DFOPTA> Warning: Read not INBAND'

      if (RDNT   .AND. RDTABT .AND. RDNP   .AND. RDTABP .AND.  &
          RDNBND .AND. RDTBKP .AND. RDNSTP .AND. RDNULO .AND. &
          RDNUHI .AND. RDNUMN .AND. RDINBD .AND. RDIDVS .AND. &
         (IDVERS .LE. 2 .OR.  &
         (RDNTBN .AND. RDNPBN .AND. RDTBN  .AND. RDITBN .AND. &
          RDDTB  .AND. RDPBN  .AND. RDIPBN .AND. RDDPB )) &
         ) THEN
        if (MESLEV .LE. 2) WRITE( OUTUNT, '( /, 7X, (A))')  &
        'DFOPTA> All necessary opacity table data defined!'
      ELSE
        if (MESLEV .LE. 3) WRITE( OUTUNT, '( /, 7X, (A))') &
        'DFOPTA> Warning: Not all necessary opacity table data defined!'
      ENDIF
        
!**   Start new page ...
!**   ATLAS version not necessary
!      if (MESLEV .LE. 2) WRITE( OUTUNT, '(1H1)')      
!
!     --- wtbbd ---
!     --- Normalize WTBBD fractions ---
      DO 320 IT = 1, NT
        DUMMY = 0.0
        DO 300 IBAND = 1, NBAND
          DUMMY = DUMMY + WTBBD( IT, IBAND)
300     CONTINUE
        DO 310 IBAND = 1, NBAND
          WTBBD( IT, IBAND) = WTBBD( IT, IBAND) / DUMMY
310     CONTINUE
320   CONTINUE
!
!     --- Compute wtbbd interpolation coefficients wtbbdc for routine xbnuwt ---
!     --- once and for all.                                                  ---
      intmod=3
      do 410 iband=0,nband
        do 400 iT=1,nT-1
          call cubitc(tabT, wTbbd(1, iband), nT, intmod, iT, tabT(iT), &
                      wTb, wtdbdT, &
                      wtbbdc(1,iT,iband), wtbbdc(2,iT,iband), &
                      wtbbdc(3,iT,iband), wtbbdc(4,iT,iband))
!          write(6,*) iband,iT,wtbbdc(1:4,iT,iband)
  400   continue
  410 continue
!
      NBDUM = NBAND
!
      RETURN
      END SUBROUTINE DFOPTA
!--------------------******--------------------------------------------+-------


!----------------*******-----------------------------------------------+-------
      SUBROUTINE DFOPTAD(MESLEV, OUTUNT, KPXUNT, NBDUM)
!----------------------------------------------------------------------+-------
!
! Version: 23. 2.93 Written by HGL
!          30. 5.97 (B.F.) Compute wtbbdc in xopta1
!           2.10.97 (M.S.) Additional dummy argument NBDUM (=NBAND)
!          28.01.02 (M.S.) call cubitic --> call cubitc
!          17.07.09 (B.F.) dfopta -> dfoptad
!
! Description: DFOPTAD reads opacity table data and stores it in common blocks
!   XOPTA, xopta1, (and XOPTAN) (character data).
!   It distinguishes 3 different entities: Heading information, comments and
!   numerical data. Heading information is copied to the output ( MESLEV<=2),
!   comments are ignored. The order of variables read has to correspond with
!   the order of writing in KAPPAX! The meslev parameter determines the 
!   messages which DFOPTAD issues:
!   MESLEV = 0 : All messages, journaling the data read
!          = 1 : No journaling
!          = 2 : No detailed information which variable was read
!          = 3 : Warnings and heading information only
!          = 4 : No output
!   IDVERS should be read as the first variable. It identifies the opacity 
!   table data structure; not all variables are available in older file
!   versions.
!
! Arguments, access, type, dimension, description:
!   MESLEV R I : Message level
!   OUTUNT R I : Unit to which messages were written
!   KPXUNT R I : Unit from which opacity table data wre read
!
! Common blocks (access): XOPTA (W), xopta1 (w), (XOPTAN (W))
!
! External procedures   : None
!
! File units (access)   : KPXUNT (R), OUTUNT (W)
!
!
! Comments: In this version the subroutine's parameter list was modified!
!           12.12.91 B_nu fraction normalization included,
!                    defaults corrected
!           29. 5.97 (B.F.) TABDTB(1:4), TABDPB(1:4) initialized to
!                    non-zero values
!           30. 5.97 (B.F.) 
!
! Legend:
!   Access: (R)ead only, (W)rite only, (*)read/write
!   Type  : (C)omplex, (D)ouble precision, (E)xternal, (I)nteger, 
!           (R)eal, (S)tring, (*)label
!
!----------------------------------------------------------------------+-------
!
!**   XOPTAD contains the opacity table data read by DFOPTAD
      USE xoptad_def
!
      IMPLICIT NONE
!
!--- Dummy arguments:
      INTEGER,       INTENT(IN)        :: MESLEV, OUTUNT, KPXUNT
      INTEGER,       INTENT(OUT)       :: NBDUM
!
!--- Local Variables ---
      INTEGER    :: I, I1, I2, INTMOD, IBAND, IIBAND, I0BAND, ISTEP, &
                    IR, IIR, IT, NU
      REAL       :: DUMMY, WTB, WTDBDT
!
!
      CHARACTER(LEN=80) :: OPTANA
      CHARACTER(LEN=20) :: DATEX
      CHARACTER(LEN=72) :: CARD
      LOGICAL           :: ISHEAD
      LOGICAL           :: RDIDVS, RDOTN, RDATEX, RDIDMN, RDNT, RDNTBN, &
                           RDNR, RDNRBN, RDNBND, RDNSTP, RDNULO, RDNUHI, &
                           RDNUMN, RDINTM, RDTABT, RDTBN, RDITBN, RDDTB, &
                           RDTABR, RDRBN, RDIRBN, RDDRB, RDTBKP, RDWTB, RDINBD

!----------------------------------------------------------------------+-------
!**   Set initial values
      ISHEAD = .FALSE.
      RDIDVS = .FALSE.
      RDOTN  = .FALSE.
      RDATEX = .FALSE.
      RDIDMN = .FALSE.
      RDNT   = .FALSE.
      RDNTBN = .FALSE.
      RDNR   = .FALSE.
      RDNRBN = .FALSE.
      RDNBND = .FALSE.
      RDNSTP = .FALSE.
      RDNULO = .FALSE.
      RDNUHI = .FALSE.
      RDNUMN = .FALSE.
      RDINTM = .FALSE.
      RDTABT = .FALSE.
      RDTBN  = .FALSE.
      RDITBN = .FALSE.
      RDDTB  = .FALSE.
      RDTABR = .FALSE.
      RDRBN  = .FALSE.
      RDIRBN = .FALSE.
      RDDRB  = .FALSE.
      RDTBKP = .FALSE.
      RDWTB  = .FALSE.
      RDINBD = .FALSE.
!**   Parameters of old TABT, TABR structure
!**   Temperatures
      OPTAD%NT     = 36
      OPTAD%NTBN   = 4
      OPTAD%TABTBN(1) = 3.5
      OPTAD%TABTBN(2) = 4.0
      OPTAD%TABTBN(3) = 4.5
      OPTAD%TABTBN(4) = 5.0
      OPTAD%IDXTBN(1) = 1
      OPTAD%IDXTBN(2) = 21
      OPTAD%IDXTBN(3) = 31
      OPTAD%IDXTBN(4) = 36
!
! --- 29. 5.97 ---
      OPTAD%TABDTB(1) = 0.025
      OPTAD%TABDTB(2) = 0.05
      OPTAD%TABDTB(3) = 0.05
      OPTAD%TABDTB(4) = 0.05
! ---          ---
!
!**   Radii (useless pressure values)
      OPTAD%NR     = 30
      OPTAD%NRBN   = 4
      OPTAD%TABRBN(1) = -2.0
      OPTAD%TABRBN(2) = 1.0
      OPTAD%TABRBN(3) = 3.0
      OPTAD%TABRBN(4) = 6.0
      OPTAD%IDXRBN(1) = 1
      OPTAD%IDXRBN(2) = 7
      OPTAD%IDXRBN(3) = 15
      OPTAD%IDXRBN(4) = 30
      OPTAD%TABDRB(1) = 0.5
      OPTAD%TABDRB(2) = 0.25
!
! --- 29. 5.97 ---
      OPTAD%TABDRB(3) = 0.2
      OPTAD%TABDRB(4) = 0.1
! ---          ---
!
!**   Default interpolation mode!
      OPTAD%INTMDX = 2
!----------------------------------------------------------------------+-------

      REWIND( KPXUNT)

!**   New page
      if (MESLEV .LE. 2) WRITE( OUTUNT, '(1H1)')

!**   DFOPTAD greetings
      if (MESLEV .LE. 2) WRITE( OUTUNT, 10)
10    FORMAT( ///, 1X, '      DFOPTAD> Start reading opacity table data', /)

      REWIND( KPXUNT)
20    READ( KPXUNT, '(A)', END = 200) CARD

      if (CARD(1:4) .EQ. '****') THEN
!**     Heading information enable / disable and copy
        ISHEAD = .NOT. ISHEAD
        if (MESLEV .LE. 2) WRITE( OUTUNT, '( 1X, (A))') CARD
        GOTO 20
      ENDIF

      if (ISHEAD) THEN
!**     Heading information is copied to stdout
        if (MESLEV .LE. 2) WRITE( OUTUNT, '( 1X, (A))') CARD
        GOTO 20
      ENDIF

      if (CARD(1:1) .EQ. '*') GOTO 20
!**   Comments are ignored if they are not part of heading information
              
!----------------------------------------------------------------------+-------
!**   Data encountered!

!**   Do it again, Sam, since you can use the *-format this way.
      BACKSPACE( KPXUNT)

!**   IDVERS
      if (.NOT. RDIDVS) THEN
        READ( KPXUNT, *) OPTAD%IDVERS
        RDIDVS = .TRUE.
!**     The following if-block reflects the fact that in older file versions
!**     the idvers parameter was missing. So in this files actually NT was read
!**     which was always 36 in this files.
        if (OPTAD%IDVERS .EQ. 36) THEN
          OPTAD%IDVERS = 1
          BACKSPACE( KPXUNT)
        ENDIF
        if (MESLEV .EQ. 0) WRITE( OUTUNT, *) '      IDVERS = ', OPTAD%IDVERS
        GOTO 20
      ENDIF

!**   OPTANA
      if (.NOT. RDOTN .AND. ( OPTAD%IDVERS .GE. 2)) THEN
        READ( KPXUNT, '(A)') OPTANA
        RDOTN = .TRUE.
        if (MESLEV .EQ. 0) WRITE( OUTUNT, *) '      OPTANA = ', OPTANA
        GOTO 20
      ENDIF

!**   DATEX
      if (.NOT. RDATEX .AND. ( OPTAD%IDVERS .GE. 2)) THEN
        READ( KPXUNT, '(A)') DATEX
        RDATEX = .TRUE.
        if (MESLEV .EQ. 0) WRITE( OUTUNT, *) '      DATEX  = ', DATEX
        GOTO 20
      ENDIF

!**   IDMEAN
      if (.NOT. RDIDMN .AND. ( OPTAD%IDVERS .GE. 2)) THEN
        READ( KPXUNT, *) OPTAD%IDMEAN
        RDIDMN = .TRUE.
        if (MESLEV .EQ. 0) WRITE( OUTUNT, *) '      IDMEAN = ', OPTAD%IDMEAN
        GOTO 20
      ENDIF

!**   NT
      if (.NOT. RDNT) THEN
        READ( KPXUNT, *) OPTAD%NT
        RDNT = .TRUE.
        if (MESLEV .EQ. 0) WRITE( OUTUNT, *) '      NT     = ', OPTAD%NT
        GOTO 20
      ENDIF

!**   NTBN
      if (.NOT. RDNTBN .AND. ( OPTAD%IDVERS .GE. 3)) THEN
        READ( KPXUNT, *) OPTAD%NTBN
        RDNTBN = .TRUE.
        if (MESLEV .EQ. 0) WRITE( OUTUNT, *) '      NTBN   = ', OPTAD%NTBN
        GOTO 20
      ENDIF

!**   NR
      if (.NOT. RDNR) THEN
        READ( KPXUNT, *) OPTAD%NR
        if (MESLEV .EQ. 0) WRITE( OUTUNT, *) '      NR     = ', OPTAD%NR
        RDNR = .TRUE.
        GOTO 20
      ENDIF

!**   NRBN
      if (.NOT. RDNRBN .AND. ( OPTAD%IDVERS .GE. 3)) THEN
        READ( KPXUNT, *) OPTAD%NRBN
        RDNRBN = .TRUE.
        if (MESLEV .EQ. 0) WRITE( OUTUNT, *) '      NRBN   = ', OPTAD%NRBN
        GOTO 20
      ENDIF

!**   NBAND
      if (.NOT. RDNBND) THEN
        READ( KPXUNT, *) OPTAD%NBAND
        if (MESLEV .EQ. 0) WRITE( OUTUNT, *) '      NBAND  = ', OPTAD%NBAND
        RDNBND = .TRUE.
        GOTO 20
      ENDIF

!**   NSTEPX
      if (.NOT. RDNSTP) THEN
        READ( KPXUNT, *) OPTAD%NSTEPX
        RDNSTP = .TRUE.
        if (MESLEV .EQ. 0) WRITE( OUTUNT, *) '      NSTEPX = ', OPTAD%NSTEPX
        GOTO 20
      ENDIF

!**   NULOX
      if (.NOT. RDNULO) THEN
        READ( KPXUNT, *) OPTAD%NULOX
        RDNULO = .TRUE.
        if (MESLEV .EQ. 0) WRITE( OUTUNT, *) '      NULOX  = ', OPTAD%NULOX
        GOTO 20
      ENDIF

!**   NUHIX
      if (.NOT. RDNUHI) THEN
        READ( KPXUNT, *) OPTAD%NUHIX
        RDNUHI = .TRUE.
        if (MESLEV .EQ. 0) WRITE( OUTUNT, *) '      NUHIX  = ', OPTAD%NUHIX
        GOTO 20
      ENDIF

!**   NUMNUX
      if (.NOT. RDNUMN) THEN
        READ( KPXUNT, *) OPTAD%NUMNUX
        RDNUMN = .TRUE.
        if (MESLEV .EQ. 0) WRITE( OUTUNT, *) '      NUMNUX = ', OPTAD%NUMNUX
        GOTO 20
      ENDIF

!**   INTMDX
      if (.NOT. RDINTM .AND. ( OPTAD%IDVERS .GE. 4)) THEN
        READ( KPXUNT, *) OPTAD%INTMDX
        RDINTM = .TRUE.
        if (MESLEV .EQ. 0) WRITE( OUTUNT, *) '      INTMDX = ', OPTAD%INTMDX
        GOTO 20
      ENDIF

!**   TABT
      if (.NOT. RDTABT) THEN
        READ( KPXUNT, *) ( OPTAD%TABT(I), I = 1, OPTAD%NT)
        RDTABT = .TRUE.
        if (MESLEV .EQ. 0) WRITE( OUTUNT, *) &
        '      TABT   = ', OPTAD%TABT( 1), ' ... ', OPTAD%TABT( OPTAD%NT)
        GOTO 20
      ENDIF

!**   TABTBN
      if (.NOT. RDTBN .AND. ( OPTAD%IDVERS .GE. 3)) THEN
        READ( KPXUNT, *) ( OPTAD%TABTBN(I), I = 1, OPTAD%NTBN)
        RDTBN = .TRUE.
        if (MESLEV .EQ. 0) WRITE( OUTUNT, *) &
        '      TABTBN = ', OPTAD%TABTBN( 1), ' ... ', OPTAD%TABTBN( OPTAD%NTBN)
        GOTO 20
      ENDIF

!**   IDXTBN
      if (.NOT. RDITBN .AND. ( OPTAD%IDVERS .GE. 3)) THEN
        READ( KPXUNT, *) ( OPTAD%IDXTBN(I), I = 1, OPTAD%NTBN)
        RDITBN = .TRUE.
        if (MESLEV .EQ. 0) WRITE( OUTUNT, *) &
        '      IDXTBN = ', OPTAD%IDXTBN( 1), ' ... ', OPTAD%IDXTBN( OPTAD%NTBN)
        GOTO 20
      ENDIF

!**   TABDTB
      if (.NOT. RDDTB .AND. ( OPTAD%IDVERS .GE. 3)) THEN
        READ( KPXUNT, *) ( OPTAD%TABDTB(I), I = 1, OPTAD%NTBN-1)
        RDDTB = .TRUE.
        if (MESLEV .EQ. 0) WRITE( OUTUNT, *) &
        '      TABDTB = ', OPTAD%TABDTB( 1), ' ... ', OPTAD%TABDTB( OPTAD%NTBN-1)
        GOTO 20
      ENDIF

!**   TABR
      if (.NOT. RDTABR) THEN
        READ( KPXUNT, *) ( OPTAD%TABR(I), I = 1, OPTAD%NR)
        if (MESLEV .EQ. 0) WRITE( OUTUNT, *) &
        '      TABR   = ', OPTAD%TABR( 1), ' ... ', OPTAD%TABR( OPTAD%NR)
        RDTABR = .TRUE.
        GOTO 20
      ENDIF

!**   TABRBN
      if (.NOT. RDRBN .AND. ( OPTAD%IDVERS .GE. 3)) THEN
        READ( KPXUNT, *) ( OPTAD%TABRBN(I), I = 1, OPTAD%NRBN)
        RDRBN = .TRUE.
        if (MESLEV .EQ. 0) WRITE( OUTUNT, *) &
        '      TABRBN = ', OPTAD%TABRBN( 1), ' ... ', OPTAD%TABRBN( OPTAD%NRBN)
        GOTO 20
      ENDIF

!**   IDXRBN
      if (.NOT. RDIRBN .AND. ( OPTAD%IDVERS .GE. 3)) THEN
        READ( KPXUNT, *) ( OPTAD%IDXRBN(I), I = 1, OPTAD%NRBN)
        RDIRBN = .TRUE.
        if (MESLEV .EQ. 0) WRITE( OUTUNT, *) &
        '      IDXRBN = ', OPTAD%IDXRBN( 1), ' ... ', OPTAD%IDXRBN( OPTAD%NRBN)
        GOTO 20
      ENDIF

!**   TABDRB
      if (.NOT. RDDRB .AND. ( OPTAD%IDVERS .GE. 3)) THEN
        READ( KPXUNT, *) ( OPTAD%TABDRB(I), I = 1, OPTAD%NRBN-1)
        RDDRB = .TRUE.
        if (MESLEV .EQ. 0) WRITE( OUTUNT, *) &
        '      TABDRB = ', OPTAD%TABDRB( 1), ' ... ', OPTAD%TABDRB( OPTAD%NRBN-1)
        GOTO 20
      ENDIF

!**   TABKAP
!**   To improve the performance, here the logic of DFOPTAD is somewhat
!**   disturbed since more than only one data block is read.
      if (.NOT. RDTBKP) THEN
!**     Does the file contain grey Rosseland opacities?
        if (OPTAD%IDVERS .GE. 3) THEN
          I0BAND = 0
        ELSE
          I0BAND = 1
        ENDIF
!**     To ensure that every data block is preceded by comments ...
        BACKSPACE( KPXUNT)
        BACKSPACE( KPXUNT)
        DO IIR = 1, OPTAD%NR
!**       Skip radius comment
          READ( KPXUNT, '(A)', END = 200) CARD
          DO IIBAND = I0BAND, OPTAD%NBAND
!**         Skip band comment
            READ( KPXUNT, '(A)', END = 200) CARD
            READ( KPXUNT, *) IR, IBAND
            READ( KPXUNT, *) ( OPTAD%TABKAP( IT, IR, IBAND), IT = 1, OPTAD%NT)
            if (MESLEV .EQ. 0) THEN
              WRITE( OUTUNT, *)
              WRITE( OUTUNT, *) '      IR     = ', IR, &
                                '      IBAND  = ', IBAND
              WRITE( OUTUNT, *) '      TABKAP = ', &
              OPTAD%TABKAP( 1, IR, IBAND), ' ... ', OPTAD%TABKAP( OPTAD%NT, IR, IBAND)
            ENDIF
          ENDDO
        ENDDO
        RDTBKP = .TRUE.
        GOTO 20
      ENDIF

!**   WTBBD
      if (.NOT. RDWTB) THEN
        READ( KPXUNT, *) IBAND
        READ( KPXUNT, *) (OPTAD%WTBBD( IT, IBAND), IT = 1, OPTAD%NT)
        if (MESLEV .EQ. 0) THEN
          WRITE( OUTUNT, *)
          WRITE( OUTUNT, *) '      IBAND  = ', IBAND
          WRITE( OUTUNT, *) '      WTBBD  = ', &
          OPTAD%WTBBD( 1, IBAND), ' ... ', OPTAD%WTBBD( OPTAD%NT, IBAND)
        ENDIF
        RDWTB = IBAND .EQ. OPTAD%NBAND
        GOTO 20
      ENDIF

!**   INBAND
      if (.NOT. RDINBD) THEN
        I1 =  OPTAD%NSTEPX *(OPTAD%NULOX-1) + 1
        I2 =  OPTAD%NSTEPX * OPTAD%NUHIX
        READ( KPXUNT, *) &
        ( OPTAD%INBAND( I), I = I1,I2 )
        if (MESLEV .EQ. 0) WRITE( OUTUNT, *) &
        '      INBAND = ', OPTAD%INBAND( I1), ' ... ', OPTAD%INBAND( I2)
        RDINBD = .TRUE.
        GOTO 20
      ENDIF

!----------------------------------------------------------------------+-------

!**   Nothing to read anymore ...
      if (MESLEV .LE. 3) WRITE( OUTUNT, '( /, 7X, (A))') &
      'DFOPTAD> Warning: File contains more data than expected!'
      if (MESLEV .LE. 3) WRITE( OUTUNT, '(    7X, (A))') &
      'DFOPTAD>          Data ignored!'

200   CONTINUE
      if (MESLEV .LE. 1 .AND. RDIDVS) &
        WRITE( OUTUNT, *) '      DFOPTAD> Read IDVERS'
      if (MESLEV .LE. 1 .AND. RDOTN ) &
        WRITE( OUTUNT, *) '      DFOPTAD> Read OPTANA'
      if (MESLEV .LE. 1 .AND. RDATEX) &
        WRITE( OUTUNT, *) '      DFOPTAD> Read DATEX'
      if (MESLEV .LE. 1 .AND. RDIDMN) &
        WRITE( OUTUNT, *) '      DFOPTAD> Read IDMEAN'
      if (MESLEV .LE. 1 .AND. RDNT  ) &
        WRITE( OUTUNT, *) '      DFOPTAD> Read NT'
      if (MESLEV .LE. 1 .AND. RDNTBN) &
        WRITE( OUTUNT, *) '      DFOPTAD> Read NTBN'
      if (MESLEV .LE. 1 .AND. RDNR  ) &
        WRITE( OUTUNT, *) '      DFOPTAD> Read NR'
      if (MESLEV .LE. 1 .AND. RDNRBN) &
        WRITE( OUTUNT, *) '      DFOPTAD> Read NRBN'
      if (MESLEV .LE. 1 .AND. RDNBND) &
        WRITE( OUTUNT, *) '      DFOPTAD> Read NBAND'
      if (MESLEV .LE. 1 .AND. RDNSTP) &
        WRITE( OUTUNT, *) '      DFOPTAD> Read NSTEPX'
      if (MESLEV .LE. 1 .AND. RDNULO) &
        WRITE( OUTUNT, *) '      DFOPTAD> Read NULOX'
      if (MESLEV .LE. 1 .AND. RDNUHI) &
        WRITE( OUTUNT, *) '      DFOPTAD> Read NUHIX'
      if (MESLEV .LE. 1 .AND. RDNUMN) &
        WRITE( OUTUNT, *) '      DFOPTAD> Read NUMNUX'
      if (MESLEV .LE. 1 .AND. RDINTM) &
        WRITE( OUTUNT, *) '      DFOPTAD> Read INTMDX'
      if (MESLEV .LE. 1 .AND. RDTABT) &
        WRITE( OUTUNT, *) '      DFOPTAD> Read TABT'
      if (MESLEV .LE. 1 .AND. RDTBN) &
        WRITE( OUTUNT, *) '      DFOPTAD> Read TABTBN'
      if (MESLEV .LE. 1 .AND. RDITBN) &
        WRITE( OUTUNT, *) '      DFOPTAD> Read IDXTBN'
      if (MESLEV .LE. 1 .AND. RDDTB ) &
        WRITE( OUTUNT, *) '      DFOPTAD> Read TABDTB'
      if (MESLEV .LE. 1 .AND. RDTABR) &
        WRITE( OUTUNT, *) '      DFOPTAD> Read TABR'
      if (MESLEV .LE. 1 .AND. RDTBN) &
        WRITE( OUTUNT, *) '      DFOPTAD> Read TABRBN'
      if (MESLEV .LE. 1 .AND. RDIRBN) &
        WRITE( OUTUNT, *) '      DFOPTAD> Read IDXRBN'
      if (MESLEV .LE. 1 .AND. RDDRB ) &
        WRITE( OUTUNT, *) '      DFOPTAD> Read TABDRB'
      if (MESLEV .LE. 1 .AND. RDTBKP) &
        WRITE( OUTUNT, *) '      DFOPTAD> Read TABKAP'
      if (MESLEV .LE. 1 .AND. RDWTB ) &
        WRITE( OUTUNT, *) '      DFOPTAD> Read WTBBD'
      if (MESLEV .LE. 1 .AND. RDINBD) &
        WRITE( OUTUNT, *) '      DFOPTAD> Read INBAND'

      if (MESLEV .LE. 3 .AND. .NOT. RDIDVS) &
        WRITE( OUTUNT, *) '      DFOPTAD> Warning: Read not IDVERS'
      if (MESLEV .LE. 3 .AND. .NOT. RDOTN ) &
        WRITE( OUTUNT, *) '      DFOPTAD> Warning: Read not OPTANA'
      if (MESLEV .LE. 3 .AND. .NOT. RDATEX) &
        WRITE( OUTUNT, *) '      DFOPTAD> Warning: Read not DATEX'
      if (MESLEV .LE. 3 .AND. .NOT. RDIDMN) &
        WRITE( OUTUNT, *) '      DFOPTAD> Warning: Read not IDMEAN'
      if (MESLEV .LE. 3 .AND. .NOT. RDNT  ) &
        WRITE( OUTUNT, *) '      DFOPTAD> Warning: Read not NT'
      if (MESLEV .LE. 3 .AND. .NOT. RDNTBN) &
        WRITE( OUTUNT, *) '      DFOPTAD> Warning: Read not NTBN, ', &
                          'default adopted!'  
      if (MESLEV .LE. 3 .AND. .NOT. RDNR  ) &
        WRITE( OUTUNT, *) '      DFOPTAD> Warning: Read not NR'  
      if (MESLEV .LE. 3 .AND. .NOT. RDNRBN) &
        WRITE( OUTUNT, *) '      DFOPTAD> Warning: Read not NRBN, ', &
                          'default adopted!'
      if (MESLEV .LE. 3 .AND. .NOT. RDNBND) &
        WRITE( OUTUNT, *) '      DFOPTAD> Warning: Read not NBAND'
      if (MESLEV .LE. 3 .AND. .NOT. RDNSTP) &
        WRITE( OUTUNT, *) '      DFOPTAD> Warning: Read not NSTEPX'
      if (MESLEV .LE. 3 .AND. .NOT. RDNULO) &
        WRITE( OUTUNT, *) '      DFOPTAD> Warning: Read not NULOX'
      if (MESLEV .LE. 3 .AND. .NOT. RDNUHI) &
        WRITE( OUTUNT, *) '      DFOPTAD> Warning: Read not NUHIX'
      if (MESLEV .LE. 3 .AND. .NOT. RDNUMN) &
        WRITE( OUTUNT, *) '      DFOPTAD> Warning: Read not NUMNUX'
      if (MESLEV .LE. 3 .AND. .NOT. RDINTM) &
        WRITE( OUTUNT, *) '      DFOPTAD> Warning: Read not INTMDX, ', &
                          'default adopted!'  
      if (MESLEV .LE. 3 .AND. .NOT. RDTABT) &
        WRITE( OUTUNT, *) '      DFOPTAD> Warning: Read not TABT'
      if (MESLEV .LE. 3 .AND. .NOT. RDTBN) &
        WRITE( OUTUNT, *) '      DFOPTAD> Warning: Read not TABTBN, ', &
                          'default adopted!'
      if (MESLEV .LE. 3 .AND. .NOT. RDITBN) &
        WRITE( OUTUNT, *) '      DFOPTAD> Warning: Read not IDXTBN, ', &
                          'default adopted!'
      if (MESLEV .LE. 3 .AND. .NOT. RDDTB) &
        WRITE( OUTUNT, *) '      DFOPTAD> Warning: Read not TABDTB, ', &
                          'default adopted!'
      if (MESLEV .LE. 3 .AND. .NOT. RDTABR) &
        WRITE( OUTUNT, *) '      DFOPTAD> Warning: Read not TABR'
      if (MESLEV .LE. 3 .AND. .NOT. RDRBN) &
        WRITE( OUTUNT, *) '      DFOPTAD> Warning: Read not TABRBN, ', &
                          'default adopted!'
      if (MESLEV .LE. 3 .AND. .NOT. RDIRBN) &
        WRITE( OUTUNT, *) '      DFOPTAD> Warning: Read not IDXRBN, ', &
                          'default adopted!'
      if (MESLEV .LE. 3 .AND. .NOT. RDDRB ) &
        WRITE( OUTUNT, *) '      DFOPTAD> Warning: Read not TABDRB, ', &
                          'default adopted!'
      if (MESLEV .LE. 3 .AND. .NOT. RDTBKP) &
        WRITE( OUTUNT, *) '      DFOPTAD> Warning: Read not TABKAP'
      if (MESLEV .LE. 3 .AND. .NOT. RDWTB ) &
        WRITE( OUTUNT, *) '      DFOPTAD> Warning: Read not WTBBD'
      if (MESLEV .LE. 3 .AND. .NOT. RDINBD) &
        WRITE( OUTUNT, *) '      DFOPTAD> Warning: Read not INBAND'

      if (RDNT   .AND. RDTABT .AND. RDNR   .AND. RDTABR .AND.  &
          RDNBND .AND. RDTBKP .AND. RDNSTP .AND. RDNULO .AND. &
          RDNUHI .AND. RDNUMN .AND. RDINBD .AND. RDIDVS .AND. &
         (OPTAD%IDVERS .LE. 2 .OR.  &
         (RDNTBN .AND. RDNRBN .AND. RDTBN  .AND. RDITBN .AND. &
          RDDTB  .AND. RDRBN  .AND. RDIRBN .AND. RDDRB )) &
         ) THEN
        if (MESLEV .LE. 2) WRITE( OUTUNT, '( /, 7X, (A))')  &
        'DFOPTAD> All necessary opacity table data defined!'
      ELSE
        if (MESLEV .LE. 3) WRITE( OUTUNT, '( /, 7X, (A))') &
        'DFOPTAD> Warning: Not all necessary opacity table data defined!'
      ENDIF
        
!**   Start new page ...
!**   ATLAS version not necessary
!      if (MESLEV .LE. 2) WRITE( OUTUNT, '(1H1)')      
!
!     --- wtbbd ---
!     --- Normalize WTBBD fractions ---
      DO 320 IT = 1, OPTAD%NT
        DUMMY = 0.0
        DO 300 IBAND = 1, OPTAD%NBAND
          DUMMY = DUMMY + OPTAD%WTBBD( IT, IBAND)
300     CONTINUE
        DO 310 IBAND = 1, OPTAD%NBAND
          OPTAD%WTBBD( IT, IBAND) = OPTAD%WTBBD( IT, IBAND) / DUMMY
310     CONTINUE
320   CONTINUE
!
!!     --- Compute wtbbd interpolation coefficients wtbbdc for routine xbnuwt ---
!!     --- once and for all.                                                  ---
!      intmod=3
!      do 410 iband=0,nband
!        do 400 iT=1,nT-1
!          call cubitc(tabT, wTbbd(1, iband), nT, intmod, iT, tabT(iT), &
!                      wTb, wtdbdT, &
!                      wtbbdc(1,iT,iband), wtbbdc(2,iT,iband), &
!                      wtbbdc(3,iT,iband), wtbbdc(4,iT,iband))
!!          write(6,*) iband,iT,wtbbdc(1:4,iT,iband)
!  400   continue
!  410 continue
!
      NBDUM = OPTAD%NBAND
!
      RETURN
      END SUBROUTINE DFOPTAD
!--------------------*******-------------------------------------------+-------


!----------------*********---------------------------------------------+-------
      subroutine opta_info(nbin)
!----------------------------------------------------------------------+-------
! NAME:
!   opta_info ('opacity_table_info')
!
! PURPOSE:
!   Give number of bins (or bands).
!
! CALLING SEQUENCE:
!   call opta_info(nbin=nbin)
!
! OUTPUT:
!   nbin: ('number_bins') optional integer, current number of bins (or bands)
!
! COMMON BLOCKS:
!   xopta: contains the opacity table data read by DFOPTA (access: read-only)
!
! MODIFICATION HISTORY:
!   13. 4.00 Written by B.F.
!----------------------------------------------------------------------+-------
      use xopta_def, only: nband
!
      implicit none
!
!     --- I/O parameters ---
      integer, optional, intent(out)    :: nbin
!----------------------------------------------------------------------+-------
!
      if (present(nbin)) then
        nbin=nband
      endif
!
      end subroutine opta_info
!--------------------*********-----------------------------------------+-------


!----------------***********-------------------------------------------+-------
      subroutine opta_modify(kappamax)
!----------------------------------------------------------------------+-------
! NAME:
!   opta_modify ('opacity_table_modify')
!
! PURPOSE:
!   Manipulate opacity data.
!
! CALLING SEQUENCE:
!   call opta_modify(kappamax=kappamax)
!
! OUTPUT:
!
! COMMON BLOCKS:
!   xopta: contains the opacity table data read by DFOPTA (access: read/write)
!
! MODIFICATION HISTORY:
!   26. 5.00 Written by B.F.
!----------------------------------------------------------------------+-------
      use xopta_def
!
      implicit none
!
!     --- I/O parameters ---
      real,    optional, intent(in)     :: kappamax
!----------------------------------------------------------------------+-------
!
      if (present(kappamax)) then
        if (kappamax > 0.0) tabkap=min(tabkap, alog10(kappamax))
      endif
!
      end subroutine opta_modify
!--------------------***********---------------------------------------+-------


!-------------------*******--------------------------------------------+-------
      REAL function xkaros0(P, T, iband)
!----------------------------------------------------------------------+-------
! NAME:
!   xkaros0 ('extended_kappa_rosseland_0')
!
! PURPOSE:
!   XKAROS0 computes opacities for given pressure P, temperature T
!   and frequency bin IBAND. It determines the opacities by interpolation in
!   a table given by TABKAP.
!
! CATEGORY:
!   Opacities, Interpolation
!
! CALLING SEQUENCE:
!   kappa=xkaros0(P, T, iband)
!
! INPUT:
!   P:     ('Pressure') real, pressure [dyn/cm^2]
!   T:     ('Temperature') real, temperature [K]
!   iband: ('identification_band') integer, bin identification number
!   
! OUTPUT:
!   xkaros0: ('extended_kappa_rosseland_0'), opacity per gram [cm^2/g]
!
! ROUTINES:
!   None
!
! INCLUDES:
!   None
!
! COMMON BLOCK:
!   XOPTA: opacity table data read by DFOPTA, access: readonly
!
! SIDE EFFECTS:
!   No file access.
!
! RESTRICTIONS:
!   NTBN, NPBN have to be smaller or equal than 4.
!
! PROCEDURE:
!
! EXAMPLE:
!
! MODIFICATION HISTORY:
!   21. 1.91 Hans-Guenter Ludwig, Astrophysik Kiel
!   27. 5.97 Bernd Freytag, Astrophysik Kiel
!            Use simple interpolation: cubic polynomial through 4 points
!   28. 5.97 Interpolation: cubic polynomial through 2 points
!            with slopes at both points given by parabolas
!   24. 6.97 (B.F.) TX=min(TABT(NT  ), max(TABT(1), alog10(T))) 
!               ->  TX=min(TABT(NT-1), max(TABT(2), alog10(T))) ...
!----------------------------------------------------------------------+-------
!
!     --- XOPTA contains the opacity table data read by DFOPTA ---
      USE xopta_def
!
      IMPLICIT NONE
!
!--- Dummy arguments:
      INTEGER,       INTENT(IN)     :: iband 
      REAL,          INTENT(IN)     :: P, T
!
!--- Local Variables ---
      REAL, PARAMETER :: ALN10=2.30258509299405E0
      INTEGER :: iTx,iPx, nTx, nPx
      REAL    :: TX, PX
      REAL    :: gT1,gT2,gT3,gT4, gP1,gP2,gP3,gP4, fP1,fP2,fP3,fP4
!
!----------------------------------------------------------------------+-------
!
!     --- Delimit values of TX and PX to the ranges given by TABT and TABP ---
      TX=min(TABT(NT-1), max(TABT(2), alog10(T)))
      PX=min(TABP(NP-1), max(TABP(2), alog10(P)))
!
!     --- Determine interval index NTX for temperature interpolation ---
!     --- The interval index is defined different for MAP and CUBIT! ---
!     --- It is assumed that TX can not be smaller than TABT(1).     ---
      if (TX .lt. TABTBN(2)) then
        NTX=int((TX - TABTBN(1)) / TABDTB(1)) + IDXTBN(1)
      else if (TX .lt. TABTBN(3)) then
        NTX=int((TX - TABTBN(2)) / TABDTB(2)) + IDXTBN(2)
      else if (TX .lt. TABTBN(4)) then
        NTX=int((TX - TABTBN(3)) / TABDTB(3)) + IDXTBN(3)
      else
        NTX=NT - 1
      endif
      iTx=min(max(2,nTx),nT-2)
!
!     --- Determine interval index NPX for pressure interpolation ---
      if (PX .lt. TABPBN(2)) then
        NPX=int((PX - TABPBN(1)) / TABDPB(1)) + IDXPBN(1)
      else if (PX .lt. TABPBN(3)) then
        NPX=int((PX - TABPBN(2)) / TABDPB(2)) + IDXPBN(2)
      else if (PX .lt. TABPBN(4)) then
        NPX=int((PX - TABPBN(3)) / TABDPB(3)) + IDXPBN(3)
      else
        NPX = NP - 1
      endif
      iPx=min(max(2,nPx),nP-2)
!
!     The interpolation procedure:
!  
!        P
!        ^
!        |       +: TABKAP, #: PKAP, *: (P,T)-value
!        |
!        |                |     |     |     |
!        |                |     |     |     |
!        |    NPX+2  -----+-----+-#---+-----+-----
!        |                |     |     |     |
!        |                |     |     |     |
!        |    NPX+1  -----+-----+-#---+-----+-----
!        |                |     |     |     |
!        |                |     | *   |     |
!        |    NPX    -----+-----+-#---+-----+-----
!        |                |     |     |     |
!        |                |     |     |     |
!        |    NPX-1  -----+-----+-#---+-----+-----
!        |                |     |     |     |
!        |                |     |     |     |
!        |
!        |              NTX-1  NTX  NTX+1 NTX+2
!        -----------------------------------------------------> T
!
!     --- Geometry factors of T table ---
!----------------------------------------------------------------------+-------
      gT1=  (Tx         -tabT(iTx  ))  * (Tx         -tabT(iTx+1))**2 / &
          ( (tabT(iTx-1)-tabT(iTx  ))  * (tabT(iTx-1)-tabT(iTx+1)) *    &
            (tabT(iTx  )-tabT(iTx+1)) )
      gT2=  (Tx         -tabT(iTx+1))  * &
          (-(tabT(iTx-1)-tabT(iTx+2))  * (Tx         -tabT(iTx  ))**2 - &
            (tabT(iTx  )-tabT(iTx+1))  * (tabT(iTx  )-tabT(iTx+2)) *    &
            (Tx         -tabT(iTx-1)) ) / &
          ( (tabT(iTx-1)-tabT(iTx  ))  * (tabT(iTx  )-tabT(iTx+1))**2*  &
            (tabT(iTx  )-tabT(iTx+2)) )
      gT3=  (Tx         -tabT(iTx  ))  * &
          ( (tabT(iTx-1)-tabT(iTx+2))  * (Tx         -tabT(iTx+1))**2 - &
            (tabT(iTx-1)-tabT(iTx+1))  * (tabT(iTx  )-tabT(iTx+1)) *    &
            (Tx         -tabT(iTx+2)) ) / &
          ( (tabT(iTx-1)-tabT(iTx+1))  * (tabT(iTx  )-tabT(iTx+1))**2*  &
            (tabT(iTx+1)-tabT(iTx+2)) )
      gT4= -(Tx         -tabT(iTx  ))**2*(Tx         -tabT(iTx+1)) /    &
          ( (tabT(iTx  )-tabT(iTx+1))  * (tabT(iTx  )-tabT(iTx+2)) *    &
            (tabT(iTx+1)-tabT(iTx+2)) )
!
!     --- Geometry factors of P table ---
      gP1=  (Px         -tabP(iPx  ))  * (Px         -tabP(iPx+1))**2 / &
          ( (tabP(iPx-1)-tabP(iPx  ))  * (tabP(iPx-1)-tabP(iPx+1)) *    &
            (tabP(iPx  )-tabP(iPx+1)) )
      gP2=  (Px         -tabP(iPx+1))  * &
          (-(tabP(iPx-1)-tabP(iPx+2))  * (Px         -tabP(iPx  ))**2 - &
            (tabP(iPx  )-tabP(iPx+1))  * (tabP(iPx  )-tabP(iPx+2)) *    &
            (Px         -tabP(iPx-1)) ) / &
          ( (tabP(iPx-1)-tabP(iPx  ))  * (tabP(iPx  )-tabP(iPx+1))**2*  &
            (tabP(iPx  )-tabP(iPx+2)) )
      gP3=  (Px         -tabP(iPx  ))  * &
          ( (tabP(iPx-1)-tabP(iPx+2))  * (Px         -tabP(iPx+1))**2 - &
            (tabP(iPx-1)-tabP(iPx+1))  * (tabP(iPx  )-tabP(iPx+1)) *    &
            (Px         -tabP(iPx+2)) ) / &
          ( (tabP(iPx-1)-tabP(iPx+1))  * (tabP(iPx  )-tabP(iPx+1))**2*  &
            (tabP(iPx+1)-tabP(iPx+2)) )
      gP4= -(Px         -tabP(iPx  ))**2*(Px         -tabP(iPx+1)) /    &
          ( (tabP(iPx  )-tabP(iPx+1))  * (tabP(iPx  )-tabP(iPx+2)) *    &
            (tabP(iPx+1)-tabP(iPx+2)) )
!
!     --- Interpolation in T direction ---
      fP1=tabkap(iTx-1,iPx-1,iband)*gT1 + &
          tabkap(iTx  ,iPx-1,iband)*gT2 + &
          tabkap(iTx+1,iPx-1,iband)*gT3 + &
          tabkap(iTx+2,iPx-1,iband)*gT4
      fP2=tabkap(iTx-1,iPx  ,iband)*gT1 + &
          tabkap(iTx  ,iPx  ,iband)*gT2 + &
          tabkap(iTx+1,iPx  ,iband)*gT3 + &
          tabkap(iTx+2,iPx  ,iband)*gT4
      fP3=tabkap(iTx-1,iPx+1,iband)*gT1 + &
          tabkap(iTx  ,iPx+1,iband)*gT2 + &
          tabkap(iTx+1,iPx+1,iband)*gT3 + &
          tabkap(iTx+2,iPx+1,iband)*gT4
      fP4=tabkap(iTx-1,iPx+2,iband)*gT1 + &
          tabkap(iTx  ,iPx+2,iband)*gT2 + &
          tabkap(iTx+1,iPx+2,iband)*gT3 + &
          tabkap(iTx+2,iPx+2,iband)*gT4
!
!     --- Interpolation in P direction ---
      xkaros0=exp(aln10*(fP1*gP1 + fP2*gP2 + fP3*gP3 + fP4*gP4))
!
      end function xkaros0
!------------------*******---------------------------------------------+-------


!--------------*******-------------------------------------------------+-------
      function xkaros1(P, T, iband)
!----------------------------------------------------------------------+-------
! NAME:
!   xkaros1 ('extended_kappa_rosseland_1')
!
! PURPOSE:
!   XKAROS1 computes opacities for given pressure P, temperature T
!   and frequency bin IBAND. It determines the opacities by interpolation in
!   a table given by TABKAP.
!
! CATEGORY:
!   Opacities, Interpolation
!
! CALLING SEQUENCE:
!   kappa=xkaros1(P, T, iband)
!
! INPUT:
!   P:     ('Pressure') real, dimension(:), pressure [dyn/cm^2]
!   T:     ('Temperature') real, dimension(:), temperature [K]
!   iband: ('identification_band') integer, bin identification number
!   
! OUTPUT:
!   xkaros1: ('extended_kappa_rosseland_1'), opacity per gram [cm^2/g]
!
! ROUTINES:
!   None
!
! INCLUDES:
!   None
!
! COMMON BLOCK:
!   XOPTA: opacity table data read by DFOPTA, access: readonly
!
! SIDE EFFECTS:
!   No file access.
!
! RESTRICTIONS:
!   NTBN, NPBN have to be smaller or equal than 4.
!
! PROCEDURE:
!
! EXAMPLE:
!
! MODIFICATION HISTORY:
!   23. 1.91 Hans-Guenter Ludwig, Astrophysik Kiel.
!   27. 5.97 Bernd Freytag, Astrophysik Kiel
!            Use simple interpolation: cubic polynomial through 4 points.
!   28. 5.97 Interpolation: cubic polynomial through 2 points
!            with slopes at both points given by parabolas.
!            Vectorized version.
!   24. 6.97 (B.F.) TX=min(TABT(NT  ), max(TABT(1), alog10(T))) 
!               ->  TX=min(TABT(NT-1), max(TABT(2), alog10(T))) ...
!----------------------------------------------------------------------+-------
!
!     --- XOPTA contains the opacity table data read by DFOPTA ---
      USE xopta_def
!
      IMPLICIT NONE
!
!     --- I/O parameters ---
      real, dimension(:), intent(in) :: P, T
      integer,            intent(in) :: iband
      real, dimension(size(P,1))     :: xkaros1
!
!--- Local Variables ---
      REAL, PARAMETER :: ALN10=2.30258509299405E0
      INTEGER :: iTx, iPx, nTx, nPx, i1
      REAL    :: TX, PX
      REAL    :: gT1,gT2,gT3,gT4, gP1,gP2,gP3,gP4, fP1,fP2,fP3,fP4

!----------------------------------------------------------------------+-------
!
      do i1=1,size(P,1)
!
!     --- Delimit values of TX and PX to the ranges given by TABT and TABP ---
      TX=min(TABT(NT-1), max(TABT(2), alog10(T(i1))))
      PX=min(TABP(NP-1), max(TABP(2), alog10(P(i1))))
!
!     --- Determine interval index NTX for temperature interpolation ---
!     --- The interval index is defined different for MAP and CUBIT! ---
!     --- It is assumed that TX can not be smaller than TABT(1).     ---
      if (TX .lt. TABTBN(2)) then
        NTX=int((TX - TABTBN(1)) / TABDTB(1)) + IDXTBN(1)
      else if (TX .lt. TABTBN(3)) then
        NTX=int((TX - TABTBN(2)) / TABDTB(2)) + IDXTBN(2)
      else if (TX .lt. TABTBN(4)) then
        NTX=int((TX - TABTBN(3)) / TABDTB(3)) + IDXTBN(3)
      else
        NTX=NT - 1
      endif
      iTx=min(max(2,nTx),nT-2)
!
!     --- Determine interval index NPX for pressure interpolation ---
      if (PX .lt. TABPBN(2)) then
        NPX=int((PX - TABPBN(1)) / TABDPB(1)) + IDXPBN(1)
      else if (PX .lt. TABPBN(3)) then
        NPX=int((PX - TABPBN(2)) / TABDPB(2)) + IDXPBN(2)
      else if (PX .lt. TABPBN(4)) then
        NPX=int((PX - TABPBN(3)) / TABDPB(3)) + IDXPBN(3)
      else
        NPX = NP - 1
      endif
      iPx=min(max(2,nPx),nP-2)
!
!     The interpolation procedure:
!  
!        P
!        ^
!        |       +: TABKAP, #: PKAP, *: (P,T)-value
!        |
!        |                |     |     |     |
!        |                |     |     |     |
!        |    NPX+2  -----+-----+-#---+-----+-----
!        |                |     |     |     |
!        |                |     |     |     |
!        |    NPX+1  -----+-----+-#---+-----+-----
!        |                |     |     |     |
!        |                |     | *   |     |
!        |    NPX    -----+-----+-#---+-----+-----
!        |                |     |     |     |
!        |                |     |     |     |
!        |    NPX-1  -----+-----+-#---+-----+-----
!        |                |     |     |     |
!        |                |     |     |     |
!        |
!        |              NTX-1  NTX  NTX+1 NTX+2
!        -----------------------------------------------------> T
!
!     --- Geometry factors of T table ---
!----------------------------------------------------------------------+-------
      gT1=  (Tx         -tabT(iTx  ))  * (Tx         -tabT(iTx+1))**2 / &
          ( (tabT(iTx-1)-tabT(iTx  ))  * (tabT(iTx-1)-tabT(iTx+1)) *    &
            (tabT(iTx  )-tabT(iTx+1)) )
      gT2=  (Tx         -tabT(iTx+1))  * &
          (-(tabT(iTx-1)-tabT(iTx+2))  * (Tx         -tabT(iTx  ))**2 - &
            (tabT(iTx  )-tabT(iTx+1))  * (tabT(iTx  )-tabT(iTx+2)) *    &
            (Tx         -tabT(iTx-1)) ) / &
          ( (tabT(iTx-1)-tabT(iTx  ))  * (tabT(iTx  )-tabT(iTx+1))**2*  &
            (tabT(iTx  )-tabT(iTx+2)) )
      gT3=  (Tx         -tabT(iTx  ))  * &
          ( (tabT(iTx-1)-tabT(iTx+2))  * (Tx         -tabT(iTx+1))**2 - &
            (tabT(iTx-1)-tabT(iTx+1))  * (tabT(iTx  )-tabT(iTx+1)) *    &
            (Tx         -tabT(iTx+2)) ) / &
          ( (tabT(iTx-1)-tabT(iTx+1))  * (tabT(iTx  )-tabT(iTx+1))**2*  &
            (tabT(iTx+1)-tabT(iTx+2)) )
      gT4= -(Tx         -tabT(iTx  ))**2*(Tx         -tabT(iTx+1)) /    &
          ( (tabT(iTx  )-tabT(iTx+1))  * (tabT(iTx  )-tabT(iTx+2)) *    &
            (tabT(iTx+1)-tabT(iTx+2)) )
!
!     --- Geometry factors of P table ---
      gP1=  (Px         -tabP(iPx  ))  * (Px         -tabP(iPx+1))**2 / &
          ( (tabP(iPx-1)-tabP(iPx  ))  * (tabP(iPx-1)-tabP(iPx+1)) *    &
            (tabP(iPx  )-tabP(iPx+1)) )
      gP2=  (Px         -tabP(iPx+1))  * &
          (-(tabP(iPx-1)-tabP(iPx+2))  * (Px         -tabP(iPx  ))**2 - &
            (tabP(iPx  )-tabP(iPx+1))  * (tabP(iPx  )-tabP(iPx+2)) *    &
            (Px         -tabP(iPx-1)) ) / &
          ( (tabP(iPx-1)-tabP(iPx  ))  * (tabP(iPx  )-tabP(iPx+1))**2*  &
            (tabP(iPx  )-tabP(iPx+2)) )
      gP3=  (Px         -tabP(iPx  ))  * &
          ( (tabP(iPx-1)-tabP(iPx+2))  * (Px         -tabP(iPx+1))**2 - &
            (tabP(iPx-1)-tabP(iPx+1))  * (tabP(iPx  )-tabP(iPx+1)) *    &
            (Px         -tabP(iPx+2)) ) / &
          ( (tabP(iPx-1)-tabP(iPx+1))  * (tabP(iPx  )-tabP(iPx+1))**2*  &
            (tabP(iPx+1)-tabP(iPx+2)) )
      gP4= -(Px         -tabP(iPx  ))**2*(Px         -tabP(iPx+1)) /    &
          ( (tabP(iPx  )-tabP(iPx+1))  * (tabP(iPx  )-tabP(iPx+2)) *    &
            (tabP(iPx+1)-tabP(iPx+2)) )
!
!     --- Interpolation in T direction ---
      fP1=tabkap(iTx-1,iPx-1,iband)*gT1 + &
          tabkap(iTx  ,iPx-1,iband)*gT2 + &
          tabkap(iTx+1,iPx-1,iband)*gT3 + &
          tabkap(iTx+2,iPx-1,iband)*gT4
      fP2=tabkap(iTx-1,iPx  ,iband)*gT1 + &
          tabkap(iTx  ,iPx  ,iband)*gT2 + &
          tabkap(iTx+1,iPx  ,iband)*gT3 + &
          tabkap(iTx+2,iPx  ,iband)*gT4
      fP3=tabkap(iTx-1,iPx+1,iband)*gT1 + &
          tabkap(iTx  ,iPx+1,iband)*gT2 + &
          tabkap(iTx+1,iPx+1,iband)*gT3 + &
          tabkap(iTx+2,iPx+1,iband)*gT4
      fP4=tabkap(iTx-1,iPx+2,iband)*gT1 + &
          tabkap(iTx  ,iPx+2,iband)*gT2 + &
          tabkap(iTx+1,iPx+2,iband)*gT3 + &
          tabkap(iTx+2,iPx+2,iband)*gT4
!
!     --- Interpolation in P direction ---
      xkaros1(i1)=exp(aln10*(fP1*gP1 + fP2*gP2 + fP3*gP3 + fP4*gP4))
!
      end do ! i1
!
      end function xkaros1
!------------------*******---------------------------------------------+-------


!--------------*******-------------------------------------------------+-------
      function xkaros2(P, T, iband)
!----------------------------------------------------------------------+-------
! NAME:
!   xkaros2 ('extended_kappa_rosseland_2')
!
! PURPOSE:
!   XKAROS2 computes opacities for given pressure P, temperature T
!   and frequency bin IBAND. It determines the opacities by interpolation in
!   a table given by TABKAP.
!
! CATEGORY:
!   Opacities, Interpolation
!
! CALLING SEQUENCE:
!   kappa=xkaros2(P, T, iband)
!
! INPUT:
!   P:     ('Pressure') real, dimension(:,:), pressure [dyn/cm^2]
!   T:     ('Temperature') real, dimension(:,:), temperature [K]
!   iband: ('identification_band') integer, bin identification number
!   
! OUTPUT:
!   xkaros2: ('extended_kappa_rosseland_2'), opacity per gram [cm^2/g]
!
! ROUTINES:
!   None
!
! INCLUDES:
!   None
!
! COMMON BLOCK:
!   XOPTA: opacity table data read by DFOPTA, access: readonly
!
! SIDE EFFECTS:
!   No file access.
!
! RESTRICTIONS:
!   NTBN, NPBN have to be smaller or equal than 4.
!
! PROCEDURE:
!
! EXAMPLE:
!
! MODIFICATION HISTORY:
!   23. 1.91 Hans-Guenter Ludwig, Astrophysik Kiel.
!   27. 5.97 Bernd Freytag, Astrophysik Kiel
!            Use simple interpolation: cubic polynomial through 4 points.
!   28. 5.97 Interpolation: cubic polynomial through 2 points
!            with slopes at both points given by parabolas.
!            Vectorized version.
!   24. 6.97 (B.F.) TX=min(TABT(NT  ), max(TABT(1), alog10(T))) 
!               ->  TX=min(TABT(NT-1), max(TABT(2), alog10(T))) ...
!    8. 2.01 (B.F.) 2D version
!----------------------------------------------------------------------+-------
!
!     --- XOPTA contains the opacity table data read by DFOPTA ---
      USE xopta_def
!
      IMPLICIT NONE
!
!     --- I/O parameters ---
      real, dimension(:,:),     intent(in) :: P, T
      integer,                  intent(in) :: iband
      real, dimension(size(P,1),size(P,2)) :: xkaros2
!
!--- Local Variables ---
      REAL, PARAMETER :: ALN10=2.30258509299405E0
      INTEGER :: iTx,iPx, nTx, nPx, i1, i2
      REAL    :: TX, PX
      REAL    :: gT1,gT2,gT3,gT4, gP1,gP2,gP3,gP4, fP1,fP2,fP3,fP4
!
!----------------------------------------------------------------------+-------
!
      do i2=1,size(P,2)
      do i1=1,size(P,1)
!
!     --- Delimit values of TX and PX to the ranges given by TABT and TABP ---
      TX=min(TABT(NT-1), max(TABT(2), alog10(T(i1,i2))))
      PX=min(TABP(NP-1), max(TABP(2), alog10(P(i1,i2))))
!
!     --- Determine interval index NTX for temperature interpolation ---
!     --- The interval index is defined different for MAP and CUBIT! ---
!     --- It is assumed that TX can not be smaller than TABT(1).     ---
      if (TX .lt. TABTBN(2)) then
        NTX=int((TX - TABTBN(1)) / TABDTB(1)) + IDXTBN(1)
      else if (TX .lt. TABTBN(3)) then
        NTX=int((TX - TABTBN(2)) / TABDTB(2)) + IDXTBN(2)
      else if (TX .lt. TABTBN(4)) then
        NTX=int((TX - TABTBN(3)) / TABDTB(3)) + IDXTBN(3)
      else
        NTX=NT - 1
      endif
      iTx=min(max(2,nTx),nT-2)
!
!     --- Determine interval index NPX for pressure interpolation ---
      if (PX .lt. TABPBN(2)) then
        NPX=int((PX - TABPBN(1)) / TABDPB(1)) + IDXPBN(1)
      else if (PX .lt. TABPBN(3)) then
        NPX=int((PX - TABPBN(2)) / TABDPB(2)) + IDXPBN(2)
      else if (PX .lt. TABPBN(4)) then
        NPX=int((PX - TABPBN(3)) / TABDPB(3)) + IDXPBN(3)
      else
        NPX = NP - 1
      endif
      iPx=min(max(2,nPx),nP-2)
!
!     The interpolation procedure:
!  
!        P
!        ^
!        |       +: TABKAP, #: PKAP, *: (P,T)-value
!        |
!        |                |     |     |     |
!        |                |     |     |     |
!        |    NPX+2  -----+-----+-#---+-----+-----
!        |                |     |     |     |
!        |                |     |     |     |
!        |    NPX+1  -----+-----+-#---+-----+-----
!        |                |     |     |     |
!        |                |     | *   |     |
!        |    NPX    -----+-----+-#---+-----+-----
!        |                |     |     |     |
!        |                |     |     |     |
!        |    NPX-1  -----+-----+-#---+-----+-----
!        |                |     |     |     |
!        |                |     |     |     |
!        |
!        |              NTX-1  NTX  NTX+1 NTX+2
!        -----------------------------------------------------> T
!
!     --- Geometry factors of T table ---
!----------------------------------------------------------------------+-------
      gT1=  (Tx         -tabT(iTx  ))  * (Tx         -tabT(iTx+1))**2 / &
          ( (tabT(iTx-1)-tabT(iTx  ))  * (tabT(iTx-1)-tabT(iTx+1)) *    &
            (tabT(iTx  )-tabT(iTx+1)) )
      gT2=  (Tx         -tabT(iTx+1))  * &
          (-(tabT(iTx-1)-tabT(iTx+2))  * (Tx         -tabT(iTx  ))**2 - &
            (tabT(iTx  )-tabT(iTx+1))  * (tabT(iTx  )-tabT(iTx+2)) *    &
            (Tx         -tabT(iTx-1)) ) / &
          ( (tabT(iTx-1)-tabT(iTx  ))  * (tabT(iTx  )-tabT(iTx+1))**2*  &
            (tabT(iTx  )-tabT(iTx+2)) )
      gT3=  (Tx         -tabT(iTx  ))  * &
          ( (tabT(iTx-1)-tabT(iTx+2))  * (Tx         -tabT(iTx+1))**2 - &
            (tabT(iTx-1)-tabT(iTx+1))  * (tabT(iTx  )-tabT(iTx+1)) *    &
            (Tx         -tabT(iTx+2)) ) / &
          ( (tabT(iTx-1)-tabT(iTx+1))  * (tabT(iTx  )-tabT(iTx+1))**2*  &
            (tabT(iTx+1)-tabT(iTx+2)) )
      gT4= -(Tx         -tabT(iTx  ))**2*(Tx         -tabT(iTx+1)) /    &
          ( (tabT(iTx  )-tabT(iTx+1))  * (tabT(iTx  )-tabT(iTx+2)) *    &
            (tabT(iTx+1)-tabT(iTx+2)) )
!
!     --- Geometry factors of P table ---
      gP1=  (Px         -tabP(iPx  ))  * (Px         -tabP(iPx+1))**2 / &
          ( (tabP(iPx-1)-tabP(iPx  ))  * (tabP(iPx-1)-tabP(iPx+1)) *    &
            (tabP(iPx  )-tabP(iPx+1)) )
      gP2=  (Px         -tabP(iPx+1))  * &
          (-(tabP(iPx-1)-tabP(iPx+2))  * (Px         -tabP(iPx  ))**2 - &
            (tabP(iPx  )-tabP(iPx+1))  * (tabP(iPx  )-tabP(iPx+2)) *    &
            (Px         -tabP(iPx-1)) ) / &
          ( (tabP(iPx-1)-tabP(iPx  ))  * (tabP(iPx  )-tabP(iPx+1))**2*  &
            (tabP(iPx  )-tabP(iPx+2)) )
      gP3=  (Px         -tabP(iPx  ))  * &
          ( (tabP(iPx-1)-tabP(iPx+2))  * (Px         -tabP(iPx+1))**2 - &
            (tabP(iPx-1)-tabP(iPx+1))  * (tabP(iPx  )-tabP(iPx+1)) *    &
            (Px         -tabP(iPx+2)) ) / &
          ( (tabP(iPx-1)-tabP(iPx+1))  * (tabP(iPx  )-tabP(iPx+1))**2*  &
            (tabP(iPx+1)-tabP(iPx+2)) )
      gP4= -(Px         -tabP(iPx  ))**2*(Px         -tabP(iPx+1)) /    &
          ( (tabP(iPx  )-tabP(iPx+1))  * (tabP(iPx  )-tabP(iPx+2)) *    &
            (tabP(iPx+1)-tabP(iPx+2)) )
!
!     --- Interpolation in T direction ---
      fP1=tabkap(iTx-1,iPx-1,iband)*gT1 + &
          tabkap(iTx  ,iPx-1,iband)*gT2 + &
          tabkap(iTx+1,iPx-1,iband)*gT3 + &
          tabkap(iTx+2,iPx-1,iband)*gT4
      fP2=tabkap(iTx-1,iPx  ,iband)*gT1 + &
          tabkap(iTx  ,iPx  ,iband)*gT2 + &
          tabkap(iTx+1,iPx  ,iband)*gT3 + &
          tabkap(iTx+2,iPx  ,iband)*gT4
      fP3=tabkap(iTx-1,iPx+1,iband)*gT1 + &
          tabkap(iTx  ,iPx+1,iband)*gT2 + &
          tabkap(iTx+1,iPx+1,iband)*gT3 + &
          tabkap(iTx+2,iPx+1,iband)*gT4
      fP4=tabkap(iTx-1,iPx+2,iband)*gT1 + &
          tabkap(iTx  ,iPx+2,iband)*gT2 + &
          tabkap(iTx+1,iPx+2,iband)*gT3 + &
          tabkap(iTx+2,iPx+2,iband)*gT4
!
!     --- Interpolation in P direction ---
      xkaros2(i1,i2)=exp(aln10*(fP1*gP1 + fP2*gP2 + fP3*gP3 + fP4*gP4))
!
      end do ! i1
      end do ! i2
!
      end function xkaros2
!------------------*******---------------------------------------------+-------


!--------------*******-------------------------------------------------+-------
      function xkaros3(P, T, iband)
!----------------------------------------------------------------------+-------
! NAME:
!   xkaros3 ('extended_kappa_rosseland_3')
!
! PURPOSE:
!   XKAROS3 computes opacities for given pressure P, temperature T
!   and frequency bin IBAND. It determines the opacities by interpolation in
!   a table given by TABKAP.
!
! CATEGORY:
!   Opacities, Interpolation
!
! CALLING SEQUENCE:
!   kappa=xkaros3(P, T, iband)
!
! INPUT:
!   P:     ('Pressure') real, dimension(:,:,:), pressure [dyn/cm^2]
!   T:     ('Temperature') real, dimension(:,:,:), temperature [K]
!   iband: ('identification_band') integer, bin identification number
!   
! OUTPUT:
!   xkaros3: ('extended_kappa_rosseland_3'), opacity per gram [cm^2/g]
!
! ROUTINES:
!   None
!
! INCLUDES:
!   None
!
! COMMON BLOCK:
!   XOPTA: opacity table data read by DFOPTA, access: readonly
!
! SIDE EFFECTS:
!   No file access.
!
! RESTRICTIONS:
!   NTBN, NPBN have to be smaller or equal than 4.
!
! PROCEDURE:
!
! EXAMPLE:
!
! MODIFICATION HISTORY:
!   23. 1.91 Hans-Guenter Ludwig, Astrophysik Kiel.
!   27. 5.97 Bernd Freytag, Astrophysik Kiel
!            Use simple interpolation: cubic polynomial through 4 points.
!   28. 5.97 Interpolation: cubic polynomial through 2 points
!            with slopes at both points given by parabolas.
!            Vectorized version.
!   24. 6.97 (B.F.) TX=min(TABT(NT  ), max(TABT(1), alog10(T))) 
!               ->  TX=min(TABT(NT-1), max(TABT(2), alog10(T))) ...
!    8. 2.01 (B.F.) 2D version
!   20. 2.01 (B.F.) 3D version
!----------------------------------------------------------------------+-------
!
!     --- XOPTA contains the opacity table data read by DFOPTA ---
      USE xopta_def
!
      IMPLICIT NONE
!
!     --- I/O parameters ---
      real, dimension(:,:,:),             intent(in) :: P, T
      integer,                            intent(in) :: iband
      real, dimension(size(P,1),size(P,2),size(P,3)) :: xkaros3
!
!--- Local Variables ---
      REAL, PARAMETER :: ALN10=2.30258509299405E0
      INTEGER :: iTx,iPx, nTx, nPx, i1, i2, i3
      REAL    :: TX, PX
      REAL    :: gT1,gT2,gT3,gT4, gP1,gP2,gP3,gP4, fP1,fP2,fP3,fP4
!
!----------------------------------------------------------------------+-------
!
      do i3=1,size(P,3)
      do i2=1,size(P,2)
      do i1=1,size(P,1)
!
!     --- Delimit values of TX and PX to the ranges given by TABT and TABP ---
      TX=min(TABT(NT-1), max(TABT(2), alog10(T(i1,i2,i3))))
      PX=min(TABP(NP-1), max(TABP(2), alog10(P(i1,i2,i3))))
!
!     --- Determine interval index NTX for temperature interpolation ---
!     --- The interval index is defined different for MAP and CUBIT! ---
!     --- It is assumed that TX can not be smaller than TABT(1).     ---
      if (TX .lt. TABTBN(2)) then
        NTX=int((TX - TABTBN(1)) / TABDTB(1)) + IDXTBN(1)
      else if (TX .lt. TABTBN(3)) then
        NTX=int((TX - TABTBN(2)) / TABDTB(2)) + IDXTBN(2)
      else if (TX .lt. TABTBN(4)) then
        NTX=int((TX - TABTBN(3)) / TABDTB(3)) + IDXTBN(3)
      else
        NTX=NT - 1
      endif
      iTx=min(max(2,nTx),nT-2)
!
!     --- Determine interval index NPX for pressure interpolation ---
      if (PX .lt. TABPBN(2)) then
        NPX=int((PX - TABPBN(1)) / TABDPB(1)) + IDXPBN(1)
      else if (PX .lt. TABPBN(3)) then
        NPX=int((PX - TABPBN(2)) / TABDPB(2)) + IDXPBN(2)
      else if (PX .lt. TABPBN(4)) then
        NPX=int((PX - TABPBN(3)) / TABDPB(3)) + IDXPBN(3)
      else
        NPX = NP - 1
      endif
      iPx=min(max(2,nPx),nP-2)
!
!     The interpolation procedure:
!  
!        P
!        ^
!        |       +: TABKAP, #: PKAP, *: (P,T)-value
!        |
!        |                |     |     |     |
!        |                |     |     |     |
!        |    NPX+2  -----+-----+-#---+-----+-----
!        |                |     |     |     |
!        |                |     |     |     |
!        |    NPX+1  -----+-----+-#---+-----+-----
!        |                |     |     |     |
!        |                |     | *   |     |
!        |    NPX    -----+-----+-#---+-----+-----
!        |                |     |     |     |
!        |                |     |     |     |
!        |    NPX-1  -----+-----+-#---+-----+-----
!        |                |     |     |     |
!        |                |     |     |     |
!        |
!        |              NTX-1  NTX  NTX+1 NTX+2
!        -----------------------------------------------------> T
!
!     --- Geometry factors of T table ---
!----------------------------------------------------------------------+-------
      gT1=  (Tx         -tabT(iTx  ))  * (Tx         -tabT(iTx+1))**2 / &
          ( (tabT(iTx-1)-tabT(iTx  ))  * (tabT(iTx-1)-tabT(iTx+1)) *    &
            (tabT(iTx  )-tabT(iTx+1)) )
      gT2=  (Tx         -tabT(iTx+1))  * &
          (-(tabT(iTx-1)-tabT(iTx+2))  * (Tx         -tabT(iTx  ))**2 - &
            (tabT(iTx  )-tabT(iTx+1))  * (tabT(iTx  )-tabT(iTx+2)) *    &
            (Tx         -tabT(iTx-1)) ) / &
          ( (tabT(iTx-1)-tabT(iTx  ))  * (tabT(iTx  )-tabT(iTx+1))**2*  &
            (tabT(iTx  )-tabT(iTx+2)) )
      gT3=  (Tx         -tabT(iTx  ))  * &
          ( (tabT(iTx-1)-tabT(iTx+2))  * (Tx         -tabT(iTx+1))**2 - &
            (tabT(iTx-1)-tabT(iTx+1))  * (tabT(iTx  )-tabT(iTx+1)) *    &
            (Tx         -tabT(iTx+2)) ) / &
          ( (tabT(iTx-1)-tabT(iTx+1))  * (tabT(iTx  )-tabT(iTx+1))**2*  &
            (tabT(iTx+1)-tabT(iTx+2)) )
      gT4= -(Tx         -tabT(iTx  ))**2*(Tx         -tabT(iTx+1)) /    &
          ( (tabT(iTx  )-tabT(iTx+1))  * (tabT(iTx  )-tabT(iTx+2)) *    &
            (tabT(iTx+1)-tabT(iTx+2)) )
!
!     --- Geometry factors of P table ---
      gP1=  (Px         -tabP(iPx  ))  * (Px         -tabP(iPx+1))**2 / &
          ( (tabP(iPx-1)-tabP(iPx  ))  * (tabP(iPx-1)-tabP(iPx+1)) *    &
            (tabP(iPx  )-tabP(iPx+1)) )
      gP2=  (Px         -tabP(iPx+1))  * &
          (-(tabP(iPx-1)-tabP(iPx+2))  * (Px         -tabP(iPx  ))**2 - &
            (tabP(iPx  )-tabP(iPx+1))  * (tabP(iPx  )-tabP(iPx+2)) *    &
            (Px         -tabP(iPx-1)) ) / &
          ( (tabP(iPx-1)-tabP(iPx  ))  * (tabP(iPx  )-tabP(iPx+1))**2*  &
            (tabP(iPx  )-tabP(iPx+2)) )
      gP3=  (Px         -tabP(iPx  ))  * &
          ( (tabP(iPx-1)-tabP(iPx+2))  * (Px         -tabP(iPx+1))**2 - &
            (tabP(iPx-1)-tabP(iPx+1))  * (tabP(iPx  )-tabP(iPx+1)) *    &
            (Px         -tabP(iPx+2)) ) / &
          ( (tabP(iPx-1)-tabP(iPx+1))  * (tabP(iPx  )-tabP(iPx+1))**2*  &
            (tabP(iPx+1)-tabP(iPx+2)) )
      gP4= -(Px         -tabP(iPx  ))**2*(Px         -tabP(iPx+1)) /    &
          ( (tabP(iPx  )-tabP(iPx+1))  * (tabP(iPx  )-tabP(iPx+2)) *    &
            (tabP(iPx+1)-tabP(iPx+2)) )
!
!     --- Interpolation in T direction ---
      fP1=tabkap(iTx-1,iPx-1,iband)*gT1 + &
          tabkap(iTx  ,iPx-1,iband)*gT2 + &
          tabkap(iTx+1,iPx-1,iband)*gT3 + &
          tabkap(iTx+2,iPx-1,iband)*gT4
      fP2=tabkap(iTx-1,iPx  ,iband)*gT1 + &
          tabkap(iTx  ,iPx  ,iband)*gT2 + &
          tabkap(iTx+1,iPx  ,iband)*gT3 + &
          tabkap(iTx+2,iPx  ,iband)*gT4
      fP3=tabkap(iTx-1,iPx+1,iband)*gT1 + &
          tabkap(iTx  ,iPx+1,iband)*gT2 + &
          tabkap(iTx+1,iPx+1,iband)*gT3 + &
          tabkap(iTx+2,iPx+1,iband)*gT4
      fP4=tabkap(iTx-1,iPx+2,iband)*gT1 + &
          tabkap(iTx  ,iPx+2,iband)*gT2 + &
          tabkap(iTx+1,iPx+2,iband)*gT3 + &
          tabkap(iTx+2,iPx+2,iband)*gT4
!
!     --- Interpolation in P direction ---
      xkaros3(i1,i2,i3)=exp(aln10*(fP1*gP1 + fP2*gP2 + fP3*gP3 + fP4*gP4))
!
      end do ! i1
      end do ! i2
      end do ! i3
!
      end function xkaros3
!------------------*******---------------------------------------------+-------


!----------------***********************-------------------------------+-------
      subroutine sub_xkaros2_logPT2kappa(logP, T, iband, xkaros)
!----------------------------------------------------------------------+-------
! NAME:
!   sub_xkaros2_logPT2kappa ('subroutine_extended_kappa_rosseland_2_logPT2kappa')
!
! PURPOSE:
!   sub_XKAROS2_logPT2kappa computes opacities for given pressure logP,
!   temperature T and frequency bin IBAND.
!   It determines the opacities by interpolation in
!   a table given by TABKAP.
!
! CATEGORY:
!   Opacities, Interpolation
!
! CALLING SEQUENCE:
!   kappa=xkaros2_logPT2kappa(logP, T, iband)
!
! INPUT:
!   logP:  ('logarithm_Pressure') real, dimension(:,:), natural logarithm
!          of pressure [log(dyn/cm^2)]
!   T:     ('Temperature') real, dimension(:,:), temperature [K]
!   iband: ('identification_band') integer, bin identification number
!   
! OUTPUT:
!   xkaros2_logPT2kappa: ('extended_kappa_rosseland_3'), opacity per gram [cm^2/g]
!
! ROUTINES:
!   None
!
! INCLUDES:
!   None
!
! COMMON BLOCK:
!   XOPTA: opacity table data read by DFOPTA, access: readonly
!
! SIDE EFFECTS:
!   No file access.
!
! RESTRICTIONS:
!   NTBN, NPBN have to be smaller or equal than 4.
!
! PROCEDURE:
!
! EXAMPLE:
!
! MODIFICATION HISTORY:
!   23. 1.91 Hans-Guenter Ludwig, Astrophysik Kiel.
!   27. 5.97 Bernd Freytag, Astrophysik Kiel
!            Use simple interpolation: cubic polynomial through 4 points.
!   28. 5.97 Interpolation: cubic polynomial through 2 points
!            with slopes at both points given by parabolas.
!            Vectorized version.
!   24. 6.97 (B.F.) TX=min(TABT(NT  ), max(TABT(1), alog10(T))) 
!               ->  TX=min(TABT(NT-1), max(TABT(2), alog10(T))) ...
!    8. 2.01 (B.F.) 2D version
!   20. 2.01 (B.F.) 3D version, logP version
!    5. 3.01 (B.F.) subroutine version
!----------------------------------------------------------------------+-------
!
!     --- XOPTA contains the opacity table data read by DFOPTA ---
      USE xopta_def
!
      IMPLICIT NONE
!
!     --- I/O parameters ---
      real, dimension(:,:),          intent(in) :: logP, T
      integer,                       intent(in) :: iband
      real, dimension(:,:),         intent(out) :: xkaros
!
!--- Local Variables ---
      REAL, PARAMETER :: ALN10=2.30258509299405E0
      INTEGER :: iTx,iPx, nTx, nPx, i1, i2
      REAL    :: TX, PX
      REAL    :: gT1,gT2,gT3,gT4, gP1,gP2,gP3,gP4, fP1,fP2,fP3,fP4
!
!----------------------------------------------------------------------+-------
!
      do i2=1,size(T,2)
      do i1=1,size(T,1)
!
!     --- Delimit values of TX and PX to the ranges given by TABT and TABP ---
      TX=min(TABT(NT-1), max(TABT(2), alog10(T(i1,i2))))
!      PX=min(TABP(NP-1), max(TABP(2), alog10(P(i1,i2))))
      PX=min(TABP(NP-1), max(TABP(2), 0.434294481903*logP(i1,i2)))
!
!     --- Determine interval index NTX for temperature interpolation ---
!     --- The interval index is defined different for MAP and CUBIT! ---
!     --- It is assumed that TX can not be smaller than TABT(1).     ---
      if (TX .lt. TABTBN(2)) then
        NTX=int((TX - TABTBN(1)) / TABDTB(1)) + IDXTBN(1)
      else if (TX .lt. TABTBN(3)) then
        NTX=int((TX - TABTBN(2)) / TABDTB(2)) + IDXTBN(2)
      else if (TX .lt. TABTBN(4)) then
        NTX=int((TX - TABTBN(3)) / TABDTB(3)) + IDXTBN(3)
      else
        NTX=NT - 1
      endif
      iTx=min(max(2,nTx),nT-2)
!
!     --- Determine interval index NPX for pressure interpolation ---
      if (PX .lt. TABPBN(2)) then
        NPX=int((PX - TABPBN(1)) / TABDPB(1)) + IDXPBN(1)
      else if (PX .lt. TABPBN(3)) then
        NPX=int((PX - TABPBN(2)) / TABDPB(2)) + IDXPBN(2)
      else if (PX .lt. TABPBN(4)) then
        NPX=int((PX - TABPBN(3)) / TABDPB(3)) + IDXPBN(3)
      else
        NPX = NP - 1
      endif
      iPx=min(max(2,nPx),nP-2)
!
!     The interpolation procedure:
!  
!        P
!        ^
!        |       +: TABKAP, #: PKAP, *: (P,T)-value
!        |
!        |                |     |     |     |
!        |                |     |     |     |
!        |    NPX+2  -----+-----+-#---+-----+-----
!        |                |     |     |     |
!        |                |     |     |     |
!        |    NPX+1  -----+-----+-#---+-----+-----
!        |                |     |     |     |
!        |                |     | *   |     |
!        |    NPX    -----+-----+-#---+-----+-----
!        |                |     |     |     |
!        |                |     |     |     |
!        |    NPX-1  -----+-----+-#---+-----+-----
!        |                |     |     |     |
!        |                |     |     |     |
!        |
!        |              NTX-1  NTX  NTX+1 NTX+2
!        -----------------------------------------------------> T
!
!     --- Geometry factors of T table ---
!----------------------------------------------------------------------+-------
      gT1=  (Tx         -tabT(iTx  ))  * (Tx         -tabT(iTx+1))**2 / &
          ( (tabT(iTx-1)-tabT(iTx  ))  * (tabT(iTx-1)-tabT(iTx+1)) *    &
            (tabT(iTx  )-tabT(iTx+1)) )
      gT2=  (Tx         -tabT(iTx+1))  * &
          (-(tabT(iTx-1)-tabT(iTx+2))  * (Tx         -tabT(iTx  ))**2 - &
            (tabT(iTx  )-tabT(iTx+1))  * (tabT(iTx  )-tabT(iTx+2)) *    &
            (Tx         -tabT(iTx-1)) ) / &
          ( (tabT(iTx-1)-tabT(iTx  ))  * (tabT(iTx  )-tabT(iTx+1))**2*  &
            (tabT(iTx  )-tabT(iTx+2)) )
      gT3=  (Tx         -tabT(iTx  ))  * &
          ( (tabT(iTx-1)-tabT(iTx+2))  * (Tx         -tabT(iTx+1))**2 - &
            (tabT(iTx-1)-tabT(iTx+1))  * (tabT(iTx  )-tabT(iTx+1)) *    &
            (Tx         -tabT(iTx+2)) ) / &
          ( (tabT(iTx-1)-tabT(iTx+1))  * (tabT(iTx  )-tabT(iTx+1))**2*  &
            (tabT(iTx+1)-tabT(iTx+2)) )
      gT4= -(Tx         -tabT(iTx  ))**2*(Tx         -tabT(iTx+1)) /    &
          ( (tabT(iTx  )-tabT(iTx+1))  * (tabT(iTx  )-tabT(iTx+2)) *    &
            (tabT(iTx+1)-tabT(iTx+2)) )
!
!     --- Geometry factors of P table ---
      gP1=  (Px         -tabP(iPx  ))  * (Px         -tabP(iPx+1))**2 / &
          ( (tabP(iPx-1)-tabP(iPx  ))  * (tabP(iPx-1)-tabP(iPx+1)) *    &
            (tabP(iPx  )-tabP(iPx+1)) )
      gP2=  (Px         -tabP(iPx+1))  * &
          (-(tabP(iPx-1)-tabP(iPx+2))  * (Px         -tabP(iPx  ))**2 - &
            (tabP(iPx  )-tabP(iPx+1))  * (tabP(iPx  )-tabP(iPx+2)) *    &
            (Px         -tabP(iPx-1)) ) / &
          ( (tabP(iPx-1)-tabP(iPx  ))  * (tabP(iPx  )-tabP(iPx+1))**2*  &
            (tabP(iPx  )-tabP(iPx+2)) )
      gP3=  (Px         -tabP(iPx  ))  * &
          ( (tabP(iPx-1)-tabP(iPx+2))  * (Px         -tabP(iPx+1))**2 - &
            (tabP(iPx-1)-tabP(iPx+1))  * (tabP(iPx  )-tabP(iPx+1)) *    &
            (Px         -tabP(iPx+2)) ) / &
          ( (tabP(iPx-1)-tabP(iPx+1))  * (tabP(iPx  )-tabP(iPx+1))**2*  &
            (tabP(iPx+1)-tabP(iPx+2)) )
      gP4= -(Px         -tabP(iPx  ))**2*(Px         -tabP(iPx+1)) /    &
          ( (tabP(iPx  )-tabP(iPx+1))  * (tabP(iPx  )-tabP(iPx+2)) *    &
            (tabP(iPx+1)-tabP(iPx+2)) )
!
!     --- Interpolation in T direction ---
      fP1=tabkap(iTx-1,iPx-1,iband)*gT1 + &
          tabkap(iTx  ,iPx-1,iband)*gT2 + &
          tabkap(iTx+1,iPx-1,iband)*gT3 + &
          tabkap(iTx+2,iPx-1,iband)*gT4
      fP2=tabkap(iTx-1,iPx  ,iband)*gT1 + &
          tabkap(iTx  ,iPx  ,iband)*gT2 + &
          tabkap(iTx+1,iPx  ,iband)*gT3 + &
          tabkap(iTx+2,iPx  ,iband)*gT4
      fP3=tabkap(iTx-1,iPx+1,iband)*gT1 + &
          tabkap(iTx  ,iPx+1,iband)*gT2 + &
          tabkap(iTx+1,iPx+1,iband)*gT3 + &
          tabkap(iTx+2,iPx+1,iband)*gT4
      fP4=tabkap(iTx-1,iPx+2,iband)*gT1 + &
          tabkap(iTx  ,iPx+2,iband)*gT2 + &
          tabkap(iTx+1,iPx+2,iband)*gT3 + &
          tabkap(iTx+2,iPx+2,iband)*gT4
!
!     --- Interpolation in P direction ---
      xkaros(i1,i2)= exp(aln10*(fP1*gP1 + fP2*gP2 + fP3*gP3 + fP4*gP4))
!
      end do ! i1
      end do ! i2
!
      end subroutine sub_xkaros2_logPT2kappa
!------------------*******************---------------------------------+-------


!----------------***********************-------------------------------+-------
      subroutine sub_xkaros3_logPT2kappa(logP, T, iband, xkaros)
!----------------------------------------------------------------------+-------
! NAME:
!   sub_xkaros3_logPT2kappa ('subroutine_extended_kappa_rosseland_2_logPT2kappa')
!
! PURPOSE:
!   sub_XKAROS3_logPT2kappa computes opacities for given pressure logP,
!   temperature T and frequency bin IBAND.
!   It determines the opacities by interpolation in
!   a table given by TABKAP.
!
! CATEGORY:
!   Opacities, Interpolation
!
! CALLING SEQUENCE:
!   kappa=xkaros3_logPT2kappa(logP, T, iband)
!
! INPUT:
!   logP:  ('logarithm_Pressure') real, dimension(:,:), natural logarithm
!          of pressure [log(dyn/cm^2)]
!   T:     ('Temperature') real, dimension(:,:), temperature [K]
!   iband: ('identification_band') integer, bin identification number
!   
! OUTPUT:
!   xkaros2_logPT2kappa: ('extended_kappa_rosseland_3'), opacity per gram [cm^2/g]
!
! ROUTINES:
!   None
!
! INCLUDES:
!   None
!
! COMMON BLOCK:
!   XOPTA: opacity table data read by DFOPTA, access: readonly
!
! SIDE EFFECTS:
!   No file access.
!
! RESTRICTIONS:
!   NTBN, NPBN have to be smaller or equal than 4.
!
! PROCEDURE:
!
! EXAMPLE:
!
! MODIFICATION HISTORY:
!   23. 1.91 Hans-Guenter Ludwig, Astrophysik Kiel.
!   27. 5.97 Bernd Freytag, Astrophysik Kiel
!            Use simple interpolation: cubic polynomial through 4 points.
!   28. 5.97 Interpolation: cubic polynomial through 2 points
!            with slopes at both points given by parabolas.
!            Vectorized version.
!   24. 6.97 (B.F.) TX=min(TABT(NT  ), max(TABT(1), alog10(T))) 
!               ->  TX=min(TABT(NT-1), max(TABT(2), alog10(T))) ...
!    8. 2.01 (B.F.) 2D version
!   20. 2.01 (B.F.) 3D version, logP version
!    6. 3.01 (B.F.) subroutine version
!----------------------------------------------------------------------+-------
!
!     --- XOPTA contains the opacity table data read by DFOPTA ---
      USE xopta_def
!
      IMPLICIT NONE
!
!     --- I/O parameters ---
      real, dimension(:,:,:),        intent(in) :: logP, T
      integer,                       intent(in) :: iband
      real, dimension(:,:,:),       intent(out) :: xkaros
!
!--- Local Variables ---
      REAL, PARAMETER :: ALN10=2.30258509299405E0
      INTEGER :: iTx,iPx, nTx, nPx, i1, i2, i3
      REAL    :: TX, PX
      REAL    :: gT1,gT2,gT3,gT4, gP1,gP2,gP3,gP4, fP1,fP2,fP3,fP4
!
!----------------------------------------------------------------------+-------
!
!$OMP DO
      do i3=1,size(T,3)
      do i2=1,size(T,2)
      do i1=1,size(T,1)
!
!     --- Delimit values of TX and PX to the ranges given by TABT and TABP ---
      TX=min(TABT(NT-1), max(TABT(2), alog10(T(i1,i2,i3))))
!      PX=min(TABP(NP-1), max(TABP(2), alog10(P(i1,i2,i3))))
      PX=min(TABP(NP-1), max(TABP(2), 0.434294481903*logP(i1,i2,i3)))
!
!     --- Determine interval index NTX for temperature interpolation ---
!     --- The interval index is defined different for MAP and CUBIT! ---
!     --- It is assumed that TX can not be smaller than TABT(1).     ---
      if (TX .lt. TABTBN(2)) then
        NTX=int((TX - TABTBN(1)) / TABDTB(1)) + IDXTBN(1)
      else if (TX .lt. TABTBN(3)) then
        NTX=int((TX - TABTBN(2)) / TABDTB(2)) + IDXTBN(2)
      else if (TX .lt. TABTBN(4)) then
        NTX=int((TX - TABTBN(3)) / TABDTB(3)) + IDXTBN(3)
      else
        NTX=NT - 1
      endif
      iTx=min(max(2,nTx),nT-2)
!
!     --- Determine interval index NPX for pressure interpolation ---
      if (PX .lt. TABPBN(2)) then
        NPX=int((PX - TABPBN(1)) / TABDPB(1)) + IDXPBN(1)
      else if (PX .lt. TABPBN(3)) then
        NPX=int((PX - TABPBN(2)) / TABDPB(2)) + IDXPBN(2)
      else if (PX .lt. TABPBN(4)) then
        NPX=int((PX - TABPBN(3)) / TABDPB(3)) + IDXPBN(3)
      else
        NPX = NP - 1
      endif
      iPx=min(max(2,nPx),nP-2)
!
!     The interpolation procedure:
!  
!        P
!        ^
!        |       +: TABKAP, #: PKAP, *: (P,T)-value
!        |
!        |                |     |     |     |
!        |                |     |     |     |
!        |    NPX+2  -----+-----+-#---+-----+-----
!        |                |     |     |     |
!        |                |     |     |     |
!        |    NPX+1  -----+-----+-#---+-----+-----
!        |                |     |     |     |
!        |                |     | *   |     |
!        |    NPX    -----+-----+-#---+-----+-----
!        |                |     |     |     |
!        |                |     |     |     |
!        |    NPX-1  -----+-----+-#---+-----+-----
!        |                |     |     |     |
!        |                |     |     |     |
!        |
!        |              NTX-1  NTX  NTX+1 NTX+2
!        -----------------------------------------------------> T
!
!     --- Geometry factors of T table ---
!----------------------------------------------------------------------+-------
      gT1=  (Tx         -tabT(iTx  ))  * (Tx         -tabT(iTx+1))**2 / &
          ( (tabT(iTx-1)-tabT(iTx  ))  * (tabT(iTx-1)-tabT(iTx+1)) *    &
            (tabT(iTx  )-tabT(iTx+1)) )
      gT2=  (Tx         -tabT(iTx+1))  * &
          (-(tabT(iTx-1)-tabT(iTx+2))  * (Tx         -tabT(iTx  ))**2 - &
            (tabT(iTx  )-tabT(iTx+1))  * (tabT(iTx  )-tabT(iTx+2)) *    &
            (Tx         -tabT(iTx-1)) ) / &
          ( (tabT(iTx-1)-tabT(iTx  ))  * (tabT(iTx  )-tabT(iTx+1))**2*  &
            (tabT(iTx  )-tabT(iTx+2)) )
      gT3=  (Tx         -tabT(iTx  ))  * &
          ( (tabT(iTx-1)-tabT(iTx+2))  * (Tx         -tabT(iTx+1))**2 - &
            (tabT(iTx-1)-tabT(iTx+1))  * (tabT(iTx  )-tabT(iTx+1)) *    &
            (Tx         -tabT(iTx+2)) ) / &
          ( (tabT(iTx-1)-tabT(iTx+1))  * (tabT(iTx  )-tabT(iTx+1))**2*  &
            (tabT(iTx+1)-tabT(iTx+2)) )
      gT4= -(Tx         -tabT(iTx  ))**2*(Tx         -tabT(iTx+1)) /    &
          ( (tabT(iTx  )-tabT(iTx+1))  * (tabT(iTx  )-tabT(iTx+2)) *    &
            (tabT(iTx+1)-tabT(iTx+2)) )
!
!     --- Geometry factors of P table ---
      gP1=  (Px         -tabP(iPx  ))  * (Px         -tabP(iPx+1))**2 / &
          ( (tabP(iPx-1)-tabP(iPx  ))  * (tabP(iPx-1)-tabP(iPx+1)) *    &
            (tabP(iPx  )-tabP(iPx+1)) )
      gP2=  (Px         -tabP(iPx+1))  * &
          (-(tabP(iPx-1)-tabP(iPx+2))  * (Px         -tabP(iPx  ))**2 - &
            (tabP(iPx  )-tabP(iPx+1))  * (tabP(iPx  )-tabP(iPx+2)) *    &
            (Px         -tabP(iPx-1)) ) / &
          ( (tabP(iPx-1)-tabP(iPx  ))  * (tabP(iPx  )-tabP(iPx+1))**2*  &
            (tabP(iPx  )-tabP(iPx+2)) )
      gP3=  (Px         -tabP(iPx  ))  * &
          ( (tabP(iPx-1)-tabP(iPx+2))  * (Px         -tabP(iPx+1))**2 - &
            (tabP(iPx-1)-tabP(iPx+1))  * (tabP(iPx  )-tabP(iPx+1)) *    &
            (Px         -tabP(iPx+2)) ) / &
          ( (tabP(iPx-1)-tabP(iPx+1))  * (tabP(iPx  )-tabP(iPx+1))**2*  &
            (tabP(iPx+1)-tabP(iPx+2)) )
      gP4= -(Px         -tabP(iPx  ))**2*(Px         -tabP(iPx+1)) /    &
          ( (tabP(iPx  )-tabP(iPx+1))  * (tabP(iPx  )-tabP(iPx+2)) *    &
            (tabP(iPx+1)-tabP(iPx+2)) )
!
!     --- Interpolation in T direction ---
      fP1=tabkap(iTx-1,iPx-1,iband)*gT1 + &
          tabkap(iTx  ,iPx-1,iband)*gT2 + &
          tabkap(iTx+1,iPx-1,iband)*gT3 + &
          tabkap(iTx+2,iPx-1,iband)*gT4
      fP2=tabkap(iTx-1,iPx  ,iband)*gT1 + &
          tabkap(iTx  ,iPx  ,iband)*gT2 + &
          tabkap(iTx+1,iPx  ,iband)*gT3 + &
          tabkap(iTx+2,iPx  ,iband)*gT4
      fP3=tabkap(iTx-1,iPx+1,iband)*gT1 + &
          tabkap(iTx  ,iPx+1,iband)*gT2 + &
          tabkap(iTx+1,iPx+1,iband)*gT3 + &
          tabkap(iTx+2,iPx+1,iband)*gT4
      fP4=tabkap(iTx-1,iPx+2,iband)*gT1 + &
          tabkap(iTx  ,iPx+2,iband)*gT2 + &
          tabkap(iTx+1,iPx+2,iband)*gT3 + &
          tabkap(iTx+2,iPx+2,iband)*gT4
!
!     --- Interpolation in P direction ---
      xkaros(i1,i2,i3)= exp(aln10*(fP1*gP1 + fP2*gP2 + fP3*gP3 + fP4*gP4))
!
      end do ! i1
      end do ! i2
      end do ! i3
!$OMP END DO
!
      end subroutine sub_xkaros3_logPT2kappa
!------------------*******************---------------------------------+-------

!----------------************************------------------------------+-------
      subroutine sub_xkaros2_logPT2rhokap(logP, T, rho, iband, xkaros)
!----------------------------------------------------------------------+-------
! NAME:
!   sub_xkaros2_logPT2rhokap 
!  ('subroutine_extended_kappa_rosseland_2_logPT2rhokappa')
!
! PURPOSE:
!   sub_XKAROS2_logPT2rhokap computes opacities for given pressure logP,
!   temperature T and frequency bin IBAND.
!   It determines the opacities by interpolation in
!   a table given by TABKAP.
!
! CATEGORY:
!   Opacities, Interpolation
!
! CALLING SEQUENCE:
!   call xkaros2_logPT2rhokap(logP, T, rho, iband, xkaros)
!
! INPUT:
!   logP:  ('logarithm_Pressure') real, dimension(:,:), natural logarithm
!          of pressure [log(dyn/cm^2)]
!   T:     ('Temperature') real, dimension(:,:), temperature [K]
!   rho:   ('rho') real, dimension(:,:), density [g/cm3]
!   iband: ('identification_band') integer, bin identification number
!   
! OUTPUT:
!   xkaros: ('extended_kappa_rosseland_2'), opacity per cm [cm^-1]
!
! ROUTINES:
!   None
!
! INCLUDES:
!   None
!
! COMMON BLOCK:
!   XOPTA: opacity table data read by DFOPTA, access: readonly
!
! SIDE EFFECTS:
!   No file access.
!
! RESTRICTIONS:
!   NTBN, NPBN have to be smaller or equal than 4.
!
! PROCEDURE:
!
! EXAMPLE:
!
! MODIFICATION HISTORY:
!   23. 1.91 Hans-Guenter Ludwig, Astrophysik Kiel.
!   27. 5.97 Bernd Freytag, Astrophysik Kiel
!            Use simple interpolation: cubic polynomial through 4 points.
!   28. 5.97 Interpolation: cubic polynomial through 2 points
!            with slopes at both points given by parabolas.
!            Vectorized version.
!   24. 6.97 (B.F.) TX=min(TABT(NT  ), max(TABT(1), alog10(T))) 
!               ->  TX=min(TABT(NT-1), max(TABT(2), alog10(T))) ...
!    8. 2.01 (B.F.) 2D version
!   20. 2.01 (B.F.) 3D version, logP version
!    5. 3.01 (B.F.) subroutine version
!    23-Mar-2001 Matthias Steffen, AIP
!----------------------------------------------------------------------+-------
!
!     --- XOPTA contains the opacity table data read by DFOPTA ---
      USE xopta_def
!
      IMPLICIT NONE
!
!     --- I/O parameters ---
      real, dimension(:,:),          intent(in) :: logP, T, rho
      integer,                       intent(in) :: iband
      real, dimension(:,:),         intent(out) :: xkaros
!
!--- Local Variables ---
      REAL, PARAMETER :: ALN10=2.30258509299405E0
      INTEGER :: iTx,iPx, nTx, nPx, i1, i2
      REAL    :: TX, PX
      REAL    :: gT1,gT2,gT3,gT4, gP1,gP2,gP3,gP4, fP1,fP2,fP3,fP4
!
!----------------------------------------------------------------------+-------
!
      do i2=1,size(T,2)
      do i1=1,size(T,1)
!
!     --- Delimit values of TX and PX to the ranges given by TABT and TABP ---
      TX=min(TABT(NT-1), max(TABT(2), alog10(T(i1,i2))))
!      PX=min(TABP(NP-1), max(TABP(2), alog10(P(i1,i2))))
      PX=min(TABP(NP-1), max(TABP(2), 0.434294481903*logP(i1,i2)))
!
!     --- Determine interval index NTX for temperature interpolation ---
!     --- The interval index is defined different for MAP and CUBIT! ---
!     --- It is assumed that TX can not be smaller than TABT(1).     ---
      if (TX .lt. TABTBN(2)) then
        NTX=int((TX - TABTBN(1)) / TABDTB(1)) + IDXTBN(1)
      else if (TX .lt. TABTBN(3)) then
        NTX=int((TX - TABTBN(2)) / TABDTB(2)) + IDXTBN(2)
      else if (TX .lt. TABTBN(4)) then
        NTX=int((TX - TABTBN(3)) / TABDTB(3)) + IDXTBN(3)
      else
        NTX=NT - 1
      endif
      iTx=min(max(2,nTx),nT-2)
!
!     --- Determine interval index NPX for pressure interpolation ---
      if (PX .lt. TABPBN(2)) then
        NPX=int((PX - TABPBN(1)) / TABDPB(1)) + IDXPBN(1)
      else if (PX .lt. TABPBN(3)) then
        NPX=int((PX - TABPBN(2)) / TABDPB(2)) + IDXPBN(2)
      else if (PX .lt. TABPBN(4)) then
        NPX=int((PX - TABPBN(3)) / TABDPB(3)) + IDXPBN(3)
      else
        NPX = NP - 1
      endif
      iPx=min(max(2,nPx),nP-2)
!
!     The interpolation procedure:
!  
!        P
!        ^
!        |       +: TABKAP, #: PKAP, *: (P,T)-value
!        |
!        |                |     |     |     |
!        |                |     |     |     |
!        |    NPX+2  -----+-----+-#---+-----+-----
!        |                |     |     |     |
!        |                |     |     |     |
!        |    NPX+1  -----+-----+-#---+-----+-----
!        |                |     |     |     |
!        |                |     | *   |     |
!        |    NPX    -----+-----+-#---+-----+-----
!        |                |     |     |     |
!        |                |     |     |     |
!        |    NPX-1  -----+-----+-#---+-----+-----
!        |                |     |     |     |
!        |                |     |     |     |
!        |
!        |              NTX-1  NTX  NTX+1 NTX+2
!        -----------------------------------------------------> T
!
!     --- Geometry factors of T table ---
!----------------------------------------------------------------------+-------
      gT1=  (Tx         -tabT(iTx  ))  * (Tx         -tabT(iTx+1))**2 / &
          ( (tabT(iTx-1)-tabT(iTx  ))  * (tabT(iTx-1)-tabT(iTx+1)) *    &
            (tabT(iTx  )-tabT(iTx+1)) )
      gT2=  (Tx         -tabT(iTx+1))  * &
          (-(tabT(iTx-1)-tabT(iTx+2))  * (Tx         -tabT(iTx  ))**2 - &
            (tabT(iTx  )-tabT(iTx+1))  * (tabT(iTx  )-tabT(iTx+2)) *    &
            (Tx         -tabT(iTx-1)) ) / &
          ( (tabT(iTx-1)-tabT(iTx  ))  * (tabT(iTx  )-tabT(iTx+1))**2*  &
            (tabT(iTx  )-tabT(iTx+2)) )
      gT3=  (Tx         -tabT(iTx  ))  * &
          ( (tabT(iTx-1)-tabT(iTx+2))  * (Tx         -tabT(iTx+1))**2 - &
            (tabT(iTx-1)-tabT(iTx+1))  * (tabT(iTx  )-tabT(iTx+1)) *    &
            (Tx         -tabT(iTx+2)) ) / &
          ( (tabT(iTx-1)-tabT(iTx+1))  * (tabT(iTx  )-tabT(iTx+1))**2*  &
            (tabT(iTx+1)-tabT(iTx+2)) )
      gT4= -(Tx         -tabT(iTx  ))**2*(Tx         -tabT(iTx+1)) /    &
          ( (tabT(iTx  )-tabT(iTx+1))  * (tabT(iTx  )-tabT(iTx+2)) *    &
            (tabT(iTx+1)-tabT(iTx+2)) )
!
!     --- Geometry factors of P table ---
      gP1=  (Px         -tabP(iPx  ))  * (Px         -tabP(iPx+1))**2 / &
          ( (tabP(iPx-1)-tabP(iPx  ))  * (tabP(iPx-1)-tabP(iPx+1)) *    &
            (tabP(iPx  )-tabP(iPx+1)) )
      gP2=  (Px         -tabP(iPx+1))  * &
          (-(tabP(iPx-1)-tabP(iPx+2))  * (Px         -tabP(iPx  ))**2 - &
            (tabP(iPx  )-tabP(iPx+1))  * (tabP(iPx  )-tabP(iPx+2)) *    &
            (Px         -tabP(iPx-1)) ) / &
          ( (tabP(iPx-1)-tabP(iPx  ))  * (tabP(iPx  )-tabP(iPx+1))**2*  &
            (tabP(iPx  )-tabP(iPx+2)) )
      gP3=  (Px         -tabP(iPx  ))  * &
          ( (tabP(iPx-1)-tabP(iPx+2))  * (Px         -tabP(iPx+1))**2 - &
            (tabP(iPx-1)-tabP(iPx+1))  * (tabP(iPx  )-tabP(iPx+1)) *    &
            (Px         -tabP(iPx+2)) ) / &
          ( (tabP(iPx-1)-tabP(iPx+1))  * (tabP(iPx  )-tabP(iPx+1))**2*  &
            (tabP(iPx+1)-tabP(iPx+2)) )
      gP4= -(Px         -tabP(iPx  ))**2*(Px         -tabP(iPx+1)) /    &
          ( (tabP(iPx  )-tabP(iPx+1))  * (tabP(iPx  )-tabP(iPx+2)) *    &
            (tabP(iPx+1)-tabP(iPx+2)) )
!
!     --- Interpolation in T direction ---
      fP1=tabkap(iTx-1,iPx-1,iband)*gT1 + &
          tabkap(iTx  ,iPx-1,iband)*gT2 + &
          tabkap(iTx+1,iPx-1,iband)*gT3 + &
          tabkap(iTx+2,iPx-1,iband)*gT4
      fP2=tabkap(iTx-1,iPx  ,iband)*gT1 + &
          tabkap(iTx  ,iPx  ,iband)*gT2 + &
          tabkap(iTx+1,iPx  ,iband)*gT3 + &
          tabkap(iTx+2,iPx  ,iband)*gT4
      fP3=tabkap(iTx-1,iPx+1,iband)*gT1 + &
          tabkap(iTx  ,iPx+1,iband)*gT2 + &
          tabkap(iTx+1,iPx+1,iband)*gT3 + &
          tabkap(iTx+2,iPx+1,iband)*gT4
      fP4=tabkap(iTx-1,iPx+2,iband)*gT1 + &
          tabkap(iTx  ,iPx+2,iband)*gT2 + &
          tabkap(iTx+1,iPx+2,iband)*gT3 + &
          tabkap(iTx+2,iPx+2,iband)*gT4
!
!     --- Interpolation in P direction ---
      xkaros(i1,i2)=rho(i1,i2) * &
                    exp(aln10*(fP1*gP1 + fP2*gP2 + fP3*gP3 + fP4*gP4))
!
      end do ! i1
      end do ! i2
!
      end subroutine sub_xkaros2_logPT2rhokap
!------------------**************************--------------------------+-------


!----------------************************------------------------------+-------
      subroutine sub_xkaros3_logPT2rhokap(logP, T, rho, iband, xkaros)
!----------------------------------------------------------------------+-------
! NAME:
!   sub_xkaros3_logPT2rhokap 
!   ('subroutine_extended_kappa_rosseland_3_logPT2rhokappa')
!
! PURPOSE:
!   sub_XKAROS3_logPT2rhokap computes opacities for given pressure logP,
!   temperature T and frequency bin IBAND.
!   It determines the opacities by interpolation in
!   a table given by TABKAP.
!
! CATEGORY:
!   Opacities, Interpolation
!
! CALLING SEQUENCE:
!   call xkaros3_logPT2rhokap(logP, T, rho, iband, xkaros)
!
! INPUT:
!   logP:  ('logarithm_Pressure') real, dimension(:,:), natural logarithm
!          of pressure [log(dyn/cm^2)]
!   T:     ('Temperature') real, dimension(:,:), temperature [K]
!   rho:   ('rho') real, dimension(:,:), density [g/cm3]
!   iband: ('identification_band') integer, bin identification number
!   
! OUTPUT:
!   xkaros: ('extended_kappa_rosseland_2'), opacity per cm [cm^-1]
!
! ROUTINES:
!   None
!
! INCLUDES:
!   None
!
! COMMON BLOCK:
!   XOPTA: opacity table data read by DFOPTA, access: readonly
!
! SIDE EFFECTS:
!   No file access.
!
! RESTRICTIONS:
!   NTBN, NPBN have to be smaller or equal than 4.
!
! PROCEDURE:
!
! EXAMPLE:
!
! MODIFICATION HISTORY:
!   23. 1.91 Hans-Guenter Ludwig, Astrophysik Kiel.
!   27. 5.97 Bernd Freytag, Astrophysik Kiel
!            Use simple interpolation: cubic polynomial through 4 points.
!   28. 5.97 Interpolation: cubic polynomial through 2 points
!            with slopes at both points given by parabolas.
!            Vectorized version.
!   24. 6.97 (B.F.) TX=min(TABT(NT  ), max(TABT(1), alog10(T))) 
!               ->  TX=min(TABT(NT-1), max(TABT(2), alog10(T))) ...
!    8. 2.01 (B.F.) 2D version
!   20. 2.01 (B.F.) 3D version, logP version
!    6. 3.01 (B.F.) subroutine version
!    23-Mar-2001 Matthias Steffen, AIP
!----------------------------------------------------------------------+-------
!
!     --- XOPTA contains the opacity table data read by DFOPTA ---
      USE xopta_def
!
      IMPLICIT NONE
!
!     --- I/O parameters ---
      real, dimension(:,:,:),        intent(in) :: logP, T, rho
      integer,                       intent(in) :: iband
      real, dimension(:,:,:),       intent(out) :: xkaros
!
!--- Local Variables ---
      REAL, PARAMETER :: ALN10=2.30258509299405E0
      INTEGER :: iTx,iPx, nTx, nPx, i1, i2, i3
      REAL    :: TX, PX
      REAL    :: gT1,gT2,gT3,gT4, gP1,gP2,gP3,gP4, fP1,fP2,fP3,fP4
!
!----------------------------------------------------------------------+-------
!
!$OMP DO
      do i3=1,size(T,3)
      do i2=1,size(T,2)
      do i1=1,size(T,1)
!
!     --- Delimit values of TX and PX to the ranges given by TABT and TABP ---
      TX=min(TABT(NT-1), max(TABT(2), alog10(T(i1,i2,i3))))
!      PX=min(TABP(NP-1), max(TABP(2), alog10(P(i1,i2,i3))))
      PX=min(TABP(NP-1), max(TABP(2), 0.434294481903*logP(i1,i2,i3)))
!
!     --- Determine interval index NTX for temperature interpolation ---
!     --- The interval index is defined different for MAP and CUBIT! ---
!     --- It is assumed that TX can not be smaller than TABT(1).     ---
      if (TX .lt. TABTBN(2)) then
        NTX=int((TX - TABTBN(1)) / TABDTB(1)) + IDXTBN(1)
      else if (TX .lt. TABTBN(3)) then
        NTX=int((TX - TABTBN(2)) / TABDTB(2)) + IDXTBN(2)
      else if (TX .lt. TABTBN(4)) then
        NTX=int((TX - TABTBN(3)) / TABDTB(3)) + IDXTBN(3)
      else
        NTX=NT - 1
      endif
      iTx=min(max(2,nTx),nT-2)
!
!     --- Determine interval index NPX for pressure interpolation ---
      if (PX .lt. TABPBN(2)) then
        NPX=int((PX - TABPBN(1)) / TABDPB(1)) + IDXPBN(1)
      else if (PX .lt. TABPBN(3)) then
        NPX=int((PX - TABPBN(2)) / TABDPB(2)) + IDXPBN(2)
      else if (PX .lt. TABPBN(4)) then
        NPX=int((PX - TABPBN(3)) / TABDPB(3)) + IDXPBN(3)
      else
        NPX = NP - 1
      endif
      iPx=min(max(2,nPx),nP-2)
!
!     The interpolation procedure:
!  
!        P
!        ^
!        |       +: TABKAP, #: PKAP, *: (P,T)-value
!        |
!        |                |     |     |     |
!        |                |     |     |     |
!        |    NPX+2  -----+-----+-#---+-----+-----
!        |                |     |     |     |
!        |                |     |     |     |
!        |    NPX+1  -----+-----+-#---+-----+-----
!        |                |     |     |     |
!        |                |     | *   |     |
!        |    NPX    -----+-----+-#---+-----+-----
!        |                |     |     |     |
!        |                |     |     |     |
!        |    NPX-1  -----+-----+-#---+-----+-----
!        |                |     |     |     |
!        |                |     |     |     |
!        |
!        |              NTX-1  NTX  NTX+1 NTX+2
!        -----------------------------------------------------> T
!
!     --- Geometry factors of T table ---
!----------------------------------------------------------------------+-------
      gT1=  (Tx         -tabT(iTx  ))  * (Tx         -tabT(iTx+1))**2 / &
          ( (tabT(iTx-1)-tabT(iTx  ))  * (tabT(iTx-1)-tabT(iTx+1)) *    &
            (tabT(iTx  )-tabT(iTx+1)) )
      gT2=  (Tx         -tabT(iTx+1))  * &
          (-(tabT(iTx-1)-tabT(iTx+2))  * (Tx         -tabT(iTx  ))**2 - &
            (tabT(iTx  )-tabT(iTx+1))  * (tabT(iTx  )-tabT(iTx+2)) *    &
            (Tx         -tabT(iTx-1)) ) / &
          ( (tabT(iTx-1)-tabT(iTx  ))  * (tabT(iTx  )-tabT(iTx+1))**2*  &
            (tabT(iTx  )-tabT(iTx+2)) )
      gT3=  (Tx         -tabT(iTx  ))  * &
          ( (tabT(iTx-1)-tabT(iTx+2))  * (Tx         -tabT(iTx+1))**2 - &
            (tabT(iTx-1)-tabT(iTx+1))  * (tabT(iTx  )-tabT(iTx+1)) *    &
            (Tx         -tabT(iTx+2)) ) / &
          ( (tabT(iTx-1)-tabT(iTx+1))  * (tabT(iTx  )-tabT(iTx+1))**2*  &
            (tabT(iTx+1)-tabT(iTx+2)) )
      gT4= -(Tx         -tabT(iTx  ))**2*(Tx         -tabT(iTx+1)) /    &
          ( (tabT(iTx  )-tabT(iTx+1))  * (tabT(iTx  )-tabT(iTx+2)) *    &
            (tabT(iTx+1)-tabT(iTx+2)) )
!
!     --- Geometry factors of P table ---
      gP1=  (Px         -tabP(iPx  ))  * (Px         -tabP(iPx+1))**2 / &
          ( (tabP(iPx-1)-tabP(iPx  ))  * (tabP(iPx-1)-tabP(iPx+1)) *    &
            (tabP(iPx  )-tabP(iPx+1)) )
      gP2=  (Px         -tabP(iPx+1))  * &
          (-(tabP(iPx-1)-tabP(iPx+2))  * (Px         -tabP(iPx  ))**2 - &
            (tabP(iPx  )-tabP(iPx+1))  * (tabP(iPx  )-tabP(iPx+2)) *    &
            (Px         -tabP(iPx-1)) ) / &
          ( (tabP(iPx-1)-tabP(iPx  ))  * (tabP(iPx  )-tabP(iPx+1))**2*  &
            (tabP(iPx  )-tabP(iPx+2)) )
      gP3=  (Px         -tabP(iPx  ))  * &
          ( (tabP(iPx-1)-tabP(iPx+2))  * (Px         -tabP(iPx+1))**2 - &
            (tabP(iPx-1)-tabP(iPx+1))  * (tabP(iPx  )-tabP(iPx+1)) *    &
            (Px         -tabP(iPx+2)) ) / &
          ( (tabP(iPx-1)-tabP(iPx+1))  * (tabP(iPx  )-tabP(iPx+1))**2*  &
            (tabP(iPx+1)-tabP(iPx+2)) )
      gP4= -(Px         -tabP(iPx  ))**2*(Px         -tabP(iPx+1)) /    &
          ( (tabP(iPx  )-tabP(iPx+1))  * (tabP(iPx  )-tabP(iPx+2)) *    &
            (tabP(iPx+1)-tabP(iPx+2)) )
!
!     --- Interpolation in T direction ---
      fP1=tabkap(iTx-1,iPx-1,iband)*gT1 + &
          tabkap(iTx  ,iPx-1,iband)*gT2 + &
          tabkap(iTx+1,iPx-1,iband)*gT3 + &
          tabkap(iTx+2,iPx-1,iband)*gT4
      fP2=tabkap(iTx-1,iPx  ,iband)*gT1 + &
          tabkap(iTx  ,iPx  ,iband)*gT2 + &
          tabkap(iTx+1,iPx  ,iband)*gT3 + &
          tabkap(iTx+2,iPx  ,iband)*gT4
      fP3=tabkap(iTx-1,iPx+1,iband)*gT1 + &
          tabkap(iTx  ,iPx+1,iband)*gT2 + &
          tabkap(iTx+1,iPx+1,iband)*gT3 + &
          tabkap(iTx+2,iPx+1,iband)*gT4
      fP4=tabkap(iTx-1,iPx+2,iband)*gT1 + &
          tabkap(iTx  ,iPx+2,iband)*gT2 + &
          tabkap(iTx+1,iPx+2,iband)*gT3 + &
          tabkap(iTx+2,iPx+2,iband)*gT4
!
!     --- Interpolation in P direction ---
      xkaros(i1,i2,i3)=rho(i1,i2,i3)* &
                       exp(aln10*(fP1*gP1 + fP2*gP2 + fP3*gP3 + fP4*gP4))
!
      end do ! i1
      end do ! i2
      end do ! i3
!$OMP END DO
!
      end subroutine sub_xkaros3_logPT2rhokap
!------------------**************************--------------------------+-------

!--------------*******************-------------------------------------+-------
      function xkaros1_logPT2kappa(logP, T, iband)
!----------------------------------------------------------------------+-------
! NAME:
!   xkaros1_logPT2kappa ('extended_kappa_rosseland_1_logPT2kappa')
!
! PURPOSE:
!   XKAROS1_logPT2kappa computes opacities for given pressure logP,
!   temperature T and frequency bin IBAND.
!   It determines the opacities by interpolation in
!   a table given by TABKAP.
!
! CATEGORY:
!   Opacities, Interpolation
!
! CALLING SEQUENCE:
!   kappa=xkaros1_logPT2kappa(logP, T, iband)
!
! INPUT:
!   logP:  ('logarithm_Pressure') real, dimension(:), natural logarithm
!          of pressure [log(dyn/cm^2)]
!   T:     ('Temperature') real, dimension(:), temperature [K]
!   iband: ('identification_band') integer, bin identification number
!   
! OUTPUT:
!   xkaros1_logPT2kappa: ('extended_kappa_rosseland_1'), opacity per gram [cm^2/g]
!
! ROUTINES:
!   None
!
! INCLUDES:
!   None
!
! COMMON BLOCK:
!   XOPTA: opacity table data read by DFOPTA, access: readonly
!
! SIDE EFFECTS:
!   No file access.
!
! RESTRICTIONS:
!   NTBN, NPBN have to be smaller or equal than 4.
!
! PROCEDURE:
!
! EXAMPLE:
!
! MODIFICATION HISTORY:
!   23. 1.91 Hans-Guenter Ludwig, Astrophysik Kiel.
!   27. 5.97 Bernd Freytag, Astrophysik Kiel
!            Use simple interpolation: cubic polynomial through 4 points.
!   28. 5.97 Interpolation: cubic polynomial through 2 points
!            with slopes at both points given by parabolas.
!            Vectorized version.
!   24. 6.97 (B.F.) TX=min(TABT(NT  ), max(TABT(1), alog10(T))) 
!               ->  TX=min(TABT(NT-1), max(TABT(2), alog10(T))) ...
!    8. 2.01 (B.F.) 2D version
!   20. 2.01 (B.F.) 3D version, logP version
!----------------------------------------------------------------------+-------
!
!     --- XOPTA contains the opacity table data read by DFOPTA ---
      USE xopta_def
!
      IMPLICIT NONE
!
!     --- I/O parameters ---
      real, dimension(:),            intent(in) :: logP, T
      integer,                       intent(in) :: iband
      real, dimension(size(T))                  :: xkaros1_logPT2kappa
!
!--- Local Variables ---
      REAL, PARAMETER :: ALN10=2.30258509299405E0
      INTEGER :: iTx,iPx, nTx, nPx, i1
      REAL    :: TX, PX
      REAL    :: gT1,gT2,gT3,gT4, gP1,gP2,gP3,gP4, fP1,fP2,fP3,fP4
!
!----------------------------------------------------------------------+-------
!
      do i1=1,size(T)
!
!     --- Delimit values of TX and PX to the ranges given by TABT and TABP ---
      TX=min(TABT(NT-1), max(TABT(2), alog10(T(i1))))
!      PX=min(TABP(NP-1), max(TABP(2), alog10(P(i1))))
      PX=min(TABP(NP-1), max(TABP(2), 0.434294481903*logP(i1)))
!
!     --- Determine interval index NTX for temperature interpolation ---
!     --- The interval index is defined different for MAP and CUBIT! ---
!     --- It is assumed that TX can not be smaller than TABT(1).     ---
      if (TX .lt. TABTBN(2)) then
        NTX=int((TX - TABTBN(1)) / TABDTB(1)) + IDXTBN(1)
      else if (TX .lt. TABTBN(3)) then
        NTX=int((TX - TABTBN(2)) / TABDTB(2)) + IDXTBN(2)
      else if (TX .lt. TABTBN(4)) then
        NTX=int((TX - TABTBN(3)) / TABDTB(3)) + IDXTBN(3)
      else
        NTX=NT - 1
      endif
      iTx=min(max(2,nTx),nT-2)
!
!     --- Determine interval index NPX for pressure interpolation ---
      if (PX .lt. TABPBN(2)) then
        NPX=int((PX - TABPBN(1)) / TABDPB(1)) + IDXPBN(1)
      else if (PX .lt. TABPBN(3)) then
        NPX=int((PX - TABPBN(2)) / TABDPB(2)) + IDXPBN(2)
      else if (PX .lt. TABPBN(4)) then
        NPX=int((PX - TABPBN(3)) / TABDPB(3)) + IDXPBN(3)
      else
        NPX = NP - 1
      endif
      iPx=min(max(2,nPx),nP-2)
!
!     The interpolation procedure:
!  
!        P
!        ^
!        |       +: TABKAP, #: PKAP, *: (P,T)-value
!        |
!        |                |     |     |     |
!        |                |     |     |     |
!        |    NPX+2  -----+-----+-#---+-----+-----
!        |                |     |     |     |
!        |                |     |     |     |
!        |    NPX+1  -----+-----+-#---+-----+-----
!        |                |     |     |     |
!        |                |     | *   |     |
!        |    NPX    -----+-----+-#---+-----+-----
!        |                |     |     |     |
!        |                |     |     |     |
!        |    NPX-1  -----+-----+-#---+-----+-----
!        |                |     |     |     |
!        |                |     |     |     |
!        |
!        |              NTX-1  NTX  NTX+1 NTX+2
!        -----------------------------------------------------> T
!
!     --- Geometry factors of T table ---
!----------------------------------------------------------------------+-------
      gT1=  (Tx         -tabT(iTx  ))  * (Tx         -tabT(iTx+1))**2 / &
          ( (tabT(iTx-1)-tabT(iTx  ))  * (tabT(iTx-1)-tabT(iTx+1)) *    &
            (tabT(iTx  )-tabT(iTx+1)) )
      gT2=  (Tx         -tabT(iTx+1))  * &
          (-(tabT(iTx-1)-tabT(iTx+2))  * (Tx         -tabT(iTx  ))**2 - &
            (tabT(iTx  )-tabT(iTx+1))  * (tabT(iTx  )-tabT(iTx+2)) *    &
            (Tx         -tabT(iTx-1)) ) / &
          ( (tabT(iTx-1)-tabT(iTx  ))  * (tabT(iTx  )-tabT(iTx+1))**2*  &
            (tabT(iTx  )-tabT(iTx+2)) )
      gT3=  (Tx         -tabT(iTx  ))  * &
          ( (tabT(iTx-1)-tabT(iTx+2))  * (Tx         -tabT(iTx+1))**2 - &
            (tabT(iTx-1)-tabT(iTx+1))  * (tabT(iTx  )-tabT(iTx+1)) *    &
            (Tx         -tabT(iTx+2)) ) / &
          ( (tabT(iTx-1)-tabT(iTx+1))  * (tabT(iTx  )-tabT(iTx+1))**2*  &
            (tabT(iTx+1)-tabT(iTx+2)) )
      gT4= -(Tx         -tabT(iTx  ))**2*(Tx         -tabT(iTx+1)) /    &
          ( (tabT(iTx  )-tabT(iTx+1))  * (tabT(iTx  )-tabT(iTx+2)) *    &
            (tabT(iTx+1)-tabT(iTx+2)) )
!
!     --- Geometry factors of P table ---
      gP1=  (Px         -tabP(iPx  ))  * (Px         -tabP(iPx+1))**2 / &
          ( (tabP(iPx-1)-tabP(iPx  ))  * (tabP(iPx-1)-tabP(iPx+1)) *    &
            (tabP(iPx  )-tabP(iPx+1)) )
      gP2=  (Px         -tabP(iPx+1))  * &
          (-(tabP(iPx-1)-tabP(iPx+2))  * (Px         -tabP(iPx  ))**2 - &
            (tabP(iPx  )-tabP(iPx+1))  * (tabP(iPx  )-tabP(iPx+2)) *    &
            (Px         -tabP(iPx-1)) ) / &
          ( (tabP(iPx-1)-tabP(iPx  ))  * (tabP(iPx  )-tabP(iPx+1))**2*  &
            (tabP(iPx  )-tabP(iPx+2)) )
      gP3=  (Px         -tabP(iPx  ))  * &
          ( (tabP(iPx-1)-tabP(iPx+2))  * (Px         -tabP(iPx+1))**2 - &
            (tabP(iPx-1)-tabP(iPx+1))  * (tabP(iPx  )-tabP(iPx+1)) *    &
            (Px         -tabP(iPx+2)) ) / &
          ( (tabP(iPx-1)-tabP(iPx+1))  * (tabP(iPx  )-tabP(iPx+1))**2*  &
            (tabP(iPx+1)-tabP(iPx+2)) )
      gP4= -(Px         -tabP(iPx  ))**2*(Px         -tabP(iPx+1)) /    &
          ( (tabP(iPx  )-tabP(iPx+1))  * (tabP(iPx  )-tabP(iPx+2)) *    &
            (tabP(iPx+1)-tabP(iPx+2)) )
!
!     --- Interpolation in T direction ---
      fP1=tabkap(iTx-1,iPx-1,iband)*gT1 + &
          tabkap(iTx  ,iPx-1,iband)*gT2 + &
          tabkap(iTx+1,iPx-1,iband)*gT3 + &
          tabkap(iTx+2,iPx-1,iband)*gT4
      fP2=tabkap(iTx-1,iPx  ,iband)*gT1 + &
          tabkap(iTx  ,iPx  ,iband)*gT2 + &
          tabkap(iTx+1,iPx  ,iband)*gT3 + &
          tabkap(iTx+2,iPx  ,iband)*gT4
      fP3=tabkap(iTx-1,iPx+1,iband)*gT1 + &
          tabkap(iTx  ,iPx+1,iband)*gT2 + &
          tabkap(iTx+1,iPx+1,iband)*gT3 + &
          tabkap(iTx+2,iPx+1,iband)*gT4
      fP4=tabkap(iTx-1,iPx+2,iband)*gT1 + &
          tabkap(iTx  ,iPx+2,iband)*gT2 + &
          tabkap(iTx+1,iPx+2,iband)*gT3 + &
          tabkap(iTx+2,iPx+2,iband)*gT4
!
!     --- Interpolation in P direction ---
      xkaros1_logPT2kappa(i1) = &
          exp(aln10*(fP1*gP1 + fP2*gP2 + fP3*gP3 + fP4*gP4))
!
      end do ! i1
!
      end function xkaros1_logPT2kappa
!------------------*******************---------------------------------+-------


!--------------*******************-------------------------------------+-------
      function xkaros2_logPT2kappa(logP, T, iband)
!----------------------------------------------------------------------+-------
! NAME:
!   xkaros2_logPT2kappa ('extended_kappa_rosseland_2_logPT2kappa')
!
! PURPOSE:
!   XKAROS2_logPT2kappa computes opacities for given pressure logP,
!   temperature T and frequency bin IBAND.
!   It determines the opacities by interpolation in
!   a table given by TABKAP.
!
! CATEGORY:
!   Opacities, Interpolation
!
! CALLING SEQUENCE:
!   kappa=xkaros2_logPT2kappa(logP, T, iband)
!
! INPUT:
!   logP:  ('logarithm_Pressure') real, dimension(:,:), natural logarithm
!          of pressure [log(dyn/cm^2)]
!   T:     ('Temperature') real, dimension(:,:), temperature [K]
!   iband: ('identification_band') integer, bin identification number
!   
! OUTPUT:
!   xkaros2_logPT2kappa: ('extended_kappa_rosseland_3'), opacity per gram [cm^2/g]
!
! ROUTINES:
!   None
!
! INCLUDES:
!   None
!
! COMMON BLOCK:
!   XOPTA: opacity table data read by DFOPTA, access: readonly
!
! SIDE EFFECTS:
!   No file access.
!
! RESTRICTIONS:
!   NTBN, NPBN have to be smaller or equal than 4.
!
! PROCEDURE:
!
! EXAMPLE:
!
! MODIFICATION HISTORY:
!   23. 1.91 Hans-Guenter Ludwig, Astrophysik Kiel.
!   27. 5.97 Bernd Freytag, Astrophysik Kiel
!            Use simple interpolation: cubic polynomial through 4 points.
!   28. 5.97 Interpolation: cubic polynomial through 2 points
!            with slopes at both points given by parabolas.
!            Vectorized version.
!   24. 6.97 (B.F.) TX=min(TABT(NT  ), max(TABT(1), alog10(T))) 
!               ->  TX=min(TABT(NT-1), max(TABT(2), alog10(T))) ...
!    8. 2.01 (B.F.) 2D version
!   20. 2.01 (B.F.) 3D version, logP version
!----------------------------------------------------------------------+-------
!
!     --- XOPTA contains the opacity table data read by DFOPTA ---
      USE xopta_def
!
      IMPLICIT NONE
!
!     --- I/O parameters ---
      real, dimension(:,:),          intent(in) :: logP, T
      integer,                       intent(in) :: iband
      real, dimension(size(T,1),size(T,2))      :: xkaros2_logPT2kappa
!
!--- Local Variables ---
      REAL, PARAMETER :: ALN10=2.30258509299405E0
      INTEGER :: iTx,iPx, nTx, nPx, i1, i2
      REAL    :: TX, PX
      REAL    :: gT1,gT2,gT3,gT4, gP1,gP2,gP3,gP4, fP1,fP2,fP3,fP4
!
!----------------------------------------------------------------------+-------
!
      do i2=1,size(T,2)
      do i1=1,size(T,1)
!
!     --- Delimit values of TX and PX to the ranges given by TABT and TABP ---
      TX=min(TABT(NT-1), max(TABT(2), alog10(T(i1,i2))))
!      PX=min(TABP(NP-1), max(TABP(2), alog10(P(i1,i2))))
      PX=min(TABP(NP-1), max(TABP(2), 0.434294481903*logP(i1,i2)))
!
!     --- Determine interval index NTX for temperature interpolation ---
!     --- The interval index is defined different for MAP and CUBIT! ---
!     --- It is assumed that TX can not be smaller than TABT(1).     ---
      if (TX .lt. TABTBN(2)) then
        NTX=int((TX - TABTBN(1)) / TABDTB(1)) + IDXTBN(1)
      else if (TX .lt. TABTBN(3)) then
        NTX=int((TX - TABTBN(2)) / TABDTB(2)) + IDXTBN(2)
      else if (TX .lt. TABTBN(4)) then
        NTX=int((TX - TABTBN(3)) / TABDTB(3)) + IDXTBN(3)
      else
        NTX=NT - 1
      endif
      iTx=min(max(2,nTx),nT-2)
!
!     --- Determine interval index NPX for pressure interpolation ---
      if (PX .lt. TABPBN(2)) then
        NPX=int((PX - TABPBN(1)) / TABDPB(1)) + IDXPBN(1)
      else if (PX .lt. TABPBN(3)) then
        NPX=int((PX - TABPBN(2)) / TABDPB(2)) + IDXPBN(2)
      else if (PX .lt. TABPBN(4)) then
        NPX=int((PX - TABPBN(3)) / TABDPB(3)) + IDXPBN(3)
      else
        NPX = NP - 1
      endif
      iPx=min(max(2,nPx),nP-2)
!
!     The interpolation procedure:
!  
!        P
!        ^
!        |       +: TABKAP, #: PKAP, *: (P,T)-value
!        |
!        |                |     |     |     |
!        |                |     |     |     |
!        |    NPX+2  -----+-----+-#---+-----+-----
!        |                |     |     |     |
!        |                |     |     |     |
!        |    NPX+1  -----+-----+-#---+-----+-----
!        |                |     |     |     |
!        |                |     | *   |     |
!        |    NPX    -----+-----+-#---+-----+-----
!        |                |     |     |     |
!        |                |     |     |     |
!        |    NPX-1  -----+-----+-#---+-----+-----
!        |                |     |     |     |
!        |                |     |     |     |
!        |
!        |              NTX-1  NTX  NTX+1 NTX+2
!        -----------------------------------------------------> T
!
!     --- Geometry factors of T table ---
!----------------------------------------------------------------------+-------
      gT1=  (Tx         -tabT(iTx  ))  * (Tx         -tabT(iTx+1))**2 / &
          ( (tabT(iTx-1)-tabT(iTx  ))  * (tabT(iTx-1)-tabT(iTx+1)) *    &
            (tabT(iTx  )-tabT(iTx+1)) )
      gT2=  (Tx         -tabT(iTx+1))  * &
          (-(tabT(iTx-1)-tabT(iTx+2))  * (Tx         -tabT(iTx  ))**2 - &
            (tabT(iTx  )-tabT(iTx+1))  * (tabT(iTx  )-tabT(iTx+2)) *    &
            (Tx         -tabT(iTx-1)) ) / &
          ( (tabT(iTx-1)-tabT(iTx  ))  * (tabT(iTx  )-tabT(iTx+1))**2*  &
            (tabT(iTx  )-tabT(iTx+2)) )
      gT3=  (Tx         -tabT(iTx  ))  * &
          ( (tabT(iTx-1)-tabT(iTx+2))  * (Tx         -tabT(iTx+1))**2 - &
            (tabT(iTx-1)-tabT(iTx+1))  * (tabT(iTx  )-tabT(iTx+1)) *    &
            (Tx         -tabT(iTx+2)) ) / &
          ( (tabT(iTx-1)-tabT(iTx+1))  * (tabT(iTx  )-tabT(iTx+1))**2*  &
            (tabT(iTx+1)-tabT(iTx+2)) )
      gT4= -(Tx         -tabT(iTx  ))**2*(Tx         -tabT(iTx+1)) /    &
          ( (tabT(iTx  )-tabT(iTx+1))  * (tabT(iTx  )-tabT(iTx+2)) *    &
            (tabT(iTx+1)-tabT(iTx+2)) )
!
!     --- Geometry factors of P table ---
      gP1=  (Px         -tabP(iPx  ))  * (Px         -tabP(iPx+1))**2 / &
          ( (tabP(iPx-1)-tabP(iPx  ))  * (tabP(iPx-1)-tabP(iPx+1)) *    &
            (tabP(iPx  )-tabP(iPx+1)) )
      gP2=  (Px         -tabP(iPx+1))  * &
          (-(tabP(iPx-1)-tabP(iPx+2))  * (Px         -tabP(iPx  ))**2 - &
            (tabP(iPx  )-tabP(iPx+1))  * (tabP(iPx  )-tabP(iPx+2)) *    &
            (Px         -tabP(iPx-1)) ) / &
          ( (tabP(iPx-1)-tabP(iPx  ))  * (tabP(iPx  )-tabP(iPx+1))**2*  &
            (tabP(iPx  )-tabP(iPx+2)) )
      gP3=  (Px         -tabP(iPx  ))  * &
          ( (tabP(iPx-1)-tabP(iPx+2))  * (Px         -tabP(iPx+1))**2 - &
            (tabP(iPx-1)-tabP(iPx+1))  * (tabP(iPx  )-tabP(iPx+1)) *    &
            (Px         -tabP(iPx+2)) ) / &
          ( (tabP(iPx-1)-tabP(iPx+1))  * (tabP(iPx  )-tabP(iPx+1))**2*  &
            (tabP(iPx+1)-tabP(iPx+2)) )
      gP4= -(Px         -tabP(iPx  ))**2*(Px         -tabP(iPx+1)) /    &
          ( (tabP(iPx  )-tabP(iPx+1))  * (tabP(iPx  )-tabP(iPx+2)) *    &
            (tabP(iPx+1)-tabP(iPx+2)) )
!
!     --- Interpolation in T direction ---
      fP1=tabkap(iTx-1,iPx-1,iband)*gT1 + &
          tabkap(iTx  ,iPx-1,iband)*gT2 + &
          tabkap(iTx+1,iPx-1,iband)*gT3 + &
          tabkap(iTx+2,iPx-1,iband)*gT4
      fP2=tabkap(iTx-1,iPx  ,iband)*gT1 + &
          tabkap(iTx  ,iPx  ,iband)*gT2 + &
          tabkap(iTx+1,iPx  ,iband)*gT3 + &
          tabkap(iTx+2,iPx  ,iband)*gT4
      fP3=tabkap(iTx-1,iPx+1,iband)*gT1 + &
          tabkap(iTx  ,iPx+1,iband)*gT2 + &
          tabkap(iTx+1,iPx+1,iband)*gT3 + &
          tabkap(iTx+2,iPx+1,iband)*gT4
      fP4=tabkap(iTx-1,iPx+2,iband)*gT1 + &
          tabkap(iTx  ,iPx+2,iband)*gT2 + &
          tabkap(iTx+1,iPx+2,iband)*gT3 + &
          tabkap(iTx+2,iPx+2,iband)*gT4
!
!     --- Interpolation in P direction ---
      xkaros2_logPT2kappa(i1,i2) = &
          exp(aln10*(fP1*gP1 + fP2*gP2 + fP3*gP3 + fP4*gP4))
!
      end do ! i1
      end do ! i2
!
      end function xkaros2_logPT2kappa
!------------------*******************---------------------------------+-------


!--------------*******************-------------------------------------+-------
      function xkaros3_logPT2kappa(logP, T, iband)
!----------------------------------------------------------------------+-------
! NAME:
!   xkaros3_logPT2kappa ('extended_kappa_rosseland_3_logPT2kappa')
!
! PURPOSE:
!   XKAROS3_logPT2kappa computes opacities for given pressure logP,
!   temperature T and frequency bin IBAND.
!   It determines the opacities by interpolation in
!   a table given by TABKAP.
!
! CATEGORY:
!   Opacities, Interpolation
!
! CALLING SEQUENCE:
!   kappa=xkaros3_logPT2kappa(logP, T, iband)
!
! INPUT:
!   logP:  ('logarithm_Pressure') real, dimension(:,:,:), natural logarithm
!          of pressure [log(dyn/cm^2)]
!   T:     ('Temperature') real, dimension(:,:,:), temperature [K]
!   iband: ('identification_band') integer, bin identification number
!   
! OUTPUT:
!   xkaros3: ('extended_kappa_rosseland_3'), opacity per gram [cm^2/g]
!
! ROUTINES:
!   None
!
! INCLUDES:
!   None
!
! COMMON BLOCK:
!   XOPTA: opacity table data read by DFOPTA, access: readonly
!
! SIDE EFFECTS:
!   No file access.
!
! RESTRICTIONS:
!   NTBN, NPBN have to be smaller or equal than 4.
!
! PROCEDURE:
!
! EXAMPLE:
!
! MODIFICATION HISTORY:
!   23. 1.91 Hans-Guenter Ludwig, Astrophysik Kiel.
!   27. 5.97 Bernd Freytag, Astrophysik Kiel
!            Use simple interpolation: cubic polynomial through 4 points.
!   28. 5.97 Interpolation: cubic polynomial through 2 points
!            with slopes at both points given by parabolas.
!            Vectorized version.
!   24. 6.97 (B.F.) TX=min(TABT(NT  ), max(TABT(1), alog10(T))) 
!               ->  TX=min(TABT(NT-1), max(TABT(2), alog10(T))) ...
!    8. 2.01 (B.F.) 2D version
!   20. 2.01 (B.F.) 3D version, logP version
!----------------------------------------------------------------------+-------
!
!     --- XOPTA contains the opacity table data read by DFOPTA ---
      USE xopta_def
!
      IMPLICIT NONE
!
!     --- I/O parameters ---
      real, dimension(:,:,:),        intent(in) :: logP, T
      integer,                       intent(in) :: iband
      real, dimension(size(T,1),size(T,2),size(T,3)) &
                                                :: xkaros3_logPT2kappa
!--- Local Variables ---
      REAL, PARAMETER :: ALN10=2.30258509299405E0
      INTEGER :: iTx,iPx, nTx, nPx, i1, i2, i3
      REAL    :: TX, PX
      REAL    :: gT1,gT2,gT3,gT4, gP1,gP2,gP3,gP4, fP1,fP2,fP3,fP4
!
!----------------------------------------------------------------------+-------
!
!$OMP DO
      do i3=1,size(T,3)
      do i2=1,size(T,2)
      do i1=1,size(T,1)
!
!     --- Delimit values of TX and PX to the ranges given by TABT and TABP ---
      TX=min(TABT(NT-1), max(TABT(2), alog10(T(i1,i2,i3))))
!      PX=min(TABP(NP-1), max(TABP(2), alog10(P(i1,i2,i3))))
      PX=min(TABP(NP-1), max(TABP(2), 0.434294481903*logP(i1,i2,i3)))
!
!     --- Determine interval index NTX for temperature interpolation ---
!     --- The interval index is defined different for MAP and CUBIT! ---
!     --- It is assumed that TX can not be smaller than TABT(1).     ---
      if (TX .lt. TABTBN(2)) then
        NTX=int((TX - TABTBN(1)) / TABDTB(1)) + IDXTBN(1)
      else if (TX .lt. TABTBN(3)) then
        NTX=int((TX - TABTBN(2)) / TABDTB(2)) + IDXTBN(2)
      else if (TX .lt. TABTBN(4)) then
        NTX=int((TX - TABTBN(3)) / TABDTB(3)) + IDXTBN(3)
      else
        NTX=NT - 1
      endif
      iTx=min(max(2,nTx),nT-2)
!
!     --- Determine interval index NPX for pressure interpolation ---
      if (PX .lt. TABPBN(2)) then
        NPX=int((PX - TABPBN(1)) / TABDPB(1)) + IDXPBN(1)
      else if (PX .lt. TABPBN(3)) then
        NPX=int((PX - TABPBN(2)) / TABDPB(2)) + IDXPBN(2)
      else if (PX .lt. TABPBN(4)) then
        NPX=int((PX - TABPBN(3)) / TABDPB(3)) + IDXPBN(3)
      else
        NPX = NP - 1
      endif
      iPx=min(max(2,nPx),nP-2)
!
!     The interpolation procedure:
!  
!        P
!        ^
!        |       +: TABKAP, #: PKAP, *: (P,T)-value
!        |
!        |                |     |     |     |
!        |                |     |     |     |
!        |    NPX+2  -----+-----+-#---+-----+-----
!        |                |     |     |     |
!        |                |     |     |     |
!        |    NPX+1  -----+-----+-#---+-----+-----
!        |                |     |     |     |
!        |                |     | *   |     |
!        |    NPX    -----+-----+-#---+-----+-----
!        |                |     |     |     |
!        |                |     |     |     |
!        |    NPX-1  -----+-----+-#---+-----+-----
!        |                |     |     |     |
!        |                |     |     |     |
!        |
!        |              NTX-1  NTX  NTX+1 NTX+2
!        -----------------------------------------------------> T
!
!     --- Geometry factors of T table ---
!----------------------------------------------------------------------+-------
      gT1=  (Tx         -tabT(iTx  ))  * (Tx         -tabT(iTx+1))**2 / &
          ( (tabT(iTx-1)-tabT(iTx  ))  * (tabT(iTx-1)-tabT(iTx+1)) *    &
            (tabT(iTx  )-tabT(iTx+1)) )
      gT2=  (Tx         -tabT(iTx+1))  * &
          (-(tabT(iTx-1)-tabT(iTx+2))  * (Tx         -tabT(iTx  ))**2 - &
            (tabT(iTx  )-tabT(iTx+1))  * (tabT(iTx  )-tabT(iTx+2)) *    &
            (Tx         -tabT(iTx-1)) ) / &
          ( (tabT(iTx-1)-tabT(iTx  ))  * (tabT(iTx  )-tabT(iTx+1))**2*  &
            (tabT(iTx  )-tabT(iTx+2)) )
      gT3=  (Tx         -tabT(iTx  ))  * &
          ( (tabT(iTx-1)-tabT(iTx+2))  * (Tx         -tabT(iTx+1))**2 - &
            (tabT(iTx-1)-tabT(iTx+1))  * (tabT(iTx  )-tabT(iTx+1)) *    &
            (Tx         -tabT(iTx+2)) ) / &
          ( (tabT(iTx-1)-tabT(iTx+1))  * (tabT(iTx  )-tabT(iTx+1))**2*  &
            (tabT(iTx+1)-tabT(iTx+2)) )
      gT4= -(Tx         -tabT(iTx  ))**2*(Tx         -tabT(iTx+1)) /    &
          ( (tabT(iTx  )-tabT(iTx+1))  * (tabT(iTx  )-tabT(iTx+2)) *    &
            (tabT(iTx+1)-tabT(iTx+2)) )
!
!     --- Geometry factors of P table ---
      gP1=  (Px         -tabP(iPx  ))  * (Px         -tabP(iPx+1))**2 / &
          ( (tabP(iPx-1)-tabP(iPx  ))  * (tabP(iPx-1)-tabP(iPx+1)) *    &
            (tabP(iPx  )-tabP(iPx+1)) )
      gP2=  (Px         -tabP(iPx+1))  * &
          (-(tabP(iPx-1)-tabP(iPx+2))  * (Px         -tabP(iPx  ))**2 - &
            (tabP(iPx  )-tabP(iPx+1))  * (tabP(iPx  )-tabP(iPx+2)) *    &
            (Px         -tabP(iPx-1)) ) / &
          ( (tabP(iPx-1)-tabP(iPx  ))  * (tabP(iPx  )-tabP(iPx+1))**2*  &
            (tabP(iPx  )-tabP(iPx+2)) )
      gP3=  (Px         -tabP(iPx  ))  * &
          ( (tabP(iPx-1)-tabP(iPx+2))  * (Px         -tabP(iPx+1))**2 - &
            (tabP(iPx-1)-tabP(iPx+1))  * (tabP(iPx  )-tabP(iPx+1)) *    &
            (Px         -tabP(iPx+2)) ) / &
          ( (tabP(iPx-1)-tabP(iPx+1))  * (tabP(iPx  )-tabP(iPx+1))**2*  &
            (tabP(iPx+1)-tabP(iPx+2)) )
      gP4= -(Px         -tabP(iPx  ))**2*(Px         -tabP(iPx+1)) /    &
          ( (tabP(iPx  )-tabP(iPx+1))  * (tabP(iPx  )-tabP(iPx+2)) *    &
            (tabP(iPx+1)-tabP(iPx+2)) )
!
!     --- Interpolation in T direction ---
      fP1=tabkap(iTx-1,iPx-1,iband)*gT1 + &
          tabkap(iTx  ,iPx-1,iband)*gT2 + &
          tabkap(iTx+1,iPx-1,iband)*gT3 + &
          tabkap(iTx+2,iPx-1,iband)*gT4
      fP2=tabkap(iTx-1,iPx  ,iband)*gT1 + &
          tabkap(iTx  ,iPx  ,iband)*gT2 + &
          tabkap(iTx+1,iPx  ,iband)*gT3 + &
          tabkap(iTx+2,iPx  ,iband)*gT4
      fP3=tabkap(iTx-1,iPx+1,iband)*gT1 + &
          tabkap(iTx  ,iPx+1,iband)*gT2 + &
          tabkap(iTx+1,iPx+1,iband)*gT3 + &
          tabkap(iTx+2,iPx+1,iband)*gT4
      fP4=tabkap(iTx-1,iPx+2,iband)*gT1 + &
          tabkap(iTx  ,iPx+2,iband)*gT2 + &
          tabkap(iTx+1,iPx+2,iband)*gT3 + &
          tabkap(iTx+2,iPx+2,iband)*gT4
!
!     --- Interpolation in P direction ---
      xkaros3_logPT2kappa(i1,i2,i3)= &
          exp(aln10*(fP1*gP1 + fP2*gP2 + fP3*gP3 + fP4*gP4))
!
      end do ! i1
      end do ! i2
      end do ! i3
!$OMP END DO
!
      end function xkaros3_logPT2kappa
!------------------*******************---------------------------------+-------


!--------------********************------------------------------------+-------
      function xkaros1_logPT42kappa(n1, logP, T4, iband)
!----------------------------------------------------------------------+-------
! NAME:
!   xkaros1_logPT42kappa ('extended_kappa_rosseland_1_logPT42kappa')
!
! PURPOSE:
!   XKAROS1_logPT42kappa computes opacities for given pressure logP,
!   temperature^4 T4 and frequency bin IBAND.
!   It determines the opacities by interpolation in
!   a table given by TABKAP.
!
! CATEGORY:
!   Opacities, Interpolation
!
! CALLING SEQUENCE:
!   kappa=xkaros1_logPT42kappa(n1, logP, T4, iband)
!
! INPUT:
!   n1:    ('number_1') integer
!   logP:  ('logarithm_Pressure') real, dimension(:), natural logarithm
!          of pressure [log(dyn/cm^2)]
!   T4:    ('Temperature_4') real, dimension(:), temperature^4 [K^4]
!   iband: ('identification_band') integer, bin identification number
!   
! OUTPUT:
!   xkaros1_logPT42kappa: ('extended_kappa_rosseland_1'), opacity per gram [cm^2/g]
!
! ROUTINES:
!   None
!
! INCLUDES:
!   None
!
! COMMON BLOCK:
!   XOPTA: opacity table data read by DFOPTA, access: readonly
!
! SIDE EFFECTS:
!   No file access.
!
! RESTRICTIONS:
!   NTBN, NPBN have to be smaller or equal than 4.
!
! PROCEDURE:
!
! EXAMPLE:
!
! MODIFICATION HISTORY:
!   23. 1.91 Hans-Guenter Ludwig, Astrophysik Kiel.
!   27. 5.97 Bernd Freytag, Astrophysik Kiel
!            Use simple interpolation: cubic polynomial through 4 points.
!   28. 5.97 Interpolation: cubic polynomial through 2 points
!            with slopes at both points given by parabolas.
!            Vectorized version.
!   24. 6.97 (B.F.) TX=min(TABT(NT  ), max(TABT(1), alog10(T))) 
!               ->  TX=min(TABT(NT-1), max(TABT(2), alog10(T))) ...
!    8. 2.01 (B.F.) 2D version
!   20. 2.01 (B.F.) 3D version, logP version
!   21. 2.01 (B.F.) n1, T^4 version
!----------------------------------------------------------------------+-------
!
!     --- XOPTA contains the opacity table data read by DFOPTA ---
      USE xopta_def
!
      IMPLICIT NONE
!
!     --- I/O parameters ---
      integer,                       intent(in) :: n1, iband
      real, dimension(n1),           intent(in) :: logP, T4
      real, dimension(n1)                       :: xkaros1_logPT42kappa
!
!--- Local Variables ---
      REAL, PARAMETER :: ALN10=2.30258509299405E0
      INTEGER :: iTx,iPx, nTx, nPx, i1
      REAL    :: TX, PX
      REAL    :: gT1,gT2,gT3,gT4, gP1,gP2,gP3,gP4, fP1,fP2,fP3,fP4
!
!----------------------------------------------------------------------+-------
!
      do i1=1,n1
!
!     --- Delimit values of TX and PX to the ranges given by TABT and TABP ---
!      TX=min(TABT(NT-1), max(TABT(2), alog10(T(i1))))
      TX=min(TABT(NT-1), max(TABT(2), 0.25*alog10(T4(i1))))
!      PX=min(TABP(NP-1), max(TABP(2), alog10(P(i1))))
      PX=min(TABP(NP-1), max(TABP(2), 0.434294481903*logP(i1)))
!
!     --- Determine interval index NTX for temperature interpolation ---
!     --- The interval index is defined different for MAP and CUBIT! ---
!     --- It is assumed that TX can not be smaller than TABT(1).     ---
      if (TX .lt. TABTBN(2)) then
        NTX=int((TX - TABTBN(1)) / TABDTB(1)) + IDXTBN(1)
      else if (TX .lt. TABTBN(3)) then
        NTX=int((TX - TABTBN(2)) / TABDTB(2)) + IDXTBN(2)
      else if (TX .lt. TABTBN(4)) then
        NTX=int((TX - TABTBN(3)) / TABDTB(3)) + IDXTBN(3)
      else
        NTX=NT - 1
      endif
      iTx=min(max(2,nTx),nT-2)
!
!     --- Determine interval index NPX for pressure interpolation ---
      if (PX .lt. TABPBN(2)) then
        NPX=int((PX - TABPBN(1)) / TABDPB(1)) + IDXPBN(1)
      else if (PX .lt. TABPBN(3)) then
        NPX=int((PX - TABPBN(2)) / TABDPB(2)) + IDXPBN(2)
      else if (PX .lt. TABPBN(4)) then
        NPX=int((PX - TABPBN(3)) / TABDPB(3)) + IDXPBN(3)
      else
        NPX = NP - 1
      endif
      iPx=min(max(2,nPx),nP-2)
!
!     The interpolation procedure:
!  
!        P
!        ^
!        |       +: TABKAP, #: PKAP, *: (P,T)-value
!        |
!        |                |     |     |     |
!        |                |     |     |     |
!        |    NPX+2  -----+-----+-#---+-----+-----
!        |                |     |     |     |
!        |                |     |     |     |
!        |    NPX+1  -----+-----+-#---+-----+-----
!        |                |     |     |     |
!        |                |     | *   |     |
!        |    NPX    -----+-----+-#---+-----+-----
!        |                |     |     |     |
!        |                |     |     |     |
!        |    NPX-1  -----+-----+-#---+-----+-----
!        |                |     |     |     |
!        |                |     |     |     |
!        |
!        |              NTX-1  NTX  NTX+1 NTX+2
!        -----------------------------------------------------> T
!
!     --- Geometry factors of T table ---
!----------------------------------------------------------------------+-------
      gT1=  (Tx         -tabT(iTx  ))  * (Tx         -tabT(iTx+1))**2 / &
          ( (tabT(iTx-1)-tabT(iTx  ))  * (tabT(iTx-1)-tabT(iTx+1)) *    &
            (tabT(iTx  )-tabT(iTx+1)) )
      gT2=  (Tx         -tabT(iTx+1))  * &
          (-(tabT(iTx-1)-tabT(iTx+2))  * (Tx         -tabT(iTx  ))**2 - &
            (tabT(iTx  )-tabT(iTx+1))  * (tabT(iTx  )-tabT(iTx+2)) *    &
            (Tx         -tabT(iTx-1)) ) / &
          ( (tabT(iTx-1)-tabT(iTx  ))  * (tabT(iTx  )-tabT(iTx+1))**2*  &
            (tabT(iTx  )-tabT(iTx+2)) )
      gT3=  (Tx         -tabT(iTx  ))  * &
          ( (tabT(iTx-1)-tabT(iTx+2))  * (Tx         -tabT(iTx+1))**2 - &
            (tabT(iTx-1)-tabT(iTx+1))  * (tabT(iTx  )-tabT(iTx+1)) *    &
            (Tx         -tabT(iTx+2)) ) / &
          ( (tabT(iTx-1)-tabT(iTx+1))  * (tabT(iTx  )-tabT(iTx+1))**2*  &
            (tabT(iTx+1)-tabT(iTx+2)) )
      gT4= -(Tx         -tabT(iTx  ))**2*(Tx         -tabT(iTx+1)) /    &
          ( (tabT(iTx  )-tabT(iTx+1))  * (tabT(iTx  )-tabT(iTx+2)) *    &
            (tabT(iTx+1)-tabT(iTx+2)) )
!
!     --- Geometry factors of P table ---
      gP1=  (Px         -tabP(iPx  ))  * (Px         -tabP(iPx+1))**2 / &
          ( (tabP(iPx-1)-tabP(iPx  ))  * (tabP(iPx-1)-tabP(iPx+1)) *    &
            (tabP(iPx  )-tabP(iPx+1)) )
      gP2=  (Px         -tabP(iPx+1))  * &
          (-(tabP(iPx-1)-tabP(iPx+2))  * (Px         -tabP(iPx  ))**2 - &
            (tabP(iPx  )-tabP(iPx+1))  * (tabP(iPx  )-tabP(iPx+2)) *    &
            (Px         -tabP(iPx-1)) ) / &
          ( (tabP(iPx-1)-tabP(iPx  ))  * (tabP(iPx  )-tabP(iPx+1))**2*  &
            (tabP(iPx  )-tabP(iPx+2)) )
      gP3=  (Px         -tabP(iPx  ))  * &
          ( (tabP(iPx-1)-tabP(iPx+2))  * (Px         -tabP(iPx+1))**2 - &
            (tabP(iPx-1)-tabP(iPx+1))  * (tabP(iPx  )-tabP(iPx+1)) *    &
            (Px         -tabP(iPx+2)) ) / &
          ( (tabP(iPx-1)-tabP(iPx+1))  * (tabP(iPx  )-tabP(iPx+1))**2*  &
            (tabP(iPx+1)-tabP(iPx+2)) )
      gP4= -(Px         -tabP(iPx  ))**2*(Px         -tabP(iPx+1)) /    &
          ( (tabP(iPx  )-tabP(iPx+1))  * (tabP(iPx  )-tabP(iPx+2)) *    &
            (tabP(iPx+1)-tabP(iPx+2)) )
!
!     --- Interpolation in T direction ---
      fP1=tabkap(iTx-1,iPx-1,iband)*gT1 + &
          tabkap(iTx  ,iPx-1,iband)*gT2 + &
          tabkap(iTx+1,iPx-1,iband)*gT3 + &
          tabkap(iTx+2,iPx-1,iband)*gT4
      fP2=tabkap(iTx-1,iPx  ,iband)*gT1 + &
          tabkap(iTx  ,iPx  ,iband)*gT2 + &
          tabkap(iTx+1,iPx  ,iband)*gT3 + &
          tabkap(iTx+2,iPx  ,iband)*gT4
      fP3=tabkap(iTx-1,iPx+1,iband)*gT1 + &
          tabkap(iTx  ,iPx+1,iband)*gT2 + &
          tabkap(iTx+1,iPx+1,iband)*gT3 + &
          tabkap(iTx+2,iPx+1,iband)*gT4
      fP4=tabkap(iTx-1,iPx+2,iband)*gT1 + &
          tabkap(iTx  ,iPx+2,iband)*gT2 + &
          tabkap(iTx+1,iPx+2,iband)*gT3 + &
          tabkap(iTx+2,iPx+2,iband)*gT4
!
!     --- Interpolation in P direction ---
      xkaros1_logPT42kappa(i1) = &
          exp(aln10*(fP1*gP1 + fP2*gP2 + fP3*gP3 + fP4*gP4))
!
      end do ! i1
!
      end function xkaros1_logPT42kappa
!------------------********************--------------------------------+-------


!----------------*****-------------------------------------------------+-------
      subroutine xbnu0(T, iband, wTb)
!----------------------------------------------------------------------+-------
! NAME:
!   xbnu0 ('x_b_nu_Temperature_0')
!
! PURPOSE:
!   Compute the fraction of the Planck function B_nu
!
! CATEGORY:
!   Opacities, Interpolation
!
! CALLING SEQUENCE:
!   call xbnu0(T, iband, wTb)
!
! INPUT:
!   T:      ('Temperature') real, temperature [K]
!   iband:  ('index_band') integer, identification number of the band
!   
! OUTPUT:
!   wTb:    ('weight_Temperature_b') real, fraction of B_nu
!
! ROUTINES:
!   None
!
! INCLUDES:
!   None
!
! COMMON BLOCK:
!   XOPTA:  opacity table data read by DFOPTA,    access: readonly
!   xopta1: interpolation coefficients for wtbbd, access: readonly
!
! SIDE EFFECTS:
!   No file access.
!
! RESTRICTIONS:
!   NTBN has to be <= 4.
!   Before first call of this routine, dfopta has to be called to read all
!   data from opacity table file and to compute the interpolation coefficients
!   in xopta1.
!
! PROCEDURE:
!   xbnu0 is an functional twin of xbnuwt, but considerably faster.
!   It computes the fraction of the Planck function B_nu
!   which are covered by frequency band IBAND at temperature T.
!   It essentially interpolates the values given in XOPTA
!   for the given temperature. 
!
! EXAMPLE:
!
! MODIFICATION HISTORY:
!   13.12.91 Hans-Guenter Ludwig, Astrophysik Kiel
!   30. 5.97 Bernd Freytag, Astrophysik Kiel
!   19-Mar-2001 Matthias Steffen, Astrophysikalisches Institur Potsdam
!   2007-03-05 (B.F. Lyon) Limit temperatures by next-to-last values
!----------------------------------------------------------------------+-------
!
!     --- XOPTA contains the opacity table data read by DFOPTA ---
      USE xopta_def
!     --- xopta1 contains the interpolation coefficients for wtbbd ---
      USE xopta1_def
!
      IMPLICIT NONE
!
!     --- I/O parameters ---
      real,    intent(in)               :: T
      integer, intent(in)               :: iband
      real,    intent(out)              :: wTb
!
!     --- Local variables ---
      integer                           :: iTx, nTx
      real, parameter                   :: alge=0.434294481
!     SAVE ALGE
!     DATA ALGE / 0.434294481 /
!     ALGE = ALOG10(EXP(1.0)), INTMOD=3 defined in dfopta
      real                              :: Tx, dT
!----------------------------------------------------------------------+-------
!
!     --- Delimit value of TX to the range given by tabT ---
!      Tx=min(tabT(nT), max(tabT(1), alog10(T)))
      Tx=min(tabT(nT-1), max(tabT(2), alog10(T)))
!
!     --- Determine interval index nTx for temperature interpolation ---
!     --- The interval index is defined different for MAP and CUBIT! ---
!     --- It is assumed that Tx can not be smaller than tabT(1).     ---
      if (Tx .lt. tabTBN(2)) then
        nTx=inT((Tx - tabTBN(1)) / tabDTB(1)) + idxTBN(1)
      else if (Tx .lt. tabTBN(3)) then
        nTx=inT((Tx - tabTBN(2)) / tabDTB(2)) + idxTBN(2)
      else if (Tx .lt. tabTBN(4)) then
        nTx=inT((Tx - tabTBN(3)) / tabDTB(3)) + idxTBN(3)
      else
        nTx=nT - 1
      endif
      iTx=min(max(1,nTx),nT-1)
!
!     --- Evaluate cubic polynomial and derivative ---
      dT    =Tx - tabT(iTx)
      wTb   =dT*(dT*(    dT*wTbbdc(1,iTx,iband)  + &
                            wTbbdc(2,iTx,iband)) + &
                            wTbbdc(3,iTx,iband)) + &
                            wTbbdc(4,iTx,iband)
!
!     --- B_i(T) = b_i(T) * sigma/pi * T**4
!
      END SUBROUTINE xbnu0
!--------------------*****---------------------------------------------+-------


!----------------*****-------------------------------------------------+-------
      subroutine xbnu1(T, iband, wTb)
!----------------------------------------------------------------------+-------
! NAME:
!   xbnu1 ('x_b_nu_Temperature_1')
!
! PURPOSE:
!   Compute the fraction of the Planck function B_nu
!
! CATEGORY:
!   Opacities, Interpolation
!
! CALLING SEQUENCE:
!   call xbnu1(T, iband, wTb)
!
! INPUT:
!   T:      ('Temperature') real, dimension(:), temperature [K]
!   iband:  ('index_band') integer, identification number of the band
!   
! OUTPUT:
!   wTb:    ('weight_Temperature_b') real, dimension(:), fraction of B_nu
!
! ROUTINES:
!   None
!
! INCLUDES:
!   None
!
! COMMON BLOCK:
!   XOPTA:  opacity table data read by DFOPTA,    access: readonly
!   xopta1: interpolation coefficients for wtbbd, access: readonly
!
! SIDE EFFECTS:
!   No file access.
!
! RESTRICTIONS:
!   NTBN has to be <= 4.
!   Before first call of this routine, dfopta has to be called to read all
!   data from opacity table file and to compute the interpolation coefficients
!   in xopta1.
!
! PROCEDURE:
!   xbnu1 is a 1D vectorized version of xbnuwt.
!   It computes the fraction of the Planck function B_nu
!   which are covered by frequency band IBAND at temperature T.
!   It essentially interpolates the values given in XOPTA
!   for the given temperature. 
!
! EXAMPLE:
!
! MODIFICATION HISTORY:
!   13.12.91 Hans-Guenter Ludwig, Astrophysik Kiel
!   30. 5.97 Bernd Freytag, Astrophysik Kiel
!   19-Mar-2001 Matthias Steffen, Astrophysikalisches Institur Potsdam
!   2007-03-05 (B.F. Lyon) Limit temperatures by next-to-last values
!----------------------------------------------------------------------+-------
!
!     --- XOPTA contains the opacity table data read by DFOPTA ---
      USE xopta_def
!     --- xopta1 contains the interpolation coefficients for wtbbd ---
      USE xopta1_def
!
      IMPLICIT NONE
!
!     --- I/O parameters ---
      real, dimension(:),       intent(in)  :: T
      integer,                  intent(in)  :: iband
      real, dimension(:),       intent(out) :: wTb
!
!     --- Local variables ---
      real, parameter                   :: alge=0.434294481
!     SAVE ALGE
!     DATA ALGE / 0.434294481 /
!     ALGE = ALOG10(EXP(1.0)), INTMOD=3 defined in dfopta
      integer                           :: iTx, nTx, i1
      real                              :: Tx, dT
!----------------------------------------------------------------------+-------
!
      do i1=1,size(T,1)
!
!     --- Delimit value of TX to the range given by tabT ---
!      Tx=min(tabT(nT), max(tabT(1), alog10(T(i1))))
      Tx=min(tabT(nT-1), max(tabT(2), alog10(T(i1))))
!
!     --- Determine interval index nTx for temperature interpolation ---
!     --- The interval index is defined different for MAP and CUBIT! ---
!     --- It is assumed that Tx can not be smaller than tabT(1).     ---
      if (Tx .lt. tabTBN(2)) then
        nTx=inT((Tx - tabTBN(1)) / tabDTB(1)) + idxTBN(1)
      else if (Tx .lt. tabTBN(3)) then
        nTx=inT((Tx - tabTBN(2)) / tabDTB(2)) + idxTBN(2)
      else if (Tx .lt. tabTBN(4)) then
        nTx=inT((Tx - tabTBN(3)) / tabDTB(3)) + idxTBN(3)
      else
        nTx=nT - 1
      endif
      iTx=min(max(1,nTx),nT-1)
!
!     --- Evaluate cubic polynomial and derivative ---
      dT    =Tx - tabT(iTx)
      wTb(i1)=dT*(dT*(    dT*wTbbdc(1,iTx,iband)  + &
                             wTbbdc(2,iTx,iband)) + &
                             wTbbdc(3,iTx,iband)) + &
                             wTbbdc(4,iTx,iband)
!
!     --- B_i(T) = b_i(T) * sigma/pi * T**4
!
      end do ! i1
!
      END SUBROUTINE xbnu1
!--------------------*****---------------------------------------------+-------


!----------------*****-------------------------------------------------+-------
      subroutine xbnu2(T, iband, wTb)
!----------------------------------------------------------------------+-------
! NAME:
!   xbnu2 ('x_b_nu_Temperature_2')
!
! PURPOSE:
!   Compute the fraction of the Planck function B_nu
!
! CATEGORY:
!   Opacities, Interpolation
!
! CALLING SEQUENCE:
!   call xbnu2(T, iband, wTb)
!
! INPUT:
!   T:      ('Temperature') real, dimension(:,:), temperature [K]
!   iband:  ('index_band') integer, identification number of the band
!   
! OUTPUT:
!   wTb:    ('weight_Temperature_b') real, dimension(:,:), fraction of B_nu
!
! ROUTINES:
!   None
!
! INCLUDES:
!   None
!
! COMMON BLOCK:
!   XOPTA:  opacity table data read by DFOPTA,    access: readonly
!   xopta1: interpolation coefficients for wtbbd, access: readonly
!
! SIDE EFFECTS:
!   No file access.
!
! RESTRICTIONS:
!   NTBN has to be <= 4.
!   Before first call of this routine, dfopta has to be called to read all
!   data from opacity table file and to compute the interpolation coefficients
!   in xopta1.
!
! PROCEDURE:
!   xbnu2 is a 2D vectorized version of xbnuwt.
!   It computes the fraction of the Planck function B_nu
!   which are covered by frequency band IBAND at temperature T.
!   It essentially interpolates the values given in XOPTA
!   for the given temperature. 
!
! EXAMPLE:
!
! MODIFICATION HISTORY:
!   13.12.91 Hans-Guenter Ludwig, Astrophysik Kiel
!   30. 5.97 Bernd Freytag, Astrophysik Kiel
!    8. 2.01 (B.F. Uppsala) 2D version
!   19-Mar-2001 Matthias Steffen, Astrophysikalisches Institur Potsdam
!   2007-03-05 (B.F. Lyon) Limit temperatures by next-to-last values
!----------------------------------------------------------------------+-------
!
!     --- XOPTA contains the opacity table data read by DFOPTA ---
      USE xopta_def
!     --- xopta1 contains the interpolation coefficients for wtbbd ---
      USE xopta1_def
!
      IMPLICIT NONE
!
!     --- I/O parameters ---
      real, dimension(:,:),                 intent(in)  :: T
      integer,                              intent(in)  :: iband
      real, dimension(:,:),                 intent(out) :: wTb
!
!     --- Local variables ---
      real, parameter                   :: alge=0.434294481
!     SAVE ALGE
!     DATA ALGE / 0.434294481 /
!     ALGE = ALOG10(EXP(1.0)), INTMOD=3 defined in dfopta
      integer                           :: iTx, nTx, i1, i2
      real                              :: Tx, dT
!----------------------------------------------------------------------+-------
!
      do i2=1,size(T,2)
      do i1=1,size(T,1)
!
!     --- Delimit value of TX to the range given by tabT ---
!      Tx=min(tabT(nT), max(tabT(1), alog10(T(i1,i2))))
      Tx=min(tabT(nT-1), max(tabT(2), alog10(T(i1,i2))))
!
!     --- Determine interval index nTx for temperature interpolation ---
!     --- The interval index is defined different for MAP and CUBIT! ---
!     --- It is assumed that Tx can not be smaller than tabT(1).     ---
      if (Tx .lt. tabTBN(2)) then
        nTx=inT((Tx - tabTBN(1)) / tabDTB(1)) + idxTBN(1)
      else if (Tx .lt. tabTBN(3)) then
        nTx=inT((Tx - tabTBN(2)) / tabDTB(2)) + idxTBN(2)
      else if (Tx .lt. tabTBN(4)) then
        nTx=inT((Tx - tabTBN(3)) / tabDTB(3)) + idxTBN(3)
      else
        nTx=nT - 1
      endif
      iTx=min(max(1,nTx),nT-1)
!
!     --- Evaluate cubic polynomial and derivative ---
      dT    =Tx - tabT(iTx)
      wTb(i1,i2)=dT*(dT*(    dT*wTbbdc(1,iTx,iband)  + &
                                wTbbdc(2,iTx,iband)) + &
                                wTbbdc(3,iTx,iband)) + &
                                wTbbdc(4,iTx,iband)
!
!     --- B_i(T) = b_i(T) * sigma/pi * T**4
!
      end do ! i1
      end do ! i2
!
      END SUBROUTINE xbnu2
!--------------------*****---------------------------------------------+-------


!----------------*****-------------------------------------------------+-------
      subroutine xbnu3(T, iband, wTb)
!----------------------------------------------------------------------+-------
! NAME:
!   xbnu3 ('x_b_nu_weight_Temperature_3')
!
! PURPOSE:
!   Compute the fraction of the Planck function B_nu
!
! CATEGORY:
!   Opacities, Interpolation
!
! CALLING SEQUENCE:
!   call xbnu3(T, iband, wTb)
!
! INPUT:
!   T:      ('Temperature') real, dimension(:,:,:), temperature [K]
!   iband:  ('index_band') integer, identification number of the band
!   
! OUTPUT:
!   wTb:    ('weight_Temperature_b') real, dimension(:,:,:), fraction of B_nu
!
! ROUTINES:
!   None
!
! INCLUDES:
!   None
!
! COMMON BLOCK:
!   XOPTA:  opacity table data read by DFOPTA,    access: readonly
!   xopta1: interpolation coefficients for wtbbd, access: readonly
!
! SIDE EFFECTS:
!   No file access.
!
! RESTRICTIONS:
!   NTBN has to be <= 4.
!   Before first call of this routine, dfopta has to be called to read all
!   data from opacity table file and to compute the interpolation coefficients
!   in xopta1.
!
! PROCEDURE:
!   xbnu3 is a 3D vectorized version of xbnuwt.
!   It computes the fraction of the Planck function B_nu
!   which are covered by frequency band IBAND at temperature T.
!   It essentially interpolates the values given in XOPTA
!   for the given temperature. 
!
! EXAMPLE:
!
! MODIFICATION HISTORY:
!   13.12.91 Hans-Guenter Ludwig, Astrophysik Kiel
!   30. 5.97 Bernd Freytag, Astrophysik Kiel
!    8. 2.01 (B.F. Uppsala) 2D version
!   19-Mar-2001 Matthias Steffen, Astrophysikalisches Institur Potsdam
!   2007-03-05 (B.F. Lyon) Limit temperatures by next-to-last values
!----------------------------------------------------------------------+-------
!
!     --- XOPTA contains the opacity table data read by DFOPTA ---
      USE xopta_def
!     --- xopta1 contains the interpolation coefficients for wtbbd ---
      USE xopta1_def
!
      IMPLICIT NONE
!
!     --- I/O parameters ---
      real, dimension(:,:,:),               intent(in)  :: T
      integer,                              intent(in)  :: iband
      real, dimension(:,:,:),               intent(out) :: wTb
!
!     --- Local variables ---
      real, parameter                   :: alge=0.434294481
!     SAVE ALGE
!     DATA ALGE / 0.434294481 /
!     ALGE = ALOG10(EXP(1.0)), INTMOD=3 defined in dfopta
      integer                           :: iTx, nTx, i1, i2, i3
      real                              :: Tx, dT
!----------------------------------------------------------------------+-------
!
!$OMP DO
      do i3=1,size(T,3)
      do i2=1,size(T,2)
      do i1=1,size(T,1)
!
!     --- Delimit value of TX to the range given by tabT ---
!      Tx=min(tabT(nT), max(tabT(1), alog10(T(i1,i2,i3))))
      Tx=min(tabT(nT-1), max(tabT(2), alog10(T(i1,i2,i3))))
!
!     --- Determine interval index nTx for temperature interpolation ---
!     --- The interval index is defined different for MAP and CUBIT! ---
!     --- It is assumed that Tx can not be smaller than tabT(1).     ---
      if (Tx .lt. tabTBN(2)) then
        nTx=inT((Tx - tabTBN(1)) / tabDTB(1)) + idxTBN(1)
      else if (Tx .lt. tabTBN(3)) then
        nTx=inT((Tx - tabTBN(2)) / tabDTB(2)) + idxTBN(2)
      else if (Tx .lt. tabTBN(4)) then
        nTx=inT((Tx - tabTBN(3)) / tabDTB(3)) + idxTBN(3)
      else
        nTx=nT - 1
      endif
      iTx=min(max(1,nTx),nT-1)
!
!     --- Evaluate cubic polynomial and derivative ---
      dT    =Tx - tabT(iTx)
      wTb(i1,i2,i3)=dT*(dT*(    dT*wTbbdc(1,iTx,iband)  + &
                                   wTbbdc(2,iTx,iband)) + &
                                   wTbbdc(3,iTx,iband)) + &
                                   wTbbdc(4,iTx,iband)
!
!     --- B_i(T) = b_i(T) * sigma/pi * T**4
!
      end do ! i1
      end do ! i2
      end do ! i3
!$OMP END DO
!
      END SUBROUTINE xbnu3
!--------------------*****---------------------------------------------+-------


!----------------*******-----------------------------------------------+-------
      subroutine xbnuwT0(T, iband, wTb, wTdbdT)
!----------------------------------------------------------------------+-------
! NAME:
!   xbnuwT0 ('x_b_nu_weight_Temperature_0')
!
! PURPOSE:
!   Compute the fraction of the Planck function B_nu
!   and the fraction of the temperature derivative dB_nu/dT.
!
! CATEGORY:
!   Opacities, Interpolation
!
! CALLING SEQUENCE:
!   call xbnuwT0(T, iband, wTb, wTdbdT)
!
! INPUT:
!   T:      ('Temperature') real, temperature [K]
!   iband:  ('index_band') integer, identification number of the band
!   
! OUTPUT:
!   wTb:    ('weight_Temperature_b') real, fraction of B_nu
!   wTdbdT: ('weight_Temperature_db_dT') real, fraction of dB_nu/dT
!
! ROUTINES:
!   None
!
! INCLUDES:
!   None
!
! COMMON BLOCK:
!   XOPTA:  opacity table data read by DFOPTA,    access: readonly
!   xopta1: interpolation coefficients for wtbbd, access: readonly
!
! SIDE EFFECTS:
!   No file access.
!
! RESTRICTIONS:
!   NTBN has to be <= 4.
!   Before first call of this routine, dfopta has to be called to read all
!   data from opacity table file and to compute the interpolation coefficients
!   in xopta1.
!
! PROCEDURE:
!   xbnuwt0 is an functional twin of xbnuwt, but considerably faster.
!   It computes the fraction of the Planck function B_nu
!   and the fraction of the temperature derivative dB_nu/dT
!   which are covered by frequency band IBAND at temperature T.
!   It essentially interpolates the values given in XOPTA
!   for the given temperature. 
!
! EXAMPLE:
!
! MODIFICATION HISTORY:
!   13.12.91 Hans-Guenter Ludwig, Astrophysik Kiel
!   30. 5.97 Bernd Freytag, Astrophysik Kiel
!   2007-03-05 (B.F. Lyon) Limit temperatures by next-to-last values
!----------------------------------------------------------------------+-------
!
!     --- XOPTA contains the opacity table data read by DFOPTA ---
      USE xopta_def
!     --- xopta1 contains the interpolation coefficients for wtbbd ---
      USE xopta1_def
!
      IMPLICIT NONE
!
!     --- I/O parameters ---
      real,    intent(in)               :: T
      integer, intent(in)               :: iband
      real,    intent(out)              :: wTb, wTdbdT
!
!     --- Local variables ---
      integer                           :: iTx, nTx
      real, parameter                   :: alge=0.434294481
!     SAVE ALGE
!     DATA ALGE / 0.434294481 /
!     ALGE = ALOG10(EXP(1.0)), INTMOD=3 defined in dfopta
      real                              :: Tx, dT
!----------------------------------------------------------------------+-------
!
!     --- Delimit value of TX to the range given by tabT ---
!      Tx=min(tabT(nT), max(tabT(1), alog10(T)))
      Tx=min(tabT(nT-1), max(tabT(2), alog10(T)))
!
!     --- Determine interval index nTx for temperature interpolation ---
!     --- The interval index is defined different for MAP and CUBIT! ---
!     --- It is assumed that Tx can not be smaller than tabT(1).     ---
      if (Tx .lt. tabTBN(2)) then
        nTx=inT((Tx - tabTBN(1)) / tabDTB(1)) + idxTBN(1)
      else if (Tx .lt. tabTBN(3)) then
        nTx=inT((Tx - tabTBN(2)) / tabDTB(2)) + idxTBN(2)
      else if (Tx .lt. tabTBN(4)) then
        nTx=inT((Tx - tabTBN(3)) / tabDTB(3)) + idxTBN(3)
      else
        nTx=nT - 1
      endif
      iTx=min(max(1,nTx),nT-1)
!
!     --- Evaluate cubic polynomial and derivative ---
      dT    =Tx - tabT(iTx)
      wTb   =dT*(dT*(    dT*wTbbdc(1,iTx,iband)  + &
                            wTbbdc(2,iTx,iband)) + &
                            wTbbdc(3,iTx,iband)) + &
                            wTbbdc(4,iTx,iband)
      wTdbdT=    dT*(3.0*dT*wTbbdc(1,iTx,iband)  + &
                     2.0*   wTbbdc(2,iTx,iband)) + &
                            wTbbdc(3,iTx,iband)
!
!     --- B_i(T) = b_i(T) * sigma/pi * T**4
!     --- dB_i/dT = 4 * sigma/pi * T**3 * ( 1/4 * ALOG10(e) * db_i/dlgT + b_i)
      wTdbdT=0.25*wTdbdT*alge + wTB
!
      END SUBROUTINE xbnuwT0
!--------------------*******-------------------------------------------+-------


!----------------*******-----------------------------------------------+-------
      subroutine xbnuwT1(T, iband, wTb, wTdbdT)
!----------------------------------------------------------------------+-------
! NAME:
!   xbnuwT1 ('x_b_nu_weight_Temperature_1')
!
! PURPOSE:
!   Compute the fraction of the Planck function B_nu
!   and the fraction of the temperature derivative dB_nu/dT.
!
! CATEGORY:
!   Opacities, Interpolation
!
! CALLING SEQUENCE:
!   call xbnuwT1(T, iband, wTb, wTdbdT)
!
! INPUT:
!   T:      ('Temperature') real, dimension(:), temperature [K]
!   iband:  ('index_band') integer, identification number of the band
!   
! OUTPUT:
!   wTb:    ('weight_Temperature_b') real, dimension(:), fraction of B_nu
!   wTdbdb: ('weight_Temperature_db_dT') real, dimension(:), fraction of
!           dB_nu/dT
!
! ROUTINES:
!   None
!
! INCLUDES:
!   None
!
! COMMON BLOCK:
!   XOPTA:  opacity table data read by DFOPTA,    access: readonly
!   xopta1: interpolation coefficients for wtbbd, access: readonly
!
! SIDE EFFECTS:
!   No file access.
!
! RESTRICTIONS:
!   NTBN has to be <= 4.
!   Before first call of this routine, dfopta has to be called to read all
!   data from opacity table file and to compute the interpolation coefficients
!   in xopta1.
!
! PROCEDURE:
!   xbnuwt1 is a 1D vectorized version of xbnuwt.
!   It computes the fraction of the Planck function B_nu
!   and the fraction of the temperature derivative dB_nu/dT
!   which are covered by frequency band IBAND at temperature T.
!   It essentially interpolates the values given in XOPTA
!   for the given temperature. 
!
! EXAMPLE:
!
! MODIFICATION HISTORY:
!   13.12.91 Hans-Guenter Ludwig, Astrophysik Kiel
!   30. 5.97 Bernd Freytag, Astrophysik Kiel
!   2007-03-05 (B.F. Lyon) Limit temperatures by next-to-last values
!----------------------------------------------------------------------+-------
!
!     --- XOPTA contains the opacity table data read by DFOPTA ---
      USE xopta_def
!     --- xopta1 contains the interpolation coefficients for wtbbd ---
      USE xopta1_def
!
      IMPLICIT NONE
!
!     --- I/O parameters ---
      real, dimension(:),       intent(in)  :: T
      integer,                  intent(in)  :: iband
      real, dimension(:),       intent(out) :: wTb, wTdbdT
!
!     --- Local variables ---
      real, parameter                   :: alge=0.434294481
!     SAVE ALGE
!     DATA ALGE / 0.434294481 /
!     ALGE = ALOG10(EXP(1.0)), INTMOD=3 defined in dfopta
      integer                           :: iTx, nTx, i1
      real                              :: Tx, dT
!----------------------------------------------------------------------+-------
!
      do i1=1,size(T,1)
!
!     --- Delimit value of TX to the range given by tabT ---
!      Tx=min(tabT(nT), max(tabT(1), alog10(T(i1))))
      Tx=min(tabT(nT-1), max(tabT(2), alog10(T(i1))))
!
!     --- Determine interval index nTx for temperature interpolation ---
!     --- The interval index is defined different for MAP and CUBIT! ---
!     --- It is assumed that Tx can not be smaller than tabT(1).     ---
      if (Tx .lt. tabTBN(2)) then
        nTx=inT((Tx - tabTBN(1)) / tabDTB(1)) + idxTBN(1)
      else if (Tx .lt. tabTBN(3)) then
        nTx=inT((Tx - tabTBN(2)) / tabDTB(2)) + idxTBN(2)
      else if (Tx .lt. tabTBN(4)) then
        nTx=inT((Tx - tabTBN(3)) / tabDTB(3)) + idxTBN(3)
      else
        nTx=nT - 1
      endif
      iTx=min(max(1,nTx),nT-1)
!
!     --- Evaluate cubic polynomial and derivative ---
      dT    =Tx - tabT(iTx)
      wTb(i1)=dT*(dT*(    dT*wTbbdc(1,iTx,iband)  + &
                             wTbbdc(2,iTx,iband)) + &
                             wTbbdc(3,iTx,iband)) + &
                             wTbbdc(4,iTx,iband)
      wTdbdT(i1)= dT*(3.0*dT*wTbbdc(1,iTx,iband)  + &
                      2.0*   wTbbdc(2,iTx,iband)) + &
                             wTbbdc(3,iTx,iband)
!
!     --- B_i(T) = b_i(T) * sigma/pi * T**4
!     --- dB_i/dT = 4 * sigma/pi * T**3 * ( 1/4 * ALOG10(e) * db_i/dlgT + b_i)
      wTdbdT(i1)=0.25*wTdbdT(i1)*alge + wTB(i1)
!
      end do ! i1
!
      END SUBROUTINE xbnuwT1
!--------------------*******-------------------------------------------+-------


!----------------*******-----------------------------------------------+-------
      subroutine xbnuwT2(T, iband, wTb, wTdbdT)
!----------------------------------------------------------------------+-------
! NAME:
!   xbnuwT2 ('x_b_nu_weight_Temperature_2')
!
! PURPOSE:
!   Compute the fraction of the Planck function B_nu
!   and the fraction of the temperature derivative dB_nu/dT.
!
! CATEGORY:
!   Opacities, Interpolation
!
! CALLING SEQUENCE:
!   call xbnuwT2(T, iband, wTb, wTdbdT)
!
! INPUT:
!   T:      ('Temperature') real, dimension(:,:), temperature [K]
!   iband:  ('index_band') integer, identification number of the band
!   
! OUTPUT:
!   wTb:    ('weight_Temperature_b') real, dimension(:,:), fraction of B_nu
!   wTdbdb: ('weight_Temperature_db_dT') real, dimension(:,:), fraction of
!           dB_nu/dT
!
! ROUTINES:
!   None
!
! INCLUDES:
!   None
!
! COMMON BLOCK:
!   XOPTA:  opacity table data read by DFOPTA,    access: readonly
!   xopta1: interpolation coefficients for wtbbd, access: readonly
!
! SIDE EFFECTS:
!   No file access.
!
! RESTRICTIONS:
!   NTBN has to be <= 4.
!   Before first call of this routine, dfopta has to be called to read all
!   data from opacity table file and to compute the interpolation coefficients
!   in xopta1.
!
! PROCEDURE:
!   xbnuwt2 is a 2D vectorized version of xbnuwt.
!   It computes the fraction of the Planck function B_nu
!   and the fraction of the temperature derivative dB_nu/dT
!   which are covered by frequency band IBAND at temperature T.
!   It essentially interpolates the values given in XOPTA
!   for the given temperature. 
!
! EXAMPLE:
!
! MODIFICATION HISTORY:
!   13.12.91 Hans-Guenter Ludwig, Astrophysik Kiel
!   30. 5.97 Bernd Freytag, Astrophysik Kiel
!    8. 2.01 (B.F. Uppsala) 2D version
!   2007-03-05 (B.F. Lyon) Limit temperatures by next-to-last values
!----------------------------------------------------------------------+-------
!
!     --- XOPTA contains the opacity table data read by DFOPTA ---
      USE xopta_def
!     --- xopta1 contains the interpolation coefficients for wtbbd ---
      USE xopta1_def
!
      IMPLICIT NONE
!
!     --- I/O parameters ---
      real, dimension(:,:),                 intent(in)  :: T
      integer,                              intent(in)  :: iband
      real, dimension(:,:),                 intent(out) :: wTb, wTdbdT
!
!     --- Local variables ---
      real, parameter                   :: alge=0.434294481
!     SAVE ALGE
!     DATA ALGE / 0.434294481 /
!     ALGE = ALOG10(EXP(1.0)), INTMOD=3 defined in dfopta
      integer                           :: iTx, nTx, i1, i2
      real                              :: Tx, dT
!----------------------------------------------------------------------+-------
!
      do i2=1,size(T,2)
      do i1=1,size(T,1)
!
!     --- Delimit value of TX to the range given by tabT ---
!      Tx=min(tabT(nT), max(tabT(1), alog10(T(i1,i2))))
      Tx=min(tabT(nT-1), max(tabT(2), alog10(T(i1,i2))))
!
!     --- Determine interval index nTx for temperature interpolation ---
!     --- The interval index is defined different for MAP and CUBIT! ---
!     --- It is assumed that Tx can not be smaller than tabT(1).     ---
      if (Tx .lt. tabTBN(2)) then
        nTx=inT((Tx - tabTBN(1)) / tabDTB(1)) + idxTBN(1)
      else if (Tx .lt. tabTBN(3)) then
        nTx=inT((Tx - tabTBN(2)) / tabDTB(2)) + idxTBN(2)
      else if (Tx .lt. tabTBN(4)) then
        nTx=inT((Tx - tabTBN(3)) / tabDTB(3)) + idxTBN(3)
      else
        nTx=nT - 1
      endif
      iTx=min(max(1,nTx),nT-1)
!
!     --- Evaluate cubic polynomial and derivative ---
      dT    =Tx - tabT(iTx)
      wTb(i1,i2)=dT*(dT*(    dT*wTbbdc(1,iTx,iband)  + &
                                wTbbdc(2,iTx,iband)) + &
                                wTbbdc(3,iTx,iband)) + &
                                wTbbdc(4,iTx,iband)
      wTdbdT(i1,i2)= dT*(3.0*dT*wTbbdc(1,iTx,iband)  + &
                         2.0*   wTbbdc(2,iTx,iband)) + &
                                wTbbdc(3,iTx,iband)
!
!     --- B_i(T) = b_i(T) * sigma/pi * T**4
!     --- dB_i/dT = 4 * sigma/pi * T**3 * ( 1/4 * ALOG10(e) * db_i/dlgT + b_i)
      wTdbdT(i1,i2)=0.25*wTdbdT(i1,i2)*alge + wTB(i1,i2)
!
      end do ! i1
      end do ! i2
!
      END SUBROUTINE xbnuwT2
!--------------------*******-------------------------------------------+-------


!----------------*******-----------------------------------------------+-------
      subroutine xbnuwT3(T, iband, wTb, wTdbdT)
!----------------------------------------------------------------------+-------
! NAME:
!   xbnuwT3 ('x_b_nu_weight_Temperature_3')
!
! PURPOSE:
!   Compute the fraction of the Planck function B_nu
!   and the fraction of the temperature derivative dB_nu/dT.
!
! CATEGORY:
!   Opacities, Interpolation
!
! CALLING SEQUENCE:
!   call xbnuwT3(T, iband, wTb, wTdbdT)
!
! INPUT:
!   T:      ('Temperature') real, dimension(:,:,:), temperature [K]
!   iband:  ('index_band') integer, identification number of the band
!   
! OUTPUT:
!   wTb:    ('weight_Temperature_b') real, dimension(:,:,:), fraction of B_nu
!   wTdbdb: ('weight_Temperature_db_dT') real, dimension(:,:,:), fraction of
!           dB_nu/dT
!
! ROUTINES:
!   None
!
! INCLUDES:
!   None
!
! COMMON BLOCK:
!   XOPTA:  opacity table data read by DFOPTA,    access: readonly
!   xopta1: interpolation coefficients for wtbbd, access: readonly
!
! SIDE EFFECTS:
!   No file access.
!
! RESTRICTIONS:
!   NTBN has to be <= 4.
!   Before first call of this routine, dfopta has to be called to read all
!   data from opacity table file and to compute the interpolation coefficients
!   in xopta1.
!
! PROCEDURE:
!   xbnuwt3 is a 3D vectorized version of xbnuwt.
!   It computes the fraction of the Planck function B_nu
!   and the fraction of the temperature derivative dB_nu/dT
!   which are covered by frequency band IBAND at temperature T.
!   It essentially interpolates the values given in XOPTA
!   for the given temperature. 
!
! EXAMPLE:
!
! MODIFICATION HISTORY:
!   13.12.91 Hans-Guenter Ludwig, Astrophysik Kiel
!   30. 5.97 Bernd Freytag, Astrophysik Kiel
!    8. 2.01 (B.F. Uppsala) 2D version
!   2007-03-05 (B.F. Lyon) Limit temperatures by next-to-last values
!----------------------------------------------------------------------+-------
!
!     --- XOPTA contains the opacity table data read by DFOPTA ---
      USE xopta_def
!     --- xopta1 contains the interpolation coefficients for wtbbd ---
      USE xopta1_def
!
      IMPLICIT NONE
!
!     --- I/O parameters ---
      real, dimension(:,:,:),               intent(in)  :: T
      integer,                              intent(in)  :: iband
      real, dimension(:,:,:),               intent(out) :: wTb, wTdbdT
!
!     --- Local variables ---
      real, parameter                   :: alge=0.434294481
!     SAVE ALGE
!     DATA ALGE / 0.434294481 /
!     ALGE = ALOG10(EXP(1.0)), INTMOD=3 defined in dfopta
      integer                           :: iTx, nTx, i1, i2, i3
      real                              :: Tx, dT
!----------------------------------------------------------------------+-------
!
!$OMP DO
      do i3=1,size(T,3)
      do i2=1,size(T,2)
      do i1=1,size(T,1)
!
!     --- Delimit value of TX to the range given by tabT ---
!      Tx=min(tabT(nT), max(tabT(1), alog10(T(i1,i2,i3))))
      Tx=min(tabT(nT-1), max(tabT(2), alog10(T(i1,i2,i3))))
!
!     --- Determine interval index nTx for temperature interpolation ---
!     --- The interval index is defined different for MAP and CUBIT! ---
!     --- It is assumed that Tx can not be smaller than tabT(1).     ---
      if (Tx .lt. tabTBN(2)) then
        nTx=inT((Tx - tabTBN(1)) / tabDTB(1)) + idxTBN(1)
      else if (Tx .lt. tabTBN(3)) then
        nTx=inT((Tx - tabTBN(2)) / tabDTB(2)) + idxTBN(2)
      else if (Tx .lt. tabTBN(4)) then
        nTx=inT((Tx - tabTBN(3)) / tabDTB(3)) + idxTBN(3)
      else
        nTx=nT - 1
      endif
      iTx=min(max(1,nTx),nT-1)
!
!     --- Evaluate cubic polynomial and derivative ---
      dT    =Tx - tabT(iTx)
      wTb(i1,i2,i3)=dT*(dT*(    dT*wTbbdc(1,iTx,iband)  + &
                                   wTbbdc(2,iTx,iband)) + &
                                   wTbbdc(3,iTx,iband)) + &
                                   wTbbdc(4,iTx,iband)
      wTdbdT(i1,i2,i3)= dT*(3.0*dT*wTbbdc(1,iTx,iband)  + &
                            2.0*   wTbbdc(2,iTx,iband)) + &
                                   wTbbdc(3,iTx,iband)
!
!     --- B_i(T) = b_i(T) * sigma/pi * T**4
!     --- dB_i/dT = 4 * sigma/pi * T**3 * ( 1/4 * ALOG10(e) * db_i/dlgT + b_i)
      wTdbdT(i1,i2,i3)=0.25*wTdbdT(i1,i2,i3)*alge + wTB(i1,i2,i3)
!
      end do ! i1
      end do ! i2
      end do ! i3
!$OMP END DO
!
      END SUBROUTINE xbnuwT3
!--------------------*******-------------------------------------------+-------
      END MODULE opta_module
!----------------***********-------------------------------------------+-------


!------------**********------------------------------------------------+-------
      MODULE capco_data

!     Data for the calculation of CAPCO = CO opacity per unit mass
!     02-NOV-2005 (M.S.)
!
      IMPLICIT NONE
      SAVE
!
!  FOLLOWING TABLE ASSUMES XC = 1.75E20 PER GRAM (C/H = 4 E-4)          
      REAL, DIMENSION(94) :: CAP = (/                                   &
       2.024E+00,2.088E+00,2.154E+00,2.208E+00,2.249E+00,2.278E+00,     &
       2.297E+00,2.308E+00,2.314E+00,2.317E+00,2.317E+00,2.316E+00,     &
       2.314E+00,2.313E+00,2.313E+00,2.315E+00,2.319E+00,2.325E+00,     &
       2.335E+00,2.347E+00,2.363E+00,2.384E+00,2.570E+00,2.746E+00,     &
       2.737E+00,2.727E+00,2.717E+00,2.707E+00,2.696E+00,2.685E+00,     &
       2.674E+00,2.662E+00,2.651E+00,2.640E+00,2.629E+00,2.618E+00,     &
       2.607E+00,2.596E+00,2.585E+00,2.574E+00,2.563E+00,2.553E+00,     &
       2.542E+00,2.532E+00,2.522E+00,2.512E+00,2.502E+00,2.492E+00,     &
       2.482E+00,2.472E+00,2.463E+00,2.453E+00,2.444E+00,2.435E+00,     &
       2.425E+00,2.416E+00,2.407E+00,2.398E+00,2.389E+00,2.380E+00,     &
       2.371E+00,2.361E+00,2.352E+00,2.343E+00,2.334E+00,2.325E+00,     &
       2.316E+00,2.306E+00,2.297E+00,2.288E+00,2.278E+00,2.268E+00,     &
       2.259E+00,2.249E+00,2.239E+00,2.228E+00,2.218E+00,2.208E+00,     &
       2.197E+00,2.186E+00,2.175E+00,2.163E+00,2.152E+00,2.140E+00,     &
       2.128E+00,2.116E+00,2.103E+00,2.091E+00,2.078E+00,2.064E+00,     &
       2.051E+00,2.037E+00,2.023E+00,2.023E+00 /)

      END MODULE capco_data
!----------------**********--------------------------------------------+-------


!------------********--------------------------------------------------+-------
      MODULE xco_data
!
!     Data for the calculation of XCO = fraction of C atmos in CO
!
!     XC = nC/nH = =4.0D-4 (adopted Carbon abundance)
!     XO = nO/nH = =8.0D-4 (adopted Oxygen abundance)
!     XOXC = XO*XC, XOPC = XO+XC
!     SAHA = nC*nO/nCO
!     02-NOV-2005 (M.S.)
!
      IMPLICIT NONE
      SAVE
!
      INTEGER, PARAMETER       :: q=SELECTED_REAL_KIND(P=10,R=80)
      REAL(KIND=q), PARAMETER  :: HPERG=4.37D23
      REAL(KIND=q), PARAMETER  :: XC  =4.0D-4, XO  =8.0D-4, &
                                  XOXC=3.2D-7, XOPC=1.2D-3  
!
!
!   USE OF THE FOLLOWING TABLE HAS BEEN CHECKED TO YIELD XNCO WITHIN    
!   ABOUT 2% FOR TYPICAL SOLAR CHROMOSPHERE CONDITIONS.  21.11.83.      
      REAL(KIND=q), DIMENSION(101) :: SAHA = (/                         &
       0.D0     ,0.D0     ,0.D0     ,0.D0     ,0.D0     ,1.090D-68,     &
       2.414D-55,2.366D-45,1.464D-37,2.508D-31,3.161D-26,5.625D-22,     &
       2.219D-18,2.681D-15,1.256D-12,2.725D-10,3.137D-08,2.130D-06,     &
       9.265D-05,2.762D-03,5.955D-02,9.701D-01,1.239D+01,1.279D+02,     &
       1.095D+03,7.943D+03,4.972D+04,2.729D+05,1.331D+06,5.839D+06,     &
       2.328D+07,8.507D+07,2.874D+08,9.033D+08,2.659D+09,7.369D+09,     &
       1.932D+10,4.815D+10,1.145D+11,2.606D+11,5.697D+11,1.200D+12,     &
       2.441D+12,4.807D+12,9.183D+12,1.706D+13,3.085D+13,5.444D+13,     &
       9.385D+13,1.583D+14,2.615D+14,4.239D+14,6.745D+14,1.055D+15,     &
       1.623D+15,2.459D+15,3.671D+15,5.406D+15,7.855D+15,1.127D+16,     &
       1.599D+16,2.242D+16,3.110D+16,4.270D+16,5.806D+16,7.822D+16,     &
       1.044D+17,1.382D+17,1.815D+17,2.365D+17,3.058D+17,3.926D+17,     &
       5.006D+17,6.341D+17,7.981D+17,9.986D+17,1.242D+18,1.536D+18,     &
       1.890D+18,2.313D+18,2.816D+18,3.413D+18,4.118D+18,4.945D+18,     &
       5.912D+18,7.040D+18,8.349D+18,9.863D+18,1.161D+19,1.361D+19,     &
       1.591D+19,1.852D+19,2.150D+19,2.488D+19,2.870D+19,3.301D+19,     &
       3.785D+19,4.329D+19,4.937D+19,5.616D+19,5.616D+19/)
!
      END MODULE xco_data
!----------------********----------------------------------------------+-------


!------------**********------------------------------------------------+-------
      MODULE bnuwt_data
!
      IMPLICIT NONE
      SAVE

      REAL, DIMENSION(2) :: CW1 = (/ -1.06718D-02, 1.51828D-03 /)
      REAL, DIMENSION(2) :: CW2 = (/ -3.51202D+01,-3.64468D+01 /)
      REAL, DIMENSION(2) :: CW3 = (/  5.45520D+05, 3.13299D+05 /)
      REAL, DIMENSION(2) :: CW4 = (/ -2.70772D+08, 1.31574D+07 /)

      END MODULE bnuwt_data
!----------------**********--------------------------------------------+-------


!------------*********-------------------------------------------------+-------
      MODULE co_module
!*******************************************************************************
! Carbon Monoxide (CO) Opacity Routines
!*******************************************************************************
! SUBROUTINES:
!   capco:  Compute carbon monoxide - plank mean opacity per gram.
!   xco:    Compute LTE carbon monoxide fraction.
!   bnuwt:  bandpass weighting function.
!*******************************************************************************
      !
      interface sub_capco
        module procedure sub_capco0, sub_capco1, sub_capco2, sub_capco3
      end interface ! sub_capco
      !
      interface sub_xco
        module procedure sub_xco0, sub_xco1, sub_xco2, sub_xco3
      end interface ! sub_xco
      !
      interface sub_bnuwt
        module procedure sub_bnuwt0, sub_bnuwt1, sub_bnuwt2, sub_bnuwt3
      end interface ! sub_bnuwt
      !

      CONTAINS

!-------------------*****----------------------------------------------+-------
      REAL FUNCTION CAPCO(T,XNCO)                                          
!----------------------------------------------------------------------+-------
!       CARBON MONOXIDE - PLANK MEAN OPACITY PER GRAM                   
!         21.11.83  D.O.M.(MODIFIED 8.11.83 D.O.M.)                     
!         NEW BANDPASS  (MODIFIED 3.6.85 D.O.M.)                        
!         F90 version 02-NOV-2005 (M.S.)
!                                                                       
!       INPUT :  TEMPERATURE                                            
!                XNCO  = FRACTION OF C ATOMS IN CO.                     
!       OUTPUT:  CAPCO = AN APPROXIMATE OPACITY FOR CO.                 
!                FOR T = 800 TO 10000 K                                 
!----------------------------------------------------------------------+-------
      IMPLICIT NONE
!
!--- Dummy arguments:
      REAL,          INTENT(IN)     :: T, XNCO
!
!--- Local Variables ---
      INTEGER                   :: IT
      REAL                      :: TT, FRACT
      REAL, DIMENSION(93), SAVE :: CAP
!  FOLLOWING TABLE ASSUMES XC = 1.75E20 PER GRAM (C/H = 4 E-4)          
      DATA CAP/                                                         &
       2.024E+00,2.088E+00,2.154E+00,2.208E+00,2.249E+00,2.278E+00,     &
       2.297E+00,2.308E+00,2.314E+00,2.317E+00,2.317E+00,2.316E+00,     &
       2.314E+00,2.313E+00,2.313E+00,2.315E+00,2.319E+00,2.325E+00,     &
       2.335E+00,2.347E+00,2.363E+00,2.384E+00,2.570E+00,2.746E+00,     &
       2.737E+00,2.727E+00,2.717E+00,2.707E+00,2.696E+00,2.685E+00,     &
       2.674E+00,2.662E+00,2.651E+00,2.640E+00,2.629E+00,2.618E+00,     &
       2.607E+00,2.596E+00,2.585E+00,2.574E+00,2.563E+00,2.553E+00,     &
       2.542E+00,2.532E+00,2.522E+00,2.512E+00,2.502E+00,2.492E+00,     &
       2.482E+00,2.472E+00,2.463E+00,2.453E+00,2.444E+00,2.435E+00,     &
       2.425E+00,2.416E+00,2.407E+00,2.398E+00,2.389E+00,2.380E+00,     &
       2.371E+00,2.361E+00,2.352E+00,2.343E+00,2.334E+00,2.325E+00,     &
       2.316E+00,2.306E+00,2.297E+00,2.288E+00,2.278E+00,2.268E+00,     &
       2.259E+00,2.249E+00,2.239E+00,2.228E+00,2.218E+00,2.208E+00,     &
       2.197E+00,2.186E+00,2.175E+00,2.163E+00,2.152E+00,2.140E+00,     &
       2.128E+00,2.116E+00,2.103E+00,2.091E+00,2.078E+00,2.064E+00,     &
       2.051E+00,2.037E+00,2.023E+00/                                   
!----------------------------------------------------------------------+-------
!
      IF (T .LE.   800.0) GOTO 10
      IF (T .GE. 10000.0) GOTO 20
!
      TT=T
      IT=TT/100.0
      FRACT=(TT-IT*100.0)/100.0
      IT=IT-7
      CAPCO=CAP(IT) + FRACT*(CAP(IT+1)-CAP(IT))
      CAPCO=CAPCO*XNCO
      RETURN
!
   10 CAPCO=CAP( 1)*XNCO
      RETURN
!
   20 CAPCO=CAP(93)*XNCO
      RETURN
!
      END FUNCTION CAPCO
!------------------*****-----------------------------------------------+-------


!----------------**********--------------------------------------------+-------
      SUBROUTINE SUB_CAPCO0(T,XNCO,CAPCO)
!----------------------------------------------------------------------+-------
!       CARBON MONOXIDE - PLANK MEAN OPACITY PER GRAM                   
!         21.11.83  D.O.M.(MODIFIED 8.11.83 D.O.M.)                     
!         NEW BANDPASS  (MODIFIED 3.6.85 D.O.M.)                        
!         F90 version 02-NOV-2005 (M.S.)
!                                                                       
!       INPUT :  TEMPERATURE                                            
!       OUTPUT:  CAPCO = AN APPROXIMATE OPACITY FOR CO.                 
!                FOR T = 800 TO 10000 K                                 
!----------------------------------------------------------------------+-------
      USE capco_data

      IMPLICIT NONE
!
!--- Dummy arguments:
      REAL,          INTENT(IN)     :: T, XNCO
      REAL,          INTENT(INOUT)  :: CAPCO
!
!--- Local Variables ---
      INTEGER                   :: IT
      REAL                      :: TT, FRACT
!
!----------------------------------------------------------------------+-------
!
      IF (T .LE.   800.0) GOTO 10
      IF (T .GE. 10000.0) GOTO 20
!
      TT=T
      IT=TT/100.0
      FRACT=(TT-IT*100.0)/100.0
      IT=IT-7
      CAPCO=CAPCO + (CAP(IT) + FRACT*(CAP(IT+1)-CAP(IT)))*XNCO
      RETURN
!
   10 CAPCO=CAPCO + CAP( 1)*XNCO
      RETURN
!
   20 CAPCO=CAPCO + CAP(93)*XNCO
      RETURN
!
      END SUBROUTINE SUB_CAPCO0
!--------------------**********----------------------------------------+-------

!----------------**********--------------------------------------------+-------
      SUBROUTINE SUB_CAPCO1(T,XNCO,CAPCO)
!----------------------------------------------------------------------+-------
!       CARBON MONOXIDE - PLANK MEAN OPACITY PER GRAM                   
!         21.11.83  D.O.M.(MODIFIED 8.11.83 D.O.M.)                     
!         NEW BANDPASS  (MODIFIED 3.6.85 D.O.M.)                        
!         F90 version 02-NOV-2005 (M.S.)
!                                                                       
!       INPUT :  TEMPERATURE                                            
!       OUTPUT:  CAPCO = AN APPROXIMATE OPACITY FOR CO.                 
!                FOR T = 800 TO 10000 K                                 
!----------------------------------------------------------------------+-------
      USE capco_data

      IMPLICIT NONE
!
!--- Dummy arguments:
      REAL, DIMENSION(:), INTENT(IN)     :: T, XNCO
      REAL, DIMENSION(:), INTENT(INOUT)  :: CAPCO
!
!--- Local Variables ---
      INTEGER, DIMENSION(SIZE(T))        :: IT
      REAL,    DIMENSION(SIZE(T))        :: FRACT, TT
!
!----------------------------------------------------------------------+-------
!
      TT = MIN(MAX(T,800.0),10000.0)
      IT=TT/100.0
      FRACT=(TT-IT*100.0)/100.0
      IT=IT-7
      CAPCO=CAPCO + (CAP(IT) + FRACT*(CAP(IT+1)-CAP(IT)))*XNCO
      RETURN
!
      END SUBROUTINE SUB_CAPCO1
!--------------------**********----------------------------------------+-------

!----------------**********--------------------------------------------+-------
      SUBROUTINE SUB_CAPCO2(T,XNCO,CAPCO)
!----------------------------------------------------------------------+-------
!       CARBON MONOXIDE - PLANK MEAN OPACITY PER GRAM                   
!         21.11.83  D.O.M.(MODIFIED 8.11.83 D.O.M.)                     
!         NEW BANDPASS  (MODIFIED 3.6.85 D.O.M.)                        
!         F90 version 02-NOV-2005 (M.S.)
!                                                                       
!       INPUT :  TEMPERATURE                                            
!       OUTPUT:  CAPCO = AN APPROXIMATE OPACITY FOR CO.                 
!                FOR T = 800 TO 10000 K                                 
!----------------------------------------------------------------------+-------
      USE capco_data

      IMPLICIT NONE
!
!--- Dummy arguments:
      REAL, DIMENSION(:,:), INTENT(IN)     :: T, XNCO
      REAL, DIMENSION(:,:), INTENT(INOUT)  :: CAPCO
!
!--- Local Variables ---
      INTEGER                       :: i2, n2
      INTEGER, DIMENSION(SIZE(T,1)) :: IT
      REAL,    DIMENSION(SIZE(T,1)) :: FRACT, TT
!
!----------------------------------------------------------------------+-------
!
      n2 = SIZE(T,2)
      DO i2=1,n2
        TT = MIN(MAX(T(:,i2),800.0),10000.0)
        IT=TT/100.0
        FRACT=(TT-IT*100.0)/100.0
        IT=IT-7
        CAPCO(:,i2)=CAPCO(:,i2) + &
                   (CAP(IT) + FRACT*(CAP(IT+1)-CAP(IT)))*XNCO(:,i2)
      ENDDO
      RETURN
!
      END SUBROUTINE SUB_CAPCO2
!--------------------**********----------------------------------------+-------

!----------------**********--------------------------------------------+-------
      SUBROUTINE SUB_CAPCO3(T,XNCO,CAPCO)
!----------------------------------------------------------------------+-------
!       CARBON MONOXIDE - PLANK MEAN OPACITY PER GRAM                   
!         21.11.83  D.O.M.(MODIFIED 8.11.83 D.O.M.)                     
!         NEW BANDPASS  (MODIFIED 3.6.85 D.O.M.)                        
!         F90 version 02-NOV-2005 (M.S.)
!                                                                       
!       INPUT :  TEMPERATURE                                            
!       OUTPUT:  CAPCO = AN APPROXIMATE OPACITY FOR CO.                 
!                FOR T = 800 TO 10000 K                                 
!----------------------------------------------------------------------+-------
      USE capco_data

      IMPLICIT NONE
!
!--- Dummy arguments:
      REAL, DIMENSION(:,:,:), INTENT(IN)     :: T, XNCO
      REAL, DIMENSION(:,:,:), INTENT(INOUT)  :: CAPCO
!
!--- Local Variables ---
      INTEGER                       :: i2, i3
      INTEGER, DIMENSION(SIZE(T,1)) :: IT
      REAL,    DIMENSION(SIZE(T,1)) :: FRACT, TT
!
!----------------------------------------------------------------------+-------
!
     !$OMP DO
      DO i3=1,SIZE(T,3)
      DO i2=1,SIZE(T,2)
          TT = MIN(MAX(T(:,i2,i3),800.0),10000.0)
          IT=TT/100.0
          FRACT=(TT-IT*100.0)/100.0
          IT=IT-7
          CAPCO(:,i2,i3)=CAPCO(:,i2,i3) + &
                        (CAP(IT) + FRACT*(CAP(IT+1)-CAP(IT)))*XNCO(:,i2,i3)
      ENDDO
      ENDDO
     !$OMP END DO 
      RETURN
!
      END SUBROUTINE SUB_CAPCO3
!--------------------**********----------------------------------------+-------

!-------------------***------------------------------------------------+-------
      REAL FUNCTION XCO(TIN,RHO)                                             
!----------------------------------------------------------------------+-------
!       LTE CARBON MONOXIDE FRACTION                                    
!              10.7.84    D.O.M.                                        
!              24.11.1998 (B.F., Copenhagen) Parameter definition with ..._q
!              01.11.2005  F90 version (M.S.)
!       INPUT :  TEMPERATURE AND MASS DENSITY                           
!       OUTPUT:  XCO  = FRACTION OF C ATOMS IN CO 
!                XCO is set to zero for T > 10000. K
!
!                XC = nC/nH = =4.0D-4 (adopted Carbon abundance)
!                XO = nO/nH = =8.0D-4 (adopted Oxygen abundance)
!                XOXC = XO*XC, XOPC = XO+XC
!----------------------------------------------------------------------+-------
      IMPLICIT NONE
!
!--- Dummy arguments:
      REAL,          INTENT(IN)     :: TIN, RHO
!
!--- Local Variables ---
      INTEGER                  :: IT
      INTEGER, PARAMETER       :: q=SELECTED_REAL_KIND(P=10,R=80)
      REAL(KIND=q)             :: D, FRACT, SAHACO, T, XNH, XNCO
!
      REAL(KIND=q), PARAMETER  :: HPERG=4.37D23
      REAL(KIND=q), PARAMETER  :: XC  =4.0D-4, XO  =8.0D-4, &
                                  XOXC=3.2D-7, XOPC=1.2D-3  
!      REAL(KIND=q)        :: XC  =4.0E-4_q, XO  =8.0E-4_q, &
!                             XOXC=3.2E-7_q, XOPC=1.2E-3_q  
      REAL(KIND=q), DIMENSION(100), SAVE :: SAHA                    
!
!   USE OF THE FOLLOWING TABLE HAS BEEN CHECKED TO YIELD XNCO WITHIN    
!   ABOUT 2% FOR TYPICAL SOLAR CHROMOSPHERE CONDITIONS.  21.11.83.      
      DATA SAHA/                                                        &
       0.0      ,0.0      ,0.0      ,0.0      ,0.0      ,1.090D-68,     &
       2.414D-55,2.366D-45,1.464D-37,2.508D-31,3.161D-26,5.625D-22,     &
       2.219D-18,2.681D-15,1.256D-12,2.725D-10,3.137D-08,2.130D-06,     &
       9.265D-05,2.762D-03,5.955D-02,9.701D-01,1.239D+01,1.279D+02,     &
       1.095D+03,7.943D+03,4.972D+04,2.729D+05,1.331D+06,5.839D+06,     &
       2.328D+07,8.507D+07,2.874D+08,9.033D+08,2.659D+09,7.369D+09,     &
       1.932D+10,4.815D+10,1.145D+11,2.606D+11,5.697D+11,1.200D+12,     &
       2.441D+12,4.807D+12,9.183D+12,1.706D+13,3.085D+13,5.444D+13,     &
       9.385D+13,1.583D+14,2.615D+14,4.239D+14,6.745D+14,1.055D+15,     &
       1.623D+15,2.459D+15,3.671D+15,5.406D+15,7.855D+15,1.127D+16,     &
       1.599D+16,2.242D+16,3.110D+16,4.270D+16,5.806D+16,7.822D+16,     &
       1.044D+17,1.382D+17,1.815D+17,2.365D+17,3.058D+17,3.926D+17,     &
       5.006D+17,6.341D+17,7.981D+17,9.986D+17,1.242D+18,1.536D+18,     &
       1.890D+18,2.313D+18,2.816D+18,3.413D+18,4.118D+18,4.945D+18,     &
       5.912D+18,7.040D+18,8.349D+18,9.863D+18,1.161D+19,1.361D+19,     &
       1.591D+19,1.852D+19,2.150D+19,2.488D+19,2.870D+19,3.301D+19,     &
       3.785D+19,4.329D+19,4.937D+19,5.616D+19/                         
!----------------------------------------------------------------------+-------
!                                                                       
      T = TIN                                                           
      IF (T .GT. 1.0D4) THEN                                             
        XCO = 0.0D0                                                    
        RETURN                                                          
      ENDIF                                                             
!                                                                       
      XNH = RHO * HPERG                                                 
      IT  = T/100.                                                      
      FRACT = (T - IT*100.0)/100.0                                      
      IF (IT .LT. 100) THEN                                               
        SAHACO = SAHA(IT) + FRACT*(SAHA(IT+1) - SAHA(IT))               
      ELSE                                                              
        SAHACO = SAHA(100)                                              
      ENDIF                                                             
      D = XOPC + SAHACO/XNH                                             
      IF (XOXC/D .LT. 1.0D-7) THEN                                      
        XNCO = XO / D                                                   
      ELSE                                                              
        XNCO = 0.5D0 * (D - DSQRT(D*D - 4.0D0*XOXC)) / XC               
      ENDIF                                                             
      XCO = XNCO                                                        
      RETURN                                                            
      END FUNCTION XCO
!------------------***-------------------------------------------------+-------

!----------------********----------------------------------------------+-------
      SUBROUTINE SUB_XCO0(TIN,RHO,XCO)
!----------------------------------------------------------------------+-------
!       LTE CARBON MONOXIDE FRACTION                                    
!              10.7.84    D.O.M.                                        
!              24.11.1998 (B.F., Copenhagen) Parameter definition with ..._q
!              01.11.2005  F90 version (M.S.)
!       INPUT :  TEMPERATURE AND MASS DENSITY                           
!       OUTPUT:  XCO  = FRACTION OF C ATOMS IN CO 
!                XCO is set to zero for T > 10000. K
!
!                XC = nC/nH = =4.0D-4 (adopted Carbon abundance)
!                XO = nO/nH = =8.0D-4 (adopted Oxygen abundance)
!                XOXC = XO*XC, XOPC = XO+XC
!----------------------------------------------------------------------+-------
      USE xco_data
!
      IMPLICIT NONE
!
!--- Dummy arguments:
      REAL,          INTENT(IN)     :: TIN, RHO
      REAL,          INTENT(OUT)    :: XCO
!
!--- Local Variables ---
      INTEGER             :: IT
      REAL(KIND=q)        :: D, FRACT, SAHACO, T, XNH, XNCO
!
!----------------------------------------------------------------------+-------
!                                                                       
      T = TIN                                                           
      IF (T .GT. 1.0D4) THEN                                             
        XCO = 0.0D0                                                    
        RETURN                                                          
      ENDIF                                                             
!                                                                       
      XNH = RHO * HPERG                                                 
      IT  = T/100.                                                      
      FRACT = (T - IT*100.0)/100.0                                      
      IF (IT .LT. 100) THEN                                               
        SAHACO = SAHA(IT) + FRACT*(SAHA(IT+1) - SAHA(IT))               
      ELSE                                                              
        SAHACO = SAHA(100)                                              
      ENDIF                                                             
      D = XOPC + SAHACO/XNH                                             
      IF (XOXC/D .LT. 1.0D-7) THEN                                      
        XNCO = XO / D                                                   
      ELSE                                                              
        XNCO = 0.5D0 * (D - DSQRT(D*D - 4.0D0*XOXC)) / XC               
      ENDIF                                                             
      XCO = XNCO                                                        
      RETURN                                                            
      END SUBROUTINE SUB_XCO0
!--------------------********------------------------------------------+-------

!----------------********----------------------------------------------+-------
      SUBROUTINE SUB_XCO1(TIN,RHO,XCO)
!----------------------------------------------------------------------+-------
!       LTE CARBON MONOXIDE FRACTION                                    
!              10.7.84    D.O.M.                                        
!              24.11.1998 (B.F., Copenhagen) Parameter definition with ..._q
!              01.11.2005  F90 version (M.S.)
!       INPUT :  TEMPERATURE AND MASS DENSITY                           
!       OUTPUT:  XCO  = FRACTION OF C ATOMS IN CO 
!                XCO is set to zero for T > 10000. K
!
!                XC = nC/nH = =4.0D-4 (adopted Carbon abundance)
!                XO = nO/nH = =8.0D-4 (adopted Oxygen abundance)
!                XOXC = XO*XC, XOPC = XO+XC
!----------------------------------------------------------------------+-------
      USE xco_data
!
      IMPLICIT NONE
!
!--- Dummy arguments:
      REAL, DIMENSION(:), INTENT(IN)     :: TIN, RHO
      REAL, DIMENSION(:), INTENT(OUT)    :: XCO
!
!--- Local Variables ---
      INTEGER,      DIMENSION(SIZE(TIN,1)) :: IT
      REAL(KIND=q), DIMENSION(SIZE(TIN,1)) :: D, FRACT, SAHACO, T, XNH, XNCO
!
!----------------------------------------------------------------------+-------
!                
      T      = TIN
      XNH    = RHO * HPERG
      IT     = MIN(INT(T/100.),100)
      FRACT  = (T - IT*100.0)/100.0                                      
      SAHACO = SAHA(IT) + FRACT*(SAHA(IT+1) - SAHA(IT))               
      D      = XOPC + SAHACO/XNH
      WHERE(D .GT. XOXC*1.0D+7)
        XNCO = XO / D 
      ELSEWHERE
        XNCO = 0.5D0 * (D - DSQRT(D*D - 4.0D0*XOXC)) / XC
      END WHERE
      WHERE (T .GT. 1.0D4) XNCO = 0.D0
      XCO    = XNCO
      RETURN                                                            
      END SUBROUTINE SUB_XCO1
!--------------------********------------------------------------------+-------

!----------------********----------------------------------------------+-------
      SUBROUTINE SUB_XCO2(TIN,RHO,XCO)
!----------------------------------------------------------------------+-------
!       LTE CARBON MONOXIDE FRACTION                                    
!              10.7.84    D.O.M.                                        
!              24.11.1998 (B.F., Copenhagen) Parameter definition with ..._q
!              01.11.2005  F90 version (M.S.)
!       INPUT :  TEMPERATURE AND MASS DENSITY                           
!       OUTPUT:  XCO  = FRACTION OF C ATOMS IN CO 
!                XCO is set to zero for T > 10000. K
!
!                XC = nC/nH = =4.0D-4 (adopted Carbon abundance)
!                XO = nO/nH = =8.0D-4 (adopted Oxygen abundance)
!                XOXC = XO*XC, XOPC = XO+XC
!----------------------------------------------------------------------+-------
      USE xco_data
!
      IMPLICIT NONE
!
!--- Dummy arguments:
      REAL, DIMENSION(:,:), INTENT(IN)     :: TIN, RHO
      REAL, DIMENSION(:,:), INTENT(OUT)    :: XCO
!
!--- Local Variables ---
      INTEGER                              :: i2, n2
      INTEGER,      DIMENSION(SIZE(TIN,1)) :: IT
      REAL(KIND=q), DIMENSION(SIZE(TIN,1)) :: D, FRACT, SAHACO, T, XNH, XNCO
!
!----------------------------------------------------------------------+-------
!                
      n2 = SIZE(TIN,2)
      DO i2=1,n2
        T      = TIN(:,i2)
        XNH    = RHO(:,i2) * HPERG
        IT     = MIN(INT(T/100.),100)
        FRACT  = (T - IT*100.0)/100.0                                      
        SAHACO = SAHA(IT) + FRACT*(SAHA(IT+1) - SAHA(IT))               
        D      = XOPC + SAHACO/XNH
        WHERE(D .GT. XOXC*1.0D+7)
          XNCO = XO / D 
        ELSEWHERE
          XNCO = 0.5D0 * (D - DSQRT(D*D - 4.0D0*XOXC)) / XC
        END WHERE
        WHERE (T .GT. 1.0D4) XNCO = 0.D0
        XCO(:,i2) = XNCO
      ENDDO
      RETURN                                                            
      END SUBROUTINE SUB_XCO2
!--------------------********------------------------------------------+-------

!----------------********----------------------------------------------+-------
      SUBROUTINE SUB_XCO3(TIN,RHO,XCO)
!----------------------------------------------------------------------+-------
!       LTE CARBON MONOXIDE FRACTION                                    
!              10.7.84    D.O.M.                                        
!              24.11.1998 (B.F., Copenhagen) Parameter definition with ..._q
!              01.11.2005  F90 version (M.S.)
!       INPUT :  TEMPERATURE AND MASS DENSITY                           
!       OUTPUT:  XCO  = FRACTION OF C ATOMS IN CO 
!                XCO is set to zero for T > 10000. K
!
!                XC = nC/nH = =4.0D-4 (adopted Carbon abundance)
!                XO = nO/nH = =8.0D-4 (adopted Oxygen abundance)
!                XOXC = XO*XC, XOPC = XO+XC
!----------------------------------------------------------------------+-------
      USE xco_data
!
      IMPLICIT NONE
!
!--- Dummy arguments:
      REAL, DIMENSION(:,:,:), INTENT(IN)   :: TIN, RHO
      REAL, DIMENSION(:,:,:), INTENT(OUT)  :: XCO
!
!--- Local Variables ---
      INTEGER                              :: i1, i2, i3
      INTEGER,      DIMENSION(SIZE(TIN,1)) :: IT
      REAL(KIND=q), DIMENSION(SIZE(TIN,1)) :: D, FRACT, SAHACO, T, XNH, XNCO
!
!----------------------------------------------------------------------+-------
!                
     !$OMP DO
      DO i3=1,SIZE(TIN,3)
      DO i2=1,SIZE(TIN,2)
          T      = TIN(:,i2,i3)
          XNH    = RHO(:,i2,i3) * HPERG
          IT     = MIN(INT(T/100.),100)
          FRACT  = (T - IT*100.0)/100.0                                      
          SAHACO = SAHA(IT) + FRACT*(SAHA(IT+1) - SAHA(IT))               
          D      = XOPC + SAHACO/XNH
          WHERE(D .GT. XOXC*1.0D+7)
            XNCO = XO / D 
          ELSEWHERE
            XNCO = 0.5D0 * (D - DSQRT(D*D - 4.0D0*XOXC)) / XC
          END WHERE
          WHERE (T .GT. 1.0D4) XNCO = 0.D0
          XCO(:,i2,i3) = XNCO
      ENDDO
      ENDDO
     !$OMP END DO
      RETURN                                                            
      END SUBROUTINE SUB_XCO3
!--------------------********------------------------------------------+-------

!-------------------*****----------------------------------------------+-------
      REAL FUNCTION BNUWT(T)                                                 
!----------------------------------------------------------------------+-------
!   BANDPASS WEIGHTING FUNCTION.  3.12.83   D.O.M.                      
!     FOR IR: LAMBDA = 4 MICRON AND UP.                                 
!   BANDPASS MODIFIED  3.6.85  D.O.M.                                   
!     NOW LAMBDA = 4.29 TO 6.2 MICRON.                                  
!       I = 1 :  FOR T =  750 TO  3000 K.                               
!       I = 2 :  FOR T = 3000 TO 11000 K.                               
!       02.11.2005: F90 version (M.S.)
!----------------------------------------------------------------------+-------
      IMPLICIT NONE
!
!--- Dummy arguments:
      REAL, INTENT(IN)    :: T
!
!--- Local Variables ---
      REAL                      :: FWT1, FWT2, TINV, W
      REAL, DIMENSION(2), SAVE  :: CW1, CW2, CW3, CW4                          
      DATA CW1,CW2,CW3,CW4 /-1.06718D-2,1.51828D-3,-3.51202D1, &
         -3.64468D1,5.45520D5,3.13299D5,-2.70772D8,1.31574D7/           
!                                                                       
      TINV = 1.0D0/T                                                    
      IF (T .LT. 2.9D3)  THEN                                           
        BNUWT =((CW4(1)*TINV + CW3(1))*TINV +  CW2(1))*TINV + CW1(1)
      ELSEIF (T .LT. 3.1D3)  THEN                                       
        W = (3.1D3-T)/2.0D2                                             
        FWT1 = ((CW4(1)*TINV + CW3(1))*TINV +  CW2(1))*TINV + CW1(1)
        FWT2 = ((CW4(2)*TINV + CW3(2))*TINV +  CW2(2))*TINV + CW1(2)
        BNUWT = W * FWT1 + (1.0D0-W) * FWT2               
      ELSE                                                              
        BNUWT =((CW4(2)*TINV + CW3(2))*TINV +  CW2(2))*TINV + CW1(2)    
      ENDIF                                                             
      RETURN                                                            
      END FUNCTION BNUWT
!------------------*****-----------------------------------------------+-------
!
!----------------**********--------------------------------------------+-------
      SUBROUTINE SUB_BNUWT0(T,BNUWT)     
!----------------------------------------------------------------------+-------
!   BANDPASS WEIGHTING FUNCTION.  3.12.83   D.O.M.                      
!     FOR IR: LAMBDA = 4 MICRON AND UP.                                 
!   BANDPASS MODIFIED  3.6.85  D.O.M.                                   
!     NOW LAMBDA = 4.29 TO 6.2 MICRON.                                  
!       I = 1 :  FOR T =  750 TO  3000 K.                               
!       I = 2 :  FOR T = 3000 TO 11000 K.                               
!       02.11.2005: F90 version (M.S.)
!----------------------------------------------------------------------+-------
      USE bnuwt_data

      IMPLICIT NONE
!
!--- Dummy arguments:
      REAL, INTENT(IN)    :: T
      REAL, INTENT(OUT)   :: BNUWT
!
!--- Local Variables ---
      REAL                      :: FWT1, FWT2, TINV, W
!                                                                       
      TINV = 1.0D0/T                                                    
      IF (T .LT. 2.9D3)  THEN                                           
        BNUWT =((CW4(1)*TINV + CW3(1))*TINV +  CW2(1))*TINV + CW1(1)
      ELSEIF (T .LT. 3.1D3)  THEN                                       
        W = (3.1D3-T)/2.0D2                                             
        FWT1 = ((CW4(1)*TINV + CW3(1))*TINV +  CW2(1))*TINV + CW1(1)
        FWT2 = ((CW4(2)*TINV + CW3(2))*TINV +  CW2(2))*TINV + CW1(2)
        BNUWT = W * FWT1 + (1.0D0-W) * FWT2               
      ELSE                                                              
        BNUWT =((CW4(2)*TINV + CW3(2))*TINV +  CW2(2))*TINV + CW1(2)    
      ENDIF                                                             
      RETURN                                                            
      END SUBROUTINE SUB_BNUWT0
!--------------------**********----------------------------------------+-------

!----------------**********--------------------------------------------+-------
      SUBROUTINE SUB_BNUWT1(T,BNUWT)     
!----------------------------------------------------------------------+-------
!   BANDPASS WEIGHTING FUNCTION.  3.12.83   D.O.M.                      
!     FOR IR: LAMBDA = 4 MICRON AND UP.                                 
!   BANDPASS MODIFIED  3.6.85  D.O.M.                                   
!     NOW LAMBDA = 4.29 TO 6.2 MICRON.                                  
!       I = 1 :  FOR T =  750 TO  3000 K.                               
!       I = 2 :  FOR T = 3000 TO 11000 K.                               
!       02.11.2005: F90 version (M.S.)
!----------------------------------------------------------------------+-------
      USE bnuwt_data

      IMPLICIT NONE
!
!--- Dummy arguments:
      REAL, DIMENSION(:), INTENT(IN)    :: T
      REAL, DIMENSION(:), INTENT(OUT)   :: BNUWT
!
!--- Local Variables ---
      REAL, DIMENSION(SIZE(T)) :: FWT1, FWT2, TINV, W
!                                                                       
      TINV  = 1.0D0/T                                                    
      W     = MIN(MAX(0.D0,(3.1D3-T)/2.0D2),1.D0)
      FWT1  = ((CW4(1)*TINV + CW3(1))*TINV +  CW2(1))*TINV + CW1(1)
      FWT2  = ((CW4(2)*TINV + CW3(2))*TINV +  CW2(2))*TINV + CW1(2)
      BNUWT = W * FWT1 + (1.0D0-W) * FWT2               
!
      RETURN                                                            
      END SUBROUTINE SUB_BNUWT1
!--------------------**********----------------------------------------+-------

!----------------**********--------------------------------------------+-------
      SUBROUTINE SUB_BNUWT2(T,BNUWT)     
!----------------------------------------------------------------------+-------
!   BANDPASS WEIGHTING FUNCTION.  3.12.83   D.O.M.                      
!     FOR IR: LAMBDA = 4 MICRON AND UP.                                 
!   BANDPASS MODIFIED  3.6.85  D.O.M.                                   
!     NOW LAMBDA = 4.29 TO 6.2 MICRON.                                  
!       I = 1 :  FOR T =  750 TO  3000 K.                               
!       I = 2 :  FOR T = 3000 TO 11000 K.                               
!       02.11.2005: F90 version (M.S.)
!----------------------------------------------------------------------+-------
      USE bnuwt_data

      IMPLICIT NONE
!
!--- Dummy arguments:
      REAL, DIMENSION(:,:), INTENT(IN)    :: T
      REAL, DIMENSION(:,:), INTENT(OUT)   :: BNUWT
!
!--- Local Variables ---
      INTEGER                    :: i2, n2
      REAL, DIMENSION(SIZE(T,1)) :: FWT1, FWT2, TINV, W
!                                                                       
      n2 = SIZE(T,2)
      DO i2=1,n2
        TINV  = 1.0D0/T(:,i2)
        W     = MIN(MAX(0.D0,(3.1D3-T(:,i2))/2.0D2),1.D0)
        FWT1  = ((CW4(1)*TINV + CW3(1))*TINV +  CW2(1))*TINV + CW1(1)
        FWT2  = ((CW4(2)*TINV + CW3(2))*TINV +  CW2(2))*TINV + CW1(2)
        BNUWT(:,i2) = W * FWT1 + (1.0D0-W) * FWT2               
     ENDDO
!
      RETURN                                                            
      END SUBROUTINE SUB_BNUWT2
!--------------------**********----------------------------------------+-------
!
!----------------**********--------------------------------------------+-------
      SUBROUTINE SUB_BNUWT3(T,BNUWT)     
!----------------------------------------------------------------------+-------
!   BANDPASS WEIGHTING FUNCTION.  3.12.83   D.O.M.                      
!     FOR IR: LAMBDA = 4 MICRON AND UP.                                 
!   BANDPASS MODIFIED  3.6.85  D.O.M.                                   
!     NOW LAMBDA = 4.29 TO 6.2 MICRON.                                  
!       I = 1 :  FOR T =  750 TO  3000 K.                               
!       I = 2 :  FOR T = 3000 TO 11000 K.                               
!       02.11.2005: F90 version (M.S.)
!----------------------------------------------------------------------+-------
      USE bnuwt_data

      IMPLICIT NONE
!
!--- Dummy arguments:
      REAL, DIMENSION(:,:,:), INTENT(IN)    :: T
      REAL, DIMENSION(:,:,:), INTENT(OUT)   :: BNUWT
!
!--- Local Variables ---
      INTEGER                    :: i2, i3
      REAL, DIMENSION(SIZE(T,1)) :: FWT1, FWT2, TINV, W
!                                                                       
     !$OMP DO
      DO i3=1,SIZE(T,3)
      DO i2=1,SIZE(T,2)
          TINV  = 1.0D0/T(:,i2,i3)
          W     = MIN(MAX(0.D0,(3.1D3-T(:,i2,i3))/2.0D2),1.D0)
          FWT1  = ((CW4(1)*TINV + CW3(1))*TINV +  CW2(1))*TINV + CW1(1)
          FWT2  = ((CW4(2)*TINV + CW3(2))*TINV +  CW2(2))*TINV + CW1(2)
          BNUWT(:,i2,i3) = W * FWT1 + (1.0D0-W) * FWT2
      ENDDO
      ENDDO
     !$OMP END DO 
!
      RETURN                                                            
      END SUBROUTINE SUB_BNUWT3
!--------------------**********----------------------------------------+-------

      END MODULE co_module
!----------------*********---------------------------------------------+-------


!------------*************---------------------------------------------+-------
!      MODULE op_sf0_module
!
!      CONTAINS
!
!!-------------*****----------------------------------------------------+-------
!   SUBROUTINE op_s0(T, Plog, rho, ifdust, m1,n1, m2,n2, m3,n3, nquc, &
!                    opclam, quc)
!
!! --- This routine computes the Rosseland mean opacity in opclam.
!! --- This routine must be called in a scalar environment ---
!!
!  USE opta_module
!  USE op_dust_module
!!
!  IMPLICIT NONE
!!
!!--- Dummy arguments:
!!
!   INTEGER, INTENT(IN)    :: ifdust, m1,n1, m2,n2, m3,n3, nquc
!
!   REAL, DIMENSION(m1:n1,m2:n2,m3:n3), INTENT(OUT)   :: opclam
!   REAL, DIMENSION(m1:n1,m2:n2,m3:n3), INTENT(IN)    :: T, Plog, rho
!   REAL, DIMENSION(m1:n1,m2:n2,m3:n3,1:nquc), &
!         OPTIONAL,                     INTENT(IN)    :: quc
!!
!! --- Compute grey gas opacity
!!-----------------------------------------------------------------------
!!$OMP PARALLEL DEFAULT(NONE) &
!!$OMP SHARED(rho, Plog, T, OPCLAM)
!!-----------------------------------------------------------------------
!     call sub_xkaros_logPT2rhokap(Plog,T,rho,0,opclam)
!!-----------------------------------------------------------------------
!!$OMP END PARALLEL
!! --- add dust opacity to opclam ---
!!
!   IF (ifdust == 1) THEN
!      CALL op_dust(T, Plog, rho, quc, 0, m1,n1, m2,n2, m3,n3, nquc, opclam)
!   ENDIF
!!
!!-----------------------------------------------------------------------
!   END SUBROUTINE op_s0
!!-----------------*****------------------------------------------------+-------
!
!!-------------*****----------------------------------------------------+-------
!   SUBROUTINE op_s1(Tinci, Tsurf, Tbotm, T, Plog, rho, gfac, &
!                    nu, ifco, ifdust, m1,n1, m2,n2, m3,n3, nquc, &
!                    blinci, blsurf, dipnwt, blclam, opclam, trad3, quc)
!
!! --- This routine computes blinci, blsurf, dipnwt, opclam and blclam 
!!     for the frequency band defined by the frequency index nu,
!!     using the scheme requested by flag ifco.
!! --- This routine must be called in a scalar environment ---
!!
!  USE opta_module
!  USE op_dust_module
!  USE opta_chem
!  USE   co_module
!!
!  IMPLICIT NONE
!!
!!--- Dummy arguments:
!!
!   INTEGER, INTENT(IN)    :: nu, ifco, ifdust, m1,n1, m2,n2, m3,n3, nquc
!
!   REAL,                               INTENT(IN)    :: Tinci, Tsurf, Tbotm
!   REAL,                               INTENT(OUT)   :: blinci, blsurf, dipnwt
!   REAL, DIMENSION(            m3:n3), INTENT(IN)    :: gfac
!   REAL, DIMENSION(m1:n1,m2:n2,m3:n3), INTENT(IN)    :: T, Plog, rho
!   REAL, DIMENSION(m1:n1,m2:n2,m3:n3), INTENT(OUT)   :: blclam, opclam, trad3
!   REAL, DIMENSION(m1:n1,m2:n2,m3:n3,1:nquc), &
!         OPTIONAL,                     INTENT(IN)    :: quc
!!
!!
!!--- Local scalar variables:
!!
!   REAL, PARAMETER :: PI=3.1415926536, SIGMA=5.6705E-5, C5=SIGMA/PI
!   INTEGER         :: i2, i3, iquc
!   REAL            :: dum
!   REAL, DIMENSION(:,:,:), ALLOCATABLE :: addata, wTdbdT
!!
!   blinci = 0.0
!!
!   if (NU == 0) then
!! --- Compute grey opacity and source function ---
!! --- Compute blinci, blsurf and dipnwt ---
!     blsurf = C5*Tsurf*Tsurf*Tsurf*Tsurf
!     dipnwT = 1.0
!     IF (Tinci > 0.0) THEN
!       blinci = C5*Tinci*Tinci*Tinci*Tinci
!     ENDIF
!!-----------------------------------------------------------------------
!!$OMP PARALLEL DEFAULT(NONE) &
!!$OMP SHARED(m2,n2, m3,n3, nu, gfac, rho, Plog, T, &
!!$OMP        BLCLAM, OPCLAM, TRAD3)
!!-----------------------------------------------------------------------
!     call sub_xkaros_logPT2rhokap(Plog,T,rho,NU,opclam)
!     !$OMP DO PRIVATE(i2)
!       do i3=m3,n3
!         do i2=m2,n2
!           trad3 (:,i2,i3)=opclam(:,i2,i3) / &
!                           (1.0D0 + gfac(i3)*opclam(:,i2,i3)**2)
!           blclam(:,i2,i3)=C5*T(:,i2,i3)**4
!         end do ! i2
!       end do ! i3
!     !$OMP END DO
!!-----------------------------------------------------------------------
!!$OMP END PARALLEL
!!-----------------------------------------------------------------------
!!
!   elseif (NU > 0 .and. ifco == 0) then
!! --- Compute multi-band opacities and source function ---
!! --- Compute blinci, blsurf and dipnwt ---
!     call xBnuwT(Tsurf, nu, blsurf, dum)
!     call xBnuwT(Tbotm, nu, dum, dipnwt)
!     blsurf = C5*Tsurf*Tsurf*Tsurf*Tsurf*blsurf
!     IF (Tinci > 0.0) THEN
!       call xBnuwT(Tinci, nu, blinci, dum)
!       blinci = C5*Tinci*Tinci*Tinci*Tinci*blinci
!     ENDIF
!!
!     ALLOCATE(wTdbdT(m1:n1,m2:n2,m3:n3))
!!
!     IF (ifdust == 0) THEN
!!-----------------------------------------------------------------------
!!$OMP PARALLEL DEFAULT(NONE) &
!!$OMP SHARED(m2,n2, m3,n3, nu, gfac, rho, Plog, T, &
!!$OMP        BLCLAM, OPCLAM, TRAD3, wTdbdT)
!!-----------------------------------------------------------------------
!       call sub_xkaros_logPT2rhokap(Plog,T,rho,NU,opclam)
!       call xBnuwT(T, NU, blclam, wTdbdT)
!       !$OMP DO PRIVATE(i2)
!         do i3=m3,n3
!           do i2=m2,n2
!             trad3 (:,i2,i3)=trad3 (:,i2,i3) + &
!                             wTdbdT(:,i2,i3)*opclam(:,i2,i3) / &
!                             (1.0D0 + gfac(i3)*opclam(:,i2,i3)**2)
!             blclam(:,i2,i3)=C5*blclam(:,i2,i3)*T(:,i2,i3)**4
!           end do ! i2
!         end do ! i3
!       !$OMP END DO
!!-----------------------------------------------------------------------
!!$OMP END PARALLEL
!!-----------------------------------------------------------------------
!     ELSE
!!-----------------------------------------------------------------------
!!$OMP PARALLEL DEFAULT(NONE) &
!!$OMP SHARED(nu, rho, Plog, T, OPCLAM)
!!-----------------------------------------------------------------------
!       call sub_xkaros_logPT2rhokap(Plog, T, rho, NU, opclam)
!!-----------------------------------------------------------------------
!!$OMP END PARALLEL
!!-----------------------------------------------------------------------
!! --- add dust opacity to opclam ---
!       CALL op_dust(T, Plog, rho, quc, nu, m1,n1, m2,n2, m3,n3, nquc, opclam)
!!-----------------------------------------------------------------------
!!$OMP PARALLEL DEFAULT(NONE) &
!!$OMP SHARED(m2,n2, m3,n3, nu, gfac, rho, T, &
!!$OMP        BLCLAM, OPCLAM, TRAD3, wTdbdT)
!!-----------------------------------------------------------------------
!       call xBnuwT(T, NU, blclam, wTdbdT)
!       !$OMP DO PRIVATE(i2)
!         do i3=m3,n3
!           do i2=m2,n2
!             trad3 (:,i2,i3)=trad3 (:,i2,i3) + &
!                             wTdbdT(:,i2,i3)*opclam(:,i2,i3) / &
!                             (1.0D0 + gfac(i3)*opclam(:,i2,i3)**2)
!             blclam(:,i2,i3)=C5*blclam(:,i2,i3)*T(:,i2,i3)**4
!           end do ! i2
!         end do ! i3
!       !$OMP END DO
!!-----------------------------------------------------------------------
!!$OMP END PARALLEL
!!-----------------------------------------------------------------------
!     ENDIF
!!
!     DEALLOCATE(wTdbdT)
!!
!   elseif (NU == 1 .and. ifco > 0) then
!! --- Compute continuum band in CO 2-band scheme ---
!! --- Compute blinci, blsurf and dipnwt ---
!     call sub_bnuwt(Tsurf,blsurf)
!     blsurf = C5*Tsurf*Tsurf*Tsurf*Tsurf*(1.0-blsurf)
!     dipnwT = 1.0
!     IF (Tinci > 0.0) THEN
!       call sub_bnuwt(Tinci,blinci)
!       blinci = C5*Tinci*Tinci*Tinci*Tinci*(1.0-blinci)
!     ENDIF
!!-----------------------------------------------------------------------
!!$OMP PARALLEL DEFAULT(NONE) &
!!$OMP SHARED(m2,n2, m3,n3, gfac, rho, Plog, T, &
!!$OMP        BLCLAM, OPCLAM, TRAD3)
!!-----------------------------------------------------------------------
!! --- Store kappa_Ross in opclam
!     call sub_bnuwt(T,blclam)
!     call sub_xkaros_logPT2rhokap(Plog,T,rho,0,opclam)
!!
!     !$OMP DO PRIVATE(i2)
!       do i3=m3,n3
!         do i2=m2,n2
!           trad3 (:,i2,i3)=opclam(:,i2,i3) / &
!                           (1.0D0 + gfac(i3)*opclam(:,i2,i3)**2)
!           blclam(:,i2,i3)=C5*(1.0-blclam(:,i2,i3))*T(:,i2,i3)**4
!         end do ! i2
!       end do ! i3
!     !$OMP END DO
!!
!!-----------------------------------------------------------------------
!!$OMP END PARALLEL
!!-----------------------------------------------------------------------
!!
!   elseif (NU == 2 .and. ifco > 0) then
!! --- Compute CO band in CO 2-band scheme ---
!! --- Compute blinci, blsurf and dipnwt ---
!     call sub_bnuwt(Tsurf,blsurf)
!     blsurf = C5*Tsurf*Tsurf*Tsurf*Tsurf*blsurf
!     dipnwT = 0.0
!     IF (Tinci > 0.0) THEN
!       call sub_bnuwt(Tinci,blinci)
!       blinci = C5*Tinci*Tinci*Tinci*Tinci*blinci
!     ENDIF
!!
!     ALLOCATE(addata(m1:n1,m2:n2,m3:n3))
!!
!     if (ifco == 1) THEN
!!-----------------------------------------------------------------------
!!$OMP PARALLEL DEFAULT(NONE) &
!!$OMP SHARED(rho, T, addata)
!!-----------------------------------------------------------------------
!! --- Equilibrium CO formation: ---
!!     XCO = n(CO)/n(C) is stored in addata:
!       call sub_xco(T,rho,addata)
!!-----------------------------------------------------------------------
!!$OMP END PARALLEL
!!-----------------------------------------------------------------------
!     endif
!     if (ifco == 2) THEN
!! --- Non-equilibrium CO formation: ---
!      ! --- use number densities as result from chemistry routine ---
!      !
!!$OMP PARALLEL DEFAULT(NONE) &
!!$OMP SHARED(m2,n2, m3,n3,ispn,ispco,ispc,nspcmult,quc,addata)
!      !
!      ! --- Reset addata array  ---
!      !$OMP DO PRIVATE(i2)
!        do i3=m3,n3
!          do i2=m2,n2
!            addata(:,i2,i3)=0.0
!          end do ! i2
!        end do ! i3
!      !$OMP END DO
!      !
!       if (ispn.ge.1) then 
!      !$OMP DO PRIVATE(iquc,i2)
!         do i3=m3,n3
!           do iquc=1,ispn
!             do i2=m2,n2
!               addata(:,i2,i3)=addata(:,i2,i3) + nspcmult(iquc) * &
!                                  quc(:,i2,i3,ispc(iquc))
!             end do ! i2
!           end do ! iquc
!         end do ! i3
!      !$OMP END DO
!      ! --- Compute XCO = n(CO)/n(C) and store in addata array ---
!      !$OMP DO PRIVATE(i2)
!         do i3=m3,n3
!           do i2=m2,n2
!             addata(:,i2,i3)= quc(:,i2,i3,ispco)/addata(:,i2,i3)
!           end do ! i2
!         end do ! i3
!      !$OMP END DO
!       end if
!      !
!!-----------------------------------------------------------------------
!!$OMP END PARALLEL
!!-----------------------------------------------------------------------
!     endif
!!
!!-----------------------------------------------------------------------
!!$OMP PARALLEL DEFAULT(NONE) &
!!$OMP SHARED(m2,n2, m3,n3, rho, T, addata, BLCLAM, OPCLAM)
!!-----------------------------------------------------------------------
!  !$OMP DO PRIVATE(i2)
!     do i3=m3,n3
!       do i2=m2,n2
!           blclam(:,i2,i3)=  C5*T(:,i2,i3)**4 - blclam(:,i2,i3)
!           addata(:,i2,i3)=   rho(:,i2,i3)    * addata(:,i2,i3)
!       end do ! i2
!     end do ! i3
!  !$OMP END DO
!! --- Now rho*XCO is saved in addata
!!
!! --- Call of sub_capco computes SUM of kappa_Ross (= old opclam) + kappa_CO
!     call sub_capco(T,addata,opclam)
!!-----------------------------------------------------------------------
!!$OMP END PARALLEL
!!-----------------------------------------------------------------------
!!
!     DEALLOCATE(addata)
!!
!   else
!      write(*,*) "!!! Error in op_s1 !!!"
!      write(*,*) "NU=",NU,", IFCO=",IFCO
!      write(*,*) "Aborting program"
!      stop
!   endif
!!
!   END SUBROUTINE op_s1
!!-----------------*****------------------------------------------------+-------
!
!      END MODULE op_sf0_module
!!----------------*************-----------------------------------------+-------
