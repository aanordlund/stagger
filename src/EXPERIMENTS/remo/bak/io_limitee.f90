! $Id: io_limitee.f90,v 1.1 2016/10/12 11:16:20 remo Exp $
!***********************************************************************
MODULE io
  integer, parameter:: lcache=32
  integer, allocatable, dimension(:,:,:) :: lmax, lmin
  real, allocatable, dimension(:,:) ::  fmax, fmin
  real(kind=4), save:: wct(0:2), tstart
  character(len=120):: check_txt(100)
  integer, save:: i_check=0
END MODULE io

!***********************************************************************
SUBROUTINE init_io
  USE params
  USE io
  implicit none
  real wallclock
  character(len=mid):: id="$Id: io_limitee.f90,v 1.1 2016/10/12 11:16:20 remo Exp $"
  character(len=mfile) name, fname, tmp
  logical exists

  call print_id (id)
  mvar = 0                                              ! count I/O variables
  if (do_density   ) mvar = mvar+1
  if (do_momenta   ) mvar = mvar+3
  if (do_energy    ) mvar = mvar+1
  if (do_ionization) mvar = mvar+1
  if (do_pscalar   ) mvar = mvar+1
  if (do_mhd       ) mvar = mvar+3
  allocate (lmax(lcache,2,mz), fmax(lcache,mz))
  allocate (lmin(lcache,2,mz), fmin(lcache,mz))
  if (master) print *,'init_io: done'
  wct(0) = wallclock()
  wct(1) = 0.

  if (master) then
    open (stat_unit,file=name('stat.txt','stat',file),status='unknown')
    do while (.true.)
      read (stat_unit,'(a)',end=1) tmp
    end do
    1 backspace (stat_unit)
  end if
END

!***********************************************************************
SUBROUTINE init_stdio
  USE params
  implicit none
  logical fexists
  character(len=mfile) input_file, arg_file

  stdout = 6
  stderr = 6

!-------------------------------------------------------------------------------
!  Must read parameters from file, so all nodes can read
!-------------------------------------------------------------------------------
  stdin = stdin_unit
  inputfile = input_file()                                              ! 1st option: EXPERIMENTS/$(EXPERIMENT)/input.txt
  if (fexists('input.txt')) inputfile = 'input.txt'                     ! 2nd option: input.txt
  call getarg(1,arg_file)
  if (arg_file .ne. ' ') inputfile = arg_file                           ! 3rd option: argument
  if (master) print *,'inputfile = ',inputfile
  open (stdin, file=inputfile, status='old')
END

!***********************************************************************
SUBROUTINE rewind_stdin
  USE params
  rewind (stdin)
  if (master) print *,'************************* STDIN WAS REWOUND ****************************'
END

!***********************************************************************
SUBROUTINE read_time (jsnap)
!
!  Read time info
!
  USE params
  implicit none
  logical fexists
  integer jsnap, lrec, seedvoid, iostat, i, rec, jread
  real tvoid, dtvoid
  character(len=mfile):: name, fname, void
!-------------------------------------------------------

  call split_name(file)                                                 ! define head and tail of filename
  if (do_trace) print *,'read_time: head,tail=',head,tail

  if (iread == -2 .or. iwrite == -2) then                               ! == -2 signals get isnap from ..
    fname = name('time.txt','time',file)                                ! .. time.txt or filename.time
    if (fexists(fname)) then
      if (master) print*,'read_time: found ',fname
      open (time_unit,file=fname, &
        status='old', form='formatted')
      read (time_unit,*) void                                           ! skip header line
      read (time_unit,*) jread                                          ! last snap shot written
      close (time_unit)
    else                                                                ! if no time.txt act as with -1
      if (master) print*,"read_time: didn't find ",trim(fname),' tail=',trim(tail)
      jread = iread
    end if
    if (iread == -2) iread = jread                                      ! iread == -2 => read from last snap
    jsnap = jread                                                       ! iwrite == -2 => write to next available
    return
  end if

  fname = name('time.dat','tim',file)
  if (fexists(fname)) then                                              ! if exists
    open (time_unit, file=fname, &                                      ! open time info file
       status='old', form='unformatted', &                              ! old, unformatted
       access='direct', recl=lrec(4))                                   ! direct access
    rec = iread+1
    iostat = 0
    do while (iostat .eq. 0)
      read (time_unit,rec=rec,iostat=iostat) &
        tvoid, dtvoid, seedvoid, jsnap                                  ! previous isnap from write_data
      rec = rec+1
    end do
    close(time_unit)
    if (master) print*,'read_time: found ',trim(fname),' rec =',rec
    if (rec .eq. iread+1) jsnap = -1                                    ! empty time.dat or one short record
  else
    if (master) print*,"read_time: didn't find ",fname
    jsnap = -1
  end if
  i = index(file,' ')
  if (master) write(*,*) 'default next snap shot =', jsnap+1, file(1:i)
END

!***********************************************************************
SUBROUTINE read_snap (r,px,py,pz,e,d,Bx,By,Bz)
!
!  Read snap shot
!
  USE params
  USE arrays, only: scr1
  implicit none
  logical fexists
  integer iv, i, lrec, isnapold, iy, iz, recl, io_type
  real, dimension(mx,my,mz) :: &
        r,px,py,pz,e,d,Bx,By,Bz
  real told, wt, wallclock, divb_rms, divb_max
  character(len=mfile):: from_name, name, fname
!-------------------------------------------------------

  fname=from_name('time.dat','tim',from)
  i = index(from,' ')-1                                                 ! necessary on Windows
  if (from(1:i) == 'scratch.dat') fname='time.dat'                      ! for scratch restarts
  if (fexists(fname)) then                                              ! if exists
    open (time_unit, file=fname, &                                      ! open time info file
       status='old', form='unformatted', &                              ! old, unformatted
       access='direct', recl=lrec(4))                                   ! direct access
    read (time_unit,rec=iread+1) told, dtold, &
       iseedold, isnapold                                               ! time and seed for restart
    close(time_unit)
    if (master) print*,'read_snap: found ',trim(fname),' t,told =',t,told
    if (t  .eq. -1.) t  = told                                          ! set recovered time if not set in namelist
    if (dt .eq. dtdef) dt = dtold                                       ! set recovered dt if not set in namelist
    if (master) &
      print '(1x,a,i5,2g12.5,i12,2x,a)', &
         'recovered time info:', iread, t, dt, iseedold, trim(fname)    ! show time info
  else
    if (master) print*,"read_snap: didn't find ",trim(fname)
    if (t .eq. -1.) t = 0.                                              ! reset if not set in namelist
  end if

  if (tsnap .gt. 0.) then                                               ! snapshot every tsnap time interval
    isnap0 = t/tsnap - isnap +1                                         ! offset, in case tsnap differs
  end if
  if (tscr  .gt. 0) then                                                ! scratch file every tscr time interval
    iscr0  = t/tscr  - iscr + 1                                         ! offset, in case tscr differs
  end if

  wt = wallclock()

  if (do_scp .and. &
     (.not. fexists(from)) .and. &
     (tail .ne. 'scratch.dat')) call scp_name(from,iread)               ! from_00ttt.dat

  if (mpi_size == 1) then                                               ! OpenMP
    if (mx*my < 512**2) then
      io_type   = 0                                                     ! read local volume
      recl = lrec(mx*my*mz)
    else
      io_type   = 1                                                     ! read xy-planes
      recl = lrec(mx*my)
    end if
  else                                                                  ! MPI
    if (fexists(trim(from))) then                                       ! try from_00ttt.dat
      io_type   = 2                                                     ! read x-lines
      recl = lrec(mpi_nx*mx*mpi_ny*my)
    else
      call mpi_name(from)                                               ! try from_00ttt-00ccc.dat
      io_type   = 1                                                     ! read xy-planes
      recl = lrec(mx*my)
    end if
  end if

  if (io_type < 2 .or. mpi_rank==io_rank) then                          ! all ranks for io_type < 1
    print*,'opening '//trim(from)//' with recl =',recl                  ! print for io_rank
    open (unit=data_unit, file=trim(from), &                            ! open the file
          access='direct', status='old', recl=recl)                     ! old, direct
  end if

  iv = 0                                                                ! variable number
  if (do_density) then
    call read_chunk (r,io_type,iv,iread,data_unit)
    if (do_init_smooth) then
      !$omp parallel
      call smooth3t_set(r,scr1)
      r(:,2:my-1,izs:ize) = scr1(:,2:my-1,izs:ize)
      !$omp end parallel
    end if
    if (master) then 
      print 1,'...density ',mpi_rank,r(1,5:9,1),minval(r),maxval(r)
1     format(1x,a,i5,7g11.3)
      if (do_trace) then
        print *,minval(r),minloc(r)
        print *,maxval(r),maxloc(r)
      end if
    end if
    iv = iv+1
  end if

  if (do_momenta) then
    call read_chunk (px,io_type,iv,iread,data_unit)
    if (do_init_smooth) then
      !$omp parallel
      call smooth3t_set(px,scr1)
      px(:,2:my-1,izs:ize) = scr1(:,2:my-1,izs:ize)
      !$omp end parallel
    end if
    if (master) & 
      print 1,'..x-momenta',mpi_rank,px(1,5:9,1)
    iv = iv+1
    call read_chunk (py,io_type,iv,iread,data_unit)
    if (do_init_smooth) then
      !$omp parallel
      call smooth3t_set(py,scr1)
      py(:,2:my-1,izs:ize) = scr1(:,2:my-1,izs:ize)
      !$omp end parallel
    end if
    if (master) & 
      print 1,'..y-momenta',mpi_rank,py(1,5:9,1)
    iv = iv+1
    call read_chunk (pz,io_type,iv,iread,data_unit)
    if (do_init_smooth) then
      !$omp parallel
      call smooth3t_set(pz,scr1)
      pz(:,2:my-1,izs:ize) = scr1(:,2:my-1,izs:ize)
      !$omp end parallel
    end if
    if (master) & 
      print 1,'..z-momenta',mpi_rank,pz(1,5:9,1)
      iv = iv+1
    end if
    if (do_energy) then
      call read_chunk (e,io_type,iv,iread,data_unit)
      if (do_init_smooth) then
        !$omp parallel
        call smooth3t_set(e,scr1)
        e(:,2:my-1,izs:ize) = scr1(:,2:my-1,izs:ize)
      !$omp end parallel
    end if
    if (master) then 
      print 1,'..energy',mpi_rank,e(1,5:9,1)
      if (do_trace) then
        print *,minval(e),minloc(e)
        print *,maxval(e),maxloc(e)
      end if
    end if
    iv = iv+1
  end if

  if (do_ionization) then
    iv = iv+1
  end if

  if (do_mhd) then
    call read_chunk (Bx,io_type,iv,iread,data_unit)
    if (do_init_smooth) then
      !$omp parallel
      call smooth3t_set(Bx,scr1)
      Bx(:,2:my-1,izs:ize) = scr1(:,2:my-1,izs:ize)
      !$omp end parallel
    end if
    if (master) & 
      print 1,'..x B-field',mpi_rank,Bx(1,5:9,1)
    iv = iv+1
    call read_chunk (By,io_type,iv,iread,data_unit)
    if (do_init_smooth) then
      !$omp parallel
      call smooth3t_set(By,scr1)
      By(:,2:my-1,izs:ize) = scr1(:,2:my-1,izs:ize)
      !$omp end parallel
    end if
    if (master) & 
      print 1,'..y B-field',mpi_rank,By(1,5:9,1)
    iv = iv+1
    call read_chunk (Bz,io_type,iv,iread,data_unit)
    if (do_init_smooth) then
      !$omp parallel
      call smooth3t_set(Bz,scr1)
      Bz(:,2:my-1,izs:ize) = scr1(:,2:my-1,izs:ize)
      !$omp end parallel
    end if
    if (master) & 
      print 1,'..z B-field',mpi_rank,Bz(1,5:9,1)
    iv = iv+1

    if (do_divb_clean .or. do_init_smooth) call divb_clean

    !$omp parallel private(iy,iz)
    call ddxup_set (Bx, scratch)
    call ddyup_add (By, scratch)
    !$omp barrier
    call ddzup_add (Bz, scratch)
    do iz=izs,ize
      scratch(:,:,iz)=scratch(:,:,iz)**2 
      do iy=1,lb-1
        scratch(:,iy,iz)=0.
      end do
      do iy=ub+1,my
        scratch(:,iy,iz)=0.
      end do
    end do
    call average_subr (scratch,divb_rms)
    call fmaxval_subr ('divb',scratch,divb_max)
    !$omp end parallel
    if (master) then
      print *,'RMS(divb) =',sqrt(divb_rms)
      print *,'MAX(divb) =',sqrt(divb_max)
    end if
  end if

  if (do_pscalar) then
    call read_chunk (d,io_type,iv,iread,data_unit)
    if (master) & 
      print 1,'..pscalar  ',mpi_rank,d(1,5:9,1)
  end if
  if (io_type < 2 .or. mpi_rank==io_rank) then                          ! all ranks for io_type < 1
    close (data_unit)
  end if
  if (master) print '(1x,a,f6.1)', 'read wall time: ', wallclock() - wt
END

!***********************************************************************
SUBROUTINE write_snap (r,px,py,pz,e,d,Bx,By,Bz)
  USE params
  real, dimension(mx,my,mz) :: &
        r,px,py,pz,e,d,Bx,By,Bz
  call write_data ('dat',isnap,r,px,py,pz,e,d,Bx,By,Bz)
END

!***********************************************************************
SUBROUTINE write_scr (r,px,py,pz,e,d,Bx,By,Bz)
  USE params
  real, dimension(mx,my,mz) :: &
        r,px,py,pz,e,d,Bx,By,Bz
  call write_data ('scr',0,r,px,py,pz,e,d,Bx,By,Bz)
END

!***********************************************************************
SUBROUTINE scp_name (fname,ksnap)
  USE params
  implicit none
  integer ksnap, index, i
  integer, parameter:: mseq=6
  character(len=mfile) fname
  character(len=mseq) sequence

  if (do_scp) then
    i = index(fname,'.',back=.true.)
    write(sequence,"('_',i5.5)") ksnap
    fname = fname(1:i-1)//sequence//fname(i:mfile-mseq)
    ksnap = 0
  end if
END

!***********************************************************************
SUBROUTINE scp (fname)
  USE params
  implicit none
  character(len=*) fname
  character(len=2) amp
  if (index(scp_dir,' ').le.1) return
  amp = ' &'
  if (it+1 > nstep .or. t> tstop) amp=' '
  if (master) & 
    print *,'SCP '//trim(fname)//' '//trim(scp_dir)//amp
  call system('SCP '//trim(fname)//' '//trim(scp_dir)//' >/dev/null </dev/null '//amp)
END

!***********************************************************************
SUBROUTINE write_chunk0 (unit,rec,io_type,f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
  integer unit, rec, iz, io_type

  if (io_type == 1) then
    do iz=1,mz
      write (unit,rec=mz*(rec-1)+iz) f(:,:,iz)
    end do
  else if (io_type == 0) then
    write (unit,rec=rec) f
  end if
END

!***********************************************************************
SUBROUTINE write_data (ext,jsnap,r,px,py,pz,e,d,Bx,By,Bz)
!
!  Write snap shot
!
  USE params
  implicit none
  integer :: iv, i, lrec, iz, jsnap, ksnap
  real, dimension(mx,my,mz) :: &
        r,px,py,pz,e,d,Bx,By,Bz
  real, allocatable, dimension(:,:,:) :: lnr                             !remo: lnr made allocatable
  real, allocatable, dimension(:,:,:) :: ee,tt
  real wt, wallclock
  logical debug
  integer io_type
  character(len=*):: ext
  character(len=mfile):: fname, name
!-------------------------------------------------------

  call oflush
  call print_trace('write_data',dbg_io,'BEGIN')

  ksnap = jsnap
  if (ext .eq. 'scr') then
    fname = name('scratch.dat',ext,file)
  else
    fname = file
    call scp_name(fname,ksnap)                                          ! ksnap is set to zero here
  end if

  if (do_master_io) then
    io_type = 2
  else if (real(mx)*real(my)*real(mz) > 512.**3) then                   ! planes with large files
    io_type = 1
  else
    io_type = 0
  end if

  if (io_type == 2) then
    if (mpi_rank == io_rank) &
    open (data_unit, file=fname, &                                      ! open file
      status='unknown', form='unformatted', &                           ! unkn, unformatted
      access='direct', recl=lrec(mpi_nx*mx*mpi_ny*my))                  ! direct access
  else if (io_type == 1) then
    call mpi_name(fname)                                                ! potentially use MPI-names
    open (data_unit, file=fname, &                                      ! open file
      status='unknown', form='unformatted', &                           ! unkn, unformatted
      access='direct', recl=lrec(mx*my))                                ! direct access
  else
    call mpi_name(fname)                                                ! potentially use MPI-names
    open (data_unit, file=fname, &                                      ! open file
      status='unknown', form='unformatted', &                           ! unkn, unformatted
      access='direct', recl=lrec(mx*my*mz))                             ! direct access
  end if
  wt = wallclock()

  iv = 1
  if (do_density) then
    call write_chunk (data_unit,ksnap*mvar+iv,io_type,r )               ! density
    iv = iv+1
  end if
  if (do_momenta) then
    call write_chunk (data_unit,ksnap*mvar+iv,io_type,px)               ! x-momentum
    iv = iv+1
    call write_chunk (data_unit,ksnap*mvar+iv,io_type,py)               ! y-momentum
    iv = iv+1
    call write_chunk (data_unit,ksnap*mvar+iv,io_type,pz)               ! z-momentum
    iv = iv+1
  end if
  if (do_energy) then
    call write_chunk (data_unit,ksnap*mvar+iv,io_type,e )               ! energy
    iv = iv+1
  end if
  if (do_ionization) then
    allocate (ee(mx,my,mz), tt(mx,my,mz), lnr(mx,my,mz))		!remo: allocate lnr for limitee call
!$omp parallel private(iz)
    do iz=izs,ize
      ee(:,:,iz) = e(:,:,iz)/r(:,:,iz)
      lnr(:,:,iz)= alog(r(:,:,iz))                                      !remo: compute ln density
    end do
!$omp end parallel
    call limitee(r,lnr,e,ee)                                            !remo: call limitee before looking up temperature
    call temperature (r, ee, tt)
    if (sum(surface_int) .ne. 0.) tt(:,1,:) = surface_int
    call write_chunk (data_unit,ksnap*mvar+iv,io_type,tt)               ! temperature
    iv = iv+1
    deallocate (ee,tt)
    deallocate (lnr)                                                    !remo: deallocate lnr
  end if
  if (do_mhd) then
    call write_chunk (data_unit,ksnap*mvar+iv,io_type,Bx)               ! Bx
    iv = iv+1
    call write_chunk (data_unit,ksnap*mvar+iv,io_type,By)               ! By
    iv = iv+1
    call write_chunk (data_unit,ksnap*mvar+iv,io_type,Bz)               ! Bz
    iv = iv+1
  end if
  if (do_pscalar) then
    call write_chunk (data_unit,ksnap*mvar+iv,io_type,d )               ! metallicity
  end if
  close (data_unit)

  wt = wallclock()-wt
  if (master) then
    print '(1x,a,i4,2f14.6,i12,5x,a,f7.1,a)', &
      'write_data:', ksnap, t, dt, iseed, trim(fname), wt, 's'
  end if
  call flush_mpi
  if (do_scp .and. ext.ne.'scr') call scp(fname)

!-------------------------------------------------------
!  Write small recovery file with data needed to restart
!-------------------------------------------------------
  if (mpi_rank==io_rank) then
    if (do_trace) print *,'write_data: small files'
    fname = name('time.dat','tim',file)
    open (data_unit, file=fname, &
      status='unknown', form='unformatted', &
      access='direct', recl=lrec(4))
    write (data_unit,rec=jsnap+1) t, dtold, iseed, isnap                ! normally dtold==dt, but not at save times
    close(data_unit)
    if (debug(dbg_io)) print'(1x,a,i5,2f15.8,i5,2x,a)', &
                       'time info', jsnap, t, dt, isnap, trim(fname)
    if (do_scp) call scp(fname)
  end if

  call write_ds (file)
  call print_trace('write_data',dbg_io,'END')
END

!***********************************************************************
SUBROUTINE print_time (cpt)
!
!  Print time step information on stdout and in file.time
!
  USE params
  USE variables, ONLY: r
  USE forcing
  USE io
  implicit none
  real lgrmax
  real(kind=4):: wallclock, cpt(2), f, musppt, remain
  logical, save:: start=.true.
  logical do_io
  character(len=mfile):: name, fname
  character(len=1) timeunit
!-----------------------------------------------------------------------
  if (start .and. master) then
    write (stdout,'(a,a)') &
  '     it           t       dt   Urms   lgrmax  Cu     Cv     Cr', &
  '     Ce     Cb     Cn    musppt remain'
    start = .false.
    wct(0) = wallclock()                                                ! wall clock time start
    tstart = t                                                          ! simulation time start
  end if

  wct(2) = wallclock()-wct(0)                                           ! elapsed wall clock time

  if (nstep == 999999) then
    remain = (tstop-t)*wct(2)/max(t-tstart,1e-6)                        ! tstop wall clock time estimate
  else
    remain = (nstep-it)*wct(2)/(it+1e-6)                                ! nstep wall clock time estimate
  end if

  timeunit = 's'
  if (remain > 60) then
    remain = remain/60.
    timeunit = 'm'                                                      ! minutes
  end if
  if (remain > 60) then
    remain = remain/60.
    timeunit = 'h'                                                      ! hours
  end if
  if (remain > 72) then
    remain = remain/24.
    timeunit = 'd'                                                      ! days
  end if

  musppt = omp_nthreads*(wct(2)-wct(1))*1e6/(mx*my*mz)
  wct(1) = wct(2)
  fname = name('time.txt','time',file)
  call fmaxval_subr ('rho', r, lgrmax)
  !print *,conservative,lgrmax
  if (conservative) then
    lgrmax = alog10(lgrmax)
  else
    lgrmax = lgrmax/alog(10.)
  end if
  !print*,lgrmax,alog10(exp(maxval(r))),' lg'
  if (master) then
    open (1,file=fname,form='formatted')
    f = dtold/(dt+1e-30)
    write (stdout,'(i7,f12.6,1pe9.2,0pf7.2,f7.2,2x,6f7.3,f7.2,f6.1,a1,l3,2x,a1)') &
          it,t,dt,Urms,lgrmax,f*Cu,f*Cv,f*Cr,f*Ce,f*Cb,f*Cn,musppt,remain,timeunit,conservative,'x'
    write (1,'(a/2i7,f12.6,f9.6,2f7.2,2x,6f7.3,f7.3)') 'isnap,it,t,dt,Urms.lgrmax,Cu,Cv,Cr,Ce,Cb,Cn,musppt', &
      isnap,it,t,dt,Urms,lgrmax,f*Cu,f*Cv,f*Cr,f*Ce,f*Cb,f*Cn,musppt
    close (1)
  end if
  if (master .and. (do_check .or. do_io(t,tscr,iscr+iscr0,nscr))) then
    i_check = i_check+1
    write (check_txt(i_check),'(a)') &
     'isnap,it,t,dt,Urms,lgrmax,Cu,Cv,Cr,Ce,Cb,Cn,musppt'
    i_check = i_check+1
    write (check_txt(i_check),'(2i7,f12.6,f9.6,2f7.2,2x,6f7.3,f7.3)') &
      isnap,it,t,dt,Urms,lgrmax,f*Cu,f*Cv,f*Cr,f*Ce,f*Cb,f*Cn,musppt
  end if
END

!***********************************************************************
  SUBROUTINE stats (l, f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
  character(len=*) l
  logical omp_in_parallel

  if (mpi_size > 1) then
    call stats_omp (l, f)
  else if (omp_in_parallel()) then
    call stats_omp (l, f)
  else
    !$omp parallel
    call stats_omp (l, f)
    !$omp end parallel
  end if
END

!***********************************************************************
MODULE stats_m
  real fav, fa2, fmin, fmax
END MODULE

!***********************************************************************
  SUBROUTINE stats_omp (l, f)
  USE params
  USE stats_m
  implicit none
  real, dimension(mx,my,mz):: f
  real, pointer, dimension(:,:,:):: ff
  character(len=*) l
  real fmin1, fmax1, fminz(mz), fmaxz(mz)
  integer, dimension(3):: lmin, lmax
  integer lb1, ub1, ix, iy, iz
!-----------------------------------------------------------------------

  if (idbg.lt.3) return
  if (do_trace) print *,mpi_rank,'stats: begin'

  lb1 = max(min(lb,my),1)
  ub1 = max(min(ub,my),1)

  ff => scratch
  call average_omp (f, fav)
  if (abs(fav).lt.1e-18) then
    do iz=izs,ize
      ff(:,:,iz) = (f(:,:,iz) - fav)**2
    end do
    call average_omp (ff, fa2)
    fa2 = fa2/(fav+1e-16)**2
  else
    do iz=izs,ize
      ff(:,:,iz) = (f(:,:,iz)/(fav+1e-35)-1.)**2
    end do
    call average_omp (ff, fa2)
  end if

  if (idbg.eq.3) then
    fmin = f(1,1,1)
    fmax = f(1,1,1)
    fmin1 = f(1,1,izs)
    fmax1 = f(1,1,izs)
    do iz=izs,ize
      do iy=1,my
      do ix=1,mx
        fmin1 = min(fmin1,f(ix,iy,iz))
        fmax1 = max(fmax1,f(ix,iy,iz))
      end do
      end do
    end do
    !$omp critical
    fmax = max(fmax,fmax1)
    fmin = max(fmin,fmin1)
    !$omp end critical
    call barrier_omp('io2')
    write (*,'(1x,a,i5,4(1pe11.3),2x,a)') 'min,av,max,rms:', &
      mpi_rank,fmin,fav,fmax,fav*sqrt(fa2),l
  else if (idbg.gt.3) then
    !$omp single
    lmin = minloc(f(:,lb1:ub1,:)) ; lmin(2)=lmin(2)+lb1
    lmax = maxloc(f(:,lb1:ub1,:)) ; lmax(2)=lmax(2)+lb1
    fmin = f(lmin(1),lmin(2),lmin(3))
    fmax = f(lmax(1),lmax(2),lmax(3))
    !$omp end single
    write (*,'(1x,a,i5,1pe11.3,3i5,1pe11.3,1pe11.3,3i5,1pe11.3,2x,a)') &
      'min,av,max,rms:', mpi_rank,fmin,lmin,fav,fmax,lmax,fav*sqrt(fa2),l
  end if
  if (do_trace) print *,mpi_rank,'stats: end'

  END

!***********************************************************************
LOGICAL FUNCTION thesame (label, rec, f, g, ndif)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f, g
  integer ix,iy,iz,maxprint,ndif,rec
  character(len=*) label
!
  thesame = .true.
  maxprint = 10
  ndif = 0
  !!$omp parallel do private(iz,iy,ix), reduction(and:thesame)
  do iz=1,mz
  do iy=1,my
  do ix=1,mx
    if (f(ix,iy,iz) .ne. g(ix,iy,iz)) then
      if (ndif < maxprint) then
        thesame = thesame .and. .false.
        print 1,'difference:',label,mpi_rank,rec,ix,iy,iz,f(ix,iy,iz),g(ix,iy,iz)
1       format(2(1x,a),i3,4i5,2(1pg16.8))
      end if
      !!$omp critical
      ndif = ndif + 1
      !!$omp end critical
    end if
  end do
  end do
  end do
END

!***********************************************************************
SUBROUTINE dump(f,dumpfile,rec)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
  character(len=*) dumpfile
  integer rec
!-----------------------------------------------------------------------
  if (.not. (do_dump .or. do_compare)) return
  call dumpl(f,' ',dumpfile,rec)
END

!***********************************************************************
SUBROUTINE dumps(f,label,dumpfile)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
  character(len=*) label,dumpfile
  logical do_io, save

  if (isubstep==1 .and. (abs(t-tsnap*(isnap-1+isnap0)) < 0.01*dtold)) then
    save = do_dump
    do_dump = .true.
    call dumpn(f,label,dumpfile,1)
    do_dump = save
  end if
END

!***********************************************************************
SUBROUTINE dumpn(f,label,dumpfile,rec)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
  character(len=*) label,dumpfile
  integer rec
  integer, parameter:: mfiles=30
  integer, save:: nfiles=0, ifile, irec(mfiles)
  character(len=mid), save:: files(mfiles)

  if (.not. do_dump) return

  !$omp barrier
  !$omp master
  do ifile=1,nfiles
    if (trim(dumpfile) == trim(files(ifile))) then
      goto 1
    end if
  end do
  nfiles = nfiles+1
  if (nfiles > mfiles) then
    call end_mpi
    print *,'dumpn: nfiles, mfiles =', nfiles, mfiles
    stop
  end if
  ifile = nfiles
  files(ifile) = dumpfile
  irec(ifile) = -1
  if (master) print*,'dumpn: ifile, file =', ifile, trim(files(ifile))

1 continue
  if (rec == 0) then
    irec(ifile)=0
  else
    irec(ifile) = irec(ifile)+1
  end if
  !$omp end master
  !$omp barrier
  call dumpl (f,label,dumpfile,irec(ifile))
END

!***********************************************************************
SUBROUTINE dumpl(f,label,dumpfile,rec)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
  character(len=*) label,dumpfile
  character(len=mfile) fname
  logical fexists
  integer rec, lrec, i, ndif, io_type
  logical thesame, ok
  logical, save:: first_time = .true.
  integer, save:: irec
  character(len=mid), save:: id='dumpl: $Id: io_limitee.f90,v 1.1 2016/10/12 11:16:20 remo Exp $'
!-----------------------------------------------------------------------
  if (.not. (do_dump.or.do_compare)) return                             ! faster than fexists()
  call split_name(file)
  fname = trim(head)//dumpfile
  call barrier_omp('dumpl1')
  !$omp master
  if (fexists(fname)) then
    if (do_compare) then
      call print_trace (id, dbg_io, 'READ')
      if (mpi_size > 1 .and. (do_master_io .or. fexists(fname))) then
        io_type = 2
        open(dump_unit,file=fname,access='direct',recl=lrec(mpi_nx*mx*mpi_ny*my))
      else
        io_type = 1
        call mpi_name(fname)
        open(dump_unit,file=fname,access='direct',recl=lrec(mx*my))
      end if
      call read_chunk (scratch,io_type,rec,0,dump_unit)
      close(dump_unit)
      call print_trace (id, dbg_io, 'COMPARE')
      ok = thesame(label,rec,f,scratch,ndif)
      i = index(fname,' ')
      if (ok) then
        print 1,'compare:',label,fname(1:i),rec,mpi_rank,dbg_barrier,ok
      else
        print 1,'compare:',label,fname(1:i),rec,mpi_rank,dbg_barrier,ok,ndif/float(mw)
      end if
1     format(3(1x,a),3i5,l5,f8.3)
      call print_trace (id, dbg_io, 'END')
    else
      call print_trace (id, dbg_io, 'WRITE')
      call write_ds(fname)
      i = index(fname,' ')
      if (master) print 1,'dump:',label,fname(1:i),rec,mpi_rank

      if (do_master_io .and. mpi_size>1) then
        io_type = 2                                                   ! read global planes
        if (mpi_rank == io_rank) &
        open (dump_unit, file=fname, &                                ! open file
          status='unknown', form='unformatted', &                     ! unkn, unformatted
          access='direct', recl=lrec(mpi_nx*mx*mpi_ny*my))            ! direct access
      else
        io_type = 1                                                   ! read local planes
        call mpi_name(fname)                                          ! potentially use MPI-names
        open (dump_unit, file=fname, &                                ! open file
          status='unknown', form='unformatted', &                     ! unkn, unformatted
          access='direct', recl=lrec(mx*my))                          ! direct access
      end if
      call write_chunk (dump_unit,rec+1,io_type,f)
      close(dump_unit)

      call print_trace (id, dbg_io, 'END')
    end if
  end if
  !$omp end master
  call barrier_omp('dumpl2')
END

!***********************************************************************
SUBROUTINE open_checkfile
  USE params
  implicit none
  integer i
  character(len=mid):: id='open_checkfile'
  character(len=mfile):: name, fname
  logical do_io, opened

  if (.not. master) return
  if (.not. (do_check .or. do_io(t,tscr,iscr+iscr0,nscr))) return
  call print_id(id)

  inquire (check_unit,opened=opened)
  if (.not. opened) then 
    fname = name('check.txt','chk',file)
    print*,'opening check_unit', mpi_rank, trim(fname)
    open (check_unit,file=fname,form='formatted',status='unknown')
  end if
END

!***********************************************************************
SUBROUTINE close_checkfile
  USE params
  USE io
  implicit none
  integer i
  character(len=120) line
  character(len=mfile):: name, fname
  character(len=mid):: id='close_checkfile'
  logical omp_in_parallel, opened
  logical do_io

  if (.not. master) return
  if (.not. (do_check .or. do_io(t,tscr,iscr+iscr0,nscr))) return
  call print_id(id)

  inquire (check_unit,opened=opened)
  if (opened) then
    do i=1,i_check
      write(check_unit,'(a)') trim(check_txt(i))
    end do
    rewind (check_unit)
    i_check = 0
  end if
END

!***********************************************************************
SUBROUTINE fminval_subr (label, f, fmin1)
  USE params
  implicit none
  character(len=*) label
  real, dimension(mx,my,mz):: f
  real fmin1, fminval
  logical omp_in_parallel
  character(len=mid):: id='fminval_subr, $Id: io_limitee.f90,v 1.1 2016/10/12 11:16:20 remo Exp $'
!
  call print_id(id)
  if (omp_in_parallel()) then
    call fminval_omp (label, f, fmin1)
  else
    !$omp parallel shared(fmin1)
    call fminval_omp (label, f, fmin1)
    !$omp end parallel
  end if
END

!***********************************************************************
SUBROUTINE fminval_omp (label,f,fmin1)
!
!  Max value returned, and written to file
!
!-----------------------------------------------------------------------
  USE params
  USE io
  implicit none
  character(len=*) label
  real, dimension(mx,my,mz):: f
  integer iz, lmin1(2),im,jm,km(1),lb1,ub1
  real fmin1
  logical do_io
  character(len=mid):: id='fminval_omp, $Id: io_limitee.f90,v 1.1 2016/10/12 11:16:20 remo Exp $'
!
  call print_id(id)
!
  lb1 = max(min(lb,my),1)
  ub1 = max(min(ub,my),1)

  do iz=izs,ize
    lmin1 = minloc(f(:,lb1:ub1,iz))
    lmin(1,:,iz) = lmin1
    lmin1(1) = max(1,min(mx       ,lmin1(1)))
    lmin1(2) = max(1,min(ub1-lb1+1,lmin1(2)))
    fmin(1,iz) = f(lmin1(1),lmin1(2)+lb1-1,iz)
  end do
  call barrier_omp('io1')
!$omp single
  km = minloc(fmin(1,:))
  if (km(1).eq.0) then
    print *,'fminval: minloc returned 0'
    print *,fmin(1,:)
    stop
  end if
  im = lmin(1,1,km(1))
  if (im.eq.0) then
    print *,'fminval: minloc returned 0'
    print *,f(:,lb1,km(1))
    stop
  end if
  jm = lmin(1,2,km(1))+lb1-1
  if (jm.eq.0) then
    print *,'fminval: minloc returned 0'
    print *,f(im,:,km(1))
    stop
  end if
  fmin1 = f(im,jm,km(1))

  call fmin_mpi (im,jm,km,fmin1)

  if (mpi_master .and. (do_check .or. do_io(t,tscr,iscr+iscr0,nscr))) then
    i_check = i_check+1
    write (check_txt(i_check),'(1x,a8,3i4,1pg11.3)') label,im,jm,km(1),fmin1
  end if
  if (mpi_master .and. do_logcheck .and. isubstep==1) &
    print '(1x,a,3i4,1pg11.3,2i4)',label,im,jm,km,fmin1,omp_mythread,mpi_rank
!$omp end single
END

!***********************************************************************
SUBROUTINE fmaxval_subr (label, f, fmax1)
  USE params
  implicit none
  character(len=*) label
  real, dimension(mx,my,mz):: f
  real fmax1, fmaxval
  logical omp_in_parallel
  logical do_io
  character(len=mid):: id='fmaxval_subr, $Id: io_limitee.f90,v 1.1 2016/10/12 11:16:20 remo Exp $'
!
  call print_id(id)
  if (omp_in_parallel()) then
    call fmaxval_omp (label, f, fmax1)
  else
    !$omp parallel shared(fmax1)
    call fmaxval_omp (label, f, fmax1)
    !$omp end parallel
  end if
END

!***********************************************************************
SUBROUTINE fmaxval_omp (label,f,fmax1)
!
!  Max value returned, and written to file
!
!-----------------------------------------------------------------------
  USE params
  USE io
  implicit none
  character(len=*) label
  real, dimension(mx,my,mz):: f
  integer iz, loc1(1), loc2(2), im, jm, km, lb1, ub1
  real fmax1
  logical do_io
  character(len=mid):: id='fmaxval_omp, $Id: io_limitee.f90,v 1.1 2016/10/12 11:16:20 remo Exp $'
!
  call print_id(id)
!
  lb1 = max(min(lb,my),1)
  ub1 = max(min(ub,my),1)
  do iz=izs,ize
    fmax(1,iz) = maxval(f(:,lb1:ub1,iz))
  end do
  call barrier_omp('io1')

!$omp single
  loc1 = maxloc(fmax(1,:))
  km = loc1(1)
  if (km.eq.0) then
    print *,'fmaxval 1: maxloc returned 0'
    print *,fmax(1,:)
    stop
  end if
  loc2 = maxloc(f(:,lb1:ub1,km))
  im = loc2(1)
  if (im.eq.0) then
    print *,'fmaxval 2: maxloc returned 0'
    print *,f(:,lb1,km)
    stop
  end if
  jm = loc2(2)+lb1-1
  if (jm.eq.0) then
    print *,'fmaxval 3: maxloc returned 0'
    print *,f(im,:,km)
    stop
  end if
  fmax1 = f(im,jm,km)
  
  call fmax_mpi (im,jm,km,fmax1)

  if (master .and. (do_check .or. do_io(t,tscr,iscr+iscr0,nscr))) then
    i_check = i_check+1
    write (check_txt(i_check),'(1x,a8,3i4,1pg11.3)') label,im-1,jm-1,km-1,fmax1
  end if
  if (mpi_master .and. do_logcheck .and. isubstep==1) &
    print '(1x,a,3i4,1pg11.3)',label,im-1,jm-1,km-1,fmax1
!$omp end single
END

!***********************************************************************
SUBROUTINE maxabs_subr (nx, ny, nz, f, fmax1)
!
!  Max value returned
!
!-----------------------------------------------------------------------
  implicit none
  integer nx, ny, nz
  real, dimension(nx,ny,nz):: f
  integer iz,lmax(32,2,nz),lmax1(2),im,jm,km(1)
  real fmax1, fmax(32,nz)
!
  do iz=1,nz
    lmax1 = maxloc(abs(f(:,:,iz)))
    lmax(1,:,iz) = lmax1
    lmax1(1) = max(1,min(nx, lmax1(1)))
    lmax1(2) = max(1,min(ny, lmax1(2)))
    fmax(1,iz) = abs(f(lmax1(1),lmax1(2),iz))
  end do
  km = maxloc(fmax(1,:))
  if (km(1).eq.0) then
    print *,'maxabs: maxloc returned 0'
    print *,fmax(1,:)
    stop
  end if
  im = lmax(1,1,km(1))
  if (im.eq.0) then
    print *,'maxabs: maxloc returned 0'
    print *,f(:,1,km(1))
    stop
  end if
  jm = lmax(1,2,km(1))
  if (jm.eq.0) then
    print *,'maxabs: maxloc returned 0'
    print *,f(im,:,km(1))
    stop
  end if
  fmax1 = abs(f(im,jm,km(1)))
END

!***********************************************************************
SUBROUTINE fstat1(label,f,i1,i2)
!
!  Max value
!
!-----------------------------------------------------------------------
  USE params
  USE io, ONLY: check_txt, i_check
  implicit none
  character(len=*) label
  real, dimension(mx,my,mz):: f
  integer lmin(3), lmax(3), i1, i2
  real :: fmin, fmax, rms, aver
  logical do_io

  lmin = minloc(f(:,i1:i2,:))
  fmin = f(lmin(1),lmin(2)+i1-1,lmin(3))
  lmax = maxloc(f(:,i1:i2,:))
  fmax = f(lmax(1),lmax(2)+i1-1,lmax(3))
  aver = sum(f(:,i1:i2,:))/(mx*(i2-i1+1)*mz)
  rms = sqrt(sum((f(:,i1:i2,:)-aver)**2)/(mx*(i2-i1+1)*mz))

  if (master .and. (do_check .or. do_io(t,tscr,iscr+iscr0,nscr))) then
    i_check = i_check+1
    write (check_txt(i_check),'(1x,a,2(3i4,1pg11.3),2(1pg11.3))') label, &
      lmin(1),lmin(2)+i1-1,lmin(3),fmin,lmax(1),lmax(2)+i1-1,lmax(3), &
      fmax,aver,rms
  end if
END

!***********************************************************************
SUBROUTINE split_name (ofile)
  USE params
  implicit none
  character(len=mfile) ofile

  ihead = index(ofile,'/',back=.true.)      ! index to slash
  ihead = max(ihead,0)
  itail = ihead+1
  tail = ofile(itail:mfile)                 ! file name only
  if (ihead.gt.0) then
    head = file(1:ihead)                    ! dir name
  else
    head = './'                             ! current dir
    ihead = 2
  end if
END SUBROUTINE

!***********************************************************************
FUNCTION from_name (temp, ext, prev)
  USE params
  implicit none
  character(len=*) temp,ext
  character(len=mfile) from_name,prev,from_head,from_tail
  integer idot,ifrh,ifrt
  logical debug

  ifrh = index(from,'/',back=.true.)
  ifrt = ifrh+1
  from_tail = from(ifrt:mfile)
  if (ifrh.gt.0) then
    from_head = from(1:ifrh)                ! dir name
  else
    from_head = './'                        ! current dir
    ifrh = 2
  end if

  if (from_tail=='snapshot.dat' .or. from_tail=='scratch.dat') then
    from_name = from_head(1:ifrh)//temp
  else
    idot = index(prev,'.',back=.true.)
    from_name = prev(1:idot)//ext
  end if
  if (master .and. debug(dbg_io)) then
    print *,'from: ',trim(from) !(1:index(' ',head))
    print *,'from_head: ',trim(from_head) !(1:index(' ',head))
    print *,'from_tail: ',trim(from_tail) !(1:index(' ',tail))
    print *,'temp: ',trim(temp) !(1:index(' ',temp))
    print *,'ext: ',trim(ext) !(1:index(' ',ext))
    print *,'prev: ',trim(prev) !(1:index(' ',prev))
    print *,'from_name: ',trim(from_name) !(1:index(' ',name))
  end if
END FUNCTION

!***********************************************************************
FUNCTION name (temp, ext, prev)
  USE params
  implicit none
  character(len=*) temp,ext
  character(len=mfile) name,prev
  integer idot, len
  logical debug

  if (tail=='snapshot.dat' .or. tail=='scratch.dat') then
    if (ihead.gt.0) then
      name = head(1:ihead)//temp
    else
      name = temp
    end if
  else
    idot = index(prev,'.',back=.true.)
    if (idot.le.0) then
      print*,'FUNCTION name: idot must be positive, is',idot
      call end_mpi
    end if
    name = prev(1:idot)//ext
  end if
  if (master .and. debug(dbg_io)) then
    print'(1x,10(2a,2x))',"name: temp=",trim(temp), &
                          "ext =",trim(ext ), &
                          "prev=",trim(prev), &
                          "head=",trim(head), &
                          "tail=",trim(tail), &
                          "name=",trim(name)
  end if
END FUNCTION

!***********************************************************************
SUBROUTINE write_ds (ofile)
  USE params
  implicit none
  integer i
  logical, save:: one_scp=.true.
  character(len=mfile) ofile
  character(len=mfile) name,fname
!-------------------------------------------------------
!  Write text file intended for IDL procedure open.pro
!-------------------------------------------------------
  call split_name (ofile)
  fname = name('grid.txt','dx',ofile)
  if (master) then
    open (time_unit, file=fname,form='formatted')
    write (time_unit,'(3i5)') mx*mpi_nx,my*mpi_ny,mz*mpi_nz
    write (time_unit,'(3(1pe14.6))') dx,dy,dz
    write (time_unit,*) gamma,0
    write (time_unit,*) mvar
    write (time_unit,*) npart
    write (time_unit,'(7i5)') mpi_size, mpi_nx, mpi_ny, mpi_nz, mpi_x, mpi_y, mpi_z
    close (time_unit)
    if (do_scp .and. one_scp) call scp(fname)
  end if
  if (mpi_size > 1 .and. (.not. do_master_io)) then
    call mpi_name(fname)
    open (time_unit, file=fname,form='formatted')
    write (time_unit,'(3i5)') mx,my,mz
    write (time_unit,'(3(1pe14.6))') dx,dy,dz
    write (time_unit,*) gamma,0
    write (time_unit,*) mvar
    write (time_unit,*) npart
    write (time_unit,'(7i5)') mpi_size, mpi_nx, mpi_ny, mpi_nz, mpi_x, mpi_y, mpi_z
    close (time_unit)
    if (do_scp .and. one_scp) call scp(fname)
  end if
  one_scp = .false.
END SUBROUTINE

!***********************************************************************
REAL FUNCTION wallclock()
  implicit none
  integer, save:: count, count_rate=0, count_max
  real, save:: previous=0., offset=0.

  if (count_rate == 0) then
    call system_clock(count=count, count_rate=count_rate, count_max=count_max)
    offset = -count/real(count_rate)
  else
    call system_clock(count=count)
  end if
  wallclock = count/real(count_rate) + offset
  if (wallclock < previous) then
    offset = offset + real(count_max)/real(count_rate)
    wallclock = count/real(count_rate) + offset
  end if
  previous = wallclock
END

!***********************************************************************
SUBROUTINE print_trace (id, dbg, label)
  USE params
  implicit none
  character(len=*) id
  character(len=*) label
  integer dbg
  real wallclock
  logical debug
  
  if (do_trace .or. debug(dbg)) then
    if (master) print '(1x,f9.3,2x,a,2x,a)',wallclock(),label,id
    call barrier_mpi ('print_trace')
  end if
END

!***********************************************************************
SUBROUTINE print_id (id)
  USE params
  implicit none
  character(len=mid) id
  real wallclock
  logical debug
  
  if (master .and. do_trace) then
    print '(1x,f9.3,2(2x,i4),2x,a)',wallclock(),dbg_select,i_barrier,id
    return
  end if

  if (id .ne. ' ') then
    !$omp barrier
    !$omp master
    if (master) print '(1x,a)', id
    id = ' '
    !$omp end master
    !$omp barrier
  end if
END

!***********************************************************************
FUNCTION debug (dbg_id)
  USE params
  implicit none
  logical debug
  integer dbg_id
  
  debug = iand(dbg_id,dbg_select).ne.0
END

!***********************************************************************
SUBROUTINE assert_small (message, error, tolerance)
  USE params
  implicit none
  character(len=*) message
  real error, tolerance

  if (error < tolerance) then
    if (master) print 1, message, ' OK, for error & tolerance =', error, tolerance
  else
    print 1, message, ' FAILED for error & tolerance = ', error, tolerance &
           , ' on processor & thread', mpi_rank, omp_mythread
  end if
1 format(1x,2a,1p,2e10.3,a,2i5)
END

!***********************************************************************
SUBROUTINE assert_code (message, test, code)
  USE params
  implicit none
  character(len=*) message
  logical test
  integer code

  if (test) then
    if (.not.master) return
    !if (do_trace) print 1, message, ' OK, code = ', code
  else
    print 1, message, ' FAILED, code, rank = ', code,mpi_rank
  end if
1 format(1x,2a,i5)
END

!***********************************************************************
MODULE timer_m
  USE params
  implicit none
  integer, parameter:: mtimer=100
  integer, save:: ntimer=0
  integer itimer
  real, save:: current=0., previous=0.
  real times(mtimer), times_total(mtimer)
  character(len=mid):: time_label(mtimer)
END MODULE

!***********************************************************************
SUBROUTINE timer_print
  USE params
  USE timer_m
  implicit none
  integer rank
!-----------------------------------------------------------------------
  if (ntimer == 0) return

  call barrier_mpi('timer')
  if (mpi_rank == 0) then
    print *,'---------------------------------------------------------- timer print -----------------------------------------------'
  end if
  do rank=0,mpi_size-1
    if (rank == mpi_rank) then
      print '(i5,20f7.2/(5x,20f7.2))',rank,times(1:ntimer)
    end if
    call barrier_mpi('timer')
  end do
  if (mpi_rank == mpi_size-1) then
    print *,'----------------------------------------------------------------------------------------------------------------------'
  end if
  times = 0.
END

!***********************************************************************
SUBROUTINE timer (routine, label)
  USE params
  USE timer_m
  implicit none
  character(len=*) routine, label
  real fetime, time_incr, percent, total
  integer i
!-----------------------------------------------------------------------
  if (.not. do_timer) return

  if (ntimer == 0) times_total = 0.

  current = fetime()
  time_incr = (current-previous)*omp_nthreads
  previous = current
  if (trim(routine) == 'BEGIN') then
    time_incr=0.
  else if (trim(routine) == 'PRINT') then
    call timer_print
    return
  else if (trim(routine) == 'END') then
    if (master) then
      print *,'TIMER:'
      total = sum(times_total)
      do itimer=1,ntimer
        i = index(time_label(itimer),':')
        percent = times_total(itimer)/total*100.
        print "(i4,a16,2x,a16,f12.2,f7.1)", &
          itimer,time_label(itimer)(1:i-1),time_label(itimer)(i+1:i+20),times_total(itimer),percent
      end do
    end if
    return
  end if

  if (do_trace) then
    if (idbg > 0) then
      print "(1x,'TIMER: ',2a16,2i5,f10.3)",routine,label,mpi_rank,omp_mythread,time_incr
    else if (master) then
      print "(1x,'TIMER: ',2a16,f10.3)",routine,label,time_incr
    end if
  end if

  do itimer=1,ntimer
    if (trim(routine)//':'//trim(label) == trim(time_label(itimer))) then
      times(itimer) = times(itimer) + time_incr
      times_total(itimer) = times_total(itimer) + time_incr
      return
    end if
  end do
  ntimer = ntimer+1
  times(ntimer) = time_incr
  time_label(ntimer) = trim(routine)//':'//trim(label)
END
