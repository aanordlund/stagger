! $Id: radiation_lookup_mpi_remo.f90,v 1.1 2016/10/12 13:08:46 remo Exp $
!***********************************************************************
MODULE cooling
  USE params
  integer, parameter:: mmu=9, mphi=10
  real dphi, form_factor, dtaumin, dtaumax
  integer nmu, nphi, verbose, ny0
  integer mblocks							!maximum number of subdomains considered (per side) to fill the ghost zones

  integer n1, n2
  real y_newton, dy_newton, t_newton, ee_newton, ee_min, t_ee_min
  real, dimension(mmu):: mu0

  real, allocatable, dimension(:,:):: xi
  real, pointer, dimension(:,:,:):: rk,lnrk,s,q,dtau,qq

  logical do_table, do_newton, do_limb
  character(len=mfile):: intfile
END MODULE

!***********************************************************************
SUBROUTINE init_cooling (r,e)
  USE params
  USE arrays
  USE cooling
  implicit none
  real, dimension(mx,my,mz):: r,e
  integer i
  character(len=mfile):: name

  do_cool = .true.
  i = index(file,'.')
  nmu = 0
  nphi = 4
  dphi = 0.
  form_factor = 1.
  dtaumax = 100.
  dtaumin = 0.1
  intfile = name('intensity.dat','int',file)

  do_newton = .false.
  y_newton = -0.3
  dy_newton = 0.05
  t_newton = 0.01
  t_ee_min = 0.005
  ee_newton = 5.265
  ee_min = 3.6
  do_limb  = .false.
  verbose = 0
  ny0 = 0
  mblocks = 1								!default: communicate only nearest neighbour

  call read_cooling
  allocate (xi(mx,mz))
  rk   => scr1
  lnrk => scr2
  s    => scr3
  q    => scr4
  dtau => scr5
  qq   => scr6
END SUBROUTINE

!***********************************************************************
SUBROUTINE read_cooling
  USE params
  USE eos
  USE cooling
  integer sub_nx, sub_nz
  character(len=64) reshape_mode
  namelist /cool/do_cool, nmu, nphi, dphi, form_factor, dtaumin, dtaumax, intfile, &
    do_newton, y_newton, dy_newton, t_newton, ee_newton, ee_min, t_ee_min, do_limb, mu0, &
    ny0, verbose, mblocks, sub_nx, sub_nz, reshape_mode
  character(len=mfile):: name, fname

  character(len=mid):: id='$Id: radiation_lookup_mpi_remo.f90,v 1.1 2016/10/12 13:08:46 remo Exp $'
  call print_id(id)

  sub_nx = -999; sub_nz = -999; reshape_mode='void'
  rewind (stdin); read (stdin,cool)
  if (master) write (*,cool)
  if (master) write (*,*) 'mbox, dbox =', mbox, dbox
  if (do_limb) then
    fname = name('limb.dat','lmb',file)
    call mpi_name(fname)
    open (limb_unit, file=trim(fname), &
      form='unformatted', status='unknown')
    write (limb_unit) mx, mz,nmu,nphi
  end if
  call mpi_name(intfile)
END SUBROUTINE

!***********************************************************************
SUBROUTINE coolit (r, ee, lne, dd, dedt)
!
!  Radiative cooling
!
  USE params
  !USE variables
  USE arrays, only: lns
  USE units
  USE eos
  USE cooling
  implicit none
  real, dimension(mx,my,mz):: r, ee, lne, dd, dedt
  real xmu(mmu), wmu(mmu)
  integer ix, iy, iz, lmu, lphi, imu, iphi, ibox, n1p, n2p, ny
  real sec, fdtime, dxdy, dzdy, tanth, qq1, f1, f2, dtau2
  real phi, wphi, womega
  integer lrec, idmp, imaxval_mpi, nyr
  logical flag, do_io
  logical, save:: first=.true.
  external transfer
  logical debug
  real cput(2), void, dtime, prof
  integer, save:: nrad=0, nang=0
!
  if (.not. do_cool) return

  if (debug(dbg_cool) .and. omp_master) then; void = dtime(cput) &
    ; print '(1x,a8,2f7.3)','rad0   ',cput*omp_nthreads; endif

  if (lb > 1) then                                                      ! if we have ghost zones
    dedt(:,1:lb-1,izs:ize) = 0.                                         ! zap previous dedt there
  end if

!-----------------------------------------------------------------------
!  Angular quadrature
!  nmu > 0: Use Gauss integration (mu=1 excluded) with 2*nmu   order accuracy
!  nmu = 0: One vertical ray with weight 0.5
!  nmu < 0: Use Radau integration (mu=1 included) with 2*nmu-1 order accuracy
!-----------------------------------------------------------------------
  if      (nmu .gt. 0) then
    lmu = nmu
    call gausi (lmu,0.,1.,wmu,xmu)
  else if (nmu .eq. 0) then
    lmu = 1
    xmu(1) = 1.
    wmu(1) = 0.5
  else 
    lmu =-nmu
    call radaui(lmu,0.,1.,wmu,xmu)
  end if
  if (mu0(1) .ne. 0.) xmu=mu0

  if (first) then
    call barrier_omp ('rad-first')
    if (omp_master) first = .false.
    if (master) then
      print *,' imu       xmu    theta'
      !           1.....0.000.....00.0
      do imu=1,lmu 
        print '(i4,f10.3,f8.1,f10.3)',imu,xmu(imu),acos(xmu(imu))*180./pi,wmu(imu)
      end do
    end if
  end if

!-----------------------------------------------------------------------
!  Equation of state and opacity
!-----------------------------------------------------------------------
  call lookup (r,ee,lnrk,lns)

  !$omp barrier
  !$omp single
  n2 = 2
  !$omp end single
  n2p = 2

  do iz=izs,ize
    do iy=2,my
    do ix=1,mx
      dtau2 = 0.5*dymdn(iy)*(exp(lnrk(ix,iy-1,iz))+exp(lnrk(ix,iy,iz)))
      if (dtau2 < dtaumax) n2p = max(n2p,iy)
    end do
    end do
  end do

  !$omp critical
  n2 = max(n2p,n2)
  !$omp end critical
  !$omp barrier

  if (ny0==0) then
    n2 = imaxval_mpi (n2)
    ny  = n2
  else
    ny = ny0
  end if

  idmp = 0
  call dumpn (r,'r','rad.dmp',0)
  call dumpn (ee,'ee','rad.dmp',1)
  call dumpn (lnrk,'lnrk','rad.dmp',1)
  do ibox=1,mbox
    call dumpn (lns(:,:,:,ibox),'lns','rad.dmp',1)
  end do
  idmp = 0

  if (debug(dbg_cool) .and. omp_master) then; void = dtime(cput) &
    ; print '(1x,a8,2f7.3)','lookup ',cput*omp_nthreads; endif

  do iz=izs,ize
    qq(:,:,iz) = 0.
  end do

  call dumpn (lnrk,'lnrk','trf.dmp',0)
  do imu=1,lmu
    if (nmu .eq. 0 .and. imu .eq. 1) then
      lphi = 1 
    else if (nmu .lt. 0 .and. imu .eq. lmu) then
      lphi = 1 
    else
      lphi = nphi
    end if
    tanth = tan(acos(xmu(imu)))
    wphi = 4.*pi/lphi

    do iphi=1,lphi
      phi = (iphi-1)*2.*pi/lphi + dphi*t
      dxdy = tanth*cos(phi)
      dzdy = tanth*sin(phi)
      if (master .and. verbose==1 .and. isubstep==1) &
        print '(a,3i5,2e15.7)',' imu,iphi,ny,dxdy,dzdy=', &
	                         imu,iphi,ny,dxdy,dzdy

      call trnslt (lnrk, rk, dxdy, dzdy, .true., ny, mblocks, verbose)
      call dumpn (rk,'rk','trf.dmp',1)

      do ibox=1,mbox
        !$omp barrier
        !$omp single
        n1 = 2
        n2 = 2
        !$omp end single
        n1p = 2
        n2p = 2
        if (ibox.eq.1) then
          f1 = (0.5/xmu(imu))
          do iz=izs,ize
            dtau(:,1,iz) = dym(1)*rk(:,1,iz)/xmu(imu)
            do iy=2,ny
            do ix=1,mx
              dtau(ix,iy,iz) = f1*dymdn(iy)*(rk(ix,iy-1,iz)+rk(ix,iy,iz))
              if (dtau(ix,iy,iz) < dtaumin) n1p = max(n1p,iy)
              if (dtau(ix,iy,iz) < dtaumax) n2p = max(n2p,iy)
            end do
            end do
          end do
        else
          do iz=izs,ize
          do iy=1,ny
          do ix=1,mx
            dtau(ix,iy,iz) = (10.**dbox)*dtau(ix,iy,iz)
            if (dtau(ix,iy,iz) < dtaumin) n1p = max(n1p,iy)
            if (dtau(ix,iy,iz) < dtaumax) n2p = max(n2p,iy)
          end do
          end do
          end do
        end if

        if (ny0==0) then
          !$omp critical
          n1 = max(n1p,n1)
          n2 = max(n2p,n2)
          !$omp end critical
          !$omp barrier
          n2 = imaxval_mpi (n2)
        else
          n2 = ny0
        end if

	nrad = nrad + n2
	nang = nang + 1
        if (master .and. verbose==2 .and. isubstep==1) then
	  nyr = (nrad+0.5)/nang
          print '(a,7i5,1p(2e15.7))',' imu,iphi,ibox,ny,n1,n2,nyr,dxdy,dzdy=', &
	                               imu,iphi,ibox,ny,n1,n2,nyr,dxdy,dzdy
	end if

!-----------------------------------------------------------------------
!  Source function
!-----------------------------------------------------------------------
        call trnslt (lns(:,:,:,ibox), s, dxdy, dzdy, .true., n2, mblocks, verbose)

!-----------------------------------------------------------------------
!  Raditative transfer equation
!-----------------------------------------------------------------------
        flag = do_limb .and. do_io(t+dt,tscr,iscr+iscr0,nscr)                         ! limb darkening every scratch
        flag = flag .or. (xmu(imu)==1. .and. do_io(t+dt,tsnap,isnap+isnap0,nsnap))    ! or else every nsnap and only mu=1
        flag = flag .and. isubstep.eq.1 .and. ibox.eq.1                               ! and only for first substep and bin

        call transfer (n2,n1,dtau,s,q,xi,flag)
        if (flag) then
          call barrier_omp('limb')
          if (do_limb .and. omp_master) then
            write(limb_unit) t,imu,iphi,xmu(imu),phi,xi
            print *,omp_mythread,isubstep,ibox,imu,iphi
          end if
        end if
        call dumpn(dtau,'dtau','trf.dmp',1)
        call dumpn(s,'s','trf.dmp',1)
        call dumpn(q,'q0','trf.dmp',1)

        f1 = 10.**(ibox-1)
        do iz=izs,ize
          do iy=2,n2-1
            f2 = xmu(imu)/(ym(iy+1)-ym(iy-1))
            q(:,iy,iz) = q(:,iy,iz)*(dtau(:,iy+1,iz)+dtau(:,iy,iz))*f2
          end do
          q(:,1,iz) = q(:,1,iz)*rk(:,1,iz)*f1
        end do
        call dumpn(q,'q1','trf.dmp',1)

        if (flag .and. intfile.ne.' ') then
          !$omp barrier
          !$omp master
          if (master) print '(a,i5,4x,a,/,5g12.3)','radiation intensity:',1+isnap,intfile,xi(1:5,1)
          open (2,file=intfile,form='unformatted',status='unknown',access='direct',recl=lrec(mx*mz))
          write (2,rec=2+isnap) xi
          close (2)
          if (do_scp) call scp(intfile)
          !$omp end master
        end if

!-----------------------------------------------------------------------
!  Energy equation
!-----------------------------------------------------------------------
        call trnslt (q, s, -dxdy, -dzdy, .false., n2, mblocks, verbose)
        call dumpn(s,'q2','trf.dmp',1)

        f1 = wmu(imu)*wphi*form_factor
        do iz=izs,ize
        do iy=1,n2
        do ix=1,mx
          qq1 = f1*s(ix,iy,iz)
          qq(ix,iy,iz) = qq(ix,iy,iz) + qq1
          dedt(ix,iy,iz) = dedt(ix,iy,iz) + qq1
        end do
        end do
        end do
      end do ! ibox=1,nbox
    end do ! iphi=1,lphi
  end do ! imu=1,lmu
  call dumpn(qq,'qq','qq.dmp',1)
!!$omp end parallel

  if (do_newton) then
    do iz=izs,ize
    do iy=1,my
      prof = exp((y_newton-ym(iy))/dy_newton)
      prof = prof/(1.+prof)
      do ix=1,mx
        dedt(ix,iy,iz) = dedt(ix,iy,iz) - r(ix,iy,iz)*(ee(ix,iy,iz)-eetop)/t_newton*prof &
                                        - r(ix,iy,iz)*min(0.,ee(ix,iy,iz)-ee_min)/t_ee_min
      end do
    end do
    end do
  end if

  if (debug(dbg_cool) .and. omp_master) then; void = dtime(cput) &
    ; print '(1x,a8,2f7.3)','transfr',cput*omp_nthreads; endif

END SUBROUTINE
