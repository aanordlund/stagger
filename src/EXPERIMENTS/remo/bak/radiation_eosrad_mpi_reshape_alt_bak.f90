!...............................................................................
module cooling

  use params, only: mfile

  integer, parameter :: mmu = 9
  integer, parameter :: mphi = 10
  integer :: nmu, nphi
  integer :: verbose
  integer :: ny0
  integer :: lb0, ub0
  integer :: mblocks       ! DEPRECATED

  real :: dphi
  real :: form_factor
  real :: dtaumin, dtaumax, tauMin, tauMax
  real :: y_newton, dy_newton, t_newton, ee_newton, ee_min, t_ee_min

  real, dimension(mmu) :: mu0

  real, pointer, dimension(:,:,:) :: lnrkap, lnrkRoss, lnsrc, lnxx
  real, pointer, dimension(:,:,:) :: qq, qqr, qqscr, qpart
  real, pointer, dimension(:,:,:) :: tmpdata
  real, pointer, dimension(:,:,:) :: rk, lnrk, dtau, tau, s, q
  real, pointer, dimension(:,:,:) :: tmpdata1


  real,allocatable,target,dimension(:,:,:) :: scr_01,scr_02,scr_03,scr_04,scr_05
  real,allocatable,target,dimension(:,:,:) :: scr_06,scr_07

  real, allocatable, target, dimension(:,:,:) :: scr7,  scr8,  scr9


  real, allocatable, dimension(:)   :: yhydro, dyhydro, yrad, dyrad
  real, allocatable, dimension(:,:) :: surfi, surfi_tot

  logical do_table, do_newton, do_limb, do_yrad

  character(len=mfile) :: intfile

end module cooling

!...............................................................................
subroutine init_cooling (r,e)

  use params, only: file, do_cool, mx, my, mz, mfile, mid
  use arrays
  use cooling
  use reshape_mod, only: reshape_mode, sub_nx, sub_nz, dim_re

  implicit none

  character(len=mid) :: Id = 'init_cooling'

  real, dimension(mx,my,mz) :: r, e
  integer :: nx, ny, nz
  integer :: i            
  character(len=mfile) :: name

  i = index(file,'.')

  do_cool     = .true.
  nmu         =  0
  nphi        =  4
  dphi        =  0.
  form_factor =  1.
  dtaumax     =  300.
  dtaumin     =  0.1
  intfile     =  name('intensity.dat','int',file)
  do_newton   = .false.
  y_newton    = -0.3
  dy_newton   =  0.05
  t_newton    =  0.01
  t_ee_min    =  0.005
  ee_newton   = -1.
  ee_min      =  3.6
  do_limb     = .false.
  verbose     =  0
  ny0         =  0

  ! do_yrad: adaptive radiation depth scale?
  do_yrad     = .false.

  ! mblocks= number of subdomains communicated per side in trnslt routine;
  ! default: communicate only nearest neighbour
  ! taumin, taumax: limits for adaptive radiation scale (radiation_scale routine);

  mblocks     =  1
  tauMin      =  1.0e-6
  tauMax      =  5.0e2

  ! reshape_mod control variables: default values
  ! reshape_mode: mode of array reshaping
  ! sub_n[x,z]: number of subdivisions in the [x,z] directions
  reshape_mode = 'ymrg_xzspl'
  sub_nx       = 0
  sub_nz       = 0

  call read_cooling
  call reshape_init

  ! dimensions of reshaped arrays
  nx    = dim_re(1)
  ny    = dim_re(2)
  nz    = dim_re(3)

  ! allocate reshaped scratch arrays
  allocate( scr_01(nx,ny,nz), scr_02(nx,ny,nz), scr_03(nx,ny,nz) )
  allocate( scr_04(nx,ny,nz), scr_05(nx,ny,nz), scr_06(nx,ny,nz) )
  allocate( scr_07(nx,ny,nz) )

  ! allocate additional regular scratch arrays
  allocate(  scr7(mx,my,mz),  scr8(mx,my,mz),  scr9(mx,my,mz) )

  ! radiative transfer variables, regular arrays:
  !  qq:  heating rate per unit volume
  !  qqr: heating rate per unit mass
  !  qqscr: scratch array
  qq       => scr1 
  qqr      => scr2 
  qqscr    => scr3
  qpart    => scr4
  lnrkap   => scr5
  lnrkRoss => scr6
  lnsrc    => scr7
  lnxx     => scr8

  ! scratch array for temporary data (regular)
  tmpdata  => scr9


  ! radiative transfer variables, reshaped arrays:
  rk       => scr_01
  lnrk     => scr_02
  dtau     => scr_03
  tau      => scr_04
  s        => scr_05
  q        => scr_06

  ! scratch array for temporary data (reshaped)
  tmpdata1 => scr_07  

  ! allocate hydrodynamical and geometrical depth scales
  allocate( yhydro(ny), dyhydro(ny), yrad(ny), dyrad(ny) )

  ! allocate arrays for storing vertical outgoing intensities at the surface,
  ! partial and total
  allocate( surfi(nx,nz), surfi_tot(nx,nz) )

  ! define lb0 and ub0: limits of physical domain for reshaped arrays
  lb0 = 6
  ub0 = ny-5

end subroutine init_cooling

!...............................................................................
subroutine read_cooling

  use params
  use eos
  use cooling
  use reshape_mod, only: reshape_mode, sub_nx, sub_nz

  namelist /cool/ &
       do_cool, &
       nmu, &
       nphi, &
       dphi, &
       form_factor, &
       dtaumin, &
       dtaumax, &
       intfile, &
       do_newton, &
       y_newton, &
       dy_newton, &
       t_newton, &
       ee_newton, &
       ee_min, &
       t_ee_min, &
       do_limb, &
       mu0, &
       ny0, &
       do_yrad, &
       verbose, &
       mblocks, &
       tauMin, &
       tauMax, &
       reshape_mode, &
       sub_nx, &
       sub_nz

  character(len=mid) :: Id = 'read_cooling'
  character(len=mfile) :: name, fname

  call print_id(Id)

  rewind (stdin); read (stdin, cool)

  if (nmu==0 .and. form_factor==1.)  form_factor=0.4

  if (master) write (*,cool)

  if (do_limb) then
     fname = name('limb.dat','lmb',file)
     call mpi_name(fname)
     open (limb_unit, file=trim(fname), form='unformatted', status='unknown')
     write (limb_unit) mx, mz, nmu, nphi
  end if

end subroutine read_cooling

!...............................................................................
subroutine coolit (r, ee, lne, dd, dedt)

  use params
  use arrays, only: qqav, qqrav
  use units
  use eos
  use cooling
  use variables, only: e
  use reshape_mod, only: dim_re
  use table, only: Nlam

  implicit none

  character(len=mid) :: Id = 'coolit'

  real, dimension(mx,my,mz) :: r, ee, lne, dd, dedt
  real, dimension(mx,my,mz) :: tt

  real          :: xmu(mmu), wmu(mmu)
  real          :: sec, fdtime, dxdy, dzdy, tanth, f1, f2
  real          :: phi, wphi, womega

  integer       :: nx, ny, nz, lmu, lphi 
  integer       :: ix, iy, iz, imu, iphi, ilam, nymax
  integer       :: lrec, imaxval_mpi
  integer       :: mtauTop, mtauBot, ndtauTop, ndtauBot, ntop, nbot

  logical       :: debug
  logical       :: flag_scr, flag_snap, flag_limb, flag_surf, do_io, do_surfi
  logical, save :: first=.true.
  real          :: cput(2), void, dtime, prof
  integer, save :: nrad=0, nang=0

  character(len=mfile) :: name, fname

  external transfer

  ! do_cool=.false. : no radiative transfer
  if (.not. do_cool)  return

  ! consistent treatment of scattering?
  if (flag_scat) then
     if (master) print '(A)', &
          trim(Id)//': solver for scattering case not implemented yet. STOP.'
     call abort_mpi
     stop
  end if

  ! debug statements?
  if (debug(dbg_cool) .and. omp_master) then
     void = dtime(cput)
     print '(1x,a8,2f7.3)', 'rad0   ', cput * omp_nthreads
  end if

  ! reset dedt in ghost zones, if present (lb > 1)
  if (lb > 1) then 
     dedt(:,1:lb-1,izs:ize) = 0.
  end if

  ! reset dedt in ghost zones, if present (ub < my)
  if (ub < my) then 
     dedt(:,ub+1:my,izs:ize) = 0.
  end if

  !  Angular quadrature:
  !  nmu > 0: Use Gauss quadrature (mu=1 excluded), accuracy: 2*nmu   order
  !  nmu = 0: One vertical ray with weight 0.5
  !  nmu < 0: Use Radau quadrature (mu=1 included), accuracy: 2*nmu-1 order
  if      (nmu .gt. 0) then
     lmu = nmu
     call gausi (lmu,0.,1.,wmu,xmu)
  else if (nmu .eq. 0) then
     lmu = 1
     xmu(1) = 1.
     wmu(1) = 0.5
  else 
     lmu =-nmu
     call radaui(lmu,0.,1.,wmu,xmu)
  end if
  if (mu0(1) .ne. 0.) xmu=mu0

  if (first) then
     call barrier_omp ('rad-first')
     if (omp_master) first = .false.
     if (master) then
        print '(a4,a10,a8,a10)', 'imu', 'xmu', 'theta'
        do imu=1,lmu 
           print '(i4,f10.3,f8.1,f10.3)',imu,xmu(imu),acos(xmu(imu))*180./pi,wmu(imu)
        end do
     end if
  end if


  ! dimensions of reshaped data cubes for radiative transfer calculations
  nx  = dim_re(1)
  ny  = dim_re(2)
  nz  = dim_re(3)

  ! reshape (reconstruct) hydro depth scale yhydro and associated dyhydro
  call reshape_ymesh( ym,  yhydro)
  call reshape_ymesh(dym, dyhydro)


  ! lookup temperature and Rosseland extinction coefficient
  call temperature (r, ee, tt)
  call rosseland   (r, tt, lnrkRoss)

  ! reshape lnrkRoss -> lnrk, required by radiation_scale and tau_calc;
  call reshape_cube(lnrkRoss, lnrk)

  ! initialize extinction coeff rk (reshaped array)
  do iz=1,nz
     rk (:,:,iz) = 0. 
  end do

  ! initialize radiative heating rate qq (regular array)
  do iz=izs,ize
     qq (:,:,iz) = 0. 
  end do


  ! depth scale
  if (do_yrad) then 
     ! adaptive depth scale for radiative transfer
     call radiation_scale(yhydro, dyhydro, yrad, dyrad, mtauTop, mtauBot, .true.)
     nbot = mtauBot
  else
     ! radiation depth scale same as hydro depth scale
     call tau_calc(dyhydro, ny, 1.0, mtauTop, mtauBot, ndtauTop, ndtauBot)
     nbot = ndtauBot
  end if

  ! define nymax, max depth for tau_calc and radiative transfer (do_yrad=.false. case)
  if (ny0==0) then
     nbot = imaxval_mpi (nbot)
     nymax = nbot
  else
     nymax = ny0
  end if


  if (debug(dbg_cool) .and. omp_master) then
     void = dtime(cput)
     print '(1x,a8,2f7.3)', 'lookup ', cput * omp_nthreads
  end if


  ! loop over bins or wavelengths
  do ilam=1,Nlam

     !lookup lnxx (ln ratio of bin/monochromatic and Rosseland opacity)
     !lnsrc (ln source function * weight; simulation units)
     call lookup_rad (r, tt, 1, ilam, lnxx,  .false., .false.)
     call lookup_rad (r, tt, 2, ilam, lnsrc, .false., .false.)

     !compute ln extinction coefficient
     do iz=izs,ize
        lnrkap(:,:,iz) = lnrkRoss(:,:,iz) + lnxx(:,:,iz)
     end do

     ! loop over angles
     do imu=1,lmu

        if (nmu .eq. 0 .and. imu .eq. 1) then
           lphi = 1 
        else if (nmu .lt. 0 .and. imu .eq. lmu) then
           lphi = 1 
        else
           lphi = nphi
        end if

        tanth = tan(acos(xmu(imu)))
        wphi = 4.*pi/lphi

        do iphi=1,lphi

           phi = (iphi-1)*2.*pi/lphi + dphi*t
           dxdy = tanth*cos(phi)
           dzdy = tanth*sin(phi)

           if (master .and. verbose==1 .and. isubstep==1) then
              print '(a,3i5,2e15.7)',' imu,iphi,nymax,dxdy,dzdy=', &
                   imu,iphi,nymax,dxdy,dzdy
           end if

           ! flags for data output 
           flag_scr  = do_io(t+dt, tscr, iscr+iscr0,   nscr)
           flag_snap = do_io(t+dt, tsnap,isnap+isnap0, nsnap)

           ! flag_limb: limb darkening every scratch 
           ! or else every nsnap and only mu=1
           ! and only for first substep and bin:
           flag_limb = do_limb   .and. flag_scr
           flag_limb = flag_limb .or.  (xmu(imu)==1. .and. flag_snap)
           flag_limb = flag_limb .and. isubstep.eq.1 .and. ilam.eq.1

           ! compute surface intensity ?
           flag_surf = flag_scr .or. flag_snap
           flag_surf = flag_surf .and. xmu(imu)==1. .and. isubstep.eq.1 

           do_surfi  = flag_surf.or.flag_limb

           if (do_yrad) then

              ! solve radiative transfer on radiation depth scale

              ! trnslt: tilt lnrkap -> tmpdata (regular)
              ! reshape: tmpdata -> tmpdata1 (reshaped)
              ! yinter: interpolate to radiation depth scale; tmpdata1 -> lnrk
              ! tau_calc: compute optical depth; lnrk -> rk=exp(lnrk) 
              ! tau_calc: return ndtauTop (-> ntop) used by transfer routine 

              call trnslt(lnrkap, tmpdata, dxdy, dzdy, .false., my, mblocks, verbose)
              call reshape_cube(tmpdata, tmpdata1)
              call yinter(ny, yhydro, ny, yrad, nx, nz, tmpdata1, lnrk)
              call tau_calc(dyrad, ny, xmu(imu), mtauTop, mtauBot, ndtauTop, ndtauBot)

              ntop = ndtauTop
              nbot = ny

              ! trnslt: tilt source function lnsrc -> tmpdata
              ! reshape: tmpdata -> tmpdata1
              ! yinter: interpolate to radiation depth scale; tmpdata -> s
              ! exponentiate: s -> exp(s)

              call trnslt(lnsrc, tmpdata, dxdy, dzdy, .false., my, mblocks, verbose)
              call reshape_cube(tmpdata, tmpdata1)
              call yinter(ny, yhydro, ny, yrad, nx, nz, tmpdata1, s)
              do iz=1,nz
                 s(:,:,iz) = exp( s(:,:,iz) )
              end do

              ! transfer: solve raditative transfer equation
              ! dtau= optical depth step
              ! s   = source function
              ! q   = J-S split

              ! note: nbot = ny
              call transfer (nx, ny, nz, nbot, ntop, dtau, s, q, surfi, do_surfi )

              ! compute partial heating rate per ray per opacity bin
              ! note: nbot = ny
              do iz=1,nz
                 q(:,1,iz) = q(:,1,iz) * rk(:,1,iz)
                 do iy=2,nbot-1
                    f1 = 1. / (yrad(iy+1)-yrad(iy-1))
                    f2 = xmu(imu) * f1
                    q(:,iy,iz) = q(:,iy,iz) * (dtau(:,iy+1,iz)+dtau(:,iy,iz)) * f2
                 end do
                 q(:,nbot,iz) = q(:,nbot,iz) * rk(:,nbot,iz)
              end do
              ! set q to zero below iy=nbot
              ! note: this part should be handled internally by transfer (using diffusion approx)
              if (nbot < ny) then
                 do iz=1,nz
                    do iy=nbot+1,ny
                       q(:,iy,iz) = 0.
                    end do
                 end do
              end if

              ! ydistr: reset tmpdata1, distribute q back to hydro depth scale, q -> tmpdata1
              ! deshape: tmpdata1 -> tmpdata (regular)
              ! trnslt: tilt tmpdata back to vertical grid, tmpdata -> qpart (regular)

              call ydistr(ny, yrad, ny, yhydro, nx, nz, q, tmpdata1)
              call deshape_cube(tmpdata1, tmpdata)
              call trnslt(tmpdata, qpart, -dxdy, -dzdy, .false., my, mblocks, verbose)

           else

              ! solve radiative transfer on hydro depth scale

              ! trnslt: tilt lnrkap -> tmpdata (regular)
              ! reshape: tmpdata -> lnrk (reshaped)
              ! tau_calc: compute optical depth; lnrk -> rk=exp(lnrk) 
              ! tau_calc: return ndtauTop (-> ntop) used by transfer routine 
              ! find nbot, used by trnslt routine

              call trnslt(lnrkap, tmpdata, dxdy, dzdy, .false., my, mblocks, verbose)
              call reshape_cube(tmpdata, lnrk)
              call tau_calc(dyhydro, nymax, xmu(imu), mtauTop, mtauBot, ndtauTop, ndtauBot)

              ntop = ndtauTop
              if (ny0==0) then
                 nbot = imaxval_mpi (ndtauBot)
              else
                 nbot = ny0
              end if

              ! trnslt: tilt source function, exponentiate; lnsrc -> tmpdata (regular)
              ! reshape: tmpdata -> s (reshaped)
              call trnslt(lnsrc, tmpdata, dxdy, dzdy, .true., my, mblocks, verbose)
              call reshape_cube(tmpdata, s)

              ! transfer: solve raditative transfer equation
              ! dtau= optical depth step
              ! s   = source function
              ! q   = J-S split

              call transfer (nx, ny, nz, nbot, ntop, dtau, s, q, surfi, do_surfi)

              ! compute partial heating rate per ray per opacity bin
              do iz=1,nz
                 q(:,1,iz) = q(:,1,iz) * rk(:,1,iz)
                 do iy=2,nbot-1
                    f2 = xmu(imu) / (yhydro(iy+1)-yhydro(iy-1))
                    q(:,iy,iz) = q(:,iy,iz) * (dtau(:,iy+1,iz)+dtau(:,iy,iz)) * f2
                 end do
                 q(:,nbot,iz) = q(:,nbot,iz) * rk(:,nbot,iz)
              end do
              ! set q to zero below iy=nbot
              ! note: this part should be handled internally by transfer (using diffusion approx)
              if (nbot < ny) then
                 do iz=1,nz
                    do iy=nbot+1,ny
                       q(:,iy,iz) = 0.
                    end do
                 end do
              end if

              ! trnslt: tilt back to vertical grid, tmpdata -> qpart
              call deshape_cube(q, tmpdata)
              call trnslt(tmpdata, qpart, -dxdy, -dzdy, .false., my, mblocks, verbose)

           end if  ! do_yrad

           ! add contribution to radiative heating rate
           f1 = wmu(imu) * wphi * form_factor
           do iz=izs,ize
              do iy=1,my
                 qq(:,iy,iz) = qq(:,iy,iz) + qpart(:,iy,iz) * f1
              end do
           end do

           ! limb darkening, output           
           if (flag_limb) then
              call barrier_omp('limb')
              if (do_limb .and. omp_master) then
                 write(limb_unit) t,imu,iphi,xmu(imu),phi,surfi
                 print *,omp_mythread,isubstep,ilam,imu,iphi
              end if
           end if

           ! integrated surface intensity
           if (flag_surf) then
              if (ilam .eq. 1) then      ! overwrite surface intensity array
                 do iz=1,nz
                    surfi_tot(:,iz) = surfi(:,iz)
                 end do
              else                       ! add to surface intensity array
                 do iz=1,nz
                    surfi_tot(:,iz) = surfi_tot(:,iz) + surfi(:,iz)
                 end do
              end if
           end if

        end do !iphi
     end do !imu
  end do !ilam


  ! make energy per unit volume e consistent with r and ee ?
  ! no: handled internally by lookup_eos subroutine (invoked by temperature subroutine)
  do iz=izs,ize
     e(:,:,iz) = ee(:,:,iz) * r(:,:,iz)
  end do

  ! energy equation: add contribution of radiative heating rate per unit volume
  do iz=izs,ize
     dedt(:,:,iz) = dedt(:,:,iz) + qq(:,:,iz)
  end do

  ! compute (average) radiative flux
  call radiative_flux (qq, qqscr)

  ! compute radiative heating rates per unit mass
  do iz=izs,ize
     qqr(:,:,iz) = qq(:,:,iz) / r(:,:,iz)
  end do

  ! compute horizontally averaged radiative heating rates 
  ! per unit volume and per unit mass

  call haverage_subr(qq,  qqav)
  call haverage_subr(qqr, qqrav)

  ! reconstruct surface intensity array: surfi_tot -> surface_int (params module)
  call merge_surf(surfi_tot, surface_int)


  ! Newtonian cooling?
  if (do_newton) then
     if (ee_newton > 0) eetop = ee_newton
     do iz=izs,ize
        do iy=1,my
           prof = exp(min((y_newton-ym(iy))/dy_newton,70.))
           prof = prof/(1.+prof)
           do ix=1,mx
              dedt(ix,iy,iz) = dedt(ix,iy,iz) &
                   - r(ix,iy,iz)*(ee(ix,iy,iz)-eetop)/t_newton*prof   &
                   - r(ix,iy,iz)*min(0.,ee(ix,iy,iz)-ee_min)/t_ee_min
           end do
        end do
     end do
  end if

  if (debug(dbg_cool) .and. omp_master) then
     void = dtime(cput)
     print '(1x,a8,2f7.3)', 'transfr', cput * omp_nthreads
  end if

end subroutine coolit

!...............................................................................
