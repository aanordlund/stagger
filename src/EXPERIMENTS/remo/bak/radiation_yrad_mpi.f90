! $Id: radiation_yrad_mpi.f90,v 1.1 2016/10/12 10:03:35 remo Exp $
!***********************************************************************
MODULE cooling
  USE params
  integer, parameter:: mmu=9, mphi=10
  real dphi, form_factor, dtaumin, dtaumax, taumin, taumax
  integer nmu, nphi, verbose, ny0
  integer mblocks							!maximum number of subdomains considered (per side) to fill the ghost zones
  integer n1, n2
  real y_newton, dy_newton, t_newton, ee_newton, ee_min, t_ee_min
  real, dimension(mmu):: mu0

  real, pointer, dimension(:,:,:):: rk,lnrk,s,q,dtau,qq

  logical do_table, do_newton, do_limb
  character(len=mfile):: intfile
END MODULE

!***********************************************************************
SUBROUTINE init_cooling (r,e)
  USE params
  USE arrays
  USE cooling
  implicit none
  real, dimension(mx,my,mz):: r,e
  integer i
  character(len=mfile):: name

  if (mpi_ny > 1) then
    if (master) print*,'ERROR: this RT version needs mpi_ny=1'
    call abort_mpi
  end if

  do_cool = .true.
  i = index(file,'.')
  nmu = 0
  nphi = 4
  dphi = 0.
  form_factor = 1.
  dtaumax = 100.
  dtaumin = 0.1
  intfile = name('intensity.dat','int',file)

  do_newton = .false.
  y_newton = -0.3
  dy_newton = 0.05
  t_newton = 0.01
  t_ee_min = 0.005
  ee_newton = 5.265
  ee_min = 3.6
  do_limb  = .false.
  verbose = 0
  ny0 = 0
  mblocks = 1                                                           !default: communicate only nearest neighbour

  taumin = 1.0e-6                                                       !taumin and taumax used by radiation_scale routine
  taumax = 2.0e2

  call read_cooling
  rk   => scr1
  lnrk => scr2
  s    => scr3
  q    => scr4
  dtau => scr5
  qq   => scr6
END SUBROUTINE

!***********************************************************************
SUBROUTINE read_cooling
  USE params
  USE eos
  USE cooling
  namelist /cool/do_cool, nmu, nphi, dphi, form_factor, dtaumin, dtaumax, intfile, &
    do_newton, y_newton, dy_newton, t_newton, ee_newton, ee_min, t_ee_min, do_limb, mu0, &
    ny0, verbose, mblocks,taumin,taumax
  character(len=mfile):: name, fname

  character(len=mid):: id='$Id: radiation_yrad_mpi.f90,v 1.1 2016/10/12 10:03:35 remo Exp $'
  call print_id(id)

  rewind (stdin); read (stdin,cool)
  if (nmu==0 .and. form_factor==1.) form_factor=0.4
  if (master) write (*,cool)
  if (master) write (*,*) 'mbox, dbox =', mbox, dbox
  if (do_limb) then
    fname = name('limb.dat','lmb',file)
    call mpi_name(fname)
    open (limb_unit, file=trim(fname), &
      form='unformatted', status='unknown')
    write (limb_unit) mx, mz,nmu,nphi
  end if
END SUBROUTINE

!***********************************************************************
SUBROUTINE coolit (r, ee, lne, dd, dedt)
!
!  Radiative cooling
!
  USE params
  !USE variables
  USE arrays, only: lns
  USE units
  USE eos
  USE cooling
  implicit none
  real, dimension(mx,my,mz):: r, ee, lne, dd, dedt
  real xmu(mmu), wmu(mmu)
  integer ix, iy, iz, lmu, lphi, imu, iphi, ibox, n1p, n2p, ny
  real sec, fdtime, dxdy, dzdy, tanth, qq1, f1, f2, dtau2
  real phi, wphi, womega
  integer lrec, imaxval_mpi, nyr
  logical flag, do_io
  logical, save:: first=.true.
  external transfer
  logical debug
  real cput(2), void, dtime, prof
  integer, save:: nrad=0, nang=0
  character(len=mfile):: name, fname

  integer ntau1,ntau2
  real, dimension(my) :: yrad, dyraddn
  real, dimension(mx,my,mz) :: rk1,s1,dtau1,tau1
!
  if (.not. do_cool) return

  if (debug(dbg_cool) .and. omp_master) then; void = dtime(cput) &
    ; print '(1x,a8,2f7.3)','rad0   ',cput*omp_nthreads; endif

  if (lb > 1) then                                                      ! if we have ghost zones
    dedt(:,1:lb-1,izs:ize) = 0.                                         ! zap previous dedt there
  end if

!-----------------------------------------------------------------------
!  Angular quadrature
!  nmu > 0: Use Gauss integration (mu=1 excluded) with 2*nmu   order accuracy
!  nmu = 0: One vertical ray with weight 0.5
!  nmu < 0: Use Radau integration (mu=1 included) with 2*nmu-1 order accuracy
!-----------------------------------------------------------------------
  if      (nmu .gt. 0) then
    lmu = nmu
    call gausi (lmu,0.,1.,wmu,xmu)
  else if (nmu .eq. 0) then
    lmu = 1
    xmu(1) = 1.
    wmu(1) = 0.5
  else 
    lmu =-nmu
    call radaui(lmu,0.,1.,wmu,xmu)
  end if
  if (mu0(1) .ne. 0.) xmu=mu0

  if (first) then
    call barrier_omp ('rad-first')
    if (omp_master) first = .false.
    if (master) then
      print *,' imu       xmu    theta'
      !           1.....0.000.....00.0
      do imu=1,lmu 
        print '(i4,f10.3,f8.1,f10.3)',imu,xmu(imu),acos(xmu(imu))*180./pi,wmu(imu)
      end do
    end if
  end if

!-----------------------------------------------------------------------
!  Equation of state and opacity
!-----------------------------------------------------------------------
  call lookup (r,ee,lnrk,lns)

  do iz=izs,ize
    rk(:,:,iz)=0. !exp(lnrk(:,:,iz))
  enddo
  call radiation_scale(yrad,dyraddn,ntau1,ntau2)
  n2=ntau2


  if (ny0==0) then
    n2 = imaxval_mpi (n2)
    ny  = n2
  else
    ny = ny0
  end if

  call dumpn (r,'r','rad.dmp',0)
  call dumpn (ee,'ee','rad.dmp',1)
  call dumpn (lnrk,'lnrk','rad.dmp',1)
  do ibox=1,mbox
    call dumpn (lns(:,:,:,ibox),'lns','rad.dmp',1)
  end do

  if (debug(dbg_cool) .and. omp_master) then; void = dtime(cput) &
    ; print '(1x,a8,2f7.3)','lookup ',cput*omp_nthreads; endif

!
! initialize radiative heating rate
!
  do iz=izs,ize
    qq(:,:,iz) = 0.
  end do

  call dumpn (lnrk,'lnrk','trf.dmp',0)


  do imu=1,lmu
    if (nmu .eq. 0 .and. imu .eq. 1) then
      lphi = 1 
    else if (nmu .lt. 0 .and. imu .eq. lmu) then
      lphi = 1 
    else
      lphi = nphi
    end if
    tanth = tan(acos(xmu(imu)))
    wphi = 4.*pi/lphi

    do iphi=1,lphi
      phi = (iphi-1)*2.*pi/lphi + dphi*t
      dxdy = tanth*cos(phi)
      dzdy = tanth*sin(phi)
      !if (master .and. verbose==1 .and. isubstep==1) &
      !  print '(a,3i5,2e15.7)',' imu,iphi,ny,dxdy,dzdy=', &
      !	                         imu,iphi,ny,dxdy,dzdy

      call trnslt (lnrk, rk, dxdy, dzdy, .false., ny, mblocks, verbose)
      call dumpn (rk,'rk','trf.dmp',1)
!
! interpolate ln extinction coefficient onto radiation scale; exponentiate later in do-loop
!
      call yinter(my,ym,my,yrad,mx,mz,rk,rk1)

      do ibox=1,mbox
         !$omp barrier
         !$omp single
         n1 = 2
         !! n2 = 2
         !$omp end single
         n1p = 2
         !! n2p = 2
         if (ibox.eq.1) then
            f1 = (0.5/xmu(imu))
            do iz=izs,ize
               rk1(:,1,iz)   = exp(rk1(:,1,iz))
               dtau1(:,1,iz) = dyraddn(1)*rk1(:,1,iz)/xmu(imu)
               tau1(:,1,iz)  = dtau1(:,1,iz) 
               do iy=2,my
                  rk1(:,iy,iz) = exp(rk1(:,iy,iz))
                  do ix=1,mx
                     dtau1(ix,iy,iz) = f1*dyraddn(iy)*(rk1(ix,iy-1,iz)+rk1(ix,iy,iz))
                     tau1(ix,iy,iz)  = tau1(ix,iy-1,iz)+dtau1(ix,iy,iz) 
                     if (dtau1(ix,iy,iz) < dtaumin) n1p = max(n1p,iy)
                     !if (dtau1(ix,iy,iz) < dtaumax) n2p = max(n2p,iy)
                  end do
               end do
            end do
         else
            do iz=izs,ize
               do iy=1,my
                  do ix=1,mx
                     dtau1(ix,iy,iz)= (10.**dbox)*dtau1(ix,iy,iz)
                     tau1(ix,iy,iz) = (10.**dbox)*tau1(ix,iy,iz)
                     if (dtau1(ix,iy,iz) < dtaumin) n1p = max(n1p,iy)
                     !if (dtau1(ix,iy,iz) < dtaumax) n2p = max(n2p,iy)
                  end do
               end do
           end do
        end if
        call dumpn (dtau1,'dtau','trf.dmp',1)

        !$omp critical
        n1=max(n1p,n1)
        !$omp end critical

        !if (ny0==0) then           
        !   !$omp critical
        !   n1 = max(n1p,n1)
        !   n2 = max(n2p,n2)
        !   !$omp end critical
        !   !$omp barrier
        !   n2 = imaxval_mpi (n2)
        !else
        !   n2 = ny0
        !end if

	!nrad = nrad + n2
	!nang = nang + 1
        !if (master .and. verbose==2 .and. isubstep==1) then
	!  nyr = (nrad+0.5)/nang
        !  print '(a,7i5,1p(2e15.7))',' imu,iphi,ibox,ny,n1,n2,nyr,dxdy,dzdy=', &
	!                               imu,iphi,ibox,ny,n1,n2,nyr,dxdy,dzdy
        !end if

!-----------------------------------------------------------------------
!  Source function
!-----------------------------------------------------------------------

!
! tilt cube with ln source function; don't exponentiate yet
!
        call trnslt (lns(:,:,:,ibox), s, dxdy, dzdy, .false., ny, mblocks, verbose)

!
! interpolate ln source function onto radiation scale; then exponentiate
!
        call yinter(my,ym,my,yrad,mx,mz,s,s1)

        do iz=izs,ize
           !s(:,:,iz)  = exp(s(:,:,iz))
           s1(:,:,iz) = exp(s1(:,:,iz))
        end do

        call dumpn (s1,'s','trf.dmp',1)

!-----------------------------------------------------------------------
!  Raditative transfer equation
!-----------------------------------------------------------------------
        flag = do_limb .and. do_io(t+dt,tscr,iscr+iscr0,nscr)                         ! limb darkening every scratch
        flag = flag .or. (xmu(imu)==1. .and. do_io(t+dt,tsnap,isnap+isnap0,nsnap))    ! or else every nsnap and only mu=1
        flag = flag .and. isubstep.eq.1 .and. ibox.eq.1                               ! and only for first substep and bin

!
! solve radiative transfer equation; mind that s1, dtau1, and q1 are at this stage 
! all defined on the radiation scale
!
        !call transfer (n2,n1,dtau,s1,q,surface_int,flag)
        call transfer (my,n1,dtau1,s1,q,surface_int,flag)

        if (flag) then
          call barrier_omp('limb')
          if (do_limb .and. omp_master) then
            write(limb_unit) t,imu,iphi,xmu(imu),phi,surface_int
            print *,omp_mythread,isubstep,ibox,imu,iphi
          end if
        end if
        call dumpn(q,'q0','trf.dmp',1)

        f1 = 10.**(ibox-1)
        do iz=izs,ize
          do iy=2,my-1
            f2 = xmu(imu)/(yrad(iy+1)-yrad(iy-1))
            q(:,iy,iz) = q(:,iy,iz)*(dtau1(:,iy+1,iz)+dtau1(:,iy,iz))*f2
          end do
          q(:,1,iz) = q(:,1,iz)*rk1(:,1,iz)*f1
        end do

        call dumpn(q,'q1','trf.dmp',1)

!
! distribute back to original depth-scale ym
!
        do iz=1,mz
           !s1(:,:,iz)=0.0 !s1 reset by ydistr
           s(:,:,iz)=0.0
        enddo
        call ydistr(my,yrad,my,ym,mx,mz,q,s1)
!
! note that s1 now contains the (partial) heating rate defined 
! on the original depth scale !

!-----------------------------------------------------------------------
!  Energy equation
!-----------------------------------------------------------------------
        call trnslt (s1, s, -dxdy, -dzdy, .false., ny, mblocks, verbose)

!
! note that s now contains the (partial) heating rate defined 
! on the original (=tilted back) grid !

        call dumpn(s,'q2','trf.dmp',1)

        f1 = wmu(imu)*wphi*form_factor
        do iz=izs,ize
        do iy=1,ny
        do ix=1,mx
          qq1 = f1*s(ix,iy,iz)
          qq(ix,iy,iz) = qq(ix,iy,iz) + qq1
          dedt(ix,iy,iz) = dedt(ix,iy,iz) + qq1
        end do
        end do
        end do
      end do ! ibox=1,nbox
    end do ! iphi=1,lphi
  end do ! imu=1,lmu
  call dumpn(qq,'qq','qq.dmp',1)

!!$omp end parallel

  call radiative_flux (qq,q)

  if (do_newton) then
    do iz=izs,ize
    do iy=1,my
      prof = exp((y_newton-ym(iy))/dy_newton)
      prof = prof/(1.+prof)
      do ix=1,mx
        dedt(ix,iy,iz) = dedt(ix,iy,iz) - r(ix,iy,iz)*(ee(ix,iy,iz)-eetop)/t_newton*prof &
                                        - r(ix,iy,iz)*min(0.,ee(ix,iy,iz)-ee_min)/t_ee_min
      end do
    end do
    end do
  end if

  if (debug(dbg_cool) .and. omp_master) then; void = dtime(cput) &
    ; print '(1x,a8,2f7.3)','transfr',cput*omp_nthreads; endif

END SUBROUTINE

!***********************************************************************
SUBROUTINE radiation_scale(yrad,dyraddn,m1,m2)

  USE params
  USE cooling

  real    :: f1,dlnrkmax,drkmax
  integer :: m1,m2,m1p,m2p,iy,iz
  real, dimension(my) :: indx,fndx,sfndx
  real, dimension(my) :: yrad,dyraddn 
  real, allocatable, dimension(:,:,:) :: rk2,dtau2,tau2
  real  :: taumin2,taumax2
  logical :: do_smooth

  do_smooth=.true.

  allocate(rk2(mx,my,mz),dtau2(mx,my,mz),tau2(mx,my,mz))

  !$omp barrier
  !$omp single
  m1  = 1
  m2  = 2
  !$omp end single
  m2p = 2

  f1=0.5
  do iz=izs,ize
     rk2(:,1,iz)  = exp(lnrk(:,1,iz))
     dtau2(:,1,iz)= dymdn*rk2(:,1,iz)
     tau2(:,1,iz) = dtau2(:,1,iz) 
     do iy=2,my
        do ix=1,mx
           rk2(ix,iy,iz)=exp(lnrk(ix,iy,iz))
           !dtau2 = 0.5*dymdn(iy)*(exp(lnrk(ix,iy-1,iz))+exp(lnrk(ix,iy,iz)))
           dtau2(ix,iy,iz) = f1*dymdn(iy)*(rk2(ix,iy-1,iz)+rk2(ix,iy,iz))
           tau2(ix,iy,iz)  = tau2(ix,iy-1,iz) + dtau2(ix,iy,iz)
           !if (dtau2 < dtaumax) m2p = max(m2p,iy)
        end do
     end do
  end do

  !$omp critical
  do iy=1,my
     taumin2 = minval(tau2(:,iy,:))
     taumax2 = maxval(tau2(:,iy,:))
     if (taumax2 < taumin) m1=iy
     if (taumin2 < taumax) m2=iy
  enddo
  m2 = max(lb+2,m2)
  m1 = max(lb,m1)
  !! m2  = max(m2p,m2)
  !! m1  = min(m2-2,lb)
  
  !$omp end critical
  !$omp barrier


  fndx(:)  = 0.0
  fndx(m1) = 0.0
  do iy=m1+1,m2
     dlnrkmax=maxval(abs(lnrk(:,iy,:)-lnrk(:,iy-1,:)))
     fndx(iy)=fndx(iy-1)+sqrt(dlnrkmax)
  enddo

!
! Array with indexes for radiation scale yrad
!
  indx(1) = 1.0
  do iy=2,my
     indx(iy)=indx(iy-1)+1.0
  enddo


!
! Smooth fndx
!
  sfndx(:)  = 0.0
  sfndx(m1) = fndx(m1)
  if (do_smooth) then
     do iy=m1+1,m2-1
        sfndx(iy) = 0.5*fndx(iy) + &
             0.5*((fndx(iy+1)-fndx(iy-1))/(ym(iy+1)-ym(iy-1))* &
             (ym(iy)-ym(iy-1)) + fndx(iy-1))
     enddo
  else
     do iy=m1+1,m2-1
        sfndx(iy) = fndx(iy)
     enddo
  endif
  sfndx(m2) = fndx(m2)

!
! Normalize fndx array to span 1...my range
!
  drkmax = real(my-1)/fndx(m2)
  do iy=m1,m2
     fndx(iy) = 1.0 + drkmax*sfndx(iy)
  enddo

!
! Compute radiation scale yrad and dyraddn(i)=yrad(i)-yrad(i-i) array
!
  call interp(m2-m1+1,fndx(m1:m2),ym(m1:m2),my,indx,yrad,2,2)
  dyraddn(1)=yrad(2)-yrad(1)
  do iy=2,my
     dyraddn(iy)=yrad(iy)-yrad(iy-1)
  end do

  deallocate(rk2,dtau2,tau2)

END SUBROUTINE radiation_scale

!***********************************************************************
SUBROUTINE yinter (ny,y,nt,yt,nx,nz,f,g)
!
!  Interpolate to a new y-grid.
!
!  Update history:
!
!  22-dec-87/aake:  /czintr/ added to control extrapolation (not used
!                   in simulation code, to be used in model resizing.
!  11-jan-89/aake:  nx,ny -> mx,my in set to zero loop (nx,ny undefined)
!  09-aug-89/aake;  added block data routine
!  09-aug-89/aake;  /cscr/ instead of blank common
!  22-aug-89/bob:   (l,m,n)->(lm,n)
!
!***********************************************************************
!
  
  integer                   :: nx,ny,nz,nt
  real, dimension(ny)       :: y
  real, dimension(nt)       :: yt
  real, dimension(nx,ny,nz) :: f,d
  real, dimension(nx,nt,nz) :: g
  real, parameter           :: iextr1=0
  real, parameter           :: iextrn=0
  real, parameter           :: eps=1.0e-4             
! If closer than eps to grid point, don't interpolate
  
  real    :: dy,py,qy, pyf,pyd,qyd
  integer :: n,k,iz

!
!  Get derivatives of f
!
  call ydery(ny,y,nx,nz,f,d)
!
!  Depth loop
!
  n=2
  do k=1,nt
!
!  Find n such that  y(n-1) < yt(k) < y(n)
!
101  if (yt(k).gt.y(n)) then
        n=n+1
        if (n.gt.ny) then
           !nt=k-1
           goto 100
        endif
        goto 101
     endif
!
!  Relative distances, check extrapolations
!
     dy=(y(n)-y(n-1))
     py=(yt(k)-y(n-1))/dy
     qy=1.-py

     if (py.lt.-eps) then

        if (iextr1.eq.0) then                ! Set to zero outside
           do iz=1,nz
              g(:,k,iz)=0.0
           enddo
           goto 100
        else if (iextr1.eq.1) then           ! Set to constant outside
           py=0.0
           qy=1.0
        endif

     else if (qy.lt.-eps) then

        if (iextrn.eq.0) then                ! Set to zero outside
           do iz=1,nz
              g(:,k,iz)=0.0
           enddo
           goto 100
        else if (iextrn.eq.1) then           ! Set to constant outside
           py=1.0
           qy=0.0
        endif

     endif


     if (abs(py).lt.eps) then                !  Close to one end ?
        do iz=1,nz
           g(:,k,iz)=f(:,n-1,iz)
        enddo
     else if (abs(qy).lt.eps) then
        do iz=1,nz
           g(:,k,iz)=f(:,n,iz)
        enddo
  
        
     else                                   !  Interpolate
        pyf=py-(qy*py)*(qy-py)
        pyd=-py*(qy*py)*dy
        qyd=qy*(qy*py)*dy
        do iz=1,nz
           g(:,k,iz)=f(:,n-1,iz)+pyf*(f(:,n,iz)-f(:,n-1,iz))+qyd*d(:,n-1,iz)+pyd*d(:,n,iz)
        enddo
     endif

  enddo   !END Y-LOOP

100  continue
     
END SUBROUTINE yinter


!***********************************************************************
SUBROUTINE ydery (ny,y,nx,nz,f,d)
!
!  The following LU decomposition of the problem was calculated by the
!  startup routine:
!
!  | w2(1) w3(1)              |   |d|   |  1                    |   |e|
!  |       w2(2) w3(2)        |   |d|   | w1(2)  1              |   |e|
!  |          .     .         | * |.| = |       w1(3)  1        | * |.|
!  |                .      .  |   |.|   |              .  .     |   |.|
!  |                    w2(N) |   |d|   |                 w1  1 |   |e|
!
!  The solutions of all nx*ny equations are carried out in paralell.
!
!  4m+1d+4a = 9 flops per grid point, running at approx 1M grid pts/sec
!  on the Univ of Colo Alliant FX-8.
! 
!  Update history:
!                      .
!  20-aug-87: Coded by Ake Nordlund, Copenhagen University Observatory
!  09-nov-87: zderz0() split off as separate routine
!  27-sep-87: Removed bdry[123] parameters.
!  02-oct-87: Moved factor 3 from zder loop to zder0 loop.
!  15-oct-87: Factors a and b moved out of lm-loops 121. 13->9 flops.
!  03-nov-87: zt parameter with subroutine zderz
!  04-nov-87: inverted w2(), to turn divides into multiplications
!  14-sep-88: combined loops l,m->lm
!  20-jun-90: cubic-spline version
!  spring-91: cubic at lower, finite diff at upper boundary
!  08-may-91: combined all zder routines in one file
!
!***********************************************************************
!
   integer                   :: nx,nynz
   real, dimension(ny)       :: y
   real, dimension(3,ny)     :: w
   real, dimension(nx,ny,nz) :: f,d
   integer                   :: k,iz
   real                      :: a,b,cya,cyb
   real, dimension(nx)       :: dfa,dfb
!
!  Check if we have a degenerate case (no z-extension)
!
   if (ny.eq.1) then
      do iz=1,nz
         d(:,1,nz)=0.0
      enddo
      return
   endif

   call ydery0(ny,y,w)

!
!  First point
!
   do iz=1,nz
      d(:,1,iz)=(f(:,2,iz)-f(:,1,iz))/(y(2)-y(1))
   enddo
!
!  Interior points [2m+2s]
!
   do k=2,ny-1
      a=1./(y(k+1)-y(k))**2
      b=1./(y(k)-y(k-1))**2
      do iz=1,nz
         d(:,k,iz)=(f(:,k+1,iz)-f(:,k,iz))*a+(f(:,k,iz)-f(:,k-1,iz))*b
      enddo
   enddo
!
!  Last point
!
   cya=1./(y(ny-1)-y(ny-2))
   cyb=1./(y(ny)-y(ny-1))
   do iz=1,nz
      dfa(:)=f(:,ny-1,iz)-f(:,ny-2,iz)
      dfb(:)=f(:,ny,iz)-f(:,ny-1,iz)
      d(:,ny,iz)=2.*(dfa(:)*cya*cya*cya-dfb(:)*cyb*cyb*cyb)
   enddo
!
!  Do the forward substitution [1m+1a]
!
   do iz=1,nz
      d(:,ny,iz)=d(:,ny,iz)+w(3,ny)*d(:,ny-1,iz)
   enddo

   do k=2,ny
      do iz=1,nz
         d(:,k,iz)=d(:,k,iz)+w(1,k)*d(:,k-1,iz)
      enddo
   enddo
!
!  Do the backward substitution [1m+1d+1s]
!
   do iz=1,nz
      d(:,ny,iz)=d(:,ny,iz)*w(2,ny)
   enddo

   do k=ny-1,1,-1
      do iz=1,nz 
         d(:,k,iz)=(d(:,k,iz)-w(3,k)*d(:,k+1,iz))*w(2,k)
      enddo
   enddo
!
 END SUBROUTINE ydery
!***********************************************************************

SUBROUTINE ydery0 (ny,y,w)
!
!  Calculates the matrix elements and an LU decomposition of the problem
!  of finding the derivatives of a cubic spline with continuous second
!  derivatives.
!
!  This particular version (to be used with the hd/mhd code with fixed
!  boundary values at a fiducial extra boundary layer) has end point
!  derivatives equal to one sided first order derivatives.  These are
!  not used directly, and influence the solution only through the con-
!  dition of a continuous second derivative at the next point in.
!
!  To make a "natural spline" (zero second derivative at the end pts),
!  change the weights to
!
!     w2(1)=2./3.
!     w3(1)=1./3.
!     w2(nz)=2./3.
!     w1(nz)=1./3.
!
!  For a general cubic spline, we have an equation system of the form:
!
!     | w2 w3          |   | d |     | e |
!     | w1 w2 w3       |   | d |     | e |
!     |       .  .     | * | . |  =  | . |
!     |       .  .  .  |   | . |     | . |
!     |          w1 w2 |   | d |     | e |
!
!  This may be solved by adding a fraction (w1) of each row to the
!  following row.
!
!  This corresponds to an LU decomposition of the problem:
!
!     | w2 w3          |   | d |     | 1               |   | e |
!     |    w2 w3       |   | d |     | w1  1           |   | e |
!     |       .  .     | * | . |  =  |    w1  1        | * | . |
!     |          .  .  |   | . |     |        .  .     |   | . |
!     |             w2 |   | d |     |           w1  1 |   | e |
!
!  In this startup routine, we calculate the fractions w1.
!  These are the same for all splines with the same z-scale,
!  and only have to be calculated once for all nx*ny splines.
!
!  In principle, this startup routine only has to be called once,
!  but to make the zder entry selfcontained, zder0 is called from
!  within zder.  In practice, the zder0 time is insignificant.
! 
!  Update history:
!                      .
!  20-aug-87: coded by Ake Nordlund, Copenhagen University Observatory
!  27-sep-87: Removed bdry[123] parameters.
!  02-oct-87: Moved factor 3 from zder loop to zder0 loop.
!  04-nov-87: inverted w2(), to turn divides into multiplications
!  09-nov-87: zderz0() split off as separate routine
!  spring-91: cubic at lower, finite diff at upper boundary
!  08-may-91: combined all zder routines in one file
!
!***********************************************************************
!
  integer               :: ny
  real, dimension(ny)   :: y
  real, dimension(3,ny) :: w 
  integer               :: k
  real                  :: a3,cya,cyb,c

!
!  First point
!
  w(1,1)=0.0
  w(2,1)=1.0
  w(3,1)=0.0
!
!  Interior points
!
  a3=1./3.
  do k=2,ny-1
     w(1,k)=a3/(y(k)-y(k-1))
     w(3,k)=a3/(y(k+1)-y(k))
     w(2,k)=2.*(w(1,k)+w(3,k))
  enddo
!
!  Last point
!
  cya=1./(y(ny-1)-y(ny-2))
  cyb=1./(y(ny)-y(ny-1))
  w(1,ny)=cya*cya
  w(3,ny)=-cyb*cyb
  w(2,ny)=w(1,ny)+w(3,ny)
!
! eliminate at last point
!
  c=-w(1,ny)/w(1,ny-1)
  w(2,ny)=w(2,ny)+c*w(2,ny-1)
  w(3,ny)=w(3,ny)+c*w(3,ny-1)
  w(1,ny)=w(2,ny)
  w(2,ny)=w(3,ny)
  w(3,ny)=c
!
!  Eliminate subdiagonal elements of rows 2 to n:
!
!     |     w2 w3       | * | . |  =  | . |
!     |     w1 w2 w3    |   | . |     | . |
!
  do k=2,ny
     w(1,k)=-w(1,k)/w(2,k-1)
     w(2,k)=w(2,k)+w(1,k)*w(3,k-1)
  enddo

!
!  Invert w(2,:), turns divides into mults
!
  do k=1,ny
     w(2,k)=1./w(2,k)
  enddo
!
END SUBROUTINE ydery0
!***********************************************************************

!***********************************************************************
      subroutine ydistr (nt,yt,ny,y,nx,nz,f,g)
!
!  Distribute to a new z-grid.
!
!  Update history:
!
!  22-dec-87/aake:  start at n=2 (was n=3)
!  15-jan-88/aake:  factor 2 bug corrected
!  09-aug-89/aake:  /cscr/ instead of blank common
!  22-aug-89/bob:   (l,m,n)->(lm,n)
!  24-apr-90/aake:  0.5*(z(n+1)-z(n-1)) inst. of z(n)-z(n-1)
!
!***********************************************************************
!
  
  integer                   :: nx,ny,nz,nt
  real, dimension(ny)       :: y
  real, dimension(nt)       :: yt
  real, dimension(nx,ny,nz) :: f
  real, dimension(nx,nt,nz) :: g
  
  real    :: dy,py,qy,dyt
  integer :: n,k,iz,k1,k2,nm1,nm2,np1

!
!  Find n such that z(n-1) < zt(k) < z(n)
!
  n=2

! 
! reset g
!
  do iz=1,nz
     g(:,:,iz)=0.0
  enddo

  do k=1,nt
101  if (yt(k).gt.y(n)) then
        n=n+1
        if (n.gt.ny) then
           nt=k-1
           goto 100
        endif
        goto 101
     endif

!
!  Weight factors proportional to distance from n-pts.
!
     dy=y(n)-y(n-1)
     py=(yt(k)-y(n-1))/dy
     qy=1.-py
!
!  Input is energy per unit volume, z-integral should be conserved.
!  We require the sum over g(n)*0.5*(z(n+1)-z(n-1)) to be the same
!  as the sum over f(k)*0.5*(zt(k+1)-zt(k-1)).  Used to assume that
!  0.5*(z(n+1)-z(n-1)) was equal to z(n)-z(n-1).  Although this was
!  in practice so in the layers where radiation matters, it need not
!  be, when we allow a non-equidistant mesh.
!
     k1=max0(k-1,1)
     k2=min0(k+1,nt)
     nm2=max0(n-2,1)
     nm1=max0(n-1,1)
     np1=min0(n+1,ny)
     dyt=yt(k2)-yt(k1)
     py=py*dyt/(y(np1)-y(nm1))
     qy=qy*dyt/(y(n)-y(nm2))

!
!  Do for all x,z pts.
!
     do iz=1,nz
        do ix=1,nx
           g(ix,n-1,iz)=g(ix,n-1,iz)+qy*f(ix,k,iz)
           g(ix,n  ,iz)=g(ix,n  ,iz)+py*f(ix,k,iz)
        enddo
     enddo
  enddo

100 continue
END SUBROUTINE ydistr
