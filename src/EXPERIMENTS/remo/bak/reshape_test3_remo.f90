!$Id: reshape_test3_remo.f90,v 1.1 2016/10/11 15:07:20 remo Exp $
!-------------------------------------------------------------------------------
module reshape_mod

  implicit none

  character(len=64) :: reshape_mode
  integer           :: dim_re(3)
  integer           :: mpi_comm_re_xy, mpi_comm_re_zy
  integer           :: mpi_col_re_zy, mpi_col_re_xy
  integer           :: mpi_re_z, mpi_re_x
  integer           :: mpi_re_nx, mpi_re_ny, mpi_re_nz
  integer           :: sub_nx, sub_nz
  integer           :: sub_x, sub_z
  
end module reshape_mod


!-------------------------------------------------------------------------------
subroutine reshape_init

  use params, only: mx, my, mz, mpi_nx, mpi_ny, mpi_nz, master, mid
  use reshape_mod

  implicit none

  character(len=mid) :: id='$Id: reshape_test3_remo.f90,v 1.1 2016/10/11 15:07:20 remo Exp $'
  character(len=64)  :: default_mode
  logical            :: is_valid_case, is_undef_sub_nx, is_undef_sub_nz
  logical            :: is_error_sub =.true.


  if (master) then
     print '(x,a)', repeat("-",72)
     print '(x,a)', id
  end if


  reshape_mode = trim(reshape_mode)
  default_mode = 'ymrg_xzspl'

  ! check validity of reshape_mode
  select case (reshape_mode)
  case ('ymrg_zspl')
     is_valid_case = .true.
  case ('ymrg_xzspl')
     is_valid_case = .true.
  case default
     is_valid_case = .false.
  end select

  ! if reshape_mode not valid, set to default
  if (.not.is_valid_case) then 
     if (master) then
        print '(x,a13,x,a)', 'reshape_init:', 'warning: reshape_mode unavailable, set to default mode'
        print '(4x,a13,x,a)', 'reshape_mode=', default_mode
     end if
     reshape_mode = default_mode
  else
     if (master) then
        print '(x,a13,x,a13,x,a)', 'reshape_init:', 'reshape_mode=', reshape_mode
     end if
  end if

 
  ! check whether sub_nx or sub_nz are undefined (=0)
  is_undef_sub_nx = sub_nx.eq.0 
  is_undef_sub_nz = sub_nz.eq.0


  select case (reshape_mode)

  case ('ymrg_zspl')
     ! split in z direction:
     ! force sub_nx=1 and sub_nz=mpi_ny
     sub_nx     =  1
     sub_nz     =  mpi_ny
     dim_re(1)  =  mx / sub_nx
     dim_re(2)  =  my * mpi_ny
     dim_re(3)  =  mz / sub_nz
     if (master) then
        print '(x,a13,x,a,x,3(x,i5))', 'reshape_init:', 'mpi_ny, sub_n[x,z] =', mpi_ny, sub_nx, sub_nz
     end if

  case ('ymrg_xzspl')

     if (is_undef_sub_nx.and.is_undef_sub_nz) then
        ! if both sub_nx and sub_nz are undefined, factorize mpi_ny = sub_nx * sub_nz
        call factorize_two(mpi_ny, sub_nx, sub_nz)
     else
        if (is_undef_sub_nz) then
           ! only sub_nz is undefined
           sub_nz = mpi_ny / sub_nx
        else
           ! only sub_nx is undefined
           sub_nx = mpi_ny / sub_nz
        end if
     end if

     ! check that product sub_nx * sub_nz = mpi_ny
     ! stop program if that is not the case
     is_error_sub = mpi_ny.ne.(sub_nx*sub_nz)

     if (is_error_sub.and.master) then
        print '(x,a13,x,a,x,3(x,i5))', 'reshape_init:', 'mpi_ny, sub_n[x,z] =', mpi_ny, sub_nx, sub_nz
        print '(x,a13,x,a)', 'error:', 'sub_nx * sub_nz must be equal to mpi_ny. STOP.'
        call abort_mpi
        stop
     end if

     ! dimensions reshaped arrays
     dim_re(1)  =  mx / sub_nx
     dim_re(2)  =  my * mpi_ny
     dim_re(3)  =  mz / sub_nz
     if (master) then
        print '(x,a13,x,a,x,3(x,i5))', 'reshape_init:', 'mpi_ny, sub_n[x,z] =', mpi_ny, sub_nx, sub_nz
     end if

 end select


  ! initialize new plane communicators for configuration with reshaped arrays
  call mpi_split_comm_reshape

  ! print mpi dimensions for reshaped configuration:
  if (master) then
     print '(x,a13,x,a,3(x,i4))', 'reshape_init:', 'mpi_re_n[x,y,z] =', mpi_re_nx, mpi_re_ny, mpi_re_nz
     print '(a)', ''
  end if

end subroutine reshape_init


!-------------------------------------------------------------------------------
subroutine reshape_cube(f, g) 

  use params, only: mx, my, mz
  use reshape_mod

  implicit none

  real, dimension( mx, my, mz )                      :: f
  real, dimension( dim_re(1), dim_re(2), dim_re(3) ) :: g

  select case (reshape_mode)
  case ('ymrg_zspl')
     call reshape_cube_ymrg_zspl(f, g)
  case ('ymrg_xzspl')
     call reshape_cube_ymrg_xzspl(f, g)
  end select

end subroutine reshape_cube


!-------------------------------------------------------------------------------
subroutine deshape_cube(g, f) 

  use params, only: mx, my, mz
  use reshape_mod

  implicit none

  real, dimension( dim_re(1), dim_re(2), dim_re(3) ) :: g
  real, dimension( mx, my, mz )                      :: f

  select case (reshape_mode)
  case ('ymrg_zspl')
     call deshape_cube_yspl_zmrg(g, f)
  case ('ymrg_xzspl')
     call deshape_cube_yspl_xzmrg(g, f)
  end select

end subroutine deshape_cube


!-------------------------------------------------------------------------------
subroutine reshape_cube_ymrg_zspl(f, g)

  use params, only: mx, my, mz, mpi_ny
  use mpi_mod, only: mpi_comm_beam, my_real, mpi_err
  use reshape_mod

  implicit none

  integer :: iy_ini, iy_end
  integer :: iz_ini, iz_end
  integer :: mpi_iy
  integer :: ncount

  logical :: is_same_dim

  real, dimension( mx, my, mz )                      :: f
  real, dimension( dim_re(1), dim_re(2), dim_re(3) ) :: g
  real, dimension( mx, my, dim_re(3), 0:mpi_ny-1 )   :: send_buffer, recv_buffer


  ! if dimensions are the same, simply copy and return
  is_same_dim = (mx.eq.dim_re(1).and.my.eq.dim_re(2).and.mz.eq.dim_re(3))
  if (is_same_dim) then
     g = f
     return
  end if

  ! size of buffer (number of data points to be communicated)
  ncount = mx * my * dim_re(3)

  ! split f array in blocks along z direction
  ! copy blocks to buffer before communicating

  do mpi_iy=0,mpi_ny-1
     iz_ini = dim_re(3) * mpi_iy    + 1
     iz_end = dim_re(3) *(mpi_iy+1)
     send_buffer(:,:,:,mpi_iy) = f(:,:,iz_ini:iz_end)
  end do

  ! communicate buffer
  call MPI_ALLTOALL(send_buffer,ncount,my_real,recv_buffer,ncount,my_real,mpi_comm_beam(2),mpi_err)

  ! sort blocks from receive buffer to g array

  do mpi_iy=0,mpi_ny-1
     iy_ini = my * mpi_iy   + 1
     iy_end = my *(mpi_iy+1)
     g(:,iy_ini:iy_end,:) = recv_buffer(:,:,:,mpi_iy)
  end do

end subroutine reshape_cube_ymrg_zspl


!-------------------------------------------------------------------------------
subroutine reshape_cube_ymrg_xzspl(f, g)

  use params, only: mx, my, mz, mpi_ny
  use mpi_mod, only: mpi_comm_beam, my_real, mpi_err
  use reshape_mod

  implicit none

  integer :: sub_ix, sub_iz
  integer :: ix_ini, ix_end
  integer :: iy_ini, iy_end
  integer :: iz_ini, iz_end
  integer :: mpi_iy
  integer :: ncount

  logical :: is_same_dim

  real, dimension( mx, my, mz )                      :: f
  real, dimension( dim_re(1), dim_re(2), dim_re(3) ) :: g
  real, dimension( dim_re(1), my, dim_re(3), 0:mpi_ny-1 ) :: send_buffer, recv_buffer


  ! if dimensions are the same, simply copy and return
  is_same_dim = (mx.eq.dim_re(1).and.my.eq.dim_re(2).and.mz.eq.dim_re(3))
  if (is_same_dim) then
     g = f
     return
  end if

  ! size of buffer (number of data points to be communicated)
  ncount = dim_re(1) * my * dim_re(3)

  ! split f array in blocks along z direction;
  ! copy blocks to buffer before communicating. 
  ! note: this version of the routine uses a double loop over sub_x and subz,
  ! with sub_nx * sub_nz = mpi_ny.

  do mpi_iy=0,mpi_ny-1
     sub_ix = mpi_iy / sub_nz
     sub_iz = mod(mpi_iy,sub_nz)
     ix_ini = dim_re(1) *  sub_ix      + 1
     ix_end = dim_re(1) * (sub_ix +1)
     iz_ini = dim_re(3) *  sub_iz      + 1
     iz_end = dim_re(3) * (sub_iz +1)  
     send_buffer(:,:,:,mpi_iy) = f(ix_ini:ix_end,:,iz_ini:iz_end)
  end do
 
  ! communicate buffer
  call MPI_ALLTOALL(send_buffer,ncount,my_real,recv_buffer,ncount,my_real,mpi_comm_beam(2),mpi_err)

  ! sort blocks from receive buffer to g array
  do mpi_iy=0,mpi_ny-1
     iy_ini = my * mpi_iy   + 1
     iy_end = my *(mpi_iy+1)
     g(:,iy_ini:iy_end,:) = recv_buffer(:,:,:,mpi_iy)
  end do

end subroutine reshape_cube_ymrg_xzspl


!-------------------------------------------------------------------------------
subroutine deshape_cube_yspl_zmrg(g, f)

  use params, only: mx, my, mz, mpi_ny
  use mpi_mod, only: mpi_comm_beam, my_real, mpi_err
  use reshape_mod

  implicit none

  integer :: iy_ini, iy_end
  integer :: iz_ini, iz_end
  integer :: mpi_iy
  integer :: ncount

  logical :: is_same_dim

  real, dimension( dim_re(1), dim_re(2), dim_re(3) ) :: g
  real, dimension( mx, my, mz )                      :: f
  real, dimension( mx, my, dim_re(3), 0:mpi_ny-1 )   :: send_buffer, recv_buffer


  ! if dimensions are the same, simply copy and return
  is_same_dim = (mx.eq.dim_re(1).and.my.eq.dim_re(2).and.mz.eq.dim_re(3))
  if (is_same_dim) then
     f = g
     return
  end if

  ! size of buffer (number of data points to be communicated)
  ncount = mx * my * dim_re(3)

  ! split g array in blocks along y
  ! copy to send_buffer before communicating

  do mpi_iy=0,mpi_ny-1
     iy_ini = my * mpi_iy   + 1
     iy_end = my *(mpi_iy+1)
     send_buffer(:,:,:,mpi_iy) = g(:,iy_ini:iy_end,:)
  end do

  ! communicate buffer
  call MPI_ALLTOALL(send_buffer,ncount,my_real,recv_buffer,ncount,my_real,mpi_comm_beam(2),mpi_err)

  ! sort blocks from receive buffer to f array
  do mpi_iy=0,mpi_ny-1
     iz_ini = dim_re(3) * mpi_iy    + 1
     iz_end = dim_re(3) *(mpi_iy+1)
     f(:,:,iz_ini:iz_end) = recv_buffer(:,:,:,mpi_iy)
  end do

end subroutine deshape_cube_yspl_zmrg


!-------------------------------------------------------------------------------
subroutine deshape_cube_yspl_xzmrg(g, f)

  use params, only: mx, my, mz, mpi_ny
  use mpi_mod, only: mpi_comm_beam, my_real, mpi_err
  use reshape_mod

  implicit none

  integer :: sub_ix, sub_iz
  integer :: ix_ini, ix_end
  integer :: iy_ini, iy_end
  integer :: iz_ini, iz_end
  integer :: mpi_iy
  integer :: ncount

  logical :: is_same_dim

  real, dimension( dim_re(1), dim_re(2), dim_re(3) ) :: g
  real, dimension( mx, my, mz )                      :: f
  real, dimension( dim_re(1), my, dim_re(3), 0:mpi_ny-1 ) :: send_buffer, recv_buffer


  ! if dimensions are the same, simply copy and return
  is_same_dim = (mx.eq.dim_re(1).and.my.eq.dim_re(2).and.mz.eq.dim_re(3))
  if (is_same_dim) then
     f = g
     return
  end if

  ! size of buffer (number of data points to be communicated)
  ncount = dim_re(1) * my * dim_re(3)

  ! split g array in blocks along y
  ! copy to send_buffer before communicating
  do mpi_iy=0,mpi_ny-1
     iy_ini = my * mpi_iy   + 1
     iy_end = my *(mpi_iy+1)
     send_buffer(:,:,:,mpi_iy) = g(:,iy_ini:iy_end,:)
  end do

  ! communicate buffer
  call MPI_ALLTOALL(send_buffer,ncount,my_real,recv_buffer,ncount,my_real,mpi_comm_beam(2),mpi_err)

  ! sort blocks from receive buffer to f array;
  ! note: this version of the routine uses a double loop over sub_x and subz,
  ! with sub_nx * sub_nz = mpi_ny.
  do mpi_iy=0,mpi_ny-1
     sub_ix = mpi_iy / sub_nz
     sub_iz = mod(mpi_iy,sub_nz)
     ix_ini = dim_re(1) *  sub_ix      + 1
     ix_end = dim_re(1) * (sub_ix +1)
     iz_ini = dim_re(3) *  sub_iz      + 1
     iz_end = dim_re(3) * (sub_iz +1)  
     f(ix_ini:ix_end,:,iz_ini:iz_end) = recv_buffer(:,:,:,mpi_iy)
  end do

end subroutine deshape_cube_yspl_xzmrg


!-------------------------------------------------------------------------------
subroutine reshape_ymesh(f, g)

  use params, only: my, mpi_ny
  use mpi_mod, only: mpi_comm_beam, my_real, mpi_err
  use reshape_mod

  implicit none

  integer :: iy_ini, iy_end
  integer :: mpi_iy
  integer :: ncount

  logical :: is_same_dim

  real, dimension( my )               :: f
  real, dimension( dim_re(2) )        :: g
  real, dimension( my )               :: send_buffer
  real, dimension( my, 0:mpi_ny-1 )   :: recv_buffer


  ! if dimensions are the same, simply copy and return
  is_same_dim = (my.eq.dim_re(2))
  if (is_same_dim) then
     g = f
     return
  end if

  ! size of buffer (number of depth points to be communicated)
  ncount = my

  ! copy f to buffer before communicating
  send_buffer(:) = f(:)

  ! communicate buffer
  call MPI_ALLGATHER(send_buffer,ncount,my_real,recv_buffer,ncount,my_real,mpi_comm_beam(2),mpi_err)

  ! sort blocks from receive buffer to g array
  do mpi_iy=0,mpi_ny-1
     iy_ini = my * mpi_iy   + 1
     iy_end = my *(mpi_iy+1)
     g(iy_ini:iy_end) = recv_buffer(:,mpi_iy)
  end do

end subroutine reshape_ymesh


!-------------------------------------------------------------------------------
subroutine merge_surf(g, f)

  use params, only: mx, mz
  use reshape_mod

  implicit none

  real, dimension( dim_re(1), dim_re(3) )       :: g
  real, dimension( mx, mz )                     :: f

  select case (reshape_mode)
  case ('ymrg_zspl')
     call merge_xzsurf_zmrg(g, f)
  case ('ymrg_xzspl')
     call merge_xzsurf_xzmrg(g, f)
  end select

end subroutine merge_surf


!-------------------------------------------------------------------------------
subroutine merge_xzsurf_zmrg(g, f)

  use params, only: mx, mz, mpi_ny, mpi_y
  use mpi_mod, only: mpi_comm_beam, my_real, mpi_err
  use reshape_mod

  implicit none

  integer :: iz_ini, iz_end
  integer :: mpi_iy, mpi_root
  integer :: ncount

  logical :: is_same_dim

  ! note: dim_re(1) = mx for this particular reshape case

  real, dimension( dim_re(1), dim_re(3) )       :: g
  real, dimension( mx, mz )                     :: f
  real, dimension( dim_re(1), dim_re(3) )       :: send_buffer
  real, dimension( dim_re(1), dim_re(3), 0:mpi_ny-1 )  :: recv_buffer


  ! if dimensions are the same, simply copy and return
  is_same_dim = (mx.eq.dim_re(1).and.mz.eq.dim_re(3))
  if (is_same_dim) then
     f = g 
     return
  end if

  !root task
  mpi_root = 0

  ! size of "tile" to be communicated
  ncount = dim_re(1) * dim_re(3)

  ! copy to send_buffer before communicating
  send_buffer(:,:) = g(:,:)

  ! the root task (mpi_y=0) receives data from all tasks within mpi_comm_beam(2) 
  ! in order to reconstruct the surface array; 
  ! all other tasks return an empty array.

  call MPI_GATHER(send_buffer,ncount,my_real,recv_buffer,ncount,my_real,mpi_root,mpi_comm_beam(2),mpi_err)

  if (mpi_y.eq.mpi_root) then
     ! sort data from receive buffer to f array to reconstruct surface array
     do mpi_iy=0,mpi_ny-1
        iz_ini = dim_re(3) *  mpi_iy     + 1
        iz_end = dim_re(3) * (mpi_iy +1)
        f(:,iz_ini:iz_end) = recv_buffer(:,:,mpi_iy)
     end do
  else
     ! return empty array
     f(:,:) = 0.0
  end if

end subroutine merge_xzsurf_zmrg


!-------------------------------------------------------------------------------
subroutine merge_xzsurf_xzmrg(g, f)

  use params, only: mx, mz, mpi_ny, mpi_y
  use mpi_mod, only: mpi_comm_beam, my_real, mpi_err
  use reshape_mod

  implicit none

  integer :: sub_ix, sub_iz
  integer :: ix_ini, ix_end
  integer :: iz_ini, iz_end
  integer :: mpi_iy, mpi_root
  integer :: ncount

  logical :: is_same_dim

  ! note: dim_re(1) = mx for this particular reshape case

  real, dimension( dim_re(1), dim_re(3) )       :: g
  real, dimension( mx, mz )                     :: f
  real, dimension( dim_re(1), dim_re(3) )       :: send_buffer
  real, dimension( dim_re(1), dim_re(3), 0:mpi_ny-1 )  :: recv_buffer


  ! if dimensions are the same, simply copy and return
  is_same_dim = (mx.eq.dim_re(1).and.mz.eq.dim_re(3))
  if (is_same_dim) then
     f = g 
     return
  end if


  !root task
  mpi_root = 0

  ! size of "tile" to be communicated
  ncount = dim_re(1) * dim_re(3)

  ! copy to send_buffer before communicating
  send_buffer(:,:) = g(:,:)

  ! the root task (mpi_y=0) receives data from all tasks within mpi_comm_beam(2) 
  ! in order to reconstruct the surface array; 
  ! all other tasks return an empty array.

  call MPI_GATHER(send_buffer,ncount,my_real,recv_buffer,ncount,my_real,mpi_root,mpi_comm_beam(2),mpi_err)

  if (mpi_y.eq.mpi_root) then
     ! sort data from receive buffer to f array to reconstruct surface array
     ! note: use double loop in sub_x and sub_z, with sub_nx * sub_nz = mpi_ny
     do mpi_iy=0,mpi_ny-1
        sub_ix = mpi_iy / sub_nz
        sub_iz = mod(mpi_iy,sub_nz)
        ix_ini = dim_re(1) *  sub_ix      + 1
        ix_end = dim_re(1) * (sub_ix +1)
        iz_ini = dim_re(3) *  sub_iz      + 1
        iz_end = dim_re(3) * (sub_iz +1)  
        f(ix_ini:ix_end,iz_ini:iz_end) = recv_buffer(:,:,mpi_iy)
     end do
  else
     ! return empty array
     f(:,:) = 0.0
  end if

end subroutine merge_xzsurf_xzmrg


!-------------------------------------------------------------------------------
subroutine mpi_split_comm_reshape

  use reshape_mod, only: reshape_mode
  implicit none

  select case (reshape_mode)
  case ('ymrg_zspl')
     call mpi_split_comm_reshape_ymrg_zspl
  case ('ymrg_xzspl')
     call mpi_split_comm_reshape_ymrg_xzspl
  end select

end subroutine mpi_split_comm_reshape


!-------------------------------------------------------------------------------
subroutine mpi_split_comm_reshape_ymrg_zspl

  use params, only: mpi_rank, mpi_nx, mpi_ny, mpi_nz, mpi_x, mpi_y, mpi_z
  use reshape_mod, only: mpi_comm_re_xy, mpi_comm_re_zy, &
       mpi_col_re_xy, mpi_col_re_zy, mpi_re_x, mpi_re_z, &
       mpi_re_nx, mpi_re_ny, mpi_re_nz
  use mpi_mod

  implicit none

  ! MPI dimensions for the new configuration with reshaped arrays 
  mpi_re_nx  =  mpi_nx
  mpi_re_ny  =  1
  mpi_re_nz  =  mpi_nz * mpi_ny

  ! MPI color to identify all reshaped domains along local zy plane;
  ! MPI key to locate reshaped domains along zy plane 
  mpi_col_re_zy = mpi_x
  mpi_re_z      = mpi_z * mpi_ny + mpi_y

  ! MPI color to identify all reshaped domains along local xy plane 
  ! MPI key to locate reshaped domains along xy plane 
  mpi_col_re_xy = mpi_z * mpi_ny + mpi_y
  mpi_re_x      = mpi_x


  ! MPI split MPI_COMM_WORLD into subcommunicators according to reshaped zy and xy planes
  call MPI_COMM_SPLIT(mpi_comm_world, mpi_col_re_zy, mpi_re_z, mpi_comm_re_zy, mpi_err)
  call MPI_COMM_SPLIT(mpi_comm_world, mpi_col_re_xy, mpi_re_x, mpi_comm_re_xy, mpi_err)

end subroutine mpi_split_comm_reshape_ymrg_zspl


!-------------------------------------------------------------------------------
subroutine mpi_split_comm_reshape_ymrg_xzspl

  use params, only: mpi_rank, mpi_nx, mpi_ny, mpi_nz, mpi_x, mpi_y, mpi_z
  use reshape_mod, only: mpi_comm_re_xy, mpi_comm_re_zy, &
       mpi_col_re_xy, mpi_col_re_zy, mpi_re_x, mpi_re_z, &
       mpi_re_nx, mpi_re_ny, mpi_re_nz, &
       sub_nx, sub_nz, sub_x, sub_z
  use mpi_mod

  implicit none


  ! MPI dimensions for the new configuration with reshaped arrays 
  mpi_re_nx  =  mpi_nx * sub_nx
  mpi_re_ny  =  1
  mpi_re_nz  =  mpi_nz * sub_nz


  ! split "mx * mz" columns in  mpi_ny = sub_nx * sub_nz  sub-blocks;
  ! identify each sub-block with coordinates (sub_x,sub_z);
  ! define coordinates in such a way that sub-blocks with sub_x = constant
  ! have consecutive values of mpi_y

  sub_x         = mpi_y / sub_nz
  sub_z         = mod(mpi_y,sub_nz)

  ! MPI color to identify all reshaped domains along local zy plane;
  ! MPI key to locate reshaped domains along zy plane 
  mpi_col_re_zy = sub_x + mpi_x * sub_nx
  mpi_re_z      = sub_z + mpi_z * sub_nz 

  ! MPI color to identify all reshaped domains along local xy plane 
  ! MPI key to locate reshaped domains along xy plane 
  mpi_col_re_xy = sub_z + mpi_z * sub_nz
  mpi_re_x      = sub_x + mpi_x * sub_nx


  ! MPI split MPI_COMM_WORLD into subcommunicators according to reshaped zy and xy planes
  call MPI_COMM_SPLIT(mpi_comm_world, mpi_col_re_zy, mpi_re_z, mpi_comm_re_zy, mpi_err)
  call MPI_COMM_SPLIT(mpi_comm_world, mpi_col_re_xy, mpi_re_x, mpi_comm_re_xy, mpi_err)

end subroutine mpi_split_comm_reshape_ymrg_xzspl


!-------------------------------------------------------------------------------
subroutine buffer_blocks_alt_z(nx,ny,nz,nymax,nyBlocks,f1,f2,iyIniArr,iyEndArr,kBlockArr)

  use mpi_mod, only: send, recv, my_real, MPI_STATUS_SIZE, mpi_err
  use params, only: mpi_rank, mpi_nz, mpi_z
  use reshape_mod, only: mpi_comm_re_zy, mpi_col_re_zy, mpi_re_z, mpi_re_nz

  implicit none

  integer,                intent(in)  :: nx, ny, nz, nymax, nyBlocks
  integer, dimension(ny), intent(in)  :: iyIniArr, iyEndArr, kBlockArr
  real,    dimension( nx, ny, nz ),         intent(in)   :: f1
  real,    dimension( nx, ny, -1:nz+nz+2 ), intent(out)  :: f2
  real,    dimension( :, :, : ), allocatable :: send1, recv1

  integer  :: request1, request2, request3, request4
  integer  :: request5, request6, request7, request8
  integer  :: tag_send, tag_recv
  integer  :: status(MPI_STATUS_SIZE)

  integer  :: mpi_izdnL,  mpi_izupL,  mpi_izdnR,  mpi_izupR
  integer  :: mpi_izdnL1, mpi_izupL1, mpi_izdnR1, mpi_izupR1
  integer  :: m, m1, nyb, j, iyIni, iyEnd
  integer  ::  kBlockL,  kBlockR,  kBlockL1,  kBlockR1
  integer  :: mkBlockL, mkBlockR, mkBlockL1, mkBlockR1
  logical  :: is_not_zero_L, is_not_zero_R, is_not_zero_L1, is_not_zero_R1  

  ! Block topology:
  ! 
  ! - -------------------------------------  
  !    |    |           |           |    |
  !    | L1 |     L     |     R     | R1 |       
  !    |    |           |           |    |
  ! - -------------------------------------  
  !
  ! L, R  = left and right main blocks, nz grid-points wide
  ! L1,R1 = left and right thin layers, two grid-points thick


  ! case: no split in z direction

  if (mpi_re_nz == 1) then
     f2( 1:nx, 1:nymax,       -1:0      )  =  f1( 1:nx, 1:nymax, nz-1:nz )
     f2( 1:nx, 1:nymax,       1:nz      )  =  f1( 1:nx, 1:nymax,    1:nz )
     f2( 1:nx, 1:nymax,    nz+1:nz+nz   )  =  f1( 1:nx, 1:nymax,    1:nz )
     f2( 1:nx, 1:nymax, nz+nz+1:nz+nz+2 )  =  f1( 1:nx, 1:nymax,    1:2  )
     return
  end if


  ! case: mpi-split in z direction

  do j=1,nyBlocks

     iyIni     = iyIniArr(j) 
     iyEnd     = iyEndArr(j)
     nyb       = iyEnd - iyIni + 1

     kBlockL   = kBlockArr(j)
     kBlockR   = kBlockArr(j) + 1
     kBlockR1  = kBlockArr(j) + 2
     kBlockL1  = kBlockArr(j) - 1

     mkBlockL  = modulo( kBlockL,  mpi_re_nz )
     mkBlockR  = modulo( kBlockR,  mpi_re_nz )
     mkBlockR1 = modulo( kBlockR1, mpi_re_nz )
     mkBlockL1 = modulo( kBlockL1, mpi_re_nz )

     is_not_zero_L  = mkBlockL  .ne. 0
     is_not_zero_R  = mkBlockR  .ne. 0
     is_not_zero_R1 = mkBlockR1 .ne. 0
     is_not_zero_L1 = mkBlockL1 .ne. 0

     m         = nx * nyb * nz
     m1        = nx * nyb * 2

     allocate ( send  (1:nx, 1:nyb, 1:nz), recv  (1:nx, 1:nyb, 1:nz) )
     allocate ( send1 (1:nx, 1:nyb, 1:2 ), recv1 (1:nx, 1:nyb, 1:2 ) )


     ! communicate L and R blocks

     send  = f1( 1:nx, iyIni:iyEnd, 1:nz )

     if (is_not_zero_L) then 
        mpi_izdnL = modulo( mpi_re_z - kBlockL, mpi_re_nz)
        mpi_izupL = modulo( mpi_re_z + kBlockL, mpi_re_nz)
        tag_send  = 4 * mpi_re_z
        tag_recv  = 4 * mpi_izupL
        call MPI_ISEND (send, m, my_real, mpi_izdnL, tag_send, mpi_comm_re_zy, request1, mpi_err)
        call MPI_IRECV (recv, m, my_real, mpi_izupL, tag_recv, mpi_comm_re_zy, request5, mpi_err)
        call MPI_WAIT (request1, status, mpi_err)
        call MPI_WAIT (request5, status, mpi_err)
        f2( 1:nx, iyIni:iYEnd, 1:nz ) = recv
     else
        f2( 1:nx, iyIni:iYEnd, 1:nz ) = f1( 1:nx, iyIni:iYEnd, 1:nz )
     endif


     if (is_not_zero_R) then 
        mpi_izdnR = modulo( mpi_re_z - kBlockR, mpi_re_nz)
        mpi_izupR = modulo( mpi_re_z + kBlockR, mpi_re_nz)
        tag_send  = 4 * mpi_re_z + 1
        tag_recv  = 4 * mpi_izupR + 1
        call MPI_ISEND (send, m, my_real, mpi_izdnR, tag_send, mpi_comm_re_zy, request2, mpi_err)
        call MPI_IRECV (recv, m, my_real, mpi_izupR, tag_recv, mpi_comm_re_zy, request6, mpi_err)
        call MPI_WAIT (request2, status, mpi_err)
        call MPI_WAIT (request6, status, mpi_err)
        f2( 1:nx, iyIni:iYEnd, nz+1:nz+nz ) = recv
     else
        f2( 1:nx, iyIni:iYEnd, nz+1:nz+nz ) = f1( 1:nx, iyIni:iYEnd, 1:nz )
     endif


     ! communicate L1 and R1 layers

     send1  = f1( 1:nx, iyIni:iyEnd, nz-1:nz )

     if (is_not_zero_L1) then 
        mpi_izdnL1 = modulo( mpi_re_z - kBlockL1, mpi_re_nz)
        mpi_izupL1 = modulo( mpi_re_z + kBlockL1, mpi_re_nz)
        tag_send  = 4 * mpi_re_z + 2
        tag_recv  = 4 * mpi_izupL1 + 2
        call MPI_ISEND (send1, m1, my_real, mpi_izdnL1, tag_send, mpi_comm_re_zy, request3, mpi_err)
        call MPI_IRECV (recv1, m1, my_real, mpi_izupL1, tag_recv, mpi_comm_re_zy, request7, mpi_err)
        call MPI_WAIT (request3, status, mpi_err)
        call MPI_WAIT (request7, status, mpi_err)
        f2( 1:nx, iyIni:iYEnd, -1:0 ) = recv1
     else
        f2( 1:nx, iyIni:iYEnd, -1:0 ) = send1
     endif

     send1  = f1( 1:nx, iyIni:iyEnd, 1:2 )

     if (is_not_zero_R1) then 
        mpi_izdnR1 = modulo( mpi_re_z - kBlockR1, mpi_re_nz)
        mpi_izupR1 = modulo( mpi_re_z + kBlockR1, mpi_re_nz)
        tag_send  = 4 * mpi_re_z + 3
        tag_recv  = 4 * mpi_izupR1 + 3
        call MPI_ISEND (send1, m1, my_real, mpi_izdnR1, tag_send, mpi_comm_re_zy, request4, mpi_err)
        call MPI_IRECV (recv1, m1, my_real, mpi_izupR1, tag_recv, mpi_comm_re_zy, request8, mpi_err)
        call MPI_WAIT (request4, status, mpi_err)
        call MPI_WAIT (request8, status, mpi_err)
        f2( 1:nx, iyIni:iYEnd, nz+nz+1:nz+nz+2 ) = recv1
     else
        f2( 1:nx, iyIni:iYEnd, nz+nz+1:nz+nz+2 ) = send1
     endif

     deallocate ( send,  recv )
     deallocate ( send1, recv1 )

  end do

end subroutine buffer_blocks_alt_z


!-------------------------------------------------------------------------------
subroutine buffer_blocks_alt_x(nx,ny,nz,nymax,nyBlocks,f1,f2,iyIniArr,iyEndArr,kBlockArr)

  use mpi_mod, only: send, recv, my_real, MPI_STATUS_SIZE, mpi_err
  use params, only: mpi_rank, mpi_nx, mpi_x
  use reshape_mod, only: mpi_comm_re_xy, mpi_col_re_xy, mpi_re_x, mpi_re_nx

  implicit none

  integer,                intent(in)  :: nx, ny, nz, nymax, nyBlocks
  integer, dimension(ny), intent(in)  :: iyIniArr, iyEndArr, kBlockArr
  real,    dimension(         nx, ny, nz ), intent(in)   :: f1
  real,    dimension( -1:nx+nx+2, ny, nz ), intent(out)  :: f2

  real, allocatable, dimension(:,:,:) :: bufL, bufR, bufL1, bufR1
  real, allocatable, dimension(:,:,:) :: sendL1, sendR1
  integer  :: req_send, req_recv
  integer  :: tag_send, tag_recv
  integer  :: ireq, nrequests
  integer, allocatable, dimension(:)   :: requests
  integer, allocatable, dimension(:,:) :: status

  integer  :: mpi_ixdnL,  mpi_ixupL,  mpi_ixdnR,  mpi_ixupR
  integer  :: mpi_ixdnL1, mpi_ixupL1, mpi_ixdnR1, mpi_ixupR1
  integer  :: m, m1, nyb, j, iy, iyIni, iyEnd, iz
  integer  ::  kBlockL,  kBlockR,  kBlockL1,  kBlockR1
  integer  :: mkBlockL, mkBlockR, mkBlockL1, mkBlockR1
  logical  :: is_not_zero_L, is_not_zero_R, is_not_zero_L1, is_not_zero_R1  


  ! Block topology:
  ! 
  ! - -------------------------------------  
  !    |    |           |           |    |
  !    | L1 |     L     |     R     | R1 |       
  !    |    |           |           |    |
  ! - -------------------------------------  
  !
  ! L, R  = left and right main blocks, nx grid-points wide
  ! L1,R1 = left and right thin layers, two grid-points thick


  ! case: no MPI-split in x direction
  if (mpi_re_nx == 1) then
     f2(      -1:0      , 1:nymax, 1:nz )  =  f1( nx-1:nx, 1:nymax, 1:nz )
     f2(       1:nx     , 1:nymax, 1:nz )  =  f1(    1:nx, 1:nymax, 1:nz )
     f2(    nx+1:nx+nx  , 1:nymax, 1:nz )  =  f1(    1:nx, 1:nymax, 1:nz )
     f2( nx+nx+1:nx+nx+2, 1:nymax, 1:nz )  =  f1(    1:2 , 1:nymax, 1:nz )
     return
  end if


  ! case: MPI-split in x direction

  ! allocate buffer for sending L and R blocks;
  ! transpose f1 array -> tf1: 
  ! y-index needs to be the last one in order for MPI_ISEND/MPI_IRECV
  ! to communicate the correct data using pointers
  allocate( send( 1:nx, 1:nz, 1:nymax) )
  do iy=1,nymax
     do iz=1,nz
           send(:,iz,iy) = f1(:,iy,iz)
     end do
  end do
  
  ! allocate and fill buffers for sending L1 and R1 layers
  allocate( sendL1(      -1:0      , 1:nz, 1:nymax ) )
  allocate( sendR1( nx+nx+1:nx+nx+2, 1:nz, 1:nymax ) )
  sendL1(      -1:0      , 1:nz, 1:nymax ) = send( nx-1:nx, 1:nz, 1:nymax )
  sendR1( nx+nx+1:nx+nx+2, 1:nz, 1:nymax ) = send(    1:2,  1:nz, 1:nymax )

  ! allocate buffers for receiving
  allocate( bufL1(      -1:0      , 1:nz, 1:nymax ) )
  allocate(  bufL(       1:nx     , 1:nz, 1:nymax ) )
  allocate(  bufR(    nx+1:nx+nx  , 1:nz, 1:nymax ) )
  allocate( bufR1( nx+nx+1:nx+nx+2, 1:nz, 1:nymax ) )

  ! allocate requests and status handles
  allocate( requests( nyBlocks*8 ) )
  allocate( status( MPI_STATUS_SIZE,nyBlocks*8 ) )
    

  ! reset requests counters
  ireq = 0
  nrequests = 0

  do j=1,nyBlocks

     iyIni     = iyIniArr(j) 
     iyEnd     = iyEndArr(j)
     nyb       = iyEnd - iyIni + 1

     kBlockL   = kBlockArr(j)
     kBlockR   = kBlockArr(j) + 1
     kBlockR1  = kBlockArr(j) + 2
     kBlockL1  = kBlockArr(j) - 1

     mkBlockL  = modulo( kBlockL,  mpi_re_nx )
     mkBlockR  = modulo( kBlockR,  mpi_re_nx )
     mkBlockR1 = modulo( kBlockR1, mpi_re_nx )
     mkBlockL1 = modulo( kBlockL1, mpi_re_nx )

     is_not_zero_L  = mkBlockL  .ne. 0
     is_not_zero_R  = mkBlockR  .ne. 0
     is_not_zero_R1 = mkBlockR1 .ne. 0
     is_not_zero_L1 = mkBlockL1 .ne. 0

     ! size of communication buffers
     m         = nx * nyb * nz
     m1        =  2 * nyb * nz

     ! communicate L and R blocks
     if (is_not_zero_L) then 
        mpi_ixupL = modulo( mpi_re_x + kBlockL, mpi_re_nx)
        tag_recv  = 4 * (mpi_ixupL * nyBlocks + (j-1) )
        call MPI_IRECV ( bufL( 1:nx, 1:nz, iyIni:iyEnd ), m, my_real, mpi_ixupL, tag_recv, mpi_comm_re_xy, req_recv, mpi_err)
        ireq = ireq + 1
        requests( ireq ) = req_recv
     else
        bufL( 1:nx, 1:nz, iyIni:iyEnd ) = send( 1:nx, 1:nz, iyIni:iyEnd )
     endif
     ! note: send array is the same as for L-block communication, careful with the indexes!
     if (is_not_zero_R) then 
        mpi_ixupR = modulo( mpi_re_x + kBlockR, mpi_re_nx)
        tag_recv  = 4 * (mpi_ixupR * nyBlocks + (j-1) ) + 1
        call MPI_IRECV ( bufR( nx+1:nx+nx, 1:nz, iyIni:iyEnd ), m, my_real, mpi_ixupR, tag_recv, mpi_comm_re_xy, req_recv, mpi_err)
        ireq = ireq + 1
        requests( ireq ) = req_recv
     else
        bufR( nx+1:nx+nx, 1:nz, iyIni:iYEnd ) = send( 1:nx, 1:nz, iyIni:iyEnd )
     endif

     ! communicate L1 and R1 layers
     if (is_not_zero_L1) then 
        mpi_ixupL1 = modulo( mpi_re_x + kBlockL1, mpi_re_nx)
        tag_recv  = 4 * ( mpi_ixupL1 * nyBlocks + (j-1) ) + 2
        call MPI_IRECV (  bufL1( -1:0, 1:nz, iyIni:iyEnd ), m1, my_real, mpi_ixupL1, tag_recv, mpi_comm_re_xy, req_recv, mpi_err)
        ireq = ireq + 1
        requests( ireq ) = req_recv
      else
        bufL1( -1:0, 1:nz, iyIni:iyEnd ) = sendL1( -1:0, 1:nz, iyIni:iyEnd )
     endif

     if (is_not_zero_R1) then 
        mpi_ixupR1 = modulo( mpi_re_x + kBlockR1, mpi_re_nx)
        tag_recv  = 4 * ( mpi_ixupR1 * nyBlocks + (j-1) ) + 3
        call MPI_IRECV (  bufR1( nx+nx+1:nx+nx+2, 1:nz, iyIni:iYEnd ), m1, my_real, mpi_ixupR1, tag_recv, mpi_comm_re_xy, req_recv, mpi_err)
        ireq = ireq + 1
        requests( ireq ) = req_recv
     else
        bufR1( nx+nx+1:nx+nx+2, 1:nz, iyIni:iYEnd ) = sendR1( nx+nx+1:nx+nx+2, 1:nz, iyIni:iYEnd )
     endif

  end do


  do j=1,nyBlocks

     iyIni     = iyIniArr(j) 
     iyEnd     = iyEndArr(j)
     nyb       = iyEnd - iyIni + 1

     kBlockL   = kBlockArr(j)
     kBlockR   = kBlockArr(j) + 1
     kBlockR1  = kBlockArr(j) + 2
     kBlockL1  = kBlockArr(j) - 1

     mkBlockL  = modulo( kBlockL,  mpi_re_nx )
     mkBlockR  = modulo( kBlockR,  mpi_re_nx )
     mkBlockR1 = modulo( kBlockR1, mpi_re_nx )
     mkBlockL1 = modulo( kBlockL1, mpi_re_nx )

     is_not_zero_L  = mkBlockL  .ne. 0
     is_not_zero_R  = mkBlockR  .ne. 0
     is_not_zero_R1 = mkBlockR1 .ne. 0
     is_not_zero_L1 = mkBlockL1 .ne. 0

     ! size of communication buffers
     m         = nx * nyb * nz
     m1        =  2 * nyb * nz

     ! communicate L and R blocks
     if (is_not_zero_L) then 
        mpi_ixdnL = modulo( mpi_re_x - kBlockL, mpi_re_nx)
        tag_send  = 4 * (mpi_re_x  * nyBlocks + (j-1) )
        call MPI_ISEND ( send( 1:nx, 1:nz, iyIni:iyEnd ), m, my_real, mpi_ixdnL, tag_send, mpi_comm_re_xy, req_send, mpi_err)
        ireq = ireq + 1
        requests( ireq  ) = req_send
     else
        bufL( 1:nx, 1:nz, iyIni:iyEnd ) = send( 1:nx, 1:nz, iyIni:iyEnd )
     endif
     ! note: send array is the same as for L-block communication, careful with the indexes!
     if (is_not_zero_R) then 
        mpi_ixdnR = modulo( mpi_re_x - kBlockR, mpi_re_nx)
        tag_send  = 4 * (mpi_re_x  * nyBlocks + (j-1) ) + 1
        call MPI_ISEND ( send(    1:nx   , 1:nz, iyIni:iyEnd ), m, my_real, mpi_ixdnR, tag_send, mpi_comm_re_xy, req_send, mpi_err)
        ireq = ireq + 1
        requests( ireq  ) = req_send
     else
        bufR( nx+1:nx+nx, 1:nz, iyIni:iYEnd ) = send( 1:nx, 1:nz, iyIni:iyEnd )
     endif

     ! communicate L1 and R1 layers
     if (is_not_zero_L1) then 
        mpi_ixdnL1 = modulo( mpi_re_x - kBlockL1, mpi_re_nx)
        tag_send  = 4 * ( mpi_re_x   * nyBlocks + (j-1) ) + 2
        call MPI_ISEND ( sendL1( -1:0, 1:nz, iyIni:iyEnd ), m1, my_real, mpi_ixdnL1, tag_send, mpi_comm_re_xy, req_send, mpi_err)
        ireq = ireq + 1
        requests(ireq  ) = req_send
     else
        bufL1( -1:0, 1:nz, iyIni:iyEnd ) = sendL1( -1:0, 1:nz, iyIni:iyEnd )
     endif

     if (is_not_zero_R1) then 
        mpi_ixdnR1 = modulo( mpi_re_x - kBlockR1, mpi_re_nx)
        tag_send  = 4 * ( mpi_re_x   * nyBlocks + (j-1) ) + 3
        call MPI_ISEND ( sendR1( nx+nx+1:nx+nx+2, 1:nz, iyIni:iYEnd ), m1, my_real, mpi_ixdnR1, tag_send, mpi_comm_re_xy, req_send, mpi_err)
        ireq = ireq + 1
        requests(ireq  ) = req_send
     else
        bufR1( nx+nx+1:nx+nx+2, 1:nz, iyIni:iYEnd ) = sendR1( nx+nx+1:nx+nx+2, 1:nz, iyIni:iYEnd )
     endif

  end do

  ! done nyBlocks loop

  ! total number of requests
  nrequests = ireq
     
  ! Wait for all communications to complete
  call MPI_WAITALL(nrequests, requests(1:nrequests), status(:,1:nrequests), mpi_err)
  

  ! transpose and copy data from buffers to f2
  do iz=1,nz
     do iy=1,nymax
        f2(      -1:0      , iy, iz )  =  bufL1(    -1:0        , iz, iy )
        f2(       1:nx     , iy, iz )  =  bufL (     1:nx       , iz, iy )
        f2(    nx+1:nx+nx  , iy, iz )  =  bufR (  nx+1:nx+nx    , iz, iy )
        f2( nx+nx+1:nx+nx+2, iy, iz )  =  bufR1( nx+nx+1:nx+nx+2, iz, iy )
     end do
  end do

  !deallocate
  deallocate( bufL, bufR, bufL1, bufR1 )
  deallocate( sendL1, sendR1 )
  deallocate( send )

  deallocate( requests, status )

end subroutine buffer_blocks_alt_x


!-------------------------------------------------------------------------------
subroutine factorize_two( num, fa, fb )

  ! the subroutine factorizes num into fa and fb (num=fa*fb),
  ! such that fa and fb are fa and fb are as close as possible
  ! to each other

  implicit none

  integer :: num, fa, fb, rem
  real    :: xnum, xfa
 
  ! first guess for fa: closest integer to square root of num
  xnum = real(num)
  xfa  = sqrt(xnum)
  fa   = floor(xfa)
  rem  = mod(num,fa)
  
  ! reduce fa until it divides num exactly (reminder == 0)
  do while(rem .ne. 0)
     fa  = fa-1
     rem = mod(num,fa)
  end do

  ! finally, find second factor
  fb = num / fa

end subroutine factorize_two

!-------------------------------------------------------------------------------
