! $Id: limitee.f90,v 1.1 2016/10/12 11:16:20 remo Exp $
!**********************************************************************
SUBROUTINE limitee(r,lnr,e,ee)
  USE params
  real, dimension(mx,my,mz):: r,lnr,e,ee
  logical omp_in_parallel
  !-----------------------------------------------------------------------
  if (omp_in_parallel()) then
     call limitee_omp (r,lnr,e,ee)
  else
     !$omp parallel
     call limitee_omp (r,lnr,e,ee)
     !$omp end parallel
  end if
END SUBROUTINE limitee

!**********************************************************************
SUBROUTINE limitee_omp (r,lnr,e,ee)
  USE params
  USE table

  implicit none
  real, dimension(mx,my,mz):: r,lnr,e,ee
  real rhm1,rhm2,drhm,algrk,lnrx,prho,lnrlim, &
       eemin1,eemin2,eemax1,eemax2,eex
  integer nrho
  integer ix, iy, iz

  data lnrlim /-4.0/

  do iz=izs,ize
     do iy=1,my
        do ix=1,mx

           lnrx = alog( r(ix,iy,iz) )
           eex  = ee(ix,iy,iz)

           if ( lnrx .lt. lnrlim ) then

              rhm1  = alog(rhm(1))
              rhm2  = alog(rhm(mrho))
              drhm  = (rhm2-rhm1)/(mrho-1)

              lnrx  = max( lnrx, alog(rhm(2)) )

              algrk = 1.+(lnrx-rhm1)/drhm
              nrho  = max0(2,min0(mrho,floor(algrk)))
              prho  = algrk-nrho

              eemin1= eemin(nrho-1)
              eemin2= eemin(nrho)

              eemax1= eemax(nrho-1)
              eemax2= eemax(nrho)

              ee(ix,iy,iz) = min(eemax1,eemax2,max(eemin1,eemin2,eex)) 
              lnr(ix,iy,iz)= lnrx
              r(ix,iy,iz)  = exp(lnrx)
              e(ix,iy,iz)  = ee(ix,iy,iz)*r(ix,iy,iz)

           endif

        end do
     end do
  end do

END SUBROUTINE limitee_omp
