! $Id: init.f90,v 1.2 2011/08/19 20:34:35 aake Exp $
!-----------------------------------------------------------------------
FUNCTION input_file()
  USE params
  implicit none
  character(len=mfile) input_file
  input_file = 'EXPERIMENTS/aris/input.txt'
END
!-----------------------------------------------------------------------
SUBROUTINE init_values (r,px,py,pz,e,d,Bx,By,Bz)
  USE params
  USE variables, ONLY: dpxdt,dpydt,dpzdt
  USE arrays
  USE forcing
  implicit none

  real, dimension(mx,my,mz):: r,px,py,pz,e,d,Bx,By,Bz
  real, allocatable, dimension(:,:,:):: Ui,dpx,dpy,dpz
  logical do_init, do_test
  character(len=32):: test
  real r0,e0,d0,b0,gam1,mrms,average,ti_turn
  integer iz,iy,ix
  namelist /init/do_init,do_test,test,r0,e0,d0,b0,ti_turn

!-----------------------------------------------------------------------
!  Initial value parameters
!-----------------------------------------------------------------------
  r0 = 1.
  b0 = 0.
  d0 = 1.
  e0 = 0.
  do_init = .true.
  do_test = .false.
  ti_turn = t_turn
  test = 'shear'

  if (master) write (*,*) hl
  read(stdin,init)
  if (master) write (*,init)

  gam1=gamma*(gamma-1.)
  if (gamma .eq. 1.) gam1=1.
  if (e0 .eq. 0.) e0 = r0*csound**2/gam1

!$omp parallel do private(iz)
  do iz=1,mz
    r(:,:,iz) = r0
    e(:,:,iz) = e0
    px(:,:,iz) = 0.
    py(:,:,iz) = 0.
    pz(:,:,iz) = 0.
    if (do_mhd) then
      bx(:,:,iz) = 0.
      by(:,:,iz) = 0.
      bz(:,:,iz) = b0
    end if
    if (do_pscalar) d(:,:,iz)=d0
  end do

  if (do_test) then
    call test_values (test,r,px,py,pz,e,d,Bx,By,Bz)
    return
  end if

  if (.not. do_init) return

!-----------------------------------------------------------------------
!  Initial linear acceleration
!-----------------------------------------------------------------------
  call forceit (r,px,py,pz,r,r,r,dpxdt,dpydt,dpzdt)
  !$omp parallel do private(iz)
  do iz=1,mz
    if (do_helmh) then
      px(:,:,iz) = (0.25+0.25*a_helmh)*ti_turn*dpxdt(:,:,iz)
      py(:,:,iz) = (0.25+0.25*a_helmh)*ti_turn*dpydt(:,:,iz)
      pz(:,:,iz) = (0.25+0.25*a_helmh)*ti_turn*dpzdt(:,:,iz)
    else
      px(:,:,iz) = (0.25*ti_turn)*dpxdt(:,:,iz)
      py(:,:,iz) = (0.25*ti_turn)*dpydt(:,:,iz)
      pz(:,:,iz) = (0.25*ti_turn)*dpzdt(:,:,iz)
    end if
  end do

END
