PRO progress,dir,tmax=tmax
  default,dir,'run27f_AL'
  f=read_table('data/'+dir+'/stellarmasses.txt')
  default,tmax,max(f[0,*])
  w=where(f[0,*] lt tmax)
  !p.multi=[0,1,2]
  plhist,f[0,w]+f[2,w],min=0,max=49,bin=1,title=dir,xtitle='time [My]',ytitle='N'
  oplot,[1,1]*tmax,[0,1e4],line=2
  print,'total mass, high mass stars:',total(f[1,w]),n_elements(where(f[2,w] lt 100))
  plot,f[0,w],total(f[1,w],/cum),xr=[0,50],yr=[0,5e4],xtitle='time [My]',ytitle='mass'
END
