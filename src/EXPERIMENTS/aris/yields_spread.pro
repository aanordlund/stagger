FUNCTION read_tbl,file
  openr,u,/get,file
  readf,u,n
  tbl=fltarr(2,n)
  readf,u,tbl
  free_lun,u
  return,tbl
END

PRO yields_spread,dir,tmax=tmax,dt=dt,every=nev
  default,dir,'run27g'
  if !d.name eq 'PS' then setdev,'ps',file='yields_spread_'+dir+'.ps'
  f=read_table('data/'+dir+'_FE/stellarmasses.txt')
  sz=size(f) & nstars=sz[2]
  al=read_tbl('data/YIELDS/al_LC06b.tbl') & al_mass=reform(al[0,*]) & al_yields=reform(al[1,*])
  fe=read_tbl('data/YIELDS/fe_LC06c.tbl') & fe_mass=reform(fe[0,*]) & fe_yields=reform(fe[1,*])

  cd,'data/'+dir+'_AL',current=save
  open,a1,nt=nt,times=times
  cd,save

  cd,'data/'+dir+'_FE',current=save
  open,a2,nt=nt,times=times
  cd,save

  default,dt,times[2]-times[1]
  default,nev,300

  !p.multi=[0,1,1]
  plot,[0,tmax],[1e-2,1e+2],yst=1,/ylog,/nodata, $
    xtitle='time [My]',ytitle='60Fe/26Al',xmargin=[12,2]

  t0=long(10./dt)
  for t=t0,nt-1 do begin
    time=t*dt
    rho=a1[9*t]
    w=where (rho gt 500.,nw)
    if nw gt 0 then begin
      ratio=a2[9*t+8]/a1[9*t+8]
      ratio=ratio[w]
      ratio=every(ratio,nev)
      oplot,replicate(time,nw),ratio,psym=6,symsize=0.025
    end
  end

  default,tmax,max(f[0,*])
  w=where(f[0,*] lt tmax and f[2,*] lt 1e3 and f[1,*] gt al_mass[0],nw)
  ;w=where(f[0,*] lt tmax and f[2,*] lt 1e3 and f[1,*] gt 8.,nw)
  altot=0.
  fetot=0.
  for i=0,nw-1 do begin
    mass=f[1,w[i]]
    time=f[0,w[i]]
    al=exp(interpol(alog(al_yields),alog(al_mass),alog(mass)))
    fe=exp(interpol(alog(fe_yields),alog(fe_mass),alog(mass)))
    altot=altot+al
    fetot=fetot+fe
    sz=0.4
    plots,time,fe/al,psym=1,color=thecolor('orange'),syms=sz
    plots,time,fetot/altot,psym=1,color=thecolor('red'),syms=sz
  end

  if !d.name eq 'PS' then device,/close
END
