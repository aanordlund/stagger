; $Id: find_regions_all.pro,v 1.2 2011/11/06 23:50:18 aake Exp $

PRO find_regions_all, a, nt, rho_min, out=out, images=images, t0=t0, dt=dt, wait=wt, $
    psym=psym, symsize=symsize
 if !d.name eq 'PS' then begin
   default,symsize,1
   default,psym,3
 end else begin
   default,symsize,0.1
   default,psym,1
 end
 default,out,'aris_tmp.txt'
 default,t0,0				; first snapshot
 default,dt,0.05			; time btw snapshots (Myr)
 default,wt,0.5				; wait 0.5 sec btw snapshots
 ws,0
 plot,[t0,nt-1]*dt,[1e-10,1e0],/ylog,/nodata,xtitle='t [Myr]',ytitle='d/rho'
 close,1
 openw,1,out
 for t=t0,nt-1 do begin
  rho=a[9*t]
  rho1=rho
  d=a[9*t+8]
  sz=size(a)
  nx=sz[1]
  ny=sz[2]
  nz=sz[3]
  ireg=0
  rhomax=max(rho)
  while (1) do begin
    ireg=ireg+1
    rho_max=max(rho,w0)
    if (rho_max) lt (rho_min) then break
    iz=w0/(nx*ny)
    w0=w0-iz*nx*ny
    iy=w0/nx
    w0=w0-iy*nx
    ix=w0
    rho_max=1e6
    region=search3d_periodic(rho,ix,iy,iz,rho_min,rho_max,/diag)
    nr=n_elements(region)
    if nr gt 4 then begin
      print,t*dt,ireg,nr,aver(rho(region)),aver(d(region)),aver(d(region)/rho(region))
      printf,1,t*dt,ireg,nr,ix,iy,iz,aver(rho(region)),aver(d(region)),aver(d(region)/rho(region))
      ws,0
      plots,psym=psym,symsize=symsize,t*dt,aver(d(region)/rho(region))
      wait,0
    end
    ;
    if keyword_set(images) then begin
      ws,2,xsize=600,ysize=200
      rho1(region)=2.*rhomax
      imsz,200
      image,aver(rho1,1),0,title=string(t*dt,format='(f5.2)')		; along x-axis
      image,aver(rho1,2),1						; along y-axis
      image,aver(rho1,3),2						; along z-axis
      wait,0
    end
    ;
    rho(region)=0.
  end
  wait,wt					; wait wt seconds
 end
 close,1
 if !d.name eq 'PS' then device,/close
END
