FUNCTION read_tbl,file
  openr,u,/get,file
  readf,u,n
  tbl=fltarr(2,n)
  readf,u,tbl
  free_lun,u
  return,tbl
END

PRO Yields_O18toO16,dir,tmax=tmax
  default,dir,'test18O'; doesnt matter which
  f=read_table('data/'+dir+'/stellarmasses.txt'); because stellarmasses.txt 's are the same
  sz=size(f) & nstars=sz[2]
  O16=read_tbl('data/YIELDS/16O_WW.tbl') & O16_mass=reform(O16[0,*]) & O16_yields=reform(O16[1,*])
  O18=read_tbl('data/YIELDS/18O_WW.tbl') & O18_mass=reform(O18[0,*]) & O18_yields=reform(O18[1,*])
  default,tmax,max(f[0,*])
  ;w=where(f[0,*] lt tmax and f[2,*] lt 1e3 and f[1,*] gt O16_mass[0],nw)
  w=where(f[0,*] lt tmax and f[2,*] lt 1e3 and f[1,*] gt 8.,nw)

  !p.multi=[0,1,1]
  ws,0
  plot,[0,tmax],[1e-4,1e-1],yst=1,/ylog,/nodata, $
    xtitle='time [Ma]',ytitle='!u18!nO/!u16!nO',xmargin=[12,2]
  ws,2
  plot,[1,100],[1e-5,1e+1],yst=1,/ylog,/nodata, $
    xtitle='mass',ytitle='yield'
  O16tot=0.
  O18tot=0.
  for i=0,nw-1 do begin
    mass=f[1,w[i]]
    time=f[0,w[i]]
    ;O16=interpol(O16_yields,O16_mass,mass)
    ;O18=interpol(O18_yields,O18_mass,mass)
    O16=exp(interpol(alog(O16_yields),alog(O16_mass),alog(mass)))
    O18=exp(interpol(alog(O18_yields),alog(O18_mass),alog(mass)))
    O16tot=O16tot+O16
    O18tot=O18tot+O18
    ws,0
    plots,time,O18/O16,psym=1,color=thecolor('orange'); individual yields
    ; plots,time,O18,psym=1,color=thecolor('orange')
    plots,time,O18tot/O16tot,psym=1,color=thecolor('red'); accumulated yields
    ws,2
    plots,mass,O16,psy=1,color=thecolor('blue')
    plots,mass,O18,psy=1,color=thecolor('green')
    plots,mass,O18/O16,psy=1 ; *NOTICE* the factor of *1E-5 (was here)
  end
END
