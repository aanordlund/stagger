PRO find_regions, a, t, rho_min, out=out
  rho=a[9*t]
  d=a[9*t+8]
  sz=size(a)
  nx=sz[1]
  ny=sz[2]
  nz=sz[3]
  ireg=0
  default,out,'aris_tmp.txt'
  openw,1,out
  while (1) do begin
    ireg=ireg+1
    w=where(rho gt rho_min,nw)
    if (nw le 0) then return
    w0=w[0]
    iz=w0/(nx*ny)
    w0=w0-iz*nx*ny
    iy=w0/nx
    w0=w0-iy*nx
    ix=w0
    rho_max=1e6
    region=search3d_periodic(rho,ix,iy,iz,rho_min,rho_max)
    nr=n_elements(region)
    print,ireg,nr,aver(rho(region)),aver(d(region)),aver(d(region)/rho(region))
    printf,1,ireg,nr,aver(rho(region)),aver(d(region)),aver(d(region)/rho(region))
    ;
    rhomax=max(rho)
    rho(region)=2.*rhomax
    imsz,200
    image,aver(rho,1),0,title=ireg		; along x-axis
    image,aver(rho,2),1				; along y-axis
    image,aver(rho,3),2				; along z-axis
    wait,2					; wait 2 seconds
    ;
    rho(region)=0.
  end
  close,1
END
