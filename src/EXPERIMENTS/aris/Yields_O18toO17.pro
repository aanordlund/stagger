FUNCTION read_tbl,file
  openr,u,/get,file
  readf,u,n
  tbl=fltarr(2,n)
  readf,u,tbl
  free_lun,u
  return,tbl
END

PRO Yields_O18toO17,dir,tmax=tmax
  default,dir,'test18O'; doesnt matter which
  f=read_table('data/'+dir+'/stellarmasses.txt'); because stellarmasses.txt 's are the same
  sz=size(f) & nstars=sz[2]
  O17=read_tbl('data/YIELDS/17O_WW.tbl') & O17_mass=reform(O17[0,*]) & O17_yields=reform(O17[1,*])
  O18=read_tbl('data/YIELDS/18O_WW.tbl') & O18_mass=reform(O18[0,*]) & O18_yields=reform(O18[1,*])
  default,tmax,max(f[0,*])
  ;w=where(f[0,*] lt tmax and f[2,*] lt 1e3 and f[1,*] gt O17_mass[0],nw)
  w=where(f[0,*] lt tmax and f[2,*] lt 1e3 and f[1,*] gt 8.,nw)

  !p.multi=[0,1,1]
  ws,0
  plot,[0,tmax],[1e-2,1e+2],yst=1,/ylog,/nodata, $
    xtitle='time [Ma]',ytitle='!u18!nO/!u17!nO',xmargin=[12,2]
  ws,2
  plot,[1,100],[1e-5,1e+2],yst=1,/ylog,/nodata, $
    xtitle='mass',ytitle='yield'
  O17tot=0.
  O18tot=0.
  for i=0,nw-1 do begin
    mass=f[1,w[i]]
    time=f[0,w[i]]
    ;O17=interpol(O17_yields,O17_mass,mass)
    ;O18=interpol(O18_yields,O18_mass,mass)
    O17=exp(interpol(alog(O17_yields),alog(O17_mass),alog(mass)))
    O18=exp(interpol(alog(O18_yields),alog(O18_mass),alog(mass)))
    O17tot=O17tot+O17
    O18tot=O18tot+O18
    ws,0
    plots,time,O18/O17,psym=1,color=thecolor('orange'); individual yields
    ; plots,time,O18,psym=1,color=thecolor('orange')
    plots,time,O18tot/O17tot,psym=1,color=thecolor('red'); accumulated yields
    ws,2
    plots,mass,O17,psy=1,color=thecolor('cyan')
    plots,mass,O18,psy=1,color=thecolor('green')
    plots,mass,O18/O17,psy=1 ; *NOTICE* the factor of *1E-5 (was here)
  end
END
