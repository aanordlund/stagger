#!/bin/csh

set echo

set job   = $1
set ncore = $2
set prog  = aris_ifort.x

set dir = .
set dir = data/${job}
mkdir -p $dir

set id  = `date +%y%m%d_%H%M`
set in  = in_$id.txt
set out = out_$id.txt

cp ${job}_input.txt $dir/$in
#cp ~/bin/$MACHTYPE/$prog $dir/$prog
cp ../../$prog $dir/$prog
ln -sf $dir/$out ${job}_output.txt

cd $dir
setenv OMP_NUM_THREADS $ncore
taskset -c 0-15 ./$prog $in >& $out
