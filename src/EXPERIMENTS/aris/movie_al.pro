; to make images for a movie:

set_plot,'x'
sz=400
window,0,xsize=sz,ysize=sz
imsz,sz

p=0.3                                           ; possibly adjust the contrast
zrange=[1e-8,1e-3]^p                              ; possibly adjust the 400

dir='data/run13/'                               ; change for different cases
cd,dir,cur=cur
open,a,'snapshot.dat',nt=nt
cd,cur
tsnap=0.05                                    	; take from input file

d=2
loadct,0
name='movie1'                                   ; movie name
name='movie_al1'                              ; movie name
loadct,3
name='movie_al2'                              ; movie name
;loadct,39
;name='movie_al3'                              ; movie name

i0=0

i1=nt-1
i1=200

tvlct,/get,r,g,b

dev=!d.name
set_plot,'z'
device,set_resolution=[sz,sz]

file_mkdir,dir+name

;for i=0,nt-1 do begin
for i=i0,i1 do begin
 set_plot,'z'
 title=string(i*tsnap,format='(f4.1)')+' Myr'
 tmp=aver(a[9*i+8],d)/aver(a[9*i],d)
 ;tmp=aver(a[9*i+8],d)
 print,min(tmp,max=max),max
 tmp=tmp^p
 image,tmp,title=title,zr=zrange,/bold
 im=tvrd()
 file=dir+name+'/'+str(i,format='(i5.5)')+'.png'
 write_png,file,im,r,g,b

 set_plot,dev
 image,tmp,title=title,wait=0.1,zr=zrange,/minmax,/bold
end

END
