; $Id: Mn53_average.pro,v 1.1 2012/11/17 13:28:41 aris Exp $
PRO Mn53_average, dir, dt=dt
  if !d.name eq 'PS' then setdev,'ps',file='Mn53_average_'+dir+'.ps'
  default,dir,'test53Mn'
  solar=elements()
  cd,'data/'+dir,current=save
  open,a,nt=nt,times=times
  if n_elements(times) gt 0 then default,dt,times[2]-times[1] else default,dt,0.1
  cd,save
  !p.multi=0
  plot,[0,50],[1e-12,1e-3],/ylog,/nodata,title=dir ;calibrate limits
  for t=0,nt-1 do begin
    mn53=a[9*t+8] 	; juicy 53Mn
    rho=a[t*9]
    mn55=solar.bymass[13]*rho ;stable Mn: using Cr value (very similar)
    w=where(rho gt 1e3,nw)
    if nw gt 0 then $
      plots,t*dt,aver(mn53[w])/aver(mn55[w]),psy=1,symsize=0.5
    wait,0.
  end
  if !d.name eq 'PS' then device,/close
END
