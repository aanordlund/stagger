PRO alaver,a,nt,dt,t0=t0,reset=reset
  cd,cur=cwd
  cwd=fileroot(cwd,tail=tail)
  print,tail
  plot,[0,nt]*dt,[1e-8,1e-2],/nodata,/ylog,title=tail
  default,t0,10
  ddav=fltarr(nt)
  ddav1=fltarr(nt)
  rhoav=fltarr(nt)
  times=fltarr(nt)
  if (not keyword_set(reset)) and exists('alaver.idlsave') then begin
    restore,'alaver.idlsave'
    ddav[0:s.nt-1]=s.ddav[0:s.nt-1]
    ddav1[0:s.nt-1]=s.ddav1[0:s.nt-1]
    rhoav[0:s.nt-1]=s.rhoav[0:s.nt-1]
    times[0:s.nt-1]=times[0:s.nt-1]
    print,'previous IDL save file: nt =', s.nt
    t1=s.nt
  end else begin
    t1=t0
  end
  imsz,200
  for t=t1,nt-1 do begin
    times[t]=dt*t
    rho=a[9*t]
    d=a[8+9*t]
    dd=d/rho
    tt=a[4+9*t]/rho*45
    ddav[t]=aver(dd)
    plots,t*dt,ddav[t],psy=6
    rhoav[t]=aver(rho)
    plots,t*dt,rhoav[t]/50.*1e-6,psy=3
    w=where(rho gt 2000 and tt lt 2000,nw)
    if (nw gt 0) then begin
      ddav1[t]=aver(dd(w))
      plots,t*dt,ddav1[t],psy=1
    end 
    w=where(tt gt 3e7,nw)
    if (nw gt 0) then plots,t*dt,aver(dd(w)),psy=2
    print,t,nw
    s={ddav:ddav,ddav1:ddav1,rhoav:rhoav,nt:1+t,times:times}
    save,s,file='alaver.idlsave'
    image,aver(rho,1),tit=t
    wait,0.
  end
END
