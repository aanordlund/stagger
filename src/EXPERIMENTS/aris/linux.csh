#!/bin/csh

set id = `date +%y%m%d_%H%M`

cat >input_$id.txt <<END
&io     iread=-1 tstop=100.  tsnap=0.2 nscr=50    file='data/snapshot2.dat' io_word=1 /
&run    Cdt=0.4 iseed=-1 idbg=0 do_trace=f / Cdt: Courant condition (leave it), iseed: random seed, idbg: debug, do_trace: trace calls
&vars   do_pscalar=f do_mhd=t / do_pscalar: turn on passive scalar, do_mhd: use magnetic fields
&grid   mx=100 my=100 mz=100 sx=40 sy=40 sz=40 / m[xyz]: number of grid pts, s[xyz]: box size (code units)
&pde    do_loginterp=t do_2nddiv=t nu1=0.1 nu2=0.7 nur=.15 csound=15.48 / log interpolation, 2nd order divergence, nu?: viscosity params, csound: sound speed
&bdry   / boundary value params (check output for defaults)
&slice  do_slice=f / do_slice=t -> output 2-D slice data for animations
&quench / quenching trick (off by default)
&eqofst do_ionization=f / ionization handled in detail or not
&part   / tracer particles (off by default)
&force  do_force=f ampl_turb=24. t_turn=10. / force parameters (driving turbulence, off here)
&selfg  grav=8.34343e-4 / self gravity parameter (equivalent to changing mean density)
&init   r0=70. b0=10 / initialization parameters
&cool   do_cool=t do_table=f do_power=t turnon=-1 do_heat=t do_extraterm=f G0=6.e-24 rho_crit=350. / cooling function parameters
&expl   do_expl=t Tlimit=1.e5 rholim=700. masslim=4500. ctime=1 do_delayexpl=t
        wsn=4 do_initialSNE=t tSN=0.006 nSN=3 SNtemp=5.e7 rhoav=7. wtime=1/  exposion (SNe) parameters
&wind   do_wind=t windtemp=1.5e4 ww2=12. errw=0.01 pwind=25./  stellar wind parameter
END

nice +19
setenv OMP_NUM_THREADS 1
ln -sf output_$id.txt output.txt
./aris_ifort.x input_$id.txt >& output_$id.txt
