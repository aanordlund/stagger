FUNCTION read_tbl,file
  openr,u,/get,file
  readf,u,n
  tbl=fltarr(2,n)
  readf,u,tbl
  free_lun,u
  return,tbl
END

PRO yields,dir,tmax=tmax
  default,dir,'run27f_AL'
  f=read_table('data/'+dir+'/stellarmasses.txt')
  sz=size(f) & nstars=sz[2]
  al=read_tbl('data/YIELDS/al_LC06b.tbl') & al_mass=reform(al[0,*]) & al_yields=reform(al[1,*])
  fe=read_tbl('data/YIELDS/fe_LC06b.tbl') & fe_mass=reform(fe[0,*]) & fe_yields=reform(fe[1,*])
  default,tmax,max(f[0,*])
  ;w=where(f[0,*] lt tmax and f[2,*] lt 1e3 and f[1,*] gt al_mass[0],nw)
  w=where(f[0,*] lt tmax and f[2,*] lt 1e3 and f[1,*] gt 8.,nw)

  !p.multi=[0,1,1]
  ws,0
  plot,[0,tmax],[1e-3,1e+1],yst=1,/ylog,/nodata, $
    xtitle='time',ytitle='60Fe/27Al',xmargin=[12,2]
  ws,2
  plot,[1,100],[1e-8,1e-3],yst=1,/ylog,/nodata, $
    xtitle='mass',ytitle='yield'
  altot=0.
  fetot=0.
  for i=0,nw-1 do begin
    mass=f[1,w[i]]
    time=f[0,w[i]]
    ;al=interpol(al_yields,al_mass,mass)
    ;fe=interpol(fe_yields,fe_mass,mass)
    al=exp(interpol(alog(al_yields),alog(al_mass),alog(mass)))
    fe=exp(interpol(alog(fe_yields),alog(fe_mass),alog(mass)))
    altot=altot+al
    fetot=fetot+fe
    ws,0
    plots,time,fe/al,psym=1,color=thecolor('cyan')
    ; plots,time,fe,psym=1,color=thecolor('orange')
    plots,time,fetot/altot,psym=1
    ws,2
    plots,mass,al,psy=1,color=thecolor('cyan')
    plots,mass,fe,psy=1,color=thecolor('orange')
    plots,mass,fe/al*1e-5,psy=1
  end
END
