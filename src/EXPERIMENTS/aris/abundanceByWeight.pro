PRO ABUNDANCEBYWEIGHT, atomicWeight, exponent12
;    example for Mn:     54.94 ,       5.5
;                        
; try to run elements() before this, if it doesn't work-
;
; exponents for solar abundances in the H = 1e12 notation, same order as elements.pro :
;
;             H   He      C     N     O     Ne    Na   Mg    Al    Si    S     K     Ca    Cr    Fe    Ni
expAbundance=[12, 10.899, 8.39, 7.83, 8.69, 7.87, 6.3, 7.55, 6.46, 7.54, 7.19, 5.11, 6.34, 5.65, 7.47, 6.22]
solar=elements()
something=solar.mass
; initialise
denominator = 0.
; compute the "by weight" denominator
for i = 0,15 do begin
	denominator = denominator + (something[i] * 10.^expAbundance[i])
	print, denominator
end
; calculate:
abundanceW = atomicWeight^exponent12 / denominator
; spit it out
print, abundanceW

END
