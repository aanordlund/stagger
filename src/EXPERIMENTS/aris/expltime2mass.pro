PRO expltime2mass,dir,tmax=tmax
  if !d.name eq 'PS' then setdev,'ps',file='expltime2mass.ps'
  !p.multi=[0,1,1]
  default,dir,'run27f_AL
  f=read_table('data/'+dir+'/stellarmasses.txt')
  default,tmax,max(f[0,*])
  w=where(f[0,*] lt tmax)
  plot,f[0,w]+f[2,w],f[1,w],xr=[0,50],yr=[1,100],/ylog,psym=1,syms=0.5, $
    xtitle='Explosion time [My]',ytitle='Mass',title=dir+' tmax='+str(tmax)
  if !d.name eq 'PS' then close,/dev
END
