PRO export2ramses,dir,data=data,t=t
  default,data,'data'
  default,t,300 
  open,a,data+'/'+dir+'/snapshot.dat',nt=nt
  sz=size(a)
  f=fltarr(12,sz[1],sz[2],sz[3])
  nvar=9
  gamma=5./3.
  f[ 0,*,*,*]=a[0+t*nvar]
  f[ 1,*,*,*]=xup(fux(a,t,nvar=nvar))
  f[ 2,*,*,*]=yup(fuy(a,t,nvar=nvar))
  f[ 3,*,*,*]=zup(fuz(a,t,nvar=nvar))
  f[ 4,*,*,*]=(gamma-1.)*a[4+t*nvar]
  f[ 5,*,*,*]=a[5+t*9]
  f[ 6,*,*,*]=a[6+t*9]
  f[ 7,*,*,*]=a[7+t*9]
  f[ 8,*,*,*]=shift(a[5+t*9],-1,0,0)
  f[ 9,*,*,*]=shift(a[6+t*9],0,-1,0)     
  f[10,*,*,*]=shift(a[7+t*9],0,0,-1)
  f[11,*,*,*]=a[8+t*9]
  out=data+'/'+dir+'/ramses'
  spawn,'mkdir -p '+out
  mk_grafic,f,dir=out
END
