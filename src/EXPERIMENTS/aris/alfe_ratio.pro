FUNCTION read_tbl,file
  openr,u,/get,file
  readf,u,n
  tbl=fltarr(2,n)
  readf,u,tbl
  free_lun,u
  return,tbl
END

PRO alfe_ratio,dir,tmax=tmax,dt=dt,every=nev,t0=t0,xrange=xrange,rholim=rholim,table=table, $
    ps=ps,data=data,toff=toff
  default,dir,'run27g'
  default,xrange,[0,20]
  default,t0,0
  default,nev,300
  default,rholim,1e3
  default,table,'LC06c.tbl'
  default,data,'data_sc2_aake'
  default,toff,10

  dname=!d.name
  if keyword_set(ps) then setdev,'ps',file='alfe_ratio_'+dir+'.ps'

  f=read_table(data+'/'+dir+'_FE/stellarmasses.txt')
  sz=size(f) & nstars=sz[2]
  al=read_tbl(data+'/YIELDS/al_'+table) & al_mass=reform(al[0,*]) & al_yields=reform(al[1,*])
  fe=read_tbl(data+'/YIELDS/fe_'+table) & fe_mass=reform(fe[0,*]) & fe_yields=reform(fe[1,*])

  cd,data+'//'+dir+'_AL',current=save
  open,a1,nt=nt,times=times
  cd,save

  cd,data+'/'+dir+'_FE',current=save
  open,a2,nt=nt,times=times
  cd,save

  el=elements()

  if n_elements(times) eq 0 then begin
    default,dt,0.05
    times=dt*findgen(nt)+t0
  end else begin
    default,dt,times[2]-times[1]
  end

  !p.multi=[0,1,1]
  plot,xrange,[1e-2,1e+2],yst=1,/ylog,/nodata,xst=3, $
    xtitle='time [Myr]',ytitle='!u60!nFe/!u26!nAl',xmargin=[12,2]

  loadct,0,/silent
  for t=0,nt-1 do begin
    time=times[t]-toff
    if (time gt xrange[0] and time le xrange[1]) then begin
      rho=a1[9*t]
      rhomax=max(rho)
      w=where (rho gt rholim,nw)
      if nw gt 0 then begin
        ratio=a2[9*t+8]/a1[9*t+8]*el.mass[8]/el.mass[14]
        ratio=ratio[w]
        ratio=every(ratio,nev)
        oplot,replicate(time,nw),ratio,psym=6,symsize=0.025,color=100
      end
    end else begin
      rhomax=0.
      nw=-1
    end
    print,time,rhomax,nw
  end
  loadct,39,/silent

  default,tmax,max(f[0,*])
  w=where(f[0,*] lt tmax and f[2,*] lt 1e3 and f[1,*] gt al_mass[0],nw)
  ;w=where(f[0,*] lt tmax and f[2,*] lt 1e3 and f[1,*] gt 8.,nw)
  altot=0.
  fetot=0.
  for i=0,nw-1 do begin
    mass=f[1,w[i]]
    time=f[0,w[i]]-toff
    al=exp(interpol(alog(al_yields),alog(al_mass),alog(mass)))
    fe=exp(interpol(alog(fe_yields),alog(fe_mass),alog(mass)))
    altot=altot+al
    fetot=fetot+fe
    sz=0.4
    if (time gt xrange[0] and time le xrange[1]) then begin
      plots,time,fe/al,psym=1,color=thecolor('orange'),syms=sz
      plots,time,fetot/altot,psym=1,color=thecolor('red'),syms=sz
    end
  end

  if keyword_set(ps) then begin
    device,/close
    setdev,dname
  end 
END
