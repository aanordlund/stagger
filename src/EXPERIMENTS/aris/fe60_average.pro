; $Id: fe60_average.pro,v 1.2 2011/11/06 23:50:12 aake Exp $
PRO fe60_average, dir, dt=dt
  if !d.name eq 'PS' then setdev,'ps',file='fe60_average_'+dir+'.ps'
  default,dir,'run27g_FE'
  solar=elements()
  cd,'data/'+dir,current=save
  open,a,nt=nt,times=times
  if n_elements(times) gt 0 then default,dt,times[2]-times[1] else default,dt,0.1
  cd,save
  !p.multi=0
  plot,[0,50],[1e-12,1e-3],/ylog,/nodata,title=dir ;calibrate limits
  for t=0,nt-1 do begin
    fe60=a[9*t+8] 	; juicy Iron 60
    rho=a[t*9]
    fe56=solar.bymass[14]*rho ;idexes start from 0, so '14' for normal, boring iron 56
    w=where(rho gt 1e3,nw)
    if nw gt 0 then $
      plots,t*dt,aver(fe60[w])/aver(fe56[w]),psy=1,symsize=0.5
    wait,0.
  end
  if !d.name eq 'PS' then device,/close
END
