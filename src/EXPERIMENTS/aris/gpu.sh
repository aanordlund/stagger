#!/bin/bash
# 
# @ job_name         = tracetest26Al
# @ notification     = never
# @ output           = $(jobid).out
# @ error            = $(jobid).out
# @ class            = astro_gpu
# @ job_type         = parallel
# @ node_usage       = not_shared
# @ wall_clock_limit = 48:00:00
# @ node             = 1
# @ tasks_per_node   = 8
# @ resources        = ConsumableCpus(1) ConsumableMemory(3gb)
# @ queue

export OMPI_MCA_mpi_paffinity_alone=1
export MACHTYPE=nehalem

/software/astro/bin/jobcontrol.csh ./job.csh tracetest26Al 8
