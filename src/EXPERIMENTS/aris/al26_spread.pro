; $Id: al26_spread.pro,v 1.2 2011/11/06 23:50:04 aake Exp $
PRO al26_spread, dir, dt=dt, every=nev
  if !d.name eq 'PS' then setdev,'ps',file='al26_spread_'+dir+'.ps'
  default,dir,'run27g_AL'
  default,nev,300
  solar=elements()
  cd,'data/'+dir,current=save
  open,a,nt=nt,times=times
  if n_elements(times) gt 0 then default,dt,times[2]-times[1] else default,dt,0.1
  cd,save
  !p.multi=0
  plot,[0,50],[1e-7,1e-3],/ylog,/nodata,title=dir
  for t=0,nt-1 do begin
    al26=a[9*t+8]
    rho=a[t*9]
    al27=solar.bymass[8]*rho
    w=where(rho gt 1e3,nw)
    if nw gt 0 then begin
      time=replicate(t*dt,nw)
      ratio=al26[w]/al27[w]
      oplot,every(time,nev),every(ratio,nev),psym=3
    end
    wait,0.
  end
  if !d.name eq 'PS' then device,/close
END
