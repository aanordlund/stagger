; to make images for a movie:

set_plot,'x'
sz=400
window,0,xsize=sz,ysize=sz
imsz,sz

p=0.1                                           ; possibly adjust the contrast
zrange=[1.,1e6]^p                              ; possibly adjust the 400

dir='data/run13/'                               ; change for different cases
cd,dir,cur=cur
open,a,'snapshot.dat',nt=nt
cd,cur
tsnap=0.05                                    	; take from input file

d=2
loadct,0
name='movie1'                                   ; movie name
name='movie_energy1'                              ; movie name
loadct,3
name='movie_energy2'                              ; movie name
;loadct,39
;name='movie_energy3'                              ; movie name

i0=0
;i0=200

i1=nt-1
i1=200
;i1=367

tvlct,/get,r,g,b

dev=!d.name
set_plot,'z'
device,set_resolution=[sz,sz]

file_mkdir,dir+name

;for i=0,nt-1 do begin
for i=i0,i1 do begin
 set_plot,'z'
 title=string(i*tsnap,format='(f4.2)')+' Myr'
 image,aver(a[9*i+4],d)^p,title=title,zr=zrange,/bold
 im=tvrd()
 file=dir+name+'/'+str(i,format='(i5.5)')+'.png'
 write_png,file,im,r,g,b

 set_plot,dev
 image,aver(a[9*i+4],d)^p,title=title,wait=0.1,zr=zrange,/minmax,/bold
end

END
