#!/bin/csh

#set run = run34g_AL
#set run = run35_AL
#set run = trc908to939
#set run = trc825to870
#set run = traceX1
#set run = trace27g

#set run = trc651to719
#set run = X128
#set run = X128notFG
set run = X256b
#set run = X128b
#set run = X128Fe

#set ncore = 20
set ncore = 12
set first = 12

set data = data_sc2

/software/astro/bin/jobcontrol.csh ./job.csh $run $ncore $first $data
