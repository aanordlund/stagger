! $Id: force.f90,v 1.1 2011/01/23 23:03:28 aake Exp $
!**********************************************************************
MODULE forcing

logical conservative
logical do_force

END MODULE
!**********************************************************************
  SUBROUTINE init_force
    USE params
    USE forcing
    implicit none
    character(len=mid):: id='$Id: force.f90,v 1.1 2011/01/23 23:03:28 aake Exp $'

    call print_id(id)

    conservative = .true.
    do_force = .false.

    call read_force
  END SUBROUTINE

!***********************************************************************
  SUBROUTINE read_force
    USE params
    USE forcing
    namelist /force/ do_force

    rewind (stdin); read (stdin,force)
    if (master) write (*,force)

    call init_selfg
  END SUBROUTINE

!***********************************************************************
  SUBROUTINE forceit (r,Ux,Uy,Uz,xdnr,ydnr,zdnr,dpxdt,dpydt,dpzdt)
  USE params
  implicit none
  real, dimension(mx,my,mz):: r,Ux,Uy,Uz,xdnr,ydnr,zdnr,dpxdt,dpydt,dpzdt
  logical omp_in_parallel
!-----------------------------------------------------------------------
  if (omp_in_parallel()) then
    call forceit_omp (r,Ux,Uy,Uz,xdnr,ydnr,zdnr,dpxdt,dpydt,dpzdt)
  else
    !$omp parallel
    call forceit_omp (r,Ux,Uy,Uz,xdnr,ydnr,zdnr,dpxdt,dpydt,dpzdt)
    !$omp end parallel
  end if
  END SUBROUTINE

!***********************************************************************
  SUBROUTINE forceit_omp (r,Ux,Uy,Uz,xdnr,ydnr,zdnr,dpxdt,dpydt,dpzdt)
  USE params
  implicit none
  real, dimension(mx,my,mz):: r,Ux,Uy,Uz,xdnr,ydnr,zdnr,dpxdt,dpydt,dpzdt

  call selfgrav (r,xdnr,ydnr,zdnr,dpxdt,dpydt,dpzdt)
END SUBROUTINE
