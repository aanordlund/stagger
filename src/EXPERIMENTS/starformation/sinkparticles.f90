! $Id: sinkparticles.f90,v 1.30 2012/04/22 19:27:11 aake Exp $
!*******************************************************************************
MODULE stars_m
  USE params
  implicit none
  type :: star_t                                                                ! star data type
    real x,y,z                                                                  ! positions
    real px,py,pz                                                               ! momenta
    real dpx,dpy,dpz                                                            ! momenta
    real mass                                                                   ! mass
    real dmass                                                                  ! mass
    real t_SN                                                                   ! explosion time
    real density                                                                ! neigborhood density
    real t_create                                                               ! creation time
    real dxdt,dydt,dzdt                                                         ! dr/dr
    real dpxdt,dpydt,dpzdt                                                      ! dp/dt
    integer owner                                                               ! owner rank
  end type
  type(star_t), allocatable, dimension(:) :: star, ostar                        ! star data
  real w_prof, phi_bh
  real twrite                                                                   ! how often to write
  real twrote                                                                   ! last written
  real starmass                                                                 ! total star mass
  real xmax, ymax, zmax                                                         ! domain limits
  real xmin_mpi, ymin_mpi, zmin_mpi                                             ! domain limits
  real xmax_mpi, ymax_mpi, zmax_mpi                                             ! domain limits
  real xmin_box, ymin_box, zmin_box                                             ! box limits
  real xmax_box, ymax_box, zmax_box                                             ! box limits
  integer nstars                                                                ! number of stars
  integer pstars                                                                ! previous number of stars
  integer verbose                                                               ! for debugging
  integer, parameter:: m=2                                                      ! mass profile size
  real, dimension(-m:m,-m:m,-m:m) :: prf                                        ! density profile
  real rho_limit, ee_limit, rho_cut, max_distance                               ! accretion parameters
  logical do_SNe, do_sink, need_sync, accreted                                  ! need MPI sync, update
  real, allocatable, dimension(:,:,:):: Uxc, Uyc, Uzc                           ! centered velocities
  real, allocatable, dimension(:,:,:):: dpx, dpy, dpz                           ! centered momentum changes
  character(len=mfile):: sink_file                                              ! sink file name
CONTAINS

!-------------------------------------------------------------------------------
SUBROUTINE accretion (rho)                                                      ! accretion
  USE params, only: mx,my,mz
  implicit none
  real, dimension(mx,my,mz):: rho
  integer ix,iy,iz
!...............................................................................
  if (nstars==0) return
  do iz=1,mz
    do iy=1,my
      do ix=1,mx
        if (rho(ix,iy,iz) > rho_limit) then                                     ! density above limit?
          call accrete_on_star (rho, ix, iy, iz)                                ! accrete on nearest star
        end if
      end do
    end do
  end do
END SUBROUTINE

!-------------------------------------------------------------------------------
REAL FUNCTION mod2 (s1, s2, period)
  implicit none
  real s1, s2, period
  mod2 = mod(s1-s2+1.5*period,period)-0.5*period
END FUNCTION

!-------------------------------------------------------------------------------
SUBROUTINE accrete_on_star (rho, ix, iy, iz)
  USE params, x=>xm, y=>ym, z=>zm
  implicit none
  real, dimension(mx,my,mz):: rho
  integer ix, iy, iz, i, j 
  real distance, min_distance, dmass, dv, drho, rho_c
!...............................................................................
  min_distance = 1e10
  do i=1,nstars
    distance = sqrt(mod2(star(i)%x,x(ix),sx)**2 &
                  + mod2(star(i)%y,y(iy),sy)**2 &
                  + mod2(star(i)%z,z(iz),sz)**2)                                ! distance to star(i)
    if (distance < min_distance) then                                           ! smaller than previous?
      min_distance = distance                                                   ! new min distance
      j = i                                                                     ! index of nearest star
    end if
  end do
  dv = dx*dy*dz
  rho_c = rho_limit*rho_cut                                                     ! renormalize parameter
  if (min_distance < max_distance*dx) then
    dmass = (rho(ix,iy,iz)-rho_c)*dv                                            ! accrete this much
    star(j)%dmass = star(j)%dmass + dmass                                       ! add mass
    star(j)%dpx = star(j)%dpx + Uxc(ix,iy,iz)*dmass                             ! add x-momentum
    star(j)%dpy = star(j)%dpy + Uyc(ix,iy,iz)*dmass                             ! add y-momentum
    star(j)%dpz = star(j)%dpz + Uzc(ix,iy,iz)*dmass                             ! add z-momentum
    rho(ix,iy,iz) = rho_c                                                       ! remove cell mass
    drho = dmass/dv
    dpx(ix,iy,iz) = dpx(ix,iy,iz) + Uxc(ix,iy,iz)*drho                          ! momentum exchange
    dpy(ix,iy,iz) = dpy(ix,iy,iz) + Uyc(ix,iy,iz)*drho                          ! momentum exchange
    dpz(ix,iy,iz) = dpz(ix,iy,iz) + Uzc(ix,iy,iz)*drho                          ! momentum exchange
  end if
  if (verbose > 1) print'(1x,2i5,2x,3i5,2x,2f7.3,2(a,1pe9.2))',j,mpi_rank, &
    ix+mpi_x*mx, iy+mpi_y*my, iz+mpi_z*mz, &
    min_distance,max_distance*dx,' accretion', &
    star(j)%dmass,' mass',star(j)%mass
  if (star(j)%owner .ne. mpi_rank) need_sync = .true.                           ! accreted onto non-local star
  accreted = .true.
END SUBROUTINE

!-------------------------------------------------------------------------------
SUBROUTINE create_stars (rho,e)
  USE params, only: mx,my,mz
  implicit none
  real, dimension(mx,my,mz):: rho,e
  integer loc(3)
  real rho_max, xx, yy, zz, mass, rho_c
!...............................................................................
  rho_c = rho_limit*rho_cut                                                     ! renormalize parameter
  do while (maxval(rho) > rho_limit)                                            ! as long as there is excess 
    loc = maxloc(rho)                                                           ! find the max
    if (e(loc(1),loc(2),loc(3))/rho(loc(1),loc(2),loc(3)) > ee_limit) then
      return
    endif
    mass = 0.                                                                   ! zero initial mass
    call create_new_star (loc, mass)                                            ! add a new star
    call accretion (rho)                                                        ! accrete onto it
    need_sync = .true.                                                          ! created a new star
  end do
END SUBROUTINE

!-------------------------------------------------------------------------------
SUBROUTINE create_new_star (loc, mass)                                          ! add a star
  USE params, x=>xm, y=>ym, z=>zm
  implicit none
  integer loc(3), i
  real mass, distance
!...............................................................................
  if (verbose > 3) print*,'create:',mpi_rank,nstars,allocated(star)
  if (nstars == 0) then
    nstars = 1                                                                  ! first star!                    
    if (verbose > 2) print*,mpi_rank,': create0, allocate star(), size =', nstars, allocated(star)
    if (allocated(star)) deallocate(star); allocate (star(nstars))              ! first (re-)allocate
  else
    allocate (ostar(nstars))                                                    ! temporary
    ostar = star                                                                ! copy
    nstars = nstars+1                                                           ! one more
    if (verbose > 2) print*,mpi_rank,': create1, allocate star(), size =', nstars, allocated(star)
    deallocate (star)                                                           ! deallocate
    allocate (star(nstars))                                                     ! allocate
    star(1:nstars-1) = ostar                                                    ! copy
    deallocate (ostar)                                                          ! deallocate
  end if
  star(nstars)%x = x(loc(1))                                                    ! x-coordinate
  star(nstars)%y = y(loc(2))                                                    ! y-coordinate
  star(nstars)%z = z(loc(3))                                                    ! z-coordinate
  star(nstars)%px = mass*Uxc(loc(1),loc(2),loc(3))                              ! x-momentum
  star(nstars)%py = mass*Uyc(loc(1),loc(2),loc(3))                              ! y-momentum
  star(nstars)%pz = mass*Uzc(loc(1),loc(2),loc(3))                              ! z-momentum
  star(nstars)%dxdt = 0.
  star(nstars)%dydt = 0.
  star(nstars)%dzdt = 0.
  star(nstars)%dpxdt = 0.
  star(nstars)%dpydt = 0.
  star(nstars)%dpzdt = 0.
  star(nstars)%dpx = 0.                                                         ! increment
  star(nstars)%dpy = 0.                                                         ! increment
  star(nstars)%dpz = 0.                                                         ! increment
  star(nstars)%mass = mass                                                      ! mass
  star(nstars)%dmass = 0.                                                       ! increment
  star(nstars)%t_create = t                                                     ! time of creation
  star(nstars)%t_SN = 1e30                                                      ! no SN
  star(nstars)%owner = mpi_rank                                                 ! owner
  distance = sx
  do i=1,nstars-1
    distance = min(distance,sqrt(mod2(star(i)%x,star(nstars)%x,sx)**2 &
                                +mod2(star(i)%y,star(nstars)%y,sy)**2 &
                                +mod2(star(i)%z,star(nstars)%z,sz)**2))
  end do
  if (verbose <= 1) then
    print'(a,i4)', 'new-born star, no', nstars
  else
    print'(a,i4,/a,i4,/a,3i4,/2(a,3f7.3/),a,f7.3/,a,1pe10.2)', &
      'new-born star, no', nstars, &
      ' MPI process:', mpi_rank, &
      '    location:', loc, &
      ' coordinates:', x(loc(1)), y(loc(2)), z(loc(3)), &
      '    velocity:', Uxc(loc(1),loc(2),loc(3)), Uyc(loc(1),loc(2),loc(3)), Uzc(loc(1),loc(2),loc(3)), &
      '  *-distance:', distance, &
      '        mass:', mass
  end if
END SUBROUTINE

!-------------------------------------------------------------------------------
SUBROUTINE write_stars
  USE params, only: did_io, t
  implicit none
  integer i
  real t_SN
  character(len=mfile) fname, name
  integer, parameter:: version=1
!...............................................................................
  if (nstars==0 .or. .not.master) return
  if (did_io .or. (nstars > pstars) .or. (t > (twrote+twrite)) ) then
    fname = name(sink_file,'stars',file)
    open (sink_unit, file=fname, position='append', form='unformatted', status='unknown')
    write (sink_unit) version,nstars,t,size(star),sizeof(star)
    write (sink_unit) star
    close(sink_unit)
    if ( t > (twrote+twrite)) twrote = t
    if (verbose > 0) print*,nstars,' stars written to ',trim(fname)
!
    t_SN = 1e30
    do i=1,nstars
      if (star(i)%t_SN > t) then
	t_SN = min(t_SN,star(i)%t_SN)
      end if
    end do
    if (verbose > 0) print*,'next SN at time =',t_SN

    fname = name('stars.txt','snktxt',file)
    open (sink_unit,file=fname, form='formatted', status='unknown')
    write (sink_unit,*) nstars, t
    write (sink_unit,'(a7,a8,11a12)') &
      'i', &
      'x ','y ','z ', &
      'vx','vy','vz', &
      'm ','u ','rho', &
      't ','1/t_acc'
    do i=1,nstars
      if (star(i)%mass == 0.) star(i)%mass = 1e-30
      write(sink_unit,'(i6,12g12.4)') &
        i, star(i)%x, star(i)%y, star(i)%z, &
	star(i)%px/star(i)%mass, &
	star(i)%py/star(i)%mass, &
	star(i)%pz/star(i)%mass, &
	star(i)%mass, &
	sqrt(star(i)%px**2+star(i)%py**2+star(i)%pz**2)/star(i)%mass, &
	star(i)%density, &
	star(i)%t_create, &
	star(i)%dmass/star(i)%mass
    end do
    write (sink_unit,*) 'total mass =', sum(star(:)%mass)
    close (sink_unit)
  end if
END subroutine

!-------------------------------------------------------------------------------
SUBROUTINE read_stars
  implicit none
  logical exists
  integer nstars1, iostat, i, version
  real t0, t1, t_SN
  character(len=mfile) fname, name
!...............................................................................
  nstars = 0                                                                    ! no stars initially
  fname = name(sink_file,'stars',file)
  inquire (file=fname,exist=exists)
  if (exists) then 
    open (sink_unit, file=fname, status='old', form='unformatted')
    t1 = -1.
    do while (.true.)
      read (sink_unit,iostat=iostat) version, nstars1,t1
      if (iostat .ne. 0) exit
      if (t1 > t) exit
      nstars = nstars1
      if (verbose > 2) print*,mpi_rank,': read, allocate star(), size =', nstars, allocated(star)
      if (allocated(star)) deallocate (star)
      allocate (star(nstars))
      read (sink_unit) star
      t0 = t1
      twrote = t1
    end do
    close(sink_unit)
    if (nstars>0) then
      if (master) print*,'Data of ',nstars,' stars recovered from time ',t0
      if (do_SNe) then
        t_SN = 1e30
        do i=1,nstars
          if (star(i)%t_SN < t) star(i)%t_SN=1e30                               ! ignore previous SNe
          if (star(i)%t_SN > t) then
            t_SN = min(t_SN,star(i)%t_SN)
          end if
        end do
        if (master) print*,'next SN at time =',t_SN
      endif
      close(sink_unit)
      return
    end if
    if (allocated(star)) then
      deallocate (star)
      print*,mpi_rank,' deallocated'
    end if
  else
    if (master) print *,'no stars.dat file'
  endif 
  if (master) print *,'WARNING: no stars found for time', t
  nstars = 0
END subroutine 

!*******************************************************************************
END MODULE

!*******************************************************************************
MODULE supernova_m
  implicit none
  real(kind=8), parameter:: foe=1d51, msun=2d33
  real(kind=8):: rho_unit=1d-24, l_unit=3d18, t_unit=3d13
  real:: profile_power=4, profile_cells=4.
  real:: norm, SN_radius, profile_radius
CONTAINS

!*******************************************************************************
SUBROUTINE init_SN (mass, rho_SN, ee_SN)
  USE params, only: dx, dy, dz, pi
  USE stars_m, only: star
  implicit none
  real mass, rho_SN, ee_SN, dr, r, vol
  real(kind=8) ee_unit, m_unit
  integer, parameter:: nr=100
  integer i
!
  profile_radius = profile_cells*dx
  m_unit = rho_unit*l_unit**3
  ee_unit = m_unit*(l_unit/t_unit)**2
  ee_SN = (foe/ee_unit)/mass
  SN_radius = 4.*profile_radius
  !print*,'dx,prof_rad,SN_rad=',dx,profile_radius,SN_radius
  dr = SN_radius/nr
  vol = 0.
  do i=1,nr
    r = i*dr
    vol = vol+SN_profile(r)*4.*pi*r**2*dr
  end do
  rho_SN = mass/vol
END SUBROUTINE

!*******************************************************************************
REAL FUNCTION SN_profile (radius)
  implicit none
  real radius
  SN_profile = exp(-(radius/profile_radius)**profile_power)
END FUNCTION

!*******************************************************************************
END MODULE


!*******************************************************************************
SUBROUTINE supernova (r,e)
  USE params
  USE stars_m
  USE supernova_m
  implicit none
  real, dimension(mx,my,mz):: r,e
  integer ix, iy, iz, i, j 
  real distance, r_SN, ee_SN, rtmp
!
  do i=1,nstars
    if (star(i)%t_SN < t) then
      call init_SN (star(i)%mass, r_SN, ee_SN)
      if (master) print'(1x,a,i4,2f8.3,1p,2g12.3)', &
        'BOOM! i,t,tSN,ee,rho =',i,t,star(i)%t_SN,ee_SN,r_SN
      do iz=1,mz
      do iy=1,my
      do ix=1,mx
        distance = sqrt(mod2(star(i)%x,xm(ix),sx)**2 &
                      + mod2(star(i)%y,ym(iy),sy)**2 &
                      + mod2(star(i)%z,zm(iz),sz)**2)                                ! distance to star(i)
        if (distance < SN_radius) then
          !rtmp=r(ix,iy,iz)
          r(ix,iy,iz) = r(ix,iy,iz)+SN_profile(distance)*r_SN
          e(ix,iy,iz) = e(ix,iy,iz)+SN_profile(distance)*r_SN*ee_SN
          !if (SN_profile(distance) > 0.2) &
          !  print'(3i4,10g12.3)',ix,iy,iz,xm(ix),ym(iy),zm(iz), &
          !    distance,rtmp,r(ix,iy,iz),SN_profile(distance)
        end if
      end do
      end do
      end do
      star(i)%t_SN = 1e30
      star(i)%mass = 1e-30
      star(i)%px = 0.
      star(i)%py = 0.
      star(i)%pz = 0.
    endif
    need_sync = .true.                                                               ! set for next time step
  end do
END subroutine 

!*******************************************************************************
SUBROUTINE starformation (rho,e)
  USE stars_m
  USE params, only: mx,my,mz
  USE variables, only: Ux, Uy, Uz, px, py, pz
  implicit none
  real, dimension(mx,my,mz):: rho,e
  integer i
  real fsum
!...............................................................................
  do i=1,nstars                                                                 ! reset increments
    star(i)%dpx = 0.
    star(i)%dpy = 0.
    star(i)%dpz = 0.
    star(i)%dmass = 0.
  end do
  pstars = nstars                                                               ! remember number of stars 
  accreted = .false.

  allocate (Uxc(mx,my,mz), Uyc(mx,my,mz), Uzc(mx,my,mz))
  allocate (dpx(mx,my,mz), dpy(mx,my,mz), dpz(mx,my,mz))
  dpx=0.; dpy=0.; dpz=0.
  call xup_set (Ux, Uxc)
  call yup_set (Uy, Uyc)
  call zup_set (Uz, Uzc)

  call accretion (rho)                                                          ! accrete to nearby stars
  call create_stars (rho,e)                                                     ! create stars if needed
  if (verbose>3 .and. nstars>0) print*,mpi_rank,':a ',nstars,star(nstars)%x,star(nstars)%mass
  call sync_stars                                                               ! add increments
  if (verbose>3 .and. nstars>0) print*,mpi_rank,':b ',nstars,star(nstars)%x,star(nstars)%mass
  call write_stars                                                              ! keep track
  if (verbose>3 .and. nstars>0) print*,mpi_rank,':c ',nstars,star(nstars)%x,star(nstars)%mass
  if (master .and. nstars > 0) then
    starmass = sum(star(:)%mass)                                                ! master node includes all mass
  else
    starmass=0.
  end if
  
  do i=1,nstars                                                                 ! avoid repeateded summation
    star(i)%dmass = 0.                                                          ! for calls from xyz_move_stars
    star(i)%dpx = 0.
    star(i)%dpy = 0.
    star(i)%dpz = 0.
  end do

  call xdn1_set (dpx, Uxc); px = px - Uxc                                       ! recenter and subtract 
  call ydn1_set (dpy, Uyc); py = py - Uyc                                       ! recenter and subtract 
  call zdn1_set (dpz, Uzc); pz = pz - Uzc                                       ! recenter and subtract

  need_sync = .false.                                                           ! set for next time step
  deallocate (Uxc, Uyc, Uzc)
  deallocate (dpx, dpy, dpz)
END subroutine

!*******************************************************************************
SUBROUTINE init_stars
  USE params
  USE stars_m
  implicit none
 !..............................................................................
  w_prof = 1.                                                                   ! profile width (in grid units)
  rho_limit = 300.                                                              ! make stars above this
  ee_limit=1e4                                                                  ! T corresp to 100 km/s
  rho_cut = 0.90                                                                ! fraction of rho_lim
  max_distance = 5.                                                             ! max accretion distance (grids)
  verbose = 0                                                                   ! for debugging
  sink_file = 'stars.dat'                                                       ! default file name
  twrite = 0.                                                                   ! write every time step
  twrote = -999.                                                                ! needs to be defined later

  call read_explode                                                             ! read namelist
  if (.not. do_sink) return
  
  xmax = xmin + sx
  ymax = ymin + sy
  zmax = zmin + sz

  xmin_mpi = xm(1) - 0.5*dx                                                     ! domain bounds
  ymin_mpi = ym(1) - 0.5*dy
  zmin_mpi = zm(1) - 0.5*dz
  xmax_mpi = xmin_mpi + mx*dx
  ymax_mpi = ymin_mpi + my*dy
  zmax_mpi = zmin_mpi + mz*dy

  xmin_box = xmin - 0.5*dx                                                      ! box bounds
  ymin_box = ymin - 0.5*dy
  zmin_box = zmin - 0.5*dz
  xmax_box = xmin_box + sx
  ymax_box = ymin_box + sy
  zmax_box = zmin_box + sz

  call read_stars                                                               ! read old stars, if any

  need_sync = .false.                                                           ! set for next time step
END subroutine

!*******************************************************************************
SUBROUTINE add_stars (rho_tot)
  USE params
  USE variables, only: rho=>r
  USE stars_m
  implicit none
  real, dimension(mx,my,mz):: rho_tot
  integer i,ix,iy,iz,kx,ky,kz,lx,ly,lz,cells
  real drho
!...............................................................................
  rho_tot = rho
  if (nstars==0) return
  if (verbose > 2) print*,mpi_rank,':d ',nstars,star(nstars)%x,star(nstars)%mass
  if (size(star).ne.nstars) print*,'add_star_mass ERROR:',mpi_rank,nstars,size(star)
  do i=1,nstars
    drho = star(i)%mass/(dx*dy*dz)
    iz = floor((star(i)%z-zmin_mpi)/dz + 1.0)
    iy = floor((star(i)%y-ymin_mpi)/dy + 1.0)
    ix = floor((star(i)%x-xmin_mpi)/dx + 1.0)
    cells = 0
    do lz=-m,+m
      kz = iz+lz
      if (mpi_nz<=1) kz = mod(kz+mz-1,mz)+1                                     ! wrap index if periodic
      if (kz>=1 .and. kz<=mz) then                                              ! inside?
        do ly=-m,+m
          ky = iy+ly
          if (mpi_ny<=1) ky = mod(ky+my-1,my)+1                                 ! wrap index if periodic
          if (ky>=1 .and. ky<=my) then                                          ! inside?
            do lx=-m,+m
              kx = ix+lx
              if (mpi_nx<=1) kx = mod(kx+mx-1,mx)+1                             ! wrap index if periodic
              if (kx>=1 .and. kx<=mx) then                                      ! inside?
                rho_tot(kx,ky,kz) = rho_tot(kx,ky,kz)+drho*prf(lx,ly,lz)
                cells = cells+1
                !if (i==1) print'(a,i5,2(3i5,g14.6))','ADD',mpi_rank,kx,ky,kz,drho,lx,ly,lz,prf(lx,ly,lz)
              end if
            end do
          end if
        end do
      end if
    end do
    if (verbose > 2) &
      print'(i3,a,i3,1pe9.1,3i4,i5)',mpi_rank,': add_mass',i,drho,ix,iy,iz,cells
  end do
END SUBROUTINE

!*******************************************************************************
SUBROUTINE move_stars (fx, fy, fz, phi)
  USE params
  USE timeintegration
  USE stars_m
  USE variables, only: rho=>r
  implicit none
  real, dimension(mx,my,mz):: fx, fy, fz, phi
  integer i,ix,iy,iz
!...............................................................................
  if (size(star).ne.nstars) print*,'move_stars ERROR:',mpi_rank,nstars,size(star)
  do i=1,nstars
   if (star(i)%owner == mpi_rank) then
    ix = (star(i)%x-xmin_mpi)/dx + 1.0
    iy = (star(i)%y-ymin_mpi)/dy + 1.0
    iz = (star(i)%z-zmin_mpi)/dz + 1.0
!
    if (ix < 1 .or. ix > mx) print*,'move_stars ix WARNING:',ix,star(i)%x,xmin_mpi
    if (iy < 1 .or. iy > my) print*,'move_stars iy WARNING:',iy,star(i)%y,ymin_mpi
    if (iz < 1 .or. iz > mz) print*,'move_stars iz WARNING:',iz,star(i)%z,zmin_mpi
    ix = min(max(ix,1),mx)
    iy = min(max(iy,1),my)
    iz = min(max(iz,1),mz)
!
    star(i)%dxdt  = star(i)%dxdt *alpha(isubstep) + star(i)%px/star(i)%mass     ! dx/dt
    star(i)%dydt  = star(i)%dydt *alpha(isubstep) + star(i)%py/star(i)%mass     ! dy/dt
    star(i)%dzdt  = star(i)%dzdt *alpha(isubstep) + star(i)%pz/star(i)%mass     ! dz/dt
    star(i)%dpxdt = star(i)%dpxdt*alpha(isubstep) - fx(ix,iy,iz)*star(i)%mass   ! dpx/dt
    star(i)%dpydt = star(i)%dpydt*alpha(isubstep) - fy(ix,iy,iz)*star(i)%mass   ! dpy/dt
    star(i)%dpzdt = star(i)%dpzdt*alpha(isubstep) - fz(ix,iy,iz)*star(i)%mass   ! dpz/dt
    star(i)%x  = star(i)%x  + dt*beta(isubstep)*star(i)%dxdt                    ! update positions
    star(i)%y  = star(i)%y  + dt*beta(isubstep)*star(i)%dydt                    ! update positions
    star(i)%z  = star(i)%z  + dt*beta(isubstep)*star(i)%dzdt                    ! update positions
    star(i)%px = star(i)%px + dt*beta(isubstep)*star(i)%dpxdt                   ! update momenta
    star(i)%py = star(i)%py + dt*beta(isubstep)*star(i)%dpydt                   ! update momenta
    star(i)%pz = star(i)%pz + dt*beta(isubstep)*star(i)%dpzdt                   ! update momenta
    star(i)%density = rho(ix,iy,iz)                                             ! increment
!
    if (star(i)%x <  xmin_box) star(i)%x =  star(i)%x + sx                      ! periodic wrapping
    if (star(i)%y <  ymin_box) star(i)%y =  star(i)%y + sy                      ! periodic wrapping
    if (star(i)%z <  zmin_box) star(i)%z =  star(i)%z + sz                      ! periodic wrapping
    if (star(i)%x >= xmax_box) star(i)%x =  star(i)%x - sx                      ! periodic wrapping
    if (star(i)%y >= ymax_box) star(i)%y =  star(i)%y - sy                      ! periodic wrapping
    if (star(i)%z >= zmax_box) star(i)%z =  star(i)%z - sz                      ! periodic wrapping
    if (star(i)%x < xmin_mpi .or. star(i)%x > xmax_mpi) need_sync = .true.      ! x in range?
    if (star(i)%y < ymin_mpi .or. star(i)%y > ymax_mpi) need_sync = .true.      ! y in range?
    if (star(i)%z < zmin_mpi .or. star(i)%z > zmax_mpi) need_sync = .true.      ! z in range?
    if (mpi_nx > 1 .and. (ix <=m .or. ix > mx-m)) need_sync = .true.            ! non-periodic and near boundary
    if (mpi_ny > 1 .and. (iy <=m .or. iy > my-m)) need_sync = .true.            ! non-periodic and near boundary
    if (mpi_nz > 1 .and. (iz <=m .or. iz > mz-m)) need_sync = .true.            ! non-periodic and near boundary

    !if (i==1) print'(a,4i5,8g14.6)','MK',mpi_rank,ix,iy,iz, &
    !star(i)%y*1000.,star(i)%py/star(i)%mass,star(i)%dpydt,fy(ix,iy,iz),phi(ix,iy,iz)
   end if
  end do
  call sync_stars                                                               ! in case new domain
END SUBROUTINE

!*******************************************************************************
SUBROUTINE init_explode (r)
  USE params, only: mx,my,mz,mid
  USE stars_m, only: do_sink, do_SNe
  implicit none
  real, dimension(mx,my,mz):: r
  character(len=mid):: id='$Id: sinkparticles.f90,v 1.30 2012/04/22 19:27:11 aake Exp $'
!...............................................................................
  call print_id(id)
  do_SNe = .false.
  do_sink = .false.
  call init_stars
END

!*******************************************************************************
SUBROUTINE explode (r,px,py,pz,e,d,bx,by,bz)
  USE params, only: mx,my,mz
  USE stars_m, only: do_sink, do_SNe
  implicit none
  real, dimension(mx,my,mz):: r,px,py,pz,e,d,bx,by,bz
!...............................................................................
  if (do_SNe) call supernova (r,e)
  if (do_sink) call starformation (r,e)
END

!*******************************************************************************
SUBROUTINE read_explode
  USE params
  USE stars_m
  USE supernova_m
  implicit none
  real sum
  integer ix, iy, iz
  namelist /sink/do_sink, twrite, rho_limit, rho_cut, phi_bh, max_distance, &
                 w_prof, verbose, sink_file
  namelist /SNe/ do_SNe, rho_unit, l_unit, t_unit, t_decay, profile_power, profile_cells
 !..............................................................................
  rewind(stdin); read (stdin,sink); if (master) write(*,sink)                   ! namelist parameters 
  rewind(stdin); read (stdin, SNe); if (master) write(*, SNe)                   ! namelist parameters 
  
  sum = 0.                                                                      ! for normalization
  do iz=-m,m; do iy=-m,m; do ix=-m,m                                            ! index loops
    prf(ix,iy,iz) = exp(-(ix**2+iy**2+iz**2)/w_prof**2)                         ! profile 
    sum = sum + prf(ix,iy,iz)                                                   ! sum up normalization
  end do; end do; end do
  prf = prf/sum                                                                 ! normalize to sum(prf)=1.
END
