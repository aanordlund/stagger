! $Id: selfgravity.f90,v 1.4 2010/05/13 20:52:40 aake Exp $
!**********************************************************************
MODULE selfgravity
  real grav, soft
  logical isolated
  real, allocatable, dimension(:,:,:):: phi, dphi
END MODULE

!***********************************************************************
SUBROUTINE init_selfg
  USE params
  USE selfgravity
  implicit none

  grav = 0.
  soft = 2.
  isolated = .false.
  call read_selfg
  if (grav > 0.) call test_fft
END SUBROUTINE

!***********************************************************************
SUBROUTINE read_selfg
  USE params
  USE selfgravity
  implicit none
  namelist /selfg/ grav,soft,isolated
  character(len=mid):: id="$Id: selfgravity.f90,v 1.4 2010/05/13 20:52:40 aake Exp $"

  call print_id(id)

  if (stdin.ge.0) then
    rewind (stdin); read (stdin,selfg)
  else
    read (*,selfg)
  end if
  if (master) write (*,selfg)
END SUBROUTINE

!***********************************************************************
SUBROUTINE selfgrav (r,xdnr,ydnr,zdnr,dpxdt,dpydt,dpzdt)
  USE params
  USE selfgravity
  implicit none
  real, dimension(mx,my,mz):: r,xdnr,ydnr,zdnr,dpxdt,dpydt,dpzdt
  logical omp_in_parallel, debug

  if (debug(dbg_force)) print *,'selfgrav:',grav,omp_in_parallel(),omp_mythread
  if (grav == 0.) return
  if (omp_in_parallel()) then
    !$omp master
    allocate (phi(mx,my,mz), dphi(mx,my,mz))
    !$omp end master
    !$omp barrier

    call selfgrav_omp (r,xdnr,ydnr,zdnr,dpxdt,dpydt,dpzdt,phi,dphi)

    !$omp barrier
    !$omp master
    deallocate (phi, dphi)
    !$omp end master
  else
    allocate (phi(mx,my,mz), dphi(mx,my,mz))

    !$omp parallel
    call selfgrav_omp (r,xdnr,ydnr,zdnr,dpxdt,dpydt,dpzdt,phi,dphi)
    !$omp end parallel

    deallocate (phi, dphi)
  end if

END SUBROUTINE

!***********************************************************************
SUBROUTINE selfgrav_omp (r,xdnr,ydnr,zdnr,dpxdt,dpydt,dpzdt,phi,dphi)
  USE params
  USE forcing
  USE arrays, only: scr4, scr5, scr6
  implicit none
  real, dimension(mx,my,mz):: r,xdnr,ydnr,zdnr,dpxdt,dpydt,dpzdt,phi,dphi
  integer iz
  real egrav
  character(len=mid):: id="$Id: selfgravity.f90,v 1.4 2010/05/13 20:52:40 aake Exp $"
!-----------------------------------------------------------------------
  call print_trace (id, dbg_force, 'BEGIN')

  if (conservative) then
    do iz=izs,ize
      scr4(:,:,iz) = r(:,:,iz)
    end do
    !$omp barrier                                                       ! need consistent scratch
    call add_stars (scr4)
    !$omp barrier                                                       ! need consistent scratch
    call gravpot (scr4,phi)                                             ! uses scr5, scr6, and scratch
      call dumpl(scr4,'rho','grav.dmp',0)
      call dumpl(phi,'phi','grav.dmp',1)
    do iz=izs,ize
      scr4(:,:,iz) = scr4(:,:,iz)*phi(:,:,iz)
    end do
    call average_subr (scr4, e_grav)
    call ddxdn_set (phi, dphi)
    call xup_set (dphi,scr4)
      call dumpl(scr4,'phi_x','grav.dmp',2)
    !$omp barrier                                                       ! need consistent scratch
    do iz=izs,ize
      dpxdt(:,:,iz) = dpxdt(:,:,iz)-xdnr(:,:,iz)*dphi(:,:,iz)
    end do
    call ddydn_set (phi, dphi)
    call yup_set (dphi,scr5)
      call dumpl(scr5,'phi_y','grav.dmp',3)
    do iz=izs,ize
      dpydt(:,:,iz) = dpydt(:,:,iz)-ydnr(:,:,iz)*dphi(:,:,iz)
    end do
    call ddzdn_set (phi, dphi)
    call zup_set (dphi,scr6)
      call dumpl(scr6,'phi_z','grav.dmp',4)
    call move_stars (scr4, scr5, scr6, phi)
    do iz=izs,ize
      dpzdt(:,:,iz) = dpzdt(:,:,iz)-zdnr(:,:,iz)*dphi(:,:,iz)
    end do
  else 
    do iz=izs,ize
      scr4(:,:,iz) = r(:,:,iz)
    end do
    call add_stars (scr4)
    call gravpot (scr4,phi)
      call dumpl(phi,'phi','sink.dmp',1)
    call ddxdn_set (phi, dphi)
    call xup_set (dphi,scr4)
      call dumpl(scr4,'phi_x','sink.dmp',2)
    do iz=izs,ize
      dpxdt(:,:,iz) = dpxdt(:,:,iz)-dphi(:,:,iz)
    end do
    call ddydn_set (phi, dphi)
    call yup_set (dphi,scr5)
      call dumpl(scr4,'phi_y','sink.dmp',4)
    do iz=izs,ize
      dpydt(:,:,iz) = dpydt(:,:,iz)-dphi(:,:,iz)
    end do
    call ddzdn_set (phi, dphi)
    call zup_set (dphi,scr6)
      call dumpl(scr4,'phi_z','sink.dmp',5)
    call move_stars (scr4, scr5, scr6, phi)
    do iz=izs,ize
      dpzdt(:,:,iz) = dpzdt(:,:,iz)-dphi(:,:,iz)
    end do
  end if

  call print_trace (id, dbg_force, 'END')
END SUBROUTINE

