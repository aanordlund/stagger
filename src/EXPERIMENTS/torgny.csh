#!/bin/csh

set run = /u60/torgny/DATA/run86
set pgm = torgny-table.x

cat > $run.in << EOF
&io      iread=-1 nstep=10000 nsnap=10 nscr=10 file="$run.dat" /
&run     Cdt=0.3 iseed=77 idbg=0 /
&vars    do_pscalar=t do_mhd=f /
&grid    mx=63, my=63, mz=63, sx=255, sy=255, sz=255 /
&pde     do_loginterp=t do_2nddiv=t nu1=0.1 nu2=0.7 nur=0.15 poff=1.3228E-13 /
&eqofst  do_ionization=f /
&force   do_force=f /
&init    do_init=t rhoav=1. tempav=3.e5 rfl=0.5 k2=5 /
&bdry    /
&expl    do_expl=t do_delayexpl=t do_lmsf=t do_psft=f Tlimit=2000. snseed=333 sntime=0.2 ctime=10 /
&cool    do_cool=t do_heat=t do_table=t Tlower=500. tablefile='../src/COOLING/lambda.dat' /
EOF

./$pgm < $run.in >> $run.log

