! $Id: boundary_charbc.f90,v 1.13 2009/06/26 03:11:00 bob Exp $
!***********************************************************************
MODULE boundary
  real t_Bbdry, Bscale_top, Bscale_bot, lb_eampl, t_bdry, eetop_frac, Uy_bdry, fmtop_frac
  real pdamp, rtop_frac,ee_top
  real rbot, ebot, htop
  real ampxy, ampyz, period, omegap, akx, aky, akz, kxp, kyp, kzp
  real Bx0, By0, Bz0, rmin, rmax, pb_fact, uy_max
  real(kind=8) rub,pyub,elb,eub,pub,pubt,dpydtub
  real, allocatable, dimension(:,:,:):: Bxl, Byl, Bzl, Bxu, Byu, Bzu
  real, allocatable, dimension(:,:) :: drdt_bot, dpxdt_bot, dpydt_bot, &
                                       dpzdt_bot, dedt_bot, &
                                       dBxdt_bot, dBydt_bot, dBzdt_bot
  real, allocatable, dimension(:,:) :: drdt_top, dpxdt_top, dpydt_top, &
                                       dpzdt_top, dedt_top, &
                                       dBxdt_top, dBydt_top, dBzdt_top


  real, allocatable, dimension(:,:,:):: Uxc, Uyc, Uzc, Bxc, Byc, Bzc, rdn, Csdn, &
                                        vadn, vfdn, vsdn, alpha_f, alpha_s, beta_x, beta_z, &
                                        drdy, dpdy, dUxdy, dUydy, dUzdy, dBxdy, dBydy, dBzdy, &
                                        d1, d2, d3, d4, d5, d6, d7, d8

  real, allocatable, dimension(:,:):: dpdx, dpdz, divh_ph, divh_uh, &
                                      divh_pxuh, divh_pyuh, divh_pzuh, divh_euh, &
                                      delta_p, rtop, lnrtop, etop, &
                                      divh_BxBh, divh_ByBh, divh_BzBh, &
                                      divh_Bxuh, divh_Byuh, divh_Bzuh

  real, allocatable, dimension(:,:,:):: nu_top, nud_top, nu_bot, nud_bot

  real, allocatable, dimension(:,:):: scra, scrb, scrc, scrd, scre, scrf

!  real(kind=8), allocatable, dimension(:):: pyav,Csav

  logical debug, bdry_first, Binflow, nonreflecting
  character(len=16) test

END module

!-----------------------------------------------------------------------
SUBROUTINE init_boundary
  USE params
  USE arrays
  USE boundary
  implicit none
  character(len=mid):: id='$Id: boundary_charbc.f90,v 1.13 2009/06/26 03:11:00 bob Exp $'

  call print_id(id)

  if (mpi_y == 0) then
    lb = 6
    allocate (Bxl(mx,lb,mz),Byl(mx,lb,mz),Bzl(mx,lb,mz))
    allocate (drdt_top(mx,mz),dpxdt_top(mx,mz),dpydt_top(mx,mz),dpzdt_top(mx,mz),dedt_top(mx,mz))
    allocate (dBxdt_top(mx,mz),dBydt_top(mx,mz),dBzdt_top(mx,mz))
    allocate(nu_top(mx,2,mz),nud_top(mx,2,mz))
  else
    lb = 0
  end if
  if (mpi_y == mpi_ny-1) then
    ub = my-5
    allocate (Bxu(mx,my-ub,mz),Byu(mx,my-ub,mz),Bzu(mx,my-ub,mz))
    allocate (drdt_bot(mx,mz),dpxdt_bot(mx,mz),dpydt_bot(mx,mz),dpzdt_bot(mx,mz),dedt_bot(mx,mz))
    allocate (dBxdt_bot(mx,mz),dBydt_bot(mx,mz),dBzdt_bot(mx,mz))
    allocate(nu_bot(mx,2,mz),nud_bot(mx,2,mz))
  else
    ub = my
  end if

  allocate(Uxc(mx,my,mz),Uyc(mx,my,mz),Uzc(mx,my,mz), Bxc(mx,my,mz), Byc(mx,my,mz), Bzc(mx,my,mz))
  allocate(alpha_f(mx,my,mz),alpha_s(mx,my,mz),beta_x(mx,my,mz),beta_z(mx,my,mz))
  allocate(rdn(mx,my,mz),Csdn(mx,my,mz),vadn(mx,my,mz),vfdn(mx,my,mz),vsdn(mx,my,mz),drdy(mx,my,mz),dpdy(mx,my,mz))
  allocate(dUxdy(mx,my,mz),dUydy(mx,my,mz),dUzdy(mx,my,mz),dBxdy(mx,my,mz),dBydy(mx,my,mz),dBzdy(mx,my,mz))
  allocate(d1(mx,my,mz),d2(mx,my,mz),d3(mx,my,mz),d4(mx,my,mz),d5(mx,my,mz),d6(mx,my,mz),d7(mx,my,mz),d8(mx,my,mz))

  allocate(dpdx(mx,mz),dpdz(mx,mz),divh_ph(mx,mz),divh_uh(mx,mz),divh_pxuh(mx,mz),divh_pyuh(mx,mz),divh_pzuh(mx,mz))
  allocate(divh_euh(mx,mz),delta_p(mx,mz),rtop(mx,mz),lnrtop(mx,mz),etop(mx,mz))
  allocate(divh_BxBh(mx,mz),divh_ByBh(mx,mz),divh_BzBh(mx,mz),divh_Bxuh(mx,mz),divh_Byuh(mx,mz),divh_Bzuh(mx,mz))
  allocate(scra(mx,mz),scrb(mx,mz),scrc(mx,mz),scrd(mx,mz),scre(mx,mz),scrf(mx,mz))
!  allocate(pyav(my),Csav(my))

  t_bdry = 0.01                                                         ! bdry decay time
  t_Bbdry  = 0.01                                                       ! bdry decay time for B
  eetop_frac=0.05                                                       ! fraction of current eetop
  rtop_frac=0.05                                                        ! fraction of current rtop
  fmtop_frac=1.0                                                        ! fraction fmass correction for rho(lb)
  lb_eampl = 0.
  pdamp = 0.
  rbot = -1.
  htop = 0.
  rmin = 0.0
  rmax = 1e1
  pb_fact = 0.36
  uy_max = 2.                                                           ! 20 km/s with solar scaling
  ampxy = 0.
  ampyz = 0.
  period = 0.
  akx = 1.
  aky = 1.
  akz = 1.
  Bscale_top = 10
  Bscale_bot = 10
  Bx0 = 0.
  By0 = 0.
  Bz0 = 0.
  Binflow = .false.
  debug = .false.
  bdry_first = .true.
  do_stratified = .true.
  nonreflecting = .false.
  test=''
  call read_boundary

  kxp=akx*2.*pi/mx
  kyp=aky*2.*pi/(ub-lb+1)
  kzp=akz*2.*pi/mz
  if (period .gt. 0.) omegap=2.*pi/period

  call init_potential_lower
END

!-----------------------------------------------------------------------
SUBROUTINE read_boundary
  USE params
  USE boundary
  implicit none
  namelist /bdry/ lb, ub, t_bdry, t_Bbdry, Bscale_top, Bscale_bot, eetop_frac, rtop_frac, fmtop_frac, &
                  htop, rbot, ebot, Bx0, By0, Bz0, Binflow, Uy_bdry, &
		  rmin, rmax, pb_fact, uy_max, lb_eampl, pdamp, debug, &
                  ampxy, ampyz, period, akx, aky, akz, nonreflecting, test

  rewind (stdin); read (stdin,bdry)
  if (master) write (*,bdry)
END

!-----------------------------------------------------------------------
SUBROUTINE viscosity_boundary (nu, cs)
  USE params
  implicit none
  real, dimension(mx,my,mz):: nu, cs
  integer iz

! if (mpi_y==0) then                                                    ! top bdry
!   do iz=izs,ize
!     nu(:,1:lb,iz) = 0.3*cs(:,1:lb,iz)                                 ! hardwire high viscosity 
!   end do
! end if
END

!-----------------------------------------------------------------------
SUBROUTINE density_boundary (r,lnr,py,e)
  USE params
  USE arrays, only: scr6, eeav
  USE boundary
  USE eos
  implicit none
  real, dimension(mx,my,mz):: r,lnr,py,e
  integer ix,iy,iz
  real htopi, dlnrdy, lnrmin, lnrmax

if (do_trace .and. omp_master) print *, 'density_BC: mpi_rank,y,ny = ', mpi_rank, mpi_y, mpi_ny
  call dumpn(r,'density1:r','bdry.dmp',1)
  call dumpn(e,'density1:e','bdry.dmp',1)

  call haverage_subr (r,rav)                                            ! horizontally averaged rho
  call haverage_subr (e,eav)                                            ! horizontally averaged e
  do iz=izs,ize
    scr6(:,:,iz) = e(:,:,iz)/r(:,:,iz)                                  ! ee = e/r
  end do
!$omp barrier
  if (pav(1).eq.0.) then                                                ! if first time
    call haverage_subr (scr6, eeav)					! ee horizontal average
!$omp single
    ee_top=eeav(lb+1)
!$omp end single
    do iz = izs, ize
      rtop(:,iz)=rav(lb)
      etop(:,iz)=eav(lb)
    end do
    if (debug .and. omp_master) print *,'eetop =',ee_top
  end if
  do iz=izs,ize
    scr6(:,lb,iz) = ee_top
  end do

  if (lb > 1) then
    call constant_center_lower(scr6)					! vanishing deriv at top
    call extrapolate_center_lower(lnr)                                  ! basic default action
   
    do iz=izs,ize
      do ix=1,mx
        if(rmin > 0.0) then
          lnrmin = max(alog(rmin),lnr(ix,1,iz))
        else
          lnrmin = lnr(ix,1,iz)
        end if
        lnrmax = lnr(ix,lb,iz)
        lnrmin = min(lnrmin, lnrmax + 3.0)
        dlnrdy = (lnrmax - lnrmin) / (ym(lb) - ym(1))
        do iy=1,lb
          if(.not. nonreflecting) &
            lnr(ix,iy,iz) = lnrmin + dlnrdy * (ym(iy) - ym(1))
          r(ix,iy,iz) = exp(lnr(ix,iy,iz))                               ! consistent density
          if(.not. nonreflecting) &
            e(ix,iy,iz)=r(ix,iy,iz)*scr6(ix,iy,iz)
        end do
      end do
    end do
  end if

  call extrapolate_center_upper(lnr)                                    ! extrapolate in the log

  if (ub < my) then
    do iz=izs,ize
    do iy=ub,my
    do ix=1,mx
      r(ix,iy,iz)=exp(lnr(ix,iy,iz))                                    ! consistent density
    end do 
    end do 
    end do
  end if
  call dumpn(r,'density2:r','bdry.dmp',1)
  call dumpn(e,'density2:e','bdry.dmp',1)
  call barrier_omp ('density')

END

!-----------------------------------------------------------------------
SUBROUTINE energy_boundary (r,lnr,Ux,Uy,Uz,e,ee,p,Bx,By,Bz)
  USE params
  USE arrays, ONLY: eeav
  USE eos
  USE boundary
  implicit none
  real, dimension(mx,my,mz):: r,lnr,Ux,Uy,Uz,e,ee,p,Bx,By,Bz
  real s, pb, efact
  real(kind=8) tmp(3)
  integer ix, iy, iz

if (do_trace .and. omp_master) print *, 'energy_BC: mpi_rank,y,ny = ', mpi_rank, mpi_y, mpi_ny
  call dumpn(r,'energy1:r','bdry.dmp',1)
  call dumpn(e,'energy1:e','bdry.dmp',1)
  call dumpn(ee,'energy1:ee','bdry.dmp',1)

  if (lb > 1) then                                                      ! top BC
!$omp single
    ee_top = (1.-eetop_frac)*ee_top + eetop_frac*eeav(lb+1)
!$omp end single
    if (debug .and. omp_master) print *,'eetop =',ee_top

  if(.not. nonreflecting) then 

    do iz=izs,ize
      ee(:,lb,iz) = ee_top
    end do

  end if

    call constant_center_lower(ee)					! vanishing deriv at top
    call haverage_subr (ee, eeav)

    do iz=izs,ize
      do iy=1,lb
        e(:,iy,iz)=r(:,iy,iz)*ee(:,iy,iz)			        ! consistent energy
      end do
    end do
  end if

  if (mpi_y == mpi_ny-1 .and. rbot < 0.) then                           ! bottom BC
    call barrier_omp ('rbot')
!$omp single
    rbot=sum(r(:,ub,:))/(mx*mz)                                         ! save density
    ebot=sum(e(:,ub,:))/(mx*mz)                                         ! save density
    tmp(1) = rbot
    tmp(2) = ebot
    call haverage_mpi (tmp, 2)
    rbot = tmp(1)
    ebot = tmp(2)
!$omp end single
    if (master) print *,'ub, rbot, ebot =', ub, rbot, ebot
  end if

  if (mpi_y == mpi_ny-1 .and. lb_eampl .ne. 0) then
    do iz=izs,ize
      do ix=1,mx
!        efact = 1.+lb_eampl*(cos((ix-97)*2.*pi/mx)+cos((iz-91)*2.*pi/mz))
!        e(ix,ub,iz) = e(ix,ub,iz)*efact
!        r(ix,ub,iz) = r(ix,ub,iz)*efact**(1./gamma)
!        ee(ix,ub,iz) = e(ix,ub,iz)/r(ix,ub,iz)                          ! redo energy per unit mass
      end do
    end do
!    call extrapolate_center_log_upper(r)
    do iz=izs,ize
      do iy=ub+1,my
!        lnr(:,iy,iz)=alog(r(:,iy,iz))
      end do
    end do
  end if

  if(nonreflecting) then
  call constant_center_upper(ee)
  else
  call extrapolate_center_upper(ee)                                     ! extrapolate at bottom
  end if

  if (ub < my) then
    do iz=izs,ize
    do iy=ub,my
      e(:,iy,iz)=r(:,iy,iz)*ee(:,iy,iz)
    end do
    end do
  end if
  call dumpn(r,'energy2:r','bdry.dmp',1)
  call dumpn(e,'energy2:e','bdry.dmp',1)
  call dumpn(ee,'energy2:ee','bdry.dmp',1)

END

!-----------------------------------------------------------------------
SUBROUTINE passive_boundary (r,Ux,Uy,Uz,d,dd)
  USE params
  implicit none
  real, dimension(mx,my,mz):: r,Ux,Uy,Uz,d,dd
  integer iy, iz

  call symmetric_center(dd)
  if (mpi_y == 0) then                                                  ! top BC
    do iz=izs,ize
    do iy=1,lb-1
      d(:,iy,iz)=r(:,iy,iz)*dd(:,iy,iz)
    end do
    end do
  end if

  if (mpi_y == mpi_ny-1) then                                           ! bottom BC
    do iz=izs,ize
    do iy=ub+1,my
      d(:,iy,iz)=r(:,iy,iz)*dd(:,iy,iz)
    end do
    end do
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE velocity_boundary (r,xdnr,ydnr,zdnr,Ux,Uy,Uz,px,py,pz,e,Bx,By,Bz)
  USE params
  USE boundary
  implicit none
  real, dimension(mx,my,mz):: r,xdnr,ydnr,zdnr,Ux,Uy,Uz,px,py,pz,e,Bx,By,Bz
  real p, rin, rtot, fmout, fmtot, uyin, uytot
  integer ix, iy, iz

    if (do_trace) print 1, 'velocity_BC 1: mpi_rank,y,ny = ', mpi_rank, mpi_y, mpi_ny, omp_mythread
1   format(1x,a,4i6)
    call dumpn(px,'velocity1;px','bdry.dmp',1)
    call dumpn(py,'velocity1;py','bdry.dmp',1)
    call dumpn(pz,'velocity1;pz','bdry.dmp',1)
!
!  Top boundary, vanishing velocity derivatives
!
   call extrapolate_center_lower(Ux)
   call extrapolate_face_lower  (Uy)
   call extrapolate_center_lower(Uz)

!   call symmetric_center_lower(Ux)
!   call symmetric_face_lower  (Uy)
!   call symmetric_center_lower(Uz)


    if (do_trace) print 1,'velocity: 2', mpi_rank, mpi_y, mpi_ny, omp_mythread
!
!  Top boundary, impose maximum velocity -- relevant when the 
!  density is clambed and cannot become lower
!
  if (lb > 1) then
!   do iz=izs,ize
!   do ix=1,mx
!     if (Uy(ix,lb+1,iz) > 0. .or. Uy(ix,lb,iz) > 0.) then
!     do iy=1,lb+2
!       Ux(ix,iy,iz) = max(min(Ux(ix,iy,iz),+uy_max),-uy_max)
!       Uy(ix,iy,iz) = max(min(Uy(ix,iy,iz),+uy_max),-uy_max)
!       Uz(ix,iy,iz) = max(min(Uz(ix,iy,iz),+uy_max),-uy_max)
!     end do
!     end if
!   end do
!   end do
!
!  Recompute mass fluxes
!
    do iz=izs,ize
      do iy=1,lb
        px(:,iy,iz)=xdnr(:,iy,iz)*Ux(:,iy,iz)
        py(:,iy,iz)=ydnr(:,iy,iz)*Uy(:,iy,iz)
        pz(:,iy,iz)=zdnr(:,iy,iz)*Uz(:,iy,iz)
      end do
      py(:,lb+1,iz)=ydnr(:,lb+1,iz)*Uy(:,lb+1,iz)
    end do
  end if
    if(do_trace) print 1,'velocity: 3', mpi_rank, mpi_y, mpi_ny, omp_mythread

!  call symmetric_center_upper (Ux)
!  call symmetric_face_upper   (Uy)
!  call symmetric_center_upper (Uz)

   call extrapolate_center_upper(Ux)  
   call extrapolate_face_upper  (Uy)
   call extrapolate_center_upper(Uz)

!
!  Recompute mass fluxes
!
  if (mpi_y == mpi_ny-1) then
    do iz=izs,ize
    do iy=ub,my
      px(:,iy,iz)=xdnr(:,iy,iz)*Ux(:,iy,iz)
      py(:,iy,iz)=ydnr(:,iy,iz)*Uy(:,iy,iz)
      pz(:,iy,iz)=zdnr(:,iy,iz)*Uz(:,iy,iz)
    end do
    end do

  end if
    if(do_trace) print 1,'velocity: 4', mpi_rank, mpi_y, mpi_ny, omp_mythread
    call dumpn(px,'velocity2:px','bdry.dmp',1)
    call dumpn(py,'velocity2:py','bdry.dmp',1)
    call dumpn(pz,'velocity2:pz','bdry.dmp',1)

END

!-----------------------------------------------------------------------
SUBROUTINE magnetic_pressure_boundary (BB)
  USE params
  USE boundary
  implicit none
  real, dimension(mx,my,mz):: BB
  
  if (ub < my) then
    pbub(:,izs:ize) = 0.5*BB(:,ub,izs:ize)
    if (debug) print *,'pbub:',mpi_rank,minval(pbub(:,izs:ize)),maxval(pbub(:,izs:ize))
  end if

END 

!-----------------------------------------------------------------------
SUBROUTINE mfield_boundary (Ux,Uy,Uz,Bx,By,Bz)
  USE params
  USE boundary
  USE arrays, ONLY: wk06, wk19
  implicit none
  real, dimension(mx,my,mz):: Ux,Uy,Uz,Bx,By,Bz
  integer ix, iy, iz
  real f
  character(len=mid), save:: id='mfield_boundary $Id: boundary_charbc.f90,v 1.13 2009/06/26 03:11:00 bob Exp $'

  call print_id (id)

  do iz=izs,ize
    nu_top(:,1,iz) = wk06(:,lb,iz) + 1.0e-20
    nu_top(:,2,iz) = wk06(:,lb+1,iz) + 1.0e-20
    nud_top(:,1,iz) = wk19(:,lb,iz) + 1.0e-20
    nud_top(:,2,iz) = wk19(:,lb+1,iz) + 1.0e-20
    nu_bot(:,1,iz) = wk06(:,ub-1,iz) + 1.0e-20
    nu_bot(:,2,iz) = wk06(:,ub,iz) + 1.0e-20
    nud_bot(:,1,iz) = wk19(:,ub-1,iz) + 1.0e-20
    nud_bot(:,2,iz) = wk19(:,ub,iz) + 1.0e-20
  end do


  if (mpi_y == 0) then                                                  ! top BC
    do iz=izs,ize
      do iy=1,lb
        Bxl(:,iy,iz)=Bx(:,iy,iz)                                        ! store orig bdry B
        Byl(:,iy,iz)=By(:,iy,iz)
        Bzl(:,iy,iz)=Bz(:,iy,iz)
      end do
    end do

    if (omp_master.and.debug) then
      print *,'Bx (before)=',Bx(1,0:9,1)
      print *,'By (before)=',By(1,0:9,1)
      print *,'Bz (before)=',Bz(1,0:9,1)
    end if
    call dumpn(bx,'mfield1:bx','bdry.dmp',1)
    call dumpn(by,'mfield1:by','bdry.dmp',1)
    call dumpn(bz,'mfield1:bz','bdry.dmp',1)

!    call potential_lower(Bx,By,Bz,lb)                                   ! ->potential
    call extrapolate_center_lower(Bx)
!    call constant_face_lower  (By)
    call extrapolate_face_lower  (By)
    call extrapolate_center_lower(Bz)

    call dumpn(bx,'mfield2:bx','bdry.dmp',1)
    call dumpn(bz,'mfield2:bz','bdry.dmp',1)
    if (master.and.debug) then
      print *,'Bx (after)=',Bx(1,0:9,1)
      print *,'By (after)=',By(1,0:9,1)
      print *,'Bz (after)=',Bz(1,0:9,1)
    end if
  end if

!-----------------------------------------------------------------------
!
!  Lower boundary: Store & restore original ghost zone field, to avoid
!  destroying div(B).  But set up for computing the electric fiels as
!  if the field in inflows is constant.  This gets multiplied by the
!  inflow velocity field, which vanishes btw inflow and outflow, thus
!  avoiding creating a discontinuity in the electric field.
!
!-----------------------------------------------------------------------

  if (mpi_y == mpi_ny-1) then                                           ! bottom BC
    do iz=izs,ize
      do iy=1,my-ub
        Bxu(:,iy,iz)=Bx(:,ub+iy,iz)                                     ! store orig bdry B
        Byu(:,iy,iz)=By(:,ub+iy,iz)
        Bzu(:,iy,iz)=Bz(:,ub+iy,iz)
      end do
    end do

!   call mixed_center_upper(Bx,Bscale_bot)
!   call mixed_center_upper(Bz,Bscale_bot)
    call extrapolate_center_upper(Bx)
!    call constant_center_upper  (By)
    call extrapolate_face_upper  (By)
    call extrapolate_center_upper(Bz)

     if (Binflow) then
       do iz=izs,ize
       do ix=1,mx
         f = 1./(1.+exp(min(Uy(ix,ub,iz)/Uy_bdry,80.))) 		! -> 1 in inflows
         do iy=ub+1,my
           Bx(ix,iy,iz) = (1.-f)*Bx(ix,iy,iz) + f*Bx0
           Bz(ix,iy,iz) = (1.-f)*Bz(ix,iy,iz) + f*Bz0
         enddo
       enddo
       enddo
     end if
  end if
    call dumpn(bx,'mfield1:bx','bdry.dmp',1)
    call dumpn(by,'mfield1:by','bdry.dmp',1)
    call dumpn(bz,'mfield1:bz','bdry.dmp',1)

END

!-----------------------------------------------------------------------
SUBROUTINE ecurrent_boundary (Ex, Ey, Ez, Jx, Jy, Jz)
  USE params
  USE boundary
  implicit none
  real, dimension(mx,my,mz):: Ex, Ey, Ez, Jx, Jy, Jz

END

!-----------------------------------------------------------------------
SUBROUTINE efield_boundary (Ex, Ey, Ez, Bx_y, Bx_z, By_z, By_x, Bz_x, Bz_y, &
                            Bx, By, Bz, Ux_y, Ux_z, Uy_z, Uy_x, Uz_x, Uz_y, &
                            Ux, Uy, Uz)
  USE params
  USE boundary
  USE arrays, only: scr1, scr2
  implicit none
  real, dimension(mx,my,mz):: Ex, Ey, Ez, Bx_y, Bx_z, By_z, By_x, Bz_x, Bz_y, &
                              Ux_y, Ux_z, Uy_z, Uy_x, Uz_x, Uz_y, &
                              Ux, Uy, Uz, Bx, By, Bz
  integer ix, iy, iz
  real f, ct

  character(len=mid), save:: id='efield_boundary $Id: boundary_charbc.f90,v 1.13 2009/06/26 03:11:00 bob Exp $'

  call print_id (id)

!-----------------------------------------------------------------------
!  Top boundary: Do nothing, since the two contributions -- resistive 
!  and advective are only contaminated beyond three points from the bdry.
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!  Bottom boundary:  Ex and Ez are already good, having been computed
!  from regularized velocity and magnetic field values.  However, the
!  values more than 3 grids outside the boundary are contaminated, due
!  to the ydn(Bxz) and ydn(Uxz) interpolations, so these need to be
!  regularized.
!-----------------------------------------------------------------------
  if (mpi_y == mpi_ny-1) then                                           ! bottom BC
    if (Binflow) then
      do iz=izs,ize
       do ix=1,mx
        do iy=ub+4,my
          Ex(ix,iy,iz) = Ex(ix,ub+3,iz)                                 ! regularize
          Ez(ix,iy,iz) = Ez(ix,ub+3,iz)                                 ! regularize
        enddo
       enddo
      enddo
    else
      do iz=izs,ize
        scr1(:,ub,iz) = -(Bz_y(:,ub,iz)-Bz0)*ct
        scr2(:,ub,iz) = +(Bx_y(:,ub,iz)-Bx0)*ct
      enddo
      call derivative_face_upper (Ex, scr1) 
      call derivative_face_upper (Ez, scr2) 
    end if

    do iz=izs,ize
      Bx(:,ub+1:ub+2,iz)=Bxu(:,1:2,iz)                                    ! restore lower bdry B
      By(:,ub+1:ub+3,iz)=Byu(:,1:3,iz)                                    ! restore lower bdry B
      Bz(:,ub+1:ub+2,iz)=Bzu(:,1:2,iz)                                    ! restore lower bdry B
    end do
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE regularize (f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
!
  if (do_trace) print *,'regularize 1:',mpi_rank,omp_mythread,mpi_y
  if (mpi_y ==        0) call symmetric_center_lower (f)
  if (mpi_y == mpi_ny-1) call symmetric_center_upper (f)
  if (do_trace) print *,'regularize 2:',mpi_rank,omp_mythread,mpi_y
END

!-----------------------------------------------------------------------
SUBROUTINE regularize_face (f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
!
  if (mpi_y ==        0) call symmetric_face_lower (f)
  if (mpi_y == mpi_ny-1) call symmetric_face_upper (f)
END

!-----------------------------------------------------------------------
SUBROUTINE ddt_boundary (r,px,py,pz,e,p,Bx,By,Bz, &
                         drdt,dpxdt,dpydt,dpzdt, &
                         dedt,dBxdt,dBydt,dBzdt)
  USE params
  USE arrays, ONLY: wk05, wk07, wk08
  USE variables, ONLY: Ux, Uy, Uz
  implicit none
  real, dimension(mx,my,mz):: r,px,py,pz,e,p,Bx,By,Bz, &
                              drdt,dpxdt,dpydt,dpzdt, &
                              dedt,dBxdt,dBydt,dBzdt

  call ddt_boundary1(r,px,py,pz,e,p,Bx,By,Bz, &
                     drdt,dpxdt,dpydt,dpzdt, &
                     dedt,dBxdt,dBydt,dBzdt, &
                     Ux,Uy,Uz,wk08,wk05,wk07)

END





!-----------------------------------------------------------------------
SUBROUTINE ddt_boundary1(r,px,py,pz,e,p,Bx,By,Bz, &
                         drdt,dpxdt,dpydt,dpzdt, &
                         dedt,dBxdt,dBydt,dBzdt, &
                         Ux,Uy,Uz,lnr,ee,cs)
  USE params
  USE arrays, ONLY: scr1,scr2,scr3,scr4,scr5,scr6,eeav
  USE forcing
  USE eos
  USE boundary
  USE timeintegration, ONLY: alpha
  implicit none
  real, dimension(mx,my,mz):: r,px,py,pz,e,Bx,By,Bz, &
                              drdt,dpxdt,dpydt,dpzdt, &
                              dedt,dBxdt,dBydt,dBzdt, &
                              Ux,Uy,Uz,lnr,p,ee,Cs
                              
  real ::                     gam1, csq, caq, vaq, vfq, vsq, dd, f, vfast, vslow, Bt
  integer ix,iy,iz
  logical omp_in_parallel

  character(len=mid):: id='$Id: boundary_charbc.f90,v 1.13 2009/06/26 03:11:00 bob Exp $'
  call print_id (id)
  if (do_trace .and. omp_master) print *,'ddt_boundary: begin'

!-------------------------------------------------------------------------
! Characteristic Boundary Conditions
!   Cell (zone) centered variables are defined at j
!   Face centered variables are defined at j-1/2
!   1...2...3...4...5...6...  ... ....n-5...n-4...n-3...n-2...n-1..n  :cell
! 1...2...3...4...5...6...7.. ... .n-5...n-4...n-3...n-2...n-1...n    :face
!   Boundary is at middle of cell lb and ub
!   Bottom is at mx, Top is at 1: y=height (increases downward)
!-------------------------------------------------------------------------
    call barrier_omp ('ddt1')
    call dumpn(r,'ddt1:r','bdry.dmp',1)
    call dumpn(px,'ddt1:px','bdry.dmp',1)
    call dumpn(py,'ddt1:py','bdry.dmp',1)
    call dumpn(pz,'ddt1:pz','bdry.dmp',1)
    call dumpn(e,'ddt1:e','bdry.dmp',1)
    if (do_mhd) then
      call dumpn(bx,'ddt1:bx','bdry.dmp',1)
      call dumpn(by,'ddt1:by','bdry.dmp',1)
      call dumpn(bz,'ddt1:bz','bdry.dmp',1)
    endif
    call dumpn(drdt,'ddt1:drdt','bdry.dmp',1)
    call dumpn(dpxdt,'ddt1:dpxdt','bdry.dmp',1)
    call dumpn(dpydt,'ddt1:dpydt','bdry.dmp',1)
    call dumpn(dpzdt,'ddt1:dpzdt','bdry.dmp',1)
    call dumpn(dedt,'ddt1:dedt','bdry.dmp',1)
    if (do_mhd) then
      call dumpn(dbxdt,'ddt1:dbxdt','bdry.dmp',1)
      call dumpn(dbydt,'ddt1:dbydt','bdry.dmp',1)
      call dumpn(dbzdt,'ddt1:dbzdt','bdry.dmp',1)
    endif
!-----------------------------------------------------------------------

  if(gamma .eq. 1.0) then
    gam1 = 1.0
  else
    gam1 = gamma - 1.0
  end if

  do iz = izs, ize
    lnr(:,:,iz)=alog(r(:,:,iz))					! overwritten in pde
    ee(:,:,iz)=e(:,:,iz)/r(:,:,iz)				! overwritten in mhd
!    wk06(:,:,iz)=wk06(:,:,iz) + 1.0e-20                         ! add a small viscosity to both viscosity arrays
!    wk19(:,:,iz)=wk19(:,:,iz) + 1.0e-20                         ! to avoid problems with logarithmic interpolation
  end do

  if(.not. do_ionization) then
    do iz = izs, ize
      p(:,:,iz) = gam1 * ee(:,:,iz) * r(:,:,iz)
      dlnpdlnr_E(:,:,iz) = 1.0
      dlnpdE_r(:,:,iz) = 1.0 / ee(:,:,iz)
    end do
    call haverage_subr(p, pav)
  end if

!-----------------------------------------------------------------------
! Cell centered velocities and magnetic fields
!-----------------------------------------------------------------------

  call xup_set(Ux, Uxc)
  call yup_set(Uy, Uyc)
  call zup_set(Uz, Uzc)

  call xup_set(Bx, Bxc)
  call yup_set(By, Byc)
  call zup_set(Bz, Bzc)

!-----------------------------------------------------------------------
! Vertical derivative
!-----------------------------------------------------------------------

  call ddydn_set(r, drdy)

  call ddydn_set(p, dpdy)

  call ddydn_set(Uxc, dUxdy)

  call ddydn_set(Uyc, dUydy)

  call ddydn_set(Uzc, dUzdy)

  call ydn_set(lnr, scr2)

  do iz=izs,ize
    rdn(:,:,iz) = exp(scr2(:,:,iz))
  end do

  call ddydn_set(Bxc, dBxdy)

  call ddydn_set(Byc, dBydy)

  call ddydn_set(Bzc, dBzdy)

!----------------------------------------------------------------------
! Wave speeds 
!----------------------------------------------------------------------

  do iz = izs, ize

    Cs(:,:,iz) = sqrt(p(:,:,iz) / r(:,:,iz) * (dlnPdlnr_E(:,:,iz) + &
                 p(:,:,iz) * dlnPdE_r(:,:,iz) / r(:,:,iz)))

  end do

  call ydn_set(Cs, Csdn)

!!$omp single
!  write(*, *) Uy(9,6,3) / Csdn(9,6,3), Uy(9,7,3) / Csdn(9,7,3), Uy(9,8,3) / Csdn(9,8,3) 
!!$omp end single

  call ydn_set(Bxc, scr1)
  call ydn_set(Bzc, scr2)

  do iz = izs, ize
    do iy = 1, my
      do ix = 1, mx

        csq = Csdn(ix,iy,iz)**2

        caq = (scr1(ix,iy,iz)**2 + By(ix,iy,iz)**2 + scr2(ix,iy,iz)**2) / &
               rdn(ix,iy,iz)

        vaq = By(ix,iy,iz)**2 / rdn(ix,iy,iz)

        dd = sqrt(max((csq + caq)**2 - 4.0 * vaq * csq, 0.0))

        vfq = 0.5 * (caq + csq + dd) 
        vsq = max(0.5 * (caq + csq - dd), 0.0)

        vfdn(ix,iy,iz) = sqrt(vfq)
        vsdn(ix,iy,iz) = sqrt(vsq)

        if(dd > 0.00001 * vfq) then
          alpha_f(ix,iy,iz) = sqrt(max((csq - vsq), 0.0) / dd)
          alpha_s(ix,iy,iz) = sqrt(max((vfq - csq), 0.0) / dd)
        else
          alpha_f(ix,iy,iz) = sqrt(0.5)
          alpha_s(ix,iy,iz) = sqrt(0.5)
        end if

        vadn(ix,iy,iz) = sqrt(vaq)

        Bt = sqrt(scr1(ix,iy,iz)**2 + scr2(ix,iy,iz)**2)

        if(Bt**2 > 0.00001 * p(ix,iy,iz)) then
          beta_x(ix,iy,iz) = scr1(ix,iy,iz) / Bt
          beta_z(ix,iy,iz) = scr2(ix,iy,iz) / Bt
        else
          beta_x(ix,iy,iz) = sqrt(0.5)
          beta_z(ix,iy,iz) = sqrt(0.5)
        end if

      end do
    end do
  end do

!----------------------------------------------------------------------
! HD limit test
!----------------------------------------------------------------------

!  do iz = izs, ize
!    dBxdy(:,:,iz) = 0.0
!    dBydy(:,:,iz) = 0.0
!    dBzdy(:,:,iz) = 0.0
!
!    vfdn(:,:,iz) = Csdn(:,:,iz)
!    vsdn(:,:,iz) = 0.0
!    vadn(:,:,iz) = 0.0
!
!    alpha_f(:,:,iz) = 1.0
!    alpha_s(:,:,iz) = 0.0
!
!    beta_x(:,:,iz) = sqrt(0.5)
!    beta_z(:,:,iz) = sqrt(0.5)
!  end do

!----------------------------------------------------------------------
! Wave amplitudes for outgoing charcteristics
!----------------------------------------------------------------------

  do iz = izs, ize
    scr3(:,:,iz)=Csdn(:,:,iz)**2*drdy(:,:,iz)-dpdy(:,:,iz)
    scr1(:,:,iz)=beta_x(:,:,iz)*dUxdy(:,:,iz)+beta_z(:,:,iz)*dUzdy(:,:,iz)
    scr2(:,:,iz)=beta_x(:,:,iz)*dBxdy(:,:,iz)+beta_z(:,:,iz)*dBzdy(:,:,iz)
  end do

  call yup_set(scr3, scr5)

  do iz = izs, ize
    do iy=1,my/2

      d1(:,iy,iz) = 0.5 * (Uy(:,iy,iz) - vfdn(:,iy,iz)) * ( &
                   alpha_f(:,iy,iz) * (dpdy(:,iy,iz) - &
                   rdn(:,iy,iz) * vfdn(:,iy,iz) * dUydy(:,iy,iz)) + &
                   alpha_s(:,iy,iz) * (rdn(:,iy,iz) * vsdn(:,iy,iz) * &
                            sign(1.0, Byc(:,lb,iz)) * scr1(:,iy,iz) + &
                   sqrt(rdn(:,iy,iz)) * csdn(:,iy,iz) * scr2(:,iy,iz)))

      d2(:,iy,iz) = 0.5 * (Uy(:,iy,iz) - vadn(:,iy,iz)) * ( &
                   sign(1.0, Byc(:,lb,iz)) * (beta_z(:,iy,iz) * dUxdy(:,iy,iz) - &
                   beta_x(:,iy,iz) * dUzdy(:,iy,iz)) + &
                   (beta_z(:,iy,iz) * dBxdy(:,iy,iz) - &
                   beta_x(:,iy,iz) * dBzdy(:,iy,iz)) / sqrt(rdn(:,iy,iz)))
                
      d3(:,iy,iz) = 0.5 * (Uy(:,iy,iz) - vsdn(:,iy,iz)) * ( &
                   alpha_s(:,iy,iz) * (dpdy(:,iy,iz) - &
                   rdn(:,iy,iz) * vsdn(:,iy,iz) * dUydy(:,iy,iz)) + &
                   alpha_f(:,iy,iz) * (-rdn(:,iy,iz) * vfdn(:,iy,iz) * &
                            sign(1.0, Byc(:,lb,iz)) * scr1(:,iy,iz) - &
                   sqrt(rdn(:,iy,iz)) * csdn(:,iy,iz) * scr2(:,iy,iz)))

      d4(:,iy,iz) = Uyc(:,iy,iz) * scr5(:,iy,iz)

      d5(:,iy,iz) = Uy(:,iy,iz) * dBydy(:,iy,iz)

      d6(:,iy,iz) = 0.5 * (Uy(:,iy,iz) + vsdn(:,iy,iz)) * ( &
                   alpha_s(:,iy,iz) * (dpdy(:,iy,iz) + &
                   rdn(:,iy,iz) * vsdn(:,iy,iz) * dUydy(:,iy,iz)) + &
                   alpha_f(:,iy,iz) * (rdn(:,iy,iz) * vfdn(:,iy,iz) * &
                            sign(1.0, Byc(:,lb,iz)) * scr1(:,iy,iz) - &
                   sqrt(rdn(:,iy,iz)) * csdn(:,iy,iz) * scr2(:,iy,iz)))

      d7(:,iy,iz) = 0.5 * (Uy(:,iy,iz) + vadn(:,iy,iz)) * ( &
                   -sign(1.0, Byc(:,lb,iz)) * (beta_z(:,iy,iz) * dUxdy(:,iy,iz) - &
                   beta_x(:,iy,iz) * dUzdy(:,iy,iz)) + &
                   (beta_z(:,iy,iz) * dBxdy(:,iy,iz) - &
                   beta_x(:,iy,iz) * dBzdy(:,iy,iz)) / sqrt(rdn(:,iy,iz)))
                
      d8(:,iy,iz) = 0.5 * (Uy(:,iy,iz) + vfdn(:,iy,iz)) * ( &
                   alpha_f(:,iy,iz) * (dpdy(:,iy,iz) + &
                   rdn(:,iy,iz) * vfdn(:,iy,iz) * dUydy(:,iy,iz)) + &
                   alpha_s(:,iy,iz) * (-rdn(:,iy,iz) * vsdn(:,iy,iz) * &
                            sign(1.0, Byc(:,lb,iz)) * scr1(:,iy,iz) + &
                   sqrt(rdn(:,iy,iz)) * csdn(:,iy,iz) * scr2(:,iy,iz)))

    end do
  end do

  do iz = izs, ize
    do iy=my/2+1,my

      d1(:,iy,iz) = 0.5 * (Uy(:,iy,iz) - vfdn(:,iy,iz)) * ( &
                   alpha_f(:,iy,iz) * (dpdy(:,iy,iz) - &
                   rdn(:,iy,iz) * vfdn(:,iy,iz) * dUydy(:,iy,iz)) + &
                   alpha_s(:,iy,iz) * (rdn(:,iy,iz) * vsdn(:,iy,iz) * &
                            sign(1.0, Byc(:,ub,iz)) * scr1(:,iy,iz) + &
                   sqrt(rdn(:,iy,iz)) * csdn(:,iy,iz) * scr2(:,iy,iz)))

      d2(:,iy,iz) = 0.5 * (Uy(:,iy,iz) - vadn(:,iy,iz)) * ( &
                   sign(1.0, Byc(:,ub,iz)) * (beta_z(:,iy,iz) * dUxdy(:,iy,iz) - &
                   beta_x(:,iy,iz) * dUzdy(:,iy,iz)) + &
                   (beta_z(:,iy,iz) * dBxdy(:,iy,iz) - &
                   beta_x(:,iy,iz) * dBzdy(:,iy,iz)) / sqrt(rdn(:,iy,iz)))
                
      d3(:,iy,iz) = 0.5 * (Uy(:,iy,iz) - vsdn(:,iy,iz)) * ( &
                   alpha_s(:,iy,iz) * (dpdy(:,iy,iz) - &
                   rdn(:,iy,iz) * vsdn(:,iy,iz) * dUydy(:,iy,iz)) + &
                   alpha_f(:,iy,iz) * (-rdn(:,iy,iz) * vfdn(:,iy,iz) * &
                            sign(1.0, Byc(:,ub,iz)) * scr1(:,iy,iz) - &
                   sqrt(rdn(:,iy,iz)) * csdn(:,iy,iz) * scr2(:,iy,iz)))

      d4(:,iy,iz) = Uyc(:,iy,iz) * scr5(:,iy,iz)

      d5(:,iy,iz) = Uy(:,iy,iz) * dBydy(:,iy,iz)

      d6(:,iy,iz) = 0.5 * (Uy(:,iy,iz) + vsdn(:,iy,iz)) * ( &
                   alpha_s(:,iy,iz) * (dpdy(:,iy,iz) + &
                   rdn(:,iy,iz) * vsdn(:,iy,iz) * dUydy(:,iy,iz)) + &
                   alpha_f(:,iy,iz) * (rdn(:,iy,iz) * vfdn(:,iy,iz) * &
                            sign(1.0, Byc(:,ub,iz)) * scr1(:,iy,iz) - &
                   sqrt(rdn(:,iy,iz)) * csdn(:,iy,iz) * scr2(:,iy,iz)))

      d7(:,iy,iz) = 0.5 * (Uy(:,iy,iz) + vadn(:,iy,iz)) * ( &
                   -sign(1.0, Byc(:,ub,iz)) * (beta_z(:,iy,iz) * dUxdy(:,iy,iz) - &
                   beta_x(:,iy,iz) * dUzdy(:,iy,iz)) + &
                   (beta_z(:,iy,iz) * dBxdy(:,iy,iz) - &
                   beta_x(:,iy,iz) * dBzdy(:,iy,iz)) / sqrt(rdn(:,iy,iz)))
                
      d8(:,iy,iz) = 0.5 * (Uy(:,iy,iz) + vfdn(:,iy,iz)) * ( &
                   alpha_f(:,iy,iz) * (dpdy(:,iy,iz) + &
                   rdn(:,iy,iz) * vfdn(:,iy,iz) * dUydy(:,iy,iz)) + &
                   alpha_s(:,iy,iz) * (-rdn(:,iy,iz) * vsdn(:,iy,iz) * &
                            sign(1.0, Byc(:,ub,iz)) * scr1(:,iy,iz) + &
                   sqrt(rdn(:,iy,iz)) * csdn(:,iy,iz) * scr2(:,iy,iz)))

    end do
  end do


  call ddt_boundary_top (r,px,py,pz,e,Bx,By,Bz, &
                         drdt,dpxdt,dpydt,dpzdt, &
                         dedt,dBxdt,dBydt,dBzdt, &
                         Ux,Uy,Uz,lnr,p,ee,cs)

  call ddt_boundary_bot (r,px,py,pz,e,Bx,By,Bz, &
                         drdt,dpxdt,dpydt,dpzdt, &
                         dedt,dBxdt,dBydt,dBzdt, &
                         Ux,Uy,Uz,lnr,p,ee,cs)

  call barrier_omp ('ddt40')
  call dumpn(drdt,'ddt2:drdt','bdry.dmp',1)
  call dumpn(dpxdt,'ddt2:dpxdt','bdry.dmp',1)
  call dumpn(dpydt,'ddt2:dpydt','bdry.dmp',1)
  call dumpn(dpzdt,'ddt2:dpzdt','bdry.dmp',1)
  call dumpn(dedt,'ddt2:dedt','bdry.dmp',1)
  call dumpn(dBxdt,'ddt2:dBxdt','bdry.dmp',1)
  call dumpn(dBydt,'ddt2:dBydt','bdry.dmp',1)
  call dumpn(dBzdt,'ddt2:dBzdt','bdry.dmp',1)

END

!-----------------------------------------------------------------------
SUBROUTINE ddt_boundary_top (r,px,py,pz,e,Bx,By,Bz, &
                             drdt,dpxdt,dpydt,dpzdt, &
                             dedt,dBxdt,dBydt,dBzdt, &
                             Ux,Uy,Uz,lnr,p,ee,Cs)
  USE params
  USE arrays, ONLY: scr1,scr2,scr3,scr4,scr5,scr6,eeav
  USE forcing
  USE eos
  USE boundary
  USE timeintegration, ONLY: alpha
  implicit none
  real, dimension(mx,my,mz):: r,px,py,pz,e,Bx,By,Bz, &
                              drdt,dpxdt,dpydt,dpzdt, &
                              dedt,dBxdt,dBydt,dBzdt, &
                              Ux,Uy,Uz,lnr,p,ee,Cs
                              
  real ::                     trelaxt, htopi, nuS1, nuS2
  integer ix,iy,iz
  logical omp_in_parallel

  character(len=mid):: id='$Id: boundary_charbc.f90,v 1.13 2009/06/26 03:11:00 bob Exp $'
  call print_id (id)

!----------------------------------------------------------------------
  if(mpi_y == 0) then
!-----------------------------------------------------------------------
! Top boundary
!-----------------------------------------------------------------------

  if(isubstep == 1) then
    do iz = izs, ize
      drdt_top(:,iz) = 0.0
      dpxdt_top(:,iz) = 0.0
      dpydt_top(:,iz) = 0.0
      dpzdt_top(:,iz) = 0.0
      dedt_top(:,iz) = 0.0
      dBxdt_top(:,iz) = 0.0
      dBydt_top(:,iz) = 0.0
      dBzdt_top(:,iz) = 0.0
    end do
  else
    do iz = izs, ize
      drdt_top(:,iz) = alpha(isubstep) * drdt_top(:,iz)
      dpxdt_top(:,iz) = alpha(isubstep) * dpxdt_top(:,iz)
      dpydt_top(:,iz) = alpha(isubstep) * dpydt_top(:,iz)
      dpzdt_top(:,iz) = alpha(isubstep) * dpzdt_top(:,iz)
      dedt_top(:,iz) = alpha(isubstep) * dedt_top(:,iz)
      dBxdt_top(:,iz) = alpha(isubstep) * dBxdt_top(:,iz)
      dBydt_top(:,iz) = alpha(isubstep) * dBydt_top(:,iz)
      dBzdt_top(:,iz) = alpha(isubstep) * dBzdt_top(:,iz)
    end do
  end if

  call barrier_omp ('ddt2')
  htopi = gy*rav(lb)/pav(lb)
  trelaxt = pdamp / max((maxval(abs(Uyc(:,lb,:))) / dx + 1.0e-30) , &
            maxval(r(:,lb,:) * Cs(:,lb,:) * gy / p(:,lb,:)) )

!-----------------------------------------------------------------------
! Horizontal derivatives
!-----------------------------------------------------------------------

  call ddxdn32_set(p, lb, dpdx)
  call ddzdn32_set(p, lb, dpdz)
 
  call ddxup32_set(px, lb, divh_ph)
  call ddzup32_add(pz, lb, divh_ph)

  call ddxup32_set(Ux, lb, divh_uh)
  call ddzup32_add(Uz, lb, divh_uh)

  call xdn32_set(lnr, lb, scra)
  call barrier_omp ('ddt3')
  call zdn22_set(scra, scrb)

  call zdn32_set(Ux, lb, scrc)
  call xdn32_set(Uz, lb, scrd)

  call barrier_omp ('ddt4')
  do iz = izs, ize
    scra(:,iz) = exp(scrb(:,iz)) * scrc(:,iz) * scrd(:,iz)
    scre(:,iz) = r(:,lb,iz) * Uxc(:,lb,iz)**2 + p(:,lb,iz)
    scrf(:,iz) = r(:,lb,iz) * Uzc(:,lb,iz)**2 + p(:,lb,iz)
  end do

  call barrier_omp ('ddt5')
  call ddxdn22_set(scre, divh_pxuh)
  call ddzup22_add(scra, divh_pxuh)

  call ddzdn22_set(scrf, divh_pzuh)
  call barrier_omp ('ddt6')
  call ddxup22_add(scra, divh_pzuh)

  call zdn32_set(ee, lb, scrb)
  call xdn32_set(ee, lb, scra)

  do iz = izs, ize
    scra(:,iz) = scra(:,iz) * px(:,lb,iz)
    scrb(:,iz) = scrb(:,iz) * pz(:,lb,iz)
  end do

  call barrier_omp ('ddt7')
  call ddxup22_set(scra, divh_euh)
  call ddzup22_add(scrb, divh_euh)

  call ydn_set(lnr, scr1)
  call xdn32_set(scr1, lb+1, scra)

  call zdn_set(lnr, scr4)
  call barrier_omp ('ddt8')
  call ydn_set(scr4, scr3)

  call ydn_set(Ux, scr1)
  call xdn32_set(Uy, lb+1, scrc)

  call ydn_set(Uz, scr2)
  call zdn32_set(Uy, lb+1, scrd)

  do iz=izs, ize
    scre(:,iz) = exp(scra(:,iz)) * scr1(:,lb+1,iz) * scrc(:,iz)
    scrf(:,iz) = exp(scr3(:,lb+1,iz)) * scr2(:,lb+1,iz) * scrd(:,iz)
  end do

  call barrier_omp ('ddt9')
  call ddxup22_set(scre, divh_pyuh)
  call ddzup22_add(scrf, divh_pyuh)
  call barrier_omp ('ddt10')

!--------------------------------------------------------------------------
! Lorentz force and terms for induction equation
!--------------------------------------------------------------------------

  call xdn_set(By, scr1)
  call zdn_set(By, scr2)

  call ddzdn32_set(Bx, lb, scra)
  call ddxdn32_sub(Bz, lb, scra)

  call ddxdn_set(By, scr3)
  call ddzdn_set(By, scr4)

  call xdn32_set(Bz, lb, scrb)
  call zdn32_set(Bx, lb, scrc)

  do iz=izs, ize
    scre(:,iz) = scra(:,iz) * scrb(:,iz)
    scr5(:,:,iz) = -scr3(:,:,iz) * scr1(:,:,iz)
  end do

  call yup_set(scr5, scr6)
  call barrier_omp ('ddt10b')
  call zup22_set(scre, scrf)

  do iz=izs, ize
    divh_BxBh(:,iz) = scrf(:,iz) + scr6(:,lb,iz)
  end do

  call barrier_omp ('ddt10c')
  do iz=izs, ize
    scre(:,iz) = -scra(:,iz) * scrc(:,iz)
    scr5(:,:,iz) = -scr4(:,:,iz) * scr2(:,:,iz)
  end do

  call yup_set(scr5, scr6)
  call xup22_set(scre, scrf)

  do iz=izs, ize
    divh_BzBh(:,iz) = scrf(:,iz) + scr6(:,lb,iz)
  end do

  call ydn_set(Bx, scr5)
  call ydn_set(Bz, scr6)

  do iz=izs, ize
    scre(:,iz) = scr3(:,lb+1,iz) * scr5(:,lb+1,iz)
    scrf(:,iz) = scr4(:,lb+1,iz) * scr6(:,lb+1,iz)
  end do

  call xup22_set(scre, divh_ByBh)
  call barrier_omp ('ddt10d')
  call zup22_add(scrf, divh_ByBh)


  call zdn32_set(Ux, lb, scra)
  call xdn32_set(Uz, lb, scrd)

  do iz=izs, ize
    scre(:,iz) = scrd(:,iz) * scrc(:,iz) - scra(:,iz) * scrb(:,iz)
  end do

  call barrier_omp ('ddt10d')
  call ddzup22_set(scre, divh_Bxuh)
  call ddxup22_set(scre, divh_Bzuh)

  call xdn32_set(Uy, lb+1, scra)
  call zdn32_set(Uy, lb+1, scrd)

  call ydn_set(Ux, scr3)
  call ydn_set(Uz, scr4)

  call barrier_omp ('ddt10e')
  do iz=izs, ize
    scre(:,iz) = scrd(:,iz) * scr6(:,lb+1,iz) - scr4(:,lb+1,iz) * scr2(:,lb+1,iz)
    scrf(:,iz) = scr3(:,lb+1,iz) * scr1(:,lb+1,iz) - scra(:,iz) * scr5(:,lb+1,iz)
  end do

  call barrier_omp ('ddt10f')
  call ddzup22_set(scre, divh_Byuh)
  call ddxup22_sub(scrf, divh_Byuh)
  call barrier_omp ('ddt10g')

!-----------------------------------------------------------------------
! Apply boundary conditions on ingoing characteristics
!-----------------------------------------------------------------------

!  do iz = izs , ize
!    lnrtop(:,iz)=alog(rtop(:,iz))
!    lnrtop(:,iz)=lnrtop(:,iz)+alog(1.-fmtop_frac*dt*htopi*real(py(:,lb+1,iz))/(0.5*(r(:,lb+1,iz)+r(:,lb,iz))))
!    rtop(:,iz) = (1.-rtop_frac)*rtop(:,iz) + rtop_frac*exp(lnrtop(:,iz))
!   rtop(:,iz) = (1.-rtop_frac)*rtop(:,iz) + rtop_frac*rav(lb)
!   etop(:,iz) = rtop(:,iz) * ee_top
!  end do

  call ydn_set(Uxc, scr1)
  call ydn_set(Uzc, scr2)

  do iz = izs, ize
    scr3(:,:,iz) = beta_x(:,:,iz) * scr1(:,:,iz) + beta_z(:,:,iz) * scr2(:,:,iz)
    scr4(:,:,iz) = beta_z(:,:,iz) * scr1(:,:,iz) - beta_x(:,:,iz) * scr2(:,:,iz)
  end do

  call ydn_set(Bxc, scr1)
  call ydn_set(Bzc, scr2)

  do iz = izs, ize
    scr5(:,:,iz) = beta_x(:,:,iz) * (scr1(:,:,iz) - bx0) + beta_z(:,:,iz) * (scr2(:,:,iz) - bz0)
    scr6(:,:,iz) = beta_z(:,:,iz) * (scr1(:,:,iz) - bx0) - beta_x(:,:,iz) * (scr2(:,:,iz) - bz0)
  end do

if(nonreflecting) then

  do iz = izs, ize
    do iy = 1, my/2
      do ix = 1,mx

      if (Uy(ix,lb+1,iz) .gt. vfdn(ix,lb+1,iz) .or. Uy(ix,lb,iz) .gt. vfdn(ix,lb,iz)) &
        d1(ix,iy,iz) = -0.5 * alpha_f(ix,iy,iz) * rdn(ix,iy,iz) * vfdn(ix,iy,iz) * gy 

      if (Uy(ix,lb,iz) .gt. vadn(ix,lb,iz)) &
        d2(ix,iy,iz) = 0.0

      if (Uy(ix,lb+1,iz) .gt. vsdn(ix,lb+1,iz)) &
        d3(ix,iy,iz) = -0.5 * alpha_s(ix,iy,iz) * rdn(ix,iy,iz) * vsdn(ix,iy,iz) * gy 

      if (Uy(ix,lb+1,iz) .gt. 0.0 .or. Uy(ix,lb,iz) .gt. 0.0) &
         d4(ix,iy,iz) = 0.0

      if(Uy(ix,lb+1,iz) .gt. 0.0) &
         d5(ix,iy,iz) = 0.0

      if (Uy(ix,lb+1,iz) .gt. -vsdn(ix,lb+1,iz)) &
        d6(ix,iy,iz) = 0.5 * alpha_s(ix,iy,iz) * rdn(ix,iy,iz) * vsdn(ix,iy,iz) * gy 

      if (Uy(ix,lb,iz) .gt. -vadn(ix,lb,iz)) &
        d7(ix,iy,iz) = 0.0

      if (Uy(ix,lb+1,iz) .gt. -vfdn(ix,lb+1,iz)) &
        d8(ix,iy,iz) = 0.5 * alpha_f(ix,iy,iz) * rdn(ix,iy,iz) * vfdn(ix,iy,iz) * gy 

      end do
    end do
  end do

else

  do iz = izs, ize
    do iy = 1, my/2
      do ix = 1,mx

      if (Uy(ix,lb+1,iz) .gt. vfdn(ix,lb+1,iz) .or. Uy(ix,lb,iz) .gt. vfdn(ix,lb,iz)) &
        d1(ix,iy,iz) = -0.5 * alpha_f(ix,iy,iz) * rdn(ix,iy,iz) * vfdn(ix,iy,iz) * gy - &
                        5.0 * alpha_f(ix,iy,iz) * vfdn(ix,iy,iz) * py(ix,iy,iz) * gy / csdn(ix,iy,iz) + &
                        0.5 * rdn(ix,iy,iz) * alpha_s(ix,iy,iz) * vsdn(ix,iy,iz) * &
                        sign(1.0, Byc(ix,lb,iz)) * scr3(ix,iy,iz) / trelaxt + &
                        0.5 * sqrt(rdn(ix,iy,iz)) * alpha_s(ix,iy,iz) * Csdn(ix,iy,iz) * &
                        scr5(ix,iy,iz) / trelaxt

      if (Uy(ix,lb,iz) .gt. vadn(ix,lb,iz)) &
        d2(ix,iy,iz) = 0.5 * sign(1.0, Byc(ix,lb,iz)) * scr4(ix,iy,iz) / trelaxt + &
                       0.5 * scr6(ix,iy,iz) / (sqrt(rdn(ix,iy,iz)) * trelaxt)

      if (Uy(ix,lb+1,iz) .gt. vsdn(ix,lb+1,iz)) &
        d3(ix,iy,iz) = -0.5 * alpha_s(ix,iy,iz) * rdn(ix,iy,iz) * vsdn(ix,iy,iz) * gy - &
                        5.0 * alpha_s(ix,iy,iz) * vsdn(ix,iy,iz) * py(ix,iy,iz) * gy / csdn(ix,iy,iz) - &
                        0.5 * rdn(ix,iy,iz) * alpha_f(ix,iy,iz) * vfdn(ix,iy,iz) * &
                        sign(1.0, Byc(ix,lb,iz)) * scr3(ix,iy,iz) / trelaxt - &
                        0.5 * sqrt(rdn(ix,iy,iz)) * alpha_f(ix,iy,iz) * Csdn(ix,iy,iz) * &
                        scr5(ix,iy,iz) / trelaxt

      if (Uy(ix,lb+1,iz) .gt. 0.0 .or. Uy(ix,lb,iz) .gt. 0.0) &
        d4(ix,iy,iz) = - 1.*Cs(ix,iy,iz) * (p(ix,iy,iz)/r(ix,iy,iz)) * dlnPdE_r(ix,iy,iz) * &
                      ( (e(ix,iy,iz) + p(ix,iy,iz)) * &
                      (rtop(ix,iz) - r(ix,lb,iz))/r(ix,iy,iz) - &
                      r(ix,iy,iz)*(ee_top - ee(ix,lb,iz)) ) * htopi
!         d4(ix,iy,iz) = 0.0

      if(Uy(ix,lb+1,iz) .gt. 0.0) &
         d5(ix,iy,iz) = 0.0

      if (Uy(ix,lb+1,iz) .gt. -vsdn(ix,lb+1,iz)) &
        d6(ix,iy,iz) = 0.5 * alpha_s(ix,iy,iz) * rdn(ix,iy,iz) * vsdn(ix,iy,iz) * gy + &
                        0.25 * alpha_s(ix,iy,iz) * vsdn(ix,iy,iz) * py(ix,iy,iz) * gy / csdn(ix,iy,iz) + &
                        0.5 * rdn(ix,iy,iz) * alpha_f(ix,iy,iz) * vfdn(ix,iy,iz) * &
                        sign(1.0, Byc(ix,lb,iz)) * scr3(ix,iy,iz) / trelaxt - &
                        0.5 * sqrt(rdn(ix,iy,iz)) * alpha_f(ix,iy,iz) * Csdn(ix,iy,iz) * &
                        scr5(ix,iy,iz) / trelaxt

      if (Uy(ix,lb,iz) .gt. -vadn(ix,lb,iz)) &
        d7(ix,iy,iz) = -0.5 * sign(1.0, Byc(ix,lb,iz)) * scr4(ix,iy,iz) / trelaxt + &
                       0.5 * scr6(ix,iy,iz) / (sqrt(rdn(ix,iy,iz)) * trelaxt)

      if (Uy(ix,lb+1,iz) .gt. -vfdn(ix,lb+1,iz)) &
        d8(ix,iy,iz) = 0.5 * alpha_f(ix,iy,iz) * rdn(ix,iy,iz) * vfdn(ix,iy,iz) * gy + &
                        0.25 * alpha_f(ix,iy,iz) * vfdn(ix,iy,iz) * py(ix,iy,iz) * gy / csdn(ix,iy,iz) - &
                        0.5 * rdn(ix,iy,iz) * alpha_s(ix,iy,iz) * vsdn(ix,iy,iz) * &
                        sign(1.0, Byc(ix,lb,iz)) * scr3(ix,iy,iz) / trelaxt + &
                        0.5 * sqrt(rdn(ix,iy,iz)) * alpha_s(ix,iy,iz) * Csdn(ix,iy,iz) * &
                        scr5(ix,iy,iz) / trelaxt

      end do
    end do
  end do

end if

!-----------------------------------------------------------------------
! Time derivatives on the boundary
!-----------------------------------------------------------------------

   do iz = izs, ize
     scr1(:,:,iz) = alpha_f(:,:,iz) * (d1(:,:,iz) + d8(:,:,iz)) + &
                    alpha_s(:,:,iz) * (d3(:,:,iz) + d6(:,:,iz))
     scr4(:,:,iz) = alpha_f(:,:,iz) * vfdn(:,:,iz) * (d6(:,:,iz) - d3(:,:,iz)) + &
                    alpha_s(:,:,iz) * vsdn(:,:,iz) * (d1(:,:,iz) - d8(:,:,iz))
   end do

  call ydn_set(d4, scr2)
  call yup_set(scr1, scr3)

  do iz = izs, ize

    scra(:,iz) = (d4(:,lb,iz) + scr3(:,lb,iz)) / Cs(:,lb,iz)**2
    scrb(:,iz) = (scr2(:,lb+1,iz) + scr1(:,lb+1,iz)) / Csdn(:,lb+1,iz)**2

  end do

  do iz = izs, ize

    drdt_top(:,iz) = drdt_top(:,iz) - scra(:,iz) - divh_ph(:,iz)

    dedt_top(:,iz) = dedt_top(:,iz) + d4(:,lb,iz) * r(:,lb,iz) / (p(:,lb,iz) * dlnPdE_r(:,lb,iz)) - &
                     (e(:,lb,iz) + p(:,lb,iz)) * scra(:,iz) / r(:,lb,iz) - &
                     divh_euh(:,iz) - p(:,lb,iz) * divh_uh(:,iz)

    dpydt_top(:,iz) = dpydt_top(:,iz) - Uy(:,lb+1,iz) * scrb(:,iz) + &
                      (alpha_f(:,lb+1,iz) * vfdn(:,lb+1,iz) * (d1(:,lb+1,iz) - d8(:,lb+1,iz)) + &
                       alpha_s(:,lb+1,iz) * vsdn(:,lb+1,iz) * (d3(:,lb+1,iz) - d6(:,lb+1,iz))) / Csdn(:,lb+1,iz)**2 + &
                      rdn(:,lb+1,iz) * gy - divh_pyuh(:,iz) + divh_ByBh(:,iz)

  end do

  do iz = izs, ize
    do iy = 1, my

      scr1(:,iy,iz) = (-scr4(:,iy,iz) * beta_x(:,iy,iz) / Csdn(:,iy,iz)**2 + &
                     rdn(:,iy,iz) * beta_z(:,iy,iz) * (d7(:,iy,iz) - d2(:,iy,iz))) &
                     * sign(1.0, Byc(:,lb,iz))
      scr2(:,iy,iz) = (-scr4(:,iy,iz) * beta_z(:,iy,iz) / Csdn(:,iy,iz)**2 - &
                     rdn(:,iy,iz) * beta_x(:,iy,iz) * (d7(:,iy,iz) - d2(:,iy,iz))) &
                     * sign(1.0, Byc(:,lb,iz))

    end do
  end do

  call yup_set(scr1, scr5)
  call yup_set(scr2, scr6)

  do iz = izs, ize

    scre(:,iz) = -Uxc(:,lb,iz) * scra(:,iz) + scr5(:,lb,iz)
    scrf(:,iz) = -Uzc(:,lb,iz) * scra(:,iz) + scr6(:,lb,iz)

  end do

  call barrier_omp ('ddt11')
  call xdn22_set(scre, scrc)
  call zdn22_set(scrf, scrd)

  do iz = izs, ize

    dpxdt_top(:,iz) = dpxdt_top(:,iz) + scrc(:,iz) - divh_pxuh(:,iz) + divh_BxBh(:,iz)

    dpzdt_top(:,iz) = dpzdt_top(:,iz) + scrd(:,iz) - divh_pzuh(:,iz) + divh_BzBh(:,iz)

  end do

  do iz = izs, ize
    scr3(:,:,iz) = alpha_s(:,:,iz) * (d1(:,:,iz) + d8(:,:,iz)) - &
                   alpha_f(:,:,iz) * (d3(:,:,iz) + d6(:,:,iz))
  end do

  do iz = izs, ize
    scr1(:,:,iz) = -beta_x(:,:,iz) * scr3(:,:,iz) / (sqrt(rdn(:,:,iz)) * Csdn(:,:,iz)) - &
                sqrt(rdn(:,:,iz)) * beta_z(:,:,iz) * (d2(:,:,iz) + d7(:,:,iz))
    scr2(:,:,iz) = -beta_z(:,:,iz) * scr3(:,:,iz) / (sqrt(rdn(:,:,iz)) * Csdn(:,:,iz)) + &
                sqrt(rdn(:,:,iz)) * beta_x(:,:,iz) * (d2(:,:,iz) + d7(:,:,iz))
  end do   

  call yup_set(scr1, scr5)
  call yup_set(scr2, scr6)

  call barrier_omp ('ddt11b')
  call xdn32_set(scr5, lb, scrc)
  call zdn32_set(scr6, lb, scrd)

  do iz = izs, ize

    dBydt_top(:,iz) = dBydt_top(:,iz) - d5(:,lb+1,iz) + divh_Byuh(:,iz)

    dBxdt_top(:,iz) = dBxdt_top(:,iz) + scrc(:,iz) - divh_Bxuh(:,iz)

    dBzdt_top(:,iz) = dBzdt_top(:,iz) + scrd(:,iz) + divh_Bzuh(:,iz)

  end do

!-----------------------------------------------------------------------
! Diffusion - horizontal directions
!-----------------------------------------------------------------------
! S_ii = ddiup(U_i)
! S_ij = 0.5*(ddidn(U_j)+ddjdn(U_i))
! T_ii = 2*d_i*(nu+d_i*nud)*S_ii
! nu_ij = exp(idn(jdn(log(nu+.5*(d_i+d_j)*nud))))
! T_ij = (d_i+d_j)*nu_ij*S_ij
! dedt = dedt + sum_i [(T_ii*S_ii)]
! dedt = dedt + sum_ij [(d_i+d_j)*nu*iup(jup(S_ij))^2]
! dp_idt = dp_idt + ddjdn(T_ii) + sum_j [ddjup(T_ij)]
!-----------------------------------------------------------------------

  nuS1 = 3.*nuS/(1.+2.*nuS)
  nuS2 = (1.-nuS)/(1.+2.*nuS)

  call barrier_omp ('ddt12')

  call ddxup_set(Ux, scr4)
  call ddyup_add(Uy, scr4)
  call ddzup_add(Uz, scr4)

  call ddxup32_set(Ux, lb, scra)

  do iz = izs, ize
    do ix = 1, mx
!      scrb(ix,iz) = 2.0 * dxm(ix) * (wk06(ix,lb,iz) + dxm(ix) * wk19(ix,lb,iz)) * &
!                    (nuS1 * scra(ix,iz) + nuS2 * scr4(ix,lb,iz))
      scrb(ix,iz) = 2.0 * dxm(ix) * (nu_top(ix,1,iz) + dxm(ix) * nud_top(ix,1,iz)) * &
                    (nuS1 * scra(ix,iz) + nuS2 * scr4(ix,lb,iz))
    end do
  end do

  call ddxdn22_set(scrb, scrc)

  do iz = izs, ize
    dpxdt_top(:,iz) = dpxdt_top(:,iz) + scrc(:,iz)
    dedt_top(:,iz) = dedt_top(:,iz) + scra(:,iz) * scrb(:,iz)
  end do
    
  call ddyup_set(Uy, scr1)

!  do iz = izs, ize
!    do iy = 1, my
!      scr2(:,iy,iz) = 2.0 * dym(iy) * (wk06(:,iy,iz) + dym(iy) * wk19(:,iy,iz)) * &
!                      (nuS1 * scr1(:,iy,iz) + nuS2 * scr4(:,iy,iz))
!    end do
!  end do

  do iz = izs, ize
    scr2(:,lb,iz) = 2.0 * dym(lb) * (nu_top(:,1,iz) + dym(lb) * nud_top(:,1,iz)) * &
                    (nuS1 * scr1(:,lb,iz) + nuS2 * scr4(:,lb,iz))
    scr2(:,lb+1,iz) = 2.0 * dym(lb+1) * (nu_top(:,2,iz) + dym(lb+1) * nud_top(:,2,iz)) * &
                    (nuS1 * scr1(:,lb+1,iz) + nuS2 * scr4(:,lb+1,iz))
  end do


!  call ddydn_set(scr2, scr3)

  do iz = izs, ize
    scr3(:,lb+1,iz) = (scr2(:,lb+1,iz) - scr2(:,lb,iz)) / dym(lb)
  end do

  do iz = izs, ize
    dpydt_top(:,iz) = dpydt_top(:,iz) + scr3(:,lb+1,iz)
    dedt_top(:,iz) = dedt_top(:,iz) + scr1(:,lb,iz) * scr2(:,lb,iz)
  end do

  call ddzup32_set(Uz, lb, scra)

  do iz = izs, ize
!      scrb(:,iz) = 2.0 * dzm(iz) * (wk06(:,lb,iz) + dzm(iz) * wk19(:,lb,iz)) * &
!                   (nuS1 * scra(:,iz) + nuS2 * scr4(:,lb,iz))
      scrb(:,iz) = 2.0 * dzm(iz) * (nu_top(:,1,iz) + dzm(iz) * nud_top(:,1,iz)) * &
                   (nuS1 * scra(:,iz) + nuS2 * scr4(:,lb,iz))
  end do

  call barrier_omp ('ddt13')
  call ddzdn22_set(scrb, scrc)
  call barrier_omp ('ddt14')

  do iz = izs, ize
    dpzdt_top(:,iz) = dpzdt_top(:,iz) + scrc(:,iz)
    dedt_top(:,iz) = dedt_top(:,iz) + scra(:,iz) * scrb(:,iz)
  end do

  call ddxdn32_set(Uz, lb, scra)
  call ddzdn32_add(Ux, lb, scra)

  do iz = izs, ize
    do ix =1, mx
!        scrb(ix,iz) = alog(wk06(ix,lb,iz) + 0.5 * (dxm(ix) + dzm(iz)) * wk19(ix,lb,iz))
!        scrb(ix,iz) = alog(wk06(ix,lb,iz))
        scrb(ix,iz) = alog(nu_top(ix,1,iz))
   end do
  end do

  call xdn22_set(scrb, scrc)
  call barrier_omp ('ddt15')
  call zdn22_set(scrc, scrd)

  do iz = izs, ize
    do ix = 1, mx
      scrd(ix,iz) = 0.5 * nuS1 * (dxmdn(ix) + dzmdn(iz)) * exp(scrd(ix,iz)) * scra(ix,iz)
    end do
  end do

  call barrier_omp ('ddt16')
  call ddxup22_set(scrd, scre)
  call ddzup22_set(scrd, scrf)

  do iz = izs, ize
    dpxdt_top(:,iz) = dpxdt_top(:,iz) + scrf(:,iz)
    dpzdt_top(:,iz) = dpzdt_top(:,iz) + scre(:,iz)
  end do

  call xup22_set(scra, scrc)
  call barrier_omp ('ddt17')
  call zup22_set(scrc, scrd)

  do iz = izs, ize
    scrb(:,iz) = 0.25 * scrd(:,iz)**2
  end do

  do iz = izs, ize
    do ix = 1, mx
!      dedt_top(ix,iz) = dedt_top(ix,iz) + nuS1 * (dxm(ix) + dzm(iz)) * wk06(ix,lb,iz) * scrb(ix,iz)
      dedt_top(ix,iz) = dedt_top(ix,iz) + nuS1 * (dxm(ix) + dzm(iz)) * nu_top(ix,1,iz) * scrb(ix,iz)
    end do
  end do

  call ddxdn_set(Uy, scr1)
  call ddydn_add(Ux, scr1)

!  do iz = izs, ize
!    do ix = 1, mx
!      do iy = 1, my
!        scr2(ix,iy,iz) = alog(wk06(ix,iy,iz) + 0.5 * (dxm(ix) + dym(iy)) * wk19(ix,iy,iz))
!        scr2(ix,iy,iz) = alog(wk06(ix,iy,iz))
!      end do
!    end do
!  end do

!  call ydn_set(scr2, scr3)

  do iz = izs, ize
    do ix = 1, mx
      scr3(ix,lb+1,iz) = 0.5 * (alog(nu_top(ix,1,iz)) + alog(nu_top(ix,2,iz)))
    end do
  end do


  call xdn32_set(scr3,lb+1,scra)

  do iz = izs, ize
    do ix = 1, mx
      scra(ix,iz) = 0.5 * nuS1 * (dxmdn(ix) + dymdn(lb+1)) * exp(scra(ix,iz)) * scr1(ix,lb+1,iz)
    end do
  end do

  call ddxup22_set(scra, scrb)

  do iz = izs, ize
    dpydt_top(:,iz) = dpydt_top(:,iz) + scrb(:,iz)
  end do

  call yup_set(scr1, scr3)
  call xup32_set(scr3, lb, scra)

  do iz = izs, ize
    scrb(:,iz) = 0.25 * scra(:,iz)**2
  end do

  do iz = izs, ize
    do ix = 1, mx
!      dedt_top(ix,iz) = dedt_top(ix,iz) + nuS1 * (dxm(ix) + dym(lb)) * wk06(ix,lb,iz) * scrb(ix,iz)
      dedt_top(ix,iz) = dedt_top(ix,iz) + nuS1 * (dxm(ix) + dym(lb)) * nu_top(ix,1,iz) * scrb(ix,iz)
    end do
  end do

  call ddzdn_set(Uy, scr1)
  call barrier_omp ('ddt18')
  call ddydn_add(Uz, scr1)

!  do iz = izs, ize
!    do iy = 1, my
!      scr2(:,iy,iz) = alog(wk06(:,iy,iz) + 0.5 * (dym(iy) + dzm(iz)) * wk19(:,iy,iz))
!      scr2(:,iy,iz) = alog(wk06(:,iy,iz))
!    end do
!  end do

!  call ydn_set(scr2, scr3)

  do iz = izs, ize
    scr3(:,lb+1,iz) = 0.5 * (alog(nu_top(:,1,iz)) + alog(nu_top(:,2,iz)))
  end do

  call barrier_omp ('ddt19')
  call zdn32_set(scr3, lb+1, scra)

  do iz = izs, ize
    scra(:,iz) = 0.5 * nuS1 * (dymdn(lb+1) + dzmdn(iz)) * exp(scra(:,iz)) * scr1(:,lb+1,iz)
  end do

  call barrier_omp ('ddt20')
  call ddzup22_set(scra, scrb)

  do iz = izs, ize
    dpydt_top(:,iz) = dpydt_top(:,iz) + scrb(:,iz)
  end do

  call yup_set(scr1, scr3)
  call barrier_omp ('ddt21')
  call zup32_set(scr3, lb, scra)

  do iz = izs, ize
    scrb(:,iz) = 0.25 * scra(:,iz)**2
  end do

  do iz = izs, ize
!    dedt_top(:,iz) = dedt_top(:,iz) + nuS1 * (dym(lb) + dzm(iz)) * wk06(:,lb,iz) * scrb(:,iz)
    dedt_top(:,iz) = dedt_top(:,iz) + nuS1 * (dym(lb) + dzm(iz)) * nu_top(:,1,iz) * scrb(:,iz)
 end do

!-----------------------------------------------------------------------
! Limit inflows to sonic
!-----------------------------------------------------------------------
!  call haverage_subr (Cs,Csav)				! mass flux
! do iz = izs, ize
!   do ix=1,mx
!      if ((Uy(ix,lb+1,iz) .gt. Csav(lb+1) .or. Uy(ix,lb,iz) .gt. Csav(lb)) .and. pdamp .gt. 0.) &
!        dpydt_top(ix,iz) = (min(Csav(lb+1)*rav(lb+1),py(ix,lb+2,iz)) - py(ix,lb+1,iz))/(pdamp*dt)				! limit inflows to sonic
!     if (py(ix,lb+1,iz) > 0. .or. py(ix,lb,iz) > 0.) &			! for inflows ..
!       dpydt_top(ix,iz) = dpydt_top(ix,iz) &				! let the velocity
!         - htopi*py(ix,lb+1,iz)*abs(py(ix,lb+1,iz))/(0.5*(r(ix,lb,iz)+r(ix,lb+1,iz)))   ! tend to zero --- but so
!   end do
! end do

  do iz = izs, ize
    drdt(:,lb,iz) = drdt_top(:,iz)
    drdt(:,1:lb-1,iz) = 0.
    dpxdt(:,lb,iz) = dpxdt_top(:,iz)
    dpydt(:,lb+1,iz) = dpydt_top(:,iz)
    dpzdt(:,lb,iz) = dpzdt_top(:,iz)
    dedt(:,lb,iz) = dedt_top(:,iz)
    dedt(:,1:lb-1,iz) = 0.
    if (do_mhd) then
      dBxdt(:,lb,iz) = dBxdt_top(:,iz)
!     dBydt(:,lb+1,iz) = dBydt_top(:,iz)
      dBzdt(:,lb,iz) = dBzdt_top(:,iz)
    end if
  end do

!-----------------------------------------------------------------------
! Ghost zones
!-----------------------------------------------------------------------

   do iz=izs,ize
     do ix=1,mx
       drdt (ix,1:lb-1,iz) = 0.
!      dpxdt(ix,1:lb-1,iz) = 0.
!      dpydt(ix,1:lb  ,iz) = 0.
!      dpzdt(ix,1:lb-1,iz) = 0.
       dedt (ix,1:lb-1,iz) = 0.
       if (do_mhd) then
         dbxdt(ix,1:2,iz)=0.
         dbydt(ix,1:2,iz)=0.
         dbzdt(ix,1:2,iz)=0.
       endif
     end do
   end do

  end if 

END


!-----------------------------------------------------------------------
SUBROUTINE ddt_boundary_bot (r,px,py,pz,e,Bx,By,Bz, &
                             drdt,dpxdt,dpydt,dpzdt, &
                             dedt,dBxdt,dBydt,dBzdt, &
                             Ux,Uy,Uz,lnr,p,ee,Cs)
  USE params
  USE arrays, ONLY: scr1,scr2,scr3,scr4,scr5,scr6,eeav
  USE forcing
  USE eos
  USE boundary
  USE timeintegration, ONLY: alpha
  implicit none
  real, dimension(mx,my,mz):: r,px,py,pz,e,Bx,By,Bz, &
                              drdt,dpxdt,dpydt,dpzdt, &
                              dedt,dBxdt,dBydt,dBzdt, &
                              Ux,Uy,Uz,lnr,p,ee,Cs
                              
  real ::                     trelaxb, htopi, nuS1, nuS2
  real ::                     k, csq, caq, vaq, vfq, vsq, dd, f, &
                              vfast, vslow, Bn, Bt, amp1, amp2, amp3
  integer ix,iy,iz
  logical omp_in_parallel

  character(len=mid):: id='$Id: boundary_charbc.f90,v 1.13 2009/06/26 03:11:00 bob Exp $'
  call print_id (id)

!----------------------------------------------------------------------
  if(mpi_y == mpi_ny - 1) then
!-----------------------------------------------------------------------
! Bottom boundary

  if(isubstep == 1) then
    do iz = izs, ize
      drdt_bot(:,iz) = 0.0
      dpxdt_bot(:,iz) = 0.0
      dpydt_bot(:,iz) = 0.0
      dpzdt_bot(:,iz) = 0.0
      dedt_bot(:,iz) = 0.0
      dBxdt_bot(:,iz) = 0.0
      dBydt_bot(:,iz) = 0.0
      dBzdt_bot(:,iz) = 0.0
    end do
  else
    do iz = izs, ize
      drdt_bot(:,iz) = alpha(isubstep) * drdt_bot(:,iz)
      dpxdt_bot(:,iz) = alpha(isubstep) * dpxdt_bot(:,iz)
      dpydt_bot(:,iz) = alpha(isubstep) * dpydt_bot(:,iz)
      dpzdt_bot(:,iz) = alpha(isubstep) * dpzdt_bot(:,iz)
      dedt_bot(:,iz) = alpha(isubstep) * dedt_bot(:,iz)
      dBxdt_bot(:,iz) = alpha(isubstep) * dBxdt_bot(:,iz)
      dBydt_bot(:,iz) = alpha(isubstep) * dBydt_bot(:,iz)
      dBzdt_bot(:,iz) = alpha(isubstep) * dBzdt_bot(:,iz)
   end do
  end if

  trelaxb = pdamp / max(1.0 * (maxval(abs(Uyc(:,ub,:))) / dx + 1.0e-30) , &
            1.0 * maxval(r(:,ub,:) * Cs(:,ub,:) * gy / p(:,ub,:)) )

  do iz = izs, ize
    delta_p(:,iz) = 0.38 * (e(:,ub,iz) / ebot - 1.0)
!   delta_p(:,iz) = 5.00 * (e(:,ub,iz) / ebot - 1.0)
  end do

!  delta_p = 0.0
 
!-----------------------------------------------------------------------
! Horizontal derivatives
!-----------------------------------------------------------------------

  call ddxdn32_set(p, ub, dpdx)
  call ddzdn32_set(p, ub, dpdz)

  call ddxup32_set(px, ub, divh_ph)
  call ddzup32_add(pz, ub, divh_ph)

  call ddxup32_set(Ux, ub, divh_uh)
  call ddzup32_add(Uz, ub, divh_uh)

  call xdn32_set(lnr, ub, scra)
  call barrier_omp ('ddt22')
  call zdn22_set(scra, scrb)

  call zdn32_set(Ux, ub, scrc)
  call xdn32_set(Uz, ub, scrd)

  call barrier_omp ('ddt23')
  do iz = izs, ize
    scra(:,iz) = exp(scrb(:,iz)) * scrc(:,iz) * scrd(:,iz)
    scre(:,iz) = r(:,ub,iz) * Uxc(:,ub,iz)**2 + p(:,ub,iz)
    scrf(:,iz) = r(:,ub,iz) * Uzc(:,ub,iz)**2 + p(:,ub,iz)
  end do

  call barrier_omp ('ddt24')
  call ddxdn22_set(scre, divh_pxuh)
  call ddzup22_add(scra, divh_pxuh)

  call ddzdn22_set(scrf, divh_pzuh)
  call barrier_omp ('ddt25')
  call ddxup22_add(scra, divh_pzuh)

  call zdn32_set(ee, ub, scrb)
  call xdn32_set(ee, ub, scra)

  do iz = izs, ize
    scra(:,iz) = scra(:,iz) * px(:,ub,iz)
    scrb(:,iz) = scrb(:,iz) * pz(:,ub,iz)
  end do

  call barrier_omp ('ddt26')
  call ddxup22_set(scra, divh_euh)
  call ddzup22_add(scrb, divh_euh)

  call ydn_set(lnr, scr1)
  call xdn32_set(scr1, ub, scra)

  call zdn_set(lnr, scr4)
  call barrier_omp ('ddt27')
  call ydn_set(scr4, scr3)

  call ydn_set(Ux, scr1)
  call xdn32_set(Uy, ub, scrc)

  call ydn_set(Uz, scr2)
  call zdn32_set(Uy, ub, scrd)

  do iz=izs, ize
    scre(:,iz) = exp(scra(:,iz)) * scr1(:,ub,iz) * scrc(:,iz)
    scrf(:,iz) = exp(scr3(:,ub,iz)) * scr2(:,ub,iz) * scrd(:,iz)
  end do

  call barrier_omp ('ddt28')
  call ddxup22_set(scre, divh_pyuh)
  call ddzup22_add(scrf, divh_pyuh)
  call barrier_omp ('ddt29')

!--------------------------------------------------------------------------
! Lorentz force and terms for induction equation
!--------------------------------------------------------------------------

  call xdn_set(By, scr1)
  call zdn_set(By, scr2)

  call ddzdn32_set(Bx, ub, scra)
  call ddxdn32_sub(Bz, ub, scra)

  call ddxdn_set(By, scr3)
  call ddzdn_set(By, scr4)

  call xdn32_set(Bz, ub, scrb)
  call zdn32_set(Bx, ub, scrc)

  do iz=izs, ize
    scre(:,iz) = scra(:,iz) * scrb(:,iz)
    scr5(:,:,iz) = -scr3(:,:,iz) * scr1(:,:,iz)
  end do

  call yup_set(scr5, scr6)
!$omp barrier
  call zup22_set(scre, scrf)

  do iz=izs, ize
    divh_BxBh(:,iz) = scrf(:,iz) + scr6(:,ub,iz)
  end do

!$omp barrier
  do iz=izs, ize
    scre(:,iz) = -scra(:,iz) * scrc(:,iz)
    scr5(:,:,iz) = -scr4(:,:,iz) * scr2(:,:,iz)
  end do

  call yup_set(scr5, scr6)
  call xup22_set(scre, scrf)

  do iz=izs, ize
    divh_BzBh(:,iz) = scrf(:,iz) + scr6(:,ub,iz)
  end do

  call ydn_set(Bx, scr5)
  call ydn_set(Bz, scr6)

  do iz=izs, ize
    scre(:,iz) = scr3(:,ub,iz) * scr5(:,ub,iz)
    scrf(:,iz) = scr4(:,ub,iz) * scr6(:,ub,iz)
  end do

  call xup22_set(scre, divh_ByBh)
!$omp barrier
  call zup22_add(scrf, divh_ByBh)


  call zdn32_set(Ux, ub, scra)
  call xdn32_set(Uz, ub, scrd)

  do iz=izs, ize
    scre(:,iz) = scrd(:,iz) * scrc(:,iz) - scra(:,iz) * scrb(:,iz)
  end do

!$omp barrier
  call ddzup22_set(scre, divh_Bxuh)
  call ddxup22_set(scre, divh_Bzuh)

  call xdn32_set(Uy, ub, scra)
  call zdn32_set(Uy, ub, scrd)

  call ydn_set(Ux, scr3)
  call ydn_set(Uz, scr4)

!$omp barrier
  do iz=izs, ize
    scre(:,iz) = scrd(:,iz) * scr6(:,ub,iz) - scr4(:,ub,iz) * scr2(:,ub,iz)
    scrf(:,iz) = scr3(:,ub,iz) * scr1(:,ub,iz) - scra(:,iz) * scr5(:,ub,iz)
  end do

!$omp barrier
  call ddzup22_set(scre, divh_Byuh)
  call ddxup22_sub(scrf, divh_Byuh)
!$omp barrier

!-----------------------------------------------------------------------
! Apply boundary conditions on ingoing characteristics
! Assume always subsonic, not necessarily sub-Alfvenic
!-----------------------------------------------------------------------

  call ydn_set(Uxc, scr1)
  call ydn_set(Uzc, scr2)

  do iz = izs, ize
    scr3(:,:,iz) = beta_x(:,:,iz) * scr1(:,:,iz) + beta_z(:,:,iz) * scr2(:,:,iz)
    scr4(:,:,iz) = beta_z(:,:,iz) * scr1(:,:,iz) - beta_x(:,:,iz) * scr2(:,:,iz)
  end do

  call ydn_set(Bxc, scr1)
  call ydn_set(Bzc, scr2)

  do iz = izs, ize
    scr5(:,:,iz) = beta_x(:,:,iz) * (scr1(:,:,iz) - bx0) + beta_z(:,:,iz) * (scr2(:,:,iz) - bz0)
    scr6(:,:,iz) = beta_z(:,:,iz) * (scr1(:,:,iz) - bx0) - beta_x(:,:,iz) * (scr2(:,:,iz) - bz0)
  end do

  kyp=2.0*pi*aky/sy
  kzp=2.0*pi*akz/sz
  k=sqrt(kyp**2 + kzp**2)
  bn = (kyp * by0 + kzp * bz0) / k
  f = sign(1.0, bn)
  csq = 1.0
  vaq = bn**2
  caq = (bx0**2 + by0**2 + bz0**2)
  dd = sqrt(max((csq + caq)**2 - 4.0 * vaq * csq, 0.0))
  vfq = 0.5 * (csq + caq + dd)
  vsq = 0.5 * max(csq + caq - dd, 0.0)
  vfast = sign(sqrt(vfq), k)
  vslow = sign(sqrt(vsq), k)

  if(test == 'fast') then
    omegap = vfast * k
    amp1 = ampyz
    amp2 = 0.0
    amp3 = 0.0
  else if(test == 'alfven') then
    omegap = f * bn * k
    amp1 = 0.0
    amp2 = ampyz
    amp3 = 0.0
  else if(test == 'slow') then
    omegap = vslow * k
    amp1 = 0.0
    amp2 = 0.0
    amp3 = ampyz
  else
    amp1 = 0.0
    amp2 = 0.0
    amp3 = 0.0
  end if

if(nonreflecting .or. test .ne. '') then

  do iz = izs, ize
    do iy = my/2 + 1, my
      do ix = 1,mx

        if(Uy(ix,ub,iz) .lt. vfdn(ix,ub,iz)) &
          d1(ix,iy,iz) = -0.5 * alpha_f(ix,iy,iz) * rdn(ix,iy,iz) * gy * &
                         vfdn(ix,iy,iz) + amp1 * sin(kyp * ym(iy) + kzp * zm(iz) - omegap * t) 

        if(Uy(ix,ub,iz) .lt. vadn(ix,ub,iz)) &
          d2(ix,iy,iz) = amp2 * sin(kyp * ym(iy) + kzp * zm(iz) - omegap * t) 

        if(Uy(ix,ub,iz) .lt. vsdn(ix,ub,iz)) &
          d3(ix,iy,iz) = -0.5 * alpha_s(ix,iy,iz) * rdn(ix,iy,iz) * gy * &
                         vsdn(ix,iy,iz) + amp3 * sin(kyp * ym(iy) + kzp * zm(iz) - omegap * t) 

        if(Uy(ix,ub,iz) .lt. 0.0 .or. Uy(ix,ub-1,iz) .lt. 0.0) &
          d4(ix,iy,iz) = 0.0

        if(Uy(ix,ub,iz) .lt. 0.0) &
          d5(ix,iy,iz) = 0.0

        if(Uy(ix,ub,iz) .lt. -vsdn(ix,ub,iz)) &
          d6(ix,iy,iz) = 0.5 * alpha_s(ix,iy,iz) * rdn(ix,iy,iz) * gy * &
                         vsdn(ix,iy,iz) 

        if(Uy(ix,ub,iz) .lt. -vadn(ix,ub,iz)) &
          d7(ix,iy,iz) = 0.0 

        if(Uy(ix,ub,iz) .lt. -vfdn(ix,ub,iz)) &
          d8(ix,iy,iz) = 0.5 * alpha_f(ix,iy,iz) * rdn(ix,iy,iz) * gy * &
                         vfdn(ix,iy,iz)

      end do
    end do
  end do

else

  do iz = izs, ize
    do iy = my/2 + 1, my
      do ix = 1,mx

        if(Uy(ix,ub,iz) .lt. vfdn(ix,ub,iz)) &
          d1(ix,iy,iz) = -0.5 * alpha_f(ix,iy,iz) * rdn(ix,iy,iz) * gy * &
                         (vfdn(ix,iy,iz) - csdn(ix,iy,iz) * delta_p(ix,iz)) + &
                         0.5 * rdn(ix,iy,iz) * alpha_s(ix,iy,iz) * vsdn(ix,iy,iz) * &
                         sign(1.0, Byc(ix,ub,iz)) * scr3(ix,iy,iz) / trelaxb + &
                         0.5 * sqrt(rdn(ix,iy,iz)) * alpha_s(ix,iy,iz) * Csdn(ix,iy,iz) * &
                         scr5(ix,iy,iz) / trelaxb

        if(Uy(ix,ub,iz) .lt. vadn(ix,ub,iz)) &
          d2(ix,iy,iz) = 0.5 * sign(1.0, Byc(ix,ub,iz)) * scr4(ix,iy,iz) / trelaxb + &
                         0.5 * scr6(ix,iy,iz) / (sqrt(rdn(ix,iy,iz)) * trelaxb)

        if(Uy(ix,ub,iz) .lt. vsdn(ix,ub,iz)) &
          d3(ix,iy,iz) = -0.5 * alpha_s(ix,iy,iz) * rdn(ix,iy,iz) * gy * &
                         (vsdn(ix,iy,iz) - csdn(ix,iy,iz) * delta_p(ix,iz)) - &
                         0.5 * rdn(ix,iy,iz) * alpha_f(ix,iy,iz) * vfdn(ix,iy,iz) * &
                         sign(1.0, Byc(ix,ub,iz)) * scr3(ix,iy,iz) / trelaxb - &
                         0.5 * sqrt(rdn(ix,iy,iz)) * alpha_f(ix,iy,iz) * Csdn(ix,iy,iz) * &
                         scr5(ix,iy,iz) / trelaxb

        if(Uy(ix,ub,iz) .lt. 0.0) &
          d4(ix,iy,iz) = - Cs(ix,iy,iz) * (p(ix,iy,iz)/r(ix,iy,iz)) * dlnPdE_r(ix,iy,iz) * &
                          ( (e(ix,iy,iz)+p(ix,iy,iz)) * (rbot-r(ix,iy,iz))/r(ix,iy,iz) - &
                          (ebot-e(ix,iy,iz)) ) * (rav(iy)*gy/pav(iy))
!           d4(ix,iy,iz) = 0.0

      if(Uy(ix,ub,iz) .lt. 0.0) &
         d5(ix,iy,iz) = 0.0

        if(Uy(ix,ub,iz) .lt. -vsdn(ix,ub,iz)) &
          d6(ix,iy,iz) = 0.5 * alpha_s(ix,iy,iz) * rdn(ix,iy,iz) * gy * &
                         (vsdn(ix,iy,iz) + csdn(ix,iy,iz) * delta_p(ix,iz)) + &
                         0.5 * rdn(ix,iy,iz) * alpha_f(ix,iy,iz) * vfdn(ix,iy,iz) * &
                         sign(1.0, Byc(ix,ub,iz)) * scr3(ix,iy,iz) / trelaxb - &
                         0.5 * sqrt(rdn(ix,iy,iz)) * alpha_f(ix,iy,iz) * Csdn(ix,iy,iz) * &
                         scr5(ix,iy,iz) / trelaxb

        if(Uy(ix,ub,iz) .lt. -vadn(ix,ub,iz)) &
          d7(ix,iy,iz) = -0.5 * sign(1.0, Byc(ix,ub,iz)) * scr4(ix,iy,iz) / trelaxb + &
                         0.5 * scr6(ix,iy,iz) / (sqrt(rdn(ix,iy,iz)) * trelaxb)

!        if(Uy(ix,ub,iz) .lt. -vfdn(ix,ub,iz)) &
!          d8(ix,iy,iz) = 0.5 * alpha_f(ix,iy,iz) * rdn(ix,iy,iz) * gy * &
!                         (vfdn(ix,iy,iz) + csdn(ix,iy,iz) * delta_p(ix,iz)) - &
!                        0.5 * rdn(ix,iy,iz) * alpha_s(ix,iy,iz) * vsdn(ix,iy,iz) * &
!                        sign(1.0, Byc(ix,ub,iz)) * scr3(ix,iy,iz) / trelaxb + &
!                        0.5 * sqrt(rdn(ix,iy,iz)) * alpha_s(ix,iy,iz) * Csdn(ix,iy,iz) * &
!                        scr5(ix,iy,iz) / trelaxb

      end do
    end do
  end do


end if

!-----------------------------------------------------------------------
! Time derivatives on the boundary
!-----------------------------------------------------------------------

   do iz = izs, ize
     scr1(:,:,iz) = alpha_f(:,:,iz) * (d1(:,:,iz) + d8(:,:,iz)) + &
                    alpha_s(:,:,iz) * (d3(:,:,iz) + d6(:,:,iz))
     scr4(:,:,iz) = alpha_f(:,:,iz) * vfdn(:,:,iz) * (d6(:,:,iz) - d3(:,:,iz)) + &
                    alpha_s(:,:,iz) * vsdn(:,:,iz) * (d1(:,:,iz) - d8(:,:,iz))
   end do

  call ydn_set(d4, scr2)
  call yup_set(scr1, scr3)

  do iz = izs, ize

    scra(:,iz) = (d4(:,ub,iz) + scr3(:,ub,iz)) / Cs(:,ub,iz)**2
    scrb(:,iz) = (scr2(:,ub,iz) + scr1(:,ub,iz)) / Csdn(:,ub,iz)**2

  end do

  do iz = izs, ize

    drdt_bot(:,iz) = drdt_bot(:,iz) - scra(:,iz) - divh_ph(:,iz)

    dedt_bot(:,iz) = dedt_bot(:,iz) + d4(:,ub,iz) * r(:,ub,iz) / (p(:,ub,iz) * dlnPdE_r(:,ub,iz)) - &
                     (e(:,ub,iz) + p(:,ub,iz)) * scra(:,iz) / r(:,ub,iz) - &
                     divh_euh(:,iz) - p(:,ub,iz) * divh_uh(:,iz)

    dpydt_bot(:,iz) = dpydt_bot(:,iz) - Uy(:,ub,iz) * scrb(:,iz) + &
                      (alpha_f(:,ub,iz) * vfdn(:,ub,iz) * (d1(:,ub,iz) - d8(:,ub,iz)) + &
                       alpha_s(:,ub,iz) * vsdn(:,ub,iz) * (d3(:,ub,iz) - d6(:,ub,iz))) / Csdn(:,ub,iz)**2 + &
                      rdn(:,ub,iz) * gy - divh_pyuh(:,iz) + divh_ByBh(:,iz)

  end do

  do iz = izs, ize
    do iy = 1, my

      scr1(:,iy,iz) = (-scr4(:,iy,iz) * beta_x(:,iy,iz) / Csdn(:,iy,iz)**2 + &
                     rdn(:,iy,iz) * beta_z(:,iy,iz) * (d7(:,iy,iz) - d2(:,iy,iz))) &
                     * sign(1.0, Byc(:,ub,iz))
      scr2(:,iy,iz) = (-scr4(:,iy,iz) * beta_z(:,iy,iz) / Csdn(:,iy,iz)**2 - &
                     rdn(:,iy,iz) * beta_x(:,iy,iz) * (d7(:,iy,iz) - d2(:,iy,iz))) &
                     * sign(1.0, Byc(:,ub,iz))

    end do
  end do

  call yup_set(scr1, scr5)
  call yup_set(scr2, scr6)

  do iz = izs, ize

    scre(:,iz) = -Uxc(:,ub,iz) * scra(:,iz) + scr5(:,ub,iz)
    scrf(:,iz) = -Uzc(:,ub,iz) * scra(:,iz) + scr6(:,ub,iz)

  end do

  call barrier_omp ('ddt11')
  call xdn22_set(scre, scrc)
  call zdn22_set(scrf, scrd)

  do iz = izs, ize

    dpxdt_bot(:,iz) = dpxdt_bot(:,iz) + scrc(:,iz) - divh_pxuh(:,iz) + divh_BxBh(:,iz)

    dpzdt_bot(:,iz) = dpzdt_bot(:,iz) + scrd(:,iz) - divh_pzuh(:,iz) + divh_BzBh(:,iz)

  end do

  do iz = izs, ize
    scr3(:,:,iz) = alpha_s(:,:,iz) * (d1(:,:,iz) + d8(:,:,iz)) - &
                   alpha_f(:,:,iz) * (d3(:,:,iz) + d6(:,:,iz))
  end do

  do iz = izs, ize
    scr1(:,:,iz) = -beta_x(:,:,iz) * scr3(:,:,iz) / (sqrt(rdn(:,:,iz)) * Csdn(:,:,iz)) - &
                 sqrt(rdn(:,:,iz)) * beta_z(:,:,iz) * (d2(:,:,iz) + d7(:,:,iz))
    scr2(:,:,iz) = -beta_z(:,:,iz) * scr3(:,:,iz) / (sqrt(rdn(:,:,iz)) * Csdn(:,:,iz)) + &
                 sqrt(rdn(:,:,iz)) * beta_x(:,:,iz) * (d2(:,:,iz) + d7(:,:,iz))
  end do  
   

  call yup_set(scr1, scr5)
  call yup_set(scr2, scr6)

!$omp barrier
  call xdn32_set(scr5, ub, scrc)
  call zdn32_set(scr6, ub, scrd)

  do iz = izs, ize

    dBydt_bot(:,iz) = dBydt_bot(:,iz) - d5(:,ub,iz) + divh_Byuh(:,iz)

    dBxdt_bot(:,iz) = dBxdt_bot(:,iz) + scrc(:,iz) - divh_Bxuh(:,iz)

    dBzdt_bot(:,iz) = dBzdt_bot(:,iz) + scrd(:,iz) + divh_Bzuh(:,iz)

  end do

!-----------------------------------------------------------------------
! Diffusion - horizontal directions
!-----------------------------------------------------------------------
! S_ii = ddiup(U_i)
! S_ij = 0.5*(ddidn(U_j)+ddjdn(U_i))
! T_ii = 2*d_i*(nu+d_i*nud)*S_ii
! nu_ij = exp(idn(jdn(log(nu+.5*(d_i+d_j)*nud))))
! T_ij = (d_i+d_j)*nu_ij*S_ij
! dedt = dedt + sum_i [(T_ii*S_ii)]
! dedt = dedt + sum_ij [(d_i+d_j)*nu*iup(jup(S_ij))^2]
! dp_idt = dp_idt + ddjdn(T_ii) + sum_j [ddjup(T_ij)]
!-----------------------------------------------------------------------

  nuS1 = 3.*nuS/(1.+2.*nuS)
  nuS2 = (1.-nuS)/(1.+2.*nuS)

  call barrier_omp ('ddt31')

  call ddxup_set(Ux, scr4)
  call ddyup_add(Uy, scr4)
  call ddzup_add(Uz, scr4)

  call ddxup32_set(Ux, ub, scra)

  do iz = izs, ize
    do ix = 1, mx
!      scrb(ix,iz) = 2.0 * dxm(ix) * (wk06(ix,ub,iz) + dxm(ix) * wk19(ix,ub,iz)) * &
!                    (nuS1 * scra(ix,iz) + nuS2 * scr4(ix,ub,iz))
      scrb(ix,iz) = 2.0 * dxm(ix) * (nu_bot(ix,2,iz) + dxm(ix) * nud_bot(ix,2,iz)) * &
                    (nuS1 * scra(ix,iz) + nuS2 * scr4(ix,ub,iz))
   end do
  end do

  call ddxdn22_set(scrb, scrc)

  do iz = izs, ize
    dpxdt_bot(:,iz) = dpxdt_bot(:,iz) + scrc(:,iz)
    dedt_bot(:,iz) = dedt_bot(:,iz) + scra(:,iz) * scrb(:,iz)
  end do
    
  call ddyup_set(Uy, scr1)

!  do iz = izs, ize
!    do iy = 1, my
!      scr2(:,iy,iz) = 2.0 * dym(iy) * (wk06(:,iy,iz) + dym(iy) * wk19(:,iy,iz)) * &
!                      (nuS1 * scr1(:,iy,iz) + nuS2 * scr4(:,iy,iz))
!    end do
!  end do

  do iz = izs, ize
    scr2(:,ub-1,iz) = 2.0 * dym(ub-1) * (nu_bot(:,1,iz) + dym(ub-1) * nud_bot(:,1,iz)) * &
                    (nuS1 * scr1(:,ub-1,iz) + nuS2 * scr4(:,ub-1,iz))
    scr2(:,ub,iz) = 2.0 * dym(ub) * (nu_bot(:,2,iz) + dym(ub) * nud_bot(:,2,iz)) * &
                    (nuS1 * scr1(:,ub,iz) + nuS2 * scr4(:,ub,iz))
  end do

!  call ddydn_set(scr2, scr3)

  do iz = izs, ize
    scr3(:,ub,iz) = (scr2(:,ub,iz) - scr2(:,ub-1,iz)) / dym(ub)
  end do

  do iz = izs, ize
    dpydt_bot(:,iz) = dpydt_bot(:,iz) + scr3(:,ub,iz)
    dedt_bot(:,iz) = dedt_bot(:,iz) + scr1(:,ub,iz) * scr2(:,ub,iz)
  end do

  call ddzup32_set(Uz, ub, scra)

  do iz = izs, ize
!      scrb(:,iz) = 2.0 * dzm(iz) * (wk06(:,ub,iz) + dzm(iz) * wk19(:,ub,iz)) * &
!                   (nuS1 * scra(:,iz) + nuS2 * scr4(:,ub,iz))
      scrb(:,iz) = 2.0 * dzm(iz) * (nu_bot(:,2,iz) + dzm(iz) * nud_bot(:,2,iz)) * &
                   (nuS1 * scra(:,iz) + nuS2 * scr4(:,ub,iz))
 end do

  call barrier_omp ('ddt32')
  call ddzdn22_set(scrb, scrc)
  call barrier_omp ('ddt33')

  do iz = izs, ize
    dpzdt_bot(:,iz) = dpzdt_bot(:,iz) + scrc(:,iz)
    dedt_bot(:,iz) = dedt_bot(:,iz) + scra(:,iz) * scrb(:,iz)
  end do

  call ddxdn32_set(Uz, ub, scra)
  call ddzdn32_add(Ux, ub, scra)

  do iz = izs, ize
    do ix =1, mx
!        scrb(ix,iz) = alog(wk06(ix,ub,iz) + 0.5 * (dxm(ix) + dzm(iz)) * wk19(ix,ub,iz))
!        scrb(ix,iz) = alog(wk06(ix,ub,iz))
        scrb(ix,iz) = alog(nu_bot(ix,2,iz))
    end do
  end do

  call xdn22_set(scrb, scrc)
  call barrier_omp ('ddt34')
  call zdn22_set(scrc, scrd)

  do iz = izs, ize
    do ix = 1, mx
      scrd(ix,iz) = 0.5 * nuS1 * (dxm(ix) + dzm(iz)) * exp(scrd(ix,iz)) * scra(ix,iz)
    end do
  end do

  call barrier_omp ('ddt35')
  call ddxup22_set(scrd, scre)
  call ddzup22_set(scrd, scrf)

  do iz = izs, ize
    dpxdt_bot(:,iz) = dpxdt_bot(:,iz) + scrf(:,iz)
    dpzdt_bot(:,iz) = dpzdt_bot(:,iz) + scre(:,iz)
  end do

  call xup22_set(scra, scrc)
  call barrier_omp ('ddt36')
  call zup22_set(scrc, scrd)

  do iz = izs, ize
    scrb(:,iz) = 0.25 * scrd(:,iz)**2
  end do

  do iz = izs, ize
    do ix = 1, mx
!      dedt_bot(ix,iz) = dedt_bot(ix,iz) + nuS1 * (dxm(ix) + dzm(iz)) * wk06(ix,ub,iz) * scrb(ix,iz)
      dedt_bot(ix,iz) = dedt_bot(ix,iz) + nuS1 * (dxm(ix) + dzm(iz)) * nu_bot(ix,2,iz) * scrb(ix,iz)
    end do
  end do

  call ddxdn_set(Uy, scr1)

!  do iz = izs, ize
!    do ix = 1, mx
!      do iy = 1, my
!        scr2(ix,iy,iz) = alog(wk06(ix,iy,iz) + 0.5 * (dxm(ix) + dym(iy)) * wk19(ix,iy,iz))
!        scr2(ix,iy,iz) = alog(wk06(ix,iy,iz))
!      end do
!    end do
!  end do

!  call ydn_set(scr2, scr3)

  do iz = izs, ize
    do ix = 1, mx
      scr3(ix,ub,iz) = 0.5 * (alog(nu_bot(ix,1,iz)) + alog(nu_bot(ix,2,iz)))
    end do
  end do

  call xdn32_set(scr3, ub, scra)

  do iz = izs, ize
    do ix = 1, mx
      scra(ix,iz) = 0.5 * nuS1 * (dxm(ix) + dym(ub)) * exp(scra(ix,iz)) * scr1(ix,ub,iz)
    end do
  end do

  call ddxup22_set(scra, scrb)

  do iz = izs, ize
    dpydt_bot(:,iz) = dpydt_bot(:,iz) + scrb(:,iz)
  end do

  call yup_set(scr1, scr3)
  call xup32_set(scr3, ub, scra)

  do iz = izs, ize
    scrb(:,iz) = 0.25 * scra(:,iz)**2
  end do

  do iz = izs, ize
    do ix = 1, mx
!      dedt_bot(ix,iz) = dedt_bot(ix,iz) + nuS1 * (dxm(ix) + dym(ub)) * wk06(ix,ub,iz) * scrb(ix,iz)
      dedt_bot(ix,iz) = dedt_bot(ix,iz) + nuS1 * (dxm(ix) + dym(ub)) * nu_bot(ix,2,iz) * scrb(ix,iz)
    end do
  end do

  call ddzdn_set(Uy, scr1)

!  do iz = izs, ize
!    do iy = 1, my
!      scr2(:,iy,iz) = alog(wk06(:,iy,iz) + 0.5 * (dym(iy) + dzm(iz)) * wk19(:,iy,iz))
!      scr2(:,iy,iz) = alog(wk06(:,iy,iz))
!    end do
!  end do

!  call ydn_set(scr2, scr3)

  do iz = izs, ize
    scr3(:,ub,iz) = 0.5 * (alog(nu_bot(:,1,iz)) + alog(nu_bot(:,2,iz)))
  end do

  call barrier_omp ('ddt37')
  call zdn32_set(scr3, ub, scra)

  do iz = izs, ize
    scra(:,iz) = 0.5 * nuS2 * (dym(ub) + dzm(iz)) * exp(scra(:,iz)) * scr1(:,ub,iz)
  end do

  call barrier_omp ('ddt38')
  call ddzup22_set(scra, scrb)

  do iz = izs, ize
    dpydt_bot(:,iz) = dpydt_bot(:,iz) + scrb(:,iz)
  end do

  call yup_set(scr1, scr3)
  call barrier_omp ('ddt39')
  call zup32_set(scr3, ub, scra)

  do iz = izs, ize
    scrb(:,iz) = 0.25 * scra(:,iz)**2
  end do

  do iz = izs, ize
!    dedt_bot(:,iz) = dedt_bot(:,iz) + nuS1 * (dym(ub) + dzm(iz)) * wk06(:,ub,iz) * scrb(:,iz)
    dedt_bot(:,iz) = dedt_bot(:,iz) + nuS1 * (dym(ub) + dzm(iz)) * nu_bot(:,ub,iz) * scrb(:,iz)
  end do

  do iz = izs, ize
    drdt(:,ub,iz) = drdt_bot(:,iz)
    dpxdt(:,ub,iz) = dpxdt_bot(:,iz)
    dpydt(:,ub,iz) = dpydt_bot(:,iz)
    dpzdt(:,ub,iz) = dpzdt_bot(:,iz)
    dedt(:,ub,iz) = dedt_bot(:,iz)
    dBxdt(:,ub,iz) = dBxdt_bot(:,iz)
!    dBydt(:,ub,iz) = dBydt_bot(:,iz)
    dBzdt(:,ub,iz) = dBzdt_bot(:,iz)
 end do

!-----------------------------------------------------------------------
! damp vertical waves from bottom boundary
!-----------------------------------------------------------------------
!  if (pdamp .gt. 0. .and. lb > 1) then
!    call haverage_subr (py,pyav)				! mass flux
!    do iz = izs, ize
!     do iy=1,my
!       dpydt(:,iy,iz)=dpydt(:,iy,iz)-pyav(iy)/(pdamp*trelaxt)
!     end do
!      do iy=lb,min(2*lb-1,ub)
!        dpxdt(:,iy,iz)=dpxdt(:,iy,iz)-px(:,iy,iz)*real(2*lb-iy)/(pdamp*trelaxt)
!        dpydt(:,iy,iz)=dpydt(:,iy,iz)-py(:,iy,iz)*real(2*lb-iy)/(pdamp*trelaxt)
!        dpzdt(:,iy,iz)=dpzdt(:,iy,iz)-pz(:,iy,iz)*real(2*lb-iy)/(pdamp*trelaxt)
!      end do
!    end do
!  end if

  end if
!-----------------------------------------------------------------------


END
