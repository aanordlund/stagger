@stagger_6th

; $Id: init_bm1.pro,v 1.2 2012/02/22 21:27:12 trier Exp $
;------------------------------------------------------------------------------
;
; Case 1: Initial pressure equilibrium
;
;------------------------------------------------------------------------------

L=1.
ds=L/12.
init_derivs,dx=ds,dy=ds,dz=ds

default, Lx, 240.
default, Ly, 60.

nx=long(Lx/ds+0.5)
ny=long(Ly/ds+0.5)
help,nx,ny

x=ds*findgen(nx)
y=ds*findgen(ny)-Ly/2.
xx=rebin(reform(x,nx,1),nx,ny)
yy=rebin(reform(y,1,ny),nx,ny)

B0=1.0
pb=B0^2/20.
e0=0.1

Bx=B0*tanh(yy/L)
By=fltarr(nx,ny)
Bz=fltarr(nx,ny)

pgas=float(pb+pb*((1D0/cosh(yy/L))^2))

gamma=5./3.
rho=pgas/((gamma-1.)*e0)

e=replicate(e0,nx,ny)

px=fltarr(nx,ny)
py=fltarr(nx,ny)
pz=fltarr(nx,ny)

eps=0.1
Az=eps*B0*L*cos(2.*!pi*((xx-0.5*ds)-Lx/2.)/Lx)*cos(!pi*(yy-0.5*ds)/Ly)
lb=5
ub=ny-6
Bx[*,lb:ub,*]=Bx[*,lb:ub,*]+(ddyup(Az))[*,lb:ub,*]
By=By-ddxup(Az)

close,1
openw,1,'snapshot.dat'
writeu,1,rho
writeu,1,px
writeu,1,py
writeu,1,pz
writeu,1,e
writeu,1,Bx
writeu,1,By
writeu,1,Bz
close,1

END
