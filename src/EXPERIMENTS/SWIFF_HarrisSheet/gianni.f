      implicit real*8 (a-h,o-z)
      real*8 rhr(1),bxr(1)

      wspx=0.2
      wspy=0.3
      wspz=0.4
      rlargx=1.
      rlargy=1.
      rlargz=1.
      bxr=1.
      rhr=1.
      gm1=1.

      call double_harris(wspx,wspy,wspz,
     &        rlargx,rlargy,rlargz,
     &        bxr,rhr,gm1,
     &        bx_ini,by_ini,bz_ini,
     &        p_ini,u_ini,rho_ini)

      print*,
     &        bx_ini,by_ini,bz_ini,
     &        p_ini,u_ini,rho_ini

      end

      subroutine double_harris(wspx,wspy,wspz,
     &        rlargx,rlargy,rlargz,
     &        bxr,rhr,gm1,
     &        bx_ini,by_ini,bz_ini,
     &        p_ini,u_ini,rho_ini)
c
      implicit real*8 (a-h,o-z)
      real*8 rhr(1),bxr(1)

      print*, wspx,wspy,wspz,
     &        rlargx,rlargy,rlargz,
     &        bxr,rhr
c
      pi=4.*atan(1.)
      xcntr=rlargx/2.
      zcntr=rlargz/2.
      zcntr1=rlargz/4.
      zcntr2=rlargz*3./4.

      elle=1.d0

      bx_ini=bxr(1)*(-1d0+tanh((wspz-zcntr1)/elle))
      bx_ini=bx_ini+bxr(1)*tanh((zcntr2-wspz)/elle)
      by_ini=0d0
      bz_ini=0d0

      epsilon_x=1d-1*bxr(1)
      epsilon_gem=0d-1*bxr(1)
      fkx=2.*pi/rlargx
      fky=2.*pi/rlargy
      fkz=2.*pi/rlargz
c
c      GEM perturbation
c
      bz_ini=bz_ini+epsilon_gem*sin(fkx*(wspx-xcntr))
     &             *sin(fkz*(wspz-zcntr1))*fkx
      bx_ini=bx_ini+epsilon_gem*cos(fkx*(wspx-xcntr))
     &             *cos(fkz*(wspz-zcntr1))*fkz
c
c      x-point perturbation (first layer)
c
      bx_ini=bx_ini-epsilon_x*cos(fkx*(wspx-xcntr))
     &         *(sin(fkz*(wspz-zcntr1))+2*(wspz-zcntr1)*
     &           cos(fkz*(wspz-zcntr1)))*fkz
     &  *exp(-fkx*(wspx-xcntr)**2-fkz*(wspz-zcntr1)**2)
      bz_ini=bz_ini+epsilon_x*(sin(fkx*(wspx-xcntr))+
     &  2*(wspx-xcntr)*cos(fkx*(wspx-xcntr)))
     &  *cos(fkz*(wspz-zcntr1))*fkx
     &  *exp(-fkx*(wspx-xcntr)**2-fkz*(wspz-zcntr1)**2)
c
c      x-point perturbation (second layer)
c
      bx_ini=bx_ini+epsilon_x*cos(fkx*(wspx-xcntr))
     &         *(sin(fkz*(wspz-zcntr2))+2*(wspz-zcntr2)*
     &           cos(fkz*(wspz-zcntr2)))*fkz
     &  *exp(-fkx*(wspx-xcntr)**2-fkz*(wspz-zcntr2)**2)
      bz_ini=bz_ini-epsilon_x*(sin(fkx*(wspx-xcntr))+
     &  2*(wspx-xcntr)*cos(fkx*(wspx-xcntr)))
     &  *cos(fkz*(wspz-zcntr2))*fkx
     &  *exp(-fkx*(wspx-xcntr)**2-fkz*(wspz-zcntr2)**2)


      rho_ini=rhr(1)/10d0+rhr(1)/cosh((wspz-zcntr1)/elle)**2
      rho_ini=rho_ini+rhr(1)/cosh((wspz-zcntr2)/elle)**2

      p_ini = gm1 * rho_ini * bxr(1)**2/2.0

      u_ini=0.d0

      end subroutine double_harris
