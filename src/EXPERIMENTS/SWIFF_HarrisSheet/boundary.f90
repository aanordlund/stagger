! $Id: boundary.f90,v 1.2 2011/12/14 20:20:22 aake Exp $

!************************************************************************
FUNCTION input_file()
  USE params
  implicit none
  character(len=mfile) input_file
  input_file = 'EXPERIMENTS/SWIFF_HarrisSheet/input.txt'
END

!-----------------------------------------------------------------------
SUBROUTINE init_boundary
  USE params
  implicit none
  character(len=mid):: id="$Id: boundary.f90,v 1.2 2011/12/14 20:20:22 aake Exp $"
!
  call print_id (id)
  lb = 1
  ub = my
  if (mpi_y==0) lb=6
  if (mpi_y==mpi_ny-1) ub=my-5
END

!-----------------------------------------------------------------------
SUBROUTINE read_boundary
  implicit none
END

!-----------------------------------------------------------------------
SUBROUTINE density_boundary (r,lnr,py,e)
  USE params
  implicit none
  real, dimension(mx,my,mz):: r,lnr,py,e
!
  call regularize(r)
  if (mpi_y==0) then
    lnr(:,1:lb,:)=alog(r(:,1:lb,:))
  end if
  if (mpi_y==mpi_ny-1) then
    lnr(:,ub:my,:)=alog(r(:,ub:my,:))
  endif
END

!-----------------------------------------------------------------------
!SUBROUTINE density_boundary_log (lnr,r)
!  USE params
!  implicit none
!  real, dimension(mx,my,mz):: lnr,r
!END

!-----------------------------------------------------------------------
SUBROUTINE energy_boundary (r,lnr,Ux,Uy,Uz,e,ee,p,Bx,By,Bz)
  USE params
  implicit none
  real, dimension(mx,my,mz):: r,lnr,Ux,Uy,Uz,e,ee,p,Bx,By,Bz
  call regularize(e)
END

!-----------------------------------------------------------------------
SUBROUTINE passive_boundary (r,Ux,Uy,Uz,d,dd)
  USE params
  implicit none
  real, dimension(mx,my,mz):: r,Ux,Uy,Uz,d,dd
  call regularize(d)
END

!-----------------------------------------------------------------------
SUBROUTINE velocity_boundary (r,xdnr,ydnr,zdnr,Ux,Uy,Uz,px,py,pz,e,Bx,By,Bz)
  USE params
  implicit none
  real, dimension(mx,my,mz):: r,xdnr,ydnr,zdnr,Ux,Uy,Uz,px,py,pz,e,Bx,By,Bz
!
  if (mpi_y==0) then
    Ux(:, 1:lb,:)=0.
    Uy(:, 1:lb,:)=0.
    Uz(:, 1:lb,:)=0.
    px(:, 1:lb,:)=0.
    py(:, 1:lb,:)=0.
    pz(:, 1:lb,:)=0.
  end if
  if (mpi_y==mpi_ny-1) then
    Ux(:,ub  :my,:)=0.
    Uy(:,ub  :my,:)=0.
    Uz(:,ub+1:my,:)=0.
    px(:,ub  :my,:)=0.
    py(:,ub  :my,:)=0.
    pz(:,ub+1:my,:)=0.
  endif
END

!-----------------------------------------------------------------------
SUBROUTINE viscosity_boundary (nu, scr)
  USE params
  implicit none
  real, dimension(mx,my,mz):: nu, scr
!
  call regularize(nu)
END

!-----------------------------------------------------------------------
SUBROUTINE mfield_boundary (Ux,Uy,Uz,Bx,By,Bz)
  USE params
  implicit none
  real, dimension(mx,my,mz):: Ux,Uy,Uz,Bx,By,Bz
END

!-----------------------------------------------------------------------
SUBROUTINE ecurrent_boundary (Ex, Ey, Ez, Jx, Jy, Jz)
  USE params
  implicit none
  real, dimension(mx,my,mz):: Ex, Ey, Ez, Jx, Jy, Jz
!
  if (mpi_y==0) then
    Jx(:, 1:lb,:)=0.
    Jy(:, 1:lb,:)=0.
    Jz(:, 1:lb,:)=0.
  end if
  if (mpi_y==mpi_ny-1) then
    Jx(:,ub+1:my,:)=0.
    Jy(:,ub+1:my,:)=0.
    Jz(:,ub  :my,:)=0.
  endif
END

!-----------------------------------------------------------------------
SUBROUTINE  efield_boundary (Ex, Ey, Ez, Bx_y, Bx_z, By_z, By_x, Bz_x, Bz_y, &
                             Rx, Ry, Rz, Ux_y, Ux_z, Uy_z, Uy_x, Uz_x, Uz_y, &
                             Ux, Uy, Uz)
  USE params
  implicit none
  real, dimension(mx,my,mz):: Ex, Ey, Ez, Bx_y, Bx_z, By_z, By_x, Bz_x, Bz_y, &
                              Rx, Ry, Rz, Ux_y, Ux_z, Uy_z, Uy_x, Uz_x, Uz_y, &
                              Ux, Uy, Uz
  if (mpi_y==0) then
    Ex(:, 1:lb,:)=0.
    Ey(:, 1:lb,:)=0.
    Ez(:, 1:lb,:)=0.
  end if
  if (mpi_y==mpi_ny-1) then
    Ex(:,ub+1:my,:)=0.
    Ey(:,ub+1:my,:)=0.
    Ez(:,ub  :my,:)=0.
  endif
END

!-----------------------------------------------------------------------
SUBROUTINE ddt_boundary (r,px,py,pz,e,p,Bx,By,Bz, &
                         drdt,dpxdt,dpydt,dpzdt, &
                         dedt,dBxdt,dBydt,dBzdt)
  USE params
  implicit none
  real, dimension(mx,my,mz):: r,px,py,pz,e,p,Bx,By,Bz, &
                              drdt,dpxdt,dpydt,dpzdt, &
                              dedt,dBxdt,dBydt,dBzdt
  if (mpi_y==0) then
    drdt (:,1:lb,:)=0.
    dpxdt(:,1:lb,:)=0.
    dpydt(:,1:lb,:)=0.
    dpzdt(:,1:lb,:)=0.
    dedt (:,1:lb,:)=0.
    dBxdt(:,1:lb,:)=0.
    dBydt(:,1:lb,:)=0.
    dBzdt(:,1:lb,:)=0.
  end if
  if (mpi_y==mpi_ny-1) then
    drdt (:,ub  :my,:)=0.
    dpxdt(:,ub  :my,:)=0.
    dpydt(:,ub  :my,:)=0.
    dpzdt(:,ub+1:my,:)=0.
    dedt (:,ub  :my,:)=0.
    dBxdt(:,ub  :my,:)=0.
    dBydt(:,ub  :my,:)=0.
    dBzdt(:,ub+1:my,:)=0.
  endif
END

!-----------------------------------------------------------------------
SUBROUTINE E_driver (e1,e2)
  USE params
  implicit none
  real, dimension(mx,my,mz):: e1,e2
!
END

!-----------------------------------------------------------------------
SUBROUTINE regularize (f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
  integer iy
!
  if (mpi_y==0) then
    do iy=1,lb-1
      f(:,iy,:)=f(:,lb,:)
    enddo
  end if
  if (mpi_y==mpi_ny-1) then
    do iy=ub+1,my
      f(:,iy,:)=f(:,ub,:)
    enddo
  endif
END

!-----------------------------------------------------------------------
SUBROUTINE regularize_face (f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
  integer iy
!
  if (mpi_y==0) then
    do iy=1,lb
      f(:,iy,:)=f(:,lb+1,:)
    enddo
  end if
  if (mpi_y==mpi_ny-1) then
    do iy=ub+1,my
      f(:,iy,:)=f(:,ub,:)
    enddo
  endif
END

!-----------------------------------------------------------------------
SUBROUTINE magnetic_pressure_boundary (f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
END
