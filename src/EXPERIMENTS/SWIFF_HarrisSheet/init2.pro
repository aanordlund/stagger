@stagger_6th

; $Id: init2.pro,v 1.9 2011/12/16 08:05:11 aake Exp $
;------------------------------------------------------------------------------
;
; Case 2: Initial force-free equilibrium
;
;------------------------------------------------------------------------------

L=1.
ds=L/12.
init_derivs,dx=ds,dy=ds,dz=ds

default, Lx, 30.
default, Ly, 120.

nx=long(Lx/ds+0.5)
ny=long(Ly/ds+0.5)
help,nx,ny

x=ds*findgen(nx)
y=ds*findgen(ny)-Ly/2.
xx=rebin(reform(x,nx,1),nx,ny)
yy=rebin(reform(y,1,ny),nx,ny)

B0=1.
Bx=B0*tanh(yy/L)
By=fltarr(nx,ny)
Bz=B0*sech(yy/L)

beta=0.1
rho=replicate(B0^2,nx,ny)
pgas=0.5*rho*beta

px=fltarr(nx,ny)
py=fltarr(nx,ny)
pz=fltarr(nx,ny)

eps=0.1
Az=eps*B0*L*cos(2.*!pi*((xx-0.5*ds)-Lx/2.)/Lx)*cos(!pi*(yy-0.5*ds)/Ly)
lb=5
ub=ny-6
Bx[*,lb:ub,*]=Bx[*,lb:ub,*]+(ddyup(Az))[*,lb:ub,*]
By=By-ddxup(Az)

gamma=5./3.
e=pgas/(gamma-1.)

close,1
openw,1,'init2.dat'
writeu,1,rho
writeu,1,px
writeu,1,py
writeu,1,pz
writeu,1,e
writeu,1,Bx
writeu,1,By
writeu,1,Bz
close,1

END
