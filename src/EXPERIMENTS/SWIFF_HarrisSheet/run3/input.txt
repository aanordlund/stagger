! $Id: input.txt,v 1.1 2011/12/15 14:18:08 aake Exp $

&io      iread=0 iwrite=0 from='../init1.dat'
         tstop=400. tsnap=1.0 nscr=0 do_parallel_io=f do_master_io=t do_scp=f /
&run     t=0. dt=1e-2 Cdt=0.4 Cdtd=0.6 Cdtr=0.1 do_check=t do_flags=t do_dump=f do_logcheck=f do_timer=f /

&vars    do_pscalar=f do_energy=t do_mhd=t /
&grid    mpi_nz=1 omp_nz=1 mx=360 my=1440 mz=1 sx=30. sy=120. sz=0.08333 /

&pde     do_aniso_nu=f do_loginterp=f do_2nddiv=f nu1=.007 nu2=0.4 Csmag=.0 nu3=.007 nu4=1.4 nuB=1. nur=.0 cmax=0.0 /
&pde     do_aniso_nu=f do_loginterp=f do_2nddiv=f nu1=.005 nu2=0.2 Csmag=.0 nu3=.005 nu4=1.4 nuB=1. nur=.0 cmax=0.0 /

&bdry    /
&slice   /
&quench  do_quench=f qlim=2. qmax=2. /
&eqofst  /
&part    /
&force   /
&init    /
&expl    /
&cool    /
