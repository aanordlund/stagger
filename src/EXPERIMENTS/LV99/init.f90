! $Id: init.f90,v 1.2 2012/08/27 11:31:53 aake Exp $
!***********************************************************************
MODULE init_m
  implicit none
  real beta,r0,e0,d0,Bx0,Bz0,B0
  logical beltrami
END MODULE

!***********************************************************************
FUNCTION input_file()
  USE params
  implicit none
  character(len=mfile) input_file
  input_file='EXPERIMENTS/LV99/input.txt'
END

!***********************************************************************
SUBROUTINE read_init
  USE params
  USE init_m
  implicit none
  namelist /init/beta,r0,e0,Bx0,Bz0,beltrami
!
  rewind(stdin); read(stdin,init)
  e0=beta*0.5*Bz0**2/(gamma-1.)
  if (master) write(stdout,init)
END

!***********************************************************************
SUBROUTINE init_values (r,px,py,pz,e,d,Bx,By,Bz)
  USE params
  USE init_m
  implicit none
  real, dimension(mx,my,mz):: r,px,py,pz,e,d,Bx,By,Bz
  integer ix,iy,iz
  real Bx1,kx,x,ky,y,kz,z

  call init_force                                                       ! init_force always gets called
  call read_init
  
  !$omp parallel
  r(:,:,izs:ize)=r0                                                     ! encourage local mem
  e(:,:,izs:ize)=e0
  px(:,:,izs:ize)=0.
  py(:,:,izs:ize)=0.
  pz(:,:,izs:ize)=0.
  if (do_pscalar) d(:,:,izs:ize)=d0
  kx=2.*pi/sx
  ky=2.*pi/sy
  kz=2.*pi/sz
  if (do_mhd) then
    if (beltrami) then
      if (master) print*,'Beltrami field'
      do iz=1,mz
        z=(izoff+iz-1.0)*dz
	do iy=1,my
          y=(iyoff+iy-1.0)*dy
	  do ix=1,mx
            x=(ixoff+ix-1.0)*dx
	    Bx(ix,iy,iz)=B0*(cos(ky*y)+cos(kz*z))
	    By(ix,iy,iz)=B0*(cos(kz*z)+cos(kx*x))
	    Bz(ix,iy,iz)=B0*(cos(kx*x)+cos(ky*y))
          enddo
        enddo
      enddo
    else
      do iy=1,my
        y=(iyoff+iy-1.0)*dy
        Bx1=Bx0*cos(ky*y)
        Bz(:,iy,izs:ize) = sqrt(Bz0**2-Bx1**2)
        Bx(:,iy,izs:ize) = Bx0*cos(ky*y)
      end do
    endif
  end if
  !$omp end parallel
END
