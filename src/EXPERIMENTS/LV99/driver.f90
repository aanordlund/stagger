! $Id: driver.f90,v 1.2 2012/08/27 11:31:12 aake Exp $
!***********************************************************************
! Call hierachy:
!   call pde(...)
!     call forceit(...)
!     call velocity_boundary(...)
!       call driver(...)
!         call random_numbers(...)
!         call velocity_update(...)
!***********************************************************************
MODULE forcing
  real ampl,p
  integer case
  logical conservative
  real, allocatable, dimension(:):: rav
END MODULE

!***********************************************************************
SUBROUTINE init_force
  USE params
  USE forcing
  USE init_m
  implicit none
!.......................................................................

  beta=4.
  r0=1.
  d0=0.
  B0=1.
  Bz0=1.
  Bx0=Bz0*sx/sz
  beltrami=.false.
  gamma=5./3.

  ampl=1.
  case=0
  p=1.
  call read_force
  call read_init                                                        ! make sure &init namelist is read

  conservative=.true.
END

!***********************************************************************
SUBROUTINE read_force
  USE params
  USE init_m
  USE forcing
  implicit none
  namelist /force/ampl,p,case
!.......................................................................
  rewind (stdin)
  rewind (stdin); read (stdin,force)
  if (master) write(stdout,force)
  if (case > 2) call read_turb
END

!***********************************************************************
REAL FUNCTION flat(x)
  USE params
 !flat=cos(x)-cos(3.*x)/6.+cos(5.*x)/50.
  flat=cos(x)-cos(3.*x)/9
END FUNCTION

!***********************************************************************
REAL FUNCTION dflat(x)
  USE params
 !dflat=-sin(x)+3.*sin(3.*x)/6.-5.*sin(5.*x)/50.
  dflat=-sin(x)+3.*sin(3.*x)/9.
END FUNCTION

!***********************************************************************
SUBROUTINE forceit (r,Ux,Uy,Uz,xdnr,ydnr,zdnr,dpxdt,dpydt,dpzdt)
  USE params
  USE forcing
  implicit none
  real, dimension(mx,my,mz):: r,Ux,Uy,Uz,xdnr,ydnr,zdnr,dpxdt,dpydt,dpzdt
  real kx,ky,kz,x0,x1,y0,y1,z0,z1,flat,dflat
  integer ix,iy,iz
!.......................................................................
  kx=2.*pi/sx
  ky=2.*pi/sy
  kz=2.*pi/sz
!
! Compressive case: This has convergence maxima at x=0,y=0.25,z=0 and at
! x=0.5,y=0.75,z=0 + mirror images at z=0.5.
!
  if (case==0) then
    do iz=1,mz
      z0 = (izoff+iz-1.0)*dz
      do iy=1,my
        y1 = (iyoff+iy-1.5)*dy
        do ix=1,mx
          x0 = (ixoff+ix-1.0)*dx
	  dpydt(ix,iy,iz) = dpydt(ix,iy,iz) + ampl*  &
	    flat(kx*x0)*flat(ky*y1)/ky*flat(kz*z0)
        enddo
      enddo
    enddo
!
! Incompressive case 1: This has y-velocity going down at x=0,z=0
! and going up at x=0.5,z=0 + mirror images at z=0.5
!
  else if (case==1) then
    do iz=1,mz
      z0 = (izoff+iz-1.0)*dz
      do iy=1,my
        do ix=1,mx
          x0 = (ixoff+ix-1.0)*dx
	  dpydt(ix,iy,iz) = dpydt(ix,iy,iz) + ampl*  &
	    (flat(kx*x0)+flat(kz*z0))*ydnr(ix,iy,iz)
        enddo
      enddo
    enddo
!
! Incompressive case 2: This has y-velocity with B-convergence points
! at x=0,y=0,z=0 and x=0.5,y=0.5,z=0 + mirror images at z=0.5.  The
! compensating divergent scalar flow is in the x-direction for p=1 
! and in the z-direction for p=0.
!
  else if (case==2) then
    do iz=1,mz
      z0 = (izoff+iz-1.0)*dz
      z1 = (izoff+iz-1.5)*dz
      do iy=1,my
        y0 = (iyoff+iy-1.0)*dy + sy*0.25
        y1 = (iyoff+iy-1.5)*dy + sy*0.25
        do ix=1,mx
          x0 = (ixoff+ix-1.0)*dx
          x1 = (ixoff+ix-1.5)*dx
	  dpxdt(ix,iy,iz) = dpxdt(ix,iy,iz) + p*ampl* &
	    dflat(kx*x1)/kx*flat(ky*y0)*flat(kz*z0)*xdnr(ix,iy,iz)
	  dpydt(ix,iy,iz) = dpydt(ix,iy,iz) - ampl*  &
	    flat(kx*x0)*dflat(ky*y1)/ky*flat(kz*z0)*ydnr(ix,iy,iz)
	  dpzdt(ix,iy,iz) = dpzdt(ix,iy,iz) + (1.-p)*ampl* &
	    flat(kx*x0)*flat(ky*y0)*dflat(kz*z1)/kz*zdnr(ix,iy,iz)
        enddo
      enddo
    enddo
  else if (case==3) then
!
! Helical driving case.  Pure helical driving has curl(U) = c*U, which
! means ik x U_k = x U_k in Fourier space.  One can achieve this by taking
! a random force field, realized by choosing a few amplitudes and wave
! vectors at random, and then making a Helmholtz projection; i.e., subtracting
! from each Fourier component U_k its projection along k.  Here we simply
! take over the well proven driver from our turbulence work.
!
    call turb_force (r,Ux,Uy,Uz,xdnr,ydnr,zdnr,dpxdt,dpydt,dpzdt)
  endif
END SUBROUTINE
