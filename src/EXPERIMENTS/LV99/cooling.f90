! $Id: cooling.f90,v 1.2 2012/08/27 12:01:46 aake Exp $
!***********************************************************************
MODULE cooling
  USE params
  real t_newton, ee_newton
END MODULE

!***********************************************************************
SUBROUTINE init_cooling (r,e)
  USE params
  USE init_m, only: r0, e0
  USE cooling
  implicit none
  real, dimension(mx,my,mz):: r,e

  do_cool = .true.
  t_newton = 1.
  ee_newton = e0/r0

  call read_cooling
END SUBROUTINE

!***********************************************************************
SUBROUTINE read_cooling
  USE params
  USE eos
  USE cooling
  namelist /cool/do_cool, t_newton, ee_newton

  character(len=mid):: id='$Id: cooling.f90,v 1.2 2012/08/27 12:01:46 aake Exp $'
  call print_id(id)

  rewind (stdin); read (stdin,cool)
  if (master) write (*,cool)
END SUBROUTINE

!***********************************************************************
SUBROUTINE coolit (r,ee,lne,dd,dedt)
  USE params
  USE cooling
  implicit none
  real, dimension(mx,my,mz) :: r, ee, lne, dd, dedt
  logical omp_in_parallel

  if (omp_in_parallel()) then
    call coolit_omp (r,ee,lne,dd,dedt)
  else
    !$omp parallel
    call coolit_omp (r,ee,lne,dd,dedt)
    !$omp end parallel
  end if
END SUBROUTINE

!***********************************************************************
SUBROUTINE coolit_omp (r, ee, lne, dd, dedt)
!
!  Radiative cooling
!
  USE params
  USE units
  USE eos
  USE cooling
  implicit none
  real, dimension(mx,my,mz):: r, ee, lne, dd, dedt

  if (do_cool) then
    dedt(:,:,izs:ize) = dedt(:,:,izs:ize) &
                      - r(:,:,izs:ize)*(ee(:,:,izs:ize)-ee_newton)/t_newton
  end if
END
