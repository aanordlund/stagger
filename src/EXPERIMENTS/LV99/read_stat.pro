; $Id: read_stat.pro,v 1.2 2012/08/27 12:03:34 aake Exp $
;+
 FUNCTION read_stat,d,interval=interval,verbose=verbose
  default,d,'.'
  default,verbose,0
;-
  f=read_table(d+'/stat.txt')
  time=reform(f[0,*])
  n=n_elements(time)
  w=where(shift(time,1) gt time,nw)
  if n_elements(interval) eq 0 then begin
    if verbose gt 0 then print,nw,' intervals:'
    lmax=0.
    for i=0,nw-1 do begin
      w0=w[i]
      if i lt nw-1 then w1=w[i+1]-1 else w1=n-1
      times=[time[w0],time[w1]]
      length=time[w1]-time[w0]
      if length gt lmax then begin
        lmax=length
	interval=i 
      end
      if verbose gt 0 then print,i,w1-w0+1,time[w0],time[w1],time[w1]-time[w0]
    end
  end
  ; default,interval,nw-1
  w0=w[interval]
  if interval lt nw-1 then w1=w[interval+1]-1 else w1=n-1
  if verbose gt 0 then begin
    print,'interval:',interval
    print,'points:',w1-w0+1
    print,'t =',time[w0],time[w1]
  end
  return,f[*,w0:w1] 
END
