! $Id: boundary.f90,v 1.1 2012/04/22 22:37:20 aake Exp $
!************************************************************************
MODULE braiding
  USE params
  implicit none
  logical perpendicular
  real drive, t_rise, t_drive
  real r0, e0, d0, b0, beta
  real, allocatable, dimension(:,:):: fbdry, Ux_yl, Uz_yl, Ux_yh, Uz_yh

  integer ifirst, i_l, i_h
  real t_event, ampl, j0_l, j0_h, k0_l, k0_h, t_prv, amean, t_delta
  data ifirst/1/, t_event/-1e-20/, t_prv/-1./

CONTAINS

!************************************************************************
REAL FUNCTION ran1(idum)
  INTEGER idum,IA,IM,IQ,IR,NTAB,NDIV
  REAL AM,EPS,RNMX
  PARAMETER (IA=16807,IM=2147483647,AM=1./IM,IQ=127773,IR=2836, &
    NTAB=32,NDIV=1+(IM-1)/NTAB,EPS=1.2e-7,RNMX=1.-EPS)
  INTEGER j,k,iv(NTAB),iy
  SAVE iv,iy
  DATA iv /NTAB*0/, iy /0/
  if (idum.le.0.or.iy.eq.0) then
    idum=max(-idum,1)
    do j=NTAB+8,1,-1
      k=idum/IQ
      idum=IA*(idum-k*IQ)-IR*k
      if (idum.lt.0) idum=idum+IM
      if (j.le.NTAB) iv(j)=idum
    end do
    iy=iv(1)
  endif
  k=idum/IQ
  idum=IA*(idum-k*IQ)-IR*k
  if (idum.lt.0) idum=idum+IM
  j=1+iy/NDIV
  iy=iv(j)
  iv(j)=idum
  ran1=min(AM*iy,RNMX)
  return
END FUNCTION

!  (C) Copr. 1986-92 Numerical Recipes Software #10KQ'K5.){2p491"n26.
!****************************************************************
REAL FUNCTION gauss(A,S,ISEED)
!****************************************************************
!
!  GENERATES NORMAL (GAUSSIAN) PSEUDO-RANDOM DEVIATES
!  WITH EXPECTATION (AVERAGE) = A, AND STANDARD DEV. = S, SEED NUMBER
!  ISEED
!
  REAL A,S
  INTEGER ISEED
  REAL HOLD,R1,R2,G01
  COMMON /RND/ OLD, HOLD
  LOGICAL  OLD
!
  IF (OLD)  GO TO 1
  R2 = 6.2831853*ran1(iseed)
  R1 = SQRT(-2.*ALOG(ran1(iseed)))
  G01 = R1*COS(R2)
  HOLD  = R1*SIN(R2)
  OLD = .TRUE.
  gauss = A + S*G01
  RETURN
1 G01 = HOLD
  OLD = .FALSE.
  gauss = A + S*G01
  RETURN
END FUNCTION

!***********************************************************************
SUBROUTINE boundary_velocity (Ux, Uz)
  USE params, only: mx, mz, lb, ub
  implicit none
  real, dimension(mx,my,mz):: Ux, Uz
!
!  Control velocity at zone inside the driver to prevent ripples when driving
!
!  3 order polynomium with linear tangent at Uy,z(4,:,:) and Uy,z(nxgrid-4,:,:)
!  determined by Uyz(4) and Uyz(6) ...
!
!-----------------------------------------------------------------------
  call print_trace('boundary_velocity', dbg_force, 'BEGIN')
  if (mpi_y == 0) then
    Ux(:,lb+1,izs:ize) = (4./9.)*Ux(:,lb,izs:ize) + (6./9.)*Ux(:,lb+2,izs:ize) - (1./9.)*Ux(:,lb+3,izs:ize)
    Uz(:,lb+1,izs:ize) = (4./9.)*Uz(:,lb,izs:ize) + (6./9.)*Uz(:,lb+2,izs:ize) - (1./9.)*Uz(:,lb+3,izs:ize)
  endif
  if (mpi_y == mpi_ny-1) then
    Ux(:,ub-1,izs:ize) = (4./9.)*Ux(:,ub,izs:ize) + (6./9.)*Ux(:,ub-2,izs:ize) - (1./9.)*Ux(:,ub-3,izs:ize)
    Uz(:,ub-1,izs:ize) = (4./9.)*Uz(:,ub,izs:ize) + (6./9.)*Uz(:,ub-2,izs:ize) - (1./9.)*Uz(:,ub-3,izs:ize)
  endif
  call print_trace('boundary_velocity', dbg_force, 'END')
END subroutine

END MODULE
!=======================================================================

!-----------------------------------------------------------------------
SUBROUTINE density_boundary (r,lnr,py,e)
  USE params
  implicit none
  real, dimension(mx,my,mz):: r,lnr,py,e

  call print_trace('density_boundary', dbg_force, 'BEGIN')
  call density_boundary_log (r,lnr)
  call print_trace('density_boundary', dbg_force, 'END')
END

!-----------------------------------------------------------------------
SUBROUTINE density_boundary_log (r,lnr)
  USE params
  implicit none
  real, dimension(mx,my,mz):: r,lnr

  if (mpi_y == 0) then
    call extrapolate_center_lower (lnr)
    r(:,1:lb-1,izs:ize)=exp(lnr(:,1:lb-1,izs:ize))
  end if

  if (mpi_y == mpi_ny-1) then
    call extrapolate_center_upper (lnr)
    r(:,ub+1:my,izs:ize)=exp(lnr(:,ub+1:my,izs:ize))
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE energy_boundary (r,lnr,Ux,Uy,Uz,e,ee,p,Bx,By,Bz)
  USE params
  implicit none
  real, dimension(mx,my,mz):: r,lnr,Ux,Uy,Uz,e,ee,p,Bx,By,Bz

  if (.not. do_energy) return
  call print_trace('energy_boundary', dbg_force, 'BEGIN')
  call symmetric_center(ee)
  if (mpi_y == 0) then
    e(:,1:lb-1,izs:ize)=ee(:,1:lb-1,izs:ize)*r(:,1:lb-1,izs:ize)
  end if
  if (mpi_y == mpi_ny-1) then
    e(:,ub+1:my,izs:ize)=ee(:,ub+1:my,izs:ize)*r(:,ub+1:my,izs:ize)
  end if
  call print_trace('energy_boundary', dbg_force, 'END')
END

!-----------------------------------------------------------------------
SUBROUTINE energy_boundary_log (e)
  USE params
  implicit none
  real, dimension(mx,my,mz):: e

END

!-----------------------------------------------------------------------
SUBROUTINE passive_boundary (r,Ux,Uy,Uz,d,dd)
  USE params
  implicit none
  real, dimension(mx,my,mz):: r,Ux,Uy,Uz,d,dd
  integer iy, iz

  call symmetric_center(d)
END

!-----------------------------------------------------------------------
SUBROUTINE velocity_boundary (r,xdnr,ydnr,zdnr,Ux,Uy,Uz,px,py,pz,e,Bx,By,Bz)
  USE params
  USE braiding
  implicit none
  real, dimension(mx,my,mz):: r,xdnr,ydnr,zdnr,Ux,Uy,Uz,px,py,pz,e,ee,Bx,By,Bz

  call print_trace('velocity_boundary', dbg_force, 'BEGIN')

  call driver (Ux,Uz)
  !call boundary_velocity (Ux, Uz)
  
  if (mpi_y == 0) then
    px(:,lb:lb+1,izs:ize) = Ux(:,lb:lb+1,izs:ize)*xdnr(:,lb:lb+1,izs:ize)
    pz(:,lb:lb+1,izs:ize) = Uz(:,lb:lb+1,izs:ize)*zdnr(:,lb:lb+1,izs:ize)
  end if
  if (mpi_y == mpi_ny-1) then
    px(:,ub-1:ub,izs:ize) = Ux(:,ub-1:ub,izs:ize)*xdnr(:,ub-1:ub,izs:ize)
    pz(:,ub-1:ub,izs:ize) = Uz(:,ub-1:ub,izs:ize)*zdnr(:,ub-1:ub,izs:ize)
  end if

  call extrapolate_center (Ux)
  call extrapolate_center (Uz)
  call antisymmetric_face (Uy)
  
  if (mpi_y == 0) then
    px(:,1:lb,izs:ize) = Ux(:,1:lb,izs:ize)*xdnr(:,1:lb,izs:ize) 
    py(:,1:lb,izs:ize) = Uy(:,1:lb,izs:ize)*ydnr(:,1:lb,izs:ize) 
    pz(:,1:lb,izs:ize) = Uz(:,1:lb,izs:ize)*zdnr(:,1:lb,izs:ize) 
  end if
  if (mpi_y == mpi_ny-1) then
    px(:,ub  :my,izs:ize) = Ux(:,ub  :my,izs:ize)*xdnr(:,ub  :my,izs:ize) 
    py(:,ub+1:my,izs:ize) = Uy(:,ub+1:my,izs:ize)*ydnr(:,ub+1:my,izs:ize) 
    pz(:,ub  :my,izs:ize) = Uz(:,ub  :my,izs:ize)*zdnr(:,ub  :my,izs:ize) 
  end if

  call print_trace('velocity_boundary', dbg_force, 'END')
END

!-----------------------------------------------------------------------
SUBROUTINE mfield_boundary (Ux,Uy,Uz,Bx,By,Bz)
  USE params
  implicit none
  real, dimension(mx,my,mz):: Ux,Uy,Uz,Bx,By,Bz

  call extrapolate_center (Bx)
  call extrapolate_center (Bz)
END

!-----------------------------------------------------------------------
SUBROUTINE ecurrent_boundary (Ex, Ey, Ez, Jx, Jy, Jz)
  USE params
  USE braiding
  implicit none
  real, dimension(mx,my,mz):: Ex, Ey, Ez, Jx, Jy, Jz

! Zap the electric current in the ghost zones, to avoid that interpolations
! leave an influence on the Lorentz force at iy=lb.  The horizontal components
! are centered at iy-1/2 so we zap them to index lb.  The vertical component
! is centered at iy, and the vertical current at iy=lb is needed for proper
! PDE behavior, so we zap Jy to index lb-1.

  if (mpi_y == 0) then
    ! Jx(:,1:lb,izs:ize) = 0.                                           ! zap outside
    ! Jy(:,1:lb,izs:ize) = 0.                                           ! zap outside + bdry
    ! Jz(:,1:lb,izs:ize) = 0.                                           ! zap outside
    ! Jx(:,1:lb-1,izs:ize) = 0.                                         ! zap outside
    ! Jy(:,1:lb-2,izs:ize) = 0.                                         ! zap outside 
    ! Jz(:,1:lb-1,izs:ize) = 0.                                         ! zap outside
  end if
  if (mpi_y == mpi_ny-1) then
    ! Jx(:,ub+1:my,izs:ize) = 0.                                        ! zap outside
    ! Jy(:,ub  :my,izs:ize) = 0.                                        ! zap outside + bdry
    ! Jz(:,ub+1:my,izs:ize) = 0.                                        ! zap outside
    ! Jx(:,ub+2:my,izs:ize) = 0.                                        ! zap outside
    ! Jy(:,ub+2:my,izs:ize) = 0.                                        ! zap outside
    ! Jz(:,ub+2:my,izs:ize) = 0.                                        ! zap outside
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE viscosity_boundary (nu, f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: nu, f
END

!-----------------------------------------------------------------------
SUBROUTINE efield_boundary (Ex, Ey, Ez, Bx_y, Bx_z, By_z, By_x, Bz_x, Bz_y, &
                            Rx, Ry, Rz, Ux_y, Ux_z, Uy_z, Uy_x, Uz_x, Uz_y, &
                            Ux, Uy, Uz)
  USE params
  USE braiding
  implicit none
  real, dimension(mx,my,mz):: Ex, Ey, Ez, Bx_y, Bx_z, By_z, By_x, Bz_x, Bz_y, &
                              Rx, Ry, Rz, Ux_y, Ux_z, Uy_z, Uy_x, Uz_x, Uz_y, &
                              Ux, Uy, Uz

  call print_trace('efield_boundary', dbg_force, 'BEGIN')

  if (mpi_y == 0) then
    fbdry(:,izs:ize) = +Uz_yl(:,izs:ize)*b0
    call extrapolate_face_value_lower (Ex, fbdry)
  end if
  if (mpi_y == mpi_ny-1) then
    fbdry(:,izs:ize) = +Uz_yh(:,izs:ize)*b0
    call extrapolate_face_value_upper (Ex, fbdry)
  end if

  if (mpi_y == 0) then
    fbdry(:,izs:ize) = -Ux_yl(:,izs:ize)*b0
    call extrapolate_face_value_lower (Ez, fbdry)
  end if
  if (mpi_y == mpi_ny-1) then
    fbdry(:,izs:ize) = -Ux_yh(:,izs:ize)*b0
    call extrapolate_face_value_upper (Ez, fbdry)
  end if

  call print_trace('efield_boundary', dbg_force, 'END')
END

!-----------------------------------------------------------------------
SUBROUTINE regularize (f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
!
  call symmetric_center(f)
END

!-----------------------------------------------------------------------
SUBROUTINE regularize_face (f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
!
  call symmetric_face (f)
END

!-----------------------------------------------------------------------
SUBROUTINE ddt_boundary (r,px,py,pz,e,p,Bx,By,Bz, &
                         drdt,dpxdt,dpydt,dpzdt, &
                         dedt,dBxdt,dBydt,dBzdt)
  USE params
  USE braiding
  implicit none
  real, dimension(mx,my,mz):: r,px,py,pz,e,p,Bx,By,Bz, &
                              drdt,dpxdt,dpydt,dpzdt, &
                              dedt,dBxdt,dBydt,dBzdt

  call print_trace('ddt_boundary',dbg_hd,'BEGIN')

  if (mpi_y == 0) then
    dpxdt(:,lb,:) = drdt(:,lb,:)*Ux_yl(:,:)
    dpzdt(:,lb,:) = drdt(:,lb,:)*Uz_yl(:,:)
  end if
  if (mpi_y == mpi_ny-1) then
    dpxdt(:,ub,:) = drdt(:,ub,:)*Ux_yh(:,:)
    dpzdt(:,ub,:) = drdt(:,ub,:)*Uz_yh(:,:)
  end if

  call symmetric_center(drdt)
  if (do_energy) call symmetric_center(dedt)
  call extrapolate_center(dpxdt)
  call extrapolate_center(dpzdt)
  call antisymmetric_face(dpydt)
  call extrapolate_center(dBxdt)
  call extrapolate_center(dBzdt)
END

!-----------------------------------------------------------------------
SUBROUTINE init_boundary
  USE params
  USE braiding
  USE arrays, ONLY: lns
  implicit none
  integer iy
  character(len=mfile):: name
  character(len=mid):: id='$Id: boundary.f90,v 1.1 2012/04/22 22:37:20 aake Exp $'

  call print_id(id)
  periodic(2) = .false.

  perpendicular = .false.                                               ! not necessarily perpendicular
  t_drive = 5.                                                          ! average time for each event
  t_rise  = .1                                                          ! velocity rise time
  amean   = .5                                                          ! 50/50 chance of direction change
  drive   = .1                                                          ! drive speed amplitude
  
  r0   = 1.
  b0   = 1.
  d0   = 1.
  beta = 0.02

  call read_boundary
  
  lb = 6                                                                ! five boundary zones
  dy = sy/(my*mpi_ny-2*lb+1)                                            ! reset y-mesh size
  ymin = -(lb-1)*dy                                                     ! force ym(lb)=0.
  
  if (mpi_y > 0) lb = 1                                                 ! not a lower bdry domain

  if (mpi_y == mpi_ny-1) then
    ub = my-5                                                           ! upper bdry domain
  else
    ub = my                                                             ! not an upper bdry domain
  end if

  if (mpi_y ==        0) allocate (Ux_yl(mx,mz), Uz_yl(mx,mz))
  if (mpi_y == mpi_ny-1) allocate (Ux_yh(mx,mz), Uz_yh(mx,mz))
  allocate (fbdry(mx,mz))
END

!-----------------------------------------------------------------------
SUBROUTINE read_boundary
  USE params
  USE braiding
  implicit none
  namelist /bdry/ t_drive, t_event, t_rise, iseed, amean, drive, &
                  beta, b0, r0, perpendicular

  if (stdin.ge.0) then
    rewind (stdin); read (stdin,bdry)
  else
    read (*,bdry)
  end if
  if (master) write (*,bdry)
  e0=beta/(gamma-1.)*0.5*b0**2
END
