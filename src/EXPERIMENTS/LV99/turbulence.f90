! $Id: turbulence.f90,v 1.1 2012/08/27 12:10:36 aake Exp $
!**********************************************************************
MODULE turbulence_mod

logical conservative
logical do_force, do_helmh
logical, save:: initialized=.false.
integer istart
real tprev, t_turb, t_turn, ampl_turb, k1, k2, pk, a_helmh
real calib, enom, mach, edot
real rnd1, rnd2, rnd3, rhoav, xav, yav, zav
real, allocatable, dimension(:,:,:):: pranx,prany,pranz,franx,frany,franz
real, allocatable, dimension(:):: rav

END MODULE
!**********************************************************************
  SUBROUTINE init_turb
    USE params
    USE turbulence_mod
    implicit none
    integer iz
    character(len=mid):: id='$Id: turbulence.f90,v 1.1 2012/08/27 12:10:36 aake Exp $'

    call print_id(id)

    ampl_turb = 0.1
    do_force = .true.
    do_helmh = .true.
    conservative = .true.
    a_helmh=1.
    k1=1.
    k2=1.5
    pk=7./6.
    t_turn = 0.1
    t_turb = 0.
    if (t_turb == 0.) t_turb = -1.5*t_turn

    istart = 1
    tprev = -1.

    allocate (pranx(mx,my,mz), prany(mx,my,mz), pranz(mx,my,mz))
    allocate (franx(mx,my,mz), frany(mx,my,mz), franz(mx,my,mz))
!$omp parallel private(iz)
    do iz=izs,ize                                ! encourage distribution
      pranx(:,:,iz)=0. ; prany(:,:,iz)=0. ; pranz(:,:,iz)=0.
      franx(:,:,iz)=0. ; frany(:,:,iz)=0. ; franz(:,:,iz)=0.
    end do
!$omp end parallel

    initialized=.true.
  END SUBROUTINE

!***********************************************************************
  SUBROUTINE read_turb
    USE params
    USE turbulence_mod
    namelist /turb/ do_force, do_helmh, a_helmh, k1, k2, ampl_turb, t_turn, t_turb, pk
!-----------------------------------------------------------------------
    if (.not. initialized) call init_turb
    rewind (stdin); read (stdin,turb)
    if (master) write (*,turb)
  END SUBROUTINE

!***********************************************************************
  SUBROUTINE turb_force (r,Ux,Uy,Uz,xdnr,ydnr,zdnr,dpxdt,dpydt,dpzdt)
  USE params
  implicit none
  real, dimension(mx,my,mz):: r,Ux,Uy,Uz,xdnr,ydnr,zdnr,dpxdt,dpydt,dpzdt
  logical omp_in_parallel
!-----------------------------------------------------------------------
  if (omp_in_parallel()) then
    call turb_force_omp (r,Ux,Uy,Uz,xdnr,ydnr,zdnr,dpxdt,dpydt,dpzdt)
  else
    !$omp parallel
    call turb_force_omp (r,Ux,Uy,Uz,xdnr,ydnr,zdnr,dpxdt,dpydt,dpzdt)
    !$omp end parallel
  end if
  END SUBROUTINE

!***********************************************************************
  SUBROUTINE turb_force_omp (r,Ux,Uy,Uz,xdnr,ydnr,zdnr,dpxdt,dpydt,dpzdt)
!
!  Calculate a random force, confined to a shell in k-space, at
!  regular intervals in time.  We assume rho, P, and the sound
!  speed to be near unity.  The velocity amplitude should be of the
!  order ampl_turb.  The size (scale) of the driving motions is of the
!  order of mx*dx/k1.  Thus the turnover time is given by
!  t_turb*ampl_turb = mx*dx/k1; and the acceleration is of the order
!  ampl_turb/t_turb.
!
!  Note:  In order to restart properly, one would have to save the
!  previous value of iseed.
!
!  06-aug-93/aake:  bug corrected; iseed was not negative at start
!  09-aug-93/aake:  changed to allow restart w same random coeffs
!  21-feb-95/paolo: changed in order to use segments of constant
!                   time der. of acceleration (so continuous accel.)
!            franx(,y,z) are time deriv. of accel. (random, solenoidal);
!            pranx(,y,z) (and scr2(,3,4)) are accelerations. The variable
!            ``accel'' is now the time derivative of the acceleration.
!            ``t_turn'' has the same meaning as before (see comment above).
!  23-jan-96/aake:  bug corrected; ran1() -> 2.*ran1()-1. to span [-1,1]
!
!            Restart must be done AFTER t=t_turn.
!            ------------------------------------
!----------------------------------------------------------------------
  USE params
  USE arrays, only: scr1,scr2,scr3,scr4
  USE turbulence_mod

  implicit none

  real, dimension(mx,my,mz):: r,Ux,Uy,Uz,xdnr,ydnr,zdnr,dpxdt,dpydt,dpzdt

  integer i,j,k,jx,jy,jz,nrand,kmax

  complex fxx,fyy,fzz,kx,ky,kz,corr,expikrx,expikry,expikrz

  real x, y, z, w, fact, test, fpow, accel, ran1s
  real eps, fk, average
  real xsum(mz), ysum(mz), zsum(mz), rsum(mz)

  logical debug

!----------------------------------------------------------------------
!   
                    if (t .ne. tprev) then    ! prevent repeat at same t

!--------------------------------------------- new random numbers:
!-----------------------------------------------------------------

  if (master .and. idbg>0) print *,'forceit: t,t_turb,istart=',t,t_turb,istart

  if (t .gt. t_turb .or. istart .ne. 0) then
  if (master .and. idbg>0) print *,'new force'

  eps = 1e-5

!  Loop until t_turb > t
!
100 continue

    if (do_trace) print *,'forceit1:',omp_mythread,t,t_turb,istart

    if (istart .ne. 0) then
      do k=izs,ize
        pranx(:,:,k)=0.
        prany(:,:,k)=0.
        pranz(:,:,k)=0.
      end do
    else
      do k=izs,ize
        pranx(:,:,k)=franx(:,:,k)
        prany(:,:,k)=frany(:,:,k)
        pranz(:,:,k)=franz(:,:,k)
      end do
    endif

    call barrier_omp('turb1')
!$omp single
    istart = 0
    tprev = t
    t_turb = t_turb + t_turn
!$omp end single

    nrand = 0
    kmax=ifix(k2+1.)
    do jx=-kmax,kmax
      do jy=-kmax,kmax
        do jz=-kmax,kmax
          fk = sqrt(float(jx**2+jy**2+jz**2))
          if (fk .ge. k1-eps .and. fk .le. k2+eps) nrand=nrand+1
        enddo
      enddo
    enddo
    if (master) print '(1x,a,2g14.6,i4,i12)','random_force:t=',t,t_turb,nrand,iseed

!--------------------------------------------------------------------------------
    accel  = ampl_turb/t_turn/sqrt(float(nrand)/8.)    ! rms=1./8. per comp.
!--------------------------------------------------------------------------------


    do k=izs,ize
      franx(:,:,k) = 0.
      frany(:,:,k) = 0.
      franz(:,:,k) = 0.
    end do

! To obtain a Kolmogorov slope of the driving alone, one should have amplitudes
! a(k) that drop with k^(-11./6.), to have a(k)^2*k^2 = k^(-5./3.).  This ASSUMES
! that the amplitudes are proportional to the driving.  But the energy drain may
! be rather inversely proportional to the turnover time, which goes as k^(2./3.)

    ! pk = 11./6.
    ! pk = 7./6.

    fpow = 0.
    do jx=-kmax,kmax
      kx = cmplx (0., jx*2.*pi/sx)
      do jy=-kmax,kmax
        ky = cmplx (0., jy*2.*pi/sy)
        do jz=-kmax,kmax
          kz = cmplx (0., jz*2.*pi/sz)
          fk = sqrt(float(jx**2+jy**2+jz**2))
          if (fk .ge. k1-eps .and. fk .le. k2+eps) then

! Need a barrier here to prevent the single thread that updates rnd1-rnd3
! from doing so before some other thread is through using it, someplace
! in the loop below.  In realistic cases this is very unlikely to happen,
! because the threads spend a lot of time in the last ijk-loop, when they
! have already used rnd1-rnd3.  But it does happen in small tests.

!$omp barrier
!$omp single
            rnd1 = ran1s(iseed); rnd2 = ran1s(iseed); rnd3 = ran1s(iseed)
!$omp end single
            fxx =   cexp(cmplx(0., 2.*pi*rnd1))/fk**pk
            fyy =   cexp(cmplx(0., 2.*pi*rnd2))/fk**pk
            if (mz.gt.1) then
              fzz = cexp(cmplx(0., 2.*pi*rnd3))/fk**pk
            else
              fzz=0.
            end if

! solenoidal field:
!------------------
           if (do_helmh) then
              corr=(kx*fxx+ky*fyy+kz*fzz)/(kx*kx+ky*ky+kz*kz+1e-20) 

              if (jx.ne.0) fxx = fxx - a_helmh*corr*kx
              if (jy.ne.0) fyy = fyy - a_helmh*corr*ky
              if (jz.ne.0) fzz = fzz - a_helmh*corr*kz
           endif
!------------------

           fact=1.
           if (jx.gt.0) fact=fact*0.5
           if (jy.gt.0) fact=fact*0.5
           if (jz.gt.0) fact=fact*0.5
           fpow = fpow+fact*(cabs(fxx)**2+cabs(fyy)**2+cabs(fzz)**2)

           do k=izs,ize
             z = zm(k)-zmin
             do j=1,my
               y = ym(j)-ymin
               do i=1,mx
                 x = xm(i)-xmin
                 expikrx = accel*cexp(kx*(x-0.5*dx)+ky*y+kz*z)
                 expikry = accel*cexp(kx*x+ky*(y-0.5*dy)+kz*z)
                 expikrz = accel*cexp(kx*x+ky*y+kz*(z-0.5*dz))
                 franx(i,j,k) = franx(i,j,k) + real(fxx*expikrx)
                 frany(i,j,k) = frany(i,j,k) + real(fyy*expikry)
                 franz(i,j,k) = franz(i,j,k) + real(fzz*expikrz)
               end do
             end do
           end do
           if (master .and. debug(dbg_force)) then
	     print '(1x,a,3i2,f6.3,3(f7.3,f6.3),6f7.3)', &
               'random_force:k=',jx,jy,jz,fk,fxx,fyy,fzz,fact,zm(1),zmin,kz,franx(1,1,1)
           endif
          endif
        enddo
      enddo
    enddo

!--------------------------------          

! TEST:
!--------------------------------------------------
    if (master .and. idbg.ge.1) then
      test=sqrt(sum(franx**2+frany**2+franz**2)/mw)
      print *,'AVERAGE ACCELERATION = ',test,sqrt(fpow)*accel
    end if
!--------------------------------------------------
   
    call barrier_omp('turb2')
    if (do_trace) print *,'forceit2:',omp_mythread,t,t_turb,istart
    if (t_turb .lt. t) then
       goto 100
    endif
  
  endif  ! new random numbers
!----------------------------------------end of new random numbers.
!------------------------------------------------------------------
                            endif                            

  if (master .and. idbg>0) print *,'interpolate force'

!  Time interpolation of the force
!
  !w = 1.-exp(-(t+t_turn-t_turb)/t_turn)
  w = (t_turn-(t_turb-t))/t_turn
  w = 0.5*(1.-cos(w*pi))
  if (master .and. idbg>0 .and. isubstep==1) &
    print *,'force: t-t_turb, t_turn, w:', t-t_turb, t_turn, w
  do k=izs,ize
    scr1(:,:,k) = pranx(:,:,k) + (franx(:,:,k)-pranx(:,:,k))*w
    scr2(:,:,k) = prany(:,:,k) + (frany(:,:,k)-prany(:,:,k))*w
    scr3(:,:,k) = pranz(:,:,k) + (franz(:,:,k)-pranz(:,:,k))*w
  end do

! Force and velocity averages
!----------------------------

  call average_subr (r, rhoav)
  !rhoav=1.
  if (master .and. do_trace) print *,'turbulence: average1', rhoav
  do k=izs,ize
    scr4(:,:,k) = xdnr(:,:,k)*(scr1(:,:,k)+Ux(:,:,k)*(1./t_turn))/rhoav
  end do
  call average_subr (scr4, xav)
  if (master .and. do_trace) print *,'turbulence: average2', xav
  !xav=0.

  do k=izs,ize
    scr4(:,:,k) = ydnr(:,:,k)*(scr2(:,:,k)+Uy(:,:,k)*(1./t_turn))/rhoav
  end do
  call average_subr (scr4, yav)
  if (master .and. do_trace) print *,'turbulence: average3', yav
  !yav=0.

  do k=izs,ize
    scr4(:,:,k) = zdnr(:,:,k)*(scr3(:,:,k)+Uz(:,:,k)*(1./t_turn))/rhoav
  end do
  call average_subr (scr4, zav)
  if (master .and. do_trace) print *,'turbulence: average4', zav
  !zav=0.

  do k=izs,ize
    dpxdt(:,:,k) = dpxdt(:,:,k) + xdnr(:,:,k)*(scr1(:,:,k) - xav)
    dpydt(:,:,k) = dpydt(:,:,k) + ydnr(:,:,k)*(scr2(:,:,k) - yav)
    dpzdt(:,:,k) = dpzdt(:,:,k) + zdnr(:,:,k)*(scr3(:,:,k) - zav)
  end do

  if (master .and. idbg > 1) then
    call stats ('pranx', scr1)
    call stats ('prany', scr2)
    call stats ('pranz', scr3)
  end if

  if (master .and. do_trace) print *,'selfg'

END SUBROUTINE
