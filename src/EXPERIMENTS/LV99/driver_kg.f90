! $Id: driver_kg.f90,v 1.1 2012/04/22 22:37:23 aake Exp $

!***********************************************************************
SUBROUTINE driver (Ux,Uz)
  USE params, ONLY: mx, mz, iseed, master, izs, ize
  USE braiding
  implicit none
  real, dimension(mx,my,mz):: Ux, Uz
!
  integer ii
  real Ux_sum, Uz_sum, UxS, UzS

!-----------------------------------------------------------------------
!
!  iseed, t and t_event should be taken from the previous run when continuing
!
  call print_trace('driver', dbg_force, 'BEGIN')
  !$omp barrier
  !$omp single
  if (ifirst.gt.0) then
    ifirst=0
    if (mpi_y == 0) then
      Ux_yl = Ux(:,lb,:)
      Uz_yl = Uz(:,lb,:)
    endif
    if (mpi_y == mpi_ny-1) then
      Ux_yh = Ux(:,ub,:)
      Uz_yh = Uz(:,ub,:)
    endif
    iseed = -iseed
    do ii=1,50
      j0_l = sx*ran1(iseed)
    end do
    i_l   = 0
    if (perpendicular) i_l=1
    k0_l  = 0.
    t_prv = t-1.e-6
  endif
!
!  At event time, change component, phase and amplitude at random
!
1 continue
  if (t .gt. t_event) then
    call rand_numbers (i_l, amean, iseed, j0_l, k0_l, drive, t_rise, &
                       ampl, t_delta, t_drive, t_event)
    t_event = t_event + t_delta
    ! t_delta = t_event
    if (mpi_y == 0) then
      Ux_sum = sum(Ux_yl**2)/(mx*mz)
      Uz_sum = sum(Uz_yl**2)/(mx*mz)
    else
      Ux_sum = 0.
      Uz_sum = 0.
    endif
    call sum_mpi_real (Ux_sum, UxS, 1)
    call sum_mpi_real (Uz_sum, UzS, 1)
    UxS = sqrt(UxS/(mpi_nx*mpi_nz))
    UzS = sqrt(UzS/(mpi_nx*mpi_nz))
    if (mpi_rank .eq. 0) then
      write (*,'(1x,a,i8,a,f10.6)') 'next event in about', &
        ifix(t_delta/dt),' time steps, at time', t_event
      write (*,'(1x,a,i3,2(1x,f7.3),4(1x,f10.6),i12)') &
       'drive:',i_l,j0_l,k0_l,ampl,t_event,UxS,UzS,iseed
    endif
    goto 1
  endif
  !$omp end single
  !$omp barrier
!
!  Update the bdry velocities, but only once (predictor step)
!
  if (t .gt. t_prv) then
    !$omp barrier
    !$omp single
    t_prv = t
    !$omp end single
    if (mpi_y == 0) then
      Ux_yl(:,izs:ize) = Ux_yl(:,izs:ize)*(1.-dt/t_rise)
      Uz_yl(:,izs:ize) = Uz_yl(:,izs:ize)*(1.-dt/t_rise)
      call velocity_update (i_l, drive, +ampl, k0_l, j0_l, Ux_yl, Uz_yl)
    endif
    if (mpi_y == mpi_ny-1) then
      Ux_yh(:,izs:ize) = Ux_yh(:,izs:ize)*(1.-dt/t_rise)
      Uz_yh(:,izs:ize) = Uz_yh(:,izs:ize)*(1.-dt/t_rise)
      i_h = i_l
      if (perpendicular) i_h = 3-i_l
      call velocity_update (i_h, drive, -ampl, k0_l, j0_l, Ux_yh, Uz_yh)
    endif
  endif

  if (mpi_y == 0) then
    Ux(:,lb,izs:ize) = Ux_yl(:,izs:ize)
    Uz(:,lb,izs:ize) = Uz_yl(:,izs:ize)
  endif
  if (mpi_y == mpi_ny-1) then
    Ux(:,ub,izs:ize) = Ux_yh(:,izs:ize)
    Uz(:,ub,izs:ize) = Uz_yh(:,izs:ize)
  endif
!
  call print_trace('driver', dbg_force, 'END')
END subroutine

!***********************************************************************
SUBROUTINE velocity_update (i, drive, ampl, k0, j0, Uxb, Uzb)
!
! update the velocity field for the rand_shear drivers
!
  USE params
  implicit none
  integer i
  real k0, j0
  real drive, ampl
  real, DIMENSION(mx,mz) :: Uxb, Uzb
  integer iz

  call print_trace('velocity_update', dbg_force, 'BEGIN')
  if (i.eq.1) then
    if (drive .gt. 0.) then
      do iz=izs,ize
        Uxb(:,iz) = Uxb(:,iz) + dt*ampl*sin(2.*pi*(zm(iz)-zmin-k0)/sz)
      end do
    else
      do iz=izs,ize
        Uxb(:,iz) = Uxb(:,iz) + dt*ampl*sin(2.*pi*(zm(iz)-zmin+k0)/sz)
      end do
    endif
  else
    if (drive .gt. 0.) then
      do iz=izs,ize
        Uzb(:,iz) = Uzb(:,iz) + dt*ampl*sin(2.*pi*(xm(:)-xmin-j0)/sx)
      end do
    else
      do iz=izs,ize
        Uzb(:,iz) = Uzb(:,iz) + dt*ampl*sin(2.*pi*(xm(:)-xmin+j0)/sx)
      end do
    endif
  endif
  call print_trace('velocity_update', dbg_force, 'END')
END subroutine

!***********************************************************************
SUBROUTINE rand_numbers (i, amean, iseed, j0, k0, drive, t_rise, &
                           ampl, t_delta, t_drive, t_event)
!
!  controls the random numbers in the rand_shear drivers; amean=0.5 
!  gives 50/50 chance of change, amean=0.75 gives 3/4 chance of change
!
  USE params, only: dbg_force, sx, sz
  USE braiding, only: ran1, gauss
  implicit none
  integer i, iseed
  real j0, k0
  real amean, drive, ampl, t_delta, t_event
  real t_rise, t_drive, d_tmp

  call print_trace('rand_numbers', dbg_force, 'BEGIN')

!  driver direction
  i = i + 1
  i = mod(i - amean + ran1(iseed),2.) + 1
  if (i .eq. 2) then
    j0 = sx*ran1(iseed)                                                 ! i==2 is Uz(x-j0)
  else
    k0 = sz*ran1(iseed)                                                 ! i==1 is Ux(x-k0)
  endif

!  driver amplitude
  d_tmp = abs(drive/t_rise)
  if (t_drive .gt. 0.) then
    ampl = amin1(amax1(gauss(d_tmp, d_tmp/5., iseed), .5*d_tmp), 1.5*d_tmp)
    ampl = sign(ampl,drive)
!  driver time
    t_delta = t_drive*amax1(amin1(4.,-alog(ran1(iseed))),.2)
  else
    ampl = sign(d_tmp,drive)
    t_delta = -t_drive
  endif

  call print_trace('rand_numbers', dbg_force, 'END')
END subroutine

