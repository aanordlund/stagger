! $Id: initial.f90,v 1.7 2007/04/23 01:56:28 aake Exp $
!************************************************************************
FUNCTION input_file()
  USE params
  implicit none
  character(len=mfile) input_file
  input_file = 'EXPERIMENTS/oblique/input.txt'
END

!-----------------------------------------------------------------------
SUBROUTINE init_values (lnr,Ux,Uy,Uz,ee,d,Bx,By,Bz)
  USE params
  USE boundary
  USE arrays, ONLY: scr5, scr6
  USE forcing
  implicit none

  real, dimension(mx,my,mz):: lnr,Ux,Uy,Uz,ee,d,Bx,By,Bz
  logical do_test, scr_alloc
  character(len=32):: test
  real ee0,gam1,mrms,average,ti_turn,ran1s,y0,kx,kz
  integer iz,iy,ix,iy1
  character(len=mid), save:: id='$Id: initial.f90,v 1.7 2007/04/23 01:56:28 aake Exp $'

  call print_id(id)

!-----------------------------------------------------------------------
!  Initial value parameters
!-----------------------------------------------------------------------

  gam1=gamma*(gamma-1.)
  if (gamma .eq. 1.) gam1=1.
  ee0 = csound**2/gam1

  y0 = 0.
  kx = 2.*pi/sx
  kz = 2.*pi/sz
  print *,mpi_rank
  print *,ym
  iy1 = (my+1)/2
!$omp parallel do private(iz)
  do iz=1,mz
    do iy=1,my
      do ix=1,mx
        lnr(ix,iy,iz) = lnr_amp*ran1s(iseed)
      end do
      ee(:,iy,iz) = ee0
      if (abs(ym(iy)-y0) .lt. 0.5*dy) then
        Ux(:,iy,iz) = U0(1)
	do ix=1,mx
          Uy(ix,iy,iz) = 0. + u_amp*(sin(kx*xm(ix))+sin(2.*kx*xm(ix)))*sin(kz*zm(iz))
	end do
        Uz(:,iy,iz) = U0(3)
        if (do_mhd) then
          Bx(:,iy,iz) = B0(1)
          By(:,iy,iz) = 0.
          Bz(:,iy,iz) = B0(3)
        end if
      else if (ym(iy) .lt. y0) then
        Ux(:,iy,iz) = U0(1)
	do ix=1,mx
          Uy(ix,iy,iz) = U0(2) + u_amp*(sin(kx*xm(ix))+sin(2.*kx*xm(ix)))*sin(kz*zm(iz))
	end do
        Uz(:,iy,iz) = U0(3)
        if (do_mhd) then
          Bx(:,iy,iz) = B0(1)
          By(:,iy,iz) = B0(2)
          Bz(:,iy,iz) = B0(3)
        end if
      else
        Ux(:,iy,iz) = -U0(1)
	do ix=1,mx
          Uy(ix,iy,iz) = -U0(2) + u_amp*(sin(kx*xm(ix))+sin(2.*kx*xm(ix)))*sin(kz*zm(iz))
	end do
        Uz(:,iy,iz) = -U0(3)
        if (do_mhd) then
          Bx(:,iy,iz) = B0(1)
          By(:,iy,iz) = B0(2)
          Bz(:,iy,iz) = -B0(3)
        end if
      end if
    end do
  end do
END
