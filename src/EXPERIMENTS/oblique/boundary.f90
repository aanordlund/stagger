! $Id: boundary.f90,v 1.9 2007/04/23 01:56:53 aake Exp $
!***********************************************************************

MODULE boundary
  integer, parameter:: mbdry=8
  real, allocatable, dimension(:,:,:):: lnrb,Uxb,Uyb,Uzb,eb,db,bxb,byb,bzb
  real, dimension(3):: U0, B0
  logical load_boundaries
  real d0, lnr_amp, u_amp
END MODULE boundary

!-----------------------------------------------------------------------
SUBROUTINE init_boundary
  USE params
  USE boundary
  implicit none
  character(len=mid):: id="$Id: boundary.f90,v 1.9 2007/04/23 01:56:53 aake Exp $"
  call print_id (id)

  lb = 6
  ub = my-5

  !load_boundaries=.true.
  !allocate (lnrb(mx,my,mz),Uxb(mx,my,mz),Uyb(mx,my,mz),Uzb(mx,my,mz))
  !lnrb=0.; Uxb=0.; Uyb=0.; Uzb=0.
  !if (do_energy) then
  !  allocate (eb(mx,my,mz))
  !end if
  !if (do_mhd) then
  !  allocate (Bxb(mx,my,mz),Byb(mx,my,mz),Bzb(mx,my,mz))
  !end if

  b0 = 0.
  d0 = 1.
  U0 = (/1.,1.,0./)
  B0 = (/1.,0.,1./)
  lnr_amp = 0.01
  u_amp = 0.5

  call read_boundary

  if (do_trace) print *,'exit'
END

!-----------------------------------------------------------------------
SUBROUTINE read_boundary
  USE params
  USE boundary
  implicit none
  namelist /bdry/ d0, b0, U0, lnr_amp, u_amp

  rewind (stdin); read (stdin,bdry)
  if (master) write (stdout,bdry)
!
END

!-----------------------------------------------------------------------
SUBROUTINE density_boundary (r)
  USE params
  USE boundary
  implicit none
  real, dimension(mx,my,mz):: r
END

!-----------------------------------------------------------------------
SUBROUTINE density_boundary_log (lnr)
  USE params
  USE boundary
  implicit none
  real, dimension(mx,my,mz):: lnr
END

!-----------------------------------------------------------------------
SUBROUTINE energy_boundary (r,lnr,Ux,Uy,Uz,e,ee,p,Bx,By,Bz)
  USE params
  USE boundary
  implicit none
  real, dimension(mx,my,mz):: r,lnr,Ux,Uy,Uz,e,ee,p,Bx,By,Bz
END

!-----------------------------------------------------------------------
SUBROUTINE passive_boundary (r,Ux,Uy,Uz,d,dd)
  USE params
  USE boundary
  implicit none
  real, dimension(mx,my,mz):: r,Ux,Uy,Uz,d,dd
END

!-----------------------------------------------------------------------
SUBROUTINE velocity_boundary (Ux,Uy,Uz)
  USE params
  USE boundary
  implicit none
  real, dimension(mx,my,mz):: Ux,Uy,Uz
END

!-----------------------------------------------------------------------
SUBROUTINE mfield_boundary (Bx,By,Bz)
  USE params
  USE boundary
  implicit none
  real, dimension(mx,my,mz):: Bx,By,Bz
END

!-----------------------------------------------------------------------
SUBROUTINE efield_boundary (Ex, Ey, Ez)
  USE params
  USE boundary
  implicit none
  real, dimension(mx,my,mz):: Ex, Ey, Ez
  integer iz
!
  if (mpi_y == 0) then
    do iz=izs,ize
      Ex(:,1:mbdry+1,iz) = -U0(2)*B0(3)+U0(3)*B0(2)
      Ey(:,1:mbdry+1,iz) = -U0(3)*B0(1)+U0(1)*B0(3)
      Ez(:,1:mbdry+1,iz) = -U0(3)*B0(1)+U0(2)*B0(1)
    end do
  end if
  if (mpi_y == mpi_ny-1) then
    do iz=izs,ize
      Ex(:,my-mbdry:my,iz) = -U0(2)*B0(3)-U0(3)*B0(2)
      Ey(:,my-mbdry:my,iz) = +U0(3)*B0(1)+U0(1)*B0(3)
      Ez(:,my-mbdry:my,iz) = +U0(3)*B0(1)-U0(2)*B0(1)
    end do
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE ddt_boundary (lnr,Ux,Uy,Uz,e,Bx,By,Bz, &
                         dlnrdt,dUxdt,dUydt,dUzdt, &
                         dedt,dBxdt,dBydt,dBzdt)
  USE params
  USE boundary
  implicit none
  real, dimension(mx,my,mz):: lnr,Ux,Uy,Uz,e,Bx,By,Bz, &
                              dlnrdt,dUxdt,dUydt,dUzdt, &
                              dedt,dBxdt,dBydt,dBzdt
  integer ix,iy,iz
  real, allocatable, dimension(:,:,:):: r
  real ra, lnra
!
  allocate (r(mx,my,mz))
  do iz=izs,ize
    r(:,:,iz) = exp(lnr(:,:,iz))
  end do
  call average_subr (r, ra)
  lnra = alog(ra)
  deallocate (r)

  if (mpi_y == 0) then
    do iz=izs,ize
      dlnrdt(:,1:mbdry,iz) = (lnra - lnr(:,1:mbdry,iz))
      dUxdt(:,1:mbdry,iz) = 0.
      dUydt(:,1:mbdry+1,iz) = 0.
      dUzdt(:,1:mbdry,iz) = 0.
      if (do_mhd) then
        dBxdt(:,1:mbdry,iz) = 0.
        dBydt(:,1:mbdry+1,iz) = 0.
        dBzdt(:,1:mbdry,iz) = 0.
      end if
    end do
  end if
  if (mpi_y == mpi_ny-1) then
    do iz=izs,ize
      dlnrdt(:,my-mbdry+1:my,iz) = (lnra - lnr(:,my-mbdry+1:my,iz))
      dUxdt(:,my-mbdry+1:my,iz) = 0.
      dUydt(:,my-mbdry+1:my,iz) = 0.
      dUzdt(:,my-mbdry+1:my,iz) = 0.
      if (do_mhd) then
        dBxdt(:,my-mbdry+1:my,iz) = 0.
        dBydt(:,my-mbdry+1:my,iz) = 0.
        dBzdt(:,my-mbdry+1:my,iz) = 0.
      end if
    end do
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE regularize (f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
END

!-----------------------------------------------------------------------
SUBROUTINE regularize_face (f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
END
