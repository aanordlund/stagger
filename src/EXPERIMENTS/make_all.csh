#!/bin/csh

set ee = `echo EXPERIMENTS/*mkf | sed -e 's/:.*//' | sort -u | sed -e 's/\.mkf//g' -e 's/EXPERIMENTS.//g'`

cp /dev/null EXPERIMENTS/make_all_ok.log
cp /dev/null EXPERIMENTS/make_all_nok.log
@ ok=0
@ nok=0

foreach e ($ee)
  ( echo "";\
    echo "================================ $e =======================================";\
    make clean;\
    make EXPERIMENT=$e $* FOPT=-O0 PAR= MP= -j ) >& tmp.log
  if ($status) then
    echo "$e NOK"
    cat tmp.log >> EXPERIMENTS/make_all_nok.log
    @ nok++
  else
    echo "$e OK"
    cat tmp.log >> EXPERIMENTS/make_all_ok.log
    @ ok++
  endif
  \rm tmp.log
end
echo ""
echo "$ok OK, $nok NOK"
