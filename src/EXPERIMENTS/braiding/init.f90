! $Id: init.f90,v 1.7 2012/04/22 22:34:29 aake Exp $
!***********************************************************************
MODULE init_m
  implicit none
  character(len=16) test
  real a_braid6
  logical do_init, do_test, do_braid6
END MODULE

!***********************************************************************
FUNCTION input_file()
  USE params
  implicit none
  character(len=mfile) input_file
  input_file='EXPERIMENTS/braiding/input.txt'
END

!***********************************************************************
SUBROUTINE init_values (r,px,py,pz,e,d,Bx,By,Bz)
  USE params
  USE init_m
  USE braiding
  implicit none
  real, dimension(mx,my,mz):: r,px,py,pz,e,d,Bx,By,Bz
  real, dimension(6):: xc,yc,zc,kk,aa,ll 
  integer jb,ix,iy,iz
  real(kind=8) expp, sqt

  do_init=.true.
  do_braid6=.false.
  a_braid6=2.
  do_test=.false.
  test=''
  call read_init
  
  if (do_test) then
    call test_values (test,r,px,py,pz,e,d,Bx,By,Bz)
    return
  end if

  if (.not. do_init) return

  !$omp parallel
  r(:,:,izs:ize)=r0                                                     ! encourage local mem
  e(:,:,izs:ize)=e0
  px(:,:,izs:ize)=0.
  py(:,:,izs:ize)=0.
  pz(:,:,izs:ize)=0.
  if (do_pscalar) d(:,:,izs:ize)=d0
  if (do_mhd) then
   if (do_braid6) then
    do iz=izs,ize
    do iy=1,my
    do ix=1,mx
      bz(ix,iy,iz) = 0.0
      bx(ix,iy,iz) = 0.0
      by(ix,iy,iz) = 1.0	
      sqt = SQRT(2.0d0)
      zc=[1.0d0,-1.0d0,1.0d0,-1.0d0,1.0d0,-1.0d0]
      xc=[0.0d0,0.0d0,0.0d0,0.0d0,0.0d0,0.0d0]
      yc=[-20.0d0,-12.0d0,-4.0d0,4.0d0,12.0d0,20.0d0]
      kk=[1.0d0,-1.0d0,1.0d0,-1.0d0,1.0d0,-1.0d0]
      aa=[sqt,sqt,sqt,sqt,sqt,sqt]
      ll=[2.0d0,2.0d0,2.0d0,2.0d0,2.0d0,2.0d0]	
      DO jb = 1,6
        expp  =  EXP(-(zmdn(iz)-zc(jb))*(zm(iz)-zc(jb))/(aa(jb)*aa(jb)) &
                     -(xm  (ix)-xc(jb))*(xm(ix)-xc(jb))/(aa(jb)*aa(jb)) &
                     -(ym  (iy)-yc(jb))*(ym(iy)-yc(jb))/(ll(jb)*ll(jb)))
        bz(ix,iy,iz) =  bz(ix,iy,iz) - a_braid6*(xm(ix)-xc(jb))*expp*kk(jb)/aa(jb)
        expp  =  EXP(-(zm  (iz)-zc(jb))*(zm(iz)-zc(jb))/(aa(jb)*aa(jb)) &
                     -(xmdn(ix)-xc(jb))*(xm(ix)-xc(jb))/(aa(jb)*aa(jb)) &
                     -(ym  (iy)-yc(jb))*(ym(iy)-yc(jb))/(ll(jb)*ll(jb)))
        bx(ix,iy,iz) =  bx(ix,iy,iz) + a_braid6*(zm(iz)-zc(jb))*expp*kk(jb)/aa(jb)
      END DO
    end do
    end do
    end do
   else
    Bx(:,:,izs:ize) = 0.
    By(:,:,izs:ize) = b0
    Bz(:,:,izs:ize) = 0.
   end if
  end if
  !$omp end parallel
END

!***********************************************************************
SUBROUTINE read_init
  USE params
  USE init_m
  implicit none
  namelist /init/do_init,do_braid6,a_braid6,do_test,test

  if (stdin < 0) then
    read (*,init)
    if (master) write (*,init)
  else
    rewind (stdin); read (stdin,init)
    if (master) write (stdout,init)
  end if
END
