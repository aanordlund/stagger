      MODULE stagger
        implicit none
	integer mx,my,mz
        real dx,dy,dz
        real a,b,c,aa3,bb3,aa1,
     &    adx,bdx,cdx,adx3,bdx3,cdx3,
     &    ady,bdy,cdy,ady3,bdy3,cdy3,
     &    adz,bdz,cdz,adz3,bdz3,cdz3,
     &    adx1,ady1,adz1

      private a,b,c

      CONTAINS
!***********************************************************************
      subroutine init_stagger ()
      implicit none
!
!  Initialize the pshift setups.
!
!
!  Enable interpolation routines, derivatives must await dx,dy,dz in start
!
      c = 3./256.
      b = -25./256.
      a = .5-b-c
      aa3=.5-b
      bb3= b
      aa1=.5
!
      end subroutine
!-----------------------------------------------------------------------
      subroutine init_derivs (mx1,my1,mz1,dx1,dy1,dz1)
      implicit none
      integer mx1,my1,mz1
      real dx1,dy1,dz1
!
!  Initialize the derivertives setups.
!
!-----------------------------------------------------------------------
      call init_stagger ()
      mx = mx1
      my = my1
      mz = mz1
      dx = dx1
      dy = dy1
      dz = dz1
      cdx = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
      bdx = (-1.-120.*cdx)/24.
      adx = (1.-3.*bdx-5.*cdx)/dx
      bdx = bdx/dx
      cdx = cdx/dx
      adx1 = 1./dx
      bdx3 = -1./(24*dx)
      adx3 = adx1-3.*bdx3
      cdy = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
      bdy = (-1.-120.*cdy)/24.
      ady = (1.-3.*bdy-5.*cdy)/dy
      bdy = bdy/dy
      cdy = cdy/dy
      ady1 = 1./dy
      bdy3 = -1./(24*dy)
      ady3 = ady1-3.*bdy3
      cdz = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
      bdz = (-1.-120.*cdz)/24.
      adz = (1.-3.*bdz-5.*cdz)/dz
      bdz = bdz/dz
      cdz = cdz/dz
      adz1 =  1./dz
      bdz3 = -1./(24*dz)
      adz3 =  adz1-3.*bdz3

      end subroutine

!***********************************************************************
      subroutine xup_set (f, fp)
      implicit none
!
!  f is centered on (i-.5,j,k), fifth order stagger
!
      real, DIMENSION(:,:,:):: f, fp
!
!-----------------------------------------------------------------------
!- 5th order inside the domain
        fp(3:mx-3,:,:) =  (
     &                 a*(f(3:mx-3,:,:) + f(4:mx-2,:,:)) +
     &                 b*(f(2:mx-4,:,:) + f(5:mx-1,:,:)) +
     &                 c*(f(1:mx-5,:,:) + f(6:mx  ,:,:)))
!- 3rd order inside the domain
        fp(2   ,:,:) =  (
     &               aa3*(f(2   ,:,:) + f(3   ,:,:)) +
     &               bb3*(f(1   ,:,:) + f(4   ,:,:)))
        fp(mx-2,:,:) =  (
     &               aa3*(f(mx-2,:,:) + f(mx-1,:,:)) +
     &               bb3*(f(mx-3,:,:) + f(mx  ,:,:)))
!- 1st order inside the domain
        fp(1   ,:,:) =  (
     &               aa1*(f(1   ,:,:) + f(2   ,:,:)))
        fp(mx-1,:,:) =  (
     &               aa1*(f(mx-1,:,:) + f(mx  ,:,:)))
!- boundary fill
        fp(mx  ,:,:) = fp(mx-1,:,:)

      end subroutine
!***********************************************************************
      subroutine xdn_set (f, fp)
      implicit none
!
!  f is centered on (i-.5,j,k)
!
      real, DIMENSION(:,:,:):: f, fp
!-----------------------------------------------------------------------
!- 5th order inside domain
        fp(4:mx-3,:,:) =  (
     &                 a*(f(3:mx-4,:,:) + f(4:mx-3,:,:)) +
     &                 b*(f(2:mx-5,:,:) + f(5:mx-2,:,:)) +
     &                 c*(f(1:mx-6,:,:) + f(6:mx-1,:,:)))
!- 3th order inside the domain
        fp(3   ,:,:) =  (
     &               aa3*(f(2   ,:,:) + f(3   ,:,:)) +
     &               bb3*(f(1   ,:,:) + f(4   ,:,:)))
        fp(mx-2,:,:) =  (
     &               aa3*(f(mx-3,:,:) + f(mx-2,:,:)) +
     &               bb3*(f(mx-4,:,:) + f(mx-1,:,:)))
!- 1st order inside the domain
        fp(2   ,:,:) =  (
     &               aa1*(f(1   ,:,:) + f(2   ,:,:)))
        fp(mx-1,:,:) =  (
     &               aa1*(f(mx-2,:,:) + f(mx-1,:,:)))
!- boundary fill
        fp(1   ,:,:) = fp(2   ,:,:)
        fp(mx  ,:,:) = fp(mx-1,:,:)
!
      end subroutine
!***********************************************************************
      subroutine ddxup_set (f, fp)
      implicit none
!
!  X partial derivative
!
      real, DIMENSION(:,:,:):: f, fp
      integer izs,ize
!-----------------------------------------------------------------------
      call limits_omp(1,mz,izs,ize)
!- 5th order inside the domain
        fp(3:mx-3,:,izs:ize) = 
     &              (adx*(-f(3:mx-3,:,izs:ize) + f(4:mx-2,:,izs:ize)) +
     &               bdx*(-f(2:mx-4,:,izs:ize) + f(5:mx-1,:,izs:ize)) +
     &               cdx*(-f(1:mx-5,:,izs:ize) + f(6:mx  ,:,izs:ize)))
!- 3rd order inside the domain
        fp(2   ,:,izs:ize) =  
     &              (adx3*(-f(2   ,:,izs:ize) + f(3   ,:,izs:ize)) +
     &               bdx3*(-f(1   ,:,izs:ize) + f(4   ,:,izs:ize)))
        fp(mx-2,:,izs:ize) =  
     &              (adx3*(-f(mx-2,:,izs:ize) + f(mx-1,:,izs:ize)) +
     &               bdx3*(-f(mx-3,:,izs:ize) + f(mx  ,:,izs:ize)))
!- 1th order inside the domain
        fp(1   ,:,izs:ize) =  
     &              (adx1*(-f(1   ,:,izs:ize) + f(2   ,:,izs:ize)))
        fp(mx-1,:,izs:ize) =  
     &              (adx1*(-f(mx-1,:,izs:ize) + f(mx  ,:,izs:ize)))
!- boundary fill
        fp(mx,:,izs:ize) = fp(mx-1,:,izs:ize)
!-
      end subroutine
!***********************************************************************
      subroutine ddxdn_set (f, fp)
      implicit none
!
!  X partial derivative
!
      real, DIMENSION(:,:,:):: f, fp
      integer izs,ize
!-----------------------------------------------------------------------
      call limits_omp(1,mz,izs,ize)
!- 5th order inside domain
        fp(4:mx-3,:,izs:ize) =  
     &              (adx*(-f(3:mx-4,:,izs:ize) + f(4:mx-3,:,izs:ize)) +
     &               bdx*(-f(2:mx-5,:,izs:ize) + f(5:mx-2,:,izs:ize)) +
     &               cdx*(-f(1:mx-6,:,izs:ize) + f(6:mx-1,:,izs:ize)))
!- 3th order inside the domain
        fp(3   ,:,izs:ize) = 
     &              (adx3*(-f(2   ,:,izs:ize) + f(3   ,:,izs:ize)) +
     &               bdx3*(-f(1   ,:,izs:ize) + f(4   ,:,izs:ize)))
        fp(mx-2,:,izs:ize) =  
     &              (adx3*(-f(mx-3,:,izs:ize) + f(mx-2,:,izs:ize)) +
     &               bdx3*(-f(mx-4,:,izs:ize) + f(mx-1,:,izs:ize)))
!- 1st order inside the domain
        fp(2   ,:,izs:ize) =  
     &              (adx1*(-f(1   ,:,izs:ize) + f(2   ,:,izs:ize)))
        fp(mx-1,:,izs:ize) =  
     &              (adx1*(-f(mx-2,:,izs:ize) + f(mx-1,:,izs:ize)))
!- boundary fill
        fp(1   ,:,izs:ize) = fp(2   ,:,izs:ize)
        fp(mx  ,:,izs:ize) = fp(mx-1,:,izs:ize)
!-
      end subroutine
!***********************************************************************
      subroutine xup1_set (f, fp)
      implicit none
!
!  f is centered on (i-.5,j,k)
!
      real, DIMENSION(:,:,:):: f, fp
!-----------------------------------------------------------------------
!      fp =  0.5*(f + cshift(f, dim=1, shift=1))
      fp(1:mx-1,:,:) =  0.5*(f(2:mx,:,:)+f(1:mx-1,:,:))
      fp(mx,:,:) = fp(mx-1,:,:)
      end subroutine
!***********************************************************************
      subroutine xdn1_set (f, fp)
      implicit none
!
!  f is centered on (i-.5,j,k)
!
      real, DIMENSION(:,:,:):: f, fp
!-----------------------------------------------------------------------
!      fp =  0.5*(f + cshift(f, dim=1, shift=-1))
!      fp(1,:,:) = 2.*f(1,:,:)-fp(2,:,:)
!      fp(mx,:,:) = 2.*f(mx-1,:,:)-fp(mx-1,:,:)
      fp(2:mx-1,:,:) =  0.5*(f(2:mx-1,:,:)+f(1:mx-2,:,:))
      fp(1,:,:) = fp(2,:,:)
      fp(mx,:,:) = fp(mx-1,:,:)
      end subroutine
!***********************************************************************
      subroutine ddxdn1_set (f, fp)
      implicit none
!
!  f is centered on (i-.5,j,k)
!
      real, DIMENSION(:,:,:):: f, fp
!-----------------------------------------------------------------------
!      fp =  1./dx*(f - cshift(f, dim=1, shift=-1))
      fp(2:mx-1,:,:) =  (1./dx)*(f(2:mx-1,:,:)-f(1:mx-2,:,:))
      fp(1,:,:) = fp(2,:,:)
      fp(mx,:,:) = fp(mx-1,:,:)
      end subroutine
!***********************************************************************
      subroutine ddxup1_set (f, fp)
      implicit none
!
!  f is centered on (i-.5,j,k)
!
      real, DIMENSION(:,:,:):: f, fp
!-----------------------------------------------------------------------
!      fp = 1./dx*(cshift(f, dim=1, shift=1) - f)
      fp(1:mx-1,:,:) =  (1./dx)*(f(2:mx,:,:)-f(1:mx-1,:,:))
      fp(mx,:,:) = fp(mx-1,:,:)
      end subroutine

!***********************************************************************
      subroutine yup_set (f, fp)
      implicit none
!
!  f is centered on (i,j-.5,k)
!
      real, DIMENSION(:,:,:):: f, fp

      fp(:,3:my-3,:) =  (
     &   a*(f(:,3:my-3,:) + f(:,4:my-2,:)) +
     &   b*(f(:,2:my-4,:) + f(:,5:my-1,:)) +
     &   c*(f(:,1:my-5,:) + f(:,6:my  ,:)))

        fp(:,my-2,:) =  (
     &     a*(f(:,my-2,:) + f(:,my-1,:)) +
     &     b*(f(:,my-3,:) + f(:,my  ,:)) +
     &     c*(f(:,my-4,:) + f(:,1       ,:)))
        fp(:,my-1,:) =  (
     &     a*(f(:,my-1,:) + f(:,my  ,:)) +
     &     b*(f(:,my-2,:) + f(:,1       ,:)) +
     &     c*(f(:,my-3,:) + f(:,2       ,:)))
        fp(:,my  ,:) =  (
     &     a*(f(:,my  ,:) + f(:,1       ,:)) +
     &     b*(f(:,my-1,:) + f(:,2       ,:)) +
     &     c*(f(:,my-2,:) + f(:,3       ,:)))
        fp(:,1   ,:) =  (
     &     a*(f(:,1       ,:) + f(:,2       ,:)) +
     &     b*(f(:,my  ,:) + f(:,3       ,:)) +
     &     c*(f(:,my-1,:) + f(:,4       ,:)))
        fp(:,2   ,:) =  (
     &     a*(f(:,2       ,:) + f(:,3       ,:)) +
     &     b*(f(:,1       ,:) + f(:,4       ,:)) +
     &     c*(f(:,my  ,:) + f(:,5       ,:)))

!-
      end subroutine
!***********************************************************************
      subroutine ydn_set (f, fp)
      implicit none
!
!  f is centered on (i,j,k-.5)
!
      real, DIMENSION(:,:,:):: f, fp
!-----------------------------------------------------------------------

      fp(:,4:my-2,:) =  (
     &   a*(f(:,3:my-3,:) + f(:,4:my-2,:)) +
     &   b*(f(:,2:my-4,:) + f(:,5:my-1,:)) +
     &   c*(f(:,1:my-5,:) + f(:,6:my  ,:)))

! one processor in the y direction
        fp(:,my-1,:) =  (
     &     a*(f(:,my-2,:) + f(:,my-1,:)) +
     &     b*(f(:,my-3,:) + f(:,my  ,:)) +
     &     c*(f(:,my-4,:) + f(:,1       ,:)))
        fp(:,my  ,:) =  (
     &     a*(f(:,my-1,:) + f(:,my  ,:)) +
     &     b*(f(:,my-2,:) + f(:,1       ,:)) +
     &     c*(f(:,my-3,:) + f(:,2       ,:)))
        fp(:,1   ,:) =  (
     &     a*(f(:,my  ,:) + f(:,1       ,:)) +
     &     b*(f(:,my-1,:) + f(:,2       ,:)) +
     &     c*(f(:,my-2,:) + f(:,3       ,:)))
        fp(:,2   ,:) =  (
     &     a*(f(:,1       ,:) + f(:,2       ,:)) +
     &     b*(f(:,my  ,:) + f(:,3       ,:)) +
     &     c*(f(:,my-1,:) + f(:,4       ,:)))
        fp(:,3   ,:) =  (
     &     a*(f(:,2       ,:) + f(:,3       ,:)) +
     &     b*(f(:,1       ,:) + f(:,4       ,:)) +
     &     c*(f(:,my  ,:) + f(:,5       ,:)))

!-
      end subroutine
!***********************************************************************
      subroutine ddyup_set (f, fp)
      implicit none
!
!  Y partial derivative
!
      real, DIMENSION(:,:,:):: f, fp
      integer izs,ize
!-----------------------------------------------------------------------
      call limits_omp(1,mz,izs,ize)

      fp(:,3:my-3,izs:ize) =  
     &    (ady*(- f(:,3:my-3,izs:ize) + f(:,4:my-2,izs:ize)) +
     &     bdy*(- f(:,2:my-4,izs:ize) + f(:,5:my-1,izs:ize)) +
     &     cdy*(- f(:,1:my-5,izs:ize) + f(:,6:my  ,izs:ize)))

! one cpu in the y direction
        fp(:,my-2,izs:ize) =  
     &      (ady*(- f(:,my-2,izs:ize) + f(:,my-1,izs:ize)) +
     &       bdy*(- f(:,my-3,izs:ize) + f(:,my  ,izs:ize)) +
     &       cdy*(- f(:,my-4,izs:ize) + f(:,1       ,izs:ize)))
        fp(:,my-1,izs:ize) =  
     &      (ady*(- f(:,my-1,izs:ize) + f(:,my  ,izs:ize)) +
     &       bdy*(- f(:,my-2,izs:ize) + f(:,1       ,izs:ize)) +
     &       cdy*(- f(:,my-3,izs:ize) + f(:,2       ,izs:ize)))
        fp(:,my  ,izs:ize) =  
     &      (ady*(- f(:,my  ,izs:ize) + f(:,1       ,izs:ize)) +
     &       bdy*(- f(:,my-1,izs:ize) + f(:,2       ,izs:ize)) +
     &       cdy*(- f(:,my-2,izs:ize) + f(:,3       ,izs:ize)))
        fp(:,1   ,izs:ize) =  
     &      (ady*(- f(:,1       ,izs:ize) + f(:,2       ,izs:ize)) +
     &       bdy*(- f(:,my  ,izs:ize) + f(:,3       ,izs:ize)) +
     &       cdy*(- f(:,my-1,izs:ize) + f(:,4       ,izs:ize)))
        fp(:,2   ,izs:ize) =  
     &      (ady*(- f(:,2       ,izs:ize) + f(:,3       ,izs:ize)) +
     &       bdy*(- f(:,1       ,izs:ize) + f(:,4       ,izs:ize)) +
     &       cdy*(- f(:,my  ,izs:ize) + f(:,5       ,izs:ize)))

!-
      end subroutine
!***********************************************************************
      subroutine ddydn_set (f, fp)
      implicit none
!
!  Y partial derivative
!
      real, DIMENSION(:,:,:):: f, fp
      integer izs,ize
!-----------------------------------------------------------------------
      call limits_omp(1,mz,izs,ize)

      fp(:,4:my-2,izs:ize) =  
     &    (ady*(- f(:,3:my-3,izs:ize) + f(:,4:my-2,izs:ize)) +
     &     bdy*(- f(:,2:my-4,izs:ize) + f(:,5:my-1,izs:ize)) +
     &     cdy*(- f(:,1:my-5,izs:ize) + f(:,6:my  ,izs:ize)))

! one cpu in the y direction
        fp(:,my-1,izs:ize) =  
     &      (ady*(- f(:,my-2,izs:ize) + f(:,my-1,izs:ize)) +
     &       bdy*(- f(:,my-3,izs:ize) + f(:,my  ,izs:ize)) +
     &       cdy*(- f(:,my-4,izs:ize) + f(:,1       ,izs:ize)))
        fp(:,my  ,izs:ize) =  
     &      (ady*(- f(:,my-1,izs:ize) + f(:,my  ,izs:ize)) +
     &       bdy*(- f(:,my-2,izs:ize) + f(:,1       ,izs:ize)) +
     &       cdy*(- f(:,my-3,izs:ize) + f(:,2       ,izs:ize)))
        fp(:,1   ,izs:ize) =  
     &      (ady*(- f(:,my  ,izs:ize) + f(:,1       ,izs:ize)) +
     &       bdy*(- f(:,my-1,izs:ize) + f(:,2       ,izs:ize)) +
     &       cdy*(- f(:,my-2,izs:ize) + f(:,3       ,izs:ize)))
        fp(:,2   ,izs:ize) =  
     &      (ady*(- f(:,1       ,izs:ize) + f(:,2       ,izs:ize)) +
     &       bdy*(- f(:,my  ,izs:ize) + f(:,3       ,izs:ize)) +
     &       cdy*(- f(:,my-1,izs:ize) + f(:,4       ,izs:ize)))
        fp(:,3   ,izs:ize) =  
     &      (ady*(- f(:,2       ,izs:ize) + f(:,3       ,izs:ize)) +
     &       bdy*(- f(:,1       ,izs:ize) + f(:,4       ,izs:ize)) +
     &       cdy*(- f(:,my  ,izs:ize) + f(:,5       ,izs:ize)))

!-
      end subroutine
!***********************************************************************
      subroutine ydn1_set (f, fp)
      implicit none
!
!  f is centered on (i,j,k-.5)
!
      real, DIMENSION(:,:,:):: f, fp
!-----------------------------------------------------------------------

      fp(:,2:my,:) =  
     &     0.5*(f(:,1:my-1,:) 
     &        + f(:,2:my,:))
      fp(:,1,:) =  
     &      0.5*(f(:,my,:) + f(:,1,:))
!-
      end subroutine
!***********************************************************************
      subroutine yup1_set (f, fp)
      implicit none
!
!  f is centered on (i,j,k-.5)
!
      real, DIMENSION(:,:,:):: f, fp
!-----------------------------------------------------------------------

      fp(:,1:my-1,:) =  
     &      0.5*(f(:,1:my-1,:) 
     &         + f(:,2:my,:))
      fp(:,my,:) =  
     &      0.5*(f(:,my,:) + f(:,1,:))
!-
      end subroutine
!***********************************************************************
      subroutine ddydn1_set (f, fp)
      implicit none
!
!  f is centered on (i,j,k-.5)
!
      real, DIMENSION(:,:,:):: f, fp
!-----------------------------------------------------------------------

      fp(:,2:my,:) =  
     &      1./dy*(-f(:,1:my-1,:) 
     &           +  f(:,2:my,:))
      fp(:,1,:) =  
     &      1./dy*(-f(:,my,:) + f(:,1,:))
!-
      end subroutine
!***********************************************************************
      subroutine ddyup1_set (f, fp)
      implicit none
!
!  f is centered on (i,j,k-.5)
!
      real, DIMENSION(:,:,:):: f, fp
!-----------------------------------------------------------------------

      fp(:,1:my-1,:) =  
     &      1./dy*(-f(:,1:my-1,:) 
     &           +  f(:,2:my,:))
      fp(:,my,:) =  
     &      1./dy*(-f(:,my,:) +  f(:,1,:))
!-
      end subroutine

!***********************************************************************
      subroutine zup_set (f, fp)
      implicit none
!
!  f is centered on (i,j,k-.5)
!
      real, DIMENSION(:,:,:):: f, fp
!-----------------------------------------------------------------------

      fp(:,:,3:mz-3) =  (
     &   a*(f(:,:,3:mz-3) + f(:,:,4:mz-2)) +
     &   b*(f(:,:,2:mz-4) + f(:,:,5:mz-1)) +
     &   c*(f(:,:,1:mz-5) + f(:,:,6:mz  )))

! only one processor in the z direction
        fp(:,:,mz-2) =  (
     &     a*(f(:,:,mz-2) + f(:,:,mz-1)) +
     &     b*(f(:,:,mz-3) + f(:,:,mz  )) +
     &     c*(f(:,:,mz-4) + f(:,:,1   )))
        fp(:,:,mz-1) =  (
     &     a*(f(:,:,mz-1) + f(:,:,mz  )) +
     &     b*(f(:,:,mz-2) + f(:,:,1   )) +
     &     c*(f(:,:,mz-3) + f(:,:,2   )))
        fp(:,:,mz  ) =  (
     &     a*(f(:,:,mz  ) + f(:,:,1   )) +
     &     b*(f(:,:,mz-1) + f(:,:,2   )) +
     &     c*(f(:,:,mz-2) + f(:,:,3   )))
        fp(:,:,1   ) =   (
     &     a*(f(:,:,1       ) + f(:,:,2   )) +
     &     b*(f(:,:,mz  ) + f(:,:,3   )) +
     &     c*(f(:,:,mz-1) + f(:,:,4   )))
        fp(:,:,2   ) =   (
     &     a*(f(:,:,2       ) + f(:,:,3   )) +
     &     b*(f(:,:,1       ) + f(:,:,4   )) +
     &     c*(f(:,:,mz  ) + f(:,:,5   )))
!-
      end subroutine
!***********************************************************************
      subroutine zdn_set (f, fp)
      implicit none
!
!  f is centered on (i,j,k-.5)
!
      real, DIMENSION(:,:,:):: f, fp
!-----------------------------------------------------------------------
      fp(:,:,4:mz-2) =   (
     &   a*(f(:,:,3:mz-3) + f(:,:,4:mz-2)) +
     &   b*(f(:,:,2:mz-4) + f(:,:,5:mz-1)) +
     &   c*(f(:,:,1:mz-5) + f(:,:,6:mz  )))

        fp(:,:,mz-1) =   (
     &     a*(f(:,:,mz-2) + f(:,:,mz-1)) +
     &     b*(f(:,:,mz-3) + f(:,:,mz  )) +
     &     c*(f(:,:,mz-4) + f(:,:,1   )))
        fp(:,:,mz  ) =   (
     &     a*(f(:,:,mz-1) + f(:,:,mz  )) +
     &     b*(f(:,:,mz-2) + f(:,:,1   )) +
     &     c*(f(:,:,mz-3) + f(:,:,2   )))
        fp(:,:,1   ) =   (
     &     a*(f(:,:,mz  ) + f(:,:,1   )) +
     &     b*(f(:,:,mz-1) + f(:,:,2   )) +
     &     c*(f(:,:,mz-2) + f(:,:,3   )))
        fp(:,:,2   ) =   (
     &     a*(f(:,:,1       ) + f(:,:,2   )) +
     &     b*(f(:,:,mz  ) + f(:,:,3   )) +
     &     c*(f(:,:,mz-1) + f(:,:,4   )))
        fp(:,:,3   ) =   (
     &     a*(f(:,:,2     ) + f(:,:,3   )) +
     &     b*(f(:,:,1     ) + f(:,:,4   )) +
     &     c*(f(:,:,mz) + f(:,:,5   )))

!-
      end subroutine
!***********************************************************************
      subroutine ddzup_set (f, fp)
      implicit none
!
!  Z partial derivative
!
      real, DIMENSION(:,:,:):: f, fp
      integer iys,iye
!-----------------------------------------------------------------------
      call limits_omp(1,my,iys,iye)

      fp(:,iys:iye,3:mz-3) =   
     &    (adz*(- f(:,iys:iye,3:mz-3) + f(:,iys:iye,4:mz-2)) +
     &     bdz*(- f(:,iys:iye,2:mz-4) + f(:,iys:iye,5:mz-1)) +
     &     cdz*(- f(:,iys:iye,1:mz-5) + f(:,iys:iye,6:mz  )))

! only one processor in the z direction
        fp(:,iys:iye,mz-2) =  
     &      (adz*(- f(:,iys:iye,mz-2) + f(:,iys:iye,mz-1)) +
     &       bdz*(- f(:,iys:iye,mz-3) + f(:,iys:iye,mz  )) +
     &       cdz*(- f(:,iys:iye,mz-4) + f(:,iys:iye,1   )))
        fp(:,iys:iye,mz-1) =  
     &      (adz*(- f(:,iys:iye,mz-1) + f(:,iys:iye,mz  )) +
     &       bdz*(- f(:,iys:iye,mz-2) + f(:,iys:iye,1   )) +
     &       cdz*(- f(:,iys:iye,mz-3) + f(:,iys:iye,2   )))
        fp(:,iys:iye,mz  ) =  
     &      (adz*(- f(:,iys:iye,mz  ) + f(:,iys:iye,1   )) +
     &       bdz*(- f(:,iys:iye,mz-1) + f(:,iys:iye,2   )) +
     &       cdz*(- f(:,iys:iye,mz-2) + f(:,iys:iye,3   )))
        fp(:,iys:iye,1   ) =  
     &      (adz*(- f(:,iys:iye,1   )     + f(:,iys:iye,2   )) +
     &       bdz*(- f(:,iys:iye,mz)   + f(:,iys:iye,3   )) +
     &       cdz*(- f(:,iys:iye,mz-1) + f(:,iys:iye,4   )))
        fp(:,iys:iye,2   ) =  
     &      (adz*(- f(:,iys:iye,2   )   + f(:,iys:iye,3   )) +
     &       bdz*(- f(:,iys:iye,1   )   + f(:,iys:iye,4   )) +
     &       cdz*(- f(:,iys:iye,mz) + f(:,iys:iye,5   )))

!-
      end subroutine
!***********************************************************************
      subroutine ddzdn_set (f, fp)
      implicit none
!
!  Z partial derivative
!
      real, DIMENSION(:,:,:):: f, fp
      integer iys,iye
!-----------------------------------------------------------------------
      call limits_omp(1,my,iys,iye)

      fp(:,iys:iye,4:mz-2) =   
     &    (adz*(- f(:,iys:iye,3:mz-3) + f(:,iys:iye,4:mz-2)) +
     &     bdz*(- f(:,iys:iye,2:mz-4) + f(:,iys:iye,5:mz-1)) +
     &     cdz*(- f(:,iys:iye,1:mz-5) + f(:,iys:iye,6:mz  )))

        fp(:,iys:iye,mz-1) =  
     &      (adz*(- f(:,iys:iye,mz-2) + f(:,iys:iye,mz-1)) +
     &       bdz*(- f(:,iys:iye,mz-3) + f(:,iys:iye,mz  )) +
     &       cdz*(- f(:,iys:iye,mz-4) + f(:,iys:iye,1   )))
        fp(:,iys:iye,mz  ) =  
     &      (adz*(- f(:,iys:iye,mz-1) + f(:,iys:iye,mz  )) +
     &       bdz*(- f(:,iys:iye,mz-2) + f(:,iys:iye,1   )) +
     &       cdz*(- f(:,iys:iye,mz-3) + f(:,iys:iye,2   )))
        fp(:,iys:iye,1   ) =  
     &      (adz*(- f(:,iys:iye,mz  ) + f(:,iys:iye,1   )) +
     &       bdz*(- f(:,iys:iye,mz-1) + f(:,iys:iye,2   )) +
     &       cdz*(- f(:,iys:iye,mz-2) + f(:,iys:iye,3   )))
        fp(:,iys:iye,2   ) =  
     &      (adz*(- f(:,iys:iye,1       ) + f(:,iys:iye,2   )) +
     &       bdz*(- f(:,iys:iye,mz  ) + f(:,iys:iye,3   )) +
     &       cdz*(- f(:,iys:iye,mz-1) + f(:,iys:iye,4   )))
        fp(:,iys:iye,3   ) =  
     &      (adz*(- f(:,iys:iye,2     ) + f(:,iys:iye,3   )) +
     &       bdz*(- f(:,iys:iye,1     ) + f(:,iys:iye,4   )) +
     &       cdz*(- f(:,iys:iye,mz) + f(:,iys:iye,5   )))

!-
      end subroutine
!***********************************************************************
      subroutine zup1_set (f, fp)
      implicit none
!
!  f is centered on (i,j,k-.5)
!
      real, DIMENSION(:,:,:):: f, fp
!-----------------------------------------------------------------------

      fp(:,:,1:mz-1) =  
     &    0.5*(f(:,:,1:mz-1) + f(:,:,2:mz)) 
       fp(:,:,mz) =  
     &       0.5*(f(:,:,mz) + f(:,:,1)) 

      end subroutine
!***********************************************************************
      subroutine zdn1_set (f, fp)
      implicit none
!
!  f is centered on (i,j,k-.5)
!
      real, DIMENSION(:,:,:):: f, fp
!-----------------------------------------------------------------------
      fp(:,:,2:mz) =  
     &    0.5*(f(:,:,1:mz-1) + f(:,:,2:mz)) 
      fp(:,:,1) =  
     &       0.5*(f(:,:,mz) + f(:,:,1))
!-
      end subroutine
!***********************************************************************
      subroutine ddzup1_set (f, fp)
      implicit none
!
!  f is centered on (i,j,k-.5)
!
      real, DIMENSION(:,:,:):: f, fp
!-----------------------------------------------------------------------

      fp(:,:,1:mz-1) = 
     &      1./dz*(-f(:,:,1:mz-1) + f(:,:,2:mz))

         fp(:,:,mz) = 
     &         1./dz*(-f(:,:,mz) + f(:,:,1))
!-
      end subroutine
!***********************************************************************
      subroutine ddzdn1_set (f, fp)
      implicit none
!
!  f is centered on (i,j,k-.5)
!
      real, DIMENSION(:,:,:):: f, fp
!-----------------------------------------------------------------------

      fp(:,:,2:mz) =   
     &      1./dz*(-f(:,:,1:mz-1) + f(:,:,2:mz))
      fp(:,:,1) =   
     &        1./dz*(-f(:,:,mz) + f(:,:,1))
!-
      end subroutine
!***********************************************************************
      END MODULE
