!$Id: divb_clean_kg.f90,v 1.4 2016/11/09 20:31:30 aake Exp $
!***********************************************************************
REAL FUNCTION wallclock()
  implicit none
  integer, save:: count, count_rate=0, count_max
  real, save:: previous=0., offset=0.

  if (count_rate == 0) then
    call system_clock(count=count, count_rate=count_rate, count_max=count_max)
    offset = -count/real(count_rate)
  else
    call system_clock(count=count)
  end if
  wallclock = count/real(count_rate) + offset
  if (wallclock < previous) then
    offset = offset + real(count_max)/real(count_rate)
    wallclock = count/real(count_rate) + offset
  end if
  wallclock = wallclock-previous
  previous = wallclock+previous
END

!***********************************************************************
SUBROUTINE limits_omp (n1,n2,i1,i2)
  implicit none
  integer n1,n2,i1,i2
  integer omp_get_num_threads, omp_get_thread_num, omp_mythread, omp_nthreads

#ifdef _OPENMP
  omp_mythread = omp_get_thread_num()
  omp_nthreads = omp_get_num_threads()
  i1 = n1 + ((omp_mythread  )*(n2-n1+1))/omp_nthreads
  i2 = n1 + ((omp_mythread+1)*(n2-n1+1))/omp_nthreads - 1
  !if (omp_mythread==0) print *,'limits_omp:',omp_mythread,n2,i1,i2
#else
  omp_mythread = 0
  i1 = n1
  i2 = n2
  print *,'limits_omp: scalar'
#endif
END

!***********************************************************************
PROGRAM divb_clean_x
  USE stagger
  implicit none
  real, parameter:: pi=3.14159265
  real, allocatable, dimension(:,:,:):: bx,by,bz,divb,dphit,scr
  real, allocatable, dimension(:,:):: w,k2,tmp1,tmp2
  real, allocatable, dimension(:):: b,brms
  integer mvar,isnap,izs,ize,lb,ub,iys,iye
  integer iter,niter,ibx,ix,iy,iz,io_word,verbose,commit
  real divmin,divmax,eps,wt,wallclock,c,over,f,bnorm
  logical doit
  character(len=1) answer
  character(len=40) file
  namelist /in/file,mvar,lb,ub,ibx,isnap,niter,eps,mx,my,mz, &
    dx,dy,dz,f,verbose,commit

  file='snapshot.dat'                                                   ! snapshot file
  mvar=8                                                                ! fields per snapshot
  ibx=5                                                                 ! Bx index in file
  isnap=0                                                               ! snapshot number

  niter=10                                                              ! max iterations
  eps=1e-4                                                              ! accuracy request
  lb = 1
  ub = lb
  dx = 1.
  dy = 5.*128./256.
  dz = 1.
  f = 1.0
  verbose = 0
  commit = 0                                                            ! ask

  write (*,in)
  read (*,in)
  write (*,in)

  ub = mx-ub

  print'(1x,a,f8.2,a)','Memory needed:',6.*4.*real(mx)*real(my)*real(mz)/1024.**3,' GB'

  allocate (bx(mx,my,mz), by(mx,my,mz), bz(mx,my,mz))
  allocate (divb(mx,my,mz), dphit(mx,my,mz), scr(mx,my,mz))
  allocate (k2(mx,mz), tmp1(mx,mz), tmp2(mx,mz), brms(mz))

  open (1,file=file,access='direct',status='unknown',recl=io_word()*mx*my*mz)

  wt=wallclock()
  print *,'reading bx'
  read (1,rec=mvar*isnap+ibx+1) bx
  print *,'reading by'
  read (1,rec=mvar*isnap+ibx+2) by
  print *,'reading bz'
  read (1,rec=mvar*isnap+ibx+3) bz
  print *,wallclock(),' sec'

  call init_derivs(mx,my,mz,dx,dy,dz)

  !$omp parallel private(iz)
  call limits_omp(1,mz,izs,ize)
  do iz=izs,ize
    dphit(:,:,iz) = 0.
    brms(iz) = sum(bx(:,:,iz)**2+by(:,:,iz)**2+bz(:,:,iz)**2)
  end do
  !$omp end parallel
  bnorm=sqrt(sum(brms)/(real(mx)*real(my)*real(mz)))
  print*,'bnorm =',bnorm

  do iter=1,niter
    if (verbose > 0) print*,'loop start',iter
    !$omp parallel private (ix,iy,iz,izs,ize)
    call ddxup_set (bx, scr)
    call limits_omp(1,mz,izs,ize)
    do iz=izs,ize
      divb(:,lb+1:ub-1,iz) = scr(:,lb+1:ub-1,iz)
    end do
    call ddyup_set (by, scr)
    do iz=izs,ize
      do iy=lb+1,ub-1
      do ix=1,mx
        divb(ix,iy,iz) = divb(ix,iy,iz) + scr(ix,iy,iz)
      end do
      end do
    end do
    !$omp barrier
    call ddzup_set (bz, scr)
    !$omp barrier
    do iz=izs,ize
      divb(:,lb+1:ub-1,iz) = divb(:,lb+1:ub-1,iz) + scr(:,lb+1:ub-1,iz)
    end do
    !$omp end parallel
    if (verbose > 0) print*,'divb',wallclock()

    divmax = maxval(abs(divb))
    divmax = divmax*dx/bnorm
    if (verbose > 0) print*,'maxval',wallclock()

    wt=wallclock()
    print '(i3,1p,1e15.6,0p,f10.2,a)',iter,divmax,wt,' sec'
    if (divmax < eps) exit

    !$omp parallel private(iy,iys,iye)
    call limits_omp(lb+1,ub-1,iys,iye)
    do iy=iys,iye
      tmp1 = divb(:,iy,:)
      call fft2df (tmp1,tmp2,mx,mz)
      dphit(:,iy,:) = tmp2
    end do
    !$omp end parallel
    if (verbose > 0) print*,'fft2df',wallclock()

    call fft2d_k2(k2,dx*mx,dz*mz,mx,mz)
    if (verbose > 0) print*,'fft2df_k2',wallclock()

    !$omp parallel private(ix,iy,iz,w,b,izs,ize)
    allocate (w(my,3),b(my))
    w = 0.
    b = 0.
    call limits_omp(1,mz,izs,ize)
    do iz=izs,ize                                                       ! loop over all ..
    do ix=1,mx                                                          ! .. Fourier components

      w(lb,2) = 1.
      do iy=lb+1,ub-1                                                   ! tridiagonal matrix elements
        w(iy,1) = 1./dx**2
        w(iy,3) = w(iy,1)
        w(iy,2) = -w(iy,1)-w(iy,3)-k2(ix,iz)
        b(iy) = dphit(ix,iy,iz)                                         ! right hand side; FT of div(B)
      end do
      w(ub,2) = 1.

      do iy=lb+1,ub-1                                                   ! forward loop
        w(iy,1) = -w(iy,1)/w(iy-1,2)
        w(iy,2) = w(iy,2) + w(iy,1)*w(iy-1,3)                           ! eliminate sub-diagonal
        b(iy)   = b(iy)   + w(iy,1)*b(iy-1)                             ! accumulate RHS
      end do

      do iy=ub-1,lb+1,-1                                                ! reverse loop
        b(iy) = (b(iy)-w(iy,3)*b(iy+1))/w(iy,2)                         ! back substitute
        dphit(iy,iy,iz) = b(iy)                                         ! FT of phi such that Laplace(phi)=div(B)
      end do
    end do
    end do
    deallocate (w,b)
    !$omp end parallel
    if (verbose > 0) print*,'solve',wallclock()

    !$omp parallel private(iy,iys,iye)
    call limits_omp(lb+1,ub-1,iys,iye)
    do iy=iys,iye
      tmp1 = dphit(:,iy,:)
      call fft2db (tmp1,tmp2,mx,mz)
      divb(:,iy,:) = tmp2
    end do
    !$omp end parallel
    if (verbose > 0) print*,'fft2db',wallclock()

    !$omp parallel private (ix,iy,iz,izs,ize)
    call ddxdn_set (divb, scr)
    call limits_omp(1,mz,izs,ize)
    do iz=izs,ize
      bx(:,lb+1:ub-1,iz) = bx(:,lb+1:ub-1,iz) - scr(:,lb+1:ub-1,iz)*f
    end do
    call ddydn_set (divb, scr)
    do iz=izs,ize
      do iy=lb+1,ub
      do ix=1,mx
        by(ix,iy,iz) = by(ix,iy,iz) - scr(ix,iy,iz)*f
      end do
      end do
    end do
    !$omp barrier
    call ddzdn_set (divb, scr)
    !$omp barrier
    do iz=izs,ize
      bz(:,lb+1:ub-1,iz) = bz(:,lb+1:ub-1,iz) - scr(:,lb+1:ub-1,iz)*f
    end do
    !$omp end parallel
    if (verbose > 0) print*,'correct',wallclock()

  end do

  doit = .false.
  if (commit == 1) then
    doit = .true.
  else if (commit == 0) then
    answer = "y"
    print *,'commit to file (y)?'
    read *,answer
    if (answer == "y") doit = .true.
  end if

  if (doit) then
    print *,'writing bx'
    close (1)
    open (1,file=file,access='direct',status='unknown',recl=io_word()*mx*my)
    do iz=1,mz
      write (1,rec=(mvar*isnap+ibx+0)*mz+iz) bx(:,:,iz)
    end do
    print *,'writing by'
    do iz=1,mz
      write (1,rec=(mvar*isnap+ibx+1)*mz+iz) by(:,:,iz)
    end do
    print *,'writing bz'
    do iz=1,mz
      write (1,rec=(mvar*isnap+ibx+2)*mz+iz) bz(:,:,iz)
    end do
    print *,'commmitted'
  else
    print *,'NOT commmitted'
    stop
  end if
  print *,wallclock(),' sec'

  close (1)
  deallocate (bx,by,bz,divb,dphit,k2,brms)
END

