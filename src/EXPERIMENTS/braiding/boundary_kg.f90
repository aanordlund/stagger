! $Id: boundary_kg.f90,v 1.5 2008/09/02 23:36:03 aake Exp $

MODULE braiding
  USE params
  implicit none
  real drive, t_rise, t_drive
  real, allocatable, dimension(:,:):: fbdry, Ux_yl, Uz_yl, Ux_yh, Uz_yh

  integer ifirst, i
  real t_event, ampl, j0, k0, t_prv, amean, t_delta
  data ifirst/1/, t_event/-1e-20/, t_prv/-1./

CONTAINS

!************************************************************************
REAL FUNCTION ran1(idum)
  INTEGER idum,IA,IM,IQ,IR,NTAB,NDIV
  REAL AM,EPS,RNMX
  PARAMETER (IA=16807,IM=2147483647,AM=1./IM,IQ=127773,IR=2836, &
    NTAB=32,NDIV=1+(IM-1)/NTAB,EPS=1.2e-7,RNMX=1.-EPS)
  INTEGER j,k,iv(NTAB),iy
  SAVE iv,iy
  DATA iv /NTAB*0/, iy /0/
  if (idum.le.0.or.iy.eq.0) then
    idum=max(-idum,1)
    do j=NTAB+8,1,-1
      k=idum/IQ
      idum=IA*(idum-k*IQ)-IR*k
      if (idum.lt.0) idum=idum+IM
      if (j.le.NTAB) iv(j)=idum
    end do
    iy=iv(1)
  endif
  k=idum/IQ
  idum=IA*(idum-k*IQ)-IR*k
  if (idum.lt.0) idum=idum+IM
  j=1+iy/NDIV
  iy=iv(j)
  iv(j)=idum
  ran1=min(AM*iy,RNMX)
  return
END FUNCTION

!  (C) Copr. 1986-92 Numerical Recipes Software #10KQ'K5.){2p491"n26.
!****************************************************************
REAL FUNCTION gauss(A,S,ISEED)
!****************************************************************
!
!  GENERATES NORMAL (GAUSSIAN) PSEUDO-RANDOM DEVIATES
!  WITH EXPECTATION (AVERAGE) = A, AND STANDARD DEV. = S, SEED NUMBER
!  ISEED
!
  REAL A,S
  INTEGER ISEED
  REAL HOLD,R1,R2,G01
  COMMON /RND/ OLD, HOLD
  LOGICAL  OLD
!
  IF (OLD)  GO TO 1
  R2 = 6.2831853*ran1(iseed)
  R1 = SQRT(-2.*ALOG(ran1(iseed)))
  G01 = R1*COS(R2)
  HOLD  = R1*SIN(R2)
  OLD = .TRUE.
  gauss = A + S*G01
  RETURN
1 G01 = HOLD
  OLD = .FALSE.
  gauss = A + S*G01
  RETURN
END FUNCTION

!***********************************************************************
SUBROUTINE driver (Ux,Uz)
  USE params, ONLY: mx, mz, iseed, master
  implicit none
  real, dimension(mx,my,mz):: Ux, Uz
!
  integer ii
  real Ux_sum, Uz_sum, UxS, UzS

!
!-----------------------------------------------------------------------
!
!  iseed, t and t_event should be taken from the previous run when continuing
!
  call print_trace('driver', dbg_force, 'BEGIN')
  !$omp barrier
  !$omp single
  if (ifirst.gt.0) then
    ifirst=0
    if (mpi_y == 0) then
      Ux_yl = Ux(:,lb,:)
      Uz_yl = Uz(:,lb,:)
    endif
    if (mpi_y == mpi_ny-1) then
      Ux_yh = Ux(:,ub,:)
      Uz_yh = Uz(:,ub,:)
    endif
    iseed = -iseed
    do ii=1,50
      j0 = sx*ran1(iseed)
    end do
    i   = 0
    k0  = 0.
    t_prv = t-1.e-6
  endif
!
!  At event time, change component, phase and amplitude at random
!
1 continue
  if (t .gt. t_event) then
    call rand_numbers (i, amean, iseed, j0, k0, drive, t_rise, &
                       ampl, t_delta, t_drive, t_event)
    t_event = t_event + t_delta
    ! t_delta = t_event
    if (mpi_y == 0) then
      Ux_sum = sum(Ux_yl**2)/(mx*mz)
      Uz_sum = sum(Uz_yl**2)/(mx*mz)
    else
      Ux_sum = 0.
      Uz_sum = 0.
    endif
    call sum_mpi_real (Ux_sum, UxS, 1)
    call sum_mpi_real (Uz_sum, UzS, 1)
    UxS = sqrt(UxS/(mpi_nx*mpi_nz))
    UzS = sqrt(UzS/(mpi_nx*mpi_nz))
    if (mpi_rank .eq. 0) then
      write (*,'(1x,a,i8,a,f10.6)') 'next event in about', &
        ifix(t_delta/dt),' time steps, at time', t_event
      write (*,'(1x,a,i3,2(1x,f7.3),4(1x,f10.6),i12)') &
       'drive:',i,j0,k0,ampl,t_event,UxS,UzS,iseed
    endif
    goto 1
  endif
  !$omp end single
  !$omp barrier
!
!  Update the bdry velocities, but only once (predictor step)
!
  if (t .gt. t_prv) then
    !$omp barrier
    !$omp single
    t_prv = t
    !$omp end single
    if (mpi_y == 0) then
      Ux_yl(:,izs:ize) = Ux_yl(:,izs:ize)*(1.-dt/t_rise)
      Uz_yl(:,izs:ize) = Uz_yl(:,izs:ize)*(1.-dt/t_rise)
      call velocity_update (i, drive, dt, +ampl, k0, j0, Ux_yl, Uz_yl)
    endif
    if (mpi_y == mpi_ny-1) then
      Ux_yh(:,izs:ize) = Ux_yh(:,izs:ize)*(1.-dt/t_rise)
      Uz_yh(:,izs:ize) = Uz_yh(:,izs:ize)*(1.-dt/t_rise)
      call velocity_update (i, drive, dt, -ampl, k0, j0, Ux_yh, Uz_yh)
    endif
  endif

  if (mpi_y == 0) then
    Ux(:,lb,izs:ize) = Ux_yl(:,izs:ize)
    Uz(:,lb,izs:ize) = Uz_yl(:,izs:ize)
  endif
  if (mpi_y == mpi_ny-1) then
    Ux(:,ub,izs:ize) = Ux_yh(:,izs:ize)
    Uz(:,ub,izs:ize) = Uz_yh(:,izs:ize)
  endif
!
  call boundary_velocity (Ux, Uz)

  call print_trace('driver', dbg_force, 'END')
END subroutine

!***********************************************************************
SUBROUTINE boundary_velocity (Ux, Uz)
  USE params, only: mx, mz, lb, ub
  implicit none
  real, dimension(mx,my,mz):: Ux, Uz
!
!  Control velocity at zone inside the driver to prevent ripples when driving
!
!  3 order polynomium with linear tangent at Uy,z(4,:,:) and Uy,z(nxgrid-4,:,:)
!  determined by Uyz(4) and Uyz(6) ...
!
!-----------------------------------------------------------------------
  call print_trace('boundary_velocity', dbg_force, 'BEGIN')
  if (mpi_y == 0) then
    Ux(:,lb+1,izs:ize) = (4./9.)*Ux(:,lb,izs:ize) + (6./9.)*Ux(:,lb+2,izs:ize) - (1./9.)*Ux(:,lb+3,izs:ize)
    Uz(:,lb+1,izs:ize) = (4./9.)*Uz(:,lb,izs:ize) + (6./9.)*Uz(:,lb+2,izs:ize) - (1./9.)*Uz(:,lb+3,izs:ize)
  endif
  if (mpi_y == mpi_ny-1) then
    Ux(:,ub-1,izs:ize) = (4./9.)*Ux(:,ub,izs:ize) + (6./9.)*Ux(:,ub-2,izs:ize) - (1./9.)*Ux(:,ub-3,izs:ize)
    Uz(:,ub-1,izs:ize) = (4./9.)*Uz(:,ub,izs:ize) + (6./9.)*Uz(:,ub-2,izs:ize) - (1./9.)*Uz(:,ub-3,izs:ize)
  endif
  call print_trace('boundary_velocity', dbg_force, 'END')
END subroutine

!***********************************************************************
SUBROUTINE velocity_update (i, drive, dt, ampl, k0, j0, Uxb, Uzb)
!
! update the velocity field for the rand_shear drivers
!
  USE params, ONLY: mx, mz, sx, sz, izs, ize
  implicit none
  integer i
  real k0, j0
  real drive, dt, ampl
  real, DIMENSION(mx,mz) :: Uxb, Uzb
  integer iz

  call print_trace('velocity_update', dbg_force, 'BEGIN')
  if (i.eq.1) then
    if (drive .gt. 0.) then
      do iz=izs,ize
        Uxb(:,iz) = Uxb(:,iz) + dt*ampl*sin(2.*pi*(zm(iz)-zmin-k0)/sz)
      end do
    else
      do iz=izs,ize
        Uxb(:,iz) = Uxb(:,iz) + dt*ampl*sin(2.*pi*(zm(iz)-zmin+k0)/sz)
      end do
    endif
  else
    if (drive .gt. 0.) then
      do iz=izs,ize
        Uzb(:,iz) = Uzb(:,iz) + dt*ampl*sin(2.*pi*(xm(:)-xmin-j0)/sx)
      end do
    else
      do iz=izs,ize
        Uzb(:,iz) = Uzb(:,iz) + dt*ampl*sin(2.*pi*(xm(:)-xmin+j0)/sx)
      end do
    endif
  endif
  call print_trace('velocity_update', dbg_force, 'END')
END subroutine

!***********************************************************************
SUBROUTINE rand_numbers (i, amean, iseed, j0, k0, drive, t_rise, &
                           ampl, t_delta, t_drive, t_event)
!
!  controls the random numbers in the rand_shear drivers; amean=0.5 
!  gives 50/50 chance of change, amean=0.75 gives 3/4 chance of change
!
  implicit none
  integer i, iseed
  real j0, k0
  real amean, drive, ampl, t_delta, t_event
  real t_rise, t_drive, d_tmp

  call print_trace('rand_numbers', dbg_force, 'BEGIN')

!  driver direction
  i = i + 1
  i = mod(i - amean + ran1(iseed),2.) + 1
  if (i .eq. 2) then
    j0 = sx*ran1(iseed)                                                 ! i==2 is Uz(x-j0)
  else
    k0 = sz*ran1(iseed)                                                 ! i==1 is Ux(x-k0)
  endif

!  driver amplitude
  d_tmp = abs(drive/t_rise)
  if (t_drive .gt. 0.) then
    ampl = amin1(amax1(gauss(d_tmp, d_tmp/5., iseed), .5*d_tmp), 1.5*d_tmp)
    ampl = sign(ampl,drive)
!  driver time
    t_delta = t_drive*amax1(amin1(4.,-alog(ran1(iseed))),.2)
  else
    ampl = sign(d_tmp,drive)
    t_delta = -t_drive
  endif

  call print_trace('rand_numbers', dbg_force, 'END')
END subroutine

END MODULE
!=======================================================================

!-----------------------------------------------------------------------
SUBROUTINE density_boundary (r,lnr,py,e)
  USE params
  implicit none
  real, dimension(mx,my,mz):: r,lnr,py,e

  call print_trace('density_boundary', dbg_force, 'BEGIN')
  call symmetric_center(r)
  call print_trace('density_boundary', dbg_force, 'END')
END

!-----------------------------------------------------------------------
SUBROUTINE density_boundary_log (r,lnr)
  USE params
  implicit none
  real, dimension(mx,my,mz):: r,lnr

  if (mpi_y == 0) then
    call symmetric_center_lower (lnr)
    r(:,1:lb-1,izs:ize)=exp(lnr(:,1:lb-1,izs:ize))
  end if

  if (mpi_y == mpi_ny-1) then
    call symmetric_center_upper (lnr)
    r(:,ub+1:my,izs:ize)=exp(lnr(:,ub+1:my,izs:ize))
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE energy_boundary (r,lnr,Ux,Uy,Uz,e,ee,p,Bx,By,Bz)
  USE params
  implicit none
  real, dimension(mx,my,mz):: r,lnr,Ux,Uy,Uz,e,ee,p,Bx,By,Bz

  call print_trace('energy_boundary', dbg_force, 'BEGIN')
  call symmetric_center(e)
  call print_trace('energy_boundary', dbg_force, 'END')
END

!-----------------------------------------------------------------------
SUBROUTINE passive_boundary (r,Ux,Uy,Uz,d,dd)
  USE params
  implicit none
  real, dimension(mx,my,mz):: r,Ux,Uy,Uz,d,dd
  integer iy, iz

  call symmetric_center(d)
END

!-----------------------------------------------------------------------
SUBROUTINE velocity_boundary (r,xdnr,ydnr,zdnr,Ux,Uy,Uz,px,py,pz,e,Bx,By,Bz)
  USE params
  USE braiding
  implicit none
  real, dimension(mx,my,mz):: r,xdnr,ydnr,zdnr,Ux,Uy,Uz,px,py,pz,e,ee,Bx,By,Bz

  call print_trace('velocity_boundary', dbg_force, 'BEGIN')

  call driver (Ux,Uz)
  if (mpi_y == 0) then
    px(:,lb:lb+1,izs:ize) = Ux(:,lb:lb+1,izs:ize)*xdnr(:,lb:lb+1,izs:ize)
    pz(:,lb:lb+1,izs:ize) = Uz(:,lb:lb+1,izs:ize)*zdnr(:,lb:lb+1,izs:ize)
  end if
  if (mpi_y == mpi_ny-1) then
    px(:,ub-1:ub,izs:ize) = Ux(:,ub-1:ub,izs:ize)*xdnr(:,ub-1:ub,izs:ize)
    pz(:,ub-1:ub,izs:ize) = Uz(:,ub-1:ub,izs:ize)*zdnr(:,ub-1:ub,izs:ize)
  end if

  call symmetric_center (px)
  call symmetric_center (pz)
  call antisymmetric_face (py)

  call symmetric_center (Ux)
  call symmetric_center (Uz)
  call antisymmetric_face (Uy)

  call print_trace('velocity_boundary', dbg_force, 'END')
END

!-----------------------------------------------------------------------
SUBROUTINE mfield_boundary (Ux,Uy,Uz,Bx,By,Bz)
  USE params
  implicit none
  real, dimension(mx,my,mz):: Ux,Uy,Uz,Bx,By,Bz
END

!-----------------------------------------------------------------------
SUBROUTINE ecurrent_boundary (Ex, Ey, Ez, Jx, Jy, Jz)
  USE params
  USE braiding
  implicit none
  real, dimension(mx,my,mz):: Ex, Ey, Ez, Jx, Jy, Jz

! Zap the electric current in the ghost zones, to avoid that interpolations
! leave an influence on the Lorentz force at iy=lb.  The horizontal components
! are centered at iy-1/2 so we zap them to index lb.  The vertical component
! is centered at iy, and the vertical current at iy=lb is needed for proper
! PDE behavior, so we zap Jy to index lb-1.

  if (mpi_y == 0) then
    Jx(:,1:lb  ,izs:ize) = 0.
    Jy(:,1:lb-1,izs:ize) = 0.
    Jz(:,1:lb  ,izs:ize) = 0.
  end if
  if (mpi_y == mpi_ny-1) then
    Jx(:,ub  :my,izs:ize) = 0.
    Jy(:,ub+1:my,izs:ize) = 0.
    Jz(:,ub  :my,izs:ize) = 0.
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE viscosity_boundary (nu, f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: nu, f
END

!-----------------------------------------------------------------------
SUBROUTINE efield_boundary (Ex, Ey, Ez, Bx_y, Bx_z, By_z, By_x, Bz_x, Bz_y, &
                            Rx, Ry, Rz, Ux_y, Ux_z, Uy_z, Uy_x, Uz_x, Uz_y, &
                            Ux, Uy, Uz)
  USE params
  USE braiding
  implicit none
  real, dimension(mx,my,mz):: Ex, Ey, Ez, Bx_y, Bx_z, By_z, By_x, Bz_x, Bz_y, &
                              Rx, Ry, Rz, Ux_y, Ux_z, Uy_z, Uy_x, Uz_x, Uz_y, &
                              Ux, Uy, Uz

  call print_trace('efield_boundary', dbg_force, 'BEGIN')

  call yup_set (By_z, scratch)                                          ! must be perfectly antisymmetric 
  if (mpi_y == 0) then
    fbdry(:,izs:ize) = +Uz_yl(:,izs:ize)*scratch(:,lb,izs:ize)
    call extrapolate_face_value_lower (Ex, fbdry)
  end if
  if (mpi_y == mpi_ny-1) then
    fbdry(:,izs:ize) = +Uz_yh(:,izs:ize)*scratch(:,ub,izs:ize)
    call extrapolate_face_value_upper (Ex, fbdry)
  end if

  call yup_set (By_x, scratch)
  if (mpi_y == 0) then
    fbdry(:,izs:ize) = -Ux_yl(:,izs:ize)*scratch(:,lb,izs:ize)
    call extrapolate_face_value_lower (Ez, fbdry)
  end if
  if (mpi_y == mpi_ny-1) then
    fbdry(:,izs:ize) = -Ux_yh(:,izs:ize)*scratch(:,ub,izs:ize)
    call extrapolate_face_value_upper (Ez, fbdry)
  end if

  call print_trace('efield_boundary', dbg_force, 'END')
END

!-----------------------------------------------------------------------
SUBROUTINE regularize (f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
!
  call symmetric_center(f)
END

!-----------------------------------------------------------------------
SUBROUTINE regularize_face (f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
!
  call symmetric_face (f)
END

!-----------------------------------------------------------------------
SUBROUTINE ddt_boundary (r,px,py,pz,e,p,Bx,By,Bz, &
                         drdt,dpxdt,dpydt,dpzdt, &
                         dedt,dBxdt,dBydt,dBzdt)
  USE params
  implicit none
  real, dimension(mx,my,mz):: r,px,py,pz,e,p,Bx,By,Bz, &
                              drdt,dpxdt,dpydt,dpzdt, &
                              dedt,dBxdt,dBydt,dBzdt

  call print_trace('ddt_boundary',dbg_hd,'BEGIN')
END

!-----------------------------------------------------------------------
SUBROUTINE init_boundary
  USE params
  USE braiding
  USE arrays, ONLY: lns
  implicit none
  character(len=mfile):: name
  character(len=mid):: id='$Id: boundary_kg.f90,v 1.5 2008/09/02 23:36:03 aake Exp $'

  call print_id(id)

  t_drive = 5.                                                          ! average time for each event
  t_rise  = .1                                                          ! velocity rise time
  amean   = .5                                                          ! 50/50 chance of direction change
  drive   = .1                                                          ! drive speed amplitude

  lb = 1
  if (mpi_y == 0) lb = 6
  ub = 0
  if (mpi_y == mpi_ny-1) ub = 5

  call read_boundary
  ub = my - ub

  if (mpi_y ==        0) allocate (Ux_yl(mx,mz), Uz_yl(mx,mz))
  if (mpi_y == mpi_ny-1) allocate (Ux_yh(mx,mz), Uz_yh(mx,mz))
  allocate (fbdry(mx,mz))
END

!-----------------------------------------------------------------------
SUBROUTINE read_boundary
  USE params
  USE braiding
  implicit none
  namelist /bdry/ t_drive, t_event, t_rise, iseed, amean, drive, lb, ub

  if (stdin.ge.0) then
    rewind (stdin); read (stdin,bdry)
  else
    read (*,bdry)
  end if
  if (master) write (*,bdry)
END
