!--------Boundaries need to be corrected accordingly to Franz's experiment

!-----------------------------------------------------------------------
SUBROUTINE init_boundary
  USE params
  implicit none
  character(len=mid), save:: id='$Id: boundary.f90,v 1.26 2007/05/27 10:02:18 aake Exp $'

  call print_id (id)                                                    ! print id string
  lb = 6                                                                ! 5 boundary zones
  ub = my-6                                                             ! 5 boundary zones
  call read_boundary
END

!-----------------------------------------------------------------------
SUBROUTINE read_boundary
  USE params
  implicit none
  namelist /bdry/lb,ub

  rewind (stdin); read(stdin,bdry)
  if (master) write(*,bdry)
END

!-----------------------------------------------------------------------
SUBROUTINE density_boundary (r, lnr, py, e)
  USE params
  implicit none
  real, dimension(mx,my,mz):: r, lnr, py, e
  integer iz

  call density_boundary_log (r, lnr)
END

!-----------------------------------------------------------------------
SUBROUTINE density_boundary_log (r, lnr)
  USE params
  USE forcing, only: rb, eb
  implicit none
  real, dimension(mx,my,mz):: lnr, r
  integer iy, iz

  if (mpi_y == 0) then
    do iz=izs,ize
      r(:,lb,iz) = rb(:,iz)                                             ! set r(lb)
      lnr(:,lb,iz) = alog(r(:,lb,iz))
    end do
    call extrapolate_center_lower (lnr)
    do iz=izs,ize
      do iy=1,lb
	r(:,iy,iz) = exp(lnr(:,iy,iz))
      end do
    end do
  end if

  if (mpi_y == mpi_ny-1) then
    call extrapolate_center_upper (lnr)
    do iz=izs,ize
      do iy=ub+1,my
	r(:,iy,iz) = exp(lnr(:,iy,iz))
      end do
    end do
  end if

END


!-----------------------------------------------------------------------
SUBROUTINE energy_boundary (r,lnr,Ux,Uy,Uz,e,ee,p,Bx,By,Bz)
  USE params
  USE forcing, only: rb, eb
  implicit none
  real, dimension(mx,my,mz):: r,lnr,Ux,Uy,Uz,e,ee,p,Bx,By,Bz
  integer iz

  if (mpi_y == mpi_ny-1) then
    do iz=izs,ize
      ee(:,ub,iz) = ee(:,ub-1,iz)                                       ! copy "temperature" from ub-1 to ub
    end do
  end if

  if (mpi_y == 0) then                                                  ! recompute energy per unit volume
    do iz=izs,ize
      e(:,lb,iz) = eb(:,iz)                                             ! set e(lb)
      ee(:,lb,iz) = e(:,lb,iz)/r(:,lb,iz)
    end do
  end if

  call symmetric_center (ee)                                            ! symmetrize temperature

  if (mpi_y == 0) then                                                  ! recompute energy per unit volume
    do iz=izs,ize
      e(:,1:lb-1,iz) = r(:,1:lb-1,iz)*ee(:,1:lb-1,iz)
    end do
  end if
  if (mpi_y == mpi_ny-1) then
    do iz=izs,ize
      e(:,ub:my,iz) = r(:,ub:my,iz)*ee(:,ub:my,iz)
    end do
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE passive_boundary (r,Ux,Uy,Uz,d,dd)
  USE params
  implicit none
  real, dimension(mx,my,mz):: r,Ux,Uy,Uz,d,dd

  call symmetric_center (d)
END

!-----------------------------------------------------------------------
SUBROUTINE velocity_boundary (r,xdnr,ydnr,zdnr,Ux,Uy,Uz,px,py,pz,e,Bx,By,Bz)
  USE params
  USE forcing, only: Uxb, Uzb
  implicit none
  real, dimension(mx,my,mz):: r,xdnr,ydnr,zdnr,Ux,Uy,Uz,px,py,pz,e,Bx,By,Bz
  integer iz

  if (mpi_y == 0) then
    do iz=izs,ize
      Ux(:,lb,iz) = Uxb(:,iz)
      Uz(:,lb,iz) = Uzb(:,iz)
      Ux(:,lb+1,iz) = (4./9.)*Ux(:,lb,iz) + (6./9.)*Ux(:,lb+2,iz) - (1./9.)*Ux(:,lb+3,iz)
      Uz(:,lb+1,iz) = (4./9.)*Uz(:,lb,iz) + (6./9.)*Uz(:,lb+2,iz) - (1./9.)*Uz(:,lb+3,iz)
    end do
    call symmetric_center (Ux)
    call symmetric_center (Uz)
    do iz=izs,ize
      px(:,1:lb+1,iz) = xdnr(:,1:lb+1,iz)*Ux(:,1:lb+1,iz)
      pz(:,1:lb+1,iz) = zdnr(:,1:lb+1,iz)*Uz(:,1:lb+1,iz)
    end do
  end if

!  call antisymmetric_face (py) 

  call symmetric_face_lower (Uy)
  if (mpi_y == 0) then
    do iz=izs,ize
      py(:,1:lb,iz) = ydnr(:,1:lb,iz)*Uy(:,1:lb,iz)
    end do
  end if

  call symmetric_face_upper (Uy)
  if (mpi_y == mpi_ny-1) then
    do iz=izs,ize
      py(:,ub+1:my,iz) = ydnr(:,ub+1:my,iz)*Uy(:,ub+1:my,iz)
    end do
  end if

END

!-----------------------------------------------------------------------
SUBROUTINE mfield_boundary (Ux,Uy,Uz,Bx,By,Bz)
  USE params
  implicit none
  real, dimension(mx,my,mz):: Ux,Uy,Uz,Bx,By,Bz

  call extrapolate_center (Bx)                                          ! important to use extrapolate here, to make
  call extrapolate_center (Bz)                                          ! the bdry current symmetric across bdry!
  call symmetric_face_upper (By)                                        ! ignore div(B) constraint beyond boundary
END

!-----------------------------------------------------------------------
SUBROUTINE  efield_boundary (Ex, Ey, Ez, Bx_y, Bx_z, By_z, By_x, Bz_x, Bz_y, &
                             Rx, Ry, Rz, Ux_y, Ux_z, Uy_z, Uy_x, Uz_x, Uz_y, &
                             Ux, Uy, Uz, Bxl, Byl, Bzl, Bxu, Byu, Bzu)
  USE params
  implicit none
  real, dimension(mx,my,mz):: Ex, Ey, Ez, Bx_y, Bx_z, By_z, By_x, Bz_x, Bz_y, &
                              Rx, Ry, Rz, Ux_y, Ux_z, Uy_z, Uy_x, Uz_x, Uz_y, &
                              Ux, Uy, Uz, Bxl, Byl, Bzl, Bxu, Byu, Bzu

  if (mpi_y == 0) call E_driver (Ex, Ez)                                ! this sets Ex and Ez on the lower boundary only

  call symmetric_face_upper(Ex)
  call symmetric_face_upper(Ez)
END

!-----------------------------------------------------------------------
SUBROUTINE ddt_boundary (r,px,py,pz,e,Bx,By,Bz, &
                         drdt,dpxdt,dpydt,dpzdt, &
                         dedt,dBxdt,dBydt,dBzdt)
  USE params
  implicit none
  real, dimension(mx,my,mz):: r,px,py,pz,e,Bx,By,Bz, &
                              drdt,dpxdt,dpydt,dpzdt, &
                              dedt,dBxdt,dBydt,dBzdt
  integer ix,iy,iz
  real c

  call antisymmetric_center_lower (drdt)
  call symmetric_center_upper (drdt)
  call symmetric_center (dpxdt)
  call symmetric_face_lower (dpydt)

  if (mpi_y == mpi_ny-1) then
    c = 1.1/dt                                                                  ! damp inflow over two time steps
    do iz=1,mz                                                                  ! loop over ..
    do iy=ub,ub+1                                                               ! .. outflow boundary
    do ix=1,mx                                                                  ! .. outflow boundary
      dpydt(ix,iy,iz) = dpydt(ix,iy,iz) - c*min(py(ix,iy,iz),0.0)               ! damping of inflow (only)
    end do
    end do
    end do
  end if

  call extrapolate_face_upper (dpydt)
  call symmetric_center (dpzdt)
  call antisymmetric_center_lower (dedt)
  call symmetric_center_upper (dedt)
  call symmetric_center (dBxdt)
  call symmetric_center (dBzdt)
  call antisymmetric_face_lower (dBydt)                                         ! By should remain unchanged at lb
  call symmetric_face_upper (dBydt)                                             ! By should be smooth at ub
END

!-----------------------------------------------------------------------
SUBROUTINE regularize (f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f

  call symmetric_center (f)
END

!-----------------------------------------------------------------------
SUBROUTINE regularize_face (f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f

  call symmetric_face (f)
END
