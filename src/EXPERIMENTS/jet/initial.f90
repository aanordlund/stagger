!************************************************************************

SUBROUTINE init_values
  USE params
  USE forcing
  USE variables
  implicit none
  real B0, r0, beta
  namelist /init/ r0,beta,phi0



! MYM - All the following is specific for the jet, inspiration taken from Franz' 
! earlier experiment in the start.f
! MYM - The following lines of codes initialises the jet setup.
r0=1.
  !allocate(phi(mx,my,mz))

  B0  = sqrt(2.*(gamma-1.)/(gamma*beta)* &
        (phi0+1./sqrt(1.+soft**2))**(gamma/(gamma-1.)))
  r   = (-phi+phi0)*sqrt(-phi+phi0)
  e  = (r**gamma)/gamma

  Bx = 0.
  By = B0
  Bz = 0.
  px = 0.
  py = 0.
  pz = 0.
!  e  = e*ee_cool ! what about this term?

  !deallocate(phi)

END
