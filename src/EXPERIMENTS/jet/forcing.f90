!***********************************************************************
!  The experiment forcing file must contain the following modules and
!  subroutines:
!
!  forcing      module with forcing parameters
!  init_force   called only once, at the beginning of a job
!  read_force   reads paramters; called from init_force and check.f90
!  forceit      adds forces, if any, to the dp[xyz]dt variables
!
!***********************************************************************
MODULE forcing
  implicit none
  real soft, psoft, softu, psoftu, phi0, shift, drive
  real, allocatable, dimension(:,:,:):: x, y, z, phi, gx, gy, gz
  real, allocatable, dimension(:,:):: rxz, Uxb, Uzb, phiu, rb, eb
  logical, save:: conservative=.true.                                   ! MUST be included = true for linear momemnta, false for velocities
END MODULE

!***********************************************************************
SUBROUTINE init_force
  USE params
  USE forcing
  USE variables, only: r,e                                              ! need these for rb, eb
  implicit none
 
  psoft=2.                                                              ! MYM
  soft=1.                                                               ! MYM
  softu=1.
  psoftu=2.
  drive=1.
  shift=2.5
  phi0=0. 

  call read_force                                                       ! read params

!#######################################################################
! The following sections is jet specific and taken from Franz'
! - MYM corrected according to jet direction, so x<->y.
  
  allocate(x(mx,my,mz),y(mx,my,mz),z(mx,my,mz),phi(mx,my,mz))           ! MYM
  allocate(gx(mx,my,mz),gy(mx,my,mz),gz(mx,my,mz))                      ! MYM

  x = spread(spread(xm, dim=2, ncopies=my), dim=3, ncopies=mz)          ! make 3D array from xm
  y = spread(spread(ym, dim=1, ncopies=mx), dim=3, ncopies=mz)          ! make 3D array from ym
  z = spread(spread(zm, dim=1, ncopies=mx), dim=2, ncopies=my)          ! make 3D array from zm

  phi = -1./((x**2+y**2+z**2)**(psoft/2.)+soft**psoft)**(1./psoft)      ! 3D gravitational potential

!  gx = -ddxdn(phi)                                                      ! x- gravity force
!  gy = -ddydn(phi)                                                      ! y- gravity force
!  gz = -ddzdn(phi)                                                      ! z- gravity force

  call ddxdn_neg (phi, gx)                                              ! MPI-enabled routines
  call ddydn_neg (phi, gy)                                              ! MPI-enabled routines
  call ddzdn_neg (phi, gz)                                              ! MPI-enabled routines

  call dumpl (phi, 'phi', 'phi.dmp', 0)                                 ! for checking
  call dumpl (gx , 'gx' , 'phi.dmp', 1)
  call dumpl (gy , 'gy' , 'phi.dmp', 2)
  call dumpl (gz , 'gz' , 'phi.dmp', 3)

  deallocate(x,y,z)                                                 ! MYM

!#######################################################################

!#######################################################################
! The following sections is jet specific and taken from Franz' - now it is 
! - MYM - adjusted to y being the jet direction.

  allocate(rxz(mx,mz),phiu(mx,mz),Uxb(mx,mz),Uzb(mx,mz))                ! MYM

  allocate(rb(mx,mz),eb(mx,mz))                                         ! boundary density and energy
  rb = r(:,lb,:)
  eb = e(:,lb,:)

  rxz = sqrt(spread(xmdn, dim=2, ncopies=mz)**2+ &                      ! y-face radius in xz-plane
             spread(zm  , dim=1, ncopies=mx)**2)


  phiu = 1./(rxz**psoftu+softu**psoftu)**(1./psoftu)                    ! gravitational potential
  print *,psoftu,softu,shift
  print *,maxval(rxz),minval(rxz)
  print *,maxval(phiu),minval(phiu)
  !Uxb = +drive*spread(zm, dim=1, ncopies=mx) &                          ! Keplerian y-velocity
  !      *sqrt(phiu*rxz**(psoftu-2)/(rxz**psoftu+softu**psoftu)) &
  !      *(tanh((sx/2.-shift)-rxz)+1.)/2.
  Uxb = -drive*spread(zm, dim=1, ncopies=mx)
  Uxb = Uxb *sqrt(phiu*rxz**(psoftu-2)/(rxz**psoftu+softu**psoftu))
  Uxb = Uxb *(tanh((sx/2.-shift)-rxz)+1.)/2.

  rxz = sqrt(spread(xm  , dim=2, ncopies=mz)**2+ &                      ! z-face radius in xz-plane - 
             spread(zmdn, dim=1, ncopies=mx)**2)

  phiu = 1./(rxz**psoftu+softu**psoftu)**(1./psoftu)                    ! gravitaional potential
  Uzb = +drive*spread(xm, dim=2, ncopies=mz) &                          ! Keplerian z-velocity
        *sqrt(phiu*rxz**(psoftu-2)/(rxz**psoftu+softu**psoftu)) &
        *(tanh((sz/2.-shift)-rxz)+1.)/2.

  deallocate(rxz,phiu)                                              ! MYM
  !deallocate(Uzb,Uxb)                                                       ! MYM


!#######################################################################

END SUBROUTINE

!***********************************************************************
SUBROUTINE read_force
  USE params
  USE forcing
  implicit none
  namelist /force/ soft, softu, psoft, psoftu, drive, shift

  rewind (stdin); read  (stdin,force)                                                   ! read parameter namelist
  if (master) write (*,force)                                           ! print, but only in master thread
END SUBROUTINE

!***********************************************************************
SUBROUTINE forceit (r,Ux,Uy,Uz,xdnr,ydnr,zdnr,dpxdt,dpydt,dpzdt)
  USE params
  USE forcing
  implicit none
  real, dimension(mx,my,mz):: r,Ux,Uy,Uz,xdnr,ydnr,zdnr,dpxdt,dpydt,dpzdt
                
!#######################################################################
!Jet specific and taken from franz
! - MYM
! - do we need a call to init_force in order to collect the correct values for gx,gy,gz? NO!!
  dpxdt = dpxdt + gx*xdnr
  dpydt = dpydt + gy*ydnr
  dpzdt = dpzdt + gz*zdnr
!deallocate(gx,gy,gz)
!########################################################################

END SUBROUTINE

!***********************************************************************
SUBROUTINE E_driver (Ex, Ez)
!- Needs correction for the 
!
! Set electric fields in the first layer outside the model proper.
! Called from efield_boundary.  We use the mesh coordinates xm(),
! zm(), xmdn(), and zmdn() from the params module; these give absolute
! coordinates, that work also when running under MPI, when each
! process only has a sub-domain.
!
!-----------------------------------------------------------------------
  USE params
  USE variables
  USE forcing
  implicit none
  real, dimension(mx,my,mz) :: Ex, Ez
  real, dimension(mx,mz) :: Exb, Ezb
  real B0
  integer ix, iz

  do iz=izs,ize
  do ix=1,mx
    B0 = 0.5*(By(ix,lb,iz)+By(ix,lb+1,iz))                             ! interpolate By to boundary
    Exb(ix,iz) = +Uzb(ix,iz)*B0                                        ! Kepler velocity times contant B
    Ezb(ix,iz) = -Uxb(ix,iz)*B0                                        ! Kepler velocity times contant B
  end do
  end do
  call extrapolate_face_value_lower (Ex, Exb)
  call extrapolate_face_value_lower (Ez, Ezb)

END subroutine
