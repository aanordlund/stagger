; file to fix
file='snapshot.dat'

; assemble single file from MPI
close,1
openw,1,file

; read snapshot 31
t=31
for i=0,7 do writeu,1,rm(t,i,file)
close,1

; open for updating
open,a,file,/update

; set density no smaller than the most common density
f=a(0)
for iy=170,203 do f(*,iy,*) = f(*,iy,*) > most(f(*,iy,*))
a(0)=f

; set energy no smaller than the most common energy
f=a(4)
for iy=170,203 do f(*,iy,*) = f(*,iy,*) > most(f(*,iy,*))
a(4)=f

; after this one needs to start the next job with

; &io iread=0 iwrite=32 file='snapshot.dat' ... /
; ...
; &run t=12.5 ... /

; which reads from snapshot 0 in the single file, and writes
; the next snapshot to slot 32 in the MPI mess, and sets the
; time of the snapshot manually (to 12.5 in this case).

END
