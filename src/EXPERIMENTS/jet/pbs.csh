#! /bin/csh
#PBS -S /bin/csh -j oe -q parlinux -N myjob
#PBS -l ncpus=16,cput=3200:00:00,walltime=168:00:00,mem=28gb

# $Id: pbs.csh,v 1.1 2007/05/21 22:20:55 aake Exp $

# ------------------------------------------------------------------------------------
cd $PBS_O_WORKDIR				# working directory
umask 026					# allow group to read
unlimit stack					# max stack size
limit core 0					# no core dump
setenv OMP_NUM_THREAD 1

# ------------------------------------------------------------------------------------
set x = jet_ifort_mpi.x
#set x = jet_aake_mpi.x
cp ~/bin/`uname -m`/$x run
ls -lL run/$x
\rm run/mesh*dat
#mpirun -np 4      run/$x run/input.txt >>& run/output.txt
mpirun -np $NCPUS run/$x run/input.txt >>& run/output.txt

