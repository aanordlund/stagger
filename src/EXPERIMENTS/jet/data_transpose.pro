; $Id: data_transpose.pro,v 1.5 2007/05/22 07:42:06 aake Exp $
;-----------------------------------------------------------------------
PRO transp, fnew, fold, tr, ny
  fnew[*,2:ny-3,*]=transpose(fold,tr)                                   ; copy interior
  for i=0,1       do fnew[*,i,*] = fnew[*,   2,*]                       ; duplicate first layer
  for i=ny-2,ny-1 do fnew[*,i,*] = fnew[*,ny-3,*]                       ; duplicate last layer
END

;-----------------------------------------------------------------------
PRO data_transpose,file1,file2,t,_extra=_extra
  open,a,file1,nv=nv,nt=nt,_extra=_extra
  default,t,nt-1
  sz=size(a)
  mx=sz[1]
  my=sz[2]
  mz=sz[3]
  ny=mx+4
  nz=my
  nx=mz
  fnew=fltarr(nx,ny,nz)
  openw,u,/get,file2
  tr=[2,0,1]
  transp, fnew, a[nv*t+0], tr, ny & writeu,u,fnew
  transp, fnew, a[nv*t+3], tr, ny & writeu,u,fnew
  transp, fnew, a[nv*t+1], tr, ny & writeu,u,fnew
  transp, fnew, a[nv*t+2], tr, ny & writeu,u,fnew
  transp, fnew, a[nv*t+4], tr, ny & writeu,u,fnew
  transp, fnew, a[nv*t+7], tr, ny & writeu,u,fnew
  transp, fnew, a[nv*t+5], tr, ny & writeu,u,fnew
  transp, fnew, a[nv*t+6], tr, ny & writeu,u,fnew
  free_lun,u
  mesh_transpose,file1,file2,_extra=_extra
END
