; $Id: mesh_transpose.pro,v 1.7 2007/05/22 07:42:07 aake Exp $
PRO mesh_transpose,file1,file2,_extra=_extra                            ; allow extra /swap_if_little
  open,a,file1,xm=xm,ym=ym,zm=zm,_extra=_extra                          ; get old mesh
  read_mesh,fileroot(file1)+'.mesh',xm=xm,ym=ym,zm=zm
  help,xm,ym,zm
  ym1=xm                                                                ; transpose
  xm1=zm
  zm1=ym
  dx0=xm[1]-xm[0]                                                       ; first dx
  nx=n_elements(xm)
  dxn=xm[nx-1]-xm[nx-2]                                                 ; last dx
  ym1=[xm[0]-2*dx0,xm[0]-dx0,xm,xm[nx-1]+dxn,xm[nx-1]+2*dxn]            ; add two extra zones
  default,file2,file1
  openw,u,/get,fileroot(file2)+'.mshtxt'                                ; open file.mshtxt for writing
  printf,u,n_elements(xm1)                                              ; write in 'meshtxt' format
  printf,u,xm1
  printf,u,n_elements(ym1)
  printf,u,ym1
  printf,u,n_elements(zm1)
  printf,u,zm1
  free_lun,u
  help,xm1,ym1,zm1
  ;stop

  file=fileroot(file1)+'.dim'
  openr,u,/get,file
  nx=0L & ny=0L & nz=0L
  readf,u,nx,ny,nz
  sx=0. & sy=0. & sz=0.
  readf,u,sx,sy,sz
  gam=0. & iv=0L
  readf,u,gam,iv
  nvar=0L
  readf,u,nvar
  free_lun,u
  nx=nx+4

  file=fileroot(file2)+'.dx'
  openw,u,/get,file
  printf,u,nz,nx,ny
  printf,u,sz/nz,sx/nx,sy/ny
  printf,u,gam,iv
  printf,u,nvar
  free_lun,u
END 
