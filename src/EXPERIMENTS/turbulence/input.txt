&mpi    mpi_nz=1 /
&io     iread=-1 nscr=25 nstep=20  nsnap=25 /
&run    Cdt=0.4 iseed=33 do_trace=f /
&vars   do_pscalar=t do_mhd=f /
&grid   mx=32 my=32 mz=32 sx=1. sy=0.25 sz=1. /
&pde    nu1=0.03 nu2=0.3 /
&bdry   /
&slice  /
&quench  /
&eqofst do_ionization=f /
&part   /
&force  do_force=t /
&init   /
&expl   /
&cool   /
