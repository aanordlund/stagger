PRO balsara_blast
  open,b,'test.scr'
  imsz,400
  window,0,xsize=800,ysize=800
  loadct,39

  image,/ws,b[*,*,0,0],/zl,0
  image,b[*,*,0,4],/zl,1
  image,sqrt(b[*,*,0,5]^2+b[*,*,0,6]^2),2
  image,sqrt((b[*,*,0,1]/xdn(b[*,*,0,0]))^2+(b[*,*,0,2]/ydn(b[*,*,0,0]))^2),3
END
