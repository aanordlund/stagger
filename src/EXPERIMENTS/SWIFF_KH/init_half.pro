@stagger_6th

  mx=512*3
  my=512
  mz=1

  rho0   = 1.0                                                     ; scalings and KH setup params
  beta   = 1.0                                                     ; 
  b0     = 1.0                                                     ; 
  theta  = 0.05                                                    ; 
  pfac   = 1.e-3                                                   ; 
  a_eq   = 1.0                                                     ; 
  l_eq   = 3.0                                                     ; 
  npm    = my/4
  gamma  = 5./3.

  fname = 'init_half.dat'                                          ; output file name
  iseed = 25                                                       ; for random phases


  sx = 180.
  sy = 30.*!pi
  dx = sx/mx
  dy = sy/my
  x = dx*findgen(mx)
  y = dy*findgen(my)
  xx = rebin(reform(x,mx,1,1),mx,my,mz)

  rho = replicate(rho0,mx,my,mz)
  zero = fltarr(mx,my,mz)
  px = zero
  xc = sx/4.
  pz = zero

  bx = zero
  by = zero
  bz = zero
  by[*,*,*] = b0*sin(theta)
  bz[*,*,*] = b0*cos(theta)

  e = zero
  e[*,*,*] = (1./(gamma-1.))*beta*0.5*b0^2                          ; if we have constant pressure everywhere.... beta == Pgas/Pmagn

  m = fltarr(npm)
  phim = fltarr(npm)
  for i=0,npm-1 do begin
   m[i] = i+1                                                       ; summation counter
   phim[i] = 2.*!pi*randomu(iseed)                                  ; uniformly distr. random phases for perturbation
  end

  fx = fltarr(mx)
  for i=0,mx-1 do begin
   fx[i] = exp(-((x[i]-xc)/l_eq)^2)                                 ; exponential decreasing perturb. away from shearing edge
  end
  gy = fltarr(my)
  for i=0,npm-1 do begin
   gy = gy + cos(2.*!pi*m[i]*y/sy + phim[i])/m[i]                   ; summation over m=[1,my/4] of cosines along y
  end

  psi = fltarr(mx,my,mz)
  for j=0,my-1 do begin
  for i=0,mx-1 do begin
   psi[i,j,0] = fx[i] * gy[j]                                       ; fold functions (correct??)
  end
  end

  psi = reform(psi,mx,my,mz)
  px = - ddydn(xdn(yup(psi)))*rho0
  py = + ddxdn(ydn(xup(psi)))*rho0
  pz = zero

  px = reform(px,mx,my,mz)
  py = reform(py,mx,my,mz)

  p2 = xup(px)^2+yup(py)^2
  pmax = max(p2)^0.5
  px = px*(pfac/pmax)
  py = py*(pfac/pmax)

 ;py = py +       a_eq*((tanh((xx-1.0*xc)/l_eq)-tanh((xx-3.0*xc)/l_eq))-1.)*rho0
  py = py + 0.5 * a_eq*((tanh((xx-1.0*xc)/l_eq)-tanh((xx-3.0*xc)/l_eq))-1.)*rho0

  close,1
  openw,1,fname
  writeu,1,rho
  writeu,1,px
  writeu,1,py
  writeu,1,pz
  writeu,1,e
  writeu,1,bx
  writeu,1,by
  writeu,1,bz
  close,1
END
