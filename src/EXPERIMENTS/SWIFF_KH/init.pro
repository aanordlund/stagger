@stagger_6th                                                       ; pick up some differential stagger operators

PRO init, pic_stagger=pic_stagger, velocities=velocities

default, pic_stagger, 0 ; no PIC format
default, velocities, 0    ; no velocities

  mx=512*3                                                         ; number grid points in x-direction
  my=512                                                           ; ditto y-dir
  mz=1                                                             ; ditto z-dir (2D sim)

; Scalings and KH setup params. Taken from Califano's pdf-handout (1st SWIFF meeting).
  default, default_data, 0                                         ; default, don't read random number phase sequence data from file (use 'iseed' + IDL randomu below instead)
  rho0   = 1.0                                                     ; density
  beta   = 1.0                                                     ; plasma beta
  b0     = 1.0                                                     ; B-field strength
  theta  = 0.05                                                    ; angle of B-field
  pfac   = 1.e-3                                                   ; perturbation strength limiter
  a_eq   = 1.0                                                     ; amplitude
  l_eq   = 3.0                                                     ; length of tranverse exponential drop-off
  gamma  = 5./3.                                                   ; adiabatic index
  iseed  = 25                                                      ; seed for the random number sequence for phase 
                                                                   ; shifts (nb: using same seed should lead to 
                                                                   ; identical sequence on all platforms/OSs).
  
  if (default_data) then begin 
   print,'Using default phase data'
   restore,'kh_rnd_phases.sav'                                     ; get the set of random phases from file instead (see below)
  endif else begin
   read,prompt='Give new random seed (presently iseed='+strtrim(iseed,1)+') : ',iseed 
   npm = my/4                                                      ; or produce new set
  endelse

  sx = 180.                                                        ; Lx
  sy = 30.*!pi                                                     ; Ly
  dx = sx/mx                                                       ; cell size, x-length
  dy = sy/my                                                       ; ditto y-length
  x  = dx*findgen(mx)                                              ; x-coordinate array
  y  = dy*findgen(my)                                              ; ditto y-coord
  xx = rebin(reform(x,mx,1,1),mx,my,mz)                            ; spread x-coordinate to entire grid (needed for the shearing profile done below)

  rho = replicate(rho0,mx,my,mz)                                   ; copy uniform density to entire grid
  zero = fltarr(mx,my,mz)                                          ; fill zeroes into all other MHD variables
  px = zero                                                        ; 
  pz = zero
  bx = zero
  by = zero
  bz = zero

  by[*,*,*] = b0*sin(theta)                                        ; y-comp of Bfield
  bz[*,*,*] = b0*cos(theta)                                        ; z-comp of Bfield

  e = zero                                                         ; zap internal energy array
  e[*,*,*] = (1./(gamma-1.))*beta*0.5*b0^2                         ; if we have constant pressure everywhere.... beta == Pgas/Pmagn

  xc = sx/4.                                                       ; centering of the shearing jump (@ x=1/4Lx and 3/4 Lx)

  if (fexists('kh_rnd_phases.sav') || fexists('kh_rnd_phases.sav')) then begin
   fname='alt_kh_rnd_phases'
  endif else begin
   fname='kh_rnd_phases'
  endelse

  if ( ~ default_data) then begin                                   ; in case we making new random phase sequence (and writing) to file...
   print,'Making new phase data'
   close,2                                                          ;
   openw,2,fname+'.txt'                                             ;
   printf,2,npm                                                     ; number of random phases
   m = fltarr(npm)
   phim = fltarr(npm)
   for i=0,npm-1 do begin
    m[i] = i+1                                                      ; summation counter
    phim[i] = 2.*!pi*randomu(iseed)                                 ; uniformly distr. random phases for perturbation == "phi_m" in Califano notes
    printf,2,m[i],phim[i]                                           ; wavenumbers
   end
   close,2                                                          ;
   save,npm,m,phim,filename=fname+'.sav'                            ;
   help,npm,m,phim                                                  ; Used once only and stored exp.specific data in CVS dir
  endif
  
  fx = fltarr(mx)                                                   ; array for the exponential folding == "f(x)" in Califano notes.
  for i=0,mx-1 do begin
   fx[i] = exp(-((x[i]-xc)/l_eq)^2)                                 ; construct "f(x)"
  end
  gy = fltarr(my)                                                   ; array for the cosine series perturbation == Sum_m(cos(.....))
  for i=0,npm-1 do begin
   gy = gy + cos(2.*!pi*m[i]*y/sy + phim[i])/m[i]                   ; construct the series; summation over m=[1,my/4] of cosines along y
  end

  psi = fltarr(mx,my,mz)                                            ; array for the perturb. 'potential', "phi
  for j=0,my-1 do begin
   for i=0,mx-1 do begin
    psi[i,j,0] = fx[i] * gy[j]                                      ; fold functions to obtain psi == "psi/epsilon" in Califano notes (epsilon normalization done below)
   endfor
  endfor

  psi = reform(psi,mx,my,mz)                                        ; reform for ability to use stagger operators (in stagger_6th include, 1st line)
  if (keyword_set(pic_stagger)) then begin
    if (keyword_set(velocities)) then begin
     px  = - ddydn(xup(yup(psi)))                                   ; make dp == e_z .X. grad(psi), x-component
     py  = + ddxdn(yup(xup(psi)))                                   ; ditto y-component
    endif else begin
     px  = - ddydn(xup(yup(psi)))*rho0                              ; make dp == e_z .X. grad(psi), x-component
     py  = + ddxdn(yup(xup(psi)))*rho0                              ; ditto y-component
    endelse
  endif else begin
   px  = - ddydn(xdn(yup(psi)))*rho0                                ; make dp == e_z .X. grad(psi), x-component
   py  = + ddxdn(ydn(xup(psi)))*rho0                                ; ditto y-component
  endelse

  pz  = zero                                                        ; only planar flow

  px = reform(px,mx,my,mz)                                          ; reform again
  py = reform(py,mx,my,mz)                                          ; ditto
  if (keyword_set(pic_stagger)) then begin
   p2 = xup(px)^2+yup(py)^2                                         ; square it, for normalization
  endif else begin
   p2 = xdn(px)^2+ydn(py)^2                                         ; square it, for normalization
  endelse
    
  pmax = max(p2)^0.5                                                ; root
  px   = px*(pfac/pmax)                                             ; normalize px perturbation, as to not exceed max(|dp| ~ 1.e-3)
  py   = py*(pfac/pmax)                                             ; ditto, py

  py = py + a_eq*( (tanh((xx-1.0*xc)/l_eq) - $
                    tanh((xx-3.0*xc)/l_eq)    )-1.)*rho0            ; finally add the large scale velocity (momentum) profile.....

  close,1                                                           ; writeout MHD (or PIC) variables to initial condition snapshot.

  openw,1,'init.dat'                                                ;

  writeu,1,rho                                                      ;

  writeu,1,px                                                       ;
  writeu,1,py                                                       ;
  writeu,1,pz                                                       ;

  writeu,1,e                                                        ;

  writeu,1,bx                                                       ;
  writeu,1,by                                                       ;
  writeu,1,bz                                                       ;

  if (keyword_set(pic_stagger)) then begin
   ex = -ydn(xup(py)*bz)+zdn(xup(pz)*by)                            ; electric field
   ey = -zdn(yup(pz)*bx)+xdn(yup(px)*bz)                            ; 
   ez = -xdn(zup(px)*by)+ydn(zup(py)*bx)                            ; 
   charge = ddxdn(ex) + ddydn(ey) + ddzdn(ez)                       ; gauss takes care of charge
   writeu,1,ex                                                      ;
   writeu,1,ey                                                      ;
   writeu,1,ez                                                      ;
   writeu,1,charge                                                  ;
  endif

  close,1                                                           ; done.

END
