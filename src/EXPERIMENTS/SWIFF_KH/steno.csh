#!/bin/csh

set x = SWIFF_KH_ifort_mpi.x

set a = input.txt
if ("$1" != "") set a = $1			# non-default input file

source /software/astro/bin/LLsetup.csh		# std LL setup

set i = $l.in
set o = $l.out

set o = $o.$$
if (-e output.txt) mv output.txt previous.txt
ln -sf $o output.txt

#\cp -p ~/bin/x86_64/$x $l.$x			# make a local copy to avoid killing jobs
\cp -p ../../$x $l.$x				# make a local copy to avoid killing jobs
set x = $l.$x
\cp -p $a $l.in.$$				# save a copy of the input file

(echo "BEGIN `date` `ls -l $x`"; echo '') >> $o	# date and executable logged

#set c = `wc -l < $LOADL_HOSTFILE`		# number of threads in hostfile
#mpirun -np $c --hostfile $h ./$x $a >> $o

@ n = 1
foreach f ( `sort -u $h` )
  grep $f $h > $h.$n
  mpirun -np 8 --hostfile $h.$n ./$x test$n.in > test$n.log &
  @ n++
end

wait
