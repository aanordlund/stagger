#!/bin/bash
# @ job_name         = SW_KH
# @ notification     = never
# @ output           = LOG/$(Cluster).err
# @ error            = LOG/$(Cluster).err
# @ class            = astro2
# @ job_type         = parallel
# @ NODE_USAGE       = NOT_SHARED
# @ wall_clock_limit = 24:00:00
# @ node             = 3
# @ tasks_per_node   = 8
# @ resources        = ConsumableCpus(1) ConsumableMemory(3gb)
# @ queue

/software/astro/bin/jobcontrol.csh ./steno.csh
