; IDL Version 7.0 (linux x86_64 m64)
; Journal File for trier@comp
; Working directory: /p7/trier/SWIFF/src/EXPERIMENTS/SWIFF_KH
; Date: Mon Nov 14 11:09:34 2011
 
PRO readsnap, fname, time=time, varnr=varnr, snap=snap

open,a,fname+'.dat'

snap = a[varnr+9*time]
nx= (size(snap))[1]
snap = snap[0:nx/2-1,*]

close,/all

END
