@stagger_6th
; IDL Version 7.0 (linux x86_64 m64)
; Journal File for trier@comp
; Working directory: /p7/trier/SWIFF/src/EXPERIMENTS/SWIFF_KH
; Date: Tue Nov 15 10:25:45 2011

PRO khgrowth, fname, t1, t2, varnr=varnr, totp=totp, spectrum=spectrum, velo=velo, png=png

totp = fltarr(t2-t1+1)
spectrum = fltarr(512,t2-t1+1)

for i=t1,t2 do begin 
 print,'i=',strtrim(i,1)
 readsnap, fname, time=i, varnr=varnr, snap=snap                      ; get the data

 if(keyword_set(velo) AND (varnr GE 1) AND (varnr LE 3)) then begin
  readsnap, fname, time=i, varnr=0, snap=rho                          ; get density (to construct velocity if momentum is read)
  case varnr of 
    1: snap = snap/xup1(rho)
    2: snap = snap/yup1(rho)
    3: snap = snap/zup1(rho)
  endcase
 endif
 
 f = abs(fft(snap,dimension=2))^2                                     ; make the FFT of a variable 'varnr' along Y-axis
 spectrum[*,i] = total(f[*,*],1)                                      ; spectrum intgrated (summed) along X-axis, not notmalized
 totp[i] = total(total(f[*,*],1),1)                                   ; get the total power, all modes (two-sided spectrum)
endfor

help,spectrum
help,totp

device,decomp=0
loadct,5

window,0,xsize=750,ysize=1000
!P.multi=[0,2,5]
!p.charsize=2.
plot,spectrum[ 1,*],tit='Ptot(t), mode 1',xtit='time (code units)',ytit='Ptot (a.u.)',/yl,ys=1,yr=[0.9*min(spectrum[1,*]),1.1*max(spectrum[1,*])]

for i=2,10 do begin
 plot,spectrum[ i,*],tit='mode '+strtrim(i,1),ytit='Ptot (code units)',/yl,ys=1,yr=[0.9*min(spectrum[i,*]),1.1*max(spectrum[i,*])]
endfor

if (keyword_set(png)) then begin
 filename=fname+'_10modes.png'
 WRITE_JPEG, filename, TVRD(/TRUE), /TRUE
endif

!p.charsize=1.

ybot = 0.12
ytop = 0.95
contourpos1 = [0.08,ybot+(ytop-ybot)*.55,0.85,ytop]
contourpos2 = [0.08,ybot,0.85,ybot+(ytop-ybot)*.45]
cbpos  = [0.92,ybot,0.95,ytop]

window,1,xsize=1000,ysize=1000
!P.multi=[0,1,2]
contour,transpose(alog10(spectrum[0:255,*])),/fill,nlev=255,$
        tit='log!I10!N(Sum!Iix=0,nx!N|fft_y(vx(t))|!E2!N) along y-axis',$
	ytit='K!IY!N',charsize=2.,xs=1,ys=1,yr=[1,255],$
	position = contourpos1
contour,transpose(alog10(spectrum[0:255,*])),/fill,nlev=255,$
        xtit='Time (code units)',ytit='log K!IY!N',charsize=2.,/yl,xs=1,ys=1,yr=[1,255],$
	position = contourpos2

colorbar, /vertical, range=[min(alog10(spectrum[0:255,*])),max(alog10(spectrum[0:255,*]))], $
          pos = cbpos, charsize=1.5, format='(f6.2)'

!p.multi=0

if (keyword_set(png)) then begin
 filename=fname+'_spectrum.png'
 WRITE_JPEG, filename, TVRD(/TRUE), /TRUE
endif

save,totp,spectrum,filename=fname+'.sav'

END
