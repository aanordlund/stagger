! $Id: initial.f90,v 1.1 2007/04/11 00:38:23 aake Exp $
!************************************************************************
FUNCTION input_file()
  USE params
  implicit none
  character(len=mfile) input_file
  input_file = 'EXPERIMENTS/shear/input.txt'
END

!************************************************************************
SUBROUTINE init_values
  USE params
  USE forcing
  USE variables
  implicit none
  real B0, r0
  namelist /init/ B0, r0
  character(len=mid), save:: id="$Id: initial.f90,v 1.1 2007/04/11 00:38:23 aake Exp $"

  call print_id (id)

  B0 = 1.
  r0 = 1.
  rewind (stdin); read (stdin, init)
  if (master) write (*, init)

  Bx = 0.
  By = B0
  Bz = 0.
  px = 0.
  py = 0.
  pz = 0.
  r  = r0
END
