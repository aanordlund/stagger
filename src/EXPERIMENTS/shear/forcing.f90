! $Id: forcing.f90,v 1.1 2007/04/11 00:38:23 aake Exp $
!***********************************************************************
!  The experiment forcing file must contain the following modules and
!  subroutines:
!
!  forcing      module with forcing parameters
!  init_force   called only once, at the beginning of a job
!  read_force   reads paramters; called from init_force and check.f90
!  forceit      adds forces, if any, to the dp[xyz]dt variables
!
!***********************************************************************
MODULE forcing
  implicit none
  real:: ampl, beta, deltax, deltaz
  logical, save:: conservative=.true.                                   ! MUST be included = true for linear momemnta, false for velocities
END MODULE

!***********************************************************************
SUBROUTINE init_force
  USE params
  USE forcing
  implicit none
  character(len=mid):: id="$Id: forcing.f90,v 1.1 2007/04/11 00:38:23 aake Exp $"

  call print_id (id)

  ampl = 0.3                                                            ! driver amplitude
  beta = 0.1                                                            ! ratio of magnetic to gas pressure
  deltax = 0.                                                           ! driver y-shift (size units)
  deltaz = 0.                                                           ! driver z-shift (size units)
  call read_force                                                       ! read params
END SUBROUTINE

!***********************************************************************
SUBROUTINE read_force
  USE params
  USE forcing
  implicit none
  namelist /force/ ampl, beta, deltax, deltaz

  read  (stdin,force)                                                   ! read parameter namelist
  if (master) write (*,force)                                           ! print, but only in master thread
END SUBROUTINE

!***********************************************************************
SUBROUTINE forceit (r,Ux,Uy,Uz,scr1,scr2,scr3,dpxdt,dpydt,dpzdt)
  USE params
  implicit none
  real, dimension(mx,my,mz):: r,Ux,Uy,Uz,scr1,scr2,scr3,dpxdt,dpydt,dpzdt
                                                                        ! do nothing; we force through the E-field
END SUBROUTINE

!***********************************************************************
SUBROUTINE E_driver (Ex, Ez)
!
! Set electric fields in the first layer outside the model proper.
! Called from efield_boundary.  We use the mesh coordinates xm(),
! zm(), xmdn(), and zmdn() from the params module; these give absolute
! coordinates, that work also when running under MPI, when each
! process only has a sub-domain.
!
!-----------------------------------------------------------------------
  USE params
  USE forcing
  implicit none
  real, dimension(mx,my,mz) :: Ex, Ez
  real rx, rz, kx, kz
  integer ix, iz

  kx = 2.*pi/sx
  kz = 2.*pi/sz
  do iz=izs,ize                                                         ! OpenMP z-loop limits
    do ix=1,mx
      rx = xm(ix) + deltax                                              ! xm(ix) is at cell center 
      Ex(ix,lb,iz) = +ampl*cos(kx*rx)                                   ! shear
      rz = zm(iz) + deltaz
      Ez(ix,ub+1,iz) = ampl*cos(kz*rz)
    end do
  end do
END subroutine
