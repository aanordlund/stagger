@stagger_6th                                                       ; pick up some differential stagger operators

PRO init ; Initialize double GEM setup

; grid sizes...
  mx = 960                                                         ; number grid points in x-direction
  my = 960                                                         ; ditto y-dir
  mz = 1                                                           ; ditto z-dir (2D sim)

; physical sizes...
  lx = 30.d0                                                       ; Lx
  ly = 30.d0                                                       ; Ly
  dx = lx/mx                                                       ; cell size, x-length
  dy = ly/my                                                       ; ditto y-length
  dz = (dx+dy)/2.                                                  ; why not?

  x  = dx*(indgen(mx)-mx/2)                                        ; make x-coord, center on 0 (precision)
  y  = dy*(indgen(my)-my/2)                                        ; make y-coord, center on 0 (presicsion)
  xx = rebin(reform(x,mx,1,1),mx,my,mz)                            ; spread x-coordinate to entire grid (needed for the shearing profile done below)
  yy = rebin(reform(y,1,my,1),mx,my,mz)                            ; spread y-coordinate to entire grid (needed for the shearing profile done below)

; setup parameters
  psilo  = 0.1d0
  psiup  = 0.1d0
;  yup    = 0.75d0*ly
;  ylo    = 0.25d0*ly
;  xmid   = 0.50d0*lx
  yup    = +0.25d0*ly ; center on 0...
  ylo    = -0.25d0*ly ; ditto
  xmid   =  0.00d0*lx ; ditto

; Physical parameters 
  b0     = 1.d0                                                    ; B-field strength
  gamma  = 5.d0/3.d0                                               ; adiabatic index
  t0     = b0^2*0.5d0                                              ; uniform temp
  e0     = t0/(gamma-1.d0)                                         ; constant internal energy
  twopi  = 2.d0*!pi

; construct the perturbation
  tplx=twopi/lx
  tply=twopi/ly

  a=exp(-tplx*((xx-xmid)^2.d0)-tply*((yy-ylo)^2.d0))
  b=exp(-tplx*((xx-xmid)^2.d0)-tply*((yy-yup)^2.d0))

  c=+psilo*tply*cos(tplx*(xx-xmid))
  d=+psilo*tplx*cos(tply*(yy-ylo))
  e=+psiup*tply*cos(tplx*(xx-xmid))
  f=+psiup*tplx*cos(tply*(yy-yup))

  g=(sin(tply*(yy-ylo))+ 2.d0*(yy-ylo)*cos(tply*(yy-ylo)))
  h=(sin(tply*(yy-yup))+ 2.d0*(yy-yup)*cos(tply*(yy-yup)))
  i=(sin(tplx*(xx-xmid))+ 2.d0*(xx-xmid)*cos(tplx*(xx-xmid)))
  j=(sin(tplx*(xx-xmid))+ 2.d0*(xx-xmid)*cos(tplx*(xx-xmid)))

  dbx1 = - c*g*a + e*h*b
  dby1 = + d*i*a - f*j*b

; make the MHD field 
  rho = (0.1d0+(cosh(yy-ylo))^(-2.d0)+(cosh(yy-yup))^(-2.d0))      ; density

  px   = replicate(0.d0,mx,my,mz)                                  ; x-mom
  py   = replicate(0.d0,mx,my,mz)                                  ; x-mom
  pz   = replicate(0.d0,mx,my,mz)                                  ; x-mom

  ;e   = replicate(e0,mx,my,mz)                                     ; internal energy
  ;e   = 1./(gamma-1)*(t0^2.*rho)

  bx = b0*(-1.+tanh(yy-ylo)+tanh(yup-yy))+dbx1                      ; x-comp of Bfield
  by = dby1                                                         ; y-comp of Bfield
  bz = replicate(0.d0,mx,my,mz)                                     ; z-comp of Bfield
  e   = 1./(gamma-1)*(.55 - bx^2/2. ) 

stat,rho
stat,px
stat,py
stat,pz
stat,e
stat,bx
stat,by
stat,bz

  bx=xdn(bx)
  by=ydn(by)
  
  close,1                                                          ; writeout MHD (or PIC) variables to initial condition snapshot.

  openw,1,'init.dat'                                               ;

  writeu,1,float(rho)                                              ;

  writeu,1,float(px)                                               ;
  writeu,1,float(py)                                               ;
  writeu,1,float(pz)                                               ;

  writeu,1,float(e)                                                ;

  writeu,1,float(bx)                                               ;
  writeu,1,float(by)                                               ;
  writeu,1,float(bz)                                               ;

  close,1                                                          ; done.

END
