module units
!
!  Stellar CGS units
!
  use params
  implicit none

  real, parameter:: cdu=1e-9, ctu=1e7, clu=1e12
  real, parameter:: cvu=clu/ctu, cku=cvu**2, cpu=cdu*cku

  real, parameter:: kB=1.38e-16, mproton=1.6726e-24, melectron=9.1094e-28, &
                    echarge=1.602e-12, hplanck=6.6261e-27, stefan=5.675e-5
  real, parameter:: u_l=clu, u_t=ctu, u_r=cdu, u_rho=u_r, u_u=u_l/u_t, &
                    u_ee=u_u**2, u_e=u_r*u_ee, pp2temp=(mproton/kB)*u_ee
  real u_temp

end module units
