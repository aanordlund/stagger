; $Id: disc_profile.pro,v 1.3 2013/08/21 07:45:18 remo Exp $
; const_temp(Initial pressure, sound speed, initial gravitational acceleration, vector of length coordinates) 
FUNCTION const_temp, P0, cs, g0, depth, nx=nx

  default, nx, 1

  gamma=1.4                                                                     ; gas gamma
  scale_height = sqrt(cs^2 / (gamma * g0))
  a = - 1. / (2.*scale_height^2)                                                ; pre-factor = 1 / (2 H^2)
  P = P0*exp(a*depth^2)                                                         ; pressure as a function of depth
  rho = gamma*P/cs^2                                                            ; Rho from E.O.S.

  elms = n_elements(depth)                                                      ; Nr of elements along a coordinate direction
  mx   = elms
  my   = elms / nx
  mz   = elms / nx

  nvar = 5                                                                      ; vars = {density, x-vel, y-vel, z-vel, pressure, Bx_left, ..., Bx_right}.
  vect = fltarr(nvar, mx, my, mz)                                               ; default value is zero, if not given in grafic file

  vect[0,*,*,*]=rebin(rho, mx, my, mz)                                          ; Set density
  vect[4,*,*,*]=rebin(P, mx, my, mz)                                            ; Set pressure

  return, vect

END

; output stagger file, mesh file, and dx file
stagger_file    = 'stagger.scr'
stagger_mshfile = 'stagger.msh'
stagger_dxfile  = 'stagger.dx'


; stagger-code units?
do_override_units = 1

; swap endianness of binary files?
swap_endian       = 0


AU_in_cm = 1.49597871d13
solar_mass_in_g = 1.9891d33

T0 = 100.                                                                       ; Kelvin
mu = 2.37                                                                       ; Molecular weight for H2 with Helium and solar metalicity
kb = 1.3806488d-16                                                              ; Boltzmann's constant in erg / K
m_unit = 1.660538922d-24                                                        ; unit mass
G_newton = 6.67260d-8                                                           ; Gravitational constant [cgs]

gamma = 1.4
cs_cgs = sqrt(gamma * kb * T0 / (mu * m_unit))                                  ; corresponding sound velocity

print, 'Temperature    [K]    : ', T0
print, 'Gas gamma             : ', gamma
print, 'Sound velocity [cm/s] : ', cs_cgs 

units_of_velocity = cs_cgs

; This is assuming a 1 solar mass central object and a distance of 1 AU. We have g_acc = gunit * height
gunit = G_newton * solar_mass_in_g / AU_in_cm^3                                 ; Prefactor for vertical gravitational acceleration [s^-2]

scale_height_cgs = sqrt(cs_cgs^2 / (gamma * gunit))                             ; scale height [cm]

units_of_length = scale_height_cgs

print, ''
print, 'Scale height [cgs] :', scale_height_cgs
print, 'Scale height [AU]  :', scale_height_cgs / AU_in_cm

print, ''
print, 'Gravitational acceleration in cm/s^2 at unit distance :', gunit * units_of_length
print, 'Gravitational acceleration in numerical units at unit distance :', gunit * units_of_length^2 / units_of_velocity^2

sigma_in_cgs = 3e3                                                              ; Surface density; Reference: ...

print, 'Surface density [g/cm^2] :', sigma_in_cgs

rho0 = sigma_in_cgs / (sqrt(2.*!DPI) * scale_height_cgs)

units_of_density = rho0 

P0_in_cgs = cs_cgs^2 / gamma * rho0

if (do_override_units) then begin

   units_of_length  = 1.0e12
   units_of_density = 1.0e-9
   units_of_velocity= 1.0e5

endif

print, ''
print, 'Units for physics_params namelist :'
print, 'Units_of_length   :', units_of_length
print, 'Units_of_velocity :', units_of_velocity
print, 'Units_of_density  :', units_of_density
print, ''

P0 = P0_in_cgs / (units_of_density * (units_of_velocity^2))
cs = cs_cgs / units_of_velocity
g0 = gunit * units_of_length^2 / units_of_velocity^2

print, 'P0 :', P0
print, 'cs :', cs
print, 'g0 :', g0

levelmin = 5L                                                                   ; level for the unigrid resolution
default, dir, '.'                                                               ; directory, where to dump the grafic file
nx = 4L                                                                         ; aspect ratio: nx = Lx / Ly
xoffset  = 0.0                                                                  ; offset in index space, to account for position of bdry in Stagger
;xoffset  = 0.5                                                                  ; offset in index space, to account for position of bdry in  Ramses


; depth array, internal units
depth = (findgen(nx*2l^levelmin)/2^levelmin + xoffset) * scale_height_cgs / units_of_length

; construct hydrostatic profile
vect = const_temp(P0,cs,g0,depth,nx=nx)

; transpose, stagger-code style?
transpose_to_y=1b
if (transpose_to_y) then begin
  print, 'data transpose: yes'
  vect = transpose(vect, [0,2,1,3])
;  mk_grafic, vect, dir=dir, boxlen=1.                                           ; write out initial data. =1 because dx is set by considering the x-direction
endif else begin
  print, 'data transpose: no'
;  mk_grafic, vect, dir=dir, boxlen=nx                                           ; write out initial data
endelse



; prepare stagger data cube and mesh

; get dimensions
ns   = size( vect )
nvar = ns[1]

; number of ghost zones, position of "lower" and "upper" boundaries
ng = 5L

;; dimensions stagger data cube
;my   = ns[3]
;mx   = my
;mz   = my

; assume same dimensions as vect?
mx   = ns[2]
my   = ns[3]
mz   = ns[4]

; add ghost zones
my   = my + ng*2L

; position of "lower" and "upper" boundaries
lb = ng
ub = my-ng-1


; replicate first column of data over entire cube;
; reverse order of depth points: midplane located at iy=ub in stagger
data = fltarr(nvar,mx,my,mz)
for iv=0L,nvar-1 do begin
   for i=0L,mx-1 do begin
      for k=0L,mz-1 do begin
         data[iv,i,lb:ub,k] = float(reverse(reform(vect[iv,0,*,0])))
      endfor
   endfor
endfor

; fill ghost zones with boundary values, for conditioning
; (not strictly necessary, but better safe than sorry)
if (lb gt 0L) then begin
   for iv=0L,nvar-1 do begin
      for j=0L,lb-1 do begin
         data[iv,*,j,*] = data[iv,*,lb,*] 
      endfor
   endfor
endif

if (ub lt my-1) then begin
   for iv=0L,nvar-1 do begin
      for j=ub+1,my-1 do begin
         data[iv,*,j,*] = data[iv,*,ub,*] 
      endfor
   endfor
endif

; mesh arrays
xm = fltarr(mx)
ym = fltarr(my)
zm = fltarr(mz)

; reverse order of depth points: midplane located at iy=ub in stagger;
; set depth at midplane (iy=ub) equal to zero;
; note: in stagger, ym is a depth scale in the sense that ym decreases outwards!
ym[lb:ub] = float(reverse(reform(depth[0]-depth)))

; ghost zones: fill them later when writing mesh into file


; horizontal (x and z) axes: take a constant stepping (approximately the same as for depth scale)
dx = float(depth[ns[3]-1] / float(ns[3]-1))
dz = dx
xm = findgen(mx) * dx
zm = findgen(mz) * dz


; write stagger data cube into file
openw, lun, /get_lun, stagger_file, swap_endian=swap_endian
for iv=0L,nvar-1L do begin
  writeu,lun,reform(data[iv,*,*,*])
endfor
free_lun,lun


; write stagger mesh information
; note: needs write_mesh.pro routine
write_mesh, stagger_mshfile, xm, ym, zm, dxfile=stagger_dxfile, $
   /do_ghost, nghost=ng, gamma=gamma, nvar=nvar, swap_endian=swap_endian


END

;
; This routine sets up a density and pressure profile in
; hydrostatic Equilibrium under the assumption of
; 
;   - Constant temperature
;   - Adiabatic equation of state
;   - Linear gravity field along the x-axis
;
