pro stg_mesh, xm, ym, zm, $
              dxmo=dxmo, dxmdno=dxmdno, xmdno=xmdno, dxidxupo=dxidxupo, dxidxdno=dxidxdno, $
              dymo=dymo, dymdno=dymdno, ymdno=ymdno, dyidyupo=dyidyupo, dyidydno=dyidydno, $
              dzmo=dzmo, dzmdno=dzmdno, zmdno=zmdno, dzidzupo=dzidzupo, dzidzdno=dzidzdno, $
              sx=sx, sy=sy, sz=sz, $
              spline=spline, double=double

;+
; NAME:
; PURPOSE:
; SYNTAX:
; ARGUMENTS/OPTIONS:
;-

  id = 'stg_mesh'

  nx   = n_elements( xm )
  ny   = n_elements( ym )
  nz   = n_elements( zm )

; define double precision working arrays for main mesh arrays
  nx2  = long( nx )
  ny2  = long( ny )
  nz2  = long( nz )

  xm2  = dblarr( nx2 )
  ym2  = dblarr( ny2 )
  zm2  = dblarr( nz2 )

; define staggered mesh arrrays and index derivatives
  xmdn2 = xm2  &  dxidxup2 = xm2  &  dxidxdn2 = xm2  &  dxm2 = xm2  &  dxmdn2 = xm2
  ymdn2 = ym2  &  dyidyup2 = ym2  &  dyidydn2 = ym2  &  dym2 = ym2  &  dymdn2 = ym2
  zmdn2 = zm2  &  dzidzup2 = zm2  &  dzidzdn2 = zm2  &  dzm2 = zm2  &  dzmdn2 = zm2

; convert xm, ym, zm to double-precision arrays
  xm2 = double( xm )
  ym2 = double( ym )
  zm2 = double( zm )
  

; linear index scales
  xi2  = dindgen( nx2 ) + 1.0
  yi2  = dindgen( ny2 ) + 1.0
  zi2  = dindgen( nz2 ) + 1.0


; spline index derivatives
  dxidxup2 = 1.0 / derf(xi2, xm2)
  dyidyup2 = 1.0 / derf(yi2, ym2)
  dzidzup2 = 1.0 / derf(zi2, zm2)

; stagger down mesh arrays 
; note: use linear interpolation to ensure down-staggered points are
; exactly in the middle between non-staggered grid points
  xmdn2	   = interpol(xm2, xi2, xi2-0.5 )
  ymdn2    = interpol(ym2, yi2, yi2-0.5 )
  zmdn2	   = interpol(zm2, zi2, zi2-0.5 )
  
; spline derivatives
  dxidxdn2 = 1.0 / derf(xi2, xmdn2)
  dyidydn2 = 1.0 / derf(yi2, ymdn2)
  dzidzdn2 = 1.0 / derf(zi2, zmdn2)

; separation between cell centres
  dxmdn2[1:nx2-1] = xm2[1:nx2-1] - xm2[0:nx2-2]
  dymdn2[1:ny2-1] = ym2[1:ny2-1] - ym2[0:ny2-2]
  dzmdn2[1:nz2-1] = zm2[1:nz2-1] - zm2[0:nz2-2]
  dxmdn2[0]       = dxmdn2[1]
  dymdn2[0]       = dymdn2[1]
  dzmdn2[0]       = dzmdn2[1]

; separation between cell faces (size of cells)
  dxm2[0:nx2-2]   = xmdn2[1:nx2-1] - xmdn2[0:nx2-2]
  dym2[0:ny2-2]   = ymdn2[1:ny2-1] - ymdn2[0:ny2-2]
  dzm2[0:nz2-2]   = zmdn2[1:nz2-1] - zmdn2[0:nz2-2]
  dxm2[nx2-1]     = dxm2[nx2-2]
  dym2[ny2-1]     = dym2[ny2-2]
  dzm2[nz2-1]     = dzm2[nz2-2]

; compute mesh's physical size
  sx2 = xm2[nx2-1] - xm2[0] + (xm2[0] - xmdn2[0]) + (xm2[nx2-1] - xmdn2[nx2-1])
  sy2 = ym2[ny2-1] - ym2[0] + (ym2[0] - ymdn2[0]) + (ym2[ny2-1] - ymdn2[ny2-1])
  sz2 = zm2[nz2-1] - zm2[0] + (zm2[0] - zmdn2[0]) + (zm2[nz2-1] - zmdn2[nz2-1])


; convert to float
if keyword_set( double ) then begin
  
  sx       = sx2
  sy       = sy2
  sz       = sz2

  dxmo     = dxm2
  dxmdno   = dxmdn2
  xmdno    = xmdn2
  dxidxupo = dxidxup2
  dxidxdno = dxidxdn2

  dymo     = dym2
  dymdno   = dymdn2
  ymdno    = ymdn2
  dyidyupo = dyidyup2
  dyidydno = dyidydn2

  dzmo     = dzm2
  dzmdno   = dzmdn2
  zmdno    = zmdn2
  dzidzupo = dzidzup2
  dzidzdno = dzidzdn2
 
endif else begin
     
  sx       = float( sx2 ) 
  sy       = float( sy2 )
  sz       = float( sz2 )

  dxmo     = float( dxm2 )
  dxmdno   = float( dxmdn2 )
  xmdno    = float( xmdn2 )
  dxidxupo = float( dxidxup2 )
  dxidxdno = float( dxidxdn2 )

  dymo     = float( dym2 )
  dymdno   = float( dymdn2 )
  ymdno    = float( ymdn2 )
  dyidyupo = float( dyidyup2 )
  dyidydno = float( dyidydn2 )

  dzmo     = float( dzm2 )
  dzmdno   = float( dzmdn2 )
  zmdno    = float( zmdn2 )
  dzidzupo = float( dzidzup2 )
  dzidzdno = float( dzidzdn2 )
 
endelse

end
