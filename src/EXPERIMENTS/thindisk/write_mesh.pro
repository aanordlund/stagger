pro write_mesh, fmesh, xm, ym, zm, $
                dxfile=dxfile, nvar=nvar, gamma=gamma, swap_endian=swap_endian, $
                sx=sx, sy=sy, sz=sz, do_ghost=do_ghost, nghost=nghost

;+
; NAME: write_mesh
;
; PURPOSE: write mesh information for stagger-code simulations into a
; binary file
;
; SYNTAX:  write_mesh, fmesh, xm, ym, zm, $
; dxfile=dxfile, nvar=nvar, gamma=gamma, swap_endian=swap_endian, $
; sx=sx, sy=sy, sz=sz, do_ghost=do_ghost, nghost=nghost
;
; ARGUMENTS/OPTIONS
; IN:   fmesh          file name where to save mesh information    
;       xm, ym, zm     main mesh arrays
;	dxfile         dx file name        (optional)
;       nvar           number of variables (optional)
;       gamma          gamma exponent      (optional)
;       swap_endian
; OUT:  sx, sy, sz     xyz-sizes           (optional)
;-

  id = 'write_mesh'

  default, nghost, 5L
  default, do_ghost, 0

  default, nvar,  6
  default, gamma, 1.666667
  default, xvert, 0L
  default, zero, 0L

  nx = n_elements( xm )
  ny = n_elements( ym )
  nz = n_elements( zm )

; if do_ghost, then force recompute ghost zones for ym array
  if keyword_set( do_ghost) then begin
     if (nghost gt 0L) then begin
        for j=nghost,0,-1 do begin
           ym[nghost-j] = float(j+1) * ym[nghost] - float(j)*ym[nghost+1]
        endfor
        for j=ny-nghost,ny-1,+1  do begin
           ym[j] = 3.*ym[j-1] -3.*ym[j-2] + ym[j-3]
        endfor
     endif
  endif

  stg_mesh, xm, ym, zm, $
            dxmo=dxm, dxmdno=dxmdn, xmdno=xmdn, dxidxupo=dxidxup, dxidxdno=dxidxdn, $
            dymo=dym, dymdno=dymdn, ymdno=ymdn, dyidyupo=dyidyup, dyidydno=dyidydn, $
            dzmo=dzm, dzmdno=dzmdn, zmdno=zmdn, dzidzupo=dzidzup, dzidzdno=dzidzdn, $
            sx=sx, sy=sy, sz=sz, $
            /spline

; write mesh file
  openw, /get, lun, fmesh, /f77_unformatted, swap_endian=swap_endian
  writeu, lun, long(nx), long(ny), long(nz)
  writeu, lun, dxm, dxmdn, xm, xmdn, dxidxup, dxidxdn
  writeu, lun, dym, dymdn, ym, ymdn, dyidyup, dyidydn
  writeu, lun, dzm, dzmdn, zm, zmdn, dzidzup, dzidzdn
  free_lun, lun


; write dxfile?
  if keyword_set( dxfile ) then begin
     dx = sx / float(nx)
     dy = sy / float(ny)
     dz = sz / float(nz)
     openw, lun, dxfile
     printf,lun, nx, ny, nz
     printf,lun, dx, dy, dz 
     printf,lun, gamma, xvert
     printf,lun, nvar
     printf,lun, zero
     free_lun, lun
  endif

end

