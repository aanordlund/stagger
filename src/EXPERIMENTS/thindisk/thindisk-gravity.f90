! $Id: thindisk-gravity.f90,v 1.3 2013/08/21 07:32:20 remo Exp $
!**********************************************************************
MODULE forcing

  logical, parameter :: conservative=.true.
  logical :: do_force
  real    :: gx, gy, gz, ay, t_damp
  real,         allocatable, dimension(:) :: gya
  real(kind=8), allocatable, dimension(:) :: pyav, rav, pav, eav
  integer :: ky, ny

END MODULE forcing

!***********************************************************************
SUBROUTINE init_force
 
  USE params
  USE forcing
  implicit none
  integer :: iy
  character(len=mid):: id="$Id: thindisk-gravity.f90,v 1.3 2013/08/21 07:32:20 remo Exp $"

  call print_id(id)

  if (mpi_ny > 1) then
     if (master) then
        print *, '--- init_force: '
        print *, '    MPI in y-direction not enabled in this version'
     end if
    call end_mpi
  end if

  do_force = .true.

  ! in this setup, we assume the vertical (y) component of the gravitational force
  ! to change linearly with height, starting from zero at the bottom boundary and
  ! increasing upwards in magnitude (directed toward the bottom though)
  !
  ! ay is the rate at which the gravitational force varies with height; it can
  ! be computed as:
  !
  ! ay = GM/R^3
  ! 
  ! where M is the mass of the central object and R is the distance of the portion
  ! of the disk from it
  ! 
  ! With M= 1 M_Sun and R= 1 au (assuming internal units for this module):
  ! ay     = 4.0                     ! rate of change in grav force with y (height)
  
  ay     = 0.

  ! gy : reference gravitational force at bottom boundary (iy=ub)

  ! we assume that the y scale is a depth scale, i.e. y increases downwards

  ! gy : reference gravitational force at bottom boundary (iy=ub)
  gy     = 0.

  ! other g components are assumed to be constant
  gx     = 0.
  gz     = 0.

  ! angular velocity components
  omegax = 0.
  omegay = 0.
  omegaz = 0.

  t_damp = 0.               ! default, no damping

  call read_force

  allocate (gya(my),pyav(my),rav(my),pav(my),eav(my))
  pav = 0.

  ! gravitaional force linearly decreasing with depth
  do iy=1,my
     gya(iy) = gy + ay * (ym(ub) - ymdn(iy))
  end do

  !debug
  if (master) then 
     print *,'gravitational force, vertical component:'
     do iy=1,my
        print *,iy,ymdn(iy),gya(iy)
     enddo
  endif

END SUBROUTINE init_force


!***********************************************************************
SUBROUTINE read_force
  USE params
  USE forcing
  implicit none
  namelist /force/ do_force, gx, gy, gz, omegax, omegay, omegaz, ay, t_damp
  character(len=mid):: id="$Id: thindisk-gravity.f90,v 1.3 2013/08/21 07:32:20 remo Exp $"

  call print_id(id)

  rewind (stdin); read (stdin,force)
  if (master) write (*,force)
END SUBROUTINE read_force

!***********************************************************************
SUBROUTINE forceit (r,Ux,Uy,Uz,xdnr,ydnr,zdnr,dpxdt,dpydt,dpzdt)
  USE params
  implicit none
  real, dimension(mx,my,mz):: r,Ux,Uy,Uz,xdnr,ydnr,zdnr,dpxdt,dpydt,dpzdt
  logical omp_in_parallel
  !-----------------------------------------------------------------------
  if (omp_in_parallel()) then
     call forceit_omp (r,Ux,Uy,Uz,xdnr,ydnr,zdnr,dpxdt,dpydt,dpzdt)
  else
     !$omp parallel
     call forceit_omp (r,Ux,Uy,Uz,xdnr,ydnr,zdnr,dpxdt,dpydt,dpzdt)
     !$omp end parallel
  end if
END SUBROUTINE forceit

!***********************************************************************
SUBROUTINE forceit_omp (r,Ux,Uy,Uz,xdnr,ydnr,zdnr,dpxdt,dpydt,dpzdt)
  USE params
  USE variables, only: py
  USE forcing
  USE arrays, ONLY: scr4, scr5
  implicit none
  real, dimension(mx,my,mz):: r,Ux,Uy,Uz,xdnr,ydnr,zdnr,dpxdt,dpydt,dpzdt
  integer iy, iz
  real f
  character(len=mid):: id="$Id: thindisk-gravity.f90,v 1.3 2013/08/21 07:32:20 remo Exp $"
  !-----------------------------------------------------------------------
  call print_id(id)

  if (gx.ne.0.) then
     do iz=izs,ize
        dpxdt(:,:,iz) = dpxdt(:,:,iz) + gx*xdnr(:,:,iz)
     end do
  end if

  if (gy.ne.0.0.or.ay.ne.0.0) then
     do iz=izs,ize
        do iy=1,my
           dpydt(:,iy,iz) = dpydt(:,iy,iz) + gya(iy)*ydnr(:,iy,iz)
        end do
     end do
  end if

  if (gz.ne.0.) then
     do iz=izs,ize
        dpzdt(:,:,iz) = dpzdt(:,:,iz) + gz*zdnr(:,:,iz)
     end do
  end if

  if (t_damp.ne.0.) then
     call haverage_subr (py, pyav)                                       ! horizontally averaged py
     do iz=izs,ize
        do iy=lb,ub
           dpydt(:,iy,iz) = dpydt(:,iy,iz) - xdnr(:,iy,iz)*(pyav(iy)/(rav(iy)*t_damp))
        end do
     end do
  end if

  if (omegax.ne.0 .or. omegay.ne.0 .or. omegaz.ne.0) then
     if (omegay.ne.0 .or. omegaz.ne.0) then
        do iz=izs,ize
           scr5(:,:,iz) = xdnr(:,:,iz)*Ux(:,:,iz)
        end do
        call xup_set(scr5,scr4)
        if (omegaz .ne. 0) then
           call ydn_set(scr4,scr5)
           do iz=izs,ize
              do iy=lb,ub
                 dpydt(:,iy,iz) = dpydt(:,iy,iz) - (2.*omegaz)*scr5(:,iy,iz)
              end do
           end do
        end if
        if (omegay .ne. 0) then
           !$omp barrier
           call zdn_set(scr4,scr5)
           do iz=izs,ize
              do iy=lb,ub
                 dpzdt(:,iy,iz) = dpzdt(:,iy,iz) + (2.*omegay)*scr5(:,iy,iz)
              end do
           end do
        end if
     end if
     if (omegaz.ne.0 .or. omegax.ne.0) then
        do iz=izs,ize
           scr5(:,:,iz) = ydnr(:,:,iz)*Uy(:,:,iz)
        end do
        call yup_set(scr5,scratch)
        if (omegax .ne. 0) then
           !$omp barrier
           call zdn_set(scratch,scr5)
           do iz=izs,ize
              do iy=lb,ub
                 dpzdt(:,iy,iz) = dpzdt(:,iy,iz) - (2.*omegax)*scr5(:,iy,iz)
              end do
           end do
        end if
        if (omegaz .ne. 0) then
           call xdn_set(scratch,scr5)
           do iz=izs,ize
              do iy=lb,ub
                 dpxdt(:,iy,iz) = dpxdt(:,iy,iz) + (2.*omegaz)*scr5(:,iy,iz)
              end do
           end do
        end if
     end if
     if (omegax.ne.0 .or. omegay .ne.0) then
        do iz=izs,ize
           scr5(:,:,iz) = zdnr(:,:,iz)*Uz(:,:,iz)
        end do
        !$omp barrier
        call zup_set(scr5,scr4)
        if (omegay .ne. 0) then
           call xdn_set(scr4,scratch)
           do iz=izs,ize
              do iy=lb,ub
                 dpxdt(:,iy,iz) = dpxdt(:,iy,iz) - (2.*omegay)*scratch(:,iy,iz)
              end do
           end do
        end if
        if (omegax .ne. 0) then
           call ydn_set(scr4,scratch)
           do iz=izs,ize
              do iy=lb,ub
                 dpydt(:,iy,iz) = dpydt(:,iy,iz) + (2.*omegax)*scratch(:,iy,iz)
              end do
           end do
        end if
     end if
  end if

  return
END SUBROUTINE forceit_omp
