; add density perturbation ("blob") to existing stratification

swap_endian = 0

fin     = 'stagger.scr'
fout    = 'blob.scr'
fmshout = 'blob.msh'
fdxout  = 'blob.dx'

; load stagger snapshot
open,a,fin,swap_endian=swap_endian

; load variables
r  = a[0]
px = a[1]
py = a[2]
pz = a[3]
e  = a[4]
ee = e/r

; position of blob's centre in index space
ix0=16L
iy0=32L+5L
iz0=16L

; scale length (propto gaussian sigma) of blob in units of pixels
scale_blob_pxl=0.02*128

; power index for funcional shape of perturbation
npow = 2

; overdensity with respect to conditions at (ix0,iy0,iz0)
factor_blob = 40.00 
rho_blob_centre = factor_blob * reform(r[ix0,iy0,iz0])

; density perturbation array
rho_blob = fltarr(nx,ny,nz)

for i=0L,nx-1 do begin
  for j=0L,ny-1 do begin
    for k=0L,nz-1 do begin
       dist_pxl = sqrt((i-ix0)^2+(j-iy0)^2+(k-iz0)^2)
       xdist    = dist_pxl / scale_blob_pxl
       xdist_pow= xdist^npow
       rho_blob[i,j,k] = rho_blob_centre * exp(-xdist_pow)
    endfor
  endfor
endfor

; apply scaling factor to density
r1 = r + rho_blob

; update energy per unit mass
ee1 = e/r1

; write fout
openw,lun,/get,fout,swap_endian=swap_endian
writeu,lun,r1
writeu,lun,px
writeu,lun,py
writeu,lun,pz
writeu,lun,e
writeu,lun,ee1
free_lun,lun

write_mesh,fmshout,xm,ym,zm,dxfile=fdxout,nvar=5,gamma=gamma,swap_endian=swap_endian,/do_ghost

end
