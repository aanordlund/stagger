! $Id: boundary_divb.f90,v 1.31 2013/03/07 03:51:32 remo Exp $
!***********************************************************************
MODULE boundary
  real t_Bbdry, Bscale_top, Bscale_bot, lb_eampl, t_bdry, eetop_frac, Uy_bdry
  real rbot, pbot, ebot, htop
  real Bx0, By0, Bz0, rmin, pb_fact, uy_max
  real pdamp, rtop_frac, fmtop_frac, divb_max
  real ampxy, ampyz, period, omegap, akx, aky, akz, kxp, kyp, kzp
  real(kind=8) rub,pyub,elb,eub,pub,pubt,dpydtub
  real, allocatable, dimension(:,:,:):: Bxl, Byl, Bzl, Bxu, Byu, Bzu
  real, allocatable, dimension(:,:,:):: Exl, Eyl, Ezl, Exu, Eyu, Ezu
  logical debug, bdry_first, Binflow
  integer verbose, lb1, ndump, idump

END module

!-----------------------------------------------------------------------
SUBROUTINE init_boundary
  USE params
  USE arrays
  USE boundary
  implicit none
  character(len=mid):: id='$Id: boundary_divb.f90,v 1.31 2013/03/07 03:51:32 remo Exp $'

  call print_id(id)

  if (mpi_y == 0) then
    lb = 6
    allocate (Bxl(mx,lb,mz),Byl(mx,lb,mz),Bzl(mx,lb,mz))
  else
    lb = 1
  end if
  if (mpi_y == mpi_ny-1) then
    ub = my-5
    allocate (Bxu(mx,my-ub,mz),Byu(mx,my-ub,mz),Bzu(mx,my-ub,mz))
  else
    ub = my
  end if

  t_bdry = 0.01                                                         ! bdry decay time
  t_Bbdry  = 0.01                                                       ! bdry decay time for B
  eetop_frac = 1.                                                       ! fraction of current eetop
  eetop = -1.                                                           ! signal setting eetop
  rtop_frac=0.05                                                        ! fraction of current rtop
  fmtop_frac=1.0                                                        ! fraction fmass correction for rho(lb)
  pdamp = 0.
  Uy_bdry = 0.1
  lb_eampl = 0.
  rbot = -1.
  htop = 0.
  rmin = 1e-3
  pb_fact = 1.2
  uy_max = 2.                                                           ! 20 km/s with solar scaling
  Bscale_top = 10.
  Bscale_bot = 10.
  Bx0 = 0.
  By0 = 0.
  Bz0 = 0.
  Binflow = .true.
  debug = .false.
  verbose = 0
  bdry_first = .true.
  do_stratified = .true.
  ampxy = 0.
  ampyz = 0.
  period = 0.
  akx = 1.
  aky = 1.
  akz = 1.

  call read_boundary

  ndump=25
  if (do_mhd) ndump=40
  idump=0

END

!-----------------------------------------------------------------------
SUBROUTINE read_boundary
  USE params
  USE boundary
  implicit none
  namelist /bdry/ lb, ub, t_bdry, t_Bbdry, Bscale_top, Bscale_bot, eetop_frac, lb_eampl, &
                  htop, rtop_frac, fmtop_frac, eetop, rbot, ebot, pdamp, &
                  Bx0, By0, Bz0, Binflow, Uy_bdry, do_stratified, &
                  rmin, pb_fact, uy_max, debug, verbose, &
                  ampxy, ampyz, period, akx, aky, akz

  rewind (stdin); read (stdin,bdry)
  if (master) write (*,bdry)
END

!-----------------------------------------------------------------------
SUBROUTINE viscosity_boundary (nu, cs)
  USE params
  implicit none
  real, dimension(mx,my,mz):: nu, cs
  integer iz

  if (mpi_y==0) then                                                    ! top bdry
    do iz=izs,ize
      nu(:,1:lb,iz) = 0.3*cs(:,1:lb,iz)                                 ! hardwire high viscosity 
    end do
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE density_boundary (r,lnr,py,e)
  USE params
  USE boundary
  USE eos
  implicit none
  real, dimension(mx,my,mz):: r,lnr,py,e
  integer ix,iy,iz
  real efact, lnrmin, lnrmax
  real(kind=8) tmp(3)
  logical do_io, flag

if (do_trace) print *, 'density_BC: mpi_rank,y,ny = ', mpi_rank, mpi_y, mpi_ny

  call dumpl(r,'density1:r','bdry.dmp',0+ndump*idump)

  if (ub < my) then                                                     ! bottom BC
    call extrapolate_center_upper(lnr)                                  ! extrapolate in the log
    do iz=izs,ize
    do iy=ub+1,my
    do ix=1,mx
      r(ix,iy,iz)=exp(lnr(ix,iy,iz))                                    ! consistent density
    end do 
    end do 
    end do
  end if

  call haverage_subr (r, rav)                                           ! horizontally averaged rho
  call haverage_subr (e, eav)                                           ! horizontally averaged e

  if (lb > 1) then                                                      ! top BC
    call extrapolate_center_lower(lnr)                                  ! extrapolate in the log
    lnrmin = alog(rmin*real(rav(lb)))                                   ! min lnrho value
    lnrmax = alog(3.*real(rav(lb)))                                     ! max lnrho value in inflows
    do iz=izs,ize
      do ix=1,mx
        do iy=1,lb
          lnr(ix,iy,iz) = max(lnr(ix,iy,iz),lnrmin)                     ! clamp extreme values
          lnr(ix,iy,iz) = min(lnr(ix,iy,iz),lnrmax)                     ! clamp extreme values
          r(ix,iy,iz) = exp(lnr(ix,iy,iz))                              ! consistent density
        end do
      end do
    end do

    if (eetop_frac == 1.) then
      eetop = eav(lb)/rav(lb)                                           ! set
    else
      !$omp single                                                      ! only on master thread
      if (eetop.lt.0) eetop=eav(lb)/rav(lb)                             ! top boundary energy
      eetop = (1.-eetop_frac)*eetop + eetop_frac*eav(lb)/rav(lb)        ! gradual change (optional)
      !$omp end single
    end if
    e(:,1:lb-1,izs:ize)=r(:,1:lb-1,izs:ize)*eetop                       ! set values in ghost zone
  end if
  if ((do_io (t, tsnap, isnap+isnap0, nsnap) .or. flag('snapshot') &
  .or. do_io (t, tscr, iscr+iscr0, nscr) .or. flag('scratch') &
  .or. debug) .and. omp_master .and. isubstep==timeorder) print *,'eetop =',eetop

  if (mpi_y == mpi_ny-1 .and. rbot < 0.) then                           ! compute rbot and ebot if not set
!$omp barrier
!$omp single
    rbot=sum(r(:,ub,:))/(mx*mz)                                         ! save density
    ebot=sum(e(:,ub,:))/(mx*mz)                                         ! save density
    tmp(1) = rbot
    tmp(2) = ebot
    call haverage_mpi (tmp, 2)
    rbot = tmp(1)
    ebot = tmp(2)
!$omp end single
    if (master) print *,'ub, rbot, ebot =', ub, rbot, ebot
  end if
  call dumpl(r,'density2:r','bdry.dmp',1+ndump*idump)

END

!-----------------------------------------------------------------------
SUBROUTINE energy_boundary (r,lnr,Ux,Uy,Uz,e,ee,p,Bx,By,Bz)
  USE params
  USE eos
  USE forcing
  USE boundary
  implicit none
  real, dimension(mx,my,mz):: r,lnr,Ux,Uy,Uz,e,ee,p,Bx,By,Bz

  if (ub < my) then                                                     ! bottom BC
    call extrapolate_center_upper(ee)                                   ! extrapolate at bottom
    e(:,ub+1:my,izs:ize)=r(:,ub+1:my,izs:ize)*ee(:,ub+1:my,izs:ize)
  end if
  call dumpl(e,'energy2:e','bdry.dmp',2+ndump*idump)
  call dumpl(ee,'energy2:ee','bdry.dmp',3+ndump*idump)
END

!-----------------------------------------------------------------------
SUBROUTINE passive_boundary (r,Ux,Uy,Uz,d,dd)
  USE params
  implicit none
  real, dimension(mx,my,mz):: r,Ux,Uy,Uz,d,dd
  integer iy, iz

  call symmetric_center(dd)
  if (mpi_y == 0) then                                                  ! top BC
    do iz=izs,ize
    do iy=1,lb-1
      d(:,iy,iz)=r(:,iy,iz)*dd(:,iy,iz)
    end do
    end do
  end if

  if (mpi_y == mpi_ny-1) then                                           ! bottom BC
    do iz=izs,ize
    do iy=ub+1,my
      d(:,iy,iz)=r(:,iy,iz)*dd(:,iy,iz)
    end do
    end do
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE velocity_boundary (r,xdnr,ydnr,zdnr,Ux,Uy,Uz,px,py,pz,e,Bx,By,Bz)
  USE params
  USE arrays, only: scr1
  USE boundary
  !USE arrays, ONLY: scr1
  implicit none
  real, dimension(mx,my,mz):: r,xdnr,ydnr,zdnr,Ux,Uy,Uz,px,py,pz,e,Bx,By,Bz
  real p, rin, rtot, fmout, fmtot, uyin, uytot
  integer ix, iy, iz
  integer im1,ip1,km1,kp1

    if (do_trace) print 1, 'velocity_BC: mpi_rank,y,ny = ', mpi_rank, mpi_y, mpi_ny, omp_mythread
1   format(1x,a,4i6)
!
!  Top boundary, vanishing velocity derivatives
!
    call dumpl(Ux,'velocity1;Ux','bdry.dmp',4+ndump*idump)
    call dumpl(Uy,'velocity1;Uy','bdry.dmp',5+ndump*idump)
    call dumpl(Uz,'velocity1;Uz','bdry.dmp',6+ndump*idump)

! call smooth3_set(Uy,scr1)
  do iz=izs,ize
    km1=mod(iz+mz-2,mz)+1
    kp1=mod(iz+mz  ,mz)+1
    do ix=1,mx
      im1=mod(ix+mx-2,mx)+1
      ip1=mod(ix+mx  ,mx)+1
!     Uy(ix,lb,iz) = Uy(ix,lb+1,iz)
!     Uy(ix,lb+1,iz) = scr1(ix,lb+1,iz)
      Uy(ix,lb+1,iz) = 0.25*(0.25*Uy(im1,lb+2,km1)+0.5*Uy(ix,lb+2,km1)+0.25*Uy(ip1,lb+2,km1)) &
                      +0.5 *(0.25*Uy(im1,lb+2,iz) +0.5*Uy(ix,lb+2,iz) +0.25*Uy(ip1,lb+2,iz))  &
                      +0.25*(0.25*Uy(im1,lb+2,kp1)+0.5*Uy(ix,lb+2,kp1)+0.25*Uy(ip1,lb+2,kp1))
    end do
  end do

  call constant_center_lower (Ux)
  call constant_face_lower  (Uy)
  call constant_center_lower (Uz)
  !!$omp barrier
  !call smooth3t_set (Uy, scr1)
  !Uy(:,1:lb,izs:ize) = scr1(:,1:lb,izs:ize)
    if (do_trace) print 1,'velocity: 2', mpi_rank, mpi_y, mpi_ny, omp_mythread
!
!  Top boundary, impose maximum velocity -- relevant when the 
!  density is clambed and cannot become lower
!
  if (lb > 1) then
    do iz=izs,ize
    do ix=1,mx
      do iy=1,lb+1
        ux(ix,iy,iz) = max(min(ux(ix,iy,iz),+uy_max),-uy_max)
        uy(ix,iy,iz) = max(min(uy(ix,iy,iz),+uy_max),-uy_max)
        uz(ix,iy,iz) = max(min(uz(ix,iy,iz),+uy_max),-uy_max)
      end do
      if (uy(ix,lb+1,iz) > 0. .or. uy(ix,lb,iz) > 0.) then
        do iy=1,lb
          uy(ix,iy,iz) = min(uy(ix,iy,iz),uy(ix,lb+1,iz))
        end do
      end if
    end do
    end do
!
!  Recompute mass fluxes
!
    do iz=izs,ize
      do iy=1,lb
        px(:,iy,iz)=xdnr(:,iy,iz)*Ux(:,iy,iz)
        py(:,iy,iz)=ydnr(:,iy,iz)*Uy(:,iy,iz)
        pz(:,iy,iz)=zdnr(:,iy,iz)*Uz(:,iy,iz)
      end do
      py(:,lb+1,iz)=ydnr(:,lb+1,iz)*Uy(:,lb+1,iz)
    end do
  end if
    if(do_trace) print 1,'velocity: 4', mpi_rank, mpi_y, mpi_ny, omp_mythread

  call symmetric_center_upper (Ux)
  call symmetric_face_upper   (Uy)
  call symmetric_center_upper (Uz)
!
!  Recompute mass fluxes
!
  if (mpi_y == mpi_ny-1) then
    do iz=izs,ize
    do iy=ub,my
      px(:,iy,iz)=xdnr(:,iy,iz)*Ux(:,iy,iz)
      py(:,iy,iz)=ydnr(:,iy,iz)*Uy(:,iy,iz)
      pz(:,iy,iz)=zdnr(:,iy,iz)*Uz(:,iy,iz)
    end do
    end do
  end if
    if(do_trace) print 1,'velocity: 5', mpi_rank, mpi_y, mpi_ny, omp_mythread
    call dumpl(Ux,'velocity2;Ux','bdry.dmp',7+ndump*idump)
    call dumpl(Uy,'velocity2;Uy','bdry.dmp',8+ndump*idump)
    call dumpl(Uz,'velocity2;Uz','bdry.dmp',9+ndump*idump)
END

!-----------------------------------------------------------------------
SUBROUTINE mfield_boundary (Ux,Uy,Uz,Bx,By,Bz)
  USE params
  USE boundary
  implicit none
  real, dimension(mx,my,mz):: Ux,Uy,Uz,Bx,By,Bz
  integer ix, iy, iz
  real f, p, q
  character(len=mid), save:: id='mfield_boundary $Id: boundary_divb.f90,v 1.31 2013/03/07 03:51:32 remo Exp $'

  call print_id (id)

  if (mpi_y == 0) then                                                  ! top BC
    if (master.and.debug) then
      print *,'Bx (before)=',Bx(1,0:9,1)
      print *,'By (before)=',By(1,0:9,1)
      print *,'Bz (before)=',Bz(1,0:9,1)
    end if
    call dumpl(Bx,'mfield1:Bx','bdry.dmp',25+ndump*idump)
    call dumpl(By,'mfield1:By','bdry.dmp',26+ndump*idump)
    call dumpl(Bz,'mfield1:Bz','bdry.dmp',27+ndump*idump)

    do iz=izs,ize; do iy=1,3; do ix=1,mx
      p = (4-iy)/Bscale_top
      q = 1.-p
      Bx(ix,iy,iz) = Bx(ix,4,iz)*q + Bx0*p                              ! make regular in 1:3
      By(ix,iy,iz) = By(ix,4,iz)*q + By0*p                              ! make regular in 1:3
      Bz(ix,iy,iz) = Bz(ix,4,iz)*q + Bz0*p                              ! make regular in 1:3
    end do; end do; end do

    if (master.and.debug) then
      print *,'Bx (after)=',Bx(1,0:9,1)
      print *,'By (after)=',By(1,0:9,1)
      print *,'Bz (after)=',Bz(1,0:9,1)
    end if
  end if

!-----------------------------------------------------------------------
!
!  Lower boundary: Store & restore original ghost zone field, to avoid
!  destroying div(B).  But set up for computing the electric fiels as
!  if the field in inflows is constant.  This gets multiplied by the
!  inflow velocity field, which vanishes btw inflow and outflow, thus
!  avoiding creating a discontinuity in the electric field.
!
!-----------------------------------------------------------------------

  if (mpi_y == mpi_ny-1) then                                           ! bottom BC
    do iz=izs,ize
      do iy=1,my-ub
        Bxu(:,iy,iz)=Bx(:,ub+iy,iz)                                     ! store orig bdry B
        Byu(:,iy,iz)=By(:,ub+iy,iz)
        Bzu(:,iy,iz)=Bz(:,ub+iy,iz)
      end do
    end do

    if (Binflow) then
      call symmetric_center_upper (Bx)
      call symmetric_center_upper (Bz)
      call symmetric_face_upper (By)
      do iz=izs,ize
      do ix=1,mx
        f = 1./(1.+exp(Uy(ix,ub,iz)/Uy_bdry))                           ! -> 1 in inflows
        do iy=ub+1,my
          Bx(ix,iy,iz) = (1.-f)*Bx(ix,iy,iz) + f*Bx0
          Bz(ix,iy,iz) = (1.-f)*Bz(ix,iy,iz) + f*Bz0
        enddo
      enddo
      enddo
    else
      call mixed_center_upper(Bx,Bscale_bot)
      call mixed_center_upper(Bz,Bscale_bot)
      call extrapolate_face_upper(By)
    end if
  end if
    call dumpl(Bx,'mfield2:Bx','bdry.dmp',28+ndump*idump)
    call dumpl(By,'mfield2:By','bdry.dmp',29+ndump*idump)
    call dumpl(Bz,'mfield2:Bz','bdry.dmp',30+ndump*idump)
END

!-----------------------------------------------------------------------
SUBROUTINE ecurrent_boundary (Ex, Ey, Ez, Jx, Jy, Jz)
  USE params
  USE boundary
  implicit none
  real, dimension(mx,my,mz):: Ex, Ey, Ez, Jx, Jy, Jz

  if (mpi_y==0) then
    if (omp_master) then
      allocate (Exl(mx,lb,mz), Ezl(mx,lb,mz))
    end if
    !$omp barrier
    Exl(:,:,izs:ize) = Ex(:,1:lb,izs:ize)
    Ezl(:,:,izs:ize) = Ez(:,1:lb,izs:ize)
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE efield_boundary (Ex, Ey, Ez, Bx_y, Bx_z, By_z, By_x, Bz_x, Bz_y, &
                            Bx, By, Bz, Ux_y, Ux_z, Uy_z, Uy_x, Uz_x, Uz_y, &
                            Ux, Uy, Uz)
  USE params
  USE boundary
  USE arrays, only: scr1, scr2
  implicit none
  real, dimension(mx,my,mz):: Ex, Ey, Ez, Bx_y, Bx_z, By_z, By_x, Bz_x, Bz_y, &
                              Ux_y, Ux_z, Uy_z, Uy_x, Uz_x, Uz_y, &
                              Ux, Uy, Uz, Bx, By, Bz
  integer ix, iy, iz
  real f, ct

  character(len=mid), save:: id='efield_boundary $Id: boundary_divb.f90,v 1.31 2013/03/07 03:51:32 remo Exp $'

  call print_id (id)

!-----------------------------------------------------------------------
!  Top boundary:  The template magnetic field is from a potential
!  extrapolation, and we construct an E-field that drives the real
!  B-field towards the template field.  Only Ex and Ez are involved
!  in this; Ey depends only on horizontal derivatives and interpolations,
!  so is in that sense OK.
!-----------------------------------------------------------------------
  ct = 1./t_Bbdry
  if (mpi_y == 0) then                                                  ! top BC
   do iz=izs,ize
    do iy=lb,1,-1
     Bxl(:,iy,iz) = Bx(:,lb,iz)*(1.-(lb-iy)/Bscale_top)                 ! guide field
     Bzl(:,iy,iz) = Bz(:,lb,iz)*(1.-(lb-iy)/Bscale_top)                 ! guide field
     Ex(:,iy,iz) = Ex(:,iy+1,iz) - (Bzl(:,iy,iz)-Bz(:,iy,iz))* &        ! drive towards guide field
       ct*(ym(iy+1)-ym(iy))
     Ez(:,iy,iz) = Ez(:,iy+1,iz) + (Bxl(:,iy,iz)-Bx(:,iy,iz))* &        ! drive towards guide field
       ct*(ym(iy+1)-ym(iy))
    end do
    do iy=4,lb
      Ex(:,iy,iz) = Ex(:,iy,iz) + Exl(:,iy,iz)                          ! add the resistive part
      Ez(:,iy,iz) = Ez(:,iy,iz) + Ezl(:,iy,iz)                          ! add the resistive part
    end do
   end do
  end if
!$omp barrier
  if (omp_master) deallocate (Exl, Ezl)

!-----------------------------------------------------------------------
!  Bottom boundary:  Ex and Ez are already good, having been computed
!  from regularized velocity and magnetic field values.  However, the
!  values more than 3 grids outside the boundary are contaminated, due
!  to the ydn(Bxz) and ydn(Uxz) interpolations, so these need to be
!  regularized.
!-----------------------------------------------------------------------
  if (mpi_y == mpi_ny-1) then                                           ! bottom BC
    if (Binflow) then
      do iz=izs,ize
       do ix=1,mx
        do iy=ub+4,my
          Ex(ix,iy,iz) = Ex(ix,ub+3,iz)                                 ! regularize
          Ez(ix,iy,iz) = Ez(ix,ub+3,iz)                                 ! regularize
        enddo
       enddo
      enddo
    else
      do iz=izs,ize
        scr1(:,ub,iz) = -(Bz_y(:,ub,iz)-Bz0)*ct
        scr2(:,ub,iz) = +(Bx_y(:,ub,iz)-Bx0)*ct
      enddo
      call derivative_face_upper (Ex, scr1) 
      call derivative_face_upper (Ez, scr2) 
    end if

    do iz=izs,ize
      Bx(:,ub+1:ub+2,iz)=Bxu(:,1:2,iz)                                    ! restore lower bdry B
      By(:,ub+1:ub+3,iz)=Byu(:,1:3,iz)                                    ! restore lower bdry B
      Bz(:,ub+1:ub+2,iz)=Bzu(:,1:2,iz)                                    ! restore lower bdry B
    end do
  end if
  call dumpl(Ex,'efield2:Ex','bdry.dmp',31+ndump*idump)
  call dumpl(Ey,'efield2:Ey','bdry.dmp',32+ndump*idump)
  call dumpl(Ez,'efield2:Ez','bdry.dmp',33+ndump*idump)

END

!-----------------------------------------------------------------------
SUBROUTINE regularize (f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
!
  if (do_trace) print *,'regularize 1:',mpi_rank,omp_mythread,mpi_y
  if (mpi_y ==        0) call symmetric_center_lower (f)
  if (mpi_y == mpi_ny-1) call symmetric_center_upper (f)
  if (do_trace) print *,'regularize 2:',mpi_rank,omp_mythread,mpi_y
END

!-----------------------------------------------------------------------
SUBROUTINE regularize_face (f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
!
  if (mpi_y ==        0) call symmetric_face_lower (f)
  if (mpi_y == mpi_ny-1) call symmetric_face_upper (f)
END

!-----------------------------------------------------------------------
SUBROUTINE ddt_boundary (r,px,py,pz,e,p,Bx,By,Bz, &
                         drdt,dpxdt,dpydt,dpzdt, &
                         dedt,dBxdt,dBydt,dBzdt)
  USE params
  USE forcing
  USE eos
  USE boundary
  implicit none
  real, dimension(mx,my,mz):: r,px,py,pz,e,p,Bx,By,Bz, &
                              drdt,dpxdt,dpydt,dpzdt, &
                              dedt,dBxdt,dBydt,dBzdt
  integer ix,iz,ixp1,izp1
  real(kind=8) rub1,pyub1,elb1,eub1,ct,pub1,pubt1,dpydtub1,scr,htopi
  real F1,F2,F3,F4,F5,ee,hh,drdt_add
  integer, parameter:: mprint=10
  integer nprint
  real, dimension(mx,mz)::pb
  real(kind=8), dimension(6):: tmp
  character(len=mid), save:: id='ddt_boundary $Id: boundary_divb.f90,v 1.31 2013/03/07 03:51:32 remo Exp $'
!.......................................................................
  call print_id (id)

!$omp barrier
  if (debug) print *,'bdry',mpi_y,ym(1),rbot,ebot
    call dumpl(r,'ddt1:r','bdry.dmp',10+ndump*idump)
    call dumpl(px,'ddt1:px','bdry.dmp',11+ndump*idump)
    call dumpl(py,'ddt1:py','bdry.dmp',12+ndump*idump)
    call dumpl(pz,'ddt1:pz','bdry.dmp',13+ndump*idump)
    call dumpl(e,'ddt1:e','bdry.dmp',14+ndump*idump)
    call dumpl(drdt,'ddt1:drdt','bdry.dmp',15+ndump*idump)
    call dumpl(dpxdt,'ddt1:dpxdt','bdry.dmp',16+ndump*idump)
    call dumpl(dpydt,'ddt1:dpydt','bdry.dmp',17+ndump*idump)
    call dumpl(dpzdt,'ddt1:dpzdt','bdry.dmp',18+ndump*idump)
    call dumpl(dedt,'ddt1:dedt','bdry.dmp',19+ndump*idump)
    if (do_mhd) then
      call dumpl(Bx,'ddt1:Bx','bdry.dmp',34+ndump*idump)
      call dumpl(By,'ddt1:By','bdry.dmp',35+ndump*idump)
      call dumpl(Bz,'ddt1:Bz','bdry.dmp',36+ndump*idump)
    end if

  if (mpi_y == mpi_ny-1) then                                           ! bottom BC
    nprint = 0
    if (master .and. verbose>0 .and. isubstep==1) &
      print *,'pb:',pbub(mx/2+1,mz/2+1),pbot0(mx/2+1,mz/2+1)            ! keep track of spot center

    ct = 1.0/t_bdry                                                     ! damping constant
    do iz=izs,ize
      izp1 = mod(iz,mz)+1
      do ix=1,mx
        if (do_mhd) then
          ixp1 = mod(ix,mx)+1
          pb(ix,iz) = 0.125*((bx(ix,ub,iz)+bx(ixp1,ub ,iz  ))**2 + &
                            (by(ix,ub,iz)+by(ix  ,ub+1,iz  ))**2 + &
                            (bz(ix,ub,iz)+bz(ix  ,ub  ,izp1))**2)
        else
          pb(ix,iz) = 0.
        end if
      end do
    end do
!$omp single
    rub = 0.
    eub = 0.
    pub = 0.
    pubt = 0.
    pyub = 0.
    dpydtub = 0.
!$omp end single
    rub1 = 0.
    eub1 = 0.
    pub1 = 0.
    pubt1 = 0.
    pyub1 = 0.
    dpydtub1 = 0.
    do iz=izs,ize
      rub1 = rub1 + sum(r(:,ub,iz))
      eub1 = eub1 + sum(e(:,ub,iz))
      pub1 = pub1 + sum(pbot0(:,iz))
      pubt1 = pubt1 + sum(pbot0(:,iz)+pb(:,iz))
      pyub1 = pyub1 + sum(1.5*py(:,ub,iz)-0.5*py(:,ub-1,iz))
       ! py is regular(?)
      dpydtub1 = dpydtub1 + sum(1.5*dpydt(:,ub,iz)-0.5*dpydt(:,ub-1,iz))
       ! extrapolate to bdry
    end do
!$omp critical
    rub = rub + rub1/(mx*mz)
    eub = eub + eub1/(mx*mz)
    pub = pub + pub1/(mx*mz)
    pubt = pubt + pubt1/(mx*mz)
    pyub = pyub + pyub1/(mx*mz)
    dpydtub = dpydtub + dpydtub1/(mx*mz)
!$omp end critical
!$omp barrier
    tmp(1) = rub
    tmp(2) = eub
    tmp(3) = pub
    tmp(4) = pubt
    tmp(5) = pyub
    tmp(6) = dpydtub
    call haverage_mpi (tmp, 6)
    rub = tmp(1)
    eub = tmp(2)
    pub = tmp(3)
    pubt = tmp(4)
    pyub = tmp(5)
    dpydtub = tmp(6)

    if (omp_master .and. debug) then
      print*,'rub',rub
      print*,'eub',eub
      print*,'pub',pub
      print*,'pubt',pubt
      print*,'pyub',pyub
      print*,'dpydtub',dpydtub
      print*,'pbot0(test)',pbot0(10,10)
      print*,'dlnpdE_r(test)',dlnpdE_r(10,10)
    end if

    ct = 0.5/t_bdry                                                                                        ! damping constant
    if (omp_master .and. debug) print*,'ct',ct
    do iz=izs,ize
    do ix=1,mx
      if (py(ix,ub,iz) > 0.) then                                       ! for outgoing flows..
        F1 = dlnpdlnr_E(ix,iz)                                          ! EOS table dlnpdlnr_E
        F2 = dlnpdE_r(ix,iz)                                            ! EOS table dlnpdE_r
        ee = e(ix,ub,iz)/r(ix,ub,iz)                                    ! E in notes
        hh = ee+pbot0(ix,iz)/r(ix,ub,iz)                                ! H in notes
        F5 = F1 + F2*(hh-ee)                                            ! denominator of F3 & F4
        F3 = (F1 - F2*ee)/F5
        F4 = F2/F5
        drdt_add = F3*((rbot-r(ix,ub,iz))*ct-drdt(ix,ub,iz)) + &        ! Eq.18 boundaries.tex
                   F4*((ebot-e(ix,ub,iz))*ct-dedt(ix,ub,iz))
        if (debug .and. nprint < mprint) then
          print 1,ix,F1,F2*ee,F3,F4*ee,F5,ee,hh,r(ix,ub,iz),drdt(ix,ub,iz),drdt_add
1         format(i5,5f10.3,5g12.4)
        end if
        nprint = nprint+1
        drdt(ix,ub,iz) = drdt(ix,ub,iz) + drdt_add
        dedt(ix,ub,iz) = dedt(ix,ub,iz) + hh*drdt_add                   ! Eq.21 boundaries.tex
      else                                                              ! for incoming flows
        dedt(ix,ub,iz) = (ebot-e(ix,ub,iz))*ct                          ! damp int.en. deviations
        drdt(ix,ub,iz) = (rbot-r(ix,ub,iz))*ct                          ! damp density deviations
        dpxdt(ix,ub,iz) = -px(ix,ub,iz)*ct                              ! damp horizontal motions
        dpzdt(ix,ub,iz) = -pz(ix,ub,iz)*ct
      endif
      drdt (ix,ub+1:my,iz) = 0.                                         ! make sure nothing bad happens in ghost zones
      dpxdt(ix,ub+1:my,iz) = 0.
      dpydt(ix,ub+1:my,iz) = 0.
      dpzdt(ix,ub+1:my,iz) = 0.
      dedt (ix,ub+1:my,iz) = 0.
    end do
    end do
    if (do_mhd) then                                                    ! MHD case
      dBxdt(:,ub+3:,izs:ize) = 0.                                       ! zap the dB/dt in contaminated region
      dBydt(:,ub+4:,izs:ize) = 0.                                       ! zap the dB/dt in contaminated region
      dBzdt(:,ub+3:,izs:ize) = 0.                                       ! zap the dB/dt in contaminated region
    end if
  end if

  call haverage_subr (p, pav)                                           ! needed below
  if (mpi_y == 0) then                                                  ! top BC
    htopi = gy*rav(lb)/pav(lb)                                          ! inverse pressure scale height
    do iz=izs,ize
    do ix=1,mx
      if (py(ix,lb+1,iz) > 0. .or. py(ix,lb,iz) > 0.) then              ! for inflows ..
        drdt(ix,lb,iz) = drdt(ix,lb,iz) &                               ! let the density
          + (py(ix,lb+1,iz)-py(ix,lb,iz))/dym(lb) &                     ! remove the -ddyup(py) part!
          - htopi*py(ix,lb+1,iz)                                        ! replace with -py/H term
      end if
      drdt (ix,1:lb-1,iz) = 0.                                          ! zap the ghost zone dfdt's
      dpxdt(ix,1:lb-1,iz) = 0.
      dpydt(ix,1:lb  ,iz) = 0.
      dpzdt(ix,1:lb-1,iz) = 0.
      dedt (ix,1:lb-1,iz) = 0.
      if (do_mhd) then                                                  ! MHD case
        dBxdt(ix,1:3,iz)=0.                                             ! the E-field is contaminated up to iy=3
        dBydt(ix,1:3,iz)=0.                                             ! the E-field is contaminated up to iy=3
        dBzdt(ix,1:3,iz)=0.                                             ! might as well zap the whole ghost zone
      endif
    end do
    end do
  end if
!$omp barrier
  call dumpl(drdt,'ddt2:drdt','bdry.dmp',20+ndump*idump)
  call dumpl(dpxdt,'ddt2:dpxdt','bdry.dmp',21+ndump*idump)
  call dumpl(dpydt,'ddt2:dpydt','bdry.dmp',22+ndump*idump)
  call dumpl(dpzdt,'ddt2:dpzdt','bdry.dmp',23+ndump*idump)
  call dumpl(dedt,'ddt2:dedt','bdry.dmp',24+ndump*idump)
  if (do_mhd) then
    call dumpl(dBxdt,'ddt2:dBxdt','bdry.dmp',37+ndump*idump)
    call dumpl(dBydt,'ddt2:dBydt','bdry.dmp',38+ndump*idump)
    call dumpl(dBzdt,'ddt2:dBzdt','bdry.dmp',39+ndump*idump)
  end if
!$omp single
  idump=idump+1
!$omp end single

END
