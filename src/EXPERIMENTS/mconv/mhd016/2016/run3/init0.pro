@stagger_y_low

file='snapshot_00025'

sz=1008
iy0=30
my=500
ny=25
iy1=iy0+ny-1

close,1
openr,1,file+'.dat'
a=assoc(1,fltarr(2016,ny),4L*2016L*iy0)

rh=fltarr(sz,ny,sz)
uy=fltarr(sz,ny,sz)
tt=fltarr(sz,ny,sz)
by=fltarr(sz,ny,sz)

iv=0
for iz=0,sz-1 do begin
  if (iz mod 100) eq 0 then print,iz
  tmp=rebin(a[*,*,(my/ny)*(2*iz+2016L*iv)],sz,ny)
  rh[*,*,iz]=tmp
end

iv=2
for iz=0,sz-1 do begin
  if (iz mod 100) eq 0 then print,iz
  tmp=rebin(a[*,*,(my/ny)*(2*iz+2016L*iv)],sz,ny)
  uy[*,*,iz]=tmp
end
uy=yup(uy/exp(ydown(alog(rh))))

iv=5
for iz=0,sz-1 do begin
  if (iz mod 100) eq 0 then print,iz
  tmp=rebin(a[*,*,(my/ny)*(2*iz+2016L*iv)],sz,ny)
  tt[*,*,iz]=tmp
end

iv=7
for iz=0,sz-1 do begin
  if (iz mod 100) eq 0 then print,iz
  tmp=rebin(a[*,*,(my/ny)*(2*iz+2016L*iv)],sz,ny)
  by[*,*,iz]=tmp
end

ny=1
a=assoc(1,fltarr(2016,ny))
iv=5
for iz=0,sz-1 do begin
  if (iz mod 100) eq 0 then print,iz
  tmp=rebin(a[*,*,(my/ny)*(2*iz+2016L*iv)],sz,ny)
  tt[*,0,iz]=tmp
end

save,rh,uy,tt,by,file=file+'.idlsave'

END
