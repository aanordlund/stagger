; IDL Version 7.1.1 (linux x86_64 m64)
; Journal File for aake@fend01.dcsc.ku.dk
; Working directory: /users/astro/andersl/scratch/mconv/mhd016/2016/run3
; Date: Thu Mar 24 10:25:06 2011
 
set_plot,'z' & device,set_res=[1,1]*1008
loadct,0 & image,tt[*,0,*]<0.3 & labels
;reading magnetic features data from circles_25.txt
tvlct,/get,r,g,b & write_png,'ic_features.png',tvrd(),r,g,b
color & image,tt[*,15,*] & labels
;reading magnetic features data from circles_25.txt
tvlct,/get,r,g,b & write_png,'tt_features.png',tvrd(),r,g,b
color & image,-uy[*,2,*],/zero,zr=[-1,1]*0.7 & labels
;reading magnetic features data from circles_25.txt
tvlct,/get,r,g,b & write_png,'uy32_features.png',tvrd(),r,g,b
color & image,-uy[*,12,*],/zero,zr=[-1,1]*0.7 & labels
;reading magnetic features data from circles_25.txt
tvlct,/get,r,g,b & write_png,'uy42_features.png',tvrd(),r,g,b
color & image,by[*,2,*],/zero,im=im & labels & color
;reading magnetic features data from circles_25.txt
tvlct,/get,r,g,b & write_png,'by32_features.png',tvrd(),r,g,b
color & image,by[*,12,*],/zero,im=im & labels & color
;reading magnetic features data from circles_25.txt
tvlct,/get,r,g,b & write_png,'by42_features.png',tvrd(),r,g,b
set_plot,'x'
