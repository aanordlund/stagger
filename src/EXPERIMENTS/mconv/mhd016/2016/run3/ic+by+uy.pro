t0=10
t1=40
nt=t1-t0+1

m=2016
ic=fltarr(m,m,nt)
by1=fltarr(m,m,nt)
by2=fltarr(m,m,nt)
uy1=fltarr(m,m,nt)
uy2=fltarr(m,m,nt)

for t=t0,t1 do begin
  ic[*,*,t-t0]=rm(t,5,iy=0)
  ic[*,*,t-t0]=ic[*,*,t-t0]/aver(ic[*,*,t-t0])
  by1[*,*,t-t0]=rm(t,7,iy=32)
  by2[*,*,t-t0]=rm(t,7,iy=42)
  r1=rm(t,0,iy=33)
  r0=rm(t,0,iy=32)
  uy1[*,*,t-t0]=rm(t,2,iy=33)*2./(r0+r1)
  r1=rm(t,0,iy=43)
  r0=rm(t,0,iy=42)
  uy2[*,*,t-t0]=rm(t,2,iy=43)*2./(r0+r1)
  by=by1[*,*,t-t0]
  print,t
end

sz=1008
ic=rebin(ic,sz,sz)
uy1=rebin(uy1,sz,sz)
uy2=rebin(uy2,sz,sz)
by1=rebin(by1,sz,sz)
by2=rebin(by2,sz,sz)
save,ic,by1,by2,uy1,uy2,file='ic+by+uy.idlsave'

END

