!...............................................................................
SUBROUTINE viscosity_boundary_bot (nu, cs)
  USE params
  implicit none
  real, dimension(mx,my,mz) :: nu, cs

END SUBROUTINE viscosity_boundary_bot

!...............................................................................
SUBROUTINE density_boundary_bot (r,lnr,py,e)
  USE params
  USE boundary
  USE arrays, only: scr1
  USE eos
  implicit none
  real, dimension(mx,my,mz) :: r,lnr,py,e
  integer :: ix, iy, iz
  real :: efact, lnrmin, lnrmax, r_prv
  real(kind=8) :: tmp(3)

  ! bottom BC
  if (ub < my) then
     ! extrapolate in the log
     call extrapolate_center_upper(lnr)
     do iz=izs,ize
        do iy=ub+1,my
           do ix=1,mx
              ! consistent density
              r(ix,iy,iz)=exp(lnr(ix,iy,iz))
           end do
        end do
     end do
  end if

  ! compute rbot and ebot if not set
  if (mpi_y == mpi_ny-1 .and. rbot < 0.) then
     !$omp barrier
     !$omp master
     ! average density and energy at bottom boundary
     rbot=sum(r(:,ub,:))/(mx*mz)
     ebot=sum(e(:,ub,:))/(mx*mz)
     tmp(1) = rbot
     tmp(2) = ebot
     call haverage_mpi (tmp, 2)
     rbot = tmp(1)
     ebot = tmp(2)
     !$omp end master
     if (master) print *,'ub, rbot, ebot =', ub, rbot, ebot
  end if
  call dumpn(r,'r1','bdry.dmp',1)

END SUBROUTINE density_boundary_bot

!...............................................................................
SUBROUTINE energy_boundary_bot (r,lnr,Ux,Uy,Uz,e,ee,p,Bx,By,Bz)
  USE params
  USE eos
  USE forcing
  USE boundary
  implicit none
  
  real, dimension(mx,my,mz) :: r,lnr,Ux,Uy,Uz,e,ee,p,Bx,By,Bz

  ! bottom BC
  if (ub < my) then
     ! extrapolate at bottom
     call extrapolate_center_upper(ee) 
     e(:,ub+1:my,izs:ize) = r(:,ub+1:my,izs:ize) * ee(:,ub+1:my,izs:ize)
  end if
END SUBROUTINE energy_boundary_bot

!...............................................................................
SUBROUTINE passive_boundary_bot (r,Ux,Uy,Uz,d,dd)
  USE params
  implicit none
  
  real, dimension(mx,my,mz) :: r,Ux,Uy,Uz,d,dd
  integer :: iy, iz
  
  ! bottom BC
  if (mpi_y == mpi_ny-1) then
     do iz=izs,ize
        do iy=ub+1,my
           d(:,iy,iz)=r(:,iy,iz)*dd(:,iy,iz)
        end do
     end do
  end if
  
END SUBROUTINE passive_boundary_bot

!...............................................................................
SUBROUTINE velocity_boundary_bot (r,xdnr,ydnr,zdnr,Ux,Uy,Uz,px,py,pz,e,Bx,By,Bz)
  USE params
  USE boundary
  implicit none
  real, dimension(mx,my,mz) :: r,xdnr,ydnr,zdnr,Ux,Uy,Uz,px,py,pz,e,Bx,By,Bz
  real :: p, rin, rtot, fmout, fmtot, uyin, uytot, f
  integer :: ix, iy, iz

  call symmetric_center_upper (px)
  call symmetric_face_upper   (py)
  call symmetric_center_upper (pz)

  ! Extrapolate in outflows
  if (mpi_y == mpi_ny-1) then
     do iz=izs,ize
        
        !do ix=1,mx
        !   ! inflows:
        !   if (py(ix,ub,iz) < 0.) then
        !
        !      ! Taper off horizontal velocities linearly, to make the advective
        !      ! terms contribute a weak damping.  With sufficiently large time
        !      ! scale t_bdry, this should not be necessary. The vertical 
        !      ! velocity must ABSOLUTELY NOT be tapered off, since this leads
        !      ! to a systematic <py> > 0.0, which causes imbalance at the
        !      ! boundary -- as has been present with rev. 1.19 of this file
        !
        !      do iy=ub+1,my
        !         f=max(0.0, 1.-(iy-ub)/5.)
        !         Ux(ix,iy,iz)=Ux(ix,ub,iz)*f
        !         Uz(ix,iy,iz)=Uz(ix,ub,iz)*f
        !      end do
        !   endif
        !end do
        !
        !! In order for the symmetrization of U[xyz] above to have an effect
        !! the mass fluxes must be updated accordingy.  Updating at iy=ub
        !! is not necessary (and may cause some numerical round-off noise in
        !! long run; only U-values in the range ub+1:my have been modified
        
        Ux(:,ub+1:my,iz)=px(:,ub+1:my,iz)/xdnr(:,ub+1:my,iz)
        Uy(:,ub+1:my,iz)=py(:,ub+1:my,iz)/ydnr(:,ub+1:my,iz)
        Uz(:,ub+1:my,iz)=pz(:,ub+1:my,iz)/zdnr(:,ub+1:my,iz)
     end do
  end if

  call dumpn(px,'px','bdry.dmp',1)
  call dumpn(py,'py','bdry.dmp',1)
  call dumpn(pz,'pz','bdry.dmp',1)
  
END SUBROUTINE velocity_boundary_bot

!...............................................................................
SUBROUTINE magnetic_pressure_boundary_bot (BB)
  USE params
  USE boundary
  implicit none
  real, dimension(mx,my,mz) :: BB
  logical :: fexists

  if (ub < my) then
     pbub(:,izs:ize) = 0.5 * BB(:,ub,izs:ize)
     if (debug) then
        print *,'pbub:',mpi_rank,minval(pbub(:,izs:ize)),maxval(pbub(:,izs:ize))
     end if
  end if
  
END SUBROUTINE magnetic_pressure_boundary_bot

!...............................................................................
SUBROUTINE mfield_boundary_bot (Ux,Uy,Uz,Bx,By,Bz)
  USE params
  USE boundary
  USE timeintegration
  implicit none
  real, dimension(mx,my,mz):: Ux,Uy,Uz,Bx,By,Bz
  integer ix, iy, iz
  real f
  logical do_io
  character(len=mid), save :: Id='mfield_boundary_bot'

  call print_id (Id)

  !  Lower boundary: Store & restore original ghost zone field, to avoid
  !  destroying div(B).  But set up for computing the electric fiels as
  !  if the field in inflows is constant.  This gets multiplied by the
  !  inflow velocity field, which vanishes btw inflow and outflow, thus
  !  avoiding creating a discontinuity in the electric field.

  ! bottom BC
  if (mpi_y == mpi_ny-1) then
     do iz=izs,ize
        do iy=1,my-ub
           ! store orig bdry B
           Bxu(:,iy,iz)=Bx(:,ub+iy,iz)
           Byu(:,iy,iz)=By(:,ub+iy,iz)
           Bzu(:,iy,iz)=Bz(:,ub+iy,iz)
        end do
     end do
     
     ! handle t_Bgrowth
     if (t_Bgrowth .ne. 0) then
        if (isubstep .eq. 1) then
           dBx0dt=0.
           dBy0dt=0.
           dBz0dt=0.
        else
           dBx0dt = alpha(isubstep)*dBx0dt
           dBy0dt = alpha(isubstep)*dBy0dt
           dBz0dt = alpha(isubstep)*dBz0dt
        endif
        dBx0dt = dBx0dt + Bx0/t_Bgrowth
        dBy0dt = dBy0dt + By0/t_Bgrowth
        dBz0dt = dBz0dt + Bz0/t_Bgrowth
        Bx0 = Bx0 + (dt*beta(isubstep))*dBx0dt
        By0 = By0 + (dt*beta(isubstep))*dBy0dt
        Bz0 = Bz0 + (dt*beta(isubstep))*dBz0dt
        if ((do_io   (t+dt, tsnap, isnap+isnap0, nsnap) &
             .or. do_io (t+dt, tscr , iscr +iscr0 , nscr )  &
             .or. debug) .and. master .and. isubstep==timeorder) &
             print *,'Bx0,By0,Bz0 =',Bx0,By0,Bz0
     endif

     if (Binflow) then
        call extrapolate_center_upper (Bx)
        call extrapolate_center_upper (Bz)
        call extrapolate_face_upper (By)
        do iz=izs,ize
           do ix=1,mx
              ! -> 1 in inflows
              f = 1./(1.+exp(min(Uy(ix,ub,iz)/Uy_bdry,80.))) 
              do iy=ub+1,my
                 Bx(ix,iy,iz) = (1.-f)*Bx(ix,iy,iz) + f*Bx0
                 Bz(ix,iy,iz) = (1.-f)*Bz(ix,iy,iz) + f*Bz0
              enddo
           enddo
        enddo
     else
        call mixed_center_upper(Bx,Bscale_bot)
        call mixed_center_upper(Bz,Bscale_bot)
        call extrapolate_face_upper(By)
     end if
  end if
  
END SUBROUTINE mfield_boundary_bot

!...............................................................................
SUBROUTINE ecurrent_boundary_bot (Ex, Ey, Ez, Jx, Jy, Jz)
  USE params
  USE boundary
  implicit none
  real, dimension(mx,my,mz):: Ex, Ey, Ez, Jx, Jy, Jz

END SUBROUTINE ecurrent_boundary_bot

!...............................................................................
SUBROUTINE efield_boundary_bot (Ex, Ey, Ez, Bx_y, Bx_z, By_z, By_x, Bz_x, Bz_y,&
     Bx, By, Bz, Ux_y, Ux_z, Uy_z, Uy_x, Uz_x, Uz_y, &
     Ux, Uy, Uz)
  USE params
  USE boundary
  USE arrays, only: scr1, scr2
  implicit none
  real, dimension(mx,my,mz) :: Ex, Ey, Ez, Bx_y, Bx_z, By_z, By_x, Bz_x, Bz_y
  real, dimension(mx,my,mz) :: Ux_y, Ux_z, Uy_z, Uy_x, Uz_x, Uz_y
  real, dimension(mx,my,mz) :: Ux, Uy, Uz, Bx, By, Bz
  integer :: ix, iy, iz
  real :: f, ct

  character(len=mid), save :: Id = 'efield_boundary_bot'

  call print_id (Id)
  ct = 1./t_Bbdry

  !  Bottom boundary:  Ex and Ez are already good, having been computed
  !  from regularized velocity and magnetic field values.  However, the
  !  values more than 3 grids outside the boundary are contaminated, due
  !  to the ydn(Bxz) and ydn(Uxz) interpolations, so these need to be
  !  regularized.
  
  ! bottom BC
  if (mpi_y == mpi_ny-1) then 
     if (Binflow) then
        do iz=izs,ize
           do ix=1,mx
              do iy=ub+4,my
                 ! regularize
                 Ex(ix,iy,iz) = Ex(ix,ub+3,iz)
                 Ez(ix,iy,iz) = Ez(ix,ub+3,iz)
              enddo
           enddo
        enddo
     else
        do iz=izs,ize
           scr1(:,ub,iz) = -(Bz_y(:,ub,iz)-Bz0)*ct
           scr2(:,ub,iz) = +(Bx_y(:,ub,iz)-Bx0)*ct
        enddo
        call derivative_face_upper (Ex, scr1) 
        call derivative_face_upper (Ez, scr2) 
     end if

     ! restore lower bdry B
     do iz=izs,ize
        Bx(:,ub+1:ub+2,iz)=Bxu(:,1:2,iz)
        By(:,ub+1:ub+3,iz)=Byu(:,1:3,iz)
        Bz(:,ub+1:ub+2,iz)=Bzu(:,1:2,iz)
     end do
  end if
  
END SUBROUTINE efield_boundary_bot

!...............................................................................
SUBROUTINE ddt_boundary_bot (r,px,py,pz,e,p,Bx,By,Bz, &
     drdt,dpxdt,dpydt,dpzdt, &
     dedt,dBxdt,dBydt,dBzdt)
  USE params
  USE forcing
  USE eos
  USE boundary
  implicit none
  
  real, dimension(mx,my,mz) :: r,px,py,pz,e,p,Bx,By,Bz
  real, dimension(mx,my,mz) :: drdt,dpxdt,dpydt,dpzdt
  real, dimension(mx,my,mz) :: dedt,dBxdt,dBydt,dBzdt
  integer :: ix,iz,ixp1,izp1
  real(kind=8) :: rub1,pyub1,elb1,eub1,ct,pub1,pubt1,dpydtub1,scr,htopi,cvin
  real(kind=8) :: iin1,pyubin1,iout1,pyubout1,out
  real :: F1,F2,F3,F4,F5,ee,hh,drdt_add
  integer, parameter :: mprint=10
  integer :: nprint
  real, dimension(mx,mz) :: pb
  real :: drdt_B, dedt_B
  real(kind=8), dimension(10) :: tmp

  character(len=mid), save :: Id = 'ddt_boundary_bot'
  !'ddt_boundary_bot 0.05 fraction smoothing'

  call print_id (Id)

  !$omp barrier
  if (debug) print *,'bdry',mpi_y,ym(1),rbot,ebot
  call dumpn(drdt,'drdt','bdry.dmp',1)
  call dumpn(dedt,'dedt','bdry.dmp',1)

  ! bottom BC
  if (mpi_y == mpi_ny-1) then
     nprint = 0
     ! keep track of spot center
     if (master .and. verbose>0 .and. isubstep==1) &
          print *,'pb:',pbub(mx/2+1,mz/2+1),pbot0(mx/2+1,mz/2+1)

     if (do_mhd) then
        pb = pbub
     else
        pb = 0.
     end if
     if(debug) print *,'pb:',mpi_rank,minval(pb(:,izs:ize)),maxval(pb(:,izs:ize))

     !$omp single
     rub = 0.
     eub = 0.
     pub = 0.
     pubt = 0.
     pyub = 0.
     dpydtub = 0.
     iin = 0.
     iout = 0.
     pyubin = 0.
     pyubout = 0.
     !$omp end single
     rub1 = 0.
     eub1 = 0.
     pub1 = 0.
     pubt1 = 0.
     pyub1 = 0.
     dpydtub1 = 0.
     iin1 = 0.
     iout1 = 0.
     pyubin1 = 0.
     pyubout1 = 0.
     do iz=izs,ize
        rub1 = rub1 + sum(r(:,ub,iz))
        eub1 = eub1 + sum(e(:,ub,iz))
        pub1 = pub1 + sum(pbot0(:,iz))
        pubt1 = pubt1 + sum(pbot0(:,iz)+pb(:,iz))
        pyub1 = pyub1 + sum(1.5*py(:,ub,iz)-0.5*py(:,ub-1,iz))
        ! py is regular(?)
        dpydtub1 = dpydtub1 + sum(1.5*dpydt(:,ub,iz)-0.5*dpydt(:,ub-1,iz))
        ! extrapolate to bdry
        do ix=1,mx
           out = merge (1d0, 0d0, py(ix,ub,iz) > 0.0)
           iin1 = iin1 + (1d0-out)
           iout1 = iout1 + out
           pyubin1 = pyubin1 + (1d0-out)*(1.5*py(ix,ub,iz)-0.5*py(ix,ub-1,iz))
           pyubout1 = pyubout1 + out*(1.5*py(ix,ub,iz)-0.5*py(ix,ub-1,iz))
        end do
     end do

     ! When using OMP, each section of iz has computed a sum in the loops 
     ! above, and before asking MPI to sum over ranks we need to sub the
     ! contributions from the individual sections.  The calls to MPI should
     ! be done by the master thread, only.

     !$omp critical
     rub = rub + rub1
     eub = eub + eub1
     pub = pub + pub1
     pubt = pubt + pubt1
     pyub = pyub + pyub1
     dpydtub = dpydtub + dpydtub1
     iin = iin + iin1
     iout = iout + iout1
     pyubin = pyubin + pyubin1
     pyubout = pyubout + pyubout1
     !$omp end critical
     !$omp barrier
     !$omp master
     tmp(1) = rub
     tmp(2) = eub
     tmp(3) = pub
     tmp(4) = pubt
     tmp(5) = pyub
     tmp(6) = dpydtub
     tmp(7) = iin*(mpi_nx*mpi_nz)
     tmp(8) = iout*(mpi_nx*mpi_nz)
     tmp(9) = pyubin*(mpi_nx*mpi_nz)
     tmp(10) = pyubout*(mpi_nx*mpi_nz)
     !$omp end master
     call haverage_mpi (tmp, 10)
     !$omp master
     rub     = tmp(1)
     eub     = tmp(2)
     pub     = tmp(3)
     pubt    = tmp(4)
     pyub    = tmp(5)
     dpydtub = tmp(6)
     iin     = tmp(7)
     iout    = tmp(8)
     pyubin  = tmp(9)
     pyubout = tmp(10)
     !$omp end master

     if (omp_master .and. debug) then
        print*,'rub',rub
        print*,'eub',eub
        print*,'pub',pub
        print*,'pubt',pubt
        print*,'pyub',pyub
        print*,'dpydtub',dpydtub
        print*,'pbot0(test)',pbot0(10,10)
        print*,'dlnpdE_r(test)',dlnpdE_r(10,10)
        print *,'iin,iout',mpi_rank,iin,iout,mxtot*mztot-iout
     end if

     ! damping constant
     ct = 0.5/t_bdry
     if (t_vbdry > 0.) then
        ! Vin smooth tim
        cvin = 0.5/t_vbdry 
     else
        cvin = 0.
     end if

     if (omp_master .and. debug) print*,'ct',ct

     do iz=izs,ize
        do ix=1,mx
           F1 = dlnpdlnr_E(ix,iz)                                             ! EOS table dlnpdlnr_E
           F2 = dlnpdE_r(ix,iz)                                               ! EOS table dlnpdE_r
           ee = e(ix,ub,iz)/r(ix,ub,iz)                                       ! E in notes
           hh = ee+pbot0(ix,iz)/r(ix,ub,iz)                                   ! H in notes
           F5 = F1 + F2*(hh-ee)                                               ! denominator of F3 & F4
           F3 = (F1 - F2*ee)/F5
           F4 = F2/F5
           if (py(ix,ub,iz) > 0.) then                                        ! for outgoing flows..
              drdt_add = F3*((rbot-r(ix,ub,iz))*ct-drdt(ix,ub,iz)) + &        ! Eq.18 boundaries.tex
                   F4*((ebot-e(ix,ub,iz))*ct-dedt(ix,ub,iz))
              if (debug .and. nprint < mprint) then
                 print 1,ix,F1,F2*ee,F3,F4*ee,F5,ee,hh,r(ix,ub,iz),drdt(ix,ub,iz),drdt_add
1                format(i5,5f10.3,5g12.4)
              end if
              nprint = nprint+1
              drdt(ix,ub,iz) = drdt(ix,ub,iz) + drdt_add
              dedt(ix,ub,iz) = dedt(ix,ub,iz) + hh*drdt_add                   ! Eq.21 boundaries.tex
           else                                                               ! for incoming flows
              dedt(ix,ub,iz) = (ebot-e(ix,ub,iz))*ct                          ! damp int.en. deviations
              drdt(ix,ub,iz) = (rbot-r(ix,ub,iz))*ct                          ! damp density deviations

              ! If (and only if) t_vbdry is non-zero: smooth the inflows. This
              ! is generally a bad idead, since it overspecifies the BC for 
              ! non-radial modes (as well as the radial one if smoothing 
              ! towards <py>=0), and is much better done in IDL, as a one-time
              ! only operation.  The option is left here for completeness.
              !
              if (iin > 0. .and. cvin > 0.) then
                 ! smooth py inflows:
                 dpydt(ix,ub,iz) = &
                      dpydt(ix,ub,iz) + (pyubin/iin-py(ix,ub,iz))*cvin
                 !! <py> -> 0.
                 !dpydt(ix,ub,iz) = &
                 !     dpydt(ix,ub,iz) - (pyubout/iin+py(ix,ub,iz))*cvin
              end if
           endif
           ! density and energy perturbations from B
           drdt_B = -ct*r(ix,ub,iz)*pb_fact*pb(ix,iz)/p(ix,ub,iz)/F5
           dedt_B = hh*drdt_B
           drdt(ix,ub,iz) = drdt(ix,ub,iz) + drdt_B
           dedt(ix,ub,iz) = dedt(ix,ub,iz) + dedt_B
           !ensure nothing bad happens in ghost zones
           drdt (ix,ub+1:my,iz) = 0.
           dpxdt(ix,ub+1:my,iz) = 0.
           dpydt(ix,ub+1:my,iz) = 0.
           dpzdt(ix,ub+1:my,iz) = 0.
           dedt (ix,ub+1:my,iz) = 0.
        end do
     end do
     
     ! MHD case:
     if (do_mhd) then
        ! zap the dB/dt in contaminated region
        dBxdt(:,ub+3:my,izs:ize) = 0.
        dBydt(:,ub+4:my,izs:ize) = 0.
        dBzdt(:,ub+3:my,izs:ize) = 0.
     end if
  end if

  !$omp barrier
  call dumpn(drdt,'drdt','bdry.dmp',1)
  call dumpn(dpxdt,'dpxdt','bdry.dmp',1)
  call dumpn(dpydt,'dpydt','bdry.dmp',1)
  call dumpn(dpzdt,'dpzdt','bdry.dmp',1)
  call dumpn(dedt,'dedt','bdry.dmp',1)
END SUBROUTINE ddt_boundary_bot

!...............................................................................
