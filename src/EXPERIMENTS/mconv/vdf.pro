@stagger_6th

PRO vdfout, f, t, level, var, root, ym, yn
  close,1
  f = intrp3y(ym,yn,temporary(f))
  openw,1,'tmp.raw'
  sz=size(f)
  ny=sz[2]
  writeu,1,f[*,5:ny-6,*]
  close,1
  cmd='raw2vdf -quiet -ts '+str(t)+' -level '+str(level)+' -varname '+str(var)+' '+str(root)+'.vdf tmp.raw'
  print,cmd
  spawn,cmd
END

;+ ----------------------------------------------------------------
 PRO vdf, file, ttt, do_pressure=do_pressure, nvar=nvar, lmax=lmax, level=level

;- ----------------------------------------------------------------

if n_elements(file) eq 0 then begin
  print,'SYNTAX: vdf, snapshotfile, [t0,t1,tstep] [,/do_pressure] [,lmax=3] [,level=2]
  print,' '
  print,'      snapshotfile       : OMP snapshot file (MPI not supported yet)'
  print,'     [t0,t1,tstep]       : first snapshot, last snapshot, step'
  print,'            lmax=3       : max 3 levels of resolution'
  print,'          levels=2       : use 2 levels of resolution (skip the highest resolution)'
  print,'      /do_pressure       : use the lookup procedure to compute pressure (call eos_table first!)'
  return
end

default,lmax,3
default,level,2

open,a,file,nt=nt,nv=nv,ym=ym,xm=xm
sz=size(a)
i=strpos(file,'.',/reverse_search)
root=strmid(file,0,i)

mx=sz[1]
my=sz[2]
mz=sz[3]

yn = ym[0] + findgen(my)*(ym[my-1]-ym[0])/(my-1.)
xmin=xm[0] & xmax=xm[mx-1]
ymin=ym[0] & ymax=ym[my-1]
zmin=xm[0] & zmax=xm[mx-1]
ext=str(xmin)+':'+str(xmin)+':'+str(xmax)+':'+str(ymin)+':'+str(ymax)+':'+str(zmin)+':'+str(zmax)

default,nvar,nv
default,ttt,[0,nt-1,1]

tt=ttt
nt=(tt[1]-tt[0])/tt[2]+1

dim=str(sz[1])+'x'+str(sz[2]-10)+'x'+str(sz[3])
cmd='vdfcreate -dimension '+str(dim)+' -numts '+str(nt)+' -level '+str(lmax)+ $
  ' -extents '+ext+' -varnames ux:uy:uz:u:lgrho:lgT:bx:by:bz:lgPm:lgPg '+root+'.vdf
print,cmd
spawn,cmd

for t=tt[0],tt[1],tt[2] do begin

  var = 'lgrho'
  lnrho = alog(a[nvar*t])
  f = lnrho/alog(10.)
  vdfout,f,(t-tt[0])/tt[2],level,var,root,ym,yn
  f = 0

  var = 'ux'
  f = xup(a[nvar*t+1]/exp(xdn(lnrho)))
  u = f^2
  vdfout,f,(t-tt[0])/tt[2],level,var,root,ym,yn
  f = 0

  var = 'uy'
  f = yup(a[nvar*t+2]/exp(ydn(lnrho)))
  u = temporary(u) + f^2
  vdfout,f,(t-tt[0])/tt[2],level,var,root,ym,yn
  f = 0

  var = 'uz'
  f = zup(a[nvar*t+3]/exp(zdn(lnrho)))
  u = temporary(u) + f^2
  vdfout,f,(t-tt[0])/tt[2],level,var,root,ym,yn
  f = 0

  var = 'u'
  vdfout,u,(t-tt[0])/tt[2],level,var,root,ym,yn
  u = 0
  f = 0

  var = 'bx'
  f = xup(a[nvar*t+6])
  vdfout,f,(t-tt[0])/tt[2],level,var,root,ym,yn
  f = 0

  var = 'by'
  f = yup(a[nvar*t+7])
  vdfout,f,(t-tt[0])/tt[2],level,var,root,ym,yn
  f = 0

  var = 'bz'
  f = zup(a[nvar*t+8])
  vdfout,f,(t-tt[0])/tt[2],level,var,root,ym,yn
  f = 0

  var='lgT'
  f = alog10(a[nvar*t+5])
  vdfout,f,(t-tt[0])/tt[2],level,var,root,ym,yn
  f = 0

  var='lgPm'
  f = 0.5*(xup(a[nvar*t+5])^2+ yup(a[nvar*t+6])^2+ zup(a[nvar*t+7])^2)
  f = alog10(f+1e-6)
  vdfout,f,(t-tt[0])/tt[2],level,var,root,ym,yn

  if keyword_set(do_pressure) then begin
    f = lookup(lnrho,a[nvar*t+4]/exp(lnrho))/alog(10.)
    var='lgPg'
    vdfout,f,(t-tt[0])/tt[2],level,var,root,ym,yn
  end
end

spawn,'/bin/rm tmp.raw'

print,' '
print,'Transfere these files to a directory that VAPOR has access to:'
print,'   '+root+'.vdf'
print,'   '+root+'_data/*'
print,' '

END
