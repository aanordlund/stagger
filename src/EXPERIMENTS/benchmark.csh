#!/bin/csh
# @ job_name = bench
# @ job_type = parallel
# @ total_tasks = 4
# @ class = parallel
# @ input = /dev/null
# @ output = $(job_name).$(jobid).o
# @ error =  $(job_name).$(jobid).e
# @ notification = never
# @ queue

set time = 1
#cd ~/stagger-code/src

set maxcpu = 4
if ($?MAXCPU) then
  set maxcpu = $MAXCPU
endif

set maxsize = 125
if ($?MAXSIZE) then
  set maxsize = $MAXSIZE
endif 


@ size = 125
set sizes = (33 63)
while ($size <= $maxsize)
  set sizes = ($sizes $size)
  @ size *= 2
end

echo " "
echo "............................................................."
echo ". For each size = $sizes"
echo ". While ncpu = 1,2, .. $maxcpu is less than size"
@ nb = 4
@ nc = 50
@ gb = `expr $maxsize \* $nb \* $nc / 1024 \* $size \* $size / 1024 / 1024`
echo ". This will require about $gb GB RAM"
echo "."
echo ". set the env varaiables MAXCPU and MAXSIZE to change limits"
echo ". Here MAXCPU=$maxcpu and MAXSIZE=$maxsize"
echo ".............................................................."
echo " "

set tarlist = ()
set tarid = `date +%y%m%d_%H%M`

foreach m ($sizes)

@ ncpu = 1

while  ($ncpu < $m && $ncpu <= $maxcpu)
  echo "size=$m ncpu=$ncpu"

  cat << EOF > input.txt
  &io      nstep=10 nscr=0 nsnap=0 file='TRIAL/benchmark.dat' io_word=4 /
  &run     Cdt=0.5 Cdtd=0.7 iseed=-1063256064 do_trace=f idbg=0 dbg_select=0 /
  &vars    do_pscalar=f do_energy=f do_mhd=t /
  &grid    mx=$m my=$m mz=$m sx=1 sy=1 sz=1 /
  &pde     do_loginterp=t do_2nddiv=f nu1=0.05 nu2=0.6 nur=0.2 gamma=1. /
  &slice   /
  &quench  /
  &eqofst  /
  &part    do_particles=f /
  &force   do_force=t do_helmh=t ampl_turb=6 t_turn=.1000 t_turb=-.1001 k1=1 k2=2 pk=0 /
  &selfg   grav=0. /
  &init    b0=1 do_test=f /
  &wave    type='y_alfven' k=1 ampl=0.1 /
  &bdry    /
  &expl    do_expl=f /
  &cool    do_cool=f /
  &expl    do_expl=f /
EOF

  setenv OMP_NUM_THREADS $ncpu
  \rm -f TRIAL/benchmark*msh
  ( date; uname -a; ./benchmark_$COMPILER.x; date ) > TRIAL/benchmark-$m-$ncpu.log

  echo "===== TRIAL/benchmark-$m-$ncpu.log ====="
  tail -16 TRIAL/benchmark-$m-$ncpu.log
  echo " "

  set tarlist = ( $tarlist benchmark-$m-$ncpu.log )
  (cd TRIAL; tar cf benchmark_$tarid.tar $tarlist )

  @ ncpu *= 2
end			# while
end			# foreach
