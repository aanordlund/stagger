! $Id: init.f90,v 1.3 2011/07/06 10:54:59 aake Exp $
!***********************************************************************
MODULE init_m
  implicit none
  character(len=16) test
  logical do_init, do_test
  real r0,e0,d0,b0,by0,ampl,uz0
END MODULE

!***********************************************************************
FUNCTION input_file()
  USE params
  implicit none
  character(len=mfile) input_file
  input_file = 'EXPERIMENTS/tearing/input.txt'
END

!***********************************************************************
SUBROUTINE init_values (r,px,py,pz,e,d,Bx,By,Bz)
  USE params
  USE init_m
  implicit none
  real, dimension(mx,my,mz):: r,px,py,pz,e,d,Bx,By,Bz
  integer ix,iy,iz
  real ran1s,kx,kz

!-----------------------------------------------------------------------
!  Initial value parameters
!-----------------------------------------------------------------------
  r0 = 1.
  d0 = 0.
  e0 = 2e-4
  b0 = 0.025
  by0 = 0.
  uz0 = 0.
  ampl = 1e-4
  do_init=.true.
  do_test=.false.
  test=''
  call read_init
  
  if (do_test) then
    call test_values (test,r,px,py,pz,e,d,Bx,By,Bz)
    return
  end if

  if (.not. do_init) return

  !$omp parallel
  do iz=izs,ize; do ix=1,mx
    px(ix,1,iz) = ampl*ran1s(iseed)
    !py(ix,1,iz) = ampl*ran1s(iseed)
    py(ix,1,iz) = 0.
    pz(ix,1,iz) = ampl*ran1s(iseed)
    do iy=2,my
      px(ix,iy,iz) = px(ix,1,iz)
      py(ix,iy,iz) = py(ix,1,iz)
      pz(ix,iy,iz) = pz(ix,1,iz)
    end do
  end do; end do
  r(:,:,izs:ize)=r0                                                     ! encourage local mem
  e(:,:,izs:ize)=e0
  if (do_pscalar) d(:,:,izs:ize)=d0
  if (do_mhd) then
    kz=2.*pi/sz
    kx=4.*pi/sx
    do ix=1,mx; do iy=1,my; do iz=izs,ize
      px(ix,iy,iz) = px(ix,iy,iz) + r0*uz0*cos(kz*zm(iz))*sin(kx*xm(ix))
      pz(ix,iy,iz) = pz(ix,iy,iz) - r0*uz0*cos(kx*xm(ix))*sin(kz*zm(iz))*kx/kz
      Bx(ix,iy,iz) = b0*cos(zm(iz)*kz) - r0*uz0*cos(kz*zm(iz))*cos(kx*xm(ix))
      By(ix,iy,iz) = by0
      Bz(ix,iy,iz) = 0. - r0*uz0*sin(kz*zm(iz))*sin(kx*xm(ix))*kx/kz
      e(ix,iy,iz) = e0 - 1.5*0.5*(bx(ix,iy,iz)**2+bz(ix,iy,iz)**2)
    end do; end do; end do
  end if
  !$omp end parallel
END

!***********************************************************************
SUBROUTINE read_init
  USE params
  USE init_m
  implicit none
  namelist /init/do_init,do_test,test,e0,d0,b0,by0,r0,ampl,uz0

  if (stdin < 0) then
    read (*,init)
    if (master) write (*,init)
  else
    rewind (stdin); read (stdin,init)
    if (master) write (stdout,init)
  end if
END
