!***********************************************************************
  subroutine conduction (r,e,ee,Bx,By,Bz,dedt)
!
!  Spitzer conductivity, from Schrijver & Zwaan 1999, pg 222
!
!  eps = d/ds (kappa_c dT/ds)
!  kappa_c = 8e-7 T^{5/2} erg cm^{-1} s^{-1}
!  eps = d/ds (kappa_E d(ln(E))/ds)
!  kappa_E = 8e-7 E^{7/2} (dT/dE)^{7/2} erg cm^{-1} s^{-1}		! increases as T^{3.5}
!
!  Each component should be prop to \hat{B} (\grad(E)\cdot\hat{B})
!  = B (grad(E)\cdot{B}) / B^2
!-----------------------------------------------------------------------
  USE params
  USE units
  USE cooling
  !USE stagger
  implicit none
  real, dimension(mx,my,mz):: r,e,ee,Bx,By,Bz,dedt
  real, allocatable, dimension(:,:,:):: Bxc, Byc, Bzc, lnee, B1dn, B2dn, &
    d_lnee, dn_lnee, dne, Fx, Fy, Fz, scr
  real u_cond, u_cmax, tmp, kappac, kappam, lnTbot, mu_corona, safe
  integer i, j, k
  character*80:: id= &
   "$Id: spitzer_conductivity.f90,v 1.4 2011/09/02 11:42:18 aake Exp $"

!-----------------------------------------------------------------------

  if (.not. do_conduction) return

  mu_corona = 0.63
  u_temp = 2./3.*mu_corona*mproton/kB*u_ee
  u_cond = 8e-7*u_temp**3.5/u_l**2*(u_t/u_e)				! [ u_e ] = erg cm^{-3}
  if (id .ne. ''.and.master) then 
    print *,id
    print *,'u_cond, u_temp = ', u_cond, u_temp
    id = ''
  end if
  if (master.and.verbose>0) print*, 'post 1'
!-----------------------------------------------------------------------
!  Temperature; face centered values and derivatives
!-----------------------------------------------------------------------
  allocate (   Bxc(mx,my,mz),     Byc(mx,my,mz),  Bzc(mx,my,mz))
  allocate (  lnee(mx,my,mz),    B1dn(mx,my,mz), B2dn(mx,my,mz))
  allocate (d_lnee(mx,my,mz), dn_lnee(mx,my,mz),  dne(mx,my,mz))
  allocate (    Fx(mx,my,mz),      Fy(mx,my,mz),   Fz(mx,my,mz))
  
  call xup1_set (Bx, Bxc)
  call yup1_set (By, Byc)						! centered B
  call zup1_set (Bz, Bzc)
  lnee = alog(ee)

!-----------------------------------------------------------------------
!  Face centered, field aligned heat flux
!-----------------------------------------------------------------------
  safe = cdt_cond/(3.*6.2)/dt

  call ddxdn1_set (lnee, d_lnee)
  call xdn1_set (lnee, dn_lnee)
  call xdn1_set (e, dne)
  call xdn1_set (Byc, B1dn)
  call xdn1_set (Bzc, B2dn)
  do k=1,mz; do j=lb-1,iphot+1; do i=1,mx
    Fx(i,j,k) = d_lnee(i,j,k) &
      *min( u_cond*exp(3.5*dn_lnee(i,j,k)),        &
            safe*dne(i,j,k)*min(dx,dym(j),dz)**2 ) &
      *Bx(i,j,k)**2/(Bx(i,j,k)**2 + B1dn(i,j,k)**2 + B2dn(i,j,k)**2)
  end do; end do; end do

  call ddydn1_set (lnee, d_lnee)
  call ydn1_set (lnee, dn_lnee)
  call ydn1_set (e, dne)
  call ydn1_set (Bzc, B1dn)
  call ydn1_set (Bxc, B2dn)
  do k=1,mz; do j=lb-1,iphot+1; do i=1,mx
    Fy(i,j,k) = d_lnee(i,j,k) &
      *min( u_cond*exp(3.5*dn_lnee(i,j,k)),        &
            safe*dne(i,j,k)*min(dx,dym(j),dz)**2 ) &
      *By(i,j,k)**2/(By(i,j,k)**2 + B1dn(i,j,k)**2 + B2dn(i,j,k)**2)
  end do; end do; end do

  call ddzdn1_set (lnee, d_lnee)
  call zdn1_set (lnee, dn_lnee)
  call zdn1_set (e, dne)
  call zdn1_set (Bxc, B1dn)
  call zdn1_set (Byc, B2dn)
  do k=1,mz; do j=lb-1,iphot+1; do i=1,mx
    Fz(i,j,k) = d_lnee(i,j,k) &
      *min( u_cond*exp(3.5*dn_lnee(i,j,k)),        &
            safe*dne(i,j,k)*min(dx,dym(j),dz)**2 ) &
      *Bz(i,j,k)**2/(Bz(i,j,k)**2 + B1dn(i,j,k)**2 + B2dn(i,j,k)**2)
  end do; end do; end do
  deallocate (lnee, d_lnee, dn_lnee, dne, B1dn, B2dn, Bxc, Byc, Bzc)
    call dumpn (Fx, 'Fx', 'spitzer.dmp', 1)
    call dumpn (Fy, 'Fy', 'spitzer.dmp', 1)
    call dumpn (Fz, 'Fz', 'spitzer.dmp', 1)

!-----------------------------------------------------------------------
!  Add flux divergence to dE/dt
!-----------------------------------------------------------------------
  allocate (scr(mx,my,mz))
  call ddxup1_set (Fx, scr)
  call ddyup1_add (Fy, scr)
  call ddzup1_add (Fz, scr)
    call dumpn (scr, 'spitzer', 'dedt.dmp', 1)

  do k=1,mz
  do j=lb,iphot-1
    dedt(:,j,k) = dedt(:,j,k) + scr(:,j,k)
  end do
  end do
  deallocate (scr, Fx, Fy, Fz)
  
!-----------------------------------------------------------------------
!  Make sure no conduction away from cold spots
!-----------------------------------------------------------------------
!  lnTbot = alog(eebot*(gamma-1))
!  do k=1,mz
!  do j=1,my
!  do i=1,mx
!    scr5(i,j,k) = dedt(i,j,k)
!    if (lnee(i,j,k) .gt. lnTbot) dedt(i,j,k) = dedt(i,j,k)+scr4(i,j,k)
!    scr5(i,j,k) = (dedt(i,j,k) - scr5(i,j,k))/e(i,j,k)
!  end do
!  end do
!  end do

!  call fstat1 ('spitzer', scr5, lb, ub)

  END
