
; copy mhd63.dat to mactag.dat
open,a,'mhd63.dat'
close,1
openw,1,'mactag.dat'
b=assoc(1,a[0])
for i=0,8 do writeu,1,a[i]
rh=a[0]
ee=a[4]/rh
tt=a[5]
ux=fux(a)
uy=fuy(a)
uz=fuz(a)

; temperature conversion factor
kb=1.38e-16
mp=1.67e-24
mu=0.6
u_ee=1e12
t_conversion=1.67e-24*0.6/1.38e-16/1.5*u_ee

; reduced density, enough to be outside the table
ny=14
rhof=1e-9

; add 1e3...1e7 K
for iy=0,ny+4 do begin
  f=10.^(0.7*(ny-iy))+1.
  f=f*1e6/(f+1e6)
  rh[*,iy,*]=rh[*,iy,*]/f
  ee[*,iy,*]=ee[*,iy,*]+tt[*,iy,*]*(f-1.)/t_conversion
  tt[*,iy,*]=tt[*,iy,*]*f
end

; write out
b[0]=rh
b[1]=ux*exp(xdn(alog(rh)))
b[2]=uy*exp(ydn(alog(rh)))
b[3]=uz*exp(zdn(alog(rh)))
b[4]=rh*ee
b[5]=tt
close,1

END
