! $Id: eos_lookup.f90,v 1.12 2015/03/14 22:18:34 aake Exp $ 

!**********************************************************************
MODULE table
  integer iupdte,nvar,mrho,mtable,njon
  real eosxmin,ur,ul,ut,eps,tff,grv,abnd
  real, allocatable, dimension(:):: tmean,tamp,rhm,xcorr,thmin,thmax, &
    dth,eemin,eemax,deetab,tab
  integer, allocatable, dimension(:):: itab,mtab
  real lnp_ideal,T_ideal,width1,width2,ee0,wf,arg
  real dttdee,mu_corona,T0_ideal,T_margin,ee_margin,temp_fact
END

!**********************************************************************
MODULE eos
  USE params
  implicit none
  integer mbox
  real dbox
  character(len=mfile):: tablefile
  logical do_eos, do_table
  real, allocatable, dimension(:,:):: pbot0, dlnpdE_r, dlnpdlnr_E
  logical, allocatable, dimension(:,:,:):: panic
CONTAINS

!**********************************************************************
SUBROUTINE equation_of_state (rho,ee,mask,pp,tt,ne,lnrk,lnsource)
!
!  Lookup equation of state table
!----------------------------------------------------------------------
!
  USE params
  implicit none
  real, dimension(mx,my,mz):: rho,ee
  real, dimension(mx,my,mz), optional:: pp,tt,ne,lnrk
  real, dimension(mx,my,mz,mbox), optional:: lnsource
  integer mask
  logical omp_in_parallel

  if (omp_in_parallel()) then
    call equation_of_state_omp (rho,ee,mask,pp,tt,ne,lnrk,lnsource)
  else
    !$omp parallel
    call equation_of_state_omp (rho,ee,mask,pp,tt,ne,lnrk,lnsource)
    !$omp end parallel
  end if
END SUBROUTINE

!**********************************************************************
SUBROUTINE equation_of_state_omp (rho,ee,mask,pp,tt,ne,lnrk,lnsource)
!
!  Lookup equation of state table
!----------------------------------------------------------------------
!
  USE params
  USE table
  implicit none
  real, dimension(mx,my,mz):: rho,ee
  real, dimension(mx,my,mz), optional:: pp,tt,ne,lnrk
  real, dimension(mx,my,mz,mbox), optional:: lnsource
  integer mask
!
  real, dimension(mx):: px,py,f00,f01,f10,f11, &
    fx00,fx01,fx10,fx11,fy00,fy01,fy10,fy11
  integer, dimension(mx):: np,np1,ik,ntab,ik1,ntab1
!
  logical newtable, any_mpi
  real rhm1,rhm2,drhm,algrk,eek,eek1,py1,qx,qy,pxqx,pxpx,qxqx,pypy,pyqy,qyqy,pxpy,qxqy,pxqy,qxpy
  integer ix,iy,iz,kee,kee1,j

  call print_trace('EOS',dbg_eos,'BEGIN')

1 continue
!
  rhm1=alog(rhm(1))
  rhm2=alog(rhm(mrho))
  drhm=(rhm2-rhm1)/(mrho-1)
!
!  assume no panic
!
  newtable = .false.
  do iz=izs,ize
    panic(:,:,iz)=.false.
  end do
!
!  x,y loop
!
  do iz=izs,ize
  do iy=1,my
    do ix=1,mx
!
!  Density index
!  6 flops
!
      algrk=1.+(alog(rho(ix,iy,iz))-rhm1)/drhm
      np(ix)=max0(1,min0(mrho-1,int(algrk)))
      algrk=max(min(algrk,np(ix)+1.),real(np(ix)))				! bracket
      px(ix)=algrk-np(ix)
      !panic(ix,iy,iz)=panic(ix,iy,iz).or.px(ix).lt.-0.01.or.px(ix).gt.1.01
!
!  Internal energy index.  This loop has indirect referencing.
!
! +6 = 12 flops
      ntab(ix)  = mtab(np(ix))
      eek       = 1.+(ee(ix,iy,iz)-eemin(np(ix)))/deetab(np(ix))
      kee       = min(ntab(ix)-1,max(1,floor(eek)))
      eek       = max(min(eek,kee+1.),real(kee))				! bracket
      py(ix)    = eek-kee
      !panic(ix,iy,iz) = panic(ix,iy,iz).or.py(ix).lt.-0.01.or.py(ix).gt.1.01
      ik(ix)    = itab(np(ix))+kee-1
      ntab1(ix) = mtab(np(ix)+1)
! +3 = 15 flops
      kee1      = kee - nint((eemin(np(ix)+1)-eemin(np(ix))) &
                                            /deetab(np(ix)))
      kee1      = min0(ntab1(ix)-1,max0(1,kee1))
      ik1(ix)   = itab(np(ix)+1)+kee1-1
!
!  These three lines may be commented out when we are satisfied it never panics
!
      eek1      = 1.+(ee(ix,iy,iz)-eemin(np(ix)+1))/deetab(np(ix)+1)
      eek1      = max(min(eek1,kee1+1.),kee1-1.)				! bracket
      py1       = eek1-kee1
      !panic(ix,iy,iz) = panic(ix,iy,iz) .or. abs(py(ix)-py1).gt.0.001
      !if (panic(ix,iy,iz)) then
      !  print*,ix,iy,iz
      !end if
    end do
!
!  Separate loop for weights, vectorizes well
!
    do ix=1,mx
! +5 = 20 flops
      qx   = 1. - px(ix)
      pxqx = px(ix) * qx
      pxpx = px(ix) * px(ix)
      qxqx = qx * qx
! +4 = 24 flops
      qy   = 1. - py(ix)
      pyqy = py(ix) * qy
      pypy = py(ix) * py(ix)
      qyqy = qy     * qy
! +4 = 28 flops
      pxqy = px(ix) * qy
      pxpy = px(ix) * py(ix)
      qxqy = qx     * qy
      qxpy = qx     * py(ix)
! +5 = 33 flops
      f00(ix) = qxqy * (1. + pxqx - pxpx + pyqy - pypy)
      f01(ix) = qxpy * (1. + pxqx - pxpx + pyqy - qyqy)
      f10(ix) = pxqy * (1. - qxqx + pxqx + pyqy - pypy)
      f11(ix) = pxpy * (1. - qxqx + pxqx + pyqy - qyqy)
! +4 = 37 flops
      fx00(ix) =    qxqy * pxqx
      fx01(ix) =    qxpy * pxqx
      fx10(ix) =  - pxqy * pxqx
      fx11(ix) =  - pxpy * pxqx
! +4 = 41 flops
      fy00(ix) =    qxqy * pyqy
      fy01(ix) =  - qxpy * pyqy
      fy10(ix) =    pxqy * pyqy
      fy11(ix) =  - pxpy * pyqy
    end do
!
!  Loop over table entries, nvar = 4.  Would this be more efficent
!  if split into one gather loop and one calculation loop?
!
    if (iand(mask,1).eq.1) then
     do ix=1,mx
      pp(ix,iy,iz) = exp( &
         f00(ix) * tab(ik (ix))+  f01(ix) * tab(ik (ix) + 1) &
      +  f10(ix) * tab(ik1(ix))+  f11(ix) * tab(ik1(ix) + 1) &
      + fx00(ix) * tab(ik (ix) + 2 * ntab (ix)    ) &
      + fx01(ix) * tab(ik (ix) + 2 * ntab (ix) + 1) &
      + fx10(ix) * tab(ik1(ix) + 2 * ntab1(ix)    ) &
      + fx11(ix) * tab(ik1(ix) + 2 * ntab1(ix) + 1) &
      + fy00(ix) * tab(ik (ix) +     ntab (ix)    ) &
      + fy01(ix) * tab(ik (ix) +     ntab (ix) + 1) &
      + fy10(ix) * tab(ik1(ix) +     ntab1(ix)    ) &
      + fy11(ix) * tab(ik1(ix) +     ntab1(ix) + 1) &
        )
     end do
    end if
    do ix=1,mx
      ik (ix) = ik (ix) + 3*ntab (ix)
      ik1(ix) = ik1(ix) + 3*ntab1(ix)
    end do
    if (iand(mask,2).eq.2) then
     do ix=1,mx
      lnrk(ix,iy,iz) = ( &
         f00(ix) * tab(ik (ix))+  f01(ix) * tab(ik (ix) + 1) &
      +  f10(ix) * tab(ik1(ix))+  f11(ix) * tab(ik1(ix) + 1) &
      + fx00(ix) * tab(ik (ix) + 2 * ntab (ix)    ) &
      + fx01(ix) * tab(ik (ix) + 2 * ntab (ix) + 1) &
      + fx10(ix) * tab(ik1(ix) + 2 * ntab1(ix)    ) &
      + fx11(ix) * tab(ik1(ix) + 2 * ntab1(ix) + 1) &
      + fy00(ix) * tab(ik (ix) +     ntab (ix)    ) &
      + fy01(ix) * tab(ik (ix) +     ntab (ix) + 1) &
      + fy10(ix) * tab(ik1(ix) +     ntab1(ix)    ) &
      + fy11(ix) * tab(ik1(ix) +     ntab1(ix) + 1) &
        )
     end do
    end if
    do ix=1,mx
      ik (ix) = ik (ix) + 3*ntab (ix)
      ik1(ix) = ik1(ix) + 3*ntab1(ix)
    end do
    if (iand(mask,4).eq.4) then
     do ix=1,mx
      tt(ix,iy,iz) = ( &
         f00(ix) * tab(ik (ix))+  f01(ix) * tab(ik (ix) + 1) &
      +  f10(ix) * tab(ik1(ix))+  f11(ix) * tab(ik1(ix) + 1) &
      + fx00(ix) * tab(ik (ix) + 2 * ntab (ix)    ) &
      + fx01(ix) * tab(ik (ix) + 2 * ntab (ix) + 1) &
      + fx10(ix) * tab(ik1(ix) + 2 * ntab1(ix)    ) &
      + fx11(ix) * tab(ik1(ix) + 2 * ntab1(ix) + 1) &
      + fy00(ix) * tab(ik (ix) +     ntab (ix)    ) &
      + fy01(ix) * tab(ik (ix) +     ntab (ix) + 1) &
      + fy10(ix) * tab(ik1(ix) +     ntab1(ix)    ) &
      + fy11(ix) * tab(ik1(ix) +     ntab1(ix) + 1) &
        )
     end do
    end if
    do ix=1,mx
      ik (ix) = ik (ix) + 3*ntab (ix)
      ik1(ix) = ik1(ix) + 3*ntab1(ix)
    end do
    if (iand(mask,8).eq.8) then
     do ix=1,mx
      ne(ix,iy,iz) = exp( &
         f00(ix) * tab(ik (ix))+  f01(ix) * tab(ik (ix) + 1) &
      +  f10(ix) * tab(ik1(ix))+  f11(ix) * tab(ik1(ix) + 1) &
      + fx00(ix) * tab(ik (ix) + 2 * ntab (ix)    ) &
      + fx01(ix) * tab(ik (ix) + 2 * ntab (ix) + 1) &
      + fx10(ix) * tab(ik1(ix) + 2 * ntab1(ix)    ) &
      + fx11(ix) * tab(ik1(ix) + 2 * ntab1(ix) + 1) &
      + fy00(ix) * tab(ik (ix) +     ntab (ix)    ) &
      + fy01(ix) * tab(ik (ix) +     ntab (ix) + 1) &
      + fy10(ix) * tab(ik1(ix) +     ntab1(ix)    ) &
      + fy11(ix) * tab(ik1(ix) +     ntab1(ix) + 1) &
        )
     end do
    end if
    if (iand(mask,16).eq.16) then
     do j=1,mbox
     do ix=1,mx
      ik (ix) = ik (ix) + 3*ntab (ix)
      ik1(ix) = ik1(ix) + 3*ntab1(ix)
      lnsource(ix,iy,iz,j) = ( &
         f00(ix) * tab(ik (ix))+  f01(ix) * tab(ik (ix) + 1) &
      +  f10(ix) * tab(ik1(ix))+  f11(ix) * tab(ik1(ix) + 1) &
      + fx00(ix) * tab(ik (ix) + 2 * ntab (ix)    ) &
      + fx01(ix) * tab(ik (ix) + 2 * ntab (ix) + 1) &
      + fx10(ix) * tab(ik1(ix) + 2 * ntab1(ix)    ) &
      + fx11(ix) * tab(ik1(ix) + 2 * ntab1(ix) + 1) &
      + fy00(ix) * tab(ik (ix) +     ntab (ix)    ) &
      + fy01(ix) * tab(ik (ix) +     ntab (ix) + 1) &
      + fy10(ix) * tab(ik1(ix) +     ntab1(ix)    ) &
      + fy11(ix) * tab(ik1(ix) +     ntab1(ix) + 1) &
        )
     end do
     end do
    end if
!
! End outer loops
!
  end do
  end do
!
!  Any cause for panic ?
!
  if (any_mpi(any(panic))) then
    do_dump = .true.
    call dumpn(rho,'rho','lookup.dmp',1)
    call dumpn(ee,'ee','lookup.dmp',1)

    do iz=izs,ize
    do iy=1,my
    do ix=1,mx
      if (panic(ix,iy,iz)) then
        algrk=1.+(alog(rho(ix,iy,iz))-rhm1)/drhm
        np(ix)=max0(1,min0(mrho-1,int(algrk)))
        ntab(ix) = mtab(np(ix))
        ntab1(ix)= mtab(np(ix)+1)
        eek= 1.+(ee(ix,iy,iz)-eemin(np(ix)))/deetab(np(ix))
        kee= min0(ntab(ix)-1,max0(1,floor(eek)))
        kee1= kee - nint((eemin(np(ix)+1)-eemin(np(ix)))/deetab(np(ix)))
        kee1= min0(ntab1(ix)-1,max0(1,kee1))
        py(ix)= eek-kee
        eek1= 1.+(ee(ix,iy,iz)-eemin(np(ix)+1))/deetab(np(ix)+1)
        py1  = eek1-kee1
        print '(a)','ix,iy,iz,lnrho,rhm1,rhm2,np(ix),algrk:'
        print '(3i4,3f8.3,i4,f8.3)',ix,iy,iz,alog(rho(ix,iy,iz)),rhm1,rhm2,np(ix),algrk
        print '(a)','ee(ix,iy,iz),eemin,eemax,eek,kee,py(ix),ntab:'
        print '(a)','                         eek1,kee1,py1,ntab1:'
        print '(4f8.3,i4,f8.3,i4/24x,f8.3,i4,f8.3,i4)',ee(ix,iy,iz), &
          eemin(np(ix)),eemax(np(ix)),eek,kee,py(ix),ntab(ix),eek1,kee1,py1,ntab1(ix)
        call abort_mpi
	stop
      endif
    end do
    end do
    end do

    call abort_mpi
    stop
  end if

!  Make a new table?

  call print_trace('EOS',dbg_eos,'END')
  if (newtable) then
!   call newtab
    print *,'Outside table in equation_of_state!'
    call abort_mpi
    stop
!    mtable=0
!    goto 1
  end if
END SUBROUTINE

END MODULE eos

!**********************************************************************
SUBROUTINE init_eos
  USE params
  USE arrays
  USE eos
  USE units
  USE table
  implicit none
  integer i, rank, iz
  character(len=mid):: id="$Id: eos_lookup.f90,v 1.12 2015/03/14 22:18:34 aake Exp $"

  call print_id(id)

  do_eos = .true.
  do_ionization = .true.
  do_table = .false.
  tablefile = 'table.dat'

  mu_corona = 0.62
  T0_ideal = 3e4							! critical choice
  T_margin = 100.
 
  call read_eos

  ee0    = T0_ideal*kB/mproton/u_ee
  dttdee = 2./3.*mu_corona*mproton/kB*u_ee
  ee_margin = T_margin/dttdee

  if (master) print*,'dttdee=',dttdee

  do rank=0,mpi_size-1
   call barrier_mpi ('init_eos')
   if (rank .eq. mpi_rank) then
    open (12,file=tablefile,form='unformatted',status='old',readonly)
    read (12) mrho,iupdte,nvar,mbox,eosxmin,dbox,ul,ut,ur,eps,tff,grv,abnd
    if (do_trace.and.master) print *,'init_eos:',mrho,iupdte,nvar,mbox,eosxmin,dbox,ul,ut,ur,eps,tff,grv,abnd
    njon = nvar-mbox
    allocate (tmean(mrho),tamp(mrho),rhm(mrho),xcorr(mrho),thmin(mrho),thmax(mrho))
    allocate (dth(mrho),eemin(mrho),eemax(mrho),deetab(mrho),itab(mrho),mtab(mrho))
    if (do_trace.and.master) print *,'init_eos: reading 12 times', mrho, ' values'
    read (12) tmean,tamp,rhm,xcorr,thmin,thmax,dth,eemin,eemax,deetab,itab,mtab
    read (12) mtable
    allocate (tab(mtable))
    if (master) print *, 'init_eos: reading', mtable, ' table values'
    read (12) (tab(i), i=1,mtable)
    if (master) print *, 'init_eos: read   ', mtable, ' table values'
    if (master) print *, 'init_eos: rho limits   =',rhm(1),rhm(mrho)
    if (master) print *, 'init_eos: ee  limits   =',eemin(1),eemax(1)
    if (master) print *, 'init_eos: T0_ideal, ee =',T0_ideal,ee0
    close (12)
   end if
  end do

  allocate (pbot0(mx,mz), dlnpdE_r(mx,mz), dlnpdlnr_E(mx,mz))
  allocate (panic(mx,my,mz))
!
!  Initialize (OMP only)
!
   !panic(:,:,:) = .false.
END SUBROUTINE

!**********************************************************************
SUBROUTINE read_eos
  USE eos
  USE table
  implicit none
  namelist /eqofst/ do_eos, do_ionization, do_table, tablefile, &
    T0_ideal, mu_corona

  rewind(stdin); read (stdin,eqofst); if (master) write (*,eqofst)
END SUBROUTINE

!**********************************************************************
SUBROUTINE lookup (rho,ee,lnrk,lnsource)
!
!  Lookup equation of state table
!----------------------------------------------------------------------
!
  USE params
  USE eos
  implicit none
  real, dimension(mx,my,mz), intent(in):: rho,ee
  real, dimension(mx,my,mz), intent(out):: lnrk
  real, dimension(mx,my,mz,mbox), intent(out):: lnsource
  logical omp_in_parallel

  if (omp_in_parallel()) then
    call lookup_omp (rho,ee,lnrk,lnsource)
  else
    !$omp parallel
    call lookup_omp (rho,ee,lnrk,lnsource)
    !$omp end parallel
  end if
  call dumpn(lnrk,'lnrk','eos2.dmp',1)
  call dumpn(lnsource,'lnsource','eos2.dmp',1)
END

!**********************************************************************
SUBROUTINE lookup_omp (rho,ee,lnrk,lnsource)
!
!  Lookup equation of state table
!----------------------------------------------------------------------
!
  USE params
  USE eos
  USE table
  implicit none
  real, dimension(mx,my,mz), intent(in):: rho,ee
  real, dimension(mx,my,mz), intent(out):: lnrk
  real, dimension(mx,my,mz,mbox), intent(out):: lnsource
!
  real, dimension(mx):: px,py,f00,f01,f10,f11, &
    fx00,fx01,fx10,fx11,fy00,fy01,fy10,fy11
  integer, dimension(mx):: np,np1,ik,ntab,ik1,ntab1
!
  real rhm1,rhm2,drhm,algrk,eek,qx,qy,pxqx,pxpx,qxqx,pypy,pyqy,qyqy,pxpy,qxqy,pxqy,qxpy
  integer ix,iy,iz,kee,kee1,j
  logical omp_in_parallel
!
  call print_trace('lookup',dbg_eos,'BEGIN')
  rhm1=alog(rhm(1))
  rhm2=alog(rhm(mrho))
  drhm=(rhm2-rhm1)/(mrho-1)
!
!  x,y loop
!
  do iz=izs,ize
  do iy=1,my
    do ix=1,mx
!
!  Density index
!  6 flops
!
      algrk=1.+(alog(rho(ix,iy,iz))-rhm1)/drhm
      np(ix)=max0(1,min0(mrho-1,int(algrk)))
      algrk=max(min(algrk,np(ix)+1.),real(np(ix)))				! bracket
      px(ix)=algrk-np(ix)
!
!  Internal energy index.  This loop has indirect referencing.
!
! +6 = 12 flops
      ntab(ix)  = mtab(np(ix))
      eek       = 1.+(ee(ix,iy,iz)-eemin(np(ix)))/deetab(np(ix))
      kee       = min(ntab(ix)-1,max(1,floor(eek)))
      eek       = max(min(eek,kee+1.),real(kee))				! bracket
      py(ix)    = eek-kee
      ik(ix)    = itab(np(ix))+kee-1
      ntab1(ix) = mtab(np(ix)+1)
! +3 = 15 flops
      kee1      = kee - nint((eemin(np(ix)+1)-eemin(np(ix))) &
                                            /deetab(np(ix)))
      kee1      = min0(ntab1(ix)-1,max0(1,kee1))
      ik1(ix)   = itab(np(ix)+1)+kee1-1
    end do
!
!  Loop over table entries, nvar = 4.  Would this be more efficent
!  if split into one gather loop and one calculation loop?
!
    do ix=1,mx
      ik (ix) = ik (ix) + 3*ntab (ix)
      ik1(ix) = ik1(ix) + 3*ntab1(ix)
      lnrk(ix,iy,iz) = &
        (1.-px(ix))*((1.-py(ix))*tab(ik (ix)) +  py(ix)*tab(ik (ix)+1)) &
      +     px(ix) *((1.-py(ix))*tab(ik1(ix)) +  py(ix)*tab(ik1(ix)+1))
      ik (ix) = ik (ix) + 3*ntab (ix)*(njon-2)
      ik1(ix) = ik1(ix) + 3*ntab1(ix)*(njon-2)
    end do
    do j=1,mbox
     do ix=1,mx
      ik (ix) = ik (ix) + 3*ntab (ix)
      ik1(ix) = ik1(ix) + 3*ntab1(ix)
      lnsource(ix,iy,iz,j) = &
        (1.-px(ix))*((1.-py(ix))*tab(ik (ix)) +  py(ix)*tab(ik (ix)+1)) &
      +     px(ix) *((1.-py(ix))*tab(ik1(ix)) +  py(ix)*tab(ik1(ix)+1))
     end do
    end do
!
! End outer loops
!
  end do
  end do
  call print_trace('lookup',dbg_eos,'END')
END SUBROUTINE

!**********************************************************************
SUBROUTINE pressure (rho,ee,pp)
!
!  Lookup equation of state table
!----------------------------------------------------------------------
!
  USE params
  implicit none
  real, dimension(mx,my,mz), intent(in):: rho,ee
  real, dimension(mx,my,mz), intent(out):: pp
  logical omp_in_parallel

  call dumpn(rho,'rho','eos2.dmp',0)
  if (omp_in_parallel()) then
    call pressure_omp (rho,ee,pp)
  else
    !$omp parallel
    call pressure_omp (rho,ee,pp)
    !$omp end parallel
  end if
  call dumpn(pp,'pp','eos2.dmp',1)
END

!**********************************************************************
SUBROUTINE pressure_omp (rho,ee,pp)
!
!  Lookup equation of state table
!----------------------------------------------------------------------
!
  USE params
  USE eos
  USE table
  implicit none
  real, dimension(mx,my,mz), intent(in):: rho,ee
  real, dimension(mx,my,mz), intent(out):: pp
!
  real, dimension(mx):: px,py,lnpp
  integer, dimension(mx):: np,np1,ik,ntab,ik1,ntab1
!
  logical newtable, any_mpi
  real rhm1,rhm2,drhm,algrk,eek,eek1,py1,qx,qy,pxqx,pxpx,qxqx,pypy,pyqy,qyqy,pxpy,qxqy,pxqy,qxpy
  integer ix,iy,iz,kee,kee1,j

1 continue
!
  call print_trace ('pressure',dbg_eos,'BEGIN')
  rhm1=alog(rhm(1))
  rhm2=alog(rhm(mrho))
  drhm=(rhm2-rhm1)/(mrho-1)
!
!  assume no panic
!
  newtable = .false.
!
!  x,y loop
!
  do iz=izs,ize
  do iy=1,my
    do ix=1,mx
!
!  Density index
!  6 flops
!
      algrk=1.+(alog(rho(ix,iy,iz))-rhm1)/drhm
      np(ix)=max0(1,min0(mrho-1,int(algrk)))
      algrk=max(min(algrk,np(ix)+1.),real(np(ix)))				! bracket
      px(ix)=algrk-np(ix)
      !panic(ix,iy,iz)=px(ix).lt.-0.01.or.px(ix).gt.1.01
!
!  Internal energy index.  This loop has indirect referencing.
!
! +6 = 12 flops
      ntab(ix)  = mtab(np(ix))
      eek       = 1.+(ee(ix,iy,iz)-eemin(np(ix)))/deetab(np(ix))
      kee       = min(ntab(ix)-1,max(1,floor(eek)))
      eek       = max(min(eek,kee+1.),real(kee))				! bracket
      py(ix)    = eek-kee
      !panic(ix,iy,iz) = panic(ix,iy,iz).or.py(ix).lt.-0.01.or.py(ix).gt.1.01
      ik(ix)    = itab(np(ix))+kee-1
      ntab1(ix) = mtab(np(ix)+1)
! +3 = 15 flops
      kee1      = kee - nint((eemin(np(ix)+1)-eemin(np(ix))) &
                                            /deetab(np(ix)))
      kee1      = min0(ntab1(ix)-1,max0(1,kee1))
      ik1(ix)   = itab(np(ix)+1)+kee1-1
!
!  These three lines may be commented out when we are satisfied it never panics
!
      eek1      = 1.+(ee(ix,iy,iz)-eemin(np(ix)+1))/deetab(np(ix)+1)
      eek1      = max(min(eek1,kee1+1.),kee1-1.)				! bracket
      py1       = eek1-kee1
      !panic(ix,iy,iz) = panic(ix,iy,iz) .or. abs(py(ix)-py1).gt.0.1
      !if (panic(ix,iy,iz)) then
      !  print*,ix,iy,iz
      !end if
    end do
!
!  Loop over table entries, nvar = 4.  Would this be more efficent
!  if split into one gather loop and one calculation loop?
!
    do ix=1,mx
      lnpp(ix) =  &
        (1.-px(ix))*((1.-py(ix))*tab(ik (ix)) +  py(ix)*tab(ik (ix)+1)) &
      +     px(ix) *((1.-py(ix))*tab(ik1(ix)) +  py(ix)*tab(ik1(ix)+1))
    end do
    call expn (mx, lnpp, pp(:,iy,iz))
    if (iy.eq.ub) then
!                               if (do_trace) print *,'pressure: crit1', omp_mythread
      do ix=1,mx
        pbot0(ix,iz) = pp(ix,ub,iz)
        dlnpdE_r(ix,iz) = ( &
          (1.-px(ix)) * (1.-py(ix)) * tab(ik(ix)  + ntab(ix)     ) &
      +   (1.-px(ix)) *  py(ix)     * tab(ik(ix)  + ntab(ix)  + 1) &
      +    px(ix)     * (1.-py(ix)) * tab(ik1(ix) + ntab1(ix)    ) &
      +    px(ix)     *  py(ix)     * tab(ik1(ix) + ntab1(ix) + 1) )/deetab(np(ix))
        dlnpdlnr_E(ix,iz) = ( &
          (1.-px(ix)) * (1.-py(ix)) * tab(ik(ix)  + 2*ntab(ix)     ) &
      +   (1.-px(ix)) *  py(ix)     * tab(ik(ix)  + 2*ntab(ix)  + 1) &
      +    px(ix)     * (1.-py(ix)) * tab(ik1(ix) + 2*ntab1(ix)    ) &
      +    px(ix)     *  py(ix)     * tab(ik1(ix) + 2*ntab1(ix) + 1) )/drhm
      end do
!                               if (do_trace) print *,'pressure: crit2', omp_mythread
    end if
!
!  Smooth transition to ideal gas pressure
!
    do ix=1,mx     
      arg = min(75.,max(-75.,(ee(ix,iy,iz)-eemax(np(ix)))/ee_margin+4.))
      wf = 0.5*(1.-tanh(arg))

      ee0 = 17.									! empirical
      temp_fact = (ee(ix,iy,iz)-ee0)/(eemax(np(ix))-ee0)
      temp_fact = max(0.1, temp_fact)
      lnp_ideal = lnpp(ix) + alog(temp_fact)
      lnpp(ix)  = lnpp(ix)*wf + lnp_ideal*(1.-wf)
    end do
    call expn (mx, lnpp, pp(:,iy,iz))
!
! End outer loops
!
  end do
  end do

!
!  Any cause for panic ?
!
  if (any_mpi(any(panic))) then
    do_dump = .true.
    call dumpn(rho,'rho','lookup.dmp',1)
    call dumpn(ee,'ee','lookup.dmp',1)

    do iz=izs,ize
    do iy=1,my
    do ix=1,mx
      if (panic(ix,iy,iz)) then
        algrk=1.+(alog(rho(ix,iy,iz))-rhm1)/drhm
        np(ix)=max0(1,min0(mrho-1,int(algrk)))
        ntab(ix) = mtab(np(ix))
        ntab1(ix)= mtab(np(ix)+1)
        eek= 1.+(ee(ix,iy,iz)-eemin(np(ix)))/deetab(np(ix))
        kee= min0(ntab(ix)-1,max0(1,floor(eek)))
        kee1= kee - nint((eemin(np(ix)+1)-eemin(np(ix)))/deetab(np(ix)))
        kee1= min0(ntab1(ix)-1,max0(1,kee1))
        py(ix)= eek-kee
        eek1= 1.+(ee(ix,iy,iz)-eemin(np(ix)+1))/deetab(np(ix)+1)
        py1  = eek1-kee1
        print '(a)','ix,iy,iz,lnrho,rhm1,rhm2,np(ix),algrk:'
        print '(3i4,3f8.3,i4,f8.3)',ix,iy,iz,alog(rho(ix,iy,iz)),rhm1,rhm2,np(ix),algrk
        print '(a)','ee(ix,iy,iz),eemin,eemax,eek,kee,py(ix),ntab:'
        print '(a)','                         eek1,kee1,py1,ntab1:'
        print '(4f8.3,i4,f8.3,i4/24x,f8.3,i4,f8.3,i4)',ee(ix,iy,iz), &
          eemin(np(ix)),eemax(np(ix)),eek,kee,py(ix),ntab(ix),eek1,kee1,py1,ntab1(ix)
        call abort_mpi
	stop
      endif
    end do
    end do
    end do

    call abort_mpi
    stop
  end if


   call print_trace ('pressure',dbg_eos,'END')
   if (newtable) then
!!   call newtab
     print *,'Outside table in pressure!'
     call abort_mpi
     stop
!!    mtable=0
!!    goto 1
   end if

END SUBROUTINE

!**********************************************************************
SUBROUTINE temperature (rho,ee,tt)
!----------------------------------------------------------------------
!
  USE params
  implicit none
  real, dimension(mx,my,mz), intent(in):: rho,ee
  real, dimension(mx,my,mz), intent(out):: tt
  logical omp_in_parallel

  if (omp_in_parallel()) then
    call temperature_omp (rho,ee,tt)
  else
    !$omp parallel
    call temperature_omp (rho,ee,tt)
    !$omp end parallel
  end if
  call dumpn(tt,'tt','eos2.dmp',1)
END

!**********************************************************************
SUBROUTINE temperature_omp (rho,ee,tt)
!
!  Lookup equation of state table
!----------------------------------------------------------------------
!
  USE params
  USE eos
  USE table
  implicit none
  real, dimension(mx,my,mz), intent(in):: rho,ee
  real, dimension(mx,my,mz), intent(out):: tt
!
  real, dimension(mx):: px,py,f00,f01,f10,f11, &
    fx00,fx01,fx10,fx11,fy00,fy01,fy10,fy11
  integer, dimension(mx):: np,np1,ik,ntab,ik1,ntab1
!
  logical newtable, any_mpi
  real rhm1,rhm2,drhm,algrk,eek,eek1,py1,qx,qy,pxqx,pxpx,qxqx,pypy,pyqy,qyqy,pxpy,qxqy,pxqy,qxpy
  integer ix,iy,iz,kee,kee1,j

1 continue
  call print_trace ('temperature',dbg_eos,'BEGIN')
!
  rhm1=alog(rhm(1))
  rhm2=alog(rhm(mrho))
  drhm=(rhm2-rhm1)/(mrho-1)
!
!  assume no panic
!
  newtable = .false.
!
!  x,y loop
!
  do iz=izs,ize
  do iy=1,my
    do ix=1,mx
!
!  Density index
!  6 flops
!
      algrk=1.+(alog(rho(ix,iy,iz))-rhm1)/drhm
      np(ix)=max0(1,min0(mrho-1,int(algrk)))
      algrk=max(min(algrk,np(ix)+1.),real(np(ix)))				! bracket
      px(ix)=algrk-np(ix)
      !panic(ix,iy,iz)=px(ix).lt.-0.01.or.px(ix).gt.1.01
!
!  Internal energy index.  This loop has indirect referencing.
!
! +6 = 12 flops
      ntab(ix)  = mtab(np(ix))
      eek       = 1.+(ee(ix,iy,iz)-eemin(np(ix)))/deetab(np(ix))
      kee       = min(ntab(ix)-1,max(1,floor(eek)))
      eek       = max(min(eek,kee+1.),real(kee))				! bracket
      py(ix)    = eek-kee
      !panic(ix,iy,iz) = panic(ix,iy,iz).or.py(ix).lt.-0.01.or.py(ix).gt.1.01
      ik(ix)    = itab(np(ix))+kee-1
      ntab1(ix) = mtab(np(ix)+1)
! +3 = 15 flops
      kee1      = kee - nint((eemin(np(ix)+1)-eemin(np(ix))) &
                                            /deetab(np(ix)))
      kee1      = min0(ntab1(ix)-1,max0(1,kee1))
      ik1(ix)   = itab(np(ix)+1)+kee1-1
!
!  These three lines may be commented out when we are satisfied it never panics
!
      eek1      = 1.+(ee(ix,iy,iz)-eemin(np(ix)+1))/deetab(np(ix)+1)
      eek1      = max(min(eek1,kee1+1.),real(kee1))				! bracket
      py1       = eek1-kee1
      !panic(ix,iy,iz) = panic(ix,iy,iz) .or. abs(py(ix)-py1).gt.0.01
      !if (panic(ix,iy,iz)) then
      !  print*,ix,iy,iz
      !end if
    end do
!
!  Separate loop for weights, vectorizes well
!
    do ix=1,mx
! +5 = 20 flops
      qx   = 1. - px(ix)
      pxqx = px(ix) * qx
      pxpx = px(ix) * px(ix)
      qxqx = qx * qx
! +4 = 24 flops
      qy   = 1. - py(ix)
      pyqy = py(ix) * qy
      pypy = py(ix) * py(ix)
      qyqy = qy     * qy
! +4 = 28 flops
      pxqy = px(ix) * qy
      pxpy = px(ix) * py(ix)
      qxqy = qx     * qy
      qxpy = qx     * py(ix)
! +5 = 33 flops
      f00(ix) = qxqy * (1. + pxqx - pxpx + pyqy - pypy)
      f01(ix) = qxpy * (1. + pxqx - pxpx + pyqy - qyqy)
      f10(ix) = pxqy * (1. - qxqx + pxqx + pyqy - pypy)
      f11(ix) = pxpy * (1. - qxqx + pxqx + pyqy - qyqy)
! +4 = 37 flops
      fx00(ix) =    qxqy * pxqx
      fx01(ix) =    qxpy * pxqx
      fx10(ix) =  - pxqy * pxqx
      fx11(ix) =  - pxpy * pxqx
! +4 = 41 flops
      fy00(ix) =    qxqy * pyqy
      fy01(ix) =  - qxpy * pyqy
      fy10(ix) =    pxqy * pyqy
      fy11(ix) =  - pxpy * pyqy
    end do
!
!  Loop over table entries, nvar = 4.  Would this be more efficent
!  if split into one gather loop and one calculation loop?
!
    do ix=1,mx
      ik (ix) = ik (ix) + 3*ntab (ix)*2
      ik1(ix) = ik1(ix) + 3*ntab1(ix)*2
      tt(ix,iy,iz) = ( &
         f00(ix) * tab(ik (ix))+  f01(ix) * tab(ik (ix) + 1) &
      +  f10(ix) * tab(ik1(ix))+  f11(ix) * tab(ik1(ix) + 1) &
      + fx00(ix) * tab(ik (ix) + 2 * ntab (ix)    ) &
      + fx01(ix) * tab(ik (ix) + 2 * ntab (ix) + 1) &
      + fx10(ix) * tab(ik1(ix) + 2 * ntab1(ix)    ) &
      + fx11(ix) * tab(ik1(ix) + 2 * ntab1(ix) + 1) &
      + fy00(ix) * tab(ik (ix) +     ntab (ix)    ) &
      + fy01(ix) * tab(ik (ix) +     ntab (ix) + 1) &
      + fy10(ix) * tab(ik1(ix) +     ntab1(ix)    ) &
      + fy11(ix) * tab(ik1(ix) +     ntab1(ix) + 1) &
        )
!
!  Smooth transition to ideal temperature
!
      arg = min(75.,max(-75.,(ee(ix,iy,iz)-eemax(np(ix)))/ee_margin+4.))
      wf = 0.5*(1.-tanh(arg))						! is ~0 at ee0
      T_ideal = tt(ix,iy,iz) + (ee(ix,iy,iz)-eemax(np(ix)))*dttdee	! extrapolate beyond
      tt(ix,iy,iz) = tt(ix,iy,iz)*wf + T_ideal*(1.0-wf)                 ! smooth combination
    end do
!
! End outer loops
!
  end do
  end do
!
!  Any cause for panic ?
!
  if (any_mpi(any(panic))) then
    do_dump = .true.
    call dumpn(rho,'rho','lookup.dmp',1)
    call dumpn(ee,'ee','lookup.dmp',1)

    do iz=izs,ize
    do iy=1,my
    do ix=1,mx
      if (panic(ix,iy,iz)) then
        algrk=1.+(alog(rho(ix,iy,iz))-rhm1)/drhm
        np(ix)=max0(1,min0(mrho-1,int(algrk)))
        ntab(ix) = mtab(np(ix))
        ntab1(ix)= mtab(np(ix)+1)
        eek= 1.+(ee(ix,iy,iz)-eemin(np(ix)))/deetab(np(ix))
        kee= min0(ntab(ix)-1,max0(1,floor(eek)))
        kee1= kee - nint((eemin(np(ix)+1)-eemin(np(ix)))/deetab(np(ix)))
        kee1= min0(ntab1(ix)-1,max0(1,kee1))
        py(ix)= eek-kee
        eek1= 1.+(ee(ix,iy,iz)-eemin(np(ix)+1))/deetab(np(ix)+1)
        py1  = eek1-kee1
        print '(a)','ix,iy,iz,lnrho,rhm1,rhm2,np(ix),algrk:'
        print '(3i4,3f8.3,i4,f8.3)',ix,iy,iz,alog(rho(ix,iy,iz)),rhm1,rhm2,np(ix),algrk
        print '(a)','ee(ix,iy,iz),eemin,eemax,eek,kee,py(ix),ntab:'
        print '(a)','                         eek1,kee1,py1,ntab1:'
        print '(4f8.3,i4,f8.3,i4/24x,f8.3,i4,f8.3,i4)',ee(ix,iy,iz), &
          eemin(np(ix)),eemax(np(ix)),eek,kee,py(ix),ntab(ix),eek1,kee1,py1,ntab1(ix)
        call abort_mpi
	stop
      endif
    end do
    end do
    end do

    call abort_mpi
    stop
  end if

!  Make a new table?

  call print_trace ('temperature',dbg_eos,'END')
  if (newtable) then
!   call newtab
    print *,'Outside table in temperature!'
    call abort_mpi
    stop
!    mtable=0
!    goto 1
  end if
END SUBROUTINE

SUBROUTINE temperature1 (rho,ee,tt)
  USE params
  USE eos
  implicit none
  real, dimension(mx,my,mz), intent(in):: rho,ee
  real, dimension(mx,my,mz), intent(out):: tt
  call equation_of_state (rho,ee,4,tt=tt)
END SUBROUTINE
