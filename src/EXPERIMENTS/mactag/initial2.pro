pro initial2

;  This program produces the initial condition for an experiment combining
;  magnetoconvection and the corona.

; This file needs a bit of a tidy - it's the combination of several codes.


; Read in a mesh file (cell centres)

openr,lun,'mesh.txt',/get_lun
mx = 0L & my = 0L & mz = 0L
readf,lun,mx & xc=fltarr(mx) & readf,lun,xc
readf,lun,my & yc=fltarr(my) & readf,lun,yc
readf,lun,mz & zc=fltarr(mz) & readf,lun,zc
free_lun,lun

; Add atmosphere grid

ny = 300L	; Will tidy this up at a later date
dyc = yc(1)-yc(0)
yc2 = fltarr(ny)
yc2(0) = yc(0)-dyc

for i = 1, ny-1 do begin
	dyc = dyc*1.001		; for stretching (will improve later)
	yc2(i) = yc2(i-1) - dyc
endfor

; Flip yc2

h = -yc2 ; for hydrostatic atmosphere later

fill = fltarr(ny)
for i = ny-1,0,-1 do begin
	fill(ny-1-i) = yc2(i)
endfor
yc2 = fill

ym = fltarr(my+ny)
ym = [yc2,yc]

; Write out a new mesh file

openw,lun,'mesh_new.txt',/get_lun
printf,lun,mx
printf,lun,xc
printf,lun,my+ny
printf,lun,ym
printf,lun,mz
printf,lun,zc
free_lun,lun

; Read in snapshot data

open,a,'snapshot_01077',dim=[mx,my,mz]

rho = a(0)
px = a(1)
py = a(2)
pz = a(3)
e = a(4)
t = a(5)
bx = a(6)
by = a(7)
bz = a(8)

; Correct t
t(*,0,*) = t(*,1,*)
print, t(100,0,100), t(100,1,100)

; Momemnta in the intial atmosphere

; Correct py (will depend on data file)

; need to tidy

py(*,4,*) = py(*,5,*)*0.5
py(*,3,*) = py(*,4,*)*0.5
py(*,2,*) = py(*,3,*)*0.5
py(*,1,*) = py(*,2,*)*0.5
py(*,0,*) = py(*,1,*)*0.5

px1 = fltarr(mx,ny,mz)
py1 = fltarr(mx,ny,mz)
pz1 = fltarr(mx,ny,mz)

for i = 0, mx-1 do begin
for j = ny-1, 0, -1 do begin
for k = 0, mz-1 do begin
	px1(i,j,k) = px(i,0,k)*exp(-(ny-1-j)/10.0)
	py1(i,j,k) = py(i,0,k)*exp(-(ny-1-j)/10.0)
	pz1(i,j,k) = pz(i,0,k)*exp(-(ny-1-j)/10.0)
end
end
end

print, py1(100,ny-1,100), py(100,0,100)

px = [[px1],[px]]
py = [[py1],[py]]
pz = [[pz1],[pz]]

; Calculate potential field in the atmosphere

atmos = fltarr(mx,ny,mz)

bx = [[atmos],[bx]]
by = [[atmos],[by]]
bz = [[atmos],[bz]]

potential_lower,bx,by,bz,lb=ny

; Calculate hydrostatic equilibrium

  ul=1e8                     ; length unit (1 Mm)
  ut=1e2                     ; time unit (100 sec)
  ur=1e-7                    ; density (1e-7 is used in photospheric models)
  uv=ul/ut                   ; velocity (10 km/s)
  up=ur*uv^2                 ; pressure (1e3 cgs)

  g=2.75e4*ut^2/ul           ; acceleration of gravity
  kb=1.38d-16                ; Boltzmann (cgs)
  mp=1.67d-24                ; mass unit (cgs)
  mh=mp


rho1 = [[atmos],[rho]]
e1 = [[atmos],[e]]


y_cor = 1.0	; start of the corona

dlgTdh=2
hcor = (h-y_cor) > 0
t1=10.^(alog10(aver(t(*,0,*)))+dlgTdh*hcor) < 1e6

for i=0,ny-1 do begin
	atmos(*,ny-1-i,*) = t1(i)
endfor

tt1 = [[atmos],[t]]

i0=300                     ; reference level
;  readmesh,ym=ym
  h=ym                       ; increasing downwards

  rhoh=haver(rho1,/yv)
  eh  =haver(  e1,/yv)
  Th  =haver( tt1,/yv)

  eos_table
  i=i0
   
  
  p  =make_array(size=size(e1))
  p[*,i,*]=exp(lookup(alog(rho1[*,i,*]),e1[*,i,*]/rho1[*,i,*]))

  gamma=5./3.
  
   e0 = fltarr(mx,mz)
  e0(*,*)=(e1[*,i,*]-p[*,i,*]/(gamma-1.))/rho1[*,i,*]               ; offset

  mu=fltarr(mx,mz)
  mu0=fltarr(mx,mz)
  w=fltarr(mx,mz)
  mu0(*,*)=rho1[*,i,*]*ur/(mp*p[*,i,*]*up/(kb*tt1[*,i,*]))          ; for continuity
  mu1=0.64
  ;print,'mu, e0 =',mu0, e0

  ;print,'       i            P          rho            h            T'
  ;print,i,P[i],rhoh[i],h[i],Th[i]
  while (i gt 0) do begin
    i=i-1
    dh=h[i+1]-h[i]
    w(*,*)=exp(-(tt1[*,i,*]-tt1[*,i0,*])/1e4) < 1                 ; approx mu
    mu(*,*)=mu0(*,*)*w(*,*)+mu1*(1.-w(*,*))                          ; approx mu
    for k=1,5 do begin
      if k eq 1 then begin
        rho1[*,i,*]=rho1[*,i+1,*]                          ; initial guess
      end
      p[*,i,*]=p[*,i+1,*]-g*0.5*(rho1[*,i+1,*]+rho1[*,i,*])*dh     ; predict P[i]
      rho1[*,i,*]=mu(*,*)*mh*(P[*,i,*]*up/ur)/(kb*tt1[*,i,*])        ; improve rho
      ;print,i,P[i],rhoh[i]
    end
    e1[*,i,*]=p[*,i,*]/(gamma-1.)+e0(*,*)*rho1[*,i,*]
    ;print,i,P[i],rhoh[i],h[i],Th[i]
  end

  !p.multi=[0,2,2]
  !x.title='Mm'
  !p.psym=-1
  !y.style=3
  ;plot,h,havver(rho1,/yv,/ylog,yst=3,xr=[-10,5],symsize=.3
  ;plot,h,Th,/ylog,yst=3,xr=[-10,5],symsize=.3
  ;plot,h,P[0:i0],/ylog,yst=3,xr=[-10,5],symsize=.3
  ;plot,h,eh/rhoh,/ylog,yst=3,xr=[-10,5],symsize=.3

  ;for i=0,i0-1 do begin
  ;  rho1[*,i,*]=rhoh[i]
  ;  e1[*,i,*]=eh[i]	
  ;endfor
  
  ; Write out initial condition of combined corona and convection zone

openw,lun,'initial_test.dat',/get_lun

b = assoc(lun,fltarr(mx,my+ny,mz))
b(0) = rho1
b(1) = px
b(2) = py
b(3) = pz
b(4) = e1
b(5) = tt1
b(6) = bx
b(7) = by
b(8) = bz
free_lun,lun

end

