; $Id: vdf.pro,v 1.4 2011/09/02 11:42:42 aake Exp $

PRO vdfout,fin,name,file,vdc2=vdc2,lev=lev,ym1=ym1,m=m,w=w,s=s
@common_cmesh
  if n_elements(ym) eq 0 then readmesh
  sz=size(fin) & ny=sz[2]
  default,s,[0,0,0]
  if n_elements(m) eq 3 then begin
    default, w, [1,1,1]*32
    ny=w
    f=intrp3y(ym[m[1]+s[1]-w[1]/2:m[1]+s[1]-w[1]/2+w[1]-1],ym1, $
      aaregion(shift(fin,s[0],s[1],s[2]),m+s,w=w))
  end else begin
    f=intrp3y(ym[0:ny-1],ym1,shift(fin,s[0],0,s[2]))
  end
  close,1 & openw,1,'tmp.raw' & writeu,1,f & close,1
  ; image,f[*,ny/2,*] & wait,.2
  cmd='raw2vdf'
  if keyword_set(vdc2) then begin
    cmd=cmd+' -lod '+str(lev)
  end else begin
    cmd=cmd+' -level '+str(lev)
  end
  cmd=cmd+' -quiet -ts 0 -varname '+name+' '+file+' tmp.raw'
  print,cmd
  if !version.os eq "Win32" then spawn,cmd,/log else spawn,cmd
END

PRO vdf,bx,by,bz,a=a,ux=ux,uy=uy,uz=uz,jx=jx,jy=jy,jz=jz,tt=tt,file=file, $
    dx=dx,dy=dy,dz=dz,vdc1=vdc1,lev=lev,ym1=ym1,hsize=width, $
    m=m, w=w, shift=s, do_j=do_j
@common_cmesh
  if n_elements(a) gt 0 then begin
    fmt='(a,$)'
    print,'ux..',form=fmt & ux=fux(a,rho=rho)
    print,'uy..',form=fmt & uy=fuy(a,rho=rho)
    print,'uz..',form=fmt & uz=fuz(a,rho=rho)
    rho=0
    if keyword_set(do_j) then begin
      j2=fj2(a,/bcenter,bx=bx,by=by,bz=bz,jx=jx,jy=jy,jz=jz)
    end else begin
      print,'bx..',form=fmt & bx=xup(a[6])
      print,'by..',form=fmt & by=yup(a[7])
      print,'bz..',form=fmt & bz=zup(a[8])
    end
    print,'tt..',form=fmt & tt=a[5]
    print,''
  end
  if n_elements(ym) eq 0 then readmesh
  sz=size(bx)
  nx=sz[1]
  ny=sz[2]
  nz=sz[3]
  default,w,[1,1,1]*32
  if n_elements(ym1) eq 0 then begin
    if n_elements(m) eq 3 then begin
      ny=w[1]
      dy=(ym[m[1]-w[1]/2+w[1]-1]-ym[m[1]-w[1]/2])/w[1]
      ym1=ym[m[1]-w[1]/2]+findgen(w[1])*dy
    end else begin
      ny=sz[2]
      dy=(ym[ny-1]-ym[0])/(ny-1.)
      ym1=ym[0]+findgen(ny)*dy
    end
  end
  varnames=' -vars3d bx:by:bz:bb:bi'
  if n_elements(jx) gt 0 then varnames=varnames+':jx:jy:jz:jj'
  if n_elements(ux) gt 0 then varnames=varnames+':ux:uy:uz:uu'
  if n_elements(tt) gt 0 then varnames=varnames+':tt'
  default,width,24.
  default,dx,width/nx
  default,dz,dx
  sz=size(bx)
  if n_elements(m) eq 3 then begin
    dim=' -dimension '+str(w[0])+'x'+str(w[1])+'x'+str(w[2])
    dim=dim+' -extents 0:0:0:'+str((w[0]-1)*dx)+':'+str((w[1]-1)*dy)+':'+str((w[2]-1)*dz)
  end else begin
    dim=' -dimension '+str(sz[1])+'x'+str(sz[2])+'x'+str(sz[3])
    dim=dim+' -extents 0:0:0:'+str((nx-1)*dx)+':'+str((ny-1)*dy)+':'+str((nz-1)*dz)
  end
  cmd='vdfcreate'
  default, lev, 2
  if keyword_set(vdc1) then begin
    cmd=cmd+' -level '+str(lev)
  end else begin
    vdc2=1
    cmd=cmd+' -vdc2 -cratios 10:30:100' 
  end
  default,file,'tmp.vdf'
  if strpos(file,'.') le 0 then file=file+'.vdf'
  cmd=cmd+dim+varnames+' '+file
  print,cmd
  if !version.os eq "Win32" then spawn,cmd,/log else spawn,cmd

  vdfout,bx,'bx',file,lev=lev,vdc2=vdc2,ym1=ym1,m=m,w=w,s=s
  vdfout,by,'by',file,lev=lev,vdc2=vdc2,ym1=ym1,m=m,w=w,s=s
  vdfout,bz,'bz',file,lev=lev,vdc2=vdc2,ym1=ym1,m=m,w=w,s=s
  bb=sqrt(bx^2+by^2+bz^2)
  vdfout,bb,'bb',file,lev=lev,vdc2=vdc2,ym1=ym1,m=m,w=w,s=s
  bi=1./(bb+1)
  vdfout,bi,'bi',file,lev=lev,vdc2=vdc2,ym1=ym1,m=m,w=w,s=s
  if n_elements(jx) gt 0 then begin
    vdfout,jx,'jx',file,lev=lev,vdc2=vdc2,ym1=ym1,m=m,w=w,s=s
    vdfout,jy,'jy',file,lev=lev,vdc2=vdc2,ym1=ym1,m=m,w=w,s=s
    vdfout,jz,'jz',file,lev=lev,vdc2=vdc2,ym1=ym1,m=m,w=w,s=s
    jj=sqrt(jx^2+jy^2+jz^2)
    vdfout,jj,'jj',file,lev=lev,vdc2=vdc2,ym1=ym1,m=m,w=w,s=s
  end
  if n_elements(ux) gt 0 then begin
    vdfout,ux,'ux',file,lev=lev,vdc2=vdc2,ym1=ym1,m=m,w=w,s=s
    vdfout,uy,'uy',file,lev=lev,vdc2=vdc2,ym1=ym1,m=m,w=w,s=s
    vdfout,uz,'uz',file,lev=lev,vdc2=vdc2,ym1=ym1,m=m,w=w,s=s
    uu=sqrt(ux^2+uy^2+uz^2)
    vdfout,uu,'uu',file,lev=lev,vdc2=vdc2,ym1=ym1,m=m,w=w,s=s
  end
  if n_elements(tt) gt 0 then begin
    vdfout,tt,'tt',file,lev=lev,vdc2=vdc2,ym1=ym1,m=m,w=w,s=s
  end
  close,1
  file_delete,'tmp.raw'
  root=fileroot(file)
  cmd='tar cf ~/'+root+'.tar '+root+'.vdf '+root+'_data'
  print,cmd
  spawn,cmd
END
