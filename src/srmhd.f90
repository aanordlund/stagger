! $Id: srmhd.f90,v 1.5 2012/12/31 16:19:16 aake Exp $
!***********************************************************************
  SUBROUTINE mhd(eta,W,Ux,Uy,Uz,UWx,UWy,UWz,  &
                 Bx,By,Bz,dpxdt,dpydt,dpzdt,dedt,dBxdt,dBydt,dBzdt)

  USE params
  USE stagger
  USE metric

  logical flag
  real, dimension(mx,my,mz) :: &
       eta,Ux,Uy,Uz,UWx,UWy,UWz,W, &
       Bx,By,Bz,dpxdt,dpydt,dpzdt,dedt,dBxdt,dBydt,dBzdt
!hpf$ distribute (*,*,block) :: &
!hpf$  eta,Ux,Uy,Uz,Bx,By,Bz,dpxdt,dpydt,dpzdt,dedt,dBxdt,dBydt,dBzdt
  real, allocatable, dimension(:,:,:) :: &
       Jx,Jy,Jz,Ex,Ey,Ez, &
       Rx,Ry,Rz, &
       Bx_y,Bx_z,By_x,By_z,Bz_x,Bz_y, &
       Ux_y,Ux_z,Uy_x,Uy_z,Uz_x,Uz_y
!hpf$ distribute (*,*,block) :: &
!hpf$  Jx,Jy,Jz,Ex,Ey,Ez, &
!hpf$  Bx_y,Bx_z,By_x,By_z,Bz_x,Bz_y, &
!hpf$  Ux_y,Ux_z,Uy_x,Uy_z,Uz_x,Uz_y
  integer :: lrec
  character(len=mid) id
  data id/'$Id: srmhd.f90,v 1.5 2012/12/31 16:19:16 aake Exp $'/

  if (id .ne. '') then
    print *,id
    id = ''
  end if

!-----------------------------------------------------------------------
!  Electric current I = curl(B), resistive E, and dissipation
!-----------------------------------------------------------------------
  allocate (Jx(mx,my,mz), Jy(mx,my,mz), Jz(mx,my,mz))
  Jx = ddydn(Bz) - ddzdn(By)
  Jy = ddzdn(Bx) - ddxdn(Bz)
  Jz = ddxdn(By) - ddydn(Bx)
  if (do_displacement) then  ! Calculate the displacement current??
    call displacement(W,Ux,Uy,Uz,UWx,UWy,UWz,Bx,By,Bz)
    Jx = Jx - dExdt
    Jy = Jy - dEydt
    Jz = Jz - dEzdt
  endif

  allocate (Ex(mx,my,mz), Ey(mx,my,mz), Ez(mx,my,mz))
  Ex = ydn1(zdn1(eta))*(0.5*(dz+dy))*Jx
  Ey = xdn1(zdn1(eta))*(0.5*(dz+dx))*Jy
  Ez = xdn1(ydn1(eta))*(0.5*(dy+dx))*Jz
  if (do_displacement) then  ! Calculate the displacement current??
    call displacement_etaJ(Ex,Ey,Ez)
    Jx = Jx - detaJxdt
    Jy = Jy - detaJydt
    Jz = Jz - detaJzdt
    Ex = ydn1(zdn1(eta))*(0.5*(dz+dy))*Jx
    Ey = xdn1(zdn1(eta))*(0.5*(dz+dx))*Jy
    Ez = xdn1(ydn1(eta))*(0.5*(dy+dx))*Jz
  endif

  if (lb.gt.1) then
    allocate (Rx(mx,2,mz),Ry(mx,2,mz),Rz(mx,2,mz))
    Rx(:,:,:) = Ex(:,lb:lb+1,:)
    Ry(:,:,:) = Ey(:,lb:lb+1,:)
    Rz(:,:,:) = Ez(:,lb:lb+1,:)
  end if

!-----------------------------------------------------------------------
! Q_Joule part of dissipation. Not needed here. See further down.
!-----------------------------------------------------------------------
!  if (do_energy) dedt = dedt &
!                              + yup1(zup1(Jx*Ex)) &
!                              + zup1(xup1(Jy*Ey)) &
!                              + xup1(yup1(Jz*Ez))

  if (idbg.ge.2.and.mod(it,nscr).eq.0) then
    open(3,file='mhd.dat',status='unknown',access='direct',recl=lrec(mw))
    write (3,rec=1) eta
    write (3,rec=2) Jx
    write (3,rec=3) Jy
    write (3,rec=4) Jz
    write (3,rec=5) Ex
    write (3,rec=6) Ey
    write (3,rec=7) Ez
    write (3,rec=8) dedt
    close (3)
  end if

!-----------------------------------------------------------------------
!  Lorenz force = I x B
!-----------------------------------------------------------------------
  allocate (Bx_y(mx,my,mz), Bx_z(mx,my,mz))
  allocate (By_z(mx,my,mz), By_x(mx,my,mz))
  allocate (Bz_x(mx,my,mz), Bz_y(mx,my,mz))
  Bx_y = ydn(Bx) ; Bx_z = zdn(Bx)
  By_z = zdn(By) ; By_x = xdn(By)
  Bz_x = xdn(Bz) ; Bz_y = ydn(Bz)
  dpxdt = dpxdt + zup(Jy*Bz_x) - yup(Jz*By_x)
  dpydt = dpydt + xup(Jz*Bx_y) - zup(Jx*Bz_y)
  dpzdt = dpzdt + yup(Jx*By_z) - xup(Jy*Bx_z)

!-----------------------------------------------------------------------
!  Electric field   E =  eta I - uxB
!-----------------------------------------------------------------------
  allocate (Ux_y(mx,my,mz), Ux_z(mx,my,mz))
  allocate (Uy_z(mx,my,mz), Uy_x(mx,my,mz))
  allocate (Uz_x(mx,my,mz), Uz_y(mx,my,mz))
  Ux_y = ydn(Ux) ; Ux_z = zdn(Ux)
  Uy_z = zdn(Uy) ; Uy_x = xdn(Uy)
  Uz_x = xdn(Uz) ; Uz_y = ydn(Uz)
  Ex = Ex - Uy_z*Bz_y + Uz_y*By_z
  Ey = Ey - Uz_x*Bx_z + Ux_z*Bz_x 
  Ez = Ez - Ux_y*By_x + Uy_x*Bx_y 
  call efield_boundary (Ex, Ey, Ez, Bx_y, Bx_z, By_z, By_x, Bz_x, Bz_y, &
                        Rx, Ry, Rz, Ux_y, Ux_z, Uy_z, Uy_x, Uz_x, Uz_y, &
                        Ux, Uy, Uz)
  deallocate (Bx_y, Bx_z, By_z, By_x, Bz_x, Bz_y)
  deallocate (Ux_y, Ux_z, Uy_z, Uy_x, Uz_x, Uz_y)

  if (lb.gt.1) then
    deallocate (Rx, Ry, Rz)
  end if

!-----------------------------------------------------------------------
!  Magnetic field's time derivative, dBdt = - curl(E)
!-----------------------------------------------------------------------
  dBxdt = dBxdt + ddzup(Ey) - ddyup(Ez)
  dBydt = dBydt + ddxup(Ez) - ddzup(Ex)
  dBzdt = dBzdt + ddyup(Ex) - ddxup(Ey)
!-----------------------------------------------------------------------
! Energy dissipation: dedt = dedt + I dot E (e == E_therm + E_kin)
!-----------------------------------------------------------------------
  if (do_energy) dedt = dedt + yup1(zup1(Jx*Ex)) &
                             + zup1(xup1(Jy*Ey)) &
                             + xup1(yup1(Jz*Ez))
  deallocate (Ex, Ey, Ez)
  deallocate (Jx, Jy, Jz)

  END
!**********************************************************************
