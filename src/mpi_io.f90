! $Id: mpi_io.f90,v 1.2 2014/08/28 17:57:32 aake Exp $
!*******************************************************************************
! Write a chunk at specified record number (rec)
!-------------------------------------------------------------------------------
SUBROUTINE file_write_mpi (f,rec)
  USE params
  USE mpi_mod
  implicit none
  real(kind=4), dimension(mx,my,mz):: f
  integer rec,count
  integer status(MPI_STATUS_SIZE)
  integer(kind=MPI_OFFSET_KIND) pos
!
  pos = 4*(rec-1)*mxtot
  pos = pos*mytot*mztot
  CALL MPI_FILE_SET_VIEW (handle, pos, MPI_REAL, filetype, 'native', MPI_INFO_NULL, mpi_err)   
  if (mpi_err .ne. 0) then
    print*,'MPI_FILE_SET_VIEW: rank, mpi_err =',mpi_rank,mpi_err
    call abort_mpi
  endif
  count = mx*my*mz
  CALL MPI_FILE_WRITE_ALL(handle, f , count, MPI_REAL, status, mpi_err)
  if (mpi_err .ne. 0) then
    print*,'MPI_FILE_WRITE_ALL: rank, mpi_err =',mpi_rank,mpi_err
    call abort_mpi
  endif
  if ((verbose > 0 .and. mpi_rank==0) .or. (verbose > 1)) then
    print'(1x,a,4i8,5e12.3)','mpi_file_write_all:',mpi_rank,count,rec,pos,f(1:3,1,1)
  end if
END SUBROUTINE file_write_mpi

!*******************************************************************************
! Read a chunk at specified record number (rec)
!-------------------------------------------------------------------------------
SUBROUTINE file_read_mpi (f, rec)
  USE params
  USE mpi_mod
  implicit none
  real(kind=4), dimension(mx,my,mz):: f
  integer rec, count
  integer status(MPI_STATUS_SIZE)
  integer(kind=MPI_OFFSET_KIND) pos
!
  pos = 4*(rec-1)*mxtot
  pos = pos*mytot*mztot
  count = mx*my*mz
  !if ((verbose > 0 .and. mpi_rank==0) .or. (verbose > 1)) then
    !print'(1x,a,4i8,5e12.3)','mpi_file_read_all:',mpi_rank,count,rec,pos
  !end if
  CALL MPI_FILE_SET_VIEW (handle, pos, MPI_REAL, filetype, 'native', MPI_INFO_NULL, mpi_err)   
  if (mpi_err .ne. 0) then
    print*,'MPI_FILE_SET_VIEW: rank, mpi_err =',mpi_rank,mpi_err
    call abort_mpi
  endif
  CALL MPI_FILE_READ_ALL(handle, f , count, MPI_REAL, status, mpi_err)
  if (mpi_err .ne. 0) then
    print*,'MPI_FILE_READ_ALL: rank, mpi_err =',mpi_rank,mpi_err
    call abort_mpi
  endif
  if ((verbose > 0 .and. mpi_rank==0) .or. (verbose > 1)) then
    print'(1x,a,4i8,5e12.3)','mpi_file_read_all:',mpi_rank,count,rec,pos,f(1:3,1,1)
  end if
END SUBROUTINE file_read_mpi

!*******************************************************************************
SUBROUTINE file_open_mpi (filem, mode)
  USE params
  USE mpi_mod
  implicit none
  integer mode
  character(len=*) filem
  integer, dimension(3):: size, subsize, starts
!
  CALL MPI_FILE_OPEN (mpi_comm_plane(3), filem, mode, MPI_INFO_NULL, handle, mpi_err)    
  if (verbose>0 .and. master) then
    print'(a)',' MPI_FILE_OPEN: file='//trim(filem)//'  mode='// &
      trim(merge(   'MPI_MODE_CREATE', &
                    '               ',iand(mode,MPI_MODE_CREATE) .ne. 0)) // &
      trim(merge('+MPI_MODE_RDWR'  , &
                 '              '    ,iand(mode,MPI_MODE_RDWR)   .ne. 0)) // &
      trim(merge('MPI_MODE_RDONLY'    , &
                 '               '   ,iand(mode,MPI_MODE_RDONLY) .ne. 0))
  endif
  if (mpi_err .ne. 0) then
    if (master) print*,'MPI_FILE_OPEN ERROR: mpi_err =',mpi_err
    call abort_mpi
  endif
  size = (/mxtot, mytot, mztot/)
  subsize = (/mx, my, mztot/)
  starts = (/ixoff, iyoff, 0/)
  if (verbose > 0 .and. mpi_rank==0) print'(i5,3(2x,3i7))',mpi_rank,size,subsize,starts
  if (verbose > 1 .and. mpi_rank >0) print'(i5,3(2x,3i7))',mpi_rank,size,subsize,starts
  CALL MPI_TYPE_CREATE_SUBARRAY (3, size, subsize, starts, MPI_ORDER_FORTRAN, MPI_REAL, filetype, mpi_err)   
  CALL MPI_TYPE_COMMIT (filetype, mpi_err)
  if ((verbose > 0 .and. mpi_rank==0) .or. (verbose > 1)) then
    print'(1x,a,2i8)','mpi_file_open: '//trim(filem),mpi_rank,mpi_err
  end if 
END SUBROUTINE file_open_mpi

!*******************************************************************************
SUBROUTINE file_openw_mpi (filem)
  USE mpi_mod
  implicit none
  character(len=*) filem
!
  call file_open_mpi (filem, MPI_MODE_CREATE + MPI_MODE_RDWR)
END SUBROUTINE file_openw_mpi

!*******************************************************************************
SUBROUTINE file_openr_mpi (filem)
  USE mpi_mod
  implicit none
  character(len=*) filem
!
  call file_open_mpi (filem, MPI_MODE_RDONLY)
END SUBROUTINE file_openr_mpi

!*******************************************************************************
SUBROUTINE file_close_mpi
  USE mpi_mod
  implicit none
  CALL MPI_FILE_CLOSE(handle, mpi_err)
END SUBROUTINE file_close_mpi

