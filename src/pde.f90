! $Id: pde.f90,v 1.62 2012/12/31 16:19:14 aake Exp $
!***********************************************************************
  SUBROUTINE pde(r,px,py,pz,e,d,drdt,dpxdt,dpydt,dpzdt,dedt,dddt,flag, &
                 Bx,By,Bz,dBxdt,dBydt,dBzdt)

  USE params
  USE stagger

  implicit none

  real ds, gam1, average, fmaxval, fCv
  integer ix, iy, iz

  logical flag
  real, dimension(mx,my,mz) :: &
       r,px,py,pz,e,d,drdt,dpxdt,dpydt,dpzdt,dedt,dddt, &
       Bx,By,Bz,dBxdt,dBydt,dBzdt
  real, allocatable, dimension(:,:,:) :: &
       dd,lnr,P,Cs,nu,lnu,lne,ee,Ux,Uy,Uz, &
       Sxx,Syy,Szz,Sxy,Szx,Syz,xdnl,ydnl,zdnl,xdnr,ydnr,zdnr, &
       Txx,Tyy,Tzz,Txy,Tzx,Tyz,ddxd,ddyd,ddzd
  character(len=mid) id
  data id/'$Id: pde.f90,v 1.62 2012/12/31 16:19:14 aake Exp $'/

  if (.not. do_loginterp) then
    call pde_lin(r,px,py,pz,e,d,drdt,dpxdt,dpydt,dpzdt,dedt,dddt,flag, &
                 Bx,By,Bz,dBxdt,dBydt,dBzdt)
    return
  end if

  if (id .ne. '') print *,id
  id = ''

!-----------------------------------------------------------------------
!  Velocities, pressure
!-----------------------------------------------------------------------
! print **,1,dedt(16,106,38:40)/e(16,106,38:40),e(16,106,38:40)
  allocate(lnr(mx,my,mz),xdnl(mx,my,mz),ydnl(mx,my,mz),zdnl(mx,my,mz))  !  4 chunks
  allocate(xdnr(mx,my,mz),ydnr(mx,my,mz),zdnr(mx,my,mz))  !  4 chunks
!$omp parallel do private(iz)
  do iz=1,mz
    do iy=lb,ub
      lnr(:,iy,iz) = alog(r(:,iy,iz))
    end do
  end do
  call density_boundary_log(r,lnr)
  xdnl = xdn(lnr)
  ydnl = ydn(lnr)
  zdnl = zdn(lnr)
  allocate(Ux(mx,my,mz),Uy(mx,my,mz),Uz(mx,my,mz))                      !  6 chunks        
!$omp parallel do private(iz)
  do iz=1,mz
    xdnr(:,:,iz) = exp(xdnl(:,:,iz))
    ydnr(:,:,iz) = exp(ydnl(:,:,iz))
    zdnr(:,:,iz) = exp(zdnl(:,:,iz))
    Ux(:,:,iz) = px(:,:,iz)/xdnr(:,:,iz)
    Uy(:,:,iz) = py(:,:,iz)/ydnr(:,:,iz)
    Uz(:,:,iz) = pz(:,:,iz)/zdnr(:,:,iz)
  enddo
  call velocity_boundary(r,xdnr,ydnr,zdnr,Ux,Uy,Uz,px,py,pz,e,Bx,By,Bz)
  call forceit (r,Ux,Uy,Uz,xdnr,ydnr,zdnr,dpxdt,dpydt,dpzdt)
  deallocate (xdnr,ydnr,zdnr)

  allocate(Sxx(mx,my,mz),Syy(mx,my,mz),Szz(mx,my,mz))                   !  9 chunks
  Sxx = ddxup(Ux)
  Syy = ddyup(Uy)
  Szz = ddzup(Uz)
! print **,1,dedt(16,106,38:40)/e(16,106,38:40),e(16,106,38:40)

!-----------------------------------------------------------------------
!  Numerical viscosity, Richtmyer & Morton type
!-----------------------------------------------------------------------
  allocate(P(mx,my,mz),Cs(mx,my,mz),nu(mx,my,mz))                       ! 12 chunks
!$omp parallel do private(iz)
  do iz=1,mz
    nu(:,:,iz) = Sxx(:,:,iz)+Syy(:,:,iz)+Szz(:,:,iz)
    nu(:,:,iz) = max(-nu(:,:,iz),0.0)
  end do
  if (gamma .eq. 1) then
    gam1 = 1.
  else
    gam1 = gamma-1.
  end if
  Cs = xup(Ux)**2 + yup(Uy)**2  + zup(Uz)**2
  Urms = sqrt(average(Cs))
! print **,1,dedt(16,106,38:40)/e(16,106,38:40),e(16,106,38:40)

!-----------------------------------------------------------------------
!  Energy per unit mass
!-----------------------------------------------------------------------
  allocate(ee(mx,my,mz))
!$omp parallel do private(iz)
  do iz=1,mz
    if (.not. do_energy) then
      e(:,:,iz) = (csound**2/(gamma*gam1))*r(:,:,iz)**gamma
    end if
    ee(:,:,iz) = e(:,:,iz)/r(:,:,iz)
  end do
!-----------------------------------------------------------------------
!  Boundary condition -- may need to calculate pressure internally
!-----------------------------------------------------------------------
  if (do_energy) call energy_boundary(r,lnr,Ux,Uy,Uz,e,ee,p,Bx,By,Bz)
!-----------------------------------------------------------------------
!  Pressure
!-----------------------------------------------------------------------
  if (do_ionization) then
    call pressure(r,ee,p)
  else
!$omp parallel do private(iz)
    do iz=1,mz
      P(:,:,iz)  = gam1*e(:,:,iz)
    end do
  end if
! print **,1,dedt(16,106,38:40)/e(16,106,38:40),e(16,106,38:40)

!-----------------------------------------------------------------------
!  Numerical viscosity, Richtmyer & Morton type
!-----------------------------------------------------------------------
  allocate(lnu(mx,my,mz))                                               ! 13 chunks
  ds = max(dx,dy,dz)
  if (do_momenta) then
!$omp parallel do private(iz), firstprivate(nu1,nu2,ds)
    do iz=1,mz
      Cs(:,:,iz) = sqrt(gamma*P(:,:,iz)/r(:,:,iz)) + sqrt(Cs(:,:,iz))
      nu(:,:,iz) = nu1*(Cs(:,:,iz)) + nu2*ds*nu(:,:,iz)
    end do
  else
!$omp parallel do private(iz), firstprivate(nu1,nu2,ds)
    do iz=1,mz
      Cs(:,:,iz) = sqrt(Cs(:,:,iz)) + 1e-30
      nu(:,:,iz) = nu1*(Cs(:,:,iz)) + nu2*ds*nu(:,:,iz)
    end do
  end if
  if (lb.lt.6) call regularize(nu)
  nu = smooth3(max3(nu))
  call regularize(nu)
  if (flag) then
    ds = min(dx,dy,dz)
    Cs = Cs*(dt/ds)
    Cu = fmaxval('Cu',Cs)
    fCv = 2.*6.2*3.*dt/2.
    Cs = fCv/ds*nu
    Cv = fmaxval('Cv',Cs)
  end if
  deallocate(Cs)                                                        ! 12 chunks
!$omp parallel do private(iz)
  do iz=1,mz
    nu(:,:,iz) = nu(:,:,iz)*r(:,:,iz)
    lnu(:,:,iz) = alog(nu(:,:,iz))
  end do
! print **,1,dedt(16,106,38:40)/e(16,106,38:40),e(16,106,38:40)

!-----------------------------------------------------------------------
!  Mass conservation, cell centered
!-----------------------------------------------------------------------
if (do_density) then
  if (do_2nddiv) then
    if (nur>0) then
      drdt = drdt &
         - ddxup1(px-dx*nur*exp(xdn(lnu))*ddxdn(lnr)) &
         - ddyup1(py-dy*nur*exp(ydn(lnu))*ddydn(lnr)) &
         - ddzup1(pz-dz*nur*exp(zdn(lnu))*ddzdn(lnr))
    else
      drdt = drdt - ddxup1(px) - ddyup1(py) - ddzup1(pz)
    end if
  else
    if (nur>0) then
      drdt = drdt &
         - ddxup(px-dx*nur*exp(xdn(lnu))*ddxdn(lnr)) &
         - ddyup(py-dy*nur*exp(ydn(lnu))*ddydn(lnr)) &
         - ddzup(pz-dz*nur*exp(zdn(lnu))*ddzdn(lnr))
    else
      drdt = drdt - ddxup(px) - ddyup(py) - ddzup(pz)
    end if
  end if
end if
  deallocate(lnr)                                                       !  3 chunks
! print **,1,dedt(16,106,38:40)/e(16,106,38:40),e(16,106,38:40)

if (do_momenta) then
!-----------------------------------------------------------------------
!  Viscous stress, diagonal and off-diagonal elements
!-----------------------------------------------------------------------
  allocate(Txx(mx,my,mz),Tyy(mx,my,mz),Tzz(mx,my,mz))                   ! 15 chunks
!$omp parallel do private(iz,iy,ix)
  do iz=1,mz
    do iy=1,my
    do ix=1,mx
      Txx(ix,iy,iz) = - (2.*dx)*nu(ix,iy,iz)*Sxx(ix,iy,iz) + P(ix,iy,iz)
      Tyy(ix,iy,iz) = - (2.*dy)*nu(ix,iy,iz)*Syy(ix,iy,iz) + P(ix,iy,iz)
      Tzz(ix,iy,iz) = - (2.*dz)*nu(ix,iy,iz)*Szz(ix,iy,iz) + P(ix,iy,iz)
    end do
    end do
    if (do_energy) then
      if (do_dissipation) then 
        dedt(:,:,iz) = dedt(:,:,iz) &
                       - Txx(:,:,iz)*Sxx(:,:,iz) &
                       - Tyy(:,:,iz)*Syy(:,:,iz) &
                       - Tzz(:,:,iz)*Szz(:,:,iz)
      else
          dedt(:,:,iz) = dedt(:,:,iz) - P(:,:,iz)* &
                         (Sxx(:,:,iz) + Syy(:,:,iz) + Szz(:,:,iz))
      end if
    end if
  end do
  deallocate(P,Sxx,Syy,Szz)                                             ! 10 chunks
! print **,1,dedt(16,106,38:40)/e(16,106,38:40),e(16,106,38:40)

  allocate(Sxy(mx,my,mz),Syz(mx,my,mz),Szx(mx,my,mz))                   ! 13 chunks
  Sxy = (ddxdn(Uy)+ddydn(Ux))*0.5
  Syz = (ddydn(Uz)+ddzdn(Uy))*0.5
  Szx = (ddzdn(Ux)+ddxdn(Uz))*0.5
  allocate(Txy(mx,my,mz),Tyz(mx,my,mz),Tzx(mx,my,mz))                   ! 16 chunks
  Txy = - (dx+dy)*exp(xdn(ydn(lnu)))*Sxy
  Tyz = - (dy+dz)*exp(ydn(zdn(lnu)))*Syz
  Tzx = - (dz+dx)*exp(zdn(xdn(lnu)))*Szx
! one factor 2 belongs in front of nu, one is for the mirror term
  if (do_energy .and. do_dissipation) dedt = dedt &
      + (2.*(dx+dy))*nu*xup(yup(Sxy))**2 &
      + (2.*(dy+dz))*nu*yup(zup(Syz))**2 &
      + (2.*(dz+dx))*nu*zup(xup(Szx))**2 
  deallocate(Sxy,Syz,Szx)                                               ! 13 chunks
! print **,2,dedt(16,106,38:40)/e(16,106,38:40),e(16,106,38:40)

!-----------------------------------------------------------------------
!  Add Reynolds stress
!-----------------------------------------------------------------------
!*$* assert do (concurrent)
  Txx = Txx + r*xup(Ux)**2
!*$* assert do (concurrent)
  Tyy = Tyy + r*yup(Uy)**2
!*$* assert do (concurrent)
  Tzz = Tzz + r*zup(Uz)**2
  Txy = Txy + exp(xdn(ydnl))*ydn(Ux)*xdn(Uy)
  Tyz = Tyz + exp(ydn(zdnl))*zdn(Uy)*ydn(Uz)
  Tzx = Tzx + exp(zdn(xdnl))*xdn(Uz)*zdn(Ux)
  deallocate(xdnl,ydnl,zdnl)                                            ! 10 chunks

!-----------------------------------------------------------------------
!  Equations of motion, face centred
!-----------------------------------------------------------------------
  if (do_2nddiv) then
    dpxdt = dpxdt - ddxdn1(Txx) - ddyup1(Txy) - ddzup1(Tzx)
    dpydt = dpydt - ddydn1(Tyy) - ddzup1(Tyz) - ddxup1(Txy)
    dpzdt = dpzdt - ddzdn1(Tzz) - ddxup1(Tzx) - ddyup1(Tyz)
  else
    dpxdt = dpxdt - ddxdn (Txx) - ddyup (Txy) - ddzup (Tzx)
    dpydt = dpydt - ddydn (Tyy) - ddzup (Tyz) - ddxup (Txy)
    dpzdt = dpzdt - ddzdn (Tzz) - ddxup (Tzx) - ddyup (Tyz)
  end if
  deallocate(Txy,Tyz,Tzx,Txx,Tyy,Tzz)                                   !  1 chunks
else
  deallocate(P,Sxx,Syy,Szz,xdnl,ydnl,zdnl)
end if

!-----------------------------------------------------------------------
!  MHD
!-----------------------------------------------------------------------
! print **,3,dedt(16,106,38:40)/e(16,106,38:40),e(16,106,38:40)
  if (do_mhd) then
! print **,4,dedt(16,106,38:40)/e(16,106,38:40),e(16,106,38:40)
    call mhd(r,ee,Ux,Uy,Uz,Bx,By,Bz,dpxdt,dpydt,dpzdt,dedt,dBxdt,dBydt,dBzdt,flag)
  end if
! print **,4,dedt(16,106,38:40)/e(16,106,38:40),e(16,106,38:40)

!-----------------------------------------------------------------------
!  Abundance per unit mass
!-----------------------------------------------------------------------
  if (do_pscalar .or. do_cool) then
    allocate (dd(mx,my,mz))
    dd=d/r
  end if

!-----------------------------------------------------------------------
!  Passive scalar
!-----------------------------------------------------------------------
  call passive(nu,lnu,r,px,py,pz,Ux,Uy,Uz,dd,dddt)
  deallocate (nu,Ux,Uy,Uz)                                              !  7 chunks

  if (do_energy) then

    allocate(lne(mx,my,mz))                                             !  3 chunks
!$omp parallel do private(iz)
    do iz=1,mz
      lne(:,:,iz) = alog(ee(:,:,iz))
!!      lnu(:,:,iz) = lnu(:,:,iz)+lne(:,:,iz)
    end do

!-----------------------------------------------------------------------
!  Energy advection and diffusion
!-----------------------------------------------------------------------
    if (do_2nddiv) then
      dedt = dedt &
       - ddxup1(exp(xdn(lne))*(px - dx*exp(xdn(lnu))*ddxdn(lne))) &
       - ddyup1(exp(ydn(lne))*(py - dy*exp(ydn(lnu))*ddydn(lne))) &
       - ddzup1(exp(zdn(lne))*(pz - dz*exp(zdn(lnu))*ddzdn(lne)))
    else
      dedt = dedt &
       - ddxup (exp(xdn(lne))*(px - dx*exp(xdn(lnu))*ddxdn(lne))) &
       - ddyup (exp(ydn(lne))*(py - dy*exp(ydn(lnu))*ddydn(lne))) &
       - ddzup (exp(zdn(lne))*(pz - dz*exp(zdn(lnu))*ddzdn(lne)))
    end if 

!-----------------------------------------------------------------------
!  Cooling and conduction
!-----------------------------------------------------------------------
    if (do_cool) then
      call conduction (r,e,ee,Bx,By,Bz,dedt)
      call coolit (r,ee,lne,dd,dedt)
    end if
    deallocate(lne)                                                     !  3 chunks         
  end if
  deallocate(ee,lnu)                                                    !  1 chunks
  if (do_pscalar .or. do_cool) deallocate(dd)                           !  0 chunks
! print **,5,dedt(16,106,38:40)/e(16,106,38:40),e(16,106,38:40)

  call ddt_boundary (r,px,py,pz,e,Bx,By,Bz, &
                     drdt,dpxdt,dpydt,dpzdt, &
                     dedt,dBxdt,dBydt,dBzdt)

  END
