! $Id: pde_advect.f90,v 1.1 2011/07/11 15:08:40 aake Exp $
!***********************************************************************
SUBROUTINE pde (flag)
  USE params
  USE variables
  USE arrays
  implicit none
  logical flag
  real, allocatable, dimension(:,:,:):: &
                  wk22,wk23,wk24,wk25,wk26,wk27,wk28
!.......................................................................
  allocate (wk22(mx,my,mz),wk23(mx,my,mz))
  allocate (wk24(mx,my,mz),wk25(mx,my,mz),wk26(mx,my,mz))
  allocate (wk27(mx,my,mz),wk28(mx,my,mz))
  call pde1 (flag,r   ,px  ,py  ,pz  ,e   ,Bx  ,By  ,Bz  ,           &
                  drdt,dpxdt,dpydt,dpzdt,dedt,dBxdt,dBydt,dBzdt ,    &
                  Ux  ,Uy  ,Uz  ,Ex  ,Ey  ,Ez  ,                     &
                  wk01,wk02,wk03,wk04,wk05,wk06,wk07,wk08,wk09,wk10, &
                  wk11,wk12,wk13,wk14,wk15,wk16,wk17,wk18,wk19,wk20, &
                  wk21,wk22,wk23,wk24,wk25,wk26,wk27,wk28,scr1,scr2, &
                  scr3,scr4,scr5,scr6)
  deallocate (wk22,wk23,wk24,wk25,wk26,wk27,wk28)
END SUBROUTINE

!***********************************************************************
SUBROUTINE pde1 (flag,rho ,px  ,py  ,pz  ,e   ,Bx  ,By  ,Bz ,           &
                      drdt,dpxdt,dpydt,dpzdt,dedt,dBxdt,dBydt,dBzdt ,   &
                      Ux  ,Uy  ,Uz  ,Ex  ,Ey  ,Ez  ,                    &
                      lnr ,nu  ,divU,U   ,xdnr,ydnr,zdnr,Jx ,Jy  ,Jz  , &
                      ee  ,pg  ,cA  ,lne ,lnu ,eta ,Sxx ,Syy,Szz ,Sxy , &
                      Syz ,Szx ,Txx ,Tyy ,Tzz ,Txy ,Tyz ,Tzx,scr1,scr2, &
                      scr3,scr4,scr5,scr6)
  USE params
  USE stagger
  implicit none
  logical flag
  real fmaxval
  real(kind=8), allocatable, dimension(:):: lnra
  integer ix, iy, iz
  real, dimension(mx,my,mz):: &
                      rho ,px  ,py  ,pz  ,e   ,Bx  ,By  ,Bz ,           &
                      drdt,dpxdt,dpydt,dpzdt,dedt,dBxdt,dBydt,dBzdt ,   &
                      Ux  ,Uy  ,Uz  ,Ex  ,Ey  ,Ez  ,                    &
                      lnr ,nu  ,divU,U   ,xdnr,ydnr,zdnr,Jx ,Jy  ,Jz  , &
                      ee  ,pg  ,cA  ,lne ,lnu ,eta ,Sxx ,Syy,Szz ,Sxy , &
                      Syz ,Szx ,Txx ,Tyy ,Tzz ,Txy ,Tyz ,Tzx,scr1,scr2, &
                      scr3,scr4,scr5,scr6
  real, allocatable, dimension(:,:,:):: qx, qy, qz, Vx, Vy, Vz,         &
                                        Uxc, Uyc, Uzc, Vxc, Vyc, Vzc
!-----------------------------------------------------------------------
!  Mass conservation, cell centered
!-----------------------------------------------------------------------

  lnr = alog(rho)
    call density_boundary(rho,lnr,py,e)
  xdnr = exp(xdn(lnr))
  ydnr = exp(ydn(lnr))
  zdnr = exp(zdn(lnr))

!-------------------------------------------------------------------------------
!  Velocities
!-------------------------------------------------------------------------------
  allocate (Uxc(mx,my,mz), Uyc(mx,my,mz), Uzc(mx,my,mz))
  Ux = px/xdnr
  Uy = py/ydnr
  Uz = pz/zdnr
    call velocity_boundary (rho,xdnr,ydnr,zdnr,Ux,Uy,Uz,px,py,pz,e,Bx,By,Bz)
  Uxc = xup(Ux)
  Uyc = yup(Uy)
  Uzc = zup(Uz)
  U  = sqrt(Uxc**2 + Uyc**2  + Uzc**2)
    call forceit (rho,Ux,Uy,Uz,xdnr,ydnr,zdnr,dpxdt,dpydt,dpzdt)
    call mfield_boundary (Ux,Uy,Uz,Bx,By,Bz)
  
!-------------------------------------------------------------------------------
!  Equation of state 
!-------------------------------------------------------------------------------
  ee = e/rho
  call pressure (rho, ee, pg)
  cA = sqrt((xup(Bx)**2 + yup(By)**2  + zup(Bz)**2 + pg*gamma)/rho)

!-------------------------------------------------------------------------------
!  Numerical diffusion and resistivity, Richtmyer & Morton type
!-------------------------------------------------------------------------------
  Sxx = ddxup(Ux)
  Syy = ddyup(Uy)
  Szz = ddzup(Uz)
  divU = Sxx+Syy+Szz
  do iz=1,mz; do iy=1,my; do ix=1,mx
    scr1(ix,iy,iz) = max(dxm(ix),dym(iy),dzm(iz))
  end do; end do; end do
  nu = nu1*scr1*U + nu3*scr1*cA + nu2*scr1**2*smooth3(max3(max(-divU,0.0)))
  call dumpn(nu,'nu','nu.dmp',0)
  if (nu4 > 0) nu = nu + &
     nu4*scr1**3*(xup1(ddxdn(lnr)**2)+yup1(ddydn(lnr)**2)+zup1(ddzdn(lnr)**2))
  eta = nuB*nu

!-------------------------------------------------------------------------------
!  Courant numbers based on wave speed, viscous and resistive diffusion
!-------------------------------------------------------------------------------
  do iz=1,mz; do iy=1,my; do ix=1,mx
    scr1(ix,iy,iz) = 1./min(dxm(ix),dym(iy),dzm(iz))
  end do; end do; end do
  Cu = dt*fmaxval('U+Ca',(U+cA)*scr1)
  Cv = dt*fmaxval('nu' ,nu *scr1**2)*6.2*3./2.*0.4/0.6
  Cn = dt*fmaxval('eta',eta*scr1**2)*6.2*3./2.*0.4/0.6

!-------------------------------------------------------------------------------
!  Mass conservation, cell centered
!-------------------------------------------------------------------------------
  lnu = alog(nu)
  lnu = lnu + lnr
  allocate (Vx (mx,my,mz), Vy (mx,my,mz), Vz (mx,my,mz))
  allocate (Vxc(mx,my,mz), Vyc(mx,my,mz), Vzc(mx,my,mz))
  if (nur > 0.) then
    allocate(qx(mx,my,mz),qy(mx,my,mz),qz(mx,my,mz))
    allocate(lnra(my))
    call haverage_subr (lnr,lnra)
    do iz=1,mz; do iy=1,my; do ix=1,mx
      scr1(ix,iy,iz) = lnr(ix,iy,iz)-lnra(iy)
    end do; end do; end do;
    qx = px - nur*exp(xdn(lnu))*ddxdn(scr1); Vx = qx/xdnr; Vxc = xup(Vx)
    qy = py - nur*exp(ydn(lnu))*ddydn(scr1); Vy = qy/ydnr; Vyc = yup(Vy)
    qz = pz - nur*exp(zdn(lnu))*ddzdn(scr1); Vz = qz/zdnr; Vzc = zup(Vz)
    drdt = drdt - ddxup(qx) - ddyup(qy) - ddzup(qz)
    deallocate(lnra)
    deallocate(qx,qy,qz)
  else
    Vx = Ux; Vxc = Uxc
    Vy = Uy; Vyc = Uyc
    Vz = Uz; Vzc = Uzc
    drdt = drdt - ddxup(px) - ddyup(py) - ddzup(pz)
  end if

!-------------------------------------------------------------------------------
!  Viscous stress, diagonal and off-diagonal elements
!-------------------------------------------------------------------------------
  Txx = - nuS*nu*rho*Sxx - (1.-nuS)*nu*rho*divU
  Tyy = - nuS*nu*rho*Syy - (1.-nuS)*nu*rho*divU
  Tzz = - nuS*nu*rho*Szz - (1.-nuS)*nu*rho*divU
  Sxy = 0.5*(ddxdn(Uy)+ddydn(Ux))
  Syz = 0.5*(ddydn(Uz)+ddzdn(Uy))
  Szx = 0.5*(ddzdn(Ux)+ddxdn(Uz))
  Txy = - nuS*exp(xdn(ydn(lnu)))*Sxy
  Tyz = - nuS*exp(ydn(zdn(lnu)))*Syz
  Tzx = - nuS*exp(zdn(xdn(lnu)))*Szx
 
!-------------------------------------------------------------------------------
!  Electric current j = curl(B), edge centered
!-------------------------------------------------------------------------------
  jx = ddydn(Bz) - ddzdn(By)
  jy = ddzdn(Bx) - ddxdn(Bz)
  jz = ddxdn(By) - ddydn(Bx)
  call ecurrent_boundary (Ex, Ey, Ez, Jx, Jy, Jz)

!-------------------------------------------------------------------------------
!  Lorentz force
!-------------------------------------------------------------------------------
  dpxdt = dpxdt + zup(jy*xdn(Bz)) - yup(jz*xdn(By))
  dpydt = dpydt + xup(jz*ydn(Bx)) - zup(jx*ydn(Bz))
  dpzdt = dpzdt + yup(jx*zdn(By)) - xup(jy*zdn(Bx))

!-------------------------------------------------------------------------------
!  Electric field   E = eta j - (u x B), edge centered
!-------------------------------------------------------------------------------
  Ex = ydn(zdn(eta))*jx
  Ey = zdn(xdn(eta))*jy
  Ez = xdn(ydn(eta))*jz
  if (gamma .ne. 1.) then
    dedt = dedt + zup(yup(jx*Ex) + xup(jy*Ey)) + xup(yup(jz*Ez))
  end if
  Ex = Ex - zdn(Vy)*ydn(Bz) + ydn(Vz)*zdn(By)
  Ey = Ey - xdn(Vz)*zdn(Bx) + zdn(Vx)*xdn(Bz)
  Ez = Ez - ydn(Vx)*xdn(By) + xdn(Vy)*ydn(Bx)
    call efield_boundary (Ex, Ey, Ez, scr1, scr2, scr3, scr4, scr5, scr6, &
                          Bx, By, Bz, scr1, scr2, scr3, scr4, scr5, scr6, &
                          Ux, Uy, Uz)

!-------------------------------------------------------------------------------
!  Magnetic field's time derivative, dBdt = - curl(E), face centered
!-------------------------------------------------------------------------------
  dBxdt = dBxdt + ddzup(Ey) - ddyup(Ez)
  dBydt = dBydt + ddxup(Ez) - ddzup(Ex)
  dBzdt = dBzdt + ddyup(Ex) - ddxup(Ey)

!-------------------------------------------------------------------------------
!  Energy eq, with P*dV work, difffusion, kinetic and Joule dissipation
!-------------------------------------------------------------------------------
  call energy_boundary(rho,lnr,Ux,Uy,Uz,e,ee,pg,Bx,By,Bz)
  dedt = dedt - pg*divU &
       - ddxup(xdn(ee)*px - nue*exp(xdn(lnu))*ddxdn(ee)) &
       - ddyup(ydn(ee)*py - nue*exp(ydn(lnu))*ddydn(ee)) &
       - ddzup(zdn(ee)*pz - nue*exp(zdn(lnu))*ddzdn(ee)) &
       - (Txx*Sxx + Tyy*Syy + Tzz*Szz) &
       - 2.*(xup1(yup1(Txy*Sxy) + zup1(Tzx*Szx)) + yup1(zup1(Tyz*Syz)))
  call coolit (rho, ee, lne, scr1, dedt)
!-------------------------------------------------------------------------------
!  Pressure and Reynolds stress
!-------------------------------------------------------------------------------
  Txx = Txx + pg + rho*Uxc*Vxc
  Tyy = Tyy + pg + rho*Uyc*Vyc
  Tzz = Tzz + pg + rho*Uzc*Vzc
  Txy = Txy + exp(xdn(ydn(lnr)))*ydn(Ux)*xdn(Vy)
  Tyz = Tyz + exp(ydn(zdn(lnr)))*zdn(Uy)*ydn(Vz)
  Tzx = Tzx + exp(xdn(zdn(lnr)))*zdn(Ux)*xdn(Vz)
  deallocate (Vx, Vy, Vz, Vxc, Vyc, Vzc, Uxc, Uyc, Uzc)

!-------------------------------------------------------------------------------
!  Equations of motion, face centred
!-------------------------------------------------------------------------------
  dpxdt = dpxdt - ddxdn(Txx) - ddyup(Txy) - ddzup(Tzx)
  dpydt = dpydt - ddydn(Tyy) - ddzup(Tyz) - ddxup(Txy)
  dpzdt = dpzdt - ddzdn(Tzz) - ddxup(Tzx) - ddyup(Tyz)

  call ddt_boundary (rho,px,py,pz,e,pg,Bx,By,Bz, &
                     drdt,dpxdt,dpydt,dpzdt, &
                     dedt,dBxdt,dBydt,dBzdt)
END
