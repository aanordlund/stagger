! $Id: courant_cm.f90,v 1.7 2006/08/07 11:16:59 aake Exp $
!-----------------------------------------------------------------------
SUBROUTINE courant (lnr, ee, d, dlnrdt, deedt, dddt)
  USE params
  real, dimension(mx,my,mz) :: lnr, ee, d, dlnrdt, deedt, dddt
  real, allocatable, dimension(:,:,:) :: scr
  integer iz, lb1, ub1, loc(3)
  real fmaxval
  character(len=mid) id
  data id/'$Id: courant_cm.f90,v 1.7 2006/08/07 11:16:59 aake Exp $'/

  call print_id (id)

!-----------------------------------------------------------------------
!  Courant conditions
!-----------------------------------------------------------------------

  lb1 = max(min(lb,my),1)
  ub1 = max(min(ub,my),1)

  Cr = 0.
  if (do_density) then
    allocate (scr(mx,my,mz))
    !$omp parallel private(iz)
    do iz=izs,ize
      scr(:,lb1:ub1,iz) = dt*abs(dlnrdt(:,lb1:ub1,iz))
    end do
    !Cr = fmaxval('Cr',scr)
    call fmaxval_subr ('Cr', scr, Cr)
    !$omp end parallel
    deallocate (scr)
  end if

  Ce = 0.
  if (do_energy) then
    allocate (scr(mx,my,mz))
    !$omp parallel private(iz)
    do iz=izs,ize
      scr(:,lb1:ub1,iz) = dt*abs(deedt(:,lb1:ub1,iz)/ee(:,lb1:ub1,iz))
    end do
    !Ce = fmaxval('Ce',scr)
    call fmaxval_subr ('Ce', scr, Ce)
    !$omp end parallel
    deallocate (scr)
  end if

  if (do_pscalar) then
    allocate (scr(mx,my,mz))
    !$omp parallel private(iz)
    do iz=izs,ize
      scr(:,lb1:ub1,iz) = dt*abs(dddt(:,lb1:ub1,iz)/d(:,lb1:ub1,iz))
    end do
    !Cr = max(Cr,fmaxval('Cd',scr))
    call fmaxval_subr ('Cr', scr, Cd)
    Cr = max(Cr,Cd)
    !$omp end parallel
    deallocate (scr)
  end if

  if (.not. do_momenta) then
    Cb=0.
    Cv=0
    Cn=0.
  end if
  if (idbg > 0) print *,mpi_rank,' courant: ',Cu,Cr,Ce,Cb

  if (Cdt.gt.0.) dt = dt*min(Cdt/max(Cu,Cb,1e-30),Cdtr/max(Cr,Ce,1e-30),Cdtd/max(Cv,Cn,1e-30))

  if (it > 2 .and. tsnap > 0. .and. t+dt >= tsnap*(isnap+isnap0)) then
    dtold = dt
    dt = max(dt*0.001, tsnap*(isnap+isnap0)-t)
    do while (t+dt < tsnap*(isnap+isnap0))
      dt = dt*2.
    end do
  else
    dt = min(dt, 2.*dtold)
    dtold = dt
  endif

  return
END
