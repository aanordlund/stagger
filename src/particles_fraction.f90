! $Id: particles_fraction.f90,v 1.25 2012/12/31 16:19:13 aake Exp $
!************************************************************************
MODULE particles
  USE params
  real, allocatable, dimension(:):: rx,ry,rz,drxdt,drydt,drzdt
  logical do_particles
  integer nx,ny,nz,nzs,nze
  real fraction
  common /cpart/nzs, nze
!$omp threadprivate(/cpart/)
END module

!************************************************************************
SUBROUTINE init_particles
  USE params
  USE particles
  implicit none
  integer ipart,ix,iy,iz
  character(len=mid):: id='$Id: particles_fraction.f90,v 1.25 2012/12/31 16:19:13 aake Exp $'

  call print_id (id)

  call read_part
  if (.not. do_particles) return

  nx = mx*fraction
  ny = my*fraction
  nz = mz*fraction

!$omp parallel
  nzs = 1 + ((omp_mythread  )*(mz-1))/omp_nthreads
  nze = 1 + ((omp_mythread+1)*(mz-1))/omp_nthreads
  print *,'particles:', omp_mythread, nzs, nze, nz, omp_nthreads
!$omp end parallel

  npart = nx*ny*nz
  print *,'particles: nx,ny,nz,Mp =',nx,ny,nz,npart*1e-6
  print *,'mesh: x=',xm(1),dx,xm(mx)
  print *,'mesh: y=',ym(1),dy,ym(my)
  print *,'mesh: z=',zm(1),dz,zm(mz)

  allocate (rx(npart),ry(npart),rz(npart))
  allocate (drxdt(npart),drydt(npart),drzdt(npart))
!$omp parallel private(iz,iy,ix,ipart)
  do iz=nzs,nze
  do iy=1,ny
  do ix=1,nx
    ipart = ix+(iy-1)*nx+(iz-1)*nx*ny
    rx(ipart) = xm(0)+ix*(xm(mx)-xm(0))/nx
    ry(ipart) = ym(0)+iy*(ym(my)-ym(0))/ny
    rz(ipart) = zm(0)+iz*(zm(mz)-zm(0))/nz
    drxdt(ipart) = 0.
    drydt(ipart) = 0.
    drzdt(ipart) = 0.
  end do
  end do
  end do
!$omp end parallel
END

!************************************************************************
SUBROUTINE read_part
  USE params
  USE particles
  implicit none
  namelist /part/ do_particles, fraction

  do_particles = .false.
  fraction = 0.5
  rewind (stdin); read (stdin,part)
  if (master) write (*,part)
END

!************************************************************************
SUBROUTINE read_particles (jsnap)
  USE params
  USE particles
  implicit none
  integer lrec,jsnap,i
  character(len=mfile) name, fname
  logical exists

  if (.not. do_particles) return
 
  fname = name('particles.dat','par',from)
  inquire (file=fname,exist=exists)
  if (exists) then
    i = index(fname,' ')-1
    print *,'reading particles:', jsnap,t,fname(1:i)
    open(10,file=fname,status='old', &
      access='direct', recl=3*lrec(npart))
    read (10,rec=1+jsnap) rx,ry,rz
    close (10)
  end if
END

!************************************************************************
SUBROUTINE write_particles (jsnap)
  USE params
  USE particles
  implicit none
  integer lrec,jsnap
  character(len=mfile) name, fname

  if (.not. do_particles) return
  fname = name('particles.dat','par',file)
  call mpi_name(fname)
  open(11,file=fname,status='unknown', &
    access='direct', recl=3*lrec(npart))
  write (11,rec=1+jsnap) rx,ry,rz
  close (11)
  print '(a,i4,5x,a)','particles:', jsnap, name('particles.dat','par',file)
END

!************************************************************************
SUBROUTINE trace_particles (Ux, Uy, Uz)
!
!  3rd order, 2-N storage Runge-Kutta.  Advances the trace particles
!  forward in time, given velocities computed in the subroutine pde.
!
  USE params
  USE particles
  USE timeintegration
  
  implicit none
  integer ipart,jx,jy,jz,jx1,jy1,jz1,ix,iy,iz
  real, dimension(mx,my,mz) :: Ux,Uy,Uz
  real dtb,ax,ay,az,cx,cy,cz
  character(len=mid):: id='$Id: particles_fraction.f90,v 1.25 2012/12/31 16:19:13 aake Exp $'
  logical omp_in_parallel

  if (.not. do_particles) return
  if (id.ne.' ') print *,id; id=' '

  dtb = dt*beta(isubstep)
  cx = mx/sx
  cy = my/sy
  cz = mz/sz
!$omp parallel private(iz,iy,ix,ipart,ax,ay,az,jx,jy,jz,jx1,jy1,jz1) if (.not. omp_in_parallel())
  if (isubstep .eq. 1) then
    do iz=nzs,nze
    do iy=1,ny
    do ix=1,nx
      ipart = ix+(iy-1)*nx+(iz-1)*nx*ny
      ax = (rx(ipart)-xm(1))*cx
      ay = (ry(ipart)-ym(1))*cy
      az = (rz(ipart)-zm(1))*cz
      jx = ax
      jy = ay
      jz = az
      ax = ax-jx
      ay = ay-jy
      az = az-jz
      jx = mod(jx+100*mx,mx)+1
      jy = mod(jy+100*my,my)+1
      jz = mod(jz+100*mz,mz)+1
      jx1 = mod(jx+1,mx)+1
      jy1 = mod(jy+1,my)+1
      jz1 = mod(jz+1,mz)+1
      drxdt(ipart) = (1.-az)*((1.-ay)*((1.-ax)*Ux(jx,jy ,jz )+ax*Ux(jx1,jy ,jz ))  &
                                + ay *((1.-ax)*Ux(jx,jy1,jz )+ax*Ux(jx1,jy1,jz ))) &
                    +    az *((1.-ay)*((1.-ax)*Ux(jx,jy ,jz1)+ax*Ux(jx1,jy ,jz1))  &
                                + ay *((1.-ax)*Ux(jx,jy1,jz1)+ax*Ux(jx1,jy1,jz1)))
      drydt(ipart) = (1.-az)*((1.-ay)*((1.-ax)*Uy(jx,jy ,jz )+ax*Uy(jx1,jy ,jz ))  &
                                + ay *((1.-ax)*Uy(jx,jy1,jz )+ax*Uy(jx1,jy1,jz ))) &
                    +    az *((1.-ay)*((1.-ax)*Uy(jx,jy ,jz1)+ax*Uy(jx1,jy ,jz1))  &
                                + ay *((1.-ax)*Uy(jx,jy1,jz1)+ax*Uy(jx1,jy1,jz1)))
      drzdt(ipart) = (1.-az)*((1.-ay)*((1.-ax)*Uz(jx,jy ,jz )+ax*Uz(jx1,jy ,jz ))  &
                                + ay *((1.-ax)*Uz(jx,jy1,jz )+ax*Uz(jx1,jy1,jz ))) &
                    +    az *((1.-ay)*((1.-ax)*Uz(jx,jy ,jz1)+ax*Uz(jx1,jy ,jz1))  &
                                + ay *((1.-ax)*Uz(jx,jy1,jz1)+ax*Uz(jx1,jy1,jz1)))
      rx(ipart) = rx(ipart) + dtb*drxdt(ipart)
      ry(ipart) = ry(ipart) + dtb*drydt(ipart)
      rz(ipart) = rz(ipart) + dtb*drzdt(ipart)
    end do
    end do
    end do
  else
    do iz=nzs,nze
    do iy=1,ny
    do ix=1,nx
      ipart = ix+(iy-1)*nx+(iz-1)*nx*ny
      ax = (rx(ipart)-xm(1))*cx
      ay = (ry(ipart)-ym(1))*cy
      az = (rz(ipart)-zm(1))*cz
      jx = ax
      jy = ay
      jz = az
      ax = ax-jx
      ay = ay-jy
      az = az-jz
      jx = mod(jx+100*mx,mx)+1
      jy = mod(jy+100*my,my)+1
      jz = mod(jz+100*mz,mz)+1
      jx1 = mod(jx+1,mx)+1
      jy1 = mod(jy+1,my)+1
      jz1 = mod(jz+1,mz)+1
      drxdt(ipart) = alpha(isubstep)*drxdt(ipart) + &
                        (1.-az)*((1.-ay)*((1.-ax)*Ux(jx,jy ,jz )+ax*Ux(jx1,jy ,jz ))  &
                                   + ay *((1.-ax)*Ux(jx,jy1,jz )+ax*Ux(jx1,jy1,jz ))) &
                       +    az *((1.-ay)*((1.-ax)*Ux(jx,jy ,jz1)+ax*Ux(jx1,jy ,jz1))  &
                                   + ay *((1.-ax)*Ux(jx,jy1,jz1)+ax*Ux(jx1,jy1,jz1)))
      drydt(ipart) = alpha(isubstep)*drydt(ipart) + &
                        (1.-az)*((1.-ay)*((1.-ax)*Uy(jx,jy ,jz )+ax*Uy(jx1,jy ,jz ))  &
                                   + ay *((1.-ax)*Uy(jx,jy1,jz )+ax*Uy(jx1,jy1,jz ))) &
                       +    az *((1.-ay)*((1.-ax)*Uy(jx,jy ,jz1)+ax*Uy(jx1,jy ,jz1))  &
                                   + ay *((1.-ax)*Uy(jx,jy1,jz1)+ax*Uy(jx1,jy1,jz1)))
      drzdt(ipart) = alpha(isubstep)*drzdt(ipart) + &
                        (1.-az)*((1.-ay)*((1.-ax)*Uz(jx,jy ,jz )+ax*Uz(jx1,jy ,jz ))  &
                                   + ay *((1.-ax)*Uz(jx,jy1,jz )+ax*Uz(jx1,jy1,jz ))) &
                       +    az *((1.-ay)*((1.-ax)*Uz(jx,jy ,jz1)+ax*Uz(jx1,jy ,jz1))  &
                                   + ay *((1.-ax)*Uz(jx,jy1,jz1)+ax*Uz(jx1,jy1,jz1)))
      rx(ipart) = rx(ipart) + dtb*drxdt(ipart)
      ry(ipart) = ry(ipart) + dtb*drydt(ipart)
      rz(ipart) = rz(ipart) + dtb*drzdt(ipart)
    end do
    end do
    end do
  end if
!$omp end parallel

  END
