! $Id: gottlieb_shu.f90,v 1.3 2013/08/02 12:35:48 aake Exp $
!************************************************************************
MODULE timeintegration
  implicit none
  real, dimension(3):: alpha, beta
  real, allocatable, dimension(:,:,:) :: rold, pxold, pyold, pzold, &
                                         eold, Bxold, Byold, Bzold, &
                                         dold
END MODULE timeintegration

!------------------------------------------------------------------------
SUBROUTINE init_timestep
  USE params
  USE timeintegration
  implicit none

  if (do_density) then
    allocate (rold(mx,my,mz))
  end if
  if (do_momenta) then
    allocate (pxold(mx,my,mz))
    allocate (pyold(mx,my,mz))
    allocate (pzold(mx,my,mz))
  end if
  if (do_energy) then
    allocate (eold(mx,my,mz))
  end if
  if (do_pscalar) then
    allocate (dold(mx,my,mz))
  end if
  if (do_mhd) then
    allocate (Bxold(mx,my,mz))
    allocate (Byold(mx,my,mz))
    allocate (Bzold(mx,my,mz))
  end if
END SUBROUTINE init_timestep

!------------------------------------------------------------------------
SUBROUTINE timestep (r,px,py,pz,e,d, &
                 drdt,dpxdt,dpydt,dpzdt,dedt,dddt, &
                   Bx,By,Bz,dBxdt,dBydt,dBzdt)
!
!  Gottlieb & Shu (1998), 3rd order RK. Advances the eight MHD variables
!  forward in time, given time derivatives computed in the subroutine pde.
!
  USE params
  USE timeintegration
  implicit none
  real, dimension(mx,my,mz):: r,px,py,pz,e,d, &
                           drdt,dpxdt,dpydt,dpzdt,dedt,dddt, &
                             Bx,By,Bz,dBxdt,dBydt,dBzdt
  integer, save:: idump=0
  integer nx, ny, nz, iz, isub
  real :: drtdt
  character(len=mid):: id="$Id: gottlieb_shu.f90,v 1.3 2013/08/02 12:35:48 aake Exp $"

  call print_id (id)

!------------------------------------------------------------------------
!  Loop over three Runge-Kutta stages
!------------------------------------------------------------------------
  if (do_density) then
    rold(:,:,:) =  r(:,:,:)
  endif
  if (do_momenta) then
    pxold(:,:,:) = px(:,:,:)
    pyold(:,:,:) = py(:,:,:)
    pzold(:,:,:) = pz(:,:,:)
  endif
  if (do_energy) then
    eold(:,:,:) =  e(:,:,:)
  endif
  if (do_mhd) then
    Bxold(:,:,:) = Bx(:,:,:)
    Byold(:,:,:) = By(:,:,:)
    Bzold(:,:,:) = Bz(:,:,:)
  endif
  if (do_pscalar) then
    dold(:,:,:) =  d(:,:,:)
  endif

! This advances the time to t+dt

  isubstep = 1
  call pde(.true.)
  call courant
  if (do_density) then
     r(:,:,:) =          rold(:,:,:)                    +         dt* drdt(:,:,:)
  endif
  if (do_momenta) then
    px(:,:,:) =         pxold(:,:,:)                    +         dt*dpxdt(:,:,:)
    py(:,:,:) =         pyold(:,:,:)                    +         dt*dpydt(:,:,:)
    pz(:,:,:) =         pzold(:,:,:)                    +         dt*dpzdt(:,:,:)
  endif
  if (do_energy) then
     e(:,:,:) =          eold(:,:,:)                    +         dt* dedt(:,:,:)
  endif
  if (do_mhd) then
    Bx(:,:,:) =         Bxold(:,:,:)                    +         dt*dBxdt(:,:,:)
    By(:,:,:) =         Byold(:,:,:)                    +         dt*dBydt(:,:,:)
    Bz(:,:,:) =         Bzold(:,:,:)                    +         dt*dBzdt(:,:,:)
  endif
  if (do_pscalar) then
     d(:,:,:) =          dold(:,:,:)                    +         dt* dddt(:,:,:)
  endif

! This advances the time to t+0.5*dt

  isubstep = 2
  call pde(.false.)
  if (do_density) then
     r(:,:,:) = (3./4.)* rold(:,:,:) + (1./4.)* r(:,:,:) + (1./4.)*dt* drdt(:,:,:)
  endif
  if (do_momenta) then
    px(:,:,:) = (3./4.)*pxold(:,:,:) + (1./4.)*px(:,:,:) + (1./4.)*dt*dpxdt(:,:,:)
    py(:,:,:) = (3./4.)*pyold(:,:,:) + (1./4.)*py(:,:,:) + (1./4.)*dt*dpydt(:,:,:)
    pz(:,:,:) = (3./4.)*pzold(:,:,:) + (1./4.)*pz(:,:,:) + (1./4.)*dt*dpzdt(:,:,:)
  endif
  if (do_energy) then
     e(:,:,:) = (3./4.)* eold(:,:,:) + (1./4.)* e(:,:,:) + (1./4.)*dt* dedt(:,:,:)
  endif
  if (do_mhd) then
    Bx(:,:,:) = (3./4.)*Bxold(:,:,:) + (1./4.)*Bx(:,:,:) + (1./4.)*dt*dBxdt(:,:,:)
    By(:,:,:) = (3./4.)*Byold(:,:,:) + (1./4.)*By(:,:,:) + (1./4.)*dt*dBydt(:,:,:)
    Bz(:,:,:) = (3./4.)*Bzold(:,:,:) + (1./4.)*Bz(:,:,:) + (1./4.)*dt*dBzdt(:,:,:)
  endif
  if (do_pscalar) then
     d(:,:,:) = (3./4.)* dold(:,:,:) + (1./4.)* d(:,:,:)+ (1./4.)*dt* dddt(:,:,:)
  endif

! This advances the time to t+dt

  isubstep = 3
  call pde(.false.)
  if (do_density) then
     r(:,:,:) = (1./3.)* rold(:,:,:) + (2./3.)* r(:,:,:) + (2./3.)*dt* drdt(:,:,:)
  endif
  if (do_momenta) then
    px(:,:,:) = (1./3.)*pxold(:,:,:) + (2./3.)*px(:,:,:) + (2./3.)*dt*dpxdt(:,:,:)
    py(:,:,:) = (1./3.)*pyold(:,:,:) + (2./3.)*py(:,:,:) + (2./3.)*dt*dpydt(:,:,:)
    pz(:,:,:) = (1./3.)*pzold(:,:,:) + (2./3.)*pz(:,:,:) + (2./3.)*dt*dpzdt(:,:,:)
  endif
  if (do_energy) then
     e(:,:,:) = (1./3.)* eold(:,:,:) + (2./3.)* e(:,:,:) + (2./3.)*dt* dedt(:,:,:)
  endif
  if (do_mhd) then
    Bx(:,:,:) = (1./3.)*Bxold(:,:,:) + (2./3.)*Bx(:,:,:) + (2./3.)*dt*dBxdt(:,:,:)
    By(:,:,:) = (1./3.)*Byold(:,:,:) + (2./3.)*By(:,:,:) + (2./3.)*dt*dBydt(:,:,:)
    Bz(:,:,:) = (1./3.)*Bzold(:,:,:) + (2./3.)*Bz(:,:,:) + (2./3.)*dt*dBzdt(:,:,:)
  endif
  if (do_pscalar) then
     d(:,:,:) = (1./3.)* dold(:,:,:) + (2./3.)* d(:,:,:)+ (2./3.)*dt* dddt(:,:,:)
  endif

  if (idbg > 0) then
                   call stats('r' ,r)
    if (do_momenta) then
                   call stats('px',px)
                   call stats('py',py)
                   call stats('pz',pz)
    end if
    if (do_energy) call stats('e' ,e)
    if (do_mhd) then
                   call stats('Bx',Bx)
                   call stats('By',By)
                   call stats('Bz',Bz)
    end if
    if (do_pscalar)call stats('d' ,d)
  end if

  if (dtold == dt) then                                                 ! courant normally sets dtold=dt ..
    td = td + dt                                                        ! for a normal time update ..
  else                                                                  ! except for reduced time steps ...
    td = tsnap*(isnap+isnap0)                                           ! that should result in exact save times
    if (master) print *,'setting exact save time'
  end if
  t = td
  rt= td                                                                ! rt is the "real time" computed also in substeps.
END SUBROUTINE
