! $Id: courant_omp.f90,v 1.8 2008/09/06 19:41:02 aake Exp $
!-----------------------------------------------------------------------
SUBROUTINE courant
  USE params
  USE variables, ONLY: r, e, d, drdt, dedt, dddt
  implicit none
  logical omp_in_parallel
!
  if (omp_in_parallel()) then
    call courant_omp (r, e, d, drdt, dedt, dddt)
  else
    !$omp parallel
    call courant_omp (r, e, d, drdt, dedt, dddt)
    !$omp end parallel
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE courant_omp (r, e, d, drdt, dedt, dddt)
  USE params
  implicit none
  real, dimension(mx,my,mz) :: r, e, d, drdt, dedt, dddt
  integer iz
  real fmaxval, Cp
  character(len=mid) id
  data id/'$Id: courant_omp.f90,v 1.8 2008/09/06 19:41:02 aake Exp $'/

  call print_id (id)

!-----------------------------------------------------------------------
!  Courant conditions
!-----------------------------------------------------------------------

  call barrier_omp('courant1')
!$omp single
  Cr = 0.
  Ce = 0.
  if (.not. do_momenta) then
    Cb = 0.
    Cv = 0
    Cn = 0.
  end if
  if (.not. do_mhd) then
    Cb = 0.
  end if
!$omp end single

  if (do_density) then
    do iz=izs,ize
      scratch(:,lb:ub,iz) = dt*abs(drdt(:,lb:ub,iz)/r(:,lb:ub,iz))
    end do
    call fmaxval_subr ('Cr',scratch,Cr)
  end if

  if (do_energy) then
    do iz=izs,ize
      scratch(:,lb:ub,iz) = dt*abs(dedt(:,lb:ub,iz)/e(:,lb:ub,iz))
    end do
    call fmaxval_subr ('Ce',scratch,Ce)
  end if

  if (do_pscalar) then
    do iz=izs,ize
      scratch(:,lb:ub,iz) = dt*abs(dddt(:,lb:ub,iz)/d(:,lb:ub,iz))
    end do
    Cp = Cr
    call fmaxval_subr ('Cd',scratch,Cr)
    !$omp single
    Cr = max(Cp,Cr)
    !$omp end single
  end if

!$omp single
  if (Cdt.gt.0.) dt = dt*min(Cdt/max(Cu,Cb,1e-30),Cdtr/max(Cr,Ce,1e-30),Cdtd/max(Cv,Cn,1e-30))

  if (it > 2 .and. tsnap > 0. .and. t+dt >= tsnap*(isnap+isnap0)) then          ! if t+dt is beyond save time
    dtold = dt                                                                  ! remember the dt
    dt = max(dt*0.001, tsnap*(isnap+isnap0)-t)                                  ! aim for t+dt == tsave
    do while (t+dt < tsnap*(isnap+isnap0))                                      ! round off check
      dt = dt + dtold*0.001                                                     ! add small increments
    end do                                                                      ! until criterion is OK
  else
    dt = min(dt, 2.*dtold)
    dtold = dt
  endif
!$omp end single

END
