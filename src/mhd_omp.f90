! $Id: mhd_omp.f90,v 1.41 2010/08/19 20:13:58 aake Exp $

!***********************************************************************
SUBROUTINE mhd (flag, &
            r,e,pg,Ux,Uy,Uz,Bx,By,Bz,Ex,Ey,Ez, &
	    dpxdt,dpydt,dpzdt,dedt,dBxdt,dBydt,dBzdt)
  USE params
  USE arrays, ONLY: &
            wk06,wk10,wk11,wk12, &
            wk13,wk14,wk15,wk16,wk17,wk18,wk19,wk20, &
            scr1,scr2,scr3,scr4,scr5,scr6
  implicit none
  logical flag
  real, dimension(mx,my,mz):: &
            r,e,pg,Ux,Uy,Uz,Bx,By,Bz,Ex,Ey,Ez, &
	    dpxdt,dpydt,dpzdt,dedt,dBxdt,dBydt,dBzdt

  call mhd1(flag, &
            r,e,pg,Ux,Uy,Uz,Bx,By,Bz,Ex,Ey,Ez, &
	    dpxdt,dpydt,dpzdt,dedt,dBxdt,dBydt,dBzdt, &
            wk18,wk19,wk20,wk10, &
            wk11,wk12,wk13,wk14,wk15,wk16,wk06,wk17, &
            scr1,scr2,scr3,scr4,scr5,scr6)
END

!ptr  eta => wk16; fudge => wk17

!ptr  Jx => wk18; Jy => wk19 Jz => wk20
!ptr  Bx_y => wk10; By_z => wk11; Bz_x => wk12
!ptr  Bx_z => wk13; By_x => wk14; Bz_y => wk15
!ptr  Ux_y => scr1; Uy_z => scr2; Uz_x => scr3
!ptr  Ux_z => scr4; Uy_x => scr5; Uz_y => scr6

!***********************************************************************
SUBROUTINE mhd1(flag, &
                r,e,pg,Ux,Uy,Uz,Bx,By,Bz,Ex,Ey,Ez, &
	        dpxdt,dpydt,dpzdt,dedt,dBxdt,dBydt,dBzdt, &
                Jx  ,Jy  ,Jz  ,wk10, &
                wk11,wk12,wk13,wk14,wk15,eta,etd,fudge, &
                scr1,scr2,scr3,scr4,scr5,scr6)
  USE params
  implicit none
  real, dimension(mx,my,mz):: &
    r,e,pg,Ux,Uy,Uz,Bx,By,Bz,dpxdt,dpydt,dpzdt,dedt, &
    dBxdt,dBydt,dBzdt, &
    Jx,Jy,Jz,Ex,Ey,Ez,wk10, &
    wk11,wk12,wk13,wk14,wk15,eta,etd,fudge, &
    scr1,scr2,scr3,scr4,scr5,scr6

  logical flag
  integer ix, iy, iz, j, imhd, lb1, ub1
  real:: elapsed, tm(30)
  real c, Qsum, ds
  real(kind=8) sumy

  logical debug
  real cput(2), void, dtime

  character(len=mid) id
  data id/'$Id: mhd_omp.f90,v 1.41 2010/08/19 20:13:58 aake Exp $'/

  call print_id (id)
  call print_trace (id, dbg_mhd, 'BEGIN')

                       if (debug(dbg_mhd) .and. omp_master) then; void = dtime(cput); print '(1x,a8,2f7.3)','mhd0   ',cput; endif
  imhd=0

!-----------------------------------------------------------------------

  call print_trace (id, dbg_mhd, 'bef. resist')
  call resist (flag, &                                                  ! uses wk07-wk15 internally
    eta,r,e,pg,fudge,Ux,Uy,Uz,Bx,By,Bz)
  call print_trace (id, dbg_mhd, 'aft. resist')
    call dumpn(eta,'eta','mhd.dmp',0)

                       if (debug(dbg_mhd) .and. omp_master) then; void = dtime(cput); print '(1x,a8,2f7.3)','resist ',cput; endif

!-----------------------------------------------------------------------
!  Electric current J = curl(B), resistive E, and dissipation
!-----------------------------------------------------------------------
  call mfield_boundary (Ux,Uy,Uz,Bx,By,Bz)

                       if (debug(dbg_mhd) .and. omp_master) then; void = dtime(cput); print '(1x,a8,2f7.3)','Bbdry  ',cput; endif

  call barrier_omp('mhd1')                                              ! barrier 1; By may have been modified
    call dumpn(Bx,'Bx','mhd.dmp',1)
    call dumpn(By,'By','mhd.dmp',1)
    call dumpn(Bz,'Bz','mhd.dmp',1)

  call ddydn_set (Bz,Jx)
  call ddzdn_sub (By,Jx)
  call ddzdn_set (Bx,Jy)
  call ddxdn_sub (Bz,Jy)
  call ddxdn_set (By,Jz)
  call ddydn_sub (Bx,Jz)
    call dumpn(Jx,'Jx','mhd.dmp',1)
    call dumpn(Jy,'Jy','mhd.dmp',1)
    call dumpn(Jz,'Jz','mhd.dmp',1)

                       if (debug(dbg_mhd) .and. omp_master) then; void = dtime(cput); print '(1x,a8,2f7.3)','current',cput; endif

!-----------------------------------------------------------------------
!  Resistive part of Ex
!-----------------------------------------------------------------------
  call barrier_omp('mhd2')                                              ! barrier 2;  NOT NEEDED?
  call zdn1_set (eta, scr1) ; call ydn1_set (scr1, scr2)
  call zdn1_set (etd, scr3) ; call ydn1_set (scr3, scr4)

  call ddydn_set (Bz,scr5)
  if (do_quench) call quenchy1 (scr5)
  do iz=izs,ize; do iy=1,my
    ds = 0.5*(dym(iy)+dzm(iz))
    if (do_aniso_eta) ds = dym(iy)
    do ix=1,mx
      Ex(ix,iy,iz) = ds*(scr2(ix,iy,iz)+ds*scr4(ix,iy,iz))*scr5(ix,iy,iz)
    end do
  end do; end do

  call ddzdn_set (By,scr5)
  if (do_quench) call quenchz1 (scr5)
  do iz=izs,ize; do iy=1,my
    ds = 0.5*(dym(iy)+dzm(iz))
    if (do_aniso_eta) ds = dzm(iz)
    do ix=1,mx
      Ex(ix,iy,iz) = Ex(ix,iy,iz) - ds*(scr2(ix,iy,iz)+ds*scr4(ix,iy,iz))*scr5(ix,iy,iz)
    end do
  end do; end do

!-----------------------------------------------------------------------
!  Resistive part of Ey
!-----------------------------------------------------------------------
  call xdn1_set (scr1, scr2)
  call xdn1_set (scr3, scr4)

  call ddzdn_set (Bx,scr5)
  if (do_quench) call quenchz1 (scr5)
  if (do_aniso_eta) then
    do iz=izs,ize; do iy=1,my; do ix=1,mx
      ds = dzm(iz)
      Ey(ix,iy,iz) = ds*(scr2(ix,iy,iz)+ds*scr4(ix,iy,iz))*scr5(ix,iy,iz)
    end do; end do; end do
  else
    do iz=izs,ize; do iy=1,my; do ix=1,mx
      ds = 0.5*(dzm(iz)+dxm(ix))
      Ey(ix,iy,iz) = ds*(scr2(ix,iy,iz)+ds*scr4(ix,iy,iz))*scr5(ix,iy,iz)
    end do; end do; end do
  end if

  call ddxdn_set (Bz,scr5)
  if (do_quench) call quenchx1 (scr5)
  if (do_aniso_eta) then
    do iz=izs,ize; do iy=1,my; do ix=1,mx
      ds = dxm(ix)
      Ey(ix,iy,iz) = Ey(ix,iy,iz) - ds*(scr2(ix,iy,iz)+ds*scr4(ix,iy,iz))*scr5(ix,iy,iz)
    end do; end do; end do
  else
    do iz=izs,ize; do iy=1,my; do ix=1,mx
      ds = 0.5*(dzm(iz)+dxm(ix))
      Ey(ix,iy,iz) = Ey(ix,iy,iz) - ds*(scr2(ix,iy,iz)+ds*scr4(ix,iy,iz))*scr5(ix,iy,iz)
    end do; end do; end do
  end if

!-----------------------------------------------------------------------
!  Resistive part of Ez
!-----------------------------------------------------------------------
  call ydn1_set (eta, scr1) ; call xdn1_set (scr1, scr2)
  call ydn1_set (etd, scr3) ; call xdn1_set (scr3, scr4)

  call ddxdn_set (By,scr5)
  if (do_quench) call quenchx1 (scr5)
  if (do_aniso_eta) then
    do iz=izs,ize; do iy=1,my; do ix=1,mx
      ds = dxm(ix)
      Ez(ix,iy,iz) = ds*(scr2(ix,iy,iz)+ds*scr4(ix,iy,iz))*scr5(ix,iy,iz)
    end do; end do; end do
  else
    do iz=izs,ize; do iy=1,my; do ix=1,mx
      ds = 0.5*(dxm(ix)+dym(iy))
      Ez(ix,iy,iz) = ds*(scr2(ix,iy,iz)+ds*scr4(ix,iy,iz))*scr5(ix,iy,iz)
    end do; end do; end do
  end if

  call ddydn_set (Bx,scr5)
  if (do_quench) call quenchy1 (scr5)
  if (do_aniso_eta) then
    do iz=izs,ize; do iy=1,my; do ix=1,mx
      ds = dym(iy)
      Ez(ix,iy,iz) = Ez(ix,iy,iz) - ds*(scr2(ix,iy,iz)+ds*scr4(ix,iy,iz))*scr5(ix,iy,iz)
    end do; end do; end do
  else
    do iz=izs,ize; do iy=1,my; do ix=1,mx
      ds = 0.5*(dxm(ix)+dym(iy))
      Ez(ix,iy,iz) = Ez(ix,iy,iz) - ds*(scr2(ix,iy,iz)+ds*scr4(ix,iy,iz))*scr5(ix,iy,iz)
    end do; end do; end do
  end if

!-----------------------------------------------------------------------
!  Alfven speed limiter / fudge factor
!-----------------------------------------------------------------------
  if (cmax .ne. 0) then
    call ydn1_set(fudge,scratch); call zdn1_set(scratch,scr1)
    call zdn1_set(fudge,scratch); call xdn1_set(scratch,scr2)
    call xdn1_set(fudge,scratch); call ydn1_set(scratch,scr3)
    do iz=izs,ize
      Jx(:,:,iz) = Jx(:,:,iz)*scr1(:,:,iz)
      Jy(:,:,iz) = Jy(:,:,iz)*scr2(:,:,iz)
      Jz(:,:,iz) = Jz(:,:,iz)*scr3(:,:,iz)
      Ex(:,:,iz) = Ex(:,:,iz)*scr1(:,:,iz)
      Ey(:,:,iz) = Ey(:,:,iz)*scr2(:,:,iz)
      Ez(:,:,iz) = Ez(:,:,iz)*scr3(:,:,iz)
    end do
  end if
  call print_trace (id, dbg_mhd, 'J')

    call dumpn(Ex,'Ex','mhd.dmp',1)
    call dumpn(Ey,'Ey','mhd.dmp',1)
    call dumpn(Ez,'Ez','mhd.dmp',1)
  call ecurrent_boundary (Ex, Ey, Ez, Jx, Jy, Jz)
 
!-----------------------------------------------------------------------
!  Joule dissipation
!-----------------------------------------------------------------------
  if (do_energy .and. do_dissipation) then
    do iz=izs,ize
      scr3(:,:,iz) = Jx(:,:,iz)*Ex(:,:,iz)
    end do
    call barrier_omp('mhd5')                                            ! barrier 5; scr3 just computed
    call zup1_set (scr3, scr1) ; call yup1_set (scr1 , scr2)

    lb1 = lb
    ub1 = ub
    if (isubstep == 1) then
      c = 1./(mx*(ub1-lb1+1))
      do iz=izs,ize
        sumy = 0.
        do iy=lb1,ub1
          sumy = sumy + sum(dble(scr2(:,iy,iz)))
        end do
        sumxy(iz) = sumy*c
      end do
      call dumpn(scr2,'Qj1','mhd.dmp',1)
    end if

    do iz=izs,ize
      dedt(:,lb1:ub1,iz) = dedt(:,lb1:ub1,iz) + scr2(:,lb1:ub1,iz)
      scr2(:,:,iz) = Jy(:,:,iz)*Ey(:,:,iz)
    end do
    call xup1_set (scr2, scr1)
    call barrier_omp('mhd6')                                            ! barrier 6; make sure scr3 is not changed
    call zup1_set (scr1, scr3)

    if (isubstep == 1) then
      do iz=izs,ize
        sumy = 0.
        do iy=lb1,ub1
          sumy = sumy + sum(dble(scr3(:,iy,iz)))
        end do
        sumxy(iz) = sumxy(iz) + sumy*c
      end do
      call dumpn(scr3,'Qj2','mhd.dmp',1)
    end if

    do iz=izs,ize
      dedt(:,lb1:ub1,iz) = dedt(:,lb1:ub1,iz) + scr3(:,lb1:ub1,iz)
      scr3(:,:,iz) = Jz(:,:,iz)*Ez(:,:,iz)
    end do
    call barrier_omp('mhd6a')                                           ! barrier 6a; make sure scr3 is not changed
    call yup1_set (scr3, scr1) ; call xup1_set (scr1 , scr2)
    do iz=izs,ize
      dedt(:,lb1:ub1,iz) = dedt(:,lb1:ub1,iz) + scr2(:,lb1:ub1,iz)
    end do

    if (isubstep == 1) then
      do iz=izs,ize
        sumy = 0.
        do iy=lb1,ub1
          sumy = sumy + sum(dble(scr2(:,iy,iz)))
        end do
        sumxy(iz) = sumxy(iz) + sumy*c
      end do
      call dumpn(scr3,'Qj3','mhd.dmp',1)
      !$omp barrier
      !$omp master
      Qsum = sum(sumxy)/mz
      call sum_mpi_real (Qsum, Q_mag, 1)
      Q_mag = Q_mag/(mpi_nx*mpi_ny*mpi_nz)
      !$omp end master
    end if
  end if
    call dumpn(Ex,'Ex1','mhd.dmp',1)
    call dumpn(Ey,'Ey1','mhd.dmp',1)
    call dumpn(Ez,'Ez1','mhd.dmp',1)
    
!-----------------------------------------------------------------------
!  Lorentz force = I x B
!-----------------------------------------------------------------------
  call ydn_set (Bx,wk10) ; call zdn_set (Bx,wk13)
  call zdn_set (By,wk11) ; call xdn_set (By,wk14)
  call xdn_set (Bz,wk12) ; call ydn_set (Bz,wk15)
  do iz=izs,ize
    scr1(:,:,iz) = Jy(:,:,iz)*wk12(:,:,iz)
  end do
  call barrier_omp('mhd7')                                              ! barrier 7; scr1 just computed
  call dumpn(scr1,'Fx','mhd.dmp',1)
  call zup_add (scr1, dpxdt)
  do iz=izs,ize
    scr2(:,:,iz) = Jz(:,:,iz)*wk14(:,:,iz)
  end do
  call dumpn(scr2,'Fx','mhd.dmp',1)
  call yup_sub (scr2, dpxdt)
  do iz=izs,ize
    scr2(:,:,iz) = Jz(:,:,iz)*wk10(:,:,iz)
  end do
  call dumpn(scr2,'Fy','mhd.dmp',1)
  call xup_add (scr2, dpydt)
  do iz=izs,ize
    scr3(:,:,iz) = Jx(:,:,iz)*wk15(:,:,iz)
  end do
  call barrier_omp('mhd8')                                              ! barrier 8; scr3 just computed
  call dumpn(scr3,'Fy','mhd.dmp',1)
  call zup_sub (scr3, dpydt)
  do iz=izs,ize
    scr1(:,:,iz) = Jx(:,:,iz)*wk11(:,:,iz)
  end do
  call dumpn(scr1,'Fz','mhd.dmp',1)
  call yup_add (scr1, dpzdt)
  do iz=izs,ize
    scr1(:,:,iz) = Jy(:,:,iz)*wk13(:,:,iz)
  end do
  call dumpn(scr1,'Fz','mhd.dmp',1)
  call xup_sub (scr1, dpzdt)
  call barrier_omp('mhd9')                                              ! barrier 9; NOT NEEDED??
  call print_trace (id, dbg_mhd, 'Lorentz')

  call ecurrent_boundary (Ex, Ey, Ez, Jx, Jy, Jz)

                       if (debug(dbg_mhd) .and. omp_master) then; void = dtime(cput); print '(1x,a8,2f7.3)','Lforce ',cput; endif

!-----------------------------------------------------------------------
!  Electric field   E =  eta I - uxB
!-----------------------------------------------------------------------

  call zdn_set (Uy,scr2) ; call ydn_set (Uz,scr6)
  do iz=izs,ize
    Ex(:,:,iz) = Ex(:,:,iz) - scr2(:,:,iz)*wk15(:,:,iz) + scr6(:,:,iz)*wk11(:,:,iz)
  end do
  call xdn_set (Uz,scr3) ; call zdn_set (Ux,scr4)
  do iz=izs,ize
    Ey(:,:,iz) = Ey(:,:,iz) - scr3(:,:,iz)*wk13(:,:,iz) + scr4(:,:,iz)*wk12(:,:,iz)
  end do
  call ydn_set (Ux,scr1) ; call xdn_set (Uy,scr5)
  do iz=izs,ize
    Ez(:,:,iz) = Ez(:,:,iz) - scr1(:,:,iz)*wk14(:,:,iz) + scr5(:,:,iz)*wk10(:,:,iz)
  end do
  call print_trace (id, dbg_mhd, 'E')
    call dumpn(Ex,'Ex2','mhd.dmp',1)
    call dumpn(Ey,'Ey2','mhd.dmp',1)
    call dumpn(Ez,'Ez2','mhd.dmp',1)

                       if (debug(dbg_mhd) .and. omp_master) then; void = dtime(cput); print '(1x,a8,2f7.3)','Eadvect',cput; endif

  call efield_boundary (Ex, Ey, Ez, wk10, wk13, wk11, wk14, wk12, wk15, &
                        Bx, By, Bz, scr1, scr4, scr2, scr5, scr3, scr6, &
                        Ux, Uy, Uz)

                       if (debug(dbg_mhd) .and. omp_master) then; void = dtime(cput); print '(1x,a8,2f7.3)','Ebdry  ',cput; endif

!-----------------------------------------------------------------------
!  Magnetic field's time derivative, dBdt = - curl(E)
!-----------------------------------------------------------------------
  call barrier_omp('mhd10')                                             ! barrier 10; Ex, Ey just computed
  call print_trace (id, dbg_mhd, 'Ebdry')
    call dumpn(Ex,'Ex3','mhd.dmp',1)
    call dumpn(Ey,'Ey3','mhd.dmp',1)
    call dumpn(Ez,'Ez3','mhd.dmp',1)
  if (do_2nddiv) then
    call ddzup1_add (Ey, dBxdt) ; call ddyup1_sub (Ez, dBxdt)
    call ddxup1_add (Ez, dBydt) ; call ddzup1_sub (Ex, dBydt)
    call ddyup1_add (Ex, dBzdt) ; call ddxup1_sub (Ey, dBzdt)
  else
    call ddzup_add (Ey, dBxdt)  ; call ddyup_sub (Ez, dBxdt)
    call ddxup_add (Ez, dBydt)  ; call ddzup_sub (Ex, dBydt)
    call ddyup_add (Ex, dBzdt)  ; call ddxup_sub (Ey, dBzdt)
  endif

!-----------------------------------------------------------------------
!  Fictive magnetic field growth, e.g. to slowly increase AR fields
!-----------------------------------------------------------------------
  if (t_Bgrowth.ne.0.) then
    do iz=izs,ize
      dBxdt(:,:,iz) = dBxdt(:,:,iz) + (1./t_Bgrowth)*Bx(:,:,iz)
      dBydt(:,:,iz) = dBydt(:,:,iz) + (1./t_Bgrowth)*By(:,:,iz)
      dBzdt(:,:,iz) = dBzdt(:,:,iz) + (1./t_Bgrowth)*Bz(:,:,iz)
    end do
  end if
  call print_trace (id, dbg_mhd, 'dBdt')
    call dumpn(dBxdt,'dBxdt','mhd.dmp',1)
    call dumpn(dBydt,'dBydt','mhd.dmp',1)
    call dumpn(dBzdt,'dBzdt','mhd.dmp',1)

                       if (debug(dbg_mhd) .and. omp_master) then; void = dtime(cput); print '(1x,a8,2f7.3)','induct',cput; endif

  call print_trace (id, dbg_mhd, 'END')
  END
!**********************************************************************
