!***********************************************************************
MODULE pde_mod
  integer n_d2lnr
  logical do_fluxes
  real, allocatable, dimension(:,:,:):: qbdry
END MODULE

!***********************************************************************
SUBROUTINE init_pde
  USE params
  USE pde_mod
  allocate (qbdry(mx,2*(my-ub),mz))
! call init_arrays
! call init_work
END SUBROUTINE

!***********************************************************************
SUBROUTINE pde (flag)
!-----------------------------------------------------------------------
!  To omptimize for speed, all pointer assignments and extra allocations
!  are done in this first wrapper routines, which then passes all
!  arrays on in argument lists.  To make it easier for compilers to
!  optimize, the procedure is split into smaller parts.
!-----------------------------------------------------------------------
  USE params
  USE variables
  USE arrays
  USE pde_mod
  implicit none
  logical flag, foutput
  real d2lnr_max
  real, pointer, dimension(:,:,:):: S2
  real, dimension(my):: frho

  do_fluxes = foutput('fluxes.txt')

!-----------------------------------------------------------------------
!  Pointer assignments to work arrays.  Note that there is some overlap,
!  so any changes should be 1) reviewed very carefully, and 2) tested
!  with the do_dump/do_compare mechanism
!-----------------------------------------------------------------------
  !dd  => scr4
  !lne => scr5
  !xdnl => wk01; ydnl => wk02; zdnl => wk03
  !xdnr => wk16; ydnr => wk17; zdnr => wk18
  !lnu  => wk20
  !S2   => wk09
  !p    => wk04; ee   => wk05; nu   => wk06
  !Cs   => wk07; lnr  => wk08; nud  => wk19
  !Sxy  => wk10; Syz  => wk11; Szx  => wk12
  !Sxx  => wk13; Syy  => wk14; Szz  => wk15
  !Txy  => wk13; Tyz  => wk14; Tzx  => wk15
  !Txx  => wk07; Tyy  => wk08; Tzz  => wk09

  call pde1 (flag, mx, my, mz, &
                 r, px, py, pz, e, d, Bx, By, Bz, &
                 drdt, dpxdt, dpydt, dpzdt, dedt, &
                 dddt, dBxdt, dBydt, dBzdt, &
                 wk01, wk02, wk03, wk06, wk20, wk08, wk19, &
                 wk16, wk17, wk18, &
                 Ux, Uy, Uz, wk09, wk04, wk05, wk22, wk23, wk07, &
                 wk10, wk11, wk12, wk16, wk17, wk18, &
                 wk13, wk14, wk15, wk07, wk08, wk09, wk21, &
                 scr1, scr2, scr3, scr4, scr5, scr6, d2lnr_max,frho)
  call pde2 (flag, mx, my, mz, &
                 r, px, py, pz, e, d, Bx, By, Bz, &
                 drdt, dpxdt, dpydt, dpzdt, dedt, &
                 dddt, dBxdt, dBydt, dBzdt, &
                 wk01, wk02, wk03, wk06, wk20, wk08, wk19, &
                 Ux, Uy, Uz, wk09, wk04, wk05, wk22, wk23, wk07, &
                 wk10, wk11, wk12, wk16, wk17, wk18, &
                 wk13, wk14, wk15, wk07, wk08, wk09, wk21, &
                 scr1, scr2, scr3, scr4, scr5, scr6, d2lnr_max,frho)
  call pde3 (flag, mx, my, mz, &
                 r, px, py, pz, e, d, Bx, By, Bz, Ex, Ey, Ez, &
                 drdt, dpxdt, dpydt, dpzdt, dedt, &
                 dddt, dBxdt, dBydt, dBzdt, &
                 wk01, wk02, wk03, wk06, wk20, wk08, wk19, &
                 Ux, Uy, Uz, wk09, wk04, wk05, wk22, wk23, wk07, &
                 wk10, wk11, wk12, wk16, wk17, wk18, &
                 wk13, wk14, wk15, wk07, wk08, wk09, wk21, &
                 scr1, scr2, scr3, scr4, scr5, scr6, d2lnr_max,frho)

END
!***********************************************************************
SUBROUTINE pde1 (flag, mx, my, mz, &
                 r, px, py, pz, e, d, Bx, By, Bz, &
                 drdt, dpxdt, dpydt, dpzdt, dedt, &
                 dddt, dBxdt, dBydt, dBzdt, &
                 xdnl, ydnl, zdnl, nu, lnu, lnr, nud, &
                 xdnr, ydnr, zdnr, &
                 Ux, Uy, Uz, S2, p, ee, lne, dd, Cs, &
                 Sxy, Syz, Szx, Sxx, Syy, Szz, &
                 Txy, Tyz, Tzx, Txx, Tyy, Tzz, d2lnr, &
                 scr1, scr2, scr3, scr4, scr5, scr6, d2lnr_max,frho)
  USE params, mx_void=>mx, my_void=>my, mz_void=>mz
  USE quench_m, only: qlim
  USE arrays, only: eeav, pav
  USE forcing, only: rav
  USE pde_mod
  implicit none
  logical flag
  integer mx, my, mz
  real, dimension(mx,my,mz):: &
                 r, px, py, pz, e, d, Bx, By, Bz, &
                 drdt, dpxdt, dpydt, dpzdt, dedt, &
                 dddt, dBxdt, dBydt, dBzdt, &
                 xdnl, ydnl, zdnl, nu, lnu, lnr, nud, &
                 xdnr, ydnr, zdnr, &
                 Ux, Uy, Uz, S2, p, ee, lne, dd, Cs, &
                 Sxy, Syz, Szx, Sxx, Syy, Szz, &
                 Txy, Tyz, Tzx, Txx, Tyy, Tzz, d2lnr, &
                 scr1, scr2, scr3, scr4, scr5, scr6
  integer iz, iy, ix, n_thread
  real gam1, average, fCv, wavesp, velocity, sumy, sumz
  real d2lnr_max
  real, dimension(mx):: exp1
  real, dimension(my):: frho
  character (len=mid):: id = '$Id: pde_bob.f90 $'
!-----------------------------------------------------------------------
  call print_id(id)
                                                                        call timer('pde','begin')

!-----------------------------------------------------------------------
!  The parameter 'gamma' is used in cases with polytropic equations of
!  state.  In other cases it is only used in the estimate of the sound
!  speed (which does not need to be accurate)
!-----------------------------------------------------------------------
  if (gamma .eq. 1.) then
    gam1 = 1.
  else
    gam1 = gamma-1.
  end if

!-----------------------------------------------------------------------
!  Viscosity Courant condition factors:
!    qlim : max factor from the quenching operator (=1 when do_quench=f)
!     6.2 : the maximum value returned from ddxup(ddxdn(f)) when dx=1.
!      3. : for the worst case of a 3-D checker board
!      2. : normalize so Ctd=1. when we reach 1x overshoot
!-----------------------------------------------------------------------
  if (do_2nddiv) then
    fCv = qlim*4.97*3.*dt/2.*max(1.,nuE)
  else
    fCv = qlim*6.17*3.*dt/2.*max(1.,nuE)
  end if

!-----------------------------------------------------------------------
!  Velocities, pressure
!-----------------------------------------------------------------------
  do iz=izs,ize
    lnr(:,:,iz) = alog(amax1(r(:,:,iz),1e-30))
  end do
    call dumpn(r ,'r' ,'r.dmp',1)
  call density_boundary(r,lnr,py,e)
    call dumpn(r ,'r' ,'r.dmp',1)

                                                                        call timer('pde','dens_bdry')

  if (do_2nddiv) then
    call xdn1_set (lnr, xdnl)
    call ydn1_set (lnr, ydnl)
    call barrier_omp('pde1')                                              ! barrier 1, needed because lnr just computed
    call zdn1_set (lnr, zdnl)
  else
    call xdn_set (lnr, xdnl)
    call ydn_set (lnr, ydnl)
    call barrier_omp('pde1')                                              ! barrier 1, needed because lnr just computed
    call zdn_set (lnr, zdnl)
  end if
  do iz=izs,ize
  do iy=1,my
    call expn (mx, xdnl(:,iy,iz), xdnr(:,iy,iz))
    Ux(:,iy,iz) = px(:,iy,iz)/xdnr(:,iy,iz)

    call expn (mx, ydnl(:,iy,iz), ydnr(:,iy,iz))
    Uy(:,iy,iz) = py(:,iy,iz)/ydnr(:,iy,iz)

    call expn (mx, zdnl(:,iy,iz), zdnr(:,iy,iz))
    Uz(:,iy,iz) = pz(:,iy,iz)/zdnr(:,iy,iz)
  end do
  end do
    call dumpn(Ux,'Ux','px.dmp',1)
    call dumpn(px,'px','px.dmp',1)
    call dumpn(xdnr,'xdnr','px.dmp',1)
    call dumpn(Uy,'Uy','py.dmp',1)
    call dumpn(py,'py','py.dmp',1)
    call dumpn(ydnr,'ydnr','py.dmp',1)
    call dumpn(Uz,'Uz','pz.dmp',1)
    call dumpn(pz,'pz','pz.dmp',1)
    call dumpn(zdnr,'zdnr','pz.dmp',1)
                                                                        call timer('pde','velocities')
  call velocity_boundary(r,xdnr,ydnr,zdnr,Ux,Uy,Uz,px,py,pz,e,Bx,By,Bz)
    call dumpn(Ux,'Ux>vbdry','px.dmp',1)
    call dumpn(px,'px','px.dmp',1)
    call dumpn(xdnr,'xdnr','px.dmp',1)
    call dumpn(Uy,'Uy>vbdry','py.dmp',1)
    call dumpn(py,'py','py.dmp',1)
    call dumpn(ydnr,'ydnr','py.dmp',1)
    call dumpn(Uz,'Uz>vbdry','pz.dmp',1)
    call dumpn(pz,'pz','pz.dmp',1)
    call dumpn(zdnr,'zdnr','pz.dmp',1)
                                                                        call timer('pde','vel_bdry')
    call dumpn (dpydt, 'dpydt', 'py.dmp', 1)
  call forceit (r,Ux,Uy,Uz,xdnr,ydnr,zdnr,dpxdt,dpydt,dpzdt)
    call dumpn (dpydt, 'force', 'py.dmp', 1)
                                                                        call timer('pde','force')
!-----------------------------------------------------------------------
!  Strain tensor components
!-----------------------------------------------------------------------
  call ddxup_set (Ux, Sxx)
  call ddyup_set (Uy, Syy)
  call barrier_omp('pde2')                                              ! barrier 2, needed because Uz computed since last barrier
  call ddzup_set (Uz, Szz)
  call ddzdn_set (Uy,scr2)
  call ddxdn_set (Uy,scr1)
  call ddydn_set (Ux,scr3)
  call ddzdn_set (Ux,scr4)
  call ddxdn_set (Uz,scr5)
  call ddydn_set (Uz,scr6)
  do iz=izs,ize
    Sxy(:,:,iz) = (scr1(:,:,iz)+scr3(:,:,iz))*0.5
    Syz(:,:,iz) = (scr6(:,:,iz)+scr2(:,:,iz))*0.5
    Szx(:,:,iz) = (scr4(:,:,iz)+scr5(:,:,iz))*0.5
  end do
    call dumpn(Sxx,'Sxx','px.dmp',1)
    call dumpn(Syy,'Syy','py.dmp',1)
    call dumpn(Sxy,'Sxy','px.dmp',1)
    call dumpn(Sxy,'Sxy','py.dmp',1)
    call dumpn(Syz,'Syz','py.dmp',1)
    call dumpn(Szx,'Szx','px.dmp',1)

  if (Csmag > 0) then
    do iz=izs,ize
      scr4(:,:,iz) = 0.333333*(Sxx(:,:,iz) + Syy(:,:,iz) + Szz(:,:,iz))
      S2(:,:,iz) = min(0.,Sxx(:,:,iz)-scr4(:,:,iz))**2 &
                 + min(0.,Syy(:,:,iz)-scr4(:,:,iz))**2 &
                 + min(0.,Szz(:,:,iz)-scr4(:,:,iz))**2
      scr1(:,:,iz) = Sxy(:,:,iz)**2
      scr2(:,:,iz) = Syz(:,:,iz)**2
      scr3(:,:,iz) = Szx(:,:,iz)**2
    end do
    call xup1_set (scr1,scr4)
    call yup1_set (scr4,scr1)
    call yup1_set (scr2,scr4)
    call barrier_omp('pde3')                                            ! scr4 needs to be all done
    call zup1_set (scr4,scr2)
    call zup1_set (scr3,scr5)
    call barrier_omp('pde5')                                            ! scr4 needs to be all done
    call xup1_set (scr5,scr3)
    do iz=izs,ize
      S2(:,:,iz) = sqrt(2.*(S2(:,:,iz) + 2.*(scr1(:,:,iz)+scr2(:,:,iz)+scr3(:,:,iz))))
    end do
  else
    do iz=izs,ize
      S2(:,:,iz) = 0.
    end do
  end if
                                                                        call timer('pde','strain')
!-----------------------------------------------------------------------
!  First part of viscosity
!-----------------------------------------------------------------------
  do iz=izs,ize
    lne(:,:,iz) = alog(e(:,:,iz))
  end do
  call d2abs_set (lne,scr1)
  call d2abs_set (lnr,d2lnr)
  do iz=izs,ize
    d2lnr(:,:,iz) = 0.5*(d2lnr(:,:,iz) + scr1(:,:,iz))
  end do
  call regularize (d2lnr)
  call smooth3max3_set (d2lnr, scr1)
  call dumpn(d2lnr,'d2lnr','nu.dmp',1)
  call dumpn(d2lnr,'d2lnr','py.dmp',1)
  if (do_stratified .and. nrho > 0) then
    do iy=1,my
      frho(iy)=1./(1.+(rav(iy)/rav(nrho))**.30)
    end do
  else
    frho(:)=1.
  endif
  if (nu4 < 0.) then
    if (nu2 > 0.0) then
      call dif2_set (Ux, Uy, Uz, nud)
    else
      do iz=izs,ize
        nud(:,:,iz) = 0.0
      end do
    end if
    do iz=izs,ize
    do iy=1,my
    do ix=1,mx
      nud(ix,iy,iz) = (nu2*nud(ix,iy,iz) + Csmag*S2(ix,iy,iz))*frho(iy)
    end do
    end do
    end do
    nflop = nflop+4
  else
    if (nu2 > 0.0) then
      call dif1_set (Ux,Uy,Uz,nud)
    else
      do iz=izs,ize
        nud(:,:,iz) = 0.0
      end do
    end if
    do iz=izs,ize
    do iy=1,my
    do ix=1,mx
      nud(ix,iy,iz) = -Sxx(ix,iy,iz)-Syy(ix,iy,iz)-Szz(ix,iy,iz)
      nud(ix,iy,iz) = (nu2*max(nud(ix,iy,iz),0.) + Csmag*S2(ix,iy,iz))*frho(iy)
    end do
    end do
    end do
    nflop = nflop+4
  end if
    call dumpn (nud, 'nud', 'nu.dmp', 1)
    call dumpn (Sxx, 'Sxx', 'nu.dmp', 1)
    call dumpn (Syy, 'Syy', 'nu.dmp', 1)
    call dumpn (Szz, 'Szz', 'nu.dmp', 1)
    call dumpn (S2 , 'S2' , 'nu.dmp', 1)

  if (do_2nddiv) then
    call xup1_set (Ux, scr1)
    call yup1_set (Uy, scr2)
    call zup1_set (Uz, scr3)
  else
    call xup_set (Ux, scr1)
    call yup_set (Uy, scr2)
    call zup_set (Uz, scr3)
  end if
  do iz=izs,ize
    S2(:,:,iz) = scr1(:,:,iz)**2 + scr2(:,:,iz)**2 + scr3(:,:,iz)**2
  end do
  call average_subr (S2, Urms)

  if (do_fluxes) then
    do iz=izs,ize
      scr1(:,:,iz) = 0.5*S2(:,:,iz)*r(:,:,iz)
    end do
    call average_subr (scr1, E_kin)
  end if

!-----------------------------------------------------------------------
!  Energy per unit mass
!-----------------------------------------------------------------------
  if (do_energy) then
    do iz=izs,ize
      ee(:,:,iz) = e(:,:,iz)/r(:,:,iz)
    end do
  end if
  call dumpn(ee,'ee0','e.dmp',1)
                                                                        call timer('pde','visc1')
!-----------------------------------------------------------------------
!  Boundary condition -- may need to calculate pressure internally
!-----------------------------------------------------------------------
  if (do_energy) call energy_boundary(r,lnr,Ux,Uy,Uz,e,ee,p,Bx,By,Bz)
  call dumpn(ee,'ee1','e.dmp',1)
                                                                        call timer('pde','e_bdry')
!-----------------------------------------------------------------------
!  Pressure
!-----------------------------------------------------------------------
  call pressure (r,ee,p)
  call haverage_subr (p, pav)
     call dumpn(ee,'ee2','e.dmp',1)
     call dumpn (p, 'Pg', 'px.dmp', 1)
     call dumpn (p, 'Pg', 'py.dmp', 1)
     call dumpn (p, 'Pg', 'pz.dmp', 1)
                                                                        call timer('pde','pressure')
!-----------------------------------------------------------------------
!  Numerical viscosity, Richtmyer & Morton type.  For testing purposes,
!  nu1<0 signals the use of constant diffusivity.
!-----------------------------------------------------------------------
  if (nu1.lt.0.0) then
    do iz=izs,ize
      Cs(:,:,iz) = sqrt(gamma*p(:,:,iz)/r(:,:,iz)) + sqrt(S2(:,:,iz))
      nu(:,:,iz) = abs(nu1)
      nud(:,:,iz) = 0.
    end do
  else if (do_mhd) then
    call xup_set(Bx,scr1)
    call yup_set(By,scr2)
    call zup_set(Bz,scr3)
    if (cmax.eq.0) then
      do iz=izs,ize
      do iy=1,my
      do ix=1,mx
        wavesp = sqrt((scr1(ix,iy,iz)**2+scr2(ix,iy,iz)**2+scr3(ix,iy,iz)**2 &
                     + gamma*P(ix,iy,iz))/r(ix,iy,iz))
        velocity = sqrt(S2(ix,iy,iz))
!       scr2(ix,iy,iz) = nu1*velocity
!       scr1(ix,iy,iz) = nu3*wavesp
        Cs(ix,iy,iz) = wavesp + velocity
        nu(ix,iy,iz) = nu1*velocity + nu3*wavesp
      ! scr2(ix,iy,iz) = nu(ix,iy,iz) + max(dxm(ix),dym(iy),dzm(iz))*nud(ix,iy,iz)
      end do
      end do
      end do
    else
      do iz=izs,ize
      do iy=1,my
      do ix=1,mx
        wavesp = sqrt(min(cmax**2,(scr1(ix,iy,iz)**2+scr2(ix,iy,iz)**2+scr3(ix,iy,iz)**2 &
                     + gamma*P(ix,iy,iz))/r(ix,iy,iz)))
        velocity = sqrt(S2(ix,iy,iz))
        Cs(ix,iy,iz) = wavesp + velocity
        nu(ix,iy,iz) = nu1*velocity + nu3*wavesp
      ! scr2(ix,iy,iz) = nu(ix,iy,iz) + max(dxm(ix),dym(iy),dzm(iz))*nud(ix,iy,iz)
      end do
      end do
      end do
    end if
  else
    do iz=izs,ize
    do iy=1,my
    do ix=1,mx
      wavesp = sqrt(gamma*P(ix,iy,iz)/r(ix,iy,iz))
      velocity = sqrt(S2(ix,iy,iz))
!     scr1(ix,iy,iz) = nu3*wavesp
!     scr2(ix,iy,iz) = nu1*velocity
      Cs(ix,iy,iz) = wavesp + velocity
      nu(ix,iy,iz) = nu1*velocity + nu3*wavesp
      ! scr2(ix,iy,iz) = nu(ix,iy,iz) + max(dxm(ix),dym(iy),dzm(iz))*nud(ix,iy,iz)
    end do
    end do
    end do
  end if
  call viscosity_boundary (nu, scr1)
    call dumpn (nu  , 'nu' , 'nu.dmp', 1)
!   call dumpn (scr1, 'nu_wave'  , 'nu.dmp', 1)
!   call dumpn (scr2, 'nu_vel','nu.dmp', 1)
    call dumpn (Cs  , 'Csu', 'nu.dmp', 1)

!-----------------------------------------------------------------------
!  Enhance where ln(rho) changes too rapidly
!-----------------------------------------------------------------------
  if (nu4 > 0) then
    do iz=izs,ize
    do iy=1,my
    do ix=1,mx
      ! nu (ix,iy,iz) = nu (ix,iy,iz)*max(1.,nu4*d2lnr(ix,iy,iz))
      nud(ix,iy,iz) = nud(ix,iy,iz)*max(1.,nu4*d2lnr(ix,iy,iz))
    end do
    end do
    end do
  end if

!-----------------------------------------------------------------------
!  Make compatible with pde_omp, as an option
!-----------------------------------------------------------------------
  if (.not. do_aniso_nu) then
    do iz=izs,ize
    do iy=1,my
    do ix=1,mx
      nu(ix,iy,iz) = nu(ix,iy,iz) + max(dxm(ix),dym(iy),dzm(iz))*nud(ix,iy,iz)
      nud(ix,iy,iz) = 0.
    end do
    end do
    end do
  end if
    call dumpn (nu , 'nu' , 'nu.dmp', 1)
    call dumpn (nud, 'nud', 'nu.dmp', 1)

!-----------------------------------------------------------------------
!  Spread the influence over a few zones, regularize
!-----------------------------------------------------------------------
  call regularize(nu)
  call regularize(nud)

  !call barrier_omp('pde7')                                              ! barrier needed only if removing "single"
!$omp single
  Urms = sqrt(Urms)
!$omp end single nowait

  if (do_max5_hd) then
    call smooth3max5_set (nud, scr1)
    call smooth3max5_set (nu , scr2)
  else
    call smooth3max3_set (nud, scr1)
    call smooth3max3_set (nu , scr2)
  end if

  !call barrier_omp('pde8')                                              ! barrier 4, NOT NEEDED????
  call regularize(nud)
  call regularize(nu)
    call dumpn (nu , 'nu final'  , 'nu.dmp', 1)
    call dumpn (nud, 'nud final' , 'nu.dmp', 1)

!-----------------------------------------------------------------------
!  Courant conditions
!-----------------------------------------------------------------------
  if (flag) then
    do iz=izs,ize
    do iy=1,my
    do ix=1,mx
      Cs(ix,iy,iz) = Cs(ix,iy,iz)*(dt/min(dxm(ix),dym(iy),dzm(iz)))
      scr1(ix,iy,iz) = fCv*(nud(ix,iy,iz)+nu(ix,iy,iz)/min(dxm(ix),dym(iy),dzm(iz)))
    end do
    end do
    end do
    if (do_mhd) then
      Cu = 0.
    else
      call fmaxval_subr ('Cu',Cs,Cu)
    end if
    !call fmaxval_subr ('Cv',scr1,Cv)
    Cv = maxval(scr1); call max_real_mpi (Cv)
  end if
    call dumpn (scr1, 'Cv' , 'nu.dmp', 1)

  do iz=izs,ize
    nu(:,:,iz) = nu(:,:,iz)*r(:,:,iz)
    nud(:,:,iz) = nud(:,:,iz)*r(:,:,iz)
    lnu(:,:,iz) = alog(max(nu(:,:,iz),1e-30))
  end do
    call dumpn (nu, 'r*nu', 'nu.dmp', 1)
    call dumpn (nud, 'r*nud', 'nu.dmp', 1)
                                                                        call timer('pde','visc2')

!-----------------------------------------------------------------------
!  Mass conservation, cell centered
!-----------------------------------------------------------------------
if (do_density) then
  if (nur==0) then
      call ddxup_set (px, scr1)
      call ddyup_set (py, scr2)
      call ddzup_set (pz, scr3)
      call ddxup1_set (px, scr4)
      call ddyup1_set (py, scr5)
      call ddzup1_set (pz, scr6)
      do iz=izs,ize
        where (d2lnr(:,:,iz) > d2lnr_lim .or. do_2nddiv)
          drdt(:,:,iz) = drdt(:,:,iz) &
                       - scr4(:,:,iz) &
                       - scr5(:,:,iz) &
                       - scr6(:,:,iz)
        elsewhere
          drdt(:,:,iz) = drdt(:,:,iz) &
                       - scr1(:,:,iz) &
                       - scr2(:,:,iz) &
                       - scr3(:,:,iz)
        endwhere
      end do
    call dumpn (px  , 'px'  , 'r.dmp', 1)
    call dumpn (py  , 'py'  , 'r.dmp', 1)
    call dumpn (drdt, 'drdt', 'r.dmp', 1)
    call dumpn (scr2, 'dpy', 'r.dmp', 1)
    call dumpn (scr5, 'd1py', 'r.dmp', 1)

!-----------------------------------------------------------------------
!  Mass conservation, with mass diffusion
!-----------------------------------------------------------------------
  else
    call ddxdn_set (lnr, scr1)
    if (do_aniso_nu) then
      do iz=izs,ize; do iy=1,my
        lnu(:,iy,iz) = alog(max(nu(:,iy,iz)+dxm(:)*nud(:,iy,iz),1e-30))
      end do; end do
    end if
    call xdn1_set (lnu, scr2)
    if (do_quench) then
      do iz=izs,ize                                          ! store unquenched bdry values
        do iy=1,2*(my-ub)
          qbdry(:,iy,iz)=scr1(:,ub-(my-ub)+iy,iz)
        end do
      end do
      call quenchx1 (scr1)
      do iz=izs,ize                                          ! restore unquenched bdry values
        scr1(:,ub-(my-ub)+1:my,iz)=qbdry(:,1:2*(my-ub),iz)
      end do
    end if
    do iz=izs,ize
    do iy=1,my
      call expn (mx, scr2(:,iy,iz), exp1)
      where (d2lnr(:,iy,iz) > d2lnr_lim .or. do_2nddiv)
        scr1(:,iy,iz) = -px(:,iy,iz)+(nur*dxmdn(:))*exp1(:)*scr1(:,iy,iz)
      elsewhere
        scr1(:,iy,iz) = -px(:,iy,iz)
      end where
    end do
    end do
    call dumpn (scr1, 'scr1', 'px.dmp', 1)

    call ddxup_set  (scr1, scr2)
    call ddxup1_set (scr1, scr3)
    do iz=izs,ize
      where (d2lnr(:,:,iz) > d2lnr_lim .or. do_2nddiv)
        drdt(:,:,iz) = drdt(:,:,iz) &
                     + scr3(:,:,iz)
      elsewhere
        drdt(:,:,iz) = drdt(:,:,iz) &
                     + scr2(:,:,iz)
      endwhere
    end do
!
    if (do_stratified) then
      scr2 = lnr
      call smooth (scr2, 2)
      call haverage_subr (scr2, eeav)
      do iz=izs,ize
        do iy=1,my
          scr2(:,iy,iz) = lnr(:,iy,iz)-eeav(iy)
        end do
      end do
      call ddydn_set (scr2, scr1)
    else
      call ddydn_set (lnr, scr1)
    end if
    call dumpn (scr1, 'lnr-<lnr>', 'r.dmp', 1)

    if (do_aniso_nu) then
      do iz=izs,ize; do iy=1,my
        lnu(:,iy,iz) = alog(max(nu(:,iy,iz)+dym(iy)*nud(:,iy,iz),1e-30))
      end do; end do
    end if
    call ydn1_set (lnu, scr2)
    if (do_quench) then
      do iz=izs,ize                                          ! store unquenched bdry values
        do iy=1,2*(my-ub)
          qbdry(:,iy,iz)=scr1(:,ub-(my-ub)+iy,iz)
        end do
      end do
      call quenchy1 (scr1)
      do iz=izs,ize                                          ! restore unquenched bdry values
        scr1(:,ub-(my-ub)+1:my,iz)=qbdry(:,1:2*(my-ub),iz)
      end do
    end if
    call dumpn (scr1, 'lnr-<lnr> > quench', 'nu.dmp', 1)
    do iz=izs,ize
    do iy=1,my
      call expn (mx, scr2(:,iy,iz), exp1)
      where (d2lnr(:,iy,iz) > d2lnr_lim .or. do_2nddiv)
        scr1(:,iy,iz) = -py(:,iy,iz)+(nur*dymdn(iy))*exp1(:)*scr1(:,iy,iz)
      elsewhere
        scr1(:,iy,iz) = -py(:,iy,iz)
      end where
    end do
    end do
    call dumpn (scr1, 'py+', 'py.dmp', 1)

    call ddyup_set  (scr1, scr2)
    call ddyup1_set (scr1, scr3)
    do iz=izs,ize
      where (d2lnr(:,:,iz) > d2lnr_lim .or. do_2nddiv)
        drdt(:,:,iz) = drdt(:,:,iz) &
                     + scr3(:,:,iz)
      elsewhere
        drdt(:,:,iz) = drdt(:,:,iz) &
                     + scr2(:,:,iz)
      endwhere
    end do
    call dumpn (drdt, 'drdt', 'r.dmp', 1)
!
    call ddzdn_set (lnr, scr1)
    call barrier_omp('pde9')                                            ! barrier 5, needed because lnu & scr1 just computed
    if (do_aniso_nu) then
      do iz=izs,ize
        lnu(:,:,iz) = alog(max(nu(:,:,iz)+dzm(iz)*nud(:,:,iz),1e-30))
      end do
    end if
    if (do_quench) then
     do iz=izs,ize                                          ! store unquenched bdry values
        do iy=1,2*(my-ub)
          qbdry(:,iy,iz)=scr1(:,ub-(my-ub)+iy,iz)
        end do
      end do
      call quenchz1 (scr1)
      do iz=izs,ize                                          ! restore unquenched bdry values
        scr1(:,ub-(my-ub)+1:my,iz)=qbdry(:,1:2*(my-ub),iz)
      end do
    end if
    call zdn1_set (lnu, scr2)
    do iz=izs,ize
    do iy=1,my
      call expn (mx, scr2(:,iy,iz), exp1)
      where (d2lnr(:,iy,iz) > d2lnr_lim .or. do_2nddiv)
        scr1(:,iy,iz) = -pz(:,iy,iz)+(nur*dzmdn(iz))*exp1(:)*scr1(:,iy,iz)
      elsewhere
        scr1(:,iy,iz) = -pz(:,iy,iz)
      end where
    end do
    end do

    call barrier_omp('pde11')                                           ! barrier 7, needed because scr1 modified since last barrier
    call ddzup_set  (scr1, scr2)
    call ddzup1_set (scr1, scr3)
    do iz=izs,ize
      where (d2lnr(:,:,iz) > d2lnr_lim .or. do_2nddiv)
        drdt(:,:,iz) = drdt(:,:,iz) &
                     + scr3(:,:,iz)
      elsewhere
        drdt(:,:,iz) = drdt(:,:,iz) &
                     + scr2(:,:,iz)
      endwhere
    end do
  end if
  call barrier_omp('pde12')                                             ! barrier 7a, needed because scr1 modified later on!!!
                                                                        call timer('pde','mass')
end if ! (do_density)

!-----------------------------------------------------------------------
!  Passive scalar
!-----------------------------------------------------------------------
  if (do_pscalar) then
    do iz=izs,ize
      dd(:,:,iz) = d(:,:,iz)/r(:,:,iz)
    end do
    call passive (nu,lnu,r,px,py,pz,Ux,Uy,Uz,dd,dddt)
  end if
  call trace_particles (Ux,Uy,Uz)
END

!***********************************************************************
SUBROUTINE pde2 (flag, mx, my, mz, &
                 r, px, py, pz, e, d, Bx, By, Bz, &
                 drdt, dpxdt, dpydt, dpzdt, dedt, &
                 dddt, dBxdt, dBydt, dBzdt, &
                 xdnl, ydnl, zdnl, nu, lnu, lnr, nud, &
                 Ux, Uy, Uz, S2, p, ee, lne, dd, Cs, &
                 Sxy, Syz, Szx, Sxx, Syy, Szz, &
                 Txy, Tyz, Tzx, Txx, Tyy, Tzz, d2lnr, &
                 scr1, scr2, scr3, scr4, scr5, scr6, d2lnr_max,frho)
  USE params, mx_void=>mx, my_void=>my, mz_void=>mz
  USE arrays, only: dpxav, dpzav, pav
  USE pde_mod
  implicit none
  logical flag
  integer mx, my, mz
  real, dimension(mx,my,mz):: &
                 r, px, py, pz, e, d, Bx, By, Bz, &
                 drdt, dpxdt, dpydt, dpzdt, dedt, &
                 dddt, dBxdt, dBydt, dBzdt, &
                 xdnl, ydnl, zdnl, nu, lnu, lnr, nud, &
                 Ux, Uy, Uz, S2, p, ee, lne, dd, Cs, &
                 Sxy, Syz, Szx, Sxx, Syy, Szz, &
                 Txy, Tyz, Tzx, Txx, Tyy, Tzz, d2lnr, &
                 scr1, scr2, scr3, scr4, scr5, scr6
  integer iz, iy, ix
  real d2lnr_max
  real, dimension(mx):: nuxy, nuyz, nuzx, exp1
  real, dimension(my):: frho
  real average, nuS1, nuS2, divu, nux, nuy, nuz

!-----------------------------------------------------------------------
!  Quenching operator
!-----------------------------------------------------------------------
  do iz=izs,ize
    scr1(:,:,iz) = Sxx(:,:,iz)
    scr2(:,:,iz) = Syy(:,:,iz)
    scr3(:,:,iz) = Szz(:,:,iz)
  end do
  if (do_quench) then
    do iz=izs,ize                                          ! store unquenched bdry values
      do iy=1,2*(my-ub)
        qbdry(:,iy,iz)=scr1(:,ub-(my-ub)+iy,iz)
      end do
    end do
    call quenchx1 (scr1)
    do iz=izs,ize                                          ! restore unquenched bdry values
      do iy=1,2*(my-ub)
        scr1(:,ub-(my-ub)+iy,iz)=qbdry(:,iy,iz)
      end do
    end do
    do iz=izs,ize                                          ! store unquenched bdry values
      do iy=1,2*(my-ub)
        qbdry(:,iy,iz)=scr2(:,ub-(my-ub)+iy,iz)
      end do
    end do
    call quenchy1 (scr2)
    do iz=izs,ize                                          ! restore unquenched bdry values
      scr2(:,ub-(my-ub)+1:my,iz)=qbdry(:,1:2*(my-ub),iz)
    end do
    do iz=izs,ize                                          ! store unquenched bdry values
      do iy=1,2*(my-ub)
        qbdry(:,iy,iz)=scr3(:,ub-(my-ub)+iy,iz)
      end do
    end do
    call quenchz1 (scr3)
    do iz=izs,ize                                          ! restore unquenched bdry values
      scr3(:,ub-(my-ub)+1:my,iz)=qbdry(:,1:2*(my-ub),iz)
    end do
  end if
                                                                        call timer('pde','quench')
      call dumpn (Syy, 'Syy<qy', 'quench.dmp', 1)
      call dumpn (scr2, 'Syy>qy', 'quench.dmp', 1)
      call dumpn (scr3, 'qz', 'quench.dmp', 1)
      call dumpn (scr2, 'qy', 'quench.dmp', 1)

!-----------------------------------------------------------------------
!  Viscous stress, diagonal elements
!-----------------------------------------------------------------------
    if (do_energy) call dumpn (e, 'e', 'e.dmp', 1)
    if (do_energy) call dumpn (dedt, 'detdt (0)', 'e.dmp', 1)
    call dumpn (p, 'Pg', 'px.dmp', 1)
    call dumpn (p, 'Pg', 'py.dmp', 1)
  nuS1 = 3.*nuS/(1.+2.*nuS)
  nuS2 = (1.-nuS)/(1.+2.*nuS)

  do iz=izs,ize
    do iy=1,my
    do ix=1,mx
      divu = Sxx(ix,iy,iz) + Syy(ix,iy,iz) + Szz(ix,iy,iz)
      nux = dxm(ix)*(nu(ix,iy,iz)+dxm(ix)*nud(ix,iy,iz))
      nuy = dym(iy)*(nu(ix,iy,iz)+dym(iy)*nud(ix,iy,iz))
      nuz = dzm(iz)*(nu(ix,iy,iz)+dzm(iz)*nud(ix,iy,iz))
      Txx(ix,iy,iz) = - 2.*nux*(nuS1*scr1(ix,iy,iz)+nuS2*divu)
      Tyy(ix,iy,iz) = - 2.*nuy*(nuS1*scr2(ix,iy,iz)+nuS2*divu)               ! scr2 = Syy
      Tzz(ix,iy,iz) = - 2.*nuz*(nuS1*scr3(ix,iy,iz)+nuS2*divu)
      scr2(ix,iy,iz) = nu(ix,iy,iz)+dxm(iy)*nud(ix,iy,iz)
    end do
    end do
    if (do_energy) then
      if (do_dissipation) then
        scr1(:,:,iz) = - Txx(:,:,iz)*Sxx(:,:,iz) &
                       - Tyy(:,:,iz)*Syy(:,:,iz) &
                       - Tzz(:,:,iz)*Szz(:,:,iz)
        dedt(:,:,iz) = dedt(:,:,iz) + scr1(:,:,iz)
      end if
      dedt(:,:,iz) = dedt(:,:,iz) &
        - p(:,:,iz)*(Sxx(:,:,iz) + Syy(:,:,iz) + Szz(:,:,iz))
    end if
  end do
    if (do_energy) call dumpn (scr1, 'TiiSii', 'e.dmp', 1)
    call dumpn (scr2, 'nu', 'px.dmp', 1)
    call dumpn (Sxx, 'Sxx', 'px.dmp', 1)
    call dumpn (Txx, 'Txx', 'px.dmp', 1)
    call dumpn (Tyy, 'Tyy', 'e.dmp', 1)
    call dumpn (scr2, 'nu', 'py.dmp', 1)
    call dumpn (Syy, 'Syy', 'py.dmp', 1)
    call dumpn (Tyy, 'Tyy', 'py.dmp', 1)
    call dumpn (Tyy, 'Tyy', 'e.dmp', 1)

  if (do_energy .and. do_dissipation) then
    if (lb >  1) scr1(:, 1:lb+1,izs:ize) = 0.                           ! zap ghost-zone values
    if (ub < my) scr1(:,ub-1:my,izs:ize) = 0.
    call dumpn(scr1,'diagonal','e.dmp',1)
    call barrier_omp ('Qk1')
    call average_subr (scr1, Q_kin)
  end if
                                                                        call timer('pde','mom-diag')
!-----------------------------------------------------------------------
!  Interpolated log viscosity coefficients (already contain rho-factor)
!-----------------------------------------------------------------------
  do iz=izs,ize
  do iy=1,my
  do ix=1,mx
    lnu(ix,iy,iz) = alog(max(nu(ix,iy,iz)+0.5*(dxm(ix)+dym(iy))*nud(ix,iy,iz),1e-30))
  end do
  end do
  end do
  call xdn1_set (lnu,scr1)
    call dumpn (lnu , 'lnu (nu+dy*nud)' , 'py.dmp', 1)
    call dumpn (scr1, 'scr1', 'py.dmp', 1)
  call ydn1_set (scr1,scr2)
    call dumpn (scr2, 'scr2', 'py.dmp', 1)

  do iz=izs,ize
  do iy=1,my
  do ix=1,mx
    lnu(ix,iy,iz) = alog(max(nu(ix,iy,iz)+0.5*(dxm(ix)+dzm(iz))*nud(ix,iy,iz),1e-30))
  end do
  end do
  end do
  call xdn1_set (lnu,scr1)

  call barrier_omp('pde15')                                             ! barrier 10, scr1
  call zdn1_set (scr1,scr3)

  do iz=izs,ize
  do iy=1,my
  do ix=1,mx
    lnu(ix,iy,iz) = alog(max(nu(ix,iy,iz)+0.5*(dym(iy)+dzm(iz))*nud(ix,iy,iz),1e-30))
  end do
  end do
  end do
  call ydn1_set (lnu,scr5)
  call barrier_omp('pde16')                                             ! barrier 11, scr5
  call zdn1_set (scr5,scr1)
  call barrier_omp('pde16a')                                            ! barrier 11, scr5
                                                                        call timer('pde','log nu')
!-----------------------------------------------------------------------
!  Stress tensor, off-diagonal terms
!-----------------------------------------------------------------------
  do iz=izs,ize
    Txy(:,:,iz) = Sxy(:,:,iz)
    Tyz(:,:,iz) = Syz(:,:,iz)
    Tzx(:,:,iz) = Szx(:,:,iz)
  end do
    call dumpn(Txy,'Txy<q','px.dmp',1)
    call dumpn(Txy,'Txy<q','py.dmp',1)
    call dumpn(Tyz,'Tyz<q','py.dmp',1)
    call dumpn(Tzx,'Tzx<q','px.dmp',1)
  if (do_quench) then
    do iz=izs,ize                                          ! store unquenched bdry values
      do iy=1,2*(my-ub)
        qbdry(:,iy,iz)=Txy(:,ub-(my-ub)+iy,iz)
      end do
    end do
    call quenchx2 (Txy, scr4)
    call quenchy2 (Txy, scr5)
    do iz=izs,ize
      Txy(:,:,iz) = 0.5*(scr4(:,:,iz)+scr5(:,:,iz))
    end do
    do iz=izs,ize                                          ! restore unquenched bdry values
      Txy(:,ub-(my-ub)+1:my,iz)=qbdry(:,1:2*(my-ub),iz)
    end do
    do iz=izs,ize                                          ! store unquenched bdry values
      do iy=1,2*(my-ub)
        qbdry(:,iy,iz)=Tyz(:,ub-(my-ub)+iy,iz)
      end do
    end do
    call quenchy2 (Tyz, scr4)
    call quenchz2 (Tyz, scr5)
    do iz=izs,ize
      Tyz(:,:,iz) = 0.5*(scr4(:,:,iz)+scr5(:,:,iz))
    end do
      call dumpn(scr4,'quenchy2(Tyz)','py.dmp',1)
      call dumpn(scr5,'quenchz2(Tyz)','py.dmp',1)
    do iz=izs,ize                                          ! restore unquenched bdry values
      Tyz(:,ub-(my-ub)+1:my,iz)=qbdry(:,1:2*(my-ub),iz)
    end do
    do iz=izs,ize                                          ! store unquenched bdry values
      do iy=1,2*(my-ub)
        qbdry(:,iy,iz)=Tzx(:,ub-(my-ub)+iy,iz)
      end do
    end do
    call quenchz2 (Tzx, scr4)
    call quenchx2 (Tzx, scr5)
    do iz=izs,ize
      Tzx(:,:,iz) = 0.5*(scr4(:,:,iz)+scr5(:,:,iz))
    end do
    do iz=izs,ize                                          ! restore unquenched bdry values
      Tzx(:,ub-(my-ub)+1:my,iz)=qbdry(:,1:2*(my-ub),iz)
    end do
  end if
    call dumpn(Txy,'Txy>q','px.dmp',1)
    call dumpn(Txy,'Txy>q','py.dmp',1)
    call dumpn(Tyz,'Tyz>q','py.dmp',1)
    call dumpn(Tzx,'Tzx>q','px.dmp',1)
  do iz=izs,ize
  do iy=1,my
    call expn (mx, scr2(:,iy,iz), nuxy)                                 ! scr2 = ydn(xdn(alog(nu)))
    call expn (mx, scr1(:,iy,iz), nuyz)                                 ! scr1 = zdn(ydn(alog(nu)))
    call expn (mx, scr3(:,iy,iz), nuzx)                                 ! scr3 = zdn(xdn(alog(nu)))
    do ix=1,mx
      Txy(ix,iy,iz) = - nuS1*max(dxmdn(ix),dymdn(iy))*nuxy(ix)*Txy(ix,iy,iz)
      Tyz(ix,iy,iz) = - nuS1*max(dymdn(iy),dzmdn(iz))*nuyz(ix)*Tyz(ix,iy,iz)
      Tzx(ix,iy,iz) = - nuS1*max(dzmdn(iz),dxmdn(ix))*nuzx(ix)*Tzx(ix,iy,iz)
    end do
  end do
  end do
      call dumpn (Txy, 'Txy*nu', 'px.dmp', 1)
      call dumpn (Txy, 'Txy*nu', 'py.dmp', 1)
      call dumpn (Tyz, 'Tyz*nu', 'py.dmp', 1)
      call dumpn (Tzx, 'Tzx*nu', 'px.dmp', 1)
      call dumpn (dpxdt, 'dpxdt', 'px.dmp', 1) ! 2
      call dumpn (dpzdt, 'dpzdt', 'pz.dmp', 1)
                                                                        call timer('pde','mom_off-diag')

!-----------------------------------------------------------------------
!  Compensate for net horizontal viscous drag
!-----------------------------------------------------------------------
  if (do_nodrag .and. do_stratified .and. abs(omegax)+abs(omegay)+abs(omegaz) > 0.) then
    call ddyup_set (Txy, scr1)
    call ddyup_set (Tyz, scr2)
    call haverage_subr (scr1, dpxav)
    call haverage_subr (scr2, dpzav)
    do iz=izs,ize
      do iy=1,my
        dpxdt(:,iy,iz) = dpxdt(:,iy,iz) + dpxav(iy)
        dpzdt(:,iy,iz) = dpzdt(:,iy,iz) + dpzav(iy)
      end do
    end do
      call dumpn (dpxdt, 'dpxdt', 'px.dmp', 1) ! 3
      call dumpn (dpzdt, 'dpzdt', 'pz.dmp', 1)
                                                                        call timer('pde','nodrag')
  end if

!-----------------------------------------------------------------------
!  Viscous dissipation
!-----------------------------------------------------------------------
  if (do_energy .and. do_dissipation) then
    do iz=izs,ize
      scr1(:,:,iz) = -2.*Txy(:,:,iz)*Sxy(:,:,iz)                        ! sub- and super-diagonal terms
      scr2(:,:,iz) = -2.*Tyz(:,:,iz)*Syz(:,:,iz)
      scr3(:,:,iz) = -2.*Tzx(:,:,iz)*Szx(:,:,iz)
    end do

    call xup1_set (scr1, scr4)                                           ! center the xy-dissip
    call yup1_set (scr4, scr1)

    call yup1_set (scr2, scr5)
    call barrier_omp('pde20')                                           ! barrier 14, scr5 and scr3
    call zup1_set (scr5, scr2)

    call zup1_set (scr3, scr6)
    call barrier_omp('pde21')                                           ! barrier 14, scr5 and scr3
    call xup1_set (scr6, scr3)

    do iz=izs,ize
      scr1(:,:,iz) = scr1(:,:,iz) &                                   ! for diagnostics only
                   + scr2(:,:,iz) &
                   + scr3(:,:,iz)
      dedt(:,:,iz) = dedt(:,:,iz) &                                     ! add in centered dissipation
                   + scr1(:,:,iz)
    end do

    if (do_energy .and. do_dissipation) then
      if (lb >  1) scr1(:, 1:lb+1,izs:ize) = 0.                         ! zap ghost-zone values
      if (ub < my) scr1(:,ub-1:my,izs:ize) = 0.
      call barrier_omp ('Q_kin')
      Q_kin = Q_kin + average(scr1)
      call dumpn (scr1, 'T*Sxy offdiag', 'e.dmp', 1)
      call dumpn (scr2, 'T*Syz offdiag', 'e.dmp', 1)
      call dumpn (dedt, 'dedt (diss)',   'e.dmp',1)
    end if
                                                                        call timer('pde','visc.diss.')
  end if
  call hd_fluxes (r,Ux,Uy,Uz,e,p,py,Txy,Tyy,Tyz,scr1,scr2,scr3,scr4,scr5,scr6)

!-----------------------------------------------------------------------
!  Add Reynolds stress
!-----------------------------------------------------------------------
  if (do_2nddiv) then
    call xup1_set (Ux,scr1)
    call yup1_set (Uy,scr2)
    call zup1_set (Uz,scr3)
  else
    call xup_set (Ux,scr1)
    call yup_set (Uy,scr2)
    call zup_set (Uz,scr3)
  end if
    call dumpn (Txx, 'Txx (visc)', 'px.dmp', 1)
    call dumpn (Tyy, 'Tyy (visc)', 'py.dmp', 1)
    call dumpn (Tyy, 'Tyy (visc)', 'e.dmp', 1)
  do iz=izs,ize
    Txx(:,:,iz) = Txx(:,:,iz) + r(:,:,iz)*scr1(:,:,iz)**2
    Tyy(:,:,iz) = Tyy(:,:,iz) + r(:,:,iz)*scr2(:,:,iz)**2
    Tzz(:,:,iz) = Tzz(:,:,iz) + r(:,:,iz)*scr3(:,:,iz)**2
    if (.not. do_loginterp) then
      Txx(:,:,iz) = Txx(:,:,iz) + p(:,:,iz)
      Tyy(:,:,iz) = Tyy(:,:,iz) + p(:,:,iz)
      Tzz(:,:,iz) = Tzz(:,:,iz) + p(:,:,iz)
    end if
  end do
  call dumpn (r, 'rho', 'r.dmp', 1)
  call dumpn (Ux, 'Ux', 'px.dmp', 1)
  call dumpn (Uy, 'Uy', 'py.dmp', 1)
  call dumpn (scr2, 'Uy up', 'py.dmp', 1)
  call dumpn (p, 'Pg', 'py.dmp', 1)
  call dumpn (Tyy, 'Tyy rU^2+diss+P', 'py.dmp', 1)
  call dumpn (Tyy, 'Tyy rU^2+diss+P', 'py.dmp', 1)
  call dumpn (Tyy, 'Tyy rU^2+diss+P', 'e.dmp', 1)

  if (do_2nddiv) then
    call xdn1_set (ydnl, scr1)
  else
    call xdn_set (ydnl, scr1)
  end if
  call xdn_set (Uy, scr2)
  call ydn_set (Ux, scr3)
  do iz=izs,ize
  do iy=1,my
    call expn (mx, scr1(:,iy,iz), exp1)
    Txy(:,iy,iz) = Txy(:,iy,iz) + exp1*scr3(:,iy,iz)*scr2(:,iy,iz)
  end do
  end do

  if (do_2nddiv) then
    call ydn1_set (zdnl, scr1)
  else
    call ydn_set (zdnl, scr1)
  end if
  call ydn_set (Uz, scr2)
  call zdn_set (Uy, scr3)
  do iz=izs,ize
  do iy=1,my
    call expn (mx, scr1(:,iy,iz), exp1)
    Tyz(:,iy,iz) = Tyz(:,iy,iz) + exp1*scr3(:,iy,iz)*scr2(:,iy,iz)
  end do
  end do

    call dumpn (Tzx, 'Tzx', 'px.dmp', 1)
    call dumpn (Tzx, 'Tzx', 'pz.dmp', 1)
    call dumpn (Ux, 'Ux', 'px.dmp', 1)
    call dumpn (Uz, 'Uz', 'pz.dmp', 1)

  if (do_2nddiv) then
    call zdn1_set (xdnl, scr1)
  else
    call zdn_set (xdnl, scr1)
  end if
  call zdn_set (Ux, scr2)
  call xdn_set (Uz, scr3)
    call dumpn (scr2, 'scr2', 'px.dmp', 1)
    call dumpn (scr3, 'scr3', 'pz.dmp', 1)
    call dumpn (scr1, 'scr1', 'px.dmp', 1)
    call dumpn (scr1, 'scr1', 'pz.dmp', 1)
  do iz=izs,ize
  do iy=1,my
    call expn (mx, scr1(:,iy,iz), exp1)
    Tzx(:,iy,iz) = Tzx(:,iy,iz) + exp1*scr3(:,iy,iz)*scr2(:,iy,iz)
  end do
  end do
                                                                        call timer('pde','add R-stress')

!-----------------------------------------------------------------------
!  Equations of motion, face centred
!-----------------------------------------------------------------------
  call barrier_omp('pde22')                                             ! needed because Tzx just computed
    call ddxdn_set (Txx, scr1)
    call ddyup_set (Txy, scr2)
    call ddzup_set (Tzx, scr3)
    call ddxdn1_set (Txx, scr4)
    call ddyup1_set (Txy, scr5)
    call ddzup1_set (Tzx, scr6)
    do iz=izs,ize
      where (d2lnr(:,:,iz) > d2lnr_lim .or. do_2nddiv)
        scr1(:,:,iz) = scr4(:,:,iz)
        scr2(:,:,iz) = scr5(:,:,iz)
        scr3(:,:,iz) = scr6(:,:,iz)
      endwhere
    end do
  do iz=izs,ize
    dpxdt(:,:,iz) = dpxdt(:,:,iz) - scr1(:,:,iz) - scr2(:,:,iz) - scr3(:,:,iz)
  end do
    call dumpn (Tzx, 'Tzx', 'px.dmp', 1)
    call dumpn (scr1, 'scr1', 'px.dmp', 1)
    call dumpn (scr3, 'scr3', 'px.dmp', 1)

    call ddydn_set (Tyy, scr1)
    call ddzup_set (Tyz, scr2)
    call ddxup_set (Txy, scr3)
    call ddydn1_set (Tyy, scr4)
    call ddzup1_set (Tyz, scr5)
    call ddxup1_set (Txy, scr6)
    do iz=izs,ize
      where (d2lnr(:,:,iz) > d2lnr_lim .or. do_2nddiv)
        scr1(:,:,iz) = scr4(:,:,iz)
        scr2(:,:,iz) = scr5(:,:,iz)
        scr3(:,:,iz) = scr6(:,:,iz)
      endwhere
    end do
  do iz=izs,ize
    dpydt(:,:,iz) = dpydt(:,:,iz) - scr1(:,:,iz) - scr2(:,:,iz) - scr3(:,:,iz)
  end do
    call dumpn (scr1, 'scr1', 'py.dmp', 1)
    call dumpn (scr2, 'scr2', 'py.dmp', 1)
    call dumpn (scr3, 'scr3', 'py.dmp', 1)
    call dumpn (dpydt, 'dpydt', 'py.dmp', 1)
    call dumpn (dpydt, '-div(T)', 'py.dmp', 1)

    call ddzdn_set (Tzz, scr1)
    call ddxup_set (Tzx, scr2)
    call ddyup_set (Tyz, scr3)
    call ddzdn1_set (Tzz, scr4)
    call ddxup1_set (Tzx, scr5)
    call ddyup1_set (Tyz, scr6)
    do iz=izs,ize
      where (d2lnr(:,:,iz) > d2lnr_lim .or. do_2nddiv)
        scr1(:,:,iz) = scr4(:,:,iz)
        scr2(:,:,iz) = scr5(:,:,iz)
        scr3(:,:,iz) = scr6(:,:,iz)
      endwhere
    end do
  do iz=izs,ize
    dpzdt(:,:,iz) = dpzdt(:,:,iz) - scr1(:,:,iz) - scr2(:,:,iz) - scr3(:,:,iz)
  end do
    call dumpn (Tzx, 'Tzx', 'pz.dmp', 1)
    call dumpn (Tyz, 'Tyz', 'pz.dmp', 1)
    call dumpn (scr1, 'scr1', 'pz.dmp', 1)
    call dumpn (scr2, 'scr2', 'pz.dmp', 1)
    call dumpn (dpxdt, 'dpxdt', 'px.dmp', 1) ! 4
    call dumpn (dpzdt, 'dpzdt', 'pz.dmp', 1)

  if (do_loginterp) then
    do iy=1,my
      scr4(:,iy,izs:ize) = log(pav(iy))
    end do
    call ydn_set (scr4, scr5)
    call ddydn_set (scr4, scr6)
    do iz=izs,ize
      scr4(:,:,iz) = exp(scr4(:,:,iz))
      scr5(:,:,iz) = exp(scr5(:,:,iz))
    end do

    do iz=izs,ize
      do iy=1,my
        scr1(:,iy,iz) = log(p(:,iy,iz)/pav(iy))
      end do
    end do

    call ddxdn_set (scr1, scr3)
    call ddxdn1_set (scr1, scr2)
    do iz=izs,ize
      where (d2lnr(:,:,iz) > d2lnr_lim .or. do_2nddiv)
        scr3(:,:,iz) = scr2(:,:,iz)
      endwhere
    end do
    call xdn_set (scr1, scr2)
    do iz=izs,ize
      dpxdt(:,:,iz) = dpxdt(:,:,iz) - scr4(:,:,iz)*exp(scr2(:,:,iz))*scr3(:,:,iz)
    end do
    call ddydn_set (scr1, scr3)
    call ddydn1_set (scr1, scr2)
    do iz=izs,ize
      where (d2lnr(:,:,iz) > d2lnr_lim .or. do_2nddiv)
        scr3(:,:,iz) = scr2(:,:,iz)
      endwhere
    end do
        call dumpn (scr2, 'ydn(lnPg)', 'py.dmp', 1)
        call dumpn (scr3, 'ddydn(lnPg)', 'py.dmp', 1)
    call ydn_set (scr1, scr2)
    do iz=izs,ize
      dpydt(:,:,iz) = dpydt(:,:,iz) - scr5(:,:,iz)*exp(scr2(:,:,iz))*(scr3(:,:,iz)+scr6(:,:,iz))
    end do
    call ddzdn_set (scr1, scr3)
    call ddzdn1_set (scr1, scr2)
    do iz=izs,ize
      where (d2lnr(:,:,iz) > d2lnr_lim .or. do_2nddiv)
        scr3(:,:,iz) = scr2(:,:,iz)
      endwhere
    end do
    call zdn_set (scr1, scr2)
    do iz=izs,ize
      dpzdt(:,:,iz) = dpzdt(:,:,iz) - scr4(:,:,iz)*exp(scr2(:,:,iz))*scr3(:,:,iz)
    end do
  end if
    call dumpn (dpxdt, 'dpxdt>dlnP', 'px.dmp', 1) ! 1
    call dumpn (dpydt, 'dpydt>dlnP', 'py.dmp', 1)
    call dumpn (dpzdt, 'dpzdt>dlnP', 'pz.dmp', 1)

                                                                        call timer('pde','face centered eom')
END

!***********************************************************************
SUBROUTINE pde3 (flag, mx, my, mz, &
                 r, px, py, pz, e, d, Bx, By, Bz, Ex, Ey, Ez, &
                 drdt, dpxdt, dpydt, dpzdt, dedt, &
                 dddt, dBxdt, dBydt, dBzdt, &
                 xdnl, ydnl, zdnl, nu, lnu, lnr, nud, &
                 Ux, Uy, Uz, S2, p, ee, lne, dd, Cs, &
                 Sxy, Syz, Szx, Sxx, Syy, Szz, &
                 Txy, Tyz, Tzx, Txx, Tyy, Tzz, d2lnr, &
                 scr1, scr2, scr3, scr4, scr5, scr6, d2lnr_max,frho)
  USE params, mx_void=>mx, my_void=>my, mz_void=>mz
  USE arrays, only: eeav, fedif
  USE pde_mod
  implicit none
  logical flag
  integer mx, my, mz
  real, dimension(mx,my,mz):: &
                 r, px, py, pz, e, d, Bx, By, Bz, Ex, Ey, Ez, &
                 drdt, dpxdt, dpydt, dpzdt, dedt, &
                 dddt, dBxdt, dBydt, dBzdt, &
                 xdnl, ydnl, zdnl, nu, lnu, lnr, nud, &
                 Ux, Uy, Uz, S2, p, ee, lne, dd, Cs, &
                 Sxy, Syz, Szx, Sxx, Syy, Szz, &
                 Txy, Tyz, Tzx, Txx, Tyy, Tzz, d2lnr, &
                 scr1, scr2, scr3, scr4, scr5, scr6
  integer iz, iy, ix
  real d2lnr_max
  real, dimension(mx):: exp1, exp2
  real, dimension(my):: frho
  real(8), allocatable, dimension(:,:,:):: dbl1, dbl2, dbl3

!-----------------------------------------------------------------------

if (do_energy) then
  do iz=izs,ize
  do iy=1,my
  do ix=1,mx
    lnu(ix,iy,iz) = alog(nuE*max(nu(ix,iy,iz)+dxm(ix)*nud(ix,iy,iz),1e-30))
    lne(ix,iy,iz) = alog(ee(ix,iy,iz))
  end do
  end do
  end do
    call dumpn(dedt,'dedt<adv+diff','e.dmp',1)

!-----------------------------------------------------------------------
!  Energy equation; x-advection and diffusion
!-----------------------------------------------------------------------
  call xdn1_set (lnu, scr1)                                              ! lnu = alog(rho*nu)
  if (do_2nddiv) then
    call xdn1_set (lne, scr6)                                              ! lne = alog(ee)
  else
    call xdn_set (lne, scr6)                                              ! lne = alog(ee)
  end if
  call ddxdn_set (lne, scr3)
  if (do_quench) then
    do iz=izs,ize                                          ! store unquenched bdry values
      do iy=1,2*(my-ub)
        qbdry(:,iy,iz)=scr3(:,ub-(my-ub)+iy,iz)
      end do
    end do
    call quenchx1 (scr3)
    do iz=izs,ize                                          ! restore unquenched bdry values
      scr3(:,ub-(my-ub)+1:my,iz)=qbdry(:,1:2*(my-ub),iz)
    end do
  end if
  do iz=izs,ize
  do iy=1,my
    call expn (mx, scr6(:,iy,iz), exp1)                                 ! exp1 = xdn(ee)
    call expn (mx, scr1(:,iy,iz), exp2)                                 ! exp2 = xdn(rho*nu)
    do ix=1,mx                                                          ! xdn(ee)*(px - ds*xdn(rho*nu)*ddxdn(lnee))
      scr3(ix,iy,iz) = exp1(ix)*(px(ix,iy,iz) - dxmdn(ix)*exp2(ix)*scr3(ix,iy,iz))
    end do
  end do
  end do

  call ddxup_set  (scr3, scr1)
  call ddxup1_set (scr3, scr6)
  do iz=izs,ize
    where (d2lnr(:,:,iz) > d2lnr_lim .or. do_2nddiv)
      scr1(:,:,iz) = scr6(:,:,iz)
    endwhere
  end do
  do iz=izs,ize
    dedt(:,:,iz) = dedt(:,:,iz) - scr1(:,:,iz)
  end do
    call dumpn (dedt, 'dedt_x-transp', 'e.dmp', 1)

!-----------------------------------------------------------------------
!  Energy equation; y-advection and diffusion
!-----------------------------------------------------------------------
  do iz=izs,ize
  do iy=1,my
  do ix=1,mx
    lnu(ix,iy,iz) = alog(nuE*max(nu(ix,iy,iz)+dym(iy)*nud(ix,iy,iz),1e-30))
  end do
  end do
  end do

  if (do_2nddiv) then
    call ydn1_set (lne, scr1)
  else
    call ydn_set (lne, scr1)
  end if
  call ydn1_set (lnu, scr2)

  if (do_stratified) then
    !--------------------------------------------------------------------
    ! Subtract off the mean stratification (smoothed), to reduce the
    ! mean flux component.  If stratified==1 or 3, also subtract the
    ! average diffusive flux.  If statified >=2 use quenching
    !--------------------------------------------------------------------
    do iz=izs,ize
      scr6(:,:,iz) = ee(:,:,iz)
    end do
    call smooth (scr6, 2)
    call haverage_subr (scr6, eeav)
    do iz=izs,ize
      do iy=1,my
        scr6(:,iy,iz) = lne(:,iy,iz)-log(eeav(iy))
      end do
    end do
    call ddydn1_set (scr6, scr3)
    call dumpn(scr3, 'deedy', 'e.dmp', 1)
    if (stratified>=2) then
      do iz=izs,ize                                          ! store unquenched bdry values
        do iy=1,2*(my-ub)
          qbdry(:,iy,iz)=scr3(:,ub-(my-ub)+iy,iz)
        end do
      end do
      call quenchy1 (scr3)                           ! normally, use quenching if do_quench=.true.
      do iz=izs,ize                                          ! restore unquenched bdry values
        scr3(:,ub-(my-ub)+1:my,iz)=qbdry(:,1:2*(my-ub),iz)
      end do
    end if
    call dumpn(scr3, 'deedyq', 'e.dmp', 1)
    !--------------------------------------------------------------------
    !  In cases with y-stratification, subtract off the average flux caused
    !  by a diffusive term, so subtract  <-nu*rho*ddy(E)> from the flux
    !  itself, cancelling any effect of the energy diffusion on the mean
    !  stratification.
    !--------------------------------------------------------------------
    if (modulo(stratified,2)==1) then
      do iz=izs,ize
      do iy=1,my
        call expn (mx, scr1(:,iy,iz), exp1)
        call expn (mx, scr2(:,iy,iz), exp2)
        do ix=1,mx
          scr3(ix,iy,iz) = -exp1(ix)*dymdn(iy)*exp2(ix)*scr3(ix,iy,iz)  ! = -ee*dy*rho*nu*ddydn(lne) = diffusive energy flux
          scr6(ix,iy,iz) = scr3(ix,iy,iz)
        end do
      end do
      end do
      call smooth (scr6, 2)                                             ! to retain some small scale diffusion on the mean
      call dumpn(scr3, 'Fe(diffuse)', 'e.dmp', 1)
      call dumpn(scr6, 'Fe smoothed', 'e.dmp', 1)
      call haverage_subr (scr6, eeav)                                   ! horizontally averaged ditto
      do iz=izs,ize
      do iy=1,my
        do ix=1,mx
          scr3(ix,iy,iz) = scr3(ix,iy,iz) - eeav(iy)                    ! = diffusive flux - average
        end do
      end do
      end do
      call dumpn(scr3, 'dflux_y', 'e.dmp', 1)
    !--------------------------------------------------------------------
    ! Or else, just compute the flux. This is simpler, but does
    ! not give exact cancellation, because of the weighting factors.
    !--------------------------------------------------------------------
    else
      do iz=izs,ize
      do iy=1,my
        call expn (mx, scr1(:,iy,iz), exp1)
        call expn (mx, scr2(:,iy,iz), exp2)
        do ix=1,mx
          scr3(ix,iy,iz) = -exp1(ix)*dymdn(iy)*exp2(ix)*scr3(ix,iy,iz)  ! = -dy*ee*rho*nu*ddydn(lnee-log(<ee>)) = diffusive energy flux
        end do
      end do
      end do
      call dumpn(scr3, 'dflux_y', 'e.dmp', 1)
    end if
    if (do_fluxes) call haverage_subr (scr3, fedif)
    call dumpn(scr3, 'eflux_y', 'e.dmp', 1)
  else                                                                ! Not do_stratified
    call ddydn_set (lne, scr3)
    do iz=izs,ize
    do iy=1,my
      call expn (mx, scr1(:,iy,iz), exp1)                            ! ln(ee)
      call expn (mx, scr2(:,iy,iz), exp2)                            ! ln(rho*nu)
      do ix=1,mx
        scr3(ix,iy,iz) = - exp1(ix)*dymdn(iy)*exp2(ix)*scr3(ix,iy,iz)   ! = -dy*ee*rho*nu*ddydn(lnee) = diffusive energy flux
      end do
    end do
    end do
    if (do_fluxes) call haverage_subr (scr3, fedif)
    call dumpn(scr3, 'eflux_y', 'e.dmp', 1)
  end if

!-----------------------------------------------------------------------
!  Add the internal energy flux, using double precision in interpolation
!-----------------------------------------------------------------------
  allocate (dbl1(mx,my,mz), dbl2(mx,my,mz), dbl3(mx,my,mz))
  do iz=izs,ize
  do iy=1,my
  do ix=1,mx
    dbl1(ix,iy,iz) = log(real(ee(ix,iy,iz),kind=8))
  end do
  end do
  end do
  if (do_2nddiv) then
    call ydn1_set_r8 (dbl1, dbl2)
  else
    call ydn_set_r8 (dbl1, dbl2)
  endif
  do iz=izs,ize
  do iy=1,my
    do ix=1,mx
      dbl1(ix,iy,iz) = scr3(ix,iy,iz) &
                     + exp(dbl2(ix,iy,iz))*py(ix,iy,iz)                ! add internal energy flux
    end do
  end do
  end do

!-----------------------------------------------------------------------
!  Energy equation; subtract y-derivative of y-flux.  We come here with
!  the vertical energy flux in dbl1
!-----------------------------------------------------------------------
  call ddyup_set_r8  (dbl1, dbl2)
  call ddyup1_set_r8 (dbl1, dbl3)
  do iz=izs,ize
    where (d2lnr(:,:,iz) > d2lnr_lim .or. do_2nddiv)
      dbl2(:,:,iz) = dbl3(:,:,iz)
    endwhere
  end do
  do iz=izs,ize
    dedt(:,:,iz) = dedt(:,:,iz) - dbl2(:,:,iz)
  end do
    call dumpn(real(dbl1), 'eflux_y r*Uy^2+visc', 'e.dmp', 1)
    call dumpn(scr3, 'eflux_y visc', 'e.dmp', 1)
    call dumpn(real(dbl1), 'eflux_y r*Uy^2+visc', 'py.dmp', 1)
    call dumpn(scr3, 'eflux_y visc', 'py.dmp', 1)
    call dumpn(dedt,'dedt_y','e.dmp',1)
    call dumpn(dedt, 'dedt_y', 'py.dmp', 1)
  deallocate (dbl1, dbl2, dbl3)

  do iz=izs,ize
  do iy=1,my
  do ix=1,mx
    lnu(ix,iy,iz) = alog(nuE*max(nu(ix,iy,iz)+dzm(iz)*nud(ix,iy,iz),1e-30))
  end do
  end do
  end do

!-----------------------------------------------------------------------
!  Energy equation; z-advection and diffusion
!-----------------------------------------------------------------------
  call barrier_omp('pde23')                                             ! barrier 17, lne??? (quench)
  call ddzdn_set (lne, scr3)
  if (do_quench) then
    do iz=izs,ize                                          ! store unquenched bdry values
      do iy=1,2*(my-ub)
        qbdry(:,iy,iz)=scr3(:,ub-(my-ub)+iy,iz)
      end do
    end do
    call quenchz1 (scr3)
    do iz=izs,ize                                          ! restore unquenched bdry values
      scr3(:,ub-(my-ub)+1:my,iz)=qbdry(:,1:2*(my-ub),iz)
    end do
  end if
  call zdn1_set (lnu, scr1)
  if (do_2nddiv) then
    call zdn1_set (lne, scr2)
  else
    call zdn_set (lne, scr2)
  end if
  do iz=izs,ize
  do iy=1,my
    call expn (mx, scr2(:,iy,iz), exp1)
    call expn (mx, scr1(:,iy,iz), exp2)
    do ix=1,mx
      scr3(ix,iy,iz) = exp1(ix)*(pz(ix,iy,iz) - dzmdn(iz)*exp2(ix)*scr3(ix,iy,iz))
    end do
  end do
  end do

  call barrier_omp('pde26')                                             ! barrier 20, scr3 set above
  call ddzup_set  (scr3, scr1)
  call ddzup1_set (scr3, scr6)
  do iz=izs,ize
    where (d2lnr(:,:,iz) > d2lnr_lim .or. do_2nddiv)
      scr1(:,:,iz) = scr6(:,:,iz)
    endwhere
  end do

  do iz=izs,ize
    dedt(:,:,iz) = dedt(:,:,iz) - scr1(:,:,iz)
  end do
    call dumpn(dedt, 'dedt>zadvdif', 'e.dmp', 1)
                                                                        call timer('pde','e_adv+diff')
!-----------------------------------------------------------------------
!  Cooling and conduction
!-----------------------------------------------------------------------
    call dumpn(ee,'ee3','e.dmp',1)
  call coolit (r, ee, lne, dd, dedt)
    call dumpn(ee,'ee4','e.dmp',1)
    call dumpn (dedt, 'dedt>coolit', 'e.dmp', 1)
  call conduction (r, e, ee, Bx, By, Bz, dedt)
    call dumpn (dedt, 'dedt>cond', 'e.dmp', 1)
end if  ! if (do_energy)

!-----------------------------------------------------------------------
!  MHD
!-----------------------------------------------------------------------
    call dumpn (dpxdt, 'dpxdt<mhd', 'px.dmp', 1)
    call dumpn (dpydt, 'dpydt<mhd', 'py.dmp', 1)
    call dumpn (dpzdt, 'dpzdt<mhd', 'pz.dmp', 1)
  if (do_mhd) then
    call mhd (flag, &
            r,e,p,Ux,Uy,Uz,Bx,By,Bz,Ex,Ey,Ez, &
            dpxdt,dpydt,dpzdt,dedt,dBxdt,dBydt,dBzdt,frho)
      call dumpn (dpxdt, 'dpxdt>mhd', 'px.dmp', 1)
      call dumpn (dpydt, 'dpydt>mhd', 'py.dmp', 1)
      call dumpn (dpzdt, 'dpzdt>mhd', 'pz.dmp', 1)
      call dumpn (dedt, 'dedt>mhd', 'e.dmp', 1)
  end if

!-----------------------------------------------------------------------
!  Boundary conditions applied to time derivatives
!-----------------------------------------------------------------------
    call dumpn (dpydt, 'dpydt<ddt_bdry', 'py.dmp', 1)
    call dumpn (drdt,'drdt<ddt_bdry','r.dmp',1)
    call ddt_boundary (r,px,py,pz,e,p,Bx,By,Bz, &
                     drdt,dpxdt,dpydt,dpzdt, &
                     dedt,dBxdt,dBydt,dBzdt)
    call dumpn (drdt, 'drdt>ddt_bdry','r.dmp',1)
    call dumpn (dedt, 'dedt>ddt_bdry', 'e.dmp', 1)
    call dumpn (dpxdt,'dpxdt>ddt_bdry', 'px.dmp', 1)
    call dumpn (dpydt,'dpydt>ddt_bdry', 'py.dmp', 1)
    call dumpn (dpzdt,'dpzdt>ddt_bdry','pz.dmp',1)

                                                                        call timer('pde','ddt_bdry')
    call output_fluxes
                                                                        call timer('pde','out_flux')
    call calc_hav(r,Ux,Uy,Uz,e,p,px,py,pz,Bx,By,Bz,Ex,Ey,Ez)
                                                                        call timer('pde','calc_hav')
    call output_hav
                                                                        call timer('pde','write_hav')
  END SUBROUTINE pde3
