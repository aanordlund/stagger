! $Id: check.f90,v 1.28 2016/11/09 20:31:28 aake Exp $
!***********************************************************************
MODULE check_mod
  logical, save:: dumping = .false.                                     ! dump flag
END MODULE

!***********************************************************************
SUBROUTINE check_params
  USE params
  USE check_mod
  implicit none
  logical flag, do_io

  if (.not. (do_flags .or. do_io(t,tscr,iscr+iscr0,nscr))) return       ! check flags or not

  if (flag('stop')) then
    call timer('END','')
    call end_mpi
    stop
  end if

  if (flag('debug')) then                                               ! dump request
    do_dump = .true.                                                    ! turn on, regardless
    dumping = .true.                                                    ! dumping ...
    if (master) print*,'dumping turned on for one timestep'
  else
    call check_dumping                                                  ! turn off again?
  end if

  if (.not.flag('reread')) then                                         ! reread.flag file?
    return                                                              ! no, return
  end if

  close (stdin)                                                         ! yes, reread stdin
  open (stdin,file=inputfile)                                           ! open the same file

  if (master) print *,' '
  if (master) print *,'----------------------- new parameters --------------------------------'
  call read_params
  call read_boundary
  call read_slice
  call read_quench
  call read_eos
  call read_force
  call read_cooling
  call read_explode
  call flush_mpi
  if (master) print *,'-----------------------------------------------------------------------'
  if (master) print *,' '
END SUBROUTINE

!***********************************************************************
SUBROUTINE check_dumping
  USE params
  USE check_mod
  implicit none

  if (dumping) then
    do_dump = .false.                                                   ! turn off, regardless
    dumping = .false.                                                   ! .. done
    if (master) print*,'dumping turned off'
  end if
END

!***********************************************************************
LOGICAL FUNCTION do_io (time,test,itest,ntest)
  USE params
  implicit none
  real time, test, time1
  integer itest, ntest
  logical fexists

  do_io = .false.
  if (test > 0.) then
    time1 = test*itest - 0.001*dtold
    if (time >= time1) do_io = .true.
  else if (ntest > 0) then
    if (mod(it,ntest) == 0) do_io = .true.
  end if
  if (do_trace .and. master) print*,'do_io',time,test,dtold,itest,ntest,do_io
END

!***********************************************************************
LOGICAL FUNCTION foutput(filename)
  USE params
  implicit none
  logical fexists, do_io
  character(len=*) filename

  foutput = .false.
  if (did_io .and. isubstep==1) then
    foutput = fexists(filename)
  end if
END

!***********************************************************************
SUBROUTINE check_io (r,px,py,pz,e,d,Bx,By,Bz)
  USE params
  implicit none
  real, dimension(mx,my,mz):: r,px,py,pz,e,d,Bx,By,Bz
  logical do_io, flag, void

  did_io=.false.
  if (do_io (t, tsnap, isnap+isnap0, nsnap) .or. flag('snapshot')) then
    isnap  = isnap+1                                                    ! increment counter
    if (do_mhd .and. do_divb_clean) call divb_clean                     ! clean div(B) before save
    call write_snap (r,px,py,pz,e,d,Bx,By,Bz)                           ! write snapshot
    call write_particles (isnap)                                        ! write particles
    did_io = .true.
  end if
  if (flag('dump')) then
    iscr  = iscr+1                                                      ! increment counter
    call write_scr  (r,px,py,pz,e,d,Bx,By,Bz)                           ! write scr shot
    void = flag('stop')                                                 ! clear stop flag
    void = flag('restart')                                              ! clear restart flag
    tstop = t                                                           ! stop after this step
  end if
  if (do_io (t, tscr, iscr+iscr0, nscr) .or. flag('scratch')) then
    iscr  = iscr+1                                                      ! increment counter
    call write_scr  (r,px,py,pz,e,d,Bx,By,Bz)                           ! write scr shot
  end if

  if (master) then
    write (stat_unit,'(5g15.7)') t, E_kin, Q_kin, E_mag, Q_mag
    if (mod(it,20) == 0) then
      write (*,'(a,5g15.7)') 'joule:',t, E_kin, Q_kin, E_mag, Q_mag
    end if
    flush (stat_unit)
  end if
END SUBROUTINE
