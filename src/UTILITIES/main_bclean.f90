! $Id: main_bclean.f90,v 1.4 2003/05/09 00:53:19 aake Exp $
!-----------------------------------------------------------------------
PROGRAM bclean

  USE params
  USE stagger

  real, allocatable, dimension(:,:,:) :: bx, by, bz, ox, oy, oz, ax, ay, az, divb
  real, allocatable, dimension(:) :: sbx, sby, sbz
  character*64:: infile, outfile

  print *, 'infile, outfile, mx, my, mz, nvar, isnap ?'
  read *, infile, outfile, mx, my, mz, nvar, isnap
  if (outfile.eq.' ') outfile=infile

  allocate (ax(mx,my,mz), ay(mx,my,mz), az(mx,my,mz))
  allocate (bx(mx,my,mz), by(mx,my,mz), bz(mx,my,mz))
  allocate (ox(mx,my,mz), oy(mx,my,mz), oz(mx,my,mz))
  allocate (divb(mx,my,mz))
  allocate (sbx(mz), sby(mz), sbz(mz))
  mw = mx*my*mz
  lb = 1
  ub = my
  
  open (1, file=infile, form='unformatted', &
    access='direct', recl=lrec(mw))

  read (1,rec=(nvar-2)+nvar*isnap) bx
  read (1,rec=(nvar-1)+nvar*isnap) by
  read (1,rec=(nvar-0)+nvar*isnap) bz
  ox=bx; oy=by; oz=bz;

  close (1)

  dx = 1.
  dy = 1.
  dz = 1.

  do k=1,mz
    sbx(k) = sum(bx(:,:,k))
    sby(k) = sum(by(:,:,k))
    sbz(k) = sum(bz(:,:,k))
  end do

  print 1, bx(1:8,1:4,1)
1 format(2x,8g12.5)

  divb = ddxup(bx) + ddyup(by) + ddzup(bz)
  print *,'rms(div(B)) =', sqrt(average(divb**2))

  bx = xup(bx)
  by = yup(by)
  bz = zup(bz)

  call vectorpot (bx,by,bz,ax,ay,az,mx,my,mz,dx,dy,dz)

  ax = ydn(zdn(ax))
  ay = zdn(xdn(ay))
  az = xdn(ydn(az))

  bx = ddyup(az)-ddzup(ay) + sum(sbx)/mw
  by = ddzup(ax)-ddxup(az) + sum(sby)/mw
  bz = ddxup(ay)-ddyup(ax) + sum(sbz)/mw

  print 1, bx(1:8,1:4,1)

  divb = ddxup(bx) + ddyup(by) + ddzup(bz)
  print *,'rms(div(B)) =', sqrt(average(divb**2))
  print *,'rms(dB)=', sqrt(average((bx-ox)**2+(by-oy)**2+(bz-oz)**2))


  open (2, file=outfile, form='unformatted', &
    status='unknown', access='direct', recl=lrec(mw))
  
  if (infile .eq. outfile) then
    istart=(nvar-3)+nvar*isnap
  else
    istart=0
  end if

  write (2,rec=istart+1) bx
  write (2,rec=istart+2) by
  write (2,rec=istart+3) bz

  close (2)

END
