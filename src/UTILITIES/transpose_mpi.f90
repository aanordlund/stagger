! $Id: transpose_mpi.f90,v 1.8 2007/10/21 22:58:11 aake Exp $
!***********************************************************************
MODULE transpose_m
  USE params, only: mx, my, mz, izs, ize, dbg_mpi, mpi_rank
  USE mpi_mod
  implicit none
  !include 'mpif.h'
  real, allocatable, dimension(:,:,:) :: a1, a2
  logical, save:: first_time=.true.
CONTAINS

!***********************************************************************
SUBROUTINE init_transpose
  implicit none
  logical debug
!-----------------------------------------------------------------------
!$omp master
  if (debug(dbg_mpi)) print *, mpi_rank, 'init_transpose'
  allocate (a1(my,my,mz), a2(my,my,mz))
  first_time = .false.
!$omp end master
END SUBROUTINE

!***********************************************************************
SUBROUTINE exchange (a1, a2, i1)
  USE params, only: mx, my, mz
  USE mpi_mod
  implicit none
  real, dimension(my,my,mz):: a1, a2
  integer i1, i2, iz
  integer req1, req2, status(MPI_STATUS_SIZE), tag1, tag2
  logical debug
!-----------------------------------------------------------------------
  i2 = mpi_rank
  tag1 = i1+10*i2
  tag2 = i2+10*i1
  if (i2 == i1) then
    if (debug(dbg_mpi)) print 1, mpi_rank, ' copying piece', tag1,' locally on',i1,a1(1,1,1)
    do iz=izs,ize
      a2(:,:,iz) = a1(:,:,iz)
    end do
!$omp barrier
  else
!$omp barrier
!$omp master
    call MPI_ISEND (a1, my*my*mz, MPI_REAL, i1, tag1, MPI_COMM_WORLD, req1, mpi_err)
    call MPI_IRECV (a2, my*my*mz, MPI_REAL, i1, tag2, MPI_COMM_WORLD, req2, mpi_err)
    call MPI_WAIT (req1, status, mpi_err)
    if (debug(dbg_mpi)) print 1, mpi_rank, ' sending piece', tag1, ' to processor', i1, a1(1,1,1)
    call MPI_WAIT (req2, status, mpi_err)
    if (debug(dbg_mpi)) print 1, mpi_rank, ' recving piece', tag2, ' fr processor', i1, a2(1,1,1), mpi_err
!$omp end master
!$omp barrier
  end if
1 format(i13,a,i3.2,a,i3,f10.0,2i10)
END SUBROUTINE

!-----------------------------------------------------------------------
END MODULE

!***********************************************************************
SUBROUTINE transpose (f1, f2)
  USE params, only: mx, my, mz, mid, mpi_ny
  USE transpose_m
  implicit none
  real, dimension(mx,my,mz):: f1, f2
  intent(in)  :: f1
  intent(out) :: f2
  integer i1, i2, ix, iy, iz, jx
  character(len=mid):: id="$Id: transpose_mpi.f90,v 1.8 2007/10/21 22:58:11 aake Exp $"
!-----------------------------------------------------------------------
  call print_id (id)
  if (first_time) call init_transpose

!$omp barrier
  do i1=0,mpi_ny-1
    jx = i1*my
    do iz=izs,ize; do iy=1,my; do ix=1,my
      a1(ix,iy,iz) = f1(ix+jx,iy,iz)
    end do; end do; end do
    call exchange (a1, a2, i1)
    do iz=izs,ize; do iy=1,my; do ix=1,my
      f2(ix+jx,iy,iz) = a2(iy,ix,iz)
    end do; end do; end do
  end do
!$omp barrier
END SUBROUTINE

!***********************************************************************
SUBROUTINE test_transpose (fft1, fft2)
  USE params, only: mx, my, mz, izs, ize, mpi_x, mpi_y, mpi_z, mpi_rank
  implicit none
  real, dimension(mx,my,mz):: fft1, fft2
  integer ix, iy, iz, jx, jy, jz
  real tmp
  logical ok
  integer, parameter:: mprint=10
  integer nprint

  call barrier_mpi ('transpose start')

!$omp parallel private(ix,iy,iz,jx,jy,jz,tmp,nprint,ok)
  nprint = 0
  do iz=izs,ize
    jz = iz+mpi_z*mz
    do iy=1,my
      jy = iy+mpi_y*my
      do ix=1,mx
        jx = ix+mpi_x*mx
        fft1(ix,iy,iz) = jx+1000*(jy+1000*jz)
      end do
    end do
  end do
  call transpose (fft1, fft2)
  ok = .true.
  do iz=izs,ize
    jz = iz+mpi_z*mz
    do iy=1,my
      jy = iy+mpi_y*my
      do ix=1,mx
        jx = ix+mpi_x*mx
        tmp = jy+1000*(jx+1000*jz)
	if (abs(tmp-fft2(ix,iy,iz)) > 1e-2) then
	  if (nprint < mprint) print 1, mpi_rank, ix, iy, iz, tmp, fft2(ix,iy,iz)
	  nprint = nprint+1
	  ok = .false.
	end if
      end do
    end do
  end do
  print *, mpi_rank, 'test_transpose', ok, nprint
!$omp end parallel

  call barrier_mpi ('transpose end')
1 format(i6,3i4,2f10.0)
END SUBROUTINE
