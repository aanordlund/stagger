! $Id: test_power3d.f90,v 1.3 2006/04/15 22:16:52 aake Exp $
!-----------------------------------------------------------------------
PROGRAM test_power3d
  implicit none
  integer mx, my, mz, mk
  real, allocatable, dimension(:,:,:) :: fx, fy, fz, tx, ty, tz
  real, allocatable, dimension(:) :: pkxt, pkyt, pkzt, pkxs, pkys, pkzs, xk
  integer, allocatable, dimension(:) :: nk
  integer ix, iy, iz, izs, ize, omp_get_thread_num, omp_get_num_threads
  real, parameter:: pi=3.14159265
  real ptot
  real dtime, cpu(2), void

  mx = 512
  my = mx
  mz = mx
  mk = mx/2

  allocate (fx(mx,my,mz), fy(mx,my,mz), fz(mx,my,mz))
  allocate (tx(mx,my,mz), ty(mx,my,mz), tz(mx,my,mz))

  allocate (xk(mk), nk(mk))
  allocate (pkxt(mk), pkyt(mk), pkzt(mk), pkxs(mk), pkys(mk), pkzs(mk))

  void = dtime(cpu)
!$omp parallel
  izs = 1 + ((omp_get_thread_num()  )*mz)/omp_get_num_threads()
  ize =     ((omp_get_thread_num()+1)*mz)/omp_get_num_threads()
  do iz=izs,ize; do iy=1,my; do ix=1,mx
    fx(ix,iy,iz) = 1.+cos((iz-1)*2.*pi/mz)+cos(2.*(iz-1)*2.*pi/mz)+cos((ix-1)*2.*pi/mx)*cos((iy-1)*2.*pi/my)
  end do; end do; end do
  call fft3df (fx, tx, mx, my, mz)
!$omp end parallel
  print *,tx(1,1,1:5),tx(2,2,1)

!$omp parallel
  izs = 1 + ((omp_get_thread_num()  )*mz)/omp_get_num_threads()
  ize =     ((omp_get_thread_num()+1)*mz)/omp_get_num_threads()
  do iz=izs,ize
    if (iz.eq.1) then
      fx(:,:,iz) = mz/2
      fy(:,:,iz) = mz/2
      fz(:,:,iz) = mz/2
    else
      fx(:,:,iz) = 0.
      fy(:,:,iz) = 0.
      fz(:,:,iz) = 0.
    end if
  end do
  if (omp_get_thread_num() == 0) print *,'setup:',dtime(cpu)
  
  call fft3df (fx, tx, mx, my, mz)
  call fft3df (fy, ty, mx, my, mz)
  call fft3df (fz, tz, mx, my, mz)
  if (omp_get_thread_num() == 0) print *,'fft:',dtime(cpu)

  call power3d (tx, mx, my, mz, pkxt, xk, nk, mk, ptot)
  call power3d (ty, mx, my, mz, pkyt, xk, nk, mk, ptot)
  call power3d (tz, mx, my, mz, pkzt, xk, nk, mk, ptot)
  if (omp_get_thread_num() == 0) print *,'power:',dtime(cpu)

  call helmht (tx, ty, tz, mx, my, mz)
  if (omp_get_thread_num() == 0) print *,'helmht:',dtime(cpu)

  call power3d (tx, mx, my, mz, pkxs, xk, nk, mk, ptot)
  call power3d (ty, mx, my, mz, pkys, xk, nk, mk, ptot)
  call power3d (tz, mx, my, mz, pkzs, xk, nk, mk, ptot)
  if (omp_get_thread_num() == 0) print *,'power:',dtime(cpu)

  !call fft3db (tx, fx, mx, my, mz)
  !call fft3db (ty, fy, mx, my, mz)
  !call fft3db (tz, fz, mx, my, mz)
!$omp end parallel

  print *,'relative power'
  print 1,pkxs/(pkxt+1e-10); print 1,pkys/(pkyt+1e-10); print 1,pkzs/(pkzt+1e-10)
1 format(20f6.3)

END
