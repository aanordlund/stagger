! $Id: main_helmh.f90,v 1.3 2016/11/09 22:04:58 aake Exp $
!-----------------------------------------------------------------------
PROGRAM main_helmh

  integer mx, my, mz, mvar, ivar, isnap, jvar, jsnap, io_word
  real, allocatable, dimension(:,:,:) :: fx, fy, fz
  character*64:: infile, outfile
  namelist /io/mx,my,mz,infile,ivar,isnap,mvar,outfile,jvar,jsnap

  read (*,io)
  if (outfile.eq.' ') outfile=infile
  write (*,io)

  allocate (fx(mx,my,mz), fy(mx,my,mz), fz(mx,my,mz))
  
  lrec = mx*my*io_word()

  open (1, file=infile, form='unformatted', &
    access='direct', recl=lrec)
  read (1,rec=iz+(0+ivar+isnap*mvar)*mz) (fx(:,:,iz), iz=1,mz)
  read (1,rec=iz+(1+ivar+isnap*mvar)*mz) (fy(:,:,iz), iz=1,mz)
  read (1,rec=iz+(2+ivar+isnap*mvar)*mz) (fz(:,:,iz), iz=1,mz)
  close (1)

  call helmh (fx, fy, fz, mx, my, mz)

  open (2, file=outfile, form='unformatted', &
    access='direct', recl=lrec, status='unknown')
  write (1,rec=iz+(0+ivar+isnap*mvar)*mz) (fx(:,:,iz), iz=1,mz)
  write (1,rec=iz+(1+ivar+isnap*mvar)*mz) (fy(:,:,iz), iz=1,mz)
  write (1,rec=iz+(2+ivar+isnap*mvar)*mz) (fz(:,:,iz), iz=1,mz)
  close (2)

END
