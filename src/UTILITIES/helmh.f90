SUBROUTINE helmh (fx, fy, fz, mx, my, mz)
  implicit none
  integer mx, my, mz
  real, dimension(mx,my,mz):: fx, fy, fz
  real, allocatable, dimension(:,:,:) :: tx, ty, tz
  integer i, j, k, izs, ize, omp_get_thread_num, omp_get_num_threads
  real kx, ky, kz, corr
  real dtime, cpu(2)

  allocate (tx(mx,my,mz), ty(mx,my,mz), tz(mx,my,mz))

!$omp parallel private(k,kz,j,ky,i,kx,corr)
  call fft3df (fx, tx, mx, my, mz)
  call fft3df (fy, ty, mx, my, mz)
  call fft3df (fz, tz, mx, my, mz)
  if (omp_get_thread_num() == 0) print *, 'fft:', dtime(cpu)

  izs = 1 + ((omp_get_thread_num()  )*mz)/omp_get_num_threads()
  ize =     ((omp_get_thread_num()+1)*mz)/omp_get_num_threads()
  do k=izs,ize
    kz = k/2
    do j=1,my
      ky = j/2
      do i=1,mx
        kx = i/2
        corr = tx(i,j,k)*kx + ty(i,j,k)*ky + tz(i,j,k)*kz
        corr = corr/(kx**2+ky**2+kz**2+1e-10)    
        tx(i,j,k) = tx(i,j,k) - corr*kx
        ty(i,j,k) = ty(i,j,k) - corr*ky
        tz(i,j,k) = tz(i,j,k) - corr*kz
      end do
    end do
  end do
!$omp barrier
  if (omp_get_thread_num() == 0) print *, 'proj:', dtime(cpu)

  call fft3db (tx, fx, mx, my, mz)
  call fft3db (ty, fy, mx, my, mz)
  call fft3db (tz, fz, mx, my, mz)
!$omp end parallel
  print *, 'fft:', dtime(cpu)

  deallocate (tx, ty, tz)
END
