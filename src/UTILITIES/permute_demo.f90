! $Id: permute_demo.f90,v 1.4 2012/12/31 16:19:17 aake Exp $

!*******************************************************************************
MODULE mpi_mod                                                                  ! MPI module
  implicit none
  include 'mpif.h'
  integer mpi_rank,mpi_size,mpi_err,status(MPI_STATUS_SIZE),cart                ! MPI parameters
  integer, dimension(3):: mpi_dim,mpi_coord                                     ! MPI 3-D arrays
END MODULE

!*******************************************************************************
MODULE permute_mod                                                              ! permute module
  implicit none
  real, allocatable, dimension(:,:,:,:):: snd                                   ! send buffer
  real, allocatable, dimension(:,:,:):: rcv                                     ! recieve buffer
  integer, allocatable, dimension(:):: sndrq                                    ! send requests
  integer, allocatable, dimension(:,:):: snd_status                             ! send status
  integer nx,ny,nz,m                                                            ! box parameters
  logical, save:: first_time=.true.                                             ! init flag
END MODULE

!*******************************************************************************
SUBROUTINE init_permute (mx,my,mz)
  USE mpi_mod
  USE permute_mod
  implicit none
  integer mx,my,mz
  character(len=mid):: id="$Id: permute_demo.f90,v 1.4 2012/12/31 16:19:17 aake Exp $"
!-------------------------------------------------------------------------------
  if (mpi_rank == 0) print *,id
  if (mx*mpi_dim(1) .ne. my*mpi_dim(2) .or. &                                   ! check dimensions
      mx*mpi_dim(1) .ne. mz*mpi_dim(3)) then
    print *,'Incompatible total dimensions:',mx*mpi_dim(1),my*mpi_dim(2),mz*mpi_dim(3)
    call MPI_FINALIZE(mpi_err)
    stop
  end if
  m = min(mx,my,mz)                                                             ! smallest dimension
  nx = mx/m                                                                     ! number of boxes in x
  ny = my/m                                                                     ! number of boxes in y
  nz = mz/m                                                                     ! number of boxes in z
  if (mpi_rank == 0) then
    print *,'Box dimension:',m
    print *,'Number of boxes:',nx,ny,nz
  end if
  allocate (sndrq(nx*ny*nz))                                                    ! send requests
  allocate (snd(m,m,m,nx*ny*nz))                                                ! send buffer
  allocate (snd_status(MPI_STATUS_SIZE,nx*ny*nz))                               ! status buffer
  allocate (rcv(m,m,m))                                                         ! recv buffer
  first_time = .false.                                                          ! init done
END SUBROUTINE

!*******************************************************************************
SUBROUTINE end_permute
  USE permute_mod
  implicit none
  deallocate (sndrq,snd,snd_status,rcv)
END SUBROUTINE

!*******************************************************************************
SUBROUTINE permute(mx,my,mz,f1,f2)
  USE mpi_mod, only: mpi_rank,mpi_dim,mpi_coord,mpi_err,cart,status,mpi_real
  USE permute_mod
  implicit none
  integer mx,my,mz,nreq
  real, dimension(mx,my,mz):: f1,f2
  intent(in)  :: f1
  intent(out) :: f2
  integer ix,iy,iz,jx,jy,jz,kx,ky,kz,coord(3),tag,rank,req
!-------------------------------------------------------------------------------
  if (first_time) call init_permute(mx,my,mz)                                   ! initialize
  nreq = 0                                                                      ! reset request counter
  kx = mpi_coord(1)*mx                                                          ! global x-index of domain
  ky = mpi_coord(2)*my                                                          ! global y-index of domain
  kz = mpi_coord(3)*mz                                                          ! global z-index of domain

  do jz=0,mz-1,m; do jy=0,my-1,m; do jx=0,mx-1,m                                ! SEND
    coord = (/ (jy+ky)/mx, (jz+kz)/my, (jx+kx)/mz /)                            ! receiver MPI-coordinates
    call MPI_CART_RANK (cart, coord, rank, mpi_err)                             ! who to send to
    tag = (jx+kx)/m + (my*mpi_dim(2))*((jy+ky)/m + (mz*mpi_dim(3))*(jz+kz)/m)   ! package tag
    nreq = nreq+1                                                               ! increment request counter
    do iz=1,m; do iy=1,m; do ix=1,m                                             ! copy to ..
      snd(iy,iz,ix,nreq) = f1(ix+jx,iy+jy,iz+jz)                                ! .. send buffer
    end do; end do; end do
    !print *,'SEND:', mpi_rank,rank,tag
    call MPI_ISSEND (snd(1,1,1,nreq), m*m*m, MPI_REAL, rank, tag, &             ! start send to her ..
                     cart, sndrq(nreq), mpi_err)                                ! .. saving rq
  end do; end do; end do
  call MPI_BARRIER (cart, mpi_err)                                              ! make sure all sent

  do jz=0,mz-1,m; do jy=0,my-1,m; do jx=0,mx-1,m                                ! RECIEVE
    coord = (/ (jz+kz)/mx, (jx+kx)/my, (jy+ky)/mz /)                            ! sender MPI-coordinates
    call MPI_CART_RANK (cart, coord, rank, mpi_err)                             ! who to rcv from
    tag = (jz+kz)/m + (my*mpi_dim(2))*((jx+kx)/m + (mz*mpi_dim(3))*(jy+ky)/m)   ! package tag
    !print *,'RECV:', mpi_rank,rank,tag
    call MPI_IRECV (rcv, m*m*m, MPI_REAL, rank, tag, cart, req, mpi_err)        ! recv from him ..
    call MPI_WAIT (req, status, mpi_err)                                        ! wait for completion
    do iz=1,m; do iy=1,m; do ix=1,m                                             ! copy from ..
      f2(ix+jx,iy+jy,iz+jz) = rcv(ix,iy,iz)                                     ! .. rcv buffer
    end do; end do; end do
  end do; end do; end do
  call MPI_WAITALL (nreq, sndrq, snd_status, mpi_err)                           ! wait on sndrq, to free sndrq
END SUBROUTINE

!*******************************************************************************
!SUBROUTINE test_permute
  USE mpi_mod
  USE permute_mod
  implicit none
  real, allocatable, dimension(:,:,:):: fft1, fft2
  integer mx,my,mz
  integer ix,iy,iz,jx,jy,jz
  real tmp
  logical ok,reorder,periodic
  integer, parameter:: mprint=10
  integer nprint
!-------------------------------------------------------------------------------

  call MPI_INIT (mpi_err)                                                       ! initialize
  call MPI_COMM_SIZE (mpi_comm_world, mpi_size, mpi_err)                        ! get size
  call MPI_COMM_RANK (mpi_comm_world, mpi_rank, mpi_err)                        ! get rank

  mx = 256                                                                      ! local x-dim
  my =  64                                                                      ! local y-dim
  mz = 128                                                                      ! local z-dim

  mpi_dim(1) = 1                                                                ! mpi x-size
  mpi_dim(2) = 4                                                                ! mpi u-size
  mpi_dim(3) = 2                                                                ! mpi z-size

  if (mpi_size .ne. product(mpi_dim)) then                                      ! check mpi_size
    if (mpi_rank == 0) then
      print *,'Number of MPI processes =',mpi_size
      print *,'should be               =',product(mpi_dim)
    end if
    call MPI_FINALIZE(mpi_err)
    stop
  end if

  periodic = .true.
  reorder = .false.
  CALL MPI_CART_CREATE (mpi_comm_world, 3, mpi_dim, periodic, reorder, cart,  mpi_err)
  CALL MPI_CART_COORDS (cart, mpi_rank, 3, mpi_coord, mpi_err)

  allocate (fft1(mx,my,mz), fft2(mx,my,mz))

  do iz=1,mz                                                                    ! tag input array
    jz = iz+mpi_coord(3)*mz
    do iy=1,my
      jy = iy+mpi_coord(2)*my
      do ix=1,mx
        jx = ix+mpi_coord(1)*mx
        fft1(ix,iy,iz) = jz+1000*(jy+1000*jx)
        fft2(ix,iy,iz) = jz+1000*(jy+1000*jx)
      end do
    end do
  end do

  call permute(mx,my,mz,fft1,fft2)

  ok = .true.
  nprint = 0
  do iz=1,mz                                                                    ! check permuted array
    jz = iz+mpi_coord(3)*mz
    do iy=1,my
      jy = iy+mpi_coord(2)*my
      do ix=1,mx
        jx = ix+mpi_coord(1)*mx
        tmp = jy+1000*(jx+1000*jz)
        if (abs(tmp-fft2(ix,iy,iz)) > 1e-2) then
          if (nprint < mprint) &
	    print '(i6,3i4,2f10.0)', mpi_rank, ix, iy, iz, tmp, fft2(ix,iy,iz)
          nprint = nprint+1
          ok = .false.
        end if
      end do
    end do
  end do
  print *, mpi_rank, 'test_permute', ok

  deallocate (fft1,fft2)

  call end_permute
  call MPI_FINALIZE(mpi_err)
END
