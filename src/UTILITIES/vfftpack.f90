module vfftpack

  private
  public ftrf,ftrb,fti

contains
  
function pimach(dummy)
  integer, parameter :: kr=kind(.0)
  real(kr), intent(in) :: dummy
  real(kr) :: pimach

  pimach = 3.14159265358979323846264338327950588_kr
!
return
end function
!=================================================================
!
!     fftpk, version 3, sept. 2000
!
!
!     single precision scalar version
!
  subroutine radb2 (ido,l1,cci,chi,wa1)
    integer, parameter :: kr=kind(.0)
    real(kr), dimension(:), intent(in) :: cci
    real(kr), dimension(:), intent(out) :: chi
    real(kr), dimension(:), intent(in) :: wa1
    integer, intent(in) :: ido,l1
!
    real(kr), dimension(ido,2,l1) :: cc
    real(kr), dimension(ido,l1,2) :: ch
    integer m,n,idp2,i,ic
!
    n = size(cci) 
    cc = reshape(cci,(/ido,2,l1/))
    ch(1,:,1) = cc(1,1,:)+cc(ido,2,:)
    ch(1,:,2) = cc(1,1,:)-cc(ido,2,:)
    idp2 = ido+2
    do i=3,ido,2
      ic = idp2-i
      ch(i-1,:,1) = cc(i-1,1,:)+cc(ic-1,2,:)
      ch(i,:,1) = cc(i,1,:)-cc(ic,2,:)
      ch(i-1,:,2) = wa1(i-2)*(cc(i-1,1,:)-cc(ic-1,2,:)) &
                      -wa1(i-1)*(cc(i,1,:)+cc(ic,2,:))
      ch(i,:,2) = wa1(i-2)*(cc(i,1,:)+cc(ic,2,:)) &
                      +wa1(i-1)*(cc(i-1,1,:)-cc(ic-1,2,:))
    enddo
    if (mod(ido,2) == 0) then
      ch(ido,:,1) = cc(ido,1,:)+cc(ido,1,:)
      ch(ido,:,2) = -(cc(1,2,:)+cc(1,2,:))
    endif
    chi = reshape(ch,(/n/))
  end subroutine
!=================================================================
!
!     fftpk, version 3, sept. 2000
!
!
!     single precision scalar version
!
  subroutine radb3 (ido,l1,cci,chi,wa1,wa2)
    integer, parameter :: kr=kind(.0)
    real(kr), dimension(:), intent(in) :: cci
    real(kr), dimension(:), intent(out) :: chi
    real(kr), dimension(:), intent(in) :: wa1,wa2
    integer, intent(in) :: ido,l1
!
    real(kr), parameter :: two=2._kr,three=3._kr
    real(kr), dimension(ido,3,l1) :: cc
    real(kr), dimension(ido,l1,3) :: ch
    real(kr) arg,taur,taui
    integer n,idp2,i,ic

    arg=two*pimach(.0_kr)/three
    taur=cos(arg)
    taui=sin(arg)
    n=size(cci)
    cc = reshape(cci,(/ido,3,l1/))
    ch(1,:,1) = cc(1,1,:)+two*cc(ido,2,:)
    ch(1,:,2) = cc(1,1,:)+two*(taur*cc(ido,2,:)-taui*cc(1,3,:))
    ch(1,:,3) = cc(1,1,:)+two*(taur*cc(ido,2,:)+taui*cc(1,3,:))
    idp2 = ido+2
    do i=3,ido,2
      ic = idp2-i
      ch(i-1,:,1) = cc(i-1,1,:)+(cc(i-1,3,:)+cc(ic-1,2,:))
      ch(i,:,1) = cc(i,1,:)+(cc(i,3,:)-cc(ic,2,:))
      ch(i-1,:,2) = &
         wa1(i-2)*((cc(i-1,1,:)+taur*(cc(i-1,3,:)+cc(ic-1,2,:))) &
         -(taui*(cc(i,3,:)+cc(ic,2,:)))) &
         -wa1(i-1)*((cc(i,1,:)+taur*(cc(i,3,:)-cc(ic,2,:))) &
         +(taui*(cc(i-1,3,:)-cc(ic-1,2,:))))
      ch(i,:,2) =  &
          wa1(i-2)*((cc(i,1,:)+taur*(cc(i,3,:)-cc(ic,2,:))) &
          +(taui*(cc(i-1,3,:)-cc(ic-1,2,:)))) &
          +wa1(i-1)*((cc(i-1,1,:)+taur*(cc(i-1,3,:)+cc(ic-1,2,:))) &
          -(taui*(cc(i,3,:)+cc(ic,2,:))))
      ch(i-1,:,3) =  &
          wa2(i-2)*((cc(i-1,1,:)+taur*(cc(i-1,3,:)+cc(ic-1,2,:))) &
          +(taui*(cc(i,3,:)+cc(ic,2,:)))) &
          -wa2(i-1)*((cc(i,1,:)+taur*(cc(i,3,:)-cc(ic,2,:))) &
          -(taui*(cc(i-1,3,:)-cc(ic-1,2,:))))
      ch(i,:,3) = &
          wa2(i-2)*((cc(i,1,:)+taur*(cc(i,3,:)-cc(ic,2,:))) &
          -(taui*(cc(i-1,3,:)-cc(ic-1,2,:)))) &
          +wa2(i-1)*((cc(i-1,1,:)+taur*(cc(i-1,3,:)+cc(ic-1,2,:))) &
          +(taui*(cc(i,3,:)+cc(ic,2,:))))
    enddo
    chi = reshape(ch,(/n/))
  end subroutine
!=================================================================
!
! fftpk, version 3, sept. 1990
!
!
!   single precision scalar version
!
  subroutine radb4 (ido,l1,cci,chi,wa1,wa2,wa3)
    integer, parameter :: kr=kind(.0)
    real(kr), dimension(:), intent(in) :: cci
    real(kr), dimension(:), intent(out) :: chi
    real(kr), dimension(:), intent(in) :: wa1,wa2,wa3
    integer, intent(in) :: ido,l1
!
    real(kr), parameter :: two=2._kr
    real(kr), dimension(ido,4,l1) :: cc
    real(kr), dimension(ido,l1,4) :: ch
    real(kr) sqrt2
    integer n,idp2,i,ic
!
    sqrt2 = sqrt(two)
    n=size(cci)
    cc = reshape(cci,(/ido,4,l1/))
!
    ch(1,:,3) = (cc(1,1,:)+cc(ido,4,:))-(cc(ido,2,:)+cc(ido,2,:))
    ch(1,:,1) = (cc(1,1,:)+cc(ido,4,:))+(cc(ido,2,:)+cc(ido,2,:))
    ch(1,:,4) = (cc(1,1,:)-cc(ido,4,:))+(cc(1,3,:)+cc(1,3,:))
    ch(1,:,2) = (cc(1,1,:)-cc(ido,4,:))-(cc(1,3,:)+cc(1,3,:))
    idp2 = ido+2
    do i=3,ido,2
      ic = idp2-i
       ch(i-1,:,1) = &
           (cc(i-1,1,:)+cc(ic-1,4,:))+(cc(i-1,3,:)+cc(ic-1,2,:))
       ch(i,:,1) = &
           (cc(i,1,:)-cc(ic,4,:))+(cc(i,3,:)-cc(ic,2,:))
       ch(i-1,:,2) = &
           wa1(i-2)*((cc(i-1,1,:)-cc(ic-1,4,:)) &
           -(cc(i,3,:)+cc(ic,2,:)))-wa1(i-1)*((cc(i,1,:)+cc(ic,4,:)) &
           +(cc(i-1,3,:)-cc(ic-1,2,:)))
       ch(i,:,2) = &
           wa1(i-2)*((cc(i,1,:)+cc(ic,4,:)) &
           +(cc(i-1,3,:)-cc(ic-1,2,:))) &
           +wa1(i-1)*((cc(i-1,1,:)-cc(ic-1,4,:)) &
           -(cc(i,3,:)+cc(ic,2,:)))
       ch(i-1,:,3) = &
          wa2(i-2)*((cc(i-1,1,:)+cc(ic-1,4,:)) &
          -(cc(i-1,3,:)+cc(ic-1,2,:))) &
          -wa2(i-1)*((cc(i,1,:)-cc(ic,4,:))-(cc(i,3,:)-cc(ic,2,:)))
       ch(i,:,3) = &
           wa2(i-2)*((cc(i,1,:)-cc(ic,4,:))-(cc(i,3,:)-cc(ic,2,:))) &
           +wa2(i-1)*((cc(i-1,1,:)+cc(ic-1,4,:)) &
           -(cc(i-1,3,:)+cc(ic-1,2,:)))
       ch(i-1,:,4) = &
           wa3(i-2)*((cc(i-1,1,:)-cc(ic-1,4,:)) &
           +(cc(i,3,:)+cc(ic,2,:))) &
           -wa3(i-1)*((cc(i,1,:)+cc(ic,4,:)) &
           -(cc(i-1,3,:)-cc(ic-1,2,:)))
       ch(i,:,4) = &
           wa3(i-2)*((cc(i,1,:)+cc(ic,4,:)) &
           -(cc(i-1,3,:)-cc(ic-1,2,:))) &
           +wa3(i-1)*((cc(i-1,1,:)-cc(ic-1,4,:)) &
           +(cc(i,3,:)+cc(ic,2,:)))
    enddo
    if (mod(ido,2)==0) then
      ch(ido,:,1) = (cc(ido,1,:)+cc(ido,3,:)) &
                      +(cc(ido,1,:)+cc(ido,3,:))
      ch(ido,:,2) = sqrt2*((cc(ido,1,:)-cc(ido,3,:)) &
                      -(cc(1,2,:)+cc(1,4,:)))
      ch(ido,:,3) = (cc(1,4,:)-cc(1,2,:))+(cc(1,4,:)-cc(1,2,:))
      ch(ido,:,4) = -sqrt2*((cc(ido,1,:)-cc(ido,3,:)) &
                      +(cc(1,2,:)+cc(1,4,:)))
    endif
    chi = reshape(ch,(/n/))
  end subroutine
!=================================================================
!
! fftpk, version 3, sept. 1990
!
!
!   single precision scalar version
!
  subroutine radb5 (ido,l1,cci,chi,wa1,wa2,wa3,wa4)
    integer, parameter :: kr=kind(.0)
    real(kr), dimension(:), intent(in) :: cci
    real(kr), dimension(:), intent(out) :: chi
    real(kr), dimension(:), intent(in) :: wa1,wa2,wa3,wa4
    integer, intent(in) :: ido,l1
!
    real(kr), parameter :: two=2._kr,five=5._kr
    real(kr), dimension(ido,5,l1) :: cc
    real(kr), dimension(ido,l1,5) :: ch
    real(kr) arg,darg,tr11,ti11,tr12,ti12
    integer :: n,idp2,i,ic
 
    n=size(cci)
    arg=two*pimach(.0_kr)/five
    darg=two*arg
    tr11=cos(arg)
    ti11=sin(arg)
    tr12=cos(darg)
    ti12=sin(darg)
!
    cc = reshape(cci,(/ido,5,l1/))
    ch(1,:,1) = cc(1,1,:)+two*(cc(ido,2,:)+cc(ido,4,:))
    ch(1,:,2) = cc(1,1,:)+two*(tr11*cc(ido,2,:)+tr12*cc(ido,4,:) &
                  -ti11*cc(1,3,:)-ti12*cc(1,5,:))
    ch(1,:,3) = cc(1,1,:)+two*(tr12*cc(ido,2,:)+tr11*cc(ido,4,:) &
                  -ti12*cc(1,3,:)+ti11*cc(1,5,:))
    ch(1,:,4) = cc(1,1,:)+two*(tr12*cc(ido,2,:)+tr11*cc(ido,4,:) &
                  +ti12*cc(1,3,:)-ti11*cc(1,5,:))
    ch(1,:,5) = cc(1,1,:)+two*(tr11*cc(ido,2,:)+tr12*cc(ido,4,:) &
                  +ti11*cc(1,3,:)+ti12*cc(1,5,:))
    idp2 = ido+2
    do i=3,ido,2
      ic = idp2-i
      ch(i-1,:,1) =  &
          cc(i-1,1,:)+(cc(i-1,3,:)+cc(ic-1,2,:))+(cc(i-1,5,:) &
          +cc(ic-1,4,:))
      ch(i,:,1) =  &
          cc(i,1,:)+(cc(i,3,:)-cc(ic,2,:))+(cc(i,5,:)-cc(ic,4,:))
      ch(i-1,:,2) =  &
          wa1(i-2)*((cc(i-1,1,:)+tr11*(cc(i-1,3,:)+cc(ic-1,2,:)) &
          +tr12*(cc(i-1,5,:)+cc(ic-1,4,:))) &
          -(ti11*(cc(i,3,:)+cc(ic,2,:))+ti12*(cc(i,5,:)+cc(ic,4,:)))) &
          -wa1(i-1)*((cc(i,1,:)+tr11*(cc(i,3,:)-cc(ic,2,:)) &
          +tr12*(cc(i,5,:)-cc(ic,4,:))) &
          +(ti11*(cc(i-1,3,:)-cc(ic-1,2,:)) &
          +ti12*(cc(i-1,5,:)-cc(ic-1,4,:))))
      ch(i,:,2) =  &
          wa1(i-2)*((cc(i,1,:)+tr11*(cc(i,3,:)-cc(ic,2,:)) &
          +tr12*(cc(i,5,:)-cc(ic,4,:))) &
          +(ti11*(cc(i-1,3,:)-cc(ic-1,2,:)) &
          +ti12*(cc(i-1,5,:)-cc(ic-1,4,:)))) &
          +wa1(i-1)*((cc(i-1,1,:)+tr11*(cc(i-1,3,:)+cc(ic-1,2,:)) &
          +tr12*(cc(i-1,5,:)+cc(ic-1,4,:))) &
          -(ti11*(cc(i,3,:)+cc(ic,2,:)) &
          +ti12*(cc(i,5,:)+cc(ic,4,:))))
      ch(i-1,:,3) =  &
          wa2(i-2)*((cc(i-1,1,:)+tr12*(cc(i-1,3,:)+cc(ic-1,2,:)) &
          +tr11*(cc(i-1,5,:)+cc(ic-1,4,:))) &
          -(ti12*(cc(i,3,:)+cc(ic,2,:)) &
          -ti11*(cc(i,5,:)+cc(ic,4,:)))) &
          -wa2(i-1)*((cc(i,1,:)+tr12*(cc(i,3,:)-cc(ic,2,:)) &
          +tr11*(cc(i,5,:)-cc(ic,4,:))) &
          +(ti12*(cc(i-1,3,:)-cc(ic-1,2,:)) &
          -ti11*(cc(i-1,5,:)-cc(ic-1,4,:))))
      ch(i,:,3) =  &
         wa2(i-2)*((cc(i,1,:)+tr12*(cc(i,3,:)-cc(ic,2,:)) & 
         +tr11*(cc(i,5,:)-cc(ic,4,:))) &
         +(ti12*(cc(i-1,3,:)-cc(ic-1,2,:)) &
         -ti11*(cc(i-1,5,:)-cc(ic-1,4,:)))) &
         +wa2(i-1)*((cc(i-1,1,:)+tr12*(cc(i-1,3,:)+cc(ic-1,2,:)) &
         +tr11*(cc(i-1,5,:)+cc(ic-1,4,:))) &
         -(ti12*(cc(i,3,:)+cc(ic,2,:)) &
         -ti11*(cc(i,5,:)+cc(ic,4,:))))
      ch(i-1,:,4) =  &
          wa3(i-2)*((cc(i-1,1,:)+tr12*(cc(i-1,3,:)+cc(ic-1,2,:)) &
          +tr11*(cc(i-1,5,:)+cc(ic-1,4,:))) &
          +(ti12*(cc(i,3,:)+cc(ic,2,:)) &
          -ti11*(cc(i,5,:)+cc(ic,4,:)))) &
          -wa3(i-1)*((cc(i,1,:)+tr12*(cc(i,3,:)-cc(ic,2,:)) &
          +tr11*(cc(i,5,:)-cc(ic,4,:))) &
          -(ti12*(cc(i-1,3,:)-cc(ic-1,2,:)) &
          -ti11*(cc(i-1,5,:)-cc(ic-1,4,:))))
      ch(i,:,4) =  &
          wa3(i-2)*((cc(i,1,:)+tr12*(cc(i,3,:)-cc(ic,2,:)) &
          +tr11*(cc(i,5,:)-cc(ic,4,:))) &
          -(ti12*(cc(i-1,3,:)-cc(ic-1,2,:)) &
          -ti11*(cc(i-1,5,:)-cc(ic-1,4,:)))) &
          +wa3(i-1)*((cc(i-1,1,:)+tr12*(cc(i-1,3,:)+cc(ic-1,2,:)) &
          +tr11*(cc(i-1,5,:)+cc(ic-1,4,:))) &
          +(ti12*(cc(i,3,:)+cc(ic,2,:))-ti11*(cc(i,5,:)+cc(ic,4,:))))
      ch(i-1,:,5) =  &
          wa4(i-2)*((cc(i-1,1,:)+tr11*(cc(i-1,3,:)+cc(ic-1,2,:)) &
          +tr12*(cc(i-1,5,:)+cc(ic-1,4,:))) &
          +(ti11*(cc(i,3,:)+cc(ic,2,:))+ti12*(cc(i,5,:)+cc(ic,4,:)))) &
          -wa4(i-1)*((cc(i,1,:)+tr11*(cc(i,3,:)-cc(ic,2,:)) &
          +tr12*(cc(i,5,:)-cc(ic,4,:))) &
          -(ti11*(cc(i-1,3,:)-cc(ic-1,2,:)) &
          +ti12*(cc(i-1,5,:)-cc(ic-1,4,:))))
      ch(i,:,5) =  &
          wa4(i-2)*((cc(i,1,:)+tr11*(cc(i,3,:)-cc(ic,2,:)) &
          +tr12*(cc(i,5,:)-cc(ic,4,:))) &
          -(ti11*(cc(i-1,3,:)-cc(ic-1,2,:)) &
          +ti12*(cc(i-1,5,:)-cc(ic-1,4,:)))) &
          +wa4(i-1)*((cc(i-1,1,:)+tr11*(cc(i-1,3,:)+cc(ic-1,2,:)) &
          +tr12*(cc(i-1,5,:)+cc(ic-1,4,:))) &
          +(ti11*(cc(i,3,:)+cc(ic,2,:))+ti12*(cc(i,5,:)+cc(ic,4,:))))
    enddo
    chi = reshape(ch,(/n/))
  end subroutine
!=================================================================
!
!     fftpk, version 3, sept. 2000
!
!
!  single precision scalar version
!
  subroutine radbg (ido,ip,l1,cci,chi,wa)
    integer, parameter :: kr=kind(0.0)
    real(kr), dimension(:), intent(inout) :: chi,cci
    real(kr), dimension(:), intent(in) :: wa
    integer, intent(in) :: ido,ip,l1
!
    real(kr), parameter :: zero=.0_kr,one=1._kr,two=2.0_kr
    real(kr), dimension(ido,l1,ip) :: ch,c1
    real(kr), dimension(ido,ip,l1) :: cc
    real(kr) arg,dcp,dsp,ar1,ai1,ar1h,dc2,ds2,ar2,ai2,ar2h
    integer n,ipph,ipp2,idp2,nbd,i,ic,is,idij,j,j2,jc,l,lc

    n = size(cci)
    ch = reshape(chi,(/ido,l1,ip/))
    cc = reshape(cci,(/ido,ip,l1/))

    arg = two*pimach(.0_kr)/real(ip,kr)
    dcp = cos(arg)
    dsp = sin(arg)
    idp2 = ido+2
    nbd = (ido-1)/2
    ipp2 = ip+2
    ipph = (ip+1)/2
!
    ch(:,:,1) = cc(:,1,:)
    do j=2,ipph
      jc = ipp2-j
      j2 = j+j
      ch(1,:,j) = cc(ido,j2-2,:)+cc(ido,j2-2,:)
      ch(1,:,jc) = cc(1,j2-1,:)+cc(1,j2-1,:)
    enddo
    if (ido >= 3) then
      do j=2,ipph
        jc = ipp2-j
        do i=3,ido,2
          ic = idp2-i
          ch(i-1,:,j) = cc(i-1,2*j-1,:)+cc(ic-1,2*j-2,:)
          ch(i-1,:,jc) = cc(i-1,2*j-1,:)-cc(ic-1,2*j-2,:)
          ch(i,:,j) = cc(i,2*j-1,:)-cc(ic,2*j-2,:)
          ch(i,:,jc) = cc(i,2*j-1,:)+cc(ic,2*j-2,:)
        enddo
      enddo
    endif
!
    c1 = reshape(cc,(/ido,l1,ip/))
    ar1 = one
    ai1 = zero
    do l=2,ipph
      lc = ipp2-l
      ar1h = dcp*ar1-dsp*ai1
      ai1 = dcp*ai1+dsp*ar1
      ar1 = ar1h
      c1(:,:,l) = ch(:,:,1)+ar1*ch(:,:,2)
      c1(:,:,lc) = ai1*ch(:,:,ip)
      dc2 = ar1
      ds2 = ai1
      ar2 = ar1
      ai2 = ai1
      do j=3,ipph
        jc = ipp2-j
        ar2h = dc2*ar2-ds2*ai2
        ai2 = dc2*ai2+ds2*ar2
        ar2 = ar2h
        c1(:,:,l) = c1(:,:,l)+ar2*ch(:,:,j)
        c1(:,:,lc) = c1(:,:,lc)+ai2*ch(:,:,jc)
      enddo
    enddo
    do j=2,ipph
      ch(:,:,1) = ch(:,:,1)+ch(:,:,j)
    enddo
    do j=2,ipph
      jc = ipp2-j
      ch(1,:,j) = c1(1,:,j)-c1(1,:,jc)
      ch(1,:,jc) = c1(1,:,j)+c1(1,:,jc)
    enddo
!
    if (ido /= 1) then
      do j=2,ipph
        jc = ipp2-j
        do i=3,ido,2
          ch(i-1,:,j) = c1(i-1,:,j)-c1(i,:,jc)
          ch(i-1,:,jc) = c1(i-1,:,j)+c1(i,:,jc)
          ch(i,:,j) = c1(i,:,j)+c1(i-1,:,jc)
          ch(i,:,jc) = c1(i,:,j)-c1(i-1,:,jc)
        enddo
      enddo
      c1(:,:,1) = ch(:,:,1)
      do j=2,ip
        c1(1,:,j) = ch(1,:,j)
      enddo
      is = -ido
      do j=2,ip
        is = is+ido
        idij = is
        do i=3,ido,2
          idij = idij+2
          c1(i-1,:,j) = wa(idij-1)*ch(i-1,:,j)-wa(idij)*ch(i,:,j)
          c1(i,:,j) = wa(idij-1)*ch(i,:,j)+wa(idij)*ch(i-1,:,j)
        enddo
      enddo
    endif
    cci = reshape(c1,(/n/))
    chi = reshape(ch,(/n/))
  end subroutine
!=================================================================
!
!     fftpk, version 3, sept. 2000
!
!
!     single precision scalar version
!
  subroutine radf2 (ido,l1,cci,chi,wa1)
    integer, parameter :: kr=kind(0.0)
    real(kr), dimension(:), intent(in) :: cci
    real(kr), dimension(:), intent(inout) :: chi
    real(kr), dimension(:), intent(in) :: wa1
    integer, intent(in) :: ido,l1
!
    real(kr), dimension(ido,2,l1) :: ch
    real(kr), dimension(ido,l1,2) :: cc
    integer n,idp2,i,ic
!
    n=size(cci)
    cc = reshape(cci,(/ido,l1,2/))
!
    ch(1,1,:) = cc(1,:,1)+cc(1,:,2)
    ch(ido,2,:) = cc(1,:,1)-cc(1,:,2)
    idp2 = ido+2
    do i=3,ido,2
      ic = idp2-i
      ch(i,1,:) = cc(i,:,1)+(wa1(i-2)*cc(i,:,2) &
                     -wa1(i-1)*cc(i-1,:,2))
      ch(ic,2,:) = (wa1(i-2)*cc(i,:,2)-wa1(i-1)*cc(i-1,:,2)) &
                       -cc(i,:,1)
      ch(i-1,1,:) = cc(i-1,:,1)+(wa1(i-2)*cc(i-1,:,2) &
                       +wa1(i-1)*cc(i,:,2))
      ch(ic-1,2,:) = cc(i-1,:,1)-(wa1(i-2)*cc(i-1,:,2) &
                       +wa1(i-1)*cc(i,:,2))
    enddo
    if (mod(ido,2)/=1) then
      ch(1,2,:) = -cc(ido,:,2)
      ch(ido,1,:) = cc(ido,:,1)
    endif
    chi = reshape(ch,(/n/))
  end subroutine
!=================================================================
!
!     fftpk, version 3, sept. 2000
!
!
!     single precision scalar version
!
  subroutine radf3 (ido,l1,cci,chi,wa1,wa2)
    integer, parameter :: kr=kind(0.)
    real(kr), dimension(:), intent(in) :: cci
    real(kr), dimension(:), intent(inout) :: chi
    real(kr), dimension(:), intent(in) :: wa1,wa2
    integer, intent(in) :: ido,l1
!
    real(kr), parameter :: two=2._kr,three=3._kr
    real(kr), dimension(ido,3,l1) :: ch
    real(kr), dimension(ido,l1,3) :: cc
    integer n,idp2,i,ic
    real(kr) arg,taur,taui

    n = size(cci)
    arg=two*pimach(.0_kr)/three
    taur=cos(arg) 
    taui=sin(arg)
!
    cc = reshape(cci,(/ido,l1,3/))
    ch(1,1,:) = cc(1,:,1)+(cc(1,:,2)+cc(1,:,3))
    ch(1,3,:) = taui*(cc(1,:,3)-cc(1,:,2))
    ch(ido,2,:) = cc(1,:,1)+taur*(cc(1,:,2)+cc(1,:,3))
    idp2 = ido+2
    do i=3,ido,2
      ic = idp2-i
      ch(i-1,1,:) = &
          cc(i-1,:,1)+((wa1(i-2)*cc(i-1,:,2)+wa1(i-1)*cc(i,:,2)) &
          +(wa2(i-2)*cc(i-1,:,3)+wa2(i-1)*cc(i,:,3)))
      ch(i,1,:) = &
          cc(i,:,1)+((wa1(i-2)*cc(i,:,2)-wa1(i-1)*cc(i-1,:,2)) &
          +(wa2(i-2)*cc(i,:,3)-wa2(i-1)*cc(i-1,:,3)))
      ch(i-1,3,:) = &
          (cc(i-1,:,1)+taur*((wa1(i-2)*cc(i-1,:,2) &
          +wa1(i-1)*cc(i,:,2))+(wa2(i-2)*cc(i-1,:,3) &
          +wa2(i-1)*cc(i,:,3))))+(taui*((wa1(i-2)*cc(i,:,2) &
          -wa1(i-1)*cc(i-1,:,2)) &
          -(wa2(i-2)*cc(i,:,3)-wa2(i-1)*cc(i-1,:,3))))
      ch(ic-1,2,:) = &
          (cc(i-1,:,1)+taur*((wa1(i-2)*cc(i-1,:,2) &
          +wa1(i-1)*cc(i,:,2))+(wa2(i-2)*cc(i-1,:,3) &
          +wa2(i-1)*cc(i,:,3))))-(taui*((wa1(i-2)*cc(i,:,2) &
          -wa1(i-1)*cc(i-1,:,2))-(wa2(i-2)*cc(i,:,3) &
          -wa2(i-1)*cc(i-1,:,3))))
      ch(i,3,:) = &
          (cc(i,:,1)+taur*((wa1(i-2)*cc(i,:,2)-wa1(i-1)*cc(i-1,:,2)) &
          +(wa2(i-2)*cc(i,:,3)-wa2(i-1)*cc(i-1,:,3)))) &
          +(taui*((wa2(i-2)*cc(i-1,:,3)+wa2(i-1)*cc(i,:,3)) &
          -(wa1(i-2)*cc(i-1,:,2)+wa1(i-1)*cc(i,:,2))))
      ch(ic,2,:) = &
          (taui*((wa2(i-2)*cc(i-1,:,3)+wa2(i-1)*cc(i,:,3)) &
          -(wa1(i-2)*cc(i-1,:,2)+wa1(i-1)*cc(i,:,2)))) &
          -(cc(i,:,1)+taur*((wa1(i-2)*cc(i,:,2)-wa1(i-1)*cc(i-1,:,2)) &
          +(wa2(i-2)*cc(i,:,3)-wa2(i-1)*cc(i-1,:,3))))
    enddo
    chi = reshape(ch,(/n/))
  end subroutine 
!=================================================================
!
! fftpk, version 3, sept. 2000
!
!
!    single precision scalar version
!
  subroutine radf4 (ido,l1,cci,chi,wa1,wa2,wa3)
    integer, parameter :: kr=kind(.0)
    real(kr), dimension(:), intent(in) :: cci
    real(kr), dimension(:), intent(inout) :: chi
    real(kr), dimension(:), intent(in) :: wa1,wa2,wa3
    integer, intent(in) :: ido,l1
!
    real(kr), parameter :: two=2._kr
    real(kr), dimension(ido,4,l1) :: ch
    real(kr), dimension(ido,l1,4) :: cc
    real(kr) hsqt2
    integer n,idp2,i,ic
!
    hsqt2=sqrt(two)/two
    n=size(cci)
!
    cc = reshape(cci,(/ido,l1,4/))
    ch(1,1,:) = (cc(1,:,2)+cc(1,:,4))+(cc(1,:,1)+cc(1,:,3))
    ch(ido,4,:) = (cc(1,:,1)+cc(1,:,3))-(cc(1,:,2)+cc(1,:,4))
    ch(ido,2,:) = cc(1,:,1)-cc(1,:,3)
    ch(1,3,:) = cc(1,:,4)-cc(1,:,2)
    idp2 = ido+2
    do i=3,ido,2
      ic = idp2-i
      ch(i-1,1,:) = &
          ((wa1(i-2)*cc(i-1,:,2)+wa1(i-1)*cc(i,:,2)) &
          +(wa3(i-2)*cc(i-1,:,4)+wa3(i-1)*cc(i,:,4))) &
          +(cc(i-1,:,1)+(wa2(i-2)*cc(i-1,:,3)+wa2(i-1)*cc(i,:,3)))
      ch(ic-1,4,:) = &
          (cc(i-1,:,1)+(wa2(i-2)*cc(i-1,:,3)+wa2(i-1)*cc(i,:,3))) &
          -((wa1(i-2)*cc(i-1,:,2)+wa1(i-1)*cc(i,:,2)) &
          +(wa3(i-2)*cc(i-1,:,4)+wa3(i-1)*cc(i,:,4)))
      ch(i,1,:) =  &
          ((wa1(i-2)*cc(i,:,2)-wa1(i-1)*cc(i-1,:,2)) &
          +(wa3(i-2)*cc(i,:,4)-wa3(i-1)*cc(i-1,:,4))) &
          +(cc(i,:,1)+(wa2(i-2)*cc(i,:,3)-wa2(i-1)*cc(i-1,:,3)))
      ch(ic,4,:) = & 
          ((wa1(i-2)*cc(i,:,2)-wa1(i-1)*cc(i-1,:,2)) &
          +(wa3(i-2)*cc(i,:,4)-wa3(i-1)*cc(i-1,:,4))) &
          -(cc(i,:,1)+(wa2(i-2)*cc(i,:,3)-wa2(i-1)*cc(i-1,:,3)))
      ch(i-1,3,:) = &
          ((wa1(i-2)*cc(i,:,2)-wa1(i-1)*cc(i-1,:,2)) &
          -(wa3(i-2)*cc(i,:,4)-wa3(i-1)*cc(i-1,:,4))) &
          +(cc(i-1,:,1)-(wa2(i-2)*cc(i-1,:,3)+wa2(i-1)*cc(i,:,3)))
      ch(ic-1,2,:) = &
          (cc(i-1,:,1)-(wa2(i-2)*cc(i-1,:,3)+wa2(i-1)*cc(i,:,3))) &
          -((wa1(i-2)*cc(i,:,2)-wa1(i-1)*cc(i-1,:,2)) &
          -(wa3(i-2)*cc(i,:,4)-wa3(i-1)*cc(i-1,:,4)))
      ch(i,3,:) =  &
          ((wa3(i-2)*cc(i-1,:,4)+wa3(i-1)*cc(i,:,4)) &
          -(wa1(i-2)*cc(i-1,:,2)+wa1(i-1)*cc(i,:,2))) &
          +(cc(i,:,1)-(wa2(i-2)*cc(i,:,3)-wa2(i-1)*cc(i-1,:,3)))
      ch(ic,2,:) = &
          ((wa3(i-2)*cc(i-1,:,4)+wa3(i-1)*cc(i,:,4)) &
          -(wa1(i-2)*cc(i-1,:,2)+wa1(i-1)*cc(i,:,2))) &
          -(cc(i,:,1)-(wa2(i-2)*cc(i,:,3)-wa2(i-1)*cc(i-1,:,3)))
    enddo
    if (mod(ido,2)/=1) then
      ch(ido,1,:) = (hsqt2*(cc(ido,:,2)-cc(ido,:,4)))+cc(ido,:,1)
      ch(ido,3,:) = cc(ido,:,1)-(hsqt2*(cc(ido,:,2)-cc(ido,:,4)))
      ch(1,2,:) = (-hsqt2*(cc(ido,:,2)+cc(ido,:,4)))-cc(ido,:,3)
      ch(1,4,:) = (-hsqt2*(cc(ido,:,2)+cc(ido,:,4)))+cc(ido,:,3)
    endif
    chi = reshape(ch,(/n/))
  end subroutine 
!=================================================================
!
! fftpk, version 3, sept. 2000
!
!
!   single precision scalar version
!
  subroutine radf5 (ido,l1,cci,chi,wa1,wa2,wa3,wa4)
    integer, parameter :: kr=kind(.0)
    real(kr), dimension(:), intent(in) :: cci
    real(kr), dimension(:), intent(inout) :: chi
    real(kr), dimension(:), intent(in) :: wa1,wa2,wa3,wa4
    integer, intent(in) :: ido,l1
!
    real(kr), parameter :: two=2._kr,five=5._kr
    real(kr), dimension(ido,l1,5) :: cc
    real(kr), dimension(ido,5,l1) :: ch
    real(kr) arg,darg,tr11,ti11,tr12,ti12
    integer n,idp2,i,ic

    arg=two*pimach(.0_kr)/five 
    darg=two*arg
    tr11=cos(arg)
    ti11=sin(arg)
    tr12=cos(darg)
    ti12=sin(darg)
    n=size(cci)
!
    cc = reshape(cci,(/ido,l1,5/))
    ch(1,1,:) = cc(1,:,1)+(cc(1,:,5)+cc(1,:,2)) &
                  +(cc(1,:,4)+cc(1,:,3))
    ch(ido,2,:) = cc(1,:,1)+tr11*(cc(1,:,5)+cc(1,:,2)) &
                    +tr12*(cc(1,:,4)+cc(1,:,3))
    ch(1,3,:) = ti11*(cc(1,:,5)-cc(1,:,2)) &
                  +ti12*(cc(1,:,4)-cc(1,:,3))
    ch(ido,4,:) = cc(1,:,1)+tr12*(cc(1,:,5)+cc(1,:,2)) &
                    +tr11*(cc(1,:,4)+cc(1,:,3))
    ch(1,5,:) = ti12*(cc(1,:,5)-cc(1,:,2)) &
                  -ti11*(cc(1,:,4)-cc(1,:,3))
    idp2 = ido+2
    do i=3,ido,2
      ic = idp2-i
      ch(i-1,1,:) = &
          cc(i-1,:,1)+((wa1(i-2)*cc(i-1,:,2)+wa1(i-1)*cc(i,:,2)) &
          +(wa4(i-2)*cc(i-1,:,5)+wa4(i-1)*cc(i,:,5))) &
          +((wa2(i-2)*cc(i-1,:,3)+wa2(i-1)*cc(i,:,3)) &
          +(wa3(i-2)*cc(i-1,:,4)+wa3(i-1)*cc(i,:,4)))
      ch(i,1,:) = &
          cc(i,:,1)+((wa1(i-2)*cc(i,:,2)-wa1(i-1)*cc(i-1,:,2)) &
          +(wa4(i-2)*cc(i,:,5)-wa4(i-1)*cc(i-1,:,5))) &
          +((wa2(i-2)*cc(i,:,3)-wa2(i-1)*cc(i-1,:,3)) &
          +(wa3(i-2)*cc(i,:,4)-wa3(i-1)*cc(i-1,:,4)))
      ch(i-1,3,:) = &
          cc(i-1,:,1)+tr11*(wa1(i-2)*cc(i-1,:,2)+wa1(i-1)*cc(i,:,2) &
         +wa4(i-2)*cc(i-1,:,5)+wa4(i-1)*cc(i,:,5)) &
         +tr12*(wa2(i-2)*cc(i-1,:,3)+wa2(i-1)*cc(i,:,3) &
         +wa3(i-2)*cc(i-1,:,4)+wa3(i-1)*cc(i,:,4)) &
         +ti11*(wa1(i-2)*cc(i,:,2)-wa1(i-1)*cc(i-1,:,2) &
         -(wa4(i-2)*cc(i,:,5)-wa4(i-1)*cc(i-1,:,5))) &
         +ti12*(wa2(i-2)*cc(i,:,3)-wa2(i-1)*cc(i-1,:,3) &
         -(wa3(i-2)*cc(i,:,4)-wa3(i-1)*cc(i-1,:,4)))
      ch(ic-1,2,:) = &
          cc(i-1,:,1)+tr11*(wa1(i-2)*cc(i-1,:,2)+wa1(i-1)*cc(i,:,2) &
          +wa4(i-2)*cc(i-1,:,5)+wa4(i-1)*cc(i,:,5)) &
          +tr12*(wa2(i-2)*cc(i-1,:,3)+wa2(i-1)*cc(i,:,3) &
          +wa3(i-2)*cc(i-1,:,4)+wa3(i-1)*cc(i,:,4)) &
          -(ti11*(wa1(i-2)*cc(i,:,2)-wa1(i-1)*cc(i-1,:,2) &
          -(wa4(i-2)*cc(i,:,5)-wa4(i-1)*cc(i-1,:,5))) &
          +ti12*(wa2(i-2)*cc(i,:,3)-wa2(i-1)*cc(i-1,:,3) &
          -(wa3(i-2)*cc(i,:,4)-wa3(i-1)*cc(i-1,:,4))))
      ch(i,3,:) = & 
          (cc(i,:,1)+tr11*((wa1(i-2)*cc(i,:,2)-wa1(i-1)*cc(i-1,:,2)) &
          +(wa4(i-2)*cc(i,:,5)-wa4(i-1)*cc(i-1,:,5))) &
          +tr12*((wa2(i-2)*cc(i,:,3)-wa2(i-1)*cc(i-1,:,3)) &
          +(wa3(i-2)*cc(i,:,4)-wa3(i-1)*cc(i-1,:,4)))) &
          +(ti11*((wa4(i-2)*cc(i-1,:,5)+wa4(i-1)*cc(i,:,5)) &
          -(wa1(i-2)*cc(i-1,:,2)+wa1(i-1)*cc(i,:,2))) &
          +ti12*((wa3(i-2)*cc(i-1,:,4)+wa3(i-1)*cc(i,:,4)) &
          -(wa2(i-2)*cc(i-1,:,3)+wa2(i-1)*cc(i,:,3))))
      ch(ic,2,:) = &
          (ti11*((wa4(i-2)*cc(i-1,:,5)+wa4(i-1)*cc(i,:,5)) &
          -(wa1(i-2)*cc(i-1,:,2)+wa1(i-1)*cc(i,:,2))) &
          +ti12*((wa3(i-2)*cc(i-1,:,4)+wa3(i-1)*cc(i,:,4)) &
          -(wa2(i-2)*cc(i-1,:,3)+wa2(i-1)*cc(i,:,3)))) &
          -(cc(i,:,1)+tr11*((wa1(i-2)*cc(i,:,2)-wa1(i-1)*cc(i-1,:,2)) &
          +(wa4(i-2)*cc(i,:,5)-wa4(i-1)*cc(i-1,:,5))) &
          +tr12*((wa2(i-2)*cc(i,:,3)-wa2(i-1)*cc(i-1,:,3)) &
          +(wa3(i-2)*cc(i,:,4)-wa3(i-1)*cc(i-1,:,4))))
      ch(i-1,5,:) = &
          (cc(i-1,:,1)+tr12*((wa1(i-2)*cc(i-1,:,2)+wa1(i-1)*cc(i,:,2)) &
          +(wa4(i-2)*cc(i-1,:,5)+wa4(i-1)*cc(i,:,5))) &
          +tr11*((wa2(i-2)*cc(i-1,:,3)+wa2(i-1)*cc(i,:,3)) &
          +(wa3(i-2)*cc(i-1,:,4)+wa3(i-1)*cc(i,:,4)))) &
          +(ti12*((wa1(i-2)*cc(i,:,2)-wa1(i-1)*cc(i-1,:,2)) &
          -(wa4(i-2)*cc(i,:,5)-wa4(i-1)*cc(i-1,:,5))) &
          -ti11*((wa2(i-2)*cc(i,:,3)-wa2(i-1)*cc(i-1,:,3)) &
          -(wa3(i-2)*cc(i,:,4)-wa3(i-1)*cc(i-1,:,4))))
      ch(ic-1,4,:) = &
          (cc(i-1,:,1)+tr12*((wa1(i-2)*cc(i-1,:,2)+wa1(i-1)*cc(i,:,2)) &
          +(wa4(i-2)*cc(i-1,:,5)+wa4(i-1)*cc(i,:,5))) &
          +tr11*((wa2(i-2)*cc(i-1,:,3)+wa2(i-1)*cc(i,:,3)) &
          +(wa3(i-2)*cc(i-1,:,4)+wa3(i-1)*cc(i,:,4)))) &
          -(ti12*((wa1(i-2)*cc(i,:,2)-wa1(i-1)*cc(i-1,:,2)) &
          -(wa4(i-2)*cc(i,:,5)-wa4(i-1)*cc(i-1,:,5))) &
          -ti11*((wa2(i-2)*cc(i,:,3)-wa2(i-1)*cc(i-1,:,3)) &
          -(wa3(i-2)*cc(i,:,4)-wa3(i-1)*cc(i-1,:,4))))
      ch(i,5,:) = &
          (cc(i,:,1)+tr12*((wa1(i-2)*cc(i,:,2)-wa1(i-1)*cc(i-1,:,2)) &
          +(wa4(i-2)*cc(i,:,5)-wa4(i-1)*cc(i-1,:,5))) &
          +tr11*((wa2(i-2)*cc(i,:,3)-wa2(i-1)*cc(i-1,:,3)) &
          +(wa3(i-2)*cc(i,:,4)-wa3(i-1)*cc(i-1,:,4)))) &
          +(ti12*((wa4(i-2)*cc(i-1,:,5)+wa4(i-1)*cc(i,:,5)) &
          -(wa1(i-2)*cc(i-1,:,2)+wa1(i-1)*cc(i,:,2))) &
          -ti11*((wa3(i-2)*cc(i-1,:,4)+wa3(i-1)*cc(i,:,4)) &
          -(wa2(i-2)*cc(i-1,:,3)+wa2(i-1)*cc(i,:,3))))
      ch(ic,4,:) = &
          (ti12*((wa4(i-2)*cc(i-1,:,5)+wa4(i-1)*cc(i,:,5)) &
          -(wa1(i-2)*cc(i-1,:,2)+wa1(i-1)*cc(i,:,2))) &
          -ti11*((wa3(i-2)*cc(i-1,:,4)+wa3(i-1)*cc(i,:,4)) &
          -(wa2(i-2)*cc(i-1,:,3)+wa2(i-1)*cc(i,:,3)))) &
          -(cc(i,:,1)+tr12*((wa1(i-2)*cc(i,:,2)-wa1(i-1)*cc(i-1,:,2)) &
          +(wa4(i-2)*cc(i,:,5)-wa4(i-1)*cc(i-1,:,5))) &
          +tr11*((wa2(i-2)*cc(i,:,3)-wa2(i-1)*cc(i-1,:,3)) &
          +(wa3(i-2)*cc(i,:,4)-wa3(i-1)*cc(i-1,:,4))))
    enddo
    chi = reshape(ch,(/n/))
  end subroutine
!=================================================================
!
!     fftpk, version 3, sept. 2000
!
!
!  single precision scalar version
!
  subroutine radfg (ido,ip,l1,cci,chi,wa)
    integer, parameter :: kr=kind(0.0)
    real(kr), dimension(:), intent(inout) :: chi,cci
    real(kr), dimension(:), intent(in) :: wa
    integer, intent(in) :: ido,ip,l1
!
    real(kr), parameter :: two=2.0_kr
    real(kr), dimension(ido,l1,ip) :: ch,c1
    real(kr), dimension(ido,ip,l1) :: cc
    real(kr) arg,dcp,dsp,ar1,ai1,ar1h,dc2,ds2,ar2,ai2,ar2h
    integer n,ipph,ipp2,idp2,nbd,i,ic,is,idij,j,j2,jc,l,lc
 
    n=size(cci)
    ch = reshape(chi,(/ido,l1,ip/))
    c1 = reshape(cci,(/ido,l1,ip/))

    arg = two*pimach(.0_kr)/real(ip,kr)
    dcp = cos(arg)
    dsp = sin(arg)
    ipph = (ip+1)/2
    ipp2 = ip+2
    idp2 = ido+2
    nbd = (ido-1)/2
    select case (ido)
    case (1)
      c1(:,:,1) = ch(:,:,1)
    case default
      ch(:,:,1) = c1(:,:,1)
      ch(1,:,2:ip) = c1(1,:,2:ip)
      is = -ido
      do j=2,ip
        is = is+ido
        idij = is
        do i=3,ido,2
          idij = idij+2
          ch(i-1,:,j) = wa(idij-1)*c1(i-1,:,j)+wa(idij)*c1(i,:,j)
          ch(i,:,j) = wa(idij-1)*c1(i,:,j)-wa(idij)*c1(i-1,:,j)
        enddo
      enddo
      do j=2,ipph
        jc = ipp2-j
        do i=3,ido,2
          c1(i-1,:,j) = ch(i-1,:,j)+ch(i-1,:,jc)
          c1(i-1,:,jc) = ch(i,:,j)-ch(i,:,jc)
          c1(i,:,j) = ch(i,:,j)+ch(i,:,jc)
          c1(i,:,jc) = ch(i-1,:,jc)-ch(i-1,:,j)
        enddo
      enddo
    end select
    do j=2,ipph
      jc = ipp2-j
      c1(1,:,j) = ch(1,:,j)+ch(1,:,jc)
      c1(1,:,jc) = ch(1,:,jc)-ch(1,:,j)
    enddo
!
    ar1 = 1._kr
    ai1 = 0._kr
    do l=2,ipph
      lc = ipp2-l
      ar1h = dcp*ar1-dsp*ai1
      ai1 = dcp*ai1+dsp*ar1
      ar1 = ar1h
      ch(:,:,l) = c1(:,:,1)+ar1*c1(:,:,2)
      ch(:,:,lc) = ai1*c1(:,:,ip)
      dc2 = ar1
      ds2 = ai1
      ar2 = ar1
      ai2 = ai1
      do j=3,ipph
        jc = ipp2-j
        ar2h = dc2*ar2-ds2*ai2
        ai2 = dc2*ai2+ds2*ar2
        ar2 = ar2h
        ch(:,:,l) = ch(:,:,l)+ar2*c1(:,:,j)
        ch(:,:,lc) = ch(:,:,lc)+ai2*c1(:,:,jc)
      enddo
    enddo
    do j=2,ipph
      ch(:,:,1) = ch(:,:,1)+c1(:,:,j)
    enddo
!
    cc = reshape(c1,(/ido,ip,l1/))
    cc(:,1,:) = ch(:,:,1)
    do j=2,ipph
      jc = ipp2-j
      j2 = j+j
      cc(ido,j2-2,:) = ch(1,:,j)
      cc(1,j2-1,:) = ch(1,:,jc)
      do i=3,ido,2
        ic = idp2-i
        cc(i-1,j2-1,:) = ch(i-1,:,j)+ch(i-1,:,jc)
        cc(ic-1,j2-2,:) = ch(i-1,:,j)-ch(i-1,:,jc)
        cc(i,j2-1,:) = ch(i,:,j)+ch(i,:,jc)
        cc(ic,j2-2,:) = ch(i,:,jc)-ch(i,:,j)
      enddo
    enddo
    chi = reshape(ch,(/n/))
    cci = reshape(cc,(/n/))
  end subroutine

!=================================================================
  subroutine ftrb(c,wsave)
    integer, parameter :: kr=kind(0.)
    real(kr), dimension(:), intent(inout) :: c
    real(kr), dimension(:), intent(in), target :: wsave
    integer, dimension(15) :: fac
!
    real(kr), parameter :: one=1._kr
    real(kr), dimension(size(c)) :: ch
    real(kr), dimension(:), pointer :: w1,w2,w3,w4
    real(kr) scale
    integer n,nf,na,l1,iw,k1,ip,l2,ido,idl1,ix2,ix3,ix4
!
    n=size(c)
    fac = wsave(n+1:)
!
    if (n==1) return
    nf = fac(2)
    na = 0
    l1 = 1
    iw = 1
    do k1=1,nf
      ip = fac(k1+2)
      l2 = ip*l1
      ido = n/l2
      idl1 = ido*l1
      w1 => wsave(iw:iw+ido-1)
      select case (ip)
      case (4) 
        ix2 = iw+ido
        ix3 = ix2+ido
        w2 => wsave(ix2:ix2+ido-1)
        w3 => wsave(ix3:ix3+ido-1)
        select case(na)
        case (0)
          call radb4 (ido,l1,c,ch,w1,w2,w3)
        case (1)
          call radb4 (ido,l1,ch,c,w1,w2,w3)
        end select
        na = 1-na
      case (2)
        select case (na)
        case (0)
          call radb2 (ido,l1,c,ch,w1)
        case (1)
          call radb2 (ido,l1,ch,c,w1)
        end select
        na = 1-na
      case (3)
        ix2 = iw+ido
        w2 => wsave(ix2:ix2+ido-1)
        select case (na)
        case (0)
          call radb3 (ido,l1,c,ch,w1,w2)
        case (1)
          call radb3 (ido,l1,ch,c,w1,w2)
        end select
        na = 1-na
      case (5)
        ix2 = iw+ido
        ix3 = ix2+ido
        ix4 = ix3+ido
        w2 => wsave(ix2:ix2+ido-1)
        w3 => wsave(ix3:ix3+ido-1)
        w4 => wsave(ix4:ix4+ido-1)
        select case (na)
        case (0)
          call radb5 (ido,l1,c,ch,w1,w2,w3,w4)
        case (1)
          call radb5 (ido,l1,ch,c,w1,w2,w3,w4)
        end select
        na = 1-na
      case default
        select case (na)
        case (0)
          call radbg (ido,ip,l1,c,ch,w1)
        case (1)
          call radbg (ido,ip,l1,ch,c,w1)
        end select
        if (ido==1) na = 1-na
      end select
      l1 = l2
      iw = iw+(ip-1)*ido
    enddo
    scale=one/sqrt(real(n,kr))
    select case (na)
    case (0)
      c= scale*c
    case (1)
      c = scale*ch
    end select
  end subroutine
!=================================================================
  subroutine ftrf(c,wsave)
    integer, parameter :: kr=kind(0.)
    real(kr), dimension(:), intent(inout) :: c
    real(kr), dimension(:), intent(in), target :: wsave
    integer, dimension(15) :: fac
!
    real(kr), parameter :: one=1._kr
    real(kr), dimension(size(c)) :: ch
    real(kr), dimension(:), pointer :: w1,w2,w3,w4
    real(kr) scale
    integer n,nf,na,l1,iw,k1,kh,ip,l2,ido,ix2,ix3,ix4
!
    n=size(c)
!
    fac = wsave(n+1:)
    if (n==1) return
    nf = fac(2)
    na = 1
    l2 = n
    iw = n
    do k1=1,nf
      kh = nf-k1
      ip = fac(kh+3)
      l1 = l2/ip
      ido = n/l2
      iw = iw-(ip-1)*ido
      na = 1-na
      w1 => wsave(iw:iw+ido-1)
      select case(ip)
      case (4)
        ix2 = iw+ido
        ix3 = ix2+ido
        w2 => wsave(ix2:ix2+ido-1)
        w3 => wsave(ix3:ix3+ido-1)
        select case (na)
        case (0)
          call radf4 (ido,l1,c,ch,w1,w2,w3)
        case (1)
          call radf4 (ido,l1,ch,c,w1,w2,w3)
        end select
      case (2)
        select case (na)
        case (0)
          call radf2 (ido,l1,c,ch,w1)
        case (1)
          call radf2 (ido,l1,ch,c,w1)
        end select
      case (3)
        ix2 = iw+ido
        w2 => wsave(ix2:ix2+ido-1)
        select case (na)
        case (0)
          call radf3 (ido,l1,c,ch,w1,w2)
        case (1)
          call radf3 (ido,l1,ch,c,w1,w2)
        end select
      case (5)
        ix2 = iw+ido
        ix3 = ix2+ido
        ix4 = ix3+ido
        w2 => wsave(ix2:ix2+ido-1)
        w3 => wsave(ix3:ix3+ido-1)
        w4 => wsave(ix4:ix4+ido-1)
        select case (na)
        case (0)
          call radf5(ido,l1,c,ch,w1,w2,w3,w4)
        case (1)
          call radf5(ido,l1,ch,c,w1,w2,w3,w4)
        end select
      case default
        if (ido==1) na = 1-na
        select case (na)
        case (0)
          call radfg(ido,ip,l1,c,ch,w1)
          na = 1
        case (1)
          call radfg(ido,ip,l1,ch,c,w1)
          na = 0
        end select
      end select
      l2 = l1
    enddo
    scale=one/sqrt(real(n,kr))
    select case (na)
    case (0)
      c = scale*ch
    case (1)
      c = scale*c
    end select
  end subroutine

  subroutine fti (wsave)
    integer, parameter :: kr=kind(.0)
    real(kr), dimension(:), intent(inout) :: wsave
    integer, dimension(15) :: fac
!
    real(kr), parameter :: two=2._kr
    integer, dimension(4) :: ntryh=(/4,2,3,5/)
    real(kr), dimension((size(wsave)-15)/2) :: arg,dumi
    real(kr) argh,argld
    integer n,nl,nf,j,ntry,nq,nr,i,is,nfm1,l1,ld,l2,ido,id2m1,ipm,ii,k1,ip

    n=size(wsave)-15
!
    nl = n
    nf = 0
    j = 1
    ntry = ntryh(j)
    do
      nq = nl/ntry
      nr = nl-ntry*nq
      if (nr/=0) then
        j = j+1
        select case (j)
        case (:4)
          ntry = ntryh(j)
        case (5:)
          ntry = ntry+2
        end select
        cycle
      endif
      nf = nf+1
      fac(nf+2) = ntry
      nl = nq
      if (ntry==2) then
        fac(nf+2:4:-1) = fac(nf+1:3:-1)
        fac(3) = 2
      endif
      if (nl==1) exit
    enddo
    fac(1) = n
    fac(2) = nf
    wsave(n+1:) = fac
!
    argh = two*pimach(.0_kr)/real(n,kr)
    is = 0
    nfm1 = nf-1
    l1 = 1
    if (nfm1==0) return
    dumi(:n/2) = (/ (real(ii,kr),ii=1,n/2) /)
    do k1=1,nfm1
      ip = fac(k1+2)
      ld = 0
      l2 = l1*ip
      ido = n/l2
      ipm = ip-1
      id2m1 = (ido-1)/2
      do j=1,ipm
        ld = ld+l1
        i = is
        argld = real(ld,kr)*argh
        arg(:id2m1) = dumi(:id2m1)*argld
        wsave(is+1:is+ido-2:2) = cos(arg(:id2m1))
        wsave(is+2:is+ido-1:2) = sin(arg(:id2m1))
        is = is+ido
      enddo
      l1 = l2
    enddo
!    
  return
  end subroutine
end module vfftpack
!=================================================================
