  FUNCTION gammln(xx)
  REAL gammln,xx
  INTEGER j
  DOUBLE PRECISION ser,stp,tmp,x,y,cof(6)
  SAVE cof,stp
  DATA cof,stp/76.18009172947146d0,-86.50532032941677d0, &
     24.01409824083091d0,-1.231739572450155d0,.1208650973866179d-2, &
     -.5395239384953d-5,2.5066282746310005d0/
  x=xx
  y=x
  tmp=x+5.5d0
  tmp=(x+0.5d0)*log(tmp)-tmp
  ser=1.000000000190015d0
  do j=1,6
    y=y+1.d0
    ser=ser+cof(j)/y
  end do
  gammln=tmp+log(stp*ser/x)
  return
  END

  SUBROUTINE gammlnv(xv,yv,n)
  REAL xv(n),yv(n)
  REAL gammln,xx
  INTEGER i,j,n
  REAL ser,stp,tmp,x,y,cof(6)
  SAVE cof,stp
  DATA cof,stp/76.18009,-86.50532, &
     24.014098,-1.2317395,.12086509e-2, &
     -.53952393e-5,2.5066282e0/
  do i=1,n
    x=xv(i)
    y=x
    tmp=x+5.5d0
    tmp=(x+0.5d0)*log(tmp)-tmp
    ser=1.000000000190015d0
    do j=1,6
      y=y+1.d0
      ser=ser+cof(j)/y
    end do
    yv(i)=tmp+log(stp*ser/x)
  end do
  return
  END

  SUBROUTINE expv(xv,yv,n)
  REAL xv(n),yv(n)
  do i=1,n
    yv(i)=log(exp(xv(i)))
  end do
  return
  END

  FUNCTION gammln1(xx)
  REAL gammln,xx
  INTEGER j
  REAL(kind=8) ser,y
  REAL stp,tmp,x,cof(6)
  SAVE cof,stp
  DATA cof,stp/76.18009,-86.50532, &
     24.014098,-1.2317395,.12086509e-2, &
     -.53952393e-5,2.5066282e0/
  x=xx
  y=x
  tmp=x+5.5
  tmp=(x+0.5d0)*log(tmp)-tmp
  ser=1.000000000190015d0
  do j=1,6
    y=y+1.d0
    ser=ser+cof(j)/y
  end do
  gammln1=tmp+log(stp*ser/x)
  return
  END

  SUBROUTINE timeit(f,l,n)
  character(len=*) l
  real cput(2)
  external f
!
  cpu=dtime(cput)
  n=100000
  do i=1,n
    x=1.+1e-10*i
    g=g+f(x)
  end do
  cpu=dtime(cput)
!
  n=(n*2./cpu)
  cpu=dtime(cput)
  do i=1,n
    x=1.+1e-10*i
    g=g+f(x)
  end do
  cpu=dtime(cput)
! 
  print *,l,1e9*cpu/n,' ns/call'
!
  END

  SUBROUTINE timeitv(f,l,m)
  character(len=*) l
  real cput(2),xv(m),yv(m)
  external f
!
  cpu=dtime(cput)
  n=100000/m
  xv=1.234
  do i=1,n
    call f(xv,yv,m)
  end do
  cpu=dtime(cput)
!
  n=(n*2./cpu)
  cpu=dtime(cput)
  do i=1,n
    call f(xv,yv,m)
  end do
  cpu=dtime(cput)
! 
  print *,l,1e9*cpu/(n*m),' ns/call',cpu
!
  END

  !SUBROUTINE test_gammln

  external gammln,gammln1,gammlnv,expv

  call timeit(gammln  ,'gammln ',n)
  call timeit(gammln1 ,'gammln1',n)
  call timeitv(expv   ,'expv   ',1000)
  call timeitv(gammlnv,'gammlnv',100)

  END
