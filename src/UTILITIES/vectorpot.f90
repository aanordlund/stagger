!  $Id: vectorpot.f90,v 1.6 2006/07/17 05:52:00 aake Exp $
MODULE vectorpot_m
  real, allocatable, dimension(:,:,:):: bxt, byt, bzt, axt, ayt, azt
END module

!***********************************************************************
SUBROUTINE vectorpot_test
!
!  This test assigns a by=sin(x) and bz=2.*cos(x), which should 
!  result in az=cos(x) and ay=2.*sin(x). A nyquist component is
!  added to bz, to show that it has no effect.
!
  implicit none
  real, allocatable, dimension(:,:,:):: bx, by, bz, ax, ay, az
  integer i, j, k, m, ks, ke
  real pi, ds

  m = 8
  allocate (bx(m,m,m), by(m,m,m), bz(m,m,m))
  allocate (ax(m,m,m), ay(m,m,m), az(m,m,m))

  pi = 4.*atan(1.)
  ds = 2.*pi/m

!$omp parallel private(k,ks,ke,j,i)
  call limits_omp(1,m,ks,ke)
  do k=ks,ke
  do j=1,m
  do i=1,m
    bx(i,j,k) = 0.
    by(i,j,k) = 1.*sin((i-1)*2.*pi/m)
    bz(i,j,k) = 2.*cos((i-1)*2.*pi/m)+5.*cos((i-1)*pi)
  end do
  end do
  end do
  call vectorpot (bx, by, bz, ax, ay, az, m, m, m, ds, ds, ds)
!$omp end parallel
  print '(8f10.4)',ay(:,1,1)
  print '(8f10.4)',az(:,1,1)

  deallocate (bx, by, bz, ax, ay, az)
END

!***********************************************************************
SUBROUTINE vectorpot (bx, by, bz, ax, ay, az, mx, my, mz, dx, dy, dz)

!  Compute the vectorpotential A, that produces a given B through
!  B = curl(A)

  USE vectorpot_m
  implicit none
  integer mx, my, mz
  real, dimension(mx,my,mz):: bx, by, bz, ax, ay, az
  integer:: i, j, k, i1, j1, k1, ks, ke, omp_get_thread_num
  real:: dx, dy, dz, c, kx, ky, kz, sx, sy, sz, pi, k2
!-----------------------------------------------------------------------
!
!  Forward transform of B
!
!$omp master
  allocate(bxt(mx,my,mz),byt(mx,my,mz),bzt(mx,my,mz))
  allocate(axt(mx,my,mz),ayt(mx,my,mz),azt(mx,my,mz))
!$omp end master
  print *,'FFT'
!$omp barrier
  call fft3df (bx,bxt,mx,my,mz)
  call fft3df (by,byt,mx,my,mz)
  call fft3df (bz,bzt,mx,my,mz)
!$omp barrier
!
!  A = ii*(k x B)/k**2
!
  pi = 4.*atan(1.)
  c = (1./mx)*(1./my)*(1./mz)
!$omp master
  print *,'loop'
!$omp end master
  call limits_omp(1,mz,ks,ke)
  do k=ks,ke
    k1 = (k/2)*2 + mod(k+1,2)
    k1 = min(max(k1,1),mz)
    sz = 2*mod(k+1,2)-1
    kz = 2.*pi/(mz*dz)*mod(k/2,(mz+1)/2)*sz
    ! print *,k,k1,sz
    do j=1,my
      j1 = (j/2)*2 + mod(j+1,2)
      j1 = min(max(j1,1),my)
      sy = 2*mod(j+1,2)-1
      ky = 2.*pi/(my*dy)*mod(j/2,(my+1)/2)*sy
      do i=1,mx
        i1 = (i/2)*2 + mod(i+1,2)
        i1 = min(max(i1,1),mx)
        sx = 2*mod(i+1,2)-1
        kx = 2.*pi/(mx*dx)*mod(i/2,(mx+1)/2)*sx
        k2 = kx**2 + ky**2 + kz**2 + 1e-20
        axt(i,j,k) = (byt(i ,j ,k1)*kz - bzt(i ,j1,k )*ky)*(1./k2)
        ayt(i,j,k) = (bzt(i1,j ,k )*kx - bxt(i ,j ,k1)*kz)*(1./k2)
        azt(i,j,k) = (bxt(i ,j1,k )*ky - byt(i1,j ,k )*kx)*(1./k2)
      end do
    end do
  end do
!
!  Reverse transform of A
!
!$omp master
  print *,'inverse FFT'
!$omp end master
  call fft3db (axt,ax,mx,my,mz)
  call fft3db (ayt,ay,mx,my,mz)
  call fft3db (azt,az,mx,my,mz)
!$omp barrier
!$omp single
  deallocate(bxt,byt,bzt,axt,ayt,azt)
!$omp end single
END
