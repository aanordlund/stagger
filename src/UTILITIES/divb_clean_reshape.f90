!$Id: divb_clean_reshape.f90,v 1.2 2016/11/09 22:04:57 aake Exp $

!***********************************************************************
SUBROUTINE divb_clean
  USE variables
  USE params
  USE reshape_m
  implicit none
  real, allocatable, dimension(:,:,:):: div,dphit,scr,dphit1,k2a,k2b
  real, allocatable, dimension(:,:):: w,k2,tmpf,tmpt
  real, allocatable, dimension(:):: b
  integer iter,ix,iy,iz,lb1,ub1,lz,ny,nz
  real divmin,divmax1,divmax2,wt,wallclock,c,over,dy0,dy1,dy2,fmaxval
  character(len=mid), save:: id="$Id: divb_clean_reshape.f90,v 1.2 2016/11/09 22:04:57 aake Exp $"

  allocate (div(mx,my,mz), dphit(mx,my,mz), scr(mx,my,mz), k2(mx,mz))
  allocate (tmpf(mx,mz),tmpt(mx,mz))

  !$omp parallel private (iz)
  do iz=izs,ize
    dphit(:,:,iz) = 0.
    div(:,:,iz) = 0.
  end do
  !$omp end parallel

  if (master) print*,id
 
  wt=wallclock()
  do iter=1,divb_iter
    !$omp parallel private (ix,iy,iz)
    if (do_2nddiv) then
      call ddxup1_set (bx, scr)
    else
      call ddxup_set (bx, scr)
    end if
    call dumpn(scr,'dBxdx','div.dmp',1)
    do iz=izs,ize
      div(:,lb:ub,iz) = scr(:,lb:ub,iz)
    end do

    if (do_2nddiv) then
      call ddyup1_set (by, scr)
    else
      call ddyup_set (by, scr)
    end if
    call regularize (scr)
    call dumpn(scr,'dBydy','div.dmp',1)
    do iz=izs,ize
      div(:,lb:ub,iz) = div(:,lb:ub,iz) + scr(:,lb:ub,iz)
    end do

    !$omp barrier
    if (do_2nddiv) then
      call ddzup1_set (bz, scr)
    else
      call ddzup_set (bz, scr)
    end if
    call dumpn(scr,'dBzdz','div.dmp',1)
    !$omp barrier
    do iz=izs,ize
      div(:,lb:ub,iz) = div(:,lb:ub,iz) + scr(:,lb:ub,iz)
    end do
    !$omp end parallel
    call dumpn(div,'div(B)','div.dmp',1)

    scr = abs(div)
    divmax1 = fmaxval('divb',scr)*max(sx,sz)

    !$omp parallel
    scr(:,1:lb+3,izs:ize) = 0.
    scr(:,ub-3:,izs:ize) = 0.
    !$omp end parallel
    divmax2 = fmaxval('divb',scr)*max(sx,sz)

    wt=wallclock()-wt
    if (master) print '(i3,1p,(2e15.6),0p,f10.2,a)',iter,divmax1,divmax2,wt,' sec'
    if (divmax1 < divb_eps) exit
    wt=wallclock()

    do iy=lb,ub
      tmpf = div(:,iy,:)
      call fft2df (tmpf,tmpt,mx,mz)
      dphit(:,iy,:) = tmpt
    end do
    call dumpn(dphit,'dphit','div.dmp',1)

    call fft2d_k2(k2,sx,sz,mx,mz)
    call dumpn(k2,'k2','div.dmp',1)

    !---------------------------------------------------------------------------
    ! Reshape the dphit array into full height y, narrower z
    !---------------------------------------------------------------------------
    call reshape_size (mpi_y, ny, nz, lz)
    allocate (dphit1(mx,nz,ny))
    call reshape_yz (dphit, dphit1, ny, lz, ny)
    
    !---------------------------------------------------------------------------
    ! Reshape the k2 array, via 3-D
    !---------------------------------------------------------------------------
    allocate (k2a(mx,my,mz), k2b(mx,nz,ny))
    k2a(:,1,:) = k2(:,:)
    call reshape_yz (k2a, k2b, ny, lz, ny)
    deallocate (k2)
    allocate (k2(mx,lz))
    k2(:,:) = k2b(:,:,1)
    deallocate (k2a, k2b)

    !---------------------------------------------------------------------------
    ! Now solve the full height tri-diagonal problem, wavenumber by wavenumber
    !---------------------------------------------------------------------------
    allocate (w(mytot,3),b(mytot))
    w = 0.
    b = 0.
    do iz=1,lz                                                          ! loop over all ..
    do ix=1,mx                                                          ! .. Fourier components
      if (mpi_x==0 .and. mpi_z==0 .and. ix==1 .and. iz==1) then
        dphit1(ix,:,iz) = 0.                                             ! no DC-component
        cycle
      end if
      
      lb1 = 6
      ub1 = mytot-5

      do iy=lb1,lb1                                                       ! lower boundary
        dy1 = ymg(iy)-ymg(iy-1)
        dy2 = ymg(iy+1)-ymg(iy)
        dy0 = 0.5*(dy1+dy2)
        w(iy,1) = 0.
        w(iy,3) = 1./(dy1*dy0) + 1./(dy2*dy0)                           ! symmetric dphit
        w(iy,2) = -w(iy,3)-k2(ix,iz)     
        b(iy) = dphit1(ix,iz,iy)                                        ! right hand side; FT of div(B)
      end do
      
      do iy=lb1+1,ub1-1                                                   ! tridiagonal matrix elements
        dy1 = ymg(iy)-ymg(iy-1)
        dy2 = ymg(iy+1)-ymg(iy)
        dy0 = 0.5*(dy1+dy2)
        w(iy,1) = 1./(dy1*dy0)
        w(iy,3) = 1./(dy2*dy0)
        w(iy,2) = -w(iy,1)-w(iy,3)-k2(ix,iz)     
        b(iy) = dphit1(ix,iz,iy)                                        ! right hand side; FT of div(B)
      end do

      do iy=ub1,ub1                                                       ! upper boundary
        dy1 = ymg(iy)-ymg(iy-1)
        dy2 = ymg(iy+1)-ymg(iy)
        dy0 = 0.5*(dy1+dy2)
        w(iy,1) = 1./(dy1*dy0) + 1./(dy2*dy0)                           ! symmetric dphit
        w(iy,3) = 0.
        w(iy,2) = -w(iy,1)-k2(ix,iz)     
        b(iy) = dphit1(ix,iz,iy)                                        ! right hand side; FT of div(B)
      end do
      
      do iy=lb1+1,ub1                                                     ! forward loop
        w(iy,1) = -w(iy,1)/w(iy-1,2)
        w(iy,2) = w(iy,2) + w(iy,1)*w(iy-1,3)                           ! eliminate sub-diagonal
        b(iy)   = b(iy)   + w(iy,1)*b(iy-1)                             ! accumulate RHS
      end do

      do iy=ub1,lb1,-1                                                    ! reverse loop
        b(iy) = (b(iy)-w(iy,3)*b(iy+1))/w(iy,2)                         ! back substitute
        dphit1(ix,iz,iy)= b(iy)                                         ! FT of phi such that Laplace(phi)=div(B)
      end do

      do iy=1,lb1-1
        dphit1(ix,iy,iz) = dphit1(ix,lb1+(lb1-iy),iz)                       ! symmetric_center_lower
      end do
      do iy=ub1+1,mytot
        dphit1(ix,iy,iz) = dphit1(ix,ub1-(iy-ub1),iz)                       ! symmetric_center_upper
      end do

    end do
    end do
    deallocate (w,b)

    !---------------------------------------------------------------------------
    ! Recover the dphit array in normal MHD space
    !---------------------------------------------------------------------------
    call deshape_yz (dphit, dphit1, ny, lz, ny)
    deallocate (dphit1)

    do iy=1,my
      tmpf = dphit(:,iy,:)
      call fft2db (tmpf,tmpt,mx,mz)
      div(:,iy,:) = tmpt
    end do
    call dumpn(dphit,'dphit','div.dmp',1)
    call dumpn(div,'dphi','div.dmp',1)

    !$omp parallel private (ix,iy,iz)
    if (do_2nddiv) then
      call ddxdn1_set (div, scr)
    else
      call ddxdn_set (div, scr)
    end if
    do iz=izs,ize
      bx(:,lb:ub,iz) = bx(:,lb:ub,iz) - scr(:,lb:ub,iz)
    end do
    if (do_2nddiv) then
      call ddydn1_set (div, scr)
    else
      call ddydn_set (div, scr)
    end if
    ub1 = min(ub+1,my)
    do iz=izs,ize
      by(:,lb:ub1,iz) = by(:,lb:ub1,iz) - scr(:,lb:ub1,iz)
    end do
    !$omp barrier
    if (do_2nddiv) then
      call ddzdn1_set (div, scr)
    else
      call ddzdn_set (div, scr)
    end if
    !$omp barrier
    do iz=izs,ize
      bz(:,lb:ub,iz) = bz(:,lb:ub,iz) - scr(:,lb:ub,iz)
    end do
    !$omp end parallel

  end do
 
  if (master) print *,wallclock(),' sec'

  deallocate (div,dphit,k2)
END SUBROUTINE divb_clean

