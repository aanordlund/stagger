! $Id: power3d.f90,v 1.8 2016/11/09 22:04:58 aake Exp $
!***********************************************************************
SUBROUTINE power3d (ft, mx, my, mz, pk, xk, nk, mk, ptot)
!
!  3D power spectrum, based on FFT output from FFTPACK (cos/sin real coeffs).
!  The calculations of kx,ky,kz would need to be modified if the transform
!  values are ordered differently.
!
  implicit none
  integer mx, my, mz, mk, nk(mk)
  real ft(mx,my,mz), pk(mk), xk(mk), ptot

  real pk_thread(mk), xk_thread(mk), ptot_thread, ptotx, ptoty
  real kx, ky, kz, k2, kk, cx, cy, cz, p2
  integer i, j, k, ik, nk_thread(mk)
  integer izs, ize, omp_get_thread_num, omp_get_num_threads
  character(len=64):: id='$Id: power3d.f90,v 1.8 2016/11/09 22:04:58 aake Exp $'
!-----------------------------------------------------------------------
  if (id .ne. ' ') then; print *,id; id = ' '; end if

!$omp single
  nk = 0
  xk = 0.
  pk = 0.
  ptot = 0.
!$omp end single

  nk_thread = 0
  xk_thread = 0.
  pk_thread = 0.
  ptot_thread = 0.

  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    kz = k/2
    cz = 2.
    if (k==1 .or. k==mz) cz = 1.
    ptoty = 0.
    do j=1,my
      ky = j/2
      cy = 2.
      if (j==1 .or. j==my) cy = 1.
      ptotx = 0.
      do i=1,mx
        kx = i/2
        cx = 2.
        if (i==1 .or. i==mx) cx = 1.
        k2 = kx**2 + ky**2 + kz**2
        kk = sqrt(k2)
        ik = 1.5 + kk
        p2 = (cx*cy*cz)*abs(ft(i,j,k))**2
        if (ik .le. mk) then
          xk_thread(ik) = xk_thread(ik) + kk
          nk_thread(ik) = nk_thread(ik) + 1
          pk_thread(ik) = pk_thread(ik) + k2*p2
        end if
        ptotx = ptotx + p2
      end do
      ptoty = ptoty + ptotx
    end do
    ptot_thread = ptot_thread + ptoty
  end do
 
!$omp critical
  ptot = ptot + ptot_thread
  xk = xk + xk_thread
  pk = pk + pk_thread
  nk = nk + nk_thread
!$omp end critical
!$omp barrier

!$omp single
  xk = xk/(nk+1e-10)
  xk(1) = 0.
  pk = pk/(nk+1e-10)
!$omp end single

END
