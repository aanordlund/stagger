! $Id: fft2d_rma.f90,v 1.19 2014/08/27 12:47:03 aake Exp $
!------------------------------------------------------------------------------
!
!  Two-dimensional Fourier transform routines, based on FFTPACK.
!  FFTPACK is transportable, and allows arbitrary dimensions (speed
!  will vary, of course).
!
!  CALL fft2df (f, ft, mx, my)              ! forward transform
!  CALL fft2db (ft, f, mx, my)              ! backward transform
!  CALL fft2d_k2 (k2, sx, sy, mx, my)       ! compute k2
!
!******************************************************************************
SUBROUTINE fft2d_k2 (k2, sx, sz, mx, mz)
  USE params, only: mpi_x, mpi_z, mpi_nx, mpi_nz, mpi_rank, pi
  implicit none
  real, dimension(mx,mz):: k2
  real sx, sz, kx, kz
  integer i, j, k, mx, mz
!------------------------------------------------------------------------------
  !$omp single
  do j=1,mz
    kz = 2.*pi/sz*((j+mpi_z*mz)/2)
    do i=1,mx
      kx = 2.*pi/sx*((i+mpi_x*mx)/2)
      k2(i,j) = kx**2+kz**2
    end do
  end do
  !$omp end single
END SUBROUTINE

!******************************************************************************
SUBROUTINE fft2df (r, rt, mx, mz)
  USE params, only: mpi_x, mpi_z, mpi_nx, mpi_ny, mpi_nz, mpi_rank, mid, master
  USE mpi_mod
  implicit none

  integer mx, mz
  real, dimension(mx,mz):: r, rt

  integer i, j, k, i_x, i_z, ierr, mxz, mwx, mwz, rank, nx, nz
  integer coordinates(2), status(MPI_STATUS_SIZE)
  real c
  real, allocatable, dimension(:):: wx, wz, fx, fz
  real, allocatable, dimension(:,:):: rr, rrt
  character(len=mid), save:: id='$Id: fft2d_rma.f90,v 1.19 2014/08/27 12:47:03 aake Exp $'
!------------------------------------------------------------------------------
  !$omp single
  call print_id(id)

  nx = mx*mpi_nx
  nz = mz*mpi_nz

  mxz = mx*mz                                                                   ! domain size
  mwx= 2*nx + 15                                                                ! size of work array
  allocate (fx(nx))                                                             ! allocate 1D-array
  allocate (wx(mwx))                                                            ! allocate work array
  call srffti (nx,wx)                                                           ! compute work array
  c=1./(nx)                                                                     ! normalization factor

  if (mpi_nx == 1) then                                                         ! if no x MPI-domains
    do j=1,mz                                                                   ! loop over x-strips
      fx=r(:,j)                                                                 ! copy data
      call srfftf (mx, fx, wx)                                                  ! transform
      rt(:,j)=c*fx                                                              ! normalize and copy back
    end do                                                                      ! done
  else
    if (mpi_x == 0) then                                                        ! are we taking on the work?
      allocate (rr(nx,mz), rrt(nx,mz))                                          ! allocate strip buffers

      rr(1:mx,:) = r                                                            ! fill in local part
      do i_x=1,mpi_nx-1                                                         ! for all others in the strip
        coordinates = (/ i_x, mpi_z /)                                          ! coordinates
        call MPI_CART_RANK (cart_mpi(2), coordinates, rank, ierr)               ! get their rank
        call MPI_RECV (rt, mxz, MPI_REAL, rank, i_x, cart_mpi(2), status, ierr) ! receive other domain
        rr(i_x*mx+1:i_x*mx+mx,:) = rt                                           ! fill in
      end do

      !if (mpi_z==0) print '(25f6.3)',rr(:,1)
      do j=1,mz                                                                 ! for all z in domain
        fx=rr(:,j)                                                              ! copy string
        call srfftf (nx, fx, wx)                                                ! transform
        rrt(:,j)=c*fx
      end do
      !if (mpi_z==0) print '(25f6.3)',rrt(1:10,1)

      do i_x=1,mpi_nx-1                                                         ! for all others in the strip
        coordinates = (/ i_x, mpi_z /)                                          ! coordinates
        call MPI_CART_RANK(cart_mpi(2), coordinates, rank, ierr)                ! get their rank
        rt = rrt(i_x*mx+1:i_x*mx+mx,:)                                          ! fill in
        call MPI_SEND (rt, mxz, MPI_REAL, rank, i_x, cart_mpi(2), ierr)
      end do
      rt = rrt(1:mx,:)

      deallocate (rr,rrt)
    else
      coordinates = (/ 0, mpi_z /)                                              ! coordinates
      call MPI_CART_RANK(cart_mpi(2), coordinates, rank, ierr)                  ! where to?
      call MPI_SEND (r , mxz, MPI_REAL, rank, mpi_x, cart_mpi(2), ierr)         ! send domain data
      call MPI_RECV (rt, mxz, MPI_REAL, rank, mpi_x, cart_mpi(2), status, ierr) ! receive results
    end if
  end if
  deallocate (wx, fx)
  !if (mpi_x==0 .and. mpi_z==0) print '(6f6.3)',rt(1:6,1:6)

!...............................................................................
  mxz = mx*mz                                                                   ! domain size
  mwz= 2*nz + 15                                                                ! size of work array
  allocate (fz(nz))                                                             ! allocate 1D-array
  allocate (wz(mwz))                                                            ! allocate work array
  call srffti (nz,wz)                                                           ! compute work array
  c=1./(nz)                                                                     ! normalization factor
  if (mpi_nz == 1) then                                                         ! no MPI along this dir
    do i=1,mx                                                                   ! loop over strips
      fz=rt(i,:)                                                                ! copy data
      call srfftf (mz, fz, wz)                                                  ! transform
      rt(i,:)=c*fz                                                              ! copy back
    end do                                                                      ! done!
  else
    if (mpi_z == 0) then                                                        ! are we taking on the work?
      allocate (rr(mx,nz), rrt(mx,nz))                                          ! allocate strip buffers

      rr(:,1:mz) = rt                                                           ! fill in local part
      do i_z=1,mpi_nz-1                                                         ! for all others in the strip
        coordinates = (/ mpi_x, i_z /)                                          ! coordinates
        call MPI_CART_RANK(cart_mpi(2), coordinates, rank, ierr)                ! get their rank
        call MPI_RECV (rt, mxz, MPI_REAL, rank, i_z, cart_mpi(2), status, ierr) ! receive other domain
        rr(:,i_z*mz+1:i_z*mz+mz) = rt                                           ! fill in
      end do

      !if (mpi_x==0) print '(25f6.3)',rr(4,1:2*mz)
      do j=1,mx                                                                 ! for all z in domain
        fz=rr(j,:)                                                              ! copy string
        call srfftf (nz, fz, wz)                                                ! transform
        rrt(j,:)=c*fz
      end do
      !if (mpi_x==0) print '(25f6.3)',rrt(4,1:10)

      do i_z=1,mpi_nz-1                                                         ! for all others in the strip
        coordinates = (/ mpi_x, i_z /)                                          ! coordinates
        call MPI_CART_RANK(cart_mpi(2), coordinates, rank, ierr)                ! get their rank
        rt = rrt(:,i_z*mz+1:i_z*mz+mz)                                          ! fill in
        call MPI_SEND (rt, mxz, MPI_REAL, rank, i_z, cart_mpi(2), ierr)
      end do
      rt = rrt(:,1:mz)

      deallocate (rr,rrt)
    else
      coordinates = (/ mpi_x, 0 /)                                              ! coordinates
      call MPI_CART_RANK(cart_mpi(2), coordinates, rank, ierr)                  ! where to?
      call MPI_SEND (rt, mxz, MPI_REAL, rank, mpi_z, cart_mpi(2), ierr)         ! send domain data
      call MPI_RECV (rt, mxz, MPI_REAL, rank, mpi_z, cart_mpi(2), status, ierr) ! receive results
    end if
  end if
  deallocate (wz, fz)

  !$omp end single
END SUBROUTINE

!******************************************************************************
SUBROUTINE fft2db (r, rt, mx, mz)
  USE params, only: mpi_x, mpi_z, mpi_nx, mpi_ny, mpi_nz, mpi_rank, master
  USE mpi_mod
  implicit none

  integer mx, mz
  real, dimension(mx,mz):: r, rt

  integer i, j, k, i_x, i_z, ierr, mxz, mwx, mwz, rank, tag, nx, nz
  integer coordinates(2), status(MPI_STATUS_SIZE)
  real, allocatable, dimension(:):: wx, wz, fx, fz
  real, allocatable, dimension(:,:):: rr, rrt, buf
!------------------------------------------------------------------------------
  !$omp single

  nx = mx*mpi_nx
  nz = mz*mpi_nz

!...............................................................................
  mxz = mx*mz                                                                   ! domain size
  mwz= 2*nz + 15                                                                ! size of work array
  allocate (fz(nz))                                                             ! allocate 1D-array
  allocate (wz(mwz))                                                            ! allocate work array
  call srffti (nz,wz)                                                           ! compute work array
  if (mpi_nz == 1) then                                                         ! no MPI along this dir
    do i=1,mx                                                                   ! loop over strips
      fz=r(i,:)                                                                 ! copy data
      call srfftb (mz, fz, wz)                                                  ! transform
      rt(i,:)=fz                                                                ! copy back
    end do                                                                      ! done!
  else
    if (mpi_z == 0) then                                                        ! are we taking on the work?
      allocate (rr(mx,nz), rrt(mx,nz), buf(mx,mz))                              ! allocate strip buffers

      rr(:,1:mz) = r                                                            ! fill in local part
      do i_z=1,mpi_nz-1                                                         ! for all others in the strip
        coordinates = (/ mpi_x, i_z /)                                          ! coordinates
        call MPI_CART_RANK(cart_mpi(2), coordinates, rank, ierr)                ! get their rank
        tag = i_z                                                               ! encode sender
        call MPI_RECV (buf, mxz, MPI_REAL, rank, tag, cart_mpi(2), status, ierr)! receive other domain
        rr(:,i_z*mz+1:i_z*mz+mz) = buf                                          ! fill in
      end do

      do j=1,mx                                                                 ! for all z in domain
        fz=rr(j,:)                                                              ! copy string
        call srfftb (nz, fz, wz)                                                ! transform
        rrt(j,:)=fz
      end do

      do i_z=1,mpi_nz-1                                                         ! for all others in the strip
        coordinates = (/ mpi_x, i_z /)                                          ! coordinates
        call MPI_CART_RANK(cart_mpi(2), coordinates, rank, ierr)                ! get their rank
        buf = rrt(:,i_z*mz+1:i_z*mz+mz)                                         ! fill in
        tag = i_z                                                               ! encode receiver
        call MPI_SEND (buf, mxz, MPI_REAL, rank, tag, cart_mpi(2), ierr)
      end do
      rt = rrt(:,1:mz)

      deallocate (rr,rrt,buf)
    else
      coordinates = (/ mpi_x, 0 /)                                              ! coordinates
      call MPI_CART_RANK(cart_mpi(2), coordinates, rank, ierr)                  ! where to?
      tag = mpi_z                                                               ! reverse encoding
      call MPI_SEND (r , mxz, MPI_REAL, rank, tag, cart_mpi(2), ierr)           ! send domain data
      call MPI_RECV (rt, mxz, MPI_REAL, rank, tag, cart_mpi(2), status, ierr)   ! receive results
    end if
  end if
  deallocate (wz, fz)

!...............................................................................
  mxz = mx*mz                                                                   ! domain size
  mwx= 2*nx + 15                                                                ! size of work array
  allocate (fx(nx))                                                             ! allocate 1D-array
  allocate (wx(mwx))                                                            ! allocate work array
  call srffti (nx,wx)                                                           ! compute work array

  if (mpi_nx == 1) then                                                         ! if no x MPI-domains
    do j=1,mz                                                                   ! loop over x-strips
      fx=rt(:,j)                                                                ! copy data
      call srfftb (mx, fx, wx)                                                  ! transform
      rt(:,j)=fx                                                                ! normalize and copy back
    end do                                                                      ! done
  else
    if (mpi_x == 0) then                                                        ! are we taking on the work?
      allocate (rr(nx,mz), rrt(nx,mz), buf(mx,mz))                              ! allocate strip buffers

      rr(1:mx,:) = rt                                                           ! fill in local part
      do i_x=1,mpi_nx-1                                                         ! for all others in the strip
        coordinates = (/ i_x, mpi_z /)                                          ! coordinates
        call MPI_CART_RANK (cart_mpi(2), coordinates, rank, ierr)               ! get their rank
        tag = mpi_x+i_x*mpi_nx                                                  ! encode receivet and sender
        call MPI_RECV (buf, mxz, MPI_REAL, rank, tag, cart_mpi(2), status, ierr)! receive other domain
        rr(i_x*mx+1:i_x*mx+mx,:) = buf                                          ! fill in
      end do

      do j=1,mz                                                                 ! for all z in domain
        fx=rr(:,j)                                                              ! copy string
        call srfftb (nx, fx, wx)                                                ! transform
        rrt(:,j)=fx
      end do

      do i_x=1,mpi_nx-1                                                         ! for all others in the strip
        coordinates = (/ i_x, mpi_z /)                                          ! coordinates
        call MPI_CART_RANK(cart_mpi(2), coordinates, rank, ierr)                ! get their rank
        buf = rrt(i_x*mx+1:i_x*mx+mx,:)                                         ! fill in
        tag = mpi_x+i_x*mpi_nx                                                  ! encode receivet and sender
        call MPI_SEND (buf, mxz, MPI_REAL, rank, tag, cart_mpi(2), ierr)
      end do
      rt = rrt(1:mx,:)

      deallocate (rr,rrt,buf)
    else
      coordinates = (/ 0, mpi_z /)                                              ! coordinates
      call MPI_CART_RANK(cart_mpi(2), coordinates, rank, ierr)                  ! where to?
      tag = mpi_x*mpi_nx                                                        ! reverse encoding
      call MPI_SEND (rt, mxz, MPI_REAL, rank, tag, cart_mpi(2), ierr)           ! send domain data
      call MPI_RECV (rt, mxz, MPI_REAL, rank, tag, cart_mpi(2), status, ierr)   ! receive results
    end if
  end if
  deallocate (wx, fx)

  !$omp end single
END SUBROUTINE

!******************************************************************************
SUBROUTINE fft2dby (f3, t3, mx, my, mz)
!
! Use parallel ranks, from the transform direction, to work on several 
! planes in parallel.  For each plane the pieces in the transform direction
! are collected onto the work rank, while other ranks just send and receive.
!
  USE params, only: mpi_x, mpi_y, mpi_z, mpi_nx, mpi_ny, mpi_nz, mpi_rank, master
  USE mpi_mod
  implicit none

  integer mx, my, mz
  real, dimension(mx,my,mz):: f3, t3

  integer ix, iy, iz, i_x, i_z, ierr, mxz, mwx, mwz, rank, tag, nx, nz, win, incr
  integer fft_x, fft_z
  integer coordinates(2), status(MPI_STATUS_SIZE)
  real, allocatable, dimension(:):: w1, f1
  real, allocatable, dimension(:,:):: f2, t2, buf, buf2
  integer(kind=MPI_ADDRESS_KIND) bytes, offset
  integer:: winf, wint, increment
!------------------------------------------------------------------------------
  !$omp single

  mxz = mx*mz                                                                   ! buffer size
!...............................................................................
  nz = mz*mpi_nz
  mwz= 2*nz + 15                                                                ! size of work array
  allocate (f1(nz), w1(mwz))                                                    ! allocate work array
  call srffti (nz,w1)                                                           ! compute work array

!-------------------------------------------------------------------------------
! Z-transform
!-------------------------------------------------------------------------------
  if (mpi_nz == 1) then                                                         ! no MPI along this dir
    !if (master) print*,'ff2dby: single rank in z'
    !---------------------------------------------------------------------------
    ! No extra ranks available in the transform direction; just loop
    !---------------------------------------------------------------------------
    do iy=1,my                                                                  ! loop over strips
    do ix=1,mx                                                                  ! loop over strips
      f1=f3(ix,iy,:)                                                            ! copy data
      call srfftb (nz, f1, w1)                                                  ! transform
      t3(ix,iy,:)=f1                                                            ! copy back
    end do                                                                      ! done!
    end do                                                                      ! done!

  else if (mpi_nz < my) then
    !if (master) print*,'ff2dby: single rank in z handles mpi_nz =', mpi_nz
    !---------------------------------------------------------------------------
    ! Not enough ranks available, so loop over iy
    !---------------------------------------------------------------------------
    allocate (f2(mx,mz), t2(mx,mz), buf(mx,mz), buf2(mx,nz))
    !---------------------------------------------------------------------------
    ! Create MPI windows in the local MPI YZ-plane cart_mpi(1)
    !---------------------------------------------------------------------------
    increment = 4
    bytes = 4_8*mxz
    call MPI_Win_create (f2,bytes,increment,MPI_INFO_NULL,cart_mpi(1),winf,ierr)
    call MPI_Win_create (t2,bytes,increment,MPI_INFO_NULL,cart_mpi(1),wint,ierr)
    !---------------------------------------------------------------------------
    ! Loop over iy
    !---------------------------------------------------------------------------
    do iy=1,my
      f2 = f3(:,iy,:)
      call MPI_Barrier (cart_mpi(1), ierr)
      if (mpi_z == 0) then
        !--------------------------------------------------------------------------
        ! Assemble the block we are to work on
        !--------------------------------------------------------------------------
        buf2(:,1:mz) = f2
        do i_z=1,mpi_nz-1
          coordinates = (/ mpi_y, i_z /)                                          ! coordinates
          call MPI_CART_RANK(cart_mpi(1), coordinates, rank, ierr)                ! get their rank
          call MPI_Win_lock (MPI_LOCK_EXCLUSIVE, rank, 0, winf, ierr)
          offset = 0
          call MPI_Get (buf,mxz,MPI_REAL, rank,offset,mxz,MPI_REAL,winf,ierr)
          call MPI_Win_unlock (rank, winf, ierr)
          buf2(:,i_z*mz+1:i_z*mz+mz) = buf
        end do
      end if
      call MPI_Barrier (cart_mpi(1), ierr)
      !--------------------------------------------------------------------------
      ! Do the transforms
      !--------------------------------------------------------------------------
      if (mpi_z == 0) then
        do ix=1,mx                                                                 ! for all z in domain
          f1=buf2(ix,:)                                                              ! copy string
          call srfftb (nz, f1, w1)                                                ! transform
          buf2(ix,:)=f1
        end do
      end if
      !--------------------------------------------------------------------------
      ! Put the results back
      !--------------------------------------------------------------------------
      t2 = buf2(:,1:mz)
      if (mpi_z == 0) then
        do i_z=1,mpi_nz-1
          coordinates = (/ mpi_y, i_z /)                                          ! coordinates
          call MPI_CART_RANK(cart_mpi(1), coordinates, rank, ierr)                ! get their rank
          buf = buf2(:,i_z*mz+1:i_z*mz+mz)
          offset = 0
          call MPI_Win_lock (MPI_LOCK_EXCLUSIVE, rank, 0, wint, ierr)
          call MPI_Put (buf,mxz,MPI_REAL, rank,offset,mxz,MPI_REAL,wint,ierr)
          call MPI_Win_unlock (rank, wint, ierr)
        end do
      end if
      call MPI_Barrier (cart_mpi(1), ierr)
      t3(:,iy,:) = t2
    end do ! iy
    !---------------------------------------------------------------------------
    ! Done with these windows, copy the result back
    !---------------------------------------------------------------------------
    call MPI_Win_free (wint, ierr)
    call MPI_Win_free (winf, ierr)
    deallocate (f2, t2, buf, buf2)

  else
    !if (master) print*,'ff2dby: parallel ranks in z with mpi_nz =', mpi_nz
    !---------------------------------------------------------------------------
    ! Extra ranks available in the transform direction, so share the work.
    !---------------------------------------------------------------------------
    iy = mpi_z+1
    allocate (f2(mx,mz), t2(mx,mz), buf(mx,mz), buf2(mx,nz))
    !---------------------------------------------------------------------------
    ! Create MPI windows in the local MPI YZ-plane cart_mpi(1)
    !---------------------------------------------------------------------------
    increment = 4
    bytes = 4_8*mxz
    call MPI_Win_create (f2,bytes,increment,MPI_INFO_NULL,cart_mpi(1),winf,ierr)
    call MPI_Win_create (t2,bytes,increment,MPI_INFO_NULL,cart_mpi(1),wint,ierr)
    !---------------------------------------------------------------------------
    ! Decide if we are taking care of one of the yz-planes
    !---------------------------------------------------------------------------
    f2 = f3(:,iy,:)
    call MPI_Barrier (cart_mpi(1), ierr)
    if (iy <= my) then                                                          ! are we taking on work?
      !-------------------------------------------------------------------------
      ! Assemble the block we are to work on
      !-------------------------------------------------------------------------
      do i_z=0,mpi_nz-1
        coordinates = (/ mpi_y, i_z /)                                          ! coordinates
        call MPI_CART_RANK(cart_mpi(1), coordinates, rank, ierr)                ! get their rank
        call MPI_Win_lock (MPI_LOCK_EXCLUSIVE, rank, 0, winf, ierr)
        offset = 0
        call MPI_Get (buf,mxz,MPI_REAL, rank,offset,mxz,MPI_REAL,winf,ierr)
        call MPI_Win_unlock (rank, winf, ierr)
        buf2(:,i_z*mz+1:i_z*mz+mz) = buf
      end do
    end if
    call MPI_Barrier (cart_mpi(1), ierr)
    !---------------------------------------------------------------------------
    ! Do the transforms
    !---------------------------------------------------------------------------
    if (iy <= my) then                                                          ! are we taking on work?
      do ix=1,mx                                                                ! for all z in domain
        f1=buf2(ix,:)                                                           ! copy string
        call srfftb (nz, f1, w1)                                                ! transform
        buf2(ix,:)=f1
      end do
    end if
    !---------------------------------------------------------------------------
    ! Put the results back
    !---------------------------------------------------------------------------
    if (iy <= my) then                                                          ! are we taking on work?
      do i_z=0,mpi_nz-1
        coordinates = (/ mpi_y, i_z /)                                          ! coordinates
        call MPI_CART_RANK(cart_mpi(1), coordinates, rank, ierr)                ! get their rank
        call MPI_Win_lock (MPI_LOCK_EXCLUSIVE, rank, 0, wint, ierr)
        buf = buf2(:,i_z*mz+1:i_z*mz+mz)
        offset = 0
        call MPI_Put (buf,mxz,MPI_REAL, rank,offset,mxz,MPI_REAL,wint,ierr)
        call MPI_Win_unlock (rank, wint, ierr)
      end do
    end if
    call MPI_Barrier (cart_mpi(1), ierr)
    t3(:,iy,:) = t2
    !---------------------------------------------------------------------------
    ! Done with these windows
    !---------------------------------------------------------------------------
    call MPI_Win_free (wint, ierr)
    call MPI_Win_free (winf, ierr)
    deallocate (f2, t2, buf, buf2)

  end if ! method for large jobs

  !-----------------------------------------------------------------------------
  ! X-transform
  !-----------------------------------------------------------------------------
  deallocate (f1, w1)
  nx = mx*mpi_nx
  mwx= 2*nx + 15                                                                ! size of work array
  allocate (f1(nx), w1(mwx))                                                    ! allocate work array
  call srffti (nx,w1)                                                           ! compute work array

  if (mpi_nx == 1) then                                                         ! no MPI along this dir
    !---------------------------------------------------------------------------
    ! No extra ranks available in the transform direction; just loop
    !---------------------------------------------------------------------------
    do iy=1,my                                                                  ! loop over strips
    do iz=1,mz                                                                  ! loop over strips
      f1=t3(:,iy,iz)                                                            ! copy data
      call srfftb (nx, f1, w1)                                                  ! transform
      t3(:,iy,iz)=f1                                                            ! copy back
    end do                                                                      ! done!
    end do                                                                      ! done!

  !-----------------------------------------------------------------------------
  ! Not enough ranks available, so just loop over iy
  !-----------------------------------------------------------------------------
  else if (mpi_nx < my) then
    allocate (f2(mx,mz), t2(mx,mz), buf(mx,mz), buf2(nx,mz))
    !---------------------------------------------------------------------------
    ! Create MPI windows in the local MPI YX-plane cart_mpi(3)
    !---------------------------------------------------------------------------
    increment = 4
    bytes = 4_8*mxz
    call MPI_Win_create (f2,bytes,increment,MPI_INFO_NULL,cart_mpi(3),winf,ierr)
    call MPI_Win_create (t2,bytes,increment,MPI_INFO_NULL,cart_mpi(3),wint,ierr)
    !---------------------------------------------------------------------------
    ! Loop over iy
    !---------------------------------------------------------------------------
    !call MPI_COMM_RANK (cart_mpi(3), rank, ierr)
    !CALL MPI_CART_COORDS (cart_mpi(3), rank, 2, coordinates, ierr)
    !if (coordinates(1)==0) then
    do iy=1,my
      f2 = t3(:,iy,:)
      call MPI_Barrier (cart_mpi(3), ierr)
      if (mpi_x == 0) then
        !--------------------------------------------------------------------------
        ! Assemble the block we are to work
        !--------------------------------------------------------------------------
        buf2(1:mx,:) = f2
        do i_x=1,mpi_nx-1
          coordinates = (/ i_x, mpi_y /)                                          ! coordinates
          call MPI_CART_RANK(cart_mpi(3), coordinates, rank, ierr)                ! get their rank
          offset = 0
          call MPI_Win_lock (MPI_LOCK_EXCLUSIVE, rank, 0, winf, ierr)
          call MPI_Get (buf,mxz,MPI_REAL, rank,offset,mxz,MPI_REAL,winf,ierr)
          call MPI_Win_unlock (rank, winf, ierr)
          buf2(i_x*mx+1:i_x*mx+mx,:) = buf
        end do
      end if
      call MPI_Barrier (cart_mpi(3), ierr)
      !--------------------------------------------------------------------------
      ! Do the transforms
      !--------------------------------------------------------------------------
      if (mpi_x == 0) then
        do iz=1,mz                                                                 ! for all z in domain
          f1=buf2(:,iz)                                                              ! copy string
          call srfftb (nx, f1, w1)                                                ! transform
          buf2(:,iz)=f1
        end do
      end if
      !--------------------------------------------------------------------------
      ! Put the results back
      !--------------------------------------------------------------------------
      if (mpi_x == 0) then
        t2 = buf2(1:mx,:)
        do i_x=1,mpi_nx-1
          coordinates = (/ i_x, mpi_y /)                                          ! coordinates
          call MPI_CART_RANK(cart_mpi(3), coordinates, rank, ierr)                ! get their rank
          buf = buf2(i_x*mx+1:i_x*mx+mx,:)
          offset = 0
          call MPI_Win_lock (MPI_LOCK_EXCLUSIVE, rank, 0, wint, ierr)
          call MPI_Put (buf,mxz,MPI_REAL, rank,offset,mxz,MPI_REAL,wint,ierr)
          call MPI_Win_unlock (rank, wint, ierr)
        end do
      end if
      call MPI_Barrier (cart_mpi(3), ierr)
      t3(:,iy,:) = t2
    end do
    !---------------------------------------------------------------------------
    ! Done with these windows
    !---------------------------------------------------------------------------
    call MPI_Win_free (wint, ierr)
    call MPI_Win_free (winf, ierr)
    deallocate (f2, t2, buf, buf2)

  !-----------------------------------------------------------------------------
  ! Extra ranks available in the transform direction, so share the work.
  !-----------------------------------------------------------------------------
  else
    if (master) print*,'ff2dby: parallel ranks in x with mpi_nx =', mpi_nx
    allocate (f2(mx,mz), t2(mx,mz), buf(mx,mz), buf2(nx,mz))
    !---------------------------------------------------------------------------
    ! Create MPI windows in the local MPI YX-plane cart_mpi(3)
    !---------------------------------------------------------------------------
    increment = 4
    bytes = 4_8*mxz
    call MPI_Win_create (f2,bytes,increment,MPI_INFO_NULL,cart_mpi(3),winf,ierr)
    call MPI_Win_create (t2,bytes,increment,MPI_INFO_NULL,cart_mpi(3),wint,ierr)
    !---------------------------------------------------------------------------
    ! Decide if we are taking care of one of the xy-planes
    !---------------------------------------------------------------------------
    iy = mpi_x+1
    f2 = t3(:,iy,:)
    call MPI_Barrier (cart_mpi(3), ierr)
    if (iy <= my) then                                                          ! are we taking on work?
      !-------------------------------------------------------------------------
      ! Assemble the block we are to work
      !-------------------------------------------------------------------------
      do i_x=0,mpi_nx-1
        if (i_x == mpi_x) then
          buf2(i_x*mx+1:i_x*mx+mx,:) = f2
        else
          coordinates = (/ i_x, mpi_y /)                                          ! coordinates
          call MPI_CART_RANK(cart_mpi(3), coordinates, rank, ierr)                ! get their rank
          offset = 0
          call MPI_Win_lock (MPI_LOCK_EXCLUSIVE, rank, 0, winf, ierr)
          call MPI_Get (buf,mxz,MPI_REAL, rank,offset,mxz,MPI_REAL,winf,ierr)
          call MPI_Win_unlock (rank, winf, ierr)
          buf2(i_x*mx+1:i_x*mx+mx,:) = buf
        end if
      end do
    end if
    call MPI_Barrier (cart_mpi(3), ierr)
    !-------------------------------------------------------------------------
    ! Do the transforms
    !-------------------------------------------------------------------------
    if (iy <= my) then                                                          ! are we taking on work?
      do iz=1,mz                                                                 ! for all z in domain
        f1=buf2(:,iz)                                                            ! copy string
        call srfftb (nx, f1, w1)                                                ! transform
        buf2(:,iz)=f1
      end do
    end if
    !-------------------------------------------------------------------------
    ! Put the results back
    !-------------------------------------------------------------------------
    if (iy <= my) then                                                          ! are we taking on work?
      do i_x=0,mpi_nx-1
        if (i_x == mpi_x) then
          t2 = buf2(i_x*mx+1:i_x*mx+mx,:)
        else
          coordinates = (/ i_x, mpi_y /)                                          ! coordinates
          call MPI_CART_RANK(cart_mpi(3), coordinates, rank, ierr)                ! get their rank
          buf = buf2(i_x*mx+1:i_x*mx+mx,:)
          offset = 0
          call MPI_Win_lock (MPI_LOCK_EXCLUSIVE, rank, 0, wint, ierr)
          call MPI_Put (buf,mxz,MPI_REAL, rank,offset,mxz,MPI_REAL,wint,ierr)
          call MPI_Win_unlock (rank, wint, ierr)
        end if
      end do
    end if
    call MPI_Barrier (cart_mpi(3), ierr)
    t3(:,iy,:) = t2
    !---------------------------------------------------------------------------
    ! Done with these windows
    !---------------------------------------------------------------------------
    call MPI_Win_free (wint, ierr)
    call MPI_Win_free (winf, ierr)
    deallocate (f2, t2, buf, buf2)

  end if ! method for large jobs

  !-----------------------------------------------------------------------------
  ! Clean up after X-work
  !-----------------------------------------------------------------------------
  deallocate (f1, w1)

  call MPI_Barrier (mpi_com, ierr)
  !$omp end single
END SUBROUTINE fft2dby

!*******************************************************************************
SUBROUTINE test_fft2d

  USE params
  USE mpi_mod
  implicit none
  real, allocatable, dimension(:,:):: f1, f2, ft, f3, k2
  real, allocatable, dimension(:,:,:):: g1, g2, gt, g3
  real kx, kz, f1max, f1lim, g1max, g1lim, f, err
  integer ix, iy, iz, jx, jz, ierr

  allocate (f1(mx,mz), f2(mx,mz), ft(mx,mz), f3(mx,mz), k2(mx,mz))
  allocate (g1(mx,my,mz), g2(mx,my,mz), gt(mx,my,mz), g3(mx,my,mz))

  kx = 2.*pi/sx
  kz = 2.*pi/sx
  jx = (mx*mpi_nx)/2-1
  jz = (mz*mpi_nz)/2-1
  if (master) print*,'test_fft2d: jx,jz=',jx,jz
  do iz=1,mz
  do ix=1,mx
    f1(ix,iz) = cos(2.*kx*xm(ix))*cos(3.*kz*zm(iz))
    f2(ix,iz) = f1(ix,iz)
    f3(ix,iz) = -((2.*kx)**2+(3.*kz)**2)*cos(2.*kx*xm(ix))*cos(3.*kz*zm(iz))
  end do
  end do
  do iy=1,my
    f = (1.+0.01*iy)
    g1(:,iy,:) = f*f1(:,:)
    g2(:,iy,:) = f*f2(:,:)
    g3(:,iy,:) = f*f3(:,:)
  end do

  call fft2df (f1, ft, mx, mz)

  if (mpi_x==0 .and. mpi_z==0) ft(4,6) = ft(4,6) - 0.25                         ! subtract the expected FT
  err = maxval(abs(ft))
  call max_real_mpi (err)
  if (err > 1e-5) then                                                          ! check the residue
    print '(2x,a,2i4,g14.6)', &
    'test_fft2df  inverse ERROR: mpi_x, mpi_z, maxval(abs(ft)) =', &
      mpi_x, mpi_z, maxval(abs(ft))
    !call end_mpi
    !stop
  end if
  if (mpi_x==0 .and. mpi_z==0) ft(4,6) = ft(4,6) + 0.25                         ! add the expected FT

  do iy=1,my
    f = (1.+0.01*iy)
    gt(:,iy,:) = f*ft(:,:)
  end do

  call fft2db  (ft, f1, mx, mz)
  call fft2dby (gt, g1, mx, my, mz)

  f1 = abs(f2-f1)
  err = maxval (f1)
  if (err > 1e-5) then                                                        ! check the inverse result
    print '(i7,2x,a,2i4,2g14.6)', mpi_rank, &
    'test_fft2db  inverse ERROR: mpi_x, mpi_z, maxval(abs(f2-f1)) =', &
      mpi_x, mpi_z, err
    !call end_mpi
    !stop
  end if
  call max_real_mpi (err)
  if (master) print *,'test_fft2db : diff =', err

  g1 = abs(g2-g1)
  err = maxval (g1)
  if (err > 1e-5) then                                                        ! check the inverse result
    print '(i7,2x,a,2i4,2g14.6)', mpi_rank, &
    'test_fft2dby inverse ERROR: mpi_x, mpi_z, maxval(abs(g2-g1)) =', &
      mpi_x, mpi_z, err
    !call end_mpi
    !stop
  end if
  call max_real_mpi (err)
  if (master) print *,'test_fft2dby: diff =', err

  call fft2d_k2 (k2, sx, sz, mx, mz)
!  write(80+mpi_x+mpi_z*mpi_nx) mx,mz,mpi_x,mpi_z
!  write(80+mpi_x+mpi_z*mpi_nx) f2
!  write(80+mpi_x+mpi_z*mpi_nx) ft
  do iz=1,mz
    do ix=1,mx
      ft(ix,iz) = -k2(ix,iz)*ft(ix,iz)
    end do
  end do
  do iy=1,my
    f = (1.+0.01*iy)
    gt(:,iy,:) = f*ft(:,:)
  end do
    
!  write(80+mpi_x+mpi_z*mpi_nx) ft
  call fft2db  (ft, f1, mx, mz)
  call fft2dby (gt, g1, mx, my, mz)
!  write(80+mpi_x+mpi_z*mpi_nx) f1
!  write(80+mpi_x+mpi_z*mpi_nx) f3
!  write(80+mpi_x+mpi_z*mpi_nx) k2

  f1 = abs(f3-f1)
  err = maxval (f1)
  f1lim = 2e-3*(kx*jx)**2
  if (err > f1lim) then                                                       ! check the inverse result
    print '(i7,2x,a,2i4,2g14.6)', mpi_rank, &
    'test_fft2db  Laplace ERROR: mpi_x, mpi_z, maxval(abs(f3-f1)) =', &
      mpi_x, mpi_z, err, f1lim
    !call end_mpi
    !stop
  end if
  call max_real_mpi (err)
  if (master) print *,'test_fft2dL : diff =', err

  g1 = abs(g3-g1)
  err = maxval (g1)
  g1lim = 2e-3*(kx*jx)**2
  if (err > g1lim) then                                                       ! check the inverse result
    print '(i7,2x,a,2i4,2g14.6)', mpi_rank, &
    'test_fft2dby Laplace ERROR: mpi_x, mpi_z, maxval(abs(g3-g1)) =', &
      mpi_x, mpi_z, err, g1lim
    !call end_mpi
    !stop
  end if
  call max_real_mpi (err)
  if (master) print *,'test_fft2dLy: diff =', err

  deallocate (f1, f2, ft, f3, k2)
  deallocate (g1, g2, gt, g3)
END
