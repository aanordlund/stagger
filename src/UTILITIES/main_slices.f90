PROGRAM slices
  USE params
  USE variables
  implicit none
  character(len=mfile) input
  integer nt
  namelist /io/input,nt,mvar,mx,my,mz

  call init_stdio

  iscr0 = 1
  isnap0 = 1
  do_density = .true.
  do_momenta = .true.
  do_energy = .true.
  do_mhd = .false.
  do_pscalar = .false.

  rewind (stdin); read (stdin,io)
  write (*,io)

  allocate (r(mx,my,mz), px(mx,my,mz), py(mx,my,mz), pz(mx,my,mz), e(mx,my,mz))
  if (do_mhd) allocate (Bx(mx,my,mz), By(mx,my,mz), Bz(mx,my,mz))
  if (do_pscalar) allocate (d(mx,my,mz))

  call init_slice

  do iread=1,nt
    call read_snap   (r,px,py,pz,e,d,Bx,By,Bz)
    call write_slice (r,px,py,pz,e,d,Bx,By,Bz)
  end do
END
