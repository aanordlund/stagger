
 integer, parameter:: m=200, mx=m, my=m, mz=m
 real f(mx,my,mz), ft(mx,my,mz), f1(mx,my,mz)
 real cpu, cpus(2)

 do iz=1,mz
 do iy=1,my
 do ix=1,mx
   f(ix,iy,iz)=0.0001*ix+0.01*iy+iz
 end do
 end do
 end do
 cpu = dtime(cpus)
 call fft3df(f,ft,mx,my,mz)
 print *,dtime(cpus)-cpu,' sec'
 cpu = dtime(cpus)
 call fft3db(ft,f1,mx,my,mz)
 print *,dtime(cpus)-cpu,' sec'
 print *,sum((f-f1)**2)/m**3,' rms'

 END
