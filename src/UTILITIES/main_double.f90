!***********************************************************************
MODULE params
  implicit none
  real   , parameter :: pi=3.1415926536
  integer, parameter :: mfile=50, mid=72

  character (len=mid):: hl
  integer :: mx, my, mz, nstag, nflop, nstag1
  real :: sx, sy, sz, dx, dy, dz, dxm, dym, dzm
  real, allocatable, dimension(:,:,:):: scratch

  logical master, mpi_master, omp_master
  integer omp_nthreads, omp_mythread, iys, iye, izs, ize
  common /omp/omp_nthreads, omp_mythread, iys, iye, izs, ize, omp_master, master
!$omp threadprivate(/omp/)

END MODULE

#ifndef _OMP
INTEGER FUNCTION omp_get_thread_num()
  omp_get_thread_num=0
END
INTEGER FUNCTION omp_get_num_threads()
  omp_get_num_threads=1
END
LOGICAL FUNCTION omp_in_parallel()
  omp_in_parallel=.false.
END
#endif

!***********************************************************************
SUBROUTINE limits_omp (n1,n2,i1,i2)
  USE params
  implicit none
  integer n1,n2,i1,i2
  integer omp_get_num_threads, omp_get_thread_num
!
  omp_mythread = omp_get_thread_num()
  omp_nthreads = omp_get_num_threads()
  i1 = n1 + ((omp_mythread  )*n2)/omp_nthreads
  i2 =      ((omp_mythread+1)*n2)/omp_nthreads
END

SUBROUTINE print_id(id)
  USE params
  character(len=mid) id
  id=' '
END

SUBROUTINE average_subr(f,f0)
  USE params
  real, dimension(mx,my,mz):: f
  real*8 sumx,sumy,sumz
  real f0
  sumz=0.
  do iz=1,mz
    sumy=0.
    do iy=1,my
      sumx=0.
      do ix=1,mx
        sumx=sumx+f(ix,iy,iz)
      end do
      sumy=sumy+sumx
    end do
    sumz=sumz+sumy
  end do
  f0=sumz/(mx*my*mz)
END

SUBROUTINE barrier_omp (label)
  character(len=*) label
END

INTEGER FUNCTION lrec(m)
  integer m
  lrec=m
END

SUBROUTINE double(f1,f2,mx,my,mz)
  implicit none
  integer mx, my, mz
  integer izs, ize
  real, dimension(mx,my,mz):: f1
  real, dimension(mx*2,my*2,mz*2):: f2
  integer ix,iy,iz,ixp1,iyp1,izp1,ix2,iy2,iz2,ix2p1,iy2p1,iz2p1

  call limits_omp(1,mz,izs,ize)
  print'(1x,a,5i5)','double:',mx,my,mz,izs,ize
  do iz=izs,ize
    izp1=mod(iz,mz)+1
    iz2=2*(iz-1)+1
    iz2p1=mod(iz2,mz*2)+1
    do iy=1,my
      iyp1=mod(iy,my)+1
      iy2=2*(iy-1)+1
      iy2p1=mod(iy2,my*2)+1
      do ix=1,mx
        ixp1=mod(ix,mx)+1
        ix2=2*(ix-1)+1
        ix2p1=mod(ix2,mx*2)+1
	!print *,iz,iy,ixp1,ix2,ix2p1
        f2(ix2  ,iy2  ,iz2  )=f1(ix,iy,iz)
        f2(ix2p1,iy2  ,iz2  )=0.500*(f1(ix,iy,iz  )+f1(ixp1,iy  ,iz  ))
        f2(ix2  ,iy2p1,iz2  )=0.500*(f1(ix,iy,iz  )+f1(ix  ,iyp1,iz  ))
        f2(ix2p1,iy2p1,iz2  )=0.250*(f1(ix,iy,iz  )+f1(ixp1,iy  ,iz  )+f1(ix,iyp1,iz  )+f1(ixp1,iyp1,iz  ))
        f2(ix2  ,iy2  ,iz2p1)=0.500*(f1(ix,iy,iz  )+f1(ix  ,iy  ,izp1))
        f2(ix2p1,iy2  ,iz2p1)=0.250*(f1(ix,iy,iz  )+f1(ixp1,iy  ,iz  )+f1(ix,iy  ,izp1)+f1(ixp1,iy  ,izp1))
        f2(ix2  ,iy2p1,iz2p1)=0.250*(f1(ix,iy,iz  )+f1(ix  ,iyp1,iz  )+f1(ix,iy  ,izp1)+f1(ix  ,iyp1,izp1))
        f2(ix2p1,iy2p1,iz2p1)=0.125*(f1(ix,iy,iz  )+f1(ixp1,iy  ,iz  )+f1(ix,iyp1,iz  )+f1(ixp1,iyp1,iz  )+&
                                     f1(ix,iy,izp1)+f1(ixp1,iy  ,izp1)+f1(ix,iyp1,izp1)+f1(ixp1,iyp1,izp1))
      end do
    end do
  end do
END

PROGRAM main_double
  USE params
  implicit none
  integer mx1,my1,mz1,mx2,my2,mz2,mw1,mw2
  integer ix,iy,iz,lrec,omp_get_thread_num
  real, allocatable, dimension(:,:,:):: rho1,lnr1,e1,lne1,ux1,uy1,uz1,bx1,by1,bz1,scr1, &
                                        rho2,lnr2,e2,lne2,ux2,uy2,uz2,bx2,by2,bz2,scr2, &
					ax1,ay1,az1,ax2,ay2,az2,scr3
  real bx0, by0, bz0
  character(len=mfile):: fin,fout
  namelist /in/fin,fout,mx,my,mz

  read (*,in)

  mx1=mx
  my1=my
  mz1=mz
  mx2=2*mx1
  my2=2*my1
  mz2=2*mz1
  mw1=mx1*my1*mz1
  mw2=mx2*my2*mz2
  dx=1.
  dy=dx
  dz=dx
  dxm=dx
  dym=dy
  dzm=dz
    
  allocate(rho1(mx1,my1,mz1),lnr1(mx1,my1,mz1),ux1(mx1,my1,mz1),uy1(mx1,my1,mz1),uz1(mx1,my1,mz1))
  allocate(e1(mx1,my1,mz1),lne1(mx1,my1,mz1))

  allocate(scr1(mx1,my1,mz1),scr2(mx1,my1,mz1))

  allocate(rho2(mx2,my2,mz2),lnr2(mx2,my2,mz2),ux2(mx2,my2,mz2),uy2(mx2,my2,mz2),uz2(mx2,my2,mz2))
  allocate(e2(mx2,my2,mz2),lne2(mx2,my2,mz2))

  open(unit=10,file=fin,access='direct',recl=lrec(mw1),status='old')
  open(unit=11,file=fout,access='direct',recl=lrec(mw2),status='unknown')

  print*,'read rho..'
  read(10,rec=1) rho1
  print*,'read ux..'
  read(10,rec=2) ux1
  print*,'read uy..'
  read(10,rec=3) uy1
  print*,'read uz..'
  read(10,rec=4) uz1
  print*,'read e..'
  read(10,rec=5) e1

!$omp parallel private(iz,izs,ize)
  call limits_omp(1,mz,izs,ize)
  print*,omp_get_thread_num(),izs,ize
  do iz=izs,ize
    lnr1(:,:,iz)=alog(rho1(:,:,iz))
    lne1(:,:,iz)=alog(e1(:,:,iz))
  end do
  call xdn_set(lnr1,scr1)
  do iz=izs,ize
    ux1(:,:,iz)=ux1(:,:,iz)/exp(scr1(:,:,iz))
  end do
  call ydn_set(lnr1,scr1)
  do iz=izs,ize
    uy1(:,:,iz)=uy1(:,:,iz)/exp(scr1(:,:,iz))
  end do
  call zdn_set(lnr1,scr1)
  do iz=izs,ize
    uz1(:,:,iz)=uz1(:,:,iz)/exp(scr1(:,:,iz))
  end do

  call double(lnr1,lnr2,mx,my,mz)
  call double(ux1,ux2,mx,my,mz)
  call double(uy1,uy2,mx,my,mz)
  call double(uz1,uz2,mx,my,mz)
  call double(lne1,lne2,mx,my,mz)

!$omp end parallel
  
  deallocate(scr1,scr2)
  mx=mx2
  my=my2
  mz=mz2
  allocate(scr1(mx2,my2,mz2),scr2(mx2,my2,mz2),scr3(mx2,my2,mz2))
  
!$omp parallel private(iz,izs,ize)
  call limits_omp(1,mz,izs,ize)
  
  call xup_set(ux2,scr1)
  call xdn_set(lnr2,scr2)
  do iz=izs,ize
    ux2(:,:,iz)=scr1(:,:,iz)*scr2(:,:,iz)
  end do
  
  call yup_set(uy2,scr1)
  call ydn_set(lnr2,scr2)
  do iz=izs,ize
    uy2(:,:,iz)=scr1(:,:,iz)*scr2(:,:,iz)
  end do
  
  !$omp barrier
  call zup_set(uz2,scr1)
  call zdn_set(lnr2,scr2)
  !$omp barrier
  do iz=izs,ize
    uz2(:,:,iz)=scr1(:,:,iz)*scr2(:,:,iz)
    rho2(:,:,iz)=exp(lnr2(:,:,iz))
    e2(:,:,iz)=exp(lne2(:,:,iz))
  end do

!$omp end parallel

  print*,'write rho..'
  write(11,rec=1) rho2
  print*,'write ux..'
  write(11,rec=2) ux2
  print*,'write uy..'
  write(11,rec=3) uy2
  print*,'write uz..'
  write(11,rec=4) uz2
  print*,'write e..'
  write(11,rec=5) e2
  
  deallocate(rho1,lnr1,ux1,uy1,uz1)
  deallocate(rho2,lnr2,ux2,uy2,uz2)

  allocate(bx1(mx1,my1,mz1),by1(mx1,my1,mz1),bz1(mx1,my1,mz1))
  allocate(ax1(mx1,my1,mz1),ay1(mx1,my1,mz1),az1(mx1,my1,mz1))

  allocate(ax2(mx2,my2,mz2),ay2(mx2,my2,mz2),az2(mx2,my2,mz2))

  print*,'read bx..'
  read(10,rec=6) bx1
  print*,'read by..'
  read(10,rec=7) by1
  print*,'read bz..'
  read(10,rec=8) bz1
  close(10)
  
  mx=mx1
  my=my1
  mz=mz1
  dx=1.
  dy=dx
  dz=dx

  call average_subr (bx1, bx0)
  call average_subr (by1, by0)
  call average_subr (bz1, bz0)
  print *,'aver:',bx0,by0,bz0

!$omp parallel
  call xup_set(bx1,scr1)
  call yup_set(by1,scr2)
  call zup_set(bz1,scr3)
  call vectorpot (scr1,scr2,scr3,ax1,ay1,az1,mx1,my1,mz1,dx,dy,dz)
  call double (ax1,ax2,mx1,my1,mz1)
  call double (ay1,ay2,mx1,my1,mz1)
  call double (az1,az2,mx1,my1,mz1)
!$omp end parallel

  deallocate(bx1,by1,bz1)
  deallocate(ax1,ay1,az1,scr1,scr2,scr3)
  allocate(scr1(mx2,my2,mz2))
  allocate(bx2(mx2,my2,mz2),by2(mx2,my2,mz2),bz2(mx2,my2,mz2))
  
  mx=mx2
  my=my2
  mz=mz2
  dx=0.5
  dy=dx
  dz=dx

!$omp parallel
  call ydn_set(ax2,scr1)
!$omp barrier
  call zdn_set(scr1,ax2)
!$omp barrier
  call zdn_set(ay2,scr1)
  call xdn_set(scr1,ay2)
!$omp barrier
  call xdn_set(az2,scr1)
  call ydn_set(scr1,az2)

  call ddyup_set(az2,bx2)
!$omp barrier
  call ddzup_add(ay2,bx2)
  call ddzup_set(ax2,by2)
!$omp barrier
  call ddxup_add(az2,by2)
  call ddxup_set(ay2,bz2)
  call ddyup_add(ax2,bz2)
!$omp end parallel
  
  print*,'write bx..'
  write(11,rec=6) bx2+bx0
  print*,'write by..'
  write(11,rec=7) by2+by0
  print*,'write bz..'
  write(11,rec=8) bz2+bz0
  close(11)

  deallocate(ax2,ay2,az2,scr1)
  deallocate(bx2,by2,bz2)
END
