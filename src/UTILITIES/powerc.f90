! $Id: powerc.f90,v 1.2 2005/01/13 13:24:06 aake Exp $
!***********************************************************************
  subroutine powerc (ut, nx, ny, nz, dx, dy, dz, p, xk, mk)

  implicit none
  real, parameter:: pi=3.14159265
  integer nx, ny, nz, i, j, k, ik, mk, nk(mk)
  real:: dx, dy, dz, kx, ky, kz, k2, kk, c, ptot, p2, o, parseval
  real, dimension(mk):: p, xk
  complex:: ut(nx,ny,nz)
  character:: id*72 = "$Id: powerc.f90,v 1.2 2005/01/13 13:24:06 aake Exp $"
!-----------------------------------------------------------------------

  if (id .ne. '') then
    print *,id
    id = ''
  end if

!$ doacross local(k,j,i,kz,ky,kx,kk,ik), shared(p,ut)
  p = 0.
  o = 0.18              ! empirical offset -- see power3d.pro
  nk = 0
  parseval = 0.
  do k=1,nz
    kz = min(k-1,nz-k+1)
    do j=1,ny
      ky = min(j-1,ny-j+1)
      do i=1,nx
        kx = min(i-1,nx-i+1)
        k2 = kx**2 + ky**2 + kz**2
        kk = sqrt(k2)
        ik = 1.5 + kk - o
        if (ik .le. mk) then
          p2 = abs(ut(i,j,k))**2
          p(ik) = p(ik) + k2*p2
          parseval = parseval + p2
          xk(ik) = xk(ik) + kk
          nk(ik) = nk(ik) + 1
        end if
      end do
    end do
  end do

  xk(1) = 0.
  p = p/(nk+1e-6)
  xk = xk/(nk+1e-6)
  ptot = sum(p)
  p = p*parseval/ptot

  end
