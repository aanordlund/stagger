! $Id: permute_mpi.f90,v 1.28 2012/12/31 16:19:17 aake Exp $

!***********************************************************************
MODULE permute_mod
  USE mpi_mod
  implicit none
  real, allocatable, dimension(:,:,:,:):: snd                           ! send buffer (do NOT change!)
  real, allocatable, dimension(:,:,:):: rcv                             ! recieve buffer
  integer, allocatable, dimension(:):: sndrq                            ! send requests
  integer, allocatable, dimension(:,:):: snd_status                     ! send status
  integer nx, ny, nz, m, nreq, ierr, status(MPI_STATUS_SIZE)
  logical, save:: first_time=.true., debug_permute=.false.
END MODULE

!***********************************************************************
SUBROUTINE init_permute
  USE params
  USE mpi_mod
  USE permute_mod
  implicit none
  character(len=mid):: id="$Id: permute_mpi.f90,v 1.28 2012/12/31 16:19:17 aake Exp $"
!-----------------------------------------------------------------------
  call print_id (id)
!  if (omp_nz > 1) then
!    if (master) print *,'permute_mpi.f90 does not work with OpenMP!'    ! no OMP, no hybrid
!    call end_mpi
!  end if
  first_time = .false.
  m = min(mx,my,mz)                                                     ! smallest dimension
  nx = mx/m                                                             ! number of boxes in x
  ny = my/m                                                             ! number of boxes in y
  nz = mz/m                                                             ! number of boxes in z
  if (mx > nx*m .or. my > ny*m .or. mz > nz*m) then
    if (master) then
      print *,"fft3d_2_mpi ERROR: MPI domain sizes don't match", mx, my, mz
    end if
    stop
  end if
  allocate (sndrq(nx*ny*nz))                                            ! send reqs
  allocate (snd(m,m,m,nx*ny*nz))                                        ! send buffer
  allocate (snd_status(MPI_STATUS_SIZE,nx*ny*nz))                       ! status buffer
  allocate (rcv(m,m,m))                                                 ! recv buffer
END SUBROUTINE

!***********************************************************************
SUBROUTINE permute (f1, f2)
!
! Note that this routine uses ISSEND / IRECV, which means that the send
! buffer must remain in existence until the data has been received, which
! in general does not happen in the same order as data is being sent in.
! Therefore it is necessary to have a full size send buffer on each node.
! Using another pair of MPI-routines would just shift the responsibility
! of keeping the data buffered to the MPI-system, and no memory would be
! gaoined.
!
  USE params
  USE permute_mod
  USE mpi_mod
  implicit none
  real, dimension(mx,my,mz):: f1, f2
  intent(in)  :: f1
  intent(out) :: f2
  integer ix, iy, iz, jx, jy, jz, kx, ky, kz, coord(3), tag, rank, req
!-----------------------------------------------------------------------
  !$omp master
  if (first_time) call init_permute                                     ! initialize
  nreq = 0                                                              ! reset request counter
  kx = mpi_x*mx                                                         ! global x-index of domain
  ky = mpi_y*my                                                         ! global y-index of domain
  kz = mpi_z*mz                                                         ! global z-index of domain

  do jz=0,mz-1,m; do jy=0,my-1,m; do jx=0,mx-1,m                        ! SEND
    coord = (/ (jy+ky)/mx, (jz+kz)/my, (jx+kx)/mz /)                    ! receiver MPI-coordinates
    call MPI_CART_RANK (new_com, coord, rank, ierr)                     ! who to send to
    tag = (jx+kx)/m + (my*mpi_ny)*((jy+ky)/m + (mz*mpi_nz)*(jz+kz)/m)   ! package tag
    nreq = nreq+1                                                       ! increment request counter
    do iz=1,m; do iy=1,m; do ix=1,m                                     ! copy to send buffer
      snd(iy,iz,ix,nreq) = f1(ix+jx,iy+jy,iz+jz)                        ! SEE THE COMMENTS ABOVE
    end do; end do; end do
    call MPI_ISSEND (snd(1,1,1,nreq), m*m*m, MPI_REAL, rank, tag, &     ! start send to her ..
                     new_com, sndrq(nreq), ierr)                        ! .. saving rq
  end do; end do; end do

  call MPI_BARRIER (new_com, ierr)                                      ! make sure all sent

  do jz=0,mz-1,m; do jy=0,my-1,m; do jx=0,mx-1,m                        ! RECIEVE
    coord = (/ (jz+kz)/mx, (jx+kx)/my, (jy+ky)/mz /)                    ! sender MPI-coordinates
    call MPI_CART_RANK (new_com, coord, rank, ierr)                     ! who to rcv from
    tag = (jz+kz)/m + (my*mpi_ny)*((jx+kx)/m + (mz*mpi_nz)*(jy+ky)/m)   ! package tag
    call MPI_IRECV (rcv, m*m*m, MPI_REAL, rank, tag, new_com, req, ierr)! recv from him ..
    call MPI_WAIT (req, status, ierr)                                   ! wait for completion
    do iz=1,m; do iy=1,m; do ix=1,m
      f2(ix+jx,iy+jy,iz+jz) = rcv(ix,iy,iz)                             ! put in place
    end do; end do; end do
  end do; end do; end do
  call MPI_WAITALL (nreq, sndrq, snd_status, ierr)                      ! wait on sndrq, to free sndrq
  !$omp end master
END SUBROUTINE

!***********************************************************************
SUBROUTINE test_permute
  USE params
  USE permute_mod
  implicit none
  real, allocatable, dimension(:,:,:):: fft1, fft2
  integer ix, iy, iz, jx, jy, jz
  real tmp
  logical ok
  integer, parameter:: mprint=10
  integer nprint
!-----------------------------------------------------------------------
  !$omp master
  !call barrier_mpi('test')
  allocate (fft1(mx,my,mz), fft2(mx,my,mz))
  nprint = 0
  do iz=1,mz
    jz = iz+mpi_z*mz
    do iy=1,my
      jy = iy+mpi_y*my
      do ix=1,mx
        jx = ix+mpi_x*mx
        fft1(ix,iy,iz) = jz+1000*(jy+1000*jx)
        fft2(ix,iy,iz) = jz+1000*(jy+1000*jx)
      end do
    end do
  end do
  call permute (fft1, fft2)
  ok = .true.
  do iz=1,mz
    jz = iz+mpi_z*mz
    do iy=1,my
      jy = iy+mpi_y*my
      do ix=1,mx
        jx = ix+mpi_x*mx
        tmp = jy+1000*(jx+1000*jz)
        if (abs(tmp-fft2(ix,iy,iz)) > 1e-2) then
          if (nprint < mprint) print 1, mpi_rank, ix, iy, iz, tmp, fft2(ix,iy,iz)
          nprint = nprint+1
          ok = .false.
        end if
      end do
    end do
  end do
  deallocate (fft1, fft2)
  if (.not. ok) print *, mpi_rank, omp_mythread, 'test_permute', ok, nprint
1 format(i6,3i4,2f10.0)
  !$omp end master
END SUBROUTINE
