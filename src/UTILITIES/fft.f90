MODULE FFT

USE params
USE mpi_mod

logical,save :: firstthrough
integer(KIND=8) :: plan, iplan
integer,parameter :: FFTW_FORWARD=-1,FFTW_BACKWARD=1
integer,parameter :: FFTW_REAL_T0_COMPLEX=-1,FFTW_COMPLEX_TO_REAL=1
integer,parameter :: FFTW_ESTIMATE=0,FFTW_MEASURE=1
integer,parameter :: FFTW_OUT_OF_PLACE=0,FFTW_IN_PLACE=8,FFTW_USE_WISDOM=16
integer,parameter :: FFTW_THREADSAFE=128

CONTAINS

!MPI-version

SUBROUTINE fft_2d(data,dir,dim,data_out)

integer,save :: size11,size22
integer :: dir,size1,size2
integer,dimension(3) :: dim,plan_dim
integer,dimension(3),save :: old_dim
real,dimension(mx,my,mz) :: data
real,dimension(mx,my,mz) :: data_out
logical :: new_needed
integer,dimension(2,mpi_size) :: coord
data new_needed /.false./

include 'mpif.h'
!*** dim(:) defines the area over which the fourier transform is made
!*** a value of 0 means that the whole range of that coordinate is used
!*** EXAMPLES: Making a fourier transform over r(:,lb,:), would be done
!*** by passing dim=[0,lb,0] to this procedure
!*** This implementation cannot yet do fourier transforms over a sub-range

do i=1,3
  if (dim(i) == 0) then
    if (old_dim(i) /= 0) new_needed=.true.
  else
    if (old_dim(i) == 0) new_needed=.true.
  end if
end do

if (new_needed) then
!  call fftw_f77_destroy_plan(plan)
!  call fftw_f77_destroy_plan(iplan)

  where(dim == 0) 
    plan_dim=(/mx*mpi_nx,my*mpi_ny,mz*mpi_nz/)
  elsewhere
    plan_dim=1
  end where

#if defined (_OPENMP)
  if (omp_master) then
    call rfftwnd_f77_create_plan(plan,3,plan_dim,FFTW_REAL_TO_COMPLEX,FFTW_ESTIMATE+FFTW_THREADSAFE)
    call rfftwnd_f77_create_plan(iplan,3,plan_dim,FFTW_COMPLEX_TO_REAL,FFT_ESTIMATE+FFTW_THREADSAFE)
  end if
#else      
  if (master) print*,'dim check',plan_dim
  call MPI_BARRIER(new_com,mpi_err)
  call rfftwnd_f77_mpi_create_plan(plan ,new_com,3,plan_dim,FFTW_REAL_TO_COMPLEX,FFTW_ESTIMATE)
  call rfftwnd_f77_mpi_create_plan(iplan,new_com,3,plan_dim, &
       FFTW_COMPLEX_T0_REAL,FFTW_ESTIMATE)
!  call rfftwnd_f77_mpi_create_plan(new_com,iplan,3,plan_dim, &
!       FFTW_COMPLEX_TO_REAL,FFTW_ESTIMATE)
  print*,'plans',mpi_rank,plan,iplan
#endif
  
  old_dim=dim

end if

if (dir == -1) then

#if defined (_OPENMP)
   call rfftwnd_f77_threads_nd(omp_nthreads,plan,data,data_out)
#else
   call rfftwnd_f77_mpi_local_sizes(plan,lnx,lxstart,lny_at,lystart_at,memreq)
   print*,mpi_rank,lnx,lxstart,lny_at,lystart_at
   call MPI_ALLGATHER((/lnx,lxstart/),2,MPI_INTEGER,coord,mpi_size,MPI_REAL,new_com,mpi_err)
   if (master) print*,'Master',mpi_rank,coord
!   allocate(scr1(lny_at,lnx),scr2(lny_at,lnx))
!   print*,'checking data out',mpi_rank,lxstart,lnx,lystart_at,lny_at
!   print*,'checking input   ',mpi_rank,shape(data)
!   call rfftwnd_f77_mpi(plan,(/size1,size2+1/),data)
!   print*,mpi_rank,data(1,1)
!***   do i=1,lny
!***      scr1(i,:)=data(i,:)
!***   end do
!***   call distribute_data
!***   call rfftw2d_mpi(plan,nr_of_rows,scr1,scr2)
!***   call return_data
#endif

elseif (dir == 1) then 

#if defined (_OPENMP)
!***   call rfftw2d_f77_threads_2d(omp_nthreads,iplan,data,data_out)
#else
!***   call distribute_data
!***   call rfftw2d_mpi(iplan,nr_of_rows,data,scr)
!***   call return_data
#endif

else
   if (master) print*,'Direction seems to be wrong?',dir
   stop
endif

end SUBROUTINE fft_2d

!*********

SUBROUTINE distribute_data2(dim,coord,data,f)

Implicit none
integer :: i,j,k,cpui,cpuj,cpuk,rank_send,rank_recv
integer :: x1,x2,x3,x4,y1,y1,y2,y3,y4

if (dim(3) == 0) then
  do k=1,mz*(mpi_nz+1)
    if ((k > coord(mpi_rank,1)).and.(k < sum(coord(mpi_rank,:)))) then
! WE NEED THE DATA, CHECK IF WE ALREADY HAVE IT
      if ((k > mz*mpi_nz).and.(k < (mpi_nz+1)*mz)) then
        call get_limits_recv(mpi_x,mpi_y,x1,x2,y1,y2,dim)
        call get_limits_send(mpi_x,mpi_y,x3,x4,y3,y4,dim)
        f(x1:x2,y1:y2,k-coord(1,mpi_rank)+1)=data(x3:x4,y3:y4,kk-mpi_z*mz+1)
      else
        if (dim(1) == 0) then
          cpux1=0
          cpux2=mpi_nx-1
        else
          cpux1=int(dim(1)/(1.0*mx))
          cpux2=cpux1
        end if
        if (dim(2) == 0) then 
          cpuy1=0
          cpuy2=cpu_ny-1
        else
          cpuy1=int(dim(2)/(1.0*my))
          cpuy2=cpuy1
        end if
        do cpui=cpux1,cpux2
          do cpuj=cpuy1,cpuy2
            call get_limits_recv(cpui,cpuj,x1,x2,y1,y1,dim)
            call MPI_CART_RANK(new_com,(/cpui,cpuj,mpi_z/),recv_rank,mpi_err)
            
            call MPI_IRECV(f(x1:x2,y1:y2,k-coord(1,mpi_rank)+1), &
                 (x2-x1+1)*(y2-y1+1)*1,MPI_REAL,recv_rank,k,new_com, &
                 mpi_request,mpi_err)
          end do
        end do
      end if
    else
      call get_limits_send(mpi_x,mpi_y,x3,x4,y3,y4,dim)
      rank_send=-1
      i=0
      do while ( rank_send == -1)
        i=i+1
        if ((k > coord(i,1)).and.(k < sum(coord(i,:)))) rank_send=i-1
      end do
      call MPI_ISEND(data(x3:x4,y3:y4,k-mpi_z*mz+1), &
           (x4-x3+1)*(y4-y3+1)*1,MPI_REAL,send_rank,k,new_com, &
           mpi_request,mpi_err)
    end if
  end do
else if (dim(2) == 0) then 
  do j=1,my*(mpi_ny+1)
    if ((dim(3) > mpi_z*mz).and.(dim(3) < (mpi_z+1)*mz)) then

      if ((j > coord(mpi_rank,1)).and.(j < sum(coord(mpi_rank,:)))) then
        if ((j > mpi_y*my).and.(j < (mpi_y+1)*my)) then
          call get_limits_recv(mpi_x,mpi_y,x1,x2,y1,y2,dim)
          call get_limits_send(mpi_x,mpi_y,x3,x4,y3,y4,dim)
          f(x1:x2,j-coord(1,mpi_rank)+1,1)=data(x3:x4,j-mpi_y*my+1,dim(3)-mpi_nz*mz)
        else
          cpuk=int(dim(3)/(1.0*mz))
          cpuj=int(j/(1.0*my))
          do cpui=1,mpi_nx
            call get_limits_recv(cpui,cpuj,x1,x2,y1,y2,dim)
            call MPI_CART_RANK(new_com,(/cpui,cpuj,cpuk/), &
                 rank_recv,mpi_err)
            call MPI_IRECV(f(x1:x2,j-coord(mpi_rank,1)+1,1), &
                 (x2-x1+1)*1*1,MPI_REAL,rank_recv,j,new_com, &
                 mpi_request,mpi_err)
          end do
        end if
      else
        call get_limits_send(mpi_x,mpi_y,x3,x4,y3,y4,dim)
        rank_send=-1
        i=0
        do while (rank_send == -1)
          i=i+1
          if ((j > coord(i,1)).and.(j < sum(coord(i,:)))) rank_send=i-1
        end do
        call MPI_ISEND(data(x3:x4,j-mpi_y*my+1,dim(3)-mpi_z*,z+1), &
             (x4-x3+1)*1*1,MPI_REAL,send_rank,j,new_com, &
             mpi_request,mpi_err)
      end if
    else
      if ((j > coord(mpi_rank,1)).and.(j < sum(coord(mpi_rank,:)))) then
        cpuk=int(dim(3)/(1.0*mz))
        cpuj=int(j/(1.0*my))
        do cpui=1,mpi_nx
          call get_limits_recv(cpui,cpuj,x1,x2,y1,y2,dim)
          call MPI_CART_RANK(new_com,(/cpui,cpuj,cpuk/), &
               rank_recv,mpi_err)
          call MPI_IRECV(f(x1:x2,j-coord(mpi_rank,1)+1,1), &
               (x2-x1+1)*1*1,MPI_REAL,rank_recv,j,new_com, &
               mpi_request,mpi_err)
        end do
      end if
    end if
  end do
end if

END SUBROUTINE distribute_data2

!***********************************
SUBROUTINE get_limits_recv(cpui,cpuj,x1,x2,y1,y2,dim)

integer :: cpui,cpuj,x1,x2,y1,y2
integer,dimension(3) :: dim

if (dim(1) == 0) then
  x1=cpui*mx+1
  x2=(cpui+1)*mx
else
  x1=1
  x2=x1
end if

if (dim(2) == 0) then
  if (dim(3) == 0) then 
    y1=cpuj*my+1
    y2=(cpuj+1)*my
  else 
    y1=0 ! Should be j-coord(mpi_rank,1), but will be set outside
    y2=0
  end if
else
  y1=1
  y2=1
end if

END SUBROUTINE get_limits_recv

!***********************************
SUBROUTINE get_limits_send(cpui,cpuj,x3,x4,y3,y4,dim)

integer :: cpui,cpuj,x3,x4,y3,y4
integer,dimension(3) :: dim

if (dim(1) == 0) then
  x3=1
  x4=mx
else
  x3=dim(1)-(mx*cpui)
  x4=dim(1)-(mx*cpui)
end if

if (dim(2) == 0) then
  if (dim(3) == 0) then
    y3=1
    y4=my
  else
    y3=1  ! Should in principle be j, but this will be set outside
    y4=1
  end if
else
  y3=dim(2)-mpi_y*my
  y4=dim(2)-mpi_y*my
end if

END SUBROUTINE get_limits_send
!***********************************

SUBROUTINE distribute_data(data,f2,dim,coord)

integer :: dir,num
integer :: mpi_err,cpui,cpuj,cpuk
integer,dimension(2) :: dlim_x,dlim_y,dlim_z,dim
integer,dimension(2,mpi_size) :: coord
real,dimension(mx,my,mz) :: data
real,dimension(:,:,:), pointer :: f2
real,dimension(:,:,:),allocatable,target :: f
!** Lots of communication to get data at right place **!

if (dim(1) == 0) then 
  dlim_x=(/1,mpi_nx*mx/)
else
  dlim_x(:)=dim(1)
end if

if (dim(2) == 0) then
  dlim_y=(/1,mpi_ny*my/)
else
  dlim_y(:)=dim(2)
end if

if (dim(3) == 0) then
  dlim_z=(/1,mpi_nz*mz/)
else
  dlim_z(:)=dim(3)
end if


allocate(f(1:(dlim_x(2)-dlim_x(1)+1),1:dlim_y(2)-dlim_y(1)+1,1:dlim_z(2)-dlim_z(1)+1))



! -------- Doing the ugly work ---------
if (dlim_x(1) /= dlim_x(2)) then 
!*** Get data from other cpus, in rows of x (max 3d)

  do i=dlim_x(1),dlim_x(2)
    cpui=int(i/(1.0*mx))
    if ((i >= coord(1,mpi_rank)).and. &
        (i <=(coord(1,mpi_rank)+coord(2,mpi_rank)-1))) then

!*** Needs to have this data
      do cpuj=1,mpi_ny
        do cpuk=1,mpi_nz
          if ((cpui == mpi_x).and.(cpuj == mpi_y).and.(cpuk == mpi_z)) then 
!*** copy in data which is already here
            f(i-coord(1,mpi_rank)+1,cpuj*my+1:(cpuj+1)*my, &
                 cpuk*mz+1:(cpuk+1)*mz)=data(i-mpi_x*mx+1,:,:)
          else
!*** Get rest from others
            call MPI_CART_RANK(new_com,(/cpui,cpuj,cpuk/), &
                 recv_rank,mpi_err)
            call MPI_IRECV(f(i-coord(1,mpi_rank)+1,        &
                 (cpuj*my)+1:((cpuj+1)*my) ,               &
                 (cpuk*mz)+1:((cpuk+1)*mz)),               &
                 1*my*mz,MPI_REAL,recv_rank,i,new_com,mpi_request,mpi_err)
          end if
        end do
      end do
!    else if ((i >= (mpi_x+1)*mx).and.(i < (mpi_x+2)*mx)) 
    else if (cpui == mpi_x) then
!*** Should send this data      
      do ii=1,mpi_size
        if ((i >= coord(1,ii)).and.(i < (coord(1,ii)+coord(2,ii)))) then
          call MPI_ISEND(data(i-mpi_x*mx+1,:,:),        &
               1*my*mz,MPI_REAL,ii ,i,new_com,mpi_request,mpi_err)
        end if
      end do
    end if
  end do

elseif (dlim_y(1) /= dlim_y(2)) then

!*** Get data from other cpus, in rows of y (max 2d)
  cpui=dim(1)/(1.0*mx)
  do j=dlim_y(1),dlim_y(2)
    cpuj=j/(1.0*my)
    if ((j >= coord(1,mpi_rank)).and.(j<= coord(1,mpi_rank)+coord(2,mpi_rank)-1)) then
  !*** Needs to have this data
      do cpuk=1,mpi_nz
        if ((cpuj == mpi_y).and.(cpuk == mpi_z)) then 
          f(1,j-coord(1,mpi_rank),cpuk*mz+1:((cpuk+1)*mz))= &
               data(dim(1)-mpi_x*mx+1,j-mpi_y*my+1,1:mz)
        else
          call MPI_CART_RANK(new_com,(/cpui, & 
               cpuj,cpuk/),recv_rank,mpi_err)
          call MPI_IRECV(f(1,j-coord(1,mpi_rank),cpuk*mz+1:((cpuk+1)*mz)), & 
               1*1*mz,MPI_REAL,recv_rank,j,new_com,mpi_request,mpi_err)
        end if
      end do
    elseif ((j >= mpi_y*my).and.(j < (mpi_y+1)*my)) then 
      do jj=1,mpi_size
        if ((j >= coord(1,jj)).and.(j < (coord(1,jj)+coord(2,jj)))) then
          call MPI_ISEND(data(dim(1)-mpi_x*mx+1,j-coord(1,mpi_rank),1:mz), &
               1*1*mz,MPI_REAL,jj,j,new_com,mpi_request,mpi_err)
        end if
      end do
    end if
  end do
else

  cpui=int(dim(1)/(1.0*mx))
  cpuj=int(dim(2)/(1.0*my))
!*** 1d along z (Why?!! - for completeness)
!*** only data along one z-stencil is needed and only one cpu can do the job
  if (((dim(1) > mpi_x*mx).and.(dim(1) <= (mpi_x+1)*mx)).and. &
      ((dim(2) > mpi_y*my).and.(dim(2) <= (mpi_y+1)*my))) then
    
    if (coord(2,mpi_rank) /= 1) then
!*** We need the data    
!*** We have the right stencil so do a copy
      f(1,1,mpi_z*mz:(mpi_z+1)*mz-1)=data(dim(1)-mpi_x*mx+1,dim(2)-mpi_y*my+1,:)
    else
      do k=1,mpi_size
        if (coord(2,k) == mpi_nz*mz) then
          call MPI_ISEND(data(int(dim(1)/(mx*1.0)),int(dlim_y(2)/(1.0*my)),:),1*1*mz,MPI_REAL,k,mpi_z,new_com,mpi_status,mpi_err)
        end if
      end do
    end if
  elseif (coord(2,mpi_rank) /= 1) then
    do k=0,mpi_nz-1
      call MPI_CART_RANK(new_com,(/cpui,cpuj,k/),rank_recv,mpi_err)
      call MPI_IRECV(f(1,1,k*mz+1:(k+1)*mz),mz,MPI_REAL,rank_recv,k,new_com,mpi_status,mpi_err)
    end do
  end if
end if

f2 => f

end SUBROUTINE distribute_data

end MODULE FFT
