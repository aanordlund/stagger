! $Id: interp_bob.f90,v 1.2 2016/11/09 22:04:57 aake Exp $
!******************************************************************************
MODULE interp_mod
  real, allocatable, dimension(:):: xm1,  ym1,  zm1,  xmdn1,  ymdn1,  zmdn1, &
                                   dxm1, dym1, dzm1, dxmdn1, dymdn1, dzmdn1, &
                                   dxidxdn1, dyidydn1, dzidzdn1, &
                                   dxidxup1, dyidyup1, dzidzup1
  real, allocatable, dimension(:):: xm2,  ym2,  zm2,  xmdn2,  ymdn2,  zmdn2, &
                                   dxm2, dym2, dzm2, dxmdn2, dymdn2, dzmdn2, &
                                   dxidxdn2, dyidydn2, dzidzdn2, &
                                   dxidxup2, dyidyup2, dzidzup2
CONTAINS
!-------------------------------------------------------------------------------
SUBROUTINE interp(f1,nx1,ny1,nz1,lb1,ub1,f2,nx2,ny2,nz2,lb2,ub2,kx,ky,kz)
  implicit none
  real, dimension(nx1,ny1,nz1):: f1
  real, dimension(nx2,ny2,nz2):: f2
  integer nx1, ny1, nz1, lb1, ub1, nx2, ny2, nz2, kx, ky, kz, ix1, iz1
  integer ix, iy, iz, jx, jy, jz, lb2, ub2, jx1, jy1, jz1
  real px, qx, dx2, py, qy, dy2, pz, qz, dz2
  real, allocatable, dimension(:,:,:):: f3, f4, f5
!...............................................................................
  dy2 = real(ub1-lb1)/real(ub2-lb2)

  allocate (f3(nx1,ny2,nz1))
  !$omp do private(iz,iy,ix,py,jy,qy)
  do iz=1,nz1
    do iy=1,ny2
      py = lb1 + (iy-lb2-ky*0.5)*dy2 + ky*0.5                                   ! when ky=0, iy=lb   is mapped to jy=lb
      jy = py                                                                   ! when ky=1, iy=lb+� is mapped to jy=lb+�
      py = py-jy
      qy = 1.-py
      jy = mod(jy-1+ny1,ny1)+1
      jy1 = mod(jy+ny1,ny1)+1
      do ix=1,nx1
        f3(ix,iy,iz) = qy*f1(ix,jy,iz) + py*f1(ix,jy1,iz)
      end do
    end do
  end do
  print*,'f3:',minval(f3),maxval(f3)

  dx2 = real(nx1)/real(nx2)
  allocate (f4(nx2,ny2,nz1))
  !$omp do private(iz,iy,ix,px,jx,qx)
  do iz=1,nz1
    do iy=1,ny2
      do ix=1,nx2
        px = 1 + (ix-1-kx*0.5)*dx2 + kx*0.5                                     ! when kx=0, ix=1   is mapped to jx=1
        jx = px                                                                 ! when kx=1, ix=1+� is mapped to jx=l+�
        px = px-jx
        qx = 1.-px
        jx = mod(jx-1+nx1,nx1)+1
        jx1 = mod(jx+nx1,nx1)+1                                                 ! jx can map to nx
        f4(ix,iy,iz) = qx*f3(jx,iy,iz) + px*f3(jx1,iy,iz)
      end do
    end do
  end do
  deallocate (f3)
  print*,'f4:',minval(f4),maxval(f4)

  dz2 = real(nz1)/real(nz2)
  !$omp do private(iz,iy,ix,pz,jz,qz)
  do iz=1,nz2
    pz = 1 + (iz-1-kz*0.5)*dz2 + kz*0.5                                         ! when kz=0, iz=1   is mapped to jz=1
    jz = pz                                                                     ! when kz=1, iz=1+� is mapped to jz=l+�
    pz = pz-jz
    qz = 1.-pz
    jz = mod(jz-1+nz1,nz1)+1
    jz1 = mod(jz+nz1,nz1)+1                                                     ! jz can map to nz
    do iy=1,ny2
      do ix=1,nx2
        f2(ix,iy,iz) = qz*f4(ix,iy,jz) + pz*f4(ix,iy,jz1)
      end do
    end do
  end do
  deallocate (f4)
END SUBROUTINE
!-------------------------------------------------------------------------------
SUBROUTINE interp2 (f1,nx1,ny1,nz1,lb1,ub1,f2,nx2,ny2,nz2,lb2,ub2,kx,ky,kz,ym1,ym2)
  implicit none
  real, dimension(nx1,ny1,nz1):: f1
  real, dimension(nx2,ny2,nz2):: f2
  integer nx1, ny1, nz1, lb1, ub1, nx2, ny2, nz2, kx, ky, kz, ix1, iz1
  integer ix, iy, iz, jx, jy, jz, lb2, ub2, jx1, jz1, iy2
  real px, qx, dx2, py, qy, dy2, pz, qz, dz2, y1, y2, dy1
  real ym1(ny1), ym2(ny2)
  real, allocatable, dimension(:,:,:):: f3, f4, f5
!...............................................................................

  allocate (f3(nx1,ny2,nz1))
  jy=1
  do iy=1,ny2
    iy2 = min(iy,ny2-1)  ; dy2=ym2(iy2+1)-ym2(iy2); y2=ym2(iy)-ky*dy2*0.5
    dy1=ym1(jy+1)-ym1(jy); y1=ym1(jy)-ky*dy1*0.5
    do while (ym1(jy+1)-ky*dy1*0.5 < y2 .and. jy < ny1-1)
      jy=jy+1
      dy1=ym1(jy+1)-ym1(jy); y1=ym1(jy)-ky*dy1*0.5
    end do
    py = (y2-y1)/dy1
    qy = 1.-py
    print*,iy2,jy,py
    !$omp do private(iz,ix)
    do iz=1,nz1
      do ix=1,nx1
        f3(ix,iy,iz) = qy*f1(ix,jy,iz) + py*f1(ix,jy+1,iz)
      end do
    end do
  end do
  print*,'f3:',minval(f3),maxval(f3)

  dx2 = real(nx1)/real(nx2)
  allocate (f4(nx2,ny2,nz1))
  !$omp do private(iz,iy,ix,px,jx,qx)
  do iz=1,nz1
    do iy=1,ny2
      do ix=1,nx2
        px = 1 + (ix-1-kx*0.5)*dx2 + kx*0.5                                     ! when kx=0, ix=1   is mapped to jx=1
        jx = px                                                                 ! when kx=1, ix=1+� is mapped to jx=l+�
        px = px-jx
        qx = 1.-px
        jx = mod(jx-1+nx1,nx1)+1
        jx1 = mod(jx+nx1,nx1)+1                                                 ! jx can map to nx
        f4(ix,iy,iz) = qx*f3(jx,iy,iz) + px*f3(jx1,iy,iz)
      end do
    end do
  end do
  deallocate (f3)
  print*,'f4:',minval(f4),maxval(f4)

  dz2 = real(nz1)/real(nz2)
  !$omp do private(iz,iy,ix,pz,jz,qz)
  do iz=1,nz2
    pz = 1 + (iz-1-kz*0.5)*dz2 + kz*0.5                                         ! when kz=0, iz=1   is mapped to jz=1
    jz = pz                                                                     ! when kz=1, iz=1+� is mapped to jz=l+�
    pz = pz-jz
    qz = 1.-pz
    jz = mod(jz-1+nz1,nz1)+1
    jz1 = mod(jz+nz1,nz1)+1                                                     ! jz can map to nz
    do iy=1,ny2
      do ix=1,nx2
        f2(ix,iy,iz) = qz*f4(ix,iy,jz) + pz*f4(ix,iy,jz1)
      end do
    end do
  end do
  deallocate (f4)
END SUBROUTINE
!-----------------------------------------------------------------------
SUBROUTINE spline(n,x,f,d)
! 
!  Find the derivatives at the knots (x) of a cubic spline with
!  continuous second order derivatives, passsing through the function
!  values (f).  The boundary conditions on the spline are continuous
!  third derivatives at k=2 and k=2n-1.
!
!  This version of spline is compatible with harwell tb04a.
!-----------------------------------------------------------------------
    integer,                    intent(in)  :: n
    real(kind=8), dimension(n), intent(in)  :: x,f
    real(kind=8), dimension(n), intent(out) :: d
    real(kind=8), dimension(3,n)            :: w
    real(kind=8)                            :: c,cxa,cxb,cxc,dfa,dfb,dfc
    integer                                 :: n1,k,kk

! first point
    if (n.eq.1) then
      d(1)=0.0
      return
    endif
    cxb=1./(x(2)-x(1))
    cxc=1./(x(3)-x(2))
    dfb=f(2)-f(1)
    dfc=f(3)-f(2)
    w(1,1)=cxb*cxb
    w(3,1)=-cxc*cxc
    w(2,1)=w(1,1)+w(3,1)
    d(1)=2.*(dfb*cxb*cxb*cxb-dfc*cxc*cxc*cxc)

! interior points
    n1=n-1
    do k=2,n1
      cxa=1./(x(k)-x(k-1))
      cxb=1./(x(k+1)-x(k))
      dfa=f(k)-f(k-1)
      dfb=f(k+1)-f(k)
      w(1,k)=cxa
      w(3,k)=cxb
      w(2,k)=2.*(cxa+cxb)
      d(k)=3.*(dfb*cxb*cxb+dfa*cxa*cxa)
    end do

! last point
    w(1,n)=cxa*cxa
    w(3,n)=-cxb*cxb
    w(2,n)=w(1,n)+w(3,n)
    d(n)=2.*(dfa*cxa*cxa*cxa-dfb*cxb*cxb*cxb)

! eliminate at first point
    c=-w(3,1)/w(3,2)
    w(1,1)=w(1,1)+c*w(1,2)
    w(2,1)=w(2,1)+c*w(2,2)
    d(1)=d(1)+c*d(2)
    w(3,1)=w(2,1)
    w(2,1)=w(1,1)

! eliminate at last point
    c=-w(1,n)/w(1,n-1)
    w(2,n)=w(2,n)+c*w(2,n-1)
    w(3,n)=w(3,n)+c*w(3,n-1)
    d(n)=d(n)+c*d(n-1)
    w(1,n)=w(2,n)
    w(2,n)=w(3,n)

! eliminate subdiagonal
    do k=2,n
      c=-w(1,k)/w(2,k-1)
      w(2,k)=w(2,k)+c*w(3,k-1)
      d(k)=d(k)+c*d(k-1)
    end do

! backsubstitute
    d(n)=d(n)/w(2,n)
    do kk=2,n
      k=(n+1)-kk
      d(k)=(d(k)-w(3,k)*d(k+1))/w(2,k)
    end do

END SUBROUTINE
!-----------------------------------------------------------------------
SUBROUTINE spline_interp(n,x,f,m,xx,ff,i1,in)
!
!   interp finds the values ff at the point xx of the cubic spline
!   defined by n,x,f,d.  The xx array must be montonically increasing.
!   The two  flags i1 and in determine the type of extrapolation used:
!
!     i1/in = 0, function set to zero outside
!             1, function set constant outside
!             2, linear extrapolation
!             3, quadratic extrapolation
!             4, cubic extrapolation
!-----------------------------------------------------------------------
  implicit none
  integer n,m,k,j,i1,in
  real(kind=8) p,q,d2,d36,dx,df
  real(kind=8) x(n),f(n),d(n),xx(m),ff(m),w(n,3)
!
  !print *,'interp',n,m
  k=2
  do j=1,m
    call spline (n,x,f,d)
!
!  Before x(1)
!
    if(xx(j).lt.x(1)) then
      ff(j)=0.
      if (i1.ge.1) ff(j)=f(1)
      if (i1.ge.2) ff(j)=ff(j)+(xx(j)-x(1))*d(1) 
      if (i1.ge.3) then
        dx=x(2)-x(1)
        d2=2.*(3.*(f(2)-f(1))/dx**2-(2.*d(1)+d(2))/dx)
        ff(j)=ff(j)+.5*(xx(j)-x(1))**2*d2
      end if
      if (i1.ge.4) then
        d36=(d(1)+d(2)-2.*(f(2)-f(1))/dx)/dx**2
        ff(j)=ff(j)+(xx(j)-x(1))*(xx(j)-x(1))**2*d36
      end if

! After x(n)

    else if(xx(j).ge.x(n)) then
      ff(j)=0.
      if (in.ge.1) ff(j)=f(n)
      if (in.ge.2) ff(j)=ff(j)+(xx(j)-x(n))*d(n)
      if (in.ge.3) then
        dx=x(n)-x(n-1)
        d2=2.*(-3.*(f(n)-f(n-1))/dx**2+(2.*d(n)+d(n-1))/dx)
        ff(j)=ff(j)+.5*(xx(j)-x(n))**2*d2
      end if
      if (in.ge.4) then
        d36=(d(n)+d(n-1)-2.*(f(n)-f(n-1))/dx)/dx**2
        ff(j)=ff(j)+(xx(j)-x(n))*(xx(j)-x(n))**2*d36
      end if
    else

!  Inside, calculate spline value

      do while(x(k).lt.xx(j))
        k=k+1
      end do
      dx=x(k)-x(k-1)
      df=f(k)-f(k-1)
      p=(xx(j)-x(k-1))/dx
      q=1.-p
      ff(j)=q*f(k-1)+p*f(k)+p*q*(q*(d(k-1)*dx-df)-p*(d(k)*dx-df))
    end if
    !print '(i4,6g12.5)',k,xx(j),f(k-1),d(k-1),f(k),d(k),ff(j)
  end do
END SUBROUTINE
!------------------------------------------------------------------------------
SUBROUTINE mesh_interp (n1,x1,n2,x2,lb1,ub1,lb2,ub2)
  implicit none
  integer n1,n2,lb1,ub1,lb2,ub2
  real(kind=8) x1(n1),x2(n2),p1(n1),p2(n2)
  real(kind=8) p,q,af,bf,ad,bd,ac,bc,dx1dx2
  integer k,km1,kp0,kp1,kp2,i1,i2
!..............................................................................
  dx1dx2 = real(ub1-lb1)/real(ub2-lb2)
  do i1=1,n1
    p1(i1) = i1
  end do
  do i2=1,n2
    p2(i2) = lb1+(i2-lb2)*dx1dx2
  end do
  call spline_interp(n1,p1,x1,n2,p2,x2,2,2)
END SUBROUTINE
!------------------------------------------------------------------------------
END MODULE

!*******************************************************************************
PROGRAM interp_prg
  USE interp_mod
  implicit none
  integer mx1,my1,mz1,lb1,ub1,mx2,my2,mz2,lb2,ub2,isnap,nvar,io_word,rec,iz,n1
  integer i,ny1,ny2
  real, allocatable, dimension(:,:,:):: f1
  real, allocatable, dimension(:,:,:):: f2
  real(kind=8), allocatable, dimension(:):: s1,s2
  integer, dimension(9):: kx,ky,kz
  character(len=80):: file1,file2,mesh1,mesh2,ymesh
  namelist /io/file1,file2,mx1,my1,mz1,lb1,ub1,mx2,my2,mz2,lb2,ub2,isnap,nvar, &
               kx,ky,kz,mesh1,mesh2,ymesh
!...............................................................................
  nvar=9                                                                        ! number of variables
  isnap=0                                                                       ! snapshot (first=0)
  lb1=6                                                                         ! lower y bdry index
  ub1=lb1                                                                       ! upper y bdry index
  lb2=6
  ub2=lb2
  kx = (/0,1,0,0,0,0,1,0,0/)
  ky = (/0,0,1,0,0,0,0,1,0/)
  kz = (/0,0,0,1,0,0,0,0,1/)
  ymesh=''
  mesh1=''
  mesh2=''

  read (*,io)
  ub1=my1-ub1+1                                                                 ! ub given relative to my1
  ub2=my2-ub2+1                                                                 ! ub given relative to my1
  write (*,io)

  print'(1x,a,f8.3,a)','Memory needed:',real(mx2)*real(my2)*real(mz2)*9./1024.**3,' GB'

  allocate(f1(mx1,my1,mz1))
  allocate(f2(mx2,my2,mz2))
  allocate(xm1(mx1),xmdn1(mx1),dxm1(mx1),dxmdn1(mx1),dxidxdn1(mx1),dxidxup1(mx1))
  allocate(ym1(my1),ymdn1(my1),dym1(my1),dymdn1(my1),dyidydn1(my1),dyidyup1(my1))
  allocate(zm1(mz1),zmdn1(mz1),dzm1(mz1),dzmdn1(mz1),dzidzdn1(mz1),dzidzup1(mz1))
  allocate(xm2(mx2),xmdn2(mx2),dxm2(mx2),dxmdn2(mx2),dxidxdn2(mx2),dxidxup2(mx2))
  allocate(ym2(my2),ymdn2(my2),dym2(my2),dymdn2(my2),dyidydn2(my2),dyidyup2(my2))
  allocate(zm2(mz2),zmdn2(mz2),dzm2(mz2),dzmdn2(mz2),dzidzdn2(mz2),dzidzup2(mz2))

  open (21,file=file1,form='unformatted',access='direct',status='old',recl=io_word()*mx1*my1)
  open (22,file=file2,form='unformatted',access='direct',status='unknown',recl=io_word()*mx2*my2)
  open(11,file=mesh1,form='unformatted',status='old')
  read(11),mx1,my1,mz1
  read(11) dxm1, dxmdn1, xm1, xmdn1, dxidxup1, dxidxdn1
  read(11) dym1, dymdn1, ym1, ymdn1, dyidyup1, dyidydn1
  read(11) dzm1, dzmdn1, zm1, zmdn1, dzidzup1, dzidzdn1
  close (11)
  open(12,file=mesh2,form='unformatted',status='unknown')
  read(12),mx2,my2,mz2
  read(12) dxm2, dxmdn2, xm2, xmdn2, dxidxup2, dxidxdn2
  read(12) dym2, dymdn2, ym2, ymdn2, dyidyup2, dyidydn2
  read(12) dzm2, dzmdn2, zm2, zmdn2, dzidzup2, dzidzdn2
  close (12)
!
    print*,'interpolating ...'
    do rec=1,nvar
      print*,rec
      do iz=1,mz1
        read (21,rec=iz+mz1*((rec-1)+nvar*isnap)) f1(:,:,iz)
      end do
      call interp2 (f1,mx1,my1,mz1,lb1,ub1,f2,mx2,my2,mz2,lb2,ub2,kx(rec),ky(rec),kz(rec),ym1,ym2)
      do iz=1,mz2
        write(22,rec=iz+mz2*(rec-1)) f2(:,:,iz)
      end do
    end do
!
!   do rec=1,nvar
!     print*,rec
!     do iz=1,mz1
!       read (21,rec=iz+mz1*((rec-1)+nvar*isnap)) f1(:,:,iz)
!     end do
!     call interp (f1,mx1,my1,mz1,lb1,ub1,f2,mx2,my2,mz2,lb2,ub2,kx(rec),ky(rec),kz(rec))
!     do iz=1,mz2
!       write(22,rec=iz+mz2*(rec-1)) f2(:,:,iz)
!     end do
!   end do
  close (21)
  close (22)
  deallocate (f1,f2)
END PROGRAM
