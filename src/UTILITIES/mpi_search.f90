  implicit none
  namelist /grid/nodes, mx, my, mz, mpi_nx, mpi_ny, mpi_nz, &
    sx, sy, sz, omp_nz, maxram
  integer maxram, omp_nz
  real sx, sy, sz, gb, tm
  integer mx, my, mz, nx, ny, nz, mpi_nx, mpi_ny, mpi_nz, nodes, np
  integer mpi_kx, mpi_ky, mpi_kz, nxyz, nmpi, mpi_size, mmpi
  integer nxmin, nxmax, nymin, nymax, nzmin, nzmax, power
  logical ok, mpi_search
  character (len=1) c

  mpi_nx = 0
  mpi_ny = 0
  mpi_nz = 0
  nodes = 100
  read(*,grid)
  write(*,grid)
  mpi_size = 8*nodes

  print'(a,a)','      domain size         MPI size      cores nodes', &
    '          GB     sec'

  do mmpi=mpi_size,3,-1
    !print*,'mmpi =',mmpi
    nmpi = mmpi
    mpi_kx = mpi_nx
    mpi_ky = mpi_ny
    mpi_kz = mpi_nz
    power = 3
    nxyz = mx*my*mz
    if (mpi_kx > 0) then
      nmpi = nmpi/mpi_kx
      nxyz = nxyz/mx
      power = power-1
    end if
    if (mpi_ky > 0) then
      nmpi = nmpi/mpi_ky
      nxyz = nxyz/my
      power = power-1
    end if
    if (mpi_kz > 0) then
      nmpi = nmpi/mpi_kz
      nxyz = nxyz/mz
      power = power-1
    end if

    !print*,'x',nxyz,nmpi,power
    if (mpi_kx > 0) then
      nx = mx/mpi_kx
      ok = .true.
    else if (nmpi==0) then
      ok = .false.
    else
      nx = (nxyz/nmpi)**(1./power)
      nxmax = max(nx*2,nxyz/(my*mz))
      nxmin = min(nx/2,nxyz/(my*mz))
      ok = mpi_search (nx, nxmax, +1, mx, nmpi, mpi_kx)
      if (.not. ok) ok = mpi_search (nx, nxmin, -1, mx, nmpi, mpi_kx)
      nxyz = nxyz/mx
      power = power-1
    end if
    if (ok) then

      !print*,'y',nxyz,nmpi,power
      if (mpi_ky > 0) then
        ny = my/mpi_ky
	ok = .true.
      else if (nmpi==0) then
        ok = .false.
      else
        ny = (nxyz/nmpi)**(1./power)
        nymax = max(ny*2,nxyz/mx)
        nymin = min(ny/2,nxyz/mx)
        ok = mpi_search (ny, nymax, +1, my, nmpi, mpi_ky)
        if (.not. ok) ok = mpi_search (ny, nymin, -1, my, nmpi, mpi_ky)
        nxyz = nxyz/my
	power = power-1
      end if
      if (ok) then

        !print*,'z',nxyz,nmpi,power
	if (mpi_kz > 0) then
	  ok = (mpi_kz==nmpi)
	else if (nmpi==0) then
	  ok = .false.
	else
	  !print*,mz,nmpi,nz
          nz = mz/nmpi
	  if (nz*nmpi == mz) then
	    mpi_kz = mz/nz
	    ok = .true.
	  else
            ok = .false.
	  end if
	end if
	!print*,ok
	if (ok) then
	  np = mpi_kx*mpi_ky*mpi_kz
	  c = ''
	  nodes = (np+7)/8
	  if (np .ne. nodes*8) c = 'p'
	  gb = nx*ny*nz*200e-9
	  tm = nx*ny*nz*1e-5
	  print'(3i6,3x,3i5,3x,2i6,1x,a1,3x,f7.1,g12.3)', &
	    nx,ny,nz,mpi_kx,mpi_ky,mpi_kz,np,nodes,c,gb,tm
	end if
      end if
    end if
  end do
END

LOGICAL FUNCTION mpi_search (n1, n2, n3, ngrid, nmpi, mpi)
  integer n1, n2, n3, ngrid, nmpi, mpi
  !print*,''
  n1=max(n1,1)
  n2=max(n2,1)
  !print*,n1,n2,n3,ngrid,nmpi
  do n=n1,n2,n3
    !print*,n
    if (mod(ngrid,n)==0) then
      mpi=ngrid/n
      if (mod(nmpi,mpi)==0) then
        mpi_search=.true.
	n1=n
	nmpi = nmpi/mpi
	!print*,n,mpi,nmpi
	return
      endif
    endif
  end do
  mpi_search=.false.
END
