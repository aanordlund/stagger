! $Id: fft3d_loop_parallel.f90,v 1.4 2013/03/24 13:28:15 aake Exp $
!**********************************************************************
SUBROUTINE fft3d_k2 (k2,dx,dy,dz,mx,my,mz)
  implicit none
  integer mx, my, mz
  real, dimension(mx,my,mz):: k2
  real dx, dy, dz, kx, ky, kz
  real, parameter:: pi=3.14519265
  integer i, j, k
!----------------------------------------------------------------------
  !$omp parallel do private(i,j,k,kx,ky,kz)
  do k=1,mz
    kz=2.*pi/(dz*mz)*(k/2)
    do j=1,my
      ky=2.*pi/(dy*my)*(j/2)
      do i=1,mx
        kx=2.*pi/(dx*mx)*(i/2)
        k2(i,j,k)=kx**2+ky**2+kz**2
      end do
    end do
  end do
END SUBROUTINE

!**********************************************************************
SUBROUTINE fft3df (r,rr,mx,my,mz)
  implicit none
  integer mx,my,mz
  real, dimension(mx,my,mz):: r, rr
  integer i,j,k
  integer, parameter:: lw=4096, lwx=2*lw+15, lwy=2*lw+15, lwz=2*lw+15
  real c, wx(lwx), wy(lwy), wz(lwz), fx(mx), fy(my), fz(mz,mx)
!----------------------------------------------------------------------
  if (mx.gt.lw .or. my.gt.lw .or. mz.gt.lw) then
    print *,'fft3db: dim too small',mx,my,mz,lw
    stop
  end if
  call srffti (mx,wx)
  call srffti (my,wy)
  call srffti (mz,wz)

  !$omp parallel do private(i,j,k,fx,fy,c), firstprivate(wx,wy)
  do k=1,mz
   c=1./mx
   do j=1,my
    fx(:)=r(:,j,k)
    call srfftf (mx,fx,wx)
    rr(:,j,k)=c*fx(:)
   end do
   c=1./my
   do i=1,mx
    fy=rr(i,:,k)
    call srfftf (my,fy,wy)
    rr(i,:,k)=c*fy
   end do
  end do

  !$omp parallel do private(i,j,k,fz,c), firstprivate(wz)
  do j=1,my
    do k=1,mz
      fz(k,:)=rr(:,j,k)
    end do
    do i=1,mx
      call srfftf (mz,fz(:,i),wz)
    end do
    c=1./mz
    do k=1,mz
      rr(:,j,k)=c*fz(k,:)
    end do
  end do

END SUBROUTINE

!**********************************************************************
SUBROUTINE fft3db (rr,r,mx,my,mz)
  implicit none
  integer mx,my,mz
  real, dimension(mx,my,mz):: r, rr
  integer i,j,k
  integer, parameter:: lw=4096, lwx=2*lw+15, lwy=2*lw+15, lwz=2*lw+15
  real wx(lwx), wy(lwy), wz(lwz), fx(mx), fy(my), fz(mz,mx)
!----------------------------------------------------------------------

  if (mx.gt.lw .or. my.gt.lw .or. mz.gt.lw) then
    print *,'fft3db: dim too small',mx,my,mz,lw
    stop
  end if
  call srffti (mx,wx)
  call srffti (my,wy)
  call srffti (mz,wz)

  !$omp parallel do private(i,j,k,fx,fy), firstprivate(wx,wy)
  do k=1,mz
   do j=1,my
    fx=rr(:,j,k)
    call srfftb (mx,fx,wx)
    r(:,j,k)=fx
   end do
   do i=1,mx
    fy=r(i,:,k)
    call srfftb (my,fy,wy)
    r(i,:,k)=fy
   end do
  end do

  !$omp parallel do private(i,j,k,fz), firstprivate(wz)
  do j=1,my
    do k=1,mz
      fz(k,:)=r(:,j,k)
    end do
    do i=1,mx
      call srfftb (mz,fz(:,i),wz)
    end do
    do k=1,mz
      r(:,j,k)=fz(k,:)
    end do
  end do

END SUBROUTINE
