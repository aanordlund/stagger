! $Id: fft3d_3_mpi.f90,v 1.4 2009/09/30 05:31:29 aake Exp $
!----------------------------------------------------------------------
!
!  Three-dimensional Fourier transform routines, based on FFTPACK.
!  FFTPACK is transportable, and allows arbitrary dimensions (speed
!  will vary, of course).
!
!  CALL fft3df (f, ft, mx, my, mz)              ! forward transform
!  CALL fft3db (ft, f, mx, my, mz)              ! backward transform
!  CALL fft3d_k2 (k2, dx, dy, dz, mx, my, mz)   ! compute k2
!
!**********************************************************************
SUBROUTINE fftpack_test
!
!  This test demonstrates that the coeffs returned are:
!
!   [a_0, +a_1/2., +b_1/2., ..., b_{ny-1}/2.,  a_ny]
!
!  Thus, the derivive is formed by setting the tranform to
!
!   [ 0., -b_1/2, +a_1/2., ...., a_{ny-1}/2.,  0.  ]
!
  real, allocatable, dimension(:):: f,ft,w
  integer m,lw,i,k
  real a,pi
!----------------------------------------------------------------------
  m=8
  lw=2*m+15
  allocate (f(m),ft(m),w(lw))

  a = 2.*pi/m
  f = 0.
  do k=0,m/2
    do i=1,m
      f(i) = f(i) + 2.*(k+1)*cos((i-1)*k*a) + 2.*(k+m)*sin((i-1)*k*a)
    end do
  end do
  call srffti (m,w)
  print *,'f:'
  print *,f
  ft = f
  call srfftf (m,ft,w)
  print *,'ft:'
  print '(8f10.2)',ft/m

  m = 7
  a = 2.*pi/m
  f = 0.
  do k=0,m/2
    do i=1,m
      f(i) = f(i) + 2.*(k+1)*cos((i-1)*k*a) + 2.*(k+m)*sin((i-1)*k*a)
    end do
  end do
  call srffti (m,w)
  print *,'f:'
  print '(8f10.2)',(f(i),i=1,m)
  ft = f
  call srfftf (m,ft,w)
  print *,'ft:'
  print '(8f10.2)',(ft(i)/m,i=1,m)

  deallocate (f,ft,w)
END

!**********************************************************************
SUBROUTINE fft3d_k2 (k2,dx1,dy1,dz1,mx1,my1,mz1)
  USE params
  implicit none
  real, dimension(mx,my,mz):: k2
  real dx1, dy1, dz1, kx, ky, kz
  integer i, ii, j, jj, k, kk, mx1, my1, mz1
!----------------------------------------------------------------------
  do k=1,mz
    kk=k+mpi_z*mz
    kz=2.*pi/sz*(kk/2)
    do j=1,my
      jj=j+mpi_y*my
      ky=2.*pi/sy*(jj/2)
      do i=1,mx
	ii=i+mpi_x*mx
        kx=2.*pi/sx*(ii/2)
        k2(i,j,k)=kx**2+ky**2+kz**2
      end do
    end do
  end do
END SUBROUTINE

!**********************************************************************
SUBROUTINE fft3df (r,rr,mx1,my1,mz1)
  USE params
  USE reshape_m
  implicit none
  integer i,j,k,mx1,my1,mz1
  integer, parameter:: lw=4096, lwx=2*lw+15
  real, dimension(mx,my,mz):: r, rr
  real c
  real, dimension(:), allocatable:: fx, fy, fz, wx, wy, wz
  real, dimension(:,:,:), allocatable:: r1
!----------------------------------------------------------------------
  call init_reshape

  allocate (r1(dim_x(1),dim_x(2),dim_x(3)))
  allocate (fx(dim_g(1)),fy(dim_g(2)),fz(dim_g(3)))
  allocate (wx(2*dim_g(1)+15),wy(2*dim_g(2)+15),wz(2*dim_g(3)+15))
  call srffti (dim_g(1),wx)
  call srffti (dim_g(2),wy)
  call srffti (dim_g(3),wz)

  call reshape_x (r, r1)
  do k=1,dim_x(3)
   do j=1,dim_x(2)
    do i=1,dim_x(1)
      fx(i)=r1(i,j,k)
    end do
    call srfftf (dim_x(1),fx,wx)
    c=1./dim_x(1)
    do i=1,dim_x(1)
     r1(i,j,k)=c*fx(i)
    end do
   end do
  end do
  call deshape_x (r1, rr)
  deallocate (r1)

  allocate (r1(dim_y(1),dim_y(2),dim_y(3)))
  call reshape_y (rr, r1)
  do k=1,dim_y(3)
   do i=1,dim_y(1)
    do j=1,dim_y(2)
      fy(j)=r1(i,j,k)
    end do
    call srfftf (dim_y(2),fy,wy)
    c=1./dim_y(2)
    do j=1,dim_y(2)
     r1(i,j,k)=c*fy(j)
    end do
   end do
  end do
  call deshape_y (r1, rr)
  deallocate (r1)

  allocate (r1(dim_z(1),dim_z(2),dim_z(3)))
  call reshape_z (rr, r1)
  do j=1,dim_z(2)
   do i=1,dim_z(1)
    do k=1,dim_z(3)
     fz(k)=r1(i,j,k)
    end do
    call srfftf (dim_z(3),fz,wz)
    c=1./dim_z(3)
    do k=1,dim_z(3)
     r1(i,j,k)=c*fz(k)
    end do
   end do
  end do
  call deshape_z (r1, rr)
  deallocate (r1)
  deallocate (fx, fy, fz)
  deallocate (wx, wy, wz)
END SUBROUTINE

!**********************************************************************
SUBROUTINE fft3db (r,rr,mx1,my1,mz1)
  USE params
  USE reshape_m
  implicit none
  integer i,j,k,mx1,my1,mz1
  real, dimension(mx,my,mz):: r, rr
  real, dimension(:), allocatable:: fx, fy, fz, wx, wy, wz
  real, dimension(:,:,:), allocatable:: r1
!----------------------------------------------------------------------
  call init_reshape

  allocate (r1(dim_x(1),dim_x(2),dim_x(3)))
  allocate (fx(dim_g(1)),fy(dim_g(2)),fz(dim_g(3)))
  allocate (wx(2*dim_g(1)+15),wy(2*dim_g(2)+15),wz(2*dim_g(3)+15))
  call srffti (dim_g(1),wx)
  call srffti (dim_g(2),wy)
  call srffti (dim_g(3),wz)

  call reshape_x (r, r1)
  do k=1,dim_x(3)
   do j=1,dim_x(2)
    do i=1,dim_x(1)
      fx(i)=r1(i,j,k)
    end do
    call srfftb (dim_x(1),fx,wx)
    do i=1,dim_x(1)
     r1(i,j,k)=fx(i)
    end do
   end do
  end do
  call deshape_x (r1, rr)
  deallocate (r1)

  allocate (r1(dim_y(1),dim_y(2),dim_y(3)))
  call reshape_y (rr, r1)
  do k=1,dim_y(3)
   do i=1,dim_y(1)
    do j=1,dim_y(2)
      fy(j)=r1(i,j,k)
    end do
    call srfftb (dim_y(2),fy,wy)
    do j=1,dim_y(2)
     r1(i,j,k)=fy(j)
    end do
   end do
  end do
  call deshape_y (r1, rr)
  deallocate (r1)

  allocate (r1(dim_z(1),dim_z(2),dim_z(3)))
  call reshape_z (rr, r1)
  do j=1,dim_z(2)
   do i=1,dim_z(1)
    do k=1,dim_z(3)
     fz(k)=r1(i,j,k)
    end do
    call srfftb (dim_z(3),fz,wz)
    do k=1,dim_z(3)
     r1(i,j,k)=fz(k)
    end do
   end do
  end do
  call deshape_z (r1, rr)
  deallocate (r1)
  deallocate (fx, fy, fz)
  deallocate (wx, wy, wz)
END SUBROUTINE

!**********************************************************************
SUBROUTINE test_fft
  USE params
  USE reshape_m
  implicit none
  integer ix, iy, iz
  real sum1, sum2, average, ran1s
  real, allocatable, dimension(:,:,:):: fft1, fft2, fft3
  integer jseed
  character(len=mid), save:: id='$Id: fft3d_3_mpi.f90,v 1.4 2009/09/30 05:31:29 aake Exp $'
!----------------------------------------------------------------------
  call print_id(id)

  allocate (fft1(mx,my,mz), fft2(mx,my,mz), fft3(mx,my,mz))

  do iz=1,mz
  do iy=1,my
  do ix=1,mx
    fft1(ix,iy,iz) = cos(2.*pi*(xm(ix)-xmin)/sx) + 2.*cos(2.*pi*(ym(iy)-ymin)/sy) + 3.*cos(2.*pi*(zm(iz)-zmin)/sz)
  end do
  end do
  end do
  call fft3df (fft1, fft2, mx, my, mz)
  if (verbose > 2 .and. mpi_x==0 .and. mpi_y==0 .and. mpi_z==0) then
    print*,'in(Fx):',mpi_rank,fft1(1:3,1,1)
    print'(a,i3,1p,5g12.4)','in(Tx):',mpi_rank,fft2(1:5,1,1)
    print'(a,i3,1p,5g12.4)','in(Ty):',mpi_rank,fft2(1,1:5,1)
    print'(a,i3,1p,5g12.4)','in(Tz):',mpi_rank,fft2(1,1,1:5)
    print*,'expected:',fft2(2,1,1),fft2(1,2,1),fft2(1,1,2)
  end if
  call fft3db (fft2, fft3, mx, my, mz)
  if (verbose > 2 .and. mpi_x==0 .and. mpi_y==0 .and. mpi_z==0) then
    print*,'expected:',fft2(2,1,1),fft2(1,2,1),fft2(1,1,2)
    print*,'out:',mpi_rank,fft3(1:3,1,1)
  end if

  sum1 = average(fft1**2)
  sum2 = average((fft1-fft3)**2)
  if (master) print*,'test_fft3d_3(A):',sum2/sum1
  
  jseed = 88
  do iz=1,mz
  do iy=1,my
  do ix=1,mx
    fft1(ix,iy,iz) = ran1s(jseed)
  end do
  end do
  end do
  call fft3df (fft1, fft2, mx, my, mz)
  call fft3d_k2 (fft1,0.2,0.3,0.4,mx,my,mz)
  fft1 = fft1*fft2
  call fft3db (fft2, fft3, mx, my, mz)
  sum1 = average(fft3**2)
  if (master) print*,'test_fft3d_3(B):',sum1

  deallocate (fft1, fft2, fft3)
END
