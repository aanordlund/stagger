!$Id: divb_clean.f90,v 1.27 2016/11/09 22:04:57 aake Exp $

!***********************************************************************
SUBROUTINE divb_clean
  USE variables
  USE params
  implicit none
  real, allocatable, dimension(:,:,:):: div,dphit,scr
  real, allocatable, dimension(:,:):: w,k2,tmpf,tmpt
  real, allocatable, dimension(:):: b
  integer iter,ix,iy,iz
  real divmin,divmax1,divmax2,wt,wallclock,c,over,dy0,dy1,dy2,fmaxval
  character(len=mid), save:: id="$Id: divb_clean.f90,v 1.27 2016/11/09 22:04:57 aake Exp $"

  allocate (div(mx,my,mz), dphit(mx,my,mz), scr(mx,my,mz), k2(mx,mz))
  allocate (tmpf(mx,mz),tmpt(mx,mz))

  !$omp parallel private (iz)
  do iz=izs,ize
    dphit(:,:,iz) = 0.
    div(:,:,iz) = 0.
  end do
  !$omp end parallel

  if (master) print*,id
                                                     call timer('utils','start')
  wt=wallclock()
  do iter=1,divb_iter
    !$omp parallel private (ix,iy,iz)
    if (do_2nddiv) then
      call ddxup1_set (bx, scr)
    else
      call ddxup_set (bx, scr)
    end if
    call dumpn(scr,'dBxdx','div.dmp',1)
    do iz=izs,ize
      div(:,lb:ub,iz) = scr(:,lb:ub,iz)
    end do
    if (do_2nddiv) then
      call ddyup1_set (by, scr)
    else
      call ddyup_set (by, scr)
    end if
    call regularize (scr)
    call dumpn(scr,'dBydy','div.dmp',1)
    do iz=izs,ize
      div(:,lb:ub,iz) = div(:,lb:ub,iz) + scr(:,lb:ub,iz)
    end do
    !$omp barrier
    if (do_2nddiv) then
      call ddzup1_set (bz, scr)
    else
      call ddzup_set (bz, scr)
    end if
    call dumpn(scr,'dBzdz','div.dmp',1)
    !$omp barrier
    do iz=izs,ize
      div(:,lb:ub,iz) = div(:,lb:ub,iz) + scr(:,lb:ub,iz)
    end do
    !$omp end parallel
    call dumpn(div,'div(B)','div.dmp',1)

    scr = abs(div)
    divmax1 = fmaxval('divb',scr)*max(sx,sz)

    !$omp parallel
    scr(:,1:lb+3,izs:ize) = 0.
    scr(:,ub-3:,izs:ize) = 0.
    !$omp end parallel
    divmax2 = fmaxval('divb',scr)*max(sx,sz)

    wt=wallclock()-wt
    if (master) print '(i3,1p,(2e15.6),0p,f10.2,a)',iter,divmax1,divmax2,wt,' sec'
    if (divmax1 < divb_eps) exit
    wt=wallclock()
                                                call timer('utils','divb_clean')
    do iy=lb,ub
      tmpf = div(:,iy,:)
      call fft2df (tmpf,tmpt,mx,mz)
                                                    call timer('utils','fft2df')
      dphit(:,iy,:) = tmpt
    end do
    call dumpn(dphit,'dphit','div.dmp',1)

    call fft2d_k2(k2,sx,sz,mx,mz)
    call dumpn(k2,'k2','div.dmp',1)

    !$omp parallel private(ix,iy,iz,dy0,dy1,dy2,w,b)
    allocate (w(my,3),b(my))
    w = 0.
    b = 0.
    do iz=izs,ize                                                       ! loop over all ..
    do ix=1,mx                                                          ! .. Fourier components
      if (mpi_x==0 .and. mpi_z==0 .and. ix==1 .and. iz==1) then
        dphit(ix,:,iz) = 0.                                             ! no DC-component
        cycle
      end if
      
      do iy=lb,lb                                                       ! lower boundary
        dy1 = ym(iy)-ym(iy-1)
        dy2 = ym(iy+1)-ym(iy)
        dy0 = 0.5*(dy1+dy2)
        w(iy,1) = 0.
        w(iy,3) = 1./(dy1*dy0) + 1./(dy2*dy0)                           ! symmetric dphit
        w(iy,2) = -w(iy,3)-k2(ix,iz)
        b(iy) = dphit(ix,iy,iz)                                         ! right hand side; FT of div(B)
      end do
      
      do iy=lb+1,ub-1                                                   ! tridiagonal matrix elements
        dy1 = ym(iy)-ym(iy-1)
        dy2 = ym(iy+1)-ym(iy)
        dy0 = 0.5*(dy1+dy2)
        w(iy,1) = 1./(dy1*dy0)
        w(iy,3) = 1./(dy2*dy0)
        w(iy,2) = -w(iy,1)-w(iy,3)-k2(ix,iz)
        b(iy) = dphit(ix,iy,iz)                                         ! right hand side; FT of div(B)
      end do

      do iy=ub,ub                                                       ! upper boundary
        dy1 = ym(iy)-ym(iy-1)
        dy2 = ym(iy+1)-ym(iy)
        dy0 = 0.5*(dy1+dy2)
        w(iy,1) = 1./(dy1*dy0) + 1./(dy2*dy0)                           ! symmetric dphit
        w(iy,3) = 0.
        w(iy,2) = -w(iy,1)-k2(ix,iz)
        b(iy) = dphit(ix,iy,iz)                                         ! right hand side; FT of div(B)
      end do
      
      do iy=lb+1,ub                                                     ! forward loop
        w(iy,1) = -w(iy,1)/w(iy-1,2)
        w(iy,2) = w(iy,2) + w(iy,1)*w(iy-1,3)                           ! eliminate sub-diagonal
        b(iy)   = b(iy)   + w(iy,1)*b(iy-1)                             ! accumulate RHS
      end do

      do iy=ub,lb,-1                                                    ! reverse loop
        b(iy) = (b(iy)-w(iy,3)*b(iy+1))/w(iy,2)                         ! back substitute
        dphit(ix,iy,iz) = b(iy)                                         ! FT of phi such that Laplace(phi)=div(B)
      end do

      do iy=1,lb-1
        dphit(ix,iy,iz) = dphit(ix,lb+(lb-iy),iz)                       ! symmetric_center_lower
      end do
      do iy=ub+1,my
        dphit(ix,iy,iz) = dphit(ix,ub-(iy-ub),iz)                       ! symmetric_center_upper
      end do

    end do
    end do
    deallocate (w,b)
    !$omp end parallel
    do iy=1,my
      tmpf = dphit(:,iy,:)
                                                call timer('utils','divb_clean')
      call fft2db (tmpf,tmpt,mx,mz)
                                                    call timer('utils','fft2db')
      div(:,iy,:) = tmpt
    end do
    call dumpn(dphit,'dphit','div.dmp',1)
    call dumpn(div,'dphi','div.dmp',1)

    !$omp parallel private (ix,iy,iz)
    if (do_2nddiv) then
      call ddxdn1_set (div, scr)
    else
      call ddxdn_set (div, scr)
    end if
    do iz=izs,ize
      bx(:,lb:ub,iz) = bx(:,lb:ub,iz) - scr(:,lb:ub,iz)
    end do
    if (do_2nddiv) then
      call ddydn1_set (div, scr)
    else
      call ddydn_set (div, scr)
    end if
    if (ub < my) then
      do iz=izs,ize
        by(:,lb:ub+1,iz) = by(:,lb:ub+1,iz) - scr(:,lb:ub+1,iz)
      end do
    else
      do iz=izs,ize
        by(:,lb:ub,iz) = by(:,lb:ub,iz) - scr(:,lb:ub,iz)
      end do
    end if
    !$omp barrier
    if (do_2nddiv) then
      call ddzdn1_set (div, scr)
    else
      call ddzdn_set (div, scr)
    end if
    !$omp barrier
    do iz=izs,ize
      bz(:,lb:ub,iz) = bz(:,lb:ub,iz) - scr(:,lb:ub,iz)
    end do
    !$omp end parallel
                                                call timer('utils','divb_clean')
  end do
 
  if (master) print *,wallclock(),' sec'

  deallocate (div,dphit,k2)
  deallocate (tmpf,tmpt)
END

