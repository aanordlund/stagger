!  $Id: helmhc.f90,v 1.1 2003/05/03 19:01:01 aake Exp $
!***********************************************************************
  SUBROUTINE helmhc (tx, ty, tz, nx, ny, nz, dx, dy, dz)

!  Helmholtz projection, to compute the divergence free part
!  of the vector field {fx,fy,fz}, with transform {tx,ty,tz}

  implicit none
  real, parameter:: pi=3.14159265
  complex, dimension(nx,ny,nz):: tx, ty, tz
  integer:: i, j, k, nx, ny, nz
  real:: dx, dy, dz, c, kx, ky, kz, cpu(3), elapsed
  complex:: corr
!-----------------------------------------------------------------------

  do k=1,nz
    !print *,(mod(k-2+nz/2,nz)-nz/2+1)
    kz = 2.*pi/(nz*dz)*(mod(k-2+nz/2,nz)-nz/2+1)
    do j=1,ny
      ky = 2.*pi/(ny*dy)*(mod(j-2+ny/2,ny)-ny/2+1)
      do i=1,nx
        kx = 2.*pi/(nx*dx)*(mod(i-2+nx/2,nx)-nx/2+1)
        corr = tx(i,j,k)*kx + ty(i,j,k)*ky + tz(i,j,k)*kz
        corr = corr/(kx**2+ky**2+kz**2+1e-10)    
        tx(i,j,k) = tx(i,j,k) - corr*kx
        ty(i,j,k) = ty(i,j,k) - corr*ky
        tz(i,j,k) = tz(i,j,k) - corr*kz
      end do
    end do
  end do

END
