! $Id: multigrid.f90,v 1.4 2007/07/06 17:17:38 aake Exp $
!**********************************************************************
MODULE multigrid_m
  real max_error, filter(3)
  integer min_iter, max_iter

CONTAINS

!***********************************************************************
SUBROUTINE poisson_grid_reduced (dx, dy, dz, grav, nx, ny, nz, rho, phi, lphi, kx, ky, kz)
  implicit none
  real grav, dx, dy, dz
  integer nx, ny, nz, kx, ky, kz
  real, dimension(nx,ny,nz):: rho, phi, lphi
  real, allocatable, dimension(:,:,:):: res, dphi
!-----------------------------------------------------------------------
  allocate (res(nx,ny,nz), dphi(nx,ny,nz))
  call laplace_grid (dx, dy, dz, nx, ny, nz, phi, lphi)
  res = lphi - grav*rho
  call filter_grid_reduced (dx, dy, dz, nx, ny, nz, res, dphi, kx, ky, kz)
  phi = phi + dphi
  deallocate (res, dphi)
END SUBROUTINE

!***********************************************************************
SUBROUTINE laplace_grid (dx, dy, dz, nx, ny, nz, phi, lphi)
  implicit none
  integer nx, ny, nz
  real dx, dy, dz
  real, dimension(nx,ny,nz):: phi, lphi
  real, allocatable, dimension(:,:,:):: phi1
  integer ix, iy, iz
  real ax, ay, az
!-----------------------------------------------------------------------
  ax = 1./dx**2; ay = 1./dy**2; az = 1./dz**2
  allocate (phi1(0:nx+1,0:ny+1,0:nz+1))
  call extend_grid (nx, ny, nz, phi, phi1)
  do iz=1,nz; do iy=1,ny; do ix=1,nx
    lphi(ix,iy,iz) = ax*(phi1(ix+1,iy,iz)+phi1(ix-1,iy,iz)-phi1(ix,iy,iz)-phi1(ix,iy,iz)) &
                   + ay*(phi1(ix,iy+1,iz)+phi1(ix,iy-1,iz)-phi1(ix,iy,iz)-phi1(ix,iy,iz)) &
                   + az*(phi(ix,iy,iz+1)+phi(ix,iy,iz-1)-phi(ix,iy,iz)-phi(ix,iy,iz))
  end do; end do; end do
  deallocate (phi1)
END SUBROUTINE

!***********************************************************************
SUBROUTINE extend_grid (nx, ny, nz, f, fe)
  implicit none
  integer nx, ny, nz
  real, dimension(nx,ny,nz):: f
  real, dimension(0:nx+1,0:ny+1,0:nz+1):: fe
  real, allocatable, dimension(:,:,:):: g,h
!-----------------------------------------------------------------------
  fe(1:nx,1:ny,1:nz) = f
  allocate (g(1,ny,nz),h(1,ny,nz))
  call mpi_send2_x (f, nx, ny, nz, g, 1, h, 1)
  fe(0   ,1:ny,1:nz) = g(1,1:ny,1:nz)
  fe(nx+1,1:ny,1:nz) = h(1,1:ny,1:nz)
  deallocate (g,h)
!
  allocate (g(nx,1,nz),h(nx,1,nz))
  call mpi_send2_y (f, nx, ny, nz, g, 1, h, 1)
  fe(1:nx,0   ,1:nz) = g(1:nx,1,1:nz)
  fe(1:nx,ny+1,1:nz) = h(1:nx,1,1:nz)
  deallocate (g,h)
!
  allocate (g(nx,ny,1),h(nx,ny,1))
  call mpi_send2_z (f, nx, ny, nz, g, 1, h, 1)
  fe(1:nx,1:ny,0   ) = g(1:nx,1:ny,1)
  fe(1:nx,1:ny,nz+1) = h(1:nx,1:ny,1)
  deallocate (g,h)
END SUBROUTINE

!***********************************************************************
SUBROUTINE filter_grid_reduced (dx, dy, dz, nx, ny, nz, res, dphi, kx, ky, kz)
  implicit none
  integer nx, ny, nz
  real dx, dy, dz
  real, dimension(0:nx+1,0:ny+1,0:nz+1):: res
  real, dimension(nx,ny,nz):: dphi
  real, allocatable, dimension(:,:,:):: res1, dphi1
  integer lx, ly, lz, kx, ky, kz
!-----------------------------------------------------------------------
  lx = nx/kx; ly = ny/ky; lz=nz/kz
  allocate (res1(lx,ly,lz), dphi1(lx,ly,lz))
  call restrict_grid (nx, ny, nz, res, lx, ly, lz, res1)
  call filter_grid (dx*kx, dy*ky, dz*kz, lx, ly, lz, res1, dphi1)
  call expand_grid (lx, ly, lz, dphi1, nx, ny, nz, dphi)
  deallocate (res1, dphi1)
END SUBROUTINE

!***********************************************************************
SUBROUTINE restrict_grid (nx, ny, nz, f, lx, ly, lz, g)
  implicit none
  integer nx, ny, nz, lx, ly, lz
  real, dimension(nx,ny,nz):: f
  real, dimension(lx,ly,lz):: g
  integer kx, ky, kz, ix, iy, iz, jx, jy, jz
!-----------------------------------------------------------------------
  kx = nx/lx
  ky = ny/ly
  kz = nz/lz
  do iz=1,lz
  do iy=1,ly
  do ix=1,lx
    g(ix,iy,iz) = 0.
    do jz=1,kz
    do jy=1,ky
    do jx=1,kx
      g(jx,jy,jz) = g(jx,jy,jz) + f((ix-1)*kx+jx,(iy-1)*ky+jy,(iz-1)*kz+jz)
    end do
    end do
    end do
    g(ix,iy,iz) = g(ix,iy,iz)/(kx*ky*kz)
  end do
  end do
  end do
END SUBROUTINE

!***********************************************************************
SUBROUTINE filter_grid (dx, dy, dz, nx, ny, nz, res, dphi)
  implicit none
  integer nx, ny, nz
  real dx, dy, dz
  real, dimension(nx,ny,nz):: res, dphi
  real, allocatable, dimension(:,:,:):: res1
  integer ix, iy, iz
  real a, b, norm
!-----------------------------------------------------------------------
  norm = 0.25/(1./dx**2+1./dy**2+1./dz**2)/(3.-12*filter(1))
  a = (3.-6.*filter(1))*norm; b = filter(1)*norm
  allocate (res1(0:nx+1,0:ny+1,0:nz+1))
  call extend_grid (nx, ny, nz, res, res1)
  do iz=1,nz; do iy=1,ny; do ix=2,nx
    dphi(ix,iy,iz) = a*res1(ix,iy,iz) &
                  + b*(res1(ix+1,iy,iz)+res1(ix-1,iy,iz)) &
                  + b*(res1(ix,iy+1,iz)+res1(ix,iy-1,iz)) &
                  + b*(res1(ix,iy,iz+1)+res1(ix,iy,iz-1))
  end do; end do; end do
  deallocate (res1)
END SUBROUTINE

!***********************************************************************
SUBROUTINE expand_grid (lx, ly, lz, g, nx, ny, nz, f)
  implicit none
  integer nx, ny, nz, lx, ly, lz
  real, dimension(0:nx,0:ny,0:nz):: f
  real, dimension(lx,ly,lz):: g
  integer kx, ky, kz, ix, iy, iz, jx, jy, jz
  real px, qx, py, qy, pz, qz
!-----------------------------------------------------------------------
  kx = nx/lx
  ky = ny/ly
  kz = nz/lz
  if (kx*ky*kz == 1) then
    f = g
    return
  end if
  do iz=1,nz
    pz     = (iz+kz*0.5)/kz; jz = pz; pz = pz-jz; qz = 1.-pz
    do iy=1,ny
      py   = (iy+ky*0.5)/ky; jy = py; py = py-jy; qy = 1.-py
      do ix=1,nx
        px = (ix+kx*0.5)/kx; jx = px; px = px-jx; qx = 1.-px
        f(ix,iy,iz) = &
          qx*(qy*(qz*g(jx  ,jy  ,jz)+pz*g(jx  ,jy  ,jz+1)) + &
              py*(qz*g(jx  ,jy+1,jz)+pz*g(jx  ,jy+1,jz+1))) + &
          px*(qy*(qz*g(jx+1,jy  ,jz)+pz*g(jx+1,jy  ,jz+1)) + &
              py*(qz*g(jx+1,jy+1,jz)+pz*g(jx+1,jy+1,jz+1)))
      end do
    end do
  end do
END SUBROUTINE

END MODULE

!***********************************************************************
SUBROUTINE init_multigrid (set_filter, set_min_iter, set_max_iter, set_max_error)
  USE multigrid_m
  implicit none
  real set_max_error, set_filter(3)
  integer set_min_iter, set_max_iter
!-----------------------------------------------------------------------
  filter = set_filter
  max_iter = set_max_iter
  min_iter = set_min_iter
  max_error = set_max_error
END SUBROUTINE

!***********************************************************************
SUBROUTINE poisson_multigrid (grav, nx, ny, nz, rho, phi, dx, dy, dz)
  USE multigrid_m
  implicit none
  real grav, dx, dy, dz, error
  integer nx, ny, nz, iter
  real, dimension(nx,ny,nz):: rho, phi
  real, allocatable, dimension(:,:,:):: res, lphi
!-----------------------------------------------------------------------
  allocate (res(nx,ny,nz), lphi(nx, ny, nz))
  do iter=1,min_iter
    call poisson_grid_reduced (dx, dy, dz, grav, nx, ny, nz, rho, phi, lphi, 10, 10, 10)
    call poisson_grid_reduced (dx, dy, dz, grav, nx, ny, nz, rho, phi, lphi, 01, 01, 01)
  end do
  res = lphi - grav*rho
  call maxabs_subr (nx, ny, nz, res, error)
  if (error < max_error) return
  do iter=min_iter+1,max_iter
    call poisson_grid_reduced (dx, dy, dz, grav, nx, ny, nz, rho, phi, lphi, 10, 10, 10)
    call poisson_grid_reduced (dx, dy, dz, grav, nx, ny, nz, rho, phi, lphi, 01, 01, 01)
    res = lphi - grav*rho
    call maxabs_subr (nx, ny, nz, res, error)
    if (error < max_error) exit
  end do
  deallocate (res, lphi)
END SUBROUTINE

!***********************************************************************
SUBROUTINE mg_test
  integer, parameter:: m=100
  integer seed
  real, allocatable, dimension(:,:,:):: rho, phi
  real dx, dy, dz
  allocate (rho(m,m,m), phi(m,m,m))
  dx=1.; dy=dx; dz=dx; grav=1.
  seed = -11
  do iz=1,m; do iy=1,m; do ix=1,m
    phi(ix,iy,iz) = ran1s(seed)
    rho(ix,iy,iz) = 0.
   end do; end do; end do
  call poisson_multigrid (grav,m,m,m,rho,phi,dx,dy,dz)
  deallocate (rho,phi)
END
