! $Id: interpolate_bilinear.f90,v 1.6 2007/03/31 22:32:37 aake Exp $ 
!***********************************************************************
SUBROUTINE interpolate_in_y_x (mx, my, mz, f1, f2, hy, jy, py, ix)
  USE params, ONLY: izs, ize
  implicit none
  integer mx, my, mz
  real, dimension(mx,my,mz):: f1, f2
  real, dimension(mx, 1,mz):: hy
  real py
  integer ix, iy, iz, jy
!-----------------------------------------------------------------------
  do iz=izs,ize
    do iy=1,my-jy-1
      f2(ix,iy,iz)  = (1.-py)*f1(ix,iy+jy   ,iz) + py*f1(ix,iy+jy+1   ,iz)
    end do
    do iy=my-jy+1,my
      f2(ix,iy,iz)  = (1.-py)*f1(ix,iy+jy-my,iz) + py*f1(ix,iy+jy+1-my,iz)
    end do
    f2(ix,my-jy,iz) = (1.-py)*f1(ix,      my,iz) + py*hy(ix,         1,iz)
  end do
END SUBROUTINE

!***********************************************************************
SUBROUTINE interpolate_in_z_x (mx, my, mz, f1, f2, hz, jz, pz, ix)
  USE params, ONLY: iys, iye
  implicit none
  integer mx, my, mz
  real, dimension(mx,my,mz):: f1, f2
  real, dimension(mx,my, 1):: hz
  real pz
  integer ix, iy, iz, jz
!-----------------------------------------------------------------------
  !$omp barrier
  do iz=1,mz-jz-1
    do iy=iys,iye
      f2(ix,iy,iz)  = (1.-pz)*f1(ix,iy,iz+jz   ) + pz*f1(ix,iy,iz+jz+1   )
    end do
  end do
  do iy=iys,iye
    f2(ix,iy,mz-jz) = (1.-pz)*f1(ix,iy,      mz) + pz*hz(ix,iy, 1        )
  end do
  do iz=mz-jz+1,mz
    do iy=iys,iye
      f2(ix,iy,iz)  = (1.-pz)*f1(ix,iy,iz+jz-mz) + pz*f1(ix,iy,iz+jz+1-mz)
    end do
  end do
  !$omp barrier
END SUBROUTINE

!***********************************************************************
SUBROUTINE interpolate_in_x_y (mx, my, mz, f1, f2, hx, jx, px, iy)
  USE params, ONLY: izs, ize
  implicit none
  integer mx, my, mz
  real, dimension(mx,my,mz):: f1, f2
  real, dimension( 1,my,mz):: hx
  real px
  integer ix, iy, iz, jx
!-----------------------------------------------------------------------
  do iz=izs,ize
    do ix=1,mx-jx-1
      f2(ix,iy,iz)  = (1.-px)*f1(ix+jx   ,iy,iz) + px*f1(ix+jx+1   ,iy,iz)
    end do
    do ix=mx-jx+1,mx
      f2(ix,iy,iz)  = (1.-px)*f1(ix+jx-mx,iy,iz) + px*f1(ix+jx+1-mx,iy,iz)
    end do
    f2(mx-jx,iy,iz) = (1.-px)*f1(      mx,iy,iz) + px*hx(         1,iy,iz)
  end do
END SUBROUTINE

!***********************************************************************
SUBROUTINE interpolate_in_z_y (mx, my, mz, f1, f2, hz, jz, pz, iy)
  implicit none
  integer mx, my, mz
  real, dimension(mx,my,mz):: f1, f2
  real, dimension(mx,my, 1):: hz
  real pz
  integer ix, iy, iz, jz
!-----------------------------------------------------------------------
  do iz=1,mz-jz-1
    do ix=1,mx
      f2(ix,iy,iz)  = (1.-pz)*f1(ix,iy,iz+jz   ) + pz*f1(ix,iy,iz+jz+1   )
    end do
  end do
  do ix=1,mx
    f2(ix,iy,mz-jz) = (1.-pz)*f1(ix,iy,      mz) + pz*hz(ix,iy,1         )
  end do
  do iz=mz-jz+1,mz
    do ix=1,mx
      f2(ix,iy,iz)  = (1.-pz)*f1(ix,iy,iz+jz-mz) + pz*f1(ix,iy,iz+jz+1-mz)
    end do
  end do
END SUBROUTINE

!***********************************************************************
SUBROUTINE interpolate_in_x_z (mx, my, mz, f1, f2, hx, jx, px, iz)
  implicit none
  integer mx, my, mz
  real, dimension(mx,my,mz):: f1, f2
  real, dimension( 1,my,mz):: hx
  real px
  integer ix, iy, iz, jx
!-----------------------------------------------------------------------
  do iy=1,my
    do ix=1,mx-jx-1
      f2(ix,iy,iz)  = (1.-px)*f1(ix+jx   ,iy,iz) + px*f1(ix+jx+1   ,iy,iz)
    end do
    do ix=mx-jx+1,mx
      f2(ix,iy,iz)  = (1.-px)*f1(ix+jx-mx,iy,iz) + px*f1(ix+jx+1-mx,iy,iz)
    end do
    f2(mx-jx,iy,iz) = (1.-px)*f1(      mx,iy,iz) + px*hx(         1,iy,iz)
  end do
END SUBROUTINE

!***********************************************************************
SUBROUTINE interpolate_in_y_z (mx, my, mz, f1, f2, hy, jy, py, iz)
  implicit none
  integer mx, my, mz
  real, dimension(mx,my,mz):: f1, f2
  real, dimension(mx, 1,mz):: hy
  real py
  integer ix, iy, iz, jy
!-----------------------------------------------------------------------
  do iy=1,my-jy-1
    do ix=1,mx
      f2(ix,iy,iz)  = (1.-py)*f1(ix,iy+jy   ,iz) + py*f1(ix,iy+jy+1   ,iz)
    end do
  end do
  do ix=1,mx
    f2(ix,my-jy,iz) = (1.-py)*f1(ix,      my,iz) + py*hy(ix, 1        ,iz)
  end do
  do iy=my-jy+1,my
    do ix=1,mx
      f2(ix,iy,iz)  = (1.-py)*f1(ix,iy+jy-my,iz) + py*f1(ix,iy+jy+1-my,iz)
    end do
  end do
END SUBROUTINE

!***********************************************************************
SUBROUTINE test_interp_setup (mx, my, mz, f1, f3, &
                              hx, hy, hz, kx, ky, kz, px, py, pz)
  USE params, ONLY: izs, ize
  implicit none
  integer mx, my, mz
  real, dimension(mx,my,mz):: f1, f3
  real, dimension( 1,my,mz):: hx
  real, dimension(mx, 1,mz):: hy
  real, dimension(mx,my, 1):: hz
  integer kx, ky, kz
  real px, py, pz
  integer ix, iy, iz
  
  do iz=izs,ize
    do iy=1,my
      do ix=1,mx
        f1(ix,iy,iz) = mod(ix+kx,mx) + 2.* mod(iy+ky,my)  + 3.* mod(iz+kz,mz)  
        f3(ix,iy,iz) = mod(ix,mx)+px + 2.*(mod(iy,my)+py) + 3.*(mod(iz,mz)+pz)
      end do
    end do
  end do
  hx(1,:,:) = f1(1,:,:)
  hy(:,1,:) = f1(:,1,:)
  hz(:,:,1) = f1(:,:,1)
END SUBROUTINE

!***********************************************************************
SUBROUTINE test_interpolate
  USE params, ONLY: iys, iye, izs, ize
  implicit none
  integer, parameter:: mx=9, my=11, mz=13
  real, allocatable, dimension(:,:,:):: f1, f2, f3
  real, allocatable, dimension(:,:,:):: hx, hy, hz
  integer ix, iy, iz, jx, jy, jz, kx, ky, kz
  real px, py, pz
  logical x_ok, y_ok, z_ok
  real, parameter:: eps=1e-6
  
  allocate (f1(mx,my,mz), f2(mx,my,mz), f3(mx,my,mz))
  allocate (hx( 1,my,mz), hy(mx, 1,mz), hz( 1,my,mz))

!-----------------------------------------------------------------------
! primary direction: x
!-----------------------------------------------------------------------
  x_ok = .true.
  kx=0; px=0.; kz=0; pz=0.; ky=my/2; py=0.1; 
  call test_interp_setup  (mx, my, mz, f1, f3, hx, hy, hz, kx, ky, kz, px, py, pz)
  do ix=1,mx
    call interpolate_in_y_x (mx, my, mz, f1, f2, hy, my-ky, py, ix)
  end do
  !$omp barrier
  do jy=ky-1,ky+1
    iy = mod(jy,my)+1
    x_ok = x_ok .and. maxval(abs(f2(:,iy,:)-f3(:,iy,:))) < eps
    !print 2, iy, maxloc(abs(f2(:,iy,:)-f3(:,iy,:))), maxval(abs(f2(:,iy,:)-f3(:,iy,:))), f1(1,iy,1), f2(1,iy,1), f3(1,iy,1)
2   format(i4,2i3,5f12.6)
1   format(i4,5e14.6)
  end do
  
  kx=0; px=0.; ky=0; py=0.; kz=mz/2; pz=0.25; 
  call test_interp_setup  (mx, my, mz, f1, f3, hx, hy, hz, kx, ky, kz, px, py, pz)
  do ix=1,mx
    call interpolate_in_z_x (mx, my, mz, f1, f2, hz, mz-kz, pz, ix)
  end do
  !$omp barrier
  do jz=kz-1,kz+1
    iz = mod(jz,mz)+1
    x_ok = x_ok .and. maxval(abs(f2(:,:,iz)-f3(:,:,iz))) < eps
    !print 1,iz,maxval(abs(f2(:,:,iz)-f3(:,:,iz)))
  end do
  if (x_ok) then
    print *,'yz-interpolation with x primary OK'
  else
    print *,'**************** yz-interpolation with x primary NOK ***********************'
  end if

!-----------------------------------------------------------------------
! primary direction: y
!-----------------------------------------------------------------------
  y_ok = .true.
  ky=0; py=0.; kz=0; pz=0.; kx=mx/2; px=0.25; 
  call test_interp_setup  (mx, my, mz, f1, f3, hx, hy, hz, kx, ky, kz, px, py, pz)
  do iy=1,my
    call interpolate_in_x_y (mx, my, mz, f1, f2, hx, mx-kx, px, iy)
  end do
  !$omp barrier
  do jx=kx-1,kx+1
    ix = mod(jx,mx)+1
    y_ok = y_ok .and. maxval(abs(f2(ix,:,:)-f3(ix,:,:))) < eps
    !print 1,ix,maxval(abs(f2(ix,:,:)-f3(ix,:,:)))
  end do
  
  kx=0; px=0.; ky=0; py=0.; kz=mz/2; pz=0.25; 
  call test_interp_setup  (mx, my, mz, f1, f3, hx, hy, hz, kx, ky, kz, px, py, pz)
  do iy=iys,iye
    call interpolate_in_z_y (mx, my, mz, f1, f2, hz, mz-kz, pz, iy)
  end do
  !$omp barrier
  do jz=kz-1,kz+1
    iz = mod(jz,mz)+1
    y_ok = y_ok .and. maxval(abs(f2(:,:,iz)-f3(:,:,iz))) < eps
    !print 1,iz,maxval(abs(f2(:,:,iz)-f3(:,:,iz)))
  end do
  if (y_ok) then
    print *,'xz-interpolation with y primary OK'
  else
    print *,'**************** xz-interpolation with y primary NOK ***********************'
  end if

!-----------------------------------------------------------------------
! primary direction: z
!-----------------------------------------------------------------------
  z_ok = .true.
  ky=0; py=0.; kz=0; pz=0.; kx=mx/2; px=0.25; 
  call test_interp_setup  (mx, my, mz, f1, f3, hx, hy, hz, kx, ky, kz, px, py, pz)
  do iz=izs,ize
    call interpolate_in_x_z (mx, my, mz, f1, f2, hx, mx-kx, px, iz)
  end do
  !$omp barrier
  do jx=kx-1,kx+1
    ix = mod(jx,mx)+1
    z_ok = z_ok .and. maxval(abs(f2(ix,:,:)-f3(ix,:,:))) < eps
    !print 1,ix,maxval(abs(f2(ix,:,:)-f3(ix,:,:)))
  end do
  
  kx=0; px=0.; kz=0; pz=0.; ky=mz/2; py=0.25; 
  call test_interp_setup  (mx, my, mz, f1, f3, hx, hy, hz, kx, ky, kz, px, py, pz)
  do iz=izs,ize
    call interpolate_in_y_z (mx, my, mz, f1, f2, hy, my-ky, py, iz)
  end do
  !$omp barrier
  do jy=ky-1,ky+1
    iy = mod(jy,my)+1
    z_ok = z_ok .and. maxval(abs(f2(:,iy,:)-f3(:,iy,:))) < eps
    !print 1,iy,maxval(abs(f2(:,iy,:)-f3(:,iy,:)))
  end do
  if (z_ok) then
    print *,'xy-interpolation with z primary OK'
  else
    print *,'**************** xy-interpolation with z primary NOK ***********************'
  end if
  
  deallocate (f1, f2, f3, hx, hy, hz)
END

