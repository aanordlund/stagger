
!***********************************************************************
SUBROUTINE read_dim
  USE params
  integer i
!-------------------------------------------------------
!  Read text file intended for IDL procedure open.pro
!-------------------------------------------------------
  open (2, file=file(1:index(file,'.'))//'dx', &
    form='formatted')
  read (2,'(3i5)') mx,my,mz
  read (2,'(3e14.6)') dx,dy,dz
  read (2,*) gamma,i
  read (2,*) mvar
  close (2)
  if (idbg.gt.0) print *,'read_dim: mx,my,mz,mvar=',mx,my,mz,mvar
END

!***********************************************************************
PROGRAM power
  USE params
  USE stagger

  integer:: nk, nvar
  real, allocatable, dimension(:):: px, py, pz, pcx, pcy, pcz, ptot, fk
  real, allocatable, dimension(:,:,:):: lnr, scr, pxyz, uj
  complex, allocatable, dimension(:,:,:):: uxc, uyc, uzc
  real:: t0, average

  dx = 1.
  dy = 1.
  dz = 1.

  print *,'file, snap1, snap2 (0...) ?'
  read *,file,isnap1,isnap2

  call read_dim
  print *,mx,my,mz,mvar

  allocate(pxyz(mx,my,mz),uj(mx,my,mz))
  allocate(lnr(mx,my,mz),scr(mx,my,mz))
  allocate(uxc(mx,my,mz),uyc(mx,my,mz),uzc(mx,my,mz))

  nk=min(mx,my,mz)/2
  allocate (px(nk),py(nk),pz(nk),pcx(nk),pcy(nk),pcz(nk),ptot(nk),fk(nk))

  idot=index(file,'.')
  print *,file(1:idot)//'powerc'

  open (1, file=file, form='unformatted', &
    access='direct', recl=lrec(mx*my))
    
  do isnap=isnap1,isnap2

    do k=1,mz
      read (1,rec=k+mz*(0+isnap*mvar)) lnr(:,:,k)
      lnr(:,:,k)=alog(lnr(:,:,k))
    end do

    print *,'read x'
    do k=1,mz
      read (1,rec=k+mz*(1+isnap*mvar)) pxyz(:,:,k)
    end do
    call xdn_set(lnr,scr)
    uj=pxyz/exp(scr)

    do k=1,mz
      uxc(:,:,k) = uj(:,:,k)
    end do

    print *,'read y'
    do k=1,mz
      read (1,rec=k+mz*(2+isnap*mvar)) pxyz(:,:,k)
    end do
    call ydn_set(lnr,scr)
    uj=pxyz/exp(scr)

    do k=1,mz
      uyc(:,:,k) = uj(:,:,k)
    end do

    print *,'read z'
    do k=1,mz
      read (1,rec=k+mz*(3+isnap*mvar)) pxyz(:,:,k)
    end do
    call zdn_set(lnr,scr)
    uj=pxyz/exp(scr)

    do k=1,mz
      uzc(:,:,k) = uj(:,:,k)
    end do

    print *,'fft3d'
    call fft3d(uxc,mx,my,mz)
    call fft3d(uyc,mx,my,mz)
    call fft3d(uzc,mx,my,mz)

    print *,'power of full vel'
    call powerc (uxc, mx, my, mz, dx, dy, dz, px, fk, nk)
    call powerc (uyc, mx, my, mz, dx, dy, dz, py, fk, nk)
    call powerc (uzc, mx, my, mz, dx, dy, dz, pz, fk, nk)

    print *,'helmhc'
    call helmhc (uxc, uyc, uzc, mx, my, mz, dx, dy, dz)

    print *,'power of rotational'
    call powerc (uxc, mx, my, mz, dx, dy, dz, pcx, fk, nk)
    call powerc (uyc, mx, my, mz, dx, dy, dz, pcy, fk, nk)
    call powerc (uzc, mx, my, mz, dx, dy, dz, pcz, fk, nk)

    open (2, file=file(1:idot)//'powerc', form='unformatted', &
      access='direct', recl=lrec(nk))
    write (2, rec=1+7*isnap) fk
    write (2, rec=2+7*isnap) px
    write (2, rec=3+7*isnap) py
    write (2, rec=4+7*isnap) pz
    write (2, rec=5+7*isnap) pcx
    write (2, rec=6+7*isnap) pcy
    write (2, rec=7+7*isnap) pcz
    close (2)
  end do

  close (1)

  end
