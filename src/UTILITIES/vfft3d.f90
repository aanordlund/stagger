! $Id: vfft3d.f90,v 1.1 2004/03/19 00:26:18 aake Exp $
!----------------------------------------------------------------------
!
!  Three-dimensional Fourier transform routines, based on FFTPACK.
!  FFTPACK is transportable, and allows arbitrary dimensions (speed
!  will vary, of course).
!
!  CALL fft3df (f, ft, mx, my, mz)              ! forward transform
!  CALL fft3db (ft, f, mx, my, mz)              ! backward transform
!  CALL fft3d_k2 (k2, dx, dy, dz, mx, my, mz)   ! compute k2
!
!**********************************************************************
SUBROUTINE fft3d_k2(k2,dx,dy,dz,mx,my,mz)
  real, dimension(mx,my,mz):: k2
  real pi, dx, dy, dz, kx, ky, kz
  integer i, j, k, mx, my, mz
!----------------------------------------------------------------------

  pi=4.*atan(1.)

  do k=1,mz
    kz=2.*pi/(dz*mz)*(k/2)
    do j=1,my
      ky=2.*pi/(dy*my)*(j/2)
      do i=1,mx
        kx=2.*pi/(dx*mx)*(i/2)
        k2(i,j,k)=kx**2+ky**2+kz**2
      end do
    end do
  end do

END SUBROUTINE

!**********************************************************************
SUBROUTINE fft3df (r,rt,mx,my,mz)
  use vfftpack
  implicit none

  integer i,j,k,mx,my,mz
  integer, parameter:: mw=256 !, mwx=mx+15, mwy=my+15, mwz=mz+15

  real, dimension(mx,my,mz):: r, rt
  real c, wx(mx+15), wy(my+15), wz(mz+15), fx(mx), fy(my), fz(mz)
!----------------------------------------------------------------------

  call fti (wx)
  call fti (wy)
  call fti (wz)

  do k=1,mz
  do j=1,my
    fx=r(:,j,k)
    call ftrf (fx,wx)
    rt(:,j,k)=fx
  end do
  end do

  do k=1,mz
  do i=1,mx
    fy=rt(i,:,k)
    call ftrf (fy,wy)
    rt(i,:,k)=fy
  end do
  end do

  do j=1,my
  do i=1,mx
    fz=rt(i,j,:)
    call ftrf (fz,wz)
    rt(i,j,:)=fz
  end do
  end do

END SUBROUTINE

!**********************************************************************
SUBROUTINE fft3db (r,rt,mx,my,mz)
  use vfftpack
  implicit none

  integer i,j,k,mx,my,mz
  integer, parameter:: mw=256 !, mwx=mx+15, mwy=my+15, mwz=mz+15

  real, dimension(mx,my,mz):: r, rt
  real wx(mx+15), wy(my+15), wz(mz+15), fx(mx), fy(my), fz(mz)
!----------------------------------------------------------------------

  call fti (wx)
  call fti (wy)
  call fti (wz)

  do k=1,mz
  do j=1,my
    fx=r(:,j,k)
    call ftrb (fx,wx)
    rt(:,j,k)=fx
  end do
  end do

  do k=1,mz
  do i=1,mx
    fy=rt(i,:,k)
    call ftrb (fy,wy)
    rt(i,:,k)=fy
  end do
  end do

  do j=1,my
  do i=1,mx
    fz=rt(i,j,:)
    call ftrb (fz,wz)
    rt(i,j,:)=fz
  end do
  end do

END SUBROUTINE
