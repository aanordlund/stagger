! $Id: fft2d_isend.f90,v 1.37 2015/03/14 20:32:43 aake Exp $
!------------------------------------------------------------------------------
!
!  Two-dimensional Fourier transform routines, based on FFTPACK.
!  FFTPACK is transportable, and allows arbitrary dimensions (speed
!  will vary, of course).
!
!  CALL fft2df (f, ft, mx, my)              ! forward transform
!  CALL fft2db (ft, f, mx, my)              ! backward transform
!  CALL fft2d_k2 (k2, sx, sy, mx, my)       ! compute k2
!
!******************************************************************************
SUBROUTINE fft2d_k2 (k2, sx, sz, mx, mz)
  USE params, only: mpi_x, mpi_z, mpi_nx, mpi_nz, mpi_rank, pi
  implicit none
  real, dimension(mx,mz):: k2
  real sx, sz, kx, kz
  integer i, j, k, mx, mz
!------------------------------------------------------------------------------
  !$omp single
  do j=1,mz
    kz = 2.*pi/sz*((j+mpi_z*mz)/2)
    do i=1,mx
      kx = 2.*pi/sx*((i+mpi_x*mx)/2)
      k2(i,j) = kx**2+kz**2
    end do
  end do
  !$omp end single
END SUBROUTINE

!*******************************************************************************
SUBROUTINE dbg_write (info, m1, m2, buf)
  USE params, only: mpi_rank
  implicit none
  character(len=*) info
  integer m1, m2, l
  real buf(m1,m2)
  character(len=80) string
  logical, save:: debug=.false.
!------------------------------------------------------------------------------
  if (.not.debug) return
  l = len(trim(info))
  string(1:l)=trim(info)
  string(l+1:80)=' '
  write (20+mpi_rank) string, m1 ,m2
  write (20+mpi_rank) buf
END

!*******************************************************************************
SUBROUTINE fft2df (r, rt, mx, mz)
  USE params, only: mpi_nx, mpi_nz, mid
  implicit none
  integer mx, mz, nx, nz
  real, dimension(mx,mz):: r, rt
  external srfftf
  character(len=mid),save:: id='$Id: fft2d_isend.f90,v 1.37 2015/03/14 20:32:43 aake Exp $'
  call print_id(id)
  !$omp single
  nx = mx*mpi_nx
  nz = mz*mpi_nz
  call dbg_write ("input to trf_x", mx, mz, r)
  call trf_x (srfftf,  r, rt, mx, mz, 1.0/nx)
  call dbg_write ("input after trf_x", mx, mz, r)
  call dbg_write ("output from trf_x", mx, mz, r)
  call trf_z (srfftf, rt, rt, mx, mz, 1.0/nz)
  call dbg_write ("output from trf_z", mx, mz, r)
  !$omp end single
END SUBROUTINE fft2df

!*******************************************************************************
SUBROUTINE fft2db (r, rt, mx, mz)
  implicit none
  integer mx, mz
  real, dimension(mx,mz):: r, rt
  external srfftb
  !$omp single
  call dbg_write ("input to trf_x", mx, mz, r)
  call trf_x (srfftb,  r, rt, mx, mz, 1.0)
  call dbg_write ("input after trf_x", mx, mz, r)
  call dbg_write ("output from trf_x", mx, mz, r)
  call trf_z (srfftb, rt, rt, mx, mz, 1.0)
  call dbg_write ("output from trf_z", mx, mz, r)
  !$omp end single
END SUBROUTINE fft2db

!*******************************************************************************
SUBROUTINE fft2dfy (r, rt, mx, my, mz)
  USE params, only: mxtot, mztot
  implicit none
  integer mx, my, mz
  real, dimension(mx,mz):: r, rt
  external srfftb
  !$omp single
  call trf_xy (srfftb,  r, rt, mx, my, mz, 1.0/mxtot)
  call trf_zy (srfftb, rt, rt, mx, my, mz, 1.0/mxtot)
  !$omp end single
END SUBROUTINE fft2dfy

!*******************************************************************************
SUBROUTINE fft2dby (r, rt, mx, my, mz)
  implicit none
  integer mx, my, mz
  real, dimension(mx,mz):: r, rt
  external srfftb
  !$omp single
  call trf_xy (srfftb,  r, rt, mx, my, mz, 1.0)
  call trf_zy (srfftb, rt, rt, mx, my, mz, 1.0)
  !$omp end single
END SUBROUTINE fft2dby

!******************************************************************************
SUBROUTINE trf_x (fft, f2, t2, mx, mz, c)
  USE params, only: mpi_x, mpi_y, mpi_z, mpi_nx, mpi_nz, mpi_rank, mpi_size, &
                    mpi_nmax, master
  USE mpi_mod, only: my_real, MPI_STATUSES_IGNORE, MPI_STATUS_SIZE, &
    mpi_comm_plane, mpi_comm_beam
  implicit none
  integer mx, mz
  real, dimension(mx,mz):: f2, t2
  real c
  !integer, parameter:: mpi_nmax=2
  integer nrq, tag, ierr, lz, nrank, rank, comm
  integer ix, iz, i_x, i_z, nw, nx, nz, mxz, iz1, iz2
  integer status(MPI_STATUS_SIZE)
  real, allocatable, dimension(:)    ::  f1, w1, rq
  real, allocatable, dimension(:,:)  ::  rcv, buf2
  real, allocatable, dimension(:,:,:)::  snd
  logical, save:: verbose=.true., first_time=.true.
  logical worker, remote
  external fft
  !-----------------------------------------------------------------------------
  ! Initialize
  !-----------------------------------------------------------------------------
  comm = mpi_comm_beam(1)                                                       ! short hand
  nx = mx*mpi_nx                                                                ! transform sie
  nw = 4*nx + 15                                                                ! size of work array
  allocate (f1(nx))                                                             ! allocate 1D-array
  allocate (w1(nw))                                                             ! allocate FFT work array
  call srffti (nx,w1)                                                           ! compute work array
  nrank = min(mpi_nx, mpi_nmax)                                                 ! number of ranks to use
  lz = (mz-1)/nrank + 1                                                         ! iz-values per rank
  nrank = (mz-1)/lz + 1                                                         ! make sure not to overrun
  mxz = mx*lz                                                                   ! buffer size
  tag = 1                                                                       ! x-tag
  !-----------------------------------------------------------------------------
  nrq = max(nrank-1, mpi_nx-1)
  allocate (snd(mx,lz,nrq), rcv(mx,lz), buf2(nx,lz), rq(nrq))                   ! buffers
  !-----------------------------------------------------------------------------
  first_time = first_time.and.master                                            ! only on master
  if (first_time) print*,'fft2d: ranks and width in x-direction =',nrank,lz,mz  ! info
  first_time = .false.
  !-----------------------------------------------------------------------------
  ! Send to worker ranks
  !-----------------------------------------------------------------------------
  if (nrank > 1) call MPI_Barrier (mpi_comm_plane(2), ierr)                     ! for good measure
  nrq = 0                                                                       ! reset number of requests
  iz1 = 1                                                                       ! first iz 
  do rank=0,nrank-1                                                             ! loop over workers
    iz2 = min(iz1+lz-1,mz)                                                      ! last iz
    if (mpi_x /= rank) then                                                     ! if not worker
      nrq = nrq+1                                                               ! number of sends
      snd(:,1:1+iz2-iz1,nrq) = f2(:,iz1:iz2)
      call dbg_write("sending this to work rank", mx, lz, snd(:,:,nrq))
      call MPI_Isend (snd(:,:,nrq), &
                           mxz, my_real, rank, tag, comm, rq(nrq), ierr)       ! send to worker
    end if
    iz1 = iz2 + 1                                                               ! update first iz
  end do
  !-----------------------------------------------------------------------------
  ! Assemble the slice we are to work on
  !-----------------------------------------------------------------------------
  iz1 = 1
  do rank=0,nrank-1                                                             ! loop over workers
    iz2 = min(iz1+lz-1,mz)                                                      ! last interval may be shorter
    if (mpi_x == rank) then                                                     ! if worker
      buf2(mpi_x*mx+1:mpi_x*mx+mx,1:1+iz2-iz1) = f2(:,iz1:iz2)                  ! local part
      call dbg_write ("copying this directly", mx, lz, f2(:,iz1:iz2))
      do i_x=0,mpi_nx-1                                                         ! loop over senders
        if (i_x == rank) cycle                                                  ! skip local
        call MPI_Recv (rcv, mxz, my_real, i_x, tag, comm, status, ierr)        ! receive from sender
        call dbg_write ("received from remote", mx, lz, rcv)
        buf2(i_x*mx+1:i_x*mx+mx,1:1+iz2-iz1) = rcv(:,1:1+iz2-iz1)               ! remote part
      end do
    end if
    iz1 = iz2 + 1                                                               ! next stripe
  end do
  call waitall_mpi (nrq, rq)                                                    ! satisfy requests
  !-----------------------------------------------------------------------------
  ! Do the transforms and send the results back.
  ! nrank times, and for each rank there are mpi_nx-1 sends.  The reciever loop
  ! is also executet nrank times, and for each time there are mpi_nx-1 ranks
  ! that perform a receive. I.e., there are nrank*(mpi_nx-1) sends and recvs.
  !-----------------------------------------------------------------------------
  iz1 = 1
  nrq = 0
  do rank=0,nrank-1
    iz2 = min(iz1+lz-1,mz)                                                      ! prevent overrun
    if (mpi_x == rank) then                                                     ! worker
      call dbg_write ("data to be transformed", nx, lz, buf2)
      do iz=1,1+iz2-iz1                                                         ! relevant part only
        f1 = buf2(:,iz)                                                         ! data to transform
        call fft (nx, f1, w1)                                                   ! transform
        buf2(:,iz) = c*f1                                                       ! scale result
      end do
      call dbg_write ("transformed data", nx, lz, buf2)
      do i_x=0,mpi_nx-1                                                         ! loop over remotes
        if (i_x == rank) cycle                                                  ! skip local
        nrq = nrq+1                                                             ! count requests
        snd(:,:,nrq) = buf2(i_x*mx+1:i_x*mx+mx,:)                               ! ensure correct shape
        call dbg_write ("sending this to remote", mx, lz, snd(:,:,nrq))
        call MPI_Isend (snd(:,:,nrq), &
                             mxz, my_real, i_x, tag, comm, rq(nrq), ierr)      ! send to remote
      end do
      t2(:,iz1:iz2) = buf2(mpi_x*mx+1:mpi_x*mx+mx,1:1+iz2-iz1)                  ! local part
      call dbg_write ("copying directly", mx, iz2-iz1+1, t2(:,iz1:iz2))
    end if
    iz1 = iz2 + 1
  end do
  !-----------------------------------------------------------------------------
  ! Assemble results
  !-----------------------------------------------------------------------------
  iz1 = 1
  do rank=0,nrank-1
    iz2 = min(iz1+lz-1,mz)
    if (mpi_x /= rank) then                                                     ! remote
      call MPI_Recv (rcv, mxz, my_real, rank, tag, comm, status, ierr)         ! recv from worker
      t2(:,iz1:iz2) = rcv(:,1:1+iz2-iz1)                                        ! use relevant part
      call dbg_write ("received from remote", mx, lz, rcv)
    end if
    iz1 = iz2 + 1
  end do
  call waitall_mpi (nrq, rq)                                                    ! satisfy requests
  !-----------------------------------------------------------------------------
  ! Done
  !-----------------------------------------------------------------------------
  if (nrank > 1) call MPI_Barrier (mpi_comm_plane(2), ierr)                     ! for good measure
  call dbg_write ("final result", mx, mz, t2)
  deallocate (f1, w1, snd, rcv, buf2, rq)
  verbose=.false.
END SUBROUTINE trf_x

!******************************************************************************
SUBROUTINE trf_z (fft, f2, t2, mx, mz, c)
  USE params, only: mpi_x, mpi_y, mpi_z, mpi_nx, mpi_nz, mpi_rank, mpi_size, &
                    mpi_nmax, master
  USE mpi_mod, only: my_real, MPI_STATUSES_IGNORE, MPI_STATUS_SIZE, &
    mpi_comm_plane, mpi_comm_beam
  implicit none
  integer mx, mz
  real, dimension(mx,mz):: f2, t2
  real c
  !integer, parameter:: mpi_nmax=2
  integer nrq, tag, ierr, lx, nrank, rank, comm
  integer ix, iz, i_x, i_z, nw, nx, nz, mxz, ix1, ix2
  integer status(MPI_STATUS_SIZE)
  real, allocatable, dimension(:)    ::  f1, w1, rq
  real, allocatable, dimension(:,:)  ::  rcv, buf2
  real, allocatable, dimension(:,:,:)::  snd
  logical, save:: verbose=.false., first_time=.true.
  external fft
  !-----------------------------------------------------------------------------
  ! Initialize
  !-----------------------------------------------------------------------------
  comm = mpi_comm_beam(3)                                                       ! short hand
  nz = mz*mpi_nz                                                                ! transform sie
  nw = 4*nz + 15                                                                ! size of work array
  allocate (f1(nz))                                                             ! allocate 1D-array
  allocate (w1(nw))                                                             ! allocate FFT work array
  call srffti (nz,w1)                                                           ! compute work array
  nrank = min(mpi_nz, mpi_nmax)                                                 ! number of ranks to use
  if (mx < 9 .and. mpi_nz > mx) nrank=mx                                        ! avoid integer problem
  lx = (mx-1)/nrank + 1                                                         ! iz-values per rank
  nrank = (mx-1)/lx + 1                                                         ! make sure not to overrun
  mxz = lx*mz                                                                   ! buffer size
  tag = 2                                                                       ! z-tag
  !-----------------------------------------------------------------------------
  nrq = max(nrank-1, mpi_nz-1)
  allocate (snd(lx,mz,nrq), rcv(lx,mz), buf2(lx,nz), rq(nrq))                   ! buffers
  !-----------------------------------------------------------------------------
  first_time = first_time.and.master                                            ! only on master
  if (first_time) print*,'fft2d: ranks and width in z-direction =',nrank,lx,mx
  first_time = .false.
  !-----------------------------------------------------------------------------
  ! Send to worker ranks
  !-----------------------------------------------------------------------------
  if (nrank > 1) call MPI_Barrier (mpi_comm_plane(2), ierr)                     ! for good measure
  nrq = 0                                                                       ! reset number of requests
  ix1 = 1                                                                       ! first ix 
  do rank=0,nrank-1                                                             ! loop over workers
    ix2 = min(ix1+lx-1,mx)                                                      ! last ix
    if (mpi_z /= rank) then                                                     ! if not worker
      nrq = nrq+1                                                               ! number of sends
      snd(1:1+ix2-ix1,:,nrq) = f2(ix1:ix2,:)
      call MPI_Isend (snd(:,:,nrq),mxz,my_real,rank,tag,comm,rq(nrq),ierr)     ! send to worker
    end if
    ix1 = ix2 + 1                                                               ! update first ix
  end do
  !-----------------------------------------------------------------------------
  ! Assemble the slice we are to work on
  !-----------------------------------------------------------------------------
  ix1 = 1
  do rank=0,nrank-1                                                             ! loop over workers
    ix2 = min(ix1+lx-1,mx)                                                      ! last interval may be shorter
    if (mpi_z == rank) then                                                     ! if worker
      buf2(1:1+ix2-ix1,rank*mz+1:rank*mz+mz) = f2(ix1:ix2,:)                    ! local part
      do i_z=0,mpi_nz-1                                                         ! loop over senders
        if (i_z == rank) cycle                                                  ! skip local
        call MPI_Recv (rcv, mxz, my_real, i_z, tag, comm, status, ierr)        ! receive from sender
        buf2(1:1+ix2-ix1,i_z*mz+1:i_z*mz+mz) = rcv(1:1+ix2-ix1,:)               ! remote part
      end do
    end if
    ix1 = ix2 + 1
  end do
  call waitall_mpi (nrq, rq)                                                    ! satisfy requests
  !-----------------------------------------------------------------------------
  ! Do the transforms and send the results back.  The send loop is executed
  ! nrank times, and for each rank there are mpi_nx-1 sends.  The reciever loop
  ! is also executet nrank times, and for each time there are mpi_nx-1 ranks
  ! that perform a receive. I.e., there are nrank*(mpi_nx-1) sends and recvs.
  !-----------------------------------------------------------------------------
  ix1 = 1
  nrq = 0
  do rank=0,nrank-1
    ix2 = min(ix1+lx-1,mx)
    if (mpi_z == rank) then                                                     ! worker
      do ix=1,1+ix2-ix1                                                         ! relevant x-range
        f1 = buf2(ix,:)                                                         ! data to transform
        call fft (nz, f1, w1)                                                   ! transform
        buf2(ix,:) = c*f1                                                       ! scale result
      end do
      do i_z=0,mpi_nz-1                                                         ! loop over remotes
        if (i_z == rank) cycle                                                  ! skip local
        nrq = nrq+1                                                             ! count requests
        snd(:,:,nrq) = buf2(:,i_z*mz+1:i_z*mz+mz)
        call MPI_Isend (snd(:,:,nrq),mxz,my_real,i_z,tag,comm,rq(nrq),ierr)    ! send to remote
      end do
      t2(ix1:ix2,:) = buf2(1:1+ix2-ix1,rank*mz+1:rank*mz+mz)                    ! local part
    end if
    ix1 = ix2 + 1
  end do
  !-----------------------------------------------------------------------------
  ! Assemble results
  !-----------------------------------------------------------------------------
  ix1 = 1
  do rank=0,nrank-1
    ix2 = min(ix1+lx-1,mx)
    if (mpi_z /= rank) then                                                     ! remote
      call MPI_Recv (rcv, mxz, my_real, rank, tag, comm, status, ierr)         ! recv from worker
      t2(ix1:ix2,:) = rcv(1:1+ix2-ix1,:)                                        ! data from worker
    end if
    ix1 = ix2 + 1
  end do
  call waitall_mpi (nrq, rq)                                                    ! satisfy requests
  if (nrank > 1) call MPI_Barrier (mpi_comm_plane(2), ierr)                     ! for good measure
  !-----------------------------------------------------------------------------
  ! Done
  !-----------------------------------------------------------------------------
  deallocate (f1, w1, snd, rcv, buf2, rq)
  verbose = .false.
END SUBROUTINE trf_z

!******************************************************************************
SUBROUTINE trf_xy (fft, f3, t3, mx, my, mz, c)
  USE params, only: mpi_x, mpi_y, mpi_z, mpi_nx, mpi_nz, mpi_rank, mpi_size, &
                    mpi_nmax, master
  USE mpi_mod, only: my_real, MPI_STATUSES_IGNORE, MPI_STATUS_SIZE, &
    mpi_comm_plane, mpi_comm_beam
  implicit none
  integer mx, my, mz
  real, dimension(mx,my,mz):: f3, t3
  real c
  !integer, parameter:: mpi_nmax=55
  integer nrq, tag, ierr, lz, nrank, rank, comm
  integer ix, iy, iz, i_x, i_z, nw, nx, nz, mxyz, ix1, ix2, iz1, iz2
  integer status(MPI_STATUS_SIZE)
  real, allocatable, dimension(:)    ::  f1, w1, rq
  real, allocatable, dimension(:,:,:)::  buf3, rcv, snd
  logical, save:: verbose=.true., first_time=.true.
  logical worker, remote
  external fft
  !-----------------------------------------------------------------------------
  ! Initialize
  !-----------------------------------------------------------------------------
  comm = mpi_comm_beam(1)                                                       ! short hand
  nx = mx*mpi_nx                                                                ! transform sie
  nw = 4*nx + 15                                                                ! size of work array
  allocate (f1(nx))                                                             ! allocate 1D-array
  allocate (w1(nw))                                                             ! allocate FFT work array
  call srffti (nx,w1)                                                           ! compute work array
  nrank = min(mpi_nx, mz, mpi_nmax)                                             ! number of ranks to use
  lz = (mz-1)/nrank + 1                                                         ! iz-values per rank
  nrank = (mz-1)/lz + 1                                                         ! make sure not to overrun
  !-----------------------------------------------------------------------------
  nrq = max(nrank-1, mpi_nx-1)
  allocate (snd(mx,my,lz), rcv(mx,my,lz), buf3(my,lz,nx), rq(nrq))              ! buffers
  !-----------------------------------------------------------------------------
  first_time = first_time.and.master                                            ! only on master
  if (first_time) print*,'trf_xy: ranks and width in x-direction =',nrank,lz,mz ! info
  first_time = .false.
  !-----------------------------------------------------------------------------
  ! Send to worker ranks
  !-----------------------------------------------------------------------------
  if (nrank > 1) call MPI_Barrier (mpi_comm_plane(2), ierr)                     ! for good measure
  tag = 1                                                                       ! x-tag
  nrq = 0                                                                       ! reset number of requests
  iz1 = 1                                                                       ! first iz 
  do rank=0,nrank-1                                                             ! loop over workers
    iz2 = min(iz1+lz-1,mz)                                                      ! last iz
    nz = iz2-iz1+1
    if (mpi_x /= rank) then                                                     ! if not worker
      nrq = nrq+1                                                               ! number of sends
      mxyz = mx*my*nz                                                           ! buffer size
      call MPI_Isend (f3(:,:,iz1:iz2), &
                           mxyz, my_real, rank, tag, comm, rq(nrq), ierr)      ! send to worker
    end if
    iz1 = iz2 + 1                                                               ! update first iz
  end do
                                               !write (10+mpi_rank) f3
  !-----------------------------------------------------------------------------
  ! Assemble the slice we are to work on
  !-----------------------------------------------------------------------------
  iz1 = 1
  do rank=0,nrank-1                                                             ! loop over workers
    iz2 = min(iz1+lz-1,mz)                                                      ! last interval may be shorter
    nz = iz2-iz1+1
    if (mpi_x == rank) then                                                     ! if worker
      do ix=1,mx
        buf3(:,1:nz,mpi_x*mx+ix) = f3(ix,:,iz1:iz2)                             ! local part
      end do
      do i_x=0,mpi_nx-1                                                         ! loop over senders
        if (i_x == rank) cycle                                                  ! skip local
        mxyz = mx*my*nz                                                         ! buffer size
        call MPI_Recv (rcv, mxyz, my_real, i_x, tag, comm, status, ierr)       ! receive from sender
        do ix=1,mx
          buf3(:,1:nz,i_x*mx+ix) = rcv(ix,:,1:nz)                               ! remote part
        end do
      end do
    end if
    iz1 = iz2 + 1                                                               ! next stripe
  end do
  call waitall_mpi (nrq, rq)                                                    ! satisfy requests
  deallocate (rcv)
  allocate (rcv(my,lz,mx))
                                               !write (10+mpi_rank) buf3
  !-----------------------------------------------------------------------------
  ! Do the transforms and send the results back.
  ! nrank times, and for each rank there are mpi_nx-1 sends.  The reciever loop
  ! is also executet nrank times, and for each time there are mpi_nx-1 ranks
  ! that perform a receive. I.e., there are nrank*(mpi_nx-1) sends and recvs.
  !-----------------------------------------------------------------------------
  iz1 = 1
  nrq = 0
  do rank=0,nrank-1
    iz2 = min(iz1+lz-1,mz)                                                      ! prevent overrun
    nz = iz2-iz1+1
    if (mpi_x == rank) then                                                     ! worker
      do iz=1,1+iz2-iz1                                                         ! relevant part only
      do iy=1,my
        f1 = buf3(iy,iz,:)                                                      ! data to transform
        call fft (nx, f1, w1)                                                   ! transform
        buf3(iy,iz,:) = c*f1                                                    ! scale result
      end do
      end do
      do i_x=0,mpi_nx-1                                                         ! loop over remotes
        if (i_x == rank) cycle                                                  ! skip local
        nrq = nrq+1                                                             ! count requests
        ix1 = i_x*mx + 1
        ix2 = i_x*mx + mx
        mxyz = mx*my*lz                                                         ! must send whole blocks
        call MPI_Isend (buf3(:,:,ix1:ix2), &
                             mxyz, my_real, i_x, tag, comm, rq(nrq), ierr)     ! send to remote
      end do
      do ix=1,mx
        t3(ix,:,iz1:iz2) = buf3(1:my,:,mpi_x*mx+ix)                             ! local part
      end do
    end if
    iz1 = iz2 + 1
  end do
  !-----------------------------------------------------------------------------
  ! Assemble results
  !-----------------------------------------------------------------------------
  iz1 = 1
  do rank=0,nrank-1
    iz2 = min(iz1+lz-1,mz)
    nz = iz2-iz1+1
    if (mpi_x /= rank) then                                                     ! remote
      mxyz = mx*my*lz                                                           ! buffer size
      call MPI_Recv (rcv, mxyz, my_real, rank, tag, comm, status, ierr)        ! recv from worker
      do ix=1,mx
        t3(ix,:,iz1:iz2) = rcv(:,1:nz,ix)                                       ! use relevant part
      end do
    end if
    iz1 = iz2 + 1
  end do
  call waitall_mpi (nrq, rq)                                                    ! satisfy requests
                                               !write (10+mpi_rank) buf3
                                               !write (10+mpi_rank) t3
  !-----------------------------------------------------------------------------
  ! Done
  !-----------------------------------------------------------------------------
  if (nrank > 1) call MPI_Barrier (mpi_comm_plane(2), ierr)                     ! for good measure
  deallocate (f1, w1, snd, rcv, buf3, rq)
  verbose=.false.
END SUBROUTINE trf_xy

!******************************************************************************
SUBROUTINE trf_zy (fft, f3, t3, mx, my, mz, c)
  USE params, only: mpi_x, mpi_y, mpi_z, mpi_nx, mpi_nz, mpi_rank, mpi_size, &
                    mpi_nmax, master
  USE mpi_mod, only: my_real, MPI_STATUSES_IGNORE, MPI_STATUS_SIZE, &
    mpi_comm_plane, mpi_comm_beam
  implicit none
  integer mx, my, mz
  real, dimension(mx,my,mz):: f3, t3
  real c
  !integer, parameter:: mpi_nmax=55
  integer nrq, tag, ierr, lx, nrank, rank, comm
  integer ix, iy, iz, i_x, i_z, nw, nx, nz, mxyz, ix1, ix2, iz1, iz2
  integer status(MPI_STATUS_SIZE)
  real, allocatable, dimension(:)    ::  f1, w1, rq
  real, allocatable, dimension(:,:,:)::  buf3, rcv, snd
  logical, save:: verbose=.false., first_time=.true.
  external fft
  !-----------------------------------------------------------------------------
  ! Initialize
  !-----------------------------------------------------------------------------
  comm = mpi_comm_beam(3)                                                       ! short hand
  nz = mz*mpi_nz                                                                ! transform sie
  nw = 4*nz + 15                                                                ! size of work array
  allocate (f1(nz))                                                             ! allocate 1D-array
  allocate (w1(nw))                                                             ! allocate FFT work array
  call srffti (nz,w1)                                                           ! compute work array
  nrank = min(mpi_nz, mx, mpi_nmax)                                             ! number of ranks to use
  lx = (mx-1)/nrank + 1                                                         ! iz-values per rank
  nrank = (mx-1)/lx + 1                                                         ! make sure not to overrun
  tag = 2                                                                       ! z-tag
  !-----------------------------------------------------------------------------
  nrq = max(nrank-1, mpi_nz-1)
  allocate (snd(my,mz,mx), rcv(my,mz,lx), buf3(my,lx,nz), rq(nrq))              ! buffers
  !-----------------------------------------------------------------------------
  first_time = first_time.and.master                                            ! only on master
  if (first_time) print*,'trf_zy: ranks and width in z-direction =',nrank,lx,mx
  first_time = .false.
  !-----------------------------------------------------------------------------
  ! Send to worker ranks. The snd and rcv buffers need to be (my,mz,mx), so
  ! whole slices of (my,mz) can be counted off over intervals ix1:ix2.
  !-----------------------------------------------------------------------------
  if (nrank > 1) call MPI_Barrier (mpi_comm_plane(2), ierr)                     ! for good measure
  nrq = 0                                                                       ! reset number of requests
  ix1 = 1                                                                       ! first ix 
  do rank=0,nrank-1                                                             ! loop over workers
    ix2 = min(ix1+lx-1,mx)                                                      ! last ix
    nx = ix2-ix1+1
    if (mpi_z /= rank) then                                                     ! if not worker
      nrq = nrq+1                                                               ! number of sends
      do ix=ix1,ix2
         snd(:,:,ix) = f3(ix,:,:)                                               ! transpose into buffer
      end do
      mxyz = nx*my*mz                                                           ! buffer size
      call MPI_Isend (snd(:,:,ix1:ix2), mxyz, my_real, rank, tag, comm, &
                      rq(nrq), ierr)                                            ! send to worker
    end if
    ix1 = ix2 + 1                                                               ! update first ix
  end do
                                               !write (10+mpi_rank) f3
  !-----------------------------------------------------------------------------
  ! Assemble the slice we are to work on
  !-----------------------------------------------------------------------------
  ix1 = 1
  do rank=0,nrank-1                                                             ! loop over workers
    ix2 = min(ix1+lx-1,mx)                                                      ! last interval may be shorter
    nx = ix2-ix1+1
    if (mpi_z == rank) then                                                     ! if worker
      iz1 = mpi_z*mz + 1
      iz2 = mpi_z*mz + mz
      do ix=1,nx
        buf3(:,ix,iz1:iz2) = f3(ix1+ix-1,:,:)                                   ! local part
      end do
      do i_z=0,mpi_nz-1                                                         ! loop over senders
        if (i_z == rank) cycle                                                  ! skip local
        mxyz = nx*my*mz                                                         ! buffer size
        call MPI_Recv (rcv, mxyz, my_real, i_z, tag, comm, status, ierr)       ! receive from sender
        iz1 = i_z*mz + 1
        iz2 = i_z*mz + mz
        do ix=1,nx
          buf3(:,ix,iz1:iz2) = rcv(:,:,ix)                                      ! remote part
        end do
      end do
    end if
    ix1 = ix2 + 1
  end do
  call waitall_mpi (nrq, rq)                                                    ! satisfy requests
  deallocate (rcv)
  allocate (rcv(my,lx,mz))
                                               !write (10+mpi_rank) buf3
  !-----------------------------------------------------------------------------
  ! Do the transforms and send the results back.
  !-----------------------------------------------------------------------------
  ix1 = 1
  nrq = 0
  do rank=0,nrank-1
    ix2 = min(ix1+lx-1,mx)
    nx = ix2-ix1+1
    if (mpi_z == rank) then                                                     ! worker
      do iy=1,my
      do ix=1,nx                                                                ! relevant x-range
        f1 = buf3(iy,ix,:)                                                      ! data to transform
        call fft (nz, f1, w1)                                                   ! transform
        buf3(iy,ix,:) = c*f1                                                    ! scale result
      end do
      end do
      do i_z=0,mpi_nz-1                                                         ! loop over remotes
        if (i_z == rank) cycle                                                  ! skip local
        nrq = nrq+1                                                             ! count requests
        mxyz = lx*my*mz
        iz1 = i_z*mz + 1
        iz2 = i_z*mz + mz
        call MPI_Isend (buf3(:,:,iz1:iz2), mxyz, my_real, i_z, tag, &
                        comm, rq(nrq), ierr)                                    ! send to remote
      end do
      iz1 = mpi_z*mz + 1
      iz2 = mpi_z*mz + mz
      do ix=1,nx
        t3(ix1+ix-1,:,:) = buf3(:,ix,iz1:iz2)                                   ! local part
      end do
    end if
    ix1 = ix2 + 1
  end do
  !-----------------------------------------------------------------------------
  ! Assemble results
  !-----------------------------------------------------------------------------
  ix1 = 1
  do rank=0,nrank-1
    ix2 = min(ix1+lx-1,mx)
    nx = ix2-ix1+1
    if (mpi_z /= rank) then                                                     ! remote
      mxyz = lx*my*mz
      call MPI_Recv (rcv, mxyz, my_real, rank, tag, comm, status, ierr)        ! recv from worker
      do ix=1,nx
        t3(ix1+ix-1,:,:) = rcv(:,ix,:)                                          ! data from worker
      end do
    end if
    ix1 = ix2 + 1
  end do
  call waitall_mpi (nrq, rq)                                                    ! satisfy requests
                                               !write (10+mpi_rank) buf3
                                               !write (10+mpi_rank) t3
  if (nrank > 1) call MPI_Barrier (mpi_comm_plane(2), ierr)                     ! for good measure
  !-----------------------------------------------------------------------------
  ! Done
  !-----------------------------------------------------------------------------
  deallocate (f1, w1, snd, rcv, buf3, rq)
  verbose = .false.
END SUBROUTINE trf_zy

!*******************************************************************************
SUBROUTINE test_fft2d

  USE params
  USE mpi_mod
  implicit none
  real, allocatable, dimension(:,:):: f1, f2, ft, f3, k2
  real, allocatable, dimension(:,:,:):: g1, g2, gt, g3
  real kx, kz, f1max, f1lim, g1max, g1lim, f, err
  integer ix, iy, iz, jx, jz, ierr

  allocate (f1(mx,mz), f2(mx,mz), ft(mx,mz), f3(mx,mz), k2(mx,mz))
  allocate (g1(mx,my,mz), g2(mx,my,mz), gt(mx,my,mz), g3(mx,my,mz))

  kx = 2.*pi/sx
  kz = 2.*pi/sz
  jx = (mx*mpi_nx)/2-1
  jz = (mz*mpi_nz)/2-1
  do iz=1,mz
  do ix=1,mx
    f1(ix,iz) = cos(2.*kx*xm(ix))*cos(3.*kz*zm(iz))
    f2(ix,iz) = f1(ix,iz)
    f3(ix,iz) = -((2.*kx)**2+(3.*kz)**2)*cos(2.*kx*xm(ix))*cos(3.*kz*zm(iz))
  end do
  end do
  do iy=1,my
    f = (1.+0.01*iy)
    g1(:,iy,:) = f*f1(:,:)
    g2(:,iy,:) = f*f2(:,:)
    g3(:,iy,:) = f*f3(:,:)
  end do

  call fft2df (f1, ft, mx, mz)
  
  !call MPI_Barrier (mpi_comm_plane(2), ierr)
  !close (mpi_rank+10)
  !call MPI_Barrier (mpi_comm_plane(2), ierr)
  !call MPI_Finalize (ierr)
  !stop

  if (mpi_x==0 .and. mpi_z==0) ft(4,6) = ft(4,6) - 0.25                         ! subtract the expected FT
  err = maxval(abs(ft))
  call max_real_mpi (err)
  if (err > 1e-5) then                                                          ! check the residue
    print '(2x,a,2i4,g14.6)', &
    'test_fft2df  forward ERROR: mpi_x, mpi_z, maxval(abs(ft)) =', &
      mpi_x, mpi_z, maxval(abs(ft))
    !call end_mpi
    !stop
  end if
  call max_real_mpi (err)
  if (master) print *,'test_fft2df : diff =', err

  if (mpi_x==0 .and. mpi_z==0) ft(4,6) = ft(4,6) + 0.25                         ! add the expected FT
  do iy=1,my
    f = (1.+0.01*iy)
    gt(:,iy,:) = f*ft(:,:)
  end do

  call fft2db  (ft, f1, mx, mz)
  f1 = abs(f2-f1)
  err = maxval (f1)
  if (err > 1e-5) then                                                        ! check the inverse result
    print '(i7,2x,a,2i4,2g14.6)', mpi_rank, &
    'test_fft2db  inverse ERROR: mpi_x, mpi_z, maxval(abs(f2-f1)) =', &
      mpi_x, mpi_z, err
    !call end_mpi
    !stop
  end if
  call max_real_mpi (err)
  if (master) print *,'test_fft2db : diff =', err

  call fft2dby (gt, g1, mx, my, mz)
  g1 = abs(g2-g1)
  err = maxval (g1)
  if (err > 1e-4) then                                                        ! check the inverse result
    print '(i7,2x,a,2i4,2g14.6)', mpi_rank, &
    'test_fft2dby inverse ERROR: mpi_x, mpi_z, maxval(abs(g2-g1)) =', &
      mpi_x, mpi_z, err
    !call end_mpi
    !stop
  end if
  call max_real_mpi (err)
  if (master) print *,'test_fft2dby: diff =', err

  call fft2d_k2 (k2, sx, sz, mx, mz)
!  write(80+mpi_x+mpi_z*mpi_nx) mx,mz,mpi_x,mpi_z
!  write(80+mpi_x+mpi_z*mpi_nx) f2
!  write(80+mpi_x+mpi_z*mpi_nx) ft
  do iz=1,mz
    do ix=1,mx
      ft(ix,iz) = -k2(ix,iz)*ft(ix,iz)
    end do
  end do
  do iy=1,my
    f = (1.+0.01*iy)
    gt(:,iy,:) = f*ft(:,:)
  end do
    
!  write(80+mpi_x+mpi_z*mpi_nx) ft
  call fft2db  (ft, f1, mx, mz)
  !call fft2dby (gt, g1, mx, my, mz)
!  write(80+mpi_x+mpi_z*mpi_nx) f1
!  write(80+mpi_x+mpi_z*mpi_nx) f3
!  write(80+mpi_x+mpi_z*mpi_nx) k2

  f1 = abs(f3-f1)
  err = maxval (f1)
  f1lim = 2e-3*(kx*jx)**2
  if (err > f1lim) then                                                       ! check the inverse result
    print '(i7,2x,a,2i4,2g14.6)', mpi_rank, &
    'test_fft2db  Laplace ERROR: mpi_x, mpi_z, maxval(abs(f3-f1)) =', &
      mpi_x, mpi_z, err, f1lim
    !call end_mpi
    !stop
  end if
  call max_real_mpi (err)
  if (master) print *,'test_fft2dL : diff =', err

  call fft2dby  (gt, g1, mx, my, mz)
  g1 = abs(g3-g1)
  err = maxval (g1)
  g1lim = 2e-3*(kx*jx)**2
  if (err > g1lim) then                                                       ! check the inverse result
    print '(i7,2x,a,2i4,2g14.6)', mpi_rank, &
    'test_fft2dby Laplace ERROR: mpi_x, mpi_z, maxval(abs(g3-g1)) =', &
      mpi_x, mpi_z, err, g1lim
    !call end_mpi
    !stop
  end if
  call max_real_mpi (err)
  if (master) print *,'test_fft2dLy: diff =', err

  deallocate (f1, f2, ft, f3, k2)
  deallocate (g1, g2, gt, g3)
END
