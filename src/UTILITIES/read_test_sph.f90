! $Id: read_test_sph.f90,v 1.1 2007/08/29 23:41:05 aake Exp $

!-----------------------------------------------------------------------
PROGRAM read_test_sph
!
! Read grid-based data, written with sequential fortran writes, and
! assign SPH positions and velocities.
!
! Things to change for each case:
!
! 1) The number of particles 'np'
! 2) The definition of smoothing kernel and smoothing length
! 3) The SPH output format
!
! Positions are assigned using a routine that uses a smoothing
! length and and a smoothing kernel.  As supplied, these are the
! ones used by Shu-ishiro Inutsuka, but the user should replace 
! these with the ones being used in his/hers code.
!
! The user is also supposed to add his/hers I/O write routine, and 
! write the SPH data out in whatever preferred format.
!
  implicit none
  integer mx,my,mz,mv,np,npc,ip
  real, allocatable, dimension(:,:,:):: rho,ux,uy,uz,bx,by,bz           ! mesh variables
  real, allocatable, dimension(:):: x,y,z,vx,vy,vz                      ! particle variables
  real average

  print *,'opening test.dat ...'
  open (1,file='test.dat',form='unformatted',status='old')              ! open test.dat file
  read (1) mx,my,mz,mv                                                  ! read dimensions

  print *,'reading test.dat ...'
  allocate (rho(mx,my,mz), ux(mx,my,mz), uy(mx,my,mz), uz(mx,my,mz))    ! allocate mesh variables
  read (1) rho                                                          ! read density
  read (1) ux                                                           ! read velocity
  read (1) uy                                                           ! read velocity
  read (1) uz                                                           ! read velocity

  print *,'average density =',average(mx,my,mz,rho)                     ! sanity test

  npc=10                                                                ! number of particles per cell
  np=mx*my*mz*npc                                                       ! number of particles
  allocate (x(np),y(np),z(np),vx(np),vy(np),vz(np))                     ! allocate particle data

  print *,'simple initial positions ...'
  do ip=1,np                                                            ! place all particles at ..
    x(ip) = mod(ip*123456.,real(mx))                                    ! really simple pseudo-random positions
    y(ip) = mod(ip*132456.,real(mx))
    z(ip) = mod(ip*124356.,real(mx))
  end do

  print *,'relaxing positions ...'
  call relax(mx,my,mz,rho,np,x,y,z)                                     ! relax their positions
 
  print *,'interpolating velocities ..'
  call interpolate(mx,my,mz,ux,uy,uz,np,x,y,z,vx,vy,vz)                 ! interpolate velocities

  print *,'writing SPH data ...'
  call write_sph(np,x,y,z,vx,vy,vz)                                     ! replace with user routine

  print *,'done'
END

!-----------------------------------------------------------------------
SUBROUTINE relax(mx,my,mz,rho,np,x,y,z)
!
! This routine is supposed to place particles in the ranges
! 0 < x < mx, 0 < y < my, 0 < z < mz, in such a way that they
! represent the mass density of a cube of size mx,my,mz, with
! periodic repetition in all three directions.
!
  implicit none
  integer mx,my,mz,np
  real, dimension(mx,my,mz):: rho,ux,uy,uz
  real, dimension(np):: x,y,z,vx,vy,vz

END

!-----------------------------------------------------------------------
SUBROUTINE interpolate(mx,my,mz,ux,uy,uz,np,x,y,z,vx,vy,vz)             ! tri-linear interpolation
  implicit none
  integer mx,my,mz,np,ip,ix,iy,iz,ixp1,iyp1,izp1
  real, dimension(mx,my,mz):: rho,ux,uy,uz
  real, dimension(np):: x,y,z,vx,vy,vz
  real px,py,pz,qx,qy,qz

  do ip=1,np
    px=x(ip); ix=px; px=px-ix; qx=1.-px                                 ! interpolation weights
    py=y(ip); iy=py; py=py-iy; qy=1.-py
    pz=z(ip); iz=pz; pz=pz-iz; qz=1.-pz

    ix=1+mod(ix,mx); ixp1=1+mod(ix+1,mx)                                ! periodic folding
    iy=1+mod(iy,my); iyp1=1+mod(iy+1,my)
    iz=1+mod(iz,mz); izp1=1+mod(iz+1,mz)

    vx(ip) = qz*(qy*(qx*ux(ix,iy  ,iz  )+px*ux(ixp1,iy  ,iz  ))  &
               + py*(qx*ux(ix,iyp1,iz  )+px*ux(ixp1,iyp1,iz  ))) &
           + pz*(qy*(qx*ux(ix,iy  ,izp1)+px*ux(ixp1,iy  ,izp1))  &
               + py*(qx*ux(ix,iyp1,izp1)+px*ux(ixp1,iyp1,izp1)))
    vy(ip) = qz*(qy*(qx*uy(ix,iy  ,iz  )+px*uy(ixp1,iy  ,iz  ))  &
               + py*(qx*uy(ix,iyp1,iz  )+px*uy(ixp1,iyp1,iz  ))) &
           + pz*(qy*(qx*uy(ix,iy  ,izp1)+px*uy(ixp1,iy  ,izp1))  &
               + py*(qx*uy(ix,iyp1,izp1)+px*uy(ixp1,iyp1,izp1)))
    vz(ip) = qz*(qy*(qx*uz(ix,iy  ,iz  )+px*uz(ixp1,iy  ,iz  ))  &
               + py*(qx*uz(ix,iyp1,iz  )+px*uz(ixp1,iyp1,iz  ))) &
           + pz*(qy*(qx*uz(ix,iy  ,izp1)+px*uz(ixp1,iy  ,izp1))  &
               + py*(qx*uz(ix,iyp1,izp1)+px*uz(ixp1,iyp1,izp1)))
  end do
END

!-----------------------------------------------------------------------
REAL FUNCTION average(mx,my,mz,f)                                       ! save average routine
  implicit none
  integer mx,my,mz,ix,iy,iz
  real, dimension(mx,my,mz):: f
  real(kind=8) sumx,sumy,sumz

  sumz=0.                                                               ! sum over volume
  do iz=1,mz
    sumy=0.                                                             ! sum over planes
    do iy=1,my
      sumx=0.                                                           ! sum along lines
      do ix=1,mx
        sumx=sumx+f(ix,iy,iz)
      end do
      sumy=sumy+sumx
    end do
    sumz=sumz+sumy
  end do
  average=sumz/(real(mx)*real(my)*real(mz))                             ! safe above 2G points
END

!-----------------------------------------------------------------------
SUBROUTINE write_sph(np,x,y,z,vx,vy,vz)                                 ! I/O routine template
  implicit none
  integer np
  real, dimension(np):: x,y,z,vx,vy,vz

END
