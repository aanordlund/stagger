! $Id: fft2d_mpi.f90,v 1.28 2014/08/24 11:35:06 aake Exp $
!------------------------------------------------------------------------------
!
!  Two-dimensional Fourier transform routines, based on FFTPACK.
!  FFTPACK is transportable, and allows arbitrary dimensions (speed
!  will vary, of course).
!
!  CALL fft2df (f, ft, mx, my)              ! forward transform
!  CALL fft2db (ft, f, mx, my)              ! backward transform
!  CALL fft2d_k2 (k2, sx, sy, mx, my)       ! compute k2
!
!******************************************************************************
SUBROUTINE fft2d_k2 (k2, sx, sz, mx, mz)
  USE params, only: mpi_x, mpi_z, mpi_nx, mpi_nz, mpi_rank, pi
  implicit none
  real, dimension(mx,mz):: k2
  real sx, sz, kx, kz
  integer i, j, k, mx, mz
!------------------------------------------------------------------------------
  !$omp single
  do j=1,mz
    kz = 2.*pi/sz*((j+mpi_z*mz)/2)
    do i=1,mx
      kx = 2.*pi/sx*((i+mpi_x*mx)/2)
      k2(i,j) = kx**2+kz**2
    end do
  end do
  !$omp end single
END SUBROUTINE

!******************************************************************************
SUBROUTINE fft2df (r, rt, mx, mz)
  USE params, only: mpi_x, mpi_z, mpi_nx, mpi_ny, mpi_nz, mpi_rank, master
  USE mpi_mod
  implicit none

  integer mx, mz
  real, dimension(mx,mz):: r, rt

  integer i, j, k, i_x, i_z, ierr, mxz, mwx, mwz, rank, nx, nz
  integer coordinates(2), status(MPI_STATUS_SIZE)
  real c
  real, allocatable, dimension(:):: wx, wz, fx, fz
  real, allocatable, dimension(:,:):: rr, rrt
!------------------------------------------------------------------------------
  !$omp single

  nx = mx*mpi_nx
  nz = mz*mpi_nz

  mxz = mx*mz                                                                   ! domain size
  mwx= 2*nx + 15                                                                ! size of work array
  allocate (fx(nx))                                                             ! allocate 1D-array
  allocate (wx(mwx))                                                            ! allocate work array
  call srffti (nx,wx)                                                           ! compute work array
  c=1./(nx)                                                                     ! normalization factor

  if (mpi_nx == 1) then                                                         ! if no x MPI-domains
    do j=1,mz                                                                   ! loop over x-strips
      fx=r(:,j)                                                                 ! copy data
      call srfftf (mx, fx, wx)                                                  ! transform
      rt(:,j)=c*fx                                                              ! normalize and copy back
    end do                                                                      ! done
  else
    if (mpi_x == 0) then                                                        ! are we taking on the work?
      allocate (rr(nx,mz), rrt(nx,mz))                                          ! allocate strip buffers

      rr(1:mx,:) = r                                                            ! fill in local part
      do i_x=1,mpi_nx-1                                                         ! for all others in the strip
        coordinates = (/ i_x, mpi_z /)                                          ! coordinates
        call MPI_CART_RANK (cart_mpi(2), coordinates, rank, ierr)               ! get their rank
        call MPI_RECV (rt, mxz, MPI_REAL, rank, i_x, cart_mpi(2), status, ierr) ! receive other domain
        rr(i_x*mx+1:i_x*mx+mx,:) = rt                                           ! fill in
      end do

      !if (mpi_z==0) print '(25f6.3)',rr(:,1)
      do j=1,mz                                                                 ! for all z in domain
        fx=rr(:,j)                                                              ! copy string
        call srfftf (nx, fx, wx)                                                ! transform
        rrt(:,j)=c*fx
      end do
      !if (mpi_z==0) print '(25f6.3)',rrt(1:10,1)

      do i_x=1,mpi_nx-1                                                         ! for all others in the strip
        coordinates = (/ i_x, mpi_z /)                                          ! coordinates
        call MPI_CART_RANK(cart_mpi(2), coordinates, rank, ierr)                ! get their rank
        rt = rrt(i_x*mx+1:i_x*mx+mx,:)                                          ! fill in
        call MPI_SEND (rt, mxz, MPI_REAL, rank, i_x, cart_mpi(2), ierr)
      end do
      rt = rrt(1:mx,:)

      deallocate (rr,rrt)
    else
      coordinates = (/ 0, mpi_z /)                                              ! coordinates
      call MPI_CART_RANK(cart_mpi(2), coordinates, rank, ierr)                  ! where to?
      call MPI_SEND (r , mxz, MPI_REAL, rank, mpi_x, cart_mpi(2), ierr)         ! send domain data
      call MPI_RECV (rt, mxz, MPI_REAL, rank, mpi_x, cart_mpi(2), status, ierr) ! receive results
    end if
  end if
  deallocate (wx, fx)
  !if (mpi_x==0 .and. mpi_z==0) print '(6f6.3)',rt(1:6,1:6)

!...............................................................................
  mxz = mx*mz                                                                   ! domain size
  mwz= 2*nz + 15                                                                ! size of work array
  allocate (fz(nz))                                                             ! allocate 1D-array
  allocate (wz(mwz))                                                            ! allocate work array
  call srffti (nz,wz)                                                           ! compute work array
  c=1./(nz)                                                                     ! normalization factor
  if (mpi_nz == 1) then                                                         ! no MPI along this dir
    do i=1,mx                                                                   ! loop over strips
      fz=rt(i,:)                                                                ! copy data
      call srfftf (mz, fz, wz)                                                  ! transform
      rt(i,:)=c*fz                                                              ! copy back
    end do                                                                      ! done!
  else
    if (mpi_z == 0) then                                                        ! are we taking on the work?
      allocate (rr(mx,nz), rrt(mx,nz))                                          ! allocate strip buffers

      rr(:,1:mz) = rt                                                           ! fill in local part
      do i_z=1,mpi_nz-1                                                         ! for all others in the strip
        coordinates = (/ mpi_x, i_z /)                                          ! coordinates
        call MPI_CART_RANK(cart_mpi(2), coordinates, rank, ierr)                ! get their rank
        call MPI_RECV (rt, mxz, MPI_REAL, rank, i_z, cart_mpi(2), status, ierr) ! receive other domain
        rr(:,i_z*mz+1:i_z*mz+mz) = rt                                           ! fill in
      end do

      !if (mpi_x==0) print '(25f6.3)',rr(4,1:2*mz)
      do j=1,mx                                                                 ! for all z in domain
        fz=rr(j,:)                                                              ! copy string
        call srfftf (nz, fz, wz)                                                ! transform
        rrt(j,:)=c*fz
      end do
      !if (mpi_x==0) print '(25f6.3)',rrt(4,1:10)

      do i_z=1,mpi_nz-1                                                         ! for all others in the strip
        coordinates = (/ mpi_x, i_z /)                                          ! coordinates
        call MPI_CART_RANK(cart_mpi(2), coordinates, rank, ierr)                ! get their rank
        rt = rrt(:,i_z*mz+1:i_z*mz+mz)                                          ! fill in
        call MPI_SEND (rt, mxz, MPI_REAL, rank, i_z, cart_mpi(2), ierr)
      end do
      rt = rrt(:,1:mz)

      deallocate (rr,rrt)
    else
      coordinates = (/ mpi_x, 0 /)                                              ! coordinates
      call MPI_CART_RANK(cart_mpi(2), coordinates, rank, ierr)                  ! where to?
      call MPI_SEND (rt, mxz, MPI_REAL, rank, mpi_z, cart_mpi(2), ierr)         ! send domain data
      call MPI_RECV (rt, mxz, MPI_REAL, rank, mpi_z, cart_mpi(2), status, ierr) ! receive results
    end if
  end if
  deallocate (wz, fz)

  !$omp end single
END SUBROUTINE

!******************************************************************************
SUBROUTINE fft2db (r, rt, mx, mz)
  USE params, only: mpi_x, mpi_z, mpi_nx, mpi_ny, mpi_nz, mpi_rank, master
  USE mpi_mod
  implicit none

  integer mx, mz
  real, dimension(mx,mz):: r, rt

  integer i, j, k, i_x, i_z, ierr, mxz, mwx, mwz, rank, tag, nx, nz
  integer coordinates(2), status(MPI_STATUS_SIZE)
  real, allocatable, dimension(:):: wx, wz, fx, fz
  real, allocatable, dimension(:,:):: rr, rrt, buf
!------------------------------------------------------------------------------
  !$omp single

  nx = mx*mpi_nx
  nz = mz*mpi_nz

!...............................................................................
  mxz = mx*mz                                                                   ! domain size
  mwz= 2*nz + 15                                                                ! size of work array
  allocate (fz(nz))                                                             ! allocate 1D-array
  allocate (wz(mwz))                                                            ! allocate work array
  call srffti (nz,wz)                                                           ! compute work array
  if (mpi_nz == 1) then                                                         ! no MPI along this dir
    do i=1,mx                                                                   ! loop over strips
      fz=r(i,:)                                                                 ! copy data
      call srfftb (mz, fz, wz)                                                  ! transform
      rt(i,:)=fz                                                                ! copy back
    end do                                                                      ! done!
  else
    if (mpi_z == 0) then                                                        ! are we taking on the work?
      allocate (rr(mx,nz), rrt(mx,nz), buf(mx,mz))                              ! allocate strip buffers

      rr(:,1:mz) = r                                                            ! fill in local part
      do i_z=1,mpi_nz-1                                                         ! for all others in the strip
        coordinates = (/ mpi_x, i_z /)                                          ! coordinates
        call MPI_CART_RANK(cart_mpi(2), coordinates, rank, ierr)                ! get their rank
        tag = i_z                                                               ! encode sender
        call MPI_RECV (buf, mxz, MPI_REAL, rank, tag, cart_mpi(2), status, ierr)! receive other domain
        rr(:,i_z*mz+1:i_z*mz+mz) = buf                                          ! fill in
      end do

      do j=1,mx                                                                 ! for all z in domain
        fz=rr(j,:)                                                              ! copy string
        call srfftb (nz, fz, wz)                                                ! transform
        rrt(j,:)=fz
      end do

      do i_z=1,mpi_nz-1                                                         ! for all others in the strip
        coordinates = (/ mpi_x, i_z /)                                          ! coordinates
        call MPI_CART_RANK(cart_mpi(2), coordinates, rank, ierr)                ! get their rank
        buf = rrt(:,i_z*mz+1:i_z*mz+mz)                                         ! fill in
        tag = i_z                                                               ! encode receiver
        call MPI_SEND (buf, mxz, MPI_REAL, rank, tag, cart_mpi(2), ierr)
      end do
      rt = rrt(:,1:mz)

      deallocate (rr,rrt,buf)
    else
      coordinates = (/ mpi_x, 0 /)                                              ! coordinates
      call MPI_CART_RANK(cart_mpi(2), coordinates, rank, ierr)                  ! where to?
      tag = mpi_z                                                               ! reverse encoding
      call MPI_SEND (r , mxz, MPI_REAL, rank, tag, cart_mpi(2), ierr)           ! send domain data
      call MPI_RECV (rt, mxz, MPI_REAL, rank, tag, cart_mpi(2), status, ierr)   ! receive results
    end if
  end if
  deallocate (wz, fz)

!...............................................................................
  mxz = mx*mz                                                                   ! domain size
  mwx= 2*nx + 15                                                                ! size of work array
  allocate (fx(nx))                                                             ! allocate 1D-array
  allocate (wx(mwx))                                                            ! allocate work array
  call srffti (nx,wx)                                                           ! compute work array

  if (mpi_nx == 1) then                                                         ! if no x MPI-domains
    do j=1,mz                                                                   ! loop over x-strips
      fx=rt(:,j)                                                                ! copy data
      call srfftb (mx, fx, wx)                                                  ! transform
      rt(:,j)=fx                                                                ! normalize and copy back
    end do                                                                      ! done
  else
    if (mpi_x == 0) then                                                        ! are we taking on the work?
      allocate (rr(nx,mz), rrt(nx,mz), buf(mx,mz))                              ! allocate strip buffers

      rr(1:mx,:) = rt                                                           ! fill in local part
      do i_x=1,mpi_nx-1                                                         ! for all others in the strip
        coordinates = (/ i_x, mpi_z /)                                          ! coordinates
        call MPI_CART_RANK (cart_mpi(2), coordinates, rank, ierr)               ! get their rank
        tag = mpi_x+i_x*mpi_nx                                                  ! encode receivet and sender
        call MPI_RECV (buf, mxz, MPI_REAL, rank, tag, cart_mpi(2), status, ierr)! receive other domain
        rr(i_x*mx+1:i_x*mx+mx,:) = buf                                          ! fill in
      end do

      do j=1,mz                                                                 ! for all z in domain
        fx=rr(:,j)                                                              ! copy string
        call srfftb (nx, fx, wx)                                                ! transform
        rrt(:,j)=fx
      end do

      do i_x=1,mpi_nx-1                                                         ! for all others in the strip
        coordinates = (/ i_x, mpi_z /)                                          ! coordinates
        call MPI_CART_RANK(cart_mpi(2), coordinates, rank, ierr)                ! get their rank
        buf = rrt(i_x*mx+1:i_x*mx+mx,:)                                         ! fill in
        tag = mpi_x+i_x*mpi_nx                                                  ! encode receivet and sender
        call MPI_SEND (buf, mxz, MPI_REAL, rank, tag, cart_mpi(2), ierr)
      end do
      rt = rrt(1:mx,:)

      deallocate (rr,rrt,buf)
    else
      coordinates = (/ 0, mpi_z /)                                              ! coordinates
      call MPI_CART_RANK(cart_mpi(2), coordinates, rank, ierr)                  ! where to?
      tag = mpi_x*mpi_nx                                                        ! reverse encoding
      call MPI_SEND (rt, mxz, MPI_REAL, rank, tag, cart_mpi(2), ierr)           ! send domain data
      call MPI_RECV (rt, mxz, MPI_REAL, rank, tag, cart_mpi(2), status, ierr)   ! receive results
    end if
  end if
  deallocate (wx, fx)

  !$omp end single
END SUBROUTINE

!******************************************************************************
SUBROUTINE fft2dby (r, rt, mx, my, mz)
!
! Use parallel ranks, from the transform direction, to work on several 
! planes in parallel.  For each plane the pieces in the transform direction
! are collected onto the work rank, while other ranks just send and receive.
!
  USE params, only: mpi_x, mpi_z, mpi_nx, mpi_ny, mpi_nz, mpi_rank, master
  USE mpi_mod
  implicit none

  integer mx, my, mz
  real, dimension(mx,my,mz):: r, rt

  integer i, iy, j, k, i_x, i_z, ierr, mxz, mwx, mwz, rank, tag, nx, nz
  integer fft_x, fft_z
  integer coordinates(2), status(MPI_STATUS_SIZE)
  real, allocatable, dimension(:):: wx, wz, fx, fz
  real, allocatable, dimension(:,:):: rr, rrt, buf
!------------------------------------------------------------------------------
  !$omp single

  nx = mx*mpi_nx
  nz = mz*mpi_nz

!...............................................................................
  mxz = mx*mz                                                                   ! buffer size
  mwz= 2*nz + 15                                                                ! size of work array
  allocate (fz(nz))                                                             ! allocate 1D-array
  allocate (wz(mwz))                                                            ! allocate work array
  call srffti (nz,wz)                                                           ! compute work array

!-------------------------------------------------------------------------------
! No extra ranks available in the transform direction; just loop
!-------------------------------------------------------------------------------
  if (mpi_nz == 1) then                                                         ! no MPI along this dir
    do iy=1,my                                                                  ! loop over strips
    do i=1,mx                                                                   ! loop over strips
      fz=r(i,iy,:)                                                              ! copy data
      call srfftb (mz, fz, wz)                                                  ! transform
      rt(i,iy,:)=fz                                                             ! copy back
    end do                                                                      ! done!
    end do                                                                      ! done!
!-------------------------------------------------------------------------------
! Extra ranks available in the transform direction, so share the work. For each
! value of iy, the communications only need to work in the plane perpendicular
! to y, so can use a y-plane communicator, which is independent of other y-plane
! groups. In the 4032x4032 case we have about 100 ranks in the transfrom
! direction for each plane, so no lack of cores.  Need to make sure they do not
! get into a locked situation.
!
! Note that all ranks in this communicator is involved in obtaining the solution
! for each y-plane, either as worker, or else only sending and receiving. Hence,
! when the work is done, all values are updated on the appropriate ranks that
! are the owners of the data.
!-------------------------------------------------------------------------------
  else
   allocate (buf(mx,mz))
   do iy=1,my
    fft_z = mod(iy-1,mpi_nz)
    tag = 1
    if (mpi_z == fft_z) then                                                    ! are we taking on the work?
      allocate (rr(mx,nz), rrt(mx,nz))                                          ! allocate buffers

      do i_z=0,mpi_nz-1                                                         ! for all others in the strip
        if (i_z == fft_z) then
          rr(:,i_z*mz+1:i_z*mz+mz) = r(:,iy,:)
        else
          coordinates = (/ mpi_x, i_z /)                                          ! coordinates
          call MPI_CART_RANK(cart_mpi(2), coordinates, rank, ierr)                ! get their rank
          call MPI_RECV (buf, mxz, MPI_REAL, rank, tag, cart_mpi(2), status, ierr)! receive other domain
          rr(:,i_z*mz+1:i_z*mz+mz) = buf                                          ! fill in
        end if
      end do

      do j=1,mx                                                                 ! for all z in domain
        fz=rr(j,:)                                                              ! copy string
        call srfftb (nz, fz, wz)                                                ! transform
        rrt(j,:)=fz
      end do

      do i_z=0,mpi_nz-1                                                         ! for all others in the strip
        if (i_z == fft_z) then
          rt(:,iy,:) = rrt(:,i_z*mz+1:i_z*mz+mz)
        else
          coordinates = (/ mpi_x, i_z /)                                          ! coordinates
          call MPI_CART_RANK(cart_mpi(2), coordinates, rank, ierr)                ! get their rank
          buf = rrt(:,i_z*mz+1:i_z*mz+mz)                                         ! fill in
          call MPI_SEND (buf, mxz, MPI_REAL, rank, tag, cart_mpi(2), ierr)
        end if
      end do

      deallocate (rr,rrt)
    else
      coordinates = (/ mpi_x, fft_z /)                                          ! coordinates
      call MPI_CART_RANK(cart_mpi(2), coordinates, rank, ierr)                  ! where to?
      buf = r(:,iy,:)
      call MPI_SEND (buf, mxz, MPI_REAL, rank, tag, cart_mpi(2), ierr)          ! send domain data
      call MPI_RECV (buf, mxz, MPI_REAL, rank, tag, cart_mpi(2), status, ierr)  ! receive results
      rt(:,iy,:) = buf
    end if
   end do ! iy
   deallocate (buf)
   call MPI_Barrier (cart_mpi(2), ierr)
  end if
  deallocate (wz, fz)


!...............................................................................
  mxz = mx*mz                                                                   ! domain size
  mwx= 2*nx + 15                                                                ! size of work array
  allocate (fx(nx))                                                             ! allocate 1D-array
  allocate (wx(mwx))                                                            ! allocate work array
  call srffti (nx,wx)                                                           ! compute work array

  if (mpi_nx == 1) then                                                         ! if no x MPI-domains
    do iy=1,my
    do j=1,mz                                                                   ! loop over x-strips
      fx=rt(:,iy,j)                                                             ! copy data
      call srfftb (mx, fx, wx)                                                  ! transform
      rt(:,iy,j)=fx                                                             ! normalize and copy back
    end do                                                                      ! done
    end do                                                                      ! done
  else
   allocate (buf(mx,mz))                                                        ! allocate strip buffers
   do iy=1,my
    fft_x = mod(iy-1,mpi_nx)
    tag = 2
    if (mpi_x == fft_x) then                                                    ! are we taking on the work?
      allocate (rr(nx,mz), rrt(nx,mz))                                          ! allocate strip buffers

      do i_x=0,mpi_nx-1                                                         ! for all others in the strip
        if (i_x == fft_x) then
          rr(i_x*mx+1:i_x*mx+mx,:) = rt(:,iy,:)
        else
          coordinates = (/ i_x, mpi_z /)                                        ! coordinates
          call MPI_CART_RANK (cart_mpi(2), coordinates, rank, ierr)             ! get their rank
          call MPI_RECV (buf, mxz, MPI_REAL, rank, tag, cart_mpi(2), status, ierr)! receive other domain
          rr(i_x*mx+1:i_x*mx+mx,:) = buf                                        ! fill in
        end if
      end do

      do j=1,mz                                                                 ! for all z in domain
        fx=rr(:,j)                                                              ! copy string
        call srfftb (nx, fx, wx)                                                ! transform
        rrt(:,j)=fx
      end do

      do i_x=0,mpi_nx-1                                                         ! for all others in the strip
        if (i_x == fft_x) then
          rt(:,iy,:) = rrt(i_x*mx+1:i_x*mx+mx,:) 
        else
          coordinates = (/ i_x, mpi_z /)                                          ! coordinates
          call MPI_CART_RANK(cart_mpi(2), coordinates, rank, ierr)                    ! get their rank
          buf = rrt(i_x*mx+1:i_x*mx+mx,:)                                         ! fill in
          call MPI_SEND (buf, mxz, MPI_REAL, rank, tag, cart_mpi(2), ierr)
        end if
      end do

      deallocate (rr,rrt)
    else
      coordinates = (/ fft_x, mpi_z /)                                          ! coordinates
      call MPI_CART_RANK(cart_mpi(2), coordinates, rank, ierr)                  ! where to?
      buf = rt(:,iy,:)
      call MPI_SEND (buf, mxz, MPI_REAL, rank, tag, cart_mpi(2), ierr)          ! send domain data
      call MPI_RECV (buf, mxz, MPI_REAL, rank, tag, cart_mpi(2), status, ierr)  ! receive results
      rt(:,iy,:) = buf
    end if
   end do
   deallocate (buf)
  end if
  deallocate (wx, fx)

  !$omp end single
END SUBROUTINE

!******************************************************************************
SUBROUTINE test_fft2d

  USE params
  USE mpi_mod
  implicit none
  real, allocatable, dimension(:,:):: f1, f2, ft, f3, k2
  real, allocatable, dimension(:,:,:):: g1, g2, gt, g3
  real kx, kz, f1max, f1lim, g1max, g1lim, f
  integer ix, iy, iz, jx, jz

  allocate (f1(mx,mz), f2(mx,mz), ft(mx,mz), f3(mx,mz), k2(mx,mz))
  allocate (g1(mx,my,mz), g2(mx,my,mz), gt(mx,my,mz), g3(mx,my,mz))

  kx = 2.*pi/sx
  kz = 2.*pi/sx
  jx = (mx*mpi_nx)/2-1
  jz = (mz*mpi_nz)/2-1
  if (master) print*,'test_fft2d: jx,jz=',jx,jz
  do iz=1,mz
  do ix=1,mx
    f1(ix,iz) = cos(2.*kx*xm(ix))*cos(3.*kz*zm(iz))
    f2(ix,iz) = f1(ix,iz)
    f3(ix,iz) = -((2.*kx)**2+(3.*kz)**2)*cos(2.*kx*xm(ix))*cos(3.*kz*zm(iz))
  end do
  end do
  do iy=1,my
    f = (1.+0.01*iy)
    g1(:,iy,:) = f*f1(:,:)
    g2(:,iy,:) = f*f2(:,:)
    g3(:,iy,:) = f*f3(:,:)
  end do

  call fft2df (f1, ft, mx, mz)

  if (mpi_x==0 .and. mpi_z==0) ft(4,6) = ft(4,6) - 0.25                         ! subtract the expected FT
  if (maxval(abs(ft)) > 1e-5) then                                              ! check the residue
    print '(2x,a,2i4,g14.6)', &
    'test_fft2df  inverse ERROR: mpi_x, mpi_z, maxval(abs(ft)) =', &
      mpi_x, mpi_z, maxval(abs(ft))
    !call end_mpi
    !stop
  end if
  if (mpi_x==0 .and. mpi_z==0) ft(4,6) = ft(4,6) + 0.25                         ! add the expected FT

  do iy=1,my
    f = (1.+0.01*iy)
    gt(:,iy,:) = f*ft(:,:)
  end do

  call fft2db  (ft, f1, mx, mz)
  call fft2dby (gt, g1, mx, my, mz)

  f1 = abs(f2-f1)
  f1max = maxval (f1)
  if (f1max > 1e-5) then                                                        ! check the inverse result
    print '(i7,2x,a,2i4,2g14.6)', mpi_rank, &
    'test_fft2db  inverse ERROR: mpi_x, mpi_z, maxval(abs(f2-f1)) =', &
      mpi_x, mpi_z, f1max
    !call end_mpi
    !stop
  end if
  if (master) print *,'test_fft2db : diff =', f1max

  g1 = abs(g2-g1)
  g1max = maxval (g1)
  if (g1max > 1e-5) then                                                        ! check the inverse result
    print '(i7,2x,a,2i4,2g14.6)', mpi_rank, &
    'test_fft2dby inverse ERROR: mpi_x, mpi_z, maxval(abs(g2-g1)) =', &
      mpi_x, mpi_z, g1max
    !call end_mpi
    !stop
  end if
  if (master) print *,'test_fft2dby: diff =', g1max

  call fft2d_k2 (k2, sx, sz, mx, mz)
!  write(80+mpi_x+mpi_z*mpi_nx) mx,mz,mpi_x,mpi_z
!  write(80+mpi_x+mpi_z*mpi_nx) f2
!  write(80+mpi_x+mpi_z*mpi_nx) ft
  do iz=1,mz
    do ix=1,mx
      ft(ix,iz) = -k2(ix,iz)*ft(ix,iz)
    end do
  end do
  do iy=1,my
    f = (1.+0.01*iy)
    gt(:,iy,:) = f*ft(:,:)
  end do
    
!  write(80+mpi_x+mpi_z*mpi_nx) ft
  call fft2db  (ft, f1, mx, mz)
  call fft2dby (gt, g1, mx, my, mz)
!  write(80+mpi_x+mpi_z*mpi_nx) f1
!  write(80+mpi_x+mpi_z*mpi_nx) f3
!  write(80+mpi_x+mpi_z*mpi_nx) k2

  f1 = abs(f3-f1)
  f1max = maxval (f1)
  f1lim = 2e-3*(kx*jx)**2
  if (f1max > f1lim) then                                                       ! check the inverse result
    print '(i7,2x,a,2i4,2g14.6)', mpi_rank, &
    'test_fft2db  Laplace ERROR: mpi_x, mpi_z, maxval(abs(f3-f1)) =', &
      mpi_x, mpi_z, f1max, f1lim
    !call end_mpi
    !stop
  end if
  if (master) print *,'test_fft2dL : diff =', f1max

  g1 = abs(g3-g1)
  g1max = maxval (g1)
  g1lim = 2e-3*(kx*jx)**2
  if (g1max > g1lim) then                                                       ! check the inverse result
    print '(i7,2x,a,2i4,2g14.6)', mpi_rank, &
    'test_fft2dby Laplace ERROR: mpi_x, mpi_z, maxval(abs(g3-g1)) =', &
      mpi_x, mpi_z, g1max, g1lim
    !call end_mpi
    !stop
  end if
  if (master) print *,'test_fft2dLy: diff =', g1max

  deallocate (f1, f2, ft, f3, k2)
  deallocate (g1, g2, gt, g3)
END
