! $Id: main_power3d.f90,v 1.10 2016/11/09 22:04:58 aake Exp $
!-----------------------------------------------------------------------
PROGRAM main_power3d
  implicit none
  integer ivar, jvar, mvar, mk, ix, iy, iz, lrec, isnap, isnap1, isnap2
  integer mx, my, mz, izs, ize, io_word
  real, allocatable, dimension(:,:,:) :: fx, fy, fz, tx, ty, tz, lnrho, scr
  real, allocatable, dimension(:) :: pkxt, pkyt, pkzt, pkxs, pkys, pkzs, nk, xk
  real ptot(6), sump, sumx, sumy, sumz
  logical pervolume, verbose
  integer omp_get_thread_num, omp_get_num_threads
  character*64:: infile, outfile
  namelist /io/mx,my,mz,infile,ivar,isnap1,isnap2,mvar,mk,outfile,pervolume,verbose

  isnap1 = 0
  isnap2 = 0
  pervolume = .true.
  infile = 'scratch.dat'
  mx = 1000
  my = 1000
  mz = 1000
  ivar = 1
  mk = mx/2
  outfile = 'power3d.dat'
  pervolume = .false.
  verbose = .false.

  write (*,io)
  read (*,io)
  if (outfile.eq.' ') outfile=infile
  write (*,io)

  if (verbose) print *,'allocating',mx*my*mz*4e-9*6,' GB'
  allocate (fx(mx,my,mz), fy(mx,my,mz), fz(mx,my,mz))
  allocate (tx(mx,my,mz), ty(mx,my,mz), tz(mx,my,mz))
  allocate (pkxt(mk), pkyt(mk), pkzt(mk), pkxs(mk), pkys(mk), pkzs(mk), nk(mk), xk(mk))
  if (verbose) print *,'allocation OK'
  
  do isnap=isnap1,isnap2
    lrec = mx*my*io_word()
    open (1, file=infile, form='unformatted', &
      access='direct', recl=lrec)
    do iz=1,mz; read (1,rec=iz+(0+ivar+isnap*mvar)*mz) fx(:,:,iz) ; end do
    if (verbose) print *, minval(fx), maxval(fx)
    do iz=1,mz; read (1,rec=iz+(1+ivar+isnap*mvar)*mz) fy(:,:,iz) ; end do
    if (verbose) print *, minval(fy), maxval(fy)
    do iz=1,mz; read (1,rec=iz+(2+ivar+isnap*mvar)*mz) fz(:,:,iz) ; end do
    if (verbose) print *, minval(fz), maxval(fz)

    if (pervolume) then
      allocate (lnrho(mx,my,mz), scr(mx,my,mz))
      do iz=1,mz; read (1,rec=iz+(0+isnap*mvar)*mz) lnrho(:,:,iz) ; end do
      if (verbose) print *, minval(lnrho), maxval(lnrho)

      !$omp parallel private(iz,izs,ize)
      call limits_omp(1,mz,izs,ize)
      !if (verbose) print *,omp_get_thread_num(),izs,ize
      do iz=izs,ize; lnrho(:,:,iz)=alog(lnrho(:,:,iz)); end do

      call xdn_set (lnrho, scr, mx, my, mz)
      do iz=izs,ize
        scr(:,:,iz) = fx(:,:,iz)/exp(scr(:,:,iz))
      end do
      call xup_set (scr, fx, mx, my, mz)

      call ydn_set (lnrho, scr, mx, my, mz)
      do iz=izs,ize
        scr(:,:,iz) = fy(:,:,iz)/exp(scr(:,:,iz))
      end do
      call yup_set (scr, fy, mx, my, mz)

      call zdn_set (lnrho, scr, mx, my, mz)
      do iz=izs,ize
        scr(:,:,iz) = fz(:,:,iz)/exp(scr(:,:,iz))
      end do
      call zup_set (scr, fz)
      !$omp end parallel

      deallocate (lnrho, scr)
    end if
    close (1)

    !$omp parallel
    !if (verbose .and. omp_get_thread_num() == 0) print *,'FFT'
    call fft3df (fx, tx, mx, my, mz)
    call fft3df (fy, ty, mx, my, mz)
    call fft3df (fz, tz, mx, my, mz)

    !if (verbose .and. omp_get_thread_num() == 0) print *,'power3d'
    call power3d (tx, mx, my, mz, pkxt, xk, nk, mk, ptot(1))
    call power3d (ty, mx, my, mz, pkyt, xk, nk, mk, ptot(2))
    call power3d (tz, mx, my, mz, pkzt, xk, nk, mk, ptot(3))

    !if (verbose .and. omp_get_thread_num() == 0) print *,'helmh'
    call helmht (tx, ty, tz, mx, my, mz)

    !if (verbose .and. omp_get_thread_num() == 0) print *,'power3d'
    call power3d (tx, mx, my, mz, pkxs, xk, nk, mk, ptot(4))
    call power3d (ty, mx, my, mz, pkys, xk, nk, mk, ptot(5))
    call power3d (tz, mx, my, mz, pkzs, xk, nk, mk, ptot(6))

    !call fft3db (tx, fx, mx, my, mz)
    !call fft3db (ty, fy, mx, my, mz)
    !call fft3db (tz, fz, mx, my, mz)
    !$omp end parallel

    if (verbose) print *,'write'
    lrec = (1+6+7*mk)*io_word()
    open (2, file=outfile, form='unformatted', &
      access='direct', recl=lrec, status='unknown')
    write (2,rec=1+isnap) mk,ptot,xk,pkxt,pkyt,pkzt,pkxs,pkys,pkzs
    close (2)

    sump = 0.
    !$omp parallel private(ix,iy,iz,sumx,sumy,sumz,izs,ize)
    sumz = 0.
    call limits_omp(1,mz,izs,ize)
    do iz=izs,ize
      sumy = 0.
      do iy=1,my
        sumx = 0.
        do ix=1,mx
          sumx = sumx + fx(ix,iy,iz)**2 + fy(ix,iy,iz)**2 + fz(ix,iy,iz)**2
        end do
        sumy = sumy + sumx/mx
      end do
      sumz = sumz + sumy/my
    end do
    !$omp critical
    sump = sump + sumz/mz
    !$omp end critical
    !$omp end parallel

    print *, isnap, sqrt(sum(ptot(1:3))), sqrt(sump), 1.-sum(ptot(4:6))/sum(ptot(1:3))
  end do

  deallocate (tx, ty, tz)
END
