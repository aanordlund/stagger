! $Id: fft2d_2_mpi.f90,v 1.2 2014/08/24 13:46:33 aake Exp $
!------------------------------------------------------------------------------
!
!  Two-dimensional Fourier transform routines, based on FFTPACK.
!  FFTPACK is transportable, and allows arbitrary dimensions (speed
!  will vary, of course).
!
!  CALL fft2df (f, ft, mx, my)              ! forward transform
!  CALL fft2db (ft, f, mx, my)              ! backward transform
!  CALL fft2d_k2 (k2, sx, sy, mx, my)       ! compute k2
!
!*******************************************************************************
SUBROUTINE fft2d_k2 (k2, sx, sz, mx, mz)
  USE params, only: mpi_x, mpi_z, mpi_nx, mpi_nz, mpi_rank, pi
  implicit none
  real, dimension(mx,mz):: k2
  real sx, sz, kx, kz
  integer ix, iz, mx, mz
!------------------------------------------------------------------------------
  !$omp single
  do iz=1,mz
    kz = 2.*pi/sz*((iz+mpi_z*mz)/2)
    do ix=1,mx
      kx = 2.*pi/sx*((ix+mpi_x*mx)/2)
      k2(ix,iz) = kx**2+kz**2
    end do
  end do
  !$omp end single
END SUBROUTINE

!*******************************************************************************
SUBROUTINE fft2df (r, rt, mx, mz)
  USE params, only: mpi_nx, mpi_nz, mid
  implicit none
  integer mx, mz, nx, nz
  real, dimension(mx,mz):: r, rt
  external srfftf
  character(len=mid),save:: id='$Id: fft2d_2_mpi.f90,v 1.2 2014/08/24 13:46:33 aake Exp $'
  call print_id(id)
  !$omp single
  nx = mx*mpi_nx
  nz = mz*mpi_nz
  call trf_x (srfftf,  r, rt, mx, mz, 1.0/nx)
  call trf_z (srfftf, rt, rt, mx, mz, 1.0/nz)
  !$omp end single
END SUBROUTINE fft2df

!*******************************************************************************
SUBROUTINE fft2db (r, rt, mx, mz)
  implicit none
  integer mx, mz
  real, dimension(mx,mz):: r, rt
  external srfftb
  !$omp single
  call trf_z (srfftb,  r, rt, mx, mz, 1.0)
  call trf_x (srfftb, rt, rt, mx, mz, 1.0)
  !$omp end single
END SUBROUTINE fft2db

!******************************************************************************
SUBROUTINE trf_x (fft, r, rt, mx, mz, c)
  USE params, only: mpi_x, mpi_y, mpi_z, mpi_nx, mpi_ny, mpi_nz, mpi_rank, mpi_size, master
  USE mpi_mod
  implicit none
  integer mx, mz
  real, dimension(mx,mz):: r, rt
  integer ix, jx, iz, jz, i_x, i_z, mxz, nw, rank, mhd_rank, fft_rank, nx, nz, fft_x
  integer req1, req2, tag, ierr, myrank
  integer here(2), there(2), status(MPI_STATUS_SIZE)
  real c
  real, allocatable, dimension(:,:)::  rr, rrt, wk
  real, allocatable, dimension(:)::  w, fx, snd, rcv
  external fft
!-------------------------------------------------------------------------------
  nx = mx*mpi_nx
  nz = mz*mpi_nz
  nw = 2*nx + 15                                                                ! size of work array
  mxz = mx*mz
  allocate (fx(nx))                                                             ! allocate 1D-array
  allocate (w(nw))                                                              ! allocate work array
  call srffti (nx,w)                                                            ! compute work array

  if (mpi_nx == 1) then                                                         ! if no x MPI-domains
    do iz=1,mz                                                                  ! loop over x-strips
      fx=r(:,iz)                                                                ! copy data
      call fft (nx, fx, w)                                                      ! transform
      rt(:,iz)=c*fx                                                             ! normalize and copy back
    end do                                                                      ! done

  !-----------------------------------------------------------------------------
  ! Not enough x-ranks for one per z-index; handle as in rev 1.15, using one
  ! rank per (nx,mz)-slice to do the FFTs
  !-----------------------------------------------------------------------------
  else
    call MPI_COMM_RANK (cart_mpi(3), myrank, mpi_err)                           ! my rank in z-plane
    CALL MPI_CART_COORDS (cart_mpi(3), myrank, 2, here, mpi_err)                ! my coordinates in z-plane
    if (here(1) == 0) then                                                      ! are we taking on the work?
      allocate (rr(nx,mz), rrt(nx,mz))                                          ! allocate strip buffers

      rr(1:mx,:) = r                                                            ! fill in local part
      do i_x=1,mpi_nx-1                                                         ! for all others in the strip
        there = (/ i_x, here(2) /)                                              ! coordinates
        call MPI_CART_RANK (cart_mpi(3), there, rank, ierr)                     ! get their rank
        call MPI_RECV (rt, mxz, MPI_REAL, rank, 1, cart_mpi(3), status, ierr)   ! receive other domain
        rr(i_x*mx+1:i_x*mx+mx,:) = rt                                           ! fill in
      end do

      do iz=1,mz                                                                ! for all z in domain
        fx=rr(:,iz)                                                             ! copy string
        call fft (nx, fx, w)                                                    ! transform
        rrt(:,iz)=c*fx
      end do

      do i_x=1,mpi_nx-1                                                         ! for all others in the strip
        there = (/ i_x, here(2) /)                                              ! coordinates
        call MPI_CART_RANK(cart_mpi(3), there, rank, ierr)                      ! get their rank
        rt = rrt(i_x*mx+1:i_x*mx+mx,:)                                          ! fill in
        call MPI_SEND (rt, mxz, MPI_REAL, rank, 1, cart_mpi(3), ierr)
      end do
      rt = rrt(1:mx,:)

      deallocate (rr,rrt)
    else
      there = (/ 0, here(2) /)                                                  ! coordinates
      call MPI_CART_RANK(cart_mpi(3), there, rank, ierr)                        ! where to?
      call MPI_SEND (r , mxz, MPI_REAL, rank, 1, cart_mpi(3), ierr)             ! send domain data
      call MPI_RECV (rt, mxz, MPI_REAL, rank, 1, cart_mpi(3), status, ierr)     ! receive results
    end if

    call MPI_Barrier (mpi_comm_world, ierr)
  end if
  deallocate (w, fx)
END SUBROUTINE trf_x

!*******************************************************************************
SUBROUTINE trf_z (fft, r, rt, mx, mz, c)
  USE params, only: mpi_x, mpi_y, mpi_z, mpi_nx, mpi_ny, mpi_nz, mpi_rank, mpi_size, master
  USE mpi_mod
  implicit none
  integer mx, mz
  real, dimension(mx,mz):: r, rt
  integer ix, jx, iz, jz, i_x, i_z, mxz, nw, rank, mhd_rank, fft_rank, nx, nz, fft_z
  integer req1, req2, tag, ierr, myrank
  integer here(2), there(2), status(MPI_STATUS_SIZE)
  real c
  real, allocatable, dimension(:,:)::  rr, rrt
  real, allocatable, dimension(:)::  w, fz, snd, rcv
  external fft
!-------------------------------------------------------------------------------
  nx = mx*mpi_nx
  nz = mz*mpi_nz
  nw = 2*nz + 15                                                                ! size of work array
  mxz = mx*mz
  allocate (fz(nz))                                                             ! allocate 1D-array
  allocate (w(nw))                                                              ! allocate work array
  call srffti (nz,w)                                                            ! compute work array
  if (mpi_nz == 1) then                                                         ! no MPI along this dir
    do ix=1,mx                                                                  ! loop over strips
      fz=r(ix,:)                                                                ! copy data
      call fft (mz, fz, w)                                                      ! transform
      rt(ix,:)=c*fz                                                             ! copy back
    end do                                                                      ! done!

  !-----------------------------------------------------------------------------
  ! Not enough z-ranks for one per x-index; handle as in rev 1.15, using one
  ! rank per (mx,nz)-slice to do the FFTs
  !-----------------------------------------------------------------------------
  else
    call MPI_COMM_RANK (cart_mpi(1), myrank, mpi_err)                           ! my rank in x-plane
    CALL MPI_CART_COORDS (cart_mpi(1), myrank, 2, here, mpi_err)                ! my coordinates in x-plane
    if (here(2) == 0) then                                                      ! are we taking on the work?
      allocate (rr(mx,nz), rrt(mx,nz))                                          ! allocate strip buffers

      rr(:,1:mz) = r                                                            ! fill in local part
      do i_z=1,mpi_nz-1                                                         ! for all others in the strip
        there = (/ here(1), i_z /)                                              ! coordinates
        call MPI_CART_RANK (cart_mpi(1), there, rank, ierr)                     ! get their rank
        call MPI_RECV (rt, mxz, MPI_REAL, rank, 2, cart_mpi(1), status, ierr)   ! receive other domain
        rr(:,i_z*mz+1:i_z*mz+mz) = rt                                           ! fill in
      end do

      do ix=1,mx                                                                ! for all x in domain
        fz=rr(ix,:)                                                             ! copy string
        call fft (nz, fz, w)                                                    ! transform
        rrt(ix,:)=c*fz                                                          ! scale
      end do

      do i_z=1,mpi_nz-1                                                         ! for all others in the strip
        there = (/ here(1), i_z /)                                              ! coordinates
        call MPI_CART_RANK(cart_mpi(1), there, rank, ierr)                      ! get their rank
        rt = rrt(:,i_z*mz+1:i_z*mz+mz)                                          ! fill in
        call MPI_SEND (rt, mxz, MPI_REAL, rank, 2, cart_mpi(1), ierr)
      end do
      rt = rrt(:,1:mz)

      deallocate (rr,rrt)
    else
      there = (/ here(1), 0 /)                                                  ! coordinates
      call MPI_CART_RANK(cart_mpi(1), there, rank, ierr)                        ! where to?
      call MPI_SEND (r , mxz, MPI_REAL, rank, 2, cart_mpi(1), ierr)             ! send domain data
      call MPI_RECV (rt, mxz, MPI_REAL, rank, 2, cart_mpi(1), status, ierr)     ! receive results
    end if

    call MPI_Barrier (mpi_comm_world, ierr)
  end if
  deallocate (w, fz)
END SUBROUTINE trf_z

!******************************************************************************
SUBROUTINE test_fft2d

  USE params
  USE mpi_mod
  implicit none
  real, allocatable, dimension(:,:):: f1, f2, ft, f3, k2
  real kx, kz, f1max, f1lim
  integer ix, iz, jx, jz

  allocate (f1(mx,mz), f2(mx,mz), ft(mx,mz), f3(mx,mz), k2(mx,mz))

  kx = 2.*pi/sx
  kz = 2.*pi/sx
  jx = (mx*mpi_nx)/2-1
  jz = (mz*mpi_nz)/2-1
  do iz=1,mz
  do ix=1,mx
    f1(ix,iz) = cos(2.*kx*xm(ix))*cos(3.*kz*zm(iz))
    f2(ix,iz) = f1(ix,iz)
    f3(ix,iz) = -((2.*kx)**2+(3.*kz)**2)*cos(2.*kx*xm(ix))*cos(3.*kz*zm(iz))
  end do
  end do

  if (master) print*,'fft2df called'
  call fft2df (f1, ft, mx, mz)
  if (master) print*,'fft2df done'

  if (mpi_x==0 .and. mpi_z==0) ft(4,6) = ft(4,6) - 0.25                         ! subtract the expected FT
  f1max = maxval (abs(ft))
  if (f1max > 1e-4) then                                                        ! check the residue
    print'(a,2i5,1p,e10.2)','test_fft2df ERROR: mpi_x, mpi_z, maxval(abs(ft)) =', &
      mpi_x, mpi_z, f1max
    call end_mpi
    stop
  else
    if (master) print *,'test_fft2df: diff =', f1max
  end if
  if (mpi_x==0 .and. mpi_z==0) ft(4,6) = ft(4,6) + 0.25                         ! add the expected FT

  call fft2db (ft, f1, mx, mz)

  f1 = abs(f2-f1)
  f1max = maxval (f1)
  if (f1max > 1e-5) then                                                        ! check the inverse result
    print '(a,2i4,g14.6)','test_fft2db inverse ERROR: mpi_x, mpi_z, maxval(abs(f2-f1)) =', &
      mpi_x, mpi_z, f1max
    call end_mpi
    stop
  else
    if (master) print *,'test_fft2db: diff =', f1max
  end if

  call fft2d_k2 (k2, sx, sz, mx, mz)
  do iz=1,mz
    do ix=1,mx
      ft(ix,iz) = -k2(ix,iz)*ft(ix,iz)
    end do
  end do
  call fft2db (ft, f1, mx, mz)

  f1 = abs(f3-f1)
  f1max = maxval (f1)
  f1lim = 2e-3*(kx*jx)**2
  if (f1max > f1lim) then                                                       ! check the inverse result
    print '(a,2i4,2g14.6)','test_fft2d Laplace ERROR: mpi_x, mpi_z, maxval(abs(f3-f1)) =', &
      mpi_x, mpi_z, f1max, f1lim
    call end_mpi
    stop
  else
    if (master) print *,'test_fft2d Laplace: diff =', f1max
  end if

  deallocate (f1, f2, ft, f3, k2)
END
