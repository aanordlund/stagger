! $Id: find_cores.f90,v 1.37 2016/11/09 22:04:57 aake Exp $
!*******************************************************************************
MODULE core_m
  logical do_trace,do_test
  integer, parameter:: lfile=80
  integer mx,my,mz,isnap,mvar,lrec,verbose,maxcells,noise
  real minlevel,ratiolevel,size,aver_n,mbenorm
  logical conservative,diagonal
  character(len=lfile) file,outfile
END MODULE

!***********************************************************************
SUBROUTINE limits_omp (n1,n2,i1,i2)
  implicit none
  integer n1,n2,i1,i2
#ifdef _OPENMP
  integer omp_mythread, omp_nthreads, omp_get_num_threads, omp_get_thread_num
!
  omp_mythread = omp_get_thread_num()
  omp_nthreads = omp_get_num_threads()
  i1 = n1 + ((omp_mythread  )*n2)/omp_nthreads
  i2 =      ((omp_mythread+1)*n2)/omp_nthreads
#else
  i1 = n1
  i2 = n2
#endif
END

!*******************************************************************************
PROGRAM cores
!
!  Read a snapshot and find the cores, using a procedure equivalent to the IDL
!  corefind.pro.
!-------------------------------------------------------------------------------
  USE core_m
  implicit none
  real void,dtime,time
  namelist /input/file,outfile,mx,my,mz,isnap,mvar,lrec,size,aver_n,minlevel, &
      ratiolevel,noise,mbenorm,maxcells,diagonal,conservative,do_test,do_trace,verbose
  real, allocatable, dimension(:,:,:) :: rho
  real, allocatable, dimension(:,:) :: scr
  integer iz, io_word
  logical exists
  character(len=120) :: id="$Id: find_cores.f90,v 1.37 2016/11/09 22:04:57 aake Exp $"

  !0........1........2........3........4........5........6........7........8
  character(len=80) :: help(19)= (/ &
  "PARAMETERS:                                                              ", &
  "                                                                         ", &
  " file        : input data file (raw format)                              ", &
  " outfile     : output data file (raw format)                             ", &
  " mx,my,mz    : data dimensions                                           ", &
  " isnap       : snapshot index (starting from zero)                       ", &
  " mvar        : number of fields per snapshot (7 for isothermal MHD)      ", &
  " lrec        : number of bytes per i/o word                              ", &
  " size        : region size in parsec                                     ", &
  " aver_n      : average particles per cc (=0 for Larson)                  ", &
  " minlevel    : minimum density level (in units of the mean density)      ", &
  " ratiolevel  : ratio of levels                                           ", &
  " noise       : need more voxels than this for acceptance                 ", &
  " mbenorm     : Bonnor-Ebert mass normalization                           ", &
  " maxcells    : max region size (in voxels)                               ", &
  " diagonal    : true for diagonal and edge contact                        ", &
  " conservative: true for rho in, false for ln(rho) in                     ", &
  " do_trace    : trace execution (for debuggin)                            ", &
  " verbose     : level of output noise                                     " /)

  print *,id

  file = 'snapshot.dat'
  outfile = 'cores.dat'
  mx = 128
  my = mx
  mz = mx
  mvar = 4
  isnap = 0
  lrec = io_word()
  verbose = 1
  aver_n = 0.
  noise = 3                 ! min voxels
  maxcells   = 1000000      ! max region size
  size       = 10.          ! pc
  minlevel   = 5.           ! times average
  ratiolevel = 1.04         ! step in density
  mbenorm = 3.4*0.5         ! bonnor-ebert mass normalization
  conservative = .true.     ! reading rho
  diagonal = .true.         ! include diagonal (and edge) contact
  do_trace = .false.
  do_test = .false.

  write (*,'(a80)') help

  print *,' '
  print '(a)','DEFAULT VALUES:'
  write (*,input)
  read (*,input)
  if (aver_n == 0.) aver_n  = 400./(size/5.)  ! cm^-3   should be 400 for Larson!!

  print *,' '
  print '(a)','ACTUAL VALUES:'
  write (*,input)

  if (do_test) then
    call test_find_cores
    stop
  end if

  print *, ' '
  print '(a)', 'READ DATA ...'
  allocate (rho(mx,my,mz))
  if (mx > 512) then
    print *,'reading slices'
    open (unit=1,file=file,form='unformatted',access='direct',status='old',recl=lrec*mx*my)
    allocate (scr(mx,my))
    do iz=1,mz
      if (mod(iz,100)==0) print *,iz
      read (1,rec=iz+mvar*isnap*mz) scr
      if (conservative) then
        rho(:,:,iz)=scr
      else
        rho(:,:,iz)=exp(scr)
      end if
    end do
    deallocate (scr)
  else
    print *,'reading volume'
    open (unit=1,file=file,form='unformatted',access='direct',status='old',recl=lrec*mx*my*mz)
    read (1,rec=1+mvar*isnap) rho
    if (.not. conservative) then
        rho=exp(rho)
    end if
  end if
  close (1)
  
  inquire(file='time.dat',exist=exists)
  if (exists) then
    open (1,file='time.dat',form='unformatted',access='direct',recl=lrec*4)
    read (1,rec=isnap+1) time
    close (1)
  else
    time=0.
  end if
  
  call find_cores (rho)
END

!*******************************************************************************
SUBROUTINE find_cores (rho)
!
!
!-------------------------------------------------------------------------------
  USE core_m
  implicit none
  integer nlevel, i, maxobj, maxreg, rr, c, cc, d, rrlast, version
  integer ix, iy, count, ntot, ss, j, k, l, scr, ncore, izs, nreg, delc, delp
  integer(kind=8) iz
  real, parameter:: pi=3.14159265
  real, dimension(0:mx-1,0:my-1,0:mz-1):: rho
  integer, allocatable, dimension(:,:,:) :: ones
  real mu, cs, mtot, rhomax, stellarmass
  real temp, minrho, maxrho, center(3), mass1, scrmass, density, mj
  real (kind=8):: kb, pc, msun, mh, G, dV, aver_rho, L_jeans
  real, allocatable, dimension(:) :: dmin, dmax, cells, dens, mass, mbe
  real, allocatable, dimension(:,:) :: coord
  integer(kind=8), allocatable, dimension(:) :: region,ireg
  real void
  real(kind=8) rhoav

  type object_t                                                 ! core object
    integer l                                                   ! length of index array
    integer(kind=8), allocatable, dimension(:) :: i                     ! index array
    real center(3)                                              ! center of mass
  end type
  type level_t                                                  ! level object
    real :: value                                               ! density level
    integer :: nobj                                             ! number of cores
    type (object_t), allocatable, dimension(:) ::objects        ! core objects
  end type
  type (level_t) , allocatable, dimension(:) :: level           ! array of levels
  type (object_t), allocatable, dimension(:) :: objects         ! array of cores

  if (do_trace) print *,'.................................... find_cores start'

  rhoav=0.
!$omp parallel do private(iz), reduction(+:rhoav)
  do iz=0,mz-1
    rhoav=rhoav+sum(rho(:,:,iz))
  end do
  rhoav=rhoav/(real(mx)*real(my)*real(mz))
  print *,' '
  print *,'mean snapshot density =',rhoav

  if (abs(rhoav-1.) > 0.01) then
    print *,'normalizing to unit average density'
!$omp parallel do private(iz)
    do iz=0,mz-1
      rho(:,:,iz)=rho(:,:,iz)/rhoav
    end do
  end if

  rhomax=0.
!$omp parallel do private(iz), reduction(max:rhomax)
  do iz=0,mz-1
    rhomax=max(rhomax,maxval(rho(:,:,iz)))
  end do
  print *,'max normalized density =',rhomax
  print *,' '

  nlevel  = ifix(alog(rhomax/minlevel)/alog(ratiolevel))+1

! PHYSICAL PARAMETERS:
! ====================
  temp = 10.                ! K
  kb = 1.38d-16
  mh = 1.67d-24
  mu = 2.5
  pc = 3d18 
  msun = 2d33
  G = 6.67d-8
  cs = sqrt(kb*temp/(mu*mh))
  aver_rho = aver_n*mh*mu
  mtot = (pc*size)**3*aver_rho/msun
  L_Jeans = sqrt(pi*cs**2/(G*aver_rho))
  dV = (pc*size)**3/(real(mx)*real(my)*real(mz))            ! cell volume in cm^3

  maxobj =   5000                         ! maximum number of object per density level

  print *,'       total mass :',mtot             ! solar masses
  print *,'mass/cell (rho=1) :',mtot/(real(mx)*real(my)*real(mz))  ! solar masses
  print *,'     L_Jeans (pc) :', L_Jeans/pc
  print *,'    rho*L_Jeans^3 :', aver_rho*L_Jeans**3/msun
  print *,'max obj per level :', maxobj

  allocate (level(nlevel))
  allocate (objects(maxobj))
  allocate (ones(0:mx-1,0:my-1,0:mz-1))
!
! Density levels:
!--------------

  do rr=1,nlevel
    level(rr)%value = 10.**((rr-1)*alog10(ratiolevel)+alog10(minlevel))
  end do
  print *, ' '
  print *,nlevel,'levels:'
  if (verbose > 0) print *,level(:)%value

  do i=1,maxobj
    objects(i)%l = 2
    allocate(objects(i)%i(2))
    objects(i)%i = -rr
  end do

  print *, ' '
  print '(a)','FIND ...'

  do rr=1,nlevel 

    minrho = level(rr)%value*1.0

    c = 0                                                                       ! count cells
    izs = mz-1                                                                  ! first iz hit
!$omp parallel do private(iz,iy,ix), reduction(+:c)
    do iz=0,mz-1
    do iy=0,my-1
    do ix=0,mx-1
      if (rho(ix,iy,iz) > minrho) then
        ones(ix,iy,iz) = 1                                                      ! mark hits
        izs = min(izs,int(iz))                                                       ! first iz hit
        c = c+1                                                                 ! increment cells
      else
        ones(ix,iy,iz) = 0                                                      ! no hit
      end if
    end do
    end do
    end do
    cc = c                                                                      ! remember cells

    if (rr == 1) then
      maxreg = c
      allocate (region(maxreg))                                                 ! addresses to objects
    end if

    i = 0
    count = 0
    stellarmass = 0.
    do while (c > 0)
      count = count+1
      call find_3d (rho,ones,mass1,center,ntot,region,maxreg,izs)
      if (maxreg < 0) go to 9
      scrmass = dV*mass1*aver_rho/2.d33                                         ! in solar masses
      density = mass1/ntot*aver_n                                               ! in cm^-3
      mj = mbenorm*(density/1000.)**(-0.5)*(temp/10.)**(1.5)                    ! in solar masses

      if (ntot > noise .and. scrmass > mj) then 
        if (verbose > 2) print '(2i6,3i9,f10.2,f7.2,1x,3i5,a,2x,i4,f7.3)', &
          rr,i,count,c,ntot,scrmass,mj,ifix(center),' *',izs
        i=i+1
        deallocate(objects(i)%i)
        allocate(objects(i)%i(ntot))
        objects(i)%i = region(1:ntot)
        objects(i)%l = ntot
        objects(i)%center = center
        stellarmass = stellarmass + scrmass
      else
        if (verbose > 3) print '(2i6,3i9,f10.2,f7.2,1x,3i5,a,2x,i4,f7.3)', &
          rr,i,count,c,ntot,scrmass,mj,ifix(center),'  ',izs
      end if
      c = c-ntot
    end do
    level(rr)%nobj = i
    allocate(level(rr)%objects(i))              ! allocate objects for this level
    do i=1,level(rr)%nobj                       ! for each object
      l = objects(i)%l                          ! its size
      allocate(level(rr)%objects(i)%i(l))       ! allocate indices
      level(rr)%objects(i)%i = objects(i)%i     ! copy indices
      level(rr)%objects(i)%l = objects(i)%l     ! index length
      level(rr)%objects(i)%center = objects(i)%center ! center of mass
    end do
    if (verbose > 0) print '(a,1pe9.2,i6,a,i4,i6,a,i9,a,1pe10.2,a)', &
      ' level ',minrho,rr,' /',nlevel,level(rr)%nobj,' objects ',cc,' cells', &
      stellarmass,' Msun'
    rrlast = rr
  end do                                                                        ! rr loop

  print *, ' '
  print '(a)', 'READ TREE ...'

  delc = 0                                                                      ! deleted childs
  delp = 0                                                                      ! deleted parents

  do rr=1,nlevel-1                                                              ! for each level
    if (verbose > 0) print *,'level ',rr,'/',nlevel
    do i=1,level(rr)%nobj                                                       ! for each object
      do ss=rr+1,nlevel                                                         ! check higher levels
        if (verbose > 4) print '(4i5)',rr,i,ss,level(ss)%nobj
        c = 0                                                                   ! c counts hits for all ss objects
        do j=1,level(ss)%nobj                                                   ! check objects
          d = 0                                                                 ! d counts hits for one object
          if (level(ss)%objects(j)%l > 0) then
            do k=1,level(rr)%objects(i)%l                                       ! count overlap
              if (level(rr)%objects(i)%i(k) == level(ss)%objects(j)%i(1)) then
                d = d+1
              end if
            end do
          end if
          c = c+d
          if (d == 1) then
            scr = j
          endif
          if (c >= 2) then                                                      ! object (rr,i) contains 2 or more
            level(rr)%objects(i)%i(:) = -rr                                     ! delete object (rr,i)
            level(rr)%objects(i)%l = 0
            if (verbose > 3)  print '(20x,3i5,4x,a)',rr,i,c,'p'
            delp = delp+1
            go to 2
          end if
        end do ! j
        if (c == 1) then                                                        ! if (rr,i) contains just one
          level(ss)%objects(scr)%i(:) = -ss                                     ! delete contained object
          level(ss)%objects(scr)%l = 0
          if (verbose > 3) print '(40x,3i5,4x,a)',ss,j,c,'c'
          delc = delc+1
        end if
      end do ! ss
2   continue

    end do ! i
  end do ! rr
  if (verbose > 0) print *,'deleted ',delp,' parents, ',delc,' children'

  print *,' '
  print '(a)','SUMMARY ...'

!3) SUMMARY: this part computes the masses of objects left in the
!            tree, and saves them.

  count = 0
  c = 0
  do rr=1,nlevel
    do i=1,level(rr)%nobj
      if (level(rr)%objects(i)%i(1) > 0) then
        count  = count+1
        c = c + level(rr)%objects(i)%l
      end if
    end do
  end do
  ncore = count
  nreg = c
  allocate (mass(ncore),mbe(ncore),dens(ncore),cells(ncore),dmin(ncore),dmax(ncore),coord(ncore,3))
  allocate (ireg(ncore))

  count = 0
  c = 1
  if (verbose > 1) print *,' core   voxels  mass       coordinates'
  do rr=1,nlevel
    do i=1,level(rr)%nobj
      if (level(rr)%objects(i)%i(1) > 0) then
        count  = count+1
        dmin(count) = 1e10
        dmax(count) = 0.
        mass(count) = 0.
        do k=1,level(rr)%objects(i)%l
          iz =  level(rr)%objects(i)%i(k)/(mx*my)
          iy = (level(rr)%objects(i)%i(k)-(iz*mx*my))/mx
          ix = (level(rr)%objects(i)%i(k)-(iz*mx*my))-(iy*mx)
          mass(count) = mass(count)+rho(ix,iy,iz)
          dmin(count) = min(dmin(count),rho(ix,iy,iz))
          dmax(count) = max(dmax(count),rho(ix,iy,iz))
        end do
        k = level(rr)%objects(i)%l
        dens(count)  = mass(count)/k*aver_n
        mass(count)  = mass(count)*dV*aver_rho/msun
        mbe(count)   = mbenorm*(dens(count)/1000.)**(-0.5)*(temp/10.)**(1.5) !in solar masses
        cells(count) = k
        coord(count,:) = level(rr)%objects(i)%center
        ireg(count) = c-1
        region(c:c+k-1) = level(rr)%objects(i)%i
        c = c+k
        if (verbose > 1) print '(i5,i8,f8.2,2x,3f7.1)',count,k,mass(count),coord(count,:)
      end if
    end do
  end do

  print *,nreg,' cells'
  print *,ncore,' cores'
  print *,'Efficiency:',sum(mass(1:count))/(real(mx)*real(my)*real(mz)*dV*aver_rho/msun)

  version = -1
  open (2,file=outfile,form='unformatted',status='unknown')
  write (2) version
  write (2) ncore,size,minlevel,ratiolevel,nlevel,aver_n,mbenorm
  write (2) mass,mbe,dens,cells,dmin,dmax,coord
  write (2) nreg,mx,my,mz
  write (2) ireg,region(1:nreg)
  close (2)

  print *,' '
  print *,'results saved in '//outfile

  if (do_trace) print *,'.................................... deallocate (mass,mbe,dens,cells,dmin,dmax,coord)'
  deallocate (mass,mbe,dens,cells,dmin,dmax,coord,ireg)

9 continue
  do rr=1,rrlast
    if (do_trace) print *,'.................................... deallocate (level(rr)%objects(i)%i)',rr
    do i=1,level(rr)%nobj
      deallocate(level(rr)%objects(i)%i)
    end do
    if (do_trace) print *,'.................................... deallocate (level(rr)%objects)'
    deallocate(level(rr)%objects)
  end do
  if (do_trace) print *,'.................................... deallocate (level,region,objects,ones)'
  deallocate (level,region,objects,ones)

END
!*******************************************************************************
INTEGER FUNCTION near(i,j,m)
!
!  Return the integer closest to i, which is equivalent (modulo m) to j, 
!  Replace i with the result, so it may be used in the next call.
!
!-------------------------------------------------------------------------------
  implicit none
  integer i,j,m

  if (abs(i-j) .gt. m/2) then                                                   ! is j too far from i?
    near=j+m*sign(1,i-j)                                                        ! add m to come closer
  else
    near=j                                                                      ! was close enough
  end if
  i=near                                                                        ! replace i
END FUNCTION

!*******************************************************************************
SUBROUTINE find_3d (rho,ones,mass,center,ntot,region,maxreg,izs)
!
!  Find connected points that have ones(:,:,:)==1, and mark as found.  Return
!  the total mass, the center of mass, and the total number of points.
!  
!-------------------------------------------------------------------------------
  USE core_m
  implicit none

  integer ntot,maxreg
  real    mass,center(3)
  real   , dimension(0:mx-1,0:my-1,0:mz-1) :: rho
  integer, dimension(0:mx-1,0:my-1,0:mz-1) :: ones
  integer(kind=8), dimension(maxreg) :: region
  integer ix,iy,ix0,iy0,ix1,iy1,iz1,k,k1,npos,npos1
  integer(kind=8) iz,iz0
  integer ixc,iyc,izc,ixp,iyp,izp,near,izs,ii2

  do iz=izs,mz-1
    do iy=0,my-1
      do ix=0,mx-1
        if (ones(ix,iy,iz).eq.1) goto 1                                         ! first contact
      enddo
    enddo
  enddo
  mass=0.                                                                       ! no contact
  center=0.
  ntot=0
  return

1 k1=1
  izs=iz
  region(k1)=ix+mx*(iy+my*iz)                                                   ! packed index
  ixp=ix
  iyp=iy
  izp=iz
  ones(ix,iy,iz)=2                                                              ! mark as counted
  mass=rho(ix,iy,iz)
  center(1)=rho(ix,iy,iz)*ix
  center(2)=rho(ix,iy,iz)*iy
  center(3)=rho(ix,iy,iz)*iz
  k=1

! Search a 3x3x3 neighborhood for connected points; these are indexed by 'k1',
! their locations are encoded in 'region', and the 'ones' marker is zeroed. 
! The center of the search is then advanced to the next available point, as
! long as their are more points.  In the beginning each 'k' point generates
! more than one 'k1' point, but towards the end there are 'k' points that do
! not generate more points, and finally the region is exhausted.

  do while (k.le.k1)                                                            ! while more cells
    iz0=region(k)/(mx*my)
    iy0=(region(k)-iz0*mx*my)/mx
    ix0=(region(k)-iz0*mx*my-iy0*mx)
    do iz1=-1,1                                                                 ! loop over neighbors
      iz=mod(int(iz0)+iz1+mz,mz)
      do iy1=-1,1
        iy=mod(iy0+iy1+my,my)
        do ix1=-1,1
	  if (.not. diagonal) then
	    ii2 = ix1**2+iy1**2+iz1**2
	    if (ii2 > 1) cycle
	  end if
          ix=mod(ix0+ix1+mx,mx)                                                 ! periodic
          if (ones(ix,iy,iz).eq.1) then
            k1=k1+1                                                             ! next connected cell
            if (k1 > maxreg) then
              print *,'increase maxreg, too small:', maxreg
              maxreg=-maxreg
              return
            end if
            region(k1)=ix+mx*(iy+my*iz)                                         ! packed index
            ones(ix,iy,iz)=0                                                    ! mark as found
            mass=mass+rho(ix,iy,iz)                                             ! add up mass
            ixc=near(ixp,ix,mx)
            iyc=near(iyp,iy,my)                                                 ! center of mass indices
            izc=near(izp,int(iz),mz)
            center(1)=center(1)+rho(ix,iy,iz)*ixc                               ! center of mass
            center(2)=center(2)+rho(ix,iy,iz)*iyc
            center(3)=center(3)+rho(ix,iy,iz)*izc
          end if
        end do
      end do
    end do
    k=k+1
  end do
  ntot=k1

  center=center/mass                                                            ! compute center
  center(1)=mod(center(1)+mx,real(mx))
  center(2)=mod(center(2)+my,real(my))
  center(3)=mod(center(3)+mz,real(mz))

END SUBROUTINE

!******************************************************************************
SUBROUTINE test_find_cores
!
!  Test the find_3d and find_cores subroutines
!
!------------------------------------------------------------------------------
  USE core_m
  implicit none
  real, allocatable, dimension(:,:,:):: rho
  integer ix,iy,iz,ix0,iy0,iz0,ix1,iy1,iz1,k,mp,iw
  real w,f,dtime,eps,aeps,r2,cw

  print *,' '
  print '(a)','GENERATING TEST DATA'

  w=3.
  mp=50

  print *,mx*my*mz*8e-6,' MB'
  allocate (rho(mx,my,mz))

  ix0=mx/2
  iy0=my/2
  iz0=mz/2

!$omp parallel do private(iz)
  do iz=1,mz
    rho(:,:,iz)=0.
  end do

  eps=1e-2
  aeps=-alog(eps)
  iw=1.+w*sqrt(aeps)
  print *,'iw',iw
  iw=min(iw,ix0-1,iy0-1,iz0-1)

  cw=1./w**2
!$omp parallel do private(iz,iy,ix,r2,f,ix1,iy1,iz1)
  do iz=ix0-iw,ix0+iw
  do iy=iy0-iw,iy0+iw
  do ix=iz0-iw,iz0+iw
    r2=((ix-ix0)**2+(iy-iy0)**2+(iz-iz0)**2)*cw
    if (r2.lt.aeps) then
      f=exp(-r2)
    else
      f=0.
    end if
    do k=1,mp
      ix1=1+mod(k*12345671+ix,mx)
      iy1=1+mod(k*21354672+iy,my)
      iz1=1+mod(k*12435673+iz,mz)
      f=f*exp(mod(k*43125673+iz,mz)/real(mz-1))
      rho(ix1,iy1,iz1)=rho(ix1,iy1,iz1)+f
    end do
  end do
  end do
  end do
  rho = 1.+100.*rho

  open (1,file=file,access='direct',status='unknown',recl=lrec*mx*my*mz)
  write (1,rec=1) rho
  close (1)

  call find_cores (rho)
  deallocate (rho)

  print *,'test input data saved in ',file
  print *,'dimensions',mx,my,mz
END
