! $Id: fft2d.f90,v 1.4 2016/11/09 20:31:30 aake Exp $
!----------------------------------------------------------------------
!
!  Two-dimensional Fourier transform routines, based on FFTPACK.
!  FFTPACK is transportable, and allows arbitrary dimensions (speed
!  will vary, of course).
!
!  CALL fft2df (f, ft, mx, my)              ! forward transform
!  CALL fft2db (ft, f, mx, my)              ! backward transform
!  CALL fft2d_k2 (k2, dx, dy, mx, my)       ! compute k2
!
!**********************************************************************
SUBROUTINE fft2d_k2 (k2, sx, sy, mx, my)
  real, dimension(mx,my):: k2
  real pi, sx, sy, kx, ky
  integer i, j, k, mx, my
!----------------------------------------------------------------------

  pi=4.*atan(1.)

  do j=1,my
    ky = 2.*pi/sy*(j/2)
    do i=1,mx
      kx = 2.*pi/sx*(i/2)
      k2(i,j) = kx**2+ky**2
    end do
  end do

END SUBROUTINE

!**********************************************************************
SUBROUTINE fft2df (r, rt, mx, my)
  implicit none

  integer i, j, k, mx, my
  real, dimension(mx,my):: r, rt

  integer mwx, mwy
  real c, fx(mx), fy(my)
  real, allocatable, dimension(:):: wx, wy
!----------------------------------------------------------------------
  mwx=2*mx+15
  mwy=2*my+15
  allocate (wx(mwx), wy(mwy))
  call srffti (mx,wx)
  call srffti (my,wy)

  do j=1,my
    fx=r(:,j)
    call srfftf (mx, fx, wx)
    c=1./mx
    rt(:,j)=c*fx
  end do

  do i=1,mx
    fy=rt(i,:)
    call srfftf (my, fy, wy)
    c=1./my
    rt(i,:)=c*fy
  end do

  deallocate (wx, wy)
END SUBROUTINE

!**********************************************************************
SUBROUTINE fft2db (r,rt,mx,my)
  implicit none
  integer i, j, k, mx, my
  real, dimension(mx,my):: r, rt

  integer mwx, mwy
  real c, fx(mx), fy(my)
  real, allocatable, dimension(:):: wx, wy
!----------------------------------------------------------------------
  mwx=2*mx+15
  mwy=2*my+15
  allocate (wx(mwx), wy(mwy))
  call srffti (mx,wx)
  call srffti (my,wy)

  do j=1,my
    fx = r(:,j)
    call srfftb (mx, fx, wx)
    rt(:,j) = fx
  end do

  do i=1,mx
    fy = rt(i,:)
    call srfftb (my, fy, wy)
    rt(i,:) = fy
  end do

  deallocate (wx, wy)
END SUBROUTINE

!**********************************************************************
SUBROUTINE cfft2df (r, rt, mx, my)
  implicit none
  integer i, j, k, mx, my
  complex, dimension(mx,my):: r, rt

  integer mwx, mwy
  complex, allocatable, dimension(:):: wx, wy
  complex c, fx(mx), fy(my)
!----------------------------------------------------------------------
  mwx=2*mx+15
  mwy=2*my+15
  allocate (wx(mwx), wy(mwy))
  call cffti (mx,wx)
  call cffti (my,wy)

  do j=1,my
    fx=r(:,j)
    call cfftf (mx, fx, wx)
    c=1./mx
    rt(:,j)=c*fx
  end do

  do i=1,mx
    fy=rt(i,:)
    call cfftf (my, fy, wy)
    c=1./my
    rt(i,:)=c*fy
  end do

  deallocate (wx, wy)
END SUBROUTINE

!**********************************************************************
SUBROUTINE cfft2db (r,rt,mx,my)
  implicit none
  integer i, j, k, mx, my
  complex, dimension(mx,my):: r, rt

  integer mwx, mwy
  complex, allocatable, dimension(:):: wx, wy
  complex c, fx(mx), fy(my)
!----------------------------------------------------------------------
  mwx=2*mx+15
  mwy=2*my+15
  allocate (wx(mwx), wy(mwy))
  call cffti (mx,wx)
  call cffti (my,wy)

  do j=1,my
    fx = r(:,j)
    call cfftb (mx, fx, wx)
    rt(:,j) = fx
  end do

  do i=1,mx
    fy = rt(i,:)
    call cfftb (my, fy, wy)
    rt(i,:) = fy
  end do

  deallocate (wx, wy)
END SUBROUTINE
