! $Id: fft3d_parallel_region.f90,v 1.2 2007/07/09 01:43:15 aake Exp $
!**********************************************************************
SUBROUTINE limits_omp (n1,n2,i1,i2)
  implicit none
  integer n1,n2,i1,i2
  integer omp_get_num_threads, omp_get_thread_num
  logical omp_in_parallel
!
#if defined (_OPENMP)
  if (omp_in_parallel()) then
    i1 = n1 + ((omp_get_thread_num()  )*n2)/omp_get_num_threads()
    i2 =      ((omp_get_thread_num()+1)*n2)/omp_get_num_threads()
  else
    i1 = max(1,n1)
    i2 = n2
  end if
#else
  i1 = max(1,n1)
  i2 = n2
#endif
END

!**********************************************************************
SUBROUTINE fft3d_k2 (k2,dx,dy,dz,mx,my,mz)
  implicit none
  integer mx, my, mz
  real, dimension(mx,my,mz):: k2
  real dx, dy, dz, kx, ky, kz
  real, parameter:: pi=3.14519265
  integer i, j, k, izs, ize, omp_get_thread_num, omp_get_num_threads
!----------------------------------------------------------------------
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    kz=2.*pi/(dz*mz)*(k/2)
    do j=1,my
      ky=2.*pi/(dy*my)*(j/2)
      do i=1,mx
        kx=2.*pi/(dx*mx)*(i/2)
        k2(i,j,k)=kx**2+ky**2+kz**2
      end do
    end do
  end do
END SUBROUTINE

!**********************************************************************
SUBROUTINE fft3df (r,rr,mx,my,mz)
  implicit none
  integer mx,my,mz
  real, dimension(mx,my,mz):: r, rr
  integer, parameter:: lw=4096, lwx=2*lw+15, lwy=2*lw+15, lwz=2*lw+15
  real c, wx(lwx), wy(lwy), wz(lwz), fx(mx), fy(my), fz(mz,mx)
  integer i, j, k, iys, iye, izs, ize, omp_get_thread_num, omp_get_num_threads
!----------------------------------------------------------------------
  if (mx.gt.lw .or. my.gt.lw .or. mz.gt.lw) then
    print *,'fft3db: dim too small',mx,my,mz,lw
    stop
  end if
  call srffti (mx,wx)
  call srffti (my,wy)
  call srffti (mz,wz)

  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
   do j=1,my
    fx(:)=r(:,j,k)
    call srfftf (mx,fx,wx)
    c=1./mx
    rr(:,j,k)=c*fx(:)
   end do
   do i=1,mx
    fy=rr(i,:,k)
    call srfftf (my,fy,wy)
    c=1./my
    rr(i,:,k)=c*fy
   end do
  end do
  !$omp barrier

  call limits_omp(1,my,iys,iye)
  do j=iys,ize
    do k=1,mz
      fz(k,:)=rr(:,j,k)
    end do
    do i=1,mx
      call srfftf (mz,fz(:,i),wz)
    end do
    c=1./mz
    do k=1,mz
      rr(:,j,k)=c*fz(k,:)
    end do
  end do

END SUBROUTINE

!**********************************************************************
SUBROUTINE fft3db (rr,r,mx,my,mz)
  implicit none
  integer mx,my,mz
  real, dimension(mx,my,mz):: r, rr
  integer, parameter:: lw=4096, lwx=2*lw+15, lwy=2*lw+15, lwz=2*lw+15
  real wx(lwx), wy(lwy), wz(lwz), fx(mx), fy(my), fz(mz,mx)
  integer i, j, k, iys, iye, izs, ize, omp_get_thread_num, omp_get_num_threads
!----------------------------------------------------------------------

  if (mx.gt.lw .or. my.gt.lw .or. mz.gt.lw) then
    print *,'fft3db: dim too small',mx,my,mz,lw
    stop
  end if
  call srffti (mx,wx)
  call srffti (my,wy)
  call srffti (mz,wz)

  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
   do j=1,my
    fx=rr(:,j,k)
    call srfftb (mx,fx,wx)
    r(:,j,k)=fx
   end do
   do i=1,mx
    fy=r(i,:,k)
    call srfftb (my,fy,wy)
    r(i,:,k)=fy
   end do
  end do
  !$omp barrier

  call limits_omp(1,my,iys,iye)
  do j=iys,ize
    do k=1,mz
      fz(k,:)=r(:,j,k)
    end do
    do i=1,mx
      call srfftb (mz,fz(:,i),wz)
    end do
    do k=1,mz
      r(:,j,k)=fz(k,:)
    end do
  end do

END SUBROUTINE
