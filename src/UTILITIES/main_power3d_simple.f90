! $Id: main_power3d_simple.f90,v 1.6 2016/11/09 22:04:58 aake Exp $
!-----------------------------------------------------------------------
PROGRAM main_power3d
  implicit none
  integer ivar, jvar, mvar, mk, ix, iy, iz, lrec, isnap, isnap1, isnap2
  integer mx, my, mz, simple, io_word
  real, allocatable, dimension(:,:,:) :: fx, tx, lnrho, scr
  real, allocatable, dimension(:) :: pkxt, pkyt, pkzt, nk, xk
  real ptot(3)
  logical pervolume, verbose
  integer omp_get_thread_num, omp_get_num_threads
  character*64:: infile, outfile
  namelist /io/mx,my,mz,infile,ivar,isnap1,isnap2,mvar,mk,outfile,pervolume,verbose

  isnap1 = 0
  isnap2 = 0
  infile = 'snapshot.dat'
  mx = 1024
  my = 1024
  mz = 1024
  ivar = 1
  mk = mx/2
  outfile = 'power3d.dat'
  pervolume = .false.
  verbose = .false.

  write (*,io)
  read (*,io)
  write (*,io)

  allocate (fx(mx,my,mz))
  allocate (tx(mx,my,mz))
  allocate (pkxt(mk), pkyt(mk), pkzt(mk), nk(mk), xk(mk))
  
  do isnap=isnap1,isnap2
    lrec = mx*my*io_word()
    open (1, file=infile, form='unformatted', &
      access='direct', recl=lrec)
    
    if (verbose) print*,'reading ux'
    do iz=1,mz
      read (1,rec=iz+(0+ivar+isnap*mvar)*mz) fx(:,:,iz)
      if (verbose .and. mod(iz,32)==0) print*,iz
    end do
    if (verbose) print *, minval(fx), maxval(fx)

    if (verbose) print*,'ux transform'
    !$omp parallel
    !print *,omp_mythread,' calling'
    call fft3df (fx, tx, mx, my, mz)
    !$omp end parallel
    if (verbose) print*,'ux power'
    !$omp parallel
    call power3d (tx, mx, my, mz, pkxt, xk, nk, mk, ptot(1))
    !$omp end parallel

    if (verbose) print*,'reading uy'
    do iz=1,mz; read (1,rec=iz+(1+ivar+isnap*mvar)*mz) fx(:,:,iz) ; end do
    if (verbose) print *, minval(fx), maxval(fx)

    if (verbose) print*,'uy transform'
    !$omp parallel
    call fft3df (fx, tx, mx, my, mz)
    call power3d (tx, mx, my, mz, pkyt, xk, nk, mk, ptot(2))
    !$omp end parallel

    if (verbose) print*,'reading uz'
    do iz=1,mz; read (1,rec=iz+(2+ivar+isnap*mvar)*mz) fx(:,:,iz) ; end do
    if (verbose) print *, minval(fx), maxval(fx)

    if (verbose) print*,'uz transform'
    !$omp parallel
    !if (verbose .and. omp_get_thread_num() == 0) print *,'FFT'
    call fft3df (fx, tx, mx, my, mz)
    call power3d (tx, mx, my, mz, pkzt, xk, nk, mk, ptot(3))
    !$omp end parallel

    print *,'write: lrec =',lrec/io_word(),'    file =',outfile
    lrec = (1+4+4*mk)*io_word()
    simple = -1
    open (2, file=outfile, form='unformatted', &
      access='direct', recl=lrec, status='unknown')
    write (2,rec=1+isnap) mk,simple,ptot,xk,pkxt,pkyt,pkzt
    close (2)

    print *, isnap, ' done'
  end do

  deallocate (fx, tx)
END

