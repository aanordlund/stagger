! $Id: fft3d.f90,v 1.4 2007/09/29 21:45:56 aake Exp $
!----------------------------------------------------------------------
!
!  Three-dimensional Fourier transform routines, based on FFTPACK.
!  FFTPACK is transportable, and allows arbitrary dimensions (speed
!  will vary, of course).
!
!  CALL fft3df (f, ft, mx, my, mz)              ! forward transform
!  CALL fft3db (ft, f, mx, my, mz)              ! backward transform
!  CALL fft3d_k2 (k2, dx, dy, dz, mx, my, mz)   ! compute k2
!
!**********************************************************************
SUBROUTINE fftpack_test
!
!  This test demonstrates that the coeffs returned are:
!
!   [a_0, +a_1/2., +b_1/2., ..., b_{ny-1}/2.,  a_ny]
!
!  Thus, the derivive is formed by setting the tranform to
!
!   [ 0., -b_1/2, +a_1/2., ...., a_{ny-1}/2.,  0.  ]
!
  real, allocatable, dimension(:):: f,ft,w
  integer m,mw,i,k
  real a,pi

  m=8
  mw=2*m+15
  allocate (f(m),ft(m),w(mw))

  pi = 4.*atan(1.)

  a = 2.*pi/m
  f = 0.
  do k=0,m/2
    do i=1,m
      f(i) = f(i) + 2.*(k+1)*cos((i-1)*k*a) + 2.*(k+m)*sin((i-1)*k*a)
    end do
  end do
  call srffti (m,w)
  print *,'f:'
  print *,f
  ft = f
  call srfftf (m,ft,w)
  print *,'ft:'
  print '(8f10.2)',ft/m

  m = 7
  a = 2.*pi/m
  f = 0.
  do k=0,m/2
    do i=1,m
      f(i) = f(i) + 2.*(k+1)*cos((i-1)*k*a) + 2.*(k+m)*sin((i-1)*k*a)
    end do
  end do
  call srffti (m,w)
  print *,'f:'
  print '(8f10.2)',(f(i),i=1,m)
  ft = f
  call srfftf (m,ft,w)
  print *,'ft:'
  print '(8f10.2)',(ft(i)/m,i=1,m)

  deallocate (f,ft,w)
END

!**********************************************************************
SUBROUTINE fft3d_k2(k2,dx,dy,dz,mx,my,mz)
  real, dimension(mx,my,mz):: k2
  real pi, dx, dy, dz, kx, ky, kz
  integer i, j, k, mx, my, mz
!----------------------------------------------------------------------

  pi=4.*atan(1.)

  do k=1,mz
    kz=2.*pi/(dz*mz)*(k/2)
    do j=1,my
      ky=2.*pi/(dy*my)*(j/2)
      do i=1,mx
        kx=2.*pi/(dx*mx)*(i/2)
        k2(i,j,k)=kx**2+ky**2+kz**2
      end do
    end do
  end do

END SUBROUTINE

!**********************************************************************
SUBROUTINE fft3df (r,rt,mx,my,mz)
  implicit none

  integer i,j,k,mx,my,mz
  integer, parameter:: mw=1024, mwx=2*mw+15, mwy=2*mw+15, mwz=2*mw+15

  real, dimension(mx,my,mz):: r, rt
  real c, wx(mwx), wy(mwy), wz(mwz), fx(mx), fy(my), fz(mz,mx)
!----------------------------------------------------------------------

  call srffti (mx,wx)
  call srffti (my,wy)
  call srffti (mz,wz)

  c=1./mx
  !$omp parallel do private(j,k,fx), firstprivate(wx)
  do k=1,mz
  do j=1,my
    fx=r(:,j,k)
    call srfftf (mx,fx,wx)
    rt(:,j,k)=c*fx
  end do
  end do

  c=1./my
  !$omp parallel do private(i,k,fy), firstprivate(wy)
  do k=1,mz
  do i=1,mx
    fy=rt(i,:,k)
    call srfftf (my,fy,wy)
    rt(i,:,k)=c*fy
  end do
  end do

  c=1./mz
  !$omp parallel do private(i,j,k,fz), firstprivate(wz)
  do j=1,my
    do k=1,mz
      fz(k,:)=rt(:,j,k)
    end do
    do i=1,mx
      call srfftf (mz,fz(1,i),wz)
    end do
    do k=1,mz
      rt(:,j,k)=c*fz(k,:)
    end do
  end do

END SUBROUTINE

!**********************************************************************
SUBROUTINE fft3db (rt,r,mx,my,mz)
  implicit none

  integer i,j,k,mx,my,mz
  integer, parameter:: mw=1024, mwx=2*mw+15, mwy=2*mw+15, mwz=2*mw+15

  real, dimension(mx,my,mz):: r, rt
  real wx(mwx), wy(mwy), wz(mwz), fx(mx), fy(my), fz(mz,mx)
!----------------------------------------------------------------------

  call srffti (mx,wx)
  call srffti (my,wy)
  call srffti (mz,wz)

  !$omp parallel do private(j,k,fx), firstprivate(wx)
  do k=1,mz
  do j=1,my
    fx=rt(:,j,k)
    call srfftb (mx,fx,wx)
    r(:,j,k)=fx
  end do
  end do

  !$omp parallel do private(i,k,fy), firstprivate(wy)
  do k=1,mz
  do i=1,mx
    fy=r(i,:,k)
    call srfftb (my,fy,wy)
    r(i,:,k)=fy
  end do
  end do

  !$omp parallel do private(i,j,k,fz), firstprivate(wz)
  do j=1,my
    do k=1,mz
      fz(k,:)=r(:,j,k)
    end do
    do i=1,mx
      call srfftb (mz,fz(1,i),wz)
    end do
    do k=1,mz
      r(:,j,k)=fz(k,:)
    end do
  end do

END SUBROUTINE

!**********************************************************************
SUBROUTINE test_fft
  USE params
  implicit none
  integer ix, iy, iz
  character(len=mid):: id="$Id: fft3d.f90,v 1.4 2007/09/29 21:45:56 aake Exp $"
  real, allocatable, dimension(:,:,:):: fft1,fft2

  call print_id (id)
  allocate (fft1(mx,my,mz), fft2(mx,my,mz))

!$omp parallel private(ix,iy,iz)
  do iz=izs,ize
  do iy=1,my
  do ix=1,mx
    fft1(ix,iy,iz) = cos(2.*pi*(xm(ix)-xmin)/sx) + 2.*cos(2.*pi*(ym(iy)-ymin)/sy) + 3.*cos(2.*pi*(zm(iz)-zmin)/sz)
  end do
  end do
  end do
!$omp barrier
  call fft3df (fft1, fft2, mx, my, mz)
!$omp end parallel

  print '(i6,8f10.3)', mpi_rank, fft2(1:2,1:2,1:2)
  deallocate (fft1, fft2)
END
