! $Id: fft2d_OMP.f90,v 1.20 2013/03/24 13:28:15 aake Exp $
!----------------------------------------------------------------------
!
!  Three-dimensional Fourier transform routines, based on FFTPACK.
!  FFTPACK is transportable, and allows arbitrary dimensions (speed
!  will vary, of course).
!
!  CALL fft2df (f, ft, mx, my, mz)              ! forward transform
!  CALL fft2db (ft, f, mx, my, mz)              ! backward transform
!  CALL fft2d_k2 (k2, dx, dy, dz, mx, my, mz)   ! compute k2
!
!**********************************************************************
MODULE fft2d_m
  real, allocatable, dimension(:,:):: f, ft, ff
END MODULE

SUBROUTINE test_fft2d
  USE params
  USE fft2d_m
  implicit none
  integer ix,iz,m(2)
  real err
  character(len=mid), save:: id='$Id: fft2d_OMP.f90,v 1.20 2013/03/24 13:28:15 aake Exp $'

  if (master) print *,hl
  call print_id(id)
!$omp master
  allocate (f(mx,mz),ft(mx,mz),ff(mx,mz))
!$omp end master
!$omp barrier

  do iz=izs,ize
  do ix=1,mx
    f(ix,iz) = cos(iz*2.*pi/mz) + cos(2.*iz*2.*pi/mz) + sin(ix*2.*pi/mx)
  end do
  end do
!$omp barrier
  call fft2df (f, ft, mx, mz)
!$omp barrier
  call fft2db (ft, ff, mx, mz)
!$omp barrier
  !print *,'thread, error:', omp_mythread, izs, ize, maxval(ff(:,izs:ize)-f(:,izs:ize))
  !m = maxloc(ff(:,izs:ize)-f(:,izs:ize))
  !print *,m
  !print *,f(:,m(2))
  !print *,ff(:,m(2))

!$omp barrier
!$omp single
  ff = abs(ff-f)
  err = maxval(ff)
  if (master) print *,'max error:', err
  if (err > 1e-5) then
    print *, mpi_rank, 'Too large error in test_fft2d!  err =', err
    stop
  end if
  deallocate(f,ft,ff)
!$omp end single

END

!**********************************************************************
SUBROUTINE fft2d_k2(k2,sx1,sz1,mx1,mz1)
  USE params
  implicit none
  integer mx1, mz1
  real, dimension(mx1,mz1):: k2
  real sx1, sz1
  logical omp_in_parallel
!----------------------------------------------------------------------

  if (omp_in_parallel()) then
    call fft2d_k2_omp(k2,sx1,sz1,mx1,mz1)
  else
    !$omp parallel shared(mx1,mz1)
    call fft2d_k2_omp(k2,sx1,sz1,mx1,mz1)
    !$omp end parallel
  end if
END SUBROUTINE

!**********************************************************************
SUBROUTINE fft2d_k2_omp(k2,sx1,sz1,mx1,mz1)
  USE params
  implicit none
  integer mx1, mz1
  real, dimension(mx1,mz1):: k2
  real sx1, sz1, kx, kz
  integer i, k
!----------------------------------------------------------------------

  do k=izs,ize
    kz=2.*pi/sz1*(k/2)
    do i=1,mx
      kx=2.*pi/sx1*(i/2)
      k2(i,k)=kx**2+kz**2
    end do
  end do
END SUBROUTINE

!**********************************************************************
SUBROUTINE fft2df (r,rr,mx1,mz1)
  USE params
  implicit none
  integer mx1,mz1
  real, dimension(mx,mz):: r, rr
  logical omp_in_parallel
!
  if (omp_in_parallel()) then
    call fft2df_omp (r,rr,mx1,mz1)
  else
    !$omp parallel shared(mx1,mz1)
    call fft2df_omp (r,rr,mx1,mz1)
    !$omp end parallel
  end if
!
END SUBROUTINE

!**********************************************************************
SUBROUTINE fft2df_omp (r,rr,mx1,mz1)
  USE params
  implicit none
  integer i,k,mx1,mz1
  integer, parameter:: lw=4096, lwx=2*lw+15, lwz=2*lw+15
  real, dimension(mx,mz):: r, rr
  real c, wx(lwx), wz(lwz), fx(mx), fz(mz)
  integer ixs, ixe
!----------------------------------------------------------------------
!
  if (mx.gt.lw .or. mz.gt.lw) then
    print *,'fft2db: dim too small',mx,mz,lw
    stop
  end if
  call srffti (mx,wx)
  call srffti (mz,wz)
!
  c=1./mx
  do k=izs,ize
   fx(:)=r(:,k)
   call srfftf (mx,fx,wx)
   rr(:,k)=c*fx(:)
  end do
!
  call barrier_omp ('fft2df')                                           ! different //-direction
  ixs = 1 + ((omp_mythread  )*mx)/omp_nthreads
  ixe =     ((omp_mythread+1)*mx)/omp_nthreads
  !print *,'thread, ixs, ize:', omp_mythread, ixs, ixe
  c=1./mz
  do i=ixs,ixe
    fz=rr(i,:)
    call srfftf (mz,fz,wz)
    rr(i,:)=c*fz
  end do
  call barrier_omp ('fft2df')                                           ! different //-direction
!
END SUBROUTINE

!**********************************************************************
SUBROUTINE fft2db (r,rr,mx1,mz1)
  USE params
  implicit none
  integer mx1,mz1
  real, dimension(mx,mz):: r, rr
  logical omp_in_parallel
!
  if (omp_in_parallel()) then
    call fft2db_omp (r,rr,mx1,mz1)
  else
    !$omp parallel shared(mx1,mz1)
    call fft2db_omp (r,rr,mx1,mz1)
    !$omp end parallel
  end if
!
END SUBROUTINE

!**********************************************************************
SUBROUTINE fft2db_omp (rr,r,mx1,mz1)
  USE params
  implicit none
  integer i,k,mx1,mz1
  integer, parameter:: lw=4096, lwx=2*lw+15, lwz=2*lw+15
  real, dimension(mx,mz):: r, rr
  real wx(lwx), wz(lwz), fx(mx), fz(mz)
  integer ixs, ixe
!----------------------------------------------------------------------
!
  if (mx.gt.lw .or. mz.gt.lw) then
    print *,'fft2db: dim too small',mx,my,mz,lw
    stop
  end if
  call srffti (mx,wx)
  call srffti (mz,wz)
!
  call barrier_omp ('fft2db')                                           ! different //-direction
  ixs = 1 + ((omp_mythread  )*mx)/omp_nthreads
  ixe =     ((omp_mythread+1)*mx)/omp_nthreads
  !print *,'thread, ixs, ixe:', omp_mythread, ixs, ixe
  do i=ixs,ixe
    fz=rr(i,:)
    call srfftb (mz,fz,wz)
    r(i,:)=fz
  end do
  call barrier_omp ('fft2db')                                           ! different //-direction
!
  !print *,'thread, izs, ize:', omp_mythread, izs, ize
  do k=izs,ize
    call srfftb (mx,r(:,k),wx)
  end do
!
END SUBROUTINE

!**********************************************************************
SUBROUTINE fft2ef (r,rr,mx1,my1,mz1)
  USE params
  implicit none
  integer mx1,my1,mz1
  real, dimension(mx,my1,mz):: r, rr
  logical omp_in_parallel
!
  if (omp_in_parallel()) then
    call fft2ef_omp (r,rr,mx1,my1,mz1)
  else
    !$omp parallel shared(mx1,my1,mz1)
    call fft2ef_omp (r,rr,mx1,my1,mz1)
    !$omp end parallel
  end if
!
END SUBROUTINE

!**********************************************************************
SUBROUTINE fft2ef_omp (r,rr,mx1,my1,mz1)
  USE params
  implicit none
  integer i,j,k,mx1,my1,mz1
  integer, parameter:: lw=4096, lwx=2*lw+15, lwz=2*lw+15
  real, dimension(mx,my1,mz):: r, rr
  real c, wx(lwx), wz(lwz), fx(mx), fz(mz)
  integer ixs, ixe
!----------------------------------------------------------------------
!
  if (mx.gt.lw .or. mz.gt.lw) then
    print *,'fft2eb: dim too small',mx,mz,lw
    stop
  end if
  call srffti (mx,wx)
  call srffti (mz,wz)
!
  c=1./mx
  do k=izs,ize
  do j=1,my1
   fx=r(:,j,k)
   call srfftf (mx,fx,wx)
   rr(:,j,k)=c*fx
  end do
  end do
!
  call barrier_omp ('fft2def')                                          ! different //-direction
  ixs = 1 + ((omp_mythread  )*mx)/omp_nthreads
  ixe =     ((omp_mythread+1)*mx)/omp_nthreads
  !print *,'thread, ixs, ize:', omp_mythread, ixs, ixe
  c=1./mz
  do i=ixs,ixe
  do j=1,my1
    fz=rr(i,j,:)
    call srfftf (mz,fz,wz)
    rr(i,j,:)=c*fz
  end do
  end do
  call barrier_omp ('fft2def')                                          ! different //-direction
!
END SUBROUTINE

!**********************************************************************
SUBROUTINE fft2eb (r,rr,mx1,my1,mz1)
  USE params
  implicit none
  integer mx1,my1,mz1
  real, dimension(mx,my1,mz):: r, rr
  logical omp_in_parallel
!
  if (omp_in_parallel()) then
    call fft2eb_omp (r,rr,mx1,my1,mz1)
  else
    !$omp parallel shared(mx1,my1,mz1)
    call fft2eb_omp (r,rr,mx1,my1,mz1)
    !$omp end parallel
  end if
!
END SUBROUTINE

!**********************************************************************
SUBROUTINE fft2eb_omp (rr,r,mx1,my1,mz1)
  USE params
  implicit none
  integer i,j,k,mx1,my1,mz1
  integer, parameter:: lw=4096, lwx=2*lw+15, lwz=2*lw+15
  real, dimension(mx,my1,mz):: r, rr
  real wx(lwx), wz(lwz), fx(mx), fz(mz)
  integer ixs, ixe
!----------------------------------------------------------------------
!
  if (mx.gt.lw .or. mz.gt.lw) then
    print *,'fft2eb: dim too small',mx,my,mz,lw
    stop
  end if
  call srffti (mx,wx)
  call srffti (mz,wz)
!
  do k=izs,ize
  do j=1,my1
   fx=rr(:,j,k)
   call srfftb (mx,fx,wx)
   r(:,j,k)=fx
  end do
  end do
!
  call barrier_omp ('fft2deb')                                          ! different //-direction
  ixs = 1 + ((omp_mythread  )*mx)/omp_nthreads
  ixe =     ((omp_mythread+1)*mx)/omp_nthreads
  !print *,'thread, ixs, ize:', omp_mythread, ixs, ixe
  do i=ixs,ixe
  do j=1,my1
    fz=r(i,j,:)
    call srfftb (mz,fz,wz)
    r(i,j,:)=fz
  end do
  end do
  call barrier_omp ('fft2deb')                                          ! different //-direction
!
END SUBROUTINE
