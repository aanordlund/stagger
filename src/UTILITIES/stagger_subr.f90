! $Id: stagger_subr.f90,v 1.2 2007/07/12 11:34:43 aake Exp $
!***********************************************************************
SUBROUTINE xup_set (f, fp, mx, my, mz)
!
!  f is centered on (i-.5,j,k), fifth order stagger
!
  implicit none
  integer mx, my, mz
  real a, b, c, a1st
  integer i, j, k, izs, ize
  real, dimension(mx,my,mz):: f, fp
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = f
    return
  end if
  c = 3./256.
  b = -25./256.
  a = .5-b-c
  a1st = 0.5
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
   do j=1,my
    fp(mx-2,j,k) = ( &
                   a*(f(mx-2,j,k)+f(mx-1,j,k)) &
                 + b*(f(mx-3,j,k)+f(mx  ,j,k)) &
                 + c*(f(mx-4,j,k)+f(1   ,j,k)))
    fp(mx-1,j,k) = ( &
                   a*(f(mx-1,j,k)+f(mx  ,j,k)) &
                 + b*(f(mx-2,j,k)+f(1   ,j,k)) &
                 + c*(f(mx-3,j,k)+f(2   ,j,k)))
    fp(mx  ,j,k) = ( &
                   a*(f(mx  ,j,k)+f(1   ,j,k)) &
                 + b*(f(mx-1,j,k)+f(2   ,j,k)) &
                 + c*(f(mx-2,j,k)+f(3   ,j,k)))
    fp(1   ,j,k) = ( &
                   a*(f(1   ,j,k)+f(2   ,j,k)) &
                 + b*(f(mx  ,j,k)+f(3   ,j,k)) &
                 + c*(f(mx-1,j,k)+f(4   ,j,k)))
    fp(2   ,j,k) = ( &
                   a*(f(2   ,j,k)+f(3   ,j,k)) &
                 + b*(f(1   ,j,k)+f(4   ,j,k)) &
                 + c*(f(mx  ,j,k)+f(5   ,j,k)))
   end do
   do j=1,my
    do i=3,mx-3
      fp(i  ,j,k) = ( &
                    a*(f(i  ,j,k)+f(i+1 ,j,k)) &
                  + b*(f(i-1,j,k)+f(i+2 ,j,k)) &
                  + c*(f(i-2,j,k)+f(i+3 ,j,k)))
    end do
   end do
  end do
END SUBROUTINE

!***********************************************************************
SUBROUTINE xup1_set (f, fp, mx, my, mz)
!
!  f is centered on (i-.5,j,k), fifth order stagger
!
  implicit none
  integer mx, my, mz
  real a, b, c, a1st
  integer i, j, k, izs, ize
  real, dimension(mx,my,mz):: f, fp
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = f
    return
  end if
!
  a = .5
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
   do j=1,my
    fp(mx  ,j,k) = ( &
                   a*(f(mx  ,j,k)+f(1   ,j,k)))
   end do
   do j=1,my
    do i=1,mx-1
      fp(i  ,j,k) = ( &
                    a*(f(i  ,j,k)+f(i+1 ,j,k)))
    end do
   end do
  end do
END SUBROUTINE

!***********************************************************************
SUBROUTINE xdn_set (f, fp, mx, my, mz)
!
!  f is centered on (i,j,k), fifth order stagger
!
  implicit none
  integer mx, my, mz
  real a, b, c, a1st
  integer i, j, k, izs, ize
  real, dimension(mx,my,mz):: f, fp
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = f
    return
  end if
!
  c = 3./256.
  b = -25./256.
  a = .5-b-c
  a1st = 0.5
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
   do j=1,my
    fp(mx-1,j,k) = ( &
                   a*(f(mx-2,j,k)+f(mx-1,j,k)) &
                 + b*(f(mx-3,j,k)+f(mx  ,j,k)) &
                 + c*(f(mx-4,j,k)+f(1   ,j,k)))
    fp(mx  ,j,k) = ( &
                   a*(f(mx-1,j,k)+f(mx  ,j,k)) &
                 + b*(f(mx-2,j,k)+f(1   ,j,k)) &
                 + c*(f(mx-3,j,k)+f(2   ,j,k)))
    fp(1  ,j,k) = ( &
                   a*(f(mx  ,j,k)+f(1   ,j,k)) &
                 + b*(f(mx-1,j,k)+f(2   ,j,k)) &
                 + c*(f(mx-2,j,k)+f(3   ,j,k)))
    fp(2  ,j,k) = ( &
                   a*(f(1   ,j,k)+f(2   ,j,k)) &
                 + b*(f(mx  ,j,k)+f(3   ,j,k)) &
                 + c*(f(mx-1,j,k)+f(4   ,j,k)))
    fp(3  ,j,k) = ( &
                   a*(f(2   ,j,k)+f(3   ,j,k)) &
                 + b*(f(1   ,j,k)+f(4   ,j,k)) &
                 + c*(f(mx  ,j,k)+f(5   ,j,k)))
   end do
   do j=1,my
    do i=4,mx-2
      fp(i,j,k) = ( &
                   a*(f(i   ,j,k)+f(i-1 ,j,k)) &
                 + b*(f(i+1 ,j,k)+f(i-2 ,j,k)) &
                 + c*(f(i+2 ,j,k)+f(i-3 ,j,k)))
    end do
   end do
  end do
END SUBROUTINE

!***********************************************************************
SUBROUTINE xdn1_set (f, fp, mx, my, mz)
!
!  f is centered on (i,j,k), fifth order stagger
!
  implicit none
  integer mx, my, mz
  real a, b, c, a1st
  integer i, j, k, izs, ize
  real, dimension(mx,my,mz):: f, fp
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = f
    return
  end if
!
  a = 0.5
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
   do j=1,my
    fp(1  ,j,k) = ( &
                   a*(f(mx  ,j,k)+f(1   ,j,k)))
   end do
   do j=1,my
    do i=2,mx
      fp(i,j,k) = ( &
                   a*(f(i   ,j,k)+f(i-1 ,j,k)))
    end do
   end do
  end do
END SUBROUTINE

!***********************************************************************
SUBROUTINE yup_set (f, fp, mx, my, mz)
!
!  f is centered on (i,j-.5,k), fifth order stagger
!
  implicit none
  integer mx, my, mz
  real a, b, c, a1st
  integer i, j, k, izs, ize
  real, intent(in) , dimension(mx,my,mz):: f
  real,              dimension(mx,my,mz):: fp
!-----------------------------------------------------------------------
!
  if (my .lt. 5) then
    call limits_omp(1,mz,izs,ize)
    do k=izs,ize
      fp(:,:,k) = f(:,:,k)
    end do
    return
  end if
  c = 3./256.
  b = -25./256.
  a = .5-b-c
  a1st = 0.5
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    do i=1,mx
      fp(i,my-2,k) = ( &
                     a*(f(i,my-2,k)+f(i,my-1,k)) + &
                     b*(f(i,my-3,k)+f(i,my  ,k)) + &
                     c*(f(i,my-4,k)+f(i,1   ,k)))
      fp(i,my-1,k) = ( &
                     a*(f(i,my-1,k)+f(i,my  ,k)) + &
                     b*(f(i,my-2,k)+f(i,1   ,k)) + &
                     c*(f(i,my-3,k)+f(i,2   ,k)))
      fp(i,my  ,k) = ( &
                     a*(f(i,my  ,k)+f(i,1   ,k)) + &
                     b*(f(i,my-1,k)+f(i,2   ,k)) + &
                     c*(f(i,my-2,k)+f(i,3   ,k)))
      fp(i,1   ,k) = ( &
                     a*(f(i,1   ,k)+f(i,2   ,k)) + &
                     b*(f(i,my  ,k)+f(i,3   ,k)) + &
                     c*(f(i,my-1,k)+f(i,4   ,k)))
      fp(i,2   ,k) = ( &
                     a*(f(i,2   ,k)+f(i,3   ,k)) + &
                     b*(f(i,1   ,k)+f(i,4   ,k)) + &
                     c*(f(i,my  ,k)+f(i,5   ,k)))
    end do
    do j=3,my-3
     do i=1,mx
      fp(i,j  ,k) = ( &
                     a*(f(i,j   ,k)+f(i,j+1 ,k)) + &
                     b*(f(i,j-1 ,k)+f(i,j+2 ,k)) + &
                     c*(f(i,j-2 ,k)+f(i,j+3 ,k)))
     end do
    end do
  end do
END SUBROUTINE

!***********************************************************************
SUBROUTINE yup1_set (f, fp, mx, my, mz)
!
!  f is centered on (i,j-.5,k), fifth order stagger
!
  implicit none
  integer mx, my, mz
  real a, b, c, a1st
  integer i, j, k, izs, ize
  real, intent(in) , dimension(mx,my,mz):: f
  real,              dimension(mx,my,mz):: fp
!-----------------------------------------------------------------------
!
  if (my .lt. 5) then
    call limits_omp(1,mz,izs,ize)
    do k=izs,ize
      fp(:,:,k) = f(:,:,k)
    end do
    return
  end if
!
  a = 0.5
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    do i=1,mx
      fp(i,my,k) = ( &
                     a*(f(i,my,k)+f(i,1,k)))
    end do
    do j=1,my-1
      do i=1,mx
        fp(i,j  ,k) = ( &
                     a*(f(i,j   ,k)+f(i,j+1 ,k)))
      end do
    end do
  end do
END SUBROUTINE

!***********************************************************************
SUBROUTINE ydn_set (f, fp, mx, my, mz)
!
!  f is centered on (i,j,k), fifth order stagger
!
  implicit none
  integer mx, my, mz
  real a, b, c, a1st
  integer i, j, k, izs, ize
  real, dimension(mx,my,mz):: f, fp
!
!-----------------------------------------------------------------------
  if (my .lt. 5) then
    call limits_omp(1,mz,izs,ize)
    do k=izs,ize
      fp(:,:,k) = f(:,:,k)
    end do
    return
  end if
  c = 3./256.
  b = -25./256.
  a = .5-b-c
  a1st = 0.5
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    do i=1,mx
      fp(i,my-1,k) = ( &
                     a*(f(i,my-2,k)+f(i,my-1,k)) + &
                     b*(f(i,my-3,k)+f(i,my  ,k)) + &
                     c*(f(i,my-4,k)+f(i,1   ,k)))
      fp(i,my  ,k) = ( &
                     a*(f(i,my-1,k)+f(i,my  ,k)) + &
                     b*(f(i,my-2,k)+f(i,1   ,k)) + &
                     c*(f(i,my-3,k)+f(i,2   ,k)))
      fp(i,1   ,k) = ( &
                     a*(f(i,my  ,k)+f(i,1   ,k)) + &
                     b*(f(i,my-1,k)+f(i,2   ,k)) + &
                     c*(f(i,my-2,k)+f(i,3   ,k)))
      fp(i,2   ,k) = ( &
                     a*(f(i,1   ,k)+f(i,2   ,k)) + &
                     b*(f(i,my  ,k)+f(i,3   ,k)) + &
                     c*(f(i,my-1,k)+f(i,4   ,k)))
      fp(i,3   ,k) = ( &
                     a*(f(i,2   ,k)+f(i,3   ,k)) + &
                     b*(f(i,1   ,k)+f(i,4   ,k)) + &
                     c*(f(i,my  ,k)+f(i,5   ,k)))
    end do
    do j=4,my-2
     do i=1,mx
      fp(i,j,k) = ( &
                     a*(f(i,j   ,k)+f(i,j-1 ,k)) + &
                     b*(f(i,j+1 ,k)+f(i,j-2 ,k)) + &
                     c*(f(i,j+2 ,k)+f(i,j-3 ,k)))
     end do
    end do
  end do
END SUBROUTINE

!***********************************************************************
SUBROUTINE ydn1_set (f, fp, mx, my, mz)
!
!  f is centered on (i,j,k), fifth order stagger
!
  implicit none
  integer mx, my, mz
  real a, b, c, a1st
  integer i, j, k, izs, ize
  real, dimension(mx,my,mz):: f, fp
!
!-----------------------------------------------------------------------
  if (my .lt. 5) then
    call limits_omp(1,mz,izs,ize)
    do k=izs,ize
      fp(:,:,k) = f(:,:,k)
    end do
    return
  end if
!
  a = 0.5
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    do i=1,mx
      fp(i,1   ,k) = ( &
                     a*(f(i,my  ,k)+f(i,1   ,k)))
    end do
    do j=2,my
     do i=1,mx
      fp(i,j,k) = ( &
                     a*(f(i,j   ,k)+f(i,j-1 ,k)))
     end do
    end do
  end do
END SUBROUTINE

!***********************************************************************
SUBROUTINE zup_set (f, fp, mx, my, mz)
!
!  f is centered on (i,j,k-.5), fifth order stagger
!
  implicit none
  integer mx, my, mz
  real a, b, c, a1st
  integer i, j, k, km2, km1, kp1, kp2, kp3, izs, ize
  real, dimension(mx,my,mz):: f, fp
!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    fp = f
    return
  end if
  c = 3./256.
  b = -25./256.
  a = .5-b-c
  a1st = 0.5
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    km2=mod(k-3+mz,mz)+1
    km1=mod(k-2+mz,mz)+1
    kp1=mod(k     ,mz)+1
    kp2=mod(k+1   ,mz)+1
    kp3=mod(k+2   ,mz)+1
    do j=1,my
      do i=1,mx
        fp(i,j,k) = ( &
                     a*(f(i,j,kp1)+f(i,j,k  )) + &
                     b*(f(i,j,kp2)+f(i,j,km1)) + &
                     c*(f(i,j,kp3)+f(i,j,km2)))
      end do
    end do
  end do
END SUBROUTINE

!***********************************************************************
SUBROUTINE zup1_set (f, fp, mx, my, mz)
!
!  f is centered on (i,j,k-.5), fifth order stagger
!
  implicit none
  integer mx, my, mz
  real a, b, c, a1st
  integer i, j, k, kp1, izs, ize
  real, dimension(mx,my,mz):: f, fp
!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    fp = f
    return
  end if
  a = .5
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    kp1=mod(k     ,mz)+1
    do j=1,my
      do i=1,mx
        fp(i,j,k) = ( &
                     a*(f(i,j,kp1)+f(i,j,k  )))
      end do
    end do
  end do
END SUBROUTINE

!***********************************************************************
SUBROUTINE zdn_set (f, fp, mx, my, mz)
!
!  f is centered on (i,j,k), fifth order stagger
!
  implicit none
  integer mx, my, mz
  real a, b, c, a1st
  integer i, j, k, km3, km2, km1, kp1, kp2, izs, ize
  real, dimension(mx,my,mz):: f, fp
!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    fp = f
    return
  end if
  c = 3./256.
  b = -25./256.
  a = .5-b-c
  a1st = 0.5
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    km3=mod(k-4+mz,mz)+1
    km2=mod(k-3+mz,mz)+1
    km1=mod(k-2+mz,mz)+1
    kp1=mod(k     ,mz)+1
    kp2=mod(k+1   ,mz)+1
    do j=1,my
      do i=1,mx
        fp(i,j,k) = ( &
                     a*(f(i,j,k  )+f(i,j,km1)) + &
                     b*(f(i,j,kp1)+f(i,j,km2)) + &
                     c*(f(i,j,kp2)+f(i,j,km3)))
      end do
    end do
  end do
END SUBROUTINE

!***********************************************************************
SUBROUTINE zdn1_set (f, fp, mx, my, mz)
!
!  f is centered on (i,j,k), fifth order stagger
!
  implicit none
  integer mx, my, mz
  real a, b, c, a1st
  integer i, j, k, km1, izs, ize
  real, dimension(mx,my,mz):: f, fp
!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    fp = f
    return
  end if
  a = .5
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    km1=mod(k-2+mz,mz)+1
    do j=1,my
      do i=1,mx
        fp(i,j,k) = ( &
                     a*(f(i,j,k  )+f(i,j,km1)))
      end do
    end do
  end do
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddxdn1_set (f, fp, mx, my, mz)
  implicit none
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = 0.
    return
  end if
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
   do j=1,my
    fp(1,j,k) = f(1,j,k)-f(mx,j,k)
   end do
   do j=1,my
    do i=2,mx
      fp(i,j,k) = f(i,j,k)-f(i-1,j,k)
    end do
   end do
  end do
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddxdn1_add (f, fp, mx, my, mz)
  implicit none
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = 0.
    return
  end if
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
   do j=1,my
    fp(1,j,k) = fp(1,j,k) + (f(1,j,k)-f(mx,j,k))
   end do
   do j=1,my
    do i=2,mx
      fp(i,j,k) = fp(i,j,k) + (f(i,j,k)-f(i-1,j,k))
    end do
   end do
  end do
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddxdn1_sub (f, fp, mx, my, mz)
  implicit none
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = 0.
    return
  end if
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
   do j=1,my
    fp(1,j,k) = fp(1,j,k) - (f(1,j,k)-f(mx,j,k))
   end do
   do j=1,my
    do i=2,mx
      fp(i,j,k) = fp(i,j,k) - (f(i,j,k)-f(i-1,j,k))
    end do
   end do
  end do
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddxup1_set (f, fp, mx, my, mz)
  implicit none
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = 0.
    return
  end if
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
   do j=1,my
    fp(mx,j,k) = f(1,j,k)-f(mx,j,k)
   end do
   do j=1,my
    do i=1,mx-1
      fp(i,j,k) = f(i+1,j,k)-f(i,j,k)
    end do
   end do
  end do
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddxup1_add (f, fp, mx, my, mz)
  implicit none
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    return
  end if
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
   do j=1,my
    fp(mx,j,k) = fp(mx,j,k) + (f(1,j,k)-f(mx,j,k))
   end do
   do j=1,my
    do i=1,mx-1
      fp(i,j,k) = fp(i,j,k) + (f(i+1,j,k)-f(i,j,k))
    end do
   end do
  end do
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddxup1_sub (f, fp, mx, my, mz)
  implicit none
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    return
  end if
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
   do j=1,my
    fp(mx,j,k) = fp(mx,j,k) - (f(1,j,k)-f(mx,j,k))
   end do
   do j=1,my
    do i=1,mx-1
      fp(i,j,k) = fp(i,j,k) - (f(i+1,j,k)-f(i,j,k))
    end do
   end do
  end do
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddydn1_set (f, fp, mx, my, mz)
  implicit none
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!-----------------------------------------------------------------------
!
  if (my .lt. 3) then
    fp = 0.
    return
  end if
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    do i=1,mx
      fp(i,1,k) = (f(i,1,k)-f(i,my,k))
    end do
    do j=2,my
     do i=1,mx
      fp(i,j ,k) = (f(i,j,k)-f(i,j-1,k))
     end do
    end do
  end do
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddydn1_add (f, fp, mx, my, mz)
  implicit none
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!-----------------------------------------------------------------------
!
  if (my .lt. 3) then
    fp = 0.
    return
  end if
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    do i=1,mx
      fp(i,1,k) = fp(i,1,k) + (f(i,1,k)-f(i,my,k))
    end do
    do j=2,my
     do i=1,mx
      fp(i,j ,k) = fp(i,j ,k) + (f(i,j,k)-f(i,j-1,k))
     end do
    end do
  end do
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddydn1_sub (f, fp, mx, my, mz)
  implicit none
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!-----------------------------------------------------------------------
!
  if (my .lt. 3) then
    fp = 0.
    return
  end if
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    do i=1,mx
      fp(i,1,k) = fp(i,1,k) - (f(i,1,k)-f(i,my,k))
    end do
    do j=2,my
     do i=1,mx
      fp(i,j ,k) = fp(i,j ,k) - (f(i,j,k)-f(i,j-1,k))
     end do
    end do
  end do
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddyup1_set (f, fp, mx, my, mz)
  implicit none
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!-----------------------------------------------------------------------
!
  if (my .lt. 3) then
    fp = 0.
    return
  end if
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    do i=1,mx
      fp(i,my,k) = (f(i,1,k)-f(i,my,k))
    end do
    do j=1,my-1
     do i=1,mx
      fp(i,j,k) = (f(i,j+1,k)-f(i,j,k))
     end do
    end do
  end do
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddyup1_add (f, fp, mx, my, mz)
  implicit none
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!-----------------------------------------------------------------------
!
  if (my .lt. 3) then
    fp = 0.
    return
  end if
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    do i=1,mx
      fp(i,my,k) = fp(i,my,k) + (f(i,1,k)-f(i,my,k))
    end do
    do j=1,my-1
     do i=1,mx
      fp(i,j,k) = fp(i,j,k) + (f(i,j+1,k)-f(i,j,k))
     end do
    end do
  end do
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddyup1_sub (f, fp, mx, my, mz)
  implicit none
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!-----------------------------------------------------------------------
!
  if (my .lt. 3) then
    fp = 0.
    return
  end if
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    do i=1,mx
      fp(i,my,k) = fp(i,my,k) - (f(i,1,k)-f(i,my,k))
    end do
    do j=1,my-1
     do i=1,mx
      fp(i,j,k) = fp(i,j,k) - (f(i,j+1,k)-f(i,j,k))
     end do
    end do
  end do
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddzdn1_set (f, fp, mx, my, mz)
  implicit none
  integer i, j, k, km1, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    fp = 0.
    return
  end if
!
  !$omp barrier
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    km1=mod(k-2+mz,mz)+1
    do j=1,my
      do i=1,mx
        fp(i,j,k) = (f(i,j,k  )-f(i,j,km1))
      end do
    end do
  end do
  !$omp barrier
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddzdn1_add (f, fp, mx, my, mz)
  implicit none
  integer i, j, k, km1, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    fp = 0.
    return
  end if
!
  !$omp barrier
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    km1=mod(k-2+mz,mz)+1
    do j=1,my
      do i=1,mx
        fp(i,j,k) = fp(i,j,k) + (f(i,j,k  )-f(i,j,km1))
      end do
    end do
  end do
  !$omp barrier
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddzdn1_sub (f, fp, mx, my, mz)
  implicit none
  integer i, j, k, km1, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    fp = 0.
    return
  end if
!
  !$omp barrier
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    km1=mod(k-2+mz,mz)+1
    do j=1,my
      do i=1,mx
        fp(i,j,k) = fp(i,j,k) - (f(i,j,k  )-f(i,j,km1))
      end do
    end do
  end do
  !$omp barrier
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddzup1_set (f, fp, mx, my, mz)
  implicit none
  integer i, j, k, km1, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    fp = 0.
    return
  end if
!
  !$omp barrier
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    km1=mod(k-2+mz,mz)+1
    do j=1,my
      do i=1,mx
        fp(i,j,km1) = (f(i,j,k  )-f(i,j,km1))
      end do
    end do
  end do
  !$omp barrier
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddzup1_add (f, fp, mx, my, mz)
  implicit none
  integer i, j, k, km1, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    fp = 0.
    return
  end if
!
  !$omp barrier
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    km1=mod(k-2+mz,mz)+1
    do j=1,my
      do i=1,mx
        fp(i,j,km1) = fp(i,j,km1) + (f(i,j,k  )-f(i,j,km1))
      end do
    end do
  end do
  !$omp barrier
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddzup1_sub (f, fp, mx, my, mz)
  implicit none
  integer i, j, k, km1, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    fp = 0.
    return
  end if
!
  !$omp barrier
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    km1=mod(k-2+mz,mz)+1
    do j=1,my
      do i=1,mx
        fp(i,j,km1) = fp(i,j,km1) - (f(i,j,k  )-f(i,j,km1))
      end do
    end do
  end do
  !$omp barrier
END SUBROUTINE
