!$Id: main_divb_clean_atm.f90,v 1.13 2016/11/09 22:04:57 aake Exp $
!***********************************************************************
SUBROUTINE limits_omp (n1,n2,i1,i2)
  implicit none
  integer n1,n2,i1,i2
  integer omp_get_num_threads, omp_get_thread_num, omp_mythread, omp_nthreads

#ifdef _OPENMP
  omp_mythread = omp_get_thread_num()
  omp_nthreads = omp_get_num_threads()
  i1 = n1 + ((omp_mythread  )*(n2-n1+1))/omp_nthreads
  i2 = n1 + ((omp_mythread+1)*(n2-n1+1))/omp_nthreads - 1
  !print *,'limits_omp:',omp_mythread,omp_nthreads,i1,i2
#else
  omp_mythread = 0
  i1 = n1
  i2 = n2
  print *,'limits_omp: scalar'
#endif
END

!***********************************************************************
PROGRAM divb_clean_atm

  implicit none
  real, parameter:: pi=3.14159265
  real, allocatable, dimension(:,:,:):: bx,by,bz,divb,dphit,scr
  real, allocatable, dimension(:,:):: w,k2
  real, allocatable, dimension(:):: b,xm,ym,zm,dxidxdn,dxidxup,dyidydn,dyidyup,dzidzdn,dzidzup
  integer mvar,isnap,mx,my,mz,izs,ize,lb,ub,iys,iye
  integer iter,niter,ibx,ix,iy,iz,io_word
  real divmin,divmax,eps,wt,wallclock,c,over,dy0,dy1,dy2,dx,dz,sx,sz
  character(len=1) answer
  character(len=40) file,meshfile
  namelist /in/file,meshfile,mvar,lb,ub,ibx,isnap,niter,eps
  namelist /grid/mx,my,mz,lb,ub

  file='snapshot.dat'                                                   ! snapshot file
  meshfile='mesh.dat'                                                   ! binary mesh file
  mvar=9                                                                ! fields per snapshot
  ibx=6                                                                 ! Bx index in file
  isnap=0                                                               ! snapshot number

  niter=10                                                              ! max iterations
  eps=1e-4                                                              ! accuracy request
  lb = 6
  ub = lb

  write (*,in)
  read (*,in)
  write (*,in)

  open (2,file=meshfile,status='old',form='unformatted')
  read (2) mx,my,mz
  ub = my-ub+1
  write (*,grid)

  allocate (bx(mx,my,mz), by(mx,my,mz), bz(mx,my,mz))
  allocate (divb(mx,my,mz), dphit(mx,my,mz), scr(mx,my,mz))
  allocate (k2(mx,mz))
  allocate (xm(mx),dxidxdn(mx),dxidxup(mx))
  allocate (ym(my),dyidydn(my),dyidyup(my))
  allocate (zm(mz),dzidzdn(mz),dzidzup(mz))

  read (2) xm,xm,xm,dxidxup,dxidxup,dxidxdn
  read (2) ym,ym,ym,dyidyup,dyidyup,dyidydn
  read (2) zm,zm,zm,dzidzup,dzidzup,dzidzdn
  close (2)

  dx=xm(2)-xm(1)
  dz=zm(2)-zm(1)

  open (1,file=file,access='direct',status='unknown',recl=io_word()*mx*my*mz)

  wt=wallclock()
  print *,'reading bx'
  read (1,rec=mvar*isnap+ibx+1) bx
  print *,'reading by'
  read (1,rec=mvar*isnap+ibx+2) by
  print *,'reading bz'
  read (1,rec=mvar*isnap+ibx+3) bz
  print *,wallclock(),' sec'

  do iz=1,mz
    dphit(:,:,iz) = 0.
  end do

  !c=alog(over)/(3.*pi**2)
  !print *,'cleaning: c =',c
  do iter=1,niter
    !print*,'loop start',iter
    !$omp parallel private (ix,iy,iz,izs,ize)
    call ddxupi_set (bx, scr, mx, my, mz)
    call limits_omp(1,mz,izs,ize)
    do iz=izs,ize
      do iy=lb,ub
        divb(:,iy,iz) = scr(:,iy,iz)*dxidxup
      end do
    end do
    call ddyupi_set (by, scr, mx, my, mz)
    do iz=izs,ize
      do iy=lb,ub
      do ix=1,mx
        divb(ix,iy,iz) = divb(ix,iy,iz) + scr(ix,iy,iz)*dyidyup(iy)
      end do
      end do
    end do
    call ddzupi_set (bz, scr, mx, my, mz)
    do iz=izs,ize
      divb(:,lb:ub,iz) = divb(:,lb:ub,iz) + scr(:,lb:ub,iz)*dzidzup(iz)
    end do
    !$omp end parallel

    !print*,'maxval'
    divmax = maxval(abs(divb))
    wt=wallclock()
    print '(i3,1p,1e15.6,0p,f10.2,a)',iter,divmax,wt,' sec'
    if (divmax < eps) exit

    !print*,'fft2df'
    !$omp parallel private(iy,iys,iye)
    call limits_omp(lb,ub,iys,iye)
    do iy=iys,iye
      call fft2df (divb(:,iy,:),dphit(:,iy,:),mx,mz)
    end do
    !$omp end parallel

    !print*,'fft2d_k2'
    sx = mx*dx
    sz = mz*dz
    call fft2d_k2(k2,sx,sz,mx,mz)

    !$omp parallel private(ix,iy,iz,dy0,dy1,dy2,w,b,izs,ize)
    allocate (w(my,3),b(my))
    w = 0.
    b = 0.
    call limits_omp(1,mz,izs,ize)
    do iz=izs,ize                                                       ! loop over all ..
    do ix=1,mx                                                          ! .. Fourier components

      do iy=lb,ub                                                       ! tridiagonal matrix elements
	dy1 = ym(iy)-ym(iy-1)
	dy2 = ym(iy+1)-ym(iy)
	dy0 = 0.5*(dy1+dy2)
        w(iy,1) = 1./(dy1*dy0)
        w(iy,3) = 1./(dy2*dy0)
        w(iy,2) = -w(iy,1)-w(iy,3)-k2(ix,iz)
	b(iy) = dphit(ix,iy,iz)                                         ! right hand side; FT of div(B)
      end do

      do iy=lb+1,ub                                                     ! forward loop
        w(iy,1) = -w(iy,1)/w(iy-1,2)
	w(iy,2) = w(iy,2) + w(iy,1)*w(iy-1,3)                           ! eliminate sub-diagonal
	b(iy)   = b(iy)   + w(iy,1)*b(iy-1)                             ! accumulate RHS
      end do

      do iy=ub,lb,-1                                                    ! reverse loop
        b(iy) = (b(iy)-w(iy,3)*b(iy+1))/w(iy,2)                         ! back substitute
	dphit(ix,iy,iz) = b(iy)                                         ! FT of phi such that Laplace(phi)=div(B)
      end do
    end do
    end do
    deallocate (w,b)
    !$omp end parallel

    !print*,'call fft2db'
    !$omp parallel private(iy,iys,iye)
    call limits_omp(lb,ub,iys,iye)
    do iy=iys,iye
      call fft2db (dphit(:,iy,:),divb(:,iy,:),mx,mz)
    end do
    !$omp end parallel

    !print*,'call ddxdni_set'
    !$omp parallel private (ix,iy,iz,izs,ize)
    call ddxdni_set (divb, scr, mx, my, mz)
    call limits_omp(1,mz,izs,ize)
    do iz=izs,ize
      bx(:,lb:ub,iz) = bx(:,lb:ub,iz) - scr(:,lb:ub,iz)*(1./dx)
    end do
    call ddydni_set (divb, scr, mx, my, mz)
    do iz=izs,ize
      do iy=1,my
      do ix=1,mx
        by(ix,iy,iz) = by(ix,iy,iz) - scr(ix,iy,iz)*dyidydn(iy)
      end do
      end do
    end do
    call ddzdni_set (divb, scr, mx, my, mz)
    do iz=izs,ize
      bz(:,lb:ub,iz) = bz(:,lb:ub,iz) - scr(:,lb:ub,iz)*(1./dz)
    end do
    !$omp end parallel
    !print*,'loop done'

  end do

  answer = "y"
  print *,'commit to file (y)?'
  read *,answer
  if (answer == "y") then
    print *,'writing bx'
    close (1)
    open (1,file=file,access='direct',status='unknown',recl=io_word()*mx*my)
    do iz=1,mz
      write (1,rec=(mvar*isnap+ibx+0)*mz+iz) bx(:,:,iz)
    end do
    print *,'writing by'
    do iz=1,mz
      write (1,rec=(mvar*isnap+ibx+1)*mz+iz) by(:,:,iz)
    end do
    print *,'writing bz'
    do iz=1,mz
      write (1,rec=(mvar*isnap+ibx+2)*mz+iz) bz(:,:,iz)
    end do
    print *,'commmitted'
  else
    print *,'NOT commmitted'
    stop
  end if
  print *,wallclock(),' sec'

  close (1)
  deallocate (bx,by,bz,divb,dphit,k2)
END

