! $Id: test_helmh.f90,v 1.4 2006/04/11 20:53:15 aake Exp $
!-----------------------------------------------------------------------
PROGRAM test_helmh
  implicit none
  integer mx, my, mz
  real, allocatable, dimension(:,:,:) :: fx, fy, fz
  real kx, ky, kz, error, fmax
  real dtime, cpu(2), void
  real, parameter:: pi=3.14159265
  integer i, j, k

  print *, 'mx=my=mz?'
  read *, mx
  my=mx
  mz=mx

  allocate (fx(mx,my,mz), fy(mx,my,mz), fz(mx,my,mz))
  kx = 2.*pi/mx
  ky = 2.*pi/my
  kz = 2.*pi/mz

  void = dtime(cpu)
  !$omp parallel do private(i,j,k), reduction(max:fmax)
  do k=1,mz
  do j=1,my
  do i=1,mx
    fx(i,j,k) = 2.*(cos(i*kx)+cos(j*ky)+cos(k*kz))
    fy(i,j,k) = 3.*(cos(i*kx)+cos(j*ky)+cos(k*kz))
    fz(i,j,k) = 5.*(cos(i*kx)+cos(j*ky)+cos(k*kz))
    fmax = max(fmax,abs(fx(i,j,k)),abs(fy(i,j,k)),abs(fz(i,j,k)))
  end do
  end do
  end do
  print *, 'max f =', fmax
  print *, 'setup:', dtime(cpu)
  
  call helmh (fx, fy, fz, mx, my, mz)
  print *, 'helmh:', dtime(cpu)

  fmax = 0.
  error = 0.
  !$omp parallel do private(i,j,k), reduction(max:error,fmax)
  do k=1,mz
  do j=1,my
  do i=1,mx
    fmax = max(fmax,abs(fx(i,j,k)),abs(fy(i,j,k)),abs(fz(i,j,k)))
    fx(i,j,k) = fx(i,j,k) - 2.*(          cos(j*ky)+cos(k*kz))
    fy(i,j,k) = fy(i,j,k) - 3.*(cos(i*kx)          +cos(k*kz))
    fz(i,j,k) = fz(i,j,k) - 5.*(cos(i*kx)+cos(j*ky)          )
    error = max(error,abs(fx(i,j,k)),abs(fy(i,j,k)),abs(fz(i,j,k)))
  end do
  end do
  end do
  print *, 'eval:', dtime(cpu)
  print *, 'max f =', fmax
  print *, 'max error =', error/fmax
  
  deallocate (fx, fy, fz)
END
