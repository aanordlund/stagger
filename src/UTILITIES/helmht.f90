! $Id: helmht.f90,v 1.2 2007/03/31 22:35:14 aake Exp $
!***********************************************************************
SUBROUTINE helmht (tx, ty, tz, mx, my, mz)

!  Helmholtz projection, to compute the divergence free part
!  of the transform {tx,ty,tz} of a vector field {fx,fy,fz}

  implicit none
  integer mx, my, mz
  real, dimension(mx,my,mz):: tx, ty, tz
  integer i, j, k, izs, ize, omp_get_thread_num, omp_get_num_threads
  real kx, ky, kz, corr

  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    kz = k/2
    do j=1,my
      ky = j/2
      do i=1,mx
        kx = i/2
        corr = tx(i,j,k)*kx + ty(i,j,k)*ky + tz(i,j,k)*kz
        corr = corr/(kx**2+ky**2+kz**2+1e-10)    
        tx(i,j,k) = tx(i,j,k) - corr*kx
        ty(i,j,k) = ty(i,j,k) - corr*ky
        tz(i,j,k) = tz(i,j,k) - corr*kz
      end do
    end do
  end do
!$omp barrier

END
