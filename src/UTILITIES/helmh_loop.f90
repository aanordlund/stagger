SUBROUTINE helmh (fx, fy, fz, mx, my, mz)
  implicit none
  integer mx, my, mz
  real, dimension(mx,my,mz):: fx, fy, fz
  real, allocatable, dimension(:,:,:) :: tx, ty, tz
  integer i, j, k
  real kx, ky, kz, corr

  allocate (tx(mx,my,mz), ty(mx,my,mz), tz(mx,my,mz))

  call fft3df (fx, tx, mx, my, mz)
  call fft3df (fy, ty, mx, my, mz)
  call fft3df (fz, tz, mx, my, mz)
  !print *,tx(2,1,1),ty(1,2,1),tz(1,1,2)

  !$omp parallel do private(i,j,k,kx,ky,kz,corr)
  do k=1,mz
    kz = k/2
    do j=1,my
      ky = j/2
      do i=1,mx
        kx = i/2
        corr = tx(i,j,k)*kx + ty(i,j,k)*ky + tz(i,j,k)*kz
        corr = corr/(kx**2+ky**2+kz**2+1e-10)    
        tx(i,j,k) = tx(i,j,k) - corr*kx
        ty(i,j,k) = ty(i,j,k) - corr*ky
        tz(i,j,k) = tz(i,j,k) - corr*kz
      end do
    end do
  end do

  call fft3db (tx, fx, mx, my, mz)
  call fft3db (ty, fy, mx, my, mz)
  call fft3db (tz, fz, mx, my, mz)

  deallocate (tx, ty, tz)
END
