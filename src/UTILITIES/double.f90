! $Id: double.f90,v 1.10 2016/11/09 22:04:57 aake Exp $
!******************************************************************************
MODULE double_mod
  implicit none
  integer, save:: verbose=0
CONTAINS
!-------------------------------------------------------------------------------
SUBROUTINE xupdn_set (f, fp, mx, my, mz, l)
  implicit none
  integer mx, my, mz, l
  real a, b, c, a1st
  integer i, j, k, izs, ize, im3, im2, im1, ip0, ip1, ip2
  real, dimension(mx,my,mz):: f, fp
!...............................................................................
  c = 3./256.
  b = -25./256.
  c=0.; b=0.
  a = .5-b-c
  !$omp do private(k,i,j,im3,im2,im1,ip0,ip1,ip2)
  do k=1,mz
    do j=1,my
      do i=1,mx
        im3=mod(i+l-4+mx,mx)+1
        im2=mod(i+l-3+mx,mx)+1
        im1=mod(i+l-2+mx,mx)+1
        ip0=mod(i+l-1+mx,mx)+1
        ip1=mod(i+l  +mx,mx)+1
        ip2=mod(i+l+1+mx,mx)+1
        fp(i,j,k) = (a*(f(ip0,j,k)+f(im1,j,k)) + &
                     b*(f(ip1,j,k)+f(im2,j,k)) + &
                     c*(f(ip2,j,k)+f(im3,j,k)))
      end do
    end do
  end do
END SUBROUTINE
!-------------------------------------------------------------------------------
SUBROUTINE zupdn_set (f, fp, mx, my, mz, l)
  implicit none
  integer mx, my, mz, l
  real a, b, c, a1st
  integer i, j, k, km3, km2, km1, kp0, kp1, kp2
  real, dimension(mx,my,mz):: f, fp
!...............................................................................
  c = 3./256.
  b = -25./256.
  c=0.; b=0.
  a = .5-b-c
  !$omp do private(k,i,j,km3,km2,km1,kp0,kp1,kp2)
  do k=1,mz
    km3=mod(k+l-4+mz,mz)+1
    km2=mod(k+l-3+mz,mz)+1
    km1=mod(k+l-2+mz,mz)+1
    kp0=mod(k+l-1+mz,mz)+1
    kp1=mod(k+l  +mz,mz)+1
    kp2=mod(k+l+1+mz,mz)+1
    do j=1,my
      do i=1,mx
        fp(i,j,k) = (a*(f(i,j,kp0)+f(i,j,km1)) + &
                     b*(f(i,j,kp1)+f(i,j,km2)) + &
                     c*(f(i,j,kp2)+f(i,j,km3)))
      end do
    end do
  end do
END SUBROUTINE
!-------------------------------------------------------------------------------
SUBROUTINE double (f1, nx1, ny1, nz1, lb1, ub1, f2, nx2, ny2, nz2, kx, ky, kz, interp, ym1, ym2)
  implicit none
  logical interp
  real, dimension(nx1,ny1,nz1):: f1
  real, dimension(nx2,ny2,nz2):: f2
  real ym1(ny1), ym2(ny2)
  integer nx1, ny1, nz1, lb1, ub1, nx2, ny2, nz2, kx, ky, kz, ix1, iz1
  integer ix, iy, iz, jy, lb2, ub2
  real py, qy, dy2
  real, allocatable, dimension(:,:,:):: f3, f4, f5
!...............................................................................
  lb2 = lb1
  ub2 = ny2-(ny1-ub1)
  dy2 = real(ub1-lb1)/real(ub2-lb2)

  allocate (f3(nx1,ny2,nz1))

  if (interp) then
    !$omp do private(iz,iy,ix,py,jy,qy)
    do iy=1,ny2
      jy=ny1-1
      do while (ym1(jy) > ym2(iy) .and. jy>1)
        jy=jy-1
      enddo
      py = (ym2(iy)-ym1(jy))/(ym1(jy+1)-ym1(jy))       ! from 0. to 1. over interval jy to jy+1
      qy = 1.-py
      do iz=1,nz1
        do ix=1,nx1
          f3(ix,iy,iz) = qy*f1(ix,jy,iz) + py*f1(ix,jy+1,iz)
        end do
      end do
      if (verbose>0) &
        print*,iy,jy,py,sum(f1(:,jy,:))/(nx1*nz1),sum(f3(:,iy,:))/(nx1*nz1)
    end do
  else
    !$omp do private(iz,iy,ix,py,jy,qy)
    do iz=1,nz1
      do iy=1,ny2
        py = lb1 + (iy-lb1-ky*0.5)*dy2 + ky*0.5          ! when ky=0, iy=lb   is mapped to jy=lb
        jy = py                                          ! when ky=1, iy=lb+� is mapped to jy=lb+�
        py = py-jy
        qy = 1.-py
        do ix=1,nx1
          f3(ix,iy,iz) = qy*f1(ix,jy,iz) + py*f1(ix,jy+1,iz)
        end do
      end do
    end do
  endif

  allocate (f4(nx1,ny2,nz1), f5(nx2,ny2,nz1))
  call xupdn_set (f3,f4,nx1,ny2,nz1,1)                 ! 1 => xup, 0 => xdn 

  !$omp do private(iz,iy,ix,py,jy,qy)
  do iz=1,nz1
    do iy=1,ny2
      if (kx == 0) then
        do ix=1,nx1
          f5(ix*2-1,iy,iz) = f3(ix,iy,iz)              ! when kx=1, f4 values come before f3 values
          f5(ix*2  ,iy,iz) = f4(ix,iy,iz)              ! when kx=0, f3 values come before f4 values
        end do
      else
        do ix=1,nx1
          ix1 = mod(ix,nx1) + 1
          f5(ix*2-1,iy,iz) = 0.5*(f3(ix ,iy,iz)+f4(ix,iy,iz))
          f5(ix*2  ,iy,iz) = 0.5*(f3(ix1,iy,iz)+f4(ix,iy,iz))
        end do
      end if
    end do
  end do
  deallocate (f3,f4)

  allocate (f4(nx2,ny2,nz1))
  call zupdn_set (f5,f4,nx2,ny2,nz1,1)                 ! 1 => zup, 0 => zdn 

  !$omp do private(iz,iy,ix,py,jy,qy)
  do iz=1,nz1
    iz1 = mod(iz,nz1) + 1
    do iy=1,ny2
      if (kz == 0) then
        do ix=1,nx2
          f2(ix,iy,iz*2-1) = f5(ix,iy,iz)              ! when kz=1, f4 values come before f4 values
          f2(ix,iy,iz*2  ) = f4(ix,iy,iz)              ! when kz=0, f5 values come before f5 values
        end do
      else
        do ix=1,nx2
          f2(ix,iy,iz*2-1) = 0.5*(f5(ix,iy,iz )+f4(ix,iy,iz))
          f2(ix,iy,iz*2  ) = 0.5*(f5(ix,iy,iz1)+f4(ix,iy,iz))
        end do
      end if
    end do
  end do
  deallocate (f4,f5)
END SUBROUTINE
END MODULE
!*******************************************************************************
PROGRAM double_prg
  USE double_mod
  implicit none
  logical interp
  integer mx,my,mz,lb,ub,mx2,my2,mz2,isnap,mvar,io_word,rec,ny,iz
  real, allocatable, dimension(:,:,:):: f1, f2
  real, allocatable, dimension(:):: ym1, ym2
  integer, dimension(9):: kx,ky,kz
  character(len=80):: in, out, fym1, fym2
  namelist /io/ in, out, mx, my, mz, lb, ub, isnap, mvar, ny, kx, ky, kz, verbose, fym1, fym2
!...............................................................................
  mvar=9; isnap=0; lb=6; ub=lb; ny=0
  kx = (/0,1,0,0,0,0,1,0,0/)
  ky = (/0,0,1,0,0,0,0,1,0/)
  kz = (/0,0,0,1,0,0,0,0,1/)
  read (*,io); ub=my-ub+1; write (*,io)
  mx2=mx*2; my2=my*2; mz2=mz*2
  if (ny > 0) my2=ny

  interp= fym1.ne.' '

  if (interp) then
    open(10,file=fym1,form='unformatted',status='old')
    read(10) my
    allocate (ym1(my))
    read(10) ym1
    close(10)
    open(10,file=fym2,form='unformatted',status='old')
    read(10) my2
    ny=my2
    allocate (ym2(my2))
    read(10) ym2
    close(10)
  endif

  print'(1x,a,f8.2,a)','Memory needed:',real(mx2)*real(my2)*real(mz2)*9./1024.**3,' GB'

  allocate(f1(mx ,my ,mz ))
  allocate(f2(mx2,my2,mz2))
  
  open (10,file=in,form='unformatted',access='direct',status='old',recl=io_word()*mx*my*mz)
  open (11,file=out,form='unformatted',access='direct',status='unknown',recl=io_word()*mx2*my2)
  do rec=1,mvar
    print*,rec
    read (10,rec=rec+mvar*isnap) f1
    call double (f1,mx,my,mz,lb,ub,f2,mx2,my2,mz2,kx(rec),ky(rec),kz(rec),interp,ym1,ym2)
    do iz=1,mz2
      write(11,rec=iz+mz2*(rec-1)) f2(:,:,iz)
    end do
  end do
  close (10)
  close (11)
  deallocate (f1,f2)
END PROGRAM
