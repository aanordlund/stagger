! $Id: reshape_yz.f90,v 1.13 2015/06/21 20:10:04 aake Exp $
!*******************************************************************************
MODULE reshape_m
  USE mpi_mod, only: mpi_comm_beam, mpi_comm_plane, MPI_REAL, MPI_STATUS_SIZE
  USE params , only: mx, my, mz, master, hl, mid, mpi_nx, mpi_ny, mpi_nz,  mpi_y, &
                     dbg_rad, dbg_mpi, iyoff, mpi_rank
  implicit none
  integer, save:: n_sent=0, n_recv=0
CONTAINS

!*******************************************************************************
SUBROUTINE send_block (a, n, rank, req)                                         ! send n floats to rank
  implicit none
  integer n, rank, tag,  req, ierr
  real a(n)
!...............................................................................
  tag = mpi_y                                                                   ! our mark
  call MPI_ISEND (a, n, MPI_REAL, rank, tag, mpi_comm_beam(2), req, ierr)       ! send non-blocking
  !call MPI_Request_free (req, ierr)                                             ! no need to check this
  n_sent = n_sent + n
END SUBROUTINE

!*******************************************************************************
SUBROUTINE recv_block (a, n, rank)                                              ! recv n floats from rank
  implicit none
  integer n, rank, tag,  req, ierr, status(MPI_STATUS_SIZE)
  real a(n)
!...............................................................................
  tag = rank                                                                    ! their mark
  call MPI_RECV (a, n, MPI_REAL, rank, tag, mpi_comm_beam(2), status, ierr)     ! receive blocking
  n_recv = n_recv + n
END SUBROUTINE

!******************************************************************************
SUBROUTINE reshape_size (rank, ny, nz, lz)                                      ! return rank  yz-size
  implicit none
  integer rank, ny, nz, lz
!...............................................................................
  ny = my*mpi_ny                                                                ! RT-size in y
  if (mpi_ny > mz) then
    if (master) print*,'mpi_ny larger than mz:', mpi_ny, mz
    call abort_mpi
  end if
  nz = (mz-1)/mpi_ny + 1                                                        ! RT-size in z
  lz = min(nz,mz-rank*nz)                                                       ! last one may be shorter
END SUBROUTINE reshape_size

!******************************************************************************
SUBROUTINE test_reshape
  implicit none
END SUBROUTINE

!******************************************************************************
SUBROUTINE reshape_yz (a, b, ny, nz, n2)                                            ! return b(1:mx,1:nz,1:ny)
!
! Easy to check indexing below, since only these should occur:
!   a(:,: ,iz+iz0)    ; xy-slice for index iz in MHD domaing
!   b(:,iz,iy1:iy2)   ; xy-slice for index iz in RT domain
!   c(:,: ,iz)        ; xy-slice arriving in RT domain from MHD domain
!
  USE params, only: dbg_mpi, mpi_rank, n_reshape
  implicit none
  real, dimension(mx,my,mz):: a
  real, dimension(mx,nz,ny):: b
  real, allocatable, dimension(:,:,:):: aa, c
  integer ny, nz, n2
  integer rank, ly, lz, iy1, iy2, jyoff, iz, iz0, iz1, iz2, nrq, req(mpi_ny), ierr
  character(len=mid), save:: id='$Id: reshape_yz.f90,v 1.13 2015/06/21 20:10:04 aake Exp $'
  logical, save:: first_time=.true.
  logical debug2
!...............................................................................
  call print_id (id)
!-------------------------------------------------------------------------------
! Send loop, non-blocking.  We are in the MHD domain and need to send our
! blocks with reduced z-size (nz instead of mz) to the ranks that take care
! of those iz-layers.  We skip the block that belongs to us, and send nothing
! that resides beyond the n2 index (dtau > dtaumax)
!-------------------------------------------------------------------------------
  n_sent = 0                                                                    ! reset
  nrq = 0                                                                       ! request count
  ly = min(n2-iyoff,my)                                                         ! our local y-size
  if (ly > 0) then                                                              ! need to send?
    allocate (aa(mx,ly,mz))                                                     ! right size buffer
    do rank=0,mpi_ny-1                                                          ! loop over y-ranks
      if (rank == mpi_y) cycle                                                  ! skip self
      call reshape_size (rank, ny, nz, lz)                                      ! size of rank block
      nrq = nrq+1                                                               ! request count
      if (debug2(dbg_mpi,5)) &
        print'(2i6,a,i6,1p,5e10.2)', &
        mpi_y, mpi_rank,' sent to',rank,a(1,1:5,1)
      if (debug2(dbg_mpi,4)) &
        print 1,mpi_rank,' reshape sends to ', rank, ly, my, lz                 ! need to send full xy-plane
      1 format(i6,a30,5i6)
      iz0 = rank*nz                                                             ! z-index offset
      iz1 = iz0+1                                                               ! 1st z-index
      iz2 = iz0+lz                                                              ! last z-index
      aa(:,:,iz1:iz2) = a(:,1:ly,iz1:iz2)                                       ! copy to buffer
      call send_block (aa(:,:,iz1:iz2), mx*ly*lz, rank, req(nrq))               ! send non-blocking
    end do
  end if
!-------------------------------------------------------------------------------
! Recv loop, blocking.  We are in the RT domain and are to receive blocks with
! size (mx,nz) from MHD ranks, if they have any such blocks that we should have.
!-------------------------------------------------------------------------------
  call reshape_size (mpi_y, ny, nz, lz)                                         ! size of my block
  do rank=0,mpi_ny-1                                                            ! loop over MHD senders
    iz0 = rank*nz                                                               ! z-index RT offset
    jyoff = rank*my                                                             ! y-index MHD offset
    ly = min(n2-jyoff,my)                                                       ! local MHD index of n2
    if (ly < 1) then                                                            ! MHD rank has nothing for us
      if (debug2(dbg_mpi,4)) &
        print 1,mpi_rank,' reshape is receiving nothing from',rank
      cycle                                                                     ! skip that rank
    end if
    iy1 = jyoff + 1                                                             ! first y-index
    iy2 = jyoff + ly                                                            ! last y-index
    if (rank == mpi_y) then                                                     ! self
      do iz=1,lz
        b(:,iz,iy1:iy2) = a(:,1:ly,iz+iz0)                                      ! just copy
      end do
    else                                                                        ! others
      if (debug2(dbg_mpi,4)) &
        print 1,mpi_rank,' reshape expects from ', rank, ly, my, lz
      allocate (c(mx,ly,lz))                                                    ! allocate buffer
      call recv_block (c, mx*ly*lz, rank)                                       ! receive theirs
      if (debug2(dbg_mpi,4)) &
        print 1,mpi_rank,' reshape received from ', rank, ly, my, lz
      n_reshape = n_reshape + mx*ly*lz
      do iz=1,lz
        b(:,iz,iy1:iy2) = c(:,1:ly,iz)                                          ! the piece of interest
      end do
      deallocate (c)                                                            ! free buffer
    end if
    if (debug2(dbg_mpi,5)) &
      print'(2i6,a,i6,1p,5e10.2)', &
      mpi_y,mpi_rank,' recv fr',rank,b(1,1,iy1:5+iy1-1)
  end do
  call waitall_mpi (nrq, req)
  ly = min(n2-iyoff,my)                                                         ! our local y-size
  if (ly > 0) deallocate (aa)
  call MPI_Barrier (mpi_comm_plane(2), ierr)
  if (debug2(dbg_rad,3) .or. first_time) then
    call sum_int_mpi (n_sent)
    if (mpi_rank==0) print *,'reshape_yz: words sent, n2 =', n_sent, n2
    first_time = .false.
  end if
END SUBROUTINE

!*******************************************************************************
SUBROUTINE deshape_yz (a, b, ny, nz, n2)
!
! Easy to check indexing below, since only these should occur:
!   a(:,: ,iz+iz0)    ; xy-slice for index iz in MHD domaing
!   b(:,iz,iy1:iy2)   ; xy-slice for index iz in RT domain
!   c(:,iz,: )        ; xy-slice arriving in MHD domain from RT domain
!
  USE params, only: n_deshape
  implicit none
  integer ny, nz, n2
  real, dimension(mx,nz,ny):: b
  real, dimension(mx,my,mz):: a
  real, allocatable, dimension(:,:,:):: c
  integer rank, ly, lz, iy1, iy2, iz, iz0, nrq, req(mpi_ny), ierr
  logical, save:: first_time=.true.
  logical debug2
!-------------------------------------------------------------------------------
! Send loop, non-blocking.  Here we are in the RT domain, sending to MHD.
! n2 is the global y-index of the deepest cell we meed to include.  In MHD
! space it belongs to rank=n2/my, and we need to send to that rank n2-rank*my
! xz-planes.  To be properly accessed then need to be complete (mx,nz) planes,
! not (mx,lz) planes.
!-------------------------------------------------------------------------------
  call reshape_size (mpi_y, ny, nz, lz)                                         ! find size of our block
  nrq = 0
  n_sent = 0
  do rank=0,mpi_ny-1                                                            ! loop over y-ranks
    if (rank == mpi_y) cycle                                                    ! skip self
    ly = min(n2-rank*my,my)                                                     ! depth layers to send
    if (ly < 1) then
      if (debug2(dbg_mpi,4)) &
        print 1,mpi_rank,' deshape sends nothing to',rank, ly
      1 format(i6,a30,5i6)
      cycle                                                                     ! skip if above n2
    end if
    iy1 = 1+rank*my                                                             ! first y-index
    iy2 = iy1+ly-1                                                              ! last y-index
    nrq = nrq+1
    if (debug2(dbg_mpi,4)) &
      print 1,mpi_rank,' deshape sends to', rank, ly, nz
    call send_block (b(:,:,iy1:iy2), mx*ly*nz, rank, req(nrq))                  ! send non-blocking
  end do
!-------------------------------------------------------------------------------
!  Recv loop, blocking.  Here we are in the MHD domain, receiving from RT
!-------------------------------------------------------------------------------
  if (iyoff < n2) then
    call reshape_size (mpi_y, ny, nz, lz)                                       ! size of my block
    allocate (c(mx,nz,my))                                                      ! allocate buffer
    do rank=0,mpi_ny-1                                                          ! loop over senders
      iz0 = rank*nz                                                             ! z-index offset
      iy1 = 1+iyoff                                                             ! first y-index
      ly = min(n2-iyoff,my)                                                     ! reduced local size (>0)
      iy2 = iy1+ly-1
      if (rank == mpi_y) then                                                   ! self
        do iz=1,lz
          a(:,1:ly,iz+iz0) = b(:,iz,iy1:iy2)                                    ! just copy
        end do
      else                                                                      ! others
        if (debug2(dbg_mpi,4)) &
          print 1,mpi_rank,' deshape expct fr', rank, ly, nz
        call recv_block (c, mx*ly*nz, rank)                                     ! receive theirs
        if (debug2(dbg_mpi,4)) &
          print 1,mpi_rank,' deshape recvd fr', rank, ly, nz
        do iz=1,lz
          a(:,1:ly,iz+iz0) = c(:,iz,1:ly)                                       ! plug it in
        end do
      end if
    end do
    n_deshape = n_deshape + mx*ly*lz
    deallocate (c)                                                              ! free buffer
  else
    if (debug2(dbg_mpi,4)) &
      print 1,mpi_rank,' deshape returns w/o receiving', n2, iyoff
  end if
  call waitall_mpi (nrq, req)
  call MPI_Barrier (mpi_comm_plane(2), ierr)
  if (debug2(dbg_rad,3) .or. first_time) then
    call sum_int_mpi (n_sent)
    if (mpi_rank==0) print *,'deshape_yz: words sent, n2 =', n_sent, n2
    first_time = .false.
  end if
END SUBROUTINE
END MODULE
