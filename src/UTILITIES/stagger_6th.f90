
!***********************************************************************
SUBROUTINE laplace_set (f, fp, mx, my, mz)
!
!  Laplace operator
!

  implicit none
  real cx, cy, cz
  real, dimension(mx,my,mz):: f, fp
  integer i, j, k, km1, kp1, mx, my, mz, izs, ize
!-----------------------------------------------------------------------

  cx=1.
  cy=1.
  cz=1.
  if (mx.gt.1) then
  call limits_omp(1,mz,izs,ize)
    do k=izs,ize
      do j=1,my
        do i=2,mx-1
          fp(i,j,k) = cx*(f(i-1 ,j,k)+f(i+1,j,k)-2.*f(i,j,k))
        end do
        fp(1 ,j,k)  = cx*(f(2,j,k)+f(mx  ,j,k)-2.*f(1 ,j,k))
        fp(mx,j,k)  = cx*(f(1,j,k)+f(mx-1,j,k)-2.*f(mx,j,k))
      end do
    end do
  else
  call limits_omp(1,mz,izs,ize)
    do k=izs,ize
      do j=1,my
        do i=1,mx
          fp(i,j,k) = 0.
        end do
      end do
    end do
  end if

 if (my .gt. 1) then
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    do j=2,my-1
      do i=1,mx
        fp(i,j,k) = fp(i,j,k) + cy*(f(i,j+1,k)+f(i,j-1,k)-2.*f(i,j,k))
      end do
    end do
    do i=1,mx
        fp(i,1 ,k) = fp(i,1 ,k) + cy*(f(i,2,k)+f(i,my  ,k)-2.*f(i,1 ,k))
        fp(i,my,k) = fp(i,my,k) + cy*(f(i,1,k)+f(i,my-1,k)-2.*f(i,my,k))
    end do
  end do
 end if

 if (mz .gt. 1) then
!$omp barrier
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    km1=mod(k-2+mz,mz)+1
    kp1=mod(k     ,mz)+1
    do j=1,my
      do i=1,mx
        fp(i,j,k) = fp(i,j,k) + cz*(f(i,j,kp1)+f(i,j,km1)-2.*f(i,j,k))
      end do
    end do
  end do
 end if
END SUBROUTINE

!***********************************************************************
SUBROUTINE dif2a_set (f, fp, mx, my, mz)
!
!  Max abs difference one zone left to one zones right.
!

  implicit none
  real cx, cy, cz
  real, dimension(mx,my,mz):: f, fp
  integer i, j, k, km1, kp1, mx, my, mz, izs, ize
!-----------------------------------------------------------------------

  if (mx.gt.1) then
  call limits_omp(1,mz,izs,ize)
    do k=izs,ize
      do j=1,my
        do i=2,mx-1
          fp(i,j,k) = abs(f(i-1 ,j,k)-f(i+1,j,k))
        end do
        fp(1 ,j,k)  = abs(f(mx  ,j,k)-f(2  ,j,k))
        fp(mx,j,k)  = abs(f(mx-1,j,k)-f(1  ,j,k))
      end do
    end do
  else
  call limits_omp(1,mz,izs,ize)
    do k=izs,ize
      do j=1,my
        do i=1,mx
          fp(i,j,k) = 0.
        end do
      end do
    end do
  end if

 if (my .gt. 1) then
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    do j=2,my-1
      do i=1,mx
        fp(i,j, k) = max(fp(i,j, k),abs(f(i,j -1,k)-f(i,j+1,k)))
      end do
    end do
    do i=1,mx
        fp(i,1 ,k) = max(fp(i,1 ,k),abs(f(i,my  ,k)-f(i,  2,k)))
        fp(i,my,k) = max(fp(i,my,k),abs(f(i,my-1,k)-f(i,  1,k)))
    end do
  end do
 end if

 if (mz .gt. 1) then
!$omp barrier
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    km1=mod(k-2+mz,mz)+1
    kp1=mod(k     ,mz)+1
    do j=1,my
      do i=1,mx
        fp(i,j,k ) = max(fp(i,j,k ),abs(f(i,j,km1)-f(i,j,kp1)))
      end do
    end do
  end do
 end if
END SUBROUTINE

!***********************************************************************
SUBROUTINE dif2_set (fx, fy, fz, fp, mx, my, mz)
!
!  Finite difference one zone left to one zones right.
!

  implicit none
  real cx(mx), cy(my), cz(mz)
  real, dimension(mx,my,mz):: fx, fy, fz, fp
  integer i, j, k, km1, kp1, mx, my, mz, izs, ize
!-----------------------------------------------------------------------

  cx=0.5
  cy=0.5
  cz=0.5

  if (mx.gt.1) then
  call limits_omp(1,mz,izs,ize)
    do k=izs,ize
      do j=1,my
        do i=2,mx-1
          fp(i,j,k) = cx(i)*(fx(i-1 ,j,k)-fx(i+1,j,k))
        end do
        fp(1 ,j,k)  = cx(1 )*(fx(mx  ,j,k)-fx(2  ,j,k))
        fp(mx,j,k)  = cx(mx)*(fx(mx-1,j,k)-fx(1  ,j,k))
      end do
    end do
  else
  call limits_omp(1,mz,izs,ize)
    do k=izs,ize
      do j=1,my
        do i=1,mx
          fp(i,j,k) = 0.
        end do
      end do
    end do
  end if

 if (my .gt. 1) then
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    do j=2,my-1
      do i=1,mx
        fp(i,j, k) = fp(i,j, k) + cy(j)*(fy(i,j -1,k)-fy(i,j+1,k))
      end do
    end do
    do i=1,mx
        fp(i,1 ,k) = fp(i,1 ,k) + cy(1 )*(fy(i,my  ,k)-fy(i,  2,k))
        fp(i,my,k) = fp(i,my,k) + cy(my)*(fy(i,my-1,k)-fy(i,  1,k))
    end do
  end do
 end if

 if (mz .gt. 1) then
!$omp barrier
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    km1=mod(k-2+mz,mz)+1
    kp1=mod(k     ,mz)+1
    do j=1,my
      do i=1,mx
        fp(i,j,k ) = fp(i,j,k ) + cz(k)*(fz(i,j,km1)-fz(i,j,kp1))
      end do
    end do
  end do
 end if

  call limits_omp(1,mz,izs,ize)
 do k=izs,ize
   do j=1,my
     do i=1,mx
       fp(i,j,k) = max (0., fp(i,j,k))
     end do
   end do
 end do

END SUBROUTINE

!***********************************************************************
SUBROUTINE dif2d_set (fx, fy, fz, fp, mx, my, mz)
!
!  Finite difference one zone left to one zones right.
!

  implicit none
  real, dimension(mx,my,mz):: fx, fy, fz, fp
  integer i, j, k, km1, kp1, mx, my, mz, izs, ize
!-----------------------------------------------------------------------

  if (mx.gt.1) then
  call limits_omp(1,mz,izs,ize)
    do k=izs,ize
      do j=1,my
        do i=2,mx-1
          fp(i,j,k) = (fx(i-1 ,j,k)-fx(i+1,j,k))
        end do
        fp(1 ,j,k)  = (fx(mx  ,j,k)-fx(2  ,j,k))
        fp(mx,j,k)  = (fx(mx-1,j,k)-fx(1  ,j,k))
      end do
    end do
  else
  call limits_omp(1,mz,izs,ize)
    do k=izs,ize
      do j=1,my
        do i=1,mx
          fp(i,j,k) = 0.
        end do
      end do
    end do
  end if

 if (my .gt. 1) then
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    do j=2,my-1
      do i=1,mx
        fp(i,j, k) = fp(i,j, k) + (fy(i,j -1,k)-fy(i,j+1,k))
      end do
    end do
    do i=1,mx
        fp(i,1 ,k) = fp(i,1 ,k) + (fy(i,my  ,k)-fy(i,  2,k))
        fp(i,my,k) = fp(i,my,k) + (fy(i,my-1,k)-fy(i,  1,k))
    end do
  end do
 end if

 if (mz .gt. 1) then
!$omp barrier
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    km1=mod(k-2+mz,mz)+1
    kp1=mod(k     ,mz)+1
    do j=1,my
      do i=1,mx
        fp(i,j,k ) = fp(i,j,k ) + (fz(i,j,km1)-fz(i,j,kp1))
      end do
    end do
  end do
 end if

  call limits_omp(1,mz,izs,ize)
 do k=izs,ize
   do j=1,my
     do i=1,mx
       fp(i,j,k) = max (0., fp(i,j,k))
     end do
   end do
 end do

END SUBROUTINE

!***********************************************************************
SUBROUTINE dif1_set (fx, fy, fz, fp, mx, my, mz)
!
!  Finite difference one zone left to one zones right.
!

  implicit none
  real, dimension(mx,my,mz):: fx, fy, fz, fp
  integer i, j, k, kp1, mx, my, mz, izs, ize
!-----------------------------------------------------------------------

  if (mx.gt.1) then
  call limits_omp(1,mz,izs,ize)
    do k=izs,ize
      do j=1,my
        do i=1,mx-1
          fp(i,j,k) = (fx( i,j,k)-fx(i+1,j,k))
        end do
        fp(mx,j,k)  = (fx(mx,j,k)-fx(  1,j,k))
      end do
    end do
  else
  call limits_omp(1,mz,izs,ize)
    do k=izs,ize
      do j=1,my
        do i=1,mx
          fp(i,j,k) = 0.
        end do
      end do
    end do
  end if

 if (my .gt. 1) then
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    do j=1,my-1
      do i=1,mx
        fp(i, j,k) = fp(i, j,k) + (fy(i, j,k)-fy(i,j+1,k))
      end do
    end do
    do i=1,mx
        fp(i,my,k) = fp(i,my,k) + (fy(i,my,k)-fy(i,  1,k))
    end do
  end do
 end if

 if (mz .gt. 1) then
!$omp barrier
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    kp1=mod(k,mz)+1
    do j=1,my
      do i=1,mx
        fp(i,j,k) = fp(i,j,k) + (fz(i,j,k)-fz(i,j,kp1))
      end do
    end do
  end do
 end if

  call limits_omp(1,mz,izs,ize)
 do k=izs,ize
   do j=1,my
     do i=1,mx
       fp(i,j,k) = max (0., fp(i,j,k))
     end do
   end do
 end do

END SUBROUTINE

!***********************************************************************
SUBROUTINE xup_set (f, fp, mx, my, mz)
!
!  f is centered on (i-.5,j,k), fifth order stagger
!
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!-----------------------------------------------------------------------
!
!
  if (mx.le.5) then
    fp = f
    return
  end if
  c = 3./256.
  b = -25./256.
  a = .5-b-c
  a1st = 0.5
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
   do j=1,my
    fp(mx-2,j,k) = ( &
                   a*(f(mx-2,j,k)+f(mx-1,j,k)) &
                 + b*(f(mx-3,j,k)+f(mx  ,j,k)) &
                 + c*(f(mx-4,j,k)+f(1   ,j,k)))
    fp(mx-1,j,k) = ( &
                   a*(f(mx-1,j,k)+f(mx  ,j,k)) &
                 + b*(f(mx-2,j,k)+f(1   ,j,k)) &
                 + c*(f(mx-3,j,k)+f(2   ,j,k)))
    fp(mx  ,j,k) = ( &
                   a*(f(mx  ,j,k)+f(1   ,j,k)) &
                 + b*(f(mx-1,j,k)+f(2   ,j,k)) &
                 + c*(f(mx-2,j,k)+f(3   ,j,k)))
    fp(1   ,j,k) = ( &
                   a*(f(1   ,j,k)+f(2   ,j,k)) &
                 + b*(f(mx  ,j,k)+f(3   ,j,k)) &
                 + c*(f(mx-1,j,k)+f(4   ,j,k)))
    fp(2   ,j,k) = ( &
                   a*(f(2   ,j,k)+f(3   ,j,k)) &
                 + b*(f(1   ,j,k)+f(4   ,j,k)) &
                 + c*(f(mx  ,j,k)+f(5   ,j,k)))
   end do
   do j=1,my
    do i=3,mx-3
      fp(i  ,j,k) = ( &
                    a*(f(i  ,j,k)+f(i+1 ,j,k)) &
                  + b*(f(i-1,j,k)+f(i+2 ,j,k)) &
                  + c*(f(i-2,j,k)+f(i+3 ,j,k)))
    end do
   end do
  end do
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE xup1_set (f, fp, mx, my, mz)
!
!  f is centered on (i-.5,j,k), fifth order stagger
!
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = f
    return
  end if
!
  a = .5
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
   do j=1,my
    fp(mx  ,j,k) = ( &
                   a*(f(mx  ,j,k)+f(1   ,j,k)))
   end do
   do j=1,my
    do i=1,mx-1
      fp(i  ,j,k) = ( &
                    a*(f(i  ,j,k)+f(i+1 ,j,k)))
    end do
   end do
  end do
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE xdn_set (f, fp, mx, my, mz)
!
!  f is centered on (i,j,k), fifth order stagger
!
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = f
    return
  end if
!
  c = 3./256.
  b = -25./256.
  a = .5-b-c
  a1st = 0.5
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
   do j=1,my
    fp(mx-1,j,k) = ( &
                   a*(f(mx-2,j,k)+f(mx-1,j,k)) &
                 + b*(f(mx-3,j,k)+f(mx  ,j,k)) &
                 + c*(f(mx-4,j,k)+f(1   ,j,k)))
    fp(mx  ,j,k) = ( &
                   a*(f(mx-1,j,k)+f(mx  ,j,k)) &
                 + b*(f(mx-2,j,k)+f(1   ,j,k)) &
                 + c*(f(mx-3,j,k)+f(2   ,j,k)))
    fp(1  ,j,k) = ( &
                   a*(f(mx  ,j,k)+f(1   ,j,k)) &
                 + b*(f(mx-1,j,k)+f(2   ,j,k)) &
                 + c*(f(mx-2,j,k)+f(3   ,j,k)))
    fp(2  ,j,k) = ( &
                   a*(f(1   ,j,k)+f(2   ,j,k)) &
                 + b*(f(mx  ,j,k)+f(3   ,j,k)) &
                 + c*(f(mx-1,j,k)+f(4   ,j,k)))
    fp(3  ,j,k) = ( &
                   a*(f(2   ,j,k)+f(3   ,j,k)) &
                 + b*(f(1   ,j,k)+f(4   ,j,k)) &
                 + c*(f(mx  ,j,k)+f(5   ,j,k)))
   end do
   do j=1,my
    do i=4,mx-2
      fp(i,j,k) = ( &
                   a*(f(i   ,j,k)+f(i-1 ,j,k)) &
                 + b*(f(i+1 ,j,k)+f(i-2 ,j,k)) &
                 + c*(f(i+2 ,j,k)+f(i-3 ,j,k)))
    end do
   end do
  end do
!
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE xdn1_set (f, fp, mx, my, mz)
!
!  f is centered on (i,j,k), fifth order stagger
!
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = f
    return
  end if
!
  a = 0.5
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
   do j=1,my
    fp(1  ,j,k) = ( &
                   a*(f(mx  ,j,k)+f(1   ,j,k)))
   end do
   do j=1,my
    do i=2,mx
      fp(i,j,k) = ( &
                   a*(f(i   ,j,k)+f(i-1 ,j,k)))
    end do
   end do
  end do
!
END SUBROUTINE
!***********************************************************************
SUBROUTINE xup32_set (f,j, fp, mx, my, mz)
!
!  f is centered on (i-.5,j,k), fifth order stagger
!
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f
  real, dimension(mx,mz):: fp
!-----------------------------------------------------------------------
!
!
  if (mx.le.5) then
    fp = f(:,j,:)
    return
  end if
  c = 3./256.
  b = -25./256.
  a = .5-b-c
  a1st = 0.5
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    fp(mx-2,k) = ( &
                   a*(f(mx-2,j,k)+f(mx-1,j,k)) &
                 + b*(f(mx-3,j,k)+f(mx  ,j,k)) &
                 + c*(f(mx-4,j,k)+f(1   ,j,k)))
    fp(mx-1,k) = ( &
                   a*(f(mx-1,j,k)+f(mx  ,j,k)) &
                 + b*(f(mx-2,j,k)+f(1   ,j,k)) &
                 + c*(f(mx-3,j,k)+f(2   ,j,k)))
    fp(mx  ,k) = ( &
                   a*(f(mx  ,j,k)+f(1   ,j,k)) &
                 + b*(f(mx-1,j,k)+f(2   ,j,k)) &
                 + c*(f(mx-2,j,k)+f(3   ,j,k)))
    fp(1   ,k) = ( &
                   a*(f(1   ,j,k)+f(2   ,j,k)) &
                 + b*(f(mx  ,j,k)+f(3   ,j,k)) &
                 + c*(f(mx-1,j,k)+f(4   ,j,k)))
    fp(2   ,k) = ( &
                   a*(f(2   ,j,k)+f(3   ,j,k)) &
                 + b*(f(1   ,j,k)+f(4   ,j,k)) &
                 + c*(f(mx  ,j,k)+f(5   ,j,k)))
    do i=3,mx-3
      fp(i  ,k) = ( &
                    a*(f(i  ,j,k)+f(i+1 ,j,k)) &
                  + b*(f(i-1,j,k)+f(i+2 ,j,k)) &
                  + c*(f(i-2,j,k)+f(i+3 ,j,k)))
    end do
  end do
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE xup132_set (f,j, fp, mx, my, mz)
!
!  f is centered on (i-.5,j,k), fifth order stagger
!
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f
  real, dimension(mx,mz):: fp
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = f(:,j,:)
    return
  end if
!
  a = .5
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    fp(mx  ,k) = ( &
                   a*(f(mx  ,j,k)+f(1   ,j,k)))
    do i=1,mx-1
      fp(i  ,k) = ( &
                    a*(f(i  ,j,k)+f(i+1 ,j,k)))
    end do
  end do
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE xdn32_set (f,j, fp, mx, my, mz)
!
!  f is centered on (i,j,k), fifth order stagger
!
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f
  real, dimension(mx,mz):: fp
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = f(:,j,:)
    return
  end if
!
  c = 3./256.
  b = -25./256.
  a = .5-b-c
  a1st = 0.5
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    fp(mx-1,k) = ( &
                   a*(f(mx-2,j,k)+f(mx-1,j,k)) &
                 + b*(f(mx-3,j,k)+f(mx  ,j,k)) &
                 + c*(f(mx-4,j,k)+f(1   ,j,k)))
    fp(mx  ,k) = ( &
                   a*(f(mx-1,j,k)+f(mx  ,j,k)) &
                 + b*(f(mx-2,j,k)+f(1   ,j,k)) &
                 + c*(f(mx-3,j,k)+f(2   ,j,k)))
    fp(1  ,k) = ( &
                   a*(f(mx  ,j,k)+f(1   ,j,k)) &
                 + b*(f(mx-1,j,k)+f(2   ,j,k)) &
                 + c*(f(mx-2,j,k)+f(3   ,j,k)))
    fp(2  ,k) = ( &
                   a*(f(1   ,j,k)+f(2   ,j,k)) &
                 + b*(f(mx  ,j,k)+f(3   ,j,k)) &
                 + c*(f(mx-1,j,k)+f(4   ,j,k)))
    fp(3  ,k) = ( &
                   a*(f(2   ,j,k)+f(3   ,j,k)) &
                 + b*(f(1   ,j,k)+f(4   ,j,k)) &
                 + c*(f(mx  ,j,k)+f(5   ,j,k)))
    do i=4,mx-2
      fp(i,k) = ( &
                   a*(f(i   ,j,k)+f(i-1 ,j,k)) &
                 + b*(f(i+1 ,j,k)+f(i-2 ,j,k)) &
                 + c*(f(i+2 ,j,k)+f(i-3 ,j,k)))
    end do
  end do
!
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE xdn132_set (f,j, fp, mx, my, mz)
!
!  f is centered on (i,j,k), fifth order stagger
!
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f
  real, dimension(mx,mz):: fp
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = f(:,j,:)
    return
  end if
!
  a = 0.5
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    fp(1  ,k) = ( &
                   a*(f(mx  ,j,k)+f(1   ,j,k)))
    do i=2,mx
      fp(i,k) = ( &
                   a*(f(i   ,j,k)+f(i-1 ,j,k)))
    end do
  end do
!
END SUBROUTINE
!***********************************************************************
SUBROUTINE xup22_set (f, fp, mx, my, mz)
!
!  f is centered on (i-.5,k), fifth order stagger
!
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,mz):: fp, f
!-----------------------------------------------------------------------
!
!
  if (mx.le.5) then
    fp = f
    return
  end if
  c = 3./256.
  b = -25./256.
  a = .5-b-c
  a1st = 0.5
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    fp(mx-2,k) = ( &
                   a*(f(mx-2,k)+f(mx-1,k)) &
                 + b*(f(mx-3,k)+f(mx  ,k)) &
                 + c*(f(mx-4,k)+f(1   ,k)))
    fp(mx-1,k) = ( &
                   a*(f(mx-1,k)+f(mx  ,k)) &
                 + b*(f(mx-2,k)+f(1   ,k)) &
                 + c*(f(mx-3,k)+f(2   ,k)))
    fp(mx  ,k) = ( &
                   a*(f(mx  ,k)+f(1   ,k)) &
                 + b*(f(mx-1,k)+f(2   ,k)) &
                 + c*(f(mx-2,k)+f(3   ,k)))
    fp(1   ,k) = ( &
                   a*(f(1   ,k)+f(2   ,k)) &
                 + b*(f(mx  ,k)+f(3   ,k)) &
                 + c*(f(mx-1,k)+f(4   ,k)))
    fp(2   ,k) = ( &
                   a*(f(2   ,k)+f(3   ,k)) &
                 + b*(f(1   ,k)+f(4   ,k)) &
                 + c*(f(mx  ,k)+f(5   ,k)))
    do i=3,mx-3
      fp(i  ,k) = ( &
                    a*(f(i  ,k)+f(i+1 ,k)) &
                  + b*(f(i-1,k)+f(i+2 ,k)) &
                  + c*(f(i-2,k)+f(i+3 ,k)))
    end do
  end do
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE xup122_set (f, fp, mx, my, mz)
!
!  f is centered on (i-.5,k), fifth order stagger
!
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,mz):: fp, f
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = f
    return
  end if
!
  a = .5
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    fp(mx  ,k) = ( &
                   a*(f(mx  ,k)+f(1   ,k)))
    do i=1,mx-1
      fp(i  ,k) = ( &
                    a*(f(i  ,k)+f(i+1 ,k)))
    end do
  end do
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE xdn22_set (f, fp, mx, my, mz)
!
!  f is centered on (i,k), fifth order stagger
!
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,mz):: fp, f
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = f
    return
  end if
!
  c = 3./256.
  b = -25./256.
  a = .5-b-c
  a1st = 0.5
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    fp(mx-1,k) = ( &
                   a*(f(mx-2,k)+f(mx-1,k)) &
                 + b*(f(mx-3,k)+f(mx  ,k)) &
                 + c*(f(mx-4,k)+f(1   ,k)))
    fp(mx  ,k) = ( &
                   a*(f(mx-1,k)+f(mx  ,k)) &
                 + b*(f(mx-2,k)+f(1   ,k)) &
                 + c*(f(mx-3,k)+f(2   ,k)))
    fp(1  ,k) = ( &
                   a*(f(mx  ,k)+f(1   ,k)) &
                 + b*(f(mx-1,k)+f(2   ,k)) &
                 + c*(f(mx-2,k)+f(3   ,k)))
    fp(2  ,k) = ( &
                   a*(f(1   ,k)+f(2   ,k)) &
                 + b*(f(mx  ,k)+f(3   ,k)) &
                 + c*(f(mx-1,k)+f(4   ,k)))
    fp(3  ,k) = ( &
                   a*(f(2   ,k)+f(3   ,k)) &
                 + b*(f(1   ,k)+f(4   ,k)) &
                 + c*(f(mx  ,k)+f(5   ,k)))
    do i=4,mx-2
      fp(i,k) = ( &
                   a*(f(i   ,k)+f(i-1 ,k)) &
                 + b*(f(i+1 ,k)+f(i-2 ,k)) &
                 + c*(f(i+2 ,k)+f(i-3 ,k)))
    end do
  end do
!
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE xdn122_set (f, fp, mx, my, mz)
!
!  f is centered on (i,k), fifth order stagger
!
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,mz):: fp, f
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = f
    return
  end if
!
  a = 0.5
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    fp(1  ,k) = ( &
                   a*(f(mx  ,k)+f(1   ,k)))
    do i=2,mx
      fp(i,k) = ( &
                   a*(f(i   ,k)+f(i-1 ,k)))
    end do
  end do
!
END SUBROUTINE
!***********************************************************************
SUBROUTINE ddxupi_set (f, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = 0.
    return
  end if
!
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)
  a1st = 1.
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
   do j=1,my
    fp(mx-2,j,k) = ( &
                   c*(f(1   ,j,k)-f(mx-4,j,k)) &
                 + b*(f(mx  ,j,k)-f(mx-3,j,k)) &
                 + a*(f(mx-1,j,k)-f(mx-2,j,k)))
    fp(mx-1,j,k) = ( &
                   c*(f(2   ,j,k)-f(mx-3,j,k)) &
                 + b*(f(1   ,j,k)-f(mx-2,j,k)) &
                 + a*(f(mx  ,j,k)-f(mx-1,j,k)))
    fp(mx  ,j,k) = ( &
                   c*(f(3   ,j,k)-f(mx-2,j,k)) &
                 + b*(f(2   ,j,k)-f(mx-1,j,k)) &
                 + a*(f(1   ,j,k)-f(mx  ,j,k)))
    fp(1  ,j,k) = ( &
                   c*(f(4   ,j,k)-f(mx-1,j,k)) &
                 + b*(f(3   ,j,k)-f(mx  ,j,k)) &
                 + a*(f(2   ,j,k)-f(1   ,j,k)))
    fp(2  ,j,k) = ( &
                   c*(f(5   ,j,k)-f(mx ,j,k)) &
                 + b*(f(4   ,j,k)-f(1  ,j,k)) &
                 + a*(f(3   ,j,k)-f(2  ,j,k)))
   end do
   do j=1,my
    do i=3,mx-3
      fp(i  ,j,k) = ( &
                   c*(f(i+3 ,j,k)-f(i-2 ,j,k)) &
                 + b*(f(i+2 ,j,k)-f(i-1 ,j,k)) &
                 + a*(f(i+1 ,j,k)-f(i   ,j,k)))
    end do
   end do
  end do
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddxupi1_set (f, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = 0.
    return
  end if
!
  a = 1.
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
   do j=1,my
    fp(mx  ,j,k) = ( &
                   a*(f(1   ,j,k)-f(mx  ,j,k)))
   end do
   do j=1,my
    do i=1,mx-1
      fp(i  ,j,k) = ( &
                   a*(f(i+1 ,j,k)-f(i   ,j,k)))
    end do
   end do
  end do
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddxdni_set (f, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = 0.
    return
  end if
!
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)
  a1st = 1.
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
   do j=1,my
    fp(mx-1,j,k) = ( &
                   a*(f(mx-1,j,k)-f(mx-2,j,k)) &
                 + b*(f(mx  ,j,k)-f(mx-3,j,k)) &
                 + c*(f(1   ,j,k)-f(mx-4,j,k)))
    fp(mx  ,j,k) = ( &
                   a*(f(mx  ,j,k)-f(mx-1,j,k)) &
                 + b*(f(1   ,j,k)-f(mx-2,j,k)) &
                 + c*(f(2   ,j,k)-f(mx-3,j,k)))
    fp(1   ,j,k) = ( &
                   a*(f(1   ,j,k)-f(mx  ,j,k)) &
                 + b*(f(2   ,j,k)-f(mx-1,j,k)) &
                 + c*(f(3   ,j,k)-f(mx-2,j,k)))
    fp(2  ,j,k) = ( &
                   a*(f(2   ,j,k)-f(1   ,j,k)) &
                 + b*(f(3   ,j,k)-f(mx  ,j,k)) &
                 + c*(f(4   ,j,k)-f(mx-1,j,k)))
    fp(3  ,j,k) = ( &
                   a*(f(3   ,j,k)-f(2   ,j,k)) &
                 + b*(f(4   ,j,k)-f(1   ,j,k)) &
                 + c*(f(5   ,j,k)-f(mx  ,j,k)))
   end do
   do j=1,my
    do i=4,mx-2
      fp(i,j,k) = ( &
                   a*(f(i   ,j,k)-f(i-1 ,j,k)) &
                 + b*(f(i+1 ,j,k)-f(i-2 ,j,k)) &
                 + c*(f(i+2 ,j,k)-f(i-3 ,j,k)))
    end do
   end do
  end do
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddxdni1_set (f, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = 0.
    return
  end if
!
  a = 1.
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
   do j=1,my
    fp(1   ,j,k) = ( &
                   a*(f(1   ,j,k)-f(mx  ,j,k)))
   end do
   do j=1,my
    do i=2,mx
      fp(i,j,k) = ( &
                   a*(f(i   ,j,k)-f(i-1 ,j,k)))
    end do
   end do
  end do
!
END SUBROUTINE
!***********************************************************************
SUBROUTINE ddxupi32_set (f,j, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f
  real, dimension(mx,mz):: fp
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = 0.
    return
  end if
!
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)
  a1st = 1.
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    fp(mx-2,k) = ( &
                   a*(f(mx-1,j,k)-f(mx-2,j,k)) &
                 + b*(f(mx  ,j,k)-f(mx-3,j,k)) &
                 + c*(f(1   ,j,k)-f(mx-4,j,k)))
    fp(mx-1,k) = ( &
                   a*(f(mx  ,j,k)-f(mx-1,j,k)) &
                 + b*(f(1   ,j,k)-f(mx-2,j,k)) &
                 + c*(f(2   ,j,k)-f(mx-3,j,k)))
    fp(mx  ,k) = ( &
                   a*(f(1   ,j,k)-f(mx  ,j,k)) &
                 + b*(f(2   ,j,k)-f(mx-1,j,k)) &
                 + c*(f(3   ,j,k)-f(mx-2,j,k)))
    fp(1  ,k) = ( &
                   a*(f(2   ,j,k)-f(1   ,j,k)) &
                 + b*(f(3   ,j,k)-f(mx  ,j,k)) &
                 + c*(f(4   ,j,k)-f(mx-1,j,k)))
    fp(2  ,k) = ( &
                   a*(f(3   ,j,k)-f(2  ,j,k)) &
                 + b*(f(4   ,j,k)-f(1  ,j,k)) &
                 + c*(f(5   ,j,k)-f(mx ,j,k)))
    do i=3,mx-3
      fp(i  ,k) = ( &
                   a*(f(i+1 ,j,k)-f(i   ,j,k)) &
                 + b*(f(i+2 ,j,k)-f(i-1 ,j,k)) &
                 + c*(f(i+3 ,j,k)-f(i-2 ,j,k)))
    end do
  end do
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddxupi132_set (f,j, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f
  real, dimension(mx,mz):: fp
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = 0.
    return
  end if
!
  a = 1.
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    fp(mx  ,k) = ( &
                   a*(f(1   ,j,k)-f(mx  ,j,k)))
    do i=1,mx-1
      fp(i  ,k) = ( &
                   a*(f(i+1 ,j,k)-f(i   ,j,k)))
    end do
  end do
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddxdni32_set (f,j, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f
  real, dimension(mx,mz):: fp
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = 0.
    return
  end if
!
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)
  a1st = 1.
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    fp(mx-1,k) = ( &
                   a*(f(mx-1,j,k)-f(mx-2,j,k)) &
                 + b*(f(mx  ,j,k)-f(mx-3,j,k)) &
                 + c*(f(1   ,j,k)-f(mx-4,j,k)))
    fp(mx  ,k) = ( &
                   a*(f(mx  ,j,k)-f(mx-1,j,k)) &
                 + b*(f(1   ,j,k)-f(mx-2,j,k)) &
                 + c*(f(2   ,j,k)-f(mx-3,j,k)))
    fp(1   ,k) = ( &
                   a*(f(1   ,j,k)-f(mx  ,j,k)) &
                 + b*(f(2   ,j,k)-f(mx-1,j,k)) &
                 + c*(f(3   ,j,k)-f(mx-2,j,k)))
    fp(2  ,k) = ( &
                   a*(f(2   ,j,k)-f(1   ,j,k)) &
                 + b*(f(3   ,j,k)-f(mx  ,j,k)) &
                 + c*(f(4   ,j,k)-f(mx-1,j,k)))
    fp(3  ,k) = ( &
                   a*(f(3   ,j,k)-f(2   ,j,k)) &
                 + b*(f(4   ,j,k)-f(1   ,j,k)) &
                 + c*(f(5   ,j,k)-f(mx  ,j,k)))
    do i=4,mx-2
      fp(i,k) = ( &
                   a*(f(i   ,j,k)-f(i-1 ,j,k)) &
                 + b*(f(i+1 ,j,k)-f(i-2 ,j,k)) &
                 + c*(f(i+2 ,j,k)-f(i-3 ,j,k)))
    end do
  end do
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddxdni132_set (f,j, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f
  real, dimension(mx,mz):: fp
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = 0.
    return
  end if
!
  a = 1.
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    fp(1   ,k) = ( &
                   a*(f(1   ,j,k)-f(mx  ,j,k)))
    do i=2,mx
      fp(i,k) = ( &
                   a*(f(i   ,j,k)-f(i-1 ,j,k)))
    end do
  end do
!
END SUBROUTINE
!***********************************************************************
SUBROUTINE ddxupi22_set (f, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,mz):: fp, f
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = 0.
    return
  end if
!
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)
  a1st = 1.
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    fp(mx-2,k) = ( &
                   a*(f(mx-1,k)-f(mx-2,k)) &
                 + b*(f(mx  ,k)-f(mx-3,k)) &
                 + c*(f(1   ,k)-f(mx-4,k)))
    fp(mx-1,k) = ( &
                   a*(f(mx  ,k)-f(mx-1,k)) &
                 + b*(f(1   ,k)-f(mx-2,k)) &
                 + c*(f(2   ,k)-f(mx-3,k)))
    fp(mx  ,k) = ( &
                   a*(f(1   ,k)-f(mx  ,k)) &
                 + b*(f(2   ,k)-f(mx-1,k)) &
                 + c*(f(3   ,k)-f(mx-2,k)))
    fp(1  ,k) = ( &
                   a*(f(2   ,k)-f(1   ,k)) &
                 + b*(f(3   ,k)-f(mx  ,k)) &
                 + c*(f(4   ,k)-f(mx-1,k)))
    fp(2  ,k) = ( &
                   a*(f(3   ,k)-f(2  ,k)) &
                 + b*(f(4   ,k)-f(1  ,k)) &
                 + c*(f(5   ,k)-f(mx ,k)))
    do i=3,mx-3
      fp(i  ,k) = ( &
                   a*(f(i+1 ,k)-f(i   ,k)) &
                 + b*(f(i+2 ,k)-f(i-1 ,k)) &
                 + c*(f(i+3 ,k)-f(i-2 ,k)))
    end do
  end do
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddxupi122_set (f, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,mz):: fp, f
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = 0.
    return
  end if
!
  a = 1.
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    fp(mx  ,k) = ( &
                   a*(f(1   ,k)-f(mx  ,k)))
    do i=1,mx-1
      fp(i  ,k) = ( &
                   a*(f(i+1 ,k)-f(i   ,k)))
    end do
  end do
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddxdni22_set (f, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,mz):: fp, f
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = 0.
    return
  end if
!
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)
  a1st = 1.
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    fp(mx-1,k) = ( &
                   a*(f(mx-1,k)-f(mx-2,k)) &
                 + b*(f(mx  ,k)-f(mx-3,k)) &
                 + c*(f(1   ,k)-f(mx-4,k)))
    fp(mx  ,k) = ( &
                   a*(f(mx  ,k)-f(mx-1,k)) &
                 + b*(f(1   ,k)-f(mx-2,k)) &
                 + c*(f(2   ,k)-f(mx-3,k)))
    fp(1   ,k) = ( &
                   a*(f(1   ,k)-f(mx  ,k)) &
                 + b*(f(2   ,k)-f(mx-1,k)) &
                 + c*(f(3   ,k)-f(mx-2,k)))
    fp(2  ,k) = ( &
                   a*(f(2   ,k)-f(1   ,k)) &
                 + b*(f(3   ,k)-f(mx  ,k)) &
                 + c*(f(4   ,k)-f(mx-1,k)))
    fp(3  ,k) = ( &
                   a*(f(3   ,k)-f(2   ,k)) &
                 + b*(f(4   ,k)-f(1   ,k)) &
                 + c*(f(5   ,k)-f(mx  ,k)))
    do i=4,mx-2
      fp(i,k) = ( &
                   a*(f(i   ,k)-f(i-1 ,k)) &
                 + b*(f(i+1 ,k)-f(i-2 ,k)) &
                 + c*(f(i+2 ,k)-f(i-3 ,k)))
    end do
  end do
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddxdni122_set (f, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,mz):: fp, f
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = 0.
    return
  end if
!
  a = 1.
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    fp(1   ,k) = ( &
                   a*(f(1   ,k)-f(mx  ,k)))
    do i=2,mx
      fp(i,k) = ( &
                   a*(f(i   ,k)-f(i-1 ,k)))
    end do
  end do
!
END SUBROUTINE
!***********************************************************************
SUBROUTINE ddxup_set (f, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = 0.
    return
  end if
!
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)
  a1st = 1.
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
   do j=1,my
    fp(mx-2,j,k) = ( &
                 + c*(f(1   ,j,k)-f(mx-4,j,k)) &
                 + b*(f(mx  ,j,k)-f(mx-3,j,k)) &
                 + a*(f(mx-1,j,k)-f(mx-2,j,k)))
    fp(mx-1,j,k) = ( &
                 + c*(f(2   ,j,k)-f(mx-3,j,k)) &
                 + b*(f(1   ,j,k)-f(mx-2,j,k)) &
                 + a*(f(mx  ,j,k)-f(mx-1,j,k)))
    fp(mx  ,j,k) = ( &
                 + c*(f(3   ,j,k)-f(mx-2,j,k)) &
                 + b*(f(2   ,j,k)-f(mx-1,j,k)) &
                 + a*(f(1   ,j,k)-f(mx  ,j,k)))
    fp(1  ,j,k) = ( &
                 + c*(f(4   ,j,k)-f(mx-1,j,k)) &
                 + b*(f(3   ,j,k)-f(mx  ,j,k)) &
                 + a*(f(2   ,j,k)-f(1   ,j,k)))
    fp(2  ,j,k) = ( &
                 + c*(f(5   ,j,k)-f(mx  ,j,k)) &
                 + b*(f(4   ,j,k)-f(1   ,j,k)) &
                 + a*(f(3   ,j,k)-f(2   ,j,k)))
   end do
   do j=1,my
    do i=3,mx-3
      fp(i  ,j,k) = ( &
                 + c*(f(i+3 ,j,k)-f(i-2 ,j,k)) &
                 + b*(f(i+2 ,j,k)-f(i-1 ,j,k)) &
                 + a*(f(i+1 ,j,k)-f(i   ,j,k)))
    end do
  end do
  end do
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddxup1_set (f, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = 0.
    return
  end if
!
  a = 1.
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
   do j=1,my
    fp(mx  ,j,k) = ( &
                   a*(f(1   ,j,k)-f(mx  ,j,k)))
   end do
   do j=1,my
    do i=1,mx-1
      fp(i  ,j,k) = ( &
                   a*(f(i+1 ,j,k)-f(i   ,j,k)))
    end do
  end do
  end do
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddxdn_set (f, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = 0.
    return
  end if
!
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)
  a1st = 1.
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
   do j=1,my
    fp(mx-1,j,k) = ( &
                 + c*(f(1   ,j,k)-f(mx-4,j,k)) &
                 + b*(f(mx  ,j,k)-f(mx-3,j,k)) &
                 + a*(f(mx-1,j,k)-f(mx-2,j,k)))
    fp(mx  ,j,k) = ( &
                 + c*(f(2   ,j,k)-f(mx-3,j,k)) &
                 + b*(f(1   ,j,k)-f(mx-2,j,k)) &
                 + a*(f(mx  ,j,k)-f(mx-1,j,k)))
    fp(1   ,j,k) = ( &
                 + c*(f(3   ,j,k)-f(mx-2,j,k)) &
                 + b*(f(2   ,j,k)-f(mx-1,j,k)) &
                 + a*(f(1   ,j,k)-f(mx  ,j,k)))
    fp(2  ,j,k) = ( &
                 + c*(f(4   ,j,k)-f(mx-1,j,k)) &
                 + b*(f(3   ,j,k)-f(mx  ,j,k)) &
                 + a*(f(2   ,j,k)-f(1   ,j,k)))
    fp(3  ,j,k) = ( &
                 + c*(f(5   ,j,k)-f(mx  ,j,k)) &
                 + b*(f(4   ,j,k)-f(1   ,j,k)) &
                 + a*(f(3   ,j,k)-f(2   ,j,k)))
   end do
   do j=1,my
    do i=4,mx-2
      fp(i,j,k) = ( &
                 + c*(f(i+2 ,j,k)-f(i-3 ,j,k)) &
                 + b*(f(i+1 ,j,k)-f(i-2 ,j,k)) &
                 + a*(f(i   ,j,k)-f(i-1 ,j,k)))
    end do
   end do
  end do
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddxdn1_set (f, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = 0.
    return
  end if
!
  a = 1.
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
   do j=1,my
    fp(1   ,j,k) = ( &
                   a*(f(1   ,j,k)-f(mx  ,j,k)))
   end do
   do j=1,my
    do i=2,mx
      fp(i,j,k) = ( &
                   a*(f(i   ,j,k)-f(i-1 ,j,k)))
    end do
   end do
  end do
!
END SUBROUTINE
!***********************************************************************
SUBROUTINE yup_set (f, fp, mx, my, mz)
!
!  f is centered on (i,j-.5,k), fifth order stagger
!
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, intent(in) , dimension(mx,my,mz):: f
  real,              dimension(mx,my,mz):: fp
!-----------------------------------------------------------------------
!

  if (my .lt. 5) then
  call limits_omp(1,mz,izs,ize)
    do k=izs,ize
      fp(:,:,k) = f(:,:,k)
    end do
    return
  end if
  c = 3./256.
  b = -25./256.
  a = .5-b-c
  a1st = 0.5
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    do i=1,mx
      fp(i,my-2,k) = ( &
                     a*(f(i,my-2,k)+f(i,my-1,k)) + &
                     b*(f(i,my-3,k)+f(i,my  ,k)) + &
                     c*(f(i,my-4,k)+f(i,1   ,k)))
      fp(i,my-1,k) = ( &
                     a*(f(i,my-1,k)+f(i,my  ,k)) + &
                     b*(f(i,my-2,k)+f(i,1   ,k)) + &
                     c*(f(i,my-3,k)+f(i,2   ,k)))
      fp(i,my  ,k) = ( &
                     a*(f(i,my  ,k)+f(i,1   ,k)) + &
                     b*(f(i,my-1,k)+f(i,2   ,k)) + &
                     c*(f(i,my-2,k)+f(i,3   ,k)))
      fp(i,1   ,k) = ( &
                     a*(f(i,1   ,k)+f(i,2   ,k)) + &
                     b*(f(i,my  ,k)+f(i,3   ,k)) + &
                     c*(f(i,my-1,k)+f(i,4   ,k)))
      fp(i,2   ,k) = ( &
                     a*(f(i,2   ,k)+f(i,3   ,k)) + &
                     b*(f(i,1   ,k)+f(i,4   ,k)) + &
                     c*(f(i,my  ,k)+f(i,5   ,k)))
    end do
    do j=3,my-3
     do i=1,mx
      fp(i,j  ,k) = ( &
                     a*(f(i,j   ,k)+f(i,j+1 ,k)) + &
                     b*(f(i,j-1 ,k)+f(i,j+2 ,k)) + &
                     c*(f(i,j-2 ,k)+f(i,j+3 ,k)))
     end do
    end do
  end do
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE yup1_set (f, fp, mx, my, mz)
!
!  f is centered on (i,j-.5,k), fifth order stagger
!
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, intent(in) , dimension(mx,my,mz):: f
  real,              dimension(mx,my,mz):: fp
!-----------------------------------------------------------------------
!
  if (my .lt. 5) then
  call limits_omp(1,mz,izs,ize)
    do k=izs,ize
      fp(:,:,k) = f(:,:,k)
    end do
    return
  end if
!
  a = 0.5
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    do i=1,mx
      fp(i,my,k) = ( &
                     a*(f(i,my,k)+f(i,1,k)))
    end do
    do j=1,my-1
      do i=1,mx
        fp(i,j  ,k) = ( &
                     a*(f(i,j   ,k)+f(i,j+1 ,k)))
      end do
    end do
  end do
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE ydn_set (f, fp, mx, my, mz)
!
!  f is centered on (i,j,k), fifth order stagger
!
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!
!-----------------------------------------------------------------------
  if (my .lt. 5) then
  call limits_omp(1,mz,izs,ize)
    do k=izs,ize
      fp(:,:,k) = f(:,:,k)
    end do
    return
  end if
  c = 3./256.
  b = -25./256.
  a = .5-b-c
  a1st = 0.5
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    do i=1,mx
      fp(i,my-1,k) = ( &
                     a*(f(i,my-2,k)+f(i,my-1,k)) + &
                     b*(f(i,my-3,k)+f(i,my  ,k)) + &
                     c*(f(i,my-4,k)+f(i,1   ,k)))
      fp(i,my  ,k) = ( &
                     a*(f(i,my-1,k)+f(i,my  ,k)) + &
                     b*(f(i,my-2,k)+f(i,1   ,k)) + &
                     c*(f(i,my-3,k)+f(i,2   ,k)))
      fp(i,1   ,k) = ( &
                     a*(f(i,my  ,k)+f(i,1   ,k)) + &
                     b*(f(i,my-1,k)+f(i,2   ,k)) + &
                     c*(f(i,my-2,k)+f(i,3   ,k)))
      fp(i,2   ,k) = ( &
                     a*(f(i,1   ,k)+f(i,2   ,k)) + &
                     b*(f(i,my  ,k)+f(i,3   ,k)) + &
                     c*(f(i,my-1,k)+f(i,4   ,k)))
      fp(i,3   ,k) = ( &
                     a*(f(i,2   ,k)+f(i,3   ,k)) + &
                     b*(f(i,1   ,k)+f(i,4   ,k)) + &
                     c*(f(i,my  ,k)+f(i,5   ,k)))
    end do
    do j=4,my-2
     do i=1,mx
      fp(i,j,k) = ( &
                     a*(f(i,j   ,k)+f(i,j-1 ,k)) + &
                     b*(f(i,j+1 ,k)+f(i,j-2 ,k)) + &
                     c*(f(i,j+2 ,k)+f(i,j-3 ,k)))
     end do
    end do
  end do
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE ydn1_set (f, fp, mx, my, mz)
!
!  f is centered on (i,j,k), fifth order stagger
!
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!
!-----------------------------------------------------------------------
  if (my .lt. 5) then
  call limits_omp(1,mz,izs,ize)
    do k=izs,ize
      fp(:,:,k) = f(:,:,k)
    end do
    return
  end if
!
  a = 0.5
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    do i=1,mx
      fp(i,1   ,k) = ( &
                     a*(f(i,my  ,k)+f(i,1   ,k)))
    end do
    do j=2,my
     do i=1,mx
      fp(i,j,k) = ( &
                     a*(f(i,j   ,k)+f(i,j-1 ,k)))
     end do
    end do
  end do
!
END SUBROUTINE
!***********************************************************************
SUBROUTINE ddyupi_set (f, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!-----------------------------------------------------------------------
!
  if (my .lt. 3) then
    fp = 0.
    return
  end if
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)
  a1st = 1.
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    do i=1,mx
      fp(i,my-2,k) = ( &
                     c*(f(i,1   ,k)-f(i,my-4,k)) + &
                     b*(f(i,my  ,k)-f(i,my-3,k)) + &
                     a*(f(i,my-1,k)-f(i,my-2,k)))
      fp(i,my-1,k) = ( &
                     c*(f(i,2   ,k)-f(i,my-3,k)) + &
                     b*(f(i,1   ,k)-f(i,my-2,k)) + &
                     a*(f(i,my  ,k)-f(i,my-1,k)))
      fp(i,my  ,k) = ( &
                     c*(f(i,3   ,k)-f(i,my-2,k)) + &
                     b*(f(i,2   ,k)-f(i,my-1,k)) + &
                     a*(f(i,1   ,k)-f(i,my  ,k)))
      fp(i,1   ,k) = ( &
                     c*(f(i,4   ,k)-f(i,my-1,k)) + &
                     b*(f(i,3   ,k)-f(i,my  ,k)) + &
                     a*(f(i,2   ,k)-f(i,1   ,k)))
      fp(i,2   ,k) = ( &
                     c*(f(i,5   ,k)-f(i,my  ,k)) + &
                     b*(f(i,4   ,k)-f(i,1   ,k)) + &
                     a*(f(i,3   ,k)-f(i,2   ,k)))
    end do
    do j=3,my-3
     do i=1,mx
      fp(i,j  ,k) = ( &
                     c*(f(i,j+3 ,k)-f(i,j-2 ,k)) + &
                     b*(f(i,j+2 ,k)-f(i,j-1 ,k)) + &
                     a*(f(i,j+1 ,k)-f(i,j   ,k)))
     end do
    end do
  end do
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddyupi1_set (f, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!-----------------------------------------------------------------------
!
  if (my .lt. 3) then
    fp = 0.
    return
  end if
  a = 1.
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    do i=1,mx
      fp(i,my,k) = ( &
                     a*(f(i,1  ,k)-f(i,my,k)))
    end do
    do j=1,my-1
      do i=1,mx
        fp(i,j  ,k) = ( &
                     a*(f(i,j+1 ,k)-f(i,j   ,k)))
     end do
    end do
  end do
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddydni_set (f, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!-----------------------------------------------------------------------
!
  if (my .lt. 3) then
    fp = 0.
    return
  end if
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)
  a1st = 1.
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    do i=1,mx
      fp(i,my-1,k) = ( &
                     a*(f(i,my-1,k)-f(i,my-2,k)) + &
                     b*(f(i,my  ,k)-f(i,my-3,k)) + &
                     c*(f(i,1   ,k)-f(i,my-4,k)))
      fp(i,my  ,k) = ( &
                     a*(f(i,my  ,k)-f(i,my-1,k)) + &
                     b*(f(i,1   ,k)-f(i,my-2,k)) + &
                     c*(f(i,2   ,k)-f(i,my-3,k)))
      fp(i,1  ,k) = ( &
                     a*(f(i,1   ,k)-f(i,my  ,k)) + &
                     b*(f(i,2   ,k)-f(i,my-1,k)) + &
                     c*(f(i,3   ,k)-f(i,my-2,k)))
      fp(i,2   ,k) = ( &
                     a*(f(i,2   ,k)-f(i,1   ,k)) + &
                     b*(f(i,3   ,k)-f(i,my  ,k)) + &
                     c*(f(i,4   ,k)-f(i,my-1,k)))
      fp(i,3   ,k) = ( &
                     a*(f(i,3   ,k)-f(i,2   ,k)) + &
                     b*(f(i,4   ,k)-f(i,1   ,k)) + &
                     c*(f(i,5   ,k)-f(i,my  ,k)))
    end do
    do j=4,my-2
     do i=1,mx
      fp(i,j ,k) = ( &
                     a*(f(i,j   ,k)-f(i,j-1 ,k)) + &
                     b*(f(i,j+1 ,k)-f(i,j-2 ,k)) + &
                     c*(f(i,j+2 ,k)-f(i,j-3 ,k)))
     end do
    end do
  end do
!
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddydni1_set (f, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!-----------------------------------------------------------------------
!
  if (my .lt. 3) then
    fp = 0.
    return
  end if
  a = 1.
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    do i=1,mx
      fp(i,1  ,k) = ( &
                     a*(f(i,1   ,k)-f(i,my  ,k)))
    end do
    do j=2,my
     do i=1,mx
      fp(i,j ,k) = ( &
                     a*(f(i,j   ,k)-f(i,j-1 ,k)))
     end do
    end do
  end do
!
END SUBROUTINE
!***********************************************************************
SUBROUTINE ddyup_set (f, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!-----------------------------------------------------------------------
!
  if (my .lt. 3) then
    fp = 0.
    return
  end if
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)
  a1st = 1.
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    do i=1,mx
      fp(i,my-2,k) = ( &
                     c*(f(i,1   ,k)-f(i,my-4,k)) + &
                     b*(f(i,my  ,k)-f(i,my-3,k)) + &
                     a*(f(i,my-1,k)-f(i,my-2,k)) )
      fp(i,my-1,k) = ( &
                     c*(f(i,2   ,k)-f(i,my-3,k)) + &
                     b*(f(i,1   ,k)-f(i,my-2,k)) + &
                     a*(f(i,my  ,k)-f(i,my-1,k)) )
      fp(i,my  ,k) = ( &
                     c*(f(i,3   ,k)-f(i,my-2,k)) + &
                     b*(f(i,2   ,k)-f(i,my-1,k)) + &
                     a*(f(i,1   ,k)-f(i,my  ,k)) )
      fp(i,1   ,k) = ( &
                     c*(f(i,4   ,k)-f(i,my-1,k)) + &
                     b*(f(i,3   ,k)-f(i,my  ,k)) + &
                     a*(f(i,2   ,k)-f(i,1   ,k)) )
      fp(i,2   ,k) = ( &
                     c*(f(i,5   ,k)-f(i,my  ,k)) + &
                     b*(f(i,4   ,k)-f(i,1   ,k)) + &
                     a*(f(i,3   ,k)-f(i,2   ,k)) )
    end do
    do j=3,my-3
     do i=1,mx
      fp(i,j  ,k) = ( &
                     c*(f(i,j+3 ,k)-f(i,j-2 ,k)) + &
                     b*(f(i,j+2 ,k)-f(i,j-1 ,k)) + &
                     a*(f(i,j+1 ,k)-f(i,j   ,k)) )
     end do
    end do
  end do
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddyup1_set (f, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!-----------------------------------------------------------------------
!
  if (my .lt. 3) then
    fp = 0.
    return
  end if
  a = 1.
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    do i=1,mx
      fp(i,my,k) = ( &
                     a*(f(i,1  ,k)-f(i,my,k)))
    end do
    do j=1,my-1
      do i=1,mx
        fp(i,j  ,k) = ( &
                     a*(f(i,j+1 ,k)-f(i,j   ,k)))
     end do
    end do
  end do
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddydn_set (f, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!-----------------------------------------------------------------------
!
  if (my .lt. 3) then
    fp = 0.
    return
  end if
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)
  a1st = 1.
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    do i=1,mx
      fp(i,my-1,k) = ( &
                     c*(f(i,1   ,k)-f(i,my-4,k)) + &
                     b*(f(i,my  ,k)-f(i,my-3,k)) + &
                     a*(f(i,my-1,k)-f(i,my-2,k)) ) 
      fp(i,my  ,k) = ( &
                     c*(f(i,2   ,k)-f(i,my-3,k)) + &
                     b*(f(i,1   ,k)-f(i,my-2,k)) + &
                     a*(f(i,my  ,k)-f(i,my-1,k)) )
      fp(i,1  ,k) = ( &
                     c*(f(i,3   ,k)-f(i,my-2,k)) + &
                     b*(f(i,2   ,k)-f(i,my-1,k)) + &
                     a*(f(i,1   ,k)-f(i,my  ,k)) )
      fp(i,2   ,k) = ( &
                     c*(f(i,4   ,k)-f(i,my-1,k)) + &
                     b*(f(i,3   ,k)-f(i,my  ,k)) + &
                     a*(f(i,2   ,k)-f(i,1   ,k)) )
      fp(i,3   ,k) = ( &
                     c*(f(i,5   ,k)-f(i,my  ,k)) + &
                     b*(f(i,4   ,k)-f(i,1   ,k)) + &
                     a*(f(i,3   ,k)-f(i,2   ,k)) )
    end do
    do j=4,my-2
     do i=1,mx
      fp(i,j ,k) = ( &
                     c*(f(i,j+2 ,k)-f(i,j-3 ,k)) + &
                     b*(f(i,j+1 ,k)-f(i,j-2 ,k)) + &
                     a*(f(i,j   ,k)-f(i,j-1 ,k)) )
     end do
    end do
  end do
!
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddydn1_set (f, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!-----------------------------------------------------------------------
!
  if (my .lt. 3) then
    fp = 0.
    return
  end if
  a = 1.
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    do i=1,mx
      fp(i,1  ,k) = ( &
                     a*(f(i,1   ,k)-f(i,my  ,k)))
    end do
    do j=2,my
     do i=1,mx
      fp(i,j ,k) = ( &
                     a*(f(i,j   ,k)-f(i,j-1 ,k)))
     end do
    end do
  end do
!
END SUBROUTINE
!***********************************************************************
SUBROUTINE zup_set (f, fp, mx, my, mz)
!
!  f is centered on (i,j,k-.5), fifth order stagger
!
  implicit none
  real a, b, c, a1st
  integer i, j, k, km2, km1, kp1, kp2, kp3, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!-----------------------------------------------------------------------
!
!
  if (mz.le.5) then
    fp = f
    return
  end if
  c = 3./256.
  b = -25./256.
  a = .5-b-c
  a1st = 0.5
!
!$omp barrier
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    km2=mod(k-3+mz,mz)+1
    km1=mod(k-2+mz,mz)+1
    kp1=mod(k     ,mz)+1
    kp2=mod(k+1   ,mz)+1
    kp3=mod(k+2   ,mz)+1
    do j=1,my
      do i=1,mx
        fp(i,j,k) = ( &
                     a*(f(i,j,kp1)+f(i,j,k  )) + &
                     b*(f(i,j,kp2)+f(i,j,km1)) + &
                     c*(f(i,j,kp3)+f(i,j,km2)))
      end do
    end do
  end do
!$omp barrier
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE zup1_set (f, fp, mx, my, mz)
!
!  f is centered on (i,j,k-.5), fifth order stagger
!
  implicit none
  real a, b, c, a1st
  integer i, j, k, kp1, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    fp = f
    return
  end if
  a = .5
!
!$omp barrier
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    kp1=mod(k     ,mz)+1
    do j=1,my
      do i=1,mx
        fp(i,j,k) = ( &
                     a*(f(i,j,kp1)+f(i,j,k  )))
      end do
    end do
  end do
!$omp barrier
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE zdn_set (f, fp, mx, my, mz)
!
!  f is centered on (i,j,k), fifth order stagger
!
  implicit none
  real a, b, c, a1st
  integer i, j, k, km3, km2, km1, kp1, kp2, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    fp = f
    return
  end if
  c = 3./256.
  b = -25./256.
  a = .5-b-c
  a1st = 0.5
!
!$omp barrier
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    km3=mod(k-4+mz,mz)+1
    km2=mod(k-3+mz,mz)+1
    km1=mod(k-2+mz,mz)+1
    kp1=mod(k     ,mz)+1
    kp2=mod(k+1   ,mz)+1
    do j=1,my
      do i=1,mx
        fp(i,j,k) = ( &
                     a*(f(i,j,k  )+f(i,j,km1)) + &
                     b*(f(i,j,kp1)+f(i,j,km2)) + &
                     c*(f(i,j,kp2)+f(i,j,km3)))
      end do
    end do
  end do
!$omp barrier
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE zdn1_set (f, fp, mx, my, mz)
!
!  f is centered on (i,j,k), fifth order stagger
!
  implicit none
  real a, b, c, a1st
  integer i, j, k, km1, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    fp = f
    return
  end if
  a = .5
!
!$omp barrier
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    km1=mod(k-2+mz,mz)+1
    do j=1,my
      do i=1,mx
        fp(i,j,k) = ( &
                     a*(f(i,j,k  )+f(i,j,km1)))
      end do
    end do
  end do
!$omp barrier
!
END SUBROUTINE
!***********************************************************************
SUBROUTINE zup32_set (f,j, fp, mx, my, mz)
!
!  f is centered on (i,j,k-.5), fifth order stagger
!
  implicit none
  real a, b, c, a1st
  integer i, j, k, km2, km1, kp1, kp2, kp3, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f
  real, dimension(mx,mz):: fp
!-----------------------------------------------------------------------
!
!
  if (mz.le.5) then
    fp = f(:,j,:)
    return
  end if
  c = 3./256.
  b = -25./256.
  a = .5-b-c
  a1st = 0.5
!
!$omp barrier
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    km2=mod(k-3+mz,mz)+1
    km1=mod(k-2+mz,mz)+1
    kp1=mod(k     ,mz)+1
    kp2=mod(k+1   ,mz)+1
    kp3=mod(k+2   ,mz)+1
      do i=1,mx
        fp(i,k) = ( &
                     a*(f(i,j,kp1)+f(i,j,k  )) + &
                     b*(f(i,j,kp2)+f(i,j,km1)) + &
                     c*(f(i,j,kp3)+f(i,j,km2)))
      end do
  end do
!$omp barrier
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE zup132_set (f,j, fp, mx, my, mz)
!
!  f is centered on (i,j,k-.5), fifth order stagger
!
  implicit none
  real a, b, c, a1st
  integer i, j, k, kp1, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f
  real, dimension(mx,mz):: fp
!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    fp = f(:,j,:)
    return
  end if
  a = .5
!
!$omp barrier
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    kp1=mod(k     ,mz)+1
      do i=1,mx
        fp(i,k) = ( &
                     a*(f(i,j,kp1)+f(i,j,k  )))
      end do
  end do
!$omp barrier
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE zdn32_set (f,j, fp, mx, my, mz)
!
!  f is centered on (i,j,k), fifth order stagger
!
  implicit none
  real a, b, c, a1st
  integer i, j, k, km3, km2, km1, kp1, kp2, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f
  real, dimension(mx,mz):: fp
!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    fp = f(:,j,:)
    return
  end if
  c = 3./256.
  b = -25./256.
  a = .5-b-c
  a1st = 0.5
!
!$omp barrier
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    km3=mod(k-4+mz,mz)+1
    km2=mod(k-3+mz,mz)+1
    km1=mod(k-2+mz,mz)+1
    kp1=mod(k     ,mz)+1
    kp2=mod(k+1   ,mz)+1
      do i=1,mx
        fp(i,k) = ( &
                     a*(f(i,j,k  )+f(i,j,km1)) + &
                     b*(f(i,j,kp1)+f(i,j,km2)) + &
                     c*(f(i,j,kp2)+f(i,j,km3)))
      end do
  end do
!$omp barrier
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE zdn132_set (f,j, fp, mx, my, mz)
!
!  f is centered on (i,j,k), fifth order stagger
!
  implicit none
  real a, b, c, a1st
  integer i, j, k, km1, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f
  real, dimension(mx,mz):: fp
!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    fp = f(:,j,:)
    return
  end if
  a = .5
!
!$omp barrier
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    km1=mod(k-2+mz,mz)+1
      do i=1,mx
        fp(i,k) = ( &
                     a*(f(i,j,k  )+f(i,j,km1)))
      end do
  end do
!$omp barrier
!
END SUBROUTINE
!***********************************************************************
SUBROUTINE zup22_set (f, fp, mx, my, mz)
!
!  f is centered on (i,k-.5), fifth order stagger
!
  implicit none
  real a, b, c, a1st
  integer i, j, k, km2, km1, kp1, kp2, kp3, mx, my, mz, izs, ize
  real, dimension(mx,mz):: fp, f
!-----------------------------------------------------------------------
!
!
  if (mz.le.5) then
    fp = f
    return
  end if
  c = 3./256.
  b = -25./256.
  a = .5-b-c
  a1st = 0.5
!
!$omp barrier
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    km2=mod(k-3+mz,mz)+1
    km1=mod(k-2+mz,mz)+1
    kp1=mod(k     ,mz)+1
    kp2=mod(k+1   ,mz)+1
    kp3=mod(k+2   ,mz)+1
      do i=1,mx
        fp(i,k) = ( &
                     a*(f(i,kp1)+f(i,k  )) + &
                     b*(f(i,kp2)+f(i,km1)) + &
                     c*(f(i,kp3)+f(i,km2)))
      end do
  end do
!$omp barrier
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE zup122_set (f, fp, mx, my, mz)
!
!  f is centered on (i,k-.5), fifth order stagger
!
  implicit none
  real a, b, c, a1st
  integer i, j, k, kp1, mx, my, mz, izs, ize
  real, dimension(mx,mz):: fp, f
!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    fp = f
    return
  end if
  a = .5
!
!$omp barrier
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    kp1=mod(k     ,mz)+1
      do i=1,mx
        fp(i,k) = ( &
                     a*(f(i,kp1)+f(i,k  )))
      end do
  end do
!$omp barrier
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE zdn22_set (f, fp, mx, my, mz)
!
!  f is centered on (i,k), fifth order stagger
!
  implicit none
  real a, b, c, a1st
  integer i, j, k, km3, km2, km1, kp1, kp2, mx, my, mz, izs, ize
  real, dimension(mx,mz):: fp, f
!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    fp = f
    return
  end if
  c = 3./256.
  b = -25./256.
  a = .5-b-c
  a1st = 0.5
!
!$omp barrier
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    km3=mod(k-4+mz,mz)+1
    km2=mod(k-3+mz,mz)+1
    km1=mod(k-2+mz,mz)+1
    kp1=mod(k     ,mz)+1
    kp2=mod(k+1   ,mz)+1
      do i=1,mx
        fp(i,k) = ( &
                     a*(f(i,k  )+f(i,km1)) + &
                     b*(f(i,kp1)+f(i,km2)) + &
                     c*(f(i,kp2)+f(i,km3)))
      end do
  end do
!$omp barrier
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE zdn122_set (f, fp, mx, my, mz)
!
!  f is centered on (i,k), fifth order stagger
!
  implicit none
  real a, b, c, a1st
  integer i, j, k, km1, mx, my, mz, izs, ize
  real, dimension(mx,mz):: fp, f
!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    fp = f
    return
  end if
  a = .5
!
!$omp barrier
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    km1=mod(k-2+mz,mz)+1
      do i=1,mx
        fp(i,k) = ( &
                     a*(f(i,k  )+f(i,km1)))
      end do
  end do
!$omp barrier
!
END SUBROUTINE
!***********************************************************************
SUBROUTINE ddzupi_set (f, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, km2, km1, kp1, kp2, kp3, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    fp = 0.
    return
  endif
!
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)
  a1st = 1.
!
!$omp barrier
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    km2=mod(k-3+mz,mz)+1
    km1=mod(k-2+mz,mz)+1
    kp1=mod(k     ,mz)+1
    kp2=mod(k+1   ,mz)+1
    kp3=mod(k+2   ,mz)+1
    do j=1,my
      do i=1,mx
        fp(i,j,k) = ( &
                     c*(f(i,j,kp3)-f(i,j,km2)) + &
                     b*(f(i,j,kp2)-f(i,j,km1)) + &
                     a*(f(i,j,kp1)-f(i,j,k  )))
      end do
    end do
  end do
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddzupi1_set (f, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, kp1, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    fp = 0.
    return
  endif
!
  a = 1.
!
!$omp barrier
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    kp1=mod(k     ,mz)+1
    do j=1,my
      do i=1,mx
        fp(i,j,k) = ( &
                     a*(f(i,j,kp1)-f(i,j,k  )))
      end do
    end do
  end do
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddzdni_set (f, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, km3, km2, km1, kp1, kp2, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    fp = 0.
    return
  endif
!
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)
  a1st = 1.
!
!$omp barrier
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    km3=mod(k-4+mz,mz)+1
    km2=mod(k-3+mz,mz)+1
    km1=mod(k-2+mz,mz)+1
    kp1=mod(k     ,mz)+1
    kp2=mod(k+1   ,mz)+1
    do j=1,my
      do i=1,mx
        fp(i,j,k) = ( &
                     a*(f(i,j,k  )-f(i,j,km1)) + &
                     b*(f(i,j,kp1)-f(i,j,km2)) + &
                     c*(f(i,j,kp2)-f(i,j,km3)))
      end do
    end do
  end do
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddzdni1_set (f, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, km1, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    fp = 0.
    return
  endif
!
  a = 1.
!
!$omp barrier
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    km1=mod(k-2+mz,mz)+1
    do j=1,my
      do i=1,mx
        fp(i,j,k) = ( &
                     a*(f(i,j,k  )-f(i,j,km1)))
      end do
    end do
  end do
!
END SUBROUTINE
!***********************************************************************
SUBROUTINE ddzupi32_set (f,j, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, km2, km1, kp1, kp2, kp3, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f
  real, dimension(mx,mz):: fp
!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    fp = 0.
    return
  endif
!
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)
  a1st = 1.
!
!$omp barrier
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    km2=mod(k-3+mz,mz)+1
    km1=mod(k-2+mz,mz)+1
    kp1=mod(k     ,mz)+1
    kp2=mod(k+1   ,mz)+1
    kp3=mod(k+2   ,mz)+1
      do i=1,mx
        fp(i,k) = ( &
                     a*(f(i,j,kp1)-f(i,j,k  )) + &
                     b*(f(i,j,kp2)-f(i,j,km1)) + &
                     c*(f(i,j,kp3)-f(i,j,km2)))
      end do
  end do
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddzupi132_set (f,j, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, kp1, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f
  real, dimension(mx,mz):: fp
!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    fp = 0.
    return
  endif
!
  a = 1.
!
!$omp barrier
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    kp1=mod(k     ,mz)+1
      do i=1,mx
        fp(i,k) = ( &
                     a*(f(i,j,kp1)-f(i,j,k  )))
      end do
  end do
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddzdni32_set (f,j, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, km3, km2, km1, kp1, kp2, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f
  real, dimension(mx,mz):: fp
!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    fp = 0.
    return
  endif
!
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)
  a1st = 1.
!
!$omp barrier
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    km3=mod(k-4+mz,mz)+1
    km2=mod(k-3+mz,mz)+1
    km1=mod(k-2+mz,mz)+1
    kp1=mod(k     ,mz)+1
    kp2=mod(k+1   ,mz)+1
      do i=1,mx
        fp(i,k) = ( &
                     a*(f(i,j,k  )-f(i,j,km1)) + &
                     b*(f(i,j,kp1)-f(i,j,km2)) + &
                     c*(f(i,j,kp2)-f(i,j,km3)))
      end do
  end do
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddzdni132_set (f,j, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, km1, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f
  real, dimension(mx,mz):: fp
!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    fp = 0.
    return
  endif
!
  a = 1.
!
!$omp barrier
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    km1=mod(k-2+mz,mz)+1
      do i=1,mx
        fp(i,k) = ( &
                     a*(f(i,j,k  )-f(i,j,km1)))
      end do
  end do
!
END SUBROUTINE
!***********************************************************************
SUBROUTINE ddzupi22_set (f, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, km2, km1, kp1, kp2, kp3, mx, my, mz, izs, ize
  real, dimension(mx,mz):: fp, f
!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    fp = 0.
    return
  endif
!
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)
  a1st = 1.
!
!$omp barrier
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    km2=mod(k-3+mz,mz)+1
    km1=mod(k-2+mz,mz)+1
    kp1=mod(k     ,mz)+1
    kp2=mod(k+1   ,mz)+1
    kp3=mod(k+2   ,mz)+1
      do i=1,mx
        fp(i,k) = ( &
                     a*(f(i,kp1)-f(i,k  )) + &
                     b*(f(i,kp2)-f(i,km1)) + &
                     c*(f(i,kp3)-f(i,km2)))
      end do
  end do
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddzupi122_set (f, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, kp1, mx, my, mz, izs, ize
  real, dimension(mx,mz):: fp, f
!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    fp = 0.
    return
  endif
!
  a = 1.
!
!$omp barrier
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    kp1=mod(k     ,mz)+1
      do i=1,mx
        fp(i,k) = ( &
                     a*(f(i,kp1)-f(i,k  )))
      end do
  end do
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddzdni22_set (f, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, km3, km2, km1, kp1, kp2, mx, my, mz, izs, ize
  real, dimension(mx,mz):: fp, f
!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    fp = 0.
    return
  endif
!
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)
  a1st = 1.
!
!$omp barrier
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    km3=mod(k-4+mz,mz)+1
    km2=mod(k-3+mz,mz)+1
    km1=mod(k-2+mz,mz)+1
    kp1=mod(k     ,mz)+1
    kp2=mod(k+1   ,mz)+1
      do i=1,mx
        fp(i,k) = ( &
                     a*(f(i,k  )-f(i,km1)) + &
                     b*(f(i,kp1)-f(i,km2)) + &
                     c*(f(i,kp2)-f(i,km3)))
      end do
  end do
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddzdni122_set (f, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, km1, mx, my, mz, izs, ize
  real, dimension(mx,mz):: fp, f
!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    fp = 0.
    return
  endif
!
  a = 1.
!
!$omp barrier
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    km1=mod(k-2+mz,mz)+1
      do i=1,mx
        fp(i,k) = ( &
                     a*(f(i,k  )-f(i,km1)))
      end do
  end do
!
END SUBROUTINE
!***********************************************************************
SUBROUTINE ddzup_set (f, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, km2, km1, kp1, kp2, kp3, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    fp = 0.
    return
  end if
!
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)
  a1st = 1.
!
!$omp barrier
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    km2=mod(k-3+mz,mz)+1
    km1=mod(k-2+mz,mz)+1
    kp1=mod(k     ,mz)+1
    kp2=mod(k+1   ,mz)+1
    kp3=mod(k+2   ,mz)+1
    do j=1,my
      do i=1,mx
        fp(i,j,k) = ( &
                     c*(f(i,j,kp3)-f(i,j,km2)) + &
                     b*(f(i,j,kp2)-f(i,j,km1)) + &
                     a*(f(i,j,kp1)-f(i,j,k  )) )
      end do
    end do
  end do
!$omp barrier
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddzup1_set (f, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, kp1, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    fp = 0.
    return
  end if
!
  a = 1.
!
!$omp barrier
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    kp1=mod(k     ,mz)+1
    do j=1,my
      do i=1,mx
        fp(i,j,k) = ( &
                     a*(f(i,j,kp1)-f(i,j,k  )))
      end do
    end do
  end do
!$omp barrier
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddzdn_set (f, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, km3, km2, km1, kp1, kp2, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    fp = 0.
    return
  end if
!
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)
  a1st = 1.
!
!$omp barrier
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    km3=mod(k-4+mz,mz)+1
    km2=mod(k-3+mz,mz)+1
    km1=mod(k-2+mz,mz)+1
    kp1=mod(k     ,mz)+1
    kp2=mod(k+1   ,mz)+1
    do j=1,my
      do i=1,mx
        fp(i,j,k) = ( &
                     c*(f(i,j,kp2)-f(i,j,km3)) + &
                     b*(f(i,j,kp1)-f(i,j,km2)) + &
                     a*(f(i,j,k  )-f(i,j,km1)) )
      end do
    end do
  end do
!$omp barrier
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddzdn1_set (f, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, km1, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    fp = 0.
    return
  end if
!
  a = 1.
!
!$omp barrier
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    km1=mod(k-2+mz,mz)+1
    do j=1,my
      do i=1,mx
        fp(i,j,k) = ( &
                     a*(f(i,j,k  )-f(i,j,km1)))
      end do
    end do
  end do
!$omp barrier
!
END SUBROUTINE
! $Id: stagger_6th.f90,v 1.3 2007/08/01 03:32:00 aake Exp $
!***********************************************************************
!  NOTE: Because smooth3 and max3 need an internal scratch array they
!  cannot easily be OpenMP parallel in an external parallel region.
!  It is therefore simplest to maintain them as loop parallel only,
!  and to only use smooth3max3 for external parallel region cases.
!***********************************************************************
SUBROUTINE smooth3_set (f, fp, mx, my, mz, scratch)
!
!  Three point smooth
!
  implicit none
  integer i,j,k,km1,kp1, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp, scratch
  real, dimension(0:mx+1):: fx
  real, dimension(0:my+1):: fy
  real c3
!-----------------------------------------------------------------------
!
  c3=1./3.
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
   do j=1,my
    do i=1,mx
      fx(i)=f(i,j,k)
    end do
    fx(0)=fx(mx)
    fx(mx+1)=fx(1)
    do i=1,mx
      scratch(i,j,k)=c3*(fx(i-1)+fx(i)+fx(i+1))
    end do
   end do
   do i=1,mx
    do j=1,my
      fy(j)=scratch(i,j,k)
    end do
    fy(0)=fy(my)
    fy(my+1)=fy(1)
    do j=1,my
      scratch(i,j,k)=c3*(fy(j-1)+fy(j)+fy(j+1))
    end do
   end do
  end do
  !$omp barrier
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
   km1=mod(k+mz-2,mz)+1
   kp1=mod(k+mz  ,mz)+1
   do j=1,my
    do i=1,mx
      fp(i,j,k)=c3*(scratch(i,j,k)+scratch(i,j,km1)+scratch(i,j,kp1))
    end do
   end do
  end do
END SUBROUTINE

!***********************************************************************
SUBROUTINE max3_set (f, fp, mx, my, mz, scratch)
!
!  Three point max
!
  implicit none
  integer i,j,k,km1,kp1, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp, scratch
  real, dimension(0:mx+1):: fx
  real, dimension(0:my+1):: fy
!-----------------------------------------------------------------------
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
   do j=1,my
    do i=1,mx
      fx(i)=f(i,j,k)
    end do
    fx(0)=fx(mx)
    fx(mx+1)=fx(1)
    do i=1,mx
      scratch(i,j,k)=amax1(fx(i-1),fx(i),fx(i+1))
    end do
   end do
   do i=1,mx
    do j=1,my
      fy(j)=scratch(i,j,k)
    end do
    fy(0)=fy(my)
    fy(my+1)=fy(1)
    do j=1,my
      scratch(i,j,k)=amax1(fy(j-1),fy(j),fy(j+1))
    end do
   end do
  end do
  !$omp barrier
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    km1=mod(k+mz-2,mz)+1
    kp1=mod(k+mz  ,mz)+1
    do j=1,my
    do i=1,mx
      fp(i,j,k)=amax1(scratch(i,j,k),scratch(i,j,km1),scratch(i,j,kp1))
    end do
   end do
  end do
END SUBROUTINE

!***********************************************************************
SUBROUTINE max5_set (f, fp, mx, my, mz, scratch)
!
!  Three point max
!
  implicit none
  integer i,j,k,km1,kp1,km2,kp2, mx, my, mz, izs, ize
  real, dimension(mx,my,mz), intent(in) :: f
  real, dimension(mx,my,mz):: fp, scratch
  real, dimension(-1:mx+2):: fx
  real, dimension(-1:my+2):: fy
!-----------------------------------------------------------------------
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
   do j=1,my
    do i=1,mx
      fx(i)=f(i,j,k)
    end do
    fx(  -1)=fx(mx-1)
    fx(   0)=fx(mx  )
    fx(mx+1)=fx(   1)
    fx(mx+2)=fx(   2)
    do i=1,mx
      scratch(i,j,k)=amax1(fx(i-2),fx(i-1),fx(i),fx(i+1),fx(i+2))
    end do
   end do
   if (my.gt.1) then
     do i=1,mx
      do j=1,my
        fy(j)=scratch(i,j,k)
      end do
      fy(  -1)=fy(my-1)
      fy(   0)=fy(my  )
      fy(my+1)=fy(   1)
      fy(my+2)=fy(   2)
      do j=1,my
        scratch(i,j,k)=amax1(fy(j-2),fy(j-1),fy(j),fy(j+1),fy(j+2))
      end do
     end do
    end if
  end do
  !$omp barrier
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    km2=mod(k+mz-3,mz)+1
    km1=mod(k+mz-2,mz)+1
    kp1=mod(k+mz  ,mz)+1
    kp2=mod(k+mz+1,mz)+1
    do j=1,my
    do i=1,mx
      fp(i,j,k)=amax1(scratch(i,j,km2),scratch(i,j,km1),scratch(i,j,k),scratch(i,j,kp1),scratch(i,j,kp2))
    end do
   end do
  end do
END SUBROUTINE

!***********************************************************************
SUBROUTINE smooth3max5_set (f, fz, mx, my, mz, scratch)
!
!  Three point max
!
  implicit none
  real c3
  integer i,j,k,km1,kp1,km2,kp2, mx, my, mz, izs, ize
  real, dimension(mx,my,mz) :: f, fz, scratch
  real, dimension(-1:mx+2):: fx
  real, dimension(-1:my+2):: fy
!-----------------------------------------------------------------------
!
  c3=1./3.
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
   do j=1,my
    do i=1,mx
      fx(i)=f(i,j,k)
    end do
    fx(  -1)=fx(mx-1)
    fx(   0)=fx(mx  )
    fx(mx+1)=fx(   1)
    fx(mx+2)=fx(   2)
    do i=1,mx
      fz(i,j,k)=amax1(fx(i-2),fx(i-1),fx(i),fx(i+1),fx(i+2))
    end do
   end do
   if (my.gt.1) then
     do i=1,mx
      do j=1,my
        fy(j)=fz(i,j,k)
      end do
      fy(  -1)=fy(my-1)
      fy(   0)=fy(my  )
      fy(my+1)=fy(   1)
      fy(my+2)=fy(   2)
      do j=1,my
        f(i,j,k)=amax1(fy(j-2),fy(j-1),fy(j),fy(j+1),fy(j+2))
      end do
     end do
    end if
  end do
  !$omp barrier
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    km2=mod(k+mz-3,mz)+1
    km1=mod(k+mz-2,mz)+1
    kp1=mod(k+mz  ,mz)+1
    kp2=mod(k+mz+1,mz)+1
    do j=1,my
      do i=1,mx
        fx(i)=amax1(f(i,j,km2),f(i,j,km1),f(i,j,k),f(i,j,kp1),f(i,j,kp2))
      end do
      fx(0)=fx(mx)
      fx(mx+1)=fx(1)
      do i=1,mx
        fz(i,j,k)=c3*(fx(i-1)+fx(i)+fx(i+1))
      end do
    end do
    do i=1,mx
      do j=1,my
        fy(j)=fz(i,j,k)
      end do
      fy(0)=fy(my)
      fy(my+1)=fy(1)
      do j=1,my
        fz(i,j,k)=c3*(fy(j-1)+fy(j)+fy(j+1))
      end do
    end do
  end do
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    km1=mod(k+mz-2,mz)+1
    kp1=mod(k+mz  ,mz)+1
    do j=1,my
      do i=1,mx
        f(i,j,k)=c3*(fz(i,j,k)+fz(i,j,km1)+fz(i,j,kp1))
      end do
    end do
  end do
END SUBROUTINE

!***********************************************************************
SUBROUTINE smooth3max3_set (f, fz, mx, my, mz, scratch)
!
!  Three point max
!
  implicit none
  real c3
  integer i,j,k,km1,kp1,km2,kp2, mx, my, mz, izs, ize
  real, dimension(mx,my,mz) :: f, fz, scratch
  real, dimension(0:mx+1):: fx
  real, dimension(0:my+1):: fy
!-----------------------------------------------------------------------
!
  c3=1./3.
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
   do j=1,my
    do i=1,mx
      fx(i)=f(i,j,k)
    end do
    fx(   0)=fx(mx  )
    fx(mx+1)=fx(   1)
    do i=1,mx
      fz(i,j,k)=amax1(fx(i-1),fx(i),fx(i+1))
    end do
   end do
   if (my.gt.1) then
     do i=1,mx
      do j=1,my
        fy(j)=fz(i,j,k)
      end do
      fy(   0)=fy(my  )
      fy(my+1)=fy(   1)
      do j=1,my
        f(i,j,k)=amax1(fy(j-1),fy(j),fy(j+1))
      end do
     end do
    end if
  end do
  !$omp barrier
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    km1=mod(k+mz-2,mz)+1
    kp1=mod(k+mz  ,mz)+1
    do j=1,my
      do i=1,mx
        fx(i)=amax1(f(i,j,km1),f(i,j,k),f(i,j,kp1))
      end do
      fx(0)=fx(mx)
      fx(mx+1)=fx(1)
      do i=1,mx
        fz(i,j,k)=c3*(fx(i-1)+fx(i)+fx(i+1))
      end do
    end do
    do i=1,mx
      do j=1,my
        fy(j)=fz(i,j,k)
      end do
      fy(0)=fy(my)
      fy(my+1)=fy(1)
      do j=1,my
        fz(i,j,k)=c3*(fy(j-1)+fy(j)+fy(j+1))
      end do
    end do
  end do
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    km1=mod(k+mz-2,mz)+1
    kp1=mod(k+mz  ,mz)+1
    do j=1,my
      do i=1,mx
        f(i,j,k)=c3*(fz(i,j,k)+fz(i,j,km1)+fz(i,j,kp1))
      end do
    end do
  end do
END SUBROUTINE

!***********************************************************************
SUBROUTINE laplace_add (f, fp, mx, my, mz)
!
!  Laplace operator
!

  implicit none
  real cx, cy, cz
  real, dimension(mx,my,mz):: f, fp
  integer i, j, k, km1, kp1, mx, my, mz, izs, ize
!-----------------------------------------------------------------------

  cx=1.
  cy=1.
  cz=1.
  if (mx.gt.1) then
  call limits_omp(1,mz,izs,ize)
    do k=izs,ize
      do j=1,my
        do i=2,mx-1
          fp(i,j,k) = fp(i,j,k) + cx*(f(i-1 ,j,k)+f(i+1,j,k)-2.*f(i,j,k))
        end do
        fp(1 ,j,k) = fp(1 ,j,k) + cx*(f(2,j,k)+f(mx  ,j,k)-2.*f(1 ,j,k))
        fp(mx,j,k) = fp(mx,j,k) + cx*(f(1,j,k)+f(mx-1,j,k)-2.*f(mx,j,k))
      end do
    end do
  else
  call limits_omp(1,mz,izs,ize)
    do k=izs,ize
      do j=1,my
        do i=1,mx
          fp(i,j,k) = fp(i,j,k) + 0.
        end do
      end do
    end do
  end if

 if (my .gt. 1) then
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    do j=2,my-1
      do i=1,mx
        fp(i,j,k) = fp(i,j,k) + fp(i,j,k) + cy*(f(i,j+1,k)+f(i,j-1,k)-2.*f(i,j,k))
      end do
    end do
    do i=1,mx
        fp(i,1 ,k) = fp(i,1 ,k) + fp(i,1 ,k) + cy*(f(i,2,k)+f(i,my  ,k)-2.*f(i,1 ,k))
        fp(i,my,k) = fp(i,my,k) + fp(i,my,k) + cy*(f(i,1,k)+f(i,my-1,k)-2.*f(i,my,k))
    end do
  end do
 end if

 if (mz .gt. 1) then
!$omp barrier
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    km1=mod(k-2+mz,mz)+1
    kp1=mod(k     ,mz)+1
    do j=1,my
      do i=1,mx
        fp(i,j,k) = fp(i,j,k) + fp(i,j,k) + cz*(f(i,j,kp1)+f(i,j,km1)-2.*f(i,j,k))
      end do
    end do
  end do
 end if
END SUBROUTINE

!***********************************************************************
SUBROUTINE dif2a_add (f, fp, mx, my, mz)
!
!  Max abs difference one zone left to one zones right.
!

  implicit none
  real cx, cy, cz
  real, dimension(mx,my,mz):: f, fp
  integer i, j, k, km1, kp1, mx, my, mz, izs, ize
!-----------------------------------------------------------------------

  if (mx.gt.1) then
  call limits_omp(1,mz,izs,ize)
    do k=izs,ize
      do j=1,my
        do i=2,mx-1
          fp(i,j,k) = fp(i,j,k) + abs(f(i-1 ,j,k)-f(i+1,j,k))
        end do
        fp(1 ,j,k) = fp(1 ,j,k) + abs(f(mx  ,j,k)-f(2  ,j,k))
        fp(mx,j,k) = fp(mx,j,k) + abs(f(mx-1,j,k)-f(1  ,j,k))
      end do
    end do
  else
  call limits_omp(1,mz,izs,ize)
    do k=izs,ize
      do j=1,my
        do i=1,mx
          fp(i,j,k) = fp(i,j,k) + 0.
        end do
      end do
    end do
  end if

 if (my .gt. 1) then
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    do j=2,my-1
      do i=1,mx
        fp(i,j, k) = fp(i,j, k) + max(fp(i,j, k),abs(f(i,j -1,k)-f(i,j+1,k)))
      end do
    end do
    do i=1,mx
        fp(i,1 ,k) = fp(i,1 ,k) + max(fp(i,1 ,k),abs(f(i,my  ,k)-f(i,  2,k)))
        fp(i,my,k) = fp(i,my,k) + max(fp(i,my,k),abs(f(i,my-1,k)-f(i,  1,k)))
    end do
  end do
 end if

 if (mz .gt. 1) then
!$omp barrier
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    km1=mod(k-2+mz,mz)+1
    kp1=mod(k     ,mz)+1
    do j=1,my
      do i=1,mx
        fp(i,j,k ) = fp(i,j,k ) + max(fp(i,j,k ),abs(f(i,j,km1)-f(i,j,kp1)))
      end do
    end do
  end do
 end if
END SUBROUTINE

!***********************************************************************
SUBROUTINE dif2_add (fx, fy, fz, fp, mx, my, mz)
!
!  Finite difference one zone left to one zones right.
!

  implicit none
  real cx(mx), cy(my), cz(mz)
  real, dimension(mx,my,mz):: fx, fy, fz, fp
  integer i, j, k, km1, kp1, mx, my, mz, izs, ize
!-----------------------------------------------------------------------

  cx=0.5
  cy=0.5
  cz=0.5

  if (mx.gt.1) then
  call limits_omp(1,mz,izs,ize)
    do k=izs,ize
      do j=1,my
        do i=2,mx-1
          fp(i,j,k) = fp(i,j,k) + cx(i)*(fx(i-1 ,j,k)-fx(i+1,j,k))
        end do
        fp(1 ,j,k) = fp(1 ,j,k) + cx(1 )*(fx(mx  ,j,k)-fx(2  ,j,k))
        fp(mx,j,k) = fp(mx,j,k) + cx(mx)*(fx(mx-1,j,k)-fx(1  ,j,k))
      end do
    end do
  else
  call limits_omp(1,mz,izs,ize)
    do k=izs,ize
      do j=1,my
        do i=1,mx
          fp(i,j,k) = fp(i,j,k) + 0.
        end do
      end do
    end do
  end if

 if (my .gt. 1) then
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    do j=2,my-1
      do i=1,mx
        fp(i,j, k) = fp(i,j, k) + fp(i,j, k) + cy(j)*(fy(i,j -1,k)-fy(i,j+1,k))
      end do
    end do
    do i=1,mx
        fp(i,1 ,k) = fp(i,1 ,k) + fp(i,1 ,k) + cy(1 )*(fy(i,my  ,k)-fy(i,  2,k))
        fp(i,my,k) = fp(i,my,k) + fp(i,my,k) + cy(my)*(fy(i,my-1,k)-fy(i,  1,k))
    end do
  end do
 end if

 if (mz .gt. 1) then
!$omp barrier
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    km1=mod(k-2+mz,mz)+1
    kp1=mod(k     ,mz)+1
    do j=1,my
      do i=1,mx
        fp(i,j,k ) = fp(i,j,k ) + fp(i,j,k ) + cz(k)*(fz(i,j,km1)-fz(i,j,kp1))
      end do
    end do
  end do
 end if

  call limits_omp(1,mz,izs,ize)
 do k=izs,ize
   do j=1,my
     do i=1,mx
       fp(i,j,k) = fp(i,j,k) + max (0., fp(i,j,k))
     end do
   end do
 end do

END SUBROUTINE

!***********************************************************************
SUBROUTINE dif2d_add (fx, fy, fz, fp, mx, my, mz)
!
!  Finite difference one zone left to one zones right.
!

  implicit none
  real, dimension(mx,my,mz):: fx, fy, fz, fp
  integer i, j, k, km1, kp1, mx, my, mz, izs, ize
!-----------------------------------------------------------------------

  if (mx.gt.1) then
  call limits_omp(1,mz,izs,ize)
    do k=izs,ize
      do j=1,my
        do i=2,mx-1
          fp(i,j,k) = fp(i,j,k) + (fx(i-1 ,j,k)-fx(i+1,j,k))
        end do
        fp(1 ,j,k) = fp(1 ,j,k) + (fx(mx  ,j,k)-fx(2  ,j,k))
        fp(mx,j,k) = fp(mx,j,k) + (fx(mx-1,j,k)-fx(1  ,j,k))
      end do
    end do
  else
  call limits_omp(1,mz,izs,ize)
    do k=izs,ize
      do j=1,my
        do i=1,mx
          fp(i,j,k) = fp(i,j,k) + 0.
        end do
      end do
    end do
  end if

 if (my .gt. 1) then
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    do j=2,my-1
      do i=1,mx
        fp(i,j, k) = fp(i,j, k) + fp(i,j, k) + (fy(i,j -1,k)-fy(i,j+1,k))
      end do
    end do
    do i=1,mx
        fp(i,1 ,k) = fp(i,1 ,k) + fp(i,1 ,k) + (fy(i,my  ,k)-fy(i,  2,k))
        fp(i,my,k) = fp(i,my,k) + fp(i,my,k) + (fy(i,my-1,k)-fy(i,  1,k))
    end do
  end do
 end if

 if (mz .gt. 1) then
!$omp barrier
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    km1=mod(k-2+mz,mz)+1
    kp1=mod(k     ,mz)+1
    do j=1,my
      do i=1,mx
        fp(i,j,k ) = fp(i,j,k ) + fp(i,j,k ) + (fz(i,j,km1)-fz(i,j,kp1))
      end do
    end do
  end do
 end if

  call limits_omp(1,mz,izs,ize)
 do k=izs,ize
   do j=1,my
     do i=1,mx
       fp(i,j,k) = fp(i,j,k) + max (0., fp(i,j,k))
     end do
   end do
 end do

END SUBROUTINE

!***********************************************************************
SUBROUTINE dif1_add (fx, fy, fz, fp, mx, my, mz)
!
!  Finite difference one zone left to one zones right.
!

  implicit none
  real, dimension(mx,my,mz):: fx, fy, fz, fp
  integer i, j, k, kp1, mx, my, mz, izs, ize
!-----------------------------------------------------------------------

  if (mx.gt.1) then
  call limits_omp(1,mz,izs,ize)
    do k=izs,ize
      do j=1,my
        do i=1,mx-1
          fp(i,j,k) = fp(i,j,k) + (fx( i,j,k)-fx(i+1,j,k))
        end do
        fp(mx,j,k) = fp(mx,j,k) + (fx(mx,j,k)-fx(  1,j,k))
      end do
    end do
  else
  call limits_omp(1,mz,izs,ize)
    do k=izs,ize
      do j=1,my
        do i=1,mx
          fp(i,j,k) = fp(i,j,k) + 0.
        end do
      end do
    end do
  end if

 if (my .gt. 1) then
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    do j=1,my-1
      do i=1,mx
        fp(i, j,k) = fp(i, j,k) + fp(i, j,k) + (fy(i, j,k)-fy(i,j+1,k))
      end do
    end do
    do i=1,mx
        fp(i,my,k) = fp(i,my,k) + fp(i,my,k) + (fy(i,my,k)-fy(i,  1,k))
    end do
  end do
 end if

 if (mz .gt. 1) then
!$omp barrier
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    kp1=mod(k,mz)+1
    do j=1,my
      do i=1,mx
        fp(i,j,k) = fp(i,j,k) + fp(i,j,k) + (fz(i,j,k)-fz(i,j,kp1))
      end do
    end do
  end do
 end if

  call limits_omp(1,mz,izs,ize)
 do k=izs,ize
   do j=1,my
     do i=1,mx
       fp(i,j,k) = fp(i,j,k) + max (0., fp(i,j,k))
     end do
   end do
 end do

END SUBROUTINE

!***********************************************************************
SUBROUTINE xup_add (f, fp, mx, my, mz)
!
!  f is centered on (i-.5,j,k), fifth order stagger
!
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!-----------------------------------------------------------------------
!
!
  if (mx.le.5) then
    fp = fp + f
    return
  end if
  c = 3./256.
  b = -25./256.
  a = .5-b-c
  a1st = 0.5
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
   do j=1,my
    fp(mx-2,j,k) = fp(mx-2,j,k) + ( &
                   a*(f(mx-2,j,k)+f(mx-1,j,k)) &
                 + b*(f(mx-3,j,k)+f(mx  ,j,k)) &
                 + c*(f(mx-4,j,k)+f(1   ,j,k)))
    fp(mx-1,j,k) = fp(mx-1,j,k) + ( &
                   a*(f(mx-1,j,k)+f(mx  ,j,k)) &
                 + b*(f(mx-2,j,k)+f(1   ,j,k)) &
                 + c*(f(mx-3,j,k)+f(2   ,j,k)))
    fp(mx  ,j,k) = fp(mx  ,j,k) + ( &
                   a*(f(mx  ,j,k)+f(1   ,j,k)) &
                 + b*(f(mx-1,j,k)+f(2   ,j,k)) &
                 + c*(f(mx-2,j,k)+f(3   ,j,k)))
    fp(1   ,j,k) = fp(1   ,j,k) + ( &
                   a*(f(1   ,j,k)+f(2   ,j,k)) &
                 + b*(f(mx  ,j,k)+f(3   ,j,k)) &
                 + c*(f(mx-1,j,k)+f(4   ,j,k)))
    fp(2   ,j,k) = fp(2   ,j,k) + ( &
                   a*(f(2   ,j,k)+f(3   ,j,k)) &
                 + b*(f(1   ,j,k)+f(4   ,j,k)) &
                 + c*(f(mx  ,j,k)+f(5   ,j,k)))
   end do
   do j=1,my
    do i=3,mx-3
      fp(i  ,j,k) = fp(i  ,j,k) + ( &
                    a*(f(i  ,j,k)+f(i+1 ,j,k)) &
                  + b*(f(i-1,j,k)+f(i+2 ,j,k)) &
                  + c*(f(i-2,j,k)+f(i+3 ,j,k)))
    end do
   end do
  end do
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE xup1_add (f, fp, mx, my, mz)
!
!  f is centered on (i-.5,j,k), fifth order stagger
!
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = fp + f
    return
  end if
!
  a = .5
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
   do j=1,my
    fp(mx  ,j,k) = fp(mx  ,j,k) + ( &
                   a*(f(mx  ,j,k)+f(1   ,j,k)))
   end do
   do j=1,my
    do i=1,mx-1
      fp(i  ,j,k) = fp(i  ,j,k) + ( &
                    a*(f(i  ,j,k)+f(i+1 ,j,k)))
    end do
   end do
  end do
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE xdn_add (f, fp, mx, my, mz)
!
!  f is centered on (i,j,k), fifth order stagger
!
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = fp + f
    return
  end if
!
  c = 3./256.
  b = -25./256.
  a = .5-b-c
  a1st = 0.5
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
   do j=1,my
    fp(mx-1,j,k) = fp(mx-1,j,k) + ( &
                   a*(f(mx-2,j,k)+f(mx-1,j,k)) &
                 + b*(f(mx-3,j,k)+f(mx  ,j,k)) &
                 + c*(f(mx-4,j,k)+f(1   ,j,k)))
    fp(mx  ,j,k) = fp(mx  ,j,k) + ( &
                   a*(f(mx-1,j,k)+f(mx  ,j,k)) &
                 + b*(f(mx-2,j,k)+f(1   ,j,k)) &
                 + c*(f(mx-3,j,k)+f(2   ,j,k)))
    fp(1  ,j,k) = fp(1  ,j,k) + ( &
                   a*(f(mx  ,j,k)+f(1   ,j,k)) &
                 + b*(f(mx-1,j,k)+f(2   ,j,k)) &
                 + c*(f(mx-2,j,k)+f(3   ,j,k)))
    fp(2  ,j,k) = fp(2  ,j,k) + ( &
                   a*(f(1   ,j,k)+f(2   ,j,k)) &
                 + b*(f(mx  ,j,k)+f(3   ,j,k)) &
                 + c*(f(mx-1,j,k)+f(4   ,j,k)))
    fp(3  ,j,k) = fp(3  ,j,k) + ( &
                   a*(f(2   ,j,k)+f(3   ,j,k)) &
                 + b*(f(1   ,j,k)+f(4   ,j,k)) &
                 + c*(f(mx  ,j,k)+f(5   ,j,k)))
   end do
   do j=1,my
    do i=4,mx-2
      fp(i,j,k) = fp(i,j,k) + ( &
                   a*(f(i   ,j,k)+f(i-1 ,j,k)) &
                 + b*(f(i+1 ,j,k)+f(i-2 ,j,k)) &
                 + c*(f(i+2 ,j,k)+f(i-3 ,j,k)))
    end do
   end do
  end do
!
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE xdn1_add (f, fp, mx, my, mz)
!
!  f is centered on (i,j,k), fifth order stagger
!
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = fp + f
    return
  end if
!
  a = 0.5
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
   do j=1,my
    fp(1  ,j,k) = fp(1  ,j,k) + ( &
                   a*(f(mx  ,j,k)+f(1   ,j,k)))
   end do
   do j=1,my
    do i=2,mx
      fp(i,j,k) = fp(i,j,k) + ( &
                   a*(f(i   ,j,k)+f(i-1 ,j,k)))
    end do
   end do
  end do
!
END SUBROUTINE
!***********************************************************************
SUBROUTINE xup32_add (f,j, fp, mx, my, mz)
!
!  f is centered on (i-.5,j,k), fifth order stagger
!
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f
  real, dimension(mx,mz):: fp
!-----------------------------------------------------------------------
!
!
  if (mx.le.5) then
    fp = fp + f(:,j,:)
    return
  end if
  c = 3./256.
  b = -25./256.
  a = .5-b-c
  a1st = 0.5
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    fp(mx-2,k) = fp(mx-2,k) + ( &
                   a*(f(mx-2,j,k)+f(mx-1,j,k)) &
                 + b*(f(mx-3,j,k)+f(mx  ,j,k)) &
                 + c*(f(mx-4,j,k)+f(1   ,j,k)))
    fp(mx-1,k) = fp(mx-1,k) + ( &
                   a*(f(mx-1,j,k)+f(mx  ,j,k)) &
                 + b*(f(mx-2,j,k)+f(1   ,j,k)) &
                 + c*(f(mx-3,j,k)+f(2   ,j,k)))
    fp(mx  ,k) = fp(mx  ,k) + ( &
                   a*(f(mx  ,j,k)+f(1   ,j,k)) &
                 + b*(f(mx-1,j,k)+f(2   ,j,k)) &
                 + c*(f(mx-2,j,k)+f(3   ,j,k)))
    fp(1   ,k) = fp(1   ,k) + ( &
                   a*(f(1   ,j,k)+f(2   ,j,k)) &
                 + b*(f(mx  ,j,k)+f(3   ,j,k)) &
                 + c*(f(mx-1,j,k)+f(4   ,j,k)))
    fp(2   ,k) = fp(2   ,k) + ( &
                   a*(f(2   ,j,k)+f(3   ,j,k)) &
                 + b*(f(1   ,j,k)+f(4   ,j,k)) &
                 + c*(f(mx  ,j,k)+f(5   ,j,k)))
    do i=3,mx-3
      fp(i  ,k) = fp(i  ,k) + ( &
                    a*(f(i  ,j,k)+f(i+1 ,j,k)) &
                  + b*(f(i-1,j,k)+f(i+2 ,j,k)) &
                  + c*(f(i-2,j,k)+f(i+3 ,j,k)))
    end do
  end do
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE xup132_add (f,j, fp, mx, my, mz)
!
!  f is centered on (i-.5,j,k), fifth order stagger
!
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f
  real, dimension(mx,mz):: fp
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = fp + f(:,j,:)
    return
  end if
!
  a = .5
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    fp(mx  ,k) = fp(mx  ,k) + ( &
                   a*(f(mx  ,j,k)+f(1   ,j,k)))
    do i=1,mx-1
      fp(i  ,k) = fp(i  ,k) + ( &
                    a*(f(i  ,j,k)+f(i+1 ,j,k)))
    end do
  end do
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE xdn32_add (f,j, fp, mx, my, mz)
!
!  f is centered on (i,j,k), fifth order stagger
!
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f
  real, dimension(mx,mz):: fp
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = fp + f(:,j,:)
    return
  end if
!
  c = 3./256.
  b = -25./256.
  a = .5-b-c
  a1st = 0.5
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    fp(mx-1,k) = fp(mx-1,k) + ( &
                   a*(f(mx-2,j,k)+f(mx-1,j,k)) &
                 + b*(f(mx-3,j,k)+f(mx  ,j,k)) &
                 + c*(f(mx-4,j,k)+f(1   ,j,k)))
    fp(mx  ,k) = fp(mx  ,k) + ( &
                   a*(f(mx-1,j,k)+f(mx  ,j,k)) &
                 + b*(f(mx-2,j,k)+f(1   ,j,k)) &
                 + c*(f(mx-3,j,k)+f(2   ,j,k)))
    fp(1  ,k) = fp(1  ,k) + ( &
                   a*(f(mx  ,j,k)+f(1   ,j,k)) &
                 + b*(f(mx-1,j,k)+f(2   ,j,k)) &
                 + c*(f(mx-2,j,k)+f(3   ,j,k)))
    fp(2  ,k) = fp(2  ,k) + ( &
                   a*(f(1   ,j,k)+f(2   ,j,k)) &
                 + b*(f(mx  ,j,k)+f(3   ,j,k)) &
                 + c*(f(mx-1,j,k)+f(4   ,j,k)))
    fp(3  ,k) = fp(3  ,k) + ( &
                   a*(f(2   ,j,k)+f(3   ,j,k)) &
                 + b*(f(1   ,j,k)+f(4   ,j,k)) &
                 + c*(f(mx  ,j,k)+f(5   ,j,k)))
    do i=4,mx-2
      fp(i,k) = fp(i,k) + ( &
                   a*(f(i   ,j,k)+f(i-1 ,j,k)) &
                 + b*(f(i+1 ,j,k)+f(i-2 ,j,k)) &
                 + c*(f(i+2 ,j,k)+f(i-3 ,j,k)))
    end do
  end do
!
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE xdn132_add (f,j, fp, mx, my, mz)
!
!  f is centered on (i,j,k), fifth order stagger
!
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f
  real, dimension(mx,mz):: fp
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = fp + f(:,j,:)
    return
  end if
!
  a = 0.5
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    fp(1  ,k) = fp(1  ,k) + ( &
                   a*(f(mx  ,j,k)+f(1   ,j,k)))
    do i=2,mx
      fp(i,k) = fp(i,k) + ( &
                   a*(f(i   ,j,k)+f(i-1 ,j,k)))
    end do
  end do
!
END SUBROUTINE
!***********************************************************************
SUBROUTINE xup22_add (f, fp, mx, my, mz)
!
!  f is centered on (i-.5,k), fifth order stagger
!
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,mz):: fp, f
!-----------------------------------------------------------------------
!
!
  if (mx.le.5) then
    fp = fp + f
    return
  end if
  c = 3./256.
  b = -25./256.
  a = .5-b-c
  a1st = 0.5
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    fp(mx-2,k) = fp(mx-2,k) + ( &
                   a*(f(mx-2,k)+f(mx-1,k)) &
                 + b*(f(mx-3,k)+f(mx  ,k)) &
                 + c*(f(mx-4,k)+f(1   ,k)))
    fp(mx-1,k) = fp(mx-1,k) + ( &
                   a*(f(mx-1,k)+f(mx  ,k)) &
                 + b*(f(mx-2,k)+f(1   ,k)) &
                 + c*(f(mx-3,k)+f(2   ,k)))
    fp(mx  ,k) = fp(mx  ,k) + ( &
                   a*(f(mx  ,k)+f(1   ,k)) &
                 + b*(f(mx-1,k)+f(2   ,k)) &
                 + c*(f(mx-2,k)+f(3   ,k)))
    fp(1   ,k) = fp(1   ,k) + ( &
                   a*(f(1   ,k)+f(2   ,k)) &
                 + b*(f(mx  ,k)+f(3   ,k)) &
                 + c*(f(mx-1,k)+f(4   ,k)))
    fp(2   ,k) = fp(2   ,k) + ( &
                   a*(f(2   ,k)+f(3   ,k)) &
                 + b*(f(1   ,k)+f(4   ,k)) &
                 + c*(f(mx  ,k)+f(5   ,k)))
    do i=3,mx-3
      fp(i  ,k) = fp(i  ,k) + ( &
                    a*(f(i  ,k)+f(i+1 ,k)) &
                  + b*(f(i-1,k)+f(i+2 ,k)) &
                  + c*(f(i-2,k)+f(i+3 ,k)))
    end do
  end do
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE xup122_add (f, fp, mx, my, mz)
!
!  f is centered on (i-.5,k), fifth order stagger
!
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,mz):: fp, f
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = fp + f
    return
  end if
!
  a = .5
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    fp(mx  ,k) = fp(mx  ,k) + ( &
                   a*(f(mx  ,k)+f(1   ,k)))
    do i=1,mx-1
      fp(i  ,k) = fp(i  ,k) + ( &
                    a*(f(i  ,k)+f(i+1 ,k)))
    end do
  end do
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE xdn22_add (f, fp, mx, my, mz)
!
!  f is centered on (i,k), fifth order stagger
!
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,mz):: fp, f
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = fp + f
    return
  end if
!
  c = 3./256.
  b = -25./256.
  a = .5-b-c
  a1st = 0.5
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    fp(mx-1,k) = fp(mx-1,k) + ( &
                   a*(f(mx-2,k)+f(mx-1,k)) &
                 + b*(f(mx-3,k)+f(mx  ,k)) &
                 + c*(f(mx-4,k)+f(1   ,k)))
    fp(mx  ,k) = fp(mx  ,k) + ( &
                   a*(f(mx-1,k)+f(mx  ,k)) &
                 + b*(f(mx-2,k)+f(1   ,k)) &
                 + c*(f(mx-3,k)+f(2   ,k)))
    fp(1  ,k) = fp(1  ,k) + ( &
                   a*(f(mx  ,k)+f(1   ,k)) &
                 + b*(f(mx-1,k)+f(2   ,k)) &
                 + c*(f(mx-2,k)+f(3   ,k)))
    fp(2  ,k) = fp(2  ,k) + ( &
                   a*(f(1   ,k)+f(2   ,k)) &
                 + b*(f(mx  ,k)+f(3   ,k)) &
                 + c*(f(mx-1,k)+f(4   ,k)))
    fp(3  ,k) = fp(3  ,k) + ( &
                   a*(f(2   ,k)+f(3   ,k)) &
                 + b*(f(1   ,k)+f(4   ,k)) &
                 + c*(f(mx  ,k)+f(5   ,k)))
    do i=4,mx-2
      fp(i,k) = fp(i,k) + ( &
                   a*(f(i   ,k)+f(i-1 ,k)) &
                 + b*(f(i+1 ,k)+f(i-2 ,k)) &
                 + c*(f(i+2 ,k)+f(i-3 ,k)))
    end do
  end do
!
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE xdn122_add (f, fp, mx, my, mz)
!
!  f is centered on (i,k), fifth order stagger
!
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,mz):: fp, f
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = fp + f
    return
  end if
!
  a = 0.5
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    fp(1  ,k) = fp(1  ,k) + ( &
                   a*(f(mx  ,k)+f(1   ,k)))
    do i=2,mx
      fp(i,k) = fp(i,k) + ( &
                   a*(f(i   ,k)+f(i-1 ,k)))
    end do
  end do
!
END SUBROUTINE
!***********************************************************************
SUBROUTINE ddxupi_add (f, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = fp + 0.
    return
  end if
!
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)
  a1st = 1.
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
   do j=1,my
    fp(mx-2,j,k) = fp(mx-2,j,k) + ( &
                   a*(f(mx-1,j,k)-f(mx-2,j,k)) &
                 + b*(f(mx  ,j,k)-f(mx-3,j,k)) &
                 + c*(f(1   ,j,k)-f(mx-4,j,k)))
    fp(mx-1,j,k) = fp(mx-1,j,k) + ( &
                   a*(f(mx  ,j,k)-f(mx-1,j,k)) &
                 + b*(f(1   ,j,k)-f(mx-2,j,k)) &
                 + c*(f(2   ,j,k)-f(mx-3,j,k)))
    fp(mx  ,j,k) = fp(mx  ,j,k) + ( &
                   a*(f(1   ,j,k)-f(mx  ,j,k)) &
                 + b*(f(2   ,j,k)-f(mx-1,j,k)) &
                 + c*(f(3   ,j,k)-f(mx-2,j,k)))
    fp(1  ,j,k) = fp(1  ,j,k) + ( &
                   a*(f(2   ,j,k)-f(1   ,j,k)) &
                 + b*(f(3   ,j,k)-f(mx  ,j,k)) &
                 + c*(f(4   ,j,k)-f(mx-1,j,k)))
    fp(2  ,j,k) = fp(2  ,j,k) + ( &
                   a*(f(3   ,j,k)-f(2  ,j,k)) &
                 + b*(f(4   ,j,k)-f(1  ,j,k)) &
                 + c*(f(5   ,j,k)-f(mx ,j,k)))
   end do
   do j=1,my
    do i=3,mx-3
      fp(i  ,j,k) = fp(i  ,j,k) + ( &
                   a*(f(i+1 ,j,k)-f(i   ,j,k)) &
                 + b*(f(i+2 ,j,k)-f(i-1 ,j,k)) &
                 + c*(f(i+3 ,j,k)-f(i-2 ,j,k)))
    end do
   end do
  end do
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddxupi1_add (f, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = fp + 0.
    return
  end if
!
  a = 1.
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
   do j=1,my
    fp(mx  ,j,k) = fp(mx  ,j,k) + ( &
                   a*(f(1   ,j,k)-f(mx  ,j,k)))
   end do
   do j=1,my
    do i=1,mx-1
      fp(i  ,j,k) = fp(i  ,j,k) + ( &
                   a*(f(i+1 ,j,k)-f(i   ,j,k)))
    end do
   end do
  end do
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddxdni_add (f, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = fp + 0.
    return
  end if
!
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)
  a1st = 1.
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
   do j=1,my
    fp(mx-1,j,k) = fp(mx-1,j,k) + ( &
                   a*(f(mx-1,j,k)-f(mx-2,j,k)) &
                 + b*(f(mx  ,j,k)-f(mx-3,j,k)) &
                 + c*(f(1   ,j,k)-f(mx-4,j,k)))
    fp(mx  ,j,k) = fp(mx  ,j,k) + ( &
                   a*(f(mx  ,j,k)-f(mx-1,j,k)) &
                 + b*(f(1   ,j,k)-f(mx-2,j,k)) &
                 + c*(f(2   ,j,k)-f(mx-3,j,k)))
    fp(1   ,j,k) = fp(1   ,j,k) + ( &
                   a*(f(1   ,j,k)-f(mx  ,j,k)) &
                 + b*(f(2   ,j,k)-f(mx-1,j,k)) &
                 + c*(f(3   ,j,k)-f(mx-2,j,k)))
    fp(2  ,j,k) = fp(2  ,j,k) + ( &
                   a*(f(2   ,j,k)-f(1   ,j,k)) &
                 + b*(f(3   ,j,k)-f(mx  ,j,k)) &
                 + c*(f(4   ,j,k)-f(mx-1,j,k)))
    fp(3  ,j,k) = fp(3  ,j,k) + ( &
                   a*(f(3   ,j,k)-f(2   ,j,k)) &
                 + b*(f(4   ,j,k)-f(1   ,j,k)) &
                 + c*(f(5   ,j,k)-f(mx  ,j,k)))
   end do
   do j=1,my
    do i=4,mx-2
      fp(i,j,k) = fp(i,j,k) + ( &
                   a*(f(i   ,j,k)-f(i-1 ,j,k)) &
                 + b*(f(i+1 ,j,k)-f(i-2 ,j,k)) &
                 + c*(f(i+2 ,j,k)-f(i-3 ,j,k)))
    end do
   end do
  end do
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddxdni1_add (f, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = fp + 0.
    return
  end if
!
  a = 1.
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
   do j=1,my
    fp(1   ,j,k) = fp(1   ,j,k) + ( &
                   a*(f(1   ,j,k)-f(mx  ,j,k)))
   end do
   do j=1,my
    do i=2,mx
      fp(i,j,k) = fp(i,j,k) + ( &
                   a*(f(i   ,j,k)-f(i-1 ,j,k)))
    end do
   end do
  end do
!
END SUBROUTINE
!***********************************************************************
SUBROUTINE ddxupi32_add (f,j, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f
  real, dimension(mx,mz):: fp
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = fp + 0.
    return
  end if
!
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)
  a1st = 1.
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    fp(mx-2,k) = fp(mx-2,k) + ( &
                   a*(f(mx-1,j,k)-f(mx-2,j,k)) &
                 + b*(f(mx  ,j,k)-f(mx-3,j,k)) &
                 + c*(f(1   ,j,k)-f(mx-4,j,k)))
    fp(mx-1,k) = fp(mx-1,k) + ( &
                   a*(f(mx  ,j,k)-f(mx-1,j,k)) &
                 + b*(f(1   ,j,k)-f(mx-2,j,k)) &
                 + c*(f(2   ,j,k)-f(mx-3,j,k)))
    fp(mx  ,k) = fp(mx  ,k) + ( &
                   a*(f(1   ,j,k)-f(mx  ,j,k)) &
                 + b*(f(2   ,j,k)-f(mx-1,j,k)) &
                 + c*(f(3   ,j,k)-f(mx-2,j,k)))
    fp(1  ,k) = fp(1  ,k) + ( &
                   a*(f(2   ,j,k)-f(1   ,j,k)) &
                 + b*(f(3   ,j,k)-f(mx  ,j,k)) &
                 + c*(f(4   ,j,k)-f(mx-1,j,k)))
    fp(2  ,k) = fp(2  ,k) + ( &
                   a*(f(3   ,j,k)-f(2  ,j,k)) &
                 + b*(f(4   ,j,k)-f(1  ,j,k)) &
                 + c*(f(5   ,j,k)-f(mx ,j,k)))
    do i=3,mx-3
      fp(i  ,k) = fp(i  ,k) + ( &
                   a*(f(i+1 ,j,k)-f(i   ,j,k)) &
                 + b*(f(i+2 ,j,k)-f(i-1 ,j,k)) &
                 + c*(f(i+3 ,j,k)-f(i-2 ,j,k)))
    end do
  end do
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddxupi132_add (f,j, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f
  real, dimension(mx,mz):: fp
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = fp + 0.
    return
  end if
!
  a = 1.
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    fp(mx  ,k) = fp(mx  ,k) + ( &
                   a*(f(1   ,j,k)-f(mx  ,j,k)))
    do i=1,mx-1
      fp(i  ,k) = fp(i  ,k) + ( &
                   a*(f(i+1 ,j,k)-f(i   ,j,k)))
    end do
  end do
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddxdni32_add (f,j, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f
  real, dimension(mx,mz):: fp
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = fp + 0.
    return
  end if
!
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)
  a1st = 1.
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    fp(mx-1,k) = fp(mx-1,k) + ( &
                   a*(f(mx-1,j,k)-f(mx-2,j,k)) &
                 + b*(f(mx  ,j,k)-f(mx-3,j,k)) &
                 + c*(f(1   ,j,k)-f(mx-4,j,k)))
    fp(mx  ,k) = fp(mx  ,k) + ( &
                   a*(f(mx  ,j,k)-f(mx-1,j,k)) &
                 + b*(f(1   ,j,k)-f(mx-2,j,k)) &
                 + c*(f(2   ,j,k)-f(mx-3,j,k)))
    fp(1   ,k) = fp(1   ,k) + ( &
                   a*(f(1   ,j,k)-f(mx  ,j,k)) &
                 + b*(f(2   ,j,k)-f(mx-1,j,k)) &
                 + c*(f(3   ,j,k)-f(mx-2,j,k)))
    fp(2  ,k) = fp(2  ,k) + ( &
                   a*(f(2   ,j,k)-f(1   ,j,k)) &
                 + b*(f(3   ,j,k)-f(mx  ,j,k)) &
                 + c*(f(4   ,j,k)-f(mx-1,j,k)))
    fp(3  ,k) = fp(3  ,k) + ( &
                   a*(f(3   ,j,k)-f(2   ,j,k)) &
                 + b*(f(4   ,j,k)-f(1   ,j,k)) &
                 + c*(f(5   ,j,k)-f(mx  ,j,k)))
    do i=4,mx-2
      fp(i,k) = fp(i,k) + ( &
                   a*(f(i   ,j,k)-f(i-1 ,j,k)) &
                 + b*(f(i+1 ,j,k)-f(i-2 ,j,k)) &
                 + c*(f(i+2 ,j,k)-f(i-3 ,j,k)))
    end do
  end do
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddxdni132_add (f,j, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f
  real, dimension(mx,mz):: fp
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = fp + 0.
    return
  end if
!
  a = 1.
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    fp(1   ,k) = fp(1   ,k) + ( &
                   a*(f(1   ,j,k)-f(mx  ,j,k)))
    do i=2,mx
      fp(i,k) = fp(i,k) + ( &
                   a*(f(i   ,j,k)-f(i-1 ,j,k)))
    end do
  end do
!
END SUBROUTINE
!***********************************************************************
SUBROUTINE ddxupi22_add (f, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,mz):: fp, f
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = fp + 0.
    return
  end if
!
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)
  a1st = 1.
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    fp(mx-2,k) = fp(mx-2,k) + ( &
                   a*(f(mx-1,k)-f(mx-2,k)) &
                 + b*(f(mx  ,k)-f(mx-3,k)) &
                 + c*(f(1   ,k)-f(mx-4,k)))
    fp(mx-1,k) = fp(mx-1,k) + ( &
                   a*(f(mx  ,k)-f(mx-1,k)) &
                 + b*(f(1   ,k)-f(mx-2,k)) &
                 + c*(f(2   ,k)-f(mx-3,k)))
    fp(mx  ,k) = fp(mx  ,k) + ( &
                   a*(f(1   ,k)-f(mx  ,k)) &
                 + b*(f(2   ,k)-f(mx-1,k)) &
                 + c*(f(3   ,k)-f(mx-2,k)))
    fp(1  ,k) = fp(1  ,k) + ( &
                   a*(f(2   ,k)-f(1   ,k)) &
                 + b*(f(3   ,k)-f(mx  ,k)) &
                 + c*(f(4   ,k)-f(mx-1,k)))
    fp(2  ,k) = fp(2  ,k) + ( &
                   a*(f(3   ,k)-f(2  ,k)) &
                 + b*(f(4   ,k)-f(1  ,k)) &
                 + c*(f(5   ,k)-f(mx ,k)))
    do i=3,mx-3
      fp(i  ,k) = fp(i  ,k) + ( &
                   a*(f(i+1 ,k)-f(i   ,k)) &
                 + b*(f(i+2 ,k)-f(i-1 ,k)) &
                 + c*(f(i+3 ,k)-f(i-2 ,k)))
    end do
  end do
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddxupi122_add (f, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,mz):: fp, f
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = fp + 0.
    return
  end if
!
  a = 1.
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    fp(mx  ,k) = fp(mx  ,k) + ( &
                   a*(f(1   ,k)-f(mx  ,k)))
    do i=1,mx-1
      fp(i  ,k) = fp(i  ,k) + ( &
                   a*(f(i+1 ,k)-f(i   ,k)))
    end do
  end do
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddxdni22_add (f, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,mz):: fp, f
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = fp + 0.
    return
  end if
!
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)
  a1st = 1.
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    fp(mx-1,k) = fp(mx-1,k) + ( &
                   a*(f(mx-1,k)-f(mx-2,k)) &
                 + b*(f(mx  ,k)-f(mx-3,k)) &
                 + c*(f(1   ,k)-f(mx-4,k)))
    fp(mx  ,k) = fp(mx  ,k) + ( &
                   a*(f(mx  ,k)-f(mx-1,k)) &
                 + b*(f(1   ,k)-f(mx-2,k)) &
                 + c*(f(2   ,k)-f(mx-3,k)))
    fp(1   ,k) = fp(1   ,k) + ( &
                   a*(f(1   ,k)-f(mx  ,k)) &
                 + b*(f(2   ,k)-f(mx-1,k)) &
                 + c*(f(3   ,k)-f(mx-2,k)))
    fp(2  ,k) = fp(2  ,k) + ( &
                   a*(f(2   ,k)-f(1   ,k)) &
                 + b*(f(3   ,k)-f(mx  ,k)) &
                 + c*(f(4   ,k)-f(mx-1,k)))
    fp(3  ,k) = fp(3  ,k) + ( &
                   a*(f(3   ,k)-f(2   ,k)) &
                 + b*(f(4   ,k)-f(1   ,k)) &
                 + c*(f(5   ,k)-f(mx  ,k)))
    do i=4,mx-2
      fp(i,k) = fp(i,k) + ( &
                   a*(f(i   ,k)-f(i-1 ,k)) &
                 + b*(f(i+1 ,k)-f(i-2 ,k)) &
                 + c*(f(i+2 ,k)-f(i-3 ,k)))
    end do
  end do
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddxdni122_add (f, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,mz):: fp, f
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = fp + 0.
    return
  end if
!
  a = 1.
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    fp(1   ,k) = fp(1   ,k) + ( &
                   a*(f(1   ,k)-f(mx  ,k)))
    do i=2,mx
      fp(i,k) = fp(i,k) + ( &
                   a*(f(i   ,k)-f(i-1 ,k)))
    end do
  end do
!
END SUBROUTINE
!***********************************************************************
SUBROUTINE ddxup_add (f, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = fp + 0.
    return
  end if
!
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)
  a1st = 1.
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
   do j=1,my
    fp(mx-2,j,k) = fp(mx-2,j,k) + ( &
                 + c*(f(1   ,j,k)-f(mx-4,j,k)) &
                 + b*(f(mx  ,j,k)-f(mx-3,j,k)) &
                 + a*(f(mx-1,j,k)-f(mx-2,j,k)))
    fp(mx-1,j,k) = fp(mx-1,j,k) + ( &
                 + c*(f(2   ,j,k)-f(mx-3,j,k)) &
                 + b*(f(1   ,j,k)-f(mx-2,j,k)) &
                 + a*(f(mx  ,j,k)-f(mx-1,j,k)))
    fp(mx  ,j,k) = fp(mx  ,j,k) + ( &
                 + c*(f(3   ,j,k)-f(mx-2,j,k)) &
                 + b*(f(2   ,j,k)-f(mx-1,j,k)) &
                 + a*(f(1   ,j,k)-f(mx  ,j,k)))
    fp(1  ,j,k) = fp(1  ,j,k) + ( &
                 + c*(f(4   ,j,k)-f(mx-1,j,k)) &
                 + b*(f(3   ,j,k)-f(mx  ,j,k)) &
                 + a*(f(2   ,j,k)-f(1   ,j,k)))
    fp(2  ,j,k) = fp(2  ,j,k) + ( &
                 + c*(f(5   ,j,k)-f(mx  ,j,k)) &
                 + b*(f(4   ,j,k)-f(1   ,j,k)) &
                 + a*(f(3   ,j,k)-f(2   ,j,k)))
   end do
   do j=1,my
    do i=3,mx-3
      fp(i  ,j,k) = fp(i  ,j,k) + ( &
                 + c*(f(i+3 ,j,k)-f(i-2 ,j,k)) &
                 + b*(f(i+2 ,j,k)-f(i-1 ,j,k)) &
                 + a*(f(i+1 ,j,k)-f(i   ,j,k)))
    end do
  end do
  end do
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddxup1_add (f, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = fp + 0.
    return
  end if
!
  a = 1.
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
   do j=1,my
    fp(mx  ,j,k) = fp(mx  ,j,k) + ( &
                   a*(f(1   ,j,k)-f(mx  ,j,k)))
   end do
   do j=1,my
    do i=1,mx-1
      fp(i  ,j,k) = fp(i  ,j,k) + ( &
                   a*(f(i+1 ,j,k)-f(i   ,j,k)))
    end do
  end do
  end do
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddxdn_add (f, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = fp + 0.
    return
  end if
!
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)
  a1st = 1.
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
   do j=1,my
    fp(mx-1,j,k) = fp(mx-1,j,k) + ( &
                 + c*(f(1   ,j,k)-f(mx-4,j,k)) &
                 + b*(f(mx  ,j,k)-f(mx-3,j,k)) &
                 + a*(f(mx-1,j,k)-f(mx-2,j,k)))
    fp(mx  ,j,k) = fp(mx  ,j,k) + ( &
                 + c*(f(2   ,j,k)-f(mx-3,j,k)) &
                 + b*(f(1   ,j,k)-f(mx-2,j,k)) &
                 + a*(f(mx  ,j,k)-f(mx-1,j,k)))
    fp(1   ,j,k) = fp(1   ,j,k) + ( &
                 + c*(f(3   ,j,k)-f(mx-2,j,k)) &
                 + b*(f(2   ,j,k)-f(mx-1,j,k)) &
                 + a*(f(1   ,j,k)-f(mx  ,j,k)))
    fp(2  ,j,k) = fp(2  ,j,k) + ( &
                 + c*(f(4   ,j,k)-f(mx-1,j,k)) &
                 + b*(f(3   ,j,k)-f(mx  ,j,k)) &
                 + a*(f(2   ,j,k)-f(1   ,j,k)))
    fp(3  ,j,k) = fp(3  ,j,k) + ( &
                 + c*(f(5   ,j,k)-f(mx  ,j,k)) &
                 + b*(f(4   ,j,k)-f(1   ,j,k)) &
                 + a*(f(3   ,j,k)-f(2   ,j,k)))
   end do
   do j=1,my
    do i=4,mx-2
      fp(i,j,k) = fp(i,j,k) + ( &
                 + c*(f(i+2 ,j,k)-f(i-3 ,j,k)) &
                 + b*(f(i+1 ,j,k)-f(i-2 ,j,k)) &
                 + a*(f(i   ,j,k)-f(i-1 ,j,k)))
    end do
   end do
  end do
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddxdn1_add (f, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!-----------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = fp + 0.
    return
  end if
!
  a = 1.
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
   do j=1,my
    fp(1   ,j,k) = fp(1   ,j,k) + ( &
                   a*(f(1   ,j,k)-f(mx  ,j,k)))
   end do
   do j=1,my
    do i=2,mx
      fp(i,j,k) = fp(i,j,k) + ( &
                   a*(f(i   ,j,k)-f(i-1 ,j,k)))
    end do
   end do
  end do
!
END SUBROUTINE
!***********************************************************************
SUBROUTINE yup_add (f, fp, mx, my, mz)
!
!  f is centered on (i,j-.5,k), fifth order stagger
!
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, intent(in) , dimension(mx,my,mz):: f
  real,              dimension(mx,my,mz):: fp
!-----------------------------------------------------------------------
!

  if (my .lt. 5) then
  call limits_omp(1,mz,izs,ize)
    do k=izs,ize
      fp(:,:,k) = fp(:,:,k) + f(:,:,k)
    end do
    return
  end if
  c = 3./256.
  b = -25./256.
  a = .5-b-c
  a1st = 0.5
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    do i=1,mx
      fp(i,my-2,k) = fp(i,my-2,k) + ( &
                     a*(f(i,my-2,k)+f(i,my-1,k)) + &
                     b*(f(i,my-3,k)+f(i,my  ,k)) + &
                     c*(f(i,my-4,k)+f(i,1   ,k)))
      fp(i,my-1,k) = fp(i,my-1,k) + ( &
                     a*(f(i,my-1,k)+f(i,my  ,k)) + &
                     b*(f(i,my-2,k)+f(i,1   ,k)) + &
                     c*(f(i,my-3,k)+f(i,2   ,k)))
      fp(i,my  ,k) = fp(i,my  ,k) + ( &
                     a*(f(i,my  ,k)+f(i,1   ,k)) + &
                     b*(f(i,my-1,k)+f(i,2   ,k)) + &
                     c*(f(i,my-2,k)+f(i,3   ,k)))
      fp(i,1   ,k) = fp(i,1   ,k) + ( &
                     a*(f(i,1   ,k)+f(i,2   ,k)) + &
                     b*(f(i,my  ,k)+f(i,3   ,k)) + &
                     c*(f(i,my-1,k)+f(i,4   ,k)))
      fp(i,2   ,k) = fp(i,2   ,k) + ( &
                     a*(f(i,2   ,k)+f(i,3   ,k)) + &
                     b*(f(i,1   ,k)+f(i,4   ,k)) + &
                     c*(f(i,my  ,k)+f(i,5   ,k)))
    end do
    do j=3,my-3
     do i=1,mx
      fp(i,j  ,k) = fp(i,j  ,k) + ( &
                     a*(f(i,j   ,k)+f(i,j+1 ,k)) + &
                     b*(f(i,j-1 ,k)+f(i,j+2 ,k)) + &
                     c*(f(i,j-2 ,k)+f(i,j+3 ,k)))
     end do
    end do
  end do
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE yup1_add (f, fp, mx, my, mz)
!
!  f is centered on (i,j-.5,k), fifth order stagger
!
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, intent(in) , dimension(mx,my,mz):: f
  real,              dimension(mx,my,mz):: fp
!-----------------------------------------------------------------------
!
  if (my .lt. 5) then
  call limits_omp(1,mz,izs,ize)
    do k=izs,ize
      fp(:,:,k) = fp(:,:,k) + f(:,:,k)
    end do
    return
  end if
!
  a = 0.5
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    do i=1,mx
      fp(i,my,k) = fp(i,my,k) + ( &
                     a*(f(i,my,k)+f(i,1,k)))
    end do
    do j=1,my-1
      do i=1,mx
        fp(i,j  ,k) = fp(i,j  ,k) + ( &
                     a*(f(i,j   ,k)+f(i,j+1 ,k)))
      end do
    end do
  end do
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE ydn_add (f, fp, mx, my, mz)
!
!  f is centered on (i,j,k), fifth order stagger
!
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!
!-----------------------------------------------------------------------
  if (my .lt. 5) then
  call limits_omp(1,mz,izs,ize)
    do k=izs,ize
      fp(:,:,k) = fp(:,:,k) + f(:,:,k)
    end do
    return
  end if
  c = 3./256.
  b = -25./256.
  a = .5-b-c
  a1st = 0.5
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    do i=1,mx
      fp(i,my-1,k) = fp(i,my-1,k) + ( &
                     a*(f(i,my-2,k)+f(i,my-1,k)) + &
                     b*(f(i,my-3,k)+f(i,my  ,k)) + &
                     c*(f(i,my-4,k)+f(i,1   ,k)))
      fp(i,my  ,k) = fp(i,my  ,k) + ( &
                     a*(f(i,my-1,k)+f(i,my  ,k)) + &
                     b*(f(i,my-2,k)+f(i,1   ,k)) + &
                     c*(f(i,my-3,k)+f(i,2   ,k)))
      fp(i,1   ,k) = fp(i,1   ,k) + ( &
                     a*(f(i,my  ,k)+f(i,1   ,k)) + &
                     b*(f(i,my-1,k)+f(i,2   ,k)) + &
                     c*(f(i,my-2,k)+f(i,3   ,k)))
      fp(i,2   ,k) = fp(i,2   ,k) + ( &
                     a*(f(i,1   ,k)+f(i,2   ,k)) + &
                     b*(f(i,my  ,k)+f(i,3   ,k)) + &
                     c*(f(i,my-1,k)+f(i,4   ,k)))
      fp(i,3   ,k) = fp(i,3   ,k) + ( &
                     a*(f(i,2   ,k)+f(i,3   ,k)) + &
                     b*(f(i,1   ,k)+f(i,4   ,k)) + &
                     c*(f(i,my  ,k)+f(i,5   ,k)))
    end do
    do j=4,my-2
     do i=1,mx
      fp(i,j,k) = fp(i,j,k) + ( &
                     a*(f(i,j   ,k)+f(i,j-1 ,k)) + &
                     b*(f(i,j+1 ,k)+f(i,j-2 ,k)) + &
                     c*(f(i,j+2 ,k)+f(i,j-3 ,k)))
     end do
    end do
  end do
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE ydn1_add (f, fp, mx, my, mz)
!
!  f is centered on (i,j,k), fifth order stagger
!
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!
!-----------------------------------------------------------------------
  if (my .lt. 5) then
  call limits_omp(1,mz,izs,ize)
    do k=izs,ize
      fp(:,:,k) = fp(:,:,k) + f(:,:,k)
    end do
    return
  end if
!
  a = 0.5
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    do i=1,mx
      fp(i,1   ,k) = fp(i,1   ,k) + ( &
                     a*(f(i,my  ,k)+f(i,1   ,k)))
    end do
    do j=2,my
     do i=1,mx
      fp(i,j,k) = fp(i,j,k) + ( &
                     a*(f(i,j   ,k)+f(i,j-1 ,k)))
     end do
    end do
  end do
!
END SUBROUTINE
!***********************************************************************
SUBROUTINE ddyupi_add (f, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!-----------------------------------------------------------------------
!
  if (my .lt. 3) then
    fp = fp + 0.
    return
  end if
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)
  a1st = 1.
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    do i=1,mx
      fp(i,my-2,k) = fp(i,my-2,k) + ( &
                     a*(f(i,my-1,k)-f(i,my-2,k)) + &
                     b*(f(i,my  ,k)-f(i,my-3,k)) + &
                     c*(f(i,1   ,k)-f(i,my-4,k)))
      fp(i,my-1,k) = fp(i,my-1,k) + ( &
                     a*(f(i,my  ,k)-f(i,my-1,k)) + &
                     b*(f(i,1   ,k)-f(i,my-2,k)) + &
                     c*(f(i,2   ,k)-f(i,my-3,k)))
      fp(i,my  ,k) = fp(i,my  ,k) + ( &
                     a*(f(i,1   ,k)-f(i,my  ,k)) + &
                     b*(f(i,2   ,k)-f(i,my-1,k)) + &
                     c*(f(i,3   ,k)-f(i,my-2,k)))
      fp(i,1   ,k) = fp(i,1   ,k) + ( &
                     a*(f(i,2   ,k)-f(i,1   ,k)) + &
                     b*(f(i,3   ,k)-f(i,my  ,k)) + &
                     c*(f(i,4   ,k)-f(i,my-1,k)))
      fp(i,2   ,k) = fp(i,2   ,k) + ( &
                     a*(f(i,3   ,k)-f(i,2   ,k)) + &
                     b*(f(i,4   ,k)-f(i,1   ,k)) + &
                     c*(f(i,5   ,k)-f(i,my  ,k)))
    end do
    do j=3,my-3
     do i=1,mx
      fp(i,j  ,k) = fp(i,j  ,k) + ( &
                     a*(f(i,j+1 ,k)-f(i,j   ,k)) + &
                     b*(f(i,j+2 ,k)-f(i,j-1 ,k)) + &
                     c*(f(i,j+3 ,k)-f(i,j-2 ,k)))
     end do
    end do
  end do
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddyupi1_add (f, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!-----------------------------------------------------------------------
!
  if (my .lt. 3) then
    fp = fp + 0.
    return
  end if
  a = 1.
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    do i=1,mx
      fp(i,my,k) = fp(i,my,k) + ( &
                     a*(f(i,1  ,k)-f(i,my,k)))
    end do
    do j=1,my-1
      do i=1,mx
        fp(i,j  ,k) = fp(i,j  ,k) + ( &
                     a*(f(i,j+1 ,k)-f(i,j   ,k)))
     end do
    end do
  end do
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddydni_add (f, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!-----------------------------------------------------------------------
!
  if (my .lt. 3) then
    fp = fp + 0.
    return
  end if
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)
  a1st = 1.
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    do i=1,mx
      fp(i,my-1,k) = fp(i,my-1,k) + ( &
                     a*(f(i,my-1,k)-f(i,my-2,k)) + &
                     b*(f(i,my  ,k)-f(i,my-3,k)) + &
                     c*(f(i,1   ,k)-f(i,my-4,k)))
      fp(i,my  ,k) = fp(i,my  ,k) + ( &
                     a*(f(i,my  ,k)-f(i,my-1,k)) + &
                     b*(f(i,1   ,k)-f(i,my-2,k)) + &
                     c*(f(i,2   ,k)-f(i,my-3,k)))
      fp(i,1  ,k) = fp(i,1  ,k) + ( &
                     a*(f(i,1   ,k)-f(i,my  ,k)) + &
                     b*(f(i,2   ,k)-f(i,my-1,k)) + &
                     c*(f(i,3   ,k)-f(i,my-2,k)))
      fp(i,2   ,k) = fp(i,2   ,k) + ( &
                     a*(f(i,2   ,k)-f(i,1   ,k)) + &
                     b*(f(i,3   ,k)-f(i,my  ,k)) + &
                     c*(f(i,4   ,k)-f(i,my-1,k)))
      fp(i,3   ,k) = fp(i,3   ,k) + ( &
                     a*(f(i,3   ,k)-f(i,2   ,k)) + &
                     b*(f(i,4   ,k)-f(i,1   ,k)) + &
                     c*(f(i,5   ,k)-f(i,my  ,k)))
    end do
    do j=4,my-2
     do i=1,mx
      fp(i,j ,k) = fp(i,j ,k) + ( &
                     a*(f(i,j   ,k)-f(i,j-1 ,k)) + &
                     b*(f(i,j+1 ,k)-f(i,j-2 ,k)) + &
                     c*(f(i,j+2 ,k)-f(i,j-3 ,k)))
     end do
    end do
  end do
!
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddydni1_add (f, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!-----------------------------------------------------------------------
!
  if (my .lt. 3) then
    fp = fp + 0.
    return
  end if
  a = 1.
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    do i=1,mx
      fp(i,1  ,k) = fp(i,1  ,k) + ( &
                     a*(f(i,1   ,k)-f(i,my  ,k)))
    end do
    do j=2,my
     do i=1,mx
      fp(i,j ,k) = fp(i,j ,k) + ( &
                     a*(f(i,j   ,k)-f(i,j-1 ,k)))
     end do
    end do
  end do
!
END SUBROUTINE
!***********************************************************************
SUBROUTINE ddyup_add (f, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!-----------------------------------------------------------------------
!
  if (my .lt. 3) then
    fp = fp + 0.
    return
  end if
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)
  a1st = 1.
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    do i=1,mx
      fp(i,my-2,k) = fp(i,my-2,k) + ( &
                     c*(f(i,1   ,k)-f(i,my-4,k)) + &
                     b*(f(i,my  ,k)-f(i,my-3,k)) + &
                     a*(f(i,my-1,k)-f(i,my-2,k)) )
      fp(i,my-1,k) = fp(i,my-1,k) + ( &
                     c*(f(i,2   ,k)-f(i,my-3,k)) + &
                     b*(f(i,1   ,k)-f(i,my-2,k)) + &
                     a*(f(i,my  ,k)-f(i,my-1,k)) )
      fp(i,my  ,k) = fp(i,my  ,k) + ( &
                     c*(f(i,3   ,k)-f(i,my-2,k)) + &
                     b*(f(i,2   ,k)-f(i,my-1,k)) + &
                     a*(f(i,1   ,k)-f(i,my  ,k)) )
      fp(i,1   ,k) = fp(i,1   ,k) + ( &
                     c*(f(i,4   ,k)-f(i,my-1,k)) + &
                     b*(f(i,3   ,k)-f(i,my  ,k)) + &
                     a*(f(i,2   ,k)-f(i,1   ,k)) )
      fp(i,2   ,k) = fp(i,2   ,k) + ( &
                     c*(f(i,5   ,k)-f(i,my  ,k)) + &
                     b*(f(i,4   ,k)-f(i,1   ,k)) + &
                     a*(f(i,3   ,k)-f(i,2   ,k)) )
    end do
    do j=3,my-3
     do i=1,mx
      fp(i,j  ,k) = fp(i,j  ,k) + ( &
                     c*(f(i,j+3 ,k)-f(i,j-2 ,k)) + &
                     b*(f(i,j+2 ,k)-f(i,j-1 ,k)) + &
                     a*(f(i,j+1 ,k)-f(i,j   ,k)) )
     end do
    end do
  end do
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddyup1_add (f, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!-----------------------------------------------------------------------
!
  if (my .lt. 3) then
    fp = fp + 0.
    return
  end if
  a = 1.
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    do i=1,mx
      fp(i,my,k) = fp(i,my,k) + ( &
                     a*(f(i,1  ,k)-f(i,my,k)))
    end do
    do j=1,my-1
      do i=1,mx
        fp(i,j  ,k) = fp(i,j  ,k) + ( &
                     a*(f(i,j+1 ,k)-f(i,j   ,k)))
     end do
    end do
  end do
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddydn_add (f, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!-----------------------------------------------------------------------
!
  if (my .lt. 3) then
    fp = fp + 0.
    return
  end if
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)
  a1st = 1.
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    do i=1,mx
      fp(i,my-1,k) = fp(i,my-1,k) + ( &
                     c*(f(i,1   ,k)-f(i,my-4,k)) + &
                     b*(f(i,my  ,k)-f(i,my-3,k)) + &
                     a*(f(i,my-1,k)-f(i,my-2,k)) ) 
      fp(i,my  ,k) = fp(i,my  ,k) + ( &
                     c*(f(i,2   ,k)-f(i,my-3,k)) + &
                     b*(f(i,1   ,k)-f(i,my-2,k)) + &
                     a*(f(i,my  ,k)-f(i,my-1,k)) )
      fp(i,1  ,k) = fp(i,1  ,k) + ( &
                     c*(f(i,3   ,k)-f(i,my-2,k)) + &
                     b*(f(i,2   ,k)-f(i,my-1,k)) + &
                     a*(f(i,1   ,k)-f(i,my  ,k)) )
      fp(i,2   ,k) = fp(i,2   ,k) + ( &
                     c*(f(i,4   ,k)-f(i,my-1,k)) + &
                     b*(f(i,3   ,k)-f(i,my  ,k)) + &
                     a*(f(i,2   ,k)-f(i,1   ,k)) )
      fp(i,3   ,k) = fp(i,3   ,k) + ( &
                     c*(f(i,5   ,k)-f(i,my  ,k)) + &
                     b*(f(i,4   ,k)-f(i,1   ,k)) + &
                     a*(f(i,3   ,k)-f(i,2   ,k)) )
    end do
    do j=4,my-2
     do i=1,mx
      fp(i,j ,k) = fp(i,j ,k) + ( &
                     c*(f(i,j+2 ,k)-f(i,j-3 ,k)) + &
                     b*(f(i,j+1 ,k)-f(i,j-2 ,k)) + &
                     a*(f(i,j   ,k)-f(i,j-1 ,k)) )
     end do
    end do
  end do
!
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddydn1_add (f, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!-----------------------------------------------------------------------
!
  if (my .lt. 3) then
    fp = fp + 0.
    return
  end if
  a = 1.
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    do i=1,mx
      fp(i,1  ,k) = fp(i,1  ,k) + ( &
                     a*(f(i,1   ,k)-f(i,my  ,k)))
    end do
    do j=2,my
     do i=1,mx
      fp(i,j ,k) = fp(i,j ,k) + ( &
                     a*(f(i,j   ,k)-f(i,j-1 ,k)))
     end do
    end do
  end do
!
END SUBROUTINE
!***********************************************************************
SUBROUTINE zup_add (f, fp, mx, my, mz)
!
!  f is centered on (i,j,k-.5), fifth order stagger
!
  implicit none
  real a, b, c, a1st
  integer i, j, k, km2, km1, kp1, kp2, kp3, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!-----------------------------------------------------------------------
!
!
  if (mz.le.5) then
    fp = fp + f
    return
  end if
  c = 3./256.
  b = -25./256.
  a = .5-b-c
  a1st = 0.5
!
!$omp barrier
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    km2=mod(k-3+mz,mz)+1
    km1=mod(k-2+mz,mz)+1
    kp1=mod(k     ,mz)+1
    kp2=mod(k+1   ,mz)+1
    kp3=mod(k+2   ,mz)+1
    do j=1,my
      do i=1,mx
        fp(i,j,k) = fp(i,j,k) + ( &
                     a*(f(i,j,kp1)+f(i,j,k  )) + &
                     b*(f(i,j,kp2)+f(i,j,km1)) + &
                     c*(f(i,j,kp3)+f(i,j,km2)))
      end do
    end do
  end do
!$omp barrier
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE zup1_add (f, fp, mx, my, mz)
!
!  f is centered on (i,j,k-.5), fifth order stagger
!
  implicit none
  real a, b, c, a1st
  integer i, j, k, kp1, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    fp = fp + f
    return
  end if
  a = .5
!
!$omp barrier
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    kp1=mod(k     ,mz)+1
    do j=1,my
      do i=1,mx
        fp(i,j,k) = fp(i,j,k) + ( &
                     a*(f(i,j,kp1)+f(i,j,k  )))
      end do
    end do
  end do
!$omp barrier
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE zdn_add (f, fp, mx, my, mz)
!
!  f is centered on (i,j,k), fifth order stagger
!
  implicit none
  real a, b, c, a1st
  integer i, j, k, km3, km2, km1, kp1, kp2, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    fp = fp + f
    return
  end if
  c = 3./256.
  b = -25./256.
  a = .5-b-c
  a1st = 0.5
!
!$omp barrier
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    km3=mod(k-4+mz,mz)+1
    km2=mod(k-3+mz,mz)+1
    km1=mod(k-2+mz,mz)+1
    kp1=mod(k     ,mz)+1
    kp2=mod(k+1   ,mz)+1
    do j=1,my
      do i=1,mx
        fp(i,j,k) = fp(i,j,k) + ( &
                     a*(f(i,j,k  )+f(i,j,km1)) + &
                     b*(f(i,j,kp1)+f(i,j,km2)) + &
                     c*(f(i,j,kp2)+f(i,j,km3)))
      end do
    end do
  end do
!$omp barrier
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE zdn1_add (f, fp, mx, my, mz)
!
!  f is centered on (i,j,k), fifth order stagger
!
  implicit none
  real a, b, c, a1st
  integer i, j, k, km1, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    fp = fp + f
    return
  end if
  a = .5
!
!$omp barrier
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    km1=mod(k-2+mz,mz)+1
    do j=1,my
      do i=1,mx
        fp(i,j,k) = fp(i,j,k) + ( &
                     a*(f(i,j,k  )+f(i,j,km1)))
      end do
    end do
  end do
!$omp barrier
!
END SUBROUTINE
!***********************************************************************
SUBROUTINE zup32_add (f,j, fp, mx, my, mz)
!
!  f is centered on (i,j,k-.5), fifth order stagger
!
  implicit none
  real a, b, c, a1st
  integer i, j, k, km2, km1, kp1, kp2, kp3, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f
  real, dimension(mx,mz):: fp
!-----------------------------------------------------------------------
!
!
  if (mz.le.5) then
    fp = fp + f(:,j,:)
    return
  end if
  c = 3./256.
  b = -25./256.
  a = .5-b-c
  a1st = 0.5
!
!$omp barrier
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    km2=mod(k-3+mz,mz)+1
    km1=mod(k-2+mz,mz)+1
    kp1=mod(k     ,mz)+1
    kp2=mod(k+1   ,mz)+1
    kp3=mod(k+2   ,mz)+1
      do i=1,mx
        fp(i,k) = fp(i,k) + ( &
                     a*(f(i,j,kp1)+f(i,j,k  )) + &
                     b*(f(i,j,kp2)+f(i,j,km1)) + &
                     c*(f(i,j,kp3)+f(i,j,km2)))
      end do
  end do
!$omp barrier
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE zup132_add (f,j, fp, mx, my, mz)
!
!  f is centered on (i,j,k-.5), fifth order stagger
!
  implicit none
  real a, b, c, a1st
  integer i, j, k, kp1, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f
  real, dimension(mx,mz):: fp
!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    fp = fp + f(:,j,:)
    return
  end if
  a = .5
!
!$omp barrier
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    kp1=mod(k     ,mz)+1
      do i=1,mx
        fp(i,k) = fp(i,k) + ( &
                     a*(f(i,j,kp1)+f(i,j,k  )))
      end do
  end do
!$omp barrier
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE zdn32_add (f,j, fp, mx, my, mz)
!
!  f is centered on (i,j,k), fifth order stagger
!
  implicit none
  real a, b, c, a1st
  integer i, j, k, km3, km2, km1, kp1, kp2, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f
  real, dimension(mx,mz):: fp
!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    fp = fp + f(:,j,:)
    return
  end if
  c = 3./256.
  b = -25./256.
  a = .5-b-c
  a1st = 0.5
!
!$omp barrier
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    km3=mod(k-4+mz,mz)+1
    km2=mod(k-3+mz,mz)+1
    km1=mod(k-2+mz,mz)+1
    kp1=mod(k     ,mz)+1
    kp2=mod(k+1   ,mz)+1
      do i=1,mx
        fp(i,k) = fp(i,k) + ( &
                     a*(f(i,j,k  )+f(i,j,km1)) + &
                     b*(f(i,j,kp1)+f(i,j,km2)) + &
                     c*(f(i,j,kp2)+f(i,j,km3)))
      end do
  end do
!$omp barrier
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE zdn132_add (f,j, fp, mx, my, mz)
!
!  f is centered on (i,j,k), fifth order stagger
!
  implicit none
  real a, b, c, a1st
  integer i, j, k, km1, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f
  real, dimension(mx,mz):: fp
!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    fp = fp + f(:,j,:)
    return
  end if
  a = .5
!
!$omp barrier
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    km1=mod(k-2+mz,mz)+1
      do i=1,mx
        fp(i,k) = fp(i,k) + ( &
                     a*(f(i,j,k  )+f(i,j,km1)))
      end do
  end do
!$omp barrier
!
END SUBROUTINE
!***********************************************************************
SUBROUTINE zup22_add (f, fp, mx, my, mz)
!
!  f is centered on (i,k-.5), fifth order stagger
!
  implicit none
  real a, b, c, a1st
  integer i, j, k, km2, km1, kp1, kp2, kp3, mx, my, mz, izs, ize
  real, dimension(mx,mz):: fp, f
!-----------------------------------------------------------------------
!
!
  if (mz.le.5) then
    fp = fp + f
    return
  end if
  c = 3./256.
  b = -25./256.
  a = .5-b-c
  a1st = 0.5
!
!$omp barrier
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    km2=mod(k-3+mz,mz)+1
    km1=mod(k-2+mz,mz)+1
    kp1=mod(k     ,mz)+1
    kp2=mod(k+1   ,mz)+1
    kp3=mod(k+2   ,mz)+1
      do i=1,mx
        fp(i,k) = fp(i,k) + ( &
                     a*(f(i,kp1)+f(i,k  )) + &
                     b*(f(i,kp2)+f(i,km1)) + &
                     c*(f(i,kp3)+f(i,km2)))
      end do
  end do
!$omp barrier
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE zup122_add (f, fp, mx, my, mz)
!
!  f is centered on (i,k-.5), fifth order stagger
!
  implicit none
  real a, b, c, a1st
  integer i, j, k, kp1, mx, my, mz, izs, ize
  real, dimension(mx,mz):: fp, f
!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    fp = fp + f
    return
  end if
  a = .5
!
!$omp barrier
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    kp1=mod(k     ,mz)+1
      do i=1,mx
        fp(i,k) = fp(i,k) + ( &
                     a*(f(i,kp1)+f(i,k  )))
      end do
  end do
!$omp barrier
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE zdn22_add (f, fp, mx, my, mz)
!
!  f is centered on (i,k), fifth order stagger
!
  implicit none
  real a, b, c, a1st
  integer i, j, k, km3, km2, km1, kp1, kp2, mx, my, mz, izs, ize
  real, dimension(mx,mz):: fp, f
!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    fp = fp + f
    return
  end if
  c = 3./256.
  b = -25./256.
  a = .5-b-c
  a1st = 0.5
!
!$omp barrier
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    km3=mod(k-4+mz,mz)+1
    km2=mod(k-3+mz,mz)+1
    km1=mod(k-2+mz,mz)+1
    kp1=mod(k     ,mz)+1
    kp2=mod(k+1   ,mz)+1
      do i=1,mx
        fp(i,k) = fp(i,k) + ( &
                     a*(f(i,k  )+f(i,km1)) + &
                     b*(f(i,kp1)+f(i,km2)) + &
                     c*(f(i,kp2)+f(i,km3)))
      end do
  end do
!$omp barrier
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE zdn122_add (f, fp, mx, my, mz)
!
!  f is centered on (i,k), fifth order stagger
!
  implicit none
  real a, b, c, a1st
  integer i, j, k, km1, mx, my, mz, izs, ize
  real, dimension(mx,mz):: fp, f
!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    fp = fp + f
    return
  end if
  a = .5
!
!$omp barrier
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    km1=mod(k-2+mz,mz)+1
      do i=1,mx
        fp(i,k) = fp(i,k) + ( &
                     a*(f(i,k  )+f(i,km1)))
      end do
  end do
!$omp barrier
!
END SUBROUTINE
!***********************************************************************
SUBROUTINE ddzupi_add (f, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, km2, km1, kp1, kp2, kp3, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    fp = fp + 0.
    return
  endif
!
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)
  a1st = 1.
!
!$omp barrier
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    km2=mod(k-3+mz,mz)+1
    km1=mod(k-2+mz,mz)+1
    kp1=mod(k     ,mz)+1
    kp2=mod(k+1   ,mz)+1
    kp3=mod(k+2   ,mz)+1
    do j=1,my
      do i=1,mx
        fp(i,j,k) = fp(i,j,k) + ( &
                     a*(f(i,j,kp1)-f(i,j,k  )) + &
                     b*(f(i,j,kp2)-f(i,j,km1)) + &
                     c*(f(i,j,kp3)-f(i,j,km2)))
      end do
    end do
  end do
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddzupi1_add (f, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, kp1, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    fp = fp + 0.
    return
  endif
!
  a = 1.
!
!$omp barrier
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    kp1=mod(k     ,mz)+1
    do j=1,my
      do i=1,mx
        fp(i,j,k) = fp(i,j,k) + ( &
                     a*(f(i,j,kp1)-f(i,j,k  )))
      end do
    end do
  end do
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddzdni_add (f, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, km3, km2, km1, kp1, kp2, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    fp = fp + 0.
    return
  endif
!
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)
  a1st = 1.
!
!$omp barrier
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    km3=mod(k-4+mz,mz)+1
    km2=mod(k-3+mz,mz)+1
    km1=mod(k-2+mz,mz)+1
    kp1=mod(k     ,mz)+1
    kp2=mod(k+1   ,mz)+1
    do j=1,my
      do i=1,mx
        fp(i,j,k) = fp(i,j,k) + ( &
                     a*(f(i,j,k  )-f(i,j,km1)) + &
                     b*(f(i,j,kp1)-f(i,j,km2)) + &
                     c*(f(i,j,kp2)-f(i,j,km3)))
      end do
    end do
  end do
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddzdni1_add (f, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, km1, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    fp = fp + 0.
    return
  endif
!
  a = 1.
!
!$omp barrier
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    km1=mod(k-2+mz,mz)+1
    do j=1,my
      do i=1,mx
        fp(i,j,k) = fp(i,j,k) + ( &
                     a*(f(i,j,k  )-f(i,j,km1)))
      end do
    end do
  end do
!
END SUBROUTINE
!***********************************************************************
SUBROUTINE ddzupi32_add (f,j, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, km2, km1, kp1, kp2, kp3, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f
  real, dimension(mx,mz):: fp
!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    fp = fp + 0.
    return
  endif
!
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)
  a1st = 1.
!
!$omp barrier
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    km2=mod(k-3+mz,mz)+1
    km1=mod(k-2+mz,mz)+1
    kp1=mod(k     ,mz)+1
    kp2=mod(k+1   ,mz)+1
    kp3=mod(k+2   ,mz)+1
      do i=1,mx
        fp(i,k) = fp(i,k) + ( &
                     a*(f(i,j,kp1)-f(i,j,k  )) + &
                     b*(f(i,j,kp2)-f(i,j,km1)) + &
                     c*(f(i,j,kp3)-f(i,j,km2)))
      end do
  end do
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddzupi132_add (f,j, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, kp1, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f
  real, dimension(mx,mz):: fp
!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    fp = fp + 0.
    return
  endif
!
  a = 1.
!
!$omp barrier
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    kp1=mod(k     ,mz)+1
      do i=1,mx
        fp(i,k) = fp(i,k) + ( &
                     a*(f(i,j,kp1)-f(i,j,k  )))
      end do
  end do
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddzdni32_add (f,j, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, km3, km2, km1, kp1, kp2, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f
  real, dimension(mx,mz):: fp
!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    fp = fp + 0.
    return
  endif
!
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)
  a1st = 1.
!
!$omp barrier
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    km3=mod(k-4+mz,mz)+1
    km2=mod(k-3+mz,mz)+1
    km1=mod(k-2+mz,mz)+1
    kp1=mod(k     ,mz)+1
    kp2=mod(k+1   ,mz)+1
      do i=1,mx
        fp(i,k) = fp(i,k) + ( &
                     a*(f(i,j,k  )-f(i,j,km1)) + &
                     b*(f(i,j,kp1)-f(i,j,km2)) + &
                     c*(f(i,j,kp2)-f(i,j,km3)))
      end do
  end do
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddzdni132_add (f,j, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, km1, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f
  real, dimension(mx,mz):: fp
!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    fp = fp + 0.
    return
  endif
!
  a = 1.
!
!$omp barrier
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    km1=mod(k-2+mz,mz)+1
      do i=1,mx
        fp(i,k) = fp(i,k) + ( &
                     a*(f(i,j,k  )-f(i,j,km1)))
      end do
  end do
!
END SUBROUTINE
!***********************************************************************
SUBROUTINE ddzupi22_add (f, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, km2, km1, kp1, kp2, kp3, mx, my, mz, izs, ize
  real, dimension(mx,mz):: fp, f
!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    fp = fp + 0.
    return
  endif
!
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)
  a1st = 1.
!
!$omp barrier
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    km2=mod(k-3+mz,mz)+1
    km1=mod(k-2+mz,mz)+1
    kp1=mod(k     ,mz)+1
    kp2=mod(k+1   ,mz)+1
    kp3=mod(k+2   ,mz)+1
      do i=1,mx
        fp(i,k) = fp(i,k) + ( &
                     a*(f(i,kp1)-f(i,k  )) + &
                     b*(f(i,kp2)-f(i,km1)) + &
                     c*(f(i,kp3)-f(i,km2)))
      end do
  end do
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddzupi122_add (f, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, kp1, mx, my, mz, izs, ize
  real, dimension(mx,mz):: fp, f
!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    fp = fp + 0.
    return
  endif
!
  a = 1.
!
!$omp barrier
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    kp1=mod(k     ,mz)+1
      do i=1,mx
        fp(i,k) = fp(i,k) + ( &
                     a*(f(i,kp1)-f(i,k  )))
      end do
  end do
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddzdni22_add (f, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, km3, km2, km1, kp1, kp2, mx, my, mz, izs, ize
  real, dimension(mx,mz):: fp, f
!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    fp = fp + 0.
    return
  endif
!
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)
  a1st = 1.
!
!$omp barrier
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    km3=mod(k-4+mz,mz)+1
    km2=mod(k-3+mz,mz)+1
    km1=mod(k-2+mz,mz)+1
    kp1=mod(k     ,mz)+1
    kp2=mod(k+1   ,mz)+1
      do i=1,mx
        fp(i,k) = fp(i,k) + ( &
                     a*(f(i,k  )-f(i,km1)) + &
                     b*(f(i,kp1)-f(i,km2)) + &
                     c*(f(i,kp2)-f(i,km3)))
      end do
  end do
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddzdni122_add (f, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, km1, mx, my, mz, izs, ize
  real, dimension(mx,mz):: fp, f
!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    fp = fp + 0.
    return
  endif
!
  a = 1.
!
!$omp barrier
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    km1=mod(k-2+mz,mz)+1
      do i=1,mx
        fp(i,k) = fp(i,k) + ( &
                     a*(f(i,k  )-f(i,km1)))
      end do
  end do
!
END SUBROUTINE
!***********************************************************************
SUBROUTINE ddzup_add (f, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, km2, km1, kp1, kp2, kp3, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    fp = fp + 0.
    return
  end if
!
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)
  a1st = 1.
!
!$omp barrier
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    km2=mod(k-3+mz,mz)+1
    km1=mod(k-2+mz,mz)+1
    kp1=mod(k     ,mz)+1
    kp2=mod(k+1   ,mz)+1
    kp3=mod(k+2   ,mz)+1
    do j=1,my
      do i=1,mx
        fp(i,j,k) = fp(i,j,k) + ( &
                     c*(f(i,j,kp3)-f(i,j,km2)) + &
                     b*(f(i,j,kp2)-f(i,j,km1)) + &
                     a*(f(i,j,kp1)-f(i,j,k  )) )
      end do
    end do
  end do
!$omp barrier
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddzup1_add (f, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, kp1, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    fp = fp + 0.
    return
  end if
!
  a = 1.
!
!$omp barrier
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    kp1=mod(k     ,mz)+1
    do j=1,my
      do i=1,mx
        fp(i,j,k) = fp(i,j,k) + ( &
                     a*(f(i,j,kp1)-f(i,j,k  )))
      end do
    end do
  end do
!$omp barrier
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddzdn_add (f, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, km3, km2, km1, kp1, kp2, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    fp = fp + 0.
    return
  end if
!
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)
  a1st = 1.
!
!$omp barrier
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    km3=mod(k-4+mz,mz)+1
    km2=mod(k-3+mz,mz)+1
    km1=mod(k-2+mz,mz)+1
    kp1=mod(k     ,mz)+1
    kp2=mod(k+1   ,mz)+1
    do j=1,my
      do i=1,mx
        fp(i,j,k) = fp(i,j,k) + ( &
                     c*(f(i,j,kp2)-f(i,j,km3)) + &
                     b*(f(i,j,kp1)-f(i,j,km2)) + &
                     a*(f(i,j,k  )-f(i,j,km1)) )
      end do
    end do
  end do
!$omp barrier
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddzdn1_add (f, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, km1, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!-----------------------------------------------------------------------
!
  if (mz.le.5) then
    fp = fp + 0.
    return
  end if
!
  a = 1.
!
!$omp barrier
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    km1=mod(k-2+mz,mz)+1
    do j=1,my
      do i=1,mx
        fp(i,j,k) = fp(i,j,k) + ( &
                     a*(f(i,j,k  )-f(i,j,km1)))
      end do
    end do
  end do
!$omp barrier
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE laplace_sub (f, fp, mx, my, mz)
!
!  Laplace operator
!

  implicit none
  real cx, cy, cz
  real, dimension(mx,my,mz):: f, fp
  integer i, j, k, km1, kp1, mx, my, mz, izs, ize
!+---------------------------------------------------------------------

  cx=1.
  cy=1.
  cz=1.
  if (mx.gt.1) then
  call limits_omp(1,mz,izs,ize)
    do k=izs,ize
      do j=1,my
        do i=2,mx-1
          fp(i,j,k) = fp(i,j,k) - cx*(f(i-1 ,j,k)+f(i+1,j,k)-2.*f(i,j,k))
        end do
        fp(1 ,j,k) = fp(1 ,j,k) - cx*(f(2,j,k)+f(mx  ,j,k)-2.*f(1 ,j,k))
        fp(mx,j,k) = fp(mx,j,k) - cx*(f(1,j,k)+f(mx-1,j,k)-2.*f(mx,j,k))
      end do
    end do
  else
  call limits_omp(1,mz,izs,ize)
    do k=izs,ize
      do j=1,my
        do i=1,mx
          fp(i,j,k) = fp(i,j,k) - 0.
        end do
      end do
    end do
  end if

 if (my .gt. 1) then
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    do j=2,my-1
      do i=1,mx
        fp(i,j,k) = fp(i,j,k) - fp(i,j,k) + cy*(f(i,j+1,k)+f(i,j-1,k)-2.*f(i,j,k))
      end do
    end do
    do i=1,mx
        fp(i,1 ,k) = fp(i,1 ,k) - fp(i,1 ,k) + cy*(f(i,2,k)+f(i,my  ,k)-2.*f(i,1 ,k))
        fp(i,my,k) = fp(i,my,k) - fp(i,my,k) + cy*(f(i,1,k)+f(i,my-1,k)-2.*f(i,my,k))
    end do
  end do
 end if

 if (mz .gt. 1) then
!$omp barrier
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    km1=mod(k-2+mz,mz)+1
    kp1=mod(k     ,mz)+1
    do j=1,my
      do i=1,mx
        fp(i,j,k) = fp(i,j,k) - fp(i,j,k) + cz*(f(i,j,kp1)+f(i,j,km1)-2.*f(i,j,k))
      end do
    end do
  end do
 end if
END SUBROUTINE

!***********************************************************************
SUBROUTINE dif2a_sub (f, fp, mx, my, mz)
!
!  Max abs difference one zone left to one zones right.
!

  implicit none
  real cx, cy, cz
  real, dimension(mx,my,mz):: f, fp
  integer i, j, k, km1, kp1, mx, my, mz, izs, ize
!+---------------------------------------------------------------------

  if (mx.gt.1) then
  call limits_omp(1,mz,izs,ize)
    do k=izs,ize
      do j=1,my
        do i=2,mx-1
          fp(i,j,k) = fp(i,j,k) - abs(f(i-1 ,j,k)-f(i+1,j,k))
        end do
        fp(1 ,j,k) = fp(1 ,j,k) - abs(f(mx  ,j,k)-f(2  ,j,k))
        fp(mx,j,k) = fp(mx,j,k) - abs(f(mx-1,j,k)-f(1  ,j,k))
      end do
    end do
  else
  call limits_omp(1,mz,izs,ize)
    do k=izs,ize
      do j=1,my
        do i=1,mx
          fp(i,j,k) = fp(i,j,k) - 0.
        end do
      end do
    end do
  end if

 if (my .gt. 1) then
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    do j=2,my-1
      do i=1,mx
        fp(i,j, k) = fp(i,j, k) - max(fp(i,j, k),abs(f(i,j -1,k)-f(i,j+1,k)))
      end do
    end do
    do i=1,mx
        fp(i,1 ,k) = fp(i,1 ,k) - max(fp(i,1 ,k),abs(f(i,my  ,k)-f(i,  2,k)))
        fp(i,my,k) = fp(i,my,k) - max(fp(i,my,k),abs(f(i,my-1,k)-f(i,  1,k)))
    end do
  end do
 end if

 if (mz .gt. 1) then
!$omp barrier
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    km1=mod(k-2+mz,mz)+1
    kp1=mod(k     ,mz)+1
    do j=1,my
      do i=1,mx
        fp(i,j,k ) = fp(i,j,k ) - max(fp(i,j,k ),abs(f(i,j,km1)-f(i,j,kp1)))
      end do
    end do
  end do
 end if
END SUBROUTINE

!***********************************************************************
SUBROUTINE dif2_sub (fx, fy, fz, fp, mx, my, mz)
!
!  Finite difference one zone left to one zones right.
!

  implicit none
  real cx(mx), cy(my), cz(mz)
  real, dimension(mx,my,mz):: fx, fy, fz, fp
  integer i, j, k, km1, kp1, mx, my, mz, izs, ize
!+---------------------------------------------------------------------

  cx=0.5
  cy=0.5
  cz=0.5

  if (mx.gt.1) then
  call limits_omp(1,mz,izs,ize)
    do k=izs,ize
      do j=1,my
        do i=2,mx-1
          fp(i,j,k) = fp(i,j,k) - cx(i)*(fx(i-1 ,j,k)-fx(i+1,j,k))
        end do
        fp(1 ,j,k) = fp(1 ,j,k) - cx(1 )*(fx(mx  ,j,k)-fx(2  ,j,k))
        fp(mx,j,k) = fp(mx,j,k) - cx(mx)*(fx(mx-1,j,k)-fx(1  ,j,k))
      end do
    end do
  else
  call limits_omp(1,mz,izs,ize)
    do k=izs,ize
      do j=1,my
        do i=1,mx
          fp(i,j,k) = fp(i,j,k) - 0.
        end do
      end do
    end do
  end if

 if (my .gt. 1) then
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    do j=2,my-1
      do i=1,mx
        fp(i,j, k) = fp(i,j, k) - fp(i,j, k) + cy(j)*(fy(i,j -1,k)-fy(i,j+1,k))
      end do
    end do
    do i=1,mx
        fp(i,1 ,k) = fp(i,1 ,k) - fp(i,1 ,k) + cy(1 )*(fy(i,my  ,k)-fy(i,  2,k))
        fp(i,my,k) = fp(i,my,k) - fp(i,my,k) + cy(my)*(fy(i,my-1,k)-fy(i,  1,k))
    end do
  end do
 end if

 if (mz .gt. 1) then
!$omp barrier
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    km1=mod(k-2+mz,mz)+1
    kp1=mod(k     ,mz)+1
    do j=1,my
      do i=1,mx
        fp(i,j,k ) = fp(i,j,k ) - fp(i,j,k ) + cz(k)*(fz(i,j,km1)-fz(i,j,kp1))
      end do
    end do
  end do
 end if

  call limits_omp(1,mz,izs,ize)
 do k=izs,ize
   do j=1,my
     do i=1,mx
       fp(i,j,k) = fp(i,j,k) - max (0., fp(i,j,k))
     end do
   end do
 end do

END SUBROUTINE

!***********************************************************************
SUBROUTINE dif2d_sub (fx, fy, fz, fp, mx, my, mz)
!
!  Finite difference one zone left to one zones right.
!

  implicit none
  real, dimension(mx,my,mz):: fx, fy, fz, fp
  integer i, j, k, km1, kp1, mx, my, mz, izs, ize
!+---------------------------------------------------------------------

  if (mx.gt.1) then
  call limits_omp(1,mz,izs,ize)
    do k=izs,ize
      do j=1,my
        do i=2,mx-1
          fp(i,j,k) = fp(i,j,k) - (fx(i-1 ,j,k)-fx(i+1,j,k))
        end do
        fp(1 ,j,k) = fp(1 ,j,k) - (fx(mx  ,j,k)-fx(2  ,j,k))
        fp(mx,j,k) = fp(mx,j,k) - (fx(mx-1,j,k)-fx(1  ,j,k))
      end do
    end do
  else
  call limits_omp(1,mz,izs,ize)
    do k=izs,ize
      do j=1,my
        do i=1,mx
          fp(i,j,k) = fp(i,j,k) - 0.
        end do
      end do
    end do
  end if

 if (my .gt. 1) then
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    do j=2,my-1
      do i=1,mx
        fp(i,j, k) = fp(i,j, k) - fp(i,j, k) + (fy(i,j -1,k)-fy(i,j+1,k))
      end do
    end do
    do i=1,mx
        fp(i,1 ,k) = fp(i,1 ,k) - fp(i,1 ,k) + (fy(i,my  ,k)-fy(i,  2,k))
        fp(i,my,k) = fp(i,my,k) - fp(i,my,k) + (fy(i,my-1,k)-fy(i,  1,k))
    end do
  end do
 end if

 if (mz .gt. 1) then
!$omp barrier
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    km1=mod(k-2+mz,mz)+1
    kp1=mod(k     ,mz)+1
    do j=1,my
      do i=1,mx
        fp(i,j,k ) = fp(i,j,k ) - fp(i,j,k ) + (fz(i,j,km1)-fz(i,j,kp1))
      end do
    end do
  end do
 end if

  call limits_omp(1,mz,izs,ize)
 do k=izs,ize
   do j=1,my
     do i=1,mx
       fp(i,j,k) = fp(i,j,k) - max (0., fp(i,j,k))
     end do
   end do
 end do

END SUBROUTINE

!***********************************************************************
SUBROUTINE dif1_sub (fx, fy, fz, fp, mx, my, mz)
!
!  Finite difference one zone left to one zones right.
!

  implicit none
  real, dimension(mx,my,mz):: fx, fy, fz, fp
  integer i, j, k, kp1, mx, my, mz, izs, ize
!+---------------------------------------------------------------------

  if (mx.gt.1) then
  call limits_omp(1,mz,izs,ize)
    do k=izs,ize
      do j=1,my
        do i=1,mx-1
          fp(i,j,k) = fp(i,j,k) - (fx( i,j,k)-fx(i+1,j,k))
        end do
        fp(mx,j,k) = fp(mx,j,k) - (fx(mx,j,k)-fx(  1,j,k))
      end do
    end do
  else
  call limits_omp(1,mz,izs,ize)
    do k=izs,ize
      do j=1,my
        do i=1,mx
          fp(i,j,k) = fp(i,j,k) - 0.
        end do
      end do
    end do
  end if

 if (my .gt. 1) then
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    do j=1,my-1
      do i=1,mx
        fp(i, j,k) = fp(i, j,k) - fp(i, j,k) + (fy(i, j,k)-fy(i,j+1,k))
      end do
    end do
    do i=1,mx
        fp(i,my,k) = fp(i,my,k) - fp(i,my,k) + (fy(i,my,k)-fy(i,  1,k))
    end do
  end do
 end if

 if (mz .gt. 1) then
!$omp barrier
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    kp1=mod(k,mz)+1
    do j=1,my
      do i=1,mx
        fp(i,j,k) = fp(i,j,k) - fp(i,j,k) + (fz(i,j,k)-fz(i,j,kp1))
      end do
    end do
  end do
 end if

  call limits_omp(1,mz,izs,ize)
 do k=izs,ize
   do j=1,my
     do i=1,mx
       fp(i,j,k) = fp(i,j,k) - max (0., fp(i,j,k))
     end do
   end do
 end do

END SUBROUTINE

!***********************************************************************
SUBROUTINE xup_sub (f, fp, mx, my, mz)
!
!  f is centered on (i-.5,j,k), fifth order stagger
!
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!+---------------------------------------------------------------------
!
!
  if (mx.le.5) then
    fp = fp - f
    return
  end if
  c = 3./256.
  b = -25./256.
  a = .5-b-c
  a1st = 0.5
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
   do j=1,my
    fp(mx-2,j,k) = fp(mx-2,j,k) - ( &
                   a*(f(mx-2,j,k)+f(mx-1,j,k)) &
                 + b*(f(mx-3,j,k)+f(mx  ,j,k)) &
                 + c*(f(mx-4,j,k)+f(1   ,j,k)))
    fp(mx-1,j,k) = fp(mx-1,j,k) - ( &
                   a*(f(mx-1,j,k)+f(mx  ,j,k)) &
                 + b*(f(mx-2,j,k)+f(1   ,j,k)) &
                 + c*(f(mx-3,j,k)+f(2   ,j,k)))
    fp(mx  ,j,k) = fp(mx  ,j,k) - ( &
                   a*(f(mx  ,j,k)+f(1   ,j,k)) &
                 + b*(f(mx-1,j,k)+f(2   ,j,k)) &
                 + c*(f(mx-2,j,k)+f(3   ,j,k)))
    fp(1   ,j,k) = fp(1   ,j,k) - ( &
                   a*(f(1   ,j,k)+f(2   ,j,k)) &
                 + b*(f(mx  ,j,k)+f(3   ,j,k)) &
                 + c*(f(mx-1,j,k)+f(4   ,j,k)))
    fp(2   ,j,k) = fp(2   ,j,k) - ( &
                   a*(f(2   ,j,k)+f(3   ,j,k)) &
                 + b*(f(1   ,j,k)+f(4   ,j,k)) &
                 + c*(f(mx  ,j,k)+f(5   ,j,k)))
   end do
   do j=1,my
    do i=3,mx-3
      fp(i  ,j,k) = fp(i  ,j,k) - ( &
                    a*(f(i  ,j,k)+f(i+1 ,j,k)) &
                  + b*(f(i-1,j,k)+f(i+2 ,j,k)) &
                  + c*(f(i-2,j,k)+f(i+3 ,j,k)))
    end do
   end do
  end do
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE xup1_sub (f, fp, mx, my, mz)
!
!  f is centered on (i-.5,j,k), fifth order stagger
!
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!+---------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = fp - f
    return
  end if
!
  a = .5
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
   do j=1,my
    fp(mx  ,j,k) = fp(mx  ,j,k) - ( &
                   a*(f(mx  ,j,k)+f(1   ,j,k)))
   end do
   do j=1,my
    do i=1,mx-1
      fp(i  ,j,k) = fp(i  ,j,k) - ( &
                    a*(f(i  ,j,k)+f(i+1 ,j,k)))
    end do
   end do
  end do
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE xdn_sub (f, fp, mx, my, mz)
!
!  f is centered on (i,j,k), fifth order stagger
!
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!+---------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = fp - f
    return
  end if
!
  c = 3./256.
  b = -25./256.
  a = .5-b-c
  a1st = 0.5
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
   do j=1,my
    fp(mx-1,j,k) = fp(mx-1,j,k) - ( &
                   a*(f(mx-2,j,k)+f(mx-1,j,k)) &
                 + b*(f(mx-3,j,k)+f(mx  ,j,k)) &
                 + c*(f(mx-4,j,k)+f(1   ,j,k)))
    fp(mx  ,j,k) = fp(mx  ,j,k) - ( &
                   a*(f(mx-1,j,k)+f(mx  ,j,k)) &
                 + b*(f(mx-2,j,k)+f(1   ,j,k)) &
                 + c*(f(mx-3,j,k)+f(2   ,j,k)))
    fp(1  ,j,k) = fp(1  ,j,k) - ( &
                   a*(f(mx  ,j,k)+f(1   ,j,k)) &
                 + b*(f(mx-1,j,k)+f(2   ,j,k)) &
                 + c*(f(mx-2,j,k)+f(3   ,j,k)))
    fp(2  ,j,k) = fp(2  ,j,k) - ( &
                   a*(f(1   ,j,k)+f(2   ,j,k)) &
                 + b*(f(mx  ,j,k)+f(3   ,j,k)) &
                 + c*(f(mx-1,j,k)+f(4   ,j,k)))
    fp(3  ,j,k) = fp(3  ,j,k) - ( &
                   a*(f(2   ,j,k)+f(3   ,j,k)) &
                 + b*(f(1   ,j,k)+f(4   ,j,k)) &
                 + c*(f(mx  ,j,k)+f(5   ,j,k)))
   end do
   do j=1,my
    do i=4,mx-2
      fp(i,j,k) = fp(i,j,k) - ( &
                   a*(f(i   ,j,k)+f(i-1 ,j,k)) &
                 + b*(f(i+1 ,j,k)+f(i-2 ,j,k)) &
                 + c*(f(i+2 ,j,k)+f(i-3 ,j,k)))
    end do
   end do
  end do
!
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE xdn1_sub (f, fp, mx, my, mz)
!
!  f is centered on (i,j,k), fifth order stagger
!
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!+---------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = fp - f
    return
  end if
!
  a = 0.5
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
   do j=1,my
    fp(1  ,j,k) = fp(1  ,j,k) - ( &
                   a*(f(mx  ,j,k)+f(1   ,j,k)))
   end do
   do j=1,my
    do i=2,mx
      fp(i,j,k) = fp(i,j,k) - ( &
                   a*(f(i   ,j,k)+f(i-1 ,j,k)))
    end do
   end do
  end do
!
END SUBROUTINE
!***********************************************************************
SUBROUTINE xup32_sub (f,j, fp, mx, my, mz)
!
!  f is centered on (i-.5,j,k), fifth order stagger
!
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f
  real, dimension(mx,mz):: fp
!+---------------------------------------------------------------------
!
!
  if (mx.le.5) then
    fp = fp - f(:,j,:)
    return
  end if
  c = 3./256.
  b = -25./256.
  a = .5-b-c
  a1st = 0.5
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    fp(mx-2,k) = fp(mx-2,k) - ( &
                   a*(f(mx-2,j,k)+f(mx-1,j,k)) &
                 + b*(f(mx-3,j,k)+f(mx  ,j,k)) &
                 + c*(f(mx-4,j,k)+f(1   ,j,k)))
    fp(mx-1,k) = fp(mx-1,k) - ( &
                   a*(f(mx-1,j,k)+f(mx  ,j,k)) &
                 + b*(f(mx-2,j,k)+f(1   ,j,k)) &
                 + c*(f(mx-3,j,k)+f(2   ,j,k)))
    fp(mx  ,k) = fp(mx  ,k) - ( &
                   a*(f(mx  ,j,k)+f(1   ,j,k)) &
                 + b*(f(mx-1,j,k)+f(2   ,j,k)) &
                 + c*(f(mx-2,j,k)+f(3   ,j,k)))
    fp(1   ,k) = fp(1   ,k) - ( &
                   a*(f(1   ,j,k)+f(2   ,j,k)) &
                 + b*(f(mx  ,j,k)+f(3   ,j,k)) &
                 + c*(f(mx-1,j,k)+f(4   ,j,k)))
    fp(2   ,k) = fp(2   ,k) - ( &
                   a*(f(2   ,j,k)+f(3   ,j,k)) &
                 + b*(f(1   ,j,k)+f(4   ,j,k)) &
                 + c*(f(mx  ,j,k)+f(5   ,j,k)))
    do i=3,mx-3
      fp(i  ,k) = fp(i  ,k) - ( &
                    a*(f(i  ,j,k)+f(i+1 ,j,k)) &
                  + b*(f(i-1,j,k)+f(i+2 ,j,k)) &
                  + c*(f(i-2,j,k)+f(i+3 ,j,k)))
    end do
  end do
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE xup132_sub (f,j, fp, mx, my, mz)
!
!  f is centered on (i-.5,j,k), fifth order stagger
!
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f
  real, dimension(mx,mz):: fp
!+---------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = fp - f(:,j,:)
    return
  end if
!
  a = .5
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    fp(mx  ,k) = fp(mx  ,k) - ( &
                   a*(f(mx  ,j,k)+f(1   ,j,k)))
    do i=1,mx-1
      fp(i  ,k) = fp(i  ,k) - ( &
                    a*(f(i  ,j,k)+f(i+1 ,j,k)))
    end do
  end do
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE xdn32_sub (f,j, fp, mx, my, mz)
!
!  f is centered on (i,j,k), fifth order stagger
!
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f
  real, dimension(mx,mz):: fp
!+---------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = fp - f(:,j,:)
    return
  end if
!
  c = 3./256.
  b = -25./256.
  a = .5-b-c
  a1st = 0.5
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    fp(mx-1,k) = fp(mx-1,k) - ( &
                   a*(f(mx-2,j,k)+f(mx-1,j,k)) &
                 + b*(f(mx-3,j,k)+f(mx  ,j,k)) &
                 + c*(f(mx-4,j,k)+f(1   ,j,k)))
    fp(mx  ,k) = fp(mx  ,k) - ( &
                   a*(f(mx-1,j,k)+f(mx  ,j,k)) &
                 + b*(f(mx-2,j,k)+f(1   ,j,k)) &
                 + c*(f(mx-3,j,k)+f(2   ,j,k)))
    fp(1  ,k) = fp(1  ,k) - ( &
                   a*(f(mx  ,j,k)+f(1   ,j,k)) &
                 + b*(f(mx-1,j,k)+f(2   ,j,k)) &
                 + c*(f(mx-2,j,k)+f(3   ,j,k)))
    fp(2  ,k) = fp(2  ,k) - ( &
                   a*(f(1   ,j,k)+f(2   ,j,k)) &
                 + b*(f(mx  ,j,k)+f(3   ,j,k)) &
                 + c*(f(mx-1,j,k)+f(4   ,j,k)))
    fp(3  ,k) = fp(3  ,k) - ( &
                   a*(f(2   ,j,k)+f(3   ,j,k)) &
                 + b*(f(1   ,j,k)+f(4   ,j,k)) &
                 + c*(f(mx  ,j,k)+f(5   ,j,k)))
    do i=4,mx-2
      fp(i,k) = fp(i,k) - ( &
                   a*(f(i   ,j,k)+f(i-1 ,j,k)) &
                 + b*(f(i+1 ,j,k)+f(i-2 ,j,k)) &
                 + c*(f(i+2 ,j,k)+f(i-3 ,j,k)))
    end do
  end do
!
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE xdn132_sub (f,j, fp, mx, my, mz)
!
!  f is centered on (i,j,k), fifth order stagger
!
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f
  real, dimension(mx,mz):: fp
!+---------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = fp - f(:,j,:)
    return
  end if
!
  a = 0.5
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    fp(1  ,k) = fp(1  ,k) - ( &
                   a*(f(mx  ,j,k)+f(1   ,j,k)))
    do i=2,mx
      fp(i,k) = fp(i,k) - ( &
                   a*(f(i   ,j,k)+f(i-1 ,j,k)))
    end do
  end do
!
END SUBROUTINE
!***********************************************************************
SUBROUTINE xup22_sub (f, fp, mx, my, mz)
!
!  f is centered on (i-.5,k), fifth order stagger
!
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,mz):: fp, f
!+---------------------------------------------------------------------
!
!
  if (mx.le.5) then
    fp = fp - f
    return
  end if
  c = 3./256.
  b = -25./256.
  a = .5-b-c
  a1st = 0.5
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    fp(mx-2,k) = fp(mx-2,k) - ( &
                   a*(f(mx-2,k)+f(mx-1,k)) &
                 + b*(f(mx-3,k)+f(mx  ,k)) &
                 + c*(f(mx-4,k)+f(1   ,k)))
    fp(mx-1,k) = fp(mx-1,k) - ( &
                   a*(f(mx-1,k)+f(mx  ,k)) &
                 + b*(f(mx-2,k)+f(1   ,k)) &
                 + c*(f(mx-3,k)+f(2   ,k)))
    fp(mx  ,k) = fp(mx  ,k) - ( &
                   a*(f(mx  ,k)+f(1   ,k)) &
                 + b*(f(mx-1,k)+f(2   ,k)) &
                 + c*(f(mx-2,k)+f(3   ,k)))
    fp(1   ,k) = fp(1   ,k) - ( &
                   a*(f(1   ,k)+f(2   ,k)) &
                 + b*(f(mx  ,k)+f(3   ,k)) &
                 + c*(f(mx-1,k)+f(4   ,k)))
    fp(2   ,k) = fp(2   ,k) - ( &
                   a*(f(2   ,k)+f(3   ,k)) &
                 + b*(f(1   ,k)+f(4   ,k)) &
                 + c*(f(mx  ,k)+f(5   ,k)))
    do i=3,mx-3
      fp(i  ,k) = fp(i  ,k) - ( &
                    a*(f(i  ,k)+f(i+1 ,k)) &
                  + b*(f(i-1,k)+f(i+2 ,k)) &
                  + c*(f(i-2,k)+f(i+3 ,k)))
    end do
  end do
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE xup122_sub (f, fp, mx, my, mz)
!
!  f is centered on (i-.5,k), fifth order stagger
!
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,mz):: fp, f
!+---------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = fp - f
    return
  end if
!
  a = .5
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    fp(mx  ,k) = fp(mx  ,k) - ( &
                   a*(f(mx  ,k)+f(1   ,k)))
    do i=1,mx-1
      fp(i  ,k) = fp(i  ,k) - ( &
                    a*(f(i  ,k)+f(i+1 ,k)))
    end do
  end do
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE xdn22_sub (f, fp, mx, my, mz)
!
!  f is centered on (i,k), fifth order stagger
!
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,mz):: fp, f
!+---------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = fp - f
    return
  end if
!
  c = 3./256.
  b = -25./256.
  a = .5-b-c
  a1st = 0.5
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    fp(mx-1,k) = fp(mx-1,k) - ( &
                   a*(f(mx-2,k)+f(mx-1,k)) &
                 + b*(f(mx-3,k)+f(mx  ,k)) &
                 + c*(f(mx-4,k)+f(1   ,k)))
    fp(mx  ,k) = fp(mx  ,k) - ( &
                   a*(f(mx-1,k)+f(mx  ,k)) &
                 + b*(f(mx-2,k)+f(1   ,k)) &
                 + c*(f(mx-3,k)+f(2   ,k)))
    fp(1  ,k) = fp(1  ,k) - ( &
                   a*(f(mx  ,k)+f(1   ,k)) &
                 + b*(f(mx-1,k)+f(2   ,k)) &
                 + c*(f(mx-2,k)+f(3   ,k)))
    fp(2  ,k) = fp(2  ,k) - ( &
                   a*(f(1   ,k)+f(2   ,k)) &
                 + b*(f(mx  ,k)+f(3   ,k)) &
                 + c*(f(mx-1,k)+f(4   ,k)))
    fp(3  ,k) = fp(3  ,k) - ( &
                   a*(f(2   ,k)+f(3   ,k)) &
                 + b*(f(1   ,k)+f(4   ,k)) &
                 + c*(f(mx  ,k)+f(5   ,k)))
    do i=4,mx-2
      fp(i,k) = fp(i,k) - ( &
                   a*(f(i   ,k)+f(i-1 ,k)) &
                 + b*(f(i+1 ,k)+f(i-2 ,k)) &
                 + c*(f(i+2 ,k)+f(i-3 ,k)))
    end do
  end do
!
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE xdn122_sub (f, fp, mx, my, mz)
!
!  f is centered on (i,k), fifth order stagger
!
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,mz):: fp, f
!+---------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = fp - f
    return
  end if
!
  a = 0.5
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    fp(1  ,k) = fp(1  ,k) - ( &
                   a*(f(mx  ,k)+f(1   ,k)))
    do i=2,mx
      fp(i,k) = fp(i,k) - ( &
                   a*(f(i   ,k)+f(i-1 ,k)))
    end do
  end do
!
END SUBROUTINE
!***********************************************************************
SUBROUTINE ddxupi_sub (f, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!+---------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = fp - 0.
    return
  end if
!
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)
  a1st = 1.
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
   do j=1,my
    fp(mx-2,j,k) = fp(mx-2,j,k) - ( &
                   a*(f(mx-1,j,k)-f(mx-2,j,k)) &
                 + b*(f(mx  ,j,k)-f(mx-3,j,k)) &
                 + c*(f(1   ,j,k)-f(mx-4,j,k)))
    fp(mx-1,j,k) = fp(mx-1,j,k) - ( &
                   a*(f(mx  ,j,k)-f(mx-1,j,k)) &
                 + b*(f(1   ,j,k)-f(mx-2,j,k)) &
                 + c*(f(2   ,j,k)-f(mx-3,j,k)))
    fp(mx  ,j,k) = fp(mx  ,j,k) - ( &
                   a*(f(1   ,j,k)-f(mx  ,j,k)) &
                 + b*(f(2   ,j,k)-f(mx-1,j,k)) &
                 + c*(f(3   ,j,k)-f(mx-2,j,k)))
    fp(1  ,j,k) = fp(1  ,j,k) - ( &
                   a*(f(2   ,j,k)-f(1   ,j,k)) &
                 + b*(f(3   ,j,k)-f(mx  ,j,k)) &
                 + c*(f(4   ,j,k)-f(mx-1,j,k)))
    fp(2  ,j,k) = fp(2  ,j,k) - ( &
                   a*(f(3   ,j,k)-f(2  ,j,k)) &
                 + b*(f(4   ,j,k)-f(1  ,j,k)) &
                 + c*(f(5   ,j,k)-f(mx ,j,k)))
   end do
   do j=1,my
    do i=3,mx-3
      fp(i  ,j,k) = fp(i  ,j,k) - ( &
                   a*(f(i+1 ,j,k)-f(i   ,j,k)) &
                 + b*(f(i+2 ,j,k)-f(i-1 ,j,k)) &
                 + c*(f(i+3 ,j,k)-f(i-2 ,j,k)))
    end do
   end do
  end do
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddxupi1_sub (f, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!+---------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = fp - 0.
    return
  end if
!
  a = 1.
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
   do j=1,my
    fp(mx  ,j,k) = fp(mx  ,j,k) - ( &
                   a*(f(1   ,j,k)-f(mx  ,j,k)))
   end do
   do j=1,my
    do i=1,mx-1
      fp(i  ,j,k) = fp(i  ,j,k) - ( &
                   a*(f(i+1 ,j,k)-f(i   ,j,k)))
    end do
   end do
  end do
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddxdni_sub (f, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!+---------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = fp - 0.
    return
  end if
!
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)
  a1st = 1.
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
   do j=1,my
    fp(mx-1,j,k) = fp(mx-1,j,k) - ( &
                   a*(f(mx-1,j,k)-f(mx-2,j,k)) &
                 + b*(f(mx  ,j,k)-f(mx-3,j,k)) &
                 + c*(f(1   ,j,k)-f(mx-4,j,k)))
    fp(mx  ,j,k) = fp(mx  ,j,k) - ( &
                   a*(f(mx  ,j,k)-f(mx-1,j,k)) &
                 + b*(f(1   ,j,k)-f(mx-2,j,k)) &
                 + c*(f(2   ,j,k)-f(mx-3,j,k)))
    fp(1   ,j,k) = fp(1   ,j,k) - ( &
                   a*(f(1   ,j,k)-f(mx  ,j,k)) &
                 + b*(f(2   ,j,k)-f(mx-1,j,k)) &
                 + c*(f(3   ,j,k)-f(mx-2,j,k)))
    fp(2  ,j,k) = fp(2  ,j,k) - ( &
                   a*(f(2   ,j,k)-f(1   ,j,k)) &
                 + b*(f(3   ,j,k)-f(mx  ,j,k)) &
                 + c*(f(4   ,j,k)-f(mx-1,j,k)))
    fp(3  ,j,k) = fp(3  ,j,k) - ( &
                   a*(f(3   ,j,k)-f(2   ,j,k)) &
                 + b*(f(4   ,j,k)-f(1   ,j,k)) &
                 + c*(f(5   ,j,k)-f(mx  ,j,k)))
   end do
   do j=1,my
    do i=4,mx-2
      fp(i,j,k) = fp(i,j,k) - ( &
                   a*(f(i   ,j,k)-f(i-1 ,j,k)) &
                 + b*(f(i+1 ,j,k)-f(i-2 ,j,k)) &
                 + c*(f(i+2 ,j,k)-f(i-3 ,j,k)))
    end do
   end do
  end do
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddxdni1_sub (f, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!+---------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = fp - 0.
    return
  end if
!
  a = 1.
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
   do j=1,my
    fp(1   ,j,k) = fp(1   ,j,k) - ( &
                   a*(f(1   ,j,k)-f(mx  ,j,k)))
   end do
   do j=1,my
    do i=2,mx
      fp(i,j,k) = fp(i,j,k) - ( &
                   a*(f(i   ,j,k)-f(i-1 ,j,k)))
    end do
   end do
  end do
!
END SUBROUTINE
!***********************************************************************
SUBROUTINE ddxupi32_sub (f,j, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f
  real, dimension(mx,mz):: fp
!+---------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = fp - 0.
    return
  end if
!
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)
  a1st = 1.
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    fp(mx-2,k) = fp(mx-2,k) - ( &
                   a*(f(mx-1,j,k)-f(mx-2,j,k)) &
                 + b*(f(mx  ,j,k)-f(mx-3,j,k)) &
                 + c*(f(1   ,j,k)-f(mx-4,j,k)))
    fp(mx-1,k) = fp(mx-1,k) - ( &
                   a*(f(mx  ,j,k)-f(mx-1,j,k)) &
                 + b*(f(1   ,j,k)-f(mx-2,j,k)) &
                 + c*(f(2   ,j,k)-f(mx-3,j,k)))
    fp(mx  ,k) = fp(mx  ,k) - ( &
                   a*(f(1   ,j,k)-f(mx  ,j,k)) &
                 + b*(f(2   ,j,k)-f(mx-1,j,k)) &
                 + c*(f(3   ,j,k)-f(mx-2,j,k)))
    fp(1  ,k) = fp(1  ,k) - ( &
                   a*(f(2   ,j,k)-f(1   ,j,k)) &
                 + b*(f(3   ,j,k)-f(mx  ,j,k)) &
                 + c*(f(4   ,j,k)-f(mx-1,j,k)))
    fp(2  ,k) = fp(2  ,k) - ( &
                   a*(f(3   ,j,k)-f(2  ,j,k)) &
                 + b*(f(4   ,j,k)-f(1  ,j,k)) &
                 + c*(f(5   ,j,k)-f(mx ,j,k)))
    do i=3,mx-3
      fp(i  ,k) = fp(i  ,k) - ( &
                   a*(f(i+1 ,j,k)-f(i   ,j,k)) &
                 + b*(f(i+2 ,j,k)-f(i-1 ,j,k)) &
                 + c*(f(i+3 ,j,k)-f(i-2 ,j,k)))
    end do
  end do
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddxupi132_sub (f,j, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f
  real, dimension(mx,mz):: fp
!+---------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = fp - 0.
    return
  end if
!
  a = 1.
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    fp(mx  ,k) = fp(mx  ,k) - ( &
                   a*(f(1   ,j,k)-f(mx  ,j,k)))
    do i=1,mx-1
      fp(i  ,k) = fp(i  ,k) - ( &
                   a*(f(i+1 ,j,k)-f(i   ,j,k)))
    end do
  end do
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddxdni32_sub (f,j, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f
  real, dimension(mx,mz):: fp
!+---------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = fp - 0.
    return
  end if
!
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)
  a1st = 1.
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    fp(mx-1,k) = fp(mx-1,k) - ( &
                   a*(f(mx-1,j,k)-f(mx-2,j,k)) &
                 + b*(f(mx  ,j,k)-f(mx-3,j,k)) &
                 + c*(f(1   ,j,k)-f(mx-4,j,k)))
    fp(mx  ,k) = fp(mx  ,k) - ( &
                   a*(f(mx  ,j,k)-f(mx-1,j,k)) &
                 + b*(f(1   ,j,k)-f(mx-2,j,k)) &
                 + c*(f(2   ,j,k)-f(mx-3,j,k)))
    fp(1   ,k) = fp(1   ,k) - ( &
                   a*(f(1   ,j,k)-f(mx  ,j,k)) &
                 + b*(f(2   ,j,k)-f(mx-1,j,k)) &
                 + c*(f(3   ,j,k)-f(mx-2,j,k)))
    fp(2  ,k) = fp(2  ,k) - ( &
                   a*(f(2   ,j,k)-f(1   ,j,k)) &
                 + b*(f(3   ,j,k)-f(mx  ,j,k)) &
                 + c*(f(4   ,j,k)-f(mx-1,j,k)))
    fp(3  ,k) = fp(3  ,k) - ( &
                   a*(f(3   ,j,k)-f(2   ,j,k)) &
                 + b*(f(4   ,j,k)-f(1   ,j,k)) &
                 + c*(f(5   ,j,k)-f(mx  ,j,k)))
    do i=4,mx-2
      fp(i,k) = fp(i,k) - ( &
                   a*(f(i   ,j,k)-f(i-1 ,j,k)) &
                 + b*(f(i+1 ,j,k)-f(i-2 ,j,k)) &
                 + c*(f(i+2 ,j,k)-f(i-3 ,j,k)))
    end do
  end do
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddxdni132_sub (f,j, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f
  real, dimension(mx,mz):: fp
!+---------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = fp - 0.
    return
  end if
!
  a = 1.
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    fp(1   ,k) = fp(1   ,k) - ( &
                   a*(f(1   ,j,k)-f(mx  ,j,k)))
    do i=2,mx
      fp(i,k) = fp(i,k) - ( &
                   a*(f(i   ,j,k)-f(i-1 ,j,k)))
    end do
  end do
!
END SUBROUTINE
!***********************************************************************
SUBROUTINE ddxupi22_sub (f, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,mz):: fp, f
!+---------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = fp - 0.
    return
  end if
!
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)
  a1st = 1.
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    fp(mx-2,k) = fp(mx-2,k) - ( &
                   a*(f(mx-1,k)-f(mx-2,k)) &
                 + b*(f(mx  ,k)-f(mx-3,k)) &
                 + c*(f(1   ,k)-f(mx-4,k)))
    fp(mx-1,k) = fp(mx-1,k) - ( &
                   a*(f(mx  ,k)-f(mx-1,k)) &
                 + b*(f(1   ,k)-f(mx-2,k)) &
                 + c*(f(2   ,k)-f(mx-3,k)))
    fp(mx  ,k) = fp(mx  ,k) - ( &
                   a*(f(1   ,k)-f(mx  ,k)) &
                 + b*(f(2   ,k)-f(mx-1,k)) &
                 + c*(f(3   ,k)-f(mx-2,k)))
    fp(1  ,k) = fp(1  ,k) - ( &
                   a*(f(2   ,k)-f(1   ,k)) &
                 + b*(f(3   ,k)-f(mx  ,k)) &
                 + c*(f(4   ,k)-f(mx-1,k)))
    fp(2  ,k) = fp(2  ,k) - ( &
                   a*(f(3   ,k)-f(2  ,k)) &
                 + b*(f(4   ,k)-f(1  ,k)) &
                 + c*(f(5   ,k)-f(mx ,k)))
    do i=3,mx-3
      fp(i  ,k) = fp(i  ,k) - ( &
                   a*(f(i+1 ,k)-f(i   ,k)) &
                 + b*(f(i+2 ,k)-f(i-1 ,k)) &
                 + c*(f(i+3 ,k)-f(i-2 ,k)))
    end do
  end do
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddxupi122_sub (f, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,mz):: fp, f
!+---------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = fp - 0.
    return
  end if
!
  a = 1.
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    fp(mx  ,k) = fp(mx  ,k) - ( &
                   a*(f(1   ,k)-f(mx  ,k)))
    do i=1,mx-1
      fp(i  ,k) = fp(i  ,k) - ( &
                   a*(f(i+1 ,k)-f(i   ,k)))
    end do
  end do
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddxdni22_sub (f, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,mz):: fp, f
!+---------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = fp - 0.
    return
  end if
!
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)
  a1st = 1.
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    fp(mx-1,k) = fp(mx-1,k) - ( &
                   a*(f(mx-1,k)-f(mx-2,k)) &
                 + b*(f(mx  ,k)-f(mx-3,k)) &
                 + c*(f(1   ,k)-f(mx-4,k)))
    fp(mx  ,k) = fp(mx  ,k) - ( &
                   a*(f(mx  ,k)-f(mx-1,k)) &
                 + b*(f(1   ,k)-f(mx-2,k)) &
                 + c*(f(2   ,k)-f(mx-3,k)))
    fp(1   ,k) = fp(1   ,k) - ( &
                   a*(f(1   ,k)-f(mx  ,k)) &
                 + b*(f(2   ,k)-f(mx-1,k)) &
                 + c*(f(3   ,k)-f(mx-2,k)))
    fp(2  ,k) = fp(2  ,k) - ( &
                   a*(f(2   ,k)-f(1   ,k)) &
                 + b*(f(3   ,k)-f(mx  ,k)) &
                 + c*(f(4   ,k)-f(mx-1,k)))
    fp(3  ,k) = fp(3  ,k) - ( &
                   a*(f(3   ,k)-f(2   ,k)) &
                 + b*(f(4   ,k)-f(1   ,k)) &
                 + c*(f(5   ,k)-f(mx  ,k)))
    do i=4,mx-2
      fp(i,k) = fp(i,k) - ( &
                   a*(f(i   ,k)-f(i-1 ,k)) &
                 + b*(f(i+1 ,k)-f(i-2 ,k)) &
                 + c*(f(i+2 ,k)-f(i-3 ,k)))
    end do
  end do
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddxdni122_sub (f, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,mz):: fp, f
!+---------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = fp - 0.
    return
  end if
!
  a = 1.
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    fp(1   ,k) = fp(1   ,k) - ( &
                   a*(f(1   ,k)-f(mx  ,k)))
    do i=2,mx
      fp(i,k) = fp(i,k) - ( &
                   a*(f(i   ,k)-f(i-1 ,k)))
    end do
  end do
!
END SUBROUTINE
!***********************************************************************
SUBROUTINE ddxup_sub (f, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!+---------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = fp - 0.
    return
  end if
!
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)
  a1st = 1.
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
   do j=1,my
    fp(mx-2,j,k) = fp(mx-2,j,k) - ( &
                 + c*(f(1   ,j,k)-f(mx-4,j,k)) &
                 + b*(f(mx  ,j,k)-f(mx-3,j,k)) &
                 + a*(f(mx-1,j,k)-f(mx-2,j,k)))
    fp(mx-1,j,k) = fp(mx-1,j,k) - ( &
                 + c*(f(2   ,j,k)-f(mx-3,j,k)) &
                 + b*(f(1   ,j,k)-f(mx-2,j,k)) &
                 + a*(f(mx  ,j,k)-f(mx-1,j,k)))
    fp(mx  ,j,k) = fp(mx  ,j,k) - ( &
                 + c*(f(3   ,j,k)-f(mx-2,j,k)) &
                 + b*(f(2   ,j,k)-f(mx-1,j,k)) &
                 + a*(f(1   ,j,k)-f(mx  ,j,k)))
    fp(1  ,j,k) = fp(1  ,j,k) - ( &
                 + c*(f(4   ,j,k)-f(mx-1,j,k)) &
                 + b*(f(3   ,j,k)-f(mx  ,j,k)) &
                 + a*(f(2   ,j,k)-f(1   ,j,k)))
    fp(2  ,j,k) = fp(2  ,j,k) - ( &
                 + c*(f(5   ,j,k)-f(mx  ,j,k)) &
                 + b*(f(4   ,j,k)-f(1   ,j,k)) &
                 + a*(f(3   ,j,k)-f(2   ,j,k)))
   end do
   do j=1,my
    do i=3,mx-3
      fp(i  ,j,k) = fp(i  ,j,k) - ( &
                 + c*(f(i+3 ,j,k)-f(i-2 ,j,k)) &
                 + b*(f(i+2 ,j,k)-f(i-1 ,j,k)) &
                 + a*(f(i+1 ,j,k)-f(i   ,j,k)))
    end do
  end do
  end do
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddxup1_sub (f, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!+---------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = fp - 0.
    return
  end if
!
  a = 1.
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
   do j=1,my
    fp(mx  ,j,k) = fp(mx  ,j,k) - ( &
                   a*(f(1   ,j,k)-f(mx  ,j,k)))
   end do
   do j=1,my
    do i=1,mx-1
      fp(i  ,j,k) = fp(i  ,j,k) - ( &
                   a*(f(i+1 ,j,k)-f(i   ,j,k)))
    end do
  end do
  end do
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddxdn_sub (f, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!+---------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = fp - 0.
    return
  end if
!
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)
  a1st = 1.
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
   do j=1,my
    fp(mx-1,j,k) = fp(mx-1,j,k) - ( &
                 + c*(f(1   ,j,k)-f(mx-4,j,k)) &
                 + b*(f(mx  ,j,k)-f(mx-3,j,k)) &
                 + a*(f(mx-1,j,k)-f(mx-2,j,k)))
    fp(mx  ,j,k) = fp(mx  ,j,k) - ( &
                 + c*(f(2   ,j,k)-f(mx-3,j,k)) &
                 + b*(f(1   ,j,k)-f(mx-2,j,k)) &
                 + a*(f(mx  ,j,k)-f(mx-1,j,k)))
    fp(1   ,j,k) = fp(1   ,j,k) - ( &
                 + c*(f(3   ,j,k)-f(mx-2,j,k)) &
                 + b*(f(2   ,j,k)-f(mx-1,j,k)) &
                 + a*(f(1   ,j,k)-f(mx  ,j,k)))
    fp(2  ,j,k) = fp(2  ,j,k) - ( &
                 + c*(f(4   ,j,k)-f(mx-1,j,k)) &
                 + b*(f(3   ,j,k)-f(mx  ,j,k)) &
                 + a*(f(2   ,j,k)-f(1   ,j,k)))
    fp(3  ,j,k) = fp(3  ,j,k) - ( &
                 + c*(f(5   ,j,k)-f(mx  ,j,k)) &
                 + b*(f(4   ,j,k)-f(1   ,j,k)) &
                 + a*(f(3   ,j,k)-f(2   ,j,k)))
   end do
   do j=1,my
    do i=4,mx-2
      fp(i,j,k) = fp(i,j,k) - ( &
                 + c*(f(i+2 ,j,k)-f(i-3 ,j,k)) &
                 + b*(f(i+1 ,j,k)-f(i-2 ,j,k)) &
                 + a*(f(i   ,j,k)-f(i-1 ,j,k)))
    end do
   end do
  end do
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddxdn1_sub (f, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!+---------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = fp - 0.
    return
  end if
!
  a = 1.
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
   do j=1,my
    fp(1   ,j,k) = fp(1   ,j,k) - ( &
                   a*(f(1   ,j,k)-f(mx  ,j,k)))
   end do
   do j=1,my
    do i=2,mx
      fp(i,j,k) = fp(i,j,k) - ( &
                   a*(f(i   ,j,k)-f(i-1 ,j,k)))
    end do
   end do
  end do
!
END SUBROUTINE
!***********************************************************************
SUBROUTINE yup_sub (f, fp, mx, my, mz)
!
!  f is centered on (i,j-.5,k), fifth order stagger
!
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, intent(in) , dimension(mx,my,mz):: f
  real,              dimension(mx,my,mz):: fp
!+---------------------------------------------------------------------
!

  if (my .lt. 5) then
  call limits_omp(1,mz,izs,ize)
    do k=izs,ize
      fp(:,:,k) = fp(:,:,k) - f(:,:,k)
    end do
    return
  end if
  c = 3./256.
  b = -25./256.
  a = .5-b-c
  a1st = 0.5
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    do i=1,mx
      fp(i,my-2,k) = fp(i,my-2,k) - ( &
                     a*(f(i,my-2,k)+f(i,my-1,k)) + &
                     b*(f(i,my-3,k)+f(i,my  ,k)) + &
                     c*(f(i,my-4,k)+f(i,1   ,k)))
      fp(i,my-1,k) = fp(i,my-1,k) - ( &
                     a*(f(i,my-1,k)+f(i,my  ,k)) + &
                     b*(f(i,my-2,k)+f(i,1   ,k)) + &
                     c*(f(i,my-3,k)+f(i,2   ,k)))
      fp(i,my  ,k) = fp(i,my  ,k) - ( &
                     a*(f(i,my  ,k)+f(i,1   ,k)) + &
                     b*(f(i,my-1,k)+f(i,2   ,k)) + &
                     c*(f(i,my-2,k)+f(i,3   ,k)))
      fp(i,1   ,k) = fp(i,1   ,k) - ( &
                     a*(f(i,1   ,k)+f(i,2   ,k)) + &
                     b*(f(i,my  ,k)+f(i,3   ,k)) + &
                     c*(f(i,my-1,k)+f(i,4   ,k)))
      fp(i,2   ,k) = fp(i,2   ,k) - ( &
                     a*(f(i,2   ,k)+f(i,3   ,k)) + &
                     b*(f(i,1   ,k)+f(i,4   ,k)) + &
                     c*(f(i,my  ,k)+f(i,5   ,k)))
    end do
    do j=3,my-3
     do i=1,mx
      fp(i,j  ,k) = fp(i,j  ,k) - ( &
                     a*(f(i,j   ,k)+f(i,j+1 ,k)) + &
                     b*(f(i,j-1 ,k)+f(i,j+2 ,k)) + &
                     c*(f(i,j-2 ,k)+f(i,j+3 ,k)))
     end do
    end do
  end do
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE yup1_sub (f, fp, mx, my, mz)
!
!  f is centered on (i,j-.5,k), fifth order stagger
!
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, intent(in) , dimension(mx,my,mz):: f
  real,              dimension(mx,my,mz):: fp
!+---------------------------------------------------------------------
!
  if (my .lt. 5) then
  call limits_omp(1,mz,izs,ize)
    do k=izs,ize
      fp(:,:,k) = fp(:,:,k) - f(:,:,k)
    end do
    return
  end if
!
  a = 0.5
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    do i=1,mx
      fp(i,my,k) = fp(i,my,k) - ( &
                     a*(f(i,my,k)+f(i,1,k)))
    end do
    do j=1,my-1
      do i=1,mx
        fp(i,j  ,k) = fp(i,j  ,k) - ( &
                     a*(f(i,j   ,k)+f(i,j+1 ,k)))
      end do
    end do
  end do
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE ydn_sub (f, fp, mx, my, mz)
!
!  f is centered on (i,j,k), fifth order stagger
!
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!
!+---------------------------------------------------------------------
  if (my .lt. 5) then
  call limits_omp(1,mz,izs,ize)
    do k=izs,ize
      fp(:,:,k) = fp(:,:,k) - f(:,:,k)
    end do
    return
  end if
  c = 3./256.
  b = -25./256.
  a = .5-b-c
  a1st = 0.5
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    do i=1,mx
      fp(i,my-1,k) = fp(i,my-1,k) - ( &
                     a*(f(i,my-2,k)+f(i,my-1,k)) + &
                     b*(f(i,my-3,k)+f(i,my  ,k)) + &
                     c*(f(i,my-4,k)+f(i,1   ,k)))
      fp(i,my  ,k) = fp(i,my  ,k) - ( &
                     a*(f(i,my-1,k)+f(i,my  ,k)) + &
                     b*(f(i,my-2,k)+f(i,1   ,k)) + &
                     c*(f(i,my-3,k)+f(i,2   ,k)))
      fp(i,1   ,k) = fp(i,1   ,k) - ( &
                     a*(f(i,my  ,k)+f(i,1   ,k)) + &
                     b*(f(i,my-1,k)+f(i,2   ,k)) + &
                     c*(f(i,my-2,k)+f(i,3   ,k)))
      fp(i,2   ,k) = fp(i,2   ,k) - ( &
                     a*(f(i,1   ,k)+f(i,2   ,k)) + &
                     b*(f(i,my  ,k)+f(i,3   ,k)) + &
                     c*(f(i,my-1,k)+f(i,4   ,k)))
      fp(i,3   ,k) = fp(i,3   ,k) - ( &
                     a*(f(i,2   ,k)+f(i,3   ,k)) + &
                     b*(f(i,1   ,k)+f(i,4   ,k)) + &
                     c*(f(i,my  ,k)+f(i,5   ,k)))
    end do
    do j=4,my-2
     do i=1,mx
      fp(i,j,k) = fp(i,j,k) - ( &
                     a*(f(i,j   ,k)+f(i,j-1 ,k)) + &
                     b*(f(i,j+1 ,k)+f(i,j-2 ,k)) + &
                     c*(f(i,j+2 ,k)+f(i,j-3 ,k)))
     end do
    end do
  end do
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE ydn1_sub (f, fp, mx, my, mz)
!
!  f is centered on (i,j,k), fifth order stagger
!
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!
!+---------------------------------------------------------------------
  if (my .lt. 5) then
  call limits_omp(1,mz,izs,ize)
    do k=izs,ize
      fp(:,:,k) = fp(:,:,k) - f(:,:,k)
    end do
    return
  end if
!
  a = 0.5
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    do i=1,mx
      fp(i,1   ,k) = fp(i,1   ,k) - ( &
                     a*(f(i,my  ,k)+f(i,1   ,k)))
    end do
    do j=2,my
     do i=1,mx
      fp(i,j,k) = fp(i,j,k) - ( &
                     a*(f(i,j   ,k)+f(i,j-1 ,k)))
     end do
    end do
  end do
!
END SUBROUTINE
!***********************************************************************
SUBROUTINE ddyupi_sub (f, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!+---------------------------------------------------------------------
!
  if (my .lt. 3) then
    fp = fp - 0.
    return
  end if
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)
  a1st = 1.
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    do i=1,mx
      fp(i,my-2,k) = fp(i,my-2,k) - ( &
                     a*(f(i,my-1,k)-f(i,my-2,k)) + &
                     b*(f(i,my  ,k)-f(i,my-3,k)) + &
                     c*(f(i,1   ,k)-f(i,my-4,k)))
      fp(i,my-1,k) = fp(i,my-1,k) - ( &
                     a*(f(i,my  ,k)-f(i,my-1,k)) + &
                     b*(f(i,1   ,k)-f(i,my-2,k)) + &
                     c*(f(i,2   ,k)-f(i,my-3,k)))
      fp(i,my  ,k) = fp(i,my  ,k) - ( &
                     a*(f(i,1   ,k)-f(i,my  ,k)) + &
                     b*(f(i,2   ,k)-f(i,my-1,k)) + &
                     c*(f(i,3   ,k)-f(i,my-2,k)))
      fp(i,1   ,k) = fp(i,1   ,k) - ( &
                     a*(f(i,2   ,k)-f(i,1   ,k)) + &
                     b*(f(i,3   ,k)-f(i,my  ,k)) + &
                     c*(f(i,4   ,k)-f(i,my-1,k)))
      fp(i,2   ,k) = fp(i,2   ,k) - ( &
                     a*(f(i,3   ,k)-f(i,2   ,k)) + &
                     b*(f(i,4   ,k)-f(i,1   ,k)) + &
                     c*(f(i,5   ,k)-f(i,my  ,k)))
    end do
    do j=3,my-3
     do i=1,mx
      fp(i,j  ,k) = fp(i,j  ,k) - ( &
                     a*(f(i,j+1 ,k)-f(i,j   ,k)) + &
                     b*(f(i,j+2 ,k)-f(i,j-1 ,k)) + &
                     c*(f(i,j+3 ,k)-f(i,j-2 ,k)))
     end do
    end do
  end do
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddyupi1_sub (f, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!+---------------------------------------------------------------------
!
  if (my .lt. 3) then
    fp = fp - 0.
    return
  end if
  a = 1.
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    do i=1,mx
      fp(i,my,k) = fp(i,my,k) - ( &
                     a*(f(i,1  ,k)-f(i,my,k)))
    end do
    do j=1,my-1
      do i=1,mx
        fp(i,j  ,k) = fp(i,j  ,k) - ( &
                     a*(f(i,j+1 ,k)-f(i,j   ,k)))
     end do
    end do
  end do
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddydni_sub (f, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!+---------------------------------------------------------------------
!
  if (my .lt. 3) then
    fp = fp - 0.
    return
  end if
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)
  a1st = 1.
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    do i=1,mx
      fp(i,my-1,k) = fp(i,my-1,k) - ( &
                     a*(f(i,my-1,k)-f(i,my-2,k)) + &
                     b*(f(i,my  ,k)-f(i,my-3,k)) + &
                     c*(f(i,1   ,k)-f(i,my-4,k)))
      fp(i,my  ,k) = fp(i,my  ,k) - ( &
                     a*(f(i,my  ,k)-f(i,my-1,k)) + &
                     b*(f(i,1   ,k)-f(i,my-2,k)) + &
                     c*(f(i,2   ,k)-f(i,my-3,k)))
      fp(i,1  ,k) = fp(i,1  ,k) - ( &
                     a*(f(i,1   ,k)-f(i,my  ,k)) + &
                     b*(f(i,2   ,k)-f(i,my-1,k)) + &
                     c*(f(i,3   ,k)-f(i,my-2,k)))
      fp(i,2   ,k) = fp(i,2   ,k) - ( &
                     a*(f(i,2   ,k)-f(i,1   ,k)) + &
                     b*(f(i,3   ,k)-f(i,my  ,k)) + &
                     c*(f(i,4   ,k)-f(i,my-1,k)))
      fp(i,3   ,k) = fp(i,3   ,k) - ( &
                     a*(f(i,3   ,k)-f(i,2   ,k)) + &
                     b*(f(i,4   ,k)-f(i,1   ,k)) + &
                     c*(f(i,5   ,k)-f(i,my  ,k)))
    end do
    do j=4,my-2
     do i=1,mx
      fp(i,j ,k) = fp(i,j ,k) - ( &
                     a*(f(i,j   ,k)-f(i,j-1 ,k)) + &
                     b*(f(i,j+1 ,k)-f(i,j-2 ,k)) + &
                     c*(f(i,j+2 ,k)-f(i,j-3 ,k)))
     end do
    end do
  end do
!
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddydni1_sub (f, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!+---------------------------------------------------------------------
!
  if (my .lt. 3) then
    fp = fp - 0.
    return
  end if
  a = 1.
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    do i=1,mx
      fp(i,1  ,k) = fp(i,1  ,k) - ( &
                     a*(f(i,1   ,k)-f(i,my  ,k)))
    end do
    do j=2,my
     do i=1,mx
      fp(i,j ,k) = fp(i,j ,k) - ( &
                     a*(f(i,j   ,k)-f(i,j-1 ,k)))
     end do
    end do
  end do
!
END SUBROUTINE
!***********************************************************************
SUBROUTINE ddyup_sub (f, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!+---------------------------------------------------------------------
!
  if (my .lt. 3) then
    fp = fp - 0.
    return
  end if
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)
  a1st = 1.
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    do i=1,mx
      fp(i,my-2,k) = fp(i,my-2,k) - ( &
                     c*(f(i,1   ,k)-f(i,my-4,k)) + &
                     b*(f(i,my  ,k)-f(i,my-3,k)) + &
                     a*(f(i,my-1,k)-f(i,my-2,k)) )
      fp(i,my-1,k) = fp(i,my-1,k) - ( &
                     c*(f(i,2   ,k)-f(i,my-3,k)) + &
                     b*(f(i,1   ,k)-f(i,my-2,k)) + &
                     a*(f(i,my  ,k)-f(i,my-1,k)) )
      fp(i,my  ,k) = fp(i,my  ,k) - ( &
                     c*(f(i,3   ,k)-f(i,my-2,k)) + &
                     b*(f(i,2   ,k)-f(i,my-1,k)) + &
                     a*(f(i,1   ,k)-f(i,my  ,k)) )
      fp(i,1   ,k) = fp(i,1   ,k) - ( &
                     c*(f(i,4   ,k)-f(i,my-1,k)) + &
                     b*(f(i,3   ,k)-f(i,my  ,k)) + &
                     a*(f(i,2   ,k)-f(i,1   ,k)) )
      fp(i,2   ,k) = fp(i,2   ,k) - ( &
                     c*(f(i,5   ,k)-f(i,my  ,k)) + &
                     b*(f(i,4   ,k)-f(i,1   ,k)) + &
                     a*(f(i,3   ,k)-f(i,2   ,k)) )
    end do
    do j=3,my-3
     do i=1,mx
      fp(i,j  ,k) = fp(i,j  ,k) - ( &
                     c*(f(i,j+3 ,k)-f(i,j-2 ,k)) + &
                     b*(f(i,j+2 ,k)-f(i,j-1 ,k)) + &
                     a*(f(i,j+1 ,k)-f(i,j   ,k)) )
     end do
    end do
  end do
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddyup1_sub (f, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!+---------------------------------------------------------------------
!
  if (my .lt. 3) then
    fp = fp - 0.
    return
  end if
  a = 1.
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    do i=1,mx
      fp(i,my,k) = fp(i,my,k) - ( &
                     a*(f(i,1  ,k)-f(i,my,k)))
    end do
    do j=1,my-1
      do i=1,mx
        fp(i,j  ,k) = fp(i,j  ,k) - ( &
                     a*(f(i,j+1 ,k)-f(i,j   ,k)))
     end do
    end do
  end do
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddydn_sub (f, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!+---------------------------------------------------------------------
!
  if (my .lt. 3) then
    fp = fp - 0.
    return
  end if
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)
  a1st = 1.
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    do i=1,mx
      fp(i,my-1,k) = fp(i,my-1,k) - ( &
                     c*(f(i,1   ,k)-f(i,my-4,k)) + &
                     b*(f(i,my  ,k)-f(i,my-3,k)) + &
                     a*(f(i,my-1,k)-f(i,my-2,k)) ) 
      fp(i,my  ,k) = fp(i,my  ,k) - ( &
                     c*(f(i,2   ,k)-f(i,my-3,k)) + &
                     b*(f(i,1   ,k)-f(i,my-2,k)) + &
                     a*(f(i,my  ,k)-f(i,my-1,k)) )
      fp(i,1  ,k) = fp(i,1  ,k) - ( &
                     c*(f(i,3   ,k)-f(i,my-2,k)) + &
                     b*(f(i,2   ,k)-f(i,my-1,k)) + &
                     a*(f(i,1   ,k)-f(i,my  ,k)) )
      fp(i,2   ,k) = fp(i,2   ,k) - ( &
                     c*(f(i,4   ,k)-f(i,my-1,k)) + &
                     b*(f(i,3   ,k)-f(i,my  ,k)) + &
                     a*(f(i,2   ,k)-f(i,1   ,k)) )
      fp(i,3   ,k) = fp(i,3   ,k) - ( &
                     c*(f(i,5   ,k)-f(i,my  ,k)) + &
                     b*(f(i,4   ,k)-f(i,1   ,k)) + &
                     a*(f(i,3   ,k)-f(i,2   ,k)) )
    end do
    do j=4,my-2
     do i=1,mx
      fp(i,j ,k) = fp(i,j ,k) - ( &
                     c*(f(i,j+2 ,k)-f(i,j-3 ,k)) + &
                     b*(f(i,j+1 ,k)-f(i,j-2 ,k)) + &
                     a*(f(i,j   ,k)-f(i,j-1 ,k)) )
     end do
    end do
  end do
!
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddydn1_sub (f, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!+---------------------------------------------------------------------
!
  if (my .lt. 3) then
    fp = fp - 0.
    return
  end if
  a = 1.
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    do i=1,mx
      fp(i,1  ,k) = fp(i,1  ,k) - ( &
                     a*(f(i,1   ,k)-f(i,my  ,k)))
    end do
    do j=2,my
     do i=1,mx
      fp(i,j ,k) = fp(i,j ,k) - ( &
                     a*(f(i,j   ,k)-f(i,j-1 ,k)))
     end do
    end do
  end do
!
END SUBROUTINE
!***********************************************************************
SUBROUTINE zup_sub (f, fp, mx, my, mz)
!
!  f is centered on (i,j,k-.5), fifth order stagger
!
  implicit none
  real a, b, c, a1st
  integer i, j, k, km2, km1, kp1, kp2, kp3, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!+---------------------------------------------------------------------
!
!
  if (mz.le.5) then
    fp = fp - f
    return
  end if
  c = 3./256.
  b = -25./256.
  a = .5-b-c
  a1st = 0.5
!
!$omp barrier
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    km2=mod(k-3+mz,mz)+1
    km1=mod(k-2+mz,mz)+1
    kp1=mod(k     ,mz)+1
    kp2=mod(k+1   ,mz)+1
    kp3=mod(k+2   ,mz)+1
    do j=1,my
      do i=1,mx
        fp(i,j,k) = fp(i,j,k) - ( &
                     a*(f(i,j,kp1)+f(i,j,k  )) + &
                     b*(f(i,j,kp2)+f(i,j,km1)) + &
                     c*(f(i,j,kp3)+f(i,j,km2)))
      end do
    end do
  end do
!$omp barrier
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE zup1_sub (f, fp, mx, my, mz)
!
!  f is centered on (i,j,k-.5), fifth order stagger
!
  implicit none
  real a, b, c, a1st
  integer i, j, k, kp1, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!+---------------------------------------------------------------------
!
  if (mz.le.5) then
    fp = fp - f
    return
  end if
  a = .5
!
!$omp barrier
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    kp1=mod(k     ,mz)+1
    do j=1,my
      do i=1,mx
        fp(i,j,k) = fp(i,j,k) - ( &
                     a*(f(i,j,kp1)+f(i,j,k  )))
      end do
    end do
  end do
!$omp barrier
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE zdn_sub (f, fp, mx, my, mz)
!
!  f is centered on (i,j,k), fifth order stagger
!
  implicit none
  real a, b, c, a1st
  integer i, j, k, km3, km2, km1, kp1, kp2, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!+---------------------------------------------------------------------
!
  if (mz.le.5) then
    fp = fp - f
    return
  end if
  c = 3./256.
  b = -25./256.
  a = .5-b-c
  a1st = 0.5
!
!$omp barrier
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    km3=mod(k-4+mz,mz)+1
    km2=mod(k-3+mz,mz)+1
    km1=mod(k-2+mz,mz)+1
    kp1=mod(k     ,mz)+1
    kp2=mod(k+1   ,mz)+1
    do j=1,my
      do i=1,mx
        fp(i,j,k) = fp(i,j,k) - ( &
                     a*(f(i,j,k  )+f(i,j,km1)) + &
                     b*(f(i,j,kp1)+f(i,j,km2)) + &
                     c*(f(i,j,kp2)+f(i,j,km3)))
      end do
    end do
  end do
!$omp barrier
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE zdn1_sub (f, fp, mx, my, mz)
!
!  f is centered on (i,j,k), fifth order stagger
!
  implicit none
  real a, b, c, a1st
  integer i, j, k, km1, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!+---------------------------------------------------------------------
!
  if (mz.le.5) then
    fp = fp - f
    return
  end if
  a = .5
!
!$omp barrier
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    km1=mod(k-2+mz,mz)+1
    do j=1,my
      do i=1,mx
        fp(i,j,k) = fp(i,j,k) - ( &
                     a*(f(i,j,k  )+f(i,j,km1)))
      end do
    end do
  end do
!$omp barrier
!
END SUBROUTINE
!***********************************************************************
SUBROUTINE zup32_sub (f,j, fp, mx, my, mz)
!
!  f is centered on (i,j,k-.5), fifth order stagger
!
  implicit none
  real a, b, c, a1st
  integer i, j, k, km2, km1, kp1, kp2, kp3, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f
  real, dimension(mx,mz):: fp
!+---------------------------------------------------------------------
!
!
  if (mz.le.5) then
    fp = fp - f(:,j,:)
    return
  end if
  c = 3./256.
  b = -25./256.
  a = .5-b-c
  a1st = 0.5
!
!$omp barrier
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    km2=mod(k-3+mz,mz)+1
    km1=mod(k-2+mz,mz)+1
    kp1=mod(k     ,mz)+1
    kp2=mod(k+1   ,mz)+1
    kp3=mod(k+2   ,mz)+1
      do i=1,mx
        fp(i,k) = fp(i,k) - ( &
                     a*(f(i,j,kp1)+f(i,j,k  )) + &
                     b*(f(i,j,kp2)+f(i,j,km1)) + &
                     c*(f(i,j,kp3)+f(i,j,km2)))
      end do
  end do
!$omp barrier
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE zup132_sub (f,j, fp, mx, my, mz)
!
!  f is centered on (i,j,k-.5), fifth order stagger
!
  implicit none
  real a, b, c, a1st
  integer i, j, k, kp1, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f
  real, dimension(mx,mz):: fp
!+---------------------------------------------------------------------
!
  if (mz.le.5) then
    fp = fp - f(:,j,:)
    return
  end if
  a = .5
!
!$omp barrier
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    kp1=mod(k     ,mz)+1
      do i=1,mx
        fp(i,k) = fp(i,k) - ( &
                     a*(f(i,j,kp1)+f(i,j,k  )))
      end do
  end do
!$omp barrier
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE zdn32_sub (f,j, fp, mx, my, mz)
!
!  f is centered on (i,j,k), fifth order stagger
!
  implicit none
  real a, b, c, a1st
  integer i, j, k, km3, km2, km1, kp1, kp2, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f
  real, dimension(mx,mz):: fp
!+---------------------------------------------------------------------
!
  if (mz.le.5) then
    fp = fp - f(:,j,:)
    return
  end if
  c = 3./256.
  b = -25./256.
  a = .5-b-c
  a1st = 0.5
!
!$omp barrier
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    km3=mod(k-4+mz,mz)+1
    km2=mod(k-3+mz,mz)+1
    km1=mod(k-2+mz,mz)+1
    kp1=mod(k     ,mz)+1
    kp2=mod(k+1   ,mz)+1
      do i=1,mx
        fp(i,k) = fp(i,k) - ( &
                     a*(f(i,j,k  )+f(i,j,km1)) + &
                     b*(f(i,j,kp1)+f(i,j,km2)) + &
                     c*(f(i,j,kp2)+f(i,j,km3)))
      end do
  end do
!$omp barrier
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE zdn132_sub (f,j, fp, mx, my, mz)
!
!  f is centered on (i,j,k), fifth order stagger
!
  implicit none
  real a, b, c, a1st
  integer i, j, k, km1, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f
  real, dimension(mx,mz):: fp
!+---------------------------------------------------------------------
!
  if (mz.le.5) then
    fp = fp - f(:,j,:)
    return
  end if
  a = .5
!
!$omp barrier
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    km1=mod(k-2+mz,mz)+1
      do i=1,mx
        fp(i,k) = fp(i,k) - ( &
                     a*(f(i,j,k  )+f(i,j,km1)))
      end do
  end do
!$omp barrier
!
END SUBROUTINE
!***********************************************************************
SUBROUTINE zup22_sub (f, fp, mx, my, mz)
!
!  f is centered on (i,k-.5), fifth order stagger
!
  implicit none
  real a, b, c, a1st
  integer i, j, k, km2, km1, kp1, kp2, kp3, mx, my, mz, izs, ize
  real, dimension(mx,mz):: fp, f
!+---------------------------------------------------------------------
!
!
  if (mz.le.5) then
    fp = fp - f
    return
  end if
  c = 3./256.
  b = -25./256.
  a = .5-b-c
  a1st = 0.5
!
!$omp barrier
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    km2=mod(k-3+mz,mz)+1
    km1=mod(k-2+mz,mz)+1
    kp1=mod(k     ,mz)+1
    kp2=mod(k+1   ,mz)+1
    kp3=mod(k+2   ,mz)+1
      do i=1,mx
        fp(i,k) = fp(i,k) - ( &
                     a*(f(i,kp1)+f(i,k  )) + &
                     b*(f(i,kp2)+f(i,km1)) + &
                     c*(f(i,kp3)+f(i,km2)))
      end do
  end do
!$omp barrier
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE zup122_sub (f, fp, mx, my, mz)
!
!  f is centered on (i,k-.5), fifth order stagger
!
  implicit none
  real a, b, c, a1st
  integer i, j, k, kp1, mx, my, mz, izs, ize
  real, dimension(mx,mz):: fp, f
!+---------------------------------------------------------------------
!
  if (mz.le.5) then
    fp = fp - f
    return
  end if
  a = .5
!
!$omp barrier
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    kp1=mod(k     ,mz)+1
      do i=1,mx
        fp(i,k) = fp(i,k) - ( &
                     a*(f(i,kp1)+f(i,k  )))
      end do
  end do
!$omp barrier
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE zdn22_sub (f, fp, mx, my, mz)
!
!  f is centered on (i,k), fifth order stagger
!
  implicit none
  real a, b, c, a1st
  integer i, j, k, km3, km2, km1, kp1, kp2, mx, my, mz, izs, ize
  real, dimension(mx,mz):: fp, f
!+---------------------------------------------------------------------
!
  if (mz.le.5) then
    fp = fp - f
    return
  end if
  c = 3./256.
  b = -25./256.
  a = .5-b-c
  a1st = 0.5
!
!$omp barrier
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    km3=mod(k-4+mz,mz)+1
    km2=mod(k-3+mz,mz)+1
    km1=mod(k-2+mz,mz)+1
    kp1=mod(k     ,mz)+1
    kp2=mod(k+1   ,mz)+1
      do i=1,mx
        fp(i,k) = fp(i,k) - ( &
                     a*(f(i,k  )+f(i,km1)) + &
                     b*(f(i,kp1)+f(i,km2)) + &
                     c*(f(i,kp2)+f(i,km3)))
      end do
  end do
!$omp barrier
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE zdn122_sub (f, fp, mx, my, mz)
!
!  f is centered on (i,k), fifth order stagger
!
  implicit none
  real a, b, c, a1st
  integer i, j, k, km1, mx, my, mz, izs, ize
  real, dimension(mx,mz):: fp, f
!+---------------------------------------------------------------------
!
  if (mz.le.5) then
    fp = fp - f
    return
  end if
  a = .5
!
!$omp barrier
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    km1=mod(k-2+mz,mz)+1
      do i=1,mx
        fp(i,k) = fp(i,k) - ( &
                     a*(f(i,k  )+f(i,km1)))
      end do
  end do
!$omp barrier
!
END SUBROUTINE
!***********************************************************************
SUBROUTINE ddzupi_sub (f, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, km2, km1, kp1, kp2, kp3, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!+---------------------------------------------------------------------
!
  if (mz.le.5) then
    fp = fp - 0.
    return
  endif
!
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)
  a1st = 1.
!
!$omp barrier
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    km2=mod(k-3+mz,mz)+1
    km1=mod(k-2+mz,mz)+1
    kp1=mod(k     ,mz)+1
    kp2=mod(k+1   ,mz)+1
    kp3=mod(k+2   ,mz)+1
    do j=1,my
      do i=1,mx
        fp(i,j,k) = fp(i,j,k) - ( &
                     a*(f(i,j,kp1)-f(i,j,k  )) + &
                     b*(f(i,j,kp2)-f(i,j,km1)) + &
                     c*(f(i,j,kp3)-f(i,j,km2)))
      end do
    end do
  end do
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddzupi1_sub (f, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, kp1, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!+---------------------------------------------------------------------
!
  if (mz.le.5) then
    fp = fp - 0.
    return
  endif
!
  a = 1.
!
!$omp barrier
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    kp1=mod(k     ,mz)+1
    do j=1,my
      do i=1,mx
        fp(i,j,k) = fp(i,j,k) - ( &
                     a*(f(i,j,kp1)-f(i,j,k  )))
      end do
    end do
  end do
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddzdni_sub (f, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, km3, km2, km1, kp1, kp2, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!+---------------------------------------------------------------------
!
  if (mz.le.5) then
    fp = fp - 0.
    return
  endif
!
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)
  a1st = 1.
!
!$omp barrier
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    km3=mod(k-4+mz,mz)+1
    km2=mod(k-3+mz,mz)+1
    km1=mod(k-2+mz,mz)+1
    kp1=mod(k     ,mz)+1
    kp2=mod(k+1   ,mz)+1
    do j=1,my
      do i=1,mx
        fp(i,j,k) = fp(i,j,k) - ( &
                     a*(f(i,j,k  )-f(i,j,km1)) + &
                     b*(f(i,j,kp1)-f(i,j,km2)) + &
                     c*(f(i,j,kp2)-f(i,j,km3)))
      end do
    end do
  end do
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddzdni1_sub (f, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, km1, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!+---------------------------------------------------------------------
!
  if (mz.le.5) then
    fp = fp - 0.
    return
  endif
!
  a = 1.
!
!$omp barrier
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    km1=mod(k-2+mz,mz)+1
    do j=1,my
      do i=1,mx
        fp(i,j,k) = fp(i,j,k) - ( &
                     a*(f(i,j,k  )-f(i,j,km1)))
      end do
    end do
  end do
!
END SUBROUTINE
!***********************************************************************
SUBROUTINE ddzupi32_sub (f,j, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, km2, km1, kp1, kp2, kp3, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f
  real, dimension(mx,mz):: fp
!+---------------------------------------------------------------------
!
  if (mz.le.5) then
    fp = fp - 0.
    return
  endif
!
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)
  a1st = 1.
!
!$omp barrier
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    km2=mod(k-3+mz,mz)+1
    km1=mod(k-2+mz,mz)+1
    kp1=mod(k     ,mz)+1
    kp2=mod(k+1   ,mz)+1
    kp3=mod(k+2   ,mz)+1
      do i=1,mx
        fp(i,k) = fp(i,k) - ( &
                     a*(f(i,j,kp1)-f(i,j,k  )) + &
                     b*(f(i,j,kp2)-f(i,j,km1)) + &
                     c*(f(i,j,kp3)-f(i,j,km2)))
      end do
  end do
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddzupi132_sub (f,j, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, kp1, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f
  real, dimension(mx,mz):: fp
!+---------------------------------------------------------------------
!
  if (mz.le.5) then
    fp = fp - 0.
    return
  endif
!
  a = 1.
!
!$omp barrier
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    kp1=mod(k     ,mz)+1
      do i=1,mx
        fp(i,k) = fp(i,k) - ( &
                     a*(f(i,j,kp1)-f(i,j,k  )))
      end do
  end do
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddzdni32_sub (f,j, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, km3, km2, km1, kp1, kp2, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f
  real, dimension(mx,mz):: fp
!+---------------------------------------------------------------------
!
  if (mz.le.5) then
    fp = fp - 0.
    return
  endif
!
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)
  a1st = 1.
!
!$omp barrier
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    km3=mod(k-4+mz,mz)+1
    km2=mod(k-3+mz,mz)+1
    km1=mod(k-2+mz,mz)+1
    kp1=mod(k     ,mz)+1
    kp2=mod(k+1   ,mz)+1
      do i=1,mx
        fp(i,k) = fp(i,k) - ( &
                     a*(f(i,j,k  )-f(i,j,km1)) + &
                     b*(f(i,j,kp1)-f(i,j,km2)) + &
                     c*(f(i,j,kp2)-f(i,j,km3)))
      end do
  end do
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddzdni132_sub (f,j, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, km1, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f
  real, dimension(mx,mz):: fp
!+---------------------------------------------------------------------
!
  if (mz.le.5) then
    fp = fp - 0.
    return
  endif
!
  a = 1.
!
!$omp barrier
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    km1=mod(k-2+mz,mz)+1
      do i=1,mx
        fp(i,k) = fp(i,k) - ( &
                     a*(f(i,j,k  )-f(i,j,km1)))
      end do
  end do
!
END SUBROUTINE
!***********************************************************************
SUBROUTINE ddzupi22_sub (f, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, km2, km1, kp1, kp2, kp3, mx, my, mz, izs, ize
  real, dimension(mx,mz):: fp, f
!+---------------------------------------------------------------------
!
  if (mz.le.5) then
    fp = fp - 0.
    return
  endif
!
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)
  a1st = 1.
!
!$omp barrier
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    km2=mod(k-3+mz,mz)+1
    km1=mod(k-2+mz,mz)+1
    kp1=mod(k     ,mz)+1
    kp2=mod(k+1   ,mz)+1
    kp3=mod(k+2   ,mz)+1
      do i=1,mx
        fp(i,k) = fp(i,k) - ( &
                     a*(f(i,kp1)-f(i,k  )) + &
                     b*(f(i,kp2)-f(i,km1)) + &
                     c*(f(i,kp3)-f(i,km2)))
      end do
  end do
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddzupi122_sub (f, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, kp1, mx, my, mz, izs, ize
  real, dimension(mx,mz):: fp, f
!+---------------------------------------------------------------------
!
  if (mz.le.5) then
    fp = fp - 0.
    return
  endif
!
  a = 1.
!
!$omp barrier
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    kp1=mod(k     ,mz)+1
      do i=1,mx
        fp(i,k) = fp(i,k) - ( &
                     a*(f(i,kp1)-f(i,k  )))
      end do
  end do
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddzdni22_sub (f, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, km3, km2, km1, kp1, kp2, mx, my, mz, izs, ize
  real, dimension(mx,mz):: fp, f
!+---------------------------------------------------------------------
!
  if (mz.le.5) then
    fp = fp - 0.
    return
  endif
!
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)
  a1st = 1.
!
!$omp barrier
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    km3=mod(k-4+mz,mz)+1
    km2=mod(k-3+mz,mz)+1
    km1=mod(k-2+mz,mz)+1
    kp1=mod(k     ,mz)+1
    kp2=mod(k+1   ,mz)+1
      do i=1,mx
        fp(i,k) = fp(i,k) - ( &
                     a*(f(i,k  )-f(i,km1)) + &
                     b*(f(i,kp1)-f(i,km2)) + &
                     c*(f(i,kp2)-f(i,km3)))
      end do
  end do
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddzdni122_sub (f, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, km1, mx, my, mz, izs, ize
  real, dimension(mx,mz):: fp, f
!+---------------------------------------------------------------------
!
  if (mz.le.5) then
    fp = fp - 0.
    return
  endif
!
  a = 1.
!
!$omp barrier
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    km1=mod(k-2+mz,mz)+1
      do i=1,mx
        fp(i,k) = fp(i,k) - ( &
                     a*(f(i,k  )-f(i,km1)))
      end do
  end do
!
END SUBROUTINE
!***********************************************************************
SUBROUTINE ddzup_sub (f, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, km2, km1, kp1, kp2, kp3, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!+---------------------------------------------------------------------
!
  if (mz.le.5) then
    fp = fp - 0.
    return
  end if
!
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)
  a1st = 1.
!
!$omp barrier
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    km2=mod(k-3+mz,mz)+1
    km1=mod(k-2+mz,mz)+1
    kp1=mod(k     ,mz)+1
    kp2=mod(k+1   ,mz)+1
    kp3=mod(k+2   ,mz)+1
    do j=1,my
      do i=1,mx
        fp(i,j,k) = fp(i,j,k) - ( &
                     c*(f(i,j,kp3)-f(i,j,km2)) + &
                     b*(f(i,j,kp2)-f(i,j,km1)) + &
                     a*(f(i,j,kp1)-f(i,j,k  )) )
      end do
    end do
  end do
!$omp barrier
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddzup1_sub (f, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, kp1, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!+---------------------------------------------------------------------
!
  if (mz.le.5) then
    fp = fp - 0.
    return
  end if
!
  a = 1.
!
!$omp barrier
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    kp1=mod(k     ,mz)+1
    do j=1,my
      do i=1,mx
        fp(i,j,k) = fp(i,j,k) - ( &
                     a*(f(i,j,kp1)-f(i,j,k  )))
      end do
    end do
  end do
!$omp barrier
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddzdn_sub (f, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, km3, km2, km1, kp1, kp2, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!+---------------------------------------------------------------------
!
  if (mz.le.5) then
    fp = fp - 0.
    return
  end if
!
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)
  a1st = 1.
!
!$omp barrier
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    km3=mod(k-4+mz,mz)+1
    km2=mod(k-3+mz,mz)+1
    km1=mod(k-2+mz,mz)+1
    kp1=mod(k     ,mz)+1
    kp2=mod(k+1   ,mz)+1
    do j=1,my
      do i=1,mx
        fp(i,j,k) = fp(i,j,k) - ( &
                     c*(f(i,j,kp2)-f(i,j,km3)) + &
                     b*(f(i,j,kp1)-f(i,j,km2)) + &
                     a*(f(i,j,k  )-f(i,j,km1)) )
      end do
    end do
  end do
!$omp barrier
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddzdn1_sub (f, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, km1, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!+---------------------------------------------------------------------
!
  if (mz.le.5) then
    fp = fp - 0.
    return
  end if
!
  a = 1.
!
!$omp barrier
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    km1=mod(k-2+mz,mz)+1
    do j=1,my
      do i=1,mx
        fp(i,j,k) = fp(i,j,k) - ( &
                     a*(f(i,j,k  )-f(i,j,km1)))
      end do
    end do
  end do
!$omp barrier
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE laplace_neg (f, fp, mx, my, mz)
!
!  Laplace operator
!

  implicit none
  real cx, cy, cz
  real, dimension(mx,my,mz):: f, fp
  integer i, j, k, km1, kp1, mx, my, mz, izs, ize
!+---------------------------------------------------------------------

  cx=1.
  cy=1.
  cz=1.
  if (mx.gt.1) then
  call limits_omp(1,mz,izs,ize)
    do k=izs,ize
      do j=1,my
        do i=2,mx-1
          fp(i,j,k) = - cx*(f(i-1 ,j,k)+f(i+1,j,k)-2.*f(i,j,k))
        end do
        fp(1 ,j,k) = - cx*(f(2,j,k)+f(mx  ,j,k)-2.*f(1 ,j,k))
        fp(mx,j,k) = - cx*(f(1,j,k)+f(mx-1,j,k)-2.*f(mx,j,k))
      end do
    end do
  else
  call limits_omp(1,mz,izs,ize)
    do k=izs,ize
      do j=1,my
        do i=1,mx
          fp(i,j,k) = - 0.
        end do
      end do
    end do
  end if

 if (my .gt. 1) then
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    do j=2,my-1
      do i=1,mx
        fp(i,j,k) = - fp(i,j,k) + cy*(f(i,j+1,k)+f(i,j-1,k)-2.*f(i,j,k))
      end do
    end do
    do i=1,mx
        fp(i,1 ,k) = - fp(i,1 ,k) + cy*(f(i,2,k)+f(i,my  ,k)-2.*f(i,1 ,k))
        fp(i,my,k) = - fp(i,my,k) + cy*(f(i,1,k)+f(i,my-1,k)-2.*f(i,my,k))
    end do
  end do
 end if

 if (mz .gt. 1) then
!$omp barrier
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    km1=mod(k-2+mz,mz)+1
    kp1=mod(k     ,mz)+1
    do j=1,my
      do i=1,mx
        fp(i,j,k) = - fp(i,j,k) + cz*(f(i,j,kp1)+f(i,j,km1)-2.*f(i,j,k))
      end do
    end do
  end do
 end if
END SUBROUTINE

!***********************************************************************
SUBROUTINE dif2a_neg (f, fp, mx, my, mz)
!
!  Max abs difference one zone left to one zones right.
!

  implicit none
  real cx, cy, cz
  real, dimension(mx,my,mz):: f, fp
  integer i, j, k, km1, kp1, mx, my, mz, izs, ize
!+---------------------------------------------------------------------

  if (mx.gt.1) then
  call limits_omp(1,mz,izs,ize)
    do k=izs,ize
      do j=1,my
        do i=2,mx-1
          fp(i,j,k) = - abs(f(i-1 ,j,k)-f(i+1,j,k))
        end do
        fp(1 ,j,k) = - abs(f(mx  ,j,k)-f(2  ,j,k))
        fp(mx,j,k) = - abs(f(mx-1,j,k)-f(1  ,j,k))
      end do
    end do
  else
  call limits_omp(1,mz,izs,ize)
    do k=izs,ize
      do j=1,my
        do i=1,mx
          fp(i,j,k) = - 0.
        end do
      end do
    end do
  end if

 if (my .gt. 1) then
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    do j=2,my-1
      do i=1,mx
        fp(i,j, k) = - max(fp(i,j, k),abs(f(i,j -1,k)-f(i,j+1,k)))
      end do
    end do
    do i=1,mx
        fp(i,1 ,k) = - max(fp(i,1 ,k),abs(f(i,my  ,k)-f(i,  2,k)))
        fp(i,my,k) = - max(fp(i,my,k),abs(f(i,my-1,k)-f(i,  1,k)))
    end do
  end do
 end if

 if (mz .gt. 1) then
!$omp barrier
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    km1=mod(k-2+mz,mz)+1
    kp1=mod(k     ,mz)+1
    do j=1,my
      do i=1,mx
        fp(i,j,k ) = - max(fp(i,j,k ),abs(f(i,j,km1)-f(i,j,kp1)))
      end do
    end do
  end do
 end if
END SUBROUTINE

!***********************************************************************
SUBROUTINE dif2_neg (fx, fy, fz, fp, mx, my, mz)
!
!  Finite difference one zone left to one zones right.
!

  implicit none
  real cx(mx), cy(my), cz(mz)
  real, dimension(mx,my,mz):: fx, fy, fz, fp
  integer i, j, k, km1, kp1, mx, my, mz, izs, ize
!+---------------------------------------------------------------------

  cx=0.5
  cy=0.5
  cz=0.5

  if (mx.gt.1) then
  call limits_omp(1,mz,izs,ize)
    do k=izs,ize
      do j=1,my
        do i=2,mx-1
          fp(i,j,k) = - cx(i)*(fx(i-1 ,j,k)-fx(i+1,j,k))
        end do
        fp(1 ,j,k) = - cx(1 )*(fx(mx  ,j,k)-fx(2  ,j,k))
        fp(mx,j,k) = - cx(mx)*(fx(mx-1,j,k)-fx(1  ,j,k))
      end do
    end do
  else
  call limits_omp(1,mz,izs,ize)
    do k=izs,ize
      do j=1,my
        do i=1,mx
          fp(i,j,k) = - 0.
        end do
      end do
    end do
  end if

 if (my .gt. 1) then
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    do j=2,my-1
      do i=1,mx
        fp(i,j, k) = - fp(i,j, k) + cy(j)*(fy(i,j -1,k)-fy(i,j+1,k))
      end do
    end do
    do i=1,mx
        fp(i,1 ,k) = - fp(i,1 ,k) + cy(1 )*(fy(i,my  ,k)-fy(i,  2,k))
        fp(i,my,k) = - fp(i,my,k) + cy(my)*(fy(i,my-1,k)-fy(i,  1,k))
    end do
  end do
 end if

 if (mz .gt. 1) then
!$omp barrier
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    km1=mod(k-2+mz,mz)+1
    kp1=mod(k     ,mz)+1
    do j=1,my
      do i=1,mx
        fp(i,j,k ) = - fp(i,j,k ) + cz(k)*(fz(i,j,km1)-fz(i,j,kp1))
      end do
    end do
  end do
 end if

  call limits_omp(1,mz,izs,ize)
 do k=izs,ize
   do j=1,my
     do i=1,mx
       fp(i,j,k) = - max (0., fp(i,j,k))
     end do
   end do
 end do

END SUBROUTINE

!***********************************************************************
SUBROUTINE dif2d_neg (fx, fy, fz, fp, mx, my, mz)
!
!  Finite difference one zone left to one zones right.
!

  implicit none
  real, dimension(mx,my,mz):: fx, fy, fz, fp
  integer i, j, k, km1, kp1, mx, my, mz, izs, ize
!+---------------------------------------------------------------------

  if (mx.gt.1) then
  call limits_omp(1,mz,izs,ize)
    do k=izs,ize
      do j=1,my
        do i=2,mx-1
          fp(i,j,k) = - (fx(i-1 ,j,k)-fx(i+1,j,k))
        end do
        fp(1 ,j,k) = - (fx(mx  ,j,k)-fx(2  ,j,k))
        fp(mx,j,k) = - (fx(mx-1,j,k)-fx(1  ,j,k))
      end do
    end do
  else
  call limits_omp(1,mz,izs,ize)
    do k=izs,ize
      do j=1,my
        do i=1,mx
          fp(i,j,k) = - 0.
        end do
      end do
    end do
  end if

 if (my .gt. 1) then
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    do j=2,my-1
      do i=1,mx
        fp(i,j, k) = - fp(i,j, k) + (fy(i,j -1,k)-fy(i,j+1,k))
      end do
    end do
    do i=1,mx
        fp(i,1 ,k) = - fp(i,1 ,k) + (fy(i,my  ,k)-fy(i,  2,k))
        fp(i,my,k) = - fp(i,my,k) + (fy(i,my-1,k)-fy(i,  1,k))
    end do
  end do
 end if

 if (mz .gt. 1) then
!$omp barrier
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    km1=mod(k-2+mz,mz)+1
    kp1=mod(k     ,mz)+1
    do j=1,my
      do i=1,mx
        fp(i,j,k ) = - fp(i,j,k ) + (fz(i,j,km1)-fz(i,j,kp1))
      end do
    end do
  end do
 end if

  call limits_omp(1,mz,izs,ize)
 do k=izs,ize
   do j=1,my
     do i=1,mx
       fp(i,j,k) = - max (0., fp(i,j,k))
     end do
   end do
 end do

END SUBROUTINE

!***********************************************************************
SUBROUTINE dif1_neg (fx, fy, fz, fp, mx, my, mz)
!
!  Finite difference one zone left to one zones right.
!

  implicit none
  real, dimension(mx,my,mz):: fx, fy, fz, fp
  integer i, j, k, kp1, mx, my, mz, izs, ize
!+---------------------------------------------------------------------

  if (mx.gt.1) then
  call limits_omp(1,mz,izs,ize)
    do k=izs,ize
      do j=1,my
        do i=1,mx-1
          fp(i,j,k) = - (fx( i,j,k)-fx(i+1,j,k))
        end do
        fp(mx,j,k) = - (fx(mx,j,k)-fx(  1,j,k))
      end do
    end do
  else
  call limits_omp(1,mz,izs,ize)
    do k=izs,ize
      do j=1,my
        do i=1,mx
          fp(i,j,k) = - 0.
        end do
      end do
    end do
  end if

 if (my .gt. 1) then
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    do j=1,my-1
      do i=1,mx
        fp(i, j,k) = - fp(i, j,k) + (fy(i, j,k)-fy(i,j+1,k))
      end do
    end do
    do i=1,mx
        fp(i,my,k) = - fp(i,my,k) + (fy(i,my,k)-fy(i,  1,k))
    end do
  end do
 end if

 if (mz .gt. 1) then
!$omp barrier
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    kp1=mod(k,mz)+1
    do j=1,my
      do i=1,mx
        fp(i,j,k) = - fp(i,j,k) + (fz(i,j,k)-fz(i,j,kp1))
      end do
    end do
  end do
 end if

  call limits_omp(1,mz,izs,ize)
 do k=izs,ize
   do j=1,my
     do i=1,mx
       fp(i,j,k) = - max (0., fp(i,j,k))
     end do
   end do
 end do

END SUBROUTINE

!***********************************************************************
SUBROUTINE xup_neg (f, fp, mx, my, mz)
!
!  f is centered on (i-.5,j,k), fifth order stagger
!
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!+---------------------------------------------------------------------
!
!
  if (mx.le.5) then
    fp = - f
    return
  end if
  c = 3./256.
  b = -25./256.
  a = .5-b-c
  a1st = 0.5
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
   do j=1,my
    fp(mx-2,j,k) = - ( &
                   a*(f(mx-2,j,k)+f(mx-1,j,k)) &
                 + b*(f(mx-3,j,k)+f(mx  ,j,k)) &
                 + c*(f(mx-4,j,k)+f(1   ,j,k)))
    fp(mx-1,j,k) = - ( &
                   a*(f(mx-1,j,k)+f(mx  ,j,k)) &
                 + b*(f(mx-2,j,k)+f(1   ,j,k)) &
                 + c*(f(mx-3,j,k)+f(2   ,j,k)))
    fp(mx  ,j,k) = - ( &
                   a*(f(mx  ,j,k)+f(1   ,j,k)) &
                 + b*(f(mx-1,j,k)+f(2   ,j,k)) &
                 + c*(f(mx-2,j,k)+f(3   ,j,k)))
    fp(1   ,j,k) = - ( &
                   a*(f(1   ,j,k)+f(2   ,j,k)) &
                 + b*(f(mx  ,j,k)+f(3   ,j,k)) &
                 + c*(f(mx-1,j,k)+f(4   ,j,k)))
    fp(2   ,j,k) = - ( &
                   a*(f(2   ,j,k)+f(3   ,j,k)) &
                 + b*(f(1   ,j,k)+f(4   ,j,k)) &
                 + c*(f(mx  ,j,k)+f(5   ,j,k)))
   end do
   do j=1,my
    do i=3,mx-3
      fp(i  ,j,k) = - ( &
                    a*(f(i  ,j,k)+f(i+1 ,j,k)) &
                  + b*(f(i-1,j,k)+f(i+2 ,j,k)) &
                  + c*(f(i-2,j,k)+f(i+3 ,j,k)))
    end do
   end do
  end do
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE xup1_neg (f, fp, mx, my, mz)
!
!  f is centered on (i-.5,j,k), fifth order stagger
!
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!+---------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = - f
    return
  end if
!
  a = .5
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
   do j=1,my
    fp(mx  ,j,k) = - ( &
                   a*(f(mx  ,j,k)+f(1   ,j,k)))
   end do
   do j=1,my
    do i=1,mx-1
      fp(i  ,j,k) = - ( &
                    a*(f(i  ,j,k)+f(i+1 ,j,k)))
    end do
   end do
  end do
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE xdn_neg (f, fp, mx, my, mz)
!
!  f is centered on (i,j,k), fifth order stagger
!
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!+---------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = - f
    return
  end if
!
  c = 3./256.
  b = -25./256.
  a = .5-b-c
  a1st = 0.5
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
   do j=1,my
    fp(mx-1,j,k) = - ( &
                   a*(f(mx-2,j,k)+f(mx-1,j,k)) &
                 + b*(f(mx-3,j,k)+f(mx  ,j,k)) &
                 + c*(f(mx-4,j,k)+f(1   ,j,k)))
    fp(mx  ,j,k) = - ( &
                   a*(f(mx-1,j,k)+f(mx  ,j,k)) &
                 + b*(f(mx-2,j,k)+f(1   ,j,k)) &
                 + c*(f(mx-3,j,k)+f(2   ,j,k)))
    fp(1  ,j,k) = - ( &
                   a*(f(mx  ,j,k)+f(1   ,j,k)) &
                 + b*(f(mx-1,j,k)+f(2   ,j,k)) &
                 + c*(f(mx-2,j,k)+f(3   ,j,k)))
    fp(2  ,j,k) = - ( &
                   a*(f(1   ,j,k)+f(2   ,j,k)) &
                 + b*(f(mx  ,j,k)+f(3   ,j,k)) &
                 + c*(f(mx-1,j,k)+f(4   ,j,k)))
    fp(3  ,j,k) = - ( &
                   a*(f(2   ,j,k)+f(3   ,j,k)) &
                 + b*(f(1   ,j,k)+f(4   ,j,k)) &
                 + c*(f(mx  ,j,k)+f(5   ,j,k)))
   end do
   do j=1,my
    do i=4,mx-2
      fp(i,j,k) = - ( &
                   a*(f(i   ,j,k)+f(i-1 ,j,k)) &
                 + b*(f(i+1 ,j,k)+f(i-2 ,j,k)) &
                 + c*(f(i+2 ,j,k)+f(i-3 ,j,k)))
    end do
   end do
  end do
!
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE xdn1_neg (f, fp, mx, my, mz)
!
!  f is centered on (i,j,k), fifth order stagger
!
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!+---------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = - f
    return
  end if
!
  a = 0.5
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
   do j=1,my
    fp(1  ,j,k) = - ( &
                   a*(f(mx  ,j,k)+f(1   ,j,k)))
   end do
   do j=1,my
    do i=2,mx
      fp(i,j,k) = - ( &
                   a*(f(i   ,j,k)+f(i-1 ,j,k)))
    end do
   end do
  end do
!
END SUBROUTINE
!***********************************************************************
SUBROUTINE xup32_neg (f,j, fp, mx, my, mz)
!
!  f is centered on (i-.5,j,k), fifth order stagger
!
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f
  real, dimension(mx,mz):: fp
!+---------------------------------------------------------------------
!
!
  if (mx.le.5) then
    fp = - f(:,j,:)
    return
  end if
  c = 3./256.
  b = -25./256.
  a = .5-b-c
  a1st = 0.5
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    fp(mx-2,k) = - ( &
                   a*(f(mx-2,j,k)+f(mx-1,j,k)) &
                 + b*(f(mx-3,j,k)+f(mx  ,j,k)) &
                 + c*(f(mx-4,j,k)+f(1   ,j,k)))
    fp(mx-1,k) = - ( &
                   a*(f(mx-1,j,k)+f(mx  ,j,k)) &
                 + b*(f(mx-2,j,k)+f(1   ,j,k)) &
                 + c*(f(mx-3,j,k)+f(2   ,j,k)))
    fp(mx  ,k) = - ( &
                   a*(f(mx  ,j,k)+f(1   ,j,k)) &
                 + b*(f(mx-1,j,k)+f(2   ,j,k)) &
                 + c*(f(mx-2,j,k)+f(3   ,j,k)))
    fp(1   ,k) = - ( &
                   a*(f(1   ,j,k)+f(2   ,j,k)) &
                 + b*(f(mx  ,j,k)+f(3   ,j,k)) &
                 + c*(f(mx-1,j,k)+f(4   ,j,k)))
    fp(2   ,k) = - ( &
                   a*(f(2   ,j,k)+f(3   ,j,k)) &
                 + b*(f(1   ,j,k)+f(4   ,j,k)) &
                 + c*(f(mx  ,j,k)+f(5   ,j,k)))
    do i=3,mx-3
      fp(i  ,k) = - ( &
                    a*(f(i  ,j,k)+f(i+1 ,j,k)) &
                  + b*(f(i-1,j,k)+f(i+2 ,j,k)) &
                  + c*(f(i-2,j,k)+f(i+3 ,j,k)))
    end do
  end do
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE xup132_neg (f,j, fp, mx, my, mz)
!
!  f is centered on (i-.5,j,k), fifth order stagger
!
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f
  real, dimension(mx,mz):: fp
!+---------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = - f(:,j,:)
    return
  end if
!
  a = .5
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    fp(mx  ,k) = - ( &
                   a*(f(mx  ,j,k)+f(1   ,j,k)))
    do i=1,mx-1
      fp(i  ,k) = - ( &
                    a*(f(i  ,j,k)+f(i+1 ,j,k)))
    end do
  end do
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE xdn32_neg (f,j, fp, mx, my, mz)
!
!  f is centered on (i,j,k), fifth order stagger
!
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f
  real, dimension(mx,mz):: fp
!+---------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = - f(:,j,:)
    return
  end if
!
  c = 3./256.
  b = -25./256.
  a = .5-b-c
  a1st = 0.5
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    fp(mx-1,k) = - ( &
                   a*(f(mx-2,j,k)+f(mx-1,j,k)) &
                 + b*(f(mx-3,j,k)+f(mx  ,j,k)) &
                 + c*(f(mx-4,j,k)+f(1   ,j,k)))
    fp(mx  ,k) = - ( &
                   a*(f(mx-1,j,k)+f(mx  ,j,k)) &
                 + b*(f(mx-2,j,k)+f(1   ,j,k)) &
                 + c*(f(mx-3,j,k)+f(2   ,j,k)))
    fp(1  ,k) = - ( &
                   a*(f(mx  ,j,k)+f(1   ,j,k)) &
                 + b*(f(mx-1,j,k)+f(2   ,j,k)) &
                 + c*(f(mx-2,j,k)+f(3   ,j,k)))
    fp(2  ,k) = - ( &
                   a*(f(1   ,j,k)+f(2   ,j,k)) &
                 + b*(f(mx  ,j,k)+f(3   ,j,k)) &
                 + c*(f(mx-1,j,k)+f(4   ,j,k)))
    fp(3  ,k) = - ( &
                   a*(f(2   ,j,k)+f(3   ,j,k)) &
                 + b*(f(1   ,j,k)+f(4   ,j,k)) &
                 + c*(f(mx  ,j,k)+f(5   ,j,k)))
    do i=4,mx-2
      fp(i,k) = - ( &
                   a*(f(i   ,j,k)+f(i-1 ,j,k)) &
                 + b*(f(i+1 ,j,k)+f(i-2 ,j,k)) &
                 + c*(f(i+2 ,j,k)+f(i-3 ,j,k)))
    end do
  end do
!
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE xdn132_neg (f,j, fp, mx, my, mz)
!
!  f is centered on (i,j,k), fifth order stagger
!
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f
  real, dimension(mx,mz):: fp
!+---------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = - f(:,j,:)
    return
  end if
!
  a = 0.5
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    fp(1  ,k) = - ( &
                   a*(f(mx  ,j,k)+f(1   ,j,k)))
    do i=2,mx
      fp(i,k) = - ( &
                   a*(f(i   ,j,k)+f(i-1 ,j,k)))
    end do
  end do
!
END SUBROUTINE
!***********************************************************************
SUBROUTINE xup22_neg (f, fp, mx, my, mz)
!
!  f is centered on (i-.5,k), fifth order stagger
!
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,mz):: fp, f
!+---------------------------------------------------------------------
!
!
  if (mx.le.5) then
    fp = - f
    return
  end if
  c = 3./256.
  b = -25./256.
  a = .5-b-c
  a1st = 0.5
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    fp(mx-2,k) = - ( &
                   a*(f(mx-2,k)+f(mx-1,k)) &
                 + b*(f(mx-3,k)+f(mx  ,k)) &
                 + c*(f(mx-4,k)+f(1   ,k)))
    fp(mx-1,k) = - ( &
                   a*(f(mx-1,k)+f(mx  ,k)) &
                 + b*(f(mx-2,k)+f(1   ,k)) &
                 + c*(f(mx-3,k)+f(2   ,k)))
    fp(mx  ,k) = - ( &
                   a*(f(mx  ,k)+f(1   ,k)) &
                 + b*(f(mx-1,k)+f(2   ,k)) &
                 + c*(f(mx-2,k)+f(3   ,k)))
    fp(1   ,k) = - ( &
                   a*(f(1   ,k)+f(2   ,k)) &
                 + b*(f(mx  ,k)+f(3   ,k)) &
                 + c*(f(mx-1,k)+f(4   ,k)))
    fp(2   ,k) = - ( &
                   a*(f(2   ,k)+f(3   ,k)) &
                 + b*(f(1   ,k)+f(4   ,k)) &
                 + c*(f(mx  ,k)+f(5   ,k)))
    do i=3,mx-3
      fp(i  ,k) = - ( &
                    a*(f(i  ,k)+f(i+1 ,k)) &
                  + b*(f(i-1,k)+f(i+2 ,k)) &
                  + c*(f(i-2,k)+f(i+3 ,k)))
    end do
  end do
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE xup122_neg (f, fp, mx, my, mz)
!
!  f is centered on (i-.5,k), fifth order stagger
!
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,mz):: fp, f
!+---------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = - f
    return
  end if
!
  a = .5
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    fp(mx  ,k) = - ( &
                   a*(f(mx  ,k)+f(1   ,k)))
    do i=1,mx-1
      fp(i  ,k) = - ( &
                    a*(f(i  ,k)+f(i+1 ,k)))
    end do
  end do
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE xdn22_neg (f, fp, mx, my, mz)
!
!  f is centered on (i,k), fifth order stagger
!
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,mz):: fp, f
!+---------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = - f
    return
  end if
!
  c = 3./256.
  b = -25./256.
  a = .5-b-c
  a1st = 0.5
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    fp(mx-1,k) = - ( &
                   a*(f(mx-2,k)+f(mx-1,k)) &
                 + b*(f(mx-3,k)+f(mx  ,k)) &
                 + c*(f(mx-4,k)+f(1   ,k)))
    fp(mx  ,k) = - ( &
                   a*(f(mx-1,k)+f(mx  ,k)) &
                 + b*(f(mx-2,k)+f(1   ,k)) &
                 + c*(f(mx-3,k)+f(2   ,k)))
    fp(1  ,k) = - ( &
                   a*(f(mx  ,k)+f(1   ,k)) &
                 + b*(f(mx-1,k)+f(2   ,k)) &
                 + c*(f(mx-2,k)+f(3   ,k)))
    fp(2  ,k) = - ( &
                   a*(f(1   ,k)+f(2   ,k)) &
                 + b*(f(mx  ,k)+f(3   ,k)) &
                 + c*(f(mx-1,k)+f(4   ,k)))
    fp(3  ,k) = - ( &
                   a*(f(2   ,k)+f(3   ,k)) &
                 + b*(f(1   ,k)+f(4   ,k)) &
                 + c*(f(mx  ,k)+f(5   ,k)))
    do i=4,mx-2
      fp(i,k) = - ( &
                   a*(f(i   ,k)+f(i-1 ,k)) &
                 + b*(f(i+1 ,k)+f(i-2 ,k)) &
                 + c*(f(i+2 ,k)+f(i-3 ,k)))
    end do
  end do
!
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE xdn122_neg (f, fp, mx, my, mz)
!
!  f is centered on (i,k), fifth order stagger
!
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,mz):: fp, f
!+---------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = - f
    return
  end if
!
  a = 0.5
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    fp(1  ,k) = - ( &
                   a*(f(mx  ,k)+f(1   ,k)))
    do i=2,mx
      fp(i,k) = - ( &
                   a*(f(i   ,k)+f(i-1 ,k)))
    end do
  end do
!
END SUBROUTINE
!***********************************************************************
SUBROUTINE ddxupi_neg (f, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!+---------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = - 0.
    return
  end if
!
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)
  a1st = 1.
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
   do j=1,my
    fp(mx-2,j,k) = - ( &
                   a*(f(mx-1,j,k)-f(mx-2,j,k)) &
                 + b*(f(mx  ,j,k)-f(mx-3,j,k)) &
                 + c*(f(1   ,j,k)-f(mx-4,j,k)))
    fp(mx-1,j,k) = - ( &
                   a*(f(mx  ,j,k)-f(mx-1,j,k)) &
                 + b*(f(1   ,j,k)-f(mx-2,j,k)) &
                 + c*(f(2   ,j,k)-f(mx-3,j,k)))
    fp(mx  ,j,k) = - ( &
                   a*(f(1   ,j,k)-f(mx  ,j,k)) &
                 + b*(f(2   ,j,k)-f(mx-1,j,k)) &
                 + c*(f(3   ,j,k)-f(mx-2,j,k)))
    fp(1  ,j,k) = - ( &
                   a*(f(2   ,j,k)-f(1   ,j,k)) &
                 + b*(f(3   ,j,k)-f(mx  ,j,k)) &
                 + c*(f(4   ,j,k)-f(mx-1,j,k)))
    fp(2  ,j,k) = - ( &
                   a*(f(3   ,j,k)-f(2  ,j,k)) &
                 + b*(f(4   ,j,k)-f(1  ,j,k)) &
                 + c*(f(5   ,j,k)-f(mx ,j,k)))
   end do
   do j=1,my
    do i=3,mx-3
      fp(i  ,j,k) = - ( &
                   a*(f(i+1 ,j,k)-f(i   ,j,k)) &
                 + b*(f(i+2 ,j,k)-f(i-1 ,j,k)) &
                 + c*(f(i+3 ,j,k)-f(i-2 ,j,k)))
    end do
   end do
  end do
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddxupi1_neg (f, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!+---------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = - 0.
    return
  end if
!
  a = 1.
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
   do j=1,my
    fp(mx  ,j,k) = - ( &
                   a*(f(1   ,j,k)-f(mx  ,j,k)))
   end do
   do j=1,my
    do i=1,mx-1
      fp(i  ,j,k) = - ( &
                   a*(f(i+1 ,j,k)-f(i   ,j,k)))
    end do
   end do
  end do
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddxdni_neg (f, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!+---------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = - 0.
    return
  end if
!
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)
  a1st = 1.
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
   do j=1,my
    fp(mx-1,j,k) = - ( &
                   a*(f(mx-1,j,k)-f(mx-2,j,k)) &
                 + b*(f(mx  ,j,k)-f(mx-3,j,k)) &
                 + c*(f(1   ,j,k)-f(mx-4,j,k)))
    fp(mx  ,j,k) = - ( &
                   a*(f(mx  ,j,k)-f(mx-1,j,k)) &
                 + b*(f(1   ,j,k)-f(mx-2,j,k)) &
                 + c*(f(2   ,j,k)-f(mx-3,j,k)))
    fp(1   ,j,k) = - ( &
                   a*(f(1   ,j,k)-f(mx  ,j,k)) &
                 + b*(f(2   ,j,k)-f(mx-1,j,k)) &
                 + c*(f(3   ,j,k)-f(mx-2,j,k)))
    fp(2  ,j,k) = - ( &
                   a*(f(2   ,j,k)-f(1   ,j,k)) &
                 + b*(f(3   ,j,k)-f(mx  ,j,k)) &
                 + c*(f(4   ,j,k)-f(mx-1,j,k)))
    fp(3  ,j,k) = - ( &
                   a*(f(3   ,j,k)-f(2   ,j,k)) &
                 + b*(f(4   ,j,k)-f(1   ,j,k)) &
                 + c*(f(5   ,j,k)-f(mx  ,j,k)))
   end do
   do j=1,my
    do i=4,mx-2
      fp(i,j,k) = - ( &
                   a*(f(i   ,j,k)-f(i-1 ,j,k)) &
                 + b*(f(i+1 ,j,k)-f(i-2 ,j,k)) &
                 + c*(f(i+2 ,j,k)-f(i-3 ,j,k)))
    end do
   end do
  end do
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddxdni1_neg (f, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!+---------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = - 0.
    return
  end if
!
  a = 1.
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
   do j=1,my
    fp(1   ,j,k) = - ( &
                   a*(f(1   ,j,k)-f(mx  ,j,k)))
   end do
   do j=1,my
    do i=2,mx
      fp(i,j,k) = - ( &
                   a*(f(i   ,j,k)-f(i-1 ,j,k)))
    end do
   end do
  end do
!
END SUBROUTINE
!***********************************************************************
SUBROUTINE ddxupi32_neg (f,j, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f
  real, dimension(mx,mz):: fp
!+---------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = - 0.
    return
  end if
!
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)
  a1st = 1.
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    fp(mx-2,k) = - ( &
                   a*(f(mx-1,j,k)-f(mx-2,j,k)) &
                 + b*(f(mx  ,j,k)-f(mx-3,j,k)) &
                 + c*(f(1   ,j,k)-f(mx-4,j,k)))
    fp(mx-1,k) = - ( &
                   a*(f(mx  ,j,k)-f(mx-1,j,k)) &
                 + b*(f(1   ,j,k)-f(mx-2,j,k)) &
                 + c*(f(2   ,j,k)-f(mx-3,j,k)))
    fp(mx  ,k) = - ( &
                   a*(f(1   ,j,k)-f(mx  ,j,k)) &
                 + b*(f(2   ,j,k)-f(mx-1,j,k)) &
                 + c*(f(3   ,j,k)-f(mx-2,j,k)))
    fp(1  ,k) = - ( &
                   a*(f(2   ,j,k)-f(1   ,j,k)) &
                 + b*(f(3   ,j,k)-f(mx  ,j,k)) &
                 + c*(f(4   ,j,k)-f(mx-1,j,k)))
    fp(2  ,k) = - ( &
                   a*(f(3   ,j,k)-f(2  ,j,k)) &
                 + b*(f(4   ,j,k)-f(1  ,j,k)) &
                 + c*(f(5   ,j,k)-f(mx ,j,k)))
    do i=3,mx-3
      fp(i  ,k) = - ( &
                   a*(f(i+1 ,j,k)-f(i   ,j,k)) &
                 + b*(f(i+2 ,j,k)-f(i-1 ,j,k)) &
                 + c*(f(i+3 ,j,k)-f(i-2 ,j,k)))
    end do
  end do
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddxupi132_neg (f,j, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f
  real, dimension(mx,mz):: fp
!+---------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = - 0.
    return
  end if
!
  a = 1.
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    fp(mx  ,k) = - ( &
                   a*(f(1   ,j,k)-f(mx  ,j,k)))
    do i=1,mx-1
      fp(i  ,k) = - ( &
                   a*(f(i+1 ,j,k)-f(i   ,j,k)))
    end do
  end do
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddxdni32_neg (f,j, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f
  real, dimension(mx,mz):: fp
!+---------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = - 0.
    return
  end if
!
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)
  a1st = 1.
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    fp(mx-1,k) = - ( &
                   a*(f(mx-1,j,k)-f(mx-2,j,k)) &
                 + b*(f(mx  ,j,k)-f(mx-3,j,k)) &
                 + c*(f(1   ,j,k)-f(mx-4,j,k)))
    fp(mx  ,k) = - ( &
                   a*(f(mx  ,j,k)-f(mx-1,j,k)) &
                 + b*(f(1   ,j,k)-f(mx-2,j,k)) &
                 + c*(f(2   ,j,k)-f(mx-3,j,k)))
    fp(1   ,k) = - ( &
                   a*(f(1   ,j,k)-f(mx  ,j,k)) &
                 + b*(f(2   ,j,k)-f(mx-1,j,k)) &
                 + c*(f(3   ,j,k)-f(mx-2,j,k)))
    fp(2  ,k) = - ( &
                   a*(f(2   ,j,k)-f(1   ,j,k)) &
                 + b*(f(3   ,j,k)-f(mx  ,j,k)) &
                 + c*(f(4   ,j,k)-f(mx-1,j,k)))
    fp(3  ,k) = - ( &
                   a*(f(3   ,j,k)-f(2   ,j,k)) &
                 + b*(f(4   ,j,k)-f(1   ,j,k)) &
                 + c*(f(5   ,j,k)-f(mx  ,j,k)))
    do i=4,mx-2
      fp(i,k) = - ( &
                   a*(f(i   ,j,k)-f(i-1 ,j,k)) &
                 + b*(f(i+1 ,j,k)-f(i-2 ,j,k)) &
                 + c*(f(i+2 ,j,k)-f(i-3 ,j,k)))
    end do
  end do
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddxdni132_neg (f,j, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f
  real, dimension(mx,mz):: fp
!+---------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = - 0.
    return
  end if
!
  a = 1.
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    fp(1   ,k) = - ( &
                   a*(f(1   ,j,k)-f(mx  ,j,k)))
    do i=2,mx
      fp(i,k) = - ( &
                   a*(f(i   ,j,k)-f(i-1 ,j,k)))
    end do
  end do
!
END SUBROUTINE
!***********************************************************************
SUBROUTINE ddxupi22_neg (f, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,mz):: fp, f
!+---------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = - 0.
    return
  end if
!
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)
  a1st = 1.
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    fp(mx-2,k) = - ( &
                   a*(f(mx-1,k)-f(mx-2,k)) &
                 + b*(f(mx  ,k)-f(mx-3,k)) &
                 + c*(f(1   ,k)-f(mx-4,k)))
    fp(mx-1,k) = - ( &
                   a*(f(mx  ,k)-f(mx-1,k)) &
                 + b*(f(1   ,k)-f(mx-2,k)) &
                 + c*(f(2   ,k)-f(mx-3,k)))
    fp(mx  ,k) = - ( &
                   a*(f(1   ,k)-f(mx  ,k)) &
                 + b*(f(2   ,k)-f(mx-1,k)) &
                 + c*(f(3   ,k)-f(mx-2,k)))
    fp(1  ,k) = - ( &
                   a*(f(2   ,k)-f(1   ,k)) &
                 + b*(f(3   ,k)-f(mx  ,k)) &
                 + c*(f(4   ,k)-f(mx-1,k)))
    fp(2  ,k) = - ( &
                   a*(f(3   ,k)-f(2  ,k)) &
                 + b*(f(4   ,k)-f(1  ,k)) &
                 + c*(f(5   ,k)-f(mx ,k)))
    do i=3,mx-3
      fp(i  ,k) = - ( &
                   a*(f(i+1 ,k)-f(i   ,k)) &
                 + b*(f(i+2 ,k)-f(i-1 ,k)) &
                 + c*(f(i+3 ,k)-f(i-2 ,k)))
    end do
  end do
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddxupi122_neg (f, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,mz):: fp, f
!+---------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = - 0.
    return
  end if
!
  a = 1.
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    fp(mx  ,k) = - ( &
                   a*(f(1   ,k)-f(mx  ,k)))
    do i=1,mx-1
      fp(i  ,k) = - ( &
                   a*(f(i+1 ,k)-f(i   ,k)))
    end do
  end do
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddxdni22_neg (f, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,mz):: fp, f
!+---------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = - 0.
    return
  end if
!
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)
  a1st = 1.
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    fp(mx-1,k) = - ( &
                   a*(f(mx-1,k)-f(mx-2,k)) &
                 + b*(f(mx  ,k)-f(mx-3,k)) &
                 + c*(f(1   ,k)-f(mx-4,k)))
    fp(mx  ,k) = - ( &
                   a*(f(mx  ,k)-f(mx-1,k)) &
                 + b*(f(1   ,k)-f(mx-2,k)) &
                 + c*(f(2   ,k)-f(mx-3,k)))
    fp(1   ,k) = - ( &
                   a*(f(1   ,k)-f(mx  ,k)) &
                 + b*(f(2   ,k)-f(mx-1,k)) &
                 + c*(f(3   ,k)-f(mx-2,k)))
    fp(2  ,k) = - ( &
                   a*(f(2   ,k)-f(1   ,k)) &
                 + b*(f(3   ,k)-f(mx  ,k)) &
                 + c*(f(4   ,k)-f(mx-1,k)))
    fp(3  ,k) = - ( &
                   a*(f(3   ,k)-f(2   ,k)) &
                 + b*(f(4   ,k)-f(1   ,k)) &
                 + c*(f(5   ,k)-f(mx  ,k)))
    do i=4,mx-2
      fp(i,k) = - ( &
                   a*(f(i   ,k)-f(i-1 ,k)) &
                 + b*(f(i+1 ,k)-f(i-2 ,k)) &
                 + c*(f(i+2 ,k)-f(i-3 ,k)))
    end do
  end do
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddxdni122_neg (f, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,mz):: fp, f
!+---------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = - 0.
    return
  end if
!
  a = 1.
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    fp(1   ,k) = - ( &
                   a*(f(1   ,k)-f(mx  ,k)))
    do i=2,mx
      fp(i,k) = - ( &
                   a*(f(i   ,k)-f(i-1 ,k)))
    end do
  end do
!
END SUBROUTINE
!***********************************************************************
SUBROUTINE ddxup_neg (f, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!+---------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = - 0.
    return
  end if
!
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)
  a1st = 1.
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
   do j=1,my
    fp(mx-2,j,k) = - ( &
                 + c*(f(1   ,j,k)-f(mx-4,j,k)) &
                 + b*(f(mx  ,j,k)-f(mx-3,j,k)) &
                 + a*(f(mx-1,j,k)-f(mx-2,j,k)))
    fp(mx-1,j,k) = - ( &
                 + c*(f(2   ,j,k)-f(mx-3,j,k)) &
                 + b*(f(1   ,j,k)-f(mx-2,j,k)) &
                 + a*(f(mx  ,j,k)-f(mx-1,j,k)))
    fp(mx  ,j,k) = - ( &
                 + c*(f(3   ,j,k)-f(mx-2,j,k)) &
                 + b*(f(2   ,j,k)-f(mx-1,j,k)) &
                 + a*(f(1   ,j,k)-f(mx  ,j,k)))
    fp(1  ,j,k) = - ( &
                 + c*(f(4   ,j,k)-f(mx-1,j,k)) &
                 + b*(f(3   ,j,k)-f(mx  ,j,k)) &
                 + a*(f(2   ,j,k)-f(1   ,j,k)))
    fp(2  ,j,k) = - ( &
                 + c*(f(5   ,j,k)-f(mx  ,j,k)) &
                 + b*(f(4   ,j,k)-f(1   ,j,k)) &
                 + a*(f(3   ,j,k)-f(2   ,j,k)))
   end do
   do j=1,my
    do i=3,mx-3
      fp(i  ,j,k) = - ( &
                 + c*(f(i+3 ,j,k)-f(i-2 ,j,k)) &
                 + b*(f(i+2 ,j,k)-f(i-1 ,j,k)) &
                 + a*(f(i+1 ,j,k)-f(i   ,j,k)))
    end do
  end do
  end do
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddxup1_neg (f, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!+---------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = - 0.
    return
  end if
!
  a = 1.
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
   do j=1,my
    fp(mx  ,j,k) = - ( &
                   a*(f(1   ,j,k)-f(mx  ,j,k)))
   end do
   do j=1,my
    do i=1,mx-1
      fp(i  ,j,k) = - ( &
                   a*(f(i+1 ,j,k)-f(i   ,j,k)))
    end do
  end do
  end do
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddxdn_neg (f, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!+---------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = - 0.
    return
  end if
!
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)
  a1st = 1.
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
   do j=1,my
    fp(mx-1,j,k) = - ( &
                 + c*(f(1   ,j,k)-f(mx-4,j,k)) &
                 + b*(f(mx  ,j,k)-f(mx-3,j,k)) &
                 + a*(f(mx-1,j,k)-f(mx-2,j,k)))
    fp(mx  ,j,k) = - ( &
                 + c*(f(2   ,j,k)-f(mx-3,j,k)) &
                 + b*(f(1   ,j,k)-f(mx-2,j,k)) &
                 + a*(f(mx  ,j,k)-f(mx-1,j,k)))
    fp(1   ,j,k) = - ( &
                 + c*(f(3   ,j,k)-f(mx-2,j,k)) &
                 + b*(f(2   ,j,k)-f(mx-1,j,k)) &
                 + a*(f(1   ,j,k)-f(mx  ,j,k)))
    fp(2  ,j,k) = - ( &
                 + c*(f(4   ,j,k)-f(mx-1,j,k)) &
                 + b*(f(3   ,j,k)-f(mx  ,j,k)) &
                 + a*(f(2   ,j,k)-f(1   ,j,k)))
    fp(3  ,j,k) = - ( &
                 + c*(f(5   ,j,k)-f(mx  ,j,k)) &
                 + b*(f(4   ,j,k)-f(1   ,j,k)) &
                 + a*(f(3   ,j,k)-f(2   ,j,k)))
   end do
   do j=1,my
    do i=4,mx-2
      fp(i,j,k) = - ( &
                 + c*(f(i+2 ,j,k)-f(i-3 ,j,k)) &
                 + b*(f(i+1 ,j,k)-f(i-2 ,j,k)) &
                 + a*(f(i   ,j,k)-f(i-1 ,j,k)))
    end do
   end do
  end do
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddxdn1_neg (f, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!+---------------------------------------------------------------------
!
  if (mx.le.5) then
    fp = - 0.
    return
  end if
!
  a = 1.
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
   do j=1,my
    fp(1   ,j,k) = - ( &
                   a*(f(1   ,j,k)-f(mx  ,j,k)))
   end do
   do j=1,my
    do i=2,mx
      fp(i,j,k) = - ( &
                   a*(f(i   ,j,k)-f(i-1 ,j,k)))
    end do
   end do
  end do
!
END SUBROUTINE
!***********************************************************************
SUBROUTINE yup_neg (f, fp, mx, my, mz)
!
!  f is centered on (i,j-.5,k), fifth order stagger
!
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, intent(in) , dimension(mx,my,mz):: f
  real,              dimension(mx,my,mz):: fp
!+---------------------------------------------------------------------
!

  if (my .lt. 5) then
  call limits_omp(1,mz,izs,ize)
    do k=izs,ize
      fp(:,:,k) = - f(:,:,k)
    end do
    return
  end if
  c = 3./256.
  b = -25./256.
  a = .5-b-c
  a1st = 0.5
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    do i=1,mx
      fp(i,my-2,k) = - ( &
                     a*(f(i,my-2,k)+f(i,my-1,k)) + &
                     b*(f(i,my-3,k)+f(i,my  ,k)) + &
                     c*(f(i,my-4,k)+f(i,1   ,k)))
      fp(i,my-1,k) = - ( &
                     a*(f(i,my-1,k)+f(i,my  ,k)) + &
                     b*(f(i,my-2,k)+f(i,1   ,k)) + &
                     c*(f(i,my-3,k)+f(i,2   ,k)))
      fp(i,my  ,k) = - ( &
                     a*(f(i,my  ,k)+f(i,1   ,k)) + &
                     b*(f(i,my-1,k)+f(i,2   ,k)) + &
                     c*(f(i,my-2,k)+f(i,3   ,k)))
      fp(i,1   ,k) = - ( &
                     a*(f(i,1   ,k)+f(i,2   ,k)) + &
                     b*(f(i,my  ,k)+f(i,3   ,k)) + &
                     c*(f(i,my-1,k)+f(i,4   ,k)))
      fp(i,2   ,k) = - ( &
                     a*(f(i,2   ,k)+f(i,3   ,k)) + &
                     b*(f(i,1   ,k)+f(i,4   ,k)) + &
                     c*(f(i,my  ,k)+f(i,5   ,k)))
    end do
    do j=3,my-3
     do i=1,mx
      fp(i,j  ,k) = - ( &
                     a*(f(i,j   ,k)+f(i,j+1 ,k)) + &
                     b*(f(i,j-1 ,k)+f(i,j+2 ,k)) + &
                     c*(f(i,j-2 ,k)+f(i,j+3 ,k)))
     end do
    end do
  end do
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE yup1_neg (f, fp, mx, my, mz)
!
!  f is centered on (i,j-.5,k), fifth order stagger
!
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, intent(in) , dimension(mx,my,mz):: f
  real,              dimension(mx,my,mz):: fp
!+---------------------------------------------------------------------
!
  if (my .lt. 5) then
  call limits_omp(1,mz,izs,ize)
    do k=izs,ize
      fp(:,:,k) = - f(:,:,k)
    end do
    return
  end if
!
  a = 0.5
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    do i=1,mx
      fp(i,my,k) = - ( &
                     a*(f(i,my,k)+f(i,1,k)))
    end do
    do j=1,my-1
      do i=1,mx
        fp(i,j  ,k) = - ( &
                     a*(f(i,j   ,k)+f(i,j+1 ,k)))
      end do
    end do
  end do
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE ydn_neg (f, fp, mx, my, mz)
!
!  f is centered on (i,j,k), fifth order stagger
!
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!
!+---------------------------------------------------------------------
  if (my .lt. 5) then
  call limits_omp(1,mz,izs,ize)
    do k=izs,ize
      fp(:,:,k) = - f(:,:,k)
    end do
    return
  end if
  c = 3./256.
  b = -25./256.
  a = .5-b-c
  a1st = 0.5
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    do i=1,mx
      fp(i,my-1,k) = - ( &
                     a*(f(i,my-2,k)+f(i,my-1,k)) + &
                     b*(f(i,my-3,k)+f(i,my  ,k)) + &
                     c*(f(i,my-4,k)+f(i,1   ,k)))
      fp(i,my  ,k) = - ( &
                     a*(f(i,my-1,k)+f(i,my  ,k)) + &
                     b*(f(i,my-2,k)+f(i,1   ,k)) + &
                     c*(f(i,my-3,k)+f(i,2   ,k)))
      fp(i,1   ,k) = - ( &
                     a*(f(i,my  ,k)+f(i,1   ,k)) + &
                     b*(f(i,my-1,k)+f(i,2   ,k)) + &
                     c*(f(i,my-2,k)+f(i,3   ,k)))
      fp(i,2   ,k) = - ( &
                     a*(f(i,1   ,k)+f(i,2   ,k)) + &
                     b*(f(i,my  ,k)+f(i,3   ,k)) + &
                     c*(f(i,my-1,k)+f(i,4   ,k)))
      fp(i,3   ,k) = - ( &
                     a*(f(i,2   ,k)+f(i,3   ,k)) + &
                     b*(f(i,1   ,k)+f(i,4   ,k)) + &
                     c*(f(i,my  ,k)+f(i,5   ,k)))
    end do
    do j=4,my-2
     do i=1,mx
      fp(i,j,k) = - ( &
                     a*(f(i,j   ,k)+f(i,j-1 ,k)) + &
                     b*(f(i,j+1 ,k)+f(i,j-2 ,k)) + &
                     c*(f(i,j+2 ,k)+f(i,j-3 ,k)))
     end do
    end do
  end do
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE ydn1_neg (f, fp, mx, my, mz)
!
!  f is centered on (i,j,k), fifth order stagger
!
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!
!+---------------------------------------------------------------------
  if (my .lt. 5) then
  call limits_omp(1,mz,izs,ize)
    do k=izs,ize
      fp(:,:,k) = - f(:,:,k)
    end do
    return
  end if
!
  a = 0.5
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    do i=1,mx
      fp(i,1   ,k) = - ( &
                     a*(f(i,my  ,k)+f(i,1   ,k)))
    end do
    do j=2,my
     do i=1,mx
      fp(i,j,k) = - ( &
                     a*(f(i,j   ,k)+f(i,j-1 ,k)))
     end do
    end do
  end do
!
END SUBROUTINE
!***********************************************************************
SUBROUTINE ddyupi_neg (f, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!+---------------------------------------------------------------------
!
  if (my .lt. 3) then
    fp = - 0.
    return
  end if
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)
  a1st = 1.
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    do i=1,mx
      fp(i,my-2,k) = - ( &
                     a*(f(i,my-1,k)-f(i,my-2,k)) + &
                     b*(f(i,my  ,k)-f(i,my-3,k)) + &
                     c*(f(i,1   ,k)-f(i,my-4,k)))
      fp(i,my-1,k) = - ( &
                     a*(f(i,my  ,k)-f(i,my-1,k)) + &
                     b*(f(i,1   ,k)-f(i,my-2,k)) + &
                     c*(f(i,2   ,k)-f(i,my-3,k)))
      fp(i,my  ,k) = - ( &
                     a*(f(i,1   ,k)-f(i,my  ,k)) + &
                     b*(f(i,2   ,k)-f(i,my-1,k)) + &
                     c*(f(i,3   ,k)-f(i,my-2,k)))
      fp(i,1   ,k) = - ( &
                     a*(f(i,2   ,k)-f(i,1   ,k)) + &
                     b*(f(i,3   ,k)-f(i,my  ,k)) + &
                     c*(f(i,4   ,k)-f(i,my-1,k)))
      fp(i,2   ,k) = - ( &
                     a*(f(i,3   ,k)-f(i,2   ,k)) + &
                     b*(f(i,4   ,k)-f(i,1   ,k)) + &
                     c*(f(i,5   ,k)-f(i,my  ,k)))
    end do
    do j=3,my-3
     do i=1,mx
      fp(i,j  ,k) = - ( &
                     a*(f(i,j+1 ,k)-f(i,j   ,k)) + &
                     b*(f(i,j+2 ,k)-f(i,j-1 ,k)) + &
                     c*(f(i,j+3 ,k)-f(i,j-2 ,k)))
     end do
    end do
  end do
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddyupi1_neg (f, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!+---------------------------------------------------------------------
!
  if (my .lt. 3) then
    fp = - 0.
    return
  end if
  a = 1.
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    do i=1,mx
      fp(i,my,k) = - ( &
                     a*(f(i,1  ,k)-f(i,my,k)))
    end do
    do j=1,my-1
      do i=1,mx
        fp(i,j  ,k) = - ( &
                     a*(f(i,j+1 ,k)-f(i,j   ,k)))
     end do
    end do
  end do
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddydni_neg (f, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!+---------------------------------------------------------------------
!
  if (my .lt. 3) then
    fp = - 0.
    return
  end if
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)
  a1st = 1.
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    do i=1,mx
      fp(i,my-1,k) = - ( &
                     a*(f(i,my-1,k)-f(i,my-2,k)) + &
                     b*(f(i,my  ,k)-f(i,my-3,k)) + &
                     c*(f(i,1   ,k)-f(i,my-4,k)))
      fp(i,my  ,k) = - ( &
                     a*(f(i,my  ,k)-f(i,my-1,k)) + &
                     b*(f(i,1   ,k)-f(i,my-2,k)) + &
                     c*(f(i,2   ,k)-f(i,my-3,k)))
      fp(i,1  ,k) = - ( &
                     a*(f(i,1   ,k)-f(i,my  ,k)) + &
                     b*(f(i,2   ,k)-f(i,my-1,k)) + &
                     c*(f(i,3   ,k)-f(i,my-2,k)))
      fp(i,2   ,k) = - ( &
                     a*(f(i,2   ,k)-f(i,1   ,k)) + &
                     b*(f(i,3   ,k)-f(i,my  ,k)) + &
                     c*(f(i,4   ,k)-f(i,my-1,k)))
      fp(i,3   ,k) = - ( &
                     a*(f(i,3   ,k)-f(i,2   ,k)) + &
                     b*(f(i,4   ,k)-f(i,1   ,k)) + &
                     c*(f(i,5   ,k)-f(i,my  ,k)))
    end do
    do j=4,my-2
     do i=1,mx
      fp(i,j ,k) = - ( &
                     a*(f(i,j   ,k)-f(i,j-1 ,k)) + &
                     b*(f(i,j+1 ,k)-f(i,j-2 ,k)) + &
                     c*(f(i,j+2 ,k)-f(i,j-3 ,k)))
     end do
    end do
  end do
!
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddydni1_neg (f, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!+---------------------------------------------------------------------
!
  if (my .lt. 3) then
    fp = - 0.
    return
  end if
  a = 1.
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    do i=1,mx
      fp(i,1  ,k) = - ( &
                     a*(f(i,1   ,k)-f(i,my  ,k)))
    end do
    do j=2,my
     do i=1,mx
      fp(i,j ,k) = - ( &
                     a*(f(i,j   ,k)-f(i,j-1 ,k)))
     end do
    end do
  end do
!
END SUBROUTINE
!***********************************************************************
SUBROUTINE ddyup_neg (f, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!+---------------------------------------------------------------------
!
  if (my .lt. 3) then
    fp = - 0.
    return
  end if
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)
  a1st = 1.
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    do i=1,mx
      fp(i,my-2,k) = - ( &
                     c*(f(i,1   ,k)-f(i,my-4,k)) + &
                     b*(f(i,my  ,k)-f(i,my-3,k)) + &
                     a*(f(i,my-1,k)-f(i,my-2,k)) )
      fp(i,my-1,k) = - ( &
                     c*(f(i,2   ,k)-f(i,my-3,k)) + &
                     b*(f(i,1   ,k)-f(i,my-2,k)) + &
                     a*(f(i,my  ,k)-f(i,my-1,k)) )
      fp(i,my  ,k) = - ( &
                     c*(f(i,3   ,k)-f(i,my-2,k)) + &
                     b*(f(i,2   ,k)-f(i,my-1,k)) + &
                     a*(f(i,1   ,k)-f(i,my  ,k)) )
      fp(i,1   ,k) = - ( &
                     c*(f(i,4   ,k)-f(i,my-1,k)) + &
                     b*(f(i,3   ,k)-f(i,my  ,k)) + &
                     a*(f(i,2   ,k)-f(i,1   ,k)) )
      fp(i,2   ,k) = - ( &
                     c*(f(i,5   ,k)-f(i,my  ,k)) + &
                     b*(f(i,4   ,k)-f(i,1   ,k)) + &
                     a*(f(i,3   ,k)-f(i,2   ,k)) )
    end do
    do j=3,my-3
     do i=1,mx
      fp(i,j  ,k) = - ( &
                     c*(f(i,j+3 ,k)-f(i,j-2 ,k)) + &
                     b*(f(i,j+2 ,k)-f(i,j-1 ,k)) + &
                     a*(f(i,j+1 ,k)-f(i,j   ,k)) )
     end do
    end do
  end do
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddyup1_neg (f, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!+---------------------------------------------------------------------
!
  if (my .lt. 3) then
    fp = - 0.
    return
  end if
  a = 1.
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    do i=1,mx
      fp(i,my,k) = - ( &
                     a*(f(i,1  ,k)-f(i,my,k)))
    end do
    do j=1,my-1
      do i=1,mx
        fp(i,j  ,k) = - ( &
                     a*(f(i,j+1 ,k)-f(i,j   ,k)))
     end do
    end do
  end do
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddydn_neg (f, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!+---------------------------------------------------------------------
!
  if (my .lt. 3) then
    fp = - 0.
    return
  end if
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)
  a1st = 1.
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    do i=1,mx
      fp(i,my-1,k) = - ( &
                     c*(f(i,1   ,k)-f(i,my-4,k)) + &
                     b*(f(i,my  ,k)-f(i,my-3,k)) + &
                     a*(f(i,my-1,k)-f(i,my-2,k)) ) 
      fp(i,my  ,k) = - ( &
                     c*(f(i,2   ,k)-f(i,my-3,k)) + &
                     b*(f(i,1   ,k)-f(i,my-2,k)) + &
                     a*(f(i,my  ,k)-f(i,my-1,k)) )
      fp(i,1  ,k) = - ( &
                     c*(f(i,3   ,k)-f(i,my-2,k)) + &
                     b*(f(i,2   ,k)-f(i,my-1,k)) + &
                     a*(f(i,1   ,k)-f(i,my  ,k)) )
      fp(i,2   ,k) = - ( &
                     c*(f(i,4   ,k)-f(i,my-1,k)) + &
                     b*(f(i,3   ,k)-f(i,my  ,k)) + &
                     a*(f(i,2   ,k)-f(i,1   ,k)) )
      fp(i,3   ,k) = - ( &
                     c*(f(i,5   ,k)-f(i,my  ,k)) + &
                     b*(f(i,4   ,k)-f(i,1   ,k)) + &
                     a*(f(i,3   ,k)-f(i,2   ,k)) )
    end do
    do j=4,my-2
     do i=1,mx
      fp(i,j ,k) = - ( &
                     c*(f(i,j+2 ,k)-f(i,j-3 ,k)) + &
                     b*(f(i,j+1 ,k)-f(i,j-2 ,k)) + &
                     a*(f(i,j   ,k)-f(i,j-1 ,k)) )
     end do
    end do
  end do
!
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddydn1_neg (f, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!+---------------------------------------------------------------------
!
  if (my .lt. 3) then
    fp = - 0.
    return
  end if
  a = 1.
!
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    do i=1,mx
      fp(i,1  ,k) = - ( &
                     a*(f(i,1   ,k)-f(i,my  ,k)))
    end do
    do j=2,my
     do i=1,mx
      fp(i,j ,k) = - ( &
                     a*(f(i,j   ,k)-f(i,j-1 ,k)))
     end do
    end do
  end do
!
END SUBROUTINE
!***********************************************************************
SUBROUTINE zup_neg (f, fp, mx, my, mz)
!
!  f is centered on (i,j,k-.5), fifth order stagger
!
  implicit none
  real a, b, c, a1st
  integer i, j, k, km2, km1, kp1, kp2, kp3, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!+---------------------------------------------------------------------
!
!
  if (mz.le.5) then
    fp = - f
    return
  end if
  c = 3./256.
  b = -25./256.
  a = .5-b-c
  a1st = 0.5
!
!$omp barrier
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    km2=mod(k-3+mz,mz)+1
    km1=mod(k-2+mz,mz)+1
    kp1=mod(k     ,mz)+1
    kp2=mod(k+1   ,mz)+1
    kp3=mod(k+2   ,mz)+1
    do j=1,my
      do i=1,mx
        fp(i,j,k) = - ( &
                     a*(f(i,j,kp1)+f(i,j,k  )) + &
                     b*(f(i,j,kp2)+f(i,j,km1)) + &
                     c*(f(i,j,kp3)+f(i,j,km2)))
      end do
    end do
  end do
!$omp barrier
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE zup1_neg (f, fp, mx, my, mz)
!
!  f is centered on (i,j,k-.5), fifth order stagger
!
  implicit none
  real a, b, c, a1st
  integer i, j, k, kp1, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!+---------------------------------------------------------------------
!
  if (mz.le.5) then
    fp = - f
    return
  end if
  a = .5
!
!$omp barrier
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    kp1=mod(k     ,mz)+1
    do j=1,my
      do i=1,mx
        fp(i,j,k) = - ( &
                     a*(f(i,j,kp1)+f(i,j,k  )))
      end do
    end do
  end do
!$omp barrier
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE zdn_neg (f, fp, mx, my, mz)
!
!  f is centered on (i,j,k), fifth order stagger
!
  implicit none
  real a, b, c, a1st
  integer i, j, k, km3, km2, km1, kp1, kp2, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!+---------------------------------------------------------------------
!
  if (mz.le.5) then
    fp = - f
    return
  end if
  c = 3./256.
  b = -25./256.
  a = .5-b-c
  a1st = 0.5
!
!$omp barrier
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    km3=mod(k-4+mz,mz)+1
    km2=mod(k-3+mz,mz)+1
    km1=mod(k-2+mz,mz)+1
    kp1=mod(k     ,mz)+1
    kp2=mod(k+1   ,mz)+1
    do j=1,my
      do i=1,mx
        fp(i,j,k) = - ( &
                     a*(f(i,j,k  )+f(i,j,km1)) + &
                     b*(f(i,j,kp1)+f(i,j,km2)) + &
                     c*(f(i,j,kp2)+f(i,j,km3)))
      end do
    end do
  end do
!$omp barrier
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE zdn1_neg (f, fp, mx, my, mz)
!
!  f is centered on (i,j,k), fifth order stagger
!
  implicit none
  real a, b, c, a1st
  integer i, j, k, km1, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!+---------------------------------------------------------------------
!
  if (mz.le.5) then
    fp = - f
    return
  end if
  a = .5
!
!$omp barrier
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    km1=mod(k-2+mz,mz)+1
    do j=1,my
      do i=1,mx
        fp(i,j,k) = - ( &
                     a*(f(i,j,k  )+f(i,j,km1)))
      end do
    end do
  end do
!$omp barrier
!
END SUBROUTINE
!***********************************************************************
SUBROUTINE zup32_neg (f,j, fp, mx, my, mz)
!
!  f is centered on (i,j,k-.5), fifth order stagger
!
  implicit none
  real a, b, c, a1st
  integer i, j, k, km2, km1, kp1, kp2, kp3, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f
  real, dimension(mx,mz):: fp
!+---------------------------------------------------------------------
!
!
  if (mz.le.5) then
    fp = - f(:,j,:)
    return
  end if
  c = 3./256.
  b = -25./256.
  a = .5-b-c
  a1st = 0.5
!
!$omp barrier
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    km2=mod(k-3+mz,mz)+1
    km1=mod(k-2+mz,mz)+1
    kp1=mod(k     ,mz)+1
    kp2=mod(k+1   ,mz)+1
    kp3=mod(k+2   ,mz)+1
      do i=1,mx
        fp(i,k) = - ( &
                     a*(f(i,j,kp1)+f(i,j,k  )) + &
                     b*(f(i,j,kp2)+f(i,j,km1)) + &
                     c*(f(i,j,kp3)+f(i,j,km2)))
      end do
  end do
!$omp barrier
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE zup132_neg (f,j, fp, mx, my, mz)
!
!  f is centered on (i,j,k-.5), fifth order stagger
!
  implicit none
  real a, b, c, a1st
  integer i, j, k, kp1, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f
  real, dimension(mx,mz):: fp
!+---------------------------------------------------------------------
!
  if (mz.le.5) then
    fp = - f(:,j,:)
    return
  end if
  a = .5
!
!$omp barrier
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    kp1=mod(k     ,mz)+1
      do i=1,mx
        fp(i,k) = - ( &
                     a*(f(i,j,kp1)+f(i,j,k  )))
      end do
  end do
!$omp barrier
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE zdn32_neg (f,j, fp, mx, my, mz)
!
!  f is centered on (i,j,k), fifth order stagger
!
  implicit none
  real a, b, c, a1st
  integer i, j, k, km3, km2, km1, kp1, kp2, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f
  real, dimension(mx,mz):: fp
!+---------------------------------------------------------------------
!
  if (mz.le.5) then
    fp = - f(:,j,:)
    return
  end if
  c = 3./256.
  b = -25./256.
  a = .5-b-c
  a1st = 0.5
!
!$omp barrier
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    km3=mod(k-4+mz,mz)+1
    km2=mod(k-3+mz,mz)+1
    km1=mod(k-2+mz,mz)+1
    kp1=mod(k     ,mz)+1
    kp2=mod(k+1   ,mz)+1
      do i=1,mx
        fp(i,k) = - ( &
                     a*(f(i,j,k  )+f(i,j,km1)) + &
                     b*(f(i,j,kp1)+f(i,j,km2)) + &
                     c*(f(i,j,kp2)+f(i,j,km3)))
      end do
  end do
!$omp barrier
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE zdn132_neg (f,j, fp, mx, my, mz)
!
!  f is centered on (i,j,k), fifth order stagger
!
  implicit none
  real a, b, c, a1st
  integer i, j, k, km1, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f
  real, dimension(mx,mz):: fp
!+---------------------------------------------------------------------
!
  if (mz.le.5) then
    fp = - f(:,j,:)
    return
  end if
  a = .5
!
!$omp barrier
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    km1=mod(k-2+mz,mz)+1
      do i=1,mx
        fp(i,k) = - ( &
                     a*(f(i,j,k  )+f(i,j,km1)))
      end do
  end do
!$omp barrier
!
END SUBROUTINE
!***********************************************************************
SUBROUTINE zup22_neg (f, fp, mx, my, mz)
!
!  f is centered on (i,k-.5), fifth order stagger
!
  implicit none
  real a, b, c, a1st
  integer i, j, k, km2, km1, kp1, kp2, kp3, mx, my, mz, izs, ize
  real, dimension(mx,mz):: fp, f
!+---------------------------------------------------------------------
!
!
  if (mz.le.5) then
    fp = - f
    return
  end if
  c = 3./256.
  b = -25./256.
  a = .5-b-c
  a1st = 0.5
!
!$omp barrier
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    km2=mod(k-3+mz,mz)+1
    km1=mod(k-2+mz,mz)+1
    kp1=mod(k     ,mz)+1
    kp2=mod(k+1   ,mz)+1
    kp3=mod(k+2   ,mz)+1
      do i=1,mx
        fp(i,k) = - ( &
                     a*(f(i,kp1)+f(i,k  )) + &
                     b*(f(i,kp2)+f(i,km1)) + &
                     c*(f(i,kp3)+f(i,km2)))
      end do
  end do
!$omp barrier
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE zup122_neg (f, fp, mx, my, mz)
!
!  f is centered on (i,k-.5), fifth order stagger
!
  implicit none
  real a, b, c, a1st
  integer i, j, k, kp1, mx, my, mz, izs, ize
  real, dimension(mx,mz):: fp, f
!+---------------------------------------------------------------------
!
  if (mz.le.5) then
    fp = - f
    return
  end if
  a = .5
!
!$omp barrier
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    kp1=mod(k     ,mz)+1
      do i=1,mx
        fp(i,k) = - ( &
                     a*(f(i,kp1)+f(i,k  )))
      end do
  end do
!$omp barrier
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE zdn22_neg (f, fp, mx, my, mz)
!
!  f is centered on (i,k), fifth order stagger
!
  implicit none
  real a, b, c, a1st
  integer i, j, k, km3, km2, km1, kp1, kp2, mx, my, mz, izs, ize
  real, dimension(mx,mz):: fp, f
!+---------------------------------------------------------------------
!
  if (mz.le.5) then
    fp = - f
    return
  end if
  c = 3./256.
  b = -25./256.
  a = .5-b-c
  a1st = 0.5
!
!$omp barrier
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    km3=mod(k-4+mz,mz)+1
    km2=mod(k-3+mz,mz)+1
    km1=mod(k-2+mz,mz)+1
    kp1=mod(k     ,mz)+1
    kp2=mod(k+1   ,mz)+1
      do i=1,mx
        fp(i,k) = - ( &
                     a*(f(i,k  )+f(i,km1)) + &
                     b*(f(i,kp1)+f(i,km2)) + &
                     c*(f(i,kp2)+f(i,km3)))
      end do
  end do
!$omp barrier
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE zdn122_neg (f, fp, mx, my, mz)
!
!  f is centered on (i,k), fifth order stagger
!
  implicit none
  real a, b, c, a1st
  integer i, j, k, km1, mx, my, mz, izs, ize
  real, dimension(mx,mz):: fp, f
!+---------------------------------------------------------------------
!
  if (mz.le.5) then
    fp = - f
    return
  end if
  a = .5
!
!$omp barrier
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    km1=mod(k-2+mz,mz)+1
      do i=1,mx
        fp(i,k) = - ( &
                     a*(f(i,k  )+f(i,km1)))
      end do
  end do
!$omp barrier
!
END SUBROUTINE
!***********************************************************************
SUBROUTINE ddzupi_neg (f, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, km2, km1, kp1, kp2, kp3, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!+---------------------------------------------------------------------
!
  if (mz.le.5) then
    fp = - 0.
    return
  endif
!
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)
  a1st = 1.
!
!$omp barrier
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    km2=mod(k-3+mz,mz)+1
    km1=mod(k-2+mz,mz)+1
    kp1=mod(k     ,mz)+1
    kp2=mod(k+1   ,mz)+1
    kp3=mod(k+2   ,mz)+1
    do j=1,my
      do i=1,mx
        fp(i,j,k) = - ( &
                     a*(f(i,j,kp1)-f(i,j,k  )) + &
                     b*(f(i,j,kp2)-f(i,j,km1)) + &
                     c*(f(i,j,kp3)-f(i,j,km2)))
      end do
    end do
  end do
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddzupi1_neg (f, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, kp1, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!+---------------------------------------------------------------------
!
  if (mz.le.5) then
    fp = - 0.
    return
  endif
!
  a = 1.
!
!$omp barrier
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    kp1=mod(k     ,mz)+1
    do j=1,my
      do i=1,mx
        fp(i,j,k) = - ( &
                     a*(f(i,j,kp1)-f(i,j,k  )))
      end do
    end do
  end do
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddzdni_neg (f, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, km3, km2, km1, kp1, kp2, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!+---------------------------------------------------------------------
!
  if (mz.le.5) then
    fp = - 0.
    return
  endif
!
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)
  a1st = 1.
!
!$omp barrier
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    km3=mod(k-4+mz,mz)+1
    km2=mod(k-3+mz,mz)+1
    km1=mod(k-2+mz,mz)+1
    kp1=mod(k     ,mz)+1
    kp2=mod(k+1   ,mz)+1
    do j=1,my
      do i=1,mx
        fp(i,j,k) = - ( &
                     a*(f(i,j,k  )-f(i,j,km1)) + &
                     b*(f(i,j,kp1)-f(i,j,km2)) + &
                     c*(f(i,j,kp2)-f(i,j,km3)))
      end do
    end do
  end do
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddzdni1_neg (f, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, km1, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!+---------------------------------------------------------------------
!
  if (mz.le.5) then
    fp = - 0.
    return
  endif
!
  a = 1.
!
!$omp barrier
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    km1=mod(k-2+mz,mz)+1
    do j=1,my
      do i=1,mx
        fp(i,j,k) = - ( &
                     a*(f(i,j,k  )-f(i,j,km1)))
      end do
    end do
  end do
!
END SUBROUTINE
!***********************************************************************
SUBROUTINE ddzupi32_neg (f,j, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, km2, km1, kp1, kp2, kp3, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f
  real, dimension(mx,mz):: fp
!+---------------------------------------------------------------------
!
  if (mz.le.5) then
    fp = - 0.
    return
  endif
!
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)
  a1st = 1.
!
!$omp barrier
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    km2=mod(k-3+mz,mz)+1
    km1=mod(k-2+mz,mz)+1
    kp1=mod(k     ,mz)+1
    kp2=mod(k+1   ,mz)+1
    kp3=mod(k+2   ,mz)+1
      do i=1,mx
        fp(i,k) = - ( &
                     a*(f(i,j,kp1)-f(i,j,k  )) + &
                     b*(f(i,j,kp2)-f(i,j,km1)) + &
                     c*(f(i,j,kp3)-f(i,j,km2)))
      end do
  end do
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddzupi132_neg (f,j, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, kp1, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f
  real, dimension(mx,mz):: fp
!+---------------------------------------------------------------------
!
  if (mz.le.5) then
    fp = - 0.
    return
  endif
!
  a = 1.
!
!$omp barrier
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    kp1=mod(k     ,mz)+1
      do i=1,mx
        fp(i,k) = - ( &
                     a*(f(i,j,kp1)-f(i,j,k  )))
      end do
  end do
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddzdni32_neg (f,j, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, km3, km2, km1, kp1, kp2, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f
  real, dimension(mx,mz):: fp
!+---------------------------------------------------------------------
!
  if (mz.le.5) then
    fp = - 0.
    return
  endif
!
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)
  a1st = 1.
!
!$omp barrier
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    km3=mod(k-4+mz,mz)+1
    km2=mod(k-3+mz,mz)+1
    km1=mod(k-2+mz,mz)+1
    kp1=mod(k     ,mz)+1
    kp2=mod(k+1   ,mz)+1
      do i=1,mx
        fp(i,k) = - ( &
                     a*(f(i,j,k  )-f(i,j,km1)) + &
                     b*(f(i,j,kp1)-f(i,j,km2)) + &
                     c*(f(i,j,kp2)-f(i,j,km3)))
      end do
  end do
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddzdni132_neg (f,j, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, km1, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f
  real, dimension(mx,mz):: fp
!+---------------------------------------------------------------------
!
  if (mz.le.5) then
    fp = - 0.
    return
  endif
!
  a = 1.
!
!$omp barrier
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    km1=mod(k-2+mz,mz)+1
      do i=1,mx
        fp(i,k) = - ( &
                     a*(f(i,j,k  )-f(i,j,km1)))
      end do
  end do
!
END SUBROUTINE
!***********************************************************************
SUBROUTINE ddzupi22_neg (f, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, km2, km1, kp1, kp2, kp3, mx, my, mz, izs, ize
  real, dimension(mx,mz):: fp, f
!+---------------------------------------------------------------------
!
  if (mz.le.5) then
    fp = - 0.
    return
  endif
!
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)
  a1st = 1.
!
!$omp barrier
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    km2=mod(k-3+mz,mz)+1
    km1=mod(k-2+mz,mz)+1
    kp1=mod(k     ,mz)+1
    kp2=mod(k+1   ,mz)+1
    kp3=mod(k+2   ,mz)+1
      do i=1,mx
        fp(i,k) = - ( &
                     a*(f(i,kp1)-f(i,k  )) + &
                     b*(f(i,kp2)-f(i,km1)) + &
                     c*(f(i,kp3)-f(i,km2)))
      end do
  end do
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddzupi122_neg (f, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, kp1, mx, my, mz, izs, ize
  real, dimension(mx,mz):: fp, f
!+---------------------------------------------------------------------
!
  if (mz.le.5) then
    fp = - 0.
    return
  endif
!
  a = 1.
!
!$omp barrier
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    kp1=mod(k     ,mz)+1
      do i=1,mx
        fp(i,k) = - ( &
                     a*(f(i,kp1)-f(i,k  )))
      end do
  end do
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddzdni22_neg (f, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, km3, km2, km1, kp1, kp2, mx, my, mz, izs, ize
  real, dimension(mx,mz):: fp, f
!+---------------------------------------------------------------------
!
  if (mz.le.5) then
    fp = - 0.
    return
  endif
!
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)
  a1st = 1.
!
!$omp barrier
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    km3=mod(k-4+mz,mz)+1
    km2=mod(k-3+mz,mz)+1
    km1=mod(k-2+mz,mz)+1
    kp1=mod(k     ,mz)+1
    kp2=mod(k+1   ,mz)+1
      do i=1,mx
        fp(i,k) = - ( &
                     a*(f(i,k  )-f(i,km1)) + &
                     b*(f(i,kp1)-f(i,km2)) + &
                     c*(f(i,kp2)-f(i,km3)))
      end do
  end do
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddzdni122_neg (f, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, km1, mx, my, mz, izs, ize
  real, dimension(mx,mz):: fp, f
!+---------------------------------------------------------------------
!
  if (mz.le.5) then
    fp = - 0.
    return
  endif
!
  a = 1.
!
!$omp barrier
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    km1=mod(k-2+mz,mz)+1
      do i=1,mx
        fp(i,k) = - ( &
                     a*(f(i,k  )-f(i,km1)))
      end do
  end do
!
END SUBROUTINE
!***********************************************************************
SUBROUTINE ddzup_neg (f, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, km2, km1, kp1, kp2, kp3, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!+---------------------------------------------------------------------
!
  if (mz.le.5) then
    fp = - 0.
    return
  end if
!
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)
  a1st = 1.
!
!$omp barrier
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    km2=mod(k-3+mz,mz)+1
    km1=mod(k-2+mz,mz)+1
    kp1=mod(k     ,mz)+1
    kp2=mod(k+1   ,mz)+1
    kp3=mod(k+2   ,mz)+1
    do j=1,my
      do i=1,mx
        fp(i,j,k) = - ( &
                     c*(f(i,j,kp3)-f(i,j,km2)) + &
                     b*(f(i,j,kp2)-f(i,j,km1)) + &
                     a*(f(i,j,kp1)-f(i,j,k  )) )
      end do
    end do
  end do
!$omp barrier
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddzup1_neg (f, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, kp1, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!+---------------------------------------------------------------------
!
  if (mz.le.5) then
    fp = - 0.
    return
  end if
!
  a = 1.
!
!$omp barrier
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    kp1=mod(k     ,mz)+1
    do j=1,my
      do i=1,mx
        fp(i,j,k) = - ( &
                     a*(f(i,j,kp1)-f(i,j,k  )))
      end do
    end do
  end do
!$omp barrier
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddzdn_neg (f, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, km3, km2, km1, kp1, kp2, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!+---------------------------------------------------------------------
!
  if (mz.le.5) then
    fp = - 0.
    return
  end if
!
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)
  a1st = 1.
!
!$omp barrier
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    km3=mod(k-4+mz,mz)+1
    km2=mod(k-3+mz,mz)+1
    km1=mod(k-2+mz,mz)+1
    kp1=mod(k     ,mz)+1
    kp2=mod(k+1   ,mz)+1
    do j=1,my
      do i=1,mx
        fp(i,j,k) = - ( &
                     c*(f(i,j,kp2)-f(i,j,km3)) + &
                     b*(f(i,j,kp1)-f(i,j,km2)) + &
                     a*(f(i,j,k  )-f(i,j,km1)) )
      end do
    end do
  end do
!$omp barrier
!
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddzdn1_neg (f, fp, mx, my, mz)
  implicit none
  real a, b, c, a1st
  integer i, j, k, km1, mx, my, mz, izs, ize
  real, dimension(mx,my,mz):: f, fp
!+---------------------------------------------------------------------
!
  if (mz.le.5) then
    fp = - 0.
    return
  end if
!
  a = 1.
!
!$omp barrier
  call limits_omp(1,mz,izs,ize)
  do k=izs,ize
    km1=mod(k-2+mz,mz)+1
    do j=1,my
      do i=1,mx
        fp(i,j,k) = - ( &
                     a*(f(i,j,k  )-f(i,j,km1)))
      end do
    end do
  end do
!$omp barrier
!
END SUBROUTINE
