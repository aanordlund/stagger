!$Id: main_divb_clean.f90,v 1.19 2016/11/09 22:04:57 aake Exp $
!*******************************************************************************
PROGRAM divb_clean

  implicit none
  real, parameter:: pi=3.14159265
  real, allocatable, dimension(:,:,:):: bx,by,bz,divb,phi,dphit,k2
  integer mvar,isnap,mx,my,mz,izs,ize
  integer iter,niter,ibx,ix,iy,iz,io_word
  real divmin,divmax,eps,wt,wallclock,c,relax,mintmp,maxtmp
  character(len=1) answer
  character(len=40) file
  namelist /in/file,mx,my,mz,mvar,ibx,isnap,niter,eps,relax
  character(len=64), save:: id="$Id: main_divb_clean.f90,v 1.19 2016/11/09 22:04:57 aake Exp $"

  print *,id

  file='snapshot.dat'                                                           ! file name
  mx=1024                                                                       ! dimension
  my=0                                                                          ! default=mx
  mz=0                                                                          ! default=mx
  mvar=7                                                                        ! fields per snapshot
  ibx=4                                                                         ! Bx index in file
  isnap=0                                                                       ! snapshot number

  niter=10                                                                      ! max iterations
  eps=1e-3                                                                      ! accuracy request
  relax=3.0                                                                     ! overrelaxation

  write (*,in)
  read (*,in)
  if (my==0) my=mx
  if (mz==0) mz=mx
  write (*,in)

  allocate (bx(mx,my,mz), by(mx,my,mz), bz(mx,my,mz))
  allocate (divb(mx,my,mz), dphit(mx,my,mz))
  allocate (k2(mx,my,mz))

  open (1,file=file,access='direct',status='unknown',recl=io_word()*mx*my)
  wt=wallclock()
  print *,'reading bx'
  do iz=1,mz
    read (1,rec=(mvar*isnap+ibx+0)*mz+iz) bx(:,:,iz)
  end do
  print *,'reading by'
  do iz=1,mz
    read (1,rec=(mvar*isnap+ibx+1)*mz+iz) by(:,:,iz)
  end do
  print *,'reading bz'
  do iz=1,mz
    read (1,rec=(mvar*isnap+ibx+2)*mz+iz) bz(:,:,iz)
  end do
  close (1)
  print *,wallclock(),' sec'

  c=alog(relax)/(3.*pi**2)
  print *,'cleaning: c =',c
  iter=0
  do while (iter <= niter)
    iter=iter+1
    !$omp parallel
    call ddxup_set (bx, divb, mx, my, mz)                                       ! omp-parallel
    call ddyup_add (by, divb, mx, my, mz)                                       ! omp-parallel
    call ddzup_add (bz, divb, mx, my, mz)                                       ! built-in barrier
    !$omp end parallel

    divmin=0.
    divmax=0.
    !$omp parallel private(izs,ize,mintmp,maxtmp)                               ! reduction() claus fails on ifort
    call limits_omp(1,mz,izs,ize)                                               ! iz,izs,ize must be private
    mintmp=minval(divb(:,:,izs:ize))                                            ! pizza box minval
    maxtmp=maxval(divb(:,:,izs:ize))                                            ! pizza box maxval
    !$omp critical
    divmin=min(divmin,mintmp)                                                   ! explicit critical section
    divmax=max(divmax,maxtmp)
    !$omp end critical
    !$omp end parallel

    print '(i3,1pe15.6,0pf12.1,a)',iter,divmax-divmin,wallclock(),' sec'

    if ((divmax-divmin) < eps .or. iter >= niter+1) then                        ! converged or last iteration?
      answer = "y"
      print *,'commit to file (y), or continue (c)?'
      read *,answer
      if (answer == "y") then                                                   ! commit results
        print *,'writing bx'
        open (1,file=file,access='direct',status='unknown',recl=io_word()*mx*my)
        do iz=1,mz
          write (1,rec=(mvar*isnap+ibx+0)*mz+iz) bx(:,:,iz)
        end do
        print *,'writing by'
        do iz=1,mz
          write (1,rec=(mvar*isnap+ibx+1)*mz+iz) by(:,:,iz)
        end do
        print *,'writing bz'
        do iz=1,mz
          write (1,rec=(mvar*isnap+ibx+2)*mz+iz) bz(:,:,iz)
        end do
        close (1)
        print *,'commmitted'
        goto 9
      else if (answer == "c") then                                              ! one more iteration
        niter=niter+1
        print *,'continuing...'
      else                                                                      ! quit, w/o saving
        print *,'NOT commmitted'
        goto 9
      end if
    end if
 
    !$omp parallel private(iz,izs,ize)
    call fft3df (divb,dphit,mx,my,mz)
    call fft3d_k2(k2,1.,1.,1.,mx,my,mz)
    k2(1,1,1) = 1.
    !$omp barrier
    call limits_omp(1,mz,izs,ize)
    do iz=izs,ize
      dphit(:,:,iz) = dphit(:,:,iz)/k2(:,:,iz)*exp(c*k2(:,:,iz))
    end do
    dphit(1,1,1) = 0.

    !$omp barrier
    call fft3db (dphit,divb,mx,my,mz)
    !$omp barrier

    call ddxdn_add(divb,bx, mx, my, mz)
    call ddydn_add(divb,by, mx, my, mz)
    call ddzdn_add(divb,bz, mx, my, mz)
    !$omp end parallel
  end do

9 continue
  deallocate (bx,by,bz,divb,dphit,k2)
END

