! $Id: double_periodic.f90,v 1.3 2016/11/09 22:04:57 aake Exp $
!******************************************************************************
MODULE double_mod
  implicit none
  integer, save:: verbose=0
CONTAINS
!-------------------------------------------------------------------------------
SUBROUTINE xupdn_set (f, fp, mx, my, mz, l)
  implicit none
  integer mx, my, mz, l
  real a, b, c, a1st
  integer i, j, k, izs, ize, im3, im2, im1, ip0, ip1, ip2
  real, dimension(mx,my,mz):: f, fp
!...............................................................................
  c = 3./256.
  b = -25./256.
  c=0.; b=0.
  a = .5-b-c
  !$omp do private(k,i,j,im3,im2,im1,ip0,ip1,ip2)
  do k=1,mz
    do j=1,my
      do i=1,mx
        im3=mod(i+l-4+mx,mx)+1
        im2=mod(i+l-3+mx,mx)+1
        im1=mod(i+l-2+mx,mx)+1
        ip0=mod(i+l-1+mx,mx)+1
        ip1=mod(i+l  +mx,mx)+1
        ip2=mod(i+l+1+mx,mx)+1
        fp(i,j,k) = (a*(f(ip0,j,k)+f(im1,j,k)) + &
                     b*(f(ip1,j,k)+f(im2,j,k)) + &
                     c*(f(ip2,j,k)+f(im3,j,k)))
      end do
    end do
  end do
END SUBROUTINE
!-------------------------------------------------------------------------------
SUBROUTINE yupdn_set (f, fp, mx, my, mz, l)
  implicit none
  integer mx, my, mz, l
  real a, b, c, a1st
  integer i, j, k, izs, ize, jm3, jm2, jm1, jp0, jp1, jp2
  real, dimension(mx,my,mz):: f, fp
!...............................................................................
  c = 3./256.
  b = -25./256.
  c=0.; b=0.
  a = .5-b-c
  !$omp do private(k,i,j,im3,im2,im1,ip0,ip1,ip2)
  do k=1,mz
    do j=1,my
      jm3=mod(j+l-4+my,my)+1
      jm2=mod(j+l-3+my,my)+1
      jm1=mod(j+l-2+my,my)+1
      jp0=mod(j+l-1+my,my)+1
      jp1=mod(j+l  +my,my)+1
      jp2=mod(j+l+1+my,my)+1
      do i=1,mx
        fp(i,j,k) = (a*(f(i,jp0,k)+f(i,jm1,k)) + &
                     b*(f(i,jp1,k)+f(i,jm2,k)) + &
                     c*(f(i,jp2,k)+f(i,jm3,k)))
      end do
    end do
  end do
END SUBROUTINE
!-------------------------------------------------------------------------------
SUBROUTINE zupdn_set (f, fp, mx, my, mz, l)
  implicit none
  integer mx, my, mz, l
  real a, b, c, a1st
  integer i, j, k, km3, km2, km1, kp0, kp1, kp2
  real, dimension(mx,my,mz):: f, fp
!...............................................................................
  c = 3./256.
  b = -25./256.
  c=0.; b=0.
  a = .5-b-c
  !$omp do private(k,i,j,km3,km2,km1,kp0,kp1,kp2)
  do k=1,mz
    km3=mod(k+l-4+mz,mz)+1
    km2=mod(k+l-3+mz,mz)+1
    km1=mod(k+l-2+mz,mz)+1
    kp0=mod(k+l-1+mz,mz)+1
    kp1=mod(k+l  +mz,mz)+1
    kp2=mod(k+l+1+mz,mz)+1
    do j=1,my
      do i=1,mx
        fp(i,j,k) = (a*(f(i,j,kp0)+f(i,j,km1)) + &
                     b*(f(i,j,kp1)+f(i,j,km2)) + &
                     c*(f(i,j,kp2)+f(i,j,km3)))
      end do
    end do
  end do
END SUBROUTINE
!-------------------------------------------------------------------------------
SUBROUTINE double (f0, nx1, ny1, nz1, f2, nx2, ny2, nz2, kx, ky, kz, interp, ym1, ym2)
  implicit none
  logical interp
  real, dimension(nx1,ny1,nz1):: f0
  real, dimension(nx2,ny2,nz2):: f2
  real ym1(ny1), ym2(ny2)
  integer nx1, ny1, nz1, nx2, ny2, nz2, kx, ky, kz, ix1, iy1, iz1
  integer ix, iy, iz, jy
  Real py, qy, dy2
  real, allocatable, dimension(:,:,:):: f1, f3, f4, f5
!...............................................................................
  allocate (f1(nx1,ny1,nz1))
  allocate (f3(nx1,ny2,nz1))
  call yupdn_set (f0,f1,nx1,ny1,nz1,1)                 ! 1 => xup, 0 => xdn 

  !$omp do private(iz,iy,ix,py,jy,qy)
  do iz=1,nz1
    do iy=1,ny1
      if (ky == 0) then
        do ix=1,nx1
          f3(ix,iy*2-1,iz) = f0(ix,iy,iz)              ! when ky=1, f4 values come before f3 values
          f3(ix,iy*2  ,iz) = f1(ix,iy,iz)              ! when ky=0, f3 values come before f4 values
        end do
      else
        iy1 = mod(iy,ny1) + 1
        do ix=1,nx1
          f3(ix,iy*2-1,iz) = 0.5*(f0(ix,iy ,iz)+f1(ix,iy,iz))
          f3(ix,iy*2  ,iz) = 0.5*(f0(ix,iy1,iz)+f1(ix,iy,iz))
        end do
      end if
    end do
  end do
  deallocate (f1)

  allocate (f4(nx1,ny2,nz1), f5(nx2,ny2,nz1))
  call xupdn_set (f3,f4,nx1,ny2,nz1,1)                 ! 1 => xup, 0 => xdn 

  !$omp do private(iz,iy,ix,py,jy,qy)
  do iz=1,nz1
    do iy=1,ny2
      if (kx == 0) then
        do ix=1,nx1
          f5(ix*2-1,iy,iz) = f3(ix,iy,iz)              ! when kx=1, f4 values come before f3 values
          f5(ix*2  ,iy,iz) = f4(ix,iy,iz)              ! when kx=0, f3 values come before f4 values
        end do
      else
        do ix=1,nx1
          ix1 = mod(ix,nx1) + 1
          f5(ix*2-1,iy,iz) = 0.5*(f3(ix ,iy,iz)+f4(ix,iy,iz))
          f5(ix*2  ,iy,iz) = 0.5*(f3(ix1,iy,iz)+f4(ix,iy,iz))
        end do
      end if
    end do
  end do
  deallocate (f3,f4)

  allocate (f4(nx2,ny2,nz1))
  call zupdn_set (f5,f4,nx2,ny2,nz1,1)                 ! 1 => zup, 0 => zdn 

  !$omp do private(iz,iy,ix,py,jy,qy)
  do iz=1,nz1
    iz1 = mod(iz,nz1) + 1
    do iy=1,ny2
      if (kz == 0) then
        do ix=1,nx2
          f2(ix,iy,iz*2-1) = f5(ix,iy,iz)              ! when kz=1, f4 values come before f4 values
          f2(ix,iy,iz*2  ) = f4(ix,iy,iz)              ! when kz=0, f5 values come before f5 values
        end do
      else
        do ix=1,nx2
          f2(ix,iy,iz*2-1) = 0.5*(f5(ix,iy,iz )+f4(ix,iy,iz))
          f2(ix,iy,iz*2  ) = 0.5*(f5(ix,iy,iz1)+f4(ix,iy,iz))
        end do
      end if
    end do
  end do
  deallocate (f4,f5)
END SUBROUTINE
END MODULE
!*******************************************************************************
PROGRAM double_prg
  USE double_mod
  implicit none
  logical interp
  integer mx,my,mz,mx2,my2,mz2,isnap,mvar,io_word,rec,ny,iz
  real, allocatable, dimension(:,:,:):: f1, f2
  real, allocatable, dimension(:):: ym1, ym2
  integer, dimension(9):: kx,ky,kz
  character(len=80):: in, out
  namelist /io/ in, out, mx, my, mz, isnap, mvar, kx, ky, kz, verbose
!...............................................................................
  mvar=8; isnap=0
  kx = (/0,1,0,0,0,0,1,0,0/)
  ky = (/0,0,1,0,0,0,0,1,0/)
  kz = (/0,0,0,1,0,0,0,0,1/)
  read (*,io); write (*,io)
  mx2=mx*2; my2=my*2; mz2=mz*2

  print'(1x,a,f8.2,a)','Memory needed:',real(mx2)*real(my2)*real(mz2)*9./1024.**3,' GB'

  allocate(f1(mx ,my ,mz ))
  allocate(f2(mx2,my2,mz2))
  
  open (10,file=in,form='unformatted',access='direct',status='old',recl=io_word()*mx*my*mz)
  open (11,file=out,form='unformatted',access='direct',status='unknown',recl=io_word()*mx2*my2)
  do rec=1,mvar
    print*,rec
    read (10,rec=rec+mvar*isnap) f1
    call double (f1,mx,my,mz,f2,mx2,my2,mz2,kx(rec),ky(rec),kz(rec),interp,ym1,ym2)
    do iz=1,mz2
      write(11,rec=iz+mz2*(rec-1)) f2(:,:,iz)
    end do
  end do
  close (10)
  close (11)
  deallocate (f1,f2)
END PROGRAM
