! $Id: main_regrid.f90,v 1.5 2016/11/09 22:04:58 aake Exp $
!-------------------------------------------------------------------------------
SUBROUTINE interpolate (vin,mxin,myin,mzin,vout,mxout,myout,mzout,o)
  implicit none
  real vin(0:mxin-1,0:myin-1,0:mzin-1), vout(0:mxout-1,0:myout-1,0:mzout-1)
  integer mxin,myin,mzin,mxout,myout,mzout,ix,iy,iz,jx,jy,jz,jxp1,jyp1,jzp1
  real px,py,pz,qx,qy,qz,f,o(3)

  !$omp parallel do private(ix,iy,iz,jx,jy,jz,jxp1,jyp1,jzp1,px,py,pz,qx,qy,qz,f)
  do iz=0,mzout-1
    f=mzin/real(mzout)
    pz=(iz+o(3))*f-o(3)
    jz=pz
    jzp1=mod(jz+1,mzin)
    pz=pz-jz
    qz=1.-pz
    do iy=0,myout-1
      f=myin/real(myout)
      py=(iy+o(2))*f-o(2)
      jy=py
      jyp1=mod(jy+1,myin)
      py=py-jy
      qy=1.-py
      do ix=0,mxout-1
        f=mxin/real(mxout)
        px=(ix+o(1))*f-o(1)
        jx=px
        jxp1=mod(jx+1,mxin)
        px=px-jx
        qx=1.-px
	vout(ix,iy,iz) = &
	  qz*(qy*(qx*vin(jx,jy  ,jz  ) + px*vin(jxp1,jy  ,jz  )) + &
	      py*(qx*vin(jx,jyp1,jz  ) + px*vin(jxp1,jyp1,jz  ))) + &
	  pz*(qy*(qx*vin(jx,jy  ,jzp1) + px*vin(jxp1,jy  ,jzp1)) + &
	      py*(qx*vin(jx,jyp1,jzp1) + px*vin(jxp1,jyp1,jzp1)))
      end do
    end do
  end do
END

!-------------------------------------------------------------------------------
PROGRAM regrid
  implicit none
  integer mxin,myin,mzin,mxout,myout,mzout,isnap,mvar,var,io_word,iz,vx(2),i
  real, allocatable, dimension(:,:,:):: vin,vout
  character(len=60) fin,fout
  namelist /in/fin,fout,mxin,myin,mzin,mxout,myout,mzout,isnap,mvar,vx
  real offset(3)

  fin='in.dat'
  fout='out.dat'
  mxin=1000
  myin=0
  mzin=0
  mxout=1024
  myout=0
  mzout=0
  isnap=0
  mvar=7
  vx=(/2,5/)

  write (*,in)
  read (*,in)
  if (myin==0) myin=mxin
  if (mzin==0) mzin=mxin
  if (myout==0) myout=mxout
  if (mzout==0) mzout=mxout
  write (*,in)

  open(1,file=fin ,access='direct',status='old'    ,recl=io_word()*mxin *myin *mzin)
  open(2,file=fout,access='direct',status='unknown',recl=io_word()*mxout*myout)
 
  allocate (vin(mxin,myin,mzin), vout(mxout,myout,mzout))

  do var=1,mvar
    print *,var,'reading'
    read(1,rec=mvar*isnap+var) vin
    print *,var,'interpolating'
    offset=(/.0,.0,.0/)
    do i=1,2
      if (var == vx(i)  ) offset=(/.5,.0,.0/)
      if (var == vx(i)+1) offset=(/.0,.5,.0/)
      if (var == vx(i)+2) offset=(/.0,.0,.5/)
    end do
    call interpolate (vin,mxin,myin,mzin,vout,mxout,myout,mzout,offset)
    print *,var,'writing'
    do iz=1,mzout
      write (2,rec=(mvar*isnap+var-1)*mzout+iz) vout(:,:,iz)
    end do
  end do

  deallocate (vin,vout)
  close (1)
  close (2)
END

