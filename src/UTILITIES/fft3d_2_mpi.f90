! $Id: fft3d_2_mpi.f90,v 1.12 2012/12/16 06:13:43 bob Exp $
!----------------------------------------------------------------------
!
!  Three-dimensional Fourier transform routines, based on FFTPACK.
!  FFTPACK is transportable, and allows arbitrary dimensions (speed
!  will vary, of course).
!
!  CALL fft3df (f, ft, mx, my, mz)              ! forward transform
!  CALL fft3db (ft, f, mx, my, mz)              ! backward transform
!  CALL fft3d_k2 (k2, dx, dy, dz, mx, my, mz)   ! compute k2
!
!**********************************************************************
SUBROUTINE fftpack_test
!
!  This test demonstrates that the coeffs returned are:
!
!   [a_0, +a_1/2., +b_1/2., ..., b_{ny-1}/2.,  a_ny]
!
!  Thus, the derivive is formed by setting the tranform to
!
!   [ 0., -b_1/2, +a_1/2., ...., a_{ny-1}/2.,  0.  ]
!
  real, allocatable, dimension(:):: f,ft,w
  integer m,lw,i,k
  real a
  real, parameter:: pi=3.14159265
!----------------------------------------------------------------------
  m=8
  lw=2*m+15
  allocate (f(m),ft(m),w(lw))

  a = 2.*pi/m
  f = 0.
  do k=0,m/2
    do i=1,m
      f(i) = f(i) + 2.*(k+1)*cos((i-1)*k*a) + 2.*(k+m)*sin((i-1)*k*a)
    end do
  end do
  call srffti (m,w)
  print *,'f:'
  print *,f
  ft = f
  call srfftf (m,ft,w)
  print *,'ft:'
  print '(8f10.2)',ft/m

  m = 7
  a = 2.*pi/m
  f = 0.
  do k=0,m/2
    do i=1,m
      f(i) = f(i) + 2.*(k+1)*cos((i-1)*k*a) + 2.*(k+m)*sin((i-1)*k*a)
    end do
  end do
  call srffti (m,w)
  print *,'f:'
  print '(8f10.2)',(f(i),i=1,m)
  ft = f
  call srfftf (m,ft,w)
  print *,'ft:'
  print '(8f10.2)',(ft(i)/m,i=1,m)

  deallocate (f,ft,w)
END

!**********************************************************************
SUBROUTINE fft3d_k2 (k2,dx1,dy1,dz1,mx1,my1,mz1)
  USE params
  implicit none
  real, dimension(mx,my,mz):: k2
  real dx1, dy1, dz1
  integer mx1, my1, mz1
  logical omp_in_parallel
!----------------------------------------------------------------------
  if (omp_in_parallel()) then
    call fft3d_k2_omp (k2,dx1,dy1,dz1,mx1,my1,mz1)
  else
    !$omp parallel shared(dx1,dy1,dz1,mx1,my1,mz1)
    call fft3d_k2_omp (k2,dx1,dy1,dz1,mx1,my1,mz1)
    !$omp end parallel
  end if
END

!**********************************************************************
SUBROUTINE fft3d_k2_omp (k2,dx1,dy1,dz1,mx1,my1,mz1)
  USE params
  implicit none
  real, dimension(mx,my,mz):: k2
  real dx1, dy1, dz1, kx, ky, kz
  integer i, j, jj, k, kk, mx1, my1, mz1
!----------------------------------------------------------------------
  do k=izs,ize
    kk=k+mpi_z*mz
    kz=2.*pi/sz*(kk/2)
    do j=1,my
      jj=j+mpi_y*my
      ky=2.*pi/sy*(jj/2)
      do i=1,mx
        kx=2.*pi/sx*(i/2)
        k2(i,j,k)=kx**2+ky**2+kz**2
      end do
    end do
  end do
!$omp barrier
END SUBROUTINE

!**********************************************************************
SUBROUTINE fft3df (r,rr,mx1,my1,mz1)
  USE params
  implicit none
  integer mx1,my1,mz1
  real, dimension(mx,my,mz):: r, rr
  logical omp_in_parallel
!----------------------------------------------------------------------
  if (omp_in_parallel()) then
    call fft3df_omp (r,rr,mx1,my1,mz1)
  else
    !$omp parallel shared(mx1,my1,mz1)
    call fft3df_omp (r,rr,mx1,my1,mz1)
    !$omp end parallel
  end if
END

!**********************************************************************
SUBROUTINE fft3df_omp (r,rr,mx1,my1,mz1)
  USE params
  implicit none
  integer i,j,k,mx1,my1,mz1
  integer, parameter:: lw=2048, lwx=2*lw+15
  real, dimension(mx,my,mz):: r, rr
  real c, wx(lwx), fx(mx), fy(my), fz(mz,mx)
!----------------------------------------------------------------------
  if (mx.gt.lw .or. my.gt.lw .or. mz.gt.lw) then
    print *,'fft3db: dim too small',mx,my,mz,lw
    stop
  end if
  call check_fft
  call srffti (mx,wx)

!$omp barrier
  do k=izs,ize
   do j=1,my
    fx(:)=r(:,j,k)
    call srfftf (mx,fx,wx)
    c=1./mx
    scratch(:,j,k)=c*fx(:)
   end do
  end do

  call permute (scratch, rr)
  do k=izs,ize
   do j=1,my
    fx(:)=rr(:,j,k)
    call srfftf (mx,fx,wx)
    c=1./mx
    scratch(:,j,k)=c*fx(:)
   end do
  end do

  call permute (scratch, rr)
  do k=izs,ize
   do j=1,my
    fx(:)=rr(:,j,k)
    call srfftf (mx,fx,wx)
    c=1./mx
    scratch(:,j,k)=c*fx(:)
   end do
  end do

  call permute (scratch, rr)
!$omp barrier

END SUBROUTINE

!**********************************************************************
SUBROUTINE fft3db (rr,r,mx1,my1,mz1)
  USE params
  implicit none
  integer mx1,my1,mz1
  real, dimension(mx,my,mz):: r, rr
  logical omp_in_parallel
!----------------------------------------------------------------------
  if (omp_in_parallel()) then
    call fft3db_omp (rr,r,mx1,my1,mz1)
  else
    !$omp parallel shared(mx1,my1,mz1)
    call fft3db_omp (rr,r,mx1,my1,mz1)
    !$omp end parallel
  end if
END

!**********************************************************************
SUBROUTINE fft3db_omp (rr,r,mx1,my1,mz1)
  USE params
  implicit none

  integer i,j,k,mx1,my1,mz1
  integer, parameter:: lw=2048, lwx=2*lw+15

  real, dimension(mx,my,mz):: r, rr
  real wx(lwx), fx(mx), fy(my), fz(mz,mx)
!----------------------------------------------------------------------

  if (mx.gt.lw .or. my.gt.lw .or. mz.gt.lw) then
    print *,'fft3db: dim too small',mx,my,mz,lw
    stop
  end if
  call check_fft
  call srffti (mx,wx)

!$omp barrier
  call permute (rr, scratch)
  do k=izs,ize
   do j=1,my
    fx=scratch(:,j,k)
    call srfftb (mx,fx,wx)
    r(:,j,k)=fx
   end do
  end do

  call permute (r, scratch)
  do k=izs,ize
   do j=1,my
    fx=scratch(:,j,k)
    call srfftb (mx,fx,wx)
    r(:,j,k)=fx
   end do
  end do

  call permute (r, scratch)
  do k=izs,ize
   do j=1,my
    fx=scratch(:,j,k)
    call srfftb (mx,fx,wx)
    r(:,j,k)=fx
   end do
  end do
!$omp barrier

END SUBROUTINE

!**********************************************************************
SUBROUTINE check_fft
  USE params
  implicit none
!----------------------------------------------------------------------
  if (mpi_nx > 1) then
    if (master) print *,'WARNING, fft3d_mpi2.f90: cannot handle MPI in x-direction; aborting', mpi_nx
    stop
  end if
END

!**********************************************************************
SUBROUTINE test_fft
  USE params
  implicit none
  integer ix, iy, iz, jseed
  real sum1, sum2, average, ran1s
  real, allocatable, dimension(:,:,:):: fft1, fft2, fft3, tmp
  character(len=mid), save:: id='$Id: fft3d_2_mpi.f90,v 1.12 2012/12/16 06:13:43 bob Exp $'
!----------------------------------------------------------------------
  call print_id(id)

  if (mpi_nx > 1) then
    if (master) print *,'WARNING, fft3d_mpi2.f90: cannot handle MPI in x-direction; skipping tests'
    return
  end if

  call check_fft
  call test_permute

  allocate (fft1(mx,my,mz), fft2(mx,my,mz), fft3(mx,my,mz), tmp(mx,my,mz))

!$omp parallel private(ix,iy,iz)
  do iz=izs,ize
  do iy=1,my
  do ix=1,mx
    fft1(ix,iy,iz) = cos(2.*pi*(xm(ix)-xmin)/sx) + 2.*cos(2.*pi*(ym(iy)-ymin)/sy) + 3.*cos(2.*pi*(zm(iz)-zmin)/sz)
  end do
  end do
  end do
!$omp barrier
  call fft3df (fft1, fft2, mx, my, mz)
!$omp barrier
  call fft3db (fft2, fft3, mx, my, mz)
!$omp end parallel

  tmp = fft1**2
  sum1 = average(tmp)
  tmp = (fft1-fft3)**2
  sum2 = average(tmp)
  if (master) print '(8f10.3)', fft2(1:2,1:2,1:2)
  if (master) print*,'test_fft3d_2:',sum2/sum1
   
  jseed = 88
  do iz=1,mz
  do iy=1,my
  do ix=1,mx
    fft1(ix,iy,iz) = ran1s(jseed)
  end do
  end do
  end do
  call fft3df (fft1, fft2, mx, my, mz)
  call fft3d_k2 (fft1,0.2,0.3,0.4,mx,my,mz)
  fft2 = fft1*fft2
  call fft3db (fft2, fft3, mx, my, mz)
  tmp = fft3**2
  sum1 = average(tmp)
  if (master) print*,'test_fft3d_2(B):',sum1

  deallocate (fft1, fft2, fft3, tmp)
END
