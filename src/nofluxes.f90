! $Id: nofluxes.f90,v 1.2 2011/02/23 13:21:57 remo Exp $

!*******************************************************************************
SUBROUTINE hd_fluxes (rho,Ux,Uy,Uz,e,p,py,Txy,Tyy,Tyz,scr1,scr2,scr3,scr4,scr5,scr6)
  USE params
  implicit none
  real, dimension(mx,my,mz):: rho,Ux,Uy,Uz,e,p,py,Txy,Tyy,Tyz,scr1,scr2,scr3,scr4,scr5,scr6
END

!*******************************************************************************
SUBROUTINE radiative_flux (qq, scr1)
  USE params
  implicit none
  real, dimension(mx,my,mz):: qq, scr1
END

!*******************************************************************************
SUBROUTINE Poynting_flux (Ex,Ey,Ez,Bx,By,Bz,scr1,scr2,scr3,scr4,scr5,scr6)
  USE params
  implicit none
  real, dimension(mx,my,mz):: Ex,Ey,Ez,Bx,By,Bz,scr1,scr2,scr3,scr4,scr5,scr6
END

!*******************************************************************************
SUBROUTINE output_fluxes
  USE params
  implicit none
  real, dimension(mx,my,mz):: Ex,Ey,Ez,Bx,By,Bz,scr1,scr2,scr3,scr4,scr5,scr6
END
