! $Id: pde_cm_adv.f90,v 1.7 2006/04/09 06:04:17 aake Exp $
!***********************************************************************
MODULE pde_cm
  USE params
  USE stagger
  USE df
  implicit none

  real ambi,tcool,chi        ! parameters

  real eemean,gg1            ! temporaries
  integer i,j,k

  real, allocatable, dimension(:,:,:):: &
    r, px, py, pz, e, &
    uxa, uya, uza, &
    xdnr, ydnr, zdnr, r1, d2, &
    lnP, cs, vx, vy, vz, &
    vx1, vy1, vz1, &
    pp, t1, bvx, bvy, bvz, &
    Ix, Iy, Iz, Ex, Ey, Ez, &
    scr1, scr2, scr3, scr4, scr5, scr6

CONTAINS

!***********************************************************************
SUBROUTINE alloc
  allocate( &
    r(mx,my,mz), px(mx,my,mz), py(mx,my,mz), pz(mx,my,mz), e(mx,my,mz), &
    Uxa(mx,my,mz), Uya(mx,my,mz), Uza(mx,my,mz), &
    xdnr(mx,my,mz), ydnr(mx,my,mz), zdnr(mx,my,mz), r1(mx,my,mz), d2(mx,my,mz), &
    lnP(mx,my,mz), cs(mx,my,mz), vx(mx,my,mz), vy(mx,my,mz), vz(mx,my,mz), &
    vx1(mx,my,mz), vy1(mx,my,mz), vz1(mx,my,mz), &
    pp(mx,my,mz), t1(mx,my,mz), bvx(mx,my,mz), bvy(mx,my,mz), bvz(mx,my,mz), &
    ix(mx,my,mz), iy(mx,my,mz), iz(mx,my,mz), ex(mx,my,mz), ey(mx,my,mz), ez(mx,my,mz), &
    scr1(mx,my,mz), scr2(mx,my,mz), scr3(mx,my,mz), scr4(mx,my,mz), scr5(mx,my,mz), scr6(mx,my,mz) &
  )
END SUBROUTINE

!***********************************************************************
SUBROUTINE dealloc
  deallocate( &
    r, px, py, pz, e, &
    Uxa, Uya, Uza, &
    xdnr, ydnr, zdnr, r1, d2, &
    lnP, cs, vx, vy, vz, &
    vx1, vy1, vz1, &
    pp, t1, bvx, bvy, bvz, &
    ix, iy, iz, ex, ey, ez, &
    scr1, scr2, scr3, scr4, scr5, scr6 &
  )
END SUBROUTINE

!***********************************************************************
SUBROUTINE stats (label, a)
  implicit none
  character*(*) label
  real, dimension (mx,my,mz):: a
  logical exist
  integer,save:: irec=0
  integer lrec

  inquire (file='stat.txt',exist=exist)
  if (exist) then
    !open (15,file='stat.txt',status='old')
    if (do_trace) write (*,*) irec,label
    write (15,*) irec,label
    open (16,file='stat.dat',status='unknown',access='direct',recl=lrec(mw))
    call mpi_name(file)
    open (16,file=file,status='unknown',access='direct',recl=lrec(mw))
    write (16,rec=irec+1) a
    close (16)
    irec=irec+1
    !close (15)
  end if
  if (idbg.eq.0) return
  if ((.not.master) .and. (idbg.eq.1)) return
  write (*,'(1x,i4,1x,a,5(1pe11.3))') mpi_rank,label,minval(a),sum(a)/mw,maxval(a)
END SUBROUTINE

!***********************************************************************
SUBROUTINE ptrace (label, level)
  implicit none
  integer level
  character*(*) label

  if (idbg .ge. level) then
    write (*,*) label
    call oflush
  end if
END SUBROUTINE

!***********************************************************************
SUBROUTINE diffus (lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt)
  implicit none
  real, dimension(mx,my,mz):: lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt

!  Face centered velocities

  !$omp parallel do private(k)
  do k=1,mz
    r(:,:,k) = exp(lnr(:,:,k))
  end do

  call xdn_set (lnr, xdnr)
  call ydn_set (lnr, ydnr)
  call zdn_set (lnr, zdnr)

  !$omp parallel do private(k)
  do k=1,mz
    xdnr(:,:,k)=exp(xdnr(:,:,k))
    ydnr(:,:,k)=exp(ydnr(:,:,k))
    zdnr(:,:,k)=exp(zdnr(:,:,k))
    px(:,:,k) = Ux(:,:,k)*xdnr(:,:,k)
    py(:,:,k) = Uy(:,:,k)*ydnr(:,:,k)
    pz(:,:,k) = Uz(:,:,k)*zdnr(:,:,k)
  end do

!  Temperature.  For Mach 10 or higher, even a single deedt*dt
!  can make the temperature negative!  Hence reset to constant.

  if (gamma .eq. 1.) then
    gg1 = 1.
  else
    gg1 = gamma*(gamma-1)
  endif
  if (tcool .lt. 0.0) then
    eemean = csound**2/gg1
    if (chi .eq. 1. .or. chi .eq. 0.) then
      !loop
      ee = eemean
    else
      !loop
      ee = eemean*exp(lnr*(chi-1.))
    endif
  endif

!  Pressure.  For perfect gas, P = e_th * (gamma-1)

  !$omp parallel do private(k)
  do k=1,mz
    r1(:,:,k) = ee(:,:,k)*(gg1/gamma)               ! r1=P/rho
    lnP(:,:,k) = alog(r1(:,:,k)*r(:,:,k))
    cs(:,:,k) = nu3*sqrt(gg1*ee(:,:,k))
  end do
  call stats ('r', r)
  call stats ('ee', ee)
  call dif1_set (Ux, Uy, Uz, scr2)
  !$omp parallel do private(k)
  do k=1,mz
    scr2(:,:,k) = amax1(scr2(:,:,k),0.)
  end do
  call stats ('difU', scr2)

!  Smooth over 3x3x3 point, to broaden influence.  Use a low order shift
!  after smoothing, to ensure positive definite values.

  call max3_set (scr2,scr1)
  call smooth3_set (scr1,scr4)

END SUBROUTINE
!************************************************************************
SUBROUTINE diffusivities (lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt,flag)
  USE params
  implicit none
  real, dimension(mx,my,mz):: lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt
  real average,fmaxval,fCv
  logical flag

!  Diffusivities

  if (nu4 > 0) then
    call ddxdn_set(lnr,scr5)
    scr5 = (dx*scr5)**2
    call xup1_set(scr5,scr1)
    call ddydn_set(lnr,scr5)
    scr5 = (dy*scr5)**2
    call yup1_set(scr5,scr2)
    call ddzdn_set(lnr,scr5)
    scr5 = (dz*scr5)**2
    call zup1_set(scr5,scr3)

    !loop
    cs = cs+nu2*scr4*(1.+nu4*(scr1+scr2+scr3))
  else
    cs = cs+nu2*scr4
  end if

  call xdn1_set (cs, scr1)
  !loop
  vx = (scr1 + nu1*abs(Ux))
  call ydn1_set (cs, scr1)
  !loop
  vy = (scr1 + nu1*abs(Uy))
  call zdn1_set (cs, scr1)
  !loop
  vz = (scr1 + nu1*abs(Uz))

  call stats ('vx', vx)
  call stats ('vy', vy)
  call stats ('vz', vz)

  if (flag) then
    !$omp parallel do private(k)
    do k=1,mz
      scr1(:,:,k) = Ux(:,:,k)**2 + Uy(:,:,k)**2 + Uz(:,:,k)**2
    end do
    Urms = sqrt(average(scr1))
    !$omp parallel do private(k)
    do k=1,mz
      scr1(:,:,k) = sqrt(scr1(:,:,k) + gg1*ee(:,:,k))
    end do
    !$omp parallel do private(k,j,i)
    do k=1,mz
    do j=1,my
    do i=1,mx
      scr1(i,j,k) = scr1(i,j,k)*(dt/min(dxm(i),dym(j),dzm(k)))
    end do
    end do
    end do
    Cu = fmaxval('Cu',scr1)

!-----------------------------------------------------------------------
!  Viscosity Courand condition factors:
!     6.2 : the maximum value returned from ddxup(ddxdn(f)) when dx=1.
!      3. : for the worst case of a 3-D checker board
!      2. : overshoot with a factor 2. at Cdtd=1., for marginal stability
!-----------------------------------------------------------------------
    fCv = 6.2*3.*dt/2.

    !$omp parallel do private(k,j,i)
    do k=1,mz
    do j=1,my
    do i=1,mx
      scr1(i,j,k) = fCv*max(vx(i,j,k)/dxm(i),vy(i,j,k)/dym(j),vz(i,j,k)/dzm(k))
    end do
    end do
    end do
    Cv = fmaxval('Cv',scr1)
  end if
  !$omp parallel do private(k,j,i)
  do k=1,mz
  do j=1,my
  do i=1,mx
    vx(i,j,k) = vx(i,j,k)*dxm(i)
    vy(i,j,k) = vy(i,j,k)*dym(j)
    vz(i,j,k) = vz(i,j,k)*dzm(k)
  end do
  end do
  end do
END SUBROUTINE

!***********************************************************************
SUBROUTINE mass (lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt)
  USE params
  implicit none
  real, dimension(mx,my,mz):: lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt

!-----------------------------------------------------------------------

!  Mass conservation, dlnM/dt = - div (U)
!  ====================================

!  High order diffusive term necessary for low-beta plasmas, where a
!  saw-tooth density cannot drive velocity via gas pressure.  Does no
!  harm for high-beta plasmas.

!      dlnrdt = - ddxup(Ux) - ddyup(Uy) - ddzup(Uz)

  call ddxdn_set (lnr,scr1)
  Uxa = Ux - nur*vx*scr1
  call ddydn_set (lnr,scr1)
  Uya = Uy - nur*vy*scr1
  call ddzdn_set (lnr,scr1)
  Uza = Uz - nur*vz*scr1

  call ddxup_neg (Uxa, scr1)
  call ddyup_sub (Uya, scr1)
  call ddzup_sub (Uza, scr1)
  dlnrdt = dlnrdt + scr1

  if (do_energy) deedt = deedt + r1*scr1

!      dlnrdt = dlnrdt
!     &         - xup(Ux*scr1)
!     &         - yup(Uy*scr2)
!     &         - zup(Uz*scr3)

  call ddxdn_set( lnr, scr1)
  call ddydn_set( lnr, scr2)
  call ddzdn_set( lnr, scr3)

  scr4 = Uxa*scr1
  call xup_sub(scr4, dlnrdt)
  scr4 = Uya*scr2
  call yup_sub(scr4, dlnrdt)
  scr4 = Uza*scr3
  call zup_sub(scr4, dlnrdt)
END SUBROUTINE

!*********************************************************************
SUBROUTINE massexact (lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt)
  USE params
  implicit none
  real, dimension(mx,my,mz):: lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt
  real, save:: dlnrm=0.
  real rm, drdtm, average
  integer, save:: i_step=0
  integer n_step

!--------------------------------------------------------------------

!  Make mass conservation exact:  Add the smoothed difference btw
!  the per volume time derivative and the time derivative from
!  logarithmic vars.  Important to include diffusion also in
!  the formula below, to minimize the difference (should only
!  be truncation errors).

   n_step=15
   if (abs(mod(i_step,n_step)-1) .le. 1) then
     rm = average(r)
     !loop
     scr1 = r*dlnrdt
     drdtm = average(scr1)
     !loop
     dlnrm = drdtm/rm
     if (mod(i_step,n_step) .eq. 0 .and. master) print *,'rm,dlnrm=',rm,dlnrm
   endif
   i_step = i_step+1
   !loop
   dlnrdt = dlnrdt-dlnrm

!c      scr1 =
!c     &      - ddxup(px - xdn(r)*vx*ddxdn(lnr))
!c     &      - ddyup(py - ydn(r)*vy*ddydn(lnr))
!c     &      - ddzup(pz - zdn(r)*vz*ddzdn(lnr))
!c      scr1 =
!c     &      - ddxup1(px - vx*ddxdn1(r))
!c     &      - ddyup1(py - vy*ddydn1(r))
!c     &      - ddzup1(pz - vz*ddzdn1(r))
!c      scr1 = scr1 - r*dlnrdt
!c      call smooth5 (scr1)
!c      dlnrdt = dlnrdt + scr1/r
END SUBROUTINE
!***********************************************************************
SUBROUTINE momx (lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt)
  USE params
  implicit none
  real, dimension(mx,my,mz):: lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt

!      dUxdt = - xdn(r1)*ddxdn(lnP)
!     &      - Ux*xdn(ddxup(Ux))
!     &      - yup(Uy*ddydn(Ux))
!     &      - zup(Uz*ddzdn(Ux))

  call ddxdn_set (lnP, scr1)
  call xdn_set (r1, scr2)
  call ddxup_set (Ux, scr3)
  call xdn_set (scr3, scr4)
  !loop
  dUxdt = dUxdt - scr1*scr2 - Uxa*scr4

  call ddydn_set (Ux, scr5)
  call xdn_set (Uy, scr6)
  !loop
  scr4 = scr6*scr5
  call yup_sub (scr4, dUxdt)

  call ddzdn_set (Ux, scr6)
  call xdn_set (Uz, scr1)
  !loop
  scr4 = scr1*scr6
  call zup_sub (scr4, dUxdt)

!      scr1 = xup1(vx)*ddxup(Ux)

!      call ddxup_set (Ux, scr3)       ! already computed
  call xup1_set (vx, scr2)
  !loop
  scr1 = scr3*scr2

!      dUxdt = dUxdt + ddxdn(scr1) + ddxdn(lnr)*xdn(scr1)

  call ddxdn_add (scr1, dUxdt)

  call ddxdn_set (lnr, scr2)
  call xdn_set (scr1, scr3)
  !loop
  dUxdt = dUxdt + scr2*scr3

!  Symmetrized
!      scr2 = 0.5*(xdn1(vy)*ddydn(Ux) +
!     &            ydn1(vx)*ddxdn(Uy))
!      scr3 = 0.5*(xdn1(vz)*ddzdn(Ux) +
!     &            zdn1(vx)*ddxdn(Uz))

!      call ddydn_set (Ux, scr5)      ! already computed
  call xdn1_set(vy, scr4)
  call ddxdn_set (Uy, scr1)       ! try to save for later
  call ydn1_set(vx, scr2)
  !loop
  scr2 = 0.25*(scr4+scr2)*(scr1+scr5)

!      call ddzdn_set (Ux, scr6)       ! already computed
  call xdn1_set(vz, scr4)
  call ddxdn_set (Uz, scr5)        ! try to save for later
  call zdn1_set(vx, scr1)
  !loop
  scr3 = 0.25*(scr4+scr1)*(scr6+scr5)

!      scr4 = xdn(lnr)
  call xdn_set (lnr, scr4)

!      dUxdt = dUxdt
!     &              + ddyup(scr2) + yup(ddydn(scr4)*scr2)
!     &              + ddzup(scr3) + zup(ddzdn(scr4)*scr3)

  call ddyup_add (scr2, dUxdt)

  call ddydn_set (scr4, scr1)
  !loop
  scr1 = scr1*scr2
  call yup_add (scr1, dUxdt)

  call ddzup_add (scr3, dUxdt)

  call ddzdn_set (scr4, scr1)
  !loop
  scr1 = scr1*scr3
  call zup_add (scr1, dUxdt)

!      scr5 = ydn(lnr)
  call ydn_set (lnr, scr5)

!      dUydt =
!     &              + ddxup(scr2) + xup(ddxdn(scr5)*scr2)

  call ddxup_add (scr2, dUydt)

  call ddxdn_set (scr5, scr1)
  !loop
  scr1 = scr1*scr2
  call xup_add (scr1, dUydt)

!      scr6 = zdn(lnr)
  call zdn_set (lnr, scr6)

!      dUzdt =
!     &              + ddxup(scr3) + xup(ddxdn(scr6)*scr3)

  call ddxup_add (scr3, dUzdt)

  call ddxdn_set (scr6, scr1)
  !loop
  scr1 = scr1*scr3
  call xup_add (scr1, dUzdt)
END SUBROUTINE
!***********************************************************************
SUBROUTINE momy (lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt)
  USE params
  implicit none
  real, dimension(mx,my,mz):: lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt

!      scr1 = yup1(vy)*ddyup(Uy)

  call ddyup_set (Uy, scr4)
  call yup1_set (vy, scr2)
  scr1 = scr4*scr2

!      dUydt = dUydt - ydn(r1)*ddydn(lnP)
!     &      - Uy*ydn(ddyup(Uy))
!     &      - zup(Uz*ddzdn(Uy))
!     &      - xup(Ux*ddxdn(Uy))
!     &      + ddydn(scr1) + ddydn(lnr)*ydn(scr1)

  call ddydn_add (scr1, dUydt)

  call ydn_set (scr1, scr2)
  call ddydn_set (lnr, scr3)
  !loop
  dUydt = dUydt + scr3*scr2

  call ddydn_set (lnP, scr1)
  call ydn_set (r1, scr2)
  !loop
  dUydt = dUydt - scr1*scr2

!      call ddyup_set (Uy, scr4)       ! already computed
  call ydn_set (scr4, scr2)
  !loop
  dUydt = dUydt - Uya*scr2

  call ddzdn_set (Uy, scr4)
  call ydn_set (Uz, scr2)
  !loop
  scr1 = scr4*scr2
  call zup_sub (scr1, dUydt)

  call ddxdn_set (Uy, scr1)
  call ydn_set (Ux, scr2)
  !loop
  scr1 = scr1*scr2
  call xup_sub (scr1, dUydt)

!  Symmetrized

!      scr2 = 0.5*(ydn1(vz)*ddzdn(Uy) +
!     &            zdn1(vy)*ddydn(Uz))

  call zdn1_set(vy, scr1)
  call ydn1_set(vz, scr2)
  !loop
  scr2 = scr1+scr2
  call ddydn_set (Uz, scr3)
!      call ddzdn_set (Uy, scr4)       ! already computed
  !loop
  scr2 = 0.25*scr2*(scr4+scr3)

!      dUydt = dUydt
!     &              + ddzup(scr2) + zup(ddzdn(scr5)*scr2)

  call ddzup_add (scr2, dUydt)

  call ddzdn_set (scr5, scr1)
  !loop
  scr1 = scr1*scr2
  call zup_add(scr1, dUydt)

!      dUzdt = dUzdt
!     &              + ddyup(scr2) + yup(ddydn(scr6)*scr2)

  call ddyup_add (scr2, dUzdt)

  call ddydn_set (scr6, scr1)
  !loop
  scr1 = scr1*scr2
  call yup_add(scr1, dUzdt)
END SUBROUTINE
!***********************************************************************
SUBROUTINE momz (lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt)
  USE params
  implicit none
  real, dimension(mx,my,mz):: lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt

!      dUzdt = dUzdt - zdn(r1)*ddzdn(lnP)
!     &      - Uz*zdn(ddzup(Uz))
!     &      - xup(Ux*ddxdn(Uz))
!     &      - yup(Uy*ddydn(Uz))

  call ddzdn_set (lnP, scr1)
  call zdn_set (r1, scr2)
  !loop
  dUzdt = dUzdt - scr2*scr1

  call ddzup_set (Uz, scr4)
  call zdn_set (scr4, scr2)
  !loop
  dUzdt = dUzdt - Uza*scr2

  call ddxdn_set (Uz, scr1)
  call zdn_set (Ux, scr2)
  !loop
  scr1 = scr2*scr1
  call xup_sub (scr1, dUzdt)

!      call ddydn_set (Uz, scr3)      ! already computed
  call zdn_set (Uy, scr2)
  !loop
  scr3 = scr2*scr3
  call yup_sub (scr3, dUzdt)

!      scr1 = zup1(vz)*ddzup(Uz)

!      call ddzup_set (Uz, scr4)      ! already computed
  call zup1_set (vz, scr2)
  !loop
  scr1 = scr2*scr4

!      dUzdt = dUzdt + ddzdn(scr1) + ddzdn(lnr)*zdn(scr1)

  call ddzdn_add (scr1, dUzdt)
  call zdn_set   (scr1, scr2)
  call ddzdn_set (lnr, scr1)
  !loop
  dUzdt = dUzdt + scr1*scr2
END SUBROUTINE
!***********************************************************************
SUBROUTINE energy (lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt)
  USE params
  implicit none
  real, dimension(mx,my,mz):: lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt

!-----------------------------------------------------------------------

!  Energy balance, d(e)/dt = - div (eU) - P div(U)
!  ===============================================


!  Cooling

  if (tcool .lt. 0.0 .or. gamma .eq. 1.) then
    !loop
    deedt = 0.
  else
    if (tcool .gt. 0.) then
       eemean = csound**2/(gamma*(gamma-1.))
       !deedt = deedt - (ee - eemean)/(tcool*(ee**0.5)*r**(-1))
       deedt = deedt - (ee - eemean)/(tcool*exp(amin1(r,70.)))
   end if

!  Note: the P/r*div(U) in done in mass

!        deedt = deedt
!     &     - xup(Ux*ddxdn(ee))
!     &     - yup(Uy*ddydn(ee))
!     &     - zup(Uz*ddzdn(ee))
!     &     +(ddxup(vx*ddxdn(ee))
!     &     + ddyup(vy*ddydn(ee))
!     &     + ddzup(vz*ddzdn(ee))
!     &      )

    call ddxdn_set(ee, scr1)
    call ddydn_set(ee, scr2)
    call ddzdn_set(ee, scr3)
    !loop
    scr4 = vx*scr1
    scr5 = vy*scr2
    scr6 = vz*scr3
    scr1 = Ux*scr1
    scr2 = Uy*scr2
    scr3 = Uz*scr3
    call xup_sub (scr1, deedt)
    call yup_sub (scr2, deedt)
    call zup_sub (scr3, deedt)
    call ddxup_add (scr4, deedt)
    call ddyup_add (scr5, deedt)
    call ddzup_add (scr6, deedt)
  end if
END SUBROUTINE
!***********************************************************************
SUBROUTINE Lorentz (lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt)
  USE params
  implicit none
  real, dimension(mx,my,mz):: lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt
  real Umax,fmaxval
  integer k

!  Current = curl(B)

!      Ix = ddydn(Bz) - ddzdn(By)
!      Iy = ddxdn(Bz)
!      Iz = ddxdn(By)
!      Iy = ddzdn(Bx) - Iy
!      Iz = Iz - ddydn(Bx)

  call ddydn_set (Bz, Ix)
  call ddzdn_sub (By, Ix)

  call ddzdn_set (Bx, Iy)
  call ddxdn_sub (Bz, Iy)

  call ddxdn_set (By, Iz)
  call ddydn_sub (Bx, Iz)

!  Lorentz force IxB.  Add ambipolar diffusion speed directly to
!  Ui, since Ui is only used in the induction equation after this.
!  Scale the coefficient with dx, to make the effect scale-indep.

!      scr1 = zup(Iy*xdn(Bz)) - yup(Iz*xdn(By))
!      scr2 = xup(Iz*ydn(Bx)) - zup(Ix*ydn(Bz))
!      scr3 = yup(Ix*zdn(By)) - xup(Iy*zdn(Bx))

!  Electric field   E = - uxB + eta I

!      Ex = Ex - zdn(py)*ydn(Bz) + ydn(pz)*zdn(By)
!      Ey = Ey - xdn(pz)*zdn(Bx) + zdn(px)*xdn(Bz)
!      Ez = Ez - ydn(px)*xdn(By) + xdn(py)*ydn(Bx)

!      Ex =  - zdn(Uy)*ydn(Bz) + ydn(Uz)*zdn(By)
!      Ey =  - xdn(Uz)*zdn(Bx) + zdn(Ux)*xdn(Bz)
!      Ez =  - ydn(Ux)*xdn(By) + xdn(Uy)*ydn(Bx)

  if (ambi .eq. 0.0) then
    call xdn_set(Bz, scr4)
    call zdn_set(Ux, scr5)
    !loop
    Ey = + scr4*scr5
    scr4 = Iy*scr4

    call zup_set(scr4, scr1)

    call xdn_set(By, scr4)
    call ydn_set(Uxa, scr5)
    !loop
    Ez = - scr4*scr5
    scr4 = Iz*scr4

    call yup_sub(scr4, scr1)

    call ydn_set(Bx, scr4)
    call xdn_set(Uya, scr5)
    !loop
    Ez = Ez + scr4*scr5
    scr4 = Iz*scr4

    call xup_set(scr4, scr2)

    call ydn_set(Bz, scr4)
    call zdn_set(Uya, scr5)
    !loop
    Ex = - scr4*scr5
    scr4 = Ix*scr4

    call zup_sub(scr4, scr2)

    call zdn_set(By, scr4)
    call ydn_set(Uza, scr5)
    !loop
    Ex = Ex + scr4*scr5
    scr4 = Ix*scr4

    call yup_set(scr4, scr3)

    call zdn_set(Bx, scr4)
    call xdn_set(Uza, scr5)
    !loop
    Ey = Ey - scr4*scr5
    scr4 = Iy*scr4

    call xup_sub(scr4, scr3)

!  With ambipolar diffusion, must calculate force before E-field

  else
    call xdn_set(Bz, scr4)
    !loop
    scr4 = Iy*scr4
    call zup_set(scr4, scr1)

    call xdn_set(By, scr4)
    !loop
    scr4 = Iz*scr4
    call yup_sub(scr4, scr1)

    call ydn_set(Bx, scr4)
    !loop
    scr4 = Iz*scr4
    call xup_set(scr4, scr2)

    call ydn_set(Bz, scr4)
    !loop
    scr4 = Ix*scr4
    call zup_sub(scr4, scr2)

    call zdn_set(By, scr4)
    !loop
    scr4 = Ix*scr4
    call yup_set(scr4, scr3)

    call zdn_set(Bx, scr4)
    !loop
    scr4 = Iy*scr4
    call xup_sub(scr4, scr3)
  end if

  !$omp parallel do private(k)
  do k=1,mz
    dUxdt(:,:,k) = dUxdt(:,:,k) + scr1(:,:,k)/xdnr(:,:,k)
    dUydt(:,:,k) = dUydt(:,:,k) + scr2(:,:,k)/ydnr(:,:,k)
    dUzdt(:,:,k) = dUzdt(:,:,k) + scr3(:,:,k)/zdnr(:,:,k)
  end do

  if (ambi .ne. 0.0) then
    !$omp parallel do private(k)
    do k=1,mz
      scr5(:,:,k) = max(abs(Ux(:,:,k)),abs(Uy(:,:,k)),abs(Uz(:,:,k)))
    end do
    Umax=fmaxval('Umax',scr5)

    !$omp parallel do private(k)
    do k=1,mz
      scr5(:,:,k) = ambi*dx*r(:,:,k)**(-1.5)
      scr1(:,:,k)=scr1(:,:,k)*scr5(:,:,k)
      scr4(:,:,k)=scr1(:,:,k)*Umax/(abs(scr1(:,:,k))+Umax)
    end do
    call smooth3_set (scr4,scr1)

    !$omp parallel do private(k)
    do k=1,mz
      scr2(:,:,k)=scr2(:,:,k)*scr5(:,:,k)
      scr4(:,:,k)=scr2(:,:,k)*Umax/(abs(scr2(:,:,k))+Umax)
    end do
    call smooth3_set (scr4,scr2)

    !$omp parallel do private(k)
    do k=1,mz
      scr3(:,:,k)=scr3(:,:,k)*scr5(:,:,k)
      scr4(:,:,k)=scr3(:,:,k)*Umax/(abs(scr3(:,:,k))+Umax)
    end do
    call smooth3_set (scr4,scr3)

    !loop
    px = Uxa + scr1
    py = Uya + scr2
    pz = Uza + scr3

    call xdn_set(Bz, scr4)
    call zdn_set(px, scr5)
    !loop
    Ey = + scr4*scr5

    call xdn_set(By, scr4)
    call ydn_set(px, scr5)
    !loop
    Ez = - scr4*scr5

    call ydn_set(Bx, scr4)
    call xdn_set(py, scr5)
    !loop
    Ez = Ez + scr4*scr5

    call ydn_set(Bz, scr4)
    call zdn_set(py, scr5)
    !loop
    Ex = - scr4*scr5

    call zdn_set(By, scr4)
    call ydn_set(pz, scr5)
    !loop
    Ex = Ex + scr4*scr5

    call zdn_set(Bx, scr4)
    call xdn_set(pz, scr5)
    !loop
    Ey = Ey - scr4*scr5
  else
    !$omp parallel do private(k)
    do k=1,mz
      px(:,:,k) = Uxa(:,:,k)
      py(:,:,k) = Uya(:,:,k)
      pz(:,:,k) = Uza(:,:,k)
    end do
  endif

END SUBROUTINE
!***********************************************************************
SUBROUTINE induction (lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt)
  USE params
  implicit none
  real, dimension(mx,my,mz):: lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt
  real fCb,dtdx,fmaxval

!  Magnetic convergence, in the plane perpendicular to the magnetic field.
!  r1 is used first for the normalization factor, then for the scalar
!  product U*Bhat.

!      bvx = xup(Bx)
!      bvy = yup(By)
!      bvz = zup(Bz)

  call xup_set (Bx, bvx)
  call yup_set (By, bvy)
  call zup_set (Bz, bvz)

  if (gamma .eq. 1.) then
    gg1 = 1.
  else
    gg1 = gamma*(gamma-1)
  endif

  dtdx = dt/min(dx,dy,dz)
  !$omp parallel do private(k)
  do k=1,mz
   r1(:,:,k) = max(bvx(:,:,k)**2 + bvy(:,:,k)**2 + bvz(:,:,k)**2, 1.e-20)
   cs(:,:,k) = sqrt(gg1*ee(:,:,k)+r1(:,:,k)/r(:,:,k))
   scr1(:,:,k) = dtdx*cs(:,:,k)
   cs(:,:,k) = (nu3*nuB)*cs(:,:,k)
   r1(:,:,k) = 1./sqrt(r1(:,:,k))
   bvx(:,:,k) = bvx(:,:,k)*r1(:,:,k)             ! bvi = unit B vector
   bvy(:,:,k) = bvy(:,:,k)*r1(:,:,k)
   bvz(:,:,k) = bvz(:,:,k)*r1(:,:,k)
  end do
  Cb = fmaxval('Cb',scr1)

!      scr1 = xup(px)          ! scri = centered velocity
!      scr2 = yup(py)
!      scr3 = zup(pz)

  call xup_set (px, scr1)
  call yup_set (py, scr2)
  call zup_set (pz, scr3)

  !$omp parallel do private(k)
  do k=1,mz
    r1(:,:,k) = scr1(:,:,k)*bvx(:,:,k) &
              + scr2(:,:,k)*bvy(:,:,k) &
              + scr3(:,:,k)*bvz(:,:,k)
    scr1(:,:,k) = scr1(:,:,k)-bvx(:,:,k)*r1(:,:,k)
    scr2(:,:,k) = scr2(:,:,k)-bvy(:,:,k)*r1(:,:,k)
    scr3(:,:,k) = scr3(:,:,k)-bvz(:,:,k)*r1(:,:,k)
  end do
  call dif2d_set(scr1,scr2,scr3,scr4)
  !loop
  scr4 = (0.5*nu2*nuB)*max(0.0,scr4)

!  Smooth over 3x3x3 point, to broaden influence.  Use a low order shift
!  after smoothing, to ensure positive definite values.

  call max3_set (scr4,scr1)
  call smooth3_set (scr1,scr4)

  !loop
  cs = cs + scr4
!      bvx = dx*((nu1*nuB)*abs(px) + xdn1(cs))
!      bvy = dy*((nu1*nuB)*abs(py) + ydn1(cs))
!      bvz = dz*((nu1*nuB)*abs(pz) + zdn1(cs))
  call xdn1_set (cs, scr1)
  !loop
  bvx = dx*((nu1*nuB)*abs(px) + scr1)
  call ydn1_set (cs, scr1)
  !loop
  bvy = dy*((nu1*nuB)*abs(py) + scr1)
  call zdn1_set (cs, scr1)
  !loop
  bvz = dz*((nu1*nuB)*abs(pz) + scr1)

  fCb = 6.2*3.*dt/2.
  scr1 = fCb*max(bvx/dx**2,bvy/dy**2,bvz/dz**2)
  Cn = fmaxval('Cn',scr1)

  call stats ('bvx', bvx)
  call stats ('bvy', bvy)
  call stats ('bvz', bvz)

!  Induction equation, d(B)/dt = - curl(E) = curl (u x B - eta I)
!  ==============================================================

!  The magnetic field is face centered.  The electric field and current
!  are centered on the cube edges.

!  eta I contribution to the Electric field

!      Ex = Ex + 0.5*(ydn1(bvz)+zdn1(bvy))*Ix
!      Ey = Ey + 0.5*(zdn1(bvx)+xdn1(bvz))*Iy
!      Ez = Ez + 0.5*(xdn1(bvy)+ydn1(bvx))*Iz

  call ydn1_set (bvz, scr1)
  call zdn1_set (bvy, scr2)
  !loop
  Ex = Ex + 0.5*(scr1+scr2)*Ix

  call zdn1_set (bvx, scr1)
  call xdn1_set (bvz, scr2)
  !loop
  Ey = Ey + 0.5*(scr1+scr2)*Iy

  call xdn1_set (bvy, scr1)
  call ydn1_set (bvx, scr2)
  !loop
  Ez = Ez + 0.5*(scr1+scr2)*Iz
END SUBROUTINE
!**********************************************************************
SUBROUTINE jouleheating (lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt)
  USE params
  implicit none
  real, dimension(mx,my,mz):: lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt
  integer, save:: i_step=0
  real Emagn,Ekin,Qjoule

  if (.not. do_energy) return

!  Joule heating added to the energy equation, eta I^2

  scr2 = Ez*Iz
  call yup_set(scr2,scr1)
  scr2 = Ey*Iy
  call zup_add(scr1,scr2)
  call xup_set(scr2,scr1)
  scr2 = Ey*Iy
  call yup_set(scr2,scr3)
  call zup_add(scr3,scr1)

!  scr1 = zup(yup(Ex*Ix)) + xup(zup(Ey*Iy)+yup(Ez*Iz))
  if (idbg>0 .and. mod(i_step,20) .eq. 1) then
    Qjoule = sum(scr1)/mw
    !loop
    scr2 = Bx**2 + By**2 + Bz**2
    Emagn = 0.5*sum(scr2)/mw
    !loop
    scr2 = r*(Ux**2+Uy**2+Uz**2)
    Ekin = 0.5*sum(scr2)/mw
    write(*,'(1x,a,f10.4,3(1pe12.5))') 'joule:',t,Qjoule,Emagn,Ekin
  endif
  i_step = i_step + 1
  !loop
  deedt = deedt + scr1/r

END SUBROUTINE
!***********************************************************************
SUBROUTINE magneticfield (lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt)
  USE params
  implicit none
  real, dimension(mx,my,mz):: lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt

!  Magnetic field 's time derivative, dBdt = - curl(E)

!      dBxdt = ddzup (Ey) - ddyup (Ez)
!      dBydt = ddxup (Ez) - ddzup (Ex)
!      dBzdt = ddyup (Ex) - ddxup (Ey)

  call ptrace ('ddzup_add', 3)
  call ddzup_add(Ey, dBxdt)
  call ptrace ('ddyup_sub', 3)
  call ddyup_sub(Ez, dBxdt)

  call ptrace ('ddxup_add', 3)
  call ddxup_add(Ez, dBydt)
  call ptrace ('ddzup_sub', 3)
  call ddzup_sub(Ex, dBydt)

  call ptrace ('ddyup_add', 3)
  call ddyup_add(Ex, dBzdt)
  call ptrace ('ddxup_sub', 3)
  call ddxup_sub(Ey, dBzdt)

END SUBROUTINE
END MODULE

!***********************************************************************
SUBROUTINE pde ( &
    lnr,Ux,Uy,Uz,ee,d,dlnrdt,dUxdt,dUydt,dUzdt,deedt,dddt,flag, &
    Bx,By,Bz,dBxdt,dBydt,dBzdt)
  USE params
  USE pde_cm
  implicit none
  logical flag
  real average
  real, dimension(mx,my,mz):: &
    lnr,Ux,Uy,Uz,ee,d,dlnrdt,dUxdt,dUydt,dUzdt,deedt,dddt, &
    Bx,By,Bz,dBxdt,dBydt,dBzdt
  character(len=mid):: id='$Id: pde_cm_adv.f90,v 1.7 2006/04/09 06:04:17 aake Exp $'

  call print_id(id)

  ambi=0.
  tcool=0.
  chi=1.

  call stats ('lnr',lnr)
  call stats ('Ux',Ux)
  call stats ('Uy',Uy)
  call stats ('Uz',Uz)
  call stats ('ee',ee)
  call stats ('Bx',Bx)
  call stats ('By',By)
  call stats ('Bz',Bz)
  call alloc
  call diffus          (lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt)
  call diffusivities   (lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt,flag)
  call mass            (lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt)
  call massexact       (lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt)
  call momx            (lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt)
  call momy            (lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt)
  call momz            (lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt)
  if (do_mhd) &
    call Lorentz       (lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt)
  call energy          (lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt)
  call stats ('dlnrdt',dlnrdt)
  call stats ('dUxdt', dUxdt)
  call stats ('dUydt', dUydt)
  call stats ('dUzdt', dUzdt)
  call stats ('deedt', deedt)
  if (do_mhd) then
    call induction     (lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt)
    call jouleheating  (lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt)
    call magneticfield (lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt)
    call stats ('Ix',Ix)
    call stats ('Iy',Iy)
    call stats ('Iz',Iz)
    call stats ('Ex',Ex)
    call stats ('Ey',Ey)
    call stats ('Ez',Ez)
    call stats ('dBxdt', dBxdt)
    call stats ('dBydt', dBydt)
    call stats ('dBzdt', dBzdt)
  endif

  call forceit (r,Ux,Uy,Uz,xdnr,ydnr,zdnr,dUxdt,dUydt,dUzdt)
  call dealloc
END SUBROUTINE

