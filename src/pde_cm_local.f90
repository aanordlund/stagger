! $Id: pde_cm_local.f90,v 1.37 2007/07/12 08:48:39 aake Exp $
!***********************************************************************
MODULE pde_cm
  USE params
  USE arrays, ONLY: scr1, scr2, scr3, scr4, scr5, scr6
!  USE stagger
!  USE df
  implicit none

  real ambi                  ! parameters

  real eemean,gg1            ! temporaries
  integer i,j,k
  integer, save:: idump=0

  real, allocatable, dimension(:,:,:):: &
    r, px, py, pz, &
    xdnr, ydnr, zdnr, r1, &
    lnP, cs, vx, vy, vz, &
    bvx, bvy, bvz, &
    Jx, Jy, Jz, Ex, Ey, Ez

CONTAINS

!***********************************************************************
SUBROUTINE alloc
  allocate( &
    r(mx,my,mz), px(mx,my,mz), py(mx,my,mz), pz(mx,my,mz), &                                          !  3
    xdnr(mx,my,mz), ydnr(mx,my,mz), zdnr(mx,my,mz), r1(mx,my,mz), &                                   !  4
    lnP(mx,my,mz), cs(mx,my,mz), vx(mx,my,mz), vy(mx,my,mz), vz(mx,my,mz), &                          !  5
    Jx(mx,my,mz), Jy(mx,my,mz), Jz(mx,my,mz), ex(mx,my,mz), ey(mx,my,mz), ez(mx,my,mz), &             !  6
    scr1(mx,my,mz), scr2(mx,my,mz), scr3(mx,my,mz), scr4(mx,my,mz), scr5(mx,my,mz), scr6(mx,my,mz) &  !  6
  )                                                                                                   ! 27
END SUBROUTINE

!***********************************************************************
SUBROUTINE dealloc
  deallocate( &
    r, px, py, pz, &
    xdnr, ydnr, zdnr, r1, &
    lnP, cs, vx, vy, vz, &
    Jx, Jy, Jz, ex, ey, ez, &
    scr1, scr2, scr3, scr4, scr5, scr6 &
  )
END SUBROUTINE

!***********************************************************************
SUBROUTINE stats (label, a)
  implicit none
  character*(*) label
  real, dimension (mx,my,mz):: a
  logical exist
  integer,save:: irec=0
  integer lrec
  character(len=16):: file

  inquire (file='stat.txt',exist=exist)
  if (exist) then
    !open (15,file='stat.txt',status='old')
    if (do_trace) write (*,*) irec,label
    write (15,*) irec,label
    file='stat.dat'
    call mpi_name(file)
    open (16,file=file,status='unknown',access='direct',recl=lrec(mw))
    write (16,rec=irec+1) a
    close (16)
    irec=irec+1
    !close (15)
  end if
  if (idbg.eq.0) return
  if ((.not.master) .and. (idbg.eq.1)) return
  write (*,'(1x,i4,1x,a,5(1pe11.3))') mpi_rank,label,minval(a),sum(a)/mw,maxval(a)
END SUBROUTINE

!***********************************************************************
SUBROUTINE ptrace (label, level)
  implicit none
  integer level
  character*(*) label

  if (idbg .ge. level) then
    write (*,*) label
    call oflush
  end if
END SUBROUTINE

!***********************************************************************
SUBROUTINE diffus (lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt)
  USE eos
  implicit none
  integer iz
  real, dimension(mx,my,mz):: lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt

!  Face centered velocities

  !$omp parallel do private(iz)
  do iz=1,mz
    r(:,:,iz) = exp(lnr(:,:,iz))
  end do

  call xdn_set_par (lnr, xdnr)
  call ydn_set_par (lnr, ydnr)
  call zdn_set_par (lnr, zdnr)

  !$omp parallel do private(iz)
  do iz=1,mz
    xdnr(:,:,iz)=exp(xdnr(:,:,iz))
    ydnr(:,:,iz)=exp(ydnr(:,:,iz))
    zdnr(:,:,iz)=exp(zdnr(:,:,iz))
    px(:,:,iz) = Ux(:,:,iz)*xdnr(:,:,iz)
    py(:,:,iz) = Uy(:,:,iz)*ydnr(:,:,iz)
    pz(:,:,iz) = Uz(:,:,iz)*zdnr(:,:,iz)
  end do

!  Temperature.  For Mach 10 or higher, even a single deedt*dt
!  can make the temperature negative!  Hence reset to constant.

  if (gamma .eq. 1.) then
    gg1 = 1.
  else
    gg1 = gamma*(gamma-1)
  endif
  call equation_of_state (r,ee,lnP)

!  Pressure.  For perfect gas, P = e_th * (gamma-1)

  !$omp parallel do private(iz)
  do iz=1,mz
    r1(:,:,iz) = ee(:,:,iz)*(gg1/gamma)               ! r1=P/rho
    lnP(:,:,iz) = alog(lnP(:,:,iz))
    cs(:,:,iz) = nu3*sqrt(gg1*ee(:,:,iz))
  end do
  call stats ('r', r)
  call stats ('ee', ee)
  call dif1_set_par (Ux, Uy, Uz, scr2)
  !$omp parallel do private(iz)
  do iz=1,mz
    scr2(:,:,iz) = amax1(scr2(:,:,iz),0.)
  end do
  call stats ('difU', scr2)

!  Smooth over 3x3x3 point, to broaden influence.  Use a low order shift
!  after smoothing, to ensure positive definite values.

  call dumpl(scr2,'scr2','pde.dmp',idump); idump=idump+1
  call max3_set_par (scr2,scr1)
  call smooth3_set_par (scr1,scr2)
  call stats ('difUsm', scr2)
  call dumpl(scr2,'scr2','pde.dmp',idump); idump=idump+1

END SUBROUTINE
!************************************************************************
SUBROUTINE diffusivities (lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt,flag)
  USE params
  implicit none
  integer iz
  real, dimension(mx,my,mz):: lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt
  real average,fmaxval,fCv
  logical flag

!  Diffusivities

  !$omp parallel do private(iz)
  do iz=1,mz
    cs(:,:,iz) = cs(:,:,iz)+nu2*scr2(:,:,iz)
  end do
  call xdn1_set_par (cs, scr1)
  !$omp parallel do private(iz)
  do iz=1,mz
    vx(:,:,iz) = (scr1(:,:,iz) + nu1*abs(Ux(:,:,iz)))
  end do
  call ydn1_set_par (cs, scr1)
  !$omp parallel do private(iz)
  do iz=1,mz
    vy(:,:,iz) = (scr1(:,:,iz) + nu1*abs(Uy(:,:,iz)))
  end do
  call zdn1_set_par (cs, scr1)
  !$omp parallel do private(iz)
  do iz=1,mz
    vz(:,:,iz) = (scr1(:,:,iz) + nu1*abs(Uz(:,:,iz)))
  end do

  call stats ('vx', vx)
  call stats ('vy', vy)
  call stats ('vz', vz)

  if (flag) then
    !$omp parallel do private(iz)
    do iz=1,mz
      scr1(:,:,iz) = Ux(:,:,iz)**2 + Uy(:,:,iz)**2 + Uz(:,:,iz)**2
    end do
    Urms = sqrt(average(scr1))
    !$omp parallel do private(k)
    do k=1,mz
      scr1(:,:,k) = sqrt(scr1(:,:,k) + gg1*ee(:,:,k))
    end do
    !$omp parallel do private(k,j,i)
    do k=1,mz
    do j=1,my
    do i=1,mx
      scr1(i,j,k) = scr1(i,j,k)*(dt/min(dxm(i),dym(j),dzm(k)))
    end do
    end do
    end do
    Cu = fmaxval('Cu',scr1)

!-----------------------------------------------------------------------
!  Viscosity Courand condition factors:
!     6.2 : the maximum value returned from ddxup(ddxdn(f)) when dx=1.
!      3. : for the worst case of a 3-D checker board
!      2. : overshoot with a factor 2. at Cdtd=1., for marginal stability
!-----------------------------------------------------------------------
    fCv = 6.2*3.*dt/2.

    !$omp parallel do private(k,j,i)
    do k=1,mz
    do j=1,my
    do i=1,mx
      scr1(i,j,k) = fCv*max(vx(i,j,k)/dxm(i),vy(i,j,k)/dym(j),vz(i,j,k)/dzm(k))
    end do
    end do
    end do
    Cv = fmaxval('Cv',scr1)
  end if
  !$omp parallel do private(k,j,i)
  do k=1,mz
  do j=1,my
  do i=1,mx
    vx(i,j,k) = vx(i,j,k)*dxm(i)
    vy(i,j,k) = vy(i,j,k)*dym(j)
    vz(i,j,k) = vz(i,j,k)*dzm(k)
  end do
  end do
  end do
END SUBROUTINE

!***********************************************************************
SUBROUTINE mass (lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt)
  USE params
  implicit none
  integer iz
  real, dimension(mx,my,mz):: lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt

!-----------------------------------------------------------------------

!  Mass conservation, dlnM/dt = - div (U)
!  ====================================

!  High order diffusive term necessary for low-beta plasmas, where a
!  saw-tooth density cannot drive velocity via gas pressure.  Does no
!  harm for high-beta plasmas.

!      dlnrdt = - ddxup(Ux) - ddyup(Uy) - ddzup(Uz)


  call ddxup_neg_par (Ux, scr1)
  call ddyup_sub_par (Uy, scr1)
  call ddzup_sub_par (Uz, scr1)
  !$omp parallel do private(iz)
  do iz=1,mz
    dlnrdt(:,:,iz) = dlnrdt(:,:,iz) + scr1(:,:,iz)
  end do
  call dump (dlnrdt, 'dlnrdt.dat', 0)
  call stats ('dlnrdt: divU', dlnrdt)

END SUBROUTINE

!***********************************************************************
SUBROUTINE mass1 (lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt)
  USE params
  implicit none
  integer iz
  real, dimension(mx,my,mz):: lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt

!      dlnrdt = dlnrdt
!     &         - xup(Ux*scr1)
!     &         - yup(Uy*scr2)
!     &         - zup(Uz*scr3)

  call ddxdn_set_par( lnr, scr1)
  call ddydn_set_par( lnr, scr2)
  call ddzdn_set_par( lnr, scr3)

  !$omp parallel do private(iz)
  do iz=1,mz
    scr4(:,:,iz) = Ux(:,:,iz)*scr1(:,:,iz)
  end do
  call xup_sub_par(scr4, dlnrdt)
  call dump (dlnrdt, 'dlnrdt.dat', 1)
  !$omp parallel do private(iz)
  do iz=1,mz
    scr4(:,:,iz) = Uy(:,:,iz)*scr2(:,:,iz)
  end do
  call yup_sub_par(scr4, dlnrdt)
  call dump (dlnrdt, 'dlnrdt.dat', 2)
  !$omp parallel do private(iz)
  do iz=1,mz
    scr4(:,:,iz) = Uz(:,:,iz)*scr3(:,:,iz)
  end do
  call zup_sub_par(scr4, dlnrdt)
  call dump (dlnrdt, 'dlnrdt.dat', 3)

  call stats ('dlnrdt w u*grad', dlnrdt)
  call dump (dlnrdt, 'dlnrdt.dat', 4)
END SUBROUTINE

!***********************************************************************
SUBROUTINE mass2 (lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt)
  USE params
  implicit none
  integer iz
  real, dimension(mx,my,mz):: lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt

!      dlnrdt = dlnrdt
!     &         +     ddxup(vx*scr1)
!     &         +     ddyup(vy*scr2)
!     &         +     ddzup(vz*scr3)

  !$omp parallel do private(iz)
  do iz=1,mz
    scr4(:,:,iz) = nur*vx(:,:,iz)*scr1(:,:,iz)
  end do
  call ddxup_set_par(scr4, scr6)

  !$omp parallel do private(iz)
  do iz=1,mz
    scr4(:,:,iz) = nur*vy(:,:,iz)*scr2(:,:,iz)
  end do
  call ddyup_add_par(scr4, scr6)

  !$omp parallel do private(iz)
  do iz=1,mz
    scr4(:,:,iz) = nur*vz(:,:,iz)*scr3(:,:,iz)
  end do
  call ddzup_add_par(scr4, scr6)

  call stats ('dlnrdt: diffusion 1', scr6)

!      dlnrdt = dlnrdt
!     &     + xup(scr1)**2*xup(vx)
!     &     + yup(scr2)**2*yup(vy)
!     &     + zup(scr3)**2*zup(vz)

  call xup_set_par( scr1, scr4)
  !$omp parallel do private(iz)
  do iz=1,mz
    scr1(:,:,iz) = nur*vx(:,:,iz)
  end do
  call xup_set_par( scr1, scr5)
  !$omp parallel do private(iz)
  do iz=1,mz
    scr6(:,:,iz) = scr6(:,:,iz) + scr4(:,:,iz)**2*scr5(:,:,iz)
  end do

  call yup_set_par( scr2, scr4)
  !$omp parallel do private(iz)
  do iz=1,mz
    scr2(:,:,iz) = nur*vy(:,:,iz)
  end do
  call yup_set_par( scr2, scr5)
  !$omp parallel do private(iz)
  do iz=1,mz
    scr6(:,:,iz) = scr6(:,:,iz) + scr4(:,:,iz)**2*scr5(:,:,iz)
  end do

  call zup_set_par( scr3, scr4)
  !$omp parallel do private(iz)
  do iz=1,mz
    scr3(:,:,iz) = nur*vz(:,:,iz)
  end do
  call zup_set_par( scr3, scr5)
  !$omp parallel do private(iz)
  do iz=1,mz
    scr6(:,:,iz) = scr6(:,:,iz) + scr4(:,:,iz)**2*scr5(:,:,iz)
  end do

  !$omp parallel do private(iz)
  do iz=1,mz
    dlnrdt(:,:,iz) = dlnrdt(:,:,iz) + scr6(:,:,iz)
  end do
  call dump (dlnrdt, 'dlnrdt.dat', 5)

  call stats ('dlnrdt: diffusion 2', scr6)
  call stats ('dlnrdt w diffusion', dlnrdt)

!  Mass flux compensation terms

  !call xdn1_set_par(scr6, scr1)
  !call ydn1_set_par(scr6, scr2)
  !call zdn1_set_par(scr6, scr3)

  !loop
  !dUxdt = dUxdt - Ux*scr1
  !dUydt = dUydt - Uy*scr2
  !dUzdt = dUzdt - Uz*scr3

END SUBROUTINE
!*********************************************************************
SUBROUTINE massexact (lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt)
  USE params
  implicit none
  integer iz
  real, dimension(mx,my,mz):: lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt
  real, save:: dlnrm=0.
  real average
  real rm, drdtm
  integer, save:: i_step=0
  integer n_step

!--------------------------------------------------------------------

!  Make mass conservation exact:  Add the smoothed difference btw
!  the per volume time derivative and the time derivative from
!  logarithmic vars.  Important to include diffusion also in
!  the formula below, to minimize the difference (should only
!  be truncation errors).

   n_step=15
   if (abs(mod(i_step,n_step)-1) .le. 1) then
     call average_subr (r, rm)
     !$omp parallel do private(iz)
     do iz=1,mz
       scr1(:,:,iz) = r(:,:,iz)*dlnrdt(:,:,iz)
     end do
     call average_subr (scr1, drdtm)
     dlnrm = drdtm/rm
     if (mod(i_step,n_step) .eq. 0 .and. master) print *,'rm,dlnrm=',rm,dlnrm,drdtm
   endif
   i_step = i_step+1
   !$omp parallel do private(iz)
   do iz=1,mz
     dlnrdt(:,:,iz) = dlnrdt(:,:,iz)-dlnrm
   end do
   call dump (dlnrdt, 'dlnrdt.dat', 6)

!c      scr1 =
!c     &      - ddxup(px - xdn(r)*vx*ddxdn(lnr))
!c     &      - ddyup(py - ydn(r)*vy*ddydn(lnr))
!c     &      - ddzup(pz - zdn(r)*vz*ddzdn(lnr))
!c      scr1 =
!c     &      - ddxup1(px - vx*ddxdn1(r))
!c     &      - ddyup1(py - vy*ddydn1(r))
!c     &      - ddzup1(pz - vz*ddzdn1(r))
!c      scr1 = scr1 - r*dlnrdt
!c      call smooth5 (scr1)
!c      dlnrdt = dlnrdt + scr1/r
END SUBROUTINE
!***********************************************************************
SUBROUTINE momx (lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt)
  USE params
  implicit none
  integer iz
  real, dimension(mx,my,mz):: lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt

!      dUxdt = - xdn(r1)*ddxdn(lnP)
!     &      - Ux*xdn(ddxup(Ux))
!     &      - yup(Uy*ddydn(Ux))
!     &      - zup(Uz*ddzdn(Ux))


  call ddxdn_set_par (lnP, scr1)
  call xdn_set_par (r1, scr2)
  call ddxup_set_par (Ux, scr3)
  call xdn_set_par (scr3, scr4)
  !$omp parallel do private(iz)
  do iz=1,mz
    dUxdt(:,:,iz) = dUxdt(:,:,iz) - scr1(:,:,iz)*scr2(:,:,iz) - Ux(:,:,iz)*scr4(:,:,iz)
  end do

  call ddydn_set_par (Ux, scr5)
  call xdn_set_par (Uy, scr6)
  !$omp parallel do private(iz)
  do iz=1,mz
    scr4(:,:,iz) = scr6(:,:,iz)*scr5(:,:,iz)
  end do
  call yup_sub_par (scr4, dUxdt)

  call ddzdn_set_par (Ux, scr6)
  call xdn_set_par (Uz, scr1)
  !$omp parallel do private(iz)
  do iz=1,mz
    scr4(:,:,iz) = scr1(:,:,iz)*scr6(:,:,iz)
  end do
  call zup_sub_par (scr4, dUxdt)

!      scr1 = xup1(vx)*ddxup(Ux)

!      call ddxup_set_par (Ux, scr3)       ! already computed
  call xup1_set_par (vx, scr2)
  !$omp parallel do private(iz)
  do iz=1,mz
    scr1(:,:,iz) = scr3(:,:,iz)*scr2(:,:,iz)
  end do

!      dUxdt = dUxdt + ddxdn(scr1) + ddxdn(lnr)*xdn(scr1)

  call ddxdn_add_par (scr1, dUxdt)

  call ddxdn_set_par (lnr, scr2)
  call xdn_set_par (scr1, scr3)
  !$omp parallel do private(iz)
  do iz=1,mz
    dUxdt(:,:,iz) = dUxdt(:,:,iz) + scr2(:,:,iz)*scr3(:,:,iz)
  end do

END SUBROUTINE
!***********************************************************************
SUBROUTINE momxsymmetrized (lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt)
  USE params
  implicit none
  integer iz
  real, dimension(mx,my,mz):: lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt

!  Symmetrized
!      scr2 = 0.5*(xdn1(vy)*ddydn(Ux) +
!     &            ydn1(vx)*ddxdn(Uy))
!      scr3 = 0.5*(xdn1(vz)*ddzdn(Ux) +
!     &            zdn1(vx)*ddxdn(Uz))

!      call ddydn_set_par (Ux, scr5)      ! already computed
  call xdn1_set_par(vy, scr4)
  call ddxdn_set_par (Uy, scr1)       ! try to save for later
  call ydn1_set_par(vx, scr2)
  !$omp parallel do private(iz)
  do iz=1,mz
    scr2(:,:,iz) = 0.25*(scr4(:,:,iz)+scr2(:,:,iz))*(scr1(:,:,iz)+scr5(:,:,iz))
  end do
  call dumpl(scr2,'scr2','pde.dmp',idump); idump=idump+1

!      call ddzdn_set_par (Ux, scr6)       ! already computed
  call xdn1_set_par(vz, scr4)
  call ddxdn_set_par (Uz, scr5)        ! try to save for later
  call zdn1_set_par(vx, scr1)
  !$omp parallel do private(iz)
  do iz=1,mz
    scr3(:,:,iz) = 0.25*(scr4(:,:,iz)+scr1(:,:,iz))*(scr6(:,:,iz)+scr5(:,:,iz))
  end do
  call dumpl(scr3,'scr3','pde.dmp',idump); idump=idump+1

 END SUBROUTINE
!***********************************************************************
SUBROUTINE momx1 (lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt)
  USE params
  implicit none
  integer iz
  real, dimension(mx,my,mz):: lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt

!      scr4 = xdn(lnr)
  call xdn_set_par (lnr, scr4)

!      dUxdt = dUxdt
!     &              + ddyup(scr2) + yup(ddydn(scr4)*scr2)
!     &              + ddzup(scr3) + zup(ddzdn(scr4)*scr3)

  call ddyup_add_par (scr2, dUxdt)

  call ddydn_set_par (scr4, scr1)
  !$omp parallel do private(iz)
  do iz=1,mz
    scr1(:,:,iz) = scr1(:,:,iz)*scr2(:,:,iz)
  end do
  call yup_add_par (scr1, dUxdt)

  call ddzup_add_par (scr3, dUxdt)

  call ddzdn_set_par (scr4, scr1)
  !$omp parallel do private(iz)
  do iz=1,mz
    scr1(:,:,iz) = scr1(:,:,iz)*scr3(:,:,iz)
  end do
  call zup_add_par (scr1, dUxdt)
 END SUBROUTINE
!***********************************************************************
SUBROUTINE momx2 (lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt)
  USE params    
  implicit none
  integer iz
  real, dimension(mx,my,mz):: lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt

!      scr5 = ydn(lnr)
  call ydn_set_par (lnr, scr5)

!      dUydt =
!     &              + ddxup(scr2) + xup(ddxdn(scr5)*scr2)

  call ddxup_add_par (scr2, dUydt)

  call ddxdn_set_par (scr5, scr1)
  !$omp parallel do private(iz)
  do iz=1,mz
    scr1(:,:,iz) = scr1(:,:,iz)*scr2(:,:,iz)
  end do
  call xup_add_par (scr1, dUydt)

!      scr6 = zdn(lnr)
  call zdn_set_par (lnr, scr6)

!      dUzdt =
!     &              + ddxup(scr3) + xup(ddxdn(scr6)*scr3)

  call ddxup_add_par (scr3, dUzdt)

  call ddxdn_set_par (scr6, scr1)
  !$omp parallel do private(iz)
  do iz=1,mz
    scr1(:,:,iz) = scr1(:,:,iz)*scr3(:,:,iz)
  end do
  call xup_add_par (scr1, dUzdt)
END SUBROUTINE
!***********************************************************************
SUBROUTINE momy (lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt)
  USE params
  implicit none
  integer iz
  real, dimension(mx,my,mz):: lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt

!      scr1 = yup1(vy)*ddyup(Uy)

  call ddyup_set_par (Uy, scr4)
  call yup1_set_par (vy, scr2)
  !$omp parallel do private(iz)
  do iz=1,mz
    scr1(:,:,iz) = scr4(:,:,iz)*scr2(:,:,iz)
  end do

!      dUydt = dUydt - ydn(r1)*ddydn(lnP)
!     &      - Uy*ydn(ddyup(Uy))
!     &      - zup(Uz*ddzdn(Uy))
!     &      - xup(Ux*ddxdn(Uy))
!     &      + ddydn(scr1) + ddydn(lnr)*ydn(scr1)

  call ddydn_add_par (scr1, dUydt)

  call ydn_set_par (scr1, scr2)
  call ddydn_set_par (lnr, scr3)
  !$omp parallel do private(iz)
  do iz=1,mz
    dUydt(:,:,iz) = dUydt(:,:,iz) + scr3(:,:,iz)*scr2(:,:,iz)
  end do

  call ddydn_set_par (lnP, scr1)
  call ydn_set_par (r1, scr2)
  !$omp parallel do private(iz)
  do iz=1,mz
    dUydt(:,:,iz) = dUydt(:,:,iz) - scr1(:,:,iz)*scr2(:,:,iz)
  end do

!      call ddyup_set_par (Uy, scr4)       ! already computed
  call ydn_set_par (scr4, scr2)
  !$omp parallel do private(iz)
  do iz=1,mz
    dUydt(:,:,iz) = dUydt(:,:,iz) - Uy(:,:,iz)*scr2(:,:,iz)
  end do

  call ddzdn_set_par (Uy, scr4)
  call ydn_set_par (Uz, scr2)
  !$omp parallel do private(iz)
  do iz=1,mz
    scr1(:,:,iz) = scr4(:,:,iz)*scr2(:,:,iz)
  end do
  call zup_sub_par (scr1, dUydt)

  call ddxdn_set_par (Uy, scr1)
  call ydn_set_par (Ux, scr2)
  !$omp parallel do private(iz)
  do iz=1,mz
    scr1(:,:,iz) = scr1(:,:,iz)*scr2(:,:,iz)
  end do
  call xup_sub_par (scr1, dUydt)

END SUBROUTINE
!**********************************************************************
SUBROUTINE momysymmetrized (lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt)
  USE params
  implicit none
  integer iz
  real, dimension(mx,my,mz):: lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt

!  Symmetrized

!      scr2 = 0.25*(ydn1(vz)*ddzdn(Uy) +
!     &            zdn1(vy)*ddydn(Uz))

  call zdn1_set_par(vy, scr1)
  call ydn1_set_par(vz, scr2)
  !$omp parallel do private(iz)
  do iz=1,mz
    scr2(:,:,iz) = scr1(:,:,iz)+scr2(:,:,iz)
  end do
  call ddydn_set_par (Uz, scr3)
!      call ddzdn_set_par (Uy, scr4)       ! already computed
  !$omp parallel do private(iz)
  do iz=1,mz
    scr2(:,:,iz) = 0.25*scr2(:,:,iz)*(scr4(:,:,iz)+scr3(:,:,iz))
  end do
END SUBROUTINE
!***********************************************************************
SUBROUTINE momyduydt (lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt)
  USE params
  implicit none
  integer iz
  real, dimension(mx,my,mz):: lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt

!      dUydt = dUydt
!     &              + ddzup(scr2) + zup(ddzdn(scr5)*scr2)

  call ddzup_add_par (scr2, dUydt)

  call ddzdn_set_par (scr5, scr1)
  !$omp parallel do private(iz)
  do iz=1,mz
    scr1(:,:,iz) = scr1(:,:,iz)*scr2(:,:,iz)
  end do
  call zup_add_par(scr1, dUydt)

!      dUzdt = dUzdt
!     &              + ddyup(scr2) + yup(ddydn(scr6)*scr2)

  call ddyup_add_par (scr2, dUzdt)

  call ddydn_set_par (scr6, scr1)
  !$omp parallel do private(iz)
  do iz=1,mz
    scr1(:,:,iz) = scr1(:,:,iz)*scr2(:,:,iz)
  end do
  call yup_add_par(scr1, dUzdt)
END SUBROUTINE
!***********************************************************************
SUBROUTINE momz (lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt)
  USE params
  implicit none
  integer iz
  real, dimension(mx,my,mz):: lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt

!      dUzdt = dUzdt - zdn(r1)*ddzdn(lnP)
!     &      - Uz*zdn(ddzup(Uz))
!     &      - xup(Ux*ddxdn(Uz))
!     &      - yup(Uy*ddydn(Uz))

  call ddzdn_set_par (lnP, scr1)
  call zdn_set_par (r1, scr2)
  !$omp parallel do private(iz)
  do iz=1,mz
    dUzdt(:,:,iz) = dUzdt(:,:,iz) - scr2(:,:,iz)*scr1(:,:,iz)
  end do

  call ddzup_set_par (Uz, scr4)
  call zdn_set_par (scr4, scr2)
  !$omp parallel do private(iz)
  do iz=1,mz
    dUzdt(:,:,iz) = dUzdt(:,:,iz) -Uz(:,:,iz)*scr2(:,:,iz)
  end do

  call ddxdn_set_par (Uz, scr1)
  call zdn_set_par (Ux, scr2)
  !$omp parallel do private(iz)
  do iz=1,mz
    scr1(:,:,iz) = scr2(:,:,iz)*scr1(:,:,iz)
  end do
  call xup_sub_par (scr1, dUzdt)

!      call ddydn_set_par (Uz, scr3)      ! already computed
  call zdn_set_par (Uy, scr2)
  !$omp parallel do private(iz)
  do iz=1,mz
    scr3(:,:,iz) = scr2(:,:,iz)*scr3(:,:,iz)
  end do
  call yup_sub_par (scr3, dUzdt)

!      scr1 = zup1(vz)*ddzup(Uz)

!      call ddzup_set_par (Uz, scr4)      ! already computed
  call zup1_set_par (vz, scr2)
  !$omp parallel do private(iz)
  do iz=1,mz
    scr1(:,:,iz) = scr2(:,:,iz)*scr4(:,:,iz)
  end do

END SUBROUTINE
!***********************************************************************
SUBROUTINE momzduzdt (lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt)
  USE params
  implicit none
  integer iz
  real, dimension(mx,my,mz):: lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt

!      dUzdt = dUzdt + ddzdn(scr1) + ddzdn(lnr)*zdn(scr1)

  call ddzdn_add_par (scr1, dUzdt)
  call zdn_set_par   (scr1, scr2)
  call ddzdn_set_par (lnr, scr1)
  !$omp parallel do private(iz)
  do iz=1,mz
     dUzdt(:,:,iz) = dUzdt(:,:,iz) + scr1(:,:,iz)*scr2(:,:,iz)
  end do
END SUBROUTINE
!***********************************************************************
SUBROUTINE energy (lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt)
  USE params
  implicit none
  integer iz
  real, dimension(mx,my,mz):: lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt

!-----------------------------------------------------------------------

!  Energy balance, d(e)/dt = - div (eU) - P div(U)
!  ===============================================


!  Note: the P/r*div(U) in done in mass

!        deedt = deedt
!     &     - xup(Ux*ddxdn(ee))
!     &     - yup(Uy*ddydn(ee))
!     &     - zup(Uz*ddzdn(ee))
!     &     +(ddxup(vx*ddxdn(ee))
!     &     + ddyup(vy*ddydn(ee))
!     &     + ddzup(vz*ddzdn(ee))
!     &      )

  if (do_energy) then
    call ddxdn_set_par(ee, scr1)
    call ddydn_set_par(ee, scr2)
    call ddzdn_set_par(ee, scr3)
    !$omp parallel do private(iz)
    do iz=1,mz
      scr4(:,:,iz) = vx(:,:,iz)*scr1(:,:,iz)
      scr5(:,:,iz) = vy(:,:,iz)*scr2(:,:,iz)
      scr6(:,:,iz) = vz(:,:,iz)*scr3(:,:,iz)
      scr1(:,:,iz) = Ux(:,:,iz)*scr1(:,:,iz)
      scr2(:,:,iz) = Uy(:,:,iz)*scr2(:,:,iz)
      scr3(:,:,iz) = Uz(:,:,iz)*scr3(:,:,iz)
    end do
    call xup_sub_par (scr1, deedt)
    call yup_sub_par (scr2, deedt)
    call zup_sub_par (scr3, deedt)
    call ddxup_add_par (scr4, deedt)
    call ddyup_add_par (scr5, deedt)
    call ddzup_add_par (scr6, deedt)
  end if
END SUBROUTINE
  
!***********************************************************************
SUBROUTINE Lorentz (lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt)
  USE params
  implicit none
  integer iz
  real, dimension(mx,my,mz):: lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt
  real Umax,fmaxval
  integer k

!  Current = curl(B)

!      Jx = ddydn(Bz) - ddzdn(By)
!      Jy = ddxdn(Bz)
!      Jz = ddxdn(By)
!      Jy = ddzdn(Bx) - Jy
!      Jz = Jz - ddydn(Bx)

  call ddydn_set_par (Bz, Jx)
  call ddzdn_sub_par (By, Jx)

  call ddzdn_set_par (Bx, Jy)
  call ddxdn_sub_par (Bz, Jy)

  call ddxdn_set_par (By, Jz)
  call ddydn_sub_par (Bx, Jz)

!  Lorentz force JxB.  Add ambipolar diffusion speed directly to
!  Ui, since Ui is only used in the induction equation after this.
!  Scale the coefficient with dx, to make the effect scale-indep.

!      scr1 = zup(Jy*xdn(Bz)) - yup(Jz*xdn(By))
!      scr2 = xup(Jz*ydn(Bx)) - zup(Jx*ydn(Bz))
!      scr3 = yup(Jx*zdn(By)) - xup(Jy*zdn(Bx))

!  Electric field   E = - uxB + eta J

!      Ex = Ex - zdn(py)*ydn(Bz) + ydn(pz)*zdn(By)
!      Ey = Ey - xdn(pz)*zdn(Bx) + zdn(px)*xdn(Bz)
!      Ez = Ez - ydn(px)*xdn(By) + xdn(py)*ydn(Bx)

!      Ex =  - zdn(Uy)*ydn(Bz) + ydn(Uz)*zdn(By)
!      Ey =  - xdn(Uz)*zdn(Bx) + zdn(Ux)*xdn(Bz)
!      Ez =  - ydn(Ux)*xdn(By) + xdn(Uy)*ydn(Bx)

  if (ambi .eq. 0.0) then
    call xdn_set_par(Bz, scr4)
    call zdn_set_par(Ux, scr5)
    !$omp parallel do private(iz)
    do iz=1,mz
      Ey(:,:,iz) = + scr4(:,:,iz)*scr5(:,:,iz)
      scr4(:,:,iz) = Jy(:,:,iz)*scr4(:,:,iz)
    end do

    call zup_set_par(scr4, scr1)

    call xdn_set_par(By, scr4)
    call ydn_set_par(Ux, scr5)
    !$omp parallel do private(iz)
    do iz=1,mz
      Ez(:,:,iz) = - scr4(:,:,iz)*scr5(:,:,iz)
      scr4(:,:,iz) = Jz(:,:,iz)*scr4(:,:,iz)
    end do

    call yup_sub_par(scr4, scr1)

    call ydn_set_par(Bx, scr4)
    call xdn_set_par(Uy, scr5)
    !$omp parallel do private(iz)
    do iz=1,mz
      Ez(:,:,iz) = Ez(:,:,iz) + scr4(:,:,iz)*scr5(:,:,iz)
      scr4(:,:,iz) = Jz(:,:,iz)*scr4(:,:,iz)
    end do

    call xup_set_par(scr4, scr2)

    call ydn_set_par(Bz, scr4)
    call zdn_set_par(Uy, scr5)
    !$omp parallel do private(iz)
    do iz=1,mz
      Ex(:,:,iz) = - scr4(:,:,iz)*scr5(:,:,iz)
      scr4(:,:,iz) = Jx(:,:,iz)*scr4(:,:,iz)
    end do

    call zup_sub_par(scr4, scr2)

    call zdn_set_par(By, scr4)
    call ydn_set_par(Uz, scr5)
    !$omp parallel do private(iz)
    do iz=1,mz
      Ex(:,:,iz) = Ex(:,:,iz) + scr4(:,:,iz)*scr5(:,:,iz)
      scr4(:,:,iz) = Jx(:,:,iz)*scr4(:,:,iz)
    end do

    call yup_set_par(scr4, scr3)

    call zdn_set_par(Bx, scr4)
    call xdn_set_par(Uz, scr5)
    !$omp parallel do private(iz)
    do iz=1,mz
      Ey(:,:,iz) = Ey(:,:,iz) - scr4(:,:,iz)*scr5(:,:,iz)
      scr4(:,:,iz) = Jy(:,:,iz)*scr4(:,:,iz)
    end do

    call xup_sub_par(scr4, scr3)

!  With ambipolar diffusion, must calculate force before E-field

  else
    call xdn_set_par(Bz, scr4)
    !$omp parallel do private(iz)
    do iz=1,mz
      scr4(:,:,iz) = Jy(:,:,iz)*scr4(:,:,iz)
    end do
    call zup_set_par(scr4, scr1)

    call xdn_set_par(By, scr4)
    !$omp parallel do private(iz)
    do iz=1,mz
      scr4(:,:,iz) = Jz(:,:,iz)*scr4(:,:,iz)
    end do
    call yup_sub_par(scr4, scr1)

    call ydn_set_par(Bx, scr4)
    !$omp parallel do private(iz)
    do iz=1,mz
      scr4(:,:,iz) = Jz(:,:,iz)*scr4(:,:,iz)
    end do
    call xup_set_par(scr4, scr2)

    call ydn_set_par(Bz, scr4)
    !$omp parallel do private(iz)
    do iz=1,mz
      scr4(:,:,iz) = Jx(:,:,iz)*scr4(:,:,iz)
    end do
    call zup_sub_par(scr4, scr2)

    call zdn_set_par(By, scr4)
    !$omp parallel do private(iz)
    do iz=1,mz
      scr4(:,:,iz) = Jx(:,:,iz)*scr4(:,:,iz)
    end do
    call yup_set_par(scr4, scr3)

    call zdn_set_par(Bx, scr4)
    !$omp parallel do private(iz)
    do iz=1,mz
      scr4(:,:,iz) = Jy(:,:,iz)*scr4(:,:,iz)
    end do
    call xup_sub_par(scr4, scr3)
  end if

  !$omp parallel do private(k)
  do k=1,mz
    dUxdt(:,:,k) = dUxdt(:,:,k) + scr1(:,:,k)/xdnr(:,:,k)
    dUydt(:,:,k) = dUydt(:,:,k) + scr2(:,:,k)/ydnr(:,:,k)
    dUzdt(:,:,k) = dUzdt(:,:,k) + scr3(:,:,k)/zdnr(:,:,k)
  end do

  if (ambi .ne. 0.0) then
    !$omp parallel do private(k)
    do k=1,mz
      scr5(:,:,k) = max(abs(Ux(:,:,k)),abs(Uy(:,:,k)),abs(Uz(:,:,k)))
    end do
    Umax=fmaxval('Umax',scr5)

    !$omp parallel do private(k)
    do k=1,mz
      scr5(:,:,k) = ambi*dx*r(:,:,k)**(-1.5)
      scr1(:,:,k)=scr1(:,:,k)*scr5(:,:,k)
      scr4(:,:,k)=scr1(:,:,k)*Umax/(abs(scr1(:,:,k))+Umax)
    end do
    call smooth3_set_par (scr4,scr1)

    !$omp parallel do private(k)
    do k=1,mz
      scr2(:,:,k)=scr2(:,:,k)*scr5(:,:,k)
      scr4(:,:,k)=scr2(:,:,k)*Umax/(abs(scr2(:,:,k))+Umax)
    end do
    call smooth3_set_par (scr4,scr2)

    !$omp parallel do private(k)
    do k=1,mz
      scr3(:,:,k)=scr3(:,:,k)*scr5(:,:,k)
      scr4(:,:,k)=scr3(:,:,k)*Umax/(abs(scr3(:,:,k))+Umax)
    end do
    call smooth3_set_par (scr4,scr3)

    !$omp parallel do private(iz)
    do iz=1,mz
      px(:,:,iz) = Ux(:,:,iz) + scr1(:,:,iz)
      py(:,:,iz) = Uy(:,:,iz) + scr2(:,:,iz)
      pz(:,:,iz) = Uz(:,:,iz) + scr3(:,:,iz)
    end do

    call xdn_set_par(Bz, scr4)
    call zdn_set_par(px, scr5)
    !$omp parallel do private(iz)
    do iz=1,mz
      Ey(:,:,iz) = + scr4(:,:,iz)*scr5(:,:,iz)
    end do

    call xdn_set_par(By, scr4)
    call ydn_set_par(px, scr5)
    !$omp parallel do private(iz)
    do iz=1,mz
      Ez(:,:,iz) = - scr4(:,:,iz)*scr5(:,:,iz)
    end do

    call ydn_set_par(Bx, scr4)
    call xdn_set_par(py, scr5)
    !$omp parallel do private(iz)
    do iz=1,mz
      Ez(:,:,iz) = Ez(:,:,iz) + scr4(:,:,iz)*scr5(:,:,iz)
    end do

    call ydn_set_par(Bz, scr4)
    call zdn_set_par(py, scr5)
    !$omp parallel do private(iz)
    do iz=1,mz
      Ex(:,:,iz) = - scr4(:,:,iz)*scr5(:,:,iz)
    end do

    call zdn_set_par(By, scr4)
    call ydn_set_par(pz, scr5)
    !$omp parallel do private(iz)
    do iz=1,mz
      Ex(:,:,iz) = Ex(:,:,iz) + scr4(:,:,iz)*scr5(:,:,iz)
    end do

    call zdn_set_par(Bx, scr4)
    call xdn_set_par(pz, scr5)
    !$omp parallel do private(iz)
    do iz=1,mz
      Ey(:,:,iz) = Ey(:,:,iz) - scr4(:,:,iz)*scr5(:,:,iz)
    end do
  else
    !$omp parallel do private(k)
    do k=1,mz
      px(:,:,k) = Ux(:,:,k)
      py(:,:,k) = Uy(:,:,k)
      pz(:,:,k) = Uz(:,:,k)
    end do
  endif

END SUBROUTINE
!***********************************************************************
SUBROUTINE induction (lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt)
  USE params
  implicit none
  integer iz
  real, dimension(mx,my,mz):: lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt
  real fCb,dtdx,fmaxval

!  Magnetic convergence, in the plane perpendicular to the magnetic field.
!  r1 is used first for the normalization factor, then for the scalar
!  product U*Bhat.

!      bvx = xup(Bx)
!      bvy = yup(By)
!      bvz = zup(Bz)

  call xup_set_par (Bx, vx)
  call yup_set_par (By, vy)
  call zup_set_par (Bz, vz)

  if (gamma .eq. 1.) then
    gg1 = 1.
  else
    gg1 = gamma*(gamma-1)
  endif

  dtdx = dt/min(dx,dy,dz)
  !$omp parallel do private(k)
  do k=1,mz
   r1(:,:,k) = max(vx(:,:,k)**2 + vy(:,:,k)**2 + vz(:,:,k)**2, 1.e-20)
   cs(:,:,k) = sqrt(gg1*ee(:,:,k)+r1(:,:,k)/r(:,:,k))
   scr1(:,:,k) = dtdx*cs(:,:,k)
   cs(:,:,k) = (nu3*nuB)*cs(:,:,k)
   r1(:,:,k) = 1./sqrt(r1(:,:,k))
   vx(:,:,k) = vx(:,:,k)*r1(:,:,k)             ! bvi = unit B vector
   vy(:,:,k) = vy(:,:,k)*r1(:,:,k)
   vz(:,:,k) = vz(:,:,k)*r1(:,:,k)
  end do
  Cb = fmaxval('Cb',scr1)

!      scr1 = xup(px)          ! scri = centered velocity
!      scr2 = yup(py)
!      scr3 = zup(pz)

  call xup_set_par (px, scr1)
  call yup_set_par (py, scr2)
  call zup_set_par (pz, scr3)

  !$omp parallel do private(k)
  do k=1,mz
    r1(:,:,k) = scr1(:,:,k)*vx(:,:,k) &
              + scr2(:,:,k)*vy(:,:,k) &
              + scr3(:,:,k)*vz(:,:,k)
    scr1(:,:,k) = scr1(:,:,k)-vx(:,:,k)*r1(:,:,k)
    scr2(:,:,k) = scr2(:,:,k)-vy(:,:,k)*r1(:,:,k)
    scr3(:,:,k) = scr3(:,:,k)-vz(:,:,k)*r1(:,:,k)
  end do
  call dif2d_set_par (scr1,scr2,scr3,scr4)
  call dumpl(scr4,'dif2d','pde.dmp',idump); idump=idump+1
  !$omp parallel do private(iz)
  do iz=1,mz
    scr4(:,:,iz) = (0.5*nu2*nuB)*max(0.0,scr4(:,:,iz))
  end do

!  Smooth over 3x3x3 point, to broaden influence.  Use a low order shift
!  after smoothing, to ensure positive definite values.

  call max3_set_par (scr4,scr1)
  call smooth3_set_par (scr1,scr4)

  !$omp parallel do private(iz)
  do iz=1,mz
    cs(:,:,iz) = cs(:,:,iz) + scr4(:,:,iz)
  end do
!      vx = dx*((nu1*nuB)*abs(px) + xdn1(cs))
!      vy = dy*((nu1*nuB)*abs(py) + ydn1(cs))
!      vz = dz*((nu1*nuB)*abs(pz) + zdn1(cs))
  call xdn1_set_par (cs, scr1)
  !$omp parallel do private(iz)
  do iz=1,mz
    vx(:,:,iz) = dx*((nu1*nuB)*abs(px(:,:,iz)) + scr1(:,:,iz))
  end do
  call ydn1_set_par (cs, scr1)
  !$omp parallel do private(iz)
  do iz=1,mz
    vy(:,:,iz) = dy*((nu1*nuB)*abs(py(:,:,iz)) + scr1(:,:,iz))
  end do
  call zdn1_set_par (cs, scr1)
  !$omp parallel do private(iz)
  do iz=1,mz
    vz(:,:,iz) = dz*((nu1*nuB)*abs(pz(:,:,iz)) + scr1(:,:,iz))
  end do

  fCb = 6.2*3.*dt/2.
  !$omp parallel do private(iz)
  do iz=1,mz
    scr1(:,:,iz) = fCb*max(vx(:,:,iz)/dx**2,vy(:,:,iz)/dy**2,vz(:,:,iz)/dz**2)
  end do
  Cn = fmaxval('Cn',scr1)

  call stats ('bvx', vx)
  call stats ('bvy', vy)
  call stats ('bvz', vz)

END SUBROUTINE
!***********************************************************************
SUBROUTINE induction2 (lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt)
  USE params
  implicit none
  integer iz
  real, dimension(mx,my,mz):: lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt

!  Induction equation, d(B)/dt = - curl(E) = curl (u x B - eta I)
!  ==============================================================

!  The magnetic field is face centered.  The electric field and current
!  are centered on the cube edges.

!  eta I contribution to the Electric field

!      Ex = Ex + 0.5*(ydn1(vz)+zdn1(vy))*Jx
!      Ey = Ey + 0.5*(zdn1(vx)+xdn1(vz))*Jy
!      Ez = Ez + 0.5*(xdn1(vy)+ydn1(vx))*Jz

  call ydn1_set_par (vz, scr1)
  call zdn1_set_par (vy, scr2)
  !$omp parallel do private(iz)
  do iz=1,mz
    Ex(:,:,iz) = Ex(:,:,iz) + 0.5*(scr1(:,:,iz)+scr2(:,:,iz))*Jx(:,:,iz)
  end do

  call zdn1_set_par (vx, scr1)
  call xdn1_set_par (vz, scr2)
  !$omp parallel do private(iz)
  do iz=1,mz
    Ey(:,:,iz) = Ey(:,:,iz) + 0.5*(scr1(:,:,iz)+scr2(:,:,iz))*Jy(:,:,iz)
  end do

  call xdn1_set_par (vy, scr1)
  call ydn1_set_par (vx, scr2)
  !$omp parallel do private(iz)
  do iz=1,mz
    Ez(:,:,iz) = Ez(:,:,iz) + 0.5*(scr1(:,:,iz)+scr2(:,:,iz))*Jz(:,:,iz)
  end do
END SUBROUTINE
!**********************************************************************
SUBROUTINE jouleheating (lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt)
  USE params
  implicit none
  integer iz
  real, dimension(mx,my,mz):: lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt
  integer, save:: i_step=0
  real Emagn,Ekin,Qjoule

  if (.not. do_energy) return

!  Joule heating added to the energy equation, eta I^2

  !scr1 = zup(yup(Ex*Jx)) + xup(zup(Ey*Jy)+yup(Ez*Jz))

  !$omp parallel do private(iz)
  do iz=1,mz
    scr1(:,:,iz) = Ex(:,:,iz)*Jx(:,:,iz)
    scr2(:,:,iz) = Ey(:,:,iz)*Jy(:,:,iz)
    scr3(:,:,iz) = Ez(:,:,iz)*Jz(:,:,iz)
  end do
  call yup_set_par (scr1, scr4)
  call zup_set_par (scr2, scr5)
  call yup_set_par (scr3, scr6)
  call zup_set_par (scr4, scr1)
  call xup_set_par (scr5, scr2)
  call xup_set_par (scr6, scr3)
  !$omp parallel do private(iz)
  do iz=1,mz
    scr1(:,:,iz) = scr1(:,:,iz)+scr2(:,:,iz)+scr3(:,:,iz)
  end do

  if (do_energy) then
    if (idbg>0 .and. mod(i_step,20) .eq. 1) then
      Qjoule = sum(scr1)/mw
      !$omp parallel do private(iz)
      do iz=1,mz
        scr2(:,:,iz) = Bx(:,:,iz)**2 + By(:,:,iz)**2 + Bz(:,:,iz)**2
      end do
      Emagn = 0.5*sum(scr2)/mw
      !$omp parallel do private(iz)
      do iz=1,mz
        scr2(:,:,iz) = r(:,:,iz)*(Ux(:,:,iz)**2+Uy(:,:,iz)**2+Uz(:,:,iz)**2)
      end do
      Ekin = 0.5*sum(scr2)/mw
      write(*,'(1x,a,f10.4,3(1pe12.5))') 'joule:',t,Qjoule,Emagn,Ekin
    endif
    i_step = i_step + 1
    !$omp parallel do private(iz)
    do iz=1,mz
      deedt(:,:,iz) = deedt(:,:,iz) + scr1(:,:,iz)/r(:,:,iz)
    end do
  end if

END SUBROUTINE
!***********************************************************************
SUBROUTINE magneticfield (lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt)
  USE params
  implicit none
  integer iz
  real, dimension(mx,my,mz):: lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt

!  Magnetic field 's time derivative, dBdt = - curl(E)

!      dBxdt = ddzup (Ey) - ddyup (Ez)
!      dBydt = ddxup (Ez) - ddzup (Ex)
!      dBzdt = ddyup (Ex) - ddxup (Ey)

  call ptrace ('ddzup_add', 3)
  call ddzup_add_par(Ey, dBxdt)
  call ptrace ('ddyup_sub', 3)
  call ddyup_sub_par(Ez, dBxdt)

  call ptrace ('ddxup_add', 3)
  call ddxup_add_par(Ez, dBydt)
  call ptrace ('ddzup_sub', 3)
  call ddzup_sub_par(Ex, dBydt)

  call ptrace ('ddyup_add', 3)
  call ddyup_add_par(Ex, dBzdt)
  call ptrace ('ddxup_sub', 3)
  call ddxup_sub_par(Ey, dBzdt)

END SUBROUTINE
END MODULE

!***********************************************************************
SUBROUTINE pde ( &
    lnr,Ux,Uy,Uz,ee,d,dlnrdt,dUxdt,dUydt,dUzdt,deedt,dddt,flag, &
    Bx,By,Bz,dBxdt,dBydt,dBzdt)
  USE params
  USE pde_cm
  implicit none
  integer iz
  logical flag
  real, dimension(mx,my,mz):: &
    lnr,Ux,Uy,Uz,ee,d,dlnrdt,dUxdt,dUydt,dUzdt,deedt,dddt, &
    Bx,By,Bz,dBxdt,dBydt,dBzdt
  character(len=mid):: id='$Id: pde_cm_local.f90,v 1.37 2007/07/12 08:48:39 aake Exp $'

  call print_id(id)

  ambi=0.

  call density_boundary_log (lnr)
  call velocity_boundary (Ux, Uy, Uz)

  call stats ('lnr',lnr)
  call stats ('Ux',Ux)
  call stats ('Uy',Uy)
  call stats ('Uz',Uz)
  call stats ('ee',ee)
  call stats ('Bx',Bx)
  call stats ('By',By)
  call stats ('Bz',Bz)
  call alloc
  call dumpl(dlnrdt,'start','pde.dmp',idump); idump=idump+1
  call diffus          (lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt)
  call dumpl(lnP,'diffus','pde.dmp',idump); idump=idump+1
  call diffusivities   (lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt,flag)
  call dumpl(vx,'difties','pde.dmp',idump); idump=idump+1
  call mass            (lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt)
  call dumpl(dlnrdt,'mass','pde.dmp',idump); idump=idump+1
  call mass1           (lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt)
  call dumpl(dlnrdt,'mass1','pde.dmp',idump); idump=idump+1
  call mass2           (lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt)
  call dumpl(dlnrdt,'mass2','pde.dmp',idump); idump=idump+1
  if (do_massexact) &
    call massexact       (lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt)
  call dumpl(dUxdt,'masse','pde.dmp',idump); idump=idump+1
  call momx            (lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt)
  call dumpl(dUxdt,'momx','pde.dmp',idump); idump=idump+1
  call momxsymmetrized (lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt)
  call dumpl(dUxdt,'momxs','pde.dmp',idump); idump=idump+1
  call momx1           (lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt)
  call dumpl(dUxdt,'mox1','pde.dmp',idump); idump=idump+1
  call momx2           (lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt)
  call dumpl(dUxdt,'mox2','pde.dmp',idump); idump=idump+1
  call momy            (lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt)
  call dumpl(dUxdt,'momy','pde.dmp',idump); idump=idump+1
  call momysymmetrized (lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt)
  call dumpl(dUxdt,'momys','pde.dmp',idump); idump=idump+1
  call momyduydt       (lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt)
  call dumpl(dUxdt,'momyd','pde.dmp',idump); idump=idump+1
  call momz            (lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt)
  call dumpl(dUxdt,'momz','pde.dmp',idump); idump=idump+1
  call momzduzdt       (lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt)
  call dumpl(dUxdt,'momzd','pde.dmp',idump); idump=idump+1

  if (do_mhd) &
    call Lorentz       (lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt)
  call energy          (lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt)
  call dumpl(dUxdt,'lorentz','pde.dmp',idump); idump=idump+1
  call stats ('dlnrdt',dlnrdt)
  call stats ('dUxdt', dUxdt)
  call stats ('dUydt', dUydt)
  call stats ('dUzdt', dUzdt)
  call stats ('deedt', deedt)
  if (do_mhd) then
    call mfield_boundary (Bx,By,Bz)
    call induction     (lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt)
    call dumpl(dBxdt,'ind1','pde.dmp',idump); idump=idump+1
    call dumpl(dBydt,'ind1','pde.dmp',idump); idump=idump+1
    call dumpl(dBzdt,'ind1','pde.dmp',idump); idump=idump+1
    call induction2    (lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt)
    call dumpl(dBxdt,'ind2','pde.dmp',idump); idump=idump+1
    call dumpl(dBydt,'ind2','pde.dmp',idump); idump=idump+1
    call dumpl(dBzdt,'ind2','pde.dmp',idump); idump=idump+1
    call jouleheating  (lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt)
    call efield_boundary (Ex,Ey,Ez)
    call magneticfield (lnr,Ux,Uy,Uz,ee,dlnrdt,dUxdt,dUydt,dUzdt,deedt,Bx,By,Bz,dBxdt,dBydt,dBzdt)
    call dumpl(dBxdt,'ind3','pde.dmp',idump); idump=idump+1
    call dumpl(dBydt,'ind3','pde.dmp',idump); idump=idump+1
    call dumpl(dBzdt,'ind3','pde.dmp',idump); idump=idump+1
    call stats ('Jx',Jx)
    call stats ('Jy',Jy)
    call stats ('Jz',Jz)
    call stats ('Ex',Ex)
    call stats ('Ey',Ey)
    call stats ('Ez',Ez)
    call stats ('dBxdt', dBxdt)
    call stats ('dBydt', dBydt)
    call stats ('dBzdt', dBzdt)
  endif

  call forceit (r,Ux,Uy,Uz,xdnr,ydnr,zdnr,dUxdt,dUydt,dUzdt)
  call ddt_boundary (lnr,Ux,Uy,Uz,ee,Bx,By,Bz,dlnrdt,dUxdt,dUydt,dUzdt,deedt,dBxdt,dBydt,dBzdt)
  call dealloc
END SUBROUTINE
