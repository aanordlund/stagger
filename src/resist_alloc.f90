! $Id: resist_alloc.f90,v 1.1 2010/01/09 09:34:52 aake Exp $
MODULE resist_mod
  real, allocatable, dimension(:,:,:):: &
                    Bxc,Byc,Bzc,Uxc,Uyc,Uzc,cA,UdB,BB,etd
END MODULE
!***********************************************************************
SUBROUTINE resist (flag, &
    eta,r,e,pg,fudge,Ux,Uy,Uz,Bx,By,Bz,d2lnr)
!-----------------------------------------------------------------------
!  To omptimize for speed, pointer assignments are done in this first
!  wrapper routines, which then passes all arrays on in argument lists.
!-----------------------------------------------------------------------
  USE params
  USE arrays, ONLY: wk04,wk06,wk07,wk08,wk09,wk10,wk11,wk12,wk13,wk14, &
                    wk15,wk16,wk17,wk21
  USE resist_mod
  implicit none
  logical flag
  real, dimension(mx,my,mz):: &
    eta,r,e,pg,fudge,Ux,Uy,Uz,Bx,By,Bz,d2lnr
!-----------------------------------------------------------------------
  call resist1 (flag,mx,my,mz, &
                eta,r,e,pg,fudge,Ux,Uy,Uz,Bx,By,Bz,d2lnr)
END

!***********************************************************************
SUBROUTINE resist1 (flag,mx,my,mz, &
                    eta,r,e,p,fudge,Ux,Uy,Uz,Bx,By,Bz,d2lnr)
  USE params, mx_void=>mx, my_void=>my, mz_void=>mz
  USE quench_m, only: qlim
  USE resist_mod
  implicit none
  integer mx,my,mz
  logical flag
  real, dimension(mx,my,mz):: &
                    eta,r,e,p,fudge,Ux,Uy,Uz,Bx,By,Bz,d2lnr
  integer ix,iy,iz
  real gg1,cmax2,fCv,Ceta,dsmin,dsmax,average
  logical omp_in_parallel,debug
  character(len=mid):: id='$Id: resist_alloc.f90,v 1.1 2010/01/09 09:34:52 aake Exp $'
!-----------------------------------------------------------------------
  call print_id (id)
                                                                        call timer('resist','begin')
!-----------------------------------------------------------------------
!  Centered fields
!-----------------------------------------------------------------------
  gg1 = gamma*(gamma-1)
  if (gamma .eq. 1.) gg1=1.
  cmax2 = cmax**2

  !$omp master
  allocate (Bxc(mx,my,mz), Byc(mx,my,mz), Bzc(mx,my,mz))
  allocate (Uxc(mx,my,mz), Uyc(mx,my,mz), Uzc(mx,my,mz))
  allocate (BB(mx,my,mz))
  allocate (cA(mx,my,mz))
  allocate (UdB(mx,my,mz))
  allocate (etd(mx,my,mz))
  !$omp end master
  !$omp barrier

  call xup_set (Bx,Bxc)
  call yup_set (By,Byc)
  call zup_set (Bz,Bzc)
  call xup_set (Ux,Uxc)
  call yup_set (Uy,Uyc)
  call zup_set (Uz,Uzc)
    call dumpn(Uxc,'Uxc','res.dmp',0)
    call dumpn(Uyc,'Uyc','res.dmp',1)
    call dumpn(Uzc,'Uzc','res.dmp',1)

  if (isubstep == 1) then
    do iz=izs,ize
      BB(:,:,iz) = Bxc(:,:,iz)**2 + Byc(:,:,iz)**2 + Bzc(:,:,iz)**2
    end do
    call average_subr (BB, Brms)
    !$omp master
      Brms = sqrt(Brms)
    !$omp end master
  end if

!-----------------------------------------------------------------------
!  Alfven speed
!-----------------------------------------------------------------------
  call dumpn(r,'r','res.dmp',1)
  call dumpn(p,'pg','res.dmp',1)
  if (cmax .eq. 0.) then
    do iz=izs,ize
      BB(:,:,iz) = Bxc(:,:,iz)**2 + Byc(:,:,iz)**2 + Bzc(:,:,iz)**2 + 1e-20
      cA(:,:,iz) = sqrt((gamma*p(:,:,iz)+BB(:,:,iz))/r(:,:,iz))
    end do
      call dumpn(Ca,'Ca','res.dmp',1)
  else
    do iz=izs,ize
      BB(:,:,iz) = Bxc(:,:,iz)**2 + Byc(:,:,iz)**2 + Bzc(:,:,iz)**2 + 1e-20
      cA(:,:,iz) = r(:,:,iz)/(gamma*p(:,:,iz)+BB(:,:,iz))
      fudge(:,:,iz) = min(1.,cmax2*cA(:,:,iz))
      cA(:,:,iz) = min(cmax,1./sqrt(cA(:,:,iz)))
    end do
      call dumpn(Ca,'Ca','res.dmp',1)
      call dumpn(fudge,'fudge','res.dmp',1)
  end if
  if (isubstep==1) E_mag = 0.5*average(BB)
    call dumpn(BB,'BB','res.dmp',1)

  if (ub < my) then
    call magnetic_pressure_boundary (BB)
  end if
                                                                        call timer('resist','A-speed')
!-----------------------------------------------------------------------
!  Perpendicular velocity convergence
!-----------------------------------------------------------------------
  Ceta = nuB*(nu2+Csmag)
  if (Ceta .gt. 0.) then
    do iz=izs,ize
      BB(:,:,iz) = 1./sqrt(BB(:,:,iz))
      Bxc(:,:,iz) = Bxc(:,:,iz)*BB(:,:,iz)
      Byc(:,:,iz) = Byc(:,:,iz)*BB(:,:,iz)
      Bzc(:,:,iz) = Bzc(:,:,iz)*BB(:,:,iz)
      UdB(:,:,iz) = Uxc(:,:,iz)*Bxc(:,:,iz) &
                  + Uyc(:,:,iz)*Byc(:,:,iz) &
                  + Uzc(:,:,iz)*Bzc(:,:,iz)
      Uxc(:,:,iz) = Uxc(:,:,iz)-Bxc(:,:,iz)*UdB(:,:,iz)
      Uyc(:,:,iz) = Uyc(:,:,iz)-Byc(:,:,iz)*UdB(:,:,iz)
      Uzc(:,:,iz) = Uzc(:,:,iz)-Bzc(:,:,iz)*UdB(:,:,iz)
    end do
                                                                        call timer('resist','Uperp')

    call barrier_omp('resist1')                                         ! barrier 1; input values from above
    call dif2_set (Uxc, Uyc, Uzc, UdB)
      call dumpn(Uxc,'Uxc','res.dmp',1)
      call dumpn(Uyc,'Uyc','res.dmp',1)
      call dumpn(Uzc,'Uzc','res.dmp',1)
      call dumpn(UdB,'UdB','res.dmp',1)
                                                                        call timer('resist','dif2')
!-----------------------------------------------------------------------
!  Resistivity
!-----------------------------------------------------------------------
    do iz=izs,ize
      do iy=1,my
      do ix=1,mx
        eta(ix,iy,iz) = (nuB*nu1)*sqrt(Uxc(ix,iy,iz)**2+Uyc(ix,iy,iz)**2+Uzc(ix,iy,iz)**2) &
                      + (nuB*nu3)*cA(ix,iy,iz)
        etd(ix,iy,iz) = (Ceta   )*UdB(ix,iy,iz)
      end do
      end do
    end do
  else
    do iz=izs,ize
      do iy=1,my
      do ix=1,mx
        eta(ix,iy,iz) = (nuB*nu1)*sqrt(Uxc(ix,iy,iz)**2+Uyc(ix,iy,iz)**2+Uzc(ix,iy,iz)**2) &
                      + (nuB*nu3)*cA(ix,iy,iz)
        etd(ix,iy,iz) = 0.
      end do
      end do
    end do
  end if

    call dumpn(eta,'eta','res.dmp',1)
    call dumpn(etd,'etd','res.dmp',1)
  if (nu4 > 0) then
    do iz=izs,ize
    do iy=1,my
    do ix=1,mx
      eta(ix,iy,iz) = eta(ix,iy,iz)*max(1.,nu4*d2lnr(ix,iy,iz))
      etd(ix,iy,iz) = etd(ix,iy,iz)*max(1.,nu4*d2lnr(ix,iy,iz))
    end do
    end do
    end do
      call dumpn(d2lnr,'d2lnr','res.dmp',1)
      call dumpn(eta,'eta','res.dmp',1)
      call dumpn(etd,'etd','res.dmp',1)
  end if
                                                                        call timer('resist','eta')
  call regularize (etd)
                                                                        call timer('resist','regular')
!-----------------------------------------------------------------------
!  Smooth over 3x3x3 point, to broaden influence.  Use low order shifts
!  when using eta in mhd, to ensure positive definite values.
!-----------------------------------------------------------------------
  call barrier_omp('resist2')                                           ! barrier 2; etd from above (NOT NEEDED?)
  if (do_max5_mhd) then
    call smooth3max5_set (etd, Bxc)
    call smooth3max5_set (eta, Byc)
  else
    call smooth3max3_set (etd, Bxc)
    call smooth3max3_set (eta, Byc)
  end if

  call barrier_omp('resist3')                                           ! barrier 3; SHOULD NOT BE NEEDED!
  call regularize (etd)
    call dumpn(eta,'eta','res.dmp',1)
    call dumpn(etd,'etd','res.dmp',1)
                                                                        call timer('resist','end')
!-----------------------------------------------------------------------
!  Magnetic diffusion Courant condition factors:
!     6.2 : the maximum value returned from ddxup(ddxdn(f)) when dx=1.
!      3. : for the worst case of a 3-D checker board
!      2. : normalize so Ctd=1. when we reach 1x overshoot
!-----------------------------------------------------------------------
  fCv = 6.2*3./2.*dt

!-----------------------------------------------------------------------
!  Evaluate the time step constraints from advection and diffusion
!-----------------------------------------------------------------------
  if (flag) then
    do iz=izs,ize
    if (do_aniso_eta) then 
     do iy=1,my
     do ix=1,mx
      cA(ix,iy,iz) = max((cA(ix,iy,iz)+abs(Uxc(ix,iy,iz))), &
                         (cA(ix,iy,iz)+abs(Uyc(ix,iy,iz))), &
                         (cA(ix,iy,iz)+abs(Uzc(ix,iy,iz))))
      dsmin = min(dxm(ix),dym(iy),dzm(iz))
      cA(ix,iy,iz) = cA(ix,iy,iz)*dt/dsmin
      BB(ix,iy,iz) = (eta(ix,iy,iz)/dsmin+etd(ix,iy,iz))*fCv
     end do
     end do
    else
     do iy=1,my
     do ix=1,mx
      cA(ix,iy,iz) = max((cA(ix,iy,iz)+abs(Uxc(ix,iy,iz))), &
                         (cA(ix,iy,iz)+abs(Uyc(ix,iy,iz))), &
                         (cA(ix,iy,iz)+abs(Uzc(ix,iy,iz))))
      dsmin = min(dxm(ix),dym(iy),dzm(iz))
      dsmax = max(0.5*(dxm(ix)+dym(iy)), &
                  0.5*(dym(iy)+dzm(iz)), &
                  0.5*(dzm(iz)+dxm(ix)))
      cA(ix,iy,iz) = cA(ix,iy,iz)*dt/dsmin
      BB(ix,iy,iz) = (eta(ix,iy,iz)*dsmax+etd(ix,iy,iz)*dsmax**2)*(fCv/dsmin**2)
     end do
     end do
    end if
    end do
    call fmaxval_subr ('Cb',cA,Cb)
    call fmaxval_subr ('Cn',BB,Cn)
  end if
    call dumpn(Ca,'Ca','res.dmp',1)
                                                                        call timer('resist','Courant')
  !$omp barrier
  !$omp master
  deallocate (UdB)
  deallocate (Uxc, Uyc, Uzc)
  deallocate (Bxc, Byc, Bzc)
  deallocate (cA)
  deallocate (BB)
  deallocate (etd)
  !$omp end master
END
