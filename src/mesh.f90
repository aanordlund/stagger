!***********************************************************************
SUBROUTINE check_linear (m, fs, ds, s)
  USE params, only: master, mpi_rank
  implicit none
  integer i,m
  real fs(m), ds(m), s(m), tmp(m), aver1, aver2, rms, aver_all
  real(kind=8) d, s1
  
  aver1 = sum(fs(1:m))/m
  tmp = (fs-aver1)**2
  aver2 = sum(tmp(1:m))/m
  rms = sqrt(aver2)/aver1
  if (rms < 1e-2) then
    fs = aver1
    d  = sum(ds(2:m-1))/(m-2)
    ds = d
    s1 = s(1)
    s  = s1 + d*(/(i-1, i=1,m)/)
    if (master) print*,'setting exact linear scale, ds =', aver1
    !print*,'setting exact linear scale, ds =', aver1, mpi_rank, rms
  else
  end if
END SUBROUTINE

!***********************************************************************
SUBROUTINE init_mesh
!
!  Priorities: If a text based mesh file exists, then read that and
!  write out a new binary mesh file.  If a binary mesh file exists,
!  then read that and use that mesh.  If no mesh file exists, then
!  generate an equidistant mesh and write out a binary mesh file.
!
!  By reading a text mesh file if it exists one can make sure to have
!  a certain precomputed mesh.
!
!  By writing out a binary mesh file, even if one existed before, one
!  is certain that the one on disk has the exact mesh that was used.
!
!  The mesh is centered such that the point furthest from the origin is
!  at ix=mx, so that the half points are symmetrically arranged around
!  the origin.
!-----------------------------------------------------------------------
  USE params
  implicit none
  integer ix, iy, iz, mx1, my1, mz1
  integer rank
  real dx1, dy1, dz1
  logical exists, debug
  real, allocatable, dimension(:):: tmp
  character(len=mfile):: name, fname

  character(len=mid):: id='$Id: mesh.f90,v 1.54 2015/04/18 09:29:06 aake Exp $'
  call print_id (id)

  mx1 = mxtot
  my1 = mytot
  mz1 = mztot

  allocate(xm(mx1),xmdn(mx1),dxm(mx1),dxmdn(mx1),dxidxdn(mx1),dxidxup(mx1))
  allocate(ym(my1),ymdn(my1),dym(my1),dymdn(my1),dyidydn(my1),dyidyup(my1))
  allocate(zm(mz1),zmdn(mz1),dzm(mz1),dzmdn(mz1),dzidzdn(mz1),dzidzup(mz1))
  allocate(xmg(mx1),ymg(my1),zmg(mz1))

!-------------------------------------------------------
!  Odd number of points: 0.1m2.3.4 , n=5 -> m=n/2-1. (IDL)
!  Odd number of points: 1.2m3.4.5 , n=5 -> m=n/2. (FTN)
!  Evn number of points: 0.m.2.3, n=4 -> m=n/2-1. (IDL)
!  Evn number of points: 1.m.3.4, n=4 -> m=n/2. (FTN)
!-------------------------------------------------------

  xm = dx*(/(ix, ix=0,mx1-1)/) + xmin
  ym = dy*(/(iy, iy=0,my1-1)/) + ymin
  zm = dz*(/(iz, iz=0,mz1-1)/) + zmin
  xmdn = xm - 0.5*dx
  ymdn = ym - 0.5*dy
  zmdn = zm - 0.5*dz
  dxm = dx
  dym = dy
  dzm = dz
  dxmdn = dxm
  dymdn = dym
  dzmdn = dzm
  dxidxdn = 1./dxmdn
  dyidydn = 1./dymdn
  dzidzdn = 1./dzmdn
  dxidxup = 1./dxm
  dyidyup = 1./dym
  dzidzup = 1./dzm

  call read_mesh                                                        ! try to read old mesh

  dx = (xm(mx1)-xm(1))/(mx1-1)                                          ! set mesh sizes
  dy = (ym(my1)-ym(1))/(my1-1)
  dz = (zm(mz1)-zm(1))/(mz1-1)
  sx = (xm(mx1)+dx)-xm(1)
  sy = (ym(my1)+dy)-ym(1)
  sz = (zm(mz1)+dz)-zm(1)

  call barrier_mpi('mesh')                                              ! make sure all have read

  if (master) then
    print'(1x,a,4g14.7)','dx,sx,xm(1),xm(mx1)=',dx,sx,xm(1),xm(mx1) &
                        ,'dy,sy,ym(1),ym(my1)=',dy,sy,ym(1),ym(my1) &
                        ,'dz,sz,zm(1),zm(mz1)=',dz,sz,zm(1),zm(mz1)    
  end if

  xmg = xm
  ymg = ym
  zmg = zm

  allocate(tmp(mx))
  tmp=     xm(1+ixoff:mx+ixoff); deallocate(     xm); allocate(     xm(mx));      xm=tmp
  tmp=   xmdn(1+ixoff:mx+ixoff); deallocate(   xmdn); allocate(   xmdn(mx));    xmdn=tmp
  tmp=    dxm(1+ixoff:mx+ixoff); deallocate(    dxm); allocate(    dxm(mx));     dxm=tmp
  tmp=  dxmdn(1+ixoff:mx+ixoff); deallocate(  dxmdn); allocate(  dxmdn(mx));   dxmdn=tmp
  tmp=dxidxdn(1+ixoff:mx+ixoff); deallocate(dxidxdn); allocate(dxidxdn(mx)); dxidxdn=tmp
  tmp=dxidxup(1+ixoff:mx+ixoff); deallocate(dxidxup); allocate(dxidxup(mx)); dxidxup=tmp
  deallocate(tmp)

  allocate(tmp(my))
  tmp=     ym(1+iyoff:my+iyoff); deallocate(     ym); allocate(     ym(my));      ym=tmp
  tmp=   ymdn(1+iyoff:my+iyoff); deallocate(   ymdn); allocate(   ymdn(my));    ymdn=tmp
  tmp=    dym(1+iyoff:my+iyoff); deallocate(    dym); allocate(    dym(my));     dym=tmp
  tmp=  dymdn(1+iyoff:my+iyoff); deallocate(  dymdn); allocate(  dymdn(my));   dymdn=tmp
  tmp=dyidydn(1+iyoff:my+iyoff); deallocate(dyidydn); allocate(dyidydn(my)); dyidydn=tmp
  tmp=dyidyup(1+iyoff:my+iyoff); deallocate(dyidyup); allocate(dyidyup(my)); dyidyup=tmp
  deallocate(tmp)

  allocate(tmp(mz))
  tmp=     zm(1+izoff:mz+izoff); deallocate(     zm); allocate(     zm(mz));      zm=tmp
  tmp=   zmdn(1+izoff:mz+izoff); deallocate(   zmdn); allocate(   zmdn(mz));    zmdn=tmp
  tmp=    dzm(1+izoff:mz+izoff); deallocate(    dzm); allocate(    dzm(mz));     dzm=tmp
  tmp=  dzmdn(1+izoff:mz+izoff); deallocate(  dzmdn); allocate(  dzmdn(mz));   dzmdn=tmp
  tmp=dzidzdn(1+izoff:mz+izoff); deallocate(dzidzdn); allocate(dzidzdn(mz)); dzidzdn=tmp
  tmp=dzidzup(1+izoff:mz+izoff); deallocate(dzidzup); allocate(dzidzup(mz)); dzidzup=tmp
  deallocate(tmp)

  if (debug(dbg_io)) then
    do rank=0,mpi_size-1
      if(rank==mpi_rank) then
        print'(a,i5,3(1x,2f8.4))','mesh.f90: rank,xm,ym,zm =', &
          mpi_rank,xm(1),xm(mx),ym(1),ym(my),zm(1),zm(mz)
      end if
      call barrier_mpi('xx')
    enddo
  end if

END SUBROUTINE

!***********************************************************************
SUBROUTINE read_mesh

  USE params
  implicit none

  logical :: exists
  integer :: mx1, my1, mz1, ix, iy, iz, i, iostat1
  character(len=mfile):: from_name, name, fname
  real, allocatable, dimension(:)   :: xi,yi,zi
  real, allocatable, dimension(:,:) :: tmp
  real(kind=4), allocatable, dimension(:) :: dxm1, dxmdn1, xm1, xmdn1, dxidxup1, dxidxdn1
  real(kind=4), allocatable, dimension(:) :: dym1, dymdn1, ym1, ymdn1, dyidyup1, dyidydn1
  real(kind=4), allocatable, dimension(:) :: dzm1, dzmdn1, zm1, zmdn1, dzidzup1, dzidzdn1

  !----------------------------------------------------------------------
  !  Try to read mesh arrays from a mesh.txt, or file.mshtxt
  !----------------------------------------------------------------------
  fname = from_name('mesh.txt','mshtxt',from)

  inquire (file=fname,exist=exists)
  if (do_trace .and. master) print *,'read_mesh ',fname,exists

  if (exists) then

     if (master) print *,'reading formatted mesh data from '//trim(fname)
     open(2, file=fname, status='old', form='formatted')
     read(2,*) mx1
     if (master) print *,mx,mx1
     if (mx1.ne.mx*mpi_nx) then
        print*,mx,mpi_nx,mx1,mpi_rank,'inconsistent mesh'
        stop
     end if
     read(2,*) xm
     read(2,*) my1
     if (master) print *,my,my1
     if (my1.ne.my*mpi_ny) then
        print*,my,mpi_ny,my1,mpi_rank,'inconsistent mesh'
        stop
     end if
     read(2,*) ym
     read(2,*) mz1
     if (master) print *,mz,mz1
     if (mz1.ne.mz*mpi_nz) then
        print*,mz,mpi_nz,mz1,mpi_rank,'inconsistent mesh'
        stop
     end if
     read(2,*) zm
     close(2)
     if (master) print *,'1: min(ym), max(ym) ', minval(ym), maxval(ym)

     dx = (xm(mx1)-xm(1))/(mx1-1)                 ! set mesh sizes
     dy = (ym(my1)-ym(1))/(my1-1)
     dz = (zm(mz1)-zm(1))/(mz1-1)
     sx = dx*mx1                                  ! set box sizes
     sy = dy*my1                                  ! FIXME: incorrect for non-periodic meshes
     sz = dz*mz1

     allocate (xi(mx1),yi(my1),zi(mz1))
     xi = (/(ix, ix=1,mx1)/)                      ! linear index scales
     yi = (/(iy, iy=1,my1)/)
     zi = (/(iz, iz=1,mz1)/)
     call spline (mx1,xi,xm,dxidxup)              ! spline derivatives
     call spline (my1,yi,ym,dyidyup)
     call spline (mz1,zi,zm,dzidzup)
     dxidxup = 1./dxidxup                         ! inverse derivatives
     dyidyup = 1./dyidyup
     dzidzup = 1./dzidzup
     call interp (mx1,xi,xm,mx1,xi-.5,xmdn,2,2)   ! half-zone meshes
     call interp (my1,yi,ym,my1,yi-.5,ymdn,2,2)
     call interp (mz1,zi,zm,mz1,zi-.5,zmdn,2,2)
     call spline (mx1,xi,xmdn,dxidxdn)            ! spline derivatives
     call spline (my1,yi,ymdn,dyidydn)
     call spline (mz1,zi,zmdn,dzidzdn)
     dxidxdn = 1./dxidxdn                         ! inverse derivatives
     dyidydn = 1./dyidydn
     dzidzdn = 1./dzidzdn

     dxmdn(2:mx1) = xm(2:mx1)-xm(1:mx1-1); dxmdn(1)=sx-sum(dxmdn(2:mx1))
     dymdn(2:my1) = ym(2:my1)-ym(1:my1-1); dymdn(1)=sy-sum(dymdn(2:my1))
     dzmdn(2:mz1) = zm(2:mz1)-zm(1:mz1-1); dzmdn(1)=sz-sum(dzmdn(2:mz1))
     dxm(1:mx1-1) = xmdn(2:mx1)-xmdn(1:mx1-1); dxm(mx1)=sx-sum(dxm(1:mx1-1))
     dym(1:my1-1) = ymdn(2:my1)-ymdn(1:my1-1); dym(my1)=sy-sum(dym(1:my1-1))
     dzm(1:mz1-1) = zmdn(2:mz1)-zmdn(1:mz1-1); dzm(mz1)=sz-sum(dzm(1:mz1-1))

     call check_linear (mx1, dxidxup, dxm, xm)
     call check_linear (my1, dyidyup, dym, ym)
     call check_linear (mz1, dzidzup, dzm, zm)
     call check_linear (mx1, dxidxdn, dxmdn, xmdn)
     call check_linear (my1, dyidydn, dymdn, ymdn)
     call check_linear (mz1, dzidzdn, dzmdn, zmdn)

     deallocate (xi,yi,zi)                        ! clean up

     if (idbg.ge.2) call print_mesh
     if (master) call write_mesh_binary (mx1, my1, mz1)                  ! write binary mesh file

     !-----------------------------------------------------------------------
     !  Else try to read mesh from binary file (possibly 'from=' file name)
     !-----------------------------------------------------------------------

  else     ! exists=.false. text file

     exists = .false.
     if (trim(meshfile) == 'mesh.dat') then
        fname = from_name(meshfile,'msh',from)                            ! binary 'from' mesh file 
     else
        fname = meshfile                                                  ! use explicitly specified name
     end if
     inquire (file=fname, exist=exists)                                   ! exists?


     if (exists) then    ! exists=.true. binary

        do i=1,maxtry
          open(data_unit, file=fname, status='old', form='unformatted', iostat=iostat1)
          if(iostat1==0) exit
          print *,'file,unit,no. tries: ',fname,data_unit,i
          call sleep(1)
        end do

        ! read dimensions
        read(data_unit) mx1, my1, mz1
        if (master) print *,'reading full mesh from ' // trim(fname), mx1, my1, mz1

        ! allocate temporary single-precision variables for reading
        allocate( dxm1(mx1), dxmdn1(mx1), xm1(mx1), xmdn1(mx1), dxidxup1(mx1), dxidxdn1(mx1) )
        allocate( dym1(my1), dymdn1(my1), ym1(my1), ymdn1(my1), dyidyup1(my1), dyidydn1(my1) )
        allocate( dzm1(mz1), dzmdn1(mz1), zm1(mz1), zmdn1(mz1), dzidzup1(mz1), dzidzdn1(mz1) )

        ! read single-precision variables
        read(data_unit) dxm1, dxmdn1, xm1, xmdn1, dxidxup1, dxidxdn1
        read(data_unit) dym1, dymdn1, ym1, ymdn1, dyidyup1, dyidydn1
        read(data_unit) dzm1, dzmdn1, zm1, zmdn1, dzidzup1, dzidzdn1

        ! copy single-precsion variables to mesh arrays (could be double-precision)
        dxm = dxm1; dxmdn = dxmdn1; xm = xm1; xmdn = xmdn1; dxidxup = dxidxup1; dxidxdn = dxidxdn1
        dym = dym1; dymdn = dymdn1; ym = ym1; ymdn = ymdn1; dyidyup = dyidyup1; dyidydn = dyidydn1
        dzm = dzm1; dzmdn = dzmdn1; zm = zm1; zmdn = zmdn1; dzidzup = dzidzup1; dzidzdn = dzidzdn1

        ! deallocate temporary variables
        deallocate( dxm1, dxmdn1, xm1, xmdn1, dxidxup1, dxidxdn1 )
        deallocate( dym1, dymdn1, ym1, ymdn1, dyidyup1, dyidydn1 )
        deallocate( dzm1, dzmdn1, zm1, zmdn1, dzidzup1, dzidzdn1 )

        close(data_unit)

        call barrier_mpi('write_mesh')
        if (master) call write_mesh_binary (mx1, my1, mz1)                  ! write binary mesh file

     else      ! exists=.false. binary file

        if (do_stratified) then
          if (master) print *,'ERROR: no mesh file (text or binary) found'
          call end_mpi
        end if
        if (master) print *,'WARNING: no mesh file found; using default mesh based on sizes'
        if (master) call write_mesh_binary (mxtot, mytot, mztot)                  ! write binary mesh file

     end if

  end if

  xmin = minval(xm)
  ymin = minval(ym)
  zmin = minval(zm)
  if (master) print *,'xmin,ymin,zmin=',xmin,ymin,zmin

END SUBROUTINE read_mesh

!***********************************************************************
SUBROUTINE write_mesh_binary (mx1, my1, mz1)

  USE params
  implicit none
  integer :: mx1, my1, mz1, i, iostat1
  character(len=mfile) :: name, fname

  real(kind=4), allocatable, dimension(:) :: dxm1, dxmdn1, xm1, xmdn1, dxidxup1, dxidxdn1
  real(kind=4), allocatable, dimension(:) :: dym1, dymdn1, ym1, ymdn1, dyidyup1, dyidydn1
  real(kind=4), allocatable, dimension(:) :: dzm1, dzmdn1, zm1, zmdn1, dzidzup1, dzidzdn1


  fname = name('mesh.dat','msh',file)                                   ! binary mesh filename

  do i=1,maxtry
    open(data_unit, file=fname, status='unknown', form='unformatted', iostat=iostat1)
    if(iostat1==0) exit
    print *,'file,unit,no. tries: ',fname,data_unit,i
    call sleep(1)
  end do
  print *, 'writing binary mesh data to ' // trim(fname),mx1,my1,mz1
  
  ! allocate temporary single-precision variables for writing
  allocate( dxm1(mx1), dxmdn1(mx1), xm1(mx1), xmdn1(mx1), dxidxup1(mx1), dxidxdn1(mx1) )
  allocate( dym1(my1), dymdn1(my1), ym1(my1), ymdn1(my1), dyidyup1(my1), dyidydn1(my1) )
  allocate( dzm1(mz1), dzmdn1(mz1), zm1(mz1), zmdn1(mz1), dzidzup1(mz1), dzidzdn1(mz1) )

  ! copy mesh arrays to temporary single-precsion variables
  dxm1 = dxm; dxmdn1 = dxmdn; xm1 = xm; xmdn1 = xmdn; dxidxup1 = dxidxup; dxidxdn1 = dxidxdn
  dym1 = dym; dymdn1 = dymdn; ym1 = ym; ymdn1 = ymdn; dyidyup1 = dyidyup; dyidydn1 = dyidydn
  dzm1 = dzm; dzmdn1 = dzmdn; zm1 = zm; zmdn1 = zmdn; dzidzup1 = dzidzup; dzidzdn1 = dzidzdn
  
  write(data_unit) mx1, my1 ,mz1
  write(data_unit) dxm1, dxmdn1, xm1, xmdn1, dxidxup1, dxidxdn1
  write(data_unit) dym1, dymdn1, ym1, ymdn1, dyidyup1, dyidydn1
  write(data_unit) dzm1, dzmdn1, zm1, zmdn1, dzidzup1, dzidzdn1

  ! deallocate temporary variables
  deallocate( dxm1, dxmdn1, xm1, xmdn1, dxidxup1, dxidxdn1 )
  deallocate( dym1, dymdn1, ym1, ymdn1, dyidyup1, dyidydn1 )
  deallocate( dzm1, dzmdn1, zm1, zmdn1, dzidzup1, dzidzdn1 )

  close(data_unit)

END SUBROUTINE write_mesh_binary


!***********************************************************************
SUBROUTINE print_mesh
  USE params
  implicit none

  if (.not. master) return
  print *,'xm, xmdn, dxidxup, dxidxdn'
  print *, xm, xmdn, dxidxup, dxidxdn
  print *,'ym, ymdn, dyidyup, dyidydn'
  print *, ym, ymdn, dyidyup, dyidydn
  print *,'zm, zmdn, dzidzup, dzidzdn'
  print *, zm, zmdn, dzidzup, dzidzdn

END SUBROUTINE

!***********************************************************************
SUBROUTINE spline(n,x,f,d)
! 
!  Find the derivatives at the knots (x) of a cubic spline with
!  continuous second order derivatives, passsing through the function
!  values (f).  The boundary conditions on the spline are continuous
!  third derivatives at k=2 and k=2n-1.
!
!  This version of spline is compatible with harwell tb04a.
!
!***********************************************************************
!
    integer,            intent(in)  :: n
    real, dimension(n), intent(in)  :: x,f
    real, dimension(n), intent(out) :: d
    real, dimension(3,n)            :: w
    real                            :: c,cxa,cxb,cxc,dfa,dfb,dfc
    integer                         :: n1,k,kk

! first point
    if (n.eq.1) then
      d(1)=0.0
      return
    endif
    cxb=1./(x(2)-x(1))
    cxc=1./(x(3)-x(2))
    dfb=f(2)-f(1)
    dfc=f(3)-f(2)
    w(1,1)=cxb*cxb
    w(3,1)=-cxc*cxc
    w(2,1)=w(1,1)+w(3,1)
    d(1)=2.*(dfb*cxb*cxb*cxb-dfc*cxc*cxc*cxc)

! interior points
    n1=n-1
    do k=2,n1
      cxa=1./(x(k)-x(k-1))
      cxb=1./(x(k+1)-x(k))
      dfa=f(k)-f(k-1)
      dfb=f(k+1)-f(k)
      w(1,k)=cxa
      w(3,k)=cxb
      w(2,k)=2.*(cxa+cxb)
      d(k)=3.*(dfb*cxb*cxb+dfa*cxa*cxa)
    end do

! last point
    w(1,n)=cxa*cxa
    w(3,n)=-cxb*cxb
    w(2,n)=w(1,n)+w(3,n)
    d(n)=2.*(dfa*cxa*cxa*cxa-dfb*cxb*cxb*cxb)

! eliminate at first point
    c=-w(3,1)/w(3,2)
    w(1,1)=w(1,1)+c*w(1,2)
    w(2,1)=w(2,1)+c*w(2,2)
    d(1)=d(1)+c*d(2)
    w(3,1)=w(2,1)
    w(2,1)=w(1,1)

! eliminate at last point
    c=-w(1,n)/w(1,n-1)
    w(2,n)=w(2,n)+c*w(2,n-1)
    w(3,n)=w(3,n)+c*w(3,n-1)
    d(n)=d(n)+c*d(n-1)
    w(1,n)=w(2,n)
    w(2,n)=w(3,n)

! eliminate subdiagonal
    do k=2,n
      c=-w(1,k)/w(2,k-1)
      w(2,k)=w(2,k)+c*w(3,k-1)
      d(k)=d(k)+c*d(k-1)
    end do

! backsubstitute
    d(n)=d(n)/w(2,n)
    do kk=2,n
      k=(n+1)-kk
      d(k)=(d(k)-w(3,k)*d(k+1))/w(2,k)
    end do

END SUBROUTINE

!********************************************************************
SUBROUTINE interp(n,x,f,m,xx,ff,i1,in)
!
!   interp finds the values ff at the point xx of the cubic spline
!   defined by n,x,f,d.  The xx array must be montonically increasing.
!   The two  flags i1 and in determine the type of extrapolation used:
!
!     i1/in = 0, function set to zero outside
!             1, function set constant outside
!             2, linear extrapolation
!             3, quadratic extrapolation
!             4, cubic extrapolation
!
!********************************************************************
!
  implicit none
  integer n,m,k,j,i1,in
  real p,q,d2,d36,dx,df
  real x(n),f(n),d(n),xx(m),ff(m),w(n,3)
!
  !print *,'interp',n,m
  k=2
  do j=1,m
    call spline (n,x,f,d)
!
!  Before x(1)
!
    if(xx(j).lt.x(1)) then
      ff(j)=0.
      if (i1.ge.1) ff(j)=f(1)
      if (i1.ge.2) ff(j)=ff(j)+(xx(j)-x(1))*d(1) 
      if (i1.ge.3) then
        dx=x(2)-x(1)
        d2=2.*(3.*(f(2)-f(1))/dx**2-(2.*d(1)+d(2))/dx)
        ff(j)=ff(j)+.5*(xx(j)-x(1))**2*d2
      end if
      if (i1.ge.4) then
        d36=(d(1)+d(2)-2.*(f(2)-f(1))/dx)/dx**2
        ff(j)=ff(j)+(xx(j)-x(1))*(xx(j)-x(1))**2*d36
      end if

! After x(n)

    else if(xx(j).ge.x(n)) then
      ff(j)=0.
      if (in.ge.1) ff(j)=f(n)
      if (in.ge.2) ff(j)=ff(j)+(xx(j)-x(n))*d(n)
      if (in.ge.3) then
        dx=x(n)-x(n-1)
        d2=2.*(-3.*(f(n)-f(n-1))/dx**2+(2.*d(n)+d(n-1))/dx)
        ff(j)=ff(j)+.5*(xx(j)-x(n))**2*d2
      end if
      if (in.ge.4) then
        d36=(d(n)+d(n-1)-2.*(f(n)-f(n-1))/dx)/dx**2
        ff(j)=ff(j)+(xx(j)-x(n))*(xx(j)-x(n))**2*d36
      end if
    else

!  Inside, calculate spline value

      do while(x(k).lt.xx(j))
        k=k+1
      end do
      dx=x(k)-x(k-1)
      df=f(k)-f(k-1)
      p=(xx(j)-x(k-1))/dx
      q=1.-p
      ff(j)=q*f(k-1)+p*f(k)+p*q*(q*(d(k-1)*dx-df)-p*(d(k)*dx-df))
    end if
    !print '(i4,6g12.5)',k,xx(j),f(k-1),d(k-1),f(k),d(k),ff(j)
  end do
END SUBROUTINE
