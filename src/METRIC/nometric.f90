! $Id: nometric.f90,v 1.4 2005/09/16 16:28:37 aake Exp $
!***********************************************************************
MODULE metric
  !
  USE params
  !
  logical :: time_dependence          ! If the metric is time-dependent we
                                      ! have to update the metric components 
                                      ! every time-step.
  integer :: metric_dim               ! Number of spatial coordinates the 
                                      ! metric depends on going as (y,z,x)
  integer :: mmx,mmy,mmz              ! dimensions of the metric
  !
END MODULE metric
  !
SUBROUTINE init_metric
  USE params
  USE metric
  character (len=mid):: id="$Id: nometric.f90,v 1.4 2005/09/16 16:28:37 aake Exp $"

  call print_id (id)

  !
  ! The metric is not time dependent
    time_dependence = .false.
  ! Metric dimensions
  metric_dim = 0
  mmx = 1 ; mmy = 1 ; mmz = 1
 !
END SUBROUTINE init_metric
