! $Id: mcomponents.f90,v 1.2 2005/01/13 13:24:05 aake Exp $
!***********************************************************************
MODULE metric_components
  USE params

  character(len=18) :: metric_name    ! Type of metric in use
  real :: scale_factor                ! Scale_factor for FRW metric
  logical :: time_dependence          ! If metric is time-dependent we
                                      ! have to update metric components every
                                      ! time-step
  integer :: metric_dim               ! Number of spatial coordinates the 
                                      ! metric depends on going as (z,y,x)
  integer :: mmx,mmy,mmz              ! dimensions of the metric

  real, allocatable, TARGET, dimension(:,:,:) :: &
       detgc,g11c,g22c,g33c,alpc,         &      ! spatial metric gamma_ij. 
       detgf,g11f,g22f,g33f,alpf          &      ! detg=Sqrt(|det(gamma)|)
       detge,g11e,g22e,g33e,alpe          &       
       
  real, Pointer, dimension(:,:,:) :: &
       detg, g11,g22,g33,alp

  real, allocatable, dimension(:,:,:) :: &
       gpd110,gpd111,g112,g113, &       ! partial derivatives of spatial metric
       gpd220,gpd221,g222,g223, &
       gpd330,gpd331,g332,g333, &
       alppd0,alppd1,alppd2,alppd3

CONTAINS

  SUBROUTINE initialize_metric
  select case (metric_name)
  case ('special_relativity')
    time_dependence = .false.
    metric_dim = 0.
    metric_type = 0.
    mmx = 1 ; mmy = 1 ; mmz = 1
    allocate(alpc(1,1,1),detgc(1,1,1), g11c(1,1,1),g22c(1,1,1),g33c(1,1,1), &
             alpf(1,1,1),detgf(1,1,1), g11f(1,1,1),g22f(1,1,1),g33f(1,1,1), &
             alpe(1,1,1),detge(1,1,1), g11e(1,1,1),g22e(1,1,1),g33e(1,1,1), &
    alpc = 1.0 ; detgc = 1.0 ; g11c = 1.0 ; g22c = 1.0 ; g33c = 1.0
    alpf = 1.0 ; detgf = 1.0 ; g11f = 1.0 ; g22f = 1.0 ; g33f = 1.0
    alpe = 1.0 ; detge = 1.0 ; g11e = 1.0 ; g22e = 1.0 ; g33e = 1.0
  case ('FRW')
    time_dependence = .true.
    metric_dim = 0.
    metric_type = 0.
    mmx = 1 ; mmy = 1 ; mmz = 1
    allocate(alpc(1,1,1),detgc(1,1,1), g11c(1,1,1),g22c(1,1,1),g33c(1,1,1), &
             alpf(1,1,1),detgf(1,1,1), g11f(1,1,1),g22f(1,1,1),g33f(1,1,1), &
             alpe(1,1,1),detge(1,1,1), g11e(1,1,1),g22e(1,1,1),g33e(1,1,1), &
  end select  
  END SUBROUTINE initialize_metric

  FUNCTION gmunu(pos,i,j)
!
! pos : 0 = Centered
!       1 = Face
!       2 = Edge
! i,j : Index in the metric tensor to be computed 
!
  integer, intent(in) :: pos, i, j
  real, dimension(mx,my,mz) :: gmunu
!
  select case (metric_name)
  case ('special_relativity')
    If (i.eq.j) Then
      If (i.eq.0) Then
        gmunu=-1.
      else
        gmunu=1.
      endif
    else
      gmunu = 0.
    endif
  case ('FRW')
    If (i.eq.j) Then
      If (i.eq.0) Then
        gmunu=-1. 
      else 
        gmunu=scale_factor**2
      endif
    else
      gmunu = 0.
    endif  
  end select
  END FUNCTION gmunu
!
  FUNCTION pdgmunu(pos,i,j,k)
!
! pos : 0 = Centered
!       1 = Face
!       2 = Edge
! i,j : Index in the metric tensor to be computed 
! k   : Index with respect to which the metric tensor is differentiated
!
  integer, intent(in) :: pos, i, j
  real, dimension(mx,my,mz) :: gmunu
!
  select case (metric_name)
  case ('special_relativity')
    If (i.eq.j) Then
      If (i.eq.0) Then
        gmunu=-1.
      else
        gmunu=1.
      endif
    else
      gmunu = 0.
    endif
  case ('FRW')
    If (i.eq.j) Then
      If (i.eq.0) Then
        gmunu=-1. 
      else 
        gmunu=scale_factor**2
      endif
    else
      gmunu = 0.
    endif  
  end select
  END FUNCTION gmunu
!
  SUBROUTINE determinantg(pos)
!
! pos : 0 = Centered
!       1 = Face
!       2 = Edge
  integer, intent(in) :: pos
  select case (metric_name)
  case ('special_relativity')
  case ('FRW')
    detg = scale_factor**3
  end select
  END SUBROUTINE determinantg



END MODULE metric_components
