! $Id: polysolve_real8.f90,v 1.1 2005/03/30 13:07:23 troels_h Exp $
!***********************************************************************
Module PolySolve

Implicit None

Real, Parameter :: one = 1.0
logical         :: pdebug

Contains
!-----------------------------------------------------------------------
! Routine to compute the complex square root
! based on numerical recipes chap. 5.4.
! Note that the chosen branch cut in the complex plane
! - here taken to be the negative real axis -
! is not enforced if as xI is input -0.0.
! This is at least true for the intel compiler ver 7.1
! If it is important run sign_test, found in
! Testing subdir.
!-----------------------------------------------------------------------
SUBROUTINE SQRTC(xR,xI,yR,yI)
  Real*8, intent(in)  :: xR, xI
  Real*8, intent(out) :: yR, yI
  Real*8              :: w, t1, t2, sgnd, m
  Real*8, Parameter   :: half = 0.5
  If ((xR.eq.0).and.(xI.eq.0)) Then
    yR = 0
    yI = 0
  Else
    m = max(abs(xR), abs(xI))
    t1 = abs(xR/m)
    t2 = abs(xI/m)
    w = sqrt(m)*sqrt(0.5*(t1 + sqrt(t1*t1 + t2*t2)))
    m = 0.5 * abs(xI)/w
    t1 = sign(half, xR) + half
    t2 = one - t1
    sgnd = sign(one, xI)
    yR = t1*w + t2*m
    yI = sgnd*(t1*m + t2*w)
  End If
END SUBROUTINE SQRTC
!-----------------------------------------------------------------------
! Routine to compute the complex square root
! based on numerical recipes chap. 5.4.
! Does not check if input argument is zero
! and save an if line
!-----------------------------------------------------------------------
! Cost: Total count (Log etc x 3): 40
! Intrinisics    : 6
! Add/Multiply   : 16
! Log,Sqrt,Divide: 6
! IF lines       : 0
!-----------------------------------------------------------------------
SUBROUTINE SQRTC_No_Zero(xR,xI,yR,yI)
  Real*8, intent(in)  :: xR, xI
  Real*8, intent(out) :: yR, yI
  Real*8              :: w, t1, t2, sgnd, m
  Real*8, Parameter   :: half = 0.5
  t1 = abs(xR)
  t2 = abs(xI)
  m = max(t1, t2)
  t1 = t1/m
  t2 = t2/m
  w = sqrt(m)*sqrt(0.5*(t1 + sqrt(t1*t1 + t2*t2)))
  m = 0.5 * abs(xI)/w
  t1 = sign(half, xR) + half
  t2 = one - t1
  sgnd = sign(one, xI)
  yR = t1*w + t2*m
  yI = sgnd*(t1*m + t2*w)
END SUBROUTINE SQRTC_No_Zero
!-----------------------------------------------------------------------
! Routine to compute the complex square root
! based on numerical recipes chap. 5.4.
! Does not check if input argument is zero
! and save an if line. This is an 2-d array version.
!-----------------------------------------------------------------------
! Cost: Total count (Log etc x 3): 39
! Intrinisics    : 6
! Add/Multiply   : 15
! Log,Sqrt,Divide: 6
!-----------------------------------------------------------------------
SUBROUTINE SQRTC_No_Zero_2d(xR,xI,yR,yI,mx,my)
  integer, intent(in) :: mx, my
  Real*8, dimension(mx,my), intent(in)  :: xR, xI
  Real*8, dimension(mx,my), intent(out) :: yR, yI
  Real*8, dimension(mx,my)              :: w, t1, t2, sgnd, m
  Real*8, Parameter   :: half = 0.5, mone = -1., zero=0., tiny = 1e-30
  t1 = abs(xR)
  t2 = abs(xI)
  m = merge(t1, t2, t1 .gt. t2) + tiny
  t1 = t1/m
  t2 = t2/m
  w = sqrt(m)*sqrt(half*(t1 + sqrt(t1*t1 + t2*t2))) + tiny
  m = half * abs(xI)/w
  t1 = merge(one, zero, xR.ge.zero)
  t2 = one - t1
  sgnd = merge(one, mone, xI.ge.zero)
  yR = t1*w + t2*m
  yI = sgnd*(t1*m + t2*w)
END SUBROUTINE SQRTC_No_Zero_2d
!-----------------------------------------------------------------------
!
! Routine to find the roots in a second degree polynomial.
! based on numerical recipes chap. 5.6
! This is the general case of complex coefficients
!-----------------------------------------------------------------------
! Cost: Total count (Log etc x 3): 47
! Intrinisics    : 8
! Add/Multiply   : 30
! Log,Sqrt,Divide: 3
!-----------------------------------------------------------------------
SUBROUTINE P2_Roots(R1R, R1I, R2R, R2I, a0R, a0I, a1R, a1I, mx, my)
  integer, intent(in) :: mx, my
  real*8, dimension(mx,my), intent(in)  :: a0R,a0I,a1R,a1I
  Real*8, dimension(mx,my), intent(out) :: R1R, R1I, R2R, R2I
  Real*8, dimension(mx,my) :: dR, dI, RRZ, RIZ
  Real*8, dimension(mx,my) :: Sgn, SqdR, SqdI, t1, t2, t3
  Real*8, parameter        :: half = 0.5, mhalf = -0.5, &
          four=4., two=2.0
  logical, dimension(mx,my)              :: determinant

  dR = a1R*a1R - a1I*a1I - four*a0R
  dI = two*a1R*a1I - four*a0I
  RRZ = mhalf*a1R
  RIZ = mhalf*a1I
  Call SQRTC_No_Zero_2d(dR, dI, sqdR, sqdI, mx, my)
  Sgn = a1R*sqdR + a1I*sqdI
  t1 = merge(half, mhalf, sgn.ge.0)
  R1R = RRZ - t1*SqdR
  R1I = RIZ - t1*SqdI
! Complex division
! Done with guard for overflow
  t1 = abs(R1R)
  t2 = abs(R1I)
  t3 = merge(t1, t2, t1 .gt. t2)        ! vector version of max(abs(R1R), abs(R1I))
  t1 = R1R/t3
  t2 = R1I/t3
  t3 = one/(R1R*t1 + R1I*t2)
  R2R = (a0R*t1 + a0I*t2)*t3
  R2I = (a0I*t1 - a0R*t2)*t3
! Done the fast way
!  t3 = 1.0/(R1R**2 + R1I**2)
!  R2R = (a0R*R1R + a0I*R1I)*t3
!  R2I = (a0I*R1R - a0R*R1I)*t3
! Complex division finished
  determinant = ((dR.eq.0.0).and.(dI.eq.0.0))
  R1R = Merge(RRZ, R1R, determinant)
  R1I = Merge(RIZ, R1I, determinant)
  R2R = Merge(RRZ, R2R, determinant)
  R2I = Merge(RIZ, R2I, determinant)
END SUBROUTINE P2_Roots
!-----------------------------------------------------------------------
! Routine to find a real root in a third degree polynomial
! based on numerical recipes chap. 5.6
!-----------------------------------------------------------------------
! Cost: Total count (Log etc x 3): 50
! Intrinisics    : 5
! Add/Multiply   : 24
! Log,Sqrt,Divide: 7
!-----------------------------------------------------------------------
SUBROUTINE P3_Root(Root, a0, a1, a2, mx, my)
  integer, intent(in) :: mx, my
  real*8, dimension(mx,my), intent(in)  :: a0,a1,a2
  real*8, dimension(mx,my), intent(out) :: Root
  real*8, dimension(mx,my) :: q, r, q3, r2, Root1, t2
  logical, dimension(mx,my) :: r2ltq3
  real*8, parameter  :: twopi=2.0*3.141592653589793238462643383279502884197169399375105820974
  real*8, parameter  :: two=2., nine=9.0, twoseven=27., &
     one9 = 1./9., one54 = 1./54., one3 = 1./3., zero=0.0, three=3.0, mtwo=-2.0
  r2 = a2**2
  q = one9*(r2 - three*a1)
  r = one54*(a2*(two*r2 - nine*a1) + twoseven*a0)
  q3 = q**3
  r2 = r**2
!
! Three Roots, which one should we choose ??
!
!  Root1 = mtwo*sqrt(q)*Cos(one3*(acos(r/sqrt(q3))-twoPI))
!  Root1 = mtwo*sqrt(q)*Cos(one3*(acos(r/sqrt(q3))+twoPI))
  r2ltq3 = r2.lt.q3
  t2 = Merge(r/sqrt(abs(q3)),one,r2ltq3)
  Root1 = mtwo*sqrt(abs(q))*Cos(one3*(acos(t2)))
  t2 = abs(r)+sqrt(abs(r2-q3))
  t2 = t2**(one3)
  r = Merge(-t2, t2, r.ge.zero)
  Root = Merge(q/r,zero,r.ne.zero)
  Root = r + Root
  Root = Merge(Root1, Root,r2ltq3)
  Root = Root - one3*a2
END SUBROUTINE P3_Root
!
! Routine to find the roots of a fourth degree polynomial
! Based on the method outlined at 
!            http://mathworld.wolfram.com/QuarticEquation.html
! using the stable way to find roots in second and third degree
! polynomials outlined in Numerical Recipes chap. 5.6.
! The polynomial is supposed to be written in a "Normalised" fashion
!                 x**4 + a3 x**3 + ... + a0, Im(ai) = 0
! The coeeficients are coming from a slice in the stagger code,
! so they are all 2-dim arrays.
! The roots are returned in the Roots array, like
!           Roots = (/ root1, ... /)
!-----------------------------------------------------------------------
! Cost: Total count (Log etc x 3): 115
! Intrinisics    : 14
! Add/Multiply   : 77
! Log,Sqrt,Divide: 8 
! Total Cost inkl sub routine calls : 339 (33 Log, power, sqrt, divides)
!-----------------------------------------------------------------------
SUBROUTINE P4_Roots (ReRoots, ImRoots, a0, a1, a2, a3, mx, my)

  integer, intent(in)                                :: mx,my
  real*8, dimension(mx,my), intent(in) :: a0,a1,a2,a3
  real*8, dimension(mx,my,4), intent(out) :: ReRoots, ImRoots
  real*8, dimension(mx,my) :: &
                          sq0r, sq1r, z11r, z10r, z21r, z20r, &
                          sq0i, sq1i, z11i, z10i, z21i, z20i, &
                          z0, z1, z2, y, sgn, gthan,lthan,xr,xi,yr,yi, &
                          ReRoot1, ReRoot2, ImRoot1, ImRoot2
  integer :: ix, iy
  real*8, parameter  :: zero = 0., two=2., mone= -1., half=0.5, four=4.0
  logical, dimension(mx,my) :: logmerge
  character(len=80):: id='$Id: polysolve_real8.f90,v 1.1 2005/03/30 13:07:23 troels_h Exp $'
  if (id.ne.' ') print *,id; id=' '

  ! First find a real root in a third degree poly
  z0 = four*a2*a0 - a1**2 - a3**2*a0
  z1 = a1*a3 - four*a0
  z2 = -a2
  call P3_Root(y, z0, z1, z2, mx, my)
  ! Then find roots in the two resulting 2nd degree polynomials
  ! x**2 + z11 x + z10 = 0
  ! x**2 + z21 x + z20 = 0
  ! Where z[ij] are themselves roots in two 2nd degree polynomials:
  ! z[1,2]0 = 1/2*(y  -+ Sqrt(y**2 - 4a0) )
  ! z[1,2]1 = 1/2*(a3 +- Sqrt(a3**2 - 4(a2-y)) )
  ! Given as
  ! x**2 - a3 x + (a2 - y) = 0
  ! x**2 - y x + a0 = 0
  Sq0R = a3*a3 - four*(a2 - y)
  z0 = Sqrt(Abs(Sq0R))
  logmerge = Sq0R.ge.zero
  Sq0I = merge(zero, z0, logmerge)
  Sq0R = merge(z0, zero, logmerge)
  ! a3 by contruction as 2.0*(1.0 + Gamma) is always positive
  !! sgn = merge(one, mone, a3.ge.zero)
  ! sgn = 1.
  !! gthan = half*(sgn + 1.) 
  ! gthan = 1.
  !! lthan = one - gthan
  ! lthan = 0.
  !! z1 = a3 + sgn*Sq0R
  z1 = a3 + Sq0R
  XR = half*z1
  !! XI = half*sgn*Sq0I
  XI = half*Sq0I
! Complex division
! Done with guard for overflow
  ReRoot1 = abs(z1)
  ReRoot2 = abs(Sq0I)
  z0 = Merge(ReRoot1, ReRoot2,ReRoot1 .gt. ReRoot2)
  YR = two*((a2 - y)/z0)/((z1/z0)*z1 + (Sq0I/z0)*Sq0I)
! Done the fast way
!  YR = 2.0*((a2 - y))/(z1**2 + Sq0I**2)
! Complex division finished
  !! YI = -sgn*Sq0I*YR
  YI = -Sq0I*YR
  YR = z1*YR
  !! Z11R = gthan*XR + lthan*YR
  !! Z21R = gthan*YR + lthan*XR
  !! Z11I = gthan*XI + lthan*YI
  !! Z21I = gthan*YI + lthan*XI
  Z11R = XR
  Z21R = YR
  Z11I = XI
  Z21I = YI
  Sq1R = y*y-four*a0
  z0 = Sqrt(Abs(Sq1R))
  logmerge = Sq1R.ge.zero
  Sq1I = merge(zero, z0, logmerge)
  Sq1R = merge(z0, zero, logmerge)
  sgn  = merge(one, mone, y.ge.zero)
  gthan = half*(sgn + one)
  lthan = one - gthan
  z1 = y + sgn*Sq1R
  XR = half*z1
  XI = half*sgn*Sq1I
! Complex division
! Done with guard for overflow
  ReRoot1 = abs(z1)
  ReRoot2 = abs(Sq1I)
  z0 = Merge(ReRoot1, ReRoot2,ReRoot1 .gt. ReRoot2)
  YR = two*(a0/z0)/((z1/z0)*z1 + (Sq1I/z0)*Sq1I)
! Done the fast way
!  YR = 2.0*(a0)/(z1**2 + Sq1I**2)
! Complex division finished
  YI = -sgn*Sq1I*YR
  YR = z1*YR
  Z20R = gthan*XR + lthan*YR
  Z10R = gthan*YR + lthan*XR
  Z20I = gthan*XI + lthan*YI
  Z10I = gthan*YI + lthan*XI
  Call P2_Roots(ReRoot1,ImRoot1,ReRoot2,ImRoot2,z10R,z10I,z11R,z11I,mx,my)
  ReRoots(:,:,1) = ReRoot1
  ImRoots(:,:,1) = ImRoot1
  ReRoots(:,:,2) = ReRoot2
  ImRoots(:,:,2) = ImRoot2
  Call P2_Roots(ReRoot1,ImRoot1,ReRoot2,ImRoot2,z20R,z20I,z21R,z21I,mx,my)
  ReRoots(:,:,3) = ReRoot1
  ImRoots(:,:,3) = ImRoot1
  ReRoots(:,:,4) = ReRoot2
  ImRoots(:,:,4) = ImRoot2
END SUBROUTINE P4_Roots

SUBROUTINE Polish_Roots(ReRoots, ImRoots, a0, a1, a2, a3, mx, my, nr)

  integer, intent(in) :: mx,my,nr
  real*8, dimension(mx,my), intent(in) :: a0,a1,a2,a3
  real*8, dimension(mx,my)             :: B, C, D, E
  real*8, dimension(mx,my,4), intent(inout) :: ReRoots, ImRoots
  integer :: ix, iy, ir, inr
  real*8, dimension(mx,my) :: fR, fI, fdR, fdI, TR, TI, T2
  pdebug = .false.
  B = 0.25*a3 - 0.25
  D = a1 + B*(-2.*a2 + B*(3. + 8.*B))
  C = a2 - B*(2. + 6.*B) - D
  fR = B**2
  E = a0 - fR**2 - fR*(C + D) - C*D
  do ir = 1, 4
    do inr = 1, nr
! First we compute p(x) and dp/dx
      fdR = ReRoots(:,:,ir) + B
      fR = fdR**2 - ImRoots(:,:,ir)**2
      TR = fR + D
      TI = fdR*ImRoots(:,:,ir)
      TI = TI + TI
      T2 = fR + ReRoots(:,:,ir) + C
      fI = TI + ImRoots(:,:,ir)
      fdI= D*fI + 2.*(ImRoots(:,:,ir)*(TR + T2) + fdR*(TI + fI))
      fdR= D*T2 + 2.*(fdR*(TR + T2) - ImRoots(:,:,ir)*(TI + fI))
      fR = E + T2*TR - fI*TI
      fI = T2*TI + fI*TR
! Then we compute f/fd
! Complex division
! Done with guard for overflow
      TR = abs(fdR)
      TI = abs(fdI)
      T2 = merge(TR, TI, TR .gt. TI)
      TR = fdR/T2
      TI = fdI/T2
      T2 = 1./(fdR*TR + fdI*TI)
      ReRoots(:,:,ir) = ReRoots(:,:,ir) - (fR*TR + fI*TI)*T2
      ImRoots(:,:,ir) = ImRoots(:,:,ir) - (fI*TR - fR*TI)*T2
! Done the fast way
!      T2 = 1./(fdR**2 + fdI**2)
!      ReRoots(:,:,ir) = ReRoots(:,:,ir) - (fR*fdR + fI*fdI)*T2
!      ImRoots(:,:,ir) = ImRoots(:,:,ir) - (fI*fdR - fR*fdI)*T2
! Complex division finished
    end do
  end do
  pdebug = .false.
END SUBROUTINE Polish_Roots

SUBROUTINE rhd_nr(X, Y2, hm1, alpha, A, mx, my)
  integer, intent(in) :: mx,my
  real*8,                   intent(in)   :: A
  real*8, dimension(mx,my), intent(in)   :: X, Y2
  real*8, dimension(mx,my), intent(inout):: hm1, alpha
  integer :: iter
  real*8, dimension(mx,my) :: M11, M12, M21, M22, W, F1, F2,a2
  real*8, dimension(mx,my) :: Det, T1, corr1, corr2, h
  real*8                   :: eps, epst
  real*8, parameter        :: epsi = 1.e-6, two = 2.0
  
  do iter=1, 5
    a2 = alpha**2                       ! a2 = (vW)^2
    W = Sqrt(a2 + one)                  ! Find Gamma factor from W^1 - 1
    h = hm1 + one                       ! Enthalpy
    F1 = h**2*a2 - Y2                   ! F1 is (guess_Y2 - Y2) [= (g_ij T^0i T^0j)]
    M21 = W - A/W                       ! D[F2,hm]
    F2 = a2/(W + one) + hm1*M21 - X     ! F2 is (guess_X - X) [= T00p/D]
    T1 = two*h*alpha
    M12 = T1*h                          ! D[F1,alpha]
    M11 = T1*alpha                      ! D[F1,hm]
    M22 = alpha*(h/W + hm1*A/W**3)       ! D[F2,alpha]
    Det = M11*M22-M12*M21 ! -T1*((1.-A)*h+(A-1.+(1.-2.*A)*hm1)*a2)
    Det = one/Det ! W**3/Det                     ! 1/(Determinant of matrix)
    Corr1 = Det*(M22*F1 - M12*F2)      ! Correction values to Y2, X
    Corr2 = Det*(M11*F2 - M21*F1)
    eps  = maxval(abs(corr1/(hm1+epsi)))
    epst = maxval(abs(corr2/(alpha+epsi)))
    eps  = max(eps, epst)
    hm1 = hm1 - Corr1
    alpha = alpha - Corr2
  end do
  
END SUBROUTINE rhd_nr
  
SUBROUTINE rhd_nr2(X, Y2, hm1, A, mx, my, iter)
  integer, intent(in) :: mx,my
  real*8,                   intent(in)   :: A
  real*8, dimension(mx,my), intent(in)   :: X, Y2
  real*8, dimension(mx,my), intent(inout):: hm1
  integer, intent(out) :: iter
  integer :: ix, iy, loc(2)
  real*8,        dimension(mx,my) :: GX, DGXDhm1
  real*8, dimension(mx,my) :: h, W, hW, corr, MW
  real*8                   :: eps, epst
  real*8, parameter        :: epsi = 1.e-8
  Integer             , parameter        :: MaxIt = 100

  do iter=1, MaxIt
    h = hm1 + 1.
    hW = Sqrt(h**2 + Y2)
    W = hW/h
    mW = W - A/W
    GX = Y2/(h**2*(W+1.)) + hm1*mW
    DGXDhm1 = mW - Y2*(A*hm1 + hW*W)/hW**3
    corr = (GX - X)/DGXDhm1
    hm1 = hm1 - corr
    eps  = maxval(abs(corr/(hm1+epsi)))
    If (eps.lt.1e-4) exit
  end do
  If (iter.eq.MaxIt+1) Then
  If (eps.gt.1e-3) Then
    Print *, "The Maximum ",Maxit, " iterations have been performed."
    Print *,"Probably we have not converged to right zero."
    loc = maxloc(abs(corr/(hm1+epsi)))
    ix = loc(1)
    iy = loc(2)
    Print *, "The max relative error is ",eps
    Print *, "The location is ", loc
    Print *, "The correction factor is ", corr(ix, iy)
    Print *, "And (hm1,W,X,GX,Y2) are given as"
    Print *, hm1(ix, iy)
    Print *, W(ix, iy)
    Print *, X(ix, iy)
    Print *, GX(ix, iy)
    Print *, Y2(ix, iy)
  End If
  End If
END SUBROUTINE rhd_nr2
!
! Input is: [We suppose that all variables are
!            either given from special relativity
!            or are evaluated in the pseudo FIDO frame
!            where ds^2 = -dt^2 + gamma_ij dx^i dx^j,
!            and have already been corrected for 
!            metric prefactors]
!  - T00p: Relativistic mass density T00 - D
!          T00 - D = -T^0_0 - D
!                  = rho h (U^t)^2 - P - rho U^t
!  - SPD: Squared momentum density
!         SPD = g^ij T^0_i T^0_j [T^0_i= rho*h*U^t*U_i]
! Output is:
!  - vW : Gamma*velocity = gamma^2 - 1
!         (Important trick: W - 1 = vW/(W + 1))
!  - hm1: Relativistic enthalpy corrected for rest mass
!         hm1 = h - 1 = e_int + P/rho
!            (= Gas_Gamma*e_int, for ideal equation of state)
!
SUBROUTINE rhdsolve (D,T00p,SPD,vW,hm1)

  USE params
!  USE metric

  integer ix,iy,iz,iter
  real*8 :: A, gamma2, TwoGamma
  real, dimension(mx,my,mz), intent(in) :: D,T00P,SPD
  real, dimension(mx,my,mz), intent(out) :: vW, hm1
  real*8, dimension(mx,my) :: Y2,xx2,X,hm1xy
  real*8, dimension(mx,my) :: a0,a1,a2,a3, Root, err
  real*8, dimension(mx,my,4) :: ReRoots, ImRoots
  real*8, parameter :: mone = -1., tiny=1e-30

  A = (gamma -1.0)/gamma
  gamma2 = gamma**2
  twogamma = gamma + gamma
! a3 is const, so we take it out of the loop
  a3 = 2.0*(1.0 + Gamma)
!$omp parallel do private(iz, a0, a1, a2, ReRoots, ImRoots, X, Y2, XX2)
  do iz =1,mz
    XX2 = 1./D(:,:,iz)
    X = T00p(:,:,iz)*XX2
    Y2  = SPD(:,:,iz)*XX2**2
    xx2 = X*(X + 2.0)
    a0 = Gamma2*(1.0 + y2)*(Y2 - xx2) 
    xx2 = 1.0 - Gamma*xx2
    a1 = TwoGamma*(xx2 + (1. + Gamma)*y2)
    a2 = (1.0 + Gamma*(3.0 + xx2 + 2.*y2))
    ! Find roots
    call P4_Roots(ReRoots, ImRoots, a0, a1, a2, a3, mx, my)
    ! Set the real part of all roots with Im(Root)/Re(Root) > 1e-3 to -1
    ReRoots = Merge(ReRoots, 0.0001, Abs(ImRoots/(ReRoots+tiny)).lt.1.e-3)
    ! Select root (should be refined, but works ;-)
    hm1xy = Maxval(ReRoots,dim=3)
    ! Polish the Roots with a newton raphson step
    if (idbg .gt. -20) Call rhd_nr2(X, Y2, hm1xy, A, mx, my,iter)
    vW(:,:,iz) = Sqrt(Y2/(1.0 + hm1xy)**2)
    hm1(:,:,iz) = hm1xy
  end do
  ! Extra check. Recompute D, T00p from found hm1, vW and compare.
  if ((idbg .gt. 2) .or. (idbg .eq. -40)) call rhdsolve_error(D,T00p,SPD,vW,hm1)
END SUBROUTINE rhdsolve

SUBROUTINE rhdsolve_error(D,T00p,SPD,vW,hm1)
  USE Params
  
  real, dimension(mx,my,mz), intent(in) :: &
  	  D,T00p,SPD
  real, dimension(mx,my,mz), intent(in) :: &
  	  vW, hm1
  real, dimension(mx,my,mz) :: &
  	  W, Y, Yt, T00pt
  real :: A, MaxT, MaxY
  integer, dimension(3) :: loc
  integer :: ix, iy, iz
!	
! Remember W = Sqrt(vW**2 + 1) and
! W - 1 = vW**2/(W + 1)
! Both computed in a stable way from W**2 - 1.
!
  A = (gamma -1.0)/gamma
  W = Sqrt(vW**2 + 1.0)
  Yt = D*(hm1 + 1.)*vW
  Y  = Sqrt(SPD)
  T00pt = D*(hm1*W + vW**2/(W+1.) - A*hm1/W)
  MaxY = MaxVal(Abs((Y - Yt)/(Y+1e-15)))
  MaxT = MaxVal(Abs(T00p-T00pt)/(T00p+1e-15))
  If ((MaxT.gt.1e-4).or.(MaxY.gt.1e-4)) Then
    Print *, "Errors is assesed by using the found vW, hm1 to recompute SPD, T00p"
    Print *, "Max Relative Error in T00p:", MaxT
    Print *, "Max Relative Error in T0i**2:", MaxY
    loc = maxloc((Abs(T00p-T00pt)/T00p))
    Print *, "Location of the Error in T00p is",loc(1), loc(2), loc(3)
    Print *, "T00p,D:", T00p(loc(1),loc(2),loc(3)), D(loc(1),loc(2),loc(3))
    Print *, "T00pt:", T00pt(loc(1),loc(2),loc(3))
    Print *, "Min, Max in hm1", minval(hm1), maxval(hm1)
    Print *, "Min, Max in vW ", minval(vW), maxval(vW)	
  Endif
END SUBROUTINE rhdsolve_error

END MODULE PolySolve
