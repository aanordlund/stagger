! $Id: metric.f90,v 1.3 2005/01/13 13:24:05 aake Exp $
!***********************************************************************
MODULE metric
  USE params

  implicit none
  
  character(len=18) :: metric_name    ! Type of metric in use
  real :: scale_factor                  ! Scale_factor for FRW metric
  logical :: time_dependence            ! If metric is time-dependent we
                                        ! have to update metric components every
                                        ! time-step
  integer :: metric_dim                 ! Number of spatial coordinates the 
                                        ! metric depends on going as (z,y,x)
  integer :: metric_type                ! 0 = Diagonal, 1 = [beta=0],
                                        ! 2 = Spatially diagonal,
                                        ! 3 = general.
  !    (-alp^2         )     (-alp^2 b1 b2 b3)
  !    (      g11      )     (b1    g11      )
  ! 0: (         g22   )  2: (b2       g22   ) 
  !    (            g33)     (b3          g33)
  !
  !    (-alp^2         )    (-alp^2 b1 b2 b3)
  !    (      g11g12g13)    (b1    g11g12g13)
  ! 1: (      g12g22g23) 3: (b2    g12g22g23)
  !    (      g13g23g33)    (b3    g13g23g33)
                                        
  real, allocatable, TARGET, dimension(:,:,:) :: &
       detgc, g11c,g12c,g13c,g22c,g23c,g33c,     &      ! spatial metric gamma_ij. 
       detgf, g11f,g12f,g13f,g22f,g23f,g33f,     &      ! detg=Sqrt(|det(gamma)|)
       detge, g11e,g12e,g13e,g22e,g23e,g33e,     &       
       alpc, b1c,b2c,b3c, &                             ! lapse and shift
       alpf, b1f,b2f,b3f, &
       alpe, b1e,b2e,b3e, &
       gttuc, gt1uc, gt2uc, gt3uc, g11uc, &             ! raised metric
       g12uc, g13uc, g22uc, g23uc, g33uc, &
       gttuf, gt1uf, gt2uf, gt3uf, g11uf, &
       g12uf, g13uf, g22uf, g23uf, g33uf, &
       gttue, gt1ue, gt2ue, gt3ue, g11ue, &
       g12ue, g13ue, g22ue, g23ue, g33ue
       
  real, Pointer, dimension(:,:,:) :: &
       detg, g11,g12,g13,g22,g23,g33, &
       alp, b1,b2,b3, &                 ! lapse and shift
       gttu, gt1u, gt2u, gt3u, g11u, &  ! raised metric
       g12u, g13u, g22u, g23u, g33u

  real, allocatable, dimension(:,:,:) :: &
       gpd110,gpd111,g112,g113, &       ! partial derivatives of spatial metric
       gpd120,gpd121,g122,g123, &
       gpd130,gpd131,g132,g133, &
       gpd220,gpd221,g222,g223, &
       gpd230,gpd231,g232,g233, &
       gpd330,gpd331,g332,g333, &
       alppd0,b1pd0,b2pd0,b3pd0, &      ! partial derivatives of lapse and shift
       alppd1,b1pd1,b2pd1,b3pd1, &
       alppd2,b1pd2,b2pd2,b3pd2, &
       alppd3,b1pd3,b2pd3,b3pd3

CONTAINS

  SUBROUTINE initialize_metric
  select case (metric_name)
  case ('special_relativity')
    time_dependence = .false.
    metric_dim = 0.
    metric_type = 0.
    allocate(alpc(1,1,1),detgc(1,1,1), g11c(1,1,1),g22c(1,1,1),g33c(1,1,1), &
             gttuc(1,1,1),g11uc(1,1,1),g22uc(1,1,1),g33uc(1,1,1), &
             alpf(1,1,1),detgf(1,1,1), g11f(1,1,1),g22f(1,1,1),g33f(1,1,1), &
             gttuf(1,1,1),g11uf(1,1,1),g22uf(1,1,1),g33uf(1,1,1), &
             alpe(1,1,1),detge(1,1,1), g11e(1,1,1),g22e(1,1,1),g33e(1,1,1), &
             gttue(1,1,1),g11ue(1,1,1),g22ue(1,1,1),g33ue(1,1,1))
    alpc = 1.0 ; detgc = 1.0 ; g11c = 1.0 ; g22c = 1.0 ; g33c = 1.0
    gttuc=-1.0 ; g11uc = 1.0 ; g22uc= 1.0 ; g33uc= 1.0
    alpf = 1.0 ; detgf = 1.0 ; g11f = 1.0 ; g22f = 1.0 ; g33f = 1.0
    gttuf=-1.0 ; g11uf = 1.0 ; g22uf= 1.0 ; g33uf= 1.0
    alpe = 1.0 ; detge = 1.0 ; g11e = 1.0 ; g22e = 1.0 ; g33e = 1.0
    gttue=-1.0 ; g11ue = 1.0 ; g22ue= 1.0 ; g33ue= 1.0
  case ('FRW')
    time_dependence = .true.
    metric_dim = 0.
    metric_type = 0.
    allocate(alpc(1,1,1),detgc(1,1,1), g11c(1,1,1),g22c(1,1,1),g33c(1,1,1), &
             gttuc(1,1,1),g11uc(1,1,1),g22uc(1,1,1),g33uc(1,1,1), &
             alpf(1,1,1),detgf(1,1,1), g11f(1,1,1),g22f(1,1,1),g33f(1,1,1), &
             gttuf(1,1,1),g11uf(1,1,1),g22uf(1,1,1),g33uf(1,1,1), &
             alpe(1,1,1),detge(1,1,1), g11e(1,1,1),g22e(1,1,1),g33e(1,1,1), &
             gttue(1,1,1),g11ue(1,1,1),g22ue(1,1,1),g33ue(1,1,1))
  end select  
  END SUBROUTINE initialize_metric

  SUBROUTINE switch_metric(pos)
!
! Purpose: Switch between different
! centerings of the metrics.
! In the time independent case this is
! done by pointers, making it very fast.
!
! pos : 0 = Centered
!       1 = Face
!       2 = Edge
!
!
! FIXME - Only works for time-independent case!
!
  integer, intent(in) :: pos
  select case(pos)
  case (0) ! Centered grid
    alp  =>  alpc ; b1   =>   b1c ; b2   =>   b2c ; b3   =>   b3c
    detg => detgc ; g11  =>  g11c ; g12  =>  g12c ; g13  =>  g13c
    g22  =>  g22c ; g23  =>  g23c ; g33  =>  g33c
    gttu => gttuc ; gt1u => gt1uc ; gt2u => gt2uc ; gt3u => gt3uc
    g11u => g11uc ; g12u => g12uc ; g13u => g13uc ; g22u => g22uc
    g23u => g23uc ; g33u => g33uc
  case (1) ! Face grid
    alp  =>  alpf ; b1   =>   b1f ; b2   =>   b2f ; b3   =>   b3f
    detg => detgf ; g11  =>  g11f ; g12  =>  g12f ; g13  =>  g13f
    g22  =>  g22f ; g23  =>  g23f ; g33  =>  g33f
    gttu => gttuf ; gt1u => gt1uf ; gt2u => gt2uf ; gt3u => gt3uf
    g11u => g11uf ; g12u => g12uf ; g13u => g13uf ; g22u => g22uf
    g23u => g23uf ; g33u => g33uf
   case (2) ! Edge grid
    alp  =>  alpe ; b1   =>   b1e ; b2   =>   b2e ; b3   =>   b3e
    detg => detge ; g11  =>  g11e ; g12  =>  g12e ; g13  =>  g13e
    g22  =>  g22e ; g23  =>  g23e ; g33  =>  g33e
    gttu => gttue ; gt1u => gt1ue ; gt2u => gt2ue ; gt3u => gt3ue
    g11u => g11ue ; g12u => g12ue ; g13u => g13ue ; g22u => g22ue
    g23u => g23ue ; g33u => g33ue
  end select
  END SUBROUTINE switch_metric
  
  FUNCTION gmunu(pos,i,j)
!
! pos : 0 = Centered
!       1 = Face
!       2 = Edge
! i,j : Index in the metric tensor to be computed 
!
  integer, intent(in) :: pos, i, j
  real, dimension(mx,my,mz) :: gmunu
!
  select case (metric_name)
  case ('special_relativity')
    If (i.eq.j) Then
      If (i.eq.0) Then
        gmunu=-1.
      else
        gmunu=1.
      endif
    else
      gmunu = 0.
    endif
  case ('FRW')
    If (i.eq.j) Then
      If (i.eq.0) Then
        gmunu=-1. 
      else 
        gmunu=scale_factor**2
      endif
    else
      gmunu = 0.
    endif  
  end select
  END FUNCTION gmunu

  SUBROUTINE determinantg(pos)
!
! pos : 0 = Centered
!       1 = Face
!       2 = Edge
  integer, intent(in) :: pos
  select case (metric_name)
  case ('special_relativity')
  case ('FRW')
    detg = scale_factor**3
  end select
  END SUBROUTINE determinantg

  FUNCTION expand_metric(f)
    real, dimension(mx,my,mz) :: expand_metric
    real, dimension(:,:,:), intent(in) :: f
    integer :: iy, iz
      select case (metric_dim)
      case(0)   ! The metric is independent of the spatial coordinates
!$omp parallel do private(iz)
        do iz = 1, mz
          expand_metric(:,:,iz) = f(1,1,1)
        enddo
      case(1)   ! The metric depends on the z-coordinate
!$omp parallel do private(iz)
        do iz = 1, mz
          expand_metric(:,:,iz) = f(1,1,iz)
        enddo
      case(2)   ! The metric depends on the {y,z}-coordinate
!$omp parallel do private(iz,iy)
        do iz = 1, mz
          do iy = 1, my
            expand_metric(:,iy,iz) = f(1,iy,iz)
          enddo
        enddo
      case(3)   ! The metric depends on all spatial coordinates
!$omp parallel do private(iz)
        do iz = 1, mz
          expand_metric(:,:,iz) = f(:,:,iz)
        enddo
      end select
  END FUNCTION expand_metric

  SUBROUTINE FindUt(ux,uy,uz,ut)
!
! Returns U_t from U_i and normalisation constraint.
! Notice in SR U_t is just W.
!    
    real, dimension(mx,my,mz) :: ux,uy,uz,ut
    integer :: iz, iy
    call switch_metric(0) ! Switch metric to centered grid
    select case (Metric_type)
    case (0)    ! We have a diagonal metric
                ! From g^mu nu U_mu U_nu = -1 
                ! we get g^tt (U_t)^2 + g^ii (U_i)^2 = -1
                ! and furthermore -1/g^tt = alpha [alp]
      select case (metric_dim)
      case(0)   ! The metric is independent of the spatial coordinates
!$omp parallel do private(iz)
        do iz = 1, mz
          ut(:,:,iz) = Sqrt((1.0 + g11u(1,1,1)*ux(:,:,iz)**2 + & 
                                   g22u(1,1,1)*uy(:,:,iz)**2 + &
                                   g33u(1,1,1)*uz(:,:,iz)**2))*alp(1,1,1)
        end do
      case(1)   ! The metric depends on the z-coordinate
!$omp parallel do private(iz)
        do iz = 1, mz
          ut(:,:,iz) = Sqrt((1.0 + g11u(1,1,iz)*ux(:,:,iz)**2 + & 
                                   g22u(1,1,iz)*uy(:,:,iz)**2 + &
                                   g33u(1,1,iz)*uz(:,:,iz)**2))*alp(1,1,iz)
        end do
      case(2)   ! The metric depends on the {y,z}-coordinate
!$omp parallel do private(iz,iy)
        do iz = 1, mz
          do iy = 1, my
            ut(:,iy,iz) = Sqrt((1.0 + g11u(1,iy,iz)*ux(:,iy,iz)**2 + & 
                                      g22u(1,iy,iz)*uy(:,iy,iz)**2 + &
                                      g33u(1,iy,iz)*uz(:,iy,iz)**2))*alp(1,iy,iz)
          end do
        end do
      case(3)   ! The metric depends on all spatial coordinates
                ! Therefore g^mu nu is not precomputed and we use
                ! instead g^mu nu = 1/g_mu nu
!$omp parallel do private(iz)
        do iz = 1, mz
          ut(:,:,iz) = Sqrt((1.0 + ux(:,:,iz)**2/g11(:,:,iz) + & 
                                   uy(:,:,iz)**2/g22(:,:,iz) + &
                                   uz(:,:,iz)**2/g33(:,:,iz)))*alp(:,:,iz)
        end do
      end select
    case (1)
    case (2)
    end select    
  END SUBROUTINE FindUt

  FUNCTION Spatial_Dot_Lo(ux,uy,uz)
!
! Returns g^ij U_i U_j
!    
    real, dimension(mx,my,mz)             :: Spatial_Dot_Lo
    real, dimension(mx,my,mz), intent(in) :: ux,uy,uz
    integer :: iz, iy
    call switch_metric(0) ! Switch metric to centered grid
    select case (Metric_type)
    case (0)    ! We have a diagonal metric
                ! we get g^ii (U_i)^2
      select case (metric_dim)
      case(0)   ! The metric is independent of the spatial coordinates
!$omp parallel do private(iz)
        do iz = 1, mz
          Spatial_Dot_Lo(:,:,iz) = g11u(1,1,1)*ux(:,:,iz)**2 + & 
                                   g22u(1,1,1)*uy(:,:,iz)**2 + &
                                   g33u(1,1,1)*uz(:,:,iz)**2
        end do
      case(1)   ! The metric depends on the z-coordinate
!$omp parallel do private(iz)
        do iz = 1, mz
          Spatial_Dot_Lo(:,:,iz) = g11u(1,1,iz)*ux(:,:,iz)**2 + & 
                                   g22u(1,1,iz)*uy(:,:,iz)**2 + &
                                   g33u(1,1,iz)*uz(:,:,iz)**2
        end do
      case(2)   ! The metric depends on the {y,z}-coordinate
!$omp parallel do private(iz,iy)
        do iz = 1, mz
          do iy = 1, my
            Spatial_Dot_Lo(:,iy,iz) = g11u(1,iy,iz)*ux(:,iy,iz)**2 + & 
                                      g22u(1,iy,iz)*uy(:,iy,iz)**2 + &
                                      g33u(1,iy,iz)*uz(:,iy,iz)**2
          end do
        end do
      case(3)   ! The metric depends on all spatial coordinates
                ! Therefore g^mu nu is not precomputed and we use
                ! instead g^mu nu = 1/g_mu nu
!$omp parallel do private(iz)
        do iz = 1, mz
          Spatial_Dot_Lo(:,:,iz) = ux(:,:,iz)**2/g11(:,:,iz) + & 
                                   uy(:,:,iz)**2/g22(:,:,iz) + &
                                   uz(:,:,iz)**2/g33(:,:,iz)
        end do
      end select
    case (1)
    case (2)
    end select    
  END FUNCTION spatial_dot_lo

  SUBROUTINE Spatial_RaiseVector(pos,xlo,ylo,zlo,vxup,vyup,vzup)
!
! Raises a four vector using V^mu = g^mu nu V_nu
!    
    real, dimension(mx,my,mz), intent(in) :: xlo,ylo,zlo
    real, dimension(mx,my,mz), intent(out):: vxup,vyup,vzup
    integer,                   intent(in) :: pos
    integer :: iz, iy
    call switch_metric(pos)                     ! Switch the metric to the proper position
    select case (Metric_type)
    case (0)    ! We have a diagonal metric
                ! we get V^x = g^xx V_x = 1/g_xx V_x etc
      select case (metric_dim)
      case(0)   ! The metric is independent of the spatial coordinates
!$omp parallel do private(iz)
        do iz = 1, mz
          vxup(:,:,iz) =  g11u(1,1,1)*xlo(:,:,iz)
          vyup(:,:,iz) =  g22u(1,1,1)*ylo(:,:,iz)
          vzup(:,:,iz) =  g33u(1,1,1)*zlo(:,:,iz)
        end do
      case(1)   ! The metric depends on the z-coordinate
!$omp parallel do private(iz)
        do iz = 1, mz
          vxup(:,:,iz) =  g11u(1,1,iz)*xlo(:,:,iz)
          vyup(:,:,iz) =  g22u(1,1,iz)*ylo(:,:,iz)
          vzup(:,:,iz) =  g33u(1,1,iz)*zlo(:,:,iz)
        end do
      case(2)   ! The metric depends on the {y,z}-coordinate
!$omp parallel do private(iz,iy)
        do iz = 1, mz
          do iy = 1, my
            vxup(:,iy,iz) =  g11u(1,iy,iz)*xlo(:,iy,iz)
            vyup(:,iy,iz) =  g22u(1,iy,iz)*ylo(:,iy,iz)
            vzup(:,iy,iz) =  g33u(1,iy,iz)*zlo(:,iy,iz)
          end do
        end do
      case(3)   ! The metric depends on all spatial coordinates
                ! Therefore g^mu nu is not precomputed and we use
                ! instead g^mu nu = 1/g_mu nu
!$omp parallel do private(iz)
        do iz = 1, mz
          vxup(:,:,iz) =  xlo(:,:,iz)/g11(:,:,iz)
          vyup(:,:,iz) =  ylo(:,:,iz)/g22(:,:,iz)
          vzup(:,:,iz) =  zlo(:,:,iz)/g33(:,:,iz)
        end do
      end select
    case (1) ! We have a Kerr type metric. Ie.
             ! V^x = 1/g_xx V_x , V^y = 1/g_yy v_y
             ! V^t = (g_zz V_t - g_tz V_z)/D
             ! V^z = (g_tt V_z - g_tz V_t)/D
             !   D =  g_tt g_zz - (g_tz)^2 = Detg/(gxx*gyy)
    case (2)
    end select    
  END SUBROUTINE Spatial_RaiseVector

  SUBROUTINE RaiseVector(pos,xlo,ylo,zlo,tlo,vxup,vyup,vzup,vtup)
!
! Raises a four vector using V^mu = g^mu nu V_nu
!    
    real, dimension(mx,my,mz), intent(in) :: xlo,ylo,zlo,tlo
    real, dimension(mx,my,mz), intent(out):: vxup,vyup,vzup,vtup
    integer,                   intent(in) :: pos
    integer :: iz, iy
    call switch_metric(pos)                     ! Switch the metric to the proper position
    select case (Metric_type)
    case (0)    ! We have a diagonal metric
                ! we get V^t = g^tt V_t = 1/g_tt V_t etc
      select case (metric_dim)
      case(0)   ! The metric is independent of the spatial coordinates
!$omp parallel do private(iz)
        do iz = 1, mz
          vtup(:,:,iz) = -tlo(:,:,iz)*gttu(1,1,1)
          vxup(:,:,iz) =  xlo(:,:,iz)*g11u(1,1,1)
          vyup(:,:,iz) =  ylo(:,:,iz)*g22u(1,1,1)
          vzup(:,:,iz) =  zlo(:,:,iz)*g33u(1,1,1)
        end do
      case(1)   ! The metric depends on the z-coordinate
!$omp parallel do private(iz)
        do iz = 1, mz
          vtup(:,:,iz) = -tlo(:,:,iz)*gttu(1,1,iz)
          vxup(:,:,iz) =  xlo(:,:,iz)*g11u(1,1,iz)
          vyup(:,:,iz) =  ylo(:,:,iz)*g22u(1,1,iz)
          vzup(:,:,iz) =  zlo(:,:,iz)*g33u(1,1,iz)
        end do
      case(2)   ! The metric depends on the {y,z}-coordinate
!$omp parallel do private(iz,iy)
        do iz = 1, mz
          do iy = 1, my
            vtup(:,iy,iz) = -tlo(:,iy,iz)*gttu(1,iy,iz)
            vxup(:,iy,iz) =  xlo(:,iy,iz)*g11u(1,iy,iz)
            vyup(:,iy,iz) =  ylo(:,iy,iz)*g22u(1,iy,iz)
            vzup(:,iy,iz) =  zlo(:,iy,iz)*g33u(1,iy,iz)
          end do
        end do
      case(3)   ! The metric depends on all spatial coordinates
                ! Therefore g^mu nu is not precomputed and we use
                ! instead g^mu nu = 1/g_mu nu
!$omp parallel do private(iz)
        do iz = 1, mz
          vtup(:,:,iz) = -tlo(:,:,iz)/alp(1,1,iz)**2
          vxup(:,:,iz) =  xlo(:,:,iz)/g11(:,:,iz)
          vyup(:,:,iz) =  ylo(:,:,iz)/g22(:,:,iz)
          vzup(:,:,iz) =  zlo(:,:,iz)/g33(:,:,iz)
        end do
      end select
    case (1) ! We have a Kerr type metric. Ie.
             ! V^x = 1/g_xx V_x , V^y = 1/g_yy v_y
             ! V^t = (g_zz V_t - g_tz V_z)/D
             ! V^z = (g_tt V_z - g_tz V_t)/D
             !   D =  g_tt g_zz - (g_tz)^2 = Detg/(gxx*gyy)
    case (2)
    end select    
  END SUBROUTINE RaiseVector

  END MODULE metric
