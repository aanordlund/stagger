! $Id: mops_diagonal.f90,v 1.2 2005/01/12 18:41:06 troels_h Exp $
!***********************************************************************
MODULE metric_operations
  USE params
  USE metric_components

  implicit none
  
CONTAINS
  !
  FUNCTION expand_metric(f)
    real, dimension(mx,my,mz) :: expand_metric
    real, dimension(:,:,:), intent(in) :: f
    integer :: iy, iz
      select case (metric_dim)
      case(0)   ! The metric is independent of the spatial coordinates
!$omp parallel do private(iz)
        do iz = 1, mz
          expand_metric(:,:,iz) = f(1,1,1)
        enddo
      case(1)   ! The metric depends on the z-coordinate
!$omp parallel do private(iz)
        do iz = 1, mz
          expand_metric(:,:,iz) = f(1,1,iz)
        enddo
      case(2)   ! The metric depends on the {y,z}-coordinate
!$omp parallel do private(iz,iy)
        do iz = 1, mz
          do iy = 1, my
            expand_metric(:,iy,iz) = f(1,iy,iz)
          enddo
        enddo
      case(3)   ! The metric depends on all spatial coordinates
!$omp parallel do private(iz)
        do iz = 1, mz
          expand_metric(:,:,iz) = f(:,:,iz)
        enddo
      end select
  END FUNCTION expand_metric
  !
  FUNCTION mmult(f,m)
    real, dimension(mx,my,mz) :: mmult
    real, dimension(mx,my,mz),   intent(in) :: f
    real, dimension(mmx,mmy,mmz),intent(in) :: m
    integer :: iy, iz
    select case (metric_dim)
    case(0)   ! The metric is independent of the spatial coordinates
!$omp parallel do private(iz)
      do iz = 1, mz
        mmult(:,:,iz) = m(1,1,1)*f(:,:,iz)
      end do
    case(1)   ! The metric depends on the y-coordinate
!$omp parallel do private(iz)
      do iz = 1, mz
        do iy = 1, my
          mmult(:,iy,iz) = m(1,iy,1)*f(:,iy,iz)
        end do
      end do
    case(2)   ! The metric depends on the {y,z}-coordinate
!$omp parallel do private(iz,iy)
      do iz = 1, mz
        do iy = 1, my
          mmult(:,iy,iz) = m(1,iy,iz)*f(:,iy,iz)
        end do
      end do
    case(3)   ! The metric depends on all spatial coordinates
!$omp parallel do private(iz)
      do iz = 1, mz
        mmult(:,:,iz) = m(:,:,iz)*f(:,:,iz)
      end do
    end select
  END FUNCTION mmult
  !
  FUNCTION madd(f,m)
    real, dimension(mx,my,mz) :: madd
    real, dimension(mx,my,mz),    intent(in) :: f
    real, dimension(mmx,mmy,mmz), intent(in) :: m
    integer :: iy, iz
    select case (metric_dim)
    case(0)   ! The metric is independent of the spatial coordinates
!$omp parallel do private(iz)
      do iz = 1, mz
        madd(:,:,iz) = m(1,1,1) + f(:,:,iz)
      end do
    case(1)   ! The metric depends on the y-coordinate
!$omp parallel do private(iz)
      do iz = 1, mz
        do iy = 1, my
          madd(:,iy,iz) = m(1,iy,1) + f(:,iy,iz)
        end do
      end do
    case(2)   ! The metric depends on the {y,z}-coordinate
!$omp parallel do private(iz,iy)
      do iz = 1, mz
        do iy = 1, my
          madd(:,iy,iz) = m(1,iy,iz) + f(:,iy,iz)
        end do
      end do
    case(3)   ! The metric depends on all spatial coordinates
!$omp parallel do private(iz)
      do iz = 1, mz
        madd(:,:,iz) = m(:,:,iz) + f(:,:,iz)
      end do
    end select
  END FUNCTION madd
  !
  FUNCTION mdiv(f,m)
    real, dimension(mx,my,mz) :: mdiv
    real, dimension(mx,my,mz),    intent(in) :: f
    real, dimension(mmx,mmy,mmz), intent(in) :: m
    integer :: iy, iz
    select case (metric_dim)
    case(0)   ! The metric is independent of the spatial coordinates
!$omp parallel do private(iz)
      do iz = 1, mz
        mdiv(:,:,iz) = m(1,1,1)/f(:,:,iz)
      end do
    case(1)   ! The metric depends on the y-coordinate
!$omp parallel do private(iz)
      do iz = 1, mz
        do iy = 1, my
          mdiv(:,iy,iz) = m(1,iy,1)/f(:,iy,iz)
        end do
      end do
    case(2)   ! The metric depends on the {y,z}-coordinate
!$omp parallel do private(iz,iy)
      do iz = 1, mz
        do iy = 1, my
          mdiv(:,iy,iz) = m(1,iy,iz)/f(:,iy,iz)
        end do
      end do
    case(3)   ! The metric depends on all spatial coordinates
!$omp parallel do private(iz)
      do iz = 1, mz
        mdiv(:,:,iz) = m(:,:,iz)/f(:,:,iz)
      end do
    end select
  END FUNCTION mdiv
  !
END MODULE metric_operations
