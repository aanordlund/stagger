! $Id: srviscosity.f90,v 1.4 2005/03/29 14:56:39 troels_h Exp $
!***********************************************************************
MODULE visc

CONTAINS

SUBROUTINE viscosity(px,py,pz,Dh,lnDh,nu,lnu,vW,W,Ux,Uy,Uz,Uxup,Uyup,Uzup, &
                     UWx,UWy,UWz,Ttt,Txx,Tyy,Tzz,Txy,Tzx,Tyz,Ttx,Tty,Ttz)
  USE params
  USE stagger
  real, dimension(mx,my,mz) :: &
       px,py,pz,UWx,UWy,UWz,W,lnDh,nu,lnu,Ux,Uy,Uz,Uxup,Uyup,Uzup
  real, allocatable, dimension(:,:,:) :: &
       vW,Dh,Ttt,Ttx,Tty,Ttz,Txx,Tyy,Tzz,Txy,Tzx,Tyz
  real, allocatable, dimension(:,:,:) :: &
       Wx,Wy,Wz,Oxx,Oyy,Ozz,Oxy,Ozx,Oyz, &
       vWxy,vWyz,vWzx,Sxy,Syz,Szx,UWxup,UWyup,UWzup

!-----------------------------------------------------------------------
!  Viscous stress, diagonal and off-diagonal elements
!-----------------------------------------------------------------------
  allocate(Wx(mx,my,mz), Wy(mx,my,mz), Wz(mx,my,mz))    ! 15 chunks
  allocate(Ttt(mx,my,mz))                               ! 15 chunks
!
!
  Wx = exp(xdn(alog(W)))
  Wy = exp(ydn(alog(W)))
  Wz = exp(zdn(alog(W)))
  Ttt = nu*Dh
  deallocate(Dh)
  allocate(Txx(mx,my,mz),Tyy(mx,my,mz),Tzz(mx,my,mz))   ! 15 chunks
  Txx = - dx*Ttt
  Tyy = - dy*Ttt
  Tzz = - dz*Ttt
  If (do_dissipation) Then
    allocate(Oxx(mx,my,mz),Oyy(mx,my,mz),Ozz(mx,my,mz)) ! 16 chunks
    allocate(UWxup(mx,my,mz),UWyup(mx,my,mz),UWzup(mx,my,mz)) ! 16 chunks
    ! Third order term (in v) of sigma^ij
    ! Oxx = U_k d_k (U_x**2)
    UWxup = Uxup*W
    UWyup = Uyup*W
    UWzup = Uzup*W
    Oxx = 0.5*Txx*W*(Uxup*ddxup((UWx)**2) &
      + Uyup*ddyup(ydn((UWxup)**2)) &
      + Uzup*ddzup(zdn((UWxup)**2)))
    Oyy = 0.5*Tyy*W*(Uyup*ddyup((UWy)**2) &
      + Uxup*ddxup(xdn((UWyup)**2)) &
      + Uzup*ddzup(zdn((UWyup)**2)))
    Ozz = 0.5*Tzz*W*(Uzup*ddzup((UWz)**2) &
      + Uyup*ddyup(ydn((UWzup)**2)) &
      + Uxup*ddxup(xdn((UWzup)**2)))
  End if
  !
  if (idbg .eq. -2) Then
    If (do_dissipation) &
      Print *,"min,av,max:",minval(ddyup1(Oyy)),average(ddyup1(Oyy)), &
                                               maxval(ddyup1(Oyy)),"ddy(Oyy)"
      Print *,"min,av,max:",minval(ddyup1(Tyy)),average(ddyup1(Tyy)), &
                                               maxval(ddyup1(Tyy)),"ddy(Tyy)"
  End if
  !
  allocate(Txy(mx,my,mz),Tyz(mx,my,mz),Tzx(mx,my,mz))   ! 15 chunks
  Txy = - (0.5*(dx+dy))*exp(xdn(ydn(lnu + lnDh)))
  Tyz = - (0.5*(dy+dz))*exp(ydn(zdn(lnu + lnDh)))
  Tzx = - (0.5*(dz+dx))*exp(zdn(xdn(lnu + lnDh)))
  If (do_dissipation) Then
    ! Third order term (in v) of sigma^ij
    ! Oij = U_k d_k (U_i U_j)
    allocate(Oxy(mx,my,mz),Oyz(mx,my,mz),Ozx(mx,my,mz))         ! 16 chunks
    Oxy = 0.5*Txy*(ydn(UWx)*ddxdn(ydn(UWxup)*UWy) &
      + xdn(UWy)*ddydn(xdn(UWyup)*UWx) &
      + xdn(ydn(UWzup))*ddzdn(zup(ydn(UWx)*xdn(UWy))))
    Oyz = 0.5*Tyz*(zdn(UWy)*ddydn(zdn(UWyup)*UWz) &
      + ydn(UWz)*ddzdn(ydn(UWzup)*UWy) &
      + ydn(zdn(UWxup))*ddxdn(xup(zdn(UWy)*ydn(UWz))))
    Ozx = 0.5*Tzx*(xdn(UWz)*ddzdn(xdn(UWzup)*UWx) &
      + zdn(UWx)*ddxdn(zdn(UWxup)*UWz) &
      + zdn(xdn(UWyup))*ddydn(yup(xdn(UWz)*zdn(UWx))))
    deallocate(UWxup,UWyup,UWzup)                               ! 13 chunks
  End if
  if (do_energy) then
     allocate(Ttx(mx,my,mz),Tty(mx,my,mz),Ttz(mx,my,mz))        ! 15 chunks
     allocate(vWxy(mx,my,mz),vWyz(mx,my,mz),vWzx(mx,my,mz))     ! 13 chunks
     If (do_dissipation) Then
       vWxy= vW*(Uxup*ddxup(xdn(vW))+Uyup*ddyup(ydn(vW))+Uzup*ddzup(zdn(vW)))
       ! sigma^tt
       Ttt = Ttt*W*vWxy
       ! 4th order term of sigma^ti
       Ttx = - 0.5*dx*Ux*xdn(Ttt)
       Tty = - 0.5*dy*Uy*ydn(Ttt)
       Ttz = - 0.5*dz*Uz*zdn(Ttt)
       Ttt = - 1./3.*(dx+dy+dz)*Ttt
       ! Off-Diagonal asymmetric remainder of sigma^ti
       vWxy = (xdn(ydn(vW)))**2
       vWyz = (ydn(zdn(vW)))**2
       vWzx = (zdn(xdn(vW)))**2
       Ttx  = Ttx + yup(Txy*vWxy*xdn(Uy)*ddydn(UWx)) &
                  + zup(Tzx*vWzx*xdn(Uz)*ddzdn(UWx))
       Tty  = Tty + xup(Txy*vWxy*ydn(Ux)*ddxdn(UWy)) &
                  + zup(Tyz*vWyz*ydn(Uz)*ddzdn(UWy))
       Ttz  = Ttz + yup(Tyz*vWyz*zdn(Uy)*ddydn(UWz)) &
                  + xup(Tzx*vWzx*zdn(Ux)*ddxdn(UWz))
     else
       ! sigma^tt
       vWxy= vW*(Uxup*ddxup(xdn(vW))+Uyup*ddyup(ydn(vW))+Uzup*ddzup(zdn(vW)))
       Ttt = - 1./3.*(dx+dy+dz)*Ttt*W*vWxy
       ! Off-Diagonal asymmetric remainder of sigma^ti
       vWxy = (xdn(ydn(vW)))**2
       vWyz = (ydn(zdn(vW)))**2
       vWzx = (zdn(xdn(vW)))**2
       Ttx  = yup(Txy*vWxy*xdn(Uy)*ddydn(UWx))+zup(Tzx*vWzx*xdn(Uz)*ddzdn(UWx))
       Tty  = xup(Txy*vWxy*ydn(Ux)*ddxdn(UWy))+zup(Tyz*vWyz*ydn(Uz)*ddzdn(UWy))
       Ttz  = yup(Tyz*vWyz*zdn(Uy)*ddydn(UWz))+xup(Tzx*vWzx*zdn(Ux)*ddxdn(UWz))
     end if
     deallocate(vWxy,vWyz,vWzx)                   ! 13 chunks
  end if
  deallocate(vW)
  Txx = Txx*ddxup(UWx)
  Tyy = Tyy*ddyup(UWy)
  Tzz = Tzz*ddzup(UWz)
  ! Diagonal part of sigma^ti
  if (do_energy) then
     Ttx  = Ttx + 0.5*xdn(Txx)*Ux*(1+Wx**2)
     Tty  = Tty + 0.5*ydn(Tyy)*Uy*(1+Wy**2)
     Ttz  = Ttz + 0.5*zdn(Tzz)*Uz*(1+Wz**2)
  end if
  deallocate(Wx,Wy,Wz)                          ! 13 chunks
  If (do_dissipation) Then
    Txx = Txx + Oxx
    Tyy = Tyy + Oyy
    Tzz = Tzz + Ozz
    deallocate(Oxx,Oyy,Ozz)                   ! 16 chunks
  end if
  ! First Order viscosity
  allocate(Sxy(mx,my,mz),Syz(mx,my,mz),Szx(mx,my,mz))   ! 13 chunks
  Sxy = (ddxdn(UWy)+ddydn(UWx))*0.5
  Syz = (ddydn(UWz)+ddzdn(UWy))*0.5
  Szx = (ddzdn(UWx)+ddxdn(UWz))*0.5
  Txy = Txy*Sxy
  Tyz = Tyz*Syz
  Tzx = Tzx*Szx
  deallocate(Sxy,Syz,Szx)                               ! 13 chunks
! Off-Diagonal symmetric part of sigma^ti
  if (do_energy) then
    Ttx  = Ttx + (yup(Txy*xdn(UWy)) + zup(Tzx*xdn(UWz)))
    Tty  = Tty + (zup(Tyz*ydn(UWz)) + xup(Txy*ydn(UWx)))
    Ttz  = Ttz + (xup(Tzx*zdn(UWx)) + yup(Tyz*zdn(UWy)))
  end if
  If (do_dissipation) Then
    Txy = Txy + Oxy
    Tyz = Tyz + Oyz
    Tzx = Tzx + Ozx
    deallocate(Oxy,Oyz,Ozx)                            ! 13 chunks
  end if
  If (idbg .eq. -2) &
     print *, "max       :", maxval(Ttt), maxval(Tty), maxval(Tyy), &
              "Ttt, Tty, Tyy"

END SUBROUTINE viscosity

END MODULE visc
