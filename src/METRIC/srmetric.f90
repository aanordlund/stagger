! $Id: srmetric.f90,v 1.2 2005/04/05 17:04:42 troels_h Exp $
!***********************************************************************
MODULE metric
  USE params
  implicit none
  !
  logical :: time_dependence          ! If the metric is time-dependent we
                                      ! have to update the metric components 
                                      ! every time-step.
  integer :: metric_dim               ! Number of spatial coordinates the 
                                      ! metric depends on going as (y,z,x)
  integer :: mmx,mmy,mmz              ! dimensions of the metric
  !
  real, dimension(:,:,:), allocatable :: dExdt,  dEydt,  dEzdt, detaJxdt, detaJydt, detaJzdt ! Needed for displacement current.
  real, dimension(:,:,:), allocatable :: deJxdt,deJydt,deJzdt
  real, dimension(:,:,:,:), allocatable, private :: oUWx,oUWy,oUWz,oBx,oBy,oBz,oetaJx,oetaJy,oetaJz
  real, dimension(:,:,:), allocatable, private :: dEdxdt, dEdydt, dEdzdt! Needed for displacement current.
  real, dimension(3),                  private :: oldtime               ! Needed for numerical differentiation
  real,                                private :: deltat
CONTAINS
!------------------------------------------------------------------------
SUBROUTINE init_metric
  integer :: iz,tt
  
  tt = timeorder
  time_dependence = .false.                                      ! The metric is not time dependent
  metric_dim = 0                                                 ! Rank of metric
  mmx = 1 ; mmy = 1 ; mmz = 1                                    ! Metric dimensions
  if (do_displacement) then
    allocate( dExdt(mx,my,mx),  dEydt(mx,my,mz),  dEzdt(mx,my,mz))  ! Updated every full timestep
    allocate(dEdxdt(mx,my,mz), dEdydt(mx,my,mz), dEdzdt(mx,my,mz))  ! Updated each substep
    allocate(oUWx(mx,my,mz,tt),oUWy(mx,my,mz,tt),oUWz(mx,my,mz,tt)) ! Old value for derivative
    allocate( oBx(mx,my,mz,tt), oBy(mx,my,mz,tt), oBz(mx,my,mz,tt)) ! Old value for derivative
    allocate(oetaJx(mx,my,mz,tt),oetaJy(mx,my,mz,tt),oetaJz(mx,my,mz,tt))
    allocate(detaJxdt(mx,my,mz),detaJydt(mx,my,mz),detaJzdt(mx,my,mz))
    allocate(deJxdt(mx,my,mz),deJydt(mx,my,mz),deJzdt(mx,my,mz))
!$omp parallel do private(iz)
    do iz=1,mz                                                   ! initializing
      dExdt(:,:,iz) = 0.
      dEydt(:,:,iz) = 0.
      dEzdt(:,:,iz) = 0.
      detaJxdt(:,:,iz) = 0.
      detaJydt(:,:,iz) = 0.
      detaJzdt(:,:,iz) = 0.
    enddo
  endif
END SUBROUTINE init_metric
!-----------------------------------------------------------------------
! Subroutine that calculates -dEdt=displacement current [J = -dEdt + curl(B)]
!-----------------------------------------------------------------------
SUBROUTINE displacement(W,Ux,Uy,Uz,UWx,UWy,UWz,Bx,By,Bz)
  USE stagger
  USE timeintegration

  real, dimension(mx,my,mz), intent(in) :: W,Ux,UWx,Bx,Uy,UWy,By,Uz,UWz,Bz
  real, dimension(:,:,:), allocatable   :: dBBxdt,dBBydt,dBBzdt, &
                                           dUWxdt,dUWydt,dUWzdt,UdtUW
  integer                               :: iz,is

  if (it .eq. 1) then
    do iz=1,mz                                                        ! First iteration
      oUWx(:,:,iz,isubstep) = UWx(:,:,iz)                             ! Just store old values
      oUWy(:,:,iz,isubstep) = UWy(:,:,iz)
      oUWz(:,:,iz,isubstep) = UWz(:,:,iz)
       oBx(:,:,iz,isubstep) =  Bx(:,:,iz)
       oBy(:,:,iz,isubstep) =  By(:,:,iz)
       oBz(:,:,iz,isubstep) =  Bz(:,:,iz)
       oldtime(isubstep)    = rt
    enddo
    return
  endif
  deltat = rt - oldtime(isubstep)
  
  oldtime(isubstep) = rt
  allocate(UdtUW(mx,my,mz),dUWxdt(mx,my,mz),dUWydt(mx,my,mz),dUWzdt(mx,my,mz))
!$omp parallel do private(iz)
  do iz=1,mz                                                          ! First calculate dUdt
    dUWxdt(:,:,iz) = (UWx(:,:,iz) - oUWx(:,:,iz,isubstep))/deltat
      oUWx(:,:,iz,isubstep) =  UWx(:,:,iz)
    dUWydt(:,:,iz) = (UWy(:,:,iz) - oUWy(:,:,iz,isubstep))/deltat
      oUWy(:,:,iz,isubstep) =  UWy(:,:,iz)
    dUWzdt(:,:,iz) = (UWz(:,:,iz) - oUWz(:,:,iz,isubstep))/deltat
      oUWz(:,:,iz,isubstep) =  UWz(:,:,iz)
  enddo
  UdtUW = xup(Ux*dUWxdt) + yup(Uy*dUWydt) + zup(Uz*dUWzdt)              ! We have
  dUWxdt = (dUWxdt - Ux*xdn(UdtUW))*exp(-xdn(alog(W)))                  ! dVidt =
  dUWydt = (dUWydt - Uy*ydn(UdtUW))*exp(-ydn(alog(W)))                  !   1/W [
  dUWzdt = (dUWzdt - Uz*zdn(UdtUW))*exp(-zdn(alog(W)))                  !      dUidt
  dEdxdt= dEdxdt - zdn(dUWydt)*ydn(Bz) + ydn(dUWzdt)*zdn(By)            !    - Vi Vj dUjdt ]
  dEdydt= dEdydt - xdn(dUWzdt)*zdn(Bx) + zdn(dUWxdt)*xdn(Bz)            ! The time derivative
  dEdzdt= dEdzdt - ydn(dUWxdt)*xdn(By) + xdn(dUWydt)*ydn(Bx)            ! of the four velocity 
  deallocate(UdtUW,dUWxdt,dUWydt,dUWzdt)                                ! is less stiff
  allocate(dBBxdt(mx,my,mz),dBBydt(mx,my,mz),dBBzdt(mx,my,mz))
!$omp parallel do private(iz)
  do iz=1,mz                                                            ! First calculate dUdt
    dBBxdt(:,:,iz) = (Bx(:,:,iz) - oBx(:,:,iz,isubstep))/deltat
       oBx(:,:,iz,isubstep) =  Bx(:,:,iz)
    dBBydt(:,:,iz) = (By(:,:,iz) - oBy(:,:,iz,isubstep))/deltat
       oBy(:,:,iz,isubstep) =  By(:,:,iz)
    dBBzdt(:,:,iz) = (Bz(:,:,iz) - oBz(:,:,iz,isubstep))/deltat
       oBz(:,:,iz,isubstep) =  Bz(:,:,iz)
  enddo
  dEdxdt= dEdxdt - ydn(Uz)*zdn(dBBydt) + zdn(Uy)*ydn(dBBzdt)            ! Finally the cross product
  dEdydt= dEdydt - zdn(Ux)*xdn(dBBzdt) + xdn(Uz)*zdn(dBBxdt)            !
  dEdzdt= dEdzdt - xdn(Uy)*ydn(dBBxdt) + ydn(Ux)*xdn(dBBydt)            !
  deallocate(dBBxdt,dBBydt,dBBzdt)
  if (isubstep .eq. timeorder) then
!$omp parallel do private(iz)
    do iz=1,mz                                                          ! storing full derivative
       dExdt(:,:,iz) = dEdxdt(:,:,iz)                                   ! and zeroing in
      dEdxdt(:,:,iz) = 0.
       dEydt(:,:,iz) = dEdydt(:,:,iz)
      dEdydt(:,:,iz) = 0.
       dEzdt(:,:,iz) = dEdzdt(:,:,iz)
      dEdzdt(:,:,iz) = 0.
    enddo
  else
    is = isubstep + 1
!$omp parallel do private(iz)
    do iz=1,mz                                                          ! update derivative
      dEdxdt(:,:,iz) = alpha(is)*dEdxdt(:,:,iz)
      dEdydt(:,:,iz) = alpha(is)*dEdydt(:,:,iz)
      dEdzdt(:,:,iz) = alpha(is)*dEdzdt(:,:,iz)
    enddo
  endif
END SUBROUTINE displacement
!------------------------------------------------------------------------
SUBROUTINE displacement_etaJ(etaJx,etaJy,etaJz)
  USE stagger
  USE timeintegration

  real, dimension(mx,my,mz), intent(in) :: etaJx,etaJy,etaJz
  integer                               :: iz,is
  if (it .eq. 1) then
    do iz=1,mz                                                        ! First iteration
      oetaJx(:,:,iz,isubstep) = etaJx(:,:,iz)                             ! Just store old values
      oetaJy(:,:,iz,isubstep) = etaJy(:,:,iz)
      oetaJz(:,:,iz,isubstep) = etaJz(:,:,iz)
    enddo
    return
  endif
  if (isubstep .eq. timeorder) then
!$omp parallel do private(iz)
    do iz=1,mz                                                          ! First calculate substep detaJidt
      deJxdt(:,:,iz) = deJxdt(:,:,iz) + (etaJx(:,:,iz) - oetaJx(:,:,iz,isubstep))/deltat
     oetaJx(:,:,iz,isubstep) =  etaJx(:,:,iz)
    detaJxdt(:,:,iz) = deJxdt(:,:,iz)
      deJxdt(:,:,iz) = 0.
       deJydt(:,:,iz) = deJydt(:,:,iz) + (etaJy(:,:,iz) - oetaJy(:,:,iz,isubstep))/deltat
     oetaJy(:,:,iz,isubstep) =  etaJy(:,:,iz)
    detaJydt(:,:,iz) = deJydt(:,:,iz)
      deJydt(:,:,iz) = 0.
       deJzdt(:,:,iz) = deJzdt(:,:,iz) + (etaJz(:,:,iz) - oetaJz(:,:,iz,isubstep))/deltat
     oetaJz(:,:,iz,isubstep) =  etaJz(:,:,iz)
    detaJzdt(:,:,iz) = deJzdt(:,:,iz)
      deJzdt(:,:,iz) = 0.
    enddo
  else
    is = isubstep + 1
!$omp parallel do private(iz)
    do iz=1,mz                                                          ! First calculate substep detaJidt
      deJxdt(:,:,iz) = alpha(is)*(deJxdt(:,:,iz) + (etaJx(:,:,iz) - oetaJx(:,:,iz,isubstep))/deltat)
        oetaJx(:,:,iz,isubstep) =  etaJx(:,:,iz)
      deJydt(:,:,iz) = alpha(is)*(deJydt(:,:,iz) + (etaJy(:,:,iz) - oetaJy(:,:,iz,isubstep))/deltat)
        oetaJy(:,:,iz,isubstep) =  etaJy(:,:,iz)
      deJzdt(:,:,iz) = alpha(is)*(deJzdt(:,:,iz) + (etaJz(:,:,iz) - oetaJz(:,:,iz,isubstep))/deltat)
       oetaJz(:,:,iz,isubstep) =  etaJz(:,:,iz)
    enddo
  endif
END SUBROUTINE displacement_etaJ
!------------------------------------------------------------------------
END MODULE metric