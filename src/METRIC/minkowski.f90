! $Id: minkowski.f90,v 1.1 2005/01/12 18:39:32 troels_h Exp $
!***********************************************************************
MODULE metric_components
  !
  USE params
  !
  character(len=18) :: metric_name    ! Type of metric in use
  logical :: time_dependence          ! If the metric is time-dependent we
                                      ! have to update the metric components 
                                      ! every time-step.
  integer :: metric_dim               ! Number of spatial coordinates the 
                                      ! metric depends on going as (y,z,x)
  integer :: mmx,mmy,mmz              ! dimensions of the metric
  !
CONTAINS
  !
  SUBROUTINE init_metric
    !
    ! ds^2 = - dt^2 + dx^2 + dy^2 + dz^2
    ! The metric is not time dependent
    !
    time_dependence = .false.
    ! Metric dimensions
    metric_dim = 0
    mmx = 1 ; mmy = 1 ; mmz = 1
    !
  END SUBROUTINE init_metric
  !
  FUNCTION metricg(pos,i,j)
    !
    integer, intent(in) :: pos, i, j
    real, dimension(mmx,mmy,mmz) :: metricg
    !
    if (i .ne. j) then
      metricg = 0.
    else
      metricg = 1
    endif
    !
  END FUNCTION metricg
  !
  FUNCTION pdmetricg(pos,i,j,k)
    !
    integer, intent(in) :: pos, i, j
    real, dimension(mmx,mmy,mmz) :: pdmetricg
    !
    pdmetricg = 0.
    !
  END FUNCTION pdmetricg
  !
  FUNCTION metricbeta(pos,i)
    !
    integer, intent(in) :: pos, i
    real, dimension(mmx,mmy,mmz) :: metricbeta
    !
    metricbeta = 0.
    !
  END FUNCTION metricbeta
  !
  FUNCTION pdmetricbeta(pos,i)
    !
    integer, intent(in) :: pos, i
    real, dimension(mmx,mmy,mmz) :: metricbeta
    !
    pdmetricbeta = 0.
    !
  END FUNCTION pdmetricbeta
  !
  FUNCTION metricalpha(pos)
    !
    integer, intent(in) :: pos
    real, dimension(mmx,mmy,mmz) :: metricalpha
    !
    metricalpha = 1.
    !
  END FUNCTION metricalpha
  !
  FUNCTION pdmetricalpha(pos,k)
    !
    integer, intent(in) :: pos,k
    real, dimension(mmx,mmy,mmz) :: pdmetricalpha
    !
    pdmetricalpha = 0.
    !
  END FUNCTION pdmetricalpha
  !
  FUNCTION determinantg(pos)
    !
    integer, intent(in) :: pos
    real, dimension(mmx,mmy,mmz) :: determinantg
    !
    determinantg = 1.
    !
  END FUNCTION determinantg
  !
END MODULE metric_components
