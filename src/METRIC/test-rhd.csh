#!/bin/sh
#
# $Header: /astro/home/aake/cvs/codes/stagger/src/METRIC/test-rhd.csh,v 1.1 2004/01/20 03:05:42 troels_h Exp $
if [ ! -e test.x ]; then
  ifc -O3 -xW -vec_report0 ../params.f90 polysolve.f90 rhdsolve-test.f90 -o test.x
fi
./test.x
