! $Id: metric_diagonal.f90,v 1.3 2005/01/12 18:42:10 troels_h Exp $
!***********************************************************************
MODULE metric
  USE params
  USE metric_components
  USE metric_operations

  implicit none
  
CONTAINS

  FUNCTION lnalphaterm(DhW,Uxup,Uyup,Uzup,Ttx,Tty,Ttz,alp)
  real, dimension(mx,my,mz)             :: lnalphaterm
  real, dimension(mx,my,mz), intent(in) :: DhW,Uxup,Uyup,Uzup,Ttx,Tty,Ttz
  real, dimension(mmx,mmy,mmz),intent(in):: alp
    lnalphaterm = mmult(mmult(DhW*Uxup + Ttx,pdmetricalpha(0,1)) + &
                        mmult(DhW*Uyup + Ttx,pdmetricalpha(0,2)) + &
                        mmult(DhW*Uzup + Ttx,pdmetricalpha(0,3))   &
                      , 1./alp)
  END FUNCTION lnalphaterm

  SUBROUTINE mterm(Mtermt,Mtermx,Mtermy,Mtermz,Ttt,Txx,Tyy,Tzz,Txy,Tyz,Tzx,detg)
  real, dimension(mx,my,mz), intent(in) :: Ttt,Txx,Tyy,Tzz,Txy,Tyz,Tzx
  real, dimension(mx,my,mz), intent(out):: Mtermt,Mtermx,Mtermy,Mtermz
  real, dimension(mmx,mmy,mmz),intent(in):: detg
  real, dimension(mmx,mmy,mmz)          :: hdetg
  ! FIXME! Mtermt is not computed.
  ! Have to fix infrastructure for time derivatives of the metric tensor
  hdetg = 0.5*detg
  Mtermx = mmult(-2.*mmult(Ttt,pdmetricalpha(0,1)) &
                 +   mmult(Txx,pdmetricg(0,1,1,1)) &
                 +   mmult(Tyy,pdmetricg(0,2,2,1)) &
                 +   mmult(Tzz,pdmetricg(0,3,3,1)) &
                 ,hdetg)
  Mtermy = mmult(-2.*mmult(Ttt,pdmetricalpha(0,2)) &
                 +   mmult(Txx,pdmetricg(0,1,1,2)) &
                 +   mmult(Tyy,pdmetricg(0,2,2,2)) &
                 +   mmult(Tzz,pdmetricg(0,3,3,2)) &
                 ,hdetg)
  Mtermz = mmult(-2.*mmult(Ttt,pdmetricalpha(0,3)) &
                 +   mmult(Txx,pdmetricg(0,1,1,3)) &
                 +   mmult(Tyy,pdmetricg(0,2,2,3)) &
                 +   mmult(Tzz,pdmetricg(0,3,3,3)) &
                 ,hdetg)
  Mtermt = 0.
  END SUBROUTINE mterm

  END MODULE metric
