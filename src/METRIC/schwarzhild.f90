! $Id: schwarzhild.f90,v 1.2 2005/01/12 18:39:32 troels_h Exp $
!***********************************************************************
MODULE metric_components
  USE params

  character(len=18) :: metric_name    ! Type of metric in use
  real    :: mass                     ! mass of black hole
  logical :: time_dependence          ! If metric is time-dependent we
                                      ! have to update metric components every
                                      ! time-step
  integer :: metric_dim               ! Number of spatial coordinates the 
                                      ! metric depends on going as (z,y,x)
  integer :: mmx,mmy,mmz              ! dimensions of the metric

  real, allocatable, dimension(:,:,:) :: &
       mdetg,mg11,mg22,mg33,malp      ! the metric and its spatial determinant

  real, allocatable, dimension(:,:,:) :: &
       pdalpr,pdg11r,pdg22r,pdg33r, & ! partial derivatives of the metric
       pdg11t

CONTAINS

  SUBROUTINE init_metric
    !
    integer          :: jx,jy,jz
    !
    ! ds^2 = - (1 - 2m/r) dt^2 + dr^2/(1 - 2m/r) + r^2(dth^2 + sin(th)^2dph^2)
    ! The metric is not time dependent
    time_dependence = .false.
    ! For the moment the mass is 1, since everything is measured 
    ! in units of the mass
    mass = 1.
    ! r->y-axis, theta->z-axis, phi->x-axis
    ! Check if y-axis range is okay
    if (ym(1) .le. 2.*mass) then 
      print *, "The Schwarzhild radius of the BH is:  ", 2.*mass
      print *, "Your lower boundary of the y-axis is: ", ym(1)
      print *, "Please correct and try again! Stopping execution..."
      stop
    endif
    !
    if (mx .gt. 1) then                 ! angle dependence?
      metric_dim = 2
      mmx = 1 ; mmy = my ; mmz = mz
      ! Ranges: z=theta=[-pi/2,pi/2],x=phi=[0,2 pi]
      if ((abs(zm(1)+pi/2.) .gt. 1e-3) .or. &
          (abs(zm(mz)+dz-pi/2.) .gt. 1e-3)) then
        print *, "WARNING! z=theta!=[-pi/2,pi/2]"
        print *, "pi/2,zmin,zmax=",pi/2.,zm(1),zm(mz)
      endif
      ! 
      if ((abs(xm(1)) .gt. 1e-3) .or. &
          (abs(xm(mx)+dx-2.*pi) .gt. 1e-3)) then
        print *, "WARNING! x=phi!=[0,2pi]"
        print *, "2pi,xmin,xmax=",2.*pi,xm(1),xm(mx)
      endif
      allocate(pdg11t(1,mmy,mmz))
    else
      metric_dim = 1
      mmx = 1 ; mmy = my ; mmz = 1
      ! Ranges: z=phi=[0,2 pi]
      if ((abs(zm(1)) .gt. 1e-3) .or. &
          (abs(zm(mz)+dz-2.*pi) .gt. 1e-3)) then
        print *, "WARNING! z=phi!=[0,2pi]"
        print *, "2pi,zmin,zmax=",2.*pi,zm(1),zm(mz)
      endif
    endif
    allocate(malp(1,mmy,mmz),mdetg(1,mmy,mmz),pdalpr(1,mmy,mmz), &
             mg11(1,mmy,mmz),mg22(1,mmy,mmz),mg33(1,mmy,mmz), &
             pdg11r(1,mmy,mmz),pdg22r(1,mmy,mmz),pdg33r(1,mmy,mmz))
    do jy=1,my
      malp(1,jy,:)  = sqrt(1 - 2.*mass/ym(jy))
      mg22(1,jy,:)  = 1./(1 - 2.*mass/ym(jy))
      mg33(1,jy,:)  = ym(jy)**2
      pdalpr(1,jy,:)= mass/(ym(jy)**2*sqrt(1 - 2.*mass/ym(jy)))
      pdg22r(1,jy,:)= 2.*mass/(ym(jy)*(1 - 2.*mass/ym(jy)))**2
      pdg33r(1,jy,:)= 2.*ym(jy)
    end do
    if (metric_dim .eq. 2) then         ! angle dependence?
      do jy=1,my
      do jz=1,mz
        mg11(1,jy,jz)  = (ym(jy)*sin(zm(jz)))**2
        mdetg(1,jy,jz) = ym(jy)**2*sin(zm(jz))/sqrt(1 - 2.*mass/ym(jy))
        pdg11r(1,jy,:)= 2.*ym(jy)*sin(zm(jz))**2
        pdg11t(1,jy,:)= ym(jy)**2*sin(2.*zm(jz))
      end do
      end do
    else
      do jy=1,my
        mg11(1,jy,:)  = ym(jy)**2
        mdetg(1,jy,:) = ym(jy)**2/sqrt(1 - 2.*mass/ym(jy))
        pdg11r(1,jy,:)= 2.*ym(jy)
      end do    
    endif    
  END SUBROUTINE init_metric
!
  FUNCTION metricg(pos,i,j)
!
! pos : 0 = Centered
!       1 = Face
!       2 = Edge
! i,j : Index in the metric tensor to be computed 
!
  integer, intent(in) :: pos, i, j
  real, dimension(mmx,mmy,mmz) :: metricg
!
  if (i .ne. j) then
    metricg = 0.
  else
! FIXME! We don't interpolate to the right position yet.
!        It is always centered
    select case (i)
      case(1)
        metricg = mg11
      case(2)
        metricg = mg22
      case(3)
        metricg = mg33
    end select         
  endif
! END FIXME!
  END FUNCTION metricg
!
  FUNCTION pdmetricg(pos,i,j,k)
!
! pos : 0 = Centered
!       1 = Face
!       2 = Edge
! i,j : Index in the metric tensor to be computed 
! k   : Index with respect to which the metric tensor is differentiated
!
  integer, intent(in) :: pos, i, j
  real, dimension(mmx,mmy,mmz) :: pdmetricg
!
  if ((i .ne. j) .or. (k .eq. 1)) then
    pdmetricg = 0.
  else
! FIXME! We don't interpolate to the right position yet.
!        It is always centered
    if (k .eq. 3) then
      if ((i .eq. 1) .and. (metric_dim .eq. 2)) then
        pdmetricg = pdg11t
      else
        pdmetricg = 0.
      endif
    else
      select case (i)
        case(1)
          pdmetricg = pdg11r
        case(2)
          pdmetricg = pdg22r
        case(3)
          pdmetricg = pdg33r
      end select         
    endif
  endif
! END FIXME!
  END FUNCTION pdmetricg
!
  FUNCTION metricbeta(pos,i)
  integer, intent(in) :: pos, i
  real, dimension(mmx,mmy,mmz) :: metricbeta
  metricbeta = 0.
  END FUNCTION metricbeta
!
  FUNCTION pdmetricbeta(pos,i)
  integer, intent(in) :: pos, i
  real, dimension(mmx,mmy,mmz) :: metricbeta
  pdmetricbeta = 0.
  END FUNCTION pdmetricbeta
!
  FUNCTION metricalpha(pos)
!
! pos : 0 = Centered
!       1 = Face
!       2 = Edge
  integer, intent(in) :: pos
  real, dimension(mmx,mmy,mmz) :: metricalpha
!
! FIXME! We don't interpolate to the right position yet.
!        It is always centered
  metricalpha = malp
! END FIXME!
  END FUNCTION metricalpha
!
  FUNCTION pdmetricalpha(pos,k)
!
! pos : 0 = Centered
!       1 = Face
!       2 = Edge
  integer, intent(in) :: pos,k
  real, dimension(mmx,mmy,mmz) :: pdmetricalpha
!
! FIXME! We don't interpolate to the right position yet.
!        It is always centered
  if (k .eq. 2) then
    pdmetricalpha = pdalp
! END FIXME!
  else
    pdmetricalpha = 0.
  endif
END FUNCTION pdmetricalpha
!
  FUNCTION determinantg(pos)
!
! pos : 0 = Centered
!       1 = Face
!       2 = Edge
  integer, intent(in) :: pos
  real, dimension(mmx,mmy,mmz) :: determinantg
!
! FIXME! We don't interpolate to the right position yet.
!        It is always centered
  determinantg = mdetg
! END FIXME!
  END FUNCTION determinantg
END MODULE metric_components
