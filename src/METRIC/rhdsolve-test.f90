! $Id: rhdsolve-test.f90,v 1.3 2005/01/13 13:24:06 aake Exp $
!-----------------------------------------------------------------------
!
PROGRAM rhdSolve_test

  USE params
  USE PolySolve

  implicit none

  real, allocatable, dimension(:,:,:) :: &
          D, T00p, T0x, T0y, T0z, sW2m1, W, hm1, Wtest, hm1test
!       complex, allocatable, dimension(:,:,:) :: Roots
  real, allocatable, dimension(:,:,:) :: ReRoots, ImRoots
  real, allocatable, dimension(:,:) :: a0,a1,a2,a3,x1,x2,x3,x4
  real, allocatable, dimension(:,:) :: &
          a30,a31,a32,a33,x31,x32,x33,e0,e1,e2,e3, Wt, hm1t
  integer :: m, ix, iy, iz
  real    :: Wm1min, Wm1max, hm1min, hm1max
  real :: Root3, Err,A
  real :: cput(2), dtime, void, kzs
  complex :: P2x1,P2x2,a20,a21
  complex, dimension(2) :: Root2
  logical                               :: p4_test, Rhd_Solve_test
  m = 1
  mx = m
  my = m
  mz = 1
  p4_test = .true.
  Rhd_Solve_test = .true.
  idbg = 0

if (p4_test) Then
! Testing the P4_Roots routine
  Print *, "================================================"
  Print *, "Testing the P4_Roots routine"
  Print *, "================================================"
        allocate(a0(mx,my),a1(mx,my),a2(mx,my),a3(mx,my), &
                 e0(mx,my),e1(mx,my),e2(mx,my),e3(mx,my), &
                 x1(mx,my),x2(mx,my),x3(mx,my),x4(mx,my))
        allocate(ReRoots(mx,my,4),ImRoots(mx,my,4))
  do iy=1,my
    do ix=1,mx
      x1(ix,iy) = real(ix)
      x2(ix,iy) = real(iy)
      x3(ix,iy) = real(ix*iy)
      x4(ix,iy) = real(iy)/real(ix)
      a0(ix,iy) = x1(ix,iy)*x2(ix,iy)*x3(ix,iy)*x4(ix,iy)
      a1(ix,iy) = - x1(ix,iy)*x2(ix,iy)*x3(ix,iy) &
                  - x1(ix,iy)*x2(ix,iy)*x4(ix,iy) &
                  - x1(ix,iy)*x3(ix,iy)*x4(ix,iy) &
                  - x2(ix,iy)*x3(ix,iy)*x4(ix,iy)
      a2(ix,iy) = x1(ix,iy)*x2(ix,iy) + x1(ix,iy)*x3(ix,iy) + &
                  x1(ix,iy)*x4(ix,iy) + x2(ix,iy)*x3(ix,iy) + &
                  x2(ix,iy)*x4(ix,iy) + x3(ix,iy)*x4(ix,iy)
      a3(ix,iy) = -x1(ix,iy)-x2(ix,iy)-x3(ix,iy)-x4(ix,iy)
    end do
  end do
  void = dtime(cput)
  a0 = -25./3.
  a1 = -40./3.
  a2 = -2./3.
  a3 = 16./3.
  x1 = -5.
  x2 = -1.
  x3 = -1.
  x4 = 5./3.
  call P4_Roots(ReRoots, ImRoots, a0, a1, a2, a3, mx, my)
  void = dtime(cput)
  Print *, ReRoots
  Print *, ImRoots
  call Polish_Roots(ReRoots, ImRoots, a0, a1, a2, a3, mx, my,2)
  kzs = mx*my*1e-3/(cput(1)+cput(2))
  Print *, "Kilo Zones", kzs
  do iy=1,my
    do ix=1,mx
      e0(ix,iy) = minval(abs(ReRoots(ix,iy,:)-x1(ix,iy)))/abs(x1(ix,iy))
      e1(ix,iy) = minval(abs(ReRoots(ix,iy,:)-x2(ix,iy)))/abs(x2(ix,iy))
      e2(ix,iy) = minval(abs(ReRoots(ix,iy,:)-x3(ix,iy)))/abs(x3(ix,iy))
      e3(ix,iy) = minval(abs(ReRoots(ix,iy,:)-x4(ix,iy)))/abs(x4(ix,iy))
      If (e0(ix,iy).gt.1.e-2) then
        Print *, "==============================================="
        Print *, "Index Nr:",ix, iy
        Print *, "Coefficients a0,a1,a2,a3:", a0(ix,iy),a1(ix,iy),a2(ix,iy),a3(ix,iy)
        Print *, "Precomputed Roots:",x1(ix,iy),x2(ix,iy),x3(ix,iy),x4(ix,iy)
        Print *, "Found Roots (Re):", ReRoots(ix,iy,:)
        Print *, "Found Roots (Im):", ImRoots(ix,iy,:)
        Stop
      endif
    end do
  end do
  Print *, "Max Relative Error in 1st Root:", MaxVal(e0)
  Print *, "Max Relative Error in 2nd Root:", MaxVal(e1)
  Print *, "Max Relative Error in 3rd Root:", MaxVal(e2)
  Print *, "Max Relative Error in 4th Root:", MaxVal(e3)
End If

if (Rhd_Solve_test) Then
! Testing the RhdSolver 
  gamma  = 1.66666666666666666666666667
  A = (gamma - 1.)/gamma
  Wm1min = 1.0e-1
  hm1min = 1.0e-1
  Wm1max = 1.0e1
  hm1max = 1.0e1
  allocate(D(mx,my,mz),T00p(mx,my,mz),T0x(mx,my,mz),T0y(mx,my,mz), &
    T0z(mx,my,mz),W(mx,my,mz),sW2m1(mx,my,mz),hm1(mx,my,mz))
! For this test we assume Density = 1.0 and Vy=Vz=0.0   
  D = 1.0
  T0y = 0.
  T0z = 0.
  If (m.eq.1) Then
    Print *, "hm1:"
!    Read (5,*) hm1
    Print *, "Sqrt(Gamma**2 -1):"
!    Read (5,*) sW2m1
    hm1 = 1.0
    Sw2m1 = 1.0
  Else
!       
! We use a logarithmic scale for W and hm1
! Here we actually compute Sqrt(W**2 - 1) to
! get a precise measure of the velocity
! and of T00p
!       
    Wm1max = Log(Wm1max/Wm1min)/real(m-1)  ! Use it for scaling
    hm1max = Log(hm1max/hm1min)/real(m-1)
    do iz=1,mz
      do iy=1,my
        do ix=1,mx
!          hm1(ix,iy,iz) = hm1min*exp((ix-1)*hm1max)
!          sW2m1(ix,iy,iz)   = Wm1min*exp((iy-1)*Wm1max)
        end do
      end do
    end do
  endif
  hm1 = 1.6666666666666667
  sW2m1 = 0.
!       
! Remember that if Sw2m1 == Sqrt(W**2 - 1)
! Then W = Sqrt(Sw2m1**2 + 1) and
! W - 1 = Sw2m1**2/(Sqrt(Sw2m1**2 + 1) + 1)
! Both computed in a stable way from W**2 - 1.
! Furthermore we have W*vx = Sqrt(W**2 -1).
!
  W = Sqrt(sW2m1**2 + 1.0)
!  Print *,"D, W, sW2m1, hm1:",D, W, sW2m1, hm1
!  Print *,"hm1*W, sW2m1**2/(W+1.), - A*hm1/W", hm1*W, sW2m1**2/(W+1.), - A*hm1/W
  T0x = D*(hm1 + 1.)*sW2m1
  T00p = D*(hm1*W + sW2m1**2/(W+1.) - A*hm1/W)
  allocate(Wtest(mx,my,mz),hm1test(mx,my,mz))
  void = dtime(cput)
  call rhdSolve(D, T00p, T0x, T0y, T0z, Wtest, hm1test)
  void = dtime(cput)
  kzs = mx*my*1e-3/(cput(1)+cput(2))
  Print *, "Kilo Zones", kzs
  Print *, "Max Relative Error in hm1:", MaxVal(Abs(hm1-hm1test)/hm1)
  Print *, "Max Relative Error in vW :", MaxVal(Abs(sW2m1-Wtest)/sW2m1)
  Print *, "Min, Max in hm1", minval(hm1), maxval(hm1)
  Print *, "Min, Max in vW ", minval(Wtest), maxval(Wtest)      
!       open (2, file='rhdsolve.dat', form='unformatted')
!       write(2) mx, my, hm1, hm1test, W, Wtest
!       close (2)
  deallocate(D, T00p, T0x, T0y, T0z, W, hm1, Wtest, hm1test)
endif

END PROGRAM rhdSolve_test       
