!-------------------------------------------------------------------------------
module mpi
  integer, parameter:: MPI_INTEGER=0, MPI_REAL=1, MPI_REAL8=2
  integer, parameter:: mpi_comm_world=0, MPI_STATUS_SIZE=1
end module mpi

module mpi_mod
  implicit none

  integer               :: mpi_err, new_com
  integer               :: mpi_yup, mpi_ydn, mpi_zdn, mpi_zup
  integer, dimension(3) :: mpi_coords, mpi_dims, mpi_periodic
  logical:: mpi_reorder

end module mpi_mod

!-------------------------------------------------------------------------------
subroutine init_mpi
  use params
  use mpi_mod
  implicit none

  mpi_rank = 0
  mpi_coords = 0
  mpi_size = 1
  mpi_dims = 1
  mpi_nx = 1
  mpi_ny = 1
  mpi_nz = 1
  mpi_x = 0
  mpi_y = 0
  mpi_z = 0
  master = .true.
  mpi_master = .true.
end subroutine init_mpi

!-------------------------------------------------------------------------------
subroutine init_geom_mpi
  use params
  implicit none
end subroutine init_geom_mpi

!-------------------------------------------------------------------------------
subroutine check_geom_mpi
  use params
  implicit none
  mpi_nx = 1
  mpi_ny = 1
  mpi_nz = 1
end subroutine check_geom_mpi

!-------------------------------------------------------------------------------
subroutine mesh_mpi
  use params
  use mpi_mod
  implicit none

  mpi_nx = 1
  mpi_ny = 1
  mpi_nz = 1
  mpi_x = 0
  mpi_y = 0
  mpi_z = 0
  mpi_dims = 1
  mpi_coords = 0

  ! global grid dimensions, x, y, and z

  mxtot = mx
  mytot = my
  mztot = mz

  !  Allocate buffer zones
  allocate (gx1(1,my,mz), hx1(1,my,mz), gx2(2,my,mz), hx2(2,my,mz), gx3(3,my,mz), hx3(3,my,mz))
  allocate (gy1(mx,1,mz), gy2(mx,2,mz), gy3(mx,3,mz), hy1(mx,1,mz), hy2(mx,2,mz), hy3(mx,3,mz))
  allocate (gz1(mx,my,1), gz2(mx,my,2), gz3(mx,my,3), hz1(mx,my,1), hz2(mx,my,2), hz3(mx,my,3))
end subroutine mesh_mpi


!-------------------------------------------------------------------------------
subroutine mpi_name (name)
  use params
  character(len=mfile) name
end subroutine mpi_name

!-------------------------------------------------------------------------------
subroutine barrier_mpi (label)
  use params
  use mpi_mod
  implicit none
  character(len=*) label
  call barrier_omp('flush')
  !$omp single
  if (do_trace) print *, mpi_rank, 'barrier_mpi: ', label
  !$omp end single
end subroutine barrier_mpi

!-------------------------------------------------------------------------------
subroutine flush_mpi
  call barrier_omp ('flush_mpi')
end subroutine flush_mpi

!-------------------------------------------------------------------------------
subroutine end_mpi
end subroutine end_mpi

!-------------------------------------------------------------------------------
subroutine abort_mpi
end subroutine abort_mpi

!-------------------------------------------------------------------------------
subroutine mpi_send_x (f, g, mg, h, mh)
  use params
  use mpi_mod
  implicit none
  integer:: mg, mh
  real, dimension(mx,my,mz):: f
  real, dimension(mg,my,mz):: g
  real, dimension(mh,my,mz):: h

  if (mh .gt. 0) then
     h(1:mh,:,izs:ize) = f(1:mh,:,izs:ize)
  end if
  if (mg .gt. 0) then
     g(1:mg,:,izs:ize) = f(mx-mg+1:mx,:,izs:ize)
  end if
end subroutine mpi_send_x

!-------------------------------------------------------------------------------
subroutine mpi_send_y (f, g, mg, h, mh)
  use params
  use mpi_mod
  implicit none
  integer:: mg, mh
  real, dimension(mx,my,mz):: f
  real, dimension(mx,mg,mz):: g
  real, dimension(mx,mh,mz):: h

  if (mh .gt. 0) then
     h(:,1:mh,izs:ize) = f(:,1:mh,izs:ize)
  end if
  if (mg .gt. 0) then
     g(:,1:mg,izs:ize) = f(:,my-mg+1:my,izs:ize)
  end if
end subroutine mpi_send_y

!-------------------------------------------------------------------------------
subroutine mpi_send_z (f, g, mg, h, mh)
  use params
  use mpi_mod
  implicit none
  integer:: mg, mh
  real, dimension(mx,my,mz):: f
  real, dimension(mx,my,mg):: g
  real, dimension(mx,my,mh):: h

  if (mh .gt. 0) then
     h(:,:,1:mh) = f(:,:,1:mh)
  end if
  if (mg .gt. 0) then
     g(:,:,1:mg) = f(:,:,mz-mg+1:mz)
  end if
end subroutine mpi_send_z

!-------------------------------------------------------------------------------
function average (f)
  use params
  implicit none

  !  Return average of array.  Make sure precision is retained, by 
  !  summing first individual rows, then columns, then planes.

  real f(mx,my,mz), average, av

  call average_subr (f, av)
  average = av
end function average

!-------------------------------------------------------------------------------
subroutine average_subr (f,average)
  use params
  implicit none
  real f(mx,my,mz), average
  logical omp_in_parallel

  if (omp_in_parallel()) then
     call average_omp(f,average)
  else
     !$omp parallel shared(average)
     call average_omp(f,average)
     !$omp end parallel
  end if
end subroutine average_subr

!-------------------------------------------------------------------------------
module dbl_m
  real(kind=8) dbl                                                      ! global dbl prec variable
end module dbl_m

!-------------------------------------------------------------------------------
subroutine average_omp (f, av)
  use params
  use dbl_m
  implicit none

  !  Return average of array.  Make sure precision is retained, by 
  !  summing first individual rows, then columns, then planes.

  integer i, j, k, lb1, ub1
  real f(mx,my,mz), av
  real(kind=8) sumx, sumy, sumz
  character(len=mid):: id = 'average_omp'

  call print_id(id)

  !$omp master
  dbl = 0.                                                              ! accumulate in dbl prec
  !$omp end master
  call barrier_omp('av_omp1')                                           ! make sure all threads agree
  lb1 = max(min(lb,my),1)
  ub1 = max(min(ub,my),1)
  sumz = 0.
  do k=izs,ize
     sumy = 0.
     do j=lb1,ub1
        sumx = 0.
        do i=1,mx
           sumx = sumx + f(i,j,k)
        end do
        sumy = sumy + sumx
     end do
     sumz = sumz + sumy
  end do
  !$omp critical
  dbl = dbl + sumz
  !$omp end critical
  call barrier_omp('av_omp2')
  !$omp critical
  av = dbl/(real(mx)*real(ub1-lb1+1)*real(mz))                          ! make sure all threads do it
  !$omp end critical
  call barrier_omp('av_omp3')
end subroutine average_omp

!-------------------------------------------------------------------------------
subroutine haverage_subr (f, av)
  use params
  implicit none
  real f(mx,my,mz)
  real(kind=8) av(my)
  logical omp_in_parallel

  if (omp_in_parallel()) then
     call haverage_omp (f, av)
  else
     !$omp parallel
     call haverage_omp (f, av)
     !$omp end parallel
  end if
end subroutine haverage_subr

!-------------------------------------------------------------------------------
subroutine haverage_omp (f, av)
  use params
  implicit none

  !  Return horizontal average of array f.  Make sure precision is retained,
  !  by summing first individual rows, then columns.  The "sumy" array is
  !  local to each CPU, and sums up each partial horizontal plane. Each
  !  CPU then delivers its result into av (which is global, since it is
  !  in the argument list.

  integer i, j, k, lb1, ub1
  real f(mx,my,mz)
  real(kind=8) sumx, sumy(my), av(my)

  do k=izs,ize
     do j=1,my
        sumx = 0.
        do i=1,mx
           sumx = sumx + f(i,j,k)
        end do
        sumy(j) = sumx
     end do
     haver_tmp(1:my,k) = sumy(1:my)
  end do
  call barrier_omp('haver')
  !$omp master
  do j=1,my
     av(j) = sum(haver_tmp(j,:))/(real(mx)*real(mz))
  end do
  !$omp end master
  call barrier_omp('haver')
end subroutine haverage_omp

!-------------------------------------------------------------------------------
subroutine haverage_bdry (f, av)
  use params
  implicit none
  real f(mx,my,mz)
  real(kind=8) av(my)
  logical omp_in_parallel

  if (omp_in_parallel()) then
     call haverage_bdry_omp (f, av)
  else
     !$omp parallel
     call haverage_bdry_omp (f, av)
     !$omp end parallel
  end if
end subroutine haverage_bdry

!-------------------------------------------------------------------------------
subroutine haverage_bdry_omp (f, av)
  use params
  implicit none

  !  Return horizontal average of array f, as in haverage_omp, but only
  !  for the boundary layers.

  integer i, j, k, lb1, ub1
  real f(mx,my,mz)
  real(kind=8) av(my)
  real(kind=8) sumx, sumy(my)

  do k=izs,ize
     sumy = 0.
     do j=1,lb
        sumx = 0.
        do i=1,mx
           sumx = sumx + f(i,j,k)
        end do
        sumy(j) = sumy(j) + sumx
     end do
     haver_tmp(1:lb,k) = sumy(1:lb)
     sumy = 0.
     do j=ub,my
        sumx = 0.
        do i=1,mx
           sumx = sumx + f(i,j,k)
        end do
        sumy(j) = sumy(j) + sumx
     end do
     haver_tmp(ub:my,k) = sumy(ub:my)
  end do
  call barrier_omp('haver_b')
  !$omp master
  do j=1,lb
     av(j) = sum(haver_tmp(j,:))/(real(mx)*real(mz))
  end do
  do j=ub,my
     av(j) = sum(haver_tmp(j,:))/(real(mx)*real(mz))
  end do
  !$omp end master
  call barrier_omp('nompi')

end subroutine haverage_bdry_omp

!-------------------------------------------------------------------------------
subroutine haverage_mpi (av, m)
  use params
  use mpi_mod
  implicit none
  integer m
  real(kind=8), dimension(m) :: av
  character(len=mid):: id = 'haverage_mpi'
  !
end subroutine haverage_mpi

!-------------------------------------------------------------------------------
subroutine sum_int_mpi (a)
  implicit none
  integer a
end subroutine sum_int_mpi

!-------------------------------------------------------------------------------
subroutine sum_int8_mpi (a)
  implicit none
  integer(kind=8):: a
end subroutine sum_int8_mpi

!-------------------------------------------------------------------------------
subroutine sum_real_mpi (a)
  implicit none
  real a
end subroutine sum_real_mpi

!-------------------------------------------------------------------------------
subroutine sum_real8_mpi (a)
  implicit none
  real(kind=8) a
end subroutine sum_real8_mpi

!-------------------------------------------------------------------------------
subroutine fminval_mpi (label, f, fmin)
  use params
  implicit none
  real, dimension(mx,my,mz):: f
  character(len=*) label
  real fmin
  !
  call fminval_omp (label, f, fmin)
end subroutine fminval_mpi

!-------------------------------------------------------------------------------
subroutine fmaxval_mpi (label, f, fmax)
  use params
  implicit none
  real, dimension(mx,my,mz):: f
  character(len=*) label
  real fmax
  !
  call fmaxval_omp (label, f, fmax)
end subroutine fmaxval_mpi

!-------------------------------------------------------------------------------
subroutine fmax_mpi (im, jm, km, fmax)
  implicit none
  integer im, jm, km
  real fmax
end subroutine fmax_mpi

!-------------------------------------------------------------------------------
subroutine fmin_mpi (im, jm, km, fmin)
  implicit none
  integer im, jm, km
  real fmin
end subroutine fmin_mpi

!-------------------------------------------------------------------------------
integer function imaxval_mpi (i)
  implicit none
  integer i
  imaxval_mpi = i
end function imaxval_mpi

!-------------------------------------------------------------------------------
real function rmaxval_mpi (r)
  implicit none
  real r
  rmaxval_mpi = r
end function rmaxval_mpi

!-------------------------------------------------------------------------------
subroutine imaxval_subr_mpi (i)
  implicit none
  integer i
end subroutine imaxval_subr_mpi

!-------------------------------------------------------------------------------
subroutine imaxval2_subr_mpi (i1, i2)
  implicit none
  integer i1, i2
end subroutine imaxval2_subr_mpi

!-------------------------------------------------------------------------------
function fmaxval(label,f)

  !  Max value returned, and written to file

  use params
  !use io
  implicit none

  character(len=*) :: label
  real, dimension(mx,my,mz):: f
  real :: fmaxval
  integer :: iz,loc1(1),loc2(2),im,jm,km,lb1,ub1
  logical :: omp_in_parallel

  !call fmaxval_subr(label,f,fmaxval)
  fmaxval = maxval(f)

end function fmaxval

!-------------------------------------------------------------------------------
subroutine horiz_minmax (f, fmin, fmax)
  use params

  implicit none

  real,   dimension(mx,my,mz) :: f
  real(kind=8), dimension(my) :: fmin, fmax
  integer iy

  ! returns minimum and maximum on horizontal planes


  call barrier_omp('horiz_minmax')
  !$omp master

  do iy=1,my
     fmin(iy) = dble( minval(f(:,iy,:)) )
     fmax(iy) = dble( maxval(f(:,iy,:)) )
  end do

  !$omp end master
  call barrier_omp('horiz_minmax')

end subroutine horiz_minmax

!-------------------------------------------------------------------------------
subroutine haver_updn(f,py,favUp,favDn,ffUp,ffDn)

  ! computes the horizontal averages of f in upflows 
  ! and downflows separately
  !
  ! f             : data cube
  ! py            : vertical momentum
  ! favUp (favDn) : horizontal average of f in upflows (downflows) only
  ! ffUp (ffDn)   : filling factor upflows (downflows)

  use params
  implicit none

  real, dimension(mx,my,mz),   intent(in)  :: f, py
  real,    dimension(mx,mz) :: fSlice, pySlice, fSliceUp, fSliceDn
  integer, dimension(mx,mz) :: maskUp, maskDn

  real(kind=8), dimension(my), intent(out) :: favUp,  favDn,  ffUp,  ffDn
  integer,      dimension(my)              :: numUp, numDn
  integer       iy,iz
  real(kind=8)  size


  size = dble( mxtot * mztot ) 

  do iy=1,my

     do iz=1,mz
        fSlice(:,iz) =  f(:,iy,iz)
        pySlice(:,iz)= py(:,iy,iz)
     enddo

     ! py >  0 : downflows
     ! py <= 0 : upflows

     where (pySlice .gt. 0.0)
        maskDn   = 1
        maskUp   = 0
        fSliceDn = fSlice
        fSliceUp = 0.0
     elsewhere
        maskDn   = 0
        maskUp   = 1
        fSliceDn = 0.0
        fSliceUp = fSlice        
     endwhere

     numUp(iy) = sum(maskUp)
     numDn(iy) = sum(maskDn)

     favUp(iy) = dble( sum(fSliceUp) )
     favDn(iy) = dble( sum(fSliceDn) ) 

  enddo


  where ( numUp .gt. 0) 
     favUp = favUp / dble( numUp )
  elsewhere
     favUp = 0.0
  endwhere

  where ( numDn .gt. 0) 
     favDn = favDn / dble( numDn )
  elsewhere
     favDn = 0.0
  endwhere

  ffUp(:) = dble( numUp(:) ) / size
  ffDn(:) = dble( numDn(:) ) / size


end subroutine haver_updn

!-------------------------------------------------------------------------------
subroutine read_chunk (f,io_type,iv,snap,unit)
  use params
  implicit none
  real, dimension(mx,my,mz):: f
  integer snap,unit,io_type
  integer ix,iy,iz,irec,iv,mv
  !
  if (io_type==1) then
     do iz=1,mz
        irec = iz+mz*(iv+snap*mvar)
        read (unit,rec=irec) f(:,:,iz)
     end do
  else
     read (unit,rec=1+iv+snap*mvar) f
  end if
end subroutine read_chunk

!-------------------------------------------------------------------------------
subroutine write_chunk (unit,rec,io_type,f)
  use params
  implicit none
  real, dimension(mx,my,mz):: f
  integer unit, rec, iz, io_type

  if (io_type > 0) then
     do iz=1,mz
        write (unit,rec=mz*(rec-1)+iz) f(:,:,iz)
     end do
  else
     write (unit,rec=rec) f
  end if
end subroutine write_chunk

!-------------------------------------------------------------------------------
subroutine sum_2d (f, ny, sum2d)

  !  Return average of array in the local domain.  Make sure precision is
  !  retained, by summing first individual rows, then columns, then planes.
  !  --- NOTE: The return value should be thread-local variable!

  use params
  !use math_m
  implicit none
  integer ny
  real f(mx,mz,ny)
  real(kind=8) sum2d(ny)

  call sum_2d_omp (f, ny, sum2d)
end subroutine sum_2d

!-------------------------------------------------------------------------------
logical function any_mpi (l)
  use mpi_mod
  implicit none
  logical l
  any_mpi = l
end function any_mpi

!-------------------------------------------------------------------------------
subroutine broadcast_mpi (a, n)
  use mpi_mod
  implicit none
  integer n
  real, dimension(n):: a
  !
end subroutine broadcast_mpi

!-------------------------------------------------------------------------------
subroutine send_real_mpi (a, n, from, to)

  !  Send the boundary layers from one domain over to the neighboring domains
  !  and recieve the corresponding values into temporary arrays.

  use mpi_mod
  implicit none
  integer n, from, to
  real, dimension(n):: a

end subroutine send_real_mpi

!-------------------------------------------------------------------------------
subroutine extend_mpi (f, mx, my, mz, g, m)

  ! Extend an array with m ghost zones on each side

  use params, only: izs, ize, omp_mythread
  implicit none

  integer mx, my, mz, m, jys, jye
  real f(mx,my,mz), g(1-m:mx+m,1-m:my+m,1-m:mz+m)


  !$omp barrier
  g(   1:mx  ,    1:my  ,  izs:ize ) = f(     1:mx  ,      1:my  ,    izs:ize)       ! copy interior

  g( 1-m:0   ,    1:my  ,  izs:ize ) = f(mx-m+1:mx  ,      1:my  ,    izs:ize)       ! extend in x
  g(mx+1:mx+m,    1:my  ,  izs:ize ) = f(     1:m   ,      1:my  ,    izs:ize)       ! extend in x

  g( 1-m:mx+m,  1-m:0   ,  izs:ize ) = g(   1-m:mx+m, my-m+1:my  ,    izs:ize)       ! extend in y
  g( 1-m:mx+m, my+1:my+m,  izs:ize ) = g(   1-m:mx+m,      1:m   ,    izs:ize)       ! extend in y

  !$omp barrier
  call limits_omp(1-m,my+m,jys,jye)

  g( 1-m:mx+m,  jys:jye ,  1-m:0   ) = g(   1-m:mx+m,    jys:jye , mz-m+1:mz )       ! extend in z
  g( 1-m:mx+m,  jys:jye , mz+1:mz+m) = g(   1-m:mx+m,    jys:jye ,      1:m  )       ! extend in z

end subroutine extend_mpi

!-------------------------------------------------------------------------------
function flag(file)
  use mpi_mod
  use params, only: flag_unit, master
  implicit none
  character(len=*) file
  logical flag

  inquire (file=file//'.flag',exist=flag)
  if (flag) then
     open (flag_unit,file=file//'.flag',status='old')
     close (flag_unit,status='delete')
  end if
end function flag

!-------------------------------------------------------------------------------
subroutine mpi_send2_x (f, nx, ny, nz, g, ng, h, nh)

  !  Send the boundary layers from one domain over to the neighboring domains
  !  and recieve the corresponding values into temporary arrays.

  use params
  use mpi_mod
  implicit none

  integer nx, ny, nz, ng, nh
  real, dimension(nx,ny,nz):: f
  real, dimension(ng,ny,nz):: g, g1
  real, dimension(nh,ny,nz):: h, h1

  stop

end subroutine mpi_send2_x

!-------------------------------------------------------------------------------
subroutine mpi_send2_y (f, nx, ny, nz, g, ng, h, nh)

  !  Send the boundary layers from one domain over to the neighboring domains
  !  and recieve the corresponding values into temporary arrays.

  use params
  use mpi_mod
  implicit none

  integer nx, ny, nz, ng, nh
  real, dimension(nx,ny,nz):: f
  real, dimension(ny,ng,nz):: g, g1
  real, dimension(ny,nh,nz):: h, h1

  stop

end subroutine mpi_send2_y

!-------------------------------------------------------------------------------
subroutine mpi_send2_z (f, nx, ny, nz, g, ng, h, nh)

  !  Send the boundary layers from one domain over to the neighboring domains
  !  and recieve the corresponding values into temporary arrays.

  use params
  use mpi_mod
  implicit none

  integer nx, ny, nz, ng, nh
  real, dimension(nx,ny,nz):: f
  real, dimension(ny,nz,ng):: g, g1
  real, dimension(ny,nz,nh):: h, h1

  stop

end subroutine mpi_send2_z

!-------------------------------------------------------------------------------
subroutine ghost_fill (mx,my,mz,ny,ma,ss)
  implicit none
  integer mx,my,mz,ny,ma
  real ss(1-ma:mx+ma,0:my+1,1-ma:mz+ma)

  ss(1-ma:0,0:ny+1,:) = ss(mx-ma+1:mx,0:ny+1,:)
  ss(:,0:ny+1,1-ma:0) = ss(:,0:ny+1,mz-ma+1:mz)
  ss(mx+1:mx+ma,0:ny+1,:) = ss(1:ma,0:ny+1,:)
  ss(:,0:ny+1,mz+1:mz+ma) = ss(:,0:ny+1,1:ma)
end subroutine ghost_fill

!-------------------------------------------------------------------------------
subroutine ghost_fill_x (mx,my,mz,f1,ng,mblocks,ny,izs,ize,f2)
  implicit none
  integer mx,my,mz,ny,ng,izs,ize,mblocks
  real f1(mx,my,mz),f2(1-ng:mx+ng,ny,mz)

  if (ng > mx) then
     print *,'ghost_fill_x: ng larger than mx',ng,mx
     stop
  end if
  f2(1   :mx   ,1:ny,izs:ize) = f1(1      :mx,1:ny,izs:ize)
  f2(1-ng:0    ,1:ny,izs:ize) = f1(mx-ng+1:mx,1:ny,izs:ize)
  f2(mx+1:mx+ng,1:ny,izs:ize) = f1(1      :ng,1:ny,izs:ize)
end subroutine ghost_fill_x

!-------------------------------------------------------------------------------
subroutine ghost_fill_z (mx,my,mz,f1,ng,mblocks,ny,izs,ize,f2)
  implicit none
  integer mx,my,mz,ny,ng,izs,ize,iz,mblocks
  real f1(mx,my,mz),f2(mx,ny,1-ng:mz+ng)

  if (ng > mz) then
     print *,'ghost_fill_z: ng .ne. mz',ng,mz
     stop
  end if
  f2(1:mx,1:ny,izs:ize)       = f1(1:mx,1:ny,izs:ize)
  do iz=mz+1,mz+ng
     if ((iz-mz) >= izs .and. (iz-mz) <= ize) then                               ! each OMP thread handles its range
        f2(1:mx,1:ny,iz) = f1(1:mx,1:ny,iz-mz)                                    ! copy from iz-mz to iz
     end if
  end do
  do iz=1-ng,0
     if ((iz+mz) >= izs .and. (iz+mz) <= ize) then                               ! each OMP thread handles its range
        f2(1:mx,1:ny,iz) = f1(1:mx,1:ny,iz+mz)                                    ! copy from iz+mz to iz
     end if
  end do
end subroutine ghost_fill_z

!-------------------------------------------------------------------------------
subroutine sum_mpi_real (a, asum, n)
  integer n
  real a(n), asum(n)
  asum = a
end subroutine sum_mpi_real

!-------------------------------------------------------------------------------
subroutine file_open_mpi (file, mode)
  implicit none
  integer mode
  character(len=*) file
end subroutine file_open_mpi

!-------------------------------------------------------------------------------
subroutine file_openw_mpi (file)
  implicit none
  character(len=*) file
end subroutine file_openw_mpi

!-------------------------------------------------------------------------------
subroutine file_openr_mpi (file)
  implicit none
  character(len=*) file
end subroutine file_openr_mpi

!-------------------------------------------------------------------------------
subroutine file_write_mpi (f,rec)
  use params, only: mx, my, mz
  implicit none
  real, dimension(mx,my,mz):: f
  integer rec
end subroutine file_write_mpi

!-------------------------------------------------------------------------------
subroutine file_read_mpi (f, rec)
  use params, only: mx, my, mz
  implicit none
  real, dimension(mx,my,mz):: f
  integer rec
end subroutine file_read_mpi

!-------------------------------------------------------------------------------
subroutine file_close_mpi
  implicit none
end subroutine file_close_mpi

!-------------------------------------------------------------------------------
function fexists(file)
  implicit none
  character(len=*) file
  logical fexists
  inquire (file=file,exist=fexists)
end function fexists

!-------------------------------------------------------------------------------
subroutine mpi_send_bdry_y_dn (f, n, g)
  use params
  implicit none
  integer n
  real, dimension(mx,my,mz) :: f
  real, dimension(mx, n,mz) :: g
end subroutine mpi_send_bdry_y_dn

!-------------------------------------------------------------------------------
subroutine mpi_send_bdry_y_up (f, n, g)
  use params
  implicit none
  integer n
  real, dimension(mx,my,mz) :: f
  real, dimension(mx, n,mz) :: g
end subroutine mpi_send_bdry_y_up

!-------------------------------------------------------------------------------
subroutine mpi_gather_single_layers_y(f, iy, g) 
  use params
  integer :: iy
  real, dimension(mx,  my, mz)         :: f
  real, dimension(mx,   1, mz, mpi_ny) :: g
end subroutine mpi_gather_single_layers_y

!-------------------------------------------------------------------------------
subroutine mpi_gather_slices_y(f, g) 
  use params
  implicit none
  real, dimension(mx,  1, mz)         :: f
  real, dimension(mx,  1, mz, mpi_ny) :: g
end subroutine mpi_gather_slices_y

!-------------------------------------------------------------------------------
subroutine MPI_CART_COORDS(new_com, rank, n, coords, mpi_err)
  implicit none
  integer new_com, rank, n, coords(n), mpi_err
  coords(:) = 0
end subroutine MPI_CART_COORDS

!-------------------------------------------------------------------------------
subroutine MPI_ALLTOALL (send, send_count, send_type, &
     recv, recv_count, recv_type, &
     comm, err)
  use params, only: mpi_size
  implicit none
  integer, dimension(mpi_size)::  send, recv 
  integer:: send_count, recv_count, send_type, recv_type, comm, err, i
  do i=1,mpi_size
     recv(i) = send(i)
  end do
end subroutine MPI_ALLTOALL

!-------------------------------------------------------------------------------
subroutine alltoallv_mpi (send, send_count, send_offset, &
     recv, recv_count, recv_offset, &
     mpi_type, mpi_rank, mpi_size, nbuf, comm)
  use mpi
  implicit none
  real, dimension(nbuf):: send, recv
  integer:: mpi_type, mpi_rank, mpi_size, comm, err, i, n, o, nbuf
  integer, dimension(0:mpi_size-1):: send_count, send_offset, send_req, &
       recv_count, recv_offset, recv_req
  integer:: status(MPI_STATUS_SIZE)

  !$omp barrier
  !$omp master

  if (mpi_type == MPI_REAL8) then
     do i=0,mpi_size-1
        recv(recv_offset(i)*2+1:recv_offset(i)*2+recv_count(1)*2) = &
             send(send_offset(i)*2+1:send_offset(i)*2+send_count(1)*2)
     end do
  else
     do i=0,mpi_size-1
        recv(recv_offset(i)+1:recv_offset(i)+recv_count(1)) = &
             send(send_offset(i)+1:send_offset(i)+send_count(1))
     end do
  end if

  !$omp end master
  !$omp barrier
end subroutine alltoallv_mpi

!-------------------------------------------------------------------------------
subroutine buffer_count
end subroutine buffer_count

!-------------------------------------------------------------------------------
subroutine buffer_reset (nsent)
  implicit none
  integer(kind=8) nsent
  nsent = 0_8
end subroutine buffer_reset

!-------------------------------------------------------------------------------
subroutine gather_y_mpi (f, ff)
  use params
  use mpi_mod
  implicit none
  real f(mx,my,mz), ff(mx,my,mz)
  ff = f
end subroutine gather_y_mpi

!-------------------------------------------------------------------------------
