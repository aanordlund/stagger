! $Id: srpde.f90,v 1.25 2012/12/31 16:19:16 aake Exp $
!***********************************************************************
 SUBROUTINE pde(r,px,py,pz,e,d,drdt,dpxdt,dpydt,dpzdt,dedt,dddt,flag, &
                 Bx,By,Bz,dBxdt,dBydt,dBzdt)
  USE params
  USE stagger
  USE forcing
  USE eos
  USE polysolve
  USE visc

  implicit none

  real ds, gam1, average, fmaxval,nuimpl
  integer iy,iz,impl,impl_last

  logical flag
  real, dimension(mx,my,mz) :: &
       r,px,py,pz,e,d,drdt,dpxdt,dpydt,dpzdt,dedt,dddt, &
       Bx,By,Bz,dBxdt,dBydt,dBzdt
!hpf$ distribute (*,*,block) :: &
!hpf$  r,px,py,pz,e,d,drdt,dpxdt,dpydt,dpzdt,dedt,dddt, &
!hpf$  Bx,By,Bz,dBxdt,dBydt,dBzdt
  real, allocatable, dimension(:,:,:) :: &
       dd,lnr,P,Cs,divU,nu,lnu,lne,ee,Ux,Uy,Uz, &
       xdnl,ydnl,zdnl,xdnr,ydnr,zdnr, &
       eta,Txx,Tyy,Tzz,Txy,Tzx,Tyz,Ttx,Tty,Ttz,Ttt,ddxd,ddyd,ddzd
  real, allocatable, dimension(:,:,:) :: &
       pxup,pyup,pzup,SPD,hm1,vW,W,Dh,rh,DhW,lnDh,Uxup,Uyup,Uzup
  real, allocatable, dimension(:,:,:) :: &
       Wx,Wy,Wz,vWxy,vWyz,vWzx,Sxy,Szx,Syz, &
       Oxx,Oyy,Ozz,Oxy,Oyz,Ozx,UWx,UWy,UWz,Sxx,Syy,Szz
  real, allocatable, dimension(:,:)   :: soundsp, velocity
!hpf$ distribute (*,*,block) :: &
!hpf$  dd,lnr,P,Cs,divU,nu,lnu,lne,ee,Ux,Uy,Uz, &
!hpf$  Sxx,Syy,Szz,Sxy,Szx,Syz,xdnl,ydnl,zdnl,xdnr,ydnr,zdnr, &
!hpf$  eta,Txx,Tyy,Tzz,Txy,Tzx,Tyz,ddxd,ddyd,ddzd
  character(len=mid) id
  data id/'$Id: srpde.f90,v 1.25 2012/12/31 16:19:16 aake Exp $'/

  if (id .ne. '') print *,id
  id = ''
  gam1 = gamma - 1.
!-----------------------------------------------------------------------
!  Correcting for metric factors, finding the primitive variables:
!  specific enthalpy minus one, Lorentz Boost
!-----------------------------------------------------------------------
  allocate(pxup(mx,my,mz),pyup(mx,my,mz),pzup(mx,my,mz))
  pxup = xup(px)         !
  pyup = yup(py)         !
  pzup = zup(pz)         ! Centered momenta
  ! We don't want noise from wrap-around boundaries, which can crash rhdsolve
  call velocity_boundary(r,xdnr,ydnr,zdnr,pxup,pyup,pzup,px,py,pz,e,Bx,By,Bz)
  ! Lets go find those primitive variables - Centered
  allocate(hm1(mx,my,mz),vW(mx,my,mz),SPD(mx,my,mz))
  SPD = pxup**2 + pyup**2 + pzup**2
  call rhdsolve(r, e, SPD, vW, hm1)
  deallocate(SPD)
  If (minval(hm1) .lt. 0) Then
    Print *, 'hm1 is negative - should not happen!!', minval(hm1)
  end if
  If (minval(vW) .lt. 0) Then
    Print *, 'vW is negative - should not happen!!', minval(vW)
  end if
  if (idbg .eq. -2) Then
    Print *,"min,av,max:",minval(vW),average(vW),maxval(vW),"vW"
    Print *,"min,av,max:",minval(hm1),average(hm1),maxval(hm1),"hm1"
  End if
!-----------------------------------------------------------------------
!  Velocities, Lorentz Boost
!-----------------------------------------------------------------------
  allocate(Uxup(mx,my,mz),Uyup(mx,my,mz),Uzup(mx,my,mz)) !  6 chunks
  allocate(W(mx,my,mz),Dh(mx,my,mz),DhW(mx,my,mz),lnDh(mx,my,mz))
!$omp parallel do private(iz)
  do iz=1,mz
      W(:,:,iz)   = Sqrt(1. + vW(:,:,iz)**2)             ! Gamma factor centered
      Dh(:,:,iz)  = (1. + hm1(:,:,iz))*r(:,:,iz)
      DhW(:,:,iz) = Dh(:,:,iz)*W(:,:,iz)
      lnDh(:,:,iz)= Alog(Dh(:,:,iz))
      Uzup(:,:,iz) = 1./DhW(:,:,iz)
      Uxup(:,:,iz) = pxup(:,:,iz)*Uzup(:,:,iz)          ! V_x - centered
      Uyup(:,:,iz) = pyup(:,:,iz)*Uzup(:,:,iz)          ! V_y - centered
      Uzup(:,:,iz) = pzup(:,:,iz)*Uzup(:,:,iz)          ! V_z - centered
  end do
  if (idbg .eq. -2) Then
    Print *,"min,av,max:",minval(W),average(W),maxval(W),"W"
    Print *,"min,av,max:",minval(Dh),average(Dh),maxval(Dh),"Dh"
    Print *,"min,av,max:",minval(Uyup),average(Uyup),maxval(Uyup),"Uyup"
  End if
  deallocate(pxup,pyup,pzup)                                             ! 10 chunks
!-----------------------------------------------------------------------
!  Velocities, face values
!-----------------------------------------------------------------------
  allocate(Ux(mx,my,mz), Uy(mx,my,mz), Uz(mx,my,mz), &
           UWx(mx,my,mz),UWy(mx,my,mz),UWz(mx,my,mz)) !  6 chunks
  Ux = px*exp(-xdn(alog(DhW))) !xdn(Uxup) ! ...Face values
  Uy = py*exp(-ydn(alog(DhW))) !ydn(Uyup) ! ...Face values
  Uz = pz*exp(-zdn(alog(DhW))) !zdn(Uzup) ! ...Face values
  UWx = px*exp(-xdn(lnDh))
  UWy = py*exp(-ydn(lnDh))
  UWz = pz*exp(-zdn(lnDh))
  !Uxup = xup(Ux)
  !Uyup = yup(Uy)
  !Uzup = zup(Uz)
!  call velocity_boundary(r,xdnr,ydnr,zdnr,Ux,Uy,Uz,px,py,pz,e,Bx,By,Bz)
!  call forceit (r,Ux,Uy,Uz,xdnr,ydnr,zdnr,dpxdt,dpydt,dpzdt)
  allocate(Sxx(mx,my,mz),Syy(mx,my,mz),Szz(mx,my,mz))                   !  9 chunks
! Divergence of SFC system three velocity.
  Sxx = ddxup(Ux)
  Syy = ddyup(Uy)
  Szz = ddzup(Uz)
!-----------------------------------------------------------------------
!  Numerical viscosity, Richtmyer & Morton type
!-----------------------------------------------------------------------
  allocate(nu(mx,my,mz),lnu(mx,my,mz))     ! 12 chunks
!$omp parallel do private(iz)
  do iz=1,mz
    nu(:,:,iz) = Sxx(:,:,iz)+Syy(:,:,iz)+Szz(:,:,iz)
    nu(:,:,iz) = max(-nu(:,:,iz),0.0)
  end do
  deallocate(Sxx,Syy,Szz)                                             ! 10 chunks
!
  allocate(Cs(mx,my,mz),soundsp(mx,my),velocity(mx,my))
  ds = max(dx,dy,dz)
  gam1 = gamma - 1.
  Cs = Uxup**2 + Uyup**2  + Uzup**2
  Urms = sqrt(average(Cs))
!$omp parallel do private(iz), firstprivate(nu1,nu2,nu3,ds,velocity,soundsp)
  do iz=1,mz
    soundsp    = sqrt(gam1*hm1(:,:,iz)/(1.+hm1(:,:,iz)))
    velocity   = sqrt(Cs(:,:,iz))
    Cs(:,:,iz) = soundsp + velocity
    nu(:,:,iz) = nu1*velocity + nu2*ds*nu(:,:,iz) + nu3*soundsp
  end do
  deallocate(soundsp,velocity)
  if (lb.lt.6) call regularize(nu)
  nu = smooth3(max5(nu))
  call regularize(nu)
!$omp parallel do private(iz)
  do iz=1,mz
    lnu(:,:,iz) = alog(nu(:,:,iz))
  end do
!-----------------------------------------------------------------------
!  Viscous stress, diagonal and off-diagonal elements
!-----------------------------------------------------------------------
  call viscosity(px,py,pz,Dh,lnDh,nu,lnu,vW,W,Ux,Uy,Uz,Uxup,Uyup,Uzup, &
                 UWx,UWy,UWz,Ttt,Txx,Tyy,Tzz,Txy,Tzx,Tyz,Ttx,Tty,Ttz)
  deallocate(Ttt)
!-----------------------------------------------------------------------
!  Viscosity, energy density
!-----------------------------------------------------------------------
  If (do_2nddiv) then
    dedt = dedt - ddxup1(Ttx) - ddyup1(Tty) - ddzup1(Ttz)
  else
    dedt = dedt - ddxup(Ttx) - ddyup(Tty) - ddzup(Ttz)
  endif
  deallocate (Ttx,Tty,Ttz)                                              ! 15 chunks
!-----------------------------------------------------------------------
!  Energy per unit mass
!-----------------------------------------------------------------------
  allocate(ee(mx,my,mz))
!  if (do_ionization) then
!    call pressure(drho,ee,p,vW,W)
!  else
!$omp parallel do private(iz)
  do iz=1,mz
    ee(:,:,iz)  = 1./gamma*hm1(:,:,iz)          ! internal energy
  end do
!-----------------------------------------------------------------------
!  Boundary condition -- may need to calculate pressure internally
!-----------------------------------------------------------------------
!  if (do_energy) call energy_boundary(r,lnr,Ux,Uy,Uz,e,ee,p,Bx,By,Bz)
!-----------------------------------------------------------------------
!  Pressure
!-----------------------------------------------------------------------
  allocate(P(mx,my,mz),rh(mx,my,mz))
!$omp parallel do private(iz)
  do iz=1,mz
    rh(:,:,iz) = r(:,:,iz)/W(:,:,iz)
    P(:,:,iz)  = gam1*rh(:,:,iz)*ee(:,:,iz)
    rh(:,:,iz) = r(:,:,iz)*(1. + hm1(:,:,iz))
  end do
!-----------------------------------------------------------------------
! Time stepping
!-----------------------------------------------------------------------
  if (flag) then
    ds = min(dx,dy,dz)
    Cs = Cs*(dt/ds)
    Cu = fmaxval('Cu',Cs)
    Cs = (6.2*3.*0.75/2.*dt/ds)*nu
    Cv = fmaxval('Cv',Cs)
  end if
  deallocate(Cs)                                                        ! 12 chunks
!-----------------------------------------------------------------------
!  Mass conservation, cell centered
!-----------------------------------------------------------------------
  allocate(lnr(mx,my,mz),xdnl(mx,my,mz),ydnl(mx,my,mz),zdnl(mx,my,mz))  !  4 chunks
!$omp parallel do private(iz)
  do iz=1,mz
    lnr(:,:,iz) = alog(r(:,:,iz))
  end do
  xdnl = xdn(lnr)
  ydnl = ydn(lnr)
  zdnl = zdn(lnr)
  if (do_2nddiv) then
    if (nur>0) then
      drdt = drdt &
         - ddxup1(Ux*exp(xdnl)-dx*nur*exp(xdn(lnu+lnr))*ddxdn(lnr)) &
         - ddyup1(Uy*exp(ydnl)-dy*nur*exp(ydn(lnu+lnr))*ddydn(lnr)) &
         - ddzup1(Uz*exp(zdnl)-dz*nur*exp(zdn(lnu+lnr))*ddzdn(lnr))
    else
      drdt = drdt - ddxup1(Ux*exp(xdnl)) - ddyup1(Uy*exp(ydnl)) - ddzup1(Uz*exp(zdnl))
    end if
  else
    if (nur>0) then
      drdt = drdt &
         - ddxup(Ux*exp(xdnl)-dx*nur*exp(xdn(lnu+lnr))*ddxdn(lnr)) &
         - ddyup(Uy*exp(ydnl)-dy*nur*exp(ydn(lnu+lnr))*ddydn(lnr)) &
         - ddzup(Uz*exp(zdnl)-dz*nur*exp(zdn(lnu+lnr))*ddzdn(lnr))
    else
      drdt = drdt - ddxup(Ux*exp(xdnl)) - ddyup(Uy*exp(ydnl)) - ddzup(Uz*exp(zdnl))
    end if
  end if
  deallocate(xdnl,ydnl,zdnl)                                            ! 10 chunks
!-----------------------------------------------------------------------
!  Add Reynolds stress
!-----------------------------------------------------------------------
  do iz=1,mz
    Txx(:,:,iz) = Txx(:,:,iz) + P(:,:,iz)
    Tyy(:,:,iz) = Tyy(:,:,iz) + P(:,:,iz)
    Tzz(:,:,iz) = Tzz(:,:,iz) + P(:,:,iz)
  end do
!*$* assert do (concurrent)
  Txx = Txx + DhW*Uxup**2
!*$* assert do (concurrent)
  Tyy = Tyy + DhW*Uyup**2
!*$* assert do (concurrent)
  Tzz = Tzz + DhW*Uzup**2
  lnDh  = Alog(DhW)
  Txy = Txy + exp(xdn(ydn(lnDh)))*ydn(Ux)*xdn(Uy)
  Tyz = Tyz + exp(ydn(zdn(lnDh)))*zdn(Uy)*ydn(Uz)
  Tzx = Tzx + exp(zdn(xdn(lnDh)))*xdn(Uz)*zdn(Ux)
  deallocate(lnDh)
!-----------------------------------------------------------------------
!  Equations of motion, face centred
!-----------------------------------------------------------------------
  if (do_2nddiv) then
    dpxdt = dpxdt - ddxdn1(Txx) - ddyup1(Txy) - ddzup1(Tzx)
    dpydt = dpydt - ddydn1(Tyy) - ddzup1(Tyz) - ddxup1(Txy)
    dpzdt = dpzdt - ddzdn1(Tzz) - ddxup1(Tzx) - ddyup1(Tyz)
  else
    dpxdt = dpxdt - ddxdn (Txx) - ddyup (Txy) - ddzup (Tzx)
    dpydt = dpydt - ddydn (Tyy) - ddzup (Tyz) - ddxup (Txy)
    dpzdt = dpzdt - ddzdn (Tzz) - ddxup (Tzx) - ddyup (Tyz)
  end if
  deallocate(Txy,Tyz,Tzx,Txx,Tyy,Tzz)                                   !  1 chunks
!-----------------------------------------------------------------------
!  MHD
!-----------------------------------------------------------------------
  if (do_mhd) then
    allocate (eta(mx,my,mz))
    call resist(eta,rh,P,Uxup,Uyup,Uzup,Bx,By,Bz,flag)
    call mhd(eta,W,Ux,Uy,Uz,UWx,UWy,UWz, &
             Bx,By,Bz,dpxdt,dpydt,dpzdt,dedt,dBxdt,dBydt,dBzdt)
    deallocate (eta)
  end if
  deallocate(W,Uxup,Uyup,Uzup,UWx,UWy,UWz,rh) !  6 chunks
!-----------------------------------------------------------------------
!  Abundance per unit mass
!-----------------------------------------------------------------------
!  if (do_pscalar .or. do_cool) then
!    allocate (dd(mx,my,mz))
!    dd=d/r
!  end if

!-----------------------------------------------------------------------
!  Passive scalar
!-----------------------------------------------------------------------
!  call passive(nu,lnu,r,px,py,pz,Ux,Uy,Uz,dd,dddt)
  deallocate (nu)                                              !  7 chunks

  if (do_energy) then

  allocate(lne(mx,my,mz))                                             !  3 chunks
!$omp parallel do private(iz)
    do iz=1,mz
      lne(:,:,iz) = alog(e(:,:,iz) + P(:,:,iz))
      lnu(:,:,iz) = lnu(:,:,iz) + lnr(:,:,iz)
      lnr(:,:,iz) = Alog(hm1(:,:,iz))
      lnu(:,:,iz) = lnu(:,:,iz) + lnr(:,:,iz)
    end do
!-----------------------------------------------------------------------
!  Energy advection and diffusion
!-----------------------------------------------------------------------
    if (do_2nddiv) then
      dedt = dedt &
       - ddxup1(exp(xdn(lne))*Ux - dx*exp(xdn(lnu))*ddxdn(lnr)) &
       - ddyup1(exp(ydn(lne))*Uy - dy*exp(ydn(lnu))*ddydn(lnr)) &
       - ddzup1(exp(zdn(lne))*Uz - dz*exp(zdn(lnu))*ddzdn(lnr))
    else
      dedt = dedt &
       - ddxup (exp(xdn(lne))*Ux - dx*exp(xdn(lnu))*ddxdn(lnr)) &
       - ddyup (exp(ydn(lne))*Uy - dy*exp(ydn(lnu))*ddydn(lnr)) &
       - ddzup (exp(zdn(lne))*Uz - dz*exp(zdn(lnu))*ddzdn(lnr))
    end if 
    deallocate(P,Ux,Uy,Uz)

!-----------------------------------------------------------------------
!  Cooling and conduction
!-----------------------------------------------------------------------
    if (do_pscalar .or. do_cool) then
      call conduction (r,e,ee,Bx,By,Bz,dedt)
      call coolit (r,ee,lne,dd,dedt)
    end if
    deallocate(lne)                                                     !  3 chunks         
  else
    deallocate(P)
  end if
  deallocate(ee,lnu)                                                    !  1 chunks
  deallocate(lnr)                                                       !  3 chunks
  if (do_pscalar .or. do_cool) deallocate(dd)                           !  0 chunks
! print **,5,dedt(16,106,38:40)/e(16,106,38:40),e(16,106,38:40)

  call ddt_boundary (r,px,py,pz,e,Bx,By,Bz,drdt,dpxdt,dpydt,dpzdt, &
                     dedt,dBxdt,dBydt,dBzdt)
  END
