#!/bin/csh

make clean
smake EXPERIMENT=torgny PDE=pde_call
mv torgny.x torgny_call.x
smake EXPERIMENT=torgny PDE=pde

./torgny_call.x < TESTS/compare.in
mv TESTS/test.dat TESTS/test_call.dat
./torgny.x < TESTS/compare.in
idl << EOF
.run TESTS/compare.pro
EOF

