  USE mpi
  implicit none
  integer(kind=MPI_ADDRESS_KIND) nbytes, offset
  integer mpi_err, mpi_size, mpi_rank, win, i, n, rank
  real, save, dimension(100):: b, c

  !----------------------------------------------------------------------------
  !  Start up MPI and get number of nodes and our node (mpi_rank)
  !----------------------------------------------------------------------------
  call MPI_INIT (mpi_err)
  call MPI_COMM_SIZE (MPI_COMM_WORLD, mpi_size, mpi_err)
  call MPI_COMM_RANK (MPI_COMM_WORLD, mpi_rank, mpi_err)

  !----------------------------------------------------------------------------
  ! Create MPI remote memory window and assert that it is clean
  !----------------------------------------------------------------------------
  nbytes = 400
  call MPI_Win_create (c, nbytes, 1, MPI_INFO_NULL, MPI_COMM_WORLD, win, mpi_err)
  !call MPI_Win_fence (MPI_MODE_NOPRECEDE, win, mpi_err)

  !----------------------------------------------------------------------------
  ! Window accessed memory has to be locked before changing it
  !----------------------------------------------------------------------------
  n = 100
  if (mpi_rank==0) then
    !call MPI_Win_lock (MPI_LOCK_EXCLUSIVE, 0, 0, win, mpi_err)
    do i=1,n
      c(i)=i
    end do
    !call MPI_Win_unlock (0, win, mpi_err)
  end if

  !----------------------------------------------------------------------------
  ! Note that offset is counted in bytes, while numbers are storage units
  !----------------------------------------------------------------------------
  offset = 8
  rank = 0
  n=n-offset/4
  call MPI_Win_fence (0, win, mpi_err)
  call MPI_Get (b, n, MPI_REAL, rank, offset, n, MPI_REAL, win, mpi_err)
  call MPI_Win_fence (0, win, mpi_err)

  !----------------------------------------------------------------------------
  ! The printout shows that b(98)=c(100)=100
  !----------------------------------------------------------------------------
  print'(2i5,6f6.0)',mpi_rank,mpi_size,c(1),b(1),b(98:100)

  !----------------------------------------------------------------------------
  ! Assert there is no more window activity, and free the window
  !----------------------------------------------------------------------------
  call MPI_Win_fence (MPI_MODE_NOSUCCEED, win, mpi_err)
  call MPI_Win_free (win, mpi_err)

  call MPI_FINALIZE (mpi_err)
END
