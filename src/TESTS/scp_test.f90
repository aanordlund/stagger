include 'mpif.h'
character*4 ii
character*80 cmd

call MPI_INIT (mpi_err)
call MPI_COMM_RANK (mpi_comm_world, mpi_rank, mpi_err)

write(ii,'(i4.4)') mpi_rank
cmd = 'scp test.csh disk3:'//ii//'.tmp </dev/null >/dev/null'
time = wallclock()
call system(cmd)
time1 = wallclock()-time

cmd = 'scp test.csh disk3:'//ii//'.tmp </dev/null >/dev/null &'
time = wallclock()
call system(cmd)
time2 = wallclock()-time

print '(i3,2f7.3,1x,a)',mpi_rank,time1,time2,cmd

call MPI_FINALIZE(mpi_err)

END

!***********************************************************************
REAL FUNCTION wallclock()
  integer, save:: count(2), count_rate=0
  if (count_rate == 0) then
    call system_clock(count=count(1), count_rate=count_rate)
  end if
  call system_clock(count=count(2))
  wallclock = (count(2)-count(1))/real(count_rate)
END

