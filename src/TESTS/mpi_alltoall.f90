! $Id: mpi_alltoall.f90,v 1.7 2014/08/17 13:49:23 aake Exp $
!*******************************************************************************
MODULE mpi_mod
  USE mpi
  implicit none
  !!include 'mpif.h'
  integer:: mpi_size, mpi_rank, mpi_err
END MODULE mpi_mod

!*******************************************************************************
SUBROUTINE alltoallv_mpi (send, send_count, send_offset, &
                          recv, recv_count, recv_offset, &
                          mpi_type, nbuf, comm, flag, verbose, word_count)
  USE mpi_mod
  implicit none
  real, dimension(nbuf):: send, recv
  integer:: mpi_type, comm, err, i, n, o, nbuf, verbose
  integer:: nsend, nrecv, j
  integer:: flag
  integer(kind=MPI_OFFSET_KIND), dimension(0:mpi_size-1):: &
                                     send_offset, recv_offset
  integer, dimension(0:mpi_size-1):: snd_offset, rcv_offset
  integer, dimension(0:mpi_size-1):: send_count, send_req, &
                                     recv_count, recv_req
  integer(kind=8):: word_count
  integer:: status(MPI_STATUS_SIZE)
  character(len=32) dbg_file
  !.............................................................................

  if (any(send_count<0) .or. &
      any(recv_count<0) .or. &
      any(send_offset<0) .or. &
      any(recv_offset<0)) then
    print*,'negative count or offset'
    stop
  end if

  if (flag==0) then
    if (all(send_offset < 2_8**31)) then
      snd_offset = send_offset
    else
      print*,'send_offset too large:', send_offset
      stop
    end if
    if (all(recv_offset < 2_8**31)) then
      rcv_offset = recv_offset
    else
      print*,'recv_offset too large:', recv_offset
      stop
    end if
    call MPI_ALLTOALLV (send, send_count, snd_offset, mpi_type, &
                        recv, recv_count, rcv_offset, mpi_type, &
                        comm, err)
    word_count = word_count + sum(send_count)
  else
    !---------------------------------------------------------------------------
    ! Post RECV from all senders that have something to send to us
    !---------------------------------------------------------------------------
    j = 0
    do i=0,mpi_size-1
      if (recv_count(i) > 0 .and. i /= mpi_rank) then
        n = recv_count(i)
        o = recv_offset(i) + 1
        call MPI_IRECV (recv(o:o+n-1), n, mpi_type, i, i, comm, recv_req(j), err)
        j = j + 1
        if (err /= 0) print*,'alltoallv ERROR 1',mpi_rank,i,err
      end if
    end do
    nrecv = j

    !---------------------------------------------------------------------------
    ! Make sure all receivers are ready to receive
    !---------------------------------------------------------------------------
    if (flag==1) call MPI_BARRIER (comm, err)

    !---------------------------------------------------------------------------
    ! Send synchronously to all receivers that we should send something to
    !---------------------------------------------------------------------------
    j = 0
    do i=0,mpi_size-1
      if (send_count(i) > 0) then
        n = send_count(i)
        o = send_offset(i) + 1
        if (i == mpi_rank) then
          recv(recv_offset(i)+1:recv_offset(i)+n) = send(o:o+n-1)
        else
          if (flag==1) then
            call MPI_ISSEND (send(o:o+n-1), n, mpi_type, i, mpi_rank, comm, send_req(j), err)
          else
            call MPI_ISEND (send(o:o+n-1), n, mpi_type, i, mpi_rank, comm, send_req(j), err)
          end if
          j = j + 1
          if (err /= 0) print*,'alltoallv ERROR 2',mpi_rank,i,err
          word_count = word_count + n
        end if
      end if
    end do
    nsend = j

    !---------------------------------------------------------------------------
    ! Wait for all send to complete
    !---------------------------------------------------------------------------
    call MPI_WAITALL (nsend, send_req, MPI_STATUSES_IGNORE, err)

    !---------------------------------------------------------------------------
    ! Wait for all recv to complete
    !---------------------------------------------------------------------------
    call MPI_WAITALL (nrecv, recv_req, MPI_STATUSES_IGNORE, err)

  end if
END SUBROUTINE

!*******************************************************************************
SUBROUTINE alltoall_mpi (send, recv, count, mpi_type, comm, flag, err)
  USE mpi_mod
  implicit none
  integer:: flag
  integer:: mpi_type, comm, err, i, count
  integer, dimension(count,0:mpi_size-1):: send, recv
  integer, dimension(0:mpi_size-1):: send_req, recv_req
  integer:: status(MPI_STATUS_SIZE)
  real(8):: wt
  !.............................................................................

  if (flag==0) then
    !$omp barrier
    !$omp master
    call MPI_Alltoall (send, count, mpi_type, recv, count, mpi_type, comm, err)
    !$omp end master
    !$omp barrier
    return
  end if
  !$omp barrier
  !$omp master

  !---------------------------------------------------------------------------
  ! Post RECV from all senders
  !---------------------------------------------------------------------------
  wt = MPI_wtime()
  do i=0,mpi_size-1
    call MPI_IRECV (recv(:,i), count, mpi_type, i, i, comm, recv_req(i), err)
  end do

  !---------------------------------------------------------------------------
  ! Make sure all receivers are ready to receive
  !---------------------------------------------------------------------------
  call MPI_BARRIER (comm, err)
  if (mpi_rank==0) print*,MPI_Wtime()-wt

  !---------------------------------------------------------------------------
  ! Send non-blocking synchronously to all receivers
  !---------------------------------------------------------------------------
  do i=0,mpi_size-1
    call MPI_ISSEND (send(:,i), count, mpi_type, i, mpi_rank, comm, send_req(i), err)
  end do

  !---------------------------------------------------------------------------
  ! Wait for all send to complete
  !---------------------------------------------------------------------------
  call MPI_WAITALL (mpi_size, send_req, MPI_STATUSES_IGNORE, err)
  if (mpi_rank==0) print*,MPI_Wtime()-wt

  !---------------------------------------------------------------------------
  ! Wait for all recv to complete
  !---------------------------------------------------------------------------
  call MPI_WAITALL (mpi_size, recv_req, MPI_STATUSES_IGNORE, err)
  if (mpi_rank==0) print*,MPI_Wtime()-wt

  !$omp end master
  !$omp barrier
END SUBROUTINE

!*******************************************************************************
SUBROUTINE test_alltoall (flag)
  USE mpi_mod
  implicit none
  integer:: nword, i, j, rank, flag
  integer, allocatable:: local(:), global(:,:)
  real(8):: wt
  logical:: ok
  real:: eps, MB

  do j=0,1
    nword = 10**j
    allocate (local(nword), global(nword,0:mpi_size-1))

    do i=1,nword
      local(i) = i + nword*mpi_rank
    end do

    wt = MPI_Wtime()
    call alltoall_mpi (local, global, nword, MPI_INTEGER, mpi_comm_world, flag, mpi_err)
    wt = MPI_Wtime()-wt

    if (mpi_rank==0) then
      ok = .true.
      do rank = 0,mpi_size-1
        do i=1,nword
          local(i) = i + nword*rank
        end do
        !print'(a,/,(10i8))', 'local',local(:)
        !print'(a,/,(10i8))', 'global',global(:,rank)
        eps = maxval(abs(local-global(:,rank)))
        MB = mpi_size*nword*4e-6
        ok = ok .and. (eps == 0)
      end do
      print'(a,l3,2i7,1p,2e10.2,l3)', &
        ' alltoall: intrinsic, mpi_size, nword, wt, MB, ok =', &
                    flag, mpi_size, nword, wt, MB, ok
    end if
    deallocate (local, global)
  end do

END

!*******************************************************************************
SUBROUTINE test_alltoallv (flag)
  USE mpi_mod
  implicit none
  integer:: nword, i, j, isnd, ircv
  integer(kind=MPI_OFFSET_KIND), allocatable, dimension(:):: snd_offset, rcv_offset
  integer, allocatable, dimension(:):: snd_count, rcv_count
  real, allocatable, dimension(:,:):: snd, rcv
  integer:: flag
  integer:: ntry=10, verbose=0
  integer(8):: nsent
  real(8):: wt
  real:: kB
!...............................................................................
  if (mpi_rank==0) print*,'test_alltoallv: $Id: mpi_alltoall.f90,v 1.7 2014/08/17 13:49:23 aake Exp $'

  allocate (snd_count(0:mpi_size-1), rcv_count(0:mpi_size-1))
  allocate (snd_offset(0:mpi_size-1), rcv_offset(0:mpi_size-1))
  ntry = min(ntry,mpi_size)
  do j=1,4
    nword = 10**j
    allocate (snd(nword,ntry), rcv(nword,ntry))

    !---------------------------------------------------------------------------
    ! Construct test data
    !---------------------------------------------------------------------------
    snd_count = 0
    rcv_count = 0
    snd_offset = 0
    rcv_offset = 0
    do i=0,ntry-1
      isnd=modulo(mpi_rank+i,mpi_size)             ! send to the next 10 ranks
      ircv=modulo(mpi_rank-i,mpi_size)             ! recv from the previous 10 ranks
      snd_count(isnd)=nword
      rcv_count(ircv)=nword
      snd_offset(isnd)=i*nword
      rcv_offset(ircv)=i*nword
    end do

    !---------------------------------------------------------------------------
    ! Time the test
    !---------------------------------------------------------------------------
    wt = MPI_Wtime()
    call alltoallv_mpi (snd, snd_count, snd_offset, &
                        rcv, rcv_count, rcv_offset, &
                        MPI_REAL, nword*ntry, mpi_comm_world, &
                        flag, verbose, nsent)
    wt = MPI_Wtime()-wt
    if (mpi_rank==0) then
      kB = ntry*nword*4e-3
      print'(a,l3,2i7,1p,2e10.2,l3)', &
        ' alltoallv: intrinsic, mpi_size, nword, wt, kB =', &
                     flag,      mpi_size, nword, wt, kB
    end if
    deallocate (snd, rcv)
  end do

  deallocate (snd_count, rcv_count, snd_offset, rcv_offset)
END

!*******************************************************************************
PROGRAM test
  USE mpi_mod
  implicit none
  integer i

!-------------------------------------------------------------------------------
!  Start up MPI and get number of nodes and our node (mpi_rank)
!-------------------------------------------------------------------------------
  call MPI_INIT (mpi_err)
  call MPI_COMM_SIZE (mpi_comm_world, mpi_size, mpi_err)
  call MPI_COMM_RANK (mpi_comm_world, mpi_rank, mpi_err)

  do i=0,2 
    if (mpi_rank==0) then
      if (i==0) then
        print*,'Using MPI_Alltoall[v]'
      else if (i==1) then
        print*,'Using Mpi_Issend/Irecv'
      else
        print*,'Using Mpi_Isend/Irecv'
      end if
    end if
    if (i==0) call test_alltoall(i)
    call test_alltoallv(i)
  end do

  call MPI_Finalize (mpi_err)
END PROGRAM
