#!/bin/csh

setenv log TESTS/speed_${MACHTYPE}_$HOST.log
csh << 'EOF1' >> $log

echo ""
echo "========== `date` $log =========="
uname -a

unsetenv EXPERIMENT
make clean
make PDE=pde_call MHD=mhd_call
mv pde.x pde_call.x
make PDE=pde

'EOF1'


set tmp = TESTS/speed_${MACHTYPE}_$HOST.tmp
csh << 'EOF2' > $tmp

foreach x (pde pde_call)
  foreach f (speed log nopassive lin noshock noforce)
    echo "------ kz/s: $x.x TESTS/$f.in ---------"
    ./$x.x < TESTS/$f.in
  end
end

'EOF2'

grep kz $tmp >> $log
