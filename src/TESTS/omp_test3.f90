! $Id: omp_test3.f90,v 1.2 2005/01/05 13:47:18 aake Exp $
!-----------------------------------------------------------------------
!  Test if threadprivate works, on common blocks and variables (non-std)
!-----------------------------------------------------------------------
module g
  implicit none
  integer m, n
  common /c/m, n
!$omp threadprivate(/c/)
  integer n1, m1
!$omp threadprivate(n1,m1)
end module

!-----------------------------------------------------------------------
program test
  use g
  call init_omp
  call omp
end

!-----------------------------------------------------------------------
subroutine init_omp
  use g
  integer omp_get_thread_num, omp_get_max_threads
  print *,'---- set ----'
!$omp parallel
  m = omp_get_thread_num()
  n = omp_get_max_threads()
  n1 = n
  m1 = m
  print *,m,m1,n,n1
!$omp end parallel
end

!-----------------------------------------------------------------------
subroutine omp
  use g
  print *,'---- use ----'
!$omp parallel
  print *,m,m1,n,n1
!$omp end parallel
end
