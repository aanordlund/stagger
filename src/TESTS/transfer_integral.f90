!***********************************************************************
SUBROUTINE transfer1 (np,n1,dtau,s,q,xi,doxi)
!
  USE params
  implicit none
!
  real, dimension(mx,my,mz) :: dtau,s,q
  real, dimension(mx,mz)    :: xi
  real, dimension(mx,my)    :: ex0, ex1, ex2, ds1dtau, dsdtau, d2sdtau2
  real, dimension(mx,my)    :: qplus, qminus
  real, dimension(mx)       :: ex
  real :: taum, dtau0, dtau1, dtau2, ctau, small
  integer :: ix, iy, iz, np, n1, irep, nrep
  real  cpu0, cpu1, cpu2, fdtime
  save cpu1, cpu2
  data cpu1/0./, cpu2/0./
  logical doxi
!
  character(len=80):: id='$Id: transfer_integral.f90,v 1.1 2005/08/06 16:47:50 aake Exp $'
!
!  Choose method
!
  if (cpu1.eq.0.0) then
    iz = 1
    cpu0 = fdtime()
    nrep=0
    do while (cpu1 < 1.)
     nrep=nrep+1
     do iy=2,np
      do ix=1,mx
        ds1dtau(ix,iy) = (s(ix,iy,iz)-s(ix,iy-1,1))/dtau(ix,iy,iz)
        small = 1./(1+dtau(ix,iy,iz)**2)
        small = small*small
        small = small*small
        ex0(ix,iy) = exp(-dtau(ix,iy,iz))
        ex1(ix,iy) = small*dtau(ix,iy,iz)+(1.-small)*(1.-ex0(ix,iy))
        ex2(ix,iy) = ex1(ix,iy)-dtau(ix,iy,iz)*ex0(ix,iy)
      end do
     end do
     do ix=1,mx
      ex1(ix, 1) = dtau(ix, 1,1)
      ex0(ix, 1) = 1.-ex1(ix, 1)
      ex2(ix, 1) = ex1(ix, 1)-dtau(ix, 1,1)*ex0(ix, 1)
     end do
     cpu1 = cpu1+fdtime()
    end do
    cpu1 = fdtime()
    do irep=1,nrep
     do iy=2,np
      do ix=1,mx
        ds1dtau(ix,iy) = (s(ix,iy,iz)-s(ix,iy-1,1))/dtau(ix,iy,iz)
        small = 1./(1+dtau(ix,iy,iz)**2)
        small = small*small
        small = small*small
        ex0(ix,iy) = exp(-dtau(ix,iy,iz))
        ex1(ix,iy) = small*dtau(ix,iy,iz)+(1.-small)*(1.-ex0(ix,iy))
        ex2(ix,iy) = ex1(ix,iy)-dtau(ix,iy,iz)*ex0(ix,iy)
      end do
     end do
     do ix=1,mx
      ex1(ix, 1) = dtau(ix, 1,1)
      ex0(ix, 1) = 1.-ex1(ix, 1)
      ex2(ix, 1) = ex1(ix, 1)-dtau(ix, 1,1)*ex0(ix, 1)
     end do
    end do
    cpu1 = fdtime()
    dtau2 = 15.
    dtau1 = 0.05
    dtau0 = 0.005
    do irep=1,nrep
     do iy=2,np
      do ix=1,mx
        ds1dtau(ix,iy) = (s(ix,iy,iz)-s(ix,iy-1,1))/dtau(ix,iy,iz)
        if      (dtau(ix,iy,iz) .gt. dtau2) then
          ex0(ix,iy) = 0.
          ex1(ix,iy) = 1.
        else if (dtau(ix,iy,iz) .gt. dtau1) then
          ex0(ix,iy) = exp(-dtau(ix,iy,iz))
          ex1(ix,iy) = 1.-ex0(ix,iy)
        else if (dtau(ix,iy,iz) .gt. dtau0) then
          ex1(ix,iy) = dtau(ix,iy,iz)*(1.-0.5*dtau(ix,iy,iz)*(1.-0.333333*dtau(ix,iy,iz)))
          ex0(ix,iy) = 1.-ex1(ix,iy)
        else
          ex1(ix,iy) = dtau(ix,iy,iz)*(1.-0.5*dtau(ix,iy,iz))
          ex0(ix,iy) = 1.-ex1(ix,iy)
        end if
        ex2(ix,iy) = ex1(ix,iy)-dtau(ix,iy,iz)*ex0(ix,iy)
      end do
     end do
     do ix=1,mx
      if      (dtau(ix,1,1) .gt. dtau2) then
        ex0(ix,1) = 0.
        ex1(ix,1) = 1.
      else if (dtau(ix,1,1) .gt. dtau1) then
        ex0(ix,1) = exp(-dtau(ix,1,1))
        ex1(ix,1) = 1.-ex0(ix,1)
      else if (dtau(ix,1,1) .gt. dtau0) then
        ex1(ix,1) = dtau(ix,1,1)*(1.-0.5*dtau(ix,1,1)*(1.-0.333333*dtau(ix,1,1)))
        ex0(ix,1) = 1.-ex1(ix,1)
      else
        ex1(ix,1) = dtau(ix,1,1)*(1.-0.5*dtau(ix,1,iz))
        ex0(ix,1) = 1.-ex1(ix,1)
      end if
      ex2(ix,1) = ex1(ix,1)-dtau(ix,1,iz)*ex0(ix,1)
     end do
    end do
    cpu2 = fdtime()
    print*,'radiation_integral: times =',cpu1,cpu2
    if (cpu1.lt.cpu2) then
      print*,'radiation_integral: choosing vector method'
    else
      print*,'radiation_integral: choosing if method'
    end if
  end if
!
!  Calculate 1-exp(-tau(l,1,n)) straight or by series expansion,
!  based on max tau(l,1,n).
!
!$omp parallel do private(iz,ix,iy,taum,ex,dtau0,dtau1,dtau2,small,ex0,ex1,ex2, &
!$omp   ctau,ds1dtau,dsdtau,d2sdtau2,qminus,qplus)
  do iz=1,mz
    taum=maxval(dtau(:,1,iz))
    if (taum.gt.0.1) then
      ex(:)=1.-exp(-dtau(:,1,iz))
    else
      ex(:)=dtau(:,1,iz)*(1.-.5*dtau(:,1,iz)*(1.-.3333*dtau(:,1,iz)))
    endif
!
!  Compute factors (4a+3m+1d)
!
    if (cpu1.lt.cpu2) then
      do iy=2,np
        do ix=1,mx
          ds1dtau(ix,iy) = (s(ix,iy,iz)-s(ix,iy-1,iz))/dtau(ix,iy,iz)
          small = 1./(1+dtau(ix,iy,iz)**2)
          small = small*small
          small = small*small
          ex0(ix,iy) = exp(-dtau(ix,iy,iz))
          ex1(ix,iy) = small*(dtau(ix,iy,iz)*(1.-0.5*dtau(ix,iy,iz)))+(1.-small)*(1.-ex0(ix,iy))
          ex2(ix,iy) = ex1(ix,iy)-dtau(ix,iy,iz)*ex0(ix,iy)
        end do
      end do
      do ix=1,mx
        ex1(ix, 1) = dtau(ix, 1,iz)
        ex0(ix, 1) = 1.-ex1(ix, 1)
        ex2(ix, 1) = ex1(ix, 1)-dtau(ix, 1,iz)*ex0(ix, 1)
      end do
    else
      dtau2 = 15.
      dtau1 = 0.05
      dtau0 = 0.005
      do iy=2,np
        do ix=1,mx
          ds1dtau(ix,iy) = (s(ix,iy,iz)-s(ix,iy-1,iz))/dtau(ix,iy,iz)
          if      (dtau(ix,iy,iz) .gt. dtau2) then
            ex0(ix,iy) = 0.
            ex1(ix,iy) = 1.
          else if (dtau(ix,iy,iz) .gt. dtau1) then
            ex0(ix,iy) = exp(-dtau(ix,iy,iz))
            ex1(ix,iy) = 1.-ex0(ix,iy)
          else if (dtau(ix,iy,iz) .gt. dtau0) then
            ex1(ix,iy) = dtau(ix,iy,iz)*(1.-0.5*dtau(ix,iy,iz)*(1.-0.333333*dtau(ix,iy,iz)))
            ex0(ix,iy) = 1.-ex1(ix,iy)
          else
            ex1(ix,iy) = dtau(ix,iy,iz)*(1.-0.5*dtau(ix,iy,iz))
            ex0(ix,iy) = 1.-ex1(ix,iy)
          end if
          ex2(ix,iy) = ex1(ix,iy)-dtau(ix,iy,iz)*ex0(ix,iy)
        end do
      end do
      do ix=1,mx
        if      (dtau(ix,1,iz) .gt. dtau2) then
          ex0(ix,1) = 0.
          ex1(ix,1) = 1.
          ex2(ix,1) = 1.
        else if (dtau(ix,1,iz) .gt. dtau1) then
          ex0(ix,1) = exp(-dtau(ix,1,iz))
          ex1(ix,1) = 1.-ex0(ix,1)
          ex2(ix,1) = ex1(ix,1)-dtau(ix,1,iz)*ex0(ix,1)
        else if (dtau(ix,1,iz) .gt. dtau0) then
          ex1(ix,1) = dtau(ix,1,iz)*(1.-0.5*dtau(ix,1,iz)*(1.-0.333333*dtau(ix,1,iz)))
          ex0(ix,1) = 1.-ex1(ix,1)
          ex2(ix,1) = ex1(ix,1)-dtau(ix,1,iz)*ex0(ix,1)
        else
          ex1(ix,1) = dtau(ix,1,iz)*(1.-0.5*dtau(ix,1,iz))
          ex0(ix,1) = 1.-ex1(ix,1)
          ex2(ix,1) = ex1(ix,1)-dtau(ix,1,iz)*ex0(ix,1)
        end if
      end do
    end if
!
!  dS/dtau and d2S/dtau2 (5m+3a+1d)
!
    do iy=2,np-1
      do ix=1,mx
        ctau = 1./(dtau(ix,iy+1,iz)+dtau(ix,iy,iz))
        dsdtau(ix,iy) = (ds1dtau(ix,iy+1)*dtau(ix,iy,iz)+ds1dtau(ix,iy)*dtau(ix,iy+1,iz))*ctau
        d2sdtau2(ix,iy) = (ds1dtau(ix,iy+1)-ds1dtau(ix,iy))*2.*ctau
      end do
    end do
    dsdtau(:,1) = 0.
    d2sdtau2(:,1) = 0.
    dsdtau(:,np) = dsdtau(:,np-1)+dtau(:,np,iz)*d2sdtau2(:,np-1)
    d2sdtau2(:,np) = d2sdtau2(:,np-1)
!
!  Incoming rays (2a+3m)
!
    qminus(:,1) = s(:,1,iz)*ex(:)-s(:,1,iz)
    do iy=2,np
      qminus(:,iy) = qminus(:,iy-1)*ex0(:,iy)-dsdtau(:,iy)*ex1(:,iy)+d2sdtau2(:,iy)*ex2(:,iy)
    end do
!
!  Outgoing rays (2a+3m)
!
    qplus(:,np) = dsdtau(:,np)+d2sdtau2(:,np)
    do iy=np-1,1,-1
      qplus(:,iy) = qplus(:,iy+1)*ex0(:,iy+1)+dsdtau(:,iy)*ex1(:,iy+1)+d2sdtau2(:,iy)*ex2(:,iy+1)
      q(:,iy,iz) = 0.5*(qplus(:,iy)+qminus(:,iy))
    end do
    q(:,np,iz) = 0.5*(qplus(:,np)+qminus(:,np))
!
!  Surface intensity.
!
    if (doxi) then
      xi(:,iz)=(1.-ex(:))*(q(:,1,iz)+s(:,1,iz))+s(:,1,iz)*0.5*ex(:)**2
    end if

  end do
!
END
