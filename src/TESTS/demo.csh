#!/bin/csh

cd $PBS_O_WORKDIR; umask 026; unlimit stack; limit core 0; limit

#make clean
/bin/rm FORCING/*.o
make EXPERIMENT=turbulence PDE=pde_call

,/pde_call.x < TESTS/demo.in > TESTS/demo.log
