#!/bin/csh

$(MAKE) FOPT=-O1 PAR= EXPERIMENT=padoan clean
$(MAKE) FOPT=-O1 PAR= EXPERIMENT=padoan
$(MAKE) FOPT=-O1 PAR= EXPERIMENT=padoan-mesh

./padoan.x << EOF
cat << EOF
&io      iread=-1 nstep=20 nsnap=20 file="TESTS/mesh-test.dat" /
&run     Cdt=0.45 iseed=-77 /
&vars    do_pscalar=f do_energy=f do_mhd=t /
&grid    mx=15, my=15, mz=15, sx=1, sy=1, sz=1 /
&pde     do_loginterp=t do_2nddiv=t nu1=0.1 nu2=0.6 nur=0.2 gamma=1. /
&eqofst  /
&force   do_force=t do_helmh=f ampl_turb=8 t_turn=.0625 k1=1 k2=2 pk=1.833 /
&selfg   grav=0. /
&init    b0=8 do_test=f /
&wave    /
&bdry    /
&expl    /
&cool    /
EOF

./padoan.x << EOF
&io      iread=1 nstep=1 nsnap=1 file="TESTS/mesh-test.dat" /
&run     Cdt=0.45 dt=0.01 iseed=-77 /
&vars    do_pscalar=f do_energy=f do_mhd=t /
&grid    mx=15, my=15, mz=15, sx=1, sy=1, sz=1 /
&pde     do_loginterp=t do_2nddiv=t nu1=0.1 nu2=0.6 nur=0.2 gamma=1. /
&eqofst  /
&force   do_force=t do_helmh=f ampl_turb=8 t_turn=.0625 k1=1 k2=2 pk=1.833 /
&selfg   grav=0. /
&init    b0=8 do_test=f /
&wave    /
&bdry    /
&expl    /
&cool    /
EOF

./padoan-mesh.x << EOF
&io      iread=1 iwrite=3 nstep=1 nsnap=1 file="TESTS/mesh-test.dat" /
&run     Cdt=0.45 dt=0.01 iseed=-77 /
&vars    do_pscalar=f do_energy=f do_mhd=t /
&grid    mx=15, my=15, mz=15, sx=1, sy=1, sz=1 /
&pde     do_loginterp=t do_2nddiv=t nu1=0.1 nu2=0.6 nur=0.2 gamma=1. /
&eqofst  /
&force   do_force=t do_helmh=f ampl_turb=8 t_turn=.0625 k1=1 k2=2 pk=1.833 /
&selfg   grav=0. /
&init    b0=8 do_test=f /
&wave    /
&bdry    /
&expl    /
&cool    /
EOF

idl << EOF
open,a,'TESTS/mesh-test.dat'
for i=0,6 do stat,a(i+7*2)-a(i+7*4)
EOF
