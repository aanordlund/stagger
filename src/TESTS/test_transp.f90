
 USE params

 implicit none
 real, allocatable, dimension(:,:,:):: s, tau, q1, q2, dtau
 real, allocatable, dimension(:,:):: xi
 integer :: n1, iy, nrep
 real :: logtaumin, logtaumax, dlogtau, logtau, cput(2), cpu(2), dtime, ampl
 logical doxi

 nrep=50
 mx=100
 mz=mx
 my=33
 doxi=.true.
 ampl=0.

 print *,'ampl ?'
 read *,ampl

 allocate (s(mx,my,mz), tau(mx,my,mz), q1(mx,my,mz), q2(mx,my,mz), xi(mx,mz), dtau(mx,my,mz))

 s=1.
 logtaumin=-4.
 logtaumax=4.
 dlogtau=(logtaumax-logtaumin)/(my-1)
 do iy=1,my
   logtau=logtaumin+dlogtau*(iy-1)
   tau(:,iy,:)=10.**logtau
   S(:,iy,:)=tau(:,iy,:)+ampl*tau(:,iy,:)**2*(1.+cos(logtau*pi))
 end do
 dtau(:,1,:)=tau(:,1,:)
 dtau(:,2:my,:)=tau(:,2:my,:)-tau(:,1:my-1,:)

 n1 = my/2
 !n1 = 1
 !n1 = my
 cput=dtime(cpu)
 do iy=1,nrep
   call radiation (my,n1,dtau,s,q1,xi,doxi)
 end do
 cput(1)=dtime(cpu)

 print *,' iy  log(tau)         S          Q'
 do iy=1,my
   print '(i4,4e12.4)',iy, alog10(tau(1,iy,1)), s(1,iy,1), q1(1,iy,1)
 end do
 print *,'cpu=',cput
 print *,'mus/pt=',cput*1e6/(mx*my*mz*real(nrep))

 open (1,file='test_transp.dat',form='unformatted')
 write (1) my
 write (1) (tau(1,iy,1), s(1,iy,1), q1(1,iy,1), iy=1,my)
 close (1)

 END

