! $Id: omp_test6.f90,v 1.1 2006/04/18 20:17:34 aake Exp $
!-----------------------------------------------------------------------
!  Test calling a subroutine from both inside and outside a
!  parallel region
!-----------------------------------------------------------------------
MODULE g
  integer m,n,izs,ize,omp_mythread,omp_nthreads
  logical omp_master
  common /c/m,n,izs,ize,omp_mythread,omp_nthreads,omp_master
!$omp threadprivate(/c/)
END MODULE


  USE g
  logical omp_in_parallel

  n=4
  call init_omp(n)

  print *,'----------------------- in PR --------------------------------'
  print *,'0:', omp_in_parallel()
!$omp parallel
  print *,'1:', omp_in_parallel()
  call sub (n)
!$omp barrier
  print *,'2:', omp_in_parallel()
!$omp end parallel
  print *,'---------------------- outside PR ----------------------------'
  print *,'3:', omp_in_parallel()
  call sub (n)
  end

!-----------------------------------------------------------------------
  subroutine sub (n)
  USE g
  logical omp_in_parallel, oip
  integer n, omp_get_thread_num

  oip = omp_in_parallel()
  !$omp parallel if (.not. oip)
  do k=izs,ize
    print *,omp_mythread,omp_get_thread_num(),k
  end do
  !$omp end parallel
  end

!***********************************************************************
SUBROUTINE init_omp(mz)
  USE g
  implicit none
  integer mz
  integer omp_get_num_threads, omp_get_thread_num
!$omp parallel
#if defined (_OPENMP)
  omp_mythread = omp_get_thread_num()
  omp_nthreads = omp_get_num_threads()
#else
  omp_mythread = 0
  omp_nthreads = 1
#endif
  omp_master = omp_mythread .eq. 0
  izs = 1 + ((omp_mythread  )*mz)/omp_nthreads
  ize =     ((omp_mythread+1)*mz)/omp_nthreads
  print *,'init_omp:', omp_mythread, izs, ize
!$omp end parallel
END SUBROUTINE
