! $Id: mpi.f90,v 1.10 2014/08/31 19:28:17 aake Exp $
!*******************************************************************************
! Mock-up for Stagger Code parameters needed for compilation in stand-alone tests
!-------------------------------------------------------------------------------
MODULE params
  integer:: mx,my,mz,mw,mxtot,mytot,mztot,mpi_nx,mpi_ny,mpi_nz,mpi_x,mpi_y,mpi_z
  integer:: mpi_rank,mpi_size,ixoff,iyoff,izoff,lb,ub,n_barrier
  integer:: mxrem,myrem,mzrem
  integer:: omp_nthreads,omp_mythread,iys,iye,izs,ize,omp_nz,isubstep,idbg
  logical:: master, mpi_master,omp_master,check_barrier, do_trace, do_logcheck
  integer:: stdin=10, stdout=11
  real:: sx,sy,sz,dx,dy,dz,pi
  real, save:: maxram    = 0.8
  real, allocatable, dimension(:):: xm,ym,zm
  real, allocatable, dimension(:,:,:):: scratch
  integer, parameter :: mfile=80, mid=120
  character(len=mfile):: file
  integer, parameter:: real_size=4
  integer, save:: dbg_level=0
  integer:: dbg_select, dbg_barrier
  integer, parameter:: dbg_mpi     = 1
  integer, parameter:: dbg_omp     = 2
  integer, parameter:: dbg_io      = 4
  integer, parameter:: dbg_hd      = 8
  integer, parameter:: dbg_mhd     = 16
  integer, parameter:: dbg_stagger = 32
  integer, parameter:: dbg_force   = 64
  integer, parameter:: dbg_expl    = 128
  integer, parameter:: dbg_cool    = 256
  integer, parameter:: dbg_rad     = 512
  integer, parameter:: dbg_eos     = 1024
  integer, parameter:: dbg_pscal   = 2048
END MODULE params

!*******************************************************************************
MODULE mpi_mod
  implicit none
  include 'mpif.h'
  integer               :: new_com, old_com, mpi_err, my_size, my_rank, mpi_com
  integer               :: mpi_xup, mpi_xdn, mpi_yup, mpi_ydn, mpi_zdn, mpi_zup
  integer, dimension(3) :: mpi_coords, mpi_dims, mpi_comm_plane, mpi_comm_beam, &
                           mpi_key_beam, mpi_color_beam
  logical               :: mpi_reorder, mpi_periodic(3)
  integer               :: ix0, ix1, iy0, iy1, iz0, iz1
  integer(kind=8)       :: mpi_sends, mpi_sent
  integer               :: handle, filetype
  integer, save         :: verbose=0
  real, allocatable, dimension(:,:,:):: send, recv
  real, allocatable, dimension(:,:):: buffer
END MODULE mpi_mod

!*******************************************************************************
SUBROUTINE init_mpi
  USE mpi_mod
  USE params
  implicit none
  character(len=mid):: id='$Id: mpi.f90,v 1.10 2014/08/31 19:28:17 aake Exp $'

!-------------------------------------------------------------------------------
!  Start up MPI and get number of nodes and our node rank
!-------------------------------------------------------------------------------
  call MPI_INIT (mpi_err)
  mpi_com = mpi_comm_world
  call MPI_COMM_SIZE (mpi_comm_world, mpi_size, mpi_err)
  call MPI_COMM_RANK (mpi_comm_world, mpi_rank, mpi_err)
  my_size = mpi_size
  my_rank = mpi_rank
  mpi_master = (mpi_rank == 0)
  master = mpi_master
  call print_id (id)
END SUBROUTINE init_mpi

!*******************************************************************************
SUBROUTINE init_geom_mpi
  USE mpi_mod
  USE params
  character(len=mfile) fmt
  logical debug, debug2
  integer i

!-------------------------------------------------------------------------------
!  Compute MPI dimensions
!-------------------------------------------------------------------------------
  if (mpi_nx==0) mpi_nx = max(1,mpi_size/(mpi_nz*mpi_ny))                       ! let the x-dir take the rest
  if (mpi_nz==0) mpi_nz = max(1,mpi_size/(mpi_nx*mpi_ny))                       ! let the z-dir take the rest
  if (mpi_nx*mpi_ny*mpi_nz /= mpi_size &
    .or. mod(mpi_size,mpi_nx) > 0 &
    .or. mod(mpi_size,mpi_ny) > 0 &
    .or. mod(mpi_size,mpi_nz) > 0 ) then
    if (master) then
      fmt='(a,2x,3i8)'
      print *,'====================================================='
      print *,'mpi_size is not compatible with the problem size:'
      print fmt,' MPI:             mpi_size =', mpi_size
      print fmt,' MPI: mpi_nx mpi_ny mpi_nz =', mpi_nx,mpi_ny,mpi_nz
      print fmt,' MPI:     mx     my     mz =', mx,my,mz
      print *,'====================================================='
    end if
    call end_mpi
  end if
  
!-------------------------------------------------------------------------------
! Create New Cartesian Communicators
!-------------------------------------------------------------------------------
  mpi_reorder = .false.
  mpi_periodic = .true.
  mpi_dims(1) = mpi_nx
  mpi_dims(2) = mpi_ny
  mpi_dims(3) = mpi_nz
  CALL MPI_CART_CREATE (mpi_com, 3, mpi_dims, mpi_periodic, mpi_reorder, new_com,  mpi_err)
  CALL MPI_CART_COORDS (new_com, mpi_rank, 3, mpi_coords, mpi_err)
  CALL MPI_CART_SHIFT  (new_com, 0, 1, mpi_xdn, mpi_xup, mpi_err)
  CALL MPI_CART_SHIFT  (new_com, 1, 1, mpi_ydn, mpi_yup, mpi_err)
  CALL MPI_CART_SHIFT  (new_com, 2, 1, mpi_zdn, mpi_zup, mpi_err)
  CALL MPI_COMM_SPLIT (mpi_com, mpi_coords(1), mpi_rank, mpi_comm_plane(1), mpi_err)
  CALL MPI_COMM_SPLIT (mpi_com, mpi_coords(2), mpi_rank, mpi_comm_plane(2), mpi_err)
  CALL MPI_COMM_SPLIT (mpi_com, mpi_coords(3), mpi_rank, mpi_comm_plane(3), mpi_err)
  mpi_x = mpi_coords(1)                                                         ! MPI coordinates
  mpi_y = mpi_coords(2)
  mpi_z = mpi_coords(3)  
  mpi_color_beam(1) = mpi_ny * mpi_z + mpi_y; mpi_key_beam(1)   = mpi_x
  mpi_color_beam(2) = mpi_nz * mpi_x + mpi_z; mpi_key_beam(2)   = mpi_y
  mpi_color_beam(3) = mpi_nx * mpi_y + mpi_x; mpi_key_beam(3)   = mpi_z
  call MPI_COMM_SPLIT(mpi_com, mpi_color_beam(1), mpi_key_beam(1), mpi_comm_beam(1), mpi_err)
  call MPI_COMM_SPLIT(mpi_com, mpi_color_beam(2), mpi_key_beam(2), mpi_comm_beam(2), mpi_err)
  call MPI_COMM_SPLIT(mpi_com, mpi_color_beam(3), mpi_key_beam(3), mpi_comm_beam(3), mpi_err)

!-------------------------------------------------------------------------------
! Pick up MPI related coordinates and sizes
!-------------------------------------------------------------------------------
  mxtot = mx                                                                    ! global dimensions of the grid
  mytot = my
  mztot = mz
  mx = (mxtot-1)/mpi_nx + 1                                                     ! domain sizes
  my = (mytot-1)/mpi_ny + 1
  mz = (mztot-1)/mpi_nz + 1
  mxrem = mx*mpi_nx - mxtot                                                     ! remaining points, if any
  myrem = my*mpi_ny - mytot
  mzrem = mz*mpi_nz - mztot
  ixoff = mpi_x*mx                                                              ! integer coordinate offsets
  iyoff = mpi_y*my
  izoff = mpi_z*mz  
  lb = 1                                                                        ! boundary indices
  ub = my
  if (mpi_y == 0) lb = 5
  if (mpi_y == mpi_ny-1) ub = my-4
 
  if (master) then
    fmt='(a,2x,3i8)'
    print *,'====================================================='
    print fmt,' MPI:  mxtot  mytot  mztot =', mxtot,mytot,mztot
    print fmt,' MPI: mpi_nx mpi_ny mpi_nz =', mpi_nx,mpi_ny,mpi_nz
    print fmt,' MPI:     mx     my     mz =', mx,my,mz
    print *,'====================================================='
    print'(a,1p,e11.3)', ' Total words per array: ',           float(mxtot)*float(mytot)*float(mztot)
    print'(a,    f7.3)', ' log2  words per array: ',      alog(float(mxtot)*float(mytot)*float(mztot))/alog(2.)
    print'(a,1p,e11.3)', ' Total bytes per array: ', real_size*float(mxtot)*float(mytot)*float(mztot)
    print'(a,1p,e11.3)', ' Local bytes per array: ', real_size*float(mx   )*float(my   )*float(mz   )
  endif

  sx = 1.
  sy = 1.
  sz = 1.
  dx = sx/mxtot
  dy = sy/mytot
  dz = sz/mztot
  allocate (xm(mx), ym(my), zm(mz))
  do i=1,mx
    xm(i) = dx*(ixoff+i-1)
  end do
  do i=1,my
    ym(i) = dy*(iyoff+i-1)
  end do
  do i=1,mz
    zm(i) = dz*(izoff+i-1)
  end do
END SUBROUTINE init_geom_mpi

!*******************************************************************************
SUBROUTINE mesh_mpi
END SUBROUTINE mesh_mpi

!*******************************************************************************
SUBROUTINE end_mpi
  USE mpi_mod
  implicit none
!..............................................................................
  call MPI_BARRIER (mpi_comm_world, mpi_err)
  call MPI_FINALIZE (mpi_err)
  call exit
END

!*******************************************************************************
SUBROUTINE print_id (id)
  USE params
  implicit none
  character(len=mid) id
  real wallclock
  logical debug
  character(len=90), save:: &
    hl='------------------------------------------------------------------------------------------'
!..............................................................................
  if (do_trace) then
    if (master) then
      print'(a)',hl
      print'(1x,f9.3,2(2x,i4),2x,a)',wallclock(),dbg_select,id
    end if
    return
  end if

  if (id .ne. ' ') then
    if (master) then
      print'(a)',hl
      print'(1x,a,f12.2)', trim(id), wallclock()
    end if
    id = ' '
  end if
END SUBROUTINE print_id

!*******************************************************************************
SUBROUTINE split_name (ofile)
  USE params
  implicit none
  integer ihead, itail
  character(len=mfile) ofile, head, tail
!..............................................................................
  ihead = index(ofile,'/',back=.true.)      ! index to slash
  ihead = max(ihead,0)
  itail = ihead+1
  tail = ofile(itail:mfile)                 ! file name only
  if (ihead.gt.0) then
    head = file(1:ihead)                    ! dir name
  else
    head = './'                             ! current dir
    ihead = 2
  end if
END SUBROUTINE split_name

!*******************************************************************************
FUNCTION debug (dbg_id)
  USE params
  implicit none
  logical debug
  integer dbg_id
!.......................................................................
  debug = iand(dbg_id,dbg_select) /= 0 .and. dbg_level > 8
END FUNCTION debug

!*******************************************************************************
FUNCTION debug2 (dbg_id, level)
  USE params
  implicit none
  logical debug2
  integer dbg_id, level
!..............................................................................
  debug2 = iand(dbg_id,dbg_select) /= 0 .and. dbg_level >= level
END FUNCTION debug2

!*******************************************************************************
SUBROUTINE print_trace (id, dbg, label)
  USE params
  implicit none
  character(len=*) id
  character(len=*) label
  integer dbg
  real wallclock
  logical debug
!..............................................................................
  if (do_trace .or. debug(dbg)) then
    call barrier_mpi ('print_trace1')
    if (master) print '(1x,f9.3,2x,a,2x,a)',wallclock(),label,id
  end if
END SUBROUTINE print_trace

!*******************************************************************************
SUBROUTINE abort_mpi
  USE params
  USE mpi_mod
  implicit none
!...............................................................................
  call MPI_ABORT (mpi_comm_world, 127, mpi_err)
  call exit
END SUBROUTINE abort_mpi

!*******************************************************************************
SUBROUTINE waitall_mpi (nreq, req)
  USE mpi_mod
  implicit none
  integer nreq, req(nreq)
!...............................................................................
  call MPI_Waitall (nreq, req, MPI_STATUSES_IGNORE, mpi_err)
END SUBROUTINE

!*******************************************************************************
SUBROUTINE wait_mpi (req)
  USE mpi_mod
  implicit none
  integer req
!...............................................................................
  call MPI_Wait (req, MPI_STATUS_IGNORE, mpi_err)
END SUBROUTINE

!*******************************************************************************
REAL FUNCTION wallclock()
  !USE mpi
  implicit none
  include 'mpif.h'
  real(kind=8):: time
  real(kind=8), save:: offset=-1.
!...............................................................................
  time = MPI_Wtime()
  if (offset == -1.) offset=time
  wallclock = time-offset
END FUNCTION wallclock

!*******************************************************************************
SUBROUTINE barrier_mpi (label)
  USE mpi_mod
  implicit none
  character(len=*) label
!...............................................................................
  call MPI_BARRIER (mpi_comm_world, mpi_err)
END SUBROUTINE barrier_mpi

!*******************************************************************************
FUNCTION intarg (i, default)
  USE params
  implicit none
  integer i, intarg, default
  character(len=16) arg
!...............................................................................
  call getarg (i, arg)
  if (trim(arg) == '') then
    intarg = default
  else
    read (arg,*) intarg
  end if
END FUNCTION intarg

!*******************************************************************************
SUBROUTINE send_yx_slice (mx, my, buf3, rank, req)
  USE mpi_mod
  implicit none
  integer mx, my, myx, rank, req, comm, tag, ierr
  real buf3(my, mx)
!...............................................................................
  myx = my*mx
  tag  = 1
  comm = mpi_comm_beam(1)
  call MPI_Isend (buf3, myx, MPI_REAL, rank, tag, comm, req, ierr)
END SUBROUTINE send_yx_slice

!*******************************************************************************
SUBROUTINE recv_yx_slice (mx, my, buf3, rank)
  USE mpi_mod
  implicit none
  integer mx, my, myx, rank, comm, tag, ierr
  integer status(MPI_STATUS_SIZE)
  real buf3(my, mx)
!...............................................................................
  myx = my*mx
  tag  = 1
  comm = mpi_comm_beam(1)
  call MPI_Recv (buf3, myx, MPI_REAL, rank, tag, comm, status, ierr)
END SUBROUTINE recv_yx_slice

!*******************************************************************************
SUBROUTINE send_yz_slice (mz, my, buf3, rank, req)
  USE mpi_mod
  implicit none
  integer mz, my, myz, rank, req, comm, tag, ierr
  real buf3(my, mz)
!...............................................................................
  myz = my*mz
  tag  = 3
  comm = mpi_comm_beam(3)
  call MPI_Isend (buf3, myz, MPI_REAL, rank, tag, comm, req, ierr)
END SUBROUTINE send_yz_slice

!*******************************************************************************
SUBROUTINE recv_yz_slice (mz, my, buf3, rank)
  USE mpi_mod
  implicit none
  integer mz, my, myz, rank, comm, tag, ierr
  integer status(MPI_STATUS_SIZE)
  real buf3(my, mz)
!...............................................................................
  myz = my*mz
  tag  = 3
  comm = mpi_comm_beam(3)
  call MPI_Recv (buf3, myz, MPI_REAL, rank, tag, comm, status, ierr)
END SUBROUTINE recv_yz_slice

!*******************************************************************************
SUBROUTINE message_mpi (comm, txt)
  USE params, only: master, do_trace
  USE mpi_mod
  implicit none
  integer comm, ierr
  character(len=*) txt
!-------------------------------------------------------------------------------
  if (.not. do_trace) return
  print*,my_rank,' has arrived at message barrier'
  call MPI_Barrier (comm, ierr)
  call sleep(1)
  if (master) print *, txt
  call sleep(1)
  call MPI_Barrier (comm, ierr)
END

!***********************************************************************
SUBROUTINE max_real_mpi (a)
  USE mpi_mod
  implicit none
  real:: a, a1
!-----------------------------------------------------------------------
  !$omp barrier
  !$omp master
  call MPI_ALLREDUCE(a,a1,1,MPI_REAL,MPI_MAX,mpi_com,mpi_err)
  a = a1
  !$omp end master
  !$omp barrier
END SUBROUTINE
