  implicit none
  include 'mpif.h'
  integer mpi_err, mpi_rank, i

  call MPI_INIT (mpi_err)
  call MPI_COMM_RANK (mpi_comm_world, mpi_rank, mpi_err)
  do i=1,12
    call use_stack (mpi_rank, 2**i)
    call MPI_BARRIER (mpi_comm_world, mpi_err)
    flush (0)
    flush (1)
    flush (6)
    flush (101)
    flush (102)
  end do
  call MPI_FINALIZE (mpi_err)
END

SUBROUTINE use_stack (mpi_rank, m)
  implicit none
  integer mpi_rank, m
  real a(m,m,m)

  a(1,1,1) = m
  print *,'OK with stack use = ', real(m)**3*4./1024.**2, mpi_rank
END
