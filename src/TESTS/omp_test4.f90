! $Id: omp_test4.f90,v 1.2 2005/01/07 16:56:49 aake Exp $
!-----------------------------------------------------------------------
!  Test if it possible to write routines that may be called from both
!  inside and outside parallel regions.  This turns out to not be 
!  possible, at least not with the methods tried here.  Even a conditional
!  omp parallel if will disturb the orphan, so it does not recognize the
!  orphan directives.
!-----------------------------------------------------------------------
module g
  implicit none
  integer m, n
  common /c/m, n
!$omp threadprivate(/c/)
end module

!-----------------------------------------------------------------------
program test
  use g
  call init_omp
  print *,'============================ outside parallel region =========================='
  call omp
  call omp1
  print *,'============================ inside parallel region  =========================='
!$omp parallel
  call omp
!$omp barrier
  call omp1
  call omp1_orphan
!$omp end parallel
end

!-----------------------------------------------------------------------
subroutine init_omp
  use g
  integer omp_get_thread_num, omp_get_max_threads
  print *,'---- set ----'
!$omp parallel
  m = omp_get_thread_num()
  n = omp_get_max_threads()
  print *,m,n
!$omp end parallel
end

!-----------------------------------------------------------------------
subroutine omp
  use g
  logical omp_in_parallel
!$omp parallel if (.not. omp_in_parallel())
  Print *,'---- omp ----',m,n
!$omp barrier
!$omp do
  do i=1,n
    print *,'omp:do',i,m,n
  enddo
!$omp master
  print *,'---- omp:master1 ----',m,n
!$omp end master
!$omp single
  print *,'---- omp:single ----',m,n
!$omp end single
!$omp master
  print *,'---- omp:master2 ----',m,n
!$omp end master
!$omp end parallel
end

!-----------------------------------------------------------------------
subroutine omp1
  logical omp_in_parallel
  print *,omp_in_parallel()
!
!  UNCOMMENT THESE, AND IT FAILS!
!
!!$omp parallel if (.not. omp_in_parallel())
  call omp1_orphan
!!$omp end parallel
end

!-----------------------------------------------------------------------
subroutine omp1_orphan
  use g
  logical omp_in_parallel
  Print *,'---- omp1 ----',m,n,omp_in_parallel()
!$omp barrier
!$omp do
  do i=1,n
    print *,'omp1:do',i,m,n
  enddo
!$omp master
  print *,'---- omp1:master1 ----',m,n
!$omp end master
!$omp single
  print *,'---- omp1:single ----',m,n
!$omp end single
!$omp master
  print *,'---- omp1:master2 ----',m,n
!$omp end master
end
