
!*******************************************************************************
PROGRAM test_nodes
  implicit none
#include 'mpif.h'
  integer mpi_size, mpi_rank, mpi_err, rank, ix, iy, iz, it, n_aver, n_min, &
          slow, nslow, cores
  real, allocatable, dimension(:,:,:):: tmp
  integer, parameter:: mx=100, my=100, mz=50
  character(len=120):: cmd
  real(8):: t1

  !-------------------------------------------------------------------------------
  !  Start up MPI, get number of nodes and our node rank
  !-------------------------------------------------------------------------------
  call MPI_INIT (mpi_err)
  call MPI_COMM_SIZE (mpi_comm_world, mpi_size, mpi_err)
  call MPI_COMM_RANK (mpi_comm_world, mpi_rank, mpi_err)
  if (mpi_rank==0) print*,'started'

  !-------------------------------------------------------------------------------
  !  Count number of iterations we can make
  !-------------------------------------------------------------------------------
  allocate (tmp(mx,my,mz))
  t1 = MPI_Wtime() + 5d0
  do while (MPI_Wtime() < t1)
    it=it+1
    do iz=1,mz
    do iy=1,my
    do ix=1,mx
      tmp(ix,iy,iz) = sin(real(ix+iy+iz+it))
    end do
    end do
    end do
  end do
  t1 = MPI_Wtime()
  call MPI_Allreduce (it, n_aver, 1, MPI_INTEGER, MPI_SUM, mpi_comm_world, mpi_err)
  n_aver = n_aver/mpi_size + 1e-30*sum(tmp)
  call MPI_Allreduce (it, n_min, 1, MPI_INTEGER, MPI_MIN, mpi_comm_world, mpi_err)
  t1 = MPI_Wtime()-t1

  !-------------------------------------------------------------------------------
  !  Evaluate
  !-------------------------------------------------------------------------------
  cores=32
  if (it < 0.95*n_aver) then
    print '(i6,a,i4)',mpi_rank,' only did',it
    slow=1
    write(cmd,'(a,i5.5,a)') &
      'mkdir -p cores; (head -100 /proc/cpuinfo /proc/meminfo /proc/loadavg; top -b -n 1) > cores/', &
      mpi_rank,'.log'
    call system(cmd)
  else
    slow=0
  end if
  call MPI_Allreduce (slow, nslow, 1, MPI_INTEGER, MPI_SUM, mpi_comm_world, mpi_err)
  call sleep(1)
  if (mpi_rank==0) then
    print*,'average iterations =', n_aver, sum(tmp)
    print*,nslow,' slow ranks'
    print'(1x,a,2p,f6.1,a)','performance ',real(n_min)/real(n_aver),' %'
  end if

  deallocate (tmp)
  CALL MPI_Finalize (mpi_err)
END PROGRAM
