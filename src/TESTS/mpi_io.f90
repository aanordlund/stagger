! $Id: mpi_io.f90,v 1.6 2014/08/28 00:02:30 aake Exp $
!*******************************************************************************
MODULE mpi_mod
  USE mpi
  implicit none
  !include 'mpif.h'
  integer       :: mx, my, mz, mpi_nx, mpi_ny, mpi_nz, mxtot, mytot, mztot
  integer       :: mpi_size, mpi_rank, ixoff, iyoff, izoff
  integer       :: mpi_err, mpi_com
  integer       :: handle, filetype, my_real, my_integer
  integer, save :: verbose=0, stdin=1
END MODULE mpi_mod

!*******************************************************************************
SUBROUTINE init_mpi
!-------------------------------------------------------------------------------
  USE mpi_mod
  implicit none
  character(len=80):: id='$Id: mpi_io.f90,v 1.6 2014/08/28 00:02:30 aake Exp $'
  character(len=64):: fmt
!-------------------------------------------------------------------------------
!  Start up MPI and get dimensions
!-------------------------------------------------------------------------------
  call MPI_INIT (mpi_err)
  mpi_com = mpi_comm_world
  call MPI_COMM_SIZE (mpi_com, mpi_size, mpi_err)
  call MPI_COMM_RANK (mpi_com, mpi_rank, mpi_err)
  if (mpi_rank==0) print'(1x,a)',id
  mpi_nz = max(1,mpi_size/(mpi_nx*mpi_ny))                                      ! let the z-dir take the rest
  mxtot = mx                                                                    ! global dimensions of the grid, x direction
  mytot = my                                                                    ! global dimensions of the grid, y direction
  mztot = mz                                                                    ! global dimensions of the grid, z direction
  mx    = mxtot / mpi_nx                                                        ! reduce grid for MPI
  my    = mytot / mpi_ny                                                        ! reduce grid for MPI
  mz    = mztot / mpi_nz                                                        ! reduce grid for MPI
  fmt='(a,2x,3i8)'
  if (mpi_rank==0) print fmt,'  mxtot  mytot  mztot =', mxtot,mytot,mztot
  if (mpi_rank==0) print fmt,' mpi_nx mpi_ny mpi_nz =', mpi_nx,mpi_ny,mpi_nz
  if (mpi_rank==0) print fmt,'     mx     my     mz =', mx,my,mz
END SUBROUTINE init_mpi

!*******************************************************************************
SUBROUTINE abort_mpi
  USE mpi_mod
  implicit none
  integer, save:: code=1
!...............................................................................
  call MPI_Abort (mpi_com, code, mpi_err)
END SUBROUTINE abort_mpi

!*******************************************************************************
SUBROUTINE end_mpi
  USE mpi_mod
  implicit none
!...............................................................................
  call MPI_Finalize (mpi_err)
END SUBROUTINE end_mpi

!*******************************************************************************
SUBROUTINE barrier_mpi
  USE mpi_mod
  implicit none
!...............................................................................
  call MPI_Barrier (mpi_com, mpi_err)
END SUBROUTINE barrier_mpi

!*******************************************************************************
SUBROUTINE file_open_mpi (filem, mode)
  USE mpi_mod
  implicit none
  integer mode
  character(len=*) filem
  integer, dimension(3):: size, subsize, starts
!...............................................................................
  CALL MPI_FILE_OPEN (mpi_com, filem, mode, MPI_INFO_NULL, handle, mpi_err)    
  size = (/mxtot, mytot, mztot/)
  subsize = (/mx, my, mz/)
  starts = (/ixoff, iyoff, izoff/)
  CALL MPI_TYPE_CREATE_SUBARRAY (3, size, subsize, starts, MPI_ORDER_FORTRAN, &
                                 MPI_REAL, filetype, mpi_err)   
  CALL MPI_TYPE_COMMIT (filetype, mpi_err)
END SUBROUTINE file_open_mpi

!*******************************************************************************
SUBROUTINE file_openw_mpi (filem)
  USE mpi_mod
  implicit none
  character(len=*) filem
!...............................................................................
  call file_open_mpi (filem, MPI_MODE_CREATE + MPI_MODE_RDWR)
END SUBROUTINE file_openw_mpi

!*******************************************************************************
SUBROUTINE file_openr_mpi (filem)
  USE mpi_mod
  implicit none
  character(len=*) filem
!...............................................................................
  call file_open_mpi (filem, MPI_MODE_RDONLY)
END SUBROUTINE file_openr_mpi

!*******************************************************************************
SUBROUTINE file_write_mpi (f,rec)
  USE mpi_mod
  implicit none
  real(kind=4), dimension(mx,my,mz):: f
  integer rec,count
  integer status(MPI_STATUS_SIZE)
  integer(kind=MPI_OFFSET_KIND) pos
!...............................................................................
  pos = 4*(rec-1)*mxtot
  pos = pos*mytot*mztot
  CALL MPI_FILE_SET_VIEW (handle, pos, MPI_REAL, filetype, 'native', &
                          MPI_INFO_NULL, mpi_err)   
  if (mpi_err .ne. 0) then
    print*,'MPI_FILE_SET_VIEW: rank, mpi_err =',mpi_rank,mpi_err
    call abort_mpi
  endif
  count = mx*my*mz
  CALL MPI_FILE_WRITE_ALL(handle, f , count, MPI_REAL, status, mpi_err)
  if (mpi_err .ne. 0) then
    print*,'MPI_FILE_WRITE_ALL: rank, mpi_err =',mpi_rank,mpi_err
    call exit
  endif
  if (mpi_rank==0 .or. verbose==1) then
    print'(1x,a,2i6,2i12,1p,5e10.2)','mpi_file_write_all:',mpi_rank,rec,count,pos,f(1:3,1,1)
  end if
END SUBROUTINE file_write_mpi

!*******************************************************************************
SUBROUTINE file_read_mpi (f, rec)
  USE mpi_mod
  implicit none
  real(kind=4), dimension(mx,my,mz):: f
  integer rec, count
  integer status(MPI_STATUS_SIZE)
  integer(kind=MPI_OFFSET_KIND) pos
!...............................................................................
  pos = 4*(rec-1)*mxtot
  pos = pos*mytot*mztot
  count = mx*my*mz
  CALL MPI_FILE_SET_VIEW (handle, pos, MPI_REAL, filetype, 'native', &
                          MPI_INFO_NULL, mpi_err)   
  if (mpi_err .ne. 0) then
    print*,'MPI_FILE_SET_VIEW: rank, mpi_err =',mpi_rank,mpi_err
    call abort_mpi
  endif
  CALL MPI_FILE_READ_ALL(handle, f , count, MPI_REAL, status, mpi_err)
  if (mpi_err .ne. 0) then
    print*,'MPI_FILE_READ_ALL: rank, mpi_err =',mpi_rank,mpi_err
    call abort_mpi
  endif
  call MPI_Barrier (mpi_com, mpi_err)
  if (mpi_rank==0 .or. verbose==1) then
    print'(1x,a,2i6,2i12,1p,5e10.2)',' mpi_file_read_all:',mpi_rank,rec,count,pos,f(1:3,1,1)
  end if
END SUBROUTINE file_read_mpi

!*******************************************************************************
SUBROUTINE file_close_mpi
  USE mpi_mod
  implicit none
!...............................................................................
  CALL MPI_FILE_CLOSE(handle, mpi_err)
END SUBROUTINE file_close_mpi

!*******************************************************************************
REAL FUNCTION wallclock()
  !USE mpi
  implicit none
  include 'mpif.h'
  real(kind=8):: time
  real(kind=8), save:: offset=-1.
!...............................................................................
  time = MPI_Wtime()
  if (offset == -1.) offset=time
  wallclock = time-offset
END FUNCTION wallclock

