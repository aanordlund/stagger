
COMPILER = ftn

# Intel mpif90
ifeq ($(COMPILER),gfortran)
  FC = mpif90
  FFLAGS = -g -fbacktrace -x f95-cpp-input
  F7LAGS = -g -fbacktrace -x f77-cpp-input
endif

# Intel mpif90
ifeq ($(COMPILER),intel)
  FC = mpif90
  FFLAGS = -g -traceback -fpp
  F7LAGS = -g -traceback -fpp
endif

# Cray
ifeq ($(COMPILER),ftn)
  FC = ftn
  # -em => .mod files, -J./ => save them here, -Z => #include preprocessing
  FFLAGS =  -emZ -J./
  FFLAGS =  -eZ
endif

LD = $(FC)

# Read optional experiment, OS, compiler, and host specific makefiles
.SUFFIXES:
.SUFFIXES: .f90 .f .o
.f90.o:
	$(FC) $(FFLAGS) -c -o $*.o $<
.f.o:
	$(FC) $(F7LAGS) -c -o $*.o $<

PROGS = read_mpi_io.x write_mpi_io.x test_mpi_io.x
default: $(PROGS)
install: $(PROGS)
	cp -p $(PROGS) $(HOME)/bin/

TEST_MPI_IO = mpi.o mpi_io_aggregate.o test_mpi_io.o
test_mpi_io.x: $(TEST_MPI_IO)
	$(LD) $(TEST_MPI_IO) -o test_mpi_io.x

READ_MPI_IO = mpi.o mpi_io_aggregate.o read_mpi_io.o
read_mpi_io.x: $(READ_MPI_IO)
	$(LD) $(READ_MPI_IO) -o read_mpi_io.x

WRITE_MPI_IO = mpi.o mpi_io_aggregate.o write_mpi_io.o
write_mpi_io.x: $(WRITE_MPI_IO)
	$(LD) $(WRITE_MPI_IO) -o write_mpi_io.x

TEST_FFT2D = mpi.o ../UTILITIES/fft2d_isend.o ../UTILITIES/fftpack.o test_fft2d.o
test_fft2d.x: $(TEST_FFT2D)
	$(LD) $(TEST_FFT2D) -o test_fft2d.x

read_mpi_io.o: mpi.o
write_mpi_io.o: mpi.o
mpi_io_aggregate.o: mpi.o
