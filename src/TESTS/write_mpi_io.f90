!*******************************************************************************
PROGRAM write_mpi_io
  USE params
  USE mpi_mod
  implicit none
  real, allocatable, dimension(:,:,:):: chunk
  integer:: iv, nv, iter, niter, intarg
  character(len=80):: datafile 
  real:: wc, wcw, wallclock, GB, MB
  logical:: exists
  character(len=mid):: id='$Id: write_mpi_io.f90,v 1.4 2014/08/30 23:26:06 aake Exp $'
!...............................................................................
  call init_mpi
  call getarg (1, datafile)
  if (datafile == '') then
    if (master) then
      print*, '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
      print*, ' '
      print*, ' Syntax: aprun -np mpi_size write_mpi_io.x file mx my mx mpi_ny mpi_nz iterations'
      print*, '       file : file to write from (should be striped)'
      print*, '     m[xyz] : array dimensions'
      print*, '     mpi_ny : MPI split in y-direction'
      print*, '     mpi_nz : MPI split in z-direction = aggregation factor'
      print*, ' iterations : number of times to write 9 arrays'
      print*, ' '
      print*, ' Example : aprun -np 4 write_mpi_io.x file 100 100 100 1 4 1'
      print*, ' '
      print*, '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
    end if
    call end_mpi
  end if
  mx     = intarg (2, 100)
  my     = intarg (3, 100)
  mz     = intarg (4, 100)
  mpi_ny = intarg (5, 1)
  mpi_nz = intarg (6, mpi_size)
  niter  = intarg (7, 1)
!-------------------------------------------------------------------------------
  mpi_nx = 0
  call init_geom_mpi
  call mesh_mpi
!-------------------------------------------------------------------------------
  call print_id(id)
  nv = 9
  GB = nv*real(mxtot)*real(mytot)*real(mztot)*4e-9
  MB = 4e-6*mx*my*mz
  if (mpi_rank==0) then
    print*,             '==============================='
    print'(1x,a,i10)'  ,' Number of processes:',mpi_size
    print'(1x,a,i10)'  ,' Number of I/O nodes:',mpi_nx*mpi_ny
    print'(1x,a,i10)'  ,' I/O operations/node:',nv
    print'(1x,a,i10)'  ,'    Repetition count:',niter
    print'(1x,a,i10)'  ,'   Chunks aggregated:',mpi_nz
    print'(1x,a,f10.2)','MB per I/O operation:',mpi_nz*MB
    print'(1x,a,f10.3)','Total file size (GB):',GB
    print*,             '==============================='
  end if
  dbg_select = dbg_mpi
  dbg_level = 1
  allocate (chunk(mx,my,mz))
!-------------------------------------------------------------------------------
  wcw = 0.0
  do iter=1,niter
    call file_openw_mpi ('new.dat')
    wc = wallclock()
    do iv=1,nv
      chunk = iv
      call file_write_mpi (chunk, iv)
    end do
    wc = wallclock()-wc
    wcw = wcw+wc
   call file_close_mpi
    if (mpi_rank==0) print'(1x,a,3f6.2)','write: GB, s, GB/s =',GB,wc,GB/wc
  !-------------------------------------------------------------------------------
    if (mpi_rank==0) then
      inquire (file='old.dat',exist=exists)
      if (.not.exists) call system('mv new.dat old.dat')
    end if
  end do
!-------------------------------------------------------------------------------
  deallocate (chunk)
  if (master) print'(1x,a,2f7.2)', 'average write GB/s =', niter*GB/wcw
  call end_mpi
END
