#!/bin/csh

setenv EXPERIMENT compare
#$MAKE clean
$MAKE -e PDE="pde.o pde_lin.o" MHD="mhd.o"
mv compare.x  pde.x
$MAKE -e PDE="pde_call.o pde_call_lin.o" MHD="mhd_call.o"
mv compare.x  pde_call.x

cd TESTS

../pde_call.x < compare.in
mv test.dat test_call.dat
mv test.dx  test_call.dx
../pde.x < compare.in
idl << EOF
.run compare.pro
EOF

