! $Id: mpi_split.f90,v 1.4 2014/08/14 07:19:00 aake Exp $
!*******************************************************************************
MODULE mpi_mod
  USE mpi
  implicit none
  integer               :: ierr, mpi_size, mpi_rank                             ! standard vars
  integer               :: global_size, global_rank, n_split, group, mpi_com    ! group vars
END MODULE mpi_mod

!*******************************************************************************
  USE mpi_mod
  implicit none
  integer mpi_tmp

!-------------------------------------------------------------------------------
!  Start up MPI and get global number of nodes and ranks
!-------------------------------------------------------------------------------
  call MPI_INIT (ierr)
  call MPI_COMM_SIZE (mpi_comm_world, global_size, ierr)
  call MPI_COMM_RANK (mpi_comm_world, global_rank, ierr)

!-------------------------------------------------------------------------------
!  Split into two groups and compute their size and ranks
!-------------------------------------------------------------------------------
  n_split = 2
  if (global_rank+n_split < global_size) then
    group = 1
  else
    group=2
  end if
  CALL MPI_COMM_SPLIT (mpi_comm_world, group, mpi_rank, mpi_com, ierr)
  call MPI_COMM_SIZE (mpi_com, mpi_size, ierr)
  call MPI_COMM_RANK (mpi_com, mpi_rank, ierr)
  print'(a,5i4)', 'group, mpi_rank, mpi_size, global_rank, global_size =', &
                   group, mpi_rank, mpi_size, global_rank, global_size

!-------------------------------------------------------------------------------
!  Global barrier
!-------------------------------------------------------------------------------
  call MPI_Barrier (mpi_comm_world, ierr)
  if (mpi_rank==0) print '(a,i3)', 'master process in group', group

!-------------------------------------------------------------------------------
!  Group 2 waits longer than group 1, then prints
!-------------------------------------------------------------------------------
  call sleep(group*5)
  call MPI_Barrier (mpi_com, ierr)
  print '(a,2i3)', 'group, rank =', group, mpi_rank

!-------------------------------------------------------------------------------
!  Stop all ranks
!-------------------------------------------------------------------------------
  call MPI_FINALIZE (ierr)
END

