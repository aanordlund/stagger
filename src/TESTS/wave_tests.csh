#!/bin/csh

set d = $cwd
if ($d:t == "TESTS") then
  cd ..
else if ($d:t != "src") then
  echo 'must run from src or TESTS dir'
endif
set src = $cwd
cd TESTS

if (! $?EXPERIMENT)	setenv EXPERIMENT	padoan_cm
if (! $?MP)		setenv MP		_mpi
if (! $?MPIRUN)		setenv MPIRUN		'mpirun -np'
if (! $?NP)		setenv NP		5
set pgm = $src/$EXPERIMENT$MP.x

if (! -d data)	mkdir data

foreach d ( \
 x_sound \
 y_sound \
 z_sound \
 )
  echo $d `date`
  if (! -d data/$d) mkdir data/$d
  sed -e "s/x-alfven/$d/" -e "s/do_mhd=t/do_mhd=f/" wave_tests.in > data/$d/input.txt
  ( cd data/$d; $MPIRUN $NP $pgm > output.txt )
  #( cd data/$d; $MPIRUN $NP $pgm )
end

foreach d ( \
 x_alfven \
 y_alfven \
 z_alfven \
 xy_alfven \
 xy16_alfven \
 x_alfven_stand \
 )
  echo $d `date`
  if (! -d data/$d) mkdir data/$d
  sed -e "s/x-alfven/$d/" -e "s/do_mhd=f/do_mhd=t/" wave_tests.in > data/$d/input.txt
  ( cd data/$d; $MPIRUN $NP $pgm > output.txt )
  #( cd data/$d; $MPIRUN $NP $pgm )
end
