! $Id: omp_test2.f90,v 1.2 2005/01/05 13:44:43 aake Exp $
!-----------------------------------------------------------------------
!  Test calling a subroutine from both inside and outside a
!  parallel region
!-----------------------------------------------------------------------
  real a(32)
  logical omp_in_parallel

  print *,'0:', omp_in_parallel()
!$omp parallel
  print *,'1:', omp_in_parallel()
  call sub (a,32)
  print *,'2:', omp_in_parallel()
!$omp end parallel
  print *,'3:', omp_in_parallel()
  call sub (a,32)
  end

!-----------------------------------------------------------------------
  subroutine sub (a, n)
  logical omp_in_parallel
  integer k, n, omp_get_thread_num
  real a(n)

  print *,'4:', omp_in_parallel()
!$omp parallel if (.not. omp_in_parallel())
  print *,'5:', omp_in_parallel()
!$omp barrier
!$omp master
  print *,'------------------------------------------'
!$omp end master
!$omp do
  do k=1,n
    print *,omp_get_thread_num(),k
    a(k) = k
  end do
!$omp end parallel
  print *,'6:', omp_in_parallel()

  end
