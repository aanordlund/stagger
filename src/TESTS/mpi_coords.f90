! $Id: mpi_coords.f90,v 1.2 2015/10/31 10:30:13 aake Exp $
!*******************************************************************************
MODULE mpi_mod
  USE mpi
  implicit none

  integer               :: new_com, old_com
  integer               :: mpi_xup, mpi_xdn, mpi_yup, mpi_ydn, mpi_zdn, mpi_zup
  integer, dimension(3) :: mpi_coords, mpi_dims, com_plane, com_cart
  logical               :: mpi_reorder, mpi_periodic(3)
  integer               :: ix0, ix1, iy0, iy1, iz0, iz1
  integer               :: mpi_sends, mpi_sent, ierr, cart_rank

  real(kind=8) dbl
  real(kind=8), allocatable:: dbl1(:)
END MODULE mpi_mod

!*******************************************************************************
  USE mpi_mod
  implicit none
  integer iostatus, mpi_size, mpi_rank, mpi_x, mpi_y, mpi_z, rank1, rank2, i
  integer status(MPI_STATUS_SIZE)

!-------------------------------------------------------------------------------
!  Start up MPI and get number of nodes and our node (mpi_rank)
!-------------------------------------------------------------------------------
  call MPI_INIT (ierr)
  call MPI_COMM_SIZE (mpi_comm_world, mpi_size, ierr)
  call MPI_COMM_RANK (mpi_comm_world, mpi_rank, ierr)

  mpi_reorder = .false.
  mpi_periodic = .true.
  mpi_dims(1) = 4
  mpi_dims(2) = 1
  mpi_dims(3) = 2
  CALL MPI_CART_CREATE (mpi_comm_world, 3, mpi_dims, mpi_periodic, mpi_reorder, new_com,  ierr)
  CALL MPI_CART_COORDS (new_com, mpi_rank, 3, mpi_coords, ierr)

  mpi_x = mpi_coords(1)
  mpi_y = mpi_coords(2)
  mpi_z = mpi_coords(3)

  com_cart = -1
  if (mpi_dims(2)*mpi_dims(3) > 1) then
    CALL MPI_COMM_SPLIT (mpi_comm_world, mpi_coords(1), mpi_rank, com_plane(1), ierr)
    CALL MPI_CART_CREATE (com_plane(1), 2, mpi_dims(2:3), mpi_periodic, mpi_reorder, com_cart(1), ierr)
  end if
  if (mpi_dims(3)*mpi_dims(1) > 1) then
    CALL MPI_COMM_SPLIT (mpi_comm_world, mpi_coords(2), mpi_rank, com_plane(2), ierr)
    CALL MPI_CART_CREATE (com_plane(2), 2, mpi_dims(1:3:2), mpi_periodic, mpi_reorder, com_cart(2), ierr)
  end if
  if (mpi_dims(1)*mpi_dims(2) > 1) then
    CALL MPI_COMM_SPLIT (mpi_comm_world, mpi_coords(3), mpi_rank, com_plane(3), ierr)
    CALL MPI_CART_CREATE (com_plane(3), 2, mpi_dims(1:2), mpi_periodic, mpi_reorder, com_cart(3), ierr)
  end if

  !print'(a,i4,2(2x,3i8))','mpi_rank,com_plane,com_cart',mpi_rank,com_plane,com_cart

  do i=1,3
    call MPI_COMM_RANK (com_cart(i), cart_rank, ierr)
    call MPI_CART_COORDS (com_cart(i), cart_rank, 2, mpi_coords, ierr)
    print'(a,2i4,2x,2i4)','cart_rank,i,mpi_coords',cart_rank,i,mpi_coords(1:2)
  end do

  call MPI_FINALIZE (ierr)
END

