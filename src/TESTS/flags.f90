!***********************************************************************
MODULE global
  implicit none
  include 'mpif.h'
  integer mpi_size, mpi_rank, mpi_err
  logical master
  real a,b
END MODULE

!***********************************************************************
SUBROUTINE init_mpi
  USE global
!-----------------------------------------------------------------------
  call MPI_INIT (mpi_err)
  call MPI_COMM_SIZE (mpi_comm_world, mpi_size, mpi_err)
  call MPI_COMM_RANK (mpi_comm_world, mpi_rank, mpi_err)
  master = mpi_rank==0
END SUBROUTINE

!***********************************************************************
SUBROUTINE end_mpi
  USE global
!-----------------------------------------------------------------------
  call MPI_FINALIZE (mpi_err)
END SUBROUTINE

!***********************************************************************
LOGICAL FUNCTION flag(file)
  USE global
  implicit none
  character(len=*) file
!-----------------------------------------------------------------------
  if (master) then
    inquire (file=file//'.flag',exist=flag)
    if (flag) then
      open (30,file=file//'.flag',status='old')
      close (30,status='delete')
    end if
  end if
  call MPI_BCAST (flag,1,MPI_LOGICAL,0,mpi_comm_world,mpi_err)
END FUNCTION

!***********************************************************************
SUBROUTINE read_params
  USE global
  implicit none
  namelist /in/a,b
!-----------------------------------------------------------------------
  open (30,file='input.txt',status='old')
  read (30,in)
  if (master) write(*,in)
  close (30)
END SUBROUTINE

!***********************************************************************
PROGRAM test_flags
  USE global
  logical,save:: do_dump=.false.
  logical flag
!-----------------------------------------------------------------------
  call init_mpi
  call read_params 

  do while(.not.flag('stop'))
    if (flag('reread')) then                                              ! reread parameters
      call read_params 
    end if

    if (flag('dump')) then                                                ! dump?
      do_dump = .true.
      if (master) print*,'dumping turned on for one timestep'
    end if

    ! ... code ...

    do_dump = .false.
  end do

  call end_mpi
END PROGRAM
