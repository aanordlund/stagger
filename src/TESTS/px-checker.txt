
 &io      iread=-1 iwrite=0 nstep=60 nsnap=1 nscr=500 file='TRIAL/snapshot.dat' /
 &run     Cdt=0.10 dt=0.2 idbg=0 do_trace=.false. /
 &vars    do_pscalar=.false. do_energy=.true. do_mhd=.false. /
 &grid    mx=16, my=16, mz=16, sx=16, sy=16, sz=16 /
 &pde     do_dissipation=.false. do_loginterp=.true. do_2nddiv=.false.
          nu1=0.01 nu2=0.00 nu3=0.01 nu4=1. nur=0. gamma=1.66667 /
 &slice   /
 &quench  do_quench=.false. qmax=8 qlim=2 /
 &eqofst  /
 &part    /
 &force   do_force=.false. /
 &selfg   /
 &init    do_init=.false. do_test=.true. /
 &mhd     /
 &wave    type='px-checker' ampl=0.1 e0=0. /
 &bdry    /
 &expl    /
 &cool    do_cool=.false. /

