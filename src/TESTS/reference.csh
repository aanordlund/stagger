#!/bin/csh

setenv EXPERIMENT compare
#$MAKE clean
$MAKE -e
mv compare.x reference.x

cd TESTS

../reference.x < compare.in
mv compare.dat reference.dat
mv compare.tim reference.tim
mv compare.dx  reference.dx

