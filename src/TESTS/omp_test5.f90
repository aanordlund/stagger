! $Id: omp_test5.f90,v 1.1 2005/06/04 19:07:03 aake Exp $
!-----------------------------------------------------------------------
!  Test calling a subroutine from both inside and outside a
!  parallel region
!-----------------------------------------------------------------------
MODULE g
  integer m,n,izs,ize,omp_mythread,omp_nthreads
  logical omp_master
  common /c/m,n,izs,ize,omp_mythread,omp_nthreads,omp_master
!$omp threadprivate(/c/)
END MODULE


  USE g
  logical omp_in_parallel

  n=4
  call init_omp(n)

  print *,'--------------------------------------------------------------'
  print *,'0:', omp_in_parallel()
!$omp parallel
  print *,'1:', omp_in_parallel()
  call sub (n)
  print *,'2:', omp_in_parallel()
!$omp end parallel
  print *,'--------------------------------------------------------------'
  print *,'3:', omp_in_parallel()
  call sub (n)
  end

!-----------------------------------------------------------------------
  subroutine sub (n)
  USE g
  logical omp_in_parallel
  integer n, omp_get_thread_num

  if (omp_in_parallel()) then
     call sub_omp (n)
  else
!$omp parallel
     call sub_omp (n)
!$omp end parallel
  end if
  print *,'6:', omp_in_parallel(), omp_get_thread_num()

  end

!-----------------------------------------------------------------------
  subroutine sub_omp (n)
  USE g
  logical omp_in_parallel
  integer k, n, omp_get_thread_num

  print *,'4:', omp_in_parallel(), omp_get_thread_num()
  do k=izs,ize
    print *,omp_mythread,omp_get_thread_num(),k
  end do
  end

!***********************************************************************
SUBROUTINE init_omp(mz)
  USE g
  implicit none
  integer mz
  integer omp_get_num_threads, omp_get_thread_num
!$omp parallel
#if defined (_OPENMP)
  omp_mythread = omp_get_thread_num()
  omp_nthreads = omp_get_num_threads()
#else
  omp_mythread = 0
  omp_nthreads = 1
#endif
  omp_master = omp_mythread .eq. 0
  izs = 1 + ((omp_mythread  )*mz)/omp_nthreads
  ize =     ((omp_mythread+1)*mz)/omp_nthreads
  print *,'init_omp:', omp_mythread, izs, ize
!$omp end parallel
END SUBROUTINE
