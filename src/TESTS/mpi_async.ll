#!/bin/sh
# @ error            = mpi_async.log
# @ output           = mpi_async.log
# @ job_name         = mpi_async
# @ job_type         = parallel
# @ class            = parallel
# @ node_usage       = not_shared
# @ notification     = never
# @ wall_clock_limit = 0:10:00
# @ node             = 2
# @ tasks_per_node   = 4
# @ queue

mpirun_ssh -np 8 $LOADL_PROCESSOR_LIST ./a.out
