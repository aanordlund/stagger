#! /bin/csh
set echo
mkdir -p test_striping
cd test_striping

set n = $1

setenv MPICH_MPIIO_HINTS_DISPLAY
setenv MPICH_MPIIO_STATS

setenv MPICH_MPIIO_HINTS "old.dat,new.dat:romio_no_indep_rw=true"
foreach c ( 10 20 40 80 )
  mkdir -p $c
  cd $c
  foreach s ( 1m 2m 4m 8m )
    if (! -d $s) then
      mkdir $s
      lfs setstripe -c $c -s $s $s
    endif
    cd $s
    echo ""
    echo "========= count: $c size: $s ================"
    mpiexec -n $n test_mpi_io.x 2000 612 2000 20 2 3
    unsetenv MPICH_MPIIO_HINTS_DISPLAY
    unsetenv MPICH_MPIIO_STATS
    cd ..
  end
  cd ..
end

