close,1,2
dir='./'
open,a,dir+'compare.dat',nv=nv,nt=nt
open,b,dir+'reference.dat',nv=nv,nt=nt
ok=1
for t=0,nt-1 do begin
  for i=0,nv-1 do begin
    err=rms(a(6*t+i)-b(6*t+i))/(rms(a(6*t+i))+aver(a(6*t+i))+1e-30)
    print,t,i,err
    if err gt 1e-5 then ok=0
  end
end
if ok eq 1 then begin
  print, 'test OK, deleting files'

  root = 'compare.'
  ext = ['dat','tim','time','dx','chk','tmp']
  for j=0,n_elements(ext)-1 do file_delete,dir+root+ext[j]

  ; root = 'reference.'
  ; ext = ['dat','dx']
  ; for j=0,n_elements(ext)-1 do file_delete,dir+root+ext[j]

end else begin
  print, 'test NOK, leaving files'
end

END
