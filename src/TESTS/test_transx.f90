
 USE params

 implicit none
 real, allocatable, dimension(:,:,:):: s, tau, q1, q2
 real, allocatable, dimension(:,:):: xi
 integer :: n1, iz
 real :: logtaumin, logtaumax, dlogtau, logtau, cput(2), cpu(2), dtime
 logical doxi

 mx=100
 my=mx
 mz=63
 doxi=.true.

 allocate (s(mx,my,mz), tau(mx,my,mz), q1(mx,my,mz), q2(mx,my,mz), xi(mx,my))

 s=1.
 logtaumin=-4.
 logtaumax=4.
 dlogtau=(logtaumax-logtaumin)/(mz-1)
 do iz=1,mz
   logtau=logtaumin+dlogtau*(iz-1)
   tau(:,:,iz)=10.**logtau
   S(:,:,iz)=tau(:,:,iz)+0.01*tau(:,:,iz)**2*(1.+cos(logtau*pi))
 end do

 n1 = mz/2
 cput=dtime(cpu)
 do iz=1,10
   call transi (mz,n1,tau,s,q1,xi,doxi)
 end do
 cput(1)=dtime(cpu)

 do iz=1,10
   call transx (mz,n1,tau,s,q2,xi,doxi)
 end do
 cput(2)=dtime(cpu)

 print *,' iz  log(tau)         S          Q1          Q2'
 do iz=1,mz
   print '(i4,4e12.4)',iz, alog10(tau(1,1,iz)), s(1,1,iz), q1(1,1,iz), q2(1,1,iz)
 end do
 print *,'cpu=',cput
 print *,'mus/pt=',cput*1e6/(mx*my*mz*10.)

 END

