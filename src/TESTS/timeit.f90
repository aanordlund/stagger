
  SUBROUTINE mads(a,b,c,m)
  real, dimension(m):: a,b,c
  do i=1,m
    c(i)=b(i)+b(i)*a(i)
  end do
  END

  SUBROUTINE divs(a,b,c,m)
  real, dimension(m):: a,b,c
  do i=1,m
    c(i)=b(i)/a(i)
  end do
  END

  SUBROUTINE invs(a,b,c,m)
  real, dimension(m):: a,b,c
  do i=1,m
    c(i)=1./a(i)
  end do
  END

  SUBROUTINE muls(a,b,c,m)
  real, dimension(m):: a,b,c
  do i=1,m
    c(i)=a(i)*b(i)
  end do
  END

  SUBROUTINE adds(a,b,c,m)
  real, dimension(m):: a,b,c
  do i=1,m
    c(i)=a(i)+b(i)
  end do
  END

  SUBROUTINE gams(xv,yv,zv,n)
  REAL xv(n),yv(n),zv(n)
  REAL gammln,xx
  INTEGER i,j,n
  DOUBLE PRECISION ser,stp,tmp,x,y,cof(6)
  SAVE cof,stp
  DATA cof,stp/76.18009172947146d0,-86.50532032941677d0, &
     24.01409824083091d0,-1.231739572450155d0,.1208650973866179d-2, &
     -.5395239384953d-5,2.5066282746310005d0/
  do i=1,n
    x=xv(i)
    y=x
    tmp=x+5.5d0
    tmp=(x+0.5d0)*log(tmp)-tmp
    ser=1.000000000190015d0
    do j=1,6
      y=y+1.d0
      ser=ser+cof(j)/y
    end do
    zv(i)=tmp+log(stp*ser/x)
  end do
  return
  END

  SUBROUTINE exps(a,b,c,m)
  real, dimension(m):: a,b,c
  do i=1,m
    c(i)=exp(a(i))
  end do
  END

  SUBROUTINE logs(a,b,c,m)
  real, dimension(m):: a,b,c
  do i=1,m
    c(i)=log(a(i))
  end do
  END

  SUBROUTINE sqrs(a,b,c,m)
  real, dimension(m):: a,b,c
  do i=1,m
    c(i)=sqrt(a(i))
  end do
  END

  SUBROUTINE equs(a,b,c,m)
  real, dimension(m):: a,b,c
  do i=1,m
    c(i)=a(i)
  end do
  END

  FUNCTION timeit(f,l,m,e)
  character(len=*) l
  real cput(2),a(m),b(m),c(m)
  external f
!
  a=1.234
  b=1.
  n=1
  cpu=dtime(cput)
  cpu=0.
  do while (cpu < 0.5)
    call f(a,b,c,m)
    n=n+1
    cpu=cpu+dtime(cput)
  end do
!
  t=1.
  n=(n*t/cpu)
  cpu=dtime(cput)
  do i=1,n
    call f(a,b,c,m)
  end do
  cpu=dtime(cput)
! 
  s=1e9*cpu/(n*m)
  print *,l,s-e,' ns/call ',cpu,' sec ',n,' calls'
  timeit=s
!
  END

  external exps,logs,sqrs,equs,gams,divs,adds,muls,mads,invs

  e=timeit(equs,'equs',10000,0.)
  t=timeit(exps,'exps',10000,e)
  t=timeit(logs,'logs',10000,e)
  t=timeit(sqrs,'sqrs',10000,e)
  t=timeit(gams,'gams',10000,e)
  t=timeit(muls,'muls',10000,e)
  t=timeit(adds,'adds',10000,e)
  t=timeit(mads,'mads',10000,e)
  t=timeit(divs,'divs',10000,e)
  t=timeit(invs,'invs',10000,e)
  END
