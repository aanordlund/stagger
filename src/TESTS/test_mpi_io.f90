!*******************************************************************************
PROGRAM test_mpi_io
  USE mpi_mod
  USE params
  implicit none
  real, allocatable, dimension(:,:,:):: chunk
  integer:: iv, nv, it, nit, intarg
  real:: wc, wcr, wcw, wallclock, GB, MB
  logical:: exists
  character(len=mid):: id='$Id: test_mpi_io.f90,v 1.17 2014/08/30 23:26:05 aake Exp $'
!...............................................................................
  call init_mpi
  if (intarg(1,-1) == -1) then
    if (master) then
      print*, '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
      print*, ' '
      print*, ' Syntax: aprun -np mpi_size test_mpi_io.x mx my mx mpi_ny mpi_nz iterations'
      print*, '     m[xyz] : array dimensions'
      print*, '     mpi_ny : MPI split in y-direction'
      print*, '     mpi_nz : MPI split in z-direction = aggregation factor'
      print*, ' iterations : number of times to write 9 arrays'
      print*, ' '
      print*, ' Example : aprun -np 4 test_mpi_io.x 100 100 100 1 4 1'
      print*, ' '
      print*, '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
    end if
    call end_mpi
  end if
  mx     = intarg (1, 100)
  my     = intarg (2, 100)
  mz     = intarg (3, 100)
  mpi_ny = intarg (4, 1)
  mpi_nz = intarg (5, mpi_size)
  nit    = intarg (6, 1)
!-------------------------------------------------------------------------------
  mpi_nx = 0
  call init_geom_mpi
  call mesh_mpi
!-------------------------------------------------------------------------------
  call print_id(id)
  nv = 9
  GB = nv*real(mxtot)*real(mytot)*real(mztot)*4e-9
  MB = 4e-6*mx*my*mz
  if (master) then
    print*,             '==============================='
    print'(1x,a,i10)'  ,' Number of processes:',mpi_size
    print'(1x,a,i10)'  ,' Number of I/O nodes:',mpi_nx*mpi_ny
    print'(1x,a,i10)'  ,' I/O operations/node:',nv
    print'(1x,a,i10)'  ,'    Repetition count:',nit
    print'(1x,a,i10)'  ,'   Chunks aggregated:',mpi_nz
    print'(1x,a,f10.2)','MB per I/O operation:',mpi_nz*MB
    print'(1x,a,f10.3)','Total file size (GB):',GB
    print*,             '==============================='
  end if
  dbg_select = dbg_mpi
  dbg_level = 1
  allocate (chunk(mx,my,mz))
!-------------------------------------------------------------------------------
  wcr = 0.0
  wcw = 0.0
  do it=1,nit
    call file_openw_mpi ('new.dat')
    wc = wallclock()
    do iv=1,nv
      chunk = iv
      call file_write_mpi (chunk, iv)
    end do
    wc = wallclock()-wc
    wcw = wcw+wc
    call file_close_mpi
    GB = nv*real(mxtot)*real(mytot)*real(mztot)*4e-9
    if (master) print'(1x,a,3f6.2)','write: GB, s, GB/s =',GB,wc,GB/wc
  !-------------------------------------------------------------------------------
    if (master) then
      inquire (file='old.dat',exist=exists)
      if (.not.exists) then
        if (master) then
          print *,'old.dat did not exist; renaming new.dat to old.dat'
          call system('mv new.dat old.dat')
        end if
      end if
    end if
    call barrier_mpi
  !-------------------------------------------------------------------------------
    call file_openr_mpi ('old.dat')
    wc = wallclock()
    do iv=1,nv
      call file_read_mpi (chunk, iv)
    end do
    wc = wallclock()-wc
    wcr = wcr+wc
    call file_close_mpi
    if (master) print'(1x,a,3f8.2)',' read: GB, s, GB/s =',GB,wc,GB/wc
  end do
!-------------------------------------------------------------------------------
  deallocate (chunk)
  if (master) print'(1x,a,2f7.2)', 'average write and read GB/s =', &
    nit*GB/wcw, nit*GB/wcr
  call end_mpi
END
