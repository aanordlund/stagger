! $Id: test_transfer.f90,v 1.2 2004/12/24 02:09:23 aake Exp $
!
!  Example program, showing how to read and use the radiative transfer test values
!
 USE params
 implicit none
 integer n1, iy, ny, i
 logical do_surface_intensity
 real, allocatable, dimension(:,:,:):: s, tau, q1, q2, dtau
 real, allocatable, dimension(:,:):: xi
 real, allocatable, dimension(:):: rkap

 mx=1
 my=401
 mz=mx
 n1 = 15
 do_surface_intensity = .true.

 allocate (s(mx,my,mz), tau(mx,my,mz), q1(mx,my,mz), q2(mx,my,mz), xi(mx,mz), dtau(mx,my,mz))
 allocate (ym(my), rkap(my))

 open (1,file='test_transp.dat',form='unformatted',status='old')
 do i=1,5
   read (1) ny
   read (1) (ym(iy), rkap(iy), tau(1,iy,1), s(1,iy,1), q2(1,iy,1), iy=1,ny)
   dtau(1,1,1) = tau(1,1,1)
   dtau(1,2:ny,1) = tau(1,2:ny,1)-tau(1,1:ny-1,1)
   call transfer (ny,n1,dtau,s,q1,xi,do_surface_intensity)
   call transfer1(ny,n1,dtau,s,q2,xi,do_surface_intensity)
   print *,' iy  log(tau)         S          Q        Qref        diff'
   do iy=1,ny
     print '(i4,5e12.4)',iy, alog10(tau(1,iy,1)), s(1,iy,1), q1(1,iy,1), q2(1,iy,1), q1(1,iy,1)-q2(1,iy,1)
   end do
 end do
 close (1)

 END

