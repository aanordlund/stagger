
 USE params
 implicit none
 integer nop
 common /cop/nop

 real, allocatable, dimension(:,:,:):: s, tau, q1, q2, dtau
 real, allocatable, dimension(:,:):: xi
 integer :: n1, iy, nrep, ny, jy, i
 real :: logtaumin, logtaumax, dlogtau, logtau, cput, cpu(2), dtime, ampl, htau
 logical doxi

 mx=100
 mz=mx
 my=401
 nrep=20*100**3/(mx*my*mz)
 doxi=.true.
 ampl=0.
 n1 = my/2
 dy = 3./my
 dx = 1.
 dz = dx
 call init_mesh

 print *,'ampl,n1 ?'
 read *,ampl,n1

 allocate (s(mx,my,mz), tau(mx,my,mz), q1(mx,my,mz), q2(mx,my,mz), xi(mx,mz), dtau(mx,my,mz))

 s=1.
 logtaumin=-4.
 logtaumax=4.
 dlogtau=(logtaumax-logtaumin)/(my-1)
 do iy=1,my
   logtau=logtaumin+dlogtau*(iy-1)
   tau(:,iy,:)=10.**logtau
   S(:,iy,:)=tau(:,iy,:)+ampl*tau(:,iy,:)**2*(1.+cos(logtau*pi))
 end do
 dtau(:,1,:)=tau(:,1,:)
 dtau(:,2:my,:)=tau(:,2:my,:)-tau(:,1:my-1,:)

 cput=dtime(cpu)
 do iy=1,10
  call transfer (my,n1,dtau,s,q1,xi,doxi)
 end do
 cput=dtime(cpu)

 call transfer1 (my,n1,dtau,s,q2,xi,doxi)
 
 nrep=1+int(10.*2./cput)
 nop=0
 cput=dtime(cpu)
 do iy=1,nrep
   call transfer (my,n1,dtau,s,q1,xi,doxi)
 end do
 cput=dtime(cpu)

 print *,' iy  log(tau)         S          Q        Qref        diff'
 do iy=1,my
   print '(i4,5e12.4)',iy, alog10(tau(1,iy,1)), s(1,iy,1), q1(1,iy,1), q2(1,iy,1), q1(1,iy,1)-q2(1,iy,1)
 end do
 print *,'cpu time =',cput
 print *,'number of calls =',nrep
 print *,'ns/pt/2ray =',cput*1e9/(mx*my*mz*real(nrep))
 print *,real(nop)/(mx*my*mz*nrep)

 dy = 3./my
 ym = dy*(/(iy,iy=0,my-1)/)
 htau = (ym(my)-ym(1))/alog(tau(1,my,1)/tau(1,1,1))

 ny = my
 jy = 1
 open (1,file='test_transp.dat',form='unformatted',status='unknown')
 do i=1,5
   write (1) ny
   write (1) (ym(iy), tau(1,iy,1)/htau, tau(1,iy,1), s(1,iy,1), q1(1,iy,1), iy=1,my,jy)
   jy = jy*2
   ny = ny/2 + 1
 end do
 close (1)

 END

