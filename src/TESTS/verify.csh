#!/bin/csh

if (! $?EXPERIMENT) then
  if (-e EXPERIMENTS/DEFAULT) then
    setenv EXPERIMENT `sed -e '/^\#/d' -e 's/EXPERIMENT *=//' EXPERIMENTS/DEFAULT`
  else if (-e EXPERIMENTS/DEFAULT.mkf) then
    setenv EXPERIMENT `sed -e '/^\#/d' -e 's/EXPERIMENT *=//' EXPERIMENTS/DEFAULT.mkf`
  else
    setenv EXPERIMENT padoan
  endif
endif

if ("$2" != "") then 
  set y = $1
  set x = $2
else
  set y = $EXPERIMENT.x
  if ("$1" != "") then
    set x = $1
  else
    set x = "./$EXPERIMENT.x"
  endif
endif

which $x
if ($status) exit
which $y
if ($status) exit

cat >verify1.in <<EOF
&io     nstep=3 nsnap=1 from='verify0.dat' iread=1 file='verify1.dat' iwrite=0 /
&run    Cdt=-0.5 /
&vars   do_pscalar=t do_mhd=t /
&grid   mx=15, my=15, mz=15, /
&pde    do_loginterp=t do_2nddiv=t nu1=0.1 nu2=0.5 nur=0.2 /
&slice  /
&quench /
&eqofst do_ionization=f /
&part   /
&force  do_force=t ampl_turb=10 /
&selfg  grav=0 /
&init   b0=1 /
&bdry   /
&expl   do_expl=f /
&cool   do_cool=f /
&part   /
EOF

sed -e 's/verify1/verify2/' verify1.in > verify2.in
sed -e "/\&io/s:.*:\&io     nstep=50 nsnap=50 nscr=500 file='verify0.dat' /:" verify1.in > verify0.in

echo "$y < verify0.in > verify0.log"
$y < verify0.in > verify0.log

cp verify0.tim $$.tim
cp $$.tim verify1.tim
echo "$y < verify1.in > verify1.log"
$y < verify1.in > verify1.log

cp $$.tim verify2.tim
cp $$.tim verify0.tim
echo "$x < verify2.in > verify2.log"
$x < verify2.in > verify2.log

\rm -f $$.tim
\rm -f ver*.scr

setenv IDL_DEVICE ps
idl << EOF
.run
open,a,'verify1.dat',nv=nv,nt=nt,/quiet
open,b,'verify2.dat',nv=nv,nt=nt,/quiet
ok=1
for t=0,nt-1 do begin
  for i=0,nv-1 do begin
    dif=(a(6*t+i)-b(6*t+i))/(rms(a(6*t+i))+aver(a(6*t+i))+1e-30)
    err=rms(dif)
    print,t,i,err,min(dif,max=max),max
    if err gt 1e-5 then ok=0
  end
end
if ok eq 1 then begin
  print, 'test OK, deleting files'
  spawn,'\rm verify?.*'
end else begin
  print, 'test NOK, leaving files'
end
END
EOF

