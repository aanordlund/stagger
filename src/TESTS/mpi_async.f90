! $Id: mpi_async.f90,v 1.3 2006/03/31 02:45:07 aake Exp $
!-------------------------------------------------------------------------------
!  Check if one can actully do computations and communications at the same time
!-------------------------------------------------------------------------------
  implicit none
  include 'mpif.h'
  integer err, status(MPI_STATUS_SIZE)
  integer, parameter:: m=400, l=m*m*m
  real, dimension(m,m,m):: f, g
  integer rank, size, from, to, i, request1, request2
  real wt, wt0, wt1, wt2, wt3, wt4, wallclock

!-------------------------------------------------------------------------------
!  Start up MPI and get number of nodes and our node (rank)
!-------------------------------------------------------------------------------
  call MPI_INIT (err)
  call MPI_COMM_SIZE (mpi_comm_world, size, err)
  call MPI_COMM_RANK (mpi_comm_world, rank, err)

  to   = mod(rank+1     ,size)
  from = mod(rank-1+size,size)

!-------------------------------------------------------------------------------
!  Communicate first, and compute afterwards
!-------------------------------------------------------------------------------
  if (rank==0) print *, 'Communicate first, then compute:'
  if (rank==0) print *, '  i rank   total      comm     comp1     comp2'
  do i=1,6
    wt = wallclock()
    if (i == 3) wt0=wt
    call MPI_ISEND (f, l, MPI_REAL, to  , rank, mpi_comm_world, request1, err)
    call MPI_IRECV (g, l, MPI_REAL, from, from, mpi_comm_world, request2, err)
    call MPI_WAIT (request1, status, err)
    call MPI_WAIT (request2, status, err)
    wt1 = wallclock()
    call compute (f, m)
    wt2 = wallclock()
    call compute (g, m)
    wt3 = wallclock()
    print 1, i, rank, wt3-wt, wt1-wt, wt2-wt1, wt3-wt2
  1 format(1x,2i3,5f10.3)
  end do
  call MPI_BARRIER (mpi_comm_world, err)
  if (rank == 0) then
    print *,'total time =',wallclock()-wt0

!-------------------------------------------------------------------------------
!  Start send and receive, compute, wait for completion, compute
!-------------------------------------------------------------------------------
    print *, 'Send, compute, wait, compute:'
    print *, '  i rank   total      send     comp1      comp2      wait'
  end if
  do i=1,6
    wt = wallclock()
    if (i == 3) wt0=wt
    call MPI_ISEND (f, l, MPI_REAL, to  , rank, mpi_comm_world, request1, err)
    call MPI_IRECV (g, l, MPI_REAL, from, from, mpi_comm_world, request2, err)
    wt1 = wallclock()
    call compute (f, m)
    wt2 = wallclock()
    call MPI_WAIT (request1, status, err)
    call MPI_WAIT (request2, status, err)
    wt3 = wallclock()
    call compute (g, m)
    wt4 = wallclock()
    print 1, i, rank, wt4-wt, wt1-wt, wt2-wt1, wt4-wt3, wt3-wt2
  end do
  call MPI_BARRIER (mpi_comm_world, err)
  if (rank == 0) then
    print *,'total time =',wallclock()-wt0

!-------------------------------------------------------------------------------
!  Communicate first, and compute afterwards
!-------------------------------------------------------------------------------
    print *, 'Use MPI_SendRecv:'
    print *, '  i rank   total      comm     comp1     comp2'
  end if
  do i=1,6
    wt = wallclock()
    if (i == 3) wt0=wt
    call MPI_SendRecv (f, l, MPI_REAL, to  , rank, &
                       g, l, MPI_REAL, from, from, &
                       mpi_comm_world, status, err)
    wt1 = wallclock()
    call compute (f, m)
    wt2 = wallclock()
    call compute (g, m)
    wt3 = wallclock()
    print 1, i, rank, wt3-wt, wt1-wt, wt2-wt1, wt3-wt2
  end do
  call MPI_BARRIER (mpi_comm_world, err)
  if (rank == 0) print *,'total time =',wallclock()-wt0

  call MPI_FINALIZE (err)
END

!***********************************************************************
SUBROUTINE compute (f, m)
  implicit none
  integer m
  real, dimension(m,m,m):: f
  integer i,j,k
  do k=1,m
  do j=1,m
  do i=1,m
    f(m,m,m) = i+100*j+10000*k
    !f(m,m,m) = sqrt(f(m,m,m))
    f(m,m,m) = f(m,m,m)**2
  end do
  end do
  end do
END

!***********************************************************************
REAL FUNCTION wallclock()
  implicit none
  integer, save:: count, count_rate=0, count_max
  real, save:: previous=0., offset=0.
  if (count_rate == 0) then
    call system_clock(count=count, count_rate=count_rate, count_max=count_max)
    offset = -count/real(count_rate)
  else
    call system_clock(count=count)
  end if
  wallclock = count/real(count_rate) + offset
  if (wallclock < previous) then
    offset = offset + real(count_max)/real(count_rate)
    wallclock = count/real(count_rate) + offset
  end if
  previous = wallclock
END
