! $Id: mpi_omp.f90,v 1.13 2006/02/24 00:55:43 aake Exp $
!-----------------------------------------------------------------------
! Test hybrid MPI + OpenMP parallelization
!-----------------------------------------------------------------------

MODULE mpi_m
  include 'mpif.h'
  integer ierr, mpi_rank, mpi_size                                      ! global MPI data
END MODULE

!-----------------------------------------------------------------------
MODULE omp_m
  integer omp_thread, omp_nthreads
  common /omp/ omp_thread, omp_nthreads                                 ! global OMP data
  !$omp threadprivate(/omp/)
END MODULE

!-----------------------------------------------------------------------
SUBROUTINE init_mpi
  USE mpi_m
  USE omp_m
  implicit none

  call mpi_init(ierr)
  call mpi_comm_rank (mpi_comm_world, mpi_rank, ierr)                   ! find the rank
  call mpi_comm_size (mpi_comm_world, mpi_size, ierr)                   ! find the size
  call mpi_barrier (mpi_comm_world, ierr)
END SUBROUTINE

!-----------------------------------------------------------------------
SUBROUTINE end_mpi
  USE mpi_m
  call mpi_finalize(ierr)
END SUBROUTINE

!-----------------------------------------------------------------------
SUBROUTINE init_omp
  USE mpi_m
  USE omp_m
  implicit none
  integer i, omp_get_num_threads, omp_get_thread_num

  call omp_set_num_threads (4)                                          ! must set in program under MPI
  !$omp parallel
  omp_thread = omp_get_thread_num()
  omp_nthreads = omp_get_num_threads()
  print *,'init_omp: mpi_rank, omp_thread, n =', mpi_rank, omp_thread, omp_nthreads
  !$omp end parallel
END SUBROUTINE

!-----------------------------------------------------------------------
PROGRAM test
  USE omp_m
  USE mpi_m

  call init_mpi
  call init_omp

  !$omp parallel
  call do_something                                                     ! call inside a parallel region
  !$omp end parallel

  call end_mpi
END

!-----------------------------------------------------------------------
SUBROUTINE do_something                                                 ! each OMP thread enters separately
  USE mpi_m
  USE omp_m
  implicit none
  integer out, in1, in2, tag, req1, req2, req3, req4, status, up, dn, from

!  Must do all mpi_calls one by one in OMP, and finish all sending and
!  receiving before waiting; hence need two critical regions here

  !$omp critical
    out = mpi_rank*10+omp_thread                                        ! what to send
    up = mod(mpi_rank+1+mpi_size,mpi_size)                              ! where to send to
    dn = mod(mpi_rank-1+mpi_size,mpi_size)                              ! where to send to
    tag = 2*(mpi_rank*omp_nthreads+omp_thread)                          ! package tag
    call mpi_isend(out,1,MPI_INT,up,tag,mpi_comm_world,req1,ierr)       ! start sending
    print 1,'rank, thread, up  , out, req1', mpi_rank, omp_thread, up, out, req1
    tag = 2*(mpi_rank*omp_nthreads+omp_thread) + 1                      ! package tag
    call mpi_isend(out,1,MPI_INT,dn,tag,mpi_comm_world,req3,ierr)       ! start sending
    print 1,'rank, thread, to  , out, req3', mpi_rank, omp_thread, dn, out, req3

    tag = 2*(dn*omp_nthreads+omp_thread)                                ! package tag
    call mpi_irecv(in1,1,MPI_INT,dn,tag,mpi_comm_world,req2,ierr)       ! start receiving
    print 1,'rank, thread, from, tag, req2', mpi_rank, omp_thread, dn, tag, req2
    tag = 2*(up*omp_nthreads+omp_thread) + 1                            ! package tag
    call mpi_irecv(in2,1,MPI_INT,up,tag,mpi_comm_world,req4,ierr)       ! start receiving
    print 1,'rank, thread, from, tag, req4', mpi_rank, omp_thread, up, tag, req4
  !$omp end critical

  ! call mpi_barrier (mpi_comm_world, ierr)

  !$omp critical
    call mpi_wait (req1, status, ierr)                                  ! wait for send
    call mpi_wait (req3, status, ierr)                                  ! wait for send
    call mpi_wait (req2, status, ierr)                                  ! wait for receive
    call mpi_wait (req4, status, ierr)                                  ! wait for receive
    print 1,'rank, thread, from, in       ', mpi_rank, omp_thread, dn, in1
    print 1,'rank, thread, from, in       ', mpi_rank, omp_thread, up, in2
    out = dn*10+omp_thread
    if (in1 .ne. out) print *,'ERROR',mpi_rank,omp_thread,in1,out
    out = up*10+omp_thread
    if (in2 .ne. out) print *,'ERROR',mpi_rank,omp_thread,in2,out
  !$omp end critical
 1  format(1x,a,10i5)
END
