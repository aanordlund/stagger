MODULE test
CONTAINS

!-----------------------------------------------------------------------
  SUBROUTINE xdn_set (f,xdn)
  USE params
  real, dimension(mx,my,mz):: f, xdn
  integer omp_get_max_threads,omp_get_num_procs,omp_get_thread_num

  print *,'xdn',omp_get_max_threads(),omp_get_num_procs(),omp_get_thread_num()
!xx$omp parallel
!$omp do private(iz)
  do iz=1,mz
    !print *,'xdn1',iz,omp_get_max_threads(),omp_get_num_procs(),omp_get_thread_num()
    xdn(:,:,iz) = cshift(f(:,:,iz),dim=1,shift=-1)
  end do
!xx$omp end parallel

  END subroutine

!-----------------------------------------------------------------------
  SUBROUTINE ydn_set (f,ydn)
  USE params
  real, dimension(mx,my,mz):: f, ydn
  integer omp_get_max_threads,omp_get_num_procs,omp_get_thread_num

  print *,'ydn',omp_get_max_threads(),omp_get_num_procs(),omp_get_thread_num()
!xx$omp parallel
!$omp do private(iz)
  do iz=1,mz
    ydn(:,:,iz) = cshift(f(:,:,iz),dim=2,shift=-1)
  end do
!xx$omp end parallel

  END subroutine
END MODULE test

!-----------------------------------------------------------------------
  USE params
  USE test
  real, allocatable, dimension(:,:,:):: f,g,h
  real, allocatable, dimension(:):: ftmp
  integer m, omp_get_max_threads,omp_get_num_procs,omp_get_thread_num
  integer ix,iy,iz,icpu
  real fsum, cpu, cpus(2,32)

  read *,m
  mx=m
  my=m
  mz=m
  allocate (f(m,m,m), g(m,m,m), h(m,m,m), ftmp(m))

!$omp parallel private(ix,iy,iz,cpu,icpu)
  icpu = omp_get_thread_num()

!$omp do
  do iz=1,mz
  do iy=1,my
  do ix=1,mx
    f(ix,iy,iz) = ix+1000*iy
  end do
  end do
  end do

!$omp master
  cpu = dtime(cpus(1,icpu))
!$omp end master
!$omp do
  do iz=1,mz
    ftmp(iz) = sum(f(:,:,iz))
  end do
!$omp master
  cpu = dtime(cpus(1,icpu))
  print *,'cpu(sum loop)=',cpu,omp_get_thread_num()
!$omp end master

!$omp single
  print *,sum(ftmp)
!$omp end single
!$omp master
  cpu = dtime(cpus(1,icpu))
  print *,'cpu(sum)=',cpu,omp_get_thread_num()
!$omp end master

  print *,omp_get_max_threads(),omp_get_num_procs(),omp_get_thread_num()
  call xdn_set(f,g)
  call ydn_set(g,h)

!$omp master
  cpu = dtime(cpus(1,icpu))
  print *,'cpu(xdn,ydn)=',cpu,omp_get_thread_num()
!$omp end master

!$omp end parallel
  print *,f(1:5,2,1)
  print *,g(1:5,2,1)
  print *,h(1:5,2,1)

  END
