!*******************************************************************************
PROGRAM test
  USE params
  USE mpi_mod
  implicit none
  integer intarg, iter, niter
!...............................................................................
  call init_mpi
  if (intarg(1,-1) == -1) then
    if (master) then
      print*, '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
      print*, ' '
      print*, ' Syntax: aprun -np mpi_size test_fft2d.x mx my mx mpi_ny mpi_nz iterations'
      print*, '     m[xyz] : array dimensions'
      print*, '     mpi_ny : MPI split in y-direction'
      print*, '     mpi_nz : MPI split in z-direction'
      print*, ' iterations : number of times to do the test'
      print*, ' '
      print*, ' Example : aprun -np 4 test_fft2d.x 100 100 100 1 4 1'
      print*, ' '
      print*, '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
    end if
    call end_mpi
  end if
  mx     = intarg (1, 100)
  my     = intarg (2, 100)
  mz     = intarg (3, 100)
  mpi_ny = intarg (4, 1)
  mpi_nz = intarg (5, mpi_size)
  niter  = intarg (6, 1)
  call init_geom_mpi
!-------------------------------------------------------------------------------
  do iter=1,niter
    call test_fft2d
  end do
  call end_mpi
END PROGRAM
