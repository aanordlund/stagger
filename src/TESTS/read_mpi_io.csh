#! /bin/csh
# $Id: read_mpi_io.csh,v 1.1 2014/08/29 11:36:55 aake Exp $

#setenv MPICH_MPIIO_HINTS "*:romio_cb_read=disable:romio_ds_read=disable:romio_no_indep_rw=true"

set echo
setenv MPICH_MPIIO_STATS
setenv MPICH_MPIIO_HINTS_DISPLAY
unsetenv MPICH_MPIIO_CB_ALIGN
foreach n ( 4032 8064 32256 )
  setenv MPICH_MPIIO_HINTS "*:romio_no_indep_rw=true"
  aprun -e MPICH_MPIIO_HINTS="$MPICH_MPIIO_HINTS" -n $n read_mpi_io.x 96x30Mm_t747b_2016x612x2016.scr 2016 612 2016 1 48 2
  unsetenv MPICH_MPIIO_HINTS_DISPLAY
  aprun -e MPICH_MPIIO_HINTS="$MPICH_MPIIO_HINTS" -n $n read_mpi_io.x 96x30Mm_t747b_2016x612x2016.scr 2016 612 2016 2  2 2
  setenv MPICH_MPIIO_HINTS "*:romio_cb_read=disable:romio_ds_read=disable"
  aprun -e MPICH_MPIIO_HINTS="$MPICH_MPIIO_HINTS" -n $n read_mpi_io.x 96x30Mm_t747b_2016x612x2016.scr 2016 612 2016 1 48 2
  aprun -e MPICH_MPIIO_HINTS="$MPICH_MPIIO_HINTS" -n $n read_mpi_io.x 96x30Mm_t747b_2016x612x2016.scr 2016 612 2016 2  2 2
end

