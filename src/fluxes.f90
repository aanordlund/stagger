! $Id: fluxes.f90,v 1.39 2015/11/03 14:18:09 bob Exp $

!*******************************************************************************
SUBROUTINE hd_fluxes (rho,Ux,Uy,Uz,e,p,py,Txy,Tyy,Tyz,scr1,scr2,scr3,scr4,scr5,scr6)
  USE params
  USE arrays, ONLY: ekin, fvisc, fkin, fconv, frad, emag, fpoynt, qqja, wk_L, fei, pyav, facous
  implicit none
  real, dimension(mx,my,mz):: rho,Ux,Uy,Uz,e,p,py,Txy,Tyy,Tyz,scr1,scr2,scr3,scr4,scr5,scr6
  real, dimension(mx,my,mz):: scr7,scr8
  logical foutput
  integer iy, iz
  real(8), allocatable, dimension(:,:,:):: dbl1, dbl2

  call print_trace ('fluxes',0,'HD')
  if (.not.foutput('fluxes.txt')) return

!-------------------------------------------------------------------------------
!  Recenter velocities
!-------------------------------------------------------------------------------
  call yup_set (Uy,scr5)
  do iz=izs,ize
    scr6(:,:,iz) = scr5(:,:,iz)**2
  end do
  call xup_set (Ux,scr5)
  do iz=izs,ize
    scr6(:,:,iz) = scr6(:,:,iz) + scr5(:,:,iz)**2
  end do
  call ydn_set (scr5,scr1)                                                      ! scr1 = ydn(xup(Ux))
  call barrier_omp ('hd_flux')
  call zup_set (Uz,scr5)
  do iz=izs,ize
    scr6(:,:,iz) = 0.5*rho(:,:,iz)*(scr6(:,:,iz) + scr5(:,:,iz)**2)
  end do
  call haverage_subr (scr6, ekin)
  call ydn_set (scr5,scr3)                                                      ! scr3 = ydn(zup(Uz))


!-------------------------------------------------------------------------------
!  Viscous energy flux   fvisc = <Ux*Txy + Uy*Tyy + Uz*Tyz>
!-------------------------------------------------------------------------------
  call xup_set (Txy, scr4)                                                      ! recenter Txy
  call ydn_set (Tyy, scr5)                                                      ! recenter Tyy-p
  call zup_set (Tyz, scr6)                                                      ! recenter Tyz
  do iz=izs,ize
    scr2(:,:,iz) = scr1(:,:,iz)*scr4(:,:,iz) &                                  ! scr2 = viscous energy flux, centered at iy-1/2
                 +   Uy(:,:,iz)*scr5(:,:,iz) &
                 + scr3(:,:,iz)*scr6(:,:,iz)
  end do
  call haverage_subr (scr2, fvisc)
  call dumpn (scr2, 'fvisc', 'hdfluxes.dmp', 0)

  ! fill ghost zones values
  if ( lb > 1 )  fvisc(    1:lb-1) = fvisc(lb)
  if ( ub < my)  fvisc( ub+1:my  ) = fvisc(ub)


!-------------------------------------------------------------------------------
!  Subtract mean flow, to avoid large excursions af convective flux
!-------------------------------------------------------------------------------
  call haverage_subr (py, pyav)                                                 ! mean mass flux
  do iz=izs,ize
    do iy=1,my
      scr4(:,iy,iz) = (py(:,iy,iz) - pyav(iy))                                  ! scr4 = py - haver(py)
    end do
    scr5(:,:,iz) = alog(rho(:,:,iz))
  end do
  call ydn_set (scr5, scr6)
  do iz=izs,ize
    scr2(:,:,iz) = scr4(:,:,iz)/exp(scr6(:,:,iz))                               ! Uy' = (py-haver(py))/rho
  end do


!-------------------------------------------------------------------------------
!  Kinetic energy flux   fkin = <(py-<py>)*0.5*(Ux**2+Uy**2+Uz**2)>
!-------------------------------------------------------------------------------
  do iz=izs,ize
    scr5(:,:,iz) = scr4(:,:,iz)*0.5*(  &                                        ! scr5 = kinetic energy flux, centered at iy-1/2
                     scr1(:,:,iz)**2 + &
                     scr2(:,:,iz)**2 + &
                     scr3(:,:,iz)**2)
  end do
  call haverage_subr (scr5, fkin)
  call dumpn (scr5, 'fkin' , 'hdfluxes.dmp', 1)


  ! fill ghost zones values
  if ( lb > 1 )  fkin(    1:lb-1) = fkin(lb)
  if ( ub < my)  fkin( ub+1:my  ) = fkin(ub)



!-------------------------------------------------------------------------------
!  Convective flux   fconv = <(py-<py>)*(e+p)/rho>
!-------------------------------------------------------------------------------
  allocate (dbl1(mx,my,mz), dbl2(mx,my,mz))
  do iz=izs,ize
    dbl1(:,:,iz) = (e(:,:,iz)+p(:,:,iz))/rho(:,:,iz)                            ! dbl1 = H = (e+p)/rho
  end do
  call ydn1_set_r8 (dbl1, dbl2)                                                 ! dbl2 = ydn(H)
  do iz=izs,ize
    scr5(:,:,iz) = scr4(:,:,iz)*dbl2(:,:,iz)                                    ! scr5 = convective energy flux, centered at iy-1/2
  end do
  call haverage_subr (scr5, fconv)
  call dumpn (scr5, 'fconv', 'hdfluxes.dmp', 1)

  ! fill ghost zones values
  if ( lb > 1 )  fconv(    1:lb-1) = fconv(lb)
  if ( ub > my)  fconv( ub+1:my  ) = fconv(ub)



!-------------------------------------------------------------------------------
!  Auxiliary information: internal energy flux and acoustic flux
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
!  Internal Energy   flux fei = <(py-<py>)*e/rho>
!-------------------------------------------------------------------------------

  do iz=izs,ize
     dbl1(:,:,iz) = e(:,:,iz)/rho(:,:,iz)                                       ! scr7 = ee = e/rho
  end do
  call ydn1_set_r8 (dbl1, dbl2)                                                 ! scr8 = ydn(ee)
  do iz=izs,ize
     scr7(:,:,iz) = scr4(:,:,iz)*dbl2(:,:,iz)                                   ! scr7 = internal energy flux, centered at iy-1/2
  end do
  deallocate (dbl1, dbl2)
  call haverage_subr (scr7, fei)
  call dumpn (scr7, 'fei', 'hdfluxes.dmp', 1)


  ! fill ghost zones values
  if ( lb > 1 )  fei(    1:lb-1) = fei(lb)
  if ( ub < my)  fei( ub+1:my  ) = fei(ub)


!-------------------------------------------------------------------------------
!  Acoustic flux   facous = <(py-<py>)*p/rho>
!-------------------------------------------------------------------------------

  do iz=izs,ize
     scr8(:,:,iz) = scr5(:,:,iz)-scr7(:,:,iz)                                   ! scr8 = acoustic flux = conv flux - int energy flux
  enddo
  call haverage_subr (scr8, facous)
  call dumpn (scr8, 'facous', 'hdfluxes.dmp', 1)


  ! fill ghost zones values
  if ( lb > 1 )  facous(    1:lb-1) = facous(lb)
  if ( ub < my)  facous( ub+1:my  ) = facous(ub)

END SUBROUTINE hd_fluxes


!*******************************************************************************
! $Id: fluxes.f90,v 1.39 2015/11/03 14:18:09 bob Exp $
SUBROUTINE radiative_flux (qq, scr1)

  USE params
  USE arrays, ONLY: frad
  implicit none
  real, dimension(mx,my,mz), intent(in)  :: qq
  real, dimension(mx,my,mz), intent(out) :: scr1
  logical  :: foutput
  integer  :: iy, iz, iy_up, iy_low
  character(len=mid):: id="$Id: fluxes.f90,v 1.39 2015/11/03 14:18:09 bob Exp $"



  ! compute partial radiative flux by integrating radiative heating rate
  ! over the extent of local sub-domain;
  !
  ! the full radiative heating rate is then computed in hav.f90

  call print_id(id)
  call print_trace ('fluxes',0,'radiative')

  if (.not.foutput('fluxes.txt')) return

  if (mpi_ny > 1 .and. master) then
     print *, 'warning: radiative_flux called with mpi_ny =', mpi_ny
  end if

  iy_up  = min ( my, ub )
  iy_low = max (  1, lb )

  do iz=izs,ize
     scr1(:,iy_up,iz) = dym(iy_up) * qq(:,iy_up,iz)
     do iy = iy_up-1, iy_low,-1
        scr1(:,iy,iz) = scr1(:,iy+1,iz) + dym(iy) * qq(:,iy,iz)
     end do
  end do

  call haverage_subr (scr1, frad)
  call dumpn (scr1, 'frad', 'hdfluxes.dmp', 1)

  ! fill ghost zones values
  if ( lb > 1 )  frad (    1:lb-1)  =  frad ( lb )
  if ( ub < my)  frad ( ub+1:my  )  =  frad ( ub )

END SUBROUTINE radiative_flux


!*******************************************************************************
SUBROUTINE Poynting_flux (Ex,Ey,Ez,Bx,By,Bz,scr1,scr2,scr3,scr4,scr5,scr6)
  USE params
  USE arrays, ONLY: fvisc, fpoynt, frad
  implicit none
  real, dimension(mx,my,mz):: Ex,Ey,Ez,Bx,By,Bz,scr1,scr2,scr3,scr4,scr5,scr6
  logical foutput
  integer iy, iz

  call print_trace ('fluxes',0,'Poynting')
  if (.not.foutput('fluxes.txt')) return

  call barrier_omp('poynting')

 if (do_dump) then
  call zup_set (Ey, scr2)
  call yup_set (Ez, scr4)
  call zup_set (Bz, scr1)                                                       ! cell centered BZ
  call yup_set (By, scr3)                                                       ! cell centered Bz
  call xdn_set (scr1, scr5)                                                     ! (ix,iy-1/2,iz)
  call xdn_set (scr3, scr6)                                                     ! (ix,iy-1/2,iz)

  do iz=izs,ize
    scr3(:,:,iz) = scr2(:,:,iz)*scr5(:,:,iz) &                                  ! x Poynting flux
                 - scr4(:,:,iz)*scr6(:,:,iz)                                    ! half-point centered
  end do
  call dumpn (scr3, 'fpoynt_x', 'mhdfluxes.dmp', 1)
 endif

  call xup_set (Ez, scr2)
  call zup_set (Ex, scr4)
  call xup_set (Bx, scr1)                                                       ! cell centered BZ
  call zup_set (Bz, scr3)                                                       ! cell centered Bz
  call ydn_set (scr1, scr5)                                                     ! (ix,iy-1/2,iz)
  call ydn_set (scr3, scr6)                                                     ! (ix,iy-1/2,iz)

  do iz=izs,ize
    scr1(:,:,iz) = scr2(:,:,iz)*scr5(:,:,iz) &                                  ! vertical Poynting flux
                 - scr4(:,:,iz)*scr6(:,:,iz)                                    ! half-point centered
  end do
  call haverage_subr (scr1, fpoynt)
  if ( lb > 1 )  fpoynt(    1:lb-1) = fpoynt(lb)                                ! fill ghost zones values
  if ( ub < my)  fpoynt( ub+1:my  ) = fpoynt(ub)
  call dumpn (scr1, 'fpoynt_y', 'mhdfluxes.dmp', 1)

 if (do_dump) then
  call yup_set (Ex, scr2)
  call xup_set (Ey, scr4)
  call yup_set (By, scr1)                                                       ! cell centered BZ
  call xup_set (Bx, scr3)                                                       ! cell centered Bz
  call zdn_set (scr1, scr5)                                                     ! (ix,iy-1/2,iz)
  call zdn_set (scr3, scr6)                                                     ! (ix,iy-1/2,iz)

  do iz=izs,ize
    scr1(:,:,iz) = scr2(:,:,iz)*scr5(:,:,iz) &                                  ! z Poynting flux
                 - scr4(:,:,iz)*scr6(:,:,iz)                                    ! half-point centered
  end do
  call dumpn (scr1, 'fpoynt_z', 'mhdfluxes.dmp', 1)
 end if

END SUBROUTINE Poynting_flux


!*******************************************************************************
SUBROUTINE output_fluxes

  USE params
  USE arrays, ONLY: ekin, fvisc, fkin, fconv, frad, pyav, emag, fpoynt, qqja, &
                    fedif, wk_L

  implicit none

  logical foutput
  integer iy, iz, i, iostat1
  character(len=mid), save:: id='$Id: fluxes.f90,v 1.39 2015/11/03 14:18:09 bob Exp $'
  character(len=mfile) name

  call print_id(id)
  call print_trace ('fluxes',0,'output')
  if (.not.foutput('fluxes.txt')) return

  if (master) then
     print '(a19,a,a15,i5)',&
          'writing fluxes to ', trim(name('fluxes.txt','flux',file)),&
          ' for snapshot ',isnap
     do i=1,maxtry
       open (data_unit,file=name('fluxes.txt','flux',file), &
          status='unknown',position='append', iostat=iostat1)
       if(iostat1==0) exit
       print *,'file,unit,no. tries: ',name('fluxes.txt','flux',file),data_unit,i
       call sleep(1)
     end do

     if (do_mhd) then
        write (data_unit,*) my,11,isnap+1,t,'  mhd fluxes'
     else
        write (data_unit,*) my,7,isnap+1,t,'  hd fluxes'
     endif

1    format(1p,11e12.4)

     write (data_unit,*) 'ekin'
     write (data_unit,1)  ekin
     write (data_unit,*) 'fvisc'
     write (data_unit,1)  fvisc
     write (data_unit,*) 'fkin'
     write (data_unit,1)  fkin
     write (data_unit,*) 'fconv'
     write (data_unit,1)  fconv
     write (data_unit,*) 'frad'
     write (data_unit,1)  frad
     write (data_unit,*) 'pyav'
     write (data_unit,1)  pyav
     write (data_unit,*) 'fedif'
     write (data_unit,1)  fedif

     if (do_mhd) then
        write (data_unit,*) 'emag'
        write (data_unit,1)  emag
        write (data_unit,*) 'fpoynt'
        write (data_unit,1)  fpoynt
        write (data_unit,*) 'qqja'
        write (data_unit,1)  qqja
        write (data_unit,*) 'wk_L'
        write (data_unit,1)  wk_L
     end if

     close (data_unit)

  end if

END SUBROUTINE output_fluxes
