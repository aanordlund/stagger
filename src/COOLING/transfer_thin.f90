! $Id: transfer_thin.f90,v 1.6 2008/10/13 20:27:45 aake Exp $
!***********************************************************************
SUBROUTINE transfer_x (mx, my, mz, dtau0, src0, q0, afwd0, arev0, lpy, lpz)
  USE params, ONLY: izs, ize
  USE radiation_m, ONLY: verbose
  implicit none
  integer                   :: mx, my, mz, lpy, lpz
  real, dimension(mx,my,mz) :: dtau0, src0, q0, afwd0, arev0
!-----------------------------------------------------------------------
! mx, my, mz    dimensions
! lpy, lpz      periodic displacements in the y-z plane
! dtau0         optical depth increments
! src0          source function
! q0            resulting I-S
! a0            attenuation factors exp(-dtau)
!-----------------------------------------------------------------------
  real, dimension(0:mx+1) :: qfwd, qrev, dtau, src, q, afwd, arev
  integer                 :: ix, iy, iz, iy1, iz1
  real                    :: qfwda, qreva
  character(len=80):: id='transfer_x $Id: transfer_thin.f90,v 1.6 2008/10/13 20:27:45 aake Exp $'
!-----------------------------------------------------------------------
  call print_id (id)

!-----------------------------------------------------------------------
! For each ray intersection with the y-z plane, set up and solve RT
!-----------------------------------------------------------------------
  qfwda = 0.
  qreva = 0.
  if (verbose>1) &
    print '(1x,a)',' ix           src             Q          Qfwd          Qrev          afwd          arev'
  !$omp barrier
  do iz=izs,ize
   do iy=1,my
     dtau(1:mx) = dtau0(1:mx,iy,iz)
     src (1:mx) = src0 (1:mx,iy,iz)
     iy1 = mod(iy+lpy-1,my)+1      
     iz1 = mod(iz+lpz-1,mz)+1
     dtau(mx+1) = dtau0(1,iy1,iz1)
     src (mx+1) = src0 (1,iy1,iz1)
     !qrev(mx+1) = -src(mx+1)
     qrev(mx+1) = 0.
     iy1 = mod(iy-lpy-1+my,my)+1      
     iz1 = mod(iz-lpz-1+mz,mz)+1
     dtau(0)    = dtau0(mx,iy1,iz1)
     src (0   ) = src0 (mx,iy1,iz1)
     !qfwd(0   ) = -src(0)
     qfwd(0   ) = 0.
     call transfer1 (mx+2, dtau, src, q, qfwd, qrev, afwd, arev)
     q0   (1:mx,iy,iz) = q   (1:mx)
     afwd0(1:mx,iy,iz) = afwd(1:mx)
     arev0(1:mx,iy,iz) = arev(1:mx)
     qfwda = qfwda + qfwd(mx)/(1-afwd(mx))
     qreva = qreva + qrev( 1)/(1-arev(1))
     if (iy.eq.1 .and. iz.eq.1 .and. verbose>1) then
       print 1,(ix,src(ix),q(ix),qfwd(ix),qrev(ix),afwd(ix),arev(ix), ix=0,mx+1)
     1 format(i4,6g14.6)
     end if
   end do
  end do
  !$omp barrier
  qfwda = qfwda/(my*mz)
  qreva = qreva/(my*mz)
  if (verbose>0) &
    print *,'qfwda,qreva =',qfwda,qreva
  !do iz=1,mz
  !do iy=1,my
  !  do ix=mx,1,-1
  !    q0(ix,iy,iz) = q0(ix,iy,iz) + (0.5*qreva)*arev0(ix,iy,iz)
  !  end do
  !!  do ix=1,mx
  !    q0(ix,iy,iz) = q0(ix,iy,iz) + (0.5*qfwda)*afwd0(ix,iy,iz)
  !  end do
  !end do
  !end do
  if (verbose>1) then
    print '(1x,a)',' ix           src             Q'
    print '(i4,2g14.6)',(ix,src0(ix,1,1),q0(ix,1,1), ix=1,mx)
  end if
END

!***********************************************************************
SUBROUTINE transfer_y (mx, my, mz, dtau0, src0, q0, afwd0, arev0, lpz, lpx)
  USE params, ONLY: izs, ize
  USE radiation_m, ONLY: verbose
  implicit none
  integer                   :: mx, my, mz, lpx, lpz
  real, dimension(mx,my,mz) :: dtau0, src0, q0, afwd0, arev0
!-----------------------------------------------------------------------
! mx, my, mz    dimensions
! lpz, lpx      periodic displacements in the x-z plane
! dtau0         optical depth increments
! src0          source function
! q0            resulting I-S
! a0            attenuation factors exp(-dtau)
!-----------------------------------------------------------------------
  real, dimension(0:my+1) :: qfwd, qrev, dtau, src, q, afwd, arev
  integer                 :: ix, iy, iz, ix1, iz1
  real                    :: qfwda, qreva
  character(len=80):: id='transfer_y $Id: transfer_thin.f90,v 1.6 2008/10/13 20:27:45 aake Exp $'
!-----------------------------------------------------------------------
  call print_id (id)

!-----------------------------------------------------------------------
! For each ray intersection with the x-z plane, set up and solve RT
!-----------------------------------------------------------------------
  qfwda = 0.
  qreva = 0.
  if (verbose>1) &
    print '(1x,a)',' iy           src             Q          Qfwd          Qrev          afwd          arev'
  !$omp barrier
  do iz=izs,ize
   do ix=1,mx
     dtau(1:my) = dtau0(ix,1:my,iz)
     src (1:my) = src0 (ix,1:my,iz)
     ix1 = mod(ix+lpx-1,mx)+1      
     iz1 = mod(iz+lpz-1,mz)+1
     dtau(my+1) = dtau0(ix1,1,iz1)                                      ! wrap around
     src (my+1) = src0 (ix1,1,iz1)                                      ! wrap around
     !qrev(my+1) = -src(my+1)                                            ! no infalling intensity
     qrev(my+1) = 0.                                                    ! no infalling cooling
     ix1 = mod(ix-lpx-1+mx,mx)+1      
     iz1 = mod(iz-lpz-1+mz,mz)+1
     dtau(0)    = dtau0(ix1,my,iz1)                                     ! wrap around
     src (0   ) = src0 (ix1,my,iz1)                                     ! wrap around
     !qfwd(0   ) = -src(0)                                               ! no infalling intensity
     qfwd(0   ) = 0.                                                    ! no infalling cooling
     call transfer1 (my+2, dtau, src, q, qfwd, qrev, afwd, arev)
     q0(ix,1:my,iz) = q(1:my)
     afwd0(ix,1:my,iz) = afwd(1:my)
     arev0(ix,1:my,iz) = arev(1:my)
     qfwda = qfwda + qfwd(my)/(1-afwd(my))
     qreva = qreva + qrev( 1)/(1-arev(1))
     if (ix.eq.1 .and. iz.eq.1 .and. verbose>1) then
       print 1,(iy,src(iy),q(iy),qfwd(iy),qrev(iy),afwd(iy),arev(iy), iy=0,my+1)
     1 format(i4,6g14.6)
     end if
   end do
  end do
  !$omp barrier
  qfwda = qfwda/(mx*mz)
  qreva = qreva/(mx*mz)
  if (verbose>0) &
    print *,'qfwda,qreva =',qfwda,qreva
  !do iz=1,mz
  !  do iy=my,1,-1
  !    q0(:,iy,iz) = q0(:,iy,iz) + (0.5*qreva)*arev0(:,iy,iz)
  !  end do
  !  do iy=1,my
  !    q0(:,iy,iz) = q0(:,iy,iz) + (0.5*qfwda)*afwd0(:,iy,iz)
  !  end do
  !end do
  if (verbose>1) then
    print '(1x,a)',' iy           src             Q'
    print '(i4,2g14.6)',(iy,src0(1,iy,1),q0(1,iy,1), iy=1,my)
  end if
END

!***********************************************************************
SUBROUTINE transfer_z (mx, my, mz, dtau0, src0, q0, afwd0, arev0, lpx, lpy)
  USE params, ONLY: iys, iye
  USE radiation_m, ONLY: verbose
  implicit none
  integer                   :: mx, my, mz, lpx, lpy
  real, dimension(mx,my,mz) :: dtau0, src0, q0, afwd0, arev0
!-----------------------------------------------------------------------
! mx, my, mz    dimensions
! lpx, lpy      periodic displacements in the x-y plane
! dtau0         optical depth increments
! src0          source function
! q0            resulting I-S
! a0            attenuation factors exp(-dtau)
!-----------------------------------------------------------------------
  real, dimension(0:mz+1) :: qfwd, qrev, dtau, src, q, afwd, arev
  integer                 :: ix, iy, iz, ix1, iy1
  real                    :: qfwda, qreva
  character(len=80):: id='transfer_z $Id: transfer_thin.f90,v 1.6 2008/10/13 20:27:45 aake Exp $'
!-----------------------------------------------------------------------
  call print_id (id)

!-----------------------------------------------------------------------
! For each ray intersection with the x-y plane, set up and solve RT
!-----------------------------------------------------------------------
  qfwda = 0.
  qreva = 0.
  if (verbose>1) &
    print '(1x,a)',' iz           src             Q          Qfwd          Qrev          afwd          arev'
  !$omp barrier
  do iy=iys,iye
   do ix=1,mx
     dtau(1:mz) = dtau0(ix,iy,1:mz)
     src (1:mz) = src0 (ix,iy,1:mz)
     ix1 = mod(ix+lpx-1,mx)+1      
     iy1 = mod(iy+lpy-1,my)+1
     dtau(mz+1) = dtau0(ix1,iy1,1)
     src (mz+1) = src0 (ix1,iy1,1)
     !qrev(mz+1) = -src(mz+1)
     qrev(mz+1) = 0.
     ix1 = mod(ix-lpx-1+mx,mx)+1      
     iy1 = mod(iy-lpy-1+my,my)+1
     dtau(0)    = dtau0(ix1,iy1,mz)
     src (0   ) = src0 (ix1,iy1,mz)
     !qfwd(0   ) = -src(0)
     qfwd(0   ) = 0.
     call transfer1 (mz+2, dtau, src, q, qfwd, qrev, afwd, arev)
     q0(ix,iy,1:mz) = q(1:mz)
     afwd0(ix,iy,1:mz) = afwd(1:mz)
     arev0(ix,iy,1:mz) = arev(1:mz)
     qfwda = qfwda + qfwd(mz)/(1-afwd(mz))
     qreva = qreva + qrev( 1)/(1-arev(1))
     if (ix.eq.1 .and. iy.eq.1 .and. verbose>1) then
       print 1,(iz,src(iz),q(iz),qfwd(iz),qrev(iz),afwd(iz),arev(iz), iz=0,mz+1)
     1 format(i4,6g14.6)
     end if
   end do
  end do
  !$omp barrier
  qfwda = qfwda/(mx*my)
  qreva = qreva/(mx*my)
  if (verbose>0) &
    print *,'qfwda,qreva =',qfwda,qreva
  !do iy=1,my
  !  do iz=mz,1,-1
  !    q0(:,iy,iz) = q0(:,iy,iz) + (0.5*qreva)*arev0(:,iy,iz)
  !  end do
  !  do iz=1,mz
  !    q0(:,iy,iz) = q0(:,iy,iz) + (0.5*qfwda)*afwd0(:,iy,iz)
  !  end do
  !end do
  if (verbose>1) then
    print '(1x,a)',' iz           src             Q'
    print '(i4,2g14.6)',(iz,src0(1,1,iz),q0(1,1,iz), iz=1,mz)
  end if
END

!***********************************************************************
SUBROUTINE test_transfer_periodic_x
!
!  The test assumes a periodic source function, with period m.  So,
!  in order to generate the proper periodic closures, we need to have
!  mx+2 points in the solver, wrapping around so we get qfwd(mz) and
!  qrev(1) from inputting (zero) into qfwd(0) and qrev(mx+1).
!
!-----------------------------------------------------------------------
  implicit none
  integer                   :: mx, my, mz, lpy, lpz
  real, allocatable, dimension(:,:,:) :: dtau, src, qq, afwd, arev
  integer ix, i
  real, parameter:: pi=3.14159265
!-----------------------------------------------------------------------
  mx=10
  my=11
  mz=12

  allocate (dtau(mx,my,mz), src(mx,my,mz), qq(mx,my,mz), afwd(mx,my,mz), arev(mx,my,mz))

  dtau = 1
  do i=0,2,2
    do ix=1,mx
      src(ix,:,:) = 1.-cos((ix-0.5+i)*2.*pi/mx)
    end do
    lpy = 0
    lpz = 0
    call transfer_x (mx, my, mz, dtau, src, qq, afwd, arev, lpy, lpz)
  end do
END

!***********************************************************************
SUBROUTINE test_transfer_periodic_y
!
!  The test assumes a periodic source function, with period my.  So,
!  in order to generate the proper periodic closures, we need to have
!  my+2 points in the solver, wrapping around so we get qfwd(mz) and
!  qrev(1) from inputting (zero) into qfwd(0) and qrev(my+1).
!
!-----------------------------------------------------------------------
  implicit none
  integer                   :: mx, my, mz, lpx, lpz
  real, allocatable, dimension(:,:,:) :: dtau, src, qq, afwd, arev
  integer iy, i
  real, parameter:: pi=3.14159265
!-----------------------------------------------------------------------
  mx=10
  my=11
  mz=12

  allocate (dtau(mx,my,mz), src(mx,my,mz), qq(mx,my,mz), afwd(mx,my,mz), arev(mx,my,mz))

  dtau = 1
  do i=0,2,2
    do iy=1,my
      src(:,iy,:) = 1.-cos((iy-0.5+i)*2.*pi/my)
    end do
    lpx = 0
    lpz = 0
    call transfer_y (mx, my, mz, dtau, src, qq, afwd, arev, lpx, lpz)
  end do
END

!***********************************************************************
SUBROUTINE test_transfer_periodic_z
!
!  The test assumes a periodic source function, with period mz.  So,
!  in order to generate the proper periodic closures, we need to have
!  mz+2 points in the solver, wrapping around so we get qfwd(mz) and
!  qrev(1) from inputting (zero) into qfwd(0) and qrev(mz+1).
!
!-----------------------------------------------------------------------
  implicit none
  integer                   :: mx, my, mz, lpx, lpy
  real, allocatable, dimension(:,:,:) :: dtau, src, qq, afwd, arev
  integer iz, i
  real, parameter:: pi=3.14159265
!-----------------------------------------------------------------------
  mx=10
  my=11
  mz=12

  allocate (dtau(mx,my,mz), src(mx,my,mz), qq(mx,my,mz), afwd(mx,my,mz), arev(mx,my,mz))

  dtau = 1
  do i=0,2,2
    do iz=1,mz
      src(:,:,iz) = 1.-cos((iz-0.5+i)*2.*pi/mz)
    end do
    lpx = 0
    lpy = 0
    call transfer_z (mx, my, mz, dtau, src, qq, afwd, arev, lpx, lpy)
  end do
END
