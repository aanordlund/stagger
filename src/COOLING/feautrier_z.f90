!***********************************************************************
  subroutine radiation (n,n1,tau,s,q,xi,doxi)
!
!  Solve the transfer equation, given optical depth and source function.
!
!  "Steins trick" is used; storing the sum of the three elements in each
!  row of the tridiagonal equation system, instead of the diagonal ele-
!  ment.  This allows accurate solutions to be otained for arbitrarily
!  small optical depths.  Without this trick, round off errors become
!  noticeable already when the smallest optical depth is less than the
!  square root of the machine precisions.
!
!  This version works with P as a variable for k <= n1, and with
!  Q = P - S for k > n1.  P is Feautriers P and S is the source function.
!  The equation solved is  Q" = P - S for k<=n1, and Q" = Q - S" for k>n1.
!  The answer returned is Q = P - S for all k.
!
!  This choice of equations produces a high precision in Q at large optical
!  depths (where P - S would have round off), while at the same time
!  retaining the precision at small optical depths (where S" becomes very
!  large and eventually would cause round off errors in the back
!  substitution).
!
!  This is a second order version.  For simulations, with rapidly
!  varying absorption coefficients and source functions, this is to be
!  preferred over spline and Hermitean versions because it is positive
!  definite, in the sense that a positive source function is guaranteed
!  to result in a positive average intensity p.  Also, the flux
!  divergence is exactly equal to q, for the conventional definition
!  of the flux.
!
!  Operation count: 5d+5m+11a = 21 flops
!
!  Timings:
!            Alliant: 0.58 * 21 / 1.25 = 9.7 Mfl @ 31*31*31
!
!  Update history:  This routine is based on transq, with updates
!
!***********************************************************************
!
  USE params
  implicit none
!
  real, dimension(mx,my,mz) :: tau,s,q
  real, dimension(mx,my,mz) :: sp1,sp2,sp3
  real, dimension(mx,my)    :: ex,xi
  integer :: k, l, m, n, n1p, n1, n2
  real :: taum, dtau2, dinv
  logical doxi
!
  character(len=80):: id='$Id: feautrier_z.f90,v 1.2 2003/04/10 19:28:24 aake Exp $'
  if (id.ne.' ') print *,id ; id=' '
!
!  Calculate 1-exp(-tau(l,m,1)) straight or by series expansion,
!  based on max tau(l,m,1).
!
  taum=tau(1,1,1)
  do m=1,my
  do l=1,mx
    taum=amax1(taum,tau(l,m,1))
  end do
  end do
  if (taum.gt.0.1) then
    do m=1,my
    do l=1,mx
      ex(l,m)=1.-exp(-tau(l,m,1))
    end do
    end do
  else
    do m=1,my
    do l=1,mx
      ex(l,m)=tau(l,m,1) &
        *(1.-.5*tau(l,m,1)*(1.-.3333*tau(l,m,1)))
    end do
    end do
  endif
!
!  Boundary condition at k=1.
!
  do m=1,my
  do l=1,mx
    dtau2=tau(l,m,2)-tau(l,m,1)
    sp2(l,m,1)=dtau2*(1.+.5*dtau2)
    sp3(l,m,1)=-1.
    q(l,m,1)=s(l,m,1)*dtau2*(.5*dtau2+ex(l,m))
  end do
  end do
!
!  Matrix elements for k>2, k<n: [3d+3s]
!
  do k=2,n-1
  do m=1,my
  do l=1,mx
    dinv=2./(tau(l,m,k+1)-tau(l,m,k-1))
    sp1(l,m,k)=dinv/(tau(l,m,k-1)-tau(l,m,k))
    sp2(l,m,k)=1.
    sp3(l,m,k)=dinv/(tau(l,m,k)-tau(l,m,k+1))
  end do
  end do
  end do
!
!  Right hand sides, for k>2, k<n1
!
  n1p=min0(max0(n1,2),n-3)
  do k=2,n1p-1
  do m=1,my
  do l=1,mx
    q(l,m,k)=s(l,m,k)
  end do
  end do
  end do
!
!  RHS for k=n1p.  The equation is still in p(k), but q(k+1) is now
!  q(k+1)=p(k+1)-s(k+1), so to get p(k+1) we have to add s(k+1) to the
!  LHS, that is subtract it from the RHS.
!
  if (n1p.le.n) then
    do m=1,my
    do l=1,mx
      q(l,m,n1p)=s(l,m,n1p)-sp3(l,m,n1p)*s(l,m,n1p+1)
    end do
    end do
  endif
!
!  RHS for k=n1p+1.  The equation is now in Q: Q" = Q - S", but q(k-1)
!  is now p(k-1)=q(k-1)+s(k-1), so we must add sp1*s(l,m,k-1) to the RHS,
!  relative to the case below.
!
  if (n1p+1.le.n) then
    do m=1,my
    do l=1,mx
      q(l,m,n1p+1)=sp1(l,m,n1p+1)*s(l,m,n1p+1) &
                 +sp3(l,m,n1p+1)*(s(l,m,n1p+1)-s(l,m,n1p+2))
    end do
    end do
  endif
!
!  k>n1p+1,<n [2m+2a]
!
  do k=n1p+2,n-1
  do m=1,my
  do l=1,mx
    q(l,m,k)=sp1(l,m,k)*(s(l,m,k)-s(l,m,k-1)) &
            +sp3(l,m,k)*(s(l,m,k)-s(l,m,k+1))
!       0.02+0.18+0.02+0.20+0.20+0.03 -> 0.58*11/0.65 = 9.8 Mfl
  end do
  end do
  end do
!
!  k=n
!
  do m=1,my
  do l=1,mx
    sp2(l,m,n)=1.
    q(l,m,n)=0.0
  end do
  end do
!
!  Eliminate subdiagonal, save factors in sp1 [1d+2m+4a]
!
  do k=1,n-2
  do m=1,my
  do l=1,mx
    sp1(l,m,k)=-sp1(l,m,k+1)/(sp2(l,m,k)-sp3(l,m,k))
    q(l,m,k+1)=q(l,m,k+1)+sp1(l,m,k)*q(l,m,k)
    sp2(l,m,k+1)=sp2(l,m,k+1)+sp1(l,m,k)*sp2(l,m,k)
    sp2(l,m,k)=sp2(l,m,k)-sp3(l,m,k)
!         0.14+0.13+0.07+0.09 sec -> 0.58*7/0.43 = 9.4 Mfl
  end do
  end do
  end do
  do m=1,my
  do l=1,mx
    sp2(l,m,n-1)=sp2(l,m,n-1)-sp3(l,m,n-1)
  end do
  end do
!
!  Backsubstitute [1d+1m+1a]
!
  do k=n-1,1,-1
  do m=1,my
  do l=1,mx
    q(l,m,k)=(q(l,m,k)-sp3(l,m,k)*q(l,m,k+1))/sp2(l,m,k)
!         0.19 sec = 580444*3/0.19 = 9.2 Mfl
  end do
  end do
  end do
!
!  Subtract S in the region where the equation is in P
!
  do k=1,n1p
    do m=1,my
    do l=1,mx
      q(l,m,k)=q(l,m,k)-s(l,m,k)
    end do
    end do
  end do
!
!  Surface intensity.
!
  if (.not.doxi) return
  do l=1,mx
  do m=1,my
    xi(l,m)=(1.-ex(l,m))*(q(l,m,1)+s(l,m,1)) &
         +s(l,m,1)*0.5*ex(l,m)**2
  end do
  end do
!
  end
