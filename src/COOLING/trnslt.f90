!-------------------------------------------------------------------------------
module trnslt_m
  implicit none
  integer(kind=8), save :: n_call=0
  real(kind=8), save :: c_trnslt = 0.
end module trnslt_m


!-------------------------------------------------------------------------------
subroutine trnslt (fin,fout,dxdy,dzdy,do_exp,ny,mblocks,verbose,do_tsc)

  use params
  implicit none

  real, dimension(mx,my,mz), intent(in) :: fin
  real, dimension(mx,my,mz), intent(out):: fout
  integer :: ny
  integer :: mblocks !DEPRECATED
  real :: dxdy, dzdy
  logical :: do_exp, verbose, do_tsc
  logical :: omp_in_parallel
  character(len=mid) :: id = 'trnslt'

  call print_id (id)
  if (omp_in_parallel()) then
     !$omp barrier
     if (do_tsc) then
        call trnslt_tsc_omp (fin,fout,dxdy,dzdy,do_exp,ny)
     else
        call trnslt_omp (fin,fout,dxdy,dzdy,do_exp,ny)
     end if
     !$omp barrier
  else
     !$omp parallel shared(dxdy,dzdy,do_exp,ny)
     if (do_tsc) then
        call trnslt_tsc_omp (fin,fout,dxdy,dzdy,do_exp,ny)
     else
        call trnslt_omp (fin,fout,dxdy,dzdy,do_exp,ny)
     end if
     !$omp end parallel
  end if
end subroutine trnslt


!-------------------------------------------------------------------------------
subroutine trnslt_omp (fin,fout,dxdy,dzdy,do_exp,ny)

  !  Translate a scalar field to an inclined coordinate system.
  !
  !  Operation count:  8m+10a = 18 flops/pnt
  !                    8m+6a  = 14 flops/pnt

  use params
  use trnslt_m
  implicit none

  real, dimension(mx,my,mz), intent(in) :: fin
  real, dimension(mx,my,mz), intent(out):: fout

  real :: dxdy, dzdy
  logical :: do_exp
  integer :: ny
  real, dimension(mx):: ftmp
  real, dimension(mx,mz):: f, g
  integer :: ix, iy, iz, k, lm1, lp0, lp1, lp2, mm1, mp0, mp1, mp2
  integer :: lm1a, lm0a, lp0a, lp1a, lp2a
  real :: p, q, ad, bd, ac, bc, af, bf, xk, zk, wct, wallclock

  wct = wallclock()


  !  Trap vertical rays

  if (abs(dxdy).lt.1e-6.and.abs(dzdy).lt.1.e-6) then
     if (do_exp) then
        do iz=izs,ize
           fout(:,:,iz) = exp(fin(:,:,iz))
        end do
     else
        do iz=izs,ize
           fout(:,:,iz) = fin(:,:,iz)
        end do
     end if
     !$omp single
     c_trnslt = c_trnslt + (wallclock()-wct)
     n_trnslt = n_trnslt + mx*ny*mz
     n_call   = n_call   + 1
     !$omp end single
     return
  end if

  !  y-loop
  if (abs(dxdy).lt.1e-6) then
     !$omp barrier
     do iz=izs,ize
        do iy=1,ny
           zk=dzdy*ym(iy)/sz
           zk=amod(zk,1.)
           if(zk.lt.0.) zk=zk+1.
           zk=mz*zk
           k=zk
           p=zk-k
           k=k+mz
           q=1.-p
           af=q+p*q*(q-p)
           bf=p-p*q*(q-p)
           ad=p*q*q*0.5
           bd=-p*q*p*0.5
           ac=af-bd
           bc=bf+ad

           !  Interpolate using cubic splines [4m+5a] [4m+3a]
           mm1 = mod(iz+k-2,mz)+1
           mp0 = mod(iz+k-1,mz)+1
           mp1 = mod(iz+k  ,mz)+1
           mp2 = mod(iz+k+1,mz)+1
           if (do_exp) then
              do ix=1,mx
                 ftmp(ix) = ac*fin(ix,iy,mp0) + bc*fin(ix,iy,mp1) &
                      - ad*fin(ix,iy,mm1) + bd*fin(ix,iy,mp2)
              end do
              call expn (mx,ftmp,fout(1,iy,iz))
           else
              do ix=1,mx
                 fout(ix,iy,iz) = ac*fin(ix,iy,mp0) + bc*fin(ix,iy,mp1) &
                      - ad*fin(ix,iy,mm1) + bd*fin(ix,iy,mp2)
              end do
           end if
        end do
     end do
     !$omp single
     c_trnslt = c_trnslt + (wallclock()-wct)
     n_trnslt = n_trnslt + mx*ny*mz
     n_call   = n_call   + 1
     !$omp end single
     return
  else if (abs(dzdy).lt.1e-6) then
     !$omp barrier                                              ! NOT NEEDEED ?
     if (idbg > 2) print *,'trnslt: only x', omp_mythread, izs, ize
     do iz=izs,ize
        do iy=1,ny
           xk=dxdy*ym(iy)/sx
           xk=amod(xk,1.)
           if(xk.lt.0.) xk=xk+1.
           xk=mx*xk
           k=xk
           p=xk-k
           k=k+mx
           q=1.-p
           af=q+p*q*(q-p)
           bf=p-p*q*(q-p)
           ad=p*q*q*0.5
           bd=-p*q*p*0.5
           ac=af-bd
           bc=bf+ad

           !  Interpolate using cubic splines [4m+5a] [4m+3a]
           lm1 = 1+k-1; if (lm1.lt.1) lm1=lm1+mx; if (lm1.gt.mx) lm1=lm1-mx
           if (do_exp) then
              do ix=1,mx
                 lp0 = lm1+1 ; if (lp0.gt.mx) lp0=lp0-mx
                 lp1 = lp0+1 ; if (lp1.gt.mx) lp1=lp1-mx
                 lp2 = lp1+1 ; if (lp2.gt.mx) lp2=lp2-mx
                 lm1a = k+ix-2+10*mx
                 !          lm1a = mod(lm1,mx)+1  ; if (lm1a .ne. lm1) print*,'lm1 error',lm1,lm1a
                 !          lp0a = mod(lm1+1,mx)+1; if (lp0a .ne. lp0) print*,'lp0 error',lp0,lp0a
                 !          lp1a = mod(lm1+2,mx)+1; if (lp1a .ne. lp1) print*,'lp1 error',lp1,lp1a
                 !          lp2a = mod(lm1+3,mx)+1; if (lp2a .ne. lp2) print*,'lp2 error',lp2,lp2a
                 ftmp(ix)      =(ac*fin(lp0,iy,iz)+bc*fin(lp1,iy,iz)- &
                      ad*fin(lm1,iy,iz)+bd*fin(lp2,iy,iz))
                 lm1 = lm1+1 ; if (lm1.gt.mx) lm1=lm1-mx
              end do
              call expn (mx,ftmp,fout(1,iy,iz))
           else
              do ix=1,mx
                 lp0 = lm1+1 ; if (lp0.gt.mx) lp0=lp0-mx
                 lp1 = lp0+1 ; if (lp1.gt.mx) lp1=lp1-mx
                 lp2 = lp1+1 ; if (lp2.gt.mx) lp2=lp2-mx
                 fout(ix,iy,iz)=ac*fin(lp0,iy,iz)+bc*fin(lp1,iy,iz)- &
                      ad*fin(lm1,iy,iz)+bd*fin(lp2,iy,iz)
                 lm1 = lm1+1 ; if (lm1.gt.mx) lm1=lm1-mx
              end do
           end if
        end do
     end do
     !$omp single
     c_trnslt = c_trnslt + (wallclock()-wct)
     n_trnslt = n_trnslt + mx*ny*mz
     n_call   = n_call   + 1
     !$omp end single
     return
  else
     !$omp barrier
     do iy=1,ny
        do iz=izs,ize
           zk=dzdy*ym(iy)/sz
           zk=amod(zk,1.)
           if(zk.lt.0.) zk=zk+1.
           zk=mz*zk
           k=zk
           p=zk-k
           k=k+mz
           q=1.-p
           af=q+p*q*(q-p)
           bf=p-p*q*(q-p)
           ad=p*q*q*0.5
           bd=-p*q*p*0.5
           ac=af-bd
           bc=bf+ad

           !  Interpolate using cubic splines [4m+5a] [4m+3a]
           mm1 = mod(iz+k-2,mz)+1
           mp0 = mod(iz+k-1,mz)+1
           mp1 = mod(iz+k  ,mz)+1
           mp2 = mod(iz+k+1,mz)+1
           do ix=1,mx
              fout(ix,iy,iz) = ac*fin(ix,iy,mp0) + bc*fin(ix,iy,mp1) &
                   - ad*fin(ix,iy,mm1) + bd*fin(ix,iy,mp2)
           end do
        end do
     end do
     do iz=izs,ize
        do iy=1,ny
           xk=dxdy*ym(iy)/sx
           xk=amod(xk,1.)
           if(xk.lt.0.) xk=xk+1.
           xk=mx*xk
           k=xk
           p=xk-k
           k=k+mx
           q=1.-p
           af=q+p*q*(q-p)
           bf=p-p*q*(q-p)
           ad=p*q*q*0.5
           bd=-p*q*p*0.5
           ac=af-bd
           bc=bf+ad

           !  Interpolate using cubic splines [4m+5a] [4m+3a]
           lm1 = 1+k-1 ; if (lm1.lt.1) lm1=lm1+mx; if (lm1.gt.mx) lm1=lm1-mx
           do ix=1,mx
              lp0 = lm1+1 ; if (lp0.gt.mx) lp0=lp0-mx
              lp1 = lp0+1 ; if (lp1.gt.mx) lp1=lp1-mx
              lp2 = lp1+1 ; if (lp2.gt.mx) lp2=lp2-mx
              ftmp(ix)=ac*fout(lp0,iy,iz)+bc*fout(lp1,iy,iz)- &
                   ad*fout(lm1,iy,iz)+bd*fout(lp2,iy,iz)
              lm1 = lm1+1 ; if (lm1.gt.mx) lm1=lm1-mx
           end do
           if (do_exp) then
              call expn (mx,ftmp,fout(1,iy,iz))
           else
              do ix=1,mx
                 fout(ix,iy,iz) = ftmp(ix)
              end do
           end if
        end do
     end do
  end if
  !$omp single
  c_trnslt = c_trnslt + (wallclock()-wct)
  n_trnslt = n_trnslt + mx*ny*mz
  n_call   = n_call   + 1
  !$omp end single

end subroutine trnslt_omp


!-------------------------------------------------------------------------------
subroutine trnslt_tsc_omp (fin,fout,dxdy,dzdy,do_exp,ny)

  !  Translate a scalar field to an inclined coordinate system.
  !
  !  Operation count:  8m+10a = 18 flops/pnt
  !                    8m+6a  = 14 flops/pnt

  use params
  use trnslt_m
  implicit none

  real, dimension(mx,my,mz), intent(in) :: fin
  real, dimension(mx,my,mz), intent(out):: fout
  real :: dxdy, dzdy
  logical :: do_exp
  integer :: ny
  real, dimension(mx) :: ftmp
  real, dimension(mx,mz) :: f, g
  integer :: ix, iy, iz, k, lm1, lp0, lp1, lp2, mm1, mp0, mp1, mp2
  integer :: lm1a, lm0a, lp0a, lp1a, lp2a
  real :: p, q, ad, bd, ac, bc, af, bf, xk, zk, wct, wallclock
  real :: wm1,wp0,wp1


  wct = wallclock()

  !  Trap vertical rays
  if (abs(dxdy).lt.1e-6.and.abs(dzdy).lt.1.e-6) then
     if (do_exp) then
        do iz=izs,ize
           fout(:,:,iz) = exp(fin(:,:,iz))
        end do
     else
        do iz=izs,ize
           fout(:,:,iz) = fin(:,:,iz)
        end do
     end if
     !$omp single
     c_trnslt = c_trnslt + (wallclock()-wct)
     n_trnslt = n_trnslt + mx*ny*mz
     n_call   = n_call   + 1
     !$omp end single
     return
  end if

  !  y-loop
  if (abs(dxdy).lt.1e-6) then
     !$omp barrier
     do iz=izs,ize
        do iy=1,ny
           zk=dzdy*ym(iy)/sz
           zk=amod(zk,1.)
           if(zk.lt.0.) zk=zk+1.
           zk=mz*zk
           k=zk+0.5
           p=zk-k
           k=k+mz
           wm1 = 0.5*(1.5-(1+p))**2
           wp0 = 0.75-p**2
           wp1 = 0.5*(1.5-(1-p))**2

           !  Interpolate using cubic splines [4m+5a] [4m+3a]
           mm1 = mod(iz+k-2,mz)+1
           mp0 = mod(iz+k-1,mz)+1
           mp1 = mod(iz+k  ,mz)+1
           if (do_exp) then
              do ix=1,mx
                 ftmp(ix) = wp0*fin(ix,iy,mp0) + wp1*fin(ix,iy,mp1) + wm1*fin(ix,iy,mm1)
              end do
              call expn (mx,ftmp,fout(1,iy,iz))
           else
              do ix=1,mx
                 fout(ix,iy,iz) = wp0*fin(ix,iy,mp0) + wp1*fin(ix,iy,mp1) + wm1*fin(ix,iy,mm1)
              end do
           end if
        end do
     end do
     !$omp single
     c_trnslt = c_trnslt + (wallclock()-wct)
     n_trnslt = n_trnslt + mx*ny*mz
     n_call   = n_call   + 1
     !$omp end single
     return
  else if (abs(dzdy).lt.1e-6) then
     !$omp barrier                                              ! NOT NEEDEED ?
     if (idbg > 2) print *,'trnslt: only x', omp_mythread, izs, ize
     do iz=izs,ize
        do iy=1,ny
           xk=dxdy*ym(iy)/sx
           xk=amod(xk,1.)
           if(xk.lt.0.) xk=xk+1.
           xk=mx*xk
           k=xk+0.5
           p=xk-k
           k=k+mx
           wm1 = 0.5*(1.5-(1+p))**2
           wp0 = 0.75-p**2
           wp1 = 0.5*(1.5-(1-p))**2

           !  Interpolate using cubic splines [4m+5a] [4m+3a]
           lm1 = 1+k-1; if (lm1.lt.1) lm1=lm1+mx; if (lm1.gt.mx) lm1=lm1-mx
           if (do_exp) then
              do ix=1,mx
                 lp0 = lm1+1 ; if (lp0.gt.mx) lp0=lp0-mx
                 lp1 = lp0+1 ; if (lp1.gt.mx) lp1=lp1-mx
                 !          lm1a = mod(lm1,mx)+1  ; if (lm1a .ne. lm1) print*,'lm1 error',lm1,lm1a
                 !          lp0a = mod(lm1+1,mx)+1; if (lp0a .ne. lp0) print*,'lp0 error',lp0,lp0a
                 !          lp1a = mod(lm1+2,mx)+1; if (lp1a .ne. lp1) print*,'lp1 error',lp1,lp1a
                 !          lp2a = mod(lm1+3,mx)+1; if (lp2a .ne. lp2) print*,'lp2 error',lp2,lp2a
                 ftmp(ix) = wp0*fin(lp0,iy,iz) + wp1*fin(lp1,iy,iz) + wm1*fin(lm1,iy,iz)
                 lm1 = lm1+1 ; if (lm1.gt.mx) lm1=lm1-mx
              end do
              call expn (mx,ftmp,fout(1,iy,iz))
           else
              do ix=1,mx
                 lp0 = lm1+1 ; if (lp0.gt.mx) lp0=lp0-mx
                 lp1 = lp0+1 ; if (lp1.gt.mx) lp1=lp1-mx
                 lp2 = lp1+1 ; if (lp2.gt.mx) lp2=lp2-mx
                 fout(ix,iy,iz) = wp0*fin(lp0,iy,iz) + wp1*fin(lp1,iy,iz) + wm1*fin(lm1,iy,iz)
                 lm1 = lm1+1 ; if (lm1.gt.mx) lm1=lm1-mx
              end do
           end if
        end do
     end do
     !$omp single
     c_trnslt = c_trnslt + (wallclock()-wct)
     n_trnslt = n_trnslt + mx*ny*mz
     n_call   = n_call   + 1
     !$omp end single
     return
  else
     !$omp barrier
     do iy=1,ny
        do iz=izs,ize
           zk=dzdy*ym(iy)/sz
           zk=amod(zk,1.)
           if(zk.lt.0.) zk=zk+1.
           zk=mz*zk
           k=zk+0.5
           p=zk-k
           k=k+mz
           wm1 = 0.5*(1.5-(1+p))**2
           wp0 = 0.75-p**2
           wp1 = 0.5*(1.5-(1-p))**2

           !  Interpolate using cubic splines [4m+5a] [4m+3a]
           mm1 = mod(iz+k-2,mz)+1
           mp0 = mod(iz+k-1,mz)+1
           mp1 = mod(iz+k  ,mz)+1
           do ix=1,mx
              fout(ix,iy,iz) = wp0*fin(ix,iy,mp0) + wp1*fin(ix,iy,mp1) + wm1*fin(ix,iy,mm1)
           end do
        end do
     end do
     do iz=izs,ize
        do iy=1,ny
           xk=dxdy*ym(iy)/sx
           xk=amod(xk,1.)
           if(xk.lt.0.) xk=xk+1.
           xk=mx*xk
           k=xk+0.5
           p=xk-k
           k=k+mx
           wm1 = 0.5*(1.5-(1+p))**2
           wp0 = 0.75-p**2
           wp1 = 0.5*(1.5-(1-p))**2

           !  Interpolate using cubic splines [4m+5a] [4m+3a]
           lm1 = 1+k-1 ; if (lm1.lt.1) lm1=lm1+mx; if (lm1.gt.mx) lm1=lm1-mx
           do ix=1,mx
              lp0 = lm1+1 ; if (lp0.gt.mx) lp0=lp0-mx
              lp1 = lp0+1 ; if (lp1.gt.mx) lp1=lp1-mx
              ftmp(ix) = wp0*fout(lp0,iy,iz) + wp1*fout(lp1,iy,iz) + wm1*fout(lm1,iy,iz)
              lm1 = lm1+1 ; if (lm1.gt.mx) lm1=lm1-mx
           end do
           if (do_exp) then
              call expn (mx,ftmp,fout(1,iy,iz))
           else
              do ix=1,mx
                 fout(ix,iy,iz) = ftmp(ix)
              end do
           end if
        end do
     end do
  end if
  !$omp single
  c_trnslt = c_trnslt + (wallclock()-wct)
  n_trnslt = n_trnslt + mx*ny*mz
  n_call   = n_call   + 1
  !$omp end single

end subroutine trnslt_tsc_omp

!-------------------------------------------------------------------------------
subroutine trnslt_end
  use trnslt_m
  use params
  implicit none
  print *, 'trnslt: ns/pt =',c_trnslt*1e9/n_trnslt,n_call/(3*(it-1))
end subroutine trnslt_end

!-------------------------------------------------------------------------------
subroutine trnslt_test (do_tsc)

  use params
  implicit none

  logical :: do_tsc
  real, allocatable, dimension(:,:,:) :: fin,fout,fcmp
  integer :: count,ix,iy,iz
  integer :: m(3)
  real :: cpux,cpue,dxdy,dzdy,fdtime,k,kx,ky,kz,dsdy
  logical, save :: done=.false.

  if (done) return
  done = .true.

  allocate (fin(mx,my,mz),fout(mx,my,mz),fcmp(mx,my,mz))

  k = 2.*pi/mx
  kx = 2.*pi/sx
  ky = 2.*pi/sy
  kz = 2.*pi/sz

  !  Testing x-direction
  dsdy = 1.
  dxdy = dsdy
  dzdy = 0.
  !$omp parallel private(iz)
  do iz=izs,ize
     do iy=1,my
        do ix=1,mx
           fin (ix,iy,iz) = cos( xm(ix)             *kx)+cos(ym(iy)*ky)+cos(zm(iz)*kz)
           fcmp(ix,iy,iz) = cos((xm(ix)+dxdy*ym(iy))*kx)+cos(ym(iy)*ky)+cos(zm(iz)*kz)
        end do
     end do
  end do
  !$omp end parallel

  print*,'1 sec timing loop'
  cpue = fdtime()
  cpue = 0.
  count = 0
  do while (cpue .lt. 1.0)
     call trnslt(fin,fout,dxdy,dzdy,.true.,my,1,.false.,do_tsc)
     cpue = cpue+fdtime()
     count = count+1
  end do
  cpue = cpue/count

  cpux = 0.
  count = 0
  do while (cpux .lt. 1.0)
     call trnslt(fin,fout,dxdy,dzdy,.false.,my,1,.false.,do_tsc)
     cpux = cpux+fdtime()
     count = count+1
  end do
  cpux = cpux/count

  print *,'trnslt-x-lin: ns/pt, error =',1e9*cpux/real(mx*my*mz),maxval(abs(fout-fcmp))
  print *,'trnslt-x-exp: ns/pt, error =',1e9*cpue/real(mx*my*mz),maxval(abs(fout-fcmp))

  !  Testing z-direction

  dxdy = 0.
  dzdy = dsdy
  !$omp parallel private(iz)
  do iz=izs,ize
     do iy=1,my
        do ix=1,mx
           fin(ix,iy,iz) = cos(xm(ix)*kx)+cos(ym(iy)*ky)+cos(zm(iz)*kz)
           fcmp(ix,iy,iz) = cos(xm(ix)*kx)+cos(ym(iy)*ky)+cos((zm(iz)+dzdy*ym(iy))*kz)
        end do
     end do
  end do
  !$omp end parallel

  cpue = 0.
  count = 0
  do while (cpue .lt. 1.0)
     call trnslt(fin,fout,dxdy,dzdy,.true.,my,1,.false.,do_tsc)
     cpue = cpue+fdtime()
     count = count+1
  end do
  cpue = cpue/count

  cpux = fdtime()
  cpux = 0.
  count = 0
  do while (cpux .lt. 1.0)
     call trnslt(fin,fout,dxdy,dzdy,.false.,my,1,.false.,do_tsc)
     cpux = cpux+fdtime()
     count = count+1
  end do
  cpux = cpux/count

  print *,'trnslt-z-lin: ns/pt, error =',1e9*cpux/real(mx*my*mz),maxval(abs(fout-fcmp))
  print *,'trnslt-z-exp: ns/pt, error =',1e9*cpue/real(mx*my*mz),maxval(abs(fout-fcmp))

  !  Testing xz-direction

  dxdy = dsdy
  dzdy = dsdy
  !$omp parallel private(iz)
  do iz=izs,ize
     do iy=1,my
        do ix=1,mx
           fin(ix,iy,iz) = cos(xm(ix)*kx)+cos(ym(iy)*ky)+cos(zm(iz)*kz)
           fcmp(ix,iy,iz) = cos((xm(ix)+dxdy*ym(iy))*kx)+cos(ym(iy)*ky)+cos((zm(iz)+dzdy*ym(iy))*kz)
        end do
     end do
  end do
  !$omp end parallel

  cpue = 0.
  count = 0
  do while (cpue .lt. 1.0)
     call trnslt(fin,fout,dxdy,dzdy,.true.,my,1,.false.,do_tsc)
     cpue = cpue+fdtime()
     count = count+1
  end do
  cpue = cpue/count

  cpux = fdtime()
  cpux = 0.
  count = 0
  do while (cpux .lt. 1.0)
     call trnslt(fin,fout,dxdy,dzdy,.false.,my,1,.false.,do_tsc)
     cpux = cpux+fdtime()
     count = count+1
  end do
  cpux = cpux/count

  print *,'trnslt-xz-lin: ns/pt, error =',1e9*cpux/real(mx*my*mz),maxval(abs(fout-fcmp))
  print *,'trnslt-xz-exp: ns/pt, error =',1e9*cpue/real(mx*my*mz),maxval(abs(fout-fcmp))

  deallocate (fin,fout,fcmp)

end subroutine trnslt_test
