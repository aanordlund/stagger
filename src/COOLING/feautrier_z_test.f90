!
!  Test program for Feautrier solution in the y-direction
!
 USE params

 implicit none
 real, allocatable, dimension(:,:,:):: s, tau, q, q0
 real, allocatable, dimension(:,:):: xi
 integer :: n1, iz, niter
 real :: logtaumin, logtaumax, dlogtau, logtau, cput, cpu(2), dtime
 logical doxi

 mx=100
 my=mx
 mz=45
 doxi=.true.

 allocate (s(mx,my,mz), tau(mx,my,mz), q(mx,my,mz), q0(mx,my,mz), xi(mx,my))

 s=1.
 logtaumin=-4.
 logtaumax=3.
 dlogtau=(logtaumax-logtaumin)/(mz-1)
 do iz=1,mz
   logtau=logtaumin+dlogtau*(iz-1)
   tau(:,:,iz)=10.**logtau
   !s(:,:,iz)=tau(:,:,iz)+0.01*tau(:,:,iz)**2*(1.+cos(logtau*pi))
   s(:,:,iz)=tau(:,:,iz)
   q0(:,:,iz)=0.5*exp(-tau(:,:,iz))
 end do

 n1 = mz/2
 cput=dtime(cpu)
 call radiation (mz,n1,tau,s,q,xi,doxi)
 cput=dtime(cpu)
 niter=1.+1./(cput+1e-2)                ! number of iterations for 1 second CPU

 cput=dtime(cpu)
 do iz=1,niter
   call radiation (mz,n1,tau,s,q,xi,doxi)
 end do
 cput=dtime(cpu)

 print *,' iz  log(tau)         S           P           Q          err'
 do iz=1,mz
   print '(i4,4e12.4,f12.6)',iz, alog10(tau(1,1,iz)), s(1,1,iz), s(1,1,iz)+q(1,1,iz), q(1,1,iz), q(1,1,iz)-q0(1,1,iz)
 end do
 print *,'cpu =',cput
 print *,'mus/pt =',cput*1e6/(mx*my*mz*niter)

 END

