! $Id: transfer_integral.f90,v 1.10 2013/08/02 12:14:32 aake Exp $
!*******************************************************************************
MODULE transfer_integral_m
  USE params, only: mx, my, mz
  implicit none
  real, save :: cpu1=0.0, cpu2=0.0
  real, save :: dtau0=0.005, dtau1=0.05, dtau2=15.
CONTAINS
!-------------------------------------------------------------------------------
  SUBROUTINE vector_method (iz, np, s, dtau, ex0, ex1, ex2, ds1dtau, dsdtau, d2sdtau2)
    implicit none
    real, dimension(mx,my,mz) :: dtau, s
    real, dimension(mx,my) :: ex0, ex1, ex2, ds1dtau, dsdtau, d2sdtau2
    integer np, ix, iy, iz
    real, parameter:: dtau1=1e-3
    real small
!...............................................................................
    do iy=2,np
      do ix=1,mx
       ds1dtau(ix,iy) = (s(ix,iy,iz)-s(ix,iy-1,iz))/dtau(ix,iy,iz)
        small = dtau1/(dtau1+dtau(ix,iy,iz)**2)
        small = small*small
        small = small*small
        ex0(ix,iy) = exp(-dtau(ix,iy,iz))
        ex1(ix,iy) = small*dtau(ix,iy,iz)+(1.-small)*(1.-ex0(ix,iy))
        ex2(ix,iy) = ex1(ix,iy)-dtau(ix,iy,iz)*ex0(ix,iy)
      end do
    end do
    do ix=1,mx
      ex1(ix, 1) = dtau(ix, 1,iz)
      ex0(ix, 1) = 1.-ex1(ix, 1)
      ex2(ix, 1) = ex1(ix, 1)-dtau(ix, 1,iz)*ex0(ix, 1)
    end do
  END SUBROUTINE
!-------------------------------------------------------------------------------
  SUBROUTINE if_method (iz, np, s, dtau, ex0, ex1, ex2, ds1dtau, dsdtau, d2sdtau2)
    implicit none
    real, dimension(mx,my,mz) :: dtau, s
    real, dimension(mx,my) :: ex0, ex1, ex2, ds1dtau, dsdtau, d2sdtau2
    integer np, ix, iy, iz
!...............................................................................
    do iy=2,np
      do ix=1,mx
        ds1dtau(ix,iy) = (s(ix,iy,iz)-s(ix,iy-1,iz))/dtau(ix,iy,iz)
        if      (dtau(ix,iy,iz) .gt. dtau2) then
          ex0(ix,iy) = 0.
          ex1(ix,iy) = 1.
        else if (dtau(ix,iy,iz) .gt. dtau1) then
          ex0(ix,iy) = exp(-dtau(ix,iy,iz))
          ex1(ix,iy) = 1.-ex0(ix,iy)
        else if (dtau(ix,iy,iz) .gt. dtau0) then
          ex1(ix,iy) = dtau(ix,iy,iz)*(1.-0.5*dtau(ix,iy,iz)*(1.-0.333333*dtau(ix,iy,iz)))
          ex0(ix,iy) = 1.-ex1(ix,iy)
        else
          ex1(ix,iy) = dtau(ix,iy,iz)*(1.-0.5*dtau(ix,iy,iz))
          ex0(ix,iy) = 1.-ex1(ix,iy)
        end if
        ex2(ix,iy) = ex1(ix,iy)-dtau(ix,iy,iz)*ex0(ix,iy)
      end do
    end do
    do ix=1,mx
      if      (dtau(ix,1,iz) .gt. dtau2) then
        ex0(ix,1) = 0.
        ex1(ix,1) = 1.
      else if (dtau(ix,1,iz) .gt. dtau1) then
        ex0(ix,1) = exp(-dtau(ix,1,iz))
        ex1(ix,1) = 1.-ex0(ix,1)
      else if (dtau(ix,1,iz) .gt. dtau0) then
        ex1(ix,1) = dtau(ix,1,iz)*(1.-0.5*dtau(ix,1,iz)*(1.-0.333333*dtau(ix,1,iz)))
        ex0(ix,1) = 1.-ex1(ix,1)
      else
        ex1(ix,1) = dtau(ix,1,iz)*(1.-0.5*dtau(ix,1,iz))
        ex0(ix,1) = 1.-ex1(ix,1)
      end if
      ex2(ix,1) = ex1(ix,1)-dtau(ix,1,iz)*ex0(ix,1)
    end do
  END SUBROUTINE
!-------------------------------------------------------------------------------
  SUBROUTINE transfer_timer (np, s, dtau)
    implicit none
    real, dimension(mx,my,mz) :: s, dtau
    real, dimension(mx,my) :: ex0, ex1, ex2, ds1dtau, dsdtau, d2sdtau2
    integer np, ix, iy, irep, nrep
    real fdtime, fetime, cpu0, small, speed
!...............................................................................
    !$omp barrier
    !$omp master
    cpu0 = fetime()
    nrep=0
    do while (cpu1 < 1.)
      nrep=nrep+1
      call vector_method (1, np, s, dtau, ex0, ex1, ex2, ds1dtau, dsdtau, d2sdtau2)
      cpu1 = fetime()-cpu0
    end do
    cpu1 = fdtime()
    do irep=1,nrep
      call vector_method (1, np, s, dtau, ex0, ex1, ex2, ds1dtau, dsdtau, d2sdtau2)
    end do
    cpu1 = fdtime()
 
    do irep=1,nrep
      call if_method (1, np, s, dtau, ex0, ex1, ex2, ds1dtau, dsdtau, d2sdtau2)
    end do
    cpu2 = fdtime()
    print*,'radiation_integral: times =',cpu1,cpu2
 
    if (cpu1 < cpu2) then
      print*,'radiation_integral: choosing vector method'
      speed = cpu1
    else
      print*,'radiation_integral: choosing if method'
      speed = cpu1
    end if
    print'(1x,a,f5.1,a)','speed =',speed*1e9/(real(nrep)*real(mx)*real(np)),' nano-sec/pt'
    !$omp end master
    !$omp barrier
  END SUBROUTINE
END MODULE

!***********************************************************************
SUBROUTINE transfer (np,n1,dtau,s,q,xi,do_xi)
  USE params
  implicit none
  integer np,n1
  real, dimension(mx,my,mz) :: dtau,s,q
  real, dimension(mx,mz)    :: xi
  logical do_xi, omp_in_parallel

  if (omp_in_parallel()) then
    call transfer_omp (np,n1,dtau,s,q,xi,do_xi)
  else
    !$omp parallel
    call transfer_omp (np,n1,dtau,s,q,xi,do_xi)
    !$omp end parallel
  end if
END

!***********************************************************************
SUBROUTINE transfer_omp (np,n1,dtau,s,q,xi,do_xi)
  USE params, only: mx, my, mz, izs, ize
  USE transfer_integral_m
  implicit none
!
  real, dimension(mx,my,mz) :: dtau,s,q
  real, dimension(mx,mz)    :: xi
  real, dimension(mx,my)    :: ex0, ex1, ex2, ds1dtau, dsdtau, d2sdtau2, dsdtaup, dsdtaum
  real, dimension(mx,my)    :: qplus, qminus
  real, dimension(mx)       :: ex, sw
  real :: taum, ctau, small, f
  integer :: ix, iy, iz, np, n1, irep, nrep
  logical do_xi
!
  character(len=80):: id='$Id: transfer_integral.f90,v 1.10 2013/08/02 12:14:32 aake Exp $'
!
!  Choose method
!
  if (cpu1 == 0.) then
    call transfer_timer (np, s, dtau)
  end if
!
!  Calculate 1-exp(-dtau(:,1,iz)) straight or by series expansion, with smooth switch (sw)
!
  do iz=izs,ize
    sw(:)=1.-exp(-dtau(:,1,iz)*10.)
    ex(:)=1.-exp(-dtau(:,1,iz))
    ex(:)=(1.-sw(:))*dtau(:,1,iz)*(1.-.5*dtau(:,1,iz)*(1.-.3333*dtau(:,1,iz)))+sw(:)*ex(:)
!
!  Compute factors (4a+3m+1d)
!
    if (cpu1 < cpu2) then
      call vector_method (iz, np, s, dtau, ex0, ex1, ex2, ds1dtau, dsdtau, d2sdtau2)
    else
      call if_method (iz, np, s, dtau, ex0, ex1, ex2, ds1dtau, dsdtau, d2sdtau2)
    end if
!
!  dS/dtau and d2S/dtau2 (5m+3a+1d).  Make a smooth transition from using 2nd order first
!  derivative to using one-sided linear slopes -- tested with IDL procedure.
!
    do iy=2,np-1
      do ix=1,mx
        f = dtau(ix,iy+1,iz)/dtau(ix,iy,iz)-1.
        f = (0.1*f)**2
        f = f/(1.0+f)
        ctau = 1./(dtau(ix,iy+1,iz)+dtau(ix,iy,iz))
        dsdtau(ix,iy) = (ds1dtau(ix,iy+1)*dtau(ix,iy,iz)+ds1dtau(ix,iy)*dtau(ix,iy+1,iz))*ctau
        dsdtaum(ix,iy) = (1.-f)*dsdtau(ix,iy) + f*ds1dtau(ix,iy)
        dsdtaup(ix,iy) = (1.-f)*dsdtau(ix,iy) + f*ds1dtau(ix,iy+1)
        d2sdtau2(ix,iy) = (ds1dtau(ix,iy+1)-ds1dtau(ix,iy))*2.*ctau
      end do
    end do
    dsdtau(:,1 ) = 0.; dsdtaup(:,1 ) = 0.; d2sdtau2(:,1) = 0.
    dsdtau(:,np) = dsdtau(:,np-1)+dtau(:,np,iz)*d2sdtau2(:,np-1)
    dsdtaum(:,np) = dsdtau(:,np)
    dsdtaup(:,np) = dsdtau(:,np)
    d2sdtau2(:,np) = d2sdtau2(:,np-1)
!
!  Incoming rays (2a+3m)
!
    qminus(:,1) = s(:,1,iz)*ex(:)-s(:,1,iz)
    do iy=2,np
      qminus(:,iy) = qminus(:,iy-1)*ex0(:,iy)-dsdtaum(:,iy)*ex1(:,iy)+d2sdtau2(:,iy)*ex2(:,iy)
    end do
!
!  Outgoing rays (2a+3m); estimate incoming heating = outgoing cooling
!
    qplus(:,np) = -qminus(:,np)*ex0(:,np)+dsdtaup(:,np)*ex1(:,np)+d2sdtau2(:,np)*ex2(:,np)
    do iy=np-1,1,-1
      qplus(:,iy) = qplus(:,iy+1)*ex0(:,iy+1)+dsdtaup(:,iy)*ex1(:,iy+1)+d2sdtau2(:,iy)*ex2(:,iy+1)
      q(:,iy,iz) = 0.5*(qplus(:,iy)+qminus(:,iy))
    end do
    q(:,np,iz) = 0.5*(qplus(:,np)+qminus(:,np))
!
!  Surface intensity.
!
    if (do_xi) then
      xi(:,iz)=2.*(1.-ex(:))*(q(:,1,iz)+s(:,1,iz))+s(:,1,iz)*ex(:)**2
    end if

  end do
!
END
