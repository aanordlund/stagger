! $Id: radiation_random.f90,v 1.22 2006/11/03 01:41:12 aake Exp $
!***********************************************************************
MODULE radiation_m
  implicit none
  integer nray, nbin, verbose
!-----------------------------------------------------------------------
  real wray, dsx, dsy, dsz, Tlower, total_size
  integer lsx, lsy, lsz, iseed0
  logical x_primary, y_primary, z_primary, do_radiation
  real, allocatable, dimension(:,:,:):: rho0, ee0, rkap0, src0, dtau0, q0, &
                                        afwd0, arev0 
  real, allocatable, dimension(:,:,:):: hx, hy, hz
CONTAINS

!***********************************************************************
SUBROUTINE get_direction (mx, my, mz, dx, dy, dz, iseed)
  USE params, ONLY: mid, master, dbg_cool, isubstep
  implicit none
  integer mx, my, mz, iseed
  real dx, dy, dz
  real ds, ran1s
  character(len=mid):: id='get_direction $Id: radiation_random.f90,v 1.22 2006/11/03 01:41:12 aake Exp $'
!-----------------------------------------------------------------------
  call print_trace (id, dbg_cool, 'BEGIN')
  !$omp single
  if (isubstep == 1) then
    iseed0 = iseed
  else
    iseed = iseed0
  end if
  dsx = 2.*ran1s(iseed)-1.
  dsy = 2.*ran1s(iseed)-1.
  dsz = 2.*ran1s(iseed)-1.
  dsx = dsx/dx
  dsy = dsy/dy
  dsz = dsz/dz
  call normalize_direction (mx, my, mz)
  if (verbose>0) print *,'direction:',dsx,dsy,dsz
  !$omp end single
  call print_trace (id, dbg_cool, 'END')
END SUBROUTINE

!***********************************************************************
SUBROUTINE normalize_direction (mx, my, mz)
  USE params, ONLY: master
  implicit none
  integer mx, my, mz
!-----------------------------------------------------------------------
  if      (abs(dsx) > max(abs(dsy), abs(dsz))) then
    x_primary = .true.; y_primary=.false.; z_primary=.false.
    dsy = dsy/dsx
    dsz = dsz/dsx
    lsy = ifix(mx*dsy+0.5+my)-my
    lsz = ifix(mx*dsz+0.5+mz)-mz
    lsy = min(max(lsy,-my),my)
    lsz = min(max(lsz,-mz),mz)
    dsy = real(lsy)/mx
    dsz = real(lsz)/mx
    lsy = mod(lsy-1+my,my)+1
    lsz = mod(lsz-1+mz,mz)+1
    dsx = 1.0000001
    if (verbose>0) print *,'x_primary:',dsy,dsz,lsy,lsz
  else if (abs(dsy) > max(abs(dsx), abs(dsz))) then
    y_primary = .true.; z_primary=.false.; x_primary=.false.
    dsx = dsx/dsy
    dsz = dsz/dsy
    lsx = ifix(my*dsx+0.5+mx)-mx
    lsz = ifix(my*dsz+0.5+mz)-mz
    lsx = min(max(lsx,-mx),mx)
    lsz = min(max(lsz,-mz),mz)
    dsx = real(lsx)/my
    dsz = real(lsz)/my
    lsx = mod(lsx-1+mx,mx)+1
    lsz = mod(lsz-1+mz,mz)+1
    dsy = 1.0000001
    if (verbose>0) print *,'y_primary:',dsx,dsz,lsx,lsz
  else
    z_primary = .true.; x_primary=.false.; y_primary=.false.
    dsx = dsx/dsz
    dsy = dsy/dsz
    lsx = ifix(mz*dsx+0.5+mx)-mx
    lsy = ifix(mz*dsy+0.5+my)-my
    lsx = min(max(lsx,-mx),mx)
    lsy = min(max(lsy,-my),my)
    dsx = real(lsx)/mz
    dsy = real(lsy)/mz
    lsx = mod(lsx-1+mx,mx)+1
    lsy = mod(lsy-1+my,my)+1
    dsz = 1.0000001
    if (verbose>0) print *,'z_primary:',dsx,dsy,lsx,lsy
  end if
END SUBROUTINE

!***********************************************************************
SUBROUTINE interpolate_to_rays (mx, my, mz, f1, f2)
  USE params, ONLY: mid, dbg_cool
  implicit none
  integer mx, my, mz
  real, dimension(mx,my,mz):: f1, f2
  character(len=mid):: id='interpolate_to_rays $Id: radiation_random.f90,v 1.22 2006/11/03 01:41:12 aake Exp $'
!-----------------------------------------------------------------------
  call print_trace (id, dbg_cool, 'BEGIN')
  if (x_primary) then
    call interpolate_to_rays_x (mx, my, mz, f1, f2)
  else if (y_primary) then
    call interpolate_to_rays_y (mx, my, mz, f1, f2)
  else
    call interpolate_to_rays_z (mx, my, mz, f1, f2)
  end if
  call print_trace (id, dbg_cool, 'END')
END SUBROUTINE

!***********************************************************************
SUBROUTINE interpolate_from_rays (mx, my, mz, f1, f2)
  USE params, ONLY: mid, dbg_cool
  implicit none
  integer mx, my, mz
  real, dimension(mx,my,mz):: f1, f2
  character(len=mid):: id='interpolate_from_rays $Id: radiation_random.f90,v 1.22 2006/11/03 01:41:12 aake Exp $'
!-----------------------------------------------------------------------
  call print_trace (id, dbg_cool, 'BEGIN')
  if (x_primary) then
    !$omp single
    dsy = -dsy; dsz = -dsz
    call normalize_direction (mx, my, mz)
    !$omp end single
    call interpolate_to_rays_x (mx, my, mz, f1, f2)
  else if (y_primary) then
    !$omp single
    dsx = -dsx; dsz = -dsz
    call normalize_direction (mx, my, mz)
    !$omp end single
    call interpolate_to_rays_y (mx, my, mz, f1, f2)
  else
    !$omp single
    dsx = -dsx; dsy = -dsy
    call normalize_direction (mx, my, mz)
    !$omp end single
    call interpolate_to_rays_z (mx, my, mz, f1, f2)
  end if
  call print_trace (id, dbg_cool, 'END')
END SUBROUTINE

!***********************************************************************
SUBROUTINE interpolate_to_rays_x (mx, my, mz, f1, f2)
  USE params, ONLY: scratch, xm
  implicit none
  integer mx, my, mz
  real, dimension(mx,my,mz):: f1, f2
  integer ix, jy, jz
  real py, pz
!-----------------------------------------------------------------------
  call mpi_send_y (f1, hy, 0, hy, 1)
  do ix=1,mx
    call indices_and_weigths (ix, jy, jz, mx, my, mz, py, pz, xm, dsy, dsz)
    call interpolate_in_y_x (mx, my, mz, f1, scratch, hy, jy, py, ix)
  end do
  !$omp barrier
  call mpi_send_z (scratch, hz, 0, hz, 1)
  do ix=1,mx
    call indices_and_weigths (ix, jy, jz, mx, my, mz, py, pz, xm, dsy, dsz)
    call interpolate_in_z_x (mx, my, mz, scratch, f2, hz, jz, pz, ix)
  end do
  !$omp barrier
END SUBROUTINE

!***********************************************************************
SUBROUTINE interpolate_to_rays_y (mx, my, mz, f1, f2)
  USE params, ONLY: scratch, ym, iys, iye
  implicit none
  integer mx, my, mz
  real, dimension(mx,my,mz):: f1, f2
  integer iy, jx, jz
  real px, pz
!-----------------------------------------------------------------------
  call mpi_send_x (f1, hx, 0, hx, 1)
  do iy=1,my
    call indices_and_weigths (iy, jx, jz, mx, my, mz, px, pz, ym, dsx, dsz)
    call interpolate_in_x_y (mx, my, mz, f1, scratch, hx, jx, px, iy)
  end do
  !$omp barrier
  call mpi_send_z (scratch, hz, 0, hz, 1)
  !$omp barrier
  do iy=iys,iye
    call indices_and_weigths (iy, jx, jz, mx, my, mz, px, pz, ym, dsx, dsz)
    call interpolate_in_z_y (mx, my, mz, scratch, f2, hz, jz, pz, iy)
  !  call interpolate_in_z_y (mx, my, mz, f1, f2, hz, jz, pz, iy)
  end do
  !$omp barrier
END SUBROUTINE

!***********************************************************************
SUBROUTINE interpolate_to_rays_z (mx, my, mz, f1, f2)
  USE params, ONLY: scratch, zm, izs, ize
  implicit none
  integer mx, my, mz
  real, dimension(mx,my,mz):: f1, f2
  integer iz, jx, jy
  real px, py
!-----------------------------------------------------------------------
  call mpi_send_x (f1, hx, 0, hx, 1)
  do iz=izs,ize
    call indices_and_weigths (iz, jx, jy, mx, my, mz, px, py, zm, dsx, dsy)
    call interpolate_in_x_z (mx, my, mz, f1, scratch, hx, jx, px, iz)
  end do
  call mpi_send_y (scratch, hy, 0, hy, 1)
  do iz=izs,ize
    call indices_and_weigths (iz, jx, jy, mx, my, mz, px, py, zm, dsx, dsy)
    call interpolate_in_y_z (mx, my, mz, scratch, f2, hy, jy, py, iz)
  end do
  !$omp barrier
END SUBROUTINE

!***********************************************************************
SUBROUTINE indices_and_weigths (i1, j2, j3, m1, m2, m3, p2, p3, x, ds2, ds3)
  USE params, ONLY: master
  implicit none
  integer i1, j2, j3, m1, m2, m3
  real p2, p3, x(m1), ds2, ds3
!-----------------------------------------------------------------------
  p2 = ds2*(i1-m1/2)+m2/2
  p3 = ds3*(i1-m1/2)+m3/2
  j2 = ifix(p2+m2)-m2
  j3 = ifix(p3+m3)-m3
  p2 = p2-j2
  p3 = p3-j3
  j2 = mod(j2+m2,m2)
  j3 = mod(j3+m3,m3)
  if (verbose>2) print '(1x,a,3i5,2f7.3)','indices_and_weights:',i1,j2,j3,p2,p3
END SUBROUTINE

!***********************************************************************
SUBROUTINE optical_depths (m1, m2, m3, dx, dy, dz)
  USE params, ONLY: mid, master, dbg_cool, izs, ize
  implicit none
  integer i1, i2, i3, m1, m2, m3
  real dx, dy, dz
  real ds
  character(len=mid):: id='optical_depths $Id: radiation_random.f90,v 1.22 2006/11/03 01:41:12 aake Exp $'
!-----------------------------------------------------------------------
  call print_trace (id, dbg_cool, 'BEGIN')
  if (x_primary) then
    ds = sqrt(dx**2+(dy*dsy)**2+(dz*dsz)**2)
    do i3=izs,ize
    do i2=1,m2
    do i1=2,m1
      dtau0(i1,i2,i3) = 0.5*(rkap0(i1-1,i2,i3)+rkap0(i1,i2,i3))*ds
    end do
    end do
    end do
  else if (y_primary) then
    ds = sqrt(dy**2+(dz*dsz)**2+(dx*dsx)**2)
    do i3=izs,ize
    do i2=2,m2
    do i1=1,m1
      dtau0(i1,i2,i3) = 0.5*(rkap0(i1,i2-1,i3)+rkap0(i1,i2,i3))*ds
    end do
    end do
    end do
  else
    ds = sqrt(dz**2+(dx*dsx)**2+(dy*dsy)**2)
    !$omp barrier
    do i3=max(2,izs),ize
    do i2=1,m2
    do i1=1,m1
      dtau0(i1,i2,i3) = 0.5*(rkap0(i1,i2,i3-1)+rkap0(i1,i2,i3))*ds
    end do
    end do
    end do
  end if
  if (master) then
    if (verbose>0) then
      print *,'optical depths:',dtau0(m1,m2,m3)
    else if (verbose>1) then
      print *,'optical depths:',dtau0(:,m2,m3)
    end if
  end if
  call print_trace (id, dbg_cool, 'END')
END SUBROUTINE

!***********************************************************************
SUBROUTINE radiative_transfer (mx, my, mz)
  USE params, ONLY: mid, dbg_cool
  implicit none
  integer mx, my, mz
  logical debug
  character(len=mid):: id='radiative_transfer $Id: radiation_random.f90,v 1.22 2006/11/03 01:41:12 aake Exp $'
!-----------------------------------------------------------------------
  call print_trace (id, dbg_cool, 'BEGIN')
  if (debug(dbg_cool)) print*,x_primary,y_primary,z_primary
  if (x_primary) then
    call transfer_x (mx, my, mz, dtau0, src0, q0, afwd0, arev0, lsy, lsz)
  else if (y_primary) then
    call transfer_y (mx, my, mz, dtau0, src0, q0, afwd0, arev0, lsz, lsx)
  else
    call transfer_z (mx, my, mz, dtau0, src0, q0, afwd0, arev0, lsx, lsy)
  end if
  call print_trace (id, dbg_cool, 'END')
END SUBROUTINE

!***********************************************************************
SUBROUTINE radiation_allocate (mx, my, mz)
  implicit none
  integer mx, my, mz
!-----------------------------------------------------------------------
!$omp master
  allocate (rho0 (mx,my,mz), ee0(mx,my,mz), rkap0(mx,my,mz), src0 (mx,my,mz))
  allocate (dtau0(mx,my,mz), q0 (mx,my,mz), afwd0(mx,my,mz), arev0(mx,my,mz))
  allocate (hx   ( 1,my,mz), hy (mx, 1,mz), hz   (mx,my, 1)) 
!$omp end master
!$omp barrier
END SUBROUTINE

!***********************************************************************
SUBROUTINE radiation_deallocate ()
  implicit none
!$omp barrier
!$omp master
  deallocate (rho0, ee0, rkap0, src0)
  deallocate (dtau0, q0, afwd0, arev0)
  deallocate (hx, hy, hz)
!$omp end master
END SUBROUTINE

!***********************************************************************
END MODULE

!***********************************************************************
SUBROUTINE init_radiation 
  USE params
  USE radiation_m
  implicit none
  character(len=mid):: id="$Id: radiation_random.f90,v 1.22 2006/11/03 01:41:12 aake Exp $"
!-----------------------------------------------------------------------
  call print_id (id)
  do_radiation = .true.
  nbin = 1                                                              ! number of freq. bins
  nray = 10                                                             ! number of rays per step
  Tlower = 1000.                                                        ! lower temperature limit
  verbose = 0                                                           ! debug output level
  total_size = 2.*max(sx,sy,sz)                                         ! total size for optical depth
END

!***********************************************************************
SUBROUTINE read_radiation 
  USE params
  USE radiation_m
  implicit none
  namelist /rad/do_radiation, nray, nbin, verbose
!-----------------------------------------------------------------------
  rewind (stdin); read (stdin, rad)
  if (master) write (*, rad)  
END

!***********************************************************************
SUBROUTINE radiation (rho, ee, lne, dd, dedt)
  USE params
  USE radiation_m
  implicit none
  real, dimension(mx,my,mz):: rho, ee, lne, dd, dedt
!-----------------------------------------------------------------------
  integer iray, ibin, iz, irad
  character(len=mid):: id='radiation $Id: radiation_random.f90,v 1.22 2006/11/03 01:41:12 aake Exp $'
!-----------------------------------------------------------------------
  if (.not. do_radiation) return
  call print_trace (id, dbg_cool, 'BEGIN')
  call radiation_allocate (mx, my, mz)

  irad = 0                                                              ! counter for dumps
  wray = 4.*pi/nray
  do iray=1,nray
    call get_direction         (mx, my, mz, dx, dy, dz, iseed)
    call dumpl (rho    ,'rho ', 'rad.dmp', irad); irad=irad+1
    call dumpl (ee     ,'ee  ', 'rad.dmp', irad); irad=irad+1
    call interpolate_to_rays   (mx, my, mz, rho, rho0)
    call dumpl (rho0   ,'rho0', 'rad.dmp', irad); irad=irad+1
    call interpolate_to_rays   (mx, my, mz, ee , ee0 )
    call dumpl (ee0    ,'ee0 ', 'rad.dmp', irad); irad=irad+1
    do ibin=1,nbin
      call radiation_lookup    (rho0, ee0, rkap0, src0, ibin)
      call dumpl (rkap0,'rkap' ,'rad.dmp',irad); irad=irad+1
      call dumpl (src0 ,'src ' ,'rad.dmp',irad); irad=irad+1
      call optical_depths      (mx, my, mz, dx, dy, dz)
      call dumpl (dtau0,'dtau','rad.dmp',irad); irad=irad+1
      call radiative_transfer  (mx, my, mz)
      do iz=izs,ize
        q0(:,:,iz) = (src0(:,:,iz)+q0(:,:,iz))*rkap0(:,:,iz)
      end do
      call dumpl (q0   ,'Q   ','rad.dmp',irad); irad=irad+1
    end do
    call interpolate_from_rays (mx, my, mz, q0, src0)
    call dumpl (scratch,'Qcart','rad.dmp',irad); irad=irad+1
    do iz=izs,ize
      dedt(:,:,iz) = dedt(:,:,iz) + src0(:,:,iz)*wray
    end do
  end do
  call radiation_deallocate ()

  call print_trace (id, dbg_cool, 'END')
END SUBROUTINE
