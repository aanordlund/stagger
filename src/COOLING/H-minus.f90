! $Id: H-minus.f90,v 1.13 2004/11/25 13:49:41 aake Exp $
! ----------------------------------------------------------------------
SUBROUTINE opacity (rho, tt, ne, rkap)
!
!  Absorption coefficient [cm^2/g] for H-, scaled to a value 3e-26 cm^4/dyne
!  at T=6300 K extracted from Fig. 3 of Doughty & Fraser MNRAS 132, 267
!  (1966).  A mean atomic weight of 1.4 (10% helium by number) is assumed.
!
!  For two reasons, the factor (1-yH) is not included: 1) yH is very small
!  for all temperatures where the optical depth is not very large, and 2)
!  to prevent the opacity from formally going to zero at high temperature,
!  which would only necessitate adding more opacity sources.
!
  USE params
  USE units
  implicit none

  real, dimension(mx,my,mz):: rho, tt, ne, rkap

  real, parameter:: eV=1.609e-12, chi_Hminus=0.747*eV, sigma_Hminus=1.5d-17, &
        m_H=1.67e-24, mu=1.4, kappa0=0.5*sigma_Hminus/(m_H*mu), &
        m_e=9.109390e-28, hbar=1.054573e-27, k_B=1.380658e-16, &
        tt_Hminus=chi_Hminus/k_B
  real:: rho0, rkap0, tt1
  integer ix, iy, iz

  character(len=mid):: id='$Id: H-minus.f90,v 1.13 2004/11/25 13:49:41 aake Exp $'
  if (id.ne.' ') print *,id; id=' '

  rho0 = m_H*mu*((m_e/hbar)*(chi_Hminus/hbar)/(2.*pi))**1.5
  rkap0 = kappa0*m_H*mu/rho0*(u_rho*u_l)/(u_l*1e-10)**3
!$omp parallel do private(ix,iy,iz,tt1)
  do iz=1,mz
    do iy=1,my
      do ix=1,mx
        tt1=tt_Hminus/tt(ix,iy,iz)
        rkap(ix,iy,iz) = rkap0*rho(ix,iy,iz)*ne(ix,iy,iz)*exp(tt1)*tt1**1.5
      end do
    end do
  end do
END
