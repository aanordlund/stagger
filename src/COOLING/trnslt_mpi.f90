! $Id: trnslt_mpi.f90,v 1.14 2013/11/03 13:52:07 aake Exp $
!***********************************************************************
MODULE trnslt_m
  real, dimension(:,:,:), allocatable:: fin2
END MODULE

SUBROUTINE test_trnslt
END SUBROUTINE test_trnslt

SUBROUTINE trnslt (fin,fout,dxdy,dzdy,do_exp,ny,mblocks,verbose)
  USE params
  implicit none
  real, dimension(mx,my,mz):: fin, fout
  intent(in):: fin
  intent(out):: fout
  real dxdy, dzdy
  logical do_exp
  integer mblocks							!maximum number of subdomains considered (per side) to fill the ghost zones
  integer ny, verbose
  logical omp_in_parallel
  character(len=mid):: id="$Id: trnslt_mpi.f90,v 1.14 2013/11/03 13:52:07 aake Exp $"
!-----------------------------------------------------------------------
  call print_id (id)
  if (omp_in_parallel()) then
    call trnslt_omp (fin,fout,dxdy,dzdy,do_exp,ny,mblocks,verbose)
  else
    !$omp parallel shared(dxdy,dzdy,do_exp,ny)
    call trnslt_omp (fin,fout,dxdy,dzdy,do_exp,ny,mblocks,verbose)
    !$omp end parallel
  end if
END

!***********************************************************************
SUBROUTINE trnslt_omp (fin,fout,dxdy,dzdy,do_exp,ny,mblocks,verbose)
!
!  Translate a scalar field to an inclined coordinate system.
!
!  Operation count:  8m+10a = 18 flops/pnt
!                    8m+6a  = 14 flops/pnt
!
!***********************************************************************
!
  USE params
  USE trnslt_m
  implicit none
  real, dimension(mx,my,mz):: fin, fout
  intent(in):: fin
  intent(out):: fout
  real dxdy, dzdy
  logical do_exp
  integer ny, verbose
  integer, save:: nwarnx=0, nwarnz=0
  real, dimension(mx):: ftmp
  real, dimension(mx,mz):: f, g
  integer ix, iy, iz, k, lm1, lp0, lp1, lp2, mm1, mp0, mp1, mp2, mg, mg1
  real p, q, ad, bd, ac, bc, af, bf, xk, zk
  integer mblocks							!maximum number of subdomains considered (per side) to fill the ghost zones
!-----------------------------------------------------------------------
!
!  Trap vertical rays
!
  if (abs(dxdy).lt.1e-6.and.abs(dzdy).lt.1.e-6) then
    if (do_exp) then
      do iz=izs,ize
        fout(:,:,iz) = exp(fin(:,:,iz))
      end do
    else
      do iz=izs,ize
        fout(:,:,iz) = fin(:,:,iz)
      end do
    end if
    return
  end if
!
!  y-loop
!
  if (abs(dxdy).lt.1e-6) then
    mg1 = max(abs(dzdy*ym(1)/dz),abs(dzdy*ym(ny)/dz)) + 1.
    mg  = min(mg1,mblocks*mz)
    if (verbose > 0) print*,'trnslt_mpi/z: mg1, mg =',mg1,mg
    if (master .and. nwarnz < 9 .and. max(abs(dzdy*ym(1)/dz),abs(dzdy*ym(ny)/dz)) > mg) then
      nwarnz = nwarnz+1
      print '(a,2f7.1,i5)',' WARNING: larger ghost zones needed for ghost_fill_z in trnslt_mpi', &
        abs(dzdy*ym(1)/dz), abs(dzdy*ym(ny)/dz), mg
    end if
    !$omp master
    allocate (fin2(mx,ny,1-mg:mz+mg))
    !$omp end master
    !$omp barrier
    call ghost_fill_z (mx,my,mz,fin,mg,mblocks,ny,izs,ize,fin2)
    do iz=izs,ize
    do iy=1,ny
      zk=dzdy*ym(iy)/dz
      k=floor(zk)
      p=zk-k
      q=1.-p
      af=q+p*q*(q-p)
      bf=p-p*q*(q-p)
      ad=p*q*q*0.5
      bd=-p*q*p*0.5
      ac=af-bd
      bc=bf+ad
!
!  Interpolate using cubic splines [4m+5a] [4m+3a]
!
      mm1 = max(min(iz+k-1,mz+mg),1-mg)
      mp0 = max(min(iz+k  ,mz+mg),1-mg)
      mp1 = max(min(iz+k+1,mz+mg),1-mg)
      mp2 = max(min(iz+k+2,mz+mg),1-mg)
      do ix=1,mx
        ftmp(ix)=ac*fin2(ix,iy,mp0)+bc*fin2(ix,iy,mp1)
      end do
      if (do_exp) then
        do ix=1,mx
	  ftmp(ix)=ftmp(ix)-ad*fin2(ix,iy,mm1)+bd*fin2(ix,iy,mp2)
        end do
        call expn (mx,ftmp,fout(1,iy,iz))
      else
        do ix=1,mx
	  fout(ix,iy,iz)=ftmp(ix)-ad*fin2(ix,iy,mm1)+bd*fin2(ix,iy,mp2)
        end do
      end if
    end do
    end do
    !$omp barrier
    !$omp master
    deallocate (fin2)
    !$omp end master
    return
  else if (abs(dzdy).lt.1e-6) then
    if (idbg > 2) print *,'trnslt: only x', omp_mythread, izs, ize
    mg1 = max(abs(dxdy*ym(1)/dx),abs(dxdy*ym(ny)/dx)) + 1.
    mg  = min(mg1,mblocks*mx)
    if (verbose > 0) print*,'trnslt_mpi/x: mg1, mg =',mg1,mg
    if (master .and. nwarnx < 9 .and. max(abs(dxdy*ym(1)/dx),abs(dxdy*ym(ny)/dx)) > mg) then
      nwarnx = nwarnx+1
      print '(a,2f7.1,i5)',' WARNING: larger ghost zones needed for ghost_fill_x in trnslt_mpi', &
        abs(dxdy*ym(1)/dx), abs(dxdy*ym(ny)/dx), mg
    end if
    !$omp master
    allocate (fin2(1-mg:mx+mg,ny,mz))
    !$omp end master
    !$omp barrier
    call ghost_fill_x (mx,my,mz,fin,mg,mblocks,ny,izs,ize,fin2)
    do iz=izs,ize
    do iy=1,ny
      xk=dxdy*ym(iy)/dx
      k=floor(xk)
      p=xk-k
      q=1.-p
      af=q+p*q*(q-p)
      bf=p-p*q*(q-p)
      ad=p*q*q*0.5
      bd=-p*q*p*0.5
      ac=af-bd
      bc=bf+ad
!
!  Interpolate using cubic splines [4m+5a] [4m+3a]
!
      lm1 = 1+k-1; if (lm1.lt.1) lm1=lm1+mx; if (lm1.gt.mx) lm1=lm1-mx
      if (do_exp) then
        do ix=1,mx
          lm1 = max(min(ix+k-1,mx+mg),1-mg)
          lp0 = max(min(ix+k  ,mx+mg),1-mg)
          lp1 = max(min(ix+k+1,mx+mg),1-mg)
          lp2 = max(min(ix+k+2,mx+mg),1-mg)
          ftmp(ix)      =(ac*fin2(lp0,iy,iz)+bc*fin2(lp1,iy,iz)- &
                          ad*fin2(lm1,iy,iz)+bd*fin2(lp2,iy,iz))
        end do
	call expn (mx,ftmp,fout(1,iy,iz))
      else
        do ix=1,mx
          lm1 = max(min(ix+k-1,mx+mg),1-mg)
          lp0 = max(min(ix+k  ,mx+mg),1-mg)
          lp1 = max(min(ix+k+1,mx+mg),1-mg)
          lp2 = max(min(ix+k+2,mx+mg),1-mg)
          fout(ix,iy,iz)=ac*fin2(lp0,iy,iz)+bc*fin2(lp1,iy,iz)- &
                         ad*fin2(lm1,iy,iz)+bd*fin2(lp2,iy,iz)
        end do
      end if
    end do
    end do
    !$omp barrier
    !$omp master
    deallocate (fin2)
    !$omp end master
    return
  else
    mg1 = max(abs(dzdy*ym(1)/dz),abs(dzdy*ym(ny)/dz)) + 1.
    mg  = min(mg1,mblocks*mz)
    if (verbose > 0) print*,'trnslt_mpi/xz/z: mg1, mg =',mg1,mg
    if (master .and. nwarnz < 9 .and. max(abs(dzdy*ym(1)/dz),abs(dzdy*ym(ny)/dz)) > mg) then
      nwarnz = nwarnz+1
      print '(a,2f7.1,i5)',' WARNING: larger ghost zones needed for ghost_fill_z in trnslt_mpi', &
        abs(dzdy*ym(1)/dz), abs(dzdy*ym(ny)/dz), mg
    end if
    !$omp master
    allocate (fin2(mx,ny,1-mg:mz+mg))
    !$omp end master
    !$omp barrier
    call ghost_fill_z (mx,my,mz,fin,mg,mblocks,ny,izs,ize,fin2)
    do iz=izs,ize
    do iy=1,ny
      zk=dzdy*ym(iy)/dz
      k=floor(zk)
      p=zk-k
      q=1.-p
      af=q+p*q*(q-p)
      bf=p-p*q*(q-p)
      ad=p*q*q*0.5
      bd=-p*q*p*0.5
      ac=af-bd
      bc=bf+ad
!
!  Interpolate using cubic splines [4m+5a] [4m+3a]
!
      mm1 = max(min(iz+k-1,mz+mg),1-mg)
      mp0 = max(min(iz+k  ,mz+mg),1-mg)
      mp1 = max(min(iz+k+1,mz+mg),1-mg)
      mp2 = max(min(iz+k+2,mz+mg),1-mg)
      do ix=1,mx
        fout(ix,iy,iz)=ac*fin2(ix,iy,mp0)+bc*fin2(ix,iy,mp1)- &
                       ad*fin2(ix,iy,mm1)+bd*fin2(ix,iy,mp2)
      end do
    end do
    end do
    !$omp barrier
    !$omp master
    deallocate (fin2)
    !$omp end master

    mg1 = max(abs(dxdy*ym(1)/dx),abs(dxdy*ym(ny)/dx)) + 1.
    mg  = min(mg1,mblocks*mx)
    if (verbose > 0) print*,'trnslt_mpi/xz/x: mg1, mg =',mg1,mg
    if (master .and. nwarnx < 9 .and. max(abs(dxdy*ym(1)/dx),abs(dxdy*ym(ny)/dx)) > mg) then
      nwarnx = nwarnx+1
      print '(a,2f7.1,i5)',' WARNING: larger ghost zones needed for ghost_fill_x in trnslt_mpi', &
        abs(dxdy*ym(1)/dx), abs(dxdy*ym(ny)/dx), mg
    end if
    !$omp master
    allocate (fin2(1-mg:mx+mg,ny,mz))
    !$omp end master
    !$omp barrier
    call ghost_fill_x (mx,my,mz,fout,mg,mblocks,ny,izs,ize,fin2)
    do iz=izs,ize
    do iy=1,ny
      xk=dxdy*ym(iy)/dx
      k=floor(xk)
      p=xk-k
      q=1.-p
      af=q+p*q*(q-p)
      bf=p-p*q*(q-p)
      ad=p*q*q*0.5
      bd=-p*q*p*0.5
      ac=af-bd
      bc=bf+ad
!
!  Interpolate using cubic splines [4m+5a] [4m+3a]
!
      do ix=1,mx
        lm1 = max(min(ix+k-1,mx+mg),1-mg)
        lp0 = max(min(ix+k  ,mx+mg),1-mg)
        lp1 = max(min(ix+k+1,mx+mg),1-mg)
        lp2 = max(min(ix+k+2,mx+mg),1-mg)
        ftmp(ix)=ac*fin2(lp0,iy,iz)+bc*fin2(lp1,iy,iz)- &
                 ad*fin2(lm1,iy,iz)+bd*fin2(lp2,iy,iz)
      end do
      if (do_exp) then
	call expn (mx,ftmp,fout(1,iy,iz))
      else
        do ix=1,mx
          fout(ix,iy,iz) = ftmp(ix)
        end do
      end if
    end do
    end do
    !$omp barrier
    !$omp master
    deallocate (fin2)
    !$omp end master
  end if
!
END

!***********************************************************************
SUBROUTINE trnslt_test
  USE params
  implicit none
  real, allocatable, dimension(:,:,:) :: fin,fout,fcmp
  integer count,ix,iy,iz
  real cpux,dxdy,dzdy,fdtime,k,kx,ky,kz,dsdy
  integer mblocks,verbose

  mx = 100
  my = mx
  mz = mx
  do_trace = .true.
  mblocks = 1
  verbose = 0

  allocate (fin(mx,my,mz),fout(mx,my,mz),fcmp(mx,my,mz))
  allocate (dxm(0:mx),dym(0:my),dzm(0:mz))
  allocate (xm(0:mx),ym(0:my),zm(0:mz))

  k = 2.*pi/mx
  dxm = 1.
  dym = 1.
  dzm = 1.
  xm = (/( ix*dxm(1), ix=1,mx )/)
  ym = (/( iy*dym(1), iy=1,my )/)
  zm = (/( iz*dzm(1), iz=1,mz )/)
  kx = 2.*pi/(mx*dxm(1))
  ky = 2.*pi/(my*dym(1))
  kz = 2.*pi/(mz*dzm(1))

!  Testing x-direction

  dsdy = 1.
  dxdy = dsdy
  dzdy = 0.
!$omp parallel private(iz)
  do iz=izs,ize
  do iy=1,my
  do ix=1,mx
    fin(ix,iy,iz) = cos(ix*k)+cos(iy*k)+cos(iz*k)
    fcmp(ix,iy,iz) = cos(ix*k+dxdy*ym(iy)*ky)+cos(iy*k)+cos(iz*k+dzdy*ym(iy)*kz)
  end do
  end do
  end do
!$omp end parallel
  cpux = fdtime()
  cpux = 0.
  count = 0
  do while (cpux .lt. 1.)
    call trnslt(fin,fout,dxdy,dzdy,.false.,my,mblocks,verbose)
    cpux = cpux+fdtime()
    count = count+1
  end do
  print *,'trnslt: count, ns/pt, error =',count,1e9*cpux/real(mx*my*mz)/count,maxval(abs(fout-fcmp))

!  Testing z-direction

  dxdy = 0.
  dzdy = dsdy
!$omp parallel private(iz)
  do iz=izs,ize
  do iy=1,my
  do ix=1,mx
    fin(ix,iy,iz) = cos(ix*k)+cos(iy*k)+cos(iz*k)
    fcmp(ix,iy,iz) = cos(ix*k+dxdy*ym(iy)*ky)+cos(iy*k)+cos(iz*k+dzdy*ym(iy)*kz)
  end do
  end do
  end do
!$omp end parallel
  cpux = fdtime()
  cpux = 0.
  count = 0
  do while (cpux .lt. 1.)
    call trnslt(fin,fout,dxdy,dzdy,.false.,my,mblocks,verbose)
    cpux = cpux+fdtime()
    count = count+1
  end do
  print *,'trnslt: count, ns/pt, error =',count,1e9*cpux/real(mx*my*mz)/count,maxval(abs(fout-fcmp))

!  Testing xz-direction

  dxdy = dsdy
  dzdy = dsdy
!$omp parallel private(iz)
  do iz=izs,ize
  do iy=1,my
  do ix=1,mx
    fin(ix,iy,iz) = cos(ix*k)+cos(iy*k)+cos(iz*k)
    fcmp(ix,iy,iz) = cos(ix*k+dxdy*ym(iy)*ky)+cos(iy*k)+cos(iz*k+dzdy*ym(iy)*kz)
  end do
  end do
  end do
!$omp end parallel
  cpux = fdtime()
  cpux = 0.
  count = 0
  do while (cpux .lt. 1.)
    call trnslt(fin,fout,dxdy,dzdy,.false.,my,mblocks,verbose)
    cpux = cpux+fdtime()
    count = count+1
  end do
  print *,'trnslt: count, ns/pt, error =',count,1e9*cpux/real(mx*my*mz)/count,maxval(abs(fout-fcmp))

  deallocate (fin,fout,fcmp)

END
