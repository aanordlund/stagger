! $Id: table.f90,v 1.25 2007/07/06 17:00:49 aake Exp $
!***********************************************************************
MODULE cooling
!
!  Cooling function from interpolation in a table of ln(lambda) as
!  a function f(lnrho,lnee,lndd)
!

real, parameter :: zlim=1.324E-12           ! guarantees that [Fe/H] > -9.0 
real, parameter :: noHHe=0.488              ! cdu -> # H+He atoms cm-3
real, parameter :: ln10=2.3025851           ! to speed up 10.** 
real, parameter :: invln10=0.4342945        ! to speed up log10 (?)
real, allocatable, dimension(:,:,:):: table
real :: a0,b0,c0,a1,b1,c1,tab0,Tlower
real :: Cerad                               ! used to solve problem with instability (not working)
integer :: ma,mb,mc,power
logical do_heat, do_table, do_power
END MODULE

!***********************************************************************
SUBROUTINE init_cooling (r,e)
  USE params
  USE cooling
  implicit none
  real, dimension(mx,my,mz):: r,e

  Tlower=1000.
  do_cool=.true.
  do_heat=.false.

  call read_cooling
END SUBROUTINE

!***********************************************************************
SUBROUTINE read_cooling
  USE params
  USE cooling
  character(len=40) tablefile
  namelist /cool/do_cool, do_heat, do_table, do_power, Tlower, tablefile

  tablefile = 'table.dat'
  read (*,cool)
  write (*,cool)

  open (3,file=tablefile,form='unformatted',status='old')
  read (3) ma,mb,mc,a0,b0,c0,a1,b1,c1,tab0,power
  print *,'(mr me md)    = (',ma,mb,mc,')'
  print *,'(r_i e_i d_i) = (',a0,b0,c0,')'
  print *,'(dr de dd)    = (',a1,b1,c1,')'
  print *,'tab_0, power: ',tab0,', ',power
  allocate (table(ma,mb,mc))
  read (3) table
  close (3) 
END SUBROUTINE

!***********************************************************************
SUBROUTINE coolit (r,ee,lne,dd,dedt,lambda)
  USE params
  USE eos
  USE units
  USE cooling
  implicit none

  real, parameter :: lnclambu=-47.1602
  real, dimension(mx,my,mz):: r,ee,lne,dd
  real, dimension(mx,my,mz), optional:: lambda,dedt
  real, dimension(mx,my):: temp
  integer :: ix,iy,iz,ia,ib,ic
  real :: a,b,c,e,pa,pb,pc,qa,qb,qc,tab,lntab,clambu,ct

  if (.not. do_cool) return

  clambu=exp(lnclambu)
  ct = 0.5*Cdt/dt

!$omp parallel do private(ix,iy,iz,a,b,c,ia,ib,ic,pa,pb,pc,qa,qb,qc,tab,temp)
  do iz=1,mz
    if (do_ionization) then
      call eos2 (r(:,:,iz),ee(:,:,iz),tt=temp)
    else
      temp = u_temp*ee(:,:,iz)
    end if
    do iy=1,my
      do ix=1,mx
        a = 1.+(alog(r(ix,iy,iz))       - a0)*a1
        b = 1.+(lne(ix,iy,iz)           - b0)*b1
        dd(ix,iy,iz)=max(dd(ix,iy,iz),zlim)          ! avoid negative values
        c = 1.+(alog(dd(ix,iy,iz))      - c0)*c1
        ia = min(max(1,ifix(a)),ma-1)
        ib = min(max(1,ifix(b)),mb-1)
        ic = min(max(1,ifix(c)),mc-1)
        pa = a-ia
        pb = b-ib
        pc = c-ic
        pa = min(max(pa,0.),1.)
        pb = min(max(pb,0.),1.)
        pc = min(max(pc,0.),1.)
        qa=1.-pa
        qb=1.-pb
        qc=1.-pc
        tab = &
          qc*(qb*(qa*table(ia  ,ib  ,ic  ) + pa*table(ia+1,ib  ,ic  ))  + &
               pb*(qa*table(ia  ,ib+1,ic  ) + pa*table(ia+1,ib+1,ic  ))) + &
          pc*(qb*(qa*table(ia  ,ib  ,ic+1) + pa*table(ia+1,ib  ,ic+1))  + &
              pb*(qa*table(ia  ,ib+1,ic+1) + pa*table(ia+1,ib+1,ic+1)))
        if (power .gt. 1) then
          tab = clambu*tab**power
        else
          tab = clambu*(exp(tab)+tab0)
        end if
        tab = tab*r(ix,iy,iz)**2
        if (present(lambda)) lambda(ix,iy,iz) = tab
        if (present(dedt)) then
          e=r(ix,iy,iz)*ee(ix,iy,iz)
          tab = sign(min(abs(tab),e*ct),tab)
          if (temp(ix,iy) .lt. Tlower) then                  ! if temperature below limit ...
            tab = -e*ct*(1.-temp(ix,iy)/Tlower)              ! ... use artificial heating
          end if

          dedt(ix,iy,iz) = dedt(ix,iy,iz) - tab

        end if
      end do
    end do
  end do

  if (present(dedt)) then
    call heatit(r,ee,dedt)                                   ! heating by ionizing radiation
  end if

END SUBROUTINE


!***********************************************************************
SUBROUTINE heatit (r,ee,dedt)

! An approximative treatment of the heating of the gas by ionizing radiation. A local 
! stromgren sphere is calculated around each individual high-mass star. The extension 
! of the stromgren sphere in different directions is estimated by the integral of 
! c0*S^(-1/3)*n^(2/3) dl = 1, where n is the no. particles cm-3, S is the no. ionizing
! photons cm-3 s-1, and c0 is a normalization factor (which depends on temperature). This 
! equation is a fit to stromgren radii for static and homogeneous media containing only 
! hydrogen. Inside these spheres a heating term G = n^2*beta_2*Q is added to the total 
! dedt, where n is the number of particles cm-3, beta_2 is the total recombination 
! coefficient cm3 s-1, and Q is the average energy injected per photoionization. Q ~ kB*T, 
! where kB = 1.38066e-16 erg K-1, and T is the effective temperature of the star(s). 
! This treatment probably underestimates the extension of the photoionized regions and 
! thereby underestimates the amount of heating in the simulation box.    

! Note: The indices are calculated for the case when all mx,my,mz are odd. I have not 
! checked whether it works for even mesh sizes. 

  USE params
  USE eos
  USE explosions
  USE units
  implicit none

  real, dimension(mx,my,mz) :: r,ee,dedt,tempgrid,ngrid,tau,heating,radheat 
  real :: x(mx),y(my),z(2*mz-1),runit(3),rvec(3),e,ct,scr
  real :: tau00,tau0,dtau,taup,nop,p,rhoav,w2,w3,avc,rho,rsf,rsfmin,t0,t1,heatav
  real :: xx,yy,zz,pt,pz,qt,qz,dn,teff,trec,r2,rif2,r2max,tevol,te,texp,chu,nip,tip
  integer, dimension(3,3) :: ind
  integer, dimension(mx,my,mz) :: flag
  integer :: ixl,iyl,izl,ixu,iyu,izu,i1,i2,i,j,ix,iy,iz,l,m,n,ll,mm,nn,nom
  integer :: xpos,ypos,zpos,interpx,interpy,interpz,offgrid,ixn,iyn,izn,ix1,ix2,ix3
  integer :: iy1,iy2,iy3,iz1,iz2,iz3,xi,yi,zi,xf,yf,odd,xodd,yodd,leven,meven 
  real cerad,nohhe,ln10

  !if ((.not.do_heat) .or. (mhms.eq.0)) return
  print *,'OBSOLETE'
  stop

  flag     = 0
  radheat  = 0. 
  tempgrid = u_temp*ee                            ! [T] = K
  ngrid    = nohhe*r                              ! [n] = cm-3
  tau      = 1.+0.0862*ngrid**(0.6666667)*tempgrid**(-0.25)
  tau00    = 4269.645          ! conversion factor (compensating for the factor 10^49 in no. ph. Myr-1)
  te       = 65825.            ! average temperature of e- per photoionization
  chu      = 3.3005e27         ! conversion factor cgs -> code units
  rhoav    = 1.                ! average density in the simulation box
  heating  = 2.e-10*chu*kB*te*tempgrid**(-0.75)   ! heating factor in code units
  Cerad    = 0.
  w2       = 0.7071068
  w3       = 0.5773503
  ct       = 0.5*Cdt/dt

  texp = 0.182
  t1   = 5.816e4
  t0   = -5.862e4

  xodd = modulo(mx,2)
  yodd = modulo(my,2)

  do i=1,2*mz-1
    z(i) = (i-mz)*dz
  end do

 do i=1,mhms  
    tau0 = tau00*exp(-ln10*hms(i,6)/3.)                      ! adding stellar radiation field
    l=modulo(hmspos(i,1)-nint(hms(i,8)*(hms(i,1)-t)/dx),mx)  ! current x-position of star
    m=modulo(hmspos(i,2)-nint(hms(i,9)*(hms(i,1)-t)/dy),my)  ! current y-position of star
    n=modulo(hmspos(i,3)-nint(hms(i,10)*(hms(i,1)-t)/dz),mz) ! current z-position of star
    if (l .eq. 0) l=mx
    if (m .eq. 0) m=my
    if (n .eq. 0) n=mz
    leven = modulo(l-1,2)
    meven = modulo(m-1,2)

    rsfmin = 0.
    rif2   = 0.
    r2max  = hms(i,11)
    do iz=1,2*mz-1 
      zz=z(iz)
      izl=iz-mz     
      nn=n+izl
      if (nn .gt. mz) then
        nn=nn-mz
      else if (nn .le. 0) then
        nn=nn+mz
      endif
      odd=modulo(nn,2)                      ! nn odd => ll,mm,nn odd; nn even => ll,mm,nn even
      xf=mx+2*odd*(xodd+leven)-odd-xodd-leven ! -leven*(1-2*odd)-(1-xodd)*odd-xodd*(1-odd)
      yf=my+2*odd*(yodd+meven)-odd-yodd-meven ! -meven*(1-2*odd)-(1-yodd)*odd-yodd*(1-odd)
      do iy=1,yf
        iyl=2*iy-yf-1
        yy=iyl*dy  
        mm=m+iyl
        if (mm .gt. my) then
          mm=mm-my+yodd*(1-2*odd)
          yy=yy+dy*yodd*(1-2*odd)
          iyl=iyl+yodd*(1-2*odd)
        else if (mm .le. 0) then
          mm=mm+my-yodd*(1-2*odd)
          yy=yy-dy*yodd*(1-2*odd)
          iyl=iyl-yodd*(1-2*odd)
        endif 
        do ix=1,xf
          ixl=2*ix-xf-1
          xx=ixl*dx      
          ll=l+ixl
          if (ll .gt. mx) then
            ll=ll-mx+xodd*(1-2*odd)
            xx=xx+dx*xodd*(1-2*odd)
            ixl=ixl+xodd*(1-2*odd)
          else if (ll .le. 0) then
            ll=ll+mx-xodd*(1-2*odd)
            xx=xx-dx*xodd*(1-2*odd)
            ixl=ixl-xodd*(1-2*odd)
          endif
          r2=(xx**2+yy**2+zz**2)
          if (r2 .le. r2max .and. flag(ll,mm,nn).eq.0) then    
            nom=max(abs(ixl),abs(iyl),abs(izl))           ! no. integration steps from star to cell
            if (nom .gt. 0) then
              runit(1)=float(ixl)/float(nom)              ! unit step vector (|runit| .neq. 1.) 
              runit(2)=float(iyl)/float(nom)
              runit(3)=float(izl)/float(nom)
              rvec=runit                                  ! step vector 
              dn=sqrt(r2)/float(nom)                      ! length of unit step vector
            else
              runit=0.
              rvec=0.
              dn=0.
            endif
            if (abs(ixl) .eq. nom) then   ! the two shortest base vectors define the interpolation plane 
              i1=2                                        
              i2=3
              ind=reshape((/0,0,0,1,1,0,0,1,1/),(/3,3/))  ! yz-plane          
            else if (abs(iyl) .eq. nom) then
              i1=1
              i2=3
              ind=reshape((/1,1,0,0,0,0,0,1,1/),(/3,3/))  ! xz-plane           
            else if (abs(izl) .eq. nom) then
              i1=1
              i2=2
              ind=reshape((/1,1,0,0,1,1,0,0,0/),(/3,3/))  ! xy-plane
            end if 
            xpos=l                                        ! position of the star
            ypos=m
            zpos=n
            ixn=xpos                                      ! starting point at star position
            iyn=ypos
            izn=zpos
            taup=0.  
            dtau=0.
            trec=0.                                 
            nip=0.
            tip=0.

            trec=0.5*ctu*2.e-10*ngrid(ixn,iyn,izn)*tempgrid(ixn,iyn,izn)**(-0.75)
            p=2.*trec*(t-hms(i,7))
            rsf=(1.-exp(-p))**(0.3333333)
            if (nom.gt.0 .and. tempgrid(ixn,iyn,izn).le.100000.) then
              taup = 0.5*tau0*dn*tempgrid(ixn,iyn,izn)**(-0.25)*ngrid(ixn,iyn,izn)**(0.6666667)
            end if
            do j=1,nom-1
              ixn=xpos+floor(rvec(1))                     ! current position in x-direction
              if (ixn .gt. mx) then 
                xpos=xpos-mx
                ixn=xpos+floor(rvec(1))              
              else if (ixn .le. 0) then
                xpos=xpos+mx
                ixn=xpos+floor(rvec(1))
              end if
              iyn=ypos+floor(rvec(2))                     ! current position in y-direction
              if (iyn .gt. my) then 
                ypos=ypos-my
                iyn=ypos+floor(rvec(2))              
              else if (iyn .le. 0) then
                ypos=ypos+my
                iyn=ypos+floor(rvec(2))
              end if
              izn=zpos+floor(rvec(3))                     ! current position in z-direction
              if (izn .gt. mz) then 
                zpos=zpos-mz
                izn=zpos+floor(rvec(3))              
              else if (izn .le. 0) then
                zpos=zpos+mz
                izn=zpos+floor(rvec(3))
              end if
         
              ix1 = ixn+ind(1,1); if (ix1.gt.mx) ix1=ix1-mx
              ix2 = ixn+ind(2,1); if (ix2.gt.mx) ix2=ix2-mx
              ix3 = ixn+ind(3,1); if (ix3.gt.mx) ix3=ix3-mx
              iy1 = iyn+ind(1,2); if (iy1.gt.my) iy1=iy1-my
              iy2 = iyn+ind(2,2); if (iy2.gt.my) iy2=iy2-my
              iy3 = iyn+ind(3,2); if (iy3.gt.my) iy3=iy3-my
              iz1 = izn+ind(1,3); if (iz1.gt.mz) iz1=iz1-mz
              iz2 = izn+ind(2,3); if (iz2.gt.mz) iz2=iz2-mz
              iz3 = izn+ind(3,3); if (iz3.gt.mz) iz3=iz3-mz
   
              pt=rvec(i1)-floor(rvec(i1))                 ! bi-linear interpolation
              pz=rvec(i2)-floor(rvec(i2))
              qt=1.-pt
              qz=1.-pz

              tip = qt*qz*tempgrid(ixn,iyn,izn)+pt*qz*tempgrid(ix1,iy1,iz1) &
                  + pt*pz*tempgrid(ix2,iy2,iz2)+qt*pz*tempgrid(ix3,iy3,iz3)    
              
              nip = qt*qz*ngrid(ixn,iyn,izn)+pt*qz*ngrid(ix1,iy1,iz1) &
                  + pt*pz*ngrid(ix2,iy2,iz2)+qt*pz*ngrid(ix3,iy3,iz3)
          
              nop = float(j)+0.5
              trec = trec + ctu*2.e-10*nip*tip**(-0.75)
              p = trec*(t-hms(i,7))/nop    
              rsf = (1.-exp(-p))**(0.3333333)             ! calculating fraction of stromgren radius
              if (tip .le. 100000.) then                  ! adding optical depth
                dtau = tau0*dn*tip**(-0.25)*nip**(0.6666667)
              else
                dtau = 0.
              end if
              taup = taup+dtau
              if (taup.gt.rsf) exit
              rvec=float(j+1)*runit                       ! stepping in direction to cell
            end do
            if (nom.gt.0) then
              if (taup.le.rsf) then
                trec = trec+0.5*ctu*2.e-10*ngrid(ll,mm,nn)*tempgrid(ll,mm,nn)**(-0.75)
                p = trec*(t-hms(i,7))/float(nom)
                rsf=(1.-exp(-p))**(0.3333333)
                if (tempgrid(ll,mm,nn).le.100000.) then
                  dtau = 0.5*tau0*dn*tempgrid(ll,mm,nn)**(-0.25)*ngrid(ll,mm,nn)**(0.6666667)
                else
                  dtau = 0.
                end if
                taup = taup+dtau
                tau(ll,mm,nn) = taup/rsf
                if (tau(ll,mm,nn).le.1.) then
                  flag(ll,mm,nn) = 1 
                  if (r2.gt.rif2) then
                    rif2 = r2
                    rsfmin = rsf**3
                  end if
                end if
              end if 
            else
              tau(ll,mm,nn)  = 0.
              flag(ll,mm,nn) = 1
            end if
          end if
        end do
      end do  
    end do
    offgrid=modulo(l,2)+modulo(m,2)+modulo(n,2)           ! tau = 0. in star cell 
    offgrid=modulo(offgrid,3)
    if (offgrid.ne.0) then
      tau(l,m,n)  = 0.
      flag(l,m,n) = 1
    end if
    if (rif2.gt.0.) then                                  ! stromgren radius prediction 
      tevol = 1.+2*dt/(t-hms(i,7))
      rsfmin = ((1.-(1.-rsfmin)**tevol)/rsfmin)**(0.3333333)
      hms(i,11) = (sqrt(rif2)*rsfmin+3.*max(dx,dy,dz))**2 
    else
      rho = min(rhoav,ngrid(l,m,n))
      hms(i,11) = (3.99e-3*exp(ln10*hms(i,6)/3.)*rho**(-0.6666667)+5.*max(dx,dy,dz))**2 
    end if
  end do 
  
  do i=1,8                 ! interpolating in tau and calculating heating term
    select case (i)
      case (1)
        xi=1
        yi=2
        zi=2
      case (2)
        xi=2
        yi=1
        zi=2
      case (3)
        xi=2
        yi=2
        zi=1
      case (4)
        xi=1
        yi=1
        zi=2
      case (5)
        xi=1
        yi=2
        zi=1
      case (6)
        xi=2
        yi=1
        zi=1
      case (7)
        xi=1
        yi=1
        zi=1
      case (8)
        xi=2
        yi=2
        zi=2
    end select  
    do iz=zi,mz,2
      do iy=yi,my,2
        do ix=xi,mx,2
          ixl=ix-1; ixu=ix+1
          iyl=iy-1; iyu=iy+1
          izl=iz-1; izu=iz+1

          if (ixl.eq.0) ixl=mx; if (ixu.eq.mx+1) ixu=1
          if (iyl.eq.0) iyl=my; if (iyu.eq.my+1) iyu=1
          if (izl.eq.0) izl=mz; if (izu.eq.mz+1) izu=1

          if (flag(ix,iy,iz).eq.1) then
            avc = 0.1464466
          else
            avc = 0.2071068
          end if 
      
          select case (i)
            case (1)  
              tau(ix,iy,iz) = avc*(w2*(tau(ix,iyl,izl)+tau(ix,iyl,izu)+tau(ix,iyu,izl) &
                            +          tau(ix,iyu,izu))+tau(ixl,iy,iz)+tau(ixu,iy,iz))

            case (2)  
              tau(ix,iy,iz) = avc*(w2*(tau(ixl,iy,izl)+tau(ixl,iy,izu)+tau(ixu,iy,izl) &
                            +          tau(ixu,iy,izu))+tau(ix,iyl,iz)+tau(ix,iyu,iz))

            case (3)  
              tau(ix,iy,iz) = avc*(w2*(tau(ixl,iyl,iz)+tau(ixu,iyl,iz)+tau(ixl,iyu,iz) &
                            +          tau(ixu,iyu,iz))+tau(ix,iy,izl)+tau(ix,iy,izu))

            case (4)
              tau(ix,iy,iz) = avc*(w2*(tau(ixl,iyl,iz)+tau(ixu,iyl,iz)+tau(ixl,iyu,iz) &
                            +          tau(ixu,iyu,iz))+tau(ix,iy,izl)+tau(ix,iy,izu))

            case (5)  
              tau(ix,iy,iz) = avc*(w2*(tau(ixl,iy,izl)+tau(ixl,iy,izu)+tau(ixu,iy,izl) &
                            +          tau(ixu,iy,izu))+tau(ix,iyl,iz)+tau(ix,iyu,iz))

            case (6)  
              tau(ix,iy,iz) = avc*(w2*(tau(ix,iyl,izl)+tau(ix,iyl,izu)+tau(ix,iyu,izl) &
                            +          tau(ix,iyu,izu))+tau(ixl,iy,iz)+tau(ixu,iy,iz))

            case default

          end select
          
          if (tau(ix,iy,iz).le.1. .and. tempgrid(ix,iy,iz).lt.100000.) then
            radheat(ix,iy,iz) = heating(ix,iy,iz)*ngrid(ix,iy,iz)*ngrid(ix,iy,iz)
          end if 

        end do
      end do
    end do
  end do   

  avc   = 0.0473842
  do iz=1,mz               ! smoothing of the heating term and updating of dedt
    do iy=1,my
      do ix=1,mx
        ixl=ix-1; ixu=ix+1
        iyl=iy-1; iyu=iy+1
        izl=iz-1; izu=iz+1
      
        if (ixl.eq.0) ixl=mx; if (ixu.eq.mx+1) ixu=1
        if (iyl.eq.0) iyl=my; if (iyu.eq.my+1) iyu=1
        if (izl.eq.0) izl=mz; if (izu.eq.mz+1) izu=1 
            
        heatav = avc*(w3*(radheat(ixl,iyl,izl)+radheat(ixl,iyl,izu)+radheat(ixl,iyu,izl) &
               +          radheat(ixl,iyu,izu)+radheat(ixu,iyl,izl)+radheat(ixu,iyl,izu) &
               +          radheat(ixu,iyu,izl)+radheat(ixu,iyu,izu))                     &
               +      w2*(radheat(ix,iyl,izl)+radheat(ix,iyu,izl)+radheat(ixl,iy,izl)  &
               +          radheat(ixu,iy,izl)+radheat(ixl,iyl,iz)+radheat(ixl,iyu,iz)  &
               +          radheat(ixu,iyl,iz)+radheat(ixu,iyu,iz)+radheat(ix,iyl,izu)  &
               +          radheat(ix,iyu,izu)+radheat(ixl,iy,izu)+radheat(ixu,iy,izu)) &
               +          radheat(ixl,iy,iz)+radheat(ixu,iy,iz)+radheat(ix,iyl,iz) &
               +          radheat(ix,iyu,iz)+radheat(ix,iy,izl)+radheat(ix,iy,izu) &
               +        2*radheat(ix,iy,iz))

        e = ee(ix,iy,iz)*r(ix,iy,iz)
        if (dedt(ix,iy,iz).lt.0.) then                         ! heating should not limit the time step
          dedt(ix,iy,iz)=min(dedt(ix,iy,iz)+heatav,0.5*e*ct)   ! heating by radiation
        else
          heatav = min(heatav,0.5*e*ct)                  
          dedt(ix,iy,iz) = dedt(ix,iy,iz) + heatav             ! heating by radiation
        end if

!        if (present(drdt) .and. present(Ceflag)) then
!          if (Ceflag .and. heatav.gt.0.) then
!            scr = dt*abs(dedt(ix,iy,iz)/e-drdt(ix,iy,iz)/r(ix,iy,iz)) ! a factor ~ 4. => no high-T regions 
!            if (scr.gt.Cerad) Cerad = scr
!          end if
!        end if
  
      end do
    end do
  end do  

END SUBROUTINE
