
!***********************************************************************
SUBROUTINE transfer_ray_integral (my, dtau, S, Q, Q0, Q1)
!
!  Solve the radiative transfer equation in both directions along a ray.
!
!  dtau:   optical depth intervals
!  S   :   source funtion
!  Q   :   cooling contribution; average of in-coming and out-going I-S
!  Q0  :   incoming Q at index 1  (= -S(1) if no incoming radiation)
!  Q1  :   incoming Q at index my (=  0.0  if I=S)
!
!  Operation count: 1e+11a+12m+2d
!
!  Author: Aake Nordlund, 2004-09-22
!-----------------------------------------------------------------------
  implicit none
  integer mx, my, ix, iy
  real, dimension(my) :: dtau, S, Q
  real, dimension(my) :: ex0, ex1, ex2, ds, dsdtau, d2sdtau2
  real, dimension(my) :: qplus, qminus
  real :: Q0, Q1
  real :: ctau, cpus(2), dtime, sum
  real(kind=8) :: exp0
  real, save :: cpu1=0., cpu2=0.
  integer :: niter, iter
  logical, save :: start=.true.
!
!-----------------------------------------------------------------------
!  If no timings, compare times for exp() and if methods
!-----------------------------------------------------------------------
  if (cpu1 == 0) then
    niter = 10000000/my

!-----------------------------------------------------------------------
!  Standard exp-method
!-----------------------------------------------------------------------
    sum=0.
    cpu1 = dtime(cpus)
    do iter=1,niter
      do iy=2,my
        ds(iy) = (s(iy)-s(iy-1))/dtau(iy)
        exp0 = exp(-dble(dtau(iy)))
        ex0(iy) = exp0
        ex1(iy) = 1d0-exp0
        ex2(iy) = 1d0-exp0-dtau(iy)*exp0
      end do
      sum=sum+ex2(mod(iter,my)+1)
    end do
    exp0 = exp(-dble(dtau(1)))
    ex0(1) = exp0
    cpu1 = dtime(cpus)

!-----------------------------------------------------------------------
!  If-method, which uses Taylor expansion at mosts depths
!-----------------------------------------------------------------------
    do iter=1,niter
    do iy=2,my
      ds(iy) = (s(iy)-s(iy-1))/dtau(iy)
      if (dtau(iy) > 15.) then
        exp0 = 0.
        ex1(iy) = 1d0
      else if (dtau(iy) < 0.03) then
        ex1(iy) = dtau(iy)*(1.-0.5*dtau(iy))
        exp0 = 1d0-ex1(iy)
      else if (dtau(iy) < 0.3) then
        ex1(iy) = dtau(iy)*(1.-0.5*dtau(iy)*(1.-0.33333333*dtau(iy)))
        exp0 = 1d0-ex1(iy)
      else 
        exp0 = exp(-dble(dtau(iy)))
        ex1(iy) = 1d0-exp0
      end if
      ex0(iy) = exp0
      ex2(iy) = 1d0-exp0-dtau(iy)*exp0
    end do
    end do
    exp0 = exp(-dble(dtau(1)))
    ex0(1) = exp0
    cpu2 = dtime(cpus)
!cpu2 = 0.

!-----------------------------------------------------------------------
!  Choose the method.
!-----------------------------------------------------------------------
    print *,'exp: ',cpu1
    print *,' if: ',cpu2
    if (cpu1 > cpu2) then
      print *,'choosing if'
    else
      print *,'choosing exp'
    end if

!-----------------------------------------------------------------------
!  Coefficients, 1e+4a+1m+1d
!-----------------------------------------------------------------------
  else if (cpu1 > cpu2) then
    if (start) print *,'if method'
    start = .false.
    do iy=2,my
      ds(iy) = (s(iy)-s(iy-1))/dtau(iy)
      if (dtau(iy) > 15.) then
        exp0 = 0.
        ex1(iy) = 1d0
      else if (dtau(iy) < 0.1) then
        ex1(iy) = dtau(iy)*(1.-0.5*dtau(iy))
        exp0 = 1d0-ex1(iy)
      else 
        exp0 = exp(-dble(dtau(iy)))
        ex1(iy) = 1d0-exp0
      end if
      ex0(iy) = exp0
      ex2(iy) = 1d0-exp0-dtau(iy)*exp0
    end do
    exp0 = exp(-dble(dtau(1)))
    ex0(1) = exp0

!-----------------------------------------------------------------------
!  Coefficients, 1e+4a+1m+1d
!-----------------------------------------------------------------------
  else
    if (start) print *,'exp method'
    start = .false.
    do iy=2,my
      ds(iy) = (s(iy)-s(iy-1))/dtau(iy)
      exp0 = exp(-dble(dtau(iy)))
      ex0(iy) = exp0
      ex1(iy) = 1d0-exp0
      ex2(iy) = 1d0-exp0-dtau(iy)*exp0
    end do
    exp0 = exp(-dble(dtau(1)))
    ex0(1) = exp0
  end if
  ex1(1) = 1d0-exp0
  ex2(1) = 1d0-exp0-dtau(1)*exp0
!
!-----------------------------------------------------------------------
!  dS/dtau and d2S/dtau2 (5m+3a+1d)
!-----------------------------------------------------------------------
  do iy=2,my-1
      ctau = 1./(dtau(iy+1)+dtau(iy))
      dsdtau(iy) = (ds(iy+1)*dtau(iy)+ds(iy)*dtau(iy+1))*ctau
      d2sdtau2(iy) = (ds(iy+1)-ds(iy))*2.*ctau
  end do
  d2sdtau2( 1) = d2sdtau2(   2)
  d2sdtau2(my) = d2sdtau2(my-1)
  dsdtau( 1) = dsdtau(   2) - dtau( 2)*d2sdtau2(   2)
  dsdtau(my) = dsdtau(my-1) + dtau(my)*d2sdtau2(my-1)
!
!-----------------------------------------------------------------------
!  Incoming rays (2a+3m)
!-----------------------------------------------------------------------
  qminus(1) = Q0
  do iy=2,my
    qminus(iy) = qminus(iy-1)*ex0(iy)-dsdtau(iy)*ex1(iy)+d2sdtau2(iy)*ex2(iy)
  end do
!
!-----------------------------------------------------------------------
!  Outgoing rays (2a+3m)
!-----------------------------------------------------------------------
  qplus(my) = Q1
  do iy=my-1,1,-1
    qplus(iy) = qplus(iy+1)*ex0(iy+1)+dsdtau(iy)*ex1(iy+1)+d2sdtau2(iy)*ex2(iy+1)
    q(iy) = 0.5*(qplus(iy)+qminus(iy))
  end do
  q(my) = 0.5*(qplus(my)+qminus(my))
END

!***********************************************************************
!SUBROUTINE test transfer_ray_integral
!
!  Test program / subroutine
!
!  About 100 ns/pnt on a 1.5 GHz Pentium M (-O3 -tpp7 -xW)
!  About  80 ns/pnt on a 2.0 GHz Pentium 4 (-O3 -tpp7 -xW)
!  About  60 ns/pnt on a 2.0 GHz Pentium 4 (-O3 -tpp7 -xW, if statements)
!  About  60 ns/pnt on a 1.3 GHz Itanium 2 (-O3)

  implicit none
  integer, parameter:: my=50, niter=10000000/my
  real, dimension(my) :: tau, dtau, S, Q
  real I, cpu, cpus(2), dtime
  integer iy, iter

  do iy=1,my
    tau(iy) = 10.**(-4+8.*(iy-1.)/(my-1))
    S(iy) = tau(iy)
  end do

  do iy=2,my
    dtau(iy) = tau(iy)-tau(iy-1)
  end do
  
  cpu = dtime(cpus)
  do iter=1,niter
    call transfer_ray_integral (my, dtau, S, Q, 0., 0.)
  end do
  cpu = dtime(cpus)
  print *,cpu,' sec, ',cpu*1e9/(my*niter),' ns/pt'

END
