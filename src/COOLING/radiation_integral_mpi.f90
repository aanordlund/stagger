! $Id: radiation_integral_mpi.f90,v 1.1 2008/07/03 21:32:11 aake Exp $
!***********************************************************************
MODULE cooling
  USE params
  integer, parameter:: mmu=9, mphi=10
  real dphi, form_factor, dtaumin, dtaumax
  integer nmu, nphi, verbose
  integer n1p, n2p
  real y_newton, dy_newton, t_newton, ee_newton, ee_min, t_ee_min
  real, dimension(mmu):: mu0

  real, allocatable, dimension(:,:):: xi
  real, pointer, dimension(:,:,:):: rk,lnrk,s,q,qq,lnss

  logical do_table, do_newton, do_limb
  character(len=mfile):: intfile
END MODULE

!***********************************************************************
SUBROUTINE init_cooling (r,e)
  USE params
  USE arrays
  USE cooling
  implicit none
  real, dimension(mx,my,mz):: r,e
  integer i
  character(len=mfile):: name

  do_cool = .true.
  i = index(file,'.')
  nmu = 0
  nphi = 4
  dphi = 0.
  form_factor = 1.
  dtaumax = 100.
  dtaumin = 0.1
  intfile = name('intensity.dat','int',file)

  do_newton = .false.
  y_newton = -0.3
  dy_newton = 0.05
  t_newton = 0.01
  t_ee_min = 0.005
  ee_newton = 5.265
  ee_min = 3.6
  do_limb = .false.
  verbose = 0

  call read_cooling
  allocate (xi(mx,mz))
  rk   => scr1
  lnrk => scr2
  s    => scr3
  q    => scr4
  lnss => scr5
  qq   => scr6
END SUBROUTINE

!***********************************************************************
SUBROUTINE read_cooling
  USE params
  USE eos
  USE cooling
  namelist /cool/do_cool, nmu, nphi, dphi, form_factor, dtaumin, dtaumax, intfile, &
    do_newton, y_newton, dy_newton, t_newton, ee_newton, ee_min, t_ee_min, do_limb, mu0, &
    verbose
  character(len=mfile):: name, fname

  character(len=mid):: id='$Id: radiation_integral_mpi.f90,v 1.1 2008/07/03 21:32:11 aake Exp $'
  call print_id(id)

  rewind (stdin); read (stdin,cool)
  if (master) write (*,cool)
  if (master) write (*,*) 'mbox, dbox =', mbox, dbox
  if (do_limb) then
    fname = name('limb.dat','lmb',file)
    call mpi_name(fname)
    open (limb_unit, file=trim(fname), &
      form='unformatted', status='unknown')
    write (limb_unit) mx, mz,nmu,nphi
  end if
  call mpi_name(intfile)
END SUBROUTINE

!***********************************************************************
SUBROUTINE coolit (r, ee, lne, dd, dedt)
!
!  Radiative cooling
!
  USE params
  !USE variables
  USE arrays, only: lns
  USE units
  USE eos
  USE cooling
  implicit none
  real, dimension(mx,my,mz):: r, ee, lne, dd, dedt
  real xmu(mmu), wmu(mmu)
  integer ix, iy, iz, lmu, lphi, imu, iphi, ibox, n1p1, n2p1
  real sec, fdtime, dxdy, dzdy, tanth, qq1, f1, f2
  real phi, wphi, womega
  integer lrec, idmp, ny
  logical flag, do_io
  logical, save:: first=.true.
  external transfer
  logical debug
  real cput(2), void, dtime, prof
!
  if (.not. do_cool .or. mpi_y  > 0) return

  if (debug(dbg_cool) .and. omp_master) then; void = dtime(cput) &
    ; print '(1x,a8,2f7.3)','rad0   ',cput*omp_nthreads; endif

  if (lb > 1) then                                                      ! if we have ghost zones
    dedt(:,1:lb-1,izs:ize) = 0.                                         ! zap previous dedt there
  end if

!-----------------------------------------------------------------------
!  Angular quadrature
!  nmu > 0: Use Gauss integration (mu=1 excluded) with 2*nmu   order accuracy
!  nmu = 0: One vertical ray with weight 0.5
!  nmu < 0: Use Radau integration (mu=1 included) with 2*nmu-1 order accuracy
!-----------------------------------------------------------------------
  if      (nmu .gt. 0) then
    lmu = nmu
    call gausi (lmu,0.,1.,wmu,xmu)
  else if (nmu .eq. 0) then
    lmu = 1
    xmu(1) = 1.
    wmu(1) = 0.5
  else 
    lmu =-nmu
    call radaui(lmu,0.,1.,wmu,xmu)
  end if
  if (mu0(1) .ne. 0.) xmu=mu0

  if (first) then
    call barrier_omp ('rad-first')
    if (omp_master) first = .false.
    if (master) then
      print *,' imu       xmu    theta'
      !           1.....0.000.....00.0
      do imu=1,lmu 
        print '(i4,f10.3,f8.1,f10.3)',imu,xmu(imu),acos(xmu(imu))*180./pi,wmu(imu)
      end do
    end if
  end if

!-----------------------------------------------------------------------
!  Equation of state and opacity
!-----------------------------------------------------------------------

!!$omp parallel private(iz,iy,ix,imu,iphi,lphi,tanth,wphi,phi,dxdy,dzdy,ibox,f1,f2,qq1,flag,n1p,n2p), shared(my)
  call lookup (r,ee,lnrk,lns)

  idmp = 0
  call dumpn (r,'r','rad.dmp',1)
  call dumpn (ee,'ee','rad.dmp',1)
  call dumpn (lnrk,'lnrk','rad.dmp',1)
  do ibox=1,mbox
    call dumpn (lns(:,:,:,ibox),'lns','rad.dmp',1)
  end do
  idmp = 0

  if (debug(dbg_cool) .and. omp_master) then; void = dtime(cput) &
    ; print '(1x,a8,2f7.3)','lookup ',cput*omp_nthreads; endif

  do iz=izs,ize
    qq(:,:,iz) = 0.
  end do
  
  do imu=1,lmu
    if (nmu .eq. 0 .and. imu .eq. 1) then
      lphi = 1 
    else if (nmu .lt. 0 .and. imu .eq. lmu) then
      lphi = 1 
    else
      lphi = nphi
    end if
    tanth = tan(acos(xmu(imu)))
    wphi = 4.*pi/lphi

    do iphi=1,lphi
      phi = (iphi-1)*2.*pi/lphi + dphi*t
      dxdy = tanth*cos(phi)
      dzdy = tanth*sin(phi)

      do ibox=1,mbox
        f1 = 10.**(ibox-1)
	lnss = lns(:,:,:,ibox)
	ny = my
        call transfer (mx,my,mz,ny,dxdy,dzdy,xm,ym,zm,dx,dz,sx,sz,lnss,lnrk,f1, &
	               q,dtaumax,verbose)
	call dumpn(q,'q','rad.dmp',1)
	if (verbose > 0) print*,'f1=',f1,wmu(imu),wphi,form_factor
        f1 = f1*wmu(imu)*wphi*form_factor
	do iz=izs,ize
	  qq(:,:,iz) = qq(:,:,iz) + q(:,:,iz)*f1*exp(lnrk(:,:,iz))
	end do
      end do
    end do
  end do
  call dumpn(qq,'qq','rad.dmp',1)
  call dumpn(qq,'qq','qq.dmp',1)
  
  do iz=izs,ize
    dedt(:,:,iz) = dedt(:,:,iz) + qq(:,:,iz)
  end do

  if (do_newton) then
    do iz=izs,ize
    do iy=1,my
      prof = exp((y_newton-ym(iy))/dy_newton)
      prof = prof/(1.+prof)
      do ix=1,mx
        dedt(ix,iy,iz) = dedt(ix,iy,iz) - r(ix,iy,iz)*(ee(ix,iy,iz)-eetop)/t_newton*prof &
                                        - r(ix,iy,iz)*min(0.,ee(ix,iy,iz)-ee_min)/t_ee_min
      end do
    end do
    end do
  end if

  if (debug(dbg_cool) .and. omp_master) then; void = dtime(cput) &
    ; print '(1x,a8,2f7.3)','transfr',cput*omp_nthreads; endif

END SUBROUTINE
