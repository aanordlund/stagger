! $Id: Lyman_luminosity.f90,v 1.7 2006/07/05 23:52:43 aake Exp $
!***********************************************************************
MODULE lyman_m
  USE params, only: mfile
  implicit none
  integer row,column,isnap_Lyman
  character (len=mfile) Lymanfile,lifefile
  real, dimension(51,26) :: Lymantbl
  real, dimension(7,2) :: lifetbl
  integer grab
  real snorm, wly, ply, warg

CONTAINS

!***********************************************************************
SUBROUTINE get_Lyman_luminosity (i, Rti, L_Lyman, progm)
  USE explosions
  USE params
  implicit none
  real,dimension(13) :: stmass, a
  integer i,j,pos1,pos2
  real Rti,L_Lyman,progm,lifetime,a0,a1
  character(len=mid):: id='get_Lyman_luminosity $Id: Lyman_luminosity.f90,v 1.7 2006/07/05 23:52:43 aake Exp $'
!-----------------------------------------------------------------------
  call print_trace (id, dbg_cool, 'BEGIN')
  stmass=(/ 1., 2.,3.,4.,5.,7.,15.,20.,25.,40.,60.,85.,120./)
  do j=1,13
     if (stmass(j) >  progm) exit
  end do 
  j=min(max(j-1,1),12)

  if (progm .gt.1 .and. progm .lt.8) then
    a1 = (lifetbl(j+1,2)-lifetbl(j,2))/ &
	 (lifetbl(j+1,1)-lifetbl(j,1))
    a0 = lifetbl(j,2)-a1*lifetbl(j,1)
    lifetime = a1*progm+a0               ! life time of star in Myrs
    Rti=Rti/lifetime                     ! get current rate of life of the star   
  endif

  pos1=int(2*j-1)
  pos2=int(2*(j+1)-1)
  call interpolation_lyman(i,Rti,Lymantbl,row,column,1,pos1,pos2,L_Lyman,progm,j)

  call print_trace (id, dbg_cool, 'END')
END SUBROUTINE

!***********************************************************************
SUBROUTINE interpolation_lyman(i,Rti,tbl,row,column,columnsFromAge,pos1,pos2,x,progm,poslow)
  USE explosions
  implicit none
  integer i,j,pos1,pos2,pos3,columnsFromAge,row,column,poslow
  real Rti,x,progm
  double precision m1,m2,m3,n1,n2,n3,interpol1,interpol2
  double precision,allocatable, dimension(:) :: Rlife1,Rlife2,a
  real, dimension(row,column) :: tbl
  character(len=mid):: id='interpolation_lyman $Id: Lyman_luminosity.f90,v 1.7 2006/07/05 23:52:43 aake Exp $'
!-----------------------------------------------------------------------
  call print_trace (id, dbg_cool, 'BEGIN')
  allocate(Rlife1(row))
  allocate(a(row))
  Rlife1=tbl(:,pos1)

  a=-1.
  where(Rlife1.le.Rti) a=Rlife1
  do j=1,row
     if (Rlife1(j).eq.maxval(a)) then
        exit
     endif
     if(j.eq.row) then
        write(*,*) '1fallo',Rlife1(j),a       
         write(*,*) 'What in the Earth1 are u doing HERE'
     endif      
  end do
  m1=(tbl(j+1,pos1+columnsFromAge)-tbl(j,pos1+columnsFromAge))/(tbl(j+1,pos1)&
     -tbl(j,pos1))
        
  n1=tbl(j,pos1+columnsFromAge)-m1*tbl(j,pos1)
  interpol1=m1*Rti+n1
  deallocate(Rlife1)

!  Second interpolation in the table
  
  allocate(Rlife2(row))
  Rlife2=tbl(:,pos2)
  a=-1.  
  where(Rlife2.le.Rti) a=Rlife2
  do j=1,row
     if (Rlife2(j).eq.maxval(a))  then 
        exit
     endif              
     if(j.eq.row) then 
       write(*,*) '2fallo:',Rlife2(j),a
        write(*,*) 'What in the Earth2 are u doing HERE'
     endif
  end do
  m2=(tbl(j+1,pos2+columnsFromAge)-tbl(j,pos2+columnsFromAge))/(tbl(j+1,pos2)&
     -tbl(j,pos2))
        
  n2=tbl(j,pos2+columnsFromAge)-m2*tbl(j,pos2)
  interpol2=m2*Rti+n2
  deallocate(Rlife2)
  deallocate(a)

!  Final interpolation depending on the initial mass!

  if (progm .gt. 8) then
     pos3=int(hms(i,13))
     m3=(interpol2-interpol1)/(genevelifetbl(pos3+1,1)-genevelifetbl(pos3,1))
     n3=interpol1-m3*genevelifetbl(pos3,1)
     x=m3*hms(i,2) + n3    
  else 
     m3=(interpol2-interpol1)/(lifetbl(poslow+1,1)-lifetbl(poslow,1))
     n3=interpol1-m3*lifetbl(poslow,1)
     x=m3*progm + n3    
  endif
  call print_trace (id, dbg_cool, 'END')
END subroutine

!***************************************************************************
SUBROUTINE add_prof (ldist, mdist, ndist, a, s, snorm, i)
  USE params
  implicit none
  integer ldist, mdist, ndist, i
  real a, snorm
  real, dimension(mx,my,mz):: s
  real radius, arg, ex, stmp, xx, yy, zz, cly, carg
  integer l, m, n, ll, mm, nn
  logical debug
  character(len=mid):: id='add_prof $Id: Lyman_luminosity.f90,v 1.7 2006/07/05 23:52:43 aake Exp $'
!-----------------------------------------------------------------------
  call print_trace (id, dbg_cool, 'BEGIN')

  stmp = 0.
  cly = 1./wly
  carg = 1./(2.*warg)
  do nn=izs,ize
    n=mz/2+nn-ndist
    if (n .gt. mz) then
      n=n-mz
    else if (n .le. 0) then
      n=n+mz
    endif
    zz=zm(n)-zm(mz/2)
    ! if (debug(dbg_cool)) print *,nn,n,zz
    do mm=1,my
      m=my/2+mm-mdist
      if (m .gt. my) then
        m=m-my
      else if (m .le. 0) then
        m=m+my
      endif
      yy=ym(m)-ym(my/2)
      do ll=1,mx
        l=mx/2+ll-ldist
        if (l .gt. mx) then
          l=l-mx
        else if (l .le. 0) then
          l=l+mx
        endif
        xx=xm(l)-xm(mx/2)
        arg = ply*(sqrt(xx**2+yy**2+zz**2)*cly-1.)                      ! normalized radius
	if (arg > warg) then                                            ! outer part
	  ex = 0.
	else if (arg < -warg) then                                      ! inner part
	  ex = 1.
	else                                                            ! transition
          !ex = 1./(1.+exp(arg)**2)                                      ! tanh()-like profile
          ex = (warg-arg)*carg                                          ! linear slope -- cheap
	end if
        s(ll,mm,nn) = s(ll,mm,nn) + a*ex
	if (i == 1) then
	  stmp = stmp + ex
	end if
      end do
    end do
  end do
  if (i == 1) then
    !$omp critical
    snorm = snorm + stmp
    !$omp end critical
    !$omp barrier                                                       ! make sure all threads done
    if (debug(dbg_cool)) print *,'snorm =',snorm
  end if
  call print_trace (id, dbg_cool, 'END')
END SUBROUTINE

!**************************************************************************
END MODULE

!***************************************************************************
SUBROUTINE init_Lyman
  USE params
  USE lyman_m
  USE units
  USE explosions, ONLY: wsn, psn
  implicit none
  integer ::i,j,k
  character(len=mid):: id="$Id: Lyman_luminosity.f90,v 1.7 2006/07/05 23:52:43 aake Exp $"
!-----------------------------------------------------------------------
  call print_id (id)
  grab=0
  row=51
  column=26
  isnap_Lyman=isnap
  Lymanfile='../src/FORCING/YIELDS/Lyman.tbl'
  lifefile='../src/FORCING/YIELDS/life_lowmass.tbl' 
  wly = 4.
  ply = 5.
  warg = 7.

  open(1,file=Lymanfile,status='old', form='formatted')
  do k=1,row
    read(1,*) ((Lymantbl(i,j), i=k,k), j=1,column)
  end do
  close(1)

  !do i=1,51
  ! Lymantbl(i,:)=Lymantbl(i,:)*(ceu/ctu)
  !enddo

  open(1,file=lifefile, status='old', form='formatted')
  do k=1,7
    read(1,*) ((lifetbl(i,j), i=k,k), j=1,2)
  end do
  close(1)   
END SUBROUTINE

!***************************************************************************
SUBROUTINE read_Lyman
  USE params
  USE lyman_m
  USE units
  implicit none
  namelist /lyman/ wly, ply, warg, lymanfile, lifefile
!---------------------------------------------------------------------------
  rewind (stdin); read (stdin, lyman)
  if (master) write (*, lyman)
END

!***************************************************************************
SUBROUTINE lyman_source (src)
  USE explosions
  USE params
  USE units
  USE lyman_m
  implicit none
  real, dimension(mx,my,mz):: src
  character(len=mfile) fname,name
  integer:: ldist,mdist,ndist,i,mlms_1
  real ti,Rti,progm,L_Lyman,cnorm
  integer iz
  logical debug
  character(len=mid):: id='lyman_source $Id: Lyman_luminosity.f90,v 1.7 2006/07/05 23:52:43 aake Exp $'
!-----------------------------------------------------------------------
  call print_trace (id, dbg_cool, 'BEGIN')
  !$omp single
  if (isnap .NE. isnap_Lyman) grab=0
  isnap_Lyman=isnap
  snorm = 0.                                                            ! accumulate profile
  !$omp end single
  
  if (grab .eq. 0 .and. master) then 
    mlms_1=0
    if (mlms.gt.0) then    !Count number of lms with masses gt 1. 
                           !(the ones that we'll record together with massive stars)
      do i=1,mlms 
         if (lms(i,7) .gt. 1) mlms_1=mlms_1+1
      enddo
    endif   
    fname = name('Lyman.dat','Lyman',file)
    open (11,file=fname, position='append', form='unformatted', status='unknown')
    write (11) mhms+mlms_1,t
  endif

  do iz=izs,ize                                                         ! initialize
    src(:,:,iz) = 0.                                                    ! zero source function
  end do

!  Loop over massive stars.

  do i=1,mhms
    ldist=(hms(i,20)-xm(1))/dx+.5                                       ! get stellar position 
    mdist=(hms(i,21)-ym(1))/dy+.5
    ndist=(hms(i,22)-zm(1))/dz+.5
    ldist=mod(ldist,mx)+1                                               ! this assumes not doing MPI!
    mdist=mod(mdist,my)+1
    ndist=mod(ndist,mz)+1
    
    ti =abs(t-(hms(i,1)-hms(i,14)))                                     ! get current rate of life of the star
    Rti=ti/hms(i,14)
    progm=hms(i,2)                                                      ! initial mass
    call get_Lyman_luminosity (i, Rti, L_Lyman, progm)                  ! get Lyman lumoninosity for the star

    if (debug(dbg_cool)) then
      print '(1x,a,5g12.4)',"Massive stars: mass,Rti,L_Lyman",progm,Rti,L_Lyman
      print *,"location:",ldist,mdist,ndist
    endif   

    call add_prof (ldist, mdist, ndist, L_Lyman, src, snorm, i)         ! add to the source function

    if (grab .eq. 0 .and. master) then
      write (11) progm,L_Lyman
    endif   
  enddo

  cnorm = 1./(dx*dy*dz*snorm)                                           ! snorm is the profile size in grids
  do iz=izs,ize
    src(:,:,iz) = src(:,:,iz)*cnorm                                     ! normalize with profile sum
  end do

!  Loop over low mass stars.

!  do i=1,mlms
!    ldist=(lms(i,1)-xm(1))/dx+.5 !get stellar position 
!    mdist=(lms(i,2)-ym(1))/dy+.5
!    ndist=(lms(i,3)-zm(1))/dz+.5
!    ldist=mod(ldist,mx)+1
!    mdist=mod(mdist,my)+1
!    ndist=mod(ndist,mz)+1
! 
!    progm=lms(i,7)
!    if (progm .gt. 1.) then 
!      Rti=t        
!      call get_Lyman_luminosity(i,Rti,L_Lyman,progm)
!      if (grab .eq. 0) then
!         write (11) progm,L_Lyman        
!      endif
!      if (debug(dbg_cool)) then
!         print *,"low mass stars: Masa,Rti,L_Lyman",progm,Rti,L_Lyman*(ceu/ctu)
!      endif
!    endif
!  enddo

  if (grab .eq. 0 .and. master) then
    close(11)
    grab=1        
  endif
  call print_trace (id, dbg_cool, 'END')
END SUBROUTINE
