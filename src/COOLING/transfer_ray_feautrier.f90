!***********************************************************************
SUBROUTINE transfer (np,n1,dtau,s,q,xi,doxi)
!
!  Solve the transfer equation, given optical depth and source function.
!
!  "Steins trick" is used; storing the sum of the three elements in each
!  row of the tridiagonal equation system, instead of the diagonal ele-
!  ment.  This allows accurate solutions to be otained for arbitrarily
!  small optical depths.  Without this trick, round off errors become
!  noticeable already when the smallest optical depth is less than the
!  square root of the machine precisions.
!
!  This version works with P as a variable for k <= n1, and with
!  Q = P - S for k > n1.  P is Feautriers P and S is the source function.
!  The equation solved is  Q" = P - S for k<=n1, and Q" = Q - S" for k>n1.
!  The answer returned is Q = P - S for all k.
!
!  This choice of equations produces a high precision in Q at large optical
!  depths (where P - S would have round off), while at the same time
!  retaining the precision at small optical depths (where S" becomes very
!  large and eventually would cause round off errors in the back
!  substitution).
!
!  This is a second order version.  For simulations, with rapidly
!  varying absorption coefficients and source functions, this is to be
!  preferred over spline and Hermitean versions because it is positive
!  definite, in the sense that a positive source function is guaranteed
!  to result in a positive average intensity p.  Also, the flux
!  divergence is exactly equal to q, for the conventional definition
!  of the flux.
!
!  Operation count: 5d+5m+11a = 21 flops
!
!  Timings:
!            Alliant: 0.58 * 21 / 1.25 = 9.7 Mfl @ 31*31*31
!
!  Update history:  This routine is based on transq, with updates
!
!***********************************************************************
!
  USE params
  implicit none
!
  real, dimension(my) :: dtau,s,q
  real, dimension(my) :: sp1,sp2,sp3
  integer :: k, l, iz, np, n1p, n1
  real :: a, taum, dinv, ex
  logical doxi
!
  character(len=76):: id='$Id: transfer_ray_feautrier.f90,v 1.1 2004/09/22 01:44:31 aake Exp $'

  if (id.ne.' ') print *,id ; id=' '
!
!  Matrix elements for k>2, k<np: [3d+3s]
!
    do k=2,np-1
      sp2(k)=1.
      dinv=2./(dtau(k+1)+dtau(k))
      sp1(k)=-dinv/dtau(k)
      sp3(k)=-dinv/dtau(k+1)
      q(k)=s(kz)
    end do
!
!  k>n1p+1,<np [2m+2a]
!
    do k=2,np-1
      q(k)=sp1(k)*(s(k)-s(k-1)) &
              +sp3(k)*(s(k)-s(k+1))
    end do
!
!  k=np
!
    sp2(np)=1.
    q(np)=0.0
!
!  Eliminate subdiagonal, save factors in sp1 [1d+2m+4a]
!
    do k=1,my-1
      a=-sp1(k+1)/(sp2(k)-sp3(k))
      q(k+1)=q(k+1)+a*q(k)
      sp2(k+1)=sp2(k+1)+a*sp2(k)
      sp2(k)=sp2(k)-sp3(k)
    end do
    sp2(np-1)=sp2(np-1)-sp3(np-1)
!
!  Backsubstitute [1d+1m+1a]
!
    do k=np-1,1,-1
      q(k)=(q(k)-sp3(k)*q(k+1))/sp2(k)
    end do
!
  end do    ! iz
!
  END
