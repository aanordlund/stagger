! $Id: radiation_rays.f90,v 1.74 2014/12/31 13:29:29 aake Exp $
!***********************************************************************
MODULE cooling
  USE params
  implicit none
  include 'mpif.h'
  integer, parameter:: mmu=9, mphi=10
  real dphi, form_factor, dtaumin, dtaumax
  integer nmu, nphi, verbose, ny0, iphot, ichrom
  integer mblocks
  integer n1, n2, mray
  real y_newton, dy_newton, t_newton, ee_newton, ee_min, t_ee_min, tt_limit
  real ee_chromos, ee_top, h_top, t_top, t_chromos, t_damp, r_top, d_top
  real cdt_cond, c_scale
  real, dimension(mmu):: mu0

  logical do_table, do_newton, do_limb, do_fld, do_conduction, do_corona, &
    do_top, do_chromos, do_damp, do_Casiana, do_limit, do_xi
  character(len=mfile):: intfile
  character(len=16) status

  integer, allocatable:: offset(:)
  integer, allocatable:: mhd_count(:), ray_count(:), tmp_count(:)
  integer(kind=MPI_OFFSET_KIND), allocatable:: mhd_offset(:), ray_offset(:)
  real   , allocatable:: mhd2(:,:), mhd(:)
  real   , allocatable:: ray2(:,:), ray(:)
  real   , allocatable, dimension(:)    :: xi
  real   , allocatable, dimension(:,:)  :: lnr_ray, ee_ray
  real   , allocatable, dimension(:,:)  :: rhokap, dtau, q, q_ray, s
  real   , allocatable, dimension(:,:,:):: lns3, qq, qq_tmp, qq_tot
  real   , allocatable, dimension(:,:,:):: lnr, lnr1, lnr2, ee1, ee2
  integer(kind=MPI_OFFSET_KIND):: max_offset=0
  integer, save:: ray_compute=1                                         ! compute ray counts istead of using MPI_alltoall
  integer, save:: truncate=2                                            ! 0: no truncation, 1: after dtau, 2: also next mu
END MODULE

!***********************************************************************
SUBROUTINE init_cooling (r,e)
  USE params
  USE arrays
  USE cooling
  USE units
  implicit none
  real, dimension(mx,my,mz):: r,e
  integer i
  character(len=mfile):: name
  character(len=mid),save:: &
    id='$Id: radiation_rays.f90,v 1.74 2014/12/31 13:29:29 aake Exp $'
!.......................................................................
  call print_id (id)

  do_cool = .true.
  i = index(file,'.')
  nmu = 0
  nphi = 4
  dphi = 0.
  form_factor = 1.
  dtaumax = 100.
  dtaumin = 0.1
  intfile = name('intensity.dat','int',file)
  !iphot = 300

  do_newton = .false.
  y_newton = -0.3
  dy_newton = 0.05
  t_newton = 0.01
  t_ee_min = 0.005
  ee_newton = -1.
  ee_min = 3.6
  do_limb  = .false.
  verbose = 0
  ny0 = 0
  mblocks = 1                                                           ! default: communicate only nearest neighbour
  do_fld = .false.
  do_conduction = .false.
  do_corona = .false.
  do_top = .true.
  do_damp = .false.
  do_chromos = .false.
  do_Casiana = .false.
  c_scale = 1.
  t_top = 1.
  r_top = 1e-8
  t_chromos = .1
  ee_chromos = 7.
  t_damp = 1.
  d_top = 2.
  iphot = 1
  ichrom = 0                                                            ! default; no chromoshpere even if do_chromos=t
  do_limit = .false.
  tt_limit = 5e6
  cdt_cond = 0.05

  call read_cooling
  if (ichrom==0 .and. (.not. do_chromos)) ichrom=iphot
END SUBROUTINE

!***********************************************************************
SUBROUTINE read_cooling
  USE params
  USE eos
  USE cooling
  implicit none
  namelist /cool/do_cool, nmu, nphi, dphi, form_factor, dtaumin, dtaumax, intfile, &
    do_newton, y_newton, dy_newton, t_newton, ee_newton, ee_min, t_ee_min, do_limb, mu0, &
    ny0, verbose, mblocks, do_fld, do_conduction, do_corona, do_chromos, t_chromos, r_top, &
    ee_chromos, do_Casiana, do_top, t_top, ee_top, h_top, d_top, do_damp, t_damp, iphot, ichrom, &
    c_scale, do_limit, tt_limit, cdt_cond, timer_verbose, ray_compute, truncate
  character(len=mfile):: name, fname

  rewind (stdin); read (stdin,cool)
  if (nmu==0 .and. form_factor==1.) form_factor=0.4
  if (master) write (*,cool)
  if (master) write (*,*) 'mbox, dbox =', mbox, dbox
  if (do_limb) then
    fname = name('limb.dat','lmb',file)
    call mpi_name(fname)
    open (limb_unit, file=trim(fname), &
      form='unformatted', status='unknown')
    write (limb_unit) mx, mz,nmu,nphi
  end if
END SUBROUTINE

!***********************************************************************
SUBROUTINE coolit (r, ee, lne, dd, dedt)
  USE params
  USE units
  USE eos
  USE cooling
  USE mpi_mod, only: new_com, mpi_com
  implicit none
  real, dimension(mx,my,mz):: r, ee, lne, dd, dedt
  real, dimension(mmu):: xmu, wmu
  integer:: lmu, lphi, imu, iphi, ibox, iray
  real   :: sec, dxdy, dzdy, dixdy, dizdy, tanth, phi, wphi, womega, f1, f
  logical:: debug, debug2
  logical, save:: first=.true., first_time=.true.
  integer, save:: nrad=0, nang=0
  character(len=mfile):: name, fname
  integer:: ix, iy, iz, jx, jy, jz, jxoff, jyoff, jzoff, rx, rz, &
            ixo, izo, rank, rank1, rank2, i1, i2, r1, r2, mpi_err, &
            rx1, rx2, rz1, rz2, ncell, nfail, &
            nmhd, nrays, nr, coord(3)
  integer(kind=8):: nsend, nsent, ncalls
  integer(kind=8):: nC0=0, nC1=0, nC2=0
  integer, parameter:: mseq=4
  integer is, nseq, s1(mseq), s2(mseq)
  integer(kind=8):: hseq(mseq), lseq(mx)
  integer:: ny_mu, ny_phi, ny_bin, ny_tau
  integer, save:: ny_rem=999999
  integer:: min_mu, min_n1, min_bn
  integer:: max_mu, max_n1, max_bn
  integer:: mhd_active, ray_active, mhd_max, ray_max
  real(8):: fray
!.......................................................................
! ny_rem: previous value of ny_mu
! ny_mu : depth points to be used for table lookup (>= ny_rem+5)
! ny_phi: max of depth points used for any direction (<= ny_mu)
! ny_bin: depth points to be used for radiative transfer (<= ny_phi)
! ny_tau: smallest depth point with dtau >= dtau_max (<= ny_bin)
!.......................................................................
  if (.not. do_cool) return
  call print_trace ('radiation_rays',dbg_rad,'begin')

  if (lb > 1) then                                                      ! if we have ghost zones
    dedt(:,1:lb-1,1:mz) = 0.                                            ! zap previous dedt there
  end if
  hseq = 0
  lseq = 0

!-----------------------------------------------------------------------
!  Angular quadrature
!  nmu > 0: Gauss integration (mu=1 excluded) with 2*nmu   order accuracy
!  nmu = 0: One vertical ray with weight 0.5
!  nmu < 0: Radau integration (mu=1 included) with 2*nmu-1 order accuracy
!-----------------------------------------------------------------------
  if (nmu .gt. 0) then
    lmu = nmu
    call gausi (lmu,0.,1.,wmu,xmu)
  else if (nmu .eq. 0) then
    lmu = 1
    xmu(1) = 1.
    wmu(1) = 0.5
  else 
    lmu =-nmu
    call radaui(lmu,0.,1.,wmu,xmu)
  end if
  if (mu0(1) .ne. 0.) xmu=mu0
  if (first) then
    call barrier_omp ('rad-first')
    if (master) then
      print *,' imu       xmu    theta'
      !           1.....0.000.....00.0
      do imu=1,lmu 
        print '(i4,f10.3,f8.1,f10.3)',imu,xmu(imu),acos(xmu(imu))*180./pi,wmu(imu)
      end do
    end if
  end if

  call print_trace ('radiation_rays',dbg_rad,'allocate')

  !$omp single
  allocate (lnr(mx,my,mz), qq_tot(mx,my,mz))
  !$omp end single
  do iz=izs,ize
    lnr(:,:,iz) = alog(r(:,:,iz))
    qq_tot(:,:,iz) = 0.0
  end do
!-----------------------------------------------------------------------
! Loop over ray directions.  For each direction, interpolate (lnr,ee)
! pairs to the ray-plane intersection points, store them in a send buffer,
! send them to RT-processing, and receive the heating / cooling rates back.
!
! Index name conventions:
!
! i[xyz] = local mhd coordinates
! j[xyz] = global coordinates
! r[xyz] = ray plane coordinates
!
! There are five blocks of code below with essentially the same index
! calculations.  Since they run through MHD-domains in the same order
! the index references are automatically consistent, and do not need
! to be communicated.  The blocks are:
!
! A) Calculate how many points to send to each RT-domain, to be able to
!    allocate an MPI_ALLTOALLV buffer and compute offsets into the buffer
! B) Store (lnrho,ee) value pairs into the buffer
! C) Unpack these values in the RT-domain and compute Q
! D) Pack the Q-values into a new buffer, with the same offsets as in C
! E) Send to MHD-domains, and unpack, reversing what was done in B
!-----------------------------------------------------------------------
  nsent = 0_8
  ncalls = 0_8
                                         call timer('radiation','begin')

  if (verbose > 2) then
    write (fname,'(a,i3.3,a)') 'ray_',mpi_rank,'.dat'
    open (101,file=fname,status='unknown',form='unformatted')
  endif

  status = 'replace'
  if (verbose > 2) then
    write (fname,'(a,i3.3,a)') 'rayA_',mpi_rank,'.dat'
    open (100,file=fname,status=status,form='formatted',position='append')
    close (100)
  end if

  call print_trace('radiation_rays',dbg_rad,'begin loop over omega')
  ny_mu = min(ny_rem+5,mytot)                                            ! start with 5 more than last step
  if (truncate==0) ny_mu = mytot
  if (do_dump) ny_mu = mytot
  do imu=lmu,1,-1
    if (nmu .eq. 0 .and. imu .eq. 1) then
      lphi = 1
    else if (nmu .lt. 0 .and. imu .eq. lmu) then
      lphi = 1 
    else
      lphi = nphi
    end if
    tanth = tan(acos(xmu(imu)))
    wphi = 4.*pi/lphi
    womega = wmu(imu)*wphi*form_factor
    ny_phi = 0
    do iphi=1,lphi
      n_omega = n_omega + 1
      phi = (iphi-1)*2.*pi/lphi + dphi*t
      dxdy = tanth*cos(phi)
      dzdy = tanth*sin(phi)
      dixdy = dxdy/dx                                                   ! coefficient for x-index change
      dizdy = dzdy/dz                                                   ! coefficient for z-index change
      if (abs(dixdy)<1e-3) dixdy=0.0                                    ! avoid short index sequences
      if (abs(dizdy)<1e-3) dizdy=0.0                                    ! avoid short index sequences
!-----------------------------------------------------------------------
! Allocate mhd and rad size and offset arrays
!-----------------------------------------------------------------------
     !$omp single
      allocate (mhd_count(0:mpi_size-1), mhd_offset(0:mpi_size-1))
      allocate (ray_count(0:mpi_size-1), ray_offset(0:mpi_size-1))
      allocate (tmp_count(0:mpi_size-1))
!-----------------------------------------------------------------------
! Interpolate rho and ee to ray-plane intersection points. Indexing is
! such that the value is stored at the lower of the two indices if the
! interpolation is linear. 
!-----------------------------------------------------------------------
      allocate (lnr1(mx,my,mz), ee1(mx,my,mz))
      allocate (lnr2(mx,my,mz), ee2(mx,my,mz))
     !$omp end single
      call interpolate_xup (lnr,  dxdy, lnr1, ny_mu)
                 if (timer_verbose>1) call timer('radiation','interp_x')
      call interpolate_zup (lnr1, dzdy, lnr2, ny_mu)
                 if (timer_verbose>1) call timer('radiation','interp_z')
     !$omp barrier
      call interpolate_xup ( ee,  dxdy,  ee1, ny_mu)
                 if (timer_verbose>1) call timer('radiation','interp_x')
      call interpolate_zup (ee1,  dzdy,  ee2, ny_mu)
                 if (timer_verbose>1) call timer('radiation','interp_z')
     !$omp barrier
     !$omp single
      n_trnslt = n_trnslt + 2*mx*ny_mu*mz
      if (verbose > 3) then
        write(101) mx,my,mz
        write(101) lnr2
        write(101) ee2
      endif
     
      deallocate (lnr1, ee1)
     !$omp end single
!-----------------------------------------------------------------------
! A) Walk through the local MHD-domain, figure out which RT-ranks to send 
! to, and how much to send to each
!-----------------------------------------------------------------------
      call print_trace('radiation_rays',dbg_rad,'block A')
      fray = real(mxtot*mztot,kind=8)/real(mpi_size,kind=8)
      mray = ceiling(fray)                                              ! number of rays per domain
      !if (mpi_rank==0) print *,'mray =',mray
      mhd_count(0:mpi_size-1) = 0                                       ! number of points owned by HD-ranks
      ncell = 0

      if (verbose > 1) then
        write (fname,'(a,i3.3,a)') 'rayA_',mpi_rank,'.dat'
        open (100,file=fname,status=status,form='formatted',position='append')
        write (100,*) 'omega:',imu,iphi,dixdy,dizdy,womega
      endif

      do iy=1,my                                                        ! loop over vertical
        jy = iy+iyoff
        if (jy > ny_mu) cycle
        ixo = floor(ymg(jy)*dixdy)                                      ! index shift in x 
        izo = floor(ymg(jy)*dizdy)                                      ! index shift in z
        if (verbose > 1) write(100,'(a)') &
          '    jy  iz ixo izo     r1     r2  rank1 rank2  n1  n2  ncell'
        do iz=1,mz                                                      ! front to back
          call mhd2ray( 1,ixoff,izoff,rank1,r1)                         ! 1st point projection
          call mhd2ray(mx,ixoff,izoff,rank2,r2)                         ! 2nd point projection
          n1 = mhd_count(rank1)
          n2 = mhd_count(rank2)
          if (rank1==rank2 .and. (r2-r1)==(mx-1)) then                  ! rame rank, consequtive
            mhd_count(rank1) = mhd_count(rank1) + mx                    ! increment rank1 count
          else
            do ix=1,mx
              call mhd2ray(ix,ixoff,izoff,rank,r1)                      ! point projection
              mhd_count(rank) = mhd_count(rank)+1                       ! increment count
            end do
          end if
          n1 = mhd_count(rank1) - n1
          n2 = mhd_count(rank2) - n2
          ncell = ncell + mx
          if (verbose > 1) write(100,'(a,4i4,3i7,i6,2i4,2i7)') &
          'A:',jy,iz,ixo,izo,r1,r2,rank1,rank2,n1,n2,ncell,ny_mu
        end do                                                          ! end iz loop
      end do                                                            ! end iy loop
      nmhd = sum(mhd_count(0:mpi_size-1))                            ! points in all ranks
      !print*,mpi_rank,' nmhd =',nmhd
      ncalls = ncalls + 2
!-----------------------------------------------------------------------
! Allocate the mhd buffer and compute the offsets into the mhd buffer 
! for each rank.  nmhd should be the same as mx*my*mz, unless some
! points are not being used (e.g. because of optical depth limits).
!-----------------------------------------------------------------------
      allocate (mhd2(2,nmhd))
      mhd_offset(0) = 0
      do rank=1,mpi_size-1
        mhd_offset(rank) = mhd_offset(rank-1) + mhd_count(rank-1)       ! offsets into the mhd buffer
      end do
      max_offset = maxval(mhd_offset)
                     if (timer_verbose>0) call timer('radiation','partA')
!-----------------------------------------------------------------------
! B) Add all pieces into the buffer, keeping track of index offsets
!-----------------------------------------------------------------------
      call print_trace('radiation_rays',dbg_rad,'block B')
      if (verbose > 2) then
        close(100)
        write (fname,'(a,i3.3,a)') 'rayB_',mpi_rank,'.dat'
        open (100,file=fname,status=status,form='formatted',position='append')
      end if
      if (verbose > 1) then
        write(100,*) 'mhd_count =',mhd_count
        write(100,*) 'nmhd =',nmhd
        write (100,*) 'omega:',imu,iphi,dixdy,dizdy,womega
      end if

      allocate (offset(0:mpi_size-1))                                   ! allocate index array
      offset(0:mpi_size-1) = mhd_offset(0:mpi_size-1) + 1               ! index to buffer pieces
      ncell = 0
      do iy=1,my                                                        ! loop over vertical
        jy = iy+iyoff
        if (jy > ny_mu) cycle
        ixo = floor(ymg(jy)*dixdy)                                      ! index shift in x 
        izo = floor(ymg(jy)*dizdy)                                      ! index shift in z
        if (verbose > 1) write(100,'(a)') &
          '     iy  iz ixo izo     r1     r2  rank1 rank2  offst1  offst2   ncell'
        do iz=1,mz                                                      ! front to back
          call mhd2ray( 1,ixoff,izoff,rank1,r1)                         ! 1st point projection
          call mhd2ray(mx,ixoff,izoff,rank2,r2)                         ! 2nd point projection
          if (rank1==rank2 .and. (r2-r1)==(mx-1)) then                  ! rame rank, consequtive
            i1 = offset(rank1)
            i2 = i1 + (mx-1)
            mhd2(1,i1:i2) = lnr2(1:mx,iy,iz)                            ! store lnr
            mhd2(2,i1:i2) =  ee2(1:mx,iy,iz)                            ! store ee
            offset(rank1) = i2 + 1
          else                                                          ! wrapped
            do ix=1,mx
              call mhd2ray(ix,ixoff,izoff,rank1,r1)                     ! point projection
              i1 = offset(rank1)                                        ! buffer offset
              mhd2(1,i1) = lnr2(ix,iy,iz)                               ! store lnr
              mhd2(2,i1) =  ee2(ix,iy,iz)                               ! store ee
              offset(rank1) = i1 + 1                                    ! increment offset
            end do
          end if
          ncell = ncell + mx                                            ! count cells
          if (verbose > 1) write(100,'(a,4i4,3i7,i6,3i8)')  &
            'B:',iy,iz,ixo,izo,r1,r2,rank1,rank2, &
            offset(rank1),offset(rank2),ncell
        end do                                                          ! end iz loop
      end do                                                            ! end iy loop
      deallocate (lnr2, ee2)  
                     if (timer_verbose>0) call timer('radiation','partB')

!-----------------------------------------------------------------------
! Swap roles from being an MHD domain to being an RT domain. Figure out 
! who we should receive from, and how much.  Each HD-rank has already 
! computed how much to send to us, so we just need to ask everyone to 
! pass us that information.
!-----------------------------------------------------------------------
      i1 = 1
      !print*,mpi_rank,'calling alltoall'
      call alltoall_mpi (mhd_count, ray_count, i1, MPI_INTEGER, &
                         mpi_com, .true., mpi_err)
                  if (timer_verbose>1) call timer('radiation','alltoall')

      !-------------------------------------------------------------------------
      ! Figure out how many other ranks a rank communicate with, on average
      !-------------------------------------------------------------------------
      if (first_time) then
        mhd_active = sum(merge(1,0,mhd_count>0))
        ray_active = sum(merge(1,0,ray_count>0))
        mhd_max = mhd_active
        ray_max = ray_active
        call sum_int_mpi (mhd_active)
        call sum_int_mpi (ray_active)
        call max_int_mpi (mhd_max)
        call max_int_mpi (ray_max)
        if (master) then
          print'(1x,a,2i8, 2f10.1)', 'max and average active MHD and RT connections =', &
            mhd_max, ray_max, mhd_active/real(mpi_size), ray_active/real(mpi_size)
        end if
      end if

      !if (first_time .and. verbose>0) then
      !  print*,mpi_rank,'mhd_count',mhd_count
      !  print*,mpi_rank,'ray_count',ray_count
      !end if
      if (verbose > 1) write(100,*)'ray_count', ray_count               ! save to file ?

      nrays = sum(ray_count(0:mpi_size-1))                              ! points in all ranks
      !print*,mpi_rank,' nrays =',nrays
      ray_offset(0) = 0                                                 !  offsets
      do rank=1,mpi_size-1
        ray_offset(rank) = ray_offset(rank-1) + ray_count(rank-1)
      end do      
      if (verbose > 1) write(100,*)'ray_offset', ray_offset

!-----------------------------------------------------------------------
! Send data from HD-ranks to RT-ranks
!-----------------------------------------------------------------------
      if (verbose >2) then
        do rank=0,mpi_size-1
         if (rank==mpi_rank) &
           print'(1x,a,i5,(8i10))','mhd_count',rank,mhd_count
           call barrier_mpi('print')
        end do
        call barrier_mpi('print')
        call sleep(1)
        do rank=0,mpi_size-1
           if (rank==mpi_rank) &
           print'(1x,a,i5,(8i10))','ray_count',rank,ray_count
           call barrier_mpi('print')
        end do
      end if
      allocate (ray2(2,nrays))
      !print*,mpi_rank,'allocated ray2'
      mhd_count = mhd_count*2
      ray_count = ray_count*2
      mhd_offset = mhd_offset*2
      ray_offset = ray_offset*2
      !print*,mpi_rank,'calling alltoallv (1)'
      call alltoallv_mpi (mhd2,  nmhd*2, mhd_count, mhd_offset, &
                          ray2, nrays*2,ray_count, ray_offset, &
                          MPI_REAL, mpi_rank, mpi_size, mpi_com, &
                          use_isend, verbose, nsent)
      !print*,mpi_rank,'returned alltoallv (1)'
      mhd_count = mhd_count/2
      ray_count = ray_count/2
      mhd_offset = mhd_offset/2
      ray_offset = ray_offset/2
      if (verbose >2) then
        call barrier_mpi('print')
        call sleep(2)
        call barrier_mpi('print')
      end if
      deallocate (mhd2)
                                   if (use_isend==0) then
                if (timer_verbose>0) call timer('radiation','alltoallv')
                                   else 
                if (timer_verbose>0) call timer('radiation','Isendrecv')
                                   end if
!-----------------------------------------------------------------------
! C) Unpack into our RT-domain.  For each rank that contributes we find
! its position in the cartesian mesh, and reconstruct the index strip we 
! need to unpack from the buffer.  The strips are coming in sequential
! order, since the ranks that contributed were handled in sequential 
! order. 
!
! NOTE: If the number of empty ranks is very large it may be helpful
! to do an ALLTOALL with a list of the relevant ranks being sent to all
! ranks.
!-----------------------------------------------------------------------
      call print_trace('radiation_rays',dbg_rad,'block C')
      if (verbose > 2) then
        close (100)
        write (fname,'(a,i3.3,a)') 'rayC_',mpi_rank,'.dat'
        open (100,file=fname,status=status,form='formatted',position='append')
      end if
      if (verbose > 1) write(100,*) 'omega:',imu,iphi,dixdy,dizdy,womega
      if (verbose > 1) write(100,*) 'mhd_offset', mhd_offset
      if (verbose > 1) Write(100,*) 'ray_offset', ray_offset
      if (verbose > 1) Write(100,*) 'ray_count ', ray_count

      allocate (lnr_ray(mray,ny_mu), ee_ray(mray,ny_mu))                ! lnrho and ee along rays  
      lnr_ray = 0.0; ee_ray = 0.0
      offset(0:mpi_size-1) = ray_offset(0:mpi_size-1) + 1               ! offset to mhd_ranks
      periodic(1:3) = .true.
      do rank=0,mpi_size-1                                              ! this is the MHD-rank
        if (ray_count(rank) == 0) cycle                                 ! rank doesn't contribute
        call MPI_CART_COORDS(new_com, rank, 3, coord, mpi_err)          ! cartesian MPI coordinates
        jxoff = coord(1)*mx                                             ! MHD domain x-offset
        jyoff = coord(2)*my                                             ! MHD domain y-offset
        jzoff = coord(3)*mz                                             ! MHD domain z-offset
        do iy=1,my                                                      ! loop over vertical
          jy = iy + jyoff
          if (jy > ny_mu) cycle
          ixo = floor(ymg(jy)*dixdy)                                    ! index shift in x 
          izo = floor(ymg(jy)*dizdy)                                    ! index shift in z
          if (verbose > 1) write(100,'(a)') &
            '     iy  iz ixo izo  rank1 rank2    r1    r2    i1    i2 ncell  rank offst'
          do iz=1,mz                                                    ! front to back
            call mhd2ray( 1,jxoff,jzoff,rank1,r1)                       ! 1st point projection
            call mhd2ray(mx,jxoff,jzoff,rank2,r2)                       ! 2nd point projection
            i1 = 0
            if (rank1==mpi_rank .and. &
                rank2==mpi_rank .and. (r2-r1)==(mx-1)) then             ! our rank, consequtive
              i1 = offset(rank)
              i2 = i1 + (mx-1)
              if (i2 > nrays .or. r2 > mray) then
                print'(i5,1x,a,8i8)',mpi_rank, &
                  ' ERROR: rank,i1,i2,r2,mray,jxoff,jzoff =', &
                           rank,i1,i2,r2,mray,jxoff,jzoff
              end if
              lnr_ray(r1:r2,jy) = ray2(1,i1:i2)
               ee_ray(r1:r2,jy) = ray2(2,i1:i2)
              offset(rank) = i2 + 1
              nC1 = nC1+mx
            else                                                        ! wrapped
              ix = 1
              do while (ix <= mx)
                jx = modulo(ix+jxoff-ixo-1,mxtot)                       ! absolute x
                nr = min(mx-ix+1,mxtot-jx)                              ! distance to mxtot wrap
                jz = modulo(iz+jzoff-izo-1,mztot)                       ! absolute z
                r1 = jx+jz*mxtot                                        ! absolute ray nr
                rank1 = r1/fray                                         ! rank
                r1 = r1-rank1*fray+1                                    ! ray nr
                nr = min(nr,int(fray)-r1+1)                             ! distance to mray wrap
                if (rank1==mpi_rank) then                               ! sequence for this rank
                  r2 = r1+nr-1                                          ! end of sequence
                  i1 = offset(rank)                                     ! start of buffer
                  i2 = i1 + (r2-r1)                                     ! end of buffer
                  lnr_ray(r1:r2,jy) = ray2(1,i1:i2)
                   ee_ray(r1:r2,jy) = ray2(2,i1:i2)
                  offset(rank) = i2 + 1
                  nC2 = nC2 + nr
                end if
                ix = ix + nr                                            ! next to check
              end do
              nC0 = nC0 + mx
            end if
            if (i1>0 .and. verbose>1) write(100,'(a,4i4,2i6,4i6,3i6)')  &
              'C:',iy,iz,ixo,izo,rank1,rank2, &
               r1,r2,i1,i2,ncell,offset(rank)
          end do                                                        ! end iz loop
        end do                                                          ! end iy loop
      end do

      if (verbose > 2) then
        write(101) mray,ny_mu
        write(101) lnr_ray
        write(101) ee_ray
      end if

      deallocate (ray2)
      !print*,mpi_rank,'deallocated ray2'
                  if (timer_verbose >0) call timer('radiation',  'partC')
                 !if (timer_verbose==1) call timer('radiation','mhd2ray')
!-----------------------------------------------------------------------
! Loop over opacity bins
!-----------------------------------------------------------------------
      if (verbose > 2) then
        close (100)
        write (fname,'(a,i3.3,a)') 'rayD_',mpi_rank,'.dat'
        open (100,file=fname,status=status,form='formatted',position='append')
      end if
      if (verbose > 1) write (100,*) 'omega:',imu,iphi,dixdy,dizdy,womega

      allocate (rhokap(mray,ny_mu), dtau(mray,ny_mu), lns3(mray,ny_mu,mbox))
      allocate (q_ray(mray,ny_mu), xi(mray))
      allocate (q(mray,ny_mu))
      allocate (s(mray,ny_mu))

      do iray=1,mray
        if (lnr_ray(iray,1)==0.0 .and. ee_ray(iray,1)==0.0) then
          if (first_time) print*,mpi_rank,' rank ray not used:', iray
          lnr_ray(iray,:) = lnr_ray(iray,10)
           ee_ray(iray,:) =  ee_ray(iray,10)
        end if
      end do

      call lookup_rays (mray,ny_mu,lnr_ray,ee_ray,rhokap,lns3)          ! compute rhokap and S
                  if (timer_verbose>1) call timer('radiation',  'lookup')
      q_ray(:,:) = 0.0                                                  ! initialize
      call dumpn(lnr_ray,'lnr_ray','rays.dmp',1)
      call dumpn( ee_ray, 'ee_ray','rays.dmp',1)
      call dumpn( rhokap, 'rhokap','rays.dmp',1)
      call print_trace('radiation_rays',dbg_rad,'ibox loop')
      ny_bin = ny_mu                                                    ! default for this mu
      do ibox=1,mbox                                                    ! bin index
        ny_tau = ny_bin                                                 ! start from previous value
        call dtau_rays(mray,ny_bin,rhokap,xmu(imu),dtau,n1,ny_tau,ibox) ! compute dtau from rhokap

        if (master .and. debug2(dbg_rad,2)) print'(a,7i6)', &
          ' ibox, iphi, imu, ny_bin, ny_tau, ny_phi, ny_mu', &
            ibox, iphi, imu, ny_bin, ny_tau, ny_phi, ny_mu

        ny_bin = ny_tau                                                 ! smaller or equal to ny_bin
        if (master .and. verbose==-1) then
          print'(1x,a,9i6)', 'imu, iphi, ibox, ny_rem, ny_mu, ny_bin =', &
                              imu, iphi, ibox, ny_rem, ny_mu, ny_bin
        end if
                  if (timer_verbose>1) call timer('radiation',    'dtau')
        if (ny_bin > ny_mu) then
          print *,'ERROR: rank,ny_bin,ny_mu =', mpi_rank,ny_bin,ny_mu
          STOP
        end if
        !min_mu=ny_mu ; call min_int_mpi(min_mu); max_mu=ny_mu ; call max_int_mpi(max_mu)
        !min_n1=n1    ; call min_int_mpi(min_n1); max_n1=n1    ; call max_int_mpi(max_n1)
        !min_bn=ny_bin; call min_int_mpi(min_bn); max_bn=ny_bin; call max_int_mpi(max_bn)
        !if (master) print'(a,3(2x,2i4))','transfer:min_mu,max_mu,min_n1,max_n1,min_bn,max_bn', &
        !                                           min_mu,max_mu,min_n1,max_n1,min_bn,max_bn
        call print_trace('radiation_rays',dbg_rad,'source')
        do jy=1,ny_bin
          do jx=1,mray
            s(jx,jy)=exp(min(max(lns3(jx,jy,ibox),-70.0),+70.0))
          end do
        end do
        call print_trace('radiation_rays',dbg_rad,'transfer call')
        do_xi = .true.
        call transfer_rays(mray,ny_mu,n1,ny_bin,dtau,s,q,xi,do_xi)      ! solve radiative transfer
                  if (timer_verbose>1) call timer('radiation','transfer')
        n_transfer = n_transfer + mray*ny_bin
        call print_trace('radiation_rays',dbg_rad,'transfer return')
        call dumpn(dtau,'dtau','rays.dmp',1)
        call dumpn(s,'s','rays.dmp',1)
        if (do_dump) q(:,ny_bin:ny_mu) = 0.
        call dumpn(q,'q','rays.dmp',1)
        f = xmu(imu)/(ymg(2)-ymg(1))
        q(:,1) = q(:,1)*dtau(:,2)*f
        do jy=2,ny_bin-1
          f = xmu(imu)/(ymg(jy+1)-ymg(jy-1))
          q(:,jy) = q(:,jy)*(dtau(:,jy+1)+dtau(:,jy))*f
        end do
        q_ray(:,1:ny_bin-1) = q_ray(:,1:ny_bin-1) + q(:,1:ny_bin-1)                 ! add the bin contribution
        call dumpn(q,'q','rays.dmp',1)
        if (verbose > 0 .and. master) then
          if (ibox==1) then
            print'(a,/,(1p,10e12.4))','lnr'   ,lnr_ray(1,1:ny_bin)
            print'(a,/,(1p,10e12.4))','ee'    , ee_ray(1,1:ny_bin)
            print'(a,/,(1p,10e12.4))','rhokap', rhokap(1,1:ny_bin)
          end if
          print'(a,5i5,1p,4g12.4)','imu,iph,ibox,n1,n2,average(s,q)', &
            imu,iphi,ibox,n1,ny_tau, &
            sum(s(:,1:ny_bin))/(mray*ny_bin), &
            sum(q(:,1:ny_bin))/(mray*ny_bin)
          print'(a,/,(1p,10e12.4))',   's',   s(1,1:ny_bin)
          print'(a,/,(1p,10e12.4))','dtau',dtau(1,1:ny_bin)
          print'(a,/,(1p,10e12.4))',   'q',   q(1,1:ny_bin)
        end if
                  if (timer_verbose>1) call timer('radiation','add_up')
        if (ibox==1) ny_phi = max(ny_phi, ny_bin)
      end do ! box
      if (verbose > 0) print'(a,3i5,29x,1p,g12.4)','rank,imu,iphi,average(q_ray)', &
           mpi_rank,imu,iphi,sum(q_ray)/(mray*ny_mu)
      deallocate (rhokap, dtau, lns3, s)
      deallocate (lnr_ray, ee_ray)
      deallocate (q)
      call dumpn(q_ray,'q_ray','rays.dmp',1)
      call print_trace('radiation_rays',dbg_rad,'end C')
!-----------------------------------------------------------------------
! D) Store radiative heating / cooling rate in buffer
!-----------------------------------------------------------------------
      call print_trace('radiation_rays',dbg_rad,'block D')
      allocate (ray(nrays))
      allocate (mhd(nmhd))
      offset(0:mpi_size-1) = ray_offset(0:mpi_size-1) + 1               ! offset to mhd_ranks
      do rank=0,mpi_size-1
        if (ray_count(rank) == 0) cycle                                 ! ranks doesn't contribute
        call MPI_CART_COORDS(new_com, rank, 3, coord, mpi_err)          ! cartesian MPI coordinates
        jxoff = coord(1)*mx                                             ! MHD domain x-offset
        jyoff = coord(2)*my                                             ! MHD domain y-offset
        jzoff = coord(3)*mz                                             ! MHD domain z-offset
        do iy=1,my                                                      ! loop over vertical
          jy = iy + jyoff                                               ! global coordinate
          if (jy > ny_mu) cycle                                        ! skip -- no contribution
          ixo = floor(ymg(jy)*dixdy)                                    ! index shift in x 
          izo = floor(ymg(jy)*dizdy)                                    ! index shift in z
          if (verbose > 1) write(100,'(a)') &
            '     iy  iz ixo izo  rank1 rank2    r1    r2    i1    i2 ncell offs1 offs2'
          do iz=1,mz                                                    ! front to back
            call mhd2ray( 1,jxoff,jzoff,rank1,r1)                       ! 1st point projection
            call mhd2ray(mx,jxoff,jzoff,rank2,r2)                       ! 2nd point projection
            i1 = 0
            if (rank1==mpi_rank .and. &
                rank2==mpi_rank .and. (r2-r1)==(mx-1)) then             ! our rank, consequtive
              i1 = offset(rank)
              i2 = i1 + (mx-1)
              ray(i1:i2) = q_ray(r1:r2,jy)
              offset(rank) = i2 + 1
            else                                                        ! wrapped
              ix = 1
              do while (ix <= mx)
                jx = modulo(ix+jxoff-ixo-1,mxtot)                       ! absolute x
                nr = min(mx-ix+1,mxtot-jx)                              ! distance to mxtot wrap
                jz = modulo(iz+jzoff-izo-1,mztot)                       ! absolute z
                r1 = jx+jz*mxtot                                        ! absolute ray nr
                rank1 = r1/fray                                         ! rank
                r1 = r1-rank1*fray+1                                    ! ray nr
                nr = min(nr,int(fray)-r1+1)                             ! distance to mray wrap
                if (rank1==mpi_rank) then                               ! sequence for this rank
                  r2 = r1+nr-1                                          ! end of sequence
                  i1 = offset(rank)                                     ! start of buffer
                  i2 = i1 + (r2-r1)                                     ! end of buffer
                  ray(i1:i2) = q_ray(r1:r2,jy)
                  offset(rank) = i2 + 1
                end if
                ix = ix + nr                                            ! next to check
              end do
            end if
            if (i1>0 .and. verbose>1) write(100,'(a,4i4,2i6,4i6,2i6)')  &
              'D:',jy,iz,ixo,izo,rank1,rank2, &
               r1,r2,i1,i2,ncell,offset(rank)
          end do                                                        ! end iz loop
        end do                                                          ! end iy loop
      end do
      deallocate (q_ray, xi)
                     if (timer_verbose>0) call timer('radiation','partD')
!-----------------------------------------------------------------------
! Send data from RT-ranks to HD-ranks
!-----------------------------------------------------------------------
      !print*,mpi_rank,'calling alltoallv (2)'
      call print_trace('radiation_rays',dbg_rad,'alltoallv call')
      call alltoallv_mpi (ray, nrays, ray_count, ray_offset, &
                          mhd,  nmhd, mhd_count, mhd_offset, &
                          MPI_REAL, mpi_rank, mpi_size, mpi_com, &
                          use_isend, verbose, nsent)
      call print_trace('radiation_rays',dbg_rad,'alltoallv return')
      !print*,mpi_rank,'returned alltoallv (2)'
      deallocate (ray_count, ray_offset)
      deallocate (tmp_count)
      deallocate (ray)
                                   if (use_isend==0) then
               if (timer_verbose>0) call timer('radiation','alltoallv2')
                                   else 
               if (timer_verbose>0) call timer('radiation','Isendrecv2')
                                   end if
!-----------------------------------------------------------------------
! E) Pick up heating / cooling from RT by unpacking rad buffer into place
!-----------------------------------------------------------------------
      call print_trace('radiation_rays',dbg_rad,'block E')
      if (verbose > 2) then
        close (100)
        write (fname,'(a,i3.3,a)') 'rayE_',mpi_rank,'.dat'
        open (100,file=fname,status=status,form='formatted',position='append')
      end if
      if (verbose > 1) write (100,*) 'omega:',imu,iphi,dixdy,dizdy,womega

      allocate (qq(mx,my,mz)); qq(:,:,:)=0.0
      offset(0:mpi_size-1) = mhd_offset(0:mpi_size-1) + 1               ! index to buffer pieces
      do iy=1,my                                                        ! loop over vertical
        jy = iy+iyoff
        if (jy > ny_mu) cycle
        ixo = floor(ymg(jy)*dixdy)                                      ! index shift in x 
        izo = floor(ymg(jy)*dizdy)                                      ! index shift in z
        if (verbose > 1) write(100,'(a)') &
          '     iy  iz ixo izo  rank1 rank2     i1     i2  ncell  offs1  offs2'
        do iz=1,mz                                                      ! front to back
          call mhd2ray( 1,ixoff,izoff,rank1,r1)                         ! 1st point projection
          call mhd2ray(mx,ixoff,izoff,rank2,r2)                         ! 2nd point projection
          n1 = mhd_count(rank1)
          n2 = mhd_count(rank2)
          if (rank1==rank2 .and. (r2-r1)==(mx-1)) then                  ! rame rank, consequtive
            i1 = offset(rank1)
            i2 = i1 + (mx-1)
            qq(1:mx,iy,iz) = mhd(i1:i2)                                 ! store Q
            offset(rank1) = i2 + 1
          else                                                          ! wrapped
            do ix=1,mx
              call mhd2ray(ix,ixoff,izoff,rank1,r1)                     ! point projection
              i1 = offset(rank1)                                        ! buffer offset
              qq(ix,iy,iz) = mhd(i1)                                    ! store Q
              offset(rank1) = i1 + 1                                    ! increment offset
            end do
          end if
          if (verbose > 1) write(100,'(a,4i4,i7,i6,5i7)')  &
            'E:',iy,iz,ixo,izo,rank1,rank2,i1,i2, &
            ncell,offset(rank1),offset(rank2)
        end do                                                          ! end iz loop
      end do                                                            ! end iy loop
      deallocate (mhd_count, mhd_offset)
      deallocate (offset)
      deallocate (mhd)
      call print_trace('radiation_rays',dbg_rad,'end E')
                     if (timer_verbose>0) call timer('radiation','partE')
!-----------------------------------------------------------------------
! End of loops over ray-direction, interpolate back to cartesian mesh
! and add to global Q
!-----------------------------------------------------------------------
      allocate (qq_tmp(mx,my,mz)); qq_tmp(:,ny_mu:,:)=0.0
      call interpolate_xdn (qq    , -dxdy, qq_tmp, ny_mu)
                 if (timer_verbose>1) call timer('radiation','interp_x')
      call interpolate_zdn (qq_tmp, -dzdy, qq    , ny_mu)
                 if (timer_verbose>1) call timer('radiation','interp_z')
      deallocate (qq_tmp)
      n_trnslt = n_trnslt + mx*ny_mu*mz
      call dumpn(qq,'qq','rays.dmp',1)
      do iz=1,mz
        do iy=1,my
          if (iy+iyoff > ny_mu) cycle
          qq_tot(:,iy,iz) = qq_tot(:,iy,iz) + qq(:,iy,iz)*womega        ! integrate over rays
        end do
      end do
      if (verbose > 0) print'(a,3i5,41x,1p,g12.4)','rank,imu,iphi,average(qq)   ', &
        mpi_rank,imu,iphi,sum(qq)/mw
      
      deallocate (qq)
      if (omp_master) then
        first = .false.
        status = 'unknown'
      end if
      if (verbose > 1) close (100)
      call print_trace('radiation_rays',dbg_rad,'end iphi loop')
    end do                                                              ! end phi loop
    call max_int_mpi (ny_phi)                                           ! for new ray directions
    if (truncate > 0 .and. imu==lmu) ny_mu = ny_phi
    if (truncate > 1 .and. imu==lmu) ny_rem = ny_phi                    ! remember ny_mu for vertical ray
    if (truncate > 2) ny_mu = ny_phi                                    ! remember for next mu
    call print_trace('radiation_rays',dbg_rad,'end mu loop')

    if (master .and. debug2(dbg_rad,1)) print'(a,6i6)', &
     ' imu, ny_phi =', &
       imu, ny_phi
  end do                                                                ! end mu loop  
  call print_trace('radiation_rays',dbg_rad,'radiation_rays: end loop over omega')
  if (verbose > 0) print'(a,i5,63x,1p,g12.4)','rank,average(q_tot)', &
       mpi_rank,sum(qq_tot)/mw
  if (verbose > 2) then
    close(101)
  end if
!-----------------------------------------------------------------------
! Add to energy equation
!-----------------------------------------------------------------------
  call dumpn(qq_tot,'qq_tot','rays.dmp',1)
  do iz=1,mz
    dedt(:,:,iz) = dedt(:,:,iz) + qq_tot(:,:,iz)  
  end do
!-----------------------------------------------------------------------
! Radiative flux
!-----------------------------------------------------------------------
  call print_trace('radiation',dbg_rad,'radiative_flux')
  call radiative_flux (qq_tot,scratch)
  deallocate (lnr, qq_tot)
!-----------------------------------------------------------------------
! Newton cooling
!-----------------------------------------------------------------------
  call print_trace('radiation',dbg_rad,'newton')
  if (do_newton) then
    if (ee_newton > 0) eetop = ee_newton
    do iz=izs,ize
    do iy=iphot,my
      f = exp(min((y_newton-ym(iy))/dy_newton,70.))
      f = f/(1.+f)
      do ix=1,mx
        dedt(ix,iy,iz) = dedt(ix,iy,iz) - r(ix,iy,iz)*(ee(ix,iy,iz)-eetop)/t_newton*f &
                                        - r(ix,iy,iz)*min(0.,ee(ix,iy,iz)-ee_min)/t_ee_min
      end do
    end do
    end do
  end if

  if (first_time) then
    call sum_int8_mpi (nsent)
    call sum_int8_mpi (nC0)
    call sum_int8_mpi (nC1)
    call sum_int8_mpi (nC2)
    do is=1,mseq
      call sum_int_mpi (hseq(is))
    end do
    call max_int8_mpi (max_offset)
    if (master) then
      print'(1x,a,f7.2,3i10)','chunks sent/step:', &
        nsent/(real(mxtot)*real(ny_bin)*real(mztot)), ncalls
      print'(1x,a,5i10)','max offset:',max_offset
      print'(1x,a,6i15)','rank stats:', nC0, nC1, nC2
      print'(1x,a,5i10)','number of sequences:',hseq
      print'(1x,a,13i8,/,(16i8))','histogram of sequences:',lseq
    end if
    call buffer_count
    call interp_count
  end if
  first_time=.false.
                                            call timer('radiation','end')
!-----------------------------------------------------------------------
CONTAINS

!***********************************************************************
subroutine mhd2ray (ix,ixof,izof,rank,ray)
  implicit none
  integer ix,jx,jz,ixof,izof,rank,ray
!.......................................................................
  jx = modulo(ix+ixof-ixo-1,mxtot)                                      ! absolute x
  jz = modulo(iz+izof-izo-1,mztot)                                      ! absolute z
  ray = jx+jz*mxtot                                                     ! absolute ray
  rank = ray/fray                                                       ! rank
  ray = ray-rank*fray+1                                                 ! rank ray
end subroutine

END
