!***********************************************************************
SUBROUTINE transfer (np,n1,dtau,s,q,xi,doxi)
!
!  Solve the transfer equation, given optical depth and source function.
!
!  "Steins trick" is used; storing the sum of the three elements in each
!  row of the tridiagonal equation system, instead of the diagonal ele-
!  ment.  This allows accurate solutions to be otained for arbitrarily
!  small optical depths.  Without this trick, round off errors become
!  noticeable already when the smallest optical depth is less than the
!  square root of the machine precisions.
!
!  This version works with P as a variable for k <= n1, and with
!  Q = P - S for k > n1.  P is Feautriers P and S is the source function.
!  The equation solved is  P" = P - S. The answer returned is Q = P - S for
!  all k, computed from Q = P" for optically thick layers.
!
!  This is a second order version.  For simulations, with rapidly
!  varying absorption coefficients and source functions, this is to be
!  preferred over spline and Hermitean versions because it is positive
!  definite, in the sense that a positive source function is guaranteed
!  to result in a positive average intensity p.  Also, the flux
!  divergence is exactly equal to q, for the conventional definition
!  of the flux.
!
!  Operation count: 5d+5m+11a = 21 flops
!
!***********************************************************************
!
  USE params
  implicit none
!
  real, dimension(mx,my,mz) :: dtau,s,q
  real, dimension(mx,my)    :: sp1,sp2,sp3,qs
  real, dimension(mx,mz)    :: xi
  real, dimension(mx)       :: ex
  integer :: k, l, iz, np, n1
  real :: a, taum, dinv
  logical doxi
!
  character(len=76):: id='$Id: transfer_feautrier3.f90,v 1.5 2009/03/08 17:24:54 aake Exp $'

  if (id.ne.' ') print *,id ; id=' '
!
!  Calculate 1-exp(-tau(l,1,iz)) straight or by series expansion,
!  based on max tau(l,1,iz).
!
!$omp parallel do private(iz,l,k,taum,ex,dinv,sp1,sp2,sp3,a)
  do iz=1,mz
    taum=dtau(1,1,iz)
    do l=1,mx
      taum=amax1(taum,dtau(l,1,iz))
    end do
    if (taum.gt.0.1) then
      do l=1,mx
        ex(l)=1.-exp(-dtau(l,1,iz))
      end do
    else
      do l=1,mx
        ex(l)=dtau(l,1,iz)*(1.-.5*dtau(l,1,iz)*(1.-.3333*dtau(l,1,iz)))
      end do
    endif
!
!  Boundary condition at k=1.
!
    do l=1,mx
      sp2(l,1)=dtau(l,2,iz)*(1.+.5*dtau(l,2,iz))
      sp3(l,1)=-1.
      q(l,1,iz)=s(l,1,iz)*dtau(l,2,iz)*(.5*dtau(l,2,iz)+ex(l))
    end do
!
!  Matrix elements for k>2, k<np: [3d+3s]
!
    do k=2,np-1
    do l=1,mx
      sp2(l,k)=1.
      dinv=2./(dtau(l,k+1,iz)+dtau(l,k,iz))
      sp1(l,k)=-dinv/dtau(l,k,iz)
      sp3(l,k)=-dinv/dtau(l,k+1,iz)
      q(l,k,iz)=s(l,k,iz)
    end do
    end do
!
!  k=np
!
    do l=1,mx
      sp2(l,np)=1.
      q(l,np,iz)=0.0
    end do
!
!  Eliminate subdiagonal, save factors in sp1 [1d+2m+4a]
!
    do k=1,np-2
    do l=1,mx
      a=-sp1(l,k+1)/(sp2(l,k)-sp3(l,k))
      q(l,k+1,iz)=q(l,k+1,iz)+a*q(l,k,iz)
      sp2(l,k+1)=sp2(l,k+1)+a*sp2(l,k)
      sp2(l,k)=sp2(l,k)-sp3(l,k)
    end do
    end do
    do l=1,mx
      sp2(l,np-1)=sp2(l,np-1)-sp3(l,np-1)
    end do
!
!  Backsubstitute [1d+1m+1a]
!
    do k=np-1,1,-1
    do l=1,mx
      q(l,k,iz)=(q(l,k,iz)-sp3(l,k)*q(l,k+1,iz))/sp2(l,k)
    end do
    end do
!
!  Subtract S in the region where the equation is in P
!
    do l=1,mx
      q(l,1,iz)=q(l,1,iz)-s(l,1,iz)
    end do
    do k=2,np-1
    do l=1,mx
      if (dtau(l,k,iz).lt.100.) then
        q(l,k,iz)=q(l,k,iz)-s(l,k,iz)
      else
        q(l,k,iz)=-sp3(l,k)*(s(l,k+1,iz)-s(l,k,iz))+sp1(l,k)*(s(l,k,iz)-s(l,k-1,iz))
      endif
    end do
    end do
!
!  Surface intensity.
!
    if (doxi) then
      do l=1,mx
        xi(l,iz)=2.*(1.-ex(l))*(q(l,1,iz)+s(l,1,iz))+s(l,1,iz)*ex(l)**2
      end do
    end if

  end do    ! iz
!
  END
