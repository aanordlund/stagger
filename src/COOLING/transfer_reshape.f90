!***********************************************************************
SUBROUTINE transfer_reshape (dtau,s,q,xi,doxi,mu,dtaumin,dtaumax,n2)
  USE params,only: mx,my,mz,mpi_rank,mpi_ny,mpi_y,iyoff,mytot,master
  USE reshape_mod, only: dim_re
  implicit none
  integer n2,n1,i
  integer ix,iy,iz,imaxval_mpi
  real mu,dtaumin,dtaumax
  real, dimension(mx,my,mz) :: dtau,s,q
  real, dimension(mx,mz) :: xi
  logical doxi, omp_in_parallel
  real, allocatable, dimension(:,:):: xi1
  real, allocatable, dimension(:,:,:):: dtau1,s1,q1

!-----------------------------------------------------------------------
! Compute global indices for where dtau<dtaumin and dtau<dtaumax
!-----------------------------------------------------------------------
  n1=2; n2=2
  do iy=1,my; do iz=1,mz; do ix=1,mx
    n1=merge(iy+iyoff,n1,dtau(ix,iy,iz) < dtaumin)
    n2=merge(iy+iyoff,n2,dtau(ix,iy,iz) < dtaumax)
  end do; end do; end do
  n1=imaxval_mpi(n1)
  n2=imaxval_mpi(n2)
                                                                        call timer('radiation','minmax')
!-----------------------------------------------------------------------
! If no y-split then just call the solver
!-----------------------------------------------------------------------
  if (mpi_ny==1) then
    if (omp_in_parallel()) then
      call feautrier_omp (mx,my,mz,n2,n1,dtau,s,q,xi,doxi,mu)
    else
      !$omp parallel
      call feautrier_omp (mx,my,mz,n2,n1,dtau,s,q,xi,doxi,mu)
      !$omp end parallel
    end if

!-----------------------------------------------------------------------
! If y-split then first reshape MPI-space to vertical boxes
!-----------------------------------------------------------------------
  else
    call reshape_init
    allocate (dtau1(dim_re(1),dim_re(2),dim_re(3)))
    allocate (   s1(dim_re(1),dim_re(2),dim_re(3)))
    allocate (   q1(dim_re(1),dim_re(2),dim_re(3)))
    allocate (  xi1(dim_re(1),dim_re(3)))
    call reshape_cube (dtau, dtau1)
    call reshape_cube (   s,    s1)
                                                                        call timer('radiation','reshape')
    if (omp_in_parallel()) then
      call feautrier_omp (dim_re(1),dim_re(2),dim_re(3),n2,n1,dtau1,s1,q1,xi1,doxi,mu)
    else
      !$omp parallel
      call feautrier_omp (dim_re(1),dim_re(2),dim_re(3),n2,n1,dtau1,s1,q1,xi1,doxi,mu)
      !$omp end parallel
    end if
    if (doxi) &
      q1(:,1,:)=xi1(:,:)                                                ! let surface intensity ride piggy-back
                                                                        call timer('radiation','feautrier')
    call deshape_cube (q1, q)
    if (doxi .and. mpi_y==0) then
      xi(:,:)=q(:,1,:)                                                  ! pick up surface intensity
      q(:,1,:)=q(:,2,:)                                                 ! copy the heating rate -- should not be used anyway
    end if
    deallocate (dtau1, s1, q1, xi1)
                                                                        call timer('radiation','deshape')
  endif
END

!***********************************************************************
SUBROUTINE feautrier_omp (nx,ny,nz,np,n1,dtau,s,q,xi,doxi,mu)
!
!  Solve the transfer equation, given optical depth and source function.
!
!  "Steins trick" is used; storing the sum of the three elements in each
!  row of the tridiagonal equation system, instead of the diagonal ele-
!  ment.  This allows accurate solutions to be otained for arbitrarily
!  small optical depths.  Without this trick, round off errors become
!  noticeable already when the smallest optical depth is less than the
!  square root of the machine precisions.
!
!  This version works with P as a variable for k <= n1, and with
!  Q = P - S for k > n1.  P is Feautriers P and S is the source function.
!  The equation solved is  Q" = P - S for k<=n1, and Q" = Q - S" for k>n1.
!  The answer returned is Q = P - S for all k.
!
!  This choice of equations produces a high precision in Q at large optical
!  depths (where P - S would have round off), while at the same time
!  retaining the precision at small optical depths (where S" becomes very
!  large and eventually would cause round off errors in the back
!  substitution).
!
!  This is a second order version.  For simulations, with rapidly
!  varying absorption coefficients and source functions, this is to be
!  preferred over spline and Hermitean versions because it is positive
!  definite, in the sense that a positive source function is guaranteed
!  to result in a positive average intensity p.  Also, the flux
!  divergence is exactly equal to q, for the conventional definition
!  of the flux.
!
!  Operation count: 5d+5m+11a = 21 flops
!
!  Update history:  This routine is based on transq, with updates
!
!***********************************************************************
!
  USE params, only: ymg, mytot, mid
  implicit none
!
  integer :: nx,ny,nz
  real, dimension(nx,ny,nz) :: dtau,s,q
  real, dimension(nx,ny)    :: sp1,sp2,sp3
  real, dimension(nx,nz)    :: xi
  real, dimension(nx)       :: ex
  integer :: k,l,iz,np,n1p,n1,omp_get_thread_num
  real :: a,taum,dinv,mu
  logical doxi
!
  character(len=mid):: id='$Id: transfer_reshape.f90,v 1.14 2013/10/25 17:25:00 aake Exp $'

  call print_id (id)
  call broadcast_mpi (ymg, mytot)
!
!  Calculate 1-exp(-tau(l,1,iz)) straight or by series expansion,
!  based on dtau(l,1,iz).
!
  do iz=1,nz
    do l=1,nx
      if (dtau(l,1,iz).gt.0.1) then
        ex(l)=1.-exp(-dtau(l,1,iz))
      else
        ex(l)=dtau(l,1,iz)*(1.-.5*dtau(l,1,iz)*(1.-.3333*dtau(l,1,iz)))
      end if
    end do
!
!  Boundary condition at k=1.
!
    do l=1,nx
      sp2(l,1)=dtau(l,2,iz)*(1.+.5*dtau(l,2,iz))
      sp3(l,1)=-1.
      q(l,1,iz)=s(l,1,iz)*dtau(l,2,iz)*(.5*dtau(l,2,iz)+ex(l))
    end do
!
!  Matrix elements for k>2, k<np: [3d+3s]
!
    do k=2,np-1
    do l=1,nx
      sp2(l,k)=1.
      dinv=2./(dtau(l,k+1,iz)+dtau(l,k,iz))
      sp1(l,k)=-dinv/dtau(l,k,iz)
      sp3(l,k)=-dinv/dtau(l,k+1,iz)
    end do
    end do
!
!  Right hand sides, for k>2, k<n1
!
    n1p=min0(max0(n1,2),np-3)
    do k=2,n1p-1
    do l=1,nx
      q(l,k,iz)=s(l,k,iz)
    end do
    end do
!
!  RHS for k=n1p.  The equation is still in p(k), but q(k+1) is now
!  q(k+1)=p(k+1)-s(k+1), so to get p(k+1) we have to add s(k+1) to the
!  LHS, that is subtract it from the RHS.
!
    if (n1p.le.np) then
      do l=1,nx
        q(l,n1p,iz)=s(l,n1p,iz)-sp3(l,n1p)*s(l,n1p+1,iz)
      end do
    endif
!
!  RHS for k=n1p+1.  The equation is now in Q: Q" = Q - S", but q(k-1)
!  is now p(k-1)=q(k-1)+s(k-1), so we must add sp1*s(l,k-1,iz) to the RHS,
!  relative to the case below.
!
    if (n1p+1.le.np) then
      do l=1,nx
        q(l,n1p+1,iz)=sp1(l,n1p+1)* s(l,n1p+1,iz) &
                     +sp3(l,n1p+1)*(s(l,n1p+1,iz)-s(l,n1p+2,iz))
      end do
    endif
!
!  k>n1p+1,<np [2m+2a]
!
    do k=n1p+2,np-1
    do l=1,nx
      q(l,k,iz)=sp1(l,k)*(s(l,k,iz)-s(l,k-1,iz)) &
               +sp3(l,k)*(s(l,k,iz)-s(l,k+1,iz))
    end do
    end do
!
!  k=np
!
    do l=1,nx
      sp2(l,np)=1.
      q(l,np,iz)=0.0
    end do
!
!  Eliminate subdiagonal, save factors in sp1 [1d+2m+4a]
!
    do k=1,np-2
    do l=1,nx
      a=-sp1(l,k+1)/(sp2(l,k)-sp3(l,k))
      q(l,k+1,iz)=q(l,k+1,iz)+a*q(l,k,iz)
      sp2(l,k+1)=sp2(l,k+1)+a*sp2(l,k)
      sp2(l,k)=sp2(l,k)-sp3(l,k)
    end do
    end do
    do l=1,nx
      sp2(l,np-1)=sp2(l,np-1)-sp3(l,np-1)
    end do
!
!  Backsubstitute [1d+1m+1a]
!
    do k=np-1,1,-1
    do l=1,nx
      q(l,k,iz)=(q(l,k,iz)-sp3(l,k)*q(l,k+1,iz))/sp2(l,k)
    end do
    end do
!
!  Subtract S in the region where the equation is in P
!
    do k=1,n1p
    do l=1,nx
      q(l,k,iz)=q(l,k,iz)-s(l,k,iz)
    end do
    end do
!
!  Surface intensity.
!
    if (doxi) then
      xi(:,iz)=2.*(1.-ex(:))*(q(:,1,iz)+s(:,1,iz))+s(:,1,iz)*ex(:)**2
    end if
!
!  Convert Q to energy per unit volume
!
    do k=2,np-1
      a = mu/(ymg(k+1)-ymg(k-1))
      q(:,k,iz) = q(:,k,iz)*(dtau(:,k+1,iz)+dtau(:,k,iz))*a
    end do
    a = mu/(ymg(2)-ymg(1))
    q(:, 1,iz) = q(:, 1,iz)*(dtau(:, 2,iz))*a
    q(:,np:ny,iz) = 0.0
  end do
!
  END
