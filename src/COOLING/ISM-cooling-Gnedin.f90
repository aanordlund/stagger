! $Id: ISM-cooling-Gnedin.f90,v 1.2 2012/09/02 20:06:36 aake Exp $
!***********************************************************************
MODULE cooling

! Note: if initial conditions are set such that [Z] could be less than
! 1.E-04, the lowest metallicity used in MODULE cooling must be set 
! to 1.E-04

! The units of lamda are energy per unit volume and time, or cdu*cvu^2/ctu,
! which the computed, dimensional value should be divided by.  But since
! a multiplicative factor of cdu^2 should also be applied, the computed
! value should be divided by cvu^2/(ctu*cdu), or multiplied by ctu*cdu/cvu^2.

  use units

  logical do_table, do_power, do_heat, do_extraterm
  character (len=40) coolH2file,tablefile

  real, allocatable, dimension(:,:):: table
  real:: a0,b0,a1,b1,tab0,Tlower,G0,G1,rho_crit
  integer:: ma,mb,turnon
END MODULE

!***********************************************************************
SUBROUTINE init_cooling (r,ee)
  USE params
  USE cooling
  implicit none
  real, dimension(mx,my,mz):: r,ee
  character(len=mid):: id='$Id: ISM-cooling-Gnedin.f90,v 1.2 2012/09/02 20:06:36 aake Exp $'

  call print_trace (id, dbg_cool, 'BEGIN')

  tablefile = file(1:index(file,'.'))//'eos'
  if (do_trace) print *,'init_cooling: read'
  do_cool = .true.
  do_table = .false.
  do_power = .false.
  do_heat=.true.
  do_extraterm=.true.
  G0=1.e-24
  G1=1.e-25
  Tlower = 100.
  rho_crit=20000.
  ma = mx ; mb = my
  turnon=-1
  call read_cooling

  allocate (table(ma,mb))
  a0=50. ; a1=10.                       ! make sure table is invalid
  b0=50. ; b1=10.

  call test_cooling

  call print_trace (id, dbg_cool, 'END')
END SUBROUTINE

!***********************************************************************
SUBROUTINE read_cooling
  USE params
  USE cooling
  implicit none
  namelist /cool/do_cool,do_table,do_power,Tlower,ma,mb,coolH2file,turnon, &
       do_heat,do_extraterm,G0,G1,rho_crit

  if (stdin.ge.0) then
    rewind (stdin); read (stdin,cool)
  else
    read (*,cool)
  end if
  if (master) write (*,cool)
END SUBROUTINE

!***********************************************************************
SUBROUTINE coolit_real (na,nb,r,ee,lne,lambda)
  USE params
  USE eos
  USE units
  implicit none
  real, parameter:: dlgT=0.1, lgT0=1.0, T_MC=10.
  integer ix,iy,na,nb,i
  real, dimension(na,nb):: r,ee,lne
  real, dimension(na,nb)::lambda
  real temp,p
  integer, parameter:: ntab=81
  real, save, dimension(ntab):: ln25
  data ln25 / &
   -8.6450,-8.1238,-7.6490,-7.2158,-6.8207,-6.4650,-6.1428,-5.8379, &
   -5.5115,-5.1155,-4.6465,-4.1402,-3.6593,-3.2304,-2.8680,-2.5690, &
   -2.3228,-2.1203,-1.9505,-1.8042,-1.6724,-1.5460,-1.2870,-1.0269, &
   -0.7525,-0.4544,-0.1468, 0.1931, 0.5939, 1.1394, 2.0933, 3.9022, &
    6.1748, 6.5501, 6.2719, 6.2451, 6.4187, 6.7583, 7.2167, 7.6392, &
    7.7969, 7.7480, 7.7677, 7.8474, 7.8755, 7.4928, 7.2349, 7.1854, &
    6.9207, 6.6918, 6.6560, 6.5481, 6.3418, 5.9320, 5.4885, 5.1625, &
    4.9726, 4.9265, 4.9445, 4.9822, 4.9843, 4.9280, 4.8032, 4.6959, &
    4.6625, 4.6812, 4.7414, 4.8195, 4.9030, 5.0019, 5.0907, 5.1745, &
    5.2423, 5.2943, 5.3318, 5.3552, 5.3790, 5.3932, 5.4009, 5.4076, &
    5.4205 /
  real, parameter:: ma=2e-24, scale=(1e-25/ma)*(ctu/clu)**2*(ctu*cdu)/ma
!...............................................................................
  do iy=1,nb
  do ix=1,na
    temp=ee(ix,iy)*utemp
    p=1.+(alog10(temp)-lgT0)/dlgT
    i=min(max(ifix(p),1),ntab-1)
    p=p-i
    lambda(ix,iy)=scale*exp((1-p)*ln25(i)+p*ln25(i+1))
  enddo
  enddo
END SUBROUTINE

!***********************************************************************
SUBROUTINE coolit (r,ee,lne,dd,dedt)
  USE params
  real, dimension(mx,my,mz):: r,ee,lne,dd,dedt
  logical omp_in_parallel
!
  if (.not. do_cool) return
  if (omp_in_parallel()) then
    call coolit_omp (r,ee,lne,dd,dedt)
  else
    !$omp parallel
    call coolit_omp (r,ee,lne,dd,dedt)
    !$omp end parallel
  end if
END SUBROUTINE

!***********************************************************************
SUBROUTINE coolit_omp (r,ee,lne,dd,dedt)
!
!  Wrapper routine, to allow calling the real routine from new_table
!
  USE params
  USE eos, void=>do_table
  USE cooling
  implicit none
  real, parameter ::nohhe=0.488,codeunits=ctu**3/(cdu*clu**2) 
  real, dimension(mx,my,mz):: r,ee,lne,dd,dedt
  real,  dimension(mx,my):: qq
!  double precision, dimension(mx,my):: qq
  integer ix,iy,iz
  real Ct, hrate, onoff, rate
  character(len=mid):: id='coolit: $Id: ISM-cooling-Gnedin.f90,v 1.2 2012/09/02 20:06:36 aake Exp $'

  if (it.lt.turnon) return 
  !call print_trace (id, dbg_cool, 'BEGIN')

  if (do_table) then
    !call coolit_table (r,ee,lne,dedt)
    print *,'do_table not implemented!'
    stop
  end if

  ct = 0.5*Cdt/dt
  do iz=izs,ize
    call coolit_real (mx,my,r(:,:,iz),ee(:,:,iz),lne(:,:,iz),qq)

!Include heating rate from photoionization (by G0*n^2, choosing do_heat)
!and extra term which origin could be dissipation of interstellar plasma
! turbulence (Reynods,Haffner, Tufte 1999) or photoelectric heating (Reynolds,
! Cox 1992) and that is included by the extraterm G1*n
!(choosing do_heat and do_extraterm). The values of G0 and G1 are extracted
!from Reynolds, Haffner, Tufte 1999, altough a value of G0=1.3e-24 could be 
!used (Reynolds, Cox 1992). 
!'codeunits' is just the value from convert [G0*n^2]=[G1*n]=erg*s^-1*cm^-3 
!to code units.

!This approx. should be valid as long as the medium is totally ionized
!as it should be consider in Giant HII regions.
!In cases were rho is larger than rho_crit then hrate=0. This approx come form the fact that 
!at in the very dense clumps the starlight can not pass. The problem must be in how to choose 
!the critical rho. The answer partially can be found in Franco 1996. He states that the condition 
!for molecula to be form is that the column density is higher than N=677/L(in pc).  

    if (do_heat) then
      if (do_extraterm) then
        do iy=1,my
        do ix=1,mx
          onoff= (rho_crit**2/(r(ix,iy,iz)**2+rho_crit**2))**2
          hrate= (G0*(nohhe*r(ix,iy,iz))**2+G1*nohhe*r(ix,iy,iz))*codeunits*onoff
          qq(ix,iy)= qq(ix,iy)*r(ix,iy,iz)**2
          rate= max(hrate,abs(qq(ix,iy)))/(ct*r(ix,iy,iz)*ee(ix,iy,iz))
          qq(ix,iy)= (hrate-qq(ix,iy))/max(rate,1.0)
          dedt(ix,iy,iz)= dedt(ix,iy,iz) + qq(ix,iy)
        enddo
        enddo
      else
        do iy=1,my
        do ix=1,mx
          onoff= (rho_crit**2/(r(ix,iy,iz)**2+rho_crit**2))**2
          hrate= (G0*(nohhe*r(ix,iy,iz))**2)*codeunits*onoff
          qq(ix,iy)= qq(ix,iy)*r(ix,iy,iz)**2
          rate= max(hrate,abs(qq(ix,iy)))/(ct*r(ix,iy,iz)*ee(ix,iy,iz))
          qq(ix,iy)= (hrate-qq(ix,iy))/max(rate,1.0)
          dedt(ix,iy,iz)= dedt(ix,iy,iz) + qq(ix,iy)
        enddo
        enddo
      endif   
    else
      do iy=1,my
      do ix=1,mx
        qq(ix,iy)= qq(ix,iy)*r(ix,iy,iz)**2
        rate= abs(qq(ix,iy))/(ct*r(ix,iy,iz)*ee(ix,iy,iz))
        qq(ix,iy)= qq(ix,iy)/max(rate,1.0)
        dedt(ix,iy,iz)= dedt(ix,iy,iz) - qq(ix,iy)
      end do
      end do
    endif   
  enddo

  !call print_trace (id, dbg_cool, 'END')
END SUBROUTINE

!***********************************************************************
SUBROUTINE test_cooling
  USE params, only: master
  USE units, only: utemp
  !USE eos
  implicit none
  real, dimension(16,1):: temp, r, ee1, ee2, lne, qq1, qq2
  integer i
!-----------------------------------------------------------------------
  temp(:,1) = (/ &
    1.e3, &
    10964.781e0, &
    19952.632e0, & 
    29512.078e0, & 
    39810.708e0, & 
    69183.121e0, &
    125892.51e0, &
    251188.7e0, &
    501187.01e0, &
    891250.55e0, &
    1.412537e6, & 
    2.511887e6, &
    6.309576e6, &
    1.e7, &
    1.5848925e7, &
    1.e9 /)
  ee1 = 0.999*temp/utemp
  ee2 = 1.001*temp/utemp
  r = 1.
  do i=1,16
    call coolit_real (16,1,r,ee1,lne,qq1) 
    call coolit_real (16,1,r,ee2,lne,qq2) 
    if (master) then
      print '(i3,4g12.4)',i,temp(i,1),qq1(i,1),qq2(i,1),qq2(i,1)/(qq1(i,1)+1e-30)
    end if
  end do
END
