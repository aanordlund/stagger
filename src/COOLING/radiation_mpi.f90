! $Id: radiation_mpi.f90,v 1.6 2008/06/28 19:29:59 aake Exp $
!*******************************************************************************
MODULE transfer_m
  integer verbose

CONTAINS
!-------------------------------------------------------------------------------
SUBROUTINE transfer (mx,my,mz,dxdy,dzdy,ym,dx,dz,sx,sz,lnss0,lnrk0,qqp0,qqm0)
  implicit none
  integer             :: mx,my,mz
  real, dimension(my) :: ym,ds
  real                :: dxdy,dzdy,dx,dz,sx,sz
  real, dimension(mx) :: rk1,rk2,ss0,ss1,ss2,d2ss1,d2ss2,dii1,dii2,d1ss1,d1ss2, &
                         dtaus,edtau1
  integer, dimension(my)    :: ixma,ixpa,izma,izpa
  real, dimension(mx,my,mz) :: lnss0,lnrk0,qqp0,qqm0
  real                      :: px0,px1,px2,pz0,pz1,pz2,ds1,ds2,dtau1,dtau2,dtauc, &
                               d1ss,d2ss
  integer                   :: ma,ix,iy,iz,nitx,nitz,nit,i,itime,ixm,ixp,izm,izp
  real                      :: time(20),cpu(2),dtime
  character(len=10)         :: name(20)
  real, allocatable, dimension(:,:,:) :: rk,ss,dqqp,dqqm, &
                                         lnrk,lnss,qqp,qqm,etp,etm,edt
  real, allocatable, dimension(:,:) :: dtau,ex0,ex1,ex2,dssdtau
!...............................................................................
!
! Compute the maximum ghost zone size needed
!
  if (verbose > 1) print*,'margins:'
  itime=1; time(itime) = dtime(cpu); name(itime)='start'
  ma = 0
  do iy=2,my-1
    px0 = dxdy*ym(iy-1)/dx
    px1 = dxdy*ym(iy  )/dx
    px2 = dxdy*ym(iy+1)/dx
    pz0 = dzdy*ym(iy-1)/dz
    pz1 = dzdy*ym(iy  )/dz
    pz2 = dzdy*ym(iy+1)/dz
    ds(iy) = sqrt((px2-px1)**2+(ym(iy+1)-ym(iy))**2+(pz2-pz1)**2)
    ixma(iy) = floor(px0)-floor(px1)
    ixpa(iy) = floor(px2)-floor(px1)
    izma(iy) = floor(pz0)-floor(pz1)
    izpa(iy) = floor(pz2)-floor(pz1)
    ma = max(ma,max(abs(ixpa(iy)),abs(izpa(iy))))
  end do
  px1 = dxdy*ym(1)/dx
  px2 = dxdy*ym(2)/dx
  pz1 = dzdy*ym(1)/dz
  pz2 = dzdy*ym(2)/dz
  px0 = dxdy*ym(my-1)/dx
  px1 = dxdy*ym(my  )/dx
  pz0 = dzdy*ym(my-1)/dz
  pz1 = dzdy*ym(my  )/dz
  ixpa( 1) = floor(px2)-floor(px1)
  izpa( 1) = floor(pz2)-floor(pz1)
  ixma(my) = floor(px0)-floor(px1)
  izma(my) = floor(pz0)-floor(pz1)
  itime=itime+1; time(itime) = dtime(cpu); name(itime)='ghost'
  if (verbose > 1) print*,'ma =',ma
!
! Allocate scratch variables with ghost zones
!
  if (verbose > 1) print*,'allocate:'
  if (verbose > 1) print'(3(i3,1h:,i2,5x))',1-ma,mx+ma,my,my,1-mz,mz+ma
  allocate (  rk(1-ma:mx+ma,0:my+1,1-ma:mz+ma), &                               ! rho*kappa
              ss(1-ma:mx+ma,0:my+1,1-ma:mz+ma), &                               ! source
             qqp(1-ma:mx+ma,0:my+1,1-ma:mz+ma), &                               ! I+
             qqm(1-ma:mx+ma,0:my+1,1-ma:mz+ma), &                               ! I-
             etp(1-ma:mx+ma,0:my+1,1-ma:mz+ma), &                               ! exp(-tau[+])
             etm(1-ma:mx+ma,0:my+1,1-ma:mz+ma), &                               ! exp(-tau[-])
             edt(1-ma:mx+ma,0:my+1,1-ma:mz+ma), &                               ! exp(-dtau)
            lnss(1-ma:mx+ma,0:my+1,1-ma:mz+ma), &                               ! ln(S)
            lnrk(1-ma:mx+ma,0:my+1,1-ma:mz+ma), &                               ! ln(rho*kappa)
            dqqp(1-ma:mx+ma,0:my+1,1-ma:mz+ma), &                               ! dI+
            dqqm(1-ma:mx+ma,0:my+1,1-ma:mz+ma), &                               ! dI-
                    dssdtau(1-ma:mx+ma,0:my+1), &                               ! exp(-dtau)
                        ex0(1-ma:mx+ma,0:my+1), &                               ! exp(-dtau)
                        ex1(1-ma:mx+ma,0:my+1), &                               ! 1.-exp(-dtau)
                        ex2(1-ma:mx+ma,0:my+1), &                               ! 
                       dtau(1-ma:mx+ma,0:my+1))                                 ! dtau
!
! Compute source functions and opacities
!
  if (verbose > 1) print*,'source:'
  lnrk(1:mx,1:my,1:mz) = lnrk0                                                  ! add ghost zones
  lnss(1:mx,1:my,1:mz) = lnss0
  call ghost_fill (mx,my,mz,ma,lnrk)                                            ! fill in ghost zones
  call ghost_fill (mx,my,mz,ma,lnss)
  itime=itime+1; time(itime) = dtime(cpu); name(itime)='fill'
  
  do iy=1,my
    px1 = dxdy*ym(iy)/dx
    pz1 = dzdy*ym(iy)/dz
    px1 = px1-floor(px1)
    pz1 = pz1-floor(pz1)
    call interpolate_xz(mx,my,mz,ma,iy,lnss,ss,px1,pz1,.true.)
    call interpolate_xz(mx,my,mz,ma,iy,lnrk,rk,px1,pz1,.true.)
    if (verbose > 1) print'(i4,10f7.3)',iy,rk(1:mx,iy,1)
  end do
  call ghost_fill (mx,my,mz,ma,ss)
  call ghost_fill (mx,my,mz,ma,rk)
  itime=itime+1; time(itime) = dtime(cpu); name(itime)='interp'
!
! Optical depth increments and antenuation factors
!
  nitx = ceiling(dxdy*(ym(my)-ym(1))/sx)
  nitz = ceiling(dzdy*(ym(my)-ym(1))/sz)
  nit = 1+max(nitx,nitz)
  if (verbose > 0) print*,'nit =',nit
  qqp(:,1,:) = 0.
  etp(:,1,:) = 1.
  qqm(:,my,:) = 0.
  etm(:,my,:) = 1.
  do iz=1,mz
    do iy=1,my-1
      ixp = ixpa(iy)
      izp = izpa(iy)
      do ix=1,mx
        dtau(ix,iy) = 0.5*ds(iy)*(rk(ix    ,iy  ,iz    ) &
                    +             rk(ix+ixp,iy+1,iz+izp))
        ex0(ix,iy) = exp(-dtau(ix,iy))
        ex1(ix,iy) = 1.-ex0(ix,iy)
        ex2(ix,iy) = ex1(ix,iy) - dtau(ix,iy)*ex0(ix,iy)
	dssdtau(ix,iy) = (ss(ix+ixp,iy+1,iz+izp) - ss(ix,iy,iz))/dtau(ix,iy)
      end do
    end do
!
! Source function 1st and 2nd derivatives
!
    do iy=2,my-1
      ixp = ixpa(iy)
      ixm = ixma(iy)
      do ix=1,mx
	dtauc = 1./(dtau(ix,iy)+dtau(ix+ixm,iy-1))
        d1ss = (dssdtau(ix,iy)*dtau(ix+ixm,iy-1)+dssdtau(ix+ixm,iy-1)*dtau(ix,iy))
        d2ss = (dssdtau(ix,iy)-dssdtau(ix+ixm,iy-1))*2.*dtauc
	dqqp(ix,iy,iz) = - d1ss*ex1(ix,iy) + d2ss*ex2(ix,iy)
	dqqm(ix,iy,iz) = + d1ss*ex1(ix,iy) + d2ss*ex2(ix,iy)
      end do
    end do
  end do
  itime=itime+1; time(itime) = dtime(cpu); name(itime)='dqq'
!
! Sum contributions and multiply attenuation factors
!
  do i=1,nit
    do iz=1,mz
      do iy=1,my-1
        ixp = ixpa(iy)
        izp = izpa(iy)
        do ix=1,mx
          qqp(ix+ixp,iy+1,iz+izp) = qqp(ix,iy,iz)*edt(ix,iy,iz) + dqqp(ix,iy,iz)
          etp(ix+ixp,iy+1,iz+izp) = etp(ix,iy,iz)*edt(ix,iy,iz)
        end do
      end do
      do iy=my-1,1,-1
        ixp = ixpa(iy)
        izp = izpa(iy)
        do ix=1,mx
	  qqm(ix,iy,iz) = qqm(ix+ixp,iy+1,iz+izp)*edt(ix,iy,iz) + dqqm(ix,iy,iz)
	  etm(ix,iy,iz) = etm(ix+ixp,iy+1,iz+izp)*edt(ix,iy,iz)
        end do
      end do
    end do
    if (verbose > 1) print'(i4,10f7.3)',iy,qqm(1:mx,iy,1)
    if (verbose > 1) print'(i4,10f7.3)',iy,qqp(1:mx,iy,1)
    call ghost_fill (mx,my,mz,ma,qqp)
    call ghost_fill (mx,my,mz,ma,qqm)
  end do
  itime=itime+1; time(itime) = dtime(cpu); name(itime)='qq'
  qqp0 = qqp(1:mx,1:my,1:mz)
  qqm0 = qqm(1:mx,1:my,1:mz)
  itime=itime+1; time(itime) = dtime(cpu); name(itime)='copy'
  
  print'(a,i4,5x,20a10)','nit,times:',nit,name(1:itime)
  print'(a,i4,20f10.3)','nit,times:',nit,time(1:itime),sum(time(2:itime))
  print'(a,i4,20f10.3)','nit,times:',nit,time(1:itime)/sum(time(2:itime))
  print'(a,i4,20f10.1)','nit,times:',nit,time(1:itime)*1e9/(2.*real(mx)*real(my)*real(mz))
  print*,sum(time(2:itime))*1e9/(2.*real(mx)*real(my)*real(mz)),' ns/pt'
  deallocate (rk,ss,qqp,qqm,lnss,lnrk,dtau,dqqp,dqqm)  
END SUBROUTINE

!-------------------------------------------------------------------------------
SUBROUTINE interpolate_xz(mx,my,mz,ma,iy,lnss,ss,px,pz,do_exp)
  implicit none
  integer mx,my,mz,ma,iy
  real, dimension(1-ma:mx+ma,0:my+1,1-ma:mz+ma):: lnss,ss
  real px,pz,qx,qz
  logical do_exp
  integer ix,iz
!...............................................................................
  qx = 1.-px
  qz = 1.-pz
  do iz=1,mz
    do ix=1,mx
      ss(ix,iy,iz) = (lnss(ix,iy,iz  )*qx+lnss(ix+1,iy,iz  )*px)*qz &           ! bi-linear
                   + (lnss(ix,iy,iz+1)*qx+lnss(ix+1,iy,iz+1)*px)*pz             ! interpolation
    end do
    if (do_exp) then                                                            ! separate
      do ix=1,mx                                                                ! small loop to
        ss(ix,iy,iz) = exp(ss(ix,iy,iz))                                        ! encourage streaming
      end do
    end if
  end do
END SUBROUTINE

!-------------------------------------------------------------------------------
SUBROUTINE ghost_fill (mx,my,mz,ma,ss)
  implicit none
  integer mx,my,mz,ma
  real ss(1-ma:mx+ma,0:my+1,1-ma:mz+ma)
!...............................................................................
  ss(1-ma:0,:,:) = ss(mx-ma+1:mx,:,:)
  ss(:,:,1-ma:0) = ss(:,:,mz-ma+1:mz)
  ss(mx+1:mx+ma,:,:) = ss(1:ma,:,:)
  ss(:,:,mz+1:mz+ma) = ss(:,:,1:ma)
  if (verbose > 1) print'(12f10.5)',ss(:,1,1)
END SUBROUTINE
END MODULE
!*******************************************************************************

!-------------------------------------------------------------------------------
PROGRAM test_transfer                                                           ! sets up properties ..
  implicit none                                                                 ! .. similar to full code
  integer mx,my,mz
  real dx,dy,dz,sx,sy,sz
  real, allocatable, dimension(:):: xm,ym,zm
  integer i
!...............................................................................
  mx=100
  my=100
  mz=100
  dx=0.1
  dy=0.05
  dz=0.1

  allocate(xm(mx),ym(my),zm(mz))
  xm = dx*(/((i-mx/2),i=1,mx)/)
  ym = dy*(/((i-my/2),i=1,my)/)
  zm = dz*(/((i-mz/2),i=1,mz)/)
  sx = dx*mx
  sy = dy*my
  sz = dz*mz
  call transfer_test(mx,my,mz,xm,ym,zm,sx,sy,sz,dx,dy,dz)
  
  deallocate(xm,ym,zm)
END

!-------------------------------------------------------------------------------
SUBROUTINE transfer_test(mx,my,mz,xm,ym,zm,sx,sy,sz,dx,dy,dz)                   ! can be called from code
  USE transfer_m
  implicit none
  integer mx,my,mz,ma
  real dxdy,dzdy,dx,dy,dz,sx,sy,sz
  real xm(mx),ym(my),zm(mz)
  real, parameter:: pi=3.14159265
  real, allocatable, dimension(:,:,:):: lnss,lnrk,qqp,qqm
  integer i,ix,iy
!...............................................................................
  verbose = 0
  dxdy = 0.8
  dzdy = 0.0

  allocate (lnss(mx,my,mz),lnrk(mx,my,mz),qqp(mx,my,mz),qqm(mx,my,mz))
  
  do i=0,min(8,mx)
    qqm = 0.
    qqp = 0.
    do iy=1,my
      do ix=1,mx
        lnss(ix,iy,:) = sin(2.*pi*(xm(ix)+i*dx)/sx)
        lnrk(ix,iy,:) = 1.+ym(iy)/0.2
      end do
      if (verbose > 1) print'(i4,5g12.4)',iy,ym(iy), &
        exp(lnss(1,iy,1)),exp(lnss(mx/2,iy,1)),exp(lnrk(1,iy,1))
    end do
    call transfer (mx,my,mz,dxdy,dzdy,ym,dx,dz,sx,sz,lnss,lnrk,qqp,qqm)
    if (verbose > 0) print'(i3,10f9.5)',i,qqm(:,1,1)
    if (verbose > 0) print*,'END'
  end do

  deallocate(lnss,lnrk,qqp,qqm)
END
