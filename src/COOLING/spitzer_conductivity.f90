!***********************************************************************
  subroutine conduction (r,e,ee,Bx,By,Bz,dedt)
!
!  Spitzer conductivity, from Schrijver & Zwaan 1999, pg 222
!
!  eps = d/ds (kappa_c dT/ds)
!  kappa_c = 8e-7 T^{5/2} erg cm^{-1} s^{-1}
!-----------------------------------------------------------------------
  USE params
  USE units
  USE cooling
  !USE stagger
  implicit none
  real, dimension(mx,my,mz):: r,e,ee,Bx,By,Bz,dedt
  real, allocatable, dimension(:,:,:):: lnee,scr1,scr2,scr3,scr4,scr5,scr6
  !real, dimension(mx,my,mz):: lnee,scr1,scr2,scr3,scr4,scr5,scr6
  real u_cond, u_cmax, tmp, kappac, kappam, lnTbot
  integer i, j, k
  character(len=mid):: id= &
   "$Id: spitzer_conductivity.f90,v 1.7 2011/04/06 11:02:28 aake Exp $"

!-----------------------------------------------------------------------

  if (.not. do_conduction) return

  call print_id(id)
  u_cond = 8e-7*u_temp**3.5/u_l**2*(u_t/u_e)
  if (id .ne. '') then 
    print *,id
    print *,'u_cond, u_temp = ', u_cond, u_temp
    id = ''
  end if
print*, 'post 1'

!-----------------------------------------------------------------------
!  Temperature; face centered values and derivatives
!-----------------------------------------------------------------------
  allocate(lnee(mx,my,mz))
  allocate(scr1(mx,my,mz))
  allocate(scr2(mx,my,mz))
  allocate(scr3(mx,my,mz))
  allocate(scr4(mx,my,mz))
  allocate(scr5(mx,my,mz))
  allocate(scr6(mx,my,mz))
  
  lnee = alog(ee)
  call ddxdn1_set(lnee,scr4)
  call ddydn1_set(lnee,scr5)
  call ddzdn1_set(lnee,scr6)
print*, 'post2'
!-----------------------------------------------------------------------
!  Cell centered temperature gradient and magnetic field components
!-----------------------------------------------------------------------
  call xup_set(scr4,scr1)
  call yup_set(scr5,scr2)
  call zup_set(scr6,scr3)
  call xup_set(Bx,scr4)
  call yup_set(By,scr5)
  call zup_set(Bz,scr6)
  call dump (scr1, 'spitzer.dat', 3)
  call dump (scr2, 'spitzer.dat', 4)
  call dump (scr3, 'spitzer.dat', 5)
  call dump (scr4, 'spitzer.dat', 6)
  call dump (scr5, 'spitzer.dat', 7)
  call dump (scr6, 'spitzer.dat', 8)
print*,'post3'
!-----------------------------------------------------------------------
!  Projection along the magnetic field
!-----------------------------------------------------------------------
  do k=1,mz
  do j=1,my
  do i=1,mx
    kappac = u_cond*exp(3.5*lnee(i,j,k))
    u_cmax = 0.6*min(dx,dy,dz)**2/dt
    kappam = u_cmax*e(i,j,k)
    tmp=min(kappac,kappam)* &
        (scr1(i,j,k)*scr4(i,j,k)+scr2(i,j,k)*scr5(i,j,k)+scr3(i,j,k)*scr6(i,j,k)) &
       /(scr4(i,j,k)*scr4(i,j,k)+scr5(i,j,k)*scr5(i,j,k)+scr6(i,j,k)*scr6(i,j,k)+1e-20)
    scr4(i,j,k) = scr4(i,j,k)*tmp
    scr5(i,j,k) = scr5(i,j,k)*tmp
    scr6(i,j,k) = scr6(i,j,k)*tmp
    scr1(i,j,k) = tmp
    scr2(i,j,k) = kappac
    scr3(i,j,k) = kappam
  end do
  end do
  end do
  call extrapolate_center (scr1)
  call dump (scr4, 'spitzer.dat', 9)
  call dump (scr1, 'spitzer.dat', 10)
  call dump (scr2, 'spitzer.dat', 11)
  call dump (scr3, 'spitzer.dat', 12)
print*,'post4'
!-----------------------------------------------------------------------
!  Face centered, field aligned heat flux
!-----------------------------------------------------------------------
  call xdn_set(scr4,scr1)
  call ydn_set(scr5,scr2)
  call zdn_set(scr6,scr3)
print*,'post 5'
!-----------------------------------------------------------------------
!  Add flux divergence to dE/dt
!-----------------------------------------------------------------------
  call ddxup1_set(scr1,scr4)
  call ddyup1_add(scr2,scr4)
  call ddzup1_add(scr3,scr4)
  call dump (scr1, 'spitzer.dat', 1)
  call dump (scr4, 'spitzer.dat', 2)
  
  deallocate(lnee)
  deallocate(scr1)
  deallocate(scr2)
  deallocate(scr3)
  deallocate(scr4)
  deallocate(scr5)
  deallocate(scr6)
  
!-----------------------------------------------------------------------
!  Make sure no conduction away from cold spots
!-----------------------------------------------------------------------
!  lnTbot = alog(eebot*(gamma-1))
!  do k=1,mz
!  do j=1,my
!  do i=1,mx
!    scr5(i,j,k) = dedt(i,j,k)
!    if (lnee(i,j,k) .gt. lnTbot) dedt(i,j,k) = dedt(i,j,k)+scr4(i,j,k)
!    scr5(i,j,k) = (dedt(i,j,k) - scr5(i,j,k))/e(i,j,k)
!  end do
!  end do
!  end do

!  call fstat1 ('spitzer', scr5, lb, ub)

  END
