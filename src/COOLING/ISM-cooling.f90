! $Id: ISM-cooling.f90,v 1.31 2004/07/13 08:37:52 aake Exp $
!***********************************************************************
MODULE cooling

! Note: if initial conditions are set such that [Z] could be less than
! 1.E-04, the lowest metallicity used in MODULE cooling must be set 
! to 1.E-04

! The units of lamda are energy per unit volume and time, or cdu*cvu^2/ctu,
! which the computed, dimensional value should be divided by.  But since
! a multiplicative factor of cdu^2 should also be applied, the computed
! value should be divided by cvu^2/(ctu*cdu), or multiplied by ctu*cdu/cvu^2.

  use units

  real   , parameter :: zlim=1.324E-12      ! sets [Fe/H]>-9.0, depends on lnfehsun
  real   , parameter :: logclambu=-20.4814  ! [L]=ctu*cdu/cvu**2
  real   , parameter :: lnclambu=-47.1602   ! [L]=ctu*cdu/cvu**2
  real   , parameter :: noH=0.451           ! cdu -> # H atoms cm-3
  real   , parameter :: noHHe=0.488         ! cdu -> # H+He atoms cm-3  
  real   , parameter :: ln10=2.3025851      ! to speed up 10.** 
  real   , parameter :: invln10=0.4342945   ! to speed up log10 (?) 
  real   , parameter :: log2=0.30103
  real   , parameter :: EPS=1.0E-03
  real   , allocatable, dimension(:) :: TN, DN, RH, ROP
  real   , allocatable, dimension(:,:,:,:) :: WG
  logical do_table, do_power, hhrr_search
  character (len=40) coolH2file,tablefile

  real, allocatable, dimension(:,:,:):: table
  real:: a0,b0,c0,a1,b1,c1,tab0,Tlower
  integer:: ma,mb,mc
CONTAINS

!***********************************************************************
SUBROUTINE coolit_real (r,ee,lne,dd,dedt,lambda,temp)

  USE params
  USE eos
  implicit none

  integer, parameter :: not=14, noz=8, notcnm=22

  real, parameter, dimension(not) :: lnt= &         ! Vector of temperatures
  (/9.210, 9.441, 9.556, 9.786, 10.247, 10.822, 11.283, &
    12.319, 13.010, 13.700, 14.276, 15.082, 17.039, 19.572/)
  real, parameter, dimension(noz) :: lnz= &         ! Vector of metallicities
  (/-9.210, -6.908, -4.605, -3.454, -2.303, -1.151, 0.000, 1.151/)

  real, parameter, dimension(not, noz) :: logl= &   !  2D-grid in Lambda 
  reshape((/21.31, 23.82, 24.79, 25.43, 25.02, 24.67, 25.21, & !zero metals
            24.46, 24.19, 24.10, 24.10, 24.16, 24.45, 24.96, &
            21.31, 23.82, 24.79, 25.43, 25.01, 24.69, 25.22, & ![Fe/H]=-3.0
            24.58, 24.21, 24.12, 24.12, 24.15, 24.46, 24.96, &
            21.32, 23.82, 24.80, 25.42, 25.02, 24.82, 25.31, & !-2.0
            25.07, 24.49, 24.26, 24.23, 24.18, 24.47, 24.96, &
            21.35, 23.83, 24.81, 25.43, 25.03, 25.08, 25.47, & !-1.5
            25.51, 24.82, 24.49, 24.43, 24.25, 24.47, 24.96, &
            21.40, 23.85, 24.82, 25.42, 25.15, 25.43, 25.77, & !-1.0
            25.98, 25.24, 24.84, 24.75, 24.41, 24.48, 24.96, &
            21.50, 23.88, 24.80, 25.40, 25.26, 25.71, 26.03, & !-0.5
            26.20, 25.46, 25.11, 25.07, 24.60, 24.51, 24.97, &
            21.63, 24.00, 24.79, 25.42, 25.41, 26.02, 26.33, & !+0.0
            26.40, 25.67, 25.41, 25.42, 24.89, 24.63, 24.95, &
            21.91, 24.23, 25.01, 25.56, 25.82, 26.48, 26.78, & !+0.5
            26.87, 26.12, 25.88, 25.88, 25.32, 24.85, 25.04/), &
          (/not, noz/))  

  real, dimension(notcnm,2) :: loglcnm = & !Lambda Table for the CNM 
  reshape((/1.9, 2.0, 2.1, 2.2, 2.3, 2.4, 2.5, 2.6, 2.7, 2.8, 2.9,  & ! log(T)
            3.0, 3.1, 3.2, 3.3, 3.4, 3.5, 3.6, 3.7, 3.8, 3.9, 4.0,  & 
            -25.91, -25.82, -25.74, -25.66, -25.60, -25.54, -25.48, & ! log(L) 
            -25.43, -25.38, -25.33, -25.28, -25.24, -25.20, -25.16, &
            -25.13, -25.10, -25.08, -25.07, -25.06, -25.06, -25.05, &
            -24.95/),                                               &
          (/notcnm,2/))

  integer :: i, j, k, n, tind, zind, tcnm
  real, dimension(mx,my,mz) :: r, ee, lne, dd, lamb
  real, dimension(mx,my,mz), optional :: lambda, dedt, temp
  real, dimension(mx,my):: tt2

  real :: pt, qt, pz, qz, ct, lntemp, logtemp, lnmet, logmet, cgs2cu, cmb, &
          a1, a0, logfH2, logfH2res, op, nH, lognH,  nHlt1, sigma2, Lbound, ampl
  real :: WH2, WH2_0, WH2_1, temp1, plam, lam1

  if (.not. do_cool) return
  if (do_trace) print *,'coolit_real', present(dedt), present(lambda), present(temp)

  ct = 200./min(dx,dy,dz)   ! Cooling rate constant, 200 km/s fiducial speed
  ct = 0.5*Cdt/dt

  tind = 1
  zind = 1
  tcnm = 1

  cgs2cu= logclambu+48.    ! erg cm-3 s-1 -> ceu clu-3 ctu-1
  logfH2= 3.0              ! the log10(n(H)/n(H_2)) ratio in the gas  (-8 < logfH2 < 6) 
  logfH2res = 6.0          ! the residual fraction H/H_2 at T = 10 000 K
  op    = 1.               ! the n(ortho)/n(para) ratio of H_2 (0.1 < op < 3)
  sigma2= 0.01             ! width^2 of the H_2 cooling drop at T = 10 000 K
  hhrr_search = .false.    ! No search if logfH2 = 3.0 and op = 1.

!!!!$omp parallel do private(i,j,k,n,lnmet,lntemp,pt,qt,pz,qz,logmet,logtemp,nH,lognH,nHlt1, &
!!!!$omp   cmb,a1,a0,lbound,WH2_0,WH2_1,ampl,WH2), &
!!!!$omp   firstprivate (ct,tind,zind,tcnm,logfH2,logfH2res,op,sigma2,hhrr_search)
  do k=1,mz                           ! Calculating a (interpolated) Lambda
    if (do_ionization) then
      call eos2 (r(:,:,k),ee(:,:,k),tt=tt2)
      if (present(temp)) temp(:,:,k)=tt2
    end if
    do j=1,my                         ! for each (x,y,z) point. 
      do i=1,mx                         
        lnmet  = alog(dd(i,j,k))+lnfehsun
        logmet = invln10*lnmet               ! logmet > [Fe/H] = -10
        lnmet  = max(lnmet,-9.210)           ! lnmet >= [Fe/H] = -4

        if (do_ionization) then              ! detailed equation-of-state?
          lntemp = alog(tt2(i,j))            ! ln(T)
          temp1 = tt2(i,j)
        else
          lntemp = lne(i,j,k)-lnCv           ! ln(T)
          temp1 = exp(lntemp)
          if (present(temp)) temp(i,j,k)=temp1
        end if

        if (lntemp < lnt(1)) then            ! Below lnt table min?

          !--------------------------------------------------------------------
          !Add cooling by e- and H impact excitation of C+, O, N, Si+, S+, 
          !Fe+, (Dalgarno & McCray 1972, ARA&A, 10, 375) and collisional 
          !excitation of H_2 (Le Bourlot et al. 1999, MNRAS, 305, 802; see 
          !also http://ccp7.dur.ac.uk/) appropriate for the cool neutral 
          !medium (T<10 000 K). Torgny (2003/02/07).
          !-------------------------------------------------------------------- 

          logtemp=invln10*lntemp
          nH=noH*r(i,j,k)                    ! number of H atoms per cm^3
          lognH=invln10*alog(nH)
          nHlt1=0.0
          if (nH .lt. 1.) then               ! lower limit is nH=1. in H_2 cooling grid 
            nHlt1=lognH                      ! cooling is nearly linear in rho for low rho
            lognH=0.0                        ! note that lognH+nHlt1 = log10(nH)
          end if

          call heating(loglcnm(1,2),logmet,nH,lognH,nHlt1,logfH2,op,cmb)

          !--------------------------------------------------------------------    
          ! The total, low T (CNM) cooling (metal+H_2-cmb) at T=10 000 K is 
          ! set such that it equals the high T (WIM+HIM) cooling at T=10 000 K.   
          !--------------------------------------------------------------------

          call isearch(lnz,noz,lnmet,zind)   ! Find the nearest metallicity element
          if (lnmet < lnz(1)) then           ! below table min?
            zind=1
            lnmet=lnz(1)                     ! Don't exptrapolate below table
          endif 
          if (lnmet > lnz(noz)) zind=noz-1   ! extrapolation above table allowed          
   
          a1=(logl(1,zind+1)-logl(1,zind))/(lnz(zind+1)-lnz(zind))
          a0=logl(1,zind)-a1*lnz(zind)
          Lbound = a1*lnmet+a0-48.           ! fixed boundary at T = 10 000 K

          !---------------------------------------------------------------------
          ! There are two options, either, 1.), there is equipartition between 
          ! H_2 cooling and metal cooling at T = 10 000 K, or, 2.), the H_2 
          ! cooling is set to a residual value mimicking the drop in n(H_2) when 
          ! the temperature approaches T = 10 000 K.
          !---------------------------------------------------------------------  

          ! 1.)
!          Lbound = Lbound+invln10*alog(r(i,j,k)**2+cmb*exp(-ln10*Lbound)) &
!                 - 2*(lognH+nHlt1)-log2
!          loglcnm(notcnm,2) = Lbound-logmet      ! boundary value for metal cooling
!          call coolH2(4.0e0,lognH,logfH2,op,WH2) ! low-nH correction is substr. in ampl
!          ampl   = Lbound-(WH2-logfH2-lognH)     ! boundary value for H_2 cooling
         
          ! 2.)
          call coolH2(4.0e0,lognH,logfH2,op,WH2_0)      
          call coolH2(4.0e0,lognH,logfH2res,op,WH2_1)
          hhrr_search = .false.                     ! search for residual H/H_2 index OK
          ampl   = WH2_1-WH2_0+(logfH2-logfH2res)   ! boundary value for H_2 cooling       
          WH2_1  = WH2_1-logfH2res+lognH+2*nHlt1      
          Lbound = Lbound+invln10*alog(r(i,j,k)**2+exp(-ln10*Lbound) &
                 * (cmb-exp(ln10*WH2_1)))-2*(lognH+nHlt1)
          loglcnm(notcnm,2) = Lbound-logmet         ! boundary value for metal cooling        

          !--------------------------------------------------------------------
          ! Find the contribution of cooling by metals  
          !--------------------------------------------------------------------
 
          tcnm=max(min(floor(10.*(logtemp-loglcnm(1,1))+1.),notcnm-1),1)
!          call isearch(loglcnm(:,1),notcnm,logtemp,tcnm)   ! find nearest temp. element
!          if (logtemp < loglcnm(1,1)) tcnm=1               ! extrapolation below table allowed
!          if (logtemp > loglcnm(notcnm,1)) tcnm=notcnm-1   ! extrapolation above table allowed

          a1=(loglcnm(tcnm+1,2)-loglcnm(tcnm,2))/(loglcnm(tcnm+1,1)-loglcnm(tcnm,1))         
          a0=loglcnm(tcnm,2)+logmet-a1*loglcnm(tcnm,1)
          lamb(i,j,k)=nH**2*exp(ln10*(a1*logtemp+a0))      ! add metal cooling 

          !--------------------------------------------------------------------
          ! Add the cooling by H_2
          !--------------------------------------------------------------------

          if (logtemp < 2.0) then                    ! temperature >= 100 K ?
            call coolH2(2.1e0,lognH,logfH2,op,WH2_1) ! H_2 cooling at ln(T) = 4.835 (126 K)
            call coolH2(2.0e0,lognH,logfH2,op,WH2_0) ! H_2 cooling at ln(T) = 4.605 (100 K)
            WH2_1 = WH2_1+nHlt1                      ! corrected if nH < 1.
            WH2_0 = WH2_0+nHlt1                      ! corrected if nH < 1.
            a1 = 10*(WH2_1-WH2_0)                    ! linear extrapolation in log(T)
            a0 = WH2_0-a1*2.0
            lamb(i,j,k) = lamb(i,j,k) &                         ! add H_2 cooling (for T < 100 K)
                        + nH*exp(ln10*(a1*logtemp+a0-logfH2))
          else
            call coolH2(logtemp,lognH,logfH2,op,WH2)            ! H_2 cooling at T >= 100 K
            WH2 = WH2+nHlt1                                     ! corrected if nH < 1.
            WH2 = WH2+ampl*exp(-(logtemp-4.0)**2/sigma2)        ! add/substr. to fit boundary
            lamb(i,j,k) = lamb(i,j,k)+nH*exp(ln10*(WH2-logfH2)) ! add H_2 cooling (for T >= 100 K)
          end if
    
          !-----------------------------------------------------------------------
          !Finally, subtract the heating due to the CMB and convert to code units. 
          !-----------------------------------------------------------------------  
   
          lamb(i,j,k) = lamb(i,j,k)-cmb                    ! substract CMB heating
          lamb(i,j,k) = lamb(i,j,k)*exp(ln10*cgs2cu)       ! erg cm-3 s-1 -> ceu clu-3 ctu-1

        else                                 ! Cooling in a hot ionized medium!

          call isearch(lnt,not,lntemp,tind)  ! Find the nearest temperature element
          if (lntemp <= lnt(1)) tind=1       ! extrapolation below table allowed
          if (lntemp > lnt(not)) tind=not-1  ! extrapolation above table allowed

          call isearch(lnz,noz,lnmet,zind)   ! Find the nearest metallicity element
          if (lnmet < lnz(1)) then           ! below table min?
            zind=1
            lnmet=lnz(1)                     ! Don't exptrapolate below table
          endif 
          if (lnmet > lnz(noz)) zind=noz-1   ! extrapolation above table allowed

          ! Calculating Lambda at each point, using bi-linear interpolation

          pt = (lntemp-lnt(tind))/(lnt(tind+1)-lnt(tind))
          pz = (lnmet -lnz(zind))/(lnz(zind+1)-lnz(zind))
          pt = min(max(pt,0.),1.)
          pz = min(max(pz,0.),1.)
          qt = 1.-pt
          qz = 1.-pz
          lamb(i,j,k)= r(i,j,k)**2*exp(ln10*( logclambu &
                     + qt*(qz*logl(tind  ,zind)+pz*logl(tind  ,zind+1)) &
                     + pt*(qz*logl(tind+1,zind)+pz*logl(tind+1,zind+1))))
        end if  

        ! Prevent the cooling from limiting the time step -- OK because cooling
        ! is much slower at most temperatures.

!        if (i.eq.ma .and. k.eq.mc) then
!          print *,r(i,j,k),lamb(i,j,k)
!        end if
        if (temp1 .lt. Tlower*1.2) then
          lam1=-abs(lamb(i,j,k))
          plam=(Tlower*1.1-temp1)/(0.4*Tlower)
          plam=min(plam,1.)
          lamb(i,j,k)=lamb(i,j,k)+plam*(lam1-lamb(i,j,k))
        end if
!        lamb(i,j,k)=0.25*lamb(i,j,k)        ! FUDGE FACTOR FOR MISSING AVERAGE ATOMIC WEIGHT/1e2-24?
        if (present(lambda)) lambda(i,j,k)=lamb(i,j,k)
        if (present(dedt)) then 
          lamb(i,j,k) = sign(min(abs(lamb(i,j,k)),r(i,j,k)*ee(i,j,k)*ct),lamb(i,j,k))
          dedt(i,j,k) = dedt(i,j,k) - lamb(i,j,k)

        end if
      end do
    end do
  end do

  if (hhrr_search) then
    print *, 'H/H_2 and/or O/P index search initiated:'
    print *, 'Explicitly set indicies are changed!' 
    hhrr_search = .false.
  endif
  if (idbg>0 .and. isubstep==1) print *,'cool:',maxval(lamb)

END SUBROUTINE
END MODULE

!***********************************************************************
SUBROUTINE init_cooling (r,e)
  USE params
  USE cooling
  USE eos
  real, dimension(mx,my,mz):: r,e
  character(len=80):: id='$Id: ISM-cooling.f90,v 1.31 2004/07/13 08:37:52 aake Exp $'
  if (id.ne.'') print *,id ; id = ''

  coolH2file = '../src/COOLING/le_cube' ! file name of H_2 cooling grid 
  tablefile = file(1:index(file,'.'))//'eos'
  if (do_trace) print *,'init_cooling: read'
  do_cool = .true.
  do_table = .false.
  do_power = .false.
  Tlower = 1000.
  ma = mx ; mb = my ; mc = mz

  call read_cooling

  call initWH2 (coolH2file)             ! load H_2 cooling grid
  allocate (table(ma,mb,mc))
  a0=50. ; a1=10.                       ! make sure table is invalid
  b0=50. ; b1=10.
  c0=50. ; c1=10.
END SUBROUTINE

!***********************************************************************
SUBROUTINE read_cooling
  USE params
  USE cooling
  USE eos
  namelist /cool/do_cool,do_table,do_power,Tlower,ma,mb,mc,coolH2file

  if (stdin.ge.0) then
    rewind (stdin); read (stdin,cool)
  else
    read (*,cool)
  end if
  write (*,cool)
END SUBROUTINE

!***********************************************************************
SUBROUTINE read_table
  USE params
  USE cooling
  USE eos
  if (do_trace) print *,'read_table'
  open (3,file=tablefile,form='unformatted',status='old')
  read (3) ma,mb,mc,a0,b0,c0,a1,b1,c1,tab0
  print *,ma,mb,mc
  read (3) table
  close (3)
END SUBROUTINE

!***********************************************************************
SUBROUTINE coolit_table (r,ee,lne,dd,dedt,lambda)

  USE params
  USE cooling
  USE eos
  implicit none
  real, dimension(mx,my,mz):: r,ee,lne,dd
  real, dimension(mx,my,mz), optional:: dedt,lambda
  real, dimension(mx,my):: temp
  integer:: ix,iy,iz,ia,ib,ic
  real:: a,b,c,e,pa,pb,pc,qa,qb,qc,lntab,tab,lamb,ct
  real:: rmin,rmax,lnemin,lnemax,eemin,eemax,ddmin,ddmax

  if (.not. do_cool) return
  if (do_trace) print *,'coolit_table'

    rmin=minval(r  ) ;   rmax=maxval(r  )
  lnemin=minval(lne) ; lnemax=maxval(lne)
   ddmin=minval(dd ) ;  ddmax=maxval(dd )
   eemin=exp(lnemin) ;  eemax=exp(lnemax)

  if ( rmin.lt.exp(a0) .or.  rmax.gt.exp(a0+(ma-1)/a1) .or. &
      eemin.lt.exp(b0) .or. eemax.gt.exp(b0+(mb-1)/b1)) then
        call new_table (r,ee,lne,dd)
  end if
  ct = 200./min(dx,dy,dz)   ! Cooling rate constant, 200 km/s fiducial speed
  ct = 0.5*Cdt/dt

!$omp parallel do private(ix,iy,iz,a,b,c,ia,ib,ic,pa,pb,pc,qa,qb,qc,lntab,lamb,e,tab)
  do iz=1,mz
    if (do_ionization) then
      call eos2 (r(:,:,iz),ee(:,:,iz),tt=temp)
    else
      temp = utemp*ee(:,:,iz)
    end if
    do iy=1,my
      do ix=1,mx
        a = 1.+(alog(r(ix,iy,iz))  - a0)*a1
        b = 1.+(lne(ix,iy,iz)      - b0)*b1
        c = 1.+(alog(dd(ix,iy,iz)) - c0)*c1
        ia = min(max(1,ifix(a)),ma-1)
        ib = min(max(1,ifix(b)),mb-1)
        ic = min(max(1,ifix(c)),mc-1)
        pa = a-ia ; qa=1.-pa
        pb = b-ib ; qb=1.-pb
        pc = c-ic ; qc=1.-pc
        tab = &
          qc*(qb*(qa*table(ia  ,ib  ,ic  ) + pa*table(ia+1,ib  ,ic  ))  + &
              pb*(qa*table(ia  ,ib+1,ic  ) + pa*table(ia+1,ib+1,ic  ))) + &
          pc*(qb*(qa*table(ia  ,ib  ,ic+1) + pa*table(ia+1,ib  ,ic+1))  + &
              pb*(qa*table(ia  ,ib+1,ic+1) + pa*table(ia+1,ib+1,ic+1)))
        if (do_power) tab=tab**5
        lamb = tab*r(ix,iy,iz)**2
        if (present(lambda)) lambda(ix,iy,iz) = lamb
        if (present(dedt)) then
          e=r(ix,iy,iz)*ee(ix,iy,iz)
          lamb = sign(min(abs(lamb),e*ct),lamb)
          if (temp(ix,iy) .lt. Tlower) then                                     ! if temperature below limit ..
            lamb = -e*ct*(1.-temp(ix,iy)/Tlower)                                ! .. use artificial heating
          end if
          dedt(ix,iy,iz) = dedt(ix,iy,iz) - lamb
        end if
      end do
    end do
  end do
END SUBROUTINE

!***********************************************************************
SUBROUTINE new_table (r,ee,lne,dd)
  USE params
  USE cooling
  USE eos
  implicit none
  real, dimension(mx,my,mz):: r,ee,lne,dd,dedt
  real, allocatable, dimension(:,:,:):: rtab,etab,ltab,dtab,temp
  real:: tabmin,tabmax,a2,b2,c2
  integer:: ix,iy,iz
!
!  Check that the table is not too large
!
  if (ma.gt.mx .or. mb.gt.my .or. mc.gt.mz) then
    print *,'dimensions too large in new_table:', ma, mb, mc
    stop
  end if
  print *,'========================== new_table ============================='
  allocate (rtab(mx,my,mz), etab(mx,my,mz), ltab(mx,my,mz), dtab(mx,my,mz))
!
!  Limits and steps in ln(rho), ln(ee), and ln(dd)
!
  a0 = minval(alog(r) ) - 1. ; a2 = maxval(alog(r) ) + 1. ; a1 = (a1-a0)/(ma-1) 
  b0 = minval(lne     ) - 1. ; b2 = maxval(lne     ) + 1. ; b1 = (b1-b0)/(mb-1)
  c0 = minval(alog(dd)) - 1. ; c2 = maxval(alog(dd)) + 1. ; c1 = (c1-c0)/(mc-1)
!
!  Round off step sizes in the table and "snap" on to multiple of step for the
!  energy and abundance interpolations.
!
  c0 = alog(1.3228e-3*1e-7) ; c1 = alog(1e8)/(mc-1)     ! cover -7 < [Fe/H] < 1
  b1 = 0.1*floor(b1/0.1+0.9) ; b0 = b1*floor(b0/b1)
  c1 = 0.1*floor(c1/0.1+0.9) ; c0 = c1*floor(c0/c1) ; c2=c0+c1*(mc-1)

  print *,'lnrho: min,step,max =',a0,a1,a2
  print *,'lnee : min,step,max =',b0,b1,b2
  print *,'lndd : min,step,max =',c0,c1,c2
  print *,'=================================================================='
!
!  Initialize to reasonable values
!
  rtab = exp(a0+a1*ma/2)
  ltab = b0+b1*mb/2
  dtab = exp(c0+c1*mc/2)
  etab = rtab*exp(ltab)
  table = 0.
!
!  Loop over table
!
  do iz=1,mc
    dtab(:,:,iz) = exp(c0+(iz-1)*c1)
    do iy=1,mb
      ltab(:,iy,iz) = b0+(iy-1)*b1
      do ix=1,ma
        rtab(ix,iy,iz) = exp(a0+(ix-1)*a1)
        etab(ix,iy,iz) = rtab(ix,iy,iz)*exp(ltab(ix,iy,iz))
      end do
    end do
  end do
!
!  Calculate the cooling
!
  allocate (temp(ma,mb,mc))
  call coolit_real (rtab,etab,ltab,dtab,lambda=table,temp=temp)
  table=table/rtab**2
  call stats('lambda',table)
  call stats('temp',temp)
  tabmin=minval(table)
  tabmax=maxval(table)
  tab0=tabmin-0.001*abs(tabmin)
  if (tab0.eq.0.) tab0=-1e-5*tabmax
  if (do_power) table=sign(abs(table)**0.2,table)
!
!  Take out the main density dependence, and invert steps
!
!  table = log((table-tab0))
  a1=1./a1 ; b1=1./b1 ; c1=1./c1
!
!  Write the table file
!
  open (3,file=tablefile,form='unformatted',status='unknown')
  write (3) ma,mb,mc,a0,b0,c0,a1,b1,c1,tab0
  write (3) (((table(ix,iy,iz), ix=1,ma), iy=1,mb), iz=1,mc)
  write (3) (((temp(ix,iy,iz), ix=1,ma), iy=1,mb), iz=1,mc)
  close (3)
  deallocate (rtab, etab, ltab, dtab, temp)

END SUBROUTINE


!-------------------------------------------------------------------------
SUBROUTINE heating (ccnm,z,nH,lognH,nHlt1,logfH2,op,cmb)

USE params
USE eos
USE cooling
implicit none

real :: ccnm
real :: z, nH, lognH, nHlt1, logfH2, op, cmb, y0, y1, ycmb
  
  call coolH2(2.1e0,lognH,logfH2,op,y1)    ! H_2 cooling at ln(T) = 4.835 (126 K)
  call coolH2(2.0e0,lognH,logfH2,op,y0)    ! H_2 cooling at ln(T) = 4.605 (100 K)
  ycmb= 2*y0-y1+nHlt1-logfH2               ! extrapolated H_2 cooling at T ~ 80 K
  cmb = nH*exp(ln10*ycmb) &                ! H_2 cooling
      + nH**2*exp(ln10*(ccnm+z))           ! metal cooling

END SUBROUTINE 


!----------------------------------------------------------------
subroutine initWH2 (filename)
!  H2 global cooling
!    W = W(T, nH, H/H2, O/P)
!
!  npt : Nb of points in Temperature grid (TN)
!  npd : Nb of points in Density grid (DN)
!  nph : Nb of points in H/H2 ratio grid (RH)
!  npr : Nb of points in Ortho/Para ratio grid (ROP)

  USE params
  USE eos
  USE cooling
  implicit none

  integer, parameter :: npt=23, npd=49, nph=36, npr=6
  integer :: itt, idd, ihh, ioo
  real    :: TNmin, TNmax, DNmin, DNmax
  character (len=40) filename

  allocate (TN(npt), &
            DN(npd), &
            RH(nph), & 
            ROP(npr))
  allocate (WG(npt,npd,nph,npr))

  data TNmin, TNmax / 100.0e0, 1.0e+04 /
  data DNmin, DNmax / 1.0e0, 1.0e+08 /

!  Read Cooling file (binary)

  open (10, file=filename, status="old")

  read(10,*) RH
  read(10,*) ROP
  read(10,*) TN
  read(10,*) DN
  read(10,*) ((((WG(itt,idd,ihh,ioo), idd=1,npd), itt=1,npt), ioo=1,npr), ihh=1,nph)

  close (10)

  return
end subroutine



!---------------------------------------------------------------------------
subroutine coolH2(tt, dd, hh, rr, WH2)
! - Compute interpolated value in 4D-grid

  USE params
  USE eos
  USE cooling
  implicit none

  integer, parameter :: npt=23, npd=49, nph=36, npr=6  
  integer ::  i, itt, idd, ihh, irr
  real    :: tt, dd, hh, rr, WH2, un, dix, tp, up, vp, wp 
  real    :: y0000, y1000, y0100, y1100, y0010, y1010, y0110, y1110, &
             y0001, y1001, y0101, y1101, y0011, y1011, y0111, y1111

  itt = 20        ! temperature is weighted towards T = 10 000 K
  idd =  7        ! density lies mostly in the interval 1. < n_H < 100.
  ihh = 29        ! H/H_2 index corresponding to logfH2 = 3.
  irr =  5        ! ortho/para index corresponding to op = 1.

  data un, dix / 1.0e0, 10.0e0 /

! - Check that the point is inside our grid
!  print *, tt, dd, hh, rr

  if (tt .lt. TN(1) .or. tt .gt. TN(npt)) then
    print *, " Value out of range (tt)!"
    print *, " T should be in the range:", dix**TN(1), " - ", dix**TN(npt)
  endif

  if (dd .lt. DN(1) .or. dd .gt. DN(npd)) then
    print *, " Value out of range (dd)!"
    print *, " nH should be in the range:", dix**DN(1), " - ", dix**DN(npd)
  endif

  if (hh .lt. RH(1) .or. hh .gt. RH(nph)) then
    print *, " Value out of range (hh)"
    print *, " H/H2 should be in the range:", dix**RH(1), " - ", dix**RH(nph)
  endif

  if (rr .lt. ROP(1) .or. rr .gt. ROP(npr)) then
    print *, " Value out of range (rr)"
    print *, " O/P should be in the range:", ROP(1), " - ", ROP(npr)
  endif

!  Find place in grid


  itt=max(min(floor(11.*(tt-TN(1))+1.),npt-1),1)
  idd=max(min(floor(6.*(dd-DN(1))+1.),npd-1),1)
!  call isearch(TN,npt,tt,itt)            ! find temperature index
!  call isearch(DN,npd,dd,idd)            ! find density index

  if ((hh-3.) .gt. EPS) then             ! find H/H_2 index, if neccesary
    hhrr_search = .true.
    call isearch(RH,nph,hh,ihh)
    if ((ihh .lt. 1) .or. (ihh .gt. nph-1)) then
      print *, "H/H_2 index out of range!"
    endif
  endif
  if ((rr-1.) .gt. EPS) then             ! find ortho/para index, if neccesary
    hhrr_search = .true.
    call isearch(ROP,npr,rr,irr)
    if ((irr .lt. 1) .or. (irr .gt. npr-1)) then
      print *, "ortho/para index out of range!"
    endif
  endif

!  print *, itt, idd, ihh, irr

!  Do a simple linear interpolation

       y0000 = WG(itt  ,idd  ,ihh  ,irr  )
       y1000 = WG(itt+1,idd  ,ihh  ,irr  )
       y0100 = WG(itt  ,idd+1,ihh  ,irr  )
       y1100 = WG(itt+1,idd+1,ihh  ,irr  )
       y0010 = WG(itt  ,idd  ,ihh+1,irr  )
       y1010 = WG(itt+1,idd  ,ihh+1,irr  )
       y0110 = WG(itt  ,idd+1,ihh+1,irr  )
       y1110 = WG(itt+1,idd+1,ihh+1,irr  )
       y0001 = WG(itt  ,idd  ,ihh  ,irr+1)
       y1001 = WG(itt+1,idd  ,ihh  ,irr+1)
       y0101 = WG(itt  ,idd+1,ihh  ,irr+1)
       y1101 = WG(itt+1,idd+1,ihh  ,irr+1)
       y0011 = WG(itt  ,idd  ,ihh+1,irr+1)
       y1011 = WG(itt+1,idd  ,ihh+1,irr+1)
       y0111 = WG(itt  ,idd+1,ihh+1,irr+1)
       y1111 = WG(itt+1,idd+1,ihh+1,irr+1)

       tp = (tt - TN(itt))  /   (TN(itt+1) - TN(itt))
       up = (dd - DN(idd))  /   (DN(idd+1) - DN(idd))
       vp = (hh - RH(ihh))  /   (RH(ihh+1) - RH(ihh))
       wp = (rr - ROP(irr)) / (ROP(irr+1) - ROP(irr))

       WH2 = (un-tp) * (un-up) * (un-vp) * (un-wp) * y0000 &
           +    tp   * (un-up) * (un-vp) * (un-wp) * y1000 &
           + (un-tp) *    up   * (un-vp) * (un-wp) * y0100 &
           +    tp   *    up   * (un-vp) * (un-wp) * y1100 &
           + (un-tp) * (un-up) *    vp   * (un-wp) * y0010 &
           +    tp   * (un-up) *    vp   * (un-wp) * y1010 &
           + (un-tp) *    up   *    vp   * (un-wp) * y0110 &
           +    tp   *    up   *    vp   * (un-wp) * y1110 &
           + (un-tp) * (un-up) * (un-vp) *    wp   * y0001 &
           +    tp   * (un-up) * (un-vp) *    wp   * y1001 &
           + (un-tp) *    up   * (un-vp) *    wp   * y0101 &
           +    tp   *    up   * (un-vp) *    wp   * y1101 &
           + (un-tp) * (un-up) *    vp   *    wp   * y0011 &
           +    tp   * (un-up) *    vp   *    wp   * y1011 &
           + (un-tp) *    up   *    vp   *    wp   * y0111 &
           +    tp   *    up   *    vp   *    wp   * y1111

  return
end subroutine


!---------------------------------------------------------------------
SUBROUTINE isearch (xarr,n,x,ilo)
!---------------------------------------------------------------------
! Given an array xarr(1,...,n) and given a value x, isearch returns
! the nearest lower index ilo such that xarr(ilo) <= x < xarr(ilo+1).
! Note that ilo=0 or ilo=n is returned to indicate that x is out of 
! range. ilo on input is taken as the initial guess for ilo on output.
!---------------------------------------------------------------------

  implicit none

  integer :: n, ilo, ihi, im, inc
  real, dimension(n) :: xarr
  real :: x

  if ((ilo .le. 0) .or. (ilo .gt. n)) then
    ilo=0
    ihi=n+1
  else
    inc=1
    if (x .ge. xarr(ilo)) then          ! hunt upwards from initial guess
      if (ilo .eq. n) return
      ihi=ilo+1
      do
        if (x .lt. xarr(ihi)) exit
        ilo=ihi
        inc=inc+1
        ihi=ilo+inc
        if (ihi .gt. n) then
          ihi=n+1
          exit
        endif 
      enddo
    else                                ! hunt downwards from initial guess
      if (ilo .eq. 1) then
        ilo=0
        return
      endif
      ihi=ilo
      ilo=ilo-1
      do 
        if (x .ge. xarr(ilo)) exit
        ihi=ilo
        inc=inc+1
        ilo=ihi-inc
        if (ilo .le. 0) then
          ilo=0
          exit
        endif
      enddo
    endif
  endif
  
  do                                    ! bisection search
    if ((ihi-ilo) .eq. 1) exit
    im=(ihi+ilo)/2
    if (x .ge. xarr(im)) then
      ilo=im
    else
      ihi=im
    endif
  enddo
  if (x .eq. xarr(1)) ilo=1
  if (x .eq. xarr(n)) ilo=n-1     

END SUBROUTINE

!***********************************************************************
SUBROUTINE coolit (r,ee,lne,dd,dedt)
!
!  Wrapper routine, to allow calling the real routine from new_table
!
  USE params
  USE eos
  USE cooling
  implicit none
  real, dimension(mx,my,mz):: r,ee,lne,dd,dedt
  real, dimension(:,:,:), allocatable:: temp,lamb
  integer i,j,k,iz
!
  if (.not. do_cool) return
!
  do k=1,mz
    do j=1,my
      do i=1,mx
        dd(i,j,k)=max(dd(i,j,k),zlim)   ! sets min(dd) = zlim to avoid negative values
      end do
    end do
  end do
  
  if (do_trace) print *,'coolit'
  if (do_table) then
    call coolit_table (r,ee,lne,dd,dedt)
  else
    call coolit_real (r,ee,lne,dd,dedt)
  end if
!
!  Optional diagnostics
!
  if (idbg > 1) then
    allocate (temp(mx,my,mz), lamb(mx,my,mz))
    do iz=1,mz
      call eos2(r(:,:,iz),ee(:,:,iz),tt=temp(:,:,iz))
    end do
    call coolit_real (r,ee,lne,dd,lambda=lamb,temp=temp)
    call stats('temp',temp)
    call stats('lamb',lamb)
    deallocate (temp, lamb)
  end if
END SUBROUTINE
