!***********************************************************************
SUBROUTINE transfer (np,n1,dtau,s,q,xi,doxi)
!
  USE params
  implicit none
!
  real, dimension(mx,my,mz) :: dtau,s,q
  real, dimension(mx,mz)    :: xi
  real, dimension(mx,my)    :: ex0, ex1, ex2
  real, dimension(mx,my)    :: iplus, iminus
  real, dimension(mx)       :: ex
  real :: taum, a, b, c, dsdt, d2sdt2
  integer :: ix, iy, iz, np, n1
  logical doxi
!
  character(len=80):: id='$Id: transfer_difference.f90,v 1.2 2009/03/08 17:24:54 aake Exp $'
!
!  Calculate 1-exp(-tau(l,1,n)) straight or by series expansion,
!  based on max tau(l,1,n).
!
!$omp do private(ix,iy,iz,taum,ex)
  do iz=1,mz
    do ix=1,mx
      if (dtau(ix,1,iz).gt.0.1) then
        ex=1.-exp(-dtau(ix,1,iz))
      else
        ex=dtau(ix,1,iz)*(1.-.5*dtau(ix,1,iz)*(1.-.3333*dtau(ix,1,iz)))
      endif
      iminus(ix,1) = s(ix,1,iz)*ex(ix)
    end do
!
!  Incoming rays (2a+3m)
!
    do iy=2,np
      do ix=1,mx
        a = 0.5*dtau(ix,iy,iz)
        c = 1.+a
        iminus(ix,iy) = ((1.-a)*iminus(ix,iy-1)+a*(s(ix,iy,iz)+s(ix,iy-1,iz)))/c
      end do
    end do
!
!  Outgoing rays (2a+3m)
!
    do ix=1,mx
      dsdt = (s(ix,np,iz)-s(ix,np-1,iz))/dtau(ix,np,iz)
      d2sdt2 = (dsdt-(s(ix,np-1,iz)-s(ix,np-2,iz))/dtau(ix,np-1,iz)) &
             *2./(dtau(ix,np-1,iz)+dtau(ix,np,iz))
      dsdt = dsdt+0.5*dtau(ix,np,iz)*d2sdt2
      iplus(ix,np) = s(ix,np,iz)+dsdt+d2sdt2
    end do
    do iy=np,2,-1
      do ix=1,mx
        a = 0.5*dtau(ix,iy,iz)
        c = 1.+a
        iplus(ix,iy-1) = ((1.-a)*iplus(ix,iy)+a*(s(ix,iy,iz)+s(ix,iy-1,iz)))/c
      end do
    end do
!
!  Results
!
    q(:, 1,iz) = 0.5*(iplus(:, 1)+iminus(:, 1))-s(:,1 ,iz)
    q(:,np,iz) = 0.5*(iplus(:,np)+iminus(:,np))-s(:,np,iz)
    do iy=2,np-1
    do ix=1,mx
      if (dtau(ix,iy,iz)+dtau(ix,iy+1,iz) .lt. 1e1) then
        q(ix,iy,iz) = 0.5*(iplus(ix,iy)+iminus(ix,iy))-s(ix,iy,iz)
      else
        q(ix,iy,iz) = ((s(ix,iy+1,iz)-s(ix,iy  ,iz))/dtau(ix,iy+1,iz) &
                      -(s(ix,iy  ,iz)-s(ix,iy-1,iz))/dtau(ix,iy  ,iz)) &
                      *2./(dtau(ix,iy+1,iz)+dtau(ix,iy,iz))
      end if
    end do
    end do
!
!  Surface intensity.
!
    if (doxi) then
      xi(:,iz)=2.*(1.-ex(:))*(q(:,1,iz)+s(:,1,iz))+s(:,1,iz)*ex(:)**2
    end if

  end do
!$omp end do
!
END
