!=======================================================================
SUBROUTINE check_linearity (f, n1, n2, nfail)
  USE params
  implicit none
  integer n1, n2, nfail
  real f(n1,n2), f1, f2, fij
  logical failed
  integer i, j
!.......................................................................
  nfail = 0
  do i=1,n1
    f1 = f(i,1)
    f2 = f(i,n2)
    failed = .false.
    do j=1,n2
      fij = ((j-1)*f2 + (n2-j)*f1)/real(n2-1)
      if (abs(fij-f(i,j)) > 1e-5*f(i,j)) failed = .true.
    end do
    if (failed) nfail = nfail+1
  end do
END SUBROUTINE

!=======================================================================
SUBROUTINE interpolate_x (fin, dxdy, fout, nymax)
  USE params
  implicit none
  real, dimension(mx,my,mz):: fin, fout
  integer:: nymax, ix, iy, iz
  real   :: dxdy, px, qx, dixdy
!.......................................................................
  call mpi_send_x (fin, gx1, 0, hx1, 1)
  dixdy = dxdy/dx
  do iy=1,nymax
    px = dixdy*ym(iy)
    px = px-floor(px)
    qx = 1.-px
    do iz=izs,ize
      do ix=1,mx-1
        fout(ix,iy,iz) = px*fin(ix+1,iy,iz) + qx*fin(ix,iy,iz)
      end do
      fout(mx,iy,iz) = px*hx1(1,iy,iz) + qx*fin(mx,iy,iz)
    end do
  end do
END SUBROUTINE interpolate_x

!=======================================================================
SUBROUTINE interpolate_z (fin, dzdy, fout, nymax)
  USE params
  implicit none
  real, dimension(mx,my,mz):: fin, fout
  integer:: nymax, ix, iy, iz
  real   :: dzdy, pz, qz, dizdy
!.......................................................................
  call mpi_send_z (fin, hz1, 0, hz1, 1)
  dizdy = dzdy/dz
  do iy=1,nymax
    pz = dizdy*ym(iy)
    pz = pz-floor(pz)
    qz = 1.-pz
    do iz=izs,min(ize,mz-1)
      do ix=1,mx
        fout(ix,iy,iz) = pz*fin(ix,iy,iz+1) + qz*fin(ix,iy,iz)
      end do
    end do
    if (ize==mz) then
      do ix=1,mx
        fout(ix,iy,mz) = pz*hz1(ix,iy,   1) + qz*fin(ix,iy,mz)
      end do
    end if
  end do
END SUBROUTINE interpolate_z
