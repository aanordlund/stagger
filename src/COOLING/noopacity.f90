! $Id: noopacity.f90,v 1.1 2003/04/10 19:27:46 aake Exp $
! ----------------------------------------------------------------------
SUBROUTINE opacity (temp, pe, kappa)
!
!  Absorption coefficient [cm^2/g] for H-, scaled to a value 3e-26 cm^4/dyne
!  at T=6300 K extracted from Fig. 3 of Doughty & Fraser MNRAS 132, 267
!  (1966).  A mean atomic weight of 1.4 (10% helium by number) is assumed.
!
  USE params
  real, dimension(mx,my,mz):: temp, pe, kappa

END
