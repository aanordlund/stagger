! $Id: global_disk_cooling.f90,v 1.10 2006/06/12 07:48:07 aake Exp $
!***********************************************************************
MODULE cooling
  real dedt0,t_cool,rho_cool,ee_cool,p_cool
END MODULE

!***********************************************************************
SUBROUTINE init_cooling (r,e)
  USE params
  USE cooling
  real, dimension(mx,my,mz):: r,e
  do_cool = .false.
  call read_cooling
END SUBROUTINE

!***********************************************************************
SUBROUTINE read_cooling
  USE params
  USE cooling
  USE forcing
  namelist /cool/do_cool,dedt0,t_cool,rho_cool,ee_cool,p_cool

  dedt0 = 1e-12/1e-5
  t_cool = 0.05
  rho_cool = 1.
  ee_cool = ee0
  p_cool = 4.
  if (stdin.ge.0) then
    rewind (stdin); read (stdin,cool)
  else
    read (*,cool)
  end if
  if (master) write (*,cool)
END SUBROUTINE

!***********************************************************************
SUBROUTINE coolit (r,ee,lne,dd,dedt)
  USE params
  implicit none
  real, dimension(mx,my,mz) :: r,ee,lne,dd,dedt
  logical omp_in_parallel

  if (omp_in_parallel()) then
    call coolit_omp (r,ee,lne,dd,dedt)
  else
    !$omp parallel
    call coolit_omp (r,ee,lne,dd,dedt)
    !$omp end parallel
  end if
END

!***********************************************************************
SUBROUTINE coolit_omp (r,ee,lne,dd,dedt)
  USE params
  USE forcing
  USE cooling
  implicit none
  real, dimension(mx,my,mz) :: r,ee,lne,dd,dedt
  integer ix,iy,iz
  real e_profile
  character(len=mid):: id='$Id: global_disk_cooling.f90,v 1.10 2006/06/12 07:48:07 aake Exp $'

  call print_id (id)

!-----------------------------------------------------------------------
!  The main cooling per unit mass is assumed to be proportional to the
!  density, even under optically thick conditions.  The rationale is 
!  that a correctly treated cooling leads to a cooling time that scales
!  in proportion to the surface density over T**3.
!
!  Add a very weak constant heating, which will prevent the density from
!  dropping too much.  For example, if dedt0 is 1e-11/1e-5, the timestep
!  is 1e-5 and the energy density dropped towards 1e-11, the heating would
!  become strong, and the density would come back up.
!-----------------------------------------------------------------------

  do iz=izs,ize
  do iy=1,my
  do ix=1,mx
    e_profile = (ee(ix,iy,iz)/(ee_cool+ee(ix,iy,iz)))**p_cool &
                *exp(-ee(ix,iy,iz)/ee_cool+p_cool)
    dedt(ix,iy,iz) = dedt(ix,iy,iz) + dedt0 &
                   - e_profile*r(ix,iy,iz)/(rho_cool*t_cool)
  end do
  end do
  end do
  return
END SUBROUTINE
