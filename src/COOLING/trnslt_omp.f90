! $Id: trnslt_omp.f90,v 1.3 2007/01/14 08:49:28 aake Exp $
!***********************************************************************
SUBROUTINE trnslt (fin,fout,dxdy,dzdy,do_exp,ny)
  USE params
  implicit none
  real, dimension(mx,my,mz):: fin, fout
  intent(in):: fin
  intent(out):: fout
  real dxdy, dzdy
  logical do_exp
  integer ny
  logical omp_in_parallel
  character(len=mid):: id="$Id: trnslt_omp.f90,v 1.3 2007/01/14 08:49:28 aake Exp $"
!-----------------------------------------------------------------------
  call print_id (id)
  if (omp_in_parallel()) then
    call trnslt_omp (fin,fout,dxdy,dzdy,do_exp,ny)
  else
    !$omp parallel shared(dxdy,dzdy,do_exp,ny)
    call trnslt_omp (fin,fout,dxdy,dzdy,do_exp,ny)
    !$omp end parallel
  end if
END

!***********************************************************************
SUBROUTINE trnslt_omp (fin,fout,dxdy,dzdy,do_exp,ny)
!
!  Translate a scalar field to an inclined coordinate system.
!
!  Operation count:  8m+10a = 18 flops/pnt
!                    8m+6a  = 14 flops/pnt
!
!***********************************************************************
!
  USE params
  implicit none
  real, dimension(mx,my,mz):: fin, fout
  intent(in):: fin
  intent(out):: fout
  real dxdy, dzdy
  logical do_exp
  integer ny
  real, dimension(mx):: ftmp
  real, dimension(mx,mz):: f, g
  integer ix, iy, iz, k, lm1, lp0, lp1, lp2, mm1, mp0, mp1, mp2
  real p, q, ad, bd, ac, bc, af, bf, xk, zk
!-----------------------------------------------------------------------
!
!  Trap vertical rays
!
  if (abs(dxdy).lt.1e-6.and.abs(dzdy).lt.1.e-6) then
    if (do_exp) then
      do iz=izs,ize
        fout(:,:,iz) = exp(fin(:,:,iz))
      end do
    else
      do iz=izs,ize
        fout(:,:,iz) = fin(:,:,iz)
      end do
    end if
    return
  end if
!
!  y-loop
!
  if (abs(dxdy).lt.1e-6) then
!$omp barrier
    do iz=izs,ize
    do iy=1,ny
      zk=dzdy*ym(iy)/sz
      zk=amod(zk,1.)
      if(zk.lt.0.) zk=zk+1.
      zk=mz*zk
      k=zk
      p=zk-k
      k=k+mz
      q=1.-p
      af=q+p*q*(q-p)
      bf=p-p*q*(q-p)
      ad=p*q*q*0.5
      bd=-p*q*p*0.5
      ac=af-bd
      bc=bf+ad
!
!  Interpolate using cubic splines [4m+5a] [4m+3a]
!
      mm1 = mod(iz+k-2,mz)+1
      mp0 = mod(iz+k-1,mz)+1
      mp1 = mod(iz+k  ,mz)+1
      mp2 = mod(iz+k+1,mz)+1
      do ix=1,mx
        ftmp(ix)=ac*fin(ix,iy,mp0)+bc*fin(ix,iy,mp1)
      end do
      if (do_exp) then
        do ix=1,mx
	  ftmp(ix)=ftmp(ix)-ad*fin(ix,iy,mm1)+bd*fin(ix,iy,mp2)
        end do
        call expn (mx,ftmp,fout(1,iy,iz))
      else
        do ix=1,mx
	  fout(ix,iy,iz)=ftmp(ix)-ad*fin(ix,iy,mm1)+bd*fin(ix,iy,mp2)
        end do
      end if
    end do
    end do
    return
  else if (abs(dzdy).lt.1e-6) then
!$omp barrier                                              ! NOT NEEDEED ?
    if (idbg > 2) print *,'trnslt: only x', omp_mythread, izs, ize
    do iz=izs,ize
    do iy=1,ny
      xk=dxdy*ym(iy)/sx
      xk=amod(xk,1.)
      if(xk.lt.0.) xk=xk+1.
      xk=mx*xk
      k=xk
      p=xk-k
      k=k+mx
      q=1.-p
      af=q+p*q*(q-p)
      bf=p-p*q*(q-p)
      ad=p*q*q*0.5
      bd=-p*q*p*0.5
      ac=af-bd
      bc=bf+ad
!
!  Interpolate using cubic splines [4m+5a] [4m+3a]
!
      lm1 = 1+k-1; if (lm1.lt.1) lm1=lm1+mx; if (lm1.gt.mx) lm1=lm1-mx
      if (do_exp) then
        do ix=1,mx
          lp0 = lm1+1 ; if (lp0.gt.mx) lp0=lp0-mx
          lp1 = lp0+1 ; if (lp1.gt.mx) lp1=lp1-mx
          lp2 = lp1+1 ; if (lp2.gt.mx) lp2=lp2-mx
          ftmp(ix)      =(ac*fin(lp0,iy,iz)+bc*fin(lp1,iy,iz)- &
                          ad*fin(lm1,iy,iz)+bd*fin(lp2,iy,iz))
          lm1 = lm1+1 ; if (lm1.gt.mx) lm1=lm1-mx
        end do
	call expn (mx,ftmp,fout(1,iy,iz))
      else
        do ix=1,mx
          lp0 = lm1+1 ; if (lp0.gt.mx) lp0=lp0-mx
          lp1 = lp0+1 ; if (lp1.gt.mx) lp1=lp1-mx
          lp2 = lp1+1 ; if (lp2.gt.mx) lp2=lp2-mx
          fout(ix,iy,iz)=ac*fin(lp0,iy,iz)+bc*fin(lp1,iy,iz)- &
                         ad*fin(lm1,iy,iz)+bd*fin(lp2,iy,iz)
          lm1 = lm1+1 ; if (lm1.gt.mx) lm1=lm1-mx
        end do
      end if
    end do
    end do
    return
  else
!$omp barrier
    do iz=izs,ize
    do iy=1,ny
      zk=dzdy*ym(iy)/sz
      zk=amod(zk,1.)
      if(zk.lt.0.) zk=zk+1.
      zk=mz*zk
      k=zk
      p=zk-k
      k=k+mz
      q=1.-p
      af=q+p*q*(q-p)
      bf=p-p*q*(q-p)
      ad=p*q*q*0.5
      bd=-p*q*p*0.5
      ac=af-bd
      bc=bf+ad
!
!  Interpolate using cubic splines [4m+5a] [4m+3a]
!
      mm1 = mod(iz+k-2,mz)+1
      mp0 = mod(iz+k-1,mz)+1
      mp1 = mod(iz+k  ,mz)+1
      mp2 = mod(iz+k+1,mz)+1
      do ix=1,mx
        fout(ix,iy,iz)=ac*fin(ix,iy,mp0)+bc*fin(ix,iy,mp1)- &
                       ad*fin(ix,iy,mm1)+bd*fin(ix,iy,mp2)
      end do
    end do
    end do
    do iz=izs,ize
    do iy=1,ny
      xk=dxdy*ym(iy)/sx
      xk=amod(xk,1.)
      if(xk.lt.0.) xk=xk+1.
      xk=mx*xk
      k=xk
      p=xk-k
      k=k+mx
      q=1.-p
      af=q+p*q*(q-p)
      bf=p-p*q*(q-p)
      ad=p*q*q*0.5
      bd=-p*q*p*0.5
      ac=af-bd
      bc=bf+ad
!
!  Interpolate using cubic splines [4m+5a] [4m+3a]
!
      lm1 = 1+k-1 ; if (lm1.lt.1) lm1=lm1+mx
      do ix=1,mx
        lp0 = lm1+1 ; if (lp0.gt.mx) lp0=lp0-mx
        lp1 = lp0+1 ; if (lp1.gt.mx) lp1=lp1-mx
        lp2 = lp1+1 ; if (lp2.gt.mx) lp2=lp2-mx
        ftmp(ix)=ac*fout(lp0,iy,iz)+bc*fout(lp1,iy,iz)- &
                 ad*fout(lm1,iy,iz)+bd*fout(lp2,iy,iz)
        lm1 = lm1+1 ; if (lm1.gt.mx) lm1=lm1-mx
      end do
      if (do_exp) then
	call expn (mx,ftmp,fout(1,iy,iz))
      else
        do ix=1,mx
          fout(ix,iy,iz) = ftmp(ix)
        end do
      end if
    end do
    end do
  end if
!
END

!***********************************************************************
SUBROUTINE trnslt_test
  USE params
  implicit none
  real, allocatable, dimension(:,:,:) :: fin,fout,fcmp
  integer count,ix,iy,iz
  real cpux,dxdy,dzdy,fdtime,k,kx,ky,kz,dsdy

  mx = 100
  my = mx
  mz = mx
  do_trace = .true.

  allocate (fin(mx,my,mz),fout(mx,my,mz),fcmp(mx,my,mz))
  allocate (dxm(0:mx),dym(0:my),dzm(0:mz))
  allocate (xm(0:mx),ym(0:my),zm(0:mz))

  k = 2.*pi/mx
  dxm = 1.
  dym = 1.
  dzm = 1.
  xm = (/( ix*dxm(1), ix=1,mx )/)
  ym = (/( iy*dym(1), iy=1,my )/)
  zm = (/( iz*dzm(1), iz=1,mz )/)
  kx = 2.*pi/(mx*dxm(1))
  ky = 2.*pi/(my*dym(1))
  kz = 2.*pi/(mz*dzm(1))

!  Testing x-direction

  dsdy = 1.
  dxdy = dsdy
  dzdy = 0.
!$omp parallel private(iz)
  do iz=izs,ize
  do iy=1,my
  do ix=1,mx
    fin(ix,iy,iz) = cos(ix*k)+cos(iy*k)+cos(iz*k)
    fcmp(ix,iy,iz) = cos(ix*k+dxdy*ym(iy)*ky)+cos(iy*k)+cos(iz*k+dzdy*ym(iy)*kz)
  end do
  end do
  end do
!$omp end parallel
  cpux = fdtime()
  cpux = 0.
  count = 0
  do while (cpux .lt. 1.)
    call trnslt(fin,fout,dxdy,dzdy,.false.,my)
    cpux = cpux+fdtime()
    count = count+1
  end do
  print *,'trnslt: count, ns/pt, error =',count,1e9*cpux/real(mx*my*mz)/count,maxval(abs(fout-fcmp))

!  Testing z-direction

  dxdy = 0.
  dzdy = dsdy
!$omp parallel private(iz)
  do iz=izs,ize
  do iy=1,my
  do ix=1,mx
    fin(ix,iy,iz) = cos(ix*k)+cos(iy*k)+cos(iz*k)
    fcmp(ix,iy,iz) = cos(ix*k+dxdy*ym(iy)*ky)+cos(iy*k)+cos(iz*k+dzdy*ym(iy)*kz)
  end do
  end do
  end do
!$omp end parallel
  cpux = fdtime()
  cpux = 0.
  count = 0
  do while (cpux .lt. 1.)
    call trnslt(fin,fout,dxdy,dzdy,.false.,my)
    cpux = cpux+fdtime()
    count = count+1
  end do
  print *,'trnslt: count, ns/pt, error =',count,1e9*cpux/real(mx*my*mz)/count,maxval(abs(fout-fcmp))

!  Testing xz-direction

  dxdy = dsdy
  dzdy = dsdy
!$omp parallel private(iz)
  do iz=izs,ize
  do iy=1,my
  do ix=1,mx
    fin(ix,iy,iz) = cos(ix*k)+cos(iy*k)+cos(iz*k)
    fcmp(ix,iy,iz) = cos(ix*k+dxdy*ym(iy)*ky)+cos(iy*k)+cos(iz*k+dzdy*ym(iy)*kz)
  end do
  end do
  end do
!$omp end parallel
  cpux = fdtime()
  cpux = 0.
  count = 0
  do while (cpux .lt. 1.)
    call trnslt(fin,fout,dxdy,dzdy,.false.,my)
    cpux = cpux+fdtime()
    count = count+1
  end do
  print *,'trnslt: count, ns/pt, error =',count,1e9*cpux/real(mx*my*mz)/count,maxval(abs(fout-fcmp))

  deallocate (fin,fout,fcmp)

END
