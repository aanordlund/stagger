!...............................................................................
subroutine gausi(k,a,b,ai,xmyi)

  ! Gauss-Legendre quadrature
  !   Lowan, Davids, Levenson,  Bull Amer Math Soc 48 p. 739  (1942)

  use params
  implicit none

  real, dimension(k) :: ai,xmyi
  real(kind=8), dimension(29) :: ap = (/ &
       1.0d0,0.55555555555555d0,.88888888888888d0,.347854845137d0, &
       0.65214515486254d0,0.23692688505618d0,0.47862867049936d0, &
       0.56888888888888d0,0.17132449237917d0,0.36076157304813d0, &
       0.46791393457269d0,0.12948496616887d0,0.27970539148927d0, &
       0.38183005050511d0,0.41795918367346d0,0.10122853629037d0, &
       0.22238103445337d0,0.31370664587788d0,0.36268378337836d0, &
       0.08127438836157d0,0.18064816069485d0,0.26061069640293d0, &
       0.31234707704000d0,0.33023935500126d0,0.06667134430868d0, &
       0.14945134915058d0,0.21908636251598d0,0.26926671930999d0, &
       0.29552422471475d0/)
  real(kind=8), dimension(29) :: xmyp = (/ &
       0.57735026918962d0,.77459666924148d0,.0d0,0.86113631159405d0, &
       0.33998104358485d0,.90617984593866d0,.53846931010568d0,.0d0, &
       0.93246951420315d0,.66120938646626d0,.23861918608319d0, &
       0.94910791234275d0,.74153118559939d0,.40584515137739d0,.0d0, &
       0.96028985649753d0,.79666647741362d0,.52553240991632d0, &
       0.18343464249565d0,.96816023950762d0,.83603110732663d0, &
       0.61337143270059d0,.32425342340380d0,.0d0,0.97390652851717d0, &
       0.86506336668898d0,.67940956829902d0,.43339539412924d0, &
       0.14887433898163d0/)

  integer, dimension(9) :: indov = (/1,3,5,8,11,15,19,24,29/)
  integer :: i,ip,k,k2,kud,ined,ioev
  real :: a,b,c,d,gausif,wi,fk,flk
  character(len=mid) :: Id = 'gausi'

  if (master) print '(X,A)', trim(Id)//': initialize'
  
  if (k.gt.9) then
     if (master) print '(X,A,X,I3)', trim(Id)//': order too high =', k
  end if

  if (k.eq.1) then
     xmyi(1)=(b+a)*0.5
     ai(1)=b-a
  else
     kud=0
     flk=float(k)/2.
     k2=k/2
     fk=float(k2)
     if(abs(flk-fk) .gt. 1.e-7) then
        k2=k2+1
        kud=1
     end if
     ioev=indov(k-1)
     ined=ioev-k2

     do i=1,k2
        ip=ined+i
        xmyi(i)=-xmyp(ip)*(b-a)*0.5+(b+a)*0.5
        ai(i)=(b-a)*0.5*ap(ip)
     end do

     k2=k2+1
     do i=k2,k
        ip=ioev+k2-i
        if (kud.gt.0) then 
           ip=ip-1
        end if
        xmyi(i)= xmyp(ip)*(b-a)*0.5+(b+a)*0.5
        ai(i)=(b-a)*0.5*ap(ip)
     end do
  end if
end subroutine gausi

!...............................................................................
function gausif(x,p)
  implicit none
  integer p
  real x, gausif
  gausif = (p+1)*x**p
end function gausif

!...............................................................................
subroutine gausi_test
  !
  !  As illustrated by this test subroutine, for even functions of x
  !  (such as Feautrier intensity as a function of mu), one obtains
  !  exact results up to 3rd order with normal 2-pnt Gaussian and exaxt
  !  results up to 6th order with half of the 4-pnt Gaussian points.
  !
  !  If we decide to use one point at mu=1, and let the position and 
  !  weight for a second point be free parameters, then one can match
  !  two expansion coeffs, so again obtain exact results of to 4th order
  !  if one assumes even functions.  The x(1) point is at 1/sqrt(5.),
  !  with a weight of 5/6, while the point x(2)=1. has a weight of 1/6.
  !
  !  For general functions, including linear, one can only make the
  !  formula accurate to 2nd order, with a point at x(1)=1/3 with a
  !  weight w(1)=3/4, and a point at x(2)=1 with w(2)=1/4.
  !
  !  One argument for NOT using the trick with assuming even order 
  !  functions is that the weights then do not allow an accurate 
  !  computation of the surface flux, which is an integral over a
  !  a non-even function.  Also, the even-order case tends to give
  !  less weight to the vertical rays.
  !
  implicit none
  integer, parameter:: m=100
  real, dimension(m):: x,y,w
  integer i,k,p
  real a,b,gausif,exact

  a=0.
  b=1.

  do k=1,4

     !  Normal Gaussian quadrature over [0,1]

     call gausi(k,a,b,w,x)
     print '(a,10f9.6)','x0=',x(1:k)
     print '(a,10f9.6)','w0=',w(1:k)
     do p=0,k*2
        do i=1,k
           y(i) = gausif(x(i),p)
        end do
        exact = 1.
        print '(2i4,f10.6)',p,k,sum(y(1:k)*w(1:k))/exact-1.
     end do

     !  For even number of points and even functions, use half of the interval [-1,1]

     call gausi(2*k,-b,b,w,x)
     x(1:k)=-x(1:k)
     print '(a,10f9.6)','x1=',x(1:k)
     print '(a,10f9.6)','w1=',w(1:k)
     do p=0,k*4,2
        do i=1,k
           y(i) = gausif(x(i),p)
        end do
        exact = 1.
        print '(2i4,f10.6)',p,k,sum(y(1:k)*w(1:k))/exact-1.
     end do

  end do

end subroutine gausi_test

!...............................................................................
