! $Id: radiation_lookup_cache.f90,v 1.8 2011/04/07 10:57:46 aake Exp $
!***********************************************************************
MODULE cooling
  USE params
  integer, parameter:: mmu=9, mphi=10
  real dphi, form_factor, dtaumin, dtaumax
  integer nmu, nphi, verbose, ny0
  integer n1, n2
  real y_newton, dy_newton, t_newton, ee_newton, ee_min, t_ee_min
  real, dimension(mmu):: mu0

  real, pointer, dimension(:,:,:):: rk,lnrk,s,q,dtau,qq
  real, allocatable, dimension(:,:,:):: ii,jj,hhx,hhy,hhz,lns

  logical do_table, do_newton, do_limb, do_fld, do_tsc
  character(len=mfile):: intfile
END MODULE

!***********************************************************************
SUBROUTINE init_cooling (r,e)
  USE params
  USE arrays, only: scr1, scr2, scr3, scr4, scr5, scr6
  USE cooling
  implicit none
  real, dimension(mx,my,mz):: r,e
  integer i
  character(len=mfile):: name

  do_cool = .true.
  i = index(file,'.')
  nmu = 0
  nphi = 4
  dphi = 0.
  form_factor = 1.
  dtaumax = 100.
  dtaumin = 0.1
  intfile = ' '

  do_newton = .false.
  y_newton = -0.3
  dy_newton = 0.05
  t_newton = 0.01
  t_ee_min = 0.005
  ee_newton = -1.
  ee_min = 3.6
  do_limb  = .false.
  verbose = 0
  ny0 = 0
  do_fld = .false.
  do_tsc = .false.

  call read_cooling
  allocate (lns(mx,my,mz))
  rk   => scr1
  lnrk => scr2
  s    => scr3
  q    => scr4
  dtau => scr5
  qq   => scr6
  call trnslt_test (do_tsc)
END SUBROUTINE

!***********************************************************************
SUBROUTINE read_cooling
  USE params
  USE eos
  USE cooling
  namelist /cool/do_cool, nmu, nphi, dphi, form_factor, dtaumin, dtaumax, intfile, &
    do_newton, y_newton, dy_newton, t_newton, ee_newton, ee_min, t_ee_min, do_limb, mu0, &
    ny0, verbose, do_fld, do_tsc
  character(len=mfile):: name, fname

  character(len=mid):: id='$Id: radiation_lookup_cache.f90,v 1.8 2011/04/07 10:57:46 aake Exp $'
  call print_id(id)

  rewind (stdin); read (stdin,cool)
  if (nmu==0 .and. form_factor==1.) form_factor=0.4
  if (master) write (*,cool)
  if (master) write (*,*) 'mbox, dbox =', mbox, dbox
  if (do_limb) then
    fname = name('limb.dat','lmb',file)
    call mpi_name(fname)
    open (limb_unit, file=trim(fname), &
      form='unformatted', status='unknown')
    write (limb_unit) mx, mz,nmu,nphi
  end if
END SUBROUTINE

!***********************************************************************
SUBROUTINE coolit (r, ee, lne, dd, dedt)
!
!  Radiative cooling.  When using the eos_lookup_cache.f90 version of the
!  lookup routines we would like to do a table lookup once for each bin,
!  and possibly once to compute a standard opacity and standard optical
!  depth (for accuracy).  So, the order of operations then must be:
!
!  for each bin
!    call lookup routine to compute lns and lnrk
!    for each angle
!      translate lnr and lnrk and exponentiate, to get source fn and opacity
!      solve the RT for that angle
!      translate solution back to Cartesian mesh
!    end for each angle
!  end for each bin
!
  USE params, rr=>scratch
  USE arrays, only: lnr=>wk08
  USE units
  USE eos
  USE cooling
  implicit none
  real, dimension(mx,my,mz):: r, ee, lne, dd, dedt
  real xmu(mmu), wmu(mmu)
  integer ix, iy, iz, lmu, lphi, imu, iphi, ibox, n1p, n2p, ny, nyr
  real sec, fdtime, dxdy, dzdy, tanth, qq1, f1, f2
  real phi, wphi, womega
  integer lrec
  logical flag, do_io
  logical, save:: first=.true.
  external transfer
  logical debug
  real cput(2), void, dtime, prof
  integer, save:: nrad=0, nang=0
  character(len=mfile):: name, fname
!
  if (.not. do_cool) return

  if (debug(dbg_cool) .and. omp_master) then; void = dtime(cput) &
    ; print '(1x,a8,2f7.3)','rad0   ',cput*omp_nthreads; endif

  if (lb > 1) then                                                      ! if we have ghost zones
    dedt(:,1:lb-1,izs:ize) = 0.                                         ! zap previous dedt there
  end if

!-----------------------------------------------------------------------
!  Angular quadrature
!  nmu > 0: Use Gauss integration (mu=1 excluded) with 2*nmu   order accuracy
!  nmu = 0: One vertical ray with weight 0.5
!  nmu < 0: Use Radau integration (mu=1 included) with 2*nmu-1 order accuracy
!-----------------------------------------------------------------------
  if      (nmu .gt. 0) then
    lmu = nmu
    call gausi (lmu,0.,1.,wmu,xmu)
  else if (nmu .eq. 0) then
    lmu = 1
    xmu(1) = 1.
    wmu(1) = 0.5
  else 
    lmu =-nmu
    call radaui(lmu,0.,1.,wmu,xmu)
  end if
  if (mu0(1) .ne. 0.) xmu=mu0

  if (first) then
    call barrier_omp ('rad-first')
    if (omp_master) first = .false.
    if (master) then
      print *,' imu       xmu    theta'
      !           1.....0.000.....00.0
      do imu=1,lmu 
        print '(i4,f10.3,f8.1,f10.3)',imu,xmu(imu),acos(xmu(imu))*180./pi,wmu(imu)
      end do
    end if
  end if

  call dumpn (lnr,'lnr','rad.dmp',0)
  call dumpn (ee,'ee','rad.dmp',1)

  do iz=izs,ize
    qq(:,:,iz) = 0.
  end do

  if (do_fld .and. do_dump) then
    !$omp master
    allocate (ii(mx,my,mz),jj(mx,my,mz))
    allocate (hhx(mx,my,mz),hhy(mx,my,mz),hhz(mx,my,mz))
    !$omp end master
    !$omp barrier
    ii(:,:,izs:ize) = 0.
    jj(:,:,izs:ize) = 0.
  end if

  lnr(:,:,izs:ize) = alog(r(:,:,izs:ize))                                       ! make sure we have consistent ln(rho)
                                              !call timer ('radiation', 'startup')
   
  do ibox=1,mbox
    call radiation_eos (lnr,ee,ibox,lns,lnrk)  
    !$omp barrier
                                              !call timer ('radiation', 'lookup')
    call dumpn (lnrk,'lnrk','rad.dmp',1)
    call dumpn (lns,'lns','rad.dmp',1)

    if (debug(dbg_cool) .and. omp_master) then; void = dtime(cput) &
      ; print '(1x,a8,2f7.3)','lookup ',cput*omp_nthreads; endif

    do imu=1,lmu
      if (nmu .eq. 0 .and. imu .eq. 1) then
        lphi = 1 
      else if (nmu .lt. 0 .and. imu .eq. lmu) then
        lphi = 1 
      else
        lphi = nphi
      end if
      tanth = tan(acos(xmu(imu)))
      wphi = 4.*pi/lphi

      do iphi=1,lphi
        phi = (iphi-1)*2.*pi/lphi + dphi*t
        dxdy = tanth*cos(phi)
        dzdy = tanth*sin(phi)

        if (ny0==0) then
          ny  = my
        else
          ny = ny0
        end if

!-----------------------------------------------------------------------
!  Opacity
!-----------------------------------------------------------------------
        call trnslt (lnrk, rk, dxdy, dzdy, do_log, ny, 1, .false., do_tsc)
                                              !call timer ('radiation', 'trnslt')
        call dumpn (rk,'rk','trf.dmp',1)

!-----------------------------------------------------------------------
!  Optical depth
!-----------------------------------------------------------------------
        !$omp barrier
        !$omp single
        n1 = 2                                                          ! all dtau < dtaumin for iy < n1
        n2 = 2                                                          ! all dtau > dtaumax for iy > n2
        !$omp end single

        n1p = my
        n2p = 2
        f1 = (0.5/xmu(imu))
        do iz=izs,ize
          dtau(:,1,iz) = dym(1)*rk(:,1,iz)/xmu(imu)
          do iy=2,my
          do ix=1,mx
            dtau(ix,iy,iz) = f1*dymdn(iy)*(rk(ix,iy-1,iz)+rk(ix,iy,iz))
            if (dtau(ix,iy,iz) > dtaumin) n1p = min(n1p,iy)
            if (dtau(ix,iy,iz) < dtaumax) n2p = max(n2p,iy)
          end do
          end do
        end do
                                              !call timer ('radiation', 'tau')
        if (ny0==0) then
          !$omp critical
          n1 = max(n1p,n1)
          n2 = max(n2p,n2)
          !$omp end critical
        else
	  !$omp master
          n1 = 2
          n2 = ny0
	  !$omp end master
        end if
        !$omp barrier

	!$omp master
        nrad = nrad + n2
        nang = nang + 1
	!$omp end master
        if (master .and. verbose==2 .and. isubstep==1) then
          nyr = (nrad+0.5)/nang
          print '(a,7i5,1p(2e15.7))',' imu,iphi,ibox,ny,n1,n2,nyr,dxdy,dzdy=', &
                                       imu,iphi,ibox,ny,n1,n2,nyr,dxdy,dzdy
        end if

!-----------------------------------------------------------------------
!  Raditative transfer equation
!-----------------------------------------------------------------------
        flag = do_limb .and. isubstep.eq.1 .and. ibox.eq.1                            ! and only for first substep and bin
        if (flag) then                                                                ! only check if necessary
          flag = do_io(t+dt,tscr,iscr+iscr0,nscr)                                     ! limb darkening every scratch
          flag = flag .or. (xmu(imu)==1. .and. do_io(t+dt,tsnap,isnap+isnap0,nsnap))  ! or else every nsnap and only mu=1
        end if

        call trnslt (lns, s, dxdy, dzdy, do_log, n2, 1, .false., do_tsc)              ! NOTE: do NOT move this furher up!
                                              !call timer ('radiation', 'trnslt')
        call transfer (n2,n1,dtau,s,q,rr,do_fld,surface_int,flag)
                                              !call timer ('radiation', 'transfer')
        if (flag) then
          call barrier_omp('limb')
          if (do_limb .and. omp_master) then
            write(limb_unit) t,imu,iphi,xmu(imu),phi,surface_int
            print *,omp_mythread,isubstep,ibox,imu,iphi
          end if
        end if
        call dumpn(dtau,'dtau','trf.dmp',1)
        call dumpn(s,'s','trf.dmp',1)
        call dumpn(q,'q0','trf.dmp',1)

        if (do_fld .and. do_dump) then
          ii(:,1:n2,izs:ize) = q(:,1:n2,izs:ize)+s(:,1:n2,izs:ize)
          if (any(ii(:,1:n2,izs:ize) < 0)) then
            print*,'WARNING: negative intensities'
          end if
          call trnslt (ii, s, -dxdy, -dzdy, .false., n2, 1, verbose, do_tsc)
          print'(4i4,2(2x,3i5),2(1pe12.3))',imu,iphi,ibox,n2 &
            ,minloc(ii(:,1:n2,izs:ize)),minloc(s(:,1:n2,izs:ize)) &
            ,minval(ii(:,1:n2,izs:ize)),minval(s(:,1:n2,izs:ize))
          jj(:,1:n2,izs:ize) = jj(:,1:n2,izs:ize) &
                             + s(:,1:n2,izs:ize)*(wmu(imu)*wphi*form_factor)
          call trnslt (rr, s, -dxdy, -dzdy, .false., n2, 1, verbose, do_tsc)
          hhx(:,1:n2,izs:ize) = hhx(:,1:n2,izs:ize) &
                              + s(:,1:n2,izs:ize)*(wmu(imu)*wphi*form_factor*sin(acos(xmu(imu)))*cos(phi))
          hhz(:,1:n2,izs:ize) = hhz(:,1:n2,izs:ize) &
                              + s(:,1:n2,izs:ize)*(wmu(imu)*wphi*form_factor*sin(acos(xmu(imu)))*sin(phi))
          hhy(:,1:n2,izs:ize) = hhy(:,1:n2,izs:ize) &
                              + s(:,1:n2,izs:ize)*(wmu(imu)*wphi*form_factor*xmu(imu))
        end if

        do iz=izs,ize
          do iy=2,n2-1
            f2 = xmu(imu)/(ym(iy+1)-ym(iy-1))
            q(:,iy,iz) = q(:,iy,iz)*(dtau(:,iy+1,iz)+dtau(:,iy,iz))*f2
          end do
          q(:,1,iz) = q(:,1,iz)*rk(:,1,iz)
        end do
        call dumpn(q,'q1','trf.dmp',1)
                                              !call timer ('radiation', 'sumup')

        if (flag .and. intfile.ne.' ') then
          !$omp barrier
          !$omp master
          fname = name(intfile,'int',file)
          call mpi_name (fname)
          if (master) print '(a,i5,4x,a,/,5g12.3)','radiation intensity:',1+isnap,intfile,surface_int(1:5,1)
          open (surface_unit,file=fname,form='unformatted',status='unknown',access='direct',recl=lrec(mx*mz))
          write (surface_unit,rec=2+isnap) surface_int
          close (surface_unit)
          if (do_scp) call scp(fname)
          !$omp end master
        end if

!-----------------------------------------------------------------------
!  Energy equation
!-----------------------------------------------------------------------
        call trnslt (q, s, -dxdy, -dzdy, .false., n2, 1, .false., do_tsc)
                                              !call timer ('radiation', 'trnslt')
        call dumpn(q,'q2','trf.dmp',1)

        f1 = wmu(imu)*wphi*form_factor
        do iz=izs,ize
        do iy=1,n2
        do ix=1,mx
          qq1 = f1*s(ix,iy,iz)
          qq(ix,iy,iz) = qq(ix,iy,iz) + qq1
          dedt(ix,iy,iz) = dedt(ix,iy,iz) + qq1
        end do
        end do
        end do
                                              !call timer ('radiation', 'qq')
      end do      ! iphi
    end do        ! imu
  end do          ! ibox
  call dumpn(qq,'qq','qq.dmp',1)
!!$omp end parallel

  call radiative_flux (qq,q)

  if (do_newton) then
    if (ee_newton > 0) eetop = ee_newton
    do iz=izs,ize
    do iy=1,my
      prof = exp(min((y_newton-ym(iy))/dy_newton,70.))
      prof = prof/(1.+prof)
      do ix=1,mx
        dedt(ix,iy,iz) = dedt(ix,iy,iz) - r(ix,iy,iz)*(ee(ix,iy,iz)-eetop)/t_newton*prof &
                                        - r(ix,iy,iz)*min(0.,ee(ix,iy,iz)-ee_min)/t_ee_min
      end do
    end do
    end do
  end if
                                              !call timer ('radiation', 'dedt')

  if (do_fld .and. do_dump) then
    call dumpn(jj,'jj','fld.dmp',1)
    call dumpn(hhx,'hx','fld.dmp',1)
    call dumpn(hhy,'hy','fld.dmp',1)
    call dumpn(hhz,'hz','fld.dmp',1)
    !$omp barrier
    !$omp master
    deallocate (ii,jj)
    deallocate (hhx,hhy,hhz)
    !$omp end master
  end if

  if (debug(dbg_cool) .and. omp_master) then; void = dtime(cput) &
    ; print '(1x,a8,2f7.3)','transfr',cput*omp_nthreads; endif

END SUBROUTINE
