! $Id: radiation_lookup_reshape.f90,v 1.13 2014/09/13 12:17:32 aake Exp $
!***********************************************************************
! To minimize the number of depth points used in the RT:
!
! For the initial, smallest inclination, start by using all points       ny = my
! For ibin=1, start with the estimate for the current inclination mu     ny = ny_omega
! For all ibin, reduce ny based on dtau                                  ny = min(ny,n2)
! For ibin=1, remember the largest number that was neeeded for any phi   ny_phi = max(ny_phi, ny)
!   [ use a global max over MPI, in absolute jy coordinates ]
! Record the largest number of depths needed, for any phi, and ibin=1    ny_omega = max(ny_omega, ny_phi)
! For the next inclination, start with the value for previous one        ny_rad = ny_omega
! 
! ny_omega = my+iyoff                                   ! global value, initially
! do imu=lmu,1,-1
!   ny_phi = 1                                          ! to record max
!   do iphi = 1,nphi
!     ny = ny_omega-iyoff                               ! local MPI-domain value
!     call lookup (ny, ...)                             ! decreasing with inclination
!     do ibin=1,nbin
!       call dtau_calc (ny, n2, ...)                    ! smallest iy with dtau > dtaumax
!       ny = min(ny,n2)
!       call transfer (ny,...)                          ! only where dtau < dtaumax
!       accumulate Q for iy=1:ny-1 only                 ! no contrib at ny
!       if (ibin==1) ny_phi=max(ny_phi,ny+iyoff)        ! largest ny needed for ibin=1 and any phi
!     end do ! ibin
!     accumulate Q over omega for iy=1:ny_phi-1         ! where there are contributions
!   end do ! iphi
!   call max_int_mpi (ny_phi)                           ! max over MPI-domains
!   ny_omega = ny_phi                                   ! default for next larger inclination
! end do ! imu
!
!***********************************************************************
MODULE cooling

  USE params
  USE reshape_mod, only: sub_nx, sub_nz, reshape_mode

  integer, parameter:: mmu=9, mphi=10
  real    dphi, form_factor, dtaumin, dtaumax
  integer nmu, nphi, verbose, ny0
  integer mblocks
  integer n1, n2
  integer,save:: truncate=0

  real y_newton, dy_newton, t_newton, ee_newton, ee_min, t_ee_min

  real, dimension(mmu):: mu0

  real, pointer, dimension(:,:,:) :: rk, lnrk, s, q, dtau, qq
  real, pointer, dimension(:,:,:) :: tau

  ! lnrkap: used to store untilted ln absorption coefficient (lnrk)
  real, pointer, dimension(:,:,:) :: lnrkap

  real, allocatable, target, dimension(:,:,:) :: scr7, scr8

  logical do_table, do_newton, do_limb, do_fld
  character(len=mfile):: intfile

END MODULE cooling


!***********************************************************************
SUBROUTINE init_cooling (r,e)

  USE params
  USE arrays
  USE cooling

  implicit none

  real, dimension(mx,my,mz):: r,e

  integer i
  character(len=mfile):: name

  !if (mpi_ny > 1) then
  !  if (master) print*,'ERROR: this RT version needs mpi_ny=1'
  !  call abort_mpi
  !end if

  do_cool     = .true.
  i           = index(file,'.')
  nmu         = 0
  nphi        = 4
  dphi        = 0.
  form_factor = 1.
  dtaumax     = 100.
  dtaumin     = 0.1
  intfile     = name('intensity.dat','int',file)

  do_newton   = .false.
  y_newton    = -0.3
  dy_newton   = 0.05
  t_newton    = 0.01
  t_ee_min    = 0.005
  ee_newton   = -1.
  ee_min      = 3.6
  do_limb     = .false.
  verbose     = 0
  ny0         = 0
  sub_nx      = 0
  sub_nz      = 0
  reshape_mode = 'ymerg_xzsplit'

  ! mblocks = number of subdomains communicated per side in old trnslt routine (deprecated!)
  mblocks     = 1
  do_fld      = .false.

  call read_cooling

  rk   => scr1
  lnrk => scr2
  s    => scr3
  q    => scr4
  dtau => scr5
  qq   => scr6

  allocate( scr7(mx,my,mz) )
  allocate( scr8(mx,my,mz) )
  tau    => scr7
  lnrkap => scr8

END SUBROUTINE init_cooling


!***********************************************************************
SUBROUTINE read_cooling

  USE params
  USE eos
  USE cooling

  namelist /cool/do_cool, nmu, nphi, dphi, form_factor, dtaumin, dtaumax, intfile, &
       do_newton, y_newton, dy_newton, t_newton, ee_newton, ee_min, t_ee_min, do_limb, mu0, &
       ny0, verbose, mblocks, do_fld, sub_nx, sub_nz, reshape_mode, timer_verbose, truncate

  character(len=mfile):: name, fname
  character(len=mid):: id='$Id: radiation_lookup_reshape.f90,v 1.13 2014/09/13 12:17:32 aake Exp $'

  call print_id(id)

  rewind (stdin); read (stdin,cool)

  if (nmu==0 .and. form_factor==1.) form_factor=0.4

  if (master) write (*,cool)

  if (master) write (*,*) 'mbox, dbox =', mbox, dbox

  if (do_limb) then
     fname = name('limb.dat','lmb',file)
     call mpi_name(fname)
     open (limb_unit, file=trim(fname), &
          form='unformatted', status='unknown')
     write (limb_unit) mx, mz,nmu,nphi
  end if

END SUBROUTINE read_cooling


!***********************************************************************
SUBROUTINE coolit (r, ee, lne, dd, dedt)

  USE params
  USE arrays, only: lns
  USE units
  USE eos
  USE cooling

  implicit none

  character(len=mid)  :: id='$Id: radiation_lookup_reshape.f90,v 1.13 2014/09/13 12:17:32 aake Exp $'

  real, dimension(mx,my,mz) :: r, ee, lne, dd, dedt
  real, dimension(mx,mz)    :: surfi
 
  ! ym0, ymN1: values of ym depth in the layers immediately above and below local MPI subdomain
  ! dtau0, dtauN1: values of dtau immediately above and below local subdomain
  real                      :: ym0,   ymN1          
  real, dimension(mx,1,mz)  :: dtau0, dtauN1

  ! tauIntr: intrinsic optical depth scale
  real, dimension(mx,my,mz) :: tauIntr

  real    xmu(mmu), wmu(mmu)
  real    sec, fdtime, dxdy, dzdy, tanth, qqTmp, f1, f2
  real    phi, wphi, womega

  integer ix, iy, iz, lmu, lphi, imu, iphi, ibox, n1p, n2p, ny
  integer lrec, imaxval_mpi, nyr
  integer ny_omega, ny_phi

  integer  mTauTop, mTauBot
  integer  nDTauTop, nDTauBot

  logical, save :: first=.true.
  logical       :: flag_scr, flag_snap, flag_limb, flag_surf, do_io, do_surfi, io_flag
  logical       :: debug
  real          :: cput(2), void, dtime, prof
  integer, save :: nrad=0, nang=0
  character(len=mfile) :: name, fname
  real, allocatable, dimension(:,:,:):: rh_ray, ee_ray, lnr
  external transfer
!.......................................................................

  ! print subroutine's ID
  call print_id(id)

  ! skip ratiative transfer ?
  if (.not. do_cool) return
                                                                                call timer('radiation','init')

  ! reset dedt in ghost zones, if present (lb > 1)
  if (lb > 1) then
     dedt(:,1:lb-1,izs:ize) = 0.
  end if

  !$omp barrier
  !$omp master
  allocate (rh_ray(mx,my,mz), ee_ray(mx,my,mz), lnr(mx,my,mz))
  !$omp end master
  !$omp barrier

  do iz=izs,ize
    lnr(:,:,iz)=alog(r(:,:,iz))
  end do

!-----------------------------------------------------------------------
!  Angular quadrature
!  nmu > 0: Use Gauss integration (mu=1 excluded) with 2*nmu   order accuracy
!  nmu = 0: One vertical ray with weight 0.5
!  nmu < 0: Use Radau integration (mu=1 included) with 2*nmu-1 order accuracy
!-----------------------------------------------------------------------

  if      (nmu .gt. 0) then
     lmu = nmu
     call gausi (lmu,0.,1.,wmu,xmu)
  else if (nmu .eq. 0) then
     lmu = 1
     xmu(1) = 1.
     wmu(1) = 0.5
  else 
     lmu =-nmu
     call radaui(lmu,0.,1.,wmu,xmu)
  end if
  if (mu0(1) .ne. 0.) xmu=mu0

  if (first) then
     call barrier_omp ('rad-first')
     if (omp_master) first = .false.
     if (master) then
        print *,' imu       xmu    theta'
        !           1.....0.000.....00.0
        do imu=1,lmu 
           print '(i4,f10.3,f8.1,f10.3)',imu,xmu(imu),acos(xmu(imu))*180./pi,wmu(imu)
        end do
     end if
  end if

  ! loop over angles
  ny_omega = my
  do imu=lmu,1,-1
                                                                                call timer('radiation','general')
     if (nmu .eq. 0 .and. imu .eq. 1) then
        lphi = 1 
     else if (nmu .lt. 0 .and. imu .eq. lmu) then
        lphi = 1 
     else
        lphi = nphi
     end if

     tanth = tan(acos(xmu(imu)))
     wphi  = 4.*pi/lphi

     ny_phi = 1
     do iphi=1,lphi
        ny = ny_omega

        phi = (iphi-1)*2.*pi/lphi + dphi*t
        dxdy = tanth*cos(phi)
        dzdy = tanth*sin(phi)

        if (master .and. verbose==1 .and. isubstep==1) then
           print '(a,3i5,2e15.7)',' imu,iphi,ny,dxdy,dzdy=', imu,iphi,ny,dxdy,dzdy
        end if

        call trnslt (lnr, rh_ray, dxdy, dzdy, .true. , ny, mblocks, verbose)
        call trnslt (ee , ee_ray, dxdy, dzdy, .false., ny, mblocks, verbose)
        call dumpn (rh_ray,'rh_ray','trf.dmp',1)
        call dumpn (ee_ray,'ee_ray','trf.dmp',1)
                                                                                call timer('radiation','trnslt(lnr)')
        call lookup (rh_ray, ee_ray, lnrk, lns, ny)
                                                                                call timer('radiation','lookup')
        do iz=izs,ize
          qq(:,:,iz) = 0.
        end do
                                                                                
        do ibox=1,mbox
           do iz=izs,ize
              s(:,1:ny,iz) = exp(lns(:,1:ny,iz,ibox))
           end do
           call dumpn (s,'s','trf.dmp',1)

           if (ibox.eq.1) then
              do iz=izs,ize
                 rk(:,1:ny,iz) = exp(lnrk(:,1:ny,iz))
              end do
              call dumpn (rk,'rk','trf.dmp',1)
              
              ! flags for data output 
              flag_scr  = do_io(t+dt, tscr, iscr+iscr0,   nscr)
              flag_snap = do_io(t+dt, tsnap,isnap+isnap0, nsnap)
              io_flag   = flag_scr .or. flag_snap

              ! flag_limb: limb darkening every scratch 
              ! or else every nsnap and only mu=1
              ! and only for first substep and bin:
              flag_limb = do_limb   .and. flag_scr
              flag_limb = flag_limb .or.  (xmu(imu)==1. .and. flag_snap)
              flag_limb = flag_limb .and. isubstep.eq.1 .and. ibox.eq.1

              ! compute surface intensity ?
              flag_surf = flag_scr .or. flag_snap
              flag_surf = flag_surf .and. xmu(imu)==1. .and. isubstep.eq.1 

              do_surfi  = flag_surf.or.flag_limb

              ! compute optical depth increments and intrinsic optical depth tauIntr
              ! tau_calc: rk = exp(lnrk)
              n2 = ny
              call tau_calc( dym, n2, xmu(imu), tauIntr, nDTauTop, nDTauBot, .false. )
              ny = n2
           else
              f1 = 10.**dbox
              n2 = ny
              do iz=izs,ize
                 do iy=1,ny
                   dtau(:,iy,iz) = f1 * dtau(:,iy,iz)
                   if (minval(dtau(:,iy,iz)) < dtaumax) n2 = iy
                 end do
              end do
              ny = n2+1
           end if
           call dumpn (dtau,'dtau','trf.dmp',1)
                                                                                call timer('radiation','dtau')
           call dumpn (s,'s','trf.dmp',1)
           call transfer_reshape (dtau, s, q, surfi, do_surfi, xmu(imu), dtaumin, dtaumax, ny)
           call dumpn (q,'q0','trf.dmp',1)

                                                                                call timer('radiation','transfer')
           if ( flag_limb ) then
              call barrier_omp('limb')
              if (do_limb .and. omp_master) then
                 write(limb_unit) t,imu,iphi,xmu(imu),phi,surfi
                 print *,omp_mythread,isubstep,ibox,imu,iphi
              end if
           end if

           ! energy equation; q has already been converted to per unit volume and time
           f1 = wmu(imu)*wphi*form_factor
           do iz=izs,ize
              do iy=1,ny-1
                 qq(:,iy,iz)   =   qq(:,iy,iz) + f1 * q(:,iy,iz)
              end do
           end do
           call dumpn (qq,'qq','trf.dmp',1)

           ! integrated surface intensity
           if (flag_surf) then
              if (ibox .eq. 1) then      ! overwrite surface_int
                 do iz=izs,ize
                    surface_int(:,iz) = surfi(:,iz)
                 end do
              else                       ! add to surface_int
                 do iz=izs,ize
                    surface_int(:,iz) = surface_int(:,iz)+surfi(:,iz)
                 end do
              end if
           end if
                                                                                call timer('radiation','sum(q)')
           if (ibox == 1) ny_phi = max(ny,ny_phi)  ! largest needed for any phi and ibox==1
        end do ! ibox=1,nbox

        call trnslt (qq, q, -dxdy, -dzdy, .false., ny_phi, mblocks, verbose)
        call dumpn (q,'q','trf.dmp',1)
        do iz=izs,ize
           do iy=1,ny_phi-1
             dedt(:,iy,iz) = dedt(:,iy,iz) + q(:,iy,iz)
           end do
        end do
        call dumpn (dedt,'dedt','trf.dmp',1)
                                                                                call timer('radiation','trnslt(q)')
     end do ! iphi=1,lphi
     ny_phi = ny_phi + iyoff    ! global value
     call max_int_mpi(ny_phi)
     ny_phi = ny_phi - iyoff    ! local value
     ny_omega = ny_phi          ! initial estimate for next larger inclination
  end do ! imu=1,lmu

  !call radiative_flux (qq,q)

  if (do_newton) then
     if (ee_newton > 0) eetop = ee_newton
     do iz=izs,ize
        do iy=1,my
           prof = exp(min((y_newton-ym(iy))/dy_newton,70.))
           prof = prof/(1.+prof)
           do ix=1,mx
              dedt(ix,iy,iz) = dedt(ix,iy,iz) - r(ix,iy,iz)*(ee(ix,iy,iz)-eetop)/t_newton*prof &
                                              - r(ix,iy,iz)*min(0.,ee(ix,iy,iz)-ee_min)/t_ee_min
           end do
        end do
     end do
  end if

  !$omp barrier
  !$omp master
  deallocate (rh_ray, ee_ray, lnr)
  !$omp end master
                                                                                call timer('radiation','dedt')
END SUBROUTINE coolit
