! $Id: radiation_simple.f90,v 1.29 2009/02/03 22:28:29 aake Exp $
!***********************************************************************
MODULE cooling

  USE params
  integer, parameter:: mmu=9, mphi=10
  real xmu(mmu), wmu(mmu), phi, wphi, womega, dphi, form_factor
  integer nmu, nphi

  logical do_table
  character(len=mfile):: intfile
END MODULE

!***********************************************************************
SUBROUTINE init_cooling (r,e)
  USE params
  USE cooling
  USE eos
  real, dimension(mx,my,mz):: r,e
  integer i
  character(len=mfile):: name
  character(len=mid):: id='$Id: radiation_simple.f90,v 1.29 2009/02/03 22:28:29 aake Exp $'
  if (id.ne.' ') print *,id ; id = ' '

  do_cool = .true.
  i = index(file,'.')
  nmu = 0
  nphi = 4
  dphi = 0.
  form_factor = 1.
  intfile = name('intensity.dat','int',file)

  call read_cooling
END SUBROUTINE

!***********************************************************************
SUBROUTINE read_cooling
  USE params
  USE cooling
  USE eos
  namelist /cool/do_cool, nmu, nphi, dphi, form_factor, intfile

  if (stdin.ge.0) then
    rewind (stdin); read (stdin,cool)
  else
    read (*,cool)
  end if
  write (*,cool)
END SUBROUTINE

!***********************************************************************
SUBROUTINE coolit (r,ee,lne,dd,dedt)
!
!  Radiative cooling
!
  USE params
  USE units
  USE eos
  USE cooling
  implicit none
  real, dimension(mx,my,mz), intent(in):: r,ee,lne,dd
  real, dimension(mx,my,mz), intent(out):: dedt
  real, dimension(mx,my,mz):: rkap,s,lnrk,lns,rk,qq
  real, dimension(:,:,:), allocatable:: tt,ne,dtau,q
  real, dimension(mx,mz):: xi
  integer iy, iz, lmu, lphi, imu, iphi
  real sec, elapsed, dxdy, dzdy, tanth, f
  integer lrec
  logical flag
  logical, save:: first=.true.
!
  if (.not. do_cool) return

!-----------------------------------------------------------------------
!  Angular quadrature
!  nmu > 0: Use Gauss integration (mu=1 excluded) with 2*nmu   order accuracy
!  nmu = 0: One vertical ray with weight 0.5
!  nmu < 0: Use Radau integration (mu=1 included) with 2*nmu-1 order accuracy
!-----------------------------------------------------------------------
  if      (nmu .gt. 0) then
    lmu = nmu
    call gausi (lmu,0.,1.,wmu,xmu)
  else if (nmu .eq. 0) then
    lmu = 1
    xmu(1) = 1.
    wmu(1) = 0.5
  else
    lmu =-nmu
    call radaui(lmu,0.,1.,wmu,xmu)
  end if

  if (first) then
    first = .false.
    print *,' imu       xmu    theta'
    !           1.....0.000.....00.0
    do imu=1,lmu 
      print '(i4,f10.3,f8.1)',imu,xmu(imu),acos(xmu(imu))*180./pi
    end do
  end if

!-----------------------------------------------------------------------
!  Equation of state and opacity
!-----------------------------------------------------------------------
  allocate (tt(mx,my,mz), ne(mx,my,mz))
  call equation_of_state (r, ee, tt, .false., tt, .true., ne, .true.)
  call stats('tt',tt)
  call stats('ne',ne)
  call opacity(r,tt,ne,rkap)
  call stats('rkap',rkap)
  call dump (rkap,'rad.dat',1)
  deallocate (ne)
  call dump (tt,'rad.dat',5)

  f = stefan/(pi*u_e*u_l/u_t)
!$omp parallel do private(iz)
  do iz=1,mz
    s(:,:,iz) = f*tt(:,:,iz)**4
    lns (:,:,iz) = alog(s   (:,:,iz))
    lnrk(:,:,iz) = alog(rkap(:,:,iz))
    qq(:,:,iz) = 0.
  end do
  call dump(s,'rad.dat',7)
  deallocate (tt)
  allocate (dtau(mx,my,mz), q(mx,my,mz))

  do imu=1,lmu
    if (nmu .eq. 0 .and. imu .eq. 1) then
      lphi = 1 
    else if (nmu .eq. -1) then
      lphi = 1
    else
      lphi = nphi
    end if
    tanth = tan(acos(xmu(imu)))
    wphi = 4.*pi/lphi

    do iphi=1,lphi
      phi = (iphi-1)*2.*pi/lphi + dphi*t
      dxdy = tanth*cos(phi)
      dzdy = tanth*sin(phi)
      if (do_trace) print *,imu,iphi,dxdy,dzdy

      call trnslt (lns , s , dxdy, dzdy, .true., my, 1, .false.)
      call trnslt (lnrk, rk, dxdy, dzdy, .true., my, 1, .false.)
      if (do_trace) print *,'trnslt OK'
      call stats('s',s)
      call stats('rk',rk)
      if (do_trace) print *,'exp OK'

!-----------------------------------------------------------------------
!  Optical depth
!-----------------------------------------------------------------------
      if (do_trace) sec=elapsed()
      f = (0.5/xmu(imu))
!$omp parallel do private(iz,iy)
      do iz=1,mz
        dtau(:,1,iz) = dym(1)*rk(:,1,iz)/xmu(imu)
        do iy=2,my
          dtau(:,iy,iz) = f*dymdn(iy)*(rk(:,iy-1,iz)+rk(:,iy,iz))
        end do
      end do
      if (do_trace) then
        sec = elapsed()
        print *,'dtau: sec, ns/pt',sec,sec*1e9/(mx*my*mz)
      end if
      call stats('dtau',dtau)
      call dump (dtau,'rad.dat',2)

!-----------------------------------------------------------------------
!  Raditative transfer equation
!-----------------------------------------------------------------------
      call dump (s,'rad.dat',3)
      flag = mod(it,nsnap).eq.0 .and. xmu(imu).eq.1.
      if (do_trace) print *,'transfer:',flag

      call transfer (my,20,dtau,s,q,xi,flag)

      if (flag .and. intfile.ne.' ') then
        open (2,file=intfile,form='unformatted',status='unknown',access='direct',recl=lrec(mx*mz))
        write (2,rec=1+isnap) xi
        close (2)
      end if

      call trnslt (q, s, -dxdy, -dzdy, .false., my, 1, .false.)

!-----------------------------------------------------------------------
!  Energy equation
!-----------------------------------------------------------------------
      f =  wmu(imu)*wphi*form_factor
!$omp parallel do private(iz)
      do iz=1,mz
        qq(:,:,iz) = qq(:,:,iz) + f*rkap(:,:,iz)*s(:,:,iz)
        dedt(:,:,iz) = dedt(:,:,iz) + f*rkap(:,:,iz)*s(:,:,iz)
      end do
    end do
  end do
  call dump (qq,'rad.dat',4)
  deallocate (dtau, q)

  call dump (dedt,'rad.dat',6)
END SUBROUTINE
