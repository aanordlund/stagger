! $Id: dtau_rays.f90,v 1.12 2014/08/22 12:12:18 aake Exp $
!=======================================================================
 SUBROUTINE dtau_rays (nray, mtau, rhokap, mu, dtau, n1, n2, ibox)
!
! Compute optical depth and the largest indices n1 and n2 where the 
! smallest optical depth is less than dtaumin and dtaumax
!
!-----------------------------------------------------------------------
  USE params , only: ymg, ym, master
  USE cooling, only: dtaumin, dtaumax
  USE eos    , only: dbox
  implicit none
  integer:: nray, mtau, n1, n2, ibox
  real, dimension(nray,mtau):: rhokap, dtau
  real:: mu, dtau1, ds, c
  integer:: itau, iray
!.......................................................................
  if (ibox == 1) then 
    ds = (ymg(2)-ymg(1))/mu                                             ! top dy
    do iray=1,nray
      dtau(iray,1) = ds*rhokap(iray,1)                                  ! estimate
    end do
    do itau=2,mtau                                                      ! OMP over tau
      ds = 0.5*(ymg(itau) - ymg(itau-1))/mu                             ! global y-scale
      do iray=1,nray                                                    ! vectorize of rays
        dtau(iray,itau) = ds*(rhokap(iray,itau)+rhokap(iray,itau-1))    ! delta-tau
      end do
    end do
    n1 = mtau
    n2 = mtau                                                           ! shared
  else
    c = 10.**dbox                                                       ! scaling factor
    do itau=1,mtau
      do iray=1,nray
        dtau(iray,itau) = dtau(iray,itau)*c                             ! bin scaling
      end do
    end do
  end if
  do itau=n2,1,-1                                                       ! update depth indices
    if (minval(dtau(:,itau)) < dtaumax) exit                            ! is itau below n2 limit?
  end do                                                                ! might exit with idtau=n2
  n2 = max(itau+1,5)                                                    ! n2 is first index above limit
  n2 = min(n2,mtau)                                                     ! make sure n2 is within limits
  n1 = min(n1,n2-1)                                                     ! no need to look above
  do itau=n1,1,-1
    if (minval(dtau(:,itau)) < dtaumin) exit                            ! is itau below n1 limit?
  end do
  n1 = max(itau,2)                                                      ! n1 is first index below limit
END SUBROUTINE
