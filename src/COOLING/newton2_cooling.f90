! $Id: newton2_cooling.f90,v 1.6 2012/12/31 16:19:11 aake Exp $
!***********************************************************************
MODULE cooling
  real t_cool,ee_cool,r_cool
END MODULE

!***********************************************************************
SUBROUTINE init_cooling (r,e)
  USE params
  USE cooling
  real, dimension(mx,my,mz):: r,e

  do_cool = .true.
  t_cool = 10.
  ee_cool = 0.
  r_cool = 1.
  call read_cooling
END SUBROUTINE

!***********************************************************************
SUBROUTINE read_cooling
  USE params
  USE cooling
  namelist /cool/do_cool,t_cool,ee_cool,r_cool

  if (stdin.ge.0) then
    rewind (stdin); read (stdin,cool)
  else
    read (*,cool)
  end if
  write (*,cool)
END SUBROUTINE

!***********************************************************************
SUBROUTINE coolit (r,ee,lne,dd,dedt)
!
!  Newtonian cooling at densities above r_cool.  Vanishing cooling at
!  lower densities, to allow a hot "corona" to surround a disk, for
!  example (as a device to avoid very low densities above the disk).
!
  USE params
  USE cooling
  implicit none
  real, dimension(mx,my,mz) :: r, ee, lne, dd, dedt
!hpf$ distribute(*,*,block):: r, ee, lne, dd, dedt
  real c_cool,r2,rc1
  integer ix,iy,iz
  character(len=mid):: id='$Id: newton2_cooling.f90,v 1.6 2012/12/31 16:19:11 aake Exp $'

  if (.not. do_cool) return
  if (id.ne.' ') print *,id; id=' '

  c_cool = 1./t_cool
  rc1 = 1./r_cool
!$omp parallel do private(ix,iy,iz,r2)
  do iz=1,mz
  do iy=1,my
  do ix=1,mx
    r2 = (rc1*r(ix,iy,iz))**2
    dedt(ix,iy,iz) = dedt(ix,iy,iz) - r2/(1+r2)*r(ix,iy,iz)*(ee(ix,iy,iz)-ee_cool)*c_cool
  end do
  end do
  end do

  return
END SUBROUTINE
