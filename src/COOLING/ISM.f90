! $Id: ISM.f90,v 1.25 2004/07/13 08:39:14 aake Exp $
!***********************************************************************
MODULE cooling

! Note: if initial conditions are set such that [Z] could be less than
! 1.E-04, the lowest metallicity used in MODULE cooling must be set 
! to 1.E-04

! The units of lamda are energy per unit volume and time, or cdu*cvu^2/ctu,
! which the computed, dimensional value should be divided by.  But since
! a multiplicative factor of cdu^2 should also be applied, the computed
! value should be divided by cvu^2/(ctu*cdu), or multiplied by ctu*cdu/cvu^2.

  real*8 , parameter :: cdu=1.d-24, clu=3.0857d18, ctu=3.1557d13
  real   , parameter :: cvu=clu/ctu, cmu=((cdu*clu)*clu)*clu, msun=1.989e33  
  real*8 , parameter :: ceu=((((cdu*cvu**2)*clu)*clu)*clu)
  real   , parameter :: lncV=-3.8286        ! mu=0.60 is used; [cV]=ceu cmu^-1 K^-1
  real   , parameter :: lnfehsun=6.6280     ! (m_H/m_Fe)*10^(H-Fe_sun)*X_p^-1
  real   , parameter :: zlim=1.324E-12      ! sets [Fe/H]>-9, depends on lnfehsun
  real   , parameter :: logclambu=-20.4814  ! [L]=ctu*cdu/cvu**2
  real   , parameter :: noH=0.451           ! cdu -> # H atoms cm-3
  real   , parameter :: ln10=2.3025851      ! to speed up 10.** 
  real   , parameter :: invln10=0.4342945   ! to speed up log10 (?) 
  real   , parameter :: log2=0.30103
  real   , allocatable, dimension(:) :: TN, DN, RH, ROP
  real   , allocatable, dimension(:,:,:,:) :: WG
  logical hhrr_search
  character (len=40) coolH2file
END MODULE

!***********************************************************************
SUBROUTINE init_cooling (r,ee)
  USE params
  USE cooling
  real, dimension(mx,my,mz):: r,ee
  namelist /cool/do_cool,coolH2file
  coolH2file = '../input/COOLING/le_cube.dat' ! file name of H_2 cooling grid 
  call read_cooling
  if (do_cool) call initWH2 (coolH2file)      ! load H_2 cooling grid
END SUBROUTINE

!***********************************************************************
SUBROUTINE read_cooling
  USE params
  USE cooling
  namelist /cool/do_cool,coolH2file
  if (stdin.ge.0) then
    rewind (stdin); read (stdin,cool)
  else
    read (*,cool)
  end if
  write (*,cool)
END SUBROUTINE


!-------------------------------------------------------------------------
SUBROUTINE heating (ccnm,z,nH,lognH,nHlt1,logfH2,op,cmb)

USE params
USE cooling
implicit none

real :: ccnm
real :: z, nH, lognH, nHlt1, logfH2, op, cmb, y0, y1, ycmb
  
  call coolH2(2.1e0,lognH,logfH2,op,y1)    ! H_2 cooling at ln(T) = 4.835 (126 K)
  call coolH2(2.0e0,lognH,logfH2,op,y0)    ! H_2 cooling at ln(T) = 4.605 (100 K)
  ycmb= 2*y0-y1+nHlt1-logfH2               ! extrapolated H_2 cooling at T ~ 80 K
  cmb = nH*exp(ln10*ycmb) &                ! H_2 cooling
      + nH**2*exp(ln10*(ccnm+z))           ! metal cooling

END SUBROUTINE 


!----------------------------------------------------------------
subroutine initWH2 (filename)
!  H2 global cooling
!    W = W(T, nH, H/H2, O/P)
!
!  npt : Nb of points in Temperature grid (TN)
!  npd : Nb of points in Density grid (DN)
!  nph : Nb of points in H/H2 ratio grid (RH)
!  npr : Nb of points in Ortho/Para ratio grid (ROP)

USE params
USE cooling
  implicit none

  integer, parameter :: npt=23, npd=49, nph=36, npr=6
  integer :: itt, idd, ihh, ioo
  real    :: TNmin, TNmax, DNmin, DNmax
  character (len=40) filename

  allocate (TN(npt), &
            DN(npd), &
            RH(nph), & 
            ROP(npr))
  allocate (WG(npt,npd,nph,npr))

  data TNmin, TNmax / 100.0e0, 1.0e+04 /
  data DNmin, DNmax / 1.0e0, 1.0e+08 /

!  Read Cooling file (binary)

  open (10, file=filename, status="old")

  read(10,*) RH
  read(10,*) ROP
  read(10,*) TN
  read(10,*) DN
  read(10,*) ((((WG(itt,idd,ihh,ioo), idd=1,npd), itt=1,npt), ioo=1,npr), ihh=1,nph)

  close (10)

  return
end subroutine



!---------------------------------------------------------------------------
subroutine coolH2(tt, dd, hh, rr, WH2)
! - Compute interpolated value in 4D-grid

USE params
USE cooling
  implicit none

  integer, parameter :: npt=23, npd=49, nph=36, npr=6  
  integer ::  i, itt, idd, ihh, irr
  real    :: tt, dd, hh, rr, WH2, un, dix, tp, up, vp, wp 
  real    :: y0000, y1000, y0100, y1100, y0010, y1010, y0110, y1110, &
             y0001, y1001, y0101, y1101, y0011, y1011, y0111, y1111

  itt = 11        ! initial guess
  idd = 24        ! initial guess
  ihh = 18        ! initial guess
  irr =  3        ! initial guess

  data un, dix / 1.0e0, 10.0e0 /

! - Check that the point is inside our grid
!  print *, tt, dd, hh, rr

  if (tt .lt. TN(1) .or. tt .gt. TN(npt)) then
    print *, " Value out of range (tt)!"
    print *, " T should be in the range:", dix**TN(1), " - ", dix**TN(npt)
  endif

  if (dd .lt. DN(1) .or. dd .gt. DN(npd)) then
    print *, " Value out of range (dd)!"
    print *, " nH should be in the range:", dix**DN(1), " - ", dix**DN(npd)
  endif

  if (hh .lt. RH(1) .or. hh .gt. RH(nph)) then
    print *, " Value out of range (hh)"
    print *, " H/H2 should be in the range:", dix**RH(1), " - ", dix**RH(nph)
  endif

  if (rr .lt. ROP(1) .or. rr .gt. ROP(npr)) then
    print *, " Value out of range (rr)"
    print *, " O/P should be in the range:", ROP(1), " - ", ROP(npr)
  endif

!  Find place in grid

  call isearch(TN,npt,tt,itt)      ! find temperature index
  call isearch(DN,npd,dd,idd)      ! find density index
  ihh = 29                         ! H/H_2 index corresponding to logfH2 = 3.
  irr =  5                         ! ortho/para index corresponding to op = 1.
  if (hh .ne. 3.) then             ! find H/H_2 index, if neccesary
    hhrr_search = .true.
    call isearch(RH,nph,hh,ihh)
    if ((ihh .lt. 1) .or. (ihh .gt. nph-1)) then
      print *, "H/H_2 index out of range!"
    endif
  endif
  if (rr .ne. 1.) then             ! find ortho/para index, if neccesary
    hhrr_search = .true.
    call isearch(ROP,npr,rr,irr)
    if ((irr .lt. 1) .or. (irr .gt. npr-1)) then
      print *, "ortho/para index out of range!"
    endif
  endif

!  print *, itt, idd, ihh, irr

!  Do a simple linear interpolation

       y0000 = WG(itt  ,idd  ,ihh  ,irr  )
       y1000 = WG(itt+1,idd  ,ihh  ,irr  )
       y0100 = WG(itt  ,idd+1,ihh  ,irr  )
       y1100 = WG(itt+1,idd+1,ihh  ,irr  )
       y0010 = WG(itt  ,idd  ,ihh+1,irr  )
       y1010 = WG(itt+1,idd  ,ihh+1,irr  )
       y0110 = WG(itt  ,idd+1,ihh+1,irr  )
       y1110 = WG(itt+1,idd+1,ihh+1,irr  )
       y0001 = WG(itt  ,idd  ,ihh  ,irr+1)
       y1001 = WG(itt+1,idd  ,ihh  ,irr+1)
       y0101 = WG(itt  ,idd+1,ihh  ,irr+1)
       y1101 = WG(itt+1,idd+1,ihh  ,irr+1)
       y0011 = WG(itt  ,idd  ,ihh+1,irr+1)
       y1011 = WG(itt+1,idd  ,ihh+1,irr+1)
       y0111 = WG(itt  ,idd+1,ihh+1,irr+1)
       y1111 = WG(itt+1,idd+1,ihh+1,irr+1)

       tp = (tt - TN(itt))  /   (TN(itt+1) - TN(itt))
       up = (dd - DN(idd))  /   (DN(idd+1) - DN(idd))
       vp = (hh - RH(ihh))  /   (RH(ihh+1) - RH(ihh))
       wp = (rr - ROP(irr)) / (ROP(irr+1) - ROP(irr))

       WH2 = (un-tp) * (un-up) * (un-vp) * (un-wp) * y0000 &
           +    tp   * (un-up) * (un-vp) * (un-wp) * y1000 &
           + (un-tp) *    up   * (un-vp) * (un-wp) * y0100 &
           +    tp   *    up   * (un-vp) * (un-wp) * y1100 &
           + (un-tp) * (un-up) *    vp   * (un-wp) * y0010 &
           +    tp   * (un-up) *    vp   * (un-wp) * y1010 &
           + (un-tp) *    up   *    vp   * (un-wp) * y0110 &
           +    tp   *    up   *    vp   * (un-wp) * y1110 &
           + (un-tp) * (un-up) * (un-vp) *    wp   * y0001 &
           +    tp   * (un-up) * (un-vp) *    wp   * y1001 &
           + (un-tp) *    up   * (un-vp) *    wp   * y0101 &
           +    tp   *    up   * (un-vp) *    wp   * y1101 &
           + (un-tp) * (un-up) *    vp   *    wp   * y0011 &
           +    tp   * (un-up) *    vp   *    wp   * y1011 &
           + (un-tp) *    up   *    vp   *    wp   * y0111 &
           +    tp   *    up   *    vp   *    wp   * y1111

  return
end subroutine


!---------------------------------------------------------------------
SUBROUTINE isearch (xarr,n,x,ilo)
!---------------------------------------------------------------------
! Given an array xarr(1,...,n) and given a value x, isearch returns
! the nearest lower index ilo such that xarr(ilo) <= x < xarr(ilo+1).
! Note that ilo=0 or ilo=n is returned to indicate that x is out of 
! range. ilo on input is taken as the initial guess for ilo on output.
!---------------------------------------------------------------------

USE params
USE cooling
  implicit none

  integer :: n, ilo, ihi, im, inc
  real, dimension(n) :: xarr
  real :: x

  if ((ilo .le. 0) .or. (ilo .gt. n)) then
    ilo=0
    ihi=n+1
  else
    inc=1
    if (x .ge. xarr(ilo)) then          ! hunt upwards from initial guess
      if (ilo .eq. n) return
      ihi=ilo+1
      do
        if (x .lt. xarr(ihi)) exit
        ilo=ihi
        inc=inc+1
        ihi=ilo+inc
        if (ihi .gt. n) then
          ihi=n+1
          exit
        endif 
      enddo
    else                                ! hunt downwards from initial guess
      if (ilo .eq. 1) then
        ilo=0
        return
      endif
      ihi=ilo
      ilo=ilo-1
      do 
        if (x .ge. xarr(ilo)) exit
        ihi=ilo
        inc=inc+1
        ilo=ihi-inc
        if (ilo .le. 0) then
          ilo=0
          exit
        endif
      enddo
    endif
  endif
  
  do                                    ! bisection search
    if ((ihi-ilo) .eq. 1) exit
    im=(ihi+ilo)/2
    if (x .ge. xarr(im)) then
      ilo=im
    else
      ihi=im
    endif
  enddo
  if (x .eq. xarr(1)) ilo=1
  if (x .eq. xarr(n)) ilo=n-1     

END SUBROUTINE

!***********************************************************************
SUBROUTINE coolit (r,ee,lne,dd,dedt)
  USE params
  USE cooling
  implicit none

  integer, parameter :: not=14, noz=8, notcnm=22

  real, parameter, dimension(not) :: lnt= &         ! Vector of temperatures
  (/9.210, 9.441, 9.556, 9.786, 10.247, 10.822, 11.283, &
    12.319, 13.010, 13.700, 14.276, 15.082, 17.039, 19.572/)
  real, parameter, dimension(noz) :: lnz= &         ! Vector of metallicities
  (/-9.210, -6.908, -4.605, -3.454, -2.303, -1.151, 0.000, 1.151/)

  real, parameter, dimension(not, noz) :: logl= &   !  2D-grid in Lambda 
  reshape((/21.31, 23.82, 24.79, 25.43, 25.02, 24.67, 25.21, & !zero metals
            24.46, 24.19, 24.10, 24.10, 24.16, 24.45, 24.96, &
            21.31, 23.82, 24.79, 25.43, 25.01, 24.69, 25.22, & ![Fe/H]=-3.0
            24.58, 24.21, 24.12, 24.12, 24.15, 24.46, 24.96, &
            21.32, 23.82, 24.80, 25.42, 25.02, 24.82, 25.31, & !-2.0
            25.07, 24.49, 24.26, 24.23, 24.18, 24.47, 24.96, &
            21.35, 23.83, 24.81, 25.43, 25.03, 25.08, 25.47, & !-1.5
            25.51, 24.82, 24.49, 24.43, 24.25, 24.47, 24.96, &
            21.40, 23.85, 24.82, 25.42, 25.15, 25.43, 25.77, & !-1.0
            25.98, 25.24, 24.84, 24.75, 24.41, 24.48, 24.96, &
            21.50, 23.88, 24.80, 25.40, 25.26, 25.71, 26.03, & !-0.5
            26.20, 25.46, 25.11, 25.07, 24.60, 24.51, 24.97, &
            21.63, 24.00, 24.79, 25.42, 25.41, 26.02, 26.33, & !+0.0
            26.40, 25.67, 25.41, 25.42, 24.89, 24.63, 24.95, &
            21.91, 24.23, 25.01, 25.56, 25.82, 26.48, 26.78, & !+0.5
            26.87, 26.12, 25.88, 25.88, 25.32, 24.85, 25.04/), &
          (/not, noz/))  

  real, dimension(notcnm,2) :: loglcnm = & !Lambda Table for the CNM 
  reshape((/1.9, 2.0, 2.1, 2.2, 2.3, 2.4, 2.5, 2.6, 2.7, 2.8, 2.9,  & ! log(T)
            3.0, 3.1, 3.2, 3.3, 3.4, 3.5, 3.6, 3.7, 3.8, 3.9, 4.0,  & 
            -25.91, -25.82, -25.74, -25.66, -25.60, -25.54, -25.48, & ! log(L) 
            -25.43, -25.38, -25.33, -25.28, -25.24, -25.20, -25.16, &
            -25.13, -25.10, -25.08, -25.07, -25.06, -25.06, -25.05, &
            -24.95/),                                               &
          (/notcnm,2/))

  integer :: i, j, k, n, tind, zind, tcnm
  real, dimension(mx,my,mz) :: r, ee, lne, dd, lamb, dedt

  real :: pt, qt, pz, qz, ct, lntemp, logtemp, lnmet, logmet, cgs2cu, cmb, &
          a1, a0, logfH2, logfH2res, op, nH, lognH,  nHlt1, sigma2, Lbound, ampl
  real :: WH2, WH2_0, WH2_1

  if (.not. do_cool) return

  ct = 0.5*Cdt/dt                     ! Cooling rate constant

  tind = 1
  zind = 1
  tcnm = 1

  cgs2cu= logclambu+48.    ! erg cm-3 s-1 -> ceu clu-3 ctu-1
  logfH2= 3.0              ! the log10(n(H)/n(H_2)) ratio in the gas  (-8 < logfH2 < 6) 
  logfH2res = 6.0          ! the residual fraction H/H_2 at T = 10 000 K
  op    = 1.               ! the n(ortho)/n(para) ratio of H_2 (0.1 < op < 3)
  sigma2= 0.01             ! width^2 of the H_2 cooling drop at T = 10 000 K
  hhrr_search = .false.    ! No search if logfH2 = 3.0 and op = 1.

!$omp parallel do private(i,j,k,n,lnmet,lntemp,pt,qt,pz,qz), firstprivate (ct,tind,zind)
  do k=1,mz                           ! Calculating a (interpolated) Lambda
    do j=1,my                         ! for each (x,y,z) point. 
      do i=1,mx                         
        dd(i,j,k) = max(dd(i,j,k),0.0)
        lnmet  = alog(dd(i,j,k)+zlim)+lnfehsun
        logmet = invln10*lnmet               ! logmet > [Fe/H] = -10
        lnmet  = max(lnmet,-9.210)           ! lnmet  > [Fe/H] = -4
        lntemp = lne(i,j,k)-lncV             ! ln(T)

        if (lntemp < lnt(1)) then            ! Below lnt table min?

          !--------------------------------------------------------------------
          !Add cooling by e- and H impact excitation of C+, O, N, Si+, S+, 
          !Fe+, (Dalgarno & McCray 1972, ARA&A, 10, 375) and collisional 
          !excitation of H_2 (Le Bourlot et al. 1999, MNRAS, 305, 802; see 
          !also http://ccp7.dur.ac.uk/) appropriate for the cool neutral 
          !medium (T<10 000 K). Torgny (2003/02/07).
          !-------------------------------------------------------------------- 

          logtemp=invln10*lntemp
          nH=noH*r(i,j,k)                    ! number of H atoms per cm^3
          lognH=invln10*alog(nH)
          nHlt1=0.0
          if (nH .lt. 1.) then               ! lower limit is nH=1. in H_2 cooling grid 
            nHlt1=lognH                      ! cooling is nearly linear in rho for low rho
            lognH=0.0                        ! note that lognH+nHlt1 = log10(nH)
          end if

          call heating(loglcnm(1,2),logmet,nH,lognH,nHlt1,logfH2,op,cmb)

          !--------------------------------------------------------------------    
          ! The total, low T (CNM) cooling (metal+H_2-cmb) at T=10 000 K is 
          ! set such that it equals the high T (WIM+HIM) cooling at T=10 000 K.   
          !--------------------------------------------------------------------

          call isearch(lnz,noz,lnmet,zind)   ! Find the nearest metallicity element
          if (lnmet < lnz(1)) then           ! below table min?
            zind=1
            lnmet=lnz(1)                     ! Don't exptrapolate below table
          endif 
          if (lnmet > lnz(noz)) zind=noz-1   ! extrapolation above table allowed          
   
          a1=(logl(1,zind+1)-logl(1,zind))/(lnz(zind+1)-lnz(zind))
          a0=logl(1,zind)-a1*lnz(zind)
          Lbound = a1*lnmet+a0-48.           ! fixed boundary at T = 10 000 K

          !---------------------------------------------------------------------
          ! There are two options, either, 1.), there is equipartition between 
          ! H_2 cooling and metal cooling at T = 10 000 K, or, 2.), the H_2 
          ! cooling is set to a residual value mimicking the drop in n(H_2) when 
          ! the temperature approaches T = 10 000 K.
          !---------------------------------------------------------------------  

          ! 1.)
!          Lbound = Lbound+invln10*alog(r(i,j,k)**2+cmb*exp(-ln10*Lbound)) &
!                 - 2*(lognH+nHlt1)-log2
!          loglcnm(notcnm,2) = Lbound-logmet      ! boundary value for metal cooling
!          call coolH2(4.0e0,lognH,logfH2,op,WH2) ! low-nH correction is substr. in ampl
!          ampl   = Lbound-(WH2-logfH2-lognH)     ! boundary value for H_2 cooling
         
          ! 2.)
          call coolH2(4.0e0,lognH,logfH2,op,WH2_0)      
          call coolH2(4.0e0,lognH,logfH2res,op,WH2_1)
          hhrr_search = .false.                     ! search for residual H/H_2 index OK
          ampl   = WH2_1-WH2_0+(logfH2-logfH2res)   ! boundary value for H_2 cooling       
          WH2_1  = WH2_1-logfH2res+lognH+2*nHlt1      
          Lbound = Lbound+invln10*alog(r(i,j,k)**2+exp(-ln10*Lbound) &
                 * (cmb-exp(ln10*WH2_1)))-2*(lognH+nHlt1)
          loglcnm(notcnm,2) = Lbound-logmet         ! boundary value for metal cooling        

          !--------------------------------------------------------------------
          ! Find the contribution of cooling by metals  
          !--------------------------------------------------------------------
 
          call isearch(loglcnm(:,1),notcnm,logtemp,tcnm)   ! find nearest temp. element
          if (logtemp < loglcnm(1,1)) tcnm=1               ! extrapolation below table allowed
          if (logtemp > loglcnm(notcnm,1)) tcnm=notcnm-1   ! extrapolation above table allowed

          a1=(loglcnm(tcnm+1,2)-loglcnm(tcnm,2))/(loglcnm(tcnm+1,1)-loglcnm(tcnm,1))         
          a0=loglcnm(tcnm,2)+logmet-a1*loglcnm(tcnm,1)
          lamb(i,j,k)=nH**2*exp(ln10*(a1*logtemp+a0))      ! add metal cooling 

          !--------------------------------------------------------------------
          ! Add the cooling by H_2
          !--------------------------------------------------------------------

          if (logtemp < 2.0) then                    ! temperature >= 100 K ?
            call coolH2(2.1e0,lognH,logfH2,op,WH2_1) ! H_2 cooling at ln(T) = 4.835 (126 K)
            call coolH2(2.0e0,lognH,logfH2,op,WH2_0) ! H_2 cooling at ln(T) = 4.605 (100 K)
            WH2_1 = WH2_1+nHlt1                      ! corrected if nH < 1.
            WH2_0 = WH2_0+nHlt1                      ! corrected if nH < 1.
            a1 = 10*(WH2_1-WH2_0)                    ! linear extrapolation in log(T)
            a0 = WH2_0-a1*2.0
            lamb(i,j,k) = lamb(i,j,k) &                         ! add H_2 cooling (for T < 100 K)
                        + nH*exp(ln10*(a1*logtemp+a0-logfH2))
          else
            call coolH2(logtemp,lognH,logfH2,op,WH2)            ! H_2 cooling at T >= 100 K
            WH2 = WH2+nHlt1                                     ! corrected if nH < 1.
            WH2 = WH2+ampl*exp(-(logtemp-4.0)**2/sigma2)        ! add/substr. to fit boundary
            lamb(i,j,k) = lamb(i,j,k)+nH*exp(ln10*(WH2-logfH2)) ! add H_2 cooling (for T >= 100 K)
          end if
    
          !-----------------------------------------------------------------------
          !Finally, subtract the heating due to the CMB and convert to code units. 
          !-----------------------------------------------------------------------  
   
          lamb(i,j,k) = lamb(i,j,k)-cmb                    ! substract CMB heating
          lamb(i,j,k) = lamb(i,j,k)*exp(ln10*cgs2cu)       ! erg cm-3 s-1 -> ceu clu-3 ctu-1

        else                                 ! Cooling in a hot ionized medium!

          call isearch(lnt,not,lntemp,tind)  ! Find the nearest temperature element
          if (lntemp <= lnt(1)) tind=1       ! extrapolation below table allowed
          if (lntemp > lnt(not)) tind=not-1  ! extrapolation above table allowed

          call isearch(lnz,noz,lnmet,zind)   ! Find the nearest metallicity element
          if (lnmet < lnz(1)) then           ! below table min?
            zind=1
            lnmet=lnz(1)                     ! Don't exptrapolate below table
          endif 
          if (lnmet > lnz(noz)) zind=noz-1   ! extrapolation above table allowed

          ! Calculating Lambda at each point, using bi-linear interpolation

          pt = (lntemp-lnt(tind))/(lnt(tind+1)-lnt(tind))
          pz = (lnmet -lnz(zind))/(lnz(zind+1)-lnz(zind))
          qt = 1.-pt
          qz = 1.-pz
          lamb(i,j,k)= r(i,j,k)**2*exp(ln10*( logclambu &
                     + qt*(qz*logl(tind  ,zind)+pz*logl(tind  ,zind+1)) &
                     + pt*(qz*logl(tind+1,zind)+pz*logl(tind+1,zind+1))))
        end if  

        ! Prevent the cooling from limiting the time step -- OK because cooling
        ! is much slower at most temperatures.

        lamb(i,j,k) = min(lamb(i,j,k),r(i,j,k)*ee(i,j,k)*ct)
        dedt(i,j,k) = dedt(i,j,k) - lamb(i,j,k)
      end do
    end do
  end do

  if (hhrr_search) then
    print *, 'H/H_2 and/or O/P index search initiated:'
    print *, 'Explicitly set indicies are changed!' 
    hhrr_search = .false.
  endif
  if (idbg>0 .and. isubstep==1) print *,'cool:',maxval(lamb)

END SUBROUTINE

