subroutine transfer (np,n1,dtau,s,q,xi,doxi)

  ! Solves radiative transfer equation using integral solver, second 
  ! order accurate. Returns Q = P - S where P = (Iplus + Iminus)/2.
  !
  ! Q=Q(tau) is computed by solving the radiative transfer equation in
  ! the forward (fw)  and reverse (rv) direction separately, then 
  ! averaging the solutions in the two directions:
  !
  !   d Qfw / d tau = - Qfw - d S / d tau
  !
  !   d Qrv / d tau =   Qrv - d S / d tau 
  !
  ! Q = (Qfw + Qrv) / 2
  

  use params, only: mid, mx, my, mz, izs, ize

  implicit none

  integer np,n1 
  ! note: n1 not used explicitly in this subroutine (defined only to have same 
  ! syntax as Feautrier-like version


  real, dimension(mx,my,mz) :: dtau,s,q
  real, dimension(mx,mz)    :: xi
  logical doxi

  integer ix,iy,iz


  real, dimension(mx,my,mz) :: qfw, qrv

  real, allocatable, dimension(:,:,:) :: ds1dtau, dsdtau, d2sdtau2
  real, allocatable, dimension(:,:,:) :: ex0, ex1, ex2

  real, dimension(mx,mz) :: dinv

  real dtau1


  !real, parameter           :: dtau_thin = 0.01, dtau_mid = 0.1, dtau_thick = 15.0

  real, parameter           :: dtau_thin = 0.05, dtau_thick=100.0

  

  ! Allocate arrays for storing exponential coefficients and derivatives 
  ! of the source function

  allocate ( ex0(mx,my,mz), ex1(mx,my,mz), ex2(mx,my,mz) )
  allocate ( ds1dtau(mx,my,mz), dsdtau(mx,my,mz), d2sdtau2(mx,my,mz) )



  ! Compute derivatives of source function
  !
  !  ds1dtau  = first derivative at half-points (first-order accuracy);
  !             Note that ds1dtau(i) = derivative at half point i-1/2;
  !  dsdtau   = first derivatives, centred; second-order accuracy;
  !  d2sdtau2 = second derivatives, centred; second-order accuracy.

  ! Calculate ds1dtau

  ! Note that dtau(i) = tau(i)-tau(i-1) 

  do iz=izs,ize
     do iy=2,np
        ds1dtau(:,iy,iz) = (s(:,iy,iz) - s(:,iy-1,iz)) / dtau(:,iy,iz)
     end do
  end do

  ! Calculate dsdtau and ds2dtau2

  do iz=izs,ize
     do iy=2,np-1
        dinv  (:,iz)      = 1.0 / (dtau(:,iy+1,iz) + dtau(:,iy,iz))       
        dsdtau(:,iy,iz)   = (ds1dtau(:,iy+1,iz)*dtau(:,iy,iz) + ds1dtau(:,iy,iz)*dtau(:,iy+1,iz)) * dinv(:,iz)
        d2sdtau2(:,iy,iz) = (ds1dtau(:,iy+1,iz) - ds1dtau(:,iy,iz)) * dinv(:,iz) * 2.0
     end do
  end do

  ! Boundary values for dsdtau and d2sdtau2
  !
  ! d2sdtau2 : constant extrapolation
  ! ds1dtau  : linear extrapolation 

  do iz=izs,ize
        d2sdtau2(:, 1,iz) = d2sdtau2(:,2,iz)
        d2sdtau2(:,np,iz) = d2sdtau2(:,np-1,iz)
        dsdtau(:, 1,iz)   = ds1dtau(:, 2,iz) - d2sdtau2(:, 2,iz) * dtau(:, 2,iz) * 0.5
        dsdtau(:,np,iz)   = ds1dtau(:,np,iz) + d2sdtau2(:,np,iz) * dtau(:,np,iz) * 0.5
  end do


  ! compute exponential coefficients

  do iz=izs,ize
     do iy=1,np
        do ix=1,mx
          dtau1 = dtau(ix,iy,iz)
          if ( dtau1 .gt. dtau_thick ) then
              ex0(ix,iy,iz) = 0.0
              ex1(ix,iy,iz) = 1.0
              ex2(ix,iy,iz) = 1.0
           else if ( dtau1 .gt. dtau_thin ) then
              ex0(ix,iy,iz) = exp( -dtau1 )
              ex1(ix,iy,iz) = 1.0 - ex0(ix,iy,iz)
              ex2(ix,iy,iz) = ex1(ix,iy,iz) - dtau1 * ex0(ix,iy,iz)
           else 
              ex1(ix,iy,iz) = dtau1 * (1.0 - 0.5 * dtau1 * (1.0 -0.333333*dtau1))
              ex0(ix,iy,iz) = 1.0 - ex1(ix,iy,iz)
              ex2(ix,iy,iz) = dtau1**2 * (0.5 - 0.333333 * dtau1)
           end if
        end do
     end do
  end do


  ! Solution forward direction

  do iz=izs,ize
     qfw(:,1,iz) = - s(:,1,iz) * ex0(:,1,iz)
     do iy=2,np
        qfw(:,iy,iz) = qfw(:,iy-1,iz) * ex0(:,iy,iz) - dsdtau(:,iy,iz) * ex1(:,iy,iz) &
             + d2sdtau2(:,iy,iz) * ex2(:,iy,iz)
     end do
  end do


  ! Solution reverse direction

  do iz=izs,ize
     qrv(:,np,iz) = dsdtau(:,np,iz) + d2sdtau2(:,np,iz)
     do iy=np-1,1,-1
        qrv(:,iy,iz) = qrv(:,iy+1,iz) * ex0(:,iy+1,iz) + dsdtau(:,iy,iz) * ex1(:,iy+1,iz) &
             + d2sdtau2(:,iy,iz) * ex2(:,iy+1,iz)
     end do
  end do


  ! Finally, compute average of Qfw and Qrv, that is P = ((Ifw + Irv) - S ) / 2

  do iz=izs,ize
     do iy=1,np
        q(:,iy,iz) = 0.5 * (qfw(:,iy,iz)+qrv(:,iy,iz))
     end do
  end do


  ! Surface intensity

  if (doxi) then
     do iz=izs,ize
        xi(:,iz) = 2.0 * ex0(:,1,iz) * ( q(:,1,iz) + s(:,1,iz) )  + s(:,1,iz) * ex1(:,1,iz)**2
     end do
  end if


  ! Deallocate arrays
  
  deallocate ( ex0, ex1, ex2 )
  deallocate ( ds1dtau, dsdtau, d2sdtau2 ) 

end subroutine transfer
