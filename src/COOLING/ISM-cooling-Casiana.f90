! $Id: ISM-cooling-Casiana.f90,v 1.26 2012/01/31 18:48:51 aake Exp $
!***********************************************************************
MODULE cooling

! Note: if initial conditions are set such that [Z] could be less than
! 1.E-04, the lowest metallicity used in MODULE cooling must be set 
! to 1.E-04

! The units of lamda are energy per unit volume and time, or cdu*cvu^2/ctu,
! which the computed, dimensional value should be divided by.  But since
! a multiplicative factor of cdu^2 should also be applied, the computed
! value should be divided by cvu^2/(ctu*cdu), or multiplied by ctu*cdu/cvu^2.

  use units

  logical do_table, do_power, do_heat, do_extraterm
  character (len=40) coolH2file,tablefile

  real, allocatable, dimension(:,:):: table
  real:: a0,b0,a1,b1,tab0,Tlower,G0,G1,rho_crit
  integer:: ma,mb,turnon
END MODULE

!***********************************************************************
SUBROUTINE init_cooling (r,ee)
  USE params
  USE cooling
  implicit none
  real, dimension(mx,my,mz):: r,ee
  character(len=mid):: id='$Id: ISM-cooling-Casiana.f90,v 1.26 2012/01/31 18:48:51 aake Exp $'

  call print_trace (id, dbg_cool, 'BEGIN')

  tablefile = file(1:index(file,'.'))//'eos'
  if (do_trace) print *,'init_cooling: read'
  do_cool = .true.
  do_table = .false.
  do_power = .false.
  do_heat=.true.
  do_extraterm=.true.
  G0=1.e-24
  G1=1.e-25
  Tlower = 100.
  rho_crit=20000.
  ma = mx ; mb = my
  turnon=-1
  call read_cooling

  allocate (table(ma,mb))
  a0=50. ; a1=10.                       ! make sure table is invalid
  b0=50. ; b1=10.

  call test_cooling

  call print_trace (id, dbg_cool, 'END')
END SUBROUTINE

!***********************************************************************
SUBROUTINE read_cooling
  USE params
  USE cooling
  implicit none
  namelist /cool/do_cool,do_table,do_power,Tlower,ma,mb,coolH2file,turnon, &
       do_heat,do_extraterm,G0,G1,rho_crit

  if (stdin.ge.0) then
    rewind (stdin); read (stdin,cool)
  else
    read (*,cool)
  end if
  if (master) write (*,cool)
END SUBROUTINE

!***********************************************************************
SUBROUTINE coolit_table (r,ee,lne,dedt)

  USE params
  USE cooling
  implicit none
  real, dimension(mx,my,mz):: r,ee,lne
  real, dimension(mx,my,mz):: dedt
  integer:: ix,iy,iz,ia,ib,ic
  real:: a,b,c,pa,pb,pc,qa,qb,qc,lntab,tab,lamb,ct
  real:: rmin,rmax,lnemin,lnemax,eemin,eemax
  character(len=mid):: id='coolit_table: $Id: ISM-cooling-Casiana.f90,v 1.26 2012/01/31 18:48:51 aake Exp $'


  call print_trace (id, dbg_cool, 'BEGIN')

    rmin=minval(r  ) ;   rmax=maxval(r  )
  lnemin=minval(lne) ; lnemax=maxval(lne)
   eemin=exp(lnemin) ;  eemax=exp(lnemax)

  if ( rmin.lt.exp(a0) .or.  rmax.gt.exp(a0+(ma-1)/a1) .or. &
      eemin.lt.exp(b0) .or. eemax.gt.exp(b0+(mb-1)/b1)) then
        call new_table (r,ee,lne)
  end if
  ct = 200./min(dx,dy,dz)   ! Cooling rate constant, 200 km/s fiducial speed
  ct = 0.5*Cdt/dt
  call print_trace (id, dbg_cool, 'LOOP')

!$omp parallel private(ix,iy,iz,a,b,ia,ib,pa,pb,qa,qb,tab,lamb)
  do iz=izs,ize
    do iy=1,my
      do ix=1,mx
        a = 1.+(alog(r(ix,iy,iz))  - a0)*a1
        b = 1.+(lne(ix,iy,iz)      - b0)*b1
        ia = min(max(1,ifix(a)),ma-1)
        ib = min(max(1,ifix(b)),mb-1)
        pa = a-ia ; qa=1.-pa
        pb = b-ib ; qb=1.-pb
        tab = qb*(Qa*table(ia  ,ib  ) + pa*table(ia+1,ib  ))  + &
              pb*(qa*table(ia  ,ib+1) + pa*table(ia+1,ib+1))
	tab = tab+tab0
        if (do_power) tab=tab**5
        lamb = tab*r(ix,iy,iz)**2
        lamb = sign(min(abs(lamb),ee(ix,iy,iz)*ct),lamb)
        dedt(ix,iy,iz) = dedt(ix,iy,iz) - lamb
      end do
    end do
  end do
!$omp end parallel

  call print_trace (id, dbg_cool, 'END')
END SUBROUTINE

!***********************************************************************
SUBROUTINE new_table (r,ee,lne)
  USE params
  USE cooling
  implicit none
  real, dimension(mx,my,mz):: r,ee,lne
  real, allocatable, dimension(:,:):: rtab,etab,ltab
  real:: tabmin,tabmax
  integer:: ix,iy,iz
  character(len=mid):: id='new_table: $Id: ISM-cooling-Casiana.f90,v 1.26 2012/01/31 18:48:51 aake Exp $'

  call print_trace (id, dbg_cool, 'BEGIN')
!
!  New table
!
  print *,'========================== new_table ============================='
  allocate (rtab(ma,mb), etab(ma,mb), ltab(ma,mb))
!
!  Limits and steps in ln(rho), ln(ee)
!
  a0 = minval(alog(r) ) - 1. ; a1 = maxval(alog(r) ) + 1. ; a1 = (a1-a0)/(ma-1)
  b0 = minval(lne     ) - 1. ; b1 = maxval(lne     ) + 1. ; b1 = (b1-b0)/(mb-1)
!
!  Initialize to reasonable values
!
  rtab = exp(a0+a1*ma/2)
  ltab = b0+b1*mb/2
  etab = rtab*exp(ltab)
  table = 0.
!
!  Loop over table
!
    do iy=1,mb
      ltab(:,iy) = b0+(iy-1)*b1
      do ix=1,ma
        rtab(ix,iy) = exp(a0+(ix-1)*a1)
        etab(ix,iy) = rtab(ix,iy)*exp(ltab(ix,iy))
      end do
    end do
!
!  Calculate the cooling
!
  call coolit_real (ma,mb,rtab,etab,ltab,table)
  tabmin=minval(table)
  tabmax=maxval(table)
  tab0=tabmin-0.001*abs(tabmin)
  if (tab0.eq.0.) tab0=-1e-5*tabmax
  if (do_power) table=sign(abs(table)**0.2,table)
!
!  Take out the main density dependence, and invert steps
!
!  table = log((table-tab0))
  a1=1./a1 ; b1=1./b1
!
!  Write the table file
!
  open (3,file=tablefile,form='unformatted',status='unknown')
  write (3) ma,mb,a0,b0,a1,b1,tab0
  write (3) ((table(ix,iy), ix=1,ma), iy=1,mb)
  close (3)
  deallocate (rtab, etab, ltab)

  print *,'dlnrho, dlnee, tab0 =',a1,b1,tab0
  print *,'=================================================================='

  call print_trace (id, dbg_cool, 'END')
END SUBROUTINE

!***********************************************************************
SUBROUTINE coolit_real (na,nb,r,ee,lne,lambda)
  USE params
  USE eos
  USE units
  USE cooling
  implicit none
  integer ix,iy,na,nb
  real, dimension(na,nb):: r,ee,lne
  real, dimension(na,nb)::lambda
!  double precision, dimension(na,nb)::lambda
  real temp
  real qq
!  double precision qq
  
  logical, save:: first_time=.true.
  real, parameter:: &

    TEm=1.e2, &
    TE0=1.e3, &
    TE1=10964.781e0, &
    TE2=19952.632e0, & 
    TE3=29512.078e0, & 
    TE4=39810.708e0, & 
    TE5=69183.121e0, &
    TE6=125892.51e0, &
    TE7=251188.7e0, &
    TE8=501187.01e0, &
    TE9=891250.55e0, &
    TE10=1.412537e6, & 
    TE11=2.511887e6, &
    TE12=6.309576e6, &
    TE13=1.e7, &
    TE14=1.5848925e7, &
    TE15=1.e9, &

    Cm=2.,   &
    C0=2.596,   &
    C1=3.846, &
    C2=-0.1764, &
    C3=1.00001, &
    C4=1.6666, &
    C5=1.15384, &
    C6=0.1, &
    C7= -3.933, &
    C8= 0.72, &
    C9= -0.65,  &
    C10= 0.4, &
    C11= -1.25, &
    C12= 0., &
    C13= -0.4,  &
    C14= 0.43333, &
    
    Hm=4.98282e+15, &
    H0=3.2473989e-14/4.e-28, &
    H1=2.894e-19/4.e-28,  &
    H2=5.739e-2/4.e-28,  &
    H3=3.1620277e-07/4.e-28,  &
    H4=2.7123742e-10/4.e-28,  &
    H5=8.2298862e-08/4.e-28,  &   
    H6=1.9497e-02/4.e-28,  &
    H7=1.1749e+20       ,  &
    H8=3.5155e-7/4.e-28,  &
    H9=4.982757e+1/4.e-28,  &
    H10=1.7379e-5/4.e-28,  &   
    H11=6.309e+5/4.e-28,  &
    H12=1.99525e-03/4.e-28,  &
    H13=1.2589e-00/4.e-28,  &
    H14=1.2589e-06/4.e-28 
   
   
!   real, parameter:: scale=(ctu/clu)**2*(ctu*cdu)
   double precision, parameter:: scale=(ctu/clu)**2*(ctu*cdu)
!   double precision, parameter:: scale=(ctu**3/clu**2)/cdu

  character(len=mid):: id='coolit_real: $Id: ISM-cooling-Casiana.f90,v 1.26 2012/01/31 18:48:51 aake Exp $'

  call print_trace (id, dbg_cool, 'BEGIN')

  if (first_time) then
    first_time=.false.
    if (master) then
      print *,'scale,utemp=',scale,utemp
      print *,'TE1,tlow,TE2,H1  ',TE1,Tlower,TE2,TE3,H1
    end if
  end if

!XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXna=mx nb=my
  do iy=1,nb
  do ix=1,na
    temp = utemp*ee(ix,iy)*1.d0
    if      (temp .le. Tlower) then
      qq = 0.
    else if (temp .le. TE0) then
      qq = scale*(Hm*temp**Cm)
    else if (temp .le. TE1) then
      qq = scale*(H0*temp**C0)
    else if (temp .le. TE2) then
      qq = scale*(H1*temp**C1)
    else if (temp .le. TE3) then
      qq = scale*(H2*temp**C2)
    else if (temp .le. TE4) then
      qq = scale*(H3*temp**C3)
    else if (temp .le. TE5) then
      qq = scale*(H4*temp**C4)
    else if (temp .le. TE6) then
      qq = scale*(H5*temp**C5)
    else if (temp .le. TE7) then
      qq = scale*(H6*temp**C6)
    else if (temp .le. TE8) then
      qq = (scale/4e-28)*(H7*temp**C7)
    else if (temp .le. TE9) then
      qq = scale*(H8*temp**C8)
    else if (temp .le. TE10) then
      qq = scale*(H9*temp**C9)
    else if (temp .le. TE11) then
      qq = scale*(H10*temp**C10)
    else if (temp .le. TE12) then
      qq = scale*(H11*temp**C11)
    else if (temp .le. TE13) then
      qq = scale*(H12*temp**C12)
    else if (temp .le. TE14) then
      qq = scale*(H13*temp**C13)
    else
      qq = scale*(H14*temp**C14)
    end if
    lambda(ix,iy)=qq
  end do
  end do

  call print_trace (id, dbg_cool, 'END')
END SUBROUTINE

!***********************************************************************
SUBROUTINE coolit (r,ee,lne,dd,dedt)
  USE params
  real, dimension(mx,my,mz):: r,ee,lne,dd,dedt
  logical omp_in_parallel
!
  if (.not. do_cool) return
  if (omp_in_parallel()) then
    call coolit_omp (r,ee,lne,dd,dedt)
  else
    !$omp parallel
    call coolit_omp (r,ee,lne,dd,dedt)
    !$omp end parallel
  end if
END SUBROUTINE

!***********************************************************************
SUBROUTINE coolit_omp (r,ee,lne,dd,dedt)
!
!  Wrapper routine, to allow calling the real routine from new_table
!
  USE params
  USE eos, void=>do_table
  USE cooling
  implicit none
  real, parameter ::nohhe=0.488,codeunits=ctu**3/(cdu*clu**2) 
  real, dimension(mx,my,mz):: r,ee,lne,dd,dedt
  real,  dimension(mx,my):: qq
!  double precision, dimension(mx,my):: qq
  integer ix,iy,iz
  real Ct, hrate, onoff
  character(len=mid):: id='coolit: $Id: ISM-cooling-Casiana.f90,v 1.26 2012/01/31 18:48:51 aake Exp $'

  if (it.lt.turnon) return 
  !call print_trace (id, dbg_cool, 'BEGIN')

  if (do_table) then
    !call coolit_table (r,ee,lne,dedt)
    print *,'do_table not implemented!'
    stop
  end if

  ct = 0.5*Cdt/dt
  do iz=izs,ize
    call coolit_real (mx,my,r(:,:,iz),ee(:,:,iz),lne(:,:,iz),qq)

!Include heating rate from photoionization (by G0*n^2, choosing do_heat)
!and extra term which origin could be dissipation of interstellar plasma
! turbulence (Reynods,Haffner, Tufte 1999) or photoelectric heating (Reynolds,
! Cox 1992) and that is included by the extraterm G1*n
!(choosing do_heat and do_extraterm). The values of G0 and G1 are extracted
!from Reynolds, Haffner, Tufte 1999, altough a value of G0=1.3e-24 could be 
!used (Reynolds, Cox 1992). 
!'codeunits' is just the value from convert [G0*n^2]=[G1*n]=erg*s^-1*cm^-3 
!to code units.

!This approx. should be valid as long as the medium is totally ionized
!as it should be consider in Giant HII regions.
!In cases were rho is larger than rho_crit then hrate=0. This approx come form the fact that 
!at in the very dense clumps the starlight can not pass. The problem must be in how to choose 
!the critical rho. The answer partially can be found in Franco 1996. He states that the condition 
!for molecula to be form is that the column density is higher than N=677/L(in pc).  

    if (do_heat) then
      if (do_extraterm) then
        do iy=1,my
        do ix=1,mx
          hrate= (G0*(nohhe*r(ix,iy,iz))**2+G1*nohhe*r(ix,iy,iz))*codeunits 
          onoff= rho_crit**2/(r(ix,iy,iz)**2+rho_crit**2)
	  onoff= onoff**2
          qq(ix,iy)= onoff*hrate-qq(ix,iy)*r(ix,iy,iz)**2
          qq(ix,iy)= sign(min(abs(qq(ix,iy)),r(ix,iy,iz)*ee(ix,iy,iz)*ct),qq(ix,iy))
          dedt(ix,iy,iz)= dedt(ix,iy,iz) + qq(ix,iy)
        enddo
        enddo
      else
        do iy=1,my
        do ix=1,mx
          hrate= G0*codeunits*(nohhe*r(ix,iy,iz))**2  
          onoff= rho_crit**2/(r(ix,iy,iz)**2+rho_crit**2)
	  onoff= onoff**2
          qq(ix,iy)= onoff*hrate-qq(ix,iy)*r(ix,iy,iz)**2
          qq(ix,iy)= sign(min(abs(qq(ix,iy)),r(ix,iy,iz)*ee(ix,iy,iz)*ct),qq(ix,iy))
          dedt(ix,iy,iz)= dedt(ix,iy,iz) + qq(ix,iy)
        enddo
        enddo
      endif   
    else
      do iy=1,my
      do ix=1,mx
        qq(ix,iy)= qq(ix,iy)*r(ix,iy,iz)**2
        qq(ix,iy)= min(qq(ix,iy),r(ix,iy,iz)*ee(ix,iy,iz)*ct)
        dedt(ix,iy,iz)= dedt(ix,iy,iz) - qq(ix,iy)
      end do
      end do
    endif   
  enddo

  !call print_trace (id, dbg_cool, 'END')
END SUBROUTINE

!***********************************************************************
SUBROUTINE test_cooling
  USE params, only: master
  USE units, only: utemp
  !USE eos
  implicit none
  real, dimension(16,1):: temp, r, ee1, ee2, lne, qq1, qq2
  integer i
!-----------------------------------------------------------------------
  temp(:,1) = (/ &
    1.e3, &
    10964.781e0, &
    19952.632e0, & 
    29512.078e0, & 
    39810.708e0, & 
    69183.121e0, &
    125892.51e0, &
    251188.7e0, &
    501187.01e0, &
    891250.55e0, &
    1.412537e6, & 
    2.511887e6, &
    6.309576e6, &
    1.e7, &
    1.5848925e7, &
    1.e9 /)
  ee1 = 0.999*temp/utemp
  ee2 = 1.001*temp/utemp
  r = 1.
  do i=1,16
    call coolit_real (16,1,r,ee1,lne,qq1) 
    call coolit_real (16,1,r,ee2,lne,qq2) 
    if (master) then
      print '(i3,4g12.4)',i,temp(i,1),qq1(i,1),qq2(i,1),qq2(i,1)/(qq1(i,1)+1e-30)
    end if
  end do
END
