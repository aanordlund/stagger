! $Id: radiation_lookup_opt.f90,v 1.31 2016/11/09 18:19:41 aake Exp $
!***********************************************************************
! This version call trnslt 3 times per ray direction, as compared to
! 1 + 2*nbin = 9 times in radiation_lookup_mpi.f90.  On the other hand
! it calls lookup Nray times, as compared to 1 time, but this is CPU-
! bound so scales fine, and can also be optimized and simplified by 
! using a table with dense values, so bi-linear interpolation can be used.
!
! The number of active depth points is set as follows:
!
! 1) for a new time step, the previous number for vertical rays + 5
! 2) for a new mu, the value for the previous mu
! 3) for the first phi, the largest value for the previous mu
! 3) for subsequent phi, the max value needed for the previous phi + 5
! 4) for ibox=1, the first index with dtau > dtaumax everywhere
! 5) for the next ibox, start with the previous value, then test dtau
!***********************************************************************
MODULE cooling
  USE params
  integer, parameter:: mmu=9, mphi=10
  real dphi, form_factor, dtaumin, dtaumax
  integer nmu, nphi, verbose, ny0
  integer mblocks                                                       !maximum number of subdomains considered (per side) to fill the ghost zones
  integer n1, n2
  integer truncate
  real y_newton, dy_newton, t_newton, ee_newton, ee_min, t_ee_min
  real, dimension(mmu):: mu0=0.0

  real, pointer, dimension(:,:,:):: rk,lnrk,s,q,dtau,qq
  real, allocatable, dimension(:,:,:):: lnr_ray, ee_ray, lnr, q1
  real, allocatable, dimension(:,:,:,:):: lns

  logical do_table, do_newton, do_limb, do_fld
  character(len=mfile):: intfile
END MODULE

!***********************************************************************
SUBROUTINE init_cooling (r,e)
  USE params
  USE arrays
  USE cooling
  implicit none
  real, dimension(mx,my,mz):: r,e
  integer i
  character(len=mfile):: name

  do_cool = .true.
  i = index(file,'.')
  truncate = 0
  nmu = 0
  nphi = 4
  dphi = 0.
  form_factor = 1.
  dtaumax = 100.
  dtaumin = 0.1
  intfile = name('intensity.dat','int',file)

  do_newton = .false.
  y_newton = -0.3
  dy_newton = 0.05
  t_newton = 0.01
  t_ee_min = 0.005
  ee_newton = -1.
  ee_min = 3.6
  do_limb  = .false.
  verbose = 0
  ny0 = 0
  mblocks = 1                                                           !default: communicate only nearest neighbour
  do_fld = .false.

  call read_cooling
  rk   => scr1
  lnrk => scr2
  s    => scr3
  q    => scr4
  dtau => scr5
  qq   => scr6

  !call test_trnslt
END SUBROUTINE

!***********************************************************************
SUBROUTINE read_cooling
  USE params
  USE eos
  USE cooling
  namelist /cool/do_cool, nmu, nphi, dphi, form_factor, dtaumin, dtaumax, intfile, &
    do_newton, y_newton, dy_newton, t_newton, ee_newton, ee_min, t_ee_min, do_limb, mu0, &
    yslice, ny0, verbose, mblocks, do_fld, timer_verbose, truncate
  character(len=mfile):: name, fname
  character(len=mid),save:: &
    id='$Id: radiation_lookup_opt.f90,v 1.31 2016/11/09 18:19:41 aake Exp $'
!.......................................................................
  call print_id(id)

  rewind (stdin); read (stdin,cool)
  if (nmu==0 .and. form_factor==1.) form_factor=0.4
  if (master) write (*,cool)
  if (master) write (*,*) 'mbox, dbox =', mbox, dbox
  if (do_limb) then
    fname = name('limb.dat','lmb',file)
    call mpi_name(fname)
    open (limb_unit, file=trim(fname), &
      form='unformatted', status='unknown')
    write (limb_unit) mx, mz,nmu,nphi
  end if
END SUBROUTINE

!***********************************************************************
SUBROUTINE coolit (r, ee, lne, dd, dedt)
!
!  Radiative cooling
!
  USE params
  !USE variables
  USE units
  USE eos
  USE cooling
  implicit none
  real, dimension(mx,my,mz):: r, ee, lne, dd, dedt
  real:: xmu(mmu)=0.0, wmu(mmu)=0.0
  integer ix, iy, iz, lmu, lphi, imu, iphi, ibox, n1p, n2p, ny
  real sec, dxdy, dzdy, tanth, qq1, f1, f2, dtau2
  real phi, wphi, womega
  integer lrec, nyr, nzap, nymax, nymax_phi, ny_trnslt
  integer ny_tmp(mpi_size-1), req(mpi_size-1), rank
  logical flag, do_io, io_flag
  logical, save:: first_time=.true.
  external transfer
  logical debug2
  real cput(2), void, prof
  integer, save:: nrad=0, nang=0, nyrem=999999
  character(len=mfile):: name, fname
  integer(kind=8), save:: nsent, ncalls
!
  if (.not. do_cool) return
  call timer('radiation','start')

  if (lb > 1) then                                                      ! if we have ghost zones
    dedt(:,1:lb-1,izs:ize) = 0.                                         ! zap previous dedt there
  end if
  
  call buffer_reset (nsent)

  !$omp master
  allocate (lnr_ray(mx,my,mz), ee_ray(mx,my,mz), lnr(mx,my,mz), q1(mx,my,mz))
  allocate (lns(mx,my,mz,mbox))
  !$omp end master
  !$omp barrier

  do iz=izs,ize
    lnr(:,:,iz)=alog(r(:,:,iz))
  end do

!-----------------------------------------------------------------------
!  Angular quadrature
!  nmu > 0: Use Gauss integration (mu=1 excluded) with 2*nmu   order accuracy
!  nmu = 0: One vertical ray with weight 0.5
!  nmu < 0: Use Radau integration (mu=1 included) with 2*nmu-1 order accuracy
!-----------------------------------------------------------------------
  if      (nmu .gt. 0) then
    lmu = nmu
    call gausi (lmu,0.,1.,wmu,xmu)
  else if (nmu .eq. 0) then
    lmu = 1
    xmu(1) = 1.
    wmu(1) = 0.5
  else 
    lmu =-nmu
    call radaui(lmu,0.,1.,wmu,xmu)
  end if
  if (mu0(1) .ne. 0.) xmu=mu0

  if (first_time) then
    call barrier_omp ('rad-first')
    if (master) then
      print *,' imu       xmu    theta'
      !           1.....0.000.....00.0
      do imu=1,lmu 
        print '(i4,f10.3,f8.1,f10.3)',imu,xmu(imu),acos(xmu(imu))*180./pi,wmu(imu)
      end do
    end if
  end if

  call dumpn (r , 'r','rad.dmp',0)
  call dumpn (ee,'ee','rad.dmp',1)

  do iz=izs,ize
    qq(:,:,iz) = 0.
  end do
                                        call timer ('radiation', 'begin')

  nymax = min(nyrem+5,mytot)                                            ! 5 extra zones margin
  do imu=lmu,1,-1                                                       ! towards larger inclination
    if (timer_verbose>0) call timer('radiation','general')
    if (nmu .eq. 0 .and. imu .eq. 1) then
      lphi = 1 
    else if (nmu .lt. 0 .and. imu .eq. lmu) then
      lphi = 1 
    else
      lphi = nphi
    end if
    tanth = tan(acos(xmu(imu)))
    wphi = 4.*pi/lphi

    nymax_phi = 0                                                       ! max over phi
    do iphi=1,lphi
      n_omega = n_omega + 1
      phi = (iphi-1)*2.*pi/lphi + dphi*t
      dxdy = tanth*cos(phi)
      dzdy = tanth*sin(phi)

      ny = nymax
      ny_trnslt = ny
      if (master .and. verbose==1 .and. isubstep==1) &
        print '(a,3i5,2e15.7)',' imu,iphi,ny,dxdy,dzdy=', &
                                 imu,iphi,ny,dxdy,dzdy
      call trnslt (lnr, lnr_ray, dxdy, dzdy, .false., ny, mblocks, verbose)
      call dumpn (lnr_ray,'lnr_ray','opt.dmp',1)
      call trnslt (ee , ee_ray, dxdy, dzdy, .false., ny, mblocks, verbose)
      call dumpn (ee_ray,'ee_ray','opt.dmp',1)

      call lookup (lnr_ray, ee_ray, lnrk, lns, ny)
      !call dumpn (lnrk,'lnrk','opt.dmp',1)

                                          call timer('radiation','lookup')
      do iz=izs,ize
        q1(:,:,iz) = 0.
      end do
      nymax = 0
      do ibox=1,mbox
        !$omp barrier
        !$omp single
        n1 = 2
        n2 = 2
        !$omp end single
        n1p = 2
        n2p = 2

        do iz=izs,ize
           s(:,1:ny,iz) = exp(lns(:,1:ny,iz,ibox))
        end do
                    if (timer_verbose>1) call timer('radiation','source')
        call dumpn (s,'s','trf.dmp',1)

        if (ibox.eq.1) then
          do iz=izs,ize
            rk(:,1:ny,iz) = exp(lnrk(:,1:ny,iz))
          end do
          call dumpn (rk,'rk','opt.dmp',1)
          f1 = (0.5/xmu(imu))
          do iz=izs,ize
            dtau(:,1,iz) = (ym(2)-ym(1))*rk(:,1,iz)/xmu(imu)
            do iy=2,ny
            do ix=1,mx
              dtau(ix,iy,iz) = f1*dymdn(iy)*(rk(ix,iy-1,iz)+rk(ix,iy,iz))
              n1p = merge(max(n1p,iy), n1p, dtau(ix,iy,iz) < dtaumin)
              n2p = merge(max(n2p,iy), n2p, dtau(ix,iy,iz) < dtaumax)
            end do
            end do
          end do
        else
          do iz=izs,ize
          do iy=1,ny
          do ix=1,mx
            dtau(ix,iy,iz) = (10.**dbox)*dtau(ix,iy,iz)
            n1p = merge(max(n1p,iy), n1p, dtau(ix,iy,iz) < dtaumin)
            n2p = merge(max(n2p,iy), n2p, dtau(ix,iy,iz) < dtaumax)
          end do
          end do
          end do
        end if
        n2p=n2p+1
                      if (timer_verbose>0) call timer('radiation','dtau')
        !$omp critical
        n1 = max(n1p,n1)
        n2 = max(n2p,n2)
        !$omp end critical

        if (master .and. debug2(dbg_rad,2)) print'(a,6i6)', &
          ' ibox, iphi, imu, n2, ny, nymax_phi', &
            ibox, iphi, imu, n2, ny, nymax_phi

        ny = n2                                         ! remember the number of layers until next ibox
        nzap = ny+1

        call dumpn (dtau,'dtau','trf.dmp',1)

        nrad = nrad + n2
        nang = nang + 1

        if (do_dump .and. nzap<my) s(:,nzap:my,izs:ize) = 0.
        call dumpn (s,'s','trf.dmp',1)

!-----------------------------------------------------------------------
!  Raditative transfer equation
!-----------------------------------------------------------------------
        flag = do_limb .and. do_io(t+dt,tscr,iscr+iscr0,nscr)                         ! limb darkening every scratch
        io_flag = do_io(t+dt,tscr ,iscr +iscr0 ,nscr ) .or. &                         ! scratch or
                  do_io(t+dt,tsnap,isnap+isnap0,nsnap)                                ! snapshot 
        flag = flag .or. (xmu(imu)==1. .and. io_flag)                                 ! every i/o but only mu=1
        flag = flag .and. isubstep.eq.1 .and. ibox.eq.1                               ! and only for first substep and bin

        call transfer (n2,n1,dtau,s,q,surface_int,flag)
        n_transfer = n_transfer + mx*n2*mz
        call dumpn (dtau,'dtau','opt.dmp',1)
        call dumpn (   s,   's','opt.dmp',1)
        if (do_dump .and. nzap<my) q(:,nzap:my,izs:ize) = 0.
        call dumpn (   q,   'q','opt.dmp',1)
        if (verbose > 0) then
          if (ibox==1) then
            print'(a,/,(1p,10e12.4))','lnr'   ,   lnr_ray(1,1:n2,1)
            print'(a,/,(1p,10e12.4))','ee'    ,    ee_ray(1,1:n2,1)
            print'(a,/,(1p,10e12.4))','rhokap',        rk(1,1:n2,1)
          end if
          print'(a,6i5,1p,4g12.4)','imu,iph,ibox,n1,n2,ny', &
            imu,iphi,ibox,n1,n2,ny
          print'(a,/,(1p,10e12.4))',   's',   s(1,1:n2,1)
          print'(a,/,(1p,10e12.4))','dtau',dtau(1,1:n2,1)
          print'(a,/,(1p,10e12.4))',   'q',   q(1,1:n2,1)
        end if
        if (timer_verbose>0) call timer('radiation','transfer')

        if (flag) then
          !print'(1x,a,4i4,l3,1p2e12.3)','MARK',isubstep,ibox,imu,iphi,io_flag,xmu(imu),surface_int(1,1)
          call barrier_omp('limb')
          if (do_limb .and. omp_master) then
            write(limb_unit) t,imu,iphi,xmu(imu),phi,surface_int
            print *,omp_mythread,isubstep,ibox,imu,iphi
          end if
        end if

        do iz=izs,ize
          do iy=2,n2-1
            f2 = xmu(imu)/(ym(iy+1)-ym(iy-1))
            q(:,iy,iz) = q(:,iy,iz)*(dtau(:,iy+1,iz)+dtau(:,iy,iz))*f2
          end do
          f2 = xmu(imu)/(ym(2)-ym(1))
          q(:, 1,iz) = q(:,1,iz)*dtau(:,2,iz)*f2
          q(:,n2,iz) = 0.0
        end do

        if (do_dump .and. nzap<my) q(:,nzap:my,izs:ize) = 0.
        call dumpn(q,'q','trf.dmp',1)
        call dumpn (   q,   'q','opt.dmp',1)

!-----------------------------------------------------------------------
! Sum contributions over bins
!-----------------------------------------------------------------------
        if (do_dump .and. nzap<my) q(:,nzap:my,izs:ize) = 0.

        do iz=izs,ize
        do iy=1,n2-1
          q1(:,iy,iz) = q1(:,iy,iz) + q(:,iy,iz)
        end do
        end do
        nymax = max(n2-1,nymax)                                         ! max over ibox, for Q-trnslt
        if (timer_verbose>0) call timer('radiation','Q')

        call dumpn(q1,'q1','trf.dmp',1)
        !if (ibox == 1) then
        !  if (mpi_rank == 0) then
        !    do rank=1,mpi_size-1
        !      call irecv_int_mpi (ny_tmp(rank), 1, rank, req(rank))
        !    end do
        !  else
        !    call isend_int_mpi (nymax, 1, 0, req(1))
        !  end if
        !end if
      end do ! ibox=1,nbox

      !if (mpi_rank == 0) then
      !  call waitall_mpi (mpi_size-1, req)
      !  nymax = maxval(ny_tmp)
      !else
      !  call wait_mpi (req(1))
      !end if
      !call bcast_int_mpi (nymax, 1)
      !                               call timer ('radiation', 'max_int')

!-----------------------------------------------------------------------
! Tilt back to cartesian mesh and add to dedt
!-----------------------------------------------------------------------
      call max_int_mpi (nymax)                                          ! max over all ranks
                                       call timer ('radiation','max_int')
      ny = nymax
      call trnslt (q1, q, -dxdy, -dzdy, .false., ny, mblocks, verbose)
      call dumpn(q,'q','opt.dmp',1)

      f1 = wmu(imu)*wphi*form_factor
      do iz=izs,ize
        qq(:,1:nymax,iz) = qq(:,1:nymax,iz) + q(:,1:nymax,iz)*f1
      end do
      call dumpn(qq,'qq','opt.dmp',1)
      call dumpn(dedt,'dedt','trf.dmp',1)
                                       call timer ('radiation', 'sum(Q)')

      nymax_phi = max(nymax+1,nymax_phi)                                ! max over phi
      nymax = nymax + 5                                                 ! next phi initial value
    end do ! iphi=1,lphi

    nymax = nymax_phi                                                   ! next mu initial value

    if (master .and. debug2(dbg_rad,1)) print'(a,6i6)', &
     ' imu, nymax_phi =', &
       imu, nymax_phi


    if (imu==lmu) nyrem = nymax_phi
  end do ! imu=lmu,1,-1
  call dumpn(qq,'qq_tot','opt.dmp',1)

  do iz=izs,ize
    dedt(:,:,iz) = dedt(:,:,iz) + qq(:,:,iz)
  end do
                                         call timer ('radiation', 'dedt')
                                    call dumpn (dedt, 'dedt','opt.dmp',1)
  call radiative_flux (qq,q)
                                     call timer ('radiation', 'rad.flux')
  if (do_newton) then
    if (ee_newton > 0) eetop = ee_newton
    do iz=izs,ize
    do iy=1,my
      prof = exp(min((y_newton-ym(iy))/dy_newton,70.))
      prof = prof/(1.+prof)
      do ix=1,mx
        dedt(ix,iy,iz) = dedt(ix,iy,iz) - r(ix,iy,iz)*(ee(ix,iy,iz)-eetop)/t_newton*prof &
                                        - r(ix,iy,iz)*min(0.,ee(ix,iy,iz)-ee_min)/t_ee_min
      end do
    end do
    end do
  end if
                                       call timer ('radiation', 'newton')

  !$omp barrier
  !$omp master
  deallocate (lnr_ray, ee_ray, lnr, q1, lns)
  !$omp end master

  if (omp_master) first_time = .false.

  call buffer_count
!  call trnslt_count
  call timer('radiation','end')
END SUBROUTINE
