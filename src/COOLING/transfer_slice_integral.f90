! $Id: transfer_slice_integral.f90,v 1.1 2004/07/23 07:44:35 aake Exp $
!***********************************************************************
SUBROUTINE transfer_slice_integral (mx, my, dtau, S, Q, I)
!
!  Solve the radiative transfer equation in an x-y slice, where y is the
!  vertical direction and x is a redundant direction.
!
!  dtau:   optical depth intervals
!  S   :   source funtion
!  Q   :   cooling contribution; average of in-coming and out-going I-S
!  I   :   surface intensity
!
!  Operation count: 1e+11a+12m+2d
!
!  Author: Aake Nordlund, 2004-07-22
!-----------------------------------------------------------------------
  implicit none
!
  integer mx, my, ix, iy
  real, dimension(mx,my) :: dtau, S, Q
  real, dimension(mx,my) :: ex0, ex1, ex2, ds, dsdtau, d2sdtau2
  real, dimension(mx,my) :: qplus, qminus
  real, dimension(mx)    :: I
  real :: ctau
  real(kind=8):: exp0
!
!-----------------------------------------------------------------------
!  Coefficients, 1e+4a+1m+1d
!-----------------------------------------------------------------------
  do iy=2,my
    do ix=1,mx
      ds(ix,iy) = (s(ix,iy)-s(ix,iy-1))/dtau(ix,iy)
      exp0 = exp(-dble(dtau(ix,iy)))
      ex0(ix,iy) = exp0
      ex1(ix,iy) = 1d0-exp0
      ex2(ix,iy) = 1d0-exp0-dtau(ix,iy)*exp0
    end do  
  end do
  do ix=1,mx
    exp0 = exp(-dble(dtau(ix,1)))
    ex0(ix,1) = exp0
    ex1(ix,1) = 1d0-exp0
    ex2(ix,1) = 1d0-exp0-dtau(ix,1)*exp0
  end do
!
!-----------------------------------------------------------------------
!  dS/dtau and d2S/dtau2 (5m+3a+1d)
!-----------------------------------------------------------------------
  do iy=2,my-1
    do ix=1,mx
      ctau = 1./(dtau(ix,iy+1)+dtau(ix,iy))
      dsdtau(ix,iy) = (ds(ix,iy+1)*dtau(ix,iy)+ds(ix,iy)*dtau(ix,iy+1))*ctau
      d2sdtau2(ix,iy) = (ds(ix,iy+1)-ds(ix,iy))*2.*ctau
    end do
  end do
  dsdtau(:,1) = dsdtau(:,2)
  d2sdtau2(:,1) = d2sdtau2(:,2)
  dsdtau(:,my) = dsdtau(:,my-1)+dtau(:,my)*d2sdtau2(:,my-1)
  d2sdtau2(:,my) = d2sdtau2(:,my-1)
!
!-----------------------------------------------------------------------
!  Incoming rays (2a+3m)
!-----------------------------------------------------------------------
  qminus(:,1) = -s(:,1)*ex0(:,1)
  do iy=2,my
    qminus(:,iy) = qminus(:,iy-1)*ex0(:,iy)-dsdtau(:,iy)*ex1(:,iy)+d2sdtau2(:,iy)*ex2(:,iy)
  end do
!
!-----------------------------------------------------------------------
!  Outgoing rays (2a+3m)
!-----------------------------------------------------------------------
  qplus(:,my) = dsdtau(:,my)+d2sdtau2(:,my)
  do iy=my-1,1,-1
    qplus(:,iy) = qplus(:,iy+1)*ex0(:,iy+1)+dsdtau(:,iy)*ex1(:,iy+1)+d2sdtau2(:,iy)*ex2(:,iy+1)
    q(:,iy) = 0.5*(qplus(:,iy)+qminus(:,iy))
  end do
  q(:,my) = 0.5*(qplus(:,my)+qminus(:,my))
!
!-----------------------------------------------------------------------
!  Surface intensity.
!-----------------------------------------------------------------------
  I(:) = 2.*(q(:,1)+s(:,1))
END
