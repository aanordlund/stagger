! $Id: radiation_vertical.f90,v 1.13 2011/04/07 10:57:46 aake Exp $
!***********************************************************************
MODULE cooling

  USE params
  logical do_table
  character(len=mfile) intfile
  real form_factor
END MODULE

!***********************************************************************
SUBROUTINE init_cooling (r,e)
  USE params
  USE cooling
  real, dimension(mx,my,mz):: r,e
  character(len=mfile) name
  character(len=mid):: id='$Id: radiation_vertical.f90,v 1.13 2011/04/07 10:57:46 aake Exp $'
  if (id.ne.' ') print *,id ; id = ' '

  do_cool = .true.
  form_factor = 0.5
  intfile = name('intensity.dat','int',file)

  call read_cooling
END SUBROUTINE

!***********************************************************************
SUBROUTINE read_cooling
  USE params
  USE cooling
  namelist /cool/do_cool, form_factor, intfile

  if (stdin.ge.0) then
    rewind (stdin); read (stdin,cool)
  else
    read (*,cool)
  end if
  write (*,cool)
END SUBROUTINE


!***********************************************************************
SUBROUTINE coolit (r,ee,lne,dd,dedt)
!
!  Radiative cooling
!
  USE params
  USE units
  USE eos
  USE cooling
  implicit none
  real, dimension(mx,my,mz):: r,ee,lne,dd,dedt
  real, dimension(mx,my,mz):: tt,rkap,s,q
  real, dimension(:,:,:), allocatable:: ne,dtau
  real, dimension(mx,mz):: xi
  integer iy,iz
  integer lrec
  logical flag
!
  if (.not. do_cool) return

!-----------------------------------------------------------------------
!  Equation of state and opacity
!-----------------------------------------------------------------------
  allocate (ne(mx,my,mz))
  !call press (r,ee,tt=tt,ne=ne)
  print *,'OBSOLOTE CALL IN radiation_vertical.f90'
  call opacity (r,tt,ne,rkap)
  call dump (rkap,'rad.dat',1)
  call dump (ne  ,'rad.dat',5)
  deallocate (ne)

!-----------------------------------------------------------------------
!  Optical depth
!-----------------------------------------------------------------------
  allocate (dtau(mx,my,mz))
!$omp parallel do private(iy,iz)
  do iz=1,mz
    dtau(:,1,iz) = dy*rkap(:,1,iz)
    do iy=2,my
      dtau(:,iy,iz) = dy*0.5*(rkap(:,iy-1,iz)+rkap(:,iy,iz))
    end do
  end do
  call dump (dtau,'rad.dat',2)

!-----------------------------------------------------------------------
!  Raditative transfer equation
!-----------------------------------------------------------------------
!$omp parallel do private(iz)
  do iz=1,mz
    s(:,:,iz) = (stefan/(pi*u_e*u_l/u_t))*tt(:,:,iz)**4
  end do
  call dump (s,'rad.dat',3)
  flag = mod(it,nsnap).eq.0
  if (do_trace) print *,'radiation:',flag
  call transfer (my,20,dtau,s,q,xi,flag)
  deallocate (dtau)

  if (flag .and. intfile.ne.' ') then
    open (2,file=intfile,form='unformatted',status='unknown',access='direct',recl=lrec(mx*mz))
    write (2,rec=1+isnap) xi
    close (2)
  end if

  call dump (q,'rad.dat',4)

!-----------------------------------------------------------------------
!  Energy equation; the form_factor represents <Q>/Q_vertical
!-----------------------------------------------------------------------
!$omp parallel do private(iz)
  do iz=1,mz
    dedt(:,:,iz) = dedt(:,:,iz) + (4.*pi*form_factor)*rkap(:,:,iz)*q(:,:,iz)
  end do
  call dump (dedt,'rad.dat',6)

END SUBROUTINE
