MODULE interp_m
  integer(kind=8):: ninterp=0
END MODULE

SUBROUTINE interp_count
  USE interp_m
  USE params
  if (master) print 1, ninterp, real(ninterp)
  call sum_int8_mpi (ninterp)
  if (master) print 1, ninterp, real(ninterp)
1 format(' interp: ninterp =', 1p, i15, e11.2)
  ninterp = 0
END SUBROUTINE interp_count

!=======================================================================
SUBROUTINE check_linearity (f, n1, n2, nfail)
  USE params
  implicit none
  integer n1, n2, nfail
  real f(n1,n2), f1, f2, fij
  logical failed
  integer i, j
!.......................................................................
  nfail = 0
  do i=1,n1
    f1 = f(i,1)
    f2 = f(i,n2)
    failed = .false.
    do j=1,n2
      fij = ((j-1)*f2 + (n2-j)*f1)/real(n2-1)
      if (abs(fij-f(i,j)) > 1e-5*f(i,j)) failed = .true.
    end do
    if (failed) nfail = nfail+1
  end do
END SUBROUTINE

!=======================================================================
SUBROUTINE interpolate_xup (fin, dxdy, fout, nymax)
  USE interp_m
  USE params
  implicit none
  real, dimension(mx,my,mz):: fin, fout
  integer:: nymax, ix, iy, iz
  real   :: dxdy, dixdy, p, q, p1, p2, p3, p4
!.......................................................................
  if (abs(dxdy*(ymg(mytot)-ymg(1))) < 1e-3) then
    do iz=izs,ize
      fout(:,:,iz) = fin(:,:,iz)
    end do
  else
    call mpi_send_x (fin, gx1, 1, hx2, 2)
    dixdy = dxdy/dx
    do iy=1,my
      if (iy+iyoff > nymax) cycle
  !     af  =  q+p*q*(q-p)
  !     bf  =  p-p*q*(q-p)
  !     ad  =  p*q*q*0.5
  !     bd  = -p*q*p*0.5
  !     ac  =  af - bd
  !     bc  =  bf + ad
  !                   ac * fin2( ix, iy, mp0 ) + bc * fin2( ix, iy, mp1 ) - &
  !                   ad * fin2( ix, iy, mm1 ) + bd * fin2( ix, iy, mp2 )
      p  = dixdy*ym(iy)
      p  = p-floor(p)
      q  = 1.-p
      p1 = -p*q*q*0.5           ! ad
      p4 = -p*q*p*0.5           ! bd
      p2 =  q+p*q*(q-p) - p4    ! ac
      p3 =  p-p*q*(q-p) - p1    ! bc
      do iz=izs,ize
        do ix=2,mx-2
          fout(ix,iy,iz) = p1*fin(ix-1,iy,iz) &
                         + p2*fin(ix  ,iy,iz) &
                         + p3*fin(ix+1,iy,iz) &
                         + p4*fin(ix+2,iy,iz)
        end do
        fout( 1,iy,iz)   = p1*gx1(   1,iy,iz) &
                         + p2*fin(   1,iy,iz) &
                         + p3*fin(   2,iy,iz) &
                         + p4*fin(   3,iy,iz)
        fout(mx-1,iy,iz) = p1*fin(mx-2,iy,iz) &
                         + p2*fin(mx-1,iy,iz) &
                         + p3*fin(mx  ,iy,iz) &
                         + p4*hx2(   1,iy,iz)
        fout(mx,iy,iz)   = p1*fin(mx-1,iy,iz) &
                         + p2*fin(  mx,iy,iz) &
                         + p3*hx2(   1,iy,iz) &
                         + p4*hx2(   2,iy,iz)
      end do
    end do
    ninterp = ninterp + mx*nymax*(ize-izs+1)
  end if
END SUBROUTINE interpolate_xup

!=======================================================================
SUBROUTINE interpolate_zup (fin, dzdy, fout, nymax)
  USE interp_m
  USE params
  implicit none
  real, dimension(mx,my,mz):: fin, fout
  integer:: nymax, ix, iy, iz
  real   :: dzdy, dizdy, p, q, p1, p2, p3, p4
!.......................................................................
  if (abs(dzdy*(ymg(mytot)-ymg(1))) < 1e-3) then
    do iz=izs,ize
      fout(:,:,iz) = fin(:,:,iz)
    end do
  else
    call mpi_send_z (fin, gz1, 1, hz2, 2)
    dizdy = dzdy/dz
    do iy=1,my
      if (iy+iyoff > nymax) cycle
      p  = dizdy*ym(iy)
      p  = p-floor(p)
      q  = 1.-p
      p1 = -p*q*q*0.5
      p4 = -p*q*p*0.5
      p2 =  q+p*q*(q-p) - p4
      p3 =  p-p*q*(q-p) - p1
      do iz=max(izs,2),min(ize,mz-2)
        do ix=1,mx
          fout(ix,iy,iz)   = p1*fin(ix,iy,iz-1) &
                           + p2*fin(ix,iy,iz  ) &
                           + p3*fin(ix,iy,iz+1) &
                           + p4*fin(ix,iy,iz+2)
        end do
      end do
      if (izs==1) then
        do ix=1,mx
          fout(ix,iy, 1)   = p1*gz1(ix,iy,   1) &
                           + p2*fin(ix,iy,   1) &
                           + p3*fin(ix,iy,   2) &
                           + p4*fin(ix,iy,   3)
        end do
      end if
      if (ize==mz) then
        do ix=1,mx
          fout(ix,iy,mz-1) = p1*fin(ix,iy,mz-2) &
                           + p2*fin(ix,iy,mz-1) &
                           + p3*fin(ix,iy,mz  ) &
                           + p4*hz2(ix,iy,   1)
          fout(ix,iy,mz)   = p1*fin(ix,iy,mz-1) &
                           + p2*fin(ix,iy,mz  ) &
                           + p3*hz2(ix,iy,   1) &
                           + p4*hz2(ix,iy,   2)
        end do
      end if
    end do
    ninterp = ninterp + mx*nymax*(ize-izs+1)
  end if
END SUBROUTINE interpolate_zup

!=======================================================================
SUBROUTINE interpolate_xdn (fin, dxdy, fout, nymax)
  USE interp_m
  USE params
  implicit none
  real, dimension(mx,my,mz):: fin, fout
  integer:: nymax, ix, iy, iz
  real   :: dxdy, dixdy, p, q, p1, p2, p3, p4
!.......................................................................
  if (abs(dxdy*(ymg(mytot)-ymg(1))) < 1e-3) then
    do iz=izs,ize
      fout(:,:,iz) = fin(:,:,iz)
    end do
  else
    call mpi_send_x (fin, gx2, 2, hx1, 1)
    dixdy = dxdy/dx
    do iy=1,my
      if (iy+iyoff > nymax) cycle
  !     af  =  q+p*q*(q-p)
  !     bf  =  p-p*q*(q-p)
  !     ad  =  p*q*q*0.5
  !     bd  = -p*q*p*0.5
  !     ac  =  af - bd
  !     bc  =  bf + ad
  !                   ac * fin2( ix, iy, mp0 ) + bc * fin2( ix, iy, mp1 ) - &
  !                   ad * fin2( ix, iy, mm1 ) + bd * fin2( ix, iy, mp2 )
      p  = dixdy*ym(iy)
      p  = p-floor(p)
      q  = 1.-p
      p1 = -p*q*q*0.5           ! ad
      p4 = -p*q*p*0.5           ! bd
      p2 =  q+p*q*(q-p) - p4    ! ac
      p3 =  p-p*q*(q-p) - p1    ! bc
      do iz=izs,ize
        do ix=3,mx-1
          fout(ix,iy,iz) = p1*fin(ix-2,iy,iz) &
                         + p2*fin(ix-1,iy,iz) &
                         + p3*fin(ix  ,iy,iz) &
                         + p4*fin(ix+1,iy,iz)
        end do
        fout( 1,iy,iz)   = p1*gx2(   1,iy,iz) &
                         + p2*gx2(   2,iy,iz) &
                         + p3*fin(   1,iy,iz) &
                         + p4*fin(   2,iy,iz)
        fout( 2,iy,iz)   = p1*gx2(   2,iy,iz) &
                         + p2*fin(   1,iy,iz) &
                         + p3*fin(   2,iy,iz) &
                         + p4*fin(   3,iy,iz)
        fout(mx,iy,iz)   = p1*fin(mx-2,iy,iz) &
                         + p2*fin(mx-1,iy,iz) &
                         + p3*fin(mx  ,iy,iz) &
                         + p4*hx1(   1,iy,iz)
      end do
    end do
    ninterp = ninterp + mx*nymax*(ize-izs+1)
  end if
END SUBROUTINE interpolate_xdn

!=======================================================================
SUBROUTINE interpolate_zdn (fin, dzdy, fout, nymax)
  USE interp_m
  USE params
  implicit none
  real, dimension(mx,my,mz):: fin, fout
  integer:: nymax, ix, iy, iz
  real   :: dzdy, dizdy, p, q, p1, p2, p3, p4
!.......................................................................
  if (abs(dzdy*(ymg(mytot)-ymg(1))) < 1e-3) then
    do iz=izs,ize
      fout(:,:,iz) = fin(:,:,iz)
    end do
  else
    call mpi_send_z (fin, gz2, 2, hz1, 1)
    dizdy = dzdy/dz
    do iy=1,my
      if (iy+iyoff > nymax) cycle
      p  = dizdy*ym(iy)
      p  = p-floor(p)
      q  = 1.-p
      p1 = -p*q*q*0.5
      p4 = -p*q*p*0.5
      p2 =  q+p*q*(q-p) - p4
      p3 =  p-p*q*(q-p) - p1
      do iz=max(izs,3),min(ize,mz-1)
        do ix=1,mx
          fout(ix,iy,iz)   = p1*fin(ix,iy,iz-2) &
                           + p2*fin(ix,iy,iz-1) &
                           + p3*fin(ix,iy,iz  ) &
                           + p4*fin(ix,iy,iz+1)
        end do
      end do
      if (izs==1) then
        do ix=1,mx
          fout(ix,iy, 1)   = p1*gz2(ix,iy,   1) &
                           + p2*gz2(ix,iy,   2) &
                           + p3*fin(ix,iy,   1) &
                           + p4*fin(ix,iy,   2)
          fout(ix,iy, 2)   = p1*gz2(ix,iy,   2) &
                           + p2*fin(ix,iy,   1) &
                           + p3*fin(ix,iy,   2) &
                           + p4*fin(ix,iy,   3)
        end do
      end if
      if (ize==mz) then
        do ix=1,mx
          fout(ix,iy,mz)   = p1*fin(ix,iy,mz-2) &
                           + p2*fin(ix,iy,mz-1) &
                           + p3*fin(ix,iy,mz  ) &
                           + p4*hz1(ix,iy,   1)
        end do
      end if
    end do
    ninterp = ninterp + mx*nymax*(ize-izs+1)
  end if
END SUBROUTINE interpolate_zdn
