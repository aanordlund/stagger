! $Id: corona_cooling.f90,v 1.12 2011/01/20 23:03:53 aake Exp $
!***********************************************************************
MODULE cooling
  USE params
  implicit none
  real, allocatable, dimension(:):: eeav
  real mu, t_top, t_damp, t_chromo, tt_top
  logical do_chromos, do_corona, do_conduction, do_damp, do_top
END MODULE

!***********************************************************************
SUBROUTINE init_cooling (r,e)
  USE params
  USE cooling
  USE units
  real, dimension(mx,my,mz):: r,e
  logical exists
  integer i,iy,ny
  character(len=80):: chromosphere_file

  do_cool = .true.
  do_corona = .true.
  do_chromos = .true.
  do_conduction = .true.
  do_damp = .false.
  t_chromo = 0.1                                        ! default chromospheric equilibration time
  t_damp = 10.                                          ! default windup temperature damping time
  t_top = 0.3                                           ! default upper boundary cooling time
  tt_top = 1e6                                          ! default upper boundary temperature
  mu = 0.8                                              ! default average molecular weight

  call read_cooling
  u_temp = pp2temp*mu*(gamma-1.)
  print *,'tempeture unit =',u_temp

  allocate (eeav(my))
  i = index(from,'.',back=.true.)                       ! index to last period
  chromosphere_file = from(1:i)//'chr'                  ! from.chr
  inquire (file=chromosphere_file,exist=exists)         ! does from.chr exist?
  if (exists) then
    open (54,file=chromosphere_file,status='old',form='unformatted')
    read (54) ny
    if (ny.ne.my) then
      print *,'init_cooling: incompatible dimensions',ny,my,chromosphere_file
      stop
    end if
    read (54) eeav
    close (54)
    print *,'read chromospheric temperature =',eeav*u_temp
  else
    do iy=1,my
      eeav(iy) = sum(e(:,iy,:)/r(:,iy,:))/(mx*mz)
    end do
    open (54,file=from(1:i)//'chr',status='new',form='unformatted')
    write (54) my
    write (54) eeav
    close (54)
    print *,'wrote chromospheric temperature =',eeav*u_temp
  end if
END SUBROUTINE

!***********************************************************************
SUBROUTINE read_cooling
  USE params
  USE cooling
  namelist /cool/do_cool,do_chromos,do_corona,do_conduction,do_damp, &
                 mu, t_chromo, t_damp, t_top
  if (stdin.ge.0) then
    rewind (stdin); read (stdin,cool)
  else
    rewind (stdin); read (*,cool)
  end if
  write (*,cool)
END SUBROUTINE

!***********************************************************************
SUBROUTINE corona_cooling (r,ee,lne,dd,dedt)
  USE params
  USE cooling
  !USE stagger
  USE units
  implicit none
  real, dimension(mx,my,mz):: r,ee,lne,dd,dEdt,scr
  real:: u_cool, Temp, Tquench, quench1, quench2, quench, power
  real:: scr1
  integer:: ix, iy, iz
  character*64:: id= &
   "$Id: corona_cooling.f90,v 1.12 2011/01/20 23:03:53 aake Exp $"

!-----------------------------------------------------------------------
!  Corona cooling, from Schrijver & Zwaan 1999:
!
!  Q = 1.5e-18 T^{-2/3} n_e n_H erg cm^{-3} s^{-1}
!
!  Assume n_e = n_H = rho/(m_atom) = rho/(2e-24)  [Kahn, 1976]
!
!  Q [sim units] = 1.5e-18 (e/rho uTemp)^{-2/3} ((rho urho)/mp)^2 ut/ue
!                = 1.5e-18 ut/ue (urho/mp)^2 
!                  * Temp^{-2/3} rho^2
!-----------------------------------------------------------------------

  !u_cool = 1.5e-18*(u_t/u_e)*(u_r/2e-24)**2            ! Schrijver
  !power = -2./3.

  u_cool = 1.33e-19*(u_t/u_e)*(u_r/2e-24)**2            ! Kahn 1976
  power = -0.5
  Tquench = 4e4

  if (id .ne. '') then 
    print *,id
    print *,'u_cool, u_temp, Tquench = ', u_cool, u_temp, Tquench
    id = ''
  end if

  do iz=1,mz
  do ix=1,mx
  do iy=lb,ub
    Temp = ee(ix,iy,iz)*u_temp
    quench1 = Tquench/Temp
    quench2 = quench1*quench1
    quench = exp(-quench2)
    scr1 = -u_cool*quench*exp(power*alog(Temp))*r(ix,iy,iz)*r(ix,iy,iz)
    dEdt(ix,iy,iz) = dEdt(ix,iy,iz) + scr1
    scr(ix,iy,iz) = scr1/(r(ix,iy,iz)*ee(ix,iy,iz))
  end do
  end do
  end do
!  call fstat1 ('Qcool',scr2,lb,ub)

END SUBROUTINE

!***********************************************************************
SUBROUTINE coolit(r,ee,lne,dd,dedt)
  USE params
  !USE stagger
  USE units
  USE cooling
  implicit none
  real, dimension(mx,my,mz):: r,ee,lne,dd,dEdt,scr
  real:: u_cool, Temp, Tquench, quench1, quench2, quench, power
  real:: rcool, ccool
  integer:: ix, iy, iz, iw

  if (.not. do_cool) return

!-----------------------------------------------------------------------
!  Cooling of the chromoosphere -- insignificant in the corona.  At the
!  corona base density, which is about six orders of magnitude below
!  the photospheric density, the cooling time should have increased 
!  to about 1000 seconds, so by 1e4.  The exponent thus needs to be
!  about 1+4./6.=1.67 (should be 1+4./8. if we go down to rho=3 (e-7).
!-----------------------------------------------------------------------

  if (do_chromos) then
    rcool = sum(r(:,lb,:))/(mx*mz)
    ccool = u_t/t_chromo                ! chromospheric equlibration time
!$omp parallel do private(iy,iz)
    do iz=1,mz
    do iy=lb,ub
      dEdt(:,iy,iz) = dEdt(:,iy,iz) - r(:,iy,iz)* &
          (r(:,iy,iz)*(1./rcool))**1.67*(ee(:,iy,iz)-eeav(iy))*ccool
    end do
    end do
  end if

!-----------------------------------------------------------------------
!  Corona cooling and heat conduction, after initial wind-up
!-----------------------------------------------------------------------
  if (do_corona) then
    call corona_cooling (r,ee,lne,dd,dedt)
    eetop= sum(ee(:,ub,:))/(mx*mz)

!-----------------------------------------------------------------------
!  Keep horizontal temperature fluctuations small during wind-up
!-----------------------------------------------------------------------
  else if (do_damp) then
    do iy=1,my
      eeav(iy)=sum(ee(:,iy,:))/(mx*mz)
    end do
!$omp parallel do private(ix,iy,iz)
    do iz=1,mz
    do iy=1,my
    do ix=1,mx
      dedt(ix,iy,iz)=dedt(ix,iy,iz)-r(ix,iy,iz)*(ee(ix,iy,iz)-eeav(iy))*(u_t/t_damp)
    end do
    end do
    end do
    eetop= tt_top/u_temp
  end if

!-----------------------------------------------------------------------
!  Cooling of the top boundary
!-----------------------------------------------------------------------
  iw=3
  if (do_top) then
    do iy=ub-iw,ub
      ccool = t_top/u_t*(1.0+ub-iy)             !  top boundary cooling time
      dEdt(:,iy,:) = dEdt(:,iy,:) - r(:,iy,:)*(ee(:,iy,:)-eetop)*ccool
    end do
  end if

END SUBROUTINE
