!***********************************************************************
SUBROUTINE transfer (np,n1,tau,s,q,xi,doxi)
!
  USE params
  implicit none
!
  real, dimension(mx,my,mz) :: tau,s,q
  real, dimension(mx,mz)    :: xi
  real, dimension(mx,my)    :: ex0, ex1, ex2, dtau
  real, dimension(mx,my)    :: qplus, qminus
  real, dimension(mx)       :: ex
  real :: taum, a, b, c, dsdt, d2sdt2
  integer :: ix, iy, iz, np, n1
  logical doxi
!
  character(len=80):: id='$Id: transfer_difference_q.f90,v 1.3 2009/03/08 17:24:54 aake Exp $'
!
!  Calculate 1-exp(-tau(l,1,n)) straight or by series expansion,
!  based on max tau(l,1,n).
!
!$omp do private(ix,iy,iz,taum,ex)
  do iz=1,mz
    do ix=1,mx
      if (tau(ix,1,iz).gt.0.1) then
        ex=exp(-tau(ix,1,iz))
      else
        ex=1.-tau(ix,1,iz)*(1.-.5*tau(ix,1,iz)*(1.-.3333*tau(ix,1,iz)))
      endif
      qminus(ix,1) = -s(ix,1,iz)*ex(ix)
    end do
!
!  Compute factors (1a)
!
    do iy=2,np
      dtau(:,iy) = tau(:,iy,iz)-tau(:,iy-1,iz)
    end do
    dtau(:,1) = tau(:,1,iz)
!
!  Incoming rays (2a+3m)
!
    do iy=2,np
      do ix=1,mx
        a = 0.5*dtau(ix,iy)
        b = 1.-a
        c = 1.+a
        qminus(ix,iy) = (qminus(ix,iy-1)*b-(s(ix,iy,iz)-s(ix,iy-1,iz)))/c
      end do
    end do
!
!  Outgoing rays (2a+3m)
!
    do ix=1,mx
      dsdt = (s(ix,np,iz)-s(ix,np-1,iz))/dtau(ix,np)
      d2sdt2 = (dsdt-(s(ix,np-1,iz)-s(ix,np-2,iz))/dtau(ix,np-1)) &
             *2./(dtau(ix,np-1)+dtau(ix,np))
      dsdt = dsdt+0.5*dtau(ix,np)*d2sdt2
      qplus(ix,np) = dsdt+d2sdt2
    end do
    do iy=np,2,-1
      do ix=1,mx
        a = 0.5*dtau(ix,iy)
        b = 1.-a
        c = 1.+a
        qplus(ix,iy-1) = (qplus(ix,iy)*b+(s(ix,iy,iz)-s(ix,iy-1,iz)))/c
      end do
    end do
!
!  Results
!
    do iy=1,np
      q(:,iy,iz) = 0.5*(qplus(:,iy)+qminus(:,iy))
    end do
!
!  Surface intensity.
!
    if (doxi) then
      xi(:,iz)=2.*(1.-ex(:))*(q(:,1,iz)+s(:,1,iz))+s(:,1,iz)*ex(:)**2
    end if

  end do
!$omp end do
!
END
