! $Id: radiation_lookup_linear.f90,v 1.5 2014/12/31 13:36:34 aake Exp $
!***********************************************************************
MODULE cooling
  USE params
  implicit none
  integer, parameter:: mmu=9, mphi=10
  real dphi, form_factor, dtaumin, dtaumax
  integer nmu, nphi, verbose, ny0, iphot, ichrom
  integer mblocks                                                       !maximum number of subdomains considered (per side) to fill the ghost zones
  integer n1, n2
  real y_newton, dy_newton, t_newton, ee_newton, ee_min, t_ee_min, tt_limit
  real ee_chromos, ee_top, h_top, t_top, t_chromos, t_damp, r_top, d_top
  real cdt_cond, c_scale
  real, dimension(mmu):: mu0

  real, pointer, dimension(:,:,:):: rk,rk0,s,q,dtau,qq
  real, dimension(:,:,:), allocatable:: q1,lnr
  real, dimension(:,:,:,:), allocatable:: s0

  logical do_table, do_newton, do_limb, do_fld, do_conduction, do_corona, &
    do_top, do_chromos, do_damp, do_Casiana, do_limit
  character(len=mfile):: intfile
END MODULE

!***********************************************************************
SUBROUTINE init_cooling (r,e)
  USE params
  USE arrays
  USE cooling
  USE units
  implicit none
  real, dimension(mx,my,mz):: r,e
  integer i
  character(len=mfile):: name

  do_cool = .true.
  i = index(file,'.')
  nmu = 0
  nphi = 4
  dphi = 0.
  form_factor = 1.
  dtaumax = 100.
  dtaumin = 0.1
  intfile = name('intensity.dat','int',file)
  !iphot = 300

  do_newton = .false.
  y_newton = -0.3
  dy_newton = 0.05
  t_newton = 0.01
  t_ee_min = 0.005
  ee_newton = -1.
  ee_min = 3.6
  do_limb  = .false.
  verbose = 0
  ny0 = 0
  mblocks = 1                                                           ! default: communicate only nearest neighbour
  do_fld = .false.
  do_conduction = .false.
  do_corona = .false.
  do_top = .true.
  do_damp = .false.
  do_chromos = .false.
  do_Casiana = .false.
  c_scale = 1.
  t_top = 1.
  r_top = 1e-8
  t_chromos = .1
  ee_chromos = 7.
  t_damp = 1.
  d_top = 2.
  iphot = 1
  ichrom = 0                                                            ! default; no chromoshpere even if do_chromos=t
  do_limit = .false.
  tt_limit = 5e6
  cdt_cond = 0.05

  call read_cooling

  if (.not. do_cool) return

  if (mpi_ny > 1) then
    if (master) print*,'ERROR: this RT version needs mpi_ny=1'
    call abort_mpi
  end if

  if (ichrom==0 .and. (.not. do_chromos)) ichrom=iphot
  rk   => scr1
  rk0  => scr2
  s    => scr3
  q    => scr4
  dtau => scr5
  qq   => scr6
END SUBROUTINE

!***********************************************************************
SUBROUTINE read_cooling
  USE params
  USE eos
  USE cooling
  implicit none
  namelist /cool/do_cool, nmu, nphi, dphi, form_factor, dtaumin, dtaumax, intfile, &
    do_newton, y_newton, dy_newton, t_newton, ee_newton, ee_min, t_ee_min, do_limb, mu0, &
    ny0, verbose, mblocks, do_fld, do_conduction, do_corona, do_chromos, t_chromos, r_top, &
    ee_chromos, do_Casiana, do_top, t_top, ee_top, h_top, d_top, do_damp, t_damp, iphot, ichrom, &
    c_scale, do_limit, tt_limit, cdt_cond, timer_verbose
  character(len=mfile):: name, fname
  character(len=mid),save:: &
    id='$Id: radiation_lookup_linear.f90,v 1.5 2014/12/31 13:36:34 aake Exp $'
!.......................................................................
  call print_id(id)

  rewind (stdin); read (stdin,cool)
  if (nmu==0 .and. form_factor==1.) form_factor=0.4
  if (master) write (*,cool)
  if (master) write (*,*) 'mbox, dbox =', mbox, dbox
  if (do_limb) then
    fname = name('limb.dat','lmb',file)
    call mpi_name(fname)
    open (limb_unit, file=trim(fname), &
      form='unformatted', status='unknown')
    write (limb_unit) mx, mz,nmu,nphi
  end if
END SUBROUTINE

!***********************************************************************
SUBROUTINE coolit (r, ee, lne, dd, dedt)
!
!  Radiative cooling
!
  USE params
  !USE variables
  USE units
  USE eos
  USE cooling
  implicit none
  real, dimension(mx,my,mz):: r, ee, lne, dd, dedt
  real xmu(mmu), wmu(mmu)
  integer ix, iy, iz, lmu, lphi, imu, iphi, ibox, n1p, n2p, ny
  real sec, dxdy, dzdy, tanth, qq1, f1, f2, dtau2
  real phi, wphi, womega
  integer lrec, imaxval_mpi, nyr, nzap
  logical flag, do_io, io_flag
  logical, save:: first=.true., first_time=.true.
  external transfer
  logical debug
  real cput(2), void, prof
  integer, save:: nrad=0, nang=0
  character(len=mfile):: name, fname
  integer(kind=8), save:: nsent, ncalls
  integer n2max
!
  if (.not. do_cool) return
  call timer('radiation','start')

  !$omp master
  allocate (q1(mx,my,mz),lnr(mx,my,mz))
  allocate (s0(mx,my,mz,mbox))
  !$omp end master
  !$omp barrier

  do iz=izs,ize
    s0(:,:,iz,:) = 0.
  end do

  if (lb > 1) then                                                      ! if we have ghost zones
    dedt(:,1:lb-1,izs:ize) = 0.                                         ! zap previous dedt there
  end if

  call buffer_reset (nsent)

!-----------------------------------------------------------------------
!  Angular quadrature
!  nmu > 0: Use Gauss integration (mu=1 excluded) with 2*nmu   order accuracy
!  nmu = 0: One vertical ray with weight 0.5
!  nmu < 0: Use Radau integration (mu=1 included) with 2*nmu-1 order accuracy
!-----------------------------------------------------------------------
  if      (nmu .gt. 0) then
    lmu = nmu
    call gausi (lmu,0.,1.,wmu,xmu)
  else if (nmu .eq. 0) then
    lmu = 1
    xmu(1) = 1.
    wmu(1) = 0.5
  else 
    lmu =-nmu
    call radaui(lmu,0.,1.,wmu,xmu)
  end if
  if (mu0(1) .ne. 0.) xmu=mu0

  if (first) then
    call barrier_omp ('rad-first')
    if (omp_master) first = .false.
    if (master) then
      print *,' imu       xmu    theta'
      !           1.....0.000.....00.0
      do imu=1,lmu 
        print '(i4,f10.3,f8.1,f10.3)',imu,xmu(imu),acos(xmu(imu))*180./pi,wmu(imu)
      end do
    end if
  end if

!-------------------------------------------------------------------------------
!  Equation of state and opacity
!-------------------------------------------------------------------------------
  do iz=izs,ize
    lnr(:,:,iz) = log(r(:,:,iz))
  end do
  call lookup (lnr,ee,rk0,s0,my)
  deallocate (lnr)
  call timer('radiation','lookup')

  !$omp barrier
  !$omp single
  n2 = iphot+1
  !$omp end single
  n2p = iphot+1

  do iz=izs,ize
    do iy=iphot+1,my
    do ix=1,mx
      dtau2 = 0.5*dymdn(iy)*(rk0(ix,iy-1,iz)+rk0(ix,iy,iz))
      if (dtau2 < dtaumax) n2p = max(n2p,iy)                                    ! max iy on OMP-thread with dtau < dtaumax
    end do
    end do
  end do

  !$omp critical
  n2 = max(n2p,n2)                                                              ! max n2p over all OMP-threads
  !$omp end critical
  !$omp barrier

  if (ny0==0) then
    call imaxval_subr_mpi (n2)
    ny  = n2                                                                    ! ny==0  -> compute ny
  else
    ny = ny0                                                                    ! should be given as absolute index
  end if
  !ny=my
  if (master .and. verbose == -1) print*,'ny,n2(1) =',ny,n2

  call dumpn (r,'r','rad.dmp',0)
  call dumpn (ee,'ee','rad.dmp',1)
  call dumpn (rk0,'rk0','rad.dmp',1)
  do ibox=1,mbox
    call dumpn (s0(:,:,:,ibox),'s','rad.dmp',1)
  end do

  do iz=izs,ize
    qq(:,:,iz) = 0.
  end do

  call dumpn (rk0,'rk0','trf.dmp',0)
  do imu=1,lmu
    if (nmu .eq. 0 .and. imu .eq. 1) then
      lphi = 1 
    else if (nmu .lt. 0 .and. imu .eq. lmu) then
      lphi = 1 
    else
      lphi = nphi
    end if
    tanth = tan(acos(xmu(imu)))
    wphi = 4.*pi/lphi

    do iphi=1,lphi
      n_omega = n_omega + 1
      phi = (iphi-1)*2.*pi/lphi + dphi*t
      dxdy = tanth*cos(phi)
      dzdy = tanth*sin(phi)
      if (master .and. verbose==1 .and. isubstep==1) &
        print '(a,3i5,2e15.7)',' imu,iphi,ny,dxdy,dzdy=', &
                                 imu,iphi,ny,dxdy,dzdy

      call trnslt (rk0, rk, dxdy, dzdy, .false., ny, mblocks, verbose)

      nzap = ny+1
      if (do_dump) rk(:,nzap:my,izs:ize) = 0.
      call dumpn (rk,'rk','trf.dmp',1)

      do iz=izs,ize
        q1(:,:,iz) = 0.
      end do
      n2max = 0
      do ibox=1,mbox
        !$omp barrier
        !$omp single
        n1 = iphot+1
        n2 = iphot+1
        !$omp end single
        n1p = iphot+1
        n2p = iphot+1
        if (ibox.eq.1) then
          f1 = (0.5/xmu(imu))
          do iz=izs,ize
            dtau(:,iphot,iz) = dym(1)*rk(:,iphot,iz)/xmu(imu)
            do iy=iphot+1,ny
            do ix=1,mx
              dtau(ix,iy,iz) = f1*dymdn(iy)*(rk(ix,iy-1,iz)+rk(ix,iy,iz))
              if (dtau(ix,iy,iz) < dtaumin) n1p = max(n1p,iy)
              if (dtau(ix,iy,iz) < dtaumax) n2p = max(n2p,iy)
            end do
            end do
          end do
        else
          do iz=izs,ize
          do iy=iphot,ny
          do ix=1,mx
            dtau(ix,iy,iz) = (10.**dbox)*dtau(ix,iy,iz)
            if (dtau(ix,iy,iz) < dtaumin) n1p = max(n1p,iy)
            if (dtau(ix,iy,iz) < dtaumax) n2p = max(n2p,iy)
          end do
          end do
          end do
        end if

        !$omp critical
        n1 = max(n1p,n1)
        !$omp end critical
        call imaxval_subr_mpi (n1)
        !n1 = imaxval_mpi (n1)

        if (ny0==0) then
          !$omp critical
          n2 = max(n2p,n2)
          !$omp end critical
          call imaxval_subr_mpi (n2)
          !n2 = imaxval_mpi (n2)
        else
          n2 = ny0
        end if

        nzap = ny+1
        if (master .and. verbose == -1) print*,'ny,n1,n2,nzap(2) =',ny,n1,n2,nzap
        if (do_dump) dtau(:,nzap:my,izs:ize) = 0.
        call dumpn (dtau,'dtau','trf.dmp',1)

        nrad = nrad + n2
        nang = nang + 1
        if (master .and. verbose==2 .and. isubstep==1) then
          nyr = (nrad+0.5)/nang
          print '(a,7i5,1p,(2e15.7))',' imu,iphi,ibox,ny,n1,n2,nyr,dxdy,dzdy=', &
                                       imu,iphi,ibox,ny,n1,n2,nyr,dxdy,dzdy
        end if

!-----------------------------------------------------------------------
!  Source function
!-----------------------------------------------------------------------
        call timer('radiation','dtau')
        call trnslt (s0(:,:,:,ibox), s, dxdy, dzdy, .false., n2, mblocks, verbose)

        if (do_dump) s(:,nzap:my,izs:ize) = 0.
        call dumpn (s,'s','trf.dmp',1)

!-----------------------------------------------------------------------
!  Raditative transfer equation
!-----------------------------------------------------------------------
        flag = do_limb .and. do_io(t+dt,tscr,iscr+iscr0,nscr)                         ! limb darkening every scratch
        io_flag = do_io(t+dt,tscr ,iscr +iscr0 ,nscr ) .or. &                         ! scratch or
                  do_io(t+dt,tsnap,isnap+isnap0,nsnap)                                ! snapshot 
        flag = flag .or. (xmu(imu)==1. .and. io_flag)                                 ! every i/o but only mu=1
        flag = flag .and. isubstep.eq.1 .and. ibox.eq.1                               ! and only for first substep and bin

        if (master .and. verbose == -1) print*,'call transfer:',mpi_rank,n2,n1,ibox
        n2max = max(n2,n2max)
        call transfer (n2,n1,dtau,s,q,surface_int,flag)
        call timer('radiation','transfer')
        if (flag) then
          !print'(1x,a,4i4,l3,1p2e12.3)','MARK',isubstep,ibox,imu,iphi,io_flag,xmu(imu),surface_int(1,1)
          call barrier_omp('limb')
          if (do_limb .and. omp_master) then
            write(limb_unit) t,imu,iphi,xmu(imu),phi,surface_int
            print *,omp_mythread,isubstep,ibox,imu,iphi
          end if
        end if

        do iz=izs,ize
          do iy=iphot+1,n2-1
            f2 = xmu(imu)/(ym(iy+1)-ym(iy-1))
            q(:,iy,iz) = q(:,iy,iz)*(dtau(:,iy+1,iz)+dtau(:,iy,iz))*f2
          end do
          f2 = xmu(imu)/(ym(2)-ym(1))
          q(:, iphot,iz) = q(:,iphot,iz)*dtau(:,2,iz)*f2
          q(:,n2,iz) = 0.0
        end do

        if (do_dump) q(:,nzap:my,izs:ize) = 0.
        call dumpn(q,'q1','trf.dmp',1)

        f1 = wmu(imu)*wphi*form_factor
        do iz=izs,ize
        do iy=iphot,n2
        do ix=1,mx
          q1(ix,iy,iz) = q1(ix,iy,iz) + f1*q(ix,iy,iz)
        end do
        end do
        end do
        call timer('radiation','Q')
      end do ! ibox=1,nbox
      call trnslt (q1, q, -dxdy, -dzdy, .false., n2max, mblocks, verbose)
      do iz=izs,ize
        qq(:,1:n2max,iz) = qq(:,1:n2max,iz) + q(:,1:n2max,iz)
      end do
    end do ! iphi=1,lphi
  end do ! imu=1,lmu

  do iz=izs,ize
    dedt(:,:,iz) = dedt(:,:,iz) + qq(:,:,iz)
  end do

  call print_trace('radiation',0,'dump1')
  call dumpn(qq,'qq','rad.dmp',1)
  call dumpn(qq,'qq','qq.dmp',1)
  call print_trace('radiation',0,'dump2')
  ! call dumps(qq,'Qrad','fluxes.dat',1)

  call print_trace('radiation',0,'radiative_flux')
  call radiative_flux (qq,q)

  call print_trace('radiation',0,'newton')
  if (do_newton) then
    if (ee_newton > 0) eetop = ee_newton
    do iz=izs,ize
    do iy=iphot,my
      prof = exp(min((y_newton-ym(iy))/dy_newton,70.))
      prof = prof/(1.+prof)
      do ix=1,mx
        dedt(ix,iy,iz) = dedt(ix,iy,iz) - r(ix,iy,iz)*(ee(ix,iy,iz)-eetop)/t_newton*prof &
                                        - r(ix,iy,iz)*min(0.,ee(ix,iy,iz)-ee_min)/t_ee_min
      end do
    end do
    end do
  end if

  !$omp barrier
  !$omp master
  deallocate (q1,lnr,s0)
  !$omp end master

  if (first_time.and.mu0(1).ne.0.) call buffer_count
  if (master.and.first_time) & 
    print*,'chunks sent/step =',nsent/(real(mxtot)*real(mytot)*real(mztot)), ncalls
  first_time=.false.

  call timer('radiation','general')
  call print_trace('radiation',0,'END')
END SUBROUTINE
