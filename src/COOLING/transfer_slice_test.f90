!  $Id: transfer_slice_test.f90,v 1.2 2004/07/23 08:45:39 aake Exp $
!***********************************************************************
PROGRAM transfer_slice_test
!-----------------------------------------------------------------------
!  Compare transfer solutions with 21 and 2001 points
!-----------------------------------------------------------------------
  call transfer_slice_using (21)
  call transfer_slice_using (41)
  call transfer_slice_using (2001)
END

!***********************************************************************
SUBROUTINE transfer_slice_using (my)
!-----------------------------------------------------------------------
!  Compare transfer solutions with 'my' points
!-----------------------------------------------------------------------
  implicit none
  integer, parameter:: mx=500, mpr=10
  integer my, iy
  real, parameter:: taumin=1e-4, taumax=1e4
  real, dimension(mx,my) :: tau, S

!-----------------------------------------------------------------------
!  Optical depth scale, logarithmic
!-----------------------------------------------------------------------
  tau(1,:) = 10.**(/(alog10(taumin)+alog10(taumax/taumin)*(iy-1.)/(my-1.), iy=1,my)/)
  tau = spread(tau(1,:), dim=1, ncopies=mx)

!-----------------------------------------------------------------------
!  Constant, linear, and quadratic trial source functions
!-----------------------------------------------------------------------
  S = 1.
  call transfer_slice_compare ('S = 1', mx, my, tau, S, mpr)
  S = tau
  call transfer_slice_compare ('S = tau', mx, my, tau, S, mpr)
  S = tau**2
  call transfer_slice_compare ('S = tau**2', mx, my, tau, S, mpr)
  S = tau**3
  call transfer_slice_compare ('S = tau**3', mx, my, tau, S, mpr)
END

!***********************************************************************
SUBROUTINE transfer_slice_compare (label, mx, my, tau, S, mpr)
!-----------------------------------------------------------------------
!  Compare transfer solutions with 'my' points
!-----------------------------------------------------------------------
  implicit none
  integer:: mx, my, mpr
  character(len=*):: label
  real, dimension(mx,my) :: tau, dtau, S, Q1, Q2
  real, dimension(mx)    :: i1, i2
  integer iy
  real times(2), cpu1, cpu2, dtime

  dtau(:,2:my) = tau(:,2:my)-tau(:,1:my-1)
  dtau(:,1) = tau(:,1)

  cpu1 = dtime(times)  
  call transfer_slice_integral (mx, my, dtau, S, Q1, i1)
  cpu1 = 1e6*dtime(times)  
  call transfer_slice_pwc      (mx, my, dtau, S, Q2, i2)
  cpu2 = 1e6*dtime(times)  

  print '(1x,a,2f8.3,2f10.6,i7,a)', label, cpu1/(mx*my), cpu2/(mx*my), i1(1), i2(1), my, ' points'
  do iy=1,my,max(my/mpr,1)
    print *, iy, tau(1,iy), q1(1,iy), q2(1,iy)
  end do
END
