! $Id: newton_cooling.f90,v 1.8 2008/09/06 22:03:06 aake Exp $
MODULE cooling
  real t_cool,ee_cool
END MODULE

SUBROUTINE init_cooling (r,e)
  USE params
  USE cooling
  real, dimension(mx,my,mz):: r,e
  do_cool = .true.
  t_cool = 10.
  ee_cool = 0.
  call read_cooling
END SUBROUTINE

SUBROUTINE read_cooling
  USE params
  USE cooling
  namelist /cool/do_cool,t_cool,ee_cool
  rewind (stdin); read (stdin,cool)
  if (master) write (*,cool)
END SUBROUTINE

SUBROUTINE coolit (r,ee,lne,dd,dedt)
  USE params
  USE cooling
  implicit none
  real, dimension(mx,my,mz) :: r, ee, lne, dd, dedt
  logical omp_in_parallel

  if (omp_in_parallel()) then
    call coolit_omp (r,ee,lne,dd,dedt)
  else
    !$omp parallel
    call coolit_omp (r,ee,lne,dd,dedt)
    !$omp end parallel
  end if
END SUBROUTINE

SUBROUTINE coolit_omp (r,ee,lne,dd,dedt)
  USE params
  USE cooling
  implicit none
  real, dimension(mx,my,mz) :: r, ee, lne, dd, dedt
  character(len=mid):: id='$Id: newton_cooling.f90,v 1.8 2008/09/06 22:03:06 aake Exp $'
  integer iz

  if (.not. do_cool) return
  call print_id (id)

  do iz=izs,ize
    dedt(:,:,iz) = dedt(:,:,iz) - r(:,:,iz)*(ee(:,:,iz)-ee_cool)*(1./t_cool)
  end do

  return
END SUBROUTINE

