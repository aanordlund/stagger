! $Id: transfer1_integral.f90,v 1.9 2007/01/12 21:26:33 aake Exp $
!***********************************************************************
SUBROUTINE transfer1 (n, dtau, s, q, qfw, qre, afw, are)
  implicit none
  integer n
  real, dimension(n) :: dtau, s, q, qfw, qre, afw, are
!
!  Input: n, dtau(2:n), s(1:n), qfw(1), qre(n)
! Output: q(2:n-1), qfw(n-1), qre(2)
!
! The input is the source functions and opacities at all points, the net
! forward intensity (I-S) at the 1st point and the net reverse intensity
! at the last point.  As a result, we get the net bi-directional intensity
! everywhere, and also the outgoing forward and reverse intensities in points
! number 2 and np-1.
!
  real, dimension(n) :: ex0, ex1, ex2, ds1dtau, dsdtau, d2sdtau2
  real ctau
  integer i
  character(len=80):: id='$Id: transfer1_integral.f90,v 1.9 2007/01/12 21:26:33 aake Exp $'
!-----------------------------------------------------------------------
!  call print_id (id)
!
  do i=2,n
    ex0(i) = exp(-dtau(i))
    ex1(i) = 1.-ex0(i)
    ex2(i) = ex1(i)-dtau(i)*ex0(i)
    ds1dtau(i) = (s(i)-s(i-1))/dtau(i)
  end do
!
!  dS/dtau and d2S/dtau2 (3a+5m+1d)
!
  do i=2,n-1
    ctau = 1./(dtau(i+1)+dtau(i))
    dsdtau(i) = (ds1dtau(i+1)*dtau(i)+ds1dtau(i)*dtau(i+1))*ctau
    d2sdtau2(i) = (ds1dtau(i+1)-ds1dtau(i))*2.*ctau
  end do
!
!  Forward rays (2a+3m)
!
  afw(1) = 1.
  do i=2,n-1
    qfw(i) = qfw(i-1)*ex0(  i) - dsdtau(i)*ex1(i  ) + d2sdtau2(i)*ex2(i  )
    afw(i)=afw(i-1)*ex0(i)
  end do
!
!  Reverse rays (2a+3m)
!
  are(n) = 1.
  do i=n-1,2,-1
    qre(i) = qre(i+1)*ex0(i+1) + dsdtau(i)*ex1(i+1) + d2sdtau2(i)*ex2(i+1)
    q(i) = 0.5*(qre(i)+qfw(i))
    are(i)=are(i+1)*ex0(i+1)
  end do
!  print 1,(i,ex0(i),ex1(i),dsdtau(i),d2sdtau2(i),qfw(i),qre(i),afw(i),are(i), i=1,n)
!1 format(i4,8f14.6)
END

!***********************************************************************
SUBROUTINE test_transfer1
!
!  This test allows computing the intensity, for a case where the exact
!  surface result is known to be 4, at varying resolutions (change dlgtau).
!
!-----------------------------------------------------------------------
  implicit none
  integer, parameter:: n=103
  real, parameter:: lgtaumin=-5., dlgtau=0.1
  real, dimension(n):: d, s, q, qfw, qre, dtau, tau, afw, are
  integer i
  real tau0, tau1, qa
!-----------------------------------------------------------------------
  tau0 = 10.**lgtaumin
  do i=1,n
    tau1 = 10.**(lgtaumin+dlgtau*(i-1))
    tau(i)  = tau1-tau0
    dtau(i) = tau1*(1.-10.**(-dlgtau))
    s(i) = 1.+tau(i)+tau(i)**2
  end do
  qfw(1) = -s(1)                                                        ! surface bdry condition
  qre(n) = 1+2.*tau(n)+2.                                               ! bottom bdry
  call transfer1 (n, dtau, s, q, qfw, qre, afw, are)
  print *,'  i         tau           irev           ifwd             qq             qa'
  do i=2,n-1,10
    if (tau(i) < 60.) then
      qa = 2.-cosh(tau(i))+sinh(tau(i))
    else
      qa = 2.
    end if
    print '(i4,5(1pe15.6))',i,tau(i),qre(i)+s(i),qfw(i)+s(i), &
      0.5*(qre(i)+qfw(i)), qa
  end do
END

!***********************************************************************
SUBROUTINE test_transfer2 
!
!  As this test demonstrates, one may solve a periodic problem by first 
!  solving the non-periodic problem with vanishing boundary values and
!  then taking the integrated values across one period, divided by the 
!  local absorption factor across a period, as the boundary value.
!
!  Furthermore, instead of solving the RT a second time, one can save
!  the transmission factors the first time, and just add the boundary
!  value times that in a loop.
!
!  The solutions are compared to the analytical solution for constant
!  opacity and sinusoidally varying source function (1+cos(k tau));
!
!    q(tau)= -k^2 cos(k tau) / (1 + k^2)
!
!-----------------------------------------------------------------------
  implicit none
  integer, parameter:: n=102
  real, parameter:: dtau0=0.01, pi=3.14159265
  real, dimension(n):: d, s, q, qfw, qre, dtau, tau, afw, are
  integer i, i0
  real tau0, tau1, k
!-----------------------------------------------------------------------
  k = 2.*pi/(dtau0*(n-2.))
  print *,' i0        qrev           qfwd             qq             qa'
  do i0=1,n,10
    do i=1,n
      dtau(i) = dtau0
      tau(i) = (i-i0)*dtau0
      s(i) = 1.+cos(k*tau(i))
    end do
    qfw(1) = 0.
    qre(n) = 0.
    call transfer1 (n, dtau, s, q, qfw, qre, afw, are)                  ! results with zero bdry values

    qfw(1) = qfw(n-1)/(1.-exp(-dtau0*(n-2.)))                           ! one period divided by abs factor
    do i=2,n-1
      qfw(i) = qfw(i) + qfw(1)*afw(i)                                   ! spread that forward
    end do
    qre(n) = qre(  2)/(1.-exp(-dtau0*(n-2.)))                           ! ditto for reverse direction
    do i=n-1,2,-1
      qre(i) = qre(i) + qre(n)*are(i)
    end do
    print '(i4,4(1pe15.6))',i0,qre(2),qfw(n-1), &
      0.5*(qre(n-1)+qfw(n-1)),-k**2*cos(k*tau(n-1))/(1.+k**2)           ! print that solution

    call transfer1 (n, dtau, s, q, qfw, qre, afw, are)                  ! obtain the RT solution
    print '(i4,4(1pe15.6))',i0,qre(2),qfw(n-1), &                       ! print comparison
      0.5*(qre(n-1)+qfw(n-1)),-k**2*cos(k*tau(n-1))/(1.+k**2)
  end do
  print *,afw(1:2),are(n-1:n)                                           ! check proper positioning
END

!***********************************************************************
SUBROUTINE test_transfer
  call test_transfer1
  call test_transfer2
END
