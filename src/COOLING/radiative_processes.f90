MODULE thermal_equilibrium


implicit none

integer row,column,isnap_Lyman
character (len=80) Lymanfile,lifefile
real, dimension(51,26) :: Lymantbl
real, dimension(7,2) :: lifetbl
integer grab

CONTAINS


!****************************************************************************
SUBROUTINE get_Lyman_luminosity(i,Rti,L_Lyman,progm)

USE explosions, void=>verbose
USE params


implicit none

real, allocatable,dimension(:) :: a
real,dimension(13) :: stmass
integer i,j,pos1,pos2
real Rti,L_Lyman,progm,lifetime,a0,a1

stmass=(/ 1., 2.,3.,4.,5.,7.,15.,20.,25.,40.,60.,85.,120./)

allocate(a(13))
a(:)=0.
where(stmass(:) .le. progm) a=stmass(:)

do j=1,13
   if (stmass(j).eq.maxval(a)) exit
end do 
deallocate(a)

if (progm .gt.1 .and. progm .lt.8) then
   a1 = (lifetbl(j+1,2)-lifetbl(j,2))/ &
        (lifetbl(j+1,1)-lifetbl(j,1))
   a0 = lifetbl(j,2)-a1*lifetbl(j,1)
   lifetime = a1*progm+a0               ! life time of star in Myrs
   Rti=Rti/lifetime                     !get current rate of life of the star   
endif


pos1=int(2*j-1)
pos2=int(2*(j+1)-1)


call interpolation_lyman(i,Rti,Lymantbl,row,column,1,pos1,pos2,L_Lyman,progm,j)

END SUBROUTINE
!***********************************************************************
SUBROUTINE interpolation_lyman(i,Rti,tbl,row,column,columnsFromAge,pos1,pos2,x,progm,poslow)

USE explosions, void=>verbose

integer i,j,pos1,pos2,pos3,columnsFromAge,row,column,poslow
real Rti,x,progm

double precision m1,m2,m3,n1,n2,n3,interpol1,interpol2

double precision,allocatable, dimension(:) :: Rlife1,Rlife2,a
real, dimension(row,column) :: tbl

                
  allocate(Rlife1(row))
  allocate(a(row))
  Rlife1=tbl(:,pos1)


  a=-1.
  where(Rlife1.le.Rti) a=Rlife1
  do j=1,row
     if (Rlife1(j).eq.maxval(a)) then
        exit
     endif
     if(j.eq.row) then
        write(*,*) '1fallo',Rlife1(j),a       
         write(*,*) 'What in the Earth1 are u doing HERE'
     endif      
  end do
  m1=(tbl(j+1,pos1+columnsFromAge)-tbl(j,pos1+columnsFromAge))/(tbl(j+1,pos1)&
     -tbl(j,pos1))
        
  n1=tbl(j,pos1+columnsFromAge)-m1*tbl(j,pos1)
  interpol1=m1*Rti+n1
  deallocate(Rlife1)

!Second interpolation in the table

  
  allocate(Rlife2(row))
  Rlife2=tbl(:,pos2)
  a=-1.  
  where(Rlife2.le.Rti) a=Rlife2
  do j=1,row
     if (Rlife2(j).eq.maxval(a))  then 
        exit
     endif              
     if(j.eq.row) then 
       write(*,*) '2fallo:',Rlife2(j),a
        write(*,*) 'What in the Earth2 are u doing HERE'
     endif
  end do
  m2=(tbl(j+1,pos2+columnsFromAge)-tbl(j,pos2+columnsFromAge))/(tbl(j+1,pos2)&
     -tbl(j,pos2))
        
  n2=tbl(j,pos2+columnsFromAge)-m2*tbl(j,pos2)
  interpol2=m2*Rti+n2
  deallocate(Rlife2)
  deallocate(a)

!Final interpolation depending on the initial mass!

  if (progm .gt. 8) then
     pos3=int(hms(i,13))
     m3=(interpol2-interpol1)/(genevelifetbl(pos3+1,1)-genevelifetbl(pos3,1))
     n3=interpol1-m3*genevelifetbl(pos3,1)
     x=m3*hms(i,2) + n3    
  else 
     m3=(interpol2-interpol1)/(lifetbl(poslow+1,1)-lifetbl(poslow,1))
     n3=interpol1-m3*lifetbl(poslow,1)
     x=m3*progm + n3    
  endif

END subroutine
!**************************************************************************
END MODULE
!***************************************************************************
SUBROUTINE init_Lyman

USE params
USE thermal_equilibrium
USE units
implicit none

integer ::i,j,k
grab=0
row=51
column=26
isnap_Lyman=isnap
Lymanfile='../src/FORCING/YIELDS/Lyman.tbl'
lifefile='../src/FORCING/YIELDS/life_lowmass.tbl' 


open(1,file=Lymanfile,status='old', form='formatted')
do k=1,row
   read(1,*) ((Lymantbl(i,j), i=k,k), j=1,column)
end do
close(1)

!do i=1,51
! Lymantbl(i,:)=Lymantbl(i,:)*(ceu/ctu)
!enddo

open(1,file=lifefile, status='old', form='formatted')
do k=1,7
   read(1,*) ((lifetbl(i,j), i=k,k), j=1,2)
end do
close(1)   


END SUBROUTINE
!***************************************************************************
SUBROUTINE  equilibrium

USE explosions, void=>verbose
USE params
USE units
USE thermal_equilibrium

implicit none
character(len=mfile) fname,name
integer:: ldist,mdist,ndist,i,mlms_1
real ti,Rti,progm,L_Lyman


if (isnap .NE. isnap_Lyman) grab=0
isnap_Lyman=isnap


if (grab .eq. 0) then 
   mlms_1=0
   if (mlms.gt.0) then    !Count number of lms with masses gt 1. 
                          !(the ones that we'll record together with massive stars)
      do i=1,mlms 
         if (lms(i,7) .gt. 1) mlms_1=mlms_1+1
      enddo
   endif   
   fname = name('Lyman.dat','Lyman',file)
   open (11,file=fname, position='append', form='unformatted', status='unknown')
   write (11) mhms+mlms_1,t
endif

!Loop over massive stars.
do i=1,mhms
   ldist=(hms(i,20)-xm(1))/dx+.5 !get stellar position 
   mdist=(hms(i,21)-ym(1))/dy+.5
   ndist=(hms(i,22)-zm(1))/dz+.5
   ldist=mod(ldist,mx)+1
   mdist=mod(mdist,my)+1
   ndist=mod(ndist,mz)+1
   
   ti =abs(t-(hms(i,1)-hms(i,14))) !get current rate of life of the star
   Rti=ti/hms(i,14)
   progm=hms(i,2)  !Initial mass
   call get_Lyman_luminosity(i,Rti,L_Lyman,progm)
   if (idbg .gt.0) then
      print *,"Massive stars: Masa,Rti,L_Lyman",progm,Rti,L_Lyman*(ceu/ctu)
      print *,"location:",ldist,mdist,ndist
   endif   
   if (grab .eq. 0) then
      write (11) progm,L_Lyman
   endif   
enddo

!Loop over low mass stars.
do i=1,mlms
   ldist=(lms(i,1)-xm(1))/dx+.5 !get stellar position 
   mdist=(lms(i,2)-ym(1))/dy+.5
   ndist=(lms(i,3)-zm(1))/dz+.5
   ldist=mod(ldist,mx)+1
   mdist=mod(mdist,my)+1
   ndist=mod(ndist,mz)+1

   progm=lms(i,7)
   if (progm .gt. 1.) then 
      Rti=t        
      call get_Lyman_luminosity(i,Rti,L_Lyman,progm)
      if (grab .eq. 0) then
         write (11) progm,L_Lyman        
      endif
      if (idbg .gt.0) then
         print *,"low mass stars: Masa,Rti,L_Lyman",progm,Rti,L_Lyman*(ceu/ctu)
      endif
   endif
enddo
if (grab .eq. 0) then
   close(11)
   grab=1        
endif

end SUBROUTINE

!***************************************************************************
