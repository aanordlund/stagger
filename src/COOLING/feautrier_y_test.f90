!
!  Test program for Feautrier solution in the y-direction
!
 USE params

 implicit none
 real, allocatable, dimension(:,:,:):: s, tau, q, q0
 real, allocatable, dimension(:,:):: xi
 integer :: n1, iy, niter
 real :: logtaumin, logtaumax, dlogtau, logtau, cput, cpu(2), dtime
 logical doxi

 mx=125
 my=125
 mz=125
 doxi=.true.

 allocate (s(mx,my,mz), tau(mx,my,mz), q(mx,my,mz), q0(mx,my,mz), xi(mx,mz))

 s=1.
 logtaumin=-4.
 logtaumax=3.
 dlogtau=(logtaumax-logtaumin)/(my-1)
 do iy=1,my
   logtau=logtaumin+dlogtau*(iy-1)
   tau(:,iy,:)=10.**logtau
   !s(:,iy,:)=tau(:,iy,:)+0.01*tau(:,iy,:)**2*(1.+cos(logtau*pi))
   s(:,iy,:)=tau(:,iy,:)
   q0(:,iy,:)=0.5*exp(-tau(:,iy,:))
 end do

 n1 = 30
 cput=dtime(cpu)
 call radiation (my,n1,tau,s,q,xi,doxi)
 cput=dtime(cpu)
 niter=1.+1./(cput+1e-2)                ! number of iterations for 1 second CPU

 cput=dtime(cpu)
 call radiation (my,n1,tau,s,q,xi,doxi)
 cput=dtime(cpu)

 print *,' iy  log(tau)         S           P           Q          err'
 do iy=1,my
   print '(i4,4e12.4,f12.6)',iy, alog10(tau(1,iy,1)), s(1,iy,1), s(1,iy,1)+q(1,iy,1), q(1,iy,1), q(1,iy,1)-q0(1,iy,1)
 end do
 print *,'cpu =',cput
 print *,'mus/pt =',cput*1e6/(mx*my*mz*niter)

 END

