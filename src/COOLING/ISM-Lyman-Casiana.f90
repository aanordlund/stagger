! $Id: ISM-Lyman-Casiana.f90,v 1.4 2006/07/05 20:41:48 aake Exp $
!***********************************************************************
MODULE cooling

! The units of lamda are energy per unit volume and time, or cdu*cvu^2/ctu,
! which the computed, dimensional value should be divided by.  But since
! a multiplicative factor of cdu^2 should also be applied, the computed
! value should be divided by cvu^2/(ctu*cdu), or multiplied by ctu*cdu/cvu^2.

  USE units

  logical do_table, do_power, do_heat, do_extraterm
  character (len=mfile) coolH2file,tablefile
  real a0,b0,a1,b1,tab0,Tlower,G0,G1,rho_crit
  integer ma, mb, turnon, verbose
END MODULE

!***********************************************************************
SUBROUTINE init_cooling (r,ee)
  USE params
  USE cooling
  implicit none
  real, dimension(mx,my,mz):: r,ee
  character(len=mid):: id='$Id: ISM-Lyman-Casiana.f90,v 1.4 2006/07/05 20:41:48 aake Exp $'

  call print_trace (id, dbg_cool, 'BEGIN')

  if (do_trace) print *,'init_cooling: read'
  do_cool = .true.
  do_table = .false.
  do_power = .false.
  do_heat = .true.
  do_extraterm=.false.
  G0=1.e-24
  G1=1.e-25
  Tlower = 100.
  rho_crit = 0.
  ma = mx ; mb = my
  turnon=-1

  call init_lyman
  call init_radiation
  call read_cooling
  call test_cooling

  call print_trace (id, dbg_cool, 'END')
END SUBROUTINE

!***********************************************************************
SUBROUTINE read_cooling
  USE params
  USE cooling
  implicit none
  namelist /cool/do_cool,do_table,do_power,Tlower,ma,mb,coolH2file,turnon, &
       do_heat,do_extraterm,G0,G1,rho_crit,verbose

  if (stdin.ge.0) then
    rewind (stdin); read (stdin,cool)
  else
    read (*,cool)
  end if
  write (*,cool)

  call read_lyman
  call read_radiation
END SUBROUTINE

!***********************************************************************
SUBROUTINE coolit_real (na,nb,r,ee,lne,lambda)

  USE params
  USE eos
  USE units
  USE cooling
  implicit none
  integer ix,iy,na,nb
  real, dimension(na,nb):: r,ee,lne
  real, dimension(na,nb)::lambda
!  double precision, dimension(na,nb)::lambda
  real temp
  real qq
!  double precision qq
  
  logical, save:: first_time=.true.
  real, parameter:: &

    TEm=1.e2, &
    TE0=1.e3, &
    TE1=10964.781e0, &
    TE2=19952.632e0, & 
    TE3=29512.078e0, & 
    TE4=39810.708e0, & 
    TE5=69183.121e0, &
    TE6=125892.51e0, &
    TE7=251188.7e0, &
    TE8=501187.01e0, &
    TE9=891250.55e0, &
    TE10=1.412537e6, & 
    TE11=2.511887e6, &
    TE12=6.309576e6, &
    TE13=1.e7, &
    TE14=1.5848925e7, &
    TE15=1.e9, &

    Cm=2.,   &
    C0=2.596,   &
    C1=3.846, &
    C2=-0.1764, &
    C3=1.00001, &
    C4=1.6666, &
    C5=1.15384, &
    C6=0.1, &
    C7= -3.933, &
    C8= 0.72, &
    C9= -0.65,  &
    C10= 0.4, &
    C11= -1.25, &
    C12= 0., &
    C13= -0.4,  &
    C14= 0.43333, &
    
    Hm=4.98282e+15, &
    H0=3.2473989e-14/4.e-28, &
    H1=2.894e-19/4.e-28,  &
    H2=5.739e-2/4.e-28,  &
    H3=3.1620277e-07/4.e-28,  &
    H4=2.7123742e-10/4.e-28,  &
    H5=8.2298862e-08/4.e-28,  &   
    H6=1.9497e-02/4.e-28,  &
    H7=1.1749e+20       ,  &
    H8=3.5155e-7/4.e-28,  &
    H9=4.982757e+1/4.e-28,  &
    H10=1.7379e-5/4.e-28,  &   
    H11=6.309e+5/4.e-28,  &
    H12=1.99525e-03/4.e-28,  &
    H13=1.2589e-00/4.e-28,  &
    H14=1.2589e-06/4.e-28 
   
   
   real, parameter:: scale=(ctu/clu)**2*(ctu*cdu)
!  double precision, parameter:: scale=(ctu/clu)**2*(ctu*cdu)
!   double precision, parameter:: scale=(ctu**3/clu**2)/cdu

  if (first_time) then
    first_time=.false.
    print *,'scale=',scale
    print *,'TE1,tlow,TE2,H1  ',TE1,Tlower,TE2,TE3,H1
  end if

!XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXna=mx nb=my
  do iy=1,nb
  do ix=1,na
    temp = utemp*ee(ix,iy)*1.d0
    if      (temp .le. Tlower) then
      qq = 0.
    else if (temp .le. TE0) then
      qq = scale*(Hm*temp**Cm)
    else if (temp .le. TE1) then
      qq = scale*(H0*temp**C0)
    else if (temp .le. TE2) then
      qq = scale*(H1*temp**C1)
    else if (temp .le. TE3) then
      qq = scale*(H2*temp**C2)
    else if (temp .le. TE4) then
      qq = scale*(H3*temp**C3)
    else if (temp .le. TE5) then
      qq = scale*(H4*temp**C4)
    else if (temp .le. TE6) then
      qq = scale*(H5*temp**C5)
    else if (temp .le. TE7) then
      qq = scale*(H6*temp**C6)
    else if (temp .le. TE8) then
      qq = (scale/4e-28)*(H7*temp**C7)
    else if (temp .le. TE9) then
      qq = scale*(H8*temp**C8)
    else if (temp .le. TE10) then
      qq = scale*(H9*temp**C9)
    else if (temp .le. TE11) then
      qq = scale*(H10*temp**C10)
    else if (temp .le. TE12) then
      qq = scale*(H11*temp**C11)
    else if (temp .le. TE13) then
      qq = scale*(H12*temp**C12)
    else if (temp .le. TE14) then
      qq = scale*(H13*temp**C13)
    else
      qq = scale*(H14*temp**C14)
    end if
    lambda(ix,iy)=qq
  end do
  end do

END SUBROUTINE

!***********************************************************************
SUBROUTINE coolit (r,ee,lne,dd,dedt)
!
!  Wrapper routine.  Computes optically thin cooling, and adds heating
!  from (possibly optically thick) EUV-radiation.
!
  USE params
  USE eos
  USE cooling
  implicit none
  real, parameter ::nohhe=0.488,codeunits=ctu**3/(cdu*clu**2) 
  real, dimension(mx,my,mz):: r,ee,lne,dd,dedt
  real, dimension(mx,my):: qq
  real dedtmax, ct
  integer ix, iy, iz
  character(len=mid):: id='coolit $Id: ISM-Lyman-Casiana.f90,v 1.4 2006/07/05 20:41:48 aake Exp $'
!-----------------------------------------------------------------------
  if (it.lt.turnon) return 
  call print_trace (id, dbg_cool, 'BEGIN')

!$omp parallel private(ix,iy,iz,qq,ct,dedtmax)
  do iz=izs,ize
    call coolit_real (mx,my,r(:,:,iz),ee(:,:,iz),lne(:,:,iz),qq)
    do iy=1,my
    do ix=1,mx
      qq(ix,iy) = qq(ix,iy)*r(ix,iy,iz)**2
      dedt(ix,iy,iz) = dedt(ix,iy,iz) - qq(ix,iy)
    end do
    end do
  enddo

  call radiation (r, ee, lne, dd, dedt)

  ct = 1./(Cdtr*dt)
  do iz=izs,ize
    do iy=1,my
    do ix=1,mx
      dedtmax = r(ix,iy,iz)*ee(ix,iy,iz)*ct
      dedt(ix,iy,iz) = min(dedt(ix,iy,iz),+dedtmax)
      dedt(ix,iy,iz) = max(dedt(ix,iy,iz),-dedtmax)
    end do
    end do
  enddo
!$omp end parallel

  call print_trace (id, dbg_cool, 'END')
END SUBROUTINE

!***********************************************************************
SUBROUTINE radiation_lookup (r, ee, rk, src, ibin)
  USE params
  implicit none
  real, dimension(mx,my,mz):: r, ee, rk, src
  integer ibin, iz
  character(len=mid):: id='radiation_lookup $Id: ISM-Lyman-Casiana.f90,v 1.4 2006/07/05 20:41:48 aake Exp $'
!-----------------------------------------------------------------------
  call print_trace (id, dbg_cool, 'BEGIN')
  call lyman_opacity (r, ee, rk)
  call lyman_source (src)
  do iz=izs,ize
    src(:,:,iz) = src(:,:,iz)/((4.*pi)/rk(:,:,iz))                      ! energy/vol -> energy/area
  end do
  call print_trace (id, dbg_cool, 'END')
END SUBROUTINE

!***********************************************************************
SUBROUTINE test_cooling
  USE units, only: utemp
  !USE eos
  implicit none
  real, dimension(16,1):: temp, r, ee1, ee2, lne, qq1, qq2
  integer i
!-----------------------------------------------------------------------
  temp(:,1) = (/ &
    1.e3, &
    10964.781e0, &
    19952.632e0, & 
    29512.078e0, & 
    39810.708e0, & 
    69183.121e0, &
    125892.51e0, &
    251188.7e0, &
    501187.01e0, &
    891250.55e0, &
    1.412537e6, & 
    2.511887e6, &
    6.309576e6, &
    1.e7, &
    1.5848925e7, &
    1.e9 /)
  ee1 = 0.999*temp/utemp
  ee2 = 1.001*temp/utemp
  r = 1.
  do i=1,16
    call coolit_real (16,1,r,ee1,lne,qq1) 
    call coolit_real (16,1,r,ee2,lne,qq2) 
    print '(i3,4g12.4)',i,temp(i,1),qq1(i,1),qq2(i,1),qq2(i,1)/(qq1(i,1)+1e-30)
  end do
END
