! $Id: transfer_slice_pwc.f90,v 1.1 2004/07/23 07:44:35 aake Exp $
!***********************************************************************
SUBROUTINE transfer_slice_pwc (mx, my, dtau, S, Q, I)
!
!  Solve the radiative transfer equation in an x-y slice, where y is the
!  vertical direction and x is a redundant direction.
!
!  dtau:   optical depth intervals
!  S   :   source funtion
!  Q   :   cooling contribution; average of in-coming and out-going I-S
!  I   :   surface intensity
!
!  Operation count: 1e+3a+4m
!
!  Author: Aake Nordlund, 2004-07-22
!-----------------------------------------------------------------------
  implicit none
!
  integer mx, my, ix, iy
  real, dimension(mx,my) :: dtau, S, Q, ex0, ex1, Iplus, Iminus
  real, dimension(mx)    :: I
  real(kind=8):: exp0
!
!-----------------------------------------------------------------------
!  Coefficients, 1e+1a
!-----------------------------------------------------------------------
  do iy=1,my
    do ix=1,mx
      exp0 = exp(-dble(dtau(ix,iy)))
      ex0(ix,iy) = exp0
      ex1(ix,iy) = 1d0-exp0
    end do  
  end do
!
!-----------------------------------------------------------------------
!  Incoming rays (1a+2m)
!-----------------------------------------------------------------------
  Iminus(:,1) = s(:,1)*ex1(:,1)
  do iy=2,my
    Iminus(:,iy) = Iminus(:,iy-1)*ex0(:,iy) + s(:,iy)*ex1(:,iy)
  end do
!
!-----------------------------------------------------------------------
!  Outgoing rays (1a+2m)
!-----------------------------------------------------------------------
  Iplus(:,my) = s(:,my)
  do iy=my-1,1,-1
    Iplus(:,iy) = Iplus(:,iy+1)*ex0(:,iy+1)  + s(:,iy)*ex1(:,iy+1)
    q(:,iy) = 0.5*(Iplus(:,iy)+Iminus(:,iy)) - s(:,iy)
  end do
  q(:,my) = 0.
!
!-----------------------------------------------------------------------
!  Surface intensity.
!-----------------------------------------------------------------------
  I(:) = Iplus(:,1)*ex0(:,1) + s(:,1)*ex1(:,1)
END
