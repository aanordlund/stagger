!-------------------------------------------------------------------------------
! To compile, do for example
!
! ifort -g -traceback -O2 -openmp test_transfer.f90 transfer_feautrier.f90 -o test_fe.x
! gfortran -g -fbacktrace -O2 -fopenmp test_transfer.f90 transfer_feautrier.f90 -o test_fe.exe
! gfortran -g -fbacktrace -O2 -fopenmp test_transfer.f90 ../EXPERIMENTS/remo/transfer_feautrier_mpi.f90 ../nompi.f90 -o test_refe.exe
! gfortran -g -fbacktrace -O2 -fopenmp test_transfer.f90 ../EXPERIMENTS/remo/transfer_integral_mpi.f90 ../EXPERIMENTS/remo/ddtau_calc_mpi.f90 ../nompi.f90 -o test_rein.exe
!
!-------------------------------------------------------------------------------
MODULE params
  integer,parameter:: mx=3, my=60, mz=3, mid=120, mvar=8, lcache=32
  integer,parameter:: mfile=120, flag_unit=10
  integer,save:: mpi_nx=1, mpi_ny=1, mpi_nz=1, isubstep=1, lb, ub, izs=1, ize=mz, iys=1, iye=1
  integer,save:: maxram, n_barrier, dbg_barrier
  integer,save:: mpi_x, mpi_y, mpi_z, mxtot, mytot, mztot, mpi_size, mpi_rank
  integer,save:: omp_nthreads, omp_mythread, omp_nz
  logical,save:: master, mpi_master, omp_master, do_trace, dbg_omp
  real,allocatable,dimension(:,:,:):: scratch
  real,allocatable,dimension(:,:,:):: gx1, hx1, gy1, hy1, gz1, hz1
  real,allocatable,dimension(:,:,:):: gx2, hx2, gy2, hy2, gz2, hz2
  real,allocatable,dimension(:,:,:):: gx3, hx3, gy3, hy3, gz3, hz3
  real,allocatable,dimension(:,:):: haver_tmp
END MODULE params

!-------------------------------------------------------------------------------
SUBROUTINE sum_2d_omp (f, n, s)
  USE params
  implicit none
  integer n
  real f(mx,my,mz), s
END

!-------------------------------------------------------------------------------
SUBROUTINE fminval_omp(label,f,fmin)
  USE params
  implicit none
  character(len=*) label
  real f(mx,my,mz), fmin
END

!-------------------------------------------------------------------------------
SUBROUTINE fmaxval_omp(label,f,fmax)
  USE params
  implicit none
  character(len=*) label
  real f(mx,my,mz), fmax
END

!-------------------------------------------------------------------------------
SUBROUTINE fmaxval_subr(label,f,fmax)
  USE params
  implicit none
  character(len=*) label
  real f(mx,my,mz), fmax
END

!-------------------------------------------------------------------------------
SUBROUTINE limits_omp(i1,i2,i3,i4)
  implicit none
  integer i1,i2,i3,i4
END

!-------------------------------------------------------------------------------
SUBROUTINE barrier_omp(label)
  implicit none
  character(len=*) label
END

!-------------------------------------------------------------------------------
!LOGICAL FUNCTION omp_in_parallel()
!  implicit none
!  omp_in_parallel=.false.
!END

!-------------------------------------------------------------------------------
SUBROUTINE dumpn (var,label,i)
  USE params
  implicit none
  real, dimension(mx,my,mz):: var
  character(len=*) label
  integer i
END

!-------------------------------------------------------------------------------
REAL FUNCTION fdtime()
  real(kind=4) dtime
  real(kind=4) cpu(2)
  fdtime=dtime(cpu)
END

!-------------------------------------------------------------------------------
REAL FUNCTION fetime()
  fetime=cpu_time()
END

!-------------------------------------------------------------------------------
SUBROUTINE print_id(id)
  USE params
  character(len=mid) id
  if (id /= '') print*,id
  id=''
END

!-------------------------------------------------------------------------------
PROGRAM test_transfer
  USE params
  implicit none
  real, dimension(mx,my,mz):: tau, dtau, s, q
  real, dimension(mx,mz):: xi
  real p,dlgtau,lgtaumin
  integer n1,iy
  logical doxi

  doxi=.false.
  p=2.
  lgtaumin=-6.
  dlgtau=0.15
  n1=(lgtaumin+1.)/dlgtau

  do iy=1,my
    tau(:,iy,:)=10.**(lgtaumin+dlgtau*(iy-1))
    s(:,iy,:)=tau(:,iy,:)**p
    dtau(:,iy,:)=10.**(lgtaumin+dlgtau*iy)-10.**(lgtaumin+dlgtau*(iy-1))
  end do

  call transfer (my,n1,dtau,s,q,xi,doxi)

  do iy=1,my
    print'(i5,1p,4g15.6)',iy,tau(1,iy,1),dtau(1,iy,1),s(1,iy,1),q(1,iy,1)
  end do
END
