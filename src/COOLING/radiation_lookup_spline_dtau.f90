! $Id: radiation_lookup_spline_dtau.f90,v 1.9 2009/02/03 22:28:37 aake Exp $
!***********************************************************************
MODULE cooling

  USE params
  integer, parameter:: mmu=9, mphi=10
  real xmu(mmu), wmu(mmu), phi, wphi, womega, dphi, form_factor, &
    dtaumin, dtaumax
  integer nmu, nphi

  logical do_table
  character(len=mfile):: intfile
END MODULE

!***********************************************************************
SUBROUTINE init_cooling (r,e)
  USE params
  USE cooling
  real, dimension(mx,my,mz):: r,e
  integer i
  character(len=mfile):: name
  character(len=mid):: id='$Id: radiation_lookup_spline_dtau.f90,v 1.9 2009/02/03 22:28:37 aake Exp $'
  if (id.ne.' ') print *,id ; id = ' '

  do_cool = .true.
  i = index(file,'.')
  nmu = 0
  nphi = 4
  dphi = 0.
  form_factor = 1.
  dtaumax = 100.
  dtaumin = 0.1
  intfile = name('intensity.dat','int',file)

  call read_cooling
END SUBROUTINE

!***********************************************************************
SUBROUTINE read_cooling
  USE params
  USE cooling
  namelist /cool/do_cool, nmu, nphi, dphi, form_factor, dtaumin, dtaumax, intfile

  if (stdin.ge.0) then
    rewind (stdin); read (stdin,cool)
  else
    read (*,cool)
  end if
  write (*,cool)
END SUBROUTINE


!***********************************************************************
SUBROUTINE coolit (r,ee,lne,dd,dedt)
!
!  Radiative cooling
!
  USE params
  USE units
  USE eos
  USE cooling
  implicit none
  real, dimension(mx,my,mz), intent(in):: r,ee,lne,dd
  real, dimension(mx,my,mz), intent(out):: dedt
  real, dimension(mx,my,mz):: rk,lnrk,s,q,dtau,qq
  real, dimension(mx,my,mz,4):: lns
  real, dimension(mx,my):: rkxy
  real, dimension(mx,mz):: xi
  integer ix, iy, iz, lmu, lphi, imu, iphi, ibox, nytr1, nytr2
  real sec, fdtime, dxdy, dzdy, tanth, f, qq1, cput(7), f1, dlnrk
  integer lrec
  logical flag
  logical, save:: first=.true.
!
  if (.not. do_cool) return
  cput = 0.

!-----------------------------------------------------------------------
!  Angular quadrature
!  nmu > 0: Use Gauss integration (mu=1 excluded) with 2*nmu   order accuracy
!  nmu = 0: One vertical ray with weight 0.5
!  nmu < 0: Use Radau integration (mu=1 included) with 2*nmu-1 order accuracy
!-----------------------------------------------------------------------
  if      (nmu .gt. 0) then
    lmu = nmu
    call gausi (lmu,0.,1.,wmu,xmu)
  else if (nmu .eq. 0) then
    lmu = 1
    xmu(1) = 1.
    wmu(1) = 0.5
  else
    lmu =-nmu
    call radaui(lmu,0.,1.,wmu,xmu)
  end if

  if (first) then
    first = .false.
    print *,' imu       xmu    theta'
    !           1.....0.000.....00.0
    do imu=1,lmu 
      print '(i4,f10.3,f8.1)',imu,xmu(imu),acos(xmu(imu))*180./pi
    end do
  end if

!-----------------------------------------------------------------------
!  Equation of state and opacity
!-----------------------------------------------------------------------
  if (do_trace) sec=fdtime()
  call lookup (r,ee,lnrk,lns)
  if (do_trace) cput(1)=cput(1)+fdtime()
  call dumpl(lnrk,'lnrk','rad.dat',0)
  call dumpl(dedt,'dedt','rad.dat',1)
  do ibox=1,mbox
    call dumpl(lns(:,:,:,ibox),'lns ','rad.dat',6+ibox)
  end do

  f = stefan/(pi*u_e*u_l/u_t)
!$omp parallel do private(iz)
  do iz=1,mz
    qq(:,:,iz) = 0.
  end do

  do imu=1,lmu
    if (nmu .eq. 0 .and. imu .eq. 1) then
      lphi = 1 
    else if (nmu .eq. -1) then
      lphi = 1
    else
      lphi = nphi
    end if
    tanth = tan(acos(xmu(imu)))
    wphi = 4.*pi/lphi

    do iphi=1,lphi
      phi = (iphi-1)*2.*pi/lphi + dphi*t
      dxdy = tanth*cos(phi)
      dzdy = tanth*sin(phi)

      call trnslt (lnrk, rk, dxdy, dzdy, .false., my, 1, .false.)
      if (do_trace) cput(2)=cput(2)+fdtime()

      do ibox=1,mbox
!-----------------------------------------------------------------------
!  Optical depth
!-----------------------------------------------------------------------
        f = (0.5/xmu(imu))
        nytr1 = 1
        nytr2 = 1
        if (ibox.eq.1) then
!$omp parallel do private(iz,iy,ix), reduction(max:nytr1,nytr2)
          do iz=1,mz
            rkxy(:,1) = exp(rk(:,1,iz))
            dtau(:,1,iz) = dym(1)*rkxy(:,1)/xmu(imu)
            dtau(:,2,iz) = (f*rkxy(:,1))*dymdn(2)
            do iy=2,my-1
              f1 = 0.5*dyidyup(iy)
              do ix=1,mx
                dlnrk = f1*(rk(ix,iy+1,iz)-rk(ix,iy-1,iz))                                                      ! d ln(rho*kap) / dy
                rkxy(ix,iy) = exp(rk(ix,iy,iz))                                                                 ! rho*kap
                dtau(ix,iy+1,iz) = (f*rkxy(ix,iy))*dymdn(iy+1)*(1.+0.1666667*dlnrk*dymdn(iy+1))                 ! contrib from lower point
                dtau(ix,iy,iz) = dtau(ix,iy,iz) + (f*rkxy(ix,iy))*dymdn(iy)*(1.-0.1666667*dlnrk*dymdn(iy))      ! contrib from upper point
                if (dtau(ix,iy,iz).lt.dtaumin) nytr1 = max(nytr1,iy)
                if (dtau(ix,iy,iz).lt.dtaumax) nytr2 = max(nytr2,iy+1)
              end do
            end do
            rkxy(:,my) = exp(rk(:,my,iz))
            dtau(:,my,iz) = dym(my)*rkxy(:,my)/xmu(imu)
            rk(:,:,iz) = rkxy
          end do
        else
!$omp parallel do private(iz,iy,ix), reduction(max:nytr1,nytr2)
          do iz=1,mz
          do iy=1,my
          do ix=1,mx
            dtau(ix,iy,iz) = 10.*dtau(ix,iy,iz)
            if (dtau(ix,iy,iz).lt.dtaumin) nytr1 = max(nytr1,iy)
            if (dtau(ix,iy,iz).lt.dtaumax) nytr2 = max(nytr2,iy)
          end do
          end do
          end do
        end if
        ! print *,iphi,imu,ibox,nytr1,nytr2
        if (do_trace) cput(5)=cput(5)+fdtime()

!-----------------------------------------------------------------------
!  Source function
!-----------------------------------------------------------------------
        call trnslt (lns(:,:,:,ibox), s, dxdy, dzdy, .true., nytr2, 1, .false.)
        if (do_trace) cput(3)=cput(3)+fdtime()

!-----------------------------------------------------------------------
!  Raditative transfer equation
!-----------------------------------------------------------------------
        flag = mod(it,nsnap).eq.0 .and. isubstep.eq.1 .and. xmu(imu).eq.1. .and. ibox.eq.1
        if (do_trace) print *,'transfer:',flag,nytr1,nytr2

        call transfer (nytr2,nytr1,dtau,s,q,xi,flag)
        if (do_trace) cput(6)=cput(6)+fdtime()
        if (ibox.eq.1.and.imu.eq.1) call dumpl(dtau,'dtau','rad.dat',2)
        if (ibox.eq.1.and.imu.eq.1) call dumpl(s   ,'s   ','rad.dat',3)
        if (ibox.eq.1.and.imu.eq.1) call dumpl(q   ,'q   ','rad.dat',4)

        f = 10.**(ibox-1)
!$omp parallel do private(iz), firstprivate(f)
        do iz=1,mz
          do iy=2,nytr2-2
            q(:,iy,iz) = q(:,iy,iz)*(dtau(:,iy+1,iz)+dtau(:,iy,iz))*xmu(imu)/(ym(iy+1)-ym(iy-1))
          end do
          q(:,1,iz) = q(:,1,iz)*rk(:,1,iz)*f
        end do

        if (flag .and. intfile.ne.' ') then
          open (2,file=intfile,form='unformatted',status='unknown',access='direct',recl=lrec(mx*mz))
          print *,'radiation intensity snapshot:',1+isnap,intfile
          write (2,rec=2+isnap) xi
          close (2)
        end if

!-----------------------------------------------------------------------
!  Energy equation
!-----------------------------------------------------------------------
        call trnslt (q, s, -dxdy, -dzdy, .false., nytr2, 1, .false.)
        f =  wmu(imu)*wphi*form_factor
!$omp parallel do private(iz,iy,ix,qq1)
        do iz=1,mz
        do iy=1,nytr2
        do ix=1,mx
          qq1 = f*s(ix,iy,iz)
          qq(ix,iy,iz) = qq(ix,iy,iz) + qq1
          dedt(ix,iy,iz) = dedt(ix,iy,iz) + qq1
        end do
        end do
        end do
        if (do_trace) cput(7)=cput(7)+fdtime()
      end do
    end do
  end do

  if (do_trace) then
    print '(1x,7f8.3)',cput
  end if

  call dumpl(qq  ,'qq  ','rad.dat',5)
  call dumpl(dedt,'dedt','rad.dat',6)

END SUBROUTINE
