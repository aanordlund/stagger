! $Id: transfer_rays_feautrier.f90,v 1.10 2014/12/31 13:29:29 aake Exp $
!=======================================================================
SUBROUTINE transfer_rays (mray, mtau, n1, n2, dtau, s, q, xi, doxi)
!
!  Solve the transfer equation, given optical depth and source function.
!
!  "Steins trick" is used; storing the sum of the three elements in each
!  row of the tridiagonal equation system, instead of the diagonal ele-
!  ment.  This allows accurate solutions to be otained for arbitrarily
!  small optical depths.  Without this trick, round off errors become
!  noticeable already when the smallest optical depth is less than the
!  square root of the machine precisions.
!
!  This version works with P as a variable for k <= n1, and with
!  Q = P - S for k > n1.  P is Feautriers P and S is the source function.
!  The equation solved is  Q" = P - S for k<=n1, and Q" = Q - S" for k>n1.
!  The answer returned is Q = P - S for all k.
!
!  This choice of equations produces a high precision in Q at large optical
!  depths (where P - S would have round off), while at the same time
!  retaining the precision at small optical depths (where S" becomes very
!  large and eventually would cause round off errors in the back
!  substitution).
!
!  This is a second order version.  For simulations, with rapidly
!  varying absorption coefficients and source functions, this is to be
!  preferred over spline and Hermitean versions because it is positive
!  definite, in the sense that a positive source function is guaranteed
!  to result in a positive average intensity p.  Also, the flux
!  divergence is exactly equal to q, for the conventional definition
!  of the flux.
!
!  Operation count: 5d+5m+11a = 21 flops
!
!  Update history:  This routine is based on transq, with updates
!-----------------------------------------------------------------------
!
  USE params, only: dbg_select, dbg_rad, mid, mpi_rank, master
  implicit none
  integer n1, n2, mray, mtau
  real, dimension(mray,mtau) :: dtau,s,q
  real, dimension(mray)      :: xi
  real, dimension(:), allocatable :: ex
  real, dimension(:,:), allocatable :: sp1,sp2,sp3
  !real, dimension(mray,n2) :: sp1,sp2,sp3
  integer :: k, l, iz, n1p
  real :: a, taum, dinv
  logical doxi, debug2
!
  character(len=mid):: id='$Id: transfer_rays_feautrier.f90,v 1.10 2014/12/31 13:29:29 aake Exp $'

  call print_id (id)
  !call print_trace('feautrier',dbg_rad,'start')
  if (mray<=0 .or. mtau<=0 .or. n1<=0 .or. n2<=0) then
    print*,'ERROR in rank', mpi_rank, mray, mtau, n1, n2
    stop
  end if
  allocate (sp1(mray,n2), sp2(mray,n2), sp3(mray,n2), ex(mray))
  !call print_trace('feautrier',dbg_rad,'allocated')
!
!  Calculate 1-exp(-tau(l,1)) straight or by series expansion,
!  based on max tau(l,1).
!
  do l=1,mray
    if (dtau(l,1).gt.0.1) then
      ex(l)=1.-exp(-dtau(l,1))
    else
      ex(l)=dtau(l,1)*(1.-.5*dtau(l,1)*(1.-.3333*dtau(l,1)))
    endif
  end do
  !call print_trace('feautrier',dbg_rad,'ex')
!
!  Boundary condition at k=1.
!
  do l=1,mray
      sp2(l,1)=dtau(l,2)*(1.+.5*dtau(l,2))
      sp3(l,1)=-1.
      q(l,1)=s(l,1)*dtau(l,2)*(.5*dtau(l,2)+ex(l))
  end do
!
!  Matrix elements for k>2, k<n2: [3d+3s]
!
  do k=2,n2-1
    do l=1,mray
      sp2(l,k)=1.
      dinv=2./(dtau(l,k+1)+dtau(l,k))
      sp1(l,k)=-dinv/dtau(l,k)
      sp3(l,k)=-dinv/dtau(l,k+1)
    end do
  end do
  !call print_trace('feautrier',dbg_rad,'matrix')
!
!  Right hand sides, for k>2, k<n1
!
  n1p=min0(max0(n1,2),n2-3)                                             ! n1p = 2 < n1 < n2-2
  do k=2,n1p-1
    do l=1,mray
      q(l,k)=s(l,k)
    end do
  end do
  !call print_trace('feautrier',dbg_rad,'rhs1')
!
!  RHS for k=n1p.  The equation is still in p(k), but q(k+1) is now
!  q(k+1)=p(k+1)-s(k+1), so to get p(k+1) we have to add s(k+1) to the
!  LHS, that is subtract it from the RHS.
!
  if (n1p.le.n2) then
    do l=1,mray
      q(l,n1p)=s(l,n1p)-sp3(l,n1p)*s(l,n1p+1)
    end do
  endif
  !call print_trace('feautrier',dbg_rad,'rhs2')
!
!  RHS for k=n1p+1.  The equation is now in Q: Q" = Q - S", but q(k-1)
!  is now p(k-1)=q(k-1)+s(k-1), so we must add sp1*s(l,k-1) to the RHS,
!  relative to the case below.
!
  if (n1p+1.le.n2) then
    do l=1,mray
      q(l,n1p+1)=sp1(l,n1p+1)*s(l,n1p+1) &
                +sp3(l,n1p+1)*(s(l,n1p+1)-s(l,n1p+2))
    end do
  endif
  !call print_trace('feautrier',dbg_rad,'rhs3')
!
!  k>n1p+1,<n2 [2m+2a]
!
  do k=n1p+2,n2-1
    do l=1,mray
      q(l,k)=sp1(l,k)*(s(l,k)-s(l,k-1)) &
              +sp3(l,k)*(s(l,k)-s(l,k+1))
    end do
  end do
  !call print_trace('feautrier',dbg_rad,'back')
!
!  k=n2
!
  do l=1,mray
      sp2(l,n2)=1.
      q(l,n2)=0.0
  end do
!
!  Eliminate subdiagonal, save factors in sp1 [1d+2m+4a]
!
  do k=1,n2-2
    do l=1,mray
      a=-sp1(l,k+1)/(sp2(l,k)-sp3(l,k))
      q(l,k+1)=q(l,k+1)+a*q(l,k)
      sp2(l,k+1)=sp2(l,k+1)+a*sp2(l,k)
      sp2(l,k)=sp2(l,k)-sp3(l,k)
    end do
  end do
  do l=1,mray
    sp2(l,n2-1)=sp2(l,n2-1)-sp3(l,n2-1)
  end do
  !call print_trace('feautrier',dbg_rad,'elim')
!
!  Backsubstitute [1d+1m+1a]
!
  do k=n2-1,1,-1
    do l=1,mray
      q(l,k)=(q(l,k)-sp3(l,k)*q(l,k+1))/sp2(l,k)
    end do
  end do
  !call print_trace('feautrier',dbg_rad,'backsub')
!
!  Subtract S in the region where the equation is in P
!
  do k=1,n1p
    do l=1,mray
      q(l,k)=q(l,k)-s(l,k)
    end do
  end do
  !call print_trace('feautrier',dbg_rad,'n1p')
!
!  Surface intensity.
!
  if (doxi) then
    do l=1,mray
      xi(l)=2.*(1.-ex(l))*(q(l,1)+s(l,1))+s(l,1)*ex(l)**2
    end do
  end if
  !call print_trace('feautrier',dbg_rad,'xi')
!
  deallocate (sp1, sp2, sp3, ex)
  !if (do_trace) print'(a,i7,2i5,2i4,l3)','feau2:rank                 ', mpi_rank
  call print_trace('feautrier',dbg_rad,'end')
END
