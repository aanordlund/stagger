subroutine transfer (np,n1,dtau,s,q,xi,doxi)

  ! Solves radiative transfer equation using integral solver, second 
  ! order accurate. Returns Q = P - S where P = (Iplus + Iminus)/2.
  !
  ! Q=Q(tau) is computed by solving the radiative transfer equation in
  ! the forward (fw)  and reverse (rv) direction separately, then 
  ! averaging the solutions in the two directions:
  !
  !   d Qfw / d tau = - Qfw - d S / d tau
  !
  !   d Qrv / d tau =   Qrv - d S / d tau 
  !
  ! Q = (Qfw + Qrv) / 2

  ! This version assumes that the function f=(ds/dtau) is described by a
  ! spline; it then uses the spline coefficients to calculate the
  ! integral of (ds/dtau) exp(-tau) in d(tau).
  !
  ! The spline form used to compute the coefficients for the integral
  ! in each "dtau" interval is:
  !
  ! f(x) = f0 + (f1-f0) * x + (dfdx0-(f1-f0))*g(x) - (dfdx1-(f1-f0))*h(x)
  !
  ! where x is equal to (tau-tau(i-1) / dtau(i) in the forward
  ! solution, or to (tau-tau(i) / dtau(i+1) in the reverse one,
  ! f is equal to f( x(tau) ) = ds/dtau,
  ! and g(x)=x*(1-x)^2 and h(x)=x^2*(1-x).
  !
  ! Taylor expansions of the exponential function exp(-dtau*x) are used
  ! at x=2.0e-2 or below, to achieve numerical precision in the
  ! calculations of the coefficients.

  use params, only: mid, mx, my, mz, izs, ize

  implicit none

  integer np,n1 
  ! note: n1 not used explicitly in this subroutine (defined only to have same 
  ! syntax as Feautrier-like version


  real, dimension(mx,my,mz) :: dtau,s,q
  real, dimension(mx,mz)    :: xi
  logical doxi

  integer ix,iy,iz


  real, dimension(mx,my,mz) :: qfw, qrv

  real, allocatable, dimension(:,:,:) :: ds1dtau, dsdtau, d2sdtau2
  real, allocatable, dimension(:,:,:) :: ex0, ex1, ex2, ex3, ex4
  real dtau1
  real, dimension(mx,mz) :: ctau

  real, dimension(mx) :: f0, f1, dfdx0, dfdx1


  real, parameter     :: dtau_thin = 0.01



  ! Allocate arrays for storing exponential coefficients and derivatives 
  ! of the source function

  allocate ( ex0(mx,my,mz), ex1(mx,my,mz), ex2(mx,my,mz), ex3(mx,my,mz), ex4(mx,my,mz) )
  allocate ( ds1dtau(mx,my,mz), dsdtau(mx,my,mz), d2sdtau2(mx,my,mz) )



  ! Compute derivatives of source function
  !
  !  ds1dtau  = first derivative at half-points (first-order accuracy);
  !             Note that ds1dtau(i) = derivative at half point i-1/2;
  !  dsdtau   = first derivatives, centred; second-order accuracy;
  !  d2sdtau2 = second derivatives, centred; second-order accuracy.

  ! Calculate ds1dtau

  ! Note that dtau(i) = tau(i)-tau(i-1) 

  do iz=izs,ize
     do iy=2,np
        ds1dtau(:,iy,iz) = (s(:,iy,iz) - s(:,iy-1,iz)) / dtau(:,iy,iz)
     end do
  end do

  ! Calculate dsdtau and ds2dtau2

  do iz=izs,ize
     do iy=2,np-1
        ctau  (:,iz)      = 1.0 / (dtau(:,iy+1,iz) + dtau(:,iy,iz))       
        dsdtau(:,iy,iz)   = (ds1dtau(:,iy+1,iz)*dtau(:,iy,iz) + ds1dtau(:,iy,iz)*dtau(:,iy+1,iz)) * ctau(:,iz)
        d2sdtau2(:,iy,iz) = (ds1dtau(:,iy+1,iz) - ds1dtau(:,iy,iz)) * ctau(:,iz) * 2.0
     end do
  end do

  ! Boundary values for dsdtau and d2sdtau2
  !
  ! d2sdtau2 : constant extrapolation
  ! ds1dtau  : linear extrapolation 

  do iz=izs,ize
     d2sdtau2(:, 1,iz) = d2sdtau2(:,2,iz)
     d2sdtau2(:,np,iz) = d2sdtau2(:,np-1,iz)
     dsdtau(:, 1,iz)   = dsdtau(:,   2,iz) - d2sdtau2(:,   2,iz) * dtau(:, 2,iz)
     dsdtau(:,np,iz)   = dsdtau(:,np-1,iz) + d2sdtau2(:,np-1,iz) * dtau(:,np,iz)
  end do


  ! compute exponential coefficients

  do iz=izs,ize
     do iy=1,np
        do ix=1,mx
           dtau1 = dtau(ix,iy,iz)
           if ( dtau1 .gt. dtau_thin ) then
              ex0(ix,iy,iz) = exp(-dtau1)
              ex1(ix,iy,iz) = 1.0 - ex0(ix,iy,iz)
              ex2(ix,iy,iz) = (ex1(ix,iy,iz) - dtau1*ex0(ix,iy,iz))/dtau1
              ex3(ix,iy,iz) = ((dtau1**2 -4.0*dtau1 +6.0) - 2.0*(dtau1 + 3.0)*ex0(ix,iy,iz)) / dtau1**3
              ex4(ix,iy,iz) = (2.*(dtau1 -3.0)+(dtau1**2 + 4.0*dtau1 + 6.0)*ex0(ix,iy,iz)) / dtau1**3
           else 
              ex1(ix,iy,iz) = dtau1*(1.0 + dtau1*(-0.5 + dtau1*(0.166667 + dtau1*(-0.0416667 + dtau1*0.00833333))))
              ex0(ix,iy,iz) = 1. - ex1(ix,iy,iz)
              ex2(ix,iy,iz) = dtau1*(0.5 + dtau1*(-0.333333 + dtau1*(0.125 + dtau1*(-0.0333333 + dtau1*0.00694444))))
              ex3(ix,iy,iz) = dtau1*(0.0833333 + dtau1*(-0.0333333 + dtau1*(0.00833333 + dtau1*(-0.00158730 + dtau1*0.0002480159))))
              ex4(ix,iy,iz) = dtau1*(0.0833333 + dtau1*(-0.05 + dtau1*(0.0166667 + dtau1*(-0.003968254 + dtau1*0.0007440476))))        
           end if
        end do
     end do
  end do


  ! Solution forward direction

  do iz=izs,ize
     qfw(:,1,iz) = - s(:,1,iz) * ex0(:,1,iz)
     do iy=2,np

        f0(:)    =    dsdtau(:,iy,iz) 
        f1(:)    =    dsdtau(:,iy-1,iz)
        dfdx0(:) = -d2sdtau2(:,iy,iz)   * dtau(:,iy,iz)
        dfdx1(:) = -d2sdtau2(:,iy-1,iz) * dtau(:,iy,iz)

        qfw(:,iy,iz) = qfw(:,iy-1,iz) * ex0(:,iy,iz) - &
             ( f0(:)*ex1(:,iy,iz) + (f1(:)-f0(:))*ex2(:,iy,iz) &
             + (dfdx0(:)-(f1(:)-f0(:)))*ex3(:,iy,iz) &
             - (dfdx1(:)-(f1(:)-f0(:)))*ex4(:,iy,iz) )

     end do
  end do


  ! Solution reverse direction

  do iz=izs,ize
     qrv(:,np,iz) = dsdtau(:,np,iz) + d2sdtau2(:,np,iz)
     do iy=np-1,1,-1

        f0(:)    =    dsdtau(:,iy,  iz) 
        f1(:)    =    dsdtau(:,iy+1,iz)
        dfdx0(:) =  d2sdtau2(:,iy  ,iz) * dtau(:,iy+1,iz)
        dfdx1(:) =  d2sdtau2(:,iy+1,iz) * dtau(:,iy+1,iz)

        qrv(:,iy,iz) = qrv(:,iy+1,iz) * ex0(:,iy+1,iz) + &
             ( f0(:)*ex1(:,iy+1,iz) + (f1(:)-f0(:))*ex2(:,iy+1,iz) &
             + (dfdx0(:)-(f1(:)-f0(:)))*ex3(:,iy+1,iz) &
             - (dfdx1(:)-(f1(:)-f0(:)))*ex4(:,iy+1,iz) )
     end do
  end do


  ! Finally, compute average of Qfw and Qrv, that is P = ((Ifw + Irv) - S ) / 2

  do iz=izs,ize
     do iy=1,np
        q(:,iy,iz) = 0.5 * (qfw(:,iy,iz)+qrv(:,iy,iz))
     end do
  end do


  ! Surface intensity

  if (doxi) then
     do iz=izs,ize
        xi(:,iz) = 2.0 * ex0(:,1,iz) * ( q(:,1,iz) + s(:,1,iz) )  + s(:,1,iz) * ex1(:,1,iz)**2
     end do
  end if


  ! Deallocate arrays

  deallocate ( ex0, ex1, ex2, ex3, ex4 )
  deallocate ( ds1dtau, dsdtau, d2sdtau2 ) 

end subroutine transfer
