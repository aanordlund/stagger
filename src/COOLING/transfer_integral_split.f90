!***********************************************************************
SUBROUTINE transfer_coeffs_single (np,dtau,s,ex0,ex1,ex2,ds1dtau)
  USE params
  implicit none
  integer ix, iy, iy1, iz, np
  real, dimension(mx,my) :: dtau, s
  real, dimension(mx,my) :: ex0, ex1, ex2, ds1dtau
  real small
!
  do iy=2,np
    iy1 = iy-1
    do ix=1,mx
      ds1dtau(ix,iy) = (s(ix,iy)-s(ix,iy1))/dtau(ix,iy)
!      small = 1./(1+((dtau(ix,iy)**2)**2)**2)
      small = 1./(1+dtau(ix,iy)**2)
      small = small*small
!      small = small*small
      ex0(ix,iy) = exp(-dtau(ix,iy))
      ex1(ix,iy) = small*dtau(ix,iy)+(1.-small)*(1.-ex0(ix,iy))
      ex2(ix,iy) = ex1(ix,iy)-dtau(ix,iy)*ex0(ix,iy)
    end do
  end do
  do ix=1,mx
    ex1(ix, 1) = dtau(ix, 1)
    ex0(ix, 1) = 1.-ex1(ix, 1)
    ex2(ix, 1) = ex1(ix, 1)-dtau(ix, 1)*ex0(ix, 1)
  end do
END

!***********************************************************************
SUBROUTINE transfer_coeffs_double (np,dtau,s,ex0,ex1,ex2,ds1dtau)
  USE params
  implicit none
  integer ix, iy, iz, np
  real, dimension(mx,my) :: dtau, s
  real, dimension(mx,my) :: ex0, ex1, ex2, ds1dtau
  real, dimension(mx) :: dtau8
  real(kind=8) ex
!
  do iy=2,np
    do ix=1,mx
      dtau8(ix) = dtau(ix,iy)
    end do
    do ix=1,mx
      ds1dtau(ix,iy) = (s(ix,iy)-s(ix,iy-1))/dtau(ix,iy)
      ex = exp(-dtau8(ix))
      ex0(ix,iy) = ex
      ex1(ix,iy) = (1d0-ex)
      ex2(ix,iy) = (1d0-ex)-dtau(ix,iy)*ex
    end do
  end do
  do ix=1,mx
    dtau8(ix) = dble(dtau(ix,1))
  end do
  do ix=1,mx
    ex = exp(-dtau8(ix))
    ex0(ix,1) = ex
    ex1(ix,1) = (1d0-ex)
    ex2(ix,1) = (1d0-ex)-dtau(ix,1)*ex
  end do
END

!***********************************************************************
SUBROUTINE transfer_coeffs_if (np,dtau,s,ex0,ex1,ex2,ds1dtau)
  USE params
  implicit none
  integer ix, iy, iz, np
  real, dimension(mx,my) :: dtau, s
  real, dimension(mx,my)    :: ex0, ex1, ex2, ds1dtau
  real dtau0, dtau1, dtau2, dtau3
  real(kind=8) ex
!
      dtau3 = 10.
      dtau2 = 0.1
      dtau1 = 0.01
      dtau0 = 0.001
      do iy=2,np
        do ix=1,mx
          if      (dtau(ix,iy) .gt. dtau3) then
            ex0(ix,iy) = 0.
            ex1(ix,iy) = 1.
          else if (dtau(ix,iy) .gt. dtau2) then
            ex0(ix,iy) = exp(-dtau(ix,iy))
            ex1(ix,iy) = 1.-ex0(ix,iy)
          else if (dtau(ix,iy) .gt. dtau1) then
            ex1(ix,iy) = dtau(ix,iy)*(1.-0.5*dtau(ix,iy)*(1.-0.333333*dtau(ix,iy)))
            ex0(ix,iy) = 1.-ex1(ix,iy)
          else if (dtau(ix,iy) .gt. dtau0) then
            ex1(ix,iy) = dtau(ix,iy)*(1.-0.5*dtau(ix,iy))
            ex0(ix,iy) = 1.-ex1(ix,iy)
          else
            ex1(ix,iy) = dtau(ix,iy)
            ex0(ix,iy) = 1.-ex1(ix,iy)
          end if
        end do
        do ix=1,mx
          ds1dtau(ix,iy) = (s(ix,iy)-s(ix,iy-1))/dtau(ix,iy)
          ex2(ix,iy) = ex1(ix,iy)-dtau(ix,iy)*ex0(ix,iy)
        end do
      end do
      do ix=1,mx
        if      (dtau(ix,1) .gt. dtau3) then
          ex0(ix,1) = 0.
          ex1(ix,1) = 1.
        else if (dtau(ix,1) .gt. dtau2) then
          ex0(ix,1) = exp(-dtau(ix,1))
          ex1(ix,1) = 1.-ex0(ix,1)
        else if (dtau(ix,1) .gt. dtau1) then
          ex1(ix,1) = dtau(ix,1)*(1.-0.5*dtau(ix,1)*(1.-0.333333*dtau(ix,1)))
          ex0(ix,1) = 1.-ex1(ix,1)
        else
          ex1(ix,1) = dtau(ix,1)*(1.-0.5*dtau(ix,1))
          ex0(ix,1) = 1.-ex1(ix,1)
        end if
        ex2(ix,1) = ex1(ix,1)-dtau(ix,1)*ex0(ix,1)
      end do
END

!***********************************************************************
SUBROUTINE transfer(np,n1,dtau,s,q,xi,doxi)
!
  USE params
  implicit none
!
  real, dimension(mx,my,mz) :: dtau,s,q
  real, dimension(mx,mz)    :: xi
  real, dimension(mx,my)    :: ex0, ex1, ex2, ds1dtau, dsdtau, d2sdtau2
  real, dimension(mx,my)    :: qplus, qminus
  real, dimension(mx)       :: ex
  real :: taum, dtau0, dtau1, dtau2, ctau, small
  integer :: ix, iy, iz, np, n1, irep, nrep
  real  cpu0, cpu1, cpu2, cpu3, fdtime
  save cpu1, cpu2, cpu3
  data cpu1/0./, cpu2/0./, cpu3/0./
  logical doxi
!
  character(len=80):: id='$Id: transfer_integral_split.f90,v 1.2 2004/12/26 20:52:37 aake Exp $'
!
!  Choose method
!
  if (cpu1.eq.0.0) then
    iz = 1
    cpu0 = fdtime()
    nrep = 0
    do while (cpu1 < 1.)
      nrep = nrep+1
      call transfer_coeffs_single (np,dtau(1,1,iz),s(1,1,iz),ex0,ex1,ex2,ds1dtau)
      cpu1 = cpu1+fdtime()
    end do
    cpu1 = fdtime()
    do irep=1,nrep
      call transfer_coeffs_single (np,dtau(1,1,iz),s(1,1,iz),ex0,ex1,ex2,ds1dtau)
    end do
    cpu1 = fdtime()
    do irep=1,nrep
      call transfer_coeffs_double (np,dtau(1,1,iz),s(1,1,iz),ex0,ex1,ex2,ds1dtau)
    end do
    cpu2 = fdtime()
    do irep=1,nrep
      call transfer_coeffs_if (np,dtau(1,1,iz),s(1,1,iz),ex0,ex1,ex2,ds1dtau)
    end do
    cpu3 = fdtime()
    print*,'radiation_integral: times =',cpu1,cpu2,cpu3
    if (cpu1.lt.min(cpu2,cpu3)) then
      print*,'radiation_integral: choosing single method'
    else if (cpu2.lt.cpu3) then
      print*,'radiation_integral: choosing double method'
    else
      print*,'radiation_integral: choosing if method'
    end if
  end if
!
!  Calculate 1-exp(-tau(l,1,n)) straight or by series expansion,
!  based on max tau(l,1,n).
!
!$omp parallel do private(iz,ix,iy,taum,ex,dtau0,dtau1,dtau2,small,ex0,ex1,ex2, &
!$omp   ctau,ds1dtau,dsdtau,d2sdtau2,qminus,qplus)
  do iz=1,mz
!
!  Compute factors (4a+3m+1d)
!
    if (cpu1.lt.min(cpu2,cpu3)) then
      call transfer_coeffs_single (np,dtau(1,1,iz),s(1,1,iz),ex0,ex1,ex2,ds1dtau)
    else if (cpu2.lt.cpu3) then
      call transfer_coeffs_double (np,dtau(1,1,iz),s(1,1,iz),ex0,ex1,ex2,ds1dtau)
    else
      call transfer_coeffs_if (np,dtau(1,1,iz),s(1,1,iz),ex0,ex1,ex2,ds1dtau)
    end if
!
!  dS/dtau and d2S/dtau2 (5m+3a+1d)
!
    do iy=2,np-1
      do ix=1,mx
        ctau = 1./(dtau(ix,iy+1,iz)+dtau(ix,iy,iz))
        dsdtau(ix,iy) = (ds1dtau(ix,iy+1)*dtau(ix,iy,iz)+ds1dtau(ix,iy)*dtau(ix,iy+1,iz))*ctau
        d2sdtau2(ix,iy) = (ds1dtau(ix,iy+1)-ds1dtau(ix,iy))*2.*ctau
      end do
    end do
    dsdtau(:,1) = 0.
    d2sdtau2(:,1) = 0.
    dsdtau(:,np) = dsdtau(:,np-1)+dtau(:,np,iz)*d2sdtau2(:,np-1)
    d2sdtau2(:,np) = d2sdtau2(:,np-1)
!
!  Incoming rays (2a+3m)
!
    qminus(:,1) = -s(:,1,iz)
    do iy=2,np
      qminus(:,iy) = qminus(:,iy-1)*ex0(:,iy)-dsdtau(:,iy)*ex1(:,iy)+d2sdtau2(:,iy)*ex2(:,iy)
    end do
!
!  Outgoing rays (2a+3m)
!
    qplus(:,np) = dsdtau(:,np)+d2sdtau2(:,np)
    do iy=np-1,1,-1
      qplus(:,iy) = qplus(:,iy+1)*ex0(:,iy+1)+dsdtau(:,iy)*ex1(:,iy+1)+d2sdtau2(:,iy)*ex2(:,iy+1)
      q(:,iy,iz) = 0.5*(qplus(:,iy)+qminus(:,iy))
    end do
    q(:,np,iz) = 0.5*(qplus(:,np)+qminus(:,np))

  end do
!
END
