! $Id: transfer_integral_simple.f90,v 1.4 2004/12/27 01:46:49 aake Exp $
!***********************************************************************
SUBROUTINE transfer (np, n1, dtau, s, q, xi, do_xi)                     ! radiative transfer solution
  use params
  logical do_xi
  real, dimension(mx,my,mz):: dtau, s, q
  real, dimension(mx,my):: dq2, q1, q2, s1, s2
  real, dimension(mx,mz):: xi
  real, dimension(mx,my):: exp1
  real(kind=8) f0
  real f1, f2
  real i1, i2
  integer np, n1

  do iz=1,mz
    call deriv2 (np, dtau(1,1,iz), s(1,1,iz), s1, s2)                   ! source function derivatives
    do ix=1,mx
      q1(ix,1) = i1-s(ix,1,iz)
    end do
    do iy=2,np
    do ix=1,mx
      f0 = exp(dble(-dtau(ix,iy,iz)))                                   ! exponential factor
      exp1(ix,iy) = f0                                                  ! exponential factor
      f1 = 1d0-f0                                                       ! first derivative factor
      f2 = 1d0-f0*(1d0+dtau(ix,iy,iz))                                  ! second derivative factor
      dq1          = -s1(ix,iy  )*f1 + s2(ix,iy  )*f2                   ! local contributions
      dq2(ix,iy-1) =  s1(ix,iy-1)*f1 + s2(ix,iy-1)*f2                   ! local contributions
      q1(ix,iy) = q1(ix,iy-1)*f0 + dq1                                  ! local contributions
    end do
    end do
    do ix=1,mx
      q2(ix,np) = i2-s(ix,np,iz)
      q(ix,np,iz) = 0.5*(q1(ix,np)+q2(ix,np))
    end do
    do iy=np-1,1,-1
    do ix=1,mx
      q2(ix,iy) = q2(ix,iy+1)*exp1(ix,iy+1) + dq2(ix,iy)                ! local contributions
      q(ix,iy,iz) = 0.5*(q1(ix,iy)+q2(ix,iy))                           ! average q
    end do
    end do
  end do
END SUBROUTINE

!-----------------------------------------------------------------------
SUBROUTINE deriv2 (np, ds, y, y1, y2)                                   ! first and second derivs
  use params
  real, dimension(mx,my):: ds, y, y1, y2, cx, dyds
  integer np

  do ix=1,mx
    cx(ix,np) =  0.
    dyds(ix,1) = 0.
  end do
  do iy=2,np
  do ix=1,mx
    cx(ix,iy-1) =  1./(ds(ix,iy)+ds(ix,iy-1))                           ! inverse difference
    dyds(ix,iy) =  (y(ix,iy)-y(ix,iy-1))/ds(ix,iy)                      ! first difference
  end do
  end do

  do iy=2,np-1
  do ix=1,mx
    y1(ix,iy) = (ds(ix,iy+1)*dyds(ix,iy  ) + &
                    ds(ix,iy  )*dyds(ix,iy+1))*cx(ix,iy)                ! first derivative
    y2(ix,iy) = (dyds(ix,iy+1)-dyds(ix,iy))*2.*cx(ix,iy)                ! second derivative
  end do
  end do
  do ix=1,mx
    y1(ix,1 ) = (-ds(ix,3   )*dyds(ix,3   ) + (2.*ds(ix,3   )+ds(ix,2 ))*dyds(ix,2 ))*cx(ix,2   )
    y1(ix,np) = (-ds(ix,np-1)*dyds(ix,np-1) + (2.*ds(ix,np-1)+ds(ix,np))*dyds(ix,np))*cx(ix,np-1)
    y2(ix,1 ) = y2(ix,2   )
    y2(ix,np) = y2(ix,np-1)
  end do
END
