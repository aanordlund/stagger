! $Id: cold_opacity.f90,v 1.1 2004/02/05 07:38:51 aake Exp $
! ----------------------------------------------------------------------
SUBROUTINE opacity (temp, rho, pe, kappa)
!
!  Absorption coefficient [cm^2/g] for H-, scaled to a value 3e-26 cm^4/dyne
!  at T=6300 K extracted from Fig. 3 of Doughty & Fraser MNRAS 132, 267
!  (1966).  A mean atomic weight of 1.4 (10% helium by number) is assumed.
!
  USE params
  implicit none
  integer ix,iy,iz
  real, dimension(mx,my,mz):: temp, rho, pe, kappa
  real, dimension(6):: Ttab, kap
  real cgs_rho, rhocgs, temp2

  cgs_rho = 1e-7

  Ttab(1) = 150.
  Ttab(2) = 180.

  kap(1) = 2.e-4*1e2**2
  kap(2) = 1.15e18/1e2**8
  kap(3) = 2.e-2*1e2**0.75
  kap(4) = 1.57e24
  kap(5) = 1.6e-2
  kap(6) = 2.e34/1e2**9

  do iz=1,mz
  do iy=1,my
  do ix=1,mx
    temp2 = temp(ix,iy,iz)*1e-2
    rhocgs = rho(ix,iy,iz)*cgs_rho
    Ttab(3) = 1380.*(rhocgs*1e8)**0.02
    Ttab(4) = 1890.*(rhocgs*1e8)**(1./48.)
    Ttab(5) = 2620.*(rhocgs*1e8)**(2./27.)
    if      (temp(ix,iy,iz) .lt. Ttab(1)) then
      kappa(ix,iy,iz) = kap(1)*temp2**2
    else if (temp(ix,iy,iz) .lt. Ttab(2)) then
      kappa(ix,iy,iz) = kap(2)*temp2**(-8.)
    else if (temp(ix,iy,iz) .lt. Ttab(3)) then
      kappa(ix,iy,iz) = kap(3)*temp2**0.75
    else if (temp(ix,iy,iz) .lt. Ttab(4)) then
      kappa(ix,iy,iz) = kap(4)*temp2**(-18.)
    else if (temp(ix,iy,iz) .lt. Ttab(5)) then
      kappa(ix,iy,iz) = kap(5)*rhocgs**(2./27.)
    else
      kappa(ix,iy,iz) = kap(6)*rhocgs**(2./3.)*temp2**(-9.)
    end if
  end do
  end do
  end do

END
