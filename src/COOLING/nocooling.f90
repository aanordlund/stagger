! $Id: nocooling.f90,v 1.16 2006/03/22 18:06:15 aake Exp $
MODULE cooling
  real dummy
END MODULE

SUBROUTINE init_cooling (r,e)
  USE params
  real, dimension(mx,my,mz):: r,e
  character(len=mid):: id="$Id: nocooling.f90,v 1.16 2006/03/22 18:06:15 aake Exp $"
  call print_id (id)
  do_cool = .false.

  call read_cooling
END SUBROUTINE

SUBROUTINE read_cooling
  USE params

END SUBROUTINE

SUBROUTINE coolit (r,ee,lne,dd,dedt)
  USE params
  USE cooling
  implicit none
  real, dimension(mx,my,mz) :: r, ee, lne, dd, dedt
!hpf$ distribute(*,*,block):: r, ee, lne, dd, dedt

  return
END SUBROUTINE
