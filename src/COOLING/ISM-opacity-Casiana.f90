! $Id: ISM-opacity-Casiana.f90,v 1.8 2006/05/15 23:15:19 aake Exp $
!***********************************************************************
SUBROUTINE radiation_lookup (rho, ee, rkap, src, ibin)
!
!  Units are such that rkap is in one over length, which is the same
!  as kappa*rho [cm^2/g times g/cm^3], i.e., absorption per unit volume.  
!  To get the optically thin cooling Q per unit volume, in erg/s/cm^3, we 
!  need to take the source funtion, in erg/s/cm^2 and multiply with 
!  rho*kappa; i.e., Q = rho*kappa*S = rho^2*lambda.  Therefore, to get 
!  rho*kappa from lambda we need to multiply with rho^2 and divide with S.
!
  USE params
  USE eos
  USE units
  USE cooling
  implicit none
  real, dimension(mx,my,mz), intent(in) :: rho, ee
  real, dimension(mx,my,mz), intent(out):: rkap, src
  integer ibin
!-----------------------------------------------------------------------
  real temp,u_eflux,stef,qq
  integer ix,iy,iz
  logical, save:: first_time=.true.
  real, save:: &
    TEm=1.e3        , Cm=2.596     , Hm=4.98282e+15          , &
    TE0=1.e3        , C0=2.596     , H0=3.2473989e-14/4.e-28 , &
    TE1=10964.781e0 , C1=3.846     , H1=2.894e-19/4.e-28     , &
    TE2=19952.632e0 , C2=-0.1764   , H2=5.739e-2/4.e-28      , & 
    TE3=29512.078e0 , C3=1.00001   , H3=3.1620277e-07/4.e-28 , & 
    TE4=39810.708e0 , C4=1.6666    , H4=2.7123742e-10/4.e-28 , & 
    TE5=69183.121e0 , C5=1.15384   , H5=8.2298862e-08/4.e-28 , &
    TE6=125892.51e0 , C6=0.1       , H6=1.9497e-02/4.e-28    , &
    TE7=251188.7e0  , C7= -3.933   , H7=1.1749e+20           , &
    TE8=501187.01e0 , C8= 0.72     , H8=3.5155e-7/4.e-28     , &
    TE9=891250.55e0 , C9= -0.65    , H9=4.982757e+1/4.e-28   , &  
    TE10=1.412537e6 , C10= 0.4     , H10=1.7379e-5/4.e-28    , & 
    TE11=2.511887e6 , C11= -1.25   , H11=6.309e+5/4.e-28     , &
    TE12=6.309576e6 , C12= 0.      , H12=1.99525e-03/4.e-28  , &
    TE13=1.e7       , C13= -0.4    , H13=1.2589e-00/4.e-28   , &
    TE14=1.5848925e7, C14= 0.43333 , H14=1.2589e-06/4.e-28   , &
    TE15=1.e9     
  real, parameter:: scale=(ctu/clu)**2*(ctu*cdu)
  character(len=mid):: id='$Id: ISM-opacity-Casiana.f90,v 1.8 2006/05/15 23:15:19 aake Exp $'
!-----------------------------------------------------------------------
  call print_trace (id, dbg_cool, 'BEGIN')

  u_eflux = cdu*cvu**3
  stef = stefan/u_eflux/pi
  
  if (master .and. first_time) then
    first_time = .false.
    print *,'utemp =',utemp
    print *,'stef =',stef, stefan, u_eflux
  end if

  do iz=izs,ize
    do iy=1,my
    do ix=1,mx
      temp = utemp*ee(ix,iy,iz)
      if      (temp .le. Tlower) then
        qq = 0.
      else if (temp .le. TE0) then
        qq = scale*(Hm*temp**Cm)
      else if (temp .le. TE1) then
        qq = scale*(H0*temp**C0)
      else if (temp .le. TE2) then
        qq = scale*(H1*temp**C1)
      else if (temp .le. TE3) then
        qq = scale*(H2*temp**C2)
      else if (temp .le. TE4) then
        qq = scale*(H3*temp**C3)
      else if (temp .le. TE5) then
        qq = scale*(H4*temp**C4)
      else if (temp .le. TE6) then
        qq = scale*(H5*temp**C5)
      else if (temp .le. TE7) then
        qq = scale*(H6*temp**C6)
      else if (temp .le. TE8) then
        qq = (scale/4e-28)*(H7*temp**C7)
      else if (temp .le. TE9) then
        qq = scale*(H8*temp**C8)
      else if (temp .le. TE10) then
        qq = scale*(H9*temp**C9)
      else if (temp .le. TE11) then
        qq = scale*(H10*temp**C10)
      else if (temp .le. TE12) then
        qq = scale*(H11*temp**C11)
      else if (temp .le. TE13) then
        qq = scale*(H12*temp**C12)
      else if (temp .le. TE14) then
        qq = scale*(H13*temp**C13)
      else
        qq = scale*(H14*temp**C14)
      end if
      src(ix,iy,iz) = stef*(temp*temp*temp*temp)
      rkap(ix,iy,iz) = qq*rho(ix,iy,iz)**2/(4.*pi*src(ix,iy,iz))
    end do
    end do
  end do

  call print_trace (id, dbg_cool, 'END')
END SUBROUTINE

