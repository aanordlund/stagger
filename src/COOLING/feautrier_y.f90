!***********************************************************************
SUBROUTINE radiation (np,n1,tau,s,q,xi,doxi)
!
!  Solve the transfer equation, given optical depth and source function.
!
!  "Steins trick" is used; storing the sum of the three elements in each
!  row of the tridiagonal equation system, instead of the diagonal ele-
!  ment.  This allows accurate solutions to be otained for arbitrarily
!  small optical depths.  Without this trick, round off errors become
!  noticeable already when the smallest optical depth is less than the
!  square root of the machine precisions.
!
!  This version works with P as a variable for k <= n1, and with
!  Q = P - S for k > n1.  P is Feautriers P and S is the source function.
!  The equation solved is  Q" = P - S for k<=n1, and Q" = Q - S" for k>n1.
!  The answer returned is Q = P - S for all k.
!
!  This choice of equations produces a high precision in Q at large optical
!  depths (where P - S would have round off), while at the same time
!  retaining the precision at small optical depths (where S" becomes very
!  large and eventually would cause round off errors in the back
!  substitution).
!
!  This is a second order version.  For simulations, with rapidly
!  varying absorption coefficients and source functions, this is to be
!  preferred over spline and Hermitean versions because it is positive
!  definite, in the sense that a positive source function is guaranteed
!  to result in a positive average intensity p.  Also, the flux
!  divergence is exactly equal to q, for the conventional definition
!  of the flux.
!
!  Operation count: 5d+5m+11a = 21 flops
!
!  Update history:  This routine is based on transq, with updates
!
!***********************************************************************
!
  USE params
  implicit none
!
  real, dimension(mx,my,mz) :: tau,s,q
  real, dimension(mx,my)    :: sp1,sp2,sp3
  real, dimension(mx,mz)    :: xi
  real, dimension(mx)       :: ex
  integer :: k, l, n, np, n1p, n1
  real :: taum, dtau2, dinv
  logical doxi
!
  character(len=80):: id='$Id: feautrier_y.f90,v 1.8 2006/10/29 18:39:08 aake Exp $'

!$omp master
  if (id.ne.' ') print *,id ; id=' '
!$omp end master
!
!  Calculate 1-exp(-tau(l,1,n)) straight or by series expansion,
!  based on max tau(l,1,n).
!
!$omp do private(n,l,k,n1p,taum,ex,dtau2,dinv,sp1,sp2,sp3)
  do n=1,mz
  taum=tau(1,1,n)
  do l=1,mx
    taum=amax1(taum,tau(l,1,n))
  end do
  if (taum.gt.0.1) then
    do l=1,mx
      ex(l)=1.-exp(-tau(l,1,n))
    end do
  else
    do l=1,mx
      ex(l)=tau(l,1,n) &
        *(1.-.5*tau(l,1,n)*(1.-.3333*tau(l,1,n)))
    end do
  endif
!
!  Boundary condition at k=1.
!
  do l=1,mx
    dtau2=tau(l,2,n)-tau(l,1,n)
    sp2(l,1)=dtau2*(1.+.5*dtau2)
    sp3(l,1)=-1.
    q(l,1,n)=s(l,1,n)*dtau2*(.5*dtau2+ex(l))
  end do
!
!  Matrix elements for k>2, k<np: [3d+3s]
!
  do k=2,np-1
  do l=1,mx
    dinv=2./(tau(l,k+1,n)-tau(l,k-1,n))
    sp1(l,k)=dinv/(tau(l,k-1,n)-tau(l,k,n))
    sp2(l,k)=1.
    sp3(l,k)=dinv/(tau(l,k,n)-tau(l,k+1,n))
  end do
  end do
!
!  Right hand sides, for k>2, k<n1
!
  n1p=min0(max0(n1,2),np-3)
  do k=2,n1p-1
  do l=1,mx
    q(l,k,n)=s(l,k,n)
  end do
  end do
!
!  RHS for k=n1p.  The equation is still in p(k), but q(k+1) is now
!  q(k+1)=p(k+1)-s(k+1), so to get p(k+1) we have to add s(k+1) to the
!  LHS, that is subtract it from the RHS.
!
  if (n1p.le.np) then
    do l=1,mx
      q(l,n1p,n)=s(l,n1p,n)-sp3(l,n1p)*s(l,n1p+1,n)
    end do
  endif
!
!  RHS for k=n1p+1.  The equation is now in Q: Q" = Q - S", but q(k-1)
!  is now p(k-1)=q(k-1)+s(k-1), so we must add sp1*s(l,k-1,n) to the RHS,
!  relative to the case below.
!
  if (n1p+1.le.np) then
    do l=1,mx
      q(l,n1p+1,n)=sp1(l,n1p+1)*s(l,n1p+1,n) &
                  +sp3(l,n1p+1)*(s(l,n1p+1,n)-s(l,n1p+2,n))
    end do
  endif
!
!  k>n1p+1,<np [2m+2a]
!
  do k=n1p+2,np-1
  do l=1,mx
    q(l,k,n)=sp1(l,k)*(s(l,k,n)-s(l,k-1,n)) &
            +sp3(l,k)*(s(l,k,n)-s(l,k+1,n))
  end do
  end do
!
!  k=n
!
  do l=1,mx
    sp2(l,np)=1.
    q(l,np,n)=0.0
  end do
!
!  Eliminate subdiagonal, save factors in sp1 [1d+2m+4a]
!
  do k=1,np-2
  do l=1,mx
    sp1(l,k)=-sp1(l,k+1)/(sp2(l,k)-sp3(l,k))
    q(l,k+1,n)=q(l,k+1,n)+sp1(l,k)*q(l,k,n)
    sp2(l,k+1)=sp2(l,k+1)+sp1(l,k)*sp2(l,k)
    sp2(l,k)=sp2(l,k)-sp3(l,k)
!         0.14+0.13+0.07+0.09 sec -> 0.58*7/0.43 = 9.4 Mfl
  end do
  end do
  do l=1,mx
    sp2(l,np-1)=sp2(l,np-1)-sp3(l,np-1)
  end do
!
!  Backsubstitute [1d+1m+1a]
!
  do k=np-1,1,-1
  do l=1,mx
    q(l,k,n)=(q(l,k,n)-sp3(l,k)*q(l,k+1,n))/sp2(l,k)
!         0.19 sec = 580444*3/0.19 = 9.2 Mfl
  end do
  end do
!
!  Subtract S in the region where the equation is in P
!
  do k=1,n1p
    do l=1,mx
      q(l,k,n)=q(l,k,n)-s(l,k,n)
    end do
  end do
!
!  Surface intensity.
!
  if (doxi) then
    do l=1,mx
      xi(l,n)=(1.-ex(l))*(q(l,1,n)+s(l,1,n)) &
         +s(l,1,n)*0.5*ex(l)**2
    end do
  end if

  end do
!$omp end do
!
END
