! $Id: ISM-cooling-Rosen-etal.f90,v 1.13 2005/01/13 13:24:04 aake Exp $
!***********************************************************************
MODULE cooling

! Note: if initial conditions are set such that [Z] could be less than
! 1.E-04, the lowest metallicity used in MODULE cooling must be set 
! to 1.E-04

! The units of lamda are energy per unit volume and time, or cdu*cvu^2/ctu,
! which the computed, dimensional value should be divided by.  But since
! a multiplicative factor of cdu^2 should also be applied, the computed
! value should be divided by cvu^2/(ctu*cdu), or multiplied by ctu*cdu/cvu^2.

  use units

  logical do_table, do_power
  character (len=40) coolH2file,tablefile

  real, allocatable, dimension(:,:):: table
  real:: a0,b0,a1,b1,tab0,Tlower
  integer:: ma,mb
END MODULE

!***********************************************************************
SUBROUTINE init_cooling (r,ee)
  USE params
  USE cooling
  implicit none
  real, dimension(mx,my,mz):: r,ee
  character(len=80):: id='$Id: ISM-cooling-Rosen-etal.f90,v 1.13 2005/01/13 13:24:04 aake Exp $'
  if (id.ne.'') print *,id ; id = ''

  tablefile = file(1:index(file,'.'))//'eos'
  if (do_trace) print *,'init_cooling: read'
  do_cool = .true.
  do_table = .false.
  do_power = .false.
  Tlower = 1000.
  ma = mx ; mb = my

  call read_cooling

  allocate (table(ma,mb))
  a0=50. ; a1=10.                       ! make sure table is invalid
  b0=50. ; b1=10.
END SUBROUTINE

!***********************************************************************
SUBROUTINE read_cooling
  USE params
  USE cooling
  implicit none
  namelist /cool/do_cool,do_table,do_power,Tlower,ma,mb,coolH2file

  if (stdin.ge.0) then
    rewind (stdin); read (stdin,cool)
  else
    read (*,cool)
  end if
  write (*,cool)
END SUBROUTINE

!***********************************************************************
SUBROUTINE coolit_table (r,ee,lne,dedt)

  USE params
  USE cooling
  implicit none
  real, dimension(mx,my,mz):: r,ee,lne
  real, dimension(mx,my,mz):: dedt
  integer:: ix,iy,iz,ia,ib,ic
  real:: a,b,c,pa,pb,pc,qa,qb,qc,lntab,tab,lamb,ct
  real:: rmin,rmax,lnemin,lnemax,eemin,eemax

  if (.not. do_cool) return
  if (do_trace) print *,'coolit_table'

    rmin=minval(r  ) ;   rmax=maxval(r  )
  lnemin=minval(lne) ; lnemax=maxval(lne)
   eemin=exp(lnemin) ;  eemax=exp(lnemax)

  if ( rmin.lt.exp(a0) .or.  rmax.gt.exp(a0+(ma-1)/a1) .or. &
      eemin.lt.exp(b0) .or. eemax.gt.exp(b0+(mb-1)/b1)) then
        call new_table (r,ee,lne)
  end if
  ct = 200./min(dx,dy,dz)   ! Cooling rate constant, 200 km/s fiducial speed
  ct = 0.5*Cdt/dt

!$omp parallel do private(ix,iy,iz,a,b,ia,ib,pa,pb,qa,qb,tab,lamb)
  do iz=1,mz
    do iy=1,my
      do ix=1,mx
        a = 1.+(alog(r(ix,iy,iz))  - a0)*a1
        b = 1.+(lne(ix,iy,iz)      - b0)*b1
        ia = min(max(1,ifix(a)),ma-1)
        ib = min(max(1,ifix(b)),mb-1)
        pa = a-ia ; qa=1.-pa
        pb = b-ib ; qb=1.-pb
        tab = qb*(Qa*table(ia  ,ib  ) + pa*table(ia+1,ib  ))  + &
              pb*(qa*table(ia  ,ib+1) + pa*table(ia+1,ib+1))
        tab = tab+tab0
        if (do_power) tab=tab**5
        lamb = tab*r(ix,iy,iz)**2
        lamb = sign(min(abs(lamb),ee(ix,iy,iz)*ct),lamb)
        dedt(ix,iy,iz) = dedt(ix,iy,iz) - lamb
      end do
    end do
  end do
END SUBROUTINE

!***********************************************************************
SUBROUTINE new_table (r,ee,lne)
  USE params
  USE cooling
  implicit none
  real, dimension(mx,my,mz):: r,ee,lne,dedt
  real, allocatable, dimension(:,:):: rtab,etab,ltab
  real:: tabmin,tabmax
  integer:: ix,iy,iz
!
!  New table
!
  print *,'========================== new_table ============================='
  allocate (rtab(ma,mb), etab(ma,mb), ltab(ma,mb))
!
!  Limits and steps in ln(rho), ln(ee)
!
  a0 = minval(alog(r) ) - 1. ; a1 = maxval(alog(r) ) + 1. ; a1 = (a1-a0)/(ma-1)
  b0 = minval(lne     ) - 1. ; b1 = maxval(lne     ) + 1. ; b1 = (b1-b0)/(mb-1)
!
!  Initialize to reasonable values
!
  rtab = exp(a0+a1*ma/2)
  ltab = b0+b1*mb/2
  etab = rtab*exp(ltab)
  table = 0.
!
!  Loop over table
!
    do iy=1,mb
      ltab(:,iy) = b0+(iy-1)*b1
      do ix=1,ma
        rtab(ix,iy) = exp(a0+(ix-1)*a1)
        etab(ix,iy) = rtab(ix,iy)*exp(ltab(ix,iy))
      end do
    end do
!
!  Calculate the cooling
!
  call coolit_real (ma,mb,rtab,etab,ltab,table)
  tabmin=minval(table)
  tabmax=maxval(table)
  tab0=tabmin-0.001*abs(tabmin)
  if (tab0.eq.0.) tab0=-1e-5*tabmax
  if (do_power) table=sign(abs(table)**0.2,table)
!
!  Take out the main density dependence, and invert steps
!
!  table = log((table-tab0))
  a1=1./a1 ; b1=1./b1
!
!  Write the table file
!
  open (3,file=tablefile,form='unformatted',status='unknown')
  write (3) ma,mb,a0,b0,a1,b1,tab0
  write (3) ((table(ix,iy), ix=1,ma), iy=1,mb)
  close (3)
  deallocate (rtab, etab, ltab)

  print *,'dlnrho, dlnee, tab0 =',a1,b1,tab0
  print *,'=================================================================='

END SUBROUTINE

!***********************************************************************
SUBROUTINE coolit_real (na,nb,r,ee,lne,lambda)

  USE params
  USE eos
  USE units
  USE cooling
  implicit none
  integer ix,iy,na,nb
  real, dimension(na,nb):: r,ee,lne,lambda
  real temp, qq
  
  real, parameter:: &
    TE1=1000.0, &
    TE2=2000.0, &
    TE3=8000.0, &
    TE4=1.0e5, &
    TE5=4.0e7, &
    C1=2.0, &
    C2=1.5, &
    C3=2.867, &
    C4=-0.65, &
    C5=0.5, &
    H1=2.2380e-12/4e-28, &
    H2=1.0012e-10/4e-28, &
    H3=4.6240e-16/4e-28, &
    H4=1.7800e+02/4e-28, &
    H5=3.2217e-07/4e-28
  real, parameter:: scale=(ctu/clu)**2*(ctu*cdu)

  if (it.eq.0 .and. isubstep.eq.1) print *,'scale=',scale

  do iy=1,nb
  do ix=1,na
    temp = utemp*ee(ix,iy)
    if      (temp .le. Tlower) then
      qq = 0.
    else if (temp .le. TE2) then
      qq = scale*(H1*temp**C1)
    else if (temp .le. TE3) then
      qq = scale*(H2*temp**C2)
    else if (temp .le. TE4) then
      qq = scale*(H3*temp**C3)
    else if (temp .le. TE5) then
      qq = scale*(H4*temp**C4)
    else
      qq = scale*(H5*temp**C5)
    end if
    lambda(ix,iy)=qq
  end do
  end do
END SUBROUTINE

!***********************************************************************
SUBROUTINE coolit (r,ee,lne,dd,dedt)
!
!  Wrapper routine, to allow calling the real routine from new_table
!
  USE params
  USE eos
  USE cooling
  implicit none
  real, dimension(mx,my,mz):: r,ee,lne,dd,dedt
  real, dimension(mx,my):: qq
  integer ix,iy,iz
  real Ct
!
  if (do_trace) print *,'coolit'
  if (do_table) then
    call coolit_table (r,ee,lne,dedt)
  else
    ct = 0.5*Cdt/dt
!$omp parallel do private(ix,iy,iz,qq)
    do iz=1,mz
      call coolit_real (mx,my,r(:,:,iz),ee(:,:,iz),lne(:,:,iz),qq)
      do iy=1,my
      do ix=1,mx
        qq(ix,iy) = qq(ix,iy)*r(ix,iy,iz)**2
        qq(ix,iy) = sign(min(abs(qq(ix,iy)),r(ix,iy,iz)*ee(ix,iy,iz)*ct),qq(ix,iy))
        dedt(ix,iy,iz) = dedt(ix,iy,iz) - qq(ix,iy)
      end do
      end do
    end do
  end if
!
END SUBROUTINE

