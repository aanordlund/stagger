! $Id: radiation_reshape_rk.f90,v 1.2 2014/12/30 21:28:20 aake Exp $
!***********************************************************************
! To minimize the number of depth points used in the RT:
!
! For the initial, smallest inclination, start by using all points       ny = my
! For ibin=1, start with the estimate for the current inclination mu     ny = ny_omega
! For all ibin, reduce ny based on dtau                                  ny = min(ny,n2)
! For ibin=1, remember the largest number that was neeeded for any phi   ny_phi = max(ny_phi, ny)
!   [ use a global max over MPI, in absolute jy coordinates ]
! Record the largest number of depths needed, for any phi, and ibin=1    ny_omega = max(ny_omega, ny_phi)
! For the next inclination, start with the value for previous one        ny_rad = ny_omega
! 
! ny_omega = my+iyoff                                   ! global value, initially
! do imu=lmu,1,-1
!   ny_phi = 1                                          ! to record max
!   do iphi = 1,nphi
!     ny = ny_omega-iyoff                               ! local MPI-domain value
!     call lookup (ny, ...)                             ! decreasing with inclination
!     do ibin=1,nbin
!       call dtau_calc (ny, n2, ...)                    ! smallest iy with dtau > dtaumax
!       ny = min(ny,n2)
!       call transfer (ny,...)                          ! only where dtau < dtaumax
!       accumulate Q for iy=1:ny-1 only                 ! no contrib at ny
!       if (ibin==1) ny_phi=max(ny_phi,ny+iyoff)        ! largest ny needed for ibin=1 and any phi
!     end do ! ibin
!     accumulate Q over omega for iy=1:ny_phi-1         ! where there are contributions
!   end do ! iphi
!   call max_int_mpi (ny_phi)                           ! max over MPI-domains
!   ny_omega = ny_phi                                   ! default for next larger inclination
! end do ! imu
!
!***********************************************************************
MODULE cooling

  USE params

  integer, parameter:: mmu=9, mphi=10
  real    dphi, form_factor, dtaumin, dtaumax
  integer nmu, nphi, verbose, ny0
  integer mblocks
  integer n1, n2
  integer,save:: truncate=0

  real y_newton, dy_newton, t_newton, ee_newton, ee_min, t_ee_min

  real, dimension(mmu):: mu0

  real, pointer, dimension(:,:,:) :: rk, lnrk, s, q, dtau, qq
  real, pointer, dimension(:,:,:) :: tau

  ! lnrkap: used to store untilted ln absorption coefficient (lnrk)
  real, pointer, dimension(:,:,:) :: lnrkap

  real, allocatable, target, dimension(:,:,:) :: scr7, scr8

  logical do_table, do_newton, do_limb, do_fld
  character(len=mfile):: intfile

END MODULE cooling


!***********************************************************************
SUBROUTINE init_cooling (r,e)

  USE params
  USE arrays
  USE cooling

  implicit none

  real, dimension(mx,my,mz):: r,e

  integer i
  character(len=mfile):: name

  !if (mpi_ny > 1) then
  !  if (master) print*,'ERROR: this RT version needs mpi_ny=1'
  !  call abort_mpi
  !end if

  do_cool     = .true.
  i           = index(file,'.')
  nmu         = 0
  nphi        = 4
  dphi        = 0.
  form_factor = 1.
  dtaumax     = 100.
  dtaumin     = 0.1
  intfile     = name('intensity.dat','int',file)

  do_newton   = .false.
  y_newton    = -0.3
  dy_newton   = 0.05
  t_newton    = 0.01
  t_ee_min    = 0.005
  ee_newton   = -1.
  ee_min      = 3.6
  do_limb     = .false.
  verbose     = 0
  ny0         = 0

  ! mblocks = number of subdomains communicated per side in old trnslt routine (deprecated!)
  mblocks     = 1
  do_fld      = .false.

  call read_cooling

  rk   => scr1
  lnrk => scr2
  s    => scr3
  q    => scr4
  dtau => scr5
  qq   => scr6

  allocate( scr7(mx,my,mz) )
  allocate( scr8(mx,my,mz) )
  tau    => scr7
  lnrkap => scr8

END SUBROUTINE init_cooling


!******************************************************************************
SUBROUTINE read_cooling
  USE params
  USE eos
  USE cooling
  implicit none
  namelist /cool/do_cool, nmu, nphi, dphi, form_factor, dtaumin, dtaumax, intfile, &
       do_newton, y_newton, dy_newton, t_newton, ee_newton, ee_min, t_ee_min, do_limb, mu0, &
       ny0, verbose, mblocks, do_fld, timer_verbose, truncate

  character(len=mfile):: name, fname
  character(len=mid):: id='$Id: radiation_reshape_rk.f90,v 1.2 2014/12/30 21:28:20 aake Exp $'
!...............................................................................
  call print_id(id)

  rewind (stdin); read (stdin,cool)

  if (nmu==0 .and. form_factor==1.) form_factor=0.4

  if (master) write (*,cool)

  if (master) write (*,*) 'mbox, dbox =', mbox, dbox

  if (do_limb) then
     fname = name('limb.dat','lmb',file)
     call mpi_name(fname)
     open (limb_unit, file=trim(fname), &
          form='unformatted', status='unknown')
     write (limb_unit) mx, mz,nmu,nphi
  end if

END SUBROUTINE read_cooling


!***********************************************************************
SUBROUTINE coolit (r, ee, lne, dd, dedt)

  USE params
  USE units
  USE eos
  USE cooling
  USE mpi_mod, only: mpi_comm_plane, mpi_comm_beam, mpi_comm_world, mpi_err, &
                     MPI_STATUS_SIZE, MPI_INTEGER, MPI_MAX

  implicit none

  character(len=mid)  :: id='$Id: radiation_reshape_rk.f90,v 1.2 2014/12/30 21:28:20 aake Exp $'

  real, dimension(mx,my,mz) :: r, ee, lne, dd, dedt
  real, dimension(mx,mz)    :: surfi
 
  ! ym0, ymN1: values of ym depth in the layers immediately above and below local MPI subdomain
  ! dtau0, dtauN1: values of dtau immediately above and below local subdomain
  real                      :: ym0,   ymN1          
  real, dimension(mx,1,mz)  :: dtau0, dtauN1

  ! tauIntr: intrinsic optical depth scale
  real, dimension(mx,my,mz) :: tauIntr

  real    xmu(mmu), wmu(mmu)
  real    sec, fdtime, dxdy, dzdy, tanth, qqTmp, f1, f2
  real    phi, wphi, womega

  integer ix, iy, iz, lmu, lphi, imu, iphi, ibox, n1p, n2p, ny, nytot
  integer lrec, imaxval_mpi, nyr
  integer ny_omega, ny_phi, nyprv, imax, ny_trnslt
  integer status(MPI_STATUS_SIZE), nr, ny_tmp(mpi_size), req(mpi_size), rank

  integer  mTauTop, mTauBot
  integer  nDTauTop, nDTauBot

  logical, save :: first=.true.
  logical       :: flag_scr, flag_snap, flag_limb, flag_surf, do_io, do_surfi, io_flag
  logical       :: debug2
  real          :: cput(2), void, dtime, prof
  integer, save :: nrad=0, nang=0, ny_prev=10000
  character(len=mfile) :: name, fname
  real, allocatable, dimension(:,:,:):: lnr_ray, ee_ray, lnr, qq1
  external transfer
!.......................................................................

  ! print subroutine's ID
  call print_id(id)

  ! skip ratiative transfer ?
  if (.not. do_cool) return
                                                                                call timer('radiation','init')

  ! reset dedt in ghost zones, if present (lb > 1)
  if (lb > 1) then
     dedt(:,1:lb-1,izs:ize) = 0.
  end if

  !$omp barrier
  !$omp master
  allocate (lnr_ray(mx,my,mz), ee_ray(mx,my,mz), lnr(mx,my,mz), qq1(mx,my,mz))
  !$omp end master
  !$omp barrier

  do iz=izs,ize
    lnr(:,:,iz)=alog(r(:,:,iz))
    qq1(:,:,iz)=0.0
  end do

!-----------------------------------------------------------------------
!  Angular quadrature
!  nmu > 0: Use Gauss integration (mu=1 excluded) with 2*nmu   order accuracy
!  nmu = 0: One vertical ray with weight 0.5
!  nmu < 0: Use Radau integration (mu=1 included) with 2*nmu-1 order accuracy
!-----------------------------------------------------------------------

  if      (nmu .gt. 0) then
     lmu = nmu
     call gausi (lmu,0.,1.,wmu,xmu)
  else if (nmu .eq. 0) then
     lmu = 1
     xmu(1) = 1.
     wmu(1) = 0.5
  else 
     lmu =-nmu
     call radaui(lmu,0.,1.,wmu,xmu)
  end if
  if (mu0(1) .ne. 0.) xmu=mu0

  if (first) then
     call barrier_omp ('rad-first')
     if (omp_master) first = .false.
     if (master) then
        print *,' imu       xmu    theta'
        !           1.....0.000.....00.0
        do imu=1,lmu 
           print '(i4,f10.3,f8.1,f10.3)',imu,xmu(imu),acos(xmu(imu))*180./pi,wmu(imu)
        end do
     end if
  end if
                                           call timer('radiation',     'prelim')
                                           !call dumpn ( r,  'r', 'resh.dmp', 0)
                                           !call dumpn (ee, 'ee', 'resh.dmp', 1)
  ! loop over angles
  ny_omega = min(ny_prev+2,mytot)                                               ! global index
  do imu=lmu,1,-1
     if (nmu .eq. 0 .and. imu .eq. 1) then
        lphi = 1 
     else if (nmu .lt. 0 .and. imu .eq. lmu) then
        lphi = 1 
     else
        lphi = nphi
     end if

     tanth = tan(acos(xmu(imu)))
     wphi  = 4.*pi/lphi

     ny_phi = 1                                                                 ! for max depth over phi
     do iphi=1,lphi
        n_omega = n_omega + 1
        nytot = ny_omega                                                        ! same for all ranks
        phi = (iphi-1)*2.*pi/lphi + dphi*t
        dxdy = tanth*cos(phi)
        dzdy = tanth*sin(phi)

        !-----------------------------------------------------------------------
        ! The trnslt routines only use horizontal communicators, so all ranks in
        ! the xz-plane must use the same value of nytot
        !-----------------------------------------------------------------------
        ny = min(nytot-iyoff,my)                                                ! local ny
        ny_trnslt = ny
        call trnslt (lnr, lnr_ray, dxdy, dzdy, .false., ny, mblocks, verbose)   ! local ny
        call trnslt (ee , ee_ray , dxdy, dzdy, .false., ny, mblocks, verbose)   ! local ny
        call dumpn (lnr_ray,'lnr_ray','trf.dmp',1)
        call dumpn (ee_ray,'ee_ray','trf.dmp',1)
                                           call timer('radiation','trnslt(lnr)')
        do iz=izs,ize
           qq(:,:,iz) = 0.
        end do
                                           call timer('radiation',      'reset')
                                                                                
        n1 = nytot                                                              ! global ny
        do ibox=1,mbox
           call dumpn (s,'s','trf.dmp',1)

           ! flags for data output 
           flag_scr  = do_io(t+dt, tscr, iscr+iscr0,   nscr)
           flag_snap = do_io(t+dt, tsnap,isnap+isnap0, nsnap)
           io_flag   = flag_scr .or. flag_snap

           ! flag_limb: limb darkening every scratch 
           ! or else every nsnap and only mu=1
           ! and only for first substep and bin:
           flag_limb = do_limb   .and. flag_scr
           flag_limb = flag_limb .or.  (xmu(imu)==1. .and. flag_snap)
           flag_limb = flag_limb .and. isubstep.eq.1 .and. ibox.eq.1

           ! compute surface intensity ?
           flag_surf = flag_scr .or. flag_snap
           flag_surf = flag_surf .and. xmu(imu)==1. .and. isubstep.eq.1 
           do_surfi  = flag_surf.or.flag_limb

           nyprv = nytot                                                        ! global ny
                                               call timer('radiation',    'pre')
          !-----------------------------------------------------------------------
          ! The trasnfer routines use vertical ray communicators to reshape the
          ! arrays into full height (nytot) size.  In principle this should be
          ! doable separately for each (mpi_x,mpi_z) pair, which thus should not
          ! need to have a globally consistent nytot value.
          !-----------------------------------------------------------------------
           call transfer_lnrk (ibox, nytot, n1, lnr_ray, ee_ray, q, &
                               surfi, do_surfi, xmu(imu))
           ny = min(nytot-iyoff,my)                                             ! local ny

           if ( flag_limb ) then
              call barrier_omp('limb')
              if (do_limb .and. omp_master) then
                 write(limb_unit) t,imu,iphi,xmu(imu),phi,surfi
                 print *,omp_mythread,isubstep,ibox,imu,iphi
              end if
           end if

           ! integrated surface intensity
           if (flag_surf) then
              if (ibox .eq. 1) then      ! overwrite surface_int
                 do iz=izs,ize
                    surface_int(:,iz) = surfi(:,iz)
                 end do
              else                       ! add to surface_int
                 do iz=izs,ize
                    surface_int(:,iz) = surface_int(:,iz)+surfi(:,iz)
                 end do
              end if
           end if

           if (ibox == 1) then
              ny_phi = max(nytot,ny_phi)                                        ! largest needed for any phi and ibox==1
           end if
                                               call timer('radiation',   'post')
           if (master .and. debug2(dbg_rad,2)) &
             print '(a, 7i6)', ' ibox, iphi, n1, ny, nytot, ny_phi, ny_trnslt =', &
                                 ibox, iphi, n1, ny, nytot, ny_phi, ny_trnslt
        end do ! ibox=1,nbox

        !-----------------------------------------------------------------------
        ! energy equation; q has already been converted to per unit volume and
        ! time, and has been summed over ibox
        !-----------------------------------------------------------------------
        ny = ny_trnslt                                                          ! previously used
        call trnslt (q, qq, -dxdy, -dzdy, .false., ny, mblocks, verbose)
                                             call timer('radiation','trnslt(q)')
        call dumpn (qq,'qq','trf.dmp',1)
        call dumpn (qq, 'qq', 'resh.dmp', 1)
        f1 = wmu(imu)*wphi*form_factor
        do iz=izs,ize
           do iy=1,ny
              qq1(:,iy,iz) =  qq1(:,iy,iz) + f1*qq(:,iy,iz)                     ! only for dumps
             dedt(:,iy,iz) = dedt(:,iy,iz) + f1*qq(:,iy,iz)
           end do
        end do
        call dumpn ( qq1, 'qq1','resh.dmp',1)
                                             call timer('radiation',   'sum(q)')
     end do ! iphi=1,lphi
     call max_int_mpi (ny_phi)
     ny_omega = ny_phi          ! initial estimate for next larger inclination
     if (imu == lmu) ny_prev = ny_omega
                                              call timer('radiation', 'max_int')
     if (master .and. debug2(dbg_rad,2)) &
       print '(a, 7i6)', ' imu, ny_phi, ny_prev =', &
                           imu, ny_phi, ny_prev
  end do ! imu=lmu,1,-1

  call radiative_flux (qq1,q)
                                              call timer('radiation',    'flux')
  if (do_newton) then
     if (ee_newton > 0) eetop = ee_newton
     do iz=izs,ize
        do iy=1,my
           prof = exp(min((y_newton-ym(iy))/dy_newton,70.))
           prof = prof/(1.+prof)
           do ix=1,mx
              dedt(ix,iy,iz) = dedt(ix,iy,iz) - r(ix,iy,iz)*(ee(ix,iy,iz)-eetop)/t_newton*prof &
                                              - r(ix,iy,iz)*min(0.,ee(ix,iy,iz)-ee_min)/t_ee_min
           end do
        end do
     end do
  end if
                                               call timer('radiation', 'newton')
  !$omp barrier
  !$omp master
  deallocate (lnr_ray, ee_ray, lnr, qq1)
  call buffer_count
  !$omp end master
END SUBROUTINE coolit
