!***********************************************************************
SUBROUTINE transfer_lnrk (ibox, n2, n1, lnr, ee, q, xi, doxi, mu)
!
! 1) Split the z-direction into mpi_ny pieces with dimension (mx,my,lz),
! where the last piece may be shorter than the other ones.  Send these
! blocks to the rank they are assigned to, for the next step of 
! processing:
!
! 2) For each ray, compute the dtau(ix,iy,iz), and note the last iy 
! index where dtau < dtaumax.  Let n2 be the next iy index, so it's the
! first one with dtau > dtaumax.  Since we wish to vectorize over the
! x-direction, we need to do this (as a minimum) over all ix, so n2 is
! possibly different for each iz -- this can potentially save a lot
! of time, since the worst iz may be one with a deep "hole" with low
! optical depth, while most iz-planes have much larger optical depths.
!
! Collect the largest n2 for all z-planes, and take a global max of
! these over phi, to determine how deep we need to go when calling the
! lookup ! table for the next larger angle of inclination.  There is
! no need to maintain any other global depth index, since the dtaumax
! criterion is implemented locally
!
! 3) Solve the radiative transfer eqution
!
! 4) Ship the resulting Q-values back to the MHD domain
!
! Special consideration needs to be given to the (common) case when one
! or more ranks in the y-direction are empty.  This means no data from
! these ranks are needed, so none should be sent, nor expected.  For this
! to work, the send and receive loops need to be up-to-date on the value
! of n2; the deepest index we need to care about.  n2 is counted in 
! global y-coordinates, so if it is smaller than iyoff then the rank is
! not needed.
!
!***********************************************************************
  USE params, only: mx, my, mz, mpi_rank, mpi_ny, mpi_y, dbg_rad, &
                    n_lookup, n_reshape, n_deshape, n_transfer, &
                    timer_verbose, surface_int
  USE eos, only: mbox
  USE reshape_m
  implicit none
  integer ibox, n2, n1, i, ny, lz, nz
  real mu
  real, dimension(mx,my,mz) :: lnr, ee, q
  real, dimension(mx,mz) :: xi
  logical doxi, omp_in_parallel, debug2
  real, allocatable, dimension(:,:), save:: xi1,xi2
  real, allocatable, dimension(:,:,:), save:: lnr1,ee1,lnrk1,q1,q2
  real, allocatable, dimension(:,:,:,:), save:: lns1
  character(len=mid), save:: id='$Id: transfer_reshape_lnrk.f90,v 1.16 2015/06/21 20:06:42 aake Exp $'
  integer, save:: n2_box1
!...............................................................................
  call print_id (id)
  call reshape_size (mpi_y, ny, nz, lz)                                         ! our size
                                          call timer('radiation','reshape_size')
  if (ibox == 1) then
    allocate (  xi1(mx,nz))
    allocate (  xi2(mx,mz))
    allocate ( lnr1(mx,nz,ny))
    allocate (  ee1(mx,nz,ny))
    allocate (lnrk1(mx,nz,ny))
    allocate (   q1(mx,nz,ny))
    allocate (   q2(mx,nz,ny))
    allocate ( lns1(mx,nz,ny,mbox))
                       if (timer_verbose > 0) call timer('radiation','allocate')
    q2=0.0
                       if (timer_verbose > 0) call timer('radiation',    'q2=0')
    if (debug2(dbg_rad,6)) &
      print*,mpi_rank,' reshape called for ibox==1 with n2 =', n2
    call max_int_mpi_comm (n2, mpi_comm_beam(2))                                ! ensure agreement
    if (debug2(dbg_rad,6)) &
      print*,mpi_rank,' reshape called for ibox==1 with n2 =', n2
                      if (timer_verbose > 0) call timer('radiation','max_int_c')
    call reshape_yz (lnr, lnr1, ny, lz, n2)                                     ! reshape arrays
    call reshape_yz ( ee,  ee1, ny, lz, n2)
                                               call timer('radiation','reshape')
    call lookup_reshape (mx, ny, nz, lnr1, ee1, lnrk1, lns1, n2, lz)
                                               call timer('radiation', 'lookup')
                                       call dumpn ( lnr1,  'lnr', 'resh.dmp', 1)
                                       call dumpn (  ee1,   'ee', 'resh.dmp', 1)
                                       call dumpn (lnrk1, 'lnrk', 'resh.dmp', 1)
    deallocate (lnr1, ee1)
  end if
  if (omp_in_parallel()) then
    call feautrier_omp (ibox, mx, ny, nz, lz, n2, n1, lnrk1, &
                        lns1(:,:,:,ibox), q1, xi1, doxi, mu)
  else
    !$omp parallel
    call feautrier_omp (ibox, mx, ny, nz, lz, n2, n1, lnrk1, &
                        lns1(:,:,:,ibox), q1, xi1, doxi, mu)
    !$omp end parallel
  end if
  if (ibox == 1) n2_box1 = n2
  n_transfer = n_transfer + mx*n2*lz
  q2(:,:,2:n2) = q2(:,:,2:n2) + q1(:,:,2:n2)                                                                  ! accumulate
  if (ibox == 1) then
    q2(:,:,1) = xi1(:,:)
  end if
                                             call timer('radiation','feautrier')
  if (ibox == mbox) then
    call max_int_mpi_comm (n2_box1, mpi_comm_beam(2))                           ! make sure agreement on n2
    if (debug2(dbg_rad,6)) &
      print*,mpi_rank,' deshape called for ibox==mbox with n2 =', n2_box1
    call deshape_yz (q, q2, ny, lz, n2_box1)
                                          !call dumpn (  q, 'q2', 'resh.dmp', 1)
                                             call timer('radiation',  'deshape')
    if (doxi .and. mpi_y==0) then
      xi(:,:) = q(:,1,:)                                                        ! pick up piggy back
      q(:,1,:) = q(:,2,:)                                                       ! should not be used anyway
      surface_int(:,:) = xi(:,:)
    end if
    deallocate (lnrk1, lns1, q1, q2, xi1, xi2)
                                             call timer('radiation',  'dealloc')
  end if
END

!***********************************************************************
SUBROUTINE feautrier_omp (ibox, nx, ny, nz, lz, n2in, n1in, lnrk, lns, q, xi, doxi, mu)
!
!  Solve the transfer equation, given optical depth and source function.
!
!  "Steins trick" is used; storing the sum of the three elements in each
!  row of the tridiagonal equation system, instead of the diagonal ele-
!  ment.  This allows accurate solutions to be otained for arbitrarily
!  small optical depths.  Without this trick, round off errors become
!  noticeable already when the smallest optical depth is less than the
!  square root of the machine precisions.
!
!  This version works with P as a variable for iy <= n1, and with
!  Q = P - S for iy > n1.  P is Feautriers P and S is the source function.
!  The equation solved is  Q" = P - S for iy<=n1, and Q" = Q - S" for iy>n1.
!  The answer returned is Q = P - S for all iy.
!
!  This choice of equations produces a high precision in Q at large optical
!  depths (where P - S would have round off), while at the same time
!  retaining the precision at small optical depths (where S" becomes very
!  large and eventually would cause round off errors in the back
!  substitution).
!
!  This is a second order version.  For simulations, with rapidly
!  varying absorption coefficients and source functions, this is to be
!  preferred over spline and Hermitean versions because it is positive
!  definite, in the sense that a positive source function is guaranteed
!  to result in a positive average intensity p.  Also, the flux
!  divergence is exactly equal to q, for the conventional definition
!  of the flux.
!
!  Operation count: 5d+5m+11a = 21 flops
!
!  Update history:  This routine is based on transq, with updates
!
!******************************************************************************
!
  USE params, only: ymg, mytot, mid, master, dbg_rad, mpi_rank
  USE cooling, only: dtaumax, dtaumin
  USE eos, only: mbox, dbox
  implicit none
!
  integer:: ibox, nx, ny, nz, lz, n2in, n2, n1in, n1, n1max, n2max
  real, dimension(nx,nz,ny):: lnrk, q, lns
  real, dimension(nx,nz):: xi
  real, allocatable, dimension(:):: ex
  real, allocatable, dimension(:,:):: s, sp1, sp2, sp3, rk
  real, allocatable, dimension(:,:,:), save:: dtau
  integer:: ix, iy, iz, n1p
  real:: a, c, taum, dinv, mu
  logical:: doxi, debug2
  character(len=mid):: id='$Id: transfer_reshape_lnrk.f90,v 1.16 2015/06/21 20:06:42 aake Exp $'
!...............................................................................
  call print_id (id)

  allocate (s(nx,ny), sp1(nx,ny), sp2(nx,ny), sp3(nx,ny), rk(nx,ny), ex(nx))
  if (ibox==1) then
    allocate (dtau(nx,nz,ny))
    if (debug2(dbg_rad,1)) dtau = 0.0
    do iz=1,lz
      rk(:,1) = exp(lnrk(:,iz,1))
      c = (ymg(2)-ymg(1))/mu
      dtau(:,iz,1) = c*rk(:,1)
      do iy=2,n2in
        c = (ymg(iy)-ymg(iy-1))*0.5/mu
        rk(:,iy) = exp(lnrk(:,iz,iy))
        dtau(:,iz,iy) = c*(rk(:,iy-1)+rk(:,iy))
      end do
    end do
  else
    c = 10.**dbox
    dtau(:,1:lz,1:n2in) = dtau(:,1:lz,1:n2in)*c
  end if
                                        call dumpn (dtau, 'dtau', 'resh.dmp', 1)
                                        call dumpn ( lns,  'lns', 'resh.dmp', 1)

  if ((master .and. debug2(dbg_rad,5)) .or. debug2(dbg_rad,6)) then 
    print*, ibox, allocated(dtau)
    print '(i6,a,1p,900e10.2)', mpi_rank, ' lnrk =', lnrk(1,1,1:n2in)
    print '(i6,a,1p,900e10.2)', mpi_rank, ' dtau =', dtau(1,1,1:n2in)
    print '(i6,a,1p,900e10.2)', mpi_rank, '  lns =',  lns(1,1,1:n2in)
  end if
!-------------------------------------------------------------------------------
! Calculate 1-exp(-tau(:,1,iz)) straight or by series expansion,
! based on dtau(:,1).
!-------------------------------------------------------------------------------
  n1max = 0
  n2max = 0
  do iz=1,lz
!-------------------------------------------------------------------------------
! Reduce the depth scale locally, in this xy-slice
!-------------------------------------------------------------------------------
    n2 = n2in
    do iy=n2,1,-1
      if (minval(dtau(:,iz,iy)) < dtaumax) exit
    end do
    n2 = max(4,min(iy+1,n2))
    n1 = min(n1in,n2)
    do iy=n1,1,-1
      if (minval(dtau(:,iz,iy)) < dtaumin) exit
    end do
    n1 = max(2,min(iy+1,n1))
    n1max = max(n1,n1max)
    n2max = max(n2,n2max)
    if (master .and. debug2(dbg_rad,3)) &
      print'(a,6i4)', ' iz, ibox, n1, n2, n1max, n2max =', iz, ibox, n1, n2, n1max, n2max
!-------------------------------------------------------------------------------
! Source function, exp only in the part we need
!-------------------------------------------------------------------------------
    do iy=1,n2
      s(:,iy) = exp(lns(:,iz,iy))
    end do
!-------------------------------------------------------------------------------
! Boundary condition at iy=1.
!-------------------------------------------------------------------------------
    ex(:) = 1.-exp(-dtau(:,iz,1))
    sp2(:,1) = dtau(:,iz,2)*(1.+.5*dtau(:,iz,2))
    sp3(:,1) = -1.
    q(:,iz,1) = s(:,1)*dtau(:,iz,2)*(.5*dtau(:,iz,2)+ex(:))
!-------------------------------------------------------------------------------
! Matrix elements for iy>2, iy<n2: [3d+3s]
!-------------------------------------------------------------------------------
    do iy=2,n2-1
      do ix=1,nx
        sp2(ix,iy) = 1.
        dinv = 2./(dtau(ix,iz,iy+1)+dtau(ix,iz,iy))
        sp1(ix,iy) = -dinv/dtau(ix,iz,iy)
        sp3(ix,iy) = -dinv/dtau(ix,iz,iy+1)
      end do
    end do
!-------------------------------------------------------------------------------
! Right hand sides, for iy>2, iy<n1
!-------------------------------------------------------------------------------
    n1p=min0(max0(n1,2),n2-3)
    do iy=2,n1p-1
      q(:,iz,iy) = s(:,iy)
    end do
!-------------------------------------------------------------------------------
! RHS for iy=n1p.  The equation is still in p(iy), but q(iy+1) is now
! q(iy+1)=p(iy+1)-s(iy+1), so to get p(iy+1) we have to add s(iy+1) to the
! LHS, that is subtract it from the RHS.
!-------------------------------------------------------------------------------
    if (n1p.le.n2) then
      q(:,iz,n1p) = s(:,n1p)-sp3(:,n1p)*s(:,n1p+1)
    endif
!-------------------------------------------------------------------------------
! RHS for iy=n1p+1.  The equation is now in Q: Q" = Q - S", but q(iy-1)
! is now p(iy-1)=q(iy-1)+s(iy-1), so we must add sp1*s(:,iy-1,iz) to the RHS,
! relative to the case below.
!-------------------------------------------------------------------------------
    if (n1p+1.le.n2) then
      q(:,iz,n1p+1) = sp1(:,n1p+1)*s(:,n1p+1) &
                    + sp3(:,n1p+1)*(s(:,n1p+1)-s(:,n1p+2))
    endif
!-------------------------------------------------------------------------------
! iy>n1p+1,<n2 [2m+2a]
!-------------------------------------------------------------------------------
    do iy=n1p+2,n2-1
      q(:,iz,iy) = sp1(:,iy)*(s(:,iy)-s(:,iy-1)) &
                 + sp3(:,iy)*(s(:,iy)-s(:,iy+1))
    end do
!-------------------------------------------------------------------------------
! iy=n2
!-------------------------------------------------------------------------------
    sp2(:,n2) = 1.
    q(:,iz,n2) = 0.0
!-------------------------------------------------------------------------------
! Eliminate subdiagonal, save factors in sp1 [1d+2m+4a]
!-------------------------------------------------------------------------------
    do iy=1,n2-2
      do ix=1,nx
        a = -sp1(ix,iy+1)/(sp2(ix,iy)-sp3(ix,iy))
        q(ix,iz,iy+1) = q(ix,iz,iy+1)+a*q(ix,iz,iy)
        sp2(ix,iy+1) = sp2(ix,iy+1)+a*sp2(ix,iy)
        sp2(ix,iy) = sp2(ix,iy)-sp3(ix,iy)
      end do
    end do
    sp2(:,n2-1) = sp2(:,n2-1)-sp3(:,n2-1)
!-------------------------------------------------------------------------------
! Backsubstitute [1d+1m+1a]
!-------------------------------------------------------------------------------
    do iy=n2-1,1,-1
      q(:,iz,iy) = (q(:,iz,iy)-sp3(:,iy)*q(:,iz,iy+1))/sp2(:,iy)
    end do
    q(:,iz,n2:) = 0.0
!-------------------------------------------------------------------------------
! Subtract S in the region where the equation is in P
!------------------------------------------------------------------------------
    do iy=1,n1p
      q(:,iz,iy) = q(:,iz,iy)-s(:,iy)
    end do
!-------------------------------------------------------------------------------
! Surface intensity.
!-------------------------------------------------------------------------------
    if (doxi) then
      xi(:,iz) = 2.*(1.-ex(:))*(q(:,iz,1)+s(:,1))+s(:,1)*ex(:)**2
    end if
  end do
                                              call dumpn (q, 'q', 'resh.dmp', 1)
!-------------------------------------------------------------------------------
! Convert Q to energy per unit volume
!-------------------------------------------------------------------------------
  do iz=1,lz
    do iy=2,n2-1
      a = mu/(ymg(iy+1)-ymg(iy-1))
      q(:,iz,iy) = q(:,iz,iy)*(dtau(:,iz,iy+1)+dtau(:,iz,iy))*a
    end do
    a = mu/(ymg(2)-ymg(1))
    q(:,iz, 1) = q(:,iz, 1)*(dtau(:,iz, 2))*a
  end do
                                              call dumpn (q, 'q', 'resh.dmp', 1)
  if ((master .and. debug2(dbg_rad,5)) .or. debug2(dbg_rad,6)) then 
    print '(i6,a,1p,900e10.2)', mpi_rank, '    q =',    q(1,1,1:n2in)
  end if
!-------------------------------------------------------------------------------
! Reset max indices for next ibox and deallocate
!-------------------------------------------------------------------------------
  n1in = n1max
  n2in = n2max
  deallocate (s, sp1, sp2, sp3, rk, ex)
  if (ibox == mbox) deallocate(dtau)
  END
