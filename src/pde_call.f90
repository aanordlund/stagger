! $Id: pde_call.f90,v 1.87 2010/01/03 20:10:58 aake Exp $
!***********************************************************************
SUBROUTINE init_pde
! USE params
! USE pde_mod
! call init_arrays
! call init_work
END SUBROUTINE

!***********************************************************************

  SUBROUTINE pde(flag)

  USE params
  USE variables

  implicit none

  real gam1, average, tmp, fmaxval, soundsp, velocity, fCv
  integer ix, iy, iz
  
  real:: elapsed, tm(30)
  integer j

  logical flag
  real, allocatable, dimension(:,:,:) :: &
       dd,lnd,lnu,lnr,P,Cs,divU,nu,lne,ee, &
       Sxx,Syy,Szz,Sxy,Szx,Syz,xdnl,ydnl,zdnl,xdnr,ydnr,zdnr, &
       Txx,Tyy,Tzz,Txy,Tzx,Tyz,ddxd,ddyd,ddzd
  real, allocatable, dimension(:,:,:) :: &
    scr1, scr2, scr3, &
    xydnlnu, xzdnlnu, yzdnlnu, &
    TSxy, TSyz, TSzx, &
    xupUx, yupUy, zupUz, &
    xydnl, xdnUy, ydnUx, &
    yzdnl, ydnUz, zdnUy, &
    zxdnl, zdnUx, xdnUz, &
    ddxdnUy, ddzdnUy, &
    ddydnUz, ddxdnUz, &
    ddzdnUx, ddydnUx, &
    ddxdnTxx, ddyupTxy, ddzupTzx, &
    ddydnTyy, ddzupTyz, ddxupTxy, &
    ddzdnTzz, ddxupTzx, ddyupTyz, &
    eflux, &
    xdnlnu, xdnlne, ddxdnlne, &
    ydnlnu, ydnlne, ddydnlne, &
    zdnlnu, zdnlne, ddzdnlne, &
    ddxuppx, ddyuppy, ddzuppz
  character (len=mid):: id
  data id/'$Id: pde_call.f90,v 1.87 2010/01/03 20:10:58 aake Exp $'/

  nstag=0; nstag1=0; nflop=0;
  if (dxm(1).le.0.) dxm(1)=dym(1)
  if (dxmdn(1).le.0.) dxmdn(1)=dymdn(1)
  if (dzm(1).le.0.) dzm(1)=dym(1)
  if (dzmdn(1).le.0.) dzmdn(1)=dymdn(1)
!  if (.not. do_loginterp) then
!    call pde_lin(r,px,py,pz,e,d,drdt,dpxdt,dpydt,dpzdt,dedt,dddt,flag, &
!                 Bx,By,Bz,dBxdt,dBydt,dBzdt)
!    return
!  end if

  call print_trace (id, dbg_hd, 'BEGIN')

  if (idbg>1) then; j=1;tm(j)=elapsed(); end if
  if (do_trace) print *,'velocities'

!-----------------------------------------------------------------------
!  Velocities, pressure
!-----------------------------------------------------------------------
  allocate (lnr(mx,my,mz),xdnl(mx,my,mz),ydnl(mx,my,mz),zdnl(mx,my,mz)) !  4 chunks
  allocate (xdnr(mx,my,mz),ydnr(mx,my,mz),zdnr(mx,my,mz))               !  7 chunks
!$omp parallel do private(iz)
  do iz=1,mz
    lnr(:,:,iz) = alog(r(:,:,iz))
  end do
  nflop = nflop+1
  call density_boundary(r,lnr,py,e)
  call xdn_set (lnr, xdnl)
  call ydn_set (lnr, ydnl)
  call zdn_set (lnr, zdnl)
!$omp parallel do private(iz)
  do iz=1,mz
    xdnr(:,:,iz)=exp(xdnl(:,:,iz))
    Ux(:,:,iz) = px(:,:,iz)/xdnr(:,:,iz)
    ydnr(:,:,iz)=exp(ydnl(:,:,iz))
    Uy(:,:,iz) = py(:,:,iz)/ydnr(:,:,iz)
    zdnr(:,:,iz)=exp(zdnl(:,:,iz))
    Uz(:,:,iz) = pz(:,:,iz)/zdnr(:,:,iz)
  end do

  call dumpn(r,'r','var.dmp',1)
  call dumpn(r,'r','mom.dmp',1)
  call dumpn(e,'e','var.dmp',1)
  call dumpn(Ux,'Ux','var.dmp',1)
  call dumpn(Uy,'Uy','var.dmp',1)
  call dumpn(Uy,'Uy','mom.dmp',1)
  call dumpn(Bx,'Bx','var.dmp',1)

  nflop = nflop+6
  call velocity_boundary(r,xdnr,ydnr,zdnr,Ux,Uy,Uz,px,py,pz,e,Bx,By,Bz)
  if (idbg>1) then; j=j+1;tm(j)=elapsed(); print '(a4,2f7.3)',' ui',tm(j)-tm(j-1),tm(j)-tm(1); endif

  call forceit (r,Ux,Uy,Uz,xdnr,ydnr,zdnr,dpxdt,dpydt,dpzdt)
  if (idbg>1) then; j=j+1;tm(j)=elapsed(); print '(a4,2f7.3)','frc',tm(j)-tm(j-1),tm(j)-tm(1); endif
  deallocate (xdnr, ydnr, zdnr)

  allocate (Sxx(mx,my,mz),Syy(mx,my,mz),Szz(mx,my,mz))                  !  9 chunks
  call dumpn(Uy,'Uy','mom.dmp',1)
  call ddxup_set (Ux, Sxx)
  call ddyup_set (Uy, Syy)
  call ddzup_set (Uz, Szz)
  call dumpn(Sxx,'Sxx','drdt.dmp',1)
  call dumpn(Syy,'Syy','drdt.dmp',1)
  call dumpn(Szz,'Szz','drdt.dmp',1)
  call dumpn(Syy,'Syy','mom.dmp',1)
  call dumpn(Syy,'Syy','ddt.dmp',1)

!-----------------------------------------------------------------------
!  First part of viscosity
!-----------------------------------------------------------------------
  allocate (P(mx,my,mz),Cs(mx,my,mz),nu(mx,my,mz))                      ! 12 chunks
  if (nu4 > 0.) then
    call dif2a_set (lnr,nu)
!$omp parallel do private(iz,iy,ix)
    do iz=1,mz
    do iy=1,my
    do ix=1,mx
      nu(ix,iy,iz) = max(1.,nu4*nu(ix,iy,iz))*max(-Sxx(ix,iy,iz)-Syy(ix,iy,iz)-Szz(ix,iy,iz),0.)+1e-20
    end do
    end do
    end do
    nflop = nflop+7
  else
!$omp parallel do private(iz,iy,ix)
    do iz=1,mz
    do iy=1,my
    do ix=1,mx
      nu(ix,iy,iz) = Sxx(ix,iy,iz)+Syy(ix,iy,iz)+Szz(ix,iy,iz)
      nu(ix,iy,iz) = max(-nu(ix,iy,iz),0.)+1e-20
    end do
    end do
    end do
    nflop = nflop+4
  end if

  nflop = nflop+4
  if (gamma .eq. 1.) then
    gam1 = 1.
  else
    gam1 = gamma-1.
  end if
  allocate (scr1(mx,my,mz), scr2(mx,my,mz), scr3(mx,my,mz))
  call xup_set (Ux, scr1)
  call yup_set (Uy, scr2)
  call zup_set (Uz, scr3)
!$omp parallel do private(iz)
  do iz=1,mz
    Cs(:,:,iz) = scr1(:,:,iz)**2 + scr2(:,:,iz)**2 + scr3(:,:,iz)**2
  end do
  nflop = nflop+5
  deallocate (scr1, scr2, scr3)
  if (idbg>1) then; j=j+1;tm(j)=elapsed(); print '(a4,2f7.3)','div',tm(j)-tm(j-1),tm(j)-tm(1); endif
  Urms = sqrt(average(Cs))
  if (idbg>1) then; j=j+1;tm(j)=elapsed(); print '(a4,2f7.3)','urm',tm(j)-tm(j-1),tm(j)-tm(1); endif

  if (do_trace) print *,'pressure'
!-----------------------------------------------------------------------
!  Energy per unit mass
!-----------------------------------------------------------------------
  allocate (ee(mx,my,mz))                                                !  6 chunks
!$omp parallel do private(iz)
  do iz=1,mz
    ee(:,:,iz) = e(:,:,iz)/r(:,:,iz)
  end do
    nflop = nflop+1
!-----------------------------------------------------------------------
!  Boundary condition -- may need to calculate pressure internally
!-----------------------------------------------------------------------
  if (do_energy) call energy_boundary(r,lnr,Ux,Uy,Uz,e,ee,P,Bx,By,Bz)
!-----------------------------------------------------------------------
!  Pressure
!-----------------------------------------------------------------------
  call pressure (r,ee,P)
  call dumpn(r ,'r','drdt.dmp',1)
  call dumpn(ee,'ee','drdt.dmp',1)
  if (idbg>1) then; j=j+1;tm(j)=elapsed(); print '(a4,2f7.3)','eos',tm(j)-tm(j-1),tm(j)-tm(1); endif

!-----------------------------------------------------------------------
!  Numerical viscosity, Richtmyer & Morton type.  For testing purposes,
!  nu1<0 signals the use of constant diffusivity.
!-----------------------------------------------------------------------
  call dumpn(nu,'nu','mom.dmp',1)
  call dumpn(nu,'nu','drdt.dmp',1)
  call dumpn(Cs,'Cs','drdt.dmp',1)
  call dumpn(P ,'Pg','drdt.dmp',1)
  if (nu1.lt.0.0) then
!$omp parallel do private(iz)
    do iz=1,mz
      Cs(:,:,iz) = sqrt(gamma*P(:,:,iz)/r(:,:,iz)) + sqrt(Cs(:,:,iz))
      nu(:,:,iz) = abs(nu1)
    end do
    nflop = nflop+4+2*10
  else
  allocate (scr1(mx,my,mz),scr2(mx,my,mz))
!$omp parallel do private(iz,iy,ix,soundsp,velocity)
    do iz=1,mz
    do iy=1,my
    do ix=1,mx
      soundsp = sqrt(gamma*P(ix,iy,iz)/r(ix,iy,iz))
      velocity = sqrt(Cs(ix,iy,iz))
      Cs(ix,iy,iz) = soundsp + velocity
      nu(ix,iy,iz) = nu1*velocity &
                   + nu2*max(dxm(ix),dym(iy),dzm(iz))*nu(ix,iy,iz) &
                   + nu3*soundsp 
      scr1(ix,iy,iz)=nu3*soundsp
      scr2(ix,iy,iz)=nu1*velocity
    end do
    end do
    end do
    nflop = nflop+5+2*10
  end if

  call dumpn(nu,'nu(1+2+3)','mom.dmp',1)
  call dumpn(scr2,'nu1*velocity','mom.dmp',1)
  call dumpn(scr1,'nu3*soundsp','mom.dmp',1)
  deallocate (scr2)

  if (lb.lt.6) call regularize(nu)
  allocate (lnu(mx,my,mz))
  !nu = smooth3(max3(nu))
  call max3_set (nu, scr1)
  call smooth3_set (scr1, nu)
  deallocate (scr1)

  call regularize(nu)
  call stats('nu',nu)
  call dumpn(nu,'nu smoothed','mom.dmp',1)
!$omp parallel do private(iz)
  do iz=1,mz
    lnu(:,:,iz) = alog(nu(:,:,iz)+1.e-30)
  end do
  nflop = nflop+1
!  lnu = smooth3(max3(lnu))
!!$omp parallel do private(iz)
!  do iz=1,mz
!    nu(:,:,iz) = exp(lnu(:,:,iz))
!  end do

  if (flag) then
!$omp parallel do private(iz,iy,ix)
    do iz=1,mz
    do iy=1,my
    do ix=1,mx
     Cs(ix,iy,iz) = Cs(ix,iy,iz)*(dt/min(dxm(ix),dym(iy),dzm(iz)))
    end do
    end do
    end do
    Cu = fmaxval('Cu',Cs)

!-----------------------------------------------------------------------
!  Viscosity Courand condition factors:
!      2. : the diagonal viscosity has a factor two in front of nu
!     6.2 : the maximum value returned from ddxup(ddxdn(f)) when dx=1.
!      3. : for the worst case of a 3-D checker board
!      2. : overshoot with a factor 2. at Cdtd=1., for marginal stability
!-----------------------------------------------------------------------
    fCv = 2.*6.2*3.*dt/2.

!$omp parallel do private(iz,iy,ix)
    do iz=1,mz
    do iy=1,my
    do ix=1,mx
      Cs(ix,iy,iz) = nu(ix,iy,iz)*(fCv/min(dxm(ix),dym(iy),dzm(iz)))
    end do
    end do
    end do
    Cv = fmaxval('Cv',Cs)
  end if
  nflop = nflop+2
  deallocate (Cs)

!$omp parallel do private(iz)
  do iz=1,mz
    lnu(:,:,iz) = lnu(:,:,iz)+lnr(:,:,iz)
    nu(:,:,iz) = nu(:,:,iz)*r(:,:,iz)
  end do
  call dumpn(nu,'nu*r','mom.dmp',1)
  nflop = nflop+2
  if (idbg>1) then; j=j+1;tm(j)=elapsed(); print '(a4,2f7.3)','dif',tm(j)-tm(j-1),tm(j)-tm(1); endif

  call stats ('nu', nu)

  if (do_trace) print *,'mass'
!-----------------------------------------------------------------------
!  Mass conservation, cell centered
!-----------------------------------------------------------------------
if (do_density) then
  if (nur==0) then
    allocate (ddxuppx(mx,my,mz), ddyuppy(mx,my,mz), ddzuppz(mx,my,mz))
    if (do_2nddiv) then
      call ddxup1_set (px, ddxuppx)
      call ddyup1_set (py, ddyuppy)
      call ddzup1_set (pz, ddzuppz)
    else
      call ddxup_set (px, ddxuppx)
      call ddyup_set (py, ddyuppy)
      call ddzup_set (pz, ddzuppz)
    end if
    call dumpn(drdt,'drdt','drdt.dmp',1)
    call dumpn(px,'px','mom.dmp',1)
    call dumpn(py,'py','mom.dmp',1)
!$omp parallel do private(iz)
    do iz=1,mz
      drdt(:,:,iz) = drdt(:,:,iz) - ddxuppx(:,:,iz) - ddyuppy(:,:,iz) - ddzuppz(:,:,iz)
    end do
    call dumpn(drdt,'drdt-div(p)','drdt.dmp',1)
    call dumpn(ddxuppx,'dpxdx','drdt.dmp',1)
    call dumpn(ddyuppy,'dpydy','drdt.dmp',1)
    call dumpn(ddzuppz,'dpzdz','drdt.dmp',1)
    nflop = nflop+3
    deallocate (ddxuppx, ddyuppy, ddzuppz)                                !  1 chunk
  else
    allocate (scr1(mx,my,mz), scr2(mx,my,mz))
!
    call ddxdn_set (lnr, scr1)
    call xdn_set (lnu, scr2)
    call dumpn(drdt,'drdt','drdt.dmp',1)
    call dumpn(scr1,'dlnrdx','drdt.dmp',1)
    call dumpn(scr2,'dlnudx','drdt.dmp',1)
!$omp parallel do private(iz,iy,ix)
    do iz=1,mz
    do iy=1,my
    do ix=1,mx
      scr1(ix,iy,iz) = -px(ix,iy,iz)+(nur*dxmdn(ix))*exp(scr2(ix,iy,iz))*scr1(ix,iy,iz)
    end do
    end do
    end do
    nflop = nflop+6
    if (do_2nddiv) then
      call ddxup1_add (scr1, drdt)
    else
      call ddxup_add (scr1, drdt)
    end if
    call dumpn(drdt,'drdt','drdt.dmp',1)
    call dumpn(scr1,'-px+Fmx','drdt.dmp',1)
!
    call ddydn_set (lnr, scr1)
    call ydn_set (lnu, scr2)
!$omp parallel do private(iz,iy,ix)
    do iz=1,mz
    do iy=1,my
    do ix=1,mx
      scr1(ix,iy,iz) = -py(ix,iy,iz)+(nur*dymdn(iy))*exp(scr2(ix,iy,iz))*scr1(ix,iy,iz)
    end do
    end do
    end do
    nflop = nflop+6
    if (do_2nddiv) then
      call ddyup1_add (scr1, drdt)
    else
      call ddyup_add (scr1, drdt)
    end if
    call dumpn(drdt,'drdt','drdt.dmp',1)
    call dumpn(scr1,'-py+Fmy','drdt.dmp',1)
!
    call ddzdn_set (lnr, scr1)
    call zdn_set (lnu, scr2)
!$omp parallel do private(iz,iy,ix)
    do iz=1,mz
    do iy=1,my
    do ix=1,mx
      scr1(ix,iy,iz) = -pz(ix,iy,iz)+(nur*dzmdn(iz))*exp(scr2(ix,iy,iz))*scr1(ix,iy,iz)
    end do
    end do
    end do
    nflop = nflop+6
    if (do_2nddiv) then
      call ddzup1_add (scr1, drdt)
    else
      call ddzup_add (scr1, drdt)
    end if
!
    call dumpn(drdt,'drdt','drdt.dmp',1)
    call dumpn(scr1,'-pz+Fmz','drdt.dmp',1)
    deallocate (scr1, scr2)
  end if
end if
  deallocate (lnr)                                                      !  3 chunks
  if (idbg>1) then; j=j+1;tm(j)=elapsed(); print '(a4,2f7.3)','mas',tm(j)-tm(j-1),tm(j)-tm(1); endif

  if (do_trace) print *,'diagonal'
!-----------------------------------------------------------------------
!  Viscous stress, diagonal and off-diagonal elements
!-----------------------------------------------------------------------
  nstag=0
  allocate (Txx(mx,my,mz),Tyy(mx,my,mz),Tzz(mx,my,mz))                  ! 15 chunks
!$omp parallel do private(iz,iy,ix)
  do iz=1,mz
    do iy=1,my
    do ix=1,mx
      Txx(ix,iy,iz) = - (2.*dxm(ix))*nu(ix,iy,iz)*Sxx(ix,iy,iz) + P(ix,iy,iz)
      Tyy(ix,iy,iz) = - (2.*dym(iy))*nu(ix,iy,iz)*Syy(ix,iy,iz) + P(ix,iy,iz)
      Tzz(ix,iy,iz) = - (2.*dzm(iz))*nu(ix,iy,iz)*Szz(ix,iy,iz) + P(ix,iy,iz)
    end do
    end do
    if (do_energy) then
      call dumpn(dedt, 'dedt (0)','dedt.dmp',1)
      if (do_dissipation) then 
        dedt(:,:,iz) = dedt(:,:,iz) &
                     - Txx(:,:,iz)*Sxx(:,:,iz) &
                     - Tyy(:,:,iz)*Syy(:,:,iz) &
                     - Tzz(:,:,iz)*Szz(:,:,iz)
      else
        dedt(:,:,iz) = dedt(:,:,iz) - P(:,:,iz)* &
                       (Sxx(:,:,iz) + Syy(:,:,iz) + Szz(:,:,iz))
      end if
    end if
  end do
  nflop = nflop+15
  call dumpn (dedt,'dedt=-T*S-PdivU','dedt.dmp',1)
  call dumpn(Syy,'Syy','dedt.dmp',1)
  call dumpn(Tyy,'Tyy nuSyy+P','dedt.dmp',1)
  call dumpn(P,'Pg','mom.dmp',1)
  call dumpn(Tyy,'Tyy nuSyy+P','mom.dmp',1)
  call dumpn(Tyy,'Tyy (nuSyy+P)','ddt.dmp',1)
  deallocate (Sxx,Syy,Szz)                                            ! 10 chunks

  allocate (Sxy(mx,my,mz),Syz(mx,my,mz),Szx(mx,my,mz))                  ! 13 chunks
  allocate (ddxdnUy(mx,my,mz), ddzdnUy(mx,my,mz), ddydnUz(mx,my,mz))    ! 16 chunks
  allocate (ddxdnUz(mx,my,mz), ddzdnUx(mx,my,mz), ddydnUx(mx,my,mz))    ! 19 chunks
  call ddxdn_set(Uy,ddxdnUy)
  call ddzdn_set(Uy,ddzdnUy)
  call ddydn_set(Ux,ddydnUx)
  call ddzdn_set(Ux,ddzdnUx)
  call ddxdn_set(Uz,ddxdnUz)
  call ddydn_set(Uz,ddydnUz)
!$omp parallel do private(iz)
  do iz=1,mz
    Sxy(:,:,iz) = (ddxdnUy(:,:,iz)+ddydnUx(:,:,iz))*0.5
    Syz(:,:,iz) = (ddydnUz(:,:,iz)+ddzdnUy(:,:,iz))*0.5
    Szx(:,:,iz) = (ddzdnUx(:,:,iz)+ddxdnUz(:,:,iz))*0.5
  end do
  nflop = nflop+6
  deallocate (ddxdnUy, ddzdnUy, ddydnUz, ddxdnUz, ddzdnUx, ddydnUx)     ! 13 chunks
  allocate (Txy(mx,my,mz),Tyz(mx,my,mz),Tzx(mx,my,mz))                  ! 16 chunks
  allocate (scr1(mx,my,mz), xydnlnu(mx,my,mz), xzdnlnu(mx,my,mz), yzdnlnu(mx,my,mz))
  call xdn_set (lnu,scr1)
  call ydn_set (scr1,xydnlnu)
  call zdn_set (scr1,xzdnlnu)
  call ydn_set (lnu,scr1)
  call zdn_set (scr1,yzdnlnu)
!$omp parallel do private(iz,iy,ix)
  do iz=1,mz
  do iy=1,my
  do ix=1,mx
    Txy(ix,iy,iz) = - (dxmdn(ix)+dymdn(iy))*exp(xydnlnu(ix,iy,iz))*Sxy(ix,iy,iz)
    Tyz(ix,iy,iz) = - (dymdn(iy)+dzmdn(iz))*exp(yzdnlnu(ix,iy,iz))*Syz(ix,iy,iz)
    Tzx(ix,iy,iz) = - (dzmdn(iz)+dxmdn(ix))*exp(xzdnlnu(ix,iy,iz))*Szx(ix,iy,iz)
  end do
  end do
  end do
  nflop = nflop+9
  deallocate (xydnlnu, xzdnlnu, yzdnlnu)
  call dumpn(Txy,'Txy','mom.dmp',1)
  call dumpn(Tyz,'Tyz','mom.dmp',1)

  if (do_energy .and. do_dissipation) then
    call xup_set (Sxy, scr1)
    call yup_set (scr1, Sxy)
    call yup_set (Syz, scr1)
    call zup_set (scr1, Syz)
    call zup_set (Szx, scr1)
    call xup_set (scr1, Szx)

!$omp parallel do private(iz,iy,ix)
    do iz=1,mz
    do iy=1,my
    do ix=1,mx
      dedt(ix,iy,iz) = dedt(ix,iy,iz) + 2.*( &
        (dxm(ix)+dym(iy))*nu(ix,iy,iz)*Sxy(ix,iy,iz)**2 &
      + (dym(iy)+dzm(iz))*nu(ix,iy,iz)*Syz(ix,iy,iz)**2 &
      + (dzm(iz)+dxm(ix))*nu(ix,iy,iz)*Szx(ix,iy,iz)**2)
    end do
    end do
    end do
    nflop = nflop+6
    call dumpn (dedt,'dedt (visc offdiag)','dedt.dmp',1)
    call dumpn (nu,'nu','ddt.dmp',1)
    Call dumpn (Sxy,'Sxy','ddt.dmp',1)
    Call dumpn (Syz,'Syz','ddt.dmp',1)
    Call dumpn (Szx,'Szx','ddt.dmp',1)
  end if
  deallocate (scr1)
  deallocate (Sxy,Syz,Szx)                                              ! 13 chunks
  if (idbg>1) then; j=j+1;tm(j)=elapsed(); print '(a4,2f7.3)','vst',tm(j)-tm(j-1),tm(j)-tm(1); endif

!-----------------------------------------------------------------------
!  Add Reynolds stress
!-----------------------------------------------------------------------
  allocate (xupUx(mx,my,mz), yupUy(mx,my,mz), zupUz(mx,my,mz))
  call xup_set(Ux,xupUx)
  call yup_set(Uy,yupUy)
  call zup_set(Uz,zupUz)
!$omp parallel do private(iz)
  do iz=1,mz
    Txx(:,:,iz) = Txx(:,:,iz) + r(:,:,iz)*xupUx(:,:,iz)**2
    Tyy(:,:,iz) = Tyy(:,:,iz) + r(:,:,iz)*yupUy(:,:,iz)**2
    Tzz(:,:,iz) = Tzz(:,:,iz) + r(:,:,iz)*zupUz(:,:,iz)**2
  end do
  nflop = nflop+9
  call dumpn(Tyy,'Tyy+rUy^2','mom.dmp',1)
  call dumpn(Tyy,'Tyy+rUy^2','ddt.dmp',1)
  call dumpn(Tyy,'Tyy+rUy^2','dedt.dmp',1)
  call dumpn(r,'r@Tyy','mom.dmp',1)
  call dumpn(Uy,'Uy@Tyy','mom.dmp',1)
  call dumpn(yupUy,'Uy up','mom.dmp',1)
  deallocate (xupUx, yupUy, zupUz)

  allocate (xydnl(mx,my,mz), xdnUy(mx,my,mz), ydnUx(mx,my,mz))
  call xdn_set (ydnl, xydnl)
  call xdn_set(Uy, xdnUy)
  call ydn_set(Ux, ydnUx)
!$omp parallel do private(iz)
  do iz=1,mz
    Txy(:,:,iz) = Txy(:,:,iz) + exp(xydnl(:,:,iz))*ydnUx(:,:,iz)*xdnUy(:,:,iz)
  end do
  nflop = nflop+4
  deallocate (xydnl, xdnUy, ydnUx)

  allocate (yzdnl(mx,my,mz), ydnUz(mx,my,mz), zdnUy(mx,my,mz))
  call ydn_set (zdnl, yzdnl)
  call ydn_set(Uz, ydnUz)
  call zdn_set(Uy, zdnUy)
!$omp parallel do private(iz)
  do iz=1,mz
    Tyz(:,:,iz) = Tyz(:,:,iz) + exp(yzdnl(:,:,iz))*zdnUy(:,:,iz)*ydnUz(:,:,iz)
  end do
  nflop = nflop+4
  deallocate (yzdnl, ydnUz, zdnUy)
  call dumpn(Txy,'Txy','mom.dmp',1)
  call dumpn(Tyz,'Tyz','mom.dmp',1)

  allocate (zxdnl(mx,my,mz), zdnUx(mx,my,mz), xdnUz(mx,my,mz))
  call zdn_set (xdnl, zxdnl)
  call zdn_set(Ux, zdnUx)
  call xdn_set(Uz, xdnUz)
!$omp parallel do private(iz)
  do iz=1,mz
    Tzx(:,:,iz) = Tzx(:,:,iz) + exp(zxdnl(:,:,iz))*xdnUz(:,:,iz)*zdnUx(:,:,iz)
  end do
  nflop = nflop+4
  deallocate (zxdnl,zdnUx,xdnUz)
  deallocate (xdnl,ydnl,zdnl)                                           ! 10 chunks
  if (idbg>1) then; j=j+1;tm(j)=elapsed(); print '(a4,2f7.3)','rst',tm(j)-tm(j-1),tm(j)-tm(1); endif

  if (do_trace) print *,'off-diagonal'
!-----------------------------------------------------------------------
!  Equations of motion, face centred
!-----------------------------------------------------------------------
  allocate (ddxdnTxx(mx,my,mz), ddyupTxy(mx,my,mz), ddzupTzx(mx,my,mz))
  if (do_2nddiv) then
    call ddxdn1_set (Txx, ddxdnTxx)
    call ddyup1_set (Txy, ddyupTxy)
    call ddzup1_set (Tzx, ddzupTzx)
  else
    call ddxdn_set (Txx, ddxdnTxx)
    call ddyup_set (Txy, ddyupTxy)
    call ddzup_set (Tzx, ddzupTzx)
  end if
!$omp parallel do private(iz)
  do iz=1,mz
    dpxdt(:,:,iz) = dpxdt(:,:,iz) - ddxdnTxx(:,:,iz) - ddyupTxy(:,:,iz) - ddzupTzx(:,:,iz)
  end do
  nflop = nflop+3
  deallocate (ddxdnTxx, ddyupTxy, ddzupTzx)

  allocate (ddydnTyy(mx,my,mz), ddzupTyz(mx,my,mz), ddxupTxy(mx,my,mz))
  if (do_2nddiv) then
    call ddydn1_set (Tyy, ddydnTyy)
    call ddzup1_set (Tyz, ddzupTyz)
    call ddxup1_set (Txy, ddxupTxy)
  else
    call ddydn_set (Tyy, ddydnTyy)
    call ddzup_set (Tyz, ddzupTyz)
    call ddxup_set (Txy, ddxupTxy)
  end if
  call dumpn(dpydt,'dpydt<dT','mom.dmp',1)
!$omp parallel do private(iz)
  do iz=1,mz
    dpydt(:,:,iz) = dpydt(:,:,iz) - ddydnTyy(:,:,iz) - ddzupTyz(:,:,iz) - ddxupTxy(:,:,iz)
  end do
  call dumpn(dpydt,'dpydt (-div(T)','ddt.dmp',1)
  nflop = nflop+3
  deallocate (ddydnTyy, ddzupTyz, ddxupTxy)

  allocate (ddzdnTzz(mx,my,mz), ddxupTzx(mx,my,mz), ddyupTyz(mx,my,mz))
  if (do_2nddiv) then
    call ddzdn1_set (Tzz, ddzdnTzz)
    call ddxup1_set (Tzx, ddxupTzx)
    call ddyup1_set (Tyz, ddyupTyz)
  else
    call ddzdn_set (Tzz, ddzdnTzz)
    call ddxup_set (Tzx, ddxupTzx)
    call ddyup_set (Tyz, ddyupTyz)
  end if
!$omp parallel do private(iz)
  do iz=1,mz
    dpzdt(:,:,iz) = dpzdt(:,:,iz) - ddzdnTzz(:,:,iz) - ddxupTzx(:,:,iz) - ddyupTyz(:,:,iz)
  end do
  nflop = nflop+3
  deallocate (ddzdnTzz, ddxupTzx, ddyupTyz)
  deallocate (Txy,Tyz,Tzx,Txx,Tyy,Tzz)                                  !  4 chunks
  if (idbg>1) then; j=j+1;tm(j)=elapsed(); print '(a4,2f7.3,i4)','eom',tm(j)-tm(j-1),tm(j)-tm(1),nstag; endif
  call dumpn(dpxdt,'dpxdt>dT','mom.dmp',1)
  call dumpn(dpydt,'dpydt>dT','mom.dmp',1)
  call dumpn(dpzdt,'dpzdt>dT','mom.dmp',1)

!-----------------------------------------------------------------------
!  Abundance per unit mass
!-----------------------------------------------------------------------
  if (do_pscalar .or. do_cool) then
    allocate (dd(mx,my,mz))
    if (do_pscalar) then
!$omp parallel do private(iz)
      do iz=1,mz
        dd(:,:,iz)=d(:,:,iz)/r(:,:,iz)
      end do
    end if
    nflop = nflop+1
  end if

!-----------------------------------------------------------------------
!  Passive scalar
!-----------------------------------------------------------------------
  call passive(nu,lnu,r,px,py,pz,Ux,Uy,Uz,d,dddt)
  call trace_particles (Ux,Uy,Uz)
  deallocate (nu)
  if (idbg>1) then; j=j+1;tm(j)=elapsed(); print '(a4,2f7.3)','pas',tm(j)-tm(j-1),tm(j)-tm(1); endif

  if (do_trace) print *,'energy'
!-----------------------------------------------------------------------
!  Energy equation
!-----------------------------------------------------------------------
  allocate (lne(mx,my,mz))                                              !  6 chunks
!!  lnu = lnu+alog(2.)
!$omp parallel do private(iz)
  do iz=1,mz
    lne(:,:,iz) = alog(ee(:,:,iz))
    lnu(:,:,iz) = lnu(:,:,iz)+lne(:,:,iz)
  end do
  nflop = nflop+2
  if (idbg>1) then; j=j+1;tm(j)=elapsed(); print '(a4,2f7.3)','lne',tm(j)-tm(j-1),tm(j)-tm(1); endif

!-----------------------------------------------------------------------
!  MHD
!-----------------------------------------------------------------------
  if (do_mhd) then
    if (do_trace) print *,'mhd'
    call mhd (flag, &
            r,ee,Ux,Uy,Uz,Bx,By,Bz,Ex,Ey,Ez, &
            dpxdt,dpydt,dpzdt,dedt,dBxdt,dBydt,dBzdt)
  if (idbg>1) then; j=j+1;tm(j)=elapsed(); print '(a4,2f7.3)','mhd',tm(j)-tm(j-1),tm(j)-tm(1); endif
  call dumpn (dpydt,'dpydt (+mhd)','ddt.dmp',1)
  end if
! deallocate (Ux,Uy,Uz)                                                 !  7 chunks
  if (do_trace) print *,'end mhd'

if (do_energy) then

!-----------------------------------------------------------------------
!  Energy advection and diffusion
!-----------------------------------------------------------------------
  call dumpn(dedt,'dedt_0','ddt.dmp',1)
  call dumpn(dedt,'dedt<adv+diff','dedt.dmp',1)
  allocate (eflux(mx,my,mz), xdnlnu(mx,my,mz), xdnlne(mx,my,mz), ddxdnlne(mx,my,mz))
  call xdn_set (lnu, xdnlnu)
  call xdn_set (lne, xdnlne)
  call ddxdn_set (lne, ddxdnlne)
!$omp parallel do private(iz,iy,ix)
  do iz=1,mz
  do iy=1,my
  do ix=1,mx
    eflux(ix,iy,iz) = exp(xdnlne(ix,iy,iz))*px(ix,iy,iz) - dxm(ix)*exp(xdnlnu(ix,iy,iz))*ddxdnlne(ix,iy,iz)
  end do
  end do
  end do
  nflop = nflop+6
  deallocate (xdnlnu, xdnlne, ddxdnlne)
  call dumpn(eflux,'eflux_x','mom.dmp',1)

  allocate (scr1(mx,my,mz))
  if (do_2nddiv) then
    call ddxup1_set (eflux, scr1)
  else
    call ddxup_set (eflux, scr1)
  end if
!$omp parallel do private(iz)
  do iz=1,mz
    dedt(:,:,iz) = dedt(:,:,iz) - scr1(:,:,iz)
  end do
  call dumpn(dedt,'dedt_x','',1)
  nflop = nflop+1

  allocate (ydnlnu(mx,my,mz), ydnlne(mx,my,mz), ddydnlne(mx,my,mz))
  call ydn_set (lnu, ydnlnu)
  call ydn_set (lne, ydnlne)
  call ddydn_set (lne, ddydnlne)
!$omp parallel do private(iz,iy,ix)
  do iz=1,mz
  do iy=1,my
  do ix=1,mx
    eflux(ix,iy,iz) = exp(ydnlne(ix,iy,iz))*py(ix,iy,iz) - dym(iy)*exp(ydnlnu(ix,iy,iz))*ddydnlne(ix,iy,iz)
    scr1(ix,iy,iz) = dym(iy)*exp(ydnlnu(ix,iy,iz))*ddydnlne(ix,iy,iz)
  end do
  end do
  end do
  nflop = nflop+6
  call dumpn(eflux,   'eflux_y', 'dedt.dmp',1)
  call dumpn(scr1,    'eflux_y visc', 'dedt.dmp',1)
  call dumpn(ydnlne,  'ydnlne',  'dedt.dmp',1)
  call dumpn(py,      'py',      'dedt.dmp',1)
  call dumpn(ydnlnu,  'ydnlnu',  'dedt.dmp',1)
  call dumpn(ddydnlne,'ddydnlne','dedt.dmp',1)
  
  deallocate (ydnlnu, ydnlne, ddydnlne)

  if (do_2nddiv) then
    call ddyup1_set (eflux, scr1)
  else
    call ddyup_set (eflux, scr1)
  end if
!$omp parallel do private(iz)
  do iz=1,mz
    dedt(:,:,iz) = dedt(:,:,iz) - scr1(:,:,iz)
  end do
  nflop = nflop+1
  call dumpn(dedt,'dedt_y','dedt.dmp',1)

  allocate (zdnlnu(mx,my,mz), zdnlne(mx,my,mz), ddzdnlne(mx,my,mz))
  call zdn_set (lnu, zdnlnu)
  call zdn_set (lne, zdnlne)
  call ddzdn_set (lne, ddzdnlne)
!$omp parallel do private(iz,iy,ix)
  do iz=1,mz
  do iy=1,my
  do ix=1,mx
    eflux(ix,iy,iz) = exp(zdnlne(ix,iy,iz))*pz(ix,iy,iz) - dzm(iz)*exp(zdnlnu(ix,iy,iz))*ddzdnlne(ix,iy,iz)
  end do
  end do
  end do
  nflop = nflop+6
  deallocate (zdnlnu, zdnlne, ddzdnlne)
  call dumpn(eflux,'eflux_z','mom.dmp',1)

  if (do_2nddiv) then
    call ddzup1_set (eflux, scr1)
  else
    call ddzup_set (eflux, scr1)
  end if
  call dumpn(dedt,'dedt<z','dedt.dmp',1)
!$omp parallel do private(iz)
  do iz=1,mz
    dedt(:,:,iz) = dedt(:,:,iz) - scr1(:,:,iz)
  end do
  nflop = nflop+1
  deallocate (eflux,scr1)
  call dumpn(dedt,'dedt_z','dedt.dmp',1)
  call dumpn (dedt,'dedt (trans+diff)','dedt.dmp',1)

  if (do_trace) print *,'cooling'
!-----------------------------------------------------------------------
!  Cooling and conduction
!-----------------------------------------------------------------------
  if (do_cool) then
    call coolit (r,ee,lne,dd,dedt)
    call conduction (r,e,ee,Bx,By,Bz,dedt)
  end if
  call dumpn (dedt,'dedt (cool+cond)','dedt.dmp',1)
  if (idbg>1) then; j=j+1;tm(j)=elapsed(); print '(a4,2f7.3)','coo',tm(j)-tm(j-1),tm(j)-tm(1); endif

end if
  deallocate (ee,lne)                                                   !  6 chunks
  if (idbg>1) then; j=j+1;tm(j)=elapsed(); print '(a4,2f7.3)','eeq',tm(j)-tm(j-1),tm(j)-tm(1); endif

  if (do_pscalar .or. do_cool) deallocate(dd)
  deallocate (lnu)                                                      !  4 chunks

  call dumpn(drdt,'drdt < ddt_bdry','drdt.dmp',1)
  call dumpn(dedt,'dedt < ddt_bdry','dedt.dmp',1)
  call dumpn(dpxdt,'dpxdt < ddt_bdry','mom.dmp',1)
  call dumpn(dpydt,'dpydt < ddt_bdry','mom.dmp',1)
  if (do_trace) print *,'ddt_boundary'
    call ddt_boundary (r,px,py,pz,e,P,Bx,By,Bz, &
                     drdt,dpxdt,dpydt,dpzdt, &
                     dedt,dBxdt,dBydt,dBzdt)
  deallocate (P)                                                   !  6 chunks
  call dumpn(drdt,'drdt > ddt_bdry','drdt.dmp',1)
  call dumpn(dedt,'dedt > ddt_bdry','dedt.dmp',1)
  call dumpn(dpxdt,'dpxdt > ddt_bdry','mom.dmp',1)
  call dumpn(dpydt,'dpydt > ddt_bdry','mom.dmp',1)
  call dumpn(drdt,'drdt > ddt_bdry','ddt.dmp',1)
  call dumpn(dpydt,'dpydt > ddt_bdry','ddt.dmp',1)
  call dumpn(dedt,'dedt > ddt_bdry','ddt.dmp',1)

  call print_trace (id, dbg_hd, 'END')
  END
