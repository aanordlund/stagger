! $Id: pde_mixed.f90,v 1.34 2012/08/22 13:30:09 aake Exp $
!***********************************************************************
MODULE pde_m
  real:: tm(30)
  integer j
END MODULE

!***********************************************************************
  SUBROUTINE pde(flag)
  !SUBROUTINE pde(r,px,py,pz,e,d,drdt,dpxdt,dpydt,dpzdt,dedt,dddt,flag, &
  !           Bx,By,Bz,dBxdt,dBydt,dBzdt)
  USE params
  USE arrays
  !USE variables, ONLY: Ux,Uy,Uz,Ex,Ey,Ez
  USE variables
  implicit none
  !real, dimension(mx,my,mz) :: &
  !     r,px,py,pz,e,d,drdt,dpxdt,dpydt,dpzdt,dedt,dddt, &
  !     Bx,By,Bz,dBxdt,dBydt,dBzdt
  logical flag, debug

  !$omp parallel
  call pde1(r,px,py,pz,e,d,drdt,dpxdt,dpydt,dpzdt,dedt,dddt,flag, &
            Ux,Uy,Uz,Bx,By,Bz,dBxdt,dBydt,dBzdt, &
	    wk01,wk02,wk03,wk04,wk05,wk06,wk07,wk08,wk09,wk10, &
	    wk11,wk12,wk13,wk14,wk15,wk16,wk17,wk21, &
	    scr1,scr2,scr3,scr4,scr5,scr6)
  call pde2(r,px,py,pz,e,d,drdt,dpxdt,dpydt,dpzdt,dedt,dddt,flag, &
            Ux,Uy,Uz,Bx,By,Bz,dBxdt,dBydt,dBzdt, &
	    wk01,wk02,wk03,wk04,wk05,wk06,wk07,wk08,wk09,wk10, &
	    wk11,wk12,wk13,wk14,wk15,wk16,wk17,wk21, &
	    scr1,scr2,scr3,scr4,scr5,scr6)
  call pde3(r,px,py,pz,e,d,drdt,dpxdt,dpydt,dpzdt,dedt,dddt,flag, &
            Ux,Uy,Uz,Bx,By,Bz,dBxdt,dBydt,dBzdt, &
	    wk01,wk02,wk03,wk04,wk05,wk06,wk07,wk08,wk09,wk10, &
	    wk11,wk12,wk13,wk14,wk15,wk16,wk17,wk21, &
	    scr1,scr2,scr3,scr4,scr5,scr6)
  call pde4(r,px,py,pz,e,d,drdt,dpxdt,dpydt,dpzdt,dedt,dddt,flag, &
            Ux,Uy,Uz,Bx,By,Bz,dBxdt,dBydt,dBzdt, &
	    wk01,wk02,wk03,wk04,wk05,wk06,wk07,wk08,wk09,wk10, &
	    wk11,wk12,wk13,wk14,wk15,wk16,wk17,wk21, &
	    scr1,scr2,scr3,scr4,scr5,scr6)
  !$omp end parallel

  if (do_cool) then
    call coolit (r,wk11,wk10,wk12,dedt)
    call conduction (r,e,wk11,Bx,By,Bz,dedt)
  end if

  !$omp parallel
  call pde5(r,px,py,pz,e,d,drdt,dpxdt,dpydt,dpzdt,dedt,dddt,flag, &
            Ux,Uy,Uz,Bx,By,Bz,dBxdt,dBydt,dBzdt,Ex,Ey,Ez, &
	    wk01,wk02,wk03,wk04,wk05,wk06,wk07,wk08,wk09,wk10, &
	    wk11,wk12,wk13,wk14,wk15,wk16,wk17,wk21, &
	    scr1,scr2,scr3,scr4,scr5,scr6)
  !$omp end parallel

  END

!***********************************************************************
  SUBROUTINE pde1(r,px,py,pz,e,d,drdt,dpxdt,dpydt,dpzdt,dedt,dddt,flag, &
            Ux,Uy,Uz,Bx,By,Bz,dBxdt,dBydt,dBzdt, &
	    wk01,wk02,wk03,wk04,wk05,wk06,wk07,wk08,wk09,wk10, &
	    wk11,wk12,wk13,wk14,wk15,wk16,wk17,wk21, &
	    scr1,scr2,scr3,scr4,scr5,scr6)
  USE params
  USE pde_m
  implicit none
  logical flag, debug
  real, dimension(mx,my,mz) :: &
       r,px,py,pz,e,d,drdt,dpxdt,dpydt,dpzdt,dedt,dddt, &
       Ux,Uy,Uz,Bx,By,Bz,dBxdt,dBydt,dBzdt
  real, dimension(mx,my,mz) :: &
       wk01,wk02,wk03,wk04,wk05,wk06,wk07,wk08,wk09,wk10, &
       wk11,wk12,wk13,wk14,wk15,wk16,wk17,wk21, &
       scr1,scr2,scr3,scr4,scr5,scr6
  real, dimension(mx):: exp1, exp2, exp3
  real elapsed
  real gam1, velocity, fCv
  integer ix, iy, iz

  character (len=mid):: id
  data id/'$Id: pde_mixed.f90,v 1.34 2012/08/22 13:30:09 aake Exp $'/

  nstag=0; nstag1=0; if (omp_master) nflop =0;

  call print_id (id)

  if (omp_master.and.debug(dbg_hd)) then; j=1;tm(j)=elapsed(); end if
  call print_trace('pde_mixed',dbg_hd,'velocities')

!-----------------------------------------------------------------------
!  Velocities, pressure
!-----------------------------------------------------------------------
!ptr  wk01  => wk01; xdnl => wk02; ydnl => wk03; zdnl => wk04
!ptr  xdnr => wk05; ydnr => wk06; zdnr => wk07
  do iz=izs,ize
    wk01(:,:,iz) = alog(r(:,:,iz))
  end do
  if (omp_master) nflop = nflop+1
  call density_boundary(r,wk01,py,e)
  call xdn1_set (wk01, wk02)
  call ydn1_set (wk01, wk03)
  call barrier_omp('pde1')                                              ! ensure wk01 complete
  call zdn1_set (wk01, wk04)
  do iz=izs,ize
  do iy=1,my
    call expn(mx,wk02(:,iy,iz),exp1)
    call expn(mx,wk03(:,iy,iz),exp2)
    call expn(mx,wk04(:,iy,iz),exp3)
    do ix=1,mx
      wk05(ix,iy,iz) = exp1(ix)
      Ux(ix,iy,iz) = px(ix,iy,iz)/wk05(ix,iy,iz)
      wk06(ix,iy,iz) = exp2(ix)
      Uy(ix,iy,iz) = py(ix,iy,iz)/wk06(ix,iy,iz)
      wk07(ix,iy,iz) = exp3(ix)
      Uz(ix,iy,iz) = pz(ix,iy,iz)/wk07(ix,iy,iz)
    end do
  end do
  end do
  if (omp_master) nflop = nflop+6
  call barrier_omp('pde2')                                              ! ensure Uz complete

  call velocity_boundary(r,wk05,wk06,wk07,Ux,Uy,Uz,px,py,pz,e,Bx,By,Bz)
  if (omp_master.and.debug(dbg_hd)) then; j=j+1;tm(j)=elapsed(); print '(a4,2f7.3)',' ui',tm(j)-tm(j-1),tm(j)-tm(1); endif

  call forceit (r,Ux,Uy,Uz,wk05,wk06,wk07,dpxdt,dpydt,dpzdt)
  call dumpn(dpxdt,'pde0','dpxdt.dmp',1)
  END

!***********************************************************************
  SUBROUTINE pde2(r,px,py,pz,e,d,drdt,dpxdt,dpydt,dpzdt,dedt,dddt,flag, &
            Ux,Uy,Uz,Bx,By,Bz,dBxdt,dBydt,dBzdt, &
	    wk01,wk02,wk03,wk04,wk05,wk06,wk07,wk08,wk09,wk10, &
	    wk11,wk12,wk13,wk14,wk15,wk16,wk17,wk21, &
	    scr1,scr2,scr3,scr4,scr5,scr6)
  USE params
  USE pde_m
  implicit none
  logical flag, debug
  real, dimension(mx,my,mz) :: &
       r,px,py,pz,e,d,drdt,dpxdt,dpydt,dpzdt,dedt,dddt, &
       Ux,Uy,Uz,Bx,By,Bz,dBxdt,dBydt,dBzdt
  real, dimension(mx,my,mz) :: &
       wk01,wk02,wk03,wk04,wk05,wk06,wk07,wk08,wk09,wk10, &
       wk11,wk12,wk13,wk14,wk15,wk16,wk17,wk21, &
       scr1,scr2,scr3,scr4,scr5,scr6
  real, dimension(mx):: exp1, exp2, exp3
  real elapsed
  real gam1, wavesp, velocity, fCv
  integer ix, iy, iz

  call dumpn(dpxdt,'pde1','dpxdt.dmp',1)

  if (omp_master.and.debug(dbg_hd)) then; j=j+1;tm(j)=elapsed(); print '(a4,2f7.3)','frc',tm(j)-tm(j-1),tm(j)-tm(1); endif
  !wk05, wk06, wk07 free

!ptr  Sxx => wk05; Syy => wk06; Szz => wk07
  call ddxup_set (Ux, wk05)
  call ddyup_set (Uy, wk06)
  call ddzup_set (Uz, wk07)
  call dumpn(wk05,'Sxx','dpxdt.dmp',1)
  call dumpn(wk06,'Syy','dpxdt.dmp',1)
  call dumpn(wk07,'Szz','dpxdt.dmp',1)

!-----------------------------------------------------------------------
!  First part of viscosity
!-----------------------------------------------------------------------
!ptr  P => wk21; Cs => wk12; nu => wk10
  call dumpn(wk10 ,'nu','dpxdt.dmp',1)
  call dif1_set (Ux, Uy, Uz, wk12)
  if (nu4 > 0.) then
    call dif2a_set (wk01,wk10)
    do iz=izs,ize
    do iy=1,my
    do ix=1,mx
      wk10(ix,iy,iz) = max(1.,nu4*wk10(ix,iy,iz))*wk12(ix,iy,iz)+1e-20
      wk17(ix,iy,iz) = wk05(ix,iy,iz)+wk06(ix,iy,iz)+wk07(ix,iy,iz)
    end do
    end do
    end do
    if (omp_master) nflop = nflop+7
  else
    do iz=izs,ize
    do iy=1,my
    do ix=1,mx
      wk10(ix,iy,iz) = wk12(ix,iy,iz)+1e-20
      wk17(ix,iy,iz) = wk05(ix,iy,iz)+wk06(ix,iy,iz)+wk07(ix,iy,iz)
    end do
    end do
    end do
    if (omp_master) nflop = nflop+4
  end if
  call dumpn(wk10 ,'nu','dpxdt.dmp',1)

  if (omp_master) nflop = nflop+4
  if (gamma .eq. 1.) then
    gam1 = 1.
  else
    gam1 = gamma-1.
  end if
  call xup1_set (Ux, scr1)
  call yup1_set (Uy, scr2)
  call zup1_set (Uz, scr3)
  do iz=izs,ize
    wk12(:,:,iz) = scr1(:,:,iz)**2 + scr2(:,:,iz)**2 + scr3(:,:,iz)**2
    scratch(:,:,iz) = 0.5*r(:,:,iz)*wk12(:,:,iz)
  end do
  call average_subr (scratch, e_kin)
  if (omp_master) nflop = nflop+5
  if (omp_master.and.debug(dbg_hd)) then; j=j+1;tm(j)=elapsed(); print '(a4,2f7.3)','div',tm(j)-tm(j-1),tm(j)-tm(1); endif
  call barrier_omp('urms')
  call average_subr(wk12,Urms)
  !$omp single
  Urms = sqrt(Urms)
  !$omp end single
  if (omp_master.and.debug(dbg_hd)) then; j=j+1;tm(j)=elapsed(); print '(a4,2f7.3)','urm',tm(j)-tm(j-1),tm(j)-tm(1); endif

  call print_trace('pde_mixed',dbg_hd,'pressure')
!-----------------------------------------------------------------------
!  Energy per unit mass
!-----------------------------------------------------------------------
  do iz=izs,ize
    wk11(:,:,iz) = e(:,:,iz)/r(:,:,iz)
  end do
!-----------------------------------------------------------------------
!  Boundary condition -- may need to calculate pressure internally
!-----------------------------------------------------------------------
  if (do_energy) call energy_boundary(r,wk01,Ux,Uy,Uz,e,wk11,wk21,Bx,By,Bz)
!-----------------------------------------------------------------------
!  Pressure
!-----------------------------------------------------------------------
  call pressure (r,wk11,wk21)
  call dumpn(wk21,'p1','pg.dmp',1)
  if (omp_master.and.debug(dbg_hd)) then; j=j+1;tm(j)=elapsed(); print '(a4,2f7.3)','eos',tm(j)-tm(j-1),tm(j)-tm(1); endif

!-----------------------------------------------------------------------
!  Numerical viscosity, Richtmyer & Morton type.  For testing purposes,
!  nu1<0 signals the use of constant diffusivity.
!-----------------------------------------------------------------------
  if (nu1.lt.0.0) then
    do iz=izs,ize
      wk12(:,:,iz) = sqrt(gamma*wk21(:,:,iz)/r(:,:,iz)) + sqrt(wk12(:,:,iz))
      wk10(:,:,iz) = abs(nu1)
    end do
    if (omp_master) nflop = nflop+4+2*10
  else if (do_mhd) then
    call xup1_set (Bx, scr1)
    call yup1_set (By, scr2)
    call zup1_set (Bz, scr3)
    if (cSmag > 0.) call crl1_set (Ux, Uz, Uz, scratch)
    do iz=izs,ize
    do iy=1,my
    do ix=1,mx
      wavesp = sqrt((scr1(ix,iy,iz)**2+scr2(ix,iy,iz)**2+scr3(ix,iy,iz)**2+ &
                     gamma*wk21(ix,iy,iz))/r(ix,iy,iz))
      wavesp = merge(wavesp,min(cmax,wavesp),cmax==0.)
      velocity = sqrt(wk12(ix,iy,iz))
      wk12(ix,iy,iz) = wavesp + velocity
      wk10(ix,iy,iz) = nu1*velocity &
        + max(dxm(ix),dym(iy),dzm(iz))*(nu2*wk10(ix,iy,iz) + cSmag*scratch(ix,iy,iz)) &
        + nu3*wavesp
    end do
    end do
    end do
    if (omp_master) nflop = nflop+5+2*10
  else
    if (cSmag > 0.) call crl1_set (Ux, Uz, Uz, scratch)
    do iz=izs,ize
    do iy=1,my
    do ix=1,mx
      wavesp = sqrt(gamma*wk21(ix,iy,iz)/r(ix,iy,iz))
      velocity = sqrt(wk12(ix,iy,iz))
      wk12(ix,iy,iz) = wavesp + velocity
      wk10(ix,iy,iz) = nu1*velocity &
        + max(dxm(ix),dym(iy),dzm(iz))*(nu2*wk10(ix,iy,iz) + cSmag*scratch(ix,iy,iz)) &
        + nu3*wavesp 
    end do
    end do
    end do
    if (omp_master) nflop = nflop+5+2*10
  end if
  call dumpn(wk10,'pde13','dpxdt.dmp',1)

!ptr  lnu => wk09
  call regularize(wk10)

  if (do_max5_hd) then
    call smooth3max5_set (wk10, scr1)
  else
    call smooth3max3_set (wk10, scr1)
  end if
  call dumpn(wk10,'pde14','dpxdt.dmp',1)

  call regularize(wk10)
  call stats('wk10',wk10)
  do iz=izs,ize
    wk09(:,:,iz) = alog(wk10(:,:,iz))
  end do
  if (omp_master) nflop = nflop+1
!  wk09 = smooth3(max3(wk09))
!  do iz=izs,ize
!    wk10(:,:,iz) = exp(wk09(:,:,iz))
!  end do

  if (flag) then
    do iz=izs,ize
    do iy=1,my
    do ix=1,mx
      wk12(ix,iy,iz) = wk12(ix,iy,iz)*(dt/min(dxm(ix),dym(iy),dzm(iz)))
    end do
    end do
    end do
    !Cu = fmaxval('Cu',wk12)
    call fmaxval_subr ('Cu',wk12,Cu)

!-----------------------------------------------------------------------
!  Viscosity Courand condition factors:
!     6.2 : the maximum value returned from ddxup(ddxdn(f)) when dx=1.
!      3. : for the worst case of a 3-D checker board
!      2. : overshoot with a factor 2. at Cdtd=1., for marginal stability
!-----------------------------------------------------------------------
    fCv = 6.2*3.*dt/2.

    do iz=izs,ize
    do iy=1,my
    do ix=1,mx
      wk12(ix,iy,iz) = wk10(ix,iy,iz)*(fCv/min(dxm(ix),dym(iy),dzm(iz)))
    end do
    end do
    end do
    !Cv = fmaxval('Cv',wk12)
    call fmaxval_subr ('Cv',wk12,Cv)
  end if
  if (omp_master) nflop = nflop+2
  !wk12 free

  do iz=izs,ize
    wk09(:,:,iz) = wk09(:,:,iz)+wk01(:,:,iz)
    wk10(:,:,iz) = wk10(:,:,iz)*r(:,:,iz)
  end do
  if (omp_master) nflop = nflop+2
  if (omp_master.and.debug(dbg_hd)) then; j=j+1;tm(j)=elapsed(); print '(a4,2f7.3)','dif',tm(j)-tm(j-1),tm(j)-tm(1); endif

  call stats ('wk10', wk10)

  call print_trace('pde_mixed',dbg_hd,'mass')
!-----------------------------------------------------------------------
!  Mass conservation, cell centered
!-----------------------------------------------------------------------
if (do_density) then
  if (nur==0) then
!ptr    ddxuppx => wk12; ddyuppy => wk13; ddzuppz => wk14;
    if (do_2nddiv) then
      call ddxup1_set (px, wk12)
      call ddyup1_set (py, wk13)
      call ddzup1_set (pz, wk14)
    else
      call ddxup_set (px, wk12)
      call ddyup_set (py, wk13)
      call ddzup_set (pz, wk14)
    end if
    call dumpn (drdt,'drdt','drdt.dmp',1)
    do iz=izs,ize
      drdt(:,:,iz) = drdt(:,:,iz) - wk12(:,:,iz) - wk13(:,:,iz) - wk14(:,:,iz)
    end do
    call dumpn (wk12,'wk12','drdt.dmp',1)
    call dumpn (wk13,'wk13','drdt.dmp',1)
    call dumpn (wk14,'wk14','drdt.dmp',1)
    call dumpn (drdt,'drdt','drdt.dmp',1)
    if (omp_master) nflop = nflop+3
    !wk12,wk13-15 free
  else
!
    call ddxdn_set (wk01, scr1)
    if (do_quench) call quenchx1(scr1)
    call xdn_set (wk09, scr2)
    do iz=izs,ize
    do iy=1,my
      call expn(mx,scr2(:,iy,iz),exp1)
      do ix=1,mx
        scr1(ix,iy,iz) = -px(ix,iy,iz)+(nur*dxmdn(ix))*exp1(ix)*scr1(ix,iy,iz)
      end do
    end do
    end do
    if (omp_master) nflop = nflop+6
    if (do_2nddiv) then
      call ddxup1_add (scr1, drdt)
    else
      call ddxup_add (scr1, drdt)
    end if
!
    call ddydn_set (wk01, scr1)
    if (do_quench) call quenchy1(scr1)
    call ydn_set (wk09, scr2)
    do iz=izs,ize
    do iy=1,my
      call expn(mx,scr2(:,iy,iz),exp1)
      do ix=1,mx
        scr1(ix,iy,iz) = -py(ix,iy,iz)+(nur*dymdn(iy))*exp1(ix)*scr1(ix,iy,iz)
      end do
    end do
    end do
    if (omp_master) nflop = nflop+6
    if (do_2nddiv) then
      call ddyup1_add (scr1, drdt)
    else
      call ddyup_add (scr1, drdt)
    end if
!
    call ddzdn_set (wk01, scr1)
    if (do_quench) call quenchz1(scr1)
    call barrier_omp('pde3')                                            ! ensure wk09 complete
    call zdn_set (wk09, scr2)
    do iz=izs,ize
    do iy=1,my
      call expn(mx,scr2(:,iy,iz),exp1)
      do ix=1,mx
        scr1(ix,iy,iz) = -pz(ix,iy,iz)+(nur*dzmdn(iz))*exp1(ix)*scr1(ix,iy,iz)
      end do
    end do
    end do
    if (omp_master) nflop = nflop+6
    call barrier_omp('pde4')                                            ! ensure scr1 complete

    if (do_2nddiv) then
      call ddzup1_add (scr1, drdt)
    else
      call ddzup_add (scr1, drdt)
    end if
    call barrier_omp('pde4')                                            ! ensure scr1 untouched
!
  end if
  call dumpn (drdt,'drdt','drdt.dmp',1)
end if
  !wk01 free
  if (omp_master.and.debug(dbg_hd)) then; j=j+1;tm(j)=elapsed(); print '(a4,2f7.3)','mas',tm(j)-tm(j-1),tm(j)-tm(1); endif
  END

!***********************************************************************
  SUBROUTINE pde3(r,px,py,pz,e,d,drdt,dpxdt,dpydt,dpzdt,dedt,dddt,flag, &
            Ux,Uy,Uz,Bx,By,Bz,dBxdt,dBydt,dBzdt, &
	    wk01,wk02,wk03,wk04,wk05,wk06,wk07,wk08,wk09,wk10, &
	    wk11,wk12,wk13,wk14,wk15,wk16,wk17,wk21, &
	    scr1,scr2,scr3,scr4,scr5,scr6)
  USE params
  USE pde_m
  implicit none
  logical flag, debug
  real, dimension(mx,my,mz) :: &
       r,px,py,pz,e,d,drdt,dpxdt,dpydt,dpzdt,dedt,dddt, &
       Ux,Uy,Uz,Bx,By,Bz,dBxdt,dBydt,dBzdt
  real, dimension(mx,my,mz) :: &
       wk01,wk02,wk03,wk04,wk05,wk06,wk07,wk08,wk09,wk10, &
       wk11,wk12,wk13,wk14,wk15,wk16,wk17,wk21, &
       scr1,scr2,scr3,scr4,scr5,scr6
  real, dimension(mx):: exp1, exp2, exp3
  real elapsed, nuS1, nuS2
  integer ix, iy, iz

  call print_trace('pde_mixed',dbg_hd,'diagonal')
  call dumpn(wk21,'p2','pg.dmp',1)
!-----------------------------------------------------------------------
!  Viscous stress, diagonal and off-diagonal elements
!-----------------------------------------------------------------------
  nstag=0
  nuS1 = 3.*nuS/(1.+2.*nuS)
  nuS2 = (1.-nuS)/(1.+2.*nuS)
!ptr  Txx => wk15; Tyy => wk16; Tzz => wk17
  if (do_quench) then
    call quenchx1(wk05)
    call quenchy1(wk06)
    call quenchz1(wk07)
  end if
  do iz=izs,ize
    do iy=1,my
    do ix=1,mx
      wk15(ix,iy,iz) = - dxm(ix)*wk10(ix,iy,iz)*(nuS1*wk05(ix,iy,iz) + nuS2*wk17(ix,iy,iz)) + wk21(ix,iy,iz)
      wk16(ix,iy,iz) = - dym(iy)*wk10(ix,iy,iz)*(nuS1*wk06(ix,iy,iz) + nuS2*wk17(ix,iy,iz)) + wk21(ix,iy,iz)
      wk17(ix,iy,iz) = - dzm(iz)*wk10(ix,iy,iz)*(nuS1*wk07(ix,iy,iz) + nuS2*wk17(ix,iy,iz)) + wk21(ix,iy,iz)
    end do
    end do
    if (do_energy) then
      if (do_dissipation) then 
        dedt(:,:,iz) = dedt(:,:,iz) &
                     - wk15(:,:,iz)*wk05(:,:,iz) &
                     - wk16(:,:,iz)*wk06(:,:,iz) &
                     - wk17(:,:,iz)*wk07(:,:,iz)
      else
        dedt(:,:,iz) = dedt(:,:,iz) - wk21(:,:,iz)* &
                       (wk05(:,:,iz) + wk06(:,:,iz) + wk07(:,:,iz))
      end if
    end if
  end do
  if (omp_master) nflop = nflop+15
  call dumpn(dedt,'dedt','dpxdt.dmp',1)
  call dumpn(wk15,'pde6','dpxdt.dmp',1)
  call dumpn(wk05,'pde7','dpxdt.dmp',1)
  call dumpn(wk10 ,'pde8','dpxdt.dmp',1)

!ptr  Sxy => wk05; Syz => wk06; Szx => wk07
!ptr  ddxdnUy => scr1; ddzdnUy => scr2; ddydnUz => scr3
!ptr  ddxdnUz => scr4; ddzdnUx => scr5; ddydnUx => scr6
  call ddxdn_set(Uy,scr1)
  call ddzdn_set(Uy,scr2)
  call ddydn_set(Ux,scr6)
  call ddzdn_set(Ux,scr5)
  call ddxdn_set(Uz,scr4)
  call ddydn_set(Uz,scr3)
  if (do_quench) then
    call quenchx1 (scr1); call quenchx1 (scr4)
    call quenchy1 (scr3); call quenchx1 (scr6)
    call quenchz1 (scr2); call quenchx1 (scr5)
  end if
  do iz=izs,ize
    wk05(:,:,iz) = (scr1(:,:,iz)+scr6(:,:,iz))*0.5
    wk06(:,:,iz) = (scr3(:,:,iz)+scr2(:,:,iz))*0.5
    wk07(:,:,iz) = (scr5(:,:,iz)+scr4(:,:,iz))*0.5
  end do
  if (omp_master) nflop = nflop+6
  !scr1-3,scr4-6 free
!ptr  Txy => scr4; Tyz => scr5; Tzx => scr6
!ptr  xydnlnu => wk12; xzdnlnu => wk13; yzdnlnu => wk14
  call xdn_set (wk09,scr1)
  call ydn_set (scr1,wk12)
  call barrier_omp('pde5')                                              ! ensure scr1 complete
  call zdn_set (scr1,wk13)
  call barrier_omp('pde6')                                              ! ensure scr1 complete
  call ydn_set (wk09,scr1)
  call barrier_omp('pde7')                                              ! ensure scr1 complete
  call zdn_set (scr1,wk14)
  do iz=izs,ize
  do iy=1,my
    call expn(mx,wk12(:,iy,iz),exp1)
    call expn(mx,wk14(:,iy,iz),exp2)
    call expn(mx,wk13(:,iy,iz),exp3)
    do ix=1,mx
      scr4(ix,iy,iz) = - (0.5*nuS1*(dxmdn(ix)+dymdn(iy)))*exp1(ix)*wk05(ix,iy,iz)
      scr5(ix,iy,iz) = - (0.5*nuS1*(dymdn(iy)+dzmdn(iz)))*exp2(ix)*wk06(ix,iy,iz)
      scr6(ix,iy,iz) = - (0.5*nuS1*(dzmdn(iz)+dxmdn(ix)))*exp3(ix)*wk07(ix,iy,iz)
    end do
  end do
  end do
  if (omp_master) nflop = nflop+9
  !wk12,wk13-wk14 free

  if (do_energy .and. do_dissipation) then
    do iz=izs,ize
    do iy=1,my
    do ix=1,mx
      wk05(ix,iy,iz) = -wk05(ix,iy,iz)*scr4(ix,iy,iz)                   ! dissipation xy
      wk06(ix,iy,iz) = -wk06(ix,iy,iz)*scr5(ix,iy,iz)                   ! dissipation yz
      wk07(ix,iy,iz) = -wk07(ix,iy,iz)*scr6(ix,iy,iz)                   ! dissipation zx
    end do
    end do
    end do
    call barrier_omp('pde7a')                                           ! ensure scr1 untouched
    call xup1_set (wk05, scr1)
    call yup1_set (scr1, wk05)
    call yup1_set (wk06, scr1)
    call barrier_omp('pde8')                                            ! ensure scr1 complete
    call zup1_set (scr1, wk06)
    call barrier_omp('pde9')                                            ! ensure scr1 complete
    call zup1_set (wk07, scr1)
    call barrier_omp('pde10')                                           ! ensure scr1 complete
    call xup1_set (scr1, wk07)

    do iz=izs,ize
    do iy=1,my
    do ix=1,mx
      dedt(ix,iy,iz) = dedt(ix,iy,iz) + &
        2.*(wk05(ix,iy,iz) + wk06(ix,iy,iz) + wk07(ix,iy,iz))           ! over and under diagonal -> 2.*
    end do
    end do
    end do
    call dumpn(dedt,'dedt','dpxdt.dmp',1)
    if (omp_master) nflop = nflop+6
  end if
  !wk05-wk07 free
  if (omp_master.and.debug(dbg_hd)) then; j=j+1;tm(j)=elapsed(); print '(a4,2f7.3)','vst',tm(j)-tm(j-1),tm(j)-tm(1); endif

!-----------------------------------------------------------------------
!  Add Reynolds stress
!-----------------------------------------------------------------------
!ptr  xupUx => wk05; yupUy => wk06; zupUz => wk07
  call xup1_set(Ux,wk05)
  call yup1_set(Uy,wk06)
  call zup1_set(Uz,wk07)
  do iz=izs,ize
    wk15(:,:,iz) = wk15(:,:,iz) + r(:,:,iz)*wk05(:,:,iz)**2
    wk16(:,:,iz) = wk16(:,:,iz) + r(:,:,iz)*wk06(:,:,iz)**2
    wk17(:,:,iz) = wk17(:,:,iz) + r(:,:,iz)*wk07(:,:,iz)**2
  end do
  if (omp_master) nflop = nflop+9
  !wk05-wk07 free

!ptr  xydnl => wk05; xdnUy => wk06; ydnUx => wk07
  call xdn_set(wk03, wk05)
  call xdn_set(Uy, wk06)
  call ydn_set(Ux, wk07)
  do iz=izs,ize
  do iy=1,my
    call expn(mx,wk05(:,iy,iz),exp1)
    do ix=1,mx
      scr4(ix,iy,iz) = scr4(ix,iy,iz) + exp1(ix)*wk07(ix,iy,iz)*wk06(ix,iy,iz)
    end do
  end do
  end do
  if (omp_master) nflop = nflop+4
  !wk05-wk07 free

!ptr  yzdnl => wk05; ydnUz => wk06; zdnUy => wk07
  call ydn_set(wk04, wk05)
  call ydn_set(Uz, wk06)
  call zdn_set(Uy, wk07)
  do iz=izs,ize
  do iy=1,my
    call expn(mx,wk05(:,iy,iz),exp1)
    do ix=1,mx
      scr5(ix,iy,iz) = scr5(ix,iy,iz) + exp1(ix)*wk07(ix,iy,iz)*wk06(ix,iy,iz)
    end do
  end do
  end do
  if (omp_master) nflop = nflop+4
  !wk05-wk07 free

!ptr  zxdnl => wk05; zdnUx => wk06; xdnUz => wk07
  call zdn_set(wk02, wk05)
  call zdn_set(Ux, wk06)
  call xdn_set(Uz, wk07)
  do iz=izs,ize
  do iy=1,my
    call expn(mx,wk05(:,iy,iz),exp1)
    do ix=1,mx
      scr6(ix,iy,iz) = scr6(ix,iy,iz) + exp1(ix)*wk07(ix,iy,iz)*wk06(ix,iy,iz)
    end do
  end do
  end do
  if (omp_master) nflop = nflop+4
  !wk05-wk07 free
  !wk02-wk04 free
  if (omp_master.and.debug(dbg_hd)) then; j=j+1;tm(j)=elapsed(); print '(a4,2f7.3)','rst',tm(j)-tm(j-1),tm(j)-tm(1); endif
  END

!***********************************************************************
  SUBROUTINE pde4(r,px,py,pz,e,d,drdt,dpxdt,dpydt,dpzdt,dedt,dddt,flag, &
            Ux,Uy,Uz,Bx,By,Bz,dBxdt,dBydt,dBzdt, &
	    wk01,wk02,wk03,wk04,wk05,wk06,wk07,wk08,wk09,wk10, &
	    wk11,wk12,wk13,wk14,wk15,wk16,wk17,wk21, &
	    scr1,scr2,scr3,scr4,scr5,scr6)
  USE params
  USE pde_m
  implicit none
  logical flag, debug
  real, dimension(mx,my,mz) :: &
       r,px,py,pz,e,d,drdt,dpxdt,dpydt,dpzdt,dedt,dddt, &
       Ux,Uy,Uz,Bx,By,Bz,dBxdt,dBydt,dBzdt
  real, dimension(mx,my,mz) :: &
       wk01,wk02,wk03,wk04,wk05,wk06,wk07,wk08,wk09,wk10, &
       wk11,wk12,wk13,wk14,wk15,wk16,wk17,wk21, &
       scr1,scr2,scr3,scr4,scr5,scr6
  real, dimension(mx):: exp1, exp2
  real elapsed
  integer ix, iy, iz

  call print_trace('pde_mixed',dbg_hd,'off-diagonal')
  call dumpn(wk21,'p3','pg.dmp',1)
!-----------------------------------------------------------------------
!  Equations of motion, face centred
!-----------------------------------------------------------------------
!ptr  ddxdnTxx => wk02; ddyupTxy => wk03; ddzupTzx => wk04
  call barrier_omp('pde11')                                             ! ensure scr6 complete
  if (do_2nddiv) then
    call ddxdn1_set (wk15, wk02)
    call ddyup1_set (scr4, wk03)
    call ddzup1_set (scr6, wk04)
  else
    call ddxdn_set (wk15, wk02)
    call ddyup_set (scr4, wk03)
    call ddzup_set (scr6, wk04)
  end if
  do iz=izs,ize
    dpxdt(:,:,iz) = dpxdt(:,:,iz) - wk02(:,:,iz) - wk03(:,:,iz) - wk04(:,:,iz)
  end do
  call dumpn(dpxdt,'pde2','dpxdt.dmp',1)
  call dumpn(wk02,'pde3','dpxdt.dmp',1)
  call dumpn(wk03,'pde4','dpxdt.dmp',1)
  call dumpn(wk04,'pde5','dpxdt.dmp',1)
  if (omp_master) nflop = nflop+3
  !wk02-wk04 free

!ptr  ddydnTyy => wk02; ddzupTyz => wk03; ddxupTxy => wk04
  if (do_2nddiv) then
    call ddydn1_set (wk16, wk02)
    call ddzup1_set (scr5, wk03)
    call ddxup1_set (scr4, wk04)
  else
    call ddydn_set (wk16, wk02)
    call ddzup_set (scr5, wk03)
    call ddxup_set (scr4, wk04)
  end if
  call barrier_omp('pde14')                                             ! ensure scr5 is untouched
  do iz=izs,ize
    dpydt(:,:,iz) = dpydt(:,:,iz) - wk02(:,:,iz) - wk03(:,:,iz) - wk04(:,:,iz)
  end do
  call dumpn(dpydt,'dpydt','dpxdt.dmp',1)
  if (omp_master) nflop = nflop+3
  !wk02-wk04 free

!ptr  ddzdnTzz => wk02; ddxupTzx => wk03; ddyupTyz => wk04
  if (do_2nddiv) then
    call ddzdn1_set (wk17, wk02)
    call ddxup1_set (scr6, wk03)
    call ddyup1_set (scr5, wk04)
  else
    call ddzdn_set (wk17, wk02)
    call ddxup_set (scr6, wk03)
    call ddyup_set (scr5, wk04)
  end if
  do iz=izs,ize
    dpzdt(:,:,iz) = dpzdt(:,:,iz) - wk02(:,:,iz) - wk03(:,:,iz) - wk04(:,:,iz)
  end do
  if (omp_master) nflop = nflop+3
  !wk02-wk04 free
  !scr4-6,wk15,wk16,wk17 free
  if (omp_master.and.debug(dbg_hd)) then; j=j+1;tm(j)=elapsed(); print '(a4,2f7.3)','eom',tm(j)-tm(j-1),tm(j)-tm(1); endif

!-----------------------------------------------------------------------
!  Abundance per unit mass
!-----------------------------------------------------------------------
  if (do_pscalar .or. do_cool) then
!ptr    dd => wk12
    if (do_pscalar) then
      do iz=izs,ize
        wk12(:,:,iz)=d(:,:,iz)/r(:,:,iz)
      end do
    end if
    if (omp_master) nflop = nflop+1
  end if

!-----------------------------------------------------------------------
!  Passive scalar
!-----------------------------------------------------------------------
  call passive(wk10,wk09,r,px,py,pz,Ux,Uy,Uz,wk12,dddt)
  call trace_particles (Ux,Uy,Uz)
  !wk10 free
  if (omp_master.and.debug(dbg_hd)) then; j=j+1;tm(j)=elapsed(); print '(a4,2f7.3)','pas',tm(j)-tm(j-1),tm(j)-tm(1); endif

  call print_trace('pde_mixed',dbg_hd,'energy')
!-----------------------------------------------------------------------
!  Energy equation
!-----------------------------------------------------------------------
!ptr  lne => wk10
!!  wk09 = wk09+alog(2.)
  do iz=izs,ize
    wk10(:,:,iz) = alog(wk11(:,:,iz))
    wk09(:,:,iz) = wk09(:,:,iz)+wk10(:,:,iz)
  end do
  if (omp_master) nflop = nflop+2
  if (omp_master.and.debug(dbg_hd)) then; j=j+1;tm(j)=elapsed(); print '(a4,2f7.3)','wk10',tm(j)-tm(j-1),tm(j)-tm(1); endif

if (do_energy) then

!-----------------------------------------------------------------------
!  Energy advection and diffusion
!-----------------------------------------------------------------------
!ptr  eflux => wk01; xdnlnu => scr4; xdnlne => scr5; ddxdnlne => scr6
  call xdn1_set (wk09, scr4)
  call xdn1_set (wk10, scr5)
  call ddxdn_set (wk10, scr6)
  do iz=izs,ize
  do iy=1,my
    call expn(mx,scr5(:,iy,iz),exp1)
    call expn(mx,scr4(:,iy,iz),exp2)
    do ix=1,mx
      wk01(ix,iy,iz) = exp1(ix)*px(ix,iy,iz) - dxm(ix)*exp2(ix)*scr6(ix,iy,iz)
    end do
  end do
  end do
  if (omp_master) nflop = nflop+6
  !scr4-scr6 free

  if (do_2nddiv) then
    call ddxup1_set (wk01, scr1)
  else
    call ddxup_set (wk01, scr1)
  end if
  do iz=izs,ize
    dedt(:,:,iz) = dedt(:,:,iz) - scr1(:,:,iz)
  end do
  if (omp_master) nflop = nflop+1

!ptr  ydnlnu => scr4; ydnlne => scr5; ddydnlne => scr6
  call ydn1_set (wk09, scr4)
  call ydn1_set (wk10, scr5)
  call ddydn_set (wk10, scr6)
  do iz=izs,ize
  do iy=1,my
    call expn(mx,scr5(:,iy,iz),exp1)
    call expn(mx,scr4(:,iy,iz),exp2)
    do ix=1,mx
      wk01(ix,iy,iz) = exp1(ix)*py(ix,iy,iz) - dym(iy)*exp2(ix)*scr6(ix,iy,iz)
    end do
  end do
  end do
  if (omp_master) nflop = nflop+6
  !scr4-scr6 free

  if (do_2nddiv) then
    call ddyup1_set (wk01, scr1)
  else
    call ddyup_set (wk01, scr1)
  end if
  do iz=izs,ize
    dedt(:,:,iz) = dedt(:,:,iz) - scr1(:,:,iz)
  end do
  if (omp_master) nflop = nflop+1

!ptr  zdnlnu => scr4; zdnlne => scr5; ddzdnlne => scr6
  call barrier_omp('pde12')                                             ! ensure wk10 complete
  call zdn1_set (wk09, scr4)
  call zdn1_set (wk10, scr5)
  call ddzdn_set (wk10, scr6)
  do iz=izs,ize
  do iy=1,my
    call expn(mx,scr5(:,iy,iz),exp1)
    call expn(mx,scr4(:,iy,iz),exp2)
    do ix=1,mx
      wk01(ix,iy,iz) = exp1(ix)*pz(ix,iy,iz) - dzm(iz)*exp2(ix)*scr6(ix,iy,iz)
    end do
  end do
  end do
  if (omp_master) nflop = nflop+6
  !scr4-scr6 free

  call barrier_omp('pde13')                                             ! ensure wk01 complete
  if (do_2nddiv) then
    call ddzup1_set (wk01, scr1)
  else
    call ddzup_set (wk01, scr1)
  end if
  do iz=izs,ize
    dedt(:,:,iz) = dedt(:,:,iz) - scr1(:,:,iz)
  end do
  if (omp_master) nflop = nflop+1
  !wk01 free
end if

  call print_trace('pde_mixed',dbg_hd,'cooling')
!-----------------------------------------------------------------------
!  Cooling and conduction
!-----------------------------------------------------------------------
  END

!***********************************************************************
  SUBROUTINE pde5(r,px,py,pz,e,d,drdt,dpxdt,dpydt,dpzdt,dedt,dddt,flag, &
            Ux,Uy,Uz,Bx,By,Bz,dBxdt,dBydt,dBzdt,Ex,Ey,Ez, &
	    wk01,wk02,wk03,wk04,wk05,wk06,wk07,wk08,wk09,wk10, &
	    wk11,wk12,wk13,wk14,wk15,wk16,wk17,wk21, &
	    scr1,scr2,scr3,scr4,scr5,scr6)
  USE params
  USE pde_m
  implicit none
  logical flag, debug
  real, dimension(mx,my,mz) :: &
       r,px,py,pz,e,d,drdt,dpxdt,dpydt,dpzdt,dedt,dddt, &
       Ux,Uy,Uz,Bx,By,Bz,dBxdt,dBydt,dBzdt,Ex,Ey,Ez
  real, dimension(mx,my,mz) :: &
       wk01,wk02,wk03,wk04,wk05,wk06,wk07,wk08,wk09,wk10, &
       wk11,wk12,wk13,wk14,wk15,wk16,wk17,wk21, &
       scr1,scr2,scr3,scr4,scr5,scr6
  real, dimension(mx):: exp1, exp2, exp3
  real elapsed
  integer ix, iy, iz

  if (omp_master.and.debug(dbg_hd)) then; j=j+1;tm(j)=elapsed(); print '(a4,2f7.3)','eeq',tm(j)-tm(j-1),tm(j)-tm(1); endif
  call dumpn(wk21,'p4','pg.dmp',1)

!-----------------------------------------------------------------------
!  MHD
!-----------------------------------------------------------------------
    call dumpn(dedt,'dedt','dpxdt.dmp',1)
  if (do_mhd) then
    call mhd (flag, &
            r,e,wk21,Ux,Uy,Uz,Bx,By,Bz,Ex,Ey,Ez, &
	    dpxdt,dpydt,dpzdt,dedt,dBxdt,dBydt,dBzdt)
  call dumpn(wk21,'p5','pg.dmp',1)
  if (omp_master.and.debug(dbg_hd)) then; j=j+1;tm(j)=elapsed(); print '(a4,2f7.3)','mhd',tm(j)-tm(j-1),tm(j)-tm(1); endif
    call dumpn(dedt,'dedt','dpxdt.dmp',1)
    call dumpn(dpydt,'dpydt','dpxdt.dmp',1)
  end if
  !scr4-Uz free

  call dumpn (drdt,'drdt','drdt.dmp',1)
  call ddt_boundary (r,px,py,pz,e,wk21,Bx,By,Bz, &
                     drdt,dpxdt,dpydt,dpzdt, &
                     dedt,dBxdt,dBydt,dBzdt)
  call dumpn (drdt,'drdt','drdt.dmp',1)

  call print_trace('pde_mixed',dbg_hd,'end PDE')
  END
