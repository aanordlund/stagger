MODULE stagger
CONTAINS
! $Id: stagger.pro,v 1.1 2001/12/11 08:27:38 aake Exp &
!;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
!
!  6th order staggered derivatives, 5th order interpolation
!
!;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
FUNCTION xup (f)
!
!  f is centered on (i-.5,j,k), fifth order stagger
!
USE params
real, dimension(mx,my,mz):: f, xup
!hpf$ distribute(*,*,block):: f, xup
!-----------------------------------------------------------------------
  c = 3./256.
  b = -25./256.
  a = .5-b-c
  xup = a*(f + cshift(f,dim=1,shift=1)) &
      + b*(cshift(f,dim=1,shift=-1) + cshift(f,dim=1,shift=2)) &
      + c*(cshift(f,dim=1,shift=-2) + cshift(f,dim=1,shift=3))
END FUNCTION
SUBROUTINE xup_set (f,xup)
!
!  f is centered on (i-.5,j,k), fifth order stagger
!
USE params
real, dimension(mx,my,mz):: f, xup
!hpf$ distribute(*,*,block):: f, xup
!-----------------------------------------------------------------------
  c = 3./256.
  b = -25./256.
  a = .5-b-c
  xup = a*(f + cshift(f,dim=1,shift=1)) &
      + b*(cshift(f,dim=1,shift=-1) + cshift(f,dim=1,shift=2)) &
      + c*(cshift(f,dim=1,shift=-2) + cshift(f,dim=1,shift=3))
END SUBROUTINE
!;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
FUNCTION yup (f)
!
!  f is centered on (i,j-.5,k)
!
USE params
real, dimension(mx,my,mz):: f, yup
!hpf$ distribute(*,*,block):: f, yup
!-----------------------------------------------------------------------
  c = 3./256.
  b = -25./256.
  a = .5-b-c
  yup = a*(f + cshift(f,dim=2,shift=1)) &
      + b*(cshift(f,dim=2,shift=-1) + cshift(f,dim=2,shift=2)) &
      + c*(cshift(f,dim=2,shift=-2) + cshift(f,dim=2,shift=3))
END FUNCTION
!;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
FUNCTION zup (f)
!
!  f is centered on (i,j,k-.5)
!
USE params
real, dimension(mx,my,mz):: f, zup
!hpf$ distribute(*,*,block):: f, zup
!-----------------------------------------------------------------------
  c = 3./256.
  b = -25./256.
  a = .5-b-c
  zup = a*(f + cshift(f,dim=3,shift=1)) &
      + b*(cshift(f,dim=3,shift=-1) + cshift(f,dim=3,shift=2)) &
      + c*(cshift(f,dim=3,shift=-2) + cshift(f,dim=3,shift=3))
END FUNCTION
!;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
FUNCTION xdn (f)
!
!  f is centered on (i-.5,j,k)
!
USE params
real, dimension(mx,my,mz):: f, xdn
!hpf$ distribute(*,*,block):: f, xdn
!-----------------------------------------------------------------------
  c = 3./256.
  b = -25./256.
  a = .5-b-c
  xdn = a*(f + cshift(f,dim=1,shift=-1)) &
      + b*(cshift(f,dim=1,shift=1) + cshift(f,dim=1,shift=-2)) &
      + c*(cshift(f,dim=1,shift=2) + cshift(f,dim=1,shift=-3))
END FUNCTION
!;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
FUNCTION ydn (f)
!
!  f is centered on (i,j,k-.5)
!
USE params
real, dimension(mx,my,mz):: f, ydn
!hpf$ distribute(*,*,block):: f, ydn
!-----------------------------------------------------------------------
  c = 3./256.
  b = -25./256.
  a = .5-b-c
  ydn = a*(f + cshift(f,dim=2,shift=-1)) &
      + b*(cshift(f,dim=2,shift=1) + cshift(f,dim=2,shift=-2)) &
      + c*(cshift(f,dim=2,shift=2) + cshift(f,dim=2,shift=-3))
END FUNCTION
!;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
FUNCTION zdn (f)
!
!  f is centered on (i,j,k-.5)
!
USE params
real, dimension(mx,my,mz):: f, zdn
!hpf$ distribute(*,*,block):: f, zdn
!-----------------------------------------------------------------------
  c = 3./256.
  b = -25./256.
  a = .5-b-c
  zdn = a*(f + cshift(f,dim=3,shift=-1)) &
      + b*(cshift(f,dim=3,shift=1) + cshift(f,dim=3,shift=-2)) &
      + c*(cshift(f,dim=3,shift=2) + cshift(f,dim=3,shift=-3))
END FUNCTION
!;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
FUNCTION ddxup (f)
!
!  X partial derivative
!
USE params
real, dimension(mx,my,mz):: f, ddxup
!hpf$ distribute(*,*,block):: f, ddxup
!-----------------------------------------------------------------------
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)/dx
  c = c/dx
  b = b/dx
  ddxup = a *(cshift(f,dim=1,shift=1) - f) &
  + b *(cshift(f,dim=1,shift=2) - cshift(f,dim=1,shift=-1)) &
  + c *(cshift(f,dim=1,shift=3) - cshift(f,dim=1,shift=-2))
END FUNCTION
!;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
FUNCTION ddyup (f)
!
!  Y partial derivative
!
USE params
real, dimension(mx,my,mz):: f, ddyup
!hpf$ distribute(*,*,block):: f, ddyup
!-----------------------------------------------------------------------
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)/dy
  c = c/dy
  b = b/dy
  ddyup = a *(cshift(f,dim=2,shift=1) - f) &
  + b *(cshift(f,dim=2,shift=2) - cshift(f,dim=2,shift=-1)) &
  + c *(cshift(f,dim=2,shift=3) - cshift(f,dim=2,shift=-2))
END FUNCTION
!;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
FUNCTION ddzup (f)
!
!  Z partial derivative
!
USE params
real, dimension(mx,my,mz):: f, ddzup
!hpf$ distribute(*,*,block):: f, ddzup
!-----------------------------------------------------------------------
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)/dz
  c = c/dz
  b = b/dz
  ddzup = a *(cshift(f,dim=3,shift=1) - f) &
  + b *(cshift(f,dim=3,shift=2) - cshift(f,dim=3,shift=-1)) &
  + c *(cshift(f,dim=3,shift=3) - cshift(f,dim=3,shift=-2))
END FUNCTION
!;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
FUNCTION ddxdn (f)
!
!  X partial derivative
!
USE params
real, dimension(mx,my,mz):: f, ddxdn
!hpf$ distribute(*,*,block):: f, ddxdn
!-----------------------------------------------------------------------
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)/dx
  c = c/dx
  b = b/dx
  ddxdn = a*(f - cshift(f,dim=1,shift=-1)) &
      + b*(cshift(f,dim=1,shift=1) - cshift(f,dim=1,shift=-2)) &
      + c*(cshift(f,dim=1,shift=2) - cshift(f,dim=1,shift=-3))
END FUNCTION
!;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
FUNCTION ddydn (f)
!
!  Y partial derivative
!
USE params
real, dimension(mx,my,mz):: f, ddydn
!hpf$ distribute(*,*,block):: f, ddydn
!-----------------------------------------------------------------------
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)/dy
  c = c/dy
  b = b/dy
  ddydn = a*(f - cshift(f,dim=2,shift=-1)) &
      + b*(cshift(f,dim=2,shift=1) - cshift(f,dim=2,shift=-2)) &
      + c*(cshift(f,dim=2,shift=2) - cshift(f,dim=2,shift=-3))
END FUNCTION
!;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
FUNCTION ddzdn (f)
!
!  Z partial derivative
!
USE params
real, dimension(mx,my,mz):: f, ddzdn
!hpf$ distribute(*,*,block):: f, ddzdn
!-----------------------------------------------------------------------
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)/dz
  c = c/dz
  b = b/dz
  ddzdn = a*(f - cshift(f,dim=3,shift=-1)) &
      + b*(cshift(f,dim=3,shift=1) - cshift(f,dim=3,shift=-2)) &
      + c*(cshift(f,dim=3,shift=2) - cshift(f,dim=3,shift=-3))
END FUNCTION
!;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
FUNCTION smooth3 (f)
!
!  Three point smoothing
!
USE params
real, dimension(mx,my,mz):: f, smooth3
!hpf$ distribute(*,*,block):: f, smooth3
!-----------------------------------------------------------------------
  smooth3 = (f       + cshift(f      ,dim=1,shift=-1) + cshift(f      ,dim=1,shift=1))*(1./3.)
  smooth3 = (smooth3 + cshift(smooth3,dim=2,shift=-1) + cshift(smooth3,dim=2,shift=1))*(1./3.)
  smooth3 = (smooth3 + cshift(smooth3,dim=3,shift=-1) + cshift(smooth3,dim=3,shift=1))*(1./3.)
END FUNCTION
!***********************************************************************
FUNCTION max5 (f)
!
!  Three point max
!
  USE params
  implicit none
  integer i,j,k,km1,kp1,km2,kp2
  real, dimension(mx,my,mz), intent(in) :: f
  real, dimension(mx,my,mz):: max5
  real, dimension(-1:mx+2):: fx
  real, dimension(-1:my+2):: fy
  real, dimension(mx,my,mz):: fz
!hpf$ distribute(*,*,block)::  f, fz, max5
!-----------------------------------------------------------------------
      
!$ do across local(i,j,k,fx)
  do k=1,mz
  do j=1,my
    do i=1,mx
      fx(i)=f(i,j,k)
    enddo
    fx(  -1)=fx(mx-1)
    fx(   0)=fx(mx  )
    fx(mx+1)=fx(   1)
    fx(mx+2)=fx(   2)
    do i=1,mx
      fz(i,j,k)=amax1(fx(i-2),fx(i-1),fx(i),fx(i+1),fx(i+2))
    end do
  end do
  end do
!$ do across local(i,j,k,fy)
  do k=1,mz
  do i=1,mx
    do j=1,my
      fy(j)=fz(i,j,k)
    enddo
    fy(  -1)=fy(my-1)
    fy(   0)=fy(my  )
    fy(my+1)=fy(   1)
    fy(my+2)=fy(   2)
    do j=1,my
      fz(i,j,k)=amax1(fy(j-2),fy(j-1),fy(j),fy(j+1),fy(j+2))
    end do
   end do
  end do
!$ do across local(i,j,k,km1,kp1,km2,kp2)
  do k=1,mz
    km2=mod(k+mz-3,mz)+1
    km1=mod(k+mz-2,mz)+1
    kp1=mod(k+mz  ,mz)+1
    kp2=mod(k+mz+1,mz)+1
    do j=1,my
    do i=1,mx
      max5(i,j,k)=amax1(fz(i,j,km2),fz(i,j,km1),fz(i,j,k),fz(i,j,kp1),fz(i,j,kp2))
    enddo
    enddo
  enddo
END FUNCTION

!***********************************************************************
SUBROUTINE dif2_set (fx, fy, fz, difu)
!
!  Finite difference one zone left to one zones right.
!

  USE params
  real, dimension(mx,my,mz):: fx, fy, fz, difu
!hpf$ distribute(*,*,block):: fx, fy, fz, difu
!-----------------------------------------------------------------------

   difu = cshift(fx,dim=1,shift=-1) - cshift(fx,dim=1,shift=1) &
        + cshift(fy,dim=2,shift=-1) - cshift(fy,dim=2,shift=1) &
        + cshift(fz,dim=3,shift=-1) - cshift(fz,dim=3,shift=1)
END subroutine

SUBROUTINE init_stagger
END SUBROUTINE
SUBROUTINE init_derivs
END SUBROUTINE
END MODULE stagger
