! $Id: hyman.f90,v 1.9 2013/08/02 12:20:38 aake Exp $
!************************************************************************
MODULE timeintegration
  implicit none

  real, dimension(3):: alpha, beta
  real, allocatable, dimension(:,:,:) :: rold, pxold, pyold, pzold, &
                                         eold, Bxold, Byold, Bzold, &
                                         dold
  real, allocatable, dimension(:,:,:) :: rsav, pxsav, pysav, pzsav, &
                                         esav, Bxsav, Bysav, Bzsav, &
                                         dsav
END MODULE timeintegration

!------------------------------------------------------------------------
SUBROUTINE init_timestep
  USE params
  USE timeintegration

  if (nstep<=3) timeorder=1
  if (master) print *,'init_timestep',do_density,do_momenta, do_energy, do_pscalar, do_mhd
  
  if (do_density) then
    allocate (rold(mx,my,mz))
    allocate (rsav(mx,my,mz))
  end if
  if (do_momenta) then
    allocate (pxold(mx,my,mz))
    allocate (pyold(mx,my,mz))
    allocate (pzold(mx,my,mz))
    allocate (pxsav(mx,my,mz))
    allocate (pysav(mx,my,mz))
    allocate (pzsav(mx,my,mz))
  end if
  if (do_energy) then
    allocate (eold(mx,my,mz))
    allocate (esav(mx,my,mz))
  end if
  if (do_pscalar) then
    allocate (dold(mx,my,mz))
    allocate (dsav(mx,my,mz))
  end if
  if (do_mhd) then
    allocate (Bxold(mx,my,mz))
    allocate (Byold(mx,my,mz))
    allocate (Bzold(mx,my,mz))
    allocate (Bxsav(mx,my,mz))
    allocate (Bysav(mx,my,mz))
    allocate (Bzsav(mx,my,mz))
  end if
END SUBROUTINE

!------------------------------------------------------------------------
SUBROUTINE timestep(r,px,py,pz,e,d,drdt,dpxdt,dpydt,dpzdt,dedt,dddt, &
                      Bx,By,Bz,dBxdt,dBydt,dBzdt)
!
!  3rd order, 2-N storage Runge-Kutta.  Advances the eight MHD variables
!  forward in time, given time derivatives computed in the subroutine pde.
!
  USE params
  USE timeintegration
  implicit none

  integer nx, ny, nz, iz, iy, ix
  real, dimension(mx,my,mz) :: &
        r,px,py,pz,e,d, &
        drdt,dpxdt,dpydt,dpzdt,dedt,dddt, &
        Bx,By,Bz,dBxdt,dBydt,dBzdt
  real :: drtdt
  real :: a1, a2, a3, b1, b2, c2, dtr, f
  logical debug
  character(len=mid):: id="$Id: hyman.f90,v 1.9 2013/08/02 12:20:38 aake Exp $"

  call print_id (id)

!------------------------------------------------------------------------

  f = 2./3.                                                              ! timestep reduction
  !$omp parallel private(iz,a1,a2,a3,b1,b2,c2,dtr)
  do iz=izs,ize
    if (do_density) drdt(:,:,iz)  = 0.
    if (do_energy)  dedt(:,:,iz)  = 0.
    if (do_pscalar) dddt(:,:,iz)  = 0.
    if (do_momenta) then
                    dpxdt(:,:,iz) = 0.
                    dpydt(:,:,iz) = 0.
                    dpzdt(:,:,iz) = 0.
    end if
    if (do_mhd) then
                    dBxdt(:,:,iz) = 0.
                    dBydt(:,:,iz) = 0.
                    dBzdt(:,:,iz) = 0.
    end if
  end do

  !$omp single
  isubstep = 1
  !$omp end single
  call pde(.true.)
  call courant

  if (it==1 .or. timeorder==1) then
    do iz=izs,ize
      if (do_density) rold(:,:,iz)  = r(:,:,iz)
      if (do_energy)  eold(:,:,iz)  = e(:,:,iz)
      if (do_pscalar) dold(:,:,iz)  = d(:,:,iz)
      if (do_momenta) then
                      pxold(:,:,iz) = px(:,:,iz)
                      pyold(:,:,iz) = py(:,:,iz)
                      pzold(:,:,iz) = pz(:,:,iz)
      end if
      if (do_mhd) then
                      Bxold(:,:,iz) = Bx(:,:,iz)
                      Byold(:,:,iz) = By(:,:,iz)
                      Bzold(:,:,iz) = Bz(:,:,iz)
      end if
      if (do_density) r(:,:,iz)  =  r(:,:,iz) + dt* drdt(:,:,iz)
      if (do_energy)  e(:,:,iz)  =  e(:,:,iz) + dt* dedt(:,:,iz)
      if (do_pscalar) d(:,:,iz)  =  d(:,:,iz) + dt* dddt(:,:,iz)
      if (do_momenta) then
                      px(:,:,iz) = px(:,:,iz) + dt*dpxdt(:,:,iz)
                      py(:,:,iz) = py(:,:,iz) + dt*dpydt(:,:,iz)
                      pz(:,:,iz) = pz(:,:,iz) + dt*dpzdt(:,:,iz)
      end if
      if (do_mhd) then
                      Bx(:,:,iz) = Bx(:,:,iz) + dt*dBxdt(:,:,iz)
                      By(:,:,iz) = By(:,:,iz) + dt*dBydt(:,:,iz)
                      Bz(:,:,iz) = Bz(:,:,iz) + dt*dBzdt(:,:,iz)
      end if
    end do
    td = td + dt
    dtold = dt

  else
    dtr = dt/dtold
    a1 = dtr**2
    a2 = 2.*(1.+dtr   )/(2.+3.*dtr)
    b1 = f*dt*(1.+dtr   )
    b2 = f*dt*(1.+dtr**2)/(2.+3.*dtr)
    c2 = f*dt*(1.+dtr   )/(2.+3.*dtr)

    do iz=izs,ize
      if (do_density) then
        do iy=1,my
        do ix=1,mx
          rsav(ix,iy,iz) =       r(ix,iy,iz)
          r   (ix,iy,iz) = a1*rold(ix,iy,iz) + (1.-a1)*r(ix,iy,iz) + b1*drdt(ix,iy,iz)
          rold(ix,iy,iz) =    rsav(ix,iy,iz)
          rsav(ix,iy,iz) = a2*rold(ix,iy,iz) + (1.-a2)*r(ix,iy,iz) + b2*drdt(ix,iy,iz)
          drdt(ix,iy,iz) = 0.
        end do
        end do
      end if
      if (do_energy) then
        do iy=1,my
        do ix=1,mx
          esav(ix,iy,iz) =       e(ix,iy,iz)
          e   (ix,iy,iz) = a1*eold(ix,iy,iz) + (1.-a1)*e(ix,iy,iz) + b1*dedt(ix,iy,iz)
          eold(ix,iy,iz) =    esav(ix,iy,iz)
          esav(ix,iy,iz) = a2*eold(ix,iy,iz) + (1.-a2)*e(ix,iy,iz) + b2*dedt(ix,iy,iz)
          dedt(ix,iy,iz) = 0.
        end do
        end do
      end if
      if (do_pscalar) then
        do iy=1,my
        do ix=1,mx
          dsav(ix,iy,iz) =       d(ix,iy,iz)
          d   (ix,iy,iz) = a1*dold(ix,iy,iz) + (1.-a1)*d(ix,iy,iz) + b1*dddt(ix,iy,iz)
          dold(ix,iy,iz) =    dsav(ix,iy,iz)
          dsav(ix,iy,iz) = a2*dold(ix,iy,iz) + (1.-a2)*d(ix,iy,iz) + b2*dddt(ix,iy,iz)
          dddt(ix,iy,iz) = 0.
        end do
        end do
      end if
      if (do_momenta) then
        do iy=1,my
        do ix=1,mx
          pxsav(ix,iy,iz) =       px(ix,iy,iz)
          px   (ix,iy,iz) = a1*pxold(ix,iy,iz) + (1.-a1)*px(ix,iy,iz) + b1*dpxdt(ix,iy,iz)
          pxold(ix,iy,iz) =    pxsav(ix,iy,iz)
          pxsav(ix,iy,iz) = a2*pxold(ix,iy,iz) + (1.-a2)*px(ix,iy,iz) + b2*dpxdt(ix,iy,iz)
          dpxdt(ix,iy,iz) = 0.

          pysav(ix,iy,iz) =       py(ix,iy,iz)
          py   (ix,iy,iz) = a1*pyold(ix,iy,iz) + (1.-a1)*py(ix,iy,iz) + b1*dpydt(ix,iy,iz)
          pyold(ix,iy,iz) =    pysav(ix,iy,iz)
          pysav(ix,iy,iz) = a2*pyold(ix,iy,iz) + (1.-a2)*py(ix,iy,iz) + b2*dpydt(ix,iy,iz)
          dpydt(ix,iy,iz) = 0.

          pzsav(ix,iy,iz) =       pz(ix,iy,iz)
          pz   (ix,iy,iz) = a1*pzold(ix,iy,iz) + (1.-a1)*pz(ix,iy,iz) + b1*dpzdt(ix,iy,iz)
          pzold(ix,iy,iz) =    pzsav(ix,iy,iz)
          pzsav(ix,iy,iz) = a2*pzold(ix,iy,iz) + (1.-a2)*pz(ix,iy,iz) + b2*dpzdt(ix,iy,iz)
          dpzdt(ix,iy,iz) = 0.
        end do
        end do
      end if
      if (do_mhd) then
        do iy=1,my
        do ix=1,mx
          Bxsav(ix,iy,iz) =       Bx(ix,iy,iz)
          Bx   (ix,iy,iz) = a1*Bxold(ix,iy,iz) + (1.-a1)*Bx(ix,iy,iz) + b1*dBxdt(ix,iy,iz)
          Bxold(ix,iy,iz) =    Bxsav(ix,iy,iz)
          Bxsav(ix,iy,iz) = a2*Bxold(ix,iy,iz) + (1.-a2)*Bx(ix,iy,iz) + b2*dBxdt(ix,iy,iz)
          dBxdt(ix,iy,iz) = 0.

          Bysav(ix,iy,iz) =       By(ix,iy,iz)
          By   (ix,iy,iz) = a1*Byold(ix,iy,iz) + (1.-a1)*By(ix,iy,iz) + b1*dBydt(ix,iy,iz)
          Byold(ix,iy,iz) =    Bysav(ix,iy,iz)
          Bysav(ix,iy,iz) = a2*Byold(ix,iy,iz) + (1.-a2)*By(ix,iy,iz) + b2*dBydt(ix,iy,iz)
          dBydt(ix,iy,iz) = 0.

          Bzsav(ix,iy,iz) =       Bz(ix,iy,iz)
          Bz   (ix,iy,iz) = a1*Bzold(ix,iy,iz) + (1.-a1)*Bz(ix,iy,iz) + b1*dBzdt(ix,iy,iz)
          Bzold(ix,iy,iz) =    Bzsav(ix,iy,iz)
          Bzsav(ix,iy,iz) = a2*Bzold(ix,iy,iz) + (1.-a2)*Bz(ix,iy,iz) + b2*dBzdt(ix,iy,iz)
          dBzdt(ix,iy,iz) = 0.
        end do
        end do
      end if
    end do

    !$omp single
    isubstep = 2
    !$omp end single
    call pde(.false.)

    do iz=izs,ize
      if (do_density) r(:,lb:ub,iz)  = rsav(:,lb:ub,iz) + c2*drdt(:,lb:ub,iz)
      if (do_energy)  e(:,lb:ub,iz)  = esav(:,lb:ub,iz) + c2*dedt(:,lb:ub,iz)
      if (do_pscalar) d(:,lb:ub,iz)  = dsav(:,lb:ub,iz) + c2*dddt(:,lb:ub,iz)
      if (do_momenta) then
                      px(:,lb:ub,iz) = pxsav(:,lb:ub,iz) + c2*dpxdt(:,lb:ub,iz)                      
                      py(:,lb:ub,iz) = pysav(:,lb:ub,iz) + c2*dpydt(:,lb:ub,iz)
                      pz(:,lb:ub,iz) = pzsav(:,lb:ub,iz) + c2*dpzdt(:,lb:ub,iz)
      end if
      if (do_mhd) then
                      Bx(:,  :  ,iz) = Bxsav(:,  :  ,iz) + c2*dBxdt(:,  :  ,iz)                      
                      By(:,  :  ,iz) = Bysav(:,  :  ,iz) + c2*dBydt(:,  :  ,iz)
                      Bz(:,  :  ,iz) = Bzsav(:,  :  ,iz) + c2*dBzdt(:,  :  ,iz)
      end if
    end do
  end if

  call dumpn(drdt ,'drdt ','dfdt.dmp',0)
  call dumpn(dpxdt,'dpxdt','dfdt.dmp',1)
  call dumpn(dpydt,'dpydt','dfdt.dmp',1)
  call dumpn(dpzdt,'dpzdt','dfdt.dmp',1)
  if (do_energy) then
    call dumpn(dedt ,'dedt ','dfdt.dmp',1)
  end if
  if (do_pscalar) then
    call dumpn(dddt ,'dddt ','dfdt.dmp',1)
  end if
  if (do_mhd) then
    call dumpn(dBxdt,'dBxdt','dfdt.dmp',1)
    call dumpn(dBydt,'dBydt','dfdt.dmp',1)
    call dumpn(dBzdt,'dBzdt','dfdt.dmp',1)
  end if
  !$omp end parallel

  td = td + f*dt
  t = td
  rt= td                                 ! rt is the "real time" computed also in substeps.
  dtold = dt
END SUBROUTINE
