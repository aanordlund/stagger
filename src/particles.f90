! $Id: particles.f90,v 1.21 2013/03/24 23:26:48 bob Exp $
!************************************************************************
MODULE particles
  USE params
  real, allocatable, dimension(:,:,:):: rx,ry,rz,drxdt,drydt,drzdt
  logical do_particles
END module

!************************************************************************
SUBROUTINE init_particles
  USE params
  USE particles
  implicit none
  integer ix,iy,iz
  character(len=mid):: id='$Id: particles.f90,v 1.21 2013/03/24 23:26:48 bob Exp $'

  if (id.ne.' ') print *,id; id=' '

  call read_part
  if (.not. do_particles) return

  allocate (rx(mx,my,mz),ry(mx,my,mz),rz(mx,my,mz))
  allocate (drxdt(mx,my,mz),drydt(mx,my,mz),drzdt(mx,my,mz))
!$omp parallel do private(iz,iy,ix)
  do iz=1,mz
  do iy=1,my
  do ix=1,mx
    rx(ix,iy,iz) = xm(ix)
    ry(ix,iy,iz) = ym(iy)
    rz(ix,iy,iz) = zm(iz)
    drxdt(ix,iy,iz) = 0.
    drydt(ix,iy,iz) = 0.
    drzdt(ix,iy,iz) = 0.
  end do
  end do
  end do
!$omp end parallel
END

!************************************************************************
SUBROUTINE read_part
  USE particles
  namelist /part/ do_particles
!
  do_particles = .false.
  if (do_trace) write (*,part)
  read (*,part)
  write (*,part)
END

!************************************************************************
SUBROUTINE read_particles (jsnap)
  USE params
  USE particles
  implicit none
  integer lrec,jsnap,i
  character(len=mfile) name, fname
  logical exists

  if (.not. do_particles) return
 
  fname = name('particles.dat','par',from)
  inquire (file=fname,exist=exists)
  if (exists) then
    i = index(fname,' ')-1
    print *,'reading particles:', jsnap,t,fname(1:i)
    open(2,file=fname,status='old', &
      access='direct', recl=3*lrec(mw))
    read (2,rec=1+jsnap) rx,ry,rz
    close (2)
  end if
END

!************************************************************************
SUBROUTINE write_particles (jsnap)
  USE params
  USE particles
  implicit none
  integer lrec,jsnap
  character(len=mfile) name

  if (.not. do_particles) return
  open(2,file=name('particles.dat','par',file),status='unknown', &
    access='direct', recl=3*lrec(mw))
  write (2,rec=1+jsnap) rx,ry,rz
  close (2)
  print '(a,i4,5x,a)','particles:', jsnap, name('particles.dat','par')
END

!************************************************************************
SUBROUTINE trace_particles (Ux, Uy, Uz)
!
!  3rd order, 2-N storage Runge-Kutta.  Advances the trace particles
!  forward in time, given velocities computed in the subroutine pde.
!
  USE params
  USE particles
  USE timeintegration

  implicit none
  integer ix,iy,iz,jx,jy,jz,jx1,jy1,jz1
  real, dimension(mx,my,mz) :: Ux,Uy,Uz
  real dtb,ax,ay,az,cx,cy,cz
  character(len=mid):: id='$Id: particles.f90,v 1.21 2013/03/24 23:26:48 bob Exp $'

  if (.not. do_particles) return
  if (id.ne.' ') print *,id; id=' '

  dtb = dt*beta(isubstep)
  cx = mx/sx
  cy = my/sy
  cz = mz/sz
  if (isubstep .eq. 1) then
!$omp parallel do private(iz,iy,ix,ax,ay,az,jx,jy,jz,jx1,jy1,jz1)
    do iz=1,mz
    do iy=1,my
    do ix=1,mx

      ax = (rx(ix,iy,iz)-xm(1))*cx
      ay = (ry(ix,iy,iz)-ym(1))*cy
      az = (rz(ix,iy,iz)-zm(1))*cz
      jx = ax
      jy = ay
      jz = az
      ax = ax-jx
      ay = ay-jy
      az = az-jz
      jx1 = mod(jx+100*mx,mx)+1
      jy1 = mod(jy+100*my,my)+1
      jz1 = mod(jz+100*mz,mz)+1
      jx = mod(jx-1+100*mx,mx)+1
      jy = mod(jy-1+100*my,my)+1
      jz = mod(jz-1+100*mz,mz)+1

      drxdt(ix,iy,iz) = (1.-az)*((1.-ay)*((1.-ax)*Ux(jx,jy ,jz )+ax*Ux(jx1,jy ,jz ))  &
                                   + ay *((1.-ax)*Ux(jx,jy1,jz )+ax*Ux(jx1,jy1,jz ))) &
                       +    az *((1.-ay)*((1.-ax)*Ux(jx,jy ,jz1)+ax*Ux(jx1,jy ,jz1))  &
                                   + ay *((1.-ax)*Ux(jx,jy1,jz1)+ax*Ux(jx1,jy1,jz1)))
      drydt(ix,iy,iz) = (1.-az)*((1.-ay)*((1.-ax)*Uy(jx,jy ,jz )+ax*Uy(jx1,jy ,jz ))  &
                                   + ay *((1.-ax)*Uy(jx,jy1,jz )+ax*Uy(jx1,jy1,jz ))) &
                       +    az *((1.-ay)*((1.-ax)*Uy(jx,jy ,jz1)+ax*Uy(jx1,jy ,jz1))  &
                                   + ay *((1.-ax)*Uy(jx,jy1,jz1)+ax*Uy(jx1,jy1,jz1)))
      drzdt(ix,iy,iz) = (1.-az)*((1.-ay)*((1.-ax)*Uz(jx,jy ,jz )+ax*Uz(jx1,jy ,jz ))  &
                                   + ay *((1.-ax)*Uz(jx,jy1,jz )+ax*Uz(jx1,jy1,jz ))) &
                       +    az *((1.-ay)*((1.-ax)*Uz(jx,jy ,jz1)+ax*Uz(jx1,jy ,jz1))  &
                                   + ay *((1.-ax)*Uz(jx,jy1,jz1)+ax*Uz(jx1,jy1,jz1)))
      rx(ix,iy,iz) = rx(ix,iy,iz) + dtb*drxdt(ix,iy,iz)
      ry(ix,iy,iz) = ry(ix,iy,iz) + dtb*drydt(ix,iy,iz)
      rz(ix,iy,iz) = rz(ix,iy,iz) + dtb*drzdt(ix,iy,iz)
    end do
    end do
    end do
  else
!$omp parallel do private(iz,iy,ix,ax,ay,az,jx,jy,jz,jx1,jy1,jz1)
    do iz=1,mz
    do iy=1,my
    do ix=1,mx
      ax = (rx(ix,iy,iz)-xm(1))*cx
      ay = (ry(ix,iy,iz)-ym(1))*cy
      az = (rz(ix,iy,iz)-zm(1))*cz
      jx = ax
      jy = ay
      jz = az
      ax = ax-jx
      ay = ay-jy
      az = az-jz
      jx1 = mod(jx+100*mx,mx)+1
      jy1 = mod(jy+100*my,my)+1
      jz1 = mod(jz+100*mz,mz)+1
      jx = mod(jx-1+100*mx,mx)+1
      jy = mod(jy-1+100*my,my)+1
      jz = mod(jz-1+100*mz,mz)+1
      drxdt(ix,iy,iz) = alpha(isubstep)*drxdt(ix,iy,iz) + &
                        (1.-az)*((1.-ay)*((1.-ax)*Ux(jx,jy ,jz )+ax*Ux(jx1,jy ,jz ))  &
                                   + ay *((1.-ax)*Ux(jx,jy1,jz )+ax*Ux(jx1,jy1,jz ))) &
                       +    az *((1.-ay)*((1.-ax)*Ux(jx,jy ,jz1)+ax*Ux(jx1,jy ,jz1))  &
                                   + ay *((1.-ax)*Ux(jx,jy1,jz1)+ax*Ux(jx1,jy1,jz1)))
      drydt(ix,iy,iz) = alpha(isubstep)*drydt(ix,iy,iz) + &
                        (1.-az)*((1.-ay)*((1.-ax)*Uy(jx,jy ,jz )+ax*Uy(jx1,jy ,jz ))  &
                                   + ay *((1.-ax)*Uy(jx,jy1,jz )+ax*Uy(jx1,jy1,jz ))) &
                       +    az *((1.-ay)*((1.-ax)*Uy(jx,jy ,jz1)+ax*Uy(jx1,jy ,jz1))  &
                                   + ay *((1.-ax)*Uy(jx,jy1,jz1)+ax*Uy(jx1,jy1,jz1)))
      drzdt(ix,iy,iz) = alpha(isubstep)*drzdt(ix,iy,iz) + &
                        (1.-az)*((1.-ay)*((1.-ax)*Uz(jx,jy ,jz )+ax*Uz(jx1,jy ,jz ))  &
                                   + ay *((1.-ax)*Uz(jx,jy1,jz )+ax*Uz(jx1,jy1,jz ))) &
                       +    az *((1.-ay)*((1.-ax)*Uz(jx,jy ,jz1)+ax*Uz(jx1,jy ,jz1))  &
                                   + ay *((1.-ax)*Uz(jx,jy1,jz1)+ax*Uz(jx1,jy1,jz1)))
      rx(ix,iy,iz) = rx(ix,iy,iz) + dtb*drxdt(ix,iy,iz)
      ry(ix,iy,iz) = ry(ix,iy,iz) + dtb*drydt(ix,iy,iz)
      rz(ix,iy,iz) = rz(ix,iy,iz) + dtb*drzdt(ix,iy,iz)
    end do
    end do
    end do
  end if

  END
