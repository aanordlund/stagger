! $Id: mhd_alloc.f90,v 1.1 2010/01/09 09:34:52 aake Exp $
MODULE mhd_mod
  real, allocatable, dimension(:,:,:) ::  &
		 eta, etd, fudge, &
                 Jx, Jy, Jz, Fx, Fy, Fz, &
                 Bx_y, Bx_z, By_x, By_z, Bz_x, Bz_y, &
                 Ux_y, Ux_z, Uy_x, Uy_z, Uz_x, Uz_y
END MODULE

!***********************************************************************
SUBROUTINE mhd (flag, &
            r,e,pg,Ux,Uy,Uz,Bx,By,Bz,Ex,Ey,Ez, &
            dpxdt,dpydt,dpzdt,dedt,dBxdt,dBydt,dBzdt,d2lnr)
!-----------------------------------------------------------------------
!  To omptimize for speed, pointer assignments are done in this first
!  wrapper routines, which then passes all arrays on in argument lists.
!-----------------------------------------------------------------------
  USE params
  USE arrays, ONLY: &
            scr1,scr2,scr3,scr4,scr5,scr6
  USE mhd_mod
  implicit none
  logical flag
  real, dimension(mx,my,mz):: &
            r,e,pg,Ux,Uy,Uz,Bx,By,Bz,Ex,Ey,Ez, &
            dpxdt,dpydt,dpzdt,dedt,dBxdt,dBydt,dBzdt,d2lnr
!-----------------------------------------------------------------------
  
  allocate (Jx(mx,my,mz), Jy(mx,my,mz), Jz(mx,my,mz))
  allocate (Fx(mx,my,mz), Fy(mx,my,mz), Fz(mx,my,mz))
  allocate (Bx_y(mx,my,mz), Bx_z(mx,my,mz))
  allocate (By_z(mx,my,mz), By_x(mx,my,mz))
  allocate (Bz_x(mx,my,mz), Bz_y(mx,my,mz))
  allocate (Ux_y(mx,my,mz), Ux_z(mx,my,mz))
  allocate (Uy_z(mx,my,mz), Uy_x(mx,my,mz))
  allocate (Uz_x(mx,my,mz), Uz_y(mx,my,mz))
  allocate (eta(mx,my,mz), fudge(mx,my,mz))
  allocate (etd(mx,my,mz))

  call mhd1     (flag, mx, my, mz, &
                 r, e, pg, d2lnr, &
                 Jx, Jy, Jz, Ex, Ey, Ez, Fx, Fy, Fz, &
                 Bx_y, Bx_z, By_x, By_z, Bz_x, Bz_y, &
                 Ux_y, Ux_z, Uy_x, Uy_z, Uz_x, Uz_y, &
                 Ux, Uy, Uz, Bx, By, Bz, fudge, eta, etd, &
                 dpxdt, dpydt, dpzdt, dBxdt, dBydt, dBzdt, &
                 dedt, scr1, scr2, scr3, scr4, scr5, scr6)
  
  deallocate (Jx, Jy, Jz)
  deallocate (Fx, Fy, Fz)
  deallocate (Bx_y, Bx_z)
  deallocate (By_z, By_x)
  deallocate (Bz_x, Bz_y)
  deallocate (Ux_y, Ux_z)
  deallocate (Uy_z, Uy_x)
  deallocate (Uz_x, Uz_y)
  deallocate (eta, fudge)
  deallocate (etd)
END

!***********************************************************************
SUBROUTINE mhd1 (flag, mx, my, mz, &
                 r, e, pg, d2lnr, &
                 Jx, Jy, Jz, Ex, Ey, Ez, Fx, Fy, Fz, &
                 Bx_y, Bx_z, By_x, By_z, Bz_x, Bz_y, &
                 Ux_y, Ux_z, Uy_x, Uy_z, Uz_x, Uz_y, &
                 Ux, Uy, Uz, Bx, By, Bz, fudge, eta, etd, &
                 dpxdt, dpydt, dpzdt, dBxdt, dBydt, dBzdt, &
                 dedt, scr1, scr2, scr3, scr4, scr5, scr6)
  USE params, mx_void=>mx, my_void=>my, mz_void=>mz
  implicit none
  logical flag
  integer mx, my, mz
  real, dimension(mx,my,mz) ::  &
                 r, e, pg, d2lnr, &
                 Jx, Jy, Jz, Ex, Ey, Ez, Fx, Fy, Fz, &
                 Bx_y, Bx_z, By_x, By_z, Bz_x, Bz_y, &
                 Ux_y, Ux_z, Uy_x, Uy_z, Uz_x, Uz_y, &
                 Ux, Uy, Uz, Bx, By, Bz, fudge, eta, etd, &
                 dpxdt, dpydt, dpzdt, dBxdt, dBydt, dBzdt, &
                 dedt, scr1, scr2, scr3, scr4, scr5, scr6
  integer ix, iy, iz, j
  logical debug
  integer lb1, ub1
  real ds, c, Qsum, Ua_x, Ua_y, Ua_z, x_i
  real(kind=8) sumy
  integer, save:: nprint_ambi=10
  character(len=mid):: id='$Id: mhd_alloc.f90,v 1.1 2010/01/09 09:34:52 aake Exp $'
!-----------------------------------------------------------------------
  call print_id (id)

  call mfield_boundary (Ux,Uy,Uz,Bx,By,Bz)
                                                                        call timer('mhd','mf_bdry')
  call resist (flag, &                                                  ! uses wk06-wk15 internally
    eta,r,e,pg,fudge,Ux,Uy,Uz,Bx,By,Bz,d2lnr)
    call dumpn(eta,'eta','mhd.dmp',0)
                                                                        call timer('mhd','begin')
!-----------------------------------------------------------------------
!  Electric current I = curl(B), resistive E, and dissipation
!-----------------------------------------------------------------------

  call barrier_omp('mhd1')                                              ! barrier 1; By may have been modified
    call dumpn(Bx,'Bx','mhd.dmp',1)
    call dumpn(By,'By','mhd.dmp',1)
    call dumpn(Bz,'Bz','mhd.dmp',1)

  call ddydn_set (Bz,Jx) ; call ddzdn_sub (By,Jx)
  call ddzdn_set (Bx,Jy) ; call ddxdn_sub (Bz,Jy)
  call ddxdn_set (By,Jz) ; call ddydn_sub (Bx,Jz)
    call dumpn(Jx,'Jx','mhd.dmp',1)
    call dumpn(Jy,'Jy','mhd.dmp',1)
    call dumpn(Jz,'Jz','mhd.dmp',1)

!-----------------------------------------------------------------------
!  Alfven speed limiter / fudge factor
!-----------------------------------------------------------------------
  if (cmax .ne. 0) then
    do iz=izs,ize
      Jx(:,:,iz) = Jx(:,:,iz)*fudge(:,:,iz)
      Jy(:,:,iz) = Jy(:,:,iz)*fudge(:,:,iz)
      Jz(:,:,iz) = Jz(:,:,iz)*fudge(:,:,iz)
    end do
  end if
                                                                        call timer('mhd','current')
!-----------------------------------------------------------------------
!  Resistive part of Ex
!-----------------------------------------------------------------------
  call barrier_omp('mhd2')                                              ! barrier 2;  NOT NEEDED?
  call zdn1_set (eta, scr1) ; call ydn1_set (scr1, scr2)
  call zdn1_set (etd, scr3) ; call ydn1_set (scr3, scr4)
  if (do_quench) then
    if (do_aniso_eta) then
      call ddydn_set (Bz,scr5)
      call quenchy (scr5, scr3)
      do iz=izs,ize; do iy=1,my; do ix=1,mx
	ds = dym(iy)
	Ex(ix,iy,iz) = ds*(scr2(ix,iy,iz)+ds*scr4(ix,iy,iz))*scr3(ix,iy,iz)
      end do; end do; end do
      call ddzdn_set (By,scr5)
      call barrier_omp('mhd3')                                            ! barrier 3; Jx just computed
      call quenchz (scr5, scr3, scr1)
      do iz=izs,ize; do iy=1,my; do ix=1,mx
	ds = dzm(iz)
	Ex(ix,iy,iz) = Ex(ix,iy,iz) - ds*(scr2(ix,iy,iz)+ds*scr4(ix,iy,iz))*scr3(ix,iy,iz)
      end do; end do; end do
    else
      do iz=izs,ize; do iy=1,my; do ix=1,mx
	ds = max(dym(iy),dzm(iz))
	Ex(ix,iy,iz) = ds*(scr2(ix,iy,iz)+ds*scr4(ix,iy,iz))
      end do; end do; end do
      call barrier_omp('mhd3')                                            ! barrier 3; Jx just computed
      call quenchyz (Jx, scr3, scr4)
      do iz=izs,ize; do iy=1,my; do ix=1,mx
	ds = max(dym(iy),dzm(iz))
	Ex(ix,iy,iz) = Ex(ix,iy,iz)*scr3(ix,iy,iz)
      end do; end do; end do
    end if

  else if (do_aniso_eta) then
    call ddydn_set (Bz,scr5)
    do iz=izs,ize; do iy=1,my; do ix=1,mx
      ds = dym(iy)
      Ex(ix,iy,iz) = ds*(scr2(ix,iy,iz)+ds*scr4(ix,iy,iz))*scr5(ix,iy,iz)
    end do; end do; end do
    call ddzdn_set (By,scr5)
    do iz=izs,ize; do iy=1,my; do ix=1,mx
      ds = dzm(iz)
      Ex(ix,iy,iz) = Ex(ix,iy,iz) - ds*(scr2(ix,iy,iz)+ds*scr4(ix,iy,iz))*scr5(ix,iy,iz)
    end do; end do; end do

  else
    do iz=izs,ize; do iy=1,my; do ix=1,mx
      ds = 0.5*(dym(iy)+dzm(iz))
      Ex(ix,iy,iz) = ds*(scr2(ix,iy,iz)+ds*scr4(ix,iy,iz))*Jx(ix,iy,iz)
    end do; end do; end do
  end if

!-----------------------------------------------------------------------
!  Resistive part of Ey
!-----------------------------------------------------------------------
  if (do_quench) then
    if (do_aniso_eta) then
      call zdn1_set (eta, scr1) ; call xdn1_set (scr1, scr2)
      call zdn1_set (etd, scr3) ; call xdn1_set (scr3, scr4)
      call ddzdn_set (Bx,scr5)
      call quenchz (scr5, scr3, scr1)
      do iz=izs,ize; do iy=1,my; do ix=1,mx
	ds = dzm(iz)
	Ey(ix,iy,iz) = ds*(scr2(ix,iy,iz)+ds*scr4(ix,iy,iz))*scr3(ix,iy,iz)
      end do; end do; end do
      call barrier_omp('mhd4')                                            ! barrier 4; scr3 used above (NOT NEEDED?)
      call ddxdn_set (Bz,scr5)
      call quenchx (scr5, scr3)
      do iz=izs,ize; do iy=1,my; do ix=1,mx
	ds = dxm(ix)
	Ey(ix,iy,iz) = Ey(ix,iy,iz) - ds*(scr2(ix,iy,iz)+ds*scr4(ix,iy,iz))*scr3(ix,iy,iz)
      end do; end do; end do
    else
      call xdn1_set (scr1, scr2)
      call xdn1_set (scr3, scr4)
      do iz=izs,ize; do iy=1,my; do ix=1,mx
	ds = max(dzm(iz),dxm(ix))
	Ey(ix,iy,iz) = ds*(scr2(ix,iy,iz)+ds*scr4(ix,iy,iz))
      end do; end do; end do
      call barrier_omp('mhd4')                                            ! barrier 4; scr3 used above (NOT NEEDED?)
      call quenchzx (Jy, scr3, scr4)
      do iz=izs,ize; do iy=1,my; do ix=1,mx
	Ey(ix,iy,iz) = Ey(ix,iy,iz)*scr3(ix,iy,iz)
      end do; end do; end do
    end if

  else if (do_aniso_eta) then
    call xdn1_set (scr1, scr2)
    call xdn1_set (scr3, scr4)
    call ddzdn_set (Bx,scr5)
    do iz=izs,ize; do iy=1,my; do ix=1,mx
      ds = dzm(iz)
      Ey(ix,iy,iz) = ds*(scr2(ix,iy,iz)+ds*scr4(ix,iy,iz))*scr5(ix,iy,iz)
    end do; end do; end do
    call ddxdn_set (Bz,scr5)
    do iz=izs,ize; do iy=1,my; do ix=1,mx
      ds = dxm(ix)
      Ey(ix,iy,iz) = Ey(ix,iy,iz) - ds*(scr2(ix,iy,iz)+ds*scr4(ix,iy,iz))*scr5(ix,iy,iz)
    end do; end do; end do

  else
    call xdn1_set (scr1, scr2)
    call xdn1_set (scr3, scr4)
    do iz=izs,ize; do iy=1,my; do ix=1,mx
      ds = 0.5*(dzm(iz)+dxm(ix))
      Ey(ix,iy,iz) = ds*(scr2(ix,iy,iz)+ds*scr4(ix,iy,iz))*Jy(ix,iy,iz)
    end do; end do; end do
  end if

!-----------------------------------------------------------------------
!  Resistive part of Ez
!-----------------------------------------------------------------------
  call ydn1_set (eta, scr1) ; call xdn1_set (scr1, scr2)
  call ydn1_set (etd, scr3) ; call xdn1_set (scr3, scr4)
  if (do_quench) then
    if (do_aniso_eta) then
      call ddxdn_set (By,scr5)
      call quenchx (scr5, scr3)
      do iz=izs,ize; do iy=1,my; do ix=1,mx
	ds = dxm(ix)
	Ez(ix,iy,iz) = ds*(scr2(ix,iy,iz)+ds*scr4(ix,iy,iz))*scr3(ix,iy,iz)
      end do; end do; end do
      call ddydn_set (Bx,scr5)
      call quenchy (scr5, scr3)
      do iz=izs,ize; do iy=1,my; do ix=1,mx
	ds = dym(iy)
	Ez(ix,iy,iz) = Ez(ix,iy,iz) - ds*(scr2(ix,iy,iz)+ds*scr4(ix,iy,iz))*scr3(ix,iy,iz)
      end do; end do; end do
    else
      do iz=izs,ize; do iy=1,my; do ix=1,mx
	ds = max(dxm(ix),dym(iy))
	Ez(ix,iy,iz) = ds*(scr2(ix,iy,iz)+ds*scr4(ix,iy,iz))
      end do; end do; end do
      call quenchxy (Jz, scr3)
      do iz=izs,ize; do iy=1,my; do ix=1,mx
	Ez(ix,iy,iz) = Ez(ix,iy,iz)*scr3(ix,iy,iz)
      end do; end do; end do
    end if

  else if (do_aniso_eta) then
    call ddxdn_set (By,scr5)
    do iz=izs,ize; do iy=1,my; do ix=1,mx
      ds = dxm(ix)
      Ez(ix,iy,iz) = ds*(scr2(ix,iy,iz)+ds*scr4(ix,iy,iz))*scr5(ix,iy,iz)
    end do; end do; end do
    call ddydn_set (Bx,scr5)
    do iz=izs,ize; do iy=1,my; do ix=1,mx
      ds = dym(iy)
      Ez(ix,iy,iz) = Ez(ix,iy,iz) - ds*(scr2(ix,iy,iz)+ds*scr4(ix,iy,iz))*scr5(ix,iy,iz)
    end do; end do; end do

  else
    do iz=izs,ize; do iy=1,my; do ix=1,mx
      ds = 0.5*(dxm(ix)+dym(iy))
      Ez(ix,iy,iz) = ds*(scr2(ix,iy,iz)+ds*scr4(ix,iy,iz))*Jz(ix,iy,iz)
    end do; end do; end do
  end if

    call dumpn(Ex,'Ex','mhd.dmp',1)
    call dumpn(Ey,'Ey','mhd.dmp',1)
    call dumpn(Ez,'Ez','mhd.dmp',1)
  call ecurrent_boundary (Ex, Ey, Ez, Jx, Jy, Jz)
                                                                        call timer('mhd','E_resist')
!-----------------------------------------------------------------------
!  Joule dissipation
!-----------------------------------------------------------------------
  if (do_energy .and. do_dissipation) then
    do iz=izs,ize
      scr3(:,:,iz) = Jx(:,:,iz)*Ex(:,:,iz)
    end do
    call barrier_omp('mhd5')                                            ! barrier 5; scr3 just computed
    call zup1_set (scr3, scr1) ; call yup1_set (scr1 , scr2)

    lb1 = lb
    ub1 = ub
    if (isubstep == 1) then
      c = 1./(mx*(ub1-lb1+1))
      do iz=izs,ize
        sumy = 0.
        do iy=lb1,ub1
          sumy = sumy + sum(dble(scr2(:,iy,iz)))
        end do
        sumxy(iz) = sumy*c
      end do
      call dumpn(scr2,'Qj1','mhd.dmp',1)
    end if

    do iz=izs,ize
      dedt(:,lb1:ub1,iz) = dedt(:,lb1:ub1,iz) + scr2(:,lb1:ub1,iz)
      scr2(:,:,iz) = Jy(:,:,iz)*Ey(:,:,iz)
    end do
    call xup1_set (scr2, scr1)
    call barrier_omp('mhd6')                                            ! barrier 6; make sure scr3 is not changed
    call zup1_set (scr1, scr3)

    if (isubstep == 1) then
      do iz=izs,ize
        sumy = 0.
        do iy=lb1,ub1
          sumy = sumy + sum(dble(scr3(:,iy,iz)))
        end do
        sumxy(iz) = sumxy(iz) + sumy*c
      end do
      call dumpn(scr3,'Qj2','mhd.dmp',1)
    end if

    do iz=izs,ize
      dedt(:,lb1:ub1,iz) = dedt(:,lb1:ub1,iz) + scr3(:,lb1:ub1,iz)
      scr3(:,:,iz) = Jz(:,:,iz)*Ez(:,:,iz)
    end do
    call barrier_omp('mhd6a')                                           ! barrier 6a; make sure scr3 is not changed
    call yup1_set (scr3, scr1) ; call xup1_set (scr1 , scr2)
    do iz=izs,ize
      dedt(:,lb1:ub1,iz) = dedt(:,lb1:ub1,iz) + scr2(:,lb1:ub1,iz)
    end do

    if (isubstep == 1) then
      do iz=izs,ize
        sumy = 0.
        do iy=lb1,ub1
          sumy = sumy + sum(dble(scr2(:,iy,iz)))
        end do
        sumxy(iz) = sumxy(iz) + sumy*c
      end do
      call dumpn(scr3,'Qj3','mhd.dmp',1)
      !$omp barrier
      !$omp master
      Qsum = sum(sumxy)/mz
      call sum_mpi_real (Qsum, Q_mag, 1)
      Q_mag = Q_mag/(mpi_nx*mpi_ny*mpi_nz)
      !$omp end master

    end if
  end if
    call dumpn(Ex,'Ex1','mhd.dmp',1)
    call dumpn(Ey,'Ey1','mhd.dmp',1)
    call dumpn(Ez,'Ez1','mhd.dmp',1)
                                                                        call timer('mhd','Q_joule')
!-----------------------------------------------------------------------
!  Lorenz force = I x B
!-----------------------------------------------------------------------
  if (do_2nddiv) then
    call ydn1_set (Bx,Bx_y) ; call zdn1_set (Bx,Bx_z)
    call zdn1_set (By,By_z) ; call xdn1_set (By,By_x)
    call xdn1_set (Bz,Bz_x) ; call ydn1_set (Bz,Bz_y)
  else
    call ydn_set (Bx,Bx_y) ; call zdn_set (Bx,Bx_z)
    call zdn_set (By,By_z) ; call xdn_set (By,By_x)
    call xdn_set (Bz,Bz_x) ; call ydn_set (Bz,Bz_y)
  end if
  do iz=izs,ize
    scr1(:,:,iz) = Jy(:,:,iz)*Bz_x(:,:,iz)
  end do
  call barrier_omp('mhd7')                                              ! barrier 7; scr1 just computed
  if (do_2nddiv) then
    call zup1_set (scr1, Fx)
  else
    call zup_set (scr1, Fx)
  end if
  do iz=izs,ize
    scr2(:,:,iz) = Jz(:,:,iz)*By_x(:,:,iz)
  end do
  if (do_2nddiv) then
    call yup1_sub (scr2, Fx)
  else
    call yup_sub (scr2, Fx)
  end if
  do iz=izs,ize
    scr2(:,:,iz) = Jz(:,:,iz)*Bx_y(:,:,iz)
  end do
  if (do_2nddiv) then
    call xup1_set (scr2, Fy)
  else
    call xup_set (scr2, Fy)
  end if
  do iz=izs,ize
    scr3(:,:,iz) = Jx(:,:,iz)*Bz_y(:,:,iz)
  end do
  call barrier_omp('mhd8')                                              ! barrier 8; scr3 just computed
  if (do_2nddiv) then
    call zup1_sub (scr3, Fy)
  else
    call zup_sub (scr3, Fy)
  end if
  do iz=izs,ize
    scr1(:,:,iz) = Jx(:,:,iz)*By_z(:,:,iz)
  end do
  if (do_2nddiv) then
    call yup1_set (scr1, Fz)
  else
    call yup_set (scr1, Fz)
  end if
  do iz=izs,ize
    scr1(:,:,iz) = Jy(:,:,iz)*Bx_z(:,:,iz)
  end do
  if (do_2nddiv) then
    call xup1_sub (scr1, Fz)
  else
    call xup_sub (scr1, Fz)
  end if
  do iz=izs,ize
    dpxdt(:,:,iz) = dpxdt(:,:,iz) + Fx(:,:,iz)
    dpydt(:,:,iz) = dpydt(:,:,iz) + Fy(:,:,iz)
    dpzdt(:,:,iz) = dpzdt(:,:,iz) + Fz(:,:,iz)
  end do
  call barrier_omp('mhd9')                                              ! barrier 9; NOT NEEDED??

                                                                        call timer('mhd','Lorentz')
!-----------------------------------------------------------------------
!  Ambipolar diffusion
!-----------------------------------------------------------------------
  if (ambi .ne. 0.) then
    do iz=izs,ize
      do iy=1,my
      do ix=1,mx
        scratch(ix,iy,iz) = max(Ux(ix,iy,iz),Uy(ix,iy,iz),Uz(ix,iy,iz)) ! max absolute velocity
      end do
      end do
    end do
    call fmaxval_subr ('Umax',scratch,Umax)                             ! MPI-safe global max
    if (ambi > 0) then
      do iz=izs,ize
        do iy=1,my
        do ix=1,mx
          ds = max(sx,sy,sz)
          c  = ambi*ds/(r(ix,iy,iz)*sqrt(r(ix,iy,iz)))
          Fx(ix,iy,iz) = Fx(ix,iy,iz)*c
          Fy(ix,iy,iz) = Fy(ix,iy,iz)*c
          Fz(ix,iy,iz) = Fz(ix,iy,iz)*c
          Fx(ix,iy,iz) = Fx(ix,iy,iz)*Umax/(Umax+abs(Fx(ix,iy,iz)))
          Fy(ix,iy,iz) = Fy(ix,iy,iz)*Umax/(Umax+abs(Fy(ix,iy,iz)))
          Fz(ix,iy,iz) = Fz(ix,iy,iz)*Umax/(Umax+abs(Fz(ix,iy,iz)))
        end do
        end do
      end do
    else
      do iz=izs,ize
        do iy=1,my
        do ix=1,mx
          x_i = 1.43/r(ix,iy,iz)*exp(-1.5*r(ix,iy,iz)**0.15)
          if (r(ix,iy,iz) < 0.39) x_i = 1.
          ds = max(sx,sy,sz)
          c  = abs(ambi)*ds/(x_i*r(ix,iy,iz)**2)
          Fx(ix,iy,iz) = Fx(ix,iy,iz)*c
          Fy(ix,iy,iz) = Fy(ix,iy,iz)*c
          Fz(ix,iy,iz) = Fz(ix,iy,iz)*c
          Fx(ix,iy,iz) = Fx(ix,iy,iz)*Umax/(Umax+abs(Fx(ix,iy,iz)))
          Fy(ix,iy,iz) = Fy(ix,iy,iz)*Umax/(Umax+abs(Fy(ix,iy,iz)))
          Fz(ix,iy,iz) = Fz(ix,iy,iz)*Umax/(Umax+abs(Fz(ix,iy,iz)))
        end do
        end do
      end do
    end if
    call dumps(Fx,'vADx','ambi.dat')
    call dumps(Fy,'vADy','ambi.dat')
    call dumps(Fz,'vADz','ambi.dat')
    do iz=izs,ize
      do iy=1,my
      do ix=1,mx
        Fx(ix,iy,iz) = Ux(ix,iy,iz) + Fx(ix,iy,iz)
        Fy(ix,iy,iz) = Uy(ix,iy,iz) + Fy(ix,iy,iz)
        Fz(ix,iy,iz) = Uz(ix,iy,iz) + Fz(ix,iy,iz)
      end do
      end do
    end do

    call barrier_omp('ambi1')
    do iz=izs,ize
      scratch(:,:,iz) = abs(Fx(:,:,iz) - Ux(:,:,iz))
    end do
    call fmaxval_subr ('Uambi_x',scratch,Umax)                             ! MPI-safe global max
    if (master) Ua_x = Umax
    do iz=izs,ize
      scratch(:,:,iz) = abs(Fz(:,:,iz) - Uz(:,:,iz))
    end do
    call fmaxval_subr ('Uambi_y',scratch,Umax)                             ! MPI-safe global max
    if (master) Ua_y = Umax
    do iz=izs,ize
      scratch(:,:,iz) = abs(Fz(:,:,iz) - Uz(:,:,iz))
    end do
    call fmaxval_subr ('Uambi_z',scratch,Umax)                             ! MPI-safe global max
    if (master.and.mod(it,nprint_ambi)==0.and.isubstep==1) &
      print*,'max(U_ambi):',Ua_x,Ua_y,Umax

    call barrier_omp('ambi2')
    call zdn_set (Fy,Uy_z) ; call ydn_set (Fz,Uz_y)
    call xdn_set (Fz,Uz_x) ; call zdn_set (Fx,Ux_z)
    call ydn_set (Fx,Ux_y) ; call xdn_set (Fy,Uy_x)
      call dumpn(Fx,'Ux_ambi','mhd.dmp',1)
      call dumpn(Fy,'Uy_ambi','mhd.dmp',1)
      call dumpn(Fz,'Uz_ambi','mhd.dmp',1)
  else
    call zdn_set (Uy,Uy_z) ; call ydn_set (Uz,Uz_y)
    call xdn_set (Uz,Uz_x) ; call zdn_set (Ux,Ux_z)
    call ydn_set (Ux,Ux_y) ; call xdn_set (Uy,Uy_x)
  end if
    call dumpn(Ux,'Ux','mhd.dmp',1)
    call dumpn(Uy,'Uy','mhd.dmp',1)
    call dumpn(Uz,'Uz','mhd.dmp',1)

!-----------------------------------------------------------------------
!  Advective part of the electric field;  E -> E - UxB
!-----------------------------------------------------------------------
  do iz=izs,ize
    Ex(:,:,iz) = Ex(:,:,iz) - Uy_z(:,:,iz)*Bz_y(:,:,iz) + Uz_y(:,:,iz)*By_z(:,:,iz)
    Ey(:,:,iz) = Ey(:,:,iz) - Uz_x(:,:,iz)*Bx_z(:,:,iz) + Ux_z(:,:,iz)*Bz_x(:,:,iz)
    Ez(:,:,iz) = Ez(:,:,iz) - Ux_y(:,:,iz)*By_x(:,:,iz) + Uy_x(:,:,iz)*Bx_y(:,:,iz)
  end do
    call dumpn(Ex,'Ex2','mhd.dmp',1)
    call dumpn(Ey,'Ey2','mhd.dmp',1)
    call dumpn(Ez,'Ez2','mhd.dmp',1)
                                                                        call timer('mhd','E_advect')
!-----------------------------------------------------------------------
!  E-field boundary conditions
!-----------------------------------------------------------------------
  call efield_boundary (Ex, Ey, Ez, Bx_y, Bx_z, By_z, By_x, Bz_x, Bz_y, &
                        Bx, By, Bz, Ux_y, Ux_z, Uy_z, Uy_x, Uz_x, Uz_y, &
                        Ux, Uy, Uz)
                                                                        call timer('mhd','ef_bdry')
!-----------------------------------------------------------------------
!  Magnetic field's time derivative, dBdt = - curl(E)
!-----------------------------------------------------------------------
  call barrier_omp('mhd10')                                             ! barrier 10; Ex, Ey just computed
    call dumpn(Ex,'Ex3','mhd.dmp',1)
    call dumpn(Ey,'Ey3','mhd.dmp',1)
    call dumpn(Ez,'Ez3','mhd.dmp',1)
  call ddzup_add (Ey, dBxdt) ; call ddyup_sub (Ez, dBxdt)
  call ddxup_add (Ez, dBydt) ; call ddzup_sub (Ex, dBydt)
  call ddyup_add (Ex, dBzdt) ; call ddxup_sub (Ey, dBzdt)
  call Poynting_flux (Ex,Ey,Ez,Bx,By,Bz,scr1,scr2,scr3,scr4,scr5,scr6)
                                                                        call timer('mhd','induct')
!-----------------------------------------------------------------------
!  Fictive magnetic field growth, e.g. to slowly increase AR fields
!-----------------------------------------------------------------------
  if (t_Bgrowth.ne.0.) then
    do iz=izs,ize
      dBxdt(:,:,iz) = dBxdt(:,:,iz) + (1./t_Bgrowth)*Bx(:,:,iz)
      dBydt(:,:,iz) = dBydt(:,:,iz) + (1./t_Bgrowth)*By(:,:,iz)
      dBzdt(:,:,iz) = dBzdt(:,:,iz) + (1./t_Bgrowth)*Bz(:,:,iz)
    end do
                                                                        call timer('mhd','B-growth')
  end if
    call dumpn(dBxdt,'dBxdt','mhd.dmp',1)
    call dumpn(dBydt,'dBydt','mhd.dmp',1)
    call dumpn(dBzdt,'dBzdt','mhd.dmp',1)

END
