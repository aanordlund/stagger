! $Id: stagger_do.f90,v 1.3 2001/12/05 21:54:00 aake Exp &
MODULE stagger
!***********************************************************************
!
!  6th order staggered derivatives, 5th order interpolation. This version
!  uses explicit loops, for increased speed and parallelization.
!
!***********************************************************************
integer nflop, nstag
CONTAINS
FUNCTION xup (f)
!
!  f is centered on (i-.5,j,k), fifth order stagger
!
  use params
  real, dimension(mx,my,mz):: f, xup
!hpf$ distribute(*,*,block):: f, xup


  c = 3./256.
  b = -25./256.
  a = .5-b-c
  a1st = 0.5

!$omp parallel do private(i,j,k)
  do k=1,mz
   do j=1,my
    xup(mx-2,j,k) = ( &
                   a*(f(mx-2,j,k)+f(mx-1,j,k)) &
                 + b*(f(mx-3,j,k)+f(mx  ,j,k)) &
                 + c*(f(mx-4,j,k)+f(1   ,j,k)))
    xup(mx-1,j,k) = ( &
                   a*(f(mx-1,j,k)+f(mx  ,j,k)) &
                 + b*(f(mx-2,j,k)+f(1   ,j,k)) &
                 + c*(f(mx-3,j,k)+f(2   ,j,k)))
    xup(mx  ,j,k) = ( &
                   a*(f(mx  ,j,k)+f(1   ,j,k)) &
                 + b*(f(mx-1,j,k)+f(2   ,j,k)) &
                 + c*(f(mx-2,j,k)+f(3   ,j,k)))
    xup(1   ,j,k) = ( &
                   a*(f(1   ,j,k)+f(2   ,j,k)) &
                 + b*(f(mx  ,j,k)+f(3   ,j,k)) &
                 + c*(f(mx-1,j,k)+f(4   ,j,k)))
    xup(2   ,j,k) = ( &
                   a*(f(2   ,j,k)+f(3   ,j,k)) &
                 + b*(f(1   ,j,k)+f(4   ,j,k)) &
                 + c*(f(mx  ,j,k)+f(5   ,j,k)))
   end do
   do j=1,my
    do i=3,mx-3
      xup(i  ,j,k) = ( &
                    a*(f(i  ,j,k)+f(i+1 ,j,k)) &
                  + b*(f(i-1,j,k)+f(i+2 ,j,k)) &
                  + c*(f(i-2,j,k)+f(i+3 ,j,k)))
    end do
   end do
  end do

!
  nstag = nstag+1
  nflop = nflop+8
END FUNCTION

!***********************************************************************
FUNCTION yup (f)
!
!  f is centered on (i,j-.5,k), fifth order stagger
!
  use params
  real, intent(in) , dimension(mx,my,mz):: f
  real,              dimension(mx,my,mz):: yup
!hpf$ distribute(*,*,block):: f, yup
!-----------------------------------------------------------------------
  if (my .lt. 3) then
!$omp parallel do private(k)
    do k=1,mz
      yup(:,:,k) = f(:,:,k)
    end do
    return
  end if
  c = 3./256.
  b = -25./256.
  a = .5-b-c
  a1st = 0.5
!
!$omp parallel do private(i,j,k)
  do k=1,mz
    do i=1,mx
      yup(i,my-2,k) = ( &
                     a*(f(i,my-2,k)+f(i,my-1,k)) + &
                     b*(f(i,my-3,k)+f(i,my  ,k)) + &
                     c*(f(i,my-4,k)+f(i,1   ,k)))
      yup(i,my-1,k) = ( &
                     a*(f(i,my-1,k)+f(i,my  ,k)) + &
                     b*(f(i,my-2,k)+f(i,1   ,k)) + &
                     c*(f(i,my-3,k)+f(i,2   ,k)))
      yup(i,my  ,k) = ( &
                     a*(f(i,my  ,k)+f(i,1   ,k)) + &
                     b*(f(i,my-1,k)+f(i,2   ,k)) + &
                     c*(f(i,my-2,k)+f(i,3   ,k)))
      yup(i,1   ,k) = ( &
                     a*(f(i,1   ,k)+f(i,2   ,k)) + &
                     b*(f(i,my  ,k)+f(i,3   ,k)) + &
                     c*(f(i,my-1,k)+f(i,4   ,k)))
      yup(i,2   ,k) = ( &
                     a*(f(i,2   ,k)+f(i,3   ,k)) + &
                     b*(f(i,1   ,k)+f(i,4   ,k)) + &
                     c*(f(i,my  ,k)+f(i,5   ,k)))
    end do
    do j=3,my-3
     do i=1,mx
      yup(i,j  ,k) = ( &
                     a*(f(i,j   ,k)+f(i,j+1 ,k)) + &
                     b*(f(i,j-1 ,k)+f(i,j+2 ,k)) + &
                     c*(f(i,j-2 ,k)+f(i,j+3 ,k)))
     end do
    end do
  end do
!
  nstag = nstag+1
  nflop = nflop+8
END FUNCTION

!***********************************************************************
FUNCTION zup (f)
!
!  f is centered on (i,j,k-.5), fifth order stagger
!
  use params
  real, dimension(mx,my,mz):: f, zup
!hpf$ distribute(*,*,block):: f, zup
!
!-----------------------------------------------------------------------
  c = 3./256.
  b = -25./256.
  a = .5-b-c
  a1st = 0.5
!
!$omp parallel do private(i,j,k,km1,km2,kp1,kp2,kp3)
  do k=1,mz
    km2=mod(k-3+mz,mz)+1
    km1=mod(k-2+mz,mz)+1
    kp1=mod(k     ,mz)+1
    kp2=mod(k+1   ,mz)+1
    kp3=mod(k+2   ,mz)+1
    do j=1,my
      do i=1,mx
        zup(i,j,k) = ( &
                     a*(f(i,j,k  )+f(i,j,kp1)) + &
                     b*(f(i,j,km1)+f(i,j,kp2)) + &
                     c*(f(i,j,km2)+f(i,j,kp3)))
      end do
    end do
  end do
!
  nstag = nstag+1
  nflop = nflop+8
END FUNCTION

!***********************************************************************
FUNCTION xdn (f)
!
!  f is centered on (i,j,k), fifth order stagger
!
  use params
  real, dimension(mx,my,mz):: f, xdn
!hpf$ distribute(*,*,block):: f, xdn
!
  c = 3./256.
  b = -25./256.
  a = .5-b-c
  a1st = 0.5
!
!$omp parallel do private(i,j,k)
  do k=1,mz
   do j=1,my
    xdn(mx-1,j,k) = ( &
                   a*(f(mx-2,j,k)+f(mx-1,j,k)) &
                 + b*(f(mx-3,j,k)+f(mx  ,j,k)) &
                 + c*(f(mx-4,j,k)+f(1   ,j,k)))
    xdn(mx  ,j,k) = ( &
                   a*(f(mx-1,j,k)+f(mx  ,j,k)) &
                 + b*(f(mx-2,j,k)+f(1   ,j,k)) &
                 + c*(f(mx-3,j,k)+f(2   ,j,k)))
    xdn(1  ,j,k) = ( &
                   a*(f(mx  ,j,k)+f(1   ,j,k)) &
                 + b*(f(mx-1,j,k)+f(2   ,j,k)) &
                 + c*(f(mx-2,j,k)+f(3   ,j,k)))
    xdn(2  ,j,k) = ( &
                   a*(f(1   ,j,k)+f(2   ,j,k)) &
                 + b*(f(mx  ,j,k)+f(3   ,j,k)) &
                 + c*(f(mx-1,j,k)+f(4   ,j,k)))
    xdn(3  ,j,k) = ( &
                   a*(f(2   ,j,k)+f(3   ,j,k)) &
                 + b*(f(1   ,j,k)+f(4   ,j,k)) &
                 + c*(f(mx  ,j,k)+f(5   ,j,k)))
   end do
   do j=1,my
    do i=3,mx-3
      xdn(i+1,j,k) = ( &
                   a*(f(i   ,j,k)+f(i+1 ,j,k)) &
                 + b*(f(i-1 ,j,k)+f(i+2 ,j,k)) &
                 + c*(f(i-2 ,j,k)+f(i+3 ,j,k)))
    end do
   end do
  end do
!
!
  nstag = nstag+1
  nflop = nflop+8
END FUNCTION


!***********************************************************************
FUNCTION ydn (f)
!
!  f is centered on (i,j,k), fifth order stagger
!
  use params
  real, dimension(mx,my,mz):: f, ydn
!hpf$ distribute(*,*,block):: f, ydn
!
!-----------------------------------------------------------------------
  if (my .lt. 3) then
!$omp parallel do private(k)
    do k=1,mz
      ydn(:,:,k) = f(:,:,k)
    end do
    return
  end if
  c = 3./256.
  b = -25./256.
  a = .5-b-c
  a1st = 0.5
!
!$omp parallel do private(i,j,k)
  do k=1,mz
    do i=1,mx
      ydn(i,my-1,k) = ( &
                     a*(f(i,my-2,k)+f(i,my-1,k)) + &
                     b*(f(i,my-3,k)+f(i,my  ,k)) + &
                     c*(f(i,my-4,k)+f(i,1   ,k)))
      ydn(i,my  ,k) = ( &
                     a*(f(i,my-1,k)+f(i,my  ,k)) + &
                     b*(f(i,my-2,k)+f(i,1   ,k)) + &
                     c*(f(i,my-3,k)+f(i,2   ,k)))
      ydn(i,1   ,k) = ( &
                     a*(f(i,my  ,k)+f(i,1   ,k)) + &
                     b*(f(i,my-1,k)+f(i,2   ,k)) + &
                     c*(f(i,my-2,k)+f(i,3   ,k)))
      ydn(i,2   ,k) = ( &
                     a*(f(i,1   ,k)+f(i,2   ,k)) + &
                     b*(f(i,my  ,k)+f(i,3   ,k)) + &
                     c*(f(i,my-1,k)+f(i,4   ,k)))
      ydn(i,3   ,k) = ( &
                     a*(f(i,2   ,k)+f(i,3   ,k)) + &
                     b*(f(i,1   ,k)+f(i,4   ,k)) + &
                     c*(f(i,my  ,k)+f(i,5   ,k)))
    end do
    do j=3,my-3
     do i=1,mx
      ydn(i,j+1,k) = ( &
                     a*(f(i,j   ,k)+f(i,j+1 ,k)) + &
                     b*(f(i,j-1 ,k)+f(i,j+2 ,k)) + &
                     c*(f(i,j-2 ,k)+f(i,j+3 ,k)))
     end do
    end do
  end do
!
  nstag = nstag+1
  nflop = nflop+8
END FUNCTION

!***********************************************************************
SUBROUTINE ydn_add (f,ydn)
!
!  f is centered on (i,j,k), fifth order stagger
!
  use params
  real, dimension(mx,my,mz):: f, ydn
!hpf$ distribute(*,*,block):: f, ydn
!
!-----------------------------------------------------------------------
  if (my .lt. 3) then
    ydn = f
    return
  end if
  c = 3./256.
  b = -25./256.
  a = .5-b-c
  a1st = 0.5
!
!$omp parallel do private(i,j,k)
  do k=1,mz
    do i=1,mx
      ydn(i,my-1,k) = ydn(i,my-1,k) + ( &
                     a*(f(i,my-2,k)+f(i,my-1,k)) + &
                     b*(f(i,my-3,k)+f(i,my  ,k)) + &
                     c*(f(i,my-4,k)+f(i,1   ,k)))
      ydn(i,my  ,k) = ydn(i,my  ,k) + ( &
                     a*(f(i,my-1,k)+f(i,my  ,k)) + &
                     b*(f(i,my-2,k)+f(i,1   ,k)) + &
                     c*(f(i,my-3,k)+f(i,2   ,k)))
      ydn(i,1   ,k) = ydn(i,1   ,k) + ( &
                     a*(f(i,my  ,k)+f(i,1   ,k)) + &
                     b*(f(i,my-1,k)+f(i,2   ,k)) + &
                     c*(f(i,my-2,k)+f(i,3   ,k)))
      ydn(i,2   ,k) = ydn(i,2   ,k) + ( &
                     a*(f(i,1   ,k)+f(i,2   ,k)) + &
                     b*(f(i,my  ,k)+f(i,3   ,k)) + &
                     c*(f(i,my-1,k)+f(i,4   ,k)))
      ydn(i,3   ,k) = ydn(i,3   ,k) + ( &
                     a*(f(i,2   ,k)+f(i,3   ,k)) + &
                     b*(f(i,1   ,k)+f(i,4   ,k)) + &
                     c*(f(i,my  ,k)+f(i,5   ,k)))
    end do
    do j=3,my-3
     do i=1,mx
      ydn(i,j+1,k) = ydn(i,j+1,k) + ( &
                     a*(f(i,j   ,k)+f(i,j+1 ,k)) + &
                     b*(f(i,j-1 ,k)+f(i,j+2 ,k)) + &
                     c*(f(i,j-2 ,k)+f(i,j+3 ,k)))
     end do
    end do
  end do
!
  nstag = nstag+1
  nflop = nflop+8
END SUBROUTINE

!***********************************************************************
FUNCTION zdn (f)
!
!  f is centered on (i,j,k), fifth order stagger
!
  use params
  real, dimension(mx,my,mz):: f, zdn
!hpf$ distribute(*,*,block):: f, zdn
!
!-----------------------------------------------------------------------
  c = 3./256.
  b = -25./256.
  a = .5-b-c
  a1st = 0.5
!
!$omp parallel do private(i,j,k,km1,km2,km3,kp1,kp2)
  do k=1,mz
    km3=mod(k-4+mz,mz)+1
    km2=mod(k-3+mz,mz)+1
    km1=mod(k-2+mz,mz)+1
    kp1=mod(k     ,mz)+1
    kp2=mod(k+1   ,mz)+1
    do j=1,my
      do i=1,mx
        zdn(i,j,k) = ( &
                     a*(f(i,j,k  )+f(i,j,km1)) + &
                     b*(f(i,j,kp1)+f(i,j,km2)) + &
                     c*(f(i,j,kp2)+f(i,j,km3)))
      end do
    end do
  end do
!
  nstag = nstag+1
  nflop = nflop+8
END FUNCTION

!***********************************************************************
FUNCTION ddxup (f)
  use params
  real, dimension(mx,my,mz):: f, ddxup
!hpf$ distribute(*,*,block):: f, ddxup
!
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)/dx
  a1st = 1./dx
  b = b/dx
  c = c/dx
!
!$omp parallel do private(i,j,k)
  do k=1,mz
   do j=1,my
    ddxup(mx-2,j,k) = ( &
                   a*(f(mx-1,j,k)-f(mx-2,j,k)) &
                 + b*(f(mx  ,j,k)-f(mx-3,j,k)) &
                 + c*(f(1   ,j,k)-f(mx-4,j,k)))
    ddxup(mx-1,j,k) = ( &
                   a*(f(mx  ,j,k)-f(mx-1,j,k)) &
                 + b*(f(1   ,j,k)-f(mx-2,j,k)) &
                 + c*(f(2   ,j,k)-f(mx-3,j,k)))
    ddxup(mx  ,j,k) = ( &
                   a*(f(1   ,j,k)-f(mx  ,j,k)) &
                 + b*(f(2   ,j,k)-f(mx-1,j,k)) &
                 + c*(f(3   ,j,k)-f(mx-2,j,k)))
    ddxup(1  ,j,k) = ( &
                   a*(f(2   ,j,k)-f(1   ,j,k)) &
                 + b*(f(3   ,j,k)-f(mx  ,j,k)) &
                 + c*(f(4   ,j,k)-f(mx-1,j,k)))
    ddxup(2  ,j,k) = ( &
                   a*(f(3   ,j,k)-f(2  ,j,k)) &
                 + b*(f(4   ,j,k)-f(1  ,j,k)) &
                 + c*(f(5   ,j,k)-f(mx ,j,k)))
   end do
   do j=1,my
    do i=3,mx-3
      ddxup(i  ,j,k) = ( &
                   a*(f(i+1 ,j,k)-f(i   ,j,k)) &
                 + b*(f(i+2 ,j,k)-f(i-1 ,j,k)) &
                 + c*(f(i+3 ,j,k)-f(i-2 ,j,k)))
    end do
  end do
  end do
!
  nstag = nstag+1
  nflop = nflop+8
END FUNCTION

!***********************************************************************
FUNCTION ddyup (f)
  use params
  real, dimension(mx,my,mz):: f, ddyup
!hpf$ distribute(*,*,block):: f, ddyup
!
  if (my .lt. 3) then
    ddyup = 0.
    return
  end if
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)/dy
  a1st = 1./dy
  b = b/dy
  c = c/dy
!
!$omp parallel do private(i,j,k)
  do k=1,mz
    do i=1,mx
      ddyup(i,my-2,k) = ( &
                     a*(f(i,my-1,k)-f(i,my-2,k)) + &
                     b*(f(i,my  ,k)-f(i,my-3,k)) + &
                     c*(f(i,1   ,k)-f(i,my-4,k)))
      ddyup(i,my-1,k) = ( &
                     a*(f(i,my  ,k)-f(i,my-1,k)) + &
                     b*(f(i,1   ,k)-f(i,my-2,k)) + &
                     c*(f(i,2   ,k)-f(i,my-3,k)))
      ddyup(i,my  ,k) = ( &
                     a*(f(i,1   ,k)-f(i,my  ,k)) + &
                     b*(f(i,2   ,k)-f(i,my-1,k)) + &
                     c*(f(i,3   ,k)-f(i,my-2,k)))
      ddyup(i,1   ,k) = ( &
                     a*(f(i,2   ,k)-f(i,1   ,k)) + &
                     b*(f(i,3   ,k)-f(i,my  ,k)) + &
                     c*(f(i,4   ,k)-f(i,my-1,k)))
      ddyup(i,2   ,k) = ( &
                     a*(f(i,3   ,k)-f(i,2   ,k)) + &
                     b*(f(i,4   ,k)-f(i,1   ,k)) + &
                     c*(f(i,5   ,k)-f(i,my  ,k)))
    end do
    do j=3,my-3
     do i=1,mx
      ddyup(i,j  ,k) = ( &
                     a*(f(i,j+1 ,k)-f(i,j   ,k)) + &
                     b*(f(i,j+2 ,k)-f(i,j-1 ,k)) + &
                     c*(f(i,j+3 ,k)-f(i,j-2 ,k)))
     end do
    end do
  end do
!
  nstag = nstag+1
  nflop = nflop+8
END FUNCTION

!***********************************************************************
FUNCTION ddzup (f)
  use params
  real, dimension(mx,my,mz):: f, ddzup
!hpf$ distribute(*,*,block):: f, ddzup
!
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)/dz
  a1st = 1./dz
  b = b/dz
  c = c/dz
!
!$omp parallel do private(i,j,k,km1,km2,kp1,kp2,kp3)
  do k=1,mz
    km2=mod(k-3+mz,mz)+1
    km1=mod(k-2+mz,mz)+1
    kp1=mod(k     ,mz)+1
    kp2=mod(k+1   ,mz)+1
    kp3=mod(k+2   ,mz)+1
    do j=1,my
      do i=1,mx
        ddzup(i,j,k) = ( &
                       a*(f(i,j,kp1)-f(i,j,k  )) + &
                       b*(f(i,j,kp2)-f(i,j,km1)) + &
                       c*(f(i,j,kp3)-f(i,j,km2)))
      end do
    end do
  end do
!
  nstag = nstag+1
  nflop = nflop+8
END FUNCTION

!***********************************************************************
SUBROUTINE ddzup_set (f,ddzup)
  use params
  real, dimension(mx,my,mz):: f, ddzup
!hpf$ distribute(*,*,block):: f, ddzup
!
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)/dz
  a1st = 1./dz
  b = b/dz
  c = c/dz
!
!$omp parallel do private(i,j,k,km1,km2,kp1,kp2,kp3)
  do k=1,mz
    km2=mod(k-3+mz,mz)+1
    km1=mod(k-2+mz,mz)+1
    kp1=mod(k     ,mz)+1
    kp2=mod(k+1   ,mz)+1
    kp3=mod(k+2   ,mz)+1
    do j=1,my
      do i=1,mx
        ddzup(i,j,k) = ( &
                       a*(f(i,j,kp1)-f(i,j,k  )) + &
                       b*(f(i,j,kp2)-f(i,j,km1)) + &
                       c*(f(i,j,kp3)-f(i,j,km2)))
      end do
    end do
  end do
!
  nstag = nstag+1
  nflop = nflop+8
END SUBROUTINE

!***********************************************************************
FUNCTION ddxup1 (f)
  use params
  real, dimension(mx,my,mz):: f, ddxup1
!hpf$ distribute(*,*,block):: f, ddxup1
!
  a = 1./dx
!
!$omp parallel do private(i,j,k)
  do k=1,mz
   do j=1,my
    ddxup1(mx-2,j,k) = a*(f(mx-1,j,k)-f(mx-2,j,k))
    ddxup1(mx-1,j,k) = a*(f(mx  ,j,k)-f(mx-1,j,k))
    ddxup1(mx  ,j,k) = a*(f(1   ,j,k)-f(mx  ,j,k))
    ddxup1(1  ,j,k) = a*(f(2   ,j,k)-f(1   ,j,k))
    ddxup1(2  ,j,k) = a*(f(3   ,j,k)-f(2  ,j,k))
   end do
   do j=1,my
    do i=3,mx-3
      ddxup1(i  ,j,k) = a*(f(i+1 ,j,k)-f(i   ,j,k))
    end do
   end do
  end do
!
  nstag = nstag+1
  nflop = nflop+8
END FUNCTION

!***********************************************************************
FUNCTION ddyup1 (f)
  use params
  real, dimension(mx,my,mz):: f, ddyup1
!hpf$ distribute(*,*,block):: f, ddyup1
!
  if (my .lt. 3) then
    ddyup1 = 0.
    return
  end if
  a = 1./dy
!
!$omp parallel do private(i,j,k)
  do k=1,mz
    do i=1,mx
      ddyup1(i,my-2,k) = a*(f(i,my-1,k)-f(i,my-2,k))
      ddyup1(i,my-1,k) = a*(f(i,my  ,k)-f(i,my-1,k))
      ddyup1(i,my  ,k) = a*(f(i,1   ,k)-f(i,my  ,k))
      ddyup1(i,1   ,k) = a*(f(i,2   ,k)-f(i,1   ,k))
      ddyup1(i,2   ,k) = a*(f(i,3   ,k)-f(i,2   ,k))
    end do
    do j=3,my-3
     do i=1,mx
      ddyup1(i,j  ,k) = a*(f(i,j+1 ,k)-f(i,j   ,k))
     end do
    end do
  end do
!
  nstag = nstag+1
  nflop = nflop+8
END FUNCTION

!***********************************************************************
FUNCTION ddzup1 (f)
  use params
  real, dimension(mx,my,mz):: f, ddzup1
!hpf$ distribute(*,*,block):: f, ddzup1
!
  a = 1./dz
!
!$omp parallel do private(i,j,k,km1,km2,kp1,kp2,kp3)
  do k=1,mz
    km2=mod(k-3+mz,mz)+1
    km1=mod(k-2+mz,mz)+1
    kp1=mod(k     ,mz)+1
    kp2=mod(k+1   ,mz)+1
    kp3=mod(k+2   ,mz)+1
    do j=1,my
      do i=1,mx
        ddzup1(i,j,k) = a*(f(i,j,kp1)-f(i,j,k  ))
      end do
    end do
  end do
!
  nstag = nstag+1
  nflop = nflop+8
END FUNCTION

!***********************************************************************
FUNCTION ddxdn (f)
  use params
  real, dimension(mx,my,mz):: f, ddxdn
!hpf$ distribute(*,*,block):: f, ddxdn
!
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)/dx
  a1st = 1./dx
  b = b/dx
  c = c/dx
!
!$omp parallel do private(i,j,k)
  do k=1,mz
   do j=1,my
    ddxdn(mx-1,j,k) = ( &
                   a*(f(mx-1,j,k)-f(mx-2,j,k)) &
                 + b*(f(mx  ,j,k)-f(mx-3,j,k)) &
                 + c*(f(1   ,j,k)-f(mx-4,j,k)))
    ddxdn(mx  ,j,k) = ( &
                   a*(f(mx  ,j,k)-f(mx-1,j,k)) &
                 + b*(f(1   ,j,k)-f(mx-2,j,k)) &
                 + c*(f(2   ,j,k)-f(mx-3,j,k)))
    ddxdn(1   ,j,k) = ( &
                   a*(f(1   ,j,k)-f(mx  ,j,k)) &
                 + b*(f(2   ,j,k)-f(mx-1,j,k)) &
                 + c*(f(3   ,j,k)-f(mx-2,j,k)))
    ddxdn(2  ,j,k) = ( &
                   a*(f(2   ,j,k)-f(1   ,j,k)) &
                 + b*(f(3   ,j,k)-f(mx  ,j,k)) &
                 + c*(f(4   ,j,k)-f(mx-1,j,k)))
    ddxdn(3  ,j,k) = ( &
                   a*(f(3   ,j,k)-f(2   ,j,k)) &
                 + b*(f(4   ,j,k)-f(1   ,j,k)) &
                 + c*(f(5   ,j,k)-f(mx  ,j,k)))
   end do
   do j=1,my
    do i=3,mx-3
      ddxdn(i+1,j,k) = ( &
                   a*(f(i+1 ,j,k)-f(i   ,j,k)) &
                 + b*(f(i+2 ,j,k)-f(i-1 ,j,k)) &
                 + c*(f(i+3 ,j,k)-f(i-2 ,j,k)))
    end do
   end do
  end do
!
  nstag = nstag+1
  nflop = nflop+8
END FUNCTION

!***********************************************************************
FUNCTION ddydn (f)
  use params
  real, dimension(mx,my,mz):: f, ddydn
!hpf$ distribute(*,*,block):: f, ddydn
!
  if (my .lt. 3) then
    ddydn = 0.
    return
  end if
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)/dy
  a1st = 1./dy
  b = b/dy
  c = c/dy
!
!$omp parallel do private(i,j,k)
  do k=1,mz
    do i=1,mx
      ddydn(i,my-1,k) = ( &
                     a*(f(i,my-1,k)-f(i,my-2,k)) + &
                     b*(f(i,my  ,k)-f(i,my-3,k)) + &
                     c*(f(i,1   ,k)-f(i,my-4,k)))
      ddydn(i,my  ,k) = ( &
                     a*(f(i,my  ,k)-f(i,my-1,k)) + &
                     b*(f(i,1   ,k)-f(i,my-2,k)) + &
                     c*(f(i,2   ,k)-f(i,my-3,k)))
      ddydn(i,1  ,k) = ( &
                     a*(f(i,1   ,k)-f(i,my  ,k)) + &
                     b*(f(i,2   ,k)-f(i,my-1,k)) + &
                     c*(f(i,3   ,k)-f(i,my-2,k)))
      ddydn(i,2   ,k) = ( &
                     a*(f(i,2   ,k)-f(i,1   ,k)) + &
                     b*(f(i,3   ,k)-f(i,my  ,k)) + &
                     c*(f(i,4   ,k)-f(i,my-1,k)))
      ddydn(i,3   ,k) = ( &
                     a*(f(i,3   ,k)-f(i,2   ,k)) + &
                     b*(f(i,4   ,k)-f(i,1   ,k)) + &
                     c*(f(i,5   ,k)-f(i,my  ,k)))
    end do
    do j=3,my-3
     do i=1,mx
      ddydn(i,j+1 ,k) = ( &
                     a*(f(i,j+1 ,k)-f(i,j   ,k)) + &
                     b*(f(i,j+2 ,k)-f(i,j-1 ,k)) + &
                     c*(f(i,j+3 ,k)-f(i,j-2 ,k)))
     end do
    end do
  end do
!
!
  nstag = nstag+1
  nflop = nflop+8
END FUNCTION

!***********************************************************************
FUNCTION ddzdn (f)
  use params
  real, dimension(mx,my,mz):: f, ddzdn
!hpf$ distribute(*,*,block):: f, ddzdn
!
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)/dz
  a1st = 1./dz
  b = b/dz
  c = c/dz
!
!$omp parallel do private(i,j,k,km1,km2,km3,kp1,kp2)
  do k=1,mz
    km3=mod(k-4+mz,mz)+1
    km2=mod(k-3+mz,mz)+1
    km1=mod(k-2+mz,mz)+1
    kp1=mod(k     ,mz)+1
    kp2=mod(k+1   ,mz)+1
    do j=1,my
      do i=1,mx
        ddzdn(i,j,k) = ( &
                       a*(f(i,j,k  )-f(i,j,km1)) + &
                       b*(f(i,j,kp1)-f(i,j,km2)) + &
                       c*(f(i,j,kp2)-f(i,j,km3)))
      end do
    end do
  end do
!
  nstag = nstag+1
  nflop = nflop+8
END FUNCTION

!***********************************************************************
FUNCTION ddxdn1 (f)
  use params
  real, dimension(mx,my,mz):: f, ddxdn1
!hpf$ distribute(*,*,block):: f, ddxdn1
!
  a = 1./dx
!
!$omp parallel do private(i,j,k)
  do k=1,mz
   do j=1,my
    ddxdn1(mx-1,j,k) = a*(f(mx-1,j,k)-f(mx-2,j,k))
    ddxdn1(mx  ,j,k) = a*(f(mx  ,j,k)-f(mx-1,j,k))
    ddxdn1(1   ,j,k) = a*(f(1   ,j,k)-f(mx  ,j,k))
    ddxdn1(2   ,j,k) = a*(f(2   ,j,k)-f(1   ,j,k))
    ddxdn1(3   ,j,k) = a*(f(3   ,j,k)-f(2   ,j,k))
   end do
   do j=1,my
     do i=3,mx-3
      ddxdn1(i+1,j,k) = a*(f(i+1 ,j,k)-f(i   ,j,k))
    end do
   end do
  end do
!
  nflop = nflop+8
END FUNCTION

!***********************************************************************
FUNCTION ddydn1 (f)
  use params
  real, dimension(mx,my,mz):: f, ddydn1
!hpf$ distribute(*,*,block):: f, ddydn1
!
  if (my .lt. 3) then
    ddydn1 = 0.
    return
  end if
  a = 1./dy
!
!$omp parallel do private(i,j,k)
  do k=1,mz
    do i=1,mx
      ddydn1(i,1   ,k) = a*(f(i,1   ,k)-f(i,my  ,k))
    end do
    do j=1,my-1
     do i=1,mx
      ddydn1(i,j+1 ,k) = a*(f(i,j+1 ,k)-f(i,j   ,k))
     end do
    end do
  end do
!
!
  nflop = nflop+8
END FUNCTION

!***********************************************************************
FUNCTION ddzdn1 (f)
  use params
  real, dimension(mx,my,mz):: f, ddzdn1
!hpf$ distribute(*,*,block):: f, ddzdn1
!
  a = 1./dz
!
!$omp parallel do private(i,j,k,km1,km2,km3,kp1,kp2)
  do k=1,mz
    km1=mod(k-2+mz,mz)+1
    do j=1,my
      do i=1,mx
        ddzdn1(i,j,k) = a*(f(i,j,k  )-f(i,j,km1))
      end do
    end do
  end do
!
  nflop = nflop+8
END FUNCTION

!***********************************************************************
SUBROUTINE ddzdn1_set (f,ddzdn1)
  use params
  real, dimension(mx,my,mz):: f, ddzdn1
!hpf$ distribute(*,*,block):: f, ddzdn1
!
  a = 1./dz
!
!$omp parallel do private(i,j,k,km1,km2,km3,kp1,kp2)
  do k=1,mz
    km1=mod(k-2+mz,mz)+1
    do j=1,my
      do i=1,mx
        ddzdn1(i,j,k) = a*(f(i,j,k  )-f(i,j,km1))
      end do
    end do
  end do
!
  nflop = nflop+8
END SUBROUTINE

!***********************************************************************
FUNCTION smooth3 (f)
!
!  Three point smooth
!
  USE params
  implicit none
  integer i,j,k,km1,kp1
  real, dimension(mx,my,mz):: f, smooth3
!hpf$ distribute(*,*,block):: f, smooth3
  real, dimension(0:mx+1):: fx
  real, dimension(0:my+1):: fy
  real, dimension(mx,my,mz):: fz
!hpf$ distribute(*,*,block):: fz
  real c3
!-----------------------------------------------------------------------
!
  c3=1./3.
!$omp parallel do private(i,j,k,fx)
  do k=1,mz
  do j=1,my
    do i=1,mx
      fx(i)=f(i,j,k)
    enddo
    fx(0)=fx(mx)
    fx(mx+1)=fx(1)
    do i=1,mx
      fz(i,j,k)=c3*(fx(i-1)+fx(i)+fx(i+1))
    end do
  end do
  end do
!$omp parallel do private(i,j,k,fy)
  do k=1,mz
  do i=1,mx
    do j=1,my
      fy(j)=fz(i,j,k)
    enddo
    fy(0)=fy(my)
    fy(my+1)=fy(1)
    do j=1,my
      fz(i,j,k)=c3*(fy(j-1)+fy(j)+fy(j+1))
    end do
   end do
  end do
!$omp parallel do private(i,j,k,km1,kp1)
  do k=1,mz
   km1=mod(k+mz-2,mz)+1
   kp1=mod(k+mz  ,mz)+1
   do j=1,my
    do i=1,mx
      smooth3(i,j,k)=c3*(fz(i,j,k)+fz(i,j,km1)+fz(i,j,kp1))
    enddo
   enddo
  enddo
END FUNCTION

!***********************************************************************
FUNCTION max3 (f)
!
!  Three point max
!
  USE params
  implicit none
  integer i,j,k,km1,kp1
  real, dimension(mx,my,mz):: f, max3
!hpf$ distribute(*,*,block):: f, max3
  real, dimension(0:mx+1):: fx
  real, dimension(0:my+1):: fy
  real, dimension(mx,my,mz):: fz
!hpf$ distribute(*,*,block):: fz
!-----------------------------------------------------------------------
!
!$omp parallel do private(i,j,k,fx)
  do k=1,mz
  do j=1,my
    do i=1,mx
      fx(i)=f(i,j,k)
    enddo
    fx(0)=fx(mx)
    fx(mx+1)=fx(1)
    do i=1,mx
      fz(i,j,k)=amax1(fx(i-1),fx(i),fx(i+1))
    end do
  end do
  end do
!$omp parallel do private(i,j,k,fy)
  do k=1,mz
  do i=1,mx
    do j=1,my
      fy(j)=fz(i,j,k)
    enddo
    fy(0)=fy(my)
    fy(my+1)=fy(1)
    do j=1,my
      fz(i,j,k)=amax1(fy(j-1),fy(j),fy(j+1))
    end do
   end do
  end do
!$omp parallel do private(i,j,k,km1,kp1)
  do k=1,mz
    km1=mod(k+mz-2,mz)+1
    kp1=mod(k+mz  ,mz)+1
    do j=1,my
    do i=1,mx
      max3(i,j,k)=amax1(fz(i,j,k),fz(i,j,km1),fz(i,j,kp1))
    enddo
    enddo
  enddo
END FUNCTION

!***********************************************************************
FUNCTION max5 (f)
!
!  Three point max
!
  USE params
  implicit none
  integer i,j,k,km1,kp1,km2,kp2
  real, dimension(mx,my,mz), intent(in) :: f
  real, dimension(mx,my,mz):: max5
  real, dimension(-1:mx+2):: fx
  real, dimension(-1:my+2):: fy
  real, dimension(mx,my,mz):: fz
!-----------------------------------------------------------------------
!
  do k=1,mz
  do j=1,my
    do i=1,mx
      fx(i)=f(i,j,k)
    enddo
    fx(  -1)=fx(mx-1)
    fx(   0)=fx(mx  )
    fx(mx+1)=fx(   1)
    fx(mx+2)=fx(   2)
    do i=1,mx
      fz(i,j,k)=amax1(fx(i-2),fx(i-1),fx(i),fx(i+1),fx(i+2))
    end do
  end do
  end do
  do k=1,mz
  do i=1,mx
    do j=1,my
      fy(j)=fz(i,j,k)
    enddo
    fy(  -1)=fy(my-1)
    fy(   0)=fy(my  )
    fy(my+1)=fy(   1)
    fy(my+2)=fy(   2)
    do j=1,my
      fz(i,j,k)=amax1(fy(j-2),fy(j-1),fy(j),fy(j+1),fy(j+2))
    end do
   end do
  end do
  do k=1,mz
    km2=mod(k+mz-3,mz)+1
    km1=mod(k+mz-2,mz)+1
    kp1=mod(k+mz  ,mz)+1
    kp2=mod(k+mz+1,mz)+1
    do j=1,my
    do i=1,mx
      max5(i,j,k)=amax1(fz(i,j,km2),fz(i,j,km1),fz(i,j,k),fz(i,j,kp1),fz(i,j,kp2))
    enddo
    enddo
  enddo
END FUNCTION

!***********************************************************************
SUBROUTINE dif2_set (fx, fy, fz, difu)
!
!  Finite difference one zone left to one zones right.
!

  USE params
  real cx, cy, cz
  real, dimension(mx,my,mz):: fx, fy, fz, difu
!hpf$ align with TheCube:: fx, fy, fz, difu
!-----------------------------------------------------------------------

  cx=0.5/dx
  cy=0.5/dy
  cz=0.5/dz

!$omp parallel do private(i,j,k)
  do k=1,mz
    do j=1,my
      do i=2,mx-1
        difu(i,j,k) = cx*(fx(i-1 ,j,k)-fx(i+1,j,k))
      end do
      difu(1 ,j,k)  = cx*(fx(mx  ,j,k)-fx(2  ,j,k))
      difu(mx,j,k)  = cx*(fx(mx-1,j,k)-fx(1  ,j,k))
    end do
  end do

!$omp parallel do private(i,j,k)
  do k=1,mz
    do j=2,my-1
      do i=1,mx
        difu(i,j, k) = difu(i,j, k) + cy*(fy(i,j -1,k)-fy(i,j+1,k))
      end do
    end do
    do i=1,mx
        difu(i,1 ,k) = difu(i,1 ,k) + cy*(fy(i,my  ,k)-fy(i,  2,k))
        difu(i,my,k) = difu(i,my,k) + cy*(fy(i,my-1,k)-fy(i,  1,k))
    end do
  end do

  do j=1,my
    do i=1,mx
        difu(i,j,1 ) = difu(i,j,1 ) + cz*(fz(i,j,mz  )-fz(i,  j,2))
        difu(i,j,mz) = difu(i,j,mz) + cz*(fz(i,j,mz-1)-fz(i,  j,1))
    end do
  end do
!$omp parallel do private(i,j,k)
  do k=2,mz-1
    do j=1,my
      do i=1,mx
        difu(i,j,k ) = difu(i,j,k ) + cz*(fz(i,j,k -1)-fz(i,j,k+1))
      end do
    end do
  end do
END subroutine

!-----------------------------------------------------------------------

SUBROUTINE init_stagger
END SUBROUTINE

SUBROUTINE init_derivs
END SUBROUTINE
END MODULE
