MODULE stagger
!***********************************************************************
!
!  6th order staggered derivatives, 5th order interpolation. This version
!  uses explicit loops, for increased speed and parallelization.
!
!***********************************************************************
CONTAINS
FUNCTION xup (f)
!
!  f is centered on (i-.5,j,k), fifth order stagger
!
  use params
  real, dimension(mx,my,mz):: f, xup
!hpf$ distribute(*,*,block):: f, xup


  c = 3./256.
  b = -25./256.
  a = .5-b-c
  a1st = 0.5

!$omp parallel do private(i,j,k)
  do k=1,mz
   do j=1,my
    xup(mx-2,j,k) = ( &
                   a*(f(mx-2,j,k)+f(mx-1,j,k)) &
                 + b*(f(mx-3,j,k)+f(mx  ,j,k)) &
                 + c*(f(mx-4,j,k)+f(1   ,j,k)))
    xup(mx-1,j,k) = ( &
                   a*(f(mx-1,j,k)+f(mx  ,j,k)) &
                 + b*(f(mx-2,j,k)+f(1   ,j,k)) &
                 + c*(f(mx-3,j,k)+f(2   ,j,k)))
    xup(mx  ,j,k) = ( &
                   a*(f(mx  ,j,k)+f(1   ,j,k)) &
                 + b*(f(mx-1,j,k)+f(2   ,j,k)) &
                 + c*(f(mx-2,j,k)+f(3   ,j,k)))
    xup(1   ,j,k) = ( &
                   a*(f(1   ,j,k)+f(2   ,j,k)) &
                 + b*(f(mx  ,j,k)+f(3   ,j,k)) &
                 + c*(f(mx-1,j,k)+f(4   ,j,k)))
    xup(2   ,j,k) = ( &
                   a*(f(2   ,j,k)+f(3   ,j,k)) &
                 + b*(f(1   ,j,k)+f(4   ,j,k)) &
                 + c*(f(mx  ,j,k)+f(5   ,j,k)))
   end do
   do j=1,my
    do i=3,mx-3
      xup(i  ,j,k) = ( &
                    a*(f(i  ,j,k)+f(i+1 ,j,k)) &
                  + b*(f(i-1,j,k)+f(i+2 ,j,k)) &
                  + c*(f(i-2,j,k)+f(i+3 ,j,k)))
    end do
   end do
  end do

!
  nflop = nflop+8
END FUNCTION
SUBROUTINE xup_set (f,xup)
!
!  f is centered on (i-.5,j,k), fifth order stagger
!
  use params
  real, dimension(mx,my,mz):: f, xup
!hpf$ distribute(*,*,block):: f, xup

  c = 3./256.
  b = -25./256.
  a = .5-b-c
  a1st = 0.5

!$omp parallel do private(i,j,k)
  do k=1,mz
   do j=1,my
    xup(mx-2,j,k) = ( &
                   a*(f(mx-2,j,k)+f(mx-1,j,k)) &
                 + b*(f(mx-3,j,k)+f(mx  ,j,k)) &
                 + c*(f(mx-4,j,k)+f(1   ,j,k)))
    xup(mx-1,j,k) = ( &
                   a*(f(mx-1,j,k)+f(mx  ,j,k)) &
                 + b*(f(mx-2,j,k)+f(1   ,j,k)) &
                 + c*(f(mx-3,j,k)+f(2   ,j,k)))
    xup(mx  ,j,k) = ( &
                   a*(f(mx  ,j,k)+f(1   ,j,k)) &
                 + b*(f(mx-1,j,k)+f(2   ,j,k)) &
                 + c*(f(mx-2,j,k)+f(3   ,j,k)))
    xup(1   ,j,k) = ( &
                   a*(f(1   ,j,k)+f(2   ,j,k)) &
                 + b*(f(mx  ,j,k)+f(3   ,j,k)) &
                 + c*(f(mx-1,j,k)+f(4   ,j,k)))
    xup(2   ,j,k) = ( &
                   a*(f(2   ,j,k)+f(3   ,j,k)) &
                 + b*(f(1   ,j,k)+f(4   ,j,k)) &
                 + c*(f(mx  ,j,k)+f(5   ,j,k)))
   end do
   do j=1,my
    do i=3,mx-3
      xup(i  ,j,k) = ( &
                    a*(f(i  ,j,k)+f(i+1 ,j,k)) &
                  + b*(f(i-1,j,k)+f(i+2 ,j,k)) &
                  + c*(f(i-2,j,k)+f(i+3 ,j,k)))
    end do
   end do
  end do

!
  nflop = nflop+8
END SUBROUTINE

!***********************************************************************
FUNCTION yup (f)
!
!  f is centered on (i,j-.5,k), fifth order stagger
!
  use params
  real, intent(in) , dimension(mx,my,mz):: f
  real,              dimension(mx,my,mz):: yup
!hpf$ distribute(*,*,block):: f, yup
!-----------------------------------------------------------------------
  if (my .lt. 3) then
!$omp parallel do private(k)
    do k=1,mz
      yup(:,:,k) = f(:,:,k)
    end do
    return
  end if
  c = 3./256.
  b = -25./256.
  a = .5-b-c
  a1st = 0.5
!
!$omp parallel do private(i,j,k)
  do k=1,mz
    do i=1,mx
      yup(i,my-2,k) = ( &
                     a*(f(i,my-2,k)+f(i,my-1,k)) + &
                     b*(f(i,my-3,k)+f(i,my  ,k)) + &
                     c*(f(i,my-4,k)+f(i,1   ,k)))
      yup(i,my-1,k) = ( &
                     a*(f(i,my-1,k)+f(i,my  ,k)) + &
                     b*(f(i,my-2,k)+f(i,1   ,k)) + &
                     c*(f(i,my-3,k)+f(i,2   ,k)))
      yup(i,my  ,k) = ( &
                     a*(f(i,my  ,k)+f(i,1   ,k)) + &
                     b*(f(i,my-1,k)+f(i,2   ,k)) + &
                     c*(f(i,my-2,k)+f(i,3   ,k)))
      yup(i,1   ,k) = ( &
                     a*(f(i,1   ,k)+f(i,2   ,k)) + &
                     b*(f(i,my  ,k)+f(i,3   ,k)) + &
                     c*(f(i,my-1,k)+f(i,4   ,k)))
      yup(i,2   ,k) = ( &
                     a*(f(i,2   ,k)+f(i,3   ,k)) + &
                     b*(f(i,1   ,k)+f(i,4   ,k)) + &
                     c*(f(i,my  ,k)+f(i,5   ,k)))
    end do
    do j=3,my-3
     do i=1,mx
      yup(i,j  ,k) = ( &
                     a*(f(i,j   ,k)+f(i,j+1 ,k)) + &
                     b*(f(i,j-1 ,k)+f(i,j+2 ,k)) + &
                     c*(f(i,j-2 ,k)+f(i,j+3 ,k)))
     end do
    end do
  end do
!
  nflop = nflop+8
END FUNCTION

!***********************************************************************
SUBROUTINE yup_set (f,yup)
!
!  f is centered on (i,j-.5,k), fifth order stagger
!
  use params
  real, intent(in), dimension(mx,my,mz):: f
  real, dimension(mx,my,mz):: yup
!hpf$ distribute(*,*,block):: f, yup
!-----------------------------------------------------------------------
  if (my .lt. 3) then
    yup = f
    return
  end if
  c = 3./256.
  b = -25./256.
  a = .5-b-c
  a1st = 0.5
!
!$omp parallel do private(i,j,k)
  do k=1,mz
    do i=1,mx
      yup(i,my-2,k) = ( &
                     a*(f(i,my-2,k)+f(i,my-1,k)) + &
                     b*(f(i,my-3,k)+f(i,my  ,k)) + &
                     c*(f(i,my-4,k)+f(i,1   ,k)))
      yup(i,my-1,k) = ( &
                     a*(f(i,my-1,k)+f(i,my  ,k)) + &
                     b*(f(i,my-2,k)+f(i,1   ,k)) + &
                     c*(f(i,my-3,k)+f(i,2   ,k)))
      yup(i,my  ,k) = ( &
                     a*(f(i,my  ,k)+f(i,1   ,k)) + &
                     b*(f(i,my-1,k)+f(i,2   ,k)) + &
                     c*(f(i,my-2,k)+f(i,3   ,k)))
      yup(i,1   ,k) = ( &
                     a*(f(i,1   ,k)+f(i,2   ,k)) + &
                     b*(f(i,my  ,k)+f(i,3   ,k)) + &
                     c*(f(i,my-1,k)+f(i,4   ,k)))
      yup(i,2   ,k) = ( &
                     a*(f(i,2   ,k)+f(i,3   ,k)) + &
                     b*(f(i,1   ,k)+f(i,4   ,k)) + &
                     c*(f(i,my  ,k)+f(i,5   ,k)))
    end do
    do j=3,my-3
     do i=1,mx
      yup(i,j  ,k) = ( &
                     a*(f(i,j   ,k)+f(i,j+1 ,k)) + &
                     b*(f(i,j-1 ,k)+f(i,j+2 ,k)) + &
                     c*(f(i,j-2 ,k)+f(i,j+3 ,k)))
     end do
    end do
  end do
!
  nflop = nflop+8
END SUBROUTINE

!***********************************************************************
FUNCTION zup (f)
!
!  f is centered on (i,j,k-.5), fifth order stagger
!
  use params
  real, dimension(mx,my,mz):: f, zup
!hpf$ distribute(*,*,block):: f, zup
!
!-----------------------------------------------------------------------
  c = 3./256.
  b = -25./256.
  a = .5-b-c
  a1st = 0.5
!
!$omp parallel do private(i,j,k,km1,km2,kp1,kp2,kp3)
  do k=1,mz
    km2=mod(k-3+mz,mz)+1
    km1=mod(k-2+mz,mz)+1
    kp1=mod(k     ,mz)+1
    kp2=mod(k+1   ,mz)+1
    kp3=mod(k+2   ,mz)+1
    do j=1,my
      do i=1,mx
        zup(i,j,k) = ( &
                     a*(f(i,j,k  )+f(i,j,kp1)) + &
                     b*(f(i,j,km1)+f(i,j,kp2)) + &
                     c*(f(i,j,km2)+f(i,j,kp3)))
      end do
    end do
  end do
!
  nflop = nflop+8
END FUNCTION

!***********************************************************************
SUBROUTINE zup_set (f,zup)
!
!  f is centered on (i,j,k-.5), fifth order stagger
!
  use params
  real, dimension(mx,my,mz):: f, zup
!hpf$ distribute(*,*,block):: f, zup
!
!-----------------------------------------------------------------------
  c = 3./256.
  b = -25./256.
  a = .5-b-c
  a1st = 0.5
!
!$omp parallel do private(i,j,k,km1,km2,kp1,kp2,kp3)
  do k=1,mz
    km2=mod(k-3+mz,mz)+1
    km1=mod(k-2+mz,mz)+1
    kp1=mod(k     ,mz)+1
    kp2=mod(k+1   ,mz)+1
    kp3=mod(k+2   ,mz)+1
    do j=1,my
      do i=1,mx
        zup(i,j,k) = ( &
                     a*(f(i,j,k  )+f(i,j,kp1)) + &
                     b*(f(i,j,km1)+f(i,j,kp2)) + &
                     c*(f(i,j,km2)+f(i,j,kp3)))
      end do
    end do
  end do
!
  nflop = nflop+8
END SUBROUTINE

!***********************************************************************
FUNCTION xdn (f)
!
!  f is centered on (i,j,k), fifth order stagger
!
  use params
  real, dimension(mx,my,mz):: f, xdn
!hpf$ distribute(*,*,block):: f, xdn
!
  c = 3./256.
  b = -25./256.
  a = .5-b-c
  a1st = 0.5
!
!$omp parallel do private(i,j,k)
  do k=1,mz
  do j=1,my
    xdn(mx-1,j,k) = ( &
                   a*(f(mx-2,j,k)+f(mx-1,j,k)) &
                 + b*(f(mx-3,j,k)+f(mx  ,j,k)) &
                 + c*(f(mx-4,j,k)+f(1   ,j,k)))
    xdn(mx  ,j,k) = ( &
                   a*(f(mx-1,j,k)+f(mx  ,j,k)) &
                 + b*(f(mx-2,j,k)+f(1   ,j,k)) &
                 + c*(f(mx-3,j,k)+f(2   ,j,k)))
    xdn(1  ,j,k) = ( &
                   a*(f(mx  ,j,k)+f(1   ,j,k)) &
                 + b*(f(mx-1,j,k)+f(2   ,j,k)) &
                 + c*(f(mx-2,j,k)+f(3   ,j,k)))
    xdn(2  ,j,k) = ( &
                   a*(f(1   ,j,k)+f(2   ,j,k)) &
                 + b*(f(mx  ,j,k)+f(3   ,j,k)) &
                 + c*(f(mx-1,j,k)+f(4   ,j,k)))
    xdn(3  ,j,k) = ( &
                   a*(f(2   ,j,k)+f(3   ,j,k)) &
                 + b*(f(1   ,j,k)+f(4   ,j,k)) &
                 + c*(f(mx  ,j,k)+f(5   ,j,k)))
  end do
  do j=1,my
    do i=3,mx-3
      xdn(i+1,j,k) = ( &
                   a*(f(i   ,j,k)+f(i+1 ,j,k)) &
                 + b*(f(i-1 ,j,k)+f(i+2 ,j,k)) &
                 + c*(f(i-2 ,j,k)+f(i+3 ,j,k)))
    end do
  end do
  do j=1,my
  end do
  end do
!
!
  nflop = nflop+8
END FUNCTION

!***********************************************************************
SUBROUTINE xdn_set (f,xdn)
!
!  f is centered on (i,j,k), fifth order stagger
!
  use params
  real, dimension(mx,my,mz):: f, xdn
!hpf$ distribute(*,*,block):: f, xdn
!
  c = 3./256.
  b = -25./256.
  a = .5-b-c
  a1st = 0.5
!
!$omp parallel do private(i,j,k)
  do k=1,mz
  do j=1,my
    xdn(mx-1,j,k) = ( &
                   a*(f(mx-2,j,k)+f(mx-1,j,k)) &
                 + b*(f(mx-3,j,k)+f(mx  ,j,k)) &
                 + c*(f(mx-4,j,k)+f(1   ,j,k)))
    xdn(mx  ,j,k) = ( &
                   a*(f(mx-1,j,k)+f(mx  ,j,k)) &
                 + b*(f(mx-2,j,k)+f(1   ,j,k)) &
                 + c*(f(mx-3,j,k)+f(2   ,j,k)))
    xdn(1  ,j,k) = ( &
                   a*(f(mx  ,j,k)+f(1   ,j,k)) &
                 + b*(f(mx-1,j,k)+f(2   ,j,k)) &
                 + c*(f(mx-2,j,k)+f(3   ,j,k)))
    xdn(2  ,j,k) = ( &
                   a*(f(1   ,j,k)+f(2   ,j,k)) &
                 + b*(f(mx  ,j,k)+f(3   ,j,k)) &
                 + c*(f(mx-1,j,k)+f(4   ,j,k)))
    xdn(3  ,j,k) = ( &
                   a*(f(2   ,j,k)+f(3   ,j,k)) &
                 + b*(f(1   ,j,k)+f(4   ,j,k)) &
                 + c*(f(mx  ,j,k)+f(5   ,j,k)))
  end do
  do j=1,my
    do i=3,mx-3
      xdn(i+1,j,k) = ( &
                   a*(f(i   ,j,k)+f(i+1 ,j,k)) &
                 + b*(f(i-1 ,j,k)+f(i+2 ,j,k)) &
                 + c*(f(i-2 ,j,k)+f(i+3 ,j,k)))
    end do
  end do
  do j=1,my
  end do
  end do
!
!
  nflop = nflop+8
END SUBROUTINE

!***********************************************************************
SUBROUTINE xdn_add (f,xdn)
!
!  f is centered on (i,j,k), fifth order stagger
!
  use params
  real, dimension(mx,my,mz):: f, xdn
!hpf$ distribute(*,*,block):: f, xdn
!
  c = 3./256.
  b = -25./256.
  a = .5-b-c
  a1st = 0.5
!
!$omp parallel do private(i,j,k)
  do k=1,mz
  do j=1,my
    xdn(mx-1,j,k) = xdn(mx-1,j,k) + ( &
                   a*(f(mx-2,j,k)+f(mx-1,j,k)) &
                 + b*(f(mx-3,j,k)+f(mx  ,j,k)) &
                 + c*(f(mx-4,j,k)+f(1   ,j,k)))
    xdn(mx  ,j,k) = xdn(mx  ,j,k) + ( &
                   a*(f(mx-1,j,k)+f(mx  ,j,k)) &
                 + b*(f(mx-2,j,k)+f(1   ,j,k)) &
                 + c*(f(mx-3,j,k)+f(2   ,j,k)))
    xdn(1  ,j,k) = xdn(1  ,j,k) + ( &
                   a*(f(mx  ,j,k)+f(1   ,j,k)) &
                 + b*(f(mx-1,j,k)+f(2   ,j,k)) &
                 + c*(f(mx-2,j,k)+f(3   ,j,k)))
    xdn(2  ,j,k) = xdn(2  ,j,k) + ( &
                   a*(f(1   ,j,k)+f(2   ,j,k)) &
                 + b*(f(mx  ,j,k)+f(3   ,j,k)) &
                 + c*(f(mx-1,j,k)+f(4   ,j,k)))
    xdn(3  ,j,k) = xdn(3  ,j,k) + ( &
                   a*(f(2   ,j,k)+f(3   ,j,k)) &
                 + b*(f(1   ,j,k)+f(4   ,j,k)) &
                 + c*(f(mx  ,j,k)+f(5   ,j,k)))
  end do
  do j=1,my
    do i=3,mx-3
      xdn(i+1,j,k) = xdn(i+1,j,k) + ( &
                   a*(f(i   ,j,k)+f(i+1 ,j,k)) &
                 + b*(f(i-1 ,j,k)+f(i+2 ,j,k)) &
                 + c*(f(i-2 ,j,k)+f(i+3 ,j,k)))
    end do
  end do
  do j=1,my
  end do
  end do
!
!
  nflop = nflop+8
END SUBROUTINE

!***********************************************************************
FUNCTION ydn (f)
!
!  f is centered on (i,j,k), fifth order stagger
!
  use params
  real, dimension(mx,my,mz):: f, ydn
!hpf$ distribute(*,*,block):: f, ydn
!
!-----------------------------------------------------------------------
  if (my .lt. 3) then
!$omp parallel do private(k)
    do k=1,mz
      ydn(:,:,k) = f(:,:,k)
    end do
    return
  end if
  c = 3./256.
  b = -25./256.
  a = .5-b-c
  a1st = 0.5
!
!$omp parallel do private(i,j,k)
  do k=1,mz
    do i=1,mx
      ydn(i,my-1,k) = ( &
                     a*(f(i,my-2,k)+f(i,my-1,k)) + &
                     b*(f(i,my-3,k)+f(i,my  ,k)) + &
                     c*(f(i,my-4,k)+f(i,1   ,k)))
      ydn(i,my  ,k) = ( &
                     a*(f(i,my-1,k)+f(i,my  ,k)) + &
                     b*(f(i,my-2,k)+f(i,1   ,k)) + &
                     c*(f(i,my-3,k)+f(i,2   ,k)))
      ydn(i,1   ,k) = ( &
                     a*(f(i,my  ,k)+f(i,1   ,k)) + &
                     b*(f(i,my-1,k)+f(i,2   ,k)) + &
                     c*(f(i,my-2,k)+f(i,3   ,k)))
      ydn(i,2   ,k) = ( &
                     a*(f(i,1   ,k)+f(i,2   ,k)) + &
                     b*(f(i,my  ,k)+f(i,3   ,k)) + &
                     c*(f(i,my-1,k)+f(i,4   ,k)))
      ydn(i,3   ,k) = ( &
                     a*(f(i,2   ,k)+f(i,3   ,k)) + &
                     b*(f(i,1   ,k)+f(i,4   ,k)) + &
                     c*(f(i,my  ,k)+f(i,5   ,k)))
    end do
    do j=3,my-3
     do i=1,mx
      ydn(i,j+1,k) = ( &
                     a*(f(i,j   ,k)+f(i,j+1 ,k)) + &
                     b*(f(i,j-1 ,k)+f(i,j+2 ,k)) + &
                     c*(f(i,j-2 ,k)+f(i,j+3 ,k)))
     end do
    end do
  end do
!
  nflop = nflop+8
END FUNCTION

!***********************************************************************
SUBROUTINE ydn_set (f,ydn)
!
!  f is centered on (i,j,k), fifth order stagger
!
  use params
  real, dimension(mx,my,mz):: f, ydn
!hpf$ distribute(*,*,block):: f, ydn
!
!-----------------------------------------------------------------------
  if (my .lt. 3) then
    ydn = f
    return
  end if
  c = 3./256.
  b = -25./256.
  a = .5-b-c
  a1st = 0.5
!
!$omp parallel do private(i,j,k)
  do k=1,mz
    do i=1,mx
      ydn(i,my-1,k) = ( &
                     a*(f(i,my-2,k)+f(i,my-1,k)) + &
                     b*(f(i,my-3,k)+f(i,my  ,k)) + &
                     c*(f(i,my-4,k)+f(i,1   ,k)))
      ydn(i,my  ,k) = ( &
                     a*(f(i,my-1,k)+f(i,my  ,k)) + &
                     b*(f(i,my-2,k)+f(i,1   ,k)) + &
                     c*(f(i,my-3,k)+f(i,2   ,k)))
      ydn(i,1   ,k) = ( &
                     a*(f(i,my  ,k)+f(i,1   ,k)) + &
                     b*(f(i,my-1,k)+f(i,2   ,k)) + &
                     c*(f(i,my-2,k)+f(i,3   ,k)))
      ydn(i,2   ,k) = ( &
                     a*(f(i,1   ,k)+f(i,2   ,k)) + &
                     b*(f(i,my  ,k)+f(i,3   ,k)) + &
                     c*(f(i,my-1,k)+f(i,4   ,k)))
      ydn(i,3   ,k) = ( &
                     a*(f(i,2   ,k)+f(i,3   ,k)) + &
                     b*(f(i,1   ,k)+f(i,4   ,k)) + &
                     c*(f(i,my  ,k)+f(i,5   ,k)))
    end do
    do j=3,my-3
     do i=1,mx
      ydn(i,j+1,k) = ( &
                     a*(f(i,j   ,k)+f(i,j+1 ,k)) + &
                     b*(f(i,j-1 ,k)+f(i,j+2 ,k)) + &
                     c*(f(i,j-2 ,k)+f(i,j+3 ,k)))
     end do
    end do
  end do
!
  nflop = nflop+8
END SUBROUTINE

!***********************************************************************
SUBROUTINE ydn_add (f,ydn)
!
!  f is centered on (i,j,k), fifth order stagger
!
  use params
  real, dimension(mx,my,mz):: f, ydn
!hpf$ distribute(*,*,block):: f, ydn
!
!-----------------------------------------------------------------------
  if (my .lt. 3) then
    ydn = f
    return
  end if
  c = 3./256.
  b = -25./256.
  a = .5-b-c
  a1st = 0.5
!
!$omp parallel do private(i,j,k)
  do k=1,mz
    do i=1,mx
      ydn(i,my-1,k) = ydn(i,my-1,k) + ( &
                     a*(f(i,my-2,k)+f(i,my-1,k)) + &
                     b*(f(i,my-3,k)+f(i,my  ,k)) + &
                     c*(f(i,my-4,k)+f(i,1   ,k)))
      ydn(i,my  ,k) = ydn(i,my  ,k) + ( &
                     a*(f(i,my-1,k)+f(i,my  ,k)) + &
                     b*(f(i,my-2,k)+f(i,1   ,k)) + &
                     c*(f(i,my-3,k)+f(i,2   ,k)))
      ydn(i,1   ,k) = ydn(i,1   ,k) + ( &
                     a*(f(i,my  ,k)+f(i,1   ,k)) + &
                     b*(f(i,my-1,k)+f(i,2   ,k)) + &
                     c*(f(i,my-2,k)+f(i,3   ,k)))
      ydn(i,2   ,k) = ydn(i,2   ,k) + ( &
                     a*(f(i,1   ,k)+f(i,2   ,k)) + &
                     b*(f(i,my  ,k)+f(i,3   ,k)) + &
                     c*(f(i,my-1,k)+f(i,4   ,k)))
      ydn(i,3   ,k) = ydn(i,3   ,k) + ( &
                     a*(f(i,2   ,k)+f(i,3   ,k)) + &
                     b*(f(i,1   ,k)+f(i,4   ,k)) + &
                     c*(f(i,my  ,k)+f(i,5   ,k)))
    end do
    do j=3,my-3
     do i=1,mx
      ydn(i,j+1,k) = ydn(i,j+1,k) + ( &
                     a*(f(i,j   ,k)+f(i,j+1 ,k)) + &
                     b*(f(i,j-1 ,k)+f(i,j+2 ,k)) + &
                     c*(f(i,j-2 ,k)+f(i,j+3 ,k)))
     end do
    end do
  end do
!
  nflop = nflop+8
END SUBROUTINE

!***********************************************************************
FUNCTION zdn (f)
!
!  f is centered on (i,j,k), fifth order stagger
!
  use params
  real, dimension(mx,my,mz):: f, zdn
!hpf$ distribute(*,*,block):: f, zdn
!
!-----------------------------------------------------------------------
  c = 3./256.
  b = -25./256.
  a = .5-b-c
  a1st = 0.5
!
!$omp parallel do private(i,j,k,km1,km2,km3,kp1,kp2)
  do k=1,mz
    km3=mod(k-4+mz,mz)+1
    km2=mod(k-3+mz,mz)+1
    km1=mod(k-2+mz,mz)+1
    kp1=mod(k     ,mz)+1
    kp2=mod(k+1   ,mz)+1
    do j=1,my
      do i=1,mx
        zdn(i,j,k) = ( &
                     a*(f(i,j,k  )+f(i,j,km1)) + &
                     b*(f(i,j,kp1)+f(i,j,km2)) + &
                     c*(f(i,j,kp2)+f(i,j,km3)))
      end do
    end do
  end do
!
  nflop = nflop+8
END FUNCTION

!***********************************************************************
SUBROUTINE zdn_set (f,zdn)
!
!  f is centered on (i,j,k), fifth order stagger
!
  use params
  real, dimension(mx,my,mz):: f, zdn
!hpf$ distribute(*,*,block):: f, zdn
!
!-----------------------------------------------------------------------
  c = 3./256.
  b = -25./256.
  a = .5-b-c
  a1st = 0.5
!
!$omp parallel do private(i,j,k,km1,km2,km3,kp1,kp2)
  do k=1,mz
    km3=mod(k-4+mz,mz)+1
    km2=mod(k-3+mz,mz)+1
    km1=mod(k-2+mz,mz)+1
    kp1=mod(k     ,mz)+1
    kp2=mod(k+1   ,mz)+1
    do j=1,my
      do i=1,mx
        zdn(i,j,k) = ( &
                     a*(f(i,j,k  )+f(i,j,km1)) + &
                     b*(f(i,j,kp1)+f(i,j,km2)) + &
                     c*(f(i,j,kp2)+f(i,j,km3)))
      end do
    end do
  end do
!
  nflop = nflop+8
END SUBROUTINE

!***********************************************************************
SUBROUTINE zdn_add (f,zdn)
!
!  f is centered on (i,j,k), fifth order stagger
!
  use params
  real, dimension(mx,my,mz):: f, zdn
!hpf$ distribute(*,*,block):: f, zdn
!
!-----------------------------------------------------------------------
  c = 3./256.
  b = -25./256.
  a = .5-b-c
  a1st = 0.5
!
!$omp parallel do private(i,j,k,km1,km2,km3,kp1,kp2)
  do k=1,mz
    km3=mod(k-4+mz,mz)+1
    km2=mod(k-3+mz,mz)+1
    km1=mod(k-2+mz,mz)+1
    kp1=mod(k     ,mz)+1
    kp2=mod(k+1   ,mz)+1
    do j=1,my
      do i=1,mx
        zdn(i,j,k) = zdn(i,j,k) + ( &
                     a*(f(i,j,k  )+f(i,j,km1)) + &
                     b*(f(i,j,kp1)+f(i,j,km2)) + &
                     c*(f(i,j,kp2)+f(i,j,km3)))
      end do
    end do
  end do
!
  nflop = nflop+8
END SUBROUTINE

!***********************************************************************
FUNCTION ddxup (f)
  use params
  use mesh
  real, dimension(mx,my,mz):: f, ddxup
!hpf$ distribute(*,*,block):: f, ddxup
!
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)
  a1st = 1.
  b = b
  c = c
!
!$omp parallel do private(i,j,k)
  do k=1,mz
  do j=1,my
    ddxup(mx-2,j,k) = dxidxup(mx-2)*( &
                   a*(f(mx-1,j,k)-f(mx-2,j,k)) &
                 + b*(f(mx  ,j,k)-f(mx-3,j,k)) &
                 + c*(f(1   ,j,k)-f(mx-4,j,k)))
    ddxup(mx-1,j,k) = dxidxup(mx-1)*( &
                   a*(f(mx  ,j,k)-f(mx-1,j,k)) &
                 + b*(f(1   ,j,k)-f(mx-2,j,k)) &
                 + c*(f(2   ,j,k)-f(mx-3,j,k)))
    ddxup(mx  ,j,k) = dxidxup(mx  )*( &
                   a*(f(1   ,j,k)-f(mx  ,j,k)) &
                 + b*(f(2   ,j,k)-f(mx-1,j,k)) &
                 + c*(f(3   ,j,k)-f(mx-2,j,k)))
    ddxup(1  ,j,k) = dxidxup(   1)*( &
                   a*(f(2   ,j,k)-f(1   ,j,k)) &
                 + b*(f(3   ,j,k)-f(mx  ,j,k)) &
                 + c*(f(4   ,j,k)-f(mx-1,j,k)))
    ddxup(2  ,j,k) = dxidxup(   2)*( &
                   a*(f(3   ,j,k)-f(2  ,j,k)) &
                 + b*(f(4   ,j,k)-f(1  ,j,k)) &
                 + c*(f(5   ,j,k)-f(mx ,j,k)))
  end do
  do j=1,my
    do i=3,mx-3
      ddxup(i  ,j,k) = dxidxup(i)*( &
                   a*(f(i+1 ,j,k)-f(i   ,j,k)) &
                 + b*(f(i+2 ,j,k)-f(i-1 ,j,k)) &
                 + c*(f(i+3 ,j,k)-f(i-2 ,j,k)))
    end do
  end do
  end do
!
  nflop = nflop+8
END FUNCTION

!***********************************************************************
SUBROUTINE ddxup_set (f,ddxup)
  use params
  use mesh
  real, dimension(mx,my,mz):: f, ddxup
!hpf$ distribute(*,*,block):: f, ddxup
!
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)
  a1st = 1.
  b = b
  c = c
!
!$omp parallel do private(i,j,k)
  do k=1,mz
  do j=1,my
    ddxup(mx-2,j,k) = dxidxup(mx-2)*( &
                   a*(f(mx-1,j,k)-f(mx-2,j,k)) &
                 + b*(f(mx  ,j,k)-f(mx-3,j,k)) &
                 + c*(f(1   ,j,k)-f(mx-4,j,k)))
    ddxup(mx-1,j,k) = dxidxup(mx-1)*( &
                   a*(f(mx  ,j,k)-f(mx-1,j,k)) &
                 + b*(f(1   ,j,k)-f(mx-2,j,k)) &
                 + c*(f(2   ,j,k)-f(mx-3,j,k)))
    ddxup(mx  ,j,k) = dxidxup(mx  )*( &
                   a*(f(1   ,j,k)-f(mx  ,j,k)) &
                 + b*(f(2   ,j,k)-f(mx-1,j,k)) &
                 + c*(f(3   ,j,k)-f(mx-2,j,k)))
    ddxup(1  ,j,k) = dxidxup(   1)*( &
                   a*(f(2   ,j,k)-f(1   ,j,k)) &
                 + b*(f(3   ,j,k)-f(mx  ,j,k)) &
                 + c*(f(4   ,j,k)-f(mx-1,j,k)))
    ddxup(2  ,j,k) = dxidxup(   2)*( &
                   a*(f(3   ,j,k)-f(2  ,j,k)) &
                 + b*(f(4   ,j,k)-f(1  ,j,k)) &
                 + c*(f(5   ,j,k)-f(mx ,j,k)))
  end do
  do j=1,my
    do i=3,mx-3
      ddxup(i  ,j,k) = dxidxup(i)*( &
                   a*(f(i+1 ,j,k)-f(i   ,j,k)) &
                 + b*(f(i+2 ,j,k)-f(i-1 ,j,k)) &
                 + c*(f(i+3 ,j,k)-f(i-2 ,j,k)))
    end do
  end do
  end do
!
  nflop = nflop+8
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddxup_add (f,ddxup)
  use params
  use mesh
  real, dimension(mx,my,mz):: f, ddxup
!hpf$ distribute(*,*,block):: f, ddxup
!
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)
  a1st = 1.
  b = b
  c = c
!
!$omp parallel do private(i,j,k)
  do k=1,mz
  do j=1,my
    ddxup(mx-2,j,k) = ddxup(mx-2,j,k) + dxidxup(mx-2)*( &
                   a*(f(mx-1,j,k)-f(mx-2,j,k)) &
                 + b*(f(mx  ,j,k)-f(mx-3,j,k)) &
                 + c*(f(1   ,j,k)-f(mx-4,j,k)))
    ddxup(mx-1,j,k) = ddxup(mx-1,j,k) + dxidxup(mx-1)*( &
                   a*(f(mx  ,j,k)-f(mx-1,j,k)) &
                 + b*(f(1   ,j,k)-f(mx-2,j,k)) &
                 + c*(f(2   ,j,k)-f(mx-3,j,k)))
    ddxup(mx  ,j,k) = ddxup(mx  ,j,k) + dxidxup(  mx)*( &
                   a*(f(1   ,j,k)-f(mx  ,j,k)) &
                 + b*(f(2   ,j,k)-f(mx-1,j,k)) &
                 + c*(f(3   ,j,k)-f(mx-2,j,k)))
    ddxup(1  ,j,k) = ddxup(1  ,j,k) + dxidxup(   1)*( &
                   a*(f(2   ,j,k)-f(1   ,j,k)) &
                 + b*(f(3   ,j,k)-f(mx  ,j,k)) &
                 + c*(f(4   ,j,k)-f(mx-1,j,k)))
    ddxup(2  ,j,k) = ddxup(2  ,j,k) + dxidxup(   2)*( &
                   a*(f(3   ,j,k)-f(2  ,j,k)) &
                 + b*(f(4   ,j,k)-f(1  ,j,k)) &
                 + c*(f(5   ,j,k)-f(mx ,j,k)))
  end do
  do j=1,my
    do i=3,mx-3
      ddxup(i  ,j,k) = ddxup(i  ,j,k) + dxidxup(i)*( &
                   a*(f(i+1 ,j,k)-f(i   ,j,k)) &
                 + b*(f(i+2 ,j,k)-f(i-1 ,j,k)) &
                 + c*(f(i+3 ,j,k)-f(i-2 ,j,k)))
    end do
  end do
  end do
!
  nflop = nflop+8
END SUBROUTINE

!***********************************************************************
FUNCTION ddxup1 (f)
  use params
  use mesh
  real, dimension(mx,my,mz):: f, ddxup1
!hpf$ distribute(*,*,block):: f, ddxup1

!$omp parallel do private(i,j,k)
  do k=1,mz
  do j=1,my
    ddxup1(mx-2,j,k) = dxidxup(mx-2)*(f(mx-1,j,k)-f(mx-2,j,k))
    ddxup1(mx-1,j,k) = dxidxup(mx-1)*(f(mx  ,j,k)-f(mx-1,j,k))
    ddxup1(mx  ,j,k) = dxidxup(mx  )*(f(1   ,j,k)-f(mx  ,j,k))
    ddxup1(1   ,j,k) = dxidxup(   1)*(f(2   ,j,k)-f(1   ,j,k))
    ddxup1(2   ,j,k) = dxidxup(   2)*(f(3   ,j,k)-f(2   ,j,k))
  end do
  do j=1,my
    do i=3,mx-3
      ddxup1(i  ,j,k) = dxidxup(i)*(f(i+1 ,j,k)-f(i   ,j,k))
    end do
  end do
  end do
!
  nflop = nflop+8
END FUNCTION

!***********************************************************************
SUBROUTINE ddxup1_set (f,ddxup1)
  use params
  use mesh
  real, dimension(mx,my,mz):: f, ddxup1
!hpf$ distribute(*,*,block):: f, ddxup1

!$omp parallel do private(i,j,k)
  do k=1,mz
  do j=1,my
    ddxup1(mx-2,j,k) = dxidxup(mx-2)*(f(mx-1,j,k)-f(mx-2,j,k))
    ddxup1(mx-1,j,k) = dxidxup(mx-1)*(f(mx  ,j,k)-f(mx-1,j,k))
    ddxup1(mx  ,j,k) = dxidxup(mx  )*(f(1   ,j,k)-f(mx  ,j,k))
    ddxup1(1   ,j,k) = dxidxup(   1)*(f(2   ,j,k)-f(1   ,j,k))
    ddxup1(2   ,j,k) = dxidxup(   2)*(f(3   ,j,k)-f(2  ,j,k))
  end do
  do j=1,my
    do i=3,mx-3
      ddxup1(i  ,j,k) = dxidxup(i)*(f(i+1 ,j,k)-f(i   ,j,k))
    end do
  end do
  end do
!
  nflop = nflop+8
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddxup1_add (f,ddxup1)
  use params
  use mesh
  real, dimension(mx,my,mz):: f, ddxup1
!hpf$ distribute(*,*,block):: f, ddxup1

!$omp parallel do private(i,j,k)
  do k=1,mz
  do j=1,my
    ddxup1(mx-2,j,k) = ddxup1(mx-2,j,k) + dxidxup(mx-2)*(f(mx-1,j,k)-f(mx-2,j,k))
    ddxup1(mx-1,j,k) = ddxup1(mx-1,j,k) + dxidxup(mx-1)*(f(mx  ,j,k)-f(mx-1,j,k))
    ddxup1(mx  ,j,k) = ddxup1(mx  ,j,k) + dxidxup(mx  )*(f(1   ,j,k)-f(mx  ,j,k))
    ddxup1(1   ,j,k) = ddxup1(1   ,j,k) + dxidxup(   1)*(f(2   ,j,k)-f(1   ,j,k))
    ddxup1(2   ,j,k) = ddxup1(2   ,j,k) + dxidxup(   2)*(f(3   ,j,k)-f(2   ,j,k))
  end do
  do j=1,my
    do i=3,mx-3
      ddxup1(i ,j,k) = ddxup1(i   ,j,k) + dxidxup(i)*(f(i+1 ,j,k)-f(i   ,j,k))
    end do
  end do
  end do
!
  nflop = nflop+8
END SUBROUTINE

!***********************************************************************
FUNCTION ddxdn (f)
  use params
  use mesh
  real, dimension(mx,my,mz):: f, ddxdn
!hpf$ distribute(*,*,block):: f, ddxdn
!
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)
  a1st = 1.
  b = b
  c = c
!
!$omp parallel do private(i,j,k)
  do k=1,mz
   do j=1,my
    ddxdn(mx-1,j,k) = dxidxdn(mx-1)*( &
                   a*(f(mx-1,j,k)-f(mx-2,j,k)) &
                 + b*(f(mx  ,j,k)-f(mx-3,j,k)) &
                 + c*(f(1   ,j,k)-f(mx-4,j,k)))
    ddxdn(mx  ,j,k) = dxidxdn(mx  )*( &
                   a*(f(mx  ,j,k)-f(mx-1,j,k)) &
                 + b*(f(1   ,j,k)-f(mx-2,j,k)) &
                 + c*(f(2   ,j,k)-f(mx-3,j,k)))
    ddxdn(1   ,j,k) = dxidxdn(1   )*( &
                   a*(f(1   ,j,k)-f(mx  ,j,k)) &
                 + b*(f(2   ,j,k)-f(mx-1,j,k)) &
                 + c*(f(3   ,j,k)-f(mx-2,j,k)))
    ddxdn(2  ,j,k) = dxidxdn(2    )*( &
                   a*(f(2   ,j,k)-f(1   ,j,k)) &
                 + b*(f(3   ,j,k)-f(mx  ,j,k)) &
                 + c*(f(4   ,j,k)-f(mx-1,j,k)))
    ddxdn(3  ,j,k) = dxidxdn(3    )*( &
                   a*(f(3   ,j,k)-f(2   ,j,k)) &
                 + b*(f(4   ,j,k)-f(1   ,j,k)) &
                 + c*(f(5   ,j,k)-f(mx  ,j,k)))
   end do
   do j=1,my
     do i=3,mx-3
      ddxdn(i+1,j,k) = dxidxdn(i+1)*( &
                   a*(f(i+1 ,j,k)-f(i   ,j,k)) &
                 + b*(f(i+2 ,j,k)-f(i-1 ,j,k)) &
                 + c*(f(i+3 ,j,k)-f(i-2 ,j,k)))
    end do
   end do
  end do
!
  nflop = nflop+8
END FUNCTION

!***********************************************************************
FUNCTION ddxdn1 (f)
  use params
  use mesh
  real, dimension(mx,my,mz):: f, ddxdn1
!hpf$ distribute(*,*,block):: f, ddxdn1

!$omp parallel do private(i,j,k)
  do k=1,mz
   do j=1,my
    ddxdn1(mx-1,j,k) = dxidxdn(mx-2)*(f(mx-1,j,k)-f(mx-2,j,k))
    ddxdn1(mx  ,j,k) = dxidxdn(mx-1)*(f(mx  ,j,k)-f(mx-1,j,k))
    ddxdn1(1   ,j,k) = dxidxdn(mx  )*(f(1   ,j,k)-f(mx  ,j,k))
    ddxdn1(2   ,j,k) = dxidxdn(   1)*(f(2   ,j,k)-f(1   ,j,k))
    ddxdn1(3   ,j,k) = dxidxdn(   2)*(f(3   ,j,k)-f(2   ,j,k))
   end do
   do j=1,my
     do i=3,mx-3
      ddxdn1(i+1,j,k) = dxidxdn(i)*(f(i+1 ,j,k)-f(i   ,j,k))
    end do
   end do
  end do
!
  nflop = nflop+8
END FUNCTION

!***********************************************************************
FUNCTION ddyup (f)
  use params
  use mesh
  real, dimension(mx,my,mz):: f, ddyup
!hpf$ distribute(*,*,block):: f, ddyup
!
  if (my .lt. 3) then
    ddyup = 0.
    return
  end if
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)
  a1st = 1.
  b = b
  c = c
!
!$omp parallel do private(i,j,k)
  do k=1,mz
    do i=1,mx
      ddyup(i,my-2,k) = dyidyup(my-2)*( &
                     a*(f(i,my-1,k)-f(i,my-2,k)) + &
                     b*(f(i,my  ,k)-f(i,my-3,k)) + &
                     c*(f(i,1   ,k)-f(i,my-4,k)))
      ddyup(i,my-1,k) = dyidyup(my-1)*( &
                     a*(f(i,my  ,k)-f(i,my-1,k)) + &
                     b*(f(i,1   ,k)-f(i,my-2,k)) + &
                     c*(f(i,2   ,k)-f(i,my-3,k)))
      ddyup(i,my  ,k) = dyidyup(my  )*( &
                     a*(f(i,1   ,k)-f(i,my  ,k)) + &
                     b*(f(i,2   ,k)-f(i,my-1,k)) + &
                     c*(f(i,3   ,k)-f(i,my-2,k)))
      ddyup(i,1   ,k) = dyidyup(   1)*( &
                     a*(f(i,2   ,k)-f(i,1   ,k)) + &
                     b*(f(i,3   ,k)-f(i,my  ,k)) + &
                     c*(f(i,4   ,k)-f(i,my-1,k)))
      ddyup(i,2   ,k) = dyidyup(   2)*( &
                     a*(f(i,3   ,k)-f(i,2   ,k)) + &
                     b*(f(i,4   ,k)-f(i,1   ,k)) + &
                     c*(f(i,5   ,k)-f(i,my  ,k)))
    end do
    do j=3,my-3
     do i=1,mx
      ddyup(i,j  ,k) = dyidyup(j)*( &
                     a*(f(i,j+1 ,k)-f(i,j   ,k)) + &
                     b*(f(i,j+2 ,k)-f(i,j-1 ,k)) + &
                     c*(f(i,j+3 ,k)-f(i,j-2 ,k)))
     end do
    end do
  end do
!
  nflop = nflop+8
END FUNCTION

!***********************************************************************
SUBROUTINE ddyup_set (f,ddyup)
  use params
  use mesh
  real, dimension(mx,my,mz):: f, ddyup
!hpf$ distribute(*,*,block):: f, ddyup
!
  if (my .lt. 3) then
    ddyup = 0.
    return
  end if
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)
  a1st = 1.
  b = b
  c = c
!
!$omp parallel do private(i,j,k)
  do k=1,mz
    do i=1,mx
      ddyup(i,my-2,k) = dyidyup(my-2)*( &
                     a*(f(i,my-1,k)-f(i,my-2,k)) + &
                     b*(f(i,my  ,k)-f(i,my-3,k)) + &
                     c*(f(i,1   ,k)-f(i,my-4,k)))
      ddyup(i,my-1,k) = dyidyup(my-1)*( &
                     a*(f(i,my  ,k)-f(i,my-1,k)) + &
                     b*(f(i,1   ,k)-f(i,my-2,k)) + &
                     c*(f(i,2   ,k)-f(i,my-3,k)))
      ddyup(i,my  ,k) = dyidyup(my  )*( &
                     a*(f(i,1   ,k)-f(i,my  ,k)) + &
                     b*(f(i,2   ,k)-f(i,my-1,k)) + &
                     c*(f(i,3   ,k)-f(i,my-2,k)))
      ddyup(i,1   ,k) = dyidyup(   1)*( &
                     a*(f(i,2   ,k)-f(i,1   ,k)) + &
                     b*(f(i,3   ,k)-f(i,my  ,k)) + &
                     c*(f(i,4   ,k)-f(i,my-1,k)))
      ddyup(i,2   ,k) = dyidyup(   2)*( &
                     a*(f(i,3   ,k)-f(i,2   ,k)) + &
                     b*(f(i,4   ,k)-f(i,1   ,k)) + &
                     c*(f(i,5   ,k)-f(i,my  ,k)))
    end do
    do j=3,my-3
     do i=1,mx
      ddyup(i,j  ,k) = dyidyup(j)*( &
                     a*(f(i,j+1 ,k)-f(i,j   ,k)) + &
                     b*(f(i,j+2 ,k)-f(i,j-1 ,k)) + &
                     c*(f(i,j+3 ,k)-f(i,j-2 ,k)))
     end do
    end do
  end do
!
  nflop = nflop+8
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddyup_add (f,ddyup)
  use params
  use mesh
  real, dimension(mx,my,mz):: f, ddyup
!hpf$ distribute(*,*,block):: f, ddyup
!
  if (my .lt. 3) then
    ddyup = 0.
    return
  end if
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)
  a1st = 1.
  b = b
  c = c
!
!$omp parallel do private(i,j,k)
  do k=1,mz
    do i=1,mx
      ddyup(i,my-2,k) = ddyup(i,my-2,k) + dyidyup(my-2)*( &
                     a*(f(i,my-1,k)-f(i,my-2,k)) + &
                     b*(f(i,my  ,k)-f(i,my-3,k)) + &
                     c*(f(i,1   ,k)-f(i,my-4,k)))
      ddyup(i,my-1,k) = ddyup(i,my-1,k) + dyidyup(my-1)*( &
                     a*(f(i,my  ,k)-f(i,my-1,k)) + &
                     b*(f(i,1   ,k)-f(i,my-2,k)) + &
                     c*(f(i,2   ,k)-f(i,my-3,k)))
      ddyup(i,my  ,k) = ddyup(i,my  ,k) + dyidyup(my  )*( &
                     a*(f(i,1   ,k)-f(i,my  ,k)) + &
                     b*(f(i,2   ,k)-f(i,my-1,k)) + &
                     c*(f(i,3   ,k)-f(i,my-2,k)))
      ddyup(i,1   ,k) = ddyup(i,1   ,k) + dyidyup(   1)*( &
                     a*(f(i,2   ,k)-f(i,1   ,k)) + &
                     b*(f(i,3   ,k)-f(i,my  ,k)) + &
                     c*(f(i,4   ,k)-f(i,my-1,k)))
      ddyup(i,2   ,k) = ddyup(i,2   ,k) + dyidyup(   2)*( &
                     a*(f(i,3   ,k)-f(i,2   ,k)) + &
                     b*(f(i,4   ,k)-f(i,1   ,k)) + &
                     c*(f(i,5   ,k)-f(i,my  ,k)))
    end do
    do j=3,my-3
     do i=1,mx
      ddyup(i,j  ,k) = ddyup(i,j  ,k) + dyidyup(j)*( &
                     a*(f(i,j+1 ,k)-f(i,j   ,k)) + &
                     b*(f(i,j+2 ,k)-f(i,j-1 ,k)) + &
                     c*(f(i,j+3 ,k)-f(i,j-2 ,k)))
     end do
    end do
  end do
!
  nflop = nflop+8
END SUBROUTINE

!***********************************************************************
FUNCTION ddyup1 (f)
  use params
  use mesh
  real, dimension(mx,my,mz):: f, ddyup1
!hpf$ distribute(*,*,block):: f, ddyup1

  if (my .lt. 3) then
    ddyup1 = 0.
    return
  end if

!$omp parallel do private(i,j,k)
  do k=1,mz
    do i=1,mx
      ddyup1(i,my-2,k) = dyidyup(my-2)*(f(i,my-1,k)-f(i,my-2,k))
      ddyup1(i,my-1,k) = dyidyup(my-1)*(f(i,my  ,k)-f(i,my-1,k))
      ddyup1(i,my  ,k) = dyidyup(my  )*(f(i,1   ,k)-f(i,my  ,k))
      ddyup1(i,1   ,k) = dyidyup(   1)*(f(i,2   ,k)-f(i,1   ,k))
      ddyup1(i,2   ,k) = dyidyup(   2)*(f(i,3   ,k)-f(i,2   ,k))
    end do
    do j=3,my-3
     do i=1,mx
      ddyup1(i,j  ,k) = dyidyup(j)*(f(i,j+1 ,k)-f(i,j   ,k))
     end do
    end do
  end do
!
  nflop = nflop+8
END FUNCTION

!***********************************************************************
SUBROUTINE ddyup1_set (f,ddyup1)
  use params
  use mesh
  real, dimension(mx,my,mz):: f, ddyup1
!hpf$ distribute(*,*,block):: f, ddyup1

  if (my .lt. 3) then
    ddyup1 = 0.
    return
  end if

!$omp parallel do private(i,j,k)
  do k=1,mz
    do i=1,mx
      ddyup1(i,my-2,k) = dyidyup(my-2)*(f(i,my-1,k)-f(i,my-2,k))
      ddyup1(i,my-1,k) = dyidyup(my-1)*(f(i,my  ,k)-f(i,my-1,k))
      ddyup1(i,my  ,k) = dyidyup(my  )*(f(i,1   ,k)-f(i,my  ,k))
      ddyup1(i,1   ,k) = dyidyup(   1)*(f(i,2   ,k)-f(i,1   ,k))
      ddyup1(i,2   ,k) = dyidyup(   2)*(f(i,3   ,k)-f(i,2   ,k))
    end do
    do j=3,my-3
     do i=1,mx
      ddyup1(i,j  ,k) = dyidyup(j)*(f(i,j+1 ,k)-f(i,j   ,k))
     end do
    end do
  end do
!
  nflop = nflop+8
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddyup1_add (f,ddyup1)
  use params
  use mesh
  real, dimension(mx,my,mz):: f, ddyup1
!hpf$ distribute(*,*,block):: f, ddyup1

  if (my .lt. 3) then
    ddyup1 = 0.
    return
  end if

!$omp parallel do private(i,j,k)
  do k=1,mz
    do i=1,mx
      ddyup1(i,my-2,k) = ddyup1(i,my-2,k) + dyidyup(my-2)*(f(i,my-1,k)-f(i,my-2,k))
      ddyup1(i,my-1,k) = ddyup1(i,my-1,k) + dyidyup(my-1)*(f(i,my  ,k)-f(i,my-1,k))
      ddyup1(i,my  ,k) = ddyup1(i,my  ,k) + dyidyup(my  )*(f(i,1   ,k)-f(i,my  ,k))
      ddyup1(i,1   ,k) = ddyup1(i,1   ,k) + dyidyup(   1)*(f(i,2   ,k)-f(i,1   ,k))
      ddyup1(i,2   ,k) = ddyup1(i,2   ,k) + dyidyup(   2)*(f(i,3   ,k)-f(i,2   ,k))
    end do
    do j=3,my-3
     do i=1,mx
      ddyup1(i,j  ,k) = ddyup1(i,j  ,k) + dyidyup(j)*(f(i,j+1 ,k)-f(i,j   ,k))
     end do
    end do
  end do
!
  nflop = nflop+8
END SUBROUTINE

!***********************************************************************
FUNCTION ddydn (f) 
  use params
  use mesh
  real, dimension(mx,my,mz):: f, ddydn
!hpf$ distribute(*,*,block):: f, ddydn
! 
  if (my .lt. 3) then
    ddydn = 0.
    return         
  end if
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)
  a1st = 1.
  b = b
  c = c
! 
!$omp parallel do private(i,j,k)
  do k=1,mz        
    do i=1,mx    
      ddydn(i,my-1,k) = dyidydn(my-1)*( &
                     a*(f(i,my-1,k)-f(i,my-2,k)) + &
                     b*(f(i,my  ,k)-f(i,my-3,k)) + &
                     c*(f(i,1   ,k)-f(i,my-4,k)))
      ddydn(i,my  ,k) = dyidydn(my  )*( &
                     a*(f(i,my  ,k)-f(i,my-1,k)) + &
                     b*(f(i,1   ,k)-f(i,my-2,k)) + &
                     c*(f(i,2   ,k)-f(i,my-3,k)))
      ddydn(i,1  ,k) = dyidydn(1    )*( &
                     a*(f(i,1   ,k)-f(i,my  ,k)) + &
                     b*(f(i,2   ,k)-f(i,my-1,k)) + &
                     c*(f(i,3   ,k)-f(i,my-2,k)))                
      ddydn(i,2   ,k) = dyidydn(2    )*( &                                      
                     a*(f(i,2   ,k)-f(i,1   ,k)) + &             
                     b*(f(i,3   ,k)-f(i,my  ,k)) + &             
                     c*(f(i,4   ,k)-f(i,my-1,k)))                
      ddydn(i,3   ,k) = dyidydn(3    )*( &                                      
                     a*(f(i,3   ,k)-f(i,2   ,k)) + &             
                     b*(f(i,4   ,k)-f(i,1   ,k)) + &             
                     c*(f(i,5   ,k)-f(i,my  ,k)))                
    end do                                                       
    do j=3,my-3                                                  
     do i=1,mx                                                   
      ddydn(i,j+1 ,k) = dyidydn(j+1)*( &                                      
                     a*(f(i,j+1 ,k)-f(i,j   ,k)) + &             
                     b*(f(i,j+2 ,k)-f(i,j-1 ,k)) + &             
                     c*(f(i,j+3 ,k)-f(i,j-2 ,k)))                
     end do                                                      
    end do                                                       
  end do                                                         
!                                                                
!                                                                
  nflop = nflop+8                                                
END FUNCTION                                                     
                                                                 
!***********************************************************************
FUNCTION ddydn1 (f)
  use params
  use mesh
  real, dimension(mx,my,mz):: f, ddydn1
!hpf$ distribute(*,*,block):: f, ddydn1

  if (my .lt. 3) then
    ddydn1 = 0.
    return
  end if

!$omp parallel do private(i,j,k)
  do k=1,mz
    do i=1,mx
      ddydn1(i,1   ,k) = dyidydn(my)*(f(i,1   ,k)-f(i,my  ,k))
    end do
    do j=1,my-1
     do i=1,mx
      ddydn1(i,j+1 ,k) = dyidydn(j)*(f(i,j+1 ,k)-f(i,j   ,k))
     end do
    end do
  end do

  nflop = nflop+8
END FUNCTION

!***********************************************************************
FUNCTION ddzup (f)
  use params
  use mesh
  real, dimension(mx,my,mz):: f, ddzup
!hpf$ distribute(*,*,block):: f, ddzup
!
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)
  a1st = 1.
  b = b
  c = c
!
!$omp parallel do private(i,j,k,km1,km2,kp1,kp2,kp3)
  do k=1,mz
    km2=mod(k-3+mz,mz)+1
    km1=mod(k-2+mz,mz)+1
    kp1=mod(k     ,mz)+1
    kp2=mod(k+1   ,mz)+1
    kp3=mod(k+2   ,mz)+1
    do j=1,my
      do i=1,mx
        ddzup(i,j,k) = dzidzup(k)*( &
                       a*(f(i,j,kp1)-f(i,j,k  )) + &
                       b*(f(i,j,kp2)-f(i,j,km1)) + &
                       c*(f(i,j,kp3)-f(i,j,km2)))
      end do
    end do
  end do
!
  nflop = nflop+8
END FUNCTION

!***********************************************************************
SUBROUTINE ddzup_set (f,ddzup)
  use params
  use mesh
  real, dimension(mx,my,mz):: f, ddzup
!hpf$ distribute(*,*,block):: f, ddzup
!
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)
  a1st = 1.
  b = b
  c = c
!
!$omp parallel do private(i,j,k,km1,km2,kp1,kp2,kp3)
  do k=1,mz
    km2=mod(k-3+mz,mz)+1
    km1=mod(k-2+mz,mz)+1
    kp1=mod(k     ,mz)+1
    kp2=mod(k+1   ,mz)+1
    kp3=mod(k+2   ,mz)+1
    do j=1,my
      do i=1,mx
        ddzup(i,j,k) = dzidzup(k)*( &
                       a*(f(i,j,kp1)-f(i,j,k  )) + &
                       b*(f(i,j,kp2)-f(i,j,km1)) + &
                       c*(f(i,j,kp3)-f(i,j,km2)))
      end do
    end do
  end do
!
  nflop = nflop+8
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddzup_add (f,ddzup)
  use params
  use mesh
  real, dimension(mx,my,mz):: f, ddzup
!hpf$ distribute(*,*,block):: f, ddzup
!
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)
  a1st = 1.
  b = b
  c = c
!
!$omp parallel do private(i,j,k,km1,km2,kp1,kp2,kp3)
  do k=1,mz
    km2=mod(k-3+mz,mz)+1
    km1=mod(k-2+mz,mz)+1
    kp1=mod(k     ,mz)+1
    kp2=mod(k+1   ,mz)+1
    kp3=mod(k+2   ,mz)+1
    do j=1,my
      do i=1,mx
        ddzup(i,j,k) = ddzup(i,j,k) + dzidzup(k)*( &
                       a*(f(i,j,kp1)-f(i,j,k  )) + &
                       b*(f(i,j,kp2)-f(i,j,km1)) + &
                       c*(f(i,j,kp3)-f(i,j,km2)))
      end do
    end do
  end do
!
  nflop = nflop+8
END SUBROUTINE

!***********************************************************************
FUNCTION ddzup1 (f)
  use params
  use mesh
  real, dimension(mx,my,mz):: f, ddzup1
!hpf$ distribute(*,*,block):: f, ddzup1

!$omp parallel do private(i,j,k,km1,km2,kp1,kp2,kp3)
  do k=1,mz
    km2=mod(k-3+mz,mz)+1
    km1=mod(k-2+mz,mz)+1
    kp1=mod(k     ,mz)+1
    kp2=mod(k+1   ,mz)+1
    kp3=mod(k+2   ,mz)+1
    do j=1,my
      do i=1,mx
        ddzup1(i,j,k) = dzidzup(k)*(f(i,j,kp1)-f(i,j,k  ))
      end do
    end do
  end do
!
  nflop = nflop+8
END FUNCTION

!***********************************************************************
SUBROUTINE ddzup1_set (f,ddzup1)
  use params
  use mesh
  real, dimension(mx,my,mz):: f, ddzup1
!hpf$ distribute(*,*,block):: f, ddzup1

!$omp parallel do private(i,j,k,km1,km2,kp1,kp2,kp3)
  do k=1,mz
    km2=mod(k-3+mz,mz)+1
    km1=mod(k-2+mz,mz)+1
    kp1=mod(k     ,mz)+1
    kp2=mod(k+1   ,mz)+1
    kp3=mod(k+2   ,mz)+1
    do j=1,my
      do i=1,mx
        ddzup1(i,j,k) = dzidzup(k)*(f(i,j,kp1)-f(i,j,k  ))
      end do
    end do
  end do
!
  nflop = nflop+8
END SUBROUTINE

!***********************************************************************
SUBROUTINE ddzup1_add (f,ddzup1)
  use params
  use mesh
  real, dimension(mx,my,mz):: f, ddzup1
!hpf$ distribute(*,*,block):: f, ddzup1

!$omp parallel do private(i,j,k,km1,km2,kp1,kp2,kp3)
  do k=1,mz
    km2=mod(k-3+mz,mz)+1
    km1=mod(k-2+mz,mz)+1
    kp1=mod(k     ,mz)+1
    kp2=mod(k+1   ,mz)+1
    kp3=mod(k+2   ,mz)+1
    do j=1,my
      do i=1,mx
        ddzup1(i,j,k) = ddzup1(i,j,k) + dzidzup(k)*(f(i,j,kp1)-f(i,j,k  ))
      end do
    end do
  end do
!
  nflop = nflop+8
END SUBROUTINE

!***********************************************************************
FUNCTION ddzdn (f)
  use params
  use mesh
  real, dimension(mx,my,mz):: f, ddzdn
!hpf$ distribute(*,*,block):: f, ddzdn
!
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)
  a1st = 1.
  b = b
  c = c
!
!$omp parallel do private(i,j,k,km1,km2,km3,kp1,kp2)
  do k=1,mz
    km3=mod(k-4+mz,mz)+1
    km2=mod(k-3+mz,mz)+1
    km1=mod(k-2+mz,mz)+1
    kp1=mod(k     ,mz)+1
    kp2=mod(k+1   ,mz)+1
    do j=1,my
      do i=1,mx
        ddzdn(i,j,k) = dzidzdn(k)*( &
                       a*(f(i,j,k  )-f(i,j,km1)) + &
                       b*(f(i,j,kp1)-f(i,j,km2)) + &
                       c*(f(i,j,kp2)-f(i,j,km3)))
      end do
    end do
  end do
!
  nflop = nflop+8
END FUNCTION

!***********************************************************************
FUNCTION ddzdn1 (f)
  use params
  use mesh
  real, dimension(mx,my,mz):: f, ddzdn1
!hpf$ distribute(*,*,block):: f, ddzdn1

!$omp parallel do private(i,j,k,km1,km2,km3,kp1,kp2)
  do k=1,mz
    km1=mod(k-2+mz,mz)+1
    do j=1,my
      do i=1,mx
        ddzdn1(i,j,k) = dzidzdn(km1)*(f(i,j,k  )-f(i,j,km1))
      end do
    end do
  end do
!
  nflop = nflop+8
END FUNCTION

!***********************************************************************
FUNCTION smooth3 (f)
!
!  Three point smooth
!
  USE params
  implicit none
  integer i,j,k,km1,kp1
  real, dimension(mx,my,mz):: f, smooth3
!hpf$ distribute(*,*,block):: f, smooth3
  real, dimension(0:mx+1):: fx
  real, dimension(0:my+1):: fy
  real, dimension(mx,my,mz):: fz
!hpf$ distribute(*,*,block):: fz
  real c3
!-----------------------------------------------------------------------
!
  c3=1./3.
!$omp parallel do private(i,j,k,fx)
  do k=1,mz
  do j=1,my
    do i=1,mx
      fx(i)=f(i,j,k)
    enddo
    fx(0)=fx(mx)
    fx(mx+1)=fx(1)
    do i=1,mx
      fz(i,j,k)=c3*(fx(i-1)+fx(i)+fx(i+1))
    end do
  end do
  end do
!$omp parallel do private(i,j,k,fy)
  do k=1,mz
  do i=1,mx
    do j=1,my
      fy(j)=fz(i,j,k)
    enddo
    fy(0)=fy(my)
    fy(my+1)=fy(1)
    do j=1,my
      fz(i,j,k)=c3*(fy(j-1)+fy(j)+fy(j+1))
    end do
   end do
  end do
!$omp parallel do private(i,j,k,km1,kp1)
  do k=1,mz
   km1=mod(k+mz-2,mz)+1
   kp1=mod(k+mz  ,mz)+1
   do j=1,my
    do i=1,mx
      smooth3(i,j,k)=c3*(fz(i,j,k)+fz(i,j,km1)+fz(i,j,kp1))
    enddo
   enddo
  enddo
END FUNCTION

!***********************************************************************
FUNCTION max3 (f)
!
!  Three point max
!
  USE params
  implicit none
  integer i,j,k,km1,kp1
  real, dimension(mx,my,mz):: f, max3
!hpf$ distribute(*,*,block):: f, max3
  real, dimension(0:mx+1):: fx
  real, dimension(0:my+1):: fy
  real, dimension(mx,my,mz):: fz
!hpf$ distribute(*,*,block):: fz
!-----------------------------------------------------------------------
!
!$omp parallel do private(i,j,k,fx)
  do k=1,mz
  do j=1,my
    do i=1,mx
      fx(i)=f(i,j,k)
    enddo
    fx(0)=fx(mx)
    fx(mx+1)=fx(1)
    do i=1,mx
      fz(i,j,k)=amax1(fx(i-1),fx(i),fx(i+1))
    end do
  end do
  end do
!$omp parallel do private(i,j,k,fy)
  do k=1,mz
  do i=1,mx
    do j=1,my
      fy(j)=fz(i,j,k)
    enddo
    fy(0)=fy(my)
    fy(my+1)=fy(1)
    do j=1,my
      fz(i,j,k)=amax1(fy(j-1),fy(j),fy(j+1))
    end do
   end do
  end do
!$omp parallel do private(i,j,k,km1,kp1)
  do k=1,mz
    km1=mod(k+mz-2,mz)+1
    kp1=mod(k+mz  ,mz)+1
    do j=1,my
    do i=1,mx
      max3(i,j,k)=amax1(fz(i,j,k),fz(i,j,km1),fz(i,j,kp1))
    enddo
    enddo
  enddo
END FUNCTION

!***********************************************************************
FUNCTION max5 (f)
!
!  Three point max
!
  USE params
  implicit none
  integer i,j,k,km1,kp1,km2,kp2
  real, dimension(mx,my,mz), intent(in) :: f
!hpf$ distribute(*,*,block) :: f
  real, dimension(mx,my,mz):: max5
!hpf$ distribute(*,*,block):: max5
  real, dimension(-1:mx+2):: fx
  real, dimension(-1:my+2):: fy
  real, dimension(mx,my,mz):: fz
!hpf$ distribute(*,*,block):: fz
!-----------------------------------------------------------------------
!
!$omp parallel do private(i,j,k,fx)
  do k=1,mz
  do j=1,my
    do i=1,mx
      fx(i)=f(i,j,k)
    enddo
    fx(  -1)=fx(mx-1)
    fx(   0)=fx(mx  )
    fx(mx+1)=fx(   1)
    fx(mx+2)=fx(   2)
    do i=1,mx
      fz(i,j,k)=amax1(fx(i-2),fx(i-1),fx(i),fx(i+1),fx(i+2))
    end do
  end do
  end do
!$omp parallel do private(i,j,k,fy)
  do k=1,mz
  do i=1,mx
    do j=1,my
      fy(j)=fz(i,j,k)
    enddo
    fy(  -1)=fy(my-1)
    fy(   0)=fy(my  )
    fy(my+1)=fy(   1)
    fy(my+2)=fy(   2)
    do j=1,my
      fz(i,j,k)=amax1(fy(j-2),fy(j-1),fy(j),fy(j+1),fy(j+2))
    end do
   end do
  end do
!$omp parallel do private(i,j,k,km1,kp1,km2,kp2)
  do k=1,mz
    km2=mod(k+mz-3,mz)+1
    km1=mod(k+mz-2,mz)+1
    kp1=mod(k+mz  ,mz)+1
    kp2=mod(k+mz+1,mz)+1
    do j=1,my
    do i=1,mx
      max5(i,j,k)=amax1(fz(i,j,km2),fz(i,j,km1),fz(i,j,k),fz(i,j,kp1),fz(i,j,kp2))
    enddo
    enddo
  enddo
END FUNCTION

!***********************************************************************
SUBROUTINE dif2_set (nx, ny, nz, fx, fy, fz, difu)
!
!  Finite difference one zone left to one zones right.
!

  USE params
  use mesh
  implicit none

  integer :: i, j, k, nx, ny, nz
  real, dimension(nx,ny,nz):: fx, fy, fz, difu
!hpf$ align with TheCube:: fx, fy, fz, difu
  real, dimension(nx):: cx
  real, dimension(ny):: cy
  real, dimension(nz):: cz
!-----------------------------------------------------------------------
  cx(1)  = 0.5/(xm(mx )-xm(mx-1))
  do i=2,mx-1
   cx(i) = 1.0/(xm(i+1)-xm(i-1 ))
  end do
  cx(mx) = 0.5/(xm(  2)-xm(   1))

  cy(1)  = 0.5/(ym(my )-ym(my-1))
  do j=2,my-1
   cy(j) = 1.0/(ym(j+1)-ym(j-1 ))
  end do
  cy(my) = 0.5/(ym(  2)-ym(   1))

  cz(1)  = 0.5/(zm(mz )-zm(mz-1))
  do k=2,mz-1
   cz(k) = 1.0/(zm(k+1)-zm(k-1 ))
  end do
  cz(mz) = 0.5/(zm(  2)-zm(   1))

  do k=1,mz
    do j=1,my
      do i=2,mx-1
        difu(i,j,k) = cx( i)*(fx(i-1 ,j,k)-fx(i+1,j,k))
      end do
      difu(1 ,j,k)  = cx( 1)*(fx(mx  ,j,k)-fx(2  ,j,k))
      difu(mx,j,k)  = cx(mx)*(fx(mx-1,j,k)-fx(1  ,j,k))
    end do
  end do

  do k=1,mz
    do j=2,my-1
      do i=1,mx
        difu(i,j, k) = difu(i,j, k) + cy( j)*(fy(i,j -1,k)-fy(i,j+1,k))
      end do
    end do
    do i=1,mx
        difu(i, 1,k) = difu(i, 1,k) + cy( 1)*(fy(i,my  ,k)-fy(i,  2,k))
        difu(i,my,k) = difu(i,my,k) + cy(my)*(fy(i,my-1,k)-fy(i,  1,k))
    end do
  end do

  do j=1,my
    do i=1,mx
        difu(i,j, 1) = difu(i,j,1 ) + cz( 1)*(fz(i,j,mz  )-fz(i,j,  2))
        difu(i,j,mz) = difu(i,j,mz) + cz(mz)*(fz(i,j,mz-1)-fz(i,j,  1))
    end do
  end do
  do k=2,mz-1
    do j=1,my
      do i=1,mx
        difu(i,j,k ) = difu(i,j,k ) + cz( k)*(fz(i,j,k -1)-fz(i,j,k+1))
      end do
    end do
  end do
 END subroutine

!-----------------------------------------------------------------------
function laplace (f) ! Only valid for equidistant mesh
 use params
 implicit none

 real, dimension(mx,my,mz) :: f, laplace
 real, dimension(mx)       :: dx2
 real, dimension(my)       :: dy2
 real, dimension(mz)       :: dz2
 real                      :: c0, c1, c2, c3
 integer                   :: i, j, k
 intent(in)                :: f
 
 dx2 =  1./dx**2
 dy2 =  1./dy**2
 dz2 =  1./dz**2
 
 c0 = -49.0/18.0
 c1 =   3.0/ 2.0
 c2 =  -3.0/20.0
 c3 =   1.0/90.0

!   d^2/dx^2
  do k=1,mz
    do j=1,my
      do i=4,mx-3
        laplace(i,j,k) = dx2(i)*                        &
                        ( c0*f(i,j,k)                   &
                         +c1*(f(i-1  ,j,k)+f(i+1 ,j,k)) &
                         +c2*(f(i-2  ,j,k)+f(i+2 ,j,k)) &
                         +c3*(f(i-3  ,j,k)+f(i+3 ,j,k)))
      end do
      laplace(1   ,j,k) = dx2(1) *                      &
                         ( c0*f(1,j,k)                  &
                          +c1*(f(mx  ,j,k)+f(2   ,j,k)) &
                          +c2*(f(mx-1,j,k)+f(3   ,j,k)) &
                          +c3*(f(mx-2,j,k)+f(4   ,j,k)))
      laplace(2   ,j,k) = dx2(2) *                      &
                         ( c0*f(2,j,k)                  &
                          +c1*(f(1   ,j,k)+f(3   ,j,k)) &
                          +c2*(f(mx  ,j,k)+f(4   ,j,k)) &
                          +c3*(f(mx-1,j,k)+f(5   ,j,k)))
      laplace(3   ,j,k) = dx2(3) *                      &
                         ( c0*f(3,j,k)                  &
                          +c1*(f(2   ,j,k)+f(4   ,j,k)) &
                          +c2*(f(1   ,j,k)+f(5   ,j,k)) &
                          +c3*(f(mx  ,j,k)+f(6   ,j,k)))
      laplace(mx-2,j,k) = dx2(mx-2) *                   &
                         ( c0*f(mx-2,j,k)               &
                          +c1*(f(mx-3,j,k)+f(mx-1,j,k)) &
                          +c2*(f(mx-4,j,k)+f(mx  ,j,k)) &
                          +c3*(f(mx-5,j,k)+f(1   ,j,k)))
      laplace(mx-1,j,k) = dx2(mx-1) *                   &
                         ( c0*f(mx-1,j,k)               &
                          +c1*(f(mx-2,j,k)+f(mx  ,j,k)) &
                          +c2*(f(mx-3,j,k)+f(1   ,j,k)) &
                          +c3*(f(mx-4,j,k)+f(2   ,j,k)))
      laplace(mx  ,j,k) = dx2(mx) *                     &
                         ( c0*f(mx,j,k)                 &
                          +c1*(f(mx-1,j,k)+f(1   ,j,k)) &
                          +c2*(f(mx-2,j,k)+f(2   ,j,k)) &
                          +c3*(f(mx-3,j,k)+f(3   ,j,k)))
    end do
  end do
! + d^2/dy^2
  do k=1,mz
    do j=4,my-3
      do i=1,mx
        laplace(i,j,k) = laplace(i,j,k)                 &
                        +dy2(j)*                        &
                        ( c0*f(i,j,k)                   &
                         +c1*(f(i,j-1  ,k)+f(i,j+1 ,k)) &
                         +c2*(f(i,j-2  ,k)+f(i,j+2 ,k)) &
                         +c3*(f(i,j-3  ,k)+f(i,j+3 ,k)))
      end do
    end do
    do i=1,mx
      laplace(i,1   ,k) = laplace(i,1,k)                &
                         +dy2(1)*                       &
                         ( c0*f(i,1,k)                  &
                          +c1*(f(i,my  ,k)+f(i,2   ,k)) &
                          +c2*(f(i,my-1,k)+f(i,3   ,k)) &
                          +c3*(f(i,my-2,k)+f(i,4   ,k)))
      laplace(i,2   ,k) = laplace(i,2,k)                &
                         +dy2(2)*                       &
                         ( c0*f(i,2,k)                  &
                          +c1*(f(i,1   ,k)+f(i,3   ,k)) &
                          +c2*(f(i,my  ,k)+f(i,4   ,k)) &
                          +c3*(f(i,my-1,k)+f(i,5   ,k)))
      laplace(i,3   ,k) = laplace(i,3,k)                &
                         +dy2(3)*                       &
                         ( c0*f(i,3,k)                  &
                          +c1*(f(i,2   ,k)+f(i,4   ,k)) &
                          +c2*(f(i,1   ,k)+f(i,5   ,k)) &
                          +c3*(f(i,my  ,k)+f(i,6   ,k)))
      laplace(i,my-2,k) = laplace(i,my-2,k)             &
                         +dy2(my-2)*                    &
                         ( c0*f(i,my-2,k)               &
                          +c1*(f(i,my-3,k)+f(i,my-1,k)) &
                          +c2*(f(i,my-4,k)+f(i,my  ,k)) &
                          +c3*(f(i,my-5,k)+f(i,1   ,k)))
      laplace(i,my-1,k) = laplace(i,my-1,k)             &
                         +dy2(my-1)*                    &
                         ( c0*f(i,my-1,k)               &
                          +c1*(f(i,my-2,k)+f(i,my  ,k)) &
                          +c2*(f(i,my-3,k)+f(i,1   ,k)) &
                          +c3*(f(i,my-4,k)+f(i,2   ,k)))
      laplace(i,my  ,k) = laplace(i,my,k)               &
                         +dy2(my)*                      &
                         ( c0*f(i,my,k)                 &
                          +c1*(f(i,my-1,k)+f(i,1   ,k)) &
                          +c2*(f(i,my-2,k)+f(i,2   ,k)) &
                          +c3*(f(i,my-3,k)+f(i,3   ,k)))
    end do
  end do
!   d^2/dz^2
  do j=1,my
    do i=1,mx
      laplace(i,j,1   ) = laplace(i,j,1)                &
                         +dz2(1)*                       &
                         ( c0*f(i,j,1)                  &
                          +c1*(f(i,j,mz  )+f(i,j,2   )) &
                          +c2*(f(i,j,mz-1)+f(i,j,3   )) &
                          +c3*(f(i,j,mz-2)+f(i,j,4   )))
      laplace(i,j,2   ) = laplace(i,j,2)                &
                         +dz2(2)*                       &
                         ( c0*f(i,j,2)                  &
                          +c1*(f(i,j,1   )+f(i,j,3   )) &
                          +c2*(f(i,j,mz  )+f(i,j,4   )) &
                          +c3*(f(i,j,mz-1)+f(i,j,5   )))
      laplace(i,j,3   ) = laplace(i,j,3)                &
                         +dz2(3)*                       &
                         ( c0*f(i,j,3)                  &
                          +c1*(f(i,j,2   )+f(i,j,4   )) &
                          +c2*(f(i,j,1   )+f(i,j,5   )) &
                          +c3*(f(i,j,mz  )+f(i,j,6   )))
      laplace(i,j,mz-2) = laplace(i,j,mz-2)             &
                         +dz2(mz-2)*                    &
                         ( c0*f(i,j,mz-2)               &
                          +c1*(f(i,j,mz-3)+f(i,j,mz-1)) &
                          +c2*(f(i,j,mz-4)+f(i,j,mz  )) &
                          +c3*(f(i,j,mz-5)+f(i,j,1   )))
      laplace(i,j,mz-1) = laplace(i,j,mz-1)             &
                         +dz2(mz-1)*                    &
                         ( c0*f(i,j,mz-1)               &
                          +c1*(f(i,j,mz-2)+f(i,j,mz  )) &
                          +c2*(f(i,j,mz-3)+f(i,j,1   )) &
                          +c3*(f(i,j,mz-4)+f(i,j,2   )))
      laplace(i,j,mz  ) = laplace(i,j,mz)               &
                         +dz2(mz)*                      &
                         ( c0*f(i,j,mz)                 &
                          +c1*(f(i,j,mz-1)+f(i,j,1   )) &
                          +c2*(f(i,j,mz-2)+f(i,j,2   )) &
                          +c3*(f(i,j,mz-3)+f(i,j,3   )))
    end do
  end do
  do k=4,mz-3
    do j=1,my
      do i=1,mx
        laplace(i,j,k) = laplace(i,j,k)                 &
                        +dz2(k)*                        &
                        ( c0*f(i,j,k)                   &
                         +c1*(f(i,j,k-1  )+f(i,j,k+1 )) &
                         +c2*(f(i,j,k-2  )+f(i,j,k+2 )) &
                         +c3*(f(i,j,k-3  )+f(i,j,k+3 )))
      end do
    end do
  end do

end function laplace

!-----------------------------------------------------------------------
SUBROUTINE difxyz_set (nx, ny, nz, fx, fy, fz, difu)
  USE params
  real, dimension(nx,ny,nz):: fx, fy, fz, difu
!hpf$ align with TheCube:: fx, fy, fz, difu

   difu = cshift(fx,dim=1,shift=-1) - cshift(fx,dim=1,shift=1) &
        + cshift(fy,dim=2,shift=-1) - cshift(fy,dim=2,shift=1) &
        + cshift(fz,dim=3,shift=-1) - cshift(fz,dim=3,shift=1)
END subroutine
!-----------------------------------------------------------------------


SUBROUTINE init_stagger
END SUBROUTINE

SUBROUTINE init_derivs
END SUBROUTINE
END MODULE
