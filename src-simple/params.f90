! $Id: params.f90,v 1.11 2003/04/02 00:30:48 aake Exp $
!***********************************************************************
MODULE params

! Parameters and scalars for the induction equation code

  real   , parameter :: pi=3.1415926536, g=6.644e-05
  real*8 , parameter :: cdu=1.d-24, clu=3.0857d18, ctu=3.1557d13
  real   , parameter :: cvu=clu/ctu, cmu=((cdu*clu)*clu)*clu, msun=1.989e33  
  real*8 , parameter :: ceu=((((cdu*cvu**2)*clu)*clu)*clu)

  character (len=40) file
  integer :: mx, my, mz, mw, mvar
  integer :: lb, ub
  integer :: it, nstep, nsnap, isnap, iread, iwrite, idbg, iseed, substep
  logical :: do_density, do_momenta, do_energy, do_mhd, do_pscalar, do_loginterp, do_cooling, do_ionization
  real :: sx, sy, sz
  real :: dx, dy, dz, nu1, nu2, t, dt, dtold, Cdt, gamma, grav
  real :: Cu, Cv, Cn, Cr, Ce, Cd, Cb

PUBLIC init

CONTAINS

!***********************************************************************
SUBROUTINE init

  implicit none

  namelist /io/iread,iwrite,nstep,nsnap,file
  namelist /run/t,dt,Cdt,iseed,idbg
  namelist /grid/mx,my,mz,sx,sy,sz
  namelist /flags/do_density,do_momenta,do_energy,do_mhd,do_loginterp,do_pscalar,do_ionization
  namelist /pde/nu1,nu2,gamma,grav

!-----------------------------------------------------------------------
!  I/O control
!-----------------------------------------------------------------------
  iread = -1                                ! snap shot counter
  iwrite = -1                               ! snap shot counter
  nstep = 1001                              ! number of time steps
  nsnap = 10                                ! snap shot every nsnap
  file  = 'snapshot.dat'                    ! snapshot file

  read  (*,io)                              ! read namelist
  if (iread .ge. 0) then                    ! if iread was changed in namelist
    isnap = iread-1                         ! set snap shot counter to write there
  else                                      ! if unchanged from initial
    isnap = -1                              ! set to write from 0
  end if
  if (iwrite .ge. 0) then                   ! if iwrite was changed in namelist
    isnap = iwrite-1                        ! set snap shot counter to write there
  end if
  write (*,io)                              ! print namelist

!-----------------------------------------------------------------------
!  Run control
!-----------------------------------------------------------------------
  t     = 0.                                ! initial time
  dt    = 0.00001                           ! initial time step
  Cdt   = 0.4                               ! Courant condition
  idbg  = 0                                 ! debug printut?
  iseed = 77                                ! random number seed

  read  (*,run)                             ! read namelist
  write (*,run)                             ! print namelist

!-----------------------------------------------------------------------
!  Flags
!-----------------------------------------------------------------------
  do_loginterp = .true.                     ! conservative choice
  do_pscalar = .true.                       ! passive scalar?
  do_density = .true.                       ! continuuty equation?
  do_momenta = .true.                       ! equations of motion?
  do_energy = .true.                        ! energy equation?
  do_mhd = .false.                          ! mhd equations?
  do_cooling = .false.                      ! cooling?
  do_ionization = .false.                   ! ionization?

  read  (*,flags)                           ! read namelist
  write (*,flags)                           ! print namelist

  mvar = 0                                  ! cound I/O variables
  if (do_density) mvar = mvar+1
  if (do_momenta) mvar = mvar+3
  if (do_energy ) mvar = mvar+1
  if (do_pscalar) mvar = mvar+1
  if (do_mhd    ) mvar = mvar+3

!-----------------------------------------------------------------------
!  Grid parameters
!-----------------------------------------------------------------------
  mx    = 33	                            ! avoid powers of two
  my    = mx	                            ! powers of two OK
  mz    = mx	                            ! powers of two OK
  sx    = 1.                                ! grid size
  sy    = sx                                ! grid size
  sz    = sx                                ! grid size

  read  (*,grid)                            ! read namelist
  dx=sx/mx ; dy=sy/my ; dz=sz/mz            ! mesh size
  write (*,grid)                            ! print namelist
  mw = mx*my*mz                             ! total words per chunk

!-----------------------------------------------------------------------
!  PDE parameters
!-----------------------------------------------------------------------
  gamma = 1.                                ! gas gamma
  nu1 = 0.025                               ! normal viscosity
  nu2 = 1.0                                 ! shock viscosity
  grav = 0.                                 ! vertical acc. of gravity
  read  (*,pde)                             ! read namelist
  write (*,pde)                             ! print namelist

END SUBROUTINE init

END MODULE params
