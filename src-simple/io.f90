! $Id: io.f90,v 1.8 2003/04/01 22:19:40 aake Exp $
!***********************************************************************
SUBROUTINE read_snap (r,px,py,pz,e,d,Bx,By,Bz)
!
!  Read snap shot
!
  USE params
  implicit none
  logical exists
  integer :: iv, i, lrec
  real, dimension(mx,my,mz) :: &
        r,px,py,pz,e,d,Bx,By,Bz
!hpf$ distribute(*,*,block):: &
!hpf$   r,px,py,pz,e,d,Bx,By,Bz
!-------------------------------------------------------

  i = index(file,'.')                                   ! file name root
  inquire (file=file(1:i)//'tim',exist=exists) 	        ! does file.tim exist?
  if (exists) then                                      ! if exists
    open (2, file=file(1:i)//'tim', &                   ! open time info file
     status='old', form='unformatted', &                ! unkn, unformatted
     access='direct', recl=lrec(3))                     ! direct access
    read (2,rec=isnap+1) t, dt, iseed                   ! time and seed for restart
    close(2)
    print *,'recovered time info:', t, dt, iseed        ! show time info
  end if

  i = index(file,' ')-1                                 ! necessary on Windows
  print *,'reading snapshot:', isnap, t, file(1:i)      ! info
  open (2, file=file(1:i), &                            ! open snap shot file
   status='old', form='unformatted', &                  ! old, unformatted
   access='direct', recl=lrec(mx*my*mz))                ! direct access

  iv = 1                                                ! variable number
  if (do_density) then
    read  (2,rec=isnap*mvar+iv) r                       ! density
    iv = iv+1
  end if
  if (do_momenta) then
    print *,'..momenta'
    read  (2,rec=isnap*mvar+iv) px                      ! x-momentum
    iv = iv+1
    read  (2,rec=isnap*mvar+iv) py                      ! y-momentum
    iv = iv+1
    read  (2,rec=isnap*mvar+iv) pz                      ! z-momentum
    iv = iv+1
  end if
  if (do_energy) then
    print *,'..energy'
    read  (2,rec=isnap*mvar+iv) e                       ! energy
    iv = iv+1
  end if
  if (do_mhd) then
    print *,'..magnetic field'
    read  (2,rec=isnap*mvar+iv) Bx                      ! Bx
    iv = iv+1
    read  (2,rec=isnap*mvar+iv) By                      ! By
    iv = iv+1
    read  (2,rec=isnap*mvar+iv) Bz                      ! Bz
    iv = iv+1
  end if
  if (do_pscalar) then
    print *,'..passive scalar'
    read  (2,rec=isnap*mvar+iv) d                       ! metallicity
  end if
  close (2)
END

!***********************************************************************
SUBROUTINE write_snap (r,px,py,pz,e,d,Bx,By,Bz)
!
!  Write snap shot
!
  USE params
  USE stagger
  USE eos
  implicit none
  integer :: iv, i, lrec
  real, dimension(mx,my,mz) :: &
        r,px,py,pz,e,d,Bx,By,Bz
!hpf$ distribute(*,*,block):: &
!hpf$   r,px,py,pz,e,d,Bx,By,Bz
  real, allocatable, dimension(:,:,:):: pp,ee,tt,xdnr,ydnr,zdnr,Ux,Uy,Uz
!-------------------------------------------------------

  i = index(file,' ')-1
  open (2, file=file(1:i), &                		! open snap shot file
   status='unknown', form='unformatted', &              ! unkn, unformatted
   access='direct', recl=lrec(mx*my*mz))                ! direct access
  iv = 1
  if (do_density) then
    write  (2,rec=isnap*mvar+iv) r                      ! density
    iv = iv+1
  end if
  if (do_momenta) then
    call density_boundary(r)
    allocate (xdnr(mx,my,mz),ydnr(mx,my,mz),zdnr(mx,my,mz))
    allocate (  Ux(mx,my,mz),  Uy(mx,my,mz),  Uz(mx,my,mz))
    xdnr = xdn(r)
    ydnr = ydn(r)
    zdnr = zdn(r)
    Ux = px/xdnr
    Uy = py/ydnr
    Uz = pz/zdnr
    call velocity_boundary(r,xdnr,ydnr,zdnr,Ux,Uy,Uz,px,py,pz,e,Bx,By,Bz)
    write  (2,rec=isnap*mvar+iv) px                     ! x-momentum
    iv = iv+1
    write  (2,rec=isnap*mvar+iv) py                     ! y-momentum
    iv = iv+1
    write  (2,rec=isnap*mvar+iv) pz                     ! z-momentum
    iv = iv+1
  end if
  if (do_energy) then
    write  (2,rec=isnap*mvar+iv) e                      ! energy
    iv = iv+1
  end if
  if (do_ionization) then
    allocate (pp(mx,my,mz),ee(mx,my,mz),tt(mx,my,mz))
    ee=e/r
    pp = pressure(r,ee,tt)
    write  (2,rec=isnap*mvar+iv) tt                     ! temperature
    iv = iv+1
    deallocate (pp,ee,tt)
  end if
  if (do_mhd) then
    call mfield_boundary (px,py,pz,Bx,By,Bz)
    write  (2,rec=isnap*mvar+iv) Bx                     ! Bx
    iv = iv+1
    write  (2,rec=isnap*mvar+iv) By                     ! By
    iv = iv+1
    write  (2,rec=isnap*mvar+iv) Bz                     ! Bz
  end if
  if (do_pscalar) then
    write  (2,rec=isnap*mvar+iv) d                      ! metallicity
  end if
  close (2)
  print *,'snap shot:', isnap, t, dt, iseed

!-------------------------------------------------------
!  Write small recovery file with data needed to restart
!-------------------------------------------------------
  i = index(file,'.')
  open (2, file=file(1:i)//'tim', &
   status='unknown', form='unformatted', &
   access='direct', recl=lrec(3))
  write (2,rec=isnap+1) t, dt, iseed
  close(2)

!-------------------------------------------------------
!  Write text file intended for IDL procedure open.pro
!-------------------------------------------------------
  open (2, file=file(1:index(file,'.')-1)//'.dx', &
    form='formatted')
  write (2,'(3i5)') mx,my,mz
  write (2,'(3(1pe14.6))') dx,dy,dz
  write (2,*) gamma,0
  write (2,*) mvar
  close (2)

END

!***********************************************************************
SUBROUTINE print_time (cpu)
!
!  Print time step information on stdout and in file.time
!
  USE params
  implicit none
  real cpu(2), kzs
!-----------------------------------------------------------------------
  kzs = mx*my*mz*3*1e-3/(cpu(1)+cpu(2))
  open (1,file=file(1:index(file,'.'))//'time',form='formatted')
  write (*,'(   i5,f10.6,f9.6,2x,5f7.3,f6.0)')        it,t,dt,Cu,Cv,Cr,Ce,Cb,kzs
  write (1,'(a/2i5,f10.6,f9.6,2x,5f7.3,f6.0)') 'isnap,it,t,dt,Cu,Cv,Cr,Ce,Cb,kzs', &
                                                isnap,it,t,dt,Cu,Cv,Cr,Ce,Cb,kzs
  close (1)
END

!***********************************************************************
  SUBROUTINE stats(l,f)
  USE params
  real, dimension(mx,my,mz):: f
  character(len=*) l
!-----------------------------------------------------------------------

  if (idbg.le.0) return

  fav = average(f)
  fa2 = average((f-fav)**2)
  write (*,'(a,4(1pe11.3),2x,a)') 'min,max,av,rms:',minval(f),maxval(f),fav,sqrt(fa2),l

  END

