#!/bin/csh

make PDE=pde
make PDE=pde_call

cd TESTS

../pde_call.x < call.in
mv test.dat test_call.dat
../pde.x < call.in
idl << EOF
.run call.pro
EOF

ls -l call.*
