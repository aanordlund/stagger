#!/bin/csh

csh << 'EOF' >> TESTS/speed_${MACHTYPE}_$HOST.log

echo ""
echo "========== `date` =========="

#make clean
#make PDE=pde
#make PDE=pde_call

set x = pde
set f = speed
./$x.x < TESTS/$f.in
echo "-------- $x.x TESTS/$f.in ---------"
set x = pde_call
./$x.x < TESTS/$f.in
echo "-------- $x.x TESTS/$f.in ---------"
set f = log
./$x.x < TESTS/$f.in
echo "-------- $x.x TESTS/$f.in ---------"
set f = nopassive
./$x.x < TESTS/$f.in
echo "-------- $x.x TESTS/$f.in ---------"
set f = lin
./$x.x < TESTS/$f.in
echo "-------- $x.x TESTS/$f.in ---------"
set f = noshock
./$x.x < TESTS/$f.in
echo "-------- $x.x TESTS/$f.in ---------"

'EOF'

