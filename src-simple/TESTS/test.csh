#PBS -q parallel -S /bin/csh -j oe
#PBS -l mem=1gb,ncpus=4,cput=00:10:00

#  Trap floating point errors, or not
setenv TRAP_FPE "ALL=COUNT; UNDERFL=FLUSH_ZERO; OVERFL=TRACE, ABORT(1); INVALID=TRACE, ABORT(1); DIVZERO=TRACE, ABORT(1)"
#setenv TRAP_FPE ""

umask 026
unlimit stack
limit core 0
limit
limit -h

cd $PBS_O_WORKDIR

setenv OMP_DYNAMIC False
#setenv _DSM_MIGRATION ON
#setenv _DSM_MIGRATION_LEVEL 90
#setenv _DSM_PLACEMENT FIRST_TOUCH
setenv _DSM_VERBOSE ON

./$MACHTYPE/pde_call.x < tmp.in > tmp.log

tail /var/adm/SYSLOG
