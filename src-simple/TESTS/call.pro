close,1,2
openr,1,'test.dat'
openr,2,'test_call.dat'
a=assoc(1,fltarr(33,33,33))
b=assoc(2,fltarr(33,33,33))
ok=1
for t=0,2 do begin
  for i=0,5 do begin
    err=rms(a(6*t+i)-b(6*t+i))/(rms(a(6*t+i))+aver(a(6*t+i)))
    print,t,i,err
    if err gt 1e-5 then ok=0
  end
end
if ok eq 1 then begin
  print, 'test OK, deleting files'
  file_delete,'test.dat'
  file_delete,'test.dx'
  file_delete,'test_call.dat'
end else begin
  print, 'test NOK, leaving files'
end

END
