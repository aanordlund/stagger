! $Id: mesh_io.f90,v 1.1 2003/03/10 14:40:44 jaab Exp $
!========================================================================
subroutine write_mesh ()
  use params
  use mesh
  implicit none

  integer :: dot, lrec

  dot = index(file,'.')
  open(2, file=file(1:dot)//'mesh',             &
          status='unknown', form='unformatted', &
          access='direct', recl=lrec(6*(mx+my+mz)))
  print *,'writing mesh data', isnap

  write(2,rec=isnap+1)                        &
    xm, xmdn, dxidxup, dxidxdn, dx1dn, dx1up, &
    ym, ymdn, dyidyup, dyidydn, dy1dn, dy1up, &
    zm, zmdn, dzidzup, dzidzdn, dz1dn, dz1up
  close(2)

end subroutine write_mesh 

!========================================================================
subroutine read_mesh ()
  use params
  use mesh
  implicit none

  logical :: exists
  integer :: dot, lrec

  dot = index(file,'.')
  inquire (file=file(1:dot)//'mesh',exist=exists)
  if (exists) then
    open(2, file=file(1:dot)//'mesh',         &
            status='old', form='unformatted', &
            access='direct', recl=lrec(6*(mx+my+mz)) )
    print *,'reading mesh data', isnap

    read(2,rec=isnap+1)                         &
      xm, xmdn, dxidxup, dxidxdn, dx1dn, dx1up, &
      ym, ymdn, dyidyup, dyidydn, dy1dn, dy1up, &
      zm, zmdn, dzidzup, dzidzdn, dz1dn, dz1up
    close(2)
  end if
end subroutine read_mesh 
!========================================================================
subroutine print_mesh ()
  use mesh
  implicit none

  print *,'xm, xmdn, dxidxup, dxidxdn, dx1dn, dx1up'
  print *, xm, xmdn, dxidxup, dxidxdn, dx1dn, dx1up
  print *,'ym, ymdn, dyidyup, dyidydn, dy1dn, dy1up'
  print *, ym, ymdn, dyidyup, dyidydn, dy1dn, dy1up
  print *,'zm, zmdn, dzidzup, dzidzdn, dz1dn, dz1up'
  print *, zm, zmdn, dzidzup, dzidzdn, dz1dn, dz1up

end subroutine print_mesh 
!========================================================================
