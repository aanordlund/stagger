! $Id: mhd.f90,v 1.5 2003/04/01 22:19:40 aake Exp $
!***********************************************************************
  SUBROUTINE mhd(eta,Ux,Uy,Uz,Bx,By,Bz,dpxdt,dpydt,dpzdt,dedt,dBxdt,dBydt,dBzdt)

  USE params
  USE stagger

  logical flag
  real, dimension(mx,my,mz) :: &
       eta,Ux,Uy,Uz,Bx,By,Bz,dpxdt,dpydt,dpzdt,dedt,dBxdt,dBydt,dBzdt
  real, allocatable, dimension(:,:,:) :: &
       Jx,Jy,Jz,Ex,Ey,Ez, &
       Bx_y,Bx_z,By_x,By_z,Bz_x,Bz_y

  character(len=72) id
  data id/'$Id: mhd.f90,v 1.5 2003/04/01 22:19:40 aake Exp $'/

  if (id .ne. ' ') print *,id ; id = ' '

  call  mfield_boundary(Ux,Uy,Uz,Bx,By,Bz)

!-----------------------------------------------------------------------
!  Electric current I = curl(B), resistive E, and dissipation
!-----------------------------------------------------------------------
  allocate (Jx(mx,my,mz), Jy(mx,my,mz), Jz(mx,my,mz))
  Jx = ddydn(Bz) - ddzdn(By)
  Jy = ddzdn(Bx) - ddxdn(Bz)
  Jz = ddxdn(By) - ddydn(Bx)

  allocate (Ex(mx,my,mz), Ey(mx,my,mz), Ez(mx,my,mz))
  Ex = ydn(zdn(eta))*(dz+dy)*Jx
  Ey = xdn(zdn(eta))*(dz+dx)*Jy
  Ez = xdn(ydn(eta))*(dy+dx)*Jz

  if (do_energy) dedt = dedt + yup(zup(Jx*Ex)) &
                             + xup(zup(Jy*Ey)) &
                             + xup(yup(Jz*Ez))

!-----------------------------------------------------------------------
!  Lorenz force = I x B
!-----------------------------------------------------------------------
  allocate (Bx_y(mx,my,mz), Bx_z(mx,my,mz))
  allocate (By_z(mx,my,mz), By_x(mx,my,mz))
  allocate (Bz_x(mx,my,mz), Bz_y(mx,my,mz))
  Bx_y = ydn(Bx) ; Bx_z = zdn(Bx)
  By_z = zdn(By) ; By_x = xdn(By)
  Bz_x = xdn(Bz) ; Bz_y = ydn(Bz)
  dpxdt = dpxdt + zup(Jy*Bz_x) - yup(Jz*By_x)
  dpydt = dpydt + xup(Jz*Bx_y) - zup(Jx*Bz_y)
  dpzdt = dpzdt + yup(Jx*By_z) - xup(Jy*Bx_z)
  deallocate (Jx, Jy, Jz)

!-----------------------------------------------------------------------
!  Electric field   E =  eta I - uxB
!-----------------------------------------------------------------------
  Ex = Ex - zdn(Uy)*Bz_y + ydn(Uz)*By_z
  Ey = Ey - xdn(Uz)*Bx_z + zdn(Ux)*Bz_x 
  Ez = Ez - ydn(Ux)*By_x + xdn(Uy)*Bx_y 
  deallocate (Bx_y, Bx_z, By_z, By_x, Bz_x, Bz_y)

!-----------------------------------------------------------------------
!  Magnetic field's time derivative, dBdt = - curl(E)
!-----------------------------------------------------------------------
  dBxdt = dBxdt + ddzup(Ey) - ddyup(Ez)
  dBydt = dBydt + ddxup(Ez) - ddzup(Ex)
  dBzdt = dBzdt + ddyup(Ex) - ddxup(Ey)
  deallocate (Ex, Ey, Ez)

  END
!**********************************************************************
