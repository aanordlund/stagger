! $Id: rk3_2n.f90,v 1.5 2003/03/10 17:03:20 jaab Exp $

!************************************************************************
  SUBROUTINE timestep(r,px,py,pz,e,d,drdt,dpxdt,dpydt,dpzdt,dedt,dddt, &
                      Bx,By,Bz,dBxdt,dBydt,dBzdt)
!
!  3rd order, 2-N storage Runge-Kutta.  Advances the eight MHD variables
!  forward in time, given time derivatives computed in the subroutine pde.
!
  USE params
  implicit none

  integer nx, ny, nz, iz
  real, dimension(mx,my,mz) :: &
    r,px,py,pz,e,d, &
    drdt,dpxdt,dpydt,dpzdt,dedt,dddt, &
    Bx,By,Bz,dBxdt,dBydt,dBzdt

  real alpha(3), beta(3)
  data alpha /     0., -0.641874, -1.31021/
  data beta  /0.46173,  0.924087, 0.390614/

!------------------------------------------------------------------------
!  Loop over three Runge-Kutta stages
!------------------------------------------------------------------------
  do substep=1,3
    if (do_density) drdt  = alpha(substep)*drdt
    if (do_momenta) then
                    dpxdt = alpha(substep)*dpxdt
                    dpydt = alpha(substep)*dpydt
                    dpzdt = alpha(substep)*dpzdt
    end if
    if (do_energy)  dedt  = alpha(substep)*dedt
    if (do_mhd) then
                    dBxdt = alpha(substep)*dBxdt
                    dBydt = alpha(substep)*dBydt
                    dBzdt = alpha(substep)*dBzdt
    end if
    if (do_pscalar) dddt  = alpha(substep)*dddt

    call pde(r,px,py,pz,e,d,drdt,dpxdt,dpydt,dpzdt,dedt,dddt,substep.eq.1, &
             Bx,By,Bz,dBxdt,dBydt,dBzdt)

    if (do_density) r  = r  + (dt*beta(substep))*drdt
    if (do_momenta) then
                    px = px + (dt*beta(substep))*dpxdt
                    py = py + (dt*beta(substep))*dpydt
                    pz = pz + (dt*beta(substep))*dpzdt
    end if
    if (do_energy)  e  = e  + (dt*beta(substep))*dedt
    if (do_mhd) then
                    Bx = Bx + (dt*beta(substep))*dBxdt
                    By = By + (dt*beta(substep))*dBydt
                    Bz = Bz + (dt*beta(substep))*dBzdt
    end if
    if (do_pscalar) d  = d  + (dt*beta(substep))*dddt
  end do

  t = t + dt
  END
