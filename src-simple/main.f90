! $Id: main.f90,v 1.6 2003/03/10 17:03:20 jaab Exp $
!***********************************************************************
PROGRAM stagger_code
!
!  Main program for the stagger code
!
  USE params
  USE stagger
  USE forcing

  implicit none

  integer :: mxyz, iz, lrec, i, iv
  real :: cpu(2), cputot(2,2), dtime, etime, etm, kzs, void
  real, allocatable, dimension(:,:,:) :: &
        r,px,py,pz,e,d,drdt,dpxdt,dpydt,dpzdt,dedt,dddt, &
        Bx,By,Bz,dBxdt,dBydt,dBzdt
!hpf$ distribute(*,*,block):: &
!hpf$   r,px,py,pz,e,d,drdt,dpxdt,dpydt,dpzdt,dedt,dddt, &
!hpf$   Bx,By,Bz,dBxdt,dBydt,dBzdt

  print *,'-----------------------------------------------------------------------'
  call init

  allocate (r(mx,my,mz), &
         drdt(mx,my,mz), &
           px(mx,my,mz), &
           py(mx,my,mz), &
           pz(mx,my,mz), &
            e(mx,my,mz), &
            d(mx,my,mz), &
        dpxdt(mx,my,mz), &
        dpydt(mx,my,mz), &
        dpzdt(mx,my,mz), &
         dedt(mx,my,mz), &
         dddt(mx,my,mz))

!$omp parallel do private(iz)
  do iz=1,mz                                ! encourage distribution
    r (:,:,iz)=1.
    px(:,:,iz)=0.
    py(:,:,iz)=0.
    pz(:,:,iz)=0.
    e (:,:,iz)=1.
    d (:,:,iz)=1.
    drdt (:,:,iz)=0.
    dpxdt(:,:,iz)=0.
    dpydt(:,:,iz)=0.
    dpzdt(:,:,iz)=0.
    dedt (:,:,iz)=0.
    dddt (:,:,iz)=0.
  end do

if (do_mhd) then
  allocate (Bx(mx,my,mz), &
            By(mx,my,mz), &
            Bz(mx,my,mz), &
         dBxdt(mx,my,mz), &
         dBydt(mx,my,mz), &
         dBzdt(mx,my,mz))
!$omp parallel do private(iz)
  do iz=1,mz                                ! encourage distribution
    Bx(:,:,iz)=0.
    By(:,:,iz)=0.
    Bz(:,:,iz)=0.
    dBxdt(:,:,iz)=0.
    dBydt(:,:,iz)=0.
    dBzdt(:,:,iz)=0.
  end do
end if

!-----------------------------------------------------------------------
!  Initial values
!-----------------------------------------------------------------------

  call init_force

  if (iread .ge. 0) then
    call read_snap (r,px,py,pz,e,d,Bx,By,Bz)
  else
    call init_values (r,px,py,pz,e,d,Bx,By,Bz)
  end if
  print *,'-----------------------------------------------------------------------'

!-----------------------------------------------------------------------
!  Time step loop
!-----------------------------------------------------------------------
  do it=0,nstep                             ! nstep ...

    if (it.eq.1) etm = etime(cputot(1,1))   ! skip timing first step
    if (mod(it,nsnap) .eq. 0) then          ! snap shot every nsnap
      isnap = isnap+1                       ! increment counter
      call write_snap (r,px,py,pz,e,d,Bx,By,Bz)
    endif

    void = dtime(cpu)                       ! for speed measurement
    call timestep( &                        ! step the variables
      r,px,py,pz,e,d, &
      drdt,dpxdt,dpydt,dpzdt,dedt,dddt, &
      Bx,By,Bz,dBxdt,dBydt,dBzdt)
    void = dtime(cpu)                       ! speed measurement
    call print_time (cpu)                   ! print time step and speed info

    call oflush                             ! generic output flush
  end do
  etm = etime(cputot(1,2))-etm              ! total time for the run

!-----------------------------------------------------------------------
!  Speed summary
!-----------------------------------------------------------------------
  print '(/4(a,f10.1/))', '       user time:', cputot(1,2)-cputot(1,1), &
                          '     system time:', cputot(2,2)-cputot(2,1), &
                          '    average kz/s:',(nstep*3e-3)*mx*my*mz/(etm+1e-6), &
                          ' average mus/pnt:',(etm*1e6)/(nstep*mx*my*mz)
END

