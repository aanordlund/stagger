MODULE mesh
  USE params
  implicit none

  real,allocatable,dimension(:) :: xm,xmdn,dxidxup,dxidxdn,dx1dn,dx1up
  real,allocatable,dimension(:) :: ym,ymdn,dyidyup,dyidydn,dy1dn,dy1up
  real,allocatable,dimension(:) :: zm,zmdn,dzidzup,dzidzdn,dz1dn,dz1up
  real :: dsmin, dsmax
  
  private :: mesh_set, meshf, ddxup_idx, ddxdn_idx
  public  :: init_mesh, stretch, unstretch, volaverage

CONTAINS
!========================================================================
 subroutine init_mesh
   implicit none
   allocate (xm(mx),xmdn(mx),dxidxup(mx),dxidxdn(mx),dx1dn(mx),dx1up(mx))
   allocate (ym(my),ymdn(my),dyidyup(my),dyidydn(my),dy1dn(my),dy1up(my))
   allocate (zm(mz),zmdn(mz),dzidzup(mz),dzidzdn(mz),dz1dn(mz),dz1up(mz))

   call mesh_set(mx,sx,pack,dpack,xm,xmdn,dxidxup,dxidxdn,dx1dn,dx1up)
   call mesh_set(my,sy,pack,dpack,ym,ymdn,dyidyup,dyidydn,dy1dn,dy1up)
   call mesh_set(mz,sz,pack,dpack,zm,zmdn,dzidzup,dzidzdn,dz1dn,dz1up)
   
   dsmax = amax1(maxval(dx1dn),maxval(dy1dn),maxval(dz1dn), &
                 maxval(dx1up),maxval(dy1up),maxval(dz1up))
   dsmin = amin1(minval(dx1dn),minval(dy1dn),minval(dz1dn), &
                 minval(dx1up),minval(dy1up),minval(dz1up))

 end subroutine init_mesh
!========================================================================
 subroutine mesh_set (n,sx,pack,dpack,xm,xmdn,dxidxup,dxidxdn,dx1dn,dx1up)
   implicit none

   integer                :: i, n
   integer, parameter     :: p  = 4  ! patch
   real                   :: sx , pack, dpack
   real, dimension(n)     :: xm,xmdn,dxidxup,dxidxdn,dx1dn,dx1up
   real, dimension(n+2*p) :: xx, xxdn

   xx   = ( (/(i,i=0,n+2*p-1)/)-real(p)+0.5) / real(n) - 0.5
   xxdn = ( (/(i,i=0,n+2*p-1)/)-real(p)    ) / real(n) - 0.5

! rulers center/dn :: [xyz]m [xyz]mdn
   call meshf(n+2*p,xx  ,pack,dpack)
   call meshf(n+2*p,xxdn,pack,dpack)
   xm(1:n)   = sx * xx(p+1:n+p+1)
   xmdn(1:n) = sx * xxdn(p+1:n+p+1)

! finite diference :: d[xyz]1d
! Jacobians :: d[xyz]id[xyz]dn d[xyz]id[xyz]up = 1.0/ddxdn_idx() 1.0/ddxup_idx()
   call ddxdn_idx(xx  ,n,p,dxidxdn)
   call ddxup_idx(xxdn,n,p,dxidxup)
   dx1dn    = sx*dxidxdn
   dx1up    = sx*dxidxup
   dxidxdn = 1.0/(dx1dn)
   dxidxup = 1.0/(dx1up)

 end subroutine mesh_set
!========================================================================
 subroutine meshf (n,x,pack,dpack)
   implicit none

   integer           :: n
   real              :: d, pack, dpack, a
   real,dimension(n) :: x

   d = 8*(0.2/dpack)**2
   a = (1.+2.*pack*dpack*tanh((0.5+d*0.5**3)/dpack))
   x = x*a-pack*dpack*tanh((x+d*x**3)/dpack)

 end subroutine meshf
!========================================================================
 subroutine ddxup_idx (f,n,p,fp)
  implicit none
  integer                :: n, p, i
  real                   :: a,b,c
  real, dimension(n+2*p) :: f
  real, dimension(n)     :: fp

  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)

  do i=p+1,n+p
     fp(i-p) = (                   &
                a*(f(i+1)-f(i  ))  &
              + b*(f(i+2)-f(i-1))  &
              + c*(f(i+3)-f(i-2)))
  end do

 end subroutine ddxup_idx
!========================================================================
 subroutine ddxdn_idx (f,n,p,fp)
  implicit none
  integer                :: n, p, i
  real                   :: a,b,c
  real, dimension(n+2*p) :: f
  real, dimension(n)     :: fp

  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)

  do i=p,n+p-1
     fp(1+i-p) = (                &
                a*(f(i+1)-f(i  )) &
              + b*(f(i+2)-f(i-1)) &
              + c*(f(i+3)-f(i-2)))
  end do

 end subroutine ddxdn_idx
!========================================================================
 function stretch (fin)
   implicit none
   real, intent(in ), dimension(mx,my,mz):: fin
   real,              dimension(mx,my,mz):: stretch
   real,              dimension(mx,my,mz):: ftmp
   real xeven(mx), yeven(my), zeven(mz)
   integer i
   !hpf$ align with TheCube :: fin, ftmp, stretch

   xeven = xm(1) + (xm(mx)-xm(1))*(/(i, i=0,mx-1)/)/(mx-1.)
   yeven = ym(1) + (ym(my)-ym(1))*(/(i, i=0,my-1)/)/(my-1.)
   zeven = zm(1) + (zm(mz)-zm(1))*(/(i, i=0,mz-1)/)/(mz-1.)

   call xinterp(mx, my, mz, xeven, mx, xm, fin,  stretch)
   call yinterp(mx, my, mz, yeven, my, ym, stretch, ftmp)
   call zinterp(mx, my, mz, zeven, mz, zm, ftmp, stretch)

 end function stretch
!========================================================================
 function unstretch (fin)
   implicit none
   real, intent(in ), dimension(mx,my,mz):: fin
   real,              dimension(mx,my,mz):: unstretch
   real,              dimension(mx,my,mz):: ftmp
   real xeven(mx), yeven(my), zeven(mz)
   integer i
   !hpf$ align with TheCube :: fin, ftmp, unstretch

   xeven = xm(1) + (xm(mx)-xm(1))*(/(i, i=0,mx-1)/)/(mx-1.)
   yeven = ym(1) + (ym(my)-ym(1))*(/(i, i=0,my-1)/)/(my-1.)
   zeven = zm(1) + (zm(mz)-zm(1))*(/(i, i=0,mz-1)/)/(mz-1.)

   call xinterp(mx, my, mz, xm, mx, xeven, fin,  unstretch)
   call yinterp(mx, my, mz, ym, my, yeven, unstretch, ftmp)
   call zinterp(mx, my, mz, zm, mz, zeven, ftmp, unstretch)

 end function unstretch
!========================================================================
  function volaverage(f)
  implicit none

  integer                   :: i, j, k
  real, dimension(mx,my,mz) :: f
  real, dimension(my,mz)    :: fa
  real, dimension(mz)       :: fb
  real                      :: volaverage

!$ doacross local(k,j,fa), shared(fb)
  do k=1,mz
    do j=1,my
      fa(j,k) = sum(dx1dn*f(:,j,k))
    end do
    fb(k) = sum(dy1dn*fa(:,k))
  end do

  volaverage = sum(dz1dn*fb)/(sx*sy*sz)

  end function volaverage
!========================================================================


END MODULE mesh
