! $Id: resist.f90,v 1.8 2003/04/11 07:21:44 aake Exp $
!-----------------------------------------------------------------------
  SUBROUTINE resist (eta,r,ee,Ux,Uy,Uz,Bx,By,Bz)

  USE params
  USE stagger

  implicit none

  integer iz
  real gg1, ds
  real, dimension(mx,my,mz) :: &
       eta,r,ee,Ux,Uy,Uz,Bx,By,Bz
  real, allocatable, dimension(:,:,:) :: &
       Uxc,Uyc,Uzc,Bxc,Byc,Bzc,BB,UdB,cA

  if (nu2 .eq. 0.) then
    gg1 = gamma*(gamma-1)
    if (gamma .eq. 1.) gg1=1.
    allocate (cA(mx,my,mz))
    cA = sqrt(gg1*ee)
    Cb = maxval(Ca)*dt/min(dx,dy,dz)
    eta = nu1*cA
    deallocate (cA)
  else
    allocate (Bxc(mx,my,mz), Byc(mx,my,mz), Bzc(mx,my,mz), BB(mx,my,mz))
    Bxc = xup(Bx)
    Byc = yup(By)
    Bzc = zup(Bz)

    allocate (cA(mx,my,mz))
    gg1 = gamma*(gamma-1)
    if (gamma .eq. 1.) gg1=1.
    BB = max(Bxc**2 + Byc**2 + Bzc**2, 1.e-20)
    cA = sqrt(gg1*ee+BB/r)
    BB = 1./sqrt(BB)
    Bxc = Bxc*BB
    Byc = Byc*BB
    Bzc = Bzc*BB
    Cb = maxval(Ca)*dt/min(dx,dy,dz)
    deallocate (BB)

    allocate (Uxc(mx,my,mz), Uyc(mx,my,mz), Uzc(mx,my,mz), UdB(mx,my,mz))
    Uxc = xup(Ux)
    Uyc = yup(Uy)
    Uzc = zup(Uz)

    UdB = Uxc*Bxc &
        + Uyc*Byc &
        + Uzc*Bzc
    Uxc = Uxc-Bxc*UdB
    Uyc = Uyc-Byc*UdB
    Uzc = Uzc-Bzc*UdB

    call dif2_set (Uxc, Uyc, Uzc, UdB)

    deallocate (Uxc, Uyc, Uzc, Bxc, Byc, Bzc)

!  Smooth over 3x3x3 point, to broaden influence.  Use a low order shift
!  after smoothing, to ensure positive definite values.

    ds = max(dx,dy,dz)
    eta = (nu1*ds)*cA + (nu2*ds**2)*UdB
    call regularize(eta)
    eta = smooth3(max5(eta))
    deallocate (cA, UdB)
  end if

  end
