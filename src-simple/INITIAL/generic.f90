! $Id: generic.f90,v 1.1 2003/02/01 13:38:07 aake Exp $
SUBROUTINE init_values (r,px,py,pz,e,d,Bx,By,Bz)

  USE params
  implicit none

  real, dimension(mx,my,mz):: r,px,py,pz,e,d,Bx,By,Bz

  r=1.
  e=1.
  px=0.
  py=0.
  pz=0.
  if (do_pscalar) d=1.
  if (do_mhd) then
    Bx=0.
    By=0.
    Bz=0.
  end if
END
