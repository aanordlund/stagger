module units
!
!  Ideal gas units
!
  use params
  implicit none

  real, parameter:: urho=1, utime=1, ulength=1, &
                    uvelocity=ulength/utime, uenergy=uvelocity**2, &
		    upressure=urho*uenergy, utemp=1

end module units
