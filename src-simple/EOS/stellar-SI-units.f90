module units
!
!  Stellar CGS units
!
  use params
  implicit none

  real, parameter:: urho=1e-4, utime=1e2, ulength=1e6
  real, parameter:: uvelocity=ulength/utime, uenergy=uvelocity**2, upressure=urho*uenergy

  double precision, parameter:: kB=1.38e-23, mproton=1.6726e-27, melectron=9.1094e-31, &
                    echarge=1.602e-19, hplanck=6.6261e-34

end module units
