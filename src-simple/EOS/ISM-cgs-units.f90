module units
!
!  ISM cgs units
!
  use params
  implicit none

  real, parameter:: urho=1e-24, utime=3e13, ulength=3e18
  real, parameter:: uvelocity=ulength/utime, uenergy=uvelocity**2, upressure=urho*uenergy

  real, parameter:: kB=1.38e-16, mproton=1.6726e-24, melectron=9.1094e-28, &
                    echarge=1.602e-12, hplanck=6.6261e-27

end module units
