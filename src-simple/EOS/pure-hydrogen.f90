! $Id: pure-hydrogen.f90,v 1.3 2003/02/22 01:13:14 aake Exp $
module eos
!
!  EOS for simple hydrogren plasma
!
  use params
  use units

  implicit none

  real, parameter:: chi=13.6, ee0=chi*echarge/mproton, tt0=chi*echarge/kB
! real, parameter:: rho0=mproton*(2.*pi*melectron*chi*echarge/hplanck**2)**1.5
  real, parameter:: rho0=0.25317730
  real:: rho1,ee1

contains
  function pressure(r,ee,tt)
    implicit none
    real, dimension(mx,my,mz):: r,ee,yy,pressure
    real, dimension(mx,my,mz), optional:: tt
    real, parameter:: twothirds=0.66666667
    character(len=80):: id="$Id: pure-hydrogen.f90,v 1.3 2003/02/22 01:13:14 aake Exp $"

    if (id.ne.'') print *,id
    id=''

    rho1=urho/rho0
    ee1=uenergy/ee0

    yy = ionfrac(r,ee)
    if (present(tt)) then
      tt=(twothirds*tt0)*(ee*ee1-yy)
      pressure=(urho/upressure*ee0/tt0)*tt*r
      tt=tt/(1.+yy)
    else
      pressure=(twothirds*urho/upressure*ee0)*r*(ee*ee1-yy)
    end if
    return
  end function

  function ionfrac(r,ee)
!----------------------------------------------------------------
! root-finding loop
!----------------------------------------------------------------
    implicit none
    real, dimension(mx,my,mz) :: r,ee
    real, dimension(mx,my,mz) :: ionfrac
    integer                   :: i,j,k

    do k=1,mz
       do j=1,my
          do i=1,mx
             ionfrac(i,j,k)=rtsafe(r(i,j,k),ee(i,j,k))
          end do
       end do
    end do
  end function ionfrac

  function rtsafe(r,ee)
!----------------------------------------------------------------
! safe newton-raphson algorithm
!----------------------------------------------------------------
    implicit none
    real, intent(in)   :: r,ee
    real               :: rtsafe
    real, parameter    :: yacc=1.e-5
    integer, parameter :: maxit=100
    real               :: yh,yl,fh,fl,f,df,dy,dyold,temp
    integer            :: i
    real, save         :: ylast=.5

    yl=yacc                           ! lower bound
    yh=1.-yacc                        ! upper bound
    if (yh.ge.ee*ee1) yh=ee*ee1-yacc  ! values gt ee make no sense
                                      !   (negative temperatures)
    call saha(r,ee,yl,fl,df)
    if (fl.le.0.) then
       rtsafe=yl                      ! ionization fraction equal to zero
       ylast=yl                       ! within specified accuracy, because
       return                         ! f is monotonically decreasing
    endif
    call saha(r,ee,yh,fh,df)
    if (fh.ge.0.) then
       rtsafe=yh                      ! ionization fraction equal to one...
       ylast=yh
       return
    endif

    dyold=yh-yl
    dy=dyold
    if (ylast.ge.yh) ylast=.5*yh      ! check that last value
    rtsafe=ylast                      !   is not out of bounds
    call saha(r,ee,rtsafe,f,df)

    do i=1,maxit
       if (((rtsafe-yl)*df-f)*((rtsafe-yh)*df-f).gt.0. &
            .or. abs(2.*f).gt.abs(dyold*df)) then
          dyold=dy
          dy=.5*(yh-yl)
          rtsafe=yh-dy
          if (yh.eq.rtsafe) then
             ylast=rtsafe
             return
          end if
       else
          dyold=dy
          dy=f/df
          temp=rtsafe
          rtsafe=rtsafe-dy
          if (temp.eq.rtsafe) then
             ylast=rtsafe
             return
          end if
       end if
       if (abs(dy).lt.yacc) then
          ylast=rtsafe
          return
       end if
       call saha(r,ee,rtsafe,f,df)
       if (f.lt.0.) then
          yh=rtsafe
       else
          yl=rtsafe
       end if
    end do
    print *,'error: rtsafe exceeding maximum iterations'
    return
  end function rtsafe

  subroutine saha(r,ee,y,f,df)
!----------------------------------------------------------------
! we want to find the root of f
!----------------------------------------------------------------
    implicit none
    real, intent(in)  :: r,ee,y
    real, intent(out) :: f,df
    real              :: T,dT,rho

    T=(ee*ee1-y)/(1.+y)
    rho=r*rho1
    dT=-(1.+T)/(1.+y)
    f=1.5*log(T)-log(rho)-1./T+log(1.-y)-2.*log(y)
    df=(dT/T)*(1.5+1./T)-1./(1.-y)-2./y
  end subroutine saha

end module eos
