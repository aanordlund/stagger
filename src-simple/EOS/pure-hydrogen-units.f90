module units
!
!  Stellar CGS units.  With these units, rho=e=1 corresponds (by chance) to 5800 K,
!  which is close to the solar effective temperature.
!
  use params
  implicit none

  real, parameter:: urho=1e-7, utime=1e2, ulength=1e8
  real, parameter:: uvelocity=ulength/utime, uenergy=uvelocity**2, upressure=urho*uenergy

  real, parameter:: kB=1.38e-16, mproton=1.6726e-24, melectron=9.1094e-28, &
                    echarge=1.602e-12, hplanck=6.6261e-27

end module units
