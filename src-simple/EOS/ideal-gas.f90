module eos
!
!  EOS for ideal gas
!
  use params
  use units

  implicit none

contains

  function pressure(r,ee,tt)
    implicit none
    real, dimension(mx,my,mz):: r,ee,yy,pressure
    real, dimension(mx,my,mz), optional:: tt
    real:: gam1

    if (gamma .eq. 1.) then
      gam1 = gamma
    else
      gam1 = gamma-1.
    endif
    pressure = gam1*r*ee
    if (present(tt)) then
      tt = utemp*ee
    end if

    return
  end function

end module eos
