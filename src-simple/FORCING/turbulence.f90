! $Id: turbulence.f90,v 1.3 2003/02/01 13:53:30 aake Exp $
!**********************************************************************
MODULE forcing

real:: t_turb, t_turn, ampl_turb
integer:: iseed

real, allocatable, dimension(:,:,:):: pranx,prany,pranz,franx,frany,franz

PUBLIC read_force, alloc_force, force, dealloc_force

CONTAINS

  SUBROUTINE init_force
    USE params
    integer iz, nx, ny, nz

    call read_force
    t_turb = 0.
    allocate (pranx(mx,my,mz), prany(mx,my,mz), pranz(mx,my,mz))
    allocate (franx(mx,my,mz), frany(mx,my,mz), franz(mx,my,mz))
!$omp parallel do private(iz)
    do iz=1,mz                                ! encourage distribution
      pranx(:,:,iz)=0. ; prany(:,:,iz)=0. ; pranz(:,:,iz)=0.
      franx(:,:,iz)=0. ; frany(:,:,iz)=0. ; franz(:,:,iz)=0.
    end do
  END SUBROUTINE

  SUBROUTINE dealloc_force
    deallocate (pranx, prany, pranz)
    deallocate (franx, frany, franz)
  END SUBROUTINE

  SUBROUTINE read_force
    namelist /force/ ampl_turb, t_turn, iseed
    ampl_turb = 0.1
    t_turn = 10.
    iseed = 7
    read (*,force)
    write (*,force)
  END SUBROUTINE

  SUBROUTINE force (nx,ny,nz,r,Ux,Uy,Uz,dpxdt,dpydt,dpzdt)
!
!  Calculate a random force, confined to a shell in k-space, at
!  regular intervals in time.  We assume rho, P, and the sound
!  speed to be near unity.  The velocity amplitude should be of the
!  order ampl_turb.  The size (scale) of the driving motions is of the
!  order of mx*dx/fk1.  Thus the turnover time is given by
!  t_turb*ampl_turb = mx*dx/fk1; and the acceleration is of the order
!  ampl_turb/t_turb.
!
!  Note:  In order to restart properly, one would have to save the
!  previous value of iseed.
!
!  06-aug-93/aake:  bug corrected; iseed was not negative at start
!  09-aug-93/aake:  changed to allow restart w same random coeffs
!  21-feb-95/paolo: changed in order to use segments of constant
!                   time der. of acceleration (so continuous accel.)
!            franx(,y,z) are time deriv. of accel. (random, solenoidal);
!            pranx(,y,z) (and scr2(,3,4)) are accelerations. The variable
!            ``accel'' is now the time derivative of the acceleration.
!            ``t_turn'' has the same meaning as before (see comment above).
!  23-jan-96/aake:  bug corrected; ran1() -> 2.*ran1()-1. to span [-1,1]
!
!            Restart must be done AFTER t=t_turn.
!            ------------------------------------
!----------------------------------------------------------------------
  USE params
  USE stagger

  implicit none

  integer nx, ny, nz

  real, dimension(nx,ny,nz):: r,Ux,Uy,Uz,dpxdt,dpydt,dpzdt,scr1,scr2,scr3

  integer do_helmh, i, j, k, jx, jy, jz, jxmax, nrand

  complex fxx,fyy,fzz,kx,ky,kz,corr,expikr

  real x, y, z, xav, yav, zav, rav, w, fact, pk, test, fpow, accel
  real fk1, fk2, fk1c, fk2c, eps, fk
  real xsum(nz), ysum(nz), zsum(nz), rsum(nz)
  integer k1, k2, flag, flag2

  real, save:: istart=1 ,tprev=-1.

  do_helmh = 1
  k1 = 1
  k2 = 2
  fk1c = k1
  fk2c = k2

!----------------------------------------------------------------------
!   
                    if (t .ne. tprev) then    ! prevent repeat at same t
                    tprev=t

  flag=1
!--------------------------------------------- new random numbers:
!-----------------------------------------------------------------

  if (t .gt. t_turb .or. istart .ne. 0) then

    flag=0

    eps = 1e-5
    fk1 = fk1c - eps
    fk2 = fk2c + eps

!  Loop until t_turb > t
!
100     if (istart .ne. 0) then
      istart = 0
      iseed  = -iabs(iseed)
      t_turb = -1.5*t_turn
!$ doacross local(k)
      do k=1,mz
        pranx(:,:,k)=0.
        prany(:,:,k)=0.
        pranz(:,:,k)=0.
      end do
    else
!$ doacross local(k)
      do k=1,mz
        pranx(:,:,k)=franx(:,:,k)
        prany(:,:,k)=frany(:,:,k)
        pranz(:,:,k)=franz(:,:,k)
      end do
    endif

    t_turb = t_turb + t_turn

    nrand = 0
    jxmax=6
    if (my*mz .eq. 1) jxmax=128
    do jx=0,jxmax
      do jy=0,min0(my-1,6)
        do jz=0,min0(mz-1,6)
          fk = sqrt(float(jx**2+jy**2+jz**2))
          if (fk .ge. fk1 .and. fk .le. fk2) nrand=nrand+1
        enddo
      enddo
    enddo

!--------------------------------------------------------------------------------
    accel  = ampl_turb/t_turn/sqrt(float(nrand)/8.)    ! rms=1./8. per comp.
!--------------------------------------------------------------------------------

      print *,'random_force:t=',t,t_turb,t_turn,nrand,iseed

!$ doacross local(k)
      do k=1,mz
        franx(:,:,k) = 0.
        frany(:,:,k) = 0.
        franz(:,:,k) = 0.
      end do

! To obtain a Kolmogorov slope of the driving alone, one should have amplitudes
! a(k) that drop with k^(-11./6.), to have a(k)^2*k^2 = k^(-5./3.).

    ! pk = 1.5     ! previous value
    pk = 11./6.

    fpow = 0.
    do jx=-k2,k2
      kx = cmplx (0., jx*2.*pi/(mx*dx))
      do jy=-k2,k2
        ky = cmplx (0., jy*2.*pi/(my*dy))
        do jz=-k2,k2
          kz = cmplx (0., jz*2.*pi/(mz*dz))
          fk = sqrt(float(jx**2+jy**2+jz**2))
          if (fk .ge. fk1 .and. fk .le. fk2) then
            fxx =   cexp(cmplx(0., 2.*pi*ran1(iseed)))/fk**pk
            fyy =   cexp(cmplx(0., 2.*pi*ran1(iseed)))/fk**pk
            if (mz.gt.1) then
              fzz = cexp(cmplx(0., 2.*pi*ran1(iseed)))/fk**pk
            else
              fzz=0.
            end if

! solenoidal field:
!------------------
           if (do_helmh .ne. 0) then
              corr=(kx*fxx+ky*fyy+kz*fzz)/(kx*kx+ky*ky+kz*kz+1e-20) 

              if (jx.ne.0) fxx = fxx - corr*kx
              if (jy.ne.0) fyy = fyy - corr*ky
              if (jz.ne.0) fzz = fzz - corr*kz
           endif
!------------------

           fact=1.
           if (jx.gt.0) fact=fact*0.5
           if (jy.gt.0) fact=fact*0.5
           if (jz.gt.0) fact=fact*0.5
           fpow = fpow+fact*(cabs(fxx)**2+cabs(fyy)**2+cabs(fzz)**2)

           do k=1,mz
             z = dz*(k-1)
             do j=1,my
               y = dy*(j-1)
               do i=1,mx
                 x = dx*(i-1)
                 expikr = accel*cexp(kx*x+ky*y+kz*z)
                 franx(i,j,k) = franx(i,j,k) + real(fxx*expikr)
                 frany(i,j,k) = frany(i,j,k) + real(fyy*expikr)
                 franz(i,j,k) = franz(i,j,k) + real(fzz*expikr)
               end do
             end do
           end do
           ! print *,'random_force:k=',jx,jy,jz,fk
           ! print *,fxx,fyy,fzz,fact
          endif
        enddo
      enddo
    enddo

!--------------------------------          

! TEST:
!--------------------------------------------------
    if (idbg.ge.1) then
      test=sqrt(sum(franx**2+frany**2+franz**2)/mw)
      print *,'AVERAGE ACCELERATION = ',test,sqrt(fpow)*accel
    end if
!--------------------------------------------------
   
    if (t_turb .lt. t) then
       goto 100
    endif
  
  endif  ! new random numbers
!----------------------------------------end of new random numbers.
!------------------------------------------------------------------
                            endif                            

!  Time interpolation of the force
!
!$ doacross local(k)
  w = 1.-exp(-(t+t_turn-t_turb)/t_turn)
  do k=1,mz
    scr1(:,:,k) = pranx(:,:,k) + (franx(:,:,k)-pranx(:,:,k))*w
    scr2(:,:,k) = prany(:,:,k) + (frany(:,:,k)-prany(:,:,k))*w
    scr3(:,:,k) = pranz(:,:,k) + (franz(:,:,k)-pranz(:,:,k))*w
  end do


! Force and velocity averages
!----------------------------

!$ doacross local(k)
  do k=1,mz
    xsum(k) = sum(r(:,:,k)*(scr1(:,:,k) + Ux(:,:,k)*(1./t_turn)))
    ysum(k) = sum(r(:,:,k)*(scr2(:,:,k) + Uy(:,:,k)*(1./t_turn)))
    zsum(k) = sum(r(:,:,k)*(scr3(:,:,k) + Uz(:,:,k)*(1./t_turn)))
    rsum(k) = sum(r(:,:,k))
  end do
  rav = sum(rsum)/mw
  xav = sum(xsum)/(mw*rav)
  yav = sum(ysum)/(mw*rav)
  zav = sum(zsum)/(mw*rav)
  do k=1,mz
    scr1(:,:,k) = scr1(:,:,k) - xav
    scr2(:,:,k) = scr2(:,:,k) - yav
    scr3(:,:,k) = scr3(:,:,k) - zav
  end do

  dpxdt = dpxdt + xdn(r*scr1)
  dpydt = dpydt + ydn(r*scr2)
  dpzdt = dpzdt + zdn(r*scr3)

  call stats ('pranx', scr1)
  call stats ('prany', scr2)
  call stats ('pranz', scr3)

END SUBROUTINE

!***********************************************************************
FUNCTION ran1(idum)
  INTEGER idum,IA,IM,IQ,IR,NTAB,NDIV
  REAL ran1,AM,EPS,RNMX
  PARAMETER (IA=16807,IM=2147483647,AM=1./IM,IQ=127773,IR=2836, &
  NTAB=32,NDIV=1+(IM-1)/NTAB,EPS=1.2e-7,RNMX=1.-EPS)
  INTEGER j,k,iv(NTAB),iy
  SAVE iv,iy
  DATA iv /NTAB*0/, iy /0/
  if (idum.le.0.or.iy.eq.0) then
    idum=max(-idum,1)
    do 11 j=NTAB+8,1,-1
      k=idum/IQ
      idum=IA*(idum-k*IQ)-IR*k
      if (idum.lt.0) idum=idum+IM
      if (j.le.NTAB) iv(j)=idum
11      continue
    iy=iv(1)
  endif
  k=idum/IQ
  idum=IA*(idum-k*IQ)-IR*k
  if (idum.lt.0) idum=idum+IM
  j=1+iy/NDIV
  iy=iv(j)
  iv(j)=idum
  ran1=min(AM*iy,RNMX)
  return
END FUNCTION

END MODULE
