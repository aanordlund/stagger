! $Id: splines.f90,v 1.1 2003/03/10 14:40:44 jaab Exp $
!***********************************************************************
      subroutine der0 (nz,z,w1,w2,w3)
!
!  Calculates the matrix elements and an LU decomposition of the problem
!  of finding the derivatives of a cubic spline with continuous second
!  derivatives.
!
!  For a general cubic spline, we have an equation system of the form:
!
!     | w2 w3          |   | d |     | e |
!     | w1 w2 w3       |   | d |     | e |
!     |       .  .     | * | . |  =  | . |
!     |       .  .  .  |   | . |     | . |
!     |          w1 w2 |   | d |     | e |
!
!  This may be solved by adding a fraction (w1) of each row to the
!  following row.
!
!  This corresponds to an LU decomposition of the problem:
!
!     | w2 w3          |   | d |     | 1               |   | e |
!     |    w2 w3       |   | d |     | w1  1           |   | e |
!     |       .  .     | * | . |  =  |    w1  1        | * | . |
!     |          .  .  |   | . |     |        .  .     |   | . |
!     |             w2 |   | d |     |           w1  1 |   | e |
!
!  In this startup routine, we calculate the fractions w1.
!  These are the same for all splines with the same z-scale,
!  and only have to be calculated once for all nx*ny splines.
!
!  In principle, this startup routine only has to be called once,
!  but to make the der entry selfcontained, der0 is called from
!  within der.  In practice, the der0 time is insignificant.
! 
!  Update history:
!                      .
!  20-aug-87: coded by Ake Nordlund, Copenhagen University Observatory
!  27-sep-87: Removed bdry[123] parameters.
!  02-oct-87: Moved factor 3 from der loop to der0 loop.
!  04-nov-87: inverted w2(), to turn divides into multiplications
!  09-nov-87: der0() split off as separate routine
!  20-may-99: generic f90 version
!
!***********************************************************************
!
      integer :: nz, k
      real, dimension(nz):: z,w1,w2,w3
      real :: cza, czb, a3, c
!
!  First point
!
      cza=1./(z(3)-z(2))
      czb=1./(z(2)-z(1))
      w3(1)=cza*cza
      w1(1)=-czb*czb
      w2(1)=w1(1)+w3(1)
!
!  Interior points
!
      a3=1./3.
      do 100 k=2,nz-1
        w1(k)=a3/(z(k)-z(k-1))
        w3(k)=a3/(z(k+1)-z(k))
        w2(k)=2.*(w1(k)+w3(k))
100   continue
!
!  Last point
!
      cza=1./(z(nz-1)-z(nz-2))
      czb=1./(z(nz)-z(nz-1))
      w1(nz)=cza*cza
      w3(nz)=-czb*czb
      w2(nz)=w1(nz)+w3(nz)
!
!  Eliminate at first point
!
      c=-w3(1)/w3(2)
      w2(1)=w2(1)+c*w2(2)
      w1(1)=w1(1)+c*w1(2)
      w3(1)=w2(1)
      w2(1)=w1(1)
      w1(1)=c
!
!  Eliminate at last point
!
      c=-w1(nz)/w1(nz-1)
      w2(nz)=w2(nz)+c*w2(nz-1)
      w3(nz)=w3(nz)+c*w3(nz-1)
      w1(nz)=w2(nz)
      w2(nz)=w3(nz)
      w3(nz)=c
!
!  Eliminate subdiagonal elements of rows 2 to nz:
!
!     |     w2 w3       | * | . |  =  | . |
!     |     w1 w2 w3    |   | . |     | . |
!
      do 110 k=2,nz
        w1(k)=-w1(k)/w2(k-1)
        w2(k)=w2(k)+w1(k)*w3(k-1)
110   continue
!
!  Invert all the w2()'s, turns divides into mults
!
      do 120 k=1,nz
        w2(k)=1./w2(k)
120   continue
!
      end
!***********************************************************************
      subroutine xder (nx,ny,nz,x,f,d)
!
!  The following LU decomposition of the problem was calculated by the
!  der0 routine:
!
!  | w2(1) w3(1)              |   |d|   |  1                    |   |e|
!  |       w2(2) w3(2)        |   |d|   | w1(2)  1              |   |e|
!  |          .     .         | * |.| = |       w1(3)  1        | * |.|
!  |                .      .  |   |.|   |              .  .     |   |.|
!  |                    w2(N) |   |d|   |                 w1  1 |   |e|
!
!  The solutions of all nx*ny equations are carried out in parallel
!
!  4m+1d+4a = 9 flops per grid point
! 
!***********************************************************************
!
      integer :: nx, ny, nz, k
      real, dimension(nx):: x,w1,w2,w3
      real, dimension(nx,ny,nz):: f,d
!hpf$ distribute *(*,*,block) :: f, d
      real, dimension(ny,nz):: dfa,dfb,dfc
      real :: cxa, cxb, a, b
!
!  Check if we have a degenerate case (no x-extension)
!
      if (nx.eq.1) then
        d(:,:,:)=0.0
        return
      endif
      call der0 (nx,x,w1,w2,w3)
!
!  First point
!
      cxa=1./(x(3)-x(2))
      cxb=1./(x(2)-x(1))
      dfa=f(3,:,:)-f(2,:,:)
      dfb=f(2,:,:)-f(1,:,:)
      d(1,:,:)=2.*(dfa*cxa*cxa*cxa-dfb*cxb*cxb*cxb)
!
!  Interior points [2m+2s]
!
      do k=2,nx-1
        a=1./(x(k+1)-x(k))**2
        b=1./(x(k)-x(k-1))**2
        d(k,:,:)=(f(k+1,:,:)-f(k,:,:))*a+(f(k,:,:)-f(k-1,:,:))*b
      enddo
!
!  Last point
!
      cxa=1./(x(nx-1)-x(nx-2))
      cxb=1./(x(nx)-x(nx-1))
      dfa=f(nx-1,:,:)-f(nx-2,:,:)
      dfb=f(nx,:,:)-f(nx-1,:,:)
      d(nx,:,:)=2.*(dfa*cxa*cxa*cxa-dfb*cxb*cxb*cxb)
!
!  Do the forward substitution [1m+1a]
!
      d(1 ,:,:)=d(1 ,:,:)+w1(1 )*d(2   ,:,:)
      d(nx,:,:)=d(nx,:,:)+w3(nx)*d(nx-1,:,:)
      do k=2,nx
        d(k,:,:)=d(k,:,:)+w1(k)*d(k-1,:,:)
      enddo
!
!  Do the backward substitution [1m+1d+1s]
!
      d(nx,:,:)=d(nx,:,:)*w2(nx)
      do k=nx-1,1,-1
        d(k,:,:)=(d(k,:,:)-w3(k)*d(k+1,:,:))*w2(k)
      enddo
!
      end
!***********************************************************************
      subroutine yder (nx,ny,nz,y,f,d)
!
!  The following LU decomposition of the problem was calculated by the
!  der0 routine:
!
!  | w2(1) w3(1)              |   |d|   |  1                    |   |e|
!  |       w2(2) w3(2)        |   |d|   | w1(2)  1              |   |e|
!  |          .     .         | * |.| = |       w1(3)  1        | * |.|
!  |                .      .  |   |.|   |              .  .     |   |.|
!  |                    w2(N) |   |d|   |                 w1  1 |   |e|
!
!  The solutions of all nx*ny equations are carried out in parallel
!
!  4m+1d+4a = 9 flops per grid point
! 
!***********************************************************************
!
      integer :: nx, ny, nz, k
      real, dimension(ny):: y,w1,w2,w3
      real, dimension(nx,ny,nz):: f,d
!hpf$ distribute *(*,*,block) :: f, d
      real, dimension(nx,nz):: dfa,dfb,dfc
      real :: cya, cyb, a, b
!
!  Check if we have a degenerate case (no z-extension)
!
      if (ny.eq.1) then
        d(:,1,:)=0.0
        return
      endif
      call der0 (ny,y,w1,w2,w3)
!
!  First point
!
      cya=1./(y(3)-y(2))
      cyb=1./(y(2)-y(1))
      dfa=f(:,3,:)-f(:,2,:)
      dfb=f(:,2,:)-f(:,1,:)
      d(:,1,:)=2.*(dfa*cya*cya*cya-dfb*cyb*cyb*cyb)
!
!  Interior points [2m+2s]
!
      do k=2,ny-1
        a=1./(y(k+1)-y(k))**2
        b=1./(y(k)-y(k-1))**2
        d(:,k,:)=(f(:,k+1,:)-f(:,k,:))*a+(f(:,k,:)-f(:,k-1,:))*b
      enddo
!
!  Last point
!
      cya=1./(y(ny-1)-y(ny-2))
      cyb=1./(y(ny)-y(ny-1))
      dfa=f(:,ny-1,:)-f(:,ny-2,:)
      dfb=f(:,ny,:)-f(:,ny-1,:)
      d(:,ny,:)=2.*(dfa*cya*cya*cya-dfb*cyb*cyb*cyb)
!
!  Do the forward substitution [1m+1a]
!
      d(:,1 ,:)=d(:,1 ,:)+w1(1 )*d(:,2   ,:)
      d(:,ny,:)=d(:,ny,:)+w3(ny)*d(:,ny-1,:)
      do k=2,ny
        d(:,k,:)=d(:,k,:)+w1(k)*d(:,k-1,:)
      enddo
!
!  Do the backward substitution [1m+1d+1s]
!
      d(:,ny,:)=d(:,ny,:)*w2(ny)
      do k=ny-1,1,-1
        d(:,k,:)=(d(:,k,:)-w3(k)*d(:,k+1,:))*w2(k)
      enddo
!
      end
!***********************************************************************
      subroutine zder (nx,ny,nz,z,f,d)
!
!  The following LU decomposition of the problem was calculated by the
!  der0 routine:
!
!  | w2(1) w3(1)              |   |d|   |  1                    |   |e|
!  |       w2(2) w3(2)        |   |d|   | w1(2)  1              |   |e|
!  |          .     .         | * |.| = |       w1(3)  1        | * |.|
!  |                .      .  |   |.|   |              .  .     |   |.|
!  |                    w2(N) |   |d|   |                 w1  1 |   |e|
!
!  The solutions of all nx*ny equations are carried out in parallel
!
!  4m+1d+4a = 9 flops per grid point
! 
!***********************************************************************
!
      integer :: nx, ny, nz, k
      real, dimension(nz):: z,w1,w2,w3
      real, dimension(nx,ny,nz):: f,d
!hpf$ distribute *(*,*,block) :: f, d
      real, dimension(nx,ny):: dfa,dfb,dfc
      real :: cza, czb, a, b
!
!  Check if we have a degenerate case (no z-extension)
!
      if (nz.eq.1) then
        d(:,:,1)=0.0
        return
      endif
      call der0 (nz,z,w1,w2,w3)
!
!  First point
!
      cza=1./(z(3)-z(2))
      czb=1./(z(2)-z(1))
      dfa=f(:,:,3)-f(:,:,2)
      dfb=f(:,:,2)-f(:,:,1)
      d(:,:,1)=2.*(dfa*cza*cza*cza-dfb*czb*czb*czb)
!
!  Interior points [2m+2s]
!
      do k=2,nz-1
        a=1./(z(k+1)-z(k))**2
        b=1./(z(k)-z(k-1))**2
        d(:,:,k)=(f(:,:,k+1)-f(:,:,k))*a+(f(:,:,k)-f(:,:,k-1))*b
      enddo
!
!  Last point
!
      cza=1./(z(nz-1)-z(nz-2))
      czb=1./(z(nz)-z(nz-1))
      dfa=f(:,:,nz-1)-f(:,:,nz-2)
      dfb=f(:,:,nz)-f(:,:,nz-1)
      d(:,:,nz)=2.*(dfa*cza*cza*cza-dfb*czb*czb*czb)
!
!  Do the forward substitution [1m+1a]
!
      d(:,:,1 )=d(:,:,1 )+w1(1 )*d(:,:,2   )
      d(:,:,nz)=d(:,:,nz)+w3(nz)*d(:,:,nz-1)
      do k=2,nz
        d(:,:,k)=d(:,:,k)+w1(k)*d(:,:,k-1)
      enddo
!
!  Do the backward substitution [1m+1d+1s]
!
      d(:,:,nz)=d(:,:,nz)*w2(nz)
      do k=nz-1,1,-1
        d(:,:,k)=(d(:,:,k)-w3(k)*d(:,:,k+1))*w2(k)
      enddo
!
      end
!***********************************************************************
      subroutine xinterp (nx,ny,nz,x,nt,xt,f,ft)
!
!  Interpolate to a new z-grid.
!
      integer :: nx, ny, nz, nt, k, n
      real, dimension(nx):: x
      real, dimension(nt):: xt
      real, dimension(nx,ny,nz):: f,ft
!hpf$ distribute *(*,*,block) :: f, ft
      real, dimension(nx,ny,nz):: d
!hpf$ distribute  (*,*,block) :: d
      real :: dx, px, qx, pxf, pxd, qxd
!-----------------------------------------------------------------------
!
!  Get derivatives of f
!
      call xder (nx,ny,nz,x,f,d)
!
!  Depth loop
!
      n=2
      do k=1,nt
!
!  Find n such that  z(n-1) < zt(k) < z(n)
!
101     if (xt(k).gt.x(n)) then
          n=n+1
          if (n.gt.nx) then
            n=nx
            goto 103
          endif
          goto 101
        endif
!
!  Cubic interpolation
!
103     dx=(x(n)-x(n-1))
        px=(xt(k)-x(n-1))/dx
        qx=1.-px
!
        pxf=px-(qx*px)*(qx-px)
        pxd=-px*(qx*px)*dx
        qxd=qx*(qx*px)*dx
        ft(k,:,:)=f(n-1,:,:)+pxf*(f(n,:,:)-f(n-1,:,:)) &
     &       +qxd*d(n-1,:,:)+pxd*d(n,:,:)
!
!  End x loop
!
      enddo
!
      end
!***********************************************************************
      subroutine yinterp (nx,ny,nz,y,nt,yt,f,ft)
!
!  Interpolate to a new z-grid.
!
      integer :: nx, ny, nz, nt, k, n
      real, dimension(ny):: y
      real, dimension(nt):: yt
      real, dimension(nx,ny,nz):: f,ft
!hpf$ distribute *(*,*,block) :: f, ft
      real, dimension(nx,ny,nz):: d
!hpf$ distribute  (*,*,block) :: d
      real :: dy, py, qy, pyf, pyd, qyd
!-----------------------------------------------------------------------
!
!  Get derivatives of f
!
      call yder (nx,ny,nz,y,f,d)
!
!  Depth loop
!
      n=2
      do k=1,nt
!
!  Find n such that  y(n-1) < yt(k) < y(n)
!
101     if (yt(k).gt.y(n)) then
          n=n+1
          if (n.gt.ny) then
            n=ny
            goto 103
          endif
          goto 101
        endif
!
!  Cubic interpolation
!
103     dy=(y(n)-y(n-1))
        py=(yt(k)-y(n-1))/dy
        qy=1.-py
!
        pyf=py-(qy*py)*(qy-py)
        pyd=-py*(qy*py)*dy
        qyd=qy*(qy*py)*dy
        ft(:,k,:)=f(:,n-1,:)+pyf*(f(:,n,:)-f(:,n-1,:)) &
     &       +qyd*d(:,n-1,:)+pyd*d(:,n,:)
!
!  End y loop
!
      enddo
!
      end
!***********************************************************************
      subroutine zinterp (nx,ny,nz,z,nt,zt,f,ft)
!
!  Interpolate to a new z-grid.
!
      integer :: nx, ny, nz, nt, k, n
      real, dimension(nz):: z
      real, dimension(nt):: zt
      real, dimension(nx,ny,nz):: f
      real, dimension(nx,ny,nt):: ft
!hpf$ distribute *(*,*,block) :: f, ft
      real, dimension(nx,ny,nz):: d
!hpf$ distribute  (*,*,block) :: d
      real :: dz, pz, qz, pzf, pzd, qzd
!-----------------------------------------------------------------------
!
!  Get derivatives of f
!
      call zder (nx,ny,nz,z,f,d)
!
!  Depth loop
!
      n=2
      do k=1,nt
!
!  Find n such that  z(n-1) < zt(k) < z(n)
!
101     if (zt(k).gt.z(n)) then
          n=n+1
          if (n.gt.nz) then
            n=nz
            goto 103
          endif
          goto 101
        endif
!
!  Cubic interpolation
!
103     dz=(z(n)-z(n-1))
        pz=(zt(k)-z(n-1))/dz
        qz=1.-pz
!
        pzf=pz-(qz*pz)*(qz-pz)
        pzd=-pz*(qz*pz)*dz
        qzd=qz*(qz*pz)*dz
        ft(:,:,k)=f(:,:,n-1)+pzf*(f(:,:,n)-f(:,:,n-1)) &
     &       +qzd*d(:,:,n-1)+pzd*d(:,:,n)
!
!  End z loop
!
      enddo
!
      end
!***********************************************************************
      subroutine der_test
      integer, parameter:: mx=35,my=31,mz=32
      real, dimension(mx,my,mz):: f,d
!hpf$ distribute (*,*,block) :: f, d
      real, dimension(mx):: x
      real, dimension(my):: y
      real, dimension(mz):: z
      integer :: n
      real :: eps

      x = 0.1*(/(n, n=1,mx)/)
      do n=1,mx
        f(n,:,:) = x(n)**3
      enddo
      call xder (mx,my,mz,x,f,d)
      do n=1,mx
        eps=maxval(d(n,:,:))-minval(d(n,:,:))
        write(*,'(i3,3f10.6)') n,3.*x(n)**2,d(n,1,1),eps
      enddo

      y = 0.1*(/(n, n=1,my)/)
      do n=1,my
        f(:,n,:) = y(n)**3
      enddo
      call yder (mx,my,mz,y,f,d)
      do n=1,my
        eps=maxval(d(:,n,:))-minval(d(:,n,:))
        write(*,'(i3,3f10.6)') n,3.*y(n)**2,d(1,n,1),eps
      enddo

      z = 0.1*(/(n, n=1,mz)/)
      do n=1,mz
        f(:,:,n) = z(n)**3
      enddo
      call zder (mx,my,mz,z,f,d)
      do n=1,mz
        eps=maxval(d(:,:,n))-minval(d(:,:,n))
        write(*,'(i3,3f10.6)') n,3.*z(n)**2,d(1,1,n),eps
      enddo

      end
!***********************************************************************
      subroutine interp_test
      integer, parameter:: mx=35,my=31,mz=32
      real, dimension(mx,my,mz):: f, f1, f2
!hpf$ distribute (*,*,block) :: f, f1, f2
      real, dimension(mx):: x, x1
      real, dimension(my):: y, y1
      real, dimension(mz):: z, z1
      integer :: n
      real :: eps, dx, dy, dz

      dx=1./mx
      x = dx*(/(n, n=1,mx)/)
      x1 = x*(1.+x*(1-x))
      do n=1,mx
        f(n,:,:) = (x(n)/dx)**3
      enddo
      call xinterp(mx,my,mz,x,mx,x1,f,f1)
      do n=1,mx
        eps=maxval(f1(n,:,:))-minval(f1(n,:,:))
        write(*,'(i3,3f15.6)') n,(x1(n)/dx)**3,f1(n,1,1),eps
      enddo

      dy=1./my
      y = dy*(/(n, n=1,my)/)
      y1 = y*(1.+y*(1-y))
      do n=1,my
        f(:,n,:) = (y(n)/dy)**3
      enddo
      call yinterp(mx,my,mz,y,my,y1,f,f1)
      do n=1,my
        eps=maxval(f1(:,n,:))-minval(f1(:,n,:))
        write(*,'(i3,3f15.6)') n,(y1(n)/dy)**3,f1(1,n,1),eps
      enddo

      dz=1./mz
      z = dz*(/(n, n=1,mz)/)
      z1 = z*(1.+z*(1-z))
      do n=1,mz
        f(:,:,n) = (z(n)/dz)**3
      enddo
      call zinterp(mx,my,mz,z,mz,z1,f,f1)
      do n=1,mz
        eps=maxval(f1(:,:,n))-minval(f1(:,:,n))
        write(*,'(i3,3f15.6)') n,(z1(n)/dz)**3,f1(1,1,n),eps
      enddo

      end
