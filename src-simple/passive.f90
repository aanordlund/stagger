! $Id: passive.f90,v 1.3 2003/04/01 22:19:40 aake Exp $
!***********************************************************************
  SUBROUTINE passive(nx,ny,nz,nu,r,px,py,pz,Ux,Uy,Uz,d,dddt)

  USE params
  USE stagger

  integer nx,ny,nz
  real, dimension(nx,ny,nz) :: &
    r,px,py,pz,Ux,Uy,Uz,d,dddt, &
    nu,lnu,dd,fx,fy,fz

  character(len=72) id
  data id/'$Id: passive.f90,v 1.3 2003/04/01 22:19:40 aake Exp $'/

  if (.not. do_pscalar) then
    dddt = 0.
    return
  end if

  if (id .ne. ' ') print *,id ; id = ' '


!-----------------------------------------------------------------------
!  Passive scalar
!-----------------------------------------------------------------------
  dd = d/r
  call passive_boundary(r,Ux,Uy,Uz,d,dd)
  if (do_loginterp) then
    lnu=alog(nu)
    fx = px*xdn(dd) - exp(xdn(lnu))*ddxdn(dd)
    fy = py*ydn(dd) - exp(ydn(lnu))*ddydn(dd)
    fz = pz*zdn(dd) - exp(zdn(lnu))*ddzdn(dd)
  else
    fx = px*xdn(dd) - xdn(nu)*ddxdn(dd)
    fy = py*ydn(dd) - ydn(nu)*ddydn(dd)
    fz = pz*zdn(dd) - zdn(nu)*ddzdn(dd)
  end if
  dddt = dddt - ddxup(fx) - ddyup(fy) - ddzup(fz)

  END
