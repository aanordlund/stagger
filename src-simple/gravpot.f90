! $Id: gravpot.f90,v 1.1 2003/03/10 14:40:44 jaab Exp $
!***********************************************************************

!  Green's function for the Laplace operator
  subroutine init_grav (green, eps, ddx, ddy, ddz)
  use params
  implicit none

  real eps, ddx, ddy, ddz
  real, dimension(mx,my,mz) :: green
!hpf$ align with TheCube:: green
  real kx(mx), ky(my), kz(mz), k2, knorm
  integer i, j, k
!-----------------------------------------------------------------------

  kx = (/(i,i=0,mx-1)/)
  ky = (/(i,i=0,my-1)/)
  kz = (/(i,i=0,mz-1)/)
  kx(mx/2+2:mx) = kx(mx/2+2:mx) - float(mx)
  ky(my/2+2:my) = ky(my/2+2:my) - float(my)
  kz(mz/2+2:mz) = kz(mz/2+2:mz) - float(mz)
  kx = (2.0*pi/(ddx*mx)*kx)**2 + 1e-20
  ky = (2.0*pi/(ddy*my)*ky)**2 + 1e-20
  kz = (2.0*pi/(ddz*mz)*kz)**2 + 1e-20

  knorm = 2.*eps**2/kx(mx/2+1)
  do k=1,mz
    do j=1,my
      do i=1,mx
	    k2 = kx(i) + ky(j) + kz(k)
        green(i,j,k) = -(1./k2)*exp(-knorm*k2)
      end do
    end do
  end do
  green(1,1,1) = 0.0    ! NB 1/k^2 is NaN at k=0
!
  end subroutine init_grav

!***********************************************************************
  subroutine gravpot (rho, phi, gravity, eps, ddx, ddy, ddz, isolated_gravity)
  use params
  use stagger
  implicit none

  integer, parameter:: NF=256/2   ! NF/2 because coeff is complex -- see man ccfft3d
  real, dimension(mx,my,mz):: rho, phi, green, k2, scr
!hpf$ align with TheCube:: rho, phi, green
  real    :: gravity, eps, ddx, ddy, ddz, rhoa
  logical :: isolated_gravity
  integer :: nperif
  complex, dimension(mx,my,mz) :: phicmplx
!hpf$ align with TheCube:: phicmplx
  complex, dimension(mx+NF+my+NF+mz+NF) :: coeff

  REAL work (2*MAX(mx,my,mz))
  INTEGER ISYS(0:1)
  
  character:: id*72 = "$Id: gravpot.f90,v 1.1 2003/03/10 14:40:44 jaab Exp $"
!-----------------------------------------------------------------------
  ISYS(0)=1

  if (id .ne. '') then
    print *,id
    print *,'grav,eps,dx=',gravity,eps,dx
    id = ''
  end if

  if (gravity .eq. 0) then
    phi = 0.
    return
  endif
  call init_grav (green, eps, ddx, ddy, ddz)

  if (isolated_gravity) then
    scr = rho
    scr(1,:,:) = 0.
    scr(:,1,:) = 0.
    scr(:,:,1) = 0.
    scr(mx,:,:) = 0.
    scr(:,my,:) = 0.
    scr(:,:,mz) = 0.

    rhoa = sum(scr)/mw
    nperif = 2*(mx*my+mx*mz+my*mz)-4*(mx+my+mz)+8
    rhoa = (rhoa*mx*my*mz)/nperif

    scr(1,:,:) = -rhoa
    scr(:,1,:) = -rhoa
    scr(:,:,1) = -rhoa
    scr(mx,:,:) = -rhoa
    scr(:,my,:) = -rhoa
    scr(:,:,mz) = -rhoa

    phicmplx = cmplx(scr,0.0)
  else
    phicmplx = cmplx(rho,0.0)
  end if

  call ccfft3d ( 0, mx, my, mz, 0.0, 0, 0, 0, 0, 0, 0, coeff, 0, 0)
  call ccfft3d (+1, mx, my, mz, 1.0, phicmplx, mx, my, phicmplx, &
                    mx, my, coeff, work, isys)
  
  phicmplx = gravity*green*phicmplx

  call ccfft3d (-1, mx, my, mz, 1.0, phicmplx, mx, my, phicmplx, &
                    mx, my, coeff, work, isys)

  phi = real(phicmplx)/(mx*my*mz)

  end subroutine gravpot 

!********************************************************************
  subroutine gravpot_stretched (rho, Phi, tol, eps, maxit)
    use params
    use stagger
    use mesh
    implicit none

    real,dimension(mx,my,mz) :: rho, Phi
    real,dimension(mx,my,mz) :: rho_s, resid, rms1
    real                     :: tol, jacf, safe1
    real                     :: eps
    real                     :: dxmin, dymin, dzmin, error
    real                     :: rms
    integer                  :: maxit, exitpoint
    integer                  :: iter
    logical                  :: isolated_gravity
    intent(in) :: rho
    isolated_gravity = .false.
    jacf  = 3.0

! Smooth rho (not for real) to make Phi well behaved
    rho_s = smooth3(rho)

    !Safety factor for Jacobi-step (diffusion CFL)
    dxmin = minval(dx1up)
    dymin = minval(dy1up)
    dzmin = minval(dz1up)
    safe1 = 1.0/(jacf*(1.0/dxmin**2 + 1.0/dymin**2 + 1.0/dzmin**2))

!Root-mean-square for finite error estimate
    rms1 = rho_s - volaverage(rho_s)
    rms1 = rms1*rms1
    rms  = sqrt(volaverage(rms1))
    rms1 = 1.0/(rho_s+rms)

    do iter=1,maxit
! Step 1 : Jacobi method (fast for high frequencies)
      resid =   grav*rho_s        &
              - ddxup(ddxdn(Phi)) &
              - ddyup(ddydn(Phi)) &
              - ddzup(ddzdn(Phi))

      resid = resid - volaverage(resid)
      if (idbg .ge. 1) print *,"Selfgravity: RESIDUE jacobi:   max = ", maxval(abs(resid))

      Phi   = Phi - resid*safe1

      ! Calculate error 
      error = maxval(abs(resid*rms1))/grav
      exitpoint = 1
      if (error .lt. tol) exit

! Step 2 : Interpolation+FFT method (fast for low frequencies)
      resid =   grav*rho_s        &
              - ddxup(ddxdn(Phi)) &
              - ddyup(ddydn(Phi)) &
              - ddzup(ddzdn(Phi))

      resid = resid - volaverage(resid)
      if (idbg .ge. 1) print *,"Selfgravity: RESIDUE interpol: max = ", maxval(abs(resid))
!
      ! Calculate error 
      error = maxval(abs(resid*rms1))/grav
      exitpoint = 2
      if (error .lt. tol) exit

      resid = unstretch(resid) ! -> equidistant mesh
      call gravpot(resid, resid, 1.0, 0.0, Sx/mx, Sy/my, Sz/mz, isolated_gravity)
      resid = stretch(resid)   ! -> variable mesh
      Phi   = Phi + resid
!
      ! Calculate error 
      error = maxval(abs(resid*rms1))/grav
      exitpoint = 3
      if (error .lt. tol) exit

    end do
    print *,'Selfgravity: it, error, exitpoint = ', iter, error, exitpoint

  end subroutine gravpot_stretched
 
