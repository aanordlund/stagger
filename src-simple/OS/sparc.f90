! $Id: sparc.f90,v 1.3 2003/02/01 14:44:08 aake Exp $
!***********************************************************************
  function elapsed()

!  Return the elapsed cpu + system time since the previous call

  real cpu(2)
  elapsed = dtime(cpu)
  elapsed = cpu(1)+cpu(2)
  end
!***********************************************************************
  subroutine oflush
 
!  flush stdout (101) and stderr (100 or 102 -- who knows)
 
  call flush(5)
  call flush(6)
  end
!***********************************************************************
  integer function lrec(m)

!  Return the RECL=LREC(M) value, for M words.

  lrec = 4*m
  end
!***********************************************************************
  integer*2 function icompare(a,b)

! compare two reals, for use in qsort

  if (a.gt.b) then
    icompare=1
  else if (a.lt.b) then
    icompare=-1
  else
    icompare=0
  endif
  end function
!***********************************************************************
  FUNCTION ran1s(idum)
  INTEGER idum,IA,IM,IQ,IR,NTAB,NDIV
  REAL ran1s,AM,EPS,RNMX
  PARAMETER (IA=16807,IM=2147483647,AM=1./IM,IQ=127773,IR=2836, &
  NTAB=32,NDIV=1+(IM-1)/NTAB,EPS=1.2e-7,RNMX=1.-EPS)
  INTEGER k,iy
  if (idum.le.0) then
    idum=max(-idum,1)
  endif
  k=idum/IQ
  idum=IA*(idum-k*IQ)-IR*k
  if (idum.lt.0) idum=idum+IM
  iy=idum
  ran1s=min(AM*iy,RNMX)
  return
  END
!***********************************************************************
  FUNCTION average (f)
  USE params

!  Return average of array.  Make sure precision is retained, by 
!  summing first individual rows, then columns, then planes.

  real:: f(mx,my,mz), fa(my,mz), fb(mz)
!-----------------------------------------------------------------------

!$ doacross local(k,j,fa), shared(fb)
  do k=1,mz
    do j=1,my
      fa(j,k) = sum(f(:,j,k))
    end do
    fb(k) = sum(fa(:,k))
  end do

  average = sum(fb)*(1./mx)*(1./my)*(1./mz)

  END
!***********************************************************************
  subroutine aver (f, af, a1, a2)

  USE params
  real f(mx,my,mz), tmp(mx,my), av(mz), fmin(mz), fmax(mz)
!hpf$ align with TheCube:: f
!$ do across local(k,tmp)
!-----------------------------------------------------------------------

  do k=1,mz
    tmp=f(:,:,k)
    av(k)=sum(tmp)
    fmin(k)=minval(tmp)
    fmax(k)=maxval(tmp)
  end do
  af = sum(av)/mw
  a1 = minval(fmin)
  a2 = maxval(fmax)
  end
