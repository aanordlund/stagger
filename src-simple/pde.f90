! $Id: pde.f90,v 1.14 2003/04/11 07:21:19 aake Exp $
!***********************************************************************
  SUBROUTINE pde(r,px,py,pz,e,d,drdt,dpxdt,dpydt,dpzdt,dedt,dddt,flag, &
                 Bx,By,Bz,dBxdt,dBydt,dBzdt)
!
!  This is a "demo" version of the HD/MHD pde routine -- formulated for
!  clarity rather than speed.  However, any routine that has been made
!  to work in this context should be easy to port to the production 
!  source directory.
!-----------------------------------------------------------------------
  USE params
  USE stagger
  USE forcing
  USE eos
  implicit none

  logical flag
  real ds, gam1
  real, dimension(mx,my,mz) :: &
       r,px,py,pz,e,d,drdt,dpxdt,dpydt,dpzdt,dedt,dddt, &
       Bx,By,Bz,dBxdt,dBydt,dBzdt
  real, allocatable, dimension(:,:,:) :: &
       P,Cs,divU,nu,ee,Ux,Uy,Uz,yy, &
       Sxx,Syy,Szz,Sxy,Szx,Syz,xdnr,ydnr,zdnr, &
       eta,Txx,Tyy,Tzz,Txy,Tzx,Tyz,ddxd,ddyd,ddzd
  character(len=72) id
  data id/'$Id: pde.f90,v 1.14 2003/04/11 07:21:19 aake Exp $'/

  if (id .ne. ' ') print *,id ; id = ' '

!-----------------------------------------------------------------------
!  Velocities
!-----------------------------------------------------------------------
  allocate (xdnr(mx,my,mz), ydnr(mx,my,mz), zdnr(mx,my,mz))
  call density_boundary(r)
  xdnr = xdn(r)
  ydnr = ydn(r)
  zdnr = zdn(r)
  allocate (Ux(mx,my,mz), Uy(mx,my,mz), Uz(mx,my,mz))
  Ux = px/xdnr
  Uy = py/ydnr
  Uz = pz/zdnr
  call velocity_boundary(r,xdnr,ydnr,zdnr,Ux,Uy,Uz,px,py,pz,e,Bx,By,Bz)
  px = Ux*xdnr
  py = Uy*ydnr
  pz = Uz*zdnr

  call force (mx,my,mz,r,Ux,Uy,Uz,dpxdt,dpydt,dpzdt)

!-----------------------------------------------------------------------
!  Numerical viscosity, shock capturing part
!-----------------------------------------------------------------------
  allocate (Cs(mx,my,mz),nu(mx,my,mz),ee(mx,my,mz))
  if (do_momenta) then
    allocate (Sxx(mx,my,mz),Syy(mx,my,mz),Szz(mx,my,mz))
    Sxx = ddxup(Ux)
    Syy = ddyup(Uy)
    Szz = ddzup(Uz)

!-----------------------------------------------------------------------
!  Pressure, sound propagation speed
!-----------------------------------------------------------------------
    allocate (P(mx,my,mz))
    if (gamma .eq. 1.) then
      gam1 = gamma
    else
      gam1 = gamma-1.
    endif
    if (.not. do_energy) e = r**gamma
    ee = e/r
    if (do_ionization) then
      P = pressure(r,ee)
    else
      P = gam1*e
    endif
    call  energy_boundary(r,Ux,Uy,Uz,e,ee,p,Bx,By,Bz)
    Cs = sqrt(gamma*P/r)
    Cs = Cs + sqrt(xup(Ux)**2 + yup(Uy)**2  + zup(Uz)**2)
    if (nu2 .ne. 0.) then
      nu = Sxx+Syy+Szz
      nu = max(-nu,0.0)
    end if
  else
    Cs = sqrt(xup(Ux)**2 + yup(Uy)**2  + zup(Uz)**2)
    nu = 0.
  end if

!-----------------------------------------------------------------------
!  Numerical viscosity, Richtmyer & Morton type
!-----------------------------------------------------------------------
  ds = max(dx,dy,dz)
  nu = (nu1*ds)*Cs + (nu2*ds**2)*nu
  call regularize(nu)
  nu = smooth3(max5(nu))
  if (flag) then
    ds = min(dx,dy,dz)
    Cv = 6.2*3.*maxval(nu)*0.75/2.*dt/ds**2
    Cu = maxval(Cs)*dt/dx
  end if
  nu = nu*r
  deallocate (Cs)

!-----------------------------------------------------------------------
!  Passive scalar advection
!-----------------------------------------------------------------------
  if (do_pscalar) then
    call passive(mx,my,mz,nu,r,px,py,pz,Ux,Uy,Uz,d,dddt)
  end if

!-----------------------------------------------------------------------
!  Viscous stress, diagonal and off-diagonal elements
!-----------------------------------------------------------------------
if (do_momenta) then
  allocate (Txx(mx,my,mz),Tyy(mx,my,mz),Tzz(mx,my,mz))
  Txx = - nu*Sxx + P
  Tyy = - nu*Syy + P
  Tzz = - nu*Szz + P
  if (do_energy) dedt = dedt &
    - Txx*Sxx &
    - Tyy*Syy &
    - Tzz*Szz
  deallocate (P,Sxx,Syy,Szz)

  allocate (Sxy(mx,my,mz),Syz(mx,my,mz),Szx(mx,my,mz))
  Sxy = (ddxdn(Uy)+ddydn(Ux))*0.5
  Syz = (ddydn(Uz)+ddzdn(Uy))*0.5
  Szx = (ddzdn(Ux)+ddxdn(Uz))*0.5
  allocate (Txy(mx,my,mz),Tyz(mx,my,mz),Tzx(mx,my,mz))
  Txy = - xdn(ydn(nu))*Sxy
  Tyz = - ydn(zdn(nu))*Syz
  Tzx = - zdn(xdn(nu))*Szx
  if (do_energy) dedt = dedt &
      - 2.*xup(yup(Txy*Sxy)) &
      - 2.*yup(zup(Tyz*Syz)) &
      - 2.*zup(xup(Tzx*Szx))
  deallocate (Sxy,Syz,Szx)

!-----------------------------------------------------------------------
!  Add Reynolds stress
!-----------------------------------------------------------------------
  Txx = Txx + r*xup(Ux)**2
  Tyy = Tyy + r*yup(Uy)**2
  Tzz = Tzz + r*zup(Uz)**2
  Txy = Txy + (xdn(ydnr))*ydn(Ux)*xdn(Uy)
  Tyz = Tyz + (ydn(zdnr))*zdn(Uy)*ydn(Uz)
  Tzx = Tzx + (zdn(xdnr))*xdn(Uz)*zdn(Ux)
  if (grav .ne. 0.) dpydt = dpydt - grav*ydnr
  deallocate (xdnr,ydnr,zdnr)

!-----------------------------------------------------------------------
!  Equations of motion, face centred
!-----------------------------------------------------------------------
  dpxdt = dpxdt - ddxdn (Txx) - ddyup(Txy) - ddzup(Tzx)
  dpydt = dpydt - ddydn (Tyy) - ddzup(Tyz) - ddxup(Txy)
  dpzdt = dpzdt - ddzdn (Tzz) - ddxup(Tzx) - ddyup(Tyz)
  deallocate (Txy,Tyz,Tzx,Txx,Tyy,Tzz)
else
  deallocate (xdnr,ydnr,zdnr)
end if

!-----------------------------------------------------------------------
!  MHD
!-----------------------------------------------------------------------
  if (do_mhd) then
    allocate (eta(mx,my,mz))
    call resist(eta,r,ee,Ux,Uy,Uz,Bx,By,Bz)
    call mhd(eta,Ux,Uy,Uz,Bx,By,Bz,dpxdt,dpydt,dpzdt,dedt,dBxdt,dBydt,dBzdt)
    deallocate (eta)
  else
    Cb =0.
  end if
  deallocate (Ux,Uy,Uz)

!-----------------------------------------------------------------------
!  Energy equation
!-----------------------------------------------------------------------
  if (do_energy) then
    nu = nu*2.
    dedt = dedt &
       - ddxup (xdn(ee)*px - xdn(nu)*ddxdn(ee)) &
       - ddyup (ydn(ee)*py - ydn(nu)*ddydn(ee)) &
       - ddzup (zdn(ee)*pz - zdn(nu)*ddzdn(ee))
  end if
  deallocate (ee)
  if (do_momenta) deallocate (nu)

!-----------------------------------------------------------------------
!  Mass conservation, cell centered
!-----------------------------------------------------------------------
  if (do_density) drdt = drdt - ddxup(px) - ddyup(py) - ddzup(pz)
  call ddt_boundary (drdt,dpxdt,dpydt,dpzdt,dedt)

!-----------------------------------------------------------------------
!  Courant conditions
!-----------------------------------------------------------------------
  if (flag) then
    Cr = 0.
    Ce = 0.
    if (do_density) Cr = dt*maxval(abs(drdt/r))
    if (do_energy)  Ce = dt*maxval(abs(dedt/e))
    dt = dt*Cdt/max(Cu,Cv,Cr,Ce,Cb)
  end if

  END
