! $Id: nosymmetry.f90,v 1.1 2003/04/10 07:44:53 aake Exp $

! Boundary conditions.  The indices are assumed to be arranged like so:
!
!                                lb           ub
! centers:    1   2   3   4   5   6 ...       -5  -4  -3  -2  -1   0
! faces  :  1   2   3   4   5   6   ... -6  -5  -4  -3  -2  -1   0 
! 
!-----------------------------------------------------------------------
SUBROUTINE symmetric_center_lower(f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
END

!-----------------------------------------------------------------------
SUBROUTINE symmetric_center_upper(f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
END

!-----------------------------------------------------------------------
SUBROUTINE symmetric_center(f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f

  call symmetric_center_lower(f)
  call symmetric_center_upper(f)
END

!-----------------------------------------------------------------------
SUBROUTINE symmetric_face_lower(f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
END

!-----------------------------------------------------------------------
SUBROUTINE symmetric_face_upper(f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
END

!-----------------------------------------------------------------------
SUBROUTINE symmetric_face(f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f

  call symmetric_face_lower(f)
  call symmetric_face_upper(f)
END

!-----------------------------------------------------------------------
SUBROUTINE antisymmetric_center_lower(f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
END

!-----------------------------------------------------------------------
SUBROUTINE antisymmetric_center_upper(f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
END

!-----------------------------------------------------------------------
SUBROUTINE antisymmetric_center(f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f

  call antisymmetric_center_lower(f)
  call antisymmetric_center_upper(f)
END

!-----------------------------------------------------------------------
SUBROUTINE antisymmetric_face_lower(f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
END

!-----------------------------------------------------------------------
SUBROUTINE antisymmetric_face_upper(f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
END

!-----------------------------------------------------------------------
SUBROUTINE antisymmetric_face(f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f

  call antisymmetric_face_lower(f)
  call antisymmetric_face_upper(f)
END

!-----------------------------------------------------------------------
SUBROUTINE extrapolate_center_lower(f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
END

!-----------------------------------------------------------------------
SUBROUTINE extrapolate_center_upper(f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
END

!-----------------------------------------------------------------------
SUBROUTINE extrapolate_center(f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f

  call extrapolate_center_lower(f)
  call extrapolate_center_upper(f)
END

!-----------------------------------------------------------------------
SUBROUTINE extrapolate_face_lower(f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
END

!-----------------------------------------------------------------------
SUBROUTINE extrapolate_face_upper(f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
END

!-----------------------------------------------------------------------
SUBROUTINE extrapolate_face(f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f

  call extrapolate_face_lower(f)
  call extrapolate_face_upper(f)
END
