! $Id: closed_y.f90,v 1.2 2003/04/10 07:06:01 aake Exp $

!-----------------------------------------------------------------------
SUBROUTINE density_boundary (r)
  USE params
  implicit none
  real, dimension(mx,my,mz):: r
!
  lb = 6
  ub = my-5
  call symmetric_center(r)
END

!-----------------------------------------------------------------------
SUBROUTINE energy_boundary (r,Ux,Uy,Uz,e,ee,Bx,By,Bz)
  USE params
  implicit none
  real, dimension(mx,my,mz):: r,Ux,Uy,Uz,e,ee,Bx,By,Bz

  call symmetric_center(ee)
  e = r*ee
END

!-----------------------------------------------------------------------
SUBROUTINE passive_boundary (r,Ux,Uy,Uz,d,dd)
  USE params
  implicit none
  real, dimension(mx,my,mz):: r,Ux,Uy,Uz,d,dd

  call symmetric_center(dd)
  d = r*dd
END

!-----------------------------------------------------------------------
SUBROUTINE velocity_boundary (r,xdnr,ydnr,zdnr,Ux,Uy,Uz,px,py,pz,e,Bx,By,Bz)
  USE params
  implicit none
  real, dimension(mx,my,mz):: r,xdnr,ydnr,zdnr,Ux,Uy,Uz,px,py,pz,e,ee,Bx,By,Bz
!
  call symmetric_center(Ux)
  call antisymmetric_face(Uy)
  call symmetric_center(Uz)
  px = xdnr*Ux
  py = ydnr*Uy
  pz = zdnr*Uz
END

!-----------------------------------------------------------------------
SUBROUTINE mfield_boundary (Ux,Uy,Uz,Bx,By,Bz)
  USE params
  implicit none
  real, dimension(mx,my,mz):: Ux,Uy,Uz,Bx,By,Bz
!
  call antisymmetric_center(Bx)
  call antisymmetric_center(Bz)
END

!-----------------------------------------------------------------------
SUBROUTINE regularize (f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
!
  call symmetric_center(f)
END
