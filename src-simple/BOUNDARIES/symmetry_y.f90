! $Id: symmetry_y.f90,v 1.1 2003/04/10 07:44:53 aake Exp $

! Boundary conditions.  The indices are assumed to be arranged like so:
!
!                                lb           ub
! centers:    1   2   3   4   5   6 ...       -5  -4  -3  -2  -1   0
! faces  :  1   2   3   4   5   6   ... -6  -5  -4  -3  -2  -1   0 
! 
!-----------------------------------------------------------------------
SUBROUTINE symmetric_center_lower(f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
  integer:: iy,iz

  do iz=1,mz
    do iy=1,lb-1
      f(:,iy,iz) = f(:,lb+(lb-iy),iz)
    end do
  end do
END

!-----------------------------------------------------------------------
SUBROUTINE symmetric_center_upper(f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
  integer:: iy,iz

  do iz=1,mz
    do iy=ub+1,my
      f(:,iy,iz) = f(:,ub-(iy-ub),iz)
    end do
  end do
END

!-----------------------------------------------------------------------
SUBROUTINE symmetric_center(f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f

  call symmetric_center_lower(f)
  call symmetric_center_upper(f)
END

!-----------------------------------------------------------------------
SUBROUTINE symmetric_face_lower(f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
  integer:: iy,iz

  do iz=1,mz
    do iy=1,lb
      f(:,iy,iz) = f(:,lb+1+(lb-iy),iz)
    end do
  end do
END

!-----------------------------------------------------------------------
SUBROUTINE symmetric_face_upper(f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
  integer:: iy,iz

  do iz=1,mz
    do iy=ub+1,my
      f(:,iy,iz) = f(:,ub+1-(iy-ub),iz)
    end do
  end do
END

!-----------------------------------------------------------------------
SUBROUTINE symmetric_face(f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f

  call symmetric_face_lower(f)
  call symmetric_face_upper(f)
END

!-----------------------------------------------------------------------
SUBROUTINE antisymmetric_center_lower(f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
  integer:: iy,iz

  do iz=1,mz
    do iy=1,lb-1
      f(:,iy,iz) = -f(:,lb+(lb-iy),iz)
    end do
    f(:,lb,iz) = 0.
  end do
END

!-----------------------------------------------------------------------
SUBROUTINE antisymmetric_center_upper(f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
  integer:: iy,iz

  do iz=1,mz
    do iy=ub+1,my
      f(:,iy,iz) = -f(:,ub-(iy-ub),iz)
    end do
    f(:,ub,iz) = 0.
  end do
END

!-----------------------------------------------------------------------
SUBROUTINE antisymmetric_center(f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f

  call antisymmetric_center_lower(f)
  call antisymmetric_center_upper(f)
END

!-----------------------------------------------------------------------
SUBROUTINE antisymmetric_face_lower(f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
  integer:: iy,iz

  do iz=1,mz
    do iy=1,lb
      f(:,iy,iz) = -f(:,lb+1+(lb-iy),iz)
    end do
  end do
END

!-----------------------------------------------------------------------
SUBROUTINE antisymmetric_face_upper(f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
  integer:: iy,iz

  do iz=1,mz
    do iy=ub+1,my
      f(:,iy,iz) = -f(:,ub+1-(iy-ub),iz)
    end do
  end do
END

!-----------------------------------------------------------------------
SUBROUTINE antisymmetric_face(f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f

  call antisymmetric_face_lower(f)
  call antisymmetric_face_upper(f)
END

!-----------------------------------------------------------------------
SUBROUTINE extrapolate_center_lower(f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
  integer:: iy,iz

  do iz=1,mz
    do iy=1,lb-1
      f(:,iy,iz) = 2.*f(:,lb,iz)-f(:,lb+(lb-iy),iz)
    end do
  end do
END

!-----------------------------------------------------------------------
SUBROUTINE extrapolate_center_upper(f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
  integer:: iy,iz

  do iz=1,mz
    do iy=ub+1,my
      f(:,iy,iz) = 2.*f(:,ub,iz)-f(:,ub-(iy-ub),iz)
    end do
  end do
END

!-----------------------------------------------------------------------
SUBROUTINE extrapolate_center(f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f

  call extrapolate_center_lower(f)
  call extrapolate_center_upper(f)
END

!-----------------------------------------------------------------------
SUBROUTINE extrapolate_face_lower(f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
  integer:: iy,iz

  do iz=1,mz
    do iy=1,lb-1
      f(:,iy,iz) = 2.*f(:,lb,iz)-f(:,lb+2+(lb-iy),iz)
    end do
  end do
END

!-----------------------------------------------------------------------
SUBROUTINE extrapolate_face_upper(f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
  integer:: iy,iz

  do iz=1,mz
    do iy=ub+1,my
      f(:,iy,iz) = 2.*f(:,ub,iz)-f(:,ub-(iy-ub),iz)
    end do
  end do
END

!-----------------------------------------------------------------------
SUBROUTINE extrapolate_face(f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f

  call extrapolate_face_lower(f)
  call extrapolate_face_upper(f)
END

!-----------------------------------------------------------------------
SUBROUTINE boundary_test
  USE params
  real, allocatable, dimension(:,:,:):: f

  mx=15
  my=mx
  mz=mx
  lb=5
  ub=my-4
  allocate (f(mx,my,mz))

  do iy=1,my
    f(:,iy,:)=cos((iy-lb)*2.*pi/(ub-lb))
  end do

  call symmetric_center(f)
  print '(a/15f8.4)','s_c',f(1,:,1)  
  call antisymmetric_center(f)
  print '(a/15f8.4)','a_c',f(1,:,1)  
  call symmetric_face(f)
  print '(a/15f8.4)','s_f',f(1,:,1)  
  call antisymmetric_face(f)
  print '(a/15f8.4)','a_f',f(1,:,1)
  
END
