! $Id: noboundary.f90,v 1.3 2003/04/11 07:18:59 aake Exp $

!-----------------------------------------------------------------------
SUBROUTINE density_boundary (r)
  USE params
  implicit none
  real, dimension(mx,my,mz):: r
!
END

!-----------------------------------------------------------------------
SUBROUTINE energy_boundary (r,Ux,Uy,Uz,e,ee,Bx,By,Bz)
  USE params
  implicit none
  real, dimension(mx,my,mz):: r,Ux,Uy,Uz,e,ee,Bx,By,Bz

END

!-----------------------------------------------------------------------
SUBROUTINE passive_boundary (r,Ux,Uy,Uz,d,dd)
  USE params
  implicit none
  real, dimension(mx,my,mz):: r,Ux,Uy,Uz,d,dd

END

!-----------------------------------------------------------------------
SUBROUTINE velocity_boundary (r,xdnr,ydnr,zdnr,Ux,Uy,Uz,px,py,pz,e,Bx,By,Bz)
  USE params
  implicit none
  real, dimension(mx,my,mz):: r,xdnr,ydnr,zdnr,Ux,Uy,Uz,px,py,pz,e,ee,Bx,By,Bz
!
END

!-----------------------------------------------------------------------
SUBROUTINE mfield_boundary (Ux,Uy,Uz,Bx,By,Bz)
  USE params
  implicit none
  real, dimension(mx,my,mz):: Ux,Uy,Uz,Bx,By,Bz
!
END

!-----------------------------------------------------------------------
SUBROUTINE ddt_boundary (drdt,dpxdt,dpydt,dpzdt,dedt)
  USE params
  implicit none
  real, dimension(mx,my,mz):: drdt,dpxdt,dpydt,dpzdt,dedt
!
END

!-----------------------------------------------------------------------
SUBROUTINE regularize (f)
  USE params
  implicit none
  real, dimension(mx,my,mz):: f
!
END
