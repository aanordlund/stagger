; $Id: rhdsolve.pro,v 1.4 2004/11/05 10:18:23 troels_h Exp $
;***********************************************************************
;-----------------------------------------------------------------------
; small routine to complement the equivalent fortran statement
;-----------------------------------------------------------------------
FUNCTION merge, x, y, logic
 Return, logic*x + (1b-logic)*y
END
;-----------------------------------------------------------------------
; Routine to compute the complex square root
; based on numerical recipes chap. 5.4.
; Note that the chosen branch cut in the complex plane
; - here taken to be the negative real axis -
; is not enforced if as xI is input -0.0.
; This is at least true for the intel compiler ver 7.1
; If it is important run sign_test, found in
; Testing subdir.
;-----------------------------------------------------------------------
PRO SQRTC_No_Zero_2d, xR,xI,yR,yI
  half = 0.5
  mone = -1.
  one = 1.
  zero=0.
  tiny = 1e-30
  t1 = abs(xR)
  t2 = abs(xI)
  m = (t1 > t2) + tiny
  t1 = t1/m
  t2 = t2/m
  w = sqrt(m)*sqrt(half*(t1 + sqrt(t1*t1 + t2*t2))) + tiny
  m = half * abs(xI)/w
  t1 = merge(one, zero, xR ge zero)
  t2 = one - t1
  sgnd = merge(one, mone, xI ge zero)
  yR = t1*w + t2*m
  yI = sgnd*(t1*m + t2*w)
END
;-----------------------------------------------------------------------
;
; Routine to find the roots in a second degree polynomial.
; based on numerical recipes chap. 5.6
; This is the general case of complex coefficients
;-----------------------------------------------------------------------
; Cost: Total count (Log etc x 3): 47
; Intrinisics    : 8
; Add/Multiply   : 30
; Log,Sqrt,Divide: 3
;-----------------------------------------------------------------------
PRO P2_Roots, R1R, R1I, R2R, R2I, a0R, a0I, a1R, a1I
  half = 0.5
  mhalf = -0.5
  four=4.
  two=2.0
  one = 1.0
  dR = a1R*a1R - a1I*a1I - four*a0R
  dI = two*a1R*a1I - four*a0I
  RRZ = mhalf*a1R
  RIZ = mhalf*a1I
  SQRTC_No_Zero_2d, dR, dI, sqdR, sqdI
  Sgn = a1R*sqdR + a1I*sqdI
  t1 = merge(half, mhalf, sgn ge 0)
  R1R = RRZ - t1*SqdR
  R1I = RIZ - t1*SqdI
; Complex division
; Done with guard for overflow
  t1 = abs(R1R)
  t2 = abs(R1I)
  t3 = (t1 >t2)        ; vector version of max(abs(R1R), abs(R1I))
  t1 = R1R/t3
  t2 = R1I/t3
  t3 = one/(R1R*t1 + R1I*t2)
  R2R = (a0R*t1 + a0I*t2)*t3
  R2I = (a0I*t1 - a0R*t2)*t3
; Done the fast way
;  t3 = 1.0/(R1R^2 + R1I^2)
;  R2R = (a0R*R1R + a0I*R1I)*t3
;  R2I = (a0I*R1R - a0R*R1I)*t3
; Complex division finished
  determinant = ((dR eq 0.0) and (dI eq 0.0))
  R1R = Merge(RRZ, R1R, determinant)
  R1I = Merge(RIZ, R1I, determinant)
  R2R = Merge(RRZ, R2R, determinant)
  R2I = Merge(RIZ, R2I, determinant)
END
;-----------------------------------------------------------------------
; Routine to find a real root in a third degree polynomial
; based on numerical recipes chap. 5.6
;-----------------------------------------------------------------------
; Cost: Total count (Log etc x 3): 50
; Intrinisics    : 5
; Add/Multiply   : 24
; Log,Sqrt,Divide: 7
;-----------------------------------------------------------------------
PRO P3_Root, Root, a0, a1, a2
  twopi=2.0*3.141592653589793238462643383279502884197169399375105820974
  two=2.
  nine=9.0
  twoseven=27.
  one9 = 1./9.
  one54 = 1./54.
  one3 = 1./3.
  one = 1.
  zero=0.0
  three=3.0
  mtwo=-2.0
  r2 = a2^2
  q = one9*(r2 - three*a1)
  r = one54*(a2*(two*r2 - nine*a1) + twoseven*a0)
  q3 = q^3
  r2 = r^2
;
; Three Roots, which one should we choose ??
;
;  Root1 = mtwo*sqrt(q)*Cos(one3*(acos(r/sqrt(q3))-twoPI))
;  Root1 = mtwo*sqrt(q)*Cos(one3*(acos(r/sqrt(q3))+twoPI))
  r2ltq3 = (r2 lt q3)
  t2 = Merge(r/sqrt(abs(q3)),one,r2ltq3)
  Root1 = mtwo*sqrt(abs(q))*Cos(one3*(acos(t2)))
  t2 = abs(r)+sqrt(abs(r2-q3))
  t2 = t2^(one3)
  r = Merge(-t2, t2, r ge zero)
  Root = Merge(q/r,zero,r ne zero)
  Root = r + Root
  Root = Merge(Root1, Root,r2ltq3)
  Root = Root - one3*a2
END
;
; Routine to find the roots of a fourth degree polynomial
; Based on the method outlined at 
;            http://mathworld.wolfram.com/QuarticEquation.html
; using the stable way to find roots in second and third degree
; polynomials outlined in Numerical Recipes chap. 5.6.
; The polynomial is supposed to be written in a "Normalised" fashion
;                 x^4 + a3 x^3 + ... + a0, Im(ai) = 0
; The coeeficients are coming from a slice in the stagger code,
; so they are all 2-dim arrays.
; The roots are returned in the Roots array, like
;           Roots = (/ root1, ... /)
;-----------------------------------------------------------------------
; Cost: Total count (Log etc x 3): 115
; Intrinisics    : 14
; Add/Multiply   : 77
; Log,Sqrt,Divide: 8 
; Total Cost inkl sub routine calls : 339 (33 Log, power, sqrt, divides)
;-----------------------------------------------------------------------
PRO P4_Roots, ReRoots, ImRoots, a0, a1, a2, a3
  zero = 0.
  two=2.
  mone= -1.
  half=0.5
  four=4.0
  one=1.0
  ; First find a real root in a third degree poly
  z0 = four*a2*a0 - a1^2 - a3^2*a0
  z1 = a1*a3 - four*a0
  z2 = -a2
  P3_Root, y, z0, z1, z2
  ; Then find roots in the two resulting 2nd degree polynomials
  ; x^2 + z11 x + z10 = 0
  ; x^2 + z21 x + z20 = 0
  ; Where z[ij] are themselves roots in two 2nd degree polynomials:
  ; z[1,2]0 = 1/2*(y  -+ Sqrt(y^2 - 4a0) )
  ; z[1,2]1 = 1/2*(a3 +- Sqrt(a3^2 - 4(a2-y)) )
  ; Given as
  ; x^2 - a3 x + (a2 - y) = 0
  ; x^2 - y x + a0 = 0
  Sq0R = a3*a3 - four*(a2 - y)
  z0 = Sqrt(Abs(Sq0R))
  logmerge = Sq0R ge zero
  Sq0I = merge(zero, z0, logmerge)
  Sq0R = merge(z0, zero, logmerge)
  sgn = merge(one, mone, a3 ge zero)
  gthan = half*(sgn + 1.) 
  lthan = one - gthan
  z1 = a3 + sgn*Sq0R
  XR = half*z1
  XI = half*sgn*Sq0I
; Complex division
; Done with guard for overflow
  ReRoot1 = abs(z1)
  ReRoot2 = abs(Sq0I)
  z0 = (ReRoot1 > ReRoot2)
  YR = two*((a2 - y)/z0)/((z1/z0)*z1 + (Sq0I/z0)*Sq0I)
; Done the fast way
;  YR = 2.0*((a2 - y))/(z1^2 + Sq0I^2)
; Complex division finished
  YI = -sgn*Sq0I*YR
  YR = z1*YR
  Z11R = gthan*XR + lthan*YR
  Z21R = gthan*YR + lthan*XR
  Z11I = gthan*XI + lthan*YI
  Z21I = gthan*YI + lthan*XI
  Sq1R = y*y-four*a0
  z0 = Sqrt(Abs(Sq1R))
  logmerge = Sq1R ge zero
  Sq1I = merge(zero, z0, logmerge)
  Sq1R = merge(z0, zero, logmerge)
  sgn  = merge(one, mone, y ge zero)
  gthan = half*(sgn + one)
  lthan = one - gthan
  z1 = y + sgn*Sq1R
  XR = half*z1
  XI = half*sgn*Sq1I
; Complex division
; Done with guard for overflow
  ReRoot1 = abs(z1)
  ReRoot2 = abs(Sq1I)
  z0 = (ReRoot1 > ReRoot2)
  YR = two*(a0/z0)/((z1/z0)*z1 + (Sq1I/z0)*Sq1I)
; Done the fast way
;  YR = 2.0*(a0)/(z1^2 + Sq1I^2)
; Complex division finished
  YI = -sgn*Sq1I*YR
  YR = z1*YR
  Z20R = gthan*XR + lthan*YR
  Z10R = gthan*YR + lthan*XR
  Z20I = gthan*XI + lthan*YI
  Z10I = gthan*YI + lthan*XI
  P2_Roots, ReRoot1,ImRoot1,ReRoot2,ImRoot2,z10R,z10I,z11R,z11I
  ReRoots(*,*,0) = ReRoot1
  ImRoots(*,*,0) = ImRoot1
  ReRoots(*,*,1) = ReRoot2
  ImRoots(*,*,1) = ImRoot2
  P2_Roots, ReRoot1,ImRoot1,ReRoot2,ImRoot2,z20R,z20I,z21R,z21I
  ReRoots(*,*,2) = ReRoot1
  ImRoots(*,*,2) = ImRoot1
  ReRoots(*,*,3) = ReRoot2
  ImRoots(*,*,3) = ImRoot2
END

PRO rhd_nr2, X, Y2, hm1, A, iter
  epsi = 1.e-8
  MaxIt = 100

  for iter=1, MaxIt do begin
    h = hm1 + 1.
    hW = Sqrt(h^2 + Y2)
    W = hW/h
    mW = W - A/W
    GX = Y2/(h^2*(W+1.)) + hm1*mW
    DGXDhm1 = mW - Y2*(A*hm1 + hW*W)/hW^3
    corr = (GX - X)/DGXDhm1
    hm1 = hm1 - corr
    eps  = max(abs(corr/(hm1+epsi)))
    If (eps lt 1e-4) then return
  endfor
END
;
; Input is: [We suppose that all variables are
;            either given from special relativity
;            or are evaluated in the pseudo FIDO frame
;            where ds^2 = -dt^2 + gamma_ij dx^i dx^j,
;            and have already been corrected for 
;            metric prefactors]
;  - T00p: Relativistic mass density T00 - D
;          T00 - D = -T^0_0 - D
;                  = rho h (U^t)^2 - P - rho U^t
;  - T0i: Momentum density
;         T^0_i = rho*h*U^t*U_i
; Output is:
;  - vW : Gamma*velocity = gamma^2 - 1
;         (Important trick: W - 1 = vW/(W + 1))
;  - hm1: Relativistic enthalpy corrected for rest mass
;         hm1 = h - 1 = e_int + P/rho
;            (= Gas_Gamma*e_int, for ideal equation of state)
;
PRO rhdsolve, D,T00p,T0x,T0y,T0z,vW,hm1,gam
  ;common performance, kz, numbertimes
  ;If N_Elements(numbertimes) eq 0 Then begin 
  ;   kz=0.0d0
  ;   numbertimes = 0
  ;EndIf
  ;time1 = systime(1)
  sz = size(D)
  If sz(0) eq 2 then mz = 1 else  mz = sz(3)
  my = sz(2)
  mx = sz(1)
  mone = -1.
  A = (gam -1.0)/gam
  gamma2 = gam^2
  twogamma = gam + gam
; a3 is const, so we take it out of the loop
  a3 = Make_Array(mx,my,Value=2.0*(1.0 + gam),/float)
; Fixme has to be g^ij*T0i*T0j
;  SPD  = spatial_dot_lo(T0x,T0y,T0z)
  SPD = T0x^2 + T0y^2 + T0z^2
  ReRoots = fltarr(mx,my,4,/NoZero)
  ImRoots = fltarr(mx,my,4,/NoZero)
  hm1xy   = fltarr(mx,my,/NoZero)  
  for iz =0,mz-1 do begin
    XX2 = 1./D(*,*,iz)
    X = T00p(*,*,iz)*XX2
    Y2  = SPD(*,*,iz)*XX2^2
    xx2 = X*(X + 2.0)
    a0 = Gamma2*(1.0 + y2)*(Y2 - xx2) 
    xx2 = 1.0 - gam*xx2
    a1 = TwoGamma*(xx2 + (1. + gam)*y2)
    a2 = (1.0 + gam*(3.0 + xx2 + 2.*y2))
    ; Find roots
    P4_Roots, ReRoots, ImRoots, a0, a1, a2, a3
    ; Set the real part of all roots with Im(Root)/Re(Root) gt 1e-3 to -1
    ReRoots = Merge(ReRoots, mone, Abs(ImRoots/ReRoots) lt 1.e-3)
    ; Select root (should be refined, but works ;-)
    hm1xy = Max(ReRoots,dim=3) > 0.00001
    ; Polish the Roots with a newton raphson step
    rhd_nr2, X, Y2, hm1xy, A, iter
    vW(*,*,iz) = Sqrt(Y2/(1.0 + hm1xy)^2)
    hm1(*,*,iz) = hm1xy
  endfor
  ;time2 = systime(1)
  ;kz = 0.001*(mx*my*mz)/(time2-time1)+kz
  ;numbertimes = numbertimes + 1
  ;Print, "Averqage performance until now:", kz/numbertimes
END
