PRO harvest_merge

cd,'.',current=wd
home='~/scratch/mconv/'
host='ALL'
print,'host: '+host
iy=42

; Find all idlsave files
cd,home
f=file_search('mhd*','*.idlsave',count=n)
print,n,' total files'

; Unique list of dirs
for i=0,n-1 do f[i]=strmid(f[i],0,strpos(f[i],'/',/reverse_search)+1)
dir=f(uniq(f))
ndir=n_elements(dir)

; Loop over directories
for i=0,ndir-1 do begin
  spawn,'mkdir -p '+home+dir[i]
  cd,dir[i],current=cwd
  idls=file_search('*.idlsave',count=nidls)
  print,dir[i]+' with '+str(nidls)+' files'
  savefile=home+dir[i]+host+'.idlsave'
  print,savefile,exists(savefile)
  clean=0
  if (exists(savefile)) then begin
    for j=0,nidls-1 do begin
      if newer(idls[j],savefile) then begin
        print,savefile+' is older than '+idls[j]+', replacing'
	clean=1
      end
    end
  end
  if (clean) then file_delete,savefile
  if (not exists(savefile)) then begin
    idls=file_search('*.idlsave',count=nidls)
    restore,idls[0]
    print,'restored '+idls[0]+' with '+str(n_elements(s.file))+' snapshots'
    ic=s.ic
    by=s.by
    file=s.file
    for j=1,nidls-1 do begin
      restore,idls[j]
      print,'restored '+idls[j]+' with '+str(n_elements(s.file))+' snapshots'
      ic=[[[ic]],[[s.ic]]]
      by=[[[by]],[[s.by]]]
      file=[file,s.file]
    end
    nf=n_elements(file)
    for k=0,n_elements(file)-1 do file[k]=strmid(file[k],strpos(file[k],'snap'))
    q=sort(file)
    file=file[q]
    ic=ic[*,*,q]
    by=by[*,*,q]
    u=uniq(file)
    nu=n_elements(u)
    print,str(nu)+' unique file names out of '+str(nf)
    file=file[u]
    ic=ic[*,*,u]
    by=by[*,*,u]

    ; Put results in structure and save
    s={host:host,dir:dir[i],iy:s.iy,file:file,ic:ic,by:by}
    print,savefile
    save,s,file=savefile
  end
  cd,cwd
end

cd,wd
END
