PRO gen_ctbl

  ma=75L          ; size of grid 
  mb=75L
  mc=75L
  
  rgrid=dblarr(ma,mb,mc)
  egrid=dblarr(ma,mb,mc)
  dgrid=dblarr(ma,mb,mc)
  lamb=dblarr(ma,mb,mc)   ; store lambda in double precision to avoid underflow
 
  r0=1.0000E-05   ; lowest density [cdu]
  e0=2.1741E+00   ; lowest specific energy [ceu cmu-3]
  d0=1.3228E-12   ; lowest iron-to-matter ratio. 
                  ; Note: in cooling2.pro d0 set to 1.324E-13 and
                  ; in ISM-cooling.f90 d0 is set to 1.324e-12.  
  
  r0=alog(r0)
  e0=alog(e0)
  d0=alog(d0)

  dlnr=0.25       ; step size
  dlne=0.20       ;   - " -     Note: try to avoid dlne > 0.25
  dlnd=0.32       ;   - " -

  print, 'Range in rho:', exp(r0), ' - ', exp(r0+float(ma-1)*dlnr)
  print, 'Range in T:', 46.*exp(e0), ' - ', 46.*exp(e0+float(mb-1)*dlne)
  print, 'Range in Z:', alog10(exp(d0+6.628)), ' - ', alog10(exp(d0+float(mc-1)*dlnd+6.628))

  power=5L              ; table lookup values to be raised to this power
  power1=1./power       ; computed table values raised to this power
  tab0=0.0              ; initialize tab0

  for i=0,ma-1 do begin
    for j=0,mb-1 do begin
      for k=0,mc-1 do begin
        rgrid(i,j,k)=exp(r0+i*dlnr)
        egrid(i,j,k)=exp(e0+j*dlne)
        dgrid(i,j,k)=exp(d0+k*dlnd)
      endfor
    endfor
  endfor

  cooling2, rgrid, egrid, dgrid, lamb   ; calculate the cooling (in code units)

  lamb=lamb/rgrid^2        ; take away the main density dependence

  if (power le 1) then begin
    power=0L
    tabmin=min(lamb)
    tabmax=max(lamb)
    if (tabmin le 0.) then begin
      tab0=tabmin-0.001*abs(tabmin)
      if (tab0 eq 0.) then tab0=-1.e-5*tabmax
    endif
    lamb=alog(lamb-tab0)   ; guarantees that ln(lamb) is well defined if lamb < 0.
    lamb=lamb+47.1602      ; = lamb-ln[10^(logclambu)] -> convert to cgs units
  end else begin
    w=where(lamb lt 0,nw)
    a=exp(47.1602)
    lamb=(abs(lamb)*a)^power1
    if nw gt 0 then lamb(w)=-lamb(w)
  end

  dlnr=1./dlnr             ; invert step size
  dlne=1./dlne             ;     - " -
  dlnd=1./dlnd             ;     - " -

  openw, 1, '../src/COOLING/lambda.dat',/F77_UNFORMATTED
  writeu, 1, ma, mb, mc, r0, e0, d0, dlnr, dlne, dlnd, tab0, power
  writeu, 1, float(lamb)
  ;info=fstat(1)
  ;print, info
  close, 1

END
