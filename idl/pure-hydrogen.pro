
;-----------------------------------------------------------------------
FUNCTION y2z, y
    return,alog((y^2+1e-38)/((1.-y)+1e-8))
END

;-----------------------------------------------------------------------
FUNCTION z2y, zl
    z=exp(zl<40.)
    return, 2.*z/(z+sqrt(z^2+4.*z+1e-35))
END

;-----------------------------------------------------------------------
FUNCTION T2E, TT, rho=rho
  common ceos, ee1, rho1, eec, rhoc, T0
  if keyword_set(rho) then rhoc=rho*rho1
  T = double(TT/T0)
  z = -alog(rhoc)-1./T+1.5*alog(T)
  y = z2y(z)
  E = 1.5*T*(1.+y) + y
  return,E
END

;-----------------------------------------------------------------------
FUNCTION T2y, TT, rho=rho
  common ceos, ee1, rho1, eec, rhoc, T0
  T = double(TT)/T0
  if keyword_set(rho) then rhoc=rho*rho1
  z = -alog(rhoc)-1./T+1.5*alog(T)
  y = z2y(z)
  return,y
END

;-----------------------------------------------------------------------
FUNCTION fsahaz, z, ee=ee, rho=rho
  common ceos, ee1, rho1, eec, rhoc, tt0
  if keyword_set(rho) then rhoc=rho*rho1
  if keyword_set(ee ) then eec =ee *ee1
  y=z2y(z)
  sz=size(z)
  if sz[0] eq 0 then begin
    if y ge eec then y=eec-1e6
  end else begin
    w=where(y ge eec,nw)
    if nw gt 0 then y(w)=eec(w)-1e-6
  end
  T1=1.5*(1.+y)/(eec-y)
  return, -1.5*alog(T1)-alog(rhoc)-T1-z
END

;-----------------------------------------------------------------------
FUNCTION fsahay, yy, ee=ee, rho=rho, T1=T1
  common ceos, ee1, rho1, eec, rhoc, tt0
  if keyword_set(rho) then rhoc=rho*rho1
  if keyword_set(ee ) then eec =ee *ee1
  y=yy
  sz=size(y)
  if sz[0] eq 0 then begin
    if y ge eec then y=eec-1e6
  end else begin
    w=where(y ge eec,nw)
    if nw gt 0 then if n_elements(eec) eq 1 then y(w)=eec-1e-6 else y(w)=eec(w)-1e-6
  end
  T1=1.5*(1.+y)/(eec-y)
  return, -1.5*alog(T1)-alog(rhoc)-T1+alog(1.-y)-2.*alog(y)
END

;-----------------------------------------------------------------------
FUNCTION fsahat, T, ee=ee, rho=rho, y=y
  common ceos, ee1, rho1, eec, rhoc, tt0
  if keyword_set(rho) then rhoc=rho*rho1
  if keyword_set(ee ) then eec =ee *ee1
  T1=TT0/T
;  T1=1.5*(1.+y)/(eec-y)
;  T1/1.5*(eec-y)=1.+y
;  T1/1.5*eec-1. = y*(T1/1.5+1)
  y=(T1/1.5*eec-1.)/(T1/1.5+1.)
  y=y>1e-20
  return, -1.5*alog(T1)-alog(rhoc)-T1+alog(1.-y)-2.*alog(y)
END

;-----------------------------------------------------------------------
FUNCTION fsahaE, T, ee=ee, rho=rho
  common ceos, ee1, rho1, eec, rhoc, tt0
  if keyword_set(rho) then rhoc=rho*rho1
  if keyword_set(ee ) then eec =ee *ee1
  f= T2E(T)-eec
;  print, T, f
  return, f
END

;-----------------------------------------------------------------------
PRO init_eos
  common ceos, ee1, rho1, eec, rhoc, tt0
  kB=1.38e-16
  mproton=1.6726e-24
  melectron=9.1094e-28
  echarge=1.602e-12
  hplanck=6.6261e-27
  chi=13.6
  ee0=chi*echarge/mproton
  tt0=chi*echarge/kB
  rho0=mproton*(2.*!pi*melectron*chi*(echarge/hplanck)/hplanck)^1.5
  cdu=1e-24
  clu=3.0857d18
  ctu=3.1557d13
  cvu=clu/ctu
  ceu=cvu^2
  rho1=cdu/rho0
  ee1=ceu/ee0
  help,rho0,rho1,ee1,tt0
END

;-----------------------------------------------------------------------
PRO eos2, rho, ee, tt, _extra=ex
  common ceos, ee1, rho1, eec, rhoc, T0

  Tmin=2000.
  Tmax=5000.
  Emin=1.*Tmax/T0
  Emax=1.058
  Emax=1.065
  for i=0L,n_elements(rho)-1 do begin
    eec = ee[i]
    rhoc = rho[i]
    if (eec lt emin) then begin
      T1 = eec/1.5*0.8
      T2 = eec/1.5*1.1
      T3 = 1./(-alog(rhoc)-y2z(1e-6 > (eec-1.5*T1) < 0.9999)+1.5*alog(T1))
      T2 = T3 < T2
    end else if (eec gt emax) then begin
      T1 = (eec-1)/3.*0.99
      T2 = (eec-1)/3.*1.4
    end else begin
      T2 = 1./(-alog(rhoc)-y2z(1e-6 > (eec-1.5*Tmin/T0) < 0.9999)+1.5*alog(Tmin/T0))
      T1 = 1./(-alog(rhoc)-y2z(1e-6 > (eec-3.0*T2     ) < 0.9999)+1.5*alog(T2     ))
    end
    T2 = T0/(-alog(rhoc)-y2z(1e-6 > (eec-1.5*Tmin/T0) < 0.9999)+1.5*alog(Tmin/T0))
    T1 = T0/(-alog(rhoc)-y2z(1e-6 > (eec-3.0*T2     ) < 0.9999)+1.5*alog(T2     ))
    TT[i] = zbrent1('fsahaE',T1,T2,1e-4,it)
  end
END

;-----------------------------------------------------------------------
PRO eos1, rho, ee, tt=tt, _extra=ex
  common ceos, ee1, rho1, eec, rhoc, T0
  if keyword_set(ee ) then eec=ee*ee1
  if keyword_set(rho) then rhoc=rho*rho1
;
;  In the limit of no ionization E=1.5*T/T0, so E > 1.5*T/T0, and T/T0 < E/1.5
;  In the limit of full ionization E=1.5*(T/T0)*2.+1., so T/T0 > (E-1.)/3.
;  Can one find a better limit?  From eec we have an upper limit to y, equal
;  to min(eec,1.).  The lower limit to y is also a lower limit to z.  So in the
;  expression  -1.5*alog(T1)-alog(rhoc)-T1-z we can estimate
;
;    T1 = -alog(rhoc)-z-1.5*alog(T1) < -alog(rhoc)-y2z(eec)
;
;  So, we have
;
;    T > T0/(-alog(rho1)-y2z(eec))
;
  rhoc = rho*rho1
  eec  = ee ;*ee1
;
; Thalf and Ehalf are estimates of the temperature and energy at y=1/2.
;
  Thalf = 1./(-alog(rhoc)-y2z(0.5))
;  Thalf = 1./(-alog(rhoc)-y2z(0.5)+1.5*alog(Thalf))
  Ehalf = 0.5 + 2.25*Thalf
;
;  E1 and E2 are estimates of the "corner" energies, at temperatures just
;  below and just above Thalf.
;
  E1 = 1.5*Thalf
  E2 = 1.+3.*Thalf
;
;  For energies less than E1, estimate the temperature by assuming no ionization
;

  Tmin=2000.
  Tmax=5000.
  Emin=1.*Tmax/T0
  Emax=1.058
  Emax=1.065

  if (eec lt emin) then begin
    T1 = eec/1.5*0.8
    T2 = eec/1.5*1.1
    T3 = 1./(-alog(rhoc)-y2z(1e-6 > (eec-1.5*T1) < 0.9999)+1.5*alog(T1))
    T2 = T3 < T2
    y2max = T2^1.5*exp(-1./T2)/rhoc/emin
    print,'lower'
    print,T1*T0,T2*T0,T3*T0,y2max
  end else if (eec gt emax) then begin
    T1 = (eec-1)/3.*0.99
    T2 = (eec-1)/3.*1.4
    y1max = rhoc*(T1)^(-1.5)*exp(1./T1)
    ymin = 1.-y1max
;    T2 = 1./(-alog(rho1)-y2z(ymax)+1.5*alog(Tmin))
    print,'upper'
    print,T1*T0,T2*T0,fsahaE(T1*T0),fsahaE(T2*T0),y1max
  end else begin
    T2 = 1./(-alog(rhoc)-y2z(1e-6 > (eec-1.5*Tmin/T0) < 0.9999)+1.5*alog(Tmin/T0))
    T1 = 1./(-alog(rhoc)-y2z(1e-6 > (eec-3.0*T2     ) < 0.9999)+1.5*alog(T2     ))
    print,'middle'
    print,T1*T0,T2*T0,fsahaE(T1*T0),fsahaE(T2*T0)
  end

  T1 = double(T1)
  T2 = double(T2)
  T1 = T1*T0
  T2 = T2*T0
  Thalf = Thalf*T0

  ws,0
  temp = 300d0+50.*findgen(2000)
  plot,temp,T2E(temp),/xlog,/ylog

  T = [T1,0.5*(T1+T2),T2]
  T = [T1,T2]
  plots,psym=1,T,T2E(T)
  oplot,[1e2,1e6],[1,1]*eec,line=2
  oplot,[1e2,1e6],[1,1]*Emin,line=1
  oplot,[1e2,1e6],[1,1]*Emax,line=1
  ;T=fx_root(T,'fsahaE')
  TT = zbrent1('fsahaE',T1,T2,1e-4,it) & print,TT,it
  ;print,TT
  ;temp=T1+(T2-T1)*findgen(101)/100.
  ;plot,temp,fsahat(temp),_extra=ex & zeroline
  plots,psym=6,TT,T2E(TT)

  ws,2
  plot,temp,T2E(temp) ,xr=[temp[0],TT > 1e4], yst=3
  plots,psym=1,T,T2E(T)
  oplot,[1e2,1e6],[1,1]*eec,line=2
  oplot,[1e2,1e6],[1,1]*Emin,line=1
  oplot,[1e2,1e6],[1,1]*Emax,line=1
  plots,psym=6,TT,T2E(TT)

END

;-----------------------------------------------------------------------
PRO eos, rho, ee, tt=tt
  common ceos, ee1, rho1, eec, rhoc, T0
  ee2  = ee*ee1
  rho2 = rho*rho1

  tt = make_array(size=size(ee))
  elo = 0.02
  ehi = 1.07
  w = where (ee2 le elo, nw)
  if nw gt 0 then begin
    print,nw,' unionized'
    tt(w) = T0*ee2(w)*(2./3.)
  end
  w = where (ee2 ge ehi, nw)
  if nw gt 0 then begin
    print,nw,' fully ionized'
    tt(w) = T0*(ee2(w)-1.)*(1./3.)
  end
  w = where (ee2 gt elo and ee2 lt ehi, nw)
  if nw gt 0 then begin
    print,nw,' partially ionized'
    ttw = fltarr(nw)
    eos2, rho2(w), ee2(w), ttw
    tt(w) = ttw
  end

END

  cd,'../src/EXPERIMENTS'
  open,a,'torgny-Rosen-etal'
  t=1
  rho=a[6*t]
  ee=a[6*t+4]/rho
  tt=a[6*t+5]

;  rhox=rho[*,40,11]
;  eex =ee [*,40,11]
;  ttx =tt [*,40,11]

  eos,rho,ee,tt=ttf
  yx=t2y(ttf,rho=rho)

  ee2=(ttf/tt0*1.5*(1+yx)+yx)/ee1

  ws,0
  !p.multi=[0,1,2]
  !y.style=3
  !x.style=3

  s=sort(ttf)
  xr=[1e3,1e7]
  plot,xr=xr,ttf(s),cumulative(rho(s))/total(rho),/xlog,title='MASS FRACTION'
  nw=n_elements(ttf)
  plot,xr=xr,ttf(s),findgen(nw)/nw,/xlog,title='VOLUME FRACTION'

END
