PRO harvest_mpeg, dirs, wsize=wsize, zr=zr, steps=steps, smooth=smooth, replicate=replicate, $
                  quality=quality, bitrate=bitrate, signexp=sexp, iframe_gap=iframe_gap, $
		  nompg=nompg, jpg=jpg, keep=keep, every=every, purge=purge, _extra=_extra

default,wsize,504
default,dirs,'mhd*'
default,quality,100
default,bitrate,2d7
; default,iframe_gap,10
default,steps,29
default,smooth,5
default,replicate,1
default,sexp,[1.0,1.0]
default,every,1

; Find all idlsave files
idls=file_search(dirs,'*ALL.idlsave',count=n)
print,n,' total files'

for i=0,n-1 do begin
  dir=strmid(idls[i],0,strpos(idls[i],'/',/reverse_search)+1)
  print,i,'  '+dir
  file=dir+'ic.mpg'
  if (not keyword_set(purge)) and exists(file) then if newer(file,idls[i]) then continue

  print,i,'  '+dir+', using '+idls[i]
  restore,idls[i]
  nf=n_elements(s.file)
  print,nf,' frames'
  idx=strpos(s.file[0],'/')+1

  sz=size(s.by)
  mx=sz[1]
  wsz=(wsize > (mx/2))
  if mx ge 1008 then wsz=1008
  window,0,xsize=wsz,ysize=wsz,retain=2
  wshow,0,/iconic
  imsz,wsz

  file=dir+'by-color.mpg'
  if exists(file) then file_delete,file
  print,' ',file,exists(file)
  color,steps=steps,smooth=smooth
  ; loadct,39
  mpg=mpg_open([wsz,wsz],file=file,quality=quality,bitrate=bitrate,iframe_gap=iframe_gap,_extra=_extra)
  zrby=max(abs(s.by))
  zrby=[-zrby,zrby]
  byrms=rms(s.by)
  if byrms gt 0.3 then signexp=sexp[0] else signexp=sexp[1]
  print,'By-range, rms, signexp:',zrby[1],byrms,signexp
  fr=0
  for j=0,nf-1,every do begin
    t0=systime(1)
    image,s.by[*,*,j],zr=zrby,signexp=signexp,tit=s.dir+strmid(s.file[j],idx),/bold
    for k=0,replicate-1 do begin
      mpg_put,mpg,window=0,frame=fr,/order,jpg=jpg,_extra=_extra
      fr=fr+1
    end
    ; print,s.file[j],t1-t0,t2-t1
    print,file,'  '+str(j+1)+'/'+str(nf)
  end
  t1=systime(1)
  if not keyword_set(nompg) then mpg_save,mpg,file=file,_extra=_extra
  if not keyword_set(keep) then mpg_close,jpg=jpg,mpg
  t2=systime(1)
  print,'conversion time:',t1-t0,t2-t1

  loadct,0
  file=dir+'by-gray.mpg'
  if exists(file) then file_delete,file
  print,' ',file,exists(file)
  mpg=mpg_open([wsz,wsz],file=file,quality=quality,bitrate=bitrate,iframe_gap=iframe_gap,_extra=_extra)
  zrby=[min(s.by,max=max),max]
  fr=0
  for j=0,nf-1,every do begin
    image,s.by[*,*,j],zr=zrby,signexp=signexp,tit=s.dir+strmid(s.file[j],idx),/bold
    for k=0,replicate-1 do begin
      mpg_put,mpg,window=0,frame=fr,/order,jpg=jpg,_extra=_extra
      fr=fr+1
    end
    print,file,'  '+str(j+1)+'/'+str(nf)
  end
  if not keyword_set(nompg) then mpg_save,mpg,file=file,_extra=_extra
  if not keyword_set(keep) then mpg_close,jpg=jpg,mpg

  file=dir+'ic.mpg'
  if exists(file) then file_delete,file
  print,' ',file,exists(file)
  mpg=mpg_open([wsz,wsz],file=file,quality=quality,bitrate=bitrate,iframe_gap=iframe_gap,_extra=_extra)
  zr0=[min(s.ic,max=max),max]
  print,zr0
  if n_elements(zr) ne 2 then zr=[0.05,0.30]
  fr=0
  for j=0,nf-1,every do begin
    image,s.ic[*,*,j],zr=zr,tit=s.dir+strmid(s.file[j],idx),/bold
    for k=0,replicate-1 do begin
      mpg_put,mpg,window=0,frame=fr,/order,jpg=jpg,_extra=_extra
      fr=fr+1
    end
    print,file,'  '+str(j+1)+'/'+str(nf)
  end
  if not keyword_set(nompg) then mpg_save,mpg,file=file,_extra=_extra
  if not keyword_set(keep) then mpg_close,jpg=jpg,mpg

end

END
