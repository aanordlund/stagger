
@stagger_6th

PRO pressures,file,_extra=extra

  open,d,fileroot(file)+'.tim',dim=3,_extra=extra
  open,a,file,nt=nt,nv=nv,_extra=extra
  sz=size(a)
  comp=0.

  mx=sz[1]

  savefile=fileroot(file)+'-press.idlsave'
  if exists(savefile) then begin
    restore,savefile
    if nt-1 le t then return
    nt1=t+1
    pdyn1=pdyn
    pmag1=pmag
    time1=time
    t1=t
    pdyn=fltarr(nt)
    pmag=fltarr(nt)
    time=fltarr(nt)
    pdyn[0:t1]=pdyn1
    pmag[0:t1]=pmag1
    time[0:t1]=time1
    print,'filling in from '+str(t1)+' to '+str(nt-1)
  end else begin
    time=fltarr(nt)
    pdyn=fltarr(nt)
    pmag=fltarr(nt)
    t1=0
  end

  for t=t1+1,nt-1 do begin
    r=a(nv*t)
    lnr=alog(r)
    for i=0,2 do begin
      case i of
      0: ui=a(1+i+nv*t)/exp(xdn(lnr))
      1: ui=a(1+i+nv*t)/exp(ydn(lnr))
      2: ui=a(1+i+nv*t)/exp(zdn(lnr))
      end
      pdyn[t]=pdyn[t]+aver(r*ui^2)
    end
    for i=0,2 do begin
      ui=a(nv-3+i+nv*t)
      pmag[t]=pmag[t]+aver(ui^2)
    end
    pmag[t]=pmag[t]*0.5
    time[t]=d[0,t]
    print,t,time[t],pdyn[t],pmag[t],pmag[t]/(pdyn[t]+1e-30)
    save,file,t,time,pdyn,pmag,file=savefile
  end 

END

