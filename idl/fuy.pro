; @stagger_6th
FUNCTION fuy,aa,t,nvar=nv,rho=rho,first=first,verbose=verbose
@common_cdata
  if n_elements(nv) gt 0 then nvar=nv
  default,nvar,6
  default,t,0
  sz=size(aa)
  t0=systime(1)
  ;if sz[1] ne 7 then open,a,snapshot(aa),/quiet,lun=lun else a=aa
  a=aa
  sz=size(a)
  uy=a[2+nvar*t]
  t1=systime(1) & if keyword_set(verbose) then print,'read py',t1-t0
  if n_elements(rho) eq n_elements(uy) then scr=alog(rho) else scr=alog(a[t*nvar])
  t2=systime(1) & if keyword_set(verbose) then print,'computed ln(rho)',t2-t1
  if sz[3] lt 512 then begin
    if keyword_set(first) then scr=ydn1(temporary(scr)) else scr=ydn(temporary(scr))
  end else begin
    for iz=0,sz[3]-1 do begin
      if keyword_set(first) then scr[*,*,iz]=ydn1(scr[*,*,iz]) else scr[*,*,iz]=ydn(scr[*,*,iz])
    end
  end
  t3=systime(1) & if keyword_set(verbose) then print,'shifted ln(rho)',t3-t2
  scr=exp(temporary(scr))
  uy=temporary(uy)/scr
  t4=systime(1) & if keyword_set(verbose) then print,'divided w exp(lnr)',t4-t3
  if n_elements(lun) gt 0 then free_lun,lun
  return,uy
END
