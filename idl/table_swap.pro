  iupdte=0L
  nvar=0L
  mrho=0L
  mtable=0L
  njon=0L
  eosxmin=0.
  dbox=0.
  ur=0.
  ul=0.
  ut=0.
  eps=0.
  tff=0.
  grv=0.
  abnd=0.

  close,12
  openr,12,'hd.tab',/f77,/swap_if_big
  readu,12,mrho,iupdte,nvar,mbox,eosxmin,dbox,ul,ut,ur,eps,tff,grv,abnd
  njon = nvar-mbox

  tmean=fltarr(mrho)
  tamp=fltarr(mrho)
  rhm=fltarr(mrho)
  xcorr=fltarr(mrho)
  thmin=fltarr(mrho)
  thmax=fltarr(mrho)
  dth=fltarr(mrho)
  eemin=fltarr(mrho)
  eemax=fltarr(mrho)
  deetab=fltarr(mrho)
  ttab=fltarr(mrho)
  itab=lonarr(mrho)
  mtab=lonarr(mrho)
  readu,12,tmean,tamp,rhm,xcorr,thmin,thmax,dth,eemin,eemax,deetab,itab,mtab

  readu,12,mtable
  tab=fltarr(mtable)
  print,'table size:',mtable
  readu,12,tab
  close,12

  openw,12,'hd-be.tab',/f77,/swap_if_little
  writeu,12,mrho,iupdte,nvar,mbox,eosxmin,dbox,ul,ut,ur,eps,tff,grv,abnd
  writeu,12,tmean,tamp,rhm,xcorr,thmin,thmax,dth,eemin,eemax,deetab,itab,mtab
  writeu,12,mtable
  writeu,12,tab
  close,12
END
