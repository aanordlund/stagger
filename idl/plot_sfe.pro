pwd,cwd=cwd

i1=strpos(cwd,'rho')
label=strmid(cwd,i1)

i1=strpos(label,'rho')+3
i2=strpos(label,'_')
rho=float(strmid(label,i1,i2-i1))
print,rho
tff=sqrt(3.*!pi/(32.*rho))

title=label+'  SFE'
print,title

if !d.name eq 'PS' then setdev,'ps',file='sfe.ps'

s=rsink(/swap_if_b,u,t=t,/reopen)
time=[t]
mass=[total(s.m)]
to=t
while not eof(u) do begin
  s=rsink(/swap_if_b,u,t=t)
  if t gt to then begin
    time=[time,t]
    mass=[mass,total(s.m)]
    ;print,t,to,total(s.m)
    to=t
  end
end
time[0]=2.*time[1]-time[2]

t0 = time[0]+0.4*tff
w=where(mass gt 0.05,nw)
if nw gt 0 then t0 = t0 < time[w[0]]
w=where(time gt t0 and mass lt 0.4 ,nw)
if nw gt 0 then begin
  time=time(w)
  mass=mass(w)
end

time=(time-time[0])/tff
plot,time,100.*mass,title=title,xtitle='time/tff',ytitle='% mass'

; w=where(mass gt 0.05,nw)
if nw gt 0 then begin
  i=w[0] & fit=poly_fit(time,mass,1,yfit=yfit)
  if !d.name eq 'PS' then thick=6 else thick=2
  oplot,time,yfit*100,thick=thick
  print,'SFR =',fit[1]
  label,'SFR = '+str(fit[1],format='(f6.3)'),x=0.65,y=0.25,charsize=1.5
  openw,u2,/get,'sfr.txt'
  printf,u2,max(time),max(mass),fit[1]
  free_lun,u2
end

if !d.name eq 'PS' then begin
  device,/close
  if !VERSION.OS_FAMILY eq 'Windows' then spawn,'ps2pdf sfe.ps',/log else  spawn,'ps2pdf sfe.ps'
end

END
