
pwd,cwd=cwd
root=fileroot(cwd,tail=label)

title=label+'  N-**'
print,title

if !d.name eq 'PS' then setdev,'ps',file='nstars.ps'

s=rsink(/swap_if_b,u,t=t,m=m,/reop)
time=[t]
num=[m]
while not eof(u) do begin
  s=rsink(/swap_if_b,u,t=t,m=m)
  time=[time,t]
  num=[num,m]
end
time[0]=0.

plot,time,num,title=title,xtitle='time',ytitle='number'

if !d.name eq 'PS' then device,/close

END
