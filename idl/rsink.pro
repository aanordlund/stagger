; $Id: rsink.pro,v 1.12 2012/11/12 12:35:47 aake Exp $
;-------------------------------------------------------------------------------
FUNCTION read_sink,u,stars,msink,t,lsink,sz,array=f,version=version
  msink=0L
  lsink=0L
  sz=0L
  t=0.
  version=0L
  if stars eq 2 then begin
    s={tag:0L,n:0L,t:0.0,seed:0L}
    readu,u,s
    msink=s.n
    t=s.t
    lsink=22
    if (s.n gt 0) then begin
      f=fltarr(s.n,22)
      readu,u,f
      ss={version:0,tag:s.tag,n:s.n,t:s.t,x:f[*,19],y:f[*,20],z:f[*,21],m:f[*,1],texpl:f[*,0],ux:f[*,7],uy:f[*,8],uz:f[*,9],tlife:f[*,13]}
    end else begin
      ss={tag:s.tag,n:s.n,t:s.t,m:0.0}
    end
  end else if stars eq 1 then begin
    readu,u,version,msink,t,sz,lsink
    lsink=lsink/sz/4
    if version eq 1L then begin
      f={x:0.,y:0.,z:0.,px:0.,py:0.,pz:0.,dpx:0.,dpy:0.,dpz:0.,m:0.,dm:0.,phi:0., $
         rho:0.,time:0.,ddt:fltarr(6),owner:0L}
      f=replicate(f,msink)
    end
    readu,u,f
    ux=f[*].px/f[*].m
    uy=f[*].py/f[*].m
    uz=f[*].pz/f[*].m
    uu=sqrt(ux^2+uy^2+uz^2)
    ss={version:version,t:t,n:msink,x:f[*].x,y:f[*].y,z:f[*].z,ux:ux,uy:uy,uz:uz,px:f[*].px,py:f[*].py,pz:f[*].pz, $
        m:f[*].m,u:uu,rho:f[*].rho,t:f[*].time,r:0.,mu:0.}
  end else begin
    readu,u,msink,lsink,t,sz
    f=fltarr(msink,lsink)
    readu,u,f
    ux=f[*,3]/f[*,6]
    uy=f[*,4]/f[*,6]
    uz=f[*,5]/f[*,6]
    ss={version:-1,t:t,n:msink,x:f[*,0],y:f[*,1],z:f[*,2],ux:ux,uy:uy,uz:uz,px:f[*,3],py:f[*,4],pz:f[*,5], $
        m:f[*,6],u:f[*,7],rho:f[*,8],t:f[*,9],r:f[*,10],mu:f[*,11],zz:f[*,12:*]}
  end
  return,ss
END

;-------------------------------------------------------------------------------
FUNCTION rsink,u,t=t,size=sz,msink=msink,lsink=lsink,eof=eof,rewind=rewind,patch=patch, $
    array=f,verbose=verbose,file=file,scan=scan,reopen=reopen,get=get,stars=stars, $
    version=version,_extra=_extra

  if exists('stars.dat') then default,file,'stars.dat'
  if exists('sink.dat') then default,file,'sink.dat'
  if exists('hms.dat') then default,file,'hms.dat'
  default, file, 'stars.dat'
  if file eq 'hms.dat' then default,stars,2
  if file eq 'stars.dat' then default,stars,1 else default,stars,0
  if keyword_set(scan) or keyword_set(verbose) then print,'file: '+file

  if keyword_set(reopen) then begin
    if n_elements(u) gt 0 then free_lun,u
    openr,u,/get,file,/f77,_extra=_extra
  end

  if n_elements(u) eq 0 then openr,u,/get,file,/f77,_extra=_extra
  if keyword_set(rewind) then rewind,u

  s=read_sink(u,stars,msink,t,lsink,array=f,version=version)
  if keyword_set(verbose) then print,msink,lsink,t,total(s.m)

  if keyword_set(patch) then begin
    readu,u,msink,lsink,t,sz
    if keyword_set(verbose) then print,msink,lsink,t
  end

  if n_elements(get) gt 0 then begin
    while (not eof(u) and t lt get) do begin
      s=read_sink(u,stars,msink,t,lsink,array=f,version=version)
      if keyword_set(verbose) then begin
        print,msink,lsink,t,total(s.m)
      end
    end
  end

  if keyword_set(scan) then begin
    while not eof(u) do begin
      s=read_sink(u,stars,msink,t,lsink,array=f)
      if keyword_set(verbose) then begin
        print,msink,lsink,t,total(s.m)
      end
    end
  end

  if eof(u) then eof=1 else eof=0
  return,s
END
