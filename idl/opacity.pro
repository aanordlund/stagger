FUNCTION opacity, temp, yy, rho
;
;  Absorption coefficient [cm^2/g] for H-, scaled to a value 3e-26 cm^4/dyne
;  at T=6300 K extracted from Fig. 3 of Doughty & Fraser MNRAS 132, 267
;  (1966).  A mean atomic weight of 1.4 (10% helium by number) is assumed.
;  The electron density may alternatively be given in terms of electron
;  pressure.
;
  common copacity, kappa0
  me=9.109390d-28       ;[g]
  mH=1.673525d-24       ;[g]
  kB=1.380658d-16       ;[erg/K]
  hbar=1.054573d-27     ;[erg*s]
  eV=1.602177d-12       ;[erg]
  mu = 1.4

  N_e=yy*rho/mu/mH
  Xi_Hminus = 0.747
  temp0 = 6300.
  theta0 = eV/(kB*temp0)
  Pe0 = 1.
  N_e0 = Pe0/(kB*temp0)
  f_0 = 0.5*(2.*!pi*(hbar/me)*(hbar/kB))^1.5
  f_Hminus0 = f_0*exp(Xi_Hminus*theta0)/temp0^1.5*N_e0
  c0 = 3d-26
  kappa0 = c0/(mu*mH*exp(theta0*Xi_Hminus)*theta0^1.5*N_e0)
  theta = eV/(kB*temp)
  f_Hminus = 2.07d-16*exp(Xi_Hminus*theta)/temp^1.5*N_e
  kappa = (c0/f_Hminus0)*f_Hminus/(1.4*mH)
  return, kappa
END