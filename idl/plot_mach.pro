
default,rho,30.

pwd,cwd=head0
root1=fileroot(head0,tail=tail1,head=head1)
root2=fileroot(head1,tail=tail2,head=head2)
root3=fileroot(head2,tail=tail3,head=head3)

title=tail2+tail1+'  Mach'
print,title

if !d.name eq 'PS' then begin
  setdev,'ps',file='mach.ps'
  device,bits_per_pixel=1,yoffset=3,ysize=11
end
!p.multi=0

tmp=read_table('mach.txt')

plot,tmp(0,*),tmp(1,*),title=title,xtitle='time/t_ff',ytitle='Mach',yst=0

if !d.name eq 'PS' then device,/close

END
