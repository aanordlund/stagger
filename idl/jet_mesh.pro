; $Id: jet_mesh.pro,v 1.1 2007/07/09 07:07:25 aake Exp $
;-----------------------------------------------------------------------
@stagger_6th
FUNCTION ymeshf,y,pack,dpack
  d=8*(0.2/dpack)^2
  a=(1.+pack*dpack*tanh((1.+d*1.^3)/dpack))
  ym=y*a-pack*dpack*tanh((y+d*y^3)/dpack)
  return,ym
END
;-----------------------------------------------------------------------
PRO ymesh,n,sz,pack,dpack,ym,ymdn,y
  ym=sz*ymeshf(y,pack,dpack)			; stretched mesh
  ymdn=sz*ymeshf(y-.5/n,pack,dpack)		; shifted mesh
  d=deriv(ym)
  print,'compression:',max(d)/min(d)
END
;-----------------------------------------------------------------------
FUNCTION meshf,x,pack,dpack
  d=8*(0.2/dpack)^2
  a=(1.+2.*pack*dpack*tanh((0.5+d*0.5^3)/dpack))
  xm=x*a-pack*dpack*tanh((x+d*x^3)/dpack)
  return,xm
END
;-----------------------------------------------------------------------
PRO mesh,n,sz,pack,dpack,xm,xmdn,x
  xm=sz*meshf(x,pack,dpack)			; stretched mesh
  xm=[xm(0)+(xm(n-3:n-1)-xm(n)), $		; extend w 3 pts dn/up
  xm, xm(n)+(xm(  1:  3)-xm(0))]
  xmdn=sz*meshf(x-.5/n,pack,dpack)		; shifted mesh
  xmdn(0)=-(sz-xmdn(n))				; force exact periodicity
  xmdn=[xmdn(0)+(xmdn(n-3:n-1)-xmdn(n)), $	; extend w 3 pts dn/up
  xmdn, xmdn(n)+(xmdn(1  :3  )-xmdn(0))]
  d=deriv(xm)
  print,'compression:',max(d)/min(d)
END
;-----------------------------------------------------------------------
PRO mesh_init,pack,dpack,n
@common_cdata
@common_cmesh
  
; The grid sizes sx, sy and sz are to be interpreted as:
;	sx = xm(nx-6)-xm(5)
; 	sy = ymup(ny-1)-ymdn(0) equiv. to ymup(ny-1)=-ymdn(0)
; 	sz = zmup(nz-1)-zmdn(0) equiv. to zmup(nz-1)=-zmdn(0)

  x=(findgen(nx+1)-(nx+1)/2.)/nx
  y=(findgen(ny)-3.)/(ny-10)
  z=(findgen(nz+1)-(nz+1)/2.)/nz
  ymesh,(ny-10),(ny-10)*dy,pack(0),dpack(0),ym,ymdn,y
  mesh , nx   , nx   *dx,pack(1),dpack(1),xm,xmdn,x
  mesh , nz   , nz   *dz,pack(2),dpack(2),zm,zmdn,z

; Important to re-initialize dy! dy is used by the ddy[up,dn] routines to
; calculate the Jacobian (partly). In stagger.f a factor of d[xyz] is
; multiplied to achieve the ``right'' Jacobian d[xyz]/d[xyz]^prime. The net
; result in the Jacobian stagger routines are obtained as
; df/d[xyz]^prime = (df/d[xyz])(d[xyz]/d[xyz]^prime)
  dy=1.

  dxidxdn=(1./ddydn(xm  ))(4:4+nx-1)		; 1./dx^prime
  dxidxup=(1./ddyup(xmdn))(4:4+nx-1)		; 1./dx^prime
  dzidzdn=(1./ddydn(zm  ))(4:4+nz-1)		; 1./dz^prime
  dzidzup=(1./ddyup(zmdn))(4:4+nz-1)		; 1./dz^prime

  dyidydn=(1./ddydn(ym  ))			; 1./dy^prime
  dyidyup=(1./ddyup(ymdn))			; 1./dy^prime
  dyidyup(4)=dyidyup(6)
  dyidyup(3)=dyidyup(7) & dyidydn(5)=dyidydn(5)
  dyidyup(2)=dyidyup(8) & dyidydn(2)=dyidydn(6)
  dyidyup(1)=dyidyup(9) & dyidydn(1)=dyidydn(7)
  dyidyup(0)=dyidyup(10)& dyidydn(0)=dyidydn(8)
  dyidyup(ny-5)=dyidyup(ny-6)
  dyidyup(ny-4)=dyidyup(ny-7) & dyidydn(ny-3)=dyidydn(ny-5)
  dyidyup(ny-3)=dyidyup(ny-8) & dyidydn(ny-3)=dyidydn(ny-5)
  dyidyup(ny-2)=dyidyup(ny-9) & dyidydn(ny-2)=dyidydn(ny-6)
  dyidyup(ny-1)=dyidyup(ny-10)& dyidydn(ny-1)=dyidydn(ny-7)

  dxidxdn=rebin(reform(dxidxdn,nx,1,1),nx,ny,nz)
  dxidxup=rebin(reform(dxidxup,nx,1,1),nx,ny,nz)
  dyidydn=rebin(reform(dyidydn,1,ny,1),nx,ny,nz)
  dyidyup=rebin(reform(dyidyup,1,ny,1),nx,ny,nz)
  dzidzdn=rebin(reform(dzidzdn,1,1,nz),nx,ny,nz)
  dzidzup=rebin(reform(dzidzup,1,1,nz),nx,ny,nz)

;  xm=xm(4:4+nx-1)
  ym=ym(4:4+ny-1)
  zm=zm(4:4+nz-1)
;  xmdn=xmdn(4:4+nx-1)
  ymdn=ymdn(4:4+ny-1)
  zmdn=zmdn(4:4+nz-1)
END
;-----------------------------------------------------------------------
PRO write_mesh,file,pack=pack,dpack=dpack
@common_cmesh
  default, file, 'mesh.dat'
  openw, u, /get, file
  printf, u, n_elements(xm)
  printf, u, xm, xmdn, dxidxup(*,0,0), dxidxdn(*,0,0)
  printf, u, n_elements(ym)
  printf, u, ym, ymdn, reform(dyidyup(0,*,0)), reform(dyidydn(0,*,0))
  printf, u, n_elements(zm)
  printf, u, zm, zmdn, reform(dzidzup(0,0,*)), reform(dzidzdn(0,0,*))
  printf, u, pack, dpack
  free_lun, u
END
;-----------------------------------------------------------------------
PRO jet_mesh, pack, dpack, file=filearg, dim=dim, sizes=sizes
@common_cdata

  default,pack,0.7
  default,dpack,0.2

  if n_elements(filearg) eq 0 then begin
    files=findfile('*.dim',count=nf)
    if nf gt 0 then filearg=files(0) else begin
      print,'no *.dim file'
      return
    end
  end
  file=fileroot(filearg)

  if n_elements(dim) eq 0 or n_elements(sizes) eq 0 then begin
    openr, /get, u, file+'.dim'
    readf,u,nx,ny,nz
    sx=0.0 & sy=sx & sz=sx
    readf,u,sx,sy,sz
    free_lun,u
    print,file+'.dim:, sizes=',sx,sy,sz
  end

  if n_elements(dim) ne 0 then begin
    nx=dim(0)
    ny=dim(1)
    nz=dim(2)
  end

  if n_elements(sizes) ne 0 then begin
    sx=sizes(0)
    sy=sizes(1)
    sz=sizes(2)
  end
  print,nx,ny,nz
  print,sx,sy,sz

  dx=sx/nx
  dy=sy/(ny-10)		; ghostzone
  dz=sz/nz

  s=size(pack)
  if s(0) eq 0 then pack=[pack,pack,pack]
  s=size(dpack)
  if s(0) eq 0 then dpack=[dpack,0.5,dpack]

  mesh_init,pack,dpack

  write_mesh,file+'.mesh',pack=pack,dpack=dpack

  openw, u, file+'-mesh.log', /get_lun
  printf, u, 'pack =[',pack(0),',',pack(1),',',pack(2),']'
  printf, u, 'dpack=[',dpack(0),',',dpack(1),',',dpack(2),']'
  printf, u, 'dim  =[',nx,',',ny,',',nz,']'
  printf, u, 'sizes=[',sx,',',sy,',',sz,']'
  free_lun, u

END
;-----------------------------------------------------------------------
PRO jacobi_mesh, pack, dpack, file=filearg, dim=dim, sizes=sizes
  jet_mesh, pack, dpack, file=filearg, dim=dim, sizes=sizes
END
