common cunits, uur, uul, uut, uuv, uua, uue, uup, uug, uub

uur      = 1.0e-7
uul	 = 1.0e8
uut	 = 1.0e2

uuv	 = uul/uut
uua	 =  1./uul
uue	 = uuv*uuv

uup      = uur*uue
uug      = uuv/uut

uub      = sqrt(4.0*!pi*uup)

stefboltz = 5.6705e-5 ;cgs
stefboltz = stefboltz / uup/uuv

