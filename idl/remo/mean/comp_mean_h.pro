pro comp_mean_h, flist, fmesh, dir=dir, tabfile=tabfile, $
                 nstep=nstep, it=it, nghost=nghost, mmod=mmod, tmmod=tmmod, $
                 do_mhd=do_mhd, do_minimal=do_minimal, do_kapstd=do_kapstd, $
                 do_spline=do_spline, do_cgs=do_cgs, $
                 swap_endian=swap_endian

  IdStr = 'comp_mean_h'

;+
; This is a cleaner version of the averaging routine, which now
; includes averages of useful additional quantities such as T^4, lnPg,
; lnPe, etc.
;
;
; ****  This version of the routine works with cgs units and then
; converts to internal units, if so wished  ****
;
;
; A normalization factor tempFact is used internally to compute
; averages of T^4 and T^8 to avoid float overflows during the
; calculations (sums). Temperatures are then scaled back at the end of
; the routine.
;
;
; MEANING OF ENTRIES IN OUTPUT DATA STRUCTURE:
;
;  file:       snapshot's filename 
;  isnap:      snapshot number (in mmod: number of snapshots used for temporal average)
;  tabfile:    EOS table file
;  ltau:       reference optical depth scale
;  opac:       'standard' or 'Rosseland' (optical depth scale)
;  do_cgs:     1= cgs units;   0= internal stagger-code's units            
;  do_kapstd:  same as opac: 1= standard;  0= Rosseland            
;  do_minimal: 1= averages of selected variables only; 
;              0= averages of all variables
;     
;  r:    density,           rsq:   average density^2, 
;  lnr:  ln density,  	    lnrsq: (average ln density)^2, 
;  t:    temperature,       tsq:   temperature^2,  
;  t4:   temperature^4,	    t4sq:  temperature^8 ,  
;  z:    depth,             zsq:   depth^2, 
;  zs:   depth, shifted so that zs=0 at tau=1,  zssq: (shifted depth)^2, 
;  pg:   gas pressure,              pgsq:   (gas press.)^2, 
;  lnpg: ln gas pressure,  	    lnpgsq: (ln gas press.)^2, 
;  pe:   electron pressure,  	    pesq:   (electron press.)^2, 
;  lnpe: ln electron pressure,      lnpesq: (ln electron press.)^2, 
;  ux:   velocity, x-component (horizontal),  	uxsq: (x-velocity)^2, 
;  uy:   velocity, y-component (horizontal),  	uysq: (y-velocity)^2, 
;  uz:   velocity, z-component (VERTICAL!),  	uzsq: (z-velocity)^2, 
;  u:    velocity modulus,  		    usq:  (velocity modulus)^2,
;  ruz:  mass flux, vertical, cell-centred,  ruzsq: (mass flux, vertical)^2,              
;  ee:   internal energy per unit mass,     eesq: energy^2  , 
;  pt:   turbulent pressure <r*uz^2>,       ptsq: (turb. press.)^2,
;  lnptot: ln total pressure (pg+pt), lnptotsq: (ln ptot)^2, 
;  abStd:    standard extinction coefficient,      
;  lnabStd:  ln of standard extinction coeff.,       
;  abRoss:   Rosseland extinction coeff.,      
;  lnabRoss: ln of Rosseland extinction coeff.,
;  ekin1: kinetic energy,
;  gam1: gamma1 adiabatic exponent,
;  cm:   average column mass density
;
;  bx, by, bz: magnetic field components
;  b, bsq: magnetic field modulus (and squared)
;-

@common_cdata
@common_cmesh
@common_cunits

  default, taumin, 1.0e-8
  default, dir, './'
  default, swap_endian, 0L
  default, nstep,  1L
  default, nghost, 5L
  default, it, 0L
  default, tabfile, 'EOSrhoe.tab'
  default, do_spline, 0L
  default, do_kapstd, 0L
  default, do_cgs,    0L
  default, do_minimal,0L

  default, kboltz, 1.380658e-16

; normalisation factors for temperature and magnetic field
  tempFact = 1.0e4
  bFact    = 1.0e3

; minimum temperature
  temp_min = 1.6e3

; units: do everything in CGS first, then convert to internal at the end if necessary
  if KEYWORD_SET(do_cgs) then begin
     ur = 1.
     ul = 1.
     ut = 1.
     uv = 1.
     ua = 1.
     ue = 1.
     up = 1.
     ub = 1.
  endif else begin
     ur = uur
     ul = uul
     ut = uut
     uv = uuv
     ua = uua
     ue = uue
     up = uup
     ub = uub
  endelse


; spline interpolation?
  if KEYWORD_SET(do_spline) then linear=0 else linear=1
  print, '% '+IdStr+' comp_mean_horiz: linear interpolation switch=',linear
  
; change to work directory
  cd, current=current_dir
  cd, dir

; determine number of files in flist. If less than 1, exit. 
  nfiles   = size( flist, /n_elem )
  if (nfiles lt 1) then begin
     print, '% '+IdStr+' : flist is empty. STOP.'
     stop
  endif 

; read first snapshot. If flist contains one file only, then assume that
; there are multiple snapshots per file, else assume that each file in flist
; contains one and only one snapshot.
; In either case, the total number of snapshots will be assigned to nsnap (=nt or =nfiles).

  simfile = flist[0]
  open, a, simfile, swap_endian=swap_endian, nt=nt
  readmesh, fmesh, swap_endian=swap_endian, xm=xm,ym=ym,zm=zm

  if (nfiles eq 1) then begin
     nsnap = nt
     if (it gt nt-1) then begin
        print, '% '+IdStr+' : IT has to be smaller than NT-1: STOP. IT,NT-1=',it,nt-1
        stop
     endif
  endif else begin
     nsnap = nfiles
     if (it gt nfiles-1) then begin
        print, '% '+IdStr+' : IT has to be smaller than NFILES-1: STOP. IT,NFILES-1=',it,nfiles-1
        stop
     endif
  endelse


; load EOS table (with Rosseland Opacity)
  eostab, tabfile 

; consider physical domain only
  ub   = ny -nghost -1L
  lb   = nghost
  ndep = ub-lb+1

; define work arrays
  mx     = nx/nstep
  mz     = nz/nstep

  smallCube = fltarr(mx,mz,ndep) *0.0
  
  lnr1   = smallCube
  rho1   = smallCube
  tt1    = smallCube

  lnpp1  = smallCube
  lnnel1 = smallCube
  pp1    = smallCube
  pe1    = smallCube
  ux1    = smallCube
  uy1    = smallCube
  uz1    = smallCube
  ruz1   = smallCube            ; mass flux, vertical direction, cell-centred
  pt1    = smallCube
  lnptot1= smallCube
  ee1    = smallCube
  lnab1  = smallCube

; define mask array
  mask    = lonarr(nx,nz,ndep) * 0L
  for i=0L,nx-1,nstep do begin
     for k=0,nz-1,nstep do begin
        mask[ i, k, *]  =  1L
     endfor
  endfor
  wwmask = where(mask eq 1)

; define structures, small and large
  mmod_small = { mmod_h_ver0_small, $
                 name:       'mean_h_ver0', $
                 file:       '', $
                 isnap:      0L, $
                 tabfile:    '', $
                 do_cgs:     0L, $ 
                 do_minimal: 0L, $ 
                 r:    fltarr(ndep),  rsq:    fltarr(ndep), $
                 lnr:  fltarr(ndep),  lnrsq:  fltarr(ndep), $
                 t:    fltarr(ndep),  tsq:    fltarr(ndep), $ 
                 t4:   fltarr(ndep),  t4sq:   fltarr(ndep), $
                 z:    fltarr(ndep)  }
  mmod_large = { mmod_h_ver0_large, $
                 inherits mmod_h_ver0_small, $
                 pg:   fltarr(ndep),  pgsq:   fltarr(ndep), $
                 lnpg: fltarr(ndep),  lnpgsq: fltarr(ndep), $
                 pe:   fltarr(ndep),  pesq:   fltarr(ndep), $
                 lnpe: fltarr(ndep),  lnpesq: fltarr(ndep), $
                 ux:   fltarr(ndep),  uxsq:   fltarr(ndep), $
                 uy:   fltarr(ndep),  uysq:   fltarr(ndep), $
                 uz:   fltarr(ndep),  uzsq:   fltarr(ndep), $
                 u:    fltarr(ndep),  usq:    fltarr(ndep), $
                 ruz:  fltarr(ndep),  ruzsq:  fltarr(ndep), $
                 ee:   fltarr(ndep),  eesq:   fltarr(ndep), $
                 pt:   fltarr(ndep),  ptsq:   fltarr(ndep), $
                 lnptot: fltarr(ndep),  lnptotsq:   fltarr(ndep), $
                 ekin: fltarr(ndep),                        $
                 gam1: fltarr(ndep),                        $
                 abStd:    fltarr(ndep),                    $
                 lnabStd:  fltarr(ndep),                    $ 
                 abRoss:   fltarr(ndep),                    $
                 lnabRoss: fltarr(ndep) }
  ;;               cm: fltarr(ndep)  } 
  mmod_mhd = { mmod_h_ver0_mhd, $
               inherits mmod_h_ver0_large, $
               bx: fltarr(ndep), bxsq: fltarr(ndep), $
               by: fltarr(ndep), bysq: fltarr(ndep), $
               bz: fltarr(ndep), bzsq: fltarr(ndep), $
               b:  fltarr(ndep), bsq:  fltarr(ndep) }
  if (do_minimal) then mmod = mmod_small else mmod = mmod_large
  if (do_mhd) then mmod = mmod_mhd
  tmmod  = replicate(mmod, nsnap)


; loop over snapshots
  for isnap=0L,nsnap-1 do begin
     print, 'isnap=',isnap
     if (nfiles eq 1 ) then begin
        t = isnap
     endif else begin        
        t = 0L
        simfile = flist[isnap]
        open, a, simfile, swap_endian=swap_endian, meshfile=fmesh, nt=nt
        readmesh, fmesh, swap_endian=swap_endian, xm=xm,ym=ym,zm=zm       
     endelse

; geometrical depth
     depth = ym[lb:ub]

; load density and temperature data cubes;
; convert to cgs units
     print, '% '+IdStr+' : load density, temperature, and internal energy'
     rho   = ( transpose(a[0+nvar*t],[0,2,1]) )[*,*,lb:ub]
     tt    = ( transpose(a[5+nvar*t],[0,2,1]) )[*,*,lb:ub]
     e     = ( transpose(a[4+nvar*t],[0,2,1]) )[*,*,lb:ub]
     lnr   = alog(rho)
     ee    = e/rho

; load velocities
     if (~do_minimal) then begin
        print, '% '+IdStr+' : load velocity components'
        ux    = ( transpose(xup(a[1+nvar*t]),[0,2,1]) )[*,*,lb:ub] / rho
        uy    = ( transpose(zup(a[3+nvar*t]),[0,2,1]) )[*,*,lb:ub] / rho
        ruz   = ( transpose(yup(a[2+nvar*t]),[0,2,1]) )[*,*,lb:ub]
        uz    = ruz / rho
     endif

; magnetic field
     if (do_mhd) then begin
        print, '% '+IdStr+' : load magnetic field components'
        bx    = ( transpose(xup(a[6+nvar*t]),[0,2,1]) )[*,*,lb:ub]
        by    = ( transpose(zup(a[8+nvar*t]),[0,2,1]) )[*,*,lb:ub]
        bz    = ( transpose(yup(a[7+nvar*t]),[0,2,1]) )[*,*,lb:ub]
     endif
     
; convert to cgs units
; scalars and depth scale
     rho   = rho  * uur
     lnr   = lnr  + alog(uur)
     ee    = ee   * uuv * uuv
     e     = e    * uur * uuv * uuv
     depth = depth* uul
     rho1  = reform(rho[wwmask],mx,mz,ndep)
     lnr1  = reform(lnr[wwmask],mx,mz,ndep)
     tt1   = reform( tt[wwmask],mx,mz,ndep)
     e1    = reform(  e[wwmask],mx,mz,ndep)
     ee1   = reform( ee[wwmask],mx,mz,ndep)
; normalise temperature before averaging (rescale afterwards)
     tt1   = tt1 / tempFact
     lntt1 = alog(tt1)
; velocity field
     if (~do_minimal) then begin
        ux    =  ux * uuv  
        uy    =  uy * uuv
        uz    =  uz * uuv
        ruz   = ruz * uuv * uur
        ux1   = reform(  ux[wwmask],mx,mz,ndep )
        uy1   = reform(  uy[wwmask],mx,mz,ndep )
        uz1   = reform(  uz[wwmask],mx,mz,ndep )
        ruz1  = reform( ruz[wwmask],mx,mz,ndep )
     endif
; magnetic field
     if (do_mhd) then begin
        bx    =  bx * uub
        by    =  by * uub
        bz    =  bz * uub
        bx1   = reform( bx[wwmask],mx,mz,ndep )
        by1   = reform( by[wwmask],mx,mz,ndep )
        bz1   = reform( bz[wwmask],mx,mz,ndep )
; normalise magnetic fields before averaging (rescale afterwards)
        bx1   = bx1 / bFact
        by1   = by1 / bFact
        bz1   = bz1 / bFact
     endif    
     

; compute averages
     print, '% '+IdStr+' : computing horizontal averages'

; add header information

; add snapshot file name and table file name info:
     tmmod[isnap].file      = simfile
     tmmod[isnap].isnap     = isnap
     tmmod[isnap].tabfile   = tabfile

; add information about tau scale, reference opacity, interpolation,
; units, etc.
     tmmod[isnap].do_cgs     = do_cgs
     tmmod[isnap].do_minimal = do_minimal

; depth scale
     z_hav      = depth
     z_hav      = z_hav    /ul

; compute average for main quantities (density, temperature, etc.)
     r_hav      = haver( rho1,  /zvert)   /ur
     rsq_hav    = haver( rho1^2,/zvert)   /ur/ur
     lnr_hav    = haver( lnr1-alog(ur),    /zvert)  
     lnrsq_hav  = haver((lnr1-alog(ur)^2), /zvert)
     t_hav      = haver( tt1,   /zvert)    
     tsq_hav    = haver( tt1^2, /zvert)   
     t4_hav     = haver( tt1^4, /zvert)    
     t4sq_hav   = haver( tt1^8, /zvert)

     tmmod[isnap].r          = r_hav
     tmmod[isnap].rsq        = rsq_hav
     tmmod[isnap].lnr        = lnr_hav
     tmmod[isnap].lnrsq      = lnrsq_hav
     tmmod[isnap].t          = t_hav
     tmmod[isnap].tsq        = tsq_hav
     tmmod[isnap].t4         = t4_hav
     tmmod[isnap].t4sq       = t4sq_hav

     tmmod[isnap].z          = z_hav

; compute averages for other quantites
     if (~do_minimal) then begin

; lookup gas pressure, electron number density, extinction coefficient
; compute gamma1 exponent
        print, '% '+IdStr+' : look up EOS variables'

        lnpg1     = lookup_eos( lnr1, alog(ee1), 0,  0 )
        lnnel1    = lookup_eos( lnr1, alog(ee1), 3,  0 )
        lnabStd1  = lookup_eos( lnr1, alog(ee1), 5, 0 )
        lnabRoss1 = lookup_eos( lnr1, alog(ee1), 1, 0 )
        gam1_1    = gam1_eos( lnr1, alog(ee1) )

; electron pressure, gas and turbulent pressure, 
; standard and rosseland opacities:
        lnpe1   = lnnel1 + lntt1 + alog( kboltz )
        pg1     = exp( lnpg1 )
        pe1     = exp( lnpe1 )
        pt1     = rho1 * uz1^2
        abStd1  = exp( lnabStd1 )
        abRoss1 = exp( lnabRoss1 )

; ln ptot = ln (pg + pt)
        lnptot1 = alog(pg1+pt1)

; velocity squared
        usq1        = ux1^2 + uy1^2 + uz1^2

; kinetic energy
        ekin1       = 0.5 * rho1 * usq1
        
        ee_hav      = haver( ee1,   /zvert)   / ue 
        eesq_hav    = haver( ee1^2, /zvert)   / ue / ue
        ux_hav      = haver( ux1,   /zvert)   / uv
        uxsq_hav    = haver( ux1^2, /zvert)   / uv / uv
        uy_hav      = haver( uy1,   /zvert)   / uv
        uysq_hav    = haver( uy1^2, /zvert)   / uv / uv
        uz_hav      = haver( uz1,   /zvert)   / uv
        uzsq_hav    = haver( uz1^2, /zvert)   / uv / uv
        u_hav       = haver( sqrt(usq1),/zvert)    / uv
        usq_hav     = haver( usq1,  /zvert)   / uv / uv
        u_hav       = haver( sqrt(usq1),/zvert)    / uv
        ruz_hav     = haver( ruz1,   /zvert)   / uv / ur
        ruzsq_hav   = haver( ruz1^2, /zvert)   / uv / uv / ur / ur
        ekin_hav    = haver( ekin1, /zvert)   / up
        pg_hav      = haver( pg1,   /zvert)   / up
        pgsq_hav    = haver( pg1^2, /zvert)   / up / up
        lnpg_hav    = haver( lnpg1-alog(up),    /zvert)
        lnpgsq_hav  = haver((lnpg1-alog(up))^2, /zvert)
        pe_hav      = haver( pe1,   /zvert)   / up 
        pesq_hav    = haver( pe1^2, /zvert)   / up / up
        lnpe_hav    = haver( lnpe1-alog(up),    /zvert)
        lnpesq_hav  = haver((lnpe1-alog(up))^2, /zvert)
        pt_hav      = haver( pt1,   /zvert)   / up
        ptsq_hav    = haver( pt1^2, /zvert)   / up / up
        lnptot_hav  = haver( lnptot1-alog(up),    /zvert)
        lnptotsq_hav= haver((lnptot1-alog(up))^2, /zvert)
        abStd_hav   = haver( abStd1,   /zvert)   / ua
        abRoss_hav  = haver( abRoss1,  /zvert)   / ua
        lnabStd_hav = haver( lnabStd1-alog(ua),   /zvert)
        lnabRoss_hav= haver( lnabRoss1-alog(ua),  /zvert)
        gam1_hav    = haver( gam1_1,  /zvert)
        
        tmmod[isnap].ee      = ee_hav 
        tmmod[isnap].eesq    = eesq_hav
        tmmod[isnap].ux      = ux_hav
        tmmod[isnap].uxsq    = uxsq_hav
        tmmod[isnap].uy      = uy_hav
        tmmod[isnap].uysq    = uysq_hav
        tmmod[isnap].uz      = uz_hav
        tmmod[isnap].uzsq    = uzsq_hav
        tmmod[isnap].u       = u_hav
        tmmod[isnap].usq     = usq_hav
        tmmod[isnap].ruz     = ruz_hav
        tmmod[isnap].ruzsq   = ruzsq_hav
        tmmod[isnap].ekin    = ekin_hav
        tmmod[isnap].pg      = pg_hav
        tmmod[isnap].pgsq    = pgsq_hav
        tmmod[isnap].lnpg    = lnpg_hav
        tmmod[isnap].lnpgsq  = lnpgsq_hav
        tmmod[isnap].pe      = pe_hav
        tmmod[isnap].pesq    = pesq_hav
        tmmod[isnap].lnpe    = lnpe_hav
        tmmod[isnap].lnpesq  = lnpesq_hav
        tmmod[isnap].pt      = pt_hav
        tmmod[isnap].ptsq    = ptsq_hav
        tmmod[isnap].lnptot  = lnptot_hav
        tmmod[isnap].lnptotsq= lnptotsq_hav
        tmmod[isnap].abStd   = abStd_hav
        tmmod[isnap].abRoss  = abRoss_hav
        tmmod[isnap].lnabStd = lnabStd_hav
        tmmod[isnap].lnabRoss= lnabRoss_hav
        tmmod[isnap].gam1    = gam1_hav

     endif                      ; ~do_minimal

; MHD
     if (do_mhd) then begin
; magnetic field, squared
        bsq1        = bx1^2 + by1^2 + bz1^2
; averages
        bx_hav      = haver( bx1,   /zvert)   / ub
        bxsq_hav    = haver( bx1^2, /zvert)   / ub / ub
        by_hav      = haver( by1,   /zvert)   / ub
        bysq_hav    = haver( by1^2, /zvert)   / ub / ub
        bz_hav      = haver( bz1,   /zvert)   / ub
        bzsq_hav    = haver( bz1^2, /zvert)   / ub / ub
        b_hav       = haver( sqrt(bsq1), /zvert) / ub /ub
        bsq_hav     = haver( bsq1,  /zvert)   / ub / ub
; store in tmmod
        tmmod[isnap].bx   = bx_hav
        tmmod[isnap].bxsq = bxsq_hav
        tmmod[isnap].by   = by_hav
        tmmod[isnap].bysq = bysq_hav
        tmmod[isnap].bz   = bz_hav
        tmmod[isnap].bzsq = bzsq_hav
        tmmod[isnap].b    = b_hav
        tmmod[isnap].bsq  = bsq_hav
     endif                      ; do_mhd
     
  endfor                        ; isnap
  
; add snapshot file name and table file name info:
  mmod.file       = flist[0]
  mmod.isnap      = nsnap
  mmod.tabfile    = tabfile
  mmod.do_cgs     = do_cgs
  mmod.do_minimal = do_minimal

; compute averages over snapshots
  if (nsnap gt 1) then begin
     mmod.r       = total( tmmod.r,      2 ) / float( nsnap )
     mmod.rsq     = total( tmmod.rsq,    2 ) / float( nsnap )
     mmod.lnr     = total( tmmod.lnr,    2 ) / float( nsnap )
     mmod.lnrsq   = total( tmmod.lnrsq,  2 ) / float( nsnap )
     mmod.t       = total( tmmod.t,      2 ) / float( nsnap )
     mmod.tsq     = total( tmmod.tsq,    2 ) / float( nsnap )
     mmod.t4      = total( tmmod.t4,     2 ) / float( nsnap )
     mmod.t4sq    = total( tmmod.t4sq,   2 ) / float( nsnap )
     mmod.z       = total( tmmod.z,      2 ) / float( nsnap )
  endif else begin
     mmod.r       = tmmod.r
     mmod.rsq     = tmmod.rsq
     mmod.lnr     = tmmod.lnr
     mmod.lnrsq   = tmmod.lnrsq
     mmod.t       = tmmod.t
     mmod.tsq     = tmmod.tsq
     mmod.t4      = tmmod.t4
     mmod.t4sq    = tmmod.t4sq
     mmod.z       = tmmod.z
  endelse

; rescale temperatures (for both tmmod and mmod)
  tmmod[*].t    = tmmod[*].t    * tempFact
  tmmod[*].tsq  = tmmod[*].tsq  * tempFact^2
  tmmod[*].t4   = tmmod[*].t4   * tempFact^4
  tmmod[*].t4sq = tmmod[*].t4sq * tempFact^8
  mmod.t        = mmod.t        * tempFact
  mmod.tsq      = mmod.tsq      * tempFact^2
  mmod.t4       = mmod.t4       * tempFact^4
  mmod.t4sq     = mmod.t4sq     * tempFact^8


; temporal average: additional quantities
  if (~do_minimal) then begin
     if ( nsnap gt 1 ) then begin
        mmod.ee      = total( tmmod.ee,     2 ) / float( nsnap )
        mmod.eesq    = total( tmmod.eesq,   2 ) / float( nsnap )
        mmod.ux      = total( tmmod.ux,     2 ) / float( nsnap )
        mmod.uxsq    = total( tmmod.uxsq,   2 ) / float( nsnap )
        mmod.uy      = total( tmmod.uy,     2 ) / float( nsnap )
        mmod.uysq    = total( tmmod.uysq,   2 ) / float( nsnap )
        mmod.uz      = total( tmmod.uz,     2 ) / float( nsnap )
        mmod.uzsq    = total( tmmod.uzsq,   2 ) / float( nsnap )
        mmod.u       = total( tmmod.u,      2 ) / float( nsnap )
        mmod.usq     = total( tmmod.usq,    2 ) / float( nsnap )
        mmod.ruz     = total( tmmod.ruz,    2 ) / float( nsnap )
        mmod.ruzsq   = total( tmmod.ruzsq,  2 ) / float( nsnap )
        mmod.ekin    = total( tmmod.ekin,   2 ) / float( nsnap )
        mmod.pg      = total( tmmod.pg,     2 ) / float( nsnap )
        mmod.pgsq    = total( tmmod.pgsq,   2 ) / float( nsnap )
        mmod.lnpg    = total( tmmod.lnpg,   2 ) / float( nsnap )
        mmod.lnpgsq  = total( tmmod.lnpgsq, 2 ) / float( nsnap )
        mmod.pe      = total( tmmod.pe,     2 ) / float( nsnap )
        mmod.pesq    = total( tmmod.pesq,   2 ) / float( nsnap )
        mmod.lnpe    = total( tmmod.lnpe,   2 ) / float( nsnap )
        mmod.lnpesq  = total( tmmod.lnpesq, 2 ) / float( nsnap )
        mmod.pt      = total( tmmod.pt,     2 ) / float( nsnap )
        mmod.ptsq    = total( tmmod.ptsq,   2 ) / float( nsnap )
        mmod.lnptot  = total( tmmod.lnptot,   2 ) / float( nsnap )
        mmod.lnptotsq= total( tmmod.lnptotsq, 2 ) / float( nsnap )
        mmod.abStd   = total( tmmod.abStd,  2 ) / float( nsnap )
        mmod.lnabStd = total( tmmod.lnabStd,2 ) / float( nsnap )
        mmod.abRoss  = total( tmmod.abRoss,   2 ) / float( nsnap )
        mmod.lnabRoss= total( tmmod.lnabRoss, 2 ) / float( nsnap )
        mmod.gam1    = total( tmmod.gam1,   2 ) / float( nsnap )
     endif else begin
        mmod.ee      = tmmod.ee
        mmod.eesq    = tmmod.eesq
        mmod.ux      = tmmod.ux
        mmod.uxsq    = tmmod.uxsq
        mmod.uy      = tmmod.uy
        mmod.uysq    = tmmod.uysq
        mmod.uz      = tmmod.uz 
        mmod.uzsq    = tmmod.uzsq
        mmod.u       = tmmod.u  
        mmod.usq     = tmmod.usq
        mmod.ruz     = tmmod.ruz 
        mmod.ruzsq   = tmmod.ruzsq
        mmod.ekin    = tmmod.ekin
        mmod.pg      = tmmod.pg 
        mmod.pgsq    = tmmod.pgsq 
        mmod.lnpg    = tmmod.lnpg  
        mmod.lnpgsq  = tmmod.lnpgsq
        mmod.pe      = tmmod.pe   
        mmod.pesq    = tmmod.pesq 
        mmod.lnpe    = tmmod.lnpe
        mmod.lnpesq  = tmmod.lnpesq
        mmod.pt      = tmmod.pt
        mmod.ptsq    = tmmod.ptsq  
        mmod.lnptot  = tmmod.lnptot 
        mmod.lnptotsq  = tmmod.lnptotsq
        mmod.abStd   = tmmod.abStd    
        mmod.lnabStd = tmmod.lnabStd
        mmod.abRoss  = tmmod.abRoss    
        mmod.lnabRoss= tmmod.lnabRoss
        mmod.gam1    = tmmod.gam1
     endelse
  endif

                                ; MHD
  if (do_mhd) then begin
     if ( nsnap gt 1 ) then begin
        mmod.bx      = total( tmmod.bx,     2 ) / float( nsnap )
        mmod.bxsq    = total( tmmod.bxsq,   2 ) / float( nsnap )
        mmod.by      = total( tmmod.by,     2 ) / float( nsnap )
        mmod.bysq    = total( tmmod.bysq,   2 ) / float( nsnap )
        mmod.bz      = total( tmmod.bz,     2 ) / float( nsnap )
        mmod.bzsq    = total( tmmod.bzsq,   2 ) / float( nsnap )
        mmod.b       = total( tmmod.b,      2 ) / float( nsnap )
        mmod.bsq     = total( tmmod.bsq,    2 ) / float( nsnap )
     endif else begin
        mmod.bx      = tmmod.bx  
        mmod.bxsq    = tmmod.bxsq
        mmod.by      = tmmod.by
        mmod.bysq    = tmmod.bysq 
        mmod.bz      = tmmod.bz
        mmod.bzsq    = tmmod.bzsq
        mmod.b       = tmmod.b
        mmod.bsq     = tmmod.bsq
     endelse
; rescale magnetic fields
; tmmod
     tmmod[*].bx   = tmmod[*].bx   * bFact
     tmmod[*].bxsq = tmmod[*].bxsq * bFact^2
     tmmod[*].by   = tmmod[*].by   * bFact
     tmmod[*].bysq = tmmod[*].bysq * bFact^2
     tmmod[*].bz   = tmmod[*].bz   * bFact
     tmmod[*].bzsq = tmmod[*].bzsq * bFact^2
     tmmod[*].b    = tmmod[*].b    * bFact
     tmmod[*].bsq  = tmmod[*].bsq  * bFact^2
; mmod
     mmod.bx   = mmod.bx   * bFact
     mmod.bxsq = mmod.bxsq * bFact^2
     mmod.by   = mmod.by   * bFact
     mmod.bysq = mmod.bysq * bFact^2
     mmod.bz   = mmod.bz   * bFact
     mmod.bzsq = mmod.bzsq * bFact^2
     mmod.b    = mmod.b    * bFact
     mmod.bsq  = mmod.bsq  * bFact^2
  endif

  cd, current_dir
  
; free memory
  
  smallCube = 0.0
  
  rho       = 0.0
  ux        = 0.0
  uy        = 0.0
  uz        = 0.0
  e         = 0.0
  ee        = 0.0
  tt        = 0.0

  lnpp      = 0.0
  lnnel     = 0.0
  
  lnr1      = 0.0
  rho1      = 0.0
  tt1       = 0.0
  zz1       = 0.0
  lnpg1     = 0.0
  lnpe1     = 0.0
  lnnel1    = 0.0
  pg1       = 0.0
  pe1       = 0.0
  ux1       = 0.0
  uy1       = 0.0
  uz1       = 0.0
  usq1      = 0.0
  ruz1      = 0.0
  ekin1     = 0.0
  pt1       = 0.0
  lnptot1   = 0.0
  ee1       = 0.0
  abStd1    = 0.0
  lnabStd1  = 0.0
  abRoss1   = 0.0
  lnabRoss1 = 0.0
  gam1_1    = 0.0
  cmd1      = 0.0

  bx1       = 0.0
  by1       = 0.0
  bz1       = 0.0
  
end
