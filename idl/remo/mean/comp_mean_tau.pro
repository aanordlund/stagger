pro comp_mean_tau, flist, fmesh, ltau, $
                   mmod=mmod, tmmod=tmmod, dir=dir, tabfile=tabfile, $
                   taumin=taumin, nstep=nstep, it=it, nghost=nghost, $
                   do_mhd=do_mhd, do_minimal=do_minimal, $
                   do_kapstd=do_kapstd, do_spline=do_spline, $
                   do_cgs=do_cgs, extrapolate=extrapolate, $
                   swap_endian=swap_endian, stop=stop

  IdStr = 'comp_mean_tau'

; note: this routine was formerly known as comp_mean_tau_ver1.pro

;+
; This is a cleaner version of the averaging routine, which now
; includes averages of useful additional quantities such as T^4, lnPg,
; lnPe, etc.
;
; This routine first interpolates the independent variables from the
; fixed depth scale to the reference optical depth scale, then looks
; up the dependent variables from the EOS table
; **** (i.e. interpolation first, then lookup .) ****
;
; ****  This version of the routine works with cgs units and then
; converts to internal units, if so wished  ****
;
; A normalization factor tempFact is used internally to compute
; averages of T^4 and T^8 to avoid float overflows during the
; calculations (sums). Temperatures are then scaled back at the end of
; the routine.
;
; MEANING OF ENTRIES IN OUTPUT DATA STRUCTURE:
;
;  file:       snapshot's filename 
;  isnap:      snapshot number
;              (in mmod: num of snapshots used for temporal average)
;  tabfile:    EOS table file
;  ltau:       reference optical depth scale
;  opac:       'standard' or 'Rosseland' (optical depth scale)
;  do_cgs:     1= cgs units;   0= internal stagger-code's units
;  do_kapstd:  same as opac: 1= standard;  0= Rosseland
;  do_minimal: 1= averages of selected variables only; 
;              0= averages of all variables
;
;  r:    density,           rsq:   average density^2, 
;  lnr:  ln density,  	    lnrsq: (average ln density)^2, 
;  t:    temperature,       tsq:   temperature^2,  
;  t4:   temperature^4,	    t4sq:  temperature^8 ,  
;  z:    depth,             zsq:   depth^2, 
;  zs:   depth, shifted so that zs=0 at tau=1,  zssq: (shifted depth)^2, 
;  pg:   gas pressure,              pgsq:   (gas press.)^2, 
;  lnpg: ln gas pressure,  	    lnpgsq: (ln gas press.)^2, 
;  pe:   electron pressure,  	    pesq:   (electron press.)^2, 
;  lnpe: ln electron pressure,      lnpesq: (ln electron press.)^2, 
;  ux:   velocity, x-component (horizontal),  	uxsq: (x-velocity)^2, 
;  uy:   velocity, y-component (horizontal),  	uysq: (y-velocity)^2, 
;  uz:   velocity, z-component (VERTICAL!),  	uzsq: (z-velocity)^2, 
;  u:    velocity modulus,  		    usq:  (velocity modulus)^2, 
;  ee:   internal energy per unit mass,     eesq: energy^2  , 
;  pt:   turbulent pressure <r*uz^2>,       ptsq: (turb. press.)^2  , 
;  abStd:    standard extinction coefficient,
;  lnabStd:  ln of standard extinction coeff.,
;  abRoss:   Rosseland extinction coeff.,
;  lnabRoss: ln of Rosseland extinction coeff.,
;  cm:   average column mass density
;
;  bx, by, bz: magnetic field components
;  b, bsq: magnetic field modulus (and squared)
;-

@common_cdata
@common_cmesh
@common_cunits

  if n_elements(taumin) eq 0 then taumin=1.0e-8
  if n_elements(dir) eq 0 then dir='.'
  if n_elements(swap_endian) eq 0 then swap_endian=0
  if n_elements(nstep) eq 0 then nstep=4L
  if n_elements(nghost) eq 0 then nghost=5L
  if n_elements(it) eq 0 then it=0L
  if n_elements(tabfile) eq 0 then tabfile='EOSrhoe.tab'
  if n_elements(do_minimal) eq 0 then do_minimal=0
  if n_elements(do_spline) eq 0 then do_spline=0
  if n_elements(do_kapstd) eq 0 then do_kapstd=0
  if n_elements(do_cgs) eq 0 then do_cgs=0
  if n_elements(do_mhd) eq 0 then do_mhd=0

  kboltz_cgs = 1.380658e-16     ;Boltzmann constant cgs

; normalisation factors for temperature and magnetic field
  tempFact = 1.0e4
  bFact    = 1.0e3

; units: use cgs, then convert to internal, if so wished
  if KEYWORD_SET(do_cgs) then begin
     ur = 1.
     ul = 1.
     ut = 1.
     uv = 1.
     ua = 1.
     ue = 1.
     up = 1.
     ub = 1.                    ; magnetic field
  endif else begin
     ur = uur
     ul = uul
     ut = uut
     uv = uuv
     ua = uua
     ue = uue
     up = uup
     ub = uub                   ; magnetic field
  endelse


; spline interpolation?
  if (KEYWORD_SET(do_spline) and do_spline gt 0) then linear=0 else linear=1
  print, '% '+IdStr+' : linear interpolation switch=',linear

  ndep      = size(ltau, /n_elements)
  temp_min  = 1600.

; original directory
  cd, current=current_dir

; determine number of files in flist. If less than 1, exit.
  cd, dir
  nfiles   = size( flist, /n_elem )
  if (nfiles lt 1) then begin
     print, '% '+IdStr+' : flist is empty. STOP.'
     stop
  endif


; read first snapshot. If flist contains one file only, then assume that
; there are multiple snapshots per file, else assume that each file in flist
; contains one and only one snapshot.
; In either case, the total number of snapshots will be assigned to nsnap
; (=nt or =nfiles).

  simfile = flist[0]
  open, a, simfile, swap_endian=swap_endian, nt=nt
  readmesh, fmesh, swap_endian=swap_endian, xm=xm,ym=ym,zm=zm

  if (nfiles eq 1) then begin
     nsnap = nt
     if (it gt nt-1) then begin
        print, '% '+IdStr + $
               ' : IT has to be smaller than NT-1: STOP. IT,NT-1=', $
               it, nt-1
        stop
     endif
  endif else begin
     nsnap = nfiles
     if (it gt nfiles-1) then begin
        print, '% '+IdStr + $
               ' : IT has to be smaller than NFILES-1: STOP. IT,NFILES-1=', $
               it, nfiles-1
        stop
     endif
  endelse


; read EOS table (with Rosseland Opacity)
  eostab, tabfile

; consider physical domain only
  nb = ny -nghost -1L
  na = nghost

  mx = nx/nstep
  mz = nz/nstep

  smallCube = fltarr(mx,mz,ndep) *0.0

  lnr1      = smallCube
  rho1      = smallCube
  ee1       = smallCube
  zz1       = smallCube

  lntt1     = smallCube
  lnpg1     = smallCube
  lnpe1     = smallCube
  lnnel1    = smallCube
  pg1       = smallCube
  pe1       = smallCube
  ux1       = smallCube
  uy1       = smallCube
  uz1       = smallCube
  pt1       = smallCube
  tt1       = smallCube
  abStd1    = smallCube
  lnabStd1  = smallCube
  abRoss1   = smallCube
  lnabRoss1 = smallCube
  cm1       = smallCube
  bx1       = smallCube
  by1       = smallCube
  bz1       = smallCube
  
; define data structures, small and large
  mmod_small  = { mmod_tau_ver1_small, $
                  name:       'mean_tau_ver1', $
                  file:       '', $
                  isnap:      0L, $
                  tabfile:    '', $
                  ltau:       fltarr(ndep), $
                  opac:       '',           $
                  do_cgs:     0L,           $
                  do_kapstd:  0L,           $
                  do_minimal: 0L,           $
                  nx: 0L, $
                  ny: 0L, $
                  nz: 0L, $
                  nstep: 0L, $
                  r:    fltarr(ndep),  rsq:    fltarr(ndep), $
                  lnr:  fltarr(ndep),  lnrsq:  fltarr(ndep), $
                  t:    fltarr(ndep),  tsq:    fltarr(ndep), $
                  t4:   fltarr(ndep),  t4sq:   fltarr(ndep), $
                  z:    fltarr(ndep),  zsq:    fltarr(ndep), $
                  zs:   fltarr(ndep),  zssq:   fltarr(ndep)  }
  mmod_large = { mmod_tau_ver1_large, $
                 inherits mmod_tau_ver1_small, $
                 pg:   fltarr(ndep),  pgsq:   fltarr(ndep), $
                 lnpg: fltarr(ndep),  lnpgsq: fltarr(ndep), $
                 pe:   fltarr(ndep),  pesq:   fltarr(ndep), $
                 lnpe: fltarr(ndep),  lnpesq: fltarr(ndep), $
                 ux:   fltarr(ndep),  uxsq:   fltarr(ndep), $
                 uy:   fltarr(ndep),  uysq:   fltarr(ndep), $
                 uz:   fltarr(ndep),  uzsq:   fltarr(ndep), $
                 u:    fltarr(ndep),  usq:    fltarr(ndep), $
                 ee:   fltarr(ndep),  eesq:   fltarr(ndep), $
                 pt:   fltarr(ndep),  ptsq:   fltarr(ndep), $
                 abStd:    fltarr(ndep),                    $
                 lnabStd:  fltarr(ndep),                    $
                 abRoss:   fltarr(ndep),                    $
                 lnabRoss: fltarr(ndep),                    $
                 cm: fltarr(ndep)  }
  mmod_mhd = { mmod_tau_ver1_mhd, $
               inherits mmod_tau_ver1_large, $
               bx: fltarr(ndep), bxsq: fltarr(ndep), $
               by: fltarr(ndep), bysq: fltarr(ndep), $
               bz: fltarr(ndep), bzsq: fltarr(ndep), $
               b:  fltarr(ndep), bsq:  fltarr(ndep) }

  if (do_minimal) then mmod = mmod_small else mmod = mmod_large
  if (do_mhd) then mmod = mmod_mhd
  tmmod  = replicate(mmod, nsnap)


; loop over snapshots
  for isnap=0L,nsnap-1 do begin
     print, 'isnap=',isnap
     if (nfiles eq 1 ) then begin
        t = isnap
     endif else begin
        t = 0L
        simfile = flist[isnap]
        open, a, simfile, swap_endian=swap_endian, meshfile=fmesh, nt=nt
        readmesh, fmesh, swap_endian=swap_endian, xm=xm,ym=ym,zm=zm
     endelse

; load data cubes
     print, '% '+IdStr+' : load density, temperature, and internal energy'

     rho   = transpose( a[*,na:nb,*,0+nvar*t], [0,2,1] ) 
     e     = transpose( a[*,na:nb,*,4+nvar*t], [0,2,1] )
     lnr   = alog(rho)
     ee    = e/rho
     depth = ym[na:nb]

; if not "do_minimal", lookup 
     if (~do_minimal) then begin
        print, '% '+IdStr+' : load velocity components'
        ux    = transpose( (xup(a[1+nvar*t]))[*,na:nb,*], [0,2,1] ) / rho
        uy    = transpose( (zup(a[3+nvar*t]))[*,na:nb,*], [0,2,1] ) / rho
        uz    = transpose( (yup(a[2+nvar*t]))[*,na:nb,*], [0,2,1] ) / rho
     endif

     if (do_mhd) then begin
        print, '% '+IdStr+' : load magnetic field components'
        bx    = transpose( (xup(a[6+nvar*t]))[*,na:nb,*], [0,2,1] )
        by    = transpose( (zup(a[8+nvar*t]))[*,na:nb,*], [0,2,1] )
        bz    = transpose( (yup(a[7+nvar*t]))[*,na:nb,*], [0,2,1] )
     endif

; convert to cgs units
; basic scalars and depth scale
     rho   = rho * uur
     e     = e   * uur * uuv * uuv
     ee    = ee  * uuv * uuv
     lnr   = lnr + alog(uur)
     depth = depth * uul
; velocities
     if (~do_minimal) then begin
        ux    = ux * uuv
        uy    = uy * uuv
        uz    = uz * uuv
     endif
; magnetic fields
     if (do_mhd) then begin
        bx = bx * uub
        by = by * uub
        bz = bz * uub
     endif 

; compute tau scale
     print, '% '+IdStr+' : compute tau'
     if (do_kapstd) then begin
        iv=5
        opac = 'standard'
        print, '% '+IdStr+' : lookup standard opacity'
     endif else begin
        iv=1
        opac = 'Rosseland'
        print, '% '+IdStr+' : lookup Rosseland opacity'
     endelse
     lnab  = lookup_eos(lnr,alog(ee),iv,0)
     tau   = tau_calc(lnab,depth,taumin=taumin,vertical=2)
     lgtau = alog10(tau)

; compute column mass density
     print, '% '+IdStr+' : compute column mass density'
     cmdens = tau_calc( lnr, depth, taumin=0.0, vertical=2) ; (cgs units)


; tau interpolation: interpolate the independent variables (lnr, ee,
; depth) to the reference optical depth scale
     print, '% '+IdStr+' : interpolate in tau'

     lnr1_tmp_1d = fltarr( ndep )
     ee1_tmp_1d  = fltarr( ndep )
     zz1_tmp_1d  = fltarr( ndep )
     cm1_tmp_1d  = fltarr( ndep )

     for i=0L,nx-1,nstep do begin
        for k=0,nz-1,nstep do begin

           lgtau_tmp_1d = reform( lgtau[i,k,*] )
           lnr_tmp_1d   = reform( lnr  [i,k,*] )
           ee_tmp_1d    = reform( ee   [i,k,*] )
           cm_tmp_1d    = reform(cmdens[i,k,*] )

           ww = where( ltau ge lgtau_tmp_1d[0] and $
                       ltau le lgtau_tmp_1d[nb-na-1], nww )
           lnr1_tmp_1d[ww] = interpol( lnr_tmp_1d, $
                                       lgtau_tmp_1d, $
                                       ltau[ww], $
                                       spline=do_spline )
           ee1_tmp_1d[ww]  = interpol( ee_tmp_1d, $
                                       lgtau_tmp_1d, $
                                       ltau[ww], $
                                       spline=do_spline )
           zz1_tmp_1d[ww]  = interpol( depth, $
                                       lgtau_tmp_1d, $
                                       ltau[ww], $
                                       spline=do_spline )
           cm1_tmp_1d[ww]  = interpol( cm_tmp_1d, $
                                       lgtau_tmp_1d, $
                                       ltau[ww], $
                                       spline=do_spline )

           lnr1_tmp_1d[0:ww[0]] = lnr1_tmp_1d[ww[0]] 
           ee1_tmp_1d [0:ww[0]] = ee1_tmp_1d [ww[0]] 
           zz1_tmp_1d [0:ww[0]] = zz1_tmp_1d [ww[0]] 
           cm1_tmp_1d [0:ww[0]] = cm1_tmp_1d [ww[0]]

           lnr1_tmp_1d[ww[nww-1]:ndep-1] = lnr1_tmp_1d[ww[nww-1]:ndep-1] 
           ee1_tmp_1d [ww[nww-1]:ndep-1] = ee1_tmp_1d [ww[nww-1]:ndep-1] 
           zz1_tmp_1d [ww[nww-1]:ndep-1] = zz1_tmp_1d [ww[nww-1]:ndep-1] 
           cm1_tmp_1d [ww[nww-1]:ndep-1] = cm1_tmp_1d [ww[nww-1]:ndep-1] 

           lnr1[i/nstep, k/nstep, *] = lnr1_tmp_1d 
           ee1[ i/nstep, k/nstep, *] = ee1_tmp_1d 
           zz1[ i/nstep, k/nstep, *] = zz1_tmp_1d
           cm1[ i/nstep, k/nstep, *] = cm1_tmp_1d
        endfor                  ; k
     endfor                     ; i


; exponentiate ln density;
     rho1  = exp( lnr1 )

; look up temperature; 
     lntt1 = lookup_eos(lnr1,alog(ee1),2,0)
     tt1   = exp( lntt1 )

; normalize temperature for internal calculations (will be rescaled
; later before end of routine.)
     tt1   = tt1 / tempFact

     
; add snapshot file name and table file name info:
     tmmod[isnap].file      = simfile
     tmmod[isnap].isnap     = isnap
     tmmod[isnap].tabfile   = tabfile

; add information about tau scale, reference opacity, interpolation,
; units, etc.
     tmmod[isnap].ltau       = ltau
     tmmod[isnap].opac       = opac
     tmmod[isnap].do_cgs     = do_cgs
     tmmod[isnap].do_kapstd  = do_kapstd
     tmmod[isnap].do_minimal = do_minimal
     tmmod[isnap].nx = nx
     tmmod[isnap].ny = ny
     tmmod[isnap].nz = nz
     tmmod[isnap].nstep = nstep
     
; compute average depth scales
     z_hav      = haver( zz1,   /zvert)
     zsq_hav    = haver( zz1^2, /zvert)
     z0         = interpol( z_hav, ltau, 0.0 ) ; where ltau=0
     zs_hav     = z_hav -z0                    ; shift depth scale
     zssq_hav   = haver( (zz1-z0)^2, /zvert)

     z_hav      = z_hav    /ul
     zsq_hav    = zsq_hav  /ul/ul
     z0         = z0       /ul
     zs_hav     = zs_hav   /ul
     zssq_hav   = zssq_hav /ul/ul

     r_hav      = haver( rho1,  /zvert)   /ur
     rsq_hav    = haver( rho1^2,/zvert)   /ur/ur
     lnr_hav    = haver( lnr1-alog(ur),    /zvert)  
     lnrsq_hav  = haver((lnr1-alog(ur)^2), /zvert)
     t_hav      = haver( tt1,   /zvert)    
     tsq_hav    = haver( tt1^2, /zvert)   
     t4_hav     = haver( tt1^4, /zvert)    
     t4sq_hav   = haver( tt1^8, /zvert)   

     tmmod[isnap].r      = r_hav
     tmmod[isnap].rsq    = rsq_hav
     tmmod[isnap].lnr    = lnr_hav
     tmmod[isnap].lnrsq  = lnrsq_hav
     tmmod[isnap].t      = t_hav
     tmmod[isnap].tsq    = tsq_hav
     tmmod[isnap].t4     = t4_hav
     tmmod[isnap].t4sq   = t4sq_hav

     tmmod[isnap].z      = z_hav
     tmmod[isnap].zsq    = zsq_hav 
     tmmod[isnap].zs     = zs_hav
     tmmod[isnap].zssq   = zssq_hav

     if (~do_minimal) then begin
        for i=0L,nx-1,nstep do begin
           for k=0,nz-1,nstep do begin
              lgtau_tmp_1  = reform( lgtau[i,k,*] )
              ux_tmp_1     = reform( ux[i,k,*] )
              uy_tmp_1     = reform( uy[i,k,*] )
              uz_tmp_1     = reform( uz[i,k,*] )
              i1 = i/nstep
              k1 = k/nstep
              ux1[i1,k1,*] = interpol( ux_tmp_1, $
                                       lgtau_tmp_1, $
                                       ltau, $
                                       spline=do_spline )
              uy1[i1,k1,*] = interpol( uy_tmp_1, $
                                       lgtau_tmp_1, $
                                       ltau, $
                                       spline=do_spline )
              uz1[i1,k1,*] = interpol( uz_tmp_1, $
                                       lgtau_tmp_1, $
                                       ltau, $
                                       spline=do_spline )
           endfor               ; k
        endfor                  ; i

; lookup gas pressure, electron number density, extinction coefficient
        lnpg1     = lookup_eos( lnr1, alog(ee1), 0,  0 )
        lnnel1    = lookup_eos( lnr1, alog(ee1), 3,  0 )
        lnabStd1  = lookup_eos( lnr1, alog(ee1), 5, 0 )
        lnabRoss1 = lookup_eos( lnr1, alog(ee1), 1, 0 )

;  electron pressure, gas and turbulent pressure, standard and
;  rosseland opacities:
        lnpe1   = lnnel1 + lntt1 + alog( kboltz_cgs )
        pg1     = exp( lnpg1 )
        pe1     = exp( lnpe1 )
        pt1     = rho1 * uz1^2
        abStd1  = exp( lnabStd1 )
        abRoss1 = exp( lnabRoss1 )

; velocity squared
        usq1        = ux1^2 + uy1^2 + uz1^2
        ee_hav      = haver( ee1,   /zvert)   / ue 
        eesq_hav    = haver( ee1^2, /zvert)   / ue / ue
        ux_hav      = haver( ux1,   /zvert)   / uv
        uxsq_hav    = haver( ux1^2, /zvert)   / uv / uv
        uy_hav      = haver( uy1,   /zvert)   / uv
        uysq_hav    = haver( uy1^2, /zvert)   / uv / uv
        uz_hav      = haver( uz1,   /zvert)   / uv
        uzsq_hav    = haver( uz1^2, /zvert)   / uv / uv
        u_hav       = haver( sqrt(usq1),/zvert)    / uv
        usq_hav     = haver( usq1,  /zvert)   / uv / uv
        pg_hav      = haver( pg1,   /zvert)   / up
        pgsq_hav    = haver( pg1^2, /zvert)   / up / up
        lnpg_hav    = haver( lnpg1-alog(up),    /zvert)
        lnpgsq_hav  = haver((lnpg1-alog(up))^2, /zvert)
        pe_hav      = haver( pe1,   /zvert)   / up 
        pesq_hav    = haver( pe1^2, /zvert)   / up / up
        lnpe_hav    = haver( lnpe1-alog(up),    /zvert)
        lnpesq_hav  = haver((lnpe1-alog(up))^2, /zvert)
        pt_hav      = haver( pt1,   /zvert)   / up
        ptsq_hav    = haver( pt1^2, /zvert)   / up / up
        abStd_hav   = haver( abStd1,   /zvert)   / ua
        abRoss_hav  = haver( abRoss1,  /zvert)   / ua
        lnabStd_hav = haver( lnabStd1-alog(ua),   /zvert)
        lnabRoss_hav= haver( lnabRoss1-alog(ua),  /zvert)
        cm_hav      = haver( cm1,   /zvert)   / ur / ul

        tmmod[isnap].ee      = ee_hav 
        tmmod[isnap].eesq    = eesq_hav
        tmmod[isnap].ux      = ux_hav
        tmmod[isnap].uxsq    = uxsq_hav
        tmmod[isnap].uy      = uy_hav
        tmmod[isnap].uysq    = uysq_hav
        tmmod[isnap].uz      = uz_hav
        tmmod[isnap].uzsq    = uzsq_hav
        tmmod[isnap].u       = u_hav
        tmmod[isnap].usq     = usq_hav
        tmmod[isnap].pg      = pg_hav
        tmmod[isnap].pgsq    = pgsq_hav
        tmmod[isnap].lnpg    = lnpg_hav
        tmmod[isnap].lnpgsq  = lnpgsq_hav
        tmmod[isnap].pe      = pe_hav
        tmmod[isnap].pesq    = pesq_hav
        tmmod[isnap].lnpe    = lnpe_hav
        tmmod[isnap].lnpesq  = lnpesq_hav
        tmmod[isnap].pt      = pt_hav
        tmmod[isnap].ptsq    = ptsq_hav
        tmmod[isnap].abStd   = abStd_hav
        tmmod[isnap].abRoss  = abRoss_hav
        tmmod[isnap].lnabStd = lnabStd_hav
        tmmod[isnap].lnabRoss= lnabRoss_hav
        tmmod[isnap].cm      = cm_hav

     endif                      ; ~do_minimal

; MHD
     if (do_mhd) then begin

        for i=0L,nx-1,nstep do begin
           for k=0,nz-1,nstep do begin
              lgtau_tmp_1  = reform( lgtau[i,k,*] )
              bx_tmp_1     = reform( bx[i,k,*] )
              by_tmp_1     = reform( by[i,k,*] )
              bz_tmp_1     = reform( bz[i,k,*] )
              i1 = i/nstep
              k1 = k/nstep
              bx1[i1,k1,*] = interpol( bx_tmp_1, $
                                       lgtau_tmp_1, $
                                       ltau, $
                                       spline=do_spline ) / bFact
              by1[i1,k1,*] = interpol( by_tmp_1, $
                                       lgtau_tmp_1, $
                                       ltau, $
                                       spline=do_spline ) / bFact
              bz1[i1,k1,*] = interpol( bz_tmp_1, $
                                       lgtau_tmp_1, $
                                       ltau, $
                                       spline=do_spline ) / bFact
           endfor               ; k
        endfor                  ; i

; magnetic field, squared
        bsq1        = bx1^2 + by1^2 + bz1^2
; averages
        bx_hav      = haver( bx1,   /zvert)   / ub
        bxsq_hav    = haver( bx1^2, /zvert)   / ub / ub
        by_hav      = haver( by1,   /zvert)   / ub
        bysq_hav    = haver( by1^2, /zvert)   / ub / ub
        bz_hav      = haver( bz1,   /zvert)   / ub
        bzsq_hav    = haver( bz1^2, /zvert)   / ub / ub
        b_hav       = haver( sqrt(bsq1), /zvert) / ub /ub
        bsq_hav     = haver( bsq1,  /zvert)   / ub / ub
; store in tmmod
        tmmod[isnap].bx   = bx_hav
        tmmod[isnap].bxsq = bxsq_hav
        tmmod[isnap].by   = by_hav
        tmmod[isnap].bysq = bysq_hav
        tmmod[isnap].bz   = bz_hav
        tmmod[isnap].bzsq = bzsq_hav
        tmmod[isnap].b    = b_hav
        tmmod[isnap].bsq  = bsq_hav
     endif                      ; do_mhd

  endfor                        ; isnap


; add snapshot file name and table file name info:
  mmod.file       = flist[0]
  mmod.isnap      = nsnap
  mmod.tabfile    = tabfile

  mmod.ltau       = ltau
  mmod.opac       = opac
  mmod.do_cgs     = do_cgs
  mmod.do_kapstd  = do_kapstd
  mmod.do_minimal = do_minimal
  mmod.nx = nx
  mmod.ny = ny
  mmod.nz = nz
  mmod.nstep = nstep

; compute averages over snapshots
  if (nsnap gt 1) then begin
     mmod.r       = total( tmmod.r,      2 ) / float( nsnap )
     mmod.rsq     = total( tmmod.rsq,    2 ) / float( nsnap )
     mmod.lnr     = total( tmmod.lnr,    2 ) / float( nsnap )
     mmod.lnrsq   = total( tmmod.lnrsq,  2 ) / float( nsnap )
     mmod.t       = total( tmmod.t,      2 ) / float( nsnap )
     mmod.tsq     = total( tmmod.tsq,    2 ) / float( nsnap )
     mmod.t4      = total( tmmod.t4,     2 ) / float( nsnap )
     mmod.t4sq    = total( tmmod.t4sq,   2 ) / float( nsnap )
     mmod.z       = total( tmmod.z,      2 ) / float( nsnap )
     mmod.zsq     = total( tmmod.zsq,    2 ) / float( nsnap )
     mmod.zs      = total( tmmod.zs,     2 ) / float( nsnap )
     mmod.zssq    = total( tmmod.zssq,   2 ) / float( nsnap )
  endif else begin
     mmod.r       = tmmod.r
     mmod.rsq     = tmmod.rsq
     mmod.lnr     = tmmod.lnr
     mmod.lnrsq   = tmmod.lnrsq
     mmod.t       = tmmod.t
     mmod.tsq     = tmmod.tsq
     mmod.t4      = tmmod.t4
     mmod.t4sq    = tmmod.t4sq
     mmod.z       = tmmod.z
     mmod.zsq     = tmmod.zsq
     mmod.zs      = tmmod.zs
     mmod.zssq    = tmmod.zssq
  endelse

; rescale temperatures! Need to do that for both tmmod and mmod
  tmmod[*].t    = tmmod[*].t    * tempFact
  tmmod[*].tsq  = tmmod[*].tsq  * tempFact^2
  tmmod[*].t4   = tmmod[*].t4   * tempFact^4
  tmmod[*].t4sq = tmmod[*].t4sq * tempFact^8
  mmod.t        = mmod.t        * tempFact
  mmod.tsq      = mmod.tsq      * tempFact^2
  mmod.t4       = mmod.t4       * tempFact^4
  mmod.t4sq     = mmod.t4sq     * tempFact^8

  if (~do_minimal) then begin
     if ( nsnap gt 1 ) then begin
        mmod.ee      = total( tmmod.ee,     2 ) / float( nsnap )
        mmod.eesq    = total( tmmod.eesq,   2 ) / float( nsnap )
        mmod.ux      = total( tmmod.ux,     2 ) / float( nsnap )
        mmod.uxsq    = total( tmmod.uxsq,   2 ) / float( nsnap )
        mmod.uy      = total( tmmod.uy,     2 ) / float( nsnap )
        mmod.uysq    = total( tmmod.uysq,   2 ) / float( nsnap )
        mmod.uz      = total( tmmod.uz,     2 ) / float( nsnap )
        mmod.uzsq    = total( tmmod.uzsq,   2 ) / float( nsnap )
        mmod.u       = total( tmmod.u,      2 ) / float( nsnap )
        mmod.usq     = total( tmmod.usq,    2 ) / float( nsnap )
        mmod.pg      = total( tmmod.pg,     2 ) / float( nsnap )
        mmod.pgsq    = total( tmmod.pgsq,   2 ) / float( nsnap )
        mmod.lnpg    = total( tmmod.lnpg,   2 ) / float( nsnap )
        mmod.lnpgsq  = total( tmmod.lnpgsq, 2 ) / float( nsnap )
        mmod.pe      = total( tmmod.pe,     2 ) / float( nsnap )
        mmod.pesq    = total( tmmod.pesq,   2 ) / float( nsnap )
        mmod.lnpe    = total( tmmod.lnpe,   2 ) / float( nsnap )
        mmod.lnpesq  = total( tmmod.lnpesq, 2 ) / float( nsnap )
        mmod.pt      = total( tmmod.pt,     2 ) / float( nsnap )
        mmod.ptsq    = total( tmmod.ptsq,   2 ) / float( nsnap )
        mmod.abStd   = total( tmmod.abStd,  2 ) / float( nsnap )
        mmod.lnabStd = total( tmmod.lnabStd,2 ) / float( nsnap )
        mmod.abRoss  = total( tmmod.abRoss,   2 ) / float( nsnap )
        mmod.lnabRoss= total( tmmod.lnabRoss, 2 ) / float( nsnap )
        mmod.cm      = total( tmmod.cm,     2 ) / float( nsnap )
     endif else begin
        mmod.ee      = tmmod.ee
        mmod.eesq    = tmmod.eesq
        mmod.ux      = tmmod.ux
        mmod.uxsq    = tmmod.uxsq
        mmod.uy      = tmmod.uy
        mmod.uysq    = tmmod.uysq
        mmod.uz      = tmmod.uz 
        mmod.uzsq    = tmmod.uzsq
        mmod.u       = tmmod.u  
        mmod.usq     = tmmod.usq
        mmod.pg      = tmmod.pg 
        mmod.pgsq    = tmmod.pgsq 
        mmod.lnpg    = tmmod.lnpg  
        mmod.lnpgsq  = tmmod.lnpgsq
        mmod.pe      = tmmod.pe   
        mmod.pesq    = tmmod.pesq 
        mmod.lnpe    = tmmod.lnpe
        mmod.lnpesq  = tmmod.lnpesq
        mmod.pt      = tmmod.pt
        mmod.ptsq    = tmmod.ptsq  
        mmod.abStd   = tmmod.abStd    
        mmod.lnabStd = tmmod.lnabStd
        mmod.abRoss  = tmmod.abRoss    
        mmod.lnabRoss= tmmod.lnabRoss
        mmod.cm      = tmmod.cm
     endelse
  endif

; MHD
  if (do_mhd) then begin
     if ( nsnap gt 1 ) then begin
        mmod.bx      = total( tmmod.bx,     2 ) / float( nsnap )
        mmod.bxsq    = total( tmmod.bxsq,   2 ) / float( nsnap )
        mmod.by      = total( tmmod.by,     2 ) / float( nsnap )
        mmod.bysq    = total( tmmod.bysq,   2 ) / float( nsnap )
        mmod.bz      = total( tmmod.bz,     2 ) / float( nsnap )
        mmod.bzsq    = total( tmmod.bzsq,   2 ) / float( nsnap )
        mmod.b       = total( tmmod.b,      2 ) / float( nsnap )
        mmod.bsq     = total( tmmod.bsq,    2 ) / float( nsnap )
     endif else begin
        mmod.bx      = tmmod.bx
        mmod.bxsq    = tmmod.bxsq 
        mmod.by      = tmmod.by
        mmod.bysq    = tmmod.bysq
        mmod.bz      = tmmod.bz
        mmod.bzsq    = tmmod.bzsq
        mmod.b       = tmmod.b
        mmod.bsq     = tmmod.bsq
     endelse
; rescale magnetic fields
; tmmod
     tmmod[*].bx   = tmmod[*].bx   * bFact
     tmmod[*].bxsq = tmmod[*].bxsq * bFact^2
     tmmod[*].by   = tmmod[*].by   * bFact
     tmmod[*].bysq = tmmod[*].bysq * bFact^2
     tmmod[*].bz   = tmmod[*].bz   * bFact
     tmmod[*].bzsq = tmmod[*].bzsq * bFact^2
     tmmod[*].b    = tmmod[*].b    * bFact
     tmmod[*].bsq  = tmmod[*].bsq  * bFact^2
; mmod
     mmod.bx   = mmod.bx   * bFact
     mmod.bxsq = mmod.bxsq * bFact^2
     mmod.by   = mmod.by   * bFact
     mmod.bysq = mmod.bysq * bFact^2
     mmod.bz   = mmod.bz   * bFact
     mmod.bzsq = mmod.bzsq * bFact^2
     mmod.b    = mmod.b    * bFact
     mmod.bsq  = mmod.bsq  * bFact^2
  endif

  if keyword_set(stop) then stop
  
; reset data cubes to 0.0 to free some memory
  rho       = 0.0
  ux        = 0.0
  uy        = 0.0
  uz        = 0.0
  e         = 0.0
  ee        = 0.0
  tt        = 0.0
  lnpp      = 0.0
  lnnel     = 0.0
  bx        = 0.0
  by        = 0.0
  bz        = 0.0
  
  smallCube = 0.0

  lnr1      = 0.0
  rho1      = 0.0
  tt1       = 0.0
  zz1       = 0.0
  lnpg1     = 0.0
  lnpe1     = 0.0
  lnnel1    = 0.0
  pg1       = 0.0
  pe1       = 0.0
  ux1       = 0.0
  uy1       = 0.0
  uz1       = 0.0
  usq1      = 0.0
  pt1       = 0.0
  ee1       = 0.0
  abStd1    = 0.0
  lnabStd1  = 0.0
  abRoss1   = 0.0
  lnabRoss1 = 0.0
  cmd1      = 0.0
  bx1       = 0.0
  by1       = 0.0
  bz1       = 0.0
  bsq1      = 0.0

; return to original directory
  cd, current_dir
end
