pro comp_mean_cm, filelist, meshfile, lcm, dir=dir, tabfile=tabfile, $
                  nstep=nstep, it=it, nghost=nghost, mmod=mmod, tmmod=tmmod, $
                  do_cgs=do_cgs, do_minimal=do_minimal, do_kapstd=do_kapstd, $
                  do_spline=do_spline, swap_endian=swap_endian, verbose=verbose

  Id = 'comp_mean_cm'

;+
;
; This is a cleaner version of the averaging routine, which now
; includes averages of useful additional quantities such as T^4, lnPg,
; lnPe, etc.
;
; This routine computes all the necessary quantities on the original
; fixed geometrical mesh, then interpolates them to the reference
; optical depth later
; **** (i.e. lookup first, then interpolations.) ****
;
; ****
; This version of the routine works with cgs units and then
; converts to internal units, if so wished
; ****
;
; A normalization factor tempFact is used internally to compute
; averages of T^4 and T^8 to avoid float overflows during the
; calculations (sums). Temperatures are then scaled back at the end of
; the routine.
;
; NOTE: lcm must be in internal units
;
;-

@common_cdata
@common_cmesh
@common_cunits


  default, dir, './'
  default, swap_endian, 0L
  default, nstep,  4L
  default, nghost, 5L
  default, it, 0L
  default, tabfile, 'EOSrhoe.tab'
  default, do_minimal, 0L
  default, do_spline, 0L
  default, do_kapstd, 0L
  default, do_cgs, 0L
  default, verbose, 0L

  default, kboltz, 1.380658e-16


; define a normalization factor for temperatures, to work with
; averages of quantities such as T^4 or T^8 without overflowing

  tempFact = 1.0e4


; units: do everything in CGS first, then convert to internal at the end if necessary

  if KEYWORD_SET(do_cgs) then begin
     ur = 1.
     ul = 1.
     ut = 1.
     uv = 1.
     ua = 1.
     ue = 1.
     up = 1.
  endif else begin
     ur = uur
     ul = uul
     ut = uut
     uv = uuv
     ua = uua
     ue = uue
     up = uup
  endelse


; spline ?
  if KEYWORD_SET(do_spline) then linear=0 else linear=1
  print, '% '+id+' : linear interpolation switch=',linear


  ndep      = size(lcm, /n_elements)
  temp_min  = 1600.

  cd, dir, current=olddir


; determine number of files in filelist. If less than 1, exit.
  nfiles   = size( filelist, /n_elem )
  if (nfiles lt 1) then begin
     print, '% '+id+' : filelist is empty. STOP.'
     stop
  endif


; read first snapshot. If filelist contains one file only, then assume that
; there are multiple snapshots per file, else assume that each file in filelist
; contains one and only one snapshot.
; In either case, the total number of snapshots will be assigned to nsnap (=nt or =nfiles).

  simfile = filelist[0]
  open, a, simfile, swap_endian=swap_endian, nt=nt
  readmesh, meshfile, swap_endian=swap_endian, xm=xm,ym=ym,zm=zm
  if (nfiles eq 1) then begin
     nsnap = nt
     if (it gt nt-1) then begin
        print, '% '+id+' : IT has to be smaller than NT-1: STOP. IT,NT-1=',it,nt-1
        stop
     endif
  endif else begin
     nsnap = nfiles
     if (it gt nfiles-1) then begin
        print, '% '+id+' : IT has to be smaller than NFILES-1: STOP. IT,NFILES-1=',it,nfiles-1
        stop
     endif
  endelse


; consider physical domain only
  nb = ny -nghost -1L
  na = nghost


; read EOS table (with Rosseland Opacity)
  eostab, tabfile

  mx = nx/nstep
  mz = nz/nstep

  smallCube = fltarr(mx,mz,ndep) *0.0
  lnr1   = smallCube
  rho1   = smallCube
  tt1    = smallCube
  zz1    = smallCube
  lnpg1  = smallCube
  lnpe1  = smallCube
  pg1    = smallCube
  pe1    = smallCube
  ux1    = smallCube
  uy1    = smallCube
  uz1    = smallCube
  pt1    = smallCube
  ee1    = smallCube
  abStd1    = smallCube
  lnabStd1  = smallCube
  abRoss1   = smallCube
  lnabRoss1 = smallCube


; define structures
  if (do_minimal) then begin
     mmod  = {name:       'mean_cm_ver0',    $
              file:       '', $
              isnap:      0L, $
              tabfile:    '', $
              lcm:        fltarr(ndep), $
              do_cgs:     0L,           $
              do_minimal: 0L,           $
              r:    fltarr(ndep),  rsq:    fltarr(ndep), $
              lnr:  fltarr(ndep),  lnrsq:  fltarr(ndep), $
              t:    fltarr(ndep),  tsq:    fltarr(ndep), $
              t4:   fltarr(ndep),  t4sq:   fltarr(ndep), $
              z:    fltarr(ndep),  zsq:    fltarr(ndep)  } ;, $
                                ;zs:   fltarr(ndep),  zssq:   fltarr(ndep)  }
  endif else begin
     mmod  = {name:       'mean_cm_ver0',    $
              file:       '', $
              isnap:      0L, $
              tabfile:    '', $
              lcm:        fltarr(ndep), $
;              opac:       '',           $
              do_cgs:     0L,           $
              do_kapstd:  0L,           $
              do_minimal: 0L,           $
              r:    fltarr(ndep),  rsq:    fltarr(ndep), $
              lnr:  fltarr(ndep),  lnrsq:  fltarr(ndep), $
              t:    fltarr(ndep),  tsq:    fltarr(ndep), $
              t4:   fltarr(ndep),  t4sq:   fltarr(ndep), $
              z:    fltarr(ndep),  zsq:    fltarr(ndep), $
;              zs:   fltarr(ndep),  zssq:   fltarr(ndep), $
              pg:   fltarr(ndep),  pgsq:   fltarr(ndep), $
              lnpg: fltarr(ndep),  lnpgsq: fltarr(ndep), $
              pe:   fltarr(ndep),  pesq:   fltarr(ndep), $
              lnpe: fltarr(ndep),  lnpesq: fltarr(ndep), $
              ux:   fltarr(ndep),  uxsq:   fltarr(ndep), $
              uy:   fltarr(ndep),  uysq:   fltarr(ndep), $
              uz:   fltarr(ndep),  uzsq:   fltarr(ndep), $
              u:    fltarr(ndep),  usq:    fltarr(ndep), $
              ee:   fltarr(ndep),  eesq:   fltarr(ndep), $
              pt:   fltarr(ndep),  ptsq:   fltarr(ndep), $
              abStd:    fltarr(ndep),                    $
              lnabStd:  fltarr(ndep),                    $
              abRoss:   fltarr(ndep),                    $
              lnabRoss: fltarr(ndep)     }
  endelse
  tmmod  = replicate(mmod, nsnap)


; loop over snapshots
  for isnap=0L,nsnap-1 do begin

     print, 'isnap=',isnap
     if (nfiles eq 1 ) then begin
        t = isnap
     endif else begin
        t = 0L
        simfile = filelist[isnap]
        open, a, simfile, swap_endian=swap_endian, meshfile=meshfile, nt=nt
        readmesh, meshfile, swap_endian=swap_endian, xm=xm,ym=ym,zm=zm
     endelse


; load data cubes
     print, '% '+id+' : load density and temperature ...'
     rho   = transpose(  a[*,na:nb,*,0+nvar*t], [0,2,1] )
     tt    = transpose(  a[*,na:nb,*,5+nvar*t], [0,2,1] )
     lnr   = alog(rho)
     depth = ym[na:nb]


; column mass density scale
     print, '% '+id+' : compute column mass density'
     cmdens  = depth_int(lnr, depth, vertical=2, /logvar, /spline)
     lcmdens = alog(cmdens)


; if not "do_minimal", lookup
     if (~do_minimal) then begin
        print, '% '+id+' : load velocities and energy...'
        ux    = transpose( (xup(a[1+nvar*t]))[*,na:nb,*], [0,2,1] ) / rho
        uy    = transpose( (zup(a[3+nvar*t]))[*,na:nb,*], [0,2,1] ) / rho
        uz    = transpose( (yup(a[2+nvar*t]))[*,na:nb,*], [0,2,1] ) / rho
        e     = transpose(      a[*,na:nb,*,4+nvar*t]   , [0,2,1] )
        ee    = e / rho

        if (do_kapstd) then begin
           iv=5
           opac = 'standard'
           print, '% '+id+' : lookup standard opacity'
        endif else begin
           iv=1
           opac = 'Rosseland'
           print, '% '+id+' : lookup Rosseland opacity'
        endelse

        lnabStd  = lookup_eos(lnr+alog(uur),alog(ee*uuv*uuv),5,0) ; cgs units !
        lnabRoss = lookup_eos(lnr+alog(uur),alog(ee*uuv*uuv),1,0) ; cgs units !
        lnpg  = lookup_eos(lnr+alog(uur),alog(ee*uuv*uuv),0, 0)   ; cgs
        lnnel = lookup_eos(lnr+alog(uur),alog(ee*uuv*uuv),3, 0, /LINEAR) ; wiggles with cubic interpolation

; compute electron pressure
        kboltzTemp = tt * kboltz
        lnpe  = lnnel + alog( kboltzTemp )
     endif



;convert to cgs units
     lcm1    = lcm + alog(uul*uur)
     rho     = rho * uur
     lnr     = alog( rho )
     depth   = depth * uul
     lcmdens = lcmdens + alog(uul*uur)

     if (~do_minimal) then begin
        ux    = ux  * uuv
        uy    = uy  * uuv
        uz    = uz  * uuv
        e     = e   * uur * uuv * uuv
        ee    = ee  * uuv * uuv
     endif


     print, '% '+id+' : interpolate in column mass density'
     for i=0L,nx-1,nstep do begin
        if (verbose) then print, i
        for k=0,nz-1,nstep do begin
           lcm_tmp_1d = reform( lcmdens[i,k,*] )
           lnr_tmp_1d = reform( lnr  [i,k,*] )
           tt_tmp_1d  = reform( tt   [i,k,*] )
           lnr1[i/nstep, k/nstep, *]  = interpol( lnr_tmp_1d, lcm_tmp_1d, lcm1, spline=do_spline )
           tt1 [i/nstep, k/nstep, *]  = interpol( tt_tmp_1d,  lcm_tmp_1d, lcm1, spline=do_spline )
           zz1 [i/nstep, k/nstep, *]  = interpol( depth,      lcm_tmp_1d, lcm1, spline=do_spline )
        endfor
     endfor


; normalize temperature for intermediate calculations;
; rescale later before saving;
; exponentiate ln density;
     tt1             = tt1 / tempFact
     rho1            = exp( lnr1 )


; add snapshot file name and table file name info:
     tmmod[isnap].file      = simfile
     tmmod[isnap].isnap     = isnap
     tmmod[isnap].tabfile   = tabfile


; compute average depth scales
     zmean1   = haver( zz1,   /zvert)
     z2mean1  = haver( zz1^2, /zvert)



; fill data structure
     tmmod[isnap].lcm        = lcm1 -alog(ul*ur)
     tmmod[isnap].do_cgs     = do_cgs
     tmmod[isnap].do_minimal = do_minimal
     tmmod[isnap].r          = haver( rho1,  /zvert)   /ur
     tmmod[isnap].rsq        = haver( rho1^2,/zvert)   /ur/ur
     tmmod[isnap].lnr        = haver( lnr1-alog(ur),    /zvert)
     tmmod[isnap].lnrsq      = haver((lnr1-alog(ur)^2), /zvert)
     tmmod[isnap].t          = haver( tt1,   /zvert)
     tmmod[isnap].tsq        = haver( tt1^2, /zvert)
     tmmod[isnap].t4         = haver( tt1^4, /zvert)
     tmmod[isnap].t4sq       = haver( tt1^8, /zvert)
     tmmod[isnap].z          = zmean1                  /ul
     tmmod[isnap].zsq        = z2mean1                 /ul/ul

;     tmmod[isnap].zs         = zsmean1                 /ul
;     tmmod[isnap].zssq       = zs2mean1                /ul/ul



     if (~do_minimal) then begin

        for i=0L,nx-1,nstep do begin
           if (verbose) then print, i
           for k=0,nz-1,nstep do begin
              lcm_tmp_1d      = reform( lcmdens [i,k,*] )
              ux_tmp_1d       = reform( ux      [i,k,*] )
              uy_tmp_1d       = reform( uy      [i,k,*] )
              uz_tmp_1d       = reform( uz      [i,k,*] )
              ee_tmp_1d       = reform( ee      [i,k,*] )
              lnpg_tmp_1d     = reform( lnpg    [i,k,*] )
              lnpe_tmp_1d     = reform( lnpe    [i,k,*] )
              lnabStd_tmp_1d  = reform( lnabStd [i,k,*] )
              lnabRoss_tmp_1d = reform( lnabRoss[i,k,*] )
              ux1      [ i/nstep, k/nstep, *]  = interpol( ux_tmp_1d, lcm_tmp_1d, lcm1, spline=do_spline )
              uy1      [ i/nstep, k/nstep, *]  = interpol( uy_tmp_1d, lcm_tmp_1d, lcm1, spline=do_spline )
              uz1      [ i/nstep, k/nstep, *]  = interpol( uz_tmp_1d, lcm_tmp_1d, lcm1, spline=do_spline )
              ee1      [ i/nstep, k/nstep, *]  = interpol( ee_tmp_1d, lcm_tmp_1d, lcm1, spline=do_spline )
              lnpg1    [ i/nstep, k/nstep, *]  = interpol( lnpg_tmp_1d, lcm_tmp_1d, lcm1, spline=do_spline )
              lnpe1    [ i/nstep, k/nstep, *]  = interpol( lnpe_tmp_1d, lcm_tmp_1d, lcm1, spline=do_spline )
              lnabStd1 [ i/nstep, k/nstep, *]  = interpol( lnabStd_tmp_1d,  lcm_tmp_1d, lcm1, spline=do_spline )
              lnabRoss1[ i/nstep, k/nstep, *]  = interpol( lnabRoss_tmp_1d, lcm_tmp_1d, lcm1, spline=do_spline )
           endfor               ; k
        endfor                  ; i

;;        kboltzTempFactor = tt1 * tempFact * kboltz
;;        lnpe1 = lnnel1 + alog( kboltzTempFactor )
        pg1   = exp( lnpg1 )
        pe1   = exp( lnpe1 )
        pt1     = rho1 * uz1^2
        abStd1  = exp( lnabStd1 )
        abRoss1 = exp( lnabRoss1 )

; velocity squared
        usq1  = ux1^2 + uy1^2 + uz1^2
        tmmod[isnap].ee      = haver( ee1,   /zvert)   / ue
        tmmod[isnap].eesq    = haver( ee1^2, /zvert)   / ue / ue
        tmmod[isnap].ux      = haver( ux1,   /zvert)   / uv
        tmmod[isnap].uxsq    = haver( ux1^2, /zvert)   / uv / uv
        tmmod[isnap].uy      = haver( uy1,   /zvert)   / uv
        tmmod[isnap].uysq    = haver( uy1^2, /zvert)   / uv / uv
        tmmod[isnap].uz      = haver( uz1,   /zvert)   / uv
        tmmod[isnap].uzsq    = haver( uz1^2, /zvert)   / uv / uv
        tmmod[isnap].u       = haver( sqrt(usq1),/zvert)    / uv
        tmmod[isnap].usq     = haver( usq1,  /zvert)   / uv / uv
        tmmod[isnap].pg      = haver( pg1,   /zvert)   / up
        tmmod[isnap].pgsq    = haver( pg1^2, /zvert)   / up / up
        tmmod[isnap].lnpg    = haver( lnpg1-alog(up),    /zvert)
        tmmod[isnap].lnpgsq  = haver((lnpg1-alog(up))^2, /zvert)
        tmmod[isnap].pe      = haver( pe1,   /zvert)   / up
        tmmod[isnap].pesq    = haver( pe1^2, /zvert)   / up / up
        tmmod[isnap].lnpe    = haver( lnpe1-alog(up),    /zvert)
        tmmod[isnap].lnpesq  = haver((lnpe1-alog(up))^2, /zvert)
        tmmod[isnap].pt      = haver( pt1,   /zvert)   / up
        tmmod[isnap].ptsq    = haver( pt1^2, /zvert)   / up / up
        tmmod[isnap].abStd   = haver( abStd1,   /zvert)   / ua
        tmmod[isnap].lnabStd = haver( lnabStd1-alog(ua),   /zvert)
        tmmod[isnap].abRoss   = haver( abRoss1,   /zvert)   / ua
        tmmod[isnap].lnabRoss = haver( lnabRoss1-alog(ua),   /zvert)
     endif

  endfor                        ; isnap


; add snapshot file name and table file name info:
  mmod.file      = filelist[0]
  mmod.isnap     = nsnap
  mmod.tabfile   = tabfile

; compute averages over snapshots
  mmod.lcm        = lcm1-alog(ul*ur)
  mmod.do_cgs     = do_cgs
  mmod.do_minimal = do_minimal


  if (nsnap gt 1) then begin
     mmod.r       = total( tmmod.r,      2 ) / float( nsnap )
     mmod.rsq     = total( tmmod.rsq,    2 ) / float( nsnap )
     mmod.lnr     = total( tmmod.lnr,    2 ) / float( nsnap )
     mmod.lnrsq   = total( tmmod.lnrsq,  2 ) / float( nsnap )
     mmod.t       = total( tmmod.t,      2 ) / float( nsnap )
     mmod.tsq     = total( tmmod.tsq,    2 ) / float( nsnap )
     mmod.t4      = total( tmmod.t4,     2 ) / float( nsnap )
     mmod.t4sq    = total( tmmod.t4sq,   2 ) / float( nsnap )
     mmod.z       = total( tmmod.z,      2 ) / float( nsnap )
     mmod.zsq     = total( tmmod.zsq,    2 ) / float( nsnap )
  endif else begin
     mmod.r       = tmmod.r
     mmod.rsq     = tmmod.rsq
     mmod.lnr     = tmmod.lnr
     mmod.lnrsq   = tmmod.lnrsq
     mmod.t       = tmmod.t
     mmod.tsq     = tmmod.tsq
     mmod.t4      = tmmod.t4
     mmod.t4sq    = tmmod.t4sq
     mmod.z       = tmmod.z
     mmod.zsq     = tmmod.zsq
  endelse


; rescale temperatures! Need to do that for both tmmod and mmod

  tmmod[*].t    = tmmod[*].t    * tempFact
  tmmod[*].tsq  = tmmod[*].tsq  * tempFact^2
  tmmod[*].t4   = tmmod[*].t4   * tempFact^4
  tmmod[*].t4sq = tmmod[*].t4sq * tempFact^8

  mmod.t        = mmod.t        * tempFact
  mmod.tsq      = mmod.tsq      * tempFact^2
  mmod.t4       = mmod.t4       * tempFact^4
  mmod.t4sq     = mmod.t4sq     * tempFact^8


  if (~do_minimal) then begin
     if ( nsnap gt 1 ) then begin
        mmod.ee      = total( tmmod.ee,     2 ) / float( nsnap )
        mmod.eesq    = total( tmmod.eesq,   2 ) / float( nsnap )
        mmod.ux      = total( tmmod.ux,     2 ) / float( nsnap )
        mmod.uxsq    = total( tmmod.uxsq,   2 ) / float( nsnap )
        mmod.uy      = total( tmmod.uy,     2 ) / float( nsnap )
        mmod.uysq    = total( tmmod.uysq,   2 ) / float( nsnap )
        mmod.uz      = total( tmmod.uz,     2 ) / float( nsnap )
        mmod.uzsq    = total( tmmod.uzsq,   2 ) / float( nsnap )
        mmod.u       = total( tmmod.u,      2 ) / float( nsnap )
        mmod.usq     = total( tmmod.usq,    2 ) / float( nsnap )
        mmod.pg      = total( tmmod.pg,     2 ) / float( nsnap )
        mmod.pgsq    = total( tmmod.pgsq,   2 ) / float( nsnap )
        mmod.lnpg    = total( tmmod.lnpg,   2 ) / float( nsnap )
        mmod.lnpgsq  = total( tmmod.lnpgsq, 2 ) / float( nsnap )
        mmod.pe      = total( tmmod.pe,     2 ) / float( nsnap )
        mmod.pesq    = total( tmmod.pesq,   2 ) / float( nsnap )
        mmod.lnpe    = total( tmmod.lnpe,   2 ) / float( nsnap )
        mmod.lnpesq  = total( tmmod.lnpesq, 2 ) / float( nsnap )
        mmod.pt      = total( tmmod.pt,     2 ) / float( nsnap )
        mmod.ptsq    = total( tmmod.ptsq,   2 ) / float( nsnap )
        mmod.abStd   = total( tmmod.abStd,   2 ) / float( nsnap )
        mmod.lnabStd = total( tmmod.lnabStd, 2 ) / float( nsnap )
        mmod.abRoss  = total( tmmod.abRoss,  2 ) / float( nsnap )
        mmod.lnabRoss= total( tmmod.lnabRoss,2 ) / float( nsnap )
     endif else begin
        mmod.ee      = tmmod.ee
        mmod.eesq    = tmmod.eesq
        mmod.ux      = tmmod.ux
        mmod.uxsq    = tmmod.uxsq
        mmod.uy      = tmmod.uy
        mmod.uysq    = tmmod.uysq
        mmod.uz      = tmmod.uz
        mmod.uzsq    = tmmod.uzsq
        mmod.u       = tmmod.u
        mmod.usq     = tmmod.usq
        mmod.pg      = tmmod.pg
        mmod.pgsq    = tmmod.pgsq
        mmod.lnpg    = tmmod.lnpg
        mmod.lnpgsq  = tmmod.lnpgsq
        mmod.pe      = tmmod.pe
        mmod.pesq    = tmmod.pesq
        mmod.lnpe    = tmmod.lnpe
        mmod.lnpesq  = tmmod.lnpesq
        mmod.pt      = tmmod.pt
        mmod.ptsq    = tmmod.ptsq
        mmod.abStd   = tmmod.abStd
        mmod.lnabStd = tmmod.lnabStd
        mmod.abRoss  = tmmod.abRoss
        mmod.lnabRoss= tmmod.lnabRoss
     endelse
  endif

;stop

  cd, olddir


; set data cubes to 0.0 to free some memory
  rho      = 0.0
  ux       = 0.0
  uy       = 0.0
  uz       = 0.0
  e        = 0.0
  ee       = 0.0
  tt       = 0.0
  lnpp     = 0.0
  lnnel    = 0.0
  lnabStd  = 0.0
  lnabRoss = 0.0
  smallCube= 0.0

  lnr1   = 0.0
  rho1   = 0.0
  tt1    = 0.0
  zz1    = 0.0
  lnpg1  = 0.0
  lnpe1  = 0.0
  pg1    = 0.0
  pe1    = 0.0
  ux1    = 0.0
  uy1    = 0.0
  uz1    = 0.0
  pt1    = 0.0
  ee1    = 0.0
  ab1    = 0.0
  lnab1  = 0.0

end
