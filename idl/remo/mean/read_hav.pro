pro read_hav, havflist, hstruc, dir=dir

; +
; NAME:    READ_HAV
; PURPOSE: read .hav files and store information into data structure
; INPUT:   havflist = list of .hav files
; OUTPUT:  hstruc = data structure (or array of data structures)
;                   containing information from .hav files 
;
;
; DATA STRUCTURE OF .hav FILES (ASCII FORMAT):
;
;   &haverages
;   ...
;      parameter = value,
;   ...
;   /
;   k   var1 var2 var3 var4 ... varN
;   1   ...  ...  ...  ...
;   2   ...  ...  ...  ...
;   ...
;   ...
;   k   varN+1 varN+2 ...
;   1   ...  ...  ...
;   ...
;   ...
;
; TESTS:
;  regex='[\+\-\.]+[0-9\+\-\.]([eE][\+\-]*[0-9]+)+'
;  test='.e04' & print,stregex(test,regex,/extract)
;
;  regex='^[-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?$'
;  test='.e04' & print,stregex(test,regex,/extract)
;
;-

  IdStr = 'READ_HAV'

  default, dir,  '.'


; CD to work directory
  cd, dir, current=current_dir

; initialise hstruc
  hstruc = []

; number of elements in havflist
  nhav = n_elements(havflist)

; loop over files in havflist
  for ihav=0L,nhav-1 do begin

; open file
     havfile = havflist[ihav]
     openr, lhav, /get_lun, havfile

     
; extract NAMELIST LABEL

; skim through possible empty/invalid rows at the beginning
     repeat begin
        dumStr = ''
        readf, lhav, dumStr
        pos_amp = strpos( dumStr, '&' )
        is_amp  = ( pos_amp ge 0 ) 
     endrep until is_amp 
     lenStr   = strlen( dumStr )
     nmlLabel = strmid( dumStr, pos_amp+1, lenStr-1)


; Extract PARAMETER DEFINITIONS and store them into plist string (plStr)
     plStr = ''

; Read and concatenate strings until a line with a slash charachter is found 
     dumStr = ''
     readf, lhav, dumStr

     isnt_slash = ~strmatch( dumStr, '*/*' )

     while ( isnt_slash ) do begin
        plStr = plStr + dumStr
        readf, lhav, dumStr
        isnt_slash = ~strmatch( dumStr, '*/*' )
     endwhile

; Compress plist and split it into individual items
     plStr    = strcompress(plStr, /remove_all)
     plItems  = strsplit(plStr, ',', /regex, /extract, /preserve_null)
     nplItems = size( plItems, /n_elements )


; Check if items are (signed) long integers 
     regex_long = '^[+-]?[0-9]*[lL]?$'

     for i=0L,nplItems-1L do begin
        
        dumStr    = strsplit( plItems[i], '=', /regex, /extract, /preserve_null)
        paramStr  = dumStr[0]       ; parameter string
        paramStr  = strlowcase(paramStr) ; make lower case
        valueStr  = dumStr[1]            ; value string
        is_long   = stregex(valueStr, regex_long, /boolean)
        
; if long, remove L characters, then add one (and only one) at the end
        if (is_long) then begin
           valueStr = strjoin(strsplit(valueStr, '[lL]', /regex, /extract, /preserve_null), '')
           valueStr = valueStr + 'L'
        endif

; Check if paramStr is equal to 'ndep' (case insensitive)
; Execute command ndep = <value>

        is_par_ndep = stregex(paramStr,'ndep',/fold_case,/boolean)

        if (is_par_ndep) then begin
           execStr = paramStr+'='+valueStr
;;      print, execStr
           result_exec = execute( execStr )
; *** NOTE: NDEP is now available as a VARIABLE in the routine ! ***      
        endif

        plItems[i] = paramStr+':'+valueStr

     endfor


; Read array section

; Read array data in chunks until EOF
     is_first_chunk = 1
     is_vl_valid = 0

     while ( ~EOF( lhav ) ) do begin
; Read variable tags
        dumStr   = ''
        readf, lhav, dumStr
        varTag  = strsplit( dumStr, ' ', /regex, /extract )
        nvarTag = size( varTag, /n_elements )

        is_nvarTag_pos   = (nvarTag ge 1)
        is_nvarTag_valid = (nvarTag ge 2)

        if ( is_nvarTag_pos ) then begin

; Verify that first tag is 'k'

           is_first_k = stregex( varTag[0], '^k$', /boolean, /fold_case)
           if ( ~is_first_k ) then STOP

; Define arrays: append '_arr' to array names for avoiding conflicts
; First array: long integers
; Subsequent arrays: floating point values

           execStr = varTag[0]+'_arr = LONARR( NDEP )'
;      print, execStr
           result_exec = execute( varTag[0]+'_arr = LONARR( NDEP )' )

           if ( is_nvarTag_valid ) then begin

              for i=1L,nvarTag-1 do begin 
                 execStr = varTag[i]+'_arr = FLTARR( NDEP )'
;            print, execStr
                 result_exec = execute( execStr ) 
              endfor

; create entries for data structure definition
              vlist =  strarr( nvarTag-1 )
              for i=1L,nvarTag-1 do begin 
                 vlist[i-1]=varTag[i]+':'+varTag[i]+'_arr'
              endfor

              if ( is_first_chunk ) then begin
                 vlItems = vlist
                 is_first_chunk = 0
                 is_vl_valid    = 1
              endif else begin
                 vlItems = [vlItems, vlist]
              endelse

; fill in arrays, line by line

              for idep=0L,ndep-1L do begin
                 dumStr   = ''
                 readf, lhav, dumStr
                 valueStr  = strsplit( dumStr, ' ', /regex, /extract )
                 idepStr = strcompress(string(idep),/remove_all)

                 for i=1L,nvarTag-1 do begin 
                    execStr = varTag[i]+'_arr['+idepStr+']='+valueStr[i] 
;               print, execStr
                    result_exec = execute( execStr )
                 endfor
              endfor

           endif                ;is_nvartag_valid
        endif                   ;is_nvartag_pos
     endwhile                   ;eof


; number of floating point arrays

     nvlItems = size( vlItems, /n_elements )


; DEFINE DATA STRUCTURE

; prepare partial strings with concatentated plist and vlist

     headStr  = 'file:'+"'"+havfile+"'"+', name:'+"'"+nmlLabel+"'"

     plistStr = ''
     vlistStr = ''

     for i=0L,nplItems-2 do begin
        plistStr = plistStr + plItems[i] +', '
     endfor
     plistStr = plistStr + plItems[nplItems-1]

     for i=0L,nvlItems-2 do begin
        vlistStr = vlistStr + vlItems[i] +', '
     endfor
     vlistStr = vlistStr + vlItems[nvlItems-1]

;;print, plistStr
;;print, vlistStr

; execute command with definition of data structure

     execStr  = 'struct = { HAV, '+headStr+', '+plistStr+', '+vlistStr+' }'
     res_exec = execute( execStr )

; close current file
     free_lun, lhav

; store struct as part of hstruc array
     hstruc = [hstruc, struct]
  endfor                        ; loop over ihav

  cd, current_dir
end
