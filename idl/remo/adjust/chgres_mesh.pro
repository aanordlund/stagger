PRO CHGRES_MESH, xmi, ymi, zmi, nxo, nyo, nzo, xmo, ymo, zmo, $
                 dxmo=dxmo, dxmdno=dxmdno, xmdno=xmdno, dxidxupo=dxidxupo, dxidxdno=dxidxdno, $
                 dymo=dymo, dymdno=dymdno, ymdno=ymdno, dyidyupo=dyidyupo, dyidydno=dyidydno, $
                 dzmo=dzmo, dzmdno=dzmdno, zmdno=zmdno, dzidzupo=dzidzupo, dzidzdno=dzidzdno, $
                 sx=sx, sy=sy, sz=sz, $
                 do_ghost=do_ghost, nghost=nghost, $
                 spline=spline, double=double

;+
; NAME: chgres_mesh
;
; PURPOSE: change resolution of stagger mesh
;
; NOTES: Coded by R. Collet
;-

  IdStr = 'CHGRES_MESH'


; define do_ghost and nghost if necessary
  if ~keyword_set( do_ghost ) then begin
; then make sure do_ghost is set to zero in that case
     do_ghost=0L
  endif

  if (do_ghost gt 0) then begin
     if ~keyword_set( nghost ) then begin
        print, '% '+IdStr+': '+'DO_GHOST > 0: set nghost=5 (default)'
        nghost = 5L
     endif
  endif else begin
     if keyword_set( nghost ) then begin
        print, '% '+IdStr+': '+'DO_GHOST = 0: set nghost=0'
        nghost = 0L
     endif
  endelse

; copy nghost to ng for convenience
  ng = nghost


; determine size of input mesh arrays
  nx1 = long( n_elements( xmi ) )
  ny1 = long( n_elements( ymi ) )
  nz1 = long( n_elements( zmi ) )

; copy input mesh arrays to double-precision work arrays
  xm1 = dblarr( nx1 ) 
  ym1 = dblarr( ny1 )
  zm1 = dblarr( nz1 )

  xm1 = double( xmi )
  ym1 = double( ymi )
  zm1 = double( zmi )

; normalized index arrays for physical layers (i.e. excluding ghost
; zones in ym)
  xind1  = dindgen( nx1 )        / double( nx1-1 )
  yind1  = dindgen( ny1-2*ng )   / double( ny1-1-2*ng )
  zind1  = dindgen( nz1 )        / double( nz1-1 )
  
  xind1p = dindgen( nx1+1 )      / double( nx1 )
  yind1p = dindgen( ny1+1-2*ng ) / double( ny1-2*ng )
  zind1p = dindgen( nz1+1 )      / double( nz1 )

; output mesh, double precision working arrays
  nx2  = long( nxo )
  ny2  = long( nyo )
  nz2  = long( nzo )

  xm2  = dblarr( nx2 )
  ym2  = dblarr( ny2 )
  zm2  = dblarr( nz2 )

; normalized index arrays for physical layers (i.e. excluding ghost
; zones in ym)
  xind2  = dindgen( nx2 )        / double( nx2-1 )
  yind2  = dindgen( ny2-2*ng )   / double( ny2-1-2*ng )
  zind2  = dindgen( nz2 )        / double( nz2-1 )
  
  
; extend x and z arrays one grid point beyond physical boundary
; (assume periodic boundaries in x and z)
; note: do not extend y array
;;  xm1p   = xm1[nx1-1] + (xm1[0] - xmdn1[0]) + (xm1[nx1-1] - xmdn1[nx1-1])
;;  zm1p   = zm1[nz1-1] + (zm1[0] - zmdn1[0]) + (zm1[nz1-1] - zmdn1[nz1-1])
  xm1p   = xm1[nx1-1] + (xm1[1] - xm1[0]) 
  zm1p   = zm1[nz1-1] + (zm1[1] - zm1[0]) 
  xm1tmp = [xm1, xm1p] 
  zm1tmp = [zm1, zm1p] 

; extended index arrays for x and z
  xind2p = dindgen( nx2+1 )      / double( nx2 )
  zind2p = dindgen( nz2+1 )      / double( nz2 )


; interpolate mesh arrays to new dimensions
; note: only the physical layers are considered for the interpolation
; note: use extended arrays for x and z

; extended arrays:
  xm2tmp           = interpol( xm1tmp          , xind1p, xind2p, spline=spline )
  ym2[ng:ny2-ng-1] = interpol( ym1[ng:ny1-ng-1], yind1,  yind2,  spline=spline )
  zm2tmp           = interpol( zm1tmp          , zind1p, zind2p, spline=spline )

; extract xm2 and zm2 arrays from extended x and z arrays 
  xm2 = xm2tmp[0:nx2-1]
  zm2 = zm2tmp[0:nz2-1]

; ghost zones: continue the mesh scale beyond the physical zone to
; include ghost zone layers at both ends
  if (ng gt 0L) then begin
; lower boundary ("top")
     for j=ng,0,-1 do begin
        ym2[ng-j] = float(j+1) * ym2[ng] - float(j)*ym2[ng+1]
     endfor
; upper boundary ("bottom")
     for j=ny2-ng,ny2-1,+1  do begin
        ym2[j] = 3.*ym2[j-1] -3.*ym2[j-2] + ym2[j-3]
     endfor
  endif


  make_stagger_mesh, xm2, ym2, zm2, $
                     dxmo, dxmdno, xmdno, dxidxupo, dxidxdno, $
                     dymo, dymdno, ymdno, dyidyupo, dyidydno, $
                     dzmo, dzmdno, zmdno, dzidzupo, dzidzdno, $
                     sx=sx, $
                     sy=sy, $
                     sz=sz, $
                     double=double


; convert main mesh to float
  if keyword_set( double ) then begin
     xmo     = xm2
     ymo     = ym2
     zmo     = zm2
  endif else begin
     xmo     = float( xm2 )
     ymo     = float( ym2 )
     zmo     = float( zm2 )
  endelse

END
