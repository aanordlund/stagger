PRO regrid_stagger, finlist, finmesh, fout, nxout, nyout, nzout, $
                    FINDIR=findir, FOUTDIR=foutdir, $
                    SWAP_IN=swap_in, SWAP_OUT=swap_out, $
                    IOFFSET=ioffset, NGHOST=nghost, $
                    SMOOTHBOX=smoothbox, DEBUG=debug

;+
; NAME: regrid_stagger
;
; PURPOSE: change numerical resolution of a set of simulation
; snapshots
;
; SYNTAX: regrid_stagger, finlist, finmesh, fout, nxout, nyout, nzout
;
; ARGUMENTS/OPTIONS:
; finlist   : array of strings with names of input snapshot files
; finmesh   : input mesh file
; fout      : output filename template (used to generate individual
;             output filenames)
; nx[y,z]out: new dimensions
; findir    : directory where input files are stored
; foutdir   : directory where output files are stored
; swap_in,swap_out : swap endianness of input/output files ?
; ioffset   : offset to output snapshot number
; nghost    : number of ghost zones
; -  

  IdStr = 'REGRID_STAGGER'

@common_cdata
@common_cmesh

  default, findir,  './'
  default, foutdir, './'
  default, swap_in,  0
  default, swap_out, 0
  default, nghost,  5L
  default, smoothbox, 1L        ;smooth filter width, default: no smoothing
  default, debug, 0
  default, ioffset, 0

; check nghost and reset do_ghost if necessary
  if (nghost lt 0) then begin
     print, '% '+IdStr+': '+'stop: nghost < 0'
     stop
  endif
  if (nghost gt 0) then do_ghost=1 else do_ghost=0


; set names of output mesh and dx files
  lastSlash  = strpos(finmesh,'/',/reverse_search)
  finmeshTmp = strmid(finmesh,lastSlash+1)
  finroot    = fileroot(finmeshTmp)
  if (fout eq '') then begin
     lastSlash  = strpos(finmesh,'/',/reverse_search)
     finmeshTmp = strmid(finmesh,lastSlash+1)
     foutroot  = finroot
     foutmsh   = foutroot+'.msh'
     foutdx    = foutroot+'.dx'
  endif else begin
     foutroot  = fileroot(fout)
     foutmsh   = foutroot+'.msh'
     foutdx    = foutroot+'.dx'
  endelse


; determine number of files in filelist. If less than 1, exit. 
  nfiles   = size( finlist, /n_elem )
  if (nfiles lt 1) then begin
     print, '% '+IdStr+' : input filelist is empty. STOP.'
     stop
  endif 


; read first snapshot. If filelist contains one file only, then assume that
; there are multiple snapshots per file, else assume that each file in filelist
; contains one and only one snapshot.
; In either case, the total number of snapshots will be assigned to nsnap (=nt or =nfiles).

;  cd, findir, current=current

  simfile = finlist[0]
  findx   = finroot+'.dx'
  openr, lundx1, /get, findir+'/'+findx
  nx = 0L
  ny = 0L
  nz = 0L
  dx = 0.0
  dy = 0.0
  dz = 0.0
  gamma = 0.0
  xvert = 0L
  nvar  = 0L
  zero  = 0L
  readf, lundx1, nx, ny, nz
  readf, lundx1, dx, dy, dz
  readf, lundx1, gamma, xvert
  readf, lundx1, nvar
  readf, lundx1, zero
  free_lun, lundx1

;  open, b, findir+'/'+simfile, swap_endian=swap_in, nt=nt
;  readmesh, findir+'/'+finmesh, swap_endian=swap_in, xm=xm, ym=ym, zm=zm, nx=nx, ny=ny, nz=nz
  openr, lun1, /get_lun, findir+'/'+simfile, swap_endian=swap_in
  fs = fstat(lun1)
  nt = fs.size/(4L*nvar)/(nx*ny*nz) > 1L
  readmesh, findir+'/'+finmesh, swap_endian=swap_in, xm=xm, ym=ym, zm=zm, nx=nx, ny=ny, nz=nz
  a = assoc(lun1,fltarr(nx,ny,nz))


; determine number of snapshots
  if (nfiles eq 1) then begin
     nsnap = nt
  endif else begin
     nsnap = nfiles
  endelse


; set file extension for output file
  if (nsnap eq 1) then begin
     fext = '.scr'
  end else begin
     fext ='.dat'
  endelse 


; generate output mesh
;
; copy input mesh arrays (main and down-staggerd) to work arrays
  xm1   = xm
  ym1   = ym
  zm1   = zm
  xmdn1 = xmdn
  ymdn1 = ymdn
  zmdn1 = zmdn

  nx1   = n_elements( xm1 ) 
  ny1   = n_elements( ym1 ) 
  nz1   = n_elements( zm1 )

  sx1   = xm1[nx1-1] - xm1[0] + (xm1[0] - xmdn1[0]) + (xm1[nx1-1] - xmdn1[nx1-1])
  sy1   = ym1[ny1-1] - ym1[0] + (ym1[0] - ymdn1[0]) + (ym1[ny1-1] - ymdn1[ny1-1])
  sz1   = zm1[nz1-1] - zm1[0] + (zm1[0] - zmdn1[0]) + (zm1[nz1-1] - zmdn1[nz1-1])

  dx1   = sx1 / float( nx1 )
  dx1   = sy1 / float( ny1 )
  dz1   = sz1 / float( nz1 )

; copy dimensions output mesh to work variables
  nx2  = nxout
  ny2  = nyout
  nz2  = nzout


; depth index in physical part of the snapshots, input and output
  na1 = nghost
  nb1 = ny1 - nghost -1L
  na2 = nghost
  nb2 = ny2 - nghost -1L

; change resolution of mesh
  chgres_mesh, xm1, ym1, zm1, nx2, ny2, nz2, xm2, ym2, zm2, $
               dxmo=dxm2, dxmdno=dxmdn2, xmdno=xmdn2, dxidxupo=dxidxup2, dxidxdno=dxidxdn2, $
               dymo=dym2, dymdno=dymdn2, ymdno=ymdn2, dyidyupo=dyidyup2, dyidydno=dyidydn2, $
               dzmo=dzm2, dzmdno=dzmdn2, zmdno=zmdn2, dzidzupo=dzidzup2, dzidzdno=dzidzdn2, $
               sx=sx2, sy=sy2, sz=sz2, $
               do_ghost=do_ghost, nghost=nghost, $
               /spline

; write mesh to file
  mshfile2 = foutdir+'/'+foutmsh
  dxfile2  = foutdir+'/'+foutdx
  print, '% '+IdStr+' : write output mesh   : ',mshfile2
  print, '% '+IdStr+' : write output dx file: ',dxfile2

  write_mesh, mshfile2, xm2, ym2, zm2, dxfile=dxfile2, nvar=nvar, swap_endian=swap_out, $
              nghost=nghost, do_ghost=do_ghost



; auxiliary arrays for interpolation

; determine sizes of the right buffers, volume-centered mesh (xm and zm arrays)
  delXR   = xm2[nx2-1] - xm1[nx1-1]
  nbufXR  = abs( max([ceil(delXR/dx1 ), 0]) ) >1

  delZR   = zm2[nz2-1] - zm1[nz1-1]
  nbufZR  = abs( max([ceil(delZR/dz1 ), 0]) ) >1



; determine sizes of the right and left buffers, staggered mesh (xmdn and zmdn arrays)
  delXdnR   = xmdn2[nx2-1] - xmdn1[nx1-1]
  delXdnL   = xmdn2[0] - xmdn1[0]
  nbufXdnR  = abs( max([ceil(  delXdnR/dx1 ), 0]) ) >1
  nbufXdnL  = abs( min([floor( delXdnL/dx1 ), 0]) ) >1

  delZdnR   = zmdn2[nz2-1] - zmdn1[nz1-1]
  delZdnL   = zmdn2[0] - zmdn1[0]
  nbufZdnR  = abs( max([ceil(  delZdnR/dz1 ), 0]) ) >1
  nbufZdnL  = abs( min([floor( delZdnL/dz1 ), 0]) ) >1



; cubes for interpolation
  nXPlus1   = nx1 + nbufXR
  nZPlus1   = nz1 + nbufZR

  nXdnPlus1 = nx1 + nbufXdnL + nbufXdnR
  nZdnPlus1 = nz1 + nbufZdnL + nbufZdnR

  cube1s = fltarr(nXPlus1,   ny1, nZPlus1   ) ;cube for scalars, py, and by
  cube1x = fltarr(nXdnPlus1, ny1, nZPlus1   ) ;cube for px, bx
  cube1z = fltarr(nXPlus1,   ny1, nZdnPlus1 ) ;cube for pz, bz

  cube1  = fltarr(nx1, ny1, nz1)
  cube2  = fltarr(nx2, ny2, nz2)


; fractional indices, horizontal axes,  volume-centered mesh
  xmBuf1  = fltarr( nXPlus1 )
  xmBuf1[  0 : nx1-1    ]  = xm1
  xmBuf1[nx1 : nXPlus1-1]  = xm1[nx1-1] + ( findgen(nbufXR) +1 ) * dx1

  xiBuf1 = findgen( nXPlus1 ) 
  xiBuf2 =  interp( xmBuf1, xm2, xiBuf1 )

  zmBuf1  = fltarr( nZPlus1 )
  zmBuf1[  0 : nz1-1    ]  = zm1
  zmBuf1[nz1 : nZPlus1-1]  = zm1[nz1-1] + ( findgen(nbufZR) +1 ) * dz1

  ziBuf1 = findgen( nZPlus1 ) 
  ziBuf2 =  interp( zmBuf1, zm2, ziBuf1 )


; fractional indices, horizontal axes, staggered mesh
  xmdnBuf1  = fltarr( nXdnPlus1 )
  xmdnBuf1[  nbufXdnL : nbufXdnL +nx1-1]  = xmdn1
  xmdnBuf1[0:nbufXdnL-1]                  = xmdn1[0]     - (reverse(findgen(nbufXdnL))+1) * dx1
  xmdnBuf1[  nbufXdnL +nx1 : nXdnPlus1-1] = xmdn1[nx1-1] + (        findgen(nbufXdnR) +1) * dx1

  xidnBuf1 = findgen( nXdnPlus1 ) 
  xidnBuf2 =  interp( xmdnBuf1, xmdn2, xidnBuf1 )

  zmdnBuf1  = fltarr( nZdnPlus1 )
  zmdnBuf1[  nbufZdnL : nbufZdnL +nz1-1]  = zmdn1
  zmdnBuf1[0:nbufZdnL-1]                  = zmdn1[0]     - (reverse(findgen(nbufZdnL))+1) * dz1
  zmdnBuf1[  nbufZdnL +nz1 : nZdnPlus1-1] = zmdn1[nz1-1] + (        findgen(nbufZdnR) +1) * dz1

  zidnBuf1 = findgen( nZdnPlus1 ) 
  zidnBuf2 =  interp( zmdnBuf1, zmdn2, zidnBuf1 )



; fractional index scale, vertical
  if (nghost eq 0) then begin
     yitmp1   = findgen(ny1)
     yidntmp1 = findgen(ny1)
     yitmp2   = ( interp(  ym1, ym2,   yitmp1  , extra=2 ) <  (ny1-1) ) > 0
     yidntmp2 = ( interp(ymdn1, ymdn2, yidntmp1, extra=2 ) <  (ny1-1) ) > 0
  endif else begin
     yitmp1   = na1+findgen(nb1-na1+1)
     yidntmp1 = na1+findgen(nb1-na1+2)
     yitmp2   = ( interp(  ym1[na1:nb1  ], ym2,   yitmp1  , extra=2 ) < (ny1-1) ) > 0
     yidntmp2 = ( interp(ymdn1[na1:nb1+1], ymdn2, yidntmp1, extra=2 ) < (ny1-1) ) > 0
  endelse



; loop over snapshots
  for isnap=0L,nsnap-1 do begin
     
     print, format='(a,i5,a,i5)', '% '+IdStr+' : read snapshot ',isnap+1,' of ',nsnap

     if (nfiles eq 1 ) then begin
        t = isnap
     endif else begin
        t = 0L
        simfile = finlist[isnap]
        print, '% '+IdStr+' : open snapshot file ',simfile
        openr, lun1, /get_lun, findir+'/'+simfile, swap_endian=swap_in
        readmesh, findir+'/'+finmesh, swap_endian=swap_in, xm=xm,ym=ym,zm=zm
        a = assoc(lun1,fltarr(nx1,ny1,nz1))
     endelse

     if (nsnap eq 1) then begin
        foutlabel = ''
     end else begin
        foutlabel='_'+string(isnap+ioffset, format='(I05)')
     endelse
     

; open output file for writing
     if (fout eq '') then begin
        lastSlash = strpos(simfile,'/',/reverse_search)
        file2 = strmid(simfile,lastSlash+1)
        file2 = foutdir+'/'+file2
     endif else begin
        file2 = foutdir+'/'+foutroot+foutlabel+fext
     endelse
     openw, lun2, /get_lun, file2, swap_endian=swap_out



; interpolation
     print, '% '+IdStr+' : interpolation ... '

     for ivar=0L, nvar-1 do begin

        print, format='(a-33,i2,a10,i2,a)', '% '+IdStr+' : variable=',ivar,' (',nvar,' )'

; operate on lnr=alog(rho) and ee=e/rho instead of rho and e
        
        case ivar of
           0: begin
              vartype='s'
              cube1  = alog( a[ivar + nvar*t] )
              yi02   = yitmp2
           end
           1: begin
              vartype='x'
              cube1  = a[ivar + nvar*t]
              yi02   = yitmp2
           end
           2: begin
              vartype='s'
              cube1  = a[ivar + nvar*t]
              yi02   = yidntmp2
           end
           3: begin
              vartype='z'
              cube1  = a[ivar + nvar*t]
              yi02   = yitmp2
           end
           4: begin
              vartype='s'
              cube1  = a[ivar + nvar*t] / a[0 + nvar*t] 
              yi02   = yitmp2
           end
           5: begin
              vartype='s'
              cube1  = a[ivar + nvar*t] 
              yi02   = yitmp2
           end
           6: begin
              vartype='x'
              cube1  = a[ivar + nvar*t]
              yi02   = yitmp2
           end
           7: begin
              vartype='s'
              cube1  = a[ivar + nvar*t]
              yi02   = yidntmp2
           end
           8: begin
              vartype='z'
              cube1  = a[ivar + nvar*t]
              yi02   = yitmp2
           end
        endcase


; interpolate, linear

        case vartype of
           's': begin
              xi02   = xiBuf2
              zi02   = ziBuf2
              cube1s[    0 :nx1-1,     *,   0 :nz1-1     ] = cube1[ *, *, * ]
              cube1s[ nx1  :nXPlus1-1, *,   0 :nz1-1     ] = cube1[ 0:nbufXR-1, *, * ]
              cube1s[      *,          *, nz1 :nZPlus1-1 ] = cube1s[  *,  *, 0:nbufZR-1 ]  
              cube2 = interpolate( cube1s, xi02, yi02, zi02, /grid )
           end
           'x': begin
              xi02   = xidnBuf2
              zi02   = ziBuf2
; note: this will only work for non-pathological cases where
; nbufXdnR (nbufXdnL) is less than nx1! 
              cube1x[     0          :nbufXdnL -1,      *, 0 :nz1-1 ]  = cube1[ nx1-nbufXdnL :nx1-1, *, * ]
              cube1x[ nbufXdnL       :nbufXdnL + nx1-1, *, 0 :nz1-1 ]  = cube1[ *, *, * ]
              cube1x[ nbufXdnL + nx1 :nXdnPlus1-1,      *, 0 :nz1-1 ]  = cube1[ 0 :nbufXdnR-1, *, * ]
              cube1x[ *, *, nz1 :nZPlus1-1 ]   = cube1x[ *, *, 0 :nbufZR-1 ]
              cube2 = interpolate( cube1x, xi02, yi02, zi02, /grid )
           end      
           'z': begin
; note: this will only work for non-pathological cases where
; nbufZdnR (nbufZdnL) is less than nz1! 
              xi02   = xiBuf2
              zi02   = zidnBuf2
              cube1z[ 0 :nx1-1, *, 0              :nbufZdnL -1      ]  = cube1[ *, *, nz1-nbufZdnL :nz1-1 ]
              cube1z[ 0 :nx1-1, *, nbufZdnL       :nbufZdnL + nz1-1 ]  = cube1[ *, *, * ]
              cube1z[ 0 :nx1-1, *, nbufZdnL + nz1 :nZdnPlus1-1      ]  = cube1[ *, *, 0 :nbufZdnR-1  ]
              cube1z[ nx1 :nXPlus1-1, *, * ]   = cube1z[ 0:nbufXR-1, *, *]
              cube2 = interpolate( cube1z, xi02, yi02, zi02, /grid )
           end
        endcase

; write to output file
        case ivar of
           0: begin
              dum = exp(cube2)
              writeu, lun2, dum
           end
           4:    writeu, lun2, cube2 * dum
           else: writeu, lun2, cube2
        endcase
        
     endfor

; close output/input file  
     free_lun, lun2
     if (nfiles gt 1) then free_lun, lun1
     print, '% '+IdStr+' : written output file ',file2

;   stop
  endfor

; debug
  if keyword_set(debug) then stop

; clean up
  cube1    = 0
  cube2    = 0
  cube1s   = 0
  cube1x   = 0
  cube1z   = 0

;  cd, current
  
END
