; Generate plane-parallel model with adiabatic stratifcation in
; hydrostatic equilibrium

@common_ceostab

; good_times: Round values for time-scales, in units of 100 s
good_secs = [ 0.01, 0.02, 0.05, 0.1, 0.2, 0.5, 1., 2., 3., 5., 6., 10., 12., 15., 20., 30., 45. ]
good_mins = [1., 1.5, 2., 3., 5., 6., 10., 12., 15., 20., 30., 45. ]
good_hrs  = [1., 1.5, 2., 2.5, 3., 4., 5., 6., 8., 10., 12., 18. ]
good_days = [1., 1.5, 2., 2.5, 3., 4., 5., 6., 7., 10., 12., 14., 21., 30., 60., 90., 120., 180., 360.]

good_times = [good_secs, good_mins*60., good_hrs*3600., good_days*24.*3600.] * 0.01

; General parameters
;
; NOTE: all physical variables are in internal units, unless otherwise
; specified
;
; teff = targeted Teff
; logg = log surface gravity (cgs units)
; (grav = surface gravity internal units)
;
; sbot = entropy at bottom
;
; MX, MY, MZ = dimensions of 3D array
; NG = number of ghost zones
;
;
; SWAP_ENDIAN = swap endianness if equal to 1
; GRAV_SUN = surface gravity, Sun
; SX_SUN = width of reference simulation, Sun
; TEFF_SUN = Teff, Sun
; 
; NDEP = number of grid-points for initial density-energy scale
;
; EEINI = initial guess for ee
; TEMP_DEPTH_ZERO = temperature at depth zero
;
; SIGMA_TEMP = standard deviation of random temperature fluctuations
; to be imposed at the surface (where temperature equals teff)
; ISEED = seed for RANDOMN function
;
; Parameters determined internally by routine:
;
; LNRARR_MIN = min ln density for initial scale
; LNRARR_MAX = max ln density
;

; Work directory
dir = '~/Downloads/test1d'
;dir='/priv/moka1/remo/test1d'

; EOS table file
tabfile ='EOSrhoe.tab'

; Output files
sim = 't50g25m00'
num = '00'
scr_file = sim+num+'.scr'
msh_file = sim+num+'.msh'
dx_file  = sim+num+'.dx'

; Stellar parameters: Teff, logg, and entropy at bottom of simulation:
;teff = 5000.d0
;logg = 2.0d0
;sbot = 0.0010901870d0

teff = 5000.d0
logg = 2.5d0
sbot = 0.00070502568d0

;teff = 6000.
;logg = 4.0
;sbot = 3.17690d-04


; Dimensions of simulation domain
MX = 120L
MY = 120L
MZ = 120L

; Number of ghost zones
NG = 5L

; Number of snapshots for initial run (for input file)
NSNAP = 100L

; Swap endianness?
SWAP_ENDIAN = 1

; Include ghost zones in data cubes and mesh?
DO_GHOST = 1

; Number of 3D variables (hd: 6; mhd: 9)
NVAR = 6L


; Reference values for the solar simulation
teff_sun = 5771.d0
logg_sun = 4.437686d0
grav_sun = 2.73959d0
sbot_sun = 5.0025112d-05
sx_sun = 7.0

; size of initial density array 
NDEP = 1000L

; initial guess for internal energy per unit mass
eeini = 0.1

TEMP_DEPTH_ZERO = 9500.
DELTA_LNP_TOP = -7.0
DELTA_LNP_BOT = 7.0

; temperature fluctuations [K], Gaussian distribution
SIGMA_TEMP = 50.
iseed = 400


; Main program starts here

; Save current directory
cd, current=current_dir

; Change to work directory
cd, dir

; Surface gravity:
;   convert from log to linear scale
;   convert to internal units
grav = 10.d0^logg/uug

; "Lower and upper boundary, physical domain, index space
lb = ng
ub = my-ng-1
ndep1 = ub-lb+1

; load EOS table
; add entropy variable
eostab, tabfile
eosstab

; determine density range for which table covers entropy=sbot value
is_valid = lonarr(nrtab)
for ir=0L,nrtab-1 do begin
   smax = max(tab[*,ir,0,6]/uue)
   smin = min(tab[*,ir,0,6]/uue)
   if (sbot gt smin and sbot lt smax) then is_valid[ir] = 1
endfor
ww_ir = where(is_valid eq 1)
ir_min = min(ww_ir)
ir_max = max(ww_ir)
dlnrtab = (lnrmax - lnrmin) / float(nrtab - 1)
lnrarr_min = (lnrmin + ir_min * dlnrtab) - alog(uur)
lnrarr_max = (lnrmin + ir_max * dlnrtab) - alog(uur)

; allow for a little margin:
lnrarr_min = lnrarr_min + dlnrtab*0.1
lnrarr_max = lnrarr_max - dlnrtab*0.1

;stop

; construct density grid
; construct entropy grid with constant specific entropy
; determine internal energy per unit mass consistent with entropy
lnrarr = cgScaleVector( dindgen(ndep), lnrarr_min, lnrarr_max, /DOUBLE)
eearr  = dblarr(ndep) + eeini
ssarr  = dblarr(ndep) + sbot
smake_eos, ssarr, lnrarr, eearr, /DEBUG, ITMAX=100, EPS=1.0e-6

; look up pressure
rarr  = exp(lnrarr)
pparr = exp( lookup_eos(alog(rarr*uur), alog(eearr*uue), 0) ) / uup
ttarr = exp( lookup_eos(alog(rarr*uur), alog(eearr*uue), 2) )


; find geometrical depth that fulfils hydrostatic equilibrium equation
; given pressure-density stratification and gravity
make_depth_hydeq, pparr, rarr, grav, depth

; find pressure at which temperature = TEMP_DEPTH_ZERO
lnp_depth_zero = INTERPOL( alog(pparr), ttarr, TEMP_DEPTH_ZERO, /SPLINE )

; find depth at which ln pressure = LNP_DEPTH_ZERO;
; shift zero point there
depth0 = INTERPOL( depth, alog(pparr), lnp_depth_zero, /SPLINE )
depth = depth - depth0[0]

; find depth at which LNP - LNP_DEPTH_ZERO = DELTA_LNP_TOP and
; depth at which LNP - LNP_DEPTH_ZERO = DELTA_LNP_BOT
depth_top = INTERPOL( depth, alog(pparr), lnp_depth_zero + DELTA_LNP_TOP, /SPLINE )
depth_bot = INTERPOL( depth, alog(pparr), lnp_depth_zero + DELTA_LNP_BOT, /SPLINE )

; generate uniform depth scale, between depth_top and depth_bot,
; with new number of depth points
depth1 = cgScaleVector( findgen(ndep1), depth_top, depth_bot )

; interpolate density and internal energy to new depth scale
rho1 = exp( interpol( alog(rarr), depth, depth1, /SPLINE) )
ee1  = exp( interpol( alog(eearr), depth, depth1, /SPLINE) )

;stop

; look up temperature and pressure
tt1 = exp( lookup_eos(alog(rho1*uur), alog(ee1*uue), 2) )
pp1 = exp( lookup_eos(alog(rho1*uur), alog(ee1*uue), 0) ) / uup


; define size and mesh
; horizontal size: scaling
sx = SX_SUN * (GRAV_SUN / grav) * (teff / TEFF_SUN)
sz = sx

; horizontal size: round sx and sz
ndigits = 1L                    ; number of significant digits
sx1 = 10.^(floor(alog10(sx))-ndigits-1L)
sx2 = float(ceil(sx/sx1))
sx = sx1 * sx2

sz1 = 10.^(floor(alog10(sz))-ndigits-1L)
sz2 = float(ceil(sz/sz1))
sz = sz1 * sz2

; horizontal scales, x and z
xm = cgScaleVector( findgen(mx), 0., sx*float(mx-1)/float(mx) )
zm = xm

;vertical depth scale:
sy = depth1[ndep1-1] - depth1[0]
ym = fltarr(my)
ym[lb:ub] = depth1


; define data cubes
r  = fltarr(mx, my, mz)
tt = fltarr(mx, my, mz)
ee = fltarr(mx, my, mz)
px = fltarr(mx, my, mz)
py = fltarr(mx, my, mz)
pz = fltarr(mx, my, mz)

; density, internal energy per unit mass, temperature cubes
for k=0L,mz-1 do begin
   for i=0L,mx-1 do begin
      r[i,lb:ub,k] = rho1
      ee[i,lb:ub,k] = ee1
      tt[i,lb:ub,k] = tt1
   endfor
endfor

; fill ghost zones
for j=0,lb-1 do begin
   r[*,j,*] = r[*,lb,*]
   ee[*,j,*] = ee[*,lb,*]
   tt[*,j,*] = tt[*,lb,*]
endfor

for j=ub+1,my-1 do begin
   r[*,j,*] = r[*,ub,*]
   ee[*,j,*] = ee[*,ub,*]
   tt[*,j,*] = tt[*,ub,*]
endfor
lnr = alog(r)

; perturb temperature where tt =~ teff
ww_teff = where(tt1 gt teff)
iy_teff = lb + ww_teff[0]
tt[*,iy_teff,*] = tt[*,iy_teff,*] + randomn(iseed, mx, mz) * SIGMA_TEMP

; Consistent internal energy per unit mass
tmake_eos,tt,lnr,ee

; Internal energy per unit volume
e = ee * r

; Write snapshot 
cd, dir
openw, lun, /GET_LUN, scr_file, SWAP_ENDIAN=SWAP_ENDIAN
writeu, lun, r
writeu, lun, px
writeu, lun, py
writeu, lun, pz
writeu, lun, e
writeu, lun, tt
free_lun, lun

; Write mesh file
write_mesh, msh_file, xm, ym, zm, SWAP_ENDIAN=swap_endian, $
            DXFILE=dx_file, NVAR=nvar, NGHOST=ng, DO_GHOST=do_ghost, $
            SX=sx, SY=sy, SZ=sz

cd, current_dir


; Parameters for input file

; Density and energy per unit volume, bottom boundary
rbot = rho1[ndep1-1]
ebot = ee1[ndep1-1] * rho1[ndep1-1]

; Time scales:

; Sound speed, 1D stratification
ccs1 = csound_eos(alog(rho1),ee1,gammma)

; Pressure scale height, 1D stratification
hpress1 = pp1/ rho1 / grav

; Time interval for snapshot taking
; (Sound-crossing time over one pressure scale height at surface)
j = ww_teff[0]
tsnap1 = (hpress1 / ccs1)[j]

; Time scale for boundary adjustment
; (Fraction of sound-crossing time over one pressure scale height at bottom)
j = ndep1 -1
t_bdry1 = (hpress1 / ccs1)[j] * 0.05
t_Bbdry1 = t_bdry1 * 0.5


; Round time scales
tsnap = good_times[closest(tsnap1,good_times)]
t_bdry = good_times[closest(t_bdry1,good_times)]
t_Bbdry = good_times[closest(t_Bbdry1,good_times)]

; Estimated duration of the simulation sequence: nsnap snapshots
tstop = nsnap * tsnap + 0.001

; Print specific input parameters:
print, format='(X,A7,X,F8.2)', 'tsnap =', tsnap
print, format='(X,A7,X,F9.3)', 'tstop =', tstop
print, ''
print, format='(X,A7,X,F10.4)', 'sx =', sx
print, format='(X,A7,X,F10.4)', 'sy =', sy
print, format='(X,A7,X,F10.4)', 'sz =', sz
print, ''
print, format='(X,A4,X,F10.6)', 'gy =', grav
print, ''
print, format='(X,A6,X,F13.6)', 'rbot =', rbot
print, format='(X,A6,X,F13.6)', 'ebot =', ebot
print, ''
print, format='(X,A8,X,F8.3)', 't_bdry =', t_bdry
print, format='(X,A9,X,F8.3)','t_Bbdry =', t_Bbdry

cd, current_dir

END
