function ym_add_ghost, depth, nghost=nghost

;+
;  NAME: YM_ADD_GHOST
;
;  PURPOSE: Add ghost zones to depth scale array
;
;  INPUT:
;    DEPTH: depth array; dimensions = NDEP
;
;  OUTPUT:
;    YM_NEW: depth array with added ghost zones;
;            new dimensions = NDEP+2*NGHOST 
;
;  KEYWORDS:
;    NGHOST: number of ghost cells; default=5
;
;  NOTES:
;    Ghost zones are added using a simple linear extrapolation of the
;    step between consecutive depth points
;
;    Coded by R. Collet
;-

  Id = 'YM_ADD_GHOST'

  if n_elements(nghost) eq 0 then nghost=5L

; Check nghost
  if nghost lt 0 then begin
     print, '% '+Id+': NGHOST must be larger than or equal to 0.'
     STOP
  endif

; Size of depth array
  NDEP = n_elements(depth)
  ny = NDEP + nghost * 2

; Define output ym_new scale
  ym_new = FLTARR(ny)

; Lower and upper indexes of depth scale within ym_new
  lb = nghost
  ub = ny-nghost-1

; Fill ghost zones
  if nghost eq 0 then begin
     ym_new = depth
  endif else begin
     ym_new = fltarr(ny)
     ym_new[lb:ub] = depth
     for j=nghost,0L,-1L do begin
        ym_new[nghost-j] = $
           float(j+1) * ym_new[nghost] - float(j) * ym_new[nghost+1]
     endfor
     for j=ny-nghost,ny-1,+1  do begin
        ym_new[j] = $
           3.*ym_new[j-1] -3.*ym_new[j-2] + ym_new[j-3]
     endfor
  endelse

  RETURN, ym_new
  
END
