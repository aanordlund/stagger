PRO FIT_POLY3_EXPM4, x, y, yfit, p=p, err=err

;+
;  NAME: FIT_POLY3_EXPM4
;
;  PURPOSE: fit linear combination of polynomial and exponential
;  function of the kind exp(-z^4) --where z=z(x) is a linear 
;  function-- to tabulated y=y(x) values.
;
;  The current implementation assumes a 3rd-degree polynomial
;  component for the function.
;
;
;  Note: uses MPFIT library for the fitting.
;
;  INPUT:
;    x: array with abscissa values for data points
;    y: array with ordinates
;;
;  OUTPUT:
;    yfit: result of fit
;
;  KEYWORDS:
;    p:   array of size np = 7  with initial values for fitting
;    parameters:
;       p[0] : Amplitude of "exp(-x^4)" function
;       p[1] : Horizontal shift of exp. function
;       p[2] : Width of exp. function
;       p[3] : Polynomial coefficient for zero-th order term
;       p[4] : Poly coeff. 1st order
;       p[5] : Poly coeff. 2nd order
;       p[6] : Poly coeff. 3rd order
;
;    err: uncartainty on y values (scalar)
;-

  IdStr = 'FIT_POLY3_EXPM4'
    
; Number of fit parameters
  NPAR = 7L

; Check dimensions of x and y array
  nx = n_elements( x )
  ny = n_elements( y )
  if nx ne ny then begin
     print, '% '+IdStr+': '+'X and Y must have the same number of elements. STOP.'
     stop
  endif

; Number of data points n = nx = ny
  n = nx

; If p array with fit parameters not supplied, set default initial values
  if n_elements( p ) eq 0 then begin
     p = dblarr(NPAR)
     p[0] =  0.3
     p[1] =  0.3
     p[2] =  0.05
     p[3] =  0.1
     p[4] =  1.0
     p[5] = 0.
     p[6] = 0.
  endif
  
; Check dimensions of p array
  np = n_elements( p )
  if np ne NPAR then begin
     print, '% '+IdStr+': P array with parameter values must contain ' + $
            STRCOMPRESS(string(NPAR),/REMOVE_ALL) + $
            ' elements. STOP.'
     stop
  endif

; Uncertainty on y values
  if n_elements( err ) eq 0 then err = fltarr(n) + 0.01

  
; Min and max values of X and Y arrays
  xmin = min( x )
  xmax = max( x )
  ymin = min( y )
  ymax = max( y )

  
; Define PARINFO structure with parameter information
  parinfo_struc = { PARNAME:'', $
                    VALUE:0.0d, $
                    FIXED:0, $
                    LIMITED:INTARR(2), $
                    LIMITS:DBLARR(2), $
                    STEP:0.0d }

  parinfo = REPLICATE(parinfo_struc, NPAR)

; PARINFO: parameter names
  parinfo[0].parname = 'Exp. funct., amplitude'
  parinfo[1].parname = 'Exp. funct., horiz. shift'
  parinfo[2].parname = 'Exp. funct., width'
  parinfo[3].parname = 'Poly coeff., 0th order'
  parinfo[4].parname = 'Poly coeff., 1st order'
  parinfo[5].parname = 'Poly coeff., 2nd order'
  parinfo[6].parname = 'Poly coeff., 3rd order'

; PARINFO: parameter limits
; Amplitude: lower limit is zero
  parinfo[0].limited[0] = 0
  parinfo[0].limited[1] = 0
  parinfo[0].limits[0] = 0.

; Width: lower limit is zero
  parinfo[2].limited[0] = 1
  parinfo[2].limited[1] = 0
  parinfo[2].limits[0] = 0.

; PARINFO: step for derivatives?
; Note: if set to 0., then fitting routine automatically takes care of that
  parinfo[*].step  = 0.         ; 0.001    

; PARINFO: fixed parameters
  parinfo[*].fixed = 0          ; default: none
  parinfo[5].fixed = 0
  parinfo[6].fixed = 0

; Limits on high-order coefficients
  parinfo[5].limited[0] = 1
  parinfo[5].limited[1] = 1
  parinfo[5].limits[0] = -2.
  parinfo[5].limits[1] =  2.
  
  parinfo[6].limited[0] = 1
  parinfo[6].limited[1] = 1
  parinfo[6].limits[0] = -2.
  parinfo[6].limits[1] =  2.

; Rescale x and y to [0,1] range
  x1 = cgScaleVector( x, 0., 1.)
  y1 = cgScaleVector( y, 0., 1.)
  err1 = err / (ymax - ymin) + dblarr(n) ; array with uncertainties

; PARINFO: starting values: initialise
  pstart           = p          ; copy of starting values, for reference
  parinfo[*].value = p          ; store starting values in PARINFO data structure


; Actual fitting starts here

; IDL data structure containing arguments accepted by fitting function
  functargs = { }

; Call fitting routine: perturb p and iterate until chi^2 is minimised
  p = MPFITFUN( 'POLY3_EXPM4_MP', x1, y1, err1, pstart, $
                PARINFO=parinfo, $
                FUNCTARGS=functargs, $
                STATUS=status, $
                ERRMSG=errmsg )

; Evaluate function for best fitting parameters
  f = POLY3_EXPM4_MP(x1, p)

; Scale back to original range
  yfit = cgScaleVector(f,ymin,ymax)

END
