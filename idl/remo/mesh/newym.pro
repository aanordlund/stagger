PRO newym, fname_in, fname_out, ym_new, $
           DXFILE_IN=dxfile_in, FMESH_IN=fmesh_in, DIR=dir, T=t, $
           NGHOST=nghost, DO_GHOST=do_ghost, LINEAR=linear, $
           SWAP_IN=swap_in, SWAP_OUT=swap_out

;+
;  NAME: NEWYM
;
;  PURPOSE: interpolated Stagger snapshot to new geometrical depth
;  scale (y-axis)
;
;  INPUT:
;    fname_in: input snatpshot file name (.scr or .dat file)
;    ym_new: new geometrical depth scale (y-axis)
;
;  OUTPUT:
;    fname_out: output snapshot file name
;
;  NOTES:
;    Requires intrp1 and intrp3 from Aake Nordlund's IDL repository
;-

  Id = 'NEWYM'

@common_cmesh
@common_cdata

; Default values
  if n_elements(dir) eq 0 then dir = '.'
  if n_elements(t) eq 0 then t = 0L
  if n_elements(nghost) eq 0 then nghost = 5L
  if n_elements(do_ghost) eq 0 then do_ghost = 0
  if n_elements(linear) eq 0 then linear = 0
  if n_elements(swap_in) eq 0 then swap_in = 0
  if n_elements(swap_out) eq 0 then swap_out = 0
; Standard file names for input .dx and .msh files
  root_in  = fileroot(fname_in)
  pos = STRPOS(root_in, '_')
  if (pos ne -1) then begin
     root_in = STRMID(root_in, 0, pos) 
  endif
  if n_elements(dxfile_in) eq 0 then dxfile_in = root_in + '.dx' 
  if n_elements(fmesh_in) eq 0 then fmesh_in = root_in + '.msh' 


; Standard file names for output .dx and .msh files
  root_out  = fileroot(fname_out)
  pos = STRPOS(root_out, '_')
  if (pos ne -1) then begin
     root_out = STRMID(root_out, 0, pos) 
  endif
  dxfile_out = root_out + '.dx' 
  mshfile_out = root_out + '.msh'


; Current directory
  cd, current=current_dir

; Open and read input file
  cd, dir
  open, a, fname_in, MESHFILE=fmesh_in, SWAP_ENDIAN=swap_in

; Lower and upper boundaries (index space)
  lb = nghost
  ub = ny - nghost -1
  
; Pure hydro or MHD snapshot?
  if (nvar eq 9L) then begin
     print, '% '+Id+': NVAR = 9: MHD snapshot'
  endif

; Size of new depth array
; Lower and upper boundaries (index space), new depth array
  ny2 = n_elements(ym_new)
  lb2 = nghost
  ub2 = ny2 - nghost -1

; Initialise data cube with new depth, same horizontal dimensions
  cube2 = fltarr(nx, ny2, nz)

  
; Open output scratch file for writing
  openw, lun_out, fname_out, /GET_LUN, SWAP_ENDIAN=swap_out
  
; Loop over variables
  for ivar=0,nvar-1 do begin

     print,'% '+Id+' : ivar=',ivar
     case ivar of
        0: cube1 = alog( a[0 + nvar*t] )
        1: cube1 = a[1 + nvar*t]
        2: cube1 = a[2 + nvar*t]
        3: cube1 = a[3 + nvar*t]
        4: cube1 = a[4 + nvar*t] / a[0 + nvar*t]
        5: cube1 = a[5 + nvar*t]
        6: cube1 = a[6 + nvar*t]
        7: cube1 = a[7 + nvar*t]
        8: cube1 = a[8 + nvar*t]
     endcase

; Linear interpolation ?
; Note: too slow for large simulations, needs rewriting
     if (LINEAR) then begin
        print, '% '+Id+': LINEAR interpolation'
        for k=0L,nz-1 do begin
           for i=0L,nx-1   do begin
              cube2[i,lb2:ub2,k] = $
                 intrp1(ym[lb:ub], $
                        ym_new[lb2:ub2], $
                        cube1[i,lb:ub,k], $
                        /EXTRA, $
                        /LINEAR )
           endfor
        endfor
     endif else begin
        for k=0L,nz-1 do begin
           cube2[*,lb2:ub2,k] = $
              intrp3y(ym[lb:ub], $
                      ym_new[lb2:ub2], $
                      reform(cube1[*,lb:ub,k],nx,ub-lb+1,1), $
                      /EXTRA )
        endfor
     endelse

; Ghost zones: initialise
     if (DO_GHOST) then begin
        print, '% '+Id+': DO_GHOST'
        for j=0L,nghost-1  do begin
           cube2[*,j,*] = cube2[*,lb,*]
           cube2[*,ub+1+j,*] = cube2[*,ub,*]
        endfor
     endif

; Write arrays to output scratch file
     case ivar of
        0: begin
           dum = exp( cube2 )
           writeu, lun_out, dum
        end
        1: writeu, lun_out, cube2
        2: writeu, lun_out, cube2
        3: writeu, lun_out, cube2
        4: writeu, lun_out, cube2 * dum
        5: writeu, lun_out, cube2
        6: writeu, lun_out, cube2
        7: writeu, lun_out, cube2
        8: writeu, lun_out, cube2
     endcase

  endfor

; Close output file
  free_lun, lun_out

; Write vertical mesh
  write_mesh, mshfile_out, xm, ym_new, zm, $
              dxfile=dxfile_out, nvar=nvar, $
              sx=sx, sy=sy, sz=sz, $
              do_ghost=do_ghost, $
              nghost=nghost, $
              swap_endian=swap_out

; Free memory
  cube1 = 0
  cube2 = 0
  dum   = 0

; CD to original directory
  cd, current_dir
END
