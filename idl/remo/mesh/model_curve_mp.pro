FUNCTION MODEL_CURVE_MP, x, p

;+
;  NAME: MODEL_CURVE_MP
;
;  PURPOSE: evaluate function of the form:
;
;  f = 1. / ( 1./(abs(0.7*(x-p[0])) > 0.9*p[1])^p[3]
;           + p[2] * exp(-((x-p[0])/p[1])^4) ) 
;
;  based on the one proposed by R. Trampedach.
;
;  The function is designed for interfacing with Craig
;  Markwardt's MPFIT IDL library for curve fitting.
;
;  (https://www.physics.wisc.edu/~craigm/idl/mpfittut.html)
;
;
;  INPUT:
;    x: array with abscissas
;    p: array with parameters
;
;  NOTE: interfacing with the MPFIT library requires that the above
;  variable be named x and p
;
;  OUTPUT:
;    f: value of function at x points
;
;
;  KEYWORDS (PARAMETERS):
;
;    None
;
;  ADDITIONAL PARAMETERS AND KEYWORDS:
;
;    None
;
;  NOTES:
;    Designed to work with MPFIT library.
;    Coded by R. Collet
;
;-

  IdStr = 'MODEL_CURVE_MP'

; Number of parameters (dimension of p array):
  NPAR = 4L

; Size of x and p arrays  
  nx = N_ELEMENTS( x )
  np = N_ELEMENTS( p )

; Sanity check on number of p elements
  IS_NP_VALID = np eq NPAR
  
  if ~IS_NP_VALID then begin
     print, '% '+IdStr+': Dimension of p should be equal to ' + $
            STRCOMPRESS(STRING(NPAR,FORMAT='(I2)'),/REMOVE_ALL) + '. Stop.'
     STOP
  endif

; Evaluate function
  f = 1. / ( 1. / ( abs(0.7*(x-p[0]) ) > 0.9*p[1] )^p[3] + p[2] * exp( -((x-p[0])/p[1])^4 ) )
  
; Return result
  RETURN, f

END
