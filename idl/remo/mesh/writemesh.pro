PRO WRITEMESH, fmesh, xm, ym, zm, _extra=_extra

;+
;  NAME: WRITEMESH
;
;  PURPOSE: wrapper to write_mesh.pro; writes mesh information for
;          stagger-code simulations into a binary file
;
;  NOTES: Coded by R. Collet, 2015-09-01
;-

  WRITE_MESH, fmesh, xm, ym, zm, _extra=_extra
  
END

