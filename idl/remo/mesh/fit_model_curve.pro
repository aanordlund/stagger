PRO FIT_MODEL_CURVE, x, y, yfit, P=p, ERR=err

;+
;  NAME: FIT_MODEL_CURVE
;
;  PURPOSE: fit smooth model curve to tabulated y = y(x). The
;  actual function is coded in MODEL_CURVE_MP.
;
;  The present version assume a functional form proposed by
;  R. Trampedach:
;
;  f = 1. /  ( 1./(abs(0.7*(x-p[0])) > 0.9*p[1])^p[3] +
;              + p[2] * exp(-((x-p[0])/p[1])^4) ) 
;
;
;  INPUT:
;    x: array with abscissa values for data points
;    y: array with ordinates
;
;  OUTPUT:
;    yfit: result of fit
;
;  KEYWORDS:
;    P:   array with initial values for fitting parameters:
;
;       p[0] : zero point
;       p[1] : width of exp. function
;       p[2] : amplitude of exp. function
;       p[3] : exponent of abs. term
;
;    ERR: uncertainty on y values (array)
;
;  NOTES:
;    Designed to work with MPFIT library.
;    Functional form based on the one proposed by R. Trampedach;
;    Coded by R. Collet
;-

  IdStr = 'FIT_MODEL_CURVE'
    
; Number of fit parameters
  NPAR = 4L
  
; Check dimensions of x and y array
  nx = n_elements( x )
  ny = n_elements( y )
  if nx ne ny then begin
     print, '% '+IdStr+': '+'X and Y must have the same number of elements. STOP.'
     stop
  endif

; Number of data points n = nx = ny
  n = nx

; If p array with fit parameters not supplied, set default initial values
  if n_elements( p ) eq 0 then begin
     p = dblarr(NPAR)
     p[0] =  0.3
     p[1] =  0.2
     p[2] =  1.0
     p[3] =  1.0
  endif
  
; Check dimensions of p array
  np = n_elements( p )
  if np ne NPAR then begin
     print, '% '+IdStr+': P array with parameter values must contain ' + $
            STRCOMPRESS(string(NPAR),/REMOVE_ALL) + $
            ' elements. STOP.'
     stop
  endif

; Uncertainty on y values
  if n_elements( err ) eq 0 then err = fltarr(n) + 0.01


; Min and max values of X and Y arrays
  xmin = min( x )
  xmax = max( x )
  ymin = min( y )
  ymax = max( y )

  
; Define PARINFO structure with parameter information
  parinfo_struc = { PARNAME:'', $
                    VALUE:0.0d, $
                    FIXED:0, $
                    LIMITED:INTARR(2), $
                    LIMITS:DBLARR(2), $
                    STEP:0.0d }

  parinfo = REPLICATE(parinfo_struc, NPAR)

; PARINFO: parameter names
  parinfo[0].parname = 'zero point'
  parinfo[1].parname = 'width of exp. function'
  parinfo[2].parname = 'amplitude of exp. function'
  parinfo[3].parname = 'exponent of abs. term'

; PARINFO: parameter limits
; p[0]: zero point: between 0 and 1
  parinfo[0].limited[0] = 1
  parinfo[0].limited[1] = 1
  parinfo[0].limits[0] = 0.
  parinfo[0].limits[1] = 1.
; p[1]: width of exp. function: positive
  parinfo[1].limited[0] = 1
  parinfo[1].limited[1] = 0
  parinfo[1].limits[0] = 0.
; p[2]: amplitude of exp. function: between 0 and 1.0e4
  parinfo[2].limited[0] = 1
  parinfo[2].limited[1] = 1
  parinfo[2].limits[0] = 0.
  parinfo[2].limits[1] = 1.0e4
; p[3]: exponent of abs. term: positive
  parinfo[3].limited[0] = 1
  parinfo[3].limited[1] = 0
  parinfo[3].limits[0] = 0.
  

; PARINFO: step for derivatives?
; Note: if set to 0., then fitting routine automatically takes care of that
  parinfo[*].step  = 0.         ; 0.001    

; PARINFO: fixed parameters
  parinfo[*].fixed = 0          ; default: none

; Rescale x and y to [0,1] range
  x1 = cgScaleVector( x, 0., 1.)
  y1 = cgScaleVector( y, 0., 1.)

; Scale error array
  err1 = err / (ymax - ymin)
  
; PARINFO: starting values: initialise
  pstart           = p          ; copy of starting values, for reference
  parinfo[*].value = p          ; store starting values in PARINFO data structure


; Actual fitting starts here

; IDL data structure containing arguments accepted by fitting function
  functargs = { }

; Call fitting routine: perturb p and iterate until chi^2 is minimised
  p = MPFITFUN( 'MODEL_CURVE_MP', x1, y1, err1, pstart, $
                PARINFO=parinfo, $
                FUNCTARGS=functargs, $
                STATUS=status, $
                ERRMSG=errmsg )

; Evaluate function for best fitting parameters
  f = MODEL_CURVE_MP(x1, p)

; Scale back to original range
  yfit = cgScaleVector(f,ymin,ymax)

END
