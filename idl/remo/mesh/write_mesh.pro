PRO WRITE_MESH, fmesh, xm, ym, zm, $
                DXFILE=dxfile, $
                NVAR=nvar, $
                GAMMA=gamma, $
                SX=sx, $
                SY=sy, $
                SZ=sz, $
                DO_GHOST=do_ghost, $
                NGHOST=nghost, $
                SWAP_ENDIAN=swap_endian

;+
;  NAME: WRITE_MESH
;
;  PURPOSE: write mesh information for stagger-code simulations
;           to a binary file
;
;  INPUT:
;    fmesh = file name where to store mesh information
;    xm, ym, zm = x, y, z coordinates of cell centres
;
;  KEYWORDS (INPUT):
;    dxfile = dx file name
;    nvar = number of variables
;    gamma = gamma exponent
;    swap_endian = swap endianness
;
;  KEYWORDS ()OUTPUT):
;    sx, sy, sz = size of mesh, x, y, and z directions
;
;  NOTES:
;    Coded by R. Collet
;-

  IdStr = 'WRITE_MESH'

  default, nghost, 5L
  default, do_ghost, 0
  default, nvar,  6
  default, gamma, 1.666667
  default, xvert, 0L
  default, zero, 0L
  default, swap_endian, 0L

  nx = n_elements( xm )
  ny = n_elements( ym )
  nz = n_elements( zm )

; if do_ghost, then force recompute ghost zones for ym array
  if KEYWORD_SET( do_ghost ) then begin
     if (nghost gt 0L) then begin
        for j=nghost,0,-1 do begin
           ym[nghost-j] = float(j+1) * ym[nghost] - float(j)*ym[nghost+1]
        endfor
        for j=ny-nghost,ny-1,+1  do begin
           ym[j] = 3.*ym[j-1] - 3.*ym[j-2] + ym[j-3]
        endfor
     endif
  endif

; compute stagger mesh arrays
  spline = 1
  double = 0
  make_stagger_mesh, xm, ym, zm, $
                     dxm, dxmdn, xmdn, dxidxup, dxidxdn, $
                     dym, dymdn, ymdn, dyidyup, dyidydn, $
                     dzm, dzmdn, zmdn, dzidzup, dzidzdn, $
                     sx=sx, $
                     sy=sy, $
                     sz=sz, $
                     spline=spline, $
                     double=double

; write mesh file
  openw, /get, lun, fmesh, /f77_unformatted, swap_endian=swap_endian
  writeu, lun, long(nx), long(ny), long(nz)
  writeu, lun, dxm, dxmdn, xm, xmdn, dxidxup, dxidxdn
  writeu, lun, dym, dymdn, ym, ymdn, dyidyup, dyidydn
  writeu, lun, dzm, dzmdn, zm, zmdn, dzidzup, dzidzdn
  free_lun, lun


; write dxfile
  if KEYWORD_SET( dxfile ) then begin
     dx = sx / float(nx)
     dy = sy / float(ny)
     dz = sz / float(nz)
     openw, lun, dxfile
     printf,lun, nx, ny, nz
     printf,lun, dx, dy, dz
     printf,lun, gamma, xvert
     printf,lun, nvar
     printf,lun, zero
     free_lun, lun
  endif

END

