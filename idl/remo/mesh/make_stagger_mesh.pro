pro make_stagger_mesh, xm, ym, zm, $
                       dxm, dxmdn, xmdn, dxidxup, dxidxdn, $
                       dym, dymdn, ymdn, dyidyup, dyidydn, $
                       dzm, dzmdn, zmdn, dzidzup, dzidzdn, $
                       sx=sx, $
                       sy=sy, $
                       sz=sz, $
                       spline=spline, $
                       double=double

;+
;  NAME: make_stagger_mesh
;
;  PURPOSE: compute stagger mesh variables given xm, ym, zm arrays
;           with coordinates of cell centres
;
;  INPUT:
;    xm, ym, zm = arrays with x, y, z coordinates of cell centres
;
;  OUTPUT: finite differences of coordinate arrays and
;          staggered derivatives of indexes with respect
;          to coordinates
;  KEYWORDS:
;    sx, sy, sz = arrays with x, y, and z sizes
;-

  Id = 'make_stagger_mesh'

  if n_elements(double) eq 0 then double=0
  if n_elements(spline) eq 0 then spline=0

  nx = n_elements(xm)
  ny = n_elements(ym)
  nz = n_elements(zm)

; convert xm, ym, zm to double precision
  xm_bak = xm
  ym_bak = ym
  zm_bak = zm
  xm = double(xm)
  ym = double(ym)
  zm = double(zm)

; define staggered mesh arrrays and index derivatives
  xmdn    = dblarr(nx)
  dxidxup = dblarr(nx)
  dxidxdn = dblarr(nx)
  dxm     = dblarr(nx)
  dxmdn   = dblarr(nx)

  ymdn    = dblarr(ny)
  dyidyup = dblarr(ny)
  dyidydn = dblarr(ny)
  dym     = dblarr(ny)
  dymdn   = dblarr(ny)

  zmdn    = dblarr(nz)
  dzidzup = dblarr(nz)
  dzidzdn = dblarr(nz)
  dzm     = dblarr(nz)
  dzmdn   = dblarr(nz)

; index arrays, double precision
  xi = dindgen(nx) + 1.0d0
  yi = dindgen(ny) + 1.0d0
  zi = dindgen(nz) + 1.0d0

; index derivatives (spline), up
  dxidxup = 1.0 / derf(xi, xm)
  dyidyup = 1.0 / derf(yi, ym)
  dzidzup = 1.0 / derf(zi, zm)

; interpolate coordinate arrays to half indexes
; (coordinates of cell faces half a index down)
  xmdn = interpol(xm, xi, xi-0.5d0, spline=spline)
  ymdn = interpol(ym, yi, yi-0.5d0, spline=spline)
  zmdn = interpol(zm, zi, zi-0.5d0, spline=spline)
;  xmdn = interpol(xm, xi, xi-0.5d0)
;  ymdn = interpol(ym, yi, yi-0.5d0)
;  zmdn = interpol(zm, zi, zi-0.5d0)

; index derivatives (spline), down
  dxidxdn = 1.0 / derf(xi, xmdn)
  dyidydn = 1.0 / derf(yi, ymdn)
  dzidzdn = 1.0 / derf(zi, zmdn)

; separation between cell centres
  dxmdn[1:nx-1] = xm[1:nx-1] - xm[0:nx-2]
  dymdn[1:ny-1] = ym[1:ny-1] - ym[0:ny-2]
  dzmdn[1:nz-1] = zm[1:nz-1] - zm[0:nz-2]
  dxmdn[0] = dxmdn[1]
  dymdn[0] = dymdn[1]
  dzmdn[0] = dzmdn[1]

; separation between cell faces (size of cells)
  dxm[0:nx-2] = xmdn[1:nx-1] - xmdn[0:nx-2]
  dym[0:ny-2] = ymdn[1:ny-1] - ymdn[0:ny-2]
  dzm[0:nz-2] = zmdn[1:nz-1] - zmdn[0:nz-2]
  dxm[nx-1] = dxm[nx-2]
  dym[ny-1] = dym[ny-2]
  dzm[nz-1] = dzm[nz-2]

; compute mesh's physical size
  sx = xm[nx-1] - xm[0] + (xm[0] - xmdn[0]) + (xm[nx-1] - xmdn[nx-1])
  sy = ym[ny-1] - ym[0] + (ym[0] - ymdn[0]) + (ym[ny-1] - ymdn[ny-1])
  sz = zm[nz-1] - zm[0] + (zm[0] - zmdn[0]) + (zm[nz-1] - zmdn[nz-1])


; convert to float, if keyword double not set
if (~double) then begin

  sx = float(sx)
  sy = float(sy)
  sz = float(sz)

  dxm    = float(dxm)
  dxmdn  = float(dxmdn)
  xmdn   = float(xmdn)
  dxidxup= float(dxidxup)
  dxidxdn= float(dxidxdn)

  dym    = float(dym)
  dymdn  = float(dymdn)
  ymdn   = float(ymdn)
  dyidyup= float(dyidyup)
  dyidydn= float(dyidydn)

  dzm    = float(dzm)
  dzmdn  = float(dzmdn)
  zmdn   = float(zmdn)
  dzidzup= float(dzidzup)
  dzidzdn= float(dzidzdn)

  xm = float(xm)
  ym = float(ym)
  zm = float(zm)

endif

end
