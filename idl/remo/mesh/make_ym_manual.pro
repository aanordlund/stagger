FUNCTION MAKE_YM_MANUAL, ym, var, $
                         DX=dx, $
                         NGHOST=nghost, $
                         LIMIT=limit, $
                         DO_2ND_DERIV=do_2nd_deriv, $
                         DO_PLOT=do_plot, $
                         FPROXY=fproxy, $
                         YNODES=ynodes, $
                         FNODES=fnodes

;+
; NAME: MAKE_YM_MANUAL
;
; PURPOSE: user-guided optimisation of depth scale from Stagger
;          simulation for better resolution of near-surface layers
;
; INPUT:
;   ym:  geometrical depth scale
;   var: (3D) data cube with variable to be used to construct proxy for
;        optimal depth scale spacing (either ln Rosseland coeff. or
;        temperature)
;
; OUTPUT:
;   ym_new: optimised depth scale
;
; KEYWORDS:
;
;   DX: horizontal grid spacing
;   NGHOST: number of ghost cells (top/bottom)
;   LIMIT: maximum allowed vertical/horizontal aspect ratio
;         (corresponding to MAX_CELL_ASP_RATIO in OPTIM_DEPTH_MANUAL
;          routine)
;   DO_2ND_DERIV: use second derivative of var with respect to depth to
;                 construct proxy function
;   DO_PLOT: switch on to plot results
;   FPROXY: proxy function
;
;   YNODES, FNODES: nodes of spline curve for fitting normalised
;                   fproxy curve as function of normalised depth scale
;
; NOTES:
;
;   Coded by R. Collet
;-

  IdStr = 'MAKE_YM_MANUAL'

; number of depth points, input depth scale
  NY = n_elements(ym)

; number of ghost zones
  if n_elements(nghost) eq 0 then nghost=5L

; location of lower and upper phsyical boundaries (index space)
  lb = nghost
  ub = ny - nghost - 1L

; dy uniform grid
  mean_dy = abs( (ym[ub]-ym[lb]) / float(ny-2*nghost-1) )

; default dx, if not otherwise provided
  if n_elements(dx) eq 0 then dx=mean_dy
  if n_elements(limit) eq 0 then limit=3.0
  if n_elements(do_2nd_deriv) eq 0 then do_2nd_deriv=0L
  if n_elements(do_plot) eq 0 then do_plot=0L

; Vertical/horizontal aspect ratio for uniform grid
  mean_cell_asp_ratio = mean_dy / dx


; Construct proxy for optimal depth scale spacing
;
; Default: use max value of derivative of var on horizontal planes;
; in this case, it is usually assumed that var is equal to ln of the (Rosseland)
; extinction coefficient.
;
; Alternatively, if do_2nd_deriv is switched on, use second derivative of var;
; for this case, it is usually assumed that var is equal to the temperature

; Dimensions of var
  ns_var = SIZE(var)
  if ns_var[0] eq 1 then begin
     nx_var = 1L
     ny_var = ns_var[1]
     nz_var = 1L
  endif else begin
     nx_var = ns_var[1]
     ny_var = ns_var[2]
     nz_var = ns_var[3]
  endelse

; ny_var compatible with ny?
  if ny_var ne ny then begin
     print, '%'+IdStr+': depth dimensions of ym and var are incompatible. STOP.'
     STOP
  endif

; x and z dimensions, rename for clarity
  nx = nx_var
  nz = nz_var

; Turn var into 3D array, if not such already
; Remove ghost zones
  if ns_var[0] eq 1 then begin
     var1 = reform(var[lb:ub], nx, ny-2*nghost, nz)
  endif else begin
     var1 = reform(var[*,lb:ub,*])
  endelse
  depth = reform(ym[lb:ub])


; Array to store first or second vertical derivative of var1
  dvar = fltarr(nx, ny-2*nghost, nz)

; compute vertical derivatives: loop over x- and z- indexes, slow ...
  if (do_2nd_deriv) then begin
     print, '% '+IdStr+': '+ 'compute second derivative with respect to depth'
     for i=0L,nx-1 do begin
        for k=0L,nz-1 do begin
           derivs, depth, var1[i,*,k], dfdx, d2fdx2
           dvar[i,*,k] = d2fdx2
        endfor
     endfor
  endif else begin
     print, '% '+IdStr+': '+ 'compute first derivative with respect to depth'
     for i=0L,nx-1 do begin
        for k=0L,nz-1 do begin
           derivs, depth, var1[i,*,k], dfdx, d2fdx2
           dvar[i,*,k] = dfdx
        endfor
     endfor
  endelse

; Define proxy for optimal depth scale spacing as inverse of square root of
; maximum of abs(dvar) at each depth
  fproxy = fltarr(ny-2*nghost)
  for j=0L,ny-2*nghost-1L do begin
     fproxy[j] = max(abs(dvar[*,j,*]))
  endfor
  fproxy = 1./sqrt(fproxy)

; Fit model curve to fproxy
  max_cell_asp_ratio = limit
  depth_new = OPTIM_DEPTH_MANUAL(depth, fproxy, $
                                 MAX_CELL_ASP_RATIO=max_cell_asp_ratio, $
                                 MEAN_CELL_ASP_RATIO=mean_cell_asp_ratio, $
                                 DO_PLOT=do_plot, $
                                 YNODES=ynodes, $
                                 FNODES=fnodes)

; Fill ghost zones
  ym_new = ym_add_ghost( depth_new, nghost=nghost)

  RETURN, ym_new

END
