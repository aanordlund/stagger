function make_ym,ym,lnab,dx=dx,shift=shift,stop=stop,bottom=bottom,limit=limit

;+
;  NAME: MAKE_YM
;
;  PURPOSE: compute optimal ym scale given extinction coefficient
;           data cube lnab
;
;  NOTES: Coded by Z. Magic
;-
  
;some parameters
  if n_elements(limit) eq 0 then limit = 3.8
;  limit=3.8     ;upper limit for aspect ratio dx/dy, main parameter
  dist23=0.1    ;distance of bottom to top for middle part (pot)
  dist_io=0.04  ;distance between inner & outer parts, fraction of total depth
  ratio_io=1.   ;ratio
  nn=100        ;number of equidistant points for xn,yn for outer parts (boxiness), >50
  frac_yn0=0.4  ;fraction of yn1(5) to determin yn1(0)
  plot=0        ;do plot
;===============================================================
;start
  default, limit, 3.8
  default, nghost,  5L
  ny   = size(ym,/n_elem)
  s    = size(lnab)
  if (s[0] eq 1) then begin
     ny1  = s[1]
     lnab  = reform(lnab,1,ny1,1)
  endif
;===============================================================
;prepare refernce function lnab
  yy2   = ym[nghost:ny-nghost-1L]                  ;depth without ghostzone
  f2    = fltarr(ny)                               ;help function: f2=max_h[dlnab/dy]
  ymnew = fltarr(ny)                               ;ln(ab) = ln(rho*kap)
  dlnab  = yder(ym,lnab)                           ;first derivative: dlnab/dy
  for j=0L,ny-1L do f2[j] = max(abs(dlnab[*,j,*])) ;get horiz. max: max_h(f2)
  ff2	= 1./ sqrt( f2[nghost:ny-nghost-1L] )      ;ff2=[sqrt(f2)]^-1
;smooth ff2
  sff2=ts_smooth(ff2,50) & msff2=mean(sff2)
;===============================================================
;find 6 anchor points (xn1,yn1) to divide in three parts
  xn1=fltarr(6) & yn1=xn1
;get min of sff2
  sel=where(yy2 gt 0)
  b=where(sff2 eq min(sff2(sel))) & bmin=b(0)
;1p: bottom
  b5=n_elements(ff2)
  xn1(5)=yy2(b5-1) & yn1(5)=ff2(b5-1)
;2p: top height = half bottom
  xn1(0)=yy2(0) & yn1(0)=yn1(5)*frac_yn0
;3p: max in ff2 lt 0
  b=where(ff2(0:bmin) eq max(ff2(0:bmin))) & b1=b(0)
  xn1(1)=yy2(b1) & yn1(1)=ff2(b1)
;4p: right to max
  b=where(ff2(b1:*) ge yn1(1)) & b=b(1)+b1
  xn1(4)=yy2(b) & yn1(4)=yn1(1)
;5p: left bottom
  d=abs(xn1(4)-xn1(1))*dist23
  xn1(2)=xn1(1)+d & yn1(2)=ff2(bmin)
;6p: right bottom
  xn1(3)=xn1(4)-d & yn1(3)=ff2(bmin)
;save bottom
  xn3=xn1 & yn3=yn1
;===============================================================
;start finding best depthscale
  dtop=0.
  dbot=0.
  shift=1.001
  sign=-1. ;initially decrasing
  it=0     ;counter for iterations
  while abs(limit-dtop) gt 0.03 do begin ;shift decrement
     it++
     shift+=.001*sign           ;sign: up/down iteration of shift
;shift anchor points for bottom of middle part or outer boundaries (only down)
     if shift lt 1 then yn1(0)=yn1(1)+(yn1(0)-yn1(1))*shift/ratio_io
     if shift lt 1 then yn1(5)=yn1(1)+(yn1(5)-yn1(1))*shift/ratio_io
     yn1(2)=yn1(1)+(yn1(2)-yn1(1))*shift
     yn1(3)=yn1(1)+(yn1(3)-yn1(1))*shift
     ;yn1(5)=yn1(1)+(yn1(5)-yn1(1))*0.4
;get equidistant nodes (xn2,yn2) for outer parts
     xn2=findgen(nn)/(nn-1)*(xn1(5)-xn1(0))+xn1(0)
     yn2=interpol(yn1,xn1,xn2)
     d=abs(xn1(5)-xn1(0))*dist_io
     sel1=where(xn2 gt xn1(1)-d and xn2 lt xn1(4)+d,comp=sel2)
;combine two outer linear parts with spline nodes for middle part
     dbot=-1.
     hh=0.
     while dbot lt 0 do begin   ;hh increment
;get middle part
        hh+=.001
        d3=abs(yn1(1)-yn1(2))*0.01+hh ;height control
        d4=abs(xn1(2)-xn2(sel1(0)))
        xnmid=[xn1(2),xn2(sel1(n_elements(sel1)-1))-d4]
        ynmid=[yn1(2)+d3,yn1(3)+d3]
;add outer parts - nodes ready to spline
        xn=[xn2(0:sel1(0)),xnmid,xn2(sel1(n_elements(sel1)-1):*)]
        yn=[yn2(0:sel1(0)),ynmid,yn2(sel1(n_elements(sel1)-1):*)]
;spline, result: fnorm -> lnab
        fnorm = spline(xn,yn,yy2)
;check height of pot
        dbot=min(fnorm)-yn1(2)
     ;print,dbot,hh
     endwhile
;interpolate newym based on fnorm
     ny1   = ny-2L*nghost
     f2int = integf(yy2,1./fnorm)                         ;integf,z1,f, f=[dlnab/dy]^2
     yyint = findgen(ny1)/(float(ny1-1))*f2int(ny1-1)     ;xx
     ymnew[nghost:ny-nghost-1L] = intrp1(f2int,yyint,yy2) ;xx,xt,ff ;yn=interpol(y,x,xn)
;add ghostzone
     if (nghost gt 0) then begin
        for j=nghost,0L,-1 do $ ;top constant deriv
           ymnew[nghost-j]=float(j+1)*ymnew[nghost]-float(j)*ymnew[nghost+1]
        for j=ny-nghost,ny-1 do $ ;bottom linear extension
           ymnew[j]=3.*ymnew[j-1]-3.*ymnew[j-2]+ymnew[j-3]
     endif
     if keyword_set(bottom) then for j=ny-nghost,ny-1 do $ ;makes a kink at bot!!!
        ymnew[j]=ymnew[j-1]+(deriv(ym))(j)
     dtop=max([max(dx/deriv(ymnew)),max(deriv(ymnew)/dx)])
     if dtop lt limit then sign=+1 ;change sign -> add +hh
     if shift ne 1 then begin
        if abs(limit-dtop) gt diff1 and abs(limit-dtop) lt 0.3 then begin
           print,'# exit loop'
           goto,ex_glob
        endif
     endif
     diff1=abs(limit-dtop)
     ;print,'# it: '+str(it)+' shift: '+str(shift)+' dtop: '+str(dtop)
     if it eq 100 then begin
        print,'# it gt 100 !!!'
        goto,ex_glob
     endif
     ;if it eq 10 then goto,ex_glob
  endwhile
ex_glob:
  print,'# it: '+str(it)+' shift: '+str(shift)+' dtop: '+str(dtop)
;===============================================================
;plot
  func='!5f = [sqrt(max!dh!n[dlnab/dy]!e-2!n)]!e-1!n'
  if keyword_set(plot) then begin
     erase
plot:
     if !d.name eq 'PS' then !p.charsize=0.7
     !p.noerase=1
     !p.position=[0.1, 0.5, .98, .98] ;[x0, y0, x1, y1]
     plot,yy2,sff2,line=2,xtitle=' ',ytitle=func,$
          xrange=[min([min(ym),min(fnorm)]),max(ym)],yrange=[min(ff2),max(ff2)]
     oplot,yy2,ff2,line=1
     loadct,0,/silent
     oplot,xn,yn,psym=8,co=200
     oplot,xnmid,ynmid,psym=1,sym=3
     oplot,!x.crange,[ynmid(1),ynmid(1)],co=200,line=2
     oplot,xn1,yn1,psym=-8,syms=2,co=200
     oplot,xn3,yn3,psym=-1,co=200,line=1
     loadct,3,/silent
     oplot,yy2,fnorm,co=200,psym=-8,sym=0.5
     oplot,!x.crange,[yn1(1),yn1(1)],line=1
     al_legend,[func,'f!dsmoothed!n','f!dspline!n'],line=[1,2,0],color=[0,0,200]
     !p.position=[0.1, 0.1, .5, .45]
     plot,ymnew,deriv(ymnew),xtitle='!5Depth [Mm]',thick=2,xrange=[min(ym),max(ym)]
     oplot,!x.crange,[mean(deriv(ymnew)),mean(deriv(ymnew))]
     oplot,ym,deriv(ym),line=1
     oplot,!x.crange,[mean(deriv(ym)),mean(deriv(ym))],line=1
     al_legend,['dy!dnew!n','dy!dold!n'],line=[0,1],thick=[2,1]
     al_legend,'<dy>',/right,/bottom
     !p.position=[0.55, 0.1, .98, .45]
     plot,ymnew,dx/deriv(ymnew),thick=2,xtitle='!5Depth [Mm]',$
          xrange=[min(ym),max(ym)],yrange=[0,5]
     oplot,ymnew,deriv(ymnew)/dx,thick=2
     oplot,ym,dx/deriv(ym),line=1
     oplot,ym,deriv(ym)/dx,line=1
     oplot,!x.crange,[4.,4.]
     oplot,!x.crange,[1/4.,1/4.]
     oplot,!x.crange,[3.,3.],line=1
     oplot,!x.crange,[1/3.,1/3.],line=1
     al_legend,'dx/dy aspect ratio',/right
     !p.position=0
     !p.noerase=0
     if !d.name eq 'PS' then !p.charsize=0
;plot in ymnew.eps
     if !d.name eq 'X' then begin
        print,'# plotting into ymnew.eps'
        sd,'ymnew'
        goto,plot
     endif
     if !d.name eq 'PS' then ds
  endif
  if keyword_set(stop) then stop
  return,ymnew
end
