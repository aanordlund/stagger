FUNCTION POLY3_EXPM4_MP, x, p

;+
;  NAME: POLY3_EXPM4_MP
;
;  PURPOSE: evaluate a function defined as a linear combination of 
;  a polynomial and an exponential function of the kind exp(-z^4)
;  where z = z(x) is a linear function.
;
;  The present implementation assumes a 3rd-degree polynomial, hence a
;  function of the form:
;
;  f(x) = - p[0] * exp(-((x-p[1])/p[2])^4) + p[3] + p[4]*x + p[5]*x^2 + p[6]*x^3 
;
;
;  The function is designed for interfacing with Craig
;  Markwardt's MPFIT IDL library for curve fitting.
;
;  (https://www.physics.wisc.edu/~craigm/idl/mpfittut.html)
;
;
;  INPUT:
;    x: array with abscissas
;
;    p: array with parameters
;
;  NOTE: interfacing with the MPFIT library requires that the above
;  variable be named x and p
;
;  OUTPUT:
;
;    f: value of function at x points
;
;
;  KEYWORDS (PARAMETERS):
;
;    None
;
;  ADDITIONAL PARAMETERS AND KEYWORDS:
;
;    None
;
;-

  IdStr = 'POLY3_EXPM4_MP'

; Number of parameters (dimension of p array):
  NPAR = 7L
  
; Size of x and p arrays  
  nx = N_ELEMENTS( x )
  np = N_ELEMENTS( p )

; Sanity check on number of p elements
  IS_NP_VALID = np eq NPAR
  
  if ~IS_NP_VALID then begin
     print, '% '+IdStr+': Dimension of p should be equal to ' + $
            STRCOMPRESS(STRING(NPAR,FORMAT='(I2)'),/REMOVE_ALL) + '. Stop.'
     STOP
  endif

; Evaluate function
  f = p[3] + p[4] * x + p[5] * x^2 + p[6] * x^3 - p[0]*exp(-((x-p[1])/p[2])^4)

; Return result
  RETURN, f

END
