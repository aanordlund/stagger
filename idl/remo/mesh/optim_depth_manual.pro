FUNCTION OPTIM_DEPTH_MANUAL, depth, fdata, $
                             MAX_CELL_ASP_RATIO=max_cell_asp_ratio, $
                             MEAN_CELL_ASP_RATIO=mean_cell_asp_ratio, $
                             DO_PLOT=do_plot, $
                             YNODES=ynodes, FNODES=fnodes

;+
; NAME: OPTIM_DEPTH
;
; PURPOSE: compute new depth scale by fitting proxy for optimal depth scale
;    spacing with model curve (polynomial plus exponential function)
;
;    The function calls the FIT_MODEL_CURVE routine to fit a model
;    curve to the data.
;
; INPUT:
;   depth : original depth scale
;   fdata : proxy for depth scale spacing
;
; OUTPUT:
;   depth_new : optimal depth scale
;
; KEYWORDS:
;   MEAN_CELL_ASP_RATIO : vertical/horizontal aspect ratio of cells in
;     uniform grid
;   MAX_CELL_ASP_RATIO : maximum allowed vertical/horizontal aspect ratio
;     for output grid
;   DO_PLOT : plot results?
;
;   YNODES, FNODES: nodes of spline curve for fitting normalised
;                   fproxy curve as function of normalised depth scale
;   
;
; NOTES:
;   Coded by R. Collet
;-

  IdStr = 'OPTIM_DEPTH_MANUAL'

  NDEP = n_elements(depth)
  NFDATA = n_elements(fdata)

  if NDEP ne NFDATA then begin
     print, '% '+IdStr+$
            ': DEPTH and FDATA array must have the same number of elements. STOP.'
     STOP
  endif

; Vertical/horizontal aspect ratio of grid cells for uniform grid
; Note: if not provided as keyword, assume default value
  if n_elements(MEAN_CELL_ASP_RATIO) eq 0 then MEAN_CELL_ASP_RATIO=0.67

; Maximum allowed vertical/horizontal aspect ratio
  if n_elements(MAX_CELL_ASP_RATIO) eq 0 then MAX_CELL_ASP_RATIO=3.0

; Minimum allowed vertical/horizontal aspect ratio:
; simply, assume it to be the inverse of MAX_CELL_ASP_RATIO
  MIN_CELL_ASP_RATIO = 1. / MAX_CELL_ASP_RATIO

  if n_elements(DO_PLOT) eq 0 then DO_PLOT=0

; Spline nodes (YNODES and FNODES), default value
  if n_elements(YNODES) eq 0 then YNODES = [0., 0.5, 1.]
  if n_elements(FNODES) eq 0 then FNODES = 0.5 * [1., 1., 1.]
  

  
; Parameters:
  epsilon = 1.0e-4              ; minimum allowed value for f
  err = 0.1                     ; relative uncertainty on fdata

  MEDIAN_FILTER_WIDTH = 9L      ; width of median filter for smoothing
  

  if MEAN_CELL_ASP_RATIO lt MIN_CELL_ASP_RATIO or $
     MEAN_CELL_ASP_RATIO gt MAX_CELL_ASP_RATIO then begin
     print, '% '+IdStr+$
            ': Vertical/horizontal aspect ratio of uniform grid '+$
            'is outside allowed limits: return original DEPTH.'
     print, '   MEAN_CELL_ASP_RATIO = ', MEAN_CELL_ASP_RATIO
     print, '   MIN_CELL_ASP_RATIO = ', MIN_CELL_ASP_RATIO
     print, '   MAX_CELL_ASP_RATIO = ', MAX_CELL_ASP_RATIO
     return, depth
  endif

; Fitting

; Min and max values
  min_depth  = min(depth)
  max_depth  = max(depth)
  min_fdata  = min(fdata)
  max_fdata  = max(fdata)
  
; Normalise input data variables to span range 0 ... 1
; Also, apply median filter for smoothing
  depth1 = cgScaleVector(depth, 0., 1.)
  fdata1 = cgScaleVector(MEDIAN(fdata,MEDIAN_FILTER_WIDTH), 0., 1.)

; error array: constant, except at lower and upper ends of interval,
; where it increases gradually
  err1      = err + fltarr(nfdata)
  err1[0:2] = err * [16., 9., 4.]
  err1[nfdata-3:nfdata-1] = err * [4., 9., 16.]

; Manually fit model curve to normalised data
; note: use LINE_NORM from LTOOLS IDL library
  LINE_NORM, depth1, fdata1, dummy, f, ynodes, fnodes, YRANGE=[0., 1.]

  
; Plot
  if DO_PLOT then begin
     cgplot, depth1, fdata1, $
             ps = 'Filled circle', $
             title = IdStr, $
             xtitle = 'Depth [normalised]', $
             ytitle = '$\Delta$ depth proxy [normalised]'
     cgplot, /ov, depth1, f, col='red', thick=2
  endif

; Ensure 0 < f < 1, strictly
  f = (f > epsilon) < (1.-epsilon)

; Max and min values of f
  fmax = max(f)
  fmin = min(f)

; Stretch f to determine optimal delta depth scale
;
; Transform f -> g = b * f + a, so that
; 1) the sum of g[i] values from i=1 to i=n-1 is equal to the x-range
;    d = xdata[n-1] - xdata[0]
;
; and
;
; 2) gmin = b * fmin + a = cmin is equal to the minimum value allowed by
;    aspect ratio: cmin = (mean delta X) * (min aspect ratio) /
;    (actual aspect ratio)
;
; That is
;
; 1) (n-1) * a + b * Sum_i=1^n-1 f[i] = d
; 2) b * fmin + a = cmin
;
; or
;
; a = cmin - b * fmax
; b = (d - cmin * (n-1)) / (Sum_i=1^n-1 f[i] - (n-1) * fmin)
;
; The so-determined values of a and b have to be compatible with the
; aspect ratio constraints for gmax = b * fmax + a, i.e. gmax has to
; be less or equal cmax = (mean delta X) * (max aspect ratio) /
; (actual aspect ratio). If this is not the case, then condition 2)
; has to be replaced with:
;
; 2*) b * fmax + a = cmax


; Compute coefficients for linear system of equations
  depth_range = double(depth[NDEP-1] - depth[0])
  sum_fi = total(f[1:NDEP-1])
  cmin  = depth_range / float(NDEP-1) / MEAN_CELL_ASP_RATIO * MIN_CELL_ASP_RATIO
  cmax  = depth_range / float(NDEP-1) / MEAN_CELL_ASP_RATIO * MAX_CELL_ASP_RATIO

; System of linear equations, solution:
  bcoeff = (depth_range - (NDEP-1)*cmin) / (sum_fi - (NDEP-1)*fmin)
  acoeff = cmin - bcoeff * fmin

; Minimum and maximum values of g (tentative delta_depth)
  gmin = bcoeff * fmin + acoeff
  gmax = bcoeff * fmax + acoeff

  print, '% '+IdStr+': Stretching: checkpoint: GMIN, CMIN = ', gmin, cmin
  print, '% '+IdStr+': Stretching: checkpoint: GMAX, CMAX = ', gmax, cmax

; If gmax exceeds limit imposed by maximum allowed aspect ratio,
; recompute solution imposing aspect ratio constraint on gmax
  if (gmax gt cmax) then begin
     print, '% '+IdStr+ $
            ': Stretching: GMAX exceeds allowed limit: recompute solution'
     bcoeff = (depth_range - (NDEP-1)*cmax) / (sum_fi - (NDEP-1)*fmax)
     acoeff = cmax - bcoeff * fmax
     gmin = bcoeff * fmin + acoeff
     gmax = bcoeff * fmax + acoeff
     print, '% '+IdStr+': Stretching: checkpoint: GMIN, CMIN = ', gmin, cmin
     print, '% '+IdStr+': Stretching: checkpoint: GMAX, CMAX = ', gmax, cmax
  endif


; Compute delta depth scale by applying stretching transformation to f:
; f -> g = b*f +a
  g = bcoeff * f + acoeff
  delta_depth_new = g

; Compute depth scale by integrating delta depth_scale
  depth_new = DBLARR(NDEP)
  depth_new[0] = depth[0]
  for i=1L,NDEP-1 do begin
     depth_new[i] = depth_new[i-1] + delta_depth_new[i]
  endfor

  return, depth_new

END
