pro read_limb, fname_limb, surfi_arr, dir=dir, $
              mxtot=mxtot, mztot=mztot, $
              tsnap=tsnap, nlaml=nlaml, nlimb=nlimb, $
              mu_arr=mu_arr, phi_arr=phi_arr, $
              swap_endian=swap_endian

;+
;  NAME: READ_LIMB
;  PURPOSE: read Stagger-code limb darkening output
;           (reshape_devel version)
;-

  Id = 'READ_LIMB'

; default keywords:
  if n_elements(dir) eq 0 then dir = '.'
  if n_elements(swap_endian) eq 0 then swap_endian = 0
 
; initialize scalar variables
tsnap = 0.
mxtot = 0L
mztot = 0L
nlaml = 0L
nlimb = 0L

ilam = 0L
ilamb= 0L
mu  = 0.
phi = 0.

; cd to file directory; save current directory
cd, dir, current=current_dir

; open and read limb darkening file
openr, lun, /GET_LUN, fname_limb, /F77_UNFORMATTED, SWAP_ENDIAN=swap_endian
readu, lun, tsnap, mxtot, mztot, nlaml, nlimb
; allocate full surface intensity 2D and 2D+angles+bins array 
surfi     = fltarr(mxtot,mztot)
surfi_arr = fltarr(mxtot,mztot,nlimb,nlaml)
mu_arr    = fltarr(nlimb,nlaml)
phi_arr   = fltarr(nlimb,nlaml)
; loop over bins and limb darkening angles
for j=0L,nlaml-1 do begin
;print, 'debug: bin =',j
   for i=0L,nlimb-1 do begin 
      readu, lun, ilam, ilimb, mu, phi, surfi
      surfi_arr[*,*,i,j] = surfi
      mu_arr[i,j] = mu
      phi_arr[i,j]= phi
   endfor
endfor

; close file and exit
free_lun, lun
cd, current_dir
end
