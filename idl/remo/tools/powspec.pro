PRO powspec, f, pow, freq=freq, fft=fft

;+
; NAME: POWSPEC
;
; INPUT:
;   f    = function
;
; OUTPUT:
;   pow  = power spectrum
;   freq = frequencies
;   fft  = fast Fourier transform
;
; NOTES:
;   frequencies are in multiple of Nyquist frequency
;-

  Id = 'powspec'

; number of elements of data set
  nt = n_elements(f)

; Compute Fast Fourier transform and power spectrum
  fft1 = fft(f)
  pow1 = abs(fft1)^2

; freq1 = frequency scale (note: assumed time sampling equal one)
  freq0 = findgen((nt - 1L)/2L) + 1L
  is_n_even = (nt mod 2L) eq 0L
  if (is_n_even) then begin
     freq1 = [0.0, freq0, nt/2L, -nt/2L + freq0]/float(nt)
  endif else begin
     freq1 = [0.0, freq0, -(nt/2L + 1L) + freq0]/float(nt)
  endelse

; sort frequencies in ascending order
indx = sort(freq1)

; sort arrays accordingly for output
freq = freq1[indx]
pow  = pow1[indx]
fft  = fft1[indx]

end
