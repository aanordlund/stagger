pro pmode, f, delt, pow=pow, freq=freq, max_freq=max_freq, $
           t_pmode=t_pmode, t_damp=t_damp, t_scale_factor=t_scale_factor, $
           do_extend=do_extend

  Id = 'pmode'

;+
; NAME: pmode
; PURPOSE: compute frequency of maximum power
;-

if n_elements(t_scale_factor) eq 0 then t_scale_factor = 1.1
if n_elements(do_extend) eq 0 then do_extend = 0

; number of data points
nf = n_elements(f)

; extend dataset with zeros (resolution of relevant frequencies)
if (do_extend) then begin
   nt = 2L^(floor(alog(float(nf))/alog(2.0))+2L)
endif else begin
   nt = nf
endelse

; copy input data to buffer
fbuf = fltarr(nt)
fbuf[0:nf-1] = f
t = findgen(nt)*delt

; compute power spectrum
powspec, fbuf, pow, freq=freq, fft=fft

; scale frequencies by 1/delt
freq = freq / delt

; find where frequencies are non-negative
ww = where(freq ge 0., nww)

; find maximum power (pow_max) and its location (max_freq)
pow_max = max(pow[ww],loc)
ind = array_indices(freq[ww],loc)
max_freq = freq[ww[ind]]

; computed period of dominant cycle
t_pmode = 1. / max_freq

; damping period for Stagger code
t_damp = t_pmode / 2.0 / !pi * t_scale_factor

end
