pro read_st, a, sim, num, $
             nsnap=nsnap, $
             fext=fext, $
             fname=fname, $
             fmesh=fmesh, $
             dir=dir, $
             swap_endian=swap_endian, $
             tabfile=tabfile, $
             r=r, $
             px=px, $
             py=py, $
             pz=pz, $
             e=e, $
             tt=tt, $
             im=im, $
             lnabr=lnabr, $
             lnab5=lnab5, $
             lnpp=lnpp, $
             lgtaur=lgtaur, $
             lgtau5=lgtau5, $
             ss=ss, $
             xm=xm, $
             ym=ym, $
             zm=zm, $
             nx=nx, $
             ny=ny, $
             nz=nz

;+
; NAME: read_st
; PURPOSE: open Stagger snapshot and mesh and read basic variables
; NOTES: coded by R. Collet
;-

  Id = 'read_st'

  @common_cunits


; some default values:
  if n_elements(swap_endian) eq 0 then swap_endian = 0
  if n_elements(dir) eq 0 then dir = '.'
  if n_elements(tabfile) eq 0 then tabfile='EOSrhoe.tab'

; main starts here:
  cd, dir, current=current_dir

; if passed as optional arguments, check whether fname or fmesh exist
  if n_elements(fname) gt 0 then begin
     is_fname = file_test(fname)
  endif else begin
     is_fname = 0
  endelse

  if n_elements(fmesh) gt 0 then begin
     is_fmesh = file_test(fmesh)
  endif else begin
     is_fmesh = 0
  endelse

; if necessary, construct fname from sim, num, nsnap and fext
  if not is_fname then begin
     if n_elements(nsnap) eq 0 then begin
; assume is scratch file
        ssnap = ''
        if n_elements(fext) eq 0 then fext = '.scr'
     endif else begin
; assume is dat file
        if nsnap lt 0 then print, Id+': warning: nsnap negative',nsnap
        ssnap = '_'+string(nsnap,format='(I05)')
        if n_elements(fext) eq 0 then fext = '.dat'
     endelse
     fname = sim+num+ssnap+fext
  endif

; if necessary, construct fmesh
  if not is_fmesh then begin
     fmesh = sim+num+'.msh'
  endif

  print, Id+': fname = ',fname
  print, Id+': fmesh = ',fmesh


; open and read mesh
  readmesh, fmesh, $
            swap_endian=swap_endian, $
            xmesh=xm, $
            ymesh=ym, $
            zmesh=zm

  nx = n_elements(xm)
  ny = n_elements(ym)
  nz = n_elements(zm)

  print, Id+': nx ny nz = ', nx, ny, nz

; open fname and establish pointer to file
  if n_elements(a) gt 0 then begin
     fs = fstat(a)
     if fs.open then free_lun, fs.unit
  endif

  openr, /get, unit, fname, $
         swap_endian=swap_endian

  a = assoc(unit, fltarr(nx,ny,nz))

; read requested variables
  if arg_present(r) then begin
     r = a[0]
  endif

  if arg_present(px) then begin
     px = a[1]
  endif

  if arg_present(py) then begin
     py = a[2]
  endif

  if arg_present(pz) then begin
     pz = a[3]
  endif

  if arg_present(e) then begin
     e = a[4]
  endif

  if arg_present(tt) then begin
     tt = a[5]
  endif

  if arg_present(im) then begin
     im = reform(a[*,0,*,5])
  endif

; variables from look-up table
  do_lookup =  arg_present(lnabr) or $
               arg_present(lnab5) or $
               arg_present(lgtaur) or $
               arg_present(lgtau5) or $
               arg_present(lnpp) or $
               arg_present(ss)

  if do_lookup then begin

     eostab,tabfile

; check whether r and e exist
     if isa(r) eq 0 then r = a[0]
     if isa(e) eq 0 then e = a[4]
     lnr = alog(r)
     ee = e/r

     if arg_present(lnabr) or arg_present(lgtaur) then begin
        lnabr = lookup_eos(lnr+alog(uur),alog(ee*uue),1) - alog(uua)
     endif

     if arg_present(lnab5) or arg_present(lgtau5) then begin
        lnab5 = lookup_eos(lnr+alog(uur),alog(ee*uue),5) - alog(uua)
     endif

     if arg_present(lgtaur) then begin
        lgtaur = alog10( tau_calc(lnabr,ym,vertical=1,taumin=0.) )
     endif

     if arg_present(lgtau5) then begin
        lgtau5 = alog10( tau_calc(lnab5,ym,vertical=1,taumin=0.) )
     endif

     if arg_present(lnpp) then begin
        lnpp = lookup_eos(lnr+alog(uur),alog(ee*uue),0) - alog(uup)
     endif

     if arg_present(ss) then begin
        eosstab
        ss = lookup_eos(lnr+alog(uur),alog(ee*uue),6) / uue
     endif

  endif

  cd, current_dir
end
