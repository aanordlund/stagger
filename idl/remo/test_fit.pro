; script: test spline fitting procedure

; Synthetic profile: directory and file name
dir = '~/Desktop/test_fit'

; File with test data: contains tabulated y values as a function of x
; and initial guess for the x and y values of the spline nodes
data_file = 'test_data3.idl'

; Parameter: epsilon = minimum allowed f value
epsilon = 1.0e-4

; Relative error on Y data
err = 0.01


; Maximum allowed vertical/horizontal aspect ratio.
MAX_CELL_ASP_RATIO = 3.0

; Minimum allowed vertical/horizontal aspect ratio
; Note: assume it here to simply be the inverse of the maximum allowed
; aspect ratio 
MIN_CELL_ASP_RATIO = 1. / MAX_CELL_ASP_RATIO


; Restore the data
RESTORE, dir+'/'+data_file, /VERBOSE
ndata = n_elements(ydata)

; Min and max values
min_xdata = min(xdata)
max_xdata = max(xdata)
min_ydata = min(ydata)
max_ydata = max(ydata)


; Rename variables, for clarity
depth  = xdata                  ; depth scale (input)
fdata  = ydata                  ; proxy for delta depth scale (input)
NDEP   = ndata

; Min and max values
min_depth  = min(depth)
max_depth  = max(depth)
min_fdata  = min(fdata)
max_fdata  = max(fdata)

; Vertical/horizontal aspect ratio of grid cells for uniform grid
; Note: this should be computed in real case
MEAN_CELL_ASP_RATIO = 0.67


; Main script starts here

; Save current directory location
cd, current=current_dir

; Normalise input data variables to span range 0 ... 1
depth1 = cgScaleVector(depth, 0., 1.)
fdata1 = cgScaleVector(fdata, 0., 1.)

; Actual fitting starts here
p = []
FIT_POLY_EXPM, depth1, fdata1, f, p=p, err=err

; Plot
cgplot, depth1, fdata1, ps='filled circle', $
        title=IdStr, xtitle='Depth [normalised]', ytitle='$\Delta$ depth proxy [normalised]'
cgplot, /ov, depth1, f, col='red', thick=2

; Ensure 0 < f < 1, strictly
f = (f > epsilon) < (1.-epsilon)

; Max and min values of f
fmax = max(f)
fmin = min(f)


; Stretch f to determine optimal delta depth scale
;
; Transform f -> g = b * f + a, so that
; 1) the sum of g[i] values from i=1 to i=n-1 is equal to the x-range
;    d = xdata[n-1] - xdata[0]
;
; and
;
; 2) gmin = b * fmin + a = cmin is equal to the minimum value allowed by
;    aspect ratio: cmin = (mean delta X) * (min aspect ratio) /
;    (actual aspect ratio)
;
; That is
;
; 1) (n-1) * a + b * Sum_i=1^n-1 f[i] = d
; 2) b * fmin + a = cmin
;
; or
;
; a = cmin - b * fmax
; b = (d - cmin * (n-1)) / (Sum_i=1^n-1 f[i] - (n-1) * fmin)
;
; The so-determined values of a and b have to be compatible with the
; aspect ratio constraints for gmax = b * fmax + a, i.e. gmax has to
; be less or equal cmax = (mean delta X) * (max aspect ratio) /
; (actual aspect ratio). If this is not the case, then condition 2)
; has to be replaced with:
;
; 2*) b * fmax + a = cmax


; Compute coefficients for linear system of equations
depth_range = double(depth[NDEP-1] - depth[0])
sum_fi = total(f[1:NDEP-1])
cmin  = depth_range / float(NDEP-1) / MEAN_CELL_ASP_RATIO * MIN_CELL_ASP_RATIO
cmax  = depth_range / float(NDEP-1) / MEAN_CELL_ASP_RATIO * MAX_CELL_ASP_RATIO

; System of linear equations, solution:
bcoeff = (depth_range - (NDEP-1)*cmin) / (sum_fi - (NDEP-1)*fmin)
acoeff = cmin - bcoeff * fmin

; Minimum and maximum values of g (tentative delta_depth)
gmin = bcoeff * fmin + acoeff
gmax = bcoeff * fmax + acoeff

print, 'GMIN, CMIN = ', gmin, cmin
print, 'GMAX, CMAX = ', gmax, cmax

; If gmax exceeds limit imposed by maximum allowed aspect ratio,
; recompute solution imposing aspect ratio constraint on gmax
if (gmax gt cmax) then begin
   print, 'Recompute solution'
   bcoeff = (depth_range - (NDEP-1)*cmax) / (sum_fi - (NDEP-1)*fmax)
   acoeff = cmax - bcoeff * fmax
   gmin = bcoeff * fmin + acoeff
   gmax = bcoeff * fmax + acoeff
   print, 'GMIN, CMIN = ', gmin, cmin
   print, 'GMAX, CMAX = ', gmax, cmax
endif


; Compute delta depth scale by applying stretching transformation to f:
; f -> g = b*f +a
g = bcoeff * f + acoeff
new_delta_depth = g

; Compute depth scale by integrating delta depth_scale
new_depth = DBLARR(NDEP)
new_depth[0] = depth[0]
for i=1L,NDEP-1 do begin
   new_depth[i] = new_depth[i-1] + new_delta_depth[i]
endfor


; Return to original directory
cd, current_dir
END
