; script: histograms of tau at various depths

DO_RELOAD = 1

SWAP_ENDIAN = 1

main_dir = '~/Desktop'

; simulation labels
sim = 't50g20m00'
num = '02'
ver = 'w02'

; simulation scratch file, mesh file, and dir
fname = sim+num+'.scr'
fmesh = sim+num+'.msh'
dir = main_dir+'/'+sim+'/'+ver

; EOS table
tabfile = 'EOSrhoe.tab'

; histogram: parameters
min_hist = -10.
max_hist =  10.
bin  = 0.05
nbins= (max_hist - min_hist)/bin+1
bins = min_hist + findgen(nbins)*bin

cd, current=current_dir

; reload simulation?
if (DO_RELOAD) then begin
   cd, dir
   open,a,fname,mesh=fmesh,SWAP_ENDIAN=SWAP_ENDIAN
   eostab, tabfile
   r  = a[0]
   e  = a[4]
   tt = a[5]
   lnr= alog(r)
   ee = e/r
   lnab = lookup_eos(lnr+alog(uur), alog(ee)+alog(uue), 1, 0) - alog(uua)
   tau = tau_calc(lnab, ym, vertical=1, taumin=0.)
   lgtau = alog10(tau)
   cd, current_dir
endif

; compute histogram: loop over depth index
hc_arr  = fltarr(nbins,ny)
hist_arr= fltarr(nbins,ny)
lgtau_median_arr = fltarr(ny)

for iy=0L,ny-1 do begin
   arr = reform(lgtau[*,iy,*])
   hist = histogram(arr, min=min_hist,bin=bin,max=max_hist)
   hist_arr[*,iy] = hist
   hc = total(hist,/cumulative)/total(hist)
   hc_arr[*,iy] = hc
   lgtau_median_arr[iy] = median(arr)
endfor

; print some statistical properties
for iy=0L,ny-1 do begin
   hc = reform(hc_arr[*,iy])
   print,iy,ym[iy],$
         max(bins(where(hc le 0.1))),$
         max(bins(where(hc le 0.2))),$
         max(bins(where(hc le 0.5))),$
         max(bins(where(hc le 0.8))),$
         max(bins(where(hc le 0.9)))
endfor

cd, current_dir
END
