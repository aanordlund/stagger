FUNCTION csound_eos, lnr, ee, gamma, lnp=lnp

;+
; NAME: CSOUND_EOS  
;
; PURPOSE: computes sound speed (and gamma index) using EOSrhoe.tab
; table 
;
; NOTES:
;   dE = P/rho*dlnrho for S=const, so
;   gamma = (dlnpg/dlnrho)_E + P/rho/E (dlnpg/dlnE)_{lnrho}.
;-
  
  @common_cunits

  lnee= alog(ee)
  lnp = lookup_eos(lnr+alog(uur),lnee+alog(uue),0,0) - alog(uup)
  p_r  = exp(lnp-lnr)

  dlnpdlnee = lookup_eos(lnr+alog(uur),lnee+alog(uue),0,1)
  dlnpdlnr  = lookup_eos(lnr+alog(uur),lnee+alog(uue),0,2)

  gamma = dlnpdlnr + p_r/ee * dlnpdlnee

  RETURN, sqrt(gamma*p_r)

END
