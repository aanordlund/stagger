PRO smake_eos, ss, lnr, ee, $
               DIR=dir, $
               TABFILE=tabfile,  $
               EEINI=eeini, $
               ITMAX=ITMAX, EPS=eps, $
               UUR=uur, UUL=uul, UUT=uut, $
               DEBUG=debug

;+
;  NAME: SMAKE_EOS
;
;  PURPOSE: compute internal energy per unit mass by iterative
;  corrections (via Newton-Raphson method) so that it is consistent
;  with the input density and entropy.
; 
;  NOTES: assumes input variables in internal units;
;         reloads EOS table internally and adds entropy to it;
;
;  Coded by RC, 2017-05-11
;-
  
@common_ceostab
  
  IdStr = 'SMAKE_EOS'

; default values:
  if N_ELEMENTS(TABFILE) eq 0 then tabfile = 'EOSrhoe.tab'
  if N_ELEMENTS(DIR) eq 0 then dir ='.'
  if N_ELEMENTS(EEINI) eq 0 then eeini = 2.0
  if N_ELEMENTS(ITMAX) eq 0 then ITMAX = 15
  if N_ELEMENTS(EPS) eq 0 then eps = 1.0e-4
  if N_ELEMENTS(DEBUG) eq 0 then DEBUG = 0

; default conversion factors, internal to table units (cgs)
  if N_ELEMENTS(UUR) eq 0 then uur = 1.0e-7
  if N_ELEMENTS(UUL) eq 0 then uul = 1.0e8
  if N_ELEMENTS(UUT) eq 0 then uut = 1.0e2
  
; conversion factors, velocity and energy/mass:
  uuv = uul / uut
  uue = uuv * uuv
  uup = uue * uur

  
; main program starts here
  cd, current=current_dir

; load EOS table;
  cd, dir
  eostab, tabfile
; append entropy entry at end of EOS table
  eosStab
  ivar = NTBVAR-1
  cd, current_dir
  
; check number of elements in ss, lnr and ee array
  nss = N_ELEMENTS(ss)
  nlnr = N_ELEMENTS(lnr)
  nee = N_ELEMENTS(ee)
  if (nee eq 0) then ee = fltarr(nss) + eeini
  if (nss ne nlnr or nss ne nee) then begin
     print, '% '+IdStr+': ss, lnr, and ee must all have the same number of elements!'
     STOP
  endif

; minimum value of entropy in table, internal units
  SSMIN_TABLE = min(tab[*,*,0,NTBVAR-1])/uue
  
; Newton-Raphson iterative solution

; initialise loop
  rho = exp(lnr)
  ee1 = ee
  IS_ERR_LARGE = 1
  it = 0

  while (it lt ITMAX and IS_ERR_LARGE) do begin  

; look up entropy
     lnee1 = alog(ee1) 
     ss1 = lookup_eos(lnr+alog(uur),lnee1+alog(uue), ivar)/uue ; note: do not exp

; entropy derivatives
     pp = exp(lookup_eos(lnr+alog(uur),lnee1+alog(uue), 0))/uup ; pressure
     tt = exp(lookup_eos(lnr+alog(uur),lnee1+alog(uue), 2))     ; temperature
     dssdee = 1./tt
     dssdlnr = -pp/tt/rho
;     dssdlnee = lookup_eos(lnr+alog(uur),lnee1+alog(uue), ivar, 1, /LINEAR) /uue
;     dssdlnr  = lookup_eos(lnr+alog(uur),lnee1+alog(uue), ivar, 2, /LINEAR) /uue
;     dssdee = dssdlnee / ee1 

; compute correction to ee1 to improve solution
     dss = ss1 - ss
     dee = -dss / dssdee
     deemax = 1.0
     dee = dee/(1.+abs(dee/deemax))
     ee1 = ee1 + dee

; Error:
; use absolute difference between current value and previous iteration
     err = max(abs(dss))
;; use relative difference?
;     err = max(abs(dss/(ss-SSMIN_TABLE))) 
     if KEYWORD_SET( DEBUG ) then $
        print, format='(A,A,X,I3,A,X,E11.5)', '% '+IdStr+': DEBUG: ','it =',it,',  err =',err
     IS_ERR_LARGE = err ge eps

     it = it + 1     
  endwhile

; copy ee1 to ee for output
  ee = ee1
  
; has the solution converged?
  if (err gt eps) then $
     print, '% '+IdStr+': Warning! Not converged! err, eps =', err, eps

  RETURN
  
END
