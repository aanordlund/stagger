PRO pmake_eos,lnpg,lnr,ee,debug=debug,plot=plot,itmax=itmax,eps=eps

;+
;  NAME: PMAKE_EOS
;
;  PURPOSE: compute internal energy per unit mass by iterative
;  corrections (via Newton-Raphson method) so that it is consistent
;  with the input density and pressure.
; 
;  NOTES: assumes input variables in internal units;
;         EOS table must be loaded first (eostab, tablefile).
;
;  Coded by RC
;-
    
@common_cunits

   default,itmax,15
   default,eps,1e-4

   if keyword_set(plot) then plottablim

   npg  = n_elements(lnpg)

   if n_elements(ee) eq 0 then ee=fltarr(npg)

   w    = where(ee eq 0,nw)

   for it=0,itmax do begin

     lnr_art = lnr+alog(uur)
     lne_art = alog(ee)+alog(uue)
     lnpg1 = lookup_eos(lnr_art,lne_art,0)-alog(uup)
     dlnpg1dlne = lookup_eos(lnr_art,lne_art,0,1)
     dlnpg1dlnr = lookup_eos(lnr_art,lne_art,0,2)
     dlnpgdee   = dlnpg1dlne/ee

     dif        =  lnpg1-lnpg
     dee        = -dif/dlnpgdee
     deemax     =  1.0
     ;dee        =  dee/(1.+abs(dee/deemax))
     ee         =  ee+dee
     
     if keyword_set(plot) then oplot,exp(lnr),ee
     if keyword_set(debug) then print, max(abs(dif))

     err        = max(abs(dif)/lnpg)
     if (err lt eps) then return

   endfor

   print,'Warning! Not converged:', err, eps

END
