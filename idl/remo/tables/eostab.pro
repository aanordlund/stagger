;
;   Program to load and possibly plot a table of EOS and opacity quantities
;   used for the convection simulations. The bulk of the table, contained
;   in 'tab', is organized as an Netab*Nrtab*3*Ntbvar array of the Ntbvar
;   variables; lnPtot, ln\kap_Ross, lnT, ln\kap_Planck,ln\kap_5000, and
;   arranged as; the variable, and derivatives w.r.t. lnee, and ln\rho.
;     All table entries are functions of lnee and lnrho which are both
;   equidistant grids, with extent and resolution as given in the header.
;     All units of quantities in the table are simulation units, as given
;   by ul(length), ur(density) and ut(time) w.r.t. cgs-units.
;   N.B. All plots are in cgs-units.
;
;   Arguments (defaults in [...]):
;    tabfile:    Full name of the table file ['EOSrhoe.tab'].
;    PLOT:       Set this keyword to plot all the table entries.
;    QUIET:      Set this keyword to suppress printing of table info.
;    STOP:       Set this keyword to stop before returning, to make all
;                variables available to the user (for debugging).
;                Normally the common-block, ceostab, should suffice.
;
;   Update history:
;   ---------------
;   07.04.2008 Adopted from radtab2/RT
;   08.04.2008 Now also works for Ntbvar=6, i.e., with ln(Ne) included/RT
;   11.04.2008 New SIM-keyword, to plot everything in [sim]-units and ln,
;              not log10/RT
;
;   Known bugs: None
;
pro deflt, var, val
    if n_elements(var) eq 0 then var=val
end

function prompt, stra, ONELINE=ONELINE
    print, stra, form='(A,$)'
    answ=get_kbrd(1)
    if KEYWORD_SET(ONELINE) then begin
        print, string(13b), form='(A0,$)'
    endif else begin
        if ((byte(answ))(0) NE 10) then print,answ  else print,''
    endelse
    return, answ
end

pro eostab, tabfile, PLOT=PLOT, QUIET=QUIET, SIM=SIM, STOP=STOP, _extra=_extra
	common ceostab,Netab,Nrtab,Ma,Na,Ntbvar,Nelem,Nxopt,Nxrev,$
		date,time,lnrmin,lnrmax,lnemin,lnemax,ul,ur,ut,iel,abund,arr,tab
;
; A readonable guess for the name of the table
    deflt, tabfile, 'EOSrhoe.tab'
;
; Open the table file
    openr, lun, tabfile, /GET_LUN, _extra=_extra
;
; Declare all (long) integers before reading
    Netab=0L& Nrtab=0L& Ma=0L& Na=0L& Ntbvar=0L& Nelem=0L
    Nxopt=lonarr(20)& Nxrev=Nxopt
;
; Read the dimensions of the arrays to follow.
    readu, lun, Netab,Nrtab,Ma,Na,Ntbvar,Nelem,Nxopt,Nxrev
;
; Dimension the arrays
	ul    = 0e0 & ur=ul & ut=ul
    arr   = fltarr(Ma,Na)
    iel   = replicate("    ",Nelem)
    abund = fltarr(Nelem)
    date  = "        " & time=date
    tab   = fltarr(Netab,Nrtab,3,Ntbvar)
;
; Read the arrays of the header
    readu, lun, date,time,lnrmin,lnrmax,lnemin,lnemax,ul,ur,ut,iel,abund,arr
;
; Read the bulk of the table, one wavelength/bin at a time
	readu, lun, tab
;
; Close the file
    free_lun, lun
;
; lne and ln\rho of the table
    lnetb = findgen(Netab)/(Netab-1.)*(lnemax-lnemin) + lnemin
    lnrtb = findgen(Nrtab)/(Nrtab-1.)*(lnrmax-lnrmin) + lnrmin
;
; Diagnostic output
	if (NOT KEYWORD_SET(QUIET)) then begin
		print, " Loaded "+tabfile+" computed on "+date+" at "+time+"."
		print, " This is an EOS/opacity table with "+$
				string(Ntbvar,form='(I0)')+" entries."
	endif
;
; Stop before returning, to make all variables available to the user
; Normally the common-block, ceostab, should suffice.
    if KEYWORD_SET(STOP) then stop

    if KEYWORD_SET(PLOT) then begin
;
; Change to base 10 logarithm and cgs units
		ln10  = alog(1e1)
		uE    = (ul/ut)^2
		uP    = ur*uE
		uk    = 1./(ul*ur)
		if KEYWORD_SET(SIM) then begin
; Titles for the axes and dependent variables in the appropriate units.
			etitl = 'ln!7e!3/[!xsim!3]!x'
			rtitl = 'ln!7q!3/[!xsim!3]!x'
			Letb  = lnetb
			Lrtb  = lnrtb
		endif else begin
			etitl = 'log!d10!n!7e!3/[!xcgs!3]!x'
			rtitl = 'log!d10!n!7q!3/[!xcgs!3]!x'
			Letb  = lnetb/ln10 + alog10(uE)
			Lrtb  = lnrtb/ln10 + alog10(ur)
		endelse
;
; Titles for the z-axis, names of variables and character size
		case Ntbvar OF
			5: begin
			names = ['Pressure', 'Rosseland opacity', 'Temperature', $
					 'Planck opacity', '5000'+string(197b)+' opacity']
			ztitl = ['!8p!x!dtot!n', '!7j!x!dRoss!n', $
					 '!8T!x', $
					 '!7j!x!dPlanck!n','!7j!x!d5000!n']
			ulv   = [[alog10([uP, uk, 1, uk, uk])], $
					 [replicate(0,Ntbvar)], [replicate(0,Ntbvar)]]
			   end
			6: begin
			names = ['Pressure', 'Rosseland opacity', 'Temperature', $
					 'Electron density', 'Planck opacity', $
					 '5000'+string(197b)+' opacity']
			ztitl = ['!8p!x!dtot!n', '!7j!x!dRoss!n', $
					 '!8T!x', '!8N!x!de!n', $
					 '!7j!x!dPlanck!n','!7j!x!d5000!n']
			ulv   = [[alog10([uP, uk, 1, 1, uk, uk])], $
					 [replicate(0,Ntbvar)], [replicate(0,Ntbvar)]]
			   end
		else: begin
				print, " ERROR: An Ntbvar other than 5 or 6 is not supported."
				print, "        Bailing out!"
				stop
			  end
		endcase
		dd    = ['', 'd', 'd']
		if KEYWORD_SET(SIM) then begin
			ztitl = 'ln!i !n' + ztitl
			fd = [   1, 1, 1] 
			ulv(*,*) = 0
			dtitl = ['', '/dln!i !n!7e!x', '/dln!i !n!7q!x']
			dname = ['', 'ln ee-', 'ln rho-']
		endif else begin
			ztitl = 'log!d10 !n' + ztitl
			fd = [ln10, 1, 1]
			dtitl = ['', '/dlog!d10 !n!7e!x', '/dlog!d10 !n!7q!x']
			dname = ['', 'log ee-', 'log rho-']
		endelse
        Pcs0        = !P.charsize
        !P.charsize = 1.5*!P.charsize
;
; Loop over quantities, x_j, \eps_j*B_j, \eps_j
		for j=0, Ntbvar-1L do begin
;
; Plot each of the three monocromatic quantities
;
; Loop over wavelength/bin
			for k=0L, 2L do begin
                shade_surf, tab(*,*,k,j)/fd(k)+ulv(j,k), Letb, Lrtb, $
					ztitl=dd(k)+ztitl(j)+dtitl(k), $
                    xtitl=etitl, ytitl=rtitl, xsty=1, ysty=1, zsty=1
;
; Wait for the user before proceeding to the next plot + provide exit
                if (k LT 2L) then begin
                    s = prompt(dname(k+1)+"deriv. of "+names(j)+"? [y/n/q/s]: ")
					if (s EQ 'n'  OR  s EQ 'q'  OR  s EQ 'r') then begin
						reset_graphics& return& endif
					if (s EQ 's')                             then stop
                endif
;
; End of loop over wavelength/bin
            endfor
;
; Wait for the user before proceeding to the next quantity + provide exit
            if (j LT (Ntbvar-1)) then begin
                s = prompt(" Plot "+names(j+1)+"? [y/n/q/s]: ")
				if (s EQ 'n'  OR  s EQ 'q'  OR  s EQ 'r') then begin
					reset_graphics& return& endif
				if (s EQ 's')                             then stop
            endif
;
; End of loop over quantities
        endfor
;
        !P.charsize = Pcs0
    endif
end
