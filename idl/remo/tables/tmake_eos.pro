; $Id: tmake_eos.pro,v 1.3 2016/07/10 20:12:38 remo Exp $
PRO tmake_eos,tt,lnr,ee,debug=debug,plot=plot,itmax=itmax,eps=eps,$
              uur=uur,uul=uul,uut=uut,eeini=eeini
;+
;  Compute, by iterative correction, the energy 'ee' consistent
;  with the input temperature 'tt' and log density 'lnr'.  The
;  table lookup must be activated first, using 'IDL> eostab,'tablefile'.
;-

; default values:
; max number of iterations and accuracy level
  if n_elements(itmax) eq 0 then itmax=15L
  if n_elements(eps) eq 0 then eps=1.0e-4
; conversion factors, internal to table units
; (note: by default, table units are cgs)
  if n_elements(uur) eq 0 then uur = 1.0e-7
  if n_elements(uul) eq 0 then uul = 1.0e8
  if n_elements(uut) eq 0 then uut = 1.0e2
; velocity and internal energy per unit mass:
; conversion factors, internal to table units
; (note: by default, table units are cgs)
  uuv = uul / uut
  uue = uuv * uuv
; initial guess for ee (if ee not given)
  if n_elements(eeini) eq 0 then eeini=2.0e12/uue

; velocity and internal energy per unit mass:
; conversion factors, internal to table units
; (note: by default, table units are cgs)
  uuv = uul / uut
  uue = uuv * uuv

; plot ?
  if keyword_set(plot) then plottablim

  ntt = n_elements(tt)
  if n_elements(ee) eq 0 then ee = fltarr(ntt)+eeini
  w=where(ee eq 0.,nw)

; Iterations: Newton-Raphson method
  for it=0,itmax do begin
; convert lnr and lne to EOS-table units:
     lnr_eos=lnr+alog(uur)
     lne_eos=alog(ee)+alog(uue)
; look up temperature
     lntt1=lookup_eos(lnr_eos,lne_eos,2)
     tt1=exp(lntt1)
; look up derivatives
     dlntt1dlne=lookup_eos(lnr_eos,lne_eos,2,1)
     dlntt1dlnr=lookup_eos(lnr_eos,lne_eos,2,2)
     dttdee=dlntt1dlne*tt1/ee
     dttdlnr=dlntt1dlnr*tt1
     dif=tt1-tt
     dee=-dif/dttdee
     deemax=1.
     dee=dee/(1.+abs(dee/deemax))
     ee=ee+dee
     if keyword_set(plot) then oplot,exp(lnr),ee
     if keyword_set(debug) then print, max(abs(dif))
     err=max(abs(dif)/tt)
     if (err lt eps) then return
  endfor

  print,'warning, not converged:', err, eps
;stop
END
