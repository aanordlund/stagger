FUNCTION gam1_eos, lnr, lnee, lnp=lnp

;+
; NAME: gam1_eos
; 
; PURPOSE: compute adiabatic exponent gamma1 using EOSrhoe.tab
; equation-of-state table; the EOSrhoe.tab file needs to be opened
; with eostab.pro before calling the function.
;
; INPUT:
;   lnr  : ln gas density  (cgs units)
;   lnee : ln internal energy per unit mass (cgs units)
; 
; OUTPUT:
;   gam1 : adiabatic exponent gamma1
; 
; KEYWORDS:
;   lnp : ln gas plus radiation pressure, cgs units (optional OUTPUT)
;
; NOTES: First coded by RC 2013-09-24 
;-

  IdStr = 'GAM1_EOS'

; internal energy
  ee = exp(lnee)

; gas + radiation pressure
  lnp  = lookup_eos(lnr, lnee, 0, 0)


; derivative of ln gas pressure with respect to (ln) internal energy  
  dlnpdlnee = lookup_eos(lnr, lnee, 0, 1)
  dlnpdee   = dlnpdlnee / ee

; derivative of ln gas pressure with respect to ln gas density  
  dlnpdlnr  = lookup_eos(lnr, lnee, 0, 2)

; derivative of internal energy with respect to ln gas density at
; constant entropy
  deedlnr_s = exp(lnp-lnr)
  gamma1    = dlnpdlnr + dlnpdee * deedlnr_s

  return, gamma1

END
