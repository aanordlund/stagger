@stagger_6th

PRO powerb,file,_extra=extra

  open,d,fileroot(file)+'.tim',dim=3,_extra=extra
  open,a,file,nt=nt,nv=nv,_extra=extra
  sz=size(a)
  fit=[2,12]
  comp=0.

  mx=sz[1]

  savefile=fileroot(file)+'-powerb.idlsave'
  if exists(savefile) then begin
    restore,savefile
    if nt-1 le t then return
    nt1=t+1
    pb1=pb
    time1=time
    t1=t
    pb=fltarr(mx/2,nt,3)
    time=fltarr(nt)
    pb[*,0:t1-1,*]=pb1
    time[0:t1-1]=time1
    print,'filling in from '+str(t1)+' to '+str(nt-1)
  end else begin
    pb=fltarr(mx/2,nt,3)
    time=fltarr(nt)
    t1=0
  end

  for t=t1,nt-1 do begin
    for i=0,2 do begin
      bi=a(5+i+nv*t)
      power3d,bi,wa=k,sp=sp,/noplot,/aver,comp=comp
      pb[*,t,i]=sp
    end
    time[t]=d[0,t]
    print,t,time[t]
    save,file,time,t,i,k,pb,comp,file=savefile
  end 

END

