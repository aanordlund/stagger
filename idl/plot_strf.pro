PRO plot_strf, file, it=it
  restore,/ver,file
  sz = size(strf)
  default,it,[3,t-1]
  if sz[0] eq 4 then strf=aver(strf[*,*,*,it[0]:it[1]],4)

  ws,1
  ymin=min(strf,max=ymax)
  xmin=min(strf[*,2,*],max=xmax)
  plot,[xmin,xmax],[ymin,ymax],/xlog,/ylog,/nodata
  for i=0,sz[2]-1 do begin
    x=strf(*,2,0)
    y=strf(*,i,0)
    oplot,x,y
    x=strf(*,2,1)
    y=strf(*,i,1)
    oplot,x,y,line=2
  end

  ws,0
  xmin=min(strf[*,2,*],max=xmax)
  plot,[xmin,xmax],[0,2.5],/xlog,/nodata
  for i=0,sz[2]-1 do begin
    x=alog(strf(*,2,0))
    y=alog(strf(*,i,0))
    oplot,exp(x),deriv(x,y)
    x=alog(strf(*,2,1))
    y=alog(strf(*,i,1))
    oplot,exp(x),deriv(x,y),line=2
  end

  ws,2
  ymin=min(strf,max=ymax)
  xmin=min(dr,max=xmax)
  plot,[xmin,xmax],[0,2.5],/xlog,/nodata
  for i=0,sz[2]-1 do begin
    x=alog(strf(*,2,0))
    y=alog(strf(*,i,0))
    oplot,dr,deriv(alog(dr),y)/deriv(alog(dr),x)
    x=alog(strf(*,2,1))
    y=alog(strf(*,i,1))
    oplot,dr,deriv(alog(dr),y)/deriv(alog(dr),x),line=2
  end

END
