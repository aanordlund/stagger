function spline_derivative,z,f

   sizef=size(f)
   d=make_array(size=sizef)
   mx=sizef[1]
   my=sizef[2]
   mz=sizef[3]
   w1=fltarr(mz)
   w2=fltarr(mz)
   w3=fltarr(mz)
;
; first point
;
   w1[0]=1./(z[1]-z[0])^2
   w3[0]=-1./(z[2]-z[1])^2
   w2[0]=w1[0]+w3[0]
   d[*,*,0]=2.*((f[*,*,1]-f[*,*,0])/(z[1]-z[0])^3 $
           -(f[*,*,2]-f[*,*,1])/(z[2]-z[1])^3)
;
; interior points
;
   w1[1:mz-2]=1./(z[1:mz-2]-z[0:mz-3])
   w3[1:mz-2]=1./(z[2:mz-1]-z[1:mz-2])
   w2[1:mz-2]=2.*(w1[1:mz-2]+w3[1:mz-2])
   for j=0,mx-1 do begin
   for k=0,my-1 do begin
   d[j,k,1:mz-2]=3.*(w3[1:mz-2]^2*(f[j,k,2:mz-1]-f[j,k,1:mz-2]) $
                    +w1[1:mz-2]^2*(f[j,k,1:mz-2]-f[j,k,0:mz-3]))
   endfor
   endfor
;
; last point
;
   w1[mz-1]=1./(z[mz-2]-z[mz-3])^2
   w3[mz-1]=-1./(z[mz-1]-z[mz-2])^2
   w2[mz-1]=w1[mz-1]+w3[mz-1]
   d[*,*,mz-1]=2.*((f[*,*,mz-2]-f[*,*,mz-3])/(z[mz-2]-z[mz-3])^3 $
             -(f[*,*,mz-1]-f[*,*,mz-2])/(z[mz-1]-z[mz-2])^3)
;
; eliminate at first point
;
   c=-w3[0]/w3[1]
   w1[0]=w1[0]+c*w1[1]
   w2[0]=w2[0]+c*w2[1]
   d[*,*,0]=d[*,*,0]+c*d[*,*,1]
   w3[0]=w2[0]
   w2[0]=w1[0]
;
; eliminate at last point
;
   c=-w1[mz-1]/w1[mz-2]
   w2[mz-1]=w2[mz-1]+c*w2[mz-2]
   w3[mz-1]=w3[mz-1]+c*w3[mz-2]
   d[*,*,mz-1]=d[*,*,mz-1]+c*d[*,*,mz-2]
   w1[mz-1]=w2[mz-1]
   w2[mz-1]=w3[mz-1]
;
; eliminate subdiagonal
;
   for k=1,mz-1 do begin
       c=-w1[k]/w2[k-1]
       w2[k]=w2[k]+c*w3[k-1]
       d[*,*,k]=d[*,*,k]+c*d[*,*,k-1]
   endfor
;
; backsubstitute
;
   d[mz-1]=d[mz-1]/w2[mz-1]
   for k=mz-2,0,-1 do begin
       d[*,*,k]=(d[*,*,k]-w3[k]*d[*,*,k+1])/w2[k]
   endfor

   return,d
end

function spline_integral,z,f,I0=I0
   sizef=size(f)
   I=make_array(size=sizef)
   mx=sizef[1]
   my=sizef[2]
   mz=sizef[3]
   dz=fltarr(mz)

   if (keyword_set(I0)) then I[*,*,0]=I0 else I[*,*,0]=0.
   df=spline_derivative(z,f)
   dz[1:mz-1]=z[1:mz-1]-z[0:mz-2]
   for j=0,mx-1 do begin
   for k=0,my-1 do begin
       I[j,k,1:mz-1]=.5*dz[1:mz-1]*(f[j,k,0:mz-2]+f[j,k,1:mz-1]) $
           +(1./12.)*dz[1:mz-1]^2*(df[j,k,0:mz-2]-df[j,k,1:mz-1])
   endfor
   endfor
   for j=1,mz-1 do I(*,*,j)=I(*,*,j)+I(*,*,j-1)

   return,I
end

function opticaldepth,rho,kappa,z,tau0
   f=rho*kappa
   tau=spline_integral(z,f,I0=tau0)
   return,tau
end
