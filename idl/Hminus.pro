; $Id: Hminus.pro,v 1.5 2003/04/20 01:23:43 aake Exp $
; ----------------------------------------------------------------------
FUNCTION opacity, temp, N_e, Pe=Pe
;
;  Absorption coefficient [cm^2/g] for H-, scaled to a value 3e-26 cm^4/dyne
;  at T=6300 K extracted from Fig. 3 of Doughty & Fraser MNRAS 132, 267
;  (1966).  A mean atomic weight of 1.4 (10% helium by number) is assumed.
;  The electron density may alternatively be given in terms of electron
;  pressure.
;
@../idl/cgs
  common copacity, kappa0

  Xi_Hminus = 0.747
  if keyword_set(Pe) then N_e=Pe/(k_boltz*temp)
  if n_elements(kappa0) le 0 then begin
    temp0 = 6300.
    theta0 = q_electron/(k_boltz*temp0)
    Pe0 = 1.
    N_e0 = Pe0/(k_boltz*temp0)
    f_0 = 0.25*((h_planck/m_electron)*(h_planck/(2.*!pi*k_boltz)))^1.5
    f_Hminus0 = f_0*exp(Xi_Hminus*theta0)/temp0^1.5*N_e0
    mu = 1.4
    m_stellar = mu*m_proton
    c0 = 3d-26
    sigma0 = c0/f_Hminus0
    print,'----------------------------------------------------------------'
    print,'H-minus opacity parameters:'
    print,'At a temperature of ',temp0,' K'
    print,'and an electron pressure of ',Pe0,' dyne/cm^2'
    print,'the relative abundance of H- is ', f_Hminus0
    print,'the cross section per H- molecule is',sigma0,' cm^2'
    print,'and the cross section per stellar gram is',c0/m_stellar, ' cm^2/g'
    print,'----------------------------------------------------------------'
    kappa0 = c0/(mu*m_proton*exp(theta0*Xi_Hminus)*theta0^1.5*N_e0)
    theta = q_electron/(k_boltz*temp)
    f_Hminus = 2.07d-16*exp(Xi_Hminus*theta)/temp^1.5*N_e
    kappa = (c0/f_Hminus0)*f_Hminus/(1.4*m_proton)
  end
  theta = q_electron/(k_boltz*temp)
  kappa = kappa0*exp(theta*Xi_Hminus)*theta^1.5*N_e
  return, kappa
END

PRO test_opacity
@../idl/cgs
  T=6300.
  pe=1.
  N_e=pe/(k_B*T)
  print,opacity(T,N_e)
END
