; $Id: split_sink.pro,v 1.8 2010/04/01 17:11:56 aake Exp $

PRO split_sink,file,verbose=verbose,dryrun=dryrun,every=every,_extra=_extra
  default,verbose,1
  default,every,1
  every=long(every)

  sz=fltarr(3)
  s=rsink(u1,file=file,t=t,m=m,l=l,size=sz,array=f,version=version,_extra=_extra)
  help,f
  mass=total(s.m)

  i2=0
  while (1) do begin
    file2='sink_'+str(i2)+'.dat
    if verbose ge 0 then begin
      is = ''
      if keyword_set(dryrun) then is='not '
      print,is+'writing to '+file2
      print,'       n            t           m         mass'
    end
    if not keyword_set(dryrun) then openw,u2,/get,/f77,file2,_extra=_extra

    to=t
    masso=mass
    n=0L
    while (t ge to and mass ge masso) do begin
      if (not keyword_set(dryrun)) and (n mod every) eq 0 then begin
        if version eq 1 then begin
          writeu,u2,version,m,t,m,l
          writeu,u2,f
        end else begin
          writeu,u2,m,l,t,sz
          writeu,u2,f
        end
      end
      n = n+1L
      if n eq 1 then begin
        if verbose gt 0 then print,n,t,m,mass,'  start'
      end else begin
        if verbose eq 2 then print,n,t,m,mass
      end
      if verbose eq 4 then help,m,l,t,sz,f
      to=t
      mo=m
      masso=mass
      if eof(u1) then begin
        if verbose gt 0 then print,n,to,mo,masso,'  end'
        if not keyword_set(dryrun) then free_lun,u2
	return
      end
      s=rsink(u1,file=file,t=t,m=m,l=l,size=sz,array=f,_extra=_extra)
      mass=total(s.m)
      if verbose gt 2 then print,n,t,m,mass,to,masso
    end
    if verbose gt 0 then print,n,to,mo,masso,'  end'
    if not keyword_set(dryrun) then free_lun,u2
    i2=i2+1
  end
END
