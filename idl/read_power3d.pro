FUNCTION read_power3d, isnap, file, xk=xk
  default, file, 'power3d.dat'
  openr, lun, /get, file
  a = assoc(lun,lonarr(1))
  mk = a(0,0)
  a = assoc(lun,fltarr(1+6+7*mk))
  ptot = a(1:6,isnap)
  xk   = a(7+0*mk:6+1*mk,isnap)
  pkxt = a(7+1*mk:6+2*mk,isnap)
  pkyt = a(7+2*mk:6+3*mk,isnap)
  pkzt = a(7+3*mk:6+4*mk,isnap)
  free_lun, lun
  return, pkxt+pkyt+pkzt
END
