;@stagger_atm
;---------------------------------------------------------------------
;+
;  $Id: divb_clean.pro,v 1.5 2009/03/09 11:23:34 aake Exp $
 PRO divb_clean, bx, by, bz, lb=lb, ub=ub, iter=iter, phi=phi, dphi=dphi, plot=plot
;
; Clean the B-field, to remove div(B), by iterating the
; equation Laplace(phi)=div(B) & Bx=Bx-ddxdn(phi) & ...
;-
@common_cdata

default, lb, 5
default, ub, ny-1-lb
default, iter, 5

;  Horizontal wave numbers

kx  = rebin(reform(wavenumbers(nx,dx=dx),nx,1),nx,nz)
kz  = rebin(reform(wavenumbers(nz,dx=dz),1,nz),nx,nz)
kxz = kx^2+kz^2

ii=0

for it=1,iter do begin
  divb = ddxup(bx)+ddyup(by)+ddzup(bz)
  if keyword_set(plot) then begin
    if it eq 1 then plot,hrms(divb,/yv),/yl,yr=[1e-10,1],xr=[lb,ub],xst=1 else oplot,hrms(divb)
    oplot,[lb,lb],[1e-10,1],line=1
    oplot,[ub,ub],[1e-10,1],line=1
    wait,0.
  end
  divb[*,   0:2   ,*]=0.
  divb[*,ny-3:ny-1,*]=0.

;  Compute the horizontal Fourier transform of div(B)
  divbt = complexarr(nx,ny,nz)
  for iy=0,ny-1 do divbt(*,iy,*) = fft(reform(divb(*,iy,*)))

  dphit = complexarr(nx,ny,nz)

  ; iy=ny/2 & image,divb(*,iy,*),ii & ii=ii+1

  w = fltarr(ny,3)
  nn = 0
  for ix=0,nx-1 do begin
   for iz=0,nz-1 do begin
    w[0:lb-1,1] = 1.
    w[ub+1:*,1] = 1.
    b = reform(divbt[ix,*,iz])
    b[0:lb-1] = 0.
    b[ub+1:*] = 0.
    nn = nn + 1

;  Compute matrix elements of the Eq. div(grad(phi)) = div(B)

    for iy=lb,ub do begin
      dy1 = y[iy]-y[iy-1]
      dy2 = y[iy+1]-y[iy]
      dy0 = 0.5*(dy1+dy2)
      w[iy,0] = 1./(dy1*dy0)
      w[iy,2] = 1./(dy2*dy0)
      w[iy,1] = -w[iy,0]-w[iy,2]-kxz[ix,iz]
    end

;  Forward solution

    for iy=lb,ub+1 do begin
      c = w[iy,0]/w[iy-1,1]
      w[iy,1] = w[iy,1]-c*w[iy-1,2]
      b[iy] = b[iy]-c*b[iy-1]
     end

;  Reverse solution

     for iy=ub+1,lb,-1 do begin
       b[iy] = (b[iy]-w[iy,2]*b[iy+1])/w[iy,1]
     end
     dphit[ix,*,iz] = b
   end
  end

  stat,abs(dphit(*,iy,*))

;  print,'dphi=',dphit[100,4,45]

  dphi = fltarr(nx,ny,nz)
  for iy=0,ny-1 do dphi[*,iy,*] = float(fft(/inverse,reform(dphit[*,iy,*])))

  ; iy=ny/2 & image,dphi(*,iy,*),ii & ii=ii+1

  if n_elements(phi) gt 0 then begin
    phi = phi - dphi
    bx = ddxdn(phi)
    by = ddydn(phi)
    bz = ddzdn(phi)
  end else begin
    iy0=3
    iy1=ny-4
    bx[*,iy0:iy1,*] = bx[*,iy0:iy1,*] - (ddxdn(dphi))[*,iy0:iy1,*]
    by[*,iy0:iy1,*] = by[*,iy0:iy1,*] - (ddydn(dphi))[*,iy0:iy1,*]
    bz[*,iy0:iy1,*] = bz[*,iy0:iy1,*] - (ddzdn(dphi))[*,iy0:iy1,*]
  end
end

; iy=ny/2 & image,abs(dphit(*,iy,*))^.2,ii & ii=ii+1

END
