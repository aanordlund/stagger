; $Id: ionization.pro,v 1.6 2003/05/01 16:14:17 aake Exp $

;-----------------------------------------------------------------------
FUNCTION y2z, y
    return,alog((y^2+1e-38)/((1.-y)+1e-8))
END

;-----------------------------------------------------------------------
FUNCTION z2y, zl
    z=exp(zl<40.)
    return, 2.*z/(z+sqrt(z^2+4.*z+1e-35))
END

;-----------------------------------------------------------------------
FUNCTION T2E, TT, rho, y=y
  common cionize,r0,e0,T0,f_He,mu,m_H,k_B
  if n_elements(r0) le 0 then init_eos
  T = (TT/T0)
  z = -alog(rho/r0)-1./T+1.5*alog(T)
  y = z2y(z)
  E = e0*(1.5*T*(1.+f_He+y) + y)
  return,E
END

;-----------------------------------------------------------------------
pro saha,y,r,e,f,df
  ;
  ; We want to find the root of f
  ; r and e are measured in units of r0 and e0 respectively
  ;
  common cionize,r0,e0,T0,f_He,mu,m_H,k_B
  if n_elements(r0) le 0 then init_eos
  T=(2./3.)*(e-y)/(1.+f_He+y)
  dT=-(T+2./3.)/(1.+f_He+y)
  f=1.5*alog(T)-1./T-alog(r)+alog(1.-y)-2.*alog(y)
  df=(dT/T)*(1.5+1./T)-1./(1.-y)-2./y
end

;-----------------------------------------------------------------------
function rtsafe,ylast,r,e,debug=debug
  ;
  ; safe newton-raphson algorithm
  ; r and e are measured in units of r0 and e0 respectively
  ;
  ylim=1d-6
  yacc=1e-3
  maxit=100
  if keyword_set(debug) then begin
    if debug gt 1 then print,'rtsafe:',r,e
  end
  yl=ylim                                  ; lower bound
  yh=1.-ylim                               ; upper bound
  if (yl ge e) then begin
    if keyword_set(debug) then print,r,e,y,'   exit 1'
    return,ylim            ; very low energy
  end
  if (yh ge e) then yh=e-ylim              ; make sure temperature doesn't
                                           ;   become negative
  saha,yl,r,e,fl,df
  if (fl le 0.) then begin
    if keyword_set(debug) then print,r,e,yl,'  exit 2'
    return,yl             ; y equal to zero within
  endif
  saha,yh,r,e,fh,df                        ;   specified accuracy
  if (fh ge 0.) then begin
    if keyword_set(debug) then print,r,e,yh,'  exit 3'
    return,yh             ; y equal to one...
  end

  dyold=yh-yl
  dy=dyold

  if (ylast le yh) then y=ylast else y=yh  ; make sure guess is
                                           ;   not out of bounds
  saha,y,r,e,f,df

  for i=1,maxit do begin                   ; begin iteration loop
    if (((y-yl)*df-f)*((y-yh)*df-f) gt 0. $
        or abs(2.*f) gt abs(dyold*df)) then begin
      dyold=dy
      dy=.5*(yh-yl)
      y=yh-dy
      if (yh eq y) then begin
        if keyword_set(debug) then print,r,e,y,'  exit 4'
        return,y
      end
    endif else begin
      dyold=dy
      dy=f/df
      temp=y
      y=y-dy
      if (temp eq y) then begin
        if keyword_set(debug) then print,r,e,y,'  exit 5'
        return,y
      end
    endelse
    if (abs(dy/y) lt yacc and abs(f) lt 1) then begin
      if keyword_set(debug) then print,r,e,y,'  exit 6
      return,y
    end
    saha,y,r,e,f,df
    if keyword_set(debug) then begin
      if debug gt 1 then print,r,e,y,f,df
    end
    if (f lt 0.) then yh=y else yl=y
  endfor
  print,'error: rtsafe exceeding maximum iterations'
end

;-----------------------------------------------------------------------
function ionfrac,r,e,debug=debug
  ;
  ; root finding loop
  ; r and e are measured in units of r0 and e0 respectively
  ;
  common iteration,niter
  y=0.*r
  ylast=.5
  for i=0L,n_elements(y)-1 do begin
    y[i]=rtsafe(ylast,r[i],e[i],debug=debug)
    ylast=y[i]
  endfor
  return,y
end

;-----------------------------------------------------------------------
pro eos,r,e,P=P,y=y,T=T,debug=debug
  ;
  ; computes ionization fraction, temperature and pressure
  ;   for given mass density and specific energy
  ; all quantities are measured in cgs units
  ;
  common cionize,r0,e0,T0,f_He,mu,m_H,k_B

  if n_elements(r0) le 0 then init_eos
  y=ionfrac(r/r0,e/e0,debug=debug)
  T=T0*(2./3.)*(e/e0-y)/(1.+f_He+y)
  P=(1.+f_He+y)*r*e0*T/T0
end

pro eos_atm,r,ee,P=P,y=y,T=T,debug=debug
  ur=1e-7
  ul=1e8
  ut=1e2
  uee=(ul/ut)^2
  eos,r*ur,ee*uee,P=P,y=y,T=T,debug=debug
end

;-----------------------------------------------------------------------
PRO init_eos

common cionize,r0,e0,T0,f_He,mu,m_H,k_B

m_e =9.109390e-28  ;[g]
m_H =1.673525e-24  ;[g]
k_B =1.380658e-16  ;[erg/K]
hbar=1.054573e-27  ;[erg*s]
eV  =1.602177e-12  ;[erg]
chiH=13.6*eV
f_He=0.1
mu  =1.+4*f_He

; suitable units when dealing with ionization
r0=m_H*mu*((chiH/hbar)*m_e/(2.*!pi*hbar))^1.5  ;[g/cm^3]
e0=chiH/m_H/mu                                 ;[erg/g]
T0=chiH/k_B                                    ;[K]

END

PRO test_eos
;-----------------------------------------------------------------------
;
; main program
;
common cionize,r0,e0,T0,f_He,mu,m_H,k_B
init_eos

mx=100
my=100

; mass density
r_min=1.e-6 & r_max=1.e-1
delta_lnr=alog(r_max/r_min)/mx
r=(r_min*exp(findgen(mx+1)*delta_lnr)) # replicate(1.,mx+1)

; specific energy
e_min=1.e11 & e_max=1.e14
delta_lne=alog(e_max/e_min)/my
e=replicate(1.,my+1) # (e_min*exp(findgen(my+1)*delta_lne))

eos,r,e,P=P,y=y,T=T

;-----------------------------------------------------------------------
TT=7679.
rr=1.36e-8
yy=0.016
ee=t2e(TT,rr,y=yy)
f=1.5*alog(TT/T0)-T0/TT-alog(rr/r0)+alog(1.-yy)-2.*alog(yy)
pe=rr*yy*e0*TT/T0
pg=rr*(1.+f_He+yy)*e0*TT/T0
help,yy,f,pe,pg

end
