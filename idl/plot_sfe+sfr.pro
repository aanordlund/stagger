
default,rho,30.
default,t0,0.

pwd,cwd=head0
root1=fileroot(head0,tail=tail1,head=head1)
root2=fileroot(head1,tail=tail2,head=head2)
root3=fileroot(head2,tail=tail3,head=head3)

title=tail2+tail1+'  SFE'
print,title

if !d.name eq 'PS' then begin
  setdev,'ps',file='sfe+sfr.ps'
  device,bits_per_pixel=1,yoffset=3,ysize=21
end

if fexists('mach.txt') then begin
  !p.multi=[0,1,3]
  tmp=read_table('mach.txt')
  plot,tmp(0,*),tmp(1,*),title=title,xtitle='time/t_ff',ytitle='Mach',yst=0
end else begin
  !p.multi=[0,1,2]
end

s=rsink(/swap_if_b,u,t=t,/reop)
time=[t-t0]
mass=[total(s.m)]
to=t
while not eof(u) do begin
  s=rsink(/swap_if_b,u,t=t)
  if t gt to then begin
    time=[time,t-t0]
    mass=[mass,total(s.m)]
    ;print,t,to,total(s.m)
    to=t
  end
end
time[0]=2.*time[1]-time[2]

if rho gt 0. then tff=sqrt(3.*!pi/(32.*rho)) else tff=1.
time=time/tff

plot,time,100.*mass,title=title,xtitle='time',ytitle='% mass'

title=tail2+tail1+'  SFR, <rho> = '+str(rho)
print,title

;plot,time,deriv(time,mass)/(1.-mass) > 0,title=title,xtitle='time',ytitle='% mass'
plot,time,smooth(deriv(time,mass) > 0,10),title=title,xtitle='time/t_ff',ytitle='SFR/t_ff'

if !d.name eq 'PS' then device,/close

END
