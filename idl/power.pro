@stagger_6th

PRO power,file,_extra=extra

  open,d,fileroot(file)+'.tim',dim=3,_extra=extra
  open,a,file,nt=nt,nv=nv,_extra=extra
  sz=size(a)
  fit=[3,12]
  comp=0.

  mx=sz[1]

  savefile=fileroot(file)+'-power.idlsave'
  if exists(savefile) then begin
    restore,savefile
    if nt-1 le t then return
    nt1=t+1
    p1=p
    time1=time
    t1=t
    p=fltarr(mx/2,nt,3)
    time=fltarr(nt)
    p[*,0:t1-1,*]=p1
    time[0:t1-1]=time1
    print,'filling in from '+str(t1)+' to '+str(nt-1)
  end else begin
    p=fltarr(mx/2,nt,3)
    time=fltarr(nt)
    t1=0
  end

  for t=t1,nt-1 do begin
    lnr=alog(a(nv*t))
    for i=0,2 do begin
      case i of
      0: ui=a(1+i+nv*t)/exp(xdn(alog(lnr)))
      1: ui=a(1+i+nv*t)/exp(ydn(alog(lnr)))
      2: ui=a(1+i+nv*t)/exp(zdn(alog(lnr)))
      2:
      end
      power3d,ui,wa=k,sp=sp,/noplot,/aver,comp=comp
      p[*,t,i]=sp
    end
    time[t]=d[0,t]
    print,t,time[t]
    save,file,t,i,k,p,time,comp,file=savefile
  end 

END

