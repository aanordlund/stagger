default,rho,30.

pwd,cwd=cwd
root=fileroot(cwd,tail=label)

title=label+'  SFR, <rho> = '+str(rho)
print,title

if !d.name eq 'PS' then setdev,'ps',file='sfe.ps'

s=rsink(/swap_if_b,u,t=t,/reop)
time=[t]
mass=[total(s.m)]
to=t
while not eof(u) do begin
  s=rsink(/swap_if_b,u,t=t)
  if t gt to then begin
    time=[time,t]
    mass=[mass,total(s.m)]
    ;print,t,to,total(s.m)
    to=t
  end
end
time[0]=2.*time[1]-time[2]

tff=sqrt(3.*!pi/(32.*rho))
time=time/tff

;plot,time,deriv(time,mass)/(1.-mass) > 0,title=title,xtitle='time',ytitle='% mass'
plot,time,smooth(deriv(time,mass) > 0,10),title=title,xtitle='time',ytitle='% mass'

if !d.name eq 'PS' then device,/close

END
