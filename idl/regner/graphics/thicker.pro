pro thicker, f, cs=cs, thick=thick
	default, cs, f^.3333333333
;The overall charactersize in units of the default.
	!P.charsize  = cs*!P.charsize
;The xyouts charactersize in units of the overall size.
	cs           = 1.0*!P.charsize

	!p.charthick = exp(-f*.45)+f*.65;	to ensure a minimum of 1.
	!p.thick     = f
	!x.thick     = f
	!y.thick     = f
	!z.thick     = f
	thick        =(f*1.5)>2.0
end
