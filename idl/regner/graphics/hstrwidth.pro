;
;   Function to determine the pixel width, w, of Hershey strings.
;
;   Update history:
;   ---------------
;   06.01.2007 Coded/RT
;   27.06.2011 Expanded D to accomodate longer labels, and added warning
;              message if the returned width is the same length as D. Also
;              added keyword DEBUG, to display the content of D./RT
;   03.04.2019 Fixed side-effect on graphics, by saving and restoring system
;              graphics variables around the the bulk o fthis routine./RT
;
;   Known bugs: The text's size in the 'z'-buffer is about 1.35 times larger
;               than on the 'x'-device.
;
function hstrwidth, hstr, cs=cs, height=height, DEBUG=DEBUG, STOP=STOP
    common chstrwidth, ytest0
	default, cs, !P.charsize

	N  = N_ELEMENTS(hstr)
	w  = lonarr(N) & height=w
; Set current state of the graphics sys-variables
    x0 = !x& y0=!y& z0=!z& P0=!P& D0=!D
	set_plot, 'z'
    Nx = 500L & Ny = 50L
	device, set_resolution=[Nx, Ny]
	for i=0L, N-1L do begin
		erase
		xyouts, 0, 0, hstr(i), size=cs, /NORM
		D         = tvrd()
		w(i)      = max(where(aver(D, 2) GT 0))
        if (w(i) GE Nx) then begin
            print, "WARNING: f/n hstrwidth found w(",i,")=",w(i), $
                ">=Nx=",Nx, form='(A0,I0,A0,I0,A0,I0)'
        endif
		height(i) = max(where(aver(D, 1) GT 0))
        if (height(i) GE Ny) then begin
            print, "WARNING: f/n hstrwidth found height(",i,")=",height(i), $
                ">=Ny=",Ny, form='(A0,I0,A0,I0,A0,I0)'
        endif
	endfor
	if KEYWORD_SET(STOP) then stop
	device, /CLOSE
	set_plot, strlowcase(D0.name)
; Reset graphics sys-variables
    !x=x0& !y=y0& !z=z0& !P=P0; !D=D0
;
; Display the content of D if DEBUG is set.
    if KEYWORD_SET(DEBUG) then begin
        if (N_ELEMENTS(ytest0) LE 0L) then ytest0=0L
        tv, D, 0, ytest0*Ny
        ytest0 = ytest0 + 1L
    endif
    if (N EQ 1L) then begin
        height=height(0)
        return, w(0)
    endif else begin
        return, w
    endelse
end
