;
;   Function to return the index of the currently loaded color-table.
;
;   Command arguments (defaults in [...]):
;    ctfile:   Absolute name of the color-table file [colors1.tbl].
;    ntables:  Number of color-tables in that file (output).
;    QUIET:    If this keyword is set, don't print the name of the color-table.
;    DEBUG:    Set to get extra diagnostic output.
;    STOP:     If this keyword is set, stop before returning.
;
;   Update history:
;   ---------------
;   06.04.2004 Coded/RT
;   25.11.2012 Adde keyword DEBUG for extra diagnostic output./RT
;
function currentct, ctfile, ntables=ntables, QUIET=QUIET, DEBUG=DEBUG, STOP=STOP
	common colors, r_orig, g_orig, b_orig, r_curr, g_curr, b_curr

	default, ctfile, filepath('colors1.tbl', subdir=['resource', 'colors'])
;
; If a "loadct" has not been issued yet, assume icurr = 0L (safe assumption).
	if (N_ELEMENTS(r_curr) LE 0) then return, 0L
	ntables = 0b
	openr,lun, ctfile, /block, /GET_LUN
	readu, lun, ntables
;
; Read the color-tables in bulk
	point_lun, lun, 1
	ct = bytarr(256, 3, ntables)
	readu, lun, ct
;
; Read the names of the color-tables
	if (NOT KEYWORD_SET(QUIET)) then begin
		names = bytarr(32, ntables)
;		point_lun, lun, ntables * 768L + 1
		readu, lun, names
		names = strtrim(names, 2)
	endif
	free_lun, lun
;
 	p    = long((findgen(!D.table_size) * 255.) / (!D.table_size-1.))
    pmax = N_ELEMENTS(r_curr)-1L;   !d.table_size-1L
	sigs = fltarr(ntables)
	ctcurr = [[r_curr], [g_curr], [b_curr]]
 	for i=0L, Ntables-1L do sigs(i) = rms(reform(ct(p,*,i))-ctcurr(0:pmax,*))
;	for i=0L, Ntables-1L do sigs(i) = rms(reform(ct(*,*,i))-ctcurr)
	dum = min(sigs, icurr)
    if KEYWORD_SET(DEBUG) then stat, sigs
	if (NOT KEYWORD_SET(QUIET)) then print, icurr, names(icurr), $
							form='(" Using color-table ",I0,": ",A0)'

	if KEYWORD_SET(STOP) then stop
	return, icurr
end
