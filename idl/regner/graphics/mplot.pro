;
;   Program to make multiple plots in one figure, and with shared axis
;   to save space. Uses !P.position for correct positioning of the plots.
;
;   Arguments (defaults in [...]):
;    x, y:  data to plot.
;    xtitle:Titles for x-axis. If /SAMEX, only the first instance of xtitle
;           will be used. Otherwise every instance in a bottom row will be
;           used [''].
;    xrange:Range for x-axis. If /SAMEX, only the first instance of xrange
;           will be used. Otherwise every instance in a bottom row will be
;           used. Defaults to automatic setting based on bottom row [0, 0].
;    xmargin:x-margin around outside of plot collection [!x.margin].
;    ytitle:Titles for y-axis. If /SAMEY, only the first instance of ytitle
;           will be used. Otherwise every instance in a left column will be
;           used [''].
;    yrange:Range for y-axis. If /SAMEY, only the first instance of yrange
;           will be used. Otherwise every instance in a left column will be
;           used. Defaults to automatic setting based on left column [0, 0].
;    ymargin:y-margin around outside of plot collection [!y.margin].
;    SAMEX: Set this keyword to have all columns have the same x-range/title
;    SAMEY: Set this keyword to have all rows have the same y-range/title.
;    _EXTRA:Are passed on to the plot command [].
;    STOP:  Set this keyword to stop before returning, to make all
;           variables available to the user (for debugging).
;
;   Update history:
;   ---------------
;   02.04.2010 Coded/RT
;   19.07.2010 Corrected the vertical order of plotting (-> top to bottom)/RT
;   23.08.2011 Corrected the defaults of x/yrange to be !x/y.range/RT
;   11.10.2011 Corrected behaviour for !P.multi=0 (one plot per page)/RT
;   12.10.2011 Fixed bug: Previously re-used the x-/y-margin variables/RT
;   10.09.2012 Explicitly include xticknames in argument list, to prevent
;              e.g., s/r ticks to override mplot behaviour./RT
;   08.12.2016 Now also works with logarithmic y-axis./RT
;   12.06.2019 Out-commented setting !P.position=0 for img_plot,/OLDPOS to work.
;
;   Known bugs: When (NOT SAMEY) annotates left axis of right-hand-side plot!
;               Too little bottom margin...
;               Needs yticknames argument...
;
pro mplot, x, y, xtitle=xtitle, xrange=xrange, xmargin=xmargin, $
    xticknames=xticknames, ytitle=ytitle, yrange=yrange, ymargin=ymargin, $
    SAMEX=SAMEX, SAMEY=SAMEY, YLOG=YLOG, _EXTRA=extra_args, STOP=STOP

    common cmplot, ipl, Nx, Ny, px0, px1, dpx, py0, py1, dpy, xr, yr, xt, yt, $
                                                                       smx, smy

    default, xrange, !x.range
    default, yrange, !y.range
    default, xtitle,  ''
    default, ytitle,  ''
    default, xticknames, ""

;   !x.style=1  ; make x-axis "exact" for plots
;   !y.style=16 ; do not force zero to be included in the y-range
;   !P.TICKLEN = -0.02
;   !P.TITLE = '!17'

    ipl = !P.multi(0)
    if (ipl EQ 0) then begin
        default, xmargin, !x.margin
        default, ymargin, !y.margin
        Nx  = (!P.multi(1))>1L
        Ny  = (!P.multi(2))>1L
        xmrg= 1.2*xmargin*!D.X_CH_SIZE*!P.charsize
        ymrg= 1.2*ymargin*!D.X_CH_SIZE*!P.charsize
        px0 = xmrg(0)/float(!D.X_VSIZE)
        px1 = (!D.X_VSIZE-xmrg(1))/float(!D.X_VSIZE)
        dpx = (px1-px0)/float(Nx)
        py0 = ymrg(0)/float(!D.Y_VSIZE)
        py1 = (!D.Y_VSIZE-ymrg(1))/float(!D.Y_VSIZE)
        dpy = (py1-py0)/float(Ny)

        xr  = xrange#replicate(1., Nx)
        xt  = strarr(Nx) &  xt(*) = xtitle
        yr  = yrange#replicate(1., Ny)
        yt  = strarr(Ny,2) &  yt(*,*) = ytitle

        if KEYWORD_SET(SAMEX) then  smx=1  else  smx=0
        if KEYWORD_SET(SAMEY) then  smy=1  else  smy=0
        erase
    endif

    ix = ipl mod Nx
    iy = Ny-1L - ipl/Nx

    blanks = replicate(" ", 30)
; Bottom row
    if (iy EQ 0) then begin
        !x.tickname = xticknames
        if (smx) then begin
            xrng   = xr(*,ix)
        endif else begin
; Individual xrange - not set explicitly, it will be set by the plot command. 
            xrng = xrange
; Individual xtitle if explicitly set
            if (xtitle NE '') then xt(ix) = xtitle
        endelse
        xtitl=xt(ix)
; Other rows
    endif else begin
        !x.tickname=blanks
        xtitl=""
        xrng = xr(*,ix)
    endelse
; Left column
    RHS = 0
    if (ix EQ 0  OR  ix EQ (Nx-1L)) then begin
        !y.tickname=""
        if (smy) then begin
            yrng = yr(*,iy)
            if (ix EQ (Nx-1L)  AND  ix NE 0) then begin
                RHS = 1
                !y.tickname=blanks
                ytitl=""
            endif else begin
                ytitl=yt(iy,0)
            endelse
        endif else begin
; Individual yrange - not set explicitly, it will be set by the plot command. 
            yrng = yrange
; Individual ytitle if explicitly set
            if (ix EQ (Nx-1L)  AND  ix NE 0) then begin
                RHS = 1
                if (ytitle NE '') then yt(iy,1) = ytitle
                !y.tickname=blanks
                ytitl=""
            endif else begin
                if (ytitle NE '') then yt(iy,0) = ytitle
                ytitl=yt(iy,0)
            endelse
        endelse
; Other columns
    endif else begin
        !y.tickname=blanks
        ytitl=""
        yrng = yr(*,iy)
    endelse
;   print, ipl, ix, iy, xrng, yrng, form='(3I3, 2(f10.2, f8.2))'
    
    !P.multi = 0
    !P.position = [px0+ix*dpx,py0+iy*dpy,px0+(ix+1)*dpx,py0+(iy+1)*dpy]

    plot, x, y, xr=xrng, yr=yrng, xmarg=0, ymarg=0, $
                xtitl=xtitl, ytitl=ytitl, /NOERASE, YLOG=YLOG, _EXTRA=extra_args
    if KEYWORD_SET(YLOG) then ycr=1e1^!y.crange  else  ycr=!y.crange
    if (RHS) then begin
        !y.tickname=""
        axis, Yaxis=1, ysty=1, yr=ycr, ytitl=yt(iy,1)
    endif

    if (iy EQ 0) then xr(*,ix) = !x.crange
    if (ix EQ 0) then yr(*,iy) = ycr

    ipl = (ipl + 1L) mod (Nx*Ny)
    !P.multi = [ipl, Nx, Ny]
;   !P.position = 0
    !x.tickname=""
    !y.tickname=""

;   reset_graphics
    if KEYWORD_SET(STOP) then stop
end
