pro ticks, xc, tickval, Nticks, Nminor, ticknames, Nmax=Nmax, DEBUG=DEBUG, $
													IDL_DEFAULTS=IDL_DEFAULTS
	if KEYWORD_SET(DEBUG) then !P.COLOR = !D.TABLE_SIZE-1.
	xc = double(xc)
	det = .99999999d0
	dxc = max(xc) - min(xc)
	de = long(alog10(dxc)-det*(dxc LT 1.d0))
	ee = 1-de
	if (!P.MULTI(1) GT 2 OR !P.MULTI(2) GT 2) then chrscale=.5 else chrscale=1.
	xchars = float(chrscale*!P.charsize*!D.X_CH_SIZE)
	Nxplots = float(!P.MULTI(1)>1)
Igen:
	formstr = floatform(xc, dxc, Ndigits=Ndigits, Ndecimals=Ndecimals)
	Nchrs = Ndigits+Ndecimals+0.5*(Ndecimals GT 0)+1.0
	Nmax0 = long((!D.X_VSIZE/Nxplots-xchars*total(!x.margin))/(Nchrs*xchars))<7
	if KEYWORD_SET(DEBUG) then print, 'ticks: ', $
						Nmax0, !D.X_VSIZE/Nxplots, xchars*total(!x.margin)
	default, Nmax, Nmax0
;   Nmax = Nmax < 7
	if KEYWORD_SET(DEBUG) then print, Nmax, Nchrs, ee, Ndigits, Ndecimals

	dtick = 1
	Nticks = long(dxc*1d1^ee)/dtick
	if KEYWORD_SET(DEBUG) then print, Nticks, dtick
	Nminor = 10
	if (Nticks GT Nmax) then begin
		dtick = 2
		Nticks = long(dxc*1d1^ee)/dtick
		if KEYWORD_SET(DEBUG) then print, Nticks, dtick
		Nminor = 4
	endif
	if (Nticks GT Nmax) then begin
		dtick = 5
		Nticks = long(dxc*1d1^ee)/dtick
		if KEYWORD_SET(DEBUG) then print, Nticks, dtick
		Nminor = 5
	endif
	if (Nticks GT Nmax) then begin
		de = de+1
		ee = ee-1
;		!P.COLOR=(!D.TABLE_SIZE-1.)*.8
		goto, Igen
	endif
	tickval = (long(min(xc)*1d1^ee/dtick+det)+dindgen(Nticks+1))/1d1^ee*dtick
	if (max(tickval) GT max(xc)) then begin
		if KEYWORD_SET(DEBUG) then !P.COLOR=(!D.TABLE_SIZE-1.)*.6
		Nticks=Nticks-1>1
		tickval=(long(min(xc)*1d1^ee/dtick+det)+dindgen(Nticks+1))/1d1^ee*dtick
	endif
	formstr = floatform(tickval, Ndigits=Ndigits, Ndecimals=Ndecimals)
	ticknames = string(tickval, form=formstr)
	if KEYWORD_SET(DEBUG) then print, ticknames
	if KEYWORD_SET(IDL_DEFAULTS) then begin
		Nticks =  0
		Nminor =  0
		ticknames = ['']
		tickval = 0
	endif
	if KEYWORD_SET(DEBUG) then print, min(xc), max(xc)
	if KEYWORD_SET(DEBUG) then print, tickval
end
