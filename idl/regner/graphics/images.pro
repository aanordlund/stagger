;
;  Program to display a data-cube as images of each layer.
;
;  Update history
;  ---------------
;  ??.??.19?? Coded/Aake
;  22.10.2015 Added keyword FITWINDOW to select a subset of equally spaced
;             horizontal layers that will all fit in the plot window.
;             Also modernized the formatting of the code./RT
;  11.02.2016 Added argument, markxy, to mark location (x,y) in each image./RT
;
;  Known bugs: None.
;
pro images,f,i0,zrange=zrange, STAT=STAT,YZ=YZ,XZ=XZ,GLOBAL=GLOBAL,GRID=GRID, $
                                FITWINDOW=FITWINDOW, markxy=markxy, STOP=STOP
  s=size(f)
  max=max(f,min=min)
  if KEYWORD_SET(GLOBAL) then  zr=[min,max] else  zr=[0,0]
  if KEYWORD_SET(STAT)   then  stat,f
  if (s(0) EQ 3) then begin
    Nimgs = (!D.X_SIZE/(!im.x + KEYWORD_SET(GRID)))*$
            (!D.Y_SIZE/(!im.y + KEYWORD_SET(GRID)))
    if KEYWORD_SET(FITWINDOW) then begin
      iz = findgen(Nimgs)/(Nimgs-1.)
      if KEYWORD_SET(YZ) then begin
        iz = long(iz*(s(1L)-1.))
      end else if KEYWORD_SET(XZ) then begin
        iz = long(iz*(s(2L)-1.))
      end else begin
        iz = long(iz*(s(3L)-1.))
      end
    endif else begin
      iz=     lindgen(Nimgs)
    end
  endif
  default, zr, zrange
  default, i0, 0L
  if KEYWORD_SET(YZ) then begin
    if (s(0) EQ 3) then begin
      for i=0L,Nimgs-1L do image,zr=zr,f(iz(i),*,*),i+i0*s(3), $
                                            /order,GRID=GRID,markxy=markxy
    end else begin
      for j=0L,2L do $
	    for i=0,s(1)-1 do image,zr=zr,f(i,*,*,j),i+j*s(3)+i0*s(3)*s(4), $
											/order,GRID=GRID,markxy=markxy
    end
  end else if KEYWORD_SET(XZ) then begin
    if (s(0) EQ 3) then begin
      for i=0L,Nimgs-1L do image,zr=zr,f(*,iz(i),*),i+i0*s(3), $
                                            /order,GRID=GRID,markxy=markxy
    end else begin
      for j=0L,2L do $
        for i=0,s(2)-1 do image,zr=zr,f(*,i,*,j),i+j*s(3)+i0*s(3)*s(4),$
											/order,GRID=GRID,markxy=markxy
    end
  end else begin
    if (s(0) EQ 3) then begin
      for i=0L,Nimgs-1L do image,zr=zr,f(*,*,iz(i)),i+i0*s(3), $
                                                   GRID=GRID,markxy=markxy
    end else begin
      for j=0L,2L do $
	for i=0,s(3)-1 do image,zr=zr,f(*,*,i,j),i+j*s(3)+i0*s(3)*s(4), $
                                                   GRID=GRID,markxy=markxy
    end
  end

  if KEYWORD_SET(STOP) then stop
end
