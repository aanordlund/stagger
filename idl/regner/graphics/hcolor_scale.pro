;
;  Program to plot a horixontal color_scale.
;  Arguments:
;    min:         The number that corresponds to the zero color-index.
;    max:         The number that corresponds to the 255 color-index.
;    x0:          x-position of lower left corner of color_scale [0.30]
;    y0:          y-position of lower left corner of color_scale [0.05]
;    length:      Length of the color-scale [5.5cm].
;    charsize:    Character size [!P.CHARSIZE]
;    REVERSE:     Reverse the color-order.
;    CSTEPS:      If set, the number of color-steps in the scale, otherwise
;                 a smooth 256-color scale is used.
;    NCOLORS:     How many colors to use [!D.TABLE_SIZE].
;    STOP:        Set this keyword to stop before returning, to make all
;                 variables available to the user (for debugging).
;
pro hcolor_scale,min,max,x0=x0,y0=y0,length=length, $
        charsize=charsize, TRUECOLOR=TRUECOLOR, REVERSE=REVERSE, CSTEPS=CSTEPS, $
        INTEGER=INTEGER, NCOLORS=NCOLORS, STOP=STOP, $
        _EXTRA=extra_args
	
	xform=['','(I1)','(I2)','(I3)','(I4)','(I5)','(I6)','(I7)','(I8)','(I9)']
	SCALABLE_PIXELS = (!D.flags AND 1)
	default, x0, .30
	default, y0, .05
	if (NOT keyword_set(TRUECOLOR)) then TRUECOLOR = 0
	if (NOT keyword_set(INTEGER)) then INTEGER = 0
    default, NCOLORS, !D.TABLE_SIZE
    if (!P.charsize LE 0.0) then csdef=1.0  else  csdef = !P.charsize
    default, charsize, csdef
	if (n_elements(length) eq 0) then begin
		if (SCALABLE_PIXELS) then $
			length = 5.5*!D.X_PX_CM/float(!D.x_vsize) $
		else $
			length = (!D.TABLE_SIZE-1)/float(!D.x_vsize)
	endif
	if (SCALABLE_PIXELS) then begin
		nx = NCOLORS-1
	endif else begin
		nx = fix(length*!D.x_vsize)
	endelse
	c1 = NCOLORS-2.0 & ca = float(NCOLORS-1)/float(nx)
	ny = fix(nx/10.0)
	xx = [min, max]
	yy = [0, ny-2]
	Ptmp = !P
	xtmp = !x
	ytmp = !y
	!P.region = 0
	!x.title = ''
	!y.title = ''
	isx=ca*findgen(nx)
	isx2 = isx
	if KEYWORD_SET(REVERSE) then for i=0L,nx-1L do isx(i)=isx2(nx-1-i)
	if (N_ELEMENTS(CSTEPS) EQ 1) then begin
		fcol = float(NCOLORS-1)/float(CSTEPS)
		isx = fix(isx/fcol)*fcol
	endif
	if (TRUECOLOR) then $
		color=reform([isx,isx,isx]#replicate(1.,ny),nx,ny,3) $
	else $
		color = isx#replicate(1.,ny)
	ixform = max([fix(alog10(max([min,max])))+1,0])*INTEGER
	if (SCALABLE_PIXELS) then begin
		as = float(!D.y_vsize)/!D.x_vsize
		height = length/8.3/as
		dx = (10.*(!x.thick/2)+5.)/float(!D.x_vsize)
		!P.position=[x0+dx, y0, length+x0, height+y0]
		plot, xx, yy, xstyle=9, ystyle=5, ticklen=-.1, /NOERASE, $
					/NORMAL, /NODATA, xr=[min,max], $
					chars=charsize, _EXTRA=extra_args, $
                    xtickformat=xform(ixform);, xchars=1.25, 
		tv, color, x0, y0, xsize=length, ysize=height, /NORMAL, TRUE=3*TRUECOLOR
	endif else begin
		cx0 = x0*!D.x_vsize & cy0 = y0*!D.y_vsize
		!P.position=[cx0+fix(!x.thick)/2, cy0, nx+cx0-(!x.thick LT 2), ny+cy0]
		plot, xx, yy, xstyle=9, ystyle=5, ticklen=-.18, /NOERASE, $
					/DEVICE, /NODATA, xr=[min,max], xtickformat=xform(ixform), $
					chars=charsize, _EXTRA=extra_args;, xchars=1.25
		tv, color, cx0, cy0, TRUE=3*TRUECOLOR
	endelse

    if KEYWORD_SET(STOP) then stop
	!P = Ptmp
	!x = xtmp
	!y = ytmp
end
