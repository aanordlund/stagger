;
;   Program to construct a color-scale that adapts to the data. Specifically
;   have a discontinuity at the zero-point of the data, and the two parts
;   of the color-scale being stretched to the extremes of the data.
;
;   Arguments (defaults in [...]):
;    var:     The data that the color-scale applies to - any array of numbers.
;    di:      Off-set the black bar from the zero-point [0].
;    EQUAL:   Set this to put the center of the color-scale at the center
;             of the data (instead of at the zero-point of the data).
;    NOBLACK: Set this to avoid the black bar at the zero-point.
;    DOPLOT:  Set this to plot the R,G and B values of the color-scale.
;    BARS:    Set this to add 25 dark bars, centered on the zero-point, to
;             form contours in plots.
;    STOP:    Set this keyword to stop before returning, to make all
;             variables available to the user (for debugging).
;
;   Update history:
;   ---------------
;   17.02.2004  Coded/RT
;   13.08.2014  Added option /REDBLUE for a red-white-blue scale./RT
;
function cup, x, LINEAR=LINEAR
    if KEYWORD_SET(LINEAR) then begin
        return, x
    endif else begin
        return, sin(!pi/2.*x)
    endelse
end

function cdn, x, LINEAR=LINEAR
    if KEYWORD_SET(LINEAR) then begin
        return, 1.-x
    endif else begin
        return, cos(!pi/2.*x)
    endelse
end

pro color, var, di=di, EQUAL=EQUAL, NOBLACK=NOBLACK, DOPLOT=DOPLOT, $
            BARS=BARS, LINEAR=LINEAR, REDBLUE=REDBLUE, STOP=STOP
;
; Construct color table
;    [min - 0] = [blue - green]
;    [0] = [black]
;    [0 - max] = [yellow - red]
	common colors, r_orig, g_orig, b_orig, r_curr, g_curr, b_curr
;
	default, di, [0]
;
; Get min and max
;
	if n_elements(var) gt 0 then begin
		vmin=min(var)
		vmax=max(var)
		if keyword_set(EQUAL) then begin
			vmin=vmin < (-vmax)
			vmax=vmax > (-vmin)
		end
	end else begin
		vmin=-1.
		vmax=1.
	end

	N_colors=!D.TABLE_SIZE < 256L
	top=N_colors-1L
; Get zero pixel
    NOZERO = ((vmin*vmax GE 0.0) OR KEYWORD_SET(EQUAL))
	if (NOZERO) then begin;   zero-not included in data-range
		pix0=top/2
	endif else begin
		pix0=(bytscl([vmin,0.,vmax],top=top))(1)
	endelse
	pix0=max([pix0,4]) & pix0=min([pix0,top-3L])
; R,G,B color arrays
; End points: 0:Nb-1=black,top-Nb+1:top=white
	Nb = 1L
	black=fltarr(Nb)
	white=black + top
  if KEYWORD_SET(REDBLUE) then begin
; Blue branch
    Nb = pix0
    x  = findgen(Nb)/((Nb-1.) > 1.)
    r  = [0., top*cup(x, /LINEAR)]
    g  = [0., top*cup(x, /LINEAR)]
    b  = [0., replicate(top, Nb)]
; Red branch
    Nr = top-Nb-1L
    x  = findgen(Nr)/((Nr-1.) > 1.)
    r  = [r, replicate(top, Nr)]
    g  = [g, top*cdn(x, /LINEAR)]
    b  = [b, top*cdn(x, /LINEAR)]
  endif else begin
; Red channel
;  purple
    Np = pix0/2
    x  = findgen(Np)/((Np-1.) > 1.)
;   r  = [0., .6*top*cdn(x,     LINEAR=LINEAR)]
    g  = [0., replicate(0, Np)]
    b  = [0.,    top*cup((x+0.3)/1.3, LINEAR=LINEAR)]
    r  = [0.,    top*cup((x+0.3)/1.3, LINEAR=LINEAR)*(1.-x)*1.68]
;  blue
;  green
    Ng = pix0 - Np
    x  = findgen(Ng)/((Ng-1.) > 1.)
    r  = [r, replicate(0, Ng)]
    g  = [g, top*cup(x, LINEAR=LINEAR)^.8]
    b  = [b, top*cdn(x, LINEAR=LINEAR)]
;  yellow
    Ny = fix((top - pix0)/3.5)
    x  = findgen(Ny)/((Ny-1.) > 1.)
    if (NOZERO) then  iw=0.0  else  iw=0.8
    r  = [r, top*(iw+.13) + top*(1.-iw-.13)*cup(x, LINEAR=LINEAR)]
    g  = [g, replicate(top, Ny)]
    b  = [b, iw*top*cdn(x, LINEAR=LINEAR)]
;  red
    Nr = fix((top - (pix0 + Ny))/2.)
    x  = findgen(Nr)/((Nr-1.) > 1.)
    r  = [r, replicate(top, Nr)]
    g  = [g, top*cdn(x, LINEAR=LINEAR)]
    b  = [b, replicate(0,   Nr)]
;  infra red
    Ni = top - (pix0 + Ny + Nr) - 1L
    x  = 0.8*findgen(Ni)/((Ni-1.) > 1.)
    r  = [r, top*cdn(x, LINEAR=LINEAR), top]
    g  = [g, replicate(0,   Ni), top]
    b  = [b, replicate(0,   Ni), top]
;   help, r, g, b
  endelse
;
; Zero level
	if ((NOT KEYWORD_SET(NOBLACK)) AND (NOT NOZERO)) then begin
        r(pix0+di)=0 & g(pix0+di)=0 & b(pix0+di)=0
	endif
; Construct 25 darker bars, centered on the zero-point of the data.
    if KEYWORD_SET(BARS) then begin
        ib = lindgen(25)*10L + (fix(pix0*1./top*255) mod 10L)
        ib2= long(reform(transpose([[ib],[ib+1L]]), 2*25L)*1./255.*top)
        r(ib2) = r(ib2)*0.8
        g(ib2) = g(ib2)*0.8
        b(ib2) = b(ib2)*0.8
    endif
	r = fix(r*255./top)
	g = fix(g*255./top)
	b = fix(b*255./top)
;
;  Load color table
	tvlct,r,g,b
;  Save in table
	r_orig=(r_curr=r)
	g_orig=(g_curr=g)
	b_orig=(b_curr=b)
;
	if KEYWORD_SET(DOPLOT) then begin
		plot,  r
		oplot, r, col=top-Nb
		oplot, b, col=Nb+N0-1
		oplot, g, col=pix0-1
		for i=0, top do oplot, [1,1]*i, !y.crange, line=1
		color_scale, vmin, vmax, height=.5, x0=.15
	endif
	if KEYWORD_SET(STOP) then stop
;
	return
end
