;
;   Update history:
;   ---------------
;   23.03.2001 Coded/RT
;   15.01.2008 Ensure that y2style supresses drawing of the second y-axis/RT
;   24.07.2013 Added argument xrange./RT
;   26.06.2014 Added argument y2crange for returning the actual range of y2./RT
;   05.05.2016 Added argument _EXTRA=extra_args, fixed xsty=1->5 in 2nd plot/RT
;
;   Known bugs: None
;
PRO plot2,x,y1,y2,xtitle=xtitle,y1title=y1title,y2title=y2title,title=title, $
			line1=line1,line2=line2,p1symbol=p1symbol,p2symbol=p2symbol, $
			sym1size=sym1size,sym2size=sym2size,y1range=y1range, $
			y2range=y2range,y1style=y1style,y2style=y2style,charsize=charsize, $
			y1tickformat=y1tickformat,y2tickformat=y2tickformat, $
    		xmargin=xmargin,xticks=xticks,xtickv=xtickv,xminor=xminor, $
			xtickformat=xtickformat,xstyle=xstyle,col1=col1,col2=col2, $
			y1level=y1level, y2level=y2level, xrange=xrange,y2crange=y2crange, $
            _EXTRA=extra_args
;
;  overplot two variables with each their axes
;

default, xtitle,		''
default, y1title,		''
default, y2title,		''
default, y1style,		10
default, y2style,		22
default, y1tickformat,	''
default, y2tickformat,	''
default, title,			''
default, line1,			0
default, line2,			2
default, p1symbol,		0
default, p2symbol,		0
default, sym1size,		1
default, sym2size,		1
default, y1range,		0
default, y2range,		0
default, charsize,		1.0
default, xstyle,		!x.style
 default, xmargin,		[11,8]
 default, xticks,		0
 default, xtickv,		''
 default, xminor,		0
default, xtickformat,	''
default, col1,			!P.color
default, col2,			!P.color
default, xrange,        [min(x, max=mxx), mxx]

;print, 'xr(plot2): ', xrange

cs  = charsize*!P.charsize
yr0 = !y.range
if (NOT (y1style AND 8)/8) then y1style=y1style + 8L
!y.range = y1range
plot,x,y1,yst=y1style,line=line1,psym=p1symbol,syms=sym1size,chars=cs, $
     title=title,ytitle=y1title,ytickformat=y1tickformat,xtickform=xtickformat,$
     xmargin=xmargin, xticks=xticks,xtickv=xtickv,xminor=xminor, $
	 xtitle=xtitle, xstyle=xstyle, xrange=xrange, _EXTRA=extra_args
if (N_ELEMENTS(y1level) GT 0L) then oplot, !x.crange, [1,1]*y1level, line=1
Pvar = !P
xvar = !x
yvar = !y
!y.range = y2range
PM = !P.MULTI
!P.MULTI(0) = (PM(0)-1+PM(1)*PM(2)) mod (PM(1)*PM(2))
if ((y2style AND 4) EQ 0) then y2style = y2style + 4
plot,x,y2,/NOERASE,yst=y2style,xmarg=xmargin,chars=cs,xsty=5,xr=!x.crange,/NODAT
y2crange = !y.crange
if (N_ELEMENTS(col2) GT 1) then begin
	plots, x,y2,line=line2,psym=p2symbol,syms=sym2size,col=col2
endif else begin
	oplot, x,y2,line=line2,psym=p2symbol,syms=sym2size,col=col2
endelse
if (N_ELEMENTS(y2level) GT 0L) then oplot, !x.crange, [1,1]*y2level, line=1
;print, !y.crange
axis,yaxis=1,ystyle=17,ytitle=y2title,yrange=!y.crange,chars=cs, $
														ytickform=y2tickformat

!P = Pvar
!x = xvar
!y = yvar
!y.range = yr0
return
end
