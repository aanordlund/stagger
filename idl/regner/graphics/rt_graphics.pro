;
;   Program to set various graphics variables according to my defaults.
;   Uses the common-block crt_graphics for the original values, which
;   are used by s/r reset_graphics.
;
;   Update history:
;   ---------------
;   12.03.2010 Coded/RT
;
;   Known bugs: None
;
pro rt_graphics, STOP=STOP
    common crt_graphics, subseq, xsty0, ysty0, Ptickl0, Ptitl0, Pchars0, cmode0

    defsysv, '!p_save', exists=exsysv
    if (NOT exsysv) then begin
        if (N_ELEMENTS(subseq) LT 1) then begin
            xsty0   = !X.style
            ysty0   = !Y.style
            Ptickl0 = !P.ticklen
            Ptitl0  = !P.title
            if (Ptitl0 EQ '')  then Ptitl0='!3'
            Pchars0 = !P.charsize
            device, get_decomposed=cmode0
            subseq  = 1
        endif
        !X.style    =   1
        !Y.style    =  16
        !P.ticklen  = -0.02
        !P.title    = '!17'
        !P.charsize =  1.8
        device, decomposed=0
    endif

    if KEYWORD_SET(STOP) then stop
end
