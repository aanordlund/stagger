pro plot_arrow,p1, p2, thick=thick, linestyle=line, point_size=point_size, $
	color=color,angle=angle,fill=fill
;  plots arrow from point p1 = [x1, y1] to point p2 = [x2, y2]
;  frame must be set up beforehand
;  Size of point is given fraction of x-size

;  angle gives half-angle of point; default is pi/4
;  If fill is set, point is set as a filled triangle.

xsz = abs(!x.crange(1) - !x.crange(0))

if p1(0) eq p2(0) and p1(1) eq p2(1) then begin
	print,'Error. points coincident in plot_arrow'
	return
endif

if keyword_set(thick) then cthick = thick else cthick = 1
if keyword_set(line) then cline = line else cline = 0
if keyword_set(point_size) then cpoint = point_size else cpoint = 0.02
if keyword_set(angle) then cangle=angle else cangle=!pi/4.

if keyword_set(color) then begin
  if color eq -1 then ccolor=0 else ccolor=color
endif

if keyword_set(color) then $
  plots,[p1(0),p2(0)],[p1(1),p2(1)],thick=cthick,linestyle=cline, $
    color=ccolor $
else $
  plots,[p1(0),p2(0)],[p1(1),p2(1)],thick=cthick,linestyle=cline

;  set coordinates of point in device system

pd12 = convert_coord([p1(0),p2(0)],[p1(1),p2(1)],/to_device)
pd1 = [pd12(0,0),pd12(1,0)]
pd2 = [pd12(0,1),pd12(1,1)]

thtd = atan(pd2(1)-pd1(1),pd2(0)-pd1(0))
del = cpoint*!d.x_vsize
pdta = pd2 + del*[cos(thtd+!pi-cangle),sin(thtd+!pi-cangle)]
pdtb = pd2 + del*[cos(thtd-!pi+cangle),sin(thtd-!pi+cangle)]

; convert back to data coordinates

ptab = convert_coord([pdta(0),pdtb(0)],[pdta(1),pdtb(1)],/device,/to_data)

pta=[ptab(0,0),ptab(1,0)]
ptb=[ptab(0,1),ptab(1,1)]

if keyword_set(color) then begin
  if keyword_set(fill) then begin
    polyfill,[pta(0),p2(0),ptb(0)],[pta(1),p2(1),ptb(1)],color=ccolor
  endif else begin
    plots,[p2(0),pta(0)],[p2(1),pta(1)],thick=cthick,color=ccolor
    plots,[p2(0),ptb(0)],[p2(1),ptb(1)],thick=cthick,color=ccolor
  endelse
endif else begin
  if keyword_set(fill) then begin
    polyfill,[pta(0),p2(0),ptb(0)],[pta(1),p2(1),ptb(1)]
  endif else begin
    plots,[p2(0),pta(0)],[p2(1),pta(1)],thick=cthick
    plots,[p2(0),ptb(0)],[p2(1),ptb(1)],thick=cthick
  endelse
endelse

return
end
