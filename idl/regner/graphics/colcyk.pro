;
;  Program to cycle through the color tables.
;  Arguments:
;    t:   time/[s] between succesive color-tables.
;         Interactive version if t is not set.
;
pro colcyk, t
	answ = ''
	cnames = ''
	loadct, GET_NAMES=cnames
	cmax = N_ELEMENTS(cnames)-1L
	if (n_elements(t) eq 0) then begin
		print, 'Use [</>/s/0-9]'
		i = 0
		STOP = 0
		while (NOT STOP) do begin
			print, i, format='(I3, $)'
			loadct, i
			ss=get_kbrd(1)& s2=get_kbrd(0)& s3=get_kbrd(0)
			direction = (byte(s3))(0)
			if (direction EQ 65b)			then i=(i+1)<cmax
			if (direction EQ 67b)			then i=(i+1)<cmax
			if (direction EQ 66b)			then i=(i-1)>0
			if (direction EQ 68b)			then i=(i-1)>0
			if (ss LE '9'  AND  ss GE '0')	then i=long(ss)
			if (strlowcase(ss) EQ 's')		then STOP=1
			if (strlowcase(ss) EQ 'q')		then STOP=1
			if (strlowcase(ss) EQ 'x')		then STOP=1
		endwhile
	endif else begin
		for i=0, cmax do begin
			print, i, format='($,I3)'
			loadct, i
			wait, t
		endfor
		loadct, 0
	endelse
end
