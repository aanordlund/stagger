;
;   Program to compute major- (with annotation) and minor- (without) tickmarks
;   for a logarithmic x-axis. This routine is written specifically for
;   a temperature scale, hence the notation, but it should be generally
;   useful. The choise of tickmarks is aimed at making the annotation legible.
;
;   Update history:
;   ---------------
;   10.03.2010 Coded/RT
;   21.08.2014 Multiplied xtl0 by 1.6 to uncrowd the T-axis in hr.pro./RT
;
;   Known bugs: Assuming reversed T-scale.
;
pro logticks, xrange, Ttickv, Ttickm, tickform, STOP=STOP
    default, tickform, '(I0)'

    Tmin =       xrange(1)  &  Tmax=       xrange(0)
;    LTwidth=alog10(Tmax)-alog10(Tmin)
;
; Set-up the major tickmarks
    tickints = reform([ 1, 2, 4, 5, 10]#(1e1^(findgen(4)-1.)), 20)
    minticks = reform([10, 5, 5, 4, 10]#replicate(1., 4), 20)
    xtl0 = strlen(string(Tmax, form=tickform))*1.6
    LT0  = alog10(Tmax)
;    Tc   = Tmin
    LTc  = alog10(Tmin)
    LTwidth=LT0 - LTc
;    xtlc = strlen(string(Tc, form=tickform))
;    dLTxt= xtlc*!P.charsize*!D.X_CH_SIZE/!D.x_vsize*$
;                                (!x.window(1)-!x.window(0))*LTwidth
;    dTxt = 2*(1e1^(LTc + dLTxt*.5) - Tc)
    LT2  = fix(LTc)
    Ttickv = [-1] & Ttickm = [-1]
    while (LT2 LT fix(LT0)) do begin  
        LT2   = LT2 + 1.
;
; The width in logT at end of this decade
        dLTxt = (LT2+1)*!P.charsize*!D.X_CH_SIZE/!D.x_vsize*$
                                (!x.window(1)-!x.window(0))*LTwidth
        dTxt = 2*(1e1^(LT2 + dLTxt*.5) - 1e1^LT2)
        itckint = max(where(1e1^LT2/tickints GT dTxt))
        tickint = tickints(itckint)
        Ttcks=    round(1e1^LT2*(1.-1./tickint*findgen(tickint)))
        ii = where(Ttcks GE Tmin  AND  Ttcks LE Tmax)
        if (ii(0) GE 0) then $
            Ttickv = [Ttickv, reverse(Ttcks(ii))]
;   
; Set-up the minor tickmarks
        mintick = minticks(itckint)*tickint
        mTtcks= round(1e1^LT2*(1.-1./mintick*findgen(mintick)))
        ii = where(mTtcks GE Tmin  AND  mTtcks LE Tmax)
;       print, "tickint: ", tickint, mintick
        if (ii(0) GE 0) then $
            Ttickm = [Ttickm, reverse(mTtcks(ii))]
    endwhile
    dLTxt = (xtl0+.7)*!P.charsize*!D.X_CH_SIZE/!D.x_vsize*$
                                (!x.window(1)-!x.window(0))*LTwidth
    dTxt = 2*(1e1^(LT0 + dLTxt*.5) - 1e1^LT0)
    itckint = max(where(1e1^(fix(LT0)+1.)/tickints GT dTxt))
    tickint = tickints(itckint)
    Ttcks=    round(1e1^(fix(LT0)+1.)*(1.-1./tickint*findgen(tickint)))
    ii = where(Ttcks GE Tmin  AND  Ttcks LE Tmax)
    if (ii(0) GE 0) then $
        Ttickv = [Ttickv, reverse(Ttcks(ii))]
;
; Set-up the minor tickmarks
    mintick = minticks(itckint)*tickint
    mTtcks = round(1e1^(fix(LT0)+1.)*(1.-1./mintick*findgen(mintick)))
    ii = where(mTtcks GE Tmin  AND  mTtcks LE Tmax)
;   print, "tickint: ", tickint, mintick
    if (ii(0) GE 0) then $
        Ttickm = [Ttickm, reverse(mTtcks(ii))]
    Ttickv = Ttickv(1:*)
    Nticks = N_ELEMENTS(Ttickv) - 1L
    Ttickm = Ttickm(1:*)

    if KEYWORD_SET(STOP) then stop
end
