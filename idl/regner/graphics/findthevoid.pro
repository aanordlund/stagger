;
;   Function to find parts of a plot with less data. Needs to be issued
;   after the plot statement, so that !x and !y are established.
;   Usefull for positioning labels.
;   Returns top left coordinate of emptiest section.
;
;   Arguments (defaults in [...]):
;    STOP:    Set this keyword to stop before returning, to make all
;             variables available to the user (for debugging).
;
;   Update history:
;   ---------------
;   13.09.2017 Coded/RT
;
;   Known bugs: None
;
function findthevoid, x, y, Nx, Ny, DEBUG=DEBUG, STOP=STOP
    default, Nx, 2L
    default, Ny, 2L

    xr = !x.crange
    x1 = (x-xr(0L))/(xr(1L)-xr(0L))*float(Nx)
    yr = !y.crange
    y1 = (y-yr(0L))/(yr(1L)-yr(0L))*float(Ny)
    d  = fltarr(Nx, Ny)
    for i=0L, Nx-1L do begin
        for j=0L, NY-1L do begin
            i0 = i+0.& i5=i+.5& i1=i+1.
            j0 = j+0.& j5=j+.5& j1=j+1.
            ii = where(x1 GE i0  AND  x1 LT i1  AND  y1 GE j0  AND  y1 LT j1)
            if (ii(0L) GE 0L) then begin
;               d(i,j)=aver(sqrt((x1(ii)-i5)^2 + (y1(ii)-j5)^2))
                d(i,j)=aver(abs(x1(ii)-i5) + abs(y1(ii)-j5))
            endif else begin
                d(i,j)=1e1
            endelse
        endfor
    endfor
    dum = max(d, imx)& ijmx = array_indices(d, imx)
    if KEYWORD_SET(DEBGU) then print, reverse(d, 2L)
    xy  = [.15+ijmx(0L)/float(Nx), (ijmx(1L)+1.)/float(Ny)-0.06]

    if KEYWORD_SET(STOP) then stop
    return, xy
end
