;+
;  Program to label line-styles and plotting symbols on a plot.
;  Arguments:
;    x:            Lower left corner of label in plot-window normalized
;                  coordinates. The right-side end of the line-segment is at x.
;    y:            Lower left corner of label in plot-window normalized
;                  coordinates. The bottom-line of the text is located at y.
;    line:         Line-style to label - ignored if psym>0.
;    textin:       Text to label the line-style or symbol.`
;    psym:         Symbol to label - connected with lines if psym<0 [0].
;    size:         relative text size [1.0].
;    color:        color-index of the line/symbol [!P.COLOR].
;    txtcolor:     color-index of the line/symbol [!P.COLOR].
;    symsize:      Relative size of symbols [!P.SYMSIZE].
;    Nsym:         The number of symbols to plot if psym>0 [3L].
;    thick:        Line-thickness [!P.THICK]
;    rightjustify: text at this normalized position - otherwise left justify.
;    width:        The width of the line-segment in normalized coordinates[0.1].
;    clmn:         Enable easy handling of several columns of line-styles,
;                  starting with column=0L which is also the default [0L].
;    cblank:       The index of the bkank-out color (see below) [!P.background].
;    BLANK:        Set this keyword to blank out the text-field before writing.
;    ONESYM:       When psym<>0 plot the full line, but only one symbol.
;    HEADER:       Set to write a header over the line segment of subsequent
;                  labels. Use same lx and ly=ly0+0.7*dly.
;    ERRORBARS:    Set this keyword to add error-bars to the symbols/line-
;                  segment being labeled.
;    STOP:         Set this keyword to stop before returning, to make all
;                  variables available to the user (for debugging).
;-
;  Update history:
;  ---------------
;  07.07.1998 Coded/Regner Trampedach
;  14.01.2008 Rewrote coordinate transformations, in terms of !x/y.window,
;             and plotting is now done in normalized coordinates with one
;             'plots' statement. Added, align, as an argument/RT
;  23.01.2017 Added keyword HEADER, to make header above subsequent labels/RT
;  30.01.2017 Added argument txtcolor./RT
;  15.09.2017 Moved the position arguments into a new common-block, clabel,
;             to automate position calculation, simplify call sequence, and
;             enable different routines to contribute to the same label
;             sequence. This is backwards compatible, so accepts x,y args./RT
;  24.10.2017 More robust determination whether using common block (new) or
;             explicitly given coords. (old) through type of 2nd argument./RT
;  17.08.2018 Added keyword to add error-bars to symbols/line-segment./RT
;  20.05.2019 Added keyword LBLANK, to include the line-segment in the
;             blanking of the background, including a margin to the left./RT
;  11.09.2019 Added argument zin, and keyword T3D in order to be able to use
;             this routine in 3D plots. Also added _EXTRA=extra_args./RT
;  05.03.2020 Added argument Nsym for choosing the number of symbols to
;             include in the line-segment, when psym>0./RT
;
;  Known bugs: None
;
pro label,xin,yin,linein,textin,psym=psym,symsize=symsize,color=color, $
            rightjustify=rightjustify, thick=thick, align=align, size=size, $
            width=width, cblank=cblank, txtcolor=txtcolor, clmn=clmn,Nsym=Nsym,$
            BLANK=BLANK, ONESYM=ONESYM, HEADER=HEADER, NOADVANCE=NOADVANCE, $
            LBLANK=LBLANK, ERRORBARS=ERRORBARS, DEBUG=DEBUG, T3D=T3D, zin=zin,$
            _EXTRA=extra_args, STOP=STOP
    common clabel, lx, ly0, dly, il

	if (N_PARAMS() EQ 0L) then begin
		print, " Usage:"
		print, " label, x, y, line, text[, psym=psym][, size=size][, color=color][, symsize=symsize]"
		print, "    [, rightjustify=rightjustify][, thick=thick][, align=align]"
		print, "    [, width=width][, clmn=clmn]"
		print, " For further information:  IDL> doc_library, 'label'"
		return
	endif
    if KEYWORD_SET(DEBUG) then print, 'label: ', size(y, /TYPE)
	if (size(yin, /TYPE) EQ 7L) then begin
;  The coordinates of the label are given implicitly through the common block.
;  The label counter, il, is also advanced.
        line=xin& text=yin& x=lx
        if KEYWORD_SET(HEADER) then begin
            if (il EQ 0L) then begin;   Header at the beginning of a list
                y=ly0+.7*dly
            endif else begin;           Header in the middle of a list
                y=ly0-il*dly& il=il+1L
            endelse
        endif else begin
            y=ly0-il*dly& if (NOT KEYWORD_SET(NOADVANCE)) then il=il+1L
        endelse
    endif else begin
        x=xin& y=yin& line=linein& text = textin
    endelse
    if KEYWORD_SET(DEBUG) then print, "label: ", x, y, line, "  ", text

; Label a plot at normalized coordinates (within plot window), (x,y), with
; line type and text
	default, psym,		0
	default, size,		1.0
	default, symsize,	!P.SYMSIZE
    default, Nsym,      3L
	default, thick,		!P.THICK
	default, color,		!P.COLOR
	default, txtcolor,	!P.COLOR
	default, width,     0.1
    default, clmn,    0L
    if (Nsym EQ 1L) then ONESYM=1b
    if (line GT 50L) then width=0.0
    x = x + clmn*(width+0.02)
	if (N_ELEMENTS(rightjustify) GT 0) then begin
		default, align, 1.0
	endif else begin
		default, align, 0.0
		rightjustify  = x+0.02
	endelse
;
;  x-coordinates of text and line
	xw  = !x.window(1) - !x.window(0)
	xlb = !x.window(0) + xw*rightjustify
	xl1 = !x.window(0) + xw*x
	xl2 = !x.window(0) + xw*(x-width)
;
;  y-coordinates of text and line
	yw  = !y.window(1) - !y.window(0)
	ylb = !y.window(0) + yw*y
	yli = !y.window(0) + yw*(y+0.008)
;
;  Text size
    lbsize = .9*size*!P.charsize
;
;  Blank out the text space, if so requested
    if (KEYWORD_SET(BLANK)  OR  KEYWORD_SET(LBLANK)) then begin
        default, cblank, !P.background
        wlb =(hstrwidth(text, cs=lbsize, height=hlb)/1.33+2)/float(!D.x_vsize)
        hlb = hlb/float(!D.y_vsize)
        xmrg= width*0.07
        if KEYWORD_SET(LBLANK) then xbl0 = xl2-xmrg  $
                               else xbl0 = xlb-xmrg
        polyfill, [xbl0,xlb+wlb, xlb+wlb,xbl0,xbl0], $
                  [ylb, ylb, ylb+hlb, ylb+hlb, ylb]-.1*hlb, col=cblank, /NORMAL
    endif
;
; If this is a header to subsequent labels, then center the text above the line
    if KEYWORD_SET(HEADER) then begin
        xyouts,.5*(xl1+xl2),ylb,text,size=lbsize, align=0.5, /NORMAL, $
                               color=txtcolor, T3D=T3D, z=zin, _EXTRA=extra_args
    endif else begin
;
;  Output the text and line
        xyouts,xlb,ylb,text,size=lbsize, align=align, /NORMAL, color=txtcolor, $
                                               T3D=T3D, z=zin, _EXTRA=extra_args
        if (width GT 0.0) then begin
            if KEYWORD_SET(ONESYM) then begin
                plots,.5*(xl1+xl2),yli,line=line,psym=psym,color=color,$
                    symsize=symsize,thick=thick, /NORMAL, T3D=T3D, z=zin
                psym=0
                if KEYWORD_SET(ERRORBARS) then begin
                    xy1= convert_coord(.5*(xl1+xl2),yli-yw*0.015,/NORM,/TO_DATA)
                    xy2= convert_coord(.5*(xl1+xl2),yli+yw*0.015,/NORM,/TO_DATA)
                    errplot,xy1(0,*),xy1(1,*),xy2(1,*),color=color,thick=thick,$
                                                            T3D=T3D,z=zin
                endif
            endif
            xlis = findgen(Nsym>2L)/(Nsym>2L - 1.)*(xl2-xl1) + xl1
            ylis = replicate(yli,Nsym>2L)
            plots, xlis, ylis, line=line,psym=psym, $
                color=color, symsize=symsize,thick=thick, /NORMAL, T3D=T3D,z=zin
            if KEYWORD_SET(ERRORBARS) then begin
                xy1 = convert_coord(xlis, ylis-yw*0.015,/NORM,/TO_DATA)
                xy2 = convert_coord(xlis, ylis+yw*0.015,/NORM,/TO_DATA)
                errplot,xy1(0,*),xy1(1,*),xy2(1,*),color=color,thick=thick, $
                                                            T3D=T3D,z=zin
            endif
        endif
    endelse
    if KEYWORD_SET(STOP) then stop
end
