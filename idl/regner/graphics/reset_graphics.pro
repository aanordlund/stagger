pro reset_graphics
    common crt_graphics, subseq, xsty0, ysty0, Ptickl0, Ptitl0, Pchars0, cmode0

	defsysv, '!p_save', exists=exsysv
	if (NOT exsysv) then begin
;		print, 'The appropriate sysvars are not available for s/r ' + $
;				'reset_graphics to work optimally.'
;       print, 'Performing rudimentary reset."
        !X.style    = xsty0
        !Y.style    = ysty0
        !P.ticklen  = Ptickl0
        !P.title    = Ptitl0
        !P.charsize = Pchars0
        !P.multi    = 0
        device, decomposed=cmode0
        if (strlowcase(!D.NAME) EQ 'x') then device, set_graphics_function=3
		return
	endif
	resetsysvars
end
