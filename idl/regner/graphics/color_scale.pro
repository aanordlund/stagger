;
;  Program to plot a vertical color_scale.
;  Arguments:
;    min:         The number that corresponds to the zero color-index.
;    max:         The number that corresponds to the 255 color-index.
;    x0:          x-position of lower left corner of color_scale [0.1]
;    y0:          y-position of lower left corner of color_scale [Cntrd Verti]
;    height:      Height of the color-scale [5.5cm].
;    levels:      Plot black horizontal lines at these levels [none]
;    charsize:    Character size [!P.CHARSIZE]
;    REVERSE:     Reverse the color-order.
;    CSTEPS:      If set, the number of color-steps in the scale, otherwise
;                 a smooth 256-color scale is used.
;    NCOLORS:     How many colors to use [!D.TABLE_SIZE].
;    STOP:        Set this keyword to stop before returning, to make all
;                 variables available to the user (for debugging).
;
;   Update history:
;   ---------------
;   23.10.2002 Coded before then/RT
;   12.11.2017 Changed default vertical position to vertically center the bar/RT
;
;   Known bugs: None
;
pro color_scale,min,max,x0=x0,y0=y0, height=height, levels=levels, $
        charsize=charsize, TRUECOLOR=TRUECOLOR,REVERSE=REVERSE, CSTEPS=CSTEPS, $
        INTEGER=INTEGER, NCOLORS=NCOLORS, LEFTSIDE=LEFTSIDE, LOG=LOG, $
        _EXTRA=extra_args, STOP=STOP
	
	yform=['','(I1)','(I2)','(I3)','(I4)','(I5)','(I6)','(I7)','(I8)','(I9)']
	SCALABLE_PIXELS = (!D.flags AND 1)
	default, x0, .1
	if (NOT keyword_set(TRUECOLOR)) then TRUECOLOR = 0
	if (NOT keyword_set(INTEGER)) then INTEGER = 0
	default, NCOLORS, !D.TABLE_SIZE
    if (!P.charsize LE 0.0) then csdef=1.0  else  csdef = !P.charsize
    default, charsize, csdef
	if (n_elements(height) eq 0) then begin
		if (SCALABLE_PIXELS) then $
			height = 5.5*!D.Y_PX_CM/float(!D.y_vsize) $
		else $
			height = (!D.TABLE_SIZE-1)/float(!D.y_vsize)
	endif
	default, y0, (1.-height)/2.
	if (SCALABLE_PIXELS) then begin
		ny = NCOLORS-1
	endif else begin
		ny = fix(height*!D.y_vsize)
	endelse
	c1 = NCOLORS-2.0 & ca = float(NCOLORS-1)/float(ny)
	nx = fix(ny/10.0)
    sgnch = 1.0
    if KEYWORD_SET(LOG) then begin
        if (min*max LT 0) then begin
            print, "WARNING(color_table): The range spans across 0 and cannot" $
                  + " be plotted on a logarithmic scale."
            print, "                      Bailing out!"
            return
        endif
        if (min LT 0  AND  max LT 0) then begin
            min=-min& max=-max& sgnch = -1.0
        endif
    endif
	yy = [min, max]
	xx = [0, nx-2]
	Ptmp = !P
	xtmp = !x
	ytmp = !y
	!P.region = 0
	!x.title = ''
	!y.title = ''
	isy=ca*findgen(ny)
	isy2 = isy
	if KEYWORD_SET(REVERSE) then for i=0L,ny-1L do isy(i)=isy2(ny-1-i)
	if (N_ELEMENTS(CSTEPS) EQ 1) then begin
		fcol = float(NCOLORS-1)/float(CSTEPS)
		isy = fix(isy/fcol)*fcol
	endif
	if (TRUECOLOR) then $
		color=reform(replicate(1,nx)#[isy,isy,isy],nx,ny,3) $
	else $
		color = replicate(1,nx)#isy
	iyform = max([fix(alog10(max([min,max])))+1,0])*INTEGER
	if KEYWORD_SET(LEFTSIDE) then  ysty = 5 $
							 else  ysty = 9
    ysty0 = ysty
    if KEYWORD_SET(LOG)      then  ysty = ysty0+4b*(1b-(ysty0 AND 4b))
	if (SCALABLE_PIXELS) then begin
		as = float(!D.y_vsize)/!D.x_vsize
		width = height/8.3*as
		dy = 10.*(!y.thick/2)/float(!D.y_vsize)
		!P.position=[x0, y0+dy, width+x0, height+y0-5./float(!D.y_vsize)]
		plot, xx, yy, xstyle=5, ystyle=ysty, ticklen=-.1, /NOERASE, $
					/NORMAL, /NODATA, yr=[min,max], YLOG=LOG, $
					chars=charsize, _EXTRA=extra_args, $
					ytickformat=yform(iyform);, ychars=1.25
;		if KEYWORD_SET(LEFTSIDE) then $
;			axis, yaxis=1,ysty=9,yr=!y.crange,ticklen=-.1,chars=charsize*0.5, $
;														_EXTRA=extra_args
	endif else begin
		cx0 = x0*!D.x_vsize & cy0 = y0*!D.y_vsize
		!P.position=[cx0, cy0+(fix(!y.thick)-1)/2, nx+cx0, ny+cy0-1]
		plot, xx, yy, xstyle=5, ystyle=ysty, ticklen=-.18, /NOERASE, YLOG=LOG, $
					/DEVICE, /NODATA, yr=[min,max], ytickformat=yform(iyform), $
					chars=charsize, _EXTRA=extra_args;,ychars=1.25
	endelse
;
; Using a logarithmic scale (IDL's default is not so good...)
    if KEYWORD_SET(LOG) then begin
        ylogticks, yaxis=0, 1e1^!y.crange, ytickv, ytickm, yticknames, $
            charsize=charsize, ticklen=-.18, NEGATIVE=(sgnch LT 0)
;       if (sgnch LT 0) then yticknames= '-'+yticknames
;       axis, yaxis=0,ysty=1,yr=1e1^!y.crange, ytickv=ytickv,yticks=Nticks,$
;           ytickname=yticknames, yminor=yminor, ticklen=-.18, chars=charsize
;       xdiff  = !x.crange(1)-!x.crange(0)
;       for i=0L, N_ELEMENTS(ytickm)-1L do $
;               plots, !x.crange(0)+[0,-0.09]*xdiff, [1,1]*ytickm(i)
    endif
	if (SCALABLE_PIXELS) then begin
		tv, color, x0, y0, xsize=width, ysize=height, /NORMAL, TRUE=3*TRUECOLOR
	endif else begin
		tv, color, cx0, cy0, TRUE=3*TRUECOLOR
	endelse
;	print, yform(iyform), iyform, min, max
	Nl = N_ELEMENTS(levels)
	for i=0L, Nl-1L do oplot, !x.crange, [1,1]*levels(i), col=0

    if KEYWORD_SET(STOP) then stop
	!P = Ptmp
	!x = xtmp
	!y = ytmp
end
