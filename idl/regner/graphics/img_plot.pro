;
;  Program to display a picture and prepare IDL to plot something on top
;  of it with axes fitting snuggly around the image.
;  The subsequent plot command should look something like
;  IDL> plot, x, y, /NOERASE, xsty=1, ysty=1, /NODATA
;  and you might want to use xsty=16, ysty=16 to get exact axis ranges
;  without forcing zero to be included.
;  Arguments:
;    A:           The picture to display - a 2 dimensional array.
;    x0:          normalized x-coordinate of lower left corner of picture.
;    y0:          normalized y-coordinate of lower left corner of picture.
;    height:      Max height of picture in normalized coordinates.
;    width:       Max width of picture in normalized coordinates.
;    margin:      Margin, in normalized coords, for default x0, y0 [0.03].
;    fxy:         Factor on the dimensions of A, needed for us on PS-dev [1.0].
;    bottom:      Returns the bottom of the color-scale, as used in the image.
;    top:         Returns the top of the color-scale, as used in the image.
;                 Colors beyond top and bottom are used for background and plot.
;    SCALE:       If set, use tvscl instead of tv.
;    LINEAR:      If set, perform linear interpolation, in congrid-call.
;    DEBUG:       If set, print out useful daignotic information.
;    OLDPOS:      If set, use the current !P.position coordinates of plot.
;    STOP:        Set this keyword to stop before returning, to make all
;                 variables available to the user (for debugging).
;
;  Update history:
;  ---------------
;  16.13.2005 Coded/Regner Trampedach
;  27.09.2006 Use congrid instead of rebin - gives better alignment of plot/RT
;  24.10.2006 Improved default size and position of picture and abandoned
;             integer scaling of picture when NOT SCALABLE_PIXELS/RT
;  12.06.2007 Setting keyword /MINUS_ONE for congrid suppresses extrapolation
;             and aligns the TV with the PLOT/RT
;  09.11.2007 Corrected position of plot. Also programmed bicubic alternative
;             to congrid, but it doesn't seem necessary/RT
;  04.11.2016 Fixed bug, so that top and bottom of color-scale is no longer
;             included in the displayed image/RT
;  29.08.2019 Fixed problem with aspect ratio, appearing after introduction
;             of the OLDPOS keyword./RT
;
;  Known bugs: Only fixed aspect ratio on media without SCALABLE_PIXELS
;
pro img_plot, A, x0, y0, height=height, width=width, margin=margin, fxy=fxy, $
    top=top, bottom=bottom, SCALE=SCALE, DEBUG=DEBUG, LINEAR=LINEAR, $
    OLDPOS=OLDPOS, STOP=STOP

	default, x0,     0.13
	default, y0,     0.09
	default, margin, 0.03
    default, fxy,    1.00

    if KEYWORD_SET(OLDPOS) then begin
        margin =     0.00
        x0 = !P.position(0L)&  width = !P.position(2L) - x0
        y0 = !P.position(1L)&  height= !P.position(3L) - y0
;       print, '1: ', x0, y0, width, height
    endif
; Setting height...
    if (N_ELEMENTS(height) GT 0L) then begin
        height = (height > 0.0)< 1.0
        if (height GT (1.-y0-margin)) then begin
; Eat into the margin first
            margin = (1.0-y0-height) > 0.0
; ...then y-off-set
            y0     = (1.0-margin-height) > 0.0
        endif
    endif
;       print, '2: ', x0, y0, width, height
; ...and width can override the provided margin and off-sets.
    if (N_ELEMENTS(width) GT 0L) then begin
        width = (width > 0.0)< 1.0
        if (width GT (1.-x0-margin)) then begin
; Eat into the margin first
            margin = (1.0-x0-width) > 0.0
; ...then x-off-set
            x0     = (1.0-margin-width) > 0.0
        endif
    endif
;       print, '3: ', x0, y0, width, height
	dm = 1.0 - margin
	Daspect = float(!D.X_SIZE)*(dm-x0)/float(!D.Y_SIZE)/(dm-y0)
    sa  = size(A)
    mnA = min(A, max=mxA)
    Nx  = sa(1)  & Ny  = sa(2)
    Nx2 = Nx     & Ny2 = Ny
	Aaspect = Nx/float(Ny)
    if (NOT KEYWORD_SET(OLDPOS)) then begin
        if (Aaspect LT Daspect) then begin
            default, height, dm-y0
            default, width,  height*Aaspect
        endif else begin
            default, width,  dm-x0
            default, height, width/Aaspect
        endelse
    endif
	if KEYWORD_SET(DEBUG) then print, Aaspect,Daspect,49./57.,width,height,dm

	SCALABLE_PIXELS = 1L
;       print, '4: ', x0, y0, width, height, Nx2, Ny2
; The graphics device has scalable pixels, e.g., Postscript-files.
	if (!D.FLAGS AND SCALABLE_PIXELS) then begin
        if KEYWORD_SET(OLDPOS) then begin
            if (width/Nx LT height/Ny) then begin
                Nx2 = Nx  & Ny2 = height/width*Nx
            endif else begin
                Ny2 = Ny  & Nx2 = width/height*Ny
            endelse
        endif else begin
;   	    sxy =min([(dm-x0)*!D.X_SIZE/float(Nx),(dm-y0)*!D.Y_SIZE/float(Ny)])
            sxy =min([!D.X_SIZE, !D.Y_SIZE])
; Outcommented this for making figs/idqpi_sun.ps for ans/AstrTheo2017
; from last lines of ~/idl/convec/pmodes/check_energeq.pro
; It made the wrong aspect ration; a tall rectangle.
;           width = width* sxy/!D.X_SIZE
            height= height*sxy/!D.Y_SIZE
        endelse
        if (abs(fxy-1.00) GT 1e-2) then begin
          Nx2 = round(fxy*Nx2)&  Ny2 = round(fxy*Ny2)
        endif
;       print, 'img_plot, scl px: ', Nx, Nx2, Ny, Ny2
        if (Nx2 NE Nx  OR  Ny2 NE Ny) then begin
          if KEYWORD_SET(LINEAR) then begin
              AA  = (congrid(A, Nx2, Ny2, /INTERP,    /MINUS_ONE)>mnA)<mxA
          endif else begin
              AA  = (congrid(A, Nx2, Ny2, CUBIC=-0.5, /MINUS_ONE)>mnA)<mxA
          endelse
        endif else begin
          AA = A
        endelse
        if KEYWORD_SET(SCALE) then begin
            ba=bytscl(AA, min=mnA, max=mxA, top=!D.TABLE_SIZE-3) + 1b
        endif else begin
            ba = byte(AA)
        endelse
        tv, ba, x0, y0, xsize=width, ysiz=height, /NORM
; Set the location of subsequent plots
        if (NOT KEYWORD_SET(OLDPOS)) then begin
            !P.position=[x0, y0, x0+width, y0+height]
        endif
        if KEYWORD_SET(DEBUG) then $
            print,(!D.FLAGS AND SCALABLE_PIXELS),sxy,width,height,Nx2,Ny2,fxy
	endif else begin
;
; The graphics device does not scalable pixels, e.g., the X-device.
        if KEYWORD_SET(OLDPOS) then begin
            Nx2 = round(!D.X_SIZE*width)&  Ny2 = round(!D.Y_SIZE*height)
        endif else begin
            sxy = fix(min([width*!D.X_SIZE/Nx, height*!D.Y_SIZE/Ny]))
            sx  = width*!D.X_SIZE/float(Nx)     & Nx2 = long(sx*Nx+.5)
            sy  = height*!D.Y_SIZE/float(Ny)    & Ny2 = long(sy*Ny+.5)
        endelse
        if (Nx2 NE Nx  OR  Ny2 NE Ny) then begin
            if KEYWORD_SET(LINEAR) then begin
                AA  = (congrid(A, Nx2, Ny2, /INTERP,    /MINUS_ONE)>mnA)<mxA
            endif else begin
                AA  = (congrid(A, Nx2, Ny2, CUBIC=-0.5, /MINUS_ONE)>mnA)<mxA
            endelse
        endif else begin
          AA = A
        endelse
;       print, '5: ', x0, y0, width, height, Nx2, Ny2
        if (N_ELEMENTS(bottom) GT 0L  AND  N_ELEMENTS(top) GT 0L) then begin
            if (bottom GT top) then begin
                bottom = min(aa, max=amx) &   top = amx
            endif
        endif else begin
            bottom = min(aa, max=amx) &   top = amx
        endelse
        aa = (aa>bottom)<top
        if KEYWORD_SET(SCALE) then begin
            ba=bytscl(AA, min=mnA, max=mxA, top=!D.TABLE_SIZE-3) + 1b
        endif else begin
            ba = byte(AA)
        endelse
;       print, '6: ', x0, y0, width, height, Nx2, Ny2
		tv,    ba, x0*!D.X_SIZE, y0*!D.Y_SIZE
; Set the location of subsequent plots
        if (NOT KEYWORD_SET(OLDPOS)) then $
            !P.position=[x0,                       y0-1./!D.Y_SIZE, $
                         x0+1.0*(Nx2-1)/!D.X_SIZE, y0+(1.0*Ny2-1)/!D.Y_SIZE]
        if KEYWORD_SET(DEBUG) then $
            print,(!D.FLAGS AND SCALABLE_PIXELS),sxy,sx,sy,Nx2,Ny2,width,height
	endelse

	if KEYWORD_SET(STOP) then stop
end
