;
;   Function to return the format-string that will capture all digits of a
;   number xc, down to the last (right) digit different from 0.
;
;   Update history:
;   ---------------
;   14.01.2004 Coded/RT
;   15.05.2006 Added option to set # significant digits/RT
;
;   Known bugs: Doesn't switch to scientific notation when shorter.
;
function floatform, x, dxc, Ndigits=Ndigits, Ndecimals=Ndecimals, $
								Nsigdigits=Nsigdigits, BARE=BARE, STOP=STOP
	xc = reform(abs(x))
	if (N_ELEMENTS(dxc) LE 0) then begin
		Nxc = N_ELEMENTS(xc)
		N2xc = Nxc/20>1
		dxc = 1.d0
		for i=0, Nxc-1, N2xc do begin
;		print, i, Nxc, N2xc
			half = sign(xc(i), .5d0)
			ddxc = abs(xc(i)-long(xc(i)/dxc+half)*dxc)
			while (ddxc GT 1.d-10 AND dxc GT 1.d-12) do begin
				dxc = dxc*1.d-1
				ddxc = abs(xc(i)-long(xc(i)/dxc+half)*dxc)
;				print, dxc, ddxc, xc(i), long(xc(i)/dxc+half)*dxc
			endwhile
		endfor
		dxc = dxc*1.d1
	endif
	det = .99999999d0
	de = long(alog10(dxc)-det*(dxc LT 1.d0))
	ee = 1-de
;
; Digits in front of the decimal place
    Ndigits   = long(alog10(max(xc))+1.) > 1L
;
; Number of decimals
	if (N_ELEMENTS(Nsigdigits) GT 0L) then begin
		Ndecimals =(Nsigdigits - Ndigits) > 0L
		if (alog10(max(xc)) LT 0.0) then begin
			Ndecimals = Nsigdigits - alog10(max(xc))
		endif
	endif else begin
		Ndecimals = ee > 0L
	endelse
;
; Make rum for the '-' sign if neccessary
	if (min(x) LT 0) then Ndigits=Ndigits+1L
    if (Ndecimals LE 0) then begin
		if KEYWORD_SET(BARE) then formstr='I0'  else  formstr='(I0)'
    endif else begin
		if KEYWORD_SET(BARE) then begin
			formstr=string(Ndigits+1+Ndecimals,Ndecimals,form='("F",I0,".",I0)')
		endif else begin
			formstr=string(Ndigits+1+Ndecimals,Ndecimals,form='("(F",I0,".",I0,")")')
    	endelse
    endelse

	if KEYWORD_SET(STOP) then stop
	return, formstr
end
