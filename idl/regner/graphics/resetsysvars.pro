;
;   Program to reset graphics systems variables before leaving a program.
;
;   Update history:
;   ---------------
;   12.09.1997 Coded - split-off from reset_graphics/RT
;   01.03.2007 No longer reset !*.range and !*.crange/RT
;
;   Known bugs: None
;
pro resetsysvars
	defsysv, '!p_save', exists=exsysv
	if (exsysv) then begin
		!p=!p_save
		if (strlowcase(!D.name) NE 'x') then begin
			tmp1 = !P.background
			!P.background = !P.COLOR
			!P.COLOR = tmp1
		endif
	endif

	defsysv, '!x_save', exists=exsysv
	if (exsysv) then begin
		tmp1 = !x.crange
		tmp2 = !x.s
		tmp3 = !x.window
		tmp4 = !x.type
		!x=!x_save
		!x.crange = tmp1
		!x.s      = tmp2
		!x.window = tmp3
		!x.type   = tmp4
	endif

	defsysv, '!y_save', exists=exsysv
	if (exsysv) then begin
		tmp1 = !y.crange
		tmp2 = !y.s
		tmp3 = !y.window
		tmp4 = !y.type
		!y=!y_save
		!y.crange = tmp1
		!y.s      = tmp2
		!y.window = tmp3
		!y.type   = tmp4
	endif

	defsysv, '!z_save', exists=exsysv
	if (exsysv) then begin
		tmp1 = !z.crange
		tmp2 = !z.s
		tmp3 = !z.window
		tmp4 = !z.type
		!z=!z_save
		!z.crange = tmp1
		!z.s      = tmp2
		!z.window = tmp3
		!z.type   = tmp4
	endif
;
    if (strlowcase(!D.NAME) EQ 'x') then device, set_graphics_function=3
end
;   TITLE           STRING    ''
;   TYPE            LONG                 0
;   STYLE           LONG                 1
;   TICKS           LONG                 0
;   TICKLEN         FLOAT           0.00000
;   THICK           FLOAT           0.00000
;   RANGE           DOUBLE    Array[2]
;   CRANGE          DOUBLE    Array[2]
;   S               DOUBLE    Array[2]                                
;   MARGIN          FLOAT     Array[2]                                
;   OMARGIN         FLOAT     Array[2]
;   WINDOW          FLOAT     Array[2]                                
;   REGION          FLOAT     Array[2]                                
;   CHARSIZE        FLOAT           0.00000                           
;   MINOR           LONG                 0                            
;   TICKV           DOUBLE    Array[60]                               
;   TICKNAME        STRING    Array[60]                               
;   GRIDSTYLE       LONG                 0                            
;   TICKFORMAT      STRING    Array[10]                               
;   TICKINTERVAL    DOUBLE           0.0000000                        
;   TICKLAYOUT      LONG                 0                            
;   TICKUNITS       STRING    Array[10]
