;
;   Program to swap foreground and background colors.
;
;   Arguments (defaults in [...]):
;    STOP:    Set this keyword to stop before returning, to make all
;             variables available to the user (for debugging).
;
;   Update history:
;   ---------------
;   24.04.2015 Coded/RT
;
;   Known bugs: None
;
pro bwswap, STOP=STOP
    tmp           = !P.color
    !P.color      = !P.background
    !P.background = tmp

    if KEYWORD_SET(STOP) then stop
end
