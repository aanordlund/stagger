function screen_dim, TrueColor=TrueColor, PseudoColor=PseudoColor
    spawn, 'xdpyinfo | grep dimension;' $
         + 'xdpyinfo | grep -c TrueColor;' $
         + 'xdpyinfo | grep -c PseudoColor', result
    if (result(0L) EQ '0') then $
        result=['  dimensions:    1920x1080 pixels (508x285 millimeters)','10','0']
	dims = long(str_sep((str_sep(strmid(strtrim($
							result(0),2),11,99),'pix'))(0),'x'))
	if (long(result(1)) GT 0) then TrueColor  =1 $
							  else TrueColor  =0
	if (long(result(2)) GT 0) then PseudoColor=1 $
							  else PseudoColor=0
	return, dims
end
