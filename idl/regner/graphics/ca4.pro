;+
;  Program to start a color A4 post-script page
;  Arguments:
;    x:             width/[cm]   [17.78cm]
;    y:             height/[cm]  [25.40cm]
;    name:          name of the PS-file  [idl.ps]
;    ENCAPSULATED:  set this keyword to make eps-files.
;    LANDSCAPE:     set this keyword to use landscape orientation.
;
;  19.05.2016  Have this program add comment with the command used for
;              creating the PS-file.
;-
pro ca4, x=x, y=y, name=name, ENCAPSULATED=ENCAPSULATED, LANDSCAPE=LANDSCAPE
	default, name, 'idl.ps'
	default, x, 17.78
	default, y, 25.4
	yoff = 27.3-y
	if KEYWORD_SET(ENCAPSULATED) then begin
		psname='eps'  &  altpsname='ps'
		print, 'Printing to encapsulated PS-file.'
	endif else begin
		psname='ps'   &  altpsname='eps'
		ENCAPSULATED=0
	endelse
	laltps = strlen(altpsname)
	nameparts = str_sep(name, "\.") & nparts = N_ELEMENTS(nameparts)
	if (nameparts(nparts-1) EQ altpsname) then $
			name=strmid(name, 0, strlen(name)-laltps-1)
	if (nameparts(nparts-1) NE psname) then name=name+"."+psname
	set_plot, 'ps'
	if KEYWORD_SET(LANDSCAPE) then begin
		print, 'Plotting in landscape-orientation.'
		dum=x & x=y & y=dum & yoff = 27.3
		device, xsize=x, ysize=y, yoffset=yoff, filename=name, $
						ENCAPSULATED=ENCAPSULATED, /COLOR, BITS=8, /LANDSCAPE
	endif else begin
		device, xsize=x, ysize=y, yoffset=yoff, filename=name, $
						ENCAPSULATED=ENCAPSULATED, /COLOR, BITS=8, LANDSCAPE=0
	endelse
;
; Add comment with the command used for creating this PS-file
    printf, 100, "%!PS-Adobe-3.0"
    printf, 100, "%% Created with: IDL> "+(recall_commands())(0)
    printf, 100, "%", form='(A0,$)'
end
