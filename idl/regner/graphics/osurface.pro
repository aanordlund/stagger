;
;   Program to oplot a surface on an already made 3D plot.
;
;   Arguments (defaults in [...]):
;    STOP:    Set this keyword to stop before returning, to make all
;             variables available to the user (for debugging).
;
;   Update history:
;   ---------------
;   24.02.2016 Coded/RT
;
;   Known bugs: None
;
pro osurface, z, x, y, color=color, XLOG=XLOG, YLOG=YLOG, ZLOG=ZLOG, $
                                            _EXTRA=extra_args, STOP=STOP
    common cthepast, oplots

    if (N_ELEMENTS(oplots) LT 1L) then oplots=0L
    wc     = (0.6 + .38*oplots) mod 1.0001
    oplots = oplots+1L
    s      = size(z)

    default, x, make_array(s(1L), type=s(s(0)+1L), /INDEX)
    default, y, make_array(s(2L), type=s(s(0)+1L), /INDEX)
    default, color, (!D.TABLE_SIZE-1.)*wc

    if KEYWORD_SET(XLOG) then xrng=1e1^!x.crange  else xrng=!x.crange
    if KEYWORD_SET(YLOG) then yrng=1e1^!y.crange  else yrng=!y.crange
    if KEYWORD_SET(ZLOG) then zrng=1e1^!z.crange  else zrng=!z.crange

    surface, z, x, y, xr=xrng, xsty=5, /T3D, color=color,$
        yr=yrng, ysty=5, zr=zrng, zsty=5, /NOERASE, $
        XLOG=XLOG, YLOG=YLOG, ZLOG=ZLOG, _EXTRA=extra_args

    if KEYWORD_SET(STOP) then stop
end
