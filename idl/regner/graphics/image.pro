;
;  Program to display an individual image of an array of images.
;  This routine is called by images.
;
;  ??.??.19?? Coded/Aake
;  11.02.2016 Added argument, markxy, to mark location (x,y) in each image./RT
;
;  Known bugs: None.
;
pro imsz,xs,ys
  if n_params() eq 0 then print,'!im.x=',!im.x,' !im.y=',!im.y $
  else if n_params() eq 1 then begin !im.x=xs & !im.y=xs & end $
  else begin !im.x=xs & !im.y=ys & end
end

pro pssz,xs,ys
  COMMON cpssz, psx, psy
  if n_params() eq 0 then print,'psx=',psx,' psy=',psy $
  else if n_params() eq 1 then begin psx=xs & psy=xs & end $
  else begin psx=xs & psy=ys & end
end

pro image,im0,pos,posy,zero=zero,erase=erase,max=max,min=min,xoffset=xoffset,$
  GRID=GRID,stat=stat,rdpixf=rdpixf,color=color,zrange=zrange,title=title,$
  profile=profile,gamma=gamma,symmetric=symmetric,order=order,$
  cubic=cubic,bilinear=bilinear,markxy=markxy
  COMMON cpssz, psx, psy

  if (n_elements(markxy) GT 0L) then MARK=1b  else  MARK=0b
  if n_elements(pos) eq 0 then pos=-1
  if n_elements(erase) ne 0 then erase
  if n_elements(xoffset) eq 0 then xoffset=0
  if n_elements(gamma) eq 0 then gamma=1.0
  if n_elements(order) eq 0 then order=0 else order=1
  if n_elements(zrange) ne 0 then begin 
    min=zrange(0) & max=zrange(1)
  end else begin
    if n_elements(min) eq 0 then min=0
    if n_elements(max) eq 0 then max=0
  end
  if n_elements(symmetric) ne 0 then begin 
    max=max(im0,min=min)
    max=max > (-min) & min=-max
  end
  bilinear=1
  im=reform(im0)
  s=size(im) 
  rt_graphics

  dot = [[0]]
  if !d.name eq "PS" then begin 
    if n_elements(psx) eq 0 then psx=6.
    if n_elements(psy) eq 0 then psy=6.
    xs=psx & ys=psy
    dot = [[0,0,0,0],[0,0,0,0],[0,0,0,0],[0,0,0,0]]
  end else if keyword_set(cubic) then begin
    xs=!im.x
    ys=!im.y 
  end else begin
    xf=!im.x/float(s(1))
    yf=!im.y/float(s(2))
    if xf ge 1.0 then xs=long(xf+0.5)*s(1) else xs=s(1)/long(1./xf+0.5)
    if yf ge 1.0 then ys=long(yf+0.5)*s(2) else ys=s(2)/long(1./yf+0.5)
    !im.x=xs
    !im.y=ys 
  end
  fx = float(N_ELEMENTS(im(*,0)))/float(N_ELEMENTS(im0(*,0)))
  fy = float(N_ELEMENTS(im(0,*)))/float(N_ELEMENTS(im0(0,*)))

  if keyword_set(stat) gt 0 then stat,im
  if KEYWORD_SET(GRID) then  xygrid=1  else  xygrid=0

  if !d.name eq "TEK" then begin
    if keyword_set(cubic) then $
      amscl,congrid(/cubic,im,xs,ys),min=min,max=max $
    else if keyword_set(bilinear) then $
      amscl,congrid(/inter,im,xs,ys),min=min,max=max $
    else $
      amscl,rebin(im,xs,ys),min=min,max=max
;------------------------------------------------------------------------
  end else if !d.name eq "PS" then begin
    w=!d.x_size/!d.x_px_cm
    h=!d.y_size/!d.y_px_cm
    x=(pos > 0)*(xs+xygrid)
    y=h-ys-xygrid
    ;stop
    if x+xs gt w then begin 
      nrow=fix((w+1.)/(xs+xygrid))
      ny=fix(x/((xs+xygrid)*nrow))
      y=h-(ys+xygrid)*(ny+1)
      x=((pos > 0)-ny*nrow)*(xs+xygrid)
    end  
    print,'x,y:', x,y
 
    imax=max(im) & imin=min(im)
    if n_elements(max) eq 0 or max eq min then imax=max(im) else imax=max
    if n_elements(min) eq 0 or max eq min then imin=min(im) else imin=min
    if n_elements(zero) ne 0 then begin
      iabs=max([abs(imin),abs(imax)])
      imin=-iabs
      imax=iabs
    end   

    if gamma ne 1.0 then $
      im1=bytscl((im-imin)^gamma,max=(imax-imin)^gamma,min=0) $
    else $
      im1=bytscl(im,max=imax,min=imin)

;    if KEYWORD_SET(GRID) gt 0 then begin
;      lg=indgen(s(1))*fix(xs/s(1))
;      mg=indgen(s(2))*fix(ys/s(2))
;      top=255
;      im1(lg,*)=top
;      im1(*,mg)=top
;    end

    ;print,x,y                                        
    device,bits=8 
    eps=0.03
    if (pos ge 0) then begin
      tv,im1,/centimeter,x+xoffset,y,xsize=xs-eps,ysize=ys-eps,order=order
      if (MARK) then tv, dot, x+xoffset+fx*markxy(0),y+fy*markxy(1)
    endif else begin
      tv,im1,/centimeter,xsize=xs-eps,ysize=ys-eps,order=order
      if (MARK) then tv, dot, fx*markxy(0),fy*markxy(1)
    endelse

    if n_elements(title) ne 0 then begin
      if pos ge 0 then begin
        nx=fix(!d.x_size*1e-3/xs)
        ix=pos mod nx
        iy=pos/nx
        xt=(ix+0.5)*xs*1e3
        yt=!d.y_size-!d.y_ch_size*1.2-iy*ys*1e3
      end else begin
        xt=0.5*xs*1e3
        yt=ys*1e3-!d.y_ch_size*1.2
      end
      xyouts,xt,yt,/dev,title,align=0.5,charsize=1,color=!d.table_size-1
    end
;------------------------------------------------------------------------
  end else begin
    ;tvlct,/get,rr,gg,bb
    ;top=n_elements(rr)-2
    ;if top le 1 then top=255
    top=!d.table_size-1
    imax=max(im) & imin=min(im)
    if n_elements(max) eq 0 or max eq min then imax=max(im) else imax=max
    if n_elements(min) eq 0 or max eq min then imin=min(im) else imin=min
    if n_elements(zero) ne 0 then begin
      iabs=max([abs(imin),abs(imax)])
      imin=-iabs
      imax=iabs
    end   

    w=!d.x_size
    h=!d.y_size
    x=(pos > 0)*(xs+xygrid)
    y=h-ys-xygrid
    if (x+xs gt w) then begin
      nrow=fix((w+1.)/(xs+xygrid))
      ny=fix(x/((xs+xygrid)*nrow))
      y=h-(ys+xygrid)*(ny+1)
      x=((pos > 0)-ny*nrow)*(xs+xygrid)
    end
	if (y LT -ys) then return
    ;im1=rebin(1+bytscl(im,max=imax,min=imin,top=top),xs,ys)
    ;im1=rebin(bytscl(im,max=imax,min=imin,top=top),xs,ys)
    if keyword_set(cubic) then $
      im1=congrid(/cubic,bytscl(im,max=imax,min=imin,top=top-5),xs,ys)+2 $
    else if keyword_set(bilinear) then $
      im1=congrid(/inter,bytscl(im,max=imax,min=imin,top=top-5),xs,ys)+2 $
    else $
      im1=rebin(bytscl(im,max=imax,min=imin,top=top-3),xs,ys)+2
    
;    if KEYWORD_SET(GRID) gt 0 then begin
;      lg=indgen(s(1))*fix(xs/s(1))
;      mg=indgen(s(2))*fix(ys/s(2))
;      im1(lg,*)=top
;      im1(*,mg)=top
;    end
    if n_elements(posy) gt 0 then begin
      tv,im1,(pos > 0),posy,order=order
      if (MARK) then tv, dot, (pos > 0)+fx*markxy(0), posy+fy*markxy(1)
    end else begin
      if pos ge 0 then begin
        tv,im1,x,y,order=order; else tv,im1,order=order
        if (MARK) then tv, dot, x+fx*markxy(0), y+fy*markxy(1)
      endif
    end
    if n_elements(color) ne 0 then color,im0,/bar
    if n_elements(rdpixf) ne 0 then rdpix,rebin(im,xs,ys)
    if n_elements(profile) ne 0 then profiles,rebin(im,xs,ys)
    if n_elements(title) ne 0 then begin
      if pos ge 0 then begin
        nx=fix(!d.x_size/xs)
        ix=pos mod nx
        iy=pos/nx
        xt=(ix+0.5)*xs
        yt=!d.y_size-20-iy*ys
        ;print,ix,iy,xt,yt
      end else begin
        xt=0.5*xs
        yt=ys-10
      end
      xyouts,xt,yt,/dev,title,align=0.5,charsize=1.5
      wait,0
    end
  end
  reset_graphics
end
