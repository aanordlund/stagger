;
;   Movie of quantities relevant to the bottom boundary
;
;   Update history:
;   ---------------
;   10.12.2004 Coded/RT
;   15.05.2006 Corrected formatting of depth above each plot/RT
;
;   Known bugs: None
;
function mvar, ivar, it0, SIGNCOLOR=SIGNCOLOR
	common cbmov,fscaler,fmin,fmax,nvar,izz
	common csign,An,Ap,colmax

	if (it0 LT 0) then begin
		nvar=6
		fscaler=dblarr(nvar)
		fmax   =dblarr(nvar) & fmax(*)=-1.d+38
		fmin   =dblarr(nvar) & fmin(*)= 1.d+38
		fmin0  =dblarr(nvar) & fmin0(*)=1.d+38
		it = 0
	endif else it=it0
	SS0 = lookup(alog(10)*.5, 2.5, iv=iS)
	uS = 1.d+4
Igen:
	if KEYWORD_SET(SIGNCOLOR) then begin
		case ivar of
			0: mvar=1d0*fint(it)-aver(1d0*fint(it))
			1: mvar=1d0*fpln(it,0,izz) - aver(1d0*fpln(it,0,izz))
			2: mvar=1d0*fpln(it,3,izz)
			3: mvar=1d0*fpln(it,4,izz) - aver(1d0*fpln(it,4,izz))
			4: begin
				tmp=1d0*(lookup(fpln(it,0,izz),fpln(it,4,izz),iv=iS)-SS0)*uS
				mvar = tmp - aver(tmp)
			   end
			5: begin
				tmp=1d0*lookup(fpln(it,0,izz),fpln(it,4,izz))
				mvar = tmp - aver(tmp)
			   end
		endcase
;	color-index 130 corresponds to zero in the variable
		dc2 = (!D.TABLE_SIZE-1.)/2.
		if (it0 GE 0  OR  it GT -it0) then begin
			dmvar = -An*colmax/fscaler(ivar)*.5
;			print, 'dmvar=',dmvar, fmin(ivar), fmax(ivar), colmax/fscaler(ivar)
			mvar = mvar + dmvar
		endif
;			mvar = ((mvar/max([fmax(ivar),abs(fmin(ivar))])) + 1.38)*dc2; 135
;			mvar = ((mvar/max([fmax(ivar),abs(fmin(ivar))])) + 1.23)*dc2; 141
	endif else begin
		case ivar of
			0: mvar=fint(it)-fmin(0)
			1: mvar=fpln(it,0,izz)-fmin(1)
			2: mvar=fpln(it,3,izz)-fmin(2)
			3: mvar=fpln(it,4,izz)-fmin(3)
			4: mvar=(lookup(fpln(it,0,izz),fpln(it,4,izz),iv=iS)-SS0)*uS-fmin(4)
			5: mvar=lookup(fpln(it,0,izz),fpln(it,4,izz))-fmin(5)
		endcase
	endelse
	if (it0 LT 0  AND  it LE -it0) then begin
		fmin0(ivar)= min([min(mvar), fmin0(ivar)])
		fmean = aver(mvar)
		frms = rms(mvar-fmean)
		fdiff = ([min(mvar), max(mvar)]-fmean)
;		fmarg = frms^(alog(abs(fdiff))/alog(frms)*.75)
		fmarg = 0.1d00*frms*[1,1];	^(alog(abs(fdiff))/alog(frms)*.75)
		fmax(ivar) = max([max(mvar), fmax(ivar)])
		fmin(ivar) = min([min(mvar), fmin(ivar)])
		if (NOT KEYWORD_SET(SIGNCOLOR)) then fmin(0)=1d-80
;		fmax(ivar) = max([fmean+fmarg(1), fmax(ivar) ])
;		fmax(ivar) = max([max(mvar), fmax(ivar) ])
		if (it EQ -it0) then begin
;			fmin(ivar) = fmin0(ivar)/1.01^sign(1.0,fmin0(ivar)) & fmin(0)=0.0
;			fmin(ivar) = fmean - fmarg(0) & fmin(0)=0.0
			if KEYWORD_SET(SIGNCOLOR) then begin
				fscaler(ivar)= colmax*.5d0*min([Ap/(fmax(ivar)+fmarg(1)), An/(fmin(ivar)-fmarg(0))])
			endif else begin
				fscaler(ivar) = colmax/(fmax(ivar)-fmin(ivar)); /1.01
			endelse
		endif
		ivar = ivar + 1 
		if (ivar EQ nvar) then begin
			ivar = 0
			it = it-it0
		endif

		if (it LE -it0) then goto, Igen
		mvar = 0.0
	endif
	return, mvar
end

pro bmovie, files=files, scaler=scaler, SAVE=SAVE, iz=iz, tabfile=tabfile, $
        dn=dn, slower=slower, SIGNCOLOR=SIGNCOLOR, SCRATCH=SCRATCH, STOP=STOP
	common cdat,x,y,z,nx,ny,nz,nw,ntmax,date0,time0
	common cbmov,fscaler,fmin,fmax,nvar,izz
	common csign,An,Ap,colmax

	default, slower, 0.0
    if KEYWORD_SET(SCRATCH) then NOSCRATCH=0b  else  NOSCRATCH=1b
    rt_graphics
	if KEYWORD_SET(SIGNCOLOR) then begin
		SIGNCOLOR = 1
		loadct, 18
;
;  With the color-table ranging from 0;255 the zer0-color is col0*255d0.
;
		col0 = 0.51145038167938923d0
		fA   = 1./(2d0*min([col0, 1d0-col0]))
		An   = -2d0*col0*fA
		Ap   =  2d0*(1d0-col0)*fA
	endif else $
		SIGNCOLOR = 0
	colmax= 255d0
	titles=['!17Surface intensity','ln!7q!x','!8u!iz!n!x','!7e!x',$
														'!8S!x','!8P!x!iG!n']
	default, tabfile, stamnavn()+'.tab'
	if (strpos(tabfile, '.tab') LT 0) then tabfile=tabfile+'.tab'
	titlelen = strlen(titles(0))
	default, files, '??'
	default, dn, 1L
	print, stamnavn()
	f = simlist(files, NOSCRATCH=NOSCRATCH)
	nf = n_elements(f)
	it0 = 0L
	for n=0, nf-1 do begin
		print, '   ',f(n)
		file, f(n), /quiet
		if (n EQ 0) then begin
			default, scaler, !D.x_size/(3*nx)
			default, iz, nz-1
			izz = iz
			table, tabfile
			sstab, iS
			dummy=mvar(0,-(ntmax-1),SIGNCOLOR=SIGNCOLOR)
			bfilm = bytarr(scaler*ny,scaler*nx,nvar)
			sx0 = min([100L, (!d.x_size-3*(scaler*nx+4)+4>0)/2, $
							 (!d.y_size-2*(scaler*ny+4+!D.Y_CH_SIZE*1.3)>0)/2])
			sy0 = sx0
			cs  = (scaler*nx/(!D.X_CH_SIZE*titlelen*0.85)) < 1.0
			sx = sx0 + (indgen(nvar) mod 3)*(scaler*nx+4)
			sy = sy0 + (indgen(nvar)/3)*(scaler*ny+4+!D.Y_CH_SIZE*2.0*cs)
			if (KEYWORD_SET(SAVE)) then begin
				rundir = simdir()
;				rundir = 'stars/'+stamnavn()+'run/'
;				hosts_dir, cnvdir=rundir
				openw, 3, rundir+stamnavn()+'.bfilm'
				printf, 3, scaler*nx, scaler*ny, nvar
			endif

			erase
			if KEYWORD_SET(SIGNCOLOR) then begin
				hcolor_scale, An, Ap, y0=.9, length=.9, x0=.05
			endif
			fform = floatform(z(izz), Nsigdigits=4)
			titles(1:*)=titles(1:*)+'(!8z!x='+string(z(izz),form=fform)+'Mm)'
			xyouts, sx+.5*scaler*nx, sy+scaler*ny+4*cs, titles, align=.5, $
															/DEVICE, charsize=cs
		endif
		for t=it0,ntmax-1,dn do begin
			print, t
			for i=0,nvar-1 do begin
				bfilm(*,*,i) = byte(rebin(mvar(i,t, SIGNCOLOR=SIGNCOLOR)* $
											fscaler(i), scaler*nx, scaler*ny))
;				print, 'min/max: ', min(bfilm(*,*,i),max=mmax), $
;												aver(bfilm(*,*,i)), mmax, i
			endfor
			if KEYWORD_SET(SAVE) then begin
				writeu, 3, bfilm
			endif else begin
				for i=0, nvar-1 do $
					tv, bfilm(*,*,i)/colmax*(!D.TABLE_SIZE-1.), sx(i), sy(i)
				wait, slower
			endelse
		endfor
		it0 = t-ntmax
		print, t, it0, dn
	endfor
	if (KEYWORD_SET(SAVE)) then begin
		close, 3
		print, 'Saved the movie to "'+rundir+stamnavn()+'.sfilm".'
	endif
    reset_graphics
	if KEYWORD_SET(STOP) then stop
end
