;
;   Program to show or generate animation of surface intensity.
;
;   Update history:
;   ---------------
;   19.01.2001 Coded/RT
;   21.03.2007 Much more compact print-out/RT
;   01.04.2016 Added keyword SCRATCH for including the scratch-file/RT
;
;   Known bugs: None
;
pro smovie, files=files, scaler=scaler, dt=dt, NOLOAD=NOLOAD, SAVE=SAVE, $
									NOERASE=NOERASE, SCRATCH=SCRATCH, STOP=STOP
	common cdat,x,y,z,nx,ny,nz,nw,ntmax,date0,time0

	default, files, '??'
	default, dt, 0.3
    rt_graphics
	if KEYWORD_SET(NOLOAD) then begin
		nf = 1L
	endif else begin
;		print, stamnavn()
		if KEYWORD_SET(SCRATCH) then   f = simlist(files) $
								else   f = simlist(files, /NOSCRATCH)
		nf = n_elements(f)
	endelse
	if KEYWORD_SET(SAVE) then begin
		rundir = simdir()
;		rundir = 'stars/'+stamnavn()+'run/'
;		hosts_dir, cnvdir=rundir
		openw, 3, rundir+stamnavn()+'.sfilm'
	endif else begin
		if (NOT KEYWORD_SET(NOERASE)) then erase
		loadct, 3, /SILENT
	endelse
	maxcol = 255.*.97
	for n=0, nf-1 do begin
		if (NOT KEYWORD_SET(NOLOAD)) then file, f(n), /quiet
		if (n EQ 0) then begin
			default, scaler, 340/nx
			bfilm = bytarr(scaler*ny,scaler*nx)
;			intscaler = 180./max([fint(0),fint(ntmax-1)])
			intscaler = maxcol/max([fint(0),fint(ntmax-1)])
			if KEYWORD_SET(SAVE) then printf, 3, scaler*nx, scaler*ny
		endif
		for t=0,ntmax-1 do begin
			print, string(13b), (fstat(1)).name, t, form='(A0,"   ",A0,I4,$)'
			bfilm = byte(rebin(fint(t)*intscaler, scaler*nx, scaler*ny))
;			bfilm = byte(rebin((fpln(t,0,0)+11.4)*26, scaler*nx, scaler*ny))
			if (t*dt GT 0) then wait, dt
			if KEYWORD_SET(SAVE) then begin
				writeu, 3, bfilm
			endif else begin
				tv, bfilm/255.*(!D.TABLE_SIZE-1.), 100, 100 
			endelse
		endfor
	endfor
	print, ""
	if KEYWORD_SET(SAVE) then begin
		close, 3
		print, 'Saved the movie to "'+rundir+stamnavn()+'.sfilm".'
	endif
    reset_graphics
	if KEYWORD_SET(STOP) then stop
end
