;
;   Function to read meanrhoT.F77.dat-files
;   Manually reads the Fortran record-length info, to infer the
;   number of points in the meanrhoT-file.
;   Returns  1 (true) on success and 0 (false) otherwise.
;
;   Command arguments (defaults in [...]):
;    rhom:   Density/[simulation units] of mean-rho/-T structure.
;    ttm:    Temperature/[K] of mean-rho/-T structure.
;    zm:     Z-scale of mean-rho/-T structure, as contained in
;            post 15.04.2004 files.
;    mz:     Expected # of points in mean-rho/-T structure [nz].
;    dir:    Directory where to look for the meanrhoT-file [<simdir>].
;    ext:    Extension to the name of the meanrhoT-file, e.g., '.bak1' [''].
;    QUIET:  Suppress the printing of warnings, but not of error-messages.
;    STOP:   If this keyword is set, stop before returning.
;
;   Update history:
;   ---------------
;   15.04.2004 Coded, based on idea by Martin Asplund/RT
;
function read_meanrhot, rhom, ttm, zm, mz=mz, dir=dir, ext=ext, $
											QUIET=QUIET, STOP=STOP
	common cdat,x,y,z,nx,ny,nz,nw,ntmax,date0,time0

;	if (N_ELEMENTS(mz) LE 0  AND  N_ELEMENTS(nz) EQ 1) then mz=nz
	default, mz, nz
	default, dir, simdir()
	default, ext, ''
	if (ext NE ''  AND  strpos(ext,".") LT 0) then ext="."+ext

	mfile = dir + 'meanrhoT.F77.dat' + ext
	if (NOT file_test(mfile)) then return, 0
	reclen = 0L
	openr, lun, mfile, ERROR=err, /GET_LUN,/SWAP_IF_LITTLE_ENDIAN
	if (err NE 0) then begin
		print, err, !ERROR_STATE.MSG
		return, 0
	endif
;	print, " Opening "+mfile
	readu, lun, reclen
;
;  mz according to the record-length of the meanrhoT-file.
	mzm   = reclen/8L
	if (mzm LE 3L) then begin
		print, mzm, form='("ERROR: Too few points; mz=",I0)'
		return, 0
	endif
	if (NOT KEYWORD_SET(QUIET)  AND  N_ELEMENTS(mz) GT 0) then begin
		if (mz NE mzm  AND  mz GT 0L) then print,mz,mzm,mfile, $
							form='(" WARNING: mz=",I0," <> mzm=",I0," of ",A0)'
	endif
	rhom=fltarr(mzm) & ttm=rhom
	readu, lun, rhom, ttm
	readu, lun, reclen

	if (NOT EOF(LUN)) then begin
		readu, lun, reclen
		if (reclen/4L EQ mzm) then begin
			zm = fltarr(mzm)
			readu, lun, zm
		endif else begin
			print, reclen/4L, mzm, form='(" ERROR: The z-scale has ",I0,'+$
				  '" points, but the rho- and T-arrays has ",I0," points.")'
			print, "        Skipped reading of z-scale!"
		endelse
	endif

	free_lun, lun

	if KEYWORD_SET(STOP) then stop
	return, 1
end
