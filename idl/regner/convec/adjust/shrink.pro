;
; This program shrinks a scr-file vertically.
; n0 and n1 are the top and bottom gridpoints used in the new scr2file.
; i.e. n0 -> 1 as scr1file -> scr2file (the fiducial layer is added subsequently
; and  n1 -> nz-1 as scr1file -> scr2file.
;
;   Update history:
;   ---------------
;   27.08.2004 Coded/RT
;   27.08.2004 Automatic printing to log-file./RT
;   30.11.2015 Omit 'prompt' after last variable./RT
;
pro shrink, n0, n1, scr1file=scr1file, scr2file=scr2file, tabfile=tabfile
	common coff,mpar,mav,ipack,incr,ioff,time,voff,cfile,irec
	common intrpcom,nx1,ny1,nz1,nx2,ny2,nz2,it1,it2,scr1fil,scr2fil,tabfil,halv
	common cdat,x,y,z,nx,ny,nz,nw,ntmax,date0,time0

    default, scr1file, stamnavn()
    if (strpos(scr1file, '.scr') LT 0) then scr1file=scr1file+'.scr'
	default, scr2file, scr1file
    if (strpos(scr2file, '.scr') LT 0) then scr2file=scr2file+'.scr'
    default, tabfile, stamnavn()
    if (strpos(tabfile, '.tab') LT 0) then tabfile=tabfile+'.tab'
	tabfil = tabfile

	halv = 0					;	Not halving the xy-resolution.
    file, scr2file, /QUIET
    nx2=nx & ny2=ny & nz2=nz & scr1fil=scr1file
    file, scr1file, /QUIET
    nx1=nx & ny1=ny & nz1=nz & scr2fil=scr2file
	z1 = z
	n0 = (n0 > 1L) < (nz1-4)
	n1 = (n1 < (nz-1)) > (n0+3)
	bzone = cpar(0,'bzone')
    it1 = ntmax-1   ; source timestep
    it2 = 0         ; destination timestep

	z2 = [0,intrp1(findgen(nz1), n0+findgen(nz2-1)*(n1-n0+0.)/(nz2-2.), z1)]
	z2(0) = z2(1) - bzone*(z2(2)-z2(1))
	print, stamnavn()
;
; This loop takes care of
;	- all interpolations.
;	- saves all changes to scr2fil.
;	- updates the meanrhoT.F77.dat-file.
;	- plots and compares the resulting T-structure with the old one.
;	- calculates new rhobot, eebot and ppbot values.
;	- reopens scr2fil in read-only mode.
;
    pstr = ['Start writing to '+scr2file,'Next var','Next var','Next var', $
                              'Next var','Next var'] + '? [y/n/q/s]: '
	for i=1L, 6L do begin
		intrp_var, i, z1, z2
        if (i LT 6L) then begin
            s = prompt(pstr(i-1L))
            if (s EQ 'n'  OR  s EQ 'q'  OR  s EQ 'n') then return
            if (s EQ 's')                             then stop
        endif
	endfor
	add_to_log, /CLEAR
end
