;
;	Program to consistently update an altered scratch-file.
;	  If flnrho(it) and fee(it) have been changed, this routine restores
;	consistency, by calculating and updating the T- and lnp-cubes and the cdata;
;   If flnrho(it) and fee(it) have only been changed in the upper part,
;   this can be specified with izmax < (nz-1). The cdata:
;		ttm, ttmin, ttmax, ppm, ptm, pp0, eem, ee0, rhom, rho0, rhoim
;	are updated and written them to the scratch-file. Also updates rhobot,eebot
;	in case izmax=nz and the user confirms.
;
;   Update history:
;   ---------------
;   08.10.1998 Coded/RT
;   01.11.2007 Plot a line at the deepest point, z(izmax), of the change/RT
;   01.04.2016 Added calculation and saving of 'ptm'/RT
;
;   Known bugs: None
;
pro update, it, izmin=izmin, izmax=izmax, scrfile=scrfile, tabfile=tabfile, $
											linestyle=linestyle, STOP=STOP
	common coff,mpar,mav,ipack,incr,ioff,time,voff,cfile,irec
	common cdat,x,y,z,nx,ny,nz,nw,ntmax,date0,time0
	
	default, it, 0
	default, linestyle, 0
    if (N_ELEMENTS(scrfile) GT 0) then begin
        if (strpos(scrfile, '.scr') LT 0) then scrfile=scrfile+'.scr'
        print, scrfile
        file, scrfile, /QUIET, /WRITE
    endif else begin
        scrfile = stamnavn()+'.scr'
        fst = fstat(1)
        if (strpos(fst.name,'.scr') LT 0) then begin
            print, scrfile
            file, scrfile, /QUIET, /WRITE
        	fst = fstat(1)
        endif
		if (fst.write EQ 0) then file, fst.name, /QUIET, /WRITE
    endelse
	default, izmin, 0L
	default, izmax, nz-1L
    default, tabfile, stamnavn()
    if (strpos(tabfile, '.tab') LT 0) then tabfile=tabfile+'.tab'
    if (tabfile EQ 'EOSrhoe.tab') then eostab   $
                                  else table, tabfile
	c0 = (!D.TABLE_SIZE-1.)*.7
	plot, z, cdata(it,'ttmax'), ysty=2, line=linestyle, ytickform='(I0)', $
					xr=[z(izmin),min([z(izmax)+cpar(0,'height')*.1,z(nz-1)])], $
					xtitle='!8z!3/[!xMm!3]!x', ytitle='!8T!3/[!xK!3]!x'
	oplot, [1,1]*z(izmax), !y.crange, line=2
	oplot,z, cdata(it,'ttmin'), line=linestyle
	oplot,z, cdata(it,'ttm')
    label, .2, .90, 0, 'Old'
	ttm  = cdata(it,'ttm')
	ttmin= cdata(it,'ttmin')
	ttmax= cdata(it,'ttmax')
	eem  = cdata(it,'eem')
	ppm  = cdata(it,'ppm')
	ptm  = cdata(it,'ptm')
	rhom = cdata(it,'rhom')
	rhoim= cdata(it,'rhoim')
	for i=izmin, izmax do begin
		ee  = fpln(it, 4, i)
		lnr = fpln(it, 0, i)
		TT = lookup(lnr, ee, iv=2)
		ttm(i)   = aver(TT)
		ttmin(i) = min(TT)
		ttmax(i) = max(TT)
		wpln, TT, it, 5, i
		ppm(i) = aver(exp(lookup(lnr, ee)))
        ptm(i) = aver(exp(lnr)*fpln(it, 3, i)^2)
		eem(i) = aver(ee)
		rhom(i)= aver(exp(lnr))
		rhoim(i)=aver(exp(-lnr))
	endfor
	oplot, z, ttm,   col=c0
	oplot, z, ttmax, col=c0, line=linestyle
	oplot, z, ttmin, col=c0, line=linestyle
    label, .2, .85, 0, 'New', col=c0
	if KEYWORD_SET(STOP) then stop
;	answ = prompt('Nice enough [y/n] - The changes will now be saved to '+ $
;																scrfile+' : ')
;
; Save horizontal averages.
	wdata, ttm,              it, 'ttm'
	wdata, ttmin,            it, 'ttmin'
	wdata, ttmax,            it, 'ttmax'
	wdata, ppm,              it, 'ppm'
	wdata, ptm,              it, 'ptm'
	wdata, smoothfx(z, ppm), it, 'pp0'
	wdata, eem,              it, 'eem'
	wdata, smoothfx(z, eem), it, 'ee0'
	wdata, rhom,			 it, 'rhom'
	wdata, smoothfx(z, rhom),it, 'rho0'
	wdata, rhoim,			 it, 'rhoim'
; If this is a scratch-file, update rho-/ee-bot ?
	if (izmax EQ (nz-1)  AND  incr GT 10L) then rho_e_bot, it
; Reload scratch-file to avoid accidental overwrite.
	file, scrfile, /QUIET
end



;
; Needs some costumization. The Makefile contains;
;newtable:
;	/bin/csh -c "time tabcmp.x < tabcmp.in > tabcmp.log"
;	/bin/csh -c "time tabinv.x < tabinv.in > tabinv.log"
; as the default.
;
;	spawn, 'rsh aauc38 "cd convdat/stars/'+stamnavn()+'/run;make"', result
;	print, result
;	table, stamnavn()+'.tab'
