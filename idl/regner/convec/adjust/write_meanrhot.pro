;
;   Routine to write meanrhoT.F77.dat-files
;   If the file already exists, the old-version will be moved to
;   a back-up file with the extension; 'bak[n]' with [n] being an
;   increasing sequence of integers.
;
;   Command arguments (defaults in [...]):
;    rhom:   Density/[simulation units] of mean-rho/-T structure.
;    ttm:    Temperature/[K] of mean-rho/-T structure.
;    zm:     Z-scale of mean-rho/-T structure, as contained in
;            post 15.04.2004 files.
;    dir:    Directory where to look for the meanrhoT-file [<simdir>].
;    ext:    Extension to the name of the meanrhoT-file, e.g., '.bak1' [''].
;    STOP:   If this keyword is set, stop before returning.
;
;   Update history:
;   ---------------
;   15.04.2004 Coded, based on idea by Martin Asplund/RT
;

pro write_meanrhot, rhom, ttm, zm, dir=dir, ext=ext, STOP=STOP
	default, dir, simdir()
	default, ext, ''
	if (ext NE ''  AND  strpos(ext,".") LT 0) then ext="."+ext

	mfile = dir + 'meanrhoT.F77.dat' + ext
	backup, mfile, /QUIET
	openw, lun, mfile, /F77_UNFORMATTED, /GET_LUN,/SWAP_IF_LITTLE_ENDIAN
	writeu, lun, float(rhom), float(ttm)
	if (N_ELEMENTS(zm) GT 0L) then writeu, lun, float(zm)
	free_lun, lun
;
;   Copy to mass storage?
;
	host = (str_sep(getenv("HOST"), "\."))(0)
	case host of
		'modi4':	spawn, 'msscmd -c cd '+simdir(/REL)+', put meanrhoT.F77.dat'
		 else:
	endcase

	if KEYWORD_SET(STOP) then stop
end
