; 0.42  0.37  1.03  0.57   9x12mm.scr -> 9mm.scr
; 0.09  0.11  0.80  0.72   sun.scr -> tmp.scr  (100^2x82)
;
;  This program shifts the model in the latest timestep of "stamnavn".scr
;  (timestep 0 or 1) down, so that T=Teff at z=0.0 and 80% of the model is
;  below the photosphere. The z-axis is remodelled to obtain the finest sampling
;  at the depth with the largest temperature gradient. The fiducial layer is
;  introduced by taking z(0) = z(1) - bzone*(z(2)-z(1)). Everything is inter-
;  polated and stored in a consistent way, and new values for eebot and rhobot
;  are suggested.
;  The model is saved to timestep 0 of 'stamnavn.scr'.
;
;   Update history:
;   ---------------
;   15.05.1096 changed the interpol. to use Aake's idl/convection/intrp3.pro/RT
;   24.05.2004 No "Remember to set ntime=-1"-printout when cancelling./RT
;   27.08.2004 Automatic printing to log-file./RT
;   17.05.2005 Always print the resulting arguments to the log-file./RT
;   06.11.2006 Also accept save-files as input/RT
;   09.09.2015 Added a slider for atmexp to control slope above z=0. Increased
;              size of plot window, improved ranges of variables, xyout labels
;              for plotted quantities, improved ranges and margins for plot.
;              Only write to log if not cancelled./RT
;
function plotf,dz1,z1,bzone,z_0,w,a,expn,atmexp
	common intrpcom,nx1,ny1,nz1,nx2,ny2,nz2,it1,it2,scr1fil,scr2fil,tabfil,halv

	f=(1./(abs(.7*(z1-z_0))>.9*w)^expn+a*exp(-((z1-z_0)/w)^4))
;	f = f/(.4*((z_0-z1-w) > 0.0)^.3+1.)
;	f=(1./(abs(z1-z_0)>1.26*w)^expn+a*exp(-((z1-z_0)/w)^4))
	f(0) = f(1)/bzone
	n0 = max([1, min(where(z1 LE (-.2*(z1(nz1-1)-z1(1)))))])
	if (n0 NE 1) then print, 'n0=', n0
;	n0 = 1
;	sfn = n0+integf(z1(n0:*),f(n0:*))*(nz2-n0-1.0)/max(integf(z1(n0:*),f(n0:*)))
;	help, sfn, f, n0, nz1
	sfn = fltarr(nz1-n0)
	sfn(0) = 0.0
	for i=1,nz1-n0-1 do sfn(i) = sfn(i-1) + f(i)*(z1(i+1)-z1(i))
	sfn(0) = n0
	for i=1,nz1-n0-1 do sfn(i) = n0+sfn(i)*(nz2-n0-1.0)/sfn(nz1-n0-1)
	newz = fltarr(nz2)
	nzrat = nz1/float(nz2)
	for n=1,nz2-1 do newz(n) = intrp1(sfn, [n], z1(1:*))
;	newz(0) = newz(1) - bzone*(newz(2)-newz(1))
	newz2 = fltarr(nz2-2) & for i=2,nz2-1 do newz2(i-2)=.5*(newz(i)+newz(i-1))
	newdz = fltarr(nz2-2) & for i=2,nz2-1 do newdz(i-2)=newz(i)-newz(i-1)
	dum = min(newdz, imin)
	newdz(0:imin) = (newdz(0:imin)-newdz(imin))*atmexp + newdz(imin)
	for i=1, nz2-3 do newz2(i) = newz2(i-1) + newdz(i)
	newdz2 = smoothfx(newz2, newdz)
	for i=1, nz2-3 do newz2(i) = newz2(i-1) + newdz(i)
	newdz2 = smoothfx(newz2, newdz)
	newdz = newdz2/total(newdz2(0:*))*(newz(nz2-1)-newz(1))
	if (0) then begin
;		newdz = z1(1:nz1-1) - z1(0:nz1-2)
		newdz(0:17) = newdz(0:17)/(1.0 + (1.-[0,findgen(17)/17.])*1.5525)
;		newdz(0) = newdz(1)*bzone
;		newz(nz2-1) = z1(nz1-1)
;		for i=nz2-2,0,-1 do newz(i) = newz(i+1)-newdz(i)
	endif
	for i=2,nz2-1 do newz(i) = newz(i-1)+newdz(i-2)
	newz(0) = newz(1) - bzone*(newz(2)-newz(1))
;	plot,newz,newz(1:nz2-1)-newz(0:nz2-2),psym=-1,syms=.3,ysty=2,xmarg=[5,2]
    dznew  = newz(1:nz2-1)-newz(0:nz2-2)
    dzold  = nzrat*(z1(1:nz1-1)-z1(0:nz1-2))
    dzd2T  = nzrat*dz1
    delx   = 0.01*(z1(nz1-1L)-z1(0L))
    xrange = [min([z1(0L),newz(0L)])-delx, max([z1(nz1-1L),newz(nz2-1L)])+delx]
    yrange = [0, max([dznew(1:*), dzold(1:*), dzd2T(1:*)])]
    cs     = 0.8*!P.charsize
	plot,newz,dznew, xsty=1,xr=xrange, ysty=1,yr=yrange
    xyouts, .3, .90, 'New !7D!8z!x', /NORM, chars=cs
	oplot, z1, dzold, col=(!D.TABLE_SIZE-1)*.8
    xyouts, .3, .85, 'Old !7D!8z!x', /NORM, col=(!D.TABLE_SIZE-1)*.8, chars=cs
	oplot, z1, dzd2T, col=(!D.TABLE_SIZE-1)*.5
    xyouts, .3, .80, 'max!dh!n!3|!xd!u2!n!8T!x/d!8z!x!u2!n!3|!x', /NORM, $
                                            col=(!D.TABLE_SIZE-1)*.5, chars=cs
;	stop
	return, newz
end

pro zshift_w_event, ev
	common zshift_w_com,base,z_0_slider,w_slider,a_slider,expn_slider, $
				atmexp_slider,waitlabel,index,z_0,w,a,expn,atmexp
	common cdat,x,y,z,nx,ny,nz,nw,ntmax,date0,time0
	common plotf_com,z1,dz1,bzone,nn,newz
	common intrpcom,nx1,ny1,nz1,nx2,ny2,nz2,it1,it2,scr1fil,scr2fil,tabfil,halv

	varname=["", "lnrho", "ux", "uy", "uz", "ee", "T"]
    WIDGET_CONTROL, ev.id, get_uvalue=uval
  case (uval) of
    1: z_0    = ev.value
    2: w      = ev.value
    3: a      = ev.value
    4: expn   = ev.value
    5: atmexp = ev.value
    6: begin
		if (nn EQ 0) then begin
			print, 'new_z:', newz(0), newz(nz2-1)
			print, 'old_z:', z1(0), z1(nz1-1)
		endif
		nn = nn + 1
		widget_control, z_0_slider, SENSITIVE=0
		widget_control, w_slider, SENSITIVE=0
		widget_control, a_slider, SENSITIVE=0
		widget_control, expn_slider, SENSITIVE=0
		if (nn GT 6) then begin
			WIDGET_CONTROL, /DESTROY, base
; Take care of meanrhoT.F77.dat
			meanrhot_update, z1, newz, /FORCE;  =FORCE
			print, ""
			print, "   Remember to set ntime=-1 in the run-script."
			print, ""
            sarg = string(z_0,w,a,expn,atmexp,for='("arg=[",5(F0.4,:,","))')+"]"
            add_to_log, sarg
			goto, done
		endif
       end
    7: begin
		WIDGET_CONTROL, /DESTROY, base
		goto, done
       end
  endcase

  cur_win = !d.window
  wset, index
  if (nn EQ 0) then begin
	newz = plotf(dz1,z1,bzone,z_0,w,a,expn,atmexp)
  endif else begin
	widget_control, /HOURGLASS, waitlabel, set_value="Processing "+varname(nn)
	intrp_var, nn, z1, newz
	if (nn EQ 2) then begin
		nn = nn + 1
		widget_control, waitlabel, set_value="Processing "+varname(nn)
		intrp_var, nn, z1, newz
		nn = nn + 1
		widget_control, waitlabel, set_value="Processing "+varname(nn)
		intrp_var, nn, z1, newz
	endif
  endelse
  widget_control, waitlabel, set_value=""
  if (cur_win ne -1) then wset, cur_win

done:
end

pro zshift_w, scr1file, Teff=Teff, scr2file=scr2file, tabfile=tabfile, $
        it1in=it1in, it2in=it2in, bzone1=bzone1, halve=halve,GROUP=GROUP,arg=arg
	common coff,mpar,mav,ipack,incr,ioff,time,voff,cfile,irec
	common cdat,x,y,z,nx,ny,nz,nw,ntmax,date0,time0
	common zshift_w_com,base,z_0_slider,w_slider,a_slider,expn_slider, $
				atmexp_slider,waitlabel,index,z_0,w,a,expn,atmexp
	common plotf_com,z1,dz1,bzone,nn,newz
	common intrpcom,nx1,ny1,nz1,nx2,ny2,nz2,it1,it2,scr1fil,scr2fil,tabfil,halv

	if (XREGISTERED('zshift_w')) then return    ; Only one copy at a time
    rt_graphics
	loadct, 7
	nn=0 & answ=''
	default, scr1file, stamnavn()+'.scr'
	if (strpos(scr1file, '.scr') LT 0 AND strpos(scr1file, '.sav') LT 0) then $
														scr1file=scr1file+'.scr'
	default, scr2file, scr1file
	if (strpos(scr2file, '.scr') LT 0 AND strpos(scr2file, '.sav') LT 0) then $
														scr2file=scr2file+'.scr'
	print, scr1file, ' ', scr2file
	default, tabfile, stamnavn()+'.tab'
	if (strpos(tabfile, '.tab') LT 0) then tabfile=tabfile+'.tab'
	if KEYWORD_SET(halve) then halv=1  else  halv=0
	scr1fil = scr1file
	scr2fil = scr2file
	tabfil  = tabfile
	clear_log

	file, scr2file, /QUIET
	nx2=nx & ny2=ny & nz2=nz
	file, scr1file, /QUIET & if (incr GT 10L) then ipack=1L
	default, it1in, 0L	; source timestep
	default, it2in, 0L	; destination timestep
	it1=it1in & it2=it2in
	nx1=nx & ny1=ny & nz1=nz
	z1 = z
	z1 = z1 - intrp1(cdata(it1,'ttm'), Teff, z1)
	zmin  = z1(1)& zmax=max(z1)
	depth = zmax-zmin
    z0mn  = zmin& z0mx=min([-3*z0mn, .5*zmax])
; Default values:  z_0       w       a   expn atmexp
    arg0  =   [0.3*z0mx, 0.3*z0mx, 1.03, 0.57, 0.5]& Narg0=N_ELEMENTS(arg0)
	default, arg, arg0&                              Narg =N_ELEMENTS(arg)
    if (Narg LT Narg0) then arg=[arg, arg0(Narg:*)]
	default, bzone1, cpar(it1, 'bzone')
    bzone = bzone1
	default, Teff, cpar(it1, 'teff')
	print, 'solving for Teff=',Teff
	maxdTdz=fltarr(nz1) & maxd2Tdz2=fltarr(nz1)
	TT = ftt(it1)
	dTdz = zder(TT)
	d2Tdz2 = zder(dTdz)
	for i=0,nz1-1 do maxdTdz(i)   = max(abs(dTdz(*,*,i)))
	for i=0,nz1-1 do maxd2Tdz2(i) = max(abs(d2Tdz2(*,*,i)))
	dz1 = depth/total(1./sqrt(maxd2Tdz2))/sqrt(maxd2Tdz2)
	z_0=arg(0) & w=arg(1) & a=arg(2) & expn=arg(3) & atmexp=arg(4)

	scrdim = screen_dim()
	xsiz =long(scrdim(0)*.7) & ysiz=xsiz
	sxsiz=xsiz-60
	base = widget_base(/column, title='z-mesh')
	draw = widget_draw(base, xsize=xsiz, ysize=ysiz, /FRAME)
;	dysiz = scrdim(1)-(ysiz+747L); for ysiz=650
	dysiz = scrdim(1)-(2L*ysiz+98L)
;	print, ysiz, scrdim(1)-dysiz
	if (dysiz LT 0) then begin
		c_base_3 = widget_base(base, /column, xsize=xsiz, $
				/scroll, scr_xsize=xsiz, scr_ysize=ysiz+dysiz)
	endif else begin
		c_base_3 = widget_base(base, /column, xsize=xsiz)
	endelse
;
; Initialize the slider, incl name and range.
	  z_0_slider = cw_fslider(c_base_3, title="Zero point", value=z_0, $
			FORM='(F0.4)',/DRAG,uvalue=1,min=z0mn,max=z0mx,xsize=sxsiz)
	  w_slider = cw_fslider(c_base_3, title='Width of plateau', value=w, $
			FORM='(F0.4)', /DRAG, uvalue=2, max=z0mx*.5,xsize=sxsiz)
	  a_slider = cw_fslider(c_base_3, title='Gauss/lin-ratio', value=a, $
			FORM='(F0.4)', /DRAG, uvalue=3, max=6.0,xsize=sxsiz)
	  expn_slider = cw_fslider(c_base_3, title='Exponent', value=expn, $
			FORM='(F0.4)', /DRAG, uvalue=4, max=2.0,xsize=sxsiz)
	  atmexp_slider = cw_fslider(c_base_3, title='Atm. exponent', value=atmexp,$
			FORM='(F0.4)', /DRAG, uvalue=5, max=1.5,xsize=sxsiz)
	c_base_2 = widget_base(base, /row, space=50)
	  ok_button     = widget_button(c_base_2, value='OK', uvalue=6)
	  cancel_button = widget_button(c_base_2, value='Cancel', uvalue=7)
	  waitlabel     = widget_label(c_base_2, value="                  ")
	widget_control, base, /REALIZE
	widget_control, draw, get_value=index
	widget_control, z_0_slider,   set_value=z_0
	widget_control, w_slider,     set_value=w
	widget_control, a_slider,     set_value=a
	widget_control, expn_slider,  set_value=expn
	widget_control, atmexp_slider,set_value=atmexp
	cur_win = !d.window
	wset, index
	!x.title='z/!8[!xMm!8]!x'
	!x.margin=[8,1]
	!y.margin=[4,1]
	!y.title='dz/!8[!xMm!8]!x'
	newz = plotf(dz1,z1,bzone,z_0,w,a,expn,atmexp)
	if (cur_win ne -1) then wset, cur_win
	XMANAGER, 'zshift_w', base, GROUP_LEADER = GROUP
	reset_graphics
	print, 'Welcome back!'
end
