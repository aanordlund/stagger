;
;   Program to extend the atmosphere of a simulation, by a factor of ff.
;   When z(0) is intended not to change but the rest of z needs to catch-up,
;   use;    f=z(0)/(z(1)-cpar(0,'bzone')*(z(2)-z(1))) 
;
;   Update history:
;   ---------------
;   21.04.2003 Coded/RT
;   16.03.2006 Automatic printing to log-file./RT
;	16.04.2006 No longer forcing ee(*,*,0) = eem(0)/RT
;   15.05.2006 Corrected bug in xrange of first plot/RT
;              Added option to quit in first user-prompt/RT
;   15.05.2006 Completely new approach using intrp_var (old in .bak2) /RT
;   08.09.2015 Adjusted for new behaviour of s/r defaultscr/RT
;
;   Known bugs: Use hydrostatic extrapolation of rho in intrp_var.
;
pro extend, ff, scr1file, scr2file=scr2file, tabfile=tabfile, STOP=STOP
    common coff,mpar,mav,ipack,incr,ioff,time,voff,xfile,irec
	common cdat,x,y,z,nx,ny,nz,nw,ntmax,date0,time0
	common ctable,ttmean,rhm,drhm,eemin,eemax,dee,mtab,itab,tab
	common intrpcom,nx1,ny1,nz1,nx2,ny2,nz2,it1,it2,scr1fil,scr2fil,tabfil,halv

	defaultscr, scrfile=scr1file, sim=sim
	if (strpos(scr1file, '.scr') LT 0) then  scr1file=scr1file+'.scr'
	nx1 = nx & ny1 = ny
	default, scr2file, scr1file
	default, tabfile,  sim+'.tab'
	it1 = ntmax-1
;	print, scr1file, scr2file, tabfile, form='(A0)'
	file, scr2file, /QUIET& nx2 = nx & ny2 = ny
	it2 = 0
	scr1fil = scr1file   &  scr2fil=scr2file   &  tabfil=tabfile  & halv=0
	c0 = .5*(!D.TABLE_SIZE-1.)
	c1 = .7*(!D.TABLE_SIZE-1.)
	dc = .065*(!D.TABLE_SIZE-1.)

	fx = ((z/z(1)>0.0)^2)
	z2 = z*(fx*(ff-1.)+1.)
	z2(0) = z2(1) - cpar(0,'bzone')*(z2(2)-z2(1))
    device = strlowcase(!D.NAME)
	for ivar=1, 6 do begin
		intrp_var, ivar, z, z2
        if (device EQ 'x'  AND  ivar LT 6) then begin
            s = prompt(' Next plot? [y/n/q/s]: ')
            if (s EQ 'q'  OR  s EQ 'n'  OR  s EQ 'r') then begin
                reset_graphics
                return
            endif
            if (s EQ 's')                             then stop
        endif

	endfor

	print, ""
	print, " WARNING: Need to apply hydequalize and zshift_w repeatedly!!!"
	print, ""

	add_to_log, /CLEAR

	if KEYWORD_SET(STOP) then stop
end
