;
; Program to change the surface gravity, g0 -> g1, or the mean-molecular
; weight, mu0 -> mu1, by rescaling spatial dimensions.
;
;  fg = g1/g0 or mu1/mu0 in case /NOG is set.
;
; The program changes 'height', 'width', 'dx', 'dy', 'dz' and 'z'
; of the scratch-file.
;
;   Update history:
;   ---------------
;   04.10.2000 Coded/RT
;   23.08.2004 Introduce choise of output-file.
;	27.08.2004 Automatic printing to log-file./RT
;   30.11.2005 Tried to open undefined scrfile - fixed that/RT
;   08.10.2007 Will return an estimate of the new Teff=Teff1, based on adiabatic
;              interpolation in Smax of the simulation-grid. Teff1 is written
;              to the cpar-block and a name for the new sim is suggested/RT
;   10.10.2007 Added argument 'cmd', to be used for supplying the calling
;              sequence, when not issued interactively/RT
;   18.10.2007 The (Lfg < 0) case also seems to work for (Lgf >= 0) /RT
;   26.04.2010 Extrapolate the Smin surface and remove a larger 2-point margin,
;              since extrapolation in trigrid means quintic interpolation/RT
;
pro re_g, fg, scr0file, scr1file=scr1file, it0=it0, it1=it1, Teff0=Teff0, $
        Teff1=Teff1, cmd=cmd, NOG=NOG, XYONLY=XYONLY, QUINTIC=QUINTIC, $
        NOTEFF=NOTEFF, STOP=STOP
@cstarval
@consts
	common coff,mpar,mav,ipack,incr,ioff,time,voff,xfile,irec
	common cdat,x,y,z,nx,ny,nz,nw,ntmax,date0,time0

    default, scr0file, stamnavn()
    if (strpos(scr0file,'.scr') LT 0  AND  strpos(scr0file,'.sav') LT 0) then $
			 scr0file= scr0file+'.scr'
	default, scr1file, scr0file
    if (strpos(scr1file,'.scr') LT 0  AND  strpos(scr1file,'.sav') LT 0) then $
			 scr1file= scr1file+'.scr'
	Z_TOO = (NOT KEYWORD_SET(XYONLY))

	file, scr0file, /QUIET
	default, it0, 0L
	it0 = (it0>0L)<(ntmax-1L)
;	ipack=1
	grav0   = cpar(it0,'grav')
	height0 = cpar(it0,'height')
	width0  = cpar(it0,'width')
	dx0     = cpar(it0,'dx')
	dy0     = cpar(it0,'dy')
	dz0     = cpar(it0,'dz')
	z0      = z
	if (KEYWORD_SET(NOG)  OR  NOT file_same(scr0file, scr1file)) then begin
		tt0 = ftt(it0)
		lnr0= flnrho(it0)
	endif
    Ptot = cdata(it0,'ppm')+cdata(it0,'ptm')
	plot,z,zder1(alog(Ptot))*Ptot/cdata(it0,'rhom')/grav0, $
                            yr=[.9,1.1], xtitl='!8z!5/[!xMm!5]!x', $
                            ytitl='!s!ad!8P!x!r!s!bd!8z!r--!n/(g!7q!x!n)'
	oplot, !x.crange, [1,1], line=1
	if (NOT file_same(scr0file, scr1file)) then begin
; Read cpar-block
		cp = assoc(1, bytarr(4L*mpar), irec(it0*incr+incr-1))
		cpar0 = cp(0)
; Read cdata-block
		cd = assoc(1, fltarr(nz, mav), irec(it0*incr+incr-1)+4L*mpar)
		cdata0 = reform(cd(0), nz, mav)
; Read surface intensity
		Is = assoc(1, fltarr(nx,ny), irec(it0*incr+incr-1)+4L*(mpar+nz*mav))
		Int0 = Is(0)
; Read velocity-cubes
		ux0 = fux(it0)
		uy0 = fuy(it0)
		uz0 = fuz(it0)
	endif
	ipack=1
	file, scr1file, /WRITE, /QUIET
	print, " Writing to "+scr1file+"."
	default, it1, 0L
	it1 = (it1>0L)<(ntmax-1L)
; New z-scale
	if (Z_TOO) then z = z/fg
	if (NOT file_same(scr0file, scr1file)) then begin
; Write cpar-block
		cp = assoc(1, bytarr(4L*mpar), irec(it1*incr+incr-1))
		cp(0) = cpar0
; Write cdata-block
		cd = assoc(1, fltarr(nz,mav),irec(it1*incr+incr-1)+4L*mpar)
		cd(0) = reform(cdata0, nz*mav)
; Write surface intensity
		Is = assoc(1, fltarr(nx,ny), irec(it1*incr+incr-1)+4L*(mpar+nz*mav))
		Is(0) = Int0
; Write data-cubes
		wvar, lnr0, it1, 0
		wvar, ux0,  it1, 1
		wvar, uy0,  it1, 2
		wvar, uz0,  it1, 3
		wvar, tt0,  it1, 5
	endif
	if (KEYWORD_SET(NOG)) then begin
		print, 'Is '+stamnavn()+'.tmp updated? ', form='(A0,$)'
		stra = strlowcase(get_kbrd(1)) & print, stra
		if (stra EQ 'n') then begin
			print, 'Then calculate a new table and run re_g again.'
			file, scr1file, /QUIET
			return
		endif
		tabtmp, stamnavn()+'.tmp', mz=nz
		print, 'Old values for ".eetop,eebot,rhotop,rhobot,pptop,ppbot":'
		print, cpar(it1, 'eebot'), cpar(it1, 'rhobot'), cpar(it1, 'ppbot'), $
						format='("0.0,",F8.5,",0.0,",F6.1,",0.0,",F6.1,",")'

		ppmin = fltarr(nz) & ppmax = fltarr(nz)
		ee = lookup_tmp(lnr0, tt0, iv=2)
		eem = haver(ee)
		ee(*,*,0) = eem(0)
		wvar, ee, it1, 4
		lnP = lookup_tmp(lnr0, tt0)
		for i=0,nz-1 do ppmin(i) = exp(min(lnP(*,*,i)))
		for i=0,nz-1 do ppmax(i) = exp(max(lnP(*,*,i)))
		ppm = haver(exp(lnP))
		eebot = cpar(it1,'eebot') - (cdata(it1,'eem'))(nz-1) + eem(nz-1)
		ppbot = cpar(it1,'ppbot') - (cdata(it1,'ppm'))(nz-1) + ppm(nz-1)
		wdata, eem,   it1, 'eem'
		wdata, eem,   it1, 'ee0'
		wdata, ppmin, it1, 'ppmin'
		wdata, ppmax, it1, 'ppmax'
		wdata, ppm,   it1, 'ppm'
		wdata, ppm,   it1, 'pp0'
		wpar,  eebot, it1, 'eebot'
		wpar,  ppbot, it1, 'ppbot'
		oplot,z,zder1(ppm+cdata(it1,'ptm'))/cdata(it1,'rhom')/cpar(it1,'grav'),$
												line=2,color=(!D.TABLE_SIZE-1.)*.6
		print, 'New values for ".eetop,eebot,rhotop,rhobot,pptop,ppbot":'
		print, eebot,cpar(0, 'rhobot'),ppbot, $
						format='("0.0,",F8.5,",0.0,",F6.1,",0.0,",F6.1,",")'
;		rho_e_bot, it1
	endif else begin
		if (Z_TOO  AND  (NOT KEYWORD_SET(NOTEFF))) then begin
; Retrieve Smax from simulations with l2means.dat files
;			dirs = read_startab(mixis='AG89Fe', resis='150x150x82', $
;														abundis=0.0, /LMEAN)
            s = prompt(" Teff-logg diagram of Smax? [y/n/q/s]: ")
            if (s EQ 'n'  OR  s EQ 'q'  OR  s EQ 'r') then return
            if (s EQ 's')                             then stop
			starvalplot, 'Smax', /TV, QUINTIC=QUINTIC
; Triangulation in the atmospheric parameters of the simulations of the grid.
			LTeff = alog10(Tefobs)
			Logg  = alog10(grav)
			triangulate, LTeff*1e1, Logg, TR, B
; The current Teff
			isim = (where(name EQ stamnavn()))(0)
			if (isim GE 0) then Teff00 = Tefobs(isim) $
						   else Teff00 = (-(cdata(0,'frad'))(1)/sig*$
							cpar(0,'uur')*(cpar(0,'uul')/cpar(0,'uut'))^3)^.25
			default, Teff0, Teff00
; Interpolate the grid of simulations to a regular grid
; Generate grid with g0 and Teff0 and covering g0-g1 and Teff0 to ...
; Perform contour calculation for Smax(Teff0, g0) and interpolate to g=g1
			dLTdLg = 0.1+0.02
			Lfg   = alog10(fg)
			NLg   = (long(abs(Lfg)*50L)>20L)<100L
			NLT   = (long(abs(Lfg)*dLTdLg*100L)>20L)<150L
			Lg1   = alog10(grav0)+Lfg+4.0
			oplot, !x.crange, [1,1]*Lg1, line=1
;			if (Lfg LT 0.0) then begin
				xout  = alog10(Teff0) + Lfg*dLTdLg*(findgen(NLT) - 2. - $
												   0*(fix(NLT*.1)>1.))/(NLT-5.)
				yout  = alog10(grav0) + Lfg*(findgen(NLg)-2.)/(NLg-5.) + 4.0
				plots, xout#replicate(1,NLg), replicate(1,NLT)#yout, psym=3
;			endif else begin
;				xout  = alog10(Teff0) + findgen(NLT)/(NLT-1.)
;				yout  = 
;			endelse
;   		surf  =trigrid(LTeff,Logg,Smax,TR,xout=xout,yout=yout,QUINT=QUINTIC)
			surf  =trigrid(LTeff,Logg,Smax,TR,xout=xout,yout=yout,EXTRAPOLATE=B)
			surf  = surf(0:NLT-3, 0:NLg-3)
			xout  = xout(0:NLT-3) & yout=yout(0:NLg-3)
			xysave= [!x.crange, !y.crange, !x.window, !y.window, !x.s, !y.s]
			contour,surf,xout,yout,levels=surf(2,0),PATH_XY=xy,$
					PATH_INFO=xy_info,CLOSED=0,/PATH_DATA_COORDS, $
					min_value=min_value, max_value=max_value
			!x.crange = xysave(0:1)& !x.window=xysave(4:5)& !x.s=xysave(8:9)
			!y.crange = xysave(2:3)& !y.window=xysave(6:7)& !y.s=xysave(10:11)
			Ntot = 0L
			for i=0L, N_ELEMENTS(xy_info)-1L do begin
				ip = lindgen(xy_info(i).N)
				if (xy_info(i).TYPE EQ 1) then  ip = [ip, 0]
				Np = N_ELEMENTS(ip)
				xp = reform(xy(0,xy_info(i).OFFSET + ip))
				yp = reform(xy(1,xy_info(i).OFFSET + ip))
				plots, xp, yp
				ix = (where(abs(xp-alog10(Teff0))    LT 0.01  AND  $
						   abs(yp-alog10(grav0)-4.) LT 0.01))(0)
				if (ix GE 0L) then begin
					Teff1 = 1e1^intrp1(yp, Lg1, xp)
					Ntot  = Ntot + 1L
				endif
			endfor
			if (Ntot NE 1) then begin
				print, "ERROR: I'm a bit confused; I have " + $
							string(N_ELEMENTS(xy_info),form='(I0)')+" contours"
				print, "       and "+string(Ntot,form='(I0)') + $
							" intersect with the origonal simulation."
				print, "       Bailing out!"
				stop
			endif
; The Teff of the destination
			plots, alog10(Teff1), Lg1, psym=2, th=2, col=(!D.TABLE_SIZE-1.)*.86
            print, " The projected Teff of the new simulation is, Teff1=",Teff1
			print, " You might call the new simulation: " + $
								string(round(Teff1*0.01),round(Lg1*10), $
								form='("t",I2,"g",I2,"m+00")')
			wpar, round(Teff1/10.)*10., it1, 'teff'
            add_to_log, "Teff -> "+string(Teff1, form='(f8.2)'), cmd=cmd, /CLEAR
		endif
        wpar, grav0*fg, it1, 'grav'
	endelse
	if (Z_TOO) then begin
		wpar, height0/fg, it1, 'height'
		wpar, dz0/fg,     it1, 'dz'
		wdata,z,          it1, 'z'
	endif
	wpar,     width0/fg,  it1, 'width'
	wpar,     dx0/fg,     it1, 'dx'
	wpar,     dy0/fg,     it1, 'dy'
;
; Re-open the file without write-permission to avoid accidental overwriting.
	file, scr1file, /QUIET

	if KEYWORD_SET(STOP) then stop
end
