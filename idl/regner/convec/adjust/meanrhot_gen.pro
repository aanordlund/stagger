;
;   Program to calculate the 1D-stratification to be used for
;   the calibratory 1D radiative transfer.
;
;   Update history:
;   ---------------
;   23.02.2001 Coded/RT
;   13.04.2004 Added the option of including a z-scale and to expand
;              the mean stratification to cover lnrho(*,*,0)/RT
;   15.04.2004 Now writing the file using the routine `write_meanrhot`/RT
;	16.04.2004 Using s/r default_scr and introduced the scrfile arg/RT
;	07.05.2004 Corrected treatment of new + old combos.
;              Also use s/r read_meanrhot to read old meanrhoT-files/RT
;   17.06.2004 New treatment of excluded points (interpolation).
;   29.07.2004 Include extrapolation past min(lnr(*,*,0)) by means of
;              the new parameter lnrmarg with default value of 1.5/RT
;   27.08.2004 Automatic printing to log-file./RT
;   18.02.2005 In smoothing by fitting a polynomial to upper part, ensure
;              fit to have value and gradient of upper fitting point/RT
;   24.03.2007 Exclude the transition point between the original scale
;              and the extrapolation in the smooth polynomial fit/RT
;   19.06.2007 Automatic calculation of new table (in background) and logging
;              to NOTES-file. The PID of the proccess is returned in pid/RT
;   18.09.2007 Added argument 'cmd', to be used for supplying the calling
;              sequence, when not issued interactively/RT
;   02.10.2007 The logging of 'make'-activity is now handled by the make-file/RT
;   01.12.2008 Don't use polynomial fit, pfit, to meanT above, the photosphere
;              - use smoothfx(zm, meanT-pfit)+pfit instead. This leaves in
;              physical 'bumps' that cannot be resolved with small Npol, and
;              results in numerical artifacts with large Npol/RT
;   13.05.2009 Added keyword TAU for computing \tau-averages/RT
;   03.06.2009 Fixed handling of previous meanrhoT-files with differing nz/RT
;   22.03.2010 Use ftau for computing ab for /TAU, instead of the hardcoded
;              and potentially non-existing kaprhoT.tab through lookup_rad/RT
;   10.09.2015 Base projected Smin, Smax, Ptmax, Ptotph on call to
;              starval_intrp (with /LOG) and differential application to sim0.
;              If S1min>S1max, arbitrarily set target entropy jump to half
;              the source entropy jump and keep S1max./RT
;   29.10.2015 Fixed bug in extrapolation, where dz could become negative.
;              Now constrained to be between 0.5 and 2.0 times dz(2).
;              Also improved plot ranges and annotations./RT
;   12.11.2015 Weed out not used entries in list of meanrhoT-files, to avoid
;              alog10(0.0) in calculation of plot-ranges./RT
;   12.01.2016 Avoid plotting the T(z), rho(z) structure of scratch-file, since
;              it isn't used in the average, and it is wrong if /TAU is set./RT
;
;   Known bugs: Assumes a kaprhoT.tab is present when setting /TAU.
;
pro meanrhot_gen, mins, Npolmax=Npolmax, Nmean=Nmean, scrfile=scrfile, $
		lnrmarg=lnrmarg, iexcl=iexcl, it=it, pid=pid, files=files, cmd=cmd, $
		TAU=TAU, NOZ=NOZ, IMEAN=IMEAN, SCRATCH=SCRATCH,PATCHUP=PATCHUP,STOP=STOP
	common coff,mpar,mav,ipack,incr,ioff,time,voff,xfile,irec
	common cdat,x,y,z,nx,ny,nz,nw,ntmax,date0,time0

	default, lnrmarg, 1.5
	default, Npolmax, 6L
	default, Nmean,   3L
	default, files,  '??'
	Nexcl = N_ELEMENTS(iexcl)
	pid = '-1'
	e = 0.5
    rt_graphics
	c0 = (!D.TABLE_SIZE-1.)*(1.-e)
	c1 = (!D.TABLE_SIZE-1.)*(1.-e*.5)
	dc = (!D.TABLE_SIZE-1.);	*e
;
; Find and load the default scratch-file
	defaultscr, scrfile=scrfile
;	scrfile = findfile(stamnavn()+'.scr')
	if KEYWORD_SET(SCRATCH) then begin
		fsav = scrfile
	endif else begin
;		fsav = [findfile(stamnavn()+files+".sav")]; , scrfile]
		fsav = simlist(files, /NOSCRATCH)
		fsav = fsav(where(fsav NE ""))
	endelse
	Nfs  = N_ELEMENTS(fsav)
	if (Nfs LE 0) then begin
		if KEYWORD_SET(SCRATCH) then begin
			print, " Couldn't find the "+scrfile+"-file."
		endif else begin
			print, " Couldn't find any "+stamnavn()+"??.sav-files."
		endelse
		print,     " Bailing out!" & return
	endif

	tmax = fltarr(Nfs)
	scrfile = fsav(Nfs-1L)
	file, scrfile, /QUIET
	print, scrfile, ntmax, time(0)
	default, itscr, 0L
;	default, mins, 0.5*cpar(0,'dtsave')/60.*cpar(0,'uut');	Less than a timestep
	if KEYWORD_SET(SCRATCH) then mins=cpar(itscr,'dtsave')*1e2/6e1
	if (N_ELEMENTS(mins) LE 0) then read, 'Averaging time/[minutes]: ', mins
	OLD_MEAN = read_meanrhot(meanrho0, meanT0, z0)
	if (OLD_MEAN) then begin
		print, 'Found the current meanrhoT-file...'
        spawn, 'll meanrhoT.F77.dat', flist
        sdate0 = string((str_sep(strcompress(flist(0)), ' '))(6:8), $
                                        form='(A0,"!d !n",A0,",!d !n",A0)')
	endif else begin
		meanrho0 = cdata(itscr,'rhom')
		meanT0   = cdata(itscr,'ttm')
	endelse
	if (N_ELEMENTS(z0) NE N_ELEMENTS(meanT0)) then $
			z0 = z  - intrp1(meanT0, cpar(itscr,'teff'), z) $
	else	z0 = z0 - intrp1(meanT0, cpar(itscr,'teff'), z0)
	tscr = time(ntmax-1L)
;  Number of timesteps to cover the requested averaging time.
;	nt = long(mins*60./cpar(itscr,'uut')/30.0 + 0.5)
	nt = long(mins*60./cpar(itscr,'uut')/cpar(itscr, 'dtsave') + 0.5)

	meanrho = fltarr(nz) & meanT = fltarr(nz)
	mrho = fltarr(nz, nt) & mtt = fltarr(nz, nt)
;
;  Find old, backed up meanrhoT.F77.dat-files.
	spawn, 'll meanrhoT.F77.dat.bak?*', flist
	fmprev = findfile('meanrhoT.F77.dat.bak?*')
	if (fmprev(0) NE '') then begin
		jj = sort(long(strmid(fmprev, 20, 50)))
		Nprev = N_ELEMENTS(jj)
		if (Nmean GT Nprev) then Nmean=Nprev
		color_scale, 0, min([Nprev, Nmean]), x0=0.8, y0=0.2
		dc = dc/min([Nprev, Nmean]+1)
		meanrhoprv=fltarr(nz) & meanTprv=meanrhoprv
		mrprv = fltarr(nz,Nprev)
		mTprv = fltarr(nz,Nprev)
		imin = Nprev-Nmean
		print, imin, Nprev, Nmean
        sdate = strarr(Nprev)
	endif else begin
		imin  = 0L
		Nprev = 0L
	endelse
;  Load and plot the old meanrhoT.F77.dat.bak?*-files.
	for i=imin, Nprev-1 do  begin
		ext = strmid(fmprev(i),17,99)
		if (read_meanrhot(meanrhoprv, meanTprv, dir='./', ext=ext)) then begin
			nzprv = min([N_ELEMENTS(meanrhoprv), nz]) - 1L
			mrprv(0:nzprv,i) = meanrhoprv(0:nzprv)
			mTprv(0:nzprv,i) = meanTprv(0:nzprv)
			sdate(i) = string((str_sep(strcompress(flist(jj(i))), ' '))(6:8), $
                                        form='("!d",A0," ",A0," ",A0," !n")')
		endif
	endfor
    if (Nprev GT 0L) then begin
        iprv = where(mTprv(0L,*) GT 1.0)
        sdate = sdate(iprv) & Nprev=N_ELEMENTS(iprv)& imin=0L
        mrprv = mrprv(*,iprv)
        mTprv = mTprv(*,iprv)
    endif
;  Temperature- and density-ranges for the plots.
	rr = alog10([min([cdata(itscr,'rhom'), meanrho0], max=rrmax), rrmax]) - 7.
	Tr = alog10([min([cdata(itscr,'ttm'), meanT0],    max=Trmax), Trmax])
    if (Nprev GT 0L) then begin
        rr = [min([rr,alog10(reform(mrprv, Nz*Nprev))-7], max=rrmax),  rrmax]
        Tr = [min([Tr,alog10(reform(mTprv, Nz*Nprev))],   max=Trmax),  Trmax]
    endif
	plot, alog10(cdata(itscr,'rhom'))-7, alog10(cdata(itscr,'ttm')), /NODATA, $
			xsty=2, xtit='Log!i !n!7q!x', ytit='Log!i !n!8T!x', xr=rr, yr=Tr
	oplot, !x.crange, [1,1]*cpar(itscr,'teff'), line=1
;
;  Loop over and plot previous meanrhoT-files
	for i=imin, Nprev-1 do  begin
        oplot, alog10(mrprv(*,i))-7, alog10(mTprv(*,i)), col=dc*(i-imin+1)
        xyouts, alog10(mrprv(0,i))-7, alog10(mTprv(0,i)), sdate(i), $
											align=1, col=dc*(i-imin+1)
    endfor
;
;  Loop over and check all sav- and scratch-files
	for i=0, Nfs-1 do begin
;  The file exists... but is empty! - delete it.
		if (file_test(fsav(i), /ZERO_LENGTH)  OR  $
			file_test(fsav(i), /DANGLING_SYMLINK)) then begin
			tmax(i) = -1
			spawn, 'rm '+fsav(i), cmdstat
			print, 'removed '+fsav(i), cmdstat
		endif
		
		if (file_test(fsav(i))) then begin
;  The file exists and is not empty
			file, fsav(i), /QUIET
			print, fsav(i), ntmax, time(ntmax-1)
			if KEYWORD_SET(SCRATCH) then ntmax=1L
			tmax(i) = time(ntmax-1)
		endif else begin
;  The file doesn't exist
			tmax(i) = -1
		endelse
	endfor
	ii = sort(tmax)
	stmax = tmax(ii)
;  Index of the oldest sav-file needed
	ii0 = max(where(stmax LE tscr))
	it = 0
	print, ii, ii0
;
;  points to exclude from the fitting proccesses
	excl = intarr(nz)*0
	if (Nexcl GT 0) then excl(iexcl) = 1
	iincl = where(NOT excl)
;
;  Load the save-files needed for the requested averaging time,
;  beginning with the oldest.
	for i=ii0,0,-1 do begin
		if (stmax(i) GT 0.0) then begin
			file, fsav(ii(i)), /QUIET
			print, fsav(ii(i)), ntmax, (time(ntmax-1)-tscr)*cpar(0,'uut')/60.
			grav = cpar(0,'grav')
			if KEYWORD_SET(SCRATCH) then ntmax=1L
			for j=ntmax-1,0,-1 do begin
;
;  \tau averages
				if KEYWORD_SET(TAU) then begin
					ppm   = cdata(j,'ppm') + (cdata(j,'ppm') > 0.0)
					hab   = 0.5*ppm(0)/(grav*(cdata(j,'rhom'))(0))
					tt0   = ftt(j)
					lnr0  = flnrho(j)
					if (it EQ 0L) then begin
						Ltau0 = alog10(ftau(lnr0, tt0, hab=hab))
						Ltaur = [max(Ltau0(*,*,0)), min(Ltau0(*,*,nz-1L))<alog(3e2)]
						Ltau0 = findgen(nz)/(nz-1.)*(Ltaur(1)-Ltaur(0))+Ltaur(0)
					endif
					mrho(*, it) = exp(haver(tausc(Ltau0, lnr0, lnr=lnr0, $
											 tt=tt0, hab=hab, /NEW, /MCUBIC)))
					mtt(*, it)  = exp(haver(tausc(Ltau0, alog(tt0), /MCUBIC)))
				endif else begin
					if KEYWORD_SET(IMEAN) then begin
						sints = fint(j)/total(fint(j))
						for iz=0L,nz-1L do begin
							mrho(*, it) = total(sints*exp(fpln(it,0,iz)))
							mtt(*, it)  = total(sints*fpln(it,5,iz))
						endfor
					endif else begin
						mrho(*, it) = cdata(j, 'rhom')
						mtt(*, it)  = cdata(j, 'ttm')
					endelse
				endelse
;
;  Interpolate over excluded points
				if (Nexcl GT 0) then begin
					mrho(iexcl,it)=exp($
							intrp1(z(iincl),z(iexcl),alog(mrho(iincl,it)),/EXT))
					mtt(iexcl, it)=exp($
							intrp1(z(iincl),z(iexcl),alog(mtt(iincl,it)),/EXTR))
				endif
				oplot, alog10(mrho(*, it))-7, alog10(mtt(*, it)), psym=3
				it = it + 1
				print,j,it,nt
				if (it EQ nt) then goto, AverageIt
			endfor
		endif
	endfor
;
AverageIt:
;
    if (it EQ nt) then begin
		oplot, alog10(meanrho0)-7, alog10(meanT0), line=2
		label, 0.2, 0.6, 2, 'Old; '+sdate0
		if (nt GT 1) then begin
			if KEYWORD_SET(TAU) then begin
				rhol    = mrho
				ttl     = mtt
				meanrho = aver(mrho, 2)
				meanT   = aver(mtt, 2)
			endif else begin
				zlagr, mrho, z, mcol, zl
				vlagr, z, zl, mrho, rhol
				vlagr, z, zl, mtt, ttl
				meanrho = aver(rhol, 2)
				meanT   = aver(ttl, 2)
			endelse
		endif else begin
			meanrho = reform(mrho)
			meanT   = reform(mtt)
		endelse
		if (nt GT 2) then begin
			meanrho2= aver(rhol(*,nt/2:*), 2)
			meanT2  = aver(ttl(*,nt/2:*), 2)
		endif else begin
			meanrho2=meanrho
			meanT2  =meanT
		endelse
		if KEYWORD_SET(TAU) then begin
;
;  Construct a z-scale that reproduces the Ltau0 \tau-scale.
			ab = exp(ftau(alog(meanrho), meanT, zm=0))
			zk = integf(1d1^Ltau0, 1d0/ab)
			zk = zk - intrp1(meanT, cpar(0,'teff'), zk)
			z  = zk
		endif
;
;  Interpolate the old meanrhoT to the z-scale, z1, of the present meanrhoT.
;
		z1 = z - intrp1(meanT, cpar(0,'teff'), z)
		meanrho0 = exp(intrp1(z0, z1, alog(meanrho0), /EXTRAPOLATE))
		meanT0 = exp(intrp1(z0, z1, alog(meanT0), /EXTRAPOLATE))
		oplot, alog10(meanrho2)-7, alog10(meanT2), col=c0
		oplot, alog10(meanrho)-7, alog10(meanT);, col=c0
		label, 0.2, 0.7, 0, 'New'
        answ = prompt('Continue [y/s/n/q] ? ')
        if (answ EQ 'n'  OR  answ EQ 'q'  OR  answ EQ 'r') then return
        if (answ EQ 's')                                   then stop
;
;  Should we choose a combo of the present and the previous meanrhoT
;  to avoid oscillating between two cases?
;
		ii = where(meanT LT 2*cpar(0, 'teff'))

		repeat begin
			plot,alog10(meanrho(ii))-7,alog10(meanT(ii)), ysty=17, $
					xr=[rr(0),alog10(max(meanrho(ii)))-7+.3], $
					yr=[Tr(0), alog10(max(meanT(ii)))], $
					xtit='Log!i !n!7q!x', ytit='Log!i !n!8T!x',psym=-1,syms=.2
			oplot, alog10(meanrho2)-7, alog10(meanT2), col=c1
			oplot, alog10(meanrho)-7, alog10(meanT)
			oplot, alog10(meanrho0)-7, alog10(meanT0), line=2
			oplot, !x.crange, [1,1]*cpar(0,'teff'), line=1
			label, 0.2, 0.70, 0, 'New'
			label, 0.2, 0.63, 0, '1st half(new)', col=c1
			label, 0.2, 0.56, 2, 'Old'
			read, ' New*p + Old*(1-p): p = ', p
;			oplot, alog10(meanrho(ii))*p + alog10(meanrho0(ii))*(1.0-p)-7, $
;								alog10(meanT(ii)*p + meanT0(ii)*(1.0-p)), col=c0
			oplot, alog10(meanrho)*p + alog10(meanrho0)*(1.0-p)-7, $
								alog10(meanT*p + meanT0*(1.0-p)), col=c0
			label, 0.2, 0.49, 0, 'Combo', col=c0
            answ = prompt('Good enough [y/s/n/q] ? ')
            if (answ EQ 'q'  OR  answ EQ 'r') then return
            if (answ EQ 's')                  then stop
		endrep until (strlowcase(answ) EQ 'y')
		meanT = meanT*p + meanT0*(1.0-p)
		meanrho = meanrho*p + meanrho0*(1.0-p)
;
;  Extend z-scale to lowest rho in the cube
		if (NOT KEYWORD_SET(OLDZ)) then begin
			minlnr = min(fpln(ntmax-1,0,0)) - lnrmarg
			if KEYWORD_SET(PATCHUP) then begin
				minlnr = alog(2.29230e-05)
				print, read_meanrhot(meanrho, meanT, z, dir='./')
				z1 = z - intrp1(meanT, cpar(0,'teff'), z)
				nz = N_ELEMENTS(z)& mz=nz
			endif
			dlnrdz = derf(z1, alog(meanrho))
			ir0    = 2L
            ir1    = max(where(z1 LT 0.0))
;  Only base extrapolation on points with a high lnrho-gradient,
;  sufficiently far from the photospheric transition region.
            dlnrdzlim = 0.85*max(dlnrdz(ir0:ir1))
;		    ir     = ir0 + where(dlnrdz(ir0:*) GT 0.85*max(dlnrdz(ir0:*)))
;  Make sure that those high gradient points are contiguous.
            ir0    = min(where(dlnrdz(ir0:ir1) GT dlnrdzlim)) + ir0
            ir1    = min(where(dlnrdz(ir0:*)   LE dlnrdzlim)) + ir0
            ir     = ir0 + lindgen((ir1-ir0)>5)
;  Coefficients of quadratic extrapolation of logarithmic density.
			w  = 1d0 + 0d0*z1
			if (Nexcl GT 0) then w(iexcl) = 1d-3
			Ar = reform(svdfit(z1(ir),alog(meanrho(ir)),3L,weight=w(ir),/DOUBL))
;
;  Find the height, zm0, that corresponds to minlnr
			zm0    = (-Ar(1)+sqrt(Ar(1)^2-4d0*Ar(2)*(Ar(0)-minlnr)))/(2d0*Ar(2))
			print, "  Extrapolating <rho>(z1) and <T>(z1) from z1(0)=",z1(0),$
				   " to ", zm0, "Mm."
;
;  Construct new z-scale
			dz     = z1(1:*)-z1
			dz0    = min(dz, idzmin)
;  Quadratic extrapolation of dz(z1) from derivatives at zm0 and z1(2)
			d2dz1  = (dz(3)-dz(1))/(z1(3)-z1(1))
;  Let the derivative at the top, d2dz0, be smaller than at the old top,
;  d2dz1, by the squared ratio of the corresponding heights, zm0 and z1(2).
			d2dz0  = d2dz1/(zm0/z1(2))^2
			Az     = dblarr(3)
			Az(2)  = (d2dz1-d2dz0)/(z1(2)-zm0)/2d0
			Az(1)  =  d2dz1-2d0*Az(2)*z1(2)
			Az(0)  = dz(2) - z1(2)*(Az(1) + z1(2)*Az(2))
            dztop  = poly(zm0, Az)
            if (Az(2) GT 0.0  AND  dztop LT .5*dz(2L)) then begin
                Az(2L) = (dz(2L) - .5*dz(2L))/(zm0^2-2.*zm0*z1(2L)+z1(2L)^2)
                Az(1L) = -2.*Az(2L)*zm0
                Az(0L) = .5*dz(2L) + Az(2L)*zm0^2
            endif
            if (Az(2) LE 0.0  AND  dztop GT 2.*dz(2L)) then begin
                Az(2L) = (dz(2L) - 2.*dz(2L))/(zm0^2-2.*zm0*z1(2L)+z1(2L)^2)
                Az(1L) = -2.*Az(2L)*zm0
                Az(0L) = 2.*dz(2L) + Az(2L)*zm0^2
            endif
			za     = z1(1:*)
			B1     = Az(1) + 1.0
;  Find z1(i) corresponding to those dz(z1)
			repeat begin
				Cz = Az(0) - za(0)
				za = [(-B1+sqrt(B1^2-4*Az(2)*Cz))/2/Az(2), za]
			endrep until (za(0) LT zm0)
;  Expand that scale by a common factor until zm covers [zm0; z1(nz-1)]
;  with nz points.
			nza    = N_ELEMENTS(za)
			izm    = nza-1L
			repeat begin
				izm= izm - 1L
				zm = za(nza-1L)/za(izm)*za(0:izm)
			endrep until (N_ELEMENTS(where(zm GE zm0)) LT nz)
			zm     = float(za(nza-1L)/za(izm)*za(izm-nz+1:izm))
;  Matching point between original and extrapolated z1-scale.
			iz0    = max(where(zm LT z1(ir0)))
			iexcl  = [iz0+1L]& Nexcl=1L
;  New meanrho, above and below the matching point.
			meanrho(iz0+1L:*) = exp(intrp1(z1, zm(iz0+1L:*), alog(meanrho)))
			meanrho(0L:iz0)   = float(exp(poly(zm(0L:iz0), Ar)))
;  Coefficients of linear extrapolation of logarithmic temperature.
			lnmT   = alog(meanT) & AT=dblarr(2)
			AT(1)  = (lnmT(ir0+1L)-lnmT(ir0-1L))/double(z1(ir0+1L)-z1(ir0-1L))
			AT(0)  = (lnmT(ir0+1L)+lnmT(ir0-1L)-AT(1)*(z1(ir0+1L)+z1(ir0-1L)))/2d0
;  New meanT, above and below the matching point.
			meanT(iz0+1L:*)   = intrp1(z1, zm(iz0+1L:*), meanT)
			meanT(0L:iz0)     = float(exp(AT(1)*zm(0L:iz0) + AT(0)))
			print, " Now covering z=",zm(0)," to ",zm(nz-1),"Mm."
		endif else begin
			zm = z1
		endelse
		!P.MULTI=[0,1,2]
		splmin, zm, -derf(zm, meanT), zeff
		reff = exp(intrp1(zm,zeff,alog(meanrho)))
;
; independant variable for the fit to a smooth (polynomial) function
; I can't figure out how this one could make sense to me...
;		xx = 1e1^(-(zm-zeff)/(zm(0)-zeff)*meanrho(0)/reff*35.)
		xx = (zeff-zm)/(zm(0)-zeff); Normalizing to x(0)=-1.
		zrange = [zm(0), zeff-.1*zm(0)]
		Npol = Npolmax
		buf = 1L
		ii = where(zm LT zeff)
		iimax = N_ELEMENTS(ii)+2L+buf
		if (Nexcl GT 0L) then  ii=lindgen(iexcl(0)+buf) else  ii=lindgen(14+buf)
		w = replicate(1d0, iimax)
		if (Nexcl GT 0) then w(iexcl) = 1d-4
		repeat begin
; T-plot
			!P.MULTI=[0,1,2]
			i0 = N_ELEMENTS(ii)-1L-buf
			plot, zm, meanT, psym=-2, syms=.2, xr=zrange, ytickform='(I0)', $
				xtit='!8z!x/!3[!xMm!3]!x', ytit='!8T!x/!3[!xK!3]!x', ysty=17
			oplot, !x.crange, [1,1]*cpar(0,'teff'), line=1
			xyouts, .4*zm(0), cpar(0,'teff'), '!8T!x!ieff!n', /DATA
			oplot, [1,1]*zeff, !y.crange, line=1
			oplot, [1,1]*zm(ii(i0)), !y.crange, line=2
			label, 0.2, 0.7, 0, 'New'
			label, 0.2, 0.6, 2, 'Old'
			label, 0.2, 0.5, 0, 'polyfit', col=c0
			yout = !y.crange(0)-(!y.crange(1)-!y.crange(0))*.08
			xyouts, zeff, yout, '!8z!x!ieff!n', align=.5, /DATA
			if (Nexcl GT 0L) then begin
				dum   = where(iexcl LE ii(i0))
				if (dum(0) GE 0L) then dNpol=N_ELEMENTS(dum)  else  dNpol=0L
			endif else dNpol=0L
			Npol = (i0-dNpol) < Npolmax
			if (Npol GE 1) then begin
				dii = lindgen(buf*2+1)-buf
				w(ii(i0)+dii-[1,0]) = w(ii(i0)+dii-[1,0])*3d+1
				A = svdfit(1d0*xx(ii),1d0*meanT(ii),Npol+1,weight=w(ii),/DOUBLE)
				w(ii(i0)+dii-[1,0]) = w(ii(i0)+dii-[1,0])/3d+1
				pfit = poly(xx,A)
				meanTs = meanT
				meanTs(ii) = smoothfx(zm(ii),meanT(ii)-pfit(ii)) + pfit(ii)
				oplot, zm, pfit, col=c0
;				oplot, zm, meanTs, col=c1
				oplot, z1, meanT0, line=2
; Difference plot
				dTrange = [min(meanT(ii(1:*))-pfit(ii(1:*)),max=pmax),pmax]
				plot, zm, meanT-pfit, yr=dTrange, psym=-2, syms=.2, $
								xr=zrange,xtit='!8z!x/!3[!xMm!3]!x', $
								ytit='!7D!8T!x/!3[!xK!3]!x'
				yout = !y.crange(0)-(!y.crange(1)-!y.crange(0))*.08
				xyouts, zeff, yout, '!8z!x!ieff!n', align=.5, /DATA
				oplot, !x.crange, [0,0], line=1
				oplot, [1,1]*zeff, !y.crange, line=1
				oplot, [1,1]*zm(ii(i0)), !y.crange, line=2
				oplot, zm, meanTs-pfit, col=c1
				label, 0.2, 0.7, 0, 'New-poly'
				label, 0.2, 0.6, 0, 'Smooth-poly', col=c1
				for iz=0L, Nexcl-1L do $
					oplot,[1,1]*zm(iexcl(iz)),!y.crange,line=2,col=c0
				yout = !y.crange(0)+(!y.crange(1)-!y.crange(0))*.1
				striz = string(ii(i0), form='(I0)')
				xyouts, zm(max(ii)), yout,'!8i!iz!n!x='+striz, align=.5,/DATA
				yout = !y.crange(0)+(!y.crange(1)-!y.crange(0))*.86
				xyouts, (zm(0)-zeff)*.94+zeff, yout, $
						'!8N!x!ipol !n=!i !n'+string(Npol,form='(I0)')
			endif else  oplot, z1, meanT0, line=2
			print,string(13b),'Good enough [y/</>/-/s] ? ',form='(2A0,$)'
			ss=get_kbrd(1)& s2=get_kbrd(0)& s3=get_kbrd(0)
;		    if ((byte(ss))(0) NE 10) then print,ss  else print,''
			direction = (byte(s3))(0)
			if (direction EQ 67b) then ii=lindgen((N_ELEMENTS(ii)+1)<iimax)
			if (direction EQ 68b) then ii=lindgen((N_ELEMENTS(ii)-1)>(2+dNpol))
			if (ss EQ '-') then goto, SaveIt
            if (strlowcase(ss) EQ 's') then stop
		endrep until (strlowcase(ss) EQ 'y')
        print, ''
		if (N_ELEMENTS(ii) GT 2) then ii = ii(0:N_ELEMENTS(ii)-2)
;		if (Npol GE 1) then meanT(ii) = pfit(ii)
		if (Npol GE 1) then meanT(ii) = meanTs(ii)
SaveIt:
    	!P.MULTI=0
		dum = min(abs(zm), iTeff)
;	i2Teff = min(where(meanrho GT 1.5*meanrho(iTeff)))
; This version ensures a positive i2Teff - the above might return -1.
        i2Teff = max(where(meanrho LT 1.5*meanrho(iTeff)))
		Lrmax = alog10(meanrho(i2Teff))-7
		Tmax = meanT(i2Teff)
		plot,  alog10(meanrho)-7, meanT, xsty=2, ytickform='(I0)', $
						xtit='Log!i !n!7q!x', ytit='!8T!x', $
						xr=[rr(0), Lrmax], yr=[1e1^Tr(0), Tmax]
		for i=0,Nprev-1 do begin
			oplot, alog10(mrprv(*,i))-7, mTprv(*,i), col=dc*(i-imin+1)
			xyouts, alog10(mrprv(0,i))-7, mTprv(0,i), sdate(i), $
												align=1, col=c0*0+dc*(i-imin+1)
		endfor
		color_scale, 0, Nprev, x0=0.23, y0=0.6, title='Date'
        label, 0.3, 0.70, 0, 'New'
        label, 0.3, 0.63, 2, 'Old'
		oplot, alog10(meanrho0)-7, meanT0, line=2
		reset_graphics
		answ = prompt('Save to file [n/y/s] ? ')
		if (answ EQ 's') then stop
		if (answ NE 'y') then begin
			print, '   Did not save mean structure to file.'
			if KEYWORD_SET(STOP) then stop
			return
		endif
		if KEYWORD_SET(OLDZ) then begin
			write_meanrhot, meanrho, meanT, dir='./'
		endif else begin
			write_meanrhot, meanrho, meanT, dir='./', zm
		endelse
		file, scrfile, /QUIET
		answ = prompt(' Check the iso-tau structure [n/y] ? ')
		if (answ EQ 'y') then begin
            isotau
        endif
		answ = prompt(' Start calculation of new table [n/y] ? ')
		if (answ EQ 'y') then begin
			spawn, 'make &', result
			pid = strtrim((str_sep(result, ' '))(1), 2)
		endif
		add_to_log, string(p,striz,form='("p=",f4.2,", izmax=",A0)'), $
															  cmd=cmd, /CLEAR
	endif else begin
		print, "   Something went wrong."
		print, "   Didn't write to file - Bailing out!"
		stop
	endelse
	if KEYWORD_SET(STOP) then stop
end
