;
;
;   Update history:
;   ---------------
;   31.07.2003 Coded/RT
;   26.05.2004 Calculate rhobot with 5 SIGNIFICANT digits
;              instead of fixed 1 digit/RT
;   29.01.2007 Allow for a maximum in SSarr(eearr)/RT
;   05.03.2014 Changed to also work with EOSrhoe.tab tables/RT
;
function rho_e_sbot, rhoin, SSin, rhodec, iS
    common coff,mpar,mav,ipack,incr,ioff,time,voff,cfile,irec    
    common cdat,x,y,z,nx,ny,nz,nw,ntmax,date0,time0
    common ctable,ttmean,rhm,drhm,eemin,eemax,dee,mtab,itab,tab
    common ceostab,Netab,Nrtab,Ma,Na,Ntbvar,Nelem,Nxopt,Nxrev,$
                    edate,etime,lnrmin,lnrmax,lnemin,lnemax
    common currenttable, EOSrhoeLoaded, kaprhoTLoaded

    SS0 = lookup(alog(10)*.5, 2.5, iv=iS)
    uS = (cpar(0, 'uul')/cpar(0, 'uut'))^2/1e8
	rhodec = 5L - long(alog10(cpar(0,'rhobot'))+.9999999999)
    rhobot = long(rhoin*1e1^rhodec+.5)*1e1^(-rhodec)
    if (EOSrhoeLoaded) then begin
        Narr  = Netab
        eearr = exp(findgen(Narr)/(Narr-1.)*(lnemax-lnemin) + lnemin)
    endif else begin
        Narr  = 100L
        ir = long((alog(rhobot)-alog(rhm(0)))/drhm+.5)
        eearr = findgen(Narr)/(Narr-1.)*(eemax(ir)-eemin(ir)) + eemin(ir)
    endelse
    SSarr = (lookup(replicate(alog(rhobot),Narr),eearr,iv=iS)-SS0)*uS
	SSmax = max(SSarr, iSmax)
	if (iSmax NE (Narr-1L)  AND  iSmax NE 0L) then begin
		if(SSarr(0) LT SSarr(Narr-1L)) then begin
			iSmax = min(where(SSarr GE SSmax))
			SSarr = SSarr(0:iSmax)
			eearr = eearr(0:iSmax)
		endif else begin
			iSmax = max(where(SSarr GE SSmax))
			SSarr = SSarr(iSmax:*)
			eearr = eearr(iSmax:*)
		endelse
	endif
    eebot = intrp1(SSarr, SSin, eearr)
    return, [eebot, rhobot]
end
