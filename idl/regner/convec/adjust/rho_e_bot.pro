;
;   Update history:
;   ---------------
;   21.04.2003 Coded/RT
;   26.05.2004 Print rhobot with 5 SIGNIFICANT digits
;              instead of fixed 1 digit/RT
;   27.08.2004 Automatic printing to log-file, if s/r add_to_log is
;              called by the calling routine./RT
;   24.06.2014 Generalized format statement to avoid format overflow./RT
;
pro rho_e_bot, it, tabfile=tabfile, deebot=deebot, drhobot=drhobot, $
                                                        KEEP_S=KEEP_S, STOP=STOP
	common coff,mpar,mav,ipack,incr,ioff,time,voff,cfile,irec
	common cdat,x,y,z,nx,ny,nz,nw,ntmax,date0,time0
;   common ctable,ttmean,rhm,drhm,eemin,eemax,dee,mtab,itab,tab
    common currenttable, EOSrhoeLoaded, kaprhoTLoaded
	common cadd_to_log, log_text

	default, it, 0L
	default, deebot, 0.0
	default, drhobot, 0.0
    if (N_ELEMENTS(tabfile) GT 0) then begin
        if (strpos(tabfile, '.tab') LT 0) then tabfile=tabfile+'.tab'
        table, tabfile, mz=82L; nz
    endif else begin
        if (NOT EOSrhoeLoaded) then begin
            tabfile = stamnavn()+'.tab'
            fst = fstat(2)
            if (strpos(fst.name,'.tab') LT 0) then table, tabfile
        endif
    endelse
	sstab, iS
	SS0 = lookup(alog(10)*.5, 2.5, iv=iS)
	uS = (cpar(0, 'uul')/cpar(0, 'uut'))^2/1e8
; A binary mask for upflows
	ups = float(fpln(it,3,nz-1) LE 0.0)
    Nups = total(ups)
	
	lnrbot = fpln(it,0,nz-1)
	rhoin = total(exp(lnrbot)*ups)/Nups
	eebot = fpln(it,4,nz-1)
;    eein0 = total(eebot*ups)/Nups
	SSbot = (lookup(lnrbot, eebot, iv=iS)-SS0)*uS
    if KEYWORD_SET(KEEP_S) then begin
        SSin = (lookup(alog(cpar(it,'rhobot')), cpar(it,'eebot'), iv=iS)-SS0)*uS
    endif else begin
        SSin = total(SSbot*ups)/Nups
    endelse
	botvar = rho_e_sbot(rhoin, SSin, rhodec, iS)& srhodec=string(rhodec,form='(I0)')
	eebot  = botvar(0) + deebot
	rhobot = botvar(1) + drhobot
    ppbot = exp(lookup(alog(rhobot), eebot, iv=0))
    print, 'Old values for ".eetop,eebot,rhotop,rhobot,pptop,ppbot":'
    print, cpar(it, 'eebot'), cpar(it, 'rhobot'), cpar(it, 'ppbot'), $
				format='("0.0,",F0.5,",0.0,",F0.'+srhodec+',",0.0,",F6.1,",")'
    print, 'New values for ".eetop,eebot,rhotop,rhobot,pptop,ppbot":'
    print, eebot,rhobot,ppbot, $
                format='("0.0,",F0.5,",0.0,",F0.'+srhodec+',",0.0,",F6.1,",")'
	stra = prompt('Save the new values to '+cfile+' [y/n] ')
    if (stra EQ 's') then stop
	if (stra EQ "y") then begin
		fs1 = fstat(1)
		if (fs1.write EQ 0) then file, fs1.name, /WRITE, /QUIET
		for i=0,ntmax-1 do begin
			wpar, eebot, i, 'eebot'
			wpar, rhobot, i, 'rhobot'
			wpar, ppbot, i, 'ppbot'
		endfor
		if (fs1.write EQ 0) then file, fs1.name, /QUIET
		default, log_text, ''
		log_text = log_text + ' Saved new rho/eebot to '+cfile+'. '
	endif

    if KEYWORD_SET(STOP) then stop
end
