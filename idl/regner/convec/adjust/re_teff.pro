;+
;	  Program that changes ee, rho, pp and uz to change Teff.
;   Horizontal averages are indicated with <...>. "Subscripts" '0' indicates
;   values from the original simulation, and "Subscripts" '1' indicates the
;   changed (or projected) values.
;
; - The model is changed on a normalized Pgas-scale; Psc = Pgas/Pgas(Pturb-peak)
; - The change in Ptot(Pturb-peak), \Delta[ln(Ptot)], is found from
;   interpolation in the grid of simulations.
; - \Delta[ln(Ptot)] is added to ln(Ptot) of the whole simulation cube
; - The change in the Pturb/Ptot-ratio is found from interpolation in the grid
;   of simulations.
; - From this, the new average gas-pressure, <Pg1>, can be found.
; - The changes of the entropy minimum, Smin, and the asymptotic entropy of the
;   deep convection zone, Smax, are found from interpolation in the grid of
;   simulations.
; - The average entropy, <S>, is changed by an offset to get the correct Smin
;   in the photosphere, and scaled linearly from that minimum to the bottom,
;   to get the correct Smax.
; - The changes to <lnrho> and <ee> that result in <S1> and <Pg1> are found
;   as an adiabatic change to <Pg1>, followed by an iso-baric change to <S1>.
;     This is accomplished by computing iso-bars at Pg1 for each depth-point,
;   calculate entropies for these iso-bars, and interpolate in rho and ee of
;   the iso-bars to S1. This method is faster (and cleaner) than a linearized
;   two-step, itterative method. A sub-photospheric spike is introduced by this
;   method, and we interpolate over it, for smooth changes to rho and ee. It is
;   caused by the assumptions on S and interpolating over the spike relaxed our
;   assumptions on the transition from the entropy minimum to the deep
;   asymptotic value.
; - With the new pressures and densities, <uz> is scaled to obtain the
;   projected Pturb/Ptot-ratio.
; - internal-energy fluctuation are scaled to obtain the projected Fconv. 
; - <Ptot> and <rho> are interpolated to a finer grid (fi*nz), and the z-scale
;   is then found by integration of the Eq. of hydrostatic equilibrium, to
;   obtain the same departures from hyd. eq. as was found in the original sim.
; - Finally the parameters eebot and rhobot are changed so that rhobot with
;   5 significant digits and eebot with 5 digits, give the entropy of the
;   inflowing plasma: S(eebot, rhobot) = <S>in
;
;	  All the time the program plots hopefully illustrative quantities so you
;	can monitor the changes and stop it, if it runs screaming off in some wierd
;	direction.
;	  At the end the rest of the scratch-file is changed to make it consistent.
;   The eebot-value is changed automatically now, by rho_e_bot.
;   Use zshift_w afterwards to get a reasonable and smooth z-scale again.
;
;   NB! If  dTeff > Teff0/2  then the parameter dTeff is actually the
;       target Teff, Teff1.
;
;   Update history:
;   ---------------
;   29.05.1999 Coded/RT
;	27.08.2004 Automatic printing to log-file/RT
;   01.03.2007 Now interpolate Smin and Smax in grid of simulations/RT
;	10.10.2007 Added argument, cmd, to call of re_g/RT
;	10.10.2007 If not already called through re_g, call starvalplot, 'Smax'
;              and oplot the Teff-change. Correctly implemented Simultaneous
;              changing of Teff and grav
;   15.10.2007 Abandoned the linearized, itterative adiabatic and iso-baric
;              changes to the mean structure, to the more direct interpolation
;              of entropy in the projected iso-bars/RT
;   16.10.2007 improved and standardized the prompts and their handling/RT
;   16.10.2007 Properly detects and handles multiple or missing iso-bars/RT
;   29.10.2015 Added manual setting of target S1min, S1max, Pt1max and Ppht1/RT
;
;   fgrav:  g1/g0, the ratio of surface gravities.
;   itr:    The simulations to use in the linear inter-/extra-polation can
;           be overidden from the nearest triangle to the 3-element itr,
;           in the call to starval_intrp
;
;   Known bugs: Doesn't automatically determine whether an extrapolation out
;				of the simulation grid has occured.
;               Does actually not perform extrapolation of Smax and Smin!
;               Need to do that based on closest triangle.
;-
pro sexp, x, a, f, pder
	fexp = exp(-x/a(2))
	f = a(0) - a(1)*fexp
	pder = [[1d0+0*x], [0d0-fexp], [a(1)/a(2)*fexp]]
end

pro re_teff, Teff0, dTeff, fgrav=fgrav, scrfile=scrfile, tabfile=tabfile, $
					factor=factor, tvcolortable=tvcolortable, $
                    S1min=S1min, S1max=S1max, Pt1max=Pt1max, Ppht1=Ppht1, $
					NOFLUXSCALING=NOFLUXSCALING, PRINTTAB=PRINTTAB, STOP=STOP
@cstarval
@consts
	common ctable,ttmean,rhm,drhm,eemin,eemax,dee,mtab,itab,tab
	common coff,mpar,mav,ipack,incr,ioff,time,voff,cfile,irec
	common cdat,x,y,z,nx,ny,nz,nw,ntmax,date0,time0
    common currenttable, EOSrhoeLoaded, kaprhoTLoaded
    common clabel, lx, ly0, dly, il

	eps = 1e-4
	col0 = (!D.TABLE_SIZE-1.)*.80
; The scratch-file is the default file.
    default, scrfile, stamnavn()
    if (strpos(scrfile, '.scr') LT 0) then scrfile=scrfile+'.scr'
    default, tabfile, stamnavn();	'large'
    if (strpos(tabfile, '.tab') LT 0) then tabfile=tabfile+'.tab'
	default, dfile, ''
	default, fgrav,  1.0
	default, factor, 1.0
	default, tvcolortable, 23L
	clear_log
; Find the current color-table index and load the TVCOLORTABLE
    rt_graphics
	icurr = currentct(/QUIET, ntables=ntables)
	tvcolortable = (tvcolortable>0)<(ntables-1L)

	file, scrfile
	ipack = 1
	Lgg0  = alog10(cpar(0,'grav')*cpar(0,'uul')/cpar(0,'uut')^2)
    print, ' Original logg: ', Lgg0, cpar(0,'grav')
;
; If change of grav, do that first.
;
	if (fgrav NE 1.0) then begin
		loadct, tvcolortable, /SILENT
        Teff1 = 0.
		re_g, fgrav, scrfile, Teff0=Teff0, /NOTEFF, $;  Teff1=Teff1, $
			cmd=string(fgrav, scrfile, Teff0, format='("re_g, ",f0.4,", ' + $
			string(39b)+'",A0,"'+string(39b)+', Teff0=",f0.2,", Teff1=Teff1")')
;       Teff0 = Teff1
	endif
;
; Read the scratch-file
	file, scrfile, /WRITEACCESS
	ipack = 1
	ug    = cpar(0,'uul')/cpar(0,'uut')^2

; Using the last timestep (remember; scratch-files can have two timesteps),
; but writes to the first.
	nt = ntmax-1
; Surface gravity
	gg = cpar(nt, 'grav')& Lgg1 = alog10(gg*ug)
	bzone = cpar(nt, 'bzone')
	Nxy = float(nx*ny)
	default, dTeff, cpar(nt, 'teff')-Teff0
	if (dTeff GT 0.5*Teff0) then begin
		Teff1 = dTeff
		dTeff = Teff1 - Teff0
	endif else Teff1 = Teff0 + dTeff
;
; Logarithmic ratios between present and desired parameters
	LTr = alog10(Teff1/Teff0)
	Lgr = alog10(fgrav)
	T4r = (Teff1/Teff0)^4
; Averages of present simulation
	mPg0   = cdata(nt, 'ppm')
	mPtot0 = mPg0 + cdata(nt,'ptm')
	mT0    = cdata(nt, 'ttm')
	hydeq0 = zder1(alog(mPtot0))*mPtot0/cdata(nt,'rhom')/gg
	lrm0   = alog(cdata(nt, 'rhom'))
; Ratio between Pturb and Ptot.
	Ptp0   = cdata(nt, 'ptm')/mPtot0
; Original convective flux
	Fconv0 = -cdata(nt,'fconv') - cdata(nt,'fkin')
	Fconv0(0) = Fconv0(1);	Repair bad 1st point...
	Fconv10= Fconv0*T4r
; Original simulation cubes
	lnr    = flnrho(nt)
	ee     = fee(nt)
	uz     = fuz(nt)
; Load the table and generate entropy-table
	table, tabfile, mz=82L
    sstab, iS
    uS = 1.e+4
    SS0 = lookup(alog(10)*.5, 2.5, iv=iS); -0.43895679
	mS0 = haver(lookup(lnr, ee, iv=iS)-SS0)*uS
	S0max = mS0(nz-1)
; zmin = the height at minimum entropy, S0=S0min.
	splmin, z, mS0, zmin, S0min
	ii = where(z GT zmin)
;
; Retrieve Smin, Smax and Ptmax from simulations with l2means.dat files
; and tv the asymptotic entropy in the deep convection zone, Smax.
;	if (fgrav EQ 1.0) then begin
;   	loadct, tvcolortable, /SILENT
		starvalplot, 'Smax', /TV, /LOG, color=50, /NOREVERT; tvcolortable
;		starvalplot, 'Smax', /QUINT, /TV, color=tvcolortable
;   endif
;	dirs = read_startab(mixis='AG89Fe', resis='150x150x82', abundis=0.0, /LMEAN)
; Overplot the present and the new location of the simulation
;   plots, ([Teff0,Teff1]), [Lgg0, Lgg1]
    plot_arrow, [Teff0, Lgg0], [Teff1, Lgg1], point_size=0.015
;   plots, (Teff1), Lgg1, psym=2, th=2, col=(!D.TABLE_SIZE-1.)*.86
;
; Triangulation in the atmospheric parameters of the simulations of the grid.
	LTeff = alog10(Tefobs)
	Logg  = alog10(grav)
	simtriangulate, TR, B
;
; Check whether we are performing any extrapolations in the grid of simulations
	if (extrapolated(LTeff(B), Logg(B), alog10(Teff0), Lgg1,dx,dy)) then $
		print, "WARNING: Original extrapolated outside simulation grid, " + $
			string(dx, dy, form='("dlogTeff = ",f0.3,", dlogg = ",f0.3)')
	if (extrapolated(LTeff(B), Logg(B), alog10(Teff1), Lgg1,dx,dy)) then $
		print, "WARNING: Target extrapolated outside simulation grid, " + $
			string(dx, dy, form='("dlogTeff = ",f0.3,", dlogg = ",f0.3)')
; The entropy minima and maxima
;   dum   = starval_intrp(alog10(Teff0), Lgg0, ['Smin','Smax'], /PLOT, $
;           Ax, Ay, itr=[2, 35, 36])
;   Sfit0min = dum(0) & Sfit0max = dum(1)
    Aout0 = starval_intrp(alog10(Teff0), Lgg0,['Smin','Smax','Ptmax','Ptotph'],$
            /PLOT, Ax, Ay, /LOG)
    Aout1 = starval_intrp(alog10(Teff1), Lgg1,['Smin','Smax','Ptmax','Ptotph'],$
            /PLOT, Ax, Ay, /LOG)
    S0min = Aout0(0L)& S0max = Aout0(1L)
    default, S1min,  Aout1(0L)
    default, S1max,  Aout1(1L)
    default, Pt1max, Aout1(2L)
    default, Ppht1,  Aout1(3L)
    if (S1min GT S1max) then begin
        print, "WARNING: Extrapolation to target swaps entropy min/max!"
        print, "         Arbitrarily using half the entropy jump of the source."
        S1min = S1max-(S0max-S0min)*.5
    endif
    print, ' min/max[S(0)]: ', S0min, S0max
    print, ' min/max[S(1)]: ', S1min, S1max
; New S-stratification by adding the difference in Smin to the whole structure
	mS1 = mS0 - S0min + S1min
; ...and then stretch/compress from the minimum to the bottom to comply with
; the new Smax (S1max).
	mS1(ii) = (mS0(ii) - S0min)/(S0max-S0min)*(S1max(ii)-S1min(ii)) + S1min(ii)
	print, "Changing Teff by ",dTeff," from ",Teff0," to ",Teff1, LTr
; To plot or not to plot...
	answ = prompt(" Plot the entropy structure? [y/n/q/s] : ")
	if (answ EQ 'n'  OR  answ EQ 'r'  OR answ EQ 'q') then begin
		reset_graphics
		file, scrfile, /QUIET;      Safeguard the scratch-file from writing
		return
	endif
	if (answ EQ 's') then stop

	reset_graphics
	ztitle = '!17z/!8[!xMm!8]!x'
;
; Plotting the entropy structure
	plot, z, mS0, yr=[min([mS0, mS1], max=Smax), Smax], xtitl=ztitle, $
                                            ytitl='!18<!8S!18>!5/[!xsim!5]!x'
    oplot, !x.crange, [1,1]*S0min, li=1
    oplot, !x.crange, [1,1]*S0max, li=1
	oplot, z, mS1, line=2, col=col0
    oplot, !x.crange, [1,1]*S1min, li=1, col=col0
    oplot, !x.crange, [1,1]*S1max, li=1, col=col0
	dyy = (!y.crange(1)-!y.crange(0))
	yy = findgen(200)/199.*dyy + !y.crange(0)
	wyy = dyy*.06
; Build a repulsive potential of the two curves and the borders of the plot
; to determine at which y-value, ly0, to put the labels.
	dum = min(alog(exp(-((yy-mS0(nz*.9))/wyy)^2)+exp(-((yy-mS1(nz*.9))/wyy)^2)+$
			  exp(-((yy-yy(199))/wyy*5.)^2) + exp(-((yy-yy(0))/wyy*5.)^2)),imin)
    yl  = (yy(imin)-yy(0))/dyy
    lx=0.80& ly0=yl& dly=0.05& il=0L
	label, 2, 'Target', col=col0
	label, 0, 'Original'
;
; The ratio of max(Pturb/Ptot) from interpolation in the grid of simulations
	Ptpr = Pt1max/Aout0(2L)
; Pturb/Ptot ratio for new Teff
	Ptp1 = Ptp0*Ptpr
; Interpolate in the grid of simulations to find the total pressure at
; the peak of Pturb/Ptot
	Pgr    = (1.-Pt1max)/(1.-Aout0(2L))*Ppht1/Aout0(3L)
	mlnPg1 = alog(mPg0*Pgr)
	mPtot1 = mPg0*Pgr/(1.-Ptp1)
;	mlnPg1 = alog(mPtot1*(1.-Ptp1))
	mPturb1 = mPtot1*Ptp1

	eem   = haver(ee)& eem0 = eem
	RMSee = z*0
	for i=0L,nz-1L do RMSee(i)=rms(ee(*,*,i)-eem(i))/eem(i)
	brcnv = sqrt(T4r) + (T4r*1.7-sqrt(T4r))*(1-RMSee/max(RMSee))
	for i=0,30 do brcnv = smoothfx(z, brcnv)
	fep   = z*0+1 & fep0=fep
	fvz   = z*0+1
	uzrho = 0*ee
	rho   = 0*ee
	u2    = fux(nt)^2 + fuy(nt)^2
;
; Extract lnPg plane from the table
    extract_tablepl, lnrtb, eetb, lnPgtb, 0
    rtitl = 'ln!7q!x'
    if (EOSrhoeLoaded) then begin
        etitl = 'ln!7e!x'
        rout  = lnrtb&  eout = eetb & Pgsurf=lnPgtb
    endif else begin
        lrhm  = alog(rhm)
;
; Perform triangulation of table
        triangulate, eetb, lnrtb, TRtb
; Interpolate the table to a regular grid
        Nitb = 500L
        Pgsurf = trigrid(eetb,lnrtb,lnPgtb,TRtb,[0,0],Nx=Nitb,Ny=Nitb, $
						xgrid=eout,ygrid=rout,missing=-300., /QUINTIC)
        etitl = '!7e!x'
    endelse
; To plot or not to plot...
	answ = prompt(" Plot the entropy table? [y/n/q/s] : ")
	if (answ EQ 'n'  OR  answ EQ 'r'  OR answ EQ 'q') then begin
		reset_graphics
		file, scrfile, /QUIET;      Safeguard the scratch-file from writing
		return
	endif
	if (answ EQ 's') then stop
;
; Plot the lnPg table
;	erase
;	img_plot, Pgsurf>min(lnPgtb), /SCALE, width=.8
;	plot, eout, rout, /NODATA, ysty=1, /NOERASE, xtitl=rtitle, ytitl=etitl
;
; Generate pressure-contours = iso-bars for the projected gas-pressure at
; each depth - but don't plot.
	xysave= [!x.crange, !y.crange, !x.window, !y.window, !x.s, !y.s]
	contour,Pgsurf,eout,rout,levels=mlnPg1,PATH_XY=xy,$
			PATH_INFO=xy_info,CLOSED=0,/PATH_DATA_COORDS, $
			min_value=-290
	!x.crange = xysave(0:1)& !x.window=xysave(4:5)& !x.s=xysave(8:9)
	!y.crange = xysave(2:3)& !y.window=xysave(6:7)& !y.s=xysave(10:11)
;
;	Plot the S-table
	extract_tablepl, lnrtb, eetb, SStb, iS
    if (EOSrhoeLoaded) then begin
        etitl = 'ln!7e!x'
        rout  = lnrtb&  eout = eetb & SSsurf=SStb
    endif else begin
        SStb = (SStb-SS0)*uS
        SSsurf  = trigrid(eetb,lnrtb,SStb,TRtb,[0,0],Nx=Nitb,Ny=Nitb, $
                        xgrid=eout,ygrid=rout,missing=-300., /QUINTIC)
    endelse
    minSStb = min(SStb)
	maxSStb = max(SStb)
;	iie = where(eout GE 1. AND eout LE 5.)
;	iir = where(rout GE -1. AND rout LE 2.)
;	erase& img_plot, SSsurf(iie(0):iie(157),iir(0):iir(81))>minSStb, /SCALE, width=.8
;	plot, eout(iie), rout(iir), /NODATA, ysty=1, /NOERASE
	erase
    img_plot, (SSsurf>minSStb)<maxSStb, 0.16, /SCALE, width=0.8, height=0.8
	plot, eout, rout, /NODATA, ysty=1, /NOERASE, xtitl='ee!5/[!xsim!5]!x', $
                         ytitl='                     ln!7q!5/[!xsim!5]!x'
    color_scale,minSStb, maxSStb, x0=.05, y0=!y.window(0), height=0.40, $
                                                    titl='!8S!5/[!xsim!5]!x'
    if (NOT EOSrhoeLoaded) then begin
        plots, eetb, lnrtb, psym=3
        plots, eemin, alog(rhm)
        plots, eemax, alog(rhm)
    endif
;
; ...and oplot, the isobars of the target pressures, mlnPg1
;	Sxy = (lookup(reform(xy(1,*)), reform(xy(0,*)), iv=iS)-SS0)*uS
	delee = fltarr(nz)& dellr=delee
;
; Begin loop over depth
	izbad = [-1]
	for iz=0L, nz-1L do begin
; Find the iso-bars, i, that match the current depth-point, iz.
		i = where(xy_info.level EQ iz)
		if (i(0) GE 0L) then begin
			Ni = N_ELEMENTS(i)
			if (Ni EQ 1L) then begin
; One match :-)
				i=i(0)
			endif else begin
;
; More than one iso-bar matches the depth-point - chose the one with the most
; points and hope it's the correct one... 
				print, "WARNING: Found "+string(Ni,form='(I0)') + $
					" iso-bars to match depth-point "+string(iz,form='(I0)') + $
					". Using the one with the most points:"
				dum = max(xy_info(i).N, jmax)
				for j=0L, Ni-1L do begin
					if (j EQ jmax) then use=" * "   else  use="   "
					print, use+string(i(j), xy_info(i(j)).N, form=$
						'("i = ",I0,",  N = ",I0)')
				endfor
				i = i(jmax)
			endelse
			ip = lindgen(xy_info(i).N)
; Close the iso-bar if it is a closed iso-bar
			if (xy_info(i).TYPE EQ 1) then  ip = [ip, 0]
			Np = N_ELEMENTS(ip)
; x=ee and y=lnr coordinates of this iso-bar
			xp = reform(xy(0,xy_info(i).OFFSET + ip))
			yp = reform(xy(1,xy_info(i).OFFSET + ip))
; Entropy of this iso-bar
; Only include those points of the iso-bar that are inside the table.
; Interpolate the table limits to the densities of the iso-bars
			i0 = 0L & i1 = Np-1L
            if (NOT EOSrhoeLoaded) then begin
                ir     = long((yp-lrhm(0))/drhm)
                eeminp = eemin(ir)
                eemaxp = eemax(ir)
; Find the first point which is inside the table
                while(xp(i0) LE eeminp(i0)  OR  xp(i0) GE eemaxp(i0)) do begin
                    i0 = i0 + 1L
                    if (i0 GE Np) then break
                endwhile
; Find the last point which is inside the table
                while(xp(i1) LE eeminp(i1)  OR  xp(i1) GE eemaxp(i1)) do begin
                    i1 = i1 - 1L
                    if (i1 LE 0L) then break
                endwhile
            endif
; The entropy of the iso-bars (within the table limits)
            if (EOSrhoeLoaded) then begin
                Sp = (lookup(yp(i0:i1), exp(xp(i0:i1)), iv=iS)-SS0)*uS
            endif else begin
                Sp = (lookup(yp(i0:i1), xp(i0:i1), iv=iS)-SS0)*uS
            endelse
; Interpolate the isobars to the correct entropy
			if (i0 LT i1) then begin
                if (EOSrhoeLoaded) then begin
				    delee(iz) = intrp1(Sp, mS1(iz), xp(i0:i1)) - alog(eem(iz))
                endif else begin
                    delee(iz) = intrp1(Sp, mS1(iz), xp(i0:i1)) - eem(iz)
                endelse
				dellr(iz) = intrp1(Sp, mS1(iz), yp(i0:i1)) - lrm0(iz)
				plots, xp(i0:i1), yp(i0:i1), col=.85*(!D.TABLE_SIZE-1.)
			endif else begin
				print, "WARNING: Iso-bar " + string(i, form='(I0)') + $
						" of depth-point " + string(iz,form='(I0)') + $
						" is not inside the table."
				print, "         
				if (Ni GT 1L) then begin
					if (Ni GT 2L) then begin
						print, "         Should probably haven chosen one of "+$
						" the other " + string(Ni,form='(I0)') + " iso-bars..."
					endif else begin
						print, "         Should probably haven chosen the " + $
						"other iso-bar..."
					endelse
				endif
				print,"         Will try to interpolate the changes in ee and lnr"
				print,"         over this point."
				izbad = [izbad, iz]
			endelse
		endif else begin
			print,"WARNING: Couldn't find any iso-bars that match depth-point "$
					+string(iz, form='(I0)')+"."
			print,"         Will try to interpolate the changes in ee and lnr"
			print,"         over this point."
			izbad = [izbad, iz]
		endelse
;
; End loop over depth
	endfor
    if (EOSrhoeLoaded) then  oplot, alog10(eem0), lrm0, col=0, psym=-2, syms=.3$
	                   else  oplot, eem0, lrm0, col=0, psym=-2, syms=.3
    lx=0.13& ly0=0.94& dly=0.05& il=0L
    label, 0, 'original', col=0, psym=-2, syms=.3
; Test case:
;  re_teff, 4184.6, 4184.6+16.6, tab='scr1/t42g47m-30_18.09.2007-17:12.tab'
;
; Interpolate delee and dellr over any possible bad points
	Nbad = N_ELEMENTS(izbad)-1L
	if (Nbad GT 0L) then begin
		izbad = izbad(1:*)& izgood = [-1]
		for i=0L, nz-1L do if (where(izbad EQ i) LT 0L) then izgood=[izgood, i]
		izgood = izgood(1:*)
		delee(izbad) = intrp1(z(izgood), z(izbad), delee(izgood), /EXTRA)
		dellr(izbad) = intrp1(z(izgood), z(izbad), dellr(izgood), /EXTRA)
		print, "WARNING: Interpolated over "+string(Nbad,form='(I0)')+" points."
	endif
; Smooth interpolation over the entropy jump
	ii = where(z LT zmin OR (mS1-S1max)/(S1max-S1min) GT -.2, COMPLEMENT=i2)
	dellr(i2) = intrp1(z(ii), z(i2), dellr(ii))
	delee(i2) = intrp1(z(ii), z(i2), delee(ii))
; Apply the changes
	for i=0L, nz-1L do begin
		lnr(*,*,i) = lnr(*,*,i) + dellr(i)
		ee(*,*,i)  = ee(*,*,i)  + delee(i)
	endfor
; Update averages, exponentials, pressures, etc.
	rho = exp(lnr)
	lrm = alog(haver(rho))
	eem = haver(ee)
	if (EOSrhoeLoaded) then  oplot, alog(eem), lrm, psym=-2, syms=.3 $
	                   else  oplot, eem, lrm, psym=-2, syms=.3
    label, 0, 'target', psym=-2, syms=.3
    label, 0, 'iso-bars', col=.85*(!D.TABLE_SIZE-1.)
	lnPg= lookup(lnr, ee, dlpdee, iv=0)
	Pg  = exp(lnPg)
	Pgm = haver(Pg)
	fvz = sqrt(mPturb1/haver(rho*uz^2))
;
; Beginning of depth loop
	for i=0, nz-1 do begin
		uz(*,*,i) = fvz(i)*uz(*,*,i)
		u2(*,*,i) = fvz(i)^2*u2(*,*,i)
		uzrho(*,*,i)=uz(*,*,i)*rho(*,*,i) - aver(uz(*,*,i)*rho(*,*,i))
; The part of uz*rho that transports net-flux
		if KEYWORD_SET(NOFLUXSCALING) then begin
			fep(i)=1.0
		endif else begin
			de        = ee(*,*,i) - eem(i)
			dP        = Pg(*,*,i) - Pgm(i)
			Pg_rho    = Pg(*,*,i)/rho(*,*,i)
			Fconvi=-(ee(*,*,i)+Pg_rho+.5*(u2(*,*,i)+uz(*,*,i)^2))*uzrho(*,*,i)
			fep(i)= 1-aver(Fconv10(i)-Fconvi)/$
					aver(uzrho(*,*,i)*(dlpdee(*,*,i)*Pg_rho+1)*de)
;			print,"    fep: ",fep(i),min(de)/(1.9-eem(i)),min(de),1.9-eem(i)
			fep(i) = fep(i)>0.1
			fep0(i) = fep(i)
			if (i EQ 0) then begin
				fep(i)=1.0
			endif else begin
				if (i EQ 1) then fep(i) = (fep(i)+fep(i-1))/2.0
				if (i EQ 2) then fep(i) = (fep(i)+fep(i-1)+fep(i-2))/3.0
				if (i EQ 3) then fep(i) = total(fep(i-3:i))/4.0
				if (i GE 4) then fep(i) = total(fep(i-4:i))/5.0
			endelse
;
; Apply the energy fluctuation scaling
; Only do this once, i.e., at the first itteration of plane i <=> n=0.
			ee(*,*,i)=(ee(*,*,i)-eem(i))*fep(i) + eem(i)
		endelse
;
; End of depth loop
	endfor
;	iz0 = max(where(Fconv0 LT 0.0))
;	fep0 = fep
;	fep(0:iz0) = aver(fep(iz0:*))
;
	if (NOT KEYWORD_SET(NOFLUXSCALING)) then begin
; To plot or not to plot...
		answ = prompt(" Plot the flux scaling? [y/n/q/s] : ")
		if (answ EQ 'n'  OR  answ EQ 'r'  OR answ EQ 'q') then begin
			reset_graphics
			file, scrfile, /QUIET;      Safeguard the scratch-file from writing
			return
		endif
		if (answ EQ 's') then stop

		reset_graphics& loadct, icurr, /SILENT
        lx=0.40& ly0=0.90& dly=0.05& il=0L
		plot,  z, fep, ysty=1, yrange=[0,max(fep)*1.02], xtitl=ztitle, /NODATA,$
			ytitl='Energy fluctuation scaling', $
			title='ee-fluctuation scaling to obtain projected !8F!x!dconv!n'
        label, 0, 'downward smoothed scaling factor'
		plots, z, fep0, line=2, psym=-2, syms=.3, col=(!D.TABLE_SIZE-1.)*.8
        label, 2, 'raw scaling factor',psym=-2,syms=.3,col=(!D.TABLE_SIZE-1.)*.8
		oplot, z, fep
;		for i=0, 10 do fep=smoothfx(z, fep)
;		oplot, z, fep
		oplot, !x.crange, [1,1], line=1
		answ = prompt(" Good enough? [y/n/q/s] : ")
		if (answ EQ 'n'  OR  answ EQ 'r'  OR answ EQ 'q') then begin
			reset_graphics
			file, scrfile, /QUIET;      Safeguard the scratch-file from writing
			return
		endif
		if (answ EQ 's') then stop
	endif else begin
		reset_graphics& loadct, icurr, /SILENT
	endelse
	TT = lookup(lnr, ee, iv=2)
	mT = haver(TT)
; Calculate new convective fluxes
	print, "Pressure ratios: ",Ptpr,Pgr,LTr,Lgr
	Fconv1= -haver((ee+Pg/rho)*uzrho + .5*(u2+uz^2)*uzrho)
;	Fent1 = -haver((ee+Pg/rho)*uzrho)
;	Fkin1 = -haver(.5*(u2+uz^2)*uzrho)
    lx=0.50& ly0=0.20 & dly=0.05& il=0L
	plot,  z, Fconv0, yr=[min([Fconv10,Fconv1], max=Fmax), Fmax], xtitl=ztitle,$
			ytitl='!8F!x!dconv!n!5/[!xsim!5]!x'
	label, 0, 'Fconv0'
	oplot, !x.crange, [1,1]*sig/ue*Teff0^4, line=2
	oplot, z, Fconv1, color=col0
	label, 0, 'Fconv1', col=col0
	oplot, !x.crange, [1,1]*sig/ue*Teff1^4, line=2, color=col0
	oplot, z, Fconv10, line=2
	label, 2, 'Fconv0!9X!x(Teff1/Teff0)!e4!n'

; New consistent <Pturb>.
	ptm1 = haver(rho*uz^2)
	mPtot1 = ptm1+Pgm
	Ptp1 = ptm1/mPtot1
	rhom1 = haver(rho)
; Interpolate to a fi times finer grid, for hydrostatic integration.
	fi = 10.
	i0 = findgen(nz)
	ii = findgen((nz-2)*fi+1)/fi+1.
	mPtoti = intrp1(i0, ii, mPtot1)
	rhomi = intrp1(i0, ii, rhom1)
	hydeqi = intrp1(i0, ii, hydeq0)
	zm = [0, intrp1(ii, i0(1:*), integf(mPtoti, 1./rhomi/gg/hydeqi))+z(1)]
; z of fiducial layer
	zm(0) = zm(1)-bzone*(zm(2)-zm(1))
; set z(T=Teff) = 0.0
	zm = zm - intrp1(mT, Teff1, zm)
	z  = z  - intrp1(cdata(nt,'ttm'), Teff0, z)
	z0=z & z=zm
	hydeq = zder1(alog(mPtot1))*mPtot1/rhom1/gg
	z=z0
	!P.MULTI = [0,2,3]
	!x.margin=[8,2] & !y.margin=[5,1]
	!P.psym = -1
	!x.range = [min([z, zm], max=rmax), rmax]
	!x.title = ztitle
	!x.margin= [11, 3]
    answ = prompt("Plot summary? [y/n/q/s] : ")
	if (answ EQ 'n'  OR  answ EQ 'r'  OR answ EQ 'q') then begin
		reset_graphics
		file, scrfile, /QUIET;      Safeguard the scratch-file from writing
		return
	endif
	if (answ EQ 's') then stop

    lx=0.65& ly0=0.40 & dly=0.05*3& il=0L
	plot, z, alog(cdata(nt,'rhom')),ytit='ln(!7q!x)', syms=.3	;old value
				oplot, zm, lrm, color=col0, syms=.3				;new value
				label, 0, 'Original', psym=!P.psym
				label, 0, 'Target', psym=!P.psym, col=col0
	plot, z, alog(cdata(nt,'ppm')), ytit='ln(!8P!x!ig!n!x)', syms=.3
				oplot, zm, alog(Pgm), color=col0, syms=.3
	plot, zm, eem, ytit='!7e!x', /NODATA
				oplot, z, cdata(nt, 'eem'), syms=.3
				oplot, zm, eem, color=col0, syms=.3
	plot, z, Ptp0, ytit='P!iturb!n/P!itot!n', yr=[0,max([Ptp0,Ptp1])], syms=.3
				oplot, zm, Ptp1, color=col0, syms=.3
	plot, zm, mT, /NODATA, ytit='!8T!x/!8[!xK!8]!x', syms=.3,ytickform='(I0)'
				oplot, z, cdata(nt,'ttm'), syms=.3
				oplot, zm, mT, color=col0, syms=.3
				oplot, !x.crange, [1,1]*Teff0, line=2
				oplot, !x.crange, [1,1]*Teff1, line=2, col=col0
	plot, z, hydeq0,ytit='!s!ad!8P!x!r!s!bd!8z!r!a!i______!n/(g!7q!x!n)',syms=.3
                oplot, !x.crange, [1,1], li=1
				oplot, zm, hydeq, color=col0, syms=.3
; stop
    answ = prompt("Good enough? [y/n/q/s] : ")
	if (answ EQ 'n'  OR  answ EQ 'r'  OR answ EQ 'q') then begin
		reset_graphics
		file, scrfile, /QUIET;      Safeguard the scratch-file from writing
		return
	endif
	if (answ EQ 's') then stop

	!P.multi = 0
; The original entropy
    ups = float(fuz(nt) LE 0.0)
	SS1max = (lookup(max(lrm),max(eem),iv=iS)-SS0)*uS
	SS1 = (lookup(flnrho(nt),fee(nt),iv=iS)-SS0)*uS
	SS1m = haver(SS1)
	SS12m = h2aver(SS1, ups)
    ups = float(uz LE 0.0)
	Nups = total(total(ups, 1), 1)
	SS = (lookup(lnr,ee,iv=iS)-SS0)*uS
	SS2m = h2aver(SS, ups)
	yrr = [min([SS12m(*,0),SS12m(*,1),SS1max,SS2m(*,0),SS2m(*,1)], max=yrmax), $
                                                                         yrmax]
;
; Plot the old entropy structure
    lx=0.80& ly0=yl& dly=0.05& il=0L
	plot, z, SS1m, ytit='!8S!x', ysty=18, syms=.3, yr=yrr
		oplot, z, SS12m(*,0), line=2, syms=.3
		oplot, z, SS12m(*,1), line=1, syms=.3
		oplot, !x.crange, [1,1]*S0min, line=1
		oplot, !x.crange, [1,1]*S0max, line=1
		label, 0, 'Old !8S!x(!8z!x)'
; Overplot the new entropy structure
		oplot, zm, haver(SS), color=col0, syms=.3
		oplot, zm, SS2m(*,0), line=2, color=col0, syms=.3
		oplot, zm, SS2m(*,1), line=1, color=col0, syms=.3
		oplot, !x.crange, [1,1]*S1min, line=1, color=col0
		oplot, !x.crange, [1,1]*S1max, line=1, color=col0
		label, 0, 'Target !8S!x(!8z!x)', color=col0
;
; Print a table of the changes to the model
    if KEYWORD_SET(PRINTTAB) then begin
        openw, lun, 're_teff_'+string(Teff0,form='(I0)')+"-" $
                              +string(Teff1,form='(I0)')+".tab", /GET_LUN
        printf, lun, " The Pturb/Ptot-ratio needs to be adjusted by a " $
            + "factor ", Ptpr
        printf, lun, "         z/[Mm]     lnPtot     \Delta S  \Delta\lnrho  " $
            + "\Delta ee  fct(\del ee) "
        for i=0L, Nz-1L do printf,lun,i,z(i),alog(mPtot0(i)),mS1(i)-mS0(i),$
                            dellr(i),delee(i),fep(i), form='(I4,f12.5,5f12.7)'
        free_lun, lun
    endif
	
; New consistent temperature.
	print, 'Tbot(new-old):', mT(nz-1)-max(cdata(nt,'ttm'))
	answ = prompt("Nice enough [y/n/q/s] - This time the changes are saved to "$
		 + scrfile+" : ")
	if (answ EQ 'n'  OR  answ EQ 'r'  OR answ EQ 'q') then begin
		reset_graphics
		file, scrfile, /QUIET;      Safeguard the scratch-file from writing
		return
	endif
	if (answ EQ 's') then stop

	wvar, lnr, 0, 0
	wvar, uz, 0, 3
	wvar, ee, 0, 4
	wvar, TT, 0, 5
	wpar, Teff1, 0, 'teff'
	wpar, zm(nz-1)-zm(1), 0, 'height'
    wpar, zm(nz-1)-zm(nz-2), 0, 'dz'
	wdata, zm, 0, 0
	wdata, haver(uz), 0, 'uzm'
	ttmin = fltarr(nz)& ttmax = fltarr(nz)
	for i=0,nz-1 do ttmin(i) = min(TT(*,*,i))
	for i=0,nz-1 do ttmax(i) = max(TT(*,*,i))
	wdata, ttmin, 0, 'ttmin'
	wdata, ttmax, 0, 'ttmax'
	wdata, mT, 0, 'ttm'
	eem = haver(ee)
	wdata, rhom1, 0, 'rhom'
	wdata, exp(smoothfx(z, alog(rhom1))), 0, 'rho0'
	wdata, Pgm, 0, 'ppm'
	wdata, exp(smoothfx(z, alog(Pgm))), 0, 'pp0'
	wdata, eem, 0, 'eem'
	wdata, smoothfx(z, eem), 0, 'ee0'
	wdata, ptm1, 0, 'ptm'
;
; Here I calculate the parameters eebot and rhobot, to give the exact
; entropy of the inflowing plasma.
	rho_e_bot, 0, tabfile=tabfile
; Close and reopen the file to avoid accidental writing to it.
	file, scrfile, /QUIET
; Take care of meanrhoT.F77.dat
	print, '    How do you want to make the new meanrhoT-structure?'
	print, '1) Interpolate to new z-grid  2) Use new rhom, ttm?  [1/2] : ', $
																form='(A,$)'
	stra=get_kbrd(1)& print, stra
	if (stra NE '1' AND stra NE '2') then begin
		print, '"'+stra+'"'+' is not a valid choice.'+$
				' Returning without changing meanrhoT.F77.dat.'
	endif else begin
		if (stra EQ '2') then begin
			meanrhoT_update, z, zm, rhom1, mT
		endif else begin
			meanrhoT_update, z, zm
		endelse
	endelse
	add_to_log

	print, 'Remember to zshift_w to straighten out the z-scale.'
	reset_graphics
end
