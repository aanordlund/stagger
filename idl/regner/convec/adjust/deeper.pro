;
;   Program to extend simulations inward, to a logarithmic gass pressure
;   LogPGn.
;     lnT is integrated inward with respect to lnP assuming an exponentially
;   decreasing super-adiabatic gradient, \nabla_s, and looking-up the adiabatic
;   gradient, \nabla_ad, in the MHD-EOS-table.
;   This routine need a l2means.dat file of averages.
;
;   Update history:
;   ---------------
;   17.09.2004 Coded/RT
;
;   Known bugs: None
;
pro deeper, LogPGn, scrfile=scrfile, tabfile=tabfile, tmpfile=tmpfile, it=it, $
																	STOP=STOP
	common coff,mpar,mav,ipack,incr,ioff,time,voff,cfile,irec
	common cdat,x,y,z,nx,ny,nz,nw,ntmax,date0,time0

; Find and load the scratch-file.
	defaultscr, scrfile=scrfile
	if (strpos(scrfile, '.') LT 0) then scrfile=scrfile+'.scr'
;	print, '  Loading '+scrfile
; Use the last time-step in the file
	default, it, ntmax-1L
; Graphics settings
	c0 = (!D.TABLE_SIZE-1.)*.6
; Infinitesimal quantities and convergence criteria
	dLogP = 1e-2
;
	default, tmpfile, stamnavn()
	if (strpos(tmpfile, '.tmp') LT 0) then tmpfile=tmpfile+'.tmp'
	default, tabfile, stamnavn()
	if (strpos(tabfile, '.tab') LT 0) then tabfile=tabfile+'.tab'
; Load the table and generate entropy-table
	tabtmp, tmpfile, mz=nz
	sstab, iS
	SS0 = lookup(alog(10)*.5, 2.5, iv=iS)
	uS = 1.e+4
; Super-adiabatic gradient from l2means.dat
	mDS = mtdata('DSS')
	mDAD= mtdata('DAD')
	mLogPG = alog10(mtdata('PG'))
	mz  = mtdata(-1)
; Find min and max of mDS and fit exponential decay with depth
	dum = max(mDS, imaxDS)
	dum = min(mDS(imaxDS:*), iminDS)
	i0  = long(imaxDS + iminDS*.70)
	i1  = long(imaxDS + iminDS*.95)
	ip  = where(mDS GT 0.0)
	ADS = reform(poly_fit(mLogPG(i0:i1), alog10(mDS(i0:i1)), 1L))
	ADS(0) = ADS(0) - poly(mLogPG(i1), ADS) + alog10(mDS(i1))
	plot, mLogPG(ip), alog10(mDS(ip))
	plots, mLogPG(i0:i1), alog10(mDS(i0:i1)), psym=2, col=c0
	oplot, !x.crange, poly(!x.crange, ADS), line=2
;
; Read the local MHD-EOS-table
	mhd_read, 'EOS.tab', dir='./', EOS=EOS, tl=LogTtab
	NP = long((LogPGn - mLogPG(i1))/dLogP + .9999999999)
	mLogPi = [findgen(NP-1L)*dLogP + mLogPG(i1), LogPGn]
	mLogTi = fltarr(NP)  &  mLogTi(0) = (alog10(mtdata('T')))(i1)
	mLrhoi = fltarr(NP)  &  mLrhoi(0) = (alog10(mtdata('RHO')))(i1)
	mDADi  = fltarr(NP)  &  mDADi(0)  = mDAD(i1)
	NdLr   = 15L
	dLr    = findgen(NdLr)/(NdLr-1.)*2.0 -.9

	for i=1L, NP-1L do begin
		mDADi(i)   = mhd_intrp(mlogTi(i-1), mLrhoi(i-1), 7, EOS=EOS, tl=LogTtab)
		dLogTdLogP = mDADi(i) + 1d1^poly(mLogPi(i-1), ADS)
		mLogTi(i)  = mLogTi(i-1) + dLogTdLogP*dLogP
		LogPgx     = mhd_intrp(replicate(mlogTi(i), NdLr), mLrhoi(i-1)+dLr, 1, $
															EOS=EOS, tl=LogTtab)
		mLrhoi(i)  = intrp1(LogPgx, mLogPi(i), mLrhoi(i-1)+dLr)
	endfor
	stra = prompt(" Next plot? ")
	!P.multi = [0,1,2]
	plot, [mLogPG(0:i1-1), mLogPi], [(alog10(mtdata('T')))(0:i1-1L),mLogTi]
	plot, [mLogPG(0:i1-1), mLogPi], [mDAD(0:i1-1L), mDADi], psym=-2, syms=.3
	!P.multi = 0
; Stretch from i1 to nz-1  and reduce fluctuations by pressure-ratio.
	mLogP0   = alog10(cdata(it,'ppm')) + 5.0
	Nintrp   = nz-i1-1
	mLogP    = (findgen(Nintrp)+1.)/Nintrp*(LogPGn - mLogPG(i1)) + mLogPG(i1)
	mLogT    = intrp1(mLogPi, mLogP, mLogTi)
	mlnrho   =(intrp1(mLogPi, mLogP, mLrhoi) + 7.0)*alog(1e1)
	stra = prompt(" Save the new structure to "+scrfile+"? [y/n/q/s] ")
	if (stra EQ 's') then stop
	if (stra EQ 'n' OR stra EQ 'q') then return
	file, scrfile, /QUIET, /WRITE
	for i=i1+1L, nz-1L do begin
		j = i-i1-1L
		Prat = 1e1^(mLogP0(i)-mLogP(j))
		lnr  = fpln(it, 0L, i)  & mlnr= aver(lnr)
		ux   = fpln(it, 1L, i)
		uy   = fpln(it, 2L, i)
		uz   = fpln(it, 3L, i)
		tt   = fpln(it, 5L, i)  & mtt = aver(tt)
		tt   = (tt-mtt)*Prat   + 1e1^mLogT(j)
		lnr  = (lnr-mlnr)*Prat + mlnrho(j)
		wpln, lnr,                       it, 0L, i
		wpln, ux*Prat,                   it, 1L, i
		wpln, uy*Prat,                   it, 2L, i
		wpln, uz*Prat,                   it, 3L, i
		wpln, lookup_tmp(lnr, tt, iv=2), it, 4L, i
;  \DeltaP_12/<rho>_12 in simulation units.
		if (j EQ 0L) then begin
			dPrhom = 1e-5*(1e1^mLogP(j)-1e1^mLogP0(i1))/$
									(1e1^(mLrhoi(0)+7.0)+exp(mlnrho(j)))
		endif else begin
			dPrhom = 1e-5*(1e1^mLogP(j)-1e1^mLogP(j-1))/$
									(exp(mlnrho(j-1))+exp(mlnrho(j)))
		endelse
		dz   = dPrhom/cpar(0,'grav')
		print, i, j, z(i-1), dz, dPrhom 
		z(i) = z(i-1) + dz
	endfor
	wdata, z, 0, 'z'
	update, it, izmin=i1+1L
	logtext = string(mLogP0(nz-1L), LogPGn, z(nz-1L), $
				form='("MaxLogP=",F5.2," -> ",F5.2,", maxz=",f7.2," -> ")')
	add_to_log, logtext, /CLEAR
	print, " Remember to now use hydequalize.pro "

	if KEYWORD_SET(STOP) then stop
end
