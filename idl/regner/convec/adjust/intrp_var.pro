;
;   This routine takes care of
;	- all interpolations (consistently based on lnr and ee).
;	- saves all changes to scr2fil.
;	- updates the meanrhoT.F77.dat-file.
;	- plots and compares the resulting T-structure with the old one.
;	- calculates new rhobot, eebot and ppbot values.
;	- reopens scr2fil in read-only mode.

;   Calling sequence:
;	for i=1, 6 do begin
;		intrp_var, i, z1, z2
;	endfor
;
;   Update history:
;   ---------------
;   29.04.1997 Coded/RT
;   16.04.2006 No longer forcing ee(*,*,0) = eem(0)/RT
;	22.06.2006 Changed to Remo's ee(*,*,0)=ee(*,*,1) boundary condition/RT
;   15.07.2007 Fixed bug in computing width1 on small endian machines/RT
;
;   Known bugs: Can not yet handle the magnetic case...
;
pro intrp_var, nn, z1, z2
	common coff,mpar,mav,ipack,incr,ioff,time,voff,cfile,irec
	common cdat,x,y,z,nx,ny,nz,nw,ntmax,date0,time0
	common intrpcom,nx1,ny1,nz1,nx2,ny2,nz2,it1,it2,scr1fil,scr2fil,tabfil,halv
	common czder,w1,w2,w3

	ipar=[3,4,6,7,8,11,12,13,17,18,19,20,26,27,28,29,30,53,54,60,61,62,63,64,$
			 															65,66]
	file, scr1fil, /QUIET
;	default, it1, ntmax-1L	; source timestep
;	default, it2, 0L		; destination timestep
	if (nn EQ 1) then print, scr1fil, it1, ntmax, form='(A20,2I4,$)'
	mpar1 = 1L*mpar & mav1 = 1L*mav
	nz1 = N_ELEMENTS(z1)
	nz2 = N_ELEMENTS(z2)
	if (nn LT 6) then begin   ; lnrho, ux, uy, uz, ee
		nvar = nn-1
        vol = var(it1, nvar)
		if (nvar LT 1 OR nvar GT 3) then begin
			mvol1=haver(vol)
			for i=0, nz1-1 do vol(*,*,i)=vol(*,*,i)-mvol1(i)
			mvol2 = intrp1(z1, z2, mvol1, /EXTRA)
		endif
		if (halv) then begin
			print, 'Halving the horizontal extent.'
			nx1 = nx/2
			ny1 = ny/2
			vol = vol(0:nx1-1,0:ny1-1,*)
		endif
;		vol = vol(0:63,0:63,*)
		if (nx1 EQ nx2  AND  ny1 EQ ny2) then begin
;
;  Constant extrapolation of velocities and lnr-/ee-fluctuations.
			vol2 = intrp3(z1, z2, vol)
		endif else begin
			if (nn EQ 2) then $
				print,nx1,ny1,nx2,ny2,form='("New dimensions: ",2I5," -> ",2I5)'
			iix = findgen(nx2)*float(nx1)/float(nx2)
			iiy = findgen(ny2)*float(ny1)/float(ny2)
			w1 = fltarr(nz1)& w2=w1 & w3=w1
			voltmp = intrp3(z1, z2, vol, /EXTRAPOLATE)
			vol2 = fltarr(nx2, ny2, nz2)
			for i=0, nz2-1 do begin
				vol2(*,*,i) = interpolate(reform(voltmp(*,*,i)), iix, iiy,/GRID)
			endfor
		endelse
		if (nvar LT 1 OR nvar GT 3) then begin
			for i=0, nz2-1 do vol2(*,*,i)=vol2(*,*,i)+mvol2(i)
			for i=0, nz1-1 do vol(*,*,i) =vol(*,*,i) +mvol1(i)
		endif
		file, scr2fil, /QUIET, /WRITEACCES & if (incr GT 10L) then ipack=1L
		if (nn EQ 1) then print, scr2fil, it2, ntmax, form='(A20,2I4)'
		if ((nx1 NE nx2  OR  nz1 NE nz2) AND nvar EQ 0) then begin
;			set_scr_pointers, scr1fil, scr2fil
;			file, scr2fil, /QUIET, /WRITEACCES & if (incr GT 10L) then ipack=1L
		endif
; Don't store ee yet - need to set ee in the top plane.
		if (nn NE 5) then wvar, vol2, it2, nvar
	endif
	!x.style=2
    zrange = [min([z1, z2], max=zmax), zmax]
	case (nn) of
		1:begin
			!y.title='ln!7q!x'
			z=z1 & nz=nz1 & minmaxplot, vol/alog(10.0)-7.0, xr=zrange
			z=z2 & nz=nz2 & minmaxplot, vol2/alog(10.0)-7.0, /OPLOT
            rhom = haver(exp(vol2))
			wdata, rhom, it2, 'rhom'
			wdata, smoothfx(z2, rhom), it2, 'rho0'
          end
        2:begin
			!y.title='!8v!ix!n!x/!8[!xkm s!e-1!n!8]!x'
			z=z1 & nz=nz1 & minmaxplot, vol*10., xr=zrange
			z=z2 & nz=nz2 & minmaxplot, vol2*10., /OPLOT
          end
        3:begin
			!y.title='!8v!iy!n!x/!8[!xkm s!e-1!n!8]!x'
			z=z1 & nz=nz1 & minmaxplot, vol*10., xr=zrange
			z=z2 & nz=nz2 & minmaxplot, vol2*10., /OPLOT
          end
        4:begin
			!y.title='!8v!iz!n!x/!8[!xkm s!e-1!n!8]!x'
			z=z1 & nz=nz1 & minmaxplot, vol*10., xr=zrange
			z=z2 & nz=nz2 & minmaxplot, vol2*10., /OPLOT
            varm = haver(vol2)
            wdata, varm, it2, 'uzm'
          end
        5:begin
			!y.title='!7e!x/!8[!x10!e12!nerg g!e-1!n!8]!x'
			vol2(*,*,0) = vol2(*,*,1)
            eem = haver(vol2)
;			eem(0) = eem(1) - (eem(2)-eem(1))/(z2(2)-z2(1))*(z2(1)-z2(0))
;			vol2(*,*,0) = eem(0)
			wvar, vol2, it2, nvar
			z=z1 & nz=nz1 & minmaxplot, vol, xr=zrange
			z=z2 & nz=nz2 & minmaxplot, vol2, /OPLOT
            wdata, eem, it2, 'eem'
			wdata, smoothfx(z2, eem), it2, 'ee0'
          end
;
;  Source file is still loaded for nn >= 6 - Destination not loaded yet.
        6:begin
			!x.margin=[10,1]
			!y.title='!8T!x/!8[!xK!8]!x'
			!y.tickformat='(I5)'
 		    if (tabfil EQ 'EOSrhoe.tab') then eostab   $
 									     else table, tabfil, mz=nz1
			print, 'intrp_var: Loaded ', tabfil
; Read cpar-block
			cp = assoc(1, bytarr(4L*mpar), irec(it1*incr+incr-1))
			cpars = cp(0)
			width1 = swap_endian(float(cpars(4*4+lindgen(4)), 0L), $
												/SWAP_IF_LITTLE_ENDIAN)
; Read cdata-block
			cd = assoc(1, fltarr(nz, mav), irec(it1*incr+incr-1)+4L*mpar)
			cdata1 = reform(cd(0), nz, mav)
; Read surface intensity
			Is = assoc(1, fltarr(nx,ny), irec(it1*incr+incr-1)+4L*(mpar+nz*mav))
			Ints = Is(0)
; Compute consistent temperature cube and averages
            update, scr=scr2fil, tab=tabfil
; Load the destination file
			file, scr2fil, /QUIET, /WRITEACCES & if (incr GT 10L) then ipack=1L
; Write cpar-block
			cp = assoc(1, bytarr(4L*mpar), irec(it2*incr+incr-1))
			if (mpar EQ mpar1) then begin
				cp(0) = cpars 
			endif else begin
				icp = [lindgen(60L),63L+lindgen(6L),72L+lindgen(6L),$
																79L+lindgen(4L)]
				icp4= reform(lindgen(4)#replicate(1L,76L) + $
												4L*replicate(1L,4L)#icp, 4L*76L)
				if (mpar EQ 76L AND mpar1 EQ 83L) then begin
					cp(0) = cpars(icp4)
				endif else begin
					if (mpar EQ 83L AND mpar1 EQ 76L) then begin
						cpar1 = bytarr(4L*mpar)
						cpar1(icp4) = cpars
						cp(0) = cpar1
					endif else begin
						print, 'The case of mpar=',mpar,' and mpar1=',mpar1,$
							   ' is not supported yet. Bailing out!', $
							   form = '(A0,I0,A0,I0,A0)'
						stop
					endelse
				endelse
			endelse
			width2 = width1/(1.+halv)
; Fill in the new values in the cpar section of cdata
			wpar, nx2,                         0, 'nx'
			wpar, ny2,                         0, 'ny'
			wpar, nz2,                         0, 'nz'
			wpar, z2(nz2-1) - z2(1),           0, 'height'
			wpar, width2,                      0, 'width'
			wpar, width2/nx2,                  0, 'dx'
			wpar, width2/ny2,                  0, 'dy'
			wpar, z2(nz2-1) - z2(nz2-2),       0, 'dz'
			wpar, (z2(1)-z2(0))/(z2(2)-z2(1)), 0, 'bzone'
; cdata of the destination file.
			cdata2 = fltarr(nz2, mav)
			if (mav EQ mav1) then begin
				for i=0, mav-1L do cdata2(*,i)=intrp1(z1,z2,cdata1(*,i),/EXTRA)
			endif else begin
; I assume that differences in the cdata-block can only be in the number of
; variables and that extra variables are fit in before the 5 scratch-blocks.
				icdat = [lindgen(min([mav, mav1],max=maxmav)-5L), $
						 reverse(maxmav-1L-lindgen(5))]
				if (mav GT mav1) then begin
					for i=0, mav1-1L do $
						cdata2(*,icdat(i))=intrp1(z1,z2,cdata1(*,i),/EXTRA)
				endif else begin
					for i=0, mav-1L do $
						cdata2(*,i)=intrp1(z1,z2,cdata1(*,icdat(i)),/EXTRA)
				endelse
			endelse
			c0 = (!D.TABLE_SIZE-1)*.8
			cdata2(*,cindex('z'))   = z2
; These variables have already been saved - protect them from overwriting.
			cdata2(*,cindex('uzm')) = cdata(it2, 'uzm')
; Write cdata-block
			cd = assoc(1, fltarr(nz,mav),irec(it2*incr+incr-1)+4L*mpar)
			cd(0) = reform(cdata2, nz*mav)
; Compute consistent temperature cube and averages
            update, scr=scr2fil, tab=tabfil
; Re-load the destination file with writeaccess
			file, scr2fil, /QUIET, /WRITEACCES & if (incr GT 10L) then ipack=1L
; Write surface intensity
			Is = assoc(1, fltarr(nx,ny), irec(it2*incr+incr-1)+4L*(mpar+nz*mav))
			if (nx1 EQ nx2  AND  ny1 EQ ny2) then begin
				Is(0) = Ints
			endif else begin
				iix   = findgen(nx2)*float(nx1)/float(nx2)
				iiy   = findgen(ny2)*float(ny1)/float(ny2)
				Is(0) = interpolate(Ints, iix, iiy, /GRID)
			endelse
;
; Re-open the file without write-permission to avoid accidental overwriting.
			file, scr2fil, /QUIET
          end
		  else:begin
			print, "This version doesn't support more than 6 cases."
			print, "nn="+string(nn,form='(I0)')+" is invalid. Doing nothing."
		  end
    endcase
	!x.style=1
end
