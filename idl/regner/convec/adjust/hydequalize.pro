;
;   Program to change z-scale to result in hydrostatic equilibrium.
;
;   Update history:
;   ---------------
;   18.05.2004 Coded/RT
;   27.08.2004 Automatic printing to log-file./RT
;   03.05.2006 Corrected dum = poly_fit(z1(0:6), h1(0:6),... -> h1(1:7)
;   05.10.2006 ptm can be negative after 'extend'; now using ptm>0.0 /RT
;
pro hydequalize, it, scrfile=scrfile, xrange=xrange, FULLZRANGE=FULLZRANGE, $
                                                                    STOP=STOP
	common cdat,x,y,z,nx,ny,nz,nw,ntmax,date0,time0

    rt_graphics
; Find and load the scratch-file.
	defaultscr, scrfile=scrfile
	if (strpos(scrfile, '.') LT 0) then scrfile=scrfile+'.scr'
	file, scrfile, /QUIET, /WRITE
	print, '  Loading '+scrfile
; Use the last time-step in the file
	default, it, ntmax-1L

	c0 = (!D.TABLE_SIZE-1.)*.75
	fi = 10.
	i0 = findgen(nz-1L)+1.
	bz = cpar(it,'bzone')
	ii = findgen((nz-2L)*fi+1.)/fi+1.
	z0 = z(1:*)
	zi = intrp1(i0, ii, z0)
	mPgas1 = haver(exp(lookup(flnrho(it),fee(it),iv=0)))
	mPtot1 = mPgas1 + (cdata(it,'ptm')>0.0)	& mPtot11=mPtot1(1:*)
;	mPtot1 = cdata(it,'ppm')+(cdata(it,'ptm')>0.0)	& mPtot11=mPtot1(1:*)
;	mPtot1(0) = exp(intrp1(z(1:*),z(0),alog(mPtot11),/EXTRA))
	rhom1  = cdata(it,'rhom') 					& rhom11 = rhom1(1:*)
	mPtoti = intrp1(z0, zi, mPtot11)
	rhomi  = intrp1(z0, zi, rhom11)
	gg = cpar(it,'grav')
	hydeq1 = derf(z0,alog(mPtot11))*mPtot11/rhom11/gg
	hydeq0 = (hydeq1-1.)*.1+1.
	hydeqs = smoothfx(z0, hydeq0)
	hydeqs = smoothfx(z0, hydeqs)
	hydeqs = smoothfx(z0, hydeqs)
	hydeqi = intrp1(z0, zi, hydeqs)
	z1 = intrp1(zi, z0, integf(mPtoti, 1./rhomi/gg/hydeqi)) 
	z1 = z1 - intrp1((cdata(it,'ttm'))(1:*), cpar(it,'teff'), z1)
	Nz0 = 51L & Nz02 = (Nz0-1L)/2L & hyd0=fltarr(Nz0) & sgm=hyd0
	z10 = z1(0) - (findgen(Nz0)+1.)/Nz0*bz*(z(2)-z(1))*10
;	z10 = ((findgen(Nz0)-Nz02)/(Nz02+1.)-1.)*bz*(z1(1)-z1(0)) + z1(0)
	!x.title='!8z!3/[!xMm!3]!x'
    default, xrange, [z10(Nz0-1L),-z(0)]
    if KEYWORD_SET(FULLZRANGE) then xrange=[z10(Nz0-1L), z(NZ-1L)]
	plot, z, hydeq1, yr=[0,2], xr=xrange,  $
						ytitle='!s!ad!8P!x!r!s!bd!8z!r--!n/(g!7q!x!n)'
	plots, [1,1]*z1(0),  !y.crange, line=1
	plots, [1,1]*z1(6), !y.crange, line=1
	for i=0, Nz0-1L do begin
		h1      = derf([z10(i), z1], alog(mPtot1))*mPtot1/rhom1/gg
		hyd0(i) = h1(0)
;
;  Determine amplitude of wiggles around a smooth function
;		dum = poly_fit(z1(0:6), h1(1:7), 2L, chisq=chisq)
;		sgm(i)  = chisq
		sgm(i)  = rms(h1-1.)
		plots, [z10(i), z1], h1, col=.6*c0
	endfor
	oplot, !x.crange, [1,1], line=2
;
;  Choose the z(0) that cause the smallest wiggles in hydeq.
	splmin, z10, sgm, zmin, smin& print, smin
;	z1 = [intrp1(hyd0, (hydeqi(0)-1.)*1.2+1., z10), z1]
	z1 = [zmin, z1]
	plots, z, hydeq1
	plots, z1, derf(z1,alog(mPtot1))*mPtot1/rhom1/gg, col=c0, th=2
	label, 0.7, 0.80, 0, 'Old !8z!x-scale'
	label, 0.7, 0.75, 0, 'New !8z!x-scale', col=c0
	stra = prompt(" Continue [y/n/s/q] ? ")
	if (stra EQ 's') then stop
	if (stra EQ 'n' OR stra EQ 'q') then begin
		reset_graphics
		return
	endif

	plot, z, alog10(rhom1), ytit='Log!i10!n!7q!x'
	plots, z1, alog10(rhom1), col=c0, psym=-2, syms=.3
	label, 0.7, 0.50, 0, 'Old !8z!x-scale'
	label, 0.7, 0.45, 0, 'New !8z!x-scale', col=c0, psym=-2, syms=.3
	stra = prompt(" Continue [y/n/s/q] ? ")
	if (stra EQ 's') then stop
	if (stra EQ 'n' OR stra EQ 'q') then begin
		reset_graphics
		return
	endif

	plot, z, alog10(mPtot1), ytit='Log!i10!n!8P!x!itot!n'
	plots, z1, alog10(mPtot1), col=c0, psym=-2, syms=.3
	label, 0.7, 0.50, 0, 'Old !8z!x-scale'
	label, 0.7, 0.45, 0, 'New !8z!x-scale', col=c0, psym=-2, syms=.3
	stra = prompt(" Write the new z-scale to "+scrfile+" [y/n/s/q] ? ")
	if (stra EQ 's') then stop
	if (stra EQ 'n' OR stra EQ 'q') then begin
		reset_graphics
		return
	endif
;
;  Write the new z-scale to the scratch-file
	file, scrfile, /QUIET, /WRITE
	wdata, z1, it, 'z'
	file, scrfile, /QUIET
	add_to_log, /CLEAR
	print, ""
	print, " Now use zshift_w to get a smooth z-scale."
	print, ""

end
