;
;   Function to return locations of simulations, according to a number
;   of selections criteria:
;
;   Arguments:
;   ----------
;     file:				name of file to load ['starval.tab']
;     dir:				directory in which to look for <file> ['~/idl/convec/']
;     mixis:			The metal mixture has to match <mixis>
;     abundis:          The metal abundance has to match <abundis>
;     resis:			The resolution has to match <resis>
;     nameis:			The name has to match one of <nameis>
;     METALPOOR:        If set, include only sims with [Fe/H] <  +0.00
;     METALRICH:        If set, include only sims with [Fe/H] >= +0.00
;     LMEANS:			If set, include only sims with Lagrangian averages
;     TMEANS:			If set, include only sims with optical depth averages
;     SCRATCH:			If set, include only sims with a scratch-file
;     TAB:				If set, include only sims with a tab-file
;     TMP:				If set, include only sims with a tmp-file
;     IVSPCTR:			If set, include only sims with I/v granulation spectra
;     ALFAFIT:			If set, include only sims that have had 1D envelope
;						models fitted to them
;     OBS:				If set, include only sims that are associated with stars
;     DEBUG:            If set, print various diagnostics
;     STOP:				If set, stop before returning - for debugging.
;
;   Update history:
;   ---------------
;   23.02.2005 Coded/RT
;   16.03.2005 Included option of selecting on names of simulations/RT
;   13.03.2006 Included option of selecting on [Fe/H]: abundis/METALRICH/POOR/RT
;   28.09.2006 Included p-mode data; Overall RMS amplitude, ARMS, and amplitudes
;              and periods of the two strongest modes, A1,P1,A2 and P2, resp./RT
;   28.09.2006 All variables now included via @cstarval/RT
;   25.10.2006 Much improved checking for ?2mean files - checks sizes and
;              generates time-averages if not present/RT
;   25.10.2006 Changed from using the obsolete findfile to file_search/RT
;   14.12.2006 Changed from vform to strvlform from cstarvaltab/RT
;
;   Known bugs: Can't handle old starval.tab files with fewer entries.
;
function read_startab, file=file, dir=dir, mixis=mixis, resis=resis, $
			nameis=nameis, abundis=abundis, ext=ext, $
			LMEANS=LMEANS, TMEANS=TMEANS, SCRATCH=SCRATCH, TAB=TAB, TMP=TMP, $
			IVSPCTR=IVSPCTR, ALFAFIT=ALFAFIT, OBS=OBS, METALPOOR=METALPOOR, $
			METALRICH=METALRICH, DEBUG=DEBUG, STOP=STOP
@cstarval

	default, file, 'starval.tab'
	default, dir, '~/idl/convec/'
	if (strmid(dir,strlen(dir)-1,1) NE '/') then dir=dir+'/'
	default, ext, ''
	if (strmid(ext, 0, 1) NE '.'  AND  ext NE '') then ext = '.'+ext
	PRTDEBUG = KEYWORD_SET(DEBUG)

	openr, lun, dir+file, /GET_LUN
	s1 = ''
	readf, lun, s1
	vars = str_sep(s1, " ")
	Nvar = N_ELEMENTS(vars)
	if KEYWORD_SET(DEBUG) then print, " Read-in ",Nvar," variables."
	s1='' & s2='' & s3=''& s4='' & s5='' & s6=''
	f1=fltarr(2) & N2=Nvar-7L& f2=fltarr(N2)
	readf, lun, s1
	Nstars = 0L
	hosts_dir, cnvdir=cnvdir, cnvscr=cnvscr
	while (not eof(lun)) do begin
		readf,lun,s1,s2,s3,s4,s5,f1,s6,f2,form=strvlform
		if (s5 EQ '-') then f1(0) = -f1(0)
		include = (1 GT 0)
		if (N_ELEMENTS(resis) GT 0L) then $
			include = (include AND strtrim(s3) EQ resis)
		if (N_ELEMENTS(mixis) GT 0L) then $
			include = (include AND strtrim(s4) EQ mixis)
		if (N_ELEMENTS(abundis) GT 0L) then $
			include = (include AND f1(0) EQ abundis)
		if (N_ELEMENTS(nameis) GT 0L) then $
			include = (include AND (where(strtrim(s1) EQ nameis))(0) GE 0)
		if KEYWORD_SET(OBS) then $
			include = (include AND f1(1) GT 0.0 AND f2(0) GT 0.0 $
							   AND f2(1) GT 0.0 AND f2(2) GT 0.0)
		if KEYWORD_SET(ALFAFIT) then $
			include = (include AND f2(3) GT 0.0 AND f2(5) GT 0. AND f2(7) GT 0.)
		if KEYWORD_SET(METALPOOR) then $
			include = (include AND s5 EQ '-')
		if KEYWORD_SET(METALRICH) then $
			include = (include AND s5 EQ '+')
		name0  = strtrim(s1)
		rundir = cnvdir+'stars/'+strtrim(s3)+'/'+name0+'/run/'
		scrdir = cnvscr+strtrim(s3)+'/'+name0+'/'
		smdir = ''
		if (file_test(rundir)) then smdir=rundir $
			else if (file_test(scrdir)) then smdir=scrdir
;
; Check for pseudo Lagrangian horisontal averages of the simulation
;
		if (KEYWORD_SET(LMEANS) AND include) then begin
;
; Check the rundir first
			b1 = 0 & nz=82L
;
; Get list of l2mean.dat files
			lfiles = file_search(rundir+'l2mean.dat'+ext)
;
; Make sure there is at least 1 timestep in the file
			if ((file_info(lfiles(0))).size GT (nz*4+21)) then b1 = 1
			prt = str_sep(lfiles(0),'\.dat')
			mlfile= rundir+'l2means.dat'+ext
;
; Check to see whether there is a time-averaged file - otherwise make it
			if ((file_info(mlfile)).size LT (nz*100*4)) then begin
				if (b1) then begin
					cd, rundir, current=olddir
					defaultscr
					mtgen, ext=ext
					cd, olddir
				endif
			endif else      b1 = 1
;
; Now check the scrdir
			b2 = 0
;
; Get list of l2mean.dat files
			lfiles = file_search(scrdir+'l2mean.dat'+ext)
;
; Make sure there is at least 1 timestep in the file
			if ((file_info(lfiles(0))).size GT (nz*4+21)) then b2 = 1
			prt = str_sep(lfiles(0),'\.dat')
			mlfile= scrdir+'l2means.dat'+ext
;
; Check to see whether there is a time-averaged file - otherwise make it
			if ((file_info(mlfile)).size LT (nz*100*4)) then begin
				if (b2) then begin
					cd, scrdir, current=olddir
					defaultscr
					mtgen, ext=ext
					cd, olddir
				endif
			endif else      b2 = 1
			include = (include AND (b1 OR b2))
;
; Use the rundir if the files are available there
			if (b2) then smdir=scrdir
			if (b1) then smdir=rundir
		endif
;
; Check for optical depth averages of the simulation
;
		if (KEYWORD_SET(TMEANS) AND include) then begin
;
; Check the rundir first
			b1 = 0 & nz=82L
;
; Get list of t2mean.dat files
			tfiles = file_search(rundir+'t2mean.dat'+ext)
			if (PRTDEBUG) then print, rundir, tfiles, form='(A0)'
;
; Make sure there is at least 1 timestep in the file
			if ((file_info(tfiles(0))).size GT (nz*4+21)) then b1 = 1
			prt = str_sep(tfiles(0),'\.dat')
			mtfile= rundir+'t2means.dat'+ext
			if (PRTDEBUG) then print, mtfile, (file_info(mtfile)).size,nz*100*4
;
; Check to see whether there is a time-averaged file - otherwise make it
			if ((file_info(mtfile)).size LT (nz*100*4)) then begin
				if (b1) then begin
					cd, rundir, current=olddir
					defaultscr
					ttgen, ext=ext
					cd, olddir
				endif
			endif else      b1 = 1
;
; Now check the scrdir
			b2 = 0
;
; Get list of t2mean.dat files
			tfiles = file_search(scrdir+'t2mean.dat'+ext)
			if (PRTDEBUG) then print, scrdir, tfiles, form='(A0)'
;
; Make sure there is at least 1 timestep in the file
			if ((file_info(tfiles(0))).size GT (nz*4+21)) then b2 = 1
			prt = str_sep(tfiles(0),'\.dat')
			mtfile= scrdir+'t2means.dat'+ext
;
; Check to see whether there is a time-averaged file - otherwise make it
			if (PRTDEBUG) then print, mtfile, (file_info(mtfile)).size,nz*100*4
			if ((file_info(mtfile)).size LT (nz*100*4)) then begin
				if (b2) then begin
					cd, scrdir, current=olddir
					defaultscr
					ttgen, ext=ext
					cd, olddir
				endif
			endif else      b2 = 1
			if (PRTDEBUG) then print, " t2means: ", include, b1, b2
			include = (include AND (b1 OR b2))
;
; Use the rundir if the files are available there
			if (b2) then smdir=scrdir
			if (b1) then smdir=rundir
		endif
		if KEYWORD_SET(IVSPCTR) then begin
			b1 = file_test(rundir+'*IV(t).dat')
			b2 = file_test(scrdir+'*IV(t).dat')
			include = (include AND (b1 OR b2))
			if (b2) then smdir=scrdir
			if (b1) then smdir=rundir
		endif
		if KEYWORD_SET(SCRATCH) then begin
			b1 = file_test(rundir+name0+'.scr')
			b2 = file_test(scrdir+name0+'.scr')
			include = (include AND (b1 OR b2))
			if (b2) then smdir=scrdir
			if (b1) then smdir=rundir
		endif
		if KEYWORD_SET(TAB) then begin
			b1 = file_test(rundir+name0+'.tab')
			b2 = file_test(scrdir+name0+'.tab')
			include = (include AND (b1 OR b2))
			if (b2) then smdir=scrdir
			if (b1) then smdir=rundir
		endif
		if KEYWORD_SET(TMP) then begin
			b1 = file_test(rundir+name0+'.tmp')
			b2 = file_test(scrdir+name0+'.tmp')
			include = (include AND (b1 OR b2))
			if (b2) then smdir=scrdir
			if (b1) then smdir=rundir
		endif
		if (include) then begin
            noPtx= 1L
			if (Nstars EQ 0L) then begin
				name	= [name0]
				navn	= [strtrim(s2)]
				res		= [strtrim(s3)]
				mix		= [strtrim(s4)]
;				if (s5 EQ '-') then f1(0)=-f1(0)
				abund	= [f1(0)]
				Tefobs	= [f1(1)]
				grav	= [float(s6)]
				Lum		= [f2(0)]
				Mass	= [f2(1)]
				Rad		= [f2(2)]
				alfa	= [f2(3)]
				sigalfa = [f2(4)]
				fvcnv	= [f2(5)]
				sigfvc	= [f2(6)]
				dCZ		= [f2(7)]
				ARMS    = [f2(8)]
				A1      = [f2(9)]
				P1      = [f2(10)]
				A2      = [f2(11)]
				P2      = [f2(12)]
				Agran   = [f2(13)]
				I1      = [f2(14)]
				I2      = [f2(15)]
				I3      = [f2(16)]
				I4      = [f2(17)]
				I5      = [f2(18)]
				I6      = [f2(19)]
				Smin    = [f2(20)]
				Smax    = [f2(21)]
				Ptmax   = [f2(22)]
if (N2 GE 24L) then	Ptotph  = [f2(23)]
if (N2 GE 25L) then	AIbckgr = [f2(24)]
if (N2 GE 26L) then	BIbckgr = [f2(25)]
if (N2 GE 27L) then	eIbckgr = [f2(26)]
if (N2 GE 28L) then	aLD1    = [f2(27)]
if (N2 GE 29L) then	aLD2    = [f2(28)]
if (N2 GE 30L) then	aLD3    = [f2(29)]
if (N2 GE 31L) then	aLD4    = [f2(30)]
if (N2 GE 32L) then	depth   = [f2(31)]
if (N2 GE 33L) then LDfact  = [f2(32)]
if (N2 GE 34L) then A_Hp    = [f2(33)]
if (N2 GE 35L) then AIKplbckgr=[f2(34)]
if (N2 GE 36L) then BIKplbckgr=[f2(35)]
if (N2 GE 37L) then eIKplbckgr=[f2(36)]
if (N2 GE 38L) then aKplLD1 = [f2(37)]
if (N2 GE 39L) then aKplLD2 = [f2(38)]
if (N2 GE 40L) then aKplLD3 = [f2(39)]
if (N2 GE 41L) then aKplLD4 = [f2(40)]
if (N2 GE 42L) then TeffSpctr=[f2(41)]
if (N2 GE 43L) then TRMS    = [f2(42)]
if (N2 GE 44L) then FKpl    = [f2(43)]
if (N2 GE 45L) then KplLDfact=[f2(44)]
if (N2 GE 46L) then Iwh1    = [f2(45)]
if (N2 GE 47L) then Expan   = [f2(46)]
if (N2 GE 48L) then width   = [f2(47)]
if (N2 GE 49L) then IRMS    = [f2(48)]
if (N2 GE 50L) then beta    = [f2(49)]
if (N2 GE 51L) then begin
                    PtPa    = [f2(50)]
                    PtPb    = [f2(51)]
                    PtPc    = [f2(52)]
                    PtPd    = [f2(53)]
                  if (vars(54+7) EQ 'PtPx') then begin
                    PtPx    = [f2(54)]& noPtx= 0L
                  endif else begin
                                        noPtx= 1L
                  endelse
               endif
if (N2 GE 56L-noPtx) then begin
                    dnu0    = [f2(55-noPtx)]
                    dnu1    = [f2(56-noPtx)]
                    dnu2    = [f2(57-noPtx)]
                    dnu3    = [f2(58-noPtx)]
                    dnu4    = [f2(59-noPtx)]
               endif
if (N2 GE 62L-noPtx) then begin
                    c0      = [f2(60-noPtx)]
                    cm1     = [f2(61-noPtx)]
                    c3      = [f2(62-noPtx)]
               endif
				simdirs = [smdir]
			endif else begin
				name	= [name,	name0]
				navn	= [navn,	strtrim(s2)]
				res		= [res,		strtrim(s3)]
				mix		= [mix,		strtrim(s4)]
;				if (s5 EQ '-') then f1(0)=-f1(0)
				abund	= [abund,	f1(0)]
				Tefobs	= [Tefobs,	f1(1)]
				grav	= [grav,	float(s6)]
				Lum		= [Lum,		f2(0)]
				Mass	= [Mass,	f2(1)]
				Rad		= [Rad,		f2(2)]
				alfa	= [alfa,	f2(3)]
				sigalfa = [sigalfa,	f2(4)]
				fvcnv	= [fvcnv,	f2(5)]
				sigfvc	= [sigfvc,	f2(6)]
				dCZ		= [dCZ,		f2(7)]
				ARMS    = [ARMS,    f2(8)]
				A1      = [A1,      f2(9)]
				P1      = [P1,     f2(10)]
				A2      = [A2,     f2(11)]
				P2      = [P2,     f2(12)]
				Agran   = [Agran,  f2(13)]
				I1      = [I1,     f2(14)]
				I2      = [I2,     f2(15)]
				I3      = [I3,     f2(16)]
				I4      = [I4,     f2(17)]
				I5      = [I5,     f2(18)]
				I6      = [I6,     f2(19)]
				Smin    = [Smin,   f2(20)]
				Smax    = [Smax,   f2(21)]
				Ptmax   = [Ptmax,  f2(22)]
if (N2 GE 24L) then Ptotph  = [Ptotph, f2(23)]
if (N2 GE 25L) then AIbckgr = [AIbckgr,f2(24)]
if (N2 GE 26L) then BIbckgr = [BIbckgr,f2(25)]
if (N2 GE 27L) then eIbckgr = [eIbckgr,f2(26)]
if (N2 GE 28L) then aLD1    = [aLD1,   f2(27)]
if (N2 GE 29L) then aLD2    = [aLD2,   f2(28)]
if (N2 GE 30L) then aLD3    = [aLD3,   f2(29)]
if (N2 GE 31L) then aLD4    = [aLD4,   f2(30)]
if (N2 GE 32L) then depth   = [depth,  f2(31)]
if (N2 GE 33L) then LDfact  = [LDfact, f2(32)]
if (N2 GE 34L) then A_Hp    = [A_Hp,   f2(33)]
if (N2 GE 35L) then AIKplbckgr=[AIKplbckgr,f2(34)]
if (N2 GE 36L) then BIKplbckgr=[BIKplbckgr,f2(35)]
if (N2 GE 37L) then eIKplbckgr=[eIKplbckgr,f2(36)]
if (N2 GE 38L) then aKplLD1 = [aKplLD1,f2(37)]
if (N2 GE 39L) then aKplLD2 = [aKplLD2,f2(38)]
if (N2 GE 40L) then aKplLD3 = [aKplLD3,f2(39)]
if (N2 GE 41L) then aKplLD4 = [aKplLD4,f2(40)]
if (N2 GE 42L) then TeffSpctr=[TeffSpctr,f2(41)]
if (N2 GE 43L) then TRMS    = [TRMS,   f2(42)]
if (N2 GE 44L) then FKpl    = [FKpl,   f2(43)]
if (N2 GE 45L) then KplLDfact=[KplLDfact,f2(44)]
if (N2 GE 46L) then Iwh1    = [Iwh1,   f2(45)]
if (N2 GE 47L) then Expan   = [Expan,  f2(46)]
if (N2 GE 48L) then width   = [width,  f2(47)]
if (N2 GE 49L) then IRMS    = [IRMS,   f2(48)]
if (N2 GE 50L) then beta    = [beta,   f2(49)]
if (N2 GE 55L) then begin
                    PtPa    = [PtPa,   f2(50)]
                    PtPb    = [PtPb,   f2(51)]
                    PtPc    = [PtPc,   f2(52)]
                    PtPd    = [PtPd,   f2(53)]
if (noPtx EQ 0) then PtPx   = [PtPx,   f2(54)]
               endif
if (N2 GE 60L-noPtx) then begin
                    dnu0    = [dnu0,   f2(55-noPtx)]
                    dnu1    = [dnu1,   f2(56-noPtx)]
                    dnu2    = [dnu2,   f2(57-noPtx)]
                    dnu3    = [dnu3,   f2(58-noPtx)]
                    dnu4    = [dnu4,   f2(59-noPtx)]
               endif
if (N2 GE 63L-noPtx) then begin
                    c0      = [c0,     f2(60-noPtx)]
                    cm1     = [cm1,    f2(61-noPtx)]
                    c3      = [c3,     f2(62-noPtx)]
               endif
				simdirs = [simdirs,	smdir]
			endelse
			Nstars = Nstars + 1L
		endif
	endwhile
	free_lun, lun
	if (Nstars LE 0L) then begin
		print, " ERROR in read_startab: Found no simulations to include"
		if KEYWORD_SET(STOP) then stop
		return, [""]
	endif else begin
		if KEYWORD_SET(STOP) then stop
		return, simdirs
	endelse
end
