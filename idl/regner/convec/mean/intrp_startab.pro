;
;   Function to interpolate in the starval.tab file.
;
;   Arguments (defaults in [...]):
;    x:       Array of logTeff-values to interpolate to.
;    y:       Array of logg-values to interpolate to.
;    STOP:    Set this keyword to stop before returning, to make all
;             variables available to the user (for debugging).
;
;   Update history:
;   ---------------
;   27.09.2011 Coded/RT
;   06.02.2014 Changed to use simtriangulate for the triangulation, to ensure
;              that the triangles are aligned with the structures, instead
;              of across them. Added keyword RAW to use the old version./RT
;
;   Known bugs: None
;
function intrp_startab, x, y, xtr=xtr, resis=resis, mixis=mixis, $
                        abundis=abundis, starval=starval, RAW=RAW, STOP=STOP
@cstarval
    default, resis, '150x150x82'
    default, mixis, 'AG89Fe'
    default, abundis, 0.0
    default, starval, 'starval.tab'
;
; Open and read the simulation summary file
    simdirs = read_startab(resis=resis, /ALFA, mixis=mixis, abundis=abundis, $
                                                                file=starval)
;
; Perform triangulation
    LTeff = alog10(TeffSpctr)
    logg  = alog10(grav)
;   triangulate, LTeff*1e1, Logg, TR, B
    simtriangulate, TR, RAW=RAW

    Nx = N_ELEMENTS(x)
    xtr= intarr(Nx)
    Nv = N_ELEMENTS(strvltitl)
    jv0  = 4L
    Aout = fltarr(Nv-jv0, Nx)
    dLT = 0.02
    dLg = 0.02
    for i=0L, Nx-1L do begin
        xout = [-1,0,1]*dLT + x(i)
        yout = [-1,0,1]*dLg + y(i)
        for j=0L, Nv-jv0-1L do begin
            tmp = trigrid(LTeff, Logg, scope_varfetch(j+jv0,comm='starvaltab'),$
                                            TR,xout=xout,yout=yout,miss=-42.)
            Aout(j,i) = tmp(1,1)
        endfor
        if (abs(tmp(1,1)+42.0) LT 1e-4) then xtr(i)=1 else xtr(i)=0
    endfor
    return, Aout

    if KEYWORD_SET(STOP) then stop
end
