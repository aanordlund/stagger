function hrms,f,XVERTICAL=XVERTICAL,ARRAY=ARRAY
;
;  Horizontal RMS-average of 3-d scalar
;
	COMMON cvert, cxvert, cyvert
	s=size(f)
;
;  The input is a 2D vertical slice
;
	if (s(0) eq 2) then begin
	  h = reform(f(0,*))
	  for i=0L,s(2)-1L do h(i)=rms(f(*,i))
	  if KEYWORD_SET(ARRAY) then h=rebin(reform(h,s(1),1),s(1),s(2))
;
;  The input is a 3D cube
;
;  - with the x-axis being the vertical direction
	end else if (n_elements(XVERTICAL) ne 0 or KEYWORD_SET(cxvert)) then begin
	  h = f(*,0,0)
	  for i=0L,s(1)-1L do h(i)=rms(f(i,*,*))
	  if KEYWORD_SET(ARRAY) then h=rebin(reform(h,s(1),1,1),s(1),s(2),s(3))
;
;  - with the z-axis being the vertical direction
	end else begin
	  h = reform(f(0,0,*))
	  for i=0L,s(3)-1L do h(i)=rms(f(*,*,i))
	  if KEYWORD_SET(ARRAY) then h=rebin(reform(h,1,1,s(3)),s(1),s(2),s(3))
	end
;
	return,h
end
