;
;   Update history:
;   ---------------
;   09.10.1998 Coded/RT
;   02.09.2004 Introduced argument zz, for choosing the x-scale./RT
;   18.01.2006 Also include option for plotting RMS-fluctuations./RT
;   16.05.2016 Special arguments, cRMS and lRMS, for color and line-style of
;              RMS-plot. Merged the two loops of the RMS calculation. New
;              argument uz, for plotting separate averages in up-/down-flows./RT
;
pro minmaxplot, vol, zz, vmin=vmin, vmax=vmax, vmean=vmean, xrange=xrange, $
            yrange=yrange, uz=uz, OPLOT=OPLOT, RMS=RMS, cRMS=cRMS, lRMS=lRMS, $
            _EXTRA=extra_args, STOP=STOP
	common cdat,x,y,z,nx,ny,nz,nw,ntmax,date0,time0

	default, zz, z
	nzz = N_ELEMENTS(zz)
	default, xrange, [zz(0), zz(nzz-1)]
	vmin = fltarr(nzz) & vmax = fltarr(nzz)
	for i=0,nzz-1 do vmin(i) = min(vol(*,*,i))
	for i=0,nzz-1 do vmax(i) = max(vol(*,*,i))
	vmean = haver(vol)
	if KEYWORD_SET(OPLOT) then begin
		c0 = (!D.TABLE_SIZE-1.)*.5
		oplot, zz, vmean, col=c0;, /NOCLIP, _EXTRA=extra_args
	endif else begin
		c0 = !P.COLOR
		iz = where(zz GE xrange(0) AND zz LE xrange(1))
		default, yrange, [min(vmin(iz)), max(vmax(iz))]
		plot, zz, vmean, xr=xrange, yr=yrange, col=c0, _EXTRA=extra_args
	endelse
	oplot, zz, vmin, line=2, col=c0;, /NOCLIP
	oplot, zz, vmax, line=2, col=c0;, /NOCLIP

	if KEYWORD_SET(RMS) then begin
        default, cRMS, !p.color
        default, lRMS, 5L
		vrmsp = zz*0
		vrmsm = zz*0
		for i=0L, nzz-1L do begin
			pln = reform(vol(*,*,i))
			ip = where(pln GT vmean(i), COMPLEMENT=im)
			if (ip(0L) GE 0L) then vrmsp(i) = rms(pln(ip)-vmean(i))
			if (im(0L) GE 0L) then vrmsm(i) = rms(pln(im)-vmean(i))
;			vrmst(i)= rms(pln-vmean(i))
		endfor
;  Using vrms or vrmst is equivalent for symmetrical distributions.
		oplot, zz, vmean+vrmsp, line=lRMS, col=cRMS;, /NOCLIP
;		oplot, zz, vmean+vrmst, line=lRMS, col=cRMS;, /NOCLIP
		oplot, zz, vmean-vrmsm, line=lRMS, col=cRMS;, /NOCLIP
;		oplot, zz, vmean-vrmst, line=lRMS, col=cRMS;, /NOCLIP
	endif
    Nuz = N_ELEMENTS(uz)
    if (Nuz GT 0L) then begin
        if (Nuz EQ N_ELEMENTS(vol)) then begin
            ups = (uz LT 0.0)
            Nups= total(total(ups, 1L), 1L)
            v2mean = h2aver(vol, ups, Nups)
            oplot, zz, v2mean(*,0), li=2;, /NOCLIP
            oplot, zz, v2mean(*,1), li=1;, /NOCLIP
        endif else begin
            print, "ERROR: uz has ",Nuz," elements, whereas vol has ",$
                   N_ELEMENTS(vol)," elements. Can't compute up-/down-means."
        endelse
    endif

	if KEYWORD_SET(STOP) then stop
end
