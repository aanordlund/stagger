;
;	Program to calculate time series of lagrangian horisontal averages.
;
;   09.09.2014 Note that dlnT/dlnp is based on p_gas, not including p_turb.
;
;   Update history:
;   ---------------
;   15.10.1997 Coded/RT
;   09.02.2005 Use simlist() to find sav-files/RT
;   29.12.2005 New calling convention for ftau for consistency with rad.f.
;   26.03.2007 Include t00 as an argument/RT
;   08.05.2018 Implemented updates to handling of pseudo-Lagrangian coordinate
;              system from add_dpad.pro, changed nnt->jt, storing mtime/[s]
;              with mtime(0)=0, and updated all my IDL-code to adopt that
;              change. Hard-coded f/n fttau to use kapfile='kap.dat'./RT
;
;   Known bugs: Need to fix handling of NEW=0.
;               Need to change time/[hs]->time/[s]  in  sun/run/l2mean.dat.40-74
;
pro mflow, numbers, Np=Np, t00=t00, NEW=NEW, FAST=FAST, $
                            Gustafsson=Gustafsson, NOWRITE=NOWRITE, STOP=STOP
@imflow
	common coff,mpar,mav,ipack,incr,ioff,time,voff,cfile,irec
	common cdat,x,y,z,Nx,Ny,Nz,Nw,Ntmax,date0,time0
	common thermoquan,dlpdee,dlpdlr,P_rho,tt,dttdee,dttdlr
	
	default, numbers, '??'
	f = simlist(numbers, /NOSCRATCH)
	Nf = N_ELEMENTS(f)
	print, Nf, f
	default, t00, 0L
    t000 = t00
	dt = 1L

	file, f(0), /quiet
	default, Np, Ntmax
	Ntmaxx = Nf*Np
	if KEYWORD_SET(Gustafsson) then tabfile=stamnavn()+'.tab.gus' $
                               else tabfile=stamnavn()+'.tab'
    table, tabfile;, mz=82
	sstab, itabS
	SS0    = lookup(alog(10)*.5, 2.5, iv=itabS, status=status)
    if (status GT 0L) then begin
      print, "#################################################################"
      print, "ERROR: The point lnr=1.15129, ee=2.5 is not included in "+tabfile
      print, "       This is needed for the zero-point of entropy."
      print, "       Extend the table to include that point.  Bailing out!"
      print, "#################################################################"
      print, ""
      return
    endif
	adata  = fltarr(Nz, imax)
	mldata = fltarr(Nz, imax)
	Nxy    = float(Nx*Ny)
; Eulerian z-scale
	ze     = cdata(t00, 'z')
	grav   = cpar(t00, 'grav')

;     Derivation of the system of units
	ur=cpar(t00, 'uur') & ul=cpar(t00, 'uul') & ut=cpar(t00, 'uut')
	uu=ul/ut & ue=uu^2 & up=ue*ur & uf=ue*ur*uu & huf=.5*uf & uw=uu/ul
    uFm=uu*ur & ulnp=alog(ur*uu^2) & ulnr=alog(ur)

    if (NOT KEYWORD_SET(NOWRITE)) then begin
        if keyword_set(NEW) then begin
            openw, unit, 'l2mean.dat', /GET_LUN, /SWAP_IF_LITTLE_ENDIAN
            printf, unit, Nz, imax
            writeu, unit, ze
        endif else begin
            openw, unit, 'l2mean.dat', /GET_LUN, /SWAP_IF_LITTLE_ENDIAN, /APPEND
        endelse
        free_lun, unit
    endif

;	if keyword_set(NEW) then begin
;		rho = fltarr(Nz, Ntmaxx)
;		jt = -1L
;	endif else begin
;		rho = fltarr(Nz, Ntmaxx+1)
;		jt =  0L
;		rho(*, 0) = mdata(0,'RHO')/ur
;		close, 4
;	endelse
;
; Go through all files...
; This is a short loop to establish the pseudo Lagrangian coordinate-system,
; zl, and its velocity, vz.
    jt   = 0L
    print, " Establishing the pseudo Lagrangian coordinate-system"
    for n=0L, Nf-1L do begin
        file, f(n), /QUIET
;
; These are just the variables needed for the pseudo-Lagrangian scale.
        if (n EQ 0L) then begin
            rhom  = (cdatat('rhom'))(*,t00:*)*ur
            ppm   = (cdatat('ppm'))(*,t00:*)*up
            tid   = time(t00:*)
        endif else begin
            rhom  = [[rhom], [cdatat('rhom')*ur]]
            ppm   = [[ppm],  [cdatat('ppm')*up]]
            tid   = [tid,    time]
        endelse
        print, string(13b), f(n), ': ', Ntmax-t00, jt,form='(3A0,2I4,$)'
        jt  = jt + Ntmax-t00
;
; Only use non-zero t00 for the first save-file to average.
        t00 = 0L
; End of file-loop
    endfor
    Nttot = jt&  print, " Total number of snapshots: ", Nttot, N_ELEMENTS(tid)
    tid   = tid - tid(0L)
; Scale time to a managable magnitude, by a factor rtid.
    rtid  = float(Nttot-1L)/tid(Nttot-1L)
    sctid = tid*rtid
    if (jt GT 0L) then begin
; Old version - not sure what to do with NEW in this version...
;       if keyword_set(NEW) then begin
;           zl = reform(zl2(*,0L:jt))
;       endif else begin
;           zl = reform(zl2(*,1L:jt))
;       endelse
; Converting to the Pseudo Lagrangian coordinate system
        zlagr, rhom, ze, mcol, zl
        emlnp = alog(ppm)
; Eulerian mean logarithmic pressure, emlnp, and Lagrangian, lmlnp
        vlagr, ze, zl, emlnp,   lmlnp
    endif
; Compute velocity of the Lagrangian coordinate system
    vz = 0.*zl
    for i=0L, Nz-1L do vz(i,*)=derf(sctid,reform(zl(i,*)))*rtid;   [Mm/hs]
    help, rhom, ze, mcol, zl, emlnp,   lmlnp, vz, tid

	jt = -1L& t00=t000
  for n=0L, Nf-1L do begin
	file, f(n), /QUIET
	print, '+++++++++++', cfile
	for t=t00, Ntmax-1L, dt do begin
		jt     = jt + 1L
		print, '    ', t, jt
		ux     = fux(t) & uy = fuy(t) & uz = fuz(t)
		uh2    = ux*ux + uy*uy

		lnrho  = flnrho(t)
		rho    = exp(lnrho)
		ee     = fee(t)
; Interpolate vz to the current, and fixed, ze-scale (from the zL-scale)
        duz    = intrp1(lmlnp(*,jt), emlnp(*,jt), vz(*,jt), /EXTRA)
; Subtract the velocity of the pseudo Lagrangian coordinate system, duz, to
; make the vertical velocity with respect to that moving system, but still
; on the ze-scale - transformed to zl at the end of the loop using vlagr.pro.
        for i=0L, Nz-1L do uz(*,*,i) = uz(*,*,i) - duz(i)
		ups    = (uz LT 0.0)
        Nups   = total(total(ups, 1L), 1L)
        flrhou = rho*uz
        uz2    = uz*uz
		adata(*,iFMSS            )=  haver(flrhou)*uu*ur
		adata(*,[iFMSSup,iFMSSdn])= h2aver(flrhou, ups, Nups)*uu*ur

		adata(*,iUR            ) = - haver(uz)*uu
		adata(*,[iURup,iURdn]  ) = -h2aver(uz, ups, Nups)*uu
		adata(*,[iUR2up,iUR2dn]) =  h2aver(uz2, ups, Nups)*ue
		adata(*,iUR2           ) =   haver(uz2)*ue
        tmp                      =  (uh2+uz2)*flrhou
		adata(*,iFKIN          ) = - haver(tmp)*huf
		adata(*,[iFKINup,iFKINdn])=-h2aver(tmp, ups, Nups)*huf
		ww = vorticity(ux, uy, uz)
		adata(*,[iWup,iWdn]    ) =  h2aver(ww, ups, Nups)*uw
		adata(*,iW             ) =   haver(ww)*uw
		adata(*,[iUH2up,iUH2dn]) =  h2aver(uh2, ups, Nups)*ue
		adata(*,iUH2           ) =   haver(uh2)*ue
        uh2                      =    sqrt(uh2)
        adata(*,[iUHup,iUHdn]  ) =  h2aver(uh2, ups, Nups)*uu
        adata(*,iUH            ) =   haver(uh2)*uu
		tmp                      =  flrhou*fuz(t);  	Pturb
		adata(*,[iPTup,iPTdn]  ) =  h2aver(tmp>0.0, ups, Nups)*up
		adata(*,iPT            ) =   haver(tmp>0.0)*up
        if (min(adata(*,[iPTup,iPTdn,iPT])) LT 0.0) then $
                                            print, ' min(<p_turb>) < 0.0!!!'
;	    adata(*,iPT            ) = cdata(t,'ptm')*up
		tmp                      = tmp^2;	Pturb^2
		adata(*,[iPT2up,iPT2dn]) =  h2aver(tmp, ups, Nups)*up^2
		adata(*,iPT2           ) =   haver(tmp)*up^2
		adata(*,[iRHOup,iRHOdn]) =  h2aver(rho, ups, Nups)*ur
		adata(*,iRHO           ) = cdata(t,'rhom')*ur
		adata(*,[iRHO2up,iRHO2dn])= h2aver(rho^2, ups, Nups)*ur^2
		adata(*,iRHO2          ) =   haver(rho^2)*ur^2
		mlnrup = alog(reform(adata(*,iRHOup))/ur)
		mlnrdn = alog(reform(adata(*,iRHOdn))/ur)
		mlnr = alog(cdata(t,'rhom'))
		adata(*,[iEup,iEdn]    ) =  h2aver(ee, ups, Nups)*ue
		adata(*,iE             ) = cdata(t,'eem')*ue
		adata(*,[iE2up,iE2dn]  ) =  h2aver(ee^2, ups, Nups)*ue^2
		adata(*,iE2            ) =   haver(ee^2)*ue^2
		meeup = reform(adata(*,iEup))/ue
		meedn = reform(adata(*,iEdn))/ue
		mee = cdata(t,'eem')
		if (0) then begin
			tmp = ftt(t)
			mttup=fltarr(Nz) & mttdn=mttup
			mtttmp = h2aver(tmp, ups, Nups)
			mttup=mtttmp(*,0) & mttdn=mtttmp(*,1)
			mtt = haver(tmp)
			tmake,mttup,mlnrup,meeup
			tmake,mttdn,mlnrdn,meedn
			tmake,mtt,mlnr,mee
		endif
		adata(*,iMPGup         ) = exp(pgam(mlnrup, meeup, gam1=mgm1, $
										gam3=mgm3, cs=mcs))*up
		adata(*,iMGAM1up       ) = mgm1
		adata(*,iMDADup        ) = (mgm3 - 1.0)/mgm1
		adata(*,iMCSup		   ) = mcs*uu
		adata(*,iMCPup         ) = fcp(mlnrup, meeup)*ue
		adata(*,iMCVup         ) = fcv(mlnrup, meeup)*ue
		adata(*,iMTup          ) = tt
		hab                      = .5*adata(0,iMPGup)*ur/(grav*adata(0,iRHO)*up)
		adata(*,iMTAUup        ) = ftau(mlnrup, tt, hab=hab, kapfile='kap.dat')
		adata(*,iMTAU5up       ) = ftau(mlnrup, tt, hab=hab, kapfile='kap.dat', /AA5000)
		adata(*,iMPGdn         ) = exp(pgam(mlnrdn, meedn, gam1=mgm1, $
										gam3=mgm3, cs=mcs))*up
		adata(*,iMGAM1dn       ) = mgm1
		adata(*,iMDADdn        ) = (mgm3 - 1.0)/mgm1
		adata(*,iMCSdn		   ) = mcs*uu
		adata(*,iMCPdn         ) = fcp(mlnrdn, meedn)*ue
		adata(*,iMCVdn         ) = fcv(mlnrdn, meedn)*ue
		adata(*,iMTdn          ) = tt
		hab                      = .5*adata(0,iMPGdn)*ur/(grav*adata(0,iRHO)*up)
		adata(*,iMTAUdn        ) = ftau(mlnrdn, tt, hab=hab, kapfile='kap.dat')
		adata(*,iMTAU5dn       ) = ftau(mlnrdn, tt, hab=hab, kapfile='kap.dat', /AA5000)
		adata(*,iMPG           ) = exp(pgam(mlnr, mee, gam1=mgm1, $
										gam3=mgm3, cs=mcs))*up
		adata(*,iMGAM1         ) = mgm1
		adata(*,iMDAD          ) = (mgm3 - 1.0)/mgm1
		adata(*,iMCS           ) = mcs*uu
		adata(*,iMCP           ) = fcp(mlnr, mee)*ue
		adata(*,iMCV           ) = fcv(mlnr, mee)*ue
		adata(*,iMT            ) = tt
		hab                      = .5*adata(0,iMPG)*ur/(grav*adata(0,iRHO)*up)
		adata(*,iMTAU          ) = ftau(mlnr, tt, hab=hab, kapfile='kap.dat')
		adata(*,iMTAU5         ) = ftau(mlnr, tt, hab=hab, kapfile='kap.dat', /AA5000)

		adata(*,iUPS           ) = Nups/Nxy
		adata(*,iFRAD          ) = -cdata(t,'frad')*uf
		adata(*,iFACUST        ) = -cdata(t,'fpu')*uf
		if (NOT keyword_set(FAST)) then begin
			lnP = pgam(lnrho,ee,gam1=g1,gam3=g3,cs=cs)
            tmp = (ee+exp(lnP-lnrho))*flrhou
            adata(*,iFENT          ) = - haver(tmp)*uf
			adata(*,[iFENTup,iFENTdn])=-h2aver(tmp, ups, Nups)*uf
			tmp                      = exp(lnP);	Pgas
			adata(*,[iPGup,iPGdn]  ) =  h2aver(tmp, ups, Nups)*up
			adata(*,iPG            ) = cdata(t,'ppm')*up
			adata(*,iPG2           ) =   haver(tmp)*up^2
			adata(*,[iGAM1up,iGAM1dn])= h2aver(g1, ups, Nups)
			adata(*,iGAM1          ) =   haver(g1)
			adata(*,[iPGAMup,iPGAMdn]) =h2aver(g1*tmp, ups, Nups)*up
			adata(*,iPGAMup)         = adata(*,iPGAMup) $
											/(adata(*,iPGup)+adata(*,iPTup))
			adata(*,iPGAMdn)         = adata(*,iPGAMdn) $
											/(adata(*,iPGdn)+adata(*,iPTdn))
			adata(*,iPGAM)           =   haver(g1*tmp)*up $
											/(adata(*,iPG)+adata(*,iPT))
			tmp                      = tmp^2;		Pgas^2
			adata(*,[iPG2up,iPG2dn]) =  h2aver(tmp, ups, Nups)*up^2
			tmp = (g3 - 1.0)/g1
			adata(*,[iDADup,iDADdn]) =  h2aver(tmp, ups, Nups)
			adata(*,iDAD           ) =   haver(tmp)
			adata(*,[iCSup,iCSdn]  ) =  h2aver(cs, ups, Nups)*uu
			adata(*,iCS            ) =   haver(cs)*uu
			cp = fcp(lnrho,ee,it=t)
			adata(*,[iCPup,iCPdn]  ) =  h2aver(cp, ups, Nups)*ue
			adata(*,iCP            ) =   haver(cp)*ue
			tmp = fcv(lnrho,ee)
			adata(*,[iCVup,iCVdn]  ) =  h2aver(tmp, ups, Nups)*ue
			adata(*,iCV            ) =   haver(tmp)*ue
			adata(*,[iTup,iTdn]    ) =  h2aver(TT, ups, Nups)
			adata(*,iT             ) = cdata(t,'ttm')
			tmp = TT^2												; T^2
			adata(*,[iT2up,iT2dn]  ) =  h2aver(tmp, ups, Nups)
			adata(*,iT2            ) =   haver(tmp)
			tmp = tmp^2												; T^4
			adata(*,[iT4up,iT4dn]  ) =  h2aver(tmp, ups, Nups)
			adata(*,iT4            ) =   haver(tmp)
			hab                      =.5*adata(0,iPG)*ur/(grav*adata(0,iRHO)*up)
			tmp                      = ftau(lnrho, TT, hab=hab, kapfile='kap.dat')
			adata(*,[iTAUup,iTAUdn]) =  h2aver(tmp, ups, Nups)
			adata(*,iTAU           ) =   haver(tmp)
			tmp                      = ftau(lnrho, TT, hab=hab, kapfile='kap.dat', /AA5000)
			adata(*,[iTAU5up,iTAU5dn])= h2aver(tmp, ups, Nups)
			adata(*,iTAU5          ) =   haver(tmp)
			tmp = lookup(lnrho, ee, iv=itabS, status=status)-SS0    ; Entropy
            if (status GT 0L) then begin
              ii = where(ee GT 14.5 AND lnrho LT -8.)
              if (ii(0) GE 0L) then begin
                tmp(ii) = ((exp(lnP(ii))*$  ; from t41g15m+0014(10).sav.
                    (1.5*alog(TT(ii))+2.5-lnrho(ii)-2.9851438))/TT(ii)/rho(ii) $
                        - 0.070728178)/6.8237777 - SS0
                print, "WARNING: Perfect gas extrapolation of ", $
                   N_ELEMENTS(ii)," points outside "+tabfile+" for it=",$
                   t, " in "+f(n), form='(A0,I0,A0,I0,A0)'

              endif else begin
                print, "WARNING: Extrapolation in mflow - but not at top!"
                print, "         for it=",t, " in "+f(n), form='(A0,I0,A0)'
              endelse
            endif
			adata(*,[iSup,iSdn]    ) =  h2aver(tmp, ups, Nups)*ue
			adata(*,iS             ) =   haver(tmp)*ue
			adata(*,[iS2up,iS2dn]  ) =  h2aver(tmp^2, ups, Nups)*ue^2
			adata(*,iS2            ) =   haver(tmp^2)*ue^2
			lnPz = zder(lnP)										; dlnP_g/dz
			tmp = zder(tmp)
			adata(*,[iDSSup,iDSSdn]) =  h2aver(tmp, ups, Nups)/ $
					h2aver(cp*lnPz, ups, Nups)
			adata(*,iDSS           ) =   haver(tmp)/haver(cp*lnPz)
			tmp = zder(alog(TT))
			adata(*,[iDSup,iDSdn]  ) =  h2aver(tmp, ups, Nups)/ $
					h2aver(lnPz, ups, Nups) - adata(*,[iDADup,iDADdn])
			adata(*,iDS            ) =   haver(tmp)/haver(lnPz) - adata(*,iDAD)
		endif
		adata(*,iMSup          ) = (lookup(mlnrup, meeup, iv=itabS)-SS0)*ue
		adata(*,iMSdn          ) = (lookup(mlnrdn, meedn, iv=itabS)-SS0)*ue
		adata(*,iMS            ) = (lookup(mlnr, mee, iv=itabS)-SS0)*ue
		mlnP = alog(reform(adata(*,iPGup)))
		adata(*,iMDSup ) = derf(mlnP, alog(adata(*,iMTup)))-adata(*,iMDADup)
		adata(*,iMDSSup) = derf(mlnP, adata(*,iMSup))/adata(*,iMCPup)
		mlnP = alog(reform(adata(*,iPGdn)))
		adata(*,iMDSdn ) = derf(mlnP, alog(adata(*,iMTdn)))-adata(*,iMDADdn)
		adata(*,iMDSSdn) = derf(mlnP, adata(*,iMSdn))/adata(*,iMCPdn)
		mlnP = alog(reform(adata(*,iPG)))
		adata(*,iMDS   ) = derf(mlnP, alog(adata(*,iMT)))-adata(*,iMDAD)
		adata(*,iMDSS  ) = derf(mlnP, adata(*,iMS))/adata(*,iMCP)

		if (N_ELEMENTS(zl) GT 0) then begin
;           reload_marray, /QUIET
; Loop over all variables (including f_up) that will be averaged in the
; pseudo Lagrangian way in time.
			for i=0L, imax-1L do begin
              if (i NE iZL) then begin
                if ((where(iln EQ i))(0L) GE 0L  AND $
                                 min(adata(*, i)) GT 0.0) then begin
; Quantities that are safer to interpolate logarithmically
; Need to reform adata explicitly to  Nx*1 in order for vlagr to get correct Nt
                    vlagr, ze, zl(*,jt), alog(reform(adata(*, i), Nz, 1L)), tmp
                    mldata(*, i) = exp(tmp)
                endif else begin
                    vlagr, ze, zl(*,jt),      reform(adata(*, i), Nz, 1L),  tmp
                    mldata(*, i) = tmp
                endelse
              endif
			endfor
            mldata(*, iZL) = zl(*,jt)
		endif else begin
			mldata = adata
		endelse
; Write averages of each snap-shot
      if (NOT KEYWORD_SET(NOWRITE)) then begin
		openw,    unit, 'l2mean.dat', /GET_LUN, /SWAP_IF_LITTLE_ENDIAN, /APPEND
		writeu,   unit, mldata
		free_lun, unit
      endif
	endfor
;
; Only use non-zero t00 for the first save-file to average.
	t00 = 0L
  endfor
  tid = tid*ut; /[s]
; Write the time-array at the end
  if (NOT KEYWORD_SET(NOWRITE)) then begin
      openw,    unit, 'l2mean.dat', /GET_LUN, /SWAP_IF_LITTLE_ENDIAN, /APPEND
      writeu,   unit, tid
      free_lun, unit
  endif
  close, 1
  close, 2
    
  if KEYWORD_SET(STOP) then stop
end
