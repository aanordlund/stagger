;                       0    1   2   3     4      5    6   7    8   9   10
	common starvaltab,name,navn,res,mix,abund,Tefobs,grav,Lum,Mass,Rad,alfa,$
;           11    12     13  14   15  16 17 18 19   20  21 22 23 24 25 26  27
		sigalfa,fvcnv,sigfvc,dCZ,ARMS,A1,P1,A2,P2,Agran,I1,I2,I3,I4,I5,I6,Smin,$
;        28    29     30      31      32       33   34   35   36   37    38
		Smax,Ptmax,Ptotph,AIbckgr,BIbckgr,eIbckgr,aLD1,aLD2,aLD3,aLD4,depth,$
;          39   40         41         42         43      44      45      46
        LDfact,A_Hp,AIKplbckgr,BIKplbckgr,eIKplbckgr,aKplLD1,aKplLD2,aKplLD3,$
;           47        48   49   50     51      52   53    54    55   56
        aKplLD4,TeffSpctr,TRMS,FKpl,KplLDfact,Iwh1,Expan,width,IRMS,beta,$
;        57   58   59   60   61   62   63   64   65   66  67 68  69
        PtPa,PtPb,PtPc,PtPd,PtPx,dnu0,dnu1,dnu2,dnu3,dnu4,c0,cm1,c3
;
; Style of parenthesis around units
    if KEYWORD_SET(MNRAS) then  pb='!x('   else   pb='!5/[!x'
    if KEYWORD_SET(MNRAS) then  pe='!x)'   else   pe='!5]!x'
;   pb = '!5/[!x' & pe='!5]!x';     My default
;   pb = '!x('    & pe='!x)';       MNRAS style
    uS = pb+'10!u8!nerg!i !ng!e-1!nK!e-1!n'+pe
	strvltitl=['', '', '', '', '!5[!xFe/H'+pe, '!8T!x!deff!n'+pb+'K'+pe, $
		'!8g'+pb+'cm!i !ns!u-2!n'+pe, '!8L!5/!8L!d!9n!x!n', $
		'!8M!5/!8M!d!9n!x!n', '!8R!5/!8R!d!9n!x!n', '!7a!x', '!7r!da!n!x', $
		'!7b!x','!7r!db!n!x', '!8d!x!dCZ!n', '!8A!x!dRMS!n', '!8A!x!d1!n', $
		'!8P!x!d1!n', '!8A!x!d2!n', '!8P!x!d2!n', '!8A!x!dgran!n','I!d1!n', $
		'I!d2!n', 'I!d3!n', 'I!d4!n', 'I!d5!n', 'I!d6!n', '!8S!x!dmin!n'+uS, $
		'!8S!x!dmax!n'+uS, '!8P!x!s!dmax!r!uturb!n', '!8P!x!s!dph!r!utot!n' ,$
		'!8A!s!dI!r!u!xgran!n', '!8B!s!dI!r!u!xgran!n!5/[!xMm!5]!x', '!8e!s!dI!r!u!xgran!n',$
		'!8a!d!x1!n','!8a!d!x2!n','!8a!d!x3!n','!8a!d!x4!n','!8d'+pb+'Mm'+pe,$
        '!8f!7!Dh!x!n-1','!8A!x!dgran!n/!8H!dP!n!x', $
        '!8A!s!dIKpl!r!u!xgran!n', '!8B!s!dIKpl!r!u!xgran!n', $
        '!8e!s!dIKpl!r!u!xgran!n', $
        '!8a!d!xKpl,1!n','!8a!d!xKpl,2!n','!8a!d!xKpl,3!n','!8a!d!xKpl,4!n',$
        '!8T!x!deff!n'+pb+'K'+pe,'!7d!xRMS(!8T!x!deff!n'+pb+'K'+pe+')', $
        '!8F!x!dKpl!n','!8f!7!Dh!x,Kpl!n-1','!8I!x!dwh!n(!7h!x=1)', $
        '!7K'+pb+'Mm'+pe,'!8w'+pb+'Mm'+pe,'!8I!x!dRMS!n', $
        '(!9D!xln!8T!x!deff!n/!9D!xln!8g!x)!dad!n','a(PtP)','b(PtP)','c(PtP)',$
        'd(PtP)','x(PtP)','a0(dnu)','a1(dnu)','a2(dnu)','a3(dnu)','a4(dnu)',$
        '!8c!x!d0!n','!8c!x!d-1!n!5/[!7l!xHz!5]!x', $
        '!8c!x!d3!n!5/[!7l!xHz!5]!x']
;                0   1   2   3   4      5    6  7-9     10-13
    strvlform="(A11,A20,A12,A8,A1,F4.2,F7.1,A8,3F7.4,2(F9.6,e11.4)," $
;               14    15-32    33   34-39  40    41-42  43    44-47   48 
             +"F12.9,18e11.4,f11.7,6e11.4,f11.7,2e11.4,f11.7,4e11.4,f9.2," $
;               49    50-69
             +"f9.4,20e11.4)"
