function haver,f,XVERTICAL=XVERTICAL,YVERTICAL=YVERTICAL,array=array,cm=cm
;
;  Horizontal average of 3-d scalar
;
	COMMON cvert, cxvert, cyvert
	s=size(f)
	if s(0) eq 2 then begin
	  h=total(f,2)/s(2)
	  if keyword_set(array) then $
	    h=rebin(reform(h,s(1),1),s(1),s(2))
	end else if (keyword_set(XVERTICAL)  OR  keyword_set(cxvert)) then begin
	  h=total(total(f,2),2)/(s(2)*s(3))
	  if keyword_set(array) then $
	    h=rebin(reform(h,s(1),1,1),s(1),s(2),s(3))
	end else if (keyword_set(YVERTICAL)  OR  keyword_set(cyvert)) then begin
	  h=total(total(f,3),1)/(s(1)*s(3))
	  if keyword_set(array) then $
	    h=rebin(reform(h,1,s(2),1),s(1),s(2),s(3))
	end else begin
	  h=total(total(f,1),1)/(s(1)*s(2))
	  if keyword_set(array) then $
	    h=rebin(reform(h,1,1,s(3)),s(1),s(2),s(3))
	end
	if keyword_set(cm) then h=h(4:n_elements(h)-5)
	return,h
end
