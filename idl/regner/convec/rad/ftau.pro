;
;   Function to calculate the Rosseland or the 5000\AA monochromatic
;   optical depth in the vertical direction, for the 1D or the 3D case.
;
;   Using /MCUBIC actually seems a bad idea for dtau - it makes it more
;   erratic and does not prevent negative dtaus!
;
;   Update history:
;   ---------------
;   ??.??.200? Coded/RT
;   23.07.2004 Now checks whether it should reopen the kap.dat-file.
;   23.07.2004 Now using dynamic lun-allocation for opening kap.dat.
;   23.07.2004 Include option for alternate z-scale.
;   15.04.2005 Return lnab instead of tau if z's dimension differ from mz.
;   23.12.2005 Calculation of tau(*,*,0) consistent with that in the hydro-code.
;   29.12.2005 Include hab in argument list.
;   01.06.2006 New option, MCUBIC, for monotonic cubic interpolation/RT
;   02.10.2006 Added Pturb in expression for hab/RT
;	03.10.2006 Changed aeps from 1e-5 to 1e-28 - caused big problems!/RT
;   01.02.2007 Keyword /SIMTAB allows for use of lnab from [sim].tmp/RT
;   21.03.2007 Fixed bugs related to zm != z /RT
;   14.01.2008 Added kapfile to argument list/RT
;   10.11.2008 If the program detects a 'kaprhoT.tab' table, it automatically
;              uses that and lookup_rad instead of the kap.dat file/RT
;   26.11.2008 Improved handling of 'kaprhoT.tab' tables - avoid reloading/RT
;   11.01.2009 Fixed handling of /AA5000 when using kaprhoT.tab tables/RT
;   22.08.2013 Added keyword QUIET/RT
;   11.02.2016 Fixed bug in 1D case - now divide by xmu./RT
;
function ftau, lnr, TT, xmu, lnab=lnab, zm=zm, hab=hab, grav=grav, ppm0=ppm0, $
		scrfile=scrfile, tmpfile=tmpfile, kapfile=kapfile, AA5000=AA5000, $
		LINEAR=LINEAR, MCUBIC=MCUBIC, SIMTAB=SIMTAB, QUIET=QUIET, NOTAB=NOTAB, $
        STOP=STOP
	common cdat,x,y,z,nx,ny,nz,nw,ntmax,date0,time0
	common opacs,kaparr,nrho,nT,kapfile0,dkT,dkr,LogT,Lnrh

	default, kapfile0, '.'
	if (file_test('kaprhoT.tab')) then  default, kapfile, 'kaprhoT.tab'  $
								  else  default, kapfile, 'kap.dat'
	default, xmu, 1.0
	default, tmpfile, stamnavn()+".tmp"
	if (strpos(tmpfile, '.tmp') LT 0) then tmpfile=tmpfile+'.tmp'
	aeps  = 1e-14
  if (NOT (KEYWORD_SET(NOTAB)  AND  N_ELEMENTS(lnab) GT 1L)) then begin
;
; Find, open and read the most intelligent choise for a relevant opacity table.
	if (strpos(kapfile, 'kaprhoT') GE 0L) then begin
		if (NOT file_same(kapfile0, kapfile)) then begin
			radtab, kapfile
			kapfile0 = file_expand_path(kapfile)
		endif
        if (max(TT) LT 1e2) then begin
            if (NOT KEYWORD_SET(QUIET)) then $
                print, " WARNING: Assuming the input TT is actually lnT + "+kapfile
            lnab = lookup_rad(lnr,      TT,  2*KEYWORD_SET(AA5000), -1)
        endif else begin
            if (NOT KEYWORD_SET(QUIET)) then $
                print, " WARNING: Assuming the input TT is indeed TT + "+kapfile
            lnab = lookup_rad(lnr, alog(TT), 2*KEYWORD_SET(AA5000), -1)
        endelse
	endif else begin
		if KEYWORD_SET(SIMTAB) then begin
			if ((fstat(2))(0).name NE tmpfile) then tabtmp, tmpfile
			print,'1absorption from '+tmpfile+'...'
			if KEYWORD_SET(AA5000) then begin
				print, "WARNING: The keyword combination; /AA5000 and /SIMTAB is"
				print, "         invalid. /SIMTAB takes precedence."
			endif
            if (max(TT) LT 1e2) then begin
                if (NOT KEYWORD_SET(QUIET)) then $
                    print, " WARNING: Assuming the input TT is actually lnT + "+tmpfile
                lnab = lookup_tmp(lnr, exp(TT), iv=1)
            endif else begin
                if (NOT KEYWORD_SET(QUIET)) then $
                    print, " WARNING: Assuming the input TT is indeed TT + "+tmpfile
                lnab = lookup_tmp(lnr,     TT,  iv=1)
            endelse
		endif else begin
			if (NOT file_same(kapfile0, kapfile)) then begin
				if ((findfile(kapfile))(0) NE '') then begin
					kapfile0 = file_expand_path(kapfile)
					print, "  Loading "+kapfile0+"..."
					nrho = 20
					nT   = 70
					kaparr = dblarr(2, nT, nrho)
					openr, lun, kapfile, /GET_LUN, /SWAP_IF_LITTLE_ENDIAN
					readu, lun, kaparr
					free_lun, lun
					dkr = 0*kaparr
					dkT = 0*kaparr
					iir = findgen(nrho)
					iiT = findgen(nT)
					for iT=0L,nT-1L do begin
						dkr(0,iT,*) = zdermz1(iir, reform(kaparr(0,iT,*)))
						dkr(1,iT,*) = zdermz1(iir, reform(kaparr(1,iT,*)))
					endfor
					for ir=0L,nrho-1L do begin
						dkT(0,*,ir) = zdermz1(iiT, reform(kaparr(0,*,ir)))
						dkT(1,*,ir) = zdermz1(iiT, reform(kaparr(1,*,ir)))
					endfor
					LogT = findgen(nT)/(nT-1.0)*1.4+3.2
					Lnrh =(findgen(nrho)/(nrho-1.0)*11.-14. + 7.)*alog(1e1)
				endif else begin
					print, "  Couldn't find the file kap.dat."
					print, "  Bailing out!"
					stop
				endelse
			endif
			print,'2absorption from '+file_basename(kapfile0)+'...'
			if KEYWORD_SET(AA5000) then ivk=1   else ivk=0
			lnab =(bicubic(alog10(Tt),lnr,reform(kaparr(ivk,*,*)),LogT,Lnrh, $
						reform(dkT(ivk,*,*)),reform(dkr(ivk,*,*)))+1.)*alog(1e1)+lnr
		endelse
	endelse
  endif
;
; MCUBIC overrides LINEAR
	if (KEYWORD_SET(LINEAR) AND NOT KEYWORD_SET(MCUBIC)) then i12 = 0.0 $
														 else i12 = 1./12.
	ab=exp(lnab)

	s=size(lnr)
	default, zm, z
;
;  If zm has different dimension than rho, just return, lnab
    if (s(s(0)) NE N_ELEMENTS(zm)) then begin
        print, ' Just returning lnab, since ',s(s(0)),' != ',N_ELEMENTS(zm)
        return, lnab
    endif
	itype = s(0)+1
;
; Try to come up with an intelligent choise for hab, the scale-height
; for absorption.
	if (N_ELEMENTS(hab) LE 0) then begin
		if (N_ELEMENTS(grav) LE 0) then begin
			defaultscr, scrfile=scrfile
			grav = cpar(ntmax-1,'grav')
			ppm0 = exp(intrp1(z, zm(0), $
				alog(cdata(ntmax-1,'ppm')+(cdata(ntmax-1,'ptm')>0.0)), /EXTRA))
		endif
		if (N_ELEMENTS(ppm0) LE 0) then begin
			if ((fstat(2))(0).name NE tmpfile) then tabtmp, tmpfile
			ppm0 = aver(exp(lookup_tmp(lnr(*,*,0),TT(*,*,0))))
		endif
		hab = 0.5*ppm0/(grav*aver(exp(lnr(*,*,0))))
	endif
  if (s(0) EQ 3) then begin
	mx=s(1) & my=s(2) & mz=s(3) & nw=mx*my

	if (NOT KEYWORD_SET(QUIET)) then print,'dlnab ...'
;
; #############    NB: dlnab here, vs. dlnab/dzr  in rad.f
; derf(findgen(mz), lnab)  =  dz*derf(z, lnab)   - but cheaper
; This accounts for the difference between this and rad.f
	z0=z & z=findgen(mz) & nz0=nz & nz=mz
	if KEYWORD_SET(MCUBIC) then dlnab=zderm(lnab)   else dlnab=zder(lnab)
	z = z0 & nz=nz0
	if (NOT KEYWORD_SET(QUIET)) then print,'3D tau ...'
	tau1=make_array(mx,my,mz,type=s(itype))
	tau1(*,*,0)=ab(*,*,0)*hab/xmu
    if12 = reform(ab(*,*,0))*0
	for n=1,mz-1 do begin
	  dz=zm(n)-zm(n-1)
      ii = where(dlnab(*,*,n-1) GT (-4.)  AND  dlnab(*,*,n) LT 4.,COMPLEMENT=i0)
      if (ii(0) GE 0L) then if12(ii) = i12
      if (i0(0) GE 0L) then if12(i0) = 0.0
	  tau1(*,*,n)=tau1(*,*,n-1)+dz/xmu* $
	    ((ab(*,*,n-1)*(0.5+if12*dlnab(*,*,n-1))) + $
	     (ab(*,*,n  )*(0.5-if12*dlnab(*,*,n  ))))
	endfor
  endif
  if (s(0) EQ 1) then begin
	mz=s(1)

	if (NOT KEYWORD_SET(QUIET)) then print,'dlnab ...'
	z0=z & z=findgen(mz) & nz0=nz & nz=mz
;
;  NB: zderm1 doesn't exist yet! - it does now :-)
	if KEYWORD_SET(MCUBIC) then dlnab=zderm1(lnab)   else dlnab=zder1(lnab)
	z = z0 & nz=nz0
	if (NOT KEYWORD_SET(QUIET)) then print,'1D tau ...'
	tau1=make_array(mz,type=s(itype))
	tau1(0)=ab(0)*hab/xmu
	for n=1,mz-1 do begin
	  dz=zm(n)-zm(n-1)
      if12   = i12*float(dlnab(n-1) GT (-4.)  AND  dlnab(n) LT 4.)
	  tau1(n)=tau1(n-1)+dz/xmu* $
	    (ab(n-1)*(0.5+if12*dlnab(n-1)) + ab(n  )*(0.5-if12*dlnab(n  )))
	endfor
  endif
  if (s(0) EQ 2) then begin
	print, " Not prepared to handle a 2D case... Bailing out!"
  endif

  if KEYWORD_SET(STOP) then stop
  return,tau1
end
