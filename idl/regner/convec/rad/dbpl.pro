;
;   Function to compute the Rosseland weighting function, dBdT.
;   Adapted from subs.f, rev.72+.
;                                  .
;    dB_\lambda(T)/dT for \lambda/[A]
;
;   where  B_\lambda(T)  is computed with  bpl(xl, T).
;
;   Arguments (defaults in [...]):
;    STOP:    Set this keyword to stop before returning, to make all
;             variables available to the user (for debugging).
;
;   Update history:
;   ---------------
;   27.07.2010 Coded/RT
;
;   Known bugs: None
;
function dbpl, xl, TT, STOP=STOP
;        2*h*c^2*1d8^4
;   cp = 1.1910439d+27
;        (cp/c2)^.25 = 1d6*(2*k*c)^.25
    c1 = 5.3639363d+4
;        h*c/k*1d8
    c2 = 1.4387687d+8

    x2=(c1/xl)^2
    alf = c2/(TT*xl)
    ex  = exp(-alf)

    if KEYWORD_SET(STOP) then stop

    return, (x2*alf)^2*ex/(1d0-ex)^2
end
