;
;   Program to load and plot the cbin.F77.dat files produced by
;   radtab.x when computing tables of binned opacities.
;     The program also enables interactive manipulation of the bin
;   specifications, and automatic saving of the result to 'infile'.
;     The program is employed by cbin_opt.pro, for semi-automated
;   optimization of the bin-specifications.
;
;   Arguments (defaults in [...]):
;    ext:     A possible extension to the file-name (below) [''].
;    file:    Full name of the data file ['cbin.F77.dat'].
;    infile:  Full name of the input-file to radtab.x ['radtab.in.bin'].
;    xbins1:  Returns the new bin specifications.
;             If supplied as an argument, simply save xbins1 to 'infile'.
;    xbins0:  Returns the old bin specifications.
;    xlb:     Returns the wavelength set.
;    NOBACKUP:Don't back-up the 'infile' when saving the new bin specifications.
;    NOSAVE:  Don't save the new bin specifications.
;    DOSAVE:  Don't ask whether to save the new bin specifications - just do it.
;    QUIET:   Set this keyword to avoid diagnostic output - errors/warnings
;             are still printed.
;    ONLYPLOT:Set this keyword to just plot and not wait for changes to bins.
;    _EXTRA:  Extra keywords passed to the plot command.
;    STOP:    Set this keyword to stop before returning, to make all
;             variables available to the user (for debugging).
;
;   Update history:
;   ---------------
;   01.09.2008 Coded/RT
;   02.09.2008 Added interactive interface for changing the bin specs./RT
;   03.09.2008 Added keywords; NOBACKUP, NOSAVE, DOSAVE, QUIET and arguments;
;              xbins1 and xbins0 to fascilitate automatization with cbin_opt/RT
;   10.09.2008 Now works for more general bin arrangements. The program first
;              goes through all unique wavelength-limits and changes it for
;              all bins sharing that limit, then the same is done for all the
;              unique logx-limits/RT
;   26.06.2014 The program now returns after the first changed bin-limit./RT
;   04.02.2016 Added keyword ONLYPLOT and _EXTRA./RT
;
;   Known bugs: None
;
pro cbin_plot, ext, file=file, infile=infile, xbins1=xbins1, xbins0=xbins0, $
		xlb=xlb, NOBACKUP=NOBACKUP, NOSAVE=NOSAVE, DOSAVE=DOSAVE, QUIET=QUIET, $
		ONLYPLOT=ONLYPLOT, _EXTRA=extra_args, STOP=STOP
	default, file, 'cbin.F77.dat'
	default, ext, ''
	default, infile, 'radtab.in.bin'
	loadct, 12

	if (ext NE ''  AND  strmid(ext,0,1) NE '.') then ext='.'+ext
	file = file + ext

	openr, lun, file, /F77, /GET_LUN, /SWAP_IF_LITTLE_ENDIAN
	nwl=0L& nlb=0L
	readu,lun,nwl,nlb
	xlb=fltarr(nwl)& alogx=xlb& ibin=lonarr(nwl)
	readu, lun, xlb, alogx, ibin
	free_lun, lun
	xlb = xlb(0:nlb-1)& alogx=alogx(0:nlb-1)& ibin=ibin(0:nlb-1)-1L
; Find the bins that have been populated
    iibn  = ibin(uniq(ibin, sort(ibin)))
	Nbins = N_ELEMENTS(iibn)
; Read the radtab.in-file with bin-limits
	spawn, 'cat '+infile, stra
	idxbin = where(strpos(stra, "x1, xx, x2, lmbd1, lmbd2") GT 5L)
	sbins  = stra(idxbin)
    Nxbins = N_ELEMENTS(sbins)
	if (Nxbins NE Nbins) then begin
		print, Nbins, infile, N_ELEMENTS(sbins), form=$
			'("ERROR: #of populated bin=Nbins=",I0," != #entries in ",A0," =",I0,".")'
	endif
	xbin = fltarr(5L)&  xbins=fltarr(5L, Nxbins)
	for i=0L,Nxbins-1L do begin
		reads, sbins(i), xbin, form='(5(f,","),A0)'
		xbins(*,i) = xbin
	endfor
	xbins0 = xbins
	if (N_ELEMENTS(xbins1) EQ N_ELEMENTS(xbins)) then goto, SaveIt
;
; Indexes of the various starting wavelengths that are not end-points
	ixl = uniq(xbins(3,*), sort(xbins(3,*)))
	ixl = ixl(where(xbins(3,ixl) GT (xlb(0)-10.)>0))
	xls = xbins(3,ixl)
	Nxls= N_ELEMENTS(xls)
;
; Indexes of the various lower logx that are not end-points
	ixx = uniq(xbins(0,*), sort(xbins(0,*)))
	ixx = ixx(where(xbins(0,ixx) GT min(alogx)))
	xxs = xbins(0,ixx)
	Nxxs= N_ELEMENTS(xxs)
;
; Linestyle for plotting bin-borders that aren't currently getting changed
	lsty = 1
;
; Plot the bin-descriminator, logx, against wavelength
	plot, alog10(xlb), alogx, /NODATA, ysty=1, $
		xtitl='log!d10!i !n(!7k!3/['+string(197b)+']!x)', $
		ytitl='log!d10!i !n!7s!x!dRoss!n(!7s!dk!x!n=1)', _EXTRA=extra_args
	plots, alog10(xlb), alogx, psym=2, syms=.3,col=ibin*(!D.TABLE_SIZE-1.)/Nbins
	device, get_graphics_function=oldg, set_graphics_function=10
	ilh = [4,3] &  ixh = [2,2]; indices for top-edge
	ilv = [4,4] &  ixv = [0,2]; indices for right-edge
	for i=0L, Nxbins-1 do begin
		oplot,alog10(xbins(ilh,i)>1), xbins(ixh,i),line=lsty
		oplot,alog10(xbins(ilv,i)>1), xbins(ixv,i),line=lsty
	endfor
    if KEYWORD_SET(ONLYPLOT) then begin
        device, set_graphics_function=oldg
        if KEYWORD_SET(STOP) then stop
        return
    endif
; Counter of wavelength regions.
;
; Changes of log10(wavelength) and alogx in steps of dll and dxx, respectively,
; which are set to two pixels in the plot.
	dll = 2.*(!x.crange(1) - !x.crange(0))/$
			(!D.x_vsize-total(!x.margin, /PRESERVE)*!D.x_ch_size*!P.charsize)
	dxx = 2.*(!y.crange(1) - !y.crange(0))/$
			(!D.y_vsize-total(!y.margin, /PRESERVE)*!D.y_ch_size*!P.charsize)
	print,"Change the values with the arrow-keys, q:quit, s:stop, other:advance"
	print, " lower-limit  value  upper-limit"
    ALTERED_STATE = 0b
	for j=0L, Nxls-1L do begin
; The bins for which xls(j) is the upper boundary
		i4 = where(abs(xbins(4,*) - xls(j))/xls(j) LT 1e-5)
; The bins for which xls(j) is the lower boundary
		i3 = where(abs(xbins(3,*) - xls(j))/xls(j) LT 1e-5)
; The number of bins of each kind - at least 1 in both cases
		N4 = N_ELEMENTS(i4)  &   N3 = N_ELEMENTS(i3)
; The combination of the two = all bins affected by this wavelength change
		ib = [i4, i3]
		Nib = N_ELEMENTS(ib)
; An Nib long array of 4s and 3s, adressing the upper-/lower-boundaries as appr.
		k = [replicate(4L, N4), replicate(3L,   N3)]
; The corresponding line-styles for plotting
		l = [replicate(0L, N4), replicate(lsty, N3)]
;
; Limits of the possible values of the wavelength we are changing:
; the max of the lower limit for the bins for which we are changing the upper
		xl0 = max(xbins(3,i4)) > xlb(0)
; the min of the upper limit for the bins for which we are changing the lower
		xl1 = min(xbins(4,i3)) < xlb(nlb-1L)
; ...and keep it inside the actual wavelength range of xlb
;
; Redraw the border that we are changing as a solid line
		for i=0L, N4-1L do begin
			oplot,alog10(xbins(ilv,i4(i))>1), xbins(ixv,i4(i)),line=lsty
			oplot,alog10(xbins(ilv,i4(i))>1), xbins(ixv,i4(i))
		endfor
;
; Loop for changing wavelength limits.
		repeat begin
			str1=get_kbrd(1) & str2=get_kbrd(0) & str3=get_kbrd(0)
			change = byte(str3)& change=change(0)
; Erase the lines before the change
			for i=0L, Nib-1L do begin
				oplot,alog10(xbins(ilv,ib(i))>1), xbins(ixv,ib(i)),line=l(i)
				oplot,alog10(xbins(ilh,ib(i))>1), xbins(ixh,ib(i)),line=lsty
			endfor
			if (change EQ 67b) then xbins(k,ib)=10^(alog10(xbins(k,ib))+dll)
			if (change EQ 68b) then xbins(k,ib)=10^(alog10(xbins(k,ib))-dll)
            if (change EQ 67b  OR  change EQ 68b) then ALTERED_STATE=1b
			xbins(k,ib) = (xbins(k,ib) < xl1) > xl0
			print, string(13b), xl0, xbins(k(0), ib(0)), xl1, $
														form='(A0,3f9.0,$)
; Redraw after the change
			for i=0L, Nib-1L do begin
				oplot,alog10(xbins(ilv,ib(i))>1), xbins(ixv,ib(i)),line=l(i)
				oplot,alog10(xbins(ilh,ib(i))>1), xbins(ixh,ib(i)),line=lsty
			endfor
		endrep until (change EQ 0b)
		xls(j) = xbins(k(0), ib(0))
;
; Redraw the border that we just changed as a dotted line
		for i=0L, N4-1L do begin
			oplot,alog10(xbins(ilv,i4(i))>1), xbins(ixv,i4(i))
			oplot,alog10(xbins(ilv,i4(i))>1), xbins(ixv,i4(i)),line=lsty
		endfor
		if (str1 EQ 'q'  OR  str1 EQ 'r'  OR  str1 EQ 's') then begin
			Nxxs = 0L
			break
		endif
        if (ALTERED_STATE) then break
	endfor
;
; End of loop for changing wavelength limits.
;
; Loop for changing alogx limits.
  if (NOT ALTERED_STATE) then begin
	for n=0L, Nxxs-1L do begin
		i2 = where(abs(xbins(2,*) - xxs(n))/xxs(n) LT 1e-5)
		i0 = where(abs(xbins(0,*) - xxs(n))/xxs(n) LT 1e-5)
		N2 = N_ELEMENTS(i2)  &  N0 = N_ELEMENTS(i0)
		ib = [i2, i0]
		Nib = N_ELEMENTS(ib)
		k = [replicate(2L, N2), replicate(0L,   N0)]
		l = [replicate(0L, N2), replicate(lsty, N0)]
		xx0 = max(xbins(0,i2)) > !y.crange(0)
		xx1 = min(xbins(2,i0)) < !y.crange(1)
;
; Redraw the border that we are changing as a solid line
		for i=0L, N2-1L do begin
			oplot,alog10(xbins(ilh,i2(i))>1), xbins(ixh,i2(i)),line=lsty
			oplot,alog10(xbins(ilh,i2(i))>1), xbins(ixh,i2(i))
		endfor
		repeat begin
			str1=get_kbrd(1) & str2=get_kbrd(0) & str3=get_kbrd(0)
			change = byte(str3)& change=change(0)
; Erase the lines before the change
			for i=0L, Nib-1L do begin
				oplot,alog10(xbins(ilv,ib(i))>1), xbins(ixv,ib(i)),line=lsty
				oplot,alog10(xbins(ilh,ib(i))>1), xbins(ixh,ib(i)),line=l(i)
			endfor
			if (change EQ 65b) then xbins(k,ib)=xbins(k,ib)+dxx
			if (change EQ 66b) then xbins(k,ib)=xbins(k,ib)-dxx
            if (change EQ 65b  OR  change EQ 66b) then ALTERED_STATE=1b
			xbins(k,ib) = (xbins(k,ib) < xx1) > xx0
			print, string(13b), xx0, xbins(k(0), ib(0)), xx1, $
														form='(A0,3f9.2,$)
; Redraw after the change
			for i=0L, Nib-1L do begin
				oplot,alog10(xbins(ilv,ib(i))>1), xbins(ixv,ib(i)),line=lsty
				oplot,alog10(xbins(ilh,ib(i))>1), xbins(ixh,ib(i)),line=l(i)
			endfor
		endrep until (change EQ 0b)
		xxs(n) = xbins(k(0), ib(0))
		if (str1 EQ 'q'  OR  str1 EQ 'r'  OR  str1 EQ 's') then break
;
; Redraw the border that we just changed as a dotted line
		for i=0L, N2-1L do begin
			oplot,alog10(xbins(ilh,i2(i))>1), xbins(ixh,i2(i))
			oplot,alog10(xbins(ilh,i2(i))>1), xbins(ixh,i2(i)),line=lsty
		endfor
;
; End of loop for changing alogx limits.
        if (ALTERED_STATE) then break
	endfor
  endif

	xbins1 = xbins
	print, ''

    grfxf = 'GX'+['clear','and','andReverse','copy','andInverted','noop','xor',$
                  'or','nor','equiv','invert','orReverse','copyInverted',$
                  'orInverted','nand','set']
    if (oldg NE 3) then begin
        print, "  WARNING: The graphics_function in use before calling this"
        print, "           function = "+grfxf(oldg)+" is different from the"
        print, "           default  = "+grfxf(3)+"."
		if KEYWORD_SET(QUIET) then begin
			oldg = 3
		endif else begin
			s = prompt("Do you wish to revert to the default of " $
												+ grfxf(3) + " [y/n]? ")
			if (s NE 'n') then   oldg = 3
		endelse
    endif
    device, set_graphics_function=oldg

SaveIt:
	if (total(abs([(xbins1([0,2],*)-xbins0([0,2],*))/xbins0(2,*), $
		   (xbins1(3:4,*)  -xbins0(3:4,*))/xbins0(4,*)])) GT 1e-4 $
			AND (NOT KEYWORD_SET(NOSAVE)) ) then begin

		if (NOT KEYWORD_SET(DOSAVE)) then begin
			answ = prompt(" Save the new bin specifications? [y/n/q/s]: ")
			if (answ EQ 'n'  OR  answ EQ 'q'  OR  answ EQ 'r') then return
			if (answ EQ 's')                                   then stop
		endif

;
; back-up the old radtab.in-file
		if (NOT KEYWORD_SET(NOBACKUP)) then begin
			backup, infile
			print, ' Backed up and', form='(A0,$)'
		endif
; ...format the new bin specifications
;		print, xbins1
		for i=0L, Nbins-1L do begin
			stra0 = stra(idxbin(i))
			stra(idxbin(i))  = $
				string(xbins1(*,i),form='(3(f5.2,","),f7.0,",",f8.0,", ")') $
				+ strmid(stra0, strpos(stra0, ".x1, xx, x2, lmbd1, lmbd2"), 99)
			idxbin = where(strpos(stra, "x1, xx, x2, lmbd1, lmbd2") GT 5L)
		endfor
; ...and write the new bin specifications to the new file.
		openw,    lun, infile, /GET_LUN
		printf,   lun, stra, form='(A0)'
		free_lun, lun
		print, ' wrote to file; ', infile
; Print the new bin specifications to std. out.
		if (NOT KEYWORD_SET(QUIET)) then  print, xbins1
	endif

	if KEYWORD_SET(STOP) then stop
end
