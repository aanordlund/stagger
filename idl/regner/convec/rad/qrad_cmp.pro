;
;   Program to load and plot the qrad_cmp.F77.dat files produced by
;   radtab.x when computing tables of binned opacities.
;
;   Arguments (defaults in [...]):
;    file:    Full name of the data file ['qrad_cmp.F77.dat'].
;    ext:     A possible extension to the file-name above [''].
;    qradm:   Contains the average monochromatic heating read from the file.
;    qradb:   Contains the average binned heating read from the file.
;    Ltau:    Contains log10(\tau_Ross) read from the file.
;    EXTENSIVE: Set this keyword to plot qrad/[volume] instead /[mass].
;    NOPLOT:  If set, don't plot.
;    _EXTRA:  Extra arguments that will be passed on to the plot statement [].
;    STOP:    Set this keyword to stop before returning, to make all
;             variables available to the user (for debugging).
;
;   Update history:
;   ---------------
;   18.08.2008 Coded/RT
;   05.04.2009 Added print-out of Teff for each case/RT
;
;   Known bugs: None
;
pro qrad_cmp, ext, file=file, xrange=xrange, yrange=yrange, qradm=qradm, $
        qradb=qradb, Ltau=Ltau, EXTENSIVE=EXTENSIVE, NOPLOT=NOPLOT, $
        _EXTRA=extra_args, STOP=STOP
@consts

	default, ext, ['']
	default, file, 'qrad_cmp.F77.dat'
	Next = N_ELEMENTS(ext)

	c0 = (!D.TABLE_SIZE-1.)*.6

	for i=0L, Next-1L do $
		if (ext(i) NE ''  AND  strmid(ext(i),0,1) NE '.') then ext(i)='.'+ext(i)
	file = file + ext

	for i=0L, Next-1L do begin
		openr, lun, file(i), /F77, /GET_LUN, /SWAP_IF_LITTLE_ENDIAN
		mz = 0L & mtau=mz
		readu, lun, mz, mtau
		z = fltarr(mz)& rhom=z& tmean=z& qradm=z& qradb=z& tau=fltarr(mtau)
		readu, lun, z, tau, rhom, tmean, qradm, qradb
		free_lun, lun
		Ltau = alog10(tau)
		Teffm = (-integf(z, qradm, /TOTAL)/sig)^.25
		Teffb = (-integf(z, qradb, /TOTAL)/sig)^.25
        ytitl = '!8Q!x!drad!n!5/[!xerg!i !ncm!u-3!n!i !ns!u-1!n!5]!x'
        titl  = 'Comparing radiative heating per volume'
        xmarg = [11,2]
		if (NOT KEYWORD_SET(EXTENSIVE)) then begin
			qradm = qradm/rhom
			qradb = qradb/rhom
            ytitl = '!8q!x!drad!n!5/[!xerg!i !ng!u-1!n!i !ns!u-1!n!5]!x'
            titl  = 'Comparing radiative heating per mass'
            xmarg = [14,2]
		endif
		if KEYWORD_SET(NOPLOT) then begin
			if KEYWORD_SET(STOP) then stop
			return
		endif

		c0 = float(Next-i)/(Next+1.)*(!D.TABLE_SIZE-1.)
		ii = where(Ltau GT -5.)
		ext1 = strmid(ext(i), 1, 99)

		if (i EQ 0) then begin
			default, xrange, [Ltau(0), 2.5]
            default, yrange, [min([qradm,qradb,qradb-qradm], max=ymx),ymx]
            if KEYWORD_SET(EXTENSIVE) then $
                     yrange(1)=max([yrange(1),-.08*yrange(0)])
;
; Plot the radiative heating per mass, q_rad.
; The extensive quantity is q_rad*\rho.
			plot, Ltau, qradm, xr=xrange, psym=-2, syms=.3, ysty=1, $
				xtitl='log!d10!n!7s!x!dRoss!n', ytitl=ytitl, $
				xmarg=xmarg, title=titl, yrange=yrange, _EXTRA=extra_args
			label, .25, .50, 0, ''
			label, .37, .50, 0, 'Monochromatic', col=!P.background
			print, min(qradm(ii), max=maxq), maxq, Teffm, $
                                                form='("mon:",2e13.5,f9.2,"K")'
			xyouts, .275,.485, 'Binned  Monochr.-binned', /NORM, size=.9*!P.charsize
			oplot, !x.crange, [1,1]*0, line=1 
		endif
		plots, Ltau, qradb, col=c0
		label, .25, .42-.04*i, 0, '', col=c0
		plots, Ltau, qradb-qradm, line=2, col=c0
		label, .37, .42-.04*i, 2, underscore(ext1), col=c0
        itau  = where(Ltau GT -6.)
        dqmax = max(abs(qradb(itau)-qradm(itau)))/max(abs(qradm(itau)))*1d2

		print, min(qradb(ii), max=maxq), maxq, Teffb, Teffb-Teffm, dqmax, ext1,$
                        form='("bin:",2e13.5,f9.2,"K",f7.2,"K",f7.2,"%  ",A0)'
	endfor

	if KEYWORD_SET(STOP) then stop
end
