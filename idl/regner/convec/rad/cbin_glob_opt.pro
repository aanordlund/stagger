;
;   Program to perform a full optimization of the opacity bins.
;   This is a wrapper for the cbin_opt procedure.
;
;   Arguments (defaults in [...]):
;    series:  Next letter from last letter in extensions to the found
;             <radtabin>-files. If no files found, uses 'a'.
;    radtabin:Root name of the input-file top radtab.x ['radtab.in.bin'].
;    CONTINUE:Set this keyword to continue a previous series of bin optimization
;             by picking the highest letter (series) and the highest number (N0)
;             of the found extensions to <radtabin>-files.
;    STOP:    Set this keyword to stop before returning, to make all
;             variables available to the user (for debugging).
;
;   Update history:
;   ---------------
;   26.06.2014 Coded/RT
;   08.01.2016 Added writing to log. Improved diagnostic printing. Improved
;              handling of optimization series, and added option of automatic
;              calculation of new kaprhoT table. Succesfully carried out
;              the c-series for the t75g39m+00 simulation/RT
;   05.03.2020 No longer use Makefile for computing kaprho-table with the new
;              optimized bins, but instead use spawned, explicit commands for
;              both computing table, timing it and copy table and log for
;              posterity with date/time and optimization series + number./RT
;
;   Known bugs: None
;
pro cbin_glob_opt, series, radtabin=radtabin, iex=iex, $
                                                CONTINUE=CONTINUE, STOP=STOP
    default, radtabin, 'radtab.in.bin'
    ser = findfile(radtabin+'.?[0-9][0-9]')
    N0  = 0L
    sbegin = " Starting "
    if (ser(0) EQ '') then begin
        series0 = 'a'
    endif else begin
        series0 = max(strmid(ser, 14L, 1L))
    endelse
    default, series, series0

    if KEYWORD_SET(CONTINUE) then begin
        ii = where(strmid(ser, 14L, 1L) EQ series)
        N0 = max(long(strmid(ser(ii), 15L, 2L)))
        sbegin = " Continuing "
    endif

    seriesN0    = '.'+series+string(N0, form='(I02)')&  seriesN=seriesN0
    radtabinN0  = radtabin+seriesN0
    radtabinN00 = radtabinN0
    print, sbegin+"series '"+series+"', starting from "+radtabinN0,for='(A0,$)'
    if (NOT KEYWORD_SET(CONTINUE)) then begin
; Copy the last radtab,in-file which is not a tmp-file made by cbin_opt,
; to <radtabinN0>
        if (NOT file_test(radtabinN0)) then begin
            spawn, "ls -1t radtab*.in*|grep -v '\.tmp'|head -1", origin
            spawn, "cp "+origin(0)+" "+radtabinN0, res
            print, " which is copied from "+origin(0)
        endif else print, ' '
    endif else print, ' '
    spawn, 'cat '+radtabinN0, res
    Nbins= 12L
    head = res(0:3)
    sbins0 = res(4L:3L+Nbins)
    xbins0 = dblarr(5L, Nbins)& x1 = dblarr(5L)
    xnorm  = xbins0& xnorm(0:2,*) = 1d-2 & xnorm(3:*,*) = 1d0
    comment = strarr(Nbins)
    for i=0L, Nbins-1L do begin
        tmp = str_sep(sbins0(i),' \.')
        reads, tmp(0L), x1
        xbins0(*, i) =  x1
        comment(i) = ' .' + tmp(1)
    endfor
    xbins00 = xbins0
    foot = res(3L+Nbins+1L:*)
    N = N0& DONE = 0b
;
; Loop over bins until we are done
    repeat begin
        cbin_opt, radtabinN0, xbins_opt=xbins_opt, iex=iex

        dxbinMax = max(abs(xbins0-xbins_opt)/xnorm, imx)
        if (dxbinMax GT 0.5) then begin
            N    = N + 1L
            seriesN = '.'+series+string(N, form='(I02)')
            radtabinN = radtabin+seriesN
; Write the results of the optimization to the next radtab.in-file
            openw, lun, radtabinN, /GET_LUN
            printf, lun, head, form='(A0)'
            for i=0L, Nbins-1L do printf, lun, xbins_opt(*,i), comment(i), $
                                   form='(3(f5.2,","),f7.0,",",f8.0,",", A0)'
            printf, lun, foot, form='(A0)'
            free_lun, lun
            radtabinN0= radtabinN
            xbins0    = xbins_opt
        endif else begin
            print, " No change to this limit!"
            radtabinN = radtabinN0
        endelse

        s = prompt(" Optimize another limit? [y/n/q/s]: ")
        if (s EQ 'q'  OR  s EQ 'r') then return
        if (s EQ 'n')               then DONE=1b
        if (s EQ 's')               then stop
    endrep until (DONE)

    s = prompt(" Use " + radtabinN $
      + " for a new table calculation (otherwise abandon it)? [y/n/q/s]: ")
    if (s EQ 'n'  OR  s EQ 'q'  OR  s EQ 'r') then return
    if (s EQ 's')                             then stop
;
; Keep it and run with it!
    backup, 'radtab.in', /MOVE
    file_delete, 'radtab.in', /ALLOW_NONEXISTENT
    file_link, radtabinN, 'radtab.in'

    logtxt = 'Optimized opacity bins from ' + radtabinN00 + ' to ' + radtabinN
    logtxt = logtxt $
           + " Backed up 'radtab.in' and linked a new one to " + radtabinN
    print, logtxt

    s = prompt(" Start calculation of new table [n/y] ? ")
    if (s EQ 'y') then begin
        spawn,'(/usr/bin/time ./radtab.x < radtab.in >& radtab.log;' $
             + 'cp -p radtab.log   radtab_'+datestr()+seriesN+'.log; ' $
             + 'cp -p kaprhoT.tab kaprhoT_'+datestr()+seriesN+'.tab) &', result
        pid  = strtrim((str_sep(result, ' '))(1), 2)
        host = (str_sep(getenv('HOST'), '\.'))(0)
        host = strupcase(strmid(host,0,1))+strmid(host,1,99)
        print, " radtab.x running on "+host+" as PID="+pid
    endif
    add_to_log, logtxt, /CLEAR

    if KEYWORD_SET(STOP) then stop
end
