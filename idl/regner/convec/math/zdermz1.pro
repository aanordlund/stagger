;
;   Function to calculate monotone cubic derivatives, according to
;     Fritsch & Butland, 1984, SIAM J. Sci. Stat. Comput., 5, 300
;   This function uses truncated boundary conditions.
;
;   Update history:
;   ---------------
;   27.05.2004 Coded/RT
;   27.05.2004 Changed to 1D d-arrays/RT
;   01.06.2006 Implicit z-loop - much faster!/RT
;
function zdermz1, z, f

	nz = N_ELEMENTS(z)
	if (nz LT 2) then return, 0*f

	d2f1=0.0  &  d2fn=0.0
	a  = 0*z
	cz = a
	d  = a
	d2 = a
	cz(0) = 1e-20
	cz(1:*) = z(1:*) - z
	a       = (1.0 + cz(1:*)/(cz + cz(1:*)))/3.0
	cz = 1.0/cz
	dd = 0*f

	d(1:*)  = (f(1:*)-f(*))*cz(1:*)
	d2(1:*) = d(1:*)*d
	ii      = where(d2(2:*) GT 0.0) + 1L
	dd(ii)  = d2(ii+1)/(a(ii)*d(ii+1)+(1.-a(ii))*d(ii))
;	dd(1:nz-2) = (d2(2:*)>0.0)/(a(1:*)*d(2:*)+(1.-a(1:*))*d(1:*))
	dd(   0) = d2f1/(4.0*cz(   1)) - 0.5*dd(   1) + 1.5*d(   1)
	dd(nz-1) = d2fn/(4.0*cz(nz-1)) - 0.5*dd(nz-2) + 1.5*d(nz-1)

	return, dd
end
