;***********************************************************************
pro yder0
;
;  Calculates the matrix elements and an LU decomposition of the problem
;  of finding the derivatives of a periodic cubic spline with continuous
;  second derivatives.
;
;  For a periodic spline, we have an equation system of the form:
;
;     | w2 w3        w1 |   | d |     | e |
;     | w1 w2 w3        |   | d |     | e |
;     |     .  .  .     | * | . |  =  | . |
;     |        .  .  .  |   | . |     | . |
;     | w3        w1 w2 |   | d |     | e |
;
;  This may be solved by adding a fraction (w4) of each row to the
;  following row, and a fraction (w5) to the last row.
;
;  This corresponds to an LU decomposition of the problem:
;
;     | w2 w3        w1 |   | d |     | 1               |   | e |
;     |    w2 w3     w1 |   | d |     | w4  1           |   | e |
;     |        .  .  .  | * | . |  =  |    w4  1        | * | . |
;     |           .  .  |   | . |     |        .  .     |   | . |
;     |              w2 |   | d |     | w5 w5  .  w4  1 |   | e |
;
;  In this startup routine, we calculate the fractions w4 and w5.
;  These are the same for all periodic splines with the same period,
;  and only have to be calculated once for all ny*nz splines.
; 
;  In principle, this startup routine only has to be called once,
;  but to make the yder entry selfcontained, yder0 is called from
;  within yder.  In practice, the yder0 time is insignificant.
; 
;  Update history:
;                      .
;  20-aug-87: coded by Ake Nordlund, Copenhagen University Observatory
;  14-sep-87: performance with alternative y-/z-loop ordering tested
;  04-nov-87: inverted w2(), to turn divides into multiplications
;
;***********************************************************************
;
common cdat,x,y,z,nx,ny,nz,nw,ntmax,date0,time0 
common cyder,w1,w2,w3,w4,w5
;
;  Multiply with dy divided by a factor 3, to make the factors 1, 4, 1
;
      dy=y(1)-y(0)
      cy=dy/3.
;
      w1=fltarr(ny)
      w2=w1
      w3=w1
      w4=w1
      w5=w1
      w1(*)=cy
      w2(*)=4.*cy
      w3(*)=cy
;
;  Eliminate subdiagonal elements of rows 2 to ny-2
;
      for k=1,ny-3 do begin
        w4(k)=-w1(k)/w2(k-1)
        w2(k)=w2(k)+w4(k)*w3(k-1)
        w1(k)=w4(k)*w1(k-1)
        w5(k)=-w3(ny-1)/w2(k-1)
        w2(ny-1)=w2(ny-1)+w5(k)*w1(k-1)
        w3(ny-1)=w5(k)*w3(k-1)
      end
;
;  We now have a 3*3 system on the last three rows:
;
;     |        w2 w3 w1 | * | . |  =  | . |
;     |        w1 w2 w3 |   | . |     | . |
;     |        w3 w1 w2 |   | d |     | e |
;
      w4(ny-2)=-w1(ny-2)/w2(ny-3)
      w2(ny-2)=w2(ny-2)+w4(ny-2)*w3(ny-3)
      w3(ny-2)=w3(ny-2)+w4(ny-2)*w1(ny-3)
      w5(ny-2)=-w3(ny-1)/w2(ny-3)
      w1(ny-1)=w1(ny-1)+w5(ny-2)*w3(ny-3)
      w2(ny-1)=w2(ny-1)+w5(ny-2)*w1(ny-3)
;
;  Row ny
;
      w4(ny-1)=-w1(ny-1)/w2(ny-2)
      w2(ny-1)=w2(ny-1)+w4(ny-1)*w3(ny-2)
;
;  Invert all the w2()'s, turns divides into mults
;
      w2(*)=1./w2(*)
;
      end
;***********************************************************************
function yder,f
;
;  The following LU decomposition of the problem was calculated by the
;  startup routine:
;
;  | w2(1) w3(1)        w1(1) |   |d|   |  1                    |   |e|
;  |       w2(2) w3(2)  w1(2) |   |d|   | w4(2)  1              |   |e|
;  |          .     .      .  | * |.| = |       w4(3)  1        | * |.|
;  |                .      .  |   |.|   |              .  .     |   |.|
;  |                    w2(N) |   |d|   | w5(2) w5(3)  .  w4  1 |   |e|
;
;  The solutions of all ny*nz equations are carried out in paralell.
;
;  The y-loops are placed inside the z-loops, to obtain a smaller stride
;  (less paging).  This is important for 3D-cases with large my*mz.
;  If the z-loops are placed inside the y-loops, one achieves better
;  performance in 2D cases (where ny=1), but this is of less importance.
;
;  Operation count: 5m+5a = 10 flop
;
;  Timing: at 31*31*30
;     11 Mfl = 1.5 Mpts/sec on the CU Alliant FX-8
;     57 Mfl = 5.7 Mpts/sec on the NCSA Cray X-MP (nested loops)
;     70 Mfl =              on the PSC Cray X-MP (combined loops)
;
;  Update history:
;                      .
;  20-aug-87: coded by Ake Nordlund, Copenhagen University Observatory
;  14-sep-87: performance with alternative y-/z-loop ordering tested
;  04-nov-87: inverted w2(), to turn divides into multiplications
;  25-feb-88: removed use of swapped ff(), speeded things up 10 %
;  04-mar-88: padded blank common with one chunk, for curl, curl2, nodiv
;  07-mar-88: coalesced m,n loops -> mn loop
;  12-apr-88: removed blank common padding for hd (no curl, curl2)
;  09-oct-06: Checks for dimensions - now independant of simulation/RT
;
;***********************************************************************
;
common cdat,x,y,z,nx,ny,nz,nw,ntmax,date0,time0 
common cyder,w1,w2,w3,w4,w5
;
;  NOTE!  This version uses mw words of blank common.  Beware of cases
;  where the calling routine also uses blank common.
;  div() does, but only after yder() has been called, so it is OK.
;  curl uses mw words, and curl2 use 2*mw words blank common
;
;  Swap indices, to be able to walk through consequtive memory (time drops
;  from 27.2 s to 6.4 s for 17 times 64*64*64 derivative).
;
;  Check if we have a degenerate case (no y-extension)
;
      d=f
	  sf = size(f)
	  if (sf(0) NE 3) then begin
		print, " ERROR: This function only works for the 3D case. Bailing out!"
		stop
	  endif
      if ny eq 1 then begin
        d(*,*,*)=0.0
        return,d
      endif
	  y0 = y  &  ny0 = ny
	  if (sf(2) LT ny) then begin
		ny = sf(2)
		y  = y(lindgen(ny)*(ny0/ny))
	  endif
	  if (sf(2) GT ny) then begin
		print, " ERROR: Cannot yet handle y-dims greater than in simulation."
		print, "        Bailing out!"
		stop, "ny, ny0: ", ny, ny0
	  endif
;
;  Right hand sides [1s = 1 flop]
;
      yder0
      ny1=ny-1 & ny2=ny-2
      d(*,0,*)=(f(*,1,*)-f(*,ny1,*))
      d(*,1:ny2,*)=(f(*,2:ny1,*)-f(*,0:ny-3,*))
      d(*,ny1,*)=(f(*,0,*)-f(*,ny2,*))
;
;  Do the forward substitution [2m+2a = 4 flop]
;
      for k=1,ny2 do begin
        d(*,k,*)=d(*,k,*)+w4(k)*d(*,k-1,*)
        d(*,ny1,*)=d(*,ny1,*)+w5(k)*d(*,k-1,*)
      end
      d(*,ny1,*)=d(*,ny1,*)+w4(ny1)*d(*,ny2,*)
;
;  Do the backward substitution [3m+2s = 5 flop]
;
      d(*,ny1,*)=d(*,ny1,*)*w2(ny1)
      d(*,ny2,*)=(d(*,ny2,*)-w3(ny2)*d(*,ny1,*))*w2(ny2)
      for k=ny-3,0,-1 do begin
        d(*,k,*)=(d(*,k,*)-w3(k)*d(*,k+1,*)-w1(k)*d(*,ny1,*))*w2(k)
      end
;
;  Restore y to it's former glory
	  if (ny NE ny0) then begin
		y = y0  &  ny = ny0
	  endif
;
      return,d
      end
