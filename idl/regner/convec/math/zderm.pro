;
;   Function to calculate monotone cubic derivatives, according to
;     Fritsch & Butland, 1984, SIAM J. Sci. Stat. Comput., 5, 300
;   This function uses truncated boundary conditions.
;   It is 1.04 times faster than the cubic spline interpolation of zder,
;   and 7.23 times faster than zderm.pro.bak2 with explicit x- and y-loops.
;   Timing: 0.30s for 150x150x82 array on a 1.2GHz UltraSPARC-III+.
;
;   Update history:
;   ---------------
;   27.05.2004 Coded/RT
;   27.05.2004 Changed to 1D d-arrays/RT
;   01.06.2006 Implicit z-loop - much faster!/RT
;	09.10.2006 Implicit x-/y-loops - yet much faster!/RT
;
function zderm, f
	common cdat,x,y,z,nx,ny,nz,nw,ntmax,date0,time0

	sf = size(f)
	if (sf(0) NE 3) then begin
		print, " ERROR: This function only works for the 3D case. Bailing out!"
		stop
	endif
	if (nz LT 2) then return, 0*f
	if (sf(3) NE nz) then begin
		print, "ERROR: z-scale different from that in simulation - use zdermz."
		print, "       Bailing out!"
		stop
	endif
	mx = sf(1) & my=sf(2)

	d2f1=0.0  &  d2fn=0.0
	a  = 0*z  &  cz = a
	cz(0) = z(1)-z(0);												Checked
	for n=1, nz-2 do begin
		cz(n) = z(n+1)-z(n);										Checked
		a(n)  = (1.0 + cz(n)/(cz(n-1) + cz(n)))/3.0;				Checked
	endfor
	cz(nz-1) = 1e-20;				Not needed, but prevents Inf in next line
	cz = 1.0/cz
	dd = fltarr(mx*my, nz)

	d1 = (f(*,*,1)-f(*,*,0))*cz(0);									Checked
	dd(*,   0) = d2f1/(4.0*cz(   0)) - 0.5*dd(*,   1) + 1.5*d1
	for n=1, nz-2 do begin
		d0 = d1;													Checked
		d1 = (f(*,*,n+1)-f(*,*,n))*cz(n);							Checked
		d2 = d1*d0;													Checked
		ii = where(d2 GT 1e-20, COMPLEMENT=i0)
		if (ii(0) GE 0L) then dd(ii,n) = d2(ii)/(a(n)*d1(ii)+(1.-a(n))*d0(ii))
		if (i0(0) GE 0L) then dd(i0,n) = 0.0;						Checked
	endfor
	dd(*,nz-1) = d2fn/(4.0*cz(nz-2)) - 0.5*dd(*,nz-2) + 1.5*d1;		Checked

	return, reform(dd, mx, my, nz)
end
