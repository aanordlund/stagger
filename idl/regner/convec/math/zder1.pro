;***********************************************************************
pro zder0
;
;  Calculates the matrix elements and an LU decomposition of the problem
;  of finding the derivatives of a cubic spline with continuous second
;  derivatives.
;
;  This particular version (to be used with the hd/mhd code with fixed
;  boundary values at a fiducial extra boundary layer) has end point
;  derivatives equal to one sided first order derivatives.  These are
;  not used directly, and influence the solution only through the con-
;  dition of a continuous second derivative at the next point in.
;
;  To make a "natural spline" (zero second derivative at the end pts),
;  change the weights to
;
;     w2(1)=2./3.
;     w3(1)=1./3.
;     w2(nz)=2./3.
;     w1(nz)=1./3.
;
;  For a general cubic spline, we have an equation system of the form:
;
;     | w2 w3          |   | d |     | e |
;     | w1 w2 w3       |   | d |     | e |
;     |       .  .     | * | . |  =  | . |
;     |       .  .  .  |   | . |     | . |
;     |          w1 w2 |   | d |     | e |
;
;  This may be solved by adding a fraction (w1) of each row to the
;  following row.
;
;  This corresponds to an LU decomposition of the problem:
;
;     | w2 w3          |   | d |     | 1               |   | e |
;     |    w2 w3       |   | d |     | w1  1           |   | e |
;     |       .  .     | * | . |  =  |    w1  1        | * | . |
;     |          .  .  |   | . |     |        .  .     |   | . |
;     |             w2 |   | d |     |           w1  1 |   | e |
;
;  In this startup routine, we calculate the fractions w1.
;  These are the same for all splines with the same z-scale,
;  and only have to be calculated once for all nx*ny splines.
;
;  In principle, this startup routine only has to be called once,
;  but to make the zder entry selfcontained, zder0 is called from
;  within zder.  In practice, the zder0 time is insignificant.
; 
;  Update history:
;                      .
;  20-aug-87: coded by Ake Nordlund, Copenhagen University Observatory
;  27-sep-87: Removed bdry[123] parameters.
;  02-oct-87: Moved factor 3 from zder loop to zder0 loop.
;  04-nov-87: inverted w2(), to turn divides into multiplications
;
;***********************************************************************
;
;
common cdat,x,y,z,nx,ny,nz,nw,ntmax,date0,time0 
common czder,w1,w2,w3
common czderbdr,top,bot
    
	w1=fltarr(nz)
	w2=fltarr(nz)
	w3=fltarr(nz)
;
;  Interior points
;
	a3=1./3.
	w1(1L:*)=a3/(z(1L:*)-z)
	w3(0L:nz-2L)=w1(1L:nz-1L)
	w2=2.*(w1+w3)
;
;  First point
;
	if (top EQ 1L) then begin
		w1(0L)=0.0
		w2(0L)=1.0
		w3(0L)=0.0
	endif else begin
		czb=1./(z(1L)-z(0L))
		czc=1./(z(2L)-z(1L))
		w1(0L)= czb*czb
		w3(0L)=-czc*czc
		w2(0L)=w1(0L)+w3(0L)
	endelse
;
;  Last point
;
	if (bot EQ 1L) then begin
		w1(nz-1L)=0.0
		w2(nz-1L)=1.0
		w3(nz-1L)=0.0
	endif else begin
		cza=1./(z(nz-2L)-z(nz-3L))
		czb=1./(z(nz-1L)-z(nz-2L))
		w1(nz-1L)= cza*cza
		w3(nz-1L)=-czb*czb
		w2(nz-1L)=w1(nz-1L)+w3(nz-1L)
	endelse
;
;  Eliminate at first point
;
	if (top NE 1L) then begin
		c=-w3(0L)/w3(1L)
		w1(0L)=w1(0L)+c*w1(1L)
		w2(0L)=w2(0L)+c*w2(1L)
		w3(0L)=w2(0L)
		w2(0L)=w1(0L)
		w1(0L)=c
	endif
;
;  Eliminate at last point
;
	if (bot NE 1L) then begin
		c=-w1(nz-1L)/w1(nz-2L)
		w2(nz-1L)=w2(nz-1L)+c*w2(nz-2L)
		w3(nz-1L)=w3(nz-1L)+c*w3(nz-2L)
		w1(nz-1L)=w2(nz-1L)
		w2(nz-1L)=w3(nz-1L)
		w3(nz-1L)=c
	endif
;
;  Eliminate subdiagonal elements of rows 2 to nz:
;
;     |     w2 w3       | * | . |  =  | . |
;     |     w1 w2 w3    |   | . |     | . |
;
	for k=1L,nz-1L do begin
		w1(k)=-w1(k)/w2(k-1L)
		w2(k)=w2(k)+w1(k)*w3(k-1L)
	endfor
;
;  Invert all the w2()'s, turns divides into mults
;
	w2=1./w2
;
	end
;***********************************************************************
function zder1,f,dn
;
;  The following LU decomposition of the problem was calculated by the
;  startup routine:
;
;  | w2(1) w3(1)              |   |d|   |  1                    |   |e|
;  |       w2(2) w3(2)        |   |d|   | w1(2)  1              |   |e|
;  |          .     .         | * |.| = |       w1(3)  1        | * |.|
;  |                .      .  |   |.|   |              .  .     |   |.|
;  |                    w2(N) |   |d|   |                 w1  1 |   |e|
;
;  The solutions of all nx*ny equations are carried out in paralell.
;
;  Operation count:  4m+1d+4a = 9 flops per grid point
;
;  Timing:
;     14.2 Mfl = 1.6 Mpts/sec on the Univ of Colo Alliant FX-8.
; 
;  Update history:
;                      .
;  20-aug-87: Coded by Ake Nordlund, Copenhagen University Observatory
;  27-sep-87: Removed bdry[123] parameters.
;  02-oct-87: Moved factor 3 from zder loop to zder0 loop.
;  15-oct-87: Factors a and b moved out of lm-loops 121. 13->9 flops.
;  04-nov-87: inverted w2(), to turn divides into multiplications
;
;***********************************************************************
;
common cdat,x,y,z,nx,ny,nz,nw,ntmax,date0,time0 
common czder,w1,w2,w3
common czderbdr,top,bot
;
;  Check if we have a degenerate case (no z-extension)
;
; The hydro-code corresponds to the bot=0 & top=1 -case..!...
	bot=0L & top=1L
	d = 0*z
	if (nz eq 1L) then begin
;       d(*)=0.0
		return,d
	endif
	zder0
;
;  First point
;
	czb=1./(z(1L)-z(0L))
	czc=1./(z(2L)-z(1L))
	dfb=f(1L)-f(0L)
	dfc=f(2L)-f(1L)
	if (top EQ 1L) then  d(0L)=dfb*czb  $
				   else  d(0L)=2.*(dfb*czb*czb*czb-dfc*czc*czc*czc)
;
;  Interior points [2m+2s = 4 flop]
;
	for k=1L,nz-2L do begin
		a=1./(z(k+1L)-z(k))^2
		b=1./(z(k)-z(k-1L))^2
		d(k)=(f(k+1L)-f(k))*a+(f(k)-f(k-1L))*b
	endfor
;
;  Last point
;
	cza=1./(z(nz-2L)-z(nz-3L))
	czb=1./(z(nz-1L)-z(nz-2L))
	dfa=f(nz-2L)-f(nz-3L)
	dfb=f(nz-1L)-f(nz-2L)
	if (bot EQ 1L) then  dn0=dfb*czb  $
				   else  dn0=2.*(dfa*cza*cza*cza-dfb*czb*czb*czb)
	default, dn, dn0
	d(nz-1L) = dn
;
;  Eliminate at first and last points
;
	d(0L)=d(0L)+w1(0L)*d(1L)
	if (dn EQ dn0) then d(nz-1L)=d(nz-1L)+w3(nz-1L)*d(nz-2L)
;
;  Do the forward substitution [1m+1a = 2 flop]
;
	kmax = nz-2L+(dn EQ dn0)
	for k=1L,kmax do begin
		d(k)=d(k)+w1(k)*d(k-1L)
	endfor
;
;  Do the backward substitution [1m+1d+1s = 3 flop]
;
	if (dn EQ dn0) then d(nz-1L)=d(nz-1L)*w2(nz-1L)
	for k=nz-2L,0L,-1L do begin
		d(k)=(d(k)-w3(k)*d(k+1L))*w2(k)
	endfor
;
	return,d
end
