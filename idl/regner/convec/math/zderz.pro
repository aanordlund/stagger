;
;  Bob's convention with zderz,z,f
;  instead of Ake's:     zderz,f,z
;
;***********************************************************************
PRO zder0z,nz,z
;
;  Calculates the matrix elements and an LU decomposition of the problem
;  of finding the derivatives of a cubic spline with continuous second
;  derivatives.
;
;  This particular version (to be used with the hd/mhd code with fixed
;  boundary values at a fiducial extra boundary layer) has end point
;  derivatives equal to one sided first order derivatives.  These are
;  not used directly, and influence the solution only through the con-
;  dition of a continuous second derivative at the next point in.
;
;  To make a "natural spline" (zero second derivative at the end pts),
;  change the weights to
;
;     w2(1)=2./3.
;     w3(1)=1./3.
;     w2(nz)=2./3.
;     w1(nz)=1./3.
;
;  For a general cubic spline, we have an equation system of the form:
;
;     | w2 w3          |   | d |     | e |
;     | w1 w2 w3       |   | d |     | e |
;     |       .  .     | * | . |  =  | . |
;     |       .  .  .  |   | . |     | . |
;     |          w1 w2 |   | d |     | e |
;
;  This may be solved by adding a fraction (w1) of each row to the
;  following row.
;
;  This corresponds to an LU decomposition of the problem:
;
;     | w2 w3          |   | d |     | 1               |   | e |
;     |    w2 w3       |   | d |     | w1  1           |   | e |
;     |       .  .     | * | . |  =  |    w1  1        | * | . |
;     |          .  .  |   | . |     |        .  .     |   | . |
;     |             w2 |   | d |     |           w1  1 |   | e |
;
;  In this startup routine, we calculate the fractions w1.
;  These are the same for all splines with the same z-scale,
;  and only have to be calculated once for all nx*ny splines.
;
;  In principle, this startup routine only has to be called once,
;  but to make the zder entry selfcontained, zder0 is called from
;  within zder.  In practice, the zder0 time is insignificant.
; 
;  Update history:
;                      .
;  20-aug-87: coded by Ake Nordlund, Copenhagen University Observatory
;  27-sep-87: Removed bdry[123] parameters.
;  02-oct-87: Moved factor 3 from zder loop to zder0 loop.
;  04-nov-87: inverted w2(), to turn divides into multiplications
;
;***********************************************************************
;
;
common czder,w1,w2,w3
    
      w1=fltarr(nz)
      w2=fltarr(nz)
      w3=fltarr(nz)
;
;  Interior points
;
      a3=1./3.
      w1(1:*)=a3/(z(1:*)-z)
      w3(0:nz-2)=w1(1:nz-1)
      w2=2.*(w1+w3)
;
;  First point
;
      w1(0)=0.0
      w2(0)=1.0
      w3(0)=0.0
;
;  Last point
;
      w1(nz-1)=0.0
      w2(nz-1)=1.0
      w3(nz-1)=0.0
;
;  Natural spline (uncomment)
;
      w2(0)=2./3.
      w3(0)=1./3.
      w2(nz-1)=2./3.
      w1(nz-1)=1./3.
;
;  Eliminate subdiagonal elements of rows 2 to nz:
;
;     |     w2 w3       | * | . |  =  | . |
;     |     w1 w2 w3    |   | . |     | . |
;
      for k=1,nz-1 do begin
        w1(k)=-w1(k)/w2(k-1)
        w2(k)=w2(k)+w1(k)*w3(k-1)
      end
;
;  Invert all the w2()'s, turns divides into mults
;
      w2=1./w2
;
      end
;***********************************************************************
FUNCTION zderz,z,f
;
;  The following LU decomposition of the problem was calculated by the
;  startup routine:
;
;  | w2(1) w3(1)              |   |d|   |  1                    |   |e|
;  |       w2(2) w3(2)        |   |d|   | w1(2)  1              |   |e|
;  |          .     .         | * |.| = |       w1(3)  1        | * |.|
;  |                .      .  |   |.|   |              .  .     |   |.|
;  |                    w2(N) |   |d|   |                 w1  1 |   |e|
;
;  The solutions of all nx*ny equations are carried out in paralell.
;
;  Operation count:  4m+1d+4a = 9 flops per grid point
;
;  Timing:
;     14.2 Mfl = 1.6 Mpts/sec on the Univ of Colo Alliant FX-8.
; 
;  Update history:
;                      .
;  20-aug-87: Coded by Ake Nordlund, Copenhagen University Observatory
;  27-sep-87: Removed bdry[123] parameters.
;  02-oct-87: Moved factor 3 from zder loop to zder0 loop.
;  15-oct-87: Factors a and b moved out of lm-loops 121. 13->9 flops.
;  04-nov-87: inverted w2(), to turn divides into multiplications
;
;***********************************************************************
;
common czder,w1,w2,w3
;
;  Check if we have a degenerate case (no z-extension)
;
	  if (N_ELEMENTS(f) EQ 0) then begin
		f = z  &  z = 0
	  endif
      s=size(f)
      nx=s(1) & ny=s(2) & nz=s(3)
      d=f
      if (nz eq 1) then begin
        d(*,*,*)=0.0
        return,d
      endif
	  if (N_ELEMENTS(z) NE nz) then z=findgen(nz)
      zder0z,nz,z
;
;  First point
;
      d(*,*,0)=(f(*,*,1)-f(*,*,0))/(z(1)-z(0))
;
;  Interior points [2m+2s = 4 flop]
;
      for k=1,nz-2 do begin
        a=1./(z(k+1)-z(k))^2
        b=1./(z(k)-z(k-1))^2
        d(*,*,k)=(f(*,*,k+1)-f(*,*,k))*a+(f(*,*,k)-f(*,*,k-1))*b
      end
;
;  Last point
;
      d(*,*,nz-1)=(f(*,*,nz-1)-f(*,*,nz-2))/(z(nz-1)-z(nz-2))
;
;  Do the forward substitution [1m+1a = 2 flop]
;
      for k=1,nz-1 do begin
        d(*,*,k)=d(*,*,k)+w1(k)*d(*,*,k-1)
      end
;
;  Do the backward substitution [1m+1d+1s = 3 flop]
;
      d(*,*,nz-1)=d(*,*,nz-1)*w2(nz-1)
      for k=nz-2,0,-1 do begin
        d(*,*,k)=(d(*,*,k)-w3(k)*d(*,*,k+1))*w2(k)
      end
;
      return,d
	end
