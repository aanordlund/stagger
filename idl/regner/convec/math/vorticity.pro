function vorticity, it, uy, uz, VECTOR=VECTOR
;   print, 'Nit: ', N_ELEMENTS(it)
	if (N_ELEMENTS(it) GT 1) then begin
		ux = it
	endif else begin
		ux = fux(it) & uy = fuy(it) & uz = fuz(it)
	endelse
	vortx = yder(uz) - zder(uy)
	vorty = zder(ux) - xder(uz)
	vortz = xder(uy) - yder(ux)
    if keyword_set(VECTOR) then begin
        it = vortx
        uy = vorty
        uz = vortz
    endif
    return, sqrt(vortx*vortx + vorty*vorty + vortz*vortz)
end
