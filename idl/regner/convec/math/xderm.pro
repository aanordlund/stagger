;
;   Function to calculate monotone cubic derivatives, according to
;     Fritsch & Butland, 1984, SIAM J. Sci. Stat. Comput., 5, 300
;   This function uses cyclic boundary conditions.
;   It is 1.7 times faster than the cubic spline interpolation of xder,
;   and 9.2 times faster than xderm.pro.bak1 with explicit y- and z-loops.
;   Timing: 1.06s for 150x150x82 array on a 1.2GHz UltraSPARC-III+.
;
;   Update history:
;   ---------------
;   27.05.2004 Coded/RT
;   27.05.2004 Changed to 1D d-arrays/RT
;   09.10.2006 Checks for dimensions - now independant of simulation/RT
;   09.10.2006 Implicit y- and z-loops - much faster!/RT
;
function xderm, f
	common cdat,x,y,z,nx,ny,nz,nw,ntmax,date0,time0

	sf = size(f)
	if (sf(0) NE 3) then begin
		print, " ERROR: This function only works for the 3D case. Bailing out!"
		stop
	endif
	if (nx LT 2) then return, 0*f
	x0 = x  &  nx0 = nx
	if (sf(1) LT nx) then begin
		nx = sf(1)
		x  = x(lindgen(nx)*(nx0/nx))
	endif
	if (sf(1) GT nx) then begin
		print, " ERROR: Cannot yet handle x-dims greater than in simulation."
		print, "        Bailing out!"
		stop, "nx, nx0: ", nx, nx0
	endif
	my = sf(2) & mz=sf(3)

	cx = 1.0/(x(1)-x(0))
	d  = 0*x
	dd = fltarr(nx, my*mz)
	d1 = (f(0,*,*)-f(nx-1,*,*))*cx
	for l=0, nx-1 do begin
		d0 = d1
		l1 = (l+1) mod nx
		d1 = (f(l1,*,*)-f(l,*,*))*cx
		d2 = d1*d0*2.0
		ii = where(d2 GT 1e-20, COMPLEMENT=i0)
		if (ii(0) GE 0L) then dd(l,ii)=d2(ii)/(d1(ii) + d0(ii))
		if (i0(0) GE 0L) then dd(l,i0)=0.0
	endfor
	dd = reform(dd, nx, my, mz)
;
;  Restore x to it's former glory
	if (nx NE nx0) then begin
		x = x0  &  nx = nx0
	endif
	return, dd
end
