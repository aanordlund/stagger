;***********************************************************************
      FUNCTION intrp3,z,zt,f,EXTRAPOLATE=EXTRAPOLATE, MCUBIC=MCUBIC, STOP=STOP
;
;  Interpolate to a new z-grid.  3D version.
;
;	z	: old grid
;	zt	: new grid
;	f	: old 3d field
;	returns	: new 3d field
;
;  Update history:
;
;  08-mar-88/bob:   1D version
;  01-dec-88/bob:   idl proceedure
;  02-dec-88/aake:  Simplified where loop
;
;***********************************************************************
;
      nz=n_elements(z)
      nt=n_elements(zt)
      mz=n_elements(f)
;
;  Get derivatives of f
;
;     d=zderz(z,f)
      if KEYWORD_SET(MCUBIC) then  d=zderm(f)  else  d=zder(f)
;     print,'f=',f
;     print,'z=',z
;     print,'d=',d
;
;  Find n such that  z(n-1) < zt(k) < z(n)
;
      n=1
      for k=0,nt-1 do begin
        n1=where(z gt zt(k),count)
	  if count gt 0 then n=[n,n1(0)] else n=[n,nz-1]
      endfor
      n=n(1:*)>1
;     print,'n=',n
      nm1=n-1
;     print,nm1
;
;  Relative distances
;  Set to constant outside
;
      dz=z(n)-z(nm1)
;     print,'dz=',dz
      pz=(zt-z(nm1))/dz
;
;  Do not extrapolate
;
      if n_elements(extrapolate) eq 0 then pz=((pz<1)>0)
;     print,'pz=',pz
;
;  Make 3D
;
      s=size(f)
      s(3)=nt
      g=make_array(size=s, /NOZERO)
      qz=1.-pz
;
;  Interpolate
;
      pzf=pz-(qz*pz)*(qz-pz)
      pzd=-pz*(qz*pz)*dz
      qzd=qz*(qz*pz)*dz
      for i=0,nt-1 do g(*,*,i) = $
        f(*,*,nm1(i))+pzf(i)*(f(*,*,n(i))-f(*,*,nm1(i)))+qzd(i)*d(*,*,nm1(i))+pzd(i)*d(*,*,n(i))
;     print,'g=',g
;
;  Restore
;
      if KEYWORD_SET(STOP) then stop
      return,g
      end
