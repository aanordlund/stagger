;
;  Program to calculate S, the specific entropy per mass, and replace one of
;  the entries, iS, with S in the lookup table.
;  Entropy is calculated by integrating  dS/dlnr=-Pg/(rho*T)  with respect to
;  lnr along iso-chores. Between iso-chores we integrate (dS/dee)_rho=1/T.
;  
;
;   Update history:
;   ---------------
;   ??-???-91: Coded/AAke
;   21-jan-96: Changed to replace pseudo Planck-function with entropy/RT
;   14-may-04: Replaces ln(kappa*rho) or B_0 depending on number of
;              entries in the table - returns the entry index in iS/RT
;   26-nov-08: Now also handles the old EOSrhoe.tab format - here called NEW.
;              Avoiding "boundary layers" when computing <dS(dlnrho)>, by means
;              of mrg. Changed to cubic integration in the lnrho-direction too.
;              Removed re-setting of dssdee to linear derivatives./RT
;   11-apr-12: Can now handle the new EOSrhoe.tab format, by calling eosstab./RT
;
;   Known bugs: None
;
pro sstab, iS
    common ctable,ttmean,rhm,drhm,eemin,eemax,dee,mtab,itab,tab
    common currenttable, EOSrhoeLoaded, kaprhoTLoaded

    if (EOSrhoeLoaded) then begin
        print, "Using eosstab on an EOSrhoe.tab-table."
        eosstab, iS
        return
    endif
    nz=n_elements(rhm)
    ;plot_oi,rhm,/nodata,[-.001,.0005],ytit='S',xtit='rho',yst=1

	NEW     = (strpos((fstat(2)).name, 'EOSrhoe') GE 0L)
	ntabvar = (itab(1)-itab(0))/mtab(0)/3
	if (ntabvar LT 4L) then begin
		iS = 1L < (ntabvar-1L)
		print, iS, form='("  Warning: Entropy put in table-slot #",I0)'
	endif else begin
		iS = 3L
	endelse
;	print, 'sstab: ', NEW, iS
    dlnr=alog(rhm(nz-1)/rhm(0))/(nz-1)
	mrg = 0.25
	fmrg1 = (1.-2.*mrg)
	fmrg2 = .5*mrg/(1.-2.*mrg)
;	print, fmrg1, fmrg2
;
; Integrate with respect to ee along iso-chores.
    for nr=0,nz-1 do begin
      ntab=mtab(nr)
      ltab=3*ntab
      kpg=itab(nr)-1+indgen(ntab)
      ktt=kpg+ 2*ltab
      kss=kpg+iS*ltab
      tt    =tab(ktt)
      dttdee=tab(ktt+ntab)
	  dlpdlr=tab(kpg+ntab*2)
	  if (NEW) then begin
; If new format - integrate with respect to lnee.
	    tt = exp(tt)
		ee = exp(eemin(nr) + findgen(ntab)*dee(nr))
		dttdee=tt*dttdee;				dT/dlnee
	  endif
      pg=exp(tab(kpg))
      dssdlnr=-pg/rhm(nr)/tt
	  d2ssdlnr2 = dssdlnr*(1.-dlpdlr)
      ss=fltarr(ntab)
	  if (NEW) then begin
		  dssdee=ee/tt;					dS/dlnee
		  d2ssdee2=dssdee-dttdee/tt^2;	d2S/dlnee2   and  dee is dlnee
	  endif else begin
		  dssdee=1./tt;					dS/dee
		  d2ssdee2=-dttdee/tt^2;		d2S/dee2
	  endelse
      dss=dee(nr)*(0.5*(dssdee+dssdee(1:*))+(d2ssdee2-d2ssdee2(1:*))/12.)

      if (nr GT 0) then begin
        for k=1,ntab-1 do ss(k)=ss(k-1)+dss(k-1)
		if eemin(nr) gt eemin(nr-1) then begin
		  k1=long((eemin(nr)-eemin(nr-1))/dee(nr)+.5)
		  Ni = long(fmrg1*min([mtab(nr-1L)-k1, ntab]))
		  i  = long(fmrg2*Ni+.5) + lindgen(Ni)
		  ss=ss+aver(ssp(k1+i)-ss(i)+ dlnr*($
		   0.5*(dssdlnrp(k1+i)+dssdlnr(i))+(d2ssdlnr2p(k1+i)-d2ssdlnr2(i))/12.))
		end else begin
		  k1=long((eemin(nr-1)-eemin(nr))/dee(nr)+.5)
		  Ni = long(fmrg1*min([mtab(nr-1L), ntab-k1]))
		  i  = long(fmrg2*Ni+.5) + lindgen(Ni)
		  ss=ss+aver(ssp(i)-ss(k1+i)+dlnr*($
		   0.5*(dssdlnrp(i)+dssdlnr(k1+i))+(d2ssdlnr2p(i)-d2ssdlnr2(k1+i))/12.))
		end
      end
;
;  For the next iso-chore, ssp becomes S of the previous iso-chore.
      ssp=ss
      dssdlnrp   = dssdlnr
	  d2ssdlnr2p = d2ssdlnr2

      tab(kss)=ss
;     dssdee=(ss(2:*)-ss)/2.
;     dssdee=[2.*dssdee(0)-dssdee(1),dssdee,2.*dssdee(ntab-3)-dssdee(ntab-4)]
      tab(kss+ntab)=dssdee*dee(nr)
      tab(kss+2*ntab)=dssdlnr*dlnr

      ;oplot,replicate(rhm(nr),ntab),ss,psym=3
    end
end
