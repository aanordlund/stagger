;
;  Function to compute the mean-molecular weight, based on lnrho and T.
;  The mean molecular weight for a perfect gas is exactly:
;      mu_ml = - (d ln \rho / d ln T )_P
;  But for an ionizing ideal gas unwanted terms enter, and
;      mu_ml = rho*k*T / (p - p_rad)
;  is more accurate and is used below.
;  This still doesn't work for non-ideal or partially degenerate gasses.
;
FUNCTION fmy,lr,ee,tt=tt,dlrdlp_tt=dlrdlp_tt

  common ceostab,Netab,Nrtab,Ma,Na,Ntbvar,Nelem,Nxopt,Nxrev,$
         date,time,lnrmin,lnrmax,lnemin,lnemax,ul,ur,ut,iel,abund,arr,tab
  common currenttable, EOSrhoeLoaded, kaprhoTLoaded

  if (EOSrhoeLoaded) then begin
    uur=ur    & uup=ur*(ul/ut)^2
  endif else begin
    uur=1e-7  & uup=1e5
  endelse
  pp=exp(lookup(lr,ee,iv=0))*uup
  tt=lookup(lr,ee,iv=2)
;  These values of the physical constants are identical to the
;  ones used in the (original) MHD EOS.
  a_3 = 1d0*2.5219717d-15 & Mu = 1.6605400d-24 & k = 1.3806580d-16
  return, exp(lr)*uur/(pp/tt-a_3*tt^3)/Mu*k
END
