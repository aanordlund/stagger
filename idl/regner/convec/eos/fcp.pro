FUNCTION fcp,lr,ee,it=it
	common thermoquan,dlpdee,dlpdlr,P_rho,tt,dttdee,dttdlr
;
;  C_p = (d/dT)_P [ E + P/rho ] = 1/(dT/dE)_P - P/rho (dlnrho/dT)_P
;
	if (N_ELEMENTS(lpdee) NE N_ELEMENTS(lr)) then begin
	  	lp=lookup(lr,ee,dlpdee,dlpdlr,iv=0)
		P_rho = exp(lp - lr)
	endif
	if (N_ELEMENTS(tt) NE N_ELEMENTS(lr)) then begin
	  	tt=lookup(lr,ee,dttdee,dttdlr,iv=2,status=status)
        if (status GT 0L) then begin
          ii = where(ee GT 14.5 AND lr LT -8.)
          if (ii(0) GE 0L) then begin
            default, it, 0L
            tt(ii) = (ftt(it))(ii)
            dttdee(ii) = 8./30.*tt(ii)/ee(ii);   from t41g15m+0014(10).sav.
            dttdlr(ii) = 5./20.*tt(ii)
          endif else begin
            print, "WARNING: Extrapolation in fcp - but not at top!"
          endelse
        endif
    endif
	dlrdee_lp = -dlpdee/dlpdlr
	dttdee_lp = dttdee + dttdlr*dlrdee_lp
	cp = (1. - P_rho*dlrdee_lp)/dttdee_lp
	return,cp
END
