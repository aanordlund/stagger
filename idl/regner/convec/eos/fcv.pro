FUNCTION fcv,lr,ee
    common thermoquan,dlpdee,dlpdlr,P_rho,tt,dttdee,dttdlr
;
;  C_p = (d/dT)_P [ E + P/rho ] = 1/(dT/dE)_P - P/rho (dlnrho/dE)_P
;
	if (N_ELEMENTS(tt) NE N_ELEMENTS(lr)) then $
		tt=lookup(lr,ee,dttdee,iv=2)
	cv = 1./dttdee
	return,cv
END
