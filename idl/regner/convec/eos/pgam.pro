;
;   Function to calculate pressure, its derivatives w.r.t. lnr and ee, and
;   associated thermodynamic derivatives, gam1, gam3 and adiabatic sound speed.
;
;   Update history:
;   ---------------
;   15.10.1997 Coded/RT
;   03.11.2015 Added check for extrapolation in EOS table, and corrections./RT
;
;   Known bugs: None
;
FUNCTION pgam,lr,ee,gam1=gam1,gam3=gam3,cs=cs,status=status,QUIET=QUIET
	common thermoquan,dlpdee,dlpdlr,P_rho,tt,dttdee,dttdlr
;
;  \Gamma_3 = ( \partial P / \partial E ) _{\rho} / \rho
;
	lnP=lookup(lr,ee,dlpdee,dlpdlr,iv=0,status=status)
    if (status GT 0L) then begin
      ii = where(ee GT 14.5 AND lr LT -8.)
      if (ii(0) GE 0L) then begin
        ee0 = 8.15;   E = rho*(ee+ee0) = 2./3.*p. ee0 from t41g15m+0014(10).sav.
        lnP(ii) = alog(2./3.*(ee(ii)-8.15))+lr(ii)
        dlpdee(ii) = 1./(ee0+ee(ii))
        dlpdlr(ii) = 1.
      endif else begin
        print, "WARNING: Extrapolation in pgam - but not at top!"
      endelse
    endif
	if (NOT KEYWORD_SET(QUIET)) then print, 'Pgam...', N_ELEMENTS(dlpdee)
	P_rho = exp(lnP - lr)
	gamtmp = P_rho*dlpdee;											Correct/RT
	gam3 = gamtmp + 1.;												Correct/RT

	gam1 = dlpdlr + gamtmp

;dE = P/rho*dlnrho for S=const, so
;gamma = (dlnpp/dlnrho)_E + P/rho (dlnpp/dE)_{lnrho}.
	 
	cs = sqrt(gam1*P_rho)
	tt = 0.0
	return, lnP
end
