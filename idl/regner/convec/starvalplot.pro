;
;   Program to visualize the content of starval.tab
;
;   Update history:
;   ---------------
;   15.10.2006 Coded/RT
;   24.10.2006 Added keywords TV and QUINTIC/RT
;   24.10.2006 Automatic calculation of default \phi-angle :-) /RT
;   14.03.2007 Added keyword TLOG - default is now to plot linear Teff
;              on a logarithmic scale/RT
;   18.04.2008 Added arguments 'file' and 'ext', to allow plotting of
;              starval.tabs other than the current one. Added 'zrange'/RT
;   28.04.2008 'zrange' now also work on TV-style plots/RT
;   05.05.2008 Added new parameters; 'dadq' and 'ddczdq' that combine
;              numbers from two different 'starval.tab'-files/RT
;   05.12.2008 Added keyword, NAMES, to annotate the simulation points/RT
;   12.04.2009 Added argument 'device', for plotting to e.g., PS-files/RT
;   14.04.2009 Added keyword, Li, for plotting the limit of Li and Be
;              depletion. Added vars; dqsun (diff btw using indiv. and Solar
;              T-tau), dqks (diff btw using indiv. and Krishna Swamy T-tau)
;              and TbCZ (T at bottom of CZ)./RT
;   24.07.2012 New keyword BLANKOUT determines whether to blankout plot
;              behind labels. New arguments labels, defaults to name/RT
;   27.11.2013 Corrected labels for \ln\kappa_atm changes/RT
;   04.02.2014 Changed to use simtriangulate for the triangulation, to ensure
;              that the triangles are aligned with the structures, instead
;              of across them. Added keyword RAW to use the old version.
;              Also added option for minimum curvature surface w/MINCURVESURF/RT
;   14.10.2015 Added keyword, STAGGERGRID, to overplot its outline/RT
;   02.08.2016 Now add 'lg' to start of vstr if /LOG is set./RT
;   02.08.2016 Added keyword, CLOG, to use a logarithmic color-scale. In this
;              case, the color-table is set with 'color, /EQUAL' to avoid
;              color-break at ff=1.0 ot logff=0.0./RT
;   17.08.2016 Fixed handling of device='passthrough'./RT
;   05.10.2017 Added possibility to apply the variable via the argument list;
;              use vstr='', and supply  ff=ff, fvartitle=fvartitle  /RT
;   07.11.2019 Added labeling of plots, a color_scale, and keyword NOREVERT./RT
;
;   Known bugs: The combo 'P1', /LOG  results in wierd az/RT
;               Cannot set x- or y-ranges in the plots/RT
;
;   To do: Add plotting of PMS eclipsing binaries from {stassun:13PMS-EBs.Tab1}.
;
pro starvalplot, vstr, device, zrange=zrange, az=az, ax=ax, mixis=mixis, $
		colortable=colortable, file=file, ext=ext, labels=labels, $
        trackcol=trackcol, ff=ff, fvartitle=fvartitle, NAMES=NAMES, $
		TLOG=TLOG, LOG=LOG, TV=TV, QUINTIC=QUINTIC, EVOLTRACKS=EVOLTRACKS,$
		Li=Li, NOLOAD=NOLOAD, SUNVAL=SUNVAL, BLANKOUT=BLANKOUT, MPLOT=MPLOT, $
        RAW=RAW, MINCURVESURF=MINCURVESURF, MNRAS=MNRAS, WHITE=WHITE, $
        NOPMS=NOPMS, NOCLIP=NOCLIP, STAGGERGRID=STAGGERGRID, GRID2013=GRID2013,$
        CLOG=CLOG, REDUCED_TRACKS=REDUCED_TRACKS, NOREVERT=NOREVERT, $
        _EXTRA=extra_args, STOP=STOP

@cstarval, MNRAS=MNRAS
@consts
    common colors, r_orig, g_orig, b_orig, r_curr, g_curr, b_curr

	default, ax, 30.
    default, mixis, 'AG89Fe'
	default, file, 'starval.tab'
	default, ext, ''
	if (ext NE ''  AND  strmid(ext, 0, 1) NE '.') then ext = '.'+ext
	file = file + ext
	dxt  = ''
    if (N_ELEMENTS(labels) GT 0L) then NAMES=1b
    if KEYWORD_SET(CLOG)          then LOG  =1b

	case strlowcase(vstr) of
		'contrast':	begin&  ivar = 100L             &  end
		'sjump':	begin&  ivar = 101L             &  end
		'dadq':		begin&  ivar = 102L&  dxt='.dqa'&  end
		'ddczdqa':	begin&  ivar = 103L&  dxt='.dqa'&  end
		'ddczdq':	begin&  ivar = 104L&  dxt='.dq' &  end
		'dadk':		begin&  ivar = 105L&  dxt='.dka'&  end
		'ddczdka':	begin&  ivar = 106L&  dxt='.dka'&  end
		'ddczdk':	begin&  ivar = 107L&  dxt='.dk' &  end
		'ddczda':	begin&  ivar = 108L&  dxt='.da'	&  end
		'dqsun':	begin&  ivar = 109L&  dxt='.sunTtau'&  end
		'dqks':		begin&  ivar = 110L&  dxt='.KS'	&  end
		'tbcz':		begin&  ivar = 111L				&  end
		'dq5000':	begin&  ivar = 112L&  dxt='.5000'& end
		'a_hm':     begin&  ivar = 113L             &  end
        'ngran':    begin&  ivar = 114L             &  end
        'a0':       begin&  ivar = 115L             &  end
        'naked':    begin&  ivar = 116L             &  end
        'dteff':    begin&  ivar = 117L             &  end
		'malfa':    begin&  ivar =  10L             &  end
        'dadqval':  begin&  ivar = 118L&  dxt='.JCD-VALC'& end
        'dasun':    begin&  ivar = 119L&  dxt='.dasun'&end
        'da1d':     begin&  ivar = 120L&  dxt='.MARCS'&end
		'dq1d':	    begin&  ivar = 121L&  dxt='.MARCS'&end
        'a+':       begin&  ivar = 122L             &  end
        'a-':       begin&  ivar = 123L             &  end
        'aptmx':    begin&  ivar = 124L             &  end
        'dnu-':     begin&  ivar = 125L             &  end
        'dnu+':     begin&  ivar = 126L             &  end
        'dnuampl':  begin&  ivar = 127L             &  end
        'dnubrk':   begin&  ivar = 128L             &  end
        'dnumax':   begin&  ivar = 129L             &  end
        'dnumaxnrm':begin&  ivar = 130L             &  end
        'cm1c3':    begin&  ivar = 131L             &  end
		else:		begin&  ivar =  -1L             &  end
	endcase
	if (dxt NE '') then begin
		dirs  = read_startab(file=file+dxt, resis='150x150x82', abundis=0.0,$
                            mixis=mixis, /ALFAFIT, _EXTRA=extra_args)
		alfa0 = alfa
		dCZ0  = dCZ
		dq    = 5e-3&  da=5e-3& dk=5e-3
	endif
    if (NOT KEYWORD_SET(NOLOAD)) then $
        dirs = read_startab(file=file, resis='150x150x82', abundis=0.0, $
                            mixis=mixis, /ALFAFIT, _EXTRA=extra_args)
	vname = strlowcase(scope_varname(common='starvaltab'))
	LTeff = alog10(Tefobs)
	Logg  = alog10(grav)
    iSun  = (where(name EQ 'sun'))(0L)

	if (ivar LT 0L) then begin
		ivar  = (where(strlowcase(vstr) EQ vname))(0)
		if (ivar LT 0L) then begin
            if (N_ELEMENTS(ff) EQ N_ELEMENTS(LTeff)) then begin
                default, fvartitle, 'Custom variable'
            endif else begin
                print,"ERROR: A variable named " + vstr + $
                            " is not contained in starval.tab"
                print,"       Bailing out!"& return
            endelse
		endif
	endif
	if (ivar GE 0L  AND  ivar LT 4L) then begin
		print,"ERROR: The variable "+vstr+" is a string and cannot be plotted."
		print,"       Bailing out!"& return
	endif
    if KEYWORD_SET(LOG) then vstr = 'lg' + vstr
;   print, ivar, '  ', vstr

    if (strlowcase(!D.NAME) NE 'x') then device='passthrough'
	default, device, 'x'
	device = strlowcase(device)
	if (device EQ 'x') then begin
		x0 = 0.25 & y0=0.55
		if KEYWORD_SET(TV) then cs=1.2  else  cs=1.8
;The overall charactersize in units of the default.
		!P.charsize  = cs*!P.charsize
		default, colortable, 27L
		if KEYWORD_SET(TV) then Nsrf=200L  else  Nsrf=50L
        wc0 = 0.25
		wc1 = 0.95; 0.475
	endif else begin
        EPS = (strpos(device,'eps') GE 0L)
		x0 = 0.25 & y0=0.565
		psdir = 'stars/150x150x82/
		hosts_dir, cnvdir=psdir
		cs = 0.8
        if KEYWORD_SET(WHITE) then cstr='w'  else  cstr=''
        if (strpos(device,'color') GE 0L  OR  $
                             device EQ 'passthrough') then begin
            cstr = 'c' + cstr
            if (device NE 'passthrough') then begin
                backup,         psdir+cstr+vstr+'.ps', /QUIET
                ca4,y=17.8,name=psdir+cstr+vstr+'.ps', ENCAPS=EPS
                thicker, 2., cs=cs
            endif
            default, colortable, 27L
            !P.color = (!D.TABLE_SIZE-1.);	*.95*0
;		    !P.background = (!D.TABLE_SIZE-1.)
            wc0 = 0.7
        endif else begin
            backup,             psdir+cstr+vstr+'.ps', /QUIET
            a4, y=17.8,name=    psdir+cstr+vstr+'.ps', ENCAPS=EPS
            default, colortable, 0L
            thicker, 2., cs=cs
            wc0 = 0.25
        endelse
		if KEYWORD_SET(TV) then Nsrf=500L  else  Nsrf=50L
		wc1 = 0.95;	*.495
	endelse
    loadct, 12
    col0 = (!D.TABLE_SIZE-1.)*wc0
    col1 = (!D.TABLE_SIZE-1.)*wc1
;col2 = (!D.TABLE_SIZE-1.)*0.15
    trackcol0 = (!D.TABLE_SIZE-1.)*0.60
;The xyouts charactersize in units of the overall size.
	cs           = 0.8*!P.charsize

	if (KEYWORD_SET(Li) OR ivar EQ 111) then begin
		TbCZ = 0*Rad &  close, 5
		hosts_dir, envdir=gdir
		for i=0L, N_ELEMENTS(Rad)-1L do begin
			read_ngong,5,hdata,nmod,nn,ndata,datmod,idatmd,datgng,bc,yvar,/QUI,$
								file=gdir+'gong.'+name(i)+'rf' & close, 5
			xx = reform(10^(yvar(1,*)+11d0))/datmod(23)
			bcz = depth_of_cz(reform(yvar(26,*)), xx=xx)
			TbCZ(i) = intrp1(xx, bcz, reform(yvar(3,*)),/EXTRA)
;			TbCZ(i) = intrp1(xx, bcz, reform(alog10(yvar(11,*))),/EXTRA); logkap
;			TbCZ(i) = intrp1(xx, bcz, reform(yvar(14,*)),/EXTRA); 		\nabl_ad
		endfor
	endif
    sgnch = 1.
	c = ')!d!7a!x!n'
; Solar value, \nu_max,sun = 3090+/-30 \muHz, from citet{huber:KplSolOscScaling}
; Instead using 'standard' solar value:
    numax = 3.100d0*grav/grav(iSun)/sqrt(Tefobs/Tefobs(iSun));  /[mHz]
    Nff   = N_ELEMENTS(numax)
;   if (ivar EQ 129) then begin
;       
;   endif
	case ivar of
        -1:  begin&                     & vartitl=fvartitle               &  end
;       20:  begin&  ff=Agran*grav^1    & vartitl=strvltitl(ivar)         &  end
;       28:  begin&  ff=Smax+3.0        & vartitl=strvltitl(ivar)         &  end
        39:  begin&  ff=LDfact+1.       & vartitl='!8f!7!Dh!x!n'          &  end
        51:  begin&  ff=KplLDfact+1.    & vartitl='!8f!7!Dh!x,Kpl!n'      &  end
        55:  begin&  ff=IRMS*1e2        & vartitl=strvltitl(ivar)+pb+'%'+pe& end
;   	100: begin&  ff=abs(I5-I2)/Iwh1 & vartitl='I-contrast'            &  end
    	100: begin&  ff=abs(I5-I2)      & vartitl='I-contrast'            &  end
		101: begin&  ff=Smax-Smin 		& vartitl='S!djump!n'+uS	      &  end
;       101: begin&  ff=Smax-Smin 		& vartitl='S!djump!n'+uS 	      &  end
		102: begin&  ff=(alfa0-alfa)/dq	& vartitl='!9D!7a!x/!9D!8q!x'     &  end
		103: begin&  ff=( dCZ0- dCZ)/dq	& vartitl='!9D!8d!x!dcz/!9D!8q!x' &  end
		104: begin&  ff=( dCZ0- dCZ)/dq	& vartitl='(!9D!8d!x!dcz!n/!9D!8q!x'+c&end
		105: begin&  ff=(alfa0-alfa)/dk	& vartitl='!9D!7a!x/!9D!xln!7j!x!datm!n'&		 end
		106: begin&  ff=( dCZ0- dCZ)/dk	& vartitl='!9D!8d!x!dcz/!9D!xln!7j!x!datm!n' &  end
		107: begin&  ff=( dCZ0- dCZ)/dk	& vartitl='(!9D!8d!x!dcz!n/!9D!xln!7j!x!datm!n'+c&end
		108: begin&  ff=( dCZ0- dCZ)/da	& vartitl='!9D!8d!x!dcz!n/!9D!7a!x'& end
		109: begin&  ff=( dCZ0- dCZ)	& vartitl='!7D!8d!x!dcz!n(!8q!d!9n!n!x-!8q!x)'& end
		110: begin&  ff=( dCZ0- dCZ)	& vartitl='!7D!8d!x!dcz!n(!8q!x!dKS!n-!8q!x)'& end
		111: begin&  ff=TbCZ			& vartitl='log!d10!n!8T!x!dcz!n'  &  end
;		111: begin&  ff=TbCZ			& vartitl='log!d10!n!7j!x!dcz!n'  &  end
;		111: begin&  ff=TbCZ			& vartitl='!9G!x!iad!N!x'	      &  end
		112: begin&  ff=( dCZ0- dCZ)	& vartitl='!7D!8d!x!dcz!n(!8q!x!d5000!n-!8q!x)'& end
		113: begin&  ff=A_Hp/alfa   	&  vartitl='!8A!x!dgran!n/!8H!d!xm!n'& end
		114: begin&  ff=4/!pi*(Rad*Rsun*1e-8/Agran)^2&vartitl='!8N!x!dgran!n'& end
        115: begin&  ff=-16.147+11.047*LTeff-6.927*Logg& vartitl='!8a!x!d0!n'& end
        116: begin&  ff=I6/Iwh1         & vartitl='!7D!8I!d!xup!n/<!8I!x>'& end
        117: begin&  ff=TefObs-TeffSpctr& vartitl='!7D!8T!d!xeff!n'+pb+'K'+pe&end
        118: begin&  ff=(alfa-alfa0)    & vartitl='!7Da!x(3D-VALC)'       &  end
        119: begin&  ff=( dCZ0- dCZ)    & vartitl='!7D!8d!x!dcz!n(!7a!d!9n!x!n-!7a!x)'       &  end
        120: begin&  ff=(alfa-alfa0)    & vartitl='!7Da!x(3D-1D)'         &  end
        121: begin&  ff=( dCZ0- dCZ)    & vartitl='!7D!8d!x!dcz!n(3D-1D)' &  end
        122: begin&  ff=PtPc*(PtPb-1.)  & vartitl='!8A!x!d+!n'            &  end
        123: begin&  ff=PtPc* PtPb      & vartitl='!8A!x!d-!n'            &  end
        124: begin&  ff=PtPc/(PtPd-Ptmax)&vartitl='!8a!xPtP'              &  end
        125: begin&  ff= dnu1*dnu2      & vartitl='!7Dm!x-'               &  end
        126: begin&  ff=(dnu1-1)*dnu2   & vartitl='!7Dm!x+'               &  end
        127: begin&  ff=exp(dnu3)/numax &vartitl='Ampl(!7Dm!x)/!7m!x!dmax!n'&end
        128: begin&  ff=exp(dnu4)/numax & vartitl='!7m!x(knee)/!7m!x!dmax!n'&end
        129: begin
                ff   = fltarr(Nff)
                for jsim=0L, Nff-1L do begin
                    xtwolinsegs, alog(numax(jsim)), [dnu0(jsim),dnu1(jsim),$
                                    dnu2(jsim),dnu3(jsim),dnu4(jsim)], fit01
                    ff(jsim) = fit01
                endfor
                vartitl = '!7dm!x(!7m!x!dmax!n)/!5[!7l!xHz!5]!x'
                if (NOT KEYWORD_SET(LOG)) then ff = -exp(ff) else sgnch=-1.
             end
        130: begin
                ff   = fltarr(Nff)
                for jsim=0L, Nff-1L do begin
                    xtwolinsegs, alog(numax(jsim)), [dnu0(jsim),dnu1(jsim),$
                                    dnu2(jsim),dnu3(jsim),dnu4(jsim)], fit01
                    ff(jsim) = fit01 - alog(1d3*numax(jsim))
                    if (name(jsim) EQ 't57g30m+00') then print,jsim,fit01,numax(jsim)
                endfor
                vartitl = '!7dm!x(!7m!x!dmax!n)/!7m!x!dmax!n'
                if (NOT KEYWORD_SET(LOG)) then ff = -exp(ff) else sgnch=-1.
             end
        131: begin&  ff=cm1/c3/0.6^4   & vartitl='!8c!x!d-1!n/!8c!x!d3!n(!7m!x!dmax!n)'&  end
		else: begin
				ff = scope_varfetch(ivar,    common='starvaltab') 
				vartitl = strvltitl(ivar)
			  end
	endcase
    if (ivar EQ 40) then ff=ff*Rad^0
; Intensity contrast of the granulation
	ii    = lindgen(N_ELEMENTS(ff))
	if KEYWORD_SET(LOG) then begin
        if (strlowcase(vstr) EQ 'lgsmax') then ff=ff+4.
        if (strlowcase(vstr) EQ 'lgsmin') then ff=ff+5.
        if (strlowcase(vstr) EQ 'lgcm1')  then begin& ff=-ff& sgnch=-1.& end
        if (strlowcase(vstr) EQ 'lgc3')   then begin& ff=-ff& sgnch=-1.& end
		if (ivar EQ 129  OR  ivar EQ 130) then begin
            ff      = ff/alog(1e1)
        endif else begin
            ips     = where(ff GT 0.0)
            ff(ips) = alog10(ff(ips))
        endelse
        if (NOT KEYWORD_SET(CLOG)) then begin
            if (sgnch LT 0.0) then  vartitl = '-'+vartitl
            if KEYWORD_SET(MNRAS) then begin
                vartitl = 'log!d10!n!5[!x'+vartitl+'!5]!x'
            endif else begin
                vartitl = 'log!d10!n('+vartitl+')'
            endelse
        endif
	endif
; Possible sub-set of simulations
    if (0) then begin
        jj = where(Tefobs(ii) GT 5280.  AND  alog10(grav(ii)) GT 3.65)
        ff = ff(jj)
        ii = ii(jj)
    endif
	fmin = min(ff, max=fmax)
;
; Do this in order to avoid including the bacground color (color 0-1) in
; the TV.
	ff0 =(!D.TABLE_SIZE-1.)/(3.-!D.TABLE_SIZE)*(2./(!D.TABLE_SIZE-1.)*fmax-fmin)
	default, zrange, [ff0, fmax]
    ff   = ((ff>zrange(0))<zrange(1))
	fmin = min(ff, max=fmax)
    ff0  = zrange(0L)
	print, "min(f): ", ff0, fmin, fmax

;	Tefobs= scope_varfetch('Tefobs',common='starvaltab')
;	grav  = scope_varfetch('grav',  common='starvaltab')
	Nf    = N_ELEMENTS(ff)
	if (Nf LT 3L) then begin
		print, "ERROR: Can't plot less than 3 simulations. Bailing out!"
		return
	endif
;	stop
	LTeff = alog10(Tefobs(ii))
	Logg  = alog10(grav(ii))
	name  = name(ii)
	ii = where(name NE 'sun', COMPL=iSun) & iSun=iSun(0)
    Nii=N_ELEMENTS(ii)-(ii(0) LT 0)
	dLTe  = (max(LTeff, min=minLT) - minLT)/(Nsrf-1.0001)
	dLg   = (max(Logg,  min=minLg) - minLg)/(Nsrf-1.0001)
	LTe   = minLT + findgen(Nsrf)*dLTe
	Lgg   = minLg + findgen(Nsrf)*dLg 
;
; Construct regularely gridded surface
    if KEYWORD_SET(MINCURVESURF) then begin
; Construct minimal curvature surface
        surf = (min_curve_surf(ff,LTeff*8,Logg,xout=LTe*8,yout=Lgg)>ff0)<fmax
    endif else begin
; Perform triangulation
        simtriangulate, TR, RAW=RAW
        surf = trigrid(LTeff,Logg,ff,TR,[dLTe,dLg],missing=ff0,QUINTIC=QUINTIC)
    endelse
	if KEYWORD_SET(TLOG) then begin
        if KEYWORD_SET(MNRAS) then begin
            !x.title='log!d10!n!5[!x'+strvltitl(5)+'!5]!x'
        endif else begin
            !x.title='log!d10!n('+strvltitl(5)+')'
        endelse
		xx      =     LTeff
		xrange  =     [max(LTeff), minLT]
		xlog    = 0
	endif else begin
		!x.title= strvltitl(5)
		xx      = 1e1^LTeff
		xrange  = 1e1^[max(LTeff), minLT]
		LTe     = 1e1^LTe
		xlog    = 1
        Ttickv=[5e3] & Nticks=0L & dTtick=1e3
        while ((Ttickv(Nticks)+dTtick) LE xrange(0)) do begin
            Ttickv = [Ttickv, Ttickv(Nticks)+dTtick]
            Nticks = Nticks + 1L
        endwhile
        while ((Ttickv(0)-dTtick) GE xrange(1)) do begin
            Ttickv = [Ttickv(0)-dTtick, Ttickv]
            Nticks = Nticks + 1L
        endwhile
;       print, Ttickv, Nticks
        xm     = 1e2
        iminor = ceil(xrange/xm-1e-1) & is=sort(iminor)
	endelse
    if KEYWORD_SET(MNRAS) then begin
        !y.title='log!d10!n!5[!x'+strvltitl(6)+'!5]!x'
        REDUCED_TRACKS = 1b
	endif else begin
        !y.title='log!d10!n('+strvltitl(6)+')'
	endelse

    if KEYWORD_SET(SUNVAL) then begin
        dum  = min(abs(LTe-     TefObs(iSun)) ,iT)
        dum  = min(abs(Lgg-alog10(grav(iSun))),ig)
        fSun = intrp1(LTe,     TefObs(iSun), reform(surf(*,ig)),h=h,/MCUB)
        fSun = intrp1(Lgg,alog10(grav(iSun)),reform(surf(iT,*)),h=g,/MCUB)
        print, "      f(Sun)       df/dT       df/dlogg"
        print, ff(iSun), h(0), g(0)
    endif
	if KEYWORD_SET(TV) then begin
        if KEYWORD_SET(MPLOT) then begin
            ipl =  !P.multi(0)
            Npx = (!P.multi(1))>1L
            Npy = (!P.multi(2))>1L
            ipx = ipl mod Npx
            ipy = Npy-1L - ipl/Npx
            blanks = replicate(" ", 30)
; Left column
            if (ipx EQ 0) then begin
                !y.tickname=""
                print, 'Mplot: Left column'
; Other columns
            endif else begin
                !y.tickname=blanks
                !y.title=""
                print, 'Mplot: Other columns'
            endelse
; Bottom row
            if (ipy EQ 0) then begin
                !x.tickname=""
                print, 'Mplot: Bottom row'
; Other rows
            endif else begin
                !x.tickname=blanks
                !x.title=""
                print, 'Mplot: Other rows'
            endelse
            ix0 = 0.09
            iy0 = 0.075
            imwidth = (1.-1.14*ix0)/Npx
            imheight= (1.-1.15*iy0)/Npy
            ix  = ix0 + ipx*imwidth
            iy  = iy0 + ipy*imheight
            if (ipl EQ 0) then erase
        endif else begin
            Npx = 1L & Npy=1L
            ix0 = 0.13
            iy0 = 0.11
            imwidth = 1.-1.2*ix0; -3.0*ix0
            imheight= 1.-1.2*iy0; -4.0*iy0
            ix  = ix0
            iy  = iy0
            erase
        endelse
        eRGB=float($
            [r_curr(!P.background),g_curr(!P.background),b_curr(!P.background)])
;
; Find the current color-table index and load the TVCOLORTABLE
		icurr = currentct(/QUIET, ntables=ntables)
		print, icurr
		colortable = (colortable>0);    <(ntables-1L)
;
; Have to load the color-table, even if it is unchanged, since Ncol changes.
        if (colortable LT 41L) then loadct, colortable, /SILENT  $
                               else color,zrange,/BAR,/NOBLAC,EQUAL=(vstr EQ 'Smax'  OR  KEYWORD_SET(CLOG)),/LIN
; Change the bottom color to white and the top-most color, Ncols-1L, to black.
        tvlct, R, G, B, /GET
        Ncols = N_ELEMENTS(R)
        R(0L) = 255&    R(Ncols-1L) = 0
        G(0L) = 255&    G(Ncols-1L) = 0
        B(0L) = 255&    B(Ncols-1L) = 0
        tvlct, R, G, B
        dum = max(R*1d0+G*1d0+B*1d0, iwht)
        dum = min(R*1d0+G*1d0+B*1d0, iblck)
        if KEYWORD_SET(WHITE) then simcol=iwht  else  simcol=iblck
        simRGB = float([R(simcol), G(simcol), B(simcol)])
;
; Do it this way, to also have zrange affect TV-style plots.
;       print, ix, iy, imheight, imwidth
		img_plot, (rotate(surf, 2)-zrange(0))/(zrange(1)-zrange(0))*$
            (!D.TABLE_SIZE-1.), ix, iy, height=imheight, width=imwidth, marg=0
;       print, ix, iy, imheight, imwidth
		if KEYWORD_SET(TLOG) then begin
			plot, xx, Logg, th=2, /NOERASE, ysty=1, $
				xr=xrange, yr=[max(Logg), minLg], /NODATA
		endif else begin
			plot, xx, Logg, th=2, /NOERASE, ysty=1, /XLOG, $
				xtickv=Ttickv, xticks=Nticks, xminor=0, $
				xr=xrange, yr=[max(Logg), minLg], /NODATA
			ydiff  = !y.crange(1)-!y.crange(0)
			for i=iminor(is(0)), iminor(is(1)) do begin
				plots,[i,i]*xm,!y.crange(0)+[0,!P.ticklen/2]*ydiff
				plots,[i,i]*xm,!y.crange(1)-[0,!P.ticklen/2]*ydiff
			endfor
		endelse
        if KEYWORD_SET(STAGGERGRID) then $
            plots,[7000,5500,5500,4500,4500,4000,4000,4500,5500,5500,7000], $
                  [4.5, 4.5, 5.0, 5.0,  2.5, 2.5, 1.5, 1.5, 2.5, 3.0, 4.5]  
        if KEYWORD_SET(GRID2013) then begin
            iG13=[16,28,5,10,31,27]
            plots, xx(iG13), logg(iG13), li=2, col=0
        endif

		!P.COLOR = (!D.TABLE_SIZE-1.)
;
; Find and overplot the files of stellar evolution
		if KEYWORD_SET(EVOLTRACKS) then begin
;			!p.thick = 3& !P.charthick = !p.thick
            if (colortable EQ 27L) then begin
                trackcol0 = col1
            endif else begin
                dum = min(sqrt((r_curr-118.)^2+(g_curr-0.)^2+(b_curr-0.)^2), $
                                                                    trackcol0)
            endelse
            default, trackcol, trackcol0
;           print, "trackcol: ", trackcol
            if KEYWORD_SET(REDUCED_TRACKS) then begin
                gmask = 'M{0.??,1.00,1.10,1.20,1.30,1.40,1.60,1.80,2.00,2.20,2.40,2.60,3.??,4.??}/star.log'
            endif else begin
                gmask = 'M?.??/star.log'
            endelse
			track_oplot, gmask, /GRAVPLOT, LOGT=TLOG, /MESAEVOL, th=1, $
                                        col=trackcol, NOPMS=NOPMS, NOCLIP=NOCLIP

;           achr = [1d9, 8d9,10d9,12d9];   [1.4d8,2d8]; ,5d8,2d9,8d9]
;           mesa_isochrones,/NOPL,tiso=achr,Tfiso=Tchr,lgiso=lgchr,R=Rchr,M=Mchr
;           help, achr, Tchr, lgchr, Rchr, Mchr

;			!p.thick = 1& !P.charthick = !p.thick
;			track_oplot, /GRAVPLOT, LOGT=TLOG,col=0
		endif
		if KEYWORD_SET(Li) then begin
			Tsurf = trigrid(LTeff,Logg,TbCZ,TR,[dLTe,dLg],missing=min(TbCZ),$
							xgrid=xgrid, ygrid=ygrid, QUINTIC=QUINTIC)
;                            Overplot the depletion lines of Li  Be and B
			contour, Tsurf, 1e1^xgrid, ygrid, levels=alog10([2.5,3.5, 5.0])+6.,$
									/OVERPLOT, c_line=[2, 5];, th=2
		endif
        mx = 4950.& if KEYWORD_SET(TLOG) then mx=alog10(mx)
        mlg= aver(!y.crange)
        simsyms = 1./sqrt(max([Npx,Npy]))&  fcsim =1.0
        if (sqrt(total((simRGB-eRGB)^2)) LT 10.) then fcsim=1.0-simsyms*0.01
		plots, (xx(ii)-mx)*fcsim^2+mx, (Logg(ii)-mlg)*fcsim+mlg, psym=2, /T3D, $
            thick=1.3*(!P.thick>1.), symsize=simsyms, col=simcol
		if (iSun GE 0L) then begin
			dSuny = convert_coord([0,0], [0,!P.charsize*!D.Y_CH_SIZE*0.4], $
									/DEVICE, /TO_DATA)
			xyouts, xx(iSun), Logg(iSun)-(dSuny(1,1)-dSuny(1,0)), '!9n!x', $
                    align=.5, charth=1.3*(!P.thick>1.), /DATA, col=simcol
		endif
		if KEYWORD_SET(NAMES) then begin
            default, labels, name
			if (max(strlen(labels)) GT 4) then angle=25  else  angle=0
; Make 'shadow'-effect
			dsx = (1e1^!x.crange(1)-1e1^!x.crange(0))*!D.x_px_cm/!D.x_size*0.18
			dsy =-(!y.crange(1)-!y.crange(0))*!D.y_px_cm/!D.y_size*0.24
;
; Various ways of blaking out the background in preparation for labelling
          if KEYWORD_SET(BLANKOUT) then begin
           if (0) then begin
;			dsx = (1e1^!x.crange(1)-1e1^!x.crange(0))*!D.x_px_cm/!D.x_size*0.03
;			dsy = (!y.crange(1)-!y.crange(0))*!D.y_px_cm/!D.y_size*0.017
;			xyouts, xx+dsx, Logg+dsy, labels, size=.8*cs, orient=angle, col=0, $
;													charthick=2*!P.charthick
            blanks = strarr(Nf)
            for i=0,Nf-1L do $
                blanks(i)='!20'+strjoin(replicate('b',strlen(labels(i))-1))+'!x'
			xyouts, xx+dsx, Logg+dsy, blanks, size=2.5*cs, orient=angle, col=0
; This one works and is pretty simple
           endif else begin
            for i=0,Nf-1L do $
                plots, xx(i)+dsx*[1.1,1.1+strlen(labels(i))]*xx(i)/5500., $
                    Logg(i)-dsy*[1,1]*.6, col=0, th=14 + 16*(1-(device EQ 'x'))
            print, dsx, dsy
           endelse
          endif
; print the labels of the simulations
            print, dsx, dsy
            dvxy= convert_coord([0,!D.x_ch_size],[0,!D.y_ch_size],/DEVI,/TO_DAT)
            ch  = (dvxy(1,1)-dvxy(1,0))*1.2*1.0*cs
            cy1 = !y.crange(1) - ch
            if KEYWORD_SET(TLOG) then begin
                cw  = (dvxy(0,1)-dvxy(0,0))*(2.0*1.0*cs)
                cx1 = !x.crange(1) - cw
            endif else begin
                cw  = (dvxy(0,1)/dvxy(0,0))^(2.0*1.0*cs)
                cx1 = (1e1^!x.crange(1))/cw
            endelse
            sxa = xx  +dsx*.6
            ij  = where(sxa LT (cx1+140))
            if (ij(0) GE 0L) then sxa(ij) = sxa(ij)-5.8*dsx*.6
            sya = Logg-dsy*.6
            ij  = where(sya LT (cy1+.25))
            if (ij(0) GE 0L) then sya(ij) = sya(ij)+4.8*dsy*.6
            ij  = where(labels EQ '19')
            if (ij(0) GE 0L) then sya(ij) = Logg(ij)+dsy*.9
            ij  = where(Logg LT (LTeff-3.7)*16+2.2)
            if (ij(0) GE 0L) then sxa(ij) = sxa(ij)+5.8*dsx*.3
			xyouts, sxa, sya, labels, size=1.0*cs, orient=angle, col=simcol
		endif

		AB = reform(regress(transpose([[LTeff],[Logg]]), ff,  CHISQ=chi2AB, $
                                                Const=CAB, sig=sigAB, /DOUBLE))
;  Reduced \chi2 with DOF degrees of freedom
        DOF = float(Nf - 2L)
        rsigAB = sigAB*sqrt(chi2AB/DOF)
;
;  Print in human-readable form
        sAB = string(abs(AB), form='(f0.4)')
        srsigAB = string(rsigAB, form='(f0.4)')
        Aform='(A0," = ",f0.4,A2," (",A0,"+/-",A0,")*logTeff ",A0,' $
                              + '" (",A0,"+/-",A0,")*logg+/-",F0.4,' $
                              + '", red.sig=",F0.4)'
        print,vstr,CAB,reform(transpose([[ssign(AB)],[sAB],[srsigAB]]),6L),$
            rms(AB(0)*LTeff+AB(1)*Logg+CAB-ff,/FLUC), chi2AB/DOF, form=Aform
;
;		plots, aver(!x.crange)+[0,1./AB(0)], aver(!y.crange)+[0,1./AB(1)]

        !y.tickname=""
        if KEYWORD_SET(CLOG) then begin
            color_scale, sgnch*(1e1^zrange(0)), sgnch*(1e1^zrange(1)), $
                x0=ix+(x0-ix0)/Npx,/LOG,$
                y0=iy+(y0-iy0)/Npy, height=.35/Npy, titl=vartitl, charsize=.9*cs
        endif else begin
            color_scale, zrange(0), zrange(1), x0=ix+(x0-ix0)/Npx, $
                y0=iy+(y0-iy0)/Npy, height=.35/Npy, titl=vartitl, charsize=.9*cs
        endelse
        if KEYWORD_SET(MPLOT) then begin
            if (device NE 'passthrough') then reset_graphics
            !P.MULTI=[ipl+1L,Npx,Npy]
            return
        endif
	endif else begin
		AB = reform(regress(transpose([[LTeff],[Logg]]), ff, /DOUBLE))
		dxr = max(LTeff)-minLT  &  dyr=max(Logg)-minLg
		default, az, atan(1./AB(0)/dxr, 1./AB(1)/dyr)/!dpi*180d0+120
		if KEYWORD_SET(TLOG) then begin
			surface, surf, LTe, Lgg, ysty=1, zsty=1, /SAVE, ax=ax, az=az, $
				xr=xrange, yr=[max(Logg), minLg], zr=zrange, ztitl=vartitl
            print, 'Surf1'
		endif else  begin
			if (az GT 80 AND az LT 270) then begin
				!y.title=!x.title
				!x.title='log!d10!n('+strvltitl(6)+')'
				surface, rotate(surf,1),reverse(Lgg),LTe,xsty=1,zsty=1,/SAVE, $
					ax=ax,az=az-90,/YLOG,ytickv=Ttickv,yticks=Nticks,yminor=0, $
					yr=xrange, xr=[minLg, max(Logg)], ztitl=vartitl, $
					zr=zrange;	[min(surf), 2.]
                print, 'Surf1'
				ydiff  = !x.crange(1)-!x.crange(0)
				y1     = !x.crange(0)+[0,!P.ticklen/2]*ydiff
				y2     = !x.crange(1)-[0,!P.ticklen/2]*ydiff
				for i=iminor(is(0)), iminor(is(1)) do begin
					plots, y1, [i,i]*xm, !z.crange(0), /T3D
					plots, y2, [i,i]*xm, !z.crange(0), /T3D
				endfor
			endif else begin
				surface, surf, LTe, Lgg, ysty=1, zsty=17, zaxis=4, /SAVE, $
					ax=ax, az=az,/XLOG,xtickv=Ttickv, xticks=Nticks, xminor=0, $
					xr=xrange, yr=[max(Logg), minLg], zr=zrange, ztitl=vartitl
                print, 'Surf3'
				ydiff  = !y.crange(1)-!y.crange(0)
				y1     = !y.crange(0)+[0,!P.ticklen/2]*ydiff
				y2     = !y.crange(1)-[0,!P.ticklen/2]*ydiff
				for i=iminor(is(0)), iminor(is(1)) do begin
					plots, [i,i]*xm,y1, !z.crange(0), /T3D
					plots, [i,i]*xm,y2, !z.crange(0), /T3D
				endfor
			endelse
		endelse
		print, ' az= ',az, Nf, !z.crange
		if (Nii GT 0L) then begin
			if (az GT 80 AND az LT 270) then begin
				plots, Logg(ii), xx(ii), ff(ii), psym=2, syms=2, /T3D, col=col0
				print, "Sun0: ", iSun, Logg(iSun), xx(iSun)+80, ff(iSun)
				xyouts, Logg(iSun), xx(iSun), z=ff(iSun), /T3D, '!9n!x', $
											align=.5, col=col0, charth=2, /DATA
			endif else begin
				plots, xx(ii), Logg(ii), ff(ii), psym=2, syms=2, /T3D, col=col0
				print, "Sun1: ", iSun, Logg(iSun), xx(iSun)+80, ff(iSun)
				xyouts, xx(iSun), Logg(iSun), z=ff(iSun), /T3D, '!9n!x', $
											align=.5, col=col0, charth=2, /DATA
			endelse
		endif
        if KEYWORD_SET(EVOLTRACKS) then begin
			track_oplot, gmask, /GRAVPLOT, LOGT=TLOG, /MESAEVOL, $
                        /NOPLOT, Nout=Nout, Mout=Mout, Tout=Tout, lgout=lgout
            NM   = N_ELEMENTS(Mout)
            for i=0L, NM-1L do begin
                if KEYWORD_SET(TLOG) then begin
                    ix = (       Tout(0:Nout(i)-1L,i)  - minLT)/dLTe
                endif else begin
                    ix = (alog10(Tout(0:Nout(i)-1L,i)) - minLT)/dLTe
                endelse
                iy = (lgout(0:Nout(i)-1L,i) - minLg)/dLg
                zz = interpolate(surf, ix, iy)
                ii = where(zz GT !z.crange(0)+1e-3)
                Nii = (N_ELEMENTS(ii)-(ii(0L) LT 0L))
                if (Nii GT 0L) then begin
                    dii = ii(1L:Nii-1L)-ii(0L:Nii-2L)
                    brks = where(dii GT 1L)
                    Nbrks = N_ELEMENTS(brks) - (brks(0L) LT 0L)
                    if (Nbrks GT 0L) then begin
                        if (Nbrks GT 1L) then dbrks=brks-[0,brks(0:Nbrks-2L)] $
                                         else dbrks=brks
                        dum   = max(dbrks, imx)
                        if (imx GT 0L) then jj=brks(imx-1)+lindgen(dbrks(imx)) $
                                       else jj=            lindgen(dbrks(imx))
                        kk    = ii(jj)
                        ii    = kk(where(abs(zz(kk(1:*))-zz(kk)) LT 0.002))
                    endif
                    plots,Tout(ii,i),lgout(ii,i),zz(ii),/T3D,col=trackcol,th=2
                endif
            endfor
        endif
	endelse

	if KEYWORD_SET(STOP) then stop
    if (device NE 'x'  AND  device NE 'passthrough') then begin
        device, /CLOSE & set_plot,'x'
    endif

    if (NOT KEYWORD_SET(NOREVERT)) then begin
        if (icurr NE colortable  AND  device EQ 'x') then begin
            stra = prompt(" Revert to previous color-table? [Yes/Stop/Quit]? ")
            if (stra EQ 's') then stop
            if (stra EQ 'n') then begin
                reset_graphics
                return
            endif
;
; Revert to previous color-table
            loadct, icurr, /SILENT
        endif else begin
            loadct, icurr, /SILENT
            if (strlowcase(!D.NAME) EQ 'x') then begin
                !P.COLOR = !D.TABLE_SIZE-1L & !P.BACKGROUND=0L
            endif else begin
                !P.COLOR = 0L & !P.BACKGROUND=!D.TABLE_SIZE-1L
            endelse
            reset_graphics
            return
        endelse
    endif

reset_graphics
end
