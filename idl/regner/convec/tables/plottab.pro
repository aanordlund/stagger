;
;   Program to make images + surface plots of entire simulation rho-/ee-tables.
;
;   Command arguments (defaults in [...]):
;    iv:           Index of the variable to plot - 0-nvar [1].
;    tabfile:      Name of the table-file [stamnavn().tab].
;    scrfile:      Name of the scratch-file [stamnavn().scr].
;    az:           Azimuthal angle (in degrees) of the surface plot [40deg].
;    tvcolortable: Color-table index for the image [23].
;    GRID:         Whether to use gridded or shaded surface.
;    STOP:         If this keyword is set, stop at the end of plotting.
;
;   Update history:
;   ---------------
;   14.06.1995 Coded/RT
;   06.04.2004 Introduced GRID and extra_args to conform to plottabtmp/RT
;   06.04.2004 Introduced defaultscr => speed-up + versatility/RT
;   06.04.2004 Gracefull handling of out-of-bounds iv/RT
;   06.04.2004 Use different color-table for the image/RT
;   05.12.2007 Fixed surface plots of derivatives/RT
;
;   Known bugs: None
;
;   To do: Port (from plottabtmp.pro) overplotting of cdata and
;          meanrhoT.F77.dat on the surface-plots.
;
pro plottab, iv0=iv0, tabfile=tabfile, scrfile=scrfile, xrange=xrange, $
		yrange=yrange, zrange=zrange, az=az, tvcolortable=tvcolortable, $
		_EXTRA=extra_args, GRID=GRID, ENTROPY=ENTROPY, STOP=STOP
	common cdat,x,y,z,nx,ny,nz,nw,ntmax,date0,time0
	common ctable,ttmean,rhm,drhm,eemin,eemax,dee,mtab,itab,tab

	default, iv0, 1
	default, az, 40.
	default, tvcolortable, 23L
    rt_graphics
	cs = 0.75
; The overall charactersize in units of the default.
    !P.charsize=cs*!P.charsize
; The xyouts charactersize in units of the overall size.
    cs = 1.0*!P.charsize
	iimax = 501 & x0 = 0.18 & y0 = 0.06
; Find and load the tab-file
    default, tabfile, stamnavn()
    if (strpos(tabfile, '.tab') LT 0) then tabfile=tabfile+'.tab'
	if ((fstat(2)).name NE tabfile) then begin
        print, ' Opening ',tabfile
        table, tabfile
    endif
	nvar  = (itab(1)-itab(0))/mtab(0)/3L
	iv    = long(iv0*3.+.5)
; Annotations
	vartit = ['log!d10!n(P!ig!n/!3[!xdyn cm!e-2!n!3]!x)',$
			  'log!d10!n(!7j!x/!3[!xcm g!e-1!n!3]!x)', $
			  'log!d10!n(!8T!3)!x', 'S!i0!n', 'S!i1!n', 'S!i2!n', 'S!i3!n']
	dtit = ['', '!s!i !a!9D!r!s!bD!7e!x!r!3!u!i ______!n!x', $
			'!s!i !a!9D!r!s!bD!7q!x!r!3!u!i ______!n!x']
	if (tabfile EQ 'EOSrhoe.tab') then EOSrhoe=1   else  EOSrhoe=0
	etitle = '!7e!x!n/!3[!x10!e12!nerg g!e-1!n!3]!x'
	rtitle = 'log!d10!n(!7q!x/!3[!xg!i !ncm!e-3!n!3])!x'
	if (EOSrhoe) then etitle = 'log!d10!n('+etitle+')'
;
; Entropy?
	if KEYWORD_SET(ENTROPY) then begin
		sstab, iS
        lnr0 = alog(10)*.5 & ee0=2.5 & ue=1e12
        if (min(eemin) GT 3.0) then begin
; Energy off-set compared to MHD EOS
            xH0  = 0.70295997
;             diss.    ioni.   eV_J*1d7/Mu
            dee0 = (2.23905+13.598)*9.5729060e+11*xH0 - 1.5*13.6*1.6e-12/1.67e-24/1.4
            print, "-dee0, ue, new energy of SS0: ", -dee0, ue, ee0-dee0/ue
            ee0  = ee0-dee0/ue
        endif
		S0 = lookup(lnr0, ee0, iv=iS)
		vartit(iS) = 'S'
		iv = iS*3
	endif
	if (iv GT ((nvar-1L)*3L+2L) OR iv LT 0L) then begin
		print, " WARNING: 3*iv=",iv," outside range of [0-",(nvar-1L)*3L+2L, $
		form='(A0,I0,A0,I0,"]")'
		iv = (iv>0)<((nvar-1L)*3L)
		print, "          Using 3*iv=", iv, form='(A0,I0,".")'
	endif
; Find and load the scratch-file
	defaultscr, scrfile=scrfile
	ntot = total(mtab)
	eet = fltarr(ntot)
	fff = fltarr(ntot)
	Lrhot = fltarr(ntot)
	i0 = 0L
	nrtab = N_ELEMENTS(rhm)
	for i=0,nrtab-1 do begin
		ee = (eemin(i)+indgen(mtab(i))*dee(i))
		if (EOSrhoe) then  ee=ee/alog(1e1)
;		ff = lookup(replicate(alog(rhm(i)),mtab(i)),ee,dfdee,dfdlr,iv=iv)
		ff = tab(itab(i)+mtab(i)*iv-1:itab(i)+mtab(i)*(iv+1)-2)
;		print, i, i0, i0+mtab(i)-1, N_ELEMENTS(ff), N_ELEMENTS(ee)
		eet(i0:i0+mtab(i)-1) = (ee)
		Lrhot(i0:i0+mtab(i)-1) = alog10(rhm(i))-7.0
		if (KEYWORD_SET(ENTROPY)) then begin
			fff(i0:i0+mtab(i)-1) = (ff-S0)*1.e+4
		endif else begin
							  fff(i0:i0+mtab(i)-1) = ff
			if (iv EQ 0) then fff(i0:i0+mtab(i)-1) = ff/alog(1e1)+5.
			if (iv EQ 3) then fff(i0:i0+mtab(i)-1)=(ff-alog(rhm(i)))/alog(10.)-1.
			if ((iv mod 3) EQ 1) then fff(i0:i0+mtab(i)-1) = ff/dee(i)
			if ((iv mod 3) EQ 2) then fff(i0:i0+mtab(i)-1) = ff/drhm
		endelse
		i0 = i0 + mtab(i)
	endfor
	triangulate, eet, Lrhot, TR
	deet = (max(eet) - min(eet))/(iimax-1.0001)
	ees = min(eet) + findgen(iimax)*deet
	default, xrange, [ees(0), ees(iimax-1L)]
	ix  = where(ees GE xrange(0)  AND  ees LE xrange(1))

	dLr = (max(Lrhot) - min(Lrhot))/(iimax-1.0001)
	Logrho = min(Lrhot) + findgen(iimax)*dLr
	default, yrange, [Logrho(0), Logrho(iimax-1L)]
	iy  = where(Logrho GE yrange(0)  AND  Logrho LE yrange(1))
	if (ix(0) LT 0  OR  iy(0) LT 0) then begin
		print, "ERROR: Invalid x- or y-range. Bailing out!"
		return
	endif
;	stat, fff
	fff    = fff/alog(1e1)
	minfff = min(fff, max=maxfff)
	plot, [minfff, maxfff], ysty=18, col=0
	minfff=!y.crange(0)& maxfff=!y.crange(1)

	fffin = trigrid(eet, Lrhot, fff, TR, [deet,dLr], missing=minfff)
	fffin = fffin(ix,*) & fffin = fffin(*,iy)
	minfff=min(fffin, max=maxfff)
	default, zrange, [minfff, maxfff]

	rf = fix(700./max([N_ELEMENTS(ix), N_ELEMENTS(iy)]))
	sff = size(fffin) & fx = sff(1) & fy = sff(2)
	erase
	vartitle = dtit(iv mod 3)+vartit(iv/3)
; Find the current color-table index and load the TVCOLORTABLE
	icurr = currentct(/QUIET, ntables=ntables)
	tvcolortable = (tvcolortable>0)<(ntables-1L)
	loadct, tvcolortable, /SILENT
; Image of the table
	img_plot, fffin, x0, y0, /SCALE, margin=.07
	plot, eet, Lrhot, psym=3, /NORM, /NOERASE, tit=vartitle+'!c ', $
			xtit=etitle, ytit=rtitle, ysty=1, xrange=xrange, yrange=yrange, $
			_EXTRA=extra_args
	if (strpos((fstat(1)).name, 'sav') GE 0  OR $
		strpos((fstat(1)).name, 'scr') GE 0) then begin
		if (EOSrhoe) then begin
;			oplot, alog10(fee(0)), flnrho(0)/alog(1e1)-7., psym=2, syms=.2
			oplot, alog10(cdata(0,'eem')),alog10(cdata(0,'rhom')*1e-7),col=0
		endif else begin
;			oplot,        fee(0),  flnrho(0)/alog(1e1)-7., psym=2, syms=.2
			oplot,        cdata(0,'eem'), alog10(cdata(0,'rhom')*1e-7), col=0
		endelse
	endif
	if KEYWORD_SET(ENTROPY) then plots, ee0, lnr0/alog(1e1)-7., psym=2, col=0, syms=.5
	cx0 = !P.position(0)-.10 & cy0 = !P.position(3)-.3
	!P.region=[0,0,0,0]
	color_scale, minfff, maxfff, x0=cx0, y0=cy0, tit=vartitle, height=.3
	stra = ''

	stra = prompt(" Continue [Yes/Stop/Quit]? ")
	if (stra EQ 's') then stop
	if (stra EQ 'q' OR stra EQ 'n' OR stra EQ 'r') then begin
		reset_graphics
		return
	endif

	!P.region = 0
	loadct, icurr, /SILENT
	if KEYWORD_SET(GRID) then begin
		surface,    fffin, ees(ix), Logrho(iy), az=az, ztit=vartitle, chars=1.6*cs,$
			xtit=etitle, ytit=rtitle, zr=zrange, zsty=1, ysty=17, $
			xr=xrange, yr=yrange, /SAVE, _EXTRA=extra_args
		plots, eet, Lrhot, fff, /T3D, psym=3, color=(!D.TABLE_SIZE-1.0)*.6
	endif else begin
		shade_surf, fffin, ees(ix), Logrho(iy), az=az, ztit=vartitle, chars=1.6*cs,$
			xtit=etitle, ytit=rtitle, zr=zrange, zsty=1, ysty=17, $
			xr=xrange, yr=yrange, /SAVE, _EXTRA=extra_args
		plots, eet, Lrhot, fff, /T3D, psym=3, color=(!D.TABLE_SIZE-1.0)*.9
	endelse
	if KEYWORD_SET(ENTROPY) then $
        plots, ee0, lnr0/alog(1e1)-7., psym=2, col=0, syms=.5, z=0, /T3D

	if KEYWORD_SET(STOP) then stop
	reset_graphics
end
