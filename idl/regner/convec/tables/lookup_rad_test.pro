;
;   Program to test and visualize the interpolations in the kaprhoT.tab-tables.
;
;   Command arguments (defaults in [...]):
;    i:            index of variable to check [0].
;    j:            index of wavelength or opacity bin [0].
;    tvcolortable: Color-table index for the image [23].
;    CUBIC:        Set this keyword to perform bi-cubic spline interpolations.
;    STOP:         Set this keyword to stop before returning, to make all
;                  variables available to the user (for debugging).
;
;   Update history:
;   ---------------
;   12.03.2008 Coded/RT
;
;   Known bugs: None
;
pro lookup_rad_test, i, j, tvcolortable=tvcolortable, CUBIC=CUBIC, STOP=STOP
	common cradtab,NTtab,Nrtab,Ma,Na,Nlam,Nelem,Nxopt,Nxrev,$
		date,time,lnrmin,lnrmax,lnTmin,lnTmax,ul,ur,ut,iel,abund,arr,tab

	default, i, 0L
	default, j, 0L
	default, tvcolortable, 23L
    rt_graphics

	c0    = (!D.TABLE_SIZE-1.)*.7
	ln10  = alog(1e1)
	Ttitl = 'log!d10!n!8T!x'
	rtitl = 'log!d10!n!7q!x'
	ftitl = '!8f!x('+string(i,j,form='(I0,",",I0)')+')'
;
; Load the table
	radtab, /QUIET
	ulr   = alog10(ur)
;
; lnT- and ln\rho-grid of the table
	lnTtb = findgen(NTtab)*(lnTmax-lnTmin)/(NTtab-1.) + lnTmin
	lnrtb = findgen(Nrtab)*(lnrmax-lnrmin)/(Nrtab-1.) + lnrmin
;
; Indexes of the part of the table to investigate
	it1 = 2L& it2 = 8L
	ir1 = 0L& ir2 = 6L
;
; How many more points in each direction?
	fc = 10L
;
; lnT- and ln\rho-grid of the interpolation
	NT2 = (it2-it1)*fc+1L
	Nr2 = (ir2-ir1)*fc+1L
	lnT   = (findgen(NT2)/(NT2-1.)*(lnTtb(it2)-lnTtb(it1)) + lnTtb(it1))#$
			replicate(1, Nr2)
	lnrho = replicate(1, NT2)#$
			(findgen(Nr2)/(Nr2-1.)*(lnrtb(ir2)-lnrtb(ir1)) + lnrtb(ir1))
;
; Perform the interpolation
	ff = lookup_rad(lnrho, lnT, 0L, 0L, CUBIC=CUBIC)
	frange = [min(ff, max=maxf), maxf]
;
; Plots as function of log10(T)
	plot, lnTtb(it1:it2)/ln10, tab(it1:it2,ir1,i,j), yr=frange, ysty=1, $
													xtitl=Ttitl, ytitl=ftitl
	for ir=ir1, ir2 do $
		oplot,lnTtb(it1:it2)/ln10,tab(it1:it2,ir,i,j),psym=-2,syms=.3
	for ir=0L, Nr2-1L, fc do oplot, lnT(*,0)/ln10, ff(*,ir), col=c0
	s = prompt(" \rho-interpolation plots? [y/n/q/s]: ")
	if (s EQ 'n'  OR  s EQ 'q'  OR  s EQ 'r') then return
	if (s EQ 's')                             then stop
;
; Plots as function of log10(\rho)
	plot, lnrtb(ir1:ir2)/ln10+ulr, tab(it1,ir1:ir2,i,j), yr=frange, ysty=1, $
													xtitl=rtitl, ytitl=ftitl
	for it=it1, it2 do $
		oplot,lnrtb(ir1:ir2)/ln10+ulr,tab(it,ir1:ir2,i,j),psym=-2,syms=.3
	for it=0L, NT2-1L, fc do oplot, lnrho(0,*)/ln10+ulr, ff(iT,*), col=c0
	s = prompt(" Surface plots? [y/n/q/s]: ")
	if (s EQ 'n'  OR  s EQ 'q'  OR  s EQ 'r') then return
	if (s EQ 's')                             then stop

	surface, tab(it1:it2,ir1:ir2,i,j), $
			lnTtb(it1:it2)/ln10, lnrtb(ir1:ir2)/ln10+ulr, $
			ysty=1, az=5, /SAVE, xtitl=Ttitl, ytitl=rtitl, ztitl=ftitl
	surface, ff, lnT(*,0)/ln10, lnrho(0,*)/ln10+ulr, /T3D, /NOERASE, col=c0, $
		xsty=1, xr=!x.crange, ysty=1, yr=!y.crange, zsty=1, zr=!z.crange
	
	if KEYWORD_SET(CUBIC) then begin
		s = prompt(" \Delta tv? [y/n/q/s]: ")
		if (s EQ 'n'  OR  s EQ 'q'  OR  s EQ 'r') then return
		if (s EQ 's')                             then stop
;
; Perform linear interpolation for comparison
		flin = lookup_rad(lnrho, lnT, 0L, 0L)
		erase
; Find the current color-table index and load the TVCOLORTABLE
		icurr = currentct(/QUIET, ntables=ntables)
		tvcolortable = (tvcolortable>0)<(ntables-1L)
		loadct, tvcolortable, /SILENT
;
; Image of the interpolation differences
		img_plot, (ff-flin)/ff, .24, /SCALE
		plot, lnT(*,0)/ln10, lnrho(0,*)/ln10+ulr, /NODATA, /NOERASE, $
			xsty=1, ysty=1, xtitl=Ttitl, ytitl=rtitl
		color_scale, min((ff-flin)/ff, max=maxdf), maxdf, x0=.09, $
									titl='!7D'+ftitl+'/!3[!x%!3]!x   '

		if (icurr NE tvcolortable) then begin
			s = prompt(" Revert to previous color-table? [y/n/q/s]? ")
			if (s EQ 'n'  OR  s EQ 'q'  OR  s EQ 'r') then return
			if (s EQ 's')                             then stop
;
; Revert to previous color-table
			loadct, icurr, /SILENT
		endif
	endif

	reset_graphics

	if KEYWORD_SET(STOP) then stop
end
