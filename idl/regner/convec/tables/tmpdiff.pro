;
;   Program to plot exact differences between two tmp-tables.
;   The differences are plottet in the sense:  tmp2 - tmp1
;
;   Update history:
;   ---------------
;   13.10.1998 Coded/RT
;   19.04.2005 Use different color-table for the image/RT
;              Use prompt instead of read + better handling of response/RT
;              Allow mz NE nz (new parameter)/RT
;              Using ivp in the code, to not alter the argument iv/RT
;              Check on validity of iv(p) and useful warning statement/RT
;   20.05.2005 Changed the surface of the zero-level to a contour/RT
;   19.05.2007 Soft-coded size of picture/RT
;              Include overplotting of meanrhoT.F77.dat and zero-contour/RT
;              Corrected labels: Log -> ln /RT
;   09.07.2007 Straightened out the naming of variables/RT
;
;   Known bugs: None
;
pro tmpdiff, tmp1file, iv=iv, mz=mz, tmp2file=tmp2file, $
					tvcolortable=tvcolortable, _EXTRA=extra_args, STOP=STOP
	common cdat,x,y,z,nx,ny,nz,nw,ntmax,date0,time0
	common ctabl1,rhm,drhm,thmin,thmax,dth,mtab,itab,tab

	default, iv, 1
    default, tmp2file, stamnavn()+'.tmp'
	if (strpos(tmp2file, '.tmp') LT 0  AND  $
        strpos(tmp2file, '.tab') LT 0) then tmp2file=tmp2file+'.tmp'
    default, tmp1file, stamnavn()+'.tmp.bak'
	if (strpos(tmp1file, '.tmp') LT 0  AND  $
        strpos(tmp1file, '.tab') LT 0) then tmp1file=tmp1file+'.tmp'
	default, mz, nz
	default, tvcolortable, 23L
    rt_graphics

	cs = 0.7
;The overall charactersize in units of the default.
	!P.charsize=cs*!P.charsize
;The xyouts charactersize in units of the overall size.
	cs = .50*!P.charsize

	ivp = long(iv*3.+.5)
	stra = ''
	iimax = 501 & rf = 1 & x0 = 165 & y0 = 70
	tabtmp, tmp2file, mz=mz
	nvar = (itab(1)-itab(0))/mtab(0)/3L
; Make sure we are adressing a sensible variable number
	if (ivp LT 0)      then print, ivp, form=$
					'("WARNING: iv=",I0," < 0. Resetting to ",I0,$)'
	if (ivp GE nvar*3) then print, ivp, nvar*3, form=$
                    '("WARNING: iv=",I0," >= nvar*3=",I0,". Resetting to ",$)'
	ivp = (ivp>0L)<(nvar*3L-1L)
	print, ivp, form='(I0)'
	if (nvar EQ 5L) then begin
		vartit = ['ln(P!ig!n/!3[!xdyn cm!e-2!n!3]!x)', $
				  '!8e!x!n/!3[!x10!e12!nerg g!e-1!n!3]!x', $
				  'ln(!7j!x/!3[!xcm g!e-1!n!3]!x)', $
				  'S!i0!n','S!i1!n','S!i2!n','S!i3!n']
	endif else begin
		vartit = ['ln(P!ig!n/!3[!xdyn cm!e-2!n!3]!x)', $
				  'ln(!7j!x/!3[!xcm g!e-1!n!3]!x)', $
				  '!7e!x!n/!3[!x10!e12!nerg g!e-1!n!3]!x', $
				  'ln!8S!x!i0!n','ln!8S!x!i1!n','ln!8S!x!i2!n','ln!8S!x!i3!n']
	endelse
	dtit = ['', '!s!a !9D!x!r!s!3!A!i_____!x!r!b!9D!7H!x!n', $
			'!s!a !i !a!9D!x!r!s!3!A!i_________!x!r!b!9D!xln!7q!x!n']
	rhm2 = rhm
	thmin2 = thmin
	thmax2 = thmax
	tab2 = tab
	tabtmp, tmp1file, mz=mz
	rhm1 = rhm
	thmin1 = thmin
	thmax1 = thmax
	dth1 = dth
	tab1 = tab
	itab1 = itab
	mtab1 = mtab
	error = 0
	eps = 1.e-4
	if (max((rhm2  -rhm1)^2) GT eps) then error=1
	if (max((thmin2-thmin1)^2) GT eps) then error=1
	if (max((thmax2-thmax1)^2) GT eps) then error=1
;	plot,alog10(rhm1)-7., rhm1-rhm2, xtit='Log(!7q!x)', ytit='!7Dq!x'
;	read,'Good enough? ',stra & if (stra EQ 'n') then error=1; return
;	plot,alog10(rhm1)-7., thmin1-thmin2, xtit='Log(!7q!x)', ytit='!7DH!x!imin!n'
;	read,'Good enough? ',stra & if (stra EQ 'n') then error=1; return
;	plot,alog10(rhm1)-7., thmax1-thmax2, xtit='Log(!7q!x)', ytit='!7DH!x!imax!n'
;	read,'Good enough? ',stra & if (stra EQ 'n') then error=1; return
	if (error) then begin
		print, 'Using lookup_tmp.'
		tabtmp, tmp2file, mz=nz
	endif
; Read mean structure from meanrhoT-file
	if (read_meanrhot(mLrm, mLTm)) then begin
		nzm = N_ELEMENTS(mLTm)
		mLrm = alog10(mLrm)-7.
		mLTm = alog10(mLTm)
	endif else nzm=0L

	ntot = total(mtab1)
	eet = fltarr(ntot)
	LTt = fltarr(ntot)
	Lrhot = fltarr(ntot)
	i0 = 0L
	for i=0,nz-1 do begin
		tt = 5040./(thmin1(i)+indgen(mtab1(i))*dth1(i))
;		print, i, i0, i0+mtab(i)-1, N_ELEMENTS(tt)
		LTt(i0:i0+mtab1(i)-1) = alog10(tt)
		Lrhot(i0:i0+mtab1(i)-1) = alog10(rhm1(i))-7.0
		k0 = mtab1(i)*ivp-1;  itab is in Fortran style indexes
		if (error) then begin
			tabval2 = lookup_tmp(replicate(alog(rhm1(i)),mtab1(i)), tt, $
													dvdtcr,dvdrct, iv=ivp/3)
			if ((ivp mod 3) EQ 1) then tabval2=dvdtcr
			if ((ivp mod 3) EQ 2) then tabval2=dvdrct
		endif else begin
			tabval2 = tab2(k0+itab1(i):k0+itab1(i)+mtab1(i)-1)
		endelse
		eet(i0:i0+mtab1(i)-1) = tabval2-tab1(k0+itab1(i):k0+itab1(i)+mtab1(i)-1)
		i0 = i0 + mtab1(i)
	endfor
	stat, eet
; Find the current color-table index and load the TVCOLORTABLE
	icurr = currentct(/QUIET, ntables=ntables)
	tvcolortable = (tvcolortable>0)<(ntables-1L)
	erase
	loadct, tvcolortable, /SILENT
; Set the plot area
	plot, [0,1], [0,1], col=!p.background
	axis, /zaxis, zsty=6, zr=[min(eet), max(eet)], /save, col=!p.background
	zrr = !z.crange
	triangulate, LTt, Lrhot, TR
	dLT = (max(LTt) - min(LTt))/(iimax-1.001)
	LogT = min(LTt) + findgen(iimax)*dLT
	dLr = (max(Lrhot) - min(Lrhot))/(iimax-1.001)
	Logrho = min(Lrhot) + findgen(iimax)*dLr
	minee = zrr(0); min(eet); long(min(eet)-.99999)
	eefin = trigrid(LTt, Lrhot, eet, TR, [dLT,dLr], missing=minee)
	see = size(eefin) & ex = see(1) & ey = see(2)
	erase
;	print, ex, ey
;	cs = !P.charsize
;	!P.charsize = .6
	vartitle = '!7D!x'+dtit(ivp mod 3)+vartit(ivp/3)
	cx0 = (x0/2.-20.)/659. & cy0 = (y0+rf*iimax/2.-50.)/656.
	color_scale, min(eefin), max(eefin), x0=cx0, y0=cy0, tit=vartitle
	tvscl, rebin(eefin, rf*ex, rf*ey), x0, y0
	!P.region=[x0, y0, x0+rf*ex, y0+rf*ey]
	plot, LTt, Lrhot, psym=3, /DEVICE, /NOERASE, tit=underscore(tmp2file) + $
			" - " + underscore(tmp1file) + "!c" + vartitle, $
			xtit='Log(!8T!x)', ytit='Log(!7q!x)', ysty=1
	contour, eefin, LogT, Logrho, levels=[0.0], /NOERASE, $
		xsty=1,xr=!x.crange,ysty=1,yr=!y.crange, /DEVICE
	if (nzm GT 0L) then begin
		oplot, mLTm, mLrm, th=2, col=0
		ix  = (mLtm-LogT(0))/(LogT(1)-LogT(0))
		iy  = (mLrm-Logrho(0))/(Logrho(1)-Logrho(0))
		mtmpdiff = bispl(eefin, ix,iy)
	endif
	stra = prompt(" Continue [Yes/Stop/Quit]? ")
	if (stra EQ 's') then begin
		loadct, icurr, /SILENT
		stop
	endif
	if (stra EQ 'q' OR stra EQ 'n' OR stra EQ 'r') then begin
		reset_graphics
		loadct, icurr, /SILENT
		return
	endif
	loadct, icurr, /SILENT

	!P.region = 0
	!P.charsize = 1.8*!P.charsize
	shade_surf, eefin, LogT, Logrho, ztit=vartitle, zr=zrr, zsty=1, $
		xtit='Log(!8T!x)', ytit='Log(!7q!x)', ysty=1, /SAVE, _EXTRA=extra_args
	cols = (!D.TABLE_SIZE-1.)*(.2+.8*(eefin LT 0.0))
	contour, eefin, LogT, Logrho, levels=0.0, /T3D, /NOERASE, $
		xsty=1,xr=!x.crange,ysty=1,yr=!y.crange,zr=!z.crange,zsty=1
	if (nzm GT 0L) then   plots, mLTm, mLrm, mtmpdiff, /T3D, th=2, col=0
;	surface, replicate(0.0, iimax, iimax), LogT, Logrho, zr=zrr, zsty=1, $
;				/SAVE, ysty=1, /NOERASE, shade=cols, _EXTRA=extra_args
	plots, LTt, Lrhot, eet, /T3D, psym=3, color=100
	if KEYWORD_SET(stop) then stop
	reset_graphics
end
