;
;   Program to lookup entries in the EOS and opacity table in lnrho and T,
;   for a simulation. The interpolation in lnrho and \theta=5040./T, is linear!
;
;   Update history:
;   ---------------  .
;   ??.??.19?? Coded/Ake
;   10.03.2008 Fixed division by dth in calculation of dvdtcr/RT
;   27.11.2008 Correctly opens and interpolates in 'EOS.tab' or 'kaprhoT.tab'
;              depending on the required table-entry, if the previous
;              tabtmp-call didn't open a '[sim].tmp'-table/RT
;   12.03.2010 Don't divide dvdrct by drhm/RT
;   21.03.2011 Added comments/RT
;   11.09.2014 Added header comments, added diagnostic statements in case of
;              extrapolations, and fixed calculation of np to be from 0 to 1.
;              Also added keyword STOP/RT
;   15.09.2015 Fixed bug in finding index of location in lnrho/tt of
;              extrapolations (pymax, qymax)./RT
;
;   Known bugs: Only capable of linear interpolation.
;
function lookup_tmp, lnrho, tt, dvdtcr, dvdrct, iv=iv, QUIET=QUIET, STOP=STOP

      common ctabl1,rhm,drhm,thmin,thmax,dth,mtab,itab,tab
	  common cradtab,NTtab,Nrtab,Ma,Na,Nlam,Nelem,Nxopt,Nxrev,$
			  date,time,lnrmin,lnrmax,lnTmin,lnTmax,ul,ur,ut,iel,abund,arr

      if n_params() eq 0 then begin
		print,'lookup_tmp,lnrho,tt,n=n,iv=iv'
		return,0
      endif

      if n_elements(iv) eq 0 then iv=0

	  if ((fstat(2)).open EQ 0) then begin
		ln10 = alog(1e1)
		lnur = alog(cpar(0,'uur'))
		ue   = (cpar(0,'uul')/cpar(0,'uut'))^2
		lnup = alog(ue) + lnur
		if (iv EQ 0) then begin
			tabvar= $
			mhd_intrp(alog10(tt),(lnrho+lnur)/ln10,1,'EOS.tab',dir='./')
; adjust units
			tabvar = tabvar*alog(1e1) - lnup
			if (N_PARAMS() GT 2) then begin
				dvdtcr = $
					mhd_intrp(alog10(tt),(lnrho+lnur)/ln10,4,'EOS.tab',dir='./')
			endif
			if (N_PARAMS() GT 3) then begin
				dvdrct = $
					mhd_intrp(alog10(tt),(lnrho+lnur)/ln10,3,'EOS.tab',dir='./')
			endif
		endif
		if (iv EQ 1) then begin
			radtab
			tabvar = lookup_rad(lnrho,alog(tt),0,Nlam)
		endif
		if (iv EQ 2) then begin
			tabvar= $
			mhd_intrp(alog10(tt),(lnrho+lnur)/ln10,2,'EOS.tab',dir='./')
; adjust units
			tabvar = (1e1^(tabvar-(lnrho+lnur)/ln10))/ue
			if (N_PARAMS() GT 2) then begin
				dvdtcr = $
					mhd_intrp(alog10(tt),(lnrho+lnur)/ln10,6,'EOS.tab',dir='./')
			endif
			if (N_PARAMS() GT 3) then begin
				dvdrct = $
					mhd_intrp(alog10(tt),(lnrho+lnur)/ln10,5,'EOS.tab',dir='./')
				dvdrct = dvdrct - 1.
			endif
		endif
		return, tabvar
	  endif

	  mz = N_ELEMENTS(rhm)
      rhm1=alog(rhm(0))
      rhm2=alog(rhm(mz-1))
      drhm=(rhm2-rhm1)/(mz-1)
      s=size(lnrho)
;
;  density index
;
      algrk=(lnrho-rhm1)/drhm
      np=((long(algrk) > 0) < (mz-2))
      ; stat,np
      px=algrk-np
      qx=1.-px
      pxqx=px*qx

      pxmax = max(px, ipmx)
      qxmax = max(qx, iqmx)
      if (NOT KEYWORD_SET(QUIET)) then begin
          if (pxmax GT 1.) then begin
            print,'WARNING: extrapolation in lnrho, pxmax =',pxmax
            print,'                          lnrho, tt=', lnrho(ipmx), tt(ipmx)
          endif
          if (qxmax GT 1.) then begin
            print,'WARNING: extrapolation in lnrho, qxmax =',qxmax
            print,'                          lnrho, tt=', lnrho(iqmx), tt(iqmx)
          endif
      endif
;
;  Interpolate the vars and dvar/dlnrho at smaller lnrho
;
      ntab=mtab(np)
      ; print,max(5040./thmax(np)),min(tt),max(tt),min(5040./thmin(np))
      th1k=(5040./tt-thmin(np))/dth(np)
      kth1=((long(th1k) > 0) < (ntab-2))
      ik1=itab(np)+kth1-1+iv*3*ntab
      py1=th1k-kth1
      ; stat,py1
      qy1=1.-py1

      ntab2=mtab(np+1)
      ; print,max(5040./thmax(np+1)),min(tt),max(tt),min(5040./thmin(np+1))
      th2k=(5040./tt-thmin(np+1))/dth(np+1)
      kth2=((long(th2k) > 0) < (ntab2-2))
      ik2=itab(np+1)+kth2-1+iv*3*ntab2
      py2=th2k-kth2
      ; stat,py1
      qy2=1.-py2
      pymax = max([py1,py2], ipmx)& ipmx = ipmx mod s(1L)
      qymax = max([qy1,qy2], iqmx)& iqmx = iqmx mod s(1L)
      if (NOT KEYWORD_SET(QUIET)) then begin
          if (pymax GT 1.) then begin
            print,'WARNING: extrapolation in tt, pymax =',pymax
            print,'                          lnrho, tt=', lnrho(ipmx), tt(ipmx)
          endif
          if (qymax GT 1.) then begin
            print,'WARNING: extrapolation in tt, qymax =',qymax
            print,'                          lnrho, tt=', lnrho(iqmx), tt(iqmx)
          endif
      endif
;
; Warped linear interpolation - everywhere continuous, but not
; differentiable on grid edges.
      tabvar = $
		qx * (qy1*tab(ik1) + py1*tab(ik1+1)) + $
		px * (qy2*tab(ik2) + py2*tab(ik2+1))
;
; Same interpolation of the derivatives stored in the table.
	  if (N_PARAMS() GT 2) then begin
		dvdtcr = $
		qx * (qy1*tab(ik1+1*ntab ) + py1*tab(ik1+1+1*ntab ))/dth(np) + $
		px * (qy2*tab(ik2+1*ntab2) + py2*tab(ik2+1+1*ntab2))/dth(np+1)
	  endif
	  if (N_PARAMS() GT 3) then begin
		dvdrct = $
		qx * (qy1*tab(ik1+2*ntab ) + py1*tab(ik1+1+2*ntab )) + $
		px * (qy2*tab(ik2+2*ntab2) + py2*tab(ik2+1+2*ntab2))
		dvdrct = dvdrct;/drhm
	  endif

      if KEYWORD_SET(STOP) then stop
      return, tabvar
end
