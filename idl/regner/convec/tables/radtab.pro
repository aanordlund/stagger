;
;   Program to load and possibly plot a table of radiative quantities
;   used for the convection simulations. The bulk of the table, contained
;   in 'tab', is organized as NTtab*Nrtab arrays of ln(x_j), ln(\eps_j*B_j)
;   and ln(\eps_j), for each 'j' which enumerates opacity bins or wavelengths.
;   There are three entries at the end of the table for j=Nlam, containing
;   ln(\rho\kappa_Ross), ln(\rho\kappa_Plck) and the monochromatic 5000\AA
;   opacity ln(\rho\kappa_5000).
;
;   Arguments (defaults in [...]):
;    tabfile:   Full name of the table file ['kaprhoT.tab'].
;    tvcolortable: Color-table index for the image [23].
;    xrange:    log10(T) range for the 1D plots along the meanrhoT-structure.
;    az:        az angle for surface plots [30.0].
;    PLOT:      Set this keyword to plot all the table entries.
;    QUIET:     Set this keyword to suppress printing of table info.
;    ONED:      Set this keyword to plot quantities along the meanrhoT structure
;    CGS:       Set this keyword to force [cgs] units despite avail. sim. units.
;    PRINTBINS: Set this keyword to print bin-limits or wavelenghts/weights.
;    STOP:      Set this keyword to stop before returning, to make all
;               variables available to the user (for debugging).
;               Normally the common-block, cradtab, should suffice.
;
;   Update history:
;   ---------------
;   10.03.2008 Coded/RT
;   11.03.2008 Added common-block and in-lined 'default' and 'prompt'/RT
;   07.04.2008 Renamed to 'radtab', changed from surface -> shade_surf /RT
;   19.08.2008 Fixed handling of unit-conversions and whether the last entries
;              are \kappa or \rho*\kappa. Only do the 1D plots if sun.tmp
;              is present. Improved yrange for the 1D plot/RT
;   15.09.2008 Fixed accidental hard-coding 4->Nlam/RT
;   15.09.2008 Now always plot \kappa/[cm^2 g^-1] but make sure tab contains
;              absoption coefficient/[cm^-1] for tables computed with pre-
;              rev.73 radtab.x. Added argument tvcolortable/RT
;   15.10.2008 Printing of diagnostic info taken over by radtabinfo/RT
;   26.01.2009 Added plotting of LTE source-function (when Nxopt(1)=1) summed
;              over bins/wavelengths, and compared to \sigma/\pi T^4  /RT
;   30.08.2013 Now plot opacity spectra in case of OS tables./RT
;   17.03.2014 Added argument, az, for viewing angle of surface plots./RT
;   25.01.2016 Fixed unit-conversion for source functions and now plots
;              a check of the sum of source functions when /PLOT is set./RT
;   14.12.2017 Whether to assume mass- or volume-opacity now depends on
;              abs(Nxrev[radtab.f]), to account for Nxrev<0 for modified code/RT
;
;   Known bugs: None
;
pro default, var, val
    if n_elements(var) eq 0 then var=val
end

function prompt, stra, ONELINE=ONELINE
    print, stra, form='(A,$)'
    answ=get_kbrd(1)
    if KEYWORD_SET(ONELINE) then begin
        print, string(13b), form='(A0,$)'
    endif else begin
        if ((byte(answ))(0) NE 10) then print,answ  else print,''
    endelse
    return, answ
end

pro radtab, tabfile, tvcolortable=tvcolortable, xrange=xrange, info=info,az=az,$
    PLOT=PLOT, QUIET=QUIET, ONED=ONED, CGS=CGS, PRINTBINS=PRINTBINS, STOP=STOP
	common cradtab,NTtab,Nrtab,Ma,Na,Nlam,Nelem,Nxopt,Nxrev,$
		date,time,lnrmin,lnrmax,lntmin,lntmax,rul,rur,rut,iel,abund,arr,tab
	common ctabl1,rhm,drhm,thmin,thmax,dth,mtab,itab,dumtab
    common currenttable, EOSrhoeLoaded, kaprhoTLoaded
;@consts;    Only uses sig.
    sig = 5.6704016e-05
;
; A reasonable guess for the name of the table
    default, tabfile, 'kaprhoT.tab'
    rt_graphics
;
; Open the table file
    openr, lun, tabfile, /GET_LUN, /SWAP_IF_LITTLE_ENDIAN, /F77
;
; Declare all (long) integers before reading
    NTtab=0L& Nrtab=0L& Ma=0L& Na=0L& Nlam=0L& Nelem=0L
    Nxopt=lonarr(20)& Nxrev=Nxopt
;
; Read the dimensions of the arrays to follow.
    readu, lun, NTtab,Nrtab,Ma,Na,Nlam,Nelem,Nxopt,Nxrev
;
; Dimension the arrays
	rul   = 0e0 & rur=rul & rut=rul
    arr   = fltarr(Ma,Na)
    iel   = replicate("    ",Nelem)
    abund = fltarr(Nelem)
    date  = "        " & time=date
    rtab  = fltarr(NTtab,Nrtab,3)
    tab   = fltarr(NTtab,Nrtab,3,Nlam+1L)
;
; Read the arrays of the header
    readu, lun, date,time,lnrmin,lnrmax,lntmin,lntmax,rul,rur,rut,iel,abund,arr
;
; Read the bulk of the table, one wavelength/bin at a time
    for j=0L, Nlam do begin
        readu, lun, rtab
        tab(*,*,*,j) = rtab
    endfor
;
; Close the file
    free_lun, lun
;
; lnT and ln\rho of the table
    lnttb = findgen(NTtab)/(NTtab-1.)*(lntmax-lntmin) + lntmin
    lnrtb = findgen(Nrtab)/(Nrtab-1.)*(lnrmax-lnrmin) + lnrmin
	lnrhotab = replicate(1, NTtab)#lnrtb
    cdrv = abs(Nxrev(1))&  tbyr = long(strmid(date,6,2))
    if ((cdrv LT 73  OR  cdrv EQ 93  OR  cdrv EQ 106) AND tbyr LT 8) then begin
;   if (abs(Nxrev(1)) LT 73  OR  abs(Nxrev(1)) EQ 93) then begin
;
; Before rev. 73, kappa/[cm^2 g^-1] was stored - change to abs.coef./[cm^-1]
        print, 'NB: radtab version means we need to add lnrhotab!'
		for j=0,2 do  tab(*,*,j,Nlam) = tab(*,*,j,Nlam) + lnrhotab
	endif
;
; Diagnostic output
	if (NOT KEYWORD_SET(QUIET)) then radtabinfo, tabfile, info=info
	if KEYWORD_SET(PRINTBINS) then begin
		if (Nxopt(0) LE 5) then begin
			print, '  x1    xx    x2    lmbd1   lmbd2'
;			print, transpose(arr(0:Nlam-1,3+Nlam*3:*)), $
			print, transpose(arr(0:Nlam-1, Na-5L:*)), $
									form='(3(f5.2,","),f7.0,",",f8.0,",")'
		endif else begin
			print, '     lambda/[AA]     weight/[AA]'
			print, transpose(arr), form='(2f16.6)'
		endelse
	endif
;
; Unit conversions
	if KEYWORD_SET(CGS) then begin
		if (NOT KEYWORD_SET(QUIET)) then print, " Using cgs units."
		uur  = 1e0 & uul = 1e0 & uut = 1e0
	endif else begin
		if ((fstat(1)).open) then begin
			if (NOT KEYWORD_SET(QUIET)) then $
							print, " Converting to simulation units."
			uur  = cpar(0,'uur') & uul = cpar(0,'uul') & uut = cpar(0,'uut')
		endif else begin
			print, " WARNING: Using cgs units."
			uur  = 1e0 & uul = 1e0 & uut = 1e0
		endelse
	endelse
;
; Conversion factors for converting from table to simulation units.
	ulnabs = alog(uul/rul) & ulnB = -alog((uur/rur)*((uul/rul)/(uut/rut))^3)
	ulnr   = alog(rur/uur)
	if (abs(ulnB) GT 1e-3) then $
					tab(*,*,1L,0L:Nlam-1L) = tab(*,*,1L,0L:Nlam-1L) + ulnB
	if (abs(ulnabs) GT 1e-3) then tab(*,*,*,Nlam) = tab(*,*,*,Nlam) + ulnabs
	if (abs(ulnr) GT 1e-3) then begin
		lnrtb    = lnrtb    + ulnr & lnrmin=lnrmin+ulnr & lnrmax=lnrmax+ulnr
		lnrhotab = lnrhotab + ulnr
	endif
;
; Reset the table unit conversion factor
	rur = uur & rul = uul & rut = uut
;
; Fill-up the ctab1 common-block
	rhm  = exp(lnrtb) & drhm = lnrtb(1)-lnrtb(0)
	thmax= 5039.7475/exp(lntmin)+0*rhm &  thmin = 5039.7475/exp(lntmax)+0*rhm
    kaprhoTLoaded = 1b
;
; Stop before returning, to make all variables available to the user
; Normally the common-block, cradtab, should suffice.
    if KEYWORD_SET(STOP) then stop

    if KEYWORD_SET(PLOT) then begin
        default, az, 190.
		default, tvcolortable, 23L
		c0   = (!D.TABLE_SIZE-1.)*.75
		c1   = (!D.TABLE_SIZE-1.)*.5
;
; Change to base 10 logarithm and cgs units
		ln10  = alog(1e1)
        LTtb  = lnttb/ln10
        Lrtb  = lnrtb/ln10 + alog10(uur)
;
; Titles for the axes, names of variables and character size
        Ttitl = 'log!d10!i !n!8T!x'
        rtitl = 'log!d10!i !n!7q!x'
        names = ['relative opacities','source functions','epsilons']
        ztitl = ['log!d10!i !n!7j!8!dj!n/!7j!x!dRoss!n', $
                 'log!d10!i !n!7e!8!dj!nB!dj!n!x', $
                 'log!d10!i !n<1-!n!7e!x>!8!dj!n!x']
;
; Conversion factors from simulation units to cgs units.
		ulg   = [0., alog10(uur*(uul/uut)^3), 0., alog10((uul/rul)*(uur/rur))]

		if KEYWORD_SET(ONED) then goto, oneD
        Pcs0        = !P.charsize
        !P.charsize = 1.5*!P.charsize
;
; Loop over quantities, x_j, \eps_j*B_j, \eps_j
        for k=0L, 2L do begin
;
; Plot each of the three monocromatic quantities
; The range of the (logarithmic) quantity
            lgtab = reform(tab(*,*,k,0:Nlam-1L)/ln10+ulg(k))
            zr = [min(lgtab(where(finite(lgtab))), max=maxz), maxz]
;			zr = [-0.63089031,9.7761889]
            if (zr(0) EQ zr(1)) then begin
;
; Don't bother plotting if they are all the same value (e.g., epsilon in LTE)
                print, " All "+names(k)+"=",1e1^zr(0)," Skipping the plot."
            endif else begin
                LTpr = LTtb((NTtab-1.)*.05)
                LRpr = Lrtb(Nrtab-1)
                zpr  = (zr(1)-zr(0))*.93 + zr(0)
                sAA  = '!3!i !n'+string("305b)+'!x'
;
; Loop over wavelength/bin
                for j=0, Nlam-1L do begin
                    shade_surf, lgtab(*,*,j), LTtb, Lrtb, az=az, $
						ztitl=ztitl(k), xtitl=Ttitl, ytitl=rtitl, zr=zr, $
						xsty=1, ysty=1, xmarg=[5,2], ymarg=[2,2], zsty=1, /SAVE
                    if (Nxopt(0) GE 6L) then spr=string(j,arr(j,0),"305b, $
                                            form='(I0,": ",F0.1,A1)')+sAA $
                                        else spr=string(j,form='(I0)')
                    xyouts, LTpr, Lrpr, spr, z=zpr, text_axes=1, /T3D
;					if (k EQ 0L) then begin
;						gifname=string(Nxopt(0:1),j, $
;							form='("opac",2I1,"bin",I0,".gif")')
;						write_gif, gifname, tvrd()
;					endif
;
; Wait for the user before proceeding to the next plot + provide exit
                    if (j LT (Nlam-1L)) then begin
                        s = prompt(" Next wavelength? [y/n/q/s]: ", /ONELINE)
                        if (s EQ 'n')               then begin
                            print, ''& break
                        endif
                        if (s EQ 'q'  OR  s EQ 'r') then begin
                            print, ''& reset_graphics& return& endif
                        if (s EQ 's')               then stop
                    endif
;
; End of loop over wavelength/bin
                endfor
;
; Wait for the user before proceeding to the next quantity + provide exit
                if (k LT 2L) then begin
                    s = prompt(" Plot "+names(k+1)+"? [y/n/q/s]: ")
					if (s EQ 'n'  OR  s EQ 'q'  OR  s EQ 'r') then begin
						reset_graphics& return& endif
					if (s EQ 's')                             then stop
                endif
            endelse
;
; End of loop over quantities
        endfor

		s = prompt(" Check total of source functions? [y/n/q/s]: ")
		if (s EQ 'n'  OR  s EQ 'q'  OR  s EQ 'r') then begin
			reset_graphics& return& endif
		if (s EQ 's')                             then stop
        Stbtot = total(exp(reform(tab(*,*,1,0:Nlam-1L))), 3)
        surface, alog10(Stbtot), LTtb, Lrtb, ysty=1, /SAVE, $
            xtitl=Ttitl, ytitl=rtitl, ztitl='log!d10!n!8B!x!dtot!n'
        surface, 4.*LTtb#replicate(1.,Nrtab)+alog10(sig/!pi)+ulnB/ln10, $
            LTtb, Lrtb, /T3D, col=c1, yr=!y.crange, ysty=1, $
            xr=!x.crange, xsty=1, zr=!z.crange, zsty=1, /NOERASE
;
; Plot the auxilliary opacities on the same z-scale
        lgtab = reform((tab(*,*,*,Nlam) - $
                [[[lnrhotab]],[[lnrhotab]],[[lnrhotab]]])/ln10+ulg(3))
        zr = [min(lgtab(where(finite(lgtab))), max=maxz), maxz]
        names = ['Rosseland','Planck','5000A monochr.']+' opacity'
        ztitl = ['log!d10!n!7j!x!dRoss!n', $
                 'log!d10!n!7j!x!dPlanck!n', 'log!d10!n!7j!x!d5000!n']
        for k=0L, 2L do begin
;
; Wait for the user before proceeding to the next plot + provide exit
            s = prompt(" Plot "+names(k)+"? [y/n/q/s]: ")
            if (s EQ 'q'  OR  s EQ 'r') then begin
				reset_graphics& return& endif
            if (s EQ 'n')               then continue
            if (s EQ 's')               then stop
;
; Surface plot of the opacity
            shade_surf, lgtab(*,*,k), LTtb, Lrtb, az=30, $
				xsty=1, ysty=1, zsty=1, xtitl=Ttitl, ytitl=rtitl, $
				ztitl=ztitl(k), zr=zr, xmarg=[5,2], ymarg=[2,2]
        endfor
        !P.charsize = Pcs0
		s = prompt(" TV of whole table? [y/n/q/s]: ")
		if (s EQ 'n'  OR  s EQ 'q'  OR  s EQ 'r') then begin
			reset_graphics& return& endif
		if (s EQ 's')                             then stop

		opacrho0 = 0*tab(*,*,0,0)+tab(*,*,0,Nlam) - lnrhotab
; Find the current color-table index and load the TVCOLORTABLE
		icurr = currentct(/QUIET, ntables=ntables)
		tvcolortable = (tvcolortable>0)<(ntables-1L)
		loadct, tvcolortable, /SILENT
        erase
; Image of the Rosseland opacity
		img_plot, opacrho0, /SCALE
		plot, LTtb,Lrtb,ysty=1,/NOERASE,/NODATA
	    tmpfile = '../../sun/run/sun.tmp'
	    if (file_test(tmpfile)) then begin
			tabtmp, tmpfile, mz=82L
			plots, alog10(5040./thmax), alog10(rhm)-7., th=2
			plots, alog10(5040./thmin), alog10(rhm)-7., th=2
		endif
		plots, alog10(arr(*,1)), alog10(arr(*,0))

		s = prompt(" Surface-plot of whole table? [y/n/q/s]: ")
		if (s EQ 'n'  OR  s EQ 'q'  OR  s EQ 'r') then begin
			loadct, icurr, /SILENT& reset_graphics& return& endif
		if (s EQ 's')                             then stop
		reset_graphics
;
; Revert to previous color-table
		loadct, icurr, /SILENT
		surface, opacrho0/ln10, Lttb,Lrtb,ysty=1, zr=[-4,6], az=30, $
												xr=[3.2,4.4],yr=[-12.5,-4.5]
oneD:
      if (Nxopt(0L) LT 6) then begin
		if ((fstat(1)).open) then Teff=cpar(0,'teff')   else   Teff=5777.7
		if (Nxopt(1L) EQ 1) then begin
			if (NOT KEYWORD_SET(ONED)) then begin
				s = prompt(" Compare integrated source functions? [y/n/q/s]: ")
				if (s EQ 'n'  OR  s EQ 'q'  OR  s EQ 'r') then begin
					reset_graphics& return& endif
				if (s EQ 's')                             then stop
			endif
			plot, LTtb, 4*LTtb+alog10(sig/!dpi), ysty=1, xtitl=Ttitl, $
												ytitl='log!d10!n!8B!x!dtot!n'
			oplot, LTtb#replicate(1,Nrtab), $
				alog10(total(exp(tab(*,*,1,0:Nlam-1)),4)),psym=3,col=c0
			oplot, [1,1]*alog10(Teff), !y.crange, line=1
		endif
		if (NOT KEYWORD_SET(ONED)  OR  Nxopt(1L) EQ 1) then begin
			s = prompt(" Compare with sun.tmp? [y/n/q/s]: ")
			if (s EQ 'n'  OR  s EQ 'q'  OR  s EQ 'r') then begin
				reset_graphics& return& endif
			if (s EQ 's')                             then stop
		endif

; Plot the intensive quantities; \kappa_x/[cm^2 g^-1]
;		print, ul,ur,ut,Nxrev(1), arr(0,0)
		lnr  = alog(arr(*,0)/uur);/[sun.tmp = sim]
		lnrc = alog(arr(*,0));/[kaprhoT.tab = cgs]
		tt   =      arr(*,1)
		lntt = alog(tt)
		default, xrange, [min(lntt, max=maxlnt), maxlnt]/ln10

; log10(\kappa_Ross) from kap-rhoT.tab
		lgkR = (lookup_rad(lnr, lntt, 0, Nlam)-lnr)/ln10
		lnx  = fltarr(N_ELEMENTS(lgkR), Nlam)& Btot=0*tt& xtot=Btot& xnrm=Btot
; Bin-wise ln(\kappa_i^eff/\kappa_Ross)
		for i=0, Nlam-1L do begin
			lnx(*,i) = lookup_rad(lnr, lntt, 0, i)
			lnBi     = lookup_rad(lnr, lntt, 1, i)
			dBdT     = derf(lntt, lnBi)*exp(lnBi-lntt)
			xtot     = xtot + dBdT*exp(-lnx(*,i))
			xnrm     = xnrm + dBdT
; Btot does sum-up to \sigma/\pi T^4
			Btot     = Btot + exp(lnBi)
		endfor
		ily = 0L
; log10(\kappa_1) - opacity of the first bin
		lgk  = lnx(*,0)/ln10 + lgkR
		plot,  lntt/ln10, lgk, line=1, ysty=1, xr=xrange, $
						yr=[min([[lnx/ln10],[lgkR]], max=yr2), yr2], /NODATA
		oplot, [1,1]*alog10(Teff), !y.crange, line=1
		label, .3, .95-ily*.04, 0, 'kbin0(kaprhoT)', col=c0&	ily = ily + 1L
		label, .3, .95-ily*.04, 2, 'Ross(kaprhoT)', col=c0&		ily = ily + 1L
		oplot, lntt/ln10, lgk,  col=c0
		oplot, lntt/ln10, lgkR, col=c0, line=2
		for i=0, Nlam-1L do oplot, lntt/ln10, lnx(*,i)/ln10, line=2
		oplot, lntt/ln10, lgk-lgkR, line=2
		label, .3, .95-ily*.04, 2, 'xbin0(kaprhoT)'&			ily = ily + 1L
		for i=0L, Nlam-1L do oplot, lntt/ln10, arr(*,i+3)/ln10
		label, .3, .95-ily*.04, 0, 'xcorr(kaprhoT)'&			ily = ily + 1L
;		oplot, lntt/ln10, -alog10(total(exp(-lnx), 2)), line=5, col=c0
;		label, .3, .95-ily*.04, 5, 'Ross!3_!xeff(kaprhoT)', col=c0& ily=ily+1L
;		oplot, [1,1]*alog10(5777.), !y.crange, line=1
;		oplot, [1,1]*alog10(12234.6), !y.crange, line=1
		oplot, lntt/ln10, alog10(xnrm/xtot), col=c1, th=2
		label, .3, .95-ily*.04, 0, 'Ross!3_!xeff(kaprhoT)', col=c1, thick=2
		ily = ily + 1L
		oplot, !x.crange, [1,1]*0, line=1
      endif else begin
; We are looking at an opacity sampling (OS) table
; Make a surface for all temperature at a solar photospheric density
        dum = min(abs(lnrtb-1.15129), ir0)
        shade_surf, reform(tab(*,ir0,0,0:Nlam-1L)), LTtb, reform(arr(*,0)), $
            /YLOG, az=-100, ysty=1, xsty=1, zsty=1, az=az
      endelse
    endif

	if KEYWORD_SET(STOP) then stop
	reset_graphics
end
