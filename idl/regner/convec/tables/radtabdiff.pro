;
;   Program to compare two tables of radiative quantities used for the
;   convection simulations. The differences are plotted as  tab2-tab1.
;
;   Arguments (defaults in [...]):
;    tab2file:  Full name of the second table file [no default].
;    tab1file:  Full name of the reference table file ['kaprhoT.tab'].
;    yrange:    yrange of the 1D plot (if ONED is set) [<range of plotted vars>]
;    QUIET:     Set this keyword to suppress printing of table info.
;    ONED:      Set this keyword to plot quantities along the meanrhoT structure
;    STOP:      Set this keyword to stop before returning, to make all
;               variables available to the user (for debugging).
;               Normally the common-block, cradtab, should suffice.
;
;   Update history:
;   ---------------
;   10.03.2008 Coded/RT
;   11.03.2008 Added common-block and in-lined 'default' and 'prompt'/RT
;   07.04.2008 Renamed to 'radtab', changed from surface -> shade_surf /RT
;	19.08.2008 Can now handle comparisons of tables with differing dimensions
;              and coverage. Passing QUIET keyword to radtab calls. Now handling
;              possible differences in unit-conversion factors correctly/RT
;
;   Known bugs: None
;
pro radtabdiff, tab2file, tab1file=tab1file, yrange=yrange, QUIET=QUIET, $
													ONED=ONED, STOP=STOP
	common cradtab,NTtab,Nrtab,Ma,Na,Nlam,Nelem,Nxopt,Nxrev,$
		date,time,lnrmin,lnrmax,lntmin,lntmax,ul,ur,ut,iel,abund,arr,tab
	common ctabl1,rhm,drhm,thmin,thmax
;
; A reasonable guess for the name of the table
    default, tab1file, 'kaprhoT.tab'
    rt_graphics
;
; Open the 1st table file
    radtab, tab1file, QUIET=QUIET
	arr1 = arr& Nlam1 = Nlam
	ul1=ul& ur1=ur& ut1=ut
;
; lnT and ln\rho of the 1st table
    lnttb1 = findgen(NTtab)/(NTtab-1.)*(lntmax-lntmin) + lntmin
    lnrtb1 = findgen(Nrtab)/(Nrtab-1.)*(lnrmax-lnrmin) + lnrmin
;
; Make sure that we have \kappe*\rho in the last three entries
;   cdrv = abs(Nxrev(1))
;	if (cdrv LT 73  OR  cdrv EQ 93  OR  cdrv EQ 106) then begin
;		for i=0,2 do tab(*,*,i,Nlam)=tab(*,*,i,Nlam)+replicate(1,NTtab)#lnrtb1
;	endif
	tab1 = tab
;
; Open the 2nd table file
    radtab, tab2file, QUIET=QUIET
	ul2=ul& ur2=ur& ut2=ut
;
; lnT and ln\rho of the 2nd table
    lnttb2 = findgen(NTtab)/(NTtab-1.)*(lntmax-lntmin) + lntmin
    lnrtb2 = findgen(Nrtab)/(Nrtab-1.)*(lnrmax-lnrmin) + lnrmin
;
; Make sure that we have \kappe*\rho in the last three entries
;   cdrv = abs(Nxrev(1))
;	if (cdrv LT 73  OR  cdrv EQ 93  OR  cdrv EQ 106) then begin
;		for i=0,2 do tab(*,*,i,Nlam)=tab(*,*,i,Nlam)+replicate(1,NTtab)#lnrtb2
;	endif
	Na = min([Nlam, Nlam1])
	if (Nxopt(0) LE 5) then begin
		arr(*,3:Na-1) = arr(*,3:Na-1) - arr1(*,3:Na-1)
	endif else begin
		arr(0:Na-1,*) = arr(0:Na-1,*) - arr1(0:Na-1,*)
	endelse
;
; Change tab1 to the units of tab2
	if (ul1 NE ul2  OR  ur1 NE ur2) then begin
; The pseudo source functions, ln(\eps_j*B_j)
		tab1(*,*,1,0:Nlam1-1) = tab1(*,*,1,0:Nlam1-1) + $
								alog((ur1/ur2)*((ul1/ul2)/(ut1/ut2))^3)
; ln\kappa_Ross, ln\kappa_Planck, ln\kappa_5000
		tab1(*,*,*,Nlam1)     = tab1(*,*,*,Nlam1)     + alog(ul2/ul1)
	endif
;
; Description of various versions of the table content - padded at the
; beginning with "" to allow for Fortran style indexing beginning at 1.
    sopctype = ["", "Opacity binning, scaled",		$
					"Opacity binning","","","",		$
					"Opacity sampling","","","",""]
    ssrctype = ["", "Pure Planck function", "Scattering included","",""]
    jtype    = ["", replicate("bins", 5), replicate("wavelengths",5)]
;
; Find out whether the tables cover the same range or we need interpolations.
; Find the maximum range covered by both tables.
	rhointrp = 0  &  Tintrp = 0
	if (max(abs(lnrtb1-lnrtb2)) GT 1e-3) then begin
		print, "WARNING: Interpolating in ln\rho."
		lnrmin = max([lnrmin, lnrtb1(0)])
		lnrmax = min([lnrmax, max(lnrtb1)])
		rhointrp = 1
	endif
	if (max(abs(lnttb1-lnttb2)) GT 1e-3) then begin
		print, "WARNING: Interpolating in lnT."
		lntmin = max([lntmin, lnttb1(0)])
		lntmax = min([lntmax, max(lnttb1)])
		Tintrp = 1
	endif
    lnttb = findgen(NTtab)/(NTtab-1.)*(lntmax-lntmin) + lntmin
    lnrtb = findgen(Nrtab)/(Nrtab-1.)*(lnrmax-lnrmin) + lnrmin
;
; Perform interpolations if necessary
	if (rhointrp OR Tintrp) then begin
		dtab = fltarr(NTtab,Nrtab,3,Nlam+1L)
	endif else begin
		dtab = fltarr(NTtab,Nrtab,3,Nlam+1L)
		dtab(*,*,*,0:Nlam-1) = tab(*,*,*,0:Nlam-1) - tab1(*,*,*,0:Nlam-1);   tab2-tab1
		dtab(*,*,*,Nlam)     = tab(*,*,*,  Nlam)   - tab1(*,*,*,  Nlam1); last 3 entries
	endelse

	if (rhointrp AND Tintrp) then begin
;
; Simultaneous interpolation in T and \rho - bi (monotonic) cubic interpolation.
		for j=0L, Nlam do begin
			j1 = j & if (j EQ Nlam) then j1=Nlam1
			for k=0, 2 do begin
				dtab(*,*,k,j) =bicubic(lnttb,lnrtb, tab(*,*,k,j),lnttb2,lnrtb2,/GRID)$
							  -bicubic(lnttb,lnrtb,tab1(*,*,k,j1),lnttb1,lnrtb1,/GRID)
			endfor
		endfor
	endif else begin
		if (rhointrp) then begin
;
; Monotonic cubic interpolation in \rho
			for j=0L, Nlam do begin
				j1 = j & if (j EQ Nlam) then j1=Nlam1
				for k=0, 2 do begin
					for i=0, NTtab-1L do begin
						dtab(i,*,k,j) = intrp1(lnrtb2,lnrtb, tab(i,*,k,j),/MCU)$
									  - intrp1(lnrtb1,lnrtb,tab1(i,*,k,j1),/MCU)
					endfor
				endfor
			endfor
		endif
		if (Tintrp)  then begin
;
; Monotonic cubic interpolation in T
			for j=0L, Nlam do begin
				j1 = j & if (j EQ Nlam) then j1=Nlam1
				for k=0, 2 do begin
					for i=0, Nrtab-1L do begin
						dtab(*,i,k,j) = intrp1(lnttb2,lnttb, tab(*,i,k,j),/MCU)$
									  - intrp1(lnttb1,lnttb,tab1(*,i,k,j1),/MCU)
					endfor
				endfor
			endfor
		endif
	endelse
;
; Change to base 10 logarithm and cgs units
	ln10  = alog(1e1)
	LTtb  = lnttb/ln10
	Lrtb  = lnrtb/ln10 + alog10(ur)
;
; Titles for the axes, names of variables and character size
	Ttitl = 'log!d10 !n!8T!x'
	rtitl = 'log!d10 !n!7q!x'
	names = ['relative opacities','source functions','epsilons']
	ztitl = '!7D!x' + ['ln!7j!8!dj!n/!7j!x!dRoss!n', $
			 'ln!7e!8!dj!nB!dj!n!x', 'ln!7e!8!dj!n!x']

	if KEYWORD_SET(ONED) then goto, oneD
	Pcs0        = !P.charsize
	!P.charsize = 1.5*!P.charsize
;
; Loop over quantities, x_j, \eps_j*B_j, \eps_j
	for k=0L, 2L do begin
;
; Plot each of the three monocromatic quantities
; The range of the (logarithmic) quantity
        ii = where(finite(dtab(*,*,k,0:Nlam-1L)) EQ 1b)
		zr = [min((dtab(*,*,k,0:Nlam-1L))(ii), max=maxz), maxz]
;		zr = [-0.63089031,9.7761889]
		if (zr(0) EQ zr(1)) then begin
;
; Don't bother plotting if they are all the same value (e.g., epsilon in LTE)
			print, " All Delta "+names(k)+"=",zr(0)," Skipping the plot."
		endif else begin
            LTpr = LTtb((NTtab-1.)*.05)
            LRpr = Lrtb(Nrtab-1)
            zpr  = (zr(1)-zr(0))*.93 + zr(0)
            sAA  = '!3!i !n'+string("305b)+'!x'
;
; Loop over wavelength/bin
			for j=0, Nlam-1L do begin
				shade_surf, dtab(*,*,k,j), LTtb, Lrtb, ztitl=ztitl(k), $
                    xtitl=Ttitl, ytitl=rtitl, zr=zr, xsty=1,ysty=1,zsty=1,/SAVE
                if (Nxopt(0) GE 6L) then spr=string(j,arr1(j,0),"305b, $
                                        form='(I0,": ",F0.1,A1)')+sAA $
                                    else spr=string(j,form='(I0)')
                xyouts, LTpr, Lrpr, spr, z=zpr, text_axes=1, /T3D
;				if (k EQ 0L) then begin
;					gifname=string(Nxopt(0:1),j, $
;						form='("opac",2I1,"bin",I0,".gif")')
;					write_gif, gifname, tvrd()
;				endif
;
; Wait for the user before proceeding to the next plot + provide exit
				if (j LT (Nlam-1L)) then begin
					s = prompt(" Next wavelength? [y/n/q/s]: ", /ONELINE)
					if (s EQ 'n')               then begin
                        print, ''& break& endif
                    if (s EQ 'q'  OR  s EQ 'r') then begin
						print,''& reset_graphics& return& endif
					if (s EQ 's')                             then stop
				endif
;
; End of loop over wavelength/bin
			endfor
;
; Wait for the user before proceeding to the next quantity + provide exit
			if (k LT 2L) then begin
				s = prompt(" Plot Delta "+names(k+1)+"? [y/n/q/s]: ")
                if (s EQ 'n')               then begin
                    print, ''& break& endif
				if (s EQ 'q'  OR  s EQ 'r') then begin
					reset_graphics& return& endif
				if (s EQ 's')               then stop
			endif
		endelse
;
; End of loop over quantities
	endfor
;
; Plot the auxilliary opacities on the same z-scale
    ii = where(finite(dtab(*,*,*,Nlam-1L)) EQ 1b)
	zr = [min((dtab(*,*,*,Nlam))(ii), max=maxz), maxz]
	names = ['Rosseland','Planck','5000A monochr.']+' opacity'
	ztitl = '!7D!x' + ['ln!7j!x!dRoss!n', $
			 'ln!7j!x!dPlanck!n', 'ln!7j!x!d5000!n']
	for k=0L, 2L do begin
;
; Wait for the user before proceeding to the next plot + provide exit
		s = prompt(" Plot "+names(k)+"? [y/n/q/s]: ")
		if (s EQ 'q'  OR  s EQ 'r') then begin
			reset_graphics& return& endif
		if (s EQ 'n')               then continue
		if (s EQ 's')               then stop
;
; Surface plot of the opacity
		shade_surf, dtab(*,*,k,Nlam), LTtb, Lrtb, ztitl=ztitl(k), $
			xtitl=Ttitl, ytitl=rtitl, zr=zr, xsty=1, ysty=1, zsty=1
	endfor
	!P.charsize = Pcs0
	s = prompt(" TV of whole table? [y/n/q/s]: ")
	if (s EQ 'n'  OR  s EQ 'q'  OR  s EQ 'r') then begin
		reset_graphics& return& endif
	if (s EQ 's')                             then stop

	erase
	img_plot, dtab(*,*,0,4), /SCALE
	plot, LTtb,Lrtb,ysty=1,/NOERASE,/NODATA,xtitl=Ttitl,ytitl=rtitl
	color_scale, min(dtab(*,*,0,4), max=dkmax), dkmax, x0=.05, y0=.4, $
									height=.24, title=ztitl(0)
	tabtmp,'../../sun/run/sun.tmp',mz=82L
	plots, alog10(5040./thmax), alog10(rhm)-7., th=2
	plots, alog10(5040./thmin), alog10(rhm)-7., th=2
	plots, alog10(arr(*,1)), alog10(arr(*,0)),  th=2
	Nc = 21L
	cthick = replicate(1, Nc)& cthick((Nc-1)/2) = 2
	clines =[replicate(2, (Nc-1)/2), replicate(0, (Nc-1)/2+1)]
	contour, /OVERPLOT, 1e2*dtab(*,*,0,4), LTtb,Lrtb, c_linesty=clines, $
	levels=findgen(Nc)-(Nc-1.)/2., c_thick=cthick, /FOLLOW, $
	c_colors=replicate(.3*(!D.TABLE_SIZE-1.), Nc)

	s = prompt(" Surface-plot of whole table? [y/n/q/s]: ")
	if (s EQ 'n'  OR  s EQ 'q'  OR  s EQ 'r') then begin
		reset_graphics& return& endif
	if (s EQ 's')                             then stop
	!P.position = 0
	surface, dtab(*,*,0,4), Lttb,Lrtb,ysty=1, zsty=1, az=40, $
		xr=[3.2,4.4],yr=[-12.5,-4.5],xtitl=Ttitl, ytitl=rtitl, ztitl=ztitl(0)

	s = prompt(" Compare with sun.tmp? [y/n/q/s]: ")
	if (s EQ 'n'  OR  s EQ 'q'  OR  s EQ 'r') then begin
		reset_graphics& return& endif
	if (s EQ 's')                             then stop

oneD:
	c0   = (!D.TABLE_SIZE-1.)*.7
	lnrc = alog(arr(*,0));/[cgs]
	tt   =      arr(*,1)
	tab  = dtab
	dlnkR = lookup_rad(lnrc, alog(tt), 0, 4)
	dlnx  = fltarr(N_ELEMENTS(dlnkR), Nlam)
	for i=0, Nlam-1L do $
		dlnx(*,i) = lookup_rad(lnrc, alog(tt), 0, i)
	default,yrange,[min([transpose(dlnkR), transpose(dlnx)],max=ymax), ymax]
	plot,  alog10(tt), dlnkR, yr=yrange, ysty=1, xtitl=Ttitl, $
			ytitl=tab2file+'!i !n-!i !n'+tab1file
	label, .3, .83, 0, '!7D!xlnRoss(kaprhoT)'
	for i=0, Nlam-1L do oplot, alog10(tt), dlnx(*,i), line=5
	label, .3, .79, 5, '!7D!xlnx!di!n(kaprhoT)'
	for i=3L, Na-1L do oplot, alog10(tt), arr(*,i), col=c0, line=2
	label, .3, .75, 2, '!7D!xlnxcorr(kaprhoT)', col=c0

	oplot, !x.crange, [1,1]*0, line=1
	oplot, [1,1]*alog10(5777.), !y.crange, line=1
	oplot, [1,1]*alog10(12234.6), !y.crange, line=1
;
; Stop before returning, to make all variables available to the user
; Normally the common-block, cradtab, should suffice.
	if KEYWORD_SET(STOP) then stop
	reset_graphics
end
