;
;   Program to load and possibly plot a table of EOS and opacity quantities
;   used for the convection simulations. The bulk of the table, contained
;   in 'tab', is organized as an Netab*Nrtab*3*Ntbvar array of the Ntbvar
;   variables; lnPtot, ln\kap_Ross, lnT, ln\kap_Planck,ln\kap_5000, and
;   arranged as; the variable, and derivatives w.r.t. lnee, and ln\rho.
;     All table entries are functions of lnee and lnrho which are both
;   equidistant grids, with extent and resolution as given in the header.
;     All units of quantities in the table are simulation units, as given
;   by ul(length), ur(density) and ut(time) w.r.t. cgs-units.
;   N.B. All plots are in cgs-units.
;     Use s/r lookup_eos.pro to interpolate in the table.
;
;   Arguments (defaults in [...]):
;    tabfile:    Full name of the table file ['EOSrhoe.tab'].
;    PLOT:       Set this keyword to plot all the table entries.
;    QUIET:      Set this keyword to suppress printing of table info.
;    SIM:        Set this keyword to plot everything in [sim]-units and ln
;                instead of log10. Sets the PLOT keyword.
;    CGS:        Set this keyword to force [cgs] units despite loaded sim.
;    CHECKDERIV: Set this keyword to check the derivatives in the table
;                against numerical derivatives. Sets the PLOT keyword.
;    IMAGE:      Set this keyword to display images of the table variables.
;    STOP:       Set this keyword to stop before returning, to make all
;                variables available to the user (for debugging).
;                Normally the common-block, ceostab, should suffice.
;
;   Update history:
;   ---------------
;   07.04.2008 Adopted from radtab2/RT
;   08.04.2008 Now also works for Ntbvar=6, i.e., with ln(Ne) included/RT
;   11.04.2008 New SIM-keyword, to plot everything in [sim]-units and ln, not
;              log10, regardless of which units the table was converted to/RT
;   15.12.2010 New CHECKDERIV-keyword, to check table derivatives against
;              numerical ones. Both this and /SIM now sets /PLOT./RT
;   12.04.2012 Added keyword SSTAB to add an entropy entry with eosstab/RT
;   22.10.2015 Added filling-in of the common-block of the old table, ctable/RT
;
;   Known bugs: None
;
pro default, var, val
    if n_elements(var) eq 0 then var=val
end

function prompt, stra, ONELINE=ONELINE
    print, stra, form='(A,$)'
    answ=get_kbrd(1)
    if KEYWORD_SET(ONELINE) then begin
        print, string(13b), form='(A0,$)'
    endif else begin
        if ((byte(answ))(0) NE 10) then print,answ  else print,''
    endelse
    return, answ
end

pro eostab, tabfile, iS=iS, PLOT=PLOT, QUIET=QUIET, SIM=SIM, CGS=CGS, $
    SSTAB=SSTAB, CHECKDERIV=CHECKDERIV, REPAIR_Ne_DERIV=REPAIR_Ne_DERIV, $
    IMAGE=IMAGE, STOP=STOP

    common ctable,ttmean,rhm,drhm,eemin,eemax,dee,mtab,itab,oldtab
	common ceostab,Netab,Nrtab,Ma,Na,Ntbvar,Nelem,Nxopt,Nxrev,$
		date,time,lnrmin,lnrmax,lnemin,lnemax,ul,ur,ut,iel,abund,arr,tab
    common currenttable, EOSrhoeLoaded, kaprhoTLoaded
;
; A reasonable guess for the name of the table
    default, tabfile, 'EOSrhoe.tab'
;
; Open the table file
    openr, lun, tabfile, /GET_LUN, /F77, SWAP_IF_LITTLE_ENDIAN=!big, $
                                         SWAP_IF_BIG_ENDIAN=1b-!big
;
; Declare all (long) integers before reading
    Netab=0L& Nrtab=0L& Ma=0L& Na=0L& Ntbvar=0L& Nelem=0L
    Nxopt=lonarr(20)& Nxrev=Nxopt
;
; Read the dimensions of the arrays to follow.
    readu, lun, Netab,Nrtab,Ma,Na,Ntbvar,Nelem,Nxopt,Nxrev
    Nrtab0= Nrtab
    Nrtab = 57L
    print, Nrtab0, Nrtab
;
; Dimension the arrays
	ul    = 0e0 & ur=ul & ut=ul
    arr   = fltarr(Ma,Na)
    iel   = replicate("    ",Nelem)
    abund = fltarr(Nelem)
    date  = "        " & time=date
    tab   = fltarr(Netab,Nrtab,3,Ntbvar)
;
; Read the arrays of the header
    readu, lun, date,time,lnrmin,lnrmax,lnemin,lnemax,ul,ur,ut,iel,abund,arr
;
; Read the bulk of the table, one wavelength/bin at a time
	readu, lun, tab
;
; Close the file
    free_lun, lun
    Nrtab = Nrtab0
    tab   = tab(*,0:Nrtab-1L,*,*)
;
; lne and ln\rho of the table
   dlnetb = (lnemax-lnemin)/(Netab-1.)
    lnetb = findgen(Netab)*dlnetb + lnemin
   dlnrtb = (lnrmax-lnrmin)/(Nrtab-1.)
    lnrtb = findgen(Nrtab)*dlnrtb + lnrmin
;
; Diagnostic output
	if (NOT KEYWORD_SET(QUIET)) then begin
		print, " Loaded "+tabfile+" computed on "+date+" at "+time+"."
		print, " This is an EOS/opacity table with "+$
				string(Ntbvar,form='(I0)')+" entries."
        print, " This table was computed with tabinv_MHD.x v", Nxrev(1), $
                                                    form=  '(A0,I0)'
        if (Nxrev(3) LT 70) then linedat='odf.dat' $
                            else linedat=' OS.dat'
        print, " Versions of the atomic physics tables:" 
        print, " EOS.tab v",Nxrev(2),linedat,Nxrev(3),", subs.dat v",Nxrev(4), $
                                                    form='(3(A0,I0))'
        if (Nxopt(1) GT 0) then print, "Amended with OP`05 for logT>3.95"
	endif
;
; Repair the dlnNe/dlnrho derivative of the table
    if KEYWORD_SET(REPAIR_Ne_DERIV) then begin
        if (Ntbvar LT 6L) then begin
            print, "This table doesn't seem to have an lnNe entry, and " + $
                   "therefore no derivative to repair."
        endif else begin
            ir = where(lnrtb GE -15.  AND  lnrtb LE -10.)
            if (abs(aver(tab(0,ir,2,3))-0.3) LT 0.1) then begin
                print, "This table seems to have dlnNe/dlnrho already repaired."
            endif else begin
; Fix for wrong dlnNe/dln\rho in the table
                tab(*,*,2,3) = tab(*,*,2,3) - dlnrtb
                backup, tabfile, ext='bad-dlnNedlnr'
                print, " Backed up the table to "+tabfile+".bad-dlnNedlnr"
                print, " and wrote the corrected table to "+tabfile+"."
                openw, lun, tabfile, /GET_LUN, /F77,SWAP_IF_LITTLE_ENDIAN=!big,$
                                                    SWAP_IF_BIG_ENDIAN=1b-!big
                writeu,lun, Netab,Nrtab,Ma,Na,Ntbvar,Nelem,Nxopt,Nxrev
                writeu,lun, date,time,lnrmin,lnrmax,lnemin,lnemax,ul,ur,ut,iel,$
                                                                       abund,arr
                writeu,lun, tab
                free_lun, lun
            endelse
        endelse
    endif
;
; Unit conversions
    if KEYWORD_SET(CGS) then begin
        if (NOT KEYWORD_SET(QUIET)) then print, " Using cgs units."
        uur  = 1e0 & uul = 1e0 & uut = 1e0
    endif else begin
        if ((fstat(1)).open) then begin
            if (NOT KEYWORD_SET(QUIET)) then $
                            print, " Converting to simulation units."
            uur  = cpar(0,'uur') & uul = cpar(0,'uul') & uut = cpar(0,'uut')
        endif else begin
            print, " WARNING: Using cgs units."
            uur  = 1e0 & uul = 1e0 & uut = 1e0
        endelse
    endelse
;
; Conversion factors for converting from table to simulation units.
	ulnr  = alog(ur/uur)  & ulne = 2.*alog((ul/uul)/(ut/uut))
    ulnP  = ulnr + ulne   & ulnabs = alog(uul/ul)
	ikp = [1L, Ntbvar-[2L,1L]]
    if (abs(ulnr) GT 1e-3) then begin
        lnrtb    = lnrtb    + ulnr & lnrmin=lnrmin+ulnr & lnrmax=lnrmax+ulnr
    endif
    if (abs(ulne) GT 1e-3) then begin
		lnetb    = lnetb    + ulne & lnemin=lnemin+ulne & lnemax=lnemax+ulne
    endif
	if (abs(ulnP)   GT 1e-3) then tab(*,*,0,0)   = tab(*,*,0,0)   + ulnP
	if (abs(ulnabs) GT 1e-3) then tab(*,*,0,ikp) = tab(*,*,0,ikp) + ulnabs
;
; Reset the table unit conversion factor
	ur = uur & ul = uul & ut = uut
    EOSrhoeLoaded = 1b
;
; Fill-in the common-block for the old table
    rhm   = exp(lnrtb)
    eemin = replicate(exp(lnemin), Nrtab)
    eemax = replicate(exp(lnemax), Nrtab)
    dee   = replicate((eemax(0)-eemin(0))/Netab, Nrtab)
    mtab  = replicate(Netab, Nrtab)
;
; Compute entropy if SSTAB is set
    if KEYWORD_SET(SSTAB) then eosstab, iS
;
; Stop before returning, to make all variables available to the user
; Normally the common-block, ceostab, should suffice.
    if KEYWORD_SET(STOP) then stop

    if (KEYWORD_SET(SIM)  OR  KEYWORD_SET(CHECKDERIV) $
                          OR  KEYWORD_SET(IMAGE))       then PLOT=1
    if KEYWORD_SET(PLOT) then begin
        rt_graphics
;
; Change to base 10 logarithm and cgs units
		ln10  = alog(1e1)
		uE    = (ul/ut)^2
		uP    = ur*uE
		uk    = 1./(ul*ur)
		if KEYWORD_SET(SIM) then begin
; Titles for the axes and dependent variables in the appropriate units.
			etitl = 'ln!7e!3/[!xsim!3]!x'
			rtitl = 'ln!7q!3/[!xsim!3]!x'
			Letb  = lnetb
			Lrtb  = lnrtb
		endif else begin
			etitl = 'log!d10!n!7e!3/[!xcgs!3]!x'
			rtitl = 'log!d10!n!7q!3/[!xcgs!3]!x'
			Letb  = lnetb/ln10 + alog10(uE)
			Lrtb  = lnrtb/ln10 + alog10(ur)
		endelse
;
; Titles for the z-axis, names of variables and character size
        Ntbv = Ntbvar-KEYWORD_SET(SSTAB)
		case Ntbv OF
			5: begin
			names = ['Pressure', 'Rosseland opacity', 'Temperature', $
					 'Planck opacity', '5000'+string(197b)+' opacity']
			ztitl = ['!8p!x!dtot!n', '!7j!x!dRoss!n', $
					 '!8T!x', $
					 '!7j!x!dPlanck!n','!7j!x!d5000!n']
			ulv   = [[alog10([uP, uk, 1, uk, uk])], $
					 [replicate(0,Ntbv)], [replicate(0,Ntbv)]]
			   end
			6: begin
			names = ['Pressure', 'Rosseland opacity', 'Temperature', $
					 'Electron density', 'Planck opacity', $
					 '5000'+string(197b)+' opacity']
			ztitl = ['!8p!x!dtot!n', '!7j!x!dRoss!n', $
					 '!8T!x', '!8N!x!de!n', $
					 '!7j!x!dPlanck!n','!7j!x!d5000!n']
			ulv   = [[alog10([uP, uk, 1, 1, uk, uk])], $
					 [replicate(0,Ntbv)], [replicate(0,Ntbv)]]
			   end
		else: begin
				print, " ERROR: An Ntbvar other than 5 or 6 is not supported."
				print, "        Bailing out!"
				stop
			  end
		endcase
        if KEYWORD_SET(SSTAB) then begin
            names = [names, 'Entropy']
            ztitl = [ztitl, '!8S!x']
            ulv   = transpose([[transpose(ulv)], [0, 0, 0]])
;           tab(*,*,1,Ntbvar-1L) = tab(*,*,1,Ntbvar-1L)/tab(*,*,0,Ntbvar-1L)
;           tab(*,*,2,Ntbvar-1L) = tab(*,*,2,Ntbvar-1L)/tab(*,*,0,Ntbvar-1L)
;           tab(*,*,0,Ntbvar-1L) = alog10(tab(*,*,0,Ntbvar-1L))
        endif
		dd    = ['', 'd', 'd']
		if KEYWORD_SET(SIM) then begin
			ztitl(0L:Ntbv-1L) = 'ln!i !n' + ztitl(0L:Ntbv-1L)
			fd = replicate(1, Ntbvar)#[   1, 1, 1] 
			ulv(*,*) = 0
			dtitl = ['', '/dln!i !n!7e!x', '/dln!i !n!7q!x']
			dname = ['', 'ln ee-', 'ln rho-']
		endif else begin
			ztitl(0L:Ntbv-1L) = 'log!d10!i !n' + ztitl(0L:Ntbv-1L)
			fd = replicate(1, Ntbvar)#[ln10, 1, 1]
            if KEYWORD_SET(SSTAB) then fd(iS,*)=[1, ln10, ln10]
			dtitl = ['', '/dlog!d10 !n!7e!x', '/dlog!d10 !n!7q!x']
			dname = ['', 'log ee-', 'log rho-']
		endelse
        Pcs0        = !P.charsize
        if (NOT KEYWORD_SET(IMAGE)) then  !P.charsize = 1.5*!P.charsize
        c0          = .7*(!D.TABLE_SIZE-1.)
;
; Array to hold numerical derivatives if CHECKDERIV is set
        df = reform(tab(*,*,0,0))
;
; Loop over quantities, x_j, \eps_j*B_j, \eps_j
		for j=0, Ntbvar-1L do begin
;
; Plot each of the three monocromatic quantities
;
; Loop over wavelength/bin
			for k=0L, 2L do begin
                if KEYWORD_SET(IMAGE) then begin
                    erase& loadct, 23, /SILENT
                    img_plot, tab(*,*,k,j)/fd(j,k)+ulv(j,k), /SCALE,height=.85,$
                        .16
                    plot, Letb([0L,Netab-1L]), Lrtb([0L,Nrtab-1L]), /NOERASE, $
                        /NODATA,xsty=1,ysty=1,xtitl=etitl, $
                        ytitl='            '+rtitl,titl=dd(k)+ztitl(j)+dtitl(k)
                    color_scale, x0=0.06, y0=!y.window(0),height=.35, $
                        min(tab(*,*,k,j)/fd(j,k)+ulv(j,k), max=mxf), mxf, $
                        titl=dd(k)+ztitl(j)+dtitl(k)
                endif else begin
                    shade_surf, tab(*,*,k,j)/fd(j,k)+ulv(j,k), Letb, Lrtb, $
                        ztitl=dd(k)+ztitl(j)+dtitl(k), $
                        xtitl=etitl, ytitl=rtitl, xsty=1, ysty=1, zsty=1, /SAVE
                endelse
                if (KEYWORD_SET(SSTAB)  AND  k EQ 0L) then begin
                  if (j EQ iS) then begin
                    eetb  = 1e1^Letb#replicate(1, 57L)
                    rhotb = replicate(1,300L)#(1e1^Lrtb)
                    TT    = 1e1^reform(tab(*,*,k,2)/fd(k)+ulv(2,k))
                    SS    = tab(*,*,k,j)

                    s = prompt("Helmholtz free energy? [y/n/q/s]: ")
					if (s EQ 'n'  OR  s EQ 'q'  OR  s EQ 'r') then begin
						reset_graphics& return& endif
					if (s EQ 's')                             then stop
                    shade_surf, eetb*rhotb-TT*SS, Letb, Lrtb
                  endif
                endif
                if KEYWORD_SET(CHECKDERIV) then begin
                    if (k EQ 1L) then begin
                        for i=0L, Nrtab-1L do $
                            df(*,i)=derf(lnetb,reform(tab(*,i,0,j)))*dlnetb
                        surface, df/fd(k)+ulv(j,k), Letb, Lrtb, $
                            /T3D, xr=!x.crange, xsty=1, yr=!y.crange, ysty=1, $
                            zr=!z.crange, zsty=1, /NOERASE
                    endif
                    if (k EQ 2L) then begin
                        for i=0L, Netab-1L do $
                            df(i,*)=derf(lnrtb,reform(tab(i,*,0,j)))*dlnrtb
                        surface, df/fd(k)+ulv(j,k), Letb, Lrtb, $
                            /T3D, xr=!x.crange, xsty=1, yr=!y.crange, ysty=1, $
                            zr=!z.crange, zsty=1, /NOERASE
                    endif
                endif
;
; Wait for the user before proceeding to the next plot + provide exit
                if (k LT 2L) then begin
                    s = prompt(dname(k+1)+"deriv. of "+names(j)+"? [y/n/q/s]: ")
					if (s EQ 'n'  OR  s EQ 'q'  OR  s EQ 'r') then begin
						reset_graphics& return& endif
					if (s EQ 's')                             then stop
                endif
;
; End of loop over wavelength/bin
            endfor
;
; Wait for the user before proceeding to the next quantity + provide exit
            if (j LT (Ntbvar-1)) then begin
                s = prompt(" Plot "+names(j+1)+"? [y/n/q/s]: ")
				if (s EQ 'n'  OR  s EQ 'q'  OR  s EQ 'r') then begin
					reset_graphics& return& endif
				if (s EQ 's')                             then stop
            endif
;
; End of loop over quantities
        endfor
;
        !P.charsize = Pcs0
    endif
end
