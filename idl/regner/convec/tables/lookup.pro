;
;   Program to lookup entries in the EOS and opacity table for a simulation.
;
;   Update history:
;   ---------------  .
;   ??.??.19?? Coded/Ake
;   13.02.2007 Automatically conform to 'EOSrhoe.tab' if loaded/RT
;   05.12.2007 Fixed division by dee in calculation of dvdecr/RT
;   02.04.2012 Will now correctly divert to lookup_eos if a EOSrhoe-table
;              is loaded, based on EOSrhoeLoaded of the new c/b currenttable/RT
;
;   Known bugs: None
;
FUNCTION nint,x
  ix=long(x+.5)
  j=where(x lt 0,n)
  if n gt 0 then ix(j)=ix(j)-1
  return,ix
END

FUNCTION lookup,lnrho,ee,dvdecr,dvdrce,iv=iv,mz=mz,status=status,STOP=STOP

      common cdat,x,y,z,nx,ny,nz,mp,ntmax,date0,time0
      common ctable,ttmean,rhm,drhm,eemin,eemax,dee,mtab,itab,tab
      common currenttable, EOSrhoeLoaded, kaprhoTLoaded

      if n_params() eq 0 then begin
		print,'lookup,lnrho,ee[,dvdecr,dvdrce],mz=mz,iv=iv'
		print,'  dvdecr is the partial der of the answ wrt ee at const lnr'
		print,'  dvdrce is the partial der of the answ wrt lnr at const ee'
		return,0
      endif

      if n_elements(iv) eq 0 then iv=0
      if n_elements(mz) eq 0 then mz=n_elements(rhm)
;
;  If the loaded table is a EOSrhoe-table, use the lookup_eos routine.
      if (EOSrhoeLoaded) then begin
        tabvar =   lookup_eos(lnrho, alog(ee), iv, 0L)
        if (iv EQ 2) then tabvar = exp(tabvar)
;
;  Energy derivative
        if (n_params() GE 3) then begin
          dvdecr = lookup_eos(lnrho, alog(ee), iv, 1L)/ee;  dv/dee
          if (iv EQ 2) then dvdecr = dvdecr*tabvar;         dT/dee
        endif
;
;  Density derivative
        if (n_params() GE 4) then begin
          dvdrce = lookup_eos(lnrho, alog(ee), iv, 2L);     dv/dlnr
          if (iv EQ 2) then dvdecr = dvdecr*tabvar;         dT/dlnr
        endif
        if KEYWORD_SET(STOP) then stop
        return,tabvar
      endif

      rhm1=alog(rhm(0))
      rhm2=alog(rhm(mz-1))
      drhm=(rhm2-rhm1)/(mz-1)
      s=size(lnrho)
      lnrho1=lnrho
	  EOSrhoe = (min(eemin) LT 0.0)
      if (EOSrhoe) then  ee1=alog(ee)  else  ee1=ee
;	  if (EOSrhoe) then  print, '   Interpolating in ln(ee)'
      rhm11=lnrho1
      drhm1=rhm11
      if s(0) eq 3 then begin
        rhm11(*,*,*)=rhm1
        drhm1(*,*,*)=drhm
      endif else if s(0) eq 2 then begin
        rhm11(*,*)=rhm1
        drhm1(*,*)=drhm
      endif else begin
        rhm11=rhm1
        drhm1=drhm
      endelse
;
;  density index
;
      algrk=(lnrho1-rhm11)/drhm1
      np=((long(algrk) > 0) < (mz-2))
      px=algrk-np
      qx=1.-px
      pxqx=px*qx

      pxmax = max(px,imaxp)
      qxmax = max(qx,imaxq)
      status=0L
      if (pxmax GT 1) then begin
		status=status+1L
		print,lnrho(imaxp),ee(imaxp),pxmax,qx(imaxp), $
			form='("WARNING: lnr-extrapolation: lnrho,ee,pxmax,qx=",4f8.3)'
	  endif
	  if (qxmax GT 1) then begin
		status=status+1L
		print,lnrho(imaxq),ee(imaxq),px(imaxq),qxmax, $
			form='("WARNING: lnr-extrapolation: lnrho,ee,px,qxmax=",4f8.3)'
      endif
;
;  Interpolate the vars and dvar/dlnrho at smaller lnrho
;
      ntab=mtab(np)
      ltab=3*ntab
      eek=(ee1-eemin(np))/dee(np)
      kee=((long(eek) > 0) < (ntab-2))
      py=eek-kee
      qy=1.-py
      pyqy=py*qy

      pymax = max(py,imaxp)
      qymax = max(qy,imaxq)
      if (pymax GT 1) then begin
		status=status+1L
		print,lnrho(imaxp),ee(imaxp),pymax,qy(imaxp), $
			form='("WARNING: ee-extrapolation:  lnrho,ee,pymax,qy=",4f8.3)'
	  endif
	  if (qymax GT 1) then begin
		status=status+1L
		print,lnrho(imaxq),ee(imaxq),py(imaxq),qymax, $
			form='("WARNING: ee-extrapolation:  lnrho,ee,py,qymax=",4f8.3)'
      endif

      ik=itab(np)+kee-1
      ntab1=mtab(np+1)
      eek1=(ee1-eemin(np+1))/dee(np+1)
      ;kee1=((long(eek1) > 0) < (ntab1-2))
      kee1=kee-nint((eemin(np+1)-eemin(np))/dee(np))
      py1=eek1-kee1
      if (max(abs(py1-py)) gt 0.1) then begin
	    print, 'Address problems in lookup. max(abs(py1-py))=', max(abs(py1-py))
	    stop
      end
      kee1=(kee1 > 0) < (ntab1-2)
      ik1=itab(np+1)+kee1-1
;
;  Get ik for variable desired
;
      ik =ik +iv*3*ntab
      ik1=ik1+iv*3*ntab1
;
      dfx1=tab(ik1)-tab(ik)
      dfx2=tab(ik1+1)-tab(ik+1)
      dfy1=tab(ik+1)-tab(ik)
      dfy2=tab(ik1+1)-tab(ik1)
;
      tabvar = $
          qy * (qx * (tab(ik           ) + $
               pxqx* (tab(ik +2*ntab   ) - dfx1) + $
               pyqy* (tab(ik +  ntab   ) - dfy1) ) + $
                px * (tab(ik1          ) - $
               pxqx* (tab(ik1+2*ntab1  ) - dfx1) + $
               pyqy* (tab(ik1+  ntab1  ) - dfy2) ) ) + $
          py * (px * (tab(ik1        +1) - $
               pxqx* (tab(ik1+2*ntab1+1) - dfx2) - $
               pyqy* (tab(ik1+  ntab1+1) - dfy2) ) + $
                qx * (tab(ik         +1) + $
               pxqx* (tab(ik +2*ntab +1) - dfx2) - $
               pyqy* (tab(ik +  ntab +1) - dfy1) ) )
;
;  exponentiate lnT when reading EOSrhoe-tables.
	  if (EOSrhoe  AND  iv EQ 2) then tabvar = exp(tabvar)
;
;  Energy derivative
;
	  if KEYWORD_SET(STOP) then stop
      if n_params() lt 3 then return,tabvar
      dvdecr = qx * (   - (tab(ik           )       ) + $
     (1.-4.*py+3.*py^2) * (tab(ik +  ntab   ) - dfy1) + $
                (-pxqx) * (tab(ik +2*ntab   ) - dfx1) + $
                          (tab(ik         +1)       ) - $
        (2.*py-3.*py^2) * (tab(ik +  ntab +1) - dfy1) + $
                   pxqx * (tab(ik +2*ntab +1) - dfx2) )/dee(np) + $
               px * (     (tab(ik1        +1)       ) - $
        (2.*py-3.*py^2) * (tab(ik1+  ntab1+1) - dfy2) - $
                   pxqx * (tab(ik1+2*ntab1+1) - dfx2) - $
                          (tab(ik1          )       ) + $
     (1.-4.*py+3.*py^2) * (tab(ik1+  ntab1  ) - dfy2) - $
                (-pxqx) * (tab(ik1+2*ntab1  ) - dfx1) )/dee(np+1)
;
      if (EOSrhoe)               then  dvdecr=dvdecr/ee
	  if (EOSrhoe  AND  iv EQ 2) then  dvdecr=dvdecr*tabvar
      ; stat,dvdecr
      if n_params() lt 4 then return,tabvar
;  Density derivative
;
      dvdrce = qy * (   - (tab(ik           )       ) + $   ; 00
        (qx*qx-2.*pxqx) * (tab(ik +2*ntab   ) - dfx1) + $
                (-pyqy) * (tab(ik +  ntab   ) - dfy1) + $
                          (tab(ik1          )       ) + $   ; 10
        (px*px-2.*pxqx) * (tab(ik1+2*ntab1  ) - dfx1) + $
                  pyqy  * (tab(ik1+  ntab1  ) - dfy2) ) + $
               py * (   - (tab(ik         +1)       ) + $   ; 01
        (qx*qx-2.*pxqx) * (tab(ik +2*ntab +1) - dfx2) + $
                  pyqy  * (tab(ik +  ntab +1) - dfy1) + $
                          (tab(ik1        +1)       ) + $   ; 11
        (px*px-2.*pxqx) * (tab(ik1+2*ntab1+1) - dfx2) + $
                (-pyqy) * (tab(ik1+  ntab1+1) - dfy2) )
;
;  Dimensional factors
;
      dlnrho = drhm
      dvdrce = dvdrce/dlnrho
	  if (EOSrhoe  AND  iv EQ 2) then  dvdrce=dvdrce*tabvar
      ; stat,dvdecr
;
      return,tabvar
end
