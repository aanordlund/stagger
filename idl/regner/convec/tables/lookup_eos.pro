;
;   Program to perform bi-cubic interpolation in the EOSrhoe.tab-tables.
;   The results are consistent with OS2code/lookup.f, lookup_eos.pro and
;   math/bicubic.pro to within +/-6e-6 as tested on lnT of sune42.scr,
;   on 19.05.2009.
;
;   Arguments:
;    lnrho:      ln(density) in table units (cf. ur in common ceostab).
;    lne:        ln(internal energy per mass/[sim]).
;    j:          index of variable: 0=lnPtot, 1=ln\kap_Ross, 2=lnT,
;                3=ln\kap_Planck, 4=ln\kap_5000.
;    i:          derivative index; 0=f, 1=df/dlne, 2=df/dlnrho
;    status:     Is >0 when extrapolations were needed.
;    LINEAR:     Set this keyword to perform linear interpolation even when i=0.
;    QUIET:      Set this keyword to suppress warnings of extrapolations.
;    STOP:       Set this keyword to stop before returning, to make all
;                variables available to the user (for debugging).
;    Returns:    Array of same format as lnrho and lne contaning the
;                interpolated values of the specified table-entry.
;
;   Update history:
;   ---------------
;   07.04.2008 Ported from lookup_rad/RT
;   03.03.2009 Re-ordered terms and renamed a few variables, to be consistent
;              with the function 'bicubic'/RT
;
;   Known bugs: None
;
function lookup_eos, lnrho, lne, j, i, status=status, LINEAR=LINEAR, $
														QUIET=QUIET, STOP=STOP
	common ceostab,Netab,Nrtab,Ma,Na,Ntbvar,Nelem,Nxopt,Nxrev,$
		date,time,lnrmin,lnrmax,lnemin,lnemax,ul,ur,ut,iel,abund,arr,tab

    if n_params() eq 0 then begin
		print,'f=lookup_eos(lnrho, lne, var index, deriv index)'
		return,0
    endif
	default, j, 0
	default, i, 0

	if KEYWORD_SET(LINEAR) then ii=i  else ii=0
	tb     = tab(*,*,ii,j)

    dlnrtb = (lnrmax-lnrmin)/(Nrtab-1.)
	dlnetb = (lnemax-lnemin)/(Netab-1.)
;
;  Density index
;
	ri     = (lnrho-lnrmin)/dlnrtb
	ir     = ((long(ri) > 0) < (Nrtab-2))
	py     = ri-ir
	qy     = 1.-py

	pymax  = max(py,imaxp)
	qymax  = max(qy,imaxq)
;
;  extrapolation?
;
	status=0
	if (pymax GT 1.) then begin
		status=status+1
		if (NOT KEYWORD_SET(QUIET)) then $
			print,lnrho(imaxp),lne(imaxp),pymax,qy(imaxp), $
				form='("WARNING: lnr-extrapolation: lnrho,lne,pymax,qy=",4f8.3)'
	endif
	if (qymax GT 1.) then begin
		status=status+1
		if (NOT KEYWORD_SET(QUIET)) then $
			print,lnrho(imaxq),lne(imaxq),py(imaxq),qymax, $
				form='("WARNING: lnr-extrapolation: lnrho,lne,py,qymax=",4f8.3)'
	endif
;
;  Energy index
;
	ei     = (lne-lnemin)/dlnetb
	ie     = ((long(ei) > 0) < (Netab-2))
	px     = ei-ie
	qx     = 1.-px

	pxmax  = max(px,imaxp)
	qxmax  = max(qx,imaxq)
;
;  extrapolation?
;
	if (pxmax GT 1) then begin
		status=status+1
		if (NOT KEYWORD_SET(QUIET)) then $
			print,lnrho(imaxp),lne(imaxp),pxmax,qx(imaxp), $
				form='("WARNING: lne-extrapolation: lnrho,lne,pxmax,qx=",4f8.3)'
	endif
	if (qxmax GT 1) then begin
		status=status+1
		if (NOT KEYWORD_SET(QUIET)) then $
			print,lnrho(imaxq),lnrho(imaxq),px(imaxq),qxmax, $
				form='("WARNING: lne-extrapolation: lnrho,lne,px,qxmax=",4f8.3)'
	endif
	if (NOT KEYWORD_SET(LINEAR)) then begin
		tbx  = tab(*,*,1,j)
		tby  = tab(*,*,2,j)
; x-terms
		pxqx = px*qx
		dfx1 = tb(ie+1, ir  ) - tb(ie  , ir  )
		dfx2 = tb(ie+1, ir+1) - tb(ie  , ir+1)
; y-terms
		pyqy = py*qy
		dfy1 = tb(ie  , ir+1) - tb(ie  , ir  )
		dfy2 = tb(ie+1, ir+1) - tb(ie+1, ir  )
	endif
;
;  Perform the interpolation
;
	if (NOT KEYWORD_SET(LINEAR)) then begin
		case i of
			0: begin
				tabvar = $
			   qx * (qy * (tb (ie  , ir  )               $
				  +	pxqx* (tbx(ie  , ir  ) - dfx1)       $
				  +	pyqy* (tby(ie  , ir  ) - dfy1) )     $
				  +	 py * (tb (ie  , ir+1)               $
				  +	pxqx* (tbx(ie  , ir+1) - dfx2)       $
				  -	pyqy* (tby(ie  , ir+1) - dfy1) ) )   $
			 + px * (py * (tb (ie+1, ir+1)               $
				  -	pxqx* (tbx(ie+1, ir+1) - dfx2)       $
				  -	pyqy* (tby(ie+1, ir+1) - dfy2) )     $
				  +	 qy * (tb (ie+1, ir  )               $
				  -	pxqx* (tbx(ie+1, ir  ) - dfx1)       $
				  +	pyqy* (tby(ie+1, ir  ) - dfy2) ) )
			   end
			1: begin;		(dlnP/dlne)_rho		d/dx
                if (NOT KEYWORD_SET(QUIET)) then $
                    print, "Computing lne-derivatives"
				tabvar = $
               qy * (   - (tb (ie  , ir  )       ) +     $
        (qx*qx-2.*pxqx) * (tbx(ie  , ir  ) - dfx1) +     $
                (-pyqy) * (tby(ie  , ir  ) - dfy1) +     $
                          (tb (ie+1, ir  )       ) +     $
        (px*px-2.*pxqx) * (tbx(ie+1, ir  ) - dfx1) +     $
                  pyqy  * (tby(ie+1, ir  ) - dfy2) ) +   $
               py * (     (tb (ie+1, ir+1)       ) +     $
        (px*px-2.*pxqx) * (tbx(ie+1, ir+1) - dfx2) +     $
                (-pyqy) * (tby(ie+1, ir+1) - dfy2) -     $
                          (tb (ie  , ir+1)       ) +     $
        (qx*qx-2.*pxqx) * (tbx(ie  , ir+1) - dfx2) +     $
                  pyqy  * (tby(ie  , ir+1) - dfy1) )
			   end
			2: begin;		(dlnP/dlnr)_ee		d/dy
                if (NOT KEYWORD_SET(QUIET)) then $
                    print, "Computing lnr-derivatives"
				tabvar = $
			   qx * (   - (tb (ie  , ir  )       ) +     $
                (-pxqx) * (tbx(ie  , ir  ) - dfx1) +     $
        (qy*qy-2.*pyqy) * (tby(ie  , ir  ) - dfy1) +     $
                          (tb (ie  , ir+1)       ) +     $
                  pxqx  * (tbx(ie  , ir+1) - dfx2) +     $
        (py*py-2.*pyqy) * (tby(ie  , ir+1) - dfy1) ) +   $
               px * (   - (tb (ie+1, ir  )       ) +     $
                  pxqx  * (tbx(ie+1, ir  ) - dfx1) +     $
        (qy*qy-2.*pyqy) * (tby(ie+1, ir  ) - dfy2) +     $
                          (tb (ie+1, ir+1)       ) +     $
                (-pxqx) * (tbx(ie+1, ir+1) - dfx2) +     $
        (py*py-2.*pyqy) * (tby(ie+1, ir+1) - dfy2) )
			   end
			else: begin
				print, "WARNING: Calling lookup_eos with i=",i, $
							" Doesn't make sense. Should be i=[0,1,2]"
				print, "      Bailing out!"
				return, 0
			   end
		endcase
	endif else begin
		tabvar = qy * (qx * tb(ie  , ir  ) + px * tb(ie+1, ir  ) ) + $
				 py * (px * tb(ie+1, ir+1) + qx * tb(ie  , ir+1) )
	endelse
;
;  The table contains derivatives multiplied by the grid-spacing, e.g.,
;     (dlnP/dlne)*\Del(lne)  - this is what's needed for the interpolations.
;  Divide by \Del(lne) and \Del(lnrho), respectively, to get the proper
;  thermodynamic derivatives.
	if (i EQ 1) then tabvar=tabvar/dlnetb
	if (i EQ 2) then tabvar=tabvar/dlnrtb
;
	if KEYWORD_SET(STOP) then stop
;
	return, tabvar
end
