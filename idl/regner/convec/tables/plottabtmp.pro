;
;   Program to make images + surface plots of entire simulation rho-/T-tables.
;
;   Command arguments (defaults in [...]):
;    iv0:          Index of the variable to plot - 0-nvar [1].
;    tmpfile:      Name of the tmp-table-file [stamnavn().tmp].
;    scrfile:      Name of the scratch-file [stamnavn().scr].
;    az:           Azimuthal angle (in degrees) of the surface plot [40deg].
;    tvcolortable: Color-table index for the image [23].
;    GRID:         Whether to use gridded or shaded surface.
;    STOP:         If this keyword is set, stop at the end of plotting.
;
;   Update history
;   ---------------
;   14.06.1995 Coded/RT
;   02.02.2004 Gracefull handling of out-of-bounds iv/RT
;   09.02.2004 Introduced defaultscr => speed-up + versatility/RT
;   06.04.2004 Introduced GRID-keyword and extra_args for versatility/RT
;   06.04.2004 Use different color-table for the image/RT
;   15.04.2004 Use new read_meanrhot program to read 'meanrhoT.F77.dat'-files
;   20.06.2007 Added /QUIET keyword to all lookup_tmp calls/RT
;
pro plottabtmp, iv0=iv0, tmpfile=tmpfile, scrfile=scrfile, az=az, GRID=GRID, $
		tvcolortable=tvcolortable, CHECKDERIV=CHECKDERIV, _EXTRA=extra_args, $
		STOP=STOP
	common cdat,x,y,z,nx,ny,nz,nw,ntmax,date0,time0
	common ctabl1,rhm,drhm,thmin,thmax,dth,mtab,itab,tab

	default, iv0, 1.0
	default, az, 40
	default, tvcolortable, 23L
    rt_graphics
	iv = long(iv0*3.+.5)
	iimax = 501 & rf = 1 & x0 = 165 & y0 = 70
	cs = 0.75
;The overall charactersize in units of the default.
    !P.charsize=cs*!P.charsize
;The xyouts charactersize in units of the overall size.
    cs = 1.0*!P.charsize
; Find the tmp-file
    default, tmpfile, stamnavn()
    if (strpos(tmpfile, '.') LT 0) then tmpfile=tmpfile+'.tmp'
; Find and load the scratch-file
	defaultscr, scrfile=scrfile;, /DEBUG
; Read mean structure from scr-file
	if (scrfile NE '') then begin
		Ni=500L & xi=findgen(Ni)/(Ni-1.)*(nz-1.)
		mTs=cdata(0,'ttm') & mlnrs=alog(cdata(0,'rhom'))
		mTsi   = intrp1(findgen(nz), xi, mTs)
		mlnrsi = intrp1(findgen(nz), xi, mlnrs)
	endif
; Read mean structure from meanrhoT-file
	fm = findfile('meanrhoT.F77.dat')
	if (read_meanrhot(mlnrm, mTm)) then begin
		nzm = N_ELEMENTS(mTm)
		Ni=500L & xi=findgen(Ni)/(Ni-1.)*(nzm-1.)
		mlnrm = alog(mlnrm)
		mTmi   = intrp1(findgen(nzm), xi, mTm)
		mlnrmi = intrp1(findgen(nzm), xi, mlnrm)
	endif
; Find tabcmp.log-files and find rho-/T-points that didn't
; converge in the EOS calculation (rhomke).
	Nprbl = 0L
	tmplogs = findfile("tabcmp*.log")
	if (tmplogs(0) NE '') then begin
		spawn, 'grep -c "^ 30 " '+tmplogs(0), Nprbl
		Nprbl = Nprbl(0)
		if (Nprbl GT 0) then begin
			print, " ***Found ", Nprbl, " non-convergent points***"
			A = fltarr(9,Nprbl)
			spawn, 'grep "^ 30 " '+tmplogs(0), res
			reads, res, A, form='(I3,8e11.4)'
		endif
	endif
; Load the tmp-file if not already loaded
	if ((fstat(2))(0).name NE tmpfile) then tabtmp, tmpfile, mz=nz

	nvar  = (itab(1)-itab(0))/mtab(0)/3L
	nbox  = nvar - 3L; & print, ' nvar, nbox: ', nvar, nbox
	slog  = 'ln'&  if (iv EQ 0  OR  iv EQ 3) then slog='log!d10!n'
	uk    = 'cm!u-1!n' &        if (iv EQ 3) then uk  ='cm!u2 !ng!u-1!n'
	vartit = [slog+'(P!dg!n/!3[!xdyn cm!u-2!n!3]!x)', $
			  slog+'(!7j!x/!3[!x'+uk+'!3]!x)', $
			  '!7e!x!n/!3[!x10!u12!nerg g!u-1!n!3]!x',$
			  'S!d'+string(lindgen(nbox),form='(I0)')+'!n']
	dtit = ['', '!s!u !n!Ad!n!r!s!3!u___!n!x!r!Bd!7H!x!n', $
				'!s !u !n!Ad!n!r!s!3!u_______!n!x!r!Bdln!7q!x!n']

	if (iv GT ((nvar-1L)*3L+2L) OR iv LT 0L) then begin
		print, " WARNING: 3*iv=",iv," outside range of [0-",(nvar-1L)*3L+2L, $
												form='(A0,I0,A0,I0,"]")'
		iv = (iv>0)<((nvar-1L)*3L)
		print, "          Using 3*iv=", iv, form='(A0,I0,".")'
	endif

	ntot = total(mtab)
	eet = fltarr(ntot)
	LTt = fltarr(ntot)
	Lrhot = fltarr(ntot)
	i0 = 0L
	for i=0L,nz-1L do begin
		tt = 5040./(thmin(i)+indgen(mtab(i))*dth(i))
;		ee = lookup_tmp(replicate(alog(rhm(i)),mtab(i)), tt, iv=iv)
		ee = tab(itab(i)+mtab(i)*iv-1L:itab(i)+mtab(i)*(iv+1L)-2L)
;		print, i, i0, i0+mtab(i)-1, N_ELEMENTS(tt)
		LTt(i0:i0+mtab(i)-1) = alog10(tt)
		Lrhot(i0:i0+mtab(i)-1) = alog10(rhm(i))-7.0
		eet(i0:i0+mtab(i)-1) = ee
		if (iv EQ 0) then eet(i0:i0+mtab(i)-1) = ee/alog(10.)+5.
		if (iv EQ 3) then eet(i0:i0+mtab(i)-1) =(ee-alog(rhm(i)))/alog(10.)-1.
		if ((iv mod 3) EQ 1) then eet(i0:i0+mtab(i)-1) = ee/dth(i); delf(delth)
		if ((iv mod 3) EQ 2) then eet(i0:i0+mtab(i)-1) = ee		  ; df/dlnr
		i0 = i0 + mtab(i)
	endfor
	triangulate, LTt, Lrhot, TR
	dLT = (max(LTt) - min(LTt))/(iimax-1.0001)
	LogT = min(LTt) + findgen(iimax)*dLT
	dLr = (max(Lrhot) - min(Lrhot))/(iimax-1.0001)
	Logrho = min(Lrhot) + findgen(iimax)*dLr
	minee = min(eet); fix(min(eet)-.99999)
	eefin = trigrid(LTt, Lrhot, eet, TR, [dLT,dLr], missing=minee)
	see = size(eefin) & ex = see(1) & ey = see(2)
;	print, ex, ey
	cx0 = (x0/2.-25.)/!D.x_vsize & cy0 = (y0+rf*iimax/2.-70.)/!D.y_vsize
	vartitle = dtit(iv mod 3)+vartit(iv/3)
; Find the current color-table index and load the TVCOLORTABLE
	icurr = currentct(/QUIET, ntables=ntables)
	tvcolortable = (tvcolortable>0)<(ntables-1L)
	erase
	loadct, tvcolortable, /SILENT
; Image of the table
	color_scale, min(eefin), max(eefin), x0=cx0, y0=cy0, tit=vartitle,height=.35
	tvscl, rebin(eefin, rf*ex, rf*ey), x0, y0
	!P.region=[x0, y0, x0+rf*ex, y0+rf*ey]
	plot, LTt, Lrhot, psym=3, /DEVICE, /NOERASE, tit=vartitle+'!c ', $
			xtit='log!d10 !n!8T!x', ytit='log!d10 !n!7q!x', ysty=1
	if (scrfile NE '') then begin
		plots,alog10(mTs),mlnrs/alog(1e1)-7,psym=1,syms=.2, line=2
		plots,alog10(mTsi),mlnrsi/alog(1e1)-7, line=2
		label, .2, .90, 2, scrfile, size=cs
	endif
	if (fm(0) NE '') then begin
		plots,alog10(mTm),mlnrm/alog(1e1)-7,psym=1,syms=.2
		plots,alog10(mTmi),mlnrmi/alog(1e1)-7
		label, .2, .95, 0, fm(0), size=cs
	endif
	if (Nprbl GT 0) then plots,alog10(A(1,*)),alog10(A(3,*)),psym=2,syms=.1

	if KEYWORD_SET(CHECKDERIV) then begin
		i0 = 0L &  dedx = fltarr(ntot)
		ivv = (iv/3)*3
		print, "Checking derivatives for iv=', ivv
;
; \Theta derivative
		if ((iv mod 3) EQ 1) then begin
			for i=0L,nz-1L do begin
				th = (thmin(i)+indgen(mtab(i))*dth(i))
				ee = tab(itab(i)+mtab(i)*ivv-1L:itab(i)+mtab(i)*(ivv+1L)-2L)
				dedx(i0:i0+mtab(i)-1) = deriv(th,ee)
				i0 = i0 + mtab(i)
			endfor
		endif
;
; ln\rho derivative
		if ((iv mod 3) EQ 2) then begin
			ths = 5040./(1d1^LTt)
			lnrs = (Lrhot+7.)*alog(1d1)
;			reset_graphics
;			plot, ths, lnrs, psym=2, syms=.3, ysty=1
;			plots, thmax, alog(rhm), psym=-7, col=120
;			plots, thmin, alog(rhm), psym=-7, col=120
			eps = 1d-2
			for i=0L,Ntot-1L do begin
				th  = 5040./(1d1^LTt(i))
				lnr = (Lrhot(i)+7.)*alog(1d1)
				ir  = long((lnr-alog(rhm(0)))/drhm + 0.5)
				irmin= min(where((th-eps) LE thmax $
							AND  (th+eps) GE thmin), max=irmax)
;				plots, th*[1,1], alog(rhm([irmin,irmax])), th=2, col=80
;				plots, th, lnr, psym=2, col=80
				ira = lindgen(irmax-irmin+1) + irmin
				lnra= alog(rhm(ira))
				TTa = replicate(5040./th, irmax-irmin+1)
				ee  = lookup_tmp(lnra, TTa, iv=ivv/3, /QUIET)
				dedx(i) = (deriv(lnra, ee))(ir-irmin)
;				plots, th*[1,1], alog(rhm([irmin,irmax])), th=2, col=0
;				plots, th, lnr, psym=2, col=0
;				plots, ths, lnrs, psym=2, syms=.3
			endfor
		endif
	endif
	stra = prompt(" Continue [Yes/Stop/Quit]? ")
	if (stra EQ 's') then begin
		loadct, icurr, /SILENT
		stop
	endif
	if (stra EQ 'q' OR stra EQ 'n' OR stra EQ 'r') then begin
		reset_graphics
		loadct, icurr, /SILENT
		return
	endif
	c0 = (!D.TABLE_SIZE-1.0)*.4

	!P.region = 0
	loadct, icurr, /SILENT
	if KEYWORD_SET(GRID) then begin
		surface,    eefin, LogT, Logrho, az=az, ztit=vartitle, chars=1.6*cs, $
			ysty=1, xtit='log!d10 !n!8T!x', ytit='log!d10 !n!7q!x', /SAVE, $
			_EXTRA=extra_args
		c1 = (!D.TABLE_SIZE-1.0)*.6
	endif else begin
		shade_surf, eefin, LogT, Logrho, az=az, ztit=vartitle, chars=1.6*cs, $
			ysty=1, xtit='log!d10 !n!8T!x', ytit='log!d10 !n!7q!x', /SAVE, $
			_EXTRA=extra_args
		c1 = (!D.TABLE_SIZE-1.0)*.9
	endelse
	plots, LTt, Lrhot, eet, /T3D, psym=3, col=c1
	label, 0.06, 0.87, 1, 'Table points', size=cs, col=c1
	if (KEYWORD_SET(CHECKDERIV)  AND  ((iv mod 3) GT 0)) then begin
		plots, LTt, Lrhot, dedx, /T3D, psym=2, col=c0, syms=.3
		label, 0.06, 0.84, 0, 'Numerical derivative', size=cs, psym=2, $
											   col=c0, syms=.3
	endif
; Overplot mean structure from scr-file
	if (scrfile NE '') then begin
		mf  = lookup_tmp(mlnrs, mTs, mdfdecr, mdfdrce, iv=iv/3, /QUIET)
		mfi = lookup_tmp(mlnrsi,mTsi,mdfdecri,mdfdrcei,iv=iv/3, /QUIET)
		if ((iv mod 3) EQ 1) then	mf=mdfdecr
		if ((iv mod 3) EQ 1) then	mfi=mdfdecri
		if ((iv mod 3) EQ 2) then	mf=mdfdrce
		if ((iv mod 3) EQ 2) then	mfi=mdfdrcei
		if (iv EQ 0) then			mf=mf/alog(1e1)+5.
		if (iv EQ 0) then			mfi=mfi/alog(1e1)+5.
		if (iv EQ 3) then			mf=(mf-mlnrs)/alog(10.)-1.
		if (iv EQ 3) then			mfi=(mfi-mlnrsi)/alog(10.)-1.
		plots,alog10(mTs),mlnrs/alog(1e1)-7,mf,psym=1,syms=.2,/T3D,line=2
		plots,alog10(mTsi),mlnrsi/alog(1e1)-7,mfi,/T3D,line=2
		label, 0.06, 0.90, 2, scrfile, size=cs
	endif
; Overplot mean structure from meanrhoT-file
	if (fm(0) NE '') then begin
		mf  = lookup_tmp(mlnrm, mTm, mdfdecr, mdfdrce, iv=iv/3, /QUIET)
		mfi = lookup_tmp(mlnrmi,mTmi,mdfdecri,mdfdrcei,iv=iv/3, /QUIET)
		if ((iv mod 3) EQ 1) then	mf=mdfdecr
		if ((iv mod 3) EQ 1) then	mfi=mdfdecri
		if ((iv mod 3) EQ 2) then	mf=mdfdrce
		if ((iv mod 3) EQ 2) then	mfi=mdfdrcei
		if (iv EQ 0) then			mf=mf/alog(1e1)+5.
		if (iv EQ 0) then			mfi=mfi/alog(1e1)+5.
		if (iv EQ 3) then			mf=(mf-mlnrm)/alog(10.)-1.
		if (iv EQ 3) then			mfi=(mfi-mlnrmi)/alog(10.)-1.
		plots,alog10(mTm),mlnrm/alog(1e1)-7,mf,psym=1,syms=.2,/T3D
		plots,alog10(mTmi),mlnrmi/alog(1e1)-7,mfi,/T3D
		label, 0.06, 0.93, 0, fm(0), size=cs
	endif
	if (Nprbl GT 0) then begin
		fprbl = lookup_tmp(alog(A(3,*)*1e7),A(1,*),dfpdecr,dfpdrce,iv=iv/3,/QUI)
		if ((iv mod 3) EQ 1) then	fprbl=dfpdecr
		if ((iv mod 3) EQ 2) then	fprbl=dfpdrce
		if (iv EQ 0) then			fprbl=fprbl/alog(1e1)+5.
		if (iv EQ 3) then			fprbl=(fprbl-alog(A(3,*)*1e7))/alog(10.)-1.
		plots,alog10(A(1,*)),alog10(A(3,*)),fprbl,psym=2,syms=.1,/T3D
	endif

	if KEYWORD_SET(STOP) then stop
	reset_graphics
end

;iv=0
;itb = (itab-itab(0))/(3L*7L)
;i=nz-1 & plot, LTt(itb(i):itb(i)+mtab(i)-1), $
;	tab(itab(i)-1+mtab(i)*iv:itab(i)-1+mtab(i)*(iv+1)-1),xr=[4,4.365],yr=[4,12]
;for i=0,nz-1 do oplot, LTt(itb(i):itb(i)+mtab(i)-1), $
;	tab(itab(i)-1+mtab(i)*iv:itab(i)-1+mtab(i)*(iv+1)-1)
