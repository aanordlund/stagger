;
;   Program to perform linear (or piece-wise cubic) interpolation in the
;   kaprhoT.tab-tables.
;
;   Arguments:
;    lnrho:      ln(density) in table units (cf. ur in common cradtab).
;    lnT:        ln(temperature/[K]).
;    i:          index of variable to look-up 0=ln(x_j), 1=ln(\eps_j*B_j)
;                and 2=ln(\eps_j).
;    j:          index of wavelength or opacity bin j = [0; Nlam-1L].
;                Choosing j=Nlam or -1 gives access to ln(\rho\kappa_Ross),
;                ln(\rho\kappa_Plck) and ln(\rho\kappa_5000) for i=0,1,2.
;    status:     Is >0 when extrapolations were needed.
;    CUBIC:      Set this keyword to perform cubic spline interpolation.
;    QUIET:      Set this keyword to suppress warnings of extrapolations.
;    STOP:       Set this keyword to stop before returning, to make all
;                variables available to the user (for debugging).
;    Returns:    Array of same format as lnrho and lnT contaning the
;                interpolated values of the specified table-entry.
;
;   Update history:
;   ---------------
;   25.10.2005 Coded/RT
;   28.10.2005 Changed ordering of tab to tab(NTtab,Nrho,Nvar)/RT
;   12.03.2008 Changed to new radtab format/RT
;   13.03.2008 Added keyword CUBIC for bi-cubic interpolation/RT
;   10.11.2008 Correctly handles unit-conversions - uses conversion units
;              from the presently open save-/scratch-file as a default/RT
;   27.11.2008 Unit conversions now handled when loading the table w/radtab/RT
;
;   Known bugs: None
;
function lookup_rad, lnrho, lnT, i, j, status=status, CUBIC=CUBIC, $
														QUIET=QUIET, STOP=STOP
	common cradtab,NTtab,Nrtab,Ma,Na,Nlam,Nelem,Nxopt,Nxrev,$
		date,time,lnrmin,lnrmax,lnTmin,lnTmax,ul,ur,ut,iel,abund,arr,tab

    if n_params() eq 0 then begin
		print,'f=lookup_rad(lnrho,lnT,i,j)'
		return,0
    endif
	if (j LT 0L) then j=Nlam

	tb    = tab(*,*,i,j)

    drhm  = (lnrmax-lnrmin)/(Nrtab-1.)
	dlnT  = (lnTmax-lnTmin)/(NTtab-1.)
;
;  density index
;
	ri    = (lnrho-lnrmin)/drhm
	ir    = ((long(ri) > 0) < (Nrtab-2))
	px    = ri-ir
	qx    = 1.-px

	pxmax = max(px,imaxp)
	qxmax = max(qx,imaxq)
;
;  extrapolation?
;
	status=0
	if (pxmax GT 1.) then begin
		status=status+1
		if (NOT KEYWORD_SET(QUIET)) then $
			print,lnrho(imaxp),lnT(imaxp),pxmax,qx(imaxp), $
				form='("WARNING: lnr-extrapolation: lnrho,lnT,pxmax,qx=",4f8.3)'
	endif
	if (qxmax GT 1.) then begin
		status=status+1
		if (NOT KEYWORD_SET(QUIET)) then $
			print,lnrho(imaxq),lnT(imaxq),px(imaxq),qxmax, $
				form='("WARNING: lnr-extrapolation: lnrho,lnT,px,qxmax=",4f8.3)'
	endif
;
;  temperature index
;
	Ti    = (lnT-lnTmin)/dlnT
	iT    = ((long(Ti) > 0) < (NTtab-2))
	py    = Ti-iT
	qy    = 1.-py

	pymax = max(py,imaxp)
	qymax = max(qy,imaxq)
;
;  extrapolation?
;
	if (pymax GT 1) then begin
		status=status+1
		if (NOT KEYWORD_SET(QUIET)) then $
			print,lnrho(imaxp),lnT(imaxp),pymax,qy(imaxp), $
				form='("WARNING: lnT-extrapolation: lnrho,lnT,pymax,qy=",4f8.3)'
	endif
	if (qymax GT 1) then begin
		status=status+1
		if (NOT KEYWORD_SET(QUIET)) then $
			print,lnrho(imaxq),lnT(imaxq),py(imaxq),qymax, $
				form='("WARNING: lnT-extrapolation: lnrho,lnT,py,qymax=",4f8.3)'
	endif
	if KEYWORD_SET(CUBIC) then begin
		tby = tb  & yy=findgen(NTtab)
		for i=0L, Nrtab-1L do tby(*,i)=derf(yy,reform(tb(*,i)))
		tbx = tb  & xx=findgen(Nrtab)
		for i=0L, NTtab-1L do tbx(i,*)=derf(xx,reform(tb(i,*)))
; x-terms
		pxqx = px*qx
		dfx1 = tb(iT  , ir+1) - tb(iT  , ir  )
		dfx2 = tb(iT+1, ir+1) - tb(iT+1, ir  )
; y-terms
		pyqy = py*qy
		dfy1 = tb(iT+1, ir  ) - tb(iT  , ir  )
		dfy2 = tb(iT+1, ir+1) - tb(iT  , ir+1)
	endif
;
;  Perform the interpolation
;
	if KEYWORD_SET(CUBIC) then begin
		tabvar = $
	   qy * (qx * (tb (iT  , ir  ) + $
			pxqx* (tbx(iT  , ir  ) - dfx1) + $
			pyqy* (tby(iT  , ir  ) - dfy1) ) + $
			 px * (tb (iT  , ir+1) - $
			pxqx* (tbx(iT  , ir+1) - dfx1) + $
			pyqy* (tby(iT  , ir+1) - dfy2) ) ) + $
	   py * (px * (tb (iT+1, ir+1) - $
			pxqx* (tbx(iT+1, ir+1) - dfx2) - $
			pyqy* (tby(iT+1, ir+1) - dfy2) ) + $
			 qx * (tb (iT+1, ir  ) + $
			pxqx* (tbx(iT+1, ir  ) - dfx2) - $
			pyqy* (tby(iT+1, ir  ) - dfy1) ) )
	endif else begin
		tabvar = qy * (qx * tb(iT  , ir  ) + px * tb(iT  , ir+1) ) + $
				 py * (px * tb(iT+1, ir+1) + qx * tb(iT+1, ir  ) )
	endelse
;
	if KEYWORD_SET(STOP) then stop
;
	return, tabvar
end
