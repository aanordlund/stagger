;
;   Program to plot exact differences between two tables.
;
;   Update history:
;   ---------------
;   19.04.2005 Coded (from tmpdiff)/RT
;   19.04.2005 Changed the surface of the zero-level to a contour/RT
;   08.11.2007 Soft-coded dimensions of arrays, and size of characters/RT
;
;   Known bugs: None
;
pro tabdiff, tab2file, iv=iv, mz=mz, tab1file=tab1file, $
										tvcolortable=tvcolortable, STOP=STOP
	common cdat,x,y,z,nx,ny,nz,nw,ntmax,date0,time0
	common ctable,ttmean,rhm,drhm,eemin,eemax,dee,mtab,itab,tab

	default, iv, 1
	default, tab1file, stamnavn()+'.tab'
	if (strpos(tab1file, '.tab') LT 0) then tab1file=tab1file+'.tab'
	default, tab2file, stamnavn()+'.tab.bak'
	if (strpos(tab2file, '.tab') LT 0) then tab2file=tab2file+'.tab'
	default, mz, nz
	default, tvcolortable, 23L
    rt_graphics

	ivp = long(iv*3.+.5)
	stra = ''
	iimax = 101 & rf = 4 & x0 = 140 & y0 = 50
	table, tab2file, mz=mz
	nvar = (itab(1)-itab(0))/mtab(0)/3L
; Make sure we are adressing a sensible variable number
	if (ivp LT 0)      then print, ivp, form=$
					'("WARNING: iv=",I0," < 0. Resetting to",I0,$)'
	if (ivp GE nvar*3) then print, ivp, nvar*3, form=$
					'("WARNING: iv=",I0," > nvar*3=",I0,". Resetting to",I0,$)'
	ivp = (ivp>0L)<(nvar*3L-1L)
	print, ivp, form='(" iv=",I0)'
	if (nvar EQ 5L) then begin
		vartit = ['ln(P!ig!n/!3[!xdyn cm!e-2!n!3]!x)', $
				  'ln(!8T!x!n/!3[!xK!3]!x)', $
				  'ln(!7j!x!iRoss!n/!3[!xcm g!e-1!n!3]!x)', $
				  'ln(!7j!x!iPlck!n/!3[!xcm g!e-1!n!3]!x)', $
				  'ln(!7j!x!i5000!n/!3[!xcm g!e-1!n!3]!x)']
	endif else begin
		vartit = ['Log(P!ig!n/!3[!xdyn cm!e-2!n!3]!x)', $
				  'Log(!7j!x/!3[!xcm g!e-1!n!3]!x)', $
				  '!7e!x!n/!3[!x10!e12!nerg g!e-1!n!3]!x', $
				  'S!i0!n','S!i1!n','S!i2!n','S!i3!n']
	endelse
	dtit = ['', '!s!a !9D!x!r!s!3!A!i_____!x!r!b!9D!7H!x!n', $
			'!s!a !i !a!9D!x!r!s!3!A!i_________!x!r!b!9D!xln!7q!x!n']
	rhm1 = rhm
	eemin1 = eemin
	eemax1 = eemax
	tab1 = tab
	table, tab1file, mz=mz
	rhm0 = rhm
	eemin0 = eemin
	eemax0 = eemax
	dee0 = dee
	tab0 = tab
	itab0 = itab
	mtab0 = mtab
	error = 0
	eps = 1.e-4
	if (total((rhm0-rhm1)^2/mz) GT eps) then error=1
	if (total((eemin0-eemin1)^2/mz) GT eps) then error=1
	if (total((eemax0-eemax1)^2/mz) GT eps) then error=1
;	plot,alog10(rhm0)-7., rhm0-rhm1, xtit='Log(!7q!x)', ytit='!7Dq!x'
;	read,'Good enough? ',stra & if (stra EQ 'n') then error=1; return
;	plot,alog10(rhm0)-7., eemin0-eemin1, xtit='Log(!7q!x)', ytit='!7DH!x!imin!n'
;	read,'Good enough? ',stra & if (stra EQ 'n') then error=1; return
;	plot,alog10(rhm0)-7., eemax0-eemax1, xtit='Log(!7q!x)', ytit='!7DH!x!imax!n'
;	read,'Good enough? ',stra & if (stra EQ 'n') then error=1; return
	if (error) then begin
		table, tab2file, mz=mz
		print, 'Using lookup.'
	endif

	ntot = total(mtab0)
	dtab = fltarr(ntot)
	Lee = fltarr(ntot)
	Lrhot = fltarr(ntot)
	i0 = 0L
	for i=0,mz-1 do begin
		ee = eemin0(i)+indgen(mtab0(i))*dee0(i)
;		print, i, i0, i0+mtab(i)-1, N_ELEMENTS(ee)
		if (eemin0(0) GT 0) then  Lee(i0:i0+mtab0(i)-1) = alog10(ee) $
							else  Lee(i0:i0+mtab0(i)-1) =        ee
		Lrhot(i0:i0+mtab0(i)-1) = alog10(rhm0(i))-7.0
		k0 = mtab0(i)*ivp-1;  itab is in Fortran style indexes
		if (error) then begin
			tabval1 = lookup(replicate(alog(rhm0(i)),mtab0(i)), ee, $
													dvdtcr,dvdrct, iv=ivp/3)
			if ((ivp mod 3) EQ 1) then tabval1=dvdtcr
			if ((ivp mod 3) EQ 2) then tabval1=dvdrct
		endif else begin
			tabval1 = tab1(k0+itab0(i):k0+itab0(i)+mtab0(i)-1)
		endelse
		dtab(i0:i0+mtab0(i)-1) =tabval1-tab0(k0+itab0(i):k0+itab0(i)+mtab0(i)-1)
		i0 = i0 + mtab0(i)
	endfor
	stat, dtab
; Find the current color-table index and load the TVCOLORTABLE 
	icurr = currentct(/QUIET, ntables=ntables)
	tvcolortable = (tvcolortable>0)<(ntables-1L)
	erase
	loadct, tvcolortable, /SILENT
; Set the plot area
	plot, [0,1], [0,1], col=!p.background
	axis, /zaxis, zsty=6, zr=[min(dtab), max(dtab)], /save, col=!p.background
	zrr = !z.crange
	triangulate, Lee, Lrhot, TR
	Ni  = 100L
	dLe = (max(Lee) - min(Lee))/(Ni-1.0)
	LogT = min(Lee) + findgen(Ni)*dLe
	dLr = (max(Lrhot) - min(Lrhot))/(Ni-1.0)
	Logrho = min(Lrhot) + findgen(Ni)*dLr
	minee = zrr(0); min(dtab); long(min(dtab)-.99999)
	eefin = trigrid(Lee, Lrhot, dtab, TR, [dLe,dLr]);, missing=minee)
	see = size(eefin) & ex = see(1) & ey = see(2)
	erase
	print, ex, ey
	cs = 0.75
;The overall charactersize in units of the default.
	!P.charsize=cs*!P.charsize
;The xyouts charactersize in units of the overall size.
	cs = 1.0*!P.charsize

	vartitle = '!7D!x'+dtit(ivp mod 3)+vartit(ivp/3)
	cx0 = (x0/2.-20.)/659. & cy0 = (y0+rf*iimax/2.-50.)/656.
	color_scale, min(eefin), max(eefin), x0=cx0, y0=cy0, tit=vartitle
	tvscl, rebin(eefin, rf*ex, rf*ey), x0, y0
	!P.region=[x0, y0, x0+rf*ex, y0+rf*ey]
	plot, Lee, Lrhot, psym=3, /DEVICE, /NOERASE, tit=vartitle+'!c ', $
			xtit='Log(!8T!x)', ytit='Log(!7q!x)', ysty=1
	navn = ''
	stra = prompt(" Continue [Yes/Stop/Quit]? ")
	if (stra EQ 's') then begin
		loadct, icurr, /SILENT
		stop
	endif
	if (stra EQ 'q' OR stra EQ 'n' OR stra EQ 'r') then begin
		reset_graphics
		loadct, icurr, /SILENT
		return
	endif
	loadct, icurr, /SILENT
	!P.region = 0
;	!P.charsize = 1.8*cs
	shade_surf, eefin, LogT, Logrho, az=40, ztit=vartitle, $
		xtit='Log(!8T!x)', ytit='Log(!7q!x)', ysty=1, /SAVE, zr=zrr, zsty=1
	cols = (!D.TABLE_SIZE-1)*(.2+.8*(eefin LT 0.0))
	contour, eefin, LogT, Logrho, levels=0.0, /T3D, /NOERASE, $
		xsty=1,xr=!x.crange,ysty=1,yr=!y.crange,zr=!z.crange,zsty=1
;	surface, replicate(0.0, 101, 101), LogT, Logrho, $
;				/SAVE, ysty=1, zr=zrr, zsty=1, /NOERASE, az=40, shade=cols
	plots, Lee, Lrhot, dtab, /T3D, psym=3, color=100
	if KEYWORD_SET(stop) then stop
	reset_graphics
end
