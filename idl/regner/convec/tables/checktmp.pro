;
;   Program to run through a small set of checks on tmp tables.
;
;   Update history:
;   ---------------
;   20.06.2007 Coded/RT
;
;   Known bugs: None
;
pro checktmp, tmpfile, STOP=STOP
	common ctabl1,rhm,drhm,thmin,thmax,dth,mtab,itab,tab

	close, 2
;
; Check of the borders of the new table
	tmpshot, tmpfile=tmpfile
;
; Look for the old table
	spawn, 'ls -1t scr1/*.tmp', result
	if (N_ELEMENTS(result) GT 2) then begin
		tmpshot, tmp=result(2), /OPLOT
		close, 2
	endif
;
; Visual inspection of the first three entries in the new table.
	for iiv=0,2 do begin
		s = prompt(" plottabtmp of entry " + string(iiv,form='(I0)'))
		plottabtmp, iv=iiv
	endfor
	
	if KEYWORD_SET(STOP) then stop
end
