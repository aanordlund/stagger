pro tabtmp,file,mz=mz,DEBUG=DEBUG,STOP=STOP
;
;  Load Table
;
    common ctabl1,rhm,drhm,thmin,thmax,dth,mtab,itab,tab
	common cdat,x,y,z,nx,ny,nz,nw,ntmax,date0,time0
    common currenttable, EOSrhoeLoaded, kaprhoTLoaded

	default, mz, nz
    close,2
	if (file_test(file)) then begin
        if (strpos(file, 'kaprhoT') LT 0L) then begin
            openr,2,file,/SWAP_IF_LITTLE_ENDIAN
            kaprhoTLoaded = 0b
        endif else begin
            radtab, file
        endelse
	endif else begin
		if (file_test('EOS.tab') AND file_test('kaprhoT.tab')) then begin
			if KEYWORD_SET(DEBUG) then $
				print, "Using the EOS.tab and kaprhoT.tab tables."
            radtab
            return
		endif else begin
			print, "ERROR: Didn't find any of the files: ",file, $
							", EOS.tab"," or kaprhoT.tab."
			print, "       Bailing out!"
			if KEYWORD_SET(STOP) then stop
			return
		endelse
	endelse
    print,'common ctabl1,rhm,drhm,thmin,thmax,dth,mtab,itab,tab'

    i=assoc(2,lonarr(10))
    inext=i(0,0)
    lcfile=i(1,0)
    j=assoc(2,lonarr(10),4L*(1+lcfile+1000))
    lrec=j(0,0)
    irecp=assoc(2,lonarr(inext),4L*(lcfile+1))
    irec=4L*lrec*irecp(0)

	tabn = assoc(2,lonarr(3),irec(0))
	nvar = tabn(1,0)
	if KEYWORD_SET(DEBUG) then begin
		tabf = assoc(2,fltarr(9),irec(0)+3L*4L)
		print, tabn(0), tabf(0)
	endif
;                                   offset from the 12 scalars (above)
    tabm=assoc(2,fltarr(mz),irec(0)+12*4L)
    rhm=tabm(2)
    rhm1=alog(rhm(0))
    rhm2=alog(rhm(mz-1))
    drhm=(rhm2-rhm1)/(mz-1)
    thmin=tabm(4)
    thmax=tabm(5)
    dth=tabm(6)
    itabm=assoc(2,lonarr(mz),irec(0)+12*4L)
    itab=itabm(10)
    mtab=itabm(11)
    ; mtable=2*63*63*63L
    ; mtaba=mtable-12*mz-12

    mtaba=itab(mz-1)+nvar*3*mtab(mz-1)
    print,'reading',mtaba,' words into table'
    a=assoc(2,fltarr(mtaba),irec(0)+12*4L+12*mz*4L)
    tab=a(0)
	if KEYWORD_SET(STOP) then stop
end
