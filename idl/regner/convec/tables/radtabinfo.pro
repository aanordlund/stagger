;
;   Emulation of the F77 routine convec/OStabcode/radtabinfo.f, to translate and
;   print the information stored in the flags, Nxopt, and the revision numbers,
;   Nxrev, of a kaprhoT.F77.tab table.
;
;   Arguments (defaults in [...]):
;    tabfile: Full name of the table file ['kaprhoT.tab'].
;    STOP:    Set this keyword to stop before returning, to make all
;             variables available to the user (for debugging).
;
;   Update history:
;   ---------------
;   15.10.2008 Coded/RT
;   30.08.2013 No longer relies on already loaded tables./RT
;   27.01.2016 Included Nxopt(6) to contain flags about alterations of table./RT
;
;   Known bugs: None
;
pro radtabinfo, tabfile, info=info, STOP=STOP
;
; A reasonable guess for the name of the table
	default, tabfile, 'kaprhoT.tab'
;
; Open the table file
    openr, lun, tabfile, /GET_LUN, /SWAP_IF_LITTLE_ENDIAN, /F77
;
; Declare all (long) integers before reading
    NTtab=0L& Nrtab=0L& Ma=0L& Na=0L& Nlam=0L& Nelem=0L
    Nxopt=lonarr(20L)& Nxrev=Nxopt
;
; Read the dimensions of the arrays to follow.
    readu, lun, NTtab,Nrtab,Ma,Na,Nlam,Nelem,Nxopt,Nxrev
    date  = "        " & time=date
    readu, lun, date,time
    free_lun, lun
;
; Description of various versions of the table content - padded at the
; beginning with "" to allow for Fortran style indexing beginning at 1.
; Emulation of cradtype.inc.
	sopctype = ["", "Opacity binning, scaled",      $
					"Opacity binning","","","",     $
					"Opacity sampling","","","",""]
	ssrctype = ["", "Pure Planck function", "Scattering included","",""]
	stautype = ["", "monochromatic opacity", "Rosseland optical depth","",""]
	sxtrtype = ["", "xcorr extrapolated", "Jlambda extrapolated", $
					"T-scaled Jlambda extrapolated",""]
	jtype    = ["", replicate("bins", 5), replicate("wavelengths",5)]

	info = " Loaded "+tabfile+" computed on "+date+" at "+time+"."
	info = [info, " This is an "+sopctype(Nxopt(0))+" table with "+$
			ssrctype(Nxopt(1))+" for "+string(Nlam,form='(I0)')+" "+$
			jtype(Nxopt(0))+"."]
	if (Nxopt(1) GT 1) then begin
		if (Nxopt(2) EQ 0) then begin
			info=[info," Van Regemorter-style line-scattering NOT included."]
		endif else begin
			info=[info,string(Nxopt(2)*0.01,form='(" ",f0.2)') + $
                "* van Regemorter-style line-scattering included."]
		endelse
	endif
	if (Nxopt(0) LT 6) then begin
		info=[info," Bin membership according to "+stautype(Nxopt(3))+$
						" where tau_lambda=1."]
		info=[info," "+sxtrtype(Nxopt(4))+" from the 1D rad. transf. solution."]
		info=[info," Photospheric transition performed with exp(" $
				+ string(-Nxopt(5)*.01,form='(f0.2)')+"*tau)."]
	endif
	if (Nxrev(3) LT 70) then linedat='odf.dat' $
						else linedat=' OS.dat'
	info=[info," This table was computed with radtab.x v" + $
                                                string(Nxrev(1), form='(I0)')]
	info=[info," Versions of the atomic physics tables:"]
	info=[info,string(" EOS.tab v",Nxrev(2),linedat,Nxrev(3),", subs.dat v",$
            Nxrev(4), form='(3(A0,I0))')]
    if ((Nxopt(6L) AND 1b)  EQ 1) then $
        info=[info," Abarrent bin-wise opacities have been repaired with " $
            + "radtab_repair.pro."]

    print, info, form='(A0)'
	if KEYWORD_SET(STOP) then stop
end
