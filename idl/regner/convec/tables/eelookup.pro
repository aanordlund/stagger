;
;   Function to lookup internal energy as function of ln(rho/[sim])
;   and temperature/[K], directly in the MHD EOS table.
;   The interpolation circumvents the complication of the non-rectangular
;   MHD EOS table, by padding it with constant extrapolations in density,
;   to generate a rectangular eetab (and it's derivatives). The LogT-grid
;   of the table is not regular, but the Lrho-grid is.
;
;   Arguments:
;    lnr:       ln(density) in simulation units.
;    tt:        T/[K].
;    table:     name of the MHD EOS table-file ['EOS.tab'].
;    ur:        Unit conversion constant for density [1e-7].
;    ul:        Unit conversion constant for length [1e+8].
;    ut:        Unit conversion constant for time [1e+2].
;    LINEAR:    Set this keyword to perform linear interpolation (for testing).
;    STOP:      Set this keyword to stop before returning, to make all
;               variables available to the user (for debugging).
;
;   Update history:
;   ---------------
;   14.05.2008 Coded/RT
;
;   Known bugs: None
;
function eelookup, lnr, tt, table=table, ur=ur, ul=ul, ut=ut, LINEAR=LINEAR, $
																	STOP=STOP
@imhd
	default, table, 'EOS.tab'
	default, ur, 1e-7
	default, ul, 1e+8
	default, ut, 1e+2
;
; Convert density to cgs-units
	Lrho = lnr/alog(1e1) + alog10(ur)
	LogT = alog10(tt)
	ulE  = 2d0*alog10(ul/ut)
;
; Read the MHD EOS table
	mhd_read, table, EOS=EOS, tl=ttab, dir='./'
;
; Dimensions of the table
	Nt    = N_ELEMENTS(ttab) &  Nr = N_ELEMENTS(EOS(0,*,0))
;
; log(rho)-resolution of the table
	dLrho = EOS(0,1,iLogrho)-EOS(0,0,iLogrho)
;
; rho-dimension of a regular grid covering the densities of the table
	Nrtab = long((EOS(Nt-1L, Nr-1L,iLogrho)-EOS(0,0,iLogrho))/dLrho + 1.1)
;
; ...and the corresponding log(rho)-array
	rtab  = EOS(0,0,iLogrho) + dindgen(Nrtab)*dLrho
;
; Density index of the regular grid, corresponding to the original table.
	ir    = long((EOS(*,*,iLogrho)-EOS(0,0,iLogrho))/dLrho+.001)
;
; Temperature index
	it    = lindgen(Nt)#replicate(1, Nr)
	eetab = dblarr(Nt, Nrtab) & dedt=eetab & dedr=eetab
	eetab(it,ir) = EOS(*,*,iLoge)
;
; Constant extrapolation (in rho) of Logee outside the original MHD table
	k     = Nr-1L
	for i=0L, Nt-1L do begin
		if (ir(i,0) GT 0L) 		 then eetab(i,0:ir(i,0))=eetab(i,ir(i,0))
		if (ir(i,k) LT Nrtab-1L) then eetab(i,ir(i,k):*)=eetab(i,ir(i,k))
	endfor
;
; \Delta LogT of the LogT-grid of the MHD table
;	dT           = deriv(dindgen(Nt), ttab)
	dT           = [ttab(1:*)-ttab, ttab(Nt-1L)-ttab(Nt-2L)]
;
; The derivatives are zero outside the original MHD table
	dedt(it,ir)  = EOS(*,*,idlnedlnT)*(dT#replicate(1, Nr))
	dedr(it,ir)  = EOS(*,*,idlnedlnr)*dLrho
;
; Bicubic interpolation, using analytical derivatives, as supplied by
; the MHD EOS.
	ee = 1d1^(bicubic(LogT, Lrho, eetab, ttab, rtab, dedt, dedr, LINEAR=LINEAR)$
																 - Lrho - ulE)

	if KEYWORD_SET(STOP) then stop

	return, float(ee)
end
