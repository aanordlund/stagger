;
;   Program to open the appropriate EOS and opacity table for a simulation.
;
;   Update history:
;   ---------------
;   ??.??.19?? Coded/RT
;   13.02.2007 Automatically load fixed-format 'EOSrhoe.tab' if present/RT
;   11.07.2007 Fixed bug in calculation of mtaba/RT
;   02.04.2012 Automatically load the new 'EOSrhoe.tab'-tables using eostab/RT
;
;   Known bugs: None
;
pro chtab,nz1
    common cdat,x,y,z,nx,ny,nz,mp,ntmax,date0,time0
    common ctable,ttmean,rhm,drhm,eemin,eemax,dee,mtab,itab,tab

    nz=nz1
    rhm=rhm(0:n)
    eemin=eemin(0:n)
    eemax=eemax(0:n)
    dee=dee(0:n)
    mtab=mtab(0:n)
    itab=itab(0:n)
end

PRO table,file,mz=mz,EOSrhoe=EOSrhoe,iS=iS,SSTAB=SSTAB,QUIET=QUIET,STOP=STOP
;
;  Load Table
;
    common cdat,x,y,z,nx,ny,nz,mp,ntmax,date0,time0
    common ctable,ttmean,rhm,drhm,eemin,eemax,dee,mtab,itab,tab
	common mycom,cray,one,two
	common cSOS,Nlm,xlm,wlm
    common currenttable, EOSrhoeLoaded, kaprhoTLoaded

;    if n_params() eq 0 then begin
;		print,'table,file [,mz=mz]'
;		return
;    endif
	default, EOSrhoe, 'EOSrhoe.tab'
    LOADEOSrhoe = 0b
;
; Check to see if the requested table file is a 'EOSrhoe.tab'-file
	if (strpos(file, 'EOSrhoe') GE 0L  AND  file_test(file)) then LOADEOSrhoe=1b
;
; If the requested 'file' does not exists, but EOSrhoe does, load that instead
	if ((NOT file_test(file))  AND  file_test(EOSrhoe)) then begin
        LOADEOSrhoe=1b
        file = EOSrhoe
    endif
    if (LOADEOSrhoe) then begin
        eostab, EOSrhoe, SSTAB=SSTAB, iS=iS
;       if KEYWORD_SET(SSTAB) then eosstab, iS
        return
    endif
;
; ...otherwise, load the old format tables
    EOSrhoeLoaded = 0b

	default, kaprhoT, 'kaprhoT.tab'
	default, mz, nz
	if (N_ELEMENTS(cray) LE 0) then begin & cray=0 & one=0 & two=1 & endif

    close,2

	openr,2,file,/SWAP_IF_LITTLE_ENDIAN

    i=assoc(2,lonarr(10*two))
    inext=i(0*two+one,0)
    lcfile=i(1*two+one,0)
    j=assoc(2,lonarr(10*two),two*4L*(1+lcfile+1000))
    lrec=j(0*two+one,0)
    irecp=assoc(2,lonarr(1000L*two),two*4L*(lcfile+1))
	if (NOT KEYWORD_SET(QUIET)) then begin
		print, "Versions of the code and atomic physics tables:"
		print, irecp(996:*,0), form='(" tab-code v",I0,", EOS.tab v",I0,' $
		     				       + '", odf.dat v",I0,", subs.dat v",I0)'
	endif
	irecp=irecp(0:inext*two-1L,0)
    irec=two*4L*lrec*irecp(indgen(inext)*two+one,0)
	dm = 0

	if (cray) then begin
		tabm=assoc(2,bytarr(8L*mz),irec(0)+12*4L*two)
		ttmean=crayflt(tabm(0))
		rhm=crayflt(tabm(2))
		eemin=crayflt(tabm(7))
		eemax=crayflt(tabm(8))
		dee=crayflt(tabm(9))
		if (file EQ kaprhoT) then begin
			xlm = crayflt(tabm(0))
			wlm = crayflt(tabm(1))
		endif
	endif else begin
		iscr=assoc(2,lonarr(12),irec(0))
		nvar=iscr(1,0)
		tabm=assoc(2,fltarr(mz+dm),irec(0)+12*4L)
		ttmean=tabm(0)
		rhm=tabm(2)
		eemin=tabm(7)
		eemax=tabm(8)
		dee=tabm(9)
		if (file EQ kaprhoT) then begin
			Nlm = nvar
			xlm = tabm(0:Nlm-1L, 0)
			wlm = tabm(0:Nlm-1L, 1)
		endif
	endelse
    rhm1=alog(rhm(0))
    rhm2=alog(rhm(mz-1))
    drhm=(rhm2-rhm1)/(mz-1)
    itabm=assoc(2,lonarr((mz+dm)*two),irec(0)+12*4L*two)
    itab=itabm(indgen(mz)*two+one,10)
    mtab=itabm(indgen(mz)*two+one,11)
	Nder = (itab(1)-itab(0))/(mtab(0)*nvar)
    ; mtable=2*63*63*63L
    ; mtaba=mtable-12*mz-12

;	mtaba=itab(mz-1)+nvar*Nder*mtab(mz-1)
;	mtaba=max(itab)+nvar*Nder*mtab(mz-1)
	imax = max(where(itab GT 0L))
;	mtaba=itab(imax)+nvar*Nder*mtab(imax)
	mtaba=itab(imax)+(itab(imax)-itab(imax-1))/mtab(imax-1)*mtab(imax)
	if (NOT KEYWORD_SET(QUIET)) then $
    	print,format='(a,i7,a)','reading ',mtaba,' words into table'
	if (cray) then begin
    	a=assoc(2,bytarr(8L*(mtaba-itab(0))),irec(0)+(12*4L+12*mz*4L)*two)
		tab = crayflt(a(0))
	endif else begin
    	a=assoc(2,fltarr(mtaba-itab(0)),irec(0)+12*4L+12*mz*4L)
    	tab=a(0)
	endelse
	if (NOT KEYWORD_SET(QUIET)) then $
    	print,'used file size =',irec(0)+(12*4L+(12*mz+mtaba)*4L)*two

    if KEYWORD_SET(SSTAB) then sstab, iS

	if KEYWORD_SET(STOP) then stop
end
