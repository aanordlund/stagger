;   Update history:
;   ---------------
;   13.01.2002 Coded/RT
;   31.01.2005 Added the stellar evolution code dirs: evldir and evlscr/RT
;
;   Known bugs: None.
;
pro hosts_dir, envdir=envdir, envscr=envscr, evldir=evldir, evlscr=evlscr, $
			   mhddir=mhddir, mhdscr=mhdscr, livdir=livdir, livscr=livscr, $
			   cnvdir=cnvdir, cnvscr=cnvscr, prgdir=prgdir, $
			   host=host, USER=USER
	default, USER, getenv("USER")
	default, host, getenv("HOST")
	default, envdir, ''
	default, envscr, ''
	default, evldir, ''
	default, evlscr, ''
	default, mhddir, ''
	default, mhdscr, ''
	default, livdir, ''
	default, livscr, ''
	default, cnvdir, ''
	default, cnvscr, ''
	default, prgdir, ''

;	spawn, "finger trampedach|grep Login", result
;	Nart = N_ELEMENTS(result)
;	author = strarr(Nart)
;	for i=0, Nart-1 do begin
;		tmp=str_sep(result(i)," ")& author(i)=tmp(2)
;	endfor
;	if ((where(author EQ USER))(0) LT 0) then begin
;		cnvdir='' & envdir='' & mhddir='' & livdir='' & stldir=''
;		cnvscr='' & envscr='' & mhdscr='' & livscr='' & prgdir=''
;		return
;	endif
	is = (strmid([envdir,envscr,evldir,evlscr,mhddir,mhdscr,livdir, $
				  livscr,cnvdir,cnvscr,prgdir],0,1) NE "/")
	s  = ['', '/']
	if (strmid(envdir, 0, 1) NE '~') then envdir = getenv("ENVDIR")+s(is(0))+envdir
	if (strmid(envscr, 0, 1) NE '~') then envscr = getenv("ENVSCR")+s(is(1))+envscr
	if (strmid(evldir, 0, 1) NE '~') then evldir = getenv("EVLDIR")+s(is(0))+evldir
	if (strmid(evlscr, 0, 1) NE '~') then evlscr = getenv("EVLSCR")+s(is(1))+evlscr
	if (strmid(mhddir, 0, 1) NE '~') then mhddir = getenv("MHDDIR")+s(is(2))+mhddir
	if (strmid(mhdscr, 0, 1) NE '~') then mhdscr = getenv("MHDSCR")+s(is(3))+mhdscr
	if (strmid(livdir, 0, 1) NE '~') then livdir = getenv("LIVDIR")+s(is(4))+livdir
	if (strmid(livscr, 0, 1) NE '~') then livscr = getenv("LIVSCR")+s(is(5))+livscr
	if (strmid(cnvdir, 0, 1) NE '~') then cnvdir = getenv("CNVDIR")+s(is(6))+cnvdir
	if (strmid(cnvscr, 0, 1) NE '~') then cnvscr = getenv("CNVSCR")+s(is(7))+cnvscr
	if (strmid(prgdir, 0, 1) NE '~') then prgdir = getenv("PRGDIR")+s(is(9))+prgdir
end
