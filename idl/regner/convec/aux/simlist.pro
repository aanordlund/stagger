;+
;   Function to generate a list of simulation files, based on individual
;   indices, ranges of indices or wildcards.
;
;   Arguments (defaults in [...]):
;    device:  The plotting device to use ['x'].
;    STOP:    Set this keyword to stop before returning, to make all
;             variables available to the user (for debugging).
;
;   Update history:
;   ---------------
;   15.05.2015 Coded/RT
;   06.08.2019 Added handling of variable width of indices (fixed for a
;              particular call to 'simlist'), returned as 'width' by calls
;              to the 'str2index'-function./RT
;
;   Known bugs: None
;-
function simlist, files, ext=ext, sim=sim, NOSCRATCH=NOSCRATCH, STOP=STOP
	default, ext, '.sav'
	default, sim, stamnavn()
	if (files EQ 'scr') then begin
		f = file_search(sim+'.scr')
		if (f(0) EQ '') then begin
			print, "Couldn't find "+sim+".scr. Bailing out!"
			stop
		endif
	endif else begin
		indexes = str2index(files, width)
		if (indexes(0) EQ -1) then begin
			f1 = file_search(sim+files+ext)
		endif else begin
			f1 = sim+indexes+ext
		endelse
		f = f1
		nf = n_elements(f)
		if (strpos(files, '?') GE 0  OR  strpos(files, '*') GE 0) then begin
			dot = rstrpos(f(0), ext)
			sim = strmid(f(0), 0, dot-width)
			files =  strmid(f(0),    dot-width, width) + $
                 "-"+strmid(f(nf-1), dot-width, width)
		endif
		if (NOT KEYWORD_SET(NOSCRATCH)  AND  ext EQ '.sav') then begin
			f2 = file_search(sim+'.scr')
			if (f2(0) NE '') then f = [f1, f2]
			if (f1(0) EQ '') then f = f2
		endif
	endelse
	if (f(0) EQ '') then begin
		print, "Couldn't find any "+sim+"-files. Bailing out!"
		stop
	endif

	if KEYWORD_SET(STOP) then stop
	return, f
end
