;
;  Routine to find the root-name [danish: stamnavn] of a the
;  simulation in the current working directory.
;
;  Update history
;  ---------------
;  14.06.1995 Coded/RT
;  09.02.2004 Changed spawn to getenv => speed-up/RT
;  25.10.2006 Changed from obsolete findfile call to file_search/RT
;
function stamnavn
	fscr = file_search('*.scr')
	Nscr = N_ELEMENTS(fscr)*(fscr(0) NE "")
	if (Nscr LE 0) then begin
; Name after the directory
		cd, current=pwd
		strfscr=str_sep(pwd, '/')
		dir = strfscr(N_ELEMENTS(strfscr)-1)
; If directory is "run", take next one up.
		n0 = (strlowcase(strmid(dir, 0, 3)) EQ 'run')
		return, strfscr(N_ELEMENTS(strfscr)-n0-1)
	endif else begin
		if (Nscr EQ 1) then begin
			base = str_sep(fscr(0), "\.")
			return, base(0)
		endif else begin
			ftab = file_search('*.tab')
			Ntab = N_ELEMENTS(ftab)*(ftab(0) NE "")
			ftmp = file_search('*.tmp')
			Ntmp = N_ELEMENTS(ftmp)*(ftmp(0) NE "")
			hits = intarr(Nscr)
			bases = strarr(Nscr)
			for i=0, Nscr-1 do begin
				base = str_sep(fscr(i), "\.")
				bases(i) = base(0)
				hits(i) = total(strpos(ftab,bases(i)) GE 0) + $
						  total(strpos(ftmp,bases(i)) GE 0)
			endfor
			dum = max(hits, ihit)
			return, bases(ihit)
		endelse
;		return, strmid(fscr(0), 0, strlen(fscr(0))-4)
	endelse
end
