;
;   Function to return the date and time entry for an entry to the NOTES file.
;
;   Update history:
;   ---------------
;   06.04.2004 Extracted from add_to_log.pro/RT
;   06.04.2004 Changed from function to s/r to write time-string to log-file/RT
;   09.03.2007 Added the possibility of a log-message/RT
;   07.12.2015 Now prints to stdout (lun=-1) if logfile=''. Also use new
;              'today' function/RT
;
;   Known bugs: None
;
pro logtime, text, logfile=logfile
	common coff,mpar,mav,ipack,incr,ioff,time,voff,xfile,irec
	common cdat,x,y,z,nx,ny,nz,nw,ntmax,date0,time0

	default, text, ''
	default, logfile, 'NOTES'

;   caldat, systime(/julian), month, day, year
;   if (day   LT 10) then sdate=      string(day,  form='("0",I1,".")') $
;   				 else sdate=      string(day,  form=    '(I2,".")')
;   if (month LT 10) then sdate=sdate+string(month,form='("0",I1,".")') $
;   				 else sdate=sdate+string(month,form=    '(I2,".")')
;   sdate = sdate + string(year, time(ntmax-1L), form='(I4,": at t=",f12.5)')
    sdate = today() + string(time(ntmax-1L), form='(": at t=",f12.5)')
;
; Open the log-file and append the time-string
	if (logfile EQ '') then lun=-1  $
                       else openw, lun, logfile, /APPEND, /GET_LUN
	printf, lun, sdate
	if (text NE '') then printf, lun, "    "+text
	if (logfile NE '') then free_lun, lun
end
