;************************************************************************
;  Extract variables of cdata
;
;************************************************************************
function cindex, iv, dir=dir, RELOAD=RELOAD
;
	common cdata_names, array

	s=size(iv)
	if s(0) eq 0 and s(1) ne 7 then return,iv

	if (N_ELEMENTS(array) LE 1  OR  KEYWORD_SET(RELOAD)) then begin
        cdata_read, dir=dir
;       spawn,'cdata.help -1',array
    endif

	index=where(array EQ iv, n)
	if (n LE 0L  OR  index(0) LT 0L) then begin
		print,iv+' not in cdata:'
		print,array
		return,-1
	end
	return,index(0)
end
;
pro wdata,f,it,iv, RELOAD=RELOAD
;
	common coff,mpar,mav,ipack,incr,ioff,time,voff,xfile,irec
	common cdat,x,y,z,nx,ny,nz,nw,ntmax,date0,time0 
	common mycom,cray,one,two

	if (N_PARAMS() EQ 0L) then begin
		print,'wdata,f,isnap,iarray'
		return
	end
;
	iv=cindex(iv, RELOAD=RELOAD)
	a=assoc(1,fltarr(nz),irec(it*incr+incr-1)+two*4L*(mpar+iv*nz))
;	a=assoc(1,fltarr(nz),ioff(it)+(incr-1)*voff+4L*(mpar+iv*nz))
	a(0)=f
	return
end

function cdatat,iv, RELOAD=RELOAD
;
	common coff,mpar,mav,ipack,incr,ioff,time,voff,xfile,irec
	common cdat,x,y,z,nx,ny,nz,nw,ntmax,date0,time0 

	if (N_PARAMS() EQ 0L) then begin
		print,'tmp=cdatat(iarray)'
		return, 0
	end
;
	a=fltarr(nz,ntmax,/nozero)
	iv=cindex(iv, RELOAD=RELOAD)
	for t=0L,ntmax-1L do a(*,t)=cdata(t,iv)
	return,a
end

function cdata, it, iv, HELP=HELP, RELOAD=RELOAD
;
	common coff,mpar,mav,ipack,incr,ioff,time,voff,xfile,irec
	common cdat,x,y,z,nx,ny,nz,nw,ntmax,date0,time0 
	common cdata_names, array
	common mycom,cray,one,two

	if (N_PARAMS() EQ 0L) then print,'cdata(isnap,iarray)'
    if (N_ELEMENTS(array) LE 1L OR KEYWORD_SET(RELOAD)) then cdata_read, dir=dir
	if KEYWORD_SET(HELP) then begin
		for i=0L,N_ELEMENTS(array)-1L do print,i,array(i),format='(i3,2x,a)'
		if (N_PARAMS() LT 2L) then return, array
	end
	if (N_PARAMS() EQ 0L) then return,0L

	iv=cindex(iv, RELOAD=RELOAD)
	if (cray) then begin
		a=assoc(1,bytarr(8*nz),irec(it*incr+incr-1)+two*4L*(mpar+iv*nz))
		return, crayflt(a(0))
	endif else begin
;		a=assoc(1,fltarr(nz),ioff(it)+(incr-1)*voff+4L*(mpar+iv*nz))
		a=assoc(1,fltarr(nz),irec(it*incr+incr-1)+two*4L*(mpar+iv*nz))
		return,a(0)
	endelse
end
