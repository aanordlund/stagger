pro printbot, it, STOP=STOP
	common coff,mpar,mav,ipack,incr,ioff,time,voff,cfile,irec
	common cdat,x,y,z,nx,ny,nz,nw,ntmax,date0,time0

	parnames=['t','cnu1','cnu2','cnu3','cnue','cnur','eebot','rhobot', $
			  'ppbot','period']
	if (N_ELEMENTS(it) EQ 0) then it = ntmax-1
	if (it LT 0) then it=0
	if (it GE ntmax) then it=ntmax-1
	for i=0,N_ELEMENTS(parnames)-1 do begin
		print, parnames(i),cpar(it,parnames(i)), cparindex(parnames(i)), $
													format='(A7," =",F12.4,I5)'
	endfor
	if KEYWORD_SET(STOP) then stop
end
