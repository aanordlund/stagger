;
;   Routine to cleverly guess the name of the desired scratch-file
;   and load it, if not already loaded.
;
;   Arguments (defaults in [...]):
;    scrfile: Optional input, and outputs name of loaded file [sim+'.scr'].
;    sim:     Returns the root name of the simulation.
;    WRITEACCESS: Set this keyword to open the scratch-file with write-access.
;    DEBUG:   Set this keyword for more diagnostic output.
;    STOP:    Set this keyword to stop before returning, to make all
;             variables available to the user (for debugging).
;
;   Update history
;   ---------------
;   09.02.2004 Coded/RT
;   19.11.2004 Included option WRITEACCESS to ensure the file is opened to write
;   08.09.2015 Added output-var. sim to return root-name of simulation.
;              scrfile now always returns full name of the scratch-file and
;              all my programs have been updated accordingly. Now prompts
;              user if currently loaded is same sim, but != defscr./RT.
;
;   Known bugs: None
;
pro defaultscr, scrfile=scrfile, sim=sim, WRITEACCESS=WRITEACCESS, $
                                                        DEBUG=DEBUG, STOP=STOP

	current = (fstat(1)).name
	sim     = stamnavn()
    defscr  = sim + '.scr
	LOADIT  = 0
;
;  If a filename has not been given, infer one from stamnavn.pro.
	if (N_ELEMENTS(scrfile) EQ 0) then begin
;
;  Currently loaded file minus post-fix
;   	current = (str_sep(current, "\."))(0)
;
;  If the default scratch-name is not present in the
;  already loaded file, load the default scratch-file.
		if (strpos(current, sim) LT 0L) then begin
			scrfile = defscr & LOADIT=1
		endif else begin
            scrfile = current
            if (KEYWORD_SET(WRITEACCESS) AND (fstat(1)).write EQ 0) then LOADIT=1
            if (current NE defscr) then begin
                if (!INTERACTIVE) then begin
                    print,     " WARNING: Should we"
                    print,     "       1) keep "+current+" loaded?"
                    s = prompt("       2) or instead load "+defscr+"? ")
                endif else begin
                    s = '2'
                endelse
                if (s EQ '2') then begin
                    scrfile = defscr & LOADIT=1
                endif
            endif
		endelse
	endif else begin
;
;  Add the default postfix if not present
		if (strpos(scrfile, '.') LT 0) then  scrfile=scrfile+'.scr'
		if (scrfile NE current) then  LOADIT=1
	endelse
;
;  Is it neccesary to load a new file?
	if (LOADIT) then begin
		fs = findfile(scrfile)
;
;  Default scratch-name doesn't exist
		if (fs(0) EQ '') then begin
;  Look for any *.scr-files
			fsany = findfile('*'+sim+'*.scr')
;  Look for any *.sav-files
			if (fsany(0) EQ '') then fsany=findfile('*'+sim+'*.sav')
;  Look for any *.int-files
			if (fsany(0) EQ '') then fsany=findfile('*'+sim+'*.int')
;  If still no luck, print warning and abort
			if (fsany(0) EQ '') then begin
				print, "  Couldn't find any *"+sim+"*.{scr,sav}*-files."
				print, "  Bailing out!"
                scrfile = ''
				if KEYWORD_SET(DEBUG) then stop  else  return
;  Otherwise print the name of the substitute file
			endif else begin
				print, "  Couldn't find "+scrfile
				scrfile = fsany(N_ELEMENTS(fsany)-1L)
				print, "  Using "+scrfile+" instead."
			endelse
		endif
;
;  We can finally load the file
		file, scrfile, /QUIET, WRITEACCESS=WRITEACCESS
	endif
	if KEYWORD_SET(DEBUG) then print, current, scrfile, defscr, sim, LOADIT, $
                                                form='(4(A0," "), I4)'

	if KEYWORD_SET(STOP) then stop
end
