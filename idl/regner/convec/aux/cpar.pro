;************************************************************************
;  Extract the mpar parameters from the cdata-block.
;  Uses the variable names from cdata.inc. Needs ~/bin/cpar.help and
;  cdata.inc in on of the directories specified in cpar.help (e.g. ./).
;
;  IDL> print, cpar(it,'teff')		; prints teff from timestep it.
;  IDL> print, cpart('teff')		; prints teff from all timesteps in file.
;  IDL> wpar, 6000.0, it, 'teff'	; writes new value to teff of timestep it.
;************************************************************************
;
;   Update history:
;   ---------------
;   22.03.1999 Coded/RT
;   04.04.2006 Byte-swapping of 'dato' and 'time' is corrected/RT
;
;   Known bugs: 'Dato' and 'time' is not properly implemented for Crays...
;
function cparindex,iv, dir=dir, RELOAD=RELOAD
;
	common cpar_names, par_array

	if (N_ELEMENTS(par_array) LE 1L  OR  KEYWORD_SET(RELOAD)) then begin
		par_array = strarr(100)
        cdata_read, dir=dir
;	    spawn,'cpar.help -1',par_array
;		idato = where(par_array EQ 'dato')
;		par_array = [par_array(0:idato(0)),'dato',par_array(idato(0)+1:*)]
;		itime = where(par_array EQ 'time')
;		par_array = [par_array(0:itime(0)),'time',par_array(itime(0)+1:*)]
	endif

	s = size(iv)
	if (s(0) EQ 0  AND  s(1) NE 7) then return,iv

	index = where(par_array eq iv, n)
	if (n LE 0L  OR  index(0) LT 0L) then begin
		print,iv+' not in cpar:'
		print, par_array
		return, -1
	end
	return, index(0)
end
;
pro wpar,f,it,iv, RELOAD=RELOAD
;
	common coff,mpar,mav,ipack,incr,ioff,time,voff,xfile,irec
	common cdat,x,y,z,nx,ny,nz,nw,ntmax,date0,time0 
	common mycom,cray,one,two

	if n_params() eq 0 then begin
		print,'wpar,f,isnap,iarray'
		return
	end
    if (NOT (fstat(1)).write) then begin
        if ((fstat(1)).open) then begin
            print, " ERROR: "+(fstat(1)).name+" was opened without " + $
                           "write-permission. Please reopen with:"
            simf  = "'"+(fstat(1)).name+"'"
        endif else begin
            print, " ERROR: No simulation files are open. Please open with:"
            simf  = "<sim-file>"
        endelse
        print, "        IDL> file, "+simf+", /WRITE"
        print, "        and try 'wpar' again. Bailing out."
        return
    endif
;
	if (N_ELEMENTS(cray) LE 0) then begin & cray=0 & one=0 & two=1 & endif
	iv = cparindex(iv, RELOAD=RELOAD)
	aa = assoc(1, fltarr(two), irec(it*incr+incr-1))
	bb = assoc(1, lonarr(two), irec(it*incr+incr-1))
;	a=assoc(1,fltarr(nz),ioff(it)+(incr-1)*voff+4L*(mpar+iv*nz))
	sf = size(f)
	if (sf(sf(0)+1) GT 3) then $
		aa(iv)=[float(f)] $
	else $
		bb(iv)=[long(f)]
	return
end

function cpart,iv, RELOAD=RELOAD
;
	common coff,mpar,mav,ipack,incr,ioff,time,voff,xfile,irec
	common cdat,x,y,z,nx,ny,nz,nw,ntmax,date0,time0 

	if n_params() eq 0 then begin
		print,'tmp=cpart(iarray)'
		return,0
	end
;
	iv = cparindex(iv, RELOAD=RELOAD)
	sp = size(cpar(0,iv))
	a  = make_array(ntmax, TYPE=sp(sp(0)+1))
	for t=0,ntmax-1 do a(t)=cpar(t,iv)
	return,a
end

function cpar, it, iv, dir=dir, HELP=HELP, RELOAD=RELOAD
;
	common coff,mpar,mav,ipack,incr,ioff,time,voff,xfile,irec
	common cdat,x,y,z,nx,ny,nz,nw,ntmax,date0,time0 
	common cpar_names, par_array
	common mycom,cray,one,two

	if n_params() eq 0 then print,'cpar(isnap,iarray)'
	if (N_ELEMENTS(par_array) LE 1L  OR  KEYWORD_SET(RELOAD)) then begin
        par_array = strarr(100)
        cdata_read, dir=dir
;   	spawn,'cpar.help -1',par_array
;       idato = where(par_array EQ 'dato')
;       par_array = [par_array(0:idato(0)),'dato',par_array(idato(0)+1:*)]
;       itime = where(par_array EQ 'time')
;       par_array = [par_array(0:itime(0)),'time',par_array(itime(0)+1:*)]
    endif
    if KEYWORD_SET(HELP) then begin
		for i=0L, N_ELEMENTS(par_array)-1L do $
			print,i,par_array(i),format='(i3,2x,a)'
		if (N_PARAMS() LT 2L) then return,par_array
	end
	if (N_PARAMS() EQ 0L) then return,0

	if (N_ELEMENTS(cray) LE 0) then begin & cray=0 & one=0 & two=1 & endif
	iv=cparindex(iv, RELOAD=RELOAD)
	if (cray) then begin
;		a=assoc(1,bytarr(8*nz),irec(it*incr+incr-1)+two*4L*(mpar+iv*nz))
;		print, "cpar.pro doesn't work with cray-format files yet!" & stop
;		return, crayflt(a(0))
		aa = assoc(1, bytarr(8),   irec(it*incr+incr-1))
		bb = assoc(1, lonarr(two), irec(it*incr+incr-1))
		if (abs(bb(0,iv)) GT 10000000L) then $
			return, crayflt(aa(iv)) $
		else $
			return, bb(1,iv)
	endif else begin
		if (par_array(iv) EQ 'time'  OR  par_array(iv) EQ 'dato') then begin
			ivs = cparindex(par_array(iv), RELOAD=RELOAD)
			ss = assoc(1, bytarr(8*two), irec(it*incr+incr-1)+ivs*4*two)
;
; Check to see whether 'dato' and 'time' have been garbled-up by
; a byte-swapping by the hydro-code - and correct it.
			if (ss(1,0) LT byte('0')  OR  ss(1,0) GT byte('9')) then begin
				return, string(reverse(ss(0:3,0)))+string(reverse(ss(4:7,0)))
			endif else begin
				return, string(ss(0))
			endelse
		endif
		aa = assoc(1, fltarr(two), irec(it*incr+incr-1))
		bb = assoc(1, lonarr(two), irec(it*incr+incr-1))
		if (abs(bb(0,iv)) GT 10000000L) then $
			return, aa(0,iv) $
		else $
			return, bb(0,iv)
	endelse
end
