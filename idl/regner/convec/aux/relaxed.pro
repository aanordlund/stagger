;
;  Program to find the relaxed part of a series of simulations.
;
;   Update history:
;   ---------------
;   28.09.2006 Coded/RT
;   11.11.2015 Now allows the local set of files to not include a sav-file with
;              p-mode damping or '00', i.e., the beginning of a possible
;              production run is not found. A warning message is issued/RT
;   12.11.2015 Now also check for changes in z and rho-/eebot for relaxedness/RT
;
;   Known bugs: If called with Nmin=0, returns at least 1 sim!
;
pro relaxed, files, last, sim=sim, Nmin=Nmin, relaxedfiles=relaxedfiles, $
																	STOP=STOP
    common cdat,x,y,z,nx,ny,nz,nw,ntmax,date0,time0

	default, Nmin, 2L
	default, sim, stamnavn()

;	spawn, "ls -1t scr1/*.sav|head -1", result
	spawn, "ls -1Lt "+sim+"??.sav", result
	nn  = long(strmid(result,5,2,/REV))
    is  = sort(nn)& Nnn = N_ELEMENTS(nn)
    n00 = nn(is(0L))
    first=string(n00,form='(I02)')
    n1  = nn(is(Nnn-1L))
    last= string(n1, form='(I02)')
	close, 1
	openr, 1, sim+last+'.sav'
	if ((fstat(1)).size LE 4076L) then begin
		print, " WARNING: No time-step written to last file!"
		files = ''
	endif else begin
		file, sim+last+'.sav', /QUIET
		cnu10=cpar(0,'cnu1') & cnu20=cpar(0,'cnu2') & cnu30=cpar(0,'cnu3')
		dtsv0=cpar(0,'dtsave')& ebot0=cpar(0,'eebot')& rbot0=cpar(0,'rhobot')
        z0=z
		if (cpar(0,'period') LT 1e-6  AND  n1 GT 0L) then begin
			n0 = n1
			repeat begin
				n0 = n0-1L
				sn0 = int2str2(n0)
				if (file_test(sim+sn0+'.sav', /READ)) then begin
					file, sim+sn0+'.sav', /QUIET&
                    dzabs = total(abs(z-z0))/z0(Nz-1L)
					NOT_PRODUCTION = (cpar(0,'period') GT 1e-6  OR  $
					  cpar(0,'cnu1') NE cnu10  OR cpar(0,'cnu2') NE cnu20 OR $
					  cpar(0,'cnu3') NE cnu30  OR cpar(0,'dtsave') NE dtsv0 OR $
                      cpar(0,'eebot') NE ebot0 OR cpar(0,'rhobot') NE rbot0 OR $
                      dzabs GT 1e-2)
				endif else begin
					NOT_PRODUCTION = (1 GT 0)
				endelse
			endrep until (NOT_PRODUCTION  OR  n0 EQ n00)
            if (NOT_PRODUCTION) then begin
                n0 = (n0 + 1L) < n1
            endif else begin
                if (n00 GT 0L) then begin
                    print, " WARNING: The first sav-file I have locally is: "+$
                                                sim+first+".sav,"
                    print, "          but it doesn't seem to be the end of " +$
                                     "production run since I haven't yet"
                    print, "          found a damping period larger than 0.0."
                endif
            endelse
			relaxedfiles = int2str2(n0)+'-'+last
			if (n0 EQ n1) then relaxedfiles=last+"-"
		endif else begin
			n0  = (n1 - Nmin + 1L) > n00
			relaxedfiles = "-1"
		endelse
;
;  Make sure to have at least Nmin simulations to proccess, even if the n1 one
;  is a production run and the n0 one isn't
		n0  = min([n0, (n1-Nmin+1L)>0L])
		if (n0 LT n1) then begin
			files = int2str2(n0)+'-'+last
		endif else begin
			files = last
		endelse
	endelse

	if KEYWORD_SET(STOP) then stop
end
