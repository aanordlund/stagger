;
;   Program to print-out the cpar-block of save- and scratch-files.
;   Arguments:
;     file1:    The file for which the cpar-block should be printed.
;     file2:    An optional 2nd file to compare with the 1st.
;     itin:     The time-step to print for [ntmax-1].
;     DIFF:     Set this keyword to only print the parts that differe
;               between file1 and file2. Ignored when printing one file.
;
;   Update history:
;   ---------------
;   ??.??.???? Coded/RT
;   26.09.2009 Improved print-out and added keyword DIFF/RT
;
;   Known bugs: None
;
pro printpar, file1, file2=file2, itin=itin, DIFF=DIFF
	common coff,mpar,mav,ipack,incr,ioff,time,voff,cfile,irec
	common cdat,x,y,z,nx,ny,nz,nw,ntmax,date0,time0
	common cpar_names, par_array

	if (N_ELEMENTS(file1) EQ 1L) then file, file1, /QUIET
	file1 = cfile
	print, file1, ntmax, form='(A12,I4," ",$)'
	default, itin, ntmax-1
	typform0 = ['','I15','I15','I15','15.6','15.6','2F15.6','A15','','2F15.6']
	typform  = typform0
	it = ( (itin>0)<(ntmax-1) )
	for i=0,mpar-1 do begin
		a = cpar(it, i)
		sa = size(a)
		atype = sa(sa(0)+1)
		if (atype EQ 4 OR atype EQ 5) then begin
			absa = abs(a)
			if ((absa LT 1.e-3 AND absa GT 0.0) OR absa GT 1.e4) then $
				typform(4:5) = 'E'+typform0(4:5) $
			else $
				typform(4:5) = 'F'+typform0(4:5)
		endif
		if (N_ELEMENTS(file2) LE 0) then begin
			if (i eq 0) then print, it, form='("  it=",I4)'
			print, a,i,par_array(i), format='('+typform(atype)+',I,A8)'
		endif else begin
			file, file2, /QUIET& if (i eq 0) then print,file2,ntmax,it,$
										form='(A11,I4,"  it=",I4)'
			if (NOT KEYWORD_SET(DIFF)  OR  cpar(it, i) NE a) then begin
				if (cpar(it, i) NE a) then  print,"#",form='(A1,$)' $
									  else  print," ",form='(A1,$)'
				print, a, format='('+typform(atype)+',$)'
				a = cpar(it, i)
				sa = size(a)
				atype = sa(sa(0)+1)
				if (atype EQ 4 OR atype EQ 5) then begin
					absa = abs(a)
					if ((absa LT 1.e-3 AND absa GT 0.0) OR absa GT 1.e4) then $
						typform(4:5) = 'E'+typform0(4:5) $
					else $
						typform(4:5) = 'F'+typform0(4:5)
				endif
				print, a,i,par_array(i), format='('+typform(atype)+',I,A8)'
			endif
			file, file1, /QUIET
		endelse
	endfor
end
