;
;  Function to return the run-directory of a simulation which is
;  already loaded. This to make sure that files that should be in the
;  run-directory are accessed there, eventhough the simulation files
;  might be accessed from somewhere else, e.g., a scratch-directory.
;    If the directory doesn't exist, the function returns the current
;  working directory.
;
;      Update history:
;   ---------------
;   22.04.2003 Coded/RT
;   15.04.2004 Introduced the RELATIVE keyword/RT
;
function simdir, subdir, RELATIVE=RELATIVE
	default, subdir, "/run"
	if (subdir NE "" and  strpos(subdir, "/") NE 0) then subdir="/"+subdir
	if (subdir NE "" and rstrpos(subdir, "/") NE (strlen(subdir)-1L)) then $
														 subdir=subdir+"/"
	sdir = getenv("HOME")+'/convdat/stars/'+simres()+'/'+stamnavn()+subdir
	if ((findfile(sdir))(0) EQ "") then begin
		sdir="./"
	endif else begin
		if KEYWORD_SET(RELATIVE) then $
					  sdir='convdat/stars/'+simres()+'/'+stamnavn()+subdir
	endelse
	return, sdir
end
