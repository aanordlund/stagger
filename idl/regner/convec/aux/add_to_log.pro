;
;   Update history:
;   ---------------
;   27.08.2004 Coded/RT
;   01.09.2004 Cleared up confusion between text and log_text./RT
;   08.12.2005 Improved condition for adding "; " to log_text/RT
;   06.10.2006 Moved the forming of sdate to logtime.pro/RT
;   09.03.2007 Added keyword CLEAR to clear the log_text variable/RT
;   19.06.2007 Now able to handle multi-line texts correctly/RT
;   18.09.2007 Added argument 'cmd', to be used for supplying the calling
;              sequence, when not issued interactively/RT
;   02.10.2007 Changed TR4 in format statement, to actual \tab/RT
;
pro add_to_log, text, logfile=logfile, logdir=logdir, cmd=cmd, CLEAR=CLEAR
	common coff,mpar,mav,ipack,incr,ioff,time,voff,xfile,irec
	common cdat,x,y,z,nx,ny,nz,nw,ntmax,date0,time0
	common cadd_to_log, log_text

	default, logfile,  'NOTES'
	default, logdir,   simdir()
	default, text,     ''
	default, cmd, (recall_commands())(0)
	if KEYWORD_SET(CLEAR) then log_text=''
	if (size(log_text, /TYPE) EQ 7) then log_text = log_text + text $
									else log_text = text
	sc = "; "
	if (strmid(logdir, strlen(logdir)-1, 1) NE '/') then logdir=logdir+'/'
	if (log_text(0) NE '' AND strpos(log_text(0),sc) NE 0) then $
													log_text(0)=sc+log_text(0)

	logtime, logfile=logdir+logfile
	openw, lun, logdir+logfile, /APPEND, /GET_LUN
	printf, lun, string(9b), cmd, log_text, form='(A0,"IDL> ",A0,10(A0,/,$))'
	free_lun, lun
end
