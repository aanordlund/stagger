;
;   Program to list which simulations have \tau-averages and \alpha-fits.
;
;   Update history:
;   ---------------
;   28.06.2007 Coded/RT
;
;   Known bugs: DoesnNone
;
pro listsims, STOP=STOP
@cstarval
	sims  = read_startab(abundis=0, resis='150x150x82')
	name0 = name
	sims  = read_startab(abundis=0, resis='150x150x82', /LMEANS)
	namel = name
	sims  = read_startab(abundis=0, resis='150x150x82', /TMEANS)
	namet = name
	sims  = read_startab(abundis=0, resis='150x150x82', /ALFA)
	namea = name
	N0    = N_ELEMENTS(name0)
	print, "            lmean tmean  alfa"
	for i=0L, N0-1L do begin
		if ((where(namel EQ name0(i)))(0) GE 0L) then stra1="   *  " $
												 else stra1="      "
		if ((where(namet EQ name0(i)))(0) GE 0L) then stra2="   *  " $
												 else stra2="      "
		if ((where(namea EQ name0(i)))(0) GE 0L) then stra3="   *  " $
												 else stra3="      "
		print, name0(i), stra1, stra2, stra3, form='(A12,3A6)'
	endfor
	print, N0,  N_ELEMENTS(namel), N_ELEMENTS(namet), N_ELEMENTS(namea), $
														form='(I10,3I6)'
end
