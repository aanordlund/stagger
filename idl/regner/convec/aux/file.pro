;***************************************************************************
;
;  Write variables in a plane, unpacked, ascii or binary packed
;
;***************************************************************************
pro wpln,f,j,i,pos
	common cmov,cvar,cpos,cfirst,clast,cstep,csize,cfile,ctmp
	common coff,mpar,mav,ipack,incr,ioff,time,voff,xfile,irec
	common cdat,x,y,z,nx,ny,nz,nw,ntmax,date0,time0 
	common cplane,nc
	common mycom,cray,one,two

	if n_params() eq 0 then begin
		print,'wpln,f,itime,ivar,n  [0 indexed]'
		return
	end
	if n_params() lt 3 then pos=cpos
	cpos=pos
	ntot=nx*ny*nz
	nxy=nx*ny

	; unpacked scr file
	if (incr gt 10) then begin   
        if (incr eq 12) then nvar=5		; unpacked hd scr file
		if (incr eq 18) then nvar=8		; unpacked mhd scr file
		a=assoc(1,fltarr(nx,ny),irec(j*incr+i+nvar)+pos*nxy*4L)
		a(0)=f
		return

	;  ascii packed sav file, ipack=1
	endif else if (ipack eq 1) then begin
		nc=((ntot+3)/4)*2
		a=assoc(1,bytarr(32),irec(j*incr+i)+nc*4L)
		b=a(0)
		c1=string(b(0:15))
		c2=string(b(16:31))
		vmin=float(c1)
		vscl=float(c2)
		cscl=1./vscl
		print,b,min(f),vmin,vscl
		print,'wr.offset=',irec(j*incr+i)+pos*nxy*4L,nx*ny
		a=assoc(1,intarr(nx,ny),irec(j*incr+i)+pos*nxy*4L)
		m15=2L^15
  		a(0)=fix((f-vmin)*cscl+0.5)-m15
  		return
	endif else if (ipack eq 2) then begin
		ntot=nx*ny*nz
		nc=(ntot+1)/2
		a=assoc(1,fltarr(2),irec(j*incr+i)+nc*4L)
		b=a(0)
		vmin=b(0)
		vscl=b(1)
		vscl=1./vscl
		print,b,vmin,vscl
		a=assoc(1,intarr(nx,ny),irec(j*incr+i)+pos*nxy*2L)
		m15=2L^15
		a(0)=fix((f-vmin)*vscl+0.5)-m15
		return
	endif else if (ipack eq 3) then begin
		ntot=nx*ny
		nc=((ntot+3)/4)*2
	;	print,j,'*',incr,'+',i,'=',j*incr+i,format='("***wpln: ",3(i2,a1),i2)'
		a=assoc(1,bytarr(32),irec(j*incr+i)+nc*4L+pos*(nc*4L+32))
		b=a(0)
		c1=string(b(0:15))
		c2=string(b(16:31))
		;print,c1,c2
		vmin=min(f)
		m15=2L^15
		vscl=(max(f)-vmin)/(2.^16-1.)
		if vscl eq 0. then vscl=1.
		vscl=1./vscl
		c1=string(vmin,format='(e16.8)')
		c2=string(vscl,format='(e16.8)')
		b(0:15)=byte(c1)
		b(16:31)=byte(c2)
		a(0)=b
		;print,string(b)
		a=assoc(1,intarr(nx,ny),irec(j*incr+i)+pos*(nc*4L+32))
		tmp=fix(long((f-vmin)*vscl+0.5)-m15)
		;print,min(tmp),max(tmp)
		a(0)=tmp
		return
	end
end
;***************************************************************************
;
;  Extract variables in a plane, unpacked, ascii or binary packed
;
;***************************************************************************
function fpln,j,i,pos
	common cmov,cvar,cpos,cfirst,clast,cstep,csize,cfile,ctmp
	common coff,mpar,mav,ipack,incr,ioff,time,voff,xfile,irec
	common cdat,x,y,z,nx,ny,nz,nw,ntmax,date0,time0 
	common cplane,nc
	common mycom,cray,one,two

	if n_params() eq 0 then begin
		print,'f=fpln(itime,ivar,n) [0 indexed]'
		return,0
	end
	if n_params() lt 3 then pos=cpos
	cpos=pos
	ntot=nx*ny*nz
	nxy=nx*ny

	; unpacked scr file
	if (incr gt 10) then begin   
        if (incr eq 12) then nvar=5		; unpacked hd scr file
		if (incr eq 18) then nvar=8		; unpacked mhd scr file
		a=assoc(1,fltarr(nx,ny),irec(j*incr+i+nvar)+pos*nxy*4L)
		return,a(0)
	;  unpacked sav file, ipack=0
	endif else if (ipack eq 0) then begin
		nvar = incr-1
		a=assoc(1,fltarr(nx,ny),irec(j*incr+i)+pos*nxy*4L)
		return,a(0)
	;  ascii packed sav file, ipack=1
	endif else if (ipack eq 1) then begin
		nc=((ntot+3)/4)*2
		a=assoc(1,bytarr(32),irec(j*incr+i)+nc*4L)
		b=a(0)
		c1=string(b(0:15))
		c2=string(b(16:31))
		vmin=float(c1)
		vscl=float(c2)
		;print,b,vmin,vscl
		vscl=1./vscl
		m15=2L^15
		; print,irec(j*incr+i)+pos*nxy*4L
		if cpos ge 0 then begin
			;print,'rd.offset=',irec(j*incr+i)+pos*nxy*4L,nx*ny
			a=assoc(1,intarr(nx,ny),irec(j*incr+i)+pos*nxy*2L)
			return,vmin+vscl*(m15+a(0))
		end else begin
			tmp1=fltarr(nx,ny)
			for n=0,ny-1 do begin
			  a=assoc(1,intarr(nx),irec(j*incr+i)+(n*nxy-cpos*nx)*2L)
			  tmp1(*,n)=vmin+vscl*(m15+a(0))
			end
			return,tmp1		
		end
	endif else if (ipack eq 3) then begin
		ntot=nx*ny
		nc=((ntot+3)/4)*2
		m15=2L^15
		if cpos ge 0 then begin
		; print,j,'*',incr,'+',i,'=',j*incr+i,format='("*fpln*0: ",3(i2,a1),i2)'
		  a=assoc(1,bytarr(32),irec(j*incr+i)+nc*4L+pos*(nc*4L+32))
		  b=a(0)
		  c1=string(b(0:15))
		  c2=string(b(16:31))
		  vmin=float(c1)
		  vscl=float(c2)
		  vscl=1./vscl
		; print,irec(j*incr+i)+pos*nxy*4L
			a=assoc(1,intarr(nx,ny),irec(j*incr+i)+pos*(nc*4L+32))
			return,vmin+vscl*(m15+a(0))
		end else begin
		; print,j,'*',incr,'+',i,'=',j*incr+i,format='("*fpln*1: ",3(i2,a1),i2)'
			tmp1=fltarr(nx,nz)
			for n=0,nz-1 do begin
			  a=assoc(1,bytarr(32),irec(j*incr+i)+nc*4L+n*(nc*4L+32))
			  b=a(0)
			  c1=string(b(0:15))
			  c2=string(b(16:31))
			  vmin=float(c1)
			  vscl=float(c2)
			  vscl=1./vscl
			  a=assoc(1,intarr(nx),irec(j*incr+i)+n*(nc*4L+32)-cpos*nx*2L)
			  tmp1(*,n)=vmin+vscl*(m15+a(0))
			end
			return,tmp1		
		end
	endif else if (ipack eq 2) then begin
		ntot=nx*ny*nz
		nc=(ntot+1)/2
		a=assoc(1,fltarr(2),irec(j*incr+i)+nc*4L)
		b=a(0)
		vmin=b(0)
		vscl=b(1)
		vscl=1./vscl
		;print,b,vmin,vscl
		m15=2L^15
		a=assoc(1,intarr(nx,ny),irec(j*incr+i)+pos*nxy*2L)
		return,vmin+vscl*(m15+a(0))
	end
end
;**************************************************************************
;
;  Float array record
;
;**************************************************************************
function frec,j
	common coff,mpar,mav,ipack,incr,ioff,time,voff,xfile,irec
	common mycom,cray,one,two

	off=irec(j)
	len=(irec(j+1)-off)/4
	a=assoc(1,fltarr(len),off)
	return,a(0)
end
;***************************************************************************
;
;  Extract 3-d variables, unpacked, ascii or binary packed
;
;***************************************************************************
function var,j,i
;
;  extract time j, var i, from file
;
	common coff,mpar,mav,ipack,incr,ioff,time,voff,cfile,irec
	common cdat,x,y,z,nx,ny,nz,nw,ntmax,date0,time0
	common mycom,cray,one,two
;
	if n_params() lt 2 then begin
		print,'tmp=var(itime,ivar)'
		return,0
	end
	ntot=nx*ny*nz
	if (incr gt 10) then begin
		if (incr eq 12) then nvar=5		; unpacked hd scratch file
		if (incr eq 18) then nvar=8		; unpacked mhd scratch file
;		print, 'unpacked scratch file'
		if (cray) then begin
	  		a=assoc(1,bytarr(8L*nx*ny*nz),irec(j*incr+i+nvar))
			tmp = reform(crayflt(a(0)),nx,ny,nz)
		endif else begin
	  		a=assoc(1,fltarr(nx,ny,nz),irec(j*incr+i+nvar))
	  		tmp=a(0)
		endelse
	endif else if ipack eq 0 then begin    ;  unpacked save file
		if (cray) then $
	  		a=assoc(1,dblarr(nx,ny,nz),irec(j*incr+i)) $
		else $
	  		a=assoc(1,fltarr(nx,ny,nz),irec(j*incr+i))
  		tmp=a(0)
	endif else if ipack eq 1 then begin    ;  ascii packed scr file
 	 	nc=((ntot+3)/4)*2
 	 	a=assoc(1,bytarr(32),irec(j*incr+i)+nc*4L*two)
 	 	b=a(0)
 	 	c1=string(b(0:15))
  		c2=string(b(16:31))
  		vmin=float(c1)
  		vscl=float(c2)
  		;print,b,vmin,vscl
  		vscl=1./vscl
  		m15=2L^15
  		a=assoc(1,intarr(nx,ny,nz),irec(j*incr+i))
  		tmp=vmin+vscl*(m15+a(0))
	endif else if ipack eq 2 then begin    ;  binary packed scr file
		nc=(ntot+1)/2
		a=assoc(1,fltarr(2),irec(j*incr+i)+nc*4L)
		b=a(0)
		vmin=b(0)
		vscl=b(1)
		vscl=1./vscl
  		;print,b,vmin,vscl
		m15=2L^15
		a=assoc(1,intarr(nx,ny,nz),irec(j*incr+i))
		tmp=vmin+vscl*(m15+a(0))
	endif else if (ipack eq 3) then begin  
		tmp=fltarr(nx,ny,nz)
		for n=0,nz-1 do tmp(*,*,n)=fpln(j,i,n)
	end
	return,reform(tmp)
end
;***************************************************************************
pro wvar,f,j,i
;
;  write time j, var i to file
;
	common coff,mpar,mav,ipack,incr,ioff,time,voff,cfile,irec
	common cdat,x,y,z,nx,ny,nz,nw,ntmax,date0,time0
	common mycom,cray,one,two
;
	if n_params() eq 0 then begin
		print,'wvar,f,itime,ivar'
		return
	end
	close,1
	openu,1,cfile,/SWAP_IF_LITTLE_ENDIAN
	ntot=nx*ny*nz
	if (incr gt 10) then begin
		if (incr eq 12) then nvar=5		; unpacked hd scratch file
		if (incr eq 18) then nvar=8		; unpacked mhd scratch file
  		a=assoc(1,fltarr(nx,ny,nz),irec(j*incr+i+nvar))
		a(0)=f
  		return
	endif else if ipack eq 0 then begin    ;  unpacked sav file
		a=assoc(1,fltarr(nx,ny,nz),irec(j*incr+i))
		a(0)=f
		return
	endif else if ipack eq 1 then begin    ;  ascii packed sav file
 	 	nc=((ntot+3)/4)*2
		vmin=min(f)
		vscl=max(f)-vmin
  		m15=2L^15
		cscl=(m15+m15-1)/vscl
 	 	a=assoc(1,bytarr(32),irec(j*incr+i)+nc*4L)
		c1=string(vmin,format='(e16.8)')
		c2=string(cscl,format='(e16.8)')
		b=bytarr(32)
		b(0:15)=byte(c1)
		b(16:31)=byte(c2)
  		print,b,vmin,cscl
		a(0)=b
  		a=assoc(1,intarr(nx,ny,nz),irec(j*incr+i))
  		a(0)=fix((f-vmin)*cscl+0.5)-m15
  		return
	endif else if ipack eq 2 then begin    ;  binary packed sav file
		b=fltarr(2)
		vmin=min(f)
		vscl=max(f)-vmin
		m15=2L^15
		cscl=(m15+m15-1)/vscl
		b(0)=vmin
		b(1)=cscl
		print,vmin,cscl,b
		nc=(ntot+1)/2
		a=assoc(1,fltarr(2),irec(j*incr+i)+nc*4L)
		a(0)=b
		a=assoc(1,intarr(nx,ny,nz),irec(j*incr+i))
		a(0)=fix((f-vmin)*cscl+0.5)-m15
		return
	endif else if (ipack eq 3) then begin  
		for n=0,nz-1 do wpln,f(*,*,n),j,i,n
	end
end
function fint,j
	common coff,mpar,mav,ipack,incr,ioff,time,voff,cfile,irec
	common cdat,x,y,z,nx,ny,nz,nw,ntmax,date0,time0 
	common mycom,cray,one,two

	if (cray) then begin
		a=assoc(1,bytarr(8L*nx*ny),irec(j*incr+incr-1)+two*4l*(mpar+mav*nz))
		return, reform(crayflt(a(0)), nx, ny)
	endif else begin
		a=assoc(1,fltarr(nx,ny),irec(j*incr+incr-1)+two*4l*(mpar+mav*nz))
		return,a(0)
	endelse
end
function flnrho,i
	return,var(i,0)
end
function fux,i
	return,var(i,1)
end
function fuy,i
	return,var(i,2)
end
function fuz,i
	return,var(i,3)
end
function fee,i
	return,var(i,4)
end
function fax,i
	return,var(i,5)
end
function fay,i
	return,var(i,6)
end
function faz,i
	return,var(i,7)
end
function ftt,i
	common coff,mpar,mav,ipack,incr,ioff,time,voff,cfile,irec
	if (incr le 10) then return,var(i,incr-2)    ; sav file
	if (incr eq 12) then return,var(i,incr-5-2)  ; scr file hd
	if (incr eq 18) then return,var(i,incr-8-2)  ; scr file mhd
end

;
;  Movie 
;
PRO movie,var=var,pos=pos,first=first,last=last,step=step,size=size,vmin=vmin,vmax=vmax,sample=sample,file=file

	common cmov,cvar,cpos,cfirst,clast,cstep,csize,cfile,ctmp
	common coff,mpar,mav,ipack,incr,ioff,time,voff,xfile,irec
	common cdat,x,y,z,nx,ny,nz,nw,ntmax,date0,time0 
	common cplane,nc
	common mycom,cray,one,two

	flg=1
	print,!d.name

	if n_elements(var)    ne 0 then begin & cvar=var     & flg=0 & end
	if n_elements(pos)    ne 0 then begin & cpos=pos     & flg=0 & end
	if n_elements(first)  ne 0 then begin & cfirst=first & flg=0 & end
	if n_elements(last)   ne 0 then begin & clast=last   & flg=0 & end
	if n_elements(step)   ne 0 then begin & cstep=step   & flg=0 & end
	if n_elements(size)   ne 0 then begin & csize=size   & flg=0 & end
	if n_elements(file)   ne 0 then begin & cfile=file   & flg=0 & end

	if n_elements(sample) eq 0 then begin & sample=0             & end

	if n_elements(file)   eq 0 then begin
		if n_elements(cfile)  eq 0 then begin
			cfile="cmpsav"
			print,"file ?"
			read,cfile
			file,cfile
			flg=0
		end
	end else begin
		cfile=file
		file,cfile
		flg=0
	end
	if n_elements(cvar)   eq 0 then begin
		print,"variabel no (-1=int 0=lnrho 1=ux 2=uy 3=uz 4=ee 5=tt 8=u 9=m) ?"
		read,cvar
		cvar=long(cvar)
	end
	if n_elements(cfirst) eq 0 then begin
		print,"depth index ?"
		read,cpos
		cpos=long(cpos)
	end
	if n_elements(cfirst) eq 0 then begin
		print,"first snapshot ?"
		read,cfirst
		cfirst=long(cfirst)
	end
	if n_elements(clast) eq 0 then begin
		print,"last snapshot ?"
		read,clast
		clast=long(clast)
	end
	if n_elements(cstep) eq 0 then begin
		print,"step snapshot ?"
		read,cstep
		cstep=long(cstep)
	end
	if n_elements(csize) eq 0 then begin
		print,"size of frame (pixels) ?"
		read,csize
		csize=long(csize)
	end

	blow=long(csize/nx)
	sx=blow*nx
	sy=blow*ny
	nm=(clast-cfirst)/cstep+1
	tmax=nm-1

	if flg eq 0 then begin
		ntot=nx*ny*nz
		nc=(ntot+1)/2
		res=fltarr(nx,ny,nm>2)

		t=-1
		print,"reading ..."
		print,cfirst,clast,cstep
		for j=cfirst,clast,cstep do begin
			t=t+1
			if cvar eq 8 then begin
				res(*,*,t)=fpln(j,1)^2+fpln(j,2)^2+fpln(j,3)^2
				res(*,*,t)=sqrt(res(*,*,t))
			end else if cvar eq 9 then begin
				res(*,*,t)=fpln(j,1)^2+fpln(j,2)^2+fpln(j,3)^2
				b=1.67*1.38e-16*fpln(j,5)/(1.2*1.66e-24)*1e-12
				res(*,*,t)=sqrt(res(*,*,t)/b)
			end else if cvar ge 0 then begin
				b=fpln(j,cvar)
				res(*,*,t)=b
			end else begin
				intoff=4l*(mpar+mav*nz)
				a=assoc(1,fltarr(nx,ny),irec(j*incr+incr-1)+intoff)
				res(*,*,t)=a(0)
			end
			print,t,j
		endfor

		if n_elements(vmin) ne 0 then rmin=vmin else begin
			rmin=min(res)
			print,"vmin=",rmin
		end
		if n_elements(vmax) ne 0 then rmax=vmax else begin
			rmax=max(res)
			print,"vmax=",rmax
		end

		if sample eq 3 then begin
			tmpx=res
			res=fltarr(2*nx,2*ny,nm)
			for t=0,nm-1 do begin
				for j=0,ny-1 do res(*,j,t)=[tmpx(*,j,t),tmpx(*,j,t)]
				res(*,ny:ny+ny-1,t)=res(*,0:ny-1,t)
			endfor
			tmpx=0
			sx=sx*2
			sy=sy*2
		endif

		print,"rebinning ..."
		if !d.name eq "TEK" then begin
			if rmin eq rmax then begin
				ctmp=bytarr(sx,sy,nm)
				for i=0,nm-1 do $
					ctmp(*,*,i)=bytscl(res(*,*,i),top=255-64)+64
			end else $
				ctmp=bytscl(res,min=rmin,max=rmax,top=255-64)+64
		end else begin
			if rmin eq rmax then begin
				ctmp=bytarr(sx,sy,nm)
				for i=0,nm-1 do $
					ctmp(*,*,i)= $
					rebin(bytscl(res(*,*,i),top=250),sx,sy)
			end else $
				ctmp= $
				rebin(bytscl(res,min=rmin,max=rmax,top=250),sx,sy,nm)
		end
	end

	repeat begin
		for t=0,tmax do begin
			if !d.name eq "TEK" then begin
				am,ctmp(*,*,t)
			end else if sample eq 0 then begin
				tv,ctmp(*,*,t),0,sy
				tv,ctmp(*,*,t),sx,sy
				tv,ctmp(*,*,t)
				tv,ctmp(*,*,t),sy,0
			end else if sample eq 1 then begin
				tmpx=bytarr(2*sx,2*sy)
				tmpx(0:sx-1,0:sy-1)=ctmp(*,*,t)
				tmpx(sx:2*sx-1,0:sy-1)=ctmp(*,*,t)
				tmpx(0:sx-1,sy:2*sy-1)=ctmp(*,*,t)
				tmpx(sx:2*sx-1,sy:2*sy-1)=ctmp(*,*,t)
				tv,rebin(tmpx,4*sx,4*sy,/sam)
			end else if sample eq 2 then begin
				tmpx=bytarr(2*sx,2*sy)
				tmpx(0:sx-1,0:sy-1)=ctmp(*,*,t)
				tmpx(sx:2*sx-1,0:sy-1)=ctmp(*,*,t)
				tmpx(0:sx-1,sy:2*sy-1)=ctmp(*,*,t)
				tmpx(sx:2*sx-1,sy:2*sy-1)=ctmp(*,*,t)
				tv,tmpx
			end else if sample eq 3 then begin
				tv,ctmp(*,*,t)
			end
			xyouts,2*sx-60,2*sy-10,string(t),/device
			if !d.name eq "SUN" then empty
		endfor
		print,"again ?"
	end until get_kbrd(1) ne "y"
	print,cfile,cvar,cpos,format="(' file=',a,' var=',i1,' pos=',i2)"
	print,cfirst,clast,cstep,csize,format="(' first=',i2,' last=',i2,' step=',i2,'   size=',i3)"
end

function intens,j
;  intensity
	common cmov,cvar,cpos,cfirst,clast,cstep,csize,cfile,ctmp
	common coff,mpar,mav,ipack,incr,ioff,time,voff,xfile,irec
	common cdat,x,y,z,nx,ny,nz,nw,ntmax,date0,time0 
	common mycom,cray,one,two

	ntot=nx*ny
	nc=((ntot+3)/4)*2
	if (cray) then $
		a=assoc(1,dblarr(nx,ny),irec(j*incr)+4L*(mpar+mav*mz)) $
	else $
		a=assoc(1,fltarr(nx,ny),irec(j*incr)+4L*(mpar+mav*mz))
	return,a(0)
end


function intpck,j
;  ascii packed intensity, ipack=3
	common cmov,cvar,cpos,cfirst,clast,cstep,csize,cfile,ctmp
	common coff,mpar,mav,ipack,incr,ioff,time,voff,xfile,irec
	common cdat,x,y,z,nx,ny,nz,nw,ntmax,date0,time0 
	common mycom,cray,one,two

	ntot=nx*ny
	nc=((ntot+3)/4)*2
	a=assoc(1,bytarr(32),irec(j*incr)+nc*4L)
	b=a(0)
	;print,b
	c1=string(b(0:15))
	c2=string(b(16:31))
	vmin=float(c1)
	vscl=float(c2)
	;print,b,vmin,vscl
	vscl=1./vscl
	m15=2L^15
	a=assoc(1,intarr(nx,ny),irec(j*incr))
	return,vmin+vscl*(m15+a(0))
end

pro file,name,WRITEACCESS=WRITEACCESS,DEBUG=DEBUG,QUIET=QUIET,PACK0=PACK0
; load 3D sav or scr file, extract pointers

	common coff,mpar,mav,ipack,incr,ioff,time,voff,cfile,irec
	common cdat,x,y,z,nx,ny,nz,nw,ntmax,date0,time0
	common mycom,cray,one,two

	if KEYWORD_SET(DEBUG) then on_error,1
	if KEYWORD_SET(PACK0)       then ipack=0
    if (N_ELEMENTS(ipack) EQ 0) then ipack=0

; open file (automatically reads fixed record length)
	one=0L & two=1L & cray=0L
	close,1
	if (N_ELEMENTS(WRITEACCESS) NE 0) then openu,1,name,/SWAP_IF_LITTLE_ENDIAN $
									  else openr,1,name,/SWAP_IF_LITTLE_ENDIAN
	cfile=name

; get indices
	i=assoc(1,lonarr(10))
	inext=i(0,0)
	if (inext EQ 0) then begin
		two = 2L & one = 1L & cray = 1L
		i=assoc(1,lonarr(two*10))
		inext=i(0*two+one,0)
	endif
;inext  = 141L
	lcfile=i(1*two+one,0)
;lcfile =  16L
	ndata=i(2*two+one,0)

	nw=i(4*two+one,0)
;	lrec=min([nw,4096L])
	idata=i(5*two+one,0)
;idata  = 140L
	incr=i(7*two+one,0)
;	idata = idata - incr*8L
;incr   =   7L
;	print, "ns0:", (idata-1)/incr+1, max([(idata-1)/incr+1,1])
	j=assoc(1,lonarr(two*10),two*4l*(1+lcfile+1000))
	lrec=j(0*two+one,0)
	if lrec eq 0 then lrec=4096L
;lrec   =   1L
	;number of time steps in file
	ntmax=(idata-1)/incr+1
	if n_elements(quiet) eq 0 then $
	print,'incr  =',incr,'	ntmax =',ntmax,'	lrec =',lrec
	if n_elements(quiet) eq 0 then $
	print,'idata =',idata

; get file pointers
	if inext eq 1 then inext = inext + 2*incr	; for 3d.scr wrap
	irecp=assoc(1,ulonarr(two*inext),two*4l*(lcfile+1))
	iirec = lindgen(inext)*two+one
	irec=ulong(two*4l*lrec)*irecp(iirec,0)

; get data record, values
	mpar=55L			; number of scalar parameters in /cdata/
	mav=17L				; number of (mz) arrays in /cdata/
	timeoffset=18L		; offset of time ("t") param in /cdata/	
	compoffset=51L		; offset of ipack

	data=assoc(1,lonarr(two*mpar),irec(idata-1))
	nx=data(0*two+one,0)
	ny=data(1*two+one,0)
	nz=data(2*two+one,0)

	if ndata gt mpar+nz*mav+nx*ny*(ipack GE 0) then begin
		mpar=58L		; number of scalar parameters in /cdata/
		mav=17L			; number of (mz) arrays in /cdata/
		timeoffset=18L	; offset of time ("t") param in /cdata/	
		compoffset=51L	; offset of ipack
		data=assoc(1,lonarr(two*mpar),irec(idata-1))
	endif
	if ndata gt mpar+nz*mav+nx*ny*(ipack GE 0) or incr eq 2 then begin
;if ndata gt mpar+nz*mav+nx*ny then begin
		mpar=73L		; number of scalar parameters in /cdata/
		mav=22L			; number of (mz) arrays in /cdata/
		timeoffset=17L	; offset of time ("t") param in /cdata/	
		compoffset=31L	; offset of ipack
		data=assoc(1,lonarr(two*mpar),irec(idata-1))
	endif
	if ndata gt mpar+nz*mav+nx*ny*(ipack GE 0) then begin
		mpar=75L		; number of scalar parameters in /cdata/
		mav=22L			; number of (mz) arrays in /cdata/
		timeoffset=17L	; offset of time ("t") param in /cdata/	
		compoffset=31L	; offset of ipack
		data=assoc(1,lonarr(two*mpar),irec(idata-1))
	endif
	if ndata gt mpar+nz*mav+nx*ny*(ipack GE 0) then begin
		mpar=75L		; number of scalar parameters in /cdata/
		mav=35L			; number of (mz) arrays in /cdata/
		timeoffset=17L	; offset of time ("t") param in /cdata/	
		compoffset=31L	; offset of ipack
		data=assoc(1,lonarr(two*mpar),irec(idata-1))
	endif
	if ndata gt mpar+nz*mav+nx*ny*(ipack GE 0) then begin
		mpar=76L		; number of scalar parameters in /cdata/
		mav=35L			; number of (mz) arrays in /cdata/
		timeoffset=17L	; offset of time ("t") param in /cdata/	
		compoffset=31L	; offset of ipack
		data=assoc(1,lonarr(two*mpar),irec(idata-1))
	endif
	if ndata gt mpar+nz*mav+nx*ny*(ipack GE 0) then begin
		mpar=76L		; number of scalar parameters in /cdata/
		mav=39L			; number of (mz) arrays in /cdata/
		timeoffset=17L	; offset of time ("t") param in /cdata/	
		compoffset=31L	; offset of ipack
		data=assoc(1,lonarr(two*mpar),irec(idata-1))
	endif
	if ndata gt mpar+nz*mav+nx*ny then begin ; cdata_mhd_9109
		mpar=81L		; number of scalar parameters in /cdata/
		mav=43L			; number of (mz) arrays in /cdata/
		timeoffset=17L	; offset of time ("t") param in /cdata/
		compoffset=31L	; offset of ipack
		data=assoc(1,lonarr(mpar),irec(idata-1))
	endif
	if ndata gt mpar+nz*mav+nx*ny then begin ; cdata_mhd
		mpar=83L		; number of scalar parameters in /cdata/
		mav=48L			; number of (mz) arrays in /cdata/
		timeoffset=17L	; offset of time ("t") param in /cdata/
		compoffset=31L	; offset of ipack
		data=assoc(1,lonarr(mpar),irec(idata-1))
	endif

	if compoffset gt 0 then $
	  ipack=data(compoffset*two+one,0) $
	else $
	  ipack=1
;	if (incr GT 10L) then ipack=1L
	if n_elements(quiet) eq 0 then $
	print,'nx    =',nx,'	ny   =',ny,  '	nz =',nz
	if n_elements(quiet) eq 0 then $
	print,'ndata =',ndata,'	mpar =',mpar,'		ipack =',ipack
	if (one) then begin
		rb=assoc(1,bytarr(8*ndata),irec(idata-1-(ntmax-1)*incr))
		r1 = reform(crayflt(rb(0)),ndata,1)
	endif else $
		r1=assoc(1,fltarr(ndata*two),irec(idata-1-(ntmax-1)*incr))
	tbegin=r1(timeoffset,0)

; vertical and horizontal scales
	if (cray) then begin
		rb=assoc(1,bytarr(8*ndata),irec(idata-1))
		rr = reform(crayflt(rb(0)),ndata,1)
	endif else $
		rr=assoc(1,fltarr(ndata),irec(idata-1))
	z=rr(mpar:mpar+nz-1,0)
	dx=rr(6,0)
	dy=rr(7,0)
	x=indgen(nx)*dx
	y=indgen(ny)*dy
	tend=rr(timeoffset,0)
	if n_elements(quiet) eq 0 then $
	print,'tbegin =',tbegin,'   tend =',tend

;
	if ipack eq 2 then nw=nw/2  ; compressed records
	nbuf=(nw-1)/lrec+1
	nw=nbuf*lrec

;
	if inext gt 1 then voff=irec(1)-irec(0) else voff=0
	ioff=lonarr(ntmax)
	if (cray) then time=dblarr(ntmax) else time=fltarr(ntmax)
	for i=0,ntmax-1 do begin
		ioff(i)=irec(i*incr)
		if (cray) then begin
			datab=assoc(1,bytarr(8), $
							two*4l*timeoffset+irec(idata-1-(ntmax-1-i)*incr))
			data=reform(crayflt(datab(0)),1,1)
		endif else $
			data=assoc(1,fltarr(1), $
							two*4l*timeoffset+irec(idata-1-(ntmax-1-i)*incr))
		time(i)=data(0,0)
	end

;	print,'times =',time
;	print,'ioff =',ioff

	ilnr=0
	ivx=1
	ivy=2
	ivz=3
	if incr eq 7 then begin
		ibx=-1
		iby=-1
		ibz=-1
		iee=4
		itt=5
	end else begin
		iee=4
		ibx=5
		iby=6
		ibz=7
		itt=8
	end

if (0) then begin
	print, "inext: ", inext
	print, "lcfile: ", lcfile
	print, "ndata: ", ndata
	print, "nw: ", nw, nbuf*lrec, nbuf, lrec
	print, "idata: ", idata
	print, "incr: ", incr
	print, "lrec: ", lrec
	print, "ntmax: ", ntmax
	print, "irec: ", irec

	print, "mpar: ", mpar
	print, "mav: ", mav
	print, "ipack: ", ipack
	print, "ioff: ", ioff
	print, "time: ", time
	print, "nx*ny*nz: ", nx,ny,nz, form='(A0,I0,"*",I0,"*",I0)'
endif

return
end
