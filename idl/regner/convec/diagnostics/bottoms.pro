;
;   Program to plot, timeseries of internal energy, ee, density, rho,
;   and specific entropy at the bottom of the simulations, to monitor
;   the bottom boundary condition.
;
;   Update history:
;   ---------------
;   08.10.1998 Coded/RT
;   16.01.2001 Changed to double-precision/RT
;   13.12.2004 Introduced _EXTRA=extra_args and parse to plot statements/RT
;   16.12.2004 printout of dXbot/dt and it's sigma/RT
;   09.06.2005 dXbot/dt printout now handles negative numbers and the case
;              sigma > |dXbot/dt| correctly/RT
;   09.12.2005 Dynamic allocation of arrays - can handle any ntmax/RT
;   21.03.2007 More compact print-out and prompting/RT
;   31.03.2014 Added option to 'stop' on typing 's'/RT
;
pro bottoms, iz0, files=files, it00=it00, tabfile=tabfile, tid=tid, stal=stal, $
            SCRATCH=SCRATCH, SAVE=SAVE, LOAD=LOAD, _EXTRA=extra_args, STOP=STOP
	common coff,mpar,mav,ipack,incr,ioff,time,voff,cfile,irec
	common cdat,x,y,z,nx,ny,nz,nw,ntmax,date0,time0
	
	default, files, '??'
	default, tabfile, stamnavn()
	if (strpos(tabfile, '.tab') LT 0) then tabfile=tabfile+'.tab'
	default, nzmax, 1
	default, it00, 0
	device = strlowcase(!D.NAME)
    rt_graphics
	if (device NE 'x') then begin
		loadct, 1
		col1 = (!D.TABLE_SIZE-1.)*.5
	endif else begin
		loadct, 7
		col1 = (!D.TABLE_SIZE-1.)*.7
	endelse

	ff = simlist(files, NOSCRATCH=1-KEYWORD_SET(SCRATCH))
    nf = N_ELEMENTS(ff)
    file, ff(0), /QUIET
	default, iz0, nz-1L
; The iz0 variable can also be given as a height instead of an index...
	siz0 = size(iz0)
	if (siz0(siz0(0)+1) GT 3) then dum=min(abs(z-iz0),iz0)
	if (iz0 LT nz-1) then NOTBOT=1  else   NOTBOT=0
	striz0 = string(iz0,form='(I0)')
	cnvdir = simdir("")
;	cnvdir = 'stars/'+stamnavn()
;	hosts_dir, cnvdir=cnvdir
	Sfile = cnvdir+'/SSbot'+striz0+'.dat'
	Nt=0 & im=0

	!x.title = 't/!3[!xmin.!3]!x'
	if KEYWORD_SET(LOAD) then begin
		print, 'Loading ', Sfile
		close, 7
		openr, 7, Sfile
		readf, 7, Nt, nzmax
		a2 = dblarr(Nt, 4)
		a3 = dblarr(nzmax, Nt, 9L)
		readu, 7, a2, a3
		close, 7
		tid = a2(*,0) & SSbot=a2(*,1) & eebot=a2(*,2) & rhobot=a2(*,3)
		SSin =a3(*,*,0) & eein =a3(*,*,3) & rhoin =a3(*,*,6)
		SSnz =a3(*,*,1) & eenz =a3(*,*,4) & rhonz =a3(*,*,7)
		SSmax=a3(*,*,2) & eemax=a3(*,*,5) & rhomax=a3(*,*,8)
	endif else begin
		tid   = dblarr(ntmax)
		SSmax = dblarr(nzmax, ntmax)
		SSnz  = dblarr(nzmax, ntmax)
		SSin  = dblarr(nzmax, ntmax)
		eemax = dblarr(nzmax, ntmax)
		eenz  = dblarr(nzmax, ntmax)
		eein  = dblarr(nzmax, ntmax)
		eebot = dblarr(ntmax)
		rhomax= dblarr(nzmax, ntmax)
		rhonz = dblarr(nzmax, ntmax)
		rhoin = dblarr(nzmax, ntmax)
		rhobot= dblarr(ntmax)
		nf0 = 0
		im = nf0 + nf
		table, tabfile
		sstab, iS
		SS0 = 1d0*lookup(alog(10)*.5, 2.5, iv=iS)
		uS = 1.d+4

		for n=0,nf-1 do begin
			file, ff(n), /QUIET& print, ' ', cfile, ntmax
			Nxy = double(nx*ny)
			nm = mpar/nz+1 & off0 = nz*nm-mpar
;
;  If time doesn't increase monotonically, disconnect by adding
;  step with t=1e50. Can't happen to the first file analyzed.
			if (time(0) LT tid((Nt-1)>0)) then begin
				rhomax=[[rhomax], [rhomax(*,Nt-1)]]
				rhonz =[[rhonz],  [rhonz(*,Nt-1)]]
				rhoin =[[rhoin],  [rhoin(*,Nt-1)]]
				rhobot= [rhobot,   rhobot(Nt-1)]
				eemax =[[eemax],  [eemax(*,Nt-1)]]
				eenz  =[[eenz],   [eenz(*,Nt-1)]]
				eein  =[[eein],   [eein(*,Nt-1)]]
				eebot = [eebot,    eebot(Nt-1)]
				SSmax =[[SSmax],  [SSmax(*,Nt-1)]]
				SSnz  =[[SSnz],   [SSnz(*,Nt-1)]]
				SSin  =[[SSin],   [SSin(*,Nt-1)]]
				tid   = [tid,      1.e50]
				Nt    = Nt+1
			endif
			d1 = dblarr(ntmax)
			d2 = dblarr(nzmax, ntmax)
			if (n GT 0L) then begin
				rhomax=[[rhomax], [d2]]
				rhonz =[[rhonz],  [d2]]
				rhoin =[[rhoin],  [d2]]
				rhobot= [rhobot,   d1]
				eemax =[[eemax],  [d2]]
				eenz  =[[eenz],   [d2]]
				eein  =[[eein],   [d2]]
				eebot = [eebot,    d1]
				SSmax =[[SSmax],  [d2]]
				SSnz  =[[SSnz],   [d2]]
				SSin  =[[SSin],   [d2]]
				tid   = [tid,      d1]
			endif
			for i=0,ntmax-1 do begin
				for iz=0, nzmax-1 do begin
					ups = double(fpln(i,3,iz0-iz) LE 0.0)
					Nups = total(ups)
					lnrpln = 1d0*fpln(i,0,iz0-iz)
					rhopln = exp(lnrpln)
					eepln = 1d0*fpln(i,4,iz0-iz)
					rhomax(iz, Nt+i) = max(rhopln)
					rhonz(iz, Nt+i) = total(rhopln)/Nxy
					rhoin(iz, Nt+i) = total(rhopln*ups)/Nups
					eemax(iz, Nt+i) = max(eepln)
					eenz(iz, Nt+i) = total(eepln)/Nxy
					eein(iz, Nt+i) = total(eepln*ups)/Nups
					SSpln = (lookup(lnrpln, eepln,iv=iS)-SS0)*uS
					SSmax(iz, Nt+i) = max(SSpln)
					SSnz(iz, Nt+i) = total(SSpln)/Nxy
					SSin(iz, Nt+i) = total(SSpln*ups)/Nups
				endfor
				rhobot(Nt+i) = cpar(i, 'rhobot')
				eebot(Nt+i) = cpar(i, 'eebot')
			endfor
			tid(Nt:Nt+ntmax-1)  = time
			Nt = Nt+ntmax
		endfor
		Nt = Nt-1
		tid   = tid(it00:Nt)
		rhomax= rhomax(*, it00:Nt)
		rhonz = rhonz(*, it00:Nt)
		rhoin = rhoin(*, it00:Nt)
		rhobot= rhobot(it00:Nt)
		eemax = eemax(*, it00:Nt)
		eenz  = eenz(*, it00:Nt)
		eein  = eein(*, it00:Nt)
		eebot = eebot(it00:Nt)
		SSmax = SSmax(*, it00:Nt)
		SSnz  = SSnz(*, it00:Nt)
		SSin  = SSin(*, it00:Nt)
		SSbot = (lookup(alog(rhobot),eebot,iv=iS)-SS0)*uS
	endelse
	uE    = (cpar(0,'uul')/cpar(0,'uut'))^2
	fform = 'bot/dt=[",f6.3,"'+string(177b)+'",f5.3,"]x1e",I0," '
	tiden = (tid-tid(0))/.6
	Nt    = Nt-it00
	itfin = where(finite(tiden))
	dum   = min(abs(tiden(itfin)-max(tiden(itfin))/4.), itp1)
	dum   = min(abs(tiden(itfin)-max(tiden(itfin))/3.), itp2)
	it    = sort(tiden)
	iz    = nzmax-1L
;
; Linear fit of rhoin
	Ar = linfit(tiden(itfin), reform(rhoin(iz, itfin)), sigma=sigm)
	fsig = [Ar(1),sigm(1)]*60.*cpar(0,'uur')
	fexp = fix(alog10(max(abs(fsig))))-(max(abs(fsig)) LT 1d0)
	print, fsig/1d1^fexp, fexp, form='("drh'+fform+'g/cm^3/s")'
;
; Linear fit of eein
	Ae   = linfit(tiden(itfin), reform(eein(iz, itfin)), sigma=sigm)
	fsig = [Ae(1),sigm(1)]*60.*uE
;	fexp = fix(alog10(fsig(0)))-(fsig(0) LT 1d0)
	fexp = fix(alog10(max(abs(fsig))))-(max(abs(fsig)) LT 1d0)
	print, fsig/1d1^fexp, fexp, form='("dee'+fform+'erg/s")'
;
; Linear fit of SSin
	AS   = linfit(tiden(itfin), reform(SSin(0, itfin)), sigma=sigm)
	fsig = [AS(1),sigm(1)]*60.*uE
;	fexp = fix(alog10(fsig(0)))-(fsig(0) LT 1d0)
	fexp = fix(alog10(max(abs(fsig))))-(max(abs(fsig)) LT 1d0)
	print, fsig/1d1^fexp, fexp, form='("dSS'+fform+'erg*K/s")'
;
; Print the menu of choises
	print,'<e_in>, <rho_in>:', aver(eein(0,*)), aver(rhoin(0,*))
	if (device EQ 'x') then begin
		print, ''
		print, '     1           2         3         4'
		print, ' rho_bot(t), ee_bot(t), S_bot(t), S_bot(t).'
	endif
;
; Loop over choises
	b0=(byte('0'))(0)
	default, stal, '1'
	tal = (byte(stal))(0)-b0
	Nchoice = 4
	repeat begin
		case tal of
		1: begin
			rmin = min([rhonz, transpose(rhobot), rhomax, rhoin], max=rmax)
			if (NOTBOT) then rmin = min([rhonz, rhomax, rhoin], max=rmax)
			dr = (rmax-rmin)/25.
			plot,tiden,rhobot,yr=[rmin,rmax],ysty=2,ytit='!7q!x!ibot!n',line=1,$
													  _EXTRA=extra_args
				xyouts, tiden(itp1), rhobot(itp1)+dr, '!7q!x!ibot!n'
			for iz=0, nzmax-1 do begin
				oplot, tiden, rhomax(iz, *),          _EXTRA=extra_args
					xyouts, tiden(itp1), rhomax(iz,itp1)+dr, '!7q!x!imax!n'
				oplot, tiden, rhonz(iz, *), line=2,   _EXTRA=extra_args
					xyouts, tiden(itp1), rhonz(iz,itp1)+dr, '<!7q!x>!inz!n'
				oplot, tiden, rhoin(iz, *), col=col1, _EXTRA=extra_args
					xyouts, tiden(itp2),rhoin(iz,itp2)+dr,'!7q!x!iin!n',col=col1
				oplot, !x.crange, Ar(0)+!x.crange*Ar(1),line=2,_EXTRA=extra_args
			endfor
		   end
		2: begin
			rmin = min([eenz, transpose(eebot), eemax, eein], max=rmax)
			if (NOTBOT) then rmin = min([eenz, eemax, eein], max=rmax)
			dr = (rmax-rmin)/25.
			plot,tiden,eebot,yr=[rmin,rmax],ysty=2,ytit='!7e!x!ibot!n',line=1,$
													  _EXTRA=extra_args
				xyouts, tiden(itp1), eebot(itp1)+dr, '!7e!x!ibot!n'
			for iz=0, nzmax-1 do begin
				oplot, tiden, eemax(iz, *),           _EXTRA=extra_args
					xyouts, tiden(itp1), eemax(iz,itp1)+dr, '!7e!x!imax!n'
				oplot, tiden, eenz(iz, *), line=2,    _EXTRA=extra_args
					xyouts, tiden(itp1), eenz(iz,itp1)+dr, '<!7e!x>!inz!n'
				oplot, tiden, eein(iz, *), col=col1,  _EXTRA=extra_args
					xyouts, tiden(itp2), eein(iz,itp2)+dr,'!7e!x!iin!n',col=col1
				oplot, !x.crange, Ae(0)+!x.crange*Ae(1),line=2,_EXTRA=extra_args
			endfor
		   end
		3: begin
			rmin = min([SSnz, transpose(SSbot), SSmax, SSin], max=rmax)
			dr = (rmax-rmin)/25.
			plot,tiden,SSbot,yr=[rmin,rmax],ysty=2,ytit='S!ibot!n',line=1, $
													  _EXTRA=extra_args
				xyouts, tiden(itp1), SSbot(itp1)+dr, 'S!ibot!n'
			for iz=0, nzmax-1 do begin
				oplot, tiden, SSmax(iz, *),           _EXTRA=extra_args
					xyouts, tiden(itp1), SSmax(iz,itp1)+dr, 'S!imax!n'
				oplot, tiden, SSnz(iz, *), line=2,    _EXTRA=extra_args
					xyouts, tiden(itp1), SSnz(iz,itp1)+dr, '<S>!inz!n'
				oplot, tiden, SSin(iz, *), col=col1,  _EXTRA=extra_args
					xyouts, tiden(itp2), SSin(iz,itp2)+dr, 'S!iin!n', col=col1
				oplot, !x.crange, AS(0)+!x.crange*AS(1),line=2,_EXTRA=extra_args
			endfor
		   end
		4: begin
			rmin = min([transpose(SSbot), SSin(0, *)], max=rmax)
			dSrange = (rmax-rmin)*0.05
			Srange=[rmin-dSrange, rmax+dSrange]
			plot2, tiden, SSin(0,*), (SSin(0,*)/SSbot(0)-1.0)*100.0, $
				y1tit='S!iin!n', y2tit='(S!iin!n-S!ibot!n)/S!ibot!n!3[%]!x', $
				y1r=Srange, y2r=(Srange/SSbot(0)-1.0)*100.0, $
				y1sty=9, y2sty=21, xmarg=[16,9], p2sym=-2, sym2size=0.4
			oplot, tiden, SSbot, line=1,              _EXTRA=extra_args
			oplot, !x.crange, AS(0)+!x.crange*AS(1), line=2, _EXTRA=extra_args
		   end
		else: print, 'The program stops'
		endcase
		if (device EQ 'x') then begin
			stal = prompt('  Enter your choice: ', /ONELINE)
			tal = (byte(stal))(0)-b0
		endif else begin
			tal = tal+1b
		endelse
	endrep until (tal LT 1 OR tal GT Nchoice)
	print, ""
    if (stal EQ 's') then stop

	reset_graphics
	if KEYWORD_SET(SAVE) then begin
		Nt = Nt + 1
		Sf = findfile(Sfile)
		if (Sf(0) EQ '') then begin
			print, 'Opening new file: ',Sfile
			openw, 7, Sfile
			printf,7, Nt, nzmax
			writeu,7, tid,SSbot,eebot,rhobot,SSin,SSnz,SSmax,eein,eenz,eemax, $
															rhoin,rhonz,rhomax
		endif else begin
			openr, 7, Sfile
			readf, 7, itr, nzmaxr
			if (nzmaxr NE nzmax) then begin
				print, "Conflicting nzmax's. Bailing out!"
				stop
			endif
			a2 = dblarr(itr,4)
			a3 = dblarr(nzmax,itr,9)
			readu, 7, a2, a3
			close, 7
			tid  =[a2(*,0),tid]   & SSbot =[a2(*,1),SSbot]
			eebot=[a2(*,2),eebot] & rhobot=[a2(*,3),rhobot]
			j = uniq(tid, sort(tid))
			SSin =[[a3(*,*,0)],[SSin]]  & eein =[[a3(*,*,3)],[eein]]
										  rhoin =[[a3(*,*,6)],[rhoin]]
			SSnz =[[a3(*,*,1)],[SSnz]]  & eenz =[[a3(*,*,4)],[eenz]]
										  rhonz =[[a3(*,*,7)],[rhonz]]
			SSmax=[[a3(*,*,2)],[SSmax]] & eemax=[[a3(*,*,5)],[eemax]]
										  rhomax=[[a3(*,*,8)],[rhomax]]
			print, 'Appending to file: ', Sfile
			openw, 7, Sfile
			printf,7, Nt+itr, nzmax
			writeu,7, tid(j),SSbot(j),eebot(j),rhobot(j), $
					SSin(j),SSnz(j),SSmax(j),eein(j),eenz(j), $
					eemax(j),rhoin(j),rhonz(j),rhomax(j)
		endelse
		close, 7
	endif
	if KEYWORD_SET(STOP) then stop
end
