;
; Program to plot the timestep in a series of simulations, and indicate
; what kind of timestep is used.
;
;   Update history:
;   ---------------
;   09.04.2003 Coded/RT
;   19.05.2004 Now handles both straight dtx'es as well as dt/dtx/RT
;   21.05.2004 Corrected error when nstep > 9999 /RT
;   10.09.2004 Changed splitting of steps-string, to first split on "=",
;              and then split on " ". This is more robust/RT
;   24.07.2013 Added argument xrange./RT
;   23.10.2015 Added argument dtrange and changed yrange of dt to include 0./RT
;
;   Known bugs: Failure when t > 10000.
;
pro dtplot, files=files, xrange=xrange, dtrange=dtrange, STOP=STOP
	common cdat,x,y,z,nx,ny,nz,nw,ntmax,date0,time0

	default, files, '??'
    rt_graphics
; Load the scratch-file to obtain unit-conversion-factors
	fscr = findfile(stamnavn()+".scr")
	ut   = 1e2 & fdtc=1./0.5 & fdtd=1./0.5 & fdtr=1./0.1
	if (fscr(0) NE '') then begin
		file, fscr(0), /QUIET
		ut = cpar(0,'uut')
		fdtc = 1./cpar(0,'fdtc')
		fdtd = 1./cpar(0,'spare3')
	endif else begin
		print, "  WARNING: Couldn't find a scratch-file - winging it, i.e.,"
		print, ut, 1./fdtc, 1./fdtd, 1./fdtr, format=$
			'(TR11,"using ut=",e6.0," fdtc=",f4.2," fdtd=",f4.2," fdtr=",f4.2)'
	endelse
; Load the log-files
	if (strpos(files(0),'.log') LT 0) then f = simlist(files, ext='.log') $
									  else f = files
	Nf = N_ELEMENTS(f)
	ifile=lonarr(Nf)
	for i=0, Nf-1 do begin
		spawn, 'grep "^ step=" '+f(i)+' | grep -v NaN ', step
		spawn, 'grep "^ step=" '+f(i)+' | grep -c NaN ', Nnans
		if (Nnans GT 0L) then print, " **** Found "+string(Nnans,form='(I0)')+$
			" timesteps with NaNs in "+f(i)+" (out of " + $
			string(N_ELEMENTS(step)*(step(0) NE ""), form='(I0)') + $
			" good timesteps). ****"
		if (step(0) NE "") then begin
			if (i EQ 0) then steps = step $
						else steps = [steps, step]
		endif
		ifile(i) = N_ELEMENTS(steps)-1L
	endfor
	Nsteps = N_ELEMENTS(steps)
	if (Nsteps LE 0L) then begin
		print, "  No time-steps in the requested log-files. Returning."
		return
	endif
	t  = fltarr(Nsteps)
	dt = fltarr(Nsteps)
	iz = lonarr(Nsteps)
	dtc= fltarr(Nsteps)
	dtd= fltarr(Nsteps)
	dtr= fltarr(Nsteps)
	izc= lonarr(3)
	itp= intarr(Nsteps)
; The various dt[x]'s are printed in the log-file as dt/dt[x]
; - and that's what they are here too...
	for i=0L, Nsteps-1L do begin
		stepses = str_sep(steps(i), "=")
		if (N_ELEMENTS(stepses) EQ 5) then begin
			dt(i)   = float(stepses(3))
			stepses = str_sep(stepses(4), " ")
			dtc(i)  = float(stepses(0))
			izc(0)  =  long(stepses(1))
			dtd(i)  = float(stepses(2))
			izc(1)  =  long(stepses(3))
			dtr(i)  = float(stepses(4))
			izc(2)  =  long(stepses(5))
		endif else begin
			stepses = str_sep(stepses(1), " ")
			if (N_ELEMENTS(stepses) EQ 8L) then $
			  stepses=[strmid(steps(i),6,5),strmid(steps(i),11,12),stepses(1:*)]
			dt(i)   = float(stepses(2))
			dtc(i)  = float(stepses(3))
			izc(0)  =  long(stepses(4))
			dtd(i)  = float(stepses(5))
			izc(1)  =  long(stepses(6))
			dtr(i)  = float(stepses(7))
			izc(2)  =  long(stepses(8))
		endelse
		dum = min([dtc(i), dtd(i), dtr(i)], dt_type)
;
; If not printing the actual dtx, divide by fdtx to determine type of time-step.
		if (dt(i) NE dum) then $
			dum = max([fdtc*dtc(i),fdtd*dtd(i),fdtr*dtr(i)], dt_type)
; Change from Fortran- to IDL-style indexing (starting at 0).
		iz(i) = izc(dt_type)-1L
		itp(i)= dt_type
;		if (i EQ 0) then t(i)=float(stepses(0)) $
		if (i EQ 0) then t(i)=0.0 $
					else t(i)=t(i-1)+dt(i)
	endfor
	t = t - t(0)
	default, ut, 1e2
    default, xrange,  [0, ut*t(Nsteps-1L)]
    default, dtrange, [0, ut*max(dt)*1.03]
    izmn = min(iz, max=izmx)& izmrg = (izmx-izmn)*0.02
    izrange = [(izmn-izmrg)>0, (izmx+izmrg)<(Nz-1L)]
    print, 'xr(dtplot): ', xrange
	strt = '!8t!5/[!xs!5]!x'
	loadct, 12, /SILENT
	!y.margin=[5,2]
	ticks, xrange, tickval, Nticks, Nminor, ticknames
	plot2, t*ut, dt*ut, iz, xtitle=strt, p2sym=2, sym2size=.2, $
						xticks=Nticks, xtickv=tickval, xminor=Nminor, $
						y1title='!7D'+strt, y2title='!8i!dz!n!x', $
						col2=(!D.TABLE_SIZE-1.)/4.*(1.+itp), xrange=xrange, $
                        y1range=dtrange, y1style=9, y2range=izrange, y2style=9
	oplot, !x.crange, [1,1]*min(dt, max=maxdt)*ut, line=1
	oplot, !x.crange, [1,1]*maxdt*ut, line=1
	oplot, !x.crange, [1,1]*aver(dt)*ut, line=2
	flen = strlen(f(0L))
	for i=0,Nf-1 do begin
		oplot, [1,1]*t(ifile(i))*ut, !y.crange, line=1
		tx   = .5*ut*(t(ifile(i)) + (i GT 0)*t(ifile((i-1)>0)))
		if (i EQ 0) then sf=strmid(f(i),0,flen-4)  else sf=strmid(f(i),flen-6,2)
		xyouts, tx, !y.crange(1)*.9, sf, align=0.5, chars=0.8
	endfor
	label, .2, .60, 0, 'Courant',   psym=2, syms=.4, col=(!D.TABLE_SIZE-1.)/4.*1
	label, .2, .55, 0, 'Diffusive', psym=2, syms=.4, col=(!D.TABLE_SIZE-1.)/4.*2
	label, .2, .50, 0, 'Radiative', psym=2, syms=.4, col=(!D.TABLE_SIZE-1.)/4.*3
	print, '<dt> = ', aver(dt)
	if KEYWORD_SET(STOP) then STOP
	reset_graphics
end
