;
;   Program to display the ee and lnr extend of the simulation with
;   respect to the *.tab-table boundaries.
;
;   Update history:
;   ---------------
;   ??.??.1998 Coded/RT
;   24.06.2004 Include choise of time-step of sav-/scr-file to display/RT
;   12.10.2004 Use plots,lnr(iz),ee(iz)  instead of oplot./RT
;   24.12.2006 Improved plot-range handling for EOSrhoe-tables/RT
;   25.02.2016 Added keyword LNE to plot ln(ee) instead of ee./RT
;
function fln, x
	common ctable,ttmean,rhm,drhm,eemin,eemax,dee,mtab,itab,tab

	if (eemin(0) LT 0.0) then return,alog(x)  else return,x
end

pro tabshot, it, iz=iz, scrfile=scrfile, tabfile=tabfile, mz=mz, LNE=LNE, $
        xrange=xrange, yrange=yrange, SIMRANGE=SIMRANGE, OPLOT=OPLOT, STOP=STOP
	common cdat,x,y,z,nx,ny,nz,nw,ntmax,date0,time0
	common ctable,ttmean,rhm,drhm,eemin,eemax,dee,mtab,itab,tab

    rt_graphics
    c0 = (!D.TABLE_SIZE-1.0)*.6
	c1 = (!D.TABLE_SIZE-1.0)*.8
; Find and open the scratch- (or sav-) file.
    if (N_ELEMENTS(scrfile) GT 0) then begin
        if (strpos(scrfile, '.scr') LT 0  AND $
			strpos(scrfile, '.sav') LT 0) then scrfile=scrfile+'.scr'
		print, scrfile
        file, scrfile, /QUIET
    endif else begin
        scrfile = stamnavn()+'.scr'
        fst = fstat(1)
        if (strpos(fst.name,'.scr') LT 0) then begin
			print, scrfile
			file, scrfile, /QUIET
		endif
    endelse
; Default timestep is the last one
	default, it, ntmax-1L
	it = (it > 0) < (ntmax-1L)
; Find and open the tab-file.
    if (N_ELEMENTS(tabfile) GT 0) then begin
        if (strpos(tabfile, '.tab') LT 0) then tabfile=tabfile+'.tab'
        table, tabfile, mz=mz
    endif else begin
        if (file_test('EOSrhoe.tab')) then tabfile = 'EOSrhoe.tab'  $
                                      else tabfile = stamnavn()+'.tab'
        fst = fstat(2)
        if (strpos(fst.name,'.tab') LT 0) then table, tabfile, mz=mz
    endelse
	if (eemin(0) LT 0.0 AND NOT KEYWORD_SET(SIMRANGE)) then print, $
		'Plotting universal table; the default axis-box outlines the table.'
    if (eemin(0) GT 0.0  AND  KEYWORD_SET(LNE)) then begin
        eemin = alog(eemin) & eemax=alog(eemax)
    endif
	ee = fln(fee(it))
	lnr = flnrho(it)
	i2 = indgen(30)*nx/30
	minee = min(ee, max=maxee)
	minlnr = min(lnr, max=maxlnr)
	eebot = fln(cpar(0,'eebot'))
	if KEYWORD_SET(SIMRANGE) then begin
		default, xrange, [min(lnr, max=xrmax)-1., xrmax+1.]
		default, yrange, [min(ee,  max=yrmax)-.1, yrmax+.1]
	endif else begin
		default, xrange, [min([alog(rhm),minlnr,maxlnr], max=xrmax), xrmax]
;		yrr = [min([eemin,eemax,minee,maxee], max=yrmax), 3]
;		yrr = [min([eemin,eemax,minee,maxee], max=yrmax), min([20,yrmax])]
		default,yrange,[min([0.,eemin(0)]),max([eemin,eemax,minee,maxee,eebot])]
	endelse
	if (eemin(0) LT 0.0) then sln='ln!i !n'  else sln=''
    if (NOT KEYWORD_SET(OPLOT)) then begin
        plot, lnr,ee,xr=xrange,yr=yrange,ysty=17,xtit='lnr',ytit=sln+'ee',/NODAT
    endif
	plots, lnr(i2,i2,*), ee(i2,i2,*), psym=3
	plots, alog(cpar(0,'rhobot')), eebot, psym=2, syms=.4
	oplot,alog(rhm), eemin, psym=-2, syms=.3
	oplot,alog(rhm), eemax, psym=-2, syms=.3
	if (N_ELEMENTS(iz) EQ 1) then $
		plots,lnr(*,*,iz),ee(*,*,iz),psym=2,syms=.3,color=c1
    if (N_ELEMENTS(iz) EQ 3) then begin
		plots,lnr(*,*,iz(2)),ee(*,*,iz(2)),psym=2,syms=.3,color=c1
        plots,lnr(iz(0),iz(1),iz(2)),ee(iz(0),iz(1),iz(2)),psym=2,syms=.5,col=c0
	endif
	for i=0L,nz-1L,10L do plots, lnr(*,*,i), ee(*,*,i), psym=3, color=c0
    if (eemin(0) LE 0.0  AND  KEYWORD_SET(LNE)) then begin
        eemin = exp(eemin) & eemax=exp(eemax)
    endif
	if keyword_set(STOP) then stop
;	reset_graphics
end
