;
;   Program to monitor the general behaviour of a simulation.
;
;   Update history:
;   ---------------
;   ??.??.1997 Coded/RT
;   18.05.2004 Added dotted zero-line for Flux plot/RT
;   11.10.2006 Use the new function, decade, for z-scale/RT
;   11.10.2006 Changed goto-statement to a break/RT
;   11.10.2006 Improved handling of the simulations units-conversions/RT
;   11.10.2006 Changed from explicit to implicit t-loop - factor 8 speed-up/RT
;   11.10.2006 Only perform sine-fit of Vcm if time is monotonic/RT
;   11.10.2006 New option, 's', at prompt, to stop/RT
;   12.10.2006 Also perform sine-fit of Vcm for non-monotonic time/RT
;   12.10.2006 Improved handling of Flux-scale - and same for both plots/RT
;   13.10.2006 Split decadal prefix between vertical and p-mode velocities/RT
;   08.11.2006 Fixed plotting of overshoot flux in case of radiative interior/RT
;   22.12.2006 New keyword; FORCE_INCL_SCRATCH/RT
;   07.03.2007 Improved print-out of files and include whether damping is on/RT
;   26.09.2007 Improved stability of fitting of sub-sampled Vcm(tid)/RT
;   29.10.2007 Include Teff(iz=1) when finding yrange of Teff(t) plot/RT
;   13.01.2008 Also plot \Delta T_eff on Teff-plot for overlapping time-series
;              and annotate the various curves of that plot. Unit conv. of dt/RT
;   15.01.2008 Added a few more labels to the Teff/eebot plot/RT
;   24.11.2008 Added for not changing the units for time, length and velocity,
;              by setting the keyword SIMUNITS/RT
;   28.10.2015 Fixed bug in plotting Lagrangian timeseries of pressure/RT
;   30.11.2015 Added margin to Vcm plot, as well as recommended Pdamp/RT
;
;   Known bugs: None
;
pro checkrelax, device, files=files, Ftot=Ftot, zm=zm, vcm=vcm, tid=tid, $
		hydeq=hydeq, dn=dn, period=period, ifile=ifile, $
        NOLOAD=NOLOAD, SIMUNITS=SIMUNITS, $
		FORCE_INCL_SCRATCH=FORCE_INCL_SCRATCH,STOP=STOP
@consts
	common coff,mpar,mav,ipack,incr,ioff,time,voff,xfile,irec
	common cdat,x,y,z,nx,ny,nz,nw,ntmax,date0,time0

	Dxs = !D.x_size & Dys = !D.y_size
	default, device, 'x'
	device = strlowcase(device)
	default, files, '??'
	default, dn, 1L
    rt_graphics
	np = 160
	print, stamnavn()
	fs1 = fstat(1)
	if KEYWORD_SET(NOLOAD) then f=[fs1.name] $
						   else f=simlist(files)
	if (f(0) EQ '') then stop
	nf = n_elements(f)
	ifile = intarr(nf)
	ifi = intarr(nf) & nwh0 = 0
	for j=0,4 do begin
		wh = where(strlen(f) EQ (strlen(stamnavn())+6+j))
		nwh = nwh0 + N_ELEMENTS(wh) -(wh(0) EQ -1)
		if (wh(0) NE -1) then ifi(nwh0:nwh-1) = wh
		nwh0 = nwh
	endfor
	if (nwh0 EQ nf-1) then ifi(nwh0) = nf-1
	flen = max(strlen(f(ifi)))
	Nt = 0L
	for n=0L, nf-1L do begin
		file, f(ifi(n)), /QUIET
		if (n EQ 0L) then begin
			if (stamnavn() EQ 'wd') then begin
				Fet = replicate(1.0, nz) & Fet(0) = 0.0
			endif else begin
				Fet = replicate(0.0, nz)
			endelse
;
;  The simulations system of units
			ul = cpar(0,'uul') & ut=cpar(0,'uut') & ur=cpar(0,'uur')
			uu=ul/ut & ue=uu^2 & uf=ue*ur*uu & sigma = sig/uf
			Fe = ue/1e12
			dt = max(cpart('dtsave'))
            dt0= dt
;
;  Some quantities that are presumable not going to change during the simulation
			gg = cpar(0, 'grav')
			Teff = cpar(0, 'teff')
			eebot = cpar(0, 'eebot')
			if (Teff EQ 0.0) then begin
				case stamnavn() of
					'giant': Teff = 4070.0
					'alfa' : Teff = 5770.0
					'alfab': Teff = 5325.0
					'eta'  : Teff = 6070.0
					'sun'  : Teff = 5777.0
					'wd'   : Teff = 12000.0
				endcase
			endif
			FF0 = Teff^4.*sigma
;
;  Initialize arrays
			tid   = time
			zm    = cdatat(0)
			rho   = cdatat('rhom')
			Pg    = cdatat('ppm')
			Fkin  =-cdatat('fkin')
			Frad  =-cdatat('frad'); +Fet*FF0
			Fconv =-cdatat('fconv')
			Facust=-cdatat('fpu')
			ttm   = cdatat('ttm')
			eem   = cdatat('eem')*Fe
			uzm   = cdatat('uzm')
			Ptot  = cdatat('ptm')
			Vcm   = total(cdatat('fmass'),1)/total(cdatat('rhom'),1)
			Pdamp = cpart('period')
			ebot  = cpart('eebot')
		endif else begin
            dt0= dt
			dt1= max(cpart('dtsave'))
            dt = max([dt0, dt1])
			if (n EQ nf-1) then begin
				if KEYWORD_SET(FORCE_INCL_SCRATCH) then begin
                    time = time + tid(Nt-1)
                endif else begin
				 if (tid(Nt-1) GT time(0) OR time(0) GT tid(Nt-1)+dt) then Break
                endelse
			endif
;
;  In case we have non-monotonic time, disconnect the plot-line by inserting
;  an extra point at t=1e50
			if (time(0) LT tid((Nt-1)>0)  OR  $
				time(0) GT (tid((Nt-1)>0) + 4*dt)) then begin
                if (n EQ (nf-1)  AND KEYWORD_SET(FORCE_INCL_SCRATCH)) then begin
                    print, " Didn't add a disconnect point in the time-line."
                endif else begin
                    tid   = [tid, 1e50]
                    zm    = [[zm],     [zm(*,Nt-1)]]
                    rho   = [[rho],    [rho(*,Nt-1)]]
                    Pg    = [[Pg],     [Pg(*,Nt-1)]]
                    Fkin  = [[Fkin],   [Fkin(*,Nt-1)]]
                    Frad  = [[Frad],   [Frad(*,Nt-1)]]
                    Fconv = [[Fconv],  [Fconv(*,Nt-1)]]
                    Facust= [[Facust], [Facust(*,Nt-1)]]
                    ttm   = [[ttm],    [ttm(*,Nt-1)]]
                    eem   = [[eem],    [eem(*,Nt-1)]]
                    uzm   = [[uzm],    [uzm(*,Nt-1)]]
                    Ptot  = [[Ptot],   [Ptot(*,Nt-1)]]
                    Vcm   = [Vcm,   Vcm(Nt-1)]
                    Pdamp = [Pdamp, Pdamp(Nt-1)]
                    ebot  = [ebot,  ebot(Nt-1)]
                    Nt    = Nt + 1
                endelse
			endif

			tid   = [tid, time]
			zm    = [[zm],     [cdatat(0)]]
			rho   = [[rho],    [cdatat('rhom')]]
			Pg    = [[Pg],     [cdatat('ppm')]]
			Fkin  = [[Fkin],   [-cdatat('fkin')]]
			Frad  = [[Frad],   [-cdatat('frad')]];		+Fet*FF0]]
			Fconv = [[Fconv],  [-cdatat('fconv')]]
			Facust= [[Facust], [-cdatat('fpu')]]
			ttm   = [[ttm],    [cdatat('ttm')]]
			eem   = [[eem],    [cdatat('eem')*Fe]]
			uzm   = [[uzm],    [cdatat('uzm')*Fe]]
			Ptot  = [[Ptot],   [cdatat('ptm')]]
			Vcm   = [Vcm,   total(cdatat('fmass'),1)/total(cdatat('rhom'),1)]
			Pdamp = [Pdamp, cpart('period')]
			ebot  = [ebot,  cpart('eebot')]
		endelse
		fform = '(A0,A0,I'+string(4+flen-strlen(f(ifi(n))),form='(I0)')+',A0)'
		if (cpar(ntmax-1,'period') LT 1e-6) then damping="  No p-mode damping" $
											else damping="  p-modes are damped"
		print, '    ',f(ifi(n)),ntmax,damping,form=fform
		Nt = Nt + ntmax
		ifile(n) = Nt-1L
	endfor
;
;  Add gas pressure to turbulent pressure to get total pressure
	Ptot  = Ptot + Pg
;
;  Calculate "hydrostaticity"
	hydeq = 0*zm
	for t=0L, Nt-1L do $
		hydeq(*,t) = derf(zm(*,t),alog(Ptot(*,t)))*Ptot(*,t)/rho(*,t)/gg
;
;  Find a convinient unit for velocity
	if KEYWORD_SET(SIMUNITS) then begin
		dum   = decade(ul*1e-2,ldec,/NONSC)
		dum   = decade(ut,tdec,/NONSC)
		ztitle= '!8z!x/!3[!x'+ldec+'m!3]!x'
		utitle= '!8V!dz!n!3/[!x'+ldec+'m!i !n'+tdec+'s!u-1!n!3]!x'
		vtitle= utitle
		ttitle= '!8t!x/!3[!x'+tdec+'s!3]!x'
		tid   = tid-tid(0)
	endif else begin
		Fu    = uu*1e-2/decade(max(abs(uzm))*uu*1e-2, udec)
		uzm   = Fu*uzm & utitle='!8V!dz!n!3/[!x'+udec+'m!i !ns!u-1!n!3]!x'
		Fv    = uu*1e-2/decade(max(abs(Vcm))*uu*1e-2, vdec)
		Vcm   = Fv*Vcm & vtitle='!8V!x!dCM!n!3/[!x'+vdec+'m!i !ns!u-1!n!3]!x'
		tid   = ut*(tid-tid(0))
		dt    = ut*dt
		Pdamp = ut*Pdamp
;
;  Find a convinient unit for the x-/y-/z-scales
		Fxyz  = ul*1e-2/decade(max(abs(z))*ul*1e-2, zdec, /CENTI)
		ztitle= '!8z!x/!3[!x'+zdec+'m!3]!x'
		zm    = zm*Fxyz
;
;  Find a convinient unit for time
		if (max(tid(Nt-1L)) GE 60.) then begin
			ttitle = '!8t!x/!3[!xmin.!3]!x'
			tid    = tid/60.
			dt     = dt/60.
			Pdamp  = Pdamp/60.
		endif else begin
			ttitle = '!8t!x/!3[!xhs!3]!x'
		endelse
        trange = [min(tid(where(finite(tid))), max=tmx), tmx]
	endelse
	zm    = zm-.5*(stamnavn() EQ 'wd')
	tmin = -55./400.*Nt & tmax = (255. - 55.)*Nt/400.
;
;  Repair convective flux in fiducial layer (of scratch-files?)
	ii = where(abs(reform(Fconv(0,*))) GT 10*abs(reform(Fconv(1,*))))
	if (ii(0) GE 0L) then Fconv(0,ii)=Fconv(1,ii)
;
;  Total flux and effective temperatures from the fiducial and the 1st layer
	Ftot  = Fkin + Frad + Fconv; + Facust
	Frange = [min([Ftot,Fkin,Frad,Fconv,Facust]/FF0, max=maxF)<(-.1*maxF), maxF]
	Teff0 = (reform(Ftot(0,*))/sigma)^.25
	Teff1 = (reform(Ftot(1,*))/sigma)^.25
	mTeff1= total(Teff1)/Nt
	RMSTeff1 = sqrt(total((Teff1-mTeff1)^2)/Nt)
	mTeff0= total(Teff0)/Nt
	RMSTeff0 = sqrt(total((Teff0-mTeff0)^2)/Nt)
;	Teff1 = ((Teff^4.*sigma-reform(Fkin(nz-1,*)))/sigma)^.25
;
;  Find \Delta T_eff when there are overlapping time-series
	idt =  [where(tid GT 1e30), Nt] & Ndt=0L
	if (idt(0) GT 0L) then begin
		Ndt = N_ELEMENTS(idt)
		dTeff = fltarr(idt(0), Ndt-1L)
		for i=1L, Ndt-1L do begin
			iidt = where(tid(0:idt(0)-1L) GT (tid(idt(i-1L)+1L)-1e-2*dt)  $
					AND  tid(0:idt(0)-1L) LT (tid(idt(i)   -1L)+1e-2*dt), $
					COMPLEMENT=icmpl)
			if (iidt(0) GE 0L) then begin
				dTeff(iidt,i-1L) = Teff0(iidt) - $
					intrp1(tid(idt(i-1L)+1L:idt(i)-1L),tid(iidt), $
						 Teff0(idt(i-1L)+1L:idt(i)-1L), /EXTRA, /MCUBIC)
				print,i,aver(dTeff(iidt,i-1L)), $
								form='(" dTeff(",I0,") = ",f0.2,"K")'
			endif
			if (icmpl(0) GE 0L) then dTeff(icmpl,i-1L) = 1e50
		endfor
	endif
;
;  Ensure that the last timestep is included when using step-size dn > 1
	Nt0  = (Nt-1) mod dn
	w = .2+.7*exp(-(Nt-Nt0-1.)/4.)
	if (device EQ 'x') then begin
		cs = .9
		c0 =  w*(!D.TABLE_SIZE-1) & c1=(1.-w)*(!D.TABLE_SIZE-1)
		c2 = .6*(!D.TABLE_SIZE-1)
		loadct,7
	endif else begin
		cs = .7
		c0 =    (!D.TABLE_SIZE-1) & c1=(w-1.)*(!D.TABLE_SIZE-1)
		c2 = .4*(!D.TABLE_SIZE-1)
		if (device EQ 'ps') then begin
			a4, name='checkrelax'
			loadct,0
		endif else begin
			ca4, name='c_checkrelax'
			loadct,7
		endelse
		!x.margin = [7,3]
;		!P.MULTI=[0,1,2]
	endelse
;The overall charactersize in units of the default.
	!P.charsize=cs*!P.charsize
;The xyouts charactersize in units of the overall size.
	cs = 1.2*!P.charsize
	b0 = (byte('0'))(0)
	stal='0'
	tal = (byte(stal))(0) - b0
;	tal=total((byte(stal)-b0)*10l^reverse(lindgen(N_ELEMENTS(byte(stal)))))
	zmin = min(zm, max=zmax) & dz = zmax - zmin
	ii = where(tid LT 1e30)
	if (N_ELEMENTS(ii) GT 4L) then begin
		sinfit, tid(ii), Vcm(ii), A=A, sigm=sigm, /NON
		if (sigm GT 75.) then begin
			N2 = 10L*N_ELEMENTS(ii)
			t2 = findgen(N2)/(N2-1.)*max(tid(ii))
			V2 = intrp1(tid(ii), t2, Vcm(ii))
			sinfit, t2, V2, A=A, sigm=sigm, /NON
		endif
		sinfunct, tid(ii), A, Vfit
		iampl = 0
;		if (N_ELEMENTS(A) GT 4L) then dum=max(abs(A(2+[0,3])),iampl)
		if (N_ELEMENTS(A) GT 4L) then dum=min(abs(A(0+[0,3])),iampl)
		period = A(iampl*3)
	endif
;
; Print the menu of choises
	print, ''
	print, $
'    0        1        2        3      4       5      6     7    8     9'
	print, $
' T_eff(t) T_top(z) e_top(z) e_bot(z) F_tot(z) F(z) hydeq V_cm Lagr. v_z(z)'
;
; Begin the loop of choises
	repeat begin 
case tal of
;
0: begin
	y2r = [long(min(ebot)),long(max(ebot))+1] & dy2r = y2r(1) - y2r(0)
	y2r = [y2r(0) - .025*dy2r, y2r(1) + .025*dy2r] & dy2r = 1.05*dy2r
	plot2, tid, Teff0, ebot, y1range=[min([Teff0,Teff1,Teff]), max([Teff0,Teff1,Teff])],$
					xtitle=ttitle, y1title='!8T!x!deff!n(!8F!x!d0,1!n)', $
					y2title='!7e!x!dbot!n/!3[!x10!u12!nerg!i !ng!u-1!n!3]!x', $
					y2range=y2r, y2sty=17, y1tickformat='(I0)', xr=trange
	xyouts, !x.window(1), !y.window(0) + (!y.window(1)-!y.window(0))*$
				(max(eebot)-y2r(0))/dy2r+.008, '!7e!x!dbot!n  ',align=1., /NORM
	cyr   = max(!y.crange, min=cymin) - cymin
	ymrg  = cyr*.008
	dum = max(Teff0, imax)
	xyouts, tid(imax), Teff0(imax)+ymrg, '!8T!x!deff!n(0)',align=.5
	oplot, tid, Teff1, line=2, color=c2
	dum = max(Teff1, imax)
	xyouts, tid(imax), Teff1(imax)+ymrg, '!8T!x!deff!n(1)',align=.5, color=c2
	oplot, tid(ifile), Teff0(ifile), psym=4, color=c2, th=2
	oplot, !x.crange, [1,1]*Teff, line=2, color=c1
	xyouts, !x.crange(1), Teff+ymrg, 'Nominal !8T!x!deff!n  ',align=1., col=c1
	xyouts, !x.window(0)+.01, !y.window(1)-cs*!D.y_ch_size/float(!D.y_size), $
		string(mTeff0,RMSTeff0,format='("!8T!x!deff!n=",f0.1,"!9+!x",I0,"K")'),$
														/NORMAL;, charsize=cs
	xyouts, !x.window(0)+.27, !y.window(1)-cs*!D.y_ch_size/float(!D.y_size), $
		string(mTeff1,RMSTeff1,format='(f0.1,"!9+!x",I0,"K")'),$
												col=c2, /NORMAL;, charsize=cs
	if (Ndt GT 1L) then begin
		if ((where(finite(dTeff)))(0) GE 0L) then begin
			dTeffr=[max(dTeff(where(finite(dTeff))),imax,min=mindTeff),mindTeff]
			dTeffr(1) = dTeffr(1)<0
			dTeffr(0) = dTeffr(0)>0
			ddT   = 1d1^fix(alog10(cyr))
			dToff = fix((aver(!y.crange) - aver(dTeffr))/ddT+.5)*ddT
			for i=0L,Ndt-2L do  plots, tid(0:idt(0)-1L), dTeff(*,i)+dToff,line=5
			oplot, !x.crange, [1,1]*dToff, line=1
			xyouts, tid(imax), dTeff(imax)+dToff+ymrg,'!7D!8T!x!deff!n',align=.5
			xyouts, (!x.crange(1)-!x.crange(0))*.008, dToff+ymrg, $
														'!7D!8T!x!deff!n=0'
		endif
	endif
   end
1: begin
	n0 = min(where(zm(*,0) GE dz*.04))
	plot, zm(*,0), ttm(*,0), xr=[zmin,dz*.04], /NODATA, xtitle=ztitle, $
		yr=[min(ttm(0,0:Nt-1)), max(ttm(n0,0:Nt-1))], $
		ytitle='<!8T!x>!dh!n/!3[!xK!3]!x', ytickformat='(I0)'
	for t=Nt0, Nt-1, dn do oplot, zm(*,t), ttm(*,t), color = c0+c1*t/Nt
	oplot, [zmin,zmin+dz*.5], [Teff,Teff], line=1, color=c1
	oplot, [0,0], !y.crange, line=1, color=c1
	if (device EQ 'x') then $
		color_scale, tmin, tmax, x0=.273, y0=.457, tit=ttitle, height=0.25
    print, " Teff=", mTeff0, RMSTeff0, form='(A0,f7.1,f9.4)'
   end
2: begin
	n0 = min(where(zm(*,0) GE dz*.0))
	plot, zm(*,0), eem(*,0), yr=[min(eem(0:n0,0:Nt-1)),max(eem(0:n0,0:Nt-1))], $
			/NODATA, xr=[zmin,dz*.0], xtitle=ztitle, $
			ytitle='<!7e!x>!dh!n/!3[!x10!u12!nerg!i !ng!u-1!n!3]!x'
	for t=Nt0, Nt-1, dn do oplot, zm(*,t), eem(*,t), color = c0+c1*t/Nt
	if (device EQ 'x') then $
		color_scale, tmin, tmax, x0=.273, y0=.457, tit=ttitle, height=0.25
   end
3: begin
	n0 = max(where(zm(*,0) LT zmax-.2*dz))
	plot, zm(*,0), eem(*,0), xr=[zmax-.2*dz,zmax], /NODATA, xtitle=ztitle, $
			yr=[min(eem(n0,0:Nt-1)),max(eem(nz-1,0:Nt-1))], $
			ytitle='<!7e!x>!dh!n/!3[!x10!u12!nerg!i !ng!u-1!n!3]!x'
	for t=Nt0, Nt-1, dn do oplot, zm(*,t), eem(*,t), color = c0+c1*t/Nt
	dy = !y.crange(1) - !y.crange(0) & y0 = !y.crange(0)
	zz = [zmax-.02*dz,zmax]
	oplot, zz, [1,1]*eebot
	xyouts, zz(0), (eebot-y0)*1.01+y0, '!7e!x!dbot!n', charsize=cs, /DATA
	if (device EQ 'x') then $
		color_scale, tmin, tmax, x0=.273, y0=.457, tit=ttitle, height=0.25
   end
4: begin
	plot, zm(*,0), Ftot(*,0), yr=Frange, ysty=1, /NODATA, xtitle=ztitle, $
			ytitle='!8F!x!dtot!n/!7r!8T!x!s!u 4!n!r!deff!x!n'
	for t=Nt0, Nt-1, dn do oplot, zm(*,t), Ftot(*,t)/FF0, color = c0+c1*t/Nt
	oplot, zm(*,Nt-1), Ftot(*,Nt-1)/FF0, color = c2, thick=2
	oplot, !x.crange, [1,1]*1.0, col=c1
	oplot, !x.crange, [1,1]*1.0, line=2
	oplot, !x.crange, [1,1]*0.0, line=1
	if (Nt GT 1L) then oplot, zm(*,Nt-1), aver(Ftot,2)/FF0, thick=2
	if (device EQ 'x') then begin
		Fcx = -!x.crange(0)/2./(!x.crange(1)-!x.crange(0))*$
							(!x.window(1)-!x.window(0))+!x.window(0)
		Fcy =(.05-!y.crange(0))/(!y.crange(1)-!y.crange(0))*$
							(!y.window(1)-!y.window(0))+!y.window(0)
		color_scale, tmin, tmax, x0=Fcx, y0=Fcy, tit=ttitle, height=0.25
	endif
   end
5: begin
	plot, zm(*,0), Ftot(*,0), yr=Frange, ysty=1, /NODATA, xtitle=ztitle, $
			ytitle='!8F!x!drad, kin, conv!n/!7r!8T!x!s!u 4!n!r!deff!x!n'
	for t=Nt0, Nt-1, dn do begin
		oplot, zm(*,t), Fkin(*,t)/FF0,   line=1, color = c0+c1*t/Nt
		oplot, zm(*,t), Fconv(*,t)/FF0,  line=2, color = c0+c1*t/Nt
		oplot, zm(*,t), Facust(*,t)/FF0, line=3, color = c0+c1*t/Nt
		oplot, zm(*,t), Frad(*,t)/FF0,           color = c0+c1*t/Nt
		if ((Nt-Nt0-1) GT 0L) then dum = max(aver(Fkin+Fconv, 2), iFmax) $
							  else dum = max(Fkin+Fconv, iFmax)
		i1  = max(where((Fkin(0:iFmax,t)+Fconv(0:iFmax,t)) LT 0.0))
		if (i1 GT 0L) then $
			plots,zm(0:i1,t),(Fkin(0:i1,t)+Fconv(0:i1,t))/FF0*5e1,li=2,col=c0+c1*t/Nt
	endfor
	oplot, !x.crange, [1,1]*1.0, line=1
	oplot, !x.crange, [1,1]*0.0, line=1
	if (device EQ 'x') then begin
		Fcx = -!x.crange(0)/2./(!x.crange(1)-!x.crange(0))*$
							(!x.window(1)-!x.window(0))+!x.window(0)
		Fcy =(.05-!y.crange(0))/(!y.crange(1)-!y.crange(0))*$
							(!y.window(1)-!y.window(0))+!y.window(0)
		color_scale, tmin, tmax, x0=Fcx, y0=Fcy, tit=ttitle, height=0.25
	endif
   end
6: begin
    hqrange  = [min(hydeq, max=mxhq)<.8, mxhq>1.2]
	plot, zm(*,0), hqrange, /NODATA, xtitle=ztitle, $
			ytitle='!s!ad!8p!x!r!s!bd!8z!r--!n/(g!7q!x!n)'
	oplot, !x.crange, [1,1]*1.0, line=1
	for t=Nt0, Nt-1, dn do oplot, zm(*,t), hydeq(*,t), color = c0+c1*t/Nt
	if (device EQ 'x') then $
		color_scale, tmin, tmax, x0=.455, y0=.122, tit=ttitle, height=0.25
   end
7: begin
	if (Nt GT 1L) then begin
        Prange = [0,1.04*max([period*1.3,Pdamp])]
		plot2, tid, Vcm, Pdamp, xtitle=ttitle, y1title=vtitle, xr=trange, $
;		y2title="damping "+ttitle,y2range=[0,max(Pdamp*(Pdamp LT 1.6e3))], $
			y2title="damping "+ttitle,y2range=Prange, $
            y2sty=1, xmarg=[10,10]
		oplot, tid(ifile), Vcm(ifile), psym=4, color=c2, th=2
        oplot, !x.crange, [1,1]*(period*1.3-Prange(0L))/(Prange(1L)-Prange(0))$
            *(!y.crange(1L)-!y.crange(0L))+!y.crange(0L), li=3
		if (N_ELEMENTS(t2) gt 0L) then oplot, t2, V2, line=1
		intVsc    = 0.2
		integrVcm = integf(tid, Vcm)
		integrVcm = integrVcm - aver(integrVcm)
		RMSintVcm = sqrt(total(integrVcm^2)/Nt)
		oplot, tid, integrVcm*intVsc, color=c2
		oplot, !x.crange, [1,1]*RMSintVcm*intVsc, line=2, col=c2
		oplot, !x.crange,-[1,1]*RMSintVcm*intVsc, line=2, col=c2
		oplot, !x.crange, [1,1]*0.0, line=1
		if (N_ELEMENTS(Vfit) GT 0) then oplot, tid(ii), Vfit, col=c1
	endif
   end
8: begin
	if (Nt GT 1L) then begin
		if (N_ELEMENTS(Pgl) EQ 0) then begin
			rho2 = rho
			Pg2 = Pg
			zlagr, rho2, reform(zm(*,0)), mcol, zl, /QUIET
			vlagr, reform(zm(*,0)), zl, Pg2, Pgl
		endif
		!P.multi = [0,1,2]
		!P.charsize = !P.charsize*1.7
		surface, alog10(Pg2(1:Nz/2,*)), zm(1:Nz/2,0), tid, ax=40, az=40, $
			title='normal', xtitle=ztitle, ytitle=ttitle, ztitle='log!8p!x', $
			ystyle=17,ymargin=[0,0], yr=[tid(Nt-1L), tid(0)], zsty=1, /SAVE
; Add an arrow to show the backwards time
        plots, [-.04,0,.04]*(!x.crange(1)-!x.crange(0)) + !x.crange(0), $
                [.91,1,.91]*(!y.crange(0)-!y.crange(1)) + !y.crange(1), $
                [1,1,1]*!z.crange(0), /T3D
		surface, alog10(Pgl(1:Nz/2,*)), zm(1:Nz/2,0), tid, ax=40, az=40,$
			title='Lagrangian', xtitle=ztitle, ytitle=ttitle,ztitle='log!8p!x',$
			ystyle=17,ymargin=[0,0], yr=[tid(Nt-1L), tid(0)], zsty=1, /SAVE, $
            zr=!z.crange
; Add an arrow to show the backwards time
        plots, [-.04,0,.04]*(!x.crange(1)-!x.crange(0)) + !x.crange(0), $
                [.91,1,.91]*(!y.crange(0)-!y.crange(1)) + !y.crange(1), $
                [1,1,1]*!z.crange(0), /T3D
		!P.charsize = !P.charsize/1.7
		if (device EQ 'x') then !P.multi = 0
	endif
   end
9: begin
    Ptt = (Ptot-Pg)/Ptot
    ptrange  = [0.0, max(Ptt)*3.]
	plot2, zm(*,0), uzm(*,0), Ptt(*,0), y1r=[min(uzm, max=maxuzm),maxuzm], $
        y2r=ptrange, y2crange=ptcrange, xtitle=ztitle, y1title=utitle, $
        y2titl='!8p!x!dturb!n/!8p!x!dtot!n!x', y2sty=1
    fP  = (!y.crange(1)-!y.crange(0))/(ptcrange(1)-ptcrange(0))
    cP  = !y.crange(0) - fP*ptcrange(0)
	for t=Nt0, Nt-1, dn do begin
        oplot, zm(*,t), uzm(*,t), color=c0+c1*t/Nt
        oplot, zm(*,t), Ptt(*,t)*fP+cP, color=c0+c1*t/Nt
    endfor
	oplot, !x.crange, [1,1]*0, line=2
	if (device EQ 'x') then $
		color_scale, tmin, tmax, x0=.7, y0=.55, tit=ttitle, height=0.25
   end
else: print, 'The program stops'
endcase
	stal = prompt('  Enter your choice [q/[0-9]/s]: ', /ONELINE)
	tal = (byte(stal))(0) - b0
;	tal=total((byte(stal)-b0)*10l^reverse(lindgen(N_ELEMENTS(byte(stal)))))
endrep until (tal LT 0 OR tal GT 9)
	print, ""
	if (stal EQ 's') then stop

	if (device NE 'x') then begin
		device, /close
		set_plot,'x'
	endif
	if (keyword_set(stop)) then stop

	reset_graphics
end
