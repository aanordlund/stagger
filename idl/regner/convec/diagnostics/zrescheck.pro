;
; Program to check for 2-zone socillations in the mass-flux.
;
;  Update history
;  ---------------
;  30.11.2001 Coded/RT
;  26.10.2003 Repaired a number of bugs and added comments/RT
;  09.12.2005 Can now handle files with only one timestep/RT
;  16.11.2009 Now also plots horizontal grid-spacing in plots 1) and 2) /RT
;
pro zrescheck, device, cmpdir=cmpdir, files=files, cmpfiles=cmpfiles, $
											NORMALIZE=NORMALIZE, STOP=STOP
	common coff,mpar,mav,ipack,incr,ioff,time,voff,xfile,irec
	common cdat,x,y,z,nx,ny,nz,nw,ntmax,date0,time0

	default, device, 'x'
	device = strlowcase(device)
	default, files, '??'
    rt_graphics
	Ncmpd = N_ELEMENTS(cmpdir)
	Ncmpf = N_ELEMENTS(cmpfiles)
	if (Ncmpd NE Ncmpf) then begin
		if (Ncmpf EQ 0) then begin
			cmpfiles = make_array(Ncmpd, /STRING, value='??')
			Ncmp = Ncmpd
		endif else begin
			if (Ncmpd EQ 0) then begin
				cmpdir = make_array(Ncmpf, /STRING, value='./')
				Ncmp = Ncmpf
			endif else begin
				print, Ncmpd, Ncmpf, form=$
					'("#cmpdir=",I0," is different from #cmpfiles=",I0,".")'
				print,"The program is confused. Bailing out!"
				stop
			endelse
		endelse
	endif else Ncmp=Ncmpf
	if (Ncmp GT 0) then begin
		cmpdir  = reform([cmpdir],  Ncmp)
		cmpfiles= reform([cmpfiles],Ncmp)
	endif
	Nnu = 5L
	scnu='!8C!d!7m!i'+['1','2','3','q','e']+'!n!x'
	if (device EQ 'x') then begin
		loadct, 12
		dc = .9
		c0 = (!D.TABLE_SIZE-1.)
		dc =-(!D.TABLE_SIZE-1.)*dc/(Ncmp+1)
		cs = 0.9
	endif else begin
		ca4, name='zrescheck'
		loadct, 12
		c0 =  0
		dc = 1.0
		dc = (!D.TABLE_SIZE-1.)*dc/(Ncmp)
		cs = 1.2
	endelse
    c1 = (!D.TABLE_SIZE-1.)*0.80; Red
;The overall charactersize in units of the default.
	!P.charsize=cs*!P.charsize
;The xyouts charactersize in units of the overall size.
	cs = 1.0*!P.charsize
	dsy = 0.03*cs
	sy  = .85
	sx  = 0.55
	dsx = 0.06*cs
	fsim = stamnavn()
;	print, '  sav-file     ntmax  cnu1  cnu2  cnu3  cnue  cnur xy-res/km'
	nt_tot = 0L
	for n=0L, Ncmp do begin
		if (n GT 0) then begin
;			files=cmpfiles(n-1L)
;			cd, cmpdir(n-1L), current=olddir
			files=cmpfiles(Ncmp-n)
			cd, cmpdir(Ncmp-n), current=olddir
		endif
		f=simlist(files,/NOSCRATCH)
		Nf = N_ELEMENTS(f)
		for i=0L, Nf-1 do begin
			file, f(i), /QUIET
;			print,f(i),ntmax,cpar(0,55),cpar(0,56),cpar(0,57),cpar(0,58), $
;				cpar(0,59),cpar(0,'width')/nx*1e3, form='(A15,I5,5F6.2,F7.2)'
			if (n+i EQ 0L) then begin
				nnz  = lonarr(Ncmp+1L)
				grav = fltarr(Ncmp+1L)
			endif
			if (i   EQ 0L) then begin
				nnz(n)  = nz
				grav(n) = cpar(0,'grav')
			endif
			ii = lindgen(nnz(n))
			i1 = ii(1:*)
			k = lindgen(nnz(n)-2) + 1L
			fm = cdatat('fmass')
			if KEYWORD_SET(NORMALIZE) then begin
				norm     = fltarr(nnz(0))
				norm(k)  = aver(abs(fm(k-1,*))+abs(fm(k+1,*))+2*abs(fm(k,*))/4., 2)
				norm(0)  = norm(1)&  mk=max(k)& norm(mk:*) = norm(mk)
			endif else norm = 1.0; (cdatat('rhom'))(k,*)^.5
			if (0) then begin
				ampl     = fltarr(nnz(0))
				ampl(ii) = abs(.5*fm(k-1,*)+.5*fm(k+1,*)-fm(k,*))/norm
			endif else begin
				ampl = fltarr(nnz(0),ntmax)
				i2 = lindgen(nnz(n)/2L)*2L
				for it=0,ntmax-1L do begin
					ampl(i2+1L,it)=abs(intrp1(z(i2),z(i2+1L),fm(i2,it), $
											/LI,/EX) - fm(i2+1L,it))/norm(i2+1L)
					ampl(i2,it)=abs(intrp1(z(i2+1L),z(i2),fm(i2+1L,it), $
											/LI,/EX) - fm(i2,it))/norm(i2)
				endfor
			endelse
			if (i EQ 0) then begin
				if (n EQ 0) then begin
; Scalars as function of group
					simname = strarr(Ncmp+1L)
					n1      = lonarr(Ncmp+1L)
					maxa2   = fltarr(Ncmp+1L)
					dxy     = fltarr(Ncmp+1L)
; z-arrays as function of group
					dz = fltarr(N_ELEMENTS(i1), Ncmp+1L)
					z2 = fltarr(N_ELEMENTS(i1), Ncmp+1L)
					zz = fltarr(N_ELEMENTS(ii), Ncmp+1L)
				endif
; Scalars as function of group
				simname(n) = stamnavn()+files+":"
				n1(n)      = nt_tot
				dxy(n)     = cpar(0,'dx')
; z-arrays as function of group
				dz(i1,n) = z(i1)-z(ii)
				z2(i1,n) = z(i1-1)+.5*dz(n,i1)
				if (nnz(0)-1 GT max(i1)) then z2(max(i1)+1:*,n) = z2(max(i1),n)
				zz(ii,n) = z(ii)
			endif
			if (i EQ 0  AND  n EQ 0) then begin
; Scalars as function of time
				t  = time*1e2/6e1;	time in minuttes
				fcnu=transpose([[cpart('cnu1')],[cpart('cnu2')], $
					[cpart('cnu3')],[cpart('cnue')],[cpart('cnur')]])
; z-arrays as function of time
				a2 = ampl
				Hp = (cdatat('ppm')+cdatat('ptm'))/cdatat('rhom')/grav(n)
				Hp2= intrp1(z(ii), z2(i1-1,n), Hp(ii, 0L))
				for it=1L, ntmax-1L do $
					Hp2 = [[Hp2], [intrp1(z(ii), z2(i1-1,n), Hp(ii,it))]]
			endif else begin
; Scalars as function of time
				t  =[t, time*1e2/6e1]
				fcnu=[[fcnu], [transpose([[cpart('cnu1')],[cpart('cnu2')], $
					[cpart('cnu3')],[cpart('cnue')],[cpart('cnur')]])]]
; z-arrays as function of time
				a2 =[[a2],[ampl]]
				Hp1= 0*ampl
				Hp1(ii,*) = (cdatat('ppm')+cdatat('ptm'))/cdatat('rhom')/grav(n)
				Hp =[[Hp],[Hp1]]
				for it=0L, ntmax-1L do $
					Hp2 = [[Hp2], [intrp1(z(ii), z2(*,n), Hp(ii,nt_tot+it))]]
;					Hp2 = [[Hp2], [intrp1(z(ii), z2(i1-1,n), Hp(ii,nt_tot+it))]]
			endelse
			nt_tot = nt_tot + ntmax
; End of sav-file-loop
		endfor
		if (n GT 0) then begin
			cd, olddir
			n2 = [n2, nt_tot-1L]
		endif else begin
			n2 = [nt_tot-1L]
		endelse
		if (n1(n) EQ n2(n)) then begin
			maxa2(n) = max(a2(*,n1(n)))
		endif else begin
			maxa2(n) = max(aver(a2(*,n1(n):n2(n)), 2))
		endelse
; End of group-loop
	endfor
	zunit = '!8z!3/[!xMm!3]!x'
	zzr = [min(zz, max=maxzz), maxzz]
	fmasstitle = $
		'mplitude of 2-zone osc. in !8F!D!xmass!n!x'
;		'mplitude of 2-zone osc. in !8F!D!xmass!n/!9S!s!7q!x!r!u!U!3_____!x'
	if KEYWORD_SET(NORMALIZE) then fmasstitle='Relative a'+fmasstitle $
							  else fmasstitle=         'A'+fmasstitle
	b0=(byte('0'))(0)
	stal='3'
	tal=total((byte(stal)-b0)*10l^reverse(lindgen(N_ELEMENTS(byte(stal)))))
	Nchoise = 3
	repeat begin
		case tal of
		1: begin
			plot, zzr, [0, max(dz(1:min(nnz)-2,*))], /NODATA, xtit=zunit,$
				ytit='!7D!8z!3/[!xMm!3]!x'
			for n=0, Ncmp do begin
; Don't plot the fiducial layer.
				i1 = lindgen(nnz(n)-2) + 1
				oplot, z2(i1,n), dz(i1,n), col=c0+dc*n
				oplot, !x.crange, [1,1]*dxy(n), col=c0+dc*n, line=2
			endfor
			xyouts, !x.crange(0)+.8*(!x.crange(1)-!x.crange(0)), $
				dxy(Ncmp)+(!y.crange(1)-!y.crange(0))*.01, '!7D!8x, !7D!8y!x',/DATA
		   end
		2: begin
			dum = dz(1:*,*)/Hp2(1:*,n1)
			plot, zzr, [min(alog10(dum(where(dum GT 0.0))),max=ymax), ymax], $
				/NODATA, xtit=zunit, ytit='log!D10 !n!7D!8z!3/!8H!Dp!x!n'
			iprt = long(.8*(nnz-3))
			yprt = fltarr(Ncmp+1L)
			for n=0, Ncmp do begin
; Don't plot the fiducial layer.
				i1 = lindgen(nnz(n)-2) + 1
				if (n1(n) EQ n2(n)) then begin
					aHp2 =      Hp2(i1,n1(n))
				endif else begin
					aHp2 = aver(Hp2(i1,n1(n):n2(n)),2)
				endelse
				oplot, z2(i1,n), alog10(dz(i1,n)/aHp2), col=c0+dc*n
				oplot, z2(i1,n), alog10(dxy(n)/aHp2), col=c0+dc*n, line=2
				yprt(n)  = alog10(dxy(n)/aHp2(iprt(n)))
			endfor
			yprtmx = max(yprt, imax)
			xyouts, z2(iprt(imax)+1L, imax), $
				yprtmx+(!y.crange(1)-!y.crange(0))*.01, '!7D!8x, !7D!8y!x',/DATA
		   end
		3: begin
			plot, zzr, [0.0, max(maxa2)], /NODATA, xtit=zunit, $
				ytit=fmasstitle;, yr=[0,1]
			for n=0, Ncmp do begin
				ii = lindgen(nnz(n))
				if (n1(n) EQ n2(n)) then begin
					aa2 =      a2(ii,n1(n))
				endif else begin
					aa2 = aver(a2(ii,n1(n):n2(n)),2)
				endelse
				oplot, zz(ii,n), aa2, col=c0+dc*n
			endfor
		   end
		else: print, 'The program stops'
		endcase
		for j=0, Nnu-1 do begin
;			xyouts, sx+(j-.4)*dsx, sy+dsy, '!8C!x', /NORM, align=1, size=cs
			xyouts, sx+(j-.0)*dsx, sy+dsy, scnu(j), /NORM, align=1, size=cs*1.8
		endfor
        fcnulst = fcnu(*,n2(0))
        fcnunom = [1.5, 3.0, 0.3, 1.0, 1.0]
        l       = 0L
        if (total(abs(fcnulst-fcnunom)) GT 1e-3) then begin
            print,"WARNING: Not the nominal values for cnu."
            print,"         Please consider to change them - possible in steps."
            xyouts, sx-4*dsx,sy-dsy*l,'*** Change cnu to ***: ',/NORM,col=c0,siz=cs
			for j=0, Nnu-1L do $
				xyouts, sx+j*dsx, sy-dsy*l, string(fcnunom(j),form='(F4.2)'), $
							/NORM,align=1,col=c0,siz=cs
            l=l+1L
        endif
		for n=0, Ncmp do begin
			xyouts,sx-4*dsx,sy-dsy*l,simname(n),/NORM,col=c0+dc*n,siz=cs
			for j=0, Nnu-1L do $
				xyouts, sx+j*dsx, sy-dsy*l, $
							string(aver(fcnu(j,n1(n):n2(n))),form='(F4.2)'), $
							/NORM,align=1,col=c0+dc*n,siz=cs
            l=l+1L
		endfor
		print, ''
		print, '    1         2         3'
		print, '   dz       dz/Hp    2-zone'
		stal = prompt('  Enter your choice: ')
		tal = (byte(stal))(0)-b0
	endrep until (tal LT 1 OR tal GT Nchoise)
	if (device NE 'x') then begin
		device, /close
		set_plot,'x'
	endif
	if (KEYWORD_SET(STOP) OR stal EQ 's') then stop
	reset_graphics
end
