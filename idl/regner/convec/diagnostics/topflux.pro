pro topflux, oplot=oplot, STOP=STOP
	common coff,mpar,mav,ipack,incr,ioff,time,voff,xfile,irec
	common cdat,x,y,z,nx,ny,nz,nw,ntmax,date0,time0

	Teff = cpar(0, 'teff')
	height = cpar(0, 'height')
	ur = cpar(0, 'uur')
	ul = cpar(0, 'uul')
	ut = cpar(0, 'uut')
	uu=ul/ut & ue=uu^2 & uf=ue*ur*uu
	Ftot0 = Teff^4.*5.67051e-5/uf
	print, Teff, Ftot0, format='("  Teff=",F8.2," => Ftot=",F7.4)'
	frad = fltarr(ntmax, nz)
	zmax = height*.1
	zmin = z(0)
	ii = where(z GE zmin  AND  z LE zmax)
	nii = N_ELEMENTS(ii)*ntmax
	for i=0, ntmax-1 do $
		frad(i,*) = (-cdata(i,'frad')-cdata(i,'fkin')-cdata(i,'fconv'))/Ftot0
	frange = [min([1,reform(frad(*,ii),nii)]), max([1,reform(frad(*,ii),nii)])]
	if (NOT KEYWORD_SET(OPLOT)) then begin
		plot, z, frad(0,*), xr=[zmin, zmax], yr=frange, psym=-1, ysty=2
		oplot, z, replicate(1.d0, nz), line=1
	endif
	c0 =  0.2*!D.TABLE_SIZE & c1 = (1.- 0.2)*!D.TABLE_SIZE
	for i=0,ntmax-1 do $
		oplot, z, frad(i,*), psym=-1,color=c0+c1*i/(ntmax-1.0)
	devi = 100*abs(frad(*,0)-frad(*,1))/total(frad(*,1))*ntmax
	stat, devi
	devi = 100*max(abs(frad(*,0)-frad(*,1)))/total(frad(*,1))*ntmax
	devimin = 100*min(abs(frad(*,0)-frad(*,1)))/total(frad(*,1))*ntmax
	print, devimin, devi, format= $
	'(" Fluxpoint 0 and 1 deviates from each other by ",F7.4,"-",F7.4,"%")'
	if KEYWORD_SET(STOP) then stop
end
