;
;   Program to plot the entire cdata-block for one or two simulation files.
;   If comparing to sav-files, the z-range is that of the first.
;
;   Update history:
;   ---------------
;   31.01.2001 Coded/RT
;   13.11.2002 Renamed from maverplot to cdataplot/RT
;   14.01.2006 User choise of values for Pmulti/RT
;   18.01.2006 Include yrange and extra_args in argument list/RT
;   10.10.2007 Added parameter, dz, for offset on z-scale of sav2file/RT
;
;   Known bugs: None
;
pro cdataplot, sav1file, sav2file=sav2file, psymbol=psymbol, it=it, dz=dz, $
				ivar0=ivar0, xrange=xrange, yrange=yrange, Pmulti=Pmulti, $
				_EXTRA=extra_args, LOG=LOG, STOP=STOP
	common coff,mpar,mav,ipack,incr,ioff,time,voff,cfile,irec
	common cdat,x,y,z,nx,ny,nz,nw,ntmax,date0,time0
	common cdata_names, varname

	default, it, 0L
	default, dz, 0.0
	default, ivar0, 0
	ff = findfile('*.sav')
	default, sav1file, ff(N_ELEMENTS(ff)-1)
	default, sav2file, sav1file
	default, psymbol, 0L
	default, Pmulti, [2, 4]
    rt_graphics
	dc = 0.6
	ilog = lonarr(500)
	if KEYWORD_SET(LOG) then begin
		iilog       = [2,5,7,8,11,13,14,15,21]
		ilog(iilog) = 1L
	endif
	if (strlowcase(!D.name) NE 'x') then dc=0.4
	c0 = (!D.TABLE_SIZE-1.)*dc
	if (sav2file NE sav1file) then begin
		file, sav2file, /QUIET
		it2 = (it>0L)<(ntmax-1L)
		mean2 = fltarr(nz, mav)
		for i=0, mav-1 do begin
			if (ilog(i)) then mean2(*, i) = alog10(cdata(it2, i)) $
						 else mean2(*, i) =        cdata(it2, i)
		endfor
	endif
	file, sav1file, /QUIET
	z = cdata(0, 'z')
	default, xrange, [z(0), z(nz-1L)]
	i1  = where(z GE xrange(0)  AND  z LE xrange(1))
	if (i1(0)   GT 0L)      then i1=[i1(0)-1L, i1]   & Ni1 = N_ELEMENTS(i1)-1L
	if (i1(Ni1) LT (nz-1L)) then i1=[i1, i1(Ni1)+1L] & Ni1 = N_ELEMENTS(i1)
	it1 = (it>0L)<(ntmax-1L)
	mean1 = fltarr(Ni1, mav)
	for i=0, mav-1 do begin
		if (ilog(i)) then mean1(*, i) = alog10((cdata(it1, i))(i1)) $
					 else mean1(*, i) =        (cdata(it1, i))(i1)
	endfor
	vname           = varname
	if KEYWORD_SET(LOG) then vname(iilog)= 'Log('+varname(iilog)+')'
	print, it1, sav1file,form='("Comparing cdata from timestep",I3," of ",A0,$)'
	if (sav2file NE sav1file) then begin
		print,sav2file,form='(" and ",A0,$)'
		i2 = where(mean2(*,0) GE xrange(0)  AND  mean2(*,0) LE xrange(1))
		if (i2(0)   GT 0L)      then i2=[i2(0)-1L, i2]   & Ni2=N_ELEMENTS(i2)-1L
		if (i2(Ni2) LT (nz-1L)) then i2=[i2, i2(Ni2)+1L]
		mean2 = mean2(i2,*)
	endif
	print,'.'

	stra = ''
	!P.MULTI = [0,Pmulti(0), Pmulti(1)]
	Nplots = !P.MULTI(1)*!P.MULTI(2)
	cs = 1.2
;The overall charactersize in units of the default.
	!P.charsize=cs*!P.charsize
;The xyouts charactersize in units of the overall size.
	cs = 0.70*!P.charsize
	!y.margin = [ 3,1]
	!x.margin = [11,1]
	for ivar=ivar0,mav-4L do begin
		if (((ivar-ivar0) mod Nplots) EQ 0 AND ivar GT ivar0) then begin
			stra = prompt('Continue? <return> : ')
			if (stra EQ 's') then stop
			if (stra EQ 'q' OR stra EQ 'n' OR stra EQ 'r') then begin
				reset_graphics
				return
			endif
		endif
		if (N_ELEMENTS(mean2) GT 0) then begin
			yrvar = [min([mean1(*,ivar), mean2(*,ivar)],max=ymax),ymax]
			if (ivar EQ ivar0) then default, yrange, yrvar   else  yrange=yrvar
		endif else begin
			yrvar = [min(mean1(*,ivar), max=ymax),ymax]
			if (ivar EQ ivar0) then default, yrange, yrvar   else  yrange=yrvar
		endelse
		plot, mean1(*,0), mean1(*,ivar), psym=psymbol,syms=.1, yr=yrange, $
			xtit=vname(0), ytit=string(ivar,form='(I3,": ")')+vname(ivar), $
			xrange=xrange, _EXTRA=extra_args
		if (NOT ilog(ivar) AND ivar GT 1) then oplot, !x.crange, [0,0], line=1
		if (sav2file NE sav1file) then $
			oplot, mean2(*,0)+dz, mean2(*,ivar), col=c0
	endfor
	if KEYWORD_SET(STOP) then stop
	reset_graphics
end
