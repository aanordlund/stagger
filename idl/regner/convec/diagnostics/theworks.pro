;
;  Program to plot all kinds of useful information for
;  monitoring the progress of a simulation.
;
;  Update history
;  ---------------
;  16.10.2003 Coded/RT
;  09.02.2004 Changed print-out during "Images"/RT
;  09.12.2005 Plotting od dT/dz can now handle ntmax=1/RT
;  09.12.2005 All involved programs can now handle files
;             with only one timestep/RT
;  21.03.2006 Added T-tau plot/RT
;  06.12.2006 Added Tmin=1600 K line to T-tau plot/RT
;  30.01.2007 Added print-out of suggestions in case max(Ltau(1)) <> -4.5/RT
;  20.03.2007 Re-ordered some of the calls and added more exit options/RT
;  21.03.2007 Slimmed down ftau call to only involve the top of the sim/RT
;  20.06.2007 Communication of the PID of a possible table calculation
;             from call to meanrhot_gen/RT
;  18.09.2007 For meanrhot_gen call, use Mouse to choose averaging region,
;             and add the cmd argument to the call/RT
;  03.06.2009 Added /TAU and files=files to argument of meanrhot_gen, and
;             updated cmd to reflect the actual command used/RT
;  22.03.2010 Assume sims without kaprhoT tables are old-style, and therefore
;             should be meanrhot_gen averaged on the z-scale/RT
;  09.09.2015 Inplotting Vcm(tid) also show boundaries between sim files./RT
;  22.02.2016 Improved handling of asking for meanrhot_gen time./RT
;
pro theworks, files=files, Nf, pid=pid, STOP=STOP
	common cdat,x,y,z,nx,ny,nz,nw,ntmax,date0,time0

	default, files, "??"
	sims   = simlist(files, /NOSCRATCH)
	Nsims  = N_ELEMENTS(sims)
	default, Nf, Nsims
	Nf = Nf < Nsims
	simlen = strlen(sims(0))
	sims   = sims(Nsims-Nf:Nsims-1L)
	indx   = strmid(sims,simlen-6,2)
;	if (Nf GT 1L) then files=indx(0)+'-'+indx(Nf-1L) else files=indx(0)
;	print, 'Proccessing the files:', sims, form='(("    ",A0))'
	c1 = (!D.TABLE_SIZE-1.)*.7
	pid = -1L

	print, "  Checkrelax...", form='(/,A0)'
	checkrelax, files=files, Vcm=Vcm, tid=tid, period=period, ifile=ifile
;
	if (cpar(ntmax-1L,'spare4') GT 1e-6) then begin
		print, " The last file of this simulation is still damped."
		answ=prompt(" Do you want to check the meanrhoT-structure? [y/n/q/s]: ")
		if (answ NE 'n') then begin
			if (answ EQ 'q'  OR  answ EQ 'r') then return
			if (answ EQ 's')                  then stop
            rt_graphics
            tau_flag=['', ', /TAU']
			plot, tid, Vcm, psym=-2, syms=.4
			oplot, !x.crange, [1,1]*0, line=1
            Npmax = long(max(tid)/period)
			for i=1L, Npmax-1L do begin
				oplot, [1,1]*(max(tid)-i*period), !y.crange, line=2
				xyouts, max(tid)-i*period, $
					!y.crange(0)+.9*(!y.crange(1)-!y.crange(0)), $
					string(i,form='(I0)')
			endfor
            for i=0,Nf-1 do begin
                oplot, [1,1]*tid(ifile(i)), !y.crange, line=1
                if (i EQ 0) then sf=strmid(sims(i),0,simlen-4)  $
                            else sf=strmid(sims(i),simlen-6,2)
                tx   = .5*(tid(ifile(i)) + (i GT 0)*tid(ifile((i-1)>0)))
                xyouts, tx, !y.crange(1)*.9, sf, align=0.5, chars=0.8, /DATA
            endfor
            if (Npmax GE 1L) then begin
                answ = prompt(" How many periods of the fundamental mode? [1-" $
                     + string(Npmax, form='(I0)')+ "]: ")
            endif else begin
                answ = '0'
            endelse
			if (answ GE '1'  AND  answ LE '9') then begin
				dtime = float(answ)*period
			endif else begin
				print, " Click mouse on plot to select 1st timestep to include."
				cursor, cx, cy, /DATA
				dtime = !x.crange(1)-cx
			endelse
			oplot, [1,1]*(max(tid)-dtime), !y.crange, line=2, col=c1
            if (file_test('kaprhoT.tab')) then  TAU=1  else  TAU=0
            mrt_cmd="meanrhot_gen, " + string(dtime, form='(f0.2)') + $
                tau_flag(TAU) + ", files='" + files+"'"
			print, "  "+mrt_cmd
            meanrhot_gen, dtime, TAU=TAU, files=files, pid=pid, cmd=mrt_cmd
		endif
	endif
	print, "  T-tau-plot..."
	Ntau = N_ELEMENTS(where(z LT z(1)/2)) > 8L
	tt   = (ftt(ntmax-1))(*,*,0:Ntau-1L)
	Ltau = alog10(ftau((flnrho(ntmax-1))(*,*,0:Ntau-1L),tt,zm=z(0:Ntau-1L)))
;
; The maximum log(tau) to allow at the uppermost point.
	Ltaumax1 = -4.5
	Ltaumax  = fltarr(Ntau)
	for i=0L, Ntau-1L do Ltaumax(i)=max(Ltau(*,*,i))
	if (Ltaumax(1) GT Ltaumax1) then begin
		ii = where(Ltaumax LT 0.0)
		if (ii(0) LT 0L) then ii = lindgen(5)
		x1 = intrp1(smoothfx(z(ii),Ltaumax(ii)), Ltaumax1, z(ii), /EXTRA)
		print, Ltaumax(1), Ltaumax1, form=$
			'("WARNING: maxLtau(1) = ",f6.2," > ",f6.2," is too large.")'
		print, x1/z(1), form='("         Can be fixed with:  extend,", f7.4)'
	endif
	if (Ltaumax(2) LT Ltaumax1) then begin
		print, Ltaumax(2), Ltaumax1, form=$
			'("  maxLtau(2) = ",f6.2," < ",f6.2," is plenty small.")'
		print, "  There is room for truncating the simulation, e.g.,"
		print, max(where(Ltaumax LT Ltaumax1)), nz-1L, form=$
			'("  IDL>  shrink, ",I0,", ",I0)'
	endif
    rt_graphics
    Ltrange = [min(Ltau(*,*,0:3), max=mxLt), max([mxLt, Ltaumax1+.07])]
	plot, Ltau(*,*,0:3),tt(*,*,0:3), ytickform='(I0)', xr=LTrange, $
		psym=3, xtitl='log!d10!n!7s!x!dRoss!n', ytitl='!8T!3/[!xK!3]!x'
	plots, Ltau(*,*,1), tt(*,*,1), psym=2, syms=.2, col=c1
	plots, Ltau(*,*,0), tt(*,*,0), psym=2, syms=.2
	oplot, [1,1]*(Ltaumax1), !y.crange, line=2
	oplot, !x.crange, [1,1]*1600, line=2
	print, " Max(Ltau(0,1)) = ", max(Ltau(*,*,0)), max(Ltau(*,*,1))
	answ = prompt("  Checkrad? [y/n/q/s]: ")
	if (answ NE 'n') then begin
		if (answ EQ 'q'  OR  answ EQ 'r') then return
		if (answ EQ 's')                  then stop
		print, "  Checkrad..."
		checkrad,   files=files, /SCRATCH
	endif else begin
		file, sims(N_ELEMENTS(sims)-1L), /QUIET
	endelse
    rt_graphics
    if (ntmax GT 1) then begin
        plot, cdata(0,'z'), zder1(aver(cdatat('ttm'),2)), xr=[1,-1]*z(0), $
                xtitl='z/!3[!xMm!3]!x', ytitl='d!8T!3/!xd!8z!x'
    endif else begin
        plot, cdata(0,'z'), zder1(cdata(ntmax-1L,'ttm')), xr=[1,-1]*z(0), $
                xtitl='z/!3[!xMm!3]!x', ytitl='d!8T!3/!xd!8z!x'
    endelse
	label,.15,.8,0,sims(Nf-1)
	if (NF GT 1L) then begin
		c0 = (!D.TABLE_SIZE-1.)*.4
		dc = (!D.TABLE_SIZE-1.)*.6/(Nf-1.)
		for i=0L, Nf-2 do begin
			file, sims(i), /QUIET
			if (ntmax GT 1) then begin
				oplot, z, zder1(aver(cdatat('ttm'),2)), col=c0+i*dc
			endif else begin
				oplot, z, zder1(cdata(ntmax-1,'ttm')),  col=c0+i*dc
			endelse
			label,.15,.8-(Nf-1-i)*.05,0,sims(i),col=c0+i*dc
		endfor
	endif
	answ = prompt("  Bottoms? [y/n/q/s]: ")
	if (answ NE 'n') then begin
		if (answ EQ 'q'  OR  answ EQ 'r') then begin& reset_graphics& return&end
		if (answ EQ 's')                  then begin& reset_graphics& stop  &end
		print, "  Bottoms..."
		bottoms,    files=files, /SCRATCH
	endif

	answ = prompt("  dtplot? [y/n/q/s]: ")
	if (answ NE 'n') then begin
		if (answ EQ 'q'  OR  answ EQ 'r') then return
		if (answ EQ 's')                  then stop
		dtplot,     files=files
	endif

	answ = prompt("  Zrescheck? [y/n/q/s]: ")
	if (answ NE 'n') then begin
		if (answ EQ 'q'  OR  answ EQ 'r') then return
		if (answ EQ 's')                  then stop
		if (Nf GT 1L) then begin
			zrescheck,  files=indx(Nf-1L), cmpf=indx(0:Nf-2L)
		endif else begin
			zrescheck,  files=indx(0L)
		endelse
	endif

	answ = prompt("  Smovie? [y/n/q/s]: ")
	if (answ EQ 'q'  OR  answ EQ 'r') then return
	if (answ EQ 's')                  then stop
	while (answ EQ 'y') do begin
		smovie, files=files, dt=0.1;, sc=4
		answ = prompt(" Again [y]? ")
	endwhile
	Nimgs = (!D.X_SIZE/(nx+1))*(!D.Y_SIZE/(ny+1))
	iz = long(findgen(Nimgs)*(nz-1.)/(Nimgs-1.))
	iv = 0L
	vars = ['lnrho','ux','uy','uz','ee','tt']
	answ = prompt("  Images? [y/n/q/s]: ")
	if (answ EQ 'q'  OR  answ EQ 'r') then return
	if (answ EQ 's')                  then stop
	if (answ EQ 'y') then begin
		erase& loadct, 3
		imsz, max([nx, ny])
	endif
	while (answ EQ 'y' OR answ EQ '+' OR answ EQ '-') do begin
		print, vars(iv), form='(A5,$)'
		for it=0L, ntmax-1L do begin
			images, (var(it,iv))(*,*,iz), /GRID
			wait, .3
		endfor
		answ = prompt(":Again [n/y/+/-]? ")
		if (answ EQ '+') then iv=(iv+7) mod 6
		if (answ EQ '-') then iv=(iv+5) mod 6
	endwhile
	loadct, 7
	answ = prompt("  cdataplot? [y/n/q/s]: ")
	if (answ EQ 'q'  OR  answ EQ 'r') then return
	if (answ EQ 's')                  then stop
	while (answ EQ 'y') do begin
		file, sims(Nf-1L)
		dit = (ntmax-1)/5L>1
		it0 = (ntmax-1L-5L*dit)>0
		for i=it0, ntmax-1L, dit do begin
			if (Nf GT 1) then cdataplot,sims(Nf-1L),sav=sims(0),/log,it=i,iv=16$
						 else cdataplot,sims(Nf-1L),            /log,it=i,iv=16
			if (i LT (ntmax-1L)) then s=prompt(" Next timestep? ")
		endfor
		answ = prompt(" Repeat [y]? ")
	endwhile

    if KEYWORD_SET(STOP) then stop
end
