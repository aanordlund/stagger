;
;   Program to display the ee and lnr extend of the simulation with
;   respect to the *.tmp-table boundaries.
;
;   Update history:
;   ---------------
;   ??.??.1998 Coded/RT
;   24.06.2004 Include choise of time-step of sav-/scr-file to display/RT
;   12.10.2004 Use plots,lnr(iz),ee(iz)  instead of oplot./RT
;   20.06.2007 Plotting of the simulation is by-passed when OPLOT is set and
;              the name of the tmpfile is output to the plot/RT
;   27.11.2008 Can now also handle kaprhoT-tables, added keyword SIMRANGE/RT
;
;   Known bugs: None
;
pro tmpshot, it, iz=iz, scrfile=scrfile, tmpfile=tmpfile, mz=mz, $
		xrange=xrange, yrange=yrange, OPLOT=OPLOT, SIMRANGE=SIMRANGE, STOP=STOP
	common cdat,x,y,z,nx,ny,nz,nw,ntmax,date0,time0
	common ctabl1,rhm,drhm,thmin,thmax,dth,mtab,itab,tab
	common cthepast, oplots

	if KEYWORD_SET(OPLOT) then oplots=oplots+1L else oplots=0L
	c0 = (!D.TABLE_SIZE-1.)*.6
	c1 = (!D.TABLE_SIZE-1.)*.8
	wc = (300.0 - .38*oplots) mod 1.00001
	if (!D.name NE 'X')   then wc = 1. - wc
	c2 = (!D.TABLE_SIZE-1.)*wc
; Find and open the scratch- (or sav-) file.
	if (N_ELEMENTS(scrfile) GT 0) then begin
		if (strpos(scrfile, '.scr') LT 0  AND $
			strpos(scrfile, '.sav') LT 0) then scrfile=scrfile+'.scr'
		print, scrfile
		file, scrfile, /QUIET
	endif else begin
		scrfile = stamnavn()+'.scr'
    	fst = fstat(1)
		if (strpos(fst.name,'.scr') LT 0) then begin
			print, scrfile
			file, scrfile, /QUIET
		endif
	endelse
; Default timestep is the last one
	default, it, ntmax-1L
	it = (it > 0) < (ntmax-1L)
	default, mz, nz
; Find and open the tmp- (or tab-) file.
	if (N_ELEMENTS(tmpfile) GT 0) then begin
    	if (strpos(tmpfile, '.tmp') LT 0  AND $
			strpos(tmpfile, '.tab') LT 0) then tmpfile=tmpfile+'.tmp'
		tabtmp, tmpfile, mz=mz
	endif else begin
		if (file_test('kaprhoT.tab')) then begin
			radtab& tmpfile='kaprhoT.tab'
		endif else begin
			tmpfile = stamnavn()+'.tmp'
			fst = fstat(2)
			if (strpos(fst.name,'.tmp') LT 0) then tabtmp, tmpfile, mz=mz
		endelse
	endelse
	lrhm = alog(rhm)
	if KEYWORD_SET(OPLOT) then begin
		oplot, lrhm, 5040./thmin, col=c2
		oplot, lrhm, 5040./thmax, col=c2
		label, .2, .90-.04*oplots, 0, underscore(tmpfile), color=c2
	endif else begin
		tt = ftt(it)
        if (tt(0,0,Nz-1L) LT 1e3) then begin
            tt = exp(tt)
            print, "It seems that '+fst.name+' stores lnT. Exponentiated it."
        endif
		i2 = indgen(30)*nx/30
		lnr = flnrho(it)
		if KEYWORD_SET(SIMRANGE) then begin
			lrrange=[min(flnrho(0),max=lrmx2)-.3, lrmx2+.3]
			default, yrange, [min(tt, max=maxtt)-200, maxtt+200]
		endif else begin
			lrrange=[min([min(lrhm,max=lrmx1),min(flnrho(0),max=lrmx2)]), $
															max([lrmx1,lrmx2])]
			default, yrange, [0, max(5040./thmin)]
			if (tmpfile EQ 'kaprhoT.tab') then  print, $
			'Plotting universal table; the default axis-box outlines the table.'
		endelse
		if (lrrange(0) LT lrhm(0)  OR  lrrange(1) GT lrhm(nz-1)) then begin
			print, 'sim: ', lrrange
			print, 'tab: ', lrhm([0,nz-1])
		endif
		default, xrange, lrrange
		plot,  lrhm, 5040./thmin, ysty=17, psym=-1,syms=.4, $
			xtit='lnr',ytit='tt',ytickform='(I0)', xr=xrange, yr=yrange
		oplot, lrhm, 5040./thmax, psym=-1, syms=.4
		oplot, !x.crange, [1,1]*1600., line=1
		label, .2, .90-.04*oplots, 0, underscore(tmpfile)
;		oplot, lnr(i2,i2,*),tt(i2,i2,*),psym=3
;		for i=0,nz-1,5 do oplot, lnr(*,*,i), tt(*,*,i), psym=3, color=c0
		plots, lnr(i2,i2,*),tt(i2,i2,*),psym=3
		if (N_ELEMENTS(iz) EQ 1) then $
			plots,lnr(*,*,iz),tt(*,*,iz),psym=2,syms=.3,color=c1
		if (N_ELEMENTS(iz) EQ 3) then begin
			plots,lnr(*,*,iz(2)),tt(*,*,iz(2)),psym=2,syms=.3,color=c1
			plots,lnr(iz(0),iz(1),iz(2)),tt(iz(0),iz(1),iz(2)),psym=2,syms=.5,$
																		col=c0
		endif
		for i=0,nz-1,5 do plots, lnr(*,*,i), tt(*,*,i), psym=3, color=c0
	endelse
	if keyword_set(STOP) then stop
end
