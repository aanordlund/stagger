;
;   Program to make diagnostic plots of quantities related to the
;   radiative transfer in the simulations.
;
;   Update history:
;   ---------------
;   06.04.2004 Coded/RT
;   11.10.2006 Use the new function, decade, for z-scale/RT
;   11.10.2006 Improved scaling of Frad in 1st plot and added separate axis/RT
;   21.03.2007 Much more compact prompting/RT
;   04.03.2014 Fixed so it also works with just one snapshot in a save-file/RT
;   11.02.2016 Added Teff-axis to flux plot (2) and now interpolate flux pics
;              to equidistant z-scale and use same colr-scale for all fluxes/RT
;
;   Known bugs: None
;
pro checkrad, device,files=files,dn=dn,STOP=STOP,NOLOAD=NOLOAD,SCRATCH=SCRATCH
@consts
	common coff,mpar,mav,ipack,incr,ioff,time,voff,xfile,irec
	common cdat,x,y,z,nx,ny,nz,nw,ntmax,date0,time0

	default, device, 'x'
	device = strlowcase(device)
	default, files, '??'
	default, dn, 1L
    rt_graphics

	f = simlist(files, NOSCRATCH=1-KEYWORD_SET(SCRATCH))
	fs1 = fstat(1)
	if KEYWORD_SET(noload) then f=[fs1.name]
	if (f(0) EQ '') then begin
		print, "Couldn't find any of "+f
		print, "Bailing out!"
		stop
	endif
	Nf = N_ELEMENTS(f)

	for i=0L, Nf-1L do begin
		file, f(i), /QUIET
		print, f(i), ntmax
		if (i GT 0) then begin
			zz   = [[zz],   [ cdatat(0)]]
			zrad = [[zrad], [ cdatat('zrad')]]
			qqrm = [[qqrm], [ cdatat('qqrm')]]
			Fent = [[Fent], [-cdatat('fconv')]]
			Fkin = [[Fkin], [-cdatat('fkin')]]
			Frad = [[Frad], [-cdatat('frad')]]
		endif else begin
			zz   =  cdatat(0)
			zrad =  cdatat('zrad')
			qqrm =  cdatat('qqrm')
			Fent = -cdatat('fconv')
			Fkin = -cdatat('fkin')
			Frad = -cdatat('frad')
		endelse
	endfor
	Nt  = N_ELEMENTS(zz(0,*))
	flux = Fent + Fkin + Frad
	Ftot = aver(flux(1,*))
	flux = flux/Ftot& Frad=Frad/Ftot& Fent=Fent/Ftot& Fkin=Fkin/Ftot
	nzrad = min(where(reform(zrad(1:*,Nt-1L)-zrad(*,Nt-1L)) LT 0.0))
	if (nzrad LT 0) then nzrad=nz-1
	ii = [0,1,2,3,nzrad-4,nzrad-3,nzrad-2,nzrad-1]
	Nt0 = (Nt-1L) mod dn
	it = Nt0+lindgen(Nt/dn)*dn
	e = .5
	if (device EQ 'x') then begin
        Ncols = !D.TABLE_SIZE-1.
		c0 = Ncols*e
		dc = Ncols*(1.-e)/((Nt-1.)>1.)
	endif else begin
		!P.MULTI=[0,1,2]
		if (device EQ 'colorps') then begin
			ca4, name='c_checkrad'
		endif else begin
			a4, name='checkrad'
		endelse
        Ncols = !D.TABLE_SIZE-1.
		c0 = Ncols
		dc = Ncols*(e-1.)/((Nt-1.)>1.)
	endelse
	rx = !D.x_size/(4*nz+0*6)
	ry = (!D.y_size-long(!D.y_ch_size*!P.charsize-2))/Nt
;
;  The simulations system of units
    ul = cpar(0,'uul') & ut=cpar(0,'uut') & ur=cpar(0,'uur')
    uu=ul/ut & ue=uu^2 & uf=ue*ur*uu & sigma = sig/uf
	zz   = zz*ul*1e-2;		                SI-units - m
	zrad = zrad*ul*1e-2;		            SI-units - m
	zrange = [min(zz), max(zrad)*1.2 < max(zz)]
	zfct = decade(max(abs(zrange)), zdec);  Find sensible decadic factor/prefix
	zz=zz/zfct  & zrad=zrad/zfct  & zrange=zrange/zfct
	zunit = 'z/!3[!x'+zdec+'m!3]!x'
	izmax = max(where(reform(zz(*,Nt0)) LT zrange(1)))
	qrange = [min(qqrm(1:izmax,it), max=qmax), qmax]
	frange = [min(flux(0:izmax,it), max=fmax), fmax]
;
; Teff-range and tick mark, corresponding to the flux-range.
    Teffrng= (frange*Ftot/sigma)^.25 
    dTffrng= Teffrng(1L)-Teffrng(0L)
    decTefr= fix(alog10(dTffrng))
    Tftickv= [long(Teffrng(0L)/1e1^decTefr+1.)]*1e1^decTefr
    NTfticks= long((Teffrng(1L)-Tftickv(0L))/1e1^decTefr) + 1L
    Tftickv= Tftickv(0L) + findgen(NTfticks)*1e1^decTefr
    sTftickv=[long(Teffrng(0L)/1e1^(decTefr-1.)+1.)]*1e1^(decTefr-1.)
    NsTfticks=long((Teffrng(1L)-sTftickv(0L))/1e1^(decTefr-1.)) + 1L
    sTftickv= sTftickv(0L) + findgen(NsTfticks)*1e1^(decTefr-1.)
;
;  Pictures of fluxes as func of t and interpolated to regular z
    mnzz = min(zz,max=mxzz)
    zi   = mnzz + findgen(rx*nz)/(rx*nz-1.)*(mxzz-mnzz)
    Fmn = min([Frad, Fent, Fkin, Flux], max=Fmx)
    Fpics = fltarr(rx*nz, Nt, 4L)
    for i=0L, Nt-1L do begin
        Fpics(*,i,0L) = (intrp1(zz(*,i),zi, Frad(*,i))-Fmn)/(Fmx-Fmn)*Ncols
        Fpics(*,i,1L) = (intrp1(zz(*,i),zi, Fent(*,i))-Fmn)/(Fmx-Fmn)*Ncols
        Fpics(*,i,2L) = (intrp1(zz(*,i),zi, Fkin(*,i))-Fmn)/(Fmx-Fmn)*Ncols
        Fpics(*,i,3L) = (intrp1(zz(*,i),zi, Flux(*,i))-Fmn)/(Fmx-Fmn)*Ncols
    endfor
;
;  Print the menu of choises
	print, ''
	print, '    1         2         3         4         5'
	print, ' dz,dzrad  topflux    qqrad    zoom-qq  flux-pict'

	b0=(byte('0'))(0)
	stal='1'
	tal = (byte(stal))(0)-b0
	Nchoice = 5
	repeat begin
		case tal of
		1: begin
			plot, zrad(*,Nt0), zrad(1:nzrad,Nt0)-zrad(*,Nt0), xsty=2, ysty=9, $
                    xtitle=zunit, ytitle='!7D!xz/!3[!xMm!3]!x', $
                    yr=[0, 1.04*max(zrad(1:*,*)-zrad(0:nz-2,*))], xmarg=[9,7]
			Fmax = max([0.01, $
			 min([1.05*max(Frad(1:izmax,*)), 2.*aver(Frad(izmax,*)), $
                  8.00*min(Frad(1:izmax,*))])])
;   		 min([1.05*max(Frad(1:izmax,1:*)), 2.*aver(Frad(izmax,1:*))])])
			Fdz = !y.crange(1)/Fmax
			axis, yr=[0,Fmax*1e2], YAXIS=1, ysty=1, $
					ytitl='!8F!x!irad!n!3/!8F!x!itot!n!3/[!x%!3]!x'
			oplot, !x.crange, [1,1]*Fdz, line=2
			for i=Nt0, Nt-1L, dn do begin
				col = c0+dc*i
				oplot, zrad(*,i), zrad(1:nzrad,i)-zrad(*,i), col=col
				oplot, zz(*,i), Frad(*,i)*Fdz, col=col
				plots, zrad(ii,i),zrad(ii+1,i)-zrad(ii,i),psym=2,syms=.2,col=col
				oplot, zz(*,i), zz(1:*,i)-zz(*,i), psym=-2, syms=.2, col=col
				oplot, [1,1]*zz(0,i), !y.crange, line=1, col=col
				oplot, [1,1]*zz(1,i), !y.crange, line=1, col=col
				oplot, [1,1]*max(zrad(*,i)), !y.crange, line=2, col=c0+dc*i
			endfor
		   end
		2: begin
			plot, zz(*,Nt0), flux(*,Nt0), xr=zrange,yr=frange,ysty=25, $
				xtitle=zunit,ytitle='!8F!x!itot!n/!7r!8T!x!s!e 4!n!r!ieff!x!n',$
                xmarg=[8,8]
            axis, yaxis=1,ystyle=17,ytitle='!8T!x!deff!n!5/[!xK!5]!x', $
                yr=frange, ytickv=sigma*Tftickv^4/Ftot, yticks=NTfticks-1L, $
                ytickname=string(Tftickv, form='(I0)')
            xmintck=!x.crange(1)-[0,.5*!P.TICKLEN*(!x.crange(1L)-!x.crange(0L))]
            for i=0L,NsTfticks-1 do plots,xmintck,[1,1]*sigma*sTftickv(i)^4/Ftot
			for i=Nt0, Nt-1L, dn do begin
				oplot, zz(*,i), flux(*,i), col=c0+dc*i
				oplot, [1,1]*max(zrad(*,i)), !y.crange, line=2, col=c0+dc*i
			endfor
			oplot, !x.crange, [1,1], line=1
		   end
		3: begin
			plot, zz(*,Nt0), qqrm(*,Nt0), xr=zrange,yr=qrange,ysty=1, $
				xtitle=zunit, ytitle='!8Q!x!irad!n'
			for i=Nt0, Nt-1L, dn do begin
				oplot, zz(*,i), qqrm(*,i), col=c0+dc*i
				oplot, [1,1]*max(zrad(*,i)), !y.crange, line=2, col=c0+dc*i
			endfor
			oplot, !x.crange, [1,1]*0, line=1
		   end
		4: begin
			plot, zz(*,Nt0),qqrm(*,Nt0),xr=zrange,yr=[1,-1]*.1*qrange(0), $
				ysty=1, xtitle=zunit, ytitle='!8Q!x!irad!n'
			for i=Nt0, Nt-1L, dn do begin
				oplot, zz(*,i), qqrm(*,i), col=c0+dc*i
				oplot, [1,1]*max(zrad(*,i)), !y.crange, line=2, col=c0+dc*i
			endfor
			oplot, !x.crange, [1,1]*0, line=1
		   end
		5: begin
			erase
			tv, rebin(Fpics(*,*,0), rx*nz, ry*Nt), (rx*nz+1)*0, 0
			tv, [c0]#replicate(1, ry*Nt), rx*nz, 0
			xyouts, (rx*nz+1)*0.5, ry*Nt+3, '!8F!x!irad!n', $
															/device, align=.5
			tv, rebin(Fpics(*,*,1), rx*nz, ry*Nt), (rx*nz+1)*1, 0
			xyouts, (rx*nz+1)*1.5, ry*Nt+3, '!8F!iH!x!n', $
															/device, align=.5
			tv, rebin(Fpics(*,*,2), rx*nz, ry*Nt), (rx*nz+1)*2, 0
			xyouts, (rx*nz+1)*2.5, ry*Nt+3, '!8F!x!ikin!n', $
															/device, align=.5
			tv, rebin(Fpics(*,*,3), rx*nz, ry*Nt), (rx*nz+1)*3, 0
			xyouts, (rx*nz+1)*3.5, ry*Nt+3, '!8F!x!itot!n', $
															/device, align=.5
		   end
		else: print, 'The program stops'
		endcase
		stal = prompt('  Enter your choice: ', /ONELINE)
		tal = (byte(stal))(0)-b0
	endrep until (tal LT 1 OR tal GT Nchoice)
	print, ""
	if (device NE 'x') then begin
		device, /close
		set_plot,'x'
	endif
	if (KEYWORD_SET(STOP)  OR  stal EQ 's') then stop
	reset_graphics
end
