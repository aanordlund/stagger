;
;  Fundamental and derived natural constants from the CODATA 2002 report:
;  \cite{CODATA2002}.  Not sure where G is from...
;
;   cth = eV_J/k/alog(1d1)*1d7, theta = cth/T
;
;   Update history:
;   ---------------
;   06.04.2011 Fixed G: 6.67415979d-11  ->  6.67415979d-08*ul^3/ut^2/um
;   ??.??.2014 Updated k to Podesta et al. 2013.
;
	ce_SI   = 1.6021765314d-19
	vc_SI   = 2.99792458d+08
	eps0_SI = 1d7/(4d0*!dpi*vc_SI^2)	; F/m
;
    defsysv, '!usys', exists=exsysv
    if (NOT exsysv) then usys='cgs'  else  usys=!usys
;
;  Three systems of units are implemented.
	if (strlowcase(usys) EQ 'cgs')  then begin & $
		ul=1d0  &  ut=1d0  &  um=1d0  &  eps0=1d0/(4d0*!dpi) & $
	endif
	if (strlowcase(usys) EQ 'si')   then begin & $
		ul=1d-2 &  ut=1d0  &  um=1d-3 &  eps0=eps0_SI & $
	endif
	if (strlowcase(usys) EQ 'aake') then begin & $
		ul=1d-8 &  ut=1d-2 &  um=1d-17&  eps0=1d0/(4d0*!dpi) & $
	endif
	uv=ul/ut & ue=um*uv^2

	M_PI   = !dpi
	ln10   = alog(1d1)
	Mu     = 1.6605388628d-24*um                & LMu  =alog10(Mu)
	Mn     = 1.6749272829d-24*um                & LMn  =alog10(Mn)
    Mp     = 1.6726217129d-24*um                & LMp  =alog10(Mp)
    Me     = 9.109382616d-28*um                 & LMe  =alog10(Me)
;   k      = 1.380650524d-16*ue                 & Lk   =alog10(k)
; From de Podesta et al. 2013, {depodesta:kB-value}. 75 \mu% > CODATA value.
    k      = 1.3806515698d-16*ue                & Lk   =alog10(k)
	Rgas   = k/Mu                               & LRgas=alog10(Rgas)
    h      = 6.626069311d-27*ue*ut              & Lh   =alog10(h)
    h2pi   = h/2d0/!dpi                         & Lh2pi=alog10(h2pi);1.05457e-27
    vc     = vc_SI*1d2*uv                       & Lvc  =alog10(vc)
    a_3    = 8d0/4.5d1*(!dpi*k)^4*!dpi/(vc*h)^3 & La_3 =alog10(a_3);2.52197e-15
    sig    = 3d0*a_3*vc/4d0                     & Lsig =alog10(sig);5.670402e-05
    ce     = sqrt(1d9*um*ul*uv^2*eps0/eps0_SI)*ce_SI&Lce=alog10(ce)
	my0    = 1d0/(eps0*vc^2)                    & Lmy0 =alog10(my0)
	a0     = 4d0*!dpi*eps0*(h2pi/ce)^2/Me       & La0  =alog10(a0)
    alph   = ce^2/(h2pi*vc)
    eV_J   = ce_SI                              & LeV_J=alog10(eV_J)
    k_eV   = k/eV_J*1d-7/ue                     & Lk_eV=alog10(k_eV);8.61739e-05
	myH    = Mp*Me/(Mp+Me)
    khiH   = myH*ce^4/(2d0*h2pi^2)/eV_J*1d-7/k_eV/ln10
    khiH_  =  0.75416/k_eV/ln10
	myHe   = (2d0*Mp+2d0*Mn)*Me/((2d0*Mp+2d0*Mn)+Me)
    khiHe  = 24.58669379/k_eV/ln10
;    khiHe1 = 54.41775583/k_eV/ln10
    khiHe1 = myHe*ce^4/(2d0*h2pi^2)/eV_J*4d-7/k_eV/ln10
    khiH2  = 1.2441719D+05*(h*vc)/(eV_J*1d7)/k_eV/ln10
    DH2_0  =  36118.26d0*(h*vc)/(eV_J*1d7)/k_eV/ln10
    DH2_1  =  DH2_0 - (khiH2 - khiH)
; 1AU is the mean Sun-Earth distance, despite the Sun-barycenter distance
    AU     = 1.495978707d+13*ul			; \cite{stix:sun} + IAU def. per 2012.
    GMsun  = 1.32712438d+26*ul^3/ut^2
	G      = 6.67415979d-08*ul^3/ut^2/um
    Msun   = 1.989d+33*um				; 1.98845161d+33 g according to NIST G
    TSI    = 1360.8d3*ue/ul^2           ; \cite{kopp:SolarIrrAgree} @ Solar min.
;   Lsun   = 3.846d+33*ue/ut
    Lsun   = TSI*4d0*!dpi*(AU*1.00000261d0)^2
;   Rsun   = 6.9599d+10*ul
    Rsun   = 1919.359d0/2d0*atan(!dpi/(18d1*36d2))*AU*1.00000261d0*ul $
           - 0.025550682d8; D0 from \cite{brown:solar-radius} + solar_radius.pro
    TeffSun= (Lsun/(sig*4d0*!dpi*Rsun^2))^.25
	Mearth = 5.9737022d+27*um
	Rearth = 6.37816d8*ul
	parsec = AU*3.6d3*1.8d2/!dpi
	mile   = 1.609344d5*ul									; Statute mile
	tyear  = 3.155693d7*ut;/[s]								; Tropical year
	year   = 3.155815d7*ut;/[s]								; Sidereal year
	TNT    = 4.184d16*ue;/[J]									; 1 Ton TNT
	eV_AA  = (h*vc)/(eV_J*1d7)*1d8/ul&	LeV_AA=alog10(eV_AA); eV -> �
; \chi/[kJ/mol] * Mu/eV_J*1d3 = \chi/[eV/particle]   (*0.010364269)
; Tsar Bomba: 57MT TNT = (3200--4400)*Hiroshima = 2.4d17 J = 240 PJ
; Hiroshima, Little Boy = 13-18 kT of TNT =   5.4-7.5d13 J = 54-75 TJ.
    TTNT   = 4.184d16*ue;   1 T of TNT in ergs
;
