;
;   Conversion from vacuum wavelength to air wavelength.
;   ******  The statement below, no longer applies  ******
;   Analytical inverse of the fit in vacfunc, which is based on vac2air.tab
;   ******  The statement above, no longer applies  ******
;
;   This version includes variation with temperature, pressure, humidity
;   and CO2 concentration around the standard values listed below.
;
;   The coefficients by Birch & Downs (1993, 1994) \citet{birch:nAir}
;   and \citet{birch:nAirErratum} are derived for a
;   atmosphere as listed below:
;        B&D   U.S. Standard Atmosphere (1976)
;       --------------------
;       780900 780840 N2
;       209500 209476 O2
;         9300   9340 Ar
;          450    314 CO2
;
;   The variation with humidity and CO2 concentrations are inconsequential
;   on the scale of, say, a solar spectrum.
;
;   Arguments (defaults in [...]):
;    ll0:     Vacuum wavelength/[AA].
;    STOP:    Set this keyword to stop before returning, to make all
;             variables available to the user (for debugging).
;
;   Update history:
;   ---------------
;   07.05.1998 Coded/RT
;   21.02.2011 Changed to the formulation of Birch & Downs, 1993, Metrologia,
;              30, 155 and Erratum: 1994, Metrologia, 31, 315.
;   18.07.2013 Added keyword GRAV_RED to include gravitational redshift between
;              The Sun and the Earth. Also added option to input energy
;              differences instead of wavelength (if ll0 < 50.)/RT
;
;   Known bugs: None
;
function vac2air, ll0, T=T, P=P, fH2O=fH2O, xCO2=xCO2, GRAV_RED=GRAV_RED, $
                                            QUIET=QUIET, DEBUG=DEBUG, STOP=STOP
@consts
    Nl0 = N_ELEMENTS(ll0)
    i50 = where(ll0 LT 50.)
    if (i50(0L) LT 0L) then Ni50=0L  else  Ni50=N_ELEMENTS(i50)
    if (Ni50 EQ Nl0) then begin
        if (NOT KEYWORD_SET(QUIET)) then $
        print, " WARNING: input=",ll0," < 50, so we assume it is \Delta E/[eV]."
        ll0 = h*vc/eV_J*1d1/ll0
    endif
    if KEYWORD_SET(GRAV_RED) then begin
        ll = ll0/(1.d0+(GMsun*Mearth/Msun/Rearth-GMsun/Rsun)/vc^2)
    endif else begin
        ll = ll0
    endelse
;
; Leave wavelengths below 2000\AA unaltered.
    if (max(ll) LE 2d3) then return, ll
; The standard dry air:
    Tstd =     15d0;'C
    Pstd = 101325d0; Pa
    fH2Ostd =   0d0; Pa
    xCO2std = 450d0; ppm
; Use the standard atmosphere as default:
    default, T,     Tstd
    default, P,     Pstd
    default, fH2O,  fH2Ostd
    default, xCO2,  xCO2std
    eps = 1d-2

;	AA = [2.71709889d-03, 2.72550686d-04, 1.21847797d+02, 7.81341589d+02]
;	A = AA(1) + 1d0
;	B = AA(0) - ll - AA(1)*AA(3) - AA(3)
;	C = AA(2) - AA(0)*AA(3) + ll*AA(3)
;	D = B*B -4.d0*A*C
;	return, .5d0*(sqrt(D)-B)/A

    CC    = [8342.54d-8, 2406147.d-8, 130d0, 15998.d-8, 38.9d0]
    waven2 = (1d4/ll)^2
;
; (n-1), with n being the refractive index.
    refr = CC(0) + CC(1)/(CC(2) - waven2) + CC(3)/(CC(4) - waven2)
;
; Adjust for non-(standard dry air) conditions
    if (1e3*abs(T-Tstd)+abs(P-Pstd)  GT  eps) then begin
        if KEYWORD_SET(DEBUG) then $
            print, "vac2air: Adjusting for non-standard T and P."
; Adjusted:
;       AA  = [96095.430d0, 0.601d0, 0.00972d0, 0.0036610d0]; =0.99999234
; to:              ^ ^^^
        AA  = [96094.694d0, 0.601d0, 0.00972d0, 0.0036610d0]; =1 for Tstd/Pstd
; to obtain fPT = 1d0  for standard dry air.
        fPT = P/AA(0)*(1d0+1d-8*(AA(1)-AA(2)*T)*P)/(1d0+AA(3)*T)
        refr= refr*fPT
    endif
;
; Adjusted for humidity
    if (abs(fH2O-fH2Ostd)  GT  eps) then begin
        if KEYWORD_SET(DEBUG) then $
            print, "vac2air: Adjusting for non-zero humidity."
;       BB = [-5.7724d-08, 0.0457d-00];  birch:nAir
        BB = [-3.7345d-10, 0.0401d-10];  birch:nAirErratum
        refr = refr + fH2O*(BB(0) + BB(1)*waven2)
    endif
;
; Adjusted for CO2 content
    if (abs(xCO2-xCO2std)  GT  eps) then begin
        if KEYWORD_SET(DEBUG) then $
            print, "vac2air: Adjusting for non-standard CO2 concentration."
        refr = refr + (xCO2-xCO2std)*0.9d-8/(512d0-450d0)
    endif

    if KEYWORD_SET(STOP) then stop

    ii = where(ll LT 2d3)
    if (ii(0) GE 0L) then refr(ii) = 0d0
    return, ll/(refr + 1d0)
end
