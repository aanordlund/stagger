;
; Air wavelength to vacuum  wavelength. \sigma = 0.0005\AA
; Fit in vacfunc, based on vac2air.tab
; 07.05.98 Coded /RT
;
function air2vac, ll
	AA = [2.71709889d-03, 2.72550686d-04, 1.21847797d+02, 7.81341589d+02]
	vacfunc, ll, AA, dll
	return, ll+dll
end
