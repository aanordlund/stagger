pro printconsts
	unitsys = ['SI','cgs','Aake']
	Nsys = N_ELEMENTS(unitsys)
    rt_graphics
	erase
	xyouts, 0, 0, '!17'
	!P.charsize = 0.9*!P.charsize
	for j=0, Nsys-1 do begin
		!usys = unitsys(j)
@consts
		carr = [Mu,Mn,Mp,Me,k,h,vc,a_3,sig,ce,my0,eps0,a0,eV_J, $
				k_eV,myH,Msun,GMsun,Lsun,Rsun,Mearth,Rearth,AU,parsec]
		cnvn = ['!8M!x!iu!n','!8M!x!in!n','!8M!x!ip!n','!8M!x!ie!n',$
				'!8k!x','!8h!x','!8c!x','!8a!x/3','!7r!x','!8e!x','!7l!x!i0!n',$
				'!7e!x!i0!n','!8a!x!i0!n','eV/J','!8k!x/eV','!7l!x!iH!n', $
				'!8M!9!in!x!n', '!8GM!9!in!x!n','!8L!9!in!x!n','!8R!9!in!x!n', $
				'!8M!20!iS!x!n', '!8R!20!iS!x!n','1!i !nAU','1!i !npc']
		Nc = N_ELEMENTS(carr)
		x0 = 0.15 + j*0.30
		y0 = 0.9
		dy = 1./(Nc+3.)
		if (j EQ 0) then begin
			for i=0, Nc-1 do xyouts, 0.05, y0-i*dy, cnvn(i), /norm
			for i=0, Nc-1 do xyouts, 0.13, y0-i*dy, '=', /norm
		endif
		xyouts, x0, y0+1*dy, '    '+unitsys(j)+'-units', /norm
		for i=0, Nc-1 do $
			xyouts, x0, y0-i*dy, string(carr(i),form='(e14.7)'), /norm
	endfor
	reset_graphics
end
