function roman, arabic
	arab  = arabic
	romes =      ['I','V','X','L','C']
	arabs = long([ 1 , 5 , 10, 50,100])
	Nromes = N_ELEMENTS(romes)
	NN = N_ELEMENTS(arab)
	if (NN GT 1) then arabics=arab
	roms = strarr(NN)
	for k=0, NN-1 do begin
		if (NN GT 1) then arab=arabics(k)
		rom=''
		for i=Nromes-1,0,-2 do begin
			N = arab/arabs(i)
	;		print, arab(k), N, arabs(i), rom
			if (N EQ 9) then rom=rom+romes(i)+romes(i+2)
			if (N GT 4 AND N LT 9) then begin
				rom = rom+romes(i+1)
				for j=1, N-5 do rom=rom+romes(i)
			endif
			if (N EQ 4) then rom=rom+romes(i)+romes(i+1)
			if (N LT 4) then begin
				for j=1, N do rom=rom+romes(i)
			endif
			arab = arab-N*arabs(i)
		endfor
		roms(k) = rom
	endfor
	if (NN LE 1) then return, rom   else  return, roms
end
