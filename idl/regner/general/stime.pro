function stime, asciitime, stop=stop
	n = N_ELEMENTS(asciitime)
	if (N EQ 1) then asciitime=[asciitime] else asciitime=reform(asciitime, n)
	stimea = fltarr(n)
	for j=0,n-1 do begin
		hms = float(str_sep(asciitime(j), ':'))
		mhms = N_ELEMENTS(hms)
		nhms = mhms<3
		stimea(j) = 0.0
		for i=0, nhms-1 do  stimea(j) = stimea(j) + hms(mhms-i-1)*60.0^i
		if (mhms GT 3) then stimea(j) = stimea(j) + hms(mhms-3-1)*24.0*3.6e3
		if (mhms GT 4) then begin
			print, 'More than four time-slots. Bailing out!'
			stop
		endif
	endfor
	if KEYWORD_SET(STOP) then stop
	return, stimea
end
