;
; Function to convert time/[s] -> time/[d:hh:mm:ss.ss]
; Note: this version puts a blank-space in front of the hours...
;
;   Update history:
;   ---------------
;   14.06.1996 Coded/RT
;   01.03.2005 The input variable got changed during a run - corrected that/RT
;              The seconds were not rounded properly with keyword /INTEGER
;              correct that/RT
;
;   Known bugs: Doesn't work with arrays of secondsin.
;
function hmstime, secondsin, levels, NOHOURS=NOHOURS, NOSECONDS=NOSECONDS, $
												INTEGER=INTEGER, UNITS=UNITS
    default, levels, 20L
	seconds = secondsin
	if KEYWORD_SET(UNITS) then begin
		hmark="h" & mmark="m" & smark="s"
	endif else begin
		hmark=":" & mmark=":" & smark=""
	endelse
	eps = 0.001
	if KEYWORD_SET(INTEGER) then seconds=float(long(seconds+.5))
	hours = long(seconds/3600.+eps) mod 24L
	days  = long(seconds/3600.+eps)/24L
	sdays = ''& shours=''& sminutes=''& sseconds=''
    lvlsleft = levels
    NOTFIRST = 0b
	if (days GT 0) then begin
        sdays=string(days, form='(I0,"d")')
        lvlsleft = lvlsleft - 1L
        if (lvlsleft GT 0L) then sdays=sdays+" "
        NOTFIRST = 1b
    endif
    if (lvlsleft GT 0L  AND  (NOTFIRST  OR  hours GT 0)) then begin
        if (hours LT 10) then begin
            shours=string(hours, format='("0",I1)') + hmark
        endif else begin
            shours=string(hours, format=    '(I2)') + hmark
        endelse
        lvlsleft = lvlsleft - 1L
        NOTFIRST = 1b
    endif
	left = seconds - 3600.*(hours + 24L*days)
	minutes = long(left/60.+eps);	-1l
    if (lvlsleft GT 0L  AND  (NOTFIRST  OR  minutes GT 0)) then begin
        if (minutes LT 10 AND NOT KEYWORD_SET(NOHOURS)) then begin
            sminutes=string(minutes, format='("0",I1)') + mmark
        endif else begin
            sminutes=string(minutes, format=    '(I2)') + mmark
        endelse
        lvlsleft = lvlsleft - 1L
        NOTFIRST = 1b
    endif
	seconds = left - 60.*minutes
;	print, seconds
    if (lvlsleft GT 0L  AND  (NOTFIRST  OR  seconds GT 0)) then begin
        if KEYWORD_SET(INTEGER) then begin
            sseconds=string(seconds, format='(I02)') + smark
        endif else begin
            sseconds=string(seconds, format='(F05.2)') + smark
        endelse
        lvlsleft = lvlsleft - 1L
        NOTFIRST = 1b
    endif
	asciitime=string(shours,sminutes,sseconds,format='(3A0)')
	if KEYWORD_SET(NOSECONDS) then $
		asciitime=string(shours,sminutes,format='(2A0)')
	if KEYWORD_SET(NOHOURS) then $
		asciitime=string(sminutes,sseconds,format='(2A0)')
	if (KEYWORD_SET(NOSECONDS) AND KEYWORD_SET(NOHOURS)) then $
		asciitime=string(sminutes,format='(A0)')
;	print, shours,sminutes,sseconds,format='(10(A0,"|"))'
	return, sdays+asciitime
end
