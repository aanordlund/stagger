;
;   Routine to print the current working directory.
;
;   Update history:
;   ---------------
;   03.11.2004 Coded/RT;
;
;   Known bugs: None.
;
pro pwd, current, QUIET=QUIET
	cd, current=current
	if (NOT KEYWORD_SET(QUIET)) then print, current
end
