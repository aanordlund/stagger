;
;   Program to close all open files, regardless of how they were opened.
;
;   Arguments (defaults in [...]):
;    STOP:    Set this keyword to stop before returning, to make all
;             variables available to the user (for debugging).
;
;   Update history:
;   ---------------
;   17.10.2007 Coded/RT
;   25.07.2011 Added handling of possible open EPS/PS-files/RT
;
;   Known bugs: None
;
pro mclose, i0, i1
	default, i0, 1
	default, i1, 128
	for i= i0,     (i1< 99) do close,   i
	for i=(i0>100),(i1<128) do begin
        fname = strlowcase((fstat(i)).name)
        if (strpos(fname, '.ps') GT 0L  OR  $
            strpos(fname, '.eps') GT 0L) then begin
            device, /CLOSE
            set_plot, 'x'
        endif else begin
            free_lun, i
        endelse
    endfor
end
