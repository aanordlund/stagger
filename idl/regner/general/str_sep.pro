function str_sep, a, b, N=N
	return, strsplit(a, b, COUNT=N, /EXTRACT, /REGEX)
end
