;
; Function to replace normal underscores, with vector-font versions,
; for plot-titles, annotations and labels.
; Bugs: Can only handle less than 20 underscores.
;
; Update history
;  27.02.2003 Can now handle arrays of strings. /RT
;  26.04.2007 Now also handles '[', ']' and '�' /RT
;  24.07.2017 Added keyword REMOVE, to remove underscores, et al./RT
;
function underscore, strng, REMOVE=REMOVE
	Nstr = N_ELEMENTS(strng)
    if KEYWORD_SET(REMOVE) then repl=["","","",""] $
                           else repl=["!3_!x","!3[!x","!3]!x","!3'+string(197b)+'!x"]
    forms = ''
 	if (Nstr GT 1L) then begin
		stra = strng
		for i=0L,Nstr-1L do begin
			stra(i)=string(str_sep(strng(i), '_'),form='(20(A0,:,"!3_!x"))')
			stra(i)=string(str_sep(stra(i), '\['),form='(20(A0,:,"!3[!x"))')
			stra(i)=string(str_sep(stra(i), '\]'),form='(20(A0,:,"!3]!x"))')
			stra(i)=string(str_sep(stra(i),  '�'),form='(20(A0,:,"!3'+string(197b)+'!x"))')
		endfor
		return, stra
 	endif else begin
 		stra = string(str_sep(strng, '_'),form='(20(A0,:,"!3_!x"))')
 		stra = string(str_sep(stra, '\['),form='(20(A0,:,"!3[!x"))')
 		stra = string(str_sep(stra, '\]'),form='(20(A0,:,"!3]!x"))')
 		stra = string(str_sep(stra,  '�'),form='(20(A0,:,"!3'+string(197b)+'!x"))')
 		return, stra
 	endelse
end
