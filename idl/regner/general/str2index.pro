function str2index, strin, width
	strinlen = strlen(strin)
	ii = 0 & n = 0 & tal0 = -1 & SLUT = 0
	single0 = 0 & interval0 = 0
	single = 1 & interval = 0
    width  = 2L
    swidth = string(width, form='(I0)')
	tal = replicate(-1,100)
	if (strmid(strin,ii,1) EQ '[') then ii = ii + 1
	if (strmid(strin,ii,1) GE '0' AND strmid(strin,ii,1) LE '9') then begin
		tal1 = fix(strmid(strin,ii,1)) & ii = ii + 1
	endif else begin
		print, '  No number in the beginning'
		return, -1
	endelse
	repeat begin
		case strmid(strin,ii,1) of
			'0': tal1 = tal1*10 + fix(strmid(strin,ii,1))
			'1': tal1 = tal1*10 + fix(strmid(strin,ii,1))
			'2': tal1 = tal1*10 + fix(strmid(strin,ii,1))
			'3': tal1 = tal1*10 + fix(strmid(strin,ii,1))
			'4': tal1 = tal1*10 + fix(strmid(strin,ii,1))
			'5': tal1 = tal1*10 + fix(strmid(strin,ii,1))
			'6': tal1 = tal1*10 + fix(strmid(strin,ii,1))
			'7': tal1 = tal1*10 + fix(strmid(strin,ii,1))
			'8': tal1 = tal1*10 + fix(strmid(strin,ii,1))
			'9': tal1 = tal1*10 + fix(strmid(strin,ii,1))
			'-': begin
					if (interval) then begin
						print, '  Two consecutive intervals'
						return, -1
					endif
					if (single) then begin
						tal(n) = tal1
;						n = n + 1
						single = 0
					endif
					interval = 1
					tal0 = tal1 & tal1 = 0
				 end
			',': begin
					if (interval) then begin
						dn = tal1 - tal0 + 1
						tal(n:n+dn-1) = tal0 + indgen(dn)
						n = n + dn
						interval = 0
						single = 1
					endif else begin
						tal(n) = tal1
						n = n + 1
						single = 1
					endelse
					tal0 = tal1 & tal1 = 0
				 end
			'[':
			']':
			else:begin & print, '  No valid character' & return, -1 & end
		endcase
		ii = ii + 1
;		print,strmid(strin,ii-1,1),tal0,tal1,interval,single,format='(A4,4I6,$)'
;		print, byte(tal(0:max([0,n-1])))
	endrep until (ii GT strinlen-1)
	if (interval) then begin
		dn = tal1 - tal0 + 1
		tal(n:n+dn-1) = tal0 + indgen(dn)
		n = n + dn
		tal0 = -1
		interval = 0
	endif else begin
		if (single) then begin
			tal(n) = tal1
			n = n + 1
			single = 0
		endif else begin
			print, '  Not an interval nor a single number'
			return, -1
		endelse
	endelse
	i0 = where(tal GE 0)
	if (i0(0) EQ -1) then begin
		print, '  No numbers at all'
		return, -1
	endif
	stal = tal(i0);		tal(sort(tal(i0)))
    strindexes = string(stal, format='(I0'+swidth+')')
	return, strindexes
end
