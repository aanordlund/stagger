;
;   Routine to back-up a file, using a sequence of .bak[n]
;   suffixes, for increasing n.
;
;   Command arguments (defaults in [...]):
;    filein: The name of the file to back-up.
;    newnr:  Returns the number [n] in the back-up sequence.
;    ext:    The extension to use for the backed-up files ['.bak']
;    targetdir:Returns the directory of the backed-up file (might be linked).
;    QUIET:  Suppress the printing of warnings, but not of error-messages.
;    STOP:   Set this keyword to stop before returning, to make all
;            variables available to the user (for debugging).
;
;   Update history:
;   ---------------
;   22.11.2003 Coded/RT
;   15.04.2004 Introduced QUIET keyword/RT
;              Re-wrote spawn's in terms of `file_move` and `file_copy`/RT
;   26.10.2006 If the file is a link, back-up the target, not the link/RT
;   17.03.2010 The argument file is now protected from changes/RT
;   11.10.2011 Added possibility of returning the target-dir and fixed bug
;              in non-'bak' extensions/RT
;   03.10.2012 Added keyword NOOVERWRITE to not overwrite an original./RT
;
pro backup, filein, newnr, ext=ext, targetdir=targetdir, fileout=fileout, $
                    QUIET=QUIET, NOOVERWRITE=NOOVERWRITE, MOVE=MOVE, STOP=STOP
    default, ext, 'bak'
    if (strmid(ext, 0, 1) NE '.'  AND  ext NE '') then ext = '.'+ext

    file = filein
    targetdir = './'
	ihome = (strpos(file,'~'))(0)
	if (ihome EQ 0) then file=getenv('HOME')+strmid(file,ihome+1,99)
	if (NOT file_test(file)) then begin
		if (NOT KEYWORD_SET(QUIET)) then $
			print, "WARNING! Couldn't find the file,"+file+", to backup."
		newnr = '0'
		return
	endif
	if (file_test(file, /SYMLINK)) then begin
		target     = file_readlink(file)
		targetfile = file_basename(target)
		targetdir  = file_dirname(target)
		while (file_test(targetdir, /SYMLINK)) do begin
			targetdir  = file_readlink(targetdir)
		endwhile
        targetdir = targetdir + path_sep()
		file = targetdir + targetfile
		if (NOT file_test(file)) then begin
			if (NOT KEYWORD_SET(QUIET)) then $
				print, "WARNING! Couldn't find the file,"+file+", to backup."
			newnr = '0'
			return
		endif
	endif
 	if (NOT KEYWORD_SET(QUIET)) then print, ' Backed up ',file
	files = findfile(file+ext+'*')
	nbak = N_ELEMENTS(files)
;
; Move first, then copy, to preserve time-stamp
	if (files(0) EQ '') then begin
        if (ext EQ '.bak') then newnr = '1'   else  newnr = ''
	endif else begin
		nr = lonarr(nbak)
		inr = strlen(file) + strlen(ext)
		for i=0,nbak-1 do nr(i)=long(strmid(files(i),inr,5))
		newnr = string(max(nr)+1L, form='(I0)')
	endelse
    fileout = file
    if (NOT(FILE_TEST(file+ext+newnr) AND KEYWORD_SET(NOOVERWRITE))) then begin
		file_move, file,           file+ext+newnr
        if (NOT KEYWORD_SET(MOVE)) then $
            file_copy, file+ext+newnr, file
    endif

    if KEYWORD_SET(STOP) then stop
    return
end
