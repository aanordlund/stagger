function int2str2, n
	if (n LT 0 OR n GT 99) then begin
		if (n LT  0) then print, "ERROR: n=",n," < 0. Returning '--'."
		if (n GT 99) then print, "ERROR: n=",n," > 99. Returning '--'."
		return, '--'
	endif
	if (n LT 10) then return, string(n, form='("0",I1)') $
				 else return, string(n, form='(    I2)')
end
