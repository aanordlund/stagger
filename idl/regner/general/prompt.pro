function prompt, stra, ONELINE=ONELINE
	print, stra, form='(A,$)'
	answ=get_kbrd(1)
	if KEYWORD_SET(ONELINE) then begin
		print, string(13b), form='(A0,$)'
	endif else begin
		if ((byte(answ))(0) NE 10) then print,answ  else print,''
	endelse
	return, answ
end
