;
;   Program to display various Hershey fonts together with the corresponding
;   ASCII characters. This is similar to s/r showfont, except 'greek' also
;   shows the ASCII characters, making this tool ideal for figuring out what
;   Hershey commands will result in the desired output.
;   First line:  ASCII number (byte).
;   Second line: ASCII character (Hershey font #3).
;   Third line:  The corresponding character in the desired font, 'font'.
;
;   Arguments (defaults in [...]):
;    font:    The number of the Hershey font to show.
;    device:  The plotting device to use ['x'].
;    STOP:    Set this keyword to stop before returning, to make all
;             variables available to the user (for debugging).
;
;   Update history:
;   ---------------
;   17.10.2007 Coded/RT
;   25.07.2011 Now prints out all characters, from 0b-255b, added baselines
;              for each line of characters and title with name and number of
;              the font and added handling of devices./RT
;
;   Known bugs: None
;
pro greek, font, device=device, STOP=STOP
;
; Font names from the IDL manual
    fontnames = ['', '', '', 'Simplex Roman', 'Simplex Greek', 'Duplex Roman', $
        'Complex Roman','Complex Greek','Complex Italic','Math and Special','',$
        'Gothic English','Simplex Script','Complex Script','Gothic Italian', $
        'Gothic German','B Cyrilic','Triplex Roman','Triplex Italic', '', $
        'Miscellaneous']

	default, font, 7
    default, device, 'x'
    device = strlowcase(device)
    if (device EQ 'x') then begin
        cs = 1.1
        wc = .4
    endif else begin
        a4, y=17.78, name='~/idl/general/'+fontnames(font)+'.ps'
        cs = 0.6
        thicker, 4., cs=cs
        wc = .7
    endelse
;The overall charactersize in units of the default.
    !P.charsize=cs*!P.charsize
;The xyouts charactersize in units of the overall size.
    cs = 1.0*!P.charsize
;   loadct, 0
    c0 = (!D.N_COLORS-1.)*wc

	sfont = "!"+string(font, form='(I0)')
    x0 = .04 & dx=.03
    y0 = .955& dy=.12 & y1=y0-.04& y2=y0+.03
    plot, [0,1], [0, 1], xsty=5, ysty=5, /NODATA, xmarg=[2,1], ymarg=[1,3], $
        title='!17Font '+string(font, form='(I0)')+': '+fontnames(font)
    xyouts,x0,y2,     string(33,form='(I3)'), size=.5*cs,/DATA
    xyouts,x0,y0,     "!3!!!x", size=cs,/DATA
    xyouts,x0,y1, sfont+"!!!x", size=cs,/DATA
    Nline = 30b
	for i=34b,255b do begin
        ii = i-33b
        j = ii/Nline
        k = ii mod Nline
        oplot, x0+dx*[0,Nline],y0-[j,j]*dy, col=c0, line=1, th=1
        oplot, x0+dx*[0,Nline],y1-[j,j]*dy, col=c0, line=1, th=1
        xyouts,x0+dx*k,y2-j*dy,string(i,form='(I3)'), size=.5*cs,/DATA
		xyouts,x0+dx*k,y0-j*dy, "!3"+string(i)+"!x", size=cs,/DATA
		xyouts,x0+dx*k,y1-j*dy,sfont+string(i)+"!x", size=cs,/DATA

;		xyouts,.04+.03*k,.435,      string(i+byte('A')),     size=cs,/NORM
;		xyouts,.04+.03*k,.405,sfont+string(i+byte('A'))+"!x",size=cs,/NORM
	endfor
    oplot, x0+dx*[0,23], dy*.35*[1,1], th=1
    xyouts, x0, dy*.2, size=cs*.7, $
        'Many fonts repeat the characters from number 256/2!i !n+!i !n33!i !n=!i !n161'

    if (device NE 'x') then begin
        device, /CLOSE
        set_plot, 'x'
    endif
    reset_graphics
    if KEYWORD_SET(STOP) then stop
end
