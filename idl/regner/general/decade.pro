;
;   Function to generate the appropriate decadal prefix for a number, x.
;
;   Arguments:
;    x:				the number to generate the decadal prefix for.
;    sdec:          a string with the decadal prefix of x.
;    ASCII:			set this keyword when result is not to be output to a plot.
;    NONSCIENTIFIC: set this keyword to also include the centi-, deci-, deca-,
;                   and hecto-prefixes.
;    CENTI:         set this keyword to also include the centi-prefix.
;    Returns:       The factor (order of magnitude) that x should be divided
;                   by in order to use the decadal prefix.
;   
;   Update history:
;   ---------------
;   21.02.2001 Coded/RT
;   11.10.2006 New version that returns scaling factor, and the decadal
;              prefix is returned via an argument, sdec/RT
;   21.09.2019 Fixed type of 'ten' to be the same as that of theinput, 'x'./RT
;
;   Known bugs: None
;
function decade, x, sdec, ASCII=ASCII, NONSCIENTIFIC=NONSCIENTIFIC, $
														CENTI=CENTI, STOP=STOP
	if (abs(x) GE 1.00) then i=fix(alog10(abs(x))) $
						else i=fix(alog10(abs(x))-.99999999)
    ten = fix(10, type=size(x, /TYPE))
	si = string(i,form='(I0)')
	trpl = ['a','f','p','n','!7l!x','m','','k','M','G','T','P','E']
	if KEYWORD_SET(ASCII) then trpl(4)=string(181b)
	N  = N_ELEMENTS(trpl)
	N0 = (where(trpl EQ ''))(0L)
	i3 = (lindgen(N)-N0)*3L
	if KEYWORD_SET(NONSCIENTIFIC) then begin
		trpl = [trpl(0:N0-1), 'c','d',trpl(N0),'da','h', trpl(N0+1:*)]
		i3   = [  i3(0:N0-1), -2L,-1L,  0L,     1L, 2L,  i3(N0+1:*)]
	endif else begin
		if KEYWORD_SET(CENTI) then begin
			trpl = [trpl(0:N0-1), 'c', trpl(N0:*)]
			i3   = [  i3(0:N0-1), -2L,   i3(N0:*)]
		endif
	endelse
	mini3 = min(i3,max=maxi3)
	if (i LT mini3) then begin
		print, 'No decadic prefix for 10^'+si+'.'
		sdec = trpl(0)
		return, ten^mini3
	endif
	if (i GT (maxi3+2)) then begin
		print, 'No decadic prefix for 10^'+si+'.'
		sdec = trpl(N_ELEMENTS(trpl)-1)
		return, ten^maxi3
	endif
	ii = max(where(i3 LE i))
	sdec = trpl(ii)
	return, ten^i3(ii)
end
