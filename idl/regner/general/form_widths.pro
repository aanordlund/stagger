;
;   Function to return the widths of each entry in a format string.
;   The widths of string constants in the format string are set negative.
;
;   Update history:
;   ---------------
;   19.09.2007 Coded/RT
;   19.09.2007 Added output of array of format strings/RT
;   19.09.2007 Can now handle parenthesized sections through recursive calls/RT
;
;   Known bugs: Cannot handle all format codes or parenthesized sections.
;
function form_widths, form, formarr=formarr, STOP=STOP
	form0 = strmid(form,1,strlen(form)-2)
	fprt  = str_sep(form0,',')
	Nprt  = N_ELEMENTS(fprt)
	i     = 0L
	repeat begin
		str1 = strlowcase(strmid(fprt(i),0,1))
		switch str1 of
			'a':
			'i':
			'f':
			'e':
			'g': begin
					ww    = long(strmid(fprt(i),1))
					forms = fprt(i)
					break
				 end
			'"': begin
					ww    = -(strlen(fprt(1)) - 2L)
					forms = fprt(i)
					break
				 end
			'1':
			'2':
			'3':
			'4':
			'5':
			'6':
			'7':
			'8':
			'9': begin
					Nw    = long(strmid(fprt(i),0))
					wNw   = strlen(string(Nw, form='(I0)'))
;
;  Red alert - we got a parenthesized sub-format statement on our hands!
					if (strmid(fprt(i),wNw,1) EQ '(') then begin
						j = i
						while (strpos(fprt(j),')') LT 0  OR  j EQ (Nprt-1)) do $
																	j = j + 1L
;  We need to check whether we reached the end of fprt without succes
;
;  Concatenate the involved parts and reform the array
						if (j GT i) then begin
							fprt(i) = string(fprt(i:j), form='(99(A0,:,","))')
							if (j LT (Nprt-1L)) then begin
								fprt = [fprt(0:i),fprt(j+1:*)]
							endif else begin
								fprt = fprt(0:i)
							endelse
							Nprt = N_ELEMENTS(fprt)
;  Call recursively on the parenthesized sub-format statement.
							subform = strmid(fprt(i),wNw)
							subw = form_widths(subform, formarr=subformarr)
						endif
						ww     = [    subw] & forms = [       subformarr]
						for j=1, Nw-1L do begin
							ww = [ww, subw] & forms = [forms, subformarr]
						endfor
					endif else begin
						ww    = replicate(long(strmid(fprt(i),wNw+1)), Nw)
						forms = replicate(strmid(fprt(i),wNw),         Nw)
					endelse
					break
				 end
		endswitch
		if (i GT 0L) then begin
			w       = [w,       ww]
			formarr = [formarr, forms]
		endif else begin
			w       = [         ww]
			formarr = [         forms]
		endelse
		i = i + 1L
	endrep until (i GE Nprt)

	if KEYWORD_SET(STOP) then stop
	return, w
end
