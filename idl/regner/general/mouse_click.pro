;+
;   Program to check whether one can monitor for both mouse-clicks and
;   typing events simultaneously.
;
;   Arguments (defaults in [...]):
;    STOP:    Set this keyword to stop before returning, to make all
;             variables available to the user (for debugging).
;
;   Update history:
;   ---------------
;   05.03.2020 Coded/RT
;
;   Known bugs: None
;-
pro mouse_click, s, cx, cy, DEBUG=DEBUG, NEW_LINE=NEW_LINE, STOP=STOP

    if KEYWORD_SET(DEBUG) then dbg=1b  else  dbg=0b
    s=''
    repeat begin
; Check mouse location and button status
        cursor, cx, cy, 0, /data
        Mx = !mouse.x& Mb=!mouse.button
        if (dbg) then  plots, cx, cy, psym=4, syms=.5
; Check for key-strokes
        if (Mx LT 0) then s = get_kbrd(0)
; Stop if anything has occured
    endrep until (Mb NE 0  OR  s NE '')
    if KEYWORD_SET(NEW_LINE) then print, ''

    s = strmid(s, 0, 1)
    if (dbg) then $
        print, "mouse_click: ",s," ",cx,cy,!mouse.x,!mouse.y,!mouse.button

    if KEYWORD_SET(STOP) then stop
end
