;
;   Function to return the name of the file pointed to by a link.
; 
;   Update history:
;   ---------------
;   29.08.2007 Coded/RT
;
;   Known bugs: Only works properly from the directory of the link.
;
function linksto, filename
	spawn, 'ls -l '+filename, res
	parts = str_sep(res, ' ')
	return, parts(N_ELEMENTS(parts)-1L)
end
