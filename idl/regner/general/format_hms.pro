;
;   Function to return a Hershey formatted version a hh:mm:ss.s string
;
;   Update history:
;   ---------------
;   30.10.2007 Coded/RT
;
;   Known bugs: Only handles scalar 'asciitime'.
;
function format_hms, asciitime, ANGLE=ANGLE, STOP=STOP
	prts  = str_sep(asciitime, ":")
	Nprts = N_ELEMENTS(prts)
	secs  = str_sep(prts(Nprts-1L), "\.")
	Nsecs = N_ELEMENTS(secs)
	days  = str_sep(prts(0), "d")
	Ndays = N_ELEMENTS(days)
	form_hms = ''
	sdeg  = "!3"+string(176b)+"!x"
	if KEYWORD_SET(ANGLE) then begin
;
; Format angles
;
		if (Ndays GT 1L) then begin
; Days(!) and degrees
			print, "WARNING: An angle was requested, but a number of days was"
			print, "         also supplied. This will form a strange string..."
			form_hms = form_hms + days(0)+"!ud!n"+days(1)+sdeg
		endif else begin
; Degrees
			form_hms = form_hms + prts(0)+sdeg
		endelse
; Minuttes
		if (Nprts GT 1L) then form_hms = form_hms + prts(1) + "'"
		if (Nprts GT 2L) then begin
			if (Nsecs GT 1L) then begin
; Seconds with decimals            '!l!i !n' = spacing to center decimal point
				form_hms = form_hms + secs(0) + '!s!l!i !n.!r"' + secs(1)
			endif else begin
; Seconds without decimals
				form_hms = form_hms + prts(2) + '"'
			endelse
		endif
	endif else begin
;
; Format times
;
		if (Ndays GT 1L) then begin
; Days(!) and hours
			form_hms = form_hms + days(0)+"!ud!n"+days(1)+"!uh!n"
		endif else begin
; Hours
			form_hms = form_hms + prts(0)+"!uh!n"
		endelse
; Minuttes
		if (Nprts GT 1L) then form_hms = form_hms + prts(1) + "!um!n"
		if (Nprts GT 2L) then begin
			if (Nsecs GT 1L) then begin
; Seconds with decimals
				form_hms = form_hms + secs(0) + '!s.!r!us!n' + secs(1)
			endif else begin
; Seconds without decimals
				form_hms = form_hms + prts(2) + "!us!n"
			endelse
		endif
	endelse

	if KEYWORD_SET(STOP) then stop
	return, form_hms
end
