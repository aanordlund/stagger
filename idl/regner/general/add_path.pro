;
;   Program to add a new directory to the IDL program path and update
;   the path cache.
;
;   Update history:
;   ---------------
;   10.04.2007 Coded/RT
;
;   Known bugs: None
;
pro add_path, path_dir, STOP=STOP
; Remove leading and trailing spaces
	path_dir = strtrim(path_dir, 2)
	len1 = strlen(pathdir)-1L
; Add colon if not present
	if (strpos(pathdir, ":") LT len1) then $
			path_dir=path_dir+":"
	len1 = strlen(path_dir)-1L
; Substitution of "~/" if present
	if (strpos(path_dir, "~/") EQ 0L) then $
			path_dir=getenv('HOME')+strmid(path_dir,1,len1)
	if (strmid(path_dir,0,1) NE "/") then begin
		print, "  ERROR: path_dir='"+path_dir+"'"
		print, "         is NOT a absolute directory - try again."
		return
	endif
; Add the new directory to the path
	!path = path_dir+":"+!path
; ...and rebuild the path cache
	path_cache, /rebuild

	if KEYWORD_SET(STOP) then stop
end
