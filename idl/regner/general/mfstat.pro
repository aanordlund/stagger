pro mfstat, lun0
	default, lun0, 1L
	lun00 = lun0
	lunmax = 128
	print, lun0, form='(I3,$)'
	bs = [string(8b), string(8b), string(8b)]
	repeat begin
		wait, 0.02
		fs = fstat(lun0)
		lun0 = lun0+1L
		print, bs, lun0, form='(3A0,I3,$)'
	endrep until (fs.open OR lun0 GT lunmax)
	if (lun0 GT lunmax) then begin
		print, bs, form='(3A0,$)'
		if (lun00 GT 1) then begin
			print, 'No open files beyond LUN='+string(lun00,form='(I0)')+'.'
		endif else begin
			print, 'No open files.'
		endelse
		return
	endif
	dots='...............................'
	print,bs,'LUN       NAME                       STATUS  ACCESS  SIZE/[bytes]'
	for i=lun0-1, lun0+5 do begin
		fs = fstat(i)
		navn = fs.name
		if (navn NE '') then navn=' '+navn+dots
		acc = '---'
		if (fs.read) then acc = ' r '
		if (fs.write) then acc = ' w '
		if (fs.read AND fs.write) then acc = 'r/w'
		if (fs.open) then status = ' OPEN ' else status = 'CLOSED'
		if (fs.open) then size = fs.size else size = 0
		print, i, navn, status, acc, size, format='(I3,A33,A7,A7,I14)'
	endfor
end
