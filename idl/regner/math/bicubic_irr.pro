;
;   Function to perform bicubic interpolation of f at (x,y) given f and
;   its derivatives fx=df/dx  and fy=df/dy, as well as the (possibly)
;   irregular, but rectangular grid-points of f; xf and yf.
;
;   Update history:
;   ---------------
;   21.05.2006 Ported from bicubic.pro /RT
;
;   Known bugs: Assuming rectangular grid for f.
;
function bicubic_irr, x, y, f, xf, yf, fx, fy, dfx, dfy, LINEAR=LINEAR,STOP=STOP
;
; Dimensions
	Nx = N_ELEMENTS(xf)
	Ny = N_ELEMENTS(yf)
; x-coordinates
	xi = interpol(findgen(Nx),xf,x)
	ix = (long(xi) > 0L) < (Nx-2L)
	dx = deriv(findgen(Nx), xf);	xf(1:*) - xf
	px = xi - ix
	qx = 1.-px
; y-coordinates
	yi = interpol(findgen(Ny),yf,y)
	iy = (long(yi) > 0L) < (Ny-2L)
	dy = deriv(findgen(Ny), yf);	yf(1:*) - yf
	py = yi - iy
	qy = 1.-py
;
	if KEYWORD_SET(LINEAR) then begin
;
; Bi-linear interpolation
		ff  = qx * (qy * f(ix  , iy  ) + py * f(ix  , iy+1) ) + $
			  px * (py * f(ix+1, iy+1) + qy * f(ix+1, iy  ) )
	endif else begin
		if (N_ELEMENTS(fx) LT Nx*Ny) then begin
			fx = f
			for i=0L, Ny-1L do fx(*,i)=derf(xf, f(*,i))
		endif
		if (N_ELEMENTS(fy) LT Nx*Ny) then begin
			fy = f
			for i=0L, Nx-1L do fy(i,*)=derf(yf, f(i,*))
		endif
		pyqy=py*qy*dy(iy)
		pxqx=px*qx*dx(ix)
		py2 =py*py*dy(iy)
		qy2 =qy*qy*dy(iy)
; Differences
		dfx1=(f(ix+1,iy  )-f(ix  ,iy  ))/(xf(ix+1) - xf(ix))
		dfx2=(f(ix+1,iy+1)-f(ix  ,iy+1))/(xf(ix+1) - xf(ix))
		dfy1=(f(ix  ,iy+1)-f(ix  ,iy  ))/(yf(iy+1) - yf(iy))
		dfy2=(f(ix+1,iy+1)-f(ix+1,iy  ))/(yf(iy+1) - yf(iy))
;
; Bi-cubic interpolation
		ff = qx * (qy * (f (ix  ,iy  )             $
				+ pyqy* (fy(ix  ,iy  ) - dfy1)     $
				+ pxqx* (fx(ix  ,iy  ) - dfx1) )   $
				+  py * (f (ix  ,iy+1)             $
				- pyqy* (fy(ix  ,iy+1) - dfy1)     $
				+ pxqx* (fx(ix  ,iy+1) - dfx2) ) ) $
		   + px * (py * (f (ix+1,iy+1)             $
				- pyqy* (fy(ix+1,iy+1) - dfy2)     $
				- pxqx* (fx(ix+1,iy+1) - dfx2) )   $
				+  qy * (f (ix+1,iy  )             $
				+ pyqy* (fy(ix+1,iy  ) - dfy2)     $
				- pxqx* (fx(ix+1,iy  ) - dfx1) ) )

	endelse

	if KEYWORD_SET(STOP) then stop

	if (N_PARAMS() LT 8L) then return, ff
	if KEYWORD_SET(LINEAR) then begin
      dfx    =(qy * (   - (f (ix  ,iy  )       ) + $
                          (f (ix+1,iy  )       ) ) + $
               py * (     (f (ix+1,iy+1)       ) - $
                          (f (ix  ,iy+1)       ) ) )/dx(ix)
	endif else begin
      dfx    =(qy * (   - (f (ix  ,iy  )       ) + $
     (1.-4.*px+3.*px^2) * (fx(ix  ,iy  ) - dfx1)*dx(ix) + $
                (-pyqy) * (fy(ix  ,iy  ) - dfy1) + $
                          (f (ix+1,iy  )       ) - $
        (2.*px-3.*px^2) * (fx(ix+1,iy  ) - dfx1)*dx(ix) + $
                   pyqy * (fy(ix+1,iy  ) - dfy2) ) + $
               py * (     (f (ix+1,iy+1)       ) - $
        (2.*px-3.*px^2) * (fx(ix+1,iy+1) - dfx2)*dx(ix) - $
                   pyqy * (fy(ix+1,iy+1) - dfy2) - $
                          (f (ix  ,iy+1)       ) + $
     (1.-4.*px+3.*px^2) * (fx(ix  ,iy+1) - dfx2)*dx(ix) - $
                (-pyqy) * (fy(ix  ,iy+1) - dfy1) ) )/dx(ix)
	endelse

	if (N_PARAMS() LT 9L) then return, ff
	if KEYWORD_SET(LINEAR) then begin
      dfy    =(qx * (   - (f (ix  ,iy  )       ) + $
                          (f (ix  ,iy+1)       ) ) + $
               px * (   - (f (ix+1,iy  )       ) + $
                          (f (ix+1,iy+1)       ) ) )/dy(iy)
	endif else begin
      dfy    =(qx * (   - (f (ix  ,iy  )       ) + $
        (qy2 - 2.*pyqy) * (fy(ix  ,iy  ) - dfy1) + $
                (-pxqx) * (fx(ix  ,iy  ) - dfx1) + $
                          (f (ix  ,iy+1)       ) + $
        (py2 - 2.*pyqy) * (fy(ix  ,iy+1) - dfy1) + $
                  pxqx  * (fx(ix  ,iy+1) - dfx2) ) + $
               px * (   - (f (ix+1,iy  )       ) + $
        (qy2 - 2.*pyqy) * (fy(ix+1,iy  ) - dfy2) + $
                  pxqx  * (fx(ix+1,iy  ) - dfx1) + $
                          (f (ix+1,iy+1)       ) + $
        (py2 - 2.*pyqy) * (fy(ix+1,iy+1) - dfy2) + $
                (-pxqx) * (fx(ix+1,iy+1) - dfx2) ) )/dy(iy)
	endelse

	return, ff
end
