;
;   Function to find coefficients for the 2nd order polynomium 
;                   f = A*x^2 + B*x + C
;   going through the three points
;           [x(0),f(0)], [x(1),f(1)] and [x(2),f(2)].
;
;   Arguments (defaults in [...]):
;    x:        The three x-points.
;    f:        The three y-points.
;    xi:       x-points for which to evaluate the 2nd order polynomium.
;    AA:       Coefficients for the 2nd order polynomium going through f(x).
;    Returns:  The 2nd order polynomium evaluated at xi, P2(xi).
;
;   Update history:
;   ---------------
;   10.08.2005 Coded/RT
;
;   Known bugs: None
;
function poly2, x, f, xi, AA, dfdx=dfdx
	df12=(f(1)-f(0))/(x(1)-x(0))
	df23=(f(2)-f(1))/(x(2)-x(1))
	A = (df23-df12)/(x(2)-x(0))
	B = .5*(df12+df23 - A*(x(0)+2*x(1)+x(2)))
	C = f(2) - A*x(2)^2 - B*x(2)
	AA = [C, B, A]
;   dfdx = A*(x(1) - .5*(x(0)+x(2))) + .5*(df12+df23)
    dfdx = (df23-df12)/(x(2)-x(0))*(x(2)-x(1)) + df23
	return, A*xi^2 + B*xi + C
;
;  Alternative formulation; AA not equivalent with above definition!
;
;	xx = xi*xi - xi*(x(1)+x(2)) + x(1)*x(2)
;	x10 = x(0)-x(1)
;	x21 = x(1)-x(2)
;	x20 = x10 + x21
;	pp = xx/(x10*x20)
;	pq = (xi-x(2))/x21 - xx*(1./x10+1./x21)/x20
;	qq = xx/(x21*x20) - (xi-x(2))/x21 + 1.
;	AA = [pp, pq, qq]
;	return, pp*f(0) + pq*f(1) + qq*f(2)
;
end
