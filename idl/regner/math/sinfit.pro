;
;   Program to fit the function
;      f(x) = A2*Sin(2\pi*x/A0 + A1) + A5*Sin(2\pi*x/A3 + A4) + A6
;   given f and x. The first guess is based on a Fourier transform of f(x).
;
;   Update history:
;   ---------------
;   23.02.2001 Coded/RT
;   21.04.2004 Wider format for print-out of time/RT
;   29.01.2007 Corrected bug in dimensioning of w/RT
;   26.02.2007 Only print-out the periods of the largest amplitude mode in
;              NONINTERACTIVE mode, and cleverly avoid negative periods/RT
;   26.09.2007 Added keyword ADJUST for adjusting periods of sub-sampled data/RT
;
;   Known bugs: None
;
;----------------------------------------------------------------------------
;   The function (and it's derivatives) to be \chi^2 minimized by calls
;   to curvefit from the main program below
;
pro sinfunct, x, a, f, pder, ADJUST=ADJUST
;The sinus function 'f', we fit to. Fitting with respect to a(0:3)
	a00= 2*!pi*(x(1)-x(0))
	if (KEYWORD_SET(ADJUST) AND a(0) LT a00) then a(0)=a00+abs(a(0))
	Na = N_ELEMENTS(a)
	case (Na) of
	4: begin
		f = a(2)*sin(2*!pi*x/a(0)+a(1)) + a(3)
		pder = [[-a(2)*cos(2*!pi*x/a(0)+a(1))*2*!pi*x/a(0)^2], $	;df/da(0)
				[a(2)*cos(2*!pi*x/a(0)+a(1))], $					;df/da(1)
				[sin(2*!pi*x/a(0)+a(1))], $ 						;df/da(2)
				[replicate(1.0, n_elements(x))]]					;df/da(3)
	   end
	7: begin
		if (KEYWORD_SET(ADJUST) AND a(3) LT a00) then a(3)=a00+abs(a(3))
		f = a(2)*sin(2*!pi*x/a(0)+a(1)) + a(5)*sin(2*!pi*x/a(3)+a(4)) + a(6)
		pder = [[-a(2)*cos(2*!pi*x/a(0)+a(1))*2*!pi*x/a(0)^2], $	;df/da(0)
				[a(2)*cos(2*!pi*x/a(0)+a(1))], $					;df/da(1)
				[sin(2*!pi*x/a(0)+a(1))], $ 						;df/da(2)
				[-a(5)*cos(2*!pi*x/a(3)+a(4))*2*!pi*x/a(3)^2], $	;df/da(3)
				[a(5)*cos(2*!pi*x/a(3)+a(4))], $					;df/da(4)
				[sin(2*!pi*x/a(3)+a(4))], $ 						;df/da(5)
				[replicate(1.0, n_elements(x))]]					;df/da(6)
	   end
	else: print, 'Illegal number of fitting coeffecients!'
	endcase
end
;----------------------------------------------------------------------------
;   The main program
;
pro sinfit, x0, y0, A=A, sigm=sigm, NONINTERACTIVE=NONINTERACTIVE, QUIET=QUIET,$
																	STOP=STOP
	INTERACTIVE = 1
	if KEYWORD_SET(NONINTERACTIVE) then INTERACTIVE=0
	c0 = (!D.TABLE_SIZE-1.)*.8
;
; Check for degeneracy
	nx = N_ELEMENTS(x0)
	if (nx LT 4) then begin
		print, 'Not enough data-points to determine sine-fit.'
		A = replicate(0.0, 7)
		return
	endif
; x0 and y0 might be altered, therefore the copying.
	x = x0
	y = y0
	cleants, x, y
	fdx = total(x(1:*)-x(0:*))/(nx-1.d0)
	dxexp = floor(alog10(fdx)-1)
	dx = 1d1^dxexp*long(fdx/1d1^dxexp + .5)
	xx = dx*dindgen(nx) + x(0)
	yy = intrp1(x, xx, 1.d0*y)
	yRMS = sqrt(total(yy^2)/nx)
	FF = fft(yy) & FF=FF(0:nx/2)
	absF = abs(FF)		  & absF( 0)=0.0
	dummy = max(absF, i1) & absF(i1)=0.0
	dummy = max(absF, i2)
	if (i1 GT i2) then begin
		itmp = i1
		i1   = i2
		i2   = itmp
	endif
	absF = abs(FF)		  & absF( 0)=0.0
	if (INTERACTIVE) then begin
		plot, absF, yr=[-1,1]*max(absF), psym=-2, syms=.2
		oplot, FF, line=1
		oplot, imaginary(FF), line=2
		oplot, [1,1]*i1, !y.crange, line=1, col=c0
		oplot, [1,1]*i2, !y.crange, line=1, col=c0
		answ=prompt('Continue: ')
		plot, x, y							;Plotting the input data-set.
	endif
	if (nx LE 7) then begin
		A = [dx*nx/i1, atan(FF(i1)), 2.*abs(FF(i1)), 0.0]
	endif else begin
		A = [dx*nx/i1, atan(FF(i1),/phase)+!dpi/2., 2.*abs(FF(i1)), $
			 dx*nx/i2, atan(FF(i2),/phase)+!dpi/2., 2.*abs(FF(i2)), 0.0]
	endelse
	if (INTERACTIVE) then begin
		print, ' P/[min.]     Phase   Ampl./[km/s]'
		print, float(A), form='(3F10.6)';Print array of function parameter fits.
	endif
	sinfunct, xx, A, yy0, pder, /ADJUST
;	oplot, xx, yy0, line=2, col=c0;	Plot the initial guess
	niter = 0
	sigm = sqrt(total((yy-yy0)^2)/N_ELEMENTS(yy0))*100.0/yRMS
	w = replicate(1.d0, n_elements(xx))	;The array of weigths (set to 1.0)
	yfit = curvefit(xx,yy,w,A,sigmaa,function_name='sinfunct', $
											iter=niter,tol=1.d-12,itmax=1000)
	if (INTERACTIVE) then begin
		print, niter, sigm, form='(I6, " itterations. sigma=", F8.4,"% :")'
		oplot, xx, yfit, color=c0		;Plotting the fit for the same points.
	endif
	sigm = sqrt(total((yy-yfit)^2)/N_ELEMENTS(yfit))*100.0/yRMS
;
;  Make sure that the first of the two modes has the larger amplitude
	if (N_ELEMENTS(A) EQ 7) then begin
		if (abs(A(5)) GT abs(A(2))) then begin
			tmp = A(0:2)
			A(0:2) = A(3:5)
			A(3:5) = tmp
		endif
	endif
	if (INTERACTIVE) then begin
		print, niter, sigm, form='(I6, " itterations. sigma=", F8.4,"% :")'
		oplot, xx, yfit, color=c0		;Plotting the fit for the same points.
		oplot, !x.crange, [1,1]*A(N_ELEMENTS(A)-1), line=1
		print, "  Final fit:"
		print, A, form='(3F10.6)'		;Print array of function parameter fits.
	endif else begin
		if (NOT KEYWORD_SET(QUIET)) then begin
			iampl = 0
;			if (nx GE 7) then dum=max(abs(A(2+[0,3])),iampl)
			if (nx GE 7) then dum=min(abs(A(0+[0,3])),iampl)
			print, A(iampl*3)*1.3*[1,60./100.], sigm, $
				form='("Period*1.3 = ",F8.2,"/[min.] = ",F10.2,"/[100s], '+$
														'sigma=", F6.2,"%.")'
		endif
	endelse
	if KEYWORD_SET(STOP) then stop
end
