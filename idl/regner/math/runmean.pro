;
;  Update history:
;  ---------------
;  24.01.2000 Coded /RT
;  06.09.2002 Symmetrized cut-off at the boundaries /RT
;             Changed 3.0 -> 2.5 in Ng = ((long(width...  and
;             changed integ_trpz -> total  to be consistent with hydro-code /RT
;  06.04.2004 Introduced keyword for cyclic boundaries./RT
;  03.11.2004 Return the argument unchanged if width < 1.0./RT
;  16.04.2007 Include option for harmonic mean/RT
;  17.04.2007 Ensure long integers as loop-index/RT
;  25.04.2007 Ensure that 3 < Ng < N/2 /RT
;
function runmean, aa, width, Ng=Ng, DEBUG=DEBUG, PRINT=PRINT, CYCLIC=CYCLIC, $
			wtot=wtot, HARMONIC=HARMONIC, EXTRAPOLATE=EXTRAPOLATE, STOP=STOP
	default, width, 11d0
	N   = N_ELEMENTS(aa)
	if (width LT 1.0  OR  N LE 2L) then return, aa
	default, Ng, ((round(width*2.5) > 5L)/2L)*2L+1L
	if KEYWORD_SET(EXTRAPOLATE) then NG = NG < ((N*2L-1L) > 3L)  $
								else NG = NG < (((N/4L)*2L+1L) > 3L)
	if (Ng mod 2 EQ 0) then begin
		print, " WARNING: You are trying to average over an even number"
		print, "          of points, Ng="+string(Ng,form='(I0)')+"."
		Ng = Ng+1L
		print, "          Changing to Ng="+string(Ng,form='(I0)')+"!"
	endif
	Ng2 = (Ng-1)/2
	a   = aa
	if KEYWORD_SET(HARMONIC) then  a=1./a
	xi  = dindgen(Ng) - Ng2
	gauss = exp(-(xi/width*2d0*sqrt(alog(2d0)))^2)
	if KEYWORD_SET(EXTRAPOLATE) then begin
		ig0 = 0L & ig1=Ng-1L
		ii0 = lindgen(Ng)
		C0 = 0d0 & C1=0d0
		x0 = dindgen(Ng2)-Ng2  &  x1=dindgen(Ng2)+1
		a2 = [a(0)+C0*x0, a, a(N-1)+C1*x1]
		C0 = (a(0)-total(a2(ii0)*gauss)/total(gauss))/$
					(total(gauss(0:Ng2-1L)*x0)/total(gauss))
		C1 = (a(N-1L)-total(a2(ii0+N-1L)*gauss)/total(gauss))/$
					(total(gauss(Ng2+1L:*)*x1)/total(gauss))
		a  = [a(0)+C0*x0, a, a(N-1)+C1*x1]
	endif
	b   = dblarr(N)
;   wtot = dblarr(N,N)
	for i=0L, N-1L do begin
		if KEYWORD_SET(CYCLIC) then begin
			i0  = (i-Ng2+N) mod N
			i1  = (i+Ng2+N) mod N
			ig0 = 0L
			ig1 = Ng-1L
			ii  = (i0 + lindgen(Ng)) mod N 
		endif else begin
			if KEYWORD_SET(EXTRAPOLATE) then begin
				ii  = i + ii0
			endif else begin
				i0  = (i-Ng2) > 0L
				i1  = (i+Ng2) < (N-1L)
				di  = min([i-i0, i1-i])
				ig0 = Ng2-di & ig1 = Ng2+di
				i0  = i-di   & i1  = i+di
				ii  = i0 + lindgen(i1-i0+1L)
			endelse
		endelse
		if KEYWORD_SET(DEBUG) then print, i, i0, i1, ig0, ig1, form='(5I6)'
		if KEYWORD_SET(DEBUG) then print, gauss(ig0:Ng2)/total(gauss(ig0:ig1)),$
														form='(7(e14.8,:,","))'
		b(i) = total(gauss(ig0:ig1)*a(ii))/total(gauss(ig0:ig1))
;       wtot(i,ii) = wtot(i,ii) + gauss(ig0:ig1)/total(gauss(ig0:ig1))
	endfor
	if KEYWORD_SET(PRINT) then begin
		for i=1L, Ng2 do begin
			stra = string(gauss(Ng2-i:Ng2)/total(gauss(Ng2-i:Ng2+i)), $
								form='("     &        ",6(e14.8,:,","))')
			print, str_sep(stra, 'e-0'), form='(10(A0,:,"e-"),$)'
;			print, gauss(Ng2-i:Ng2)/total(gauss(Ng2-i:Ng2+1)), $
;								form='("     &        ",6(e14.8,:,","),$)'
			if (i LT Ng2) then begin
				print, replicate(",0.0e0        ",Ng2-i), form='(10A0,$)'
				print, ","
			endif else begin
				print, "/"
			endelse
		endfor
	endif
	if KEYWORD_SET(STOP) then stop
	if KEYWORD_SET(HARMONIC) then  return, 1./b  else  return, b
end
