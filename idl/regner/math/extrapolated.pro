;
; function to determine whether a point, (x0,y0), is outside the borders,
; (x,y), of a triangulation.
;
;   Update history:
;   ---------------
;   16.10.2007 Coded/RT
;
;   Known bugs: None
;
function extrapolated, x, y, x0, y0, dx, dy, DEBUG=DEBUG, PLOT=PLOT, STOP=STOP
	Nb = N_ELEMENTS(x)
	ib = [lindgen(Nb), 0]
	if KEYWORD_SET(PLOT) then begin
		plot, x(ib), y(ib), psym=-2, ysty=1, xr=[-1,1]*3.3, yr=[-1,1]*3.3
		plots, x0, y0, psym=2
	endif
	extrap = 0b
	dx = 0.0 & dy = 0.0
	for i1=0L, Nb-1L do begin
		i2 = (i1 + 1L) mod Nb
		d  = ((x(i2)-x(i1))*(y(i1)-y0)-(x(i1)-x0)*(y(i2)-y(i1)))$
			/sqrt((x(i2)-x(i1))^2 + (y(i2)-y(i1))^2)
; This is copied from 
		l0 = [x(i1), y(i1)]
		lv = [x(i2)-x(i1), y(i2)-y(i1)]
		l = sqrt(total(lv*lv))
		lv = lv / l			; Normal to line
		t = - (-total(lv * [x0,y0]) + total(l0 * lv))/(total(lv * lv))
		pl = t * lv + l0	; Closest point on line
		if (d GT 0.0) then begin
			dx = x0-pl(0)
			dy = y0-pl(1)
			extrap = 1b
		endif

		if KEYWORD_SET(DEBUG) then begin
			if (d GT 0.0) then print, d, dx, dy, form='(3f10.5,$)' $
						  else print, d, 		 form= '(f10.5,$)'
		endif
		if KEYWORD_SET(PLOT) then begin
			plots, x([i1,i2]), y([i1,i2]), col=(!D.TABLE_SIZE-1.)*.7
			answ = prompt(" Next borderline? [y/n/q/s] : ")
			if (answ EQ 'n'  OR  answ EQ 'r'  OR answ EQ 'q') then return,extrap
			if (answ EQ 's')                                  then stop
			plots, x([i1,i2]), y([i1,i2])
			if (d GT 0.0) then plots, [x0,pl(0)], [y0,pl(1)], psym=-2, syms=.5
		endif else if KEYWORD_SET(DEBUG) then print, ''
	endfor
	if KEYWORD_SET(STOP) then stop

	return, extrap
end
