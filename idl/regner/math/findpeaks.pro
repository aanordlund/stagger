;
;   Program to find peaks in a supplied function, f.
;
;   Arguments (defaults in [...]):
;    f:       The function to find peaks in.
;    ipks:    Array of indexes of the peak locations, sorted left-to-right.
;    ithr:    Array of indexes of the through locations, sorted left-to-right.
;    QUIET:   Suppress diagnostic messages, but still print error messages.
;    STOP:    Set this keyword to stop before returning, to make all
;             variables available to the user (for debugging).
;
;   Update history:
;   ---------------
;   10.12.2010 Coded/RT
;   15.09.2017 Added keyword QUIET, passed on to s/r monotonic/RT
;
;   Known bugs: Assumes end-points to be throughs.
;
pro findpeaks, f, ipks, ithr, Ampl, QUIET=QUIET, STOP=STOP
    Nf   = N_ELEMENTS(f)
    ipks = [-1] & ithr=[0L]
    i0   = 1L
    repeat begin
        monotonic, f, i0, i1, i2, /MAXIMUM, QUIET=QUIET
        if (i0 GE 0L) then begin
            ipks = [ipks, i0]
            ithr = [ithr, i2]
        endif
        i0   = i2+2L
    endrep until (i2 GE (Nf-2L))
    ipks = ipks(1:*)
    Npks = N_ELEMENTS(ipks)
    if (ithr(Npks) LE (Nf-2L)) then begin
        if (f(ithr(Npks)) GT f(ithr(Npks)+1L)) then ithr(Npks)=ithr(Npks)+1L
    endif
    for i=0L, Npks-1L do begin
        ii = where(ithr EQ ipks(i))
        if (ii(0) GE 0L) then begin
            ithr(ii) = -1L
            ipks(i)  = -1L
        endif
    endfor
    ithr = ithr(where(ithr GE 0L))
    ipks = ipks(where(ipks GE 0L))
    if (abs(f(ithr(0))) LT 1e-6*abs(f(ipks(0)))) then begin
        ith0 = max(where(f(0:ipks(0)) LT 1e-6*f(ipks(0))))
        if (ith0 GE 0L) then  ithr(0) = ith0
    endif
    Ampl = interpol(f(ithr), ithr, ipks)
    Ampl = f(ipks) - Ampl

    if KEYWORD_SET(STOP) then stop
end
