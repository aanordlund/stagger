;***********************************************************************
      FUNCTION intrp1,z,zt,f,h=h,d=d,EXTRAPOLATE=EXTRAPOLATE,LINEAR=LINEAR, $
																MCUBIC=MCUBIC
;
;  Interpolate to a new z-grid.
;   EXTRAPOLATE:  Set this keyword to perform linear extrapolation.
;   LINEAR:       Set this keyword to perform linear interpolation.
;  1D version
;
;  Update history:
;
;  22-dec-87/aake:  /czintr/ added to control extrapolation (not used
;                   in simulation code, to be used in model resizing.
;  08-mar-88/bob:   1D version
;     future/aake:  Need to initialize the common block with block data
;  01-dec-88/bob:   idl proceedure
;  02-dec-88/aake:  Simplified where loop
;  07-oct-98/art:	reverse arrays if monotonic decreasing.
;  24-feb-04/art:	added calculation of derivatives of the interpolant
;  14-dec-07/art:   added keyword MCUBIC, for derivatives that give
;                   monotonic cubic interpolants.
;  13-sep-17/art:   Fixed constant extrapolation for LINEAR case, when
;                   EXTRAPOLATE is not set.
;
;***********************************************************************
;
      nz=n_elements(z)
      nt=n_elements(zt)
      mz=n_elements(f)
;
;  Get derivatives of f
;
	  z = reform(z)
	  f = reform(f)
	  if (KEYWORD_SET(LINEAR) OR nz LT 3) then begin
;		print, 'Interpol:'
;		help, f,z,zt
		if (nz LT 2) then begin
            g = replicate(f(0), nt)
			d = [0.0]
		endif else begin
            if KEYWORD_SET(EXTRAPOLATE) then begin
                g = interpol(f,z,zt)
            endif else begin
                g = 0*zt
                minz  = min(z, imn)&   maxz  = max(z, imx)
                ii    = where(zt GE minz  AND  zt LE maxz)
                g(ii) = interpol(f,z,zt(ii))
                ii    = where(zt LT minz)
                if (ii(0) GE 0L) then g(ii)=f(imn)
                ii    = where(zt GT maxz)
                if (ii(0) GE 0L) then g(ii)=f(imx)
            endelse
			d = (f(1:*)-f)/(z(1:*)-z)
			d = [d(0),d]
		endelse
		if (nt EQ 1) then return, g(0)  else   return,g
	  endif
	  rev = 0
	  if (z(nz-1) LT z(0)) then begin
		rev = 1
		z = reverse(z)
		f = reverse(f)
	  endif
	  if KEYWORD_SET(MCUBIC) then default, d, derfm(z,f)  $
      						 else default, d, derf(z,f)
;     print,'f=',f
;     print,'z=',z
;     print,'d=',d
;
;  Find n such that  z(n-1) < zt(k) < z(n)
;
      n=1
      for k=0L,nt-1 do begin
        n1=where(z gt zt(k),count)
	    if count gt 0 then n=[n,n1(0)] else n=[n,nz-1]
      endfor
      n=n(1:*)>1
;     print,'n=',n
      nm1=n-1
;     print,nm1
;
;  Relative distances
;  Set to constant outside
;
      dz=z(n)-z(nm1)
;     print,'dz=',dz
      pz=(zt-z(nm1))/dz
;
;  Extrapolate ?
;
      if (NOT KEYWORD_SET(EXTRAPOLATE)) then pz=((pz<1)>0)
;     print,'pz=',pz
      qz=1.-pz
;
;  Interpolate
;
      pzf= pz-(qz*pz)*(qz-pz)*(pz LE 1.0  AND  pz GE 0.0)
      pzd=-pz*(qz*pz)*dz*(pz LE 1.0  AND  pz GE 0.0)
      qzd= qz*(qz*pz)*dz*(pz LE 1.0  AND  pz GE 0.0)
      g=f(nm1)+pzf*(f(n)-f(nm1))+qzd*d(nm1)+pzd*d(n)
	  df = (f(n)-f(nm1))/dz
;	  d1 = d(nm1) - df
;	  d2 = d(n)   - df
;	  h=df + d2*(z(nm1)/dz)^2 + 2*(d1+d2)*z(nm1)*z(n)/dz^2 + d1*(z(n)/dz)^2 $
;		- 2/dz^2*((d1+2*d2)*z(nm1) + (2*d1+d2)*z(n))*zt + 3/dz^2*(d1+d2)*zt^2 
	  h=df*(1-3*(pz^2+qz^2)+2*(pz+qz)^2) - d(nm1)*qz*(2*pz-qz) + d(n)*pz*(pz-2*qz)
;     print,'g=',g
;
;  Restore
;
	  if (rev EQ 1) then begin
		z = reverse(z)
		f = reverse(f)
;		g = reverse(g)
		d = reverse(d) 
	  endif
	  if (nt EQ 1) then return, g(0)  else   return,g
      end
