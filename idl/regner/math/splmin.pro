;
;   splmin finds the minimum of a spline defined by x,f and optionally d=df/dx.
;   The result is in general located between the grid-points, x, defining the
;   spline, and is therefore much more accurate than the index of the min(f).
;
;   Update history:
;   ---------------
;   16.05.1999 Coded/Aake
;   12.07.2002 Include capability of handling decreasing x-scales/RT
;   19.05.2004 Corrected treatment of decreasing x-scales/RT
;   14.06.2006 Supply default xmin in case of premature return/RT
;   15.06.2006 Corrected (kmin EQ n-1) -> (kmin GE n-1) /RT
;   02.10.2006 Only perform spline calculation if n>=3/RT
;
pro splmin,x,f,xmin,fmin,d=d
;
      n=n_elements(x)
	  fmin = min(f, kmin) & xmin = x(kmin)
	  if (n LT 3L) then return
;
	  if (N_ELEMENTS(d) LE 0) then d=derf(x,f)
	  dk = long((x(1)-x(0))/abs(x(1)-x(0)))
	  if (long(total((d(0)*d GT 0))) EQ n) then begin
;		print, "  WARNING - No minimum!"
		return
	  endif
      if ( kmin LE 0 ) then return
      if ( kmin GE n-1 ) then return
      if ( d(kmin) GT 0.) then kmin=(kmin-dk)>0L
      if ( d(kmin) GT 0.) then return
;
      dx=x(kmin+dk)-x(kmin)
      a=3.*((d(kmin)+d(kmin+dk))*dx-2.*(f(kmin+dk)-f(kmin)))
      b= (2.*d(kmin)+d(kmin+dk))*dx-3.*(f(kmin+dk)-f(kmin))
      c=d(kmin)*dx
      e=1.e-3*b^2
      if (abs(a*c) LE e) then begin
        p=.5*c/b
      endif else begin
        p=(b+sqrt(b^2-a*c))/a
      endelse
      xmin=x(kmin)+p*dx
;
      a=d(kmin)*dx   -(f(kmin+dk)-f(kmin))
      b=d(kmin+dk)*dx-(f(kmin+dk)-f(kmin))
      fmin=f(kmin)*(1.-p)+f(kmin+dk)*p+p*(1.-p)*(a*(1.-p)-b*p)
;
end
