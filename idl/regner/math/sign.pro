function sign,f,c
	default, c, 1
	return,c*f/(abs(f)+1.e-30)
end
