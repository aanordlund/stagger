;
;   Program to compute bi-cubic convolution weights, for 2-D interpolation.
;   This is the method used by IDL's congrid, /CUBIC.
;   A detailed description can be found on pp.28-31 of
;       ftp://earth1.esrin.esa.it/pub/stb_ftp/asd_26-27.pdf
;   which was found at:
;		http://www.quantdec.com/SYSEN597/GTKAV/section9/map_algebra.htm
;   (There is a typographic error in the formula for the cubic convolution
;   curve; the coefficient "-5" should be "-5a").
;    The input grid is assumed rectangular, but can be irregular along
;   either axis.
;
;   Arguments (defaults in [...]):
;    f:       The Nx*Ny array of data to be interpolated.
;    fx:      The Nx array of x-coordinates of f.
;    fy:      The Ny array of y-coordinates of f.
;    fxi:     The x-coordinates where f is interpolated to.
;    fyi:     The y-coordinates where f is interpolated to.
;    fi:      The interpolated values  ~ f(xi, yi).
;    a:       The parameter of the cubic convolution a=[-1;0] which results in
;             interpolations going from convex to concave [-.5].
;    extrapolation: =0 to set zero outside, =1 for constant extrapolation [0].
;    STOP:    Set this keyword to stop before returning, to make all
;             variables available to the user (for debugging).
;
;   Update history:
;   ---------------
;   26.09.2008 Coded/RT
;   29.09.2008 Now using the x- and y-coordinates of the input and output
;              arrays, instead the indices of them. Added extrapolation/RT
;
;   Known bugs: None
;
pro bicubicconv, f, fx, fy, fxi, fyi, fi, a=a, extrapolation=extrapolation, $
																	STOP=STOP
	default, a, -.5d0
	default, extrapolation, 0d0
;								Zero outside domain		Constant extrapolation
	ex0 = -1d0 + extrapolation;			-1							0
	ex1 =  4d0 - extrapolation;			 4							3
	Nx  = N_ELEMENTS(f(*,0))
	Ny  = N_ELEMENTS(f(0,*))
	fdx = fx(1:*) - fx
	fdy = fy(1:*) - fy
	x   = dindgen(Nx)
	y   = dindgen(Ny)
	Nxi = N_ELEMENTS(fxi(0,*))
	Nyi = N_ELEMENTS(fyi(*,0))
	ix  = 0*long(fxi)
	for i=0L, Nx-1L do begin
		ii = where(fxi GT fx(i))
		if (ii(0) GE 0L) then ix(ii) = i
	endfor
	iy  = 0*long(fyi)
	for j=0L, Ny-1L do begin
		ii = where(fyi GT fy(j))
		if (ii(0) GE 0L) then iy(ii) = j
	endfor
; 1st x-point in the 4-point stencil around each xi
	kx0 = ((ix-1L)>0L) < (Nx-4L)
	xi  = ix + (fxi - fx(ix))/fdx(ix<(Nx-1L))
; 1st y-point in the 4-point stencil around each yi
	ky0 = ((iy-1L)>0L) < (Ny-4L)
	yi  = iy + (fyi - fy(iy))/fdy(iy<(Ny-1L))
	dx0 = (xi - kx0)
;	dy0 = ((yi - ky0) > ex0) < ex1
	dy0 = (yi - ky0)
	R   = dblarr(Nxi, Nyi, 4L, 4L)
	R1  = dblarr(Nxi, Nyi)
	a2  = a + 2d0
	a3  = a + 3d0
	a4  = a*4d0
	a5  = a*5d0
	a8  = a*8d0

	ex  = 1d0/(3d0*extrapolation^2)
	iln0 = where(dx0 LT 1d0)
	iln1 = where(dx0 GT 2d0)
	for i=0L, 3L do begin
		R1(*,*) = 0d0
		dx  = abs(dx0 - i)
;		print, min(dx, max=maxdx), maxdx
		i01 = where(dx LE 1d0)
		if (i01(0) GE 0L) then begin
			R1(i01) = (a2*dx(i01) - a3)*dx(i01)*dx(i01) + 1d0
		endif
		i12 = where(dx GT 1d0  AND  dx LE 2d0)
		if (i12(0) GE 0L) then begin
			R1(i12) = ((a*dx(i12) - a5)*dx(i12) + a8)*dx(i12) - a4
		endif
		if (iln0(0) GE 0L) then begin
;			px =   (dx0(iln0)) >(-extrapolation)&  px=px-ex*px^3
			px = (-(dx0(iln0)-1.))<extrapolation&  px=px-ex*px^3
			if (i EQ 0L) then R1(iln0) =     px
			if (i EQ 1L) then R1(iln0) = 1d0-px
			if (i GE 2L) then R1(iln0) = 0d0
		endif
		if (iln1(0) GE 0L) then begin
			px = (dx0(iln1)-2.)<extrapolation&  px=px-ex*px^3
			if (i LE 1L) then R1(iln1) = 0d0
			if (i EQ 2L) then R1(iln1) = 1d0-px
			if (i EQ 3L) then R1(iln1) =     px
		endif
		for j=0L, 3L do  R(*,*, i, j) = R1
	endfor
		
	iln0 = where(dy0 LT 1d0)
	iln1 = where(dy0 GT 2d0)
	for j=0L, 3L do begin
		R1(*,*) = 0d0
		dy  = abs(dy0 - j)
		i01 = where(dy LE 1d0)
		if (i01(0) GE 0L) then begin
			R1(i01) = (a2*dy(i01) - a3)*dy(i01)*dy(i01) + 1d0
		endif
		i12 = where(dy GT 1d0  AND  dy LE 2d0)
		if (i12(0) GE 0L) then begin
			R1(i12) =  ((a*dy(i12) - a5)*dy(i12) + a8)*dy(i12) - a4
		endif
		if (iln0(0) GE 0L) then begin
			py = (-(dy0(iln0)-1.))<extrapolation&  py=py-ex*py^3
			if (j EQ 0L) then R1(iln0) =     py
			if (j EQ 1L) then R1(iln0) = 1d0-py
			if (j GE 2L) then R1(iln0) = 0d0
		endif
		if (iln1(0) GE 0L) then begin
			py = (dy0(iln1)-2.)<extrapolation&  py=py-ex*py^3
			if (j LE 1L) then R1(iln1) = 0d0
			if (j EQ 2L) then R1(iln1) = 1d0-py
			if (j EQ 3L) then R1(iln1) =     py
		endif
		for i=0L, 3L do  R(*,*, i, j) = R(*,*, i, j) * R1
	endfor
	fi   = dx0*0
	for j=0L, 3L do begin
		for i=0L, 3L do begin
			fi = fi + R(*,*,i,j)*f(kx0+i, ky0+j)
		endfor
	endfor

	if KEYWORD_SET(STOP) then stop
end
