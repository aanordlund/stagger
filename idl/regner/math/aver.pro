FUNCTION aver,a,axis
;+
; FUNCTION aver,a
;  Calculate the average.  Avoid accumulation roundoff by
;  using partial sums if the number of elements is larger than
;  a given number (1000).
;-
  if (N_ELEMENTS(axis) LE 0) then begin
    a=reform(a)
    n=n_elements(a)
    nmax=1000L

    if (n GT nmax) then begin
      ni=n/nmax
      sum=0.
      for i=0L,ni-1L do $
        sum=sum+total(a(i*nmax:i*nmax+nmax-1))
      if (ni*nmax LT n) then sum=sum+total(a(ni*nmax:*))
      return,sum/n

    end else begin
      return,total(a)/n

    end
  end else if (N_ELEMENTS(axis) EQ 1) then begin
    s=size(a)
    return,total(a,axis)/s(axis)
  end else if (N_ELEMENTS(axis) EQ 2) then begin
    s=size(a)
    if (axis(1) gt axis(0)) then begin
      return,total(total(a,axis(0)),axis(1)-1)/(s(axis(0))*s(axis(1)))
    end else begin 
      return,total(total(a,axis(1)),axis(0)-1)/(s(axis(0))*s(axis(1)))
    end
  end else begin
    print,'cannot do more than 2 axes yet'
    return,0
  end
END
