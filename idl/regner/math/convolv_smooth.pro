;
;   Function to smooth y(x) by convolving with a smoothing kernel,
;   which can be either a Lorentzian or a Gaussian of width, w. 
;   Integrate over the range +/-wc, depending on the kernel.
;     Lines that are outcommented with ";P" plot the smoothing kernels
;   for lambda= 8, 9, 10, 11, 12\AA when used with
;   IDL> opsum, 6.35, 22.5, /BAILEY
;   which is saved to  /texts/articles/dkappa/figs/opsumBaileyFig3cmp.ps
;
;   Arguments (defaults in [...]):
;    x:       x-coordinate of y(x) to smooth.
;    y:       y-coordinate of y(x) to smooth.
;    w:       FWHM of smoothing kernel (in x-units).
;    shape:   Functional form of kernel. So far only 'Lorentz' or ['Gauss'].
;    INTEGRAL:Set this keyword to perform cubic spline integration over kernel.
;    DOPLOT:  Set this keyword to oplot kernels.
;    STOP:    Set this keyword to stop before returning, to make all
;             variables available to the user (for debugging).
;
;   Update history:
;   ---------------
;   09.07.2015 Coded/RT
;   18.09.2017 Fixed bug at end-points. Now performs symmetric truncation
;              in x (not index), which means smaller effective smoothing
;              width towards end-points. Added keyword DOPLOT to plot kernels/RT
;
;   Known bugs: The first 2*w+1 points are left unchanged!
;
function florentz, x, w
; No need to normalize by 2\pi, since numerical normalization is
; needed anyway due to the incomplete range of x.
    return, w/(x^2+(w/2.)^2)
end

function fgauss, x, w
    return, exp(-(x/w)^2)
end

function convolv_smooth, x, y, w, shape, $
                                INTEGRAL=INTEGRAL, DOPLOT=DOPLOT, STOP=STOP
@consts
    uull = 1d8*h*vc/k/1d1^6.35d0
    default, shape, 'Gauss'
    shp = strlowcase(strmid(shape, 0, 3))
    case shp of
        'lor': begin& kfunc='florentz'& wc=8.0*w & c1=!P.color              &end
        'gau': begin& kfunc='fgauss'  & wc=2.0*w & c1=0.6*(!D.TABLE_SIZE-1.)&end
        else:  begin
                 print, "ERROR: The shape, " + shape + $
                             ", is not yet recognized by convolv_smooth.pro."
                 print, "       Bailing out!"
                 return, -1
               end
    endcase
;P
;   ll   = uull/x& lplt = [8., 9., 10., 11., 12.]
;   dum  = min(abs(ll-lplt(0)), imn)& iplt=[imn]
;   for i=1L,4L do begin& dum=min(abs(ll-lplt(i)),imn)& iplt=[iplt,imn]& endfor
;P
    Nx   = N_ELEMENTS(x)
    xmin = min(x, max=xmax)
    dx   = deriv(findgen(Nx), x)

    i1 = min(where((x-xmin) GT wc))
    i2 = max(where((xmax-x) LT wc))
    sy = y
    delx  = min([[x-xmin < wc], [xmax-x < wc]], dim=2)
    yr = !y.crange
    for i=1L, Nx-2L do begin
        ii    = where(abs(x-x(i)) LE delx(i))
        Nii   = N_ELEMENTS(ii)
        if (Nii GE 3L) then begin
            xx    = x(ii) - x(i)
            krnl   = call_function(kfunc, xx, w)
            if KEYWORD_SET(INTEGRAL) then begin
                sy(i) = integf(xx, krnl*y(ii),/TOT)/integf(xx, krnl,/TOT)
            endif else begin
                sy(i) = total(dx(ii)*krnl*y(ii))/total(dx(ii)*krnl)
            endelse
            if (KEYWORD_SET(DEBUG)  AND  (i mod 10L) EQ 0) then $
                oplot,xx+x(i),krnl/max(krnl)*(yr(1)-yr(0))*.5+yr(0),col=60
;P
;           if (where(iplt EQ i) GE 0L) then $
;               oplot, ll(ii), krnl/max(krnl)*.3+1., col=c1
;P
        endif
    endfor

    if KEYWORD_SET(STOP) then stop
    return, sy
end
