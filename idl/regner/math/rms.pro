;
;   Function to return the root-mean-squared value of the input array, f.
;                 ____________
;     RMS(f) =   / Sum[f^2]/N
;               `
;   Arguments (defaults in [...]):
;    f:       Array to calculate the RMS for.
;    i:       Dimension over which to perform the RMS.
;    average: The average, over dimension i, if given.
;    weight:  The weight for each entry in f [1.0].
;    NORMALIZE:   Set this keyword to normalize the RMS by the average value.
;    FLUCTUATION: Set this keyword to compute the RMS of fluctuations around
;                 the average.
;    STOP:    Set this keyword to stop before returning, to make all
;             variables available to the user (for debugging).
;
;   Update history:
;   ---------------
;   03.05.1994 Coded/RT
;   14.06.2019 The calculation of RMS and averages did not propoerly include
;              possible weights, now fixed./RT
;
;   Known bugs: None
;
function rms,f,i,average=average, weight=weight, NORMALIZE=NORMALIZE, $
                                            FLUCTUATION=FLUCTUATION, STOP=STOP
	default, weight, 0*f+1
	sf = size(f)
	if (sf(0) GT 1  AND  N_ELEMENTS(i) GT 0) then begin
        averw  =aver(weight,  i)
		average=aver(weight*f,i)/averw
		if (i LT 1) then begin& print, "i<1 doesn't make sense."& stop& endif
		if keyword_set(fluctuation) then begin
			if (i EQ 1) then aver2 = replicate(1.0, sf(i))#average
			if (i EQ 2) then aver2 = average#replicate(1.0, sf(i))
		endif
		if (i GT 2) then begin& print, "Can't handle arrays of more than two"+$
								" dimensions"& stop& endif
		if keyword_set(normalize) then $
			if keyword_set(fluctuation) then $
				RMSf = sqrt(aver(weight*(f-aver2)^2,i)/averw)/average $
			else $
				RMSf = sqrt(aver(weight*f^2,i)/averw)/average $
		else $
			if keyword_set(fluctuation) then $
				RMSf = sqrt(aver(weight*(f-aver2)^2,i)/averw) $
			else $
				RMSf = sqrt(aver(weight*f^2,i)/averw)
	endif else begin
        averw  =aver(weight)
		average=aver(f*weight)/averw
		if keyword_set(NORMALIZE) then $
			if keyword_set(FLUCTUATION) then $
				RMSf = sqrt(aver(weight*(f-average)^2)/averw)/average $
			else $
				RMSf = sqrt(aver(weight*f^2)/averw)/average $
		else $
			if keyword_set(FLUCTUATION) then $
				RMSf = sqrt(aver(weight*(f-average)^2)/averw) $
			else $
				RMSf = sqrt(aver(weight*f^2)/averw)
	endelse

    if KEYWORD_SET(STOP) then stop
    return, RMSf
end
