;
;   Program to find the limits to monotonic increase/decrease of an
;   array, f, away from the point, f(i0).
;   Arguments:
;    f:        The input array
;    i0:       The starting point
;    i1:       The starting point of the monotonic region, [i1; i0]
;    i2:       The end point of the monotonic region, [i0; i2]
;    MINIMUM:  If f(i0) is not a local extremum, chase down the missing
;              branch of the minimum, and reset i0.
;    MAXIMUM:  If f(i0) is not a local extremum, chase down the missing
;              branch of the maximum, and reset i0.
;    QUIET:    Suppress diagnostic messages, but still print error messages.
;    STOP:     If this keyword is set, stop before returning
;
;   Update history:
;   ---------------
;   25.01.2008 Coded/RT
;   23.08.2013 Added option for robust extrema at the ends of monotonic
;              regions. Small extrema < eps will be ignored./RT
;   08.02.2016 Repaired behaviour when i0 is accidentally a local extremum./RT
;   12.09.2017 Added a check for  (i0 LT (N-1L))  around the
;              keyword_set(MINIMUM)- and keyword_set(MAXIMUM)-blocks./RT
;   15.09.2017 Added keyword QUIET, to suppress diagnostic messages/RT
;
;   Known bugs: Only works for N>2. Should work for N=1 too.
;
;%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
;
;   Routine to search in one direction, di, for end of monotonicity of f,
;   from index, i.
;
pro search, f, i, di, N
  i0 = i
  if (di LT 0L) then begin
; Search to the left
    if (i LE 0L) then return
    if (f(i+di) GT f(i0)) then begin
; Decreasing
        while (f((i+di)>0L) GT f(i) AND i GT 0L)     do  i=i+di
    endif else begin
; Increasing
        while (f((i+di)>0L) LT f(i) AND i GT 0L)     do  i=i+di
    endelse
  endif else begin
; Search to the right
    if (i GE (N-1L)) then return
	if (f(i+di) LT f(i0)) then begin
; Decreasing
		while (f((i+di)<(N-1L)) LT f(i) AND i LT (N-1L)) do  i=i+di
	endif else begin
; Increasing
		while (f((i+di)<(N-1L)) GT f(i) AND i LT (N-1L)) do  i=i+di
	endelse
  endelse
end

pro monotonic, f, i0, i1, i2, eps=eps, $
        MINIMUM=MINIMUM, MAXIMUM=MAXIMUM, DEBUG=DEBUG, QUIET=QUIET, STOP=STOP
    default, eps, 0.0
    eps  = abs(eps)
    eps2 = eps/5.
    if KEYWORD_SET(QUIET) then LOUD=0b  else  LOUD=1b

	N  = N_ELEMENTS(f)
    i0in = i0
	i0 = (i0in>1L)<(N-2L)
;
; To the left of i0
	i1 = i0
    search, f, i1, -1L, N
    f10 = f(i1)
    if (eps GT 0.0) then begin
        ii2 = i1
        if (f(i1) GT f(i0)) then   sl=-1.0  else  sl=+1.0
        repeat begin
            i1  = ii2
            if (sl*f(i1) LT sl*f10) then f10 = f(i1)
            ii1 = i1
; Is this only a short counter-trend?
            search, f, ii1, -1L, N
            ii2 = ii1
            search, f, ii2, -1L, N
            if (LOUD) then $
                print, " A minor extremum to the left? ",f(ii1)-f(i1), eps
        endrep until (abs(f(i1)-f(ii1)) GT eps  OR  i1 LE 0L  $
                                                OR  sl*f(ii2) GT sl*(f10+eps2))
    endif
;
; To the right of i0
	i2 = i0
    search, f, i2, +1L, N
    f20 = f(i2)
    if (eps GT 0.0) then begin
        ii2 = i2
        if (f(i2) GT f(i0)) then   sl=+1.0  else  sl=-1.0
        repeat begin
            i2  = ii2
            if (sl*f(i2) GT sl*f20) then f20 = f(i2)
            ii1 = i2
; Is this only a short counter-trend?
            search, f, ii1, +1L, N
            ii2 = ii1
            search, f, ii2, +1L, N
            if (LOUD) then $
                print, " A minor extremum to the right? ",f(ii1)-f(i2), eps
        endrep until (abs(f(i2)-f(ii1)) GT eps  OR  i2 GE (N-1L)  $
                                                OR  sl*f(ii2) LT (sl*f20-eps2))
    endif
;
; If i0 did not correspond to an extremum, go find it...
	if ((f(i0+1)-f(i0))*(f(i0)-f(i0-1)) GT 0) then begin
;
; Chase a local minimum
		if KEYWORD_SET(MINIMUM) then begin
			if (f(i1) GT f(i2)) then begin
; Found the decreasing side above - now find the increasing side on the right
				i0 = i2
                search, f, i2, +1L, N
			endif else begin
; Found the increasing side above - now find the decreasing side on the left
				i0 = i1
                search, f, i1, -1L, N
			endelse
		endif
;
; Chase a local maximum
		if KEYWORD_SET(MAXIMUM) then begin
			if (f(i1) GT f(i2)) then begin
; Found the decreasing side above - now find the increasing side on the left
				i0 = i1
				search, f, i1, -1L, N
			endif else begin
; Found the increasing side above - now find the decreasing side on the right
				i0 = i2
                search, f, i2, +1L, N
			endelse
		endif
	endif
;
  if (i0 LT (N-1L)) then begin
    if (KEYWORD_SET(MAXIMUM))then begin
      if (f(i0) LT f(i0+1)) then begin
; Do we ever get here?
        if (LOUD) then $
            print, " How did we get to this point, looking for a maximum?"
        if ((f(i0+1)-f(i0))*(f(i0)-f(i0-1)) LT 0.0) then begin
            if (LOUD) then $
                print, " We started in an extremum - that's how!"
            if (f(i0-1) LT f(i0)  AND  f(i0+1) LT f(i0)) then begin
                if (LOUD) then $
                    print, " This is a maximum - we are done!"
                if KEYWORD_SET(STOP) then stop
                return
            endif else begin
                if (LOUD) then $
                    print, " This is a minimum - Let's find the closest maximum."
                if ((i0-i1) LT (i2-i0)) then begin
; Let's look for the left branch
                    i2 = i0
                    i0 = i1
                    search, f, i1, -1L, N
                endif else begin
; Let's look for the right-side branch
                    i1 = i0
                    i0 = i2
                    search, f, i2,  1L, N
                endelse
            endelse
        endif else begin
            print, "WARNING: This is not a local extremum, after all..."
            print, "         Returning i0 = -1!"&   i0=-1L
        endelse
      endif
    endif

    if (KEYWORD_SET(MINIMUM)) then begin
      if (f(i0) GT f(i0+1)) then begin
; Do we ever get here?
        if (LOUD) then $
            print, " How did we get to this point, looking for a minimum?"
        if ((f(i0+1)-f(i0))*(f(i0)-f(i0-1)) LT 0.0) then begin
            if (LOUD) then $
                print, " We started in an extremum - that's how!"
            if (f(i0-1) GT f(i0)  AND  f(i0+1) GT f(i0)) then begin
                if (LOUD) then $
                    print, " This is a minimum - we are done!"
                if KEYWORD_SET(STOP) then stop
                return
            endif else begin
                if (LOUD) then $
                    print, " This is a maximum - Let's find the closest minimum."
                if ((i0-i1) LT (i2-i0)) then begin
; Let's look for the left branch
                    i2 = i0
                    i0 = i1
                    search, f, i1, -1L, N
                endif else begin
; Let's look for the right-side branch
                    i1 = i0
                    i0 = i2
                    search, f, i2,  1L, N
                endelse
            endelse
        endif else begin
            print, "WARNING: This is not a local extremum, after all..."
            print, "         Returning i0 = -1!"&   i0=-1L
        endelse
      endif
    endif
  endif
	
	if KEYWORD_SET(STOP) then stop
	return
end
