;
;   Function to perform bicubic interpolation of f at (x,y) given
;   f and its differentials fx=df/dx*\Delta x  and fy=df/dy*\Delta y,
;   as well as the regular and rectangular grid-points of f; xf and yf.
;     The interpolations of this routine are consistent with
;   OS2code/lookup.f, lookup_eos.pro and math/bicubic.pro to within
;   +/-6e-6 as tested on lnT of sune42.scr, on 19.05.2009.
;
;   Update history:
;   ---------------
;   01.05.2006 Coded/RT
;   14.05.2008 Added option to perform bi-linear interpolation/RT
;   16.05.2008 Added posibillity of calculating derivatives, dfx and dfy, at
;              the interpolation points/RT
;   03.03.2009 Re-ordered some terms for consistency - no change in results/RT
;
;   Known bugs: Assuming regular and rectangular grid for f.
;
function bicubic, x, y, f, xf, yf, fx, fy, dfx, dfy, LINEAR=LINEAR, GRID=GRID, $
																	STOP=STOP
;
; Dimensions
	Nx = N_ELEMENTS(xf)
	Ny = N_ELEMENTS(yf)
; x-coordinates
	xi = interpol(findgen(Nx),xf,x)
	if KEYWORD_SET(GRID) then xi=xi#replicate(1,N_ELEMENTS(y))
	ix = (long(xi) > 0L) < (Nx-2L)
	dx = xf(1) - xf(0)
	px = xi - ix
	qx = 1.-px
; y-coordinates
	yi = interpol(findgen(Ny),yf,y)
	if KEYWORD_SET(GRID) then yi=replicate(1,N_ELEMENTS(x))#yi
	iy = (long(yi) > 0L) < (Ny-2L)
	dy = yf(1) - yf(0)
	py = yi - iy
	qy = 1.-py
;
	if KEYWORD_SET(LINEAR) then begin
;
; Bi-linear interpolation
		ff  = qx * (qy * f(ix  , iy  ) + py * f(ix  , iy+1) ) + $
			  px * (py * f(ix+1, iy+1) + qy * f(ix+1, iy  ) )
	endif else begin
		if (N_ELEMENTS(fx) LT Nx*Ny) then begin
			fx = f
			for i=0L, Ny-1L do fx(*,i)=derfm(xf, f(*,i))*dx
		endif
		if (N_ELEMENTS(fy) LT Nx*Ny) then begin
			fy = f
			for i=0L, Nx-1L do fy(i,*)=derfm(yf, f(i,*))*dy
		endif
		pyqy=py*qy
		pxqx=px*qx
; Differences
		dfx1=f(ix+1,iy  )-f(ix  ,iy  )
		dfx2=f(ix+1,iy+1)-f(ix  ,iy+1)
		dfy1=f(ix  ,iy+1)-f(ix  ,iy  )
		dfy2=f(ix+1,iy+1)-f(ix+1,iy  )
;
; Bi-cubic interpolation
		ff = qx * (qy * (f (ix  ,iy  )             $
				+ pxqx* (fx(ix  ,iy  ) - dfx1)     $
				+ pyqy* (fy(ix  ,iy  ) - dfy1) )   $
				+  py * (f (ix  ,iy+1)             $
				+ pxqx* (fx(ix  ,iy+1) - dfx2)     $
				- pyqy* (fy(ix  ,iy+1) - dfy1) ) ) $
		   + px * (py * (f (ix+1,iy+1)             $
				- pxqx* (fx(ix+1,iy+1) - dfx2)     $
				- pyqy* (fy(ix+1,iy+1) - dfy2) )   $
				+  qy * (f (ix+1,iy  )             $
				- pxqx* (fx(ix+1,iy  ) - dfx1)     $
				+ pyqy* (fy(ix+1,iy  ) - dfy2) ) )

	endelse

	if KEYWORD_SET(STOP) then stop

	if (N_PARAMS() LT 8L) then return, ff
	if KEYWORD_SET(LINEAR) then begin
      dfx    =(qy * (   - (f (ix  ,iy  )       ) + $
                          (f (ix+1,iy  )       ) ) + $
               py * (     (f (ix+1,iy+1)       ) - $
                          (f (ix  ,iy+1)       ) ) )/dx
	endif else begin
      dfx    =(qy * (   - (f (ix  ,iy  )       ) +    $
        (qx*qx-2.*pxqx) * (fx(ix  ,iy  ) - dfx1) +    $
                (-pyqy) * (fy(ix  ,iy  ) - dfy1) +    $
                          (f (ix+1,iy  )       ) +    $
        (px*px-2.*pxqx) * (fx(ix+1,iy  ) - dfx1) +    $
                  pyqy  * (fy(ix+1,iy  ) - dfy2) ) +  $
               py * (     (f (ix+1,iy+1)       ) +    $
        (px*px-2.*pxqx) * (fx(ix+1,iy+1) - dfx2) +    $
                (-pyqy) * (fy(ix+1,iy+1) - dfy2) +    $
                         (-f (ix  ,iy+1)       ) +    $
        (qx*qx-2.*pxqx) * (fx(ix  ,iy+1) - dfx2) +    $
                  pyqy  * (fy(ix  ,iy+1) - dfy1) ) )/dx
	endelse

	if (N_PARAMS() LT 9L) then return, ff
	if KEYWORD_SET(LINEAR) then begin
      dfy    =(qx * (   - (f (ix  ,iy  )       ) + $
                          (f (ix  ,iy+1)       ) ) + $
               px * (   - (f (ix+1,iy  )       ) + $
                          (f (ix+1,iy+1)       ) ) )/dy
	endif else begin
      dfy    =(qx * (   - (f (ix  ,iy  )       ) + $
                (-pxqx) * (fx(ix  ,iy  ) - dfx1) + $
        (qy*qy-2.*pyqy) * (fy(ix  ,iy  ) - dfy1) + $
                          (f (ix  ,iy+1)       ) + $
                  pxqx  * (fx(ix  ,iy+1) - dfx2) + $
        (py*py-2.*pyqy) * (fy(ix  ,iy+1) - dfy1) ) + $
               px * (   - (f (ix+1,iy  )       ) + $
                  pxqx  * (fx(ix+1,iy  ) - dfx1) + $
        (qy*qy-2.*pyqy) * (fy(ix+1,iy  ) - dfy2) + $
                          (f (ix+1,iy+1)       ) + $
                (-pxqx) * (fx(ix+1,iy+1) - dfx2) + $
        (py*py-2.*pyqy) * (fy(ix+1,iy+1) - dfy2) ) )/dy
	endelse

	return, ff
end
