function ssign, x, WIDE=WIDE
    strsgn = ['-','+']
	if KEYWORD_SET(WIDE) then strsgn=' '+strsgn+' '
    return, strsgn(x GE 0)
end
