;***********************************************************************
PRO derf0,x,N
;
;  Calculates the matrix elements and an LU decomposition of the problem
;  of finding the derivatives of a cubic spline with continuous second
;  derivatives.
;
;  This particular version has end point derivatives equal to one sided
;  first order derivatives.  These are not used directly, and influence
;  the solution only through the con- dition of a continuous second
;  derivative at the next point in.

;
;  To make a "natural spline" (zero second derivative at the end pts),
;  change the weights to
;
;     w2(1)=2./3.
;     w3(1)=1./3.
;     w2(N)=2./3.
;     w1(N)=1./3.
;
;  For a general cubic spline, we have an equation system of the form:
;
;     | w2 w3          |   | d |     | e |
;     | w1 w2 w3       |   | d |     | e |
;     |       .  .     | * | . |  =  | . |
;     |       .  .  .  |   | . |     | . |
;     |          w1 w2 |   | d |     | e |
;
;  This may be solved by adding a fraction (w1) of each row to the
;  following row.
;
;  This corresponds to an LU decomposition of the problem:
;
;     | w2 w3          |   | d |     | 1               |   | e |
;     |    w2 w3       |   | d |     | w1  1           |   | e |
;     |       .  .     | * | . |  =  |    w1  1        | * | . |
;     |          .  .  |   | . |     |        .  .     |   | . |
;     |             w2 |   | d |     |           w1  1 |   | e |
;
;  In this startup routine, we calculate the fractions w1.
;  These are the same for all splines with the same x-scale,
;  and only have to be calculated once for all splines.
;
;  In principle, this startup routine only has to be called once,
;  but to make the derf entry selfcontained, derf0 is called from
;  within derf.  In practice, the derf0 time is insignificant.
; 
;  Update history:
;                      .
;  20-aug-87: coded by Ake Nordlund, Copenhagen University Observatory
;  27-sep-87: Removed bdry[123] parameters.
;  02-oct-87: Moved factor 3 from zder loop to zder0 loop.
;  04-nov-87: inverted w2(), to turn divides into multiplications
;  30-nov-88: idl proceedure for generic derivative
;
;***********************************************************************
;
;
common czder,w1,w2,w3
    
        w1 = 0*x
        w2 = 0*x
        w3 = 0*x
;
;  Interior points
;
      a3=1./3.
      w1(1:*)=a3/(x(1:*)-x)
      w3(0:N-2)=w1(1:N-1)
      w2=2.*(w1+w3)
;
;  First point
;
      w1(0)=0.0
      w2(0)=1.0
      w3(0)=0.0
;
;  Last point
;
      w1(N-1)=0.0
      w2(N-1)=1.0
      w3(N-1)=0.0
;
;  Natural spline (uncomment)
;
      w2(0)=2./3.
      w3(0)=1./3.
      w2(N-1)=2./3.
      w1(N-1)=1./3.
;
;  Eliminate subdiagonal elements of rows 2 to N:
;
;     |     w2 w3       | * | . |  =  | . |
;     |     w1 w2 w3    |   | . |     | . |
;
      for k=1L,N-1L do begin
        w1(k)=-w1(k)/w2(k-1)
        w2(k)=w2(k)+w1(k)*w3(k-1)
        end
;
;  Invert all the w2()'s, turns divides into mults
;
        w2=1./w2
;
      end
;***********************************************************************
FUNCTION derf,x,f,d2=d2,dn=dn,QUADRATIC=QUADRATIC
;
;  The following LU decomposition of the problem was calculated by the
;  startup routine:
;
;  | w2(1) w3(1)              |   |d|   |  1                    |   |e|
;  |       w2(2) w3(2)        |   |d|   | w1(2)  1              |   |e|
;  |          .     .         | * |.| = |       w1(3)  1        | * |.|
;  |                .      .  |   |.|   |              .  .     |   |.|
;  |                    w2(N) |   |d|   |                 w1  1 |   |e|
;
;  The solutions of equations are carried out in paralell.
;
;  Operation count:  4m+1d+4a = 9 flops per grid point
;
;  Update history:
;                      .
;  20-aug-87: Coded by Ake Nordlund, Copenhagen University Observatory
;  27-sep-87: Removed bdry[123] parameters.
;  02-oct-87: Moved factor 3 from zder loop to zder0 loop.
;  15-oct-87: Factors a and b moved out of lm-loops 121. 13->9 flops.
;  04-nov-87: inverted w2(), to turn divides into multiplications
;  30-nov-88: idl proceedure for generic derivative
;  17-aug-10: Checked d2 expression against explicit loop and against
;             analytical test-cases. The ~6 endpoints (both ends) of d2
;             oscillate badly from forcing d2(0) = d2(N-1) = 0.0/RT
;  25-may-17: Added possibility to specify end-point derivative, dn/RT
;  05-sep-19: Added keyword for quadratic derivatives./RT
;
;***********************************************************************
;
      common czder,w1,w2,w3
;
      N=n_elements(x)
;     s=size(f)
;     ndim=s(0)
;
      d = 0*f
;
;  Check if we have a degenerate case (no x-extension)
;
      if (N eq 1) then return,d
      if (N eq 2) then return,(f(1)-f(0))/(x(1)-x(0))*[1,1]
      if KEYWORD_SET(QUADRATIC) then begin
        d = [0, (f(2:*)-f)/(x(2:*)-x), 0]
        d(0) = d(1)& d(N-1L) = d(N-2L)
        return, d
      endif
;
      derf0,x,N
;
;  Interior points [2m+2s = 4 flop]
;
      d(0:N-2)=(f(1:N-1)-f(0:N-2))/(x(1:N-1)-x(0:N-2))^2
      d(1:N-2)=d(1:N-2)+d(0:N-3)
;      for k=1L,N-2L do begin
;        a=1./(x(2:N-1)-x(1:N-2))^2
;        b=1./(x(1:N-2)-x(0:N-3))^2
;        d(1:N-2)=(f(2:N-1)-f(1:N-2))*a+(f(1:N-2)-f(0:N-3))*b
;      end
;
;  First point
;
      d(0)=(f(1)-f(0))/(x(1)-x(0))
;
;  Last point
;
      dn0  = (f(N-1)-f(N-2))/(x(N-1)-x(N-2))
      default, dn, dn0
      d(N-1) = dn
;
;  Do the forward substitution [1m+1a = 2 flop]
;
      for k=1L,N-2L+(dn EQ dn0) do begin
        d(k)=d(k)+w1(k)*d(k-1)
      end
;
;  Do the backward substitution [1m+1d+1s = 3 flop]
;
      if (dn EQ dn0) then d(N-1)=d(N-1)*w2(N-1)
      for k=N-2L,0L,-1L do begin
        d(k)=(d(k)-w3(k)*d(k+1))*w2(k)
      end
;
      cx = 1./(x(1:*)-x)
      df = f(1:*)-f
      d2 = [(6*df(0)*cx(0)        - 4*d(0)    - 2*d(1)  )  *cx(0), $
            (3*df(1:*)*cx(1:*)    - 2*d(1:N-2)-   d(2:*))  *cx(1:*)  $
         -  (3*df(0:N-3)*cx(0:N-3)- 2*d(1:N-2)-   d(0:N-3))*cx(0:N-3),      $
            (6*df(N-2)*cx(N-2)    - 4*d(N-1)  - 2*d(N-2))  *cx(N-2)]
      return,d
      end
