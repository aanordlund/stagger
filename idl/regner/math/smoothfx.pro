;
;  Function to smooth a 1D array on a non-uniform grid
;  On a uniform grid smoothfx gives
;        g_i = y_i/2 + (y_i-1 + y_i+1)/4
;  - i.e. linear interpolation weighs as much as the actual value,
;  whereas smooth(f,3) gives
;        g_i = (y_i-1 + y_i + y_i+1)/3
;
;   Update history:
;   ---------------
;   09.05.1998 Coded/RT
;   06.04.2004 Introduced keyword for cyclic boundaries.
;
function smoothfx, x, f, CYCLIC=CYCLIC
	nx = N_ELEMENTS(f)
	f0 = f
	if KEYWORD_SET(CYCLIC) then begin
		n=0L & n0=nx-1L & n1=1L
		f0(n)=.5*f(n)+.5*((f(n1)-f(n0))/(x(n1)-x(n0))*(x(n)-x(n0))+f(n0))
		n=nx-1L & n0=nx-2L & n1=0L
		f0(n)=.5*f(n)+.5*((f(n1)-f(n0))/(x(n1)-x(n0))*(x(n)-x(n0))+f(n0))
	endif
	for n=1,nx-2 do begin
		f0(n)=.5*f(n)+.5*((f(n+1)-f(n-1))/(x(n+1)-x(n-1))*(x(n)-x(n-1))+f(n-1))
	endfor
	return, f0
end
