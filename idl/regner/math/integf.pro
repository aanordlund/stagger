;
;   Cubic integration routine.
;
;   Update history:
;   ---------------
;   ??.??.???? Coded/Aake
;   17.10.2007 Included option for reverse integration, using sign=-1/RT
;   04.02.2009 Fixed sign-error in higher-order term for reverse integration/RT
;
FUNCTION integf,z,f,df,i0=i0,sign=sign,TOTAL=TOTAL
;
;  Integral of f over z
;  di(z)/dz=f
;
default, i0,   0.
default, sign, 1.
mz=n_elements(z)
i=0*f
if (mz EQ 2L) then begin
	i = [0.0, 0.5*(z(1L)-z(0L))*(f(0L)+f(1L))]
	if KEYWORD_SET(TOTAL)   then i=i(mz-1L)
	return, i
endif
;
default, df, derf(z,f)
if (sign GE 0.) then begin
  dz=z-shift(z,1L)
  i(0)=i0
  i(1L:mz-1L)=dz(1L:mz-1L)*( (0.5*(f(1L:mz-1L)+f(0L:mz-2L))) $
    -0.08333333*(df(1L:mz-1L)-df(0L:mz-2L))*dz(1L:mz-1L) )
  for n=1L,mz-1L do i(n)=i(n)+i(n-1L)
  if KEYWORD_SET(TOTAL)   then i=i(mz-1L)
endif else begin
;  dz=z-shift(z,-1L)
  i(mz-1L)=i0
;  i(0:mz-2)=dz(0:mz-2)*( (0.5*(f(1:mz-1)+f(0:mz-2))) $
;    +0.08333333*(df(1:mz-1)-df(0:mz-2))*dz(0:mz-2) )
;  for n=mz-2L,0L,-1L do i(n)=i(n)+i(n+1)
  dz2=(z(1L:*)-z)/2.
  i(0L:mz-2L)=dz2*( f(1L:mz-1L)+ f(0L:mz-2L) $
        -dz2/3.*(df(1L:mz-1L)-df(0L:mz-2L)) )
  for n=mz-2L,0L,-1L do i(n)=i(n+1L) - i(n)
  if KEYWORD_SET(TOTAL)   then i=i(0L)
endelse
;
return,i
end
