PRO stat,a,imin=imin,imax=imax
;+
; NAME:
; 	STAT
; 
; FUNCTION:
; 	Calculate the mean value, root mean square deviation and extrema of
; 	a multi- (max 4) dimensional array.
; 
; CALLING SEQUENCE
; 	STAT, A [, IMIN=IMIN, IMAX=IMAX]
; 
; INPUTS:
; 	A:	input array
; 
; OUTPUT:
; 	Prints out results
; 
; OPTIONAL PARAMETERS:
; 	IMIN:	receives indices to the minimum value
; 	IMAX:	receives indices to the maximum value
;-

	s=size(a)
    if (s(0) LT 1L) then begin
        print, 'aver = min = max = ', a, form='(a0,g12.4)'
        return
    endif
	f1=min(a,kmin)
	f2=max(a,kmax)

	imin=lonarr(s(0))
	imax=lonarr(s(0))

	m=1 	& for i=1,s(0) do m=m*s(i)

	for i=s(0),1,-1 do begin
		m=m/s(i)
		imin(i-1)=long(kmin/m)	& kmin=kmin-imin(i-1)*m
		imax(i-1)=long(kmax/m)	& kmax=kmax-imax(i-1)*m
	end

	fa=0.0
	fr=rms(a,aver=fa,/FLUC)
	if abs(fa) lt 1.e-30 then fa=1.e-30

	print,format='(2(a,1x,g12.4,5x))','aver =',fa
	print,format='(2(a,1x,g12.4,5x))','rms  =',fr,'rms/aver =',fr/fa
	print,format='(2(a,1x,g12.4,5x),2x,5i6)','min  =',f1,'min/aver =',$
		f1/fa,imin
	print,format='(2(a,1x,g12.4,5x),2x,5i6)','max  =',f2,'max/aver =',$
		f2/fa,imax
END
