; 
;   Function to interpolate in the MHD EOS tables, exploiting the logT
;   and logrho derivatives where available.
;
;   Calling sequence:
;     IDL> mhd_read, 'EOS.tab', dir='./', EOS=EOS, tl=tl
;     IDL> LogPe = mhd_intrp(LogT, Logrho, iLogPe, EOS=EOS, tl=tl)
;   or
;     IDL> LogPe = mhd_intrp(LogT, Logrho, iLogPe, 'EOS.tab', dir='./')
;
;   Arguments (defaults in [...]):
;    LogT:      Interpolate the table to this array of log10(T/[K]).
;    Logrho:    Interpolate the table to this array of log10(rho/[g cm^-3]).
;    ivar:      Variable number as found in imhd.pro.
;    tabfile:   Name of the table file ['mhd_0z8x_norad'].
;    EOS:       Array of EOS variables from previous mhd_read.
;    tl:        Array of table logT-grid from previous mhd_read.
;    QUIET:     To supress warnings and diagnostic messages. Errors are
;               always displayed.
;    _EXTRA:    Is passed to the mhd_read command.
;    STOP:      Set this keyword to stop before returning, to make all
;               variables available to the user (for debugging).
;
;   Update history:
;   ---------------
;   25.11.2008 Coded/RT
;   15.11.2019 Soft-coded read of imhd.pro depending on Nvar. This also means
;              the routine works for non-EOS quantities computed from a table,
;              with no analytical derivatives./RT
;   07.01.2020 Improved print-out of 'cubic interp' when NOT /QUIET./RT
;
;   Known bugs: Two variables have slightly off interpolants: chiT, dlne/dlnT,.
;
function mhd_intrp, LogT, Logrho, ivar, tabfile, EOS=EOS, tl=LogTtab, $
                                    QUIET=QUIET, _EXTRA=extra_args, STOP=STOP
	if (N_ELEMENTS(EOS) LE 0) then $
		mhd_read, tabfile, EOS=EOS, tl=LogTtab, _EXTRA=extra_args
    Nvar = N_ELEMENTS(EOS(0,0,*))
    case Nvar of
        20L: begin
@imhd
        end
        37L: begin
@imhd_jcd
        end
        else:begin
            if (NOT KEYWORD_SET(QUIET)) then $
                print, 'No MHD EOS tables with Nvar=',Nvar,$
                    ' so no analytical derivatives to improve interpolation.', $
                    form='(A0,I0,A0)'
        end
    endcase

	Lrho  = reform(EOS(*,*,0))& Nr=N_ELEMENTS(Lrho(0,*))& NT=N_ELEMENTS(LogTtab)
	Lrho0 = reform(EOS(*,0,0))& dLr = Lrho(0,1)-Lrho(0,0)
	iy0   = (Lrho0-min(Lrho0))/dLr
	xf  = LogTtab & yf = Lrho(uniq(Lrho, sort(Lrho)))
	Ny  = N_ELEMENTS(yf) & f = dblarr(NT,Ny)
;
; i3(0,*) are indices of table entries that have associated logT and
; logrho derivatives, and i3(1,*) and i3(2,*) are the indices of those
; logT and logrho derivatives, respectively.
;            f      df/dlnT     df/dlnrho
    if (N_ELEMENTS(iLogP) GT 0L) then begin
        i3  = [[iLogP, ichiT,      ichirho], $
               [iLoge, idlnedlnT,  idlnedlnr], $
               [iDAD,  idDADdlnT,  idDADdlnr]]
        iv3 = where(i3 EQ ivar)
    endif else  iv3=-1L
	if (iv3 GE 0L) then begin
		ivr = iv3/3
		idr = iv3 mod 3
        if (NOT KEYWORD_SET(QUIET)) then $
            print, "cubic interp(mhd_intrp): ", ivr,idr, mhdqnt(i3(0,ivr)), $
                            mhdqnt(ivar), form='(A0,2I3,2(" ",A-7))'
		fx=f & fy=f
		for i=0L, NT-1L do begin
			f(i, iy0(i):iy0(i)+Nr-1L) = EOS(i,*,i3(0,ivr))
			fx(i,iy0(i):iy0(i)+Nr-1L) = EOS(i,*,i3(1,ivr))
			fy(i,iy0(i):iy0(i)+Nr-1L) = EOS(i,*,i3(2,ivr))
		endfor
		ff  = bicubic_irr(LogT, Logrho, f, xf, yf, fx, fy, dfx, dfy)
		if (idr EQ 0L) then g = ff
		if (idr EQ 1L) then g = dfx
		if (idr EQ 2L) then g = dfy
	endif else begin
		for i=0L, NT-1L do f(i, iy0(i):iy0(i)+Nr-1L) = EOS(i,*,ivar)
		ix = intrp1(xf, LogT, findgen(NT))
		iy = intrp1(yf, Logrho, findgen(Ny))
;
; This give a fair cubic interpolation where analytical derivaties are
; not available.
		g = interpolate(f, ix, iy, cubic=-0.5)
	endelse
	if KEYWORD_SET(STOP) then stop
	return, g
end
;var( 0)     log10(rho=density)               [g/cm**3]
;var( 1)     log10(ptot=total pressure)       [dyn/cm**2]
;var( 2)     log10(e=total int energy)        [erg/cm**3]
;var( 3)     chi rho = dlogp/dlogrho          (const t)
;var( 4)     chi t   = dlogp/dlogt            (const rho)
;var( 5)     dloge/dlogrho                    (const t  )
;var( 6)     dloge/dlogt                      (const rho)
;var( 7)     delad=dlogt/dlogptot             (const s  )
;var( 8)     log10(cp)                        [erg/g/K]
;var( 9)     d(delad)/dlogrho                 (const t  )
;var(10)     d(delad)/dlogt                   (const rho)
;var(11)     dlog(cp)/dlogrho                 (const t  ) see note (1)
;var(12)     dlog(cp)/dlogt                   (const rho) see note (1)
;var(13)     x1   H+   ionization fraction                see note (2)
;var(14)     x2   He+  ionization fraction                see note (2)
;var(15)     x3   He++ ionization fraction                see note (2)
;var(16)     xmol H2   molecular  fraction                see note (2)
;var(17)     psi (or eta): electron degeneracy parameter
;var(18)     log10(electron pressure)         [dyn/cm**2]
;var(19)     log10(gas pressure)               [dyn/cm**2]

