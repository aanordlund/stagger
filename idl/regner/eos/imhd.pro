	iLogrho		= 0 & iLogP		= 1 & iLoge		= 2 & ichirho		= 3
	ichiT		= 4 & idlnedlnr	= 5
	idlnedlnT	= 6 & iDAD		= 7
	iLogCP		= 8
	idDADdlnr	= 9
	idDADdlnT	=10
	idLogG1dlnr	=11
	idLogG1dlnT	=12
	ixHII		=13
	ixHeII		=14
	ixHeIII		=15
	ixH2		=16
	ipsi		=17
	iLogPe		=18
	iLogPg		=19
	iLogT 		=20


Punit='!xdyn!e !ncm!u-2!n!x'
mhdtitl=['log(!7q!x/!5[!xg!e !ncm!u-3!n!5]!x)','log(!8P!x/!5['+Punit+'!5]!x)', $
		 'log(!7e!x/!5[!xerg!e !ncm!u-3!n!5]!x)', '!7v!dq!x!n',$
		 '!7v!8!dT!x!n', '(!9D!xln!7e!x/!9D!xln!7q!x)!d!8T!x!n', $
		 '(!9D!xln!7e!x/!9D!xln!8T!x)!d!7q!x!n', '!9G!x!dad!n', $
		 'log!8c!dP!x!n', $
		 '(!9DG!x/!9D!xln!7q!x)!d!8T!x!n', $
		 '(!9DG!x/!9D!xln!8T!x)!d!7q!x!n', $
		 '(!9D!xln!8c!dP!x!n/!9D!xln!7q!x)!d!8T!x!n', $
		 '(!9D!xln!8c!dP!x!n/!9D!xln!8T!x)!d!7q!x!n', $
		 '!8x!x(H!u+!x!n)', '!8x!x(He!u+!x!n)', '!8x!x(He!u++!x!n)', $
		 '!8x!x(H!d2!x!n)', '!7w!x', 'log!8P!x!de!n/!5['+Punit+'!5]!x)', $
		 'log!8P!d!xg!n!x/!5['+Punit+'!5]!x)', 'log(!8T!x/!5[!xK!5]!x)']

mhdqnt=['Logrho','LogP','Loge','chirho','chiT','dlnedlnr','dlnedlnT','DAD', $
		'LogCP','dDADdlnr','dDADdlnT','dLogCPdlnr','dLogCPdlnT', $
		'xHII','xHeII','xHeIII','xH2','psi','LogPrad','LogPg']
