;
;   Program to load an MHD EOS table. Doesn't do anything if the table has
;   already been loaded (if tabfile == tabfile0, as stored in the common-block
;   common MHDtab). The variables are stored according to the imhd.pro file.
;
;   Arguments (defaults in [...]):
;    tabfile:   Name of the table file ['mhd_0z8x_norad'].
;    EOS:       Array: dblarr(NT,Nrho,Nvar), returning the EOS variables.
;    tl:        Array: dblarr(NT), returning log10(T) of the table grid.
;    atwt:      Array returning the atomic weight of the included elements.
;    abun:      Returns abundance by number normalized to 1.0 for H.
;    abfrcs:    Returns abundance by weight.
;    dir:       Directory in which to look for table files ['./']
;    EO1:       Quantities from interior table.
;    t1:        log10(T) of the interior table.
;    HEADER_ONLY: Only loads the header info.
;    DEBUG:     Add diagnostic printing.
;    ASCII_TABLE: Try to read an ASCII tables, instead of the default binary.
;    LITTLE_ENDIAN: Try to read Little Endian tables.
;    STOP:      Set this keyword to stop before returning, to make all
;               variables available to the user (for debugging).
;
;   Update history:
;   ---------------
;   16.10.2001 Coded/RT
;   31.07.2003 Added return of the abundance arrays, as well as the HEADER_ONLY
;              and STOP keywords. Softcoded dir. Print abundances when /HEADER
;              and added some more print-out when /DEBUG /RT
;   14.05.2008 Added argument dir, and abundance info and dir0 to common MHDtab.
;              No longer updating common MHDtab when /HEADER_ONLY is used.
;   09.12.2010 Added this comment and history header. Hardened for general use.
;   07.07.2014 Added keyword ASCII_TABLE to read ASCII tables. Also added
;              arguments EO1 and t1 to read secondary tables of interior sets/RT
;   28.02.2019 Added keyword FORCE to force reading of table, even when it
;              appears to already have been opened in previous call (i.e.,
;              stored in c/b MHDtab)/RT
;
;   Known bugs: Doesn't check whether tabfile and tabfile0 are actually the
;               same file, or just the same name from different cwd's.
;
pro mhd_read, tabfile, EOS=EOS, tl=tl, atwt=atwt, abun=abun, abfrcs=abfrcs, $
        dir=dir, EO1=EO1, t1=t1, HEADER_ONLY=HEADER_ONLY, $
        LITTLE_ENDIAN=LITTLE_ENDIAN, $
        DEBUG=DEBUG, ASCII_TABLE=ASCII_TABLE, FORCE=FORCE, STOP=STOP
	common MHDtab, tabfile0, EOS0, tl0, atwt0, abun0, abfrcs0, dir0

	if (N_ELEMENTS(tabfile) LE 0) then tabfile='mhd_0z8x_norad'
; Check if the table is already loaded by the previous call.
; - saves a lot of time.
	if (N_ELEMENTS(tabfile0) GT 0) then begin
		if (tabfile EQ tabfile0  AND  (NOT KEYWORD_SET(FORCE))) then begin
            print, "Reusing already opened EOS-table."
			EOS = EOS0
			tl = tl0
			atwt=atwt0
			abun=abun0
			abfrcs=abfrcs0
			dir=dir0
			if KEYWORD_SET(STOP) then stop
			return
		endif
	endif 
;	hosts_dir, mhdscr=MHDdir;, USER='art'
	default, dir, './';     MHDdir
;	dir = '/k/art/pros/MHDeos/envtab/'
	if KEYWORD_SET(DEBUG) then print, dir+tabfile
	close, 5
	ff = findfile(tabfile)
	if (ff(0) EQ '') then ff = findfile(dir+tabfile)
	if (ff(0) EQ '') then begin
		print, "Couldn't find "+dir+tabfile+". Bailing out!"
		return
	endif
    if KEYWORD_SET(ASCII_TABLE) then begin
        ASCII = 1b
        openr, 5, dir+tabfile
    endif else begin
        ASCII = 0b
        if KEYWORD_SET(LITTLE_ENDIAN) then $
                 openr, 5, dir+tabfile, /F77_UNFORMATTED, /SWAP_IF_BIG_ENDIAN  $
            else openr, 5, dir+tabfile, /F77_UNFORMATTED, /SWAP_IF_LITTLE_ENDIAN
    endelse


	ivarr=0L & idxr=0L & irescr=0L & ddx=0d0
    if (ASCII) then readf,5,ivarr,idxr,irescr,ddx $
               else readu,5,ivarr,idxr,irescr,ddx
	if KEYWORD_SET(DEBUG) then $
		print,"ivarr,idxr,irescr,ddx=",ivarr,idxr,irescr,ddx
	ivar = ivarr
; s/r rabu
	nchem=0L
	point_lun, -5, nchem_pos
	if (ASCII) then readf,5,nchem $
               else readu,5,nchem
	ar = dblarr(3,nchem) & gasmu=0d0
	point_lun, 5, nchem_pos
	if (ASCII) then readf,5,nchem,ar,gasmu $
	           else readu,5,nchem,ar,gasmu
	if KEYWORD_SET(DEBUG) then $
		print, "nchem=",nchem
	atwt=reform(ar(0,*)) & abun=reform(ar(1,*)) & abfrcs=reform(ar(2,*))
	if KEYWORD_SET(DEBUG) then begin
		for i=0,1 do print,atwt(i),abun(i),abfrcs(i), form='(f8.4,2f11.7)'
		for i=2,nchem-1 do print,atwt(i),abun(i),abfrcs(i), form='(f8.4,2e11.4)'
		print, "gasmu=", gasmu
		print, alog10(total(abun(2:*))/abun(0))+2.838033832287481d0, $
					form='("[Fe/H] = ", f5.2)'
		print, abfrcs(0), total(abfrcs(2:*),/DOUBLE), $
									form='(" X,Z: ",f14.12,",",f14.12)'
	endif
	if KEYWORD_SET(HEADER_ONLY) then begin
		close, 5
		if KEYWORD_SET(STOP) then stop
		return
	endif

	nt1=0L & nt2=0L & drh1=0d0 & drh2=0d0
	if (ASCII) then readf,5,nt1,nt2,drh1,drh2 $
	           else readu,5,nt1,nt2,drh1,drh2
	if KEYWORD_SET(DEBUG) then $
		print,"nt1,nt2,drh1,drh2=",nt1,nt2,drh1,drh2
;
    if (nt1 LE 0L) then idxr=1L
    if (idxr EQ 1L) then begin
        nt=nt2;     'i' table
        tabID = 'i:   '
    endif else begin
        nt=nt1;     'x' two tables
        tabID = 'x1:  '
    endelse
	varr=dblarr(ivar)
;	EOS = dblarr(10,10,10)
Igen:
	tlr=0d0& tl=dblarr(nt) & nrr=0L
	for i=0,nt-1 do begin
		if (ASCII) then readf,5,nrr,tlr $
		           else readu,5,nrr,tlr
		tl(i)=tlr
		if KEYWORD_SET(DEBUG) then print,"nrr,tl=",nrr,tlr
		if (i EQ 0) then begin
			nr = nrr
			print, tabID,'nt, nrr, ivar=', nt, nrr, ivar
			EOS=dblarr(nt,nr,ivar)
		endif
		for j=0,nr-1 do begin
            if (ASCII) then readf,5,varr $
                       else readu,5,varr
			EOS(i,j,*) = varr
			if KEYWORD_SET(DEBUG) then print, j, EOS(i,j,0)
;			stop
		endfor
	endfor
    if (idxr EQ 0L) then begin
        nt   = nt2
        EO1  = EOS
        t1   = tl
        idxr = -1L
        tabID= 'x2:  '
        goto, Igen
    endif


	close, 5
	if (NOT KEYWORD_SET(HEADER)) then begin
		tabfile0 = tabfile
		EOS0    = EOS
		tl0     = tl
		atwt0   = atwt
		abun0   = abun
		abfrcs0 = abfrcs
		dir0    = dir
	endif
;	surface, EOS(*,*,1)
	
	if KEYWORD_SET(STOP) then stop
end
