
;***********************************************************************
FUNCTION ddxup, f
;
;  X partial derivative
;
@common_cdata
;
;-----------------------------------------------------------------------
      s=size(f)
      if n_elements(dx) eq 0 then dx=1.
      c = (-1.+(3.^5-3.)/(3.^3-3.))/(5.^5-5.-5.*(3.^5-3))/dx
      b = (-1./dx-120.*c)/24.
      a = (1./dx-3.*b-5.*c)
      if s(0) eq 1 then $
        return, a *(shift(f,-1) - f) $
              + b *(shift(f,-2) - shift(f,1)) $
              + c *(shift(f,-3) - shift(f,2))
      if s(0) eq 2 then f=reform(f,s(1),s(2),1)
      return, a *(shift(f,-1,0,0) - f) $
            + b *(shift(f,-2,0,0) - shift(f,1,0,0)) $
            + c *(shift(f,-3,0,0) - shift(f,2,0,0))
END

;***********************************************************************
FUNCTION ddzup, f
;
;  Z partial derivative
;
@common_cdata
;
;-----------------------------------------------------------------------
      s=size(f)
      if s(0) eq 1 then return, make_array(size=size(f))
      if n_elements(dz) eq 0 then dz=1.
      c = (-1.+(3.^5-3.)/(3.^3-3.))/(5.^5-5.-5.*(3.^5-3))/dz
      b = (-1./dz-120.*c)/24.
      a = (1./dz-3.*b-5.*c)
      if s(0) eq 2 then f=reform(f,s(1),s(2),1)
      return, a *(shift(f,0,0,-1) - f) $
            + b *(shift(f,0,0,-2) - shift(f,0,0,1)) $
            + c *(shift(f,0,0,-3) - shift(f,0,0,2))
END

;***********************************************************************
FUNCTION ddxdn, f
;
;  X partial derivative
;
@common_cdata
;
;-----------------------------------------------------------------------
      s=size(f)
      if s(0) eq 2 then f=reform(f,s(1),s(2),1)
      if n_elements(dx) eq 0 then dx=1.
      c = (-1.+(3.^5-3.)/(3.^3-3.))/(5.^5-5.-5.*(3.^5-3))/dx
      b = (-1./dx-120.*c)/24.
      a = (1./dx-3.*b-5.*c)
      if s(0) eq 1 then $
        return, a*(f           - shift(f,1)) $
              + b*(shift(f,-1) - shift(f,2)) $
              + c*(shift(f,-2) - shift(f,3))
      return, a*(f               - shift(f,1,0,0)) $
            + b*(shift(f,-1,0,0) - shift(f,2,0,0)) $
            + c*(shift(f,-2,0,0) - shift(f,3,0,0))
END

;***********************************************************************
FUNCTION ddzdn, f
;
;  Z partial derivative
;
@common_cdata
;
;-----------------------------------------------------------------------
      s=size(f)
      if s(0) eq 1 then return, make_array(size=size(f))
      if s(0) eq 2 then f=reform(f,s(1),s(2),1)
      if n_elements(dz) eq 0 then dz=1.
      c = (-1.+(3.^5-3.)/(3.^3-3.))/(5.^5-5.-5.*(3.^5-3))/dz
      b = (-1./dz-120.*c)/24.
      a = (1./dz-3.*b-5.*c)
      return, a*(f - shift(f,0,0,1)) $
            + b*(shift(f,0,0,-1) - shift(f,0,0,2)) $
            + c*(shift(f,0,0,-2) - shift(f,0,0,3))
END

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
FUNCTION ddyup, f 
;
;  Y partial derivative
;
@common_cdata
@common_cmesh
;-----------------------------------------------------------------------
  c = (-1.+(3.^5-3.)/(3.^3-3.))/(5.^5-5.-5.*(3.^5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)
  val = ( $
    a *(cshift(f,dim=2,shift=1) - f) $
  + b *(cshift(f,dim=2,shift=2) - cshift(f,dim=2,shift=-1)) $
  + c *(cshift(f,dim=2,shift=3) - cshift(f,dim=2,shift=-2)))
  ny = (size(f))[2]
  for i=0,ny-1 do val[*,i,*]=dyidyup[i]*val[*,i,*]
  return, val
END

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
FUNCTION ddydn, f 
;
;  Y partial derivative
;
@common_cdata
@common_cmesh
;-----------------------------------------------------------------------
  c = (-1.+(3.^5-3.)/(3.^3-3.))/(5.^5-5.-5.*(3.^5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)
  val = ( $
      a*(f - cshift(f,dim=2,shift=-1)) $
    + b*(cshift(f,dim=2,shift=1) - cshift(f,dim=2,shift=-2)) $
    + c*(cshift(f,dim=2,shift=2) - cshift(f,dim=2,shift=-3)))
  ny = (size(f))[2]
  for i=0,ny-1 do val[*,i,*]=dyidydn[i]*val[*,i,*]
  return, val
END
