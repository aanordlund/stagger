;+
; $Id: hydrostatic.pro,v 1.1 2011/01/17 16:54:59 aake Exp $
 PRO hydrostatic,g,h,T,P,rho,ee
;
;   g: gravitational acceleration
;   h: height (model units)
;   T: temperature (K)
;   P: array with P[0]=base pressure
; rho: array with rho[0]=estimate of base density (table units)
;  ee: array with ee[0]=estimate of base energy per unit mass (table units)
;-
  for j=0,9 do begin                                    ; adjust rho to give P
    lnr=alog(rho[0])
    eei=ee[0]
    tmake,T[0],lnr,eei
    ee[0]=eei
    Pi=exp(lookup(lnr,eei))
    rho[0]=rho[0]*P[0]/Pi
    ; print,j,rho[0],ee[0],Pi,P[0]
  end

  n=n_elements(T)
  i=0
  print,'       i            h            T            P          rho'
  print,i,h[i],T[i],P[i],rho[i]
  for i=1,n-1 do begin
    for k=1,5 do begin
      if k eq 1 then begin 
        rho[i]=rho[i-1]
        ee[i]=ee[i-1]
      end
      P[i]=P[i-1]-g*0.5*(rho[i-1]+rho[i])*(h[i]-h[i-1])     ; predict P[i]
      for j=0,9 do begin                                    ; adjust rho to give P
        lnr=alog(rho[i])
        eei=ee[i]
        tmake,T[i],lnr,eei
        ee[i]=eei
        Pi=exp(lookup(lnr,eei))
        rho[i]=rho[i]*P[i]/Pi
      end
    end
    print,i,h[i],T[i],P[i],rho[i]
  end
END

PRO hydrostatic_test
  eos_table
  h=0.020*findgen(25)
  dTdh=3000.
  T=6000.-dTdh*h
  rho=make_array(size=size(T))
  ee=make_array(size=size(T))
  P=make_array(size=size(T))
  P[0]=1.
  rho[0]=3.
  ee[0]=6.
  g=2.75
  hydrostatic,g,h,T,P,rho,ee
END
