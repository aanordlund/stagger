@stagger_6th

file=findfile('snapshot-*.dat',count=nt)
open,a,file[0],/quiet
sz=size(a)
m=sz[1]
file=file[sort(file)]
eka=fltarr(nt)
eks=fltarr(m,m,nt)

for t=0,nt-1 do begin
  print,t
  open,a,file[t],/quiet,/swap_if_b
  ek=0
  ui=fux(a)
  ek=temporary(ui)^2
  ui=fuy(a)
  ek=temporary(ek)+temporary(ui)^2
  ui=fuz(a)
  ek=temporary(ek)+temporary(ui)^2
  ek=temporary(ek)*a(0)
  eka[t]=0.5*total(ek,/double)/float(m)^3
  eks[*,*,t]=total(ek,3)
end
save,eka,eks,file='ek.idlsave'

END
