PRO harvest, dryrun=dryrun, host=host, assoc=assoc, reverse=reverse, $
             every=every

cd,'.',current=wd
home='~/scratch/mconv/'
if n_elements(host) eq 0 then begin
  spawn,'hostname',result
  host=result[0]
  i=strpos(host,'.')
  if i gt 0 then host=strmid(host,0,i)
end
print,'host: '+host
iy=42

; Find all snapshot files
f=file_search('mhd*','snapshot_*.dat',count=n)
print,n,' total files'

; Unique list of dirs
for i=0,n-1 do f[i]=strmid(f[i],0,strpos(f[i],'/',/reverse_search)+1)
dir=f(uniq(f))
dir=dir(sort(dir))
if keyword_set(reverse) then dir=reverse(dir)
print,dir
ndir=n_elements(dir)

; Loop over directories
for i=0,ndir-1 do begin
  spawn,'mkdir -p '+home+dir[i]
  cd,dir[i],current=cwd
  file=file_search('snapshot_*.dat',count=nfile)
  print,dir[i]+' with '+str(nfile)+' files'
  savefile=home+dir[i]+host+'.idlsave'
  print,savefile,exists(savefile)
  if (not exists(savefile) and exists('grid.txt')) then begin
    open,a,file[0],/quiet
    sz=size(a)
    m=sz[1]

    ; Loop over files
    if not keyword_set(dryrun) then begin
      ic=fltarr(m,m,nfile)
      by=fltarr(m,m,nfile)
      k=0
      for j=0,nfile-1,every do begin
	t0=systime(1)
        open,a,file[j],/quiet,mhd=mhd
        if (mhd gt 0) then begin
	  if keyword_set(assoc) then begin
            ic[*,*,k]=a[*,0,*,5]
            by[*,*,k]=a[*,iy,*,7]
	  end else begin
            i1=strpos(file[0],'snapshot_')+9
            snap=long(strmid(file[j],i1,5))
            ic[*,*,k]=rm(snap,5,iy=0)
            by[*,*,k]=rm(snap,7,iy=iy)
	  end
        end
        print,str(j)+' '+file[j]+' '+str(mhd),systime(1)-t0,format='(a,f7.1)'
	k=k+1
      end
      nk=k-1
    end

    ; Put results in structure and save
    print,savefile
    if not keyword_set(dryrun) then begin
      s={host:host,dir:dir[i],file:file[0:nfile-1:every],iy:iy,ic:ic,by:by}
      save,s,file=savefile
      if host ne 'lynx' then begin
        cmd='scp -p '+savefile+' lynx.astro.ku.dk:scratch/mconv/'+dir[i]+' &'
	print,cmd
        spawn,cmd
      end
    end
  end
  cd,cwd
end

cd,wd
END
