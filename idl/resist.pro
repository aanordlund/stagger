
FUNCTION resist,a,t,nu=nu,divu=divu,ua=ua, $
               ux=ux,uy=uy,uz=uz,ubx=ubx,uby=uby,ubz=ubz
@common_cdata
  default,nu,[0.05,.5]
  lnr=alog(a(nvar*t))
  ux=a(nvar*t+1)/exp(xdn(lnr))
  uy=a(nvar*t+2)/exp(ydn(lnr))
  uz=a(nvar*t+3)/exp(zdn(lnr))
  bx=a(nvar*t+(nvar-3))
  by=a(nvar*t+(nvar-2))
  bz=a(nvar*t+(nvar-1))
  ux=xup(ux)
  uy=yup(uy)
  uz=zup(uz)
  bx=xup(bx)
  by=yup(by)
  bz=zup(bz)
  b2=bx^2+by^2+bz^2+1e-30
  ub=(ux*bx+uy*by+uz*bz)
  ubx=ub*bx/b2
  uby=ub*by/b2
  ubz=ub*bz/b2
  ux=ux-ubx
  uy=uy-uby
  uz=uz-ubz
  divu= 0 > (dx>dy>dz)*( $
    (shift(ux,1,0,0)-shift(ux,-1,0,0))*(0.5/dx) + $
    (shift(uy,0,1,0)-shift(uy,0,-1,0))*(0.5/dy) + $
    (shift(uz,0,0,1)-shift(uz,0,0,-1))*(0.5/dz))
  ua=sqrt(((gamma*(gamma-1.))*a(nvar*t+4)+b2)/exp(lnr)+ux^2+uy^2+uz^2)
  eta=nu[0]*ua+nu[1]*divu
  eta=smooth3(max3(eta))
;  eta=exp(smooth3((alog(eta))))
  return,eta
END
