;create spinor 4D fitscube from MURAM sav files
pro stanford2spinor,sav,xregion=xr,yregion=yr,reread=reread
  common datast2s,rsav,lx,ly,z,nx,ny,nz,rho,p,t,vx,vy,vz,bx,by,bz
  
  reread=keyword_set(reread) or n_elements(t) eq 0
  if reread then begin
    restore,sav,/v
    rsav=sav
  endif
  dir=strmid(rsav,0,strpos(rsav,'/',/reverse_search)+1)
  
  sz=size(T)
  if n_elements(xr) ne 2 then xr=[0,sz(1)-1]
  if n_elements(yr) ne 2 then yr=[0,sz(3)-1]
  
  nx=xr(1)-xr(0)+1
  ny=yr(1)-yr(0)+1
  
  mm=minmax(z(1:*)-z(0:n_elements(z)-2))  
  if max(mm)-min(mm) ge 10 then begin
    print,'WARNING: height sampling is not equidistant.'
    print,'The maximum deviation from an equidistant gris is: '+ $
      string((max(mm)-min(mm))/1e5,format='(e10.3)')+' km.'
    print,'Continue [y/N]?'
    key=get_kbrd()
    if strupcase(key) ne 'Y' then retall
  endif
  
  
                                ;cut cubes at a depth where the
                                ;minimum average temperature is higher
                                ;than 15000 Kelvin
  
  Tz=total(total(T(xr(0):xr(1),*,yr(0):yr(1)),1),2)/nx/ny
  imin=min(where(tz lt 15000))
                                ;zrange: do not allow rising
                                ;temperatures, therefore cut the
                                ;atmosphere at the temperature minimum
                                ;of the whole map
  dummy=min(t,dim=2,tmin)
  tidx=(array_indices(t,tmin))(1,*)
  imax=min(tidx)
  dummy=min(tz,imax)                   
  imin=0 & imax=sz(2)-1         ;NO. Take all points:
  
  zr=[imin,imax]
  nz=zr(1)-zr(0)+1  
  izz=(indgen(nz)+imin)
  print,'Writing out cube for heights with average temperature ', $
    Tz(zr(0)),Tz(zr(1))
  
  
  if n_elements(dir) eq 0 then dir='.'
  add=''
  if nx ne sz(1) then add=add+'.x'+strjoin(string(xr,format='(i4.4)'),'-')
  if ny ne sz(3) then add=add+'.y'+strjoin(string(yr,format='(i4.4)'),'-')
  mhdfile=dir+'mhd_atmos'+add+'.fits'
  
  
  print,'Preparing Cube cube with MHD data: ',mhdfile
  
  
  npar=9
  mhdcube=fltarr(npar,nz,nx,ny)
  
  mhdcube(0,*,*,*)= $
    transpose(temporary(T(xr(0):xr(1),izz,yr(0):yr(1))),[1,0,2])
                                ;SPINOR does not like temperatures
                                ;less than 1000:
  if min(mhdcube(0,*,*,*)) lt 1000 then begin
    print,'Temperatures <1000K found. Setting min. Temp to 1000K.'
    mhdcube(0,*,*,*)=  mhdcube(0,*,*,*)>1000.
  endif
  mhdcube(1,*,*,*)= $
    transpose(temporary(RHO(xr(0):xr(1),izz,yr(0):yr(1))),[1,0,2])
  mhdcube(2,*,*,*)= $
    transpose(temporary(P(xr(0):xr(1),izz,yr(0):yr(1))),[1,0,2])
;ATTENTION: BY, BY contains height information!  
  mhdcube(3,*,*,*)= $
    transpose(temporary(VX(xr(0):xr(1),izz,yr(0):yr(1))),[1,0,2])
  mhdcube(4,*,*,*)= $
    transpose(temporary(VZ(xr(0):xr(1),izz,yr(0):yr(1))),[1,0,2])
  mhdcube(5,*,*,*)= $
    transpose(temporary(VY(xr(0):xr(1),izz,yr(0):yr(1))),[1,0,2])
  mhdcube(6,*,*,*)= $
    transpose(temporary(BX(xr(0):xr(1),izz,yr(0):yr(1))),[1,0,2])
  mhdcube(7,*,*,*)= $
    transpose(temporary(BZ(xr(0):xr(1),izz,yr(0):yr(1))),[1,0,2])
  mhdcube(8,*,*,*)= $
    transpose(temporary(BY(xr(0):xr(1),izz,yr(0):yr(1))),[1,0,2])
  
  
  mkhdr,header,mhdcube
  sxaddpar,header,'COMMENT','Stanford 4D cube for SPINOR'
  sxaddpar,header,'COMMENT','Created from directory '+dir
  fxaddpar,header,'CUBE',rsav,'IDL save file'
  fxaddpar,header,'PAR1','TEMP','temperature [K]'
  fxaddpar,header,'PAR2','RHO','density [g/cm^3]'
  fxaddpar,header,'PAR3','PRESS','pressure  [dyn/cm^2]'
  fxaddpar,header,'PAR4','VX','x-velocity [cm/s]'
  fxaddpar,header,'PAR5','VY','y-velocity [cm/s]'
  fxaddpar,header,'PAR6','VZ','z-velocity [cm/s]'
  fxaddpar,header,'PAR7','BX','B-Field X [G]'
  fxaddpar,header,'PAR8','BY','B-Field Y [G]'
  fxaddpar,header,'PAR9','BZ','B-Field Z [G]'
  fxaddpar,header,'T_X',float(lx),'x total size [cm]'
  fxaddpar,header,'T_Y',float(max(z)-min(z)),'total depth [cm]'
  fxaddpar,header,'T_Z',float(ly),'y total size [cm]'
  
  writefits,mhdfile,mhdcube,header
  print,'Wrote 4D-FITS cube with MHD data: ',mhdfile
  
end
