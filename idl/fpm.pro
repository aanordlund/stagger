; $Id: fpm.pro,v 1.1 2008/01/06 12:48:24 aake Exp $
FUNCTION fpm,a,t,nv=nv
  default,nv,7
  bi=a(nv*t+nv-3)
  bi=xup1(temporary(bi))
  pm=temporary(bi)^2
  bi=a(nv*t+nv-2)
  bi=yup1(temporary(bi))
  pm=temporary(pm)+temporary(bi)^2
  bi=a(nv*t+nv-1)
  bi=zup1(temporary(bi))
  pm=temporary(pm)+temporary(bi)^2
  pm=0.5*temporary(pm)
  return,pm
END


