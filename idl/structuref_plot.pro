; $Id: structuref_plot.pro,v 1.6 2003/05/10 10:18:38 aake Exp $
;-----------------------------------------------------------------------
PRO structuref_plot,file,longitudinal=longitudinal,average=a,ascii=ascii, $
  derivs=derivs,dr=dr,strf=strf
;+
;  Make structure function plots.  Syntax:
;
;      IDL> structuref_plot,file
;  or
;      IDL> structuref_plot,file,average=[10,15]
;  or
;      IDL> structuref_plot,file,average=[10,15],/longitudinal
;
;  where "file" is for example 'data/250M3B1-strf.idlsave'.  The 
;  average parameter determines over which snapshot range to average.
;-
  if keyword_set(ascii) then begin 
    openr,u,/get,file
    nr=0L & nstr=0L & nt=0L
    readf,u,nr,nstr,nt
    dr=fltarr(nr)
    strf=fltarr(nr,nstr,nt)
    readf,u,dr,strf
    free_lun,u
    strf=reform(strf,nr,nstr,1,nt)
  end else begin
    restore,file
    sz=size(strf)
    if sz[0] eq 3 then begin
      strf=rebin(reform(strf,sz[1],sz[2],sz[3],1),sz[1],sz[2],sz[3],2)
      sz=size(strf)
    end
    nr=sz[1]
    nstr=sz[2]
    nt=sz[4]
  end
  help,strf,nt

  default,a,[0,nt-1]

  if keyword_set(longitudinal) then begin
    k=1
    id='-t-'
  end else begin
    k=0
    id='-l-'
  end 

;  Plot structure functions
  ws,2
  if !d.name eq 'PS' then device,file=fileroot(file)+id+'-f.eps'
  plot,[min(dr,max=max),max],[min(strf[*,*,k,a[0]:a[1]],max=max),max],/nodata,/xlog,/ylog,xst=3,yst=3
  for j=0,nstr-1 do oplot,dr,aver(strf[*,j,k,a[0]:a[1]],4),psym=-1


;  Plot structure function log-log derivatives
  ws,0
  dstrf=fltarr(nr,nstr)
  for j=0,nstr-1 do begin
    dstrf[*,j]=deriv(alog(dr),alog(aver(strf[*,j,k,a[0]:a[1]],4)))
  end
  derivs=dstrf
  if !d.name eq 'PS' then device,file=fileroot(file)+id+'-d.eps'
  plot,[min(dr,max=max),max],[min(dstrf,max=max),max],/nodata,/xlog,xst=3,yst=3
  for j=0,nstr-1 do oplot,dr,dstrf[*,j],psym=-1

;  Plot ESS structure function log-log derivatives
  d3=dstrf[*,2]
  for j=0,nstr-1 do dstrf[*,j]=dstrf[*,j]-d3+1.
  ws,1
  if !d.name eq 'PS' then device,file=fileroot(file)+id+'-e.eps'
  plot,[min(dr,max=max),max],[min(dstrf,max=max)>0,max],/nodata,/xlog,xst=3,yst=3
  for j=0,nstr-1 do oplot,dr,dstrf[*,j],psym=-1

  if !d.name eq 'PS' then device,/close
END
