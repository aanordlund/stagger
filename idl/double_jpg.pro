PRO double_jpg, dir, base, shift=sh, zoom=zm

default,dir,'.'
default,base,'ic'

dbl='/double/'
file_mkdir,dir+dbl
f=file_search(dir+dbl,base+'*.jpg',count=nf)
if nf gt 0 then begin
  print,'deleting old double files:',nf
  file_delete,f
end

f=file_search(dir,base+'*.jpg',count=nf)
if nf eq 0 then return

dbl='/double/'
file_mkdir,dir+dbl

read_jpeg,f[0],im
sz=size(im)
if keyword_set(sh) and sz[0] eq 2 then im=shift(im,sh[0],sh[1])
if keyword_set(sh) and sz[0] eq 3 then im=shift(im,0,sh[0],sh[1])
if keyword_set(zm) and sz[0] eq 2 then im=im(zm[0]:zm[1],zm[2]:zm[3])
if keyword_set(zm) and sz[0] eq 3 then im=im(*,zm[0]:zm[1],zm[2]:zm[3])
k=0
base1=dir+dbl+base+'-'
file=base1+string(k,format='(i4.4)')+'.jpg' & k=k+1
if sz[0] eq 3 then write_jpeg,file,im,true=1 else write_jpeg,file,im
for i=1,nf-1 do begin
  im0=im
  read_jpeg,f[i],im
  if keyword_set(sh) and sz[0] eq 2 then im=shift(im,sh[0],sh[1])
  if keyword_set(sh) and sz[0] eq 3 then im=shift(im,0,sh[0],sh[1])
  if keyword_set(zm) and sz[0] eq 2 then im=im(zm[0]:zm[1],zm[2]:zm[3])
  if keyword_set(zm) and sz[0] eq 3 then im=im(*,zm[0]:zm[1],zm[2]:zm[3])
  file=base1+string(k,format='(i4.4)')+'.jpg' & k=k+1
  print,f[i]+' -> '+file
  im1=byte(0.5*(long(im)+long(im0)))
  if sz[0] eq 3 then write_jpeg,file,im1,true=1 else write_jpeg,file,im1
  file=base1+string(k,format='(i4.4)')+'.jpg' & k=k+1
  print,f[i]+' -> '+file
  if sz[0] eq 3 then write_jpeg,file,im,true=1 else write_jpeg,file,im
end

END
