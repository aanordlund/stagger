
k_Boltz      = 1.3807e-16
q_electron   = 1.609e-12
m_electron   = 9.1094e-28
m_proton     = 1.6726e-24
g_gravity    = 6.6726e-8
h_planck     = 6.6261e-27
c_light      = 2.99793e10
sigma_stefan = 5.6705e-5

k_B  = k_boltz
q_e  = q_electron
m_p  = m_proton
m_H  = m_proton
m_e  = m_electron
grav = g_gravity
h_p  = h_planck
c    = c_light
stefan = sigma_stefan
