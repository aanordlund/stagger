pro compute,rho,TT,yy,ee,PP,kappa,S
  common params,f_He,chiH,chiH_,TT0,TT0_,rho0,rho0_,ee0,kappa0,sigmaB

  K=(rho0/rho)*(TT/TT0)^1.5*exp(-TT0/TT)
  yy=2./(1.+sqrt(1.+4./K))
  ;yy[where(yy lt .05)]=.01
  ee=ee0*(1.5*(1.+f_He+yy)*(TT/TT0)+yy)
  PP=(2./3.)*rho*(ee-yy*ee0)
  kappa=.25*(rho/rho0_)*(TT0_/TT)^1.5*exp(TT0_/TT)*yy*(1.0-yy)*kappa0
  S=40.*sigmaB*TT^4/!pi & print,sigmaB
end

restore
init_constants,ul,ur,ut
init_params

compute,rho,TT,yy,ee,PP,kappa,S
;kappa=opacity(TT,yy,rho*ur)
rhokapm2=fltarr(1,1,163)
for i=0,162 do rhokapm2[i]=mean(rho[*,*,i]*kappa[*,*,i])
tau0=.01*mean(kappa[*,*,0])*mean(rho[*,*,0])
tau=opticaldepth(rho,kappa,z,tau0)
;tau=spline_integral(z,rhokap)
taum=fltarr(1,1,163)
taum[0,0,*]=integf(z,rhokapm)
Sm=fltarr(1,1,163)
for j=0,162 do Sm[0,0,j]=mean(S[*,*,j])
;transx,tau,S,Q,I,debug=1
transx,taum,Sm,Qm,Im

plot,rhokapm*qm*4.*!pi/1.e11


end