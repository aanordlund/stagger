
@../idl/ionization

close,1
openw,1,'../src/EOS/H+He.dat',/f77
ma=300L
mb=ma

rmin=1e-5
rmax=1e5
a0=alog(rmin)
a2=alog(rmax)
a1=(ma-1)/(a2-a0)

eemin=0.
eemax=20.
b2=eemax
b0=eemin
b1=(mb-1)/(b2-b0)

writeu,1,ma,mb,a0,b0,a1,b1

lnr = rebin(reform(a0+findgen(ma)/a1,ma,1),ma,mb)
ee  = rebin(reform(b0+findgen(mb)/b1,1,mb),ma,mb)
eos_atm, exp(lnr), ee, y=tab

tab=alog(tab)
writeu,1,tab
close,1

surface, exp(tab)

END
