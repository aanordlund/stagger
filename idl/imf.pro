; $Id: imf.pro,v 1.3 2009/09/01 13:54:39 aake Exp $
;-------------------------------------------------------------------------------
 PRO imf,file,mach=mach,alpha=alpha

  default, file, 'sink.dat'
  default, mach, 9.
  default, alpha, 1.

  pc=3d18
  mu=2.4
  mp=1.67e-24
  msun=2d33

  mach0=9. & sz0=2.5 & n0=1.50e3/alpha

  sz=(mach/mach0)^2*sz0
  n=(sz0/sz)*n0

  mbox=n*mu*mp*(sz*pc)^3/msun

  close,/all
  sp=rsink(u,/reopen,t=t,m=m,file=file,/scan)

  cd,cur=dir
  i=strpos(dir,path_sep(),/reverse_search)
  i=strpos(dir,path_sep(),i-1,/reverse_search)
  i=strpos(dir,path_sep(),i-1,/reverse_search)
  dir=strmid(dir,i+1)

  !p.multi=0
  plhist,sp.m*mbox,/xlog,/ylog,nbin=25,xr=[0.01,10.],xst=1 $
    ,title=dir+path_sep()+file
  w=where(sp.m*mbox le 0.08,nbd)
  print,'time =',t,' total mass fraction',total(sp.m)
  print,m,' stars',nbd,' BDs',mbox,' solar masses',sz, ' pc',n,' pcc
  print,'Larson scaling:',0.18*mach0*(1./sz)^0.5,' v0', n0*sz,' n0'
END
