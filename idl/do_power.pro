@stagger_6th

PRO do_power,file,_extra=extra

open,a,file,nt=nt,nv=nv,_extra=extra
sz=size(a)
p=fltarr(sz[1]/2,nt,3)
for t=0,nt-1 do begin
  r=a[nv*t]
  lnr=alog(r)
  for i=0,2 do begin
    print,t,i
    if i eq 0 then rdn=exp(xdn(-lnr))
    if i eq 1 then rdn=exp(ydn(-lnr))
    if i eq 2 then rdn=exp(zdn(-lnr))
    power3d,a[i+1+nv*t]*rdn,wa=k,sp=sp,/noplot,/aver
    p[*,t,i]=sp
    save,file,t,i,k,p,file=fileroot(file)+'-power.idlsave'
  end
end

END

