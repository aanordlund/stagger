; $Id: structuref.pro,v 1.11 2003/06/09 11:07:54 padoan Exp $
;-----------------------------------------------------------------------
PRO structuref, files, nstr=nstr, mstr=mstr, subsample=sub, strf=strf, $
    dr=dr, nt=nt, _extra=extra
;+
;  Transverse and longitudinal structure functions.  Syntax:
;
;         IDL> structuref,file[,/swap_if_l]
; or
;         IDL> structuref,filepattern[,/swap_if_l]
; or
;         IDL> structuref,filepattern,subsample=2[,/swap_if_l]
;
;  The results are saved in file-strf.idlsave or file-strf2.idlsave (subsample=2).
;-
default,nstr,10
default,sub,1

default,files,findfile('data/*.dat')
nf=n_elements(files)
if nf eq 1 then files=findfile(files)
nf=n_elements(files)

for i=0,nf-1 do begin
  file=files(i)
  open,a,file,nt=nt1,dim=dim,nv=nv,_extra=extra
  default,nt,nt1
  sz=size(a)
  dr=intarr(50)
  dr[0]=2
  dr[1]=3
  k=1
  while dr[k] le dim[0]/(2*sub) do begin
    k=k+1
    dr[k]=2*dr[k-2]
  end
  default,mstr,k
  dr=dr[0:mstr-1]
  print,dr
  strf=fltarr(mstr,nstr,2,nt)
  help,strf

  for t=0,nt-1 do begin
    lnr=alog(a[nv*t])
    ui=a[1+nv*t]/exp(xdn(lnr))
    if sub gt 1 then ui=congrid(ui,sz[1]/sub,sz[2]/sub,sz[3]/sub)
    for k=0,mstr-1 do begin
      s=dr[k]
      print,file,'  x ',t,s
      du=0
      du=abs(ui-shift(ui,0,s,0))
      for j=0,nstr-1 do begin
        strf(k,j,0,t)=strf(k,j,0,t)+aver(du^(j+1))
      end
      du=0
      du=abs(ui-shift(ui,0,0,s))
      for j=0,nstr-1 do begin
        strf(k,j,0,t)=strf(k,j,0,t)+aver(du^(j+1))
      end
      du=0
      du=abs(ui-shift(ui,s,0,0))
      for j=0,nstr-1 do begin
        strf(k,j,1,t)=strf(k,j,1,t)+aver(du^(j+1))
      end
    end

    ui=a[2+nv*t]/exp(ydn(lnr))
    if sub gt 1 then ui=congrid(ui,sz[1]/sub,sz[2]/sub,sz[3]/sub)
    for k=0,mstr-1 do begin
      s=dr[k]
      print,file,'  y ',t,s
      du=0
      du=abs(ui-shift(ui,s,0,0))
      for j=0,nstr-1 do begin
        strf(k,j,0,t)=strf(k,j,0,t)+aver(du^(j+1))
      end
      du=0
      du=abs(ui-shift(ui,0,0,s))
      for j=0,nstr-1 do begin
        strf(k,j,0,t)=strf(k,j,0,t)+aver(du^(j+1))
      end
      du=0
      du=abs(ui-shift(ui,0,s,0))
      for j=0,nstr-1 do begin
    strf(k,j,1,t)=strf(k,j,1,t)+aver(du^(j+1))
      end
    end

    ui=a[3+nv*t]/exp(zdn(lnr))
    if sub gt 1 then ui=congrid(ui,sz[1]/sub,sz[2]/sub,sz[3]/sub)
    for k=0,mstr-1 do begin
      s=dr[k]
      print,file,'  z ',t,s
      du=0
      du=abs(ui-shift(ui,s,0,0))
      for j=0,nstr-1 do begin
        strf(k,j,0,t)=strf(k,j,0,t)+aver(du^(j+1))
      end
      du=0
      du=abs(ui-shift(ui,0,s,0))
      for j=0,nstr-1 do begin
        strf(k,j,0,t)=strf(k,j,0,t)+aver(du^(j+1))
      end
      du=0
      du=abs(ui-shift(ui,0,0,s))
      for j=0,nstr-1 do begin
        strf(k,j,1,t)=strf(k,j,1,t)+aver(du^(j+1))
      end
    end
    dr0=dr
    dr=sub*dr
    if sub gt 1 then ss=str(sub) else ss=''
    save,file,t,strf,dr,file=fileroot(file)+'-strf'+ss+'.idlsave'
    dr=dr0
  end
end

END
