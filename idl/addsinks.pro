; $Id: addsinks.pro,v 1.5 2008/01/19 09:29:57 aake Exp $
;-----------------------------------------------------------------------
PRO addsinks,rho,s,xm,cmass=cmass,true=true,b=b,px=px,py=py,pz=pz
;+
;   rho: 3-D array with densities
;     s: structure obtained from rsink() -- IDL sink data reader
;    xm: x-mesh, obtained for example from open,...,xm=xm
;  true: 1 for adding true mass, 0 for visual effect
; cmass: multiplicative factor in visual effect case
;     b: width of gaussian, in mesh points
;    px: 3-D x-momentum (optional) to be updated
;    py: 3-D y-momentum (optional) to be updated
;    pz: 3-D z-momentum (optional) to be updated
;-
  default,cmass,300.
  default,b,1.

  sz=size(rho)
  mx=sz[1]
  m=n_elements(s.m)

  dx=xm[1]-xm[0]
  dv=dx^3

  rhomax=max(rho)

  v=min(abs(xm),mx2)

  x=rebin(reform(xm,mx,1,1),mx,mx,mx) 
  r2=temporary(x)^2
  y=rebin(reform(xm,1,mx,1),mx,mx,mx)
  r2=r2+temporary(y)^2
  z=rebin(reform(xm,1,1,mx),mx,mx,mx)
  r2=r2+temporary(z)^2

  for i=0,m-1 do begin
    ix=long((s.x[i]-xm[0])/dx+0.5)
    iy=long((s.y[i]-xm[0])/dx+0.5)
    iz=long((s.z[i]-xm[0])/dx+0.5)
    
    rho=shift(temporary(rho),mx2-ix,mx2-iy,mx2-iz)

    if keyword_set(true) then begin
      mass=s.m[i]
      b2=(b*dx)^2
    end else begin
      mass=s.m[i]
      lm=alog10(s.m[i]*cmass*100.) > 1.
      b2=(b*dx*lm)^2
    end

    w=where(r2 lt -alog(1e-2)*b2,nw)
    print,'nw',nw
    phi=exp(-r2[w]/b2)
    totphi = total(phi)
    rho[w]=rho[w]+phi*(mass/(dv*totphi))
    if n_elements(px) gt 0 then px[w]=px[w]+phi*(s.px[i]/(dv*totphi))
    if n_elements(py) gt 0 then py[w]=py[w]+phi*(s.py[i]/(dv*totphi))
    if n_elements(pz) gt 0 then pz[w]=pz[w]+phi*(s.pz[i]/(dv*totphi))

    rho=shift(temporary(rho),ix-mx2,iy-mx2,iz-mx2)
  end
END
