;@stagger_6th

if fexists('em.idlsave') then begin
  restore,'em.idlsave'
  nt0=n_elements(ema)
  ema0=ema
  ems0=ems
end else begin
  nt0=0
end

file=findfile('snapshot-*.dat',count=nt)
open,a,file[0],/quiet
sz=size(a)
m=sz[1]

file=file[sort(file)]
ema=fltarr(nt)
ems=fltarr(m,m,nt)
print,'nt =',nt,nt0

if (nt0 gt 0) then begin
  ema[0:nt0-1]=ema0
  ems[*,*,0:nt0-1]=ems0
  t0=nt0
end else begin
  t0=0
end

for t=t0,nt-1 do begin
  open,a,file[t],/quiet,/swap_if_b
  em=0
  em=a(4)^2
  em=temporary(em)+a(5)^2
  em=temporary(em)+a(6)^2
  ema[t]=0.5*total(em,/double)/float(m)^3
  ems[*,*,t]=total(em,3)
  print,t,ema[t]
  if ((t mod 10) eq 0) then save,ema,ems,file='em.idlsave'
end
save,ema,ems,file='em.idlsave'

END
