@stagger_6th

ny=500

idls,file,'snapshot_?????.dat'
nf=n_elements(file)
pf=fltarr(ny,nf)

for i=0,nf-1,10 do begin
  open,a,file[i]

  lnr=alog(a[0])
  ux=a[1]/exp(xdn(lnr))
  uy=a[2]/exp(ydn(lnr))
  uz=a[2]/exp(zdn(lnr))
  lnr=0
  print,'u1',i
  ux=xup(temporary(ux))
  uy=yup(temporary(uy))
  uz=zup(temporary(uz))
  print,'u2',i
  bx=xup(a[6])
  by=yup(a[7])
  bz=zup(a[8])
  print,'B',i
  Ex=-uy*Bz+uz*By
  ;Ey=-uz*Bx+ux*Bz
  Ez=-ux*By+uy*Bx
  print,'E',i
  ux=0
  uz=0
  Ey=0
  By=0

  Fy=-Ez*Bx+Ex*Bz
  pf1=haver(Fy,/yv)
  pf[5:ny-6,i]=pf1[5:ny-6]

  save,pf,file='Poynting.sav'
end

END
