@stagger_jacobi

FUNCTION uniform, f
@common_cmesh
  mx = n_elements(xm)
  i = findgen(mx)
  dx = (xm(mx-1)-xm(0))/(mx-1)
  x = xm(0) + dx*i
  ix = interpolate(i,x)

  my = n_elements(ym)
  lb = 5
  ub = my-6
  i = findgen(ub-lb+1)
  dy = (ym(ub)-ym(lb))/(ub-lb)
  my = ub-lb+1
  y = ym(lb) + dy*i
  iy = interpolate(i,y)

  mz = n_elements(zm)
  i = findgen(mz)
  dz = (zm(mz-1)-zm(0))/(mz-1)
  z = zm(0) + dz*i
  iz = interpolate(i,z)

  print,dx,dy,dz

  ix = rebin(reform(ix,mx,1,1),mx,my,mz)
  iy = rebin(reform(iy,1,my,1),mx,my,mz)
  iz = rebin(reform(iz,1,1,mz),mx,my,mz)

  return, interpolate3d(ix,iy,iz)
END

PRO vdfout, f, t, level, var, file
  print,t,'  ',var
  close,1
  openw,1,'tmp.raw'
  f = uniform(f)
  writeu,1,f
  close,1
  spawn,'raw2vdf -quiet -ts '+str(t)+' -level '+str(level)+' -varname '+str(var)+' '+str(file)+'.vdf tmp.raw'
END

PRO mpi2vdf, ttt, file, extents=extents

lmax=4
level=3

open,a,/swap_if_b

nt=99
f = rm(0,0,file,nt=nt)
print,'nt=',nt
sz=size(f)

default,ttt,[0,nt-1,1]
tt=ttt
nt=(tt[1])/tt[2]+1

lb = 5
ub = sz[2]-6

default, extents, '0:0:0:25:200:25'

dim=str(sz[1])+'x'+str(ub-lb+1)+'x'+str(sz[3])
spawn,'vdfcreate -extents '+extents+' -dimension '+str(dim)+' -numts '+str(nt)+' -level '+str(lmax)+' -varnames rho:ux:uy:uz:T:bx:by:bz:j2 '+file+'.vdf

for t=tt[0],tt[1],tt[2] do begin
  print,t

  var = 'rho' & print,var
  rho = rm(t,0,file,nt=nt)
  vdfout,rho,t/tt[2],level,var,file

  var = 'ux' & print,var
  f= rm(t,1)
  f = xup(f)/rho
  vdfout,f,t/tt[2],level,var,file
  f = 0

  var = 'uy' & print,var
  f = rm(t,2,file)
  f = yup(f)/rho
  vdfout,f,t/tt[2],level,var,file
  f = 0

  var = 'uz' & print,var
  f = rm(t,3,file)
  f = zup(f)/rho
  vdfout,f,t/tt[2],level,var,file
  f = 0

  var = 'T' & print,var
  f = rm(t,4,file)/rho
  vdfout,f,t/tt[2],level,var,file
  f = 0

  var = 'bx' & print,var
  f = rm(t,5,file)
  jy = +ddzdn(f)
  jz = -ddydn(f)
  f = xup(f)
  vdfout,f,t/tt[2],level,var,file
  f = 0

  var = 'by' & print,var
  f = rm(t,6,file)
  jz = jz + ddxdn(f)
  j2 = temporary(jz)^2
  jx =    - ddzdn(f)
  f = yup(f)
  vdfout,f,t/tt[2],level,var,file
  f = 0

  var = 'bz' & print,var
  f = rm(t,7,file)
  jx = jx + ddydn(f)
  j2 = temporary(j2) + temporary(jx)^2
  jy = jy - ddxdn(f)
  j2 = temporary(j2) + temporary(jy)^2
  f = zup(f)
  vdfout,f,t/tt[2],level,var,file
  f = 0

  var = 'j2' & print,var
  vdfout,j2,t/tt[2],level,var,file
  j2 = 0

end

END

