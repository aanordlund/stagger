 mx=4
 my=4
 mz=64
 doxi=1

 s=1+fltarr(mx,my,mz)
 tau=fltarr(mx,my,mz)
 xi=fltarr(mx,my,mz)
 q=fltarr(mx,my,mz)

 logtaumin=-6.
 logtaumax=3.
 dlogtau=(logtaumax-logtaumin)/(mz-1)
 for iz=1,mz do begin
     tau(*,*,iz-1)=10.^(logtaumin+dlogtau*(iz-1))
 endfor

 n1 = mz/2
 transx,mz,n1,tau,s,q,xi,doxi

 print,'      iz       log(tau)            S                P'
 print,'      --------------------------------------------------------------'
 for iz=1,mz do begin
     print, iz, alog10(tau(1,1,iz-1)), s(1,1,iz-1), s(1,1,iz-1)+q(1,1,iz-1)
 endfor

 end