PRO Datacube

;Note: y-axis is vertical direction

;Parameters
x_size=2016
y_size=500
z_size=2016

ghost=5 ;skip ghost layers





;Setting up equidistant vertical grid
readmesh, '/data/seismo/birch/stein/96x20new_1236.6.msh', xmesh=x,ymesh=y,zmesh=z
readmesh_face, '/data/seismo/birch/stein/96x20new_1236.6.msh', xmesh=x1,ymesh=y1,zmesh=z1


;Reading only uppermost megameter
limit=where((abs(y[ghost:y_size-1]-min(y[ghost:y_size-1])-1)) EQ min(abs(y[ghost:y_size-1]-min(y[ghost:y_size-1])-1)))
limit=limit[0]

ynew = dblarr(limit)

for i=0,n_elements(ynew)-1 do begin
    ynew[i] = y[ghost]+(y[ghost+limit-1]-y[ghost])/(limit-1)*i
endfor
ynew=reverse(ynew)


;Reading data
openr,u,/get,'/data/seismo/birch/stein/96x20new_1186.2.dat'
a=assoc(u,fltarr(x_size,y_size,z_size))
nvar=6


for t=53,100 do begin

rho=a[*,ghost:limit+ghost-1,*,t*nvar+0]
px=a[*,ghost:limit+ghost-1,*,t*nvar+1]
py=a[*,ghost:limit+ghost-1,*,t*nvar+2]
pz=a[*,ghost:limit+ghost-1,*,t*nvar+3]
E=a[*,ghost:limit+ghost-1,*,t*nvar+4]
Temp=a[*,ghost:limit+ghost-1,*,t*nvar+5]
Bx=fltarr(x_size,limit,z_size)
By=fltarr(x_size,limit,z_size)
Bz=fltarr(x_size,limit,z_size)



;Interpolate momentum to cell-centered grid
;x-direction
for i = 0,limit-1 do begin
    for j = 0,n_elements(z)-1 do begin
        px[*,i,j] = interpol(px[*,i,j],x1,x)
        py[*,i,j] = interpol(py[*,i,j],x1,x)
        pz[*,i,j] = interpol(pz[*,i,j],x1,x)
    endfor
endfor
;y-direction
for i = 0,n_elements(x)-1 do begin
    for j = 0,n_elements(z)-1 do begin
        px[i,*,j] = interpol(px[i,*,j],y1[ghost:limit+ghost-1],y[ghost:limit+ghost-1])
        py[i,*,j] = interpol(py[i,*,j],y1[ghost:limit+ghost-1],y[ghost:limit+ghost-1])
        pz[i,*,j] = interpol(pz[i,*,j],y1[ghost:limit+ghost-1],y[ghost:limit+ghost-1])
    endfor
endfor
;z-direction
for i = 0,limit-1 do begin
    for j = 0,n_elements(x)-1 do begin
        px[j,i,*] = interpol(px[j,i,*],z1,z)
        py[j,i,*] = interpol(py[j,i,*],z1,z)
        pz[j,i,*] = interpol(pz[j,i,*],z1,z)
    endfor
endfor
stop

;Calculate velocity
vx=px/rho
vy=py/rho
vz=pz/rho



;Calculate pressure
eos_table, '/data/seismo/birch/stein/table.dat'

p=rho
p[*]=0

for i=0,limit-1 do begin
    p[*,i,*]=exp(lookup(alog(rho[*,i,*]),e[*,i,*]/rho[*,i,*],iv=0))
endfor


;Convert to cgs
rho=rho*1e-7                    ;10^-7 g/cm^3 -> g/cm^3
vx=vx*1e6                       ;10 km/s -> cm/s
vy=-1*vy*1e6                       ;10 km/s -> cm/s
vz=vz*1e6                       ;10 km/s -> cm/s
p=p*1e5                        ;10^5 dyne/cm^2
Bx=Bx*1e3                      ;kG
By=By*1e3                      ;kG
Bz=Bz*1e3                      ;kG




;Put data in one array
data = fltarr(9,limit,x_size,z_size)

data[0,*,*,*] = transpose(Temp, [1,0,2])
data[1,*,*,*] = transpose(Rho, [1,0,2])
data[2,*,*,*] = transpose(P, [1,0,2])
data[3,*,*,*] = transpose(vx, [1,0,2])
data[4,*,*,*] = transpose(vy, [1,0,2])
data[5,*,*,*] = transpose(vz, [1,0,2])
data[6,*,*,*] = transpose(Bx, [1,0,2])
data[7,*,*,*] = transpose(Bz, [1,0,2])
data[8,*,*,*] = transpose(By, [1,0,2])


;Interpolate to equidistant vertical grid

for i = 0,8 do begin
    for j = 0,x_size-1 do begin
        for k = 0,z_size-1 do begin
            data[i,*,j,k] = interpol(data[i,*,j,k],y[ghost:limit+ghost-1],ynew)
        endfor
    endfor
endfor



;Create FITS-file

writefits,'t'+string(t,format='(I0)')+'_'+string(i,format='(I0)')+string(j,format='(I0)')+'.fits', data
header = headfits('t'+string(t,format='(I0)')+'_'+string(i,format='(I0)')+string(j,format='(I0)')+'.fits')
fxaddpar, header, 'T_X', x[x_size-1]*1e8
fxaddpar, header, 'T_Z', z[z_size-1]*1e8
fxaddpar, header, 'T_Y', (max(ynew)-min(ynew))*1e8
writefits, 't'+string(t,format='(I0)')+'_'+string(i,format='(I0)')+string(j,format='(I0)')+'.fits', data, header


endfor

END
