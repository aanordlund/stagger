;***********************************************************************
  pro transx,n,n1,tau,s,q,xi,doxi
;
;  Solve the transfer equation, given optical depth and source function.
;
;  "Steins trick" is used; storing the sum of the three elements in each
;  row of the tridiagonal equation system, instead of the diagonal ele-
;  ment.  This allows accurate solutions to be otained for arbitrarily
;  small optical depths.  Without this trick, round off errors become
;  noticeable already when the smallest optical depth is less than the
;  square root of the machine precisions.
;
;  This version works with P as a variable for k <= n1, and with
;  Q = P - S for k > n1.  P is Feautriers P and S is the source function.
;  The equation solved is  Q" = P - S for k<=n1, and Q" = Q - S" for k>n1.
;  The answer returned is Q = P - S for all k.
;
;  This choice of equations produces a high precision in Q at large optical
;  depths (where P - S would have round off), while at the same time
;  retaining the precision at small optical depths (where S" becomes very
;  large and eventually would cause round off errors in the back
;  substitution).
;
;  This is a second order version.  For simulations, with rapidly
;  varying absorption coefficients and source functions, this is to be
;  preferred over spline and Hermitean versions because it is positive
;  definite, in the sense that a positive source function is guaranteed
;  to result in a positive average intensity p.  Also, the flux
;  divergence is exactly equal to q, for the conventional definition
;  of the flux.
;
;  Operation count: 5d+5m+11a = 21 flops
;
;  Timings:
;            Alliant: 0.58 * 21 / 1.25 = 9.7 Mfl @ 31*31*31
;
;  Update history:  This routine is based on transq, with updates
;
;***********************************************************************
  mx=(size(tau))[1]
  my=(size(tau))[2]
  mz=(size(tau))[3]

  q=0.*tau
  sp1=0.*tau
  sp2=0.*tau
  sp3=0.*tau
  xi=fltarr(mx+1,my+1)
  ex=0.*xi

  taum=tau[0,0,0]
  for m=0,my-1 do begin
  for l=0,mx-1 do begin
      ;taum=max(taum,tau[l,m,0])
      if (taum lt tau[l,m,0]) then taum=tau[l,m,0]
  endfor
  endfor
  if (taum gt .1) then begin
     for m=0,my-1 do begin
     for l=0,mx-1 do begin
         ex[l,m]=1.-exp(-tau[l,m,0])
     endfor
     endfor
  endif else begin
     for m=0,my-1 do begin
     for l=0,mx-1 do begin
         ex[l,m]=tau[l,m,0] $
                *(1.-.5*tau[l,m,0]*(1.-.3333*tau[l,m,0]))
     endfor
     endfor
  endelse
;
;  Boundary condition at k=0
;
  for m=0,my-1 do begin
  for l=0,mx-1 do begin
      dtau2=tau[l,m,1]-tau[l,m,0]
      sp2[l,m,0]=dtau2*(1.+.5*dtau2)
      sp3[l,m,0]=-1.
      q[l,m,0]=s[l,m,0]*dtau2*(.5*dtau2+ex[l,m])
  endfor
  endfor
;
;  Matrix elements for k>0, k<n-1
;
  for k=1,n-2 do begin
  for m=0,my-1 do begin
  for l=0,mx-1 do begin
      dinv=2./(tau[l,m,k+1]-tau[l,m,k-1])
      sp1[l,m,k]=dinv/(tau[l,m,k-1]-tau[l,m,k])
      sp2[l,m,k]=1.
      sp3[l,m,k]=dinv/(tau[l,m,k]-tau[l,m,k+1])
  endfor
  endfor
  endfor
;
;  Right hand sides, for k>0, k<n1-1
;
  if (n1 gt 2) then n1p=n1 else n1p=2
  if (n1p gt n-3) then n1p=n-3
  ;n1p=min(max(n1-1,1),n-4)
  for k=1,n1p-2 do begin
  for m=0,my-1 do begin
  for l=0,mx-1 do begin
      q[l,m,k]=s[l,m,k]
  endfor
  endfor
  endfor
;
;  RHS for k=n1p-1.  The equation is still in p(k), but q(k+1) is now
;  q(k+1)=p(k+1)-s(k+1), so to get p(k+1) we have to add s(k+1) to the
;  LHS, that is subtract it from the RHS.
;
  if (n1p le n) then begin
     for m=0,my-1 do begin
     for l=0,mx-1 do begin
         q[l,m,n1p-1]=s[l,m,n1p-1]-sp3[l,m,n1p-1]*s[l,m,n1p]
     endfor
     endfor
  endif
;
;  RHS for k=n1p.  The equation is now in Q: Q" = Q - S", but q(k-1)
;  is now p(k-1)=q(k-1)+s(k-1), so we must add sp1*s(l,m,k-1) to the RHS,
;  relative to the case below.
;
  if (n1p+1 le n) then begin
     for m=0,my-1 do begin
     for l=0,mx-1 do begin
         q[l,m,n1p]=sp1[l,m,n1p]*s[l,m,n1p] $
                     +sp3[l,m,n1p]*(s[l,m,n1p]-s[l,m,n1p+1])
     endfor
     endfor
  endif
;
;  k>n1p<n-1 [3d+2m+6a]
;
  for k=n1p+1,n-2 do begin
  for m=0,my-1 do begin
  for l=0,mx-1 do begin
      q[l,m,k]=sp1[l,m,k]*(s[l,m,k]-s[l,m,k-1]) $
              +sp3[l,m,k]*(s[l,m,k]-s[l,m,k+1])
;     0.02+0.18+0.02+0.20+0.20+0.03 -> 0.58*11/0.65 = 9.8 Mfl
  endfor
  endfor
  endfor
;
;  k=n-1
;
  for m=0,my-1 do begin
  for l=0,mx-1 do begin
      sp2[l,m,n-1]=1.
      q[l,m,n-1]=q[l,m,n-2]
  endfor
  endfor
;
;  Eliminate subdiagonal, save factors in sp1 [1d+2m+4a]
;
  for k=0,n-3 do begin
  for m=0,my-1 do begin
  for l=0,mx-1 do begin
      sp1[l,m,k]=-sp1[l,m,k+1]/(sp2[l,m,k]-sp3[l,m,k])
      q[l,m,k+1]=q[l,m,k+1]+sp1[l,m,k]*q[l,m,k]
      sp2[l,m,k+1]=sp2[l,m,k+1]+sp1[l,m,k]*sp2[l,m,k]
      sp2[l,m,k]=sp2[l,m,k]-sp3[l,m,k]
;     0.14+0.13+0.07+0.09 sec -> 0.58*7/0.43 = 9.4 Mfl
  endfor
  endfor
  endfor
  for m=0,my-1 do begin
  for l=0,mx-1 do begin
      sp2[l,m,n-2]=sp2[l,m,n-2]-sp3[l,m,n-2]
  endfor
  endfor
;
;  Backsubstitute [1d+1m+1a]
;
  for k=n-2,0,-1 do begin
  for m=0,my-1 do begin
  for l=0,mx-1 do begin
      q[l,m,k]=(q[l,m,k]-sp3[l,m,k]*q[l,m,k+1])/sp2[l,m,k]
;     0.19 sec = 580444*3/0.19 = 9.2 Mfl
  endfor
  endfor
  endfor
;
;  Subtract S in the region where the equation is in P
;
  for k=0,n1p-1 do begin
  for m=0,my-1 do begin
  for l=0,mx-1 do begin
      q[l,m,k]=q[l,m,k]-s[l,m,k]
  endfor
  endfor
  endfor
;
;  Surface intensity.
;
  if (doxi) then begin
     for l=0,mx-1 do begin
     for m=0,my-1 do begin
         xi[l,m]=(1.-ex[l,m])*(q[l,m,0]+s[l,m,0]) $
                +s[l,m,0]*0.5*ex[l,m]^2
     endfor
     endfor
  endif

  end
;********************************************************************