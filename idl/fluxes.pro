; $Id: fluxes.pro,v 1.7 2015/11/01 04:17:30 aake Exp $
;+
 FUNCTION fluxes, u, file=file, reopen=reopen, verbose=verbose, all=all, _extra=_extra
;
; SYNTAX EXAMPLE:
;
; IDL> s=fluxes(u,file=file) & plot,s.ftot,xrange=[5,s.my-6],xstyle=1,title=s.t & loadct,39
; IDL> while not eof(u) do begin s=fluxes(u,file=file,/re) & oplot,s.ftot,color=80+(s.snap*40 mod 170)
; IDL> help,s,/structure
;
;-
  if n_elements(file) eq 0 then begin print,file & END
  if n_elements(verbose) eq 0 then verbose=0
  if keyword_set(reopen) then begin
    free_lun,u
    openr,u,/get,file
  end else if n_elements(u) eq 0 then begin
    openr,u,/get,file
  end
  if keyword_set(all) then begin
    ff=fluxes(u,file=file,_extra=_extra)
    while not eof(u) do ff=[ff,fluxes(u,file=file,_extra=_extra)]
    return,ff
  end
  my=0
  mf=0
  snap=0
  t=0.
  readf,u,my,mf,snap,t
  name=''
  f=fltarr(my,mf)
  ff=fltarr(my)
  scmd='s={t:t'
  fcmd=',ftot:'
  for i=0,mf-1 do begin
    readf,u,name
    name=strtrim(name,2)
    readf,u,ff
    cmd=name+'=-ff'
    scmd=scmd+','+name+':'+name
    if strmid(name,0,1) eq 'f' then begin
      fcmd=fcmd+'+'+name
    end
    if verbose gt 1 then print,cmd
    void=execute(cmd)
  end
  fcmd=fcmd+'}'
  scmd=scmd+fcmd
  if verbose gt 0 then begin
    print,'t =',t
    print,scmd
  end
  void=execute(scmd)
  ;s={t:t, snap:snap, my:my, ekin:ekin, fvisc:fvisc, fkin:fkin, fconv:fconv, frad:frad, emag:emag, $
  ;   fedif:fedif, fpoynt:fpoynt, qqja:qqja, wk_l:wk_l, ftot:fvisc+fkin+fconv+frad+fpoynt}
  return,s
END
