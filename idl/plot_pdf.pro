default,file,'scratch.dat'
open,b,file,nt=nt,times=times,nv=nv,/swap_if_big
t=nt-1

pwd,cwd=head0
root1=fileroot(head0,tail=tail1,head=head1)
root2=fileroot(head1,tail=tail2,head=head2)
root3=fileroot(head2,tail=tail3,head=head3)

title=tail2+tail1+'   PDF at time ='+str(times[0],format='(f6.2)')
print,title

if !d.name eq 'PS' then setdev,'ps',file='pdf.ps',ysize=12
!P.multi=[0,1,1]

plhist,b(nv*t),/xlog,/ylog,/pdf,title=title
scl=1e-2*exp(0.23*findgen(61))
oplot,scl,scl^(-1.5),line=2
oplot,scl,scl^(-1.0)*0.1,line=1

if !d.name eq 'PS' then device,/close

END
