; $Id: read_prf.pro,v 1.1 2009/11/25 16:15:52 aake Exp $

FUNCTION read_prf, file

  openr,u,/get,file,/f77

  nwaves=0L
  readu,u,nwaves

  waves=fltarr(nwaves)
  readu,u,waves

  nstokes=0L & nprf=0L & nx=0L & ny=0L
  readu,u,nstokes,nprf,nx,ny
  prf=fltarr(nprf,nstokes,nx,ny)
  prof=fltarr(nstokes,nprf,nx)

  print,'nprf, nx, ny =', nprf, nx, ny
  for iy=0,ny-1 do begin
    readu,u,prof
    prf[*,*,*,iy] = transpose(prof,[1,0,2])
  end

  free_lun, u
  return,{nwaves:nwaves, waves:waves, nprf:nprf, nstokes:nstokes, nx:nx, ny:ny, prf:prf}
END
