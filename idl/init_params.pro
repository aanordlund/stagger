pro init_params

common constants,me,mH,kB,hbar,eV,sigmaH_,cl
common params,f_He,chiH,chiH_,TT0,TT0_,rho0,rho0_,ee0,kappa0,sigmaB

f_He=.1
mu=1.+4.*f_He

chiH=13.6*eV
chiH_=.75*eV
TT0=chiH/kB
TT0_=chiH_/kB
rho0=mu*mH*((me/hbar)*(chiH/hbar)/2./!pi)^1.5
rho0_=mu*mH*((me/hbar)*(chiH_/hbar)/2./!pi)^1.5
ee0=chiH/mu/mH
kappa0=sigmaH_/mu/mH
sigmaB=!pi^2*kB^4/60./hbar^3/cl^2

end