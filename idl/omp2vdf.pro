@stagger_6th

PRO vdfout, f, t, level, var, file, shiftit=shiftit
  if keyword_set(shiftit) then begin
    f=shift(temporary(f),shiftit[0],shiftit[1],shiftit[2])
  end
  print,t,'  ',var
  close,1
  openw,1,'tmp.raw'
  writeu,1,f
  close,1
  spawn,'raw2vdf -quiet -ts '+str(t)+' -level '+str(level)+' -varname '+str(var)+' '+str(file)+'.vdf tmp.raw'
END

PRO omp2vdf, ttt, file, extents=extents, lb=lb, ub=ub, level=level, lmax=lmax, shiftit=shiftit, mhd=mhd

default,lmax,1
default,level,0

open,a,file,/swap_if_b,nt=nt,nv=nv

sz=size(a)

default,ttt,[0,nt-1,1]
if n_elements(ttt) eq 1 then ttt=[ttt,ttt,1]
tt=ttt
nt=(tt[1])/tt[2]+1

default,lb,0
default,ub,sz[2]-lb-1

default, extents, '0:0:0:1:1:1'

dim=str(sz[1])+'x'+str(ub-lb+1)+'x'+str(sz[3])
spawn,'vdfcreate -extents '+extents+' -dimension '+str(dim)+' -numts '+str(nt)+ $
 ' -level '+str(lmax)+' -varnames rho:ux:uy:uz:T:bx:by:bz:j2:lnPg:lnPb:Pb '+file+'.vdf

for t=tt[0],tt[1],tt[2] do begin
  print,t

  if not keyword_set(mhd) then begin

    var = 'rho'
    rho=a[nv*t+0]
    f=rho
    vdfout,f,t/tt[2],level,var,file, shiftit=shiftit
    lnr=alog(temporary(rho))

    var = 'ux'
    f= a[nv*t+1]
    f = xup1(temporary(f))/exp(xdn1(lnr))
    vdfout,f,t/tt[2],level,var,file, shiftit=shiftit
    f = 0

    var = 'uy'
    f= a[nv*t+2]
    f = yup1(temporary(f))/exp(ydn1(lnr))
    vdfout,f,t/tt[2],level,var,file, shiftit=shiftit
    f = 0

    var = 'uz'
    f= a[nv*t+3]
    f = zup1(temporary(f))/exp(zdn1(lnr))
    vdfout,f,t/tt[2],level,var,file, shiftit=shiftit
    f = 0


    if (nv eq 6) or (nv eq 9) then begin
      var = 'T'
      tt=a[nv*t+5]
      f=tt
      vdfout,f,t/tt[2],level,var,file, shiftit=shiftit
      f = alog(temporary(tt)) + temporary(lnr)
    end else begin
      f = temporary(lnr)
    end

    var = 'lnPg'
    vdfout,f,t/tt[2],level,var,file, shiftit=shiftit
    f = 0

  end

  var = 'bx'
  f=a[nv*t+nv-3]
  f = xup1(temporary(f))
  pb = f^2
  vdfout,f,t/tt[2],level,var,file, shiftit=shiftit
  f = 0

  var = 'by'
  f=a[nv*t+nv-2]
  f = yup1(temporary(f))
  pb = temporary(pb) + f^2
  vdfout,f,t/tt[2],level,var,file, shiftit=shiftit
  f = 0

  var = 'bz'
  f=a[nv*t+nv-1]
  f = zup1(temporary(f))
  pb = temporary(pb) + f^2
  vdfout,f,t/tt[2],level,var,file, shiftit=shiftit
  f = 0

  var = 'Pb'
  pb = 0.5*temporary(pb)
  f = pb
  vdfout,f,t/tt[2],level,var,file, shiftit=shiftit
  f = 0

  var = 'lnPb'
  pb = alog(temporary(pb))
  vdfout,pb,t/tt[2],level,var,file, shiftit=shiftit

end

spawn,'/bin/rm tmp.raw'

END

