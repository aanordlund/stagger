; $Id: cooling2.pro,v 1.4 2003/03/03 13:53:12 aake Exp $
;***********************************************************************

PRO isearch, xarr,n,x,ilo

  if ((ilo le 0) or (ilo gt n)) then begin
    ilo=0
    ihi=n+1
  endif else begin
    inc=1
    if (x ge xarr(ilo-1)) then begin         ; hunt upwards from initial guess
      if (ilo eq n) then return
      ihi=ilo+1
      while (x ge xarr(ihi-1)) do begin
        ilo=ihi
        inc=inc+1
        ihi=ilo+inc
        if (ihi gt n) then begin
          ihi=n+1
          break
        endif
      endwhile
    endif else begin                         ; hunt downwards from initial guess
      if (ilo eq 1) then begin
        ilo=0
        return
      endif
      ihi=ilo
      ilo=ilo-1
      while (x lt xarr(ilo-1)) do begin
        ihi=ilo
        inc=inc+1
        ilo=ihi-inc
        if (ilo le 0) then begin
          ilo=0
          break
        endif
      endwhile
    endelse
  endelse

  while ((ihi-ilo) ne 1) do begin            ; bisection search
    im=(ihi+ilo)/2
    if (x ge xarr(im-1)) then ilo=im else ihi=im
  endwhile
  ilo=ilo-1                                  ; correction for zero-offset arrays (IDL)
  if (x eq xarr(0)) then ilo=0
  if (x eq xarr(n-1)) then ilo=n-2

END


;----------------------------------------------------------------------------
FUNCTION H2_cool, tt, dd, hh, rr
; - Compute interpolated value in 4D-grid

  common gridsizes , nt, noz, ntcnm, npt, npd, nph, npr
  common gridparams, TN, DN, RH, ROP
  common grid      , WG

  itt=11
  idd=24
  ihh=18
  irr=3
  un  = 1.0d0
  dix =10.0d0

; - Check that the point is inside our grid

  if (tt lt TN(0)) or (tt gt TN(npt-1)) then begin
    print, " Value out of range (tt)!"
    print, " T should be in the range:", dix**TN(0), " - ", dix**TN(npt-1)
  endif

  if (dd lt DN(0)) or (dd gt DN(npd-1)) then begin
    print, " Value out of range (dd)!"
    print, " nH should be in the range:", dix**DN(0), " - ", dix**DN(npd-1)
  endif

  if (hh lt RH(0)) or (hh gt RH(nph-1)) then begin
    print, " Value out of range (hh)"
    print, " H/H2 should be in the range:", dix**RH(0), " - ", dix**RH(nph-1)
  endif

  if (rr lt ROP(0)) or (rr gt ROP(npr-1)) then begin
    print, " Value out of range (rr)"
    print, " O/P should be in the range:", ROP(0), " - ", ROP(npr-1)
  endif

;  Find place in grid

  isearch, TN, npt, tt, itt
  isearch, DN, npd, dd, idd

  ihh=28
  irr=4
  if (hh ne 3.) then begin
    isearch, RH, nph, hh, ihh
  endif
  if (rr ne 1.) then begin
    isearch, ROP, npr, rr, irr
  endif

;  Do a simple linear interpolation

       y0000 = WG[itt  ,idd  ,ihh  ,irr  ]
       y1000 = WG[itt+1,idd  ,ihh  ,irr  ]
       y0100 = WG[itt  ,idd+1,ihh  ,irr  ]
       y1100 = WG[itt+1,idd+1,ihh  ,irr  ]
       y0010 = WG[itt  ,idd  ,ihh+1,irr  ]
       y1010 = WG[itt+1,idd  ,ihh+1,irr  ]
       y0110 = WG[itt  ,idd+1,ihh+1,irr  ]
       y1110 = WG[itt+1,idd+1,ihh+1,irr  ]
       y0001 = WG[itt  ,idd  ,ihh  ,irr+1]
       y1001 = WG[itt+1,idd  ,ihh  ,irr+1]
       y0101 = WG[itt  ,idd+1,ihh  ,irr+1]
       y1101 = WG[itt+1,idd+1,ihh  ,irr+1]
       y0011 = WG[itt  ,idd  ,ihh+1,irr+1]
       y1011 = WG[itt+1,idd  ,ihh+1,irr+1]
       y0111 = WG[itt  ,idd+1,ihh+1,irr+1]
       y1111 = WG[itt+1,idd+1,ihh+1,irr+1]

       tp = (tt - TN[itt])  /   (TN[itt+1] - TN[itt])
       up = (dd - DN[idd])  /   (DN[idd+1] - DN[idd])
       vp = (hh - RH[ihh])  /   (RH[ihh+1] - RH[ihh])
       wp = (rr - ROP[irr]) / (ROP[irr+1] - ROP[irr])

       WH2 = (un-tp) * (un-up) * (un-vp) * (un-wp) * y0000 $
           +    tp   * (un-up) * (un-vp) * (un-wp) * y1000 $
           + (un-tp) *    up   * (un-vp) * (un-wp) * y0100 $
           +    tp   *    up   * (un-vp) * (un-wp) * y1100 $
           + (un-tp) * (un-up) *    vp   * (un-wp) * y0010 $
           +    tp   * (un-up) *    vp   * (un-wp) * y1010 $
           + (un-tp) *    up   *    vp   * (un-wp) * y0110 $
           +    tp   *    up   *    vp   * (un-wp) * y1110 $
           + (un-tp) * (un-up) * (un-vp) *    wp   * y0001 $
           +    tp   * (un-up) * (un-vp) *    wp   * y1001 $
           + (un-tp) *    up   * (un-vp) *    wp   * y0101 $
           +    tp   *    up   * (un-vp) *    wp   * y1101 $
           + (un-tp) * (un-up) *    vp   *    wp   * y0011 $
           +    tp   * (un-up) *    vp   *    wp   * y1011 $
           + (un-tp) *    up   *    vp   *    wp   * y0111 $
           +    tp   *    up   *    vp   *    wp   * y1111

  RETURN, WH2
END


;-------------------------------------------------------------------------
FUNCTION heating, ccnm,z,nH,lognH,nHlt1,logfH2,op

  common parameters, lncV, lnfehsun, noH, logclambu, log2, ln10, invln10

  y1=H2_cool(2.1d0,lognH,logfH2,op)        ; H_2 cooling at ln(T) = 4.835 (126 K)
  y0=H2_cool(2.0d0,lognH,logfH2,op)        ; H_2 cooling at ln(T) = 4.605 (100 K)
  ycmb= 2*y0-y1+nHlt1-logfH2               ; extrapolated H_2 cooling at T ~ 80 K
  cmb = nH*exp(ln10*ycmb) $                ; H_2 cooling
      + nH^2*exp(ln10*(ccnm+z))            ; metal cooling
RETURN, cmb

END


;-----------------------------------------------------------------------------
PRO cooling2, a, b, c, lamb

; Note: if initial conditions are set such that [Z] could be less than
; 1.E-04, the lowest metallicity used in MODULE cooling must be set
; to 1.E-04

; The units of lamda are energy per unit volume and time, or cdu*cvu^2/ctu,
; which the computed, dimensional value should be divided by.  But since
; a multiplicative factor of cdu^2 should also be applied, the computed
; value should be divided by cvu^2/(ctu*cdu), or multiplied by ctu*cdu/cvu^2.

  r=a
  s=size(a)
  mx=s[1]
  my=mx
  mz=mx
  e=b*a
  d=c*a

  common parameters, lncV, lnfehsun, noH, logclambu, log2, ln10, invln10
  lncV=-3.8286
  lnfehsun=6.6280
  noH=0.451
  logclambu=-20.4814
  log2=0.30103
  ln10=2.3025851
  invln10=0.4342945

  common gridsizes , nt, noz, ntcnm, npt, npd, nph, npr
  nt=14
  noz=8
  ntcnm=22
  npt=23
  npd=49
  nph=36
  npr=6
  nptot=243432

  common gridparams, TN, DN, RH, ROP
  common grid      , WG
  TN = dblarr(npt)
  DN = dblarr(npd)
  RH = dblarr(nph)
  ROP= dblarr(npr)
  WG = dblarr(npt,npd,nph,npr)
  dummy = dblarr(nptot)
  ind= -1L
  coolH2file = '../src/COOLING/le_cube'   ; file name

  print, "Loading H_2 cooling grid..."
  openr, 10, coolH2file                   ; open and read cooling file (binary)
  readf, 10, RH
  readf, 10, ROP
  readf, 10, TN
  readf, 10, DN
  readf, 10, dummy
  for k=0,nph-1 do begin
    for l=0,npr-1 do begin
      for i=0,npt-1 do begin
        for j=0,npd-1 do begin
          ind=ind+1;
          WG[i,j,k,l]=dummy[ind]
        endfor
      endfor
    endfor
  endfor
  close, 10                               ; close cooling file
  print, "Done!"

  lnt=fltarr(nt)
  lnt=[9.210, 9.441, 9.556, 9.786, 10.247, 10.822, 11.283, $
       12.319, 13.010, 13.700, 14.276, 15.082, 17.039, 19.572]
  lnz=fltarr(noz)
  lnz=[-9.210, -6.908, -4.605, -3.454, -2.303, -1.151, 0.000, 1.151]
  logl=fltarr(nt,noz)
  logl=[[21.31, 23.82, 24.79, 25.43, 25.02, 24.67, 25.21,  $ ;zero metals
         24.46, 24.19, 24.10, 24.10, 24.16, 24.45, 24.96], $
        [21.31, 23.82, 24.79, 25.43, 25.01, 24.69, 25.22,  $ ;[Fe/H]=-3.0
         24.58, 24.21, 24.12, 24.12, 24.15, 24.46, 24.96], $
        [21.32, 23.82, 24.80, 25.42, 25.02, 24.82, 25.31,  $ ;-2.0
         25.07, 24.49, 24.26, 24.23, 24.18, 24.47, 24.96], $
        [21.35, 23.83, 24.81, 25.43, 25.03, 25.08, 25.47,  $ ;-1.5
         25.51, 24.82, 24.49, 24.43, 24.25, 24.47, 24.96], $
        [21.40, 23.85, 24.82, 25.42, 25.15, 25.43, 25.77,  $ ;-1.0
         25.98, 25.24, 24.84, 24.75, 24.41, 24.48, 24.96], $
        [21.50, 23.88, 24.80, 25.40, 25.26, 25.71, 26.03,  $ ;-0.5
         26.20, 25.46, 25.11, 25.07, 24.60, 24.51, 24.97], $
        [21.63, 24.00, 24.79, 25.42, 25.41, 26.02, 26.33,  $ ;+0.0
         26.40, 25.67, 25.41, 25.42, 24.89, 24.63, 24.95], $
        [21.91, 24.23, 25.01, 25.56, 25.82, 26.48, 26.78,  $ ;+0.5
         26.87, 26.12, 25.88, 25.88, 25.32, 24.85, 25.04]]

  loglcnm=fltarr(ntcnm,2)
  loglcnm=[[1.9, 2.0, 2.1, 2.2, 2.3, 2.4, 2.5, 2.6, 2.7, 2.8, 2.9,  $ ; log(T)
            3.0, 3.1, 3.2, 3.3, 3.4, 3.5, 3.6, 3.7, 3.8, 3.9, 4.0], $
           [-25.91, -25.82, -25.74, -25.66, -25.60, -25.54, -25.48, $ ; log(L)
            -25.43, -25.38, -25.33, -25.28, -25.24, -25.20, -25.16, $
            -25.13, -25.10, -25.08, -25.07, -25.06, -25.06, -25.05, $
            -24.95]]

  tind = 0
  zind = 0
  tcnm = 0
  cgs2cu= logclambu+48.    ; erg cm-3 s-1 -> ceu clu-3 ctu-1
  logfH2= 3.0              ; the log10(n(H)/n(H_2)) ratio in the gas  (-8 < logfH2 < 6)
  logfH2res = 6.0          ; the residual fraction H/H_2 at T = 10 000 K
  op    = 1.               ; the n(ortho)/n(para) ratio of H_2 (0.1 < op < 3)
  sigma2= 0.01             ; width^2 of the H_2 cooling drop at T = 10 000 K

  print, "Calculating cooling..."
  for k=0,mz-1 do begin             ; Calculating a (interpolated) Lambda
    for j=0,my-1 do begin           ; for each (x,y,z) point.
      for i=0,mx-1 do begin
        dd     = d[i,j,k]/r[i,j,k]>0.0
        lnmet  = alog(dd+1.324e-13)+lnfehsun
        logmet = invln10*lnmet               ; logmet > [Fe/H] = -10
        lnmet  = lnmet>(-9.210)              ; lnmet >= [Fe/H] = -4
        lntemp = alog(e[i,j,k]/r[i,j,k])-lncV         ; ln(T)

        if (lntemp lt lnt[0]) then begin     ; Below lnt table min?

          ;--------------------------------------------------------------------
          ; Add cooling by e- and H impact excitation of C+, O, N, Si+, S+,
          ; Fe+, (Dalgarno & McCray 1972, ARA&A, 10, 375) and collisional
          ; excitation of H_2 (Le Bourlot et al. 1999, MNRAS, 305, 802; see
          ; also http://ccp7.dur.ac.uk/) appropriate for the cool neutral
          ; medium (T<10 000 K). Torgny (2003/02/07).
          ;--------------------------------------------------------------------

          logtemp=invln10*lntemp             ; log(T)
          nH=noH*r[i,j,k]                    ; number of H atoms per cm^3
          lognH=invln10*alog(nH)
          nHlt1=0.0
          if (nH lt 1.) then begin           ; lower limit is nH=1. in H_2 cooling grid
            nHlt1=lognH                      ; cooling is nearly linear in rho for low rho
            lognH=0.0                        ; note that lognH+nHlt1 = log10(nH)
          endif

          cmb=heating(loglcnm[0,1],logmet,nH,lognH,nHlt1,logfH2,op)

          ;--------------------------------------------------------------------
          ; The total, low T (CNM) cooling (metal+H_2-cmb) at T=10 000 K is
          ; set such that it equals the high T (WIM+HIM) cooling at T=10 000 K.
          ;--------------------------------------------------------------------

          isearch, lnz, noz, lnmet, zind     ; find the nearest metallicity index
          if (lnmet lt lnz[0]) then begin
            zind=0
            lnmet=lnz[0]
          endif
          if (lnmet gt lnz[noz-1]) then zind=noz-2

          a1=(logl[0,zind+1]-logl[0,zind])/(lnz[zind+1]-lnz[zind])
          a0=logl[0,zind]-a1*lnz[zind]
          Lbound = a1*lnmet+a0-48.                  ; fixed boundary at T = 10 000 K

          ;---------------------------------------------------------------------
          ; There are two options, either, 1.), there is equipartition between
          ; H_2 cooling and metal cooling at T = 10 000 K, or, 2.), the H_2
          ; cooling is set to a residual value mimicking the drop in n(H_2) when
          ; the temperature approaches T = 10 000 K.
          ;---------------------------------------------------------------------

          ; 1.)
;          Lbound = Lbound+invln10*alog(r[i,j,k]^2+cmb*exp(-ln10*Lbound)) $
;                 - 2*(lognH+nHlt1)-log2
;          loglcnm[ntcnm-1,1] = Lbound-logmet          ; boundary value for metal cooling
;          WH2    = H2_cool(4.d0,lognH,logfH2,op,)     ; low-nH correction is substr. in ampl
;          ampl   = Lbound-(WH2-logfH2-lognH)          ; boundary value for H_2 cooling

          ; 2.)
          WH2_0  = H2_cool(4.d0,lognH,logfH2,op)
          WH2_1  = H2_cool(4.d0,lognH,logfH2res,op)
          ampl   = WH2_1-WH2_0+(logfH2-logfH2res)     ; boundary value for H_2 cooling
          WH2_1  = WH2_1-logfH2res+lognH+2*nHlt1
          Lbound = Lbound+invln10*alog(r[i,j,k]^2+exp(-ln10*Lbound) $
                 * (cmb-exp(ln10*WH2_1)))-2*(lognH+nHlt1)
          loglcnm[ntcnm-1,1] = Lbound-logmet          ; boundary value for metal cooling

          ;--------------------------------------------------------------------
          ; Find the contribution of cooling by metals
          ;--------------------------------------------------------------------

          isearch, loglcnm[*,0], ntcnm, logtemp, tcnm
          if (logtemp lt loglcnm[0,0]) then tcnm=0               ; below table min?
          if (logtemp gt loglcnm[ntcnm-1,0]) then tcnm=ntcnm-2   ; above table max?

          a1=(loglcnm[tcnm+1,1]-loglcnm[tcnm,1])/(loglcnm[tcnm+1,0]-loglcnm[tcnm,0])
          a0=loglcnm[tcnm,1]+logmet-a1*loglcnm[tcnm,0]
          lamb[i,j,k]=nH^2*exp(ln10*(a1*logtemp+a0))            ; add metal cooling

          ;--------------------------------------------------------------------
          ; Add the cooling by H_2
          ;--------------------------------------------------------------------

          if (logtemp lt 2.0) then begin             ; temperature >= 100 K ?
            WH2_1=H2_cool(2.1d0,lognH,logfH2,op)     ; H_2 cooling at ln(T) = 4.835 (126 K)
            WH2_0=H2_cool(2.0d0,lognH,logfH2,op)     ; H_2 cooling at ln(T) = 4.605 (100 K)
            WH2_1 = WH2_1+nHlt1                      ; corrected if nH < 1.
            WH2_0 = WH2_0+nHlt1                      ; corrected if nH < 1.
            a1 = 10*(WH2_1-WH2_0)                    ; linear extrapolation in log(T)
            a0 = WH2_0-a1*2.0
            lamb[i,j,k] = lamb[i,j,k] $                         ; add H_2 cooling (for T < 100 K)
                        + nH*exp(ln10*(a1*logtemp+a0-logfH2))
          endif else begin
            WH2=H2_cool(logtemp,lognH,logfH2,op)                ; H_2 cooling at T >= 100 K
            WH2 = WH2+nHlt1                                     ; corrected if nH < 1.
            WH2 = WH2+ampl*exp(-(logtemp-4.0)^2/sigma2)         ; add/substr. to fit boundary
            lamb[i,j,k] = lamb[i,j,k]+nH*exp(ln10*(WH2-logfH2)) ; add H_2 cooling (for T >= 100 K)
          endelse

          ;-----------------------------------------------------------------------
          ;Finally, subtract the heating due to the CMB and convert to code units.
          ;-----------------------------------------------------------------------

          lamb[i,j,k] = lamb[i,j,k]-cmb                    ; substract CMB heating
          lamb[i,j,k] = lamb[i,j,k]*exp(ln10*cgs2cu)       ; erg cm-3 s-1 -> ceu clu-3 ctu-1

        endif else begin                     ; Cooling in a hot ionized medium!

          isearch, lnt, nt, lntemp, tind     ; find the nearest temperature index
          if (lntemp le lnt[0]) then tind=0
          if (lntemp gt lnt[nt-1]) then tind=nt-2

          isearch, lnz, noz, lnmet, zind     ; find the nearest metallicity index
          if (lnmet lt lnz[0]) then begin
            zind=0
            lnmet=lnz[0]
          endif
          if (lnmet gt lnz[noz-1]) then zind=noz-2

; Calculating Lambda at each point, using bi-linear interpolation

          pt = (lntemp-lnt[tind])/(lnt[tind+1]-lnt[tind])
          pz = (lnmet -lnz[zind])/(lnz[zind+1]-lnz[zind])
          qt = 1.-pt
          qz = 1.-pz
          lamb[i,j,k]= r[i,j,k]^2*exp(ln10*( logclambu $
                     + qt*(qz*logl[tind  ,zind]+pz*logl[tind  ,zind+1]) $
                     + pt*(qz*logl[tind+1,zind]+pz*logl[tind+1,zind+1])))

        endelse
      endfor
    endfor
  endfor
  print, "Done!"
END




