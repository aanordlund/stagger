;+
;  $Id: potential_lower.pro,v 1.1 2003/08/18 10:11:25 aake Exp $
 PRO potential_lower, bx, by, bz, lb=lb, phi=phi
;
;  Potential field extrapolation into the ghost zones.  A
;  potential field, periodic in y and z, is exponentially
;  decreasing in the direction away from the boundary, with
;  amplitude factors exp(-x*k_yz) for each Fourier component.
;-
@common_cdata

;  Horizontal wave numbers

  kx  = rebin(reform(wavenumbers(nx,dx=dx),nx,1),nx,nz)
  kz  = rebin(reform(wavenumbers(nz,dx=dz),1,nz),nx,nz)
  kxz = sqrt(kx^2+kz^2)+1e-20
  ci  = complex(0.,1.)

;  Horizontal Fourier transform of the vertical field

  default, lb, 5
  byt = fft(reform(by(*,lb,*)))
  if total(abs(size(phi)-size(by))) ne 0 then phi = make_array(size=size(by))

;  Potential extrapolation
;                             lb
;  phi:   0-+-1-+-2-+-3-+-4-+-5-+-
;   by: 0-+-1-+-2-+-3-+-4-+-5-+-6-

  kxz1 = 1./kxz
  kxz1(0,0) = 0.
  for i=lb-1,0,-1 do begin
    byt = byt*exp(0.5*dy*kxz)
    bxt = -ci*byt*(kx/kxz)*exp(-ci*0.5*dx*kx)
    bzt = -ci*byt*(kz/kxz)*exp(-ci*0.5*dz*kz)
    phi(*,i,*) = -float(fft(/inverse,byt*kxz1))
    bx(*,i,*) = float(fft(/inverse,bxt))
    bz(*,i,*) = float(fft(/inverse,bzt))
    ;print,i,rms(bx(i,*,*)),rms(by(i,*,*)),rms(bz(i,*,*))
    byt = byt*exp(0.5*dy*kxz)
    by(*,i,*) = float(fft(/inverse,byt))
  end
END
