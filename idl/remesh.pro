; $Id: remesh.pro,v 1.4 2007/08/25 23:09:41 aake Exp $

@stagger_6th
PRO remesh, fin, fout, t
;+
;  Remesh a stagger-code data file to twice the resolution.
;  This procedure requires lots of memory!
;-
@common_cdata
  default,fout,'snapshot.dat'

  open,a,fin,nt=nt,nv=nv
  default,t,nt-1

  sz=size(a)
  mx=sz[1] & mx2=mx*2
  my=sz[2] & my2=my*2
  mz=sz[3] & mz2=mz*2

  openw,u,/get,fout
  b=assoc(u,fltarr(mx2,my2,mz2))

  print,'r ...'
  f=a[nv*t]
  f=rebin(f,mx2,my2,mz2)
  b[0]=f

  print,'px ...'
  f=a[1+nv*t]
  f=rebin(f,mx2,my2,mz2)
  f=xup(f)
  b[1]=f

  print,'py ...'
  f=a[2+nv*t]
  f=rebin(f,mx2,my2,mz2)
  f=yup(f)
  b[2]=f

  print,'pz ...'
  f=a[3+nv*t]
  f=rebin(f,mx2,my2,mz2)
  f=zup(f)
  b[3]=f

  if nv eq 5 or nv ge 8 then begin
    print,'e ...'
    f=a[4+nv*t]
    f=rebin(f,mx2,my2,mz2)
    b[4]=f
  end
  f=0

  if nv ge 7 then begin 
    print,'reading b[xyz] ...'
    bx=xup(a[nv-3+nv*t])
    by=yup(a[nv-2+nv*t])
    bz=zup(a[nv-1+nv*t])

    bxa=aver(bx)
    bya=aver(by)
    bza=aver(bz)

    print,'vectorpotential ...'
    vectorpotential,bx,by,bz,ax,ay,az,dx=dx,dy=dy,dz=dz,/verbose

    print,'rebin a[xyz] ...'
    ax=rebin(ax,mx2,my2,mz2)
    ay=rebin(ay,mx2,my2,mz2)
    az=rebin(az,mx2,my2,mz2)

    print,'ax ...'
    ax=ydn(zdn(temporary(ax)))
    by=+ddzup(ax)
    bz=-ddyup(temporary(ax))
    ax=0

    print,'ay ...'
    ay=zdn(xdn(temporary(ay)))
    bx=-ddzup(ay)
    bz=temporary(bz)+ddxup(temporary(ay))
    ay=0

    print,'az ...'
    az=xdn(ydn(temporary(az)))
    bx=temporary(bx)+ddyup(az)
    by=temporary(by)-ddxup(temporary(az))
    az=0

    b[nv-3]=bx+bxa
    b[nv-2]=by+bya
    b[nv-1]=bz+bza
  end

  free_lun,u
END
