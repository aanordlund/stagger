default,t0,0.08
default,mf,300.
default,bin,0.2

print,'t0=',t0,' mf=',mf,' bin=',bin

pwd,cwd=cwd
root=fileroot(cwd,tail=label)

title=label+'   IMF at time ='+str(t0,format='(f6.2)')
print,title

if !d.name eq 'PS' then setdev,'ps',file='imf.ps',ysize=12
!P.multi=[0,1,1]

s=rsink(/swap_if_b,u,t=t,/reop)
while t lt t0 do begin
  s=rsink(/swap_if_b,u,t=t)
end

plhist,mf*s.m,/xlog,/ylog,bin=bin,title=title,xtitle='M/Msun',ytitle='dN/dlnM'

mass=0.01*10.^(0.1*findgen(41))
;oplot,mass,mass^(-1.35)*20.,line=1
oplot,mass,mass^(-1.40)*17.*bin/0.2,line=1
oplot,mass,mass^(-2.40)*17.*bin/0.2,line=1

if !d.name eq 'PS' then device,/close

END
