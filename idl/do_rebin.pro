@stagger_6th

open,a,'data/snapshot.dat',nv=nv,nt=nt
t=nt-1

close,2
openw,2,'data/248.dat'
b=assoc(2,fltarr(248,248,248))

r=a(nv*t)
b(0)=rebin(r,248,248,248)
b(1)=xup(rebin(a(nv*t+1),248,248,248))
b(2)=yup(rebin(a(nv*t+2),248,248,248))
b(3)=zup(rebin(a(nv*t+3),248,248,248))
close,2

END
