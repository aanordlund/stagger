@stagger_6th

open,a,'data/248',nt=nt
urms=fltarr(nt)
for t=0,nt-1 do begin
  lnr=alog(a(4*t))
  u2=(a(4*t+1)/exp(xdn(lnr)))^2
  u2=temporary(u2)+(a(4*t+2)/exp(ydn(lnr)))^2
  u2=temporary(u2)+(a(4*t+3)/exp(zdn(lnr)))^2
  urms[t]=sqrt(aver(u2))
  save,t,urms,file='data/248-urms.idlsave'
end

END

