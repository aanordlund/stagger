; @stagger_6th
FUNCTION fuz,a,t,nvar=nv,rho=rho,first=first,verbose=verbose
@common_cdata
  if n_elements(nv) gt 0 then nvar=nv
  default,nvar,6
  default,t,0
  sz=size(a)
  t0=systime(1)
  uz=a[3+nvar*t]
  t1=systime(1) & if keyword_set(verbose) then print,'read px',t1-t0
  if n_elements(rho) eq n_elements(uz) then scr=alog(rho) else scr=alog(a[t*nvar])
  t2=systime(1) & if keyword_set(verbose) then print,'computed ln(rho)',t2-t1
  if sz[1] lt 500 then begin 
    if keyword_set(first) then scr=zdn1(temporary(scr)) else scr=zdn(temporary(scr))
  end else begin
    for iy=0,sz[2]-1 do begin
      if keyword_set(first) then scr[*,iy,*]=zdn1(scr[*,iy,*]) else scr[*,iy,*]=zdn(scr[*,iy,*])
    end
  end
  t3=systime(1) & if keyword_set(verbose) then print,'shifted ln(rho)',t3-t2
  uz=temporary(uz)/exp(temporary(scr))
  t4=systime(1) & if keyword_set(verbose) then print,'divided w exp(lnr)',t4-t3
  return,uz
END
