PRO plot_power3d,i,simple=simple,xk=xk,pk=pk,_extra=_extra, $
  compensate=compensate,fit=fit,afit=afit,oplot=oplot,file=file

default,i,0
default,file,'power3d.dat'

openr,u,/get,file,_extra=_extra
l=assoc(u,lonarr(2))
nk=l[0,0]
if n_elements(simple) eq 0 then if l[1,0] eq -1 then simple=1 else simple=0
print,'nk =',nk,simple

; from power3d_simple.x
if keyword_set(simple) then begin
  lrec=1+3+nk*4
  p=assoc(u,fltarr(nk),4L*(1+3+lrec*i))

; from power3d.x
end else begin
  lrec=1+6+nk*7
  p=assoc(u,fltarr(nk),4L*(1+6+lrec*i))
end

xk=p(0)
pk=p(1)+p(2)+p(3)
w=where(xk gt 0)
xk=xk[w]
pk=pk[w]
if keyword_set(compensate) then pk=pk*xk^compensate

free_lun,u

if keyword_set(oplot) then begin
  oplot,xk,pk,_extra=_extra,psym=1
end else begin
  plot,xk,pk,/xlog,/ylog,_extra=_extra,psym=1
end

if keyword_set(fit) then begin
  if n_elements(fit) eq 2 then begin
    i=fit
  end else begin
   nk=n_elements(xk)
   i=[4,(nk/15) > 12]
  end
  kfit=alog(xk(i(0)-1:i(1)))
  pfit=alog(pk(i(0)-1:i(1)))
  afit=poly_fit(kfit,pfit,1,yfit)
  oplot,exp(kfit),exp(yfit),thick=2
  oplot,xk,exp(afit[0]+afit[1]*alog(xk)),line=1
  if keyword_set(compensate) then afit[1]=afit[1]-compensate
  print,afit[1]
end

END
