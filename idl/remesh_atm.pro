; $Id: remesh_atm.pro,v 1.2 2007/08/25 23:09:16 aake Exp $
@stagger_atm

FUNCTION rebin2,f,mx2,my2,mz2
  sz=size(f)
  mx=sz[1]
  my=sz[2]
  mz=sz[3]
  f2=rebin(f,mx2,my,mz)
  f2[mx2-1,*,*]=0.5*(f[mx-1,*,*]+f[0,*,*])
  f2=rebin(f2,mx2,my2,mz)
  f2[*,my2-1,*]=0.5*(f2[*,my2-2,*]+f2[*,0,*])
  f2=rebin(f2,mx2,my2,mz2)
  f2[*,*,mz2-1]=0.5*(f2[*,*,mz2-2]+f2[*,*,0])
  return,f2
END

PRO remesh_atm, fin, fout, t, meshfile=meshfile
;+
;  Remesh a stagger-code atmosphere to twice the resolution.
;  This procedure requires lots of memory!
;-
@common_cdata
  default,fout,'snapshot.dat'

  open,a,fin,nt=nt,nv=nv,xm=xm,ym=ym,zm=zm,meshfile=meshfile
  default,t,nt-1

  sz=size(a)
  mx=sz[1] & mx2=mx*2
  my=sz[2] & my2=my*2
  mz=sz[3] & mz2=mz*2

  lb=5 & ub=my-lb-1
  my0=2*(ub-lb)+1+2*5		; double the no. of pts btw ub and lb, add 2x5 ghost zones
  iy0=lb*2-5			; index of 1st point
  iy1=iy0+my0-1                 ; index of last point

  print,mx2,my0,mz2
  print,iy0,iy1

  openw,u,/get,fout
  b=assoc(u,fltarr(mx2,my0,mz2))

  print,'r  ...'
  f=a[nv*t]
  f=alog(temporary(f))
  f=rebin2(temporary(f),mx2,my2,mz2)
  f=exp(temporary(f))
  b[0]=f[*,iy0:iy1,*]

  print,'px ...'
  f=a[1+nv*t]
  f=rebin2(f,mx2,my2,mz2)
  f=xup1(f)
  b[1]=f[*,iy0:iy1,*]

  print,'py ...'
  f=a[2+nv*t]
  f=rebin2(f,mx2,my2,mz2)
  f=yup1(f)
  b[2]=f[*,iy0:iy1,*]

  print,'pz ...'
  f=a[3+nv*t]
  f=rebin2(f,mx2,my2,mz2)
  f=zup1(f)
  b[3]=f[*,iy0:iy1,*]

  if nv eq 5 or nv ge 8 then begin
    print,'e  ...'
    f=a[4+nv*t]
    f=alog(temporary(f))
    f=rebin2(temporary(f),mx2,my2,mz2)
    f=exp(temporary(f))
    b[4]=f[*,iy0:iy1,*]
  end
  f=0

  if nv eq 9 then begin
    print,'T  ...'
    f=a[5+nv*t]
    f=alog(temporary(f))
    f=rebin2(temporary(f),mx2,my2,mz2)
    f=exp(temporary(f))
    b[5]=f[*,iy0:iy1,*]
  end
  f=0

  if nv ge 7 then begin 
    iv=nv-3
    print,'Bx ...'
    f=a[iv+nv*t]
    f=rebin2(f,mx2,my2,mz2)
    f=xup1(f)
    b[iv]=f[*,iy0:iy1,*]

    print,'By ...'
    iv=iv+1
    f=a[iv+nv*t]
    f=rebin2(f,mx2,my2,mz2)
    f=yup1(f)
    b[iv]=f[*,iy0:iy1,*]

    print,'Bz ...'
    iv=iv+1
    f=a[iv+nv*t]
    f=rebin2(f,mx2,my2,mz2)
    f=zup1(f)
    b[iv]=f[*,iy0:iy1,*]
  end
  close,u

  openw,u,'mesh.txt'
  xm2=rebin(xm,mx2)
  zm2=rebin(zm,mz2)
  ; ym2=rebin(ym,my2)
  ym2=intrp1(findgen(my),0.5*findgen(my2),ym)
  dx2=xm2[3]-xm2[2] & xm2=[xm2[1]-dx2,xm2[1:mx2-2],xm2[mx2-2]+dx2]
  dz2=zm2[3]-zm2[2] & zm2=[zm2[1]-dz2,zm2[1:mz2-2],zm2[mz2-2]+dz2]
  ym0=ym2[iy0:iy1]
  printf,u,mx2 & printf,u,xm2
  printf,u,my0 & printf,u,ym0
  printf,u,mz2 & printf,u,zm2
  close,u 

  openw,u,'grid.txt'
  printf,u,mx2,my0,mz2
  printf,u,dx2,dx2,dx2
  printf,u,1.2,0
  printf,u,9
  close,u

  free_lun,u
end
