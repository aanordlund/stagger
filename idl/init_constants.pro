pro init_constants,ulength,udensity,utime

common constants,me,mH,kB,hbar,eV,sigmaH_,cl

me_cgs=9.109390d-28       ;[g]
mH_cgs=1.673525d-24       ;[g]
kB_cgs=1.380658d-16       ;[erg/K]
hbar_cgs=1.054573d-27     ;[erg*s]
eV_cgs=1.602177d-12       ;[erg]
sigmaH_cgs=4.d-17         ;[cm^2]
cl_cgs=2.997925d10        ;[cm/s]

utemperature=1.
uvelocity=ulength/utime
uenergy=uvelocity^2       ;specific energy
umass=udensity*ulength^3

me=me_cgs/umass
mH=mH_cgs/umass
kB=kB_cgs/umass/uenergy*utemperature
hbar=hbar_cgs/umass/uenergy/utime
eV=eV_cgs/umass/uenergy
sigmaH_=sigmaH_cgs/ulength^2
cl=cl_cgs/uvelocity        ;lightspeed

end