function opacity,rho,TT,yymin,kappa0
  common params,f_He,chiH,chiH_,TT0,TT0_,rho0,rho0_,ee0,kap0,sigmaB

  K=(rho0/rho)*(TT/TT0)^1.5*exp(-TT0/TT)
  yy=2./(1.+sqrt(1.+4./K))
  cutoff=where(yy le yymin)
  if (n_elements(cutoff) ge 2) then yy[cutoff]=yymin
  kappa=.25*(rho/rho0_)*(TT0_/TT)^1.5*exp(TT0_/TT)*yy*(1.0-yy)*kappa0
  return,kappa
end

pro transferequation,tau,S,Q,I
  tau_=fltarr(1,1,(size(tau))[1]) & S_=fltarr(1,1,(size(S))[1])
  tau_[0,0,*]=tau & S_[0,0,*]=S
  transx,tau_,S_,Q_,I_
  Q=reform(Q_) & I=reform(I_)
end

function chisq,A
  common params,f_He,chiH,chiH_,TT0,TT0_,rho0,rho0_,ee0,kap0,sigmaB
  common variables,rho,TT,qqrm,z
  common fitting,range
  Amin=1.e-5
  Amax=11.e-5
  deltaA=Amax-Amin
  yymin=Amin+A[0]*deltaA
  Bmin=1.e8
  Bmax=3.e8
  deltaB=Bmax-Bmin
  kappa0=Bmin+A[1]*deltaB

  kappa=opacity(rho,TT,yymin,kappa0)
  tau=integf(z,rho*kappa)
  S=sigmaB*TT^4/!pi
  transferequation,tau,S,Q,I
  qqrm2=Q*rho*kappa
  chisq=0.
  for i=range[0],range[1] do chisq=chisq+(qqrm[i]-qqrm2[i])^2
  return,chisq
end

common params,f_He,chiH,chiH_,TT0,TT0_,rho0,rho0_,ee0,kap0,sigmaB
common variables,rho,TT,qqrm,z
common fitting,range

restore,filename='surfaceheating.sav'

init_constants,ul,ur,ut
init_params

kappa0=1.7072603e8
yymin=1.1e-4

;kappa0=2.3e8
;yymin=0.7e-4

kappa=opacity(rho,TT,yymin,kappa0)
tau=integf(z,rho*kappa)
S=sigmaB*TT^4/!pi
transferequation,tau,S,Q,I
qqrm2=Q*rho*kappa

plot,qqrm
oplot,qqrm2
print,min(qqrm),min(qqrm2)

;A=[.5,.5]
;print,chisq(A)
;temp=amoeba(1.e-4,P0=A,scale=.1,function_name='chisq')
;temp2=amoeba(1.e-4,P0=temp,scale=.1,function_name='chisq')
;best=amoeba(1.e-4,P0=temp2,scale=.1,function_name='chisq')
;print,best
;
;Amin=1.e-5
;Amax=21.e-5
;deltaA=Amax-Amin
;yymin=Amin+best[0]*deltaA
;Bmin=1.e8
;Bmax=3.e8
;deltaB=Bmax-Bmin
;kappa0=Bmin+best[1]*deltaB
;
;print,yymin
;print,kappa0

;range=[20,50]
;n=10
;A=dindgen(n+1)/n
;B=dindgen(n+1)/n
;f=dblarr(n+1,n+1)
;for i=0,n do begin
;for j=0,n do begin
;    f[i,j]=chisq([A[i],B[j]])
;endfor
;endfor
;
;surface,f

end