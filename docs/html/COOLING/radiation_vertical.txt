#======================================================================================
# $Id: radiation_vertical.txt,v 1.1 2004/06/18 06:54:27 aake Exp $

# Radiative transfer along vertical rays only, with form factor for optimiztion.

# 		DEFAULT		  TYPE		MEANING
#--------------------------------------------------------------------------------------
&cool				# name list	cooling parameters
 do_cooling	= T		# logical	cooling or not
 form_factor	= 0.5		# real		form factor, to optimize approximation
 /
