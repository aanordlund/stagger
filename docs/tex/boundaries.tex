\documentclass[12pt]{article}

\newcommand{\ddt}[1]{{\partial #1 \over \partial t}}
\newcommand{\ddj}[1]{{\partial #1 \over \partial x_j}}
\newcommand{\ddi}[1]{{\partial #1 \over \partial x_i}}
\newcommand{\grad}[1]{{\nabla #1}}
\newcommand{\Eq}[1]{Eq.\ \ref{#1}}
\newcommand{\lbc}{{\rm bot}}
\newcommand{\half}{{\small 1/2}}

\begin{document}
\section{Stellar Surface Convection Boundary Conditions}
\subsection{Equation of State Relations}
From the equation of state we may derive relations between thermodynamic
fluctuations at the boundaries.
%------------------------------------------------------------------------------
\subsubsection{Pressure}
We have
%
\begin{equation}\label{eos}
\ln P = F(\ln \rho, E)
\end{equation}
%
and its partial derivatives
%
\begin{equation}\label{eos1}
\left( {\partial \ln P \over \partial \ln \rho} \right)_{E} = F_1(\ln \rho, E)
\end{equation}
%
and
%
\begin{equation}\label{eos2}
\left( {\partial \ln P \over \partial E} \right)_{\ln \rho} = F_2(\ln \rho, E)
\end{equation}
%
The functions $F_1$ and $F_2$ are avialable from the EOS table lookup.

The time evolution of pressure may thus be expressed in terms of density $\rho$
and internal energy per unit mass $E$ by
%
\begin{equation}\label{eos3}
\rho {\partial \ln P \over \partial t} = 
F_1 {\partial \rho \over \partial t} + \rho F_2 {\partial E \over \partial t}  
~,
\end{equation}
%
or in terms of density $\rho$ and internal energy per unit volume $e$ by
%
\begin{equation}\label{eos4}
\rho {\partial \ln P \over \partial t} = 
(F_1-F_2 E) {\partial \rho \over \partial t} + F_2 {\partial e \over \partial t}  
~,
\end{equation}

%------------------------------------------------------------------------------
\subsubsection{Entropy}
Entropy perturbations per unit mass have
%
\begin{equation}\label{eos5}
T {\partial S \over \partial t} = 
{\partial E \over \partial t} - {P \over \rho} {\partial \ln \rho \over \partial t}  
~,
\end{equation}
%
or
%
\begin{equation}\label{eos7}
\rho T {\partial S \over \partial t} = 
{\partial e \over \partial t} 
- (E+{P\over\rho}) {\partial \rho \over \partial t} 
~,
\end{equation}
%
so perturbations with constant $S$ have
%
\begin{equation}\label{eos9}
\left( {\partial e \over \partial \rho} \right)_S = H  
~,
\end{equation}
%
where $H$ is the enthalpy per unit mass
%
\begin{equation}\label{eos10}
H = E +{P\over\rho} 
~.
\end{equation}
%


%------------------------------------------------------------------------------
\subsection{Lower Boundary}
At the lower boundary we implement an open boundary, where inflowing gas has
constant entropy and pressure, and where outflowing gas is free to carry its
entropy fluctuations out, at vanishing horizontal pressure fluctuations.

In addition, we may choose to enforce the mean pressure to be constant (a node
of radial pressure perturbations) or else we may choose to enforce a node in
the average mass flux.

\subsubsection{Velocities}
We assume vanishing (rapidly damped) horizontal velocities in upflows, while
we leave the horizontal components in downflows alone.  If we enforce a node
of the average vertical mass flux we give the inflow a velocity that just
balances the mass flux of the outflow.  The (horizontally averaged) pressure 
must then be allowed to drift, and will take care of itself---an average
downflow towards the boundary increases the pressure there, which reverses
the flow.  This requires that the pressure is extrapolated, so its gradient
can deviate from hydrostatic balance!

The alternative is to assume a constant value of the pressure, in which case
the velocity (horizontally averaged) must be left alone so it can take care
of itself.  An outflow reduces the amount of mass inside, so reduces the amount
of matter that is supported.  If/when the boundary pressure is kept constant
this means that an inflow will be generated, in the next half cycle of an
oscillatory motion. 

Both of these boundary conditions are stable and robust, for good physical
reasons---essentially given by the expression $P(z) = g m(z)$ for hydrostatic
equilibrium.

\subsubsection{Entropy}
Constant entropy of the inflow is an excellent lower boundary condition, since
fluctuation in upflows are small at depth to begin with, and tend to get wiped 
out by expansion of ascending flows.

The entropy in outflows should not be tampered with, so we need do nothing
to $S$ there.  If we need to adjust $\ln\rho$ and $e$ for other reasons, it
should be done in accordance with \Eq{eos9}.

%------------------------------------------------------------------------------
\subsubsection{Constant Pressure}
One alternative at the lower boundary is to enforce constant pressure; since flows
are generally subsonic, the relative pressure fluctuations are much smaller than
the relative density and pressure fluctuations.

%------------------------------------------------------------------------------
\subsubsection{Enforcing the Lower BCs}
In the upflows we enforce a constant entropy and pressure by specifying
values for the density and internal energy;
%
\begin{equation}\label{lbc1}
\rho = \rho_{\lbc}
~,
\end{equation}
%
%
\begin{equation}\label{lbc2}
e = e_{\lbc}
~.
\end{equation}
%
In the outflows, the time derivatives ``proposed" by the equations of motion 
in general implies non-zero values of
%
\begin{equation}\label{lbc6}
\left( 
{\partial \ln P \over \partial t} 
\right)_{\rm PDE} 
= 
F_1 
\left( 
{\partial \ln \rho \over \partial t}  
\right)_{\rm PDE} 
+
F_2 
\left( 
{\partial E \over \partial t} 
\right)_{\rm PDE} 
~.
\end{equation}
%
We denote the values proposed by the partial differential equations
(without boundary conditions) with subscripts PDE, and use the definitions
of the EOS and its partial derivatives to get
%
\begin{equation}\label{lbc7}
\rho 
\left( 
{\partial \ln P \over \partial t} 
\right)_{\rm PDE} 
= 
({F_1}-F_2 E) 
\left( 
{\partial \rho \over \partial t}
\right)_{\rm PDE} 
 +
{F_2} 
\left( 
{\partial e \over \partial t} 
\right)_{\rm PDE} 
~.
\end{equation}
%
To enforce constant pressure we now modify the proposed time derivatives,
by adding contributions that satisfy Eq.\ \ref{eos9}:
%
\begin{equation}\label{lbc10}
\left( 
{\partial e \over \partial t} 
\right)_{\rm add} 
= H
\left( 
{\partial \rho \over \partial t} 
\right)_{\rm add} 
\end{equation}
%
The resulting pressure fluctuation is 
%
\begin{equation}\label{lbc11}
\rho 
\left( 
{\partial \ln P \over \partial t} 
\right)_{\rm add} 
= 
({F_1}-F_2 E + F_2 H) 
\left( 
{\partial \rho \over \partial t}
\right)_{\rm add} 
\end{equation}
%
To cancel the pressure fluctuation that would arise from the PDEs 
and to damp out, over a time $t_{\rm damp}$, a possibly preexisting 
pressure fluctuation we need to choose
\begin{equation}\label{lbc12}
\rho \left( 
{\partial \ln P \over \partial t} 
\right)_{\rm add} 
= -
\rho \left( 
{\partial \ln P \over \partial t} 
\right)_{\rm EOS}
+
\rho \left(
{\ln P_{\lbc}-\ln P
\over t_{\rm damp}} 
\right)
~,
\end{equation}
%
where the last term may be written, using \Eq{eos4}
%
\begin{equation}\label{lbc13}
\rho \left(
{\ln P_{\lbc}-\ln P
\over t_{\rm damp}} 
\right)
=
(F_1 - F_2 E) 
\left(
{\rho_{\lbc}-\rho
\over t_{\rm damp}} 
\right)
+
F_2
\left(
{e_{\lbc}-e
\over t_{\rm damp}} 
\right)
\end{equation}
%
Using \Eq{lbc11} we obtain
%
\begin{equation}\label{lbc14}
\left( 
{\partial \rho \over \partial t}
\right)_{\rm add} =   
F_3
\left[
\left(
{\rho_{\lbc}-\rho
\over t_{\rm damp}} 
\right)
-
\left( 
{\partial \rho \over \partial t}
\right)_{\rm PDE} 
\right]
+
F_4
\left[
\left(
{e_{\lbc}-e
\over t_{\rm damp}} 
\right)
-
\left( 
{\partial e \over \partial t} 
\right)_{\rm PDE} 
\right]
\end{equation}
where we define
%
\begin{equation}\label{lbc14a}
F_3 = {F_1-F_2 E\over {F_1}-F_2 E + F_2 H}
= {F_1-F_2 E\over F_1 + F_2 (P/\rho)}
~,
\end{equation}
and
\begin{equation}\label{lbc22}
F_4 = {F_2\over {F_1}-F_2 E + F_2 H} 
= {F_2\over F_1 + F_2 (P/\rho)} 
~.
\end{equation}
%
The corresponding time derivative of the specific energy is
%
\begin{equation}\label{lbc15}
\left( 
{\partial e \over \partial t}
\right)_{\rm add} =  H
\left( 
{\partial \rho \over \partial t}
\right)_{\rm add} 
\end{equation}
%
\newpage
\subsection{Magnetic Field Boundary Conditions}

\subsubsection{Potential field upper boundary}
Imposing a potential field in the ghost zones makes sense for reasonably strong 
magnetic fields at the upper boundary. For very weak fields a potential extrapolation 
in principle does not make sense, but may work anyway, if care is taken (cf. the 
discussion  below about zapping the electric current). 

Since the field inside the boundary will be entirely under the control
of the motions one expects rather sharp discontinuities in the vertical 
derivatives of the magnetic field components there. 
Sharp discontinuities in the vertical derivatives of the horizontal components
means strong horizontal electric currents, which should tend to smooth out the
discontinuities.  This requires that those currents are allowed to give rise 
to resistive electric fields, and that these are allowed to influence the time
derivatives of the magnetic field in places that lead to smoothing.
The discontinuities would arise between the last layer under the control
of the PDE and the first layer of the potential field extrapolation.  

\subsubsection{Induction equation feedback}
\begin{itemize}
\item
If the first layer of the potential field is at iy=lb-1 then the strong currents
occur in iy=lb-\half.  This gives rise to a resistive electric field there too, 
which may influence the time derivative of the horizontal magnetic field at iy=lb,
which is under PDE control.  This assumes that the resistive electric field
is not discarded in the ghost zones.

\item
If the first layer of the potential field is at iy=lb then the strong currents
occur in iy=lb+\half, where it would enter into the normal PDE and cause the
horizontal magnetic field at iy=lb+1 to try to adjust to the imposed horizontal
field at iy=lb.

The current at lb-\half gives rise to a resistive electric field there too, 
which influences the time derivative of the horizontal magnetic field at iy=lb,
which is under PDE control.  This assumes that the resistive electric field
is not discarded in the ghost zones.
\end{itemize}

\subsubsection{Lorentz force feedback}

\begin{itemize}
\item
If the first layer of the potential field is at iy=lb-1 then the strong currents
at iy=lb-\half may have effects on the Lorentz force.  Strong horizontal currents
at iy=lb-\half are multiplied with the vertical magnetic field component to give
horizontal forces in the perpendicular direction.  This involves interpolation to
iy=lb, so there is a possible feedback loop there, which should be killed by zapping
the electric current at iy=lb-\half!  When multiplied with the perpendicular components
of the horizontal magnetic field the current also gives rise to vertical forces, but
these are primarily at iy=lb-\half, where they are ignored.  Some spill-over may
occur to iy=lb, because of higher order interpolation, unless the current is indeed
zapped.

Zapping the horizontal current at iy=lb-\half implies that the resistive electric
field is also zapped, but this is probably not a problem, since the smoothing
action is not needed, and perhaps is not even desired.  The main function of the
potential field in the ghost zones is to provide a decaying field for the advective
terms in the electric field, and thus prevent unstable inward advection of horizontal
magnetic fields through the upper boundary.

\item
If the first layer of the potential field is at iy=lb then the strong currents
occur in iy=lb+\half, where it would enter into the normal PDE and strong 
accelerations at both iy=lb and iy=lb+1---probably not good!
\end{itemize}

\end{document}
