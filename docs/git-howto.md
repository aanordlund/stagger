# Git Quick Start Tutorial

Git is a light and fast, free and open-source version control system
including features such as staging areas and cheap local branching and 
merging.

Git and the official documentation can be found at:

	https://git-scm.com

For a brief overview of git, please visit:

	https://git-scm.com/about

A useful and simple quick start tutorial can be found at:

	http://rogerdudler.github.io/git-guide/


## Simple user guide

The following is a step-by-step quick-start guide to git. 
At the end of this file, you can find a brief summary with the most 
common commands for typical daily usage.
	
### New repository
To start a new repository, simply execute the following commands:
	
	mkdir /path/to/somedir
	cd /path/to/somedir
	git init

### Checkout a repository:

Create a working copy of a local repository by running the command

	git clone /path/to/repository

when using a remote server, your command will be

	git clone username@host:/path/to/repository


### Workflow

A local repository consists of three "trees" maintained by git:

1. the "Working Directory" which holds the actual files;
2. the "Index" which acts as a staging area;
3. the "HEAD", which points to the last commit you have made.


### Add and commit

You can propose changes (i.e. add them to the Index) using

	git add <filename>
or

	git add *

This is the first step in the basic git workflow. 
To actually commit these changes, use

	git commit -m "Commit message"

Now the file is committed to the HEAD, but it has not been pushed to
your remote repository yet.


### Pushing changes

Your changes are now in the HEAD of your local working copy. 
In order to send those changes to your remote repository, execute

	git push origin master

You can also push the changes to a different branch than the master. 
To do so, simply change "master" in the example above to whatever 
branch you want to push your changes to. 
(For more information about branches, scroll further down.) 

If you have not cloned an existing repository and want to connect your
repository to a remote server, you need to add it with

	git remote add origin <server>

You will then be able to push your changes to the selected remote 
server.


### Branching

Branches are used to develop features isolated from each other. 
The "master" is the default branch when you create a repository. 
Use other branches for development and merge them back to the master
branch upon completion.

Create a new branch named "feature_x" and switch to it using

	git checkout -b feature_x

Switch back to master

	git checkout master

and delete the branch again

	git branch -d feature_x

a branch is not available to others unless you push the branch 
to your remote repository

	git push origin <branch>
	
	
### Update and merge

To update your local repository to the newest commit, execute

	git pull
	
or

	git pull origin
	
in your working directory to fetch and merge remote changes.
You can fetch and integrate new commits from a particular branch with
	
	git pull origin <branch>

(<branch> can be the master too, of course.)

To merge another branch into your active branch (e.g. master), use
	
	git merge <branch>

in both cases git tries to auto-merge changes.

Unfortunately, this is not always possible and conflicts may occur. 
You are responsible to resolve and merge those conflicts manually by
editing the files shown by git. 
After changing, you need to mark them as merged with

	git add <filename>

before merging changes, you can also preview them by using

	git diff <source_branch> <target_branch>


### Status of the working tree

To check the status of the working tree, i.e. to get a summary of what
files have been created, delete, added, or modified, execute:

	git status


### Log

In its simplest form, you can study the repository history using 

	git log

To see only the commits of a certain author:

	git log --author=bob

To see a very compressed log where each commit is one line:

	git log --pretty=oneline

Or maybe you want to see an ASCII art tree of all the branches, 
decorated with the names of tags and branches: 

	git log --graph --oneline --decorate --all

To see only which files have changed: 

	git log --name-status

These are just a few of the possible parameters you can use. 
For more, see:

	git log --help
	

### Replace local changes

In case you did something wrong, you can replace local changes using 
the command

	git checkout -- <filename>

This replaces the changes in your working tree with the last content 
in HEAD. Note that changes already added to the index, as well as new 
files, will be kept.

If you instead want to drop all your local changes and commits, fetch 
the latest history from the server and point your local master branch 
at it like this:

	git fetch origin
	git reset --hard origin/master


### Tagging

Tags can be easily created for software releases. 
This is a known concept, which also exists in CVS and SVN. 
You can create a new tag named 1.0.0 by executing

	git tag 1.0.0 1b2e1d63ff

The 1b2e1d63ff stands for the first 10 characters of the commit id 
you want to reference with your tag. You can get the commit id by 
looking at the output from

	git log


### Create a repository

Last but not least, in order to create a new git repository from
scratch, simply create a new directory, open it (cd to it), and 
perform a 

	git init

------------------------------------------------------------------------

## Summary of most useful commands

Here is a brief summary of the most common commands.

Check out (clone) a repository:
	
	git clone username@host:/path/to/repository

Pull newest commits from (remote) repository, master branch:

	git pull origin master

Check status of working tree (list all local changes):

	git status	

Add (stage) a new or modified file to the Index:

	git add <filename>
	
Alternatively, add all new and modified files to the Index:

	git add *
	
Commit changes to HEAD:

	git commit -m 'My message'
	
Push commits to (remote) repository:

	git push

or

	git push origin
	
Display log (newest commits first):

	git log
	
#### Working with branches, an extended example:
Start with a clean or edited situation on the master, then branch

	git checkout -b some-branch-name
	git commit -m "some comment about <file1>" <file1>
	git commit -m "some comment about <file2>" <file2>
	...
	
After careful vetting and validation of previously running code, execute

	git commit -m "comments on final corrections"
	
If (and only if) the changes are safe to merge into the master branch,
execute the following steps:

	git checkout master
	git pull
	git merge some-branch-name
	git push

If, on the contrary, the committed changes have not been validated and 
are not deemed safe for code integrity, then execute a single

	git push
	
instead, thereby pushing the changes only to the branch and not to the 
master.

It is perfectly fine to keep working on "some-branch-name", with the 
only reason for merging back into the master being if someone else would
benefit from using the changes. If this is not the case, then there is 
no reason to merge.

Changes from the master branch that may have been made, or that are made 
later, can then instead be merged into the some-branch-name via

	git merge master
	
------------------------------------------------------------------------