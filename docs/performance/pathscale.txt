2006-04-21 on steno:

Standard EXPERIMENTS/padoan.in 100^3 MHD test problem, with pathf90 
compiler (this compiler uses byte record lengths).

    OMP-speeds in CPU-microseconds/point

compiler options        1CPU  4CPU   compile time (make -j)

pathf90  -mp -O1        14.6  14.9    5.7s
pathf90  -mp -O2         6.7   7.8     14s
pathf90  -mp -O3         5.6   7.1     52s
pathf90  -mp -Ofast      5.6   7.1    115s

Adding the options "-mcpu=auto -mtune=auto" has no effect.

Compare with

ifort -O2 -opemp         6.8   7.9     56s
ifort -O2 -opemp -xW     4.0   5.4     56s
pgf90 -O2 -mp            6.3   7.6     22s
pgf90 -O2 -fastsse       3.8    -      22s
pgf90 -O2 -fastsse -mp   3.8   5.3     22s

So

  pgf90 -O2 -fastsse -mp

is the fastest (also to compile!), followed by the ~equivalent

  ifort -O2 -xW -openmp 

++ Aake
