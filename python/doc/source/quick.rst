.. _quick:

.. _install:

Getting the source
------------------

From Louis Manchon's Bitbucket repository:

    ``hg clone https://username@bitbucket.org/lmanchon/staggerpy``

From Louis Manchon's Github repository:

	``git clone https://github.com/username/Staggerpy``

From Victor Silva Aguirre's repository:

	``git clone https://github.com/username/staggerpy``

(replace ``username`` with your bitbucket or github username).

Prerequisites
-------------

``staggerpy`` uses several Python packages that are not widely spread such that ``h5py``.
To check if you have all the required softwares, jump into the staggerpy directory and type:

    ``bash ./check_deps.sh``

Please install the missing packages before going further. You can catch them all with the excellent package manager `Anaconda <https://www.continuum.io/downloads>`_.


Install
-------

To install the ``staggerpy`` package, go in staggerpy/python and type

    ``python setup.py install``

Core concepts
-------------

The ``staggerpy`` package is basically divided in two subpackages: ``staggerpy`` and ``staggerpy.gui``. The first one is designed to perform a fast post-processing of a single snapshot or of an entire simulation and write the results in an `HDF5 <https://www.hdfgroup.org/HDF5/>`_ file. The second one is a Graphic User Interface that allows you to draw various types of plots, for a single snapshot or for an entire simulation. 

The following paragraph should give you the basics in order to start using ``staggerpy``.
We consider that you are in a directory containing the simulation ``t45g25p05``, that is to say a star with an effective temperature of 4 500 K, :math:`\log g = 2.5` and a metallicity of +0.5.
This directory contains the following files:
    - ``t45g25p0505.dx``
    - ``t45g25p0505.chk``
    - ``t45g25p0505.msh``
    - ``t45g25p0505_00000.dat``, ..., ``t45g25p0505_00099.dat``
    - ``t45g25p0505_00000.hav``, ..., ``t45g25p0505_00099.hav``
    - ``EOSrhoe_AGSS09+0.50.tab``

The extra ``'05'`` after ``t45g25p05`` is here to specify the version of the simulation. 

.. _sing_snap:

Post-processing of a single snapshot
------------------------------------

**CSnapsh**

The class that you will use here is ``CSnapsh``
The very first thing you'll need to do is create a new instance of this class::

    >>> from staggerpy import CSnapsh, CEOStab
    >>> mdl = CSnapsh('t45g25p0505_00000', 't45g25p05')
    Reading EOSrhoe_AGSS09+0.50.tab ... [Done]
    Reading t45g25p0505_00000.hav ... [Done]
    Reading t45g25p0505.dx ... [Done]
    Reading t45g25p0505.chk ... [Done]
    Reading t45g25p0505.msh ... [Done]
    Reading t45g25p0505_00000.dat ... [Done]

This operation stores in the RAM all the data cubes stored in the ``.dat`` file, as well as some extra informations contains in the other files listed above. 

This class gives you access to a large set of methods that are able to compute all the physical quantities as well as the averages over geometrical and optical depth scales. If you want to create a file containing averaged data, you should call directly the class ``CWrite.py`` (This should change in the next version of ``staggerpy``, see next chapter Advanced Topics -- Developer's Guide).

Here are some of the most used functions:

*Get the velocity cube along a certain direction:*

    >>> mdl.get_vel_cube(0) # velocity along x direction
    array([[[..., skipped for clarity, ...]], dtype=float32)
    >>> mdl.get_vel_cube(1) # velocity along y (depth) direction
    array([[[..., skipped for clarity, ...]], dtype=float32)
    >>> mdl.get_vel_cube(2) # velocity along z direction
    array([[[..., skipped for clarity, ...]], dtype=float32)


*Get an optical depth cube:*

The method involved is ``CSnapsh.get_ltau``. This method is described as follow:

``CSnapsh.get_ltau(self, lnrho = None, lne = None, kind = 'r', taumin = 1.0e-10, vertical = 1, spline = True)``

This method needs two data cubes, the log of the density and of the internal energy per unit mass (``lnrho`` and ``lne``).

**Parameters:**

        **lnrho**  (``np.array``)

            ``log(rho * uur)``.
            If ``None``, compute ``lnrho`` automatically. 

        **lne** (``np.array``)

            ``log(ee * uue)``.
            If ``None``, compute ``lne`` automatically.

        **kind** (``str``)

            - ``'r'``: Rosseland extinction coefficient.
            - ``'p'``: Planck extinction coefficient.
            - ``'r5'``: Rosseland extinction coefficient at 500 nm.

        **taumin** (``float``)

            Minimal value in the output cube.

        **vertical** (``int``)

            If the vertical dimension is, for any reason, not the 2nd dimension (1 in Python numbering), you should specify it explicitly with this argument.  

        **spline** (``bool``)

            If ``False``, linear interpolation, if ``True``, cubic interpolation. 

**Returns:**

Return the logarithm of the selected optical depth.

It can be used as simply as follows: 

    >>> mdl.get_ltau()
    array([[[..., skipped for clarity, ...]], dtype=float32)

This method with no arguments explicitly specified returns the Rosseland optical depth (in logarithm).
If you want the Planck optical depth, just write: 

    >>> mdl.get_ltau(kind = 'p')
    array([[[..., skipped for clarity, ...]], dtype=float32)


*Statistical analysis of a slice:*

In order to retrieve the average value, standard deviation, average value for up - and down-flow etc., at constant depth scale, we use the method ``CSnapsh.sl_stat()``. This method is usually applied on the whole cube in order to get the stratifications. We show here an example of utilisation of this method but this is *not* in the spirit of ``staggerpy``.

    >>> uy = mdl.get_vel_cube(1) # get the vertical velocity
    >>> mdl.sl_stat(uy[:, 0, :], 0, lgm = False, store_indices = True)
    [-3.4743403e-05,
    -0.00051036931,
    0.00022998448,
    0.00051384466,
    0.00051501347,
    -0.0050358241,
    0.0014460328]

Which corresponds, for the first slice of the vertical velocity cube, to: the mean value, the mean value of the upflow, of the downflow, the standard deviation, the rms, the minimum and the maximum value.

Since this method can separate the upflow (uf) and the downflow (df), we should, at some point, store the indices for uf and df. This is done only once when applying ``sl_stat`` on the vertical velocity cube. If we try to skip a slice, things go wrong: 

    >>> mdl.sl_stat(uy[:, 2, :], 2, store_indices = True)
    AttributeError: self.dwfl and self.upfl may not exist. 
    Please store them in memory by calling sl_stat() with the 
    vertical velocity data cube and the option store_indices = True.

Whereas, 

    >>> mdl.dwfl = []; mdl.upflw = [] # re-initialize uf and df indices
    >>> mdl.sl_stat(uy[:, 0, :], 0, lgm = False, store_indices = True)
    [-3.4743403e-05,
    -0.00051036931,
    0.00022998448,
    0.00051384466,
    0.00051501347,
    -0.0050358241,
    0.0014460328]
    >>> mdl.sl_stat(uy[:, 1, :], 1, lgm = False, store_indices = True)
    [4.5762317e-07,
    2.9565017e-06,
    2.9565017e-06,
    3.8525541e-06,
    3.8796047e-06,
    -1.9467405e-05,
    6.359088e-05]
    >>> mdl.sl_stat(uy[:, 2, :], 2, lgm = False, store_indices = True)
    [-4.356617e-06,
    -2.5938763e-05,
    7.6557581e-06,
    3.3023327e-05,
    3.3309174e-05,
    -0.00055438344,
    0.00015152979]
works.

It seems tedious but again, it is not the normal use of ``sl_stat()``. In ``staggerpy``, it is usually call from an other routine, in a loop so you don't have to bother with this. It can be useful to keep in mind that this error exists. (See :ref:`chapter 4 <advanced>` for more details).


**CWriteh5**

(for the future of this class, please read the corresponding section in :ref:`chapter 4 <advanced>`)

The aim of ``CWriteh5`` is just to write the averagings in an hdf5 file. It takes a ``CSnapsh`` instance as argument. 
I suppose that you stayed in the same directory as in the previous section. 

    >>> from staggerpy import CSnapsh, CEOStab, CWriteh5
    >>> mdl = CSnapsh('t45g25p0505_00000', 't45g25p05') # We create the CSnapsh instance
    >>> cw = CWriteh5(mdl)  # Then then CWriteh5 instance
    >>> cw.write_hav_h5()   # Finally, we compute and write the averagings 
    >>> # against geometrical depth in a new file

You now have a new file called ``t45g25p0505_00000.hdf5``. It contains a file system organised as follows (for more details about ``h5py``, please refer to `Andrew Colette's work <https://www.hdfgroup.org/HDF5/>`_):

    >>> import h5py
    >>> f = h5py.File('t45g25p0505_00000.hdf5', 'r') # We open 
    >>> # t45g25p0505_00000.hdf5 in 'reading' mode
    >>> f.keys() # return the first level in the tree file system.
    [u'hav', u'rhavn', u't5havn', u'thavn']

    - ``'hav'`` refers to averages over surfaces of constant geometrical depth;
    - ``'rhavn'`` refers to averages over surfaces of constant column mass density;
    - ``'thavn'`` refers to averages over surfaces of constant Rosseland optical depth;
    - ``'t5havn'`` refers to averages over surfaces of constant optical depth at 500nm

For the moment, we've just filled the ``'hav'`` branch. Indeed, 

    >>> f['hav'].keys() # We look to the 2nd level, stored in the 'hav' branch
    [u'chkfile',
    u'depth',
    u'depth_shiftdn',
    u'depth_shiftup',
    u'dt',
    ...
    u'uzs',
    u'uzu',
    u'uzurms',
    u'vs']
    >>> len(f['hav'].keys()) # It stores 151 arrays
    151
    >>> f['hav/tt'].value # Lets take a look to the mean temperature startification
    array([2470.54272461, 2556.02148438, 2601.36083984, ..., 19826.109375, 
    20038.46875, 20258.33789062], dtype=float32)

In order to fill other the ``'thavn'`` branch, run:

    >>> cw.write_thavn_h5()

.. note::
    The interpolation over optical depth is not well tested. Please see the corresponding section in the :ref:`developer's guide <advanced>`.

.. _whole_sim:

Post-processing of a whole simulation
-------------------------------------

**CSimu**

I assume that you are in the same directory as before (``t45g25p05``). The first thing you have to do is store the path of this directory in a variable. You will have to pass it to CSimu as an argument, it is necessary for parallelized post-processing.

    >>> from staggerpy import CSnapsh, CEOStab, CWriteh5, CSimu
    >>> path = os.getcwd() # get current working directory
    >>> path 
    '/Users/Loulou/Desktop/Louis/Mag2/STAGE/stagger_grid/t45g25p05'
    >>> sim = CSimu(path, 't45g25p05')
    >>> sim.list_snaps 
    >>> # Attribute that contains the list of snapshots present in the directory
    ['t45g25p0505_00000.dat',
    't45g25p0505_00001.dat',
    't45g25p0505_00002.dat',
    't45g25p0505_00003.dat']
    >>> I have only 4 snapshots (raw data) in my directory 
    >>> sim.list_mdl 
    >>> # Attribute that contains the list of the created CSnapsh instances
    [<CSnapsh.CSnapsh at 0x10f71f510>,
    <CSnapsh.CSnapsh at 0x10f71f3d0>,
    <CSnapsh.CSnapsh at 0x10f71f250>,
    <CSnapsh.CSnapsh at 0x10f71f050>]

If the snapshots are already post processed and if you have the corresponding HDF5 files (100 files in my case), you can read this data only by writing: 

    >>> sim.open_all_h5() # Open the hdf5 files
    >>> sim['hav/filename'] # Array containing the names of hdf5 files 
    >>> #contained in the directory
    >>> # you can find same data by writing sim['thavn/filename']
    array(['t45g25p0505_00000.hdf5', 't45g25p0505_00001.hdf5',
    't45g25p0505_00002.hdf5', ... (skipped for clarity) ...
    't45g25p0505_00098.hdf5', 't45g25p0505_00099.hdf5'], dtype=object) 
    >>> sim['hav/teff'] # Effective temperature for each snapshot
    array([4430.47265625, 4425.4013671875, 4422.6787109375, ..., 4461.09521484375,
    4462.47412109375], dtype=object)

If  the snapshots are not already post processed, you can process them individually by writing: 

    >>> sim.process(0) # 1st snapshot
    Reading EOSrhoe_AGSS09+0.50.tab ... [Done]
    Reading t45g25p0505_00000.hav ... [Done]
    Reading t45g25p0505.dx ... [Done]
    Reading t45g25p0505.chk ... [Done]
    Reading t45g25p0505.msh ... [Done]
    Reading t45g25p0505_00000.dat ... [Done]

Or in a similar way, 

    >>> sim.process('t45g25p0505_00002.dat')
    Reading EOSrhoe_AGSS09+0.50.tab ... [Done]
    Reading t45g25p0505_00002.hav ... [Done]
    Reading t45g25p0505.dx ... [Done]
    Reading t45g25p0505.chk ... [Done]
    Reading t45g25p0505.msh ... [Done]
    Reading t45g25p0505_00002.dat ... [Done]

But a smarter use of ``CSimu`` would be writing:

    >>> sim.process_all()
    Reading EOSrhoe_AGSS09+0.50.tab ... [Done]
    Reading t45g25p0505_00000.hav ... [Done]
    Reading t45g25p0505.dx ... [Done]
    ...
    Reading t45g25p0505_00003.dat ... [Done]

At the end of your work, do not forget to write

    >>> sim.close_all_h5()

An unclosed HDF5 file may be the source of many problems...










