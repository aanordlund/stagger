.. _csnapsh:


The CSnapsh class
-----------------

.. todo::
	*What needs to be implemented*

	- Interpolation over following depth scale: of Planck optical depth, monochromatic optical depth, column mass density.
	- Computation of 

		- the entropy; 
		- the coefficient :math:`\alpha` of the MLT;
		- 

	*Bugs*

	- This class is designed to find all the files needed for the post-processing. For instance, the ``.msh`` file contains all the informations about the meshing used in a simulation. There is one ``.msh`` file for each version of the simulation. Let's say you are trying to process a version 05 snapshot and the version 5 mesh file is not in the directory, only a version 4. The class will use this older version for the post processing. **Here come the issue**, the content of those files are generally the same, whatever the version is but it could happen that there are some differences that lead to a wrong analysis.  In orer to solve this issue, one can either refine the way ``CSnapsh`` choose the missing files, or on the contrary, accept to post proccess the simulation, only if the required files are in the directory. I don't know which one is the best solution, the answer should come with a more intensive use of ``staggerpy``.
	- Concerning the ``sl_stat`` method, this method take a horizontal slice from a data cube and return the mean value, the rms, etc. This method is called in a for loop in order to apply it to the whole cube. A much better way to do it would be to $map$ a cube with this method. I dind't found a way to do it efficiently. Therefore, one should, either find a way to do it in Python, or try to write a Cython (or Fortran) routine. The game really worth the candle because it is this function that compute all the averagings that we obtain at the end therefore the code spend its time calling it.
	- Concerning the ``new_ym`` method, there is apparently an issue with the interpolation over a new depth scale. The values have no common point with the one computed before. The issue can come from the interpolation methods used: ``intrp3`` and ``intrp3y``. Optical depths computed from ``get_ltau`` and ``get_ltau_all`` seems correct.


.. class:: CSnapsh(CEOStab)

	Class for post processing a snapshot.

	.. method:: __init__(file, model, path = None, read = True, erase = False, v = '05', dat = 'dat')

		*Parameters*

		file : str
			Snapshot's name (Without the ``.dat`` extension).

		model : str
			Model's name: ``txxgyymzz``.

		path : str, optional
			Path to the folder containing the snapshot. It is advisable to set it explicitly, 
			unless you already are in the right directory.

		read : bool, optional
			If ``False``, doesn't read the files.

		erase : bool, optional
			If ``True``, erases the previously existing ``.hdf5`` file.

		v : str, optional
			Version.

		dat : str, optional
			Snapshot to be read.

		*Raise*

		IOError
			If ``.dat`` or ``.src`` and ``.msh`` files are not in the directory.

		ValueError
			If the ``dat`` parameter is not equal to ``'dat'`` or ``'src'``
	
		*Examples*

    	>>> from staggerpy import CSnapsh
    	>>> cd stagger_grid/t45g25p05
    	>>> mdl = CSnapsh('t45g25p0505_00000', 't45g25p05', read = True)
    		Reading EOSrhoe_AGSS09+0.50.tab ... [Done]
		Reading t45g25p0505_00000.hav ... [Done]
		Reading t45g25p0505.dx ... [Done]
		Reading t45g25p0505.chk ... [Done]
		Reading t45g25p0505.msh ... [Done]
		Reading t45g25p0505_00000.dat ... [Done]
		

	.. method:: __len__()

		Return the size of the box-in-a star.

	.. method:: __getitem__(item)

		Return the item stored in ``.hdf5`` file.
	
		*Parameter*

		item : str
		    path in the ``.hdf5`` file.
	
		*Example*

    	>>> mdl.open_h5(op_mode = 'r')
    	>>> mdl['hav/name']
    		't45g25p05'

	.. method:: open_h5(op_mode = 'a')

		Open the associated ``.hdf5`` file.
	
		*Parameter*

		op_mode : str
			Opening mode.

				========  ================================================
				 r        Readonly, file must exist
				 r+       Read/write, file must exist
				 w        Create file, truncate if it exists
				 w- or x  Create file, fail if it exists
				 a        Read/write if it exists, create otherwise (default)
				========  ================================================
	
		.. seealso:: CSnapsh.close_h5 : Close the associated ``.hdf5`` file.
			

	.. method:: close_h5()

		Close the ``.hdf5`` file
	
		.. seealso:: CSnapsh.open_h5 : Open the associated ``.hdf5`` file.

	.. method:: read(endian = '>')

		Read all usefull files:
	
			* ``.hav`` file (and write the header in ``.hdf5``)
			* ``.dx`` file
			* ``.chk`` file
			* ``.msh`` file
			* ``.dat`` file
	
		*Parameter*

		endian : str
			Endianness.
	
		.. seealso:: 
			- ``CSnapsh.read_eos`` : Read ``.eos`` file.
			- ``CSnapsh.read_dx`` : Read ``.dx`` file.
			- ``CSnapsh.read_chk`` : Read ``.chk`` file.
			- ``CSnapsh.read_hav`` : Read ``.hav`` file.
			- ``CSnapsh.read_msh`` : Read ``.msh`` file.
			- ``CSnapsh.read_dat`` : Read ``.dat`` file.
			- ``CSnapsh.read_dat_cube`` : Read one cube from ``.dat`` file.


	.. method:: read_eos()

		Read ``.eos`` file.


	.. method:: read_dx()

		Read ``.dx`` file.


	.. method:: read_chk()

		Read ``.chk`` file.


	.. method:: read_hav()

		Read ``.hav`` file and write the header in the ``.hdf5`` file.


	.. method:: read_msh(endian = '>')

		Read ``.msh`` file

		*Parameter*

		endian : str
			Endianness.

		*Raise*

		IOError 
			If value of ``self.nxm``, ``self.nym`` and ``self.nzm`` from ``.dx`` file differ from those from ``.msh`` file.


	.. method:: read_dat(endian = '>')

		Read ``.dat`` file.
		Create attributes ``self.rho``, ``self.px``, ``self.py``, ``self.pz``, ``self.e``, ``self.tt``.
		
		*Parameter*

		endian : str
			Endianness. Default: little endian: endian = '>'.



	.. method:: read_dat_cube(var, endian = '>')

		Read only one data cube from ``.dat`` file.
	
		*Parameter*

		var : str
			Name of the data cube you wnat to read.

		endian : str
			Endianness.
	
		*Raise*

		KeyError
			If ``var`` does not match any of the keys of ``var`` (dictionnary stored in ``const.py``).


	.. method:: sl_stat(slc, ind, intensive = True, lgm = True, ud_rms = False, store_indices = False)

		Compute some statistical quantities such as the mean value, rms, standard deviation, 
		min, max, and make the difference between up and down flow.
	
		*Parameter*

		slc : 2d np.array
			Horizontal slice at constant y (``cube[:, i, :]``).

		ind : int
			Index of the horizontal slice.

		intensive : bool
			If intensive physical quantity, set to ``True``, otherwise, ``False``. 
			The mean value is not computed in the same way.

		lgm : bool
			If ``True``, compute logarithmic mean, defined as follows: exp(<log(array)>).

		ud_rms : bool
			If ``True``, compute the rms of the upflow and of the downflow.

		store_indices : bool
			If ``True``, store the indices of the upflow and the downflow.
			Should be set to True only if the cube is the vertical velocity (or if you have a 
			good reason to do so).

		*Raise*

		AttributeError
			If up and down flow indices have not been previously stored.
		


	.. method:: xdn(cube)

		Return ``cube[i - 0.5, j, k]``.


	.. method:: ydn(cube)

		Return ``cube[i, j - 0.5, k]``.


	.. method:: zdn(cube)

		Return ``cube[i, j, k - 0.5]``


	.. method:: xup(cube)

		Return ``cube[i + 0.5, j, k]``.


	.. method:: yup(cube)

		Return ``cube[i, j + 0.5, k]``.


	.. method:: zup(cube)

		Return ``cube[i, j, k + 0.5]``.


	.. method:: get_vel_cube(dir)

		Compute the velocity cube.
	
		*Parameter*

		dir : integer
			``0`` for Ux, ``1`` for Uy, ``2`` for Uz.


	.. method:: get_lnab(lnrho, lne, kind = 'r', linear = False)

		Compute the Rosseland extinction coefficient.

		*Parameters*

		lnrho : np.array
			``log(rho * uur)``.

		lne : np.array
			``log(ee * uue)``.

		kind : str
			- ``'r'``: Rosseland extinction coefficient.
			- ``'p'``: Planck extinction coefficient.
			- ``'r5'``: Rosseland extinction coefficient at 500 nm.

		linear : bool
			If ``True``, linear interpolation, if ``False``, 1st order Taylor developement.

		*Returns*

			log(kappa_r)


	.. method:: get_ltau(lnrho = None, lne = None, kind = 'r', taumin = 1.0e-10, vertical = 1, spline = True)

		Compute one of the optical depth in the following optical depths: rosseland, planck, rosseland at 500nm.

		*Parameters*

		lnrho : np.array
			``log(rho * uur)``.
			If ``None``, compute ``lnrho`` automatically.

		lne : np.array
			``log(ee * uue)``.
			If ``None``, compute ``lne`` automatically.

		kind : str
			- ``'r'``: Rosseland extinction coefficient.
			- ``'p'``: Planck extinction coefficient.
			- ``'r5'``: Rosseland extinction coefficient at 500 nm.

		taumin : float
			Minimal value in the output cube.

		vertical : int
			Dimension containing the vertical values.

		spline : bool
			If ``False``, linear interpolation, if ``True``, cubic interpolation. 

		*Returns*

		log(tau)


	.. method:: get_ltau_all(lnrho = None, lne = None, taumin = 1.0e-10, vertical = 1, spline = True)

		Compute the Rosseland, Planck, Rosseland at 500nm optical depth in the following optical depths.
	
		*Parameters*

		lnrho : np.array
			``log(rho * uur)``.
			If ``None``, compute ``lnrho`` automatically.

		lne : np.array
			``log(ee * uue)``.
			If ``None``, compute ``lne`` automatically.

		kind : str
			- ``'r'``: Rosseland extinction coefficient.
			- ``'p'``: Planck extinction coefficient.
			- ``'r5'``: Rosseland extinction coefficient at 500 nm.

		taumin : float
			Minimal value in the output cube.

		vertical : int
			Dimension containing the vertical values.

		spline : bool
			If ``False``, linear interpolation, if ``True``, cubic interpolation. 

		*Returns*

			log(tau)


	.. method:: geom_der_y(cube, staggered = False)

		Returns the derivatives of a cube cell center defined.

		*Parameter*

		cube : 3d np.array
			Your initial data cube.

		staggered : bool
			``True`` if the cube is defined at the edge of the cells.



	.. method:: intrp3(zt, f, linear = False, staggered = False)

		Interpolate to a new z-grid. 3D version.
		
		*Parameters*

		zt : 1d np.array
			New grid.

		f : 3d np.array
			Old 3d field.

		linear : bool
			``True`` if linear interpolation, ``False`` if spline.

		staggered : bool
			``True`` if the cube is defined at the edge of the cells.

		*Returns*

		New 3d field : 3d np.array

	.. method:: intrp3y(yt, ff, staggered = False)

		Interpolate to a new y-grid. Using ``intrp3`` (z-dir)
		
		*Parameters*

		yt : 1d np.array
			New grid

		f : 3d np.array
			Old 3d field

		staggered : bool
			``True`` if the cube is defined at the edge of the cells.

		*Returns*

		New 3d field




	.. method:: new_ym(new_depth, old_cube, kind = 'optical', linear = False, staggered = False)

		Compute the Rosseland, Planck, Rosseland at 500nm optical depth in the following optical depths.

		*Parameters*

		new_depth : 3d np.array
			Cube containing the new depth scale.

		old_cube : 3d np.array
			Cube containing data to be interpolated.

		kind : str
			- ``'optical'``: if the new depth scale is an optical depth scale. ex: Rosseland optical depth.
			- ``'mass'``: if the new depth scale is an mass depth scale. ex: column mass density.

		linear : bool
			If ``True``, linear interpolation, otherwise spline.

		staggered : bool
			``True`` if the ``old_cube`` is defined at the edge of the cells.

		*Returns*

		``new_cube`` containing the old cube interpolated over the new depth scale.


	.. method:: wgrp_h5(group_name)

		Try to create the group in the ``.hdf5`` file.

		*Parameters*

		group_name : str
			Name of the group to be created.


	.. method:: wdset_val_h5(name, value, group_name, ddtype = 'f4')

		Try to write a single value in the ``.hdf5`` file.

		*Parameters*

		name : str
			Name of the group to be created.

		value : int or float or str
			Value you want to write.

		group_name : str	
			Name of the group in which the value should be copied.

		dtype : str	
			Data type of the value. You should stick to one endianness otherwise, 
			everything will be messed up. By defult we use big endian.	


	.. method:: wdset_arr_h5(name, array, group_name, ddtype = 'f4')

		Try to write an array in the ``.hdf5`` file.

		*Parameters*

		name : str
			Name of the group to be created.

		array : np.array

		group_name : str	
			Name of the group in which the array should be copied

		dtype : str	
			Data type of the np.array. You should stick to one endianness otherwise, 
			everything will be messed up. By defult we use big endian.


	.. method:: wdset_all_h5(name, data, ddtype = 'f4', dset = 'arr')

		Try to write a data in all the groups.

		*Parameters*

		name : str
			Name of the group to be created.

		data : np.array or single value
			Data to be stored.

		group_name : str	
			Name of the group in which the array should be copied.

		dtype : str	
			Data type of the np.array. You should stick to one endianness otherwise, 
			everything will be messed up. By defult we use big endian.

		dset : str
			If ``dset = 'arr'``, call ``self.wdset_arr_h5``, otherwise, call ``self.wdset_val_h5``.


