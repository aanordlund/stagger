.. _csimu:

The CSimu class
------------------

.. class:: CSimu(CSnapsh, CWriteh5)
	
	Class that post processes all the snapshots available for a simulation.
	
	
	.. method:: __init__(path, sim, erase = False, v = '05', read = False, store = True)
		
		*Parameters*

		path : str
			Path to the directory in wich the snapshots are stored.
		sim : str
			Name of the simulations: ``txxgyymzz``.
		erase : bool
			If ``True``, erase the previously existing ``.hdf5`` files.
		v : str
			Number of the version.
		read : bool
			If ``False``, doesn't read the files.
		store : bool
			If ``True``, temporary arrays are stored. This process is quite automatic and you should not, 
			in principle, modify this option.
		

	.. method:: __getitem__(item)
		
			Return the item stored in ``.hdf5`` file.
	
			*Parameter*

			item : str
			    path in the ``.hdf5`` file.
	
			*Examples*

    		>>> sim.open_all_h5()
    		>>> sim['hav/filename']
    		['t45g25p05_00000', 't45g25p05_00001', 't45g25p05_00002', 
    		..., 
    		't45g25p05_00097', 't45g25p05_00098', 't45g25p05_00099']
		

	.. method:: __iter__()
		
	.. method:: __len__()
		
	.. method:: process_all()
		
		This method will process all the snapshots and write the results in an ``.hdf5`` file. 
		

	.. method:: process_i(snaps)
		
		This method will process a snapshot and write the results in an ``.hdf5`` file, given an integer as input.

		*Parameters*

		snaps : int
			Snapshots you want to plot.
		

	.. method:: process_s(snaps)
		
		This method will process a snapshot and write the results in an ``.hdf5`` file, given a string as input.

		*Parameters*

		snaps : str
			Snapshots you want to plot.
		

	.. method:: process(snaps)
		
		This method will process all the snapshots and write the results in an ``.hdf5`` file.

		*Parameters*

		snaps : int or string or list of int or list of string
			Snapshots you want to plot.
		

	.. method:: goback()
		
		This method is used to go back in the directory where you were when then instance was created. 
		This is used for a parallel post processing.
		

	.. method:: open_all_h5(op_mode = 'r')
		
		This method opens all the hdf5 files.

		*Parameter*

		op_mode : str
			Opening mode.

				========  ================================================
				 r        Readonly, file must exist
				 r+       Read/write, file must exist
				 w        Create file, truncate if it exists
				 w- or x  Create file, fail if it exists
				 a        Read/write if it exists, create otherwise (default)
				========  ================================================
		

	.. method:: close_all_h5()
		
		This method closes all the hdf5 files.

		*Parameter*

		op_mode : str
			Opening mode.

				========  ================================================
				 r        Readonly, file must exist
				 r+       Read/write, file must exist
				 w        Create file, truncate if it exists
				 w- or x  Create file, fail if it exists
				 a        Read/write if it exists, create otherwise (default)
				========  ================================================
		
		


