.. _prog_conv:


Programming Conventions
=======================

The following contains the "programming conventions" that we think we should stick to in order to facilitate the development of this code.


Comments
--------

- Explain a difficult algorithm in the block comment above the function, not in bits and pieces through one-line comments inside the function.

- Don't state the obvious in one-line comments.


Identation, white space
-----------------------

- Indentation using 4 spaces. No tabs.

- Use two empty lines between functions.

- Use an empty line to separate semanticcally different sections
  inside a function.

- White space before and after operators.


Variable naming
---------------

- Clear names (``message`` is better than ``msg``), but avoid Hungarion
  notication (``message_str``). Also, don't overdo: use ``i`` in loops,
  and in ``tmp`` versus ``temporary``, the former is almost always
  preferred (or ``temp``), since it is so common (often appended with
  the variable name for which it is temporarily standing in, e.g.
  ``tempx`` or ``tempy``).

- Class names must start with a ``C`` and then a capital letter (ex: ``CSnapsh``).
