.. _csnapshgui:

The CSnapshGUI class
------------------

.. todo::
	- The ``plot_hist2d`` method still needs some improvements, especially regarding the size of the bins.

.. class:: CSnapshGUI(CSnapsh)
	
	Class that post processes the raw data of a snapshot. Data is stored in a Direct-Output Binary File *.dat.

	
	
	.. method:: __init__(file, model, path = None, read = False, v = '05')
		
		*Parameters*

		file : string
			Name of the ``.dat`` file.
		model : str
			Name of the model: ``txxgyymzz``.

		path : str
			Path to the directory in wich the snapshots are stored. It is advisable to set it explicitly, 
			unless you already are in the right directory.
		read : bool
			If ``False``, doesn't read the files.
		v : str
			Version.
		
	.. method:: read(endian = '>')
		
		Alias of ``CSnapsh.read()``.

		*Parameter*

		endian : str
			Endianness.
		
	.. method:: read_msh(endian = '>')
		
		Alias of ``CSnapsh.read_msh()``.

		*Parameter*

		endian : str
			Endianness.


	.. method:: read_dat(endian = '>')
		
		Alias of ``CSnapsh.read_dat()``.

		*Parameter*

		endian : str
			Endianness.


	.. method:: read_dat_cube(var, endian = '>')
		
		Alias of ``CSnapsh.read_dat_cube()``.

		*Parameter*

		endian : str
			Endianness.


	.. method:: read_hav(self)
		
		Alias of ``CSnapsh.read_dat_cube()``.

	.. method:: read_dx(self)
		
		Alias of ``CSnapsh.read_dx()``.


	.. method:: read_chk(self)
		
		Alias of ``CSnapsh.read_chk()``.


	.. method:: plt_slc(slc, vmin = None, vmax = None, cmap=cm.gist_heat, interpolation='nearest', save = False, save_name = 'figure1')
		
		Plot a slice.

		*Parameters*

		slc : 2d np.array
			Slice you want to plot
		vmin : float
			Minimal value displayed. All value < ``vmin`` will have the same color.
		vmax : float
			Maximal value displayed. All value > ``vmin`` will have the same color.	
		cmap : matplotlib.colors.Colormap
			Color map.

			Recommended: ``cm.Greys``, ``cm.gray``, ``cm.gist_heat``, ``cm.bwr``, ``cm.seismic``, ``cm.jet``.

			More at http://matplotlib.org/examples/color/colormaps_reference.html
		interpolation : str
			:red:`Can take the following values: ``'nearest'``, .
		save : bool
			If ``True``, save the plot.
		save_name : string
			Name of the saved plot.
		
	.. method:: phavs(x, y, style = '', color = '', log = None, norm = 1.)
		
		This method plot horizontal average along an other variable.

		*Parameter*

		x : str
			Name of the ``x`` variable. Must be stored in the ``.hdf5`` file associated.
		y : str
			Name of the ``y`` variable. Must be stored in the ``.hdf5`` file associated.
		style : str
			Style of the curve.
		color : str
			Color of the curve.
		log : str
			Must designate the axis that will show logarithmic coordinates.
			Possible arguments: ``None``, ``'X'``, ``'x'``, ``'Y'``, ``'y'``, ``'XY'`` or ``'xy'``.
		norm : float
			Normalization factor.
		
	.. method:: phavc(x, y, mdl2, style = '', color = '', log = None, rel = True, norm = 1.)
		
		This method plot horizontal average along an other variable.

		*Parameter*

		x : str
			Name of the ``x`` variable. Must be stored in the ``.hdf5`` file associated.
		y : str
			Name of the ``y`` variable. Must be stored in the ``.hdf5`` file associated.
		mdl2 : CSnapsh or CSnapshGUI instance
			``mdl - mdl2``.
		style : str
			Style of the curve.
		color : str
			Color of the curve.
		log : str
			Must designate the axis that will show logarithmic coordinates.
			Possible arguments: ``None``, ``'X'``, ``'x'``, ``'Y'``, ``'y'``, ``'XY'`` or ``'xy'``.
		rel : bool
			If ``True``, plot relative value, otherwise, absolute value. 
		norm : float
			Normalization factor.
		

	.. method:: phstat(x, y, log = None, norm = 1.)
		
		This method plot horizontal average along an other variable.

		*Parameter*

		x : str
			Name of the ``x`` variable. Must be stored in the ``.hdf5`` file associated.
		y : str
			Name of the ``y`` variable. Must be stored in the ``.hdf5`` file associated.
		log : str
			Must designate the axis that will show logarithmic coordinates.
			Possible arguments: ``None``, ``'X'``, ``'x'``, ``'Y'``, ``'y'``, ``'XY'`` or ``'xy'``.
		norm : float
			Normalization factor.
		

	.. method:: plot_hist2d(x, y, lim1=None, nbin1=256, lim2=None, nbin2=256, 
		axis=None, bins=None, ctable='Normalize', beta=1., reverse=False)
			
			Plot the 2D histogram of two array.

			*Parameters*

			x : 3D np.array
				Name of the data cube. 
			y : 3D np.array
				Name of the data cube.  
			lim1 : tuple (pair) of float
				Min and max values of ``x`` you want to plot.
			nbin1 : int
				Number of bins.
			lim2 : tuple (pair) of float
				Min and max values of ``y`` you want to plot.
			nbin2 : int
				Number of bins.
			axis : tuple of float (4 items)
				If want not to display the axis, set ``axis = 'off'``.
			bins : pair of int	
				bin sizes
			ctable : str
				Name of the color table.
			beta : float
				Enhanced the contrast.
			reverse : bool
				Reverse the ``y``.
			






