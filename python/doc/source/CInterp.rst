.. _cinterp:

The CInterp class
-----------------

.. todo::
	- Interpolated values are computed using a trilinear interpolation. In order to offer a better precision, one could implement a "triquadric" interpolation.
	- For the moment, this class takes advantages of the fact that models are evenly spread on the 3 dimensionnal parameter space (Teff, log g, [Fe/H]). If the stagger grid is to be extended inhomogenously, the ``find_cube`` method will be to be modified either. 

.. class:: CInterp(CSimu)
	
	Class that compute an interpolated model using the stagger grid.
	
	.. method:: __init__(teff_aim, g_aim, feh_aim)
		
		*Parameters*
		
		teff_aim : float
			Aimed temperature.
		g_aim : float
			Aimed log g.
		feh_aim : float
			Aimed metallicity.
		


	.. method:: __getitem__(item)
		
			Return the item stored in ``.hdf5`` file.
	
			*Parameter*

			item : str
			    path in the ``.hdf5`` file.
	
			*Examples*
	
    			>>> mdl_interp.open_h5(op_mode = 'r')
    			>>> mdl_interp['hav/name']
    			't45g25p05'

			
	.. method:: open_h5(op_mode = 'a')
		
			Open the associated ``.hdf5`` file.
	
			*Parameter*

			op_mode : str
				Opening modes:
	
					========  ================================================
					 r        Readonly, file must exist
					 r+       Read/write, file must exist
					 w        Create file, truncate if it exists
					 w- or x  Create file, fail if it exists
					 a        Read/write if it exists, create otherwise (default)
					========  ================================================

			.. seealso::

				``CSnapsh.close_h5`` : Close the associate ``.hdf5`` file.
			

	.. method:: close_h5()
		
			Close the ``.hdf5`` file
	
			.. seealso::

				``CSnapsh.open_h5`` : Open the associate ``.hdf5`` file.


	.. method:: trilin_interp(values)
		
		Compute a value of the model ``txxgyymzz`` by triliear interpolation.

		*Parameters*
		
		values : nd array
			Values at the edge of the cube
	

	.. method:: find_cube()
		
		This method find the 6 surrounding models in the stagger grid.


	.. method:: wgrp_h5(group_name)
		
		Try to create the group.

		*Parameter*
		
		group_name : str
			Name of the group to be created.
		

	.. method:: wdset_arr_h5(name, array, group_name, ddtype = 'f4')
		
		Try to write an array in the ``.hdf5`` file.

		*Parameters*
		
		name : str
			Name of the group to be created.
		array : np.array
			Array to be written
		group_name : str
			Name of the group in which the array should be copied
		dtype : str
			Data type of the ``np.array``. You should stick to one endianness otherwise, 
			everything will be messed up. By defult we use big endian.	
		

	.. method:: trilin_interp_mmd(teff, logg, feh)
		
		Compute averagings corresponding to the model ``txxgyymzz`` by triliear interpolation.

		*Parameters*
		
		teff : float
			effective temperature
		logg : float
			log gravity at surface
		feh : float
			metallicity

		