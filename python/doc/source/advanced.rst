.. _advanced:

Indication for the developers
-----------------------------

The goal of this chapter is to explain deeper the codes that compose ``staggerpy`` and list the bugs or suggest improvement in the structure or the feature of the code. This is an endless job, therefore you're welcome to ask any question, ask for any improvement, report any bug (preferentially at louis.manchon@u-psud.fr).

**Version number**

Let's take for instance the version 1.2.4.8:

	- First digit, here 1, is increased by 1 when the Stagger-code itself faces a huge modification and the files produced are not readable anymore by ``staggerpy``.
	- Second digit, here 2,  is increased by 1 when the organization of ``staggerpy`` is strongly modified. For instance a new (significantly big) class is introduced.
	- Third digit, here 4, is increased by 1 when a major bug is fixed or major improvement in a routine or ...
	- Fourth digit, here 8, is increased by 1 when a minor bug is fixed or minor improvement in a routine or ...

.. note::
	Those rules are completely open to interpretation.

**Commit tags**

	- DIA: Affects only diagnostics.
	- ORG: Organizational; does not affect results.
	- OPT: Optimize/simplify; does not affect results.
	- MIN: Correct minimal bugs
	- POT: Potentially affects results, depends on parameters or experiment.
	- RES: Affects results, for all experiments or for the specific experiment.
	- BUG: Bug correction; previous results were incorrect, to some extent.
	- COS: Cosmetic changes, i.e commentaries.
	- ADD: Add a new method, function, class, ...
	- REM: Remove a method, function, class, ...

.. note:: Again, those tags are just here to give an hint when looking at the modification history, they're just an indication.

.. todo:: **Concerning the general organization of the code.**

	- ``staggerpy`` is currently Python 2.7 compatible only. A step of great importance would be to make it Python 3.X compatible.
	- One of the goal of ``staggerpy`` is also to synthesize spectra. Therefore, we should rapidly move to the coding of the function needed for that.
	
	**Concerning recurrent issues in the code**

	- ``staggerpy`` makes use of the `h5py package <https://www.hdfgroup.org/HDF5/>`_, which is a Python interface for HDF5 format. This format is very sensitive to the opening/closing file and forgetting to close one of this files can rapidly raise a runtime error. Therefore we should ensure that each time an hdf5 file is open, it is close at the end. An other thing is that if the execution of the code abort for any reason, the hdf5 files stay open and they should be close (*all of them*) before restarting the execution. This is very annoying. 