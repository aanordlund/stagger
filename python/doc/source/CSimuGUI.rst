.. _csimugui:

The CSimuGUI class
------------------



.. class:: CSimuGUI(CSimu)
	
	Class that can draw plots from a whole simulation.
	
	.. method:: __init__(path, sim, erase = False, v = '05', read = False)
		
		*Parameters*

		path : str
			Path to the directory in wich the snapshots are stored. It is advisable to set it explicitly, 
			unless you already are in the right directory.
		sim : str
			Name of the simulations: ``txxgyymzz``.
		erase : bool
			If ``True``, erases the previously existing ``.hdf5`` files.
		v : str
			Number of the version. 
		read : bool
			If ``False``, doesn't read the files.


	.. method:: __getitem__(item)

		return self.sim[item]

	.. method:: __getitem__()
	
		return len(self.sim)

	.. method:: open_all_h5(op_mode = 'r')
		
		This method opens all the hdf5 files.

		*Parameter*

		op_mode : str
			Opening mode.

				========  ================================================
				 r        Readonly, file must exist
				 r+       Read/write, file must exist
				 w        Create file, truncate if it exists
				 w- or x  Create file, fail if it exists
				 a        Read/write if it exists, create otherwise (default)
				========  ================================================
		


	.. method:: process_all()
		
		Alias of ``CSimu.process_all``
		


	.. method:: phavs(x, y, snaps = 'all', style = '', color = '', log = None, norm = 1.)
		
		This method plots the horizontal average along another variable.

		*Parameters*

		x : str
			Name of the ``x`` variable. Must be stored in the ``.hdf5`` file associated.
		y : str
			Name of the ``y`` variable. Must be stored in the ``.hdf5`` file associated.
		snaps : list of int
			List of integers corresponding to the snapshots you want to plot.
		style : str
			Style of the curve.
		color : str
			Color of the curve.
		log : str
			Must designate the axis that will show logarithmic coordinates.
			
			Possible arguments: ``None``, ``'X'``, ``'x'``, ``'Y'``, ``'y'``, ``'XY'`` or ``'xy'``.
		norm : float
			Normalization factor.
		
		






