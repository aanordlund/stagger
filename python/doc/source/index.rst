
staggerpy's documentation
=====================================

.. role:: raw-math(raw)
    :format: latex html

The ``staggerpy`` package is a Python2.7 library for the post processing of the Stagger Grid from the STAGGER Code. 

`The STAGGER Code <http://comp.astro.ku.dk/Twiki/view/CompAstro/StaggerCode>`_ is an MHD and radiative transfer simulation code. This code makes use of a staggered mesh. For more details, see `Å. Nordlund & K. Galsgaard <http://astro.ku.dk/~kg/Papers/MHD_code.ps.gz>`_.
`The Stagger Grid <https://staggergrid.wordpress.com>`_ is a set of experiments performed by `Z. Magic, R. Collet, M. Asplund et al <http://adsabs.harvard.edu/abs/2013A%26A...557A..26M>`_. This grid presents stars with an effective temperature between 4 000 K and 7 000 K in steps of 500 K, :math:`\log g` between 1.5 and 5.0 in steps of 0.5 and :math:`[\rm Fe/H]` between ``−`` 4.0 to ``+`` 0.5 in steps of 1.0 below ``−`` 1.0, and steps of 0.5 above that. The grid also includes 7 solar-temperature and solar-gravity stars with the previous metallicities. In total the Stagger Grid is constituted of 217 simulations.

**Copyright Notice and Statement for the staggerpy Project**


    Copyright (c) 2016-? Louis Manchon, Remo Collet, SAC and contributors
    All rights reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are
    met:

    a. Redistributions of source code must retain the above copyright
       notice, this list of conditions and the following disclaimer.

    b. Redistributions in binary form must reproduce the above copyright
       notice, this list of conditions and the following disclaimer in the
       documentation and/or other materials provided with the
       distribution.

    c. Neither the name of the author nor the names of contributors may 
       be used to endorse or promote products derived from this software 
       without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
    "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
    LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
    A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
    OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
    SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
    LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
    OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.



Where to start
==============

* :ref:`Quick-start guide <quick>`


This documentation is divided into two parts. A :ref:`user's guide <quick>` and a :ref:`developer's guide <advanced>`. It is recommended to start with the first part. 

Many methods and classes are presented in the :ref:`Quick-start guide <quick>`. Please keep in mind that all those methods still contain some bugs and need improvement. All these issues are detailed or should be detailed in the :ref:`developer's guide <advanced>`. 

If you find any bug or need any new feature please email Louis Manchon (louis.manchon@u-psud.fr).


Other resources
===============

This package is available and synchronized on three repositories:
	- `Louis Manchon's Bitbucket repository <https://bitbucket.org/lmanchon/staggerpy>`_
	- `Louis Manchon's Github repository <https://github.com/narsonalin/Staggerpy>`_
	- `Victor Silva Aguirre's Github repository (at SAC) <https://github.com/vsilvagui/staggerpy>`_

Quick Start Guide -- User's Guide
=================================

.. toctree::
    :maxdepth: 2

    quick


Advanced topics -- Developer's Guide
====================================

.. toctree::
    :maxdepth: 2

    advanced
    prog_conv
    CEOStab
    CInterp
    CSnapsh
    CWriteh5
    CSimu
    CSimuGUI
    CSnapshGUI
    todo

.. highlight:: python


