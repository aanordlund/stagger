.. _advanced:

TODO list - A summary
---------------------

**Concerning the general organization of the code**

    - ``staggerpy`` is currently Python 2.7 compatible only. A step of great importance would be to make it Python 3.X compatible.

    - One of the goal of ``staggerpy`` is also to synthesize spectra. Therefore, we should rapidly move to the coding of the function needed for that.

    - Write a configuration file in order to remove the lines ``sys.path.append(...)`` from the source code.


**Concerning recurrent issues in the code**

    - ``staggerpy`` makes use of the `h5py package <https://www.hdfgroup.org/HDF5/>`_, which is a Python interface for HDF5 format. This format is very sensitive to the opening/closing file and forgetting to close one of this files can rapidly raise a runtime error. Therefore we should ensure that each time an hdf5 file is open, it is close at the end. An other thing is that if the execution of the code abort for any reason, the hdf5 files stay open and they should be close (*all of them*) before restarting the execution. This is very annoying.


**CEOStab**

    - This class is entirely translated from several IDL routines therefore it can remains many IDL-like-ways-of-writing-things in the code. 

    - The ``lookup-eos`` method seems improvable (fasten). 


**CInterp**

    - Interpolated values are computed using a trilinear interpolation. In order to offer a better precision, one could implement a "triquadric" interpolation.

    - For the moment, this class takes advantages of the fact that models are evenly spread on the 3 dimensionnal parameter space (Teff, log g, [Fe/H]). If the stagger grid is to be extended inhomogenously, the ``find_cube`` method will be to be modified either. 


**CSnapsh**

*What needs to be implemented*

    - Interpolation over following depth scale: of Planck optical depth, monochromatic optical depth, column mass density.

    - Computation of 
    - the entropy; 
    - the coefficient alpha of the MLT;

*Bugs*

    - This class is designed to find all the files needed for the post-processing. For instance, the ``.msh`` file contains all the informations about the meshing used in a simulation. There is one ``.msh`` file for each version of the simulation. Let's say you are trying to process a version 05 snapshot and the version 5 mesh file is not in the directory, only a version 4. The class will use this older version for the post processing. **Here come the issue**, the content of those files are generally the same, whatever the version is but it could happen that there are some differences that lead to a wrong analysis.  In orer to solve this issue, one can either refine the way ``CSnapsh`` choose the missing files, or on the contrary, accept to post proccess the simulation, only if the required files are in the directory. I don't know which one is the best solution, the answer should come with a more intensive use of ``staggerpy``.

    - Concerning the ``sl_stat`` method, this method take a horizontal slice from a data cube and return the mean value, the rms, etc. This method is called in a for loop in order to apply it to the whole cube. A much better way to do it would be to $map$ a cube with this method. I dind't found a way to do it efficiently. Therefore, one should, either find a way to do it in Python, or try to write a Cython (or Fortran) routine. The game really worth the candle because it is this function that compute all the averagings that we obtain at the end therefore the code spend its time calling it.

    - Concerning the ``new_ym`` method, there is apparently an issue with the interpolation over a new depth scale. The values have no common point with the one computed before. The issue can come from the interpolation methods used: ``intrp3`` and ``intrp3y``. Optical depths computed from ``get_ltau`` and ``get_ltau_all`` seems correct.


**CWriteh5**

    - This class was originally design to 'export' the writing methods outside ``CSnapsh``, for purely aesthetic reasons. It turns out that writing this piece of code as a class doesn't clarify anything. Therefore, we should rewrite this class as separated Python functions.


**CSnapshGUI**

    - The ``plot_hist2d`` method still needs some improvements, especially regarding the size of the bins.
    - Use pcolormesh instaead of imshow in order to plot different size pixels.
    - Add a new routine that can do 3D plots only showing pixels with value under or aboce a certain threshold.
    For instance, plotting only the upflow can help visualize the convective pattern.
    - It could be interesting to can also simply plot a cube with on each face the corresponding values of the box 


**CSimuGUI**

    - Adapt line 19: 
    sys.path.append('/Users/Loulou/Desktop/Louis/Mag2/STAGE/staggerpy/python/staggerpy/')
    So that it can work with any machine.