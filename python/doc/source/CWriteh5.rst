.. _cwriteh5:

The CWriteh5 class
------------------

.. todo::
	- This class was originally design to 'export' the writing methods outside ``CSnapsh``, for purely aesthetic reasons. It turns out that writing this piece of code as a class doesn't clarify anything. Therefore, we should rewrite this class as separated Python functions.


.. class:: CWriteh5()
	
	Class that writes down the data from the averagings performed by ``CSnapsh``.
	
	.. method:: __init__(csnapsh)
		
		*Parameter*

		csnapsh : staggerpy.CSnapsh
			CSnapsh instance.
		


	.. method:: open_h5(op_mode = 'a')
		
		Open the associated ``.hdf5`` file.
	
		*Parameter*
		
		op_mode : str
			Opening mode.

				========  ================================================
				 r        Readonly, file must exist
				 r+       Read/write, file must exist
				 w        Create file, truncate if it exists
				 w- or x  Create file, fail if it exists
				 a        Read/write if it exists, create otherwise (default)
				========  ================================================
	
		.. seealso:: ``CSnapsh.close_h5`` : Close the associate ``.hdf5`` file.

	.. method:: close_h5()
		
		Close the ``.hdf5`` file
	
		.. seealso:: ``CSnapsh.open_h5`` : Open the associate ``.hdf5`` file.

	.. method:: wgrp_h5(group_name)
		
		Try to create the group in the ``.hdf5`` file.

		*Parameters*

		group_name : str
			Name of the group to be created.


	.. method:: wdset_val_h5(name, value, group_name, ddtype = 'f4')
		
		Try to write a single value in the ``.hdf5`` file.

		*Parameters*

		name : str
			Name of the group to be created.
		value : single value (int or float or str)
		group_name : str	
			Name of the group in which the value should be copied
		dtype : str	
			Data type of the value. You should stick to one endianness otherwise, 
			everything will be messed up. By defult we use big endian.	


	.. method:: wdset_arr_h5(name, array, group_name, ddtype = 'f4')
		
		Try to write an array in the ``.hdf5`` file.

		*Parameters*

		name : str
			Name of the group to be created.
		array : np.array
		group_name : str	
			Name of the group in which the array should be copied
		dtype : str	
			Data type of the np.array. You should stick to one endianness otherwise, 
			everything will be messed up. By defult we use big endian.


	.. method:: wdset_all_h5(name, data, ddtype = 'f4', dset = 'arr')
		
		Try to write a data in all the groups.

		*Parameters*

		name : str
			Name of the group to be created.
		data : np.array or single value
			Data to be stored.
		group_name : str	
			Name of the group in which the array should be copied.
		dtype : str	
			Data type of the np.array. You should stick to one endianness otherwise, 
			everything will be messed up. By defult we use big endian.
		dset : str
			If ``dset = 'arr'``, call ``self.wdset_arr_h5``, otherwise, call ``self.wdset_val_h5``.

	.. method:: store_attr()
		
		This method will try to guess if the available RAM is sufficient to store 
		some itermediate arrays in memory and then speed up the code or if they should be deleted after calling each method 



	.. method:: write_hav_h5(store = True)
		
		Write the horizontal averagings at constant geometrical depth.
		
	.. method:: write_thavn_h5()
		
		Write the averagings at constant Rosseland optical depth.
		