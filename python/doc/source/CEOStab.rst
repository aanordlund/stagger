.. _ceostab:

The CEOStab class
-----------------

.. todo:: 
	- This class is entirely translated from a set of IDL routines therefore it remains many IDL-like-ways-of-writing-things in the code. 
	- The ``lookup-eos`` method seems improvable (fasten). 

.. class:: CEOStab()

    Class that read, load and interpolate an EOS table. The bulk of the table, contained in ``tab``, is organized as an ``Netab * Nrtab * 3 * Ntbvar`` array of the ``Ntbvar`` variables: ``lnPtot``, ``ln kap_Ross``, ``lnT``, ``ln kap_Planck``, ``ln kap_500``, and arranged as: the variable, and derivatives w.r.t. ``ln ee``, and ``ln rho``. All table entries are functions of ``lnee`` and ``lnrho` which are both equidistant grids, with extent and resolution as given in the header. All units of quantities in the table are simulation units, as given by ``ul`` (length), ``ur`` (density) and ``ut`` (time) w.r.t. cgs-units.

    .. method:: __init__(tabfile = '', read = True, repair_deriv = True)

		*Parameters*

		tabfile : str
			Name of the opacity table file.
		read : ool
			If ``False``, doesn't read the file.
		repair_deriv : bool
			If ``True``, repair
		
	.. method:: read_eos(endian = '>')

		Read ``.eos`` file. and store its data in memory.

		*Parameter*

		endian : str
			Endianness.

	.. method:: conversion()

		Convert ``tab`` to simulation unit.

	.. method:: repair_deriv(endian = '>')
		
		Repair the ``dlnNe/dlnrho`` derivative of the table and write the repaired tab in new ``tab`` file.

		*Parameter*

		endian : str
			Endianness.
		
	.. method:: lookup_eos(lnrho, lne, j, i, linear = False)
		
		Program to perform linear interpolation in the ``EOSrhoe.tab-tables``.

		*Parameter*	

		lnrho : np.array
			ln(density) in table units (cf. ``ur`` in common ``ceostab``).
		lne : np.array
			ln(internal energy per mass/[sim]).
		j : int
			Index of variable: ``0 = lnPtot``, ``1 = ln kap_Ross``, ``2 = lnT``,
			``3 = ln ne``, ``4 = ln kap_Planck``, ``5 = ln kap_500``.
		i : int
			Derivative index; ``0 = f``, ``1 = df/dlne``, ``2 = df/dlnrho``
		linear : bool
			If ``True``, perform a linear interpolation else Taylor developement.

		*Returns*

			Array of same format as ``lnrho`` and ``lne`` contaning the interpolated values of the 
			specified table-entry.

		*Example*

		lnrho and lne should be in cgs units.
		When used from CSnapsh.py: 
			>>> lookup_eos(np.log(mdl.rho * mdl.uur), np.log(mdl.ee * mdl.uue), ...)
		
		

	.. method:: depth_int(data, depth, minim = 0., vertical = 1, spline = True, logvar = True)
		
		Depth integrate data cube. Returns the new depth scale.

		*Parameters*

		data : np.array
			Data cube.
		depth : np.array
			Geometrical depth.
		minim : float
			Minimum value contained in the new depth cube.
		vertical : int
			Dimension corresponding to the vertical. 
		spline : bool
			If ``False``, linear interpolation.
		logvar : bool
			If ``True``, the data are in log scale.

		*Returns*

			Depth cube with the same shape of the data cube. 
		
		
	.. method:: tau_calc(lnab, depth, taumin = 1.0e-10, vertical = 1, spline = True, logvar = True)
		
		Compute optical depth tau given ln extinction coefficient lnab and geometrical depth ym.

		*Parameters*

		lnab : np.array
			Logarithmic extinction coefficient (usually a cube).
		depth : np.array
			Geometrical depth.
		taumin : float
			Minimum value contained in the new depth cube.
		vertical : int
			Dimension corresponding to the vertical. 
		spline : bool
			If ``False``, linear interpolation.
		logvar : bool
			If ``True``, the data are in log scale.
		


 



