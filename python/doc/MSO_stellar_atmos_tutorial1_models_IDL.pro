; 
; Set environment variable IDL_STARTUP with the name of the IDL startup file:
; 
; Shell=tcsh:
; setenv IDL_STARTUP /priv/munch10/remo/teaching/stellar-atm-2013/models_tutorial/idl_startup.pro
;
; Shell=bash:
; export IDL_STARTUP=/priv/munch10/remo/teaching/stellar-atm-2013/models_tutorial/idl_startup.pro
;
;
; Example: look at model with Teff=5000K, logg=4.5, [Fe/H]=0.0
;
; Root directory with grid models:

rootdir='/priv/munch10/remo/stagger/grid/'

;
; Change to main directory and then to model's directory:
; Notation: {Teff=5000K, logg=4.5, [Fe/H]=0.0} --> 't50g45m00'
; Current version identifier: 'v05'
;

cd, rootdir
cd, 't50g45m00/v05'

; 
; List available IDL save files 
;

$ls -lrt *.sav


;
; Read mean 3D model, with physical variables averaged over time and
; over surfaces of constant optical depth tau at 500nm
;

restore, 'mean_t50g45m00v05.t5havn.sav', /verbose

;;% RESTORE: Portable (XDR) SAVE/RESTORE file.
;;% RESTORE: Save file written by zamag@ran01, Thu Oct 18 04:55:31 2012.
;;% RESTORE: IDL version 8.2 (linux, x86_64).
;;% RESTORE: Restored variable: MMD.


; View content of restored variable (data structure) MMD:

help, mmd, /structure
help, mmd, /str


; View information about some of the variables stored in the data 
; structure:
; Example: depth scale, temperature, optical depth at 500nm, gas pressure

help, mmd.depth
help, mmd.tt
help, mmd.ltau5
help, mmd.pgas


; In the example above, mmd.tt contains 3 arrays with NDEP=101 points.
; The first array (mmd[0].tt) contains the mean temperature stratification
; the other ones (mmd[1].tt and mmd[2].tt) were originally supposed to
; contain the min and max temperature values, just disregard them for now.


; Example: plot T vs. (log) tau scale:

plot, mmd[0].ltau5, mmd[0].tt


; Example: plot (log) gas pressure vs. (log) tau scale
; Overplot electron pressure

plot,  mmd[0].ltau5, alog10(mmd[0].pgas), yr=[-6,2]
oplot, mmd[0].ltau5, alog10(mmd[0].pel)


;
; Units and meaning of labels for all variables?
; See file /priv/munch10/remo/teaching/stellar-atm-2013/models_tutorial/var_names_units.txt
;


;
; Restore time-dependent mean stratifications (i.e. only spatially
; averaged)
;

restore, 'all_t50g45m00v05.t5havn.sav', /verbose
;;% RESTORE: Portable (XDR) SAVE/RESTORE file.
;;% RESTORE: Save file written by zamag@ran01, Thu Oct 18 04:55:31 2012.
;;% RESTORE: IDL version 8.2 (linux, x86_64).
;;% RESTORE: Restored variable: MD.

help, md.tt

nsnap = md[0].nsnap
plot, md[0].ltau5, md[0].tt
for i=0L,nsnap-1L do begin oplot, md[i].ltau5, md[i].tt & wait, 0.2 & endfor

;
; Example: compare the temperature stratifications versus optical depth
; from two different models with different Teff, 5000K and 4500K
; (but everything else the same)
;
; NOTE: remember to copy the data structure to another variable before 
; loading another model, otherwise it is going to be rewritten!
;
; NOTE(2): some combinations of stellar parameters (Teff/logg/[Fe/H]) are 
; not available because they are either unphysical or simply because
; the models were not computed yet.
;

cd, rootdir
restore, 't50g45m00/v05/mean_t50g45m00v05.t5havn.sav', /verbose
mmd1 = mmd
restore, 't45g45m00/v05/mean_t45g45m00v05.t5havn.sav', /verbose
mmd2 = mmd
plot,  mmd1[0].ltau5, mmd1[0].tt
oplot, mmd2[0].ltau5, mmd2[0].tt, col=120

; All the average models with the same extension *.t5havn.sav 
; use the same optical depth scale at 500nm, so you can safely
; take the ratios of various quantities directly without 
; interpolating first to the same reference optical depth scale 

; Example: plot ratio of temperature stratifications from the two
; models as a function of (log) optical depth at 500nm:

ltau5 = mmd1[0].ltau5
oplot, ltau5, mmd1[0].tt/mmd2[0].tt


; Example: compare temperature (and electron pressure) stratification
; for different metallicities

cd,rootdir
restore, 't50g45p05/v05/mean_t50g45p05v05.t5havn.sav', /verbose
mmd05 = mmd
restore, 't50g45m00/v05/mean_t50g45m00v05.t5havn.sav', /verbose
mmd00 = mmd
restore, 't50g45m10/v05/mean_t50g45m10v05.t5havn.sav', /verbose
mmd10 = mmd
restore, 't50g45m20/v05/mean_t50g45m20v05.t5havn.sav', /verbose
mmd20 = mmd
restore, 't50g45m30/v05/mean_t50g45m30v05.t5havn.sav', /verbose
mmd30 = mmd
restore, 't50g45m40/v05/mean_t50g45m40v05.t5havn.sav', /verbose
mmd40 = mmd

ltau5 = mmd00[0].ltau5
loadct, 1
plot, ltau5, mmd00[0].tt, thick=2
oplot,ltau5, mmd05[0].tt, thick=2, linestyle=2
oplot,ltau5, mmd10[0].tt, thick=2, col=240
oplot,ltau5, mmd20[0].tt, thick=2, col=200
oplot,ltau5, mmd30[0].tt, thick=2, col=160
oplot,ltau5, mmd40[0].tt, thick=2, col=120

; same concept as above, but this time plot the electron pressure:

plot, ltau5, mmd00[0].pel, thick=2, /ylog
oplot,ltau5, mmd05[0].pel, thick=2, linestyle=2
oplot,ltau5, mmd10[0].pel, thick=2, col=240
oplot,ltau5, mmd20[0].pel, thick=2, col=200
oplot,ltau5, mmd30[0].pel, thick=2, col=160
oplot,ltau5, mmd40[0].pel, thick=2, col=120


; Example: variation of density-temperature stratification as a function of
; surface gravity (everything else being the same):

cd,rootdir
restore, 't50g45m00/v05/mean_t50g45m00v05.t5havn.sav', /verbose
mmd45 = mmd
restore, 't50g25m00/v05/mean_t50g25m00v05.t5havn.sav', /verbose
mmd25 = mmd
restore, 't50g35m00/v05/mean_t50g35m00v05.t5havn.sav', /verbose
mmd35 = mmd

plot, mmd45[0].rho, mmd45[0].tt, thick=2, /xlog, xtitle='density', ytytle='temperature'
oplot,mmd35[0].rho, mmd35[0].tt, thick=2, col=220
oplot,mmd25[0].rho, mmd25[0].tt, thick=2, col=180




; ---------------------------------------------------------------------------
; Other kinds of averages available:
;
; '*.thavn.sav' : averages over surfaces of constant Rosseland optical depth
; '*.rhavn.sav' : averages over mass column density
; '*.havn.sav'  : horizontal averages
;
;
; '*.havi.sav'  : similar to '.havn.sav' file, contains also radiative flux
; ---------------------------------------------------------------------------




; ---------------------------------------------------------------------------
; 1D models: "Atmo" models, computed with the same EOS and opacity data as
; the 3D models, but with MLT as a description of convective energy transport
; ---------------------------------------------------------------------------

; restore 1D model: works in the same way as for 3D model

restore, 'mean_t50g45m00v05.atmo.sav', /verbose
help, amd, /structure


; The atmo data structures contain 2 models, one for the targeted Teff 
; and one for the actual Teff of the corresponding 3D model

help, amd[0].teff
help, amd[1].teff

cd,rootdir
restore, 't50g45m00/v05/mean_t50g45m00v05.atmo.sav', /verbose
amd00 = amd
restore, 't50g45p05/v05/mean_t50g45p05v05.atmo.sav', /verbose
amd05 = amd
restore, 't50g45m10/v05/mean_t50g45m10v05.atmo.sav', /verbose
amd10 = amd
restore, 't50g45m20/v05/mean_t50g45m20v05.atmo.sav', /verbose
amd20 = amd
restore, 't50g45m30/v05/mean_t50g45m30v05.atmo.sav', /verbose
amd30 = amd
restore, 't50g45m40/v05/mean_t50g45m40v05.atmo.sav', /verbose
amd40 = amd

plot, amd00[0].ltau5,amd00[0].tt, thick=2, xtitle='log tau', ytitle='temp'
oplot,amd05[0].ltau5,amd05[0].tt, thick=2, linestyle=2
oplot,amd10[0].ltau5,amd10[0].tt, thick=2, co=240
oplot,amd30[0].ltau5,amd30[0].tt, thick=2, co=200

; compare last model with corresponding mean 3D modelo

oplot,mmd30[0].ltau5,mmd30[0].tt, thick=2, co=200, linestyle=3




; ---------------------------------------------------------------------------
; EXTRA: 3D models (Stagger)
; ---------------------------------------------------------------------------


; Change to 3D model directory

stagger_dir = '/priv/munch10/magic/grid/'
cd, stagger_dir
cd, 't50g45m00/v05'


; List snapshot and mesh files

$ls -lrt *dat
$ls -lrt *msh



; List EOS and opacity tables

$ls -lrt EOSrhoe* kaprhoT*


;
; Example: Load simulation snapshot and mesh information:
;

open, a, 't50g45m0005_00050.dat', /swap_endian
readmesh, 't50g45m0005.msh', xm=xm, ym=ym, zm=zm, /swap_endian

;
; NOTE: The Stagger code assumes that the y-direction is the vertical!
;
; Therefore, to plot the depth scale, do:

plot, ym


; Load the main data cubes: 
;
; r:  density
; e:  energy density per unit volume
; tt: temperature
;
; ee  = e/r: energy density per unit mass
; lnr = alog(r)

r  = a[0]
e  = a[4]
tt = a[5]

lnr = alog(r)
ee  = e/r


; NOTE:
; All variables are in internal Stagger units. The cgs/internal 
; conversion factors are loaded at startup, and they are:
;
; uur: density
; uul: length
; uut: time
; uuv: velocity
: uue: energy per unit mass
; uup: pressure
; uua: extinction per unit length
; uub: magnetic field (B)
;
; If you want to convert density from internal to cvs units, just do:

rho = r * uur

; NOTE: first layer of temperature cube contains intensity in the vertical
; direction, integrated over the whole spectrum

im = reform( tt[*,0,*] )
image, im


; scatter plot ln density vs. temperature (mind that it can be very slow!)

plot, lnr, tt, ps=3


; Load EOS table
;
; This is a look-up table containing some basic thermodynamic quantities:
;
; iv=0:  (ln) gas+rad pressure
; iv=1:  (ln) Rosseland extinction coefficient
; iv=2:  (ln) temperature 
; iv=3:  (ln) electron number density
; iv=4:  (ln) Planck-function-averaged extinction coeff.
; iv=5:  (ln) standard extinction coefficient at 500nm
;
; all variables are tabulated in cgs units

tabfile='EOSrhoe_AGSS09+0.00.tab'
eostab, tabfile


; Look (gas+rad) pressure up from table
; Note: need to pass slog® and alog(ee) in cgs units to the look-up routine:
; use uur and uue conversion factors (loaded at startup) 

iv   = 0
lnpp = lookup_eos(alog(r*uur),alog(ee*uue),iv,0) ; in cgs units!
lnpp = lnpp - alog(uup)                          ; in internal units


; Look standard extinction coefficient at 500nm from table

iv   = 5
lnab = lookup_eos(lnr+alog(uur),alog(ee*uue),iv,0) ; in cgs units!
lnab = lnab - alog(uua)                            ; in internal units
