# -*- coding: utf-8 -*-
"""
NOTE: This demo should be executed cell-by-cell, by using SHIFT-RETURN in
and iPython GUI such as Spyder (Anaconda)

Created on Wed Apr 18 08:45:20 2018

@author: Aake
"""

import numpy             as np
import matplotlib.pyplot as pl

import stagger_ops       as so; reload(so)
import stagger_utils     as su; reload(su)

datadir='C:/Users/Aake/Dropbox/tutorial/data/120x120x120/sun/test'

#%% Create Stagger data object, with verbose output
s=su.stagger(datadir,verbose=1)

#%% Create Stagger data object
s=su.stagger(datadir)

#%% Display variable by index
pl.figure(1)
pl.clf()
var=2
s.image(iy=50,var=var)
pl.colorbar()
pl.title('s.image   var={}'.format(var))

#%% Display variables by name or index
pl.figure(2,figsize=(18,4))
pl.clf()
i=0
for var in (0,1,'py','T'):
    i+=1
    pl.subplot(1,4,i)
    s.image(iy=50,var=var)
    pl.colorbar()
    pl.title('o.image   var={}'.format(var))
pl.tight_layout()

#%% Display temperature from .src file
pl.figure(1)
pl.clf()
su.image(s.scrvar['T'][:,50,:])
pl.colorbar()
pl.title('su.image')

#%% Display velocity (x,z)-plane from .dat file
pl.figure(1)
pl.clf()
it=0
iy=50
uy=s.dat[2,it]/np.exp(s.xdn(np.log(s.dat[0,it])))
su.image(uy[:,iy,:])
pl.colorbar()
pl.title('Uy    snaphot:{}   iy:{}'.format(it,iy))

#%%  Demonstrating imshow interpolation methods
pl.figure(2,figsize=(18,5))
pl.clf()
i=1
for method in ('default','none','bilinear','bicubic'):
    print i,method
    pl.subplot(1,4,i)
    if i==1:
        su.image(uy[:,50,:])
    else:
        su.image(uy[:,50,:],interpolation=method)
    pl.title("interpolation='{}'".format(method))
    i+=1

#%% Plot horizontal average of temperature from .scr file
pl.figure(1)
pl.clf()
T=s.scrvar['T']
pl.plot(s.mesh.ym[4:-4],T[:,4:-4,:].mean((0,2)))
pl.xlabel('y [Mm]'); pl.ylabel('T [K]')
pl.tight_layout()

#%% Plot horizontal average of density weighted velocity from .scr file
pl.figure(1)
pl.clf()
py=s.scrvar['py']
ddn=np.exp(s.ydn(np.log(s.scrvar['d'])))
uy=py.mean((0,2))/ddn.mean((0,2))
pl.plot(s.mesh.ymdn[4:-4],uy[4:-4])
pl.xlabel('y [Mm]'); pl.ylabel('Uy [km/s]'); pl.title('density weighted vertical velocity')
pl.tight_layout()

#%% Plot horizontal average of vertical mass flux derivative
pl.figure(1)
pl.clf()
py=s.scrvar['py']
dpydy=s.ddyup(py).mean((0,2))
pl.plot(s.mesh.ym[4:-4],dpydy[4:-4])
pl.xlabel('y [Mm]'); pl.title('average vertical mass flux derivative')
pl.tight_layout()
