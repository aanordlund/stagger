## A simple Python interface to Stagger output data
This directory contains a minimalistic, very simple interface to Stagger output data. Basically, all one needs to do to use it is too add this directory to the PYTHONPATH enviroment variable, change directory to a a directory that cointains .dx, .scr, .dat and similar files, and do


```
import stagger_utils as st
s=st.stagger(verbose=True)
```

and then take it from there.
