# -*- coding: utf-8 -*-
"""
    Creates a stagger data object, with attributes filled in with relevant parameters.
    Among them are memory maps stagger.scr[:,:,:,:] and stagger.dat[:,:,:,:,:] into 
    the .scr amd .dat files, and a sub-object stagger.mesh with mesh data.  Use:

    import stagger_utils as st
    ...
    o=st.stagger()                        # read files in the current directory
    o=st.stagger(verbose=1)               # produce verbose output
    o=st.stagger(directory)               # read files in a different directory
    
    st.image(o.scr[5,:,60,:])             # make an image, oriented as in IDL
    o.image(iy=60,iv=5)                   # alternative, using object method

    clf()                                 # clear frame
    plot(o.mesh.ym,o.scr[5].mean((0,2)))  # plot (x,z) average of temperature

    Created on Wed Apr 04 12:30:45 2018
    @author: Aake
"""
import numpy as np
import matplotlib.pyplot as pl
import fortranfile as ff
import os
import stagger_ops as so

def image(im,title=None,**kwargs):
    pl.imshow(im.transpose(),origin='lower',**kwargs)

def describe(self,label='self.'):
    ''' describe (pretty-print) an object '''
    dict=self.__dict__
    keys=dict.keys()
    keys.sort()
    for key in keys:
        #print key,type(dict[key])
        if type(dict[key])==type(dict):
            print '  {:<20}: {}'.format(label+key,'dict')
            d=dict[key]
            for k in d.keys():
                s="{}['{}']".format(label+key,k)
                print "    {:<21}: float{}".format(s,d[k].shape)
        elif np.size(dict[key])<4:
            print '  {:<20}: {}'.format(label+key,dict[key])
        else:
            print '  {:<20}: float{}'.format(label+key,dict[key].shape)

class stagger():
    def __init__(self,dir='.',run='',verbose=0):
        ''' initialize a stagger data object '''
        self.dir=dir+'/'
        files=os.listdir(dir)
        files.sort()
        for file in files:
            if file.startswith('EOS'): self.eos_file=file
            if file.startswith('kap'): self.kap_file=file
        if run=='':
            for file in files:
                if type(verbose)==type(0): 
                    if verbose>1: print file
                if file.endswith ('.dx' ): 
                    self.dx_file =file
                    self.run=file.replace('.dx','')
                if file.endswith ('.scr'): self.scr_file=file
                if file.endswith ('.dat'): self.dat_file=file
                if file.endswith ('.msh'): self.msh_file=file
        else:
            self.run=run
            if os.path.isfile(run+'.dx' ): self.dx_file =run+'.dx'
            if os.path.isfile(run+'.msh'): self.msh_file=run+'.msh'
            if os.path.isfile(run+'.scr'): self.scr_file=run+'.scr'
            if os.path.isfile(run+'.dat'): self.dat_file=run+'.dat'
        self._read_ds()
        self._read_msh(verbose=verbose)
        self._map_scr()
        self._map_data()
        if verbose:
            describe(self,'stagger.')

    def _read_ds(self):
        ''' read and interpret dx_file '''
        if 'dx_file' in dir(self):
            with open(self.dir+self.dx_file,'r') as fp:
                self.n=map(int,fp.readline().split())
                self.ds=map(np.float,fp.readline().split())
                cols=fp.readline().split()
                self.gamma=np.float(cols[0])

    def _read_msh(self,verbose=0):
        ''' read and interpret msh_file '''
        if 'msh_file' in dir(self):
            class mesh():
                pass
            self.mesh=mesh()
            with ff.FortranFile(self.dir+self.msh_file,'rb',endian='>') as fp:
                n = fp.readInts()
                self.mesh.n=n
                buf=fp.readReals().reshape(6,n[0])
                self.mesh.dxm  =buf[0]
                self.mesh.dxmdn=buf[1]
                self.mesh.xm   =buf[2]
                self.mesh.xmdn =buf[3]
                buf=fp.readReals().reshape(6,n[1])
                self.mesh.dym  =buf[0]
                self.mesh.dymdn=buf[1]
                self.mesh.ym   =buf[2]
                self.mesh.ymdn =buf[3]
                self.mesh.dyup =buf[4]
                self.mesh.dydn =buf[5]
                buf=fp.readReals().reshape(6,n[2])
                self.mesh.dzm  =buf[0]
                self.mesh.dzmdn=buf[1]
                self.mesh.zm   =buf[2]
                self.mesh.zmdn =buf[3]
            if verbose:
                describe(self.mesh,'stagger.mesh.')

    def _map_scr(self,verbose=0):
        ''' map the scr_file to memory '''
        if 'scr_file' in dir(self):
            tmp=np.memmap(self.dir+self.scr_file,mode='r',dtype=np.float32)
            self.nvar=np.size(tmp)/np.product(self.n)
            shape=tuple(self.n)+(self.nvar,)
            self.scr=np.memmap(self.dir+self.scr_file,shape=shape,mode='r',
                               order='F',dtype=np.float32).byteswap().transpose(3,0,1,2)
            self.scrvar={}
            self.scrvar['d'] =self.scr[0]
            self.scrvar['px']=self.scr[1]
            self.scrvar['py']=self.scr[2]
            self.scrvar['pz']=self.scr[3]
            self.scrvar['e'] =self.scr[4]
            self.scrvar['T'] =self.scr[5]

    def _map_data(self,verbose=0):
        ''' map the dat_file to memory '''
        if 'dat_file' in dir(self):
            tmp=np.memmap(self.dir+self.dat_file,mode='r',dtype=np.float32)
            if 'nvar' in dir(self):
                self.nsnap=np.size(tmp)/np.product(self.n)/self.nvar
                shape=tuple(self.n)+(self.nvar,self.nsnap,)
            else:
                shape=tuple(self.n)+(self.nvar,)
            self.dat=np.memmap(self.dir+self.dat_file,shape=shape,mode='r',
                               order='F',dtype=np.float32).byteswap().transpose(3,4,0,1,2)
            self.var={}
            self.var['d'] =self.dat[0]
            self.var['px']=self.dat[1]
            self.var['py']=self.dat[2]
            self.var['pz']=self.dat[3]
            self.var['e'] =self.dat[4]
            self.var['T'] =self.dat[5]
                                
    def image(self,ix=None,iy=None,iz=None,var=0,**kwargs):
        ''' image planes '''
        if type(var)==type(0):
            scr=self.scr[var]
        else:
            scr=self.scrvar[var]
        if type(ix)==type(0):
            im=scr[ix,:,:]
        elif type(iy)==type(0):
            im=scr[:,iy,:]
        elif type(iz)==type(0):
            im=scr[:,:,iz]
        pl.imshow(im.transpose(),origin='lower',**kwargs)

    def xdn(self,f):
        return so.xdn(f)
    def ydn(self,f):
        return so.ydn(f)
    def zdn(self,f):
        return so.zdn(f)
    def xup(self,f):
        return so.xup(f)
    def yup(self,f):
        return so.yup(f)
    def zup(self,f):
        return so.zup(f)
    def ddxdn(self,f):
        return so.ddxdn(f,self.ds[0])
    def ddydn(self,f):
        return so.ddydn(f,1.0)*self.mesh.dydn
    def ddzdn(self,f):
        return so.ddzdn(f,self.ds[2])
    def ddxup(self,f):
        return so.ddxup(f,self.ds[0])
    def ddyup(self,f):
        return so.ddyup(f,1.0)*self.mesh.dyup
    def ddzup(self,f):
        return so.ddzup(f,self.ds[2])
