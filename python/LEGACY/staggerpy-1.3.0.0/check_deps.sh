#!/bin/bash

NO_COLOR="\033[0m"
RED="\033[31;01m"
GREEN="\033[32;01m"
YELLOW="\033[33;01m"
BLUE="\033[34;01m"
MAGENTA="\033[35;01m"
CYAN="\033[36;01m"

function check_version ()
{
    reg_exp="[[:digit:]]\+\(\.[[:digit:]]\+\)\+"
    version=`$1 --version  | grep -o ${reg_exp}`
    if [ -n "${version}" ]; then
	echo $version
    else
	echo `$1 -V  | grep -o ${reg_exp}`
    fi
}

function check_program ()
{
    echo -n "Checking $1... "
    if type $1 &> /dev/null ; then
	echo -ne "${GREEN}yes${NO_COLOR}: "
	version=`check_version $1`
	echo "${version}"
	return 0
    else
	echo -e "${RED}no${NO_COLOR}"
	return 1
    fi
}

function compare_version ()
{
    if [ $1 \< $2 ]; then
	echo -e "${YELLOW}Warning${NO_COLOR}: Version found $1 < $2 (recommended)."
    fi
}

#function check_program {
#}

COMPILERS="
gfortran
ifort
lfc
f95
g95"

gversion="4.7"

#echo '--------------------------------------------------------------'
#echo "Checking FORTRAN compilers installed:"
#echo '--------------------------------------------------------------'
#
#for fc in ${COMPILERS}; do
#    check_program ${fc}
#    if [ "${fc}" == "gfortran" ]; then
#	version=`check_version ${fc}`
#	compare_version $version $gversion
#    fi
#done
#
#echo
#
#echo '--------------------------------------------------------------'


check_program python
if [ $? -eq 0 ] ; then
    check_program ipython
    echo '--------------------------------------------------------------'
    echo "Checking python modules:"
    
    python check_python_modules.py
else
    echo "You will not be able to use the python modules."
fi

echo


