# README #

This README details the necessary steps to get staggerpy up and running.

# Staggerpy

Staggerpy is a Python module for post-processing Stagger Code simulations.

###Getting the source

You need to have Mercurial installed in your computer. If you do, go to the directory where you wish to install staggerpy and type:

`hg clone https://username@bitbucket.org/lmanchon/staggerpy`

(replace *username* with your bitbucket username).

Or you can also download it from

https://bitbucket.org/lmanchon/staggerpy

or

https://github.com/narsonalin/Staggerpy

or

https://github.com/vsilvagui/staggerpy 

*For the moment, staggerpy is a work in progress*. 

###Prerequisites

To check if you have all the required software, type (in the staggerpy root directory)
`bash ./check_deps.sh`. 

###Installing 

Jump into staggerpy directory and run: python setup.py install

If you want to remove this module, run: python setup.py clean

### Who do I talk to 

If you hav any question, please mail louis.manchon@u-psud.fr.

* Repo owner or admin:

Victor Silva Aguirre (Stellar Astrophysic Center) for https://github.com/vsilvagui/staggerpy

Louis Manchon for https://bitbucket.org/lmanchon/staggerpy and https://github.com/narsonalin/Staggerpy

* Other community or team contact

Remo Collet is the developer os stagger code at Aarhus University (Stellar Astrophysic Center)