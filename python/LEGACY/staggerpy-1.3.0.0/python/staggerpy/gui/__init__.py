#!/anaconda/bin/python

from .CSnapshGUI import CSnapshGUI
from .CSimuGUI import CSimuGUI


__version__ = '1.3.0.0'

__requires__ = ['matplotlib', 'scipy', 'numpy', 'h5py', 'math', 'os', 'sys']

__all__ = ['CInterp', 'CSimu', 'const', 'CRead_hav', 'CSnapsh', 'CEOStab']