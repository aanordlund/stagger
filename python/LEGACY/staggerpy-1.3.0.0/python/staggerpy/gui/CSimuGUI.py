#!/anaconda/bin/python
# coding: utf-8

###################################################################
##    AUTHOR: Louis Manchon                                      ##
##    ORGN: Århus University, Stellar Astrophysic Center         ##
##    FILE: CSimuGUI.py                                          ##
##    DATE: June 2016                                            ##
###################################################################

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm
from matplotlib.colors import LogNorm
from matplotlib.legend_handler import HandlerLine2D
from scipy import interpolate
import math, h5py, os, sys

sys.path.append('/Users/Loulou/Desktop/Louis/Mag2/STAGE/staggerpy/python/staggerpy/')

from const import *
from CSimu import *


class CSimuGUI(CSimu):
	"""
	Class that post process the raw data of a snapshot. Data are stored in a Direct-Output Binary File *.dat.

	"""
	def __init__(self, path, sim, erase = False, v = '05', read = False):
		"""
		Inputs
		sim			string
					Name of the simulations: txxgyymzz.

		path		string
					Path to the directory in wich the snapshots are stored.

		v			int	
					number of the version
		"""
		self.path = path
		self.cwpath = os.getcwd()
		self.model = sim
		self.version = v

		os.chdir(self.path)

		self.sim = CSimu(self.path, self.model, erase = erase, v = v, read = read)

		self.open_all_h5()
		plt.rc('figure', figsize = [12,9])
		plt.rc('text', usetex=True)
		plt.rc('font', size= 20)
		plt.rc('savefig', dpi = 500)

	def open_all_h5(self, opt_mode = 'r'):
		self.sim.open_all_h5(opt_mode = opt_mode)
		self.hdf5_files = self.sim.hdf5_files

	def process_all(self):
		self.sim.process_all()

	def __getitem__(self, item):
		return self.sim[item]

	def __getitem__(self):
		return len(self.sim)

	def phavs(self, x, y, snaps = 'all', style = '', color = '', log = None, norm = 1.):
		"""
		This method plot horizontal average along an other variable.
		Input:
			x		string
					Name of the x variable. Must be stored in the hdf5 file associated.

			y		string
					Name of the y variable. Must be stored in the hdf5 file associated.

			snaps	list of int
					List of integers corresponding to the snapshots you want to plot.
					DEfault: 'all'.

			style	string
					Style of the curve. Default: ''.

			color	string
					Color of the curve. Default: ''.

			log		string
					Must designate the axis that will show logarithmic coordinates.
					Possible arguments: None, 'X', 'x', 'Y', 'y', 'XY' or 'xy'.
					Default: None.

			norm	float
					Normalization factor. Default: 1.
		"""
		X = self.sim[x]
		Y = self.sim[y]
		if snaps == 'all':
			l = range(len(X))

		if log == None:
			pass
		elif log == 'X' or log == 'x':
			plt.semilogx
		elif log == 'Y' or log == 'y':
			plt.semilogy
		elif log == 'XY' or log == 'xy':
			plt.loglog()
		else : 
			raise ValueError("log must be equal to None, 'X', 'x', 'Y', 'y', 'XY' or 'xy'.")
		if norm != 1.:
			for i in l:
				plt.plot(X[i], Y[i]/norm, style+color)
		else :
			for i in l:
				plt.plot(X[i], Y[i], style+color)
		try:
			xlabel = self.hdf5_files[0][x].attrs['legend']
			ylabel = self.hdf5_files[0][y].attrs['legend']
			plt.xlabel(xlabel, fontsize=20)
			plt.ylabel(ylabel, fontsize=20)
		except:
			pass















