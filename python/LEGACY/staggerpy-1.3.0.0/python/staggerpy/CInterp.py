#!/anaconda/bin/python
# coding: utf-8

###################################################################
##    AUTHOR: Louis Manchon                                      ##
##    ORGN: Århus University, Fysik og Astronomi                 ##
##    FILE: CInterp.py                                           ##
##    DATE: May 2016                                             ##
###################################################################

from const import *
import numpy as np
from CSimu import *
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import math, h5py, os

__metaclass__ = type

class CInterp(CSimu):
	"""
	Class that compute an interpolated model using the stagger grid.
	"""
	
	def __init__(self, teff_aim, g_aim, feh_aim):
		"""
		Inputs
		file            string
						name of the *.hdf5 file
		
		For more details about the variables, see var.pdf or take a look to the attribute 'legend'.
		"""

		if ((teff_aim < 4000) | (teff_aim > 7000)) | ((g_aim < 1.5) | (g_aim > 5.)) | ((feh_aim < -1.5) | (feh_aim > 5.)):
			raise ValueError("teff_aim must lie between 4000 and 7000 K, g_aim between 1.5 and 5. and feh_aim between 1.5 and 5.")
		else : 
			self.model = 't' + str(teff_aim) + 'g' + str(g_aim) + 'm' + str(feh_aim*10)
			self.path = os.getcwd()
			self.teff = teff_aim
			self.g = g_aim
			self.feh = feh_aim

			self.find_cube()

	def __getitem__(self, item):
		if len(self.hav_hdf5_files[self.model + '/' + item]) == 1:
			return self.hav_hdf5_files[self.model + '/' + item][0]
		else : 
			return self.hav_hdf5_files[self.model + '/' + item].value

	def trilin_interp(self, values):
		"""
		Compute a value of the model txxgyymzz by triliear interpolation.
		Inputs:
		values 			nd array
						values at the edge of the cube

		"""

		#values in an array of float
		if self.feh < -1 : 
			xd = (self.teff-self.teff0)/500; yd = 2*(self.g-self.logg0); zd = self.feh-self.feh0; 
			xxd = 1- xd; yyd = 1- yd; 
		else :
			xd = (self.teff-self.teff0)/500; yd = 2*(self.g-self.logg0); zd = 2*(self.feh-self.feh0);
			xxd = 1- xd; yyd = 1- yd;
		c00 = values[0]*xxd+values[1]*xd
		c01 = values[3]*xxd+values[4]*xd
		c10 = values[2]*xxd+values[6]*xd
		c11 = values[5]*xxd+values[7]*xd

		c0 = c00*yyd + c10*yd
		c1 = c01*yyd + c11*yd

		return c0*(1-zd)+c1*zd

	def find_cube(self):
		"""
			mdl[5]----------mdl[7]
		    /|			    /|
		   / |			   / |
		  /	 |			  /	 |
		 /	 |			 /	 |
		mdl[3]---------mdl[4]|
		|	mdl[2]------|---mdl[6]
		|	/			|	/
		|  /			|  /	
		| /				| /		
		|/				|/		
		mdl[0]---------mdl[1]
		"""
		t = int(self.teff/1000)*1000 + 500
		if t < self.teff:
			self.teff0 = t
			self.teff1 = t + 500
		else : 
			self.teff0 = t - 500
			self.teff1 = t

		lg = int(self.g) + 0.5
		if lg < self.g : 
			self.logg0 = lg
			self.logg1 = lg + 0.5
		else : 
			self.logg0 = lg - 0.5
			self.logg1 = lg 

		if self.feh < -1.0:
			f = int(self.feh)
			self.feh0 = f
			self.feh1 = f + 1.0
		else : 
			f = int(self.feh) + 0.5
			if f < self.feh:
				self.feh0 = f
				self.feh1 = f + 0.5
			else : 
				self.feh0 = f - 0.5
				self.feh1 = f

		names = ['t'+str(int(self.teff0/100))+'g'+str(int(self.logg0*10))+'m%02d' % (int(self.feh0*10)),
		't'+str(int(self.teff1/100))+'g'+str(int(self.logg0*10))+'m%02d' % (int(self.feh0*10)),
		't'+str(int(self.teff0/100))+'g'+str(int(self.logg1*10))+'m%02d' % (int(self.feh0*10)),
		't'+str(int(self.teff0/100))+'g'+str(int(self.logg0*10))+'m%02d' % (int(self.feh1*10)),
		't'+str(int(self.teff1/100))+'g'+str(int(self.logg0*10))+'m%02d' % (int(self.feh1*10)),
		't'+str(int(self.teff0/100))+'g'+str(int(self.logg1*10))+'m%02d' % (int(self.feh1*10)),
		't'+str(int(self.teff1/100))+'g'+str(int(self.logg1*10))+'m%02d' % (int(self.feh0*10)),
		't'+str(int(self.teff1/100))+'g'+str(int(self.logg1*10))+'m%02d' % (int(self.feh1*10))]
		
		print path_to_stagger_grid + '/' + names[0]

		self.mdl = [CSimu(path_to_stagger_grid + '/' + names[0], names[0], read = False), 
		CSimu(path_to_stagger_grid + '/' + names[1], names[1], read = False),
		CSimu(path_to_stagger_grid + '/' + names[2], names[2], read = False),
		CSimu(path_to_stagger_grid + '/' + names[3], names[3], read = False),
		CSimu(path_to_stagger_grid + '/' + names[4], names[4], read = False),
		CSimu(path_to_stagger_grid + '/' + names[5], names[5], read = False),
		CSimu(path_to_stagger_grid + '/' + names[6], names[6], read = False),
		CSimu(path_to_stagger_grid + '/' + names[7], names[7], read = False)]

	def wdset_arr_h5(self, name, array, group_name, ddtype = 'f4'):
		try : 
			self.hav_hdf5_file[group_name].create_dataset(name, data = array, dtype = ddtype)
			self.hav_hdf5_file[group_name+'/'+name].attrs['legend'] = var[name][0]
			self.hav_hdf5_file[group_name+'/'+name].attrs['factor'] = var[name][1]
			self.hav_hdf5_file[group_name+'/'+name].attrs['unit'] = var[name][2]
		except: 
			pass

	def trilin_interp_mmd(self, teff, logg, feh):
		"""
		Compute a value of the model txxgyymzz by triliear interpolation
		Inputs:
		teff               float

		logg               float

		feh 				float


		 	mdl[5]----------mdl[7]
		    /|			    /|
		   / |			   / |
		  /	 |			  /	 |
		 /	 |			 /	 |
		mdl[3]---------mdl[4]|
		|	mdl[2]------|---mdl[6]
		|	/			|	/
		|  /			|  /	
		| /				| /		
		|/				|/		
		mdl[0]---------mdl[1]
		"""

		os.chdir(self.path)
		self.hav_hdf5_file = h5py.File(self.file_hav_h5, 'w')

		try : 
			self.hav_hdf5_file.create_group(self.model)
		except :
			pass

		for i in self.mdl:
			i.open_all_h5(opt_mode = 'r')

		mdl_ref = self.mdl.hav_hdf5_files[0]
		self.list_var = mdl_ref.keys()

		for i in self.list_var:
			if str(mdl_ref[i].dtype)[:2] == '|S':
				pass
			else : 
				values = np.array([j[i] for j in self.mdl])
				self.wdset_arr_h5(i, self.trilin_interp_arr(values), self.model, ddtype = mdl_ref[i].dtype)


