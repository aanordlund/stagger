#!/anaconda/bin/python
# coding: utf-8

###################################################################
##    AUTHOR: Louis Manchon                                      ##
##    ORGN: Århus University, Stellar Astrophysic Center         ##
##    FILE: CEOStab.py                                           ##
##    DATE: July 2016                                            ##
###################################################################

from const import *
import numpy as np
from scipy import interpolate
from bisect import *
import math, h5py, os, glob


#At the end, -ln(uua) 

__metaclass__ = type

class CEOStab:
	"""
	Class that read, load and interpolate an EOS table.
	"""
	
	def __init__(self, tabfile = '', read = True, repair_deriv = True):
		
		# Attribute given when creating the class
		if tabfile:
			self.tab_file = tabfile
		else : 
			self.tab_file = sorted(glob.glob('*EOS*'))[0]
		if read:
			self.read_eos()
			self.dlnetb = (self.lnemax - self.lnemin)/(self.Netab - 1.)
			self.lnetb = np.arange(self.Netab) * self.dlnetb + self.lnemin
			self.dlnrtb = (self.lnrmax - self.lnrmin)/(self.Nrtab - 1.)
			self.lnrtb = np.arange(self.Nrtab) * self.dlnrtb + self.lnrmin
			if repair_deriv:
				self.repair_deriv()
			if (self.ul, self.ur, self.ut) != (1., 1., 1.):
				self.conversion()
			

	def read_eos(self, endian = '>'):

		print "Reading %s ..." % self.tab_file,
		f = open(self.tab_file,'rb') 

		u = np.fromfile(f, dtype = endian + 'u4', count = 1)
		self.Netab, self.Nrtab, self.Ma, self.Na, self.Ntbvar, self.Nelem = np.fromfile(f, dtype = endian + 'i4', count = 6) # sizes
		self.Nxopt = np.fromfile(f, dtype = endian + 'i4', count = 20)
		self.Nxrev = np.fromfile(f, dtype = endian + 'i4', count = 20)
		u = np.fromfile(f, dtype = endian + 'u4', count = 1)

		u = np.fromfile(f, dtype = endian + 'u4', count = 1)
		self.date, self.time = np.fromfile(f, dtype=endian + 'a8', count = 2)
		self.lnrmin, self.lnrmax, self.lnemin, self.lnemax, self.ul, self.ur, self.ut = np.fromfile(f, dtype = endian + 'f4', count = 7)
		self.iel = np.fromfile(f, dtype= endian + 'a4', count = self.Nelem)
		self.abund = np.fromfile(f, dtype= endian + 'f4', count = self.Nelem)
		self.arr = np.reshape(np.fromfile(f, dtype= endian + 'f4', count = self.Ma * self.Na), (self.Na, self.Na))
		u = np.fromfile(f, dtype = endian + 'u4', count = 1)

		u = np.fromfile(f, dtype = endian + 'u4', count = 1)
		count = self.Netab * self.Nrtab * 3 * self.Ntbvar
		self.tab = np.reshape(np.fromfile(f, dtype= endian + 'f4', count = count), (self.Ntbvar, 3, self.Nrtab, self.Netab))

		f.close()
		print "%s[Done]%s" % (GREEN, NO_COLOR)

	def conversion(self):
		uur = 1.e-7 # density
		uul = 1.e8 # distance
		uut = 1.e2 # time
		uuv = 1.e6 # velocity: uul/uut

		# Conversion factors for converting from table to simulation units.
		ulnr  = math.log(self.ur / uur); ulne = 2.*math.log(self.ul / self.ut / uuv)
		ulnP  = (ulnr + ulne); ulnabs = math.log(uul / self.ul)
		if abs(ulnr) > 1.e-3:
			self.lnrtb += ulnr
			self.lnrmin += ulnr
			self.lnrmax += ulnr
		if abs(ulne) > 1.e-3:
			self.lnetb += ulne
			self.lnemin += ulne
			self.lnemax += ulne
		if abs(ulnP) > 1.e-3:
			self.tab[0, 0, :, :] += ulnP
		if abs(ulnabs) > 1.e-3:
			self.tab[1, 0, :, :] += ulnabs
			self.tab[-2, 0, :, :] += ulnabs
			self.tab[-1, 0, :, :] += ulnabs
		# Reset the table unit conversion factor
		ur = uur; ul = uul; ut = uut

	def repair_deriv(self, endian = '>'):
		if self.Ntbvar > 5:
			ir = np.where((self.lnrtb >= -15.)  &  (self.lnrtb <= -10.))
			if np.abs(np.mean(self.tab[3, 2, ir, 0])-0.3) > 0.1:
				self.tab[:,:,2,3] = self.tab[3, 2, :, :] - self.dlnrtb
				with open(self.tab_file + '.bad-dlnNedlnr', 'wb') as f:
					np.array([184], dtype = endian + 'u4').tofile(f)
					np.append(np.append([self.Netab, self.Nrtab, self.Ma, self.Na, self.Ntbvar, self.Nelem], self.Nxopt), self.Nxrev).astype(endian + 'i4').tofile(f)
					np.array([184, 184], dtype = endian + 'u4').tofile(f)
					np.array([self.date, self.time]).astype(endian + 'a8').tofile(f)
					np.array([self.lnrmin, self.lnrmax, self.lnemin, self.lnemax, self.ul, self.ur, self.ut]).astype(endian + 'f4').tofile(f)
					self.iel.astype(endian + 'a4').tofile(f)
					self.abund.astype(endian + 'f4').tofile(f)
					self.arr.flatten().astype(endian + 'f4').tofile(f)
					np.array([184, 184], dtype = endian + 'u4').tofile(f)
					self.tab.flatten().astype(endian + 'f4').tofile(f)

	#def lookup_eos(self, ri, ei, j, i, linear = True):

	def lookup_eos(self, lnrho, lne, j, i, linear = False):
		"""
		ri = (lnrho - self.lnrmin) * (self.Nrtab - 1.) / (self.lnrmax - self.lnrmin)
		ei = (lne - self.lnemin) * (self.Netab - 1.) / (self.lnemax - self.lnemin)
		Program to perform linear interpolation in the EOSrhoe.tab-tables.
		The results are consistent with OS2code/lookup.f, lookup_eos.pro and
		math/bicubic.pro to within +/-6e-6 as tested on lnT of sune42.scr,
		on 19.05.2009.
		Arguments:
			lnrho:		ln(density) in table units (cf. ur in common ceostab).
			lne:		ln(internal energy per mass/[sim]).
			j:			index of variable: 0=lnPtot, 1=ln\kap_Ross, 2=lnT,
						3= ln ne, 4=ln\kap_Planck, 5=ln\kap_5000.
			i:			derivative index; 0=f, 1=df/dlne, 2=df/dlnrho
			status:		Is >0 when extrapolations were needed.
			LINEAR:		Set this keyword to perform linear interpolation even when i=0.
			QUIET:		Set this keyword to suppress warnings of extrapolations.
			STOP:		Set this keyword to stop before returning, to make all
						variables available to the user (for debugging).
			Returns:	Array of same format as lnrho and lne contaning the
						interpolated values of the specified table-entry.

		Use: 
		lnrho and lne should be in cgs units.
		When use from CSnapsh.py: 
		>>> lookup_eos(np.log(mdl.rho * mdl.uur), np.log(mdl.ee * mdl.uue), ...)
		Update history:
		---------------
		07.04.2008	Ported from lookup_rad/RT
		03.03.2009	Re-ordered terms and renamed a few variables, to be consistent
					with the function 'bicubic'/RT
		Known bugs: None
		"""
		if linear:
			ii = i  
		else:
			ii = 0
		tb = self.tab[j, ii, :, :]

		dlnrtb = (self.lnrmax-self.lnrmin)/(self.Nrtab-1.)
		dlnetb = (self.lnemax-self.lnemin)/(self.Netab-1.)

		# Density index
		ri = (lnrho - self.lnrmin) / dlnrtb
		ir = np.floor(np.array(ri))
		np.putmask(ir, ir < 0, 0)
		np.putmask(ir, ir > self.Nrtab - 2, self.Nrtab - 2)
		ir = ir.astype('i2')
		py = (ri - ir).astype('f4')
		qy = 1. - py

		imaxp = np.unravel_index(py.argmax(), py.shape) # index of the max item in py
		imaxq = np.unravel_index(qy.argmax(), qy.shape) # index of the max item in qy
		pymax = py[imaxp] # max item in py
		qymax = qy[imaxq] # max item in qy

		# Energy index
		ei = (lne - self.lnemin) / dlnetb
		ie = np.floor(np.array(ei))
		np.putmask(ie, ie < 0, 0)
		np.putmask(ie, ie > self.Netab - 2, self.Netab - 2)
		ie = ie.astype('i2')
		px = (ei - ie).astype('f4')
		qx = 1. - px

		imaxp = np.unravel_index(px.argmax(), px.shape)
		imaxq = np.unravel_index(qx.argmax(), qx.shape)
		pxmax = px[imaxp]
		qxmax = qx[imaxq]

		if not linear:
			tbx = self.tab[j, 1, :, :]; tby = self.tab[j, 2, :, :]
			# x-terms
			pxqx = px * qx
			qxqx = qx * qx
			pxpx = px * px
			dfx1 = tb[ir, ie + 1] - tb[ir, ie]
			dfx2 = tb[ir + 1, ie + 1] - tb[ir + 1, ie]
			# y-terms
			pyqy = py * qy
			qyqy = qy * qy
			pypy = py * py
			dfy1 = tb[ir + 1, ie] - tb[ir, ie]
			dfy2 = tb[ir + 1, ie + 1] - tb[ir, ie + 1]
		#  Perform the interpolation
		if not linear:
			if i == 0:
				a1 = tb[ir, ie] + pxqx * (tbx[ir, ie] - dfx1) + pyqy * (tby[ir, ie] - dfy1)
				a2 = tb[ir + 1, ie] + pxqx * (tbx[ir + 1, ie] - dfx2) - pyqy * (tby[ir + 1, ie] - dfy1)
				tabvar = qx * (qy * a1 + py * a2)
				b1 = (tb[ir + 1, ie + 1] - pxqx* (tbx[ir + 1, ie + 1] - dfx2) - pyqy * (tby[ir + 1, ie + 1] - dfy2))
				b2 = (tb[ir, ie + 1] - pxqx * (tbx[ir, ie + 1] - dfx1) + pyqy * (tby[ir, ie + 1] - dfy2))
				tabvar += px * (py * b1 + qy * b2)
			elif i == 1:
				#(dlnP/dlne)_rho	d/dx
				a1 = - tb[ir, ie] + (qxqx - 2. * pxqx) * (tbx[ir, ie] - dfx1) 
				a2 = - pyqy * (tby[ir, ie] - dfy1) + tb[ir, ie + 1]
				a3 = (pxpx - 2. * pxqx) * (tbx[ir, ie + 1] - dfx1) + pyqy * (tby[ir, ie + 1] - dfy2)
				tabvar = qy * (a1 + a2 + a3)
				b1 = tb[ir + 1, ie + 1] + (pxpx - 2. * pxqx) * (tbx[ir + 1, ie + 1] - dfx2) 
				b2 = - pyqy * (tby[ir + 1, ie + 1] - dfy2) - tb[ir + 1, ie]
				b3 = (qxqx - 2. * pxqx) * (tbx[ir + 1, ie] - dfx2) + pyqy * (tby[ir + 1, ie] - dfy1)
				tabvar += py * (b1 + b2 + b3)
			elif i == 2:
			#(dlnP/dlnr)_ee			d/dy
				a1 = - tb[ir, ie] - pxqx * (tbx[ir, ie] - dfx1)
				a2 = (qyqy - 2. * pyqy) * (tby[ir, ie] - dfy1) + tb[ir + 1, ie]
				a3 = pxqx * (tbx[ir + 1, ie] - dfx2) + (pypy - 2. * pyqy) * (tby[ir + 1, ie] - dfy1)
				tabvar = qx * (a1 + a2 + a3)
				b1 = - tb[ir, ie + 1] + pxqx * (tbx[ir, ie + 1] - dfx1)
				b2 = (qyqy - 2. * pyqy) * (tby[ir, ie + 1] - dfy2) + tb[ir + 1, ie + 1]
				b3 = - pxqx * (tbx[ir + 1, ie + 1] - dfx2) + (pypy - 2. * pyqy) * (tby[ir + 1, ie + 1] - dfy2) 
				tabvar += px * (b1 + b2 + b3)
			else:
				print "WARNING: Calling lookup_eos with i = ", i, "Doesn't make sense. imust be equal to 0, 1, or 2."
		else:
			tabvar = qy * (qx * tb[ir, ie] + px * tb[ir, ie + 1]) + py * (px * tb[ir + 1, ie + 1] + qx * tb[ir + 1, ie])

		#  The table contains derivatives multiplied by the grid-spacing, e.g.,
		#  (dlnP/dlne)*\Del(lne) - this is what's needed for the interpolations.
		#  Divide by \Del(lne) and \Del(lnrho), respectively, to get the proper
		#  thermodynamic derivatives.
		if i == 1:
			tabvar /= self.dlnetb
		if i == 2:
			tabvar /= self.dlnrtb
		return tabvar

	def depth_int(self, data, depth, minim = 0., vertical = 1, eps = 1.0e-14, spline = True, logvar = True):
		"""
		NAME:		DEPTH_INT
		SYNTAX:		result = depth_int(var, depth, minim=minim, vertical=vertical,
				dresult=dresult, spline=spline)
		PURPOSE: 	depth integrate data cube
		"""

		if eps == 0.:
			eps = 1.e-14
		ndim = data.ndim
		if ndim == 1:
			nsize = data.size
			var = data.flatten
			vertical = 2
		elif ndim == 3:
			if vertical == 0:
				var = np.transpose(data, (2, 1, 0))
			elif vertical == 1:
				var = np.transpose(data, (0, 2, 1))
			elif vertical == 2:
				var = data
			else :
				print 'Vertical must be equal to 0, 1, or 2.'
		else:
			print 'Unsupported ndim value: ndim =', ndim
	
		# dimensions of var array;
		# define arrays res and dres that will contain result and its finite increment
		dim = var.shape
		nz = dim[0]; ny = dim[1]; nx = dim[2]
		dresult = np.zeros((nz, ny, nx), dtype = 'f4')
		result = np.zeros((nz, ny, nx), dtype = 'f4')
		
		# spline integration ?
		if spline:
			dvarl = np.zeros((nz, ny, nx), dtype = 'f4')

		# logarithmic input data? <-------------------------------------------------------------------------------------------------------------
		if logvar: 
			varl = var
			var = np.exp(varl)
		else:
			varl = np.log(var)	

		# spline integration ?
		if spline:
			for n in xrange(1, nz - 2):
				dvarl[n, :, :] = varl[n + 1, :, :] - varl[n - 1, :, :]

			dvarl[0, :, :] = 2.0 * (varl[1, :, :] - varl[0, :, :])
			dvarl[nz - 1, :, :] = 2.0 * (varl[nz - 1, :, :] - varl[nz - 2, :, :])
			dresult[0, :, :] = var[0, :, :] * (depth[1] - depth[0])
			
			for n in xrange(1,  nz - 1):
				delta = depth[n] - depth[n - 1]
				a1 = var[n - 1, :, :] * 0.5 * (1.0 + 0.08333333 * dvarl[n - 1, :, :])
				np.putmask(a1, a1 < eps, eps)
				a2 = var[n, :, :] * 0.5 * (1.0 - 0.08333333 * dvarl[n, :, :])
				np.putmask(a2, a2 < eps, eps)
				dresult[n, :, :] = delta * (a1 + a2)
		else:
			dresult[0, :, :] = 0.5 * (depth[1] - depth[0]) * var[0, :, :]
			for n in xrange(1,  nz - 1):
				delta = depth[n] - depth[n - 1]
				dresult[n, :, :] = 0.5 * delta * (var[n - 1, :,  :] + var[n, :,  :])

		result[0, :, :] = minim + dresult[0, :, :]
		for n in xrange(1,  nz - 1):
			result[n, :, :] = result[n - 1, :, :] + dresult[n, :, :]

		# if one-dimensional, reshape result arrays
		if ndim == 1:
			result = np.reshape(result, (nz, ny, nx))
			dresult = np.reshape(dresult, (nz, ny, nx))

		# transpose back ?
		if vertical == 0:
			result = np.transpose(result, (2, 1, 0))
			dresult = np.transpose(dresult, (2, 1, 0))
		elif vertical == 1:
			result = np.transpose(result, (0, 2, 1))
			dresult = np.transpose(dresult, (0, 2, 1))
		else: 
			pass
		return result#, dresult

	def tau_calc(self, lnab, depth, taumin = 1.0e-10, vertical = 1, spline = True):
		"""
		NAME:		TAU_CALC
		SYNTAX:		tau = tau_calc( lnab, z, taumin=taumin, vertical=vertical, dtau=dtau, spline=spline)
		PURPOSE:	compute optical depth tau given ln extinction coefficient lnab and
				geometrical depth ym
		"""
		return self.depth_int(lnab, depth, minim = taumin, vertical = vertical, spline = spline, logvar = True) 

