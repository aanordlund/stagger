#!/anaconda/bin/python2.7
# coding: utf-8

#If used from ast*d.phys.au.dk mahcines:
#/usr/users/lmanchon/miniconda2/envs/py27/bin/python python

###################################################################
##    AUTHOR: Louis Manchon                                      ##
##    ORGN: Århus University, Fysik og Astronomi                 ##
##    FILE: run_parallel.py                                      ##
##    DATE: June 2016                                            ##
###################################################################

import numpy as np
import glob
import datetime
#import staggerpy
from const import *
#from staggerpy import *
from threading import Thread

__metaclass__ = type   # Make sure we get new style classes

path_to_stagger_grid = '/auto_mnt/SIMDATA/stagger/grid/'

def print_time(string):
    today = datetime.date.today()
    current_time = datetime.datetime.now().time() 
    current_time.isoformat()
    print "_______ " + str +": ", today, current_time, "_______"

class RunParallel(Thread):
    def __init__(self, sims):
        """
        sims is a list of the simulations you want to process.
        """
        self.sims = sims
        Thread.__init__(self)

    def run(self):
        for sim in self.sims:
            today = datetime.date.today()
            current_time = datetime.datetime.now().time() 
            current_time.isoformat()
            print_time('Next step')
            sim.process_all()
            sim.goback


# ----------------- fin de la classe ----------

print_time('Start')

N = 4 # Number of threads

list2 = list(list_sim)
list_mods = [] # each sub list of list_mods contains the simulations for each core.
for i in range(N):
    list_mods.append([])
b = True
while b :
    try:
        for i in range(N):
            list_mods[i].append(CSimu(path_to_stagger_grid+'/'+list2[0]+'/v05', list2[0], read = False))
            list2.remove(list2[0])
    except :
        b = False

# ------------------- run models --------------
procs = []
for sims in list_mods:
    procs.append(RunParallel(sims))
    procs[-1].start()

for i in procs:
    i.join()

    if not all(procs):
        print 'there was an error...'
        break

print_time('End')


