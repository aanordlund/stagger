#!/anaconda/bin/python
# coding: utf-8

###################################################################
##    AUTHOR: Louis Manchon                                      ##
##    ORGN: Århus University, Fysik og Astronomi                 ##
##    FILE: CSimu.py                                             ##
##    DATE: June 2016                                            ##
###################################################################

from const import *
from CSnapsh import *
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
from scipy import interpolate
import math, os, glob

__metaclass__ = type

class CSimu(CSnapsh):
	"""
	Class that post process the raw data of a snapshot. Data are stored in a Direct-Output Binary File *.dat.

	"""
	
	def __init__(self, path, sim, erase = False, v = '05', read = True):
		"""
		Inputs
		sim			string
					Name of the simulations: txxgyymzz.

		path		string
					Path to the directory in wich the snapshots are stored.

		v			int	
					number of the version
		"""
		self.path = path
		self.cwpath = os.getcwd()
		self.model = sim
		self.version = v

		os.chdir(self.path)
		self.list_snaps = sorted(glob.glob('*.dat'))
		if len(self.list_snaps) == 0:
			self.list_snaps = sorted(glob.glob('*.hav'))
		self.nsnap = len(self.list_snaps)
		self.read_snap = np.ones(self.nsnap)
		self.list_mdl = [CSnapsh(i[:-4], self.model, erase = erase, v = self.version, read = read) for i in self.list_snaps]
		self.len = len(self.list_snaps)
		self.list_mdl.reverse()
		os.chdir(self.cwpath)

	def __getitem__(self, item):
		if len(self.hav_hdf5_files[0][item]) == 1:
			a = np.array([], dtype = type(self.hav_hdf5_files[0][item].value))
			for i in self.hav_hdf5_files:
				a = np.append(a, i[item].value)
			return a.flatten()
		else : 
			a = np.array([self.hav_hdf5_files[0][item].value], dtype = type(self.hav_hdf5_files[0][item].value))
			for i in self.hav_hdf5_files[1:]:
				a = np.append(a, i[item].value)
			return np.reshape(a, (len(a)/230, 230))

	def __iter__(self):
		if self.len == 0:
			raise ValueError("The attribute self.list_mdl is empty.")
		return self.list_mdl.__iter__()

	def __len__(self):
		return self.len


	def process_all(self, kill = True):
		"""
		This method will process all the snapshots and writing the results in an hdf5 file.
		Inputs :
			store 	bool
					If True, store the CSnapsh class instances into a list self.list_mdl
					Default: False. 
		"""
		os.chdir(self.path)

		j = self.len - 1
		mdl = self.list_mdl.pop()
		mdl.read()
	
		mdl.write_dat_h5()
		self.read_snap[j] = 0
		nxm = mdl.nxm; nym = mdl.nym; nzm = mdl.nzm 
		dxm = mdl.dxm; dxmdn = mdl.dxmdn; xm = mdl.xm; xmdn = mdl.xmdn; dxidxup = mdl.dxidxup; dxidxdn = mdl.dxidxdn
		dym = mdl.dym; dymdn = mdl.dymdn; ym = mdl.ym; ymdn = mdl.ymdn; dyidyup = mdl.dyidyup; dyidydn = mdl.dyidydn
		dzm = mdl.dzm; dzmdn = mdl.dzmdn; zm = mdl.zm; zmdn = mdl.zmdn; dzidzup = mdl.dzidzup; dzidzdn = mdl.dzidzdn
		nsnap = mdl.nsnap

		del mdl

		j -= 1
		while self.list_mdl:
			if self.read_snap[j]:
				mdl = mdl = self.list_mdl.pop()
				mdl.nxm = nxm; mdl.nym = nym; mdl.nzm = nzm 
				mdl.dxm = dxm; mdl.dxmdn = dxmdn; mdl.xm = xm; mdl.xmdn = xmdn; mdl.dxidxup = dxidxup; mdl.dxidxdn = dxidxdn
				mdl.dym = dym; mdl.dymdn = dymdn; mdl.ym = ym; mdl.ymdn = ymdn; mdl.dyidyup = dyidyup; mdl.dyidydn = dyidydn
				mdl.dzm = dzm; mdl.dzmdn = dzmdn; mdl.zm = zm; mdl.zmdn = zmdn; mdl.dzidzup = dzidzup; mdl.dzidzdn = dzidzdn
				mdl.nsnap = nsnap

				mdl.write_hav_h5()
				mdl.read_dat()
				mdl.write_dat_h5()
				del mdl 
				j -= 1

		self.list_mdl = [CSnapsh(i[:-4], self.model, erase = False, read = False) for i in self.list_snaps]

		

	def process(self, snaps ,store = False):
		"""
		This method will process all the snapshots and writing the results in an hdf5 file.
		Inputs :
			snaps	int or string or list of int or list of string
					snapshots you want to plot 
			store 	bool
					If True, store the CSnapsh class instances into a list self.list_mdl
					Default: False. 
		"""
		os.chdir(self.path)

		if snaps == 'all':
			self.process_all()

		else : 
			for i in snaps:
				if type(i) == int:
					index = self.list_snaps.index(self.model + '_%.5d' % i)
					mdl = self.list_mdl[index]
					mdl.read()
					mdl.write_dat_h5()
					self.read_snap[index] = 0
				elif type(i) == str:
					try :
						index = self.list_snaps.index(self.model + '_%.5d' % i)
					except : 
						raise ValueError("Items from snaps should be int or string (name of *.dat file).")
					mdl = self.list_mdl[index]
					mdl.read()
					mdl.write_dat_h5()
					self.read_snap[index] = 0
				else : 
					raise ValueError("Items from snaps should be int or string (name of *.dat file).")

	def goback(self):
		os.chdir(self.cwpath)

	def open_all_h5(self, opt_mode = 'r'):
		os.chdir(self.path)
		self.hav_hdf5_files = []
		l = sorted(glob.glob('*.hav.hdf5'))
		for i in l:
			self.hav_hdf5_files.append(h5py.File(i, opt_mode)[self.model])
		


