#!/anaconda/bin/python
# coding: utf-8

###################################################################
##    AUTHOR: Louis Manchon                                      ##
##    ORGN: Århus University, Fysik og Astronomi                 ##
##    FILE: CSnapshGUI.py                                        ##
##    DATE: June 2016                                            ##
###################################################################


import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm
from matplotlib.colors import LogNorm
from matplotlib.legend_handler import HandlerLine2D
from scipy import interpolate
import math, h5py, os, sys

sys.path.append('/Users/Loulou/Desktop/Louis/Mag2/STAGE/staggerpy/python/staggerpy/')

from const import *
from CSnapsh import *


class CSnapshGUI(CSnapsh):
	"""
	Class that post process the raw data of a snapshot. Data are stored in a Direct-Output Binary File *.dat.

	"""
	
	def __init__(self, file, model, path = None, read = False, v = '05', op_mode = 'a'):
		"""
		Inputs
		file            string
						Name of the *.dat file.

		model 			string
						Name of the model: txxgyymzz.

		read 			bool
						If False, doesn't read the files.
						Default: True.

		v 				int
						Version 
						Default: 5

		op_mode 		string
						Opening mode of the hdf5 file.
						Default: 'a'

		norm			float
						Normalization factor. Default: 1.
		"""
		self.model = model
		self.file_dat = file #raw data (Direct-Output Binary File)
		self.version = v

		if path == None:
			self.path = os.getcwd()
		else:
			self.path = path

		self.mdl = CSnapsh(file, model, path = path, read = read, v = v, op_mode = op_mode)
		self.hav_hdf5_file = self.mdl.hav_hdf5_file
		self.read_dx()
		self.read_hav()
		self.nxm = self.mdl.nxm
		self.nym = self.mdl.nym
		self.nzm = self.mdl.nzm
		self.nghost = self.mdl.nghost

		plt.rc('figure', figsize = [12,9])
		plt.rc('text', usetex=True)
		plt.rc('font', size= 20)
		plt.rc('savefig', dpi = 500)

	def read(self, endian = '>'):
		"""
		Alias of CSnapsh.read()
		"""
		self.mdl.read(endian = endian)

	def read_msh(self, endian = '>'):
		"""
		Alias of CSnapsh.read_msh()
		"""
		self.mdl.read_msh(endian = endian)

	def read_dat(self, endian = '>'):
		"""
		Alias of CSnapsh.read_dat()
		"""
		self.mdl.read_dat(endian = endian)

	def read_dat_cube(self, var, endian = '>'):
		"""
		Alias of CSnapsh.read_dat_cube()
		"""
		self.mdl.read_dat_cube(var, endian = endian)

	def read_dx(self):
		"""
		Alias of CSnapsh.read_dx()
		"""
		self.mdl.read_dx()

	def read_chk(self):
		"""
		Alias of CSnapsh.read_chk()
		"""
		self.mdl.read_chk()

	def plt_slc(self, slc, vmin = None, vmax = None, cmap=cm.gist_heat, interpolation='nearest', save = False, save_name = 'figure1'):
		"""
		2D plot.
		Inputs:	
			slc				2d array
							Slice you want to plot
	
			vmin			float
							Minimal value displayed. All value < vmin will have the same color.
							Default: None

			vmax			float
							Maximal value displayed. All value > vmin will have the same color.
							Default: None
	
			cmap			matplotlib.colors.Colormap
							Color map. Default: cm.gist_heat.
							Recommended: cm.Greys, cm.gray, cm.gist_heat, cm.bwr, cm.seismic, cm.jet.
							More at http://matplotlib.org/examples/color/colormaps_reference.html
	
			interpolation 	string
							Default: 'nearest'

			save			bool
							If True, save the plot. Default: 'False'.

			save_name		string
							Name of the saved plot. Default: 'figure1'
		"""

		plt.imshow(slc, vmin = vmin, vmax = vmax, cmap = cmap, interpolation='nearest')
		plt.colorbar()
		plt.show()

		if save : 
			plt.savefig(save_name)

	def phavs(self, x, y, style = '', color = '', log = None, norm = 1.):
		"""
		This method plot horizontal average along an other variable.
		Input:
			x		string
					Name of the x variable. Must be stored in the hdf5 file associated.

			y		string
					Name of the y variable. Must be stored in the hdf5 file associated.

			style	string
					Style of the curve. Default: ''.

			color	string
					Color of the curve. Default: ''.

			log		string
					Must designate the axis that will show logarithmic coordinates.
					Possible arguments: None, 'X', 'x', 'Y', 'y', 'XY' or 'xy'.
					Default: None.

			norm	float
					Normalization factor. Default: 1.
		"""
		try:
			self.mdl.open_h5(self.hav_hdf5_file)
		except: 
			pass

		if (self.model+'/'+x in self.hav_hdf5_file) and (self.model+'/'+y in self.hav_hdf5_file):
			if log == None:
				pass
			elif log == 'X' or log == 'x':
				plt.semilogx
			elif log == 'Y' or log == 'y':
				plt.semilogy
			elif log == 'XY' or log == 'xy':
				plt.loglog()
			else : 
				raise ValueError("log must be equal to None, 'X', 'x', 'Y', 'y', 'XY' or 'xy'.")

			if norm != 1.:
				plt.plot(self.hav_hdf5_file[self.model+'/'+x], self.hav_hdf5_file[self.model+'/'+y]/norm, style+color)
			else :
				plt.plot(self.hav_hdf5_file[self.model+'/'+x], self.hav_hdf5_file[self.model+'/'+y], style+color)
			plt.xlabel(self.hav_hdf5_file[self.model+'/'+x].attrs['legend'], fontsize=20)
			plt.ylabel(self.hav_hdf5_file[self.model+'/'+y].attrs['legend'], fontsize=20)

		else : 
			raise ValueError("It seems that '%s' or '%s' is not stored in %s." % (x, y, self.mdl.file_hav_h5))


	def phavc(self, x, y, mdl2, style = '', color = '', log = None, rel = True, norm = 1.):
		"""
		This method plot horizontal average along an other variable.
		Input:
			x		string
					Name of the x variable. Must be stored in the hdf5 file associated.

			y		string
					Name of the y variable. Must be stored in the hdf5 file associated.

			mdl2	CSnapsh or CSnapshGUI instance
					mdl - mdl2.

			style	string
					Style of the curve. Default: ''.

			color	string
					Color of the curve. Default: ''.

			log		string
					Must designate the axis that will show logarithmic coordinates.
					Possible arguments: None, 'X', 'x', 'Y', 'y', 'XY' or 'xy'.
					Default: None.

			rel		bool
					If True, plot relative value, otherwise, absolute value. 
					Default: True.

			norm	float
					Normalization factor. Default: 1.
		"""
		try:
			self.mdl.open_h5(self.hav_hdf5_file)
		except: 
			pass

		if (self.model+'/'+x in self.hav_hdf5_file) and (self.model+'/'+y in self.hav_hdf5_file):
			if log == None:
				pass
			elif log == 'X' or log == 'x':
				plt.semilogx
			elif log == 'Y' or log == 'y':
				plt.semilogy
			elif log == 'XY' or log == 'xy':
				plt.loglog()
			else : 
				raise ValueError("log must be equal to None, 'X', 'x', 'Y', 'y', 'XY' or 'xy'.")

			x2 = mdl2.hav_hdf5_file[mdl2.model+'/'+x]
			y2 = mdl2.hav_hdf5_file[mdl2.model+'/'+y]

			if norm != 1.:
				y1 = self.hav_hdf5_file[self.model+'/'+y]/norm
				y2 = y2/norm
			else : 
				y1 = self.hav_hdf5_file[self.model+'/'+y]
				y2 = y2
			tck = interpolate.splrep(self.hav_hdf5_file[self.model+'/'+x], y1)
			tck2 = interpolate.splrep(x2, y2)

			xmin = max(min(self.hav_hdf5_file[self.model+'/'+x]), min(x2)) ; xmax = min(max(self.hav_hdf5_file[self.model+'/'+x]), max(x2))
			xx = np.linspace(xmin, xmax, int((xmax-xmin)*1000))
			yy = interpolate.splev(xx, tck) - interpolate.splev(xx, tck2)

			if rel: 
				plt.plot(xx, yy, style+color)
			else : 
				plt.plot(xx, np.abs(yy), style+color)
			plt.xlabel(self.hav_hdf5_file[self.model + '/' + x].attrs['legend'], fontsize=20)
			plt.ylabel(self.hav_hdf5_file[self.model + '/' + y].attrs['legend'], fontsize=20)
		else : 
			raise ValueError("It seems that '%s' or '%s' is not stored in %s." % (x, y, self.file_hav_h5))


	def phstat(self, x, y, log = None, norm = 1.):
		"""
		This method plot horizontal average along an other variable.
		Input:
			x		string
					Name of the x variable. Must be stored in the hdf5 file associated.

			y		string
					Name of the y variable. Must be stored in the hdf5 file associated.

			log		string
					Must designate the axis that will show logarithmic coordinates.
					Possible arguments: None, 'X', 'x', 'Y', 'y', 'XY' or 'xy'.
					Default: None.

			norm	float
					Normalization factor. Default: 1.
		"""

		if (self.model+'/'+x in self.hav_hdf5_file) and (self.model+'/'+y in self.hav_hdf5_file) and (self.model+'/'+y+'u' in self.hav_hdf5_file):
			if log == None:
				pass
			elif log == 'X' or log == 'x':
				plt.semilogx
			elif log == 'Y' or log == 'y':
				plt.semilogy
			elif log == 'XY' or log == 'xy':
				plt.loglog()
			else : 
				raise ValueError("log must be equal to None, 'X', 'x', 'Y', 'y', 'XY' or 'xy'.")

			s = self.model + '/'

			line1, = plt.plot(self.hav_hdf5_file[s + x], self.hav_hdf5_file[s + y][:]/norm, 'k', label = 'Mean')
			line2, = plt.plot(self.hav_hdf5_file[s + x], self.hav_hdf5_file[s + y + 'u'][:]/norm, '--b', label = 'Upflow')
			line3, = plt.plot(self.hav_hdf5_file[s + x], self.hav_hdf5_file[s + y + 'd'][:]/norm, '--r', label = 'Downflow')
			line4, = plt.plot(self.hav_hdf5_file[s + x], self.hav_hdf5_file[s + y + 's'][:]/norm, '--k', label = 'Sigma')
			line5, = plt.plot(self.hav_hdf5_file[s + x], self.hav_hdf5_file[s + y + 'rms'][:]/norm, 'b', label = 'RMS')
			line6, = plt.plot(self.hav_hdf5_file[s + x], self.hav_hdf5_file[s + y + 'min'][:]/norm, 'k:', label = 'Extrema')
			line7, = plt.plot(self.hav_hdf5_file[s + x], self.hav_hdf5_file[s + y + 'max'][:]/norm, 'k:')
			line5.set_dashes([8, 4, 2, 4, 2, 4])

			plt.legend(handler_map={line1: HandlerLine2D(numpoints=4)})
			plt.xlabel(self.hav_hdf5_file[s + x].attrs['legend'], fontsize=20)
			plt.ylabel(self.hav_hdf5_file[s + y].attrs['legend'], fontsize=20)


	def plot_hist2d(self, x, y, lim1=None, nbin1=256, lim2=None, nbin2=256,
			axis=None, bins=None, ctable='Normalize', beta=1., reverse=False):
	
			"""
			Plot the 2D histogram of two array.
			Inputs: 
				x				3D array
								Name of the data cube. 
	
				y				3D array
								Name of the data cube.  
	
				lim1			tuple (pair) of float
								Min and max values of x you want to plot.
								Default: lim1 = None (<=> all).
	
				nbin1 			int
								Number of bins.
								Default: nbin1 = 256.
	
				lim2 			tuple (pair) of float
								Min and max values of y you want to plot.
								Default: lim2 = None (<=> all).
	
				nbin2  			int
								Number of bins.
	
				axis			tuple of float (4 elements)
								If want not to display the axis, set axis = 'off'.
								Default: axis = None. 
	
				bins			tuple (pair) of int
								Default: bins = None.
	
				ctable			string
								Name of the color table. Default: ctable = 'Normalize'.
	
				beta			float
								Enhanced the contrast. Default: beta = 1. 
	
				reverse			bool
								Reverse the values of y. Deflaut: reverse = False.
	
			"""
			if lim1 != None:
				ax_min = min(np.amin(x), lim1[0]) ; ax_max = max(np.amax(x), lim1[1])
			else:
				ax_min = np.amin(x) ; ax_max = np.amax(x)
			if lim2 != None:
				ay_min = min(np.amin(y), lim2[0]) ; ay_max = max(np.amax(y), lim2[1])
			else:
				ay_min = np.amin(y) ; ay_max = np.amax(y)
			if bins == None:
				b = max(math.ceil((ax_max - ax_min) / (nbin1 - 1)), math.ceil((ay_max - ay_min) / (nbin2 - 1)))
				bins = [b, b]
			
			for i in range(self.nghost, self.nym - self.nghost):
				if lim1 == None and lim2 == None:
					h1 = x[:, i, :] # x value
					h2 = y[:, i, :] # y value
				elif lim1 != None and lim2 != None:
					h1 = (x[np.where((x[:, i, :] > lim1[0]) & (x[:, i, :] < lim1[1]))] - lim1[0])/(lim1[1] - lim1[0])*nbin1
					h2 = (y[np.where((y[:, i, :] > lim2[0]) & (y[:, i, :] < lim2[1]))] - lim2[0])/(lim2[1] - lim2[0])*nbin2
				elif lim1 == None : 
					h1 = x[:, i, :]
					h2 = (y[np.where((y[:, i, :] > lim2[0]) & (y[:, i, :] < lim2[1]))] - lim2[0])/(lim2[1] - lim2[0])*nbin2
				elif lim2 == None : 
					h2 = y[:, i, :]
					h1 = (x[np.where((x[:, i, :] > lim1[0]) & (x[:, i, :] < lim1[1]))] - lim1[0])/(lim1[1] - lim1[0])*nbin1	
				else : 
					raise ValueError("The arguments 'lim1' and 'lim2' must be None or a list of 2 floats.")
				if beta != 1.:
					h2 = [i**beta for i in h2]
				if reverse :
					h2 = [max(h2) - i for i in h2]
				plt.hist2d(h1.flatten(), h2.flatten(), bins = bins, norm = LogNorm())

			if axis == None : 
				plt.axis([ax_min*1.01, ax_max*1.01, ay_min*1.01, ay_max*1.01])
			else : 
				plt.axis(axis)

			plt.show()
	







