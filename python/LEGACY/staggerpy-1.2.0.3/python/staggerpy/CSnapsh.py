#!/anaconda/bin/python
# coding: utf-8

###################################################################
##    AUTHOR: Louis Manchon                                      ##
##    ORGN: Århus University, Fysik og Astronomi                 ##
##    FILE: CSnapsh.py                                           ##
##    DATE: June 2016                                            ##
###################################################################

from const import *
import numpy as np
from bisect import *
import math, h5py, os, glob

__metaclass__ = type

class CSnapsh:
	"""
	Class that post process the raw data of a snapshot. Data are stored in a Direct-Output Binary File *.dat.

	"""
	
	def __init__(self, file, model, path = None, read = True, erase = False, v = '05', dat = 'dat'):
		"""
		Inputs
		file            string
						Name of the snapshot(Without the .dat extension).

		model 			string
						Name of the model: txxgyymzz.

		read 			bool
						If False, doesn't read the files.
						Default: True.

		erase			bool
						If True, erase the previously existing *hdf5 file
						Default: True.

		v 				int
						Version 
						Default: 5
		"""
		# Attributes given when creating the class
		self.model = model
		self.snapshot = file
		if path == None:
			self.path = os.getcwd()
		else:
			self.path = path
		self.version = v

		# Create some file names. We will test their presence after
		self.dat_file = file + '.' + dat # raw data (Direct-Output Binary File)
		self.hav_file = file + ".hav" # ASCII file
		self.msh_file = self.model + self.version + '.msh' # mesh data (Unformatded 'F77' Binary File)
		self.h5_file = self.snapshot+'.hdf5' #HDF5 file


		# Test if the dx file is here
		# this list contains the name of all the dx files contained in the directory, sorted in alphabetical order
		l = sorted(glob.glob(self.model + '*.dx'))
		self.dx_file = self.model + self.version + '.dx'# ASCII file
		if self.dx_file in l : # if the dx file is indeed in the list
			self.dx_exists = True
		elif l: # if the dx file is not here but some dx files yes, 
			#we re taking the one just before the index where the good 
			#version on the dx file should be in the sorted list.
			self.dx_file = l[bisect_right(l, self.dx_exists)-1]
			self.dx_exists = True #in fact, it's an earlier version
		else: #if no dx files at all, print just a Warning. Certain values may be set by default
			print "IOWraning: %s does not exist in %s." % (self.dx_file, self.path)
			self.dx_exists = False


		# Test if the chk file is here
		# this list contains the name of all the chk files contained in the directory, sorted in alphabetical order
		l = sorted(glob.glob(self.model + '*.chk'))
		self.chk_file = self.model + self.version + '.chk'# ASCII file
		if self.chk_file in l : # if the chk file is indeed in the list
			self.chk_exists = True
		elif l:# if the chk file is not here but some chk files yes, 
			#we re taking the one just before the index where the good 
			#version on the chk file should be in the sorted list.
			self.chk_file = l[bisect_right(l, self.chk_exists)-1]
			self.chk_exists = True #in fact, it's an earlier version
		else: #if no chk files at all, print just a Warning. Certain values may be set by default
			print "IOWraning: %s does not exist in %s." % (self.chk_file, self.path)
			self.chk_exists = False

		if read :
			if not os.path.exists(self.dat_file): # If dat file is not present
				raise IOError("%s does not exist in %s." % (self.dat_file, self.path))

			if not os.path.exists(self.msh_file):  # If msh file is not present, 
				#we try to find a msh file without the version extension
				self.msh_file = self.model + '.msh'
				if not os.path.exists(self.msh_file):
					raise IOError("%s does not exist in %s." % (self.msh_file, self.path))
		
		#By default, we set nsnap equal -42, It could be changed after for a real value
		self.nsnap = -42
		
		#CSnapsh can work on a dat file or a scratch file
		if dat == 'dat':
			self.isnap = int(self.dat_file[-9:-4])
		elif dat == 'src':
			self.isnap = -1
		else :
			raise ValueError("dat should take the value 'dat' or 'src'.")

		#for the moment, dx, chk, msh, hav files hve not been read.
		self.rd_dx = True	#We need to read dx
		self.rd_chk = True	#We need to read chk
		self.rd_msh = True	#We need to read msh
		self.rd_hav = True	#We need to read msh

		#This two lists will contains the indices of the cells that contains a upflow and those that contains a downflow
		self.upfl = []
		self.dwfl = []

		if read :
			if erase:
				#opening of the hdf5 file and deletion of the previous version if it exists
				self.hdf5_file = h5py.File(self.h5_file, 'w')
				#creation of 4 data group
				self.wgrp_h5('hav')
				self.wgrp_h5('thavn')
				self.wgrp_h5('rhavn')
				self.wgrp_h5('t5havn')
			else: 
				#opening of the hdf5 file
				self.hdf5_file = h5py.File(self.h5_file, 'a')

			try:
				#Try to read hav file and by the way test if it is present
				self.write_hav_h5()
			except:
				print "No %s file existing. self.nghost has been set to 5." % self.hav_file
				self.nghost = 5

			#Then, read all the other files
			#if dx or chk are not present, this step is skiped
			self.read_dx()
			self.read_chk()
			self.read_msh()
			self.read_dat()
		else : 
			#Just opening operation but no reading.
			if erase:
				self.hdf5_file = h5py.File(self.h5_file, 'w')
				self.wgrp_h5('hav')
				self.wgrp_h5('thavn')
				self.wgrp_h5('rhavn')
				self.wgrp_h5('t5havn')
			else: 
				self.hdf5_file = h5py.File(self.h5_file, 'a')

	def __len__(self):
		"""
		If the file is a all* file, return the nuber of snapshots stored in the files plus mean*, tmin and tmax.
		If it is a single snapshot, return the number of variables.
		"""
		if self.dx_exists:
			if self.rd_dx:
				self.read_dx()
		else : 
			if self.rd_msh:
				self.read_msh()
		return (self.nxm, self.nym, self.nzm)

	def __getitem__(self, item):
		if len(self.hdf5_files[0][item]) == 1:
			return np.array([], dtype = type(self.hdf5_files[0][item].value[0]))
		else : 
			return np.array([self.hdf5_files[0][item].value], dtype = type(self.hdf5_files[0][item].value))

	def open_h5(self, op_mode = 'a'):
		self.hdf5_file = h5py.File(self.h5_file, op_mode)

	def close_h5(self):
		self.hdf5_file.close()


	def read(self, endian = '>'):
		self.write_hav_h5()
		self.read_dx()
		self.read_chk()
		self.read_msh(endian = endian)
		self.read_dat(endian = endian)

	def read_dx(self):
		"""
		Read .dx file.
		"""

		if self.dx_exists:
			print "Reading %s ..." % self.dx_file,
			self.data_dx=[]
			data=open(self.dx_file, 'r')
			line=data.readline()
			while line :
				self.data_dx.append(line.split())
				line=data.readline()
			data.close()
			self.rd_dx = False #No need to read dx again
			self.nxm, self.nym, self.nzm = map(int, self.data_dx[0])
			print "%s[Done]%s" % (GREEN, NO_COLOR)


	def read_chk(self):
		"""
		Read .chk file.
		"""
		if self.chk_exists:
			print "Reading %s ..." % self.chk_file,
			self.data_chk=[]
			data=open(self.chk_file, 'r')
			line=data.readline()
			while line :
				self.data_chk.append(line.split())
				line=data.readline()
			data.close()
			self.rd_chk = False #No need to read chk again
			self.wdset_all_h5('nsnap', self.nsnap, ddtype = 'i2', dset = 'val')
		print "%s[Done]%s" % (GREEN, NO_COLOR)


	def read_msh(self, endian = '>'):
		"""
		Read mesh file

		Error : 

		Raise IOError if value from .dx differ from those of .msh.
		"""

		if self.rd_dx:
			self.read_dx()

		print "Reading %s ..." % self.msh_file,
		f = open(self.msh_file,'rb') 

		u = np.fromfile(f, dtype = endian + 'u4', count = 1)
		if self.dx_exists:
			np.fromfile(f, dtype = endian + 'i4', count = 3)
		else:
			self.nxm, self.nym, self.nzm = np.fromfile(f, dtype = endian + 'i4', count = 3) # sizes
		u = np.fromfile(f, dtype = endian + 'u4', count = 1)

		u = np.fromfile(f, dtype = endian + 'u4', count = 1)
		self.dxm = np.fromfile(f, dtype = endian + 'f4', count = self.nxm)
		self.dxmdn = np.fromfile(f, dtype = endian + 'f4', count = self.nxm)
		self.xm = np.fromfile(f, dtype = endian + 'f4', count = self.nxm)
		self.xmdn = np.fromfile(f, dtype = endian + 'f4', count = self.nxm)
		self.dxidxup = np.fromfile(f, dtype = endian + 'f4', count = self.nxm) #dx/di half a cell down
		self.dxidxdn = np.fromfile(f, dtype = endian + 'f4', count = self.nxm) #dx/di half a cell up
		u = np.fromfile(f, dtype = endian + 'u4', count = 1)

		u = np.fromfile(f, dtype = endian + 'u4', count = 1)
		self.dym = np.fromfile(f, dtype = endian + 'f4', count = self.nym)
		self.dymdn = np.fromfile(f, dtype = endian + 'f4', count = self.nym)
		self.ym = np.fromfile(f, dtype = endian + 'f4', count = self.nym)
		self.ymdn = np.fromfile(f, dtype = endian + 'f4', count = self.nym)
		self.dyidyup = np.fromfile(f, dtype = endian + 'f4', count = self.nym)
		self.dyidydn = np.fromfile(f, dtype = endian + 'f4', count = self.nym)
		u = np.fromfile(f, dtype = endian + 'u4', count = 1)

		u = np.fromfile(f, dtype = endian + 'u4', count = 1)
		self.dzm = np.fromfile(f, dtype = endian + 'f4', count = self.nzm)
		self.dzmdn = np.fromfile(f, dtype = endian + 'f4', count = self.nzm)
		self.zm = np.fromfile(f, dtype = endian + 'f4', count = self.nzm)
		self.zmdn = np.fromfile(f, dtype = endian + 'f4', count = self.nzm)
		self.dzidzup = np.fromfile(f, dtype = endian + 'f4', count = self.nzm)
		self.dzidzdn = np.fromfile(f, dtype = endian + 'f4', count = self.nzm)
		u = np.fromfile(f, dtype = endian + 'u4', count = 1)

		f.close()
		self.rd_msh = False #No need to read msh again
		self.wdset_arr_h5('depth', self.ym[self.nghost:self.nym-self.nghost], 'hav', ddtype = 'f4')
		print "%s[Done]%s" % (GREEN, NO_COLOR)

	def read_dat(self, endian = '>'):
		"""
		Read .dat file.

		Create attributes self.rho, self.px, self.py, self.pz, self.e, self.tt.

					 px
		X 	•	 ------x------
			|	|			  |
			|	|  			  |
			|	|	   •······|·····> rho, e, tt, scalars
			|	|			  |		  cell-centered
			|	|			  |
			|	 -------------
			|
			V

		"""
		####os.chdir(self.path)

		print "Reading %s ..." % self.dat_file,

		k = self.nxm*self.nym*self.nzm
		f = open(self.dat_file, 'rb')
		self.rho = np.reshape(np.fromfile(f, dtype = endian + 'f', count = k), (self.nxm, self.nym, self.nzm))
		self.px = np.reshape(np.fromfile(f, dtype = endian + 'f', count = k), (self.nxm, self.nym, self.nzm))
		self.py = np.reshape(np.fromfile(f, dtype = endian + 'f', count = k), (self.nxm, self.nym, self.nzm))
		self.pz = np.reshape(np.fromfile(f, dtype = endian + 'f', count = k), (self.nxm, self.nym, self.nzm))
		self.e = np.reshape(np.fromfile(f, dtype = endian + 'f', count = k), (self.nxm, self.nym, self.nzm))
		self.tt = np.reshape(np.fromfile(f, dtype = endian + 'f', count = k), (self.nxm, self.nym, self.nzm))

		f.close()
		print "%s[Done]%s" % (GREEN, NO_COLOR)


	def read_dat_cube(self, var, endian = '>'):
		"""
		Read only one data cube from .dat file.
		Input : 
		var 		string
					name of the data you want

		Error :

		Raise KeyError if var does not match any of the key of raw_var (stored in const.py).
		"""
		if self.rd_dx:
			self.read_dx()
		if self.rd_chk:
			self.read_chk()
		if self.rd_msh:
			self.read_msh()

		print "Reading %s from %s ..." % (var, self.dat_file),
		try : 
			index = raw_var[var]
		except KeyError: 
			raise KeyError("the argument 'var' of read_dat_cube(var) must take the value: 'rho', 'px, 'py', 'pz', 'ee' or 'tt'.")
		k = self.nxm*self.nym*self.nzm
		f = open(self.dat_file, 'rb')
		f.seek(4*k*index)
		dat = np.reshape(np.fromfile(f, dtype = endian + 'f', count = k), (self.nxm, self.nym, self.nzm))

		if var == 'rho':
			self.rho = dat
		elif var == 'px':
			self.px = dat
		elif var == 'py':
			self.py = dat
		elif var == 'pz':
			self.pz = dat
		elif var == 'e':
			self.e = dat
		elif var == 'tt':
			self.tt = dat
		f.close()
		print "%s[Done]%s" % (GREEN, NO_COLOR)

	def sl_stat(self, slc, ind, lgm = True, ud_rms = False, store_indices = False):
		"""
		Compute some statistical quantities such as mean value, rms, standard deviation, 
		min, max, and make the difference between up and down flow
		Input:
			slc				2d np.array
							Horizontal slice at constant y (cube[:, i, :]).

			ind 			integer
							Index of the horizontal slice.

			lgm				bool
							If True, compute logarithmic mean, defined as follow:
							exp(<log(array)>).
							Default: True.

			ud_rms			bool
							If True, compute the rms of the upflow and of the downflow.
							Default: False.

			store_indices	bool
							If True, store the indices of the upflow and the downflow.
							Should be set to True only if the cube is the vertical velocity (or if you have a 
							good reason to so).
							Default: False.
		"""
		res = []
		up = []
		dw = []
		if len(self.upfl) > self.nym:
			self.upfl = []
			self.dwfl = []
		if store_indices:
			d = np.where(slc > 0.)
			u = np.where(slc < 0.)
			self.dwfl.append((d[0].astype(np.uint16), d[1].astype(np.uint16)))
			self.upfl.append((u[0].astype(np.uint16), u[1].astype(np.uint16)))
		try :
			dw = slc[self.dwfl[ind]]
			up = slc[self.upfl[ind]]
		except :
			raise AttributeError("self.dwfl and self.upfl may not exist. \n Please store them in memory by reading the vertical velocity data cube.")

		res.append(np.mean(slc))
		res.append(np.mean(up))
		res.append(np.mean(dw))
		res.append(np.std(slc, ddof = 1))
		res.append(np.sqrt(np.mean(np.square(slc))))
		res.append(np.amin(slc))
		res.append(np.amax(slc))
		if lgm :
			res.append(np.exp(np.mean(np.log(slc))))
		if ud_rms :
			res.append(np.sqrt(np.mean(np.square(up))))
			res.append(np.sqrt(np.mean(np.square(dw))))
		return res


	def xdn(self, cube):
		"""
		return cube(i-0.5, j, k)
		"""
		c = 3./256.
		b = -25./256.
		a = 75./128.
		return a*(cube+np.roll(cube, 1, axis = 0))+ \
		b*(np.roll(cube, -1, axis = 0)+np.roll(cube, 2, axis = 0))+ \
		c*(np.roll(cube, -2, axis = 0)+np.roll(cube, 3, axis = 0))

	def ydn(self, cube):
		"""
		return cube(i, j-0.5, k)
		"""
		c = 3./256.
		b = -25./256.
		a = 75./128.
		return a*(cube+np.roll(cube, 1, axis = 1))+ \
		b*(np.roll(cube, -1, axis = 1)+np.roll(cube, 2, axis = 1))+ \
		c*(np.roll(cube, -2, axis = 1)+np.roll(cube, 3, axis = 1))

	def zdn(self, cube):
		"""
		return cube(i, j, k-0.5)
		"""
		c = 3./256.
		b = -25./256.
		a = 75./128.
		return a*(cube+np.roll(cube, 1, axis = 2))+ \
		b*(np.roll(cube, -1, axis = 2)+np.roll(cube, 2, axis = 2))+ \
		c*(np.roll(cube, -2, axis = 2)+np.roll(cube, 3, axis = 2))

	def xup(self, cube):
		"""
		return cube(i+0.5, j, k)
		"""
		c = 3./256.
		b = -25./256.
		a = 75./128.
		return a*(cube+np.roll(cube, -1, axis = 0))+ \
		b*(np.roll(cube, 1, axis = 0)+np.roll(cube, -2, axis = 0))+ \
		c*(np.roll(cube, 2, axis = 0)+np.roll(cube, -3, axis = 0))

	def yup(self, cube):
		"""
		return cube(i, j+0.5, k)
		"""
		c = 3./256.
		b = -25./256.
		a = 75./128.
		return a*(cube+np.roll(cube, -1, axis = 1))+ \
		b*(np.roll(cube, 1, axis = 1)+np.roll(cube, -2, axis = 1))+ \
		c*(np.roll(cube, 2, axis = 1)+np.roll(cube, -3, axis = 1))

	def zup(self, cube):
		"""
		return cube(i, j, k+0.5)
		"""
		c = 3./256.
		b = -25./256.
		a = 75./128.
		return a*(cube+np.roll(cube, -1, axis = 2))+ \
		b*(np.roll(cube, 1, axis = 2)+np.roll(cube, -2, axis = 2))+ \
		c*(np.roll(cube, 2, axis = 2)+np.roll(cube, -3, axis = 2))



	def get_vel_cube(self, dir):
		"""
		Compute the velocity cube. 
		Input:
			dir		integer
					0 for Ux, 1 for Uy, 2 for Uz.
		"""
		if dir == 0:
			return np.divide(self.px, self.xdn(self.rho))
		elif dir == 1:
			return np.divide(self.py, self.ydn(self.rho))
		elif dir == 2:
			return np.divide(self.pz, self.zdn(self.rho))
		else : 
			raise ValueError("dir must be equal to 0, 1 or 2")

	def wgrp_h5(self, group_name):
		try : 
			self.hdf5_file.create_group(group_name)
		except :
			pass

	def wdset_val_h5(self, name, value, group_name, ddtype = 'f4'):
		try : 
			self.hdf5_file[group_name].create_dataset(name, data = [value], dtype = ddtype)
			self.hdf5_file[group_name+'/'+name].attrs['legend'] = var[name][0]
			self.hdf5_file[group_name+'/'+name].attrs['factor'] = var[name][1]
			self.hdf5_file[group_name+'/'+name].attrs['unit'] = var[name][2]
		except:
			print name, group_name
			#pass 

	def wdset_arr_h5(self, name, array, group_name, ddtype = 'f4'):
		try : 
			self.hdf5_file[group_name].create_dataset(name, data = array, dtype = ddtype)
			self.hdf5_file[group_name+'/'+name].attrs['legend'] = var[name][0]
			self.hdf5_file[group_name+'/'+name].attrs['factor'] = var[name][1]
			self.hdf5_file[group_name+'/'+name].attrs['unit'] = var[name][2]
		except: 
			print name, group_name
			#pass

	def wdset_all_h5(self, name, data, ddtype = 'f4', dset = 'arr'):
		if dset == 'arr':
			self.wdset_arr_h5(name, data, 'hav', ddtype = ddtype)
			self.wdset_arr_h5(name, data, 'thavn', ddtype = ddtype)
			self.wdset_arr_h5(name, data, 'rhavn', ddtype = ddtype)
			self.wdset_arr_h5(name, data, 't5havn', ddtype = ddtype)
		else :
			self.wdset_val_h5(name, data, 'hav', ddtype = ddtype)
			self.wdset_val_h5(name, data, 'thavn', ddtype = ddtype)
			self.wdset_val_h5(name, data, 'rhavn', ddtype = ddtype)
			self.wdset_val_h5(name, data, 't5havn', ddtype = ddtype)


	def write_h5(self):
		self.write_hav_h5()
		self.write_dat_h5()

	def test_int(self, string):
		try : 
			return len(str(int(string))) == len(string)
		except:
			return False

	def write_hav_h5(self):

		print "Reading %s ..." % self.hav_file,
		f = open(self.hav_file,'r') 
		line = f.readline()
		line = f.readline()
		line = f.readline()
		while line != '/\n':
			words = line.split()
			if self.test_int(words[2][:-1]): #then it's an int
				self.wdset_all_h5(words[0], int(words[2][:-1]), ddtype = 'i4', dset = 'val')
			else:
				self.wdset_all_h5(words[0], float(words[2][:-1]), dset = 'val')
			line = f.readline()
		self.nghost = self.hdf5_file['hav/nghost'][0]
		f.close()
		self.hdf5_file.close()
		print "%s[Done]%s" % (GREEN, NO_COLOR)

	def write_dat_h5(self):
		self.hdf5_file = h5py.File(self.h5_file, 'a')

		self.wdset_all_h5('name', self.model, ddtype = np.dtype((str, len(self.model))), dset = 'val')
		self.wdset_all_h5('meshfile', self.msh_file, ddtype = np.dtype((str, len(self.msh_file))), dset = 'val')
		self.wdset_all_h5('dxfile', self.dx_file, ddtype = np.dtype((str, len(self.msh_file))), dset = 'val')
		self.wdset_all_h5('chkfile', self.chk_file, ddtype = np.dtype((str, len(self.msh_file))), dset = 'val')
		self.wdset_all_h5('filename', self.h5_file, ddtype = np.dtype((str, len(self.h5_file))), dset = 'val')
		self.wdset_all_h5('vs', int(self.version), ddtype = 'i1', dset = 'val')
		self.wdset_all_h5('isnap',self.isnap, ddtype = 'i2', dset = 'val')
		
		self.wdset_arr_h5('depth', self.ym[self.nghost:self.nym-self.nghost], 'hav')
		self.wdset_arr_h5('depth_shiftdn', self.ym[self.nghost:self.nym-self.nghost] - self.dym[self.nghost:self.nym-self.nghost]/2, 'hav')
		self.wdset_arr_h5('depth_shiftup', self.ymdn[self.nghost:self.nym-self.nghost] + self.dym[self.nghost:self.nym-self.nghost]/2, 'hav')

		self.ux = self.get_vel_cube(0)
		self.uy = self.get_vel_cube(1)
		self.uz = self.get_vel_cube(2)
		a = self.ux*self.ux + self.uz*self.uz
		b = self.uy*self.uy
		self.u = np.sqrt(a + b)
		self.pturb = b*self.ydn(self.rho)
		self.uh = np.sqrt(a)
		self.ee = np.divide(self.e, self.rho)

		del a; del b

		ux = np.array([]); uxu = np.array([]); uxd = np.array([]); uxs = np.array([]); uxrms = np.array([]); 
		uxmin = np.array([]); uxmax = np.array([]); uxurms = np.array([]); uxdrms = np.array([])
		uy = np.array([]); uyu = np.array([]); uyd = np.array([]); uys = np.array([]); uyrms = np.array([]); 
		uymin = np.array([]); uymax = np.array([]); uyurms = np.array([]); uydrms = np.array([])
		uz = np.array([]); uzu = np.array([]); uzd = np.array([]); uzs = np.array([]); uzrms = np.array([]); 
		uzmin = np.array([]); uzmax = np.array([]); uzurms = np.array([]); uzdrms = np.array([])
		u = np.array([]); uu = np.array([]); ud = np.array([]); us = np.array([]); urms = np.array([]); 
		umin = np.array([]); umax = np.array([]); uurms = np.array([]); udrms = np.array([])
 		uh = np.array([]); uhu = np.array([]); uhd = np.array([]); uhs = np.array([]); uhrms = np.array([]); 
 		uhmin = np.array([]); uhmax = np.array([]); uhurms = np.array([]); uhdrms = np.array([])
 		rho = np.array([]); rhou = np.array([]); rhod = np.array([]); rhos = np.array([]); rhorms = np.array([]); 
 		rhomin = np.array([]); rhomax = np.array([]); rholgm = np.array([])
 		tt = np.array([]); ttu = np.array([]); ttd = np.array([]); tts = np.array([]); ttrms = np.array([]); 
 		ttmin = np.array([]); ttmax = np.array([]); ttlgm = np.array([])
 		e = np.array([]); eu = np.array([]); ed = np.array([]); es = np.array([]); erms = np.array([]); 
 		emin = np.array([]); emax = np.array([]); elgm = np.array([])
 		ee = np.array([]); eeu = np.array([]); eed = np.array([]); ees = np.array([]); eerms = np.array([]); 
 		eemin = np.array([]); eemax = np.array([]); eelgm = np.array([])
 		fmass = np.array([]); fmassu = np.array([]); fmassd = np.array([]); fmasss = np.array([]); fmassrms = np.array([]); 
		fmassmin = np.array([]); fmassmax = np.array([]); fmassurms = np.array([]); fmassdrms = np.array([])
		mmlu = np.array([]); mmld = np.array([])
		pturb = np.array([]); pturbu = np.array([]); pturbd = np.array([]); pturbs = np.array([]); pturbrms = np.array([]); 
 		pturbmin = np.array([]); pturbmax = np.array([]); pturblgm = np.array([])


		for i in range(self.nym):#range(self.nghost, self.nym-self.nghost):
			#Velocities
			slc_uy = self.sl_stat(self.uy[:, i, :], i, lgm = False, ud_rms = True, store_indices = True)
			slc_ux = self.sl_stat(self.ux[:, i, :], i, lgm = False, ud_rms = True)
			slc_uz = self.sl_stat(self.uz[:, i, :], i, lgm = False, ud_rms = True)
			slc_u = self.sl_stat(self.u[:, i, :], i, lgm = False, ud_rms = True)
			slc_uh = self.sl_stat(self.uh[:, i, :], i, lgm = False, ud_rms = True)

			ux = np.append(ux, slc_ux[0])
			uxu = np.append(uxu, slc_ux[1])
			uxd = np.append(uxd, slc_ux[2])
			uxs = np.append(uxs, slc_ux[3])
			uxrms = np.append(uxrms, slc_ux[4])
			uxmin = np.append(uxmin, slc_ux[5])
			uxmax = np.append(uxmax, slc_ux[6])
			uxurms = np.append(uxurms, slc_ux[7])
			uxdrms = np.append(uxdrms, slc_ux[8])

			uy = np.append(uy, slc_uy[0])
			uyu = np.append(uyu, slc_uy[1])
			uyd = np.append(uyd, slc_uy[2])
			uys = np.append(uys, slc_uy[3])
			uyrms = np.append(uyrms, slc_uy[4])
			uymin = np.append(uymin, slc_uy[5])
			uymax = np.append(uymax, slc_uy[6])
			uyurms = np.append(uyurms, slc_uy[7])
			uydrms = np.append(uydrms, slc_uy[8])

			uz = np.append(uz, slc_uz[0])
			uzu = np.append(uzu, slc_uz[1])
			uzd = np.append(uzd, slc_uz[2])
			uzs = np.append(uzs, slc_uz[3])
			uzrms = np.append(uzrms, slc_uz[4])
			uzmin = np.append(uzmin, slc_uz[5])
			uzmax = np.append(uzmax, slc_uz[6])
			uzurms = np.append(uzurms, slc_uz[7])
			uzdrms = np.append(uzdrms, slc_uz[8])

			u = np.append(u, slc_u[0])
			uu = np.append(uu, slc_u[1])
			ud = np.append(ud, slc_u[2])
			us = np.append(us, slc_u[3])
			urms = np.append(urms, slc_u[4])
			umin = np.append(umin, slc_u[5])
			umax = np.append(umax, slc_u[6])
			uurms = np.append(uurms, slc_u[7])
			udrms = np.append(udrms, slc_u[8])

			uh = np.append(uh, slc_uh[0])
			uhu = np.append(uhu, slc_uh[1])
			uhd = np.append(uhd, slc_uh[2])
			uhs = np.append(uhs, slc_uh[3])
			uhrms = np.append(uhrms, slc_uh[4])
			uhmin = np.append(uhmin, slc_uh[5])
			uhmax = np.append(uhmax, slc_uh[6])
			uhurms = np.append(uhurms, slc_uh[7])
			uhdrms = np.append(uhdrms, slc_uh[8])

			# rho
			slc_rho = self.sl_stat(self.rho[:, i, :], i)

			rho = np.append(rho, slc_rho[0])
			rhou = np.append(rhou, slc_rho[1])
			rhod = np.append(rhod, slc_rho[2])
			rhos = np.append(rhos, slc_rho[3])
			rhorms = np.append(rhorms, slc_rho[4])
			rhomin = np.append(rhomin, slc_rho[5])
			rhomax = np.append(rhomax, slc_rho[6])
			rholgm = np.append(rholgm, slc_rho[7])

			# tt
			slc_tt = self.sl_stat(self.tt[:, i, :], i)

			tt = np.append(tt, slc_tt[0])
			ttu = np.append(ttu, slc_tt[1])
			ttd = np.append(ttd, slc_tt[2])
			tts = np.append(tts, slc_tt[3])
			ttrms = np.append(ttrms, slc_tt[4])
			ttmin = np.append(ttmin, slc_tt[5])
			ttmax = np.append(ttmax, slc_tt[6])
			ttlgm = np.append(ttlgm, slc_tt[7])


			# energy per unit mass
			slc_e = self.sl_stat(self.e[:, i, :], i)

			e = np.append(e, slc_e[0])
			eu = np.append(eu, slc_e[1])
			ed = np.append(ed, slc_e[2])
			es = np.append(es, slc_e[3])
			erms = np.append(erms, slc_e[4])
			emin = np.append(emin, slc_e[5])
			emax = np.append(emax, slc_e[6])
			elgm = np.append(elgm, slc_e[7])

			# energy per unit volume
			slc_ee = self.sl_stat(self.ee[:, i, :], i)

			ee = np.append(ee, slc_ee[0])
			eeu = np.append(eeu, slc_ee[1])
			eed = np.append(eed, slc_ee[2])
			ees = np.append(ees, slc_ee[3])
			eerms = np.append(eerms, slc_ee[4])
			eemin = np.append(eemin, slc_ee[5])
			eemax = np.append(eemax, slc_ee[6])
			eelgm = np.append(eelgm, slc_ee[7])

			# mass flux
			slc_fmass = self.sl_stat(self.py[:, i, :], i, lgm = False, ud_rms = True)

			fmass = np.append(fmass, slc_fmass[0])
			fmassu = np.append(fmassu, slc_fmass[1])
			fmassd = np.append(fmassd, slc_fmass[2])
			fmasss = np.append(fmasss, slc_fmass[3])
			fmassrms = np.append(fmassrms, slc_fmass[4])
			fmassmin = np.append(fmassmin, slc_fmass[5])
			fmassmax = np.append(fmassmax, slc_fmass[6])
			fmassurms = np.append(fmassurms, slc_fmass[7])
			fmassdrms = np.append(fmassdrms, slc_fmass[8])

			# turbulent pressure
			slc_pturb = self.sl_stat(self.pturb[:, i, :], i)

			pturb = np.append(pturb, slc_pturb[0])
			pturbu = np.append(pturbu, slc_pturb[1])
			pturbd = np.append(pturbd, slc_pturb[2])
			pturbs = np.append(pturbs, slc_pturb[3])
			pturbrms = np.append(pturbrms, slc_pturb[4])
			pturbmin = np.append(pturbmin, slc_pturb[5])
			pturbmax = np.append(pturbmax, slc_pturb[6])
			pturblgm = np.append(pturblgm, slc_pturb[7])

		#Intensities
		#slc_i = self.sl_stat(self.tt[:, 0, :], i - self.nghost, lgm = False)
		slc_i = self.sl_stat(self.tt[:, 0, :], i, lgm = False)

		Int = np.append(e, slc_i[0])
		intu = np.append(eu, slc_i[1])
		intd = np.append(ed, slc_i[2])
		ints = np.append(es, slc_i[3])
		intrms = np.append(erms, slc_i[4])
		intmin = np.append(emin, slc_i[5])
		intmax = np.append(emax, slc_i[6])

		a = self.nghost
		b = self.nym - self.nghost 

		self.wdset_arr_h5('ux', ux[a:b], 'hav')
		self.wdset_arr_h5('uxu', uxu[a:b], 'hav')
		self.wdset_arr_h5('uxd', uxd[a:b], 'hav')
		self.wdset_arr_h5('uxs', uxs[a:b], 'hav')
		self.wdset_arr_h5('uxrms', uxrms[a:b], 'hav')
		self.wdset_arr_h5('uxmin', uxmin[a:b], 'hav')
		self.wdset_arr_h5('uxmax', uxmax[a:b], 'hav')
		self.wdset_arr_h5('uxurms', uxurms[a:b], 'hav')
		self.wdset_arr_h5('uxdrms', uxdrms[a:b], 'hav')
		self.wdset_arr_h5('uy', uy[a:b], 'hav')
		self.wdset_arr_h5('uyu', uyu[a:b], 'hav')
		self.wdset_arr_h5('uyd', uyd[a:b], 'hav')
		self.wdset_arr_h5('uys', uys[a:b], 'hav')
		self.wdset_arr_h5('uyrms', uyrms[a:b], 'hav')
		self.wdset_arr_h5('uymin', uymin[a:b], 'hav')
		self.wdset_arr_h5('uymax', uymax[a:b], 'hav')
		self.wdset_arr_h5('uyurms', uyurms[a:b], 'hav')
		self.wdset_arr_h5('uydrms', uydrms[a:b], 'hav')
		self.wdset_arr_h5('uz', uz[a:b], 'hav')
		self.wdset_arr_h5('uzu', uzu[a:b], 'hav')
		self.wdset_arr_h5('uzd', uzd[a:b], 'hav')
		self.wdset_arr_h5('uzs', uzs[a:b], 'hav')
		self.wdset_arr_h5('uzrms', uzrms[a:b], 'hav')
		self.wdset_arr_h5('uzmin', uzmin[a:b], 'hav')
		self.wdset_arr_h5('uzmax', uzmax[a:b], 'hav')
		self.wdset_arr_h5('uzurms', uzurms[a:b], 'hav')
		self.wdset_arr_h5('uzdrms', uzdrms[a:b], 'hav')
		self.wdset_arr_h5('u', u[a:b], 'hav')
		self.wdset_arr_h5('uu', uu[a:b], 'hav')
		self.wdset_arr_h5('ud', ud[a:b], 'hav')
		self.wdset_arr_h5('urms', urms[a:b], 'hav')
		self.wdset_arr_h5('umin', umin[a:b], 'hav')
		self.wdset_arr_h5('umax', umax[a:b], 'hav')
		self.wdset_arr_h5('uurms', uurms[a:b], 'hav')
		self.wdset_arr_h5('udrms', udrms[a:b], 'hav')
		self.wdset_arr_h5('uh', uh[a:b], 'hav')
		self.wdset_arr_h5('uhu', uhu[a:b], 'hav')
		self.wdset_arr_h5('uhd', uhd[a:b], 'hav')
		self.wdset_arr_h5('uhs', uhs[a:b], 'hav')
		self.wdset_arr_h5('uhrms', uhrms[a:b], 'hav')
		self.wdset_arr_h5('uhmin', uhmin[a:b], 'hav')
		self.wdset_arr_h5('uhmax', uhmax[a:b], 'hav')
		self.wdset_arr_h5('uhurms', uhurms[a:b], 'hav')
		self.wdset_arr_h5('uhdrms', uhdrms[a:b], 'hav')
		self.wdset_arr_h5('rho', rho[a:b], 'hav')
		self.wdset_arr_h5('rhou', rhou[a:b], 'hav')
		self.wdset_arr_h5('rhod', rhod[a:b], 'hav')
		self.wdset_arr_h5('rhos', rhos[a:b], 'hav')
		self.wdset_arr_h5('rhorms', rhorms[a:b], 'hav')
		self.wdset_arr_h5('rhomin', rhomin[a:b], 'hav')
		self.wdset_arr_h5('rhomax', rhomax[a:b], 'hav')
		self.wdset_arr_h5('rholgm', rholgm[a:b], 'hav')
		self.wdset_arr_h5('tt', tt[a:b], 'hav')
		self.wdset_arr_h5('ttu', ttu[a:b], 'hav')
		self.wdset_arr_h5('ttd', ttd[a:b], 'hav')
		self.wdset_arr_h5('tts', tts[a:b], 'hav')
		self.wdset_arr_h5('ttrms', ttrms[a:b], 'hav')
		self.wdset_arr_h5('ttmin', ttmin[a:b], 'hav')
		self.wdset_arr_h5('ttmax', ttmax[a:b], 'hav')
		self.wdset_arr_h5('ttlgm', ttlgm[a:b], 'hav')
		self.wdset_arr_h5('e', e[a:b], 'hav')
		self.wdset_arr_h5('eu', eu[a:b], 'hav')
		self.wdset_arr_h5('ed', es[a:b], 'hav')
		self.wdset_arr_h5('erms', erms[a:b], 'hav')
		self.wdset_arr_h5('emin', emin[a:b], 'hav')
		self.wdset_arr_h5('emax', emax[a:b], 'hav')
		self.wdset_arr_h5('elgm', elgm[a:b], 'hav')
		self.wdset_arr_h5('ee', ee[a:b], 'hav')
		self.wdset_arr_h5('eeu', eeu[a:b], 'hav')
		self.wdset_arr_h5('eed', ees[a:b], 'hav')
		self.wdset_arr_h5('eerms', eerms[a:b], 'hav')
		self.wdset_arr_h5('eemin', eemin[a:b], 'hav')
		self.wdset_arr_h5('eemax', eemax[a:b], 'hav')
		self.wdset_arr_h5('eelgm', eelgm[a:b], 'hav')
		self.wdset_arr_h5('int', Int[a:b], 'hav')
		self.wdset_arr_h5('intu', intu[a:b], 'hav')
		self.wdset_arr_h5('intd', ints[a:b], 'hav')
		self.wdset_arr_h5('intrms', intrms[a:b], 'hav')
		self.wdset_arr_h5('intmin', intmin[a:b], 'hav')
		self.wdset_arr_h5('intmax', intmax[a:b], 'hav')
		self.wdset_arr_h5('fmass', fmass[a:b], 'hav')
		self.wdset_arr_h5('fmassu', fmassu[a:b], 'hav')
		self.wdset_arr_h5('fmassd', fmassd[a:b], 'hav')
		self.wdset_arr_h5('fmasss', fmasss[a:b], 'hav')
		self.wdset_arr_h5('fmassrms', fmassrms[a:b], 'hav')
		self.wdset_arr_h5('fmassmin', fmassmin[a:b], 'hav')
		self.wdset_arr_h5('fmassmax', fmassmax[a:b], 'hav')
		self.wdset_arr_h5('fmassurms', fmassurms[a:b], 'hav')
		self.wdset_arr_h5('fmassdrms', fmassdrms[a:b], 'hav')
		self.wdset_arr_h5('mmlu', (1./np.abs((np.log(np.roll(fmassu, -1)) - np.log(fmassu)) * self.dyidydn))[a:b], 'hav')
		self.wdset_arr_h5('mmld', (1./np.abs((np.log(np.roll(fmassd, -1)) - np.log(fmassd)) * self.dyidydn))[a:b], 'hav')
		self.wdset_arr_h5('pturb', pturb[a:b], 'hav')
		self.wdset_arr_h5('pturbu', pturbu[a:b], 'hav')
		self.wdset_arr_h5('pturbd', pturbs[a:b], 'hav')
		self.wdset_arr_h5('pturbrms', pturbrms[a:b], 'hav')
		self.wdset_arr_h5('pturbmin', pturbmin[a:b], 'hav')
		self.wdset_arr_h5('pturbmax', pturbmax[a:b], 'hav')
		self.wdset_arr_h5('pturblgm', pturblgm[a:b], 'hav')

		del ux; del uxu; del uxd; del uxs; del uxrms
		del uxmin; del uxmax; del uxurms; del uxdrms
		del uy; del uyu; del uyd; del uys; del uyrms
		del uymin; del uymax; del uyurms; del uydrms
		del uz; del uzu; del uzd; del uzs; del uzrms
		del uzmin; del uzmax; del uzurms; del uzdrms
		del u; del uu; del ud; del us; del urms
		del umin; del umax; del uurms; del udrms
 		del uh; del uhu; del uhd; del uhs; del uhrms
 		del uhmin; del uhmax; del uhurms; del uhdrms
 		del rho; del rhou; del rhod; del rhos; del rhorms
 		del rhomin; del rhomax; del rholgm
 		del tt; del ttu; del ttd; del tts; del ttrms
 		del ttmin; del ttmax; del ttlgm
 		del e; del eu; del ed; del es; del erms
 		del emin; del emax; del elgm
 		del ee; del eeu; del eed; del ees; del eerms
 		del eemin; del eemax; del eelgm
 		del Int; del intu; del intd; del ints; del intrms
 		del intmin; del intmax
 		del fmass; del fmassu; del fmassd; del fmasss; del fmassrms
		del fmassmin; del fmassmax; del fmassurms; del fmassdrms
		del mmlu; del mmld
		del pturb; del pturbu; del pturbd; del pturbs; del pturbrms
 		del pturbmin; del pturbmax; del pturblgm

		self.hdf5_file.close()



