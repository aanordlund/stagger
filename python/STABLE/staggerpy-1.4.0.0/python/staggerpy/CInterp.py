#!/anaconda/bin/python
# coding: utf-8

"""
@AUTHOR: Louis Manchon                             
@ORGN: Århus University, Stellar Astrophysic Center
@FILE: CInterp.py                                   
@DATE: July 2016                                   
"""

from const import *
import numpy as np
from CSimu import *
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import math, h5py, os

__metaclass__ = type

class CInterp(CSimu):
	"""
	Class that compute an interpolated model using the stagger grid.
	"""
	def __init__(self, teff_aim, g_aim, feh_aim):
		"""
		Parameters
		----------
		teff_aim : float
			Aimed temperature.
		g_aim : float
			Aimed log g.
		feh_aim : float
			Aimed metallicity.
		"""
		# Check if the aimed physical quantities are in the interval covered by the stagger grid.
		if ((teff_aim < 4000) | (teff_aim > 7000)) | ((g_aim < 1.5) | (g_aim > 5.)) | ((feh_aim < -1.5) | (feh_aim > 5.)):
			raise ValueError("teff_aim must lie between 4000 and 7000 K, g_aim between 1.5 and 5. and feh_aim between 1.5 and 5.")
		else : 
			# Standard name of this aimed simulation
			self.model = 't' + str(teff_aim) + 'g' + str(g_aim) + 'm' + str(feh_aim*10)
			self.path = os.getcwd()
			self.teff = teff_aim
			self.g = g_aim
			self.feh = feh_aim
			# Find the surrounding cube
			self.find_cube()

	def __getitem__(self, item):
		"""
			Return the item stored in ``.hdf5`` file.
	
			Parameter
			---------
			item : str
			    path in the ``.hdf5`` file.
	
			Examples
    		--------
    		>>> mdl_interp.open_h5(op_mode = 'r')
    		>>> mdl_interp['hav/name']
    			't45g25p05'
		"""
		if len(self.hdf5_file[self.model + '/' + item]) == 1:
			return self.hdf5_file[self.model + '/' + item][0]
		else : 
			return self.hdf5_file[self.model + '/' + item].value
			
	def open_h5(self, op_mode = 'a'):
		"""
			Open the associated ``.hdf5`` file.
	
			Parameter
			---------
			op_mode : str
				``Opening mode.``
				``r			Readonly, file must exist``
				``r+			Read/write, file must exist``
				``w			Create file, truncate if exists``
				``w- or x		Create file, fail if exists``
				``a			Read/write if exists, create otherwise (default)``
	
			See also
			--------
			CSnapsh.close_h5 : Close the associate ``.hdf5`` file.
			
		"""
		try:
			self.hdf5_file = h5py.File(self.h5_file, op_mode)
		except:
			pass

	def close_h5(self):
		"""
			Close the ``.hdf5`` file
	
			See also
			--------
			CSnapsh.open_h5 : Open the associate ``.hdf5`` file.
	
		"""
		try:
			self.hdf5_file.close()
		except:
			pass

	def trilin_interp(self, values):
		"""
		Compute a value of the model txxgyymzz by triliear interpolation.

		Parameters
		----------
		values : nd array
			Values at the edge of the cube
		"""

		# Values in an array of float
		if self.feh < -1 : 
			xd = (self.teff-self.teff0)/500; yd = 2*(self.g-self.logg0); zd = self.feh-self.feh0; 
			xxd = 1- xd; yyd = 1- yd; 
		else :
			xd = (self.teff-self.teff0)/500; yd = 2*(self.g-self.logg0); zd = 2*(self.feh-self.feh0);
			xxd = 1- xd; yyd = 1- yd;
		# Coefficients
		c00 = values[0]*xxd+values[1]*xd
		c01 = values[3]*xxd+values[4]*xd
		c10 = values[2]*xxd+values[6]*xd
		c11 = values[5]*xxd+values[7]*xd

		c0 = c00*yyd + c10*yd
		c1 = c01*yyd + c11*yd

		return c0*(1-zd)+c1*zd

	def find_cube(self):
		"""
		This method find the 6 surrounding models in the stagger grid.
		"""
		# Nearest neighbours along temperature axis
		t = int(self.teff/1000)*1000 + 500
		if t < self.teff:
			self.teff0 = t
			self.teff1 = t + 500
		else : 
			self.teff0 = t - 500
			self.teff1 = t
		# Nearest neighbours along log g axis
		lg = int(self.g) + 0.5
		if lg < self.g : 
			self.logg0 = lg
			self.logg1 = lg + 0.5
		else : 
			self.logg0 = lg - 0.5
			self.logg1 = lg 
		# Nearest neighbours along feh axis
		if self.feh < -1.0:
			f = int(self.feh)
			self.feh0 = f
			self.feh1 = f + 1.0
		else : 
			f = int(self.feh) + 0.5
			if f < self.feh:
				self.feh0 = f
				self.feh1 = f + 0.5
			else : 
				self.feh0 = f - 0.5
				self.feh1 = f
		# Names of the nearest simulations
		names = ['t'+str(int(self.teff0/100))+'g'+str(int(self.logg0*10))+'m%02d' % (int(self.feh0*10)),
		't'+str(int(self.teff1/100))+'g'+str(int(self.logg0*10))+'m%02d' % (int(self.feh0*10)),
		't'+str(int(self.teff0/100))+'g'+str(int(self.logg1*10))+'m%02d' % (int(self.feh0*10)),
		't'+str(int(self.teff0/100))+'g'+str(int(self.logg0*10))+'m%02d' % (int(self.feh1*10)),
		't'+str(int(self.teff1/100))+'g'+str(int(self.logg0*10))+'m%02d' % (int(self.feh1*10)),
		't'+str(int(self.teff0/100))+'g'+str(int(self.logg1*10))+'m%02d' % (int(self.feh1*10)),
		't'+str(int(self.teff1/100))+'g'+str(int(self.logg1*10))+'m%02d' % (int(self.feh0*10)),
		't'+str(int(self.teff1/100))+'g'+str(int(self.logg1*10))+'m%02d' % (int(self.feh1*10))]
		# slef.cube contans class instances of CSimu of nearest simulations
		self.cube = [CSimu(path_to_stagger_grid + '/' + names[0], names[0], read = False), 
		CSimu(path_to_stagger_grid + '/' + names[1], names[1], read = False),
		CSimu(path_to_stagger_grid + '/' + names[2], names[2], read = False),
		CSimu(path_to_stagger_grid + '/' + names[3], names[3], read = False),
		CSimu(path_to_stagger_grid + '/' + names[4], names[4], read = False),
		CSimu(path_to_stagger_grid + '/' + names[5], names[5], read = False),
		CSimu(path_to_stagger_grid + '/' + names[6], names[6], read = False),
		CSimu(path_to_stagger_grid + '/' + names[7], names[7], read = False)]

	def wgrp_h5(self, group_name):
		"""
		Try to create the group.

		Parameters
		----------
		group_name : str
			Name of the group to be created.
		"""
		try : 
			self.hdf5_file.create_group(group_name)
		except :
			pass

	def wdset_arr_h5(self, name, array, group_name, ddtype = 'f4'):
		"""
		Try to write an array in the ``.hdf5`` file.

		Parameters
		----------
		name : str
			Name of the group to be created.
		array : np.array
		group_name : str
			Name of the group in which the array should be copied
		dtype : str
			Data type of the ``np.array``. You should stick to one endianness otherwise, 
			everything will be messed up. By defult we use big endian.	
		"""
		try : 
			self.hdf5_file[group_name].create_dataset(name, data = array, dtype = ddtype)
			self.hdf5_file[group_name+'/'+name].attrs['legend'] = var[name][0]
			self.hdf5_file[group_name+'/'+name].attrs['factor'] = var[name][1]
			self.hdf5_file[group_name+'/'+name].attrs['unit'] = var[name][2]
		except: 
			pass

	def trilin_interp_mmd(self, teff, logg, feh):
		"""
		Compute a value of the model ``txxgyymzz`` by triliear interpolation.

		Parameters
		----------
		teff : float
		logg : float
		feh : float

		"""
		# We make sure we are in the right directory
		os.chdir(self.path)
		self.hdf5_file = h5py.File(self.h5_file, 'w')

		try : 
			self.hdf5_file.create_group(self.model)
			self.wgrp_h5('hav')
			self.wgrp_h5('thavn')
			self.wgrp_h5('rhavn')
			self.wgrp_h5('t5havn')
		except :
			pass

		# Open all the hdf5 files of all the nearest simulations
		for i in self.cube:
			i.open_all_h5(op_mode = 'r')
		# list of groups in hdf5 files
		self.list_group = self.cube.hdf5_files.keys()
		for j in self.list_group:
			self.list_var = self.cube.hdf5_files[j].keys()
			for i in self.list_var:
				# Value interpolation
				if str(mdl_ref[i].dtype)[:2] == '|S':
					pass
				else : 
					values = np.array([j[i] for j in self.cube])
					self.wdset_arr_h5(i, self.trilin_interp_arr(values), self.model, ddtype = mdl_ref[i].dtype)


