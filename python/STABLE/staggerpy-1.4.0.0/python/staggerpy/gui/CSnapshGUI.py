#!/anaconda/bin/python
# coding: utf-8

###################################################################
##    AUTHOR: Louis Manchon                                      ##
##    ORGN: Århus University, Stellar Astrophysic Center         ##
##    FILE: CSnapshGUI.py                                        ##
##    DATE: June 2016                                            ##
###################################################################


import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm
from matplotlib.colors import LogNorm
from matplotlib.legend_handler import HandlerLine2D
from scipy import interpolate
import math, h5py, os, sys

sys.path.append('/Users/Loulou/Desktop/Louis/Mag2/STAGE/staggerpy/python/staggerpy/')

from const import *
from CSnapsh import *


class CSnapshGUI(CSnapsh):
	"""
	Class that post process the raw data of a snapshot. Data are stored in a Direct-Output Binary File *.dat.

	"""
	
	def __init__(self, file, model, path = None, read = False, v = '05'):
		"""
		Parameters
		----------
		file : string
			Name of the ``.dat`` file.
		model : str
			Name of the model: ``txxgyymzz``.

		path : str
			Path to the directory in wich the snapshots are stored. It is advisable to set it explicitly, 
			unless your already in the right directory.
		read : bool
			If ``False``, doesn't read the files.
		v : str
			Version.
		"""
		self.model = model
		self.file_dat = file #raw data (Direct-Output Binary File)
		self.version = v
		# Guess the path. It is advisable to set it explicitly.
		if path == None:
			self.path = os.getcwd()
		else:
			self.path = path
		# Create instance of CSnapsh and open the associate hdf5 dile in reading mode.
		self.CSnapsh = CSnapsh(file, model, path = path, read = read, v = v)
		self.hdf5_file = self.CSnapsh.hdf5_file
		self.CSnapsh.open_h5(op_mode = 'r')
		self.nxm = self.CSnapsh.nxm
		self.nym = self.CSnapsh.nym
		self.nzm = self.CSnapsh.nzm
		self.nghost = self.CSnapsh.nghost
		# Set some default plotting parameters 
		plt.rc('figure', figsize = [12,9])
		plt.rc('text', usetex=True)
		plt.rc('font', size= 20)
		plt.rc('savefig', dpi = 500)

	def read(self, endian = '>'):
		"""
		Alias of ``CSnapsh.read()``.

		Parameter
		----------
		endian : str
			Endianness.
		"""
		self.CSnapsh.read(endian = endian)

	def read_msh(self, endian = '>'):
		"""
		Alias of ``CSnapsh.read_msh()``.

		Parameter
		---------
		endian : str
			Endianness.
		"""
		self.CSnapsh.read_msh(endian = endian)

	def read_dat(self, endian = '>'):
		"""
		Alias of ``CSnapsh.read_dat()``.

		Parameter
		---------
		endian : str
			Endianness.
		"""
		self.CSnapsh.read_dat(endian = endian)

	def read_dat_cube(self, var, endian = '>'):
		"""
		Alias of ``CSnapsh.read_dat_cube()``.

		Parameter
		---------
		endian : str
			Endianness.
		"""
		self.CSnapsh.read_dat_cube(var, endian = endian)

	def read_hav(self):
		"""
		Alias of ``CSnapsh.read_dat_cube()``.
		"""
		self.CSnapsh.read_hav()

	def read_dx(self):
		"""
		Alias of ``CSnapsh.read_dx()``.
		"""
		self.CSnapsh.read_dx()

	def read_chk(self):
		"""
		Alias of ``CSnapsh.read_chk()``.
		"""
		self.CSnapsh.read_chk()

	def plt_slc(self, slc, vmin = None, vmax = None, cmap=cm.gist_heat, interpolation='nearest', save = False, save_name = 'figure1'):
		"""
		Plot a slice.

		Parameters
		----------
		slc : 2d array
			Slice you want to plot
		vmin : float
			Minimal value displayed. All value < ``vmin`` will have the same color.
		vmax : float
			Maximal value displayed. All value > ``vmin`` will have the same color.	
		cmap : matplotlib.colors.Colormap
			Color map.
			Recommended: ``cm.Greys``, ``cm.gray``, ``cm.gist_heat``, ``cm.bwr``, ``cm.seismic``, ``cm.jet``.
			More at http://matplotlib.org/examples/color/colormaps_reference.html
		interpolation : str
		save : bool
			If ``True``, save the plot.
		save_name : string
			Name of the saved plot.
		"""

		plt.imshow(slc, vmin = vmin, vmax = vmax, cmap = cmap, interpolation='nearest')
		plt.colorbar()
		plt.show()
		if save : 
			plt.savefig(save_name)

	def phavs(self, x, y, style = '', color = '', log = None, norm = 1.):
		"""
		This method plot horizontal average along an other variable.

		Parameter
		---------
		x : str
			Name of the ``x`` variable. Must be stored in the ``.hdf5`` file associated.
		y : str
			Name of the ``y`` variable. Must be stored in the ``.hdf5`` file associated.
		style : str
			Style of the curve.
		color : str
			Color of the curve.
		log : str
			Must designate the axis that will show logarithmic coordinates.
			Possible arguments: ``None``, ``'X'``, ``'x'``, ``'Y'``, ``'y'``, ``'XY'`` or ``'xy'``.
		norm : float
			Normalization factor.
		"""
		if (self.model+'/'+x in self.hdf5_file) and (self.model+'/'+y in self.hdf5_file):
			# Log scale ? 
			if log == None:
				pass
			elif log == 'X' or log == 'x':
				plt.semilogx
			elif log == 'Y' or log == 'y':
				plt.semilogy
			elif log == 'XY' or log == 'xy':
				plt.loglog()
			else : 
				raise ValueError("log must be equal to None, 'X', 'x', 'Y', 'y', 'XY' or 'xy'.")

			if norm != 1.:
				# Normalized plot
				plt.plot(self.hdf5_file[self.model+'/'+x], self.hdf5_file[self.model+'/'+y]/norm, style+color)
			else :
				plt.plot(self.hdf5_file[self.model+'/'+x], self.hdf5_file[self.model+'/'+y], style+color)
			# plot legend
			plt.xlabel(self.hdf5_file[self.model+'/'+x].attrs['legend'], fontsize=20)
			plt.ylabel(self.hdf5_file[self.model+'/'+y].attrs['legend'], fontsize=20)

		else : 
			raise ValueError("It seems that '%s' or '%s' is not stored in %s." % (x, y, self.CSnapsh.h5_file))


	def phavc(self, x, y, mdl2, style = '', color = '', log = None, rel = True, norm = 1.):
		"""
		This method plot horizontal average along an other variable.

		Parameter
		---------
		x : str
			Name of the ``x`` variable. Must be stored in the ``.hdf5`` file associated.
		y : str
			Name of the ``y`` variable. Must be stored in the ``.hdf5`` file associated.
		mdl2 : CSnapsh or CSnapshGUI instance
			``mdl - mdl2``.
		style : str
			Style of the curve.
		color : str
			Color of the curve.
		log : str
			Must designate the axis that will show logarithmic coordinates.
			Possible arguments: ``None``, ``'X'``, ``'x'``, ``'Y'``, ``'y'``, ``'XY'`` or ``'xy'``.
		rel : bool
			If ``True``, plot relative value, otherwise, absolute value. 
		norm : float
			Normalization factor.
		"""
		if (self.model+'/'+x in self.hdf5_file) and (self.model+'/'+y in self.hdf5_file):
			# log scale ? 
			if log == None:
				pass
			elif log == 'X' or log == 'x':
				plt.semilogx
			elif log == 'Y' or log == 'y':
				plt.semilogy
			elif log == 'XY' or log == 'xy':
				plt.loglog()
			else : 
				raise ValueError("log must be equal to None, 'X', 'x', 'Y', 'y', 'XY' or 'xy'.")

			# value from mdl2
			x2 = mdl2.hdf5_file[mdl2.model+'/'+x]
			y2 = mdl2.hdf5_file[mdl2.model+'/'+y]

			# normalization
			y1 = self.hdf5_file[self.model+'/'+y]/norm
			y2 = y2/norm

			# spline representation of model 1 an model 2
			tck = interpolate.splrep(self.hdf5_file[self.model+'/'+x], y1)
			tck2 = interpolate.splrep(x2, y2)

			# limit values along abscissa
			xmin = max(min(self.hdf5_file[self.model+'/'+x]), min(x2)) ; xmax = min(max(self.hdf5_file[self.model+'/'+x]), max(x2))
			# Sample value on [xmin; xmax] intervalle 
			xx = np.linspace(xmin, xmax, int((xmax-xmin)*1000))
			# Evaluation of spline1 - spline2
			yy = interpolate.splev(xx, tck) - interpolate.splev(xx, tck2)
			# Relative or absolute value
			if rel: 
				plt.plot(xx, yy, style+color)
			else : 
				plt.plot(xx, np.abs(yy), style+color)
			# plot legend
			plt.xlabel(self.hdf5_file[self.model + '/' + x].attrs['legend'], fontsize=20)
			plt.ylabel(self.hdf5_file[self.model + '/' + y].attrs['legend'], fontsize=20)
		else : 
			raise ValueError("It seems that '%s' or '%s' is not stored in %s." % (x, y, self.h5_file))


	def phstat(self, x, y, log = None, norm = 1.):
		"""
		This method plot horizontal average along an other variable.

		Parameter
		---------
		x : str
			Name of the ``x`` variable. Must be stored in the ``.hdf5`` file associated.
		y : str
			Name of the ``y`` variable. Must be stored in the ``.hdf5`` file associated.
		log : str
			Must designate the axis that will show logarithmic coordinates.
			Possible arguments: ``None``, ``'X'``, ``'x'``, ``'Y'``, ``'y'``, ``'XY'`` or ``'xy'``.
		norm : float
			Normalization factor.
		"""

		if (self.model+'/'+x in self.hdf5_file) and (self.model+'/'+y in self.hdf5_file) and (self.model+'/'+y+'u' in self.hdf5_file):
			# log scale ? 
			if log == None:
				pass
			elif log == 'X' or log == 'x':
				plt.semilogx
			elif log == 'Y' or log == 'y':
				plt.semilogy
			elif log == 'XY' or log == 'xy':
				plt.loglog()
			else : 
				raise ValueError("log must be equal to None, 'X', 'x', 'Y', 'y', 'XY' or 'xy'.")

			# plot several statistical quantities and set labels
			s = self.model + '/'
			line1, = plt.plot(self.hdf5_file[s + x], self.hdf5_file[s + y][:] / norm, 'k', label = 'Mean')
			line2, = plt.plot(self.hdf5_file[s + x], self.hdf5_file[s + y + 'u'][:] / norm, '--b', label = 'Upflow')
			line3, = plt.plot(self.hdf5_file[s + x], self.hdf5_file[s + y + 'd'][:] / norm, '--r', label = 'Downflow')
			line4, = plt.plot(self.hdf5_file[s + x], self.hdf5_file[s + y + 's'][:] / norm, '--k', label = 'Sigma')
			line5, = plt.plot(self.hdf5_file[s + x], self.hdf5_file[s + y + 'rms'][:] / norm, 'b', label = 'RMS')
			line6, = plt.plot(self.hdf5_file[s + x], self.hdf5_file[s + y + 'min'][:] / norm, 'k:', label = 'Extrema')
			line7, = plt.plot(self.hdf5_file[s + x], self.hdf5_file[s + y + 'max'][:] / norm, 'k:')
			line5.set_dashes([8, 4, 2, 4, 2, 4])

			# Plot labels and legend
			plt.legend(handler_map={line1: HandlerLine2D(numpoints=4)})
			plt.xlabel(self.hdf5_file[s + x].attrs['legend'], fontsize=20)
			plt.ylabel(self.hdf5_file[s + y].attrs['legend'], fontsize=20)


	def plot_hist2d(self, x, y, lim1=None, nbin1=256, lim2=None, nbin2=256, 
		axis=None, bins=None, ctable='Normalize', beta=1., reverse=False):
			"""
			Plot the 2D histogram of two array.

			Parameters
			----------
			x : 3D np.array
				Name of the data cube. 
			y : 3D np.array
				Name of the data cube.  
			lim1 : tuple (pair) of float
				Min and max values of ``x`` you want to plot.
			nbin1 : int
				Number of bins.
			lim2 : tuple (pair) of float
				Min and max values of ``y`` you want to plot.
			nbin2 : int
				Number of bins.
			axis : tuple of float (4 items)
				If want not to display the axis, set ``axis = 'off'``.
			bins : tuple (pair) of int	
			ctable : str
				Name of the color table.
			beta : float
				Enhanced the contrast.
			reverse : bool
				Reverse the ``y``.
			"""
			# Detremine intervalle limit
			if lim1 != None:
				ax_min = min(np.amin(x), lim1[0]) ; ax_max = max(np.amax(x), lim1[1])
			else:
				ax_min = np.amin(x) ; ax_max = np.amax(x)
			if lim2 != None:
				ay_min = min(np.amin(y), lim2[0]) ; ay_max = max(np.amax(y), lim2[1])
			else:
				ay_min = np.amin(y) ; ay_max = np.amax(y)
			# Detremine bin size
			if bins == None:
				b = max(math.ceil((ax_max - ax_min) / (nbin1 - 1)), math.ceil((ay_max - ay_min) / (nbin2 - 1)))
				bins = [b, b]
			# Plot slice by slice
			for i in range(self.nghost, self.nym - self.nghost):
				if lim1 == None and lim2 == None:
					h1 = x[:, i, :] # x value
					h2 = y[:, i, :] # y value
				elif lim1 != None and lim2 != None:
					h1 = (x[np.where((x[:, i, :] > lim1[0]) & (x[:, i, :] < lim1[1]))] - lim1[0])/(lim1[1] - lim1[0])*nbin1
					h2 = (y[np.where((y[:, i, :] > lim2[0]) & (y[:, i, :] < lim2[1]))] - lim2[0])/(lim2[1] - lim2[0])*nbin2
				elif lim1 == None : 
					h1 = x[:, i, :]
					h2 = (y[np.where((y[:, i, :] > lim2[0]) & (y[:, i, :] < lim2[1]))] - lim2[0])/(lim2[1] - lim2[0])*nbin2
				elif lim2 == None : 
					h2 = y[:, i, :]
					h1 = (x[np.where((x[:, i, :] > lim1[0]) & (x[:, i, :] < lim1[1]))] - lim1[0])/(lim1[1] - lim1[0])*nbin1	
				else : 
					raise ValueError("The arguments 'lim1' and 'lim2' must be None or a list of 2 floats.")
				# Enhanced contrast
				h2 = [i**beta for i in h2]
				# Reverse ordinates
				if reverse :
					h2 = [max(h2) - i for i in h2]
				plt.hist2d(h1.flatten(), h2.flatten(), bins = bins, norm = LogNorm())
			# Set axis at the end
			if axis == None : 
				plt.axis([ax_min*1.01, ax_max*1.01, ay_min*1.01, ay_max*1.01])
			else : 
				plt.axis(axis)
			plt.show()
	







