#!/anaconda/bin/python
# coding: utf-8

###################################################################
##    AUTHOR: Louis Manchon                                      ##
##    ORGN: Århus University, Stellar Astrophysic Center         ##
##    FILE: CSnapsh.py                                           ##
##    DATE: June 2016                                            ##
###################################################################

from const import *
from CEOStab import *
from CSnapsh import *
import numpy as np
from bisect import *
from scipy import interpolate
import math, h5py, os, glob

__metaclass__ = type

class CWriteh5():
	"""
	Class that write the data from the averagings perform by ``CSnapsh``.
	"""
	def __init__(self, csnapsh):
		"""
		Parameter
		---------
		csnapsh : class CSnapsh
		"""
		self.csnapsh = csnapsh 
		#self.open_h5()
		self.h5_file = self.csnapsh.h5_file
		self.hdf5_file = self.csnapsh.hdf5_file
		#We make sure that the hdf5 file is open in appending mode ('a').
		self.close_h5()
		self.open_h5()

	def open_h5(self, op_mode = 'a'):
		"""
			Open the associated ``.hdf5`` file.
	
			Parameter
			---------
			op_mode : str
				``Opening mode.``
				``r			Readonly, file must exist``
				``r+			Read/write, file must exist``
				``w			Create file, truncate if exists``
				``w- or x		Create file, fail if exists``
				``a			Read/write if exists, create otherwise (default)``
	
			See also
			--------
			CSnapsh.close_h5 : Close the associate ``.hdf5`` file.
			
		"""
		try:
			self.hdf5_file = h5py.File(self.h5_file, op_mode)
		except:
			pass

	def close_h5(self):
		"""
			Close the ``.hdf5`` file
	
			See also
			--------
			CSnapsh.open_h5 : Open the associate ``.hdf5`` file.
	
		"""
		try:
			self.hdf5_file.close()
		except:
			pass

	def wgrp_h5(self, group_name):
		"""
			Try to create the group in the ``.hdf5`` file.

			Parameters
			----------
			group_name : str
				Name of the group to be created.
		"""
		try : 
			self.hdf5_file.create_group(group_name)
		except :
			pass

	def wdset_val_h5(self, name, value, group_name, ddtype = 'f4'):
		"""
			Try to write a single value in the ``.hdf5`` file.

			Parameters
			----------
			name : str
				Name of the group to be created.
			value : single value (int or float or str)
			group_name : str	
				Name of the group in which the value should be copied
			dtype : str	
				Data type of the value. You should stick to one endianness otherwise, 
				everything will be messed up. By defult we use big endian.	
		"""
		try : 
			self.hdf5_file[group_name].create_dataset(name, data = [value], dtype = ddtype)
			self.hdf5_file[group_name+'/'+name].attrs['legend'] = var[name][0]
			self.hdf5_file[group_name+'/'+name].attrs['factor'] = var[name][1]
			self.hdf5_file[group_name+'/'+name].attrs['unit'] = var[name][2]
		except:
			pass 

	def wdset_arr_h5(self, name, array, group_name, ddtype = 'f4'):
		"""
			Try to write an array in the ``.hdf5`` file.

			Parameters
			----------
			name : str
				Name of the group to be created.
			array : np.array
			group_name : str	
				Name of the group in which the array should be copied
			dtype : str	
				Data type of the np.array. You should stick to one endianness otherwise, 
				everything will be messed up. By defult we use big endian.
		"""
		try : 
			self.hdf5_file[group_name].create_dataset(name, data = array, dtype = ddtype)
			self.hdf5_file[group_name+'/'+name].attrs['legend'] = var[name][0]
			self.hdf5_file[group_name+'/'+name].attrs['factor'] = var[name][1]
			self.hdf5_file[group_name+'/'+name].attrs['unit'] = var[name][2]
		except: 
			pass

	def wdset_all_h5(self, name, data, ddtype = 'f4', dset = 'arr'):
		"""
			Try to write a data in all the groups.

			Parameters
			----------
			name : str
				Name of the group to be created.
			data : np.array or single value
				Data to be stored.
			group_name : str	
				Name of the group in which the array should be copied.
			dtype : str	
				Data type of the np.array. You should stick to one endianness otherwise, 
				everything will be messed up. By defult we use big endian.
			dset : str
				If ``dset = 'arr'``, call ``self.wdset_arr_h5``, otherwise, call ``self.wdset_val_h5``.
		"""
		if dset == 'arr':
			for i in self.hdf5_file.keys():
				self.wdset_arr_h5(name, data, i, ddtype = ddtype)
		else :
			for i in self.hdf5_file.keys():
				self.wdset_val_h5(name, data, i, ddtype = ddtype)

	def store_attr(self):
		"""
		This method will try to guess if the available RAM memory is sufficient to store 
		some arrays in memory and then speed up the code or if they should be deleted after the call of each method 
		"""
		try: 
			from psutil import virtual_memory
		except: 
			# By default, if the module can not be imported, the arrays will not be stored
			return False
		mem = virtual_memory()
		# limit memory defined as 85 % of the total memory
		limit = (mem.total >> 20) * 0.85
		# Memory needed: we need to store as attributes 12 arrays with the dimension of the raw data cubes
		# and 12 temporary arrays of vertical size equal to 101.
		# Each cell store a 4 bytes float.
		mem_needed = 15. * (self.csnapsh.nhm * self.csnapsh.nym) * 4. + 15. * (self.csnapsh.nhm * 101.) * 4.
		# Memory currently used
		used = float(mem.used >> 20)
		if used + mem_needed < limit:
			return True
		else:
			return False


	def write_hav_h5(self, store = True):
		"""
		Write the horizontal averagings at constant geometrical depth.
		"""
		self.open_h5()
		if 'hav' not in self.hdf5_file.keys():
			self.wgrp_h5('hav')
		a = self.csnapsh.nghost
		b = self.csnapsh.nym - self.csnapsh.nghost

		self.wdset_all_h5('name', self.csnapsh.model, ddtype = np.dtype((str, len(self.csnapsh.model))), dset = 'val')
		self.wdset_all_h5('meshfile', self.csnapsh.msh_file, ddtype = np.dtype((str, len(self.csnapsh.msh_file))), dset = 'val')
		self.wdset_all_h5('dxfile', self.csnapsh.dx_file, ddtype = np.dtype((str, len(self.csnapsh.msh_file))), dset = 'val')
		self.wdset_all_h5('chkfile', self.csnapsh.chk_file, ddtype = np.dtype((str, len(self.csnapsh.msh_file))), dset = 'val')
		self.wdset_all_h5('filename', self.csnapsh.h5_file, ddtype = np.dtype((str, len(self.csnapsh.h5_file))), dset = 'val')
		self.wdset_all_h5('vs', int(self.csnapsh.version), ddtype = 'i1', dset = 'val')
		self.wdset_all_h5('isnap',self.csnapsh.isnap, ddtype = 'i2', dset = 'val')
		
		self.wdset_arr_h5('depth', self.csnapsh.ym[a:b], 'hav')
		self.wdset_arr_h5('depth_shiftdn', self.csnapsh.ym[a:b] - self.csnapsh.dym[a:b] / 2., 'hav')
		self.wdset_arr_h5('depth_shiftup', self.csnapsh.ymdn[a:b] + self.csnapsh.dym[a:b] / 2., 'hav')

		# You want to store some arrays ? 
		if store:
			self.store = self.store_attr()
		UX = self.csnapsh.get_vel_cube(0)
		UY = self.csnapsh.get_vel_cube(1)
		UZ = self.csnapsh.get_vel_cube(2)
		c = UX * UX + UZ * UZ
		d = UY * UY
		vol = self.csnapsh.dxm * self.csnapsh.dym * self.csnapsh.dzm
		U = np.sqrt(c + d)
		PTURB = d * self.csnapsh.ydn(self.csnapsh.rho)
		UH = np.sqrt(c)
		MASS = self.csnapsh.rho * vol
		lnrho = np.log(self.csnapsh.rho * self.csnapsh.uur)
		lne = np.log(self.csnapsh.ee * self.csnapsh.uue)
		PTOT = self.csnapsh.ceostab.lookup_eos(lnrho, lne, 0, 0, linear = False) - math.log(self.csnapsh.uua)
		NEL = self.csnapsh.ceostab.lookup_eos(lnrho, lne, 3, 0, linear = False) - math.log(self.csnapsh.uua)
		KHIR_R = np.divide(np.exp(self.csnapsh.get_lnab(lnrho, lne)), self.csnapsh.rho)# khir/rho
		KHIR5_R = np.divide(np.exp(self.csnapsh.get_lnab(lnrho, lne, kind = 'r5')), self.csnapsh.rho)# khi5/rho
		KHIP_R = np.divide(np.exp(self.csnapsh.get_lnab(lnrho, lne, kind = 'p')), self.csnapsh.rho)# khip/rho
		TT4 = np.power(self.csnapsh.tt, 4)

		del c; del d; del vol; del lnrho; del lne

		# Those arrays will contain the averagings
		ux = np.array([]); uxu = np.array([]); uxd = np.array([]); uxs = np.array([]); uxrms = np.array([]); 
		uxmin = np.array([]); uxmax = np.array([]); uxurms = np.array([]); uxdrms = np.array([])
		uy = np.array([]); uyu = np.array([]); uyd = np.array([]); uys = np.array([]); uyrms = np.array([]); 
		uymin = np.array([]); uymax = np.array([]); uyurms = np.array([]); uydrms = np.array([])
		uz = np.array([]); uzu = np.array([]); uzd = np.array([]); uzs = np.array([]); uzrms = np.array([]); 
		uzmin = np.array([]); uzmax = np.array([]); uzurms = np.array([]); uzdrms = np.array([])
		u = np.array([]); uu = np.array([]); ud = np.array([]); us = np.array([]); urms = np.array([]); 
		umin = np.array([]); umax = np.array([]); uurms = np.array([]); udrms = np.array([])
 		uh = np.array([]); uhu = np.array([]); uhd = np.array([]); uhs = np.array([]); uhrms = np.array([]); 
 		uhmin = np.array([]); uhmax = np.array([]); uhurms = np.array([]); uhdrms = np.array([])
 		rho = np.array([]); rhou = np.array([]); rhod = np.array([]); rhos = np.array([]); rhorms = np.array([]); 
 		rhomin = np.array([]); rhomax = np.array([]); rholgm = np.array([])
 		tt = np.array([]); ttu = np.array([]); ttd = np.array([]); tts = np.array([]); ttrms = np.array([]); 
 		ttmin = np.array([]); ttmax = np.array([]); ttlgm = np.array([]); tt4 = np.array([])
 		e = np.array([]); eu = np.array([]); ed = np.array([]); es = np.array([]); erms = np.array([]); 
 		emin = np.array([]); emax = np.array([]); elgm = np.array([])
 		ee = np.array([]); eeu = np.array([]); eed = np.array([]); ees = np.array([]); eerms = np.array([]); 
 		eemin = np.array([]); eemax = np.array([]); eelgm = np.array([])
 		fmass = np.array([]); fmassu = np.array([]); fmassd = np.array([]); fmasss = np.array([]); fmassrms = np.array([]); 
		fmassmin = np.array([]); fmassmax = np.array([]); fmassurms = np.array([]); fmassdrms = np.array([])
		mass = np.array([])
		mmlu = np.array([]); mmld = np.array([])
		pturb = np.array([]); pturbu = np.array([]); pturbd = np.array([]); pturbs = np.array([]); pturbrms = np.array([]); 
 		pturbmin = np.array([]); pturbmax = np.array([]); pturblgm = np.array([])
 		ptot = np.array([]); ptotu = np.array([]); ptotd = np.array([]); ptots = np.array([]); ptotrms = np.array([]); 
 		ptotmin = np.array([]); ptotmax = np.array([]); ptotlgm = np.array([])
 		nel = np.array([]); nelu = np.array([]); neld = np.array([]); nels = np.array([]); nelrms = np.array([]); 
 		nelmin = np.array([]); nelmax = np.array([]); nellgm = np.array([])
 		kapr = np.array([]); kap5 = np.array([]); kapp = np.array([]) 

		for i in xrange(self.csnapsh.nym):
			#Velocities
			slc_uy = self.csnapsh.sl_stat(UY[:, i, :], i, lgm = False, ud_rms = True, store_indices = True)
			slc_ux = self.csnapsh.sl_stat(UX[:, i, :], i, lgm = False, ud_rms = True)
			slc_uz = self.csnapsh.sl_stat(UZ[:, i, :], i, lgm = False, ud_rms = True)
			slc_u = self.csnapsh.sl_stat(U[:, i, :], i, lgm = False, ud_rms = True)
			slc_uh = self.csnapsh.sl_stat(UH[:, i, :], i, lgm = False, ud_rms = True)

			ux = np.append(ux, slc_ux[0])
			uxu = np.append(uxu, slc_ux[1])
			uxd = np.append(uxd, slc_ux[2])
			uxs = np.append(uxs, slc_ux[3])
			uxrms = np.append(uxrms, slc_ux[4])
			uxmin = np.append(uxmin, slc_ux[5])
			uxmax = np.append(uxmax, slc_ux[6])
			uxurms = np.append(uxurms, slc_ux[7])
			uxdrms = np.append(uxdrms, slc_ux[8])

			uy = np.append(uy, slc_uy[0])
			uyu = np.append(uyu, slc_uy[1])
			uyd = np.append(uyd, slc_uy[2])
			uys = np.append(uys, slc_uy[3])
			uyrms = np.append(uyrms, slc_uy[4])
			uymin = np.append(uymin, slc_uy[5])
			uymax = np.append(uymax, slc_uy[6])
			uyurms = np.append(uyurms, slc_uy[7])
			uydrms = np.append(uydrms, slc_uy[8])

			uz = np.append(uz, slc_uz[0])
			uzu = np.append(uzu, slc_uz[1])
			uzd = np.append(uzd, slc_uz[2])
			uzs = np.append(uzs, slc_uz[3])
			uzrms = np.append(uzrms, slc_uz[4])
			uzmin = np.append(uzmin, slc_uz[5])
			uzmax = np.append(uzmax, slc_uz[6])
			uzurms = np.append(uzurms, slc_uz[7])
			uzdrms = np.append(uzdrms, slc_uz[8])

			u = np.append(u, slc_u[0])
			uu = np.append(uu, slc_u[1])
			ud = np.append(ud, slc_u[2])
			us = np.append(us, slc_u[3])
			urms = np.append(urms, slc_u[4])
			umin = np.append(umin, slc_u[5])
			umax = np.append(umax, slc_u[6])
			uurms = np.append(uurms, slc_u[7])
			udrms = np.append(udrms, slc_u[8])

			uh = np.append(uh, slc_uh[0])
			uhu = np.append(uhu, slc_uh[1])
			uhd = np.append(uhd, slc_uh[2])
			uhs = np.append(uhs, slc_uh[3])
			uhrms = np.append(uhrms, slc_uh[4])
			uhmin = np.append(uhmin, slc_uh[5])
			uhmax = np.append(uhmax, slc_uh[6])
			uhurms = np.append(uhurms, slc_uh[7])
			uhdrms = np.append(uhdrms, slc_uh[8])

			# rho
			slc_rho = self.csnapsh.sl_stat(self.csnapsh.rho[:, i, :], i)

			rho = np.append(rho, slc_rho[0])
			rhou = np.append(rhou, slc_rho[1])
			rhod = np.append(rhod, slc_rho[2])
			rhos = np.append(rhos, slc_rho[3])
			rhorms = np.append(rhorms, slc_rho[4])
			rhomin = np.append(rhomin, slc_rho[5])
			rhomax = np.append(rhomax, slc_rho[6])
			rholgm = np.append(rholgm, slc_rho[7])

			# tt
			slc_tt = self.csnapsh.sl_stat(self.csnapsh.tt[:, i, :], i)

			tt = np.append(tt, slc_tt[0])
			ttu = np.append(ttu, slc_tt[1])
			ttd = np.append(ttd, slc_tt[2])
			tts = np.append(tts, slc_tt[3])
			ttrms = np.append(ttrms, slc_tt[4])
			ttmin = np.append(ttmin, slc_tt[5])
			ttmax = np.append(ttmax, slc_tt[6])
			ttlgm = np.append(ttlgm, slc_tt[7])
			tt4 = np.append(tt4, np.mean(TT4[:, i, :]))

			# energy per unit mass
			slc_e = self.csnapsh.sl_stat(self.csnapsh.e[:, i, :], i)

			e = np.append(e, slc_e[0])
			eu = np.append(eu, slc_e[1])
			ed = np.append(ed, slc_e[2])
			es = np.append(es, slc_e[3])
			erms = np.append(erms, slc_e[4])
			emin = np.append(emin, slc_e[5])
			emax = np.append(emax, slc_e[6])
			elgm = np.append(elgm, slc_e[7])

			# energy per unit volume
			slc_ee = self.csnapsh.sl_stat(self.csnapsh.ee[:, i, :], i)

			ee = np.append(ee, slc_ee[0])
			eeu = np.append(eeu, slc_ee[1])
			eed = np.append(eed, slc_ee[2])
			ees = np.append(ees, slc_ee[3])
			eerms = np.append(eerms, slc_ee[4])
			eemin = np.append(eemin, slc_ee[5])
			eemax = np.append(eemax, slc_ee[6])
			eelgm = np.append(eelgm, slc_ee[7])

			# mass flux
			slc_fmass = self.csnapsh.sl_stat(self.csnapsh.py[:, i, :], i, intensive = False, lgm = False, ud_rms = True)

			fmass = np.append(fmass, slc_fmass[0])
			fmassu = np.append(fmassu, slc_fmass[1])
			fmassd = np.append(fmassd, slc_fmass[2])
			fmasss = np.append(fmasss, slc_fmass[3])
			fmassrms = np.append(fmassrms, slc_fmass[4])
			fmassmin = np.append(fmassmin, slc_fmass[5])
			fmassmax = np.append(fmassmax, slc_fmass[6])
			fmassurms = np.append(fmassurms, slc_fmass[7])
			fmassdrms = np.append(fmassdrms, slc_fmass[8])

			# Total pressure
			slc_ptot = self.csnapsh.sl_stat(PTOT[:, i, :], i)
			ptot = np.append(ptot, slc_ptot[0])
			ptotu = np.append(ptotu, slc_ptot[1])
			ptotd = np.append(ptotd, slc_ptot[2])
			ptots = np.append(ptots, slc_ptot[3])
			ptotrms = np.append(ptotrms, slc_ptot[4])
			ptotmin = np.append(ptotmin, slc_ptot[5])
			ptotmax = np.append(ptotmax, slc_ptot[6])
			ptotlgm = np.append(ptotlgm, slc_ptot[7])

			# turbulent pressure
			slc_pturb = self.csnapsh.sl_stat(PTURB[:, i, :], i)
			pturb = np.append(pturb, slc_pturb[0])
			pturbu = np.append(pturbu, slc_pturb[1])
			pturbd = np.append(pturbd, slc_pturb[2])
			pturbs = np.append(pturbs, slc_pturb[3])
			pturbrms = np.append(pturbrms, slc_pturb[4])
			pturbmin = np.append(pturbmin, slc_pturb[5])
			pturbmax = np.append(pturbmax, slc_pturb[6])
			pturblgm = np.append(pturblgm, slc_pturb[7])

			# Electron density
			slc_nel = self.csnapsh.sl_stat(NEL[:, i, :], i)
			nel = np.append(nel, slc_nel[0])
			nelu = np.append(nelu, slc_nel[1])
			neld = np.append(neld, slc_nel[2])
			nels = np.append(nels, slc_nel[3])
			nelrms = np.append(nelrms, slc_nel[4])
			nelmin = np.append(nelmin, slc_nel[5])
			nelmax = np.append(nelmax, slc_nel[6])
			nellgm = np.append(nellgm, slc_nel[7])

			# Rosseland, Rosseland at 500nm and Planck extinction coeff
			kapr = np.append(kapr, np.mean(KHIR_R[:, i, :]))
			kap5 = np.append(kap5, np.mean(KHIR5_R[:, i, :]))
			kapp = np.append(kapp, np.mean(KHIP_R[:, i, :]))

		# Intensities
		slc_i = self.csnapsh.sl_stat(self.csnapsh.tt[:, 0, :], i, lgm = False)

		Int = np.append(e, slc_i[0])
		intu = np.append(eu, slc_i[1])
		intd = np.append(ed, slc_i[2])
		ints = np.append(es, slc_i[3])
		intrms = np.append(erms, slc_i[4])
		intmin = np.append(emin, slc_i[5])
		intmax = np.append(emax, slc_i[6])

		# Total mass per slice
		#mass = np.append(mass, np.sum(MASS[:, i, :]))
		mass = np.sum(np.sum(MASS, axis = 0), axis = 1)

		# Average optical depth in the Rosseland mean, 
		#in the Rosseland mean at 500nm, in the Planck mean
		LTAUR, LTAUP, LTAU5 = self.csnapsh.get_ltau_all()
		ltaur = np.mean(np.mean(LTAUR, axis = 0), axis = 1)
		ltau5 = np.mean(np.mean(LTAU5, axis = 0), axis = 1)
		ltaup = np.mean(np.mean(LTAUP, axis = 0), axis = 1)
		
		self.wdset_arr_h5('ux', ux[a:b], 'hav')
		self.wdset_arr_h5('uxu', uxu[a:b], 'hav')
		self.wdset_arr_h5('uxd', uxd[a:b], 'hav')
		self.wdset_arr_h5('uxs', uxs[a:b], 'hav')
		self.wdset_arr_h5('uxrms', uxrms[a:b], 'hav')
		self.wdset_arr_h5('uxmin', uxmin[a:b], 'hav')
		self.wdset_arr_h5('uxmax', uxmax[a:b], 'hav')
		self.wdset_arr_h5('uxurms', uxurms[a:b], 'hav')
		self.wdset_arr_h5('uxdrms', uxdrms[a:b], 'hav')
		self.wdset_arr_h5('uy', uy[a:b], 'hav')
		self.wdset_arr_h5('uyu', uyu[a:b], 'hav')
		self.wdset_arr_h5('uyd', uyd[a:b], 'hav')
		self.wdset_arr_h5('uys', uys[a:b], 'hav')
		self.wdset_arr_h5('uyrms', uyrms[a:b], 'hav')
		self.wdset_arr_h5('uymin', uymin[a:b], 'hav')
		self.wdset_arr_h5('uymax', uymax[a:b], 'hav')
		self.wdset_arr_h5('uyurms', uyurms[a:b], 'hav')
		self.wdset_arr_h5('uydrms', uydrms[a:b], 'hav')
		self.wdset_arr_h5('uz', uz[a:b], 'hav')
		self.wdset_arr_h5('uzu', uzu[a:b], 'hav')
		self.wdset_arr_h5('uzd', uzd[a:b], 'hav')
		self.wdset_arr_h5('uzs', uzs[a:b], 'hav')
		self.wdset_arr_h5('uzrms', uzrms[a:b], 'hav')
		self.wdset_arr_h5('uzmin', uzmin[a:b], 'hav')
		self.wdset_arr_h5('uzmax', uzmax[a:b], 'hav')
		self.wdset_arr_h5('uzurms', uzurms[a:b], 'hav')
		self.wdset_arr_h5('uzdrms', uzdrms[a:b], 'hav')
		self.wdset_arr_h5('u', u[a:b], 'hav')
		self.wdset_arr_h5('uu', uu[a:b], 'hav')
		self.wdset_arr_h5('ud', ud[a:b], 'hav')
		self.wdset_arr_h5('urms', urms[a:b], 'hav')
		self.wdset_arr_h5('umin', umin[a:b], 'hav')
		self.wdset_arr_h5('umax', umax[a:b], 'hav')
		self.wdset_arr_h5('uurms', uurms[a:b], 'hav')
		self.wdset_arr_h5('udrms', udrms[a:b], 'hav')
		self.wdset_arr_h5('uh', uh[a:b], 'hav')
		self.wdset_arr_h5('uhu', uhu[a:b], 'hav')
		self.wdset_arr_h5('uhd', uhd[a:b], 'hav')
		self.wdset_arr_h5('uhs', uhs[a:b], 'hav')
		self.wdset_arr_h5('uhrms', uhrms[a:b], 'hav')
		self.wdset_arr_h5('uhmin', uhmin[a:b], 'hav')
		self.wdset_arr_h5('uhmax', uhmax[a:b], 'hav')
		self.wdset_arr_h5('uhurms', uhurms[a:b], 'hav')
		self.wdset_arr_h5('uhdrms', uhdrms[a:b], 'hav')
		self.wdset_arr_h5('rho', rho[a:b], 'hav')
		self.wdset_arr_h5('rhou', rhou[a:b], 'hav')
		self.wdset_arr_h5('rhod', rhod[a:b], 'hav')
		self.wdset_arr_h5('rhos', rhos[a:b], 'hav')
		self.wdset_arr_h5('rhorms', rhorms[a:b], 'hav')
		self.wdset_arr_h5('rhomin', rhomin[a:b], 'hav')
		self.wdset_arr_h5('rhomax', rhomax[a:b], 'hav')
		self.wdset_arr_h5('rholgm', rholgm[a:b], 'hav')
		self.wdset_arr_h5('tt', tt[a:b], 'hav')
		self.wdset_arr_h5('ttu', ttu[a:b], 'hav')
		self.wdset_arr_h5('ttd', ttd[a:b], 'hav')
		self.wdset_arr_h5('tts', tts[a:b], 'hav')
		self.wdset_arr_h5('ttrms', ttrms[a:b], 'hav')
		self.wdset_arr_h5('ttmin', ttmin[a:b], 'hav')
		self.wdset_arr_h5('ttmax', ttmax[a:b], 'hav')
		self.wdset_arr_h5('ttlgm', ttlgm[a:b], 'hav')
		self.wdset_arr_h5('tt4', tt4[a:b], 'hav')
		self.wdset_arr_h5('e', e[a:b], 'hav')
		self.wdset_arr_h5('eu', eu[a:b], 'hav')
		self.wdset_arr_h5('ed', es[a:b], 'hav')
		self.wdset_arr_h5('erms', erms[a:b], 'hav')
		self.wdset_arr_h5('emin', emin[a:b], 'hav')
		self.wdset_arr_h5('emax', emax[a:b], 'hav')
		self.wdset_arr_h5('elgm', elgm[a:b], 'hav')
		self.wdset_arr_h5('ee', ee[a:b], 'hav')
		self.wdset_arr_h5('eeu', eeu[a:b], 'hav')
		self.wdset_arr_h5('eed', ees[a:b], 'hav')
		self.wdset_arr_h5('eerms', eerms[a:b], 'hav')
		self.wdset_arr_h5('eemin', eemin[a:b], 'hav')
		self.wdset_arr_h5('eemax', eemax[a:b], 'hav')
		self.wdset_arr_h5('eelgm', eelgm[a:b], 'hav')
		self.wdset_arr_h5('int', Int[a:b], 'hav')
		self.wdset_arr_h5('intu', intu[a:b], 'hav')
		self.wdset_arr_h5('intd', ints[a:b], 'hav')
		self.wdset_arr_h5('intrms', intrms[a:b], 'hav')
		self.wdset_arr_h5('intmin', intmin[a:b], 'hav')
		self.wdset_arr_h5('intmax', intmax[a:b], 'hav')
		self.wdset_arr_h5('fmass', fmass[a:b], 'hav')
		self.wdset_arr_h5('fmassu', fmassu[a:b], 'hav')
		self.wdset_arr_h5('fmassd', fmassd[a:b], 'hav')
		self.wdset_arr_h5('fmasss', fmasss[a:b], 'hav')
		self.wdset_arr_h5('fmassrms', fmassrms[a:b], 'hav')
		self.wdset_arr_h5('fmassmin', fmassmin[a:b], 'hav')
		self.wdset_arr_h5('fmassmax', fmassmax[a:b], 'hav')
		self.wdset_arr_h5('fmassurms', fmassurms[a:b], 'hav')
		self.wdset_arr_h5('fmassdrms', fmassdrms[a:b], 'hav')
		mmlu = 1./np.abs((np.log(np.abs(np.roll(fmassu, -1))) - np.log(np.abs(fmassu))) * self.csnapsh.dyidydn)
		mmld = 1./np.abs((np.log(np.roll(fmassd, -1)) - np.log(fmassd)) * self.csnapsh.dyidydn)
		self.wdset_arr_h5('mmlu', mmlu[a:b], 'hav')
		self.wdset_arr_h5('mmld', mmld[a:b], 'hav')
		self.wdset_arr_h5('pturb', pturb[a:b], 'hav')
		self.wdset_arr_h5('pturbu', pturbu[a:b], 'hav')
		self.wdset_arr_h5('pturbd', pturbs[a:b], 'hav')
		self.wdset_arr_h5('pturbrms', pturbrms[a:b], 'hav')
		self.wdset_arr_h5('pturbmin', pturbmin[a:b], 'hav')
		self.wdset_arr_h5('pturbmax', pturbmax[a:b], 'hav')
		self.wdset_arr_h5('pturblgm', pturblgm[a:b], 'hav')
		self.wdset_arr_h5('ptot', ptot[a:b], 'hav')
		self.wdset_arr_h5('ptotu', ptotu[a:b], 'hav')
		self.wdset_arr_h5('ptotd', ptots[a:b], 'hav')
		self.wdset_arr_h5('ptotrms', ptotrms[a:b], 'hav')
		self.wdset_arr_h5('ptotmin', ptotmin[a:b], 'hav')
		self.wdset_arr_h5('ptotmax', ptotmax[a:b], 'hav')
		self.wdset_arr_h5('ptotlgm', ptotlgm[a:b], 'hav')
		self.wdset_arr_h5('nel', nel[a:b], 'hav')
		self.wdset_arr_h5('nelu', nelu[a:b], 'hav')
		self.wdset_arr_h5('neld', nels[a:b], 'hav')
		self.wdset_arr_h5('nelrms', nelrms[a:b], 'hav')
		self.wdset_arr_h5('nelmin', nelmin[a:b], 'hav')
		self.wdset_arr_h5('nelmax', nelmax[a:b], 'hav')
		self.wdset_arr_h5('nellgm', nellgm[a:b], 'hav')
		self.wdset_arr_h5('mass', mass[a:b], 'hav')
		self.wdset_arr_h5('kapr', np.log(kapr[a:b]), 'hav')
		self.wdset_arr_h5('kap5', np.log(kap5[a:b]), 'hav')
		self.wdset_arr_h5('kapp', np.log(kapp[a:b]), 'hav')
		self.wdset_arr_h5('ltaur', ltaur[a:b], 'hav')
		self.wdset_arr_h5('ltau5', ltau5[a:b], 'hav')
		self.wdset_arr_h5('ltaup', ltaup[a:b], 'hav')

		del ux; del uxu; del uxd; del uxs; del uxrms
		del uxmin; del uxmax; del uxurms; del uxdrms
		del uy; del uyu; del uyd; del uys; del uyrms
		del uymin; del uymax; del uyurms; del uydrms
		del uz; del uzu; del uzd; del uzs; del uzrms
		del uzmin; del uzmax; del uzurms; del uzdrms
		del u; del uu; del ud; del us; del urms
		del umin; del umax; del uurms; del udrms
		del uh; del uhu; del uhd; del uhs; del uhrms
		del uhmin; del uhmax; del uhurms; del uhdrms
		del rho; del rhou; del rhod; del rhos; del rhorms
		del rhomin; del rhomax; del rholgm
		del tt; del ttu; del ttd; del tts; del ttrms
		del ttmin; del ttmax; del ttlgm; del tt4
		del e; del eu; del ed; del es; del erms
		del emin; del emax; del elgm
		del ee; del eeu; del eed; del ees; del eerms
		del eemin; del eemax; del eelgm
		del Int; del intu; del intd; del ints; del intrms
		del intmin; del intmax
		del fmass; del fmassu; del fmassd; del fmasss; del fmassrms
		del fmassmin; del fmassmax; del fmassurms; del fmassdrms
		del mmlu; del mmld
		del pturb; del pturbu; del pturbd; del pturbs; del pturbrms
		del pturbmin; del pturbmax; del pturblgm
		del mass; del ltaur; del ltau5; del ltaup; del kapr; del kap5; del kapp
		del ptot; del ptotu; del ptotd; del ptots; del ptotrms
		del ptotmin; del ptotmax; del ptotlgm
		del nel; del nelu; del neld; del nels; del nelrms
		del nelmin; del nelmax; del nellgm

		if self.store:
			self.ux = UX 
			del UX
			self.uy = UY 
			del UY
			self.uz = UZ 
			del UZ
			self.u = U 
			del U
			self.pturb = PTURB
			del PTURB
			self.uh = UH
			del UH
			self.mass = MASS
			del MASS
			self.ptot = PTOT
			del PTOT
			self.nel = NEL
			del NEL
			self.khir_r = KHIR_R 
			del KHIR_R
			self.khir5_r = KHIR5_R
			del KHIR5_R
			self.khip_r = KHIP_R 
			del KHIP_R
			self.tt4 = TT4
			del TT4
			self.ltaur = LTAUR
			del LTAUR
			self.ltau5 = LTAU5
			del LTAU5
			self.ltaup = LTAUP
			del LTAUP
		else:
			del UX; del UY; del UZ; del U; del PTURB; del UH; del MASS; del PTOT; del NEL; 
			del KHIR_R; del KHIR5_R; del KHIP_R; del TT4; del LTAUR; del LTAU5; del LTAUP

		self.hdf5_file.close()

#-----------------------------------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------------------------------

	def write_thavn_h5(self):
		"""
		Write the averagings at constant Rosseland optical depth.
		"""
		self.open_h5()
		if 'thavn' not in self.hdf5_file.keys():
			self.wgrp_h5('thavn')
		a = self.csnapsh.nghost
		b = self.csnapsh.nym - self.csnapsh.nghost

		# Interpolate basic data cubes
		if self.store:
			RHO = self.csnapsh.new_ym(self.ltaur, self.csnapsh.rho)
			TT = self.csnapsh.new_ym(self.ltaur, self.csnapsh.tt)
			E = self.csnapsh.new_ym(self.ltaur, self.csnapsh.e)
			EE = np.divide(E, RHO)
			PX = self.csnapsh.new_ym(self.ltaur, self.csnapsh.px, staggered = True)
			PY = self.csnapsh.new_ym(self.ltaur, self.csnapsh.py, staggered = True)
			PZ = self.csnapsh.new_ym(self.ltaur, self.csnapsh.pz, staggered = True)
			UX = self.csnapsh.new_ym(self.ltaur, self.ux, staggered = True)
			UY = self.csnapsh.new_ym(self.ltaur, self.uy, staggered = True)
			UZ = self.csnapsh.new_ym(self.ltaur, self.uz, staggered = True)
			U = self.csnapsh.new_ym(self.ltaur, self.u, staggered = True)
			PTURB = self.csnapsh.new_ym(self.ltaur, self.pturb)
			UH = self.csnapsh.new_ym(self.ltaur, self.uh, staggered = True)
			MASS = self.csnapsh.new_ym(self.ltaur, self.mass)
			PTOT = self.csnapsh.new_ym(self.ltaur, self.ptot)
			NEL = self.csnapsh.new_ym(self.ltaur, self.nel)
			TT4 = self.csnapsh.new_ym(self.ltaur, self.tt4)
		else :
			LTAUR = self.csnapsh.get_ltau()
			RHO = self.csnapsh.new_ym(LTAUR, self.csnapsh.rho, linear = True)
			TT = self.csnapsh.new_ym(LTAUR, self.csnapsh.tt, linear = True)
			E = self.csnapsh.new_ym(LTAUR, self.csnapsh.e, linear = True)
			EE = np.divide(E, RHO)
			PX = self.csnapsh.new_ym(LTAUR, self.csnapsh.px, staggered = True, linear = True)
			PY = self.csnapsh.new_ym(LTAUR, self.csnapsh.py, staggered = True, linear = True)
			PZ = self.csnapsh.new_ym(LTAUR, self.csnapsh.pz, staggered = True, linear = True)
			UX = self.csnapsh.new_ym(LTAUR, np.divide(PX, self.csnapsh.xdn(RHO)), staggered = True, linear = True)
			UY = self.csnapsh.new_ym(LTAUR, np.divide(PY, self.csnapsh.xdn(RHO)), staggered = True, linear = True)
			UZ = self.csnapsh.new_ym(LTAUR, np.divide(PZ, self.csnapsh.xdn(RHO)), staggered = True, linear = True)
			c = UX * UX + UZ * UZ
			d = UY * UY
			vol = self.csnapsh.dxm * self.csnapsh.dym * self.csnapsh.dzm
			U = np.sqrt(c + d)
			PTURB = d * self.csnapsh.ydn(RHO)
			UH = np.sqrt(c)
			lnrho = np.log(self.csnapsh.rho * self.csnapsh.uur)
			lne = np.log(self.csnapsh.ee * self.csnapsh.uue)
			PTOT = self.csnapsh.ceostab.lookup_eos(lnrho, lne, 0, 0, linear = False) - math.log(self.csnapsh.uua)
			PTOT = self.csnapsh.new_ym(LTAUR, PTOT, linear = True)
			NEL = self.csnapsh.ceostab.lookup_eos(lnrho, lne, 3, 0, linear = False) - math.log(self.csnapsh.uua)
			NEL = self.csnapsh.new_ym(LTAUR, NEL, linear = True)
			KHIR_R = np.divide(np.exp(self.csnapsh.get_lnab(lnrho, lne)), self.csnapsh.rho)# khir/rho
			KHIR5_R = np.divide(np.exp(self.csnapsh.get_lnab(lnrho, lne, kind = 'r5')), self.csnapsh.rho)# khi5/rho
			KHIP_R = np.divide(np.exp(self.csnapsh.get_lnab(lnrho, lne, kind = 'p')), self.csnapsh.rho)# khip/rho
			KHIR_R = self.csnapsh.new_ym(LTAUR, KHIR_R, linear = True)
			KHIR5_R = self.csnapsh.new_ym(LTAUR, KHIR5_R, linear = True)
			KHIP_R = self.csnapsh.new_ym(LTAUR, KHIP_R, linear = True)
			TT4 = np.power(TT, 4)

		# Those arrays will contain the averagings
		ux = np.array([]); uxu = np.array([]); uxd = np.array([]); uxs = np.array([]); uxrms = np.array([]); 
		uxmin = np.array([]); uxmax = np.array([]); uxurms = np.array([]); uxdrms = np.array([])
		uy = np.array([]); uyu = np.array([]); uyd = np.array([]); uys = np.array([]); uyrms = np.array([]); 
		uymin = np.array([]); uymax = np.array([]); uyurms = np.array([]); uydrms = np.array([])
		uz = np.array([]); uzu = np.array([]); uzd = np.array([]); uzs = np.array([]); uzrms = np.array([]); 
		uzmin = np.array([]); uzmax = np.array([]); uzurms = np.array([]); uzdrms = np.array([])
		u = np.array([]); uu = np.array([]); ud = np.array([]); us = np.array([]); urms = np.array([]); 
		umin = np.array([]); umax = np.array([]); uurms = np.array([]); udrms = np.array([])
 		uh = np.array([]); uhu = np.array([]); uhd = np.array([]); uhs = np.array([]); uhrms = np.array([]); 
 		uhmin = np.array([]); uhmax = np.array([]); uhurms = np.array([]); uhdrms = np.array([])
 		rho = np.array([]); rhou = np.array([]); rhod = np.array([]); rhos = np.array([]); rhorms = np.array([]); 
 		rhomin = np.array([]); rhomax = np.array([]); rholgm = np.array([])
 		tt = np.array([]); ttu = np.array([]); ttd = np.array([]); tts = np.array([]); ttrms = np.array([]); 
 		ttmin = np.array([]); ttmax = np.array([]); ttlgm = np.array([]); tt4 = np.array([])
 		e = np.array([]); eu = np.array([]); ed = np.array([]); es = np.array([]); erms = np.array([]); 
 		emin = np.array([]); emax = np.array([]); elgm = np.array([])
 		ee = np.array([]); eeu = np.array([]); eed = np.array([]); ees = np.array([]); eerms = np.array([]); 
 		eemin = np.array([]); eemax = np.array([]); eelgm = np.array([])
 		fmass = np.array([]); fmassu = np.array([]); fmassd = np.array([]); fmasss = np.array([]); fmassrms = np.array([]); 
		fmassmin = np.array([]); fmassmax = np.array([]); fmassurms = np.array([]); fmassdrms = np.array([])
		mass = np.array([])
		mmlu = np.array([]); mmld = np.array([])
		pturb = np.array([]); pturbu = np.array([]); pturbd = np.array([]); pturbs = np.array([]); pturbrms = np.array([]); 
 		pturbmin = np.array([]); pturbmax = np.array([]); pturblgm = np.array([])
 		ptot = np.array([]); ptotu = np.array([]); ptotd = np.array([]); ptots = np.array([]); ptotrms = np.array([]); 
 		ptotmin = np.array([]); ptotmax = np.array([]); ptotlgm = np.array([])
 		nel = np.array([]); nelu = np.array([]); neld = np.array([]); nels = np.array([]); nelrms = np.array([]); 
 		nelmin = np.array([]); nelmax = np.array([]); nellgm = np.array([])
 		kapr = np.array([]); kap5 = np.array([]); kapp = np.array([]) 

		for i in xrange(101):
			#Velocities
			slc_uy = self.csnapsh.sl_stat(UY[:, i, :], i, lgm = False, ud_rms = True, store_indices = True)
			slc_ux = self.csnapsh.sl_stat(UX[:, i, :], i, lgm = False, ud_rms = True)
			slc_uz = self.csnapsh.sl_stat(UZ[:, i, :], i, lgm = False, ud_rms = True)
			slc_u = self.csnapsh.sl_stat(U[:, i, :], i, lgm = False, ud_rms = True)
			slc_uh = self.csnapsh.sl_stat(UH[:, i, :], i, lgm = False, ud_rms = True)

			ux = np.append(ux, slc_ux[0])
			uxu = np.append(uxu, slc_ux[1])
			uxd = np.append(uxd, slc_ux[2])
			uxs = np.append(uxs, slc_ux[3])
			uxrms = np.append(uxrms, slc_ux[4])
			uxmin = np.append(uxmin, slc_ux[5])
			uxmax = np.append(uxmax, slc_ux[6])
			uxurms = np.append(uxurms, slc_ux[7])
			uxdrms = np.append(uxdrms, slc_ux[8])

			uy = np.append(uy, slc_uy[0])
			uyu = np.append(uyu, slc_uy[1])
			uyd = np.append(uyd, slc_uy[2])
			uys = np.append(uys, slc_uy[3])
			uyrms = np.append(uyrms, slc_uy[4])
			uymin = np.append(uymin, slc_uy[5])
			uymax = np.append(uymax, slc_uy[6])
			uyurms = np.append(uyurms, slc_uy[7])
			uydrms = np.append(uydrms, slc_uy[8])

			uz = np.append(uz, slc_uz[0])
			uzu = np.append(uzu, slc_uz[1])
			uzd = np.append(uzd, slc_uz[2])
			uzs = np.append(uzs, slc_uz[3])
			uzrms = np.append(uzrms, slc_uz[4])
			uzmin = np.append(uzmin, slc_uz[5])
			uzmax = np.append(uzmax, slc_uz[6])
			uzurms = np.append(uzurms, slc_uz[7])
			uzdrms = np.append(uzdrms, slc_uz[8])

			u = np.append(u, slc_u[0])
			uu = np.append(uu, slc_u[1])
			ud = np.append(ud, slc_u[2])
			us = np.append(us, slc_u[3])
			urms = np.append(urms, slc_u[4])
			umin = np.append(umin, slc_u[5])
			umax = np.append(umax, slc_u[6])
			uurms = np.append(uurms, slc_u[7])
			udrms = np.append(udrms, slc_u[8])

			uh = np.append(uh, slc_uh[0])
			uhu = np.append(uhu, slc_uh[1])
			uhd = np.append(uhd, slc_uh[2])
			uhs = np.append(uhs, slc_uh[3])
			uhrms = np.append(uhrms, slc_uh[4])
			uhmin = np.append(uhmin, slc_uh[5])
			uhmax = np.append(uhmax, slc_uh[6])
			uhurms = np.append(uhurms, slc_uh[7])
			uhdrms = np.append(uhdrms, slc_uh[8])

			# rho
			slc_rho = self.csnapsh.sl_stat(self.csnapsh.rho[:, i, :], i)

			rho = np.append(rho, slc_rho[0])
			rhou = np.append(rhou, slc_rho[1])
			rhod = np.append(rhod, slc_rho[2])
			rhos = np.append(rhos, slc_rho[3])
			rhorms = np.append(rhorms, slc_rho[4])
			rhomin = np.append(rhomin, slc_rho[5])
			rhomax = np.append(rhomax, slc_rho[6])
			rholgm = np.append(rholgm, slc_rho[7])

			# tt
			slc_tt = self.csnapsh.sl_stat(self.csnapsh.tt[:, i, :], i)

			tt = np.append(tt, slc_tt[0])
			ttu = np.append(ttu, slc_tt[1])
			ttd = np.append(ttd, slc_tt[2])
			tts = np.append(tts, slc_tt[3])
			ttrms = np.append(ttrms, slc_tt[4])
			ttmin = np.append(ttmin, slc_tt[5])
			ttmax = np.append(ttmax, slc_tt[6])
			ttlgm = np.append(ttlgm, slc_tt[7])
			tt4 = np.append(tt4, np.mean(TT4[:, i, :]))

			# energy per unit mass
			slc_e = self.csnapsh.sl_stat(self.csnapsh.e[:, i, :], i)

			e = np.append(e, slc_e[0])
			eu = np.append(eu, slc_e[1])
			ed = np.append(ed, slc_e[2])
			es = np.append(es, slc_e[3])
			erms = np.append(erms, slc_e[4])
			emin = np.append(emin, slc_e[5])
			emax = np.append(emax, slc_e[6])
			elgm = np.append(elgm, slc_e[7])

			# energy per unit volume
			slc_ee = self.csnapsh.sl_stat(self.csnapsh.ee[:, i, :], i)

			ee = np.append(ee, slc_ee[0])
			eeu = np.append(eeu, slc_ee[1])
			eed = np.append(eed, slc_ee[2])
			ees = np.append(ees, slc_ee[3])
			eerms = np.append(eerms, slc_ee[4])
			eemin = np.append(eemin, slc_ee[5])
			eemax = np.append(eemax, slc_ee[6])
			eelgm = np.append(eelgm, slc_ee[7])

			# mass flux
			slc_fmass = self.csnapsh.sl_stat(self.csnapsh.py[:, i, :], i, intensive = False, lgm = False, ud_rms = True)

			fmass = np.append(fmass, slc_fmass[0])
			fmassu = np.append(fmassu, slc_fmass[1])
			fmassd = np.append(fmassd, slc_fmass[2])
			fmasss = np.append(fmasss, slc_fmass[3])
			fmassrms = np.append(fmassrms, slc_fmass[4])
			fmassmin = np.append(fmassmin, slc_fmass[5])
			fmassmax = np.append(fmassmax, slc_fmass[6])
			fmassurms = np.append(fmassurms, slc_fmass[7])
			fmassdrms = np.append(fmassdrms, slc_fmass[8])

			# Total pressure
			slc_ptot = self.csnapsh.sl_stat(PTOT[:, i, :], i)
			ptot = np.append(ptot, slc_ptot[0])
			ptotu = np.append(ptotu, slc_ptot[1])
			ptotd = np.append(ptotd, slc_ptot[2])
			ptots = np.append(ptots, slc_ptot[3])
			ptotrms = np.append(ptotrms, slc_ptot[4])
			ptotmin = np.append(ptotmin, slc_ptot[5])
			ptotmax = np.append(ptotmax, slc_ptot[6])
			ptotlgm = np.append(ptotlgm, slc_ptot[7])

			# turbulent pressure
			slc_pturb = self.csnapsh.sl_stat(PTURB[:, i, :], i)
			pturb = np.append(pturb, slc_pturb[0])
			pturbu = np.append(pturbu, slc_pturb[1])
			pturbd = np.append(pturbd, slc_pturb[2])
			pturbs = np.append(pturbs, slc_pturb[3])
			pturbrms = np.append(pturbrms, slc_pturb[4])
			pturbmin = np.append(pturbmin, slc_pturb[5])
			pturbmax = np.append(pturbmax, slc_pturb[6])
			pturblgm = np.append(pturblgm, slc_pturb[7])

			# Electron density
			slc_nel = self.csnapsh.sl_stat(NEL[:, i, :], i)
			nel = np.append(nel, slc_nel[0])
			nelu = np.append(nelu, slc_nel[1])
			neld = np.append(neld, slc_nel[2])
			nels = np.append(nels, slc_nel[3])
			nelrms = np.append(nelrms, slc_nel[4])
			nelmin = np.append(nelmin, slc_nel[5])
			nelmax = np.append(nelmax, slc_nel[6])
			nellgm = np.append(nellgm, slc_nel[7])

			# Rosseland, Rosseland at 500nm and Planck extinction coeff
			kapr = np.append(kapr, np.mean(KHIR_R[:, i, :]))
			kap5 = np.append(kap5, np.mean(KHIR5_R[:, i, :]))
			kapp = np.append(kapp, np.mean(KHIP_R[:, i, :]))

		# Intensities
		slc_i = self.csnapsh.sl_stat(self.csnapsh.tt[:, 0, :], i, lgm = False)

		Int = np.append(e, slc_i[0])
		intu = np.append(eu, slc_i[1])
		intd = np.append(ed, slc_i[2])
		ints = np.append(es, slc_i[3])
		intrms = np.append(erms, slc_i[4])
		intmin = np.append(emin, slc_i[5])
		intmax = np.append(emax, slc_i[6])

		# Average optical depth in the Rosseland mean, 
		#in the Rosseland mean at 500nm, in the Planck mean
		ltaur, ltaup, ltau5 = self.csnapsh.get_ltau_all()
		ltaur = np.mean(np.mean(ltaur, axis = 0), axis = 1)
		ltau5 = np.mean(np.mean(ltau5, axis = 0), axis = 1)
		ltaup = np.mean(np.mean(ltaup, axis = 0), axis = 1)
		
		self.wdset_arr_h5('ux', ux[a:b], 'thavn')
		self.wdset_arr_h5('uxu', uxu[a:b], 'thavn')
		self.wdset_arr_h5('uxd', uxd[a:b], 'thavn')
		self.wdset_arr_h5('uxs', uxs[a:b], 'thavn')
		self.wdset_arr_h5('uxrms', uxrms[a:b], 'thavn')
		self.wdset_arr_h5('uxmin', uxmin[a:b], 'thavn')
		self.wdset_arr_h5('uxmax', uxmax[a:b], 'thavn')
		self.wdset_arr_h5('uxurms', uxurms[a:b], 'thavn')
		self.wdset_arr_h5('uxdrms', uxdrms[a:b], 'thavn')
		self.wdset_arr_h5('uy', uy[a:b], 'thavn')
		self.wdset_arr_h5('uyu', uyu[a:b], 'thavn')
		self.wdset_arr_h5('uyd', uyd[a:b], 'thavn')
		self.wdset_arr_h5('uys', uys[a:b], 'thavn')
		self.wdset_arr_h5('uyrms', uyrms[a:b], 'thavn')
		self.wdset_arr_h5('uymin', uymin[a:b], 'thavn')
		self.wdset_arr_h5('uymax', uymax[a:b], 'thavn')
		self.wdset_arr_h5('uyurms', uyurms[a:b], 'thavn')
		self.wdset_arr_h5('uydrms', uydrms[a:b], 'thavn')
		self.wdset_arr_h5('uz', uz[a:b], 'thavn')
		self.wdset_arr_h5('uzu', uzu[a:b], 'thavn')
		self.wdset_arr_h5('uzd', uzd[a:b], 'thavn')
		self.wdset_arr_h5('uzs', uzs[a:b], 'thavn')
		self.wdset_arr_h5('uzrms', uzrms[a:b], 'thavn')
		self.wdset_arr_h5('uzmin', uzmin[a:b], 'thavn')
		self.wdset_arr_h5('uzmax', uzmax[a:b], 'thavn')
		self.wdset_arr_h5('uzurms', uzurms[a:b], 'thavn')
		self.wdset_arr_h5('uzdrms', uzdrms[a:b], 'thavn')
		self.wdset_arr_h5('u', u[a:b], 'thavn')
		self.wdset_arr_h5('uu', uu[a:b], 'thavn')
		self.wdset_arr_h5('ud', ud[a:b], 'thavn')
		self.wdset_arr_h5('urms', urms[a:b], 'thavn')
		self.wdset_arr_h5('umin', umin[a:b], 'thavn')
		self.wdset_arr_h5('umax', umax[a:b], 'thavn')
		self.wdset_arr_h5('uurms', uurms[a:b], 'thavn')
		self.wdset_arr_h5('udrms', udrms[a:b], 'thavn')
		self.wdset_arr_h5('uh', uh[a:b], 'thavn')
		self.wdset_arr_h5('uhu', uhu[a:b], 'thavn')
		self.wdset_arr_h5('uhd', uhd[a:b], 'thavn')
		self.wdset_arr_h5('uhs', uhs[a:b], 'thavn')
		self.wdset_arr_h5('uhrms', uhrms[a:b], 'thavn')
		self.wdset_arr_h5('uhmin', uhmin[a:b], 'thavn')
		self.wdset_arr_h5('uhmax', uhmax[a:b], 'thavn')
		self.wdset_arr_h5('uhurms', uhurms[a:b], 'thavn')
		self.wdset_arr_h5('uhdrms', uhdrms[a:b], 'thavn')
		self.wdset_arr_h5('rho', rho[a:b], 'thavn')
		self.wdset_arr_h5('rhou', rhou[a:b], 'thavn')
		self.wdset_arr_h5('rhod', rhod[a:b], 'thavn')
		self.wdset_arr_h5('rhos', rhos[a:b], 'thavn')
		self.wdset_arr_h5('rhorms', rhorms[a:b], 'thavn')
		self.wdset_arr_h5('rhomin', rhomin[a:b], 'thavn')
		self.wdset_arr_h5('rhomax', rhomax[a:b], 'thavn')
		self.wdset_arr_h5('rholgm', rholgm[a:b], 'thavn')
		self.wdset_arr_h5('tt', tt[a:b], 'thavn')
		self.wdset_arr_h5('ttu', ttu[a:b], 'thavn')
		self.wdset_arr_h5('ttd', ttd[a:b], 'thavn')
		self.wdset_arr_h5('tts', tts[a:b], 'thavn')
		self.wdset_arr_h5('ttrms', ttrms[a:b], 'thavn')
		self.wdset_arr_h5('ttmin', ttmin[a:b], 'thavn')
		self.wdset_arr_h5('ttmax', ttmax[a:b], 'thavn')
		self.wdset_arr_h5('ttlgm', ttlgm[a:b], 'thavn')
		self.wdset_arr_h5('tt4', tt4[a:b], 'thavn')
		self.wdset_arr_h5('e', e[a:b], 'thavn')
		self.wdset_arr_h5('eu', eu[a:b], 'thavn')
		self.wdset_arr_h5('ed', es[a:b], 'thavn')
		self.wdset_arr_h5('erms', erms[a:b], 'thavn')
		self.wdset_arr_h5('emin', emin[a:b], 'thavn')
		self.wdset_arr_h5('emax', emax[a:b], 'thavn')
		self.wdset_arr_h5('elgm', elgm[a:b], 'thavn')
		self.wdset_arr_h5('ee', ee[a:b], 'thavn')
		self.wdset_arr_h5('eeu', eeu[a:b], 'thavn')
		self.wdset_arr_h5('eed', ees[a:b], 'thavn')
		self.wdset_arr_h5('eerms', eerms[a:b], 'thavn')
		self.wdset_arr_h5('eemin', eemin[a:b], 'thavn')
		self.wdset_arr_h5('eemax', eemax[a:b], 'thavn')
		self.wdset_arr_h5('eelgm', eelgm[a:b], 'thavn')
		self.wdset_arr_h5('int', Int[a:b], 'thavn')
		self.wdset_arr_h5('intu', intu[a:b], 'thavn')
		self.wdset_arr_h5('intd', ints[a:b], 'thavn')
		self.wdset_arr_h5('intrms', intrms[a:b], 'thavn')
		self.wdset_arr_h5('intmin', intmin[a:b], 'thavn')
		self.wdset_arr_h5('intmax', intmax[a:b], 'thavn')
		self.wdset_arr_h5('fmass', fmass[a:b], 'thavn')
		self.wdset_arr_h5('fmassu', fmassu[a:b], 'thavn')
		self.wdset_arr_h5('fmassd', fmassd[a:b], 'thavn')
		self.wdset_arr_h5('fmasss', fmasss[a:b], 'thavn')
		self.wdset_arr_h5('fmassrms', fmassrms[a:b], 'thavn')
		self.wdset_arr_h5('fmassmin', fmassmin[a:b], 'thavn')
		self.wdset_arr_h5('fmassmax', fmassmax[a:b], 'thavn')
		self.wdset_arr_h5('fmassurms', fmassurms[a:b], 'thavn')
		self.wdset_arr_h5('fmassdrms', fmassdrms[a:b], 'thavn')
		#mmlu = 1./np.abs((np.log(np.abs(np.roll(fmassu, -1))) - np.log(np.abs(fmassu))) * self.csnapsh.dyidydn)
		#mmld = 1./np.abs((np.log(np.roll(fmassd, -1)) - np.log(fmassd)) * self.csnapsh.dyidydn)
		self.wdset_arr_h5('mmlu', mmlu[a:b], 'thavn')
		self.wdset_arr_h5('mmld', mmld[a:b], 'thavn')
		self.wdset_arr_h5('pturb', pturb[a:b], 'thavn')
		self.wdset_arr_h5('pturbu', pturbu[a:b], 'thavn')
		self.wdset_arr_h5('pturbd', pturbs[a:b], 'thavn')
		self.wdset_arr_h5('pturbrms', pturbrms[a:b], 'thavn')
		self.wdset_arr_h5('pturbmin', pturbmin[a:b], 'thavn')
		self.wdset_arr_h5('pturbmax', pturbmax[a:b], 'thavn')
		self.wdset_arr_h5('pturblgm', pturblgm[a:b], 'thavn')
		self.wdset_arr_h5('ptot', ptot[a:b], 'thavn')
		self.wdset_arr_h5('ptotu', ptotu[a:b], 'thavn')
		self.wdset_arr_h5('ptotd', ptots[a:b], 'thavn')
		self.wdset_arr_h5('ptotrms', ptotrms[a:b], 'thavn')
		self.wdset_arr_h5('ptotmin', ptotmin[a:b], 'thavn')
		self.wdset_arr_h5('ptotmax', ptotmax[a:b], 'thavn')
		self.wdset_arr_h5('ptotlgm', ptotlgm[a:b], 'thavn')
		self.wdset_arr_h5('nel', nel[a:b], 'thavn')
		self.wdset_arr_h5('nelu', nelu[a:b], 'thavn')
		self.wdset_arr_h5('neld', nels[a:b], 'thavn')
		self.wdset_arr_h5('nelrms', nelrms[a:b], 'thavn')
		self.wdset_arr_h5('nelmin', nelmin[a:b], 'thavn')
		self.wdset_arr_h5('nelmax', nelmax[a:b], 'thavn')
		self.wdset_arr_h5('nellgm', nellgm[a:b], 'thavn')
		self.wdset_arr_h5('kapr', np.log(kapr[a:b]), 'thavn')
		self.wdset_arr_h5('kap5', np.log(kap5[a:b]), 'thavn')
		self.wdset_arr_h5('kapp', np.log(kapp[a:b]), 'thavn')
		self.wdset_arr_h5('ltaur', ltaur[a:b], 'thavn')
		self.wdset_arr_h5('ltau5', ltau5[a:b], 'thavn')
		self.wdset_arr_h5('ltaup', ltaup[a:b], 'thavn')

		del ux; del uxu; del uxd; del uxs; del uxrms
		del uxmin; del uxmax; del uxurms; del uxdrms
		del uy; del uyu; del uyd; del uys; del uyrms
		del uymin; del uymax; del uyurms; del uydrms
		del uz; del uzu; del uzd; del uzs; del uzrms
		del uzmin; del uzmax; del uzurms; del uzdrms
		del u; del uu; del ud; del us; del urms
		del umin; del umax; del uurms; del udrms
		del uh; del uhu; del uhd; del uhs; del uhrms
		del uhmin; del uhmax; del uhurms; del uhdrms
		del rho; del rhou; del rhod; del rhos; del rhorms
		del rhomin; del rhomax; del rholgm
		del tt; del ttu; del ttd; del tts; del ttrms
		del ttmin; del ttmax; del ttlgm; del tt4
		del e; del eu; del ed; del es; del erms
		del emin; del emax; del elgm
		del ee; del eeu; del eed; del ees; del eerms
		del eemin; del eemax; del eelgm
		del Int; del intu; del intd; del ints; del intrms
		del intmin; del intmax
		del fmass; del fmassu; del fmassd; del fmasss; del fmassrms
		del fmassmin; del fmassmax; del fmassurms; del fmassdrms
		del mmlu; del mmld
		del pturb; del pturbu; del pturbd; del pturbs; del pturbrms
		del pturbmin; del pturbmax; del pturblgm
		del ltaur; del ltau5; del ltaup; del kapr; del kap5; del kapp
		del ptot; del ptotu; del ptotd; del ptots; del ptotrms
		del ptotmin; del ptotmax; del ptotlgm
		del nel; del nelu; del neld; del nels; del nelrms
		del nelmin; del nelmax; del nellgm

		del UX; del UY; del UZ; del U; del PTURB; del UH; del PTOT; del NEL; 
		del KHIR_R; del KHIR5_R; del KHIP_R; del TT4

		self.hdf5_file.close()