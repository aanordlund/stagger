#!/anaconda/bin/python
# coding: utf-8

###################################################################
##    AUTHOR: Louis Manchon                                      ##
##    ORGN: Århus University, Stellar Astrophysic Center         ##
##    FILE: CSnapsh.py                                           ##
##    DATE: June 2016                                            ##
###################################################################

from const import *
from CEOStab import *
import numpy as np
from bisect import *
from scipy import interpolate
import math, h5py, os, glob

__metaclass__ = type

class CSnapsh(CEOStab):
	"""
	Class for post processing of a snapshot.
	"""
	def __init__(self, file, model, path = None, read = True, erase = False, v = '05', dat = 'dat'):
		"""
		``CSnapsh.__init__(file, model, path = None, read = True, erase = False, v = '05', dat = 'dat')``

		Parameters
		----------
		file : str
			Name of the snapshot (Without the ``.dat`` extension).
		model : str
			Name of the model: ``txxgyymzz``.
		path : str, optional
			Path to the folder containing the snapshot. It is advisable to set it explicitly, 
			unless your already in the right directory.
		read : bool, optional
			If ``False``, doesn't read the files.
		erase : bool, optional
			If ``True``, erase the previously existing ``.hdf5`` file.
		v : str, optional
			Version.
		dat : str, optional
			Snapshot to be read.

		Raise
		-----
		IOError
			If ``.dat`` or ``.src`` and ``.msh`` files are not in the directory.
		ValueError
			If the ``dat`` parameter is not equal to ``'dat'`` or ``'src'``
	
		Examples
    	--------
    	>>> from staggerpy import CSnapsh
    	>>> cd stagger_grid/t45g25p05
    	>>> mdl = CSnapsh('t45g25p0505_00000', 't45g25p05', read = True)
    		Reading EOSrhoe_AGSS09+0.50.tab ... [Done]
		Reading t45g25p0505_00000.hav ... [Done]
		Reading t45g25p0505.dx ... [Done]
		Reading t45g25p0505.chk ... [Done]
		Reading t45g25p0505.msh ... [Done]
		Reading t45g25p0505_00000.dat ... [Done]
		"""
		# Attributes given when creating the class
		self.model = model
		self.snapshot = file
		# Guess the path. It is advisable to set it explicitly.
		if path == None:
			self.path = os.getcwd()
		else:
			self.path = path
		self.version = v
		# Create some file names. We will test their presence after
		self.dat_file = file + '.' + dat # raw data (Direct-Output Binary File)
		self.hav_file = file + ".hav" # ASCII file
		self.msh_file = self.model + self.version + '.msh' # mesh data (Unformatded 'F77' Binary File)
		self.h5_file = self.snapshot+'.hdf5' #HDF5 file
		# Test if the dx file is here.
		# This list contains the name of all the dx files contained in the directory, sorted in alphabetical order.
		l = sorted(glob.glob(self.model + '*.dx'))
		self.dx_file = self.model + self.version + '.dx'# ASCII file
		if self.dx_file in l : # if the dx file is indeed in the list
			self.dx_exists = True
		elif l: # if the dx file is not here but some dx files yes, 
			#we re taking the one just before the index where the good 
			#version on the dx file should be in the sorted list.
			self.dx_file = l[bisect_right(l, self.dx_file)-1]
			self.dx_exists = True #in fact, it's an earlier version
		else: #if no dx files at all, print just a Warning. Certain values may be set by default
			print "IOWarning: %s does not exist in %s." % (self.dx_file, self.path)
			self.dx_exists = False
		# Test if the chk file is here
		# this list contains the name of all the chk files contained in the directory, sorted in alphabetical order
		l = sorted(glob.glob(self.model + '*.chk'))
		self.chk_file = self.model + self.version + '.chk'# ASCII file
		if self.chk_file in l : # if the chk file is indeed in the list
			self.chk_exists = True
		elif l:# if the chk file is not here but some chk files yes, 
			# we re taking the one just before the index where the good 
			# version on the chk file should be in the sorted list.
			self.chk_file = l[bisect_right(l, self.chk_file)-1]
			self.chk_exists = True #in fact, it's an earlier version
		else: # if no chk files at all, print just a Warning. Certain values may be set by default
			print "IOWarning: %s does not exist in %s." % (self.chk_file, self.path)
			self.chk_exists = False
		if read :
			if not os.path.exists(self.dat_file): # If dat file is not present
				raise IOError("%s does not exist in %s." % (self.dat_file, self.path))

			if not os.path.exists(self.msh_file):  # If msh file is not present, 
				# we try to find a msh file without the version extension
				self.msh_file = self.model + '.msh'
				if not os.path.exists(self.msh_file):
					raise IOError("%s does not exist in %s." % (self.msh_file, self.path))
		# By default, we set nsnap equal -42, It could be changed after for a real value
		self.nsnap = -42
		# CSnapsh can work on a dat file or a scratch file
		if dat == 'dat':
			self.isnap = int(self.dat_file[-9:-4])
		elif dat == 'src':
			self.isnap = -1
		else :
			raise ValueError("dat should take the value 'dat' or 'src'.")
		# For the moment, dx, chk, msh, hav files hve not been read.
		self.rd_dx = True	#We need to read dx
		self.rd_chk = True	#We need to read chk
		self.rd_msh = True	#We need to read msh
		self.rd_hav = True	#We need to read msh
		# This two lists will contains the indices of the cells that contains a upflow and those that contains a downflow
		self.upfl = []
		self.dwfl = []
		if read :
			self.ceostab = CEOStab()
			self.tab_file = self.ceostab.tab_file
			if erase:
				# opening of the hdf5 file and deletion of the previous version if it exists
				self.hdf5_file = h5py.File(self.h5_file, 'w')
				# creation of 4 data group
				self.wgrp_h5('hav')
				self.wgrp_h5('thavn')
				self.wgrp_h5('rhavn')
				self.wgrp_h5('t5havn')
			else: 
				# opening of the hdf5 file
				self.hdf5_file = h5py.File(self.h5_file, 'a')

			try:
				# Try to read hav file and by the way test if it is present
				self.read_hav()
			except:
				print "No %s file existing. self.nghost has been set to 5." % self.hav_file
				self.nghost = 5

			# Then, read all the other files
			#if dx or chk are not present, this step is skiped
			self.read_dx()
			self.read_chk()
			self.read_msh()
			self.read_dat()
		else : 
			#Just opening operation but no reading.
			if erase:
				self.hdf5_file = h5py.File(self.h5_file, 'w')
				self.wgrp_h5('hav')
				self.wgrp_h5('thavn')
				self.wgrp_h5('rhavn')
				self.wgrp_h5('t5havn')
			else: 
				self.hdf5_file = h5py.File(self.h5_file, 'a')

		# conversion factors
		self.uur = 1.e-7 # density
		self.uul = 1.e8 # distance
		self.uut = 1.e2 # time
		self.uuv = 1.e6 # velocity: uul/uut 
		self.uue = 1.e12 # energy: uuv^2
		self.uua = 1.e-8 # 1/uul

	def __len__(self):
		# If the file is a all* file, return the number of snapshots stored in the files plus mean*, tmin and tmax.
		# If it is a single snapshot, return the number of variables.
		#read dx file or mdh file
		if self.dx_exists:
			if self.rd_dx:
				self.read_dx()
		else : 
			if self.rd_msh:
				self.read_msh()
		return (self.nxm, self.nym, self.nzm)

	def __getitem__(self, item):
		"""
			Return the item stored in ``.hdf5`` file.
	
			Parameter
			---------
			item : str
			    path in the ``.hdf5`` file.
	
			Examples
    		--------
    		>>> mdl.open_h5(op_mode = 'r')
    		>>> mdl['hav/name']
    			't45g25p05'
		"""
		if len(self.hdf5_file[item]) == 1:
			# In this case, the array contain only one value
			return self.hdf5_file[item].value[0]
		else : 
			return self.hdf5_file[item].value

	def open_h5(self, op_mode = 'a'):
		"""
			Open the associated ``.hdf5`` file.
	
			Parameter
			---------
			op_mode : str
				``Opening mode.``
				``r			Readonly, file must exist``
				``r+			Read/write, file must exist``
				``w			Create file, truncate if exists``
				``w- or x		Create file, fail if exists``
				``a			Read/write if exists, create otherwise (default)``
	
			See also
			--------
			CSnapsh.close_h5 : Close the associate ``.hdf5`` file.
			
		"""
		try:
			self.hdf5_file = h5py.File(self.h5_file, op_mode)
		except:
			pass

	def close_h5(self):
		"""
			Close the ``.hdf5`` file
	
			See also
			--------
			CSnapsh.open_h5 : Open the associate ``.hdf5`` file.
	
		"""
		try:
			self.hdf5_file.close()
		except:
			pass

	def read(self, endian = '>'):
		"""
			Read all the usefull files:
	
				* ``.hav`` file (and write the header in ``.hdf5``)
				* ``.dx`` file
				* ``.chk`` file
				* ``.msh`` file
				* ``.dat`` file
	
			Parameter
			---------
			endian : str
				Endianness.
	
			See also
			--------
			CSnapsh.read_eos : Read ``.eos`` file.
			read_dx : Read ``.dx`` file.
			read_chk : Read ``.chk`` file.
			read_hav : Read ``.hav`` file.
			read_msh : Read ``.msh`` file.
			read_dat : Read ``.dat`` file.
			read_dat_cube : Read one cube from ``.dat`` file.
	
		"""
		self.read_hav()
		self.read_dx()
		self.read_chk()
		self.read_msh(endian = endian)
		self.read_dat(endian = endian)

	def read_eos(self):
		"""
			Read ``.eos`` file.
		"""
		# Create a CEOStab instance
		self.ceostab = CEOStab()
		# Read eostab
		self.ceostab.read_eos()
		# Create an alias of eostab.tab_file in self.tab_file
		self.tab_file = self.ceostab.tab_file

	def read_dx(self):
		"""
			Read ``.dx`` file.
		"""
		if self.dx_exists: # If dx has not been read
			print "Reading %s ..." % self.dx_file,
			# store the data in self.data_dx as strings
			self.data_dx=[]
			data=open(self.dx_file, 'r')
			line=data.readline()
			while line :
				self.data_dx.append(line.split())
				line=data.readline()
			data.close()
			self.rd_dx = False #No need to read dx again
			self.nxm, self.nym, self.nzm = map(int, self.data_dx[0])
			print "%s[Done]%s" % (GREEN, NO_COLOR)


	def read_chk(self):
		"""
			Read ``.chk`` file.
		"""
		self.hdf5_file = h5py.File(self.h5_file, 'a')
		if self.chk_exists: # If chk has not been read
			print "Reading %s ..." % self.chk_file,
			# store the data in self.data_chk as strings
			self.data_chk=[]
			data=open(self.chk_file, 'r')
			line=data.readline()
			while line :
				self.data_chk.append(line.split())
				line=data.readline()
			data.close()
			self.rd_chk = False #No need to read chk again
			#store nsnap in the hdf5 file
			try:
				self.wdset_all_h5('nsnap', self.nsnap, ddtype = 'i2', dset = 'val')
			except:
				pass
		self.hdf5_file.close()
		print "%s[Done]%s" % (GREEN, NO_COLOR)

	def read_hav(self):
		"""
			Read ``.hav`` file and write the header in the ``.hdf5`` file.
		"""
		print "Reading %s ..." % self.hav_file,
		f = open(self.hav_file,'r') 
		line = f.readline()
		line = f.readline()
		line = f.readline()
		while line != '/\n':
			words = line.split()
			if self.test_int(words[2][:-1]): #then it's an int
				self.wdset_all_h5(words[0], int(words[2][:-1]), ddtype = 'i4', dset = 'val')
			else:
				self.wdset_all_h5(words[0], float(words[2][:-1]), dset = 'val')
			line = f.readline()
		try :
			self.nghost = self.hdf5_file['hav/nghost'][0]
		except : 
			self.nghost = 5
			print "IOWarning: self.nghost has been set to 5.",
		f.close()
		self.hdf5_file.close()
		print "%s[Done]%s" % (GREEN, NO_COLOR)

	def read_msh(self, endian = '>'):
		"""
			Read ``.msh`` file

			Parameter
			---------
			endian : str
				Endianness.

			Raise
			----- 
			IOError 
				If value of ``self.nxm``, ``self.nym`` and ``self.nzm`` from ``.dx`` file differ from those from ``.msh`` file.
		"""
		if self.rd_dx:
			self.read_dx()

		print "Reading %s ..." % self.msh_file,
		f = open(self.msh_file,'rb') 

		u = np.fromfile(f, dtype = endian + 'u4', count = 1)
		if self.dx_exists:
			np.fromfile(f, dtype = endian + 'i4', count = 3)
		else:
			# If dx has not been read, dimensions are read from msh file.
			self.nxm, self.nym, self.nzm = np.fromfile(f, dtype = endian + 'i4', count = 3) # sizes
		u = np.fromfile(f, dtype = endian + 'u4', count = 1)

		u = np.fromfile(f, dtype = endian + 'u4', count = 1)
		self.dxm = np.fromfile(f, dtype = endian + 'f4', count = self.nxm) 
		self.dxmdn = np.fromfile(f, dtype = endian + 'f4', count = self.nxm)
		self.xm = np.fromfile(f, dtype = endian + 'f4', count = self.nxm)
		self.xmdn = np.fromfile(f, dtype = endian + 'f4', count = self.nxm)
		self.dxidxup = np.fromfile(f, dtype = endian + 'f4', count = self.nxm) #dx/di half a cell down
		self.dxidxdn = np.fromfile(f, dtype = endian + 'f4', count = self.nxm) #dx/di half a cell up
		u = np.fromfile(f, dtype = endian + 'u4', count = 1)

		u = np.fromfile(f, dtype = endian + 'u4', count = 1)
		self.dym = np.fromfile(f, dtype = endian + 'f4', count = self.nym)
		self.dymdn = np.fromfile(f, dtype = endian + 'f4', count = self.nym)
		self.ym = np.fromfile(f, dtype = endian + 'f4', count = self.nym)
		self.ymdn = np.fromfile(f, dtype = endian + 'f4', count = self.nym)
		self.dyidyup = np.fromfile(f, dtype = endian + 'f4', count = self.nym)
		self.dyidydn = np.fromfile(f, dtype = endian + 'f4', count = self.nym)
		u = np.fromfile(f, dtype = endian + 'u4', count = 1)

		u = np.fromfile(f, dtype = endian + 'u4', count = 1)
		self.dzm = np.fromfile(f, dtype = endian + 'f4', count = self.nzm)
		self.dzmdn = np.fromfile(f, dtype = endian + 'f4', count = self.nzm)
		self.zm = np.fromfile(f, dtype = endian + 'f4', count = self.nzm)
		self.zmdn = np.fromfile(f, dtype = endian + 'f4', count = self.nzm)
		self.dzidzup = np.fromfile(f, dtype = endian + 'f4', count = self.nzm)
		self.dzidzdn = np.fromfile(f, dtype = endian + 'f4', count = self.nzm)
		u = np.fromfile(f, dtype = endian + 'u4', count = 1)

		f.close()
		self.rd_msh = False #No need to read msh again
		# Wright some of the data in hdf5 file
		self.hdf5_file = h5py.File(self.h5_file, 'a')
		self.wdset_arr_h5('depth', self.ym[self.nghost:self.nym-self.nghost], 'hav', ddtype = 'f4')
		self.wdset_all_h5('nx', self.nxm, ddtype = 'i4', dset = 'val')
		self.wdset_all_h5('ny', self.nym, ddtype = 'i4', dset = 'val')
		self.wdset_all_h5('nz', self.nzm, ddtype = 'i4', dset = 'val')
		#Number of cells in a horizontal slice.
		self.nhm = self.nxm * self.nzm
		self.wdset_all_h5('nh', self.nhm, ddtype = 'i4', dset = 'val')
		self.hdf5_file.close()
		print "%s[Done]%s" % (GREEN, NO_COLOR)

	def read_dat(self, endian = '>'):
		"""
			Read ``.dat`` file.
			Create attributes ``self.rho``, ``self.px``, ``self.py``, ``self.pz``, ``self.e``, ``self.tt``.
			
			Parameter
			---------
			endian : str
				Endianness. Default: little endian: endian = '>'.
		"""
		print "Reading %s ..." % self.dat_file,

		# Number of cells in the cube
		k = self.nxm*self.nym*self.nzm
		f = open(self.dat_file, 'rb')
		# density
		self.rho = np.reshape(np.fromfile(f, dtype = endian + 'f', count = k), (self.nzm, self.nym, self.nxm))
		# staggered x momentum
		self.px = np.reshape(np.fromfile(f, dtype = endian + 'f', count = k), (self.nzm, self.nym, self.nxm))
		# staggered z momentum
		self.py = np.reshape(np.fromfile(f, dtype = endian + 'f', count = k), (self.nzm, self.nym, self.nxm))
		# staggered y momentum
		self.pz = np.reshape(np.fromfile(f, dtype = endian + 'f', count = k), (self.nzm, self.nym, self.nxm))
		# internal energy per unit volume
		self.e = np.reshape(np.fromfile(f, dtype = endian + 'f', count = k), (self.nzm, self.nym, self.nxm))
		# temperature
		self.tt = np.reshape(np.fromfile(f, dtype = endian + 'f', count = k), (self.nzm, self.nym, self.nxm))
		#New data cube containing the internal energy per unit mass
		self.ee = np.divide(self.e, self.rho)
		f.close()
		print "%s[Done]%s" % (GREEN, NO_COLOR)


	def read_dat_cube(self, var, endian = '>'):
		"""
			Read only one data cube from ``.dat`` file.
	
			Parameter
			---------
			var : str
				Name of the data you want.
			endian : str
				Endianness.
	
			Raise
			-----
			KeyError
				If ``var`` does not match any of the key of ``var`` (dictionnary stored in ``const.py``).
		"""
		if self.rd_dx:
			self.read_dx()
		if self.rd_chk:
			self.read_chk()
		if self.rd_msh:
			self.read_msh()

		print "Reading %s from %s ..." % (var, self.dat_file),
		try : 
			index = raw_var[var]
		except KeyError: 
			raise KeyError("the argument 'var' of read_dat_cube(var) must take the value: 'rho', 'px, 'py', 'pz', 'ee' or 'tt'.")
		k = self.nxm * self.nym * self.nzm
		f = open(self.dat_file, 'rb')
		# Point to the (4 * k * index) byte
		f.seek(4 * k * index)
		dat = np.reshape(np.fromfile(f, dtype = endian + 'f', count = k), (self.nzm, self.nym, self.nxm))

		if var == 'rho':
			self.rho = dat
		elif var == 'px':
			self.px = dat
		elif var == 'py':
			self.py = dat
		elif var == 'pz':
			self.pz = dat
		elif var == 'e':
			self.e = dat
		elif var == 'tt':
			self.tt = dat
		f.close()
		print "%s[Done]%s" % (GREEN, NO_COLOR)

	def sl_stat(self, slc, ind, intensive = True, lgm = True, ud_rms = False, store_indices = False):
		"""
		Compute some statistical quantities such as mean value, rms, standard deviation, 
		min, max, and make the difference between up and down flow.
	
		Parameter
		---------
		slc : 2d np.array
			Horizontal slice at constant y (``cube[:, i, :]``).
		ind : int
			Index of the horizontal slice.
		intensive : bool
			If intensive physical quantity, set to ``True``, otherwise, ``False``. 
			The mean value is not computed the same way.
		lgm : bool
			If ``True``, compute logarithmic mean, defined as follow: exp(<log(array)>).
		ud_rms : bool
			If ``True``, compute the rms of the upflow and of the downflow.
		store_indices : bool
			If ``True``, store the indices of the upflow and the downflow.
			Should be set to True only if the cube is the vertical velocity (or if you have a 
			good reason to so).

		Raise
		-----
		AttributeError
			If up and down flow indices has not been previously stored.
		"""
		# This list will store the following statistical quantities
		res = []
		# Those lists will store the the values for the downflow and for the upflow
		dw = []
		up = []
		if len(self.upfl) > self.nym:
			# This lists will contain the indices of the downflow and upflow cells
			self.dwfl = []
			self.upfl = []
		if store_indices: # this section should be used only for the vertical velocity or vertical momentum 
			d = np.where(slc >= 0.)
			u = np.where(slc < 0.)
			self.dwfl.append((d[0].astype(np.uint16), d[1].astype(np.uint16)))
			self.upfl.append((u[0].astype(np.uint16), u[1].astype(np.uint16)))
		try :# We store the values in the two different lists
			dw = slc[self.dwfl[ind]]
			up = slc[self.upfl[ind]]
		except :
			raise AttributeError("self.dwfl and self.upfl may not exist. Please store them in memory by calling \n \
				sl_stat() with the vertical velocity data cube and the option store_indices = True.")

		# Mean value of the slice
		#np.mean(np.mean(mdl.tt, axis = 0), axis = 1)
		res.append(np.mean(slc))
		# Mean values for the upflow and for the downflow
		if intensive: # In case of intensive value, we use a regular averaging
			res.append(np.mean(up))
			res.append(np.mean(dw))
		else : 
			# In case of an extensive value, we must divide by the total number of cell in the slice
			res.append(np.sum(up)/float(self.nhm))
			res.append(np.sum(dw)/float(self.nhm))
		# Standard deviation, root mean square, minimum, maximum
		res.append(np.std(slc, ddof = 1))
		res.append(np.sqrt(np.mean(np.square(slc))))
		res.append(np.amin(slc))
		res.append(np.amax(slc))
		if lgm :# logarithmic mean if needed
			res.append(np.exp(np.mean(np.log(slc))))
		if ud_rms :# Root mean square for upflow and downflow id needed
			res.append(np.sqrt(np.mean(np.square(up))))
			res.append(np.sqrt(np.mean(np.square(dw))))
		return res


	def xdn(self, cube):
		"""
			Return ``cube[i - 0.5, j, k]``.
		"""
		c = 3./256.
		b = -25./256.
		a = 75./128.
		return a*(cube+np.roll(cube, 1, axis = 0))+ \
		b*(np.roll(cube, -1, axis = 0)+np.roll(cube, 2, axis = 0))+ \
		c*(np.roll(cube, -2, axis = 0)+np.roll(cube, 3, axis = 0))

	def ydn(self, cube):
		"""
			Return ``cube[i, j - 0.5, k]``.
		"""
		c = 3./256.
		b = -25./256.
		a = 75./128.
		return a*(cube+np.roll(cube, 1, axis = 1))+ \
		b*(np.roll(cube, -1, axis = 1)+np.roll(cube, 2, axis = 1))+ \
		c*(np.roll(cube, -2, axis = 1)+np.roll(cube, 3, axis = 1))

	def zdn(self, cube):
		"""
			Return ``cube[i, j, k - 0.5]``
		"""
		c = 3./256.
		b = -25./256.
		a = 75./128.
		return a*(cube+np.roll(cube, 1, axis = 2))+ \
		b*(np.roll(cube, -1, axis = 2)+np.roll(cube, 2, axis = 2))+ \
		c*(np.roll(cube, -2, axis = 2)+np.roll(cube, 3, axis = 2))

	def xup(self, cube):
		"""
			Return ``cube[i + 0.5, j, k]``.
		"""
		c = 3./256.
		b = -25./256.
		a = 75./128.
		return a*(cube+np.roll(cube, -1, axis = 0))+ \
		b*(np.roll(cube, 1, axis = 0)+np.roll(cube, -2, axis = 0))+ \
		c*(np.roll(cube, 2, axis = 0)+np.roll(cube, -3, axis = 0))

	def yup(self, cube):
		"""
			Return ``cube[i, j + 0.5, k]``.
		"""
		c = 3./256.
		b = -25./256.
		a = 75./128.
		return a*(cube+np.roll(cube, -1, axis = 1))+ \
		b*(np.roll(cube, 1, axis = 1)+np.roll(cube, -2, axis = 1))+ \
		c*(np.roll(cube, 2, axis = 1)+np.roll(cube, -3, axis = 1))

	def zup(self, cube):
		"""
			Return ``cube[i, j, k + 0.5]``.
		"""
		c = 3./256.
		b = -25./256.
		a = 75./128.
		return a*(cube+np.roll(cube, -1, axis = 2))+ \
		b*(np.roll(cube, 1, axis = 2)+np.roll(cube, -2, axis = 2))+ \
		c*(np.roll(cube, 2, axis = 2)+np.roll(cube, -3, axis = 2))

	def get_vel_cube(self, dir):
		"""
			Compute the velocity cube.
	
			Parameter
			---------
			dir : integer
				``0`` for Ux, ``1`` for Uy, ``2`` for Uz.
		"""
		if dir == 0:
			return np.divide(self.px, self.xdn(self.rho))
		elif dir == 1:
			return np.divide(self.py, self.ydn(self.rho))
		elif dir == 2:
			return np.divide(self.pz, self.zdn(self.rho))
		else : 
			raise ValueError("dir must be equal to 0, 1 or 2")

	def get_lnab(self, lnrho, lne, kind = 'r', linear = False): 
		"""
			Compute the Rosseland extinction coefficient.

			Parameters
			----------
			lnrho : np.array
				``log(rho * uur)``.
			lne : np.array
				``log(ee * uue)``.
			kind : str
				- ``'r'``: Rosseland extinction coefficient.
				- ``'p'``: Planck extinction coefficient.
				- ``'r5'``: Rosseland extinction coefficient at 500 nm.
			linear : bool
				If ``True``, linear interpolation, if ``False``, 1st order Taylor developement.

			Returns
			-------
				log(kappa_r)
		"""
		# index of the absorbtion coeff in the opacity table
		index = {'r' : 1, 'p' : -2, 'r5' : -1}
		return self.ceostab.lookup_eos(lnrho, lne, index[kind], 0, linear = linear) - math.log(self.uua)


	def get_ltau(self, lnrho = None, lne = None, kind = 'r', taumin = 1.0e-10, vertical = 1, spline = True):
		"""
		Compute one of the optical depth in the following optical depths: rosseland, planck, rosseland at 500nm.

		Parameters
		----------
		lnrho : np.array
			``log(rho * uur)``.
			If ``None``, compute ``lnrho`` automatically.
		lne : np.array
			``log(ee * uue)``.
			If ``None``, compute ``lne`` automatically.
		kind : st
			- ``'r'``: Rosseland extinction coefficient.
			- ``'p'``: Planck extinction coefficient.
			- ``'r5'``: Rosseland extinction coefficient at 500 nm.
		taumin : float
			Minimal value in the output cube.
		vertical : int
			Dimension containing the vertical values.	
		spline : bool
			If ``False``, linear interpolation, if ``True``, cubic interpolation. 

		Returns
		-------
		log(tau)
		"""
		# Compute lnrho and lne if needed
		if lnrho == None:
			lnrho = np.log(self.rho * self.uur)
		if lne == None:
			lne = np.log(self.ee * self.uue)
		return np.log10(self.ceostab.tau_calc(self.get_lnab(lnrho, lne, kind = kind), self.ym, taumin = taumin, vertical = vertical, spline = spline))

	def get_ltau_all(self, lnrho = None, lne = None, taumin = 1.0e-10, vertical = 1, spline = True):
		"""
			Compute the Rosseland, Planck, Rosseland at 500nm optical depth in the following optical depths.
	
			Parameters
			----------
			lnrho : np.array
				``log(rho * uur)``.
				If ``None``, compute ``lnrho`` automatically.
			lne : np.array
				``log(ee * uue)``.
				If ``None``, compute ``lne`` automatically.
			kind : str
				- ``'r'``: Rosseland extinction coefficient.
				- ``'p'``: Planck extinction coefficient.
				- ``'r5'``: Rosseland extinction coefficient at 500 nm.
			taumin : float
				Minimal value in the output cube.
			vertical : int
				Dimension containing the vertical values.
			spline : bool
				If ``False``, linear interpolation, if ``True``, cubic interpolation. 

			Returns
			-------
				log(tau)
		"""
		# Compute lnrho and lne if needed
		if lnrho == None:
			lnrho = np.log(self.rho * self.uur)
		if lne == None:
			lne = np.log(self.ee * self.uue)
		ltaur = np.log10(self.ceostab.tau_calc(self.get_lnab(lnrho, lne), self.ym, taumin = taumin, vertical = vertical, spline = spline))
		ltaup = np.log10(self.ceostab.tau_calc(self.get_lnab(lnrho, lne, kind = 'p'), self.ym, taumin = taumin, vertical = vertical, spline = spline))
		ltau5 = np.log10(self.ceostab.tau_calc(self.get_lnab(lnrho, lne, kind = 'r5'), self.ym, taumin = taumin, vertical = vertical, spline = spline))
		return ltaur, ltaup, ltau5

	def geom_der_y(self, cube, staggered = False):
		"""
			Returns the derivatives of a cube cell center defined.

			Parameter
			---------
			cube : 3d np.array
			staggered : bool
				``True`` if the cube is defined at the edge of the cells.
		"""
		if staggered: 
			return np.divide(cube - np.roll(cube, 1, axis = 1), self.dyidyup)
		else: 
			return np.divide(cube - np.roll(cube, 1, axis = 1), self.dyidydn)

	def intrp3(self, zt, f, linear = False, staggered = False):
		"""
			Interpolate to a new z-grid. 3D version.
		
			Parameters
			----------
			zt : 1d np.array
				New grid.
			f : 3d np.array
				Old 3d field.
			linear : bool
				``True`` if linear interpolation, ``False`` if spline.
			staggered : bool
				``True`` if the cube is defined at the edge of the cells.

			Returns
			-------
			New 3d field
		"""
		z = self.ym#[self.nghost:self.nym - 1 - self.nghost]
		nz = z.size; nt = zt.size; mz = f.size
		# Get derivatives of f
		if not linear:
			print f.shape
			d = self.geom_der_y(f, staggered = staggered)
		# Find n such that  z(n-1) < zt(k) < z(n)
		n = np.array([1])
		for k in xrange(nt - 1):
			n1 = np.where(z > zt[k])
			if np.count_nonzero(z) > 0:
				n = np.append(n, n1[0])
			else:
				n = np.append([n, nz - 1])
		np.putmask(n[1:], n[1:] > 1, 1)
		nm1 = n - 1
		# Relative distances
		# Set to constant outside
		dz = z[n] - z[nm1]
		print dz.shape
		print nm1.shape
		print nm1
		print zt
		print z
		print (zt - z[nm1]).shape
		pz = (zt - z[nm1]) / dz

		# Make 3D
		s = f.shape; s[2] = nt
		g = np.empty(size = s, dtype = 'f4')
		qz = 1. - pz
		#  Interpolate
		if linear:
			for i in xrange(nt - 1):
				g[:, :, i] = f[:, :, nm1[i]] + pz[i] * (f[:, :, n[i]] - f[:, :, nm1[i]])
		else:
			pzf = pz - (qz * pz) * (qz - pz)
			pzd = - pz * (qz * pz) * dz
			qzd = qz * (qz * pz) * dz
			for i in xrange(nt - 1):
				g[:, :, i] = f[:, :, nm1[i]]+pzf[i] * (f[:, :, n[i]] - f[:, :, nm1[i]]) + qzd[i] * d[:, :, nm1[i]] + pzd[i] * d[:, :, n[i]]
		return g

	#def new_mesh(self, new_depth):

	def intrp3y(self, yt, ff, staggered = False):
		"""
			Interpolate to a new y-grid. Using ``intrp3`` (z-dir)
		
			Parameters
			----------
			yt : 1d np.array
				New grid
			f : 3d np.array
				Old 3d field
			staggered : bool
				``True`` if the cube is defined at the edge of the cells.

			Returns
			-------
			New 3d field
		"""
		f = np.transpose(ff, (2, 0, 1))
		return np.transpose(self.intrp3(yt, f, staggered = staggered), (1, 2, 0))



	def new_ym(self, new_depth, old_cube, kind = 'optical', linear = False, staggered = False):
		"""
			Compute the Rosseland, Planck, Rosseland at 500nm optical depth in the following optical depths.

			Parameters
			----------
			new_depth : 3d np.array
				Cube containing the new depth scale.
			old_cube : 3d np.array
				Cube containing data to be interpolated.
			kind : str
				- ``'optical'``: if the new depth scale is an optical depth scale. ex: Rosseland optical depth.
				- ``'mass'``: if the new depth scale is an mass depth scale. ex: column mass density.
			linear : bool
				If ``True``, linear interpolation, otherwise spline.
			staggered : bool
				``True`` if the ``old_cube`` is defined at the edge of the cells.

			Returns
			-------
			``new_cube`` containing the old cube interpolated over the new depth scale.
		"""
		#New cube that will contain the interpolated values
		new_cube = np.empty((240, 101, 240), dtype = 'f4')

		if kind == 'optical': # If optical depth
			dmin = -5.; dmax = 5.
		elif kind == 'mass':# If column mass density
			dmin = -3.; dmax = 2.
		# Sampling depth points
		depthy = np.linspace(dmin, dmax, 101)
		# linear interpolation or spline interpolation
		if linear:
			for i in xrange(self.nzm):
				for j in xrange(self.nxm):
					# index of the nearest neighbour in the array containing the new depth scale
					# idx = new_depth[i, self.nghost:self.nym - self.nghost, j].searchsorted(depthy)
					idx = new_depth[i, :, j].searchsorted(depthy)
					np.putmask(idx, idx > 100, 100)
					# value stored in the lower neighbour and in the upper neighbour 
					upper = new_depth[i, idx, j]
					lower = new_depth[i, idx - 1, j]
					s = (depthy - lower) / (upper - lower)
					new_cube[i, :, j] = (1 - s) * old_cube[i, idx - 1, j] + s * old_cube[i, idx, j]
		else :
			for i in xrange(self.nxm):
				#a = self.ym[self.nghost:self.nym - 1 - self.nghost]
				#b = depthy[self.nghost:101 - 1 - self.nghost]
				b = depthy
				#c = np.transpose(np.reshape(old_cube[:, self.nghost:self.nym - self.nghost, i], (self.nxm, self.nym - 2 * self.nghost, 1)), (2, 0, 1))
				c = np.transpose(np.reshape(old_cube[:, self.nghost:self.nym - self.nghost, i], (self.nxm, self.nym - 2 * self.nghost, 1)), (2, 0, 1))
				#new_cube[:, self.nghost:101 - 1 - self.nghost, i] = self.intrp3y(b, c)#a, b, c)
				new_cube[:, :, i] = self.intrp3y(b, c, staggered = staggered)#a, b, c)
		return new_cube

	def wgrp_h5(self, group_name):
		"""
			Try to create the group in the ``.hdf5`` file.

			Parameters
			----------
			group_name : str
				Name of the group to be created.
		"""
		try : 
			self.hdf5_file.create_group(group_name)
		except :
			pass

	def wdset_val_h5(self, name, value, group_name, ddtype = 'f4'):
		"""
			Try to write a single value in the ``.hdf5`` file.

			Parameters
			----------
			name : str
				Name of the group to be created.
			value : single value (int or float or str)
			group_name : str	
				Name of the group in which the value should be copied
			dtype : str	
				Data type of the value. You should stick to one endianness otherwise, 
				everything will be messed up. By defult we use big endian.	
		"""
		try : 
			self.hdf5_file[group_name].create_dataset(name, data = [value], dtype = ddtype)
			self.hdf5_file[group_name+'/'+name].attrs['legend'] = var[name][0]
			self.hdf5_file[group_name+'/'+name].attrs['factor'] = var[name][1]
			self.hdf5_file[group_name+'/'+name].attrs['unit'] = var[name][2]
		except:
			pass 

	def wdset_arr_h5(self, name, array, group_name, ddtype = 'f4'):
		"""
			Try to write an array in the ``.hdf5`` file.

			Parameters
			----------
			name : str
				Name of the group to be created.
			array : np.array
			group_name : str	
				Name of the group in which the array should be copied
			dtype : str	
				Data type of the np.array. You should stick to one endianness otherwise, 
				everything will be messed up. By defult we use big endian.
		"""
		try : 
			self.hdf5_file[group_name].create_dataset(name, data = array, dtype = ddtype)
			self.hdf5_file[group_name+'/'+name].attrs['legend'] = var[name][0]
			self.hdf5_file[group_name+'/'+name].attrs['factor'] = var[name][1]
			self.hdf5_file[group_name+'/'+name].attrs['unit'] = var[name][2]
		except: 
			pass

	def wdset_all_h5(self, name, data, ddtype = 'f4', dset = 'arr'):
		"""
			Try to write a data in all the groups.

			Parameters
			----------
			name : str
				Name of the group to be created.
			data : np.array or single value
				Data to be stored.
			group_name : str	
				Name of the group in which the array should be copied.
			dtype : str	
				Data type of the np.array. You should stick to one endianness otherwise, 
				everything will be messed up. By defult we use big endian.
			dset : str
				If ``dset = 'arr'``, call ``self.wdset_arr_h5``, otherwise, call ``self.wdset_val_h5``.
		"""
		if dset == 'arr':
			for i in self.hdf5_file.keys():
				self.wdset_arr_h5(name, data, i, ddtype = ddtype)
		else :
			for i in self.hdf5_file.keys():
				self.wdset_val_h5(name, data, i, ddtype = ddtype)

	def write_h5(self):
		"""
		Call all the writing routines.

			- ``self.read_hav()``
			- ``self.write_hav_h5()``
		"""
		self.read_hav()
		self.write_hav_h5()

	def test_int(self, string):
		"""
		Test if a string stand for an int or a float.
		"""
		try : 
			return len(str(int(string))) == len(string)
		except:
			return False
