#!/anaconda/bin/python
# coding: utf-8

"""
@AUTHOR: Louis Manchon                             
@ORGN: Århus University, Stellar Astrophysic Center
@FILE: CSimu.py                                   
@DATE: July 2016                                   
"""

from const import *
from CSnapsh import *
from CWriteh5 import *
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
from scipy import interpolate
import math, os, glob

__metaclass__ = type

class CSimu(CSnapsh, CWriteh5):
	"""
	Class that post process the raw data of a snapshot. Data are stored in a Direct-Output Binary File ``.dat``.
	"""
	
	def __init__(self, path, sim, erase = False, v = '05', read = False, store = True):
		"""
		Parameters
		----------
		path : str
			Path to the directory in wich the snapshots are stored. It is advisable to set it explicitly, 
			unless your already in the right directory.
		sim : str
			Name of the simulations: ``txxgyymzz``.
		erase : bool
			If ``True``, erase the previously existing ``.hdf5`` files.
		v : str
			Number of the version.
		read : bool
			If ``False``, doesn't read the files.
		"""
		self.path = path
		self.cwpath = os.getcwd()
		self.model = sim
		self.version = v
		self.store = store
		# jump into the simulation directory
		os.chdir(self.path)
		# get the list of snapshots, sorted in alphabetical order, therefore in chronological order
		self.list_snaps = sorted(glob.glob('*.dat'))
		# If the previous list is empty, we try with the list of hav files.
		if len(self.list_snaps) == 0:
			self.list_snaps = sorted(glob.glob('*.hav'))
		if len(self.list_snaps) == 0:
			raise IOError("No snapshots in this directory")
		self.nsnap = len(self.list_snaps)
		#This array will contain boolean. If 1, we should read the snapshot' files, else, nope.
		self.read_snap = np.ones(self.nsnap)
		# Create the class instances for all the snapshots 
		self.list_mdl = [CSnapsh(i[:-4], self.model, erase = erase, v = self.version, read = read) for i in self.list_snaps]
		self.len = len(self.list_snaps)
		# reverse the list because we will read it from the end.
		self.list_mdl.reverse()

	def __getitem__(self, item):
		"""
			Return the item stored in ``.hdf5`` file.
	
			Parameter
			---------
			item : str
			    path in the ``.hdf5`` file.
	
			Examples
    		--------
    		>>> sim.open_all_h5()
    		>>> sim['hav/filename']
    			['t45g25p05_00000', 't45g25p05_00001', 't45g25p05_00002', 
    			..., 
    			't45g25p05_00097', 't45g25p05_00098', 't45g25p05_00099']
		"""
		# Dunder method to extract item from the hdf5 files
		if len(self.hdf5_files[0][item]) == 1: # If single value
			a = np.array([], dtype = type(self.hdf5_files[0][item].value))
			for i in self.hdf5_files:
				a = np.append(a, i[item].value)
			return a.flatten()
		else : #if array
			a = np.array([self.hdf5_files[0][item].value], dtype = type(self.hdf5_files[0][item].value))
			for i in self.hdf5_files[1:]:
				a = np.append(a, i[item].value)
				nghost = self.hdf5_files[0]['hav/nghost'][0]
				nym = self.hdf5_files[0]['hav/ny'][0]
			return np.reshape(a, (len(a) / (nym - 2 * nghost), nym - 2 * nghost))

	def __iter__(self):
		if self.len == 0:
			raise ValueError("The attribute self.list_mdl is empty.")
		return self.list_mdl.__iter__()

	def __len__(self):
		return self.len


	def process_all(self):
		"""
		This method will process all the snapshots and writing the results in an ``.hdf5`` file. 
		"""
		# We make sure we are in the good directory
		os.chdir(self.path)

		# Rank in the self.list_mdl
		j = self.len - 1
		mdl = self.list_mdl.pop()
		mdl.read()
		mdl.read_eos()
		# Create CWriteh5 instance
		cwriteh5 = CWriteh5(mdl)
		cwriteh5.csnapsh.write_hav_h5(store = self.store)
		cwriteh5.csnapsh.write_thavn_h5()
		# We change the value to false in self.read_snap
		self.read_snap[j] = 0
		# We store some varibles in order not to read again the dx, chk, msh and eos files.
		nxm = mdl.nxm; nym = mdl.nym; nzm = mdl.nzm; nhm = mdl.nhm
		dxm = mdl.dxm; dxmdn = mdl.dxmdn; xm = mdl.xm; xmdn = mdl.xmdn; dxidxup = mdl.dxidxup; dxidxdn = mdl.dxidxdn
		dym = mdl.dym; dymdn = mdl.dymdn; ym = mdl.ym; ymdn = mdl.ymdn; dyidyup = mdl.dyidyup; dyidydn = mdl.dyidydn
		dzm = mdl.dzm; dzmdn = mdl.dzmdn; zm = mdl.zm; zmdn = mdl.zmdn; dzidzup = mdl.dzidzup; dzidzdn = mdl.dzidzdn
		nsnap = mdl.nsnap
		eostab = mdl.eostab
		# We delete the file in order to free some memory. This step is almost inevitable, except for a very small simulation
		del mdl
		# Decrease index
		j -= 1
		while self.list_mdl:
			if self.read_snap[j]:
				mdl = self.list_mdl.pop()
				mdl.nxm = nxm; mdl.nym = nym; mdl.nzm = nzm; mdl.nhm = nhm
				mdl.dxm = dxm; mdl.dxmdn = dxmdn; mdl.xm = xm; mdl.xmdn = xmdn; mdl.dxidxup = dxidxup; mdl.dxidxdn = dxidxdn
				mdl.dym = dym; mdl.dymdn = dymdn; mdl.ym = ym; mdl.ymdn = ymdn; mdl.dyidyup = dyidyup; mdl.dyidydn = dyidydn
				mdl.dzm = dzm; mdl.dzmdn = dzmdn; mdl.zm = zm; mdl.zmdn = zmdn; mdl.dzidzup = dzidzup; mdl.dzidzdn = dzidzdn
				mdl.nsnap = nsnap
				mdl.eostab = eostab

				mdl.read_hav()
				mdl.read_dat()
				cwriteh5 = CWriteh5(mdl)
				cwriteh5.csnapsh.write_hav_h5(store = self.store)
				cwriteh5.csnapsh.write_thavn_h5()
				del cwriteh5
				del mdl 
				j -= 1
		# fill again self.list_mdl without reading the files in order to have the instances for zero memory.
		self.list_mdl = [CSnapsh(i[:-4], self.model, erase = False, read = False) for i in self.list_snaps]

	def process_i(self, snaps):
		"""
		This method will process a snapshot and writing the results in an ``.hdf5`` file, given an integer as input.

		Parameters
		---------- :
		snaps : int
			Snapshots you want to plot.
		"""
		index = self.list_snaps.index(self.model + '_%.5d' % i)
		mdl = self.list_mdl[index]
		mdl.read()
		cwriteh5 = CWriteh5(mdl)
		cwriteh5.csnapsh.write_hav_h5(store = self.store)
		cwriteh5.csnapsh.write_thavn_h5()
		self.read_snap[index] = 0

	def process_s(self, snaps):
		"""
		This method will process a snapshot and writing the results in an ``.hdf5`` file, given a strng as input.
		Parameters
		---------- :
		snaps : str
			Snapshots you want to plot.
		"""
		index = self.list_snaps.index(i)
		mdl = self.list_mdl[index]
		mdl.read()
		cwriteh5 = CWriteh5(mdl)
		cwriteh5.csnapsh.write_hav_h5(store = self.store)
		cwriteh5.csnapsh.write_thavn_h5()
		self.read_snap[index] = 0
		

	def process(self, snaps):
		"""
		This method will process all the snapshots and writing the results in an ``.hdf5`` file.

		Parameters
		---------- :
		snaps	int or string or list of int or list of string
			Snapshots you want to plot.
		"""
		# We make sure we are in the good directory
		os.chdir(self.path)

		if snaps == 'all':
			self.process_all()

		else : 
			# snpas is a list
			if type(snaps) == list:
				for i in snaps:
					# i is an int
					if type(i) == int:
						self.process_i(i)
					# i is a string
					elif type(i) == str:
						self.process_s(i)
					else : 
						raise ValueError("Items from snaps should be int or string (name of *.dat file).")
			# snaps is an int
			elif snaps == int:
				self.process_i(snaps)
			# snaps is a string
			elif snaps == str:
				self.process_s(snaps)
			else : 
				raise ValueError("Items from snaps should be int or string (name of *.dat file).")


	def goback(self):
		"""
		This method is used to go back in the directory where you were when then instance was created. 
		This is used for a parallel post processing.
		"""
		os.chdir(self.cwpath)

	def open_all_h5(self, op_mode = 'r'):
		"""
		This method open all the hdf5 files.

		Parameter
		---------
		op_mode : str
			``Opening mode.``
			``r			Readonly, file must exist (default)``
			``r+			Read/write, file must exist``
			``w			Create file, truncate if exists``
			``w- or x		Create file, fail if exists``
			``a			Read/write if exists, create otherwise``
		"""
		os.chdir(self.path)
		# Create a list of opened hdf5 files.
		self.hdf5_files = []
		# Create the list of hdf5 files names.
		l = sorted(glob.glob('*.hdf5'))
		for i in l:
			self.hdf5_files.append(h5py.File(i, op_mode))
		


