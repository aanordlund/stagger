#!/usr/bin/env python
# coding: utf-8

###################################################################
##    AUTHOR: Louis Manchon                                      ##
##    ORGN: Århus University, Fysik og Astronomi                 ##
##    FILE: cv_sav_h5.py                                         ##
##    DATE: May 2016                                             ##
###################################################################

from staggerpy.const import *
import numpy as np
import math
import idlsave
import h5py

"""
This program convert a .sav file into a .hdf5 file. 
The function convert() take the name of the file as argument.
"""


def convert_recarray(grp, s, f):
	"""
	Input: 
		grp 		HDF5 group


		s			savefile values
	"""
	for i in range(len(s)):

		if str(type(s[i])) == "<class 'numpy.recarray'>":
			grp2 = grp.create_group(s.keys()[0])
			convert_recarray(grp2, s[i], f)

		elif str(type(s[i])) == "<class 'numpy.record'>":
			try : 
				grp2 = grp.create_group(s[i][0])
				convert_record(grp2, s[i], f)
			except ValueError, RuntimeError:
				name = grp.name+'/'+s[i][0]
				f[name+"_"+str(s[i-1][4])] = f[name]
				del f[name]
				grp2 = grp.create_group(s[0][0]+"_"+str(s[i][4]))
				convert_record(grp2, s[i], f)
		elif str(type(s[i])) == "<type 'numpy.ndarray'>":
			try : 
				name = s.dtype.descr[i][0][0]
				dt = np.dtype(type(s[i][0]))
				dset = grp.create_dataset(name, data = s[i], dtype = dt)
			except :
				pass
		else : 
			name = s.dtype.descr[i][0][0]
			if str(type(s[i])) == "<type 'str'>":
				dset = grp.create_dataset(name, data = [s[i]], dtype = 'S10')
			else : 
				dt = np.dtype(type(s[i]))
				dset = grp.create_dataset(name, data = [s[i]], dtype = dt)


def convert_record(grp, s, f):
	"""
	Input: 
		grp 		HDF5 group


		s			savefile values
	"""
	for i in range(len(s)):
		if str(type(s[i])) == "<class 'numpy.recarray'>":
				grp2 = grp.create_group(s.keys()[0])
				convert_recarray(grp2, s[i], f)
		elif str(type(s[i])) == "<class 'numpy.record'>":
			try : 
				grp2 = grp.create_group(s.keys()[0])
				convert_record(grp2, s[i], f)
			except ValueError:
				name = grp.name
				f[name+s.keys()[0]+"_"+str(s[0][4])] = f[name]
				del f[name]
				grp2 = grp.create_group(s.keys()[0]+"_"+str(s[i][4]))
				convert_record(grp2, s[i], f)
		elif str(type(s[i])) == "<type 'numpy.ndarray'>":
			try : 
				name = s.dtype.descr[i][0][0]
				dt = np.dtype(type(s[i][0])) 
				dset = grp.create_dataset(name, data = s[i], dtype = dt)
			except :
				pass

		else: 
			name = s.dtype.descr[i][0][0]
			if str(type(s[i])) == "<type 'str'>":
				dset = grp.create_dataset(name, data = [s[i]], dtype = 'S10')
			else : 
				dt = np.dtype(type(s[i]))
				dset = grp.create_dataset(name, data = [s[i]], dtype = dt)
			


def convert(file):
	print "Reading %s..." % file,
	s = idlsave.read(file)
	print "%s[Done]%s" % (GREEN, NO_COLOR)

	print "Converting %s..." % file,
	if file[-3:] == "sav":
		f = h5py.File(file[:-3]+'hdf5', "w")
	else :
		f = h5py.File(file+'.hdf5', "w")
	for i in range(len(s.keys())): # Variable in .sav
		grp = f.create_group(s.keys()[i]) # One group for one variable
		if str(type(s.values()[i])) == "<class 'numpy.recarray'>":
			convert_recarray(grp, s.values()[i], f)
		else :
			convert_record(grp, s.values()[i], f)

	if file[:2] == 'all': # This part correct a common error in the conversion of all* files
		try : 
			i = f[f.keys()[0]].keys()[2][:-2]
			path = 'md/'+i
			isnap = str(f[path+'/isnap'][0])
			f[path+'_'+isnap] = f[path]
			del f[path]
		except : 
			pass
   	f.close()
	print "%s[Done]%s" % (GREEN, NO_COLOR)

