#!/anaconda/bin/python2.7

from distutils.core import setup

__version__ = '1.4.0.0'
__author__ = 'Remo Collet, Louis Manchon'
__contact__ = 'remo@phys.au.dk, louis.manchon@u-psud.fr'
__url__ = 'https://bitbucket.org/lmanchon/staggerpy, https://github.com/narsonalin/Staggerpy, \
      https://github.com/vsilvagui/staggerpy'

setup(name = 'staggerpy',
      version = __version__,
      description = 'Stagger Code Python Utilities',
      author = __author__,
      author_email = __contact__,
      url = __url__,
      scripts = ['cvrt_sav_txt.py', 'read_tim.py', 'cv_sav_h5.py', 'detect_peaks.py', 
      'run_proc_par.py', 'run_thr_par.py', 'run_serial.py'],
      packages = ['staggerpy', 'staggerpy.gui'])
