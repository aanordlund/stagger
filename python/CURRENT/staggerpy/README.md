# README #

This README details the necessary steps to get staggerpy up and running.

# Staggerpy

Staggerpy is a Python package for post-processing Stagger Code simulations.

### Getting the source

You need to have git installed in your computer. If you do, go to the directory where you wish to install staggerpy and type:
```sh
$ git clone git@gitlab.com:iSIMBA/staggerpy.git
```

(replace *username* with your bitbucket or github username).

Or you can also download it from

- https://gitlab.com/iSIMBA/staggerpy 


*For the moment, staggerpy is a work in progress*. 

### Prerequisites

To check if you have all the required softwares, type (in the staggerpy root directory)
```sh
$ bash ./check_deps.sh
```

### Installing 

Jump into staggerpy directory and run: 
```sh
$ python setup.py install
```
If you want to remove this module, run: 
```sh
python setup.py clean
```

### Who do I talk to 

If you have any question, please email louis.manchon@u-psud.fr.

* Repo owner or admin:

iSIMBA (Stellar Astrophysic Center) https://gitlab.com/iSIMBA/staggerpy. Louis Manchon is the main developper of staggerpy. Please contact him at louis.manchon@u-psud.fr for any request.


* Other community or team contact

Remo Collet ( remo@phys.au.dk ) is a developer of the stagger code at Aarhus University (Stellar Astrophysic Center).

### Previous releases

The following releases are available. Pleas ask for them by email ( louis.manchon@u-psud.fr ).
- Version 1.3.0.0
- Version 1.2.0.3
