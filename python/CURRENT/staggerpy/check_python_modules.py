#!/usr/bin/env python

NO_COLOR = "\033[0m"
RED = "\033[31;01m"
GREEN = "\033[32;01m"

#libs = {'numpy' : True, 'scipy' : True, 'matplotlib' : True, 
#        'enthought.traits.api' : True, 'enthought.traits.ui.api' : True, 
#        'enthought.traits.ui.menu' : True}

libs = {'numpy' : True, 'scipy' : True, 'matplotlib' : True, 'h5py' : True, 'os' : True, 
'glob' : True, 'bisect' : True, 'sys' : True, 'psutil' : True}

libs_exist = []

for lib in libs:
    try:
        print 'Checking %s...' % lib, 
        exec('import %s' %lib)
        print '%syes%s' % (GREEN, NO_COLOR)
    except ImportError:
        print '%sno%s' % (RED, NO_COLOR)
        libs[lib] = False

if all(libs.values()):
    print '\nAll python modules required are installed.'
else:
    print '\n%sMissing modules:%s' % (RED, NO_COLOR)
    for lib in libs:
        if not libs[lib]:
            print '   %s' % lib

    print '\nYou will not be able to use the python modules. Please install the missing modules.'
