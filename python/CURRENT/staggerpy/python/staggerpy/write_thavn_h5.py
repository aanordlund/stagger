#!/usr/bin/env python
# coding: utf-8

###################################################################
##    AUTHOR: Louis Manchon                                      ##
##    ORGN: Århus University, Stellar Astrophysic Center         ##
##    FILE: write_thavn_h5.py                                    ##
##    DATE: June 2016                                            ##
###################################################################

from const import *
from CEOStab import *
from CSnapsh import *
import numpy as np
from bisect import *
from scipy import interpolate
import math, h5py, os, glob


def write_thavn_h5(csnapsh, depth_range = (-5., 5.), depth_points = 101):
    """
    Write the averagings at constant Rosseland optical depth.
    """
    csnapsh.open_h5()
    if 'thavn' not in csnapsh.hdf5_file.keys():
        wgrp_h5('thavn', csnapsh.hdf5_file)
    a = csnapsh.nghost
    b = csnapsh.nym - csnapsh.nghost

    # Interpolate basic data cubes
    if csnapsh.store:
        RHO_thavn = csnapsh.new_ym(csnapsh.ltaur, csnapsh.rho
            , depth_range = depth_range, depth_points = depth_range)
        TT_thavn = csnapsh.new_ym(csnapsh.ltaur, csnapsh.tt
            , depth_range = depth_range, depth_points = depth_range)
        E_thavn = csnapsh.new_ym(csnapsh.ltaur, csnapsh.e
            , depth_range = depth_range, depth_points = depth_range)
        EE_thavn = np.divide(E_thavn, RHO_thavn)
        PX_thavn = csnapsh.new_ym(csnapsh.ltaur, csnapsh.px, staggered = True
            , depth_range = depth_range, depth_points = depth_range)
        PY_thavn = csnapsh.new_ym(csnapsh.ltaur, csnapsh.py, staggered = True
            , depth_range = depth_range, depth_points = depth_range)
        PZ_thavn = csnapsh.new_ym(csnapsh.ltaur, csnapsh.pz, staggered = True
            , depth_range = depth_range, depth_points = depth_range)
        UX_thavn = csnapsh.new_ym(csnapsh.ltaur, csnapsh.ux, staggered = True
            , depth_range = depth_range, depth_points = depth_range)
        UY_thavn = csnapsh.new_ym(csnapsh.ltaur, csnapsh.uy, staggered = True
            , depth_range = depth_range, depth_points = depth_range)
        UZ_thavn = csnapsh.new_ym(csnapsh.ltaur, csnapsh.uz, staggered = True
            , depth_range = depth_range, depth_points = depth_range)
        U_thavn = csnapsh.new_ym(csnapsh.ltaur, csnapsh.u, staggered = True
            , depth_range = depth_range, depth_points = depth_range)
        PTURB_thavn = csnapsh.new_ym(csnapsh.ltaur, csnapsh.pturb
            , depth_range = depth_range, depth_points = depth_range)
        UH_thavn = csnapsh.new_ym(csnapsh.ltaur, csnapsh.uh, staggered = True
            , depth_range = depth_range, depth_points = depth_range)
        MASS_thavn = csnapsh.new_ym(csnapsh.ltaur, csnapsh.mass
            , depth_range = depth_range, depth_points = depth_range)
        PTOT_thavn = csnapsh.new_ym(csnapsh.ltaur, csnapsh.ptot
            , depth_range = depth_range, depth_points = depth_range)
        NEL_thavn = csnapsh.new_ym(csnapsh.ltaur, csnapsh.nel
            , depth_range = depth_range, depth_points = depth_range)
        TT4_thavn = csnapsh.new_ym(csnapsh.ltaur, csnapsh.tt4
            , depth_range = depth_range, depth_points = depth_range)
    else :
        LTAUR_thavn = csnapsh.get_ltau()
        RHO_thavn = csnapsh.new_ym(LTAUR_thavn, csnapsh.rho, linear = True
            , depth_range = depth_range, depth_points = depth_range)
        TT_thavn = csnapsh.new_ym(LTAUR_thavn, csnapsh.tt, linear = True
            , depth_range = depth_range, depth_points = depth_range)
        E_thavn = csnapsh.new_ym(LTAUR_thavn, csnapsh.e, linear = True
            , depth_range = depth_range, depth_points = depth_range)
        EE_thavn = np.divide(E_thavn, RHO_thavn)
        PX_thavn = csnapsh.new_ym(LTAUR_thavn, csnapsh.px, staggered = True
            , linear = True, depth_range = depth_range
            , depth_points = depth_range)
        PY_thavn = csnapsh.new_ym(LTAUR_thavn, csnapsh.py, staggered = True
            , linear = True, depth_range = depth_range
            , depth_points = depth_range)
        PZ_thavn = csnapsh.new_ym(LTAUR_thavn, csnapsh.pz, staggered = True
            , linear = True, depth_range = depth_range
            , depth_points = depth_range)
        UX_thavn = csnapsh.new_ym(LTAUR_thavn, np.divide(PX_thavn
            , csnapsh.xdn(RHO_thavn)), staggered = True, linear = True
            , depth_range = depth_range, depth_points = depth_range)
        UY_thavn = csnapsh.new_ym(LTAUR_thavn, np.divide(PY_thavn
            , csnapsh.ydn(RHO_thavn)), staggered = True, linear = True
            , depth_range = depth_range, depth_points = depth_range)
        UZ_thavn = csnapsh.new_ym(LTAUR_thavn, np.divide(PZ_thavn
            , csnapsh.zdn(RHO_thavn)), staggered = True, linear = True
            , depth_range = depth_range, depth_points = depth_range)
        c = UX_thavn * UX_thavn + UZ_thavn * UZ_thavn
        d = UY_thavn * UY_thavn
        vol_thavn = csnapsh.dxm * csnapsh.dym * csnapsh.dzm
        U_thavn = np.sqrt(c + d)
        PTURB_thavn = d * csnapsh.ydn(RHO_thavn)
        UH_thavn = np.sqrt(c)
        lnrho = np.log(csnapsh.rho * csnapsh.uur)
        lne = np.log(csnapsh.ee * csnapsh.uue)
        PTOT_thavn = csnapsh.ceostab.lookup_eos(lnrho, lne, 0, 0, linear = False) - math.log(csnapsh.uua)
        PTOT_thavn = csnapsh.new_ym(LTAUR_thavn, PTOT_thavn, linear = True, depth_range = depth_range
            , depth_points = depth_range)
        NEL_thavn = csnapsh.ceostab.lookup_eos(lnrho, lne, 3, 0, linear = False) - math.log(csnapsh.uua)
        NEL_thavn = csnapsh.new_ym(LTAUR_thavn, NEL_thavn, linear = True, depth_range = depth_range
            , depth_points = depth_range)
        KHIR_R = np.divide(np.exp(csnapsh.get_lnab(lnrho, lne)), csnapsh.rho)# khir/rho
        KHIR5_R = np.divide(np.exp(csnapsh.get_lnab(lnrho, lne, kind = 'r5')), csnapsh.rho)# khi5/rho
        KHIP_R = np.divide(np.exp(csnapsh.get_lnab(lnrho, lne, kind = 'p')), csnapsh.rho)# khip/rho
        KHIR_R_thavn = csnapsh.new_ym(LTAUR_thavn, KHIR_R_thavn, linear = True, depth_range = depth_range
            , depth_points = depth_range)
        KHIR5_R_thavn = csnapsh.new_ym(LTAUR_thavn, KHIR5_R_thavn, linear = True, depth_range = depth_range
            , depth_points = depth_range)
        KHIP_R_thavn = csnapsh.new_ym(LTAUR_thavn, KHIP_R_thavn, linear = True, depth_range = depth_range
            , depth_points = depth_range)
        TT4_thavn = np.power(TT_thavn, 4)

    # Those arrays will contain the averagings
    ux = np.array([]); uxu = np.array([]); uxd = np.array([]); uxs = np.array([]); uxrms = np.array([]); 
    uxmin = np.array([]); uxmax = np.array([]); uxurms = np.array([]); uxdrms = np.array([])
    uy = np.array([]); uyu = np.array([]); uyd = np.array([]); uys = np.array([]); uyrms = np.array([]); 
    uymin = np.array([]); uymax = np.array([]); uyurms = np.array([]); uydrms = np.array([])
    uz = np.array([]); uzu = np.array([]); uzd = np.array([]); uzs = np.array([]); uzrms = np.array([]); 
    uzmin = np.array([]); uzmax = np.array([]); uzurms = np.array([]); uzdrms = np.array([])
    u = np.array([]); uu = np.array([]); ud = np.array([]); us = np.array([]); urms = np.array([]); 
    umin = np.array([]); umax = np.array([]); uurms = np.array([]); udrms = np.array([])
    uh = np.array([]); uhu = np.array([]); uhd = np.array([]); uhs = np.array([]); uhrms = np.array([]); 
    uhmin = np.array([]); uhmax = np.array([]); uhurms = np.array([]); uhdrms = np.array([])
    rho = np.array([]); rhou = np.array([]); rhod = np.array([]); rhos = np.array([]); rhorms = np.array([]); 
    rhomin = np.array([]); rhomax = np.array([]); rholgm = np.array([])
    tt = np.array([]); ttu = np.array([]); ttd = np.array([]); tts = np.array([]); ttrms = np.array([]); 
    ttmin = np.array([]); ttmax = np.array([]); ttlgm = np.array([]); tt4 = np.array([])
    e = np.array([]); eu = np.array([]); ed = np.array([]); es = np.array([]); erms = np.array([]); 
    emin = np.array([]); emax = np.array([]); elgm = np.array([])
    ee = np.array([]); eeu = np.array([]); eed = np.array([]); ees = np.array([]); eerms = np.array([]); 
    eemin = np.array([]); eemax = np.array([]); eelgm = np.array([])
    fmass = np.array([]); fmassu = np.array([]); fmassd = np.array([]); fmasss = np.array([]); fmassrms = np.array([]); 
    fmassmin = np.array([]); fmassmax = np.array([]); fmassurms = np.array([]); fmassdrms = np.array([])
    mass = np.array([])
    mmlu = np.array([]); mmld = np.array([])
    pturb = np.array([]); pturbu = np.array([]); pturbd = np.array([]); pturbs = np.array([]); pturbrms = np.array([]); 
    pturbmin = np.array([]); pturbmax = np.array([]); pturblgm = np.array([])
    ptot = np.array([]); ptotu = np.array([]); ptotd = np.array([]); ptots = np.array([]); ptotrms = np.array([]); 
    ptotmin = np.array([]); ptotmax = np.array([]); ptotlgm = np.array([])
    Int = np.array([]); intu = np.array([]); intd = np.array([]); ints = np.array([]); intrms = np.array([]); # 09.2017 not previously defined
    intmin = np.array([]); intmax = np.array([])
    nel = np.array([]); nelu = np.array([]); neld = np.array([]); nels = np.array([]); nelrms = np.array([]); 
    nelmin = np.array([]); nelmax = np.array([]); nellgm = np.array([])
    kapr = np.array([]); kap5 = np.array([]); kapp = np.array([]) 

    for i in range(depth_points):
        #Velocities
        slc_uy = csnapsh.sl_stat(UY_thavn[:, i, :], i, lgm = False, ud_rms = True)#, store_indices = True)
        slc_ux = csnapsh.sl_stat(UX_thavn[:, i, :], i, lgm = False, ud_rms = True)
        slc_uz = csnapsh.sl_stat(UZ_thavn[:, i, :], i, lgm = False, ud_rms = True)
        slc_u = csnapsh.sl_stat(U_thavn[:, i, :], i, lgm = False, ud_rms = True)
        slc_uh = csnapsh.sl_stat(UH_thavn[:, i, :], i, lgm = False, ud_rms = True)

        ux = np.append(ux, slc_ux[0])
        uxu = np.append(uxu, slc_ux[1])
        uxd = np.append(uxd, slc_ux[2])
        uxs = np.append(uxs, slc_ux[3])
        uxrms = np.append(uxrms, slc_ux[4])
        uxmin = np.append(uxmin, slc_ux[5])
        uxmax = np.append(uxmax, slc_ux[6])
        uxurms = np.append(uxurms, slc_ux[7])
        uxdrms = np.append(uxdrms, slc_ux[8])

        uy = np.append(uy, slc_uy[0])
        uyu = np.append(uyu, slc_uy[1])
        uyd = np.append(uyd, slc_uy[2])
        uys = np.append(uys, slc_uy[3])
        uyrms = np.append(uyrms, slc_uy[4])
        uymin = np.append(uymin, slc_uy[5])
        uymax = np.append(uymax, slc_uy[6])
        uyurms = np.append(uyurms, slc_uy[7])
        uydrms = np.append(uydrms, slc_uy[8])

        uz = np.append(uz, slc_uz[0])
        uzu = np.append(uzu, slc_uz[1])
        uzd = np.append(uzd, slc_uz[2])
        uzs = np.append(uzs, slc_uz[3])
        uzrms = np.append(uzrms, slc_uz[4])
        uzmin = np.append(uzmin, slc_uz[5])
        uzmax = np.append(uzmax, slc_uz[6])
        uzurms = np.append(uzurms, slc_uz[7])
        uzdrms = np.append(uzdrms, slc_uz[8])

        u = np.append(u, slc_u[0])
        uu = np.append(uu, slc_u[1])
        ud = np.append(ud, slc_u[2])
        us = np.append(us, slc_u[3])
        urms = np.append(urms, slc_u[4])
        umin = np.append(umin, slc_u[5])
        umax = np.append(umax, slc_u[6])
        uurms = np.append(uurms, slc_u[7])
        udrms = np.append(udrms, slc_u[8])

        uh = np.append(uh, slc_uh[0])
        uhu = np.append(uhu, slc_uh[1])
        uhd = np.append(uhd, slc_uh[2])
        uhs = np.append(uhs, slc_uh[3])
        uhrms = np.append(uhrms, slc_uh[4])
        uhmin = np.append(uhmin, slc_uh[5])
        uhmax = np.append(uhmax, slc_uh[6])
        uhurms = np.append(uhurms, slc_uh[7])
        uhdrms = np.append(uhdrms, slc_uh[8])

        # rho
        slc_rho = csnapsh.sl_stat(csnapsh.rho[:, i, :], i)

        rho = np.append(rho, slc_rho[0])
        rhou = np.append(rhou, slc_rho[1])
        rhod = np.append(rhod, slc_rho[2])
        rhos = np.append(rhos, slc_rho[3])
        rhorms = np.append(rhorms, slc_rho[4])
        rhomin = np.append(rhomin, slc_rho[5])
        rhomax = np.append(rhomax, slc_rho[6])
        rholgm = np.append(rholgm, slc_rho[7])

        # tt
        slc_tt = csnapsh.sl_stat(csnapsh.tt[:, i, :], i)

        tt = np.append(tt, slc_tt[0])
        ttu = np.append(ttu, slc_tt[1])
        ttd = np.append(ttd, slc_tt[2])
        tts = np.append(tts, slc_tt[3])
        ttrms = np.append(ttrms, slc_tt[4])
        ttmin = np.append(ttmin, slc_tt[5])
        ttmax = np.append(ttmax, slc_tt[6])
        ttlgm = np.append(ttlgm, slc_tt[7])
        tt4 = np.append(tt4, np.mean(TT4[:, i, :]))

        # energy per unit mass
        slc_e = csnapsh.sl_stat(csnapsh.e[:, i, :], i)

        e = np.append(e, slc_e[0])
        eu = np.append(eu, slc_e[1])
        ed = np.append(ed, slc_e[2])
        es = np.append(es, slc_e[3])
        erms = np.append(erms, slc_e[4])
        emin = np.append(emin, slc_e[5])
        emax = np.append(emax, slc_e[6])
        elgm = np.append(elgm, slc_e[7])

        # energy per unit volume
        slc_ee = csnapsh.sl_stat(csnapsh.ee[:, i, :], i)

        ee = np.append(ee, slc_ee[0])
        eeu = np.append(eeu, slc_ee[1])
        eed = np.append(eed, slc_ee[2])
        ees = np.append(ees, slc_ee[3])
        eerms = np.append(eerms, slc_ee[4])
        eemin = np.append(eemin, slc_ee[5])
        eemax = np.append(eemax, slc_ee[6])
        eelgm = np.append(eelgm, slc_ee[7])

        # mass flux
        slc_fmass = csnapsh.sl_stat(csnapsh.py[:, i, :], i, intensive = False, lgm = False, ud_rms = True)

        fmass = np.append(fmass, slc_fmass[0])
        fmassu = np.append(fmassu, slc_fmass[1])
        fmassd = np.append(fmassd, slc_fmass[2])
        fmasss = np.append(fmasss, slc_fmass[3])
        fmassrms = np.append(fmassrms, slc_fmass[4])
        fmassmin = np.append(fmassmin, slc_fmass[5])
        fmassmax = np.append(fmassmax, slc_fmass[6])
        fmassurms = np.append(fmassurms, slc_fmass[7])
        fmassdrms = np.append(fmassdrms, slc_fmass[8])

        # Total pressure
        slc_ptot = csnapsh.sl_stat(PTOT_thavn[:, i, :], i)
        ptot = np.append(ptot, slc_ptot[0])
        ptotu = np.append(ptotu, slc_ptot[1])
        ptotd = np.append(ptotd, slc_ptot[2])
        ptots = np.append(ptots, slc_ptot[3])
        ptotrms = np.append(ptotrms, slc_ptot[4])
        ptotmin = np.append(ptotmin, slc_ptot[5])
        ptotmax = np.append(ptotmax, slc_ptot[6])
        ptotlgm = np.append(ptotlgm, slc_ptot[7])

        # turbulent pressure
        slc_pturb = csnapsh.sl_stat(PTURB_thavn[:, i, :], i)
        pturb = np.append(pturb, slc_pturb[0])
        pturbu = np.append(pturbu, slc_pturb[1])
        pturbd = np.append(pturbd, slc_pturb[2])
        pturbs = np.append(pturbs, slc_pturb[3])
        pturbrms = np.append(pturbrms, slc_pturb[4])
        pturbmin = np.append(pturbmin, slc_pturb[5])
        pturbmax = np.append(pturbmax, slc_pturb[6])
        pturblgm = np.append(pturblgm, slc_pturb[7])

        # Electron density
        slc_nel = csnapsh.sl_stat(NEL_thavn[:, i, :], i)
        nel = np.append(nel, slc_nel[0])
        nelu = np.append(nelu, slc_nel[1])
        neld = np.append(neld, slc_nel[2])
        nels = np.append(nels, slc_nel[3])
        nelrms = np.append(nelrms, slc_nel[4])
        nelmin = np.append(nelmin, slc_nel[5])
        nelmax = np.append(nelmax, slc_nel[6])
        nellgm = np.append(nellgm, slc_nel[7])

        # Rosseland, Rosseland at 500nm and Planck extinction coeff
        kapr = np.append(kapr, np.mean(KHIR_R_thavn[:, i, :]))
        kap5 = np.append(kap5, np.mean(KHIR5_R_thavn[:, i, :]))
        kapp = np.append(kapp, np.mean(KHIP_R_thavn[:, i, :]))

    # Intensities
    slc_i = csnapsh.sl_stat(TT_thavn[:, 0, :], i, lgm = False)

    Int = np.append(Int, slc_i[0])
    intu = np.append(intu, slc_i[1])
    intd = np.append(intd, slc_i[2])
    ints = np.append(ints, slc_i[3])
    intrms = np.append(intrms, slc_i[4])
    intmin = np.append(intmin, slc_i[5])
    intmax = np.append(intmax, slc_i[6])

    # Average optical depth in the Rosseland mean, 
    #in the Rosseland mean at 500nm, in the Planck mean
    ltaur, ltaup, ltau5 = csnapsh.get_ltau_all()
    ltaur = np.mean(np.mean(ltaur, axis = 0), axis = 1)
    ltau5 = np.mean(np.mean(ltau5, axis = 0), axis = 1)
    ltaup = np.mean(np.mean(ltaup, axis = 0), axis = 1)
    
    wdset_arr_h5('ux', ux[a:b], 'thavn', csnapsh.hdf5_file)
    wdset_arr_h5('uxu', uxu[a:b], 'thavn', csnapsh.hdf5_file)
    wdset_arr_h5('uxd', uxd[a:b], 'thavn', csnapsh.hdf5_file)
    wdset_arr_h5('uxs', uxs[a:b], 'thavn', csnapsh.hdf5_file)
    wdset_arr_h5('uxrms', uxrms[a:b], 'thavn', csnapsh.hdf5_file)
    wdset_arr_h5('uxmin', uxmin[a:b], 'thavn', csnapsh.hdf5_file)
    wdset_arr_h5('uxmax', uxmax[a:b], 'thavn', csnapsh.hdf5_file)
    wdset_arr_h5('uxurms', uxurms[a:b], 'thavn', csnapsh.hdf5_file)
    wdset_arr_h5('uxdrms', uxdrms[a:b], 'thavn', csnapsh.hdf5_file)
    wdset_arr_h5('uy', uy[a:b], 'thavn', csnapsh.hdf5_file)
    wdset_arr_h5('uyu', uyu[a:b], 'thavn', csnapsh.hdf5_file)
    wdset_arr_h5('uyd', uyd[a:b], 'thavn', csnapsh.hdf5_file)
    wdset_arr_h5('uys', uys[a:b], 'thavn', csnapsh.hdf5_file)
    wdset_arr_h5('uyrms', uyrms[a:b], 'thavn', csnapsh.hdf5_file)
    wdset_arr_h5('uymin', uymin[a:b], 'thavn', csnapsh.hdf5_file)
    wdset_arr_h5('uymax', uymax[a:b], 'thavn', csnapsh.hdf5_file)
    wdset_arr_h5('uyurms', uyurms[a:b], 'thavn', csnapsh.hdf5_file)
    wdset_arr_h5('uydrms', uydrms[a:b], 'thavn', csnapsh.hdf5_file)
    wdset_arr_h5('uz', uz[a:b], 'thavn', csnapsh.hdf5_file)
    wdset_arr_h5('uzu', uzu[a:b], 'thavn', csnapsh.hdf5_file)
    wdset_arr_h5('uzd', uzd[a:b], 'thavn', csnapsh.hdf5_file)
    wdset_arr_h5('uzs', uzs[a:b], 'thavn', csnapsh.hdf5_file)
    wdset_arr_h5('uzrms', uzrms[a:b], 'thavn', csnapsh.hdf5_file)
    wdset_arr_h5('uzmin', uzmin[a:b], 'thavn', csnapsh.hdf5_file)
    wdset_arr_h5('uzmax', uzmax[a:b], 'thavn', csnapsh.hdf5_file)
    wdset_arr_h5('uzurms', uzurms[a:b], 'thavn', csnapsh.hdf5_file)
    wdset_arr_h5('uzdrms', uzdrms[a:b], 'thavn', csnapsh.hdf5_file)
    wdset_arr_h5('u', u[a:b], 'thavn', csnapsh.hdf5_file)
    wdset_arr_h5('uu', uu[a:b], 'thavn', csnapsh.hdf5_file)
    wdset_arr_h5('ud', ud[a:b], 'thavn', csnapsh.hdf5_file)
    wdset_arr_h5('urms', urms[a:b], 'thavn', csnapsh.hdf5_file)
    wdset_arr_h5('umin', umin[a:b], 'thavn', csnapsh.hdf5_file)
    wdset_arr_h5('umax', umax[a:b], 'thavn', csnapsh.hdf5_file)
    wdset_arr_h5('uurms', uurms[a:b], 'thavn', csnapsh.hdf5_file)
    wdset_arr_h5('udrms', udrms[a:b], 'thavn', csnapsh.hdf5_file)
    wdset_arr_h5('uh', uh[a:b], 'thavn', csnapsh.hdf5_file)
    wdset_arr_h5('uhu', uhu[a:b], 'thavn', csnapsh.hdf5_file)
    wdset_arr_h5('uhd', uhd[a:b], 'thavn', csnapsh.hdf5_file)
    wdset_arr_h5('uhs', uhs[a:b], 'thavn', csnapsh.hdf5_file)
    wdset_arr_h5('uhrms', uhrms[a:b], 'thavn', csnapsh.hdf5_file)
    wdset_arr_h5('uhmin', uhmin[a:b], 'thavn', csnapsh.hdf5_file)
    wdset_arr_h5('uhmax', uhmax[a:b], 'thavn', csnapsh.hdf5_file)
    wdset_arr_h5('uhurms', uhurms[a:b], 'thavn', csnapsh.hdf5_file)
    wdset_arr_h5('uhdrms', uhdrms[a:b], 'thavn', csnapsh.hdf5_file)
    wdset_arr_h5('rho', rho[a:b], 'thavn', csnapsh.hdf5_file)
    wdset_arr_h5('rhou', rhou[a:b], 'thavn', csnapsh.hdf5_file)
    wdset_arr_h5('rhod', rhod[a:b], 'thavn', csnapsh.hdf5_file)
    wdset_arr_h5('rhos', rhos[a:b], 'thavn', csnapsh.hdf5_file)
    wdset_arr_h5('rhorms', rhorms[a:b], 'thavn', csnapsh.hdf5_file)
    wdset_arr_h5('rhomin', rhomin[a:b], 'thavn', csnapsh.hdf5_file)
    wdset_arr_h5('rhomax', rhomax[a:b], 'thavn', csnapsh.hdf5_file)
    wdset_arr_h5('rholgm', rholgm[a:b], 'thavn', csnapsh.hdf5_file)
    wdset_arr_h5('tt', tt[a:b], 'thavn', csnapsh.hdf5_file)
    wdset_arr_h5('ttu', ttu[a:b], 'thavn', csnapsh.hdf5_file)
    wdset_arr_h5('ttd', ttd[a:b], 'thavn', csnapsh.hdf5_file)
    wdset_arr_h5('tts', tts[a:b], 'thavn', csnapsh.hdf5_file)
    wdset_arr_h5('ttrms', ttrms[a:b], 'thavn', csnapsh.hdf5_file)
    wdset_arr_h5('ttmin', ttmin[a:b], 'thavn', csnapsh.hdf5_file)
    wdset_arr_h5('ttmax', ttmax[a:b], 'thavn', csnapsh.hdf5_file)
    wdset_arr_h5('ttlgm', ttlgm[a:b], 'thavn', csnapsh.hdf5_file)
    wdset_arr_h5('tt4', tt4[a:b], 'thavn', csnapsh.hdf5_file)
    wdset_arr_h5('e', e[a:b], 'thavn', csnapsh.hdf5_file)
    wdset_arr_h5('eu', eu[a:b], 'thavn', csnapsh.hdf5_file)
    wdset_arr_h5('ed', es[a:b], 'thavn', csnapsh.hdf5_file)
    wdset_arr_h5('erms', erms[a:b], 'thavn', csnapsh.hdf5_file)
    wdset_arr_h5('emin', emin[a:b], 'thavn', csnapsh.hdf5_file)
    wdset_arr_h5('emax', emax[a:b], 'thavn', csnapsh.hdf5_file)
    wdset_arr_h5('elgm', elgm[a:b], 'thavn', csnapsh.hdf5_file)
    wdset_arr_h5('ee', ee[a:b], 'thavn', csnapsh.hdf5_file)
    wdset_arr_h5('eeu', eeu[a:b], 'thavn', csnapsh.hdf5_file)
    wdset_arr_h5('eed', ees[a:b], 'thavn', csnapsh.hdf5_file)
    wdset_arr_h5('eerms', eerms[a:b], 'thavn', csnapsh.hdf5_file)
    wdset_arr_h5('eemin', eemin[a:b], 'thavn', csnapsh.hdf5_file)
    wdset_arr_h5('eemax', eemax[a:b], 'thavn', csnapsh.hdf5_file)
    wdset_arr_h5('eelgm', eelgm[a:b], 'thavn', csnapsh.hdf5_file)
    wdset_arr_h5('int', Int[a:b], 'thavn', csnapsh.hdf5_file)
    wdset_arr_h5('intu', intu[a:b], 'thavn', csnapsh.hdf5_file)
    wdset_arr_h5('intd', ints[a:b], 'thavn', csnapsh.hdf5_file)
    wdset_arr_h5('intrms', intrms[a:b], 'thavn', csnapsh.hdf5_file)
    wdset_arr_h5('intmin', intmin[a:b], 'thavn', csnapsh.hdf5_file)
    wdset_arr_h5('intmax', intmax[a:b], 'thavn', csnapsh.hdf5_file)
    wdset_arr_h5('fmass', fmass[a:b], 'thavn', csnapsh.hdf5_file)
    wdset_arr_h5('fmassu', fmassu[a:b], 'thavn', csnapsh.hdf5_file)
    wdset_arr_h5('fmassd', fmassd[a:b], 'thavn', csnapsh.hdf5_file)
    wdset_arr_h5('fmasss', fmasss[a:b], 'thavn', csnapsh.hdf5_file)
    wdset_arr_h5('fmassrms', fmassrms[a:b], 'thavn', csnapsh.hdf5_file)
    wdset_arr_h5('fmassmin', fmassmin[a:b], 'thavn', csnapsh.hdf5_file)
    wdset_arr_h5('fmassmax', fmassmax[a:b], 'thavn', csnapsh.hdf5_file)
    wdset_arr_h5('fmassurms', fmassurms[a:b], 'thavn', csnapsh.hdf5_file)
    wdset_arr_h5('fmassdrms', fmassdrms[a:b], 'thavn', csnapsh.hdf5_file)
    #mmlu = 1./np.abs((np.log(np.abs(np.roll(fmassu, -1))) - np.log(np.abs(fmassu))) * csnapsh.dyidydn)
    #mmld = 1./np.abs((np.log(np.roll(fmassd, -1)) - np.log(fmassd)) * csnapsh.dyidydn)
    wdset_arr_h5('mmlu', mmlu[a:b], 'thavn', csnapsh.hdf5_file)
    wdset_arr_h5('mmld', mmld[a:b], 'thavn', csnapsh.hdf5_file)
    wdset_arr_h5('pturb', pturb[a:b], 'thavn', csnapsh.hdf5_file)
    wdset_arr_h5('pturbu', pturbu[a:b], 'thavn', csnapsh.hdf5_file)
    wdset_arr_h5('pturbd', pturbs[a:b], 'thavn', csnapsh.hdf5_file)
    wdset_arr_h5('pturbrms', pturbrms[a:b], 'thavn', csnapsh.hdf5_file)
    wdset_arr_h5('pturbmin', pturbmin[a:b], 'thavn', csnapsh.hdf5_file)
    wdset_arr_h5('pturbmax', pturbmax[a:b], 'thavn', csnapsh.hdf5_file)
    wdset_arr_h5('pturblgm', pturblgm[a:b], 'thavn', csnapsh.hdf5_file)
    wdset_arr_h5('ptot', ptot[a:b], 'thavn', csnapsh.hdf5_file)
    wdset_arr_h5('ptotu', ptotu[a:b], 'thavn', csnapsh.hdf5_file)
    wdset_arr_h5('ptotd', ptots[a:b], 'thavn', csnapsh.hdf5_file)
    wdset_arr_h5('ptotrms', ptotrms[a:b], 'thavn', csnapsh.hdf5_file)
    wdset_arr_h5('ptotmin', ptotmin[a:b], 'thavn', csnapsh.hdf5_file)
    wdset_arr_h5('ptotmax', ptotmax[a:b], 'thavn', csnapsh.hdf5_file)
    wdset_arr_h5('ptotlgm', ptotlgm[a:b], 'thavn', csnapsh.hdf5_file)
    wdset_arr_h5('nel', nel[a:b], 'thavn', csnapsh.hdf5_file)
    wdset_arr_h5('nelu', nelu[a:b], 'thavn', csnapsh.hdf5_file)
    wdset_arr_h5('neld', nels[a:b], 'thavn', csnapsh.hdf5_file)
    wdset_arr_h5('nelrms', nelrms[a:b], 'thavn', csnapsh.hdf5_file)
    wdset_arr_h5('nelmin', nelmin[a:b], 'thavn', csnapsh.hdf5_file)
    wdset_arr_h5('nelmax', nelmax[a:b], 'thavn', csnapsh.hdf5_file)
    wdset_arr_h5('nellgm', nellgm[a:b], 'thavn', csnapsh.hdf5_file)
    wdset_arr_h5('kapr', np.log(kapr[a:b]), 'thavn', csnapsh.hdf5_file)
    wdset_arr_h5('kap5', np.log(kap5[a:b]), 'thavn', csnapsh.hdf5_file)
    wdset_arr_h5('kapp', np.log(kapp[a:b]), 'thavn', csnapsh.hdf5_file)
    wdset_arr_h5('ltaur', ltaur[a:b], 'thavn', csnapsh.hdf5_file)
    wdset_arr_h5('ltau5', ltau5[a:b], 'thavn', csnapsh.hdf5_file)
    wdset_arr_h5('ltaup', ltaup[a:b], 'thavn', csnapsh.hdf5_file)

    del ux; del uxu; del uxd; del uxs; del uxrms
    del uxmin; del uxmax; del uxurms; del uxdrms
    del uy; del uyu; del uyd; del uys; del uyrms
    del uymin; del uymax; del uyurms; del uydrms
    del uz; del uzu; del uzd; del uzs; del uzrms
    del uzmin; del uzmax; del uzurms; del uzdrms
    del u; del uu; del ud; del us; del urms
    del umin; del umax; del uurms; del udrms
    del uh; del uhu; del uhd; del uhs; del uhrms
    del uhmin; del uhmax; del uhurms; del uhdrms
    del rho; del rhou; del rhod; del rhos; del rhorms
    del rhomin; del rhomax; del rholgm
    del tt; del ttu; del ttd; del tts; del ttrms
    del ttmin; del ttmax; del ttlgm; del tt4
    del e; del eu; del ed; del es; del erms
    del emin; del emax; del elgm
    del ee; del eeu; del eed; del ees; del eerms
    del eemin; del eemax; del eelgm
    del Int; del intu; del intd; del ints; del intrms
    del intmin; del intmax
    del fmass; del fmassu; del fmassd; del fmasss; del fmassrms
    del fmassmin; del fmassmax; del fmassurms; del fmassdrms
    del mmlu; del mmld
    del pturb; del pturbu; del pturbd; del pturbs; del pturbrms
    del pturbmin; del pturbmax; del pturblgm
    del ltaur; del ltau5; del ltaup; del kapr; del kap5; del kapp
    del ptot; del ptotu; del ptotd; del ptots; del ptotrms
    del ptotmin; del ptotmax; del ptotlgm
    del nel; del nelu; del neld; del nels; del nelrms
    del nelmin; del nelmax; del nellgm

    del UX_thavn; del UY_thavn; del UZ_thavn; del U_thavn; del PTURB_thavn
    del UH_thavn; del PTOT_thavn; del NEL_thavn; del KHIR_R_thavn
    del KHIR5_R_thavn; del KHIP_R_thavn; del TT4_thavn

    csnapsh.hdf5_file.close()