#!/usr/bin/env python
# coding: utf-8

###################################################################
##    AUTHOR: Louis Manchon                                      ##
##    ORGN: Århus University, Fysik og Astronomi                 ##
##    FILE: store_attr.py                                        ##
##    DATE: January 2016                                         ##
###################################################################

import math, os, glob


def store_attr(nhm, nym):
        """
        This method will try to guess if the available RAM is sufficient to store 
        some intermediate arrays in memory and then speed up the code or if 
        they should be deleted after the call of each method 

        ** DISCLAIMER **
        The main weakness of this routine is that many parameters are hardcoded and 
        should be change as the same time as the complexity of the code grow.
        """
        try: 
            from psutil import virtual_memory
        except: 
            # By default, if the module can not be imported, the arrays will not be stored
            return False
        N_arr = 15.
        v_size = 101
        mem = virtual_memory()
        # limit memory defined as 85 % of the total memory
        limit = (mem.total >> 20) * 0.85
        # Memory needed: we need to store as attributes N_arr arrays with the dimension of the raw data cubes
        # and N_arr temporary arrays of vertical size equal to vsize.
        # Each cell store a 4 bytes float.
        mem_needed = N_arr * (nhm * nym) * 4. + N_arr * (nhm * vsize) * 4.
        # Memory currently used
        used = float(mem.used >> 20)
        if used + mem_needed < limit:
            return True
        else:
            return False