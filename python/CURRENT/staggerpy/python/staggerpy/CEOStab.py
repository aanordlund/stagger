#!/usr/bin/env python
# coding: utf-8


"""
@AUTHOR: Louis Manchon
@ORGN: Århus University, Stellar Astrophysics Center
@FILE: CEOStab.py
@DATE: July 2016

History
-------
July 2016 : Louis Manchon : Translation from Remo Collet's IDL routines.
September 2017 : Andreas C. S. Joergensen : corrected some bugs and Python 2/3 
        compatibility.
"""
from __future__ import print_function
from __future__ import absolute_import

from const import *
import numpy as np
from scipy import interpolate
from bisect import *
import math, h5py, os, glob


#At the end, -ln(uua) 

__metaclass__ = type

class CEOStab:
    """
    Class that read, load and interpolate an EOS table. The bulk of the table, contained in self.tab, 
    is organized as an ``Netab * Nrtab * 3 * Ntbvar`` array of the ``Ntbvar`` variables: ``lnPtot``, ``ln kap_Ross``, 
    ``lnT``, ``ln kap_Planck``, ln kap_500``, and arranged as: the variable, and derivatives w.r.t. ``ln ee``, and ``ln rho``.
        All table entries are functions of lnee and lnrho which are both equidistant grids, with extent 
    and resolution as given in the header.
        All units of quantities in the table are simulation units, as given by ``ul``(length), ``ur``(density) 
    and ``ut``(time) w.r.t. cgs-units.
    """
    def __init__(self, tabfile = '', read = True, repair_deriv = True):
        """
        ``CEOStab.__init__(self, tabfile = '', read = True, repair_deriv = True)``

        Parameters
        ----------
        tabfile : str
            Name of the opacity table file.
        read : ool
            If ``False``, doesn't read the file.
        repair_deriv : bool
            If ``True``, repair
        """
        # Attribute given when creating the class
        if tabfile:
            self.tab_file = tabfile # Unformatted F77 binary file
        else : 
            self.tab_file = sorted(glob.glob('EOS*'))[0]
        if read:
            self.read_eos()
            # Define ln ee and ln rho, respectively the abscissa and the ordinates 
            self.dlnetb = (self.lnemax - self.lnemin)/(self.Netab - 1.)
            self.lnetb = np.arange(self.Netab) * self.dlnetb + self.lnemin
            self.dlnrtb = (self.lnrmax - self.lnrmin)/(self.Nrtab - 1.)
            self.lnrtb = np.arange(self.Nrtab) * self.dlnrtb + self.lnrmin
            if repair_deriv:
                self.repair_deriv()
            if (self.ul, self.ur, self.ut) != (1., 1., 1.):
                # If not in simulation units
                self.conversion()
            

    def read_eos(self, endian = '>'):
        """
        Read ``.eos`` file.

        Parameter
        ---------
        endian : str
            Endianness.
        """
        print("Reading %s ..." % self.tab_file, end=' ')
        f = open(self.tab_file,'rb') 

        u = np.fromfile(f, dtype = endian + 'u4', count = 1)

        # Netab: size along the energy dimension; Nrtab: size along the density dimension; 
        # Ntbvar: number of physical quantities stored in tab; Nelem: Number of abundances stored in self.iel and in self.abund
        self.Netab, self.Nrtab, self.Ma, self.Na, self.Ntbvar, self.Nelem = np.fromfile(f, dtype = endian + 'i4', count = 6) # sizes
        self.Nxopt = np.fromfile(f, dtype = endian + 'i4', count = 20)
        self.Nxrev = np.fromfile(f, dtype = endian + 'i4', count = 20)
        u = np.fromfile(f, dtype = endian + 'u4', count = 1)

        u = np.fromfile(f, dtype = endian + 'u4', count = 1)
        # Date and time of creation
        self.date, self.time = np.fromfile(f, dtype=endian + 'a8', count = 2)
        # lnrmin: minimum density; lnrmax: maximum density
        # lnemin: minimum energy per u. mass; lnemax: maximum energy per u. mass
        # factor w.r.t. to CGS units
        # ul: length; ur: density; ut: time
        self.lnrmin, self.lnrmax, self.lnemin, self.lnemax, self.ul, self.ur, self.ut = np.fromfile(f, dtype = endian + 'f4', count = 7)
        # Name of Chemical elements
        self.iel = np.fromfile(f, dtype= endian + 'a4', count = self.Nelem)
        # ABundances
        self.abund = np.fromfile(f, dtype= endian + 'f4', count = self.Nelem)
        self.arr = np.reshape(np.fromfile(f, dtype= endian + 'f4', count = self.Ma * self.Na), (self.Na, self.Na))
        u = np.fromfile(f, dtype = endian + 'u4', count = 1)

        u = np.fromfile(f, dtype = endian + 'u4', count = 1)
        #Opacity table
        count = self.Netab * self.Nrtab * 3 * self.Ntbvar
        self.tab = np.reshape(np.fromfile(f, dtype= endian + 'f4', count = count), (self.Ntbvar, 3, self.Nrtab, self.Netab))

        f.close()
        print("%s[Done]%s" % (GREEN, NO_COLOR))

    def conversion(self):
        """
        Convert ``tab`` to simulation unit.
        """
        # Simulation units w.r.t. CGS units
        uur = 1.e-7 # density
        uul = 1.e8  # distance
        uut = 1.e2  # time
        uuv = 1.e6  # velocity: uul/uut

        # Conversion factors for converting from table to simulation units.
        ulnr  = math.log(self.ur / uur); ulne = 2. * math.log(self.ul / self.ut / uuv)
        ulnP  = (ulnr + ulne); ulnabs = math.log(uul / self.ul)
        if abs(ulnr) > 1.e-3:
            self.lnrtb += ulnr
            self.lnrmin += ulnr
            self.lnrmax += ulnr
        if abs(ulne) > 1.e-3:
            self.lnetb += ulne
            self.lnemin += ulne
            self.lnemax += ulne
        if abs(ulnP) > 1.e-3:
            self.tab[0, 0, :, :] += ulnP
        if abs(ulnabs) > 1.e-3:
            self.tab[1, 0, :, :] += ulnabs
            self.tab[-2, 0, :, :] += ulnabs
            self.tab[-1, 0, :, :] += ulnabs
        # Reset the table unit conversion factor
        ur = uur; ul = uul; ut = uut

    def repair_deriv(self, endian = '>'):
        """
        Repair the ``dlnNe/dlnrho`` derivative of the table and write the repaired tab in new ``tab`` file.

        Parameter
        ---------
        endian : str
            Endianness.

        To do
        -----
            # This script is a translation from an IDL routine. 
        Check if the dimension 184 must be replace by something more flexible.
        """
        if self.Ntbvar > 5:
            ir = np.where((self.lnrtb >= -15.)  &  (self.lnrtb <= -10.))
            if np.abs(np.mean(self.tab[3, 2, ir, 0])-0.3) > 0.1:
                self.tab[:,:,2,3] = self.tab[3, 2, :, :] - self.dlnrtb
                with open(self.tab_file + '.bad-dlnNedlnr', 'wb') as f:
                    np.array([184], dtype = endian + 'u4').tofile(f)
                    np.append(np.append([self.Netab, self.Nrtab, self.Ma, self.Na, self.Ntbvar, self.Nelem], self.Nxopt), self.Nxrev).astype(endian + 'i4').tofile(f)
                    np.array([184, 184], dtype = endian + 'u4').tofile(f)
                    np.array([self.date, self.time]).astype(endian + 'a8').tofile(f)
                    np.array([self.lnrmin, self.lnrmax, self.lnemin, self.lnemax, self.ul, self.ur, self.ut]).astype(endian + 'f4').tofile(f)
                    self.iel.astype(endian + 'a4').tofile(f)
                    self.abund.astype(endian + 'f4').tofile(f)
                    self.arr.flatten().astype(endian + 'f4').tofile(f)
                    np.array([184, 184], dtype = endian + 'u4').tofile(f)
                    self.tab.flatten().astype(endian + 'f4').tofile(f)

    def lookup_eos(self, lnrho, lne, j, i, linear = False):
        """
        Program to perform linear interpolation in the ``EOSrhoe.tab-tables``.

        Parameters
        ----------
        lnrho : np.array
            ln(density) in table units (cf. ``ur`` in common ``ceostab``).
        lne : np.array
            ln(internal energy per mass/[sim]).
        j : int
            Index of variable: ``0 = lnPtot``, ``1 = ln kap_Ross``, ``2 = lnT``,
            ``3 = ln ne``, ``4 = ln kap_Planck``, ``5 = ln kap_500``.
        i : int
            Derivative index; ``0 = f``, ``1 = df/dlne``, ``2 = df/dlnrho``
        linear : bool
            If ``True``, perform a linear interpolation else Taylor developement.

        Returns
        ------- 
            Array of same format as ``lnrho`` and ``lne`` contaning the interpolated values of the 
            specified table-entry.

        Example
        -------
        lnrho and lne should be in cgs units.
        When use from CSnapsh.py: 
        >>> lookup_eos(np.log(mdl.rho * mdl.uur), np.log(mdl.ee * mdl.uue), ...)
        """
        if linear:
            ii = i  
        else:
            ii = 0
        tb = self.tab[j, ii, :, :]

        # Density index
        ri = (lnrho - self.lnrmin) / self.dlnrtb
        # This will contain the index of the nearest neighbour of ri's items
        ir = np.floor(np.array(ri))
        # Keep only the items between 0 and Nrtab - 2
        np.putmask(ir, ir < 0., 0.)
        np.putmask(ir, ir > self.Nrtab - 2., self.Nrtab - 2.)
        ir = ir.astype('i2')
        # Distance from the lower neighbour
        py = (ri - ir)
        # Distance from the upper neighbour
        qy = 1. - py
        #                +
        #        .       |
        #   +            |
        #   |            |
        #   o----|-------o
        #   <----><------>
        #     py     qy


        # Energy index
        ei = (lne - self.lnemin) / self.dlnetb
        # This will contain the index of the nearest neighbour of ei's items
        ie = np.floor(np.array(ei))
        # Keep only the items between 0 and Netab - 2
        np.putmask(ie, ie < 0., 0.)
        np.putmask(ie, ie > self.Netab - 2., self.Netab - 2.)
        ie = ie.astype('i2')
        # Distance from the lower neighbour
        px = (ei - ie)
        # Distance from the upper neighbour
        qx = 1. - px

        if not linear:
            tbx = self.tab[j, 1, :, :]; tby = self.tab[j, 2, :, :]
            # x-terms
            pxqx = px * qx
            qxqx = qx * qx
            pxpx = px * px
            dfx1 = tb[ir, ie + 1] - tb[ir, ie]
            dfx2 = tb[ir + 1, ie + 1] - tb[ir + 1, ie]
            # y-terms
            pyqy = py * qy
            qyqy = qy * qy
            pypy = py * py
            dfy1 = tb[ir + 1, ie] - tb[ir, ie]
            dfy2 = tb[ir + 1, ie + 1] - tb[ir, ie + 1]
        #  Perform the interpolation
        if not linear:
            if i == 0: 
                a1 = tb[ir, ie] + pxqx * (tbx[ir, ie] - dfx1) + pyqy * (tby[ir, ie] - dfy1)
                a2 = tb[ir + 1, ie] + pxqx * (tbx[ir + 1, ie] - dfx2) - pyqy * (tby[ir + 1, ie] - dfy1)
                tabvar = qx * (qy * a1 + py * a2)
                b1 = (tb[ir + 1, ie + 1] - pxqx* (tbx[ir + 1, ie + 1] - dfx2) - pyqy * (tby[ir + 1, ie + 1] - dfy2))
                b2 = (tb[ir, ie + 1] - pxqx * (tbx[ir, ie + 1] - dfx1) + pyqy * (tby[ir, ie + 1] - dfy2))
                tabvar += px * (py * b1 + qy * b2)
            elif i == 1:
                #(dlnP/dlne)_rho    d/dx
                a1 = - tb[ir, ie] + (qxqx - 2. * pxqx) * (tbx[ir, ie] - dfx1) 
                a2 = - pyqy * (tby[ir, ie] - dfy1) + tb[ir, ie + 1]
                a3 = (pxpx - 2. * pxqx) * (tbx[ir, ie + 1] - dfx1) + pyqy * (tby[ir, ie + 1] - dfy2)
                tabvar = qy * (a1 + a2 + a3)
                b1 = tb[ir + 1, ie + 1] + (pxpx - 2. * pxqx) * (tbx[ir + 1, ie + 1] - dfx2) 
                b2 = - pyqy * (tby[ir + 1, ie + 1] - dfy2) - tb[ir + 1, ie]
                b3 = (qxqx - 2. * pxqx) * (tbx[ir + 1, ie] - dfx2) + pyqy * (tby[ir + 1, ie] - dfy1)
                tabvar += py * (b1 + b2 + b3)
            elif i == 2:
            #(dlnP/dlnr)_ee         d/dy
                a1 = - tb[ir, ie] - pxqx * (tbx[ir, ie] - dfx1)
                a2 = (qyqy - 2. * pyqy) * (tby[ir, ie] - dfy1) + tb[ir + 1, ie]
                a3 = pxqx * (tbx[ir + 1, ie] - dfx2) + (pypy - 2. * pyqy) * (tby[ir + 1, ie] - dfy1)
                tabvar = qx * (a1 + a2 + a3)
                b1 = - tb[ir, ie + 1] + pxqx * (tbx[ir, ie + 1] - dfx1)
                b2 = (qyqy - 2. * pyqy) * (tby[ir, ie + 1] - dfy2) + tb[ir + 1, ie + 1]
                b3 = - pxqx * (tbx[ir + 1, ie + 1] - dfx2) + (pypy - 2. * pyqy) * (tby[ir + 1, ie + 1] - dfy2) 
                tabvar += px * (b1 + b2 + b3)
            else:
                print("WARNING: Calling lookup_eos with i = ", i, "Doesn't make sense. imust be equal to 0, 1, or 2.")
        else:
            tabvar = qy * (qx * tb[ir, ie] + px * tb[ir, ie + 1]) + py * (px * tb[ir + 1, ie + 1] + qx * tb[ir + 1, ie])

        #  The table contains derivatives multiplied by the grid-spacing, e.g.,
        #  (dlnP/dlne)*\Del(lne) - this is what's needed for the interpolations.
        #  Divide by \Del(lne) and \Del(lnrho), respectively, to get the proper
        #  thermodynamic derivatives.
        if i == 1:
            tabvar /= self.dlnetb
        if i == 2:
            tabvar /= self.dlnrtb
        return tabvar

    def depth_int(self, data, depth, minim = 0., vertical = 1, spline = True, logvar = True):
        """
        Depth integrate data cube. Returns the new depth scale.

        Parameters
        ----------
        data : np.array
            Data cube.
        depth : np.array
            Geometrical depth.
        minim : float
            Minimum value contained in the new depth cube.
        vertical : int
            Dimension corresponding to the vertical. 
        spline : bool
            If ``False``, linear interpolation.
        logvar : bool
            If ``True``, the data are in log scale.

        Returns
        -------
            Depth cube with the same shape of the data cube. 
        """
        eps = 1.e-14
        ndim = data.ndim
        if ndim == 1:
            nsize = data.size
            var = np.reshape(data, (1, 1, nsize))
            vertical = 2
        elif ndim == 3:
            if vertical == 0:
                var = np.transpose(data, (2, 1, 0))
            elif vertical == 1:
                var = np.transpose(data, (0, 2, 1))
            elif vertical == 2:
                var = data
            else :
                print('Vertical must be equal to 0, 1, or 2.')
        else:
            print('Unsupported ndim value: ndim =', ndim)
    
        # Dimensions of var array;
        # Define arrays res and dres that will contain result and its finite increment
        nx, ny, nz = var.shape
        dresult = np.empty((nx, ny, nz), dtype = 'f4')
        result = np.empty((nx, ny, nz), dtype = 'f4')
        
        # spline integration ?
        if spline:
            dvarl = np.empty((nx, ny, nz), dtype = 'f4')

        # logarithmic input data?
        if logvar: 
            varl = var
            var = np.exp(varl)
        else:
            varl = np.log(var)  

        # spline integration ?
        if spline:
            for n in range(1, nz - 1):
                #dvarl[n, :, :] = varl[n + 1, :, :] - varl[n - 1, :, :]
                dvarl[:, :, n] = varl[:, :, n + 1] - varl[:, :, n - 1]

            #dvarl[0, :, :] = 2.0 * (varl[1, :, :] - varl[0, :, :])
            #dvarl[nz - 1, :, :] = 2.0 * (varl[nz - 1, :, :] - varl[nz - 2, :, :])
            #dresult[0, :, :] = var[0, :, :] * (depth[1] - depth[0])

            dvarl[:, :, 0] = 2.0 * (varl[:, :, 1] - varl[:, :, 0])
            dvarl[:, :, nz - 1] = 2.0 * (varl[:, :, nz - 1] - varl[:, :, nz - 2])
            dresult[:, :, 0] = var[:, :, 0] * (depth[1] - depth[0])
            
            for n in range(1,  nz):
                #delta = depth[n] - depth[n - 1]
                #a1 = var[n - 1, :, :] * 0.5 * (1.0 + 0.08333333 * dvarl[n - 1, :, :])
                #np.putmask(a1, a1 < eps, eps)
                #a2 = var[n, :, :] * 0.5 * (1.0 - 0.08333333 * dvarl[n, :, :])
                #np.putmask(a2, a2 < eps, eps)
                #dresult[n, :, :] = delta * (a1 + a2)

                delta = depth[n] - depth[n - 1]
                a1 = var[:, :, n - 1] * 0.5 * (1.0 + 0.08333333 * dvarl[:, :, n - 1])
                np.putmask(a1, a1 < eps, eps)
                a2 = var[:, :, n] * 0.5 * (1.0 - 0.08333333 * dvarl[:, :, n])
                np.putmask(a2, a2 < eps, eps)
                dresult[:, :, n] = delta * (a1 + a2)
        else:
            #dresult = 0.5 * (depth - np.roll(depth, 1, axis = 0)) * (var + np.roll(var, 1, axis = 0))
            #dresult[0, :, :] = 0.5 * (depth[1] - depth[0]) * var[0, :, :]
            f1 = 0.5
            dresult[:, :, 0] = 0.5 * (depth[1] - depth[0]) * var[:, :, 0]
            
            for n in range(1, nz):
                delta = depth[n] - depth[n - 1]
                dresult[:, :, n] = 0.5 * delta * (var[:, :, n - 1] + var[:, :, n])

            #dresult = 0.5 * (depth - np.roll(depth, 1)) * (var + np.roll(var, 1, axis = 2))
            #dresult[:, :, 0] = 0.5 * (depth[1] - depth[0]) * var[:, :, 0]

        #result[0, :, :] = minim + dresult[0, :, :]
        result[:, :, 0] = minim + dresult[:, :, 0]
        for n in range(1,  nz):
            #result[n, :, :] = result[n - 1, :, :] + dresult[n, :, :]
            result[:, :, n] = result[:, :, n - 1] + dresult[:, :, n]

        # if one-dimensional, reshape result arrays
        if ndim == 1:
            result = np.reshape(result, (nx, ny, nz))
            dresult = np.reshape(dresult, (nx, ny, nz))

        # transpose back ?
        if vertical == 0:
            result = np.transpose(result, (2, 1, 0))
            dresult = np.transpose(dresult, (2, 1, 0))
        elif vertical == 1:
            result = np.transpose(result, (0, 2, 1))
            dresult = np.transpose(dresult, (0, 2, 1))
        else: 
            pass
        return result

    def tau_calc(self, lnab, depth, taumin = 1.0e-10, vertical = 1, spline = True, logvar = True):
        """
        Compute optical depth tau given ln extinction coefficient lnab and geometrical depth ym.

        Parameters
        ----------
        lnab : np.array
            Logarithmic extinction coefficient (usually a cube).
        depth : np.array
            Geometrical depth.
        taumin : float
            Minimum value contained in the new depth cube.
        vertical : int
            Dimension corresponding to the vertical. 
        spline : bool
            If ``False``, linear interpolation.
        logvar : bool
            If ``True``, the data are in log scale.
        """
        return self.depth_int(lnab, depth, minim = taumin, vertical = vertical, spline = spline, logvar = logvar) 

