#!/usr/bin/env python
# coding: utf-8

###################################################################
##    AUTHOR: Louis Manchon                                      ##
##    ORGN: Århus University, Stellar Astrophysic Center         ##
##    FILE: write_hav_h5.py                                      ##
##    DATE: June 2016                                            ##
###################################################################

from const import *
from store_attr import store_attr
from h5py_wrapper import *
from CEOStab import *
from CSnapsh import *
import numpy as np
from bisect import *
from scipy import interpolate
import math, h5py, os, glob


def write_hav_h5(cnapsh, store = True):
    """
    Write the horizontal averagings at constant geometrical depth.
    """
    csnapsh.open_h5()
    if 'hav' not in csnapsh.hdf5_file.keys():
        wgrp_h5('hav', csnapsh.hdf5_file)
    a = csnapsh.nghost
    b = csnapsh.nym - csnapsh.nghost

    wdset_all_h5('name', csnapsh.model, dtype = np.dtype((str, len(csnapsh.model))), dset = 'val', csnapsh.hdf5_file)
    wdset_all_h5('meshfile', csnapsh.msh_file, dtype = np.dtype((str, len(csnapsh.msh_file))), dset = 'val', csnapsh.hdf5_file)
    wdset_all_h5('dxfile', csnapsh.dx_file, dtype = np.dtype((str, len(csnapsh.msh_file))), dset = 'val', csnapsh.hdf5_file)
    wdset_all_h5('chkfile', csnapsh.chk_file, dtype = np.dtype((str, len(csnapsh.msh_file))), dset = 'val', csnapsh.hdf5_file)
    wdset_all_h5('filename', csnapsh.h5_file, dtype = np.dtype((str, len(csnapsh.h5_file))), dset = 'val', csnapsh.hdf5_file)
    wdset_all_h5('vs', int(csnapsh.version), dtype = 'i1', dset = 'val', csnapsh.hdf5_file)
    wdset_all_h5('isnap',csnapsh.isnap, dtype = 'i2', dset = 'val', csnapsh.hdf5_file)
    
    wdset_arr_h5('depth', csnapsh.ym[a:b], 'hav', csnapsh.hdf5_file)
    wdset_arr_h5('depth_shiftdn', csnapsh.ym[a:b] - csnapsh.dym[a:b] / 2., 'hav', csnapsh.hdf5_file)
    wdset_arr_h5('depth_shiftup', csnapsh.ymdn[a:b] + csnapsh.dym[a:b] / 2., 'hav', csnapsh.hdf5_file)

    # You want to store some arrays ? 
    if store:
        store = store_attr(csnapsh.nhm csnapsh.nym)
    UX_hav = csnapsh.get_vel_cube(0)
    UY_hav = csnapsh.get_vel_cube(1)
    UZ_hav = csnapsh.get_vel_cube(2)
    c = U_havX * U_havX + U_havZ * U_havZ
    d = U_havY * U_havY
    vol = csnapsh.dxm * csnapsh.dym * csnapsh.dzm
    U_hav = np.sqrt(c + d)
    PTURB_hav = d * csnapsh.ydn(csnapsh.rho)
    UH_hav = np.sqrt(c)
    MASS_hav = csnapsh.rho * vol
    lnrho = np.log(csnapsh.rho * csnapsh.uur)
    lne = np.log(csnapsh.ee * csnapsh.uue)
    PTOT_hav = csnapsh.ceostab.lookup_eos(lnrho, lne, 0, 0, linear = False) - math.log(csnapsh.uua)
    NEL_hav = csnapsh.ceostab.lookup_eos(lnrho, lne, 3, 0, linear = False) - math.log(csnapsh.uua)
    KHIR_R_hav= np.divide(np.exp(csnapsh.get_lnab(lnrho, lne)), csnapsh.rho)# khir/rho
    KHIR5_R_hav = np.divide(np.exp(csnapsh.get_lnab(lnrho, lne, kind = 'r5')), csnapsh.rho)# khi5/rho
    KHIP_R_hav = np.divide(np.exp(csnapsh.get_lnab(lnrho, lne, kind = 'p')), csnapsh.rho)# khip/rho
    TT4_hav = np.power(csnapsh.tt, 4)

    del c; del d; del vol; del lnrho; del lne

    # Those arrays will contain the averagings
    ux = np.array([]); uxu = np.array([]); uxd = np.array([]); uxs = np.array([]); uxrms = np.array([]); 
    uxmin = np.array([]); uxmax = np.array([]); uxurms = np.array([]); uxdrms = np.array([])
    uy = np.array([]); uyu = np.array([]); uyd = np.array([]); uys = np.array([]); uyrms = np.array([]); 
    uymin = np.array([]); uymax = np.array([]); uyurms = np.array([]); uydrms = np.array([])
    uz = np.array([]); uzu = np.array([]); uzd = np.array([]); uzs = np.array([]); uzrms = np.array([]); 
    uzmin = np.array([]); uzmax = np.array([]); uzurms = np.array([]); uzdrms = np.array([])
    u = np.array([]); uu = np.array([]); ud = np.array([]); us = np.array([]); urms = np.array([]); 
    umin = np.array([]); umax = np.array([]); uurms = np.array([]); udrms = np.array([])
    uh = np.array([]); uhu = np.array([]); uhd = np.array([]); uhs = np.array([]); uhrms = np.array([]); 
    uhmin = np.array([]); uhmax = np.array([]); uhurms = np.array([]); uhdrms = np.array([])
    rho = np.array([]); rhou = np.array([]); rhod = np.array([]); rhos = np.array([]); rhorms = np.array([]); 
    rhomin = np.array([]); rhomax = np.array([]); rholgm = np.array([])
    tt = np.array([]); ttu = np.array([]); ttd = np.array([]); tts = np.array([]); ttrms = np.array([]); 
    ttmin = np.array([]); ttmax = np.array([]); ttlgm = np.array([]); tt4 = np.array([])
    e = np.array([]); eu = np.array([]); ed = np.array([]); es = np.array([]); erms = np.array([]); 
    emin = np.array([]); emax = np.array([]); elgm = np.array([])
    ee = np.array([]); eeu = np.array([]); eed = np.array([]); ees = np.array([]); eerms = np.array([]); 
    eemin = np.array([]); eemax = np.array([]); eelgm = np.array([])
    fmass = np.array([]); fmassu = np.array([]); fmassd = np.array([]); fmasss = np.array([]); fmassrms = np.array([]); 
    fmassmin = np.array([]); fmassmax = np.array([]); fmassurms = np.array([]); fmassdrms = np.array([])
    Int = np.array([]); intu = np.array([]); intd = np.array([]); ints = np.array([]); intrms = np.array([]); 
    intmin = np.array([]); intmax = np.array([])
    mass = np.array([])
    mmlu = np.array([]); mmld = np.array([])
    pturb = np.array([]); pturbu = np.array([]); pturbd = np.array([]); pturbs = np.array([]); pturbrms = np.array([]); 
    pturbmin = np.array([]); pturbmax = np.array([]); pturblgm = np.array([])
    ptot = np.array([]); ptotu = np.array([]); ptotd = np.array([]); ptots = np.array([]); ptotrms = np.array([]); 
    ptotmin = np.array([]); ptotmax = np.array([]); ptotlgm = np.array([])
    nel = np.array([]); nelu = np.array([]); neld = np.array([]); nels = np.array([]); nelrms = np.array([]); 
    nelmin = np.array([]); nelmax = np.array([]); nellgm = np.array([])
    kapr = np.array([]); kap5 = np.array([]); kapp = np.array([]) 

    for i in range(csnapsh.nym):
        #Velocities
        slc_uy = csnapsh.sl_stat(UY_hav[:, i, :], i, lgm = False, ud_rms = True, store_indices = True)
        slc_ux = csnapsh.sl_stat(UX_hav[:, i, :], i, lgm = False, ud_rms = True)
        slc_uz = csnapsh.sl_stat(UZ_hav[:, i, :], i, lgm = False, ud_rms = True)
        slc_u = csnapsh.sl_stat(U_hav[:, i, :], i, lgm = False, ud_rms = True)
        slc_uh = csnapsh.sl_stat(UH_hav[:, i, :], i, lgm = False, ud_rms = True)

        ux = np.append(ux, slc_ux[0])
        uxu = np.append(uxu, slc_ux[1])
        uxd = np.append(uxd, slc_ux[2])
        uxs = np.append(uxs, slc_ux[3])
        uxrms = np.append(uxrms, slc_ux[4])
        uxmin = np.append(uxmin, slc_ux[5])
        uxmax = np.append(uxmax, slc_ux[6])
        uxurms = np.append(uxurms, slc_ux[7])
        uxdrms = np.append(uxdrms, slc_ux[8])

        uy = np.append(uy, slc_uy[0])
        uyu = np.append(uyu, slc_uy[1])
        uyd = np.append(uyd, slc_uy[2])
        uys = np.append(uys, slc_uy[3])
        uyrms = np.append(uyrms, slc_uy[4])
        uymin = np.append(uymin, slc_uy[5])
        uymax = np.append(uymax, slc_uy[6])
        uyurms = np.append(uyurms, slc_uy[7])
        uydrms = np.append(uydrms, slc_uy[8])

        uz = np.append(uz, slc_uz[0])
        uzu = np.append(uzu, slc_uz[1])
        uzd = np.append(uzd, slc_uz[2])
        uzs = np.append(uzs, slc_uz[3])
        uzrms = np.append(uzrms, slc_uz[4])
        uzmin = np.append(uzmin, slc_uz[5])
        uzmax = np.append(uzmax, slc_uz[6])
        uzurms = np.append(uzurms, slc_uz[7])
        uzdrms = np.append(uzdrms, slc_uz[8])

        u = np.append(u, slc_u[0])
        uu = np.append(uu, slc_u[1])
        ud = np.append(ud, slc_u[2])
        us = np.append(us, slc_u[3])
        urms = np.append(urms, slc_u[4])
        umin = np.append(umin, slc_u[5])
        umax = np.append(umax, slc_u[6])
        uurms = np.append(uurms, slc_u[7])
        udrms = np.append(udrms, slc_u[8])

        uh = np.append(uh, slc_uh[0])
        uhu = np.append(uhu, slc_uh[1])
        uhd = np.append(uhd, slc_uh[2])
        uhs = np.append(uhs, slc_uh[3])
        uhrms = np.append(uhrms, slc_uh[4])
        uhmin = np.append(uhmin, slc_uh[5])
        uhmax = np.append(uhmax, slc_uh[6])
        uhurms = np.append(uhurms, slc_uh[7])
        uhdrms = np.append(uhdrms, slc_uh[8])

        # rho
        slc_rho = csnapsh.sl_stat(csnapsh.rho[:, i, :], i)

        rho = np.append(rho, slc_rho[0])
        rhou = np.append(rhou, slc_rho[1])
        rhod = np.append(rhod, slc_rho[2])
        rhos = np.append(rhos, slc_rho[3])
        rhorms = np.append(rhorms, slc_rho[4])
        rhomin = np.append(rhomin, slc_rho[5])
        rhomax = np.append(rhomax, slc_rho[6])
        rholgm = np.append(rholgm, slc_rho[7])

        # tt
        slc_tt = csnapsh.sl_stat(csnapsh.tt[:, i, :], i)

        tt = np.append(tt, slc_tt[0])
        ttu = np.append(ttu, slc_tt[1])
        ttd = np.append(ttd, slc_tt[2])
        tts = np.append(tts, slc_tt[3])
        ttrms = np.append(ttrms, slc_tt[4])
        ttmin = np.append(ttmin, slc_tt[5])
        ttmax = np.append(ttmax, slc_tt[6])
        ttlgm = np.append(ttlgm, slc_tt[7])
        tt4 = np.append(tt4, np.mean(TT4[:, i, :]))

        # energy per unit mass
        slc_e = csnapsh.sl_stat(csnapsh.e[:, i, :], i)

        e = np.append(e, slc_e[0])
        eu = np.append(eu, slc_e[1])
        ed = np.append(ed, slc_e[2])
        es = np.append(es, slc_e[3])
        erms = np.append(erms, slc_e[4])
        emin = np.append(emin, slc_e[5])
        emax = np.append(emax, slc_e[6])
        elgm = np.append(elgm, slc_e[7])

        # energy per unit volume
        slc_ee = csnapsh.sl_stat(csnapsh.ee[:, i, :], i)

        ee = np.append(ee, slc_ee[0])
        eeu = np.append(eeu, slc_ee[1])
        eed = np.append(eed, slc_ee[2])
        ees = np.append(ees, slc_ee[3])
        eerms = np.append(eerms, slc_ee[4])
        eemin = np.append(eemin, slc_ee[5])
        eemax = np.append(eemax, slc_ee[6])
        eelgm = np.append(eelgm, slc_ee[7])

        # mass flux
        slc_fmass = csnapsh.sl_stat(csnapsh.py[:, i, :], i, intensive = False, lgm = False, ud_rms = True)

        fmass = np.append(fmass, slc_fmass[0])
        fmassu = np.append(fmassu, slc_fmass[1])
        fmassd = np.append(fmassd, slc_fmass[2])
        fmasss = np.append(fmasss, slc_fmass[3])
        fmassrms = np.append(fmassrms, slc_fmass[4])
        fmassmin = np.append(fmassmin, slc_fmass[5])
        fmassmax = np.append(fmassmax, slc_fmass[6])
        fmassurms = np.append(fmassurms, slc_fmass[7])
        fmassdrms = np.append(fmassdrms, slc_fmass[8])

        # Total pressure
        slc_ptot = csnapsh.sl_stat(PTOT_hav[:, i, :], i)
        ptot = np.append(ptot, slc_ptot[0])
        ptotu = np.append(ptotu, slc_ptot[1])
        ptotd = np.append(ptotd, slc_ptot[2])
        ptots = np.append(ptots, slc_ptot[3])
        ptotrms = np.append(ptotrms, slc_ptot[4])
        ptotmin = np.append(ptotmin, slc_ptot[5])
        ptotmax = np.append(ptotmax, slc_ptot[6])
        ptotlgm = np.append(ptotlgm, slc_ptot[7])

        # turbulent pressure
        slc_pturb = csnapsh.sl_stat(PTURB_hav[:, i, :], i)
        pturb = np.append(pturb, slc_pturb[0])
        pturbu = np.append(pturbu, slc_pturb[1])
        pturbd = np.append(pturbd, slc_pturb[2])
        pturbs = np.append(pturbs, slc_pturb[3])
        pturbrms = np.append(pturbrms, slc_pturb[4])
        pturbmin = np.append(pturbmin, slc_pturb[5])
        pturbmax = np.append(pturbmax, slc_pturb[6])
        pturblgm = np.append(pturblgm, slc_pturb[7])

        # Electron density
        slc_nel = csnapsh.sl_stat(NEL_hav[:, i, :], i)
        nel = np.append(nel, slc_nel[0])
        nelu = np.append(nelu, slc_nel[1])
        neld = np.append(neld, slc_nel[2])
        nels = np.append(nels, slc_nel[3])
        nelrms = np.append(nelrms, slc_nel[4])
        nelmin = np.append(nelmin, slc_nel[5])
        nelmax = np.append(nelmax, slc_nel[6])
        nellgm = np.append(nellgm, slc_nel[7])

        # Rosseland, Rosseland at 500nm and Planck extinction coeff
        kapr = np.append(kapr, np.mean(KHIR_R_hav[:, i, :]))
        kap5 = np.append(kap5, np.mean(KHIR5_R_hav[:, i, :]))
        kapp = np.append(kapp, np.mean(KHIP_R_hav[:, i, :]))

    # Intensities
    slc_i = csnapsh.sl_stat(csnapsh.tt[:, 0, :], i, lgm = False)

    Int = np.append(Int, slc_i[0])
    intu = np.append(intu, slc_i[1])
    intd = np.append(intd, slc_i[2])
    ints = np.append(ints, slc_i[3])
    intrms = np.append(intrms, slc_i[4])
    intmin = np.append(intmin, slc_i[5])
    intmax = np.append(intmax, slc_i[6])

    # Total mass per slice
    #mass = np.append(mass, np.sum(MASS[:, i, :]))
    mass = np.sum(np.sum(MASS_hav, axis = 0), axis = 1)

    # Average optical depth in the Rosseland mean, 
    #in the Rosseland mean at 500nm, in the Planck mean
    LTAUR_hav, LTAUP_hav, LTAU5_hav = csnapsh.get_ltau_all()
    ltaur = np.mean(np.mean(LTAUR_hav, axis = 0), axis = 1)
    ltau5 = np.mean(np.mean(LTAU5_hav, axis = 0), axis = 1)
    ltaup = np.mean(np.mean(LTAUP_hav, axis = 0), axis = 1)
    
    wdset_arr_h5('ux', ux[a:b], 'hav', csnapsh.hdf5_file)
    wdset_arr_h5('uxu', uxu[a:b], 'hav', csnapsh.hdf5_file)
    wdset_arr_h5('uxd', uxd[a:b], 'hav', csnapsh.hdf5_file)
    wdset_arr_h5('uxs', uxs[a:b], 'hav', csnapsh.hdf5_file)
    wdset_arr_h5('uxrms', uxrms[a:b], 'hav', csnapsh.hdf5_file)
    wdset_arr_h5('uxmin', uxmin[a:b], 'hav', csnapsh.hdf5_file)
    wdset_arr_h5('uxmax', uxmax[a:b], 'hav', csnapsh.hdf5_file)
    wdset_arr_h5('uxurms', uxurms[a:b], 'hav', csnapsh.hdf5_file)
    wdset_arr_h5('uxdrms', uxdrms[a:b], 'hav', csnapsh.hdf5_file)
    wdset_arr_h5('uy', uy[a:b], 'hav', csnapsh.hdf5_file)
    wdset_arr_h5('uyu', uyu[a:b], 'hav', csnapsh.hdf5_file)
    wdset_arr_h5('uyd', uyd[a:b], 'hav', csnapsh.hdf5_file)
    wdset_arr_h5('uys', uys[a:b], 'hav', csnapsh.hdf5_file)
    wdset_arr_h5('uyrms', uyrms[a:b], 'hav', csnapsh.hdf5_file)
    wdset_arr_h5('uymin', uymin[a:b], 'hav', csnapsh.hdf5_file)
    wdset_arr_h5('uymax', uymax[a:b], 'hav', csnapsh.hdf5_file)
    wdset_arr_h5('uyurms', uyurms[a:b], 'hav', csnapsh.hdf5_file)
    wdset_arr_h5('uydrms', uydrms[a:b], 'hav', csnapsh.hdf5_file)
    wdset_arr_h5('uz', uz[a:b], 'hav', csnapsh.hdf5_file)
    wdset_arr_h5('uzu', uzu[a:b], 'hav', csnapsh.hdf5_file)
    wdset_arr_h5('uzd', uzd[a:b], 'hav', csnapsh.hdf5_file)
    wdset_arr_h5('uzs', uzs[a:b], 'hav', csnapsh.hdf5_file)
    wdset_arr_h5('uzrms', uzrms[a:b], 'hav', csnapsh.hdf5_file)
    wdset_arr_h5('uzmin', uzmin[a:b], 'hav', csnapsh.hdf5_file)
    wdset_arr_h5('uzmax', uzmax[a:b], 'hav', csnapsh.hdf5_file)
    wdset_arr_h5('uzurms', uzurms[a:b], 'hav', csnapsh.hdf5_file)
    wdset_arr_h5('uzdrms', uzdrms[a:b], 'hav', csnapsh.hdf5_file)
    wdset_arr_h5('u', u[a:b], 'hav', csnapsh.hdf5_file)
    wdset_arr_h5('uu', uu[a:b], 'hav', csnapsh.hdf5_file)
    wdset_arr_h5('ud', ud[a:b], 'hav', csnapsh.hdf5_file)
    wdset_arr_h5('urms', urms[a:b], 'hav', csnapsh.hdf5_file)
    wdset_arr_h5('umin', umin[a:b], 'hav', csnapsh.hdf5_file)
    wdset_arr_h5('umax', umax[a:b], 'hav', csnapsh.hdf5_file)
    wdset_arr_h5('uurms', uurms[a:b], 'hav', csnapsh.hdf5_file)
    wdset_arr_h5('udrms', udrms[a:b], 'hav', csnapsh.hdf5_file)
    wdset_arr_h5('uh', uh[a:b], 'hav', csnapsh.hdf5_file)
    wdset_arr_h5('uhu', uhu[a:b], 'hav', csnapsh.hdf5_file)
    wdset_arr_h5('uhd', uhd[a:b], 'hav', csnapsh.hdf5_file)
    wdset_arr_h5('uhs', uhs[a:b], 'hav', csnapsh.hdf5_file)
    wdset_arr_h5('uhrms', uhrms[a:b], 'hav', csnapsh.hdf5_file)
    wdset_arr_h5('uhmin', uhmin[a:b], 'hav', csnapsh.hdf5_file)
    wdset_arr_h5('uhmax', uhmax[a:b], 'hav', csnapsh.hdf5_file)
    wdset_arr_h5('uhurms', uhurms[a:b], 'hav', csnapsh.hdf5_file)
    wdset_arr_h5('uhdrms', uhdrms[a:b], 'hav', csnapsh.hdf5_file)
    wdset_arr_h5('rho', rho[a:b], 'hav', csnapsh.hdf5_file)
    wdset_arr_h5('rhou', rhou[a:b], 'hav', csnapsh.hdf5_file)
    wdset_arr_h5('rhod', rhod[a:b], 'hav', csnapsh.hdf5_file)
    wdset_arr_h5('rhos', rhos[a:b], 'hav', csnapsh.hdf5_file)
    wdset_arr_h5('rhorms', rhorms[a:b], 'hav', csnapsh.hdf5_file)
    wdset_arr_h5('rhomin', rhomin[a:b], 'hav', csnapsh.hdf5_file)
    wdset_arr_h5('rhomax', rhomax[a:b], 'hav', csnapsh.hdf5_file)
    wdset_arr_h5('rholgm', rholgm[a:b], 'hav', csnapsh.hdf5_file)
    wdset_arr_h5('tt', tt[a:b], 'hav', csnapsh.hdf5_file)
    wdset_arr_h5('ttu', ttu[a:b], 'hav', csnapsh.hdf5_file)
    wdset_arr_h5('ttd', ttd[a:b], 'hav', csnapsh.hdf5_file)
    wdset_arr_h5('tts', tts[a:b], 'hav', csnapsh.hdf5_file)
    wdset_arr_h5('ttrms', ttrms[a:b], 'hav', csnapsh.hdf5_file)
    wdset_arr_h5('ttmin', ttmin[a:b], 'hav', csnapsh.hdf5_file)
    wdset_arr_h5('ttmax', ttmax[a:b], 'hav', csnapsh.hdf5_file)
    wdset_arr_h5('ttlgm', ttlgm[a:b], 'hav', csnapsh.hdf5_file)
    wdset_arr_h5('tt4', tt4[a:b], 'hav', csnapsh.hdf5_file)
    wdset_arr_h5('e', e[a:b], 'hav', csnapsh.hdf5_file)
    wdset_arr_h5('eu', eu[a:b], 'hav', csnapsh.hdf5_file)
    wdset_arr_h5('ed', es[a:b], 'hav', csnapsh.hdf5_file)
    wdset_arr_h5('erms', erms[a:b], 'hav', csnapsh.hdf5_file)
    wdset_arr_h5('emin', emin[a:b], 'hav', csnapsh.hdf5_file)
    wdset_arr_h5('emax', emax[a:b], 'hav', csnapsh.hdf5_file)
    wdset_arr_h5('elgm', elgm[a:b], 'hav', csnapsh.hdf5_file)
    wdset_arr_h5('ee', ee[a:b], 'hav', csnapsh.hdf5_file)
    wdset_arr_h5('eeu', eeu[a:b], 'hav', csnapsh.hdf5_file)
    wdset_arr_h5('eed', ees[a:b], 'hav', csnapsh.hdf5_file)
    wdset_arr_h5('eerms', eerms[a:b], 'hav', csnapsh.hdf5_file)
    wdset_arr_h5('eemin', eemin[a:b], 'hav', csnapsh.hdf5_file)
    wdset_arr_h5('eemax', eemax[a:b], 'hav', csnapsh.hdf5_file)
    wdset_arr_h5('eelgm', eelgm[a:b], 'hav', csnapsh.hdf5_file)
    wdset_arr_h5('int', Int[a:b], 'hav', csnapsh.hdf5_file)
    wdset_arr_h5('intu', intu[a:b], 'hav', csnapsh.hdf5_file)
    wdset_arr_h5('intd', ints[a:b], 'hav', csnapsh.hdf5_file)
    wdset_arr_h5('intrms', intrms[a:b], 'hav', csnapsh.hdf5_file)
    wdset_arr_h5('intmin', intmin[a:b], 'hav', csnapsh.hdf5_file)
    wdset_arr_h5('intmax', intmax[a:b], 'hav', csnapsh.hdf5_file)
    wdset_arr_h5('fmass', fmass[a:b], 'hav', csnapsh.hdf5_file)
    wdset_arr_h5('fmassu', fmassu[a:b], 'hav', csnapsh.hdf5_file)
    wdset_arr_h5('fmassd', fmassd[a:b], 'hav', csnapsh.hdf5_file)
    wdset_arr_h5('fmasss', fmasss[a:b], 'hav', csnapsh.hdf5_file)
    wdset_arr_h5('fmassrms', fmassrms[a:b], 'hav', csnapsh.hdf5_file)
    wdset_arr_h5('fmassmin', fmassmin[a:b], 'hav', csnapsh.hdf5_file)
    wdset_arr_h5('fmassmax', fmassmax[a:b], 'hav', csnapsh.hdf5_file)
    wdset_arr_h5('fmassurms', fmassurms[a:b], 'hav', csnapsh.hdf5_file)
    wdset_arr_h5('fmassdrms', fmassdrms[a:b], 'hav', csnapsh.hdf5_file)
    mmlu = 1./np.abs((np.log(np.abs(np.roll(fmassu, -1))) - np.log(np.abs(fmassu))) * csnapsh.dyidydn)
    mmld = 1./np.abs((np.log(np.roll(fmassd, -1)) - np.log(fmassd)) * csnapsh.dyidydn)
    wdset_arr_h5('mmlu', mmlu[a:b], 'hav', csnapsh.hdf5_file)
    wdset_arr_h5('mmld', mmld[a:b], 'hav', csnapsh.hdf5_file)
    wdset_arr_h5('pturb', pturb[a:b], 'hav', csnapsh.hdf5_file)
    wdset_arr_h5('pturbu', pturbu[a:b], 'hav', csnapsh.hdf5_file)
    wdset_arr_h5('pturbd', pturbs[a:b], 'hav', csnapsh.hdf5_file)
    wdset_arr_h5('pturbrms', pturbrms[a:b], 'hav', csnapsh.hdf5_file)
    wdset_arr_h5('pturbmin', pturbmin[a:b], 'hav', csnapsh.hdf5_file)
    wdset_arr_h5('pturbmax', pturbmax[a:b], 'hav', csnapsh.hdf5_file)
    wdset_arr_h5('pturblgm', pturblgm[a:b], 'hav', csnapsh.hdf5_file)
    wdset_arr_h5('ptot', ptot[a:b], 'hav', csnapsh.hdf5_file)
    wdset_arr_h5('ptotu', ptotu[a:b], 'hav', csnapsh.hdf5_file)
    wdset_arr_h5('ptotd', ptots[a:b], 'hav', csnapsh.hdf5_file)
    wdset_arr_h5('ptotrms', ptotrms[a:b], 'hav', csnapsh.hdf5_file)
    wdset_arr_h5('ptotmin', ptotmin[a:b], 'hav', csnapsh.hdf5_file)
    wdset_arr_h5('ptotmax', ptotmax[a:b], 'hav', csnapsh.hdf5_file)
    wdset_arr_h5('ptotlgm', ptotlgm[a:b], 'hav', csnapsh.hdf5_file)
    wdset_arr_h5('nel', nel[a:b], 'hav', csnapsh.hdf5_file)
    wdset_arr_h5('nelu', nelu[a:b], 'hav', csnapsh.hdf5_file)
    wdset_arr_h5('neld', nels[a:b], 'hav', csnapsh.hdf5_file)
    wdset_arr_h5('nelrms', nelrms[a:b], 'hav', csnapsh.hdf5_file)
    wdset_arr_h5('nelmin', nelmin[a:b], 'hav', csnapsh.hdf5_file)
    wdset_arr_h5('nelmax', nelmax[a:b], 'hav', csnapsh.hdf5_file)
    wdset_arr_h5('nellgm', nellgm[a:b], 'hav', csnapsh.hdf5_file)
    wdset_arr_h5('mass', mass[a:b], 'hav', csnapsh.hdf5_file)
    wdset_arr_h5('kapr', np.log(kapr[a:b]), 'hav', csnapsh.hdf5_file)
    wdset_arr_h5('kap5', np.log(kap5[a:b]), 'hav', csnapsh.hdf5_file)
    wdset_arr_h5('kapp', np.log(kapp[a:b]), 'hav', csnapsh.hdf5_file)
    wdset_arr_h5('ltaur', ltaur[a:b], 'hav', csnapsh.hdf5_file)
    wdset_arr_h5('ltau5', ltau5[a:b], 'hav', csnapsh.hdf5_file)
    wdset_arr_h5('ltaup', ltaup[a:b], 'hav', csnapsh.hdf5_file)

    del ux; del uxu; del uxd; del uxs; del uxrms
    del uxmin; del uxmax; del uxurms; del uxdrms
    del uy; del uyu; del uyd; del uys; del uyrms
    del uymin; del uymax; del uyurms; del uydrms
    del uz; del uzu; del uzd; del uzs; del uzrms
    del uzmin; del uzmax; del uzurms; del uzdrms
    del u; del uu; del ud; del us; del urms
    del umin; del umax; del uurms; del udrms
    del uh; del uhu; del uhd; del uhs; del uhrms
    del uhmin; del uhmax; del uhurms; del uhdrms
    del rho; del rhou; del rhod; del rhos; del rhorms
    del rhomin; del rhomax; del rholgm
    del tt; del ttu; del ttd; del tts; del ttrms
    del ttmin; del ttmax; del ttlgm; del tt4
    del e; del eu; del ed; del es; del erms
    del emin; del emax; del elgm
    del ee; del eeu; del eed; del ees; del eerms
    del eemin; del eemax; del eelgm
    del Int; del intu; del intd; del ints; del intrms
    del intmin; del intmax
    del fmass; del fmassu; del fmassd; del fmasss; del fmassrms
    del fmassmin; del fmassmax; del fmassurms; del fmassdrms
    del mmlu; del mmld
    del pturb; del pturbu; del pturbd; del pturbs; del pturbrms
    del pturbmin; del pturbmax; del pturblgm
    del mass; del ltaur; del ltau5; del ltaup; del kapr; del kap5; del kapp
    del ptot; del ptotu; del ptotd; del ptots; del ptotrms
    del ptotmin; del ptotmax; del ptotlgm
    del nel; del nelu; del neld; del nels; del nelrms
    del nelmin; del nelmax; del nellgm

    if csnapsh.store:
        csnapsh.ux = UX_hav
        del UX_hav
        csnapsh.uy = UY_hav
        del UY_hav
        csnapsh.uz = UZ_hav
        del UZ_hav
        csnapsh.u = U_hav
        del U_hav
        csnapsh.pturb = PTURB_hav
        del PTURB_hav
        csnapsh.uh = UH_hav
        del UH_hav
        csnapsh.mass = MASS_hav
        del MASS_hav
        csnapsh.ptot = PTOT_hav
        del PTOT_hav
        csnapsh.nel = NEL_hav
        del NEL_hav
        csnapsh.khir_r = KHIR_R_hav
        del KHIR_R_hav
        csnapsh.khir5_r = KHIR5_R_hav
        del KHIR5_R_hav
        csnapsh.khip_r = KHIP_R_hav
        del KHIP_R_hav
        csnapsh.tt4 = TT4_hav
        del TT4_hav
        csnapsh.ltaur = LTAUR_hav
        del LTAUR_hav
        csnapsh.ltau5 = LTAU5_hav
        del LTAU5_hav
        csnapsh.ltaup = LTAUP_hav
        del LTAUP_hav
    else:
        del UX_hav; del UY_hav; del UZ_hav; del U_hav; del PTURB_hav; del UH_hav
        del MASS_hav; del PTOT_hav; del NEL_hav; del KHIR_R_hav; del KHIR5_R_hav; 
        del KHIP_R_hav; del TT4_hav; del LTAUR_hav; del LTAU5_hav; del LTAUP_hav

    csnapsh.hdf5_file.close()