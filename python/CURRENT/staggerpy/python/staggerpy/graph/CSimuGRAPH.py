#!/anaconda/bin/python
# coding: utf-8

###################################################################
##    AUTHOR: Louis Manchon                                      ##
##    ORGN: Århus University, Stellar Astrophysic Center         ##
##    FILE: CSimuGUI.py                                          ##
##    DATE: June 2016                                            ##
###################################################################

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm
from matplotlib.colors import LogNorm
from matplotlib.legend_handler import HandlerLine2D
from scipy import interpolate
import math, h5py, os, sys

sys.path.append('/Users/Loulou/Desktop/Louis/Mag2/STAGE/staggerpy/python/staggerpy/')

from const import *
from CSimu import *


class CSimuGUI(CSimu):
    """
    Class that can draw plots from a whole simulation.
    """
    def __init__(self, path, sim, erase = False, v = '05', read = False):
        """
        Parameters
        ----------
        path : str
            Path to the directory in wich the snapshots are stored. It is advisable to set it explicitly, 
            unless your already in the right directory.
        sim : str
            Name of the simulations: ``txxgyymzz``.
        erase : bool
            If ``True``, erase the previously existing ``.hdf5`` files.
        v : str
            Number of the version. 
        read : bool
            If ``False``, doesn't read the files.
        """
        self.path = path
        self.cwpath = os.getcwd()
        self.model = sim
        self.version = v
        # We're jumping in the simulation directory
        os.chdir(self.path)
        # Create instance of CSimu
        self.sim = CSimu(self.path, self.model, erase = erase, v = v, read = read)
        # Open the hdf5 files
        self.open_all_h5()
        # Set some default plotting parameters 
        plt.rc('figure', figsize = [12,9])
        plt.rc('text', usetex=True)
        plt.rc('font', size= 20)
        plt.rc('savefig', dpi = 500)

    def __getitem__(self, item):
        return self.sim[item]

    def __getitem__(self):
        return len(self.sim)

    def open_all_h5(self, op_mode = 'r'):
        """
        This method open all the hdf5 files.

        Parameter
        ---------
        op_mode : str
            ``Opening mode.``
            ``r         Readonly, file must exist (default)``
            ``r+            Read/write, file must exist``
            ``w         Create file, truncate if exists``
            ``w- or x       Create file, fail if exists``
            ``a         Read/write if exists, create otherwise``
        """
        self.sim.open_all_h5(op_mode = op_mode)
        self.hdf5_files = self.sim.hdf5_files

    def process_all(self):
        """
        Alias of ``CSimu.process_all``
        """
        self.sim.process_all()

    def phavs(self, x, y, snaps = 'all', style = '', color = '', log = None, norm = 1.):
        """
        This method plot horizontal average along an other variable.

        Parameters
        ----------
        x : str
            Name of the ``x`` variable. Must be stored in the ``.hdf5`` file associated.
        y : str
            Name of the ``y`` variable. Must be stored in the ``.hdf5`` file associated.
        snaps : list of int
            List of integers corresponding to the snapshots you want to plot.
        style : str
            Style of the curve.
        color : str
            Color of the curve.
        log : str
            Must designate the axis that will show logarithmic coordinates.
            Possible arguments: ``None``, ``'X'``, ``'x'``, ``'Y'``, ``'y'``, ``'XY'`` or ``'xy'``.
        norm : float
            Normalization factor.
        """
        # Get arrays
        X = self.sim[x]
        Y = self.sim[y]
        if snaps == 'all':
            l = range(len(X))
        # Logarithmic scale ? 
        if log == None:
            pass
        elif log == 'X' or log == 'x':
            plt.semilogx
        elif log == 'Y' or log == 'y':
            plt.semilogy
        elif log == 'XY' or log == 'xy':
            plt.loglog()
        else : 
            raise ValueError("log must be equal to None, 'X', 'x', 'Y', 'y', 'XY' or 'xy'.")

        # Plot normalize values
        for i in l:
            plt.plot(X[i], Y[i]/norm, style+color)
        # Plot legend if they are stoed in var dictionnary in const.py
        try:
            xlabel = self.hdf5_files[0][x].attrs['legend']
            ylabel = self.hdf5_files[0][y].attrs['legend']
            plt.xlabel(xlabel, fontsize=20)
            plt.ylabel(ylabel, fontsize=20)
        except:
            pass















