#!/usr/bin/env python
# coding: utf-8

###################################################################
##    AUTHOR: Louis Manchon                                      ##
##    ORGN: Århus University, Stellar Astrophysic Center         ##
##    FILE: stag_hdf5.py                                         ##
##    DATE: January 2016                                         ##
###################################################################

from const import *
#from CEOStab import *
#from CSnapsh import *
import numpy as np
import math, h5py

"""
The output of staggerpy is an hdf5 file. This file contains a set of groups, each one devoted to one type of averaging.
"""

def wgrp_h5(group_name, file):
    """
    Create a group in a ``.hdf5`` file.

    Parameters
    ----------
    group_name : str
        Name of the group to be created.
    file : h5py instance
    """
    try : 
        file.create_group(group_name)
        print('done!')
    except :
        print('fail...')
        pass

def wdset_val_h5(name, value, group_name, file, dtype = 'f4'):
    """
    Try to write a single value in the ``.hdf5`` file.

    Parameters
    ----------
    name : str
        Name of the value to be created.
    value : single value (int or float or str)
    group_name : str    
        Name of the group in which the value should be copied
    file : h5py instance
    dtype : str 
        Data type of the value. You should stick to one endianness otherwise, 
        everything will be messed up. By defult we use big endian.  
    """
    try : 
        file[group_name].create_dataset(name, data = [value], dtype = dtype)
        file[group_name+'/'+name].attrs['legend'] = var[name][0]
        file[group_name+'/'+name].attrs['factor'] = var[name][1]
        file[group_name+'/'+name].attrs['unit'] = var[name][2]
    except:
        pass 

def wdset_arr_h5(name, array, group_name, file, dtype = 'f4'):
    """
    Try to write an array in the ``.hdf5`` file.

    Parameters
    ----------
    name : str
        Name of the array to be created.
    array : np.array
    group_name : str    
        Name of the group in which the array should be copied
    file : h5py instance
    dtype : str 
        Data type of the np.array. You should stick to one endianness otherwise, 
        everything will be messed up. By defult we use big endian.
    """
    try : 
        file[group_name].create_dataset(name, data = array, dtype = dtype)
        file[group_name+'/'+name].attrs['legend'] = var[name][0]
        file[group_name+'/'+name].attrs['factor'] = var[name][1]
        file[group_name+'/'+name].attrs['unit'] = var[name][2]
    except: 
        pass

def wdset_all_h5(name, data, file, dtype = 'f4', dset = 'arr'):
    """
    Try to write a data in all the groups.

    Parameters
    ----------
    name : str
        Name of the value/array to be created.
    data : np.array or single value
        Data to be stored.
    file : h5py instance
    dtype : str 
        Data type of the np.array. You should stick to one endianness otherwise, 
        everything will be messed up. By defult we use big endian.
    dset : str
        If ``dset = 'arr'``, call ``wdset_arr_h5``, otherwise, 
        call ``wdset_val_h5``.
    """
    if dset == 'arr':
        for i in file.keys():
            wdset_arr_h5(name, data, i, file, dtype = dtype)
    else :
        for i in file.keys():
            wdset_val_h5(name, data, i, file, dtype = dtype)