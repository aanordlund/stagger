#!/usr/bin/env python
# coding: utf-8

#If used from ast*d.phys.au.dk machines:
#/usr/users/lmanchon/miniconda2/envs/py27/bin/python python

###################################################################
##    AUTHOR: Louis Manchon                                      ##
##    ORGN: Århus University, Fysik og Astronomi                 ##
##    FILE: run_serial.py                                        ##
##    DATE: June 2016                                            ##
###################################################################

import numpy as np
import glob
import datetime
import staggerpy
from const import *
from staggerpy import *


path_to_stagger_grid = '/auto_mnt/SIMDATA/stagger/grid/'

today = datetime.date.today()
current_time = datetime.datetime.now().time() 
current_time.isoformat()
print "_______ Start: ", today, current_time, "_______"


for i in list_sim:
    today = datetime.date.today()
    current_time = datetime.datetime.now().time() 
    current_time.isoformat()
    print "_______ Next step: ", today, current_time, "_______" 
    try : 
        sim = CSimu(path_to_stagger_grid+'/'+i+'/v05', i, read = False)
        sim.process_all(kill = True)
        sim.goback
    except:
        pass

today = datetime.date.today()
current_time = datetime.datetime.now().time() 
current_time.isoformat()
print "_______ End: ", today, current_time, "_______" 