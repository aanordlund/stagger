#!/usr/bin/env python
# coding: utf-8

#If used from ast*d.phys.au.dk machines:
#/usr/users/lmanchon/miniconda2/envs/py27/bin/python python

###################################################################
##    AUTHOR: Louis Manchon                                      ##
##    ORGN: Århus University, Stellar Astrophysic Center         ##
##    FILE: run_parallel.py                                      ##
##    DATE: June 2016                                            ##
###################################################################

import numpy as np
import glob
import datetime
#import staggerpy
#from staggerpy.CSnapsh import *
#from staggerpy.CSimu import *
#from staggerpy.const import *
from const import *
#from staggerpy import *
import multiprocessing


path_to_stagger_grid = '/auto_mnt/SIMDATA/stagger/grid/'

def print_time(string):
    today = datetime.date.today()
    current_time = datetime.datetime.now().time() 
    current_time.isoformat()
    print "_______ " + string +": ", today, current_time, "_______"

def run(sims):
    for sim in sims:
        today = datetime.date.today()
        current_time = datetime.datetime.now().time() 
        current_time.isoformat()
        print_time('Next Step')
        try:
            sim.process_all()
        except:
            pass
        sim.goback

# ----------------- fin de la classe ----------

print_time('Start')

N = 4 # Number of threads

list2 = list(list_sim[:70])
list_mods = [] # each sub list of list_mods contains the simulations for each core.
for i in range(N):
    list_mods.append([])
b = True
while b :
    try:
        for i in range(N):
            list_mods[i].append(CSimu(path_to_stagger_grid+'/'+list2[0]+'/v05', list2[0], read = False, erase = True))
            list2.remove(list2[0])
    except :
        b = False

jobs = []
for sims in list_mods:
    p = multiprocessing.Process(target=run, args=(sims,))
    jobs.append(p)
    p.start()

print_time('End')

