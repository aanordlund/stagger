#!/usr/bin/env python

from distutils.core import setup

__version__ = '1.4.0.0'
__author__ = 'Louis Manchon, Remo Collet'
__contact__ = 'louis.manchon@u-psud.fr, remo@phys.au.dk'
__url__ = 'https://gitlab.com/iSIMBA/staggerpy.git'


setup(name = 'staggerpy',
    version = __version__,
    description = 'Stagger Code Python Utilities',
    author = __author__,
    author_email = __contact__,
    url = __url__,
    scripts = ['detect_peaks.py', 'run_proc_par.py', 'run_thr_par.py', 'run_serial.py'],
    packages = ['staggerpy', 'staggerpy.gui'])
