#!/usr/bin/env python
# coding: utf-8

import os, glob, sys


NO_COLOR = "\033[0m"
RED = "\033[31;01m"
GREEN = "\033[32;01m"
YELLOW = "\033[33;01m"

version = str(sys.argv[1])

PATH_TO_GRID = '/SIMDATA/stagger/grid'

list_sim = ['t40g15m00', 't40g15m05', 't40g15m10', 't40g15m20', 't40g15m30',
       't40g15m40', 't40g15p05', 't40g20m00', 't40g20m05', 't40g20m10',
       't40g20m20', 't40g20m30', 't40g20m40', 't40g20p05', 't40g25m00',
       't40g25m05', 't40g25m10', 't40g25m20', 't40g25m30', 't40g25m40',
       't40g25p05', 't45g15m00', 't45g15m05', 't45g15m10', 't45g15m20',
       't45g15m30', 't45g15m40', 't45g15p05', 't45g20m00', 't45g20m05',
       't45g20m10', 't45g20m20', 't45g20m30', 't45g20m40', 't45g20p05',
       't45g25m00', 't45g25m05', 't45g25m10', 't45g25m20', 't45g25m30',
       't45g25m40', 't45g25p05', 't45g30m00', 't45g30m05', 't45g30m10',
       't45g30m20', 't45g30m30', 't45g30m40', 't45g30p05', 't45g35m00',
       't45g35m05', 't45g35m10', 't45g35m20', 't45g35m30', 't45g35m40',
       't45g35p05', 't45g40m00', 't45g40m05', 't45g40m10', 't45g40m20',
       't45g40m30', 't45g40m40', 't45g40p05', 't45g45m00', 't45g45m05',
       't45g45m10', 't45g45m20', 't45g45m30', 't45g45m40', 't45g45p05',
       't45g50m00', 't45g50m05', 't45g50m10', 't45g50m20', 't45g50m30',
       't45g50m40', 't45g50p05', 't50g20m00', 't50g20m05', 't50g20m10',
       't50g20m20', 't50g20m30', 't50g20m40', 't50g25m00', 't50g25m05',
       't50g25m10', 't50g25m20', 't50g25m30', 't50g25m40', 't50g25p05',
       't50g30m00', 't50g30m05', 't50g30m10', 't50g30m20', 't50g30m30',
       't50g30m40', 't50g30p05', 't50g35m00', 't50g35m05', 't50g35m10',
       't50g35m20', 't50g35m30', 't50g35m40', 't50g35p05', 't50g40m00',
       't50g40m05', 't50g40m10', 't50g40m20', 't50g40m30', 't50g40m40',
       't50g40p05', 't50g45m00', 't50g45m05', 't50g45m10', 't50g45m20',
       't50g45m30', 't50g45m40', 't50g45p05', 't50g50m00', 't50g50m05',
       't50g50m10', 't50g50m20', 't50g50m30', 't50g50m40', 't50g50p05',
       't55g25m20', 't55g25m30', 't55g25m40', 't55g30m00', 't55g30m05',
       't55g30m10', 't55g30m20', 't55g30m30', 't55g30m40', 't55g30p05',
       't55g35m00', 't55g35m05', 't55g35m10', 't55g35m20', 't55g35m30',
       't55g35m40', 't55g35p05', 't55g40m00', 't55g40m05', 't55g40m10',
       't55g40m20', 't55g40m30', 't55g40m40', 't55g40p05', 't55g45m00',
       't55g45m05', 't55g45m10', 't55g45m20', 't55g45m30', 't55g45m40',
       't55g45p05', 't55g50m00', 't55g50m05', 't55g50m10', 't55g50m20',
       't55g50m30', 't55g50m40', 't55g50p05', 't5777g44m00', 't5777g44m05',
       't5777g44m10', 't5777g44m20', 't5777g44m30', 't5777g44m40',
       't5777g44p05', 't60g35m00', 't60g35m05', 't60g35m10', 't60g35m20',
       't60g35m30', 't60g35m40', 't60g35p05', 't60g40m00', 't60g40m05',
       't60g40m10', 't60g40m20', 't60g40m30', 't60g40m40', 't60g40p05',
       't60g45m00', 't60g45m05', 't60g45m10', 't60g45m20', 't60g45m30',
       't60g45m40', 't60g45p05', 't65g40m00', 't65g40m05', 't65g40m10',
       't65g40m20', 't65g40m30', 't65g40m40', 't65g40p05', 't65g45m00',
       't65g45m05', 't65g45m10', 't65g45m20', 't65g45m30', 't65g45m40',
       't65g45p05', 't70g45m00', 't70g45m05', 't70g45m10', 't70g45m20',
       't70g45m30', 't70g45m40', 't70g45p05']

list_sim_exists = []

for sim in list_sim:
	print '    ' + sim + '/' + version + ' exists...',
	try:
		os.chdir(PATH_TO_GRID + '/' + sim + '/' + version)
		print '%syes%s' % (GREEN, NO_COLOR),
	except:
		print '%sno%s' % (RED, NO_COLOR),

	print '    ' + sim + version[1:] + '.dx exists...',
	l = glob.glob('*dx')
	if sim+version[1:] + '.dx' in l:
		print '%syes%s' % (GREEN, NO_COLOR)
	elif len(l) > 0:
		print '%searlier version%s' % (YELLOW, NO_COLOR)
	else:
		print '%sno%s' % (RED, NO_COLOR)

	print '    ' + sim + version[1:] + '.chk exists...',
	l = glob.glob('*chk')
	if sim+version[1:] + '.chk' in l:
		print '%syes%s' % (GREEN, NO_COLOR),
	elif len(l) > 0:
		print '%searlier version%s' % (YELLOW, NO_COLOR),
	else:
		print '%sno%s' % (RED, NO_COLOR),

	print '    ' + sim + version[1:] + '.msh exists...',
	l = glob.glob('*msh')
	if sim+version[1:] + '.msh' in l:
		print '%syes%s' % (GREEN, NO_COLOR)
	elif len(l) > 0:
		print '%searlier version%s' % (YELLOW, NO_COLOR)
	else:
		print '%sno%s' % (RED, NO_COLOR)

	print '    ' + sim + version[1:] + '*.hav exists...',
	l = glob.glob('*hav')
	if len(l) == 0:
		print '%sno%s' % (RED, NO_COLOR),
	else : 
		print '%syes%s' % (GREEN, NO_COLOR),

	print '    ' + sim + version[1:] + '*.dat exists...',
	l = glob.glob('*dat')
	if len(l) == 0:
		print '%sno%s' % (RED, NO_COLOR)
	else : 
		print '%syes%s' % (GREEN, NO_COLOR)
	print
    os.chdir('../..')
