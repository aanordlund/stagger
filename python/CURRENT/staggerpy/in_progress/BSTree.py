#!/usr/bin/env python
# coding: utf-8

###################################################################
##    AUTHOR: Louis Manchon                                      ##
##    ORGN: Århus University, Fysik og Astronomi                 ##
##    FILE: BSTree.py                                            ##
##    DATE: May 2016                                             ##
###################################################################

from const import *
import numpy as np
import matplotlib.pyplot as plt
import math
import h5py
import os


class CNode:
    def __init__(self, val):
        self.l = None
        self.r = None
        self.v = val

def mix(a): 
    	l = []
    	n = len(a)
    	arr = range(n)
    	for i in range(n):
    		b = randint(0, n-i)
    		l.append(a[arr[b]])
    		del arr[b]
    	return l


def convert_bst(f):
	#path_to_root = path #path to the group that will CONTAINS the root
	sim = f['md'].keys()

	for i in sim:
		l = mix(f['md/'+i].keys())
		grp_root = f.create_group(l[0]+'_')
		f['md/'+i+'/'+l[0]+'_/'+l[0]] = f['md/'+i+'/'+l[0]]
		del f['md/'+i+'/'+l[0]]
		f['md/'+i+'/'+l[0]+'/'+l[0]] = f['md/'+i+'/'+l[0]+'_/'+l[0]]
		del f['md/'+i+'/'+l[0]+'_/'+l[0]]
		name_root = l[0]
		root = f['md/'+i+'/'+l[0]]
		root.attrs['l'] = ''
		root.attrs['v'] = str(name_root)
		root.attrs['r'] = ''
		del l[0]
		del f['md/'+i+'/'+name_root+'_']
	for j in l:
		insert(f, j, 'md/'+i+'/'+name_root, root, 'md/'+i)




def insert(f, a, path_to_node, node, path_to_root, sim):
	"""
	Inputs : 
		f 				hdf5 file

		a 				string
						name of the dataset
	
		path_to_node 	string
						path to the node
	
		node 			hdf5 group
						Node

		path_to_root	string
						path to the tree's root
	"""
	
	for i in sim
		if a < f[path_to_node].attrs['v']:
			if f[path_to_node].attrs['l'] == '' :
				f[path_to_node].create_group(a) # create left child
				f[path_to_node].attrs['l']=str(a)
				f[path_to_node+'/'+a].attrs['l'] = ''
				f[path_to_node+'/'+a].attrs['v'] = str(a)
				f[path_to_node+'/'+a].attrs['r'] = ''
				f[path_to_node+'/'+a+'/'+a] = f[path_to_root+'/'+a]
				del f[path_to_root+'/'+a]
			else:
				insert(f, a, path_to_node+'/'+f[path_to_node].attrs['l'], f[path_to_node][f[path_to_node].attrs['l']], path_to_root, sim)
	
		else :
			if f[path_to_node].attrs['r'] == '' :
				f[path_to_node].create_group(a) #create left child
				f[path_to_node].attrs['r']= str(a)
				f[path_to_node+'/'+a].attrs['l'] = ''
				f[path_to_node+'/'+a].attrs['v'] = str(a)
				f[path_to_node+'/'+a].attrs['r'] = ''
				f[path_to_node+'/'+a+'/'+a] = f[path_to_root+'/'+a]
				del f[path_to_root+'/'+a]
			else:
				insert(f, a, path_to_node+'/'+f[path_to_node].attrs['r'], f[path_to_node][f[path_to_node].attrs['r']], path_to_root, sim)


