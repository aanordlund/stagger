#!/usr/bin/env python
# coding: utf-8

###################################################################
##    AUTHOR: Louis Manchon                                      ##
##    ORGN: Århus University, Fysik og Astronomi                 ##
##    FILE: cvrt_sav_txt.py                                      ##
##    DATE: May 2016                                             ##
###################################################################

from operator import itemgetter
from scipy import interpolate
from const import *
import numpy as np
import matplotlib.pyplot as plt
import fnmatch
import math
import os
import idlsave

def convert(file):

    s = idlsave.read(file)
    txt = open(file[:-3]+'txt', 'w')    

    print "Converting...%s" % file,
    for i in s.mmd:
        line = '#'+' ; '+i[0]+' ; '+i[1]+' ; '+i[2]+' ; '+i[3]      #indices from 0 to 3
        txt.write(line+'\n')

        line = ''+str(round(i[4],12))
        for j in range(5,14):		#indices from 5 to 13
            line+=' ; '+str(round(i[j],12))
        txt.write(line+'\n')

        line = i[14]+' ; '+i[15]+' ; '+i[16]        #indices from 14 to 16
        txt.write(line+'\n')
        
        line = ''+str(round(i[17],12))
        for j in range(18,28):		#indices from 18 to 27
            line+=' ; '+str(round(i[j],12))
        txt.write(line+'\n')  
         
        
        line = ''+str(round(i[28][0],12))
        for j in range(1,51):		#index 28
        	line+=' ; '+str(round(i[28][j],12))
        txt.write(line+'\n')  
        
        line = ''+str(round(i[29][0],12))
        for j in range(1,51):		#index 29
        	line+=' ; '+str(round(i[29][j],12))
        txt.write(line+'\n')  

        txt.write(str(round(i[30],12))+'\n')  #index 30
        
        line = ''+str(round(i[31][0],12))
        for j in range(1,101):		#index 29
        	line+=' ; '+str(round(i[31][j],12))
        txt.write(line+'\n')  
        
        line = ''+str(round(i[32],12))
        for j in range(33,41):		#indices from 32 to 42
            line+=' ; '+str(round(i[j],12))
        txt.write(line+'\n')  
        
        line = ''+str(round(i[41],12))
        for j in range(42,51):		#indices from 42 to 50
            line+=' ; '+str(round(i[j],12))
        txt.write(line+'\n') 

        for j in range(51, 232):	#indices from 51 to 231
            line = ''+str(round(i[j][0],12))
            for k in range(1, 101):		
            	line+=' ; '+str(round(i[j][k],12))
            txt.write(line+'\n')  
        
        line = ''+str(round(i[232],12))
        for j in range(233,242):		#indices from 232 to 241
            line+=' ; '+str(round(i[j],12))
        txt.write(line+'\n') 
        
        line = ''+str(round(i[242],12))
        for j in range(243,252):		#indices from 242 to 251
            line+=' ; '+str(round(i[j],12))
        txt.write(line+'\n') 
        
        line = ''+str(round(i[252],12))
        for j in range(253,262):		#indices from 252 to 261
            line+=' ; '+str(round(i[j],12))
        txt.write(line+'\n') 

        txt.write(str(round(i[262],12))+'\n')  #index 262
        
        line = ''+str(round(i[263][0],12))
        for j in range(1,101):		#index 263
        	line+=' ; '+str(round(i[263][j],12))
        txt.write(line+'\n')  
        
        line = ''+str(round(i[264],12))
        for j in range(265,274):		#indices from 264 to 273
            line+=' ; '+str(round(i[j],12))
        txt.write(line+'\n')
        
        line = ''+str(round(i[274],12))
        for j in range(275,284):		#indices from 274 to 283
            line+=' ; '+str(round(i[j],12))
        txt.write(line+'\n')
        
        line = ''+str(round(i[284],12))
        for j in range(285,293):		#indices from 284 to 292
            line+=' ; '+str(round(i[j],12))
        txt.write(line+'\n')

        for j in range(293, 300):	#indices from 293 to 299
            line = ''+str(round(i[j][0],12))
            for k in range(1, 101):	
            	line+=' ; '+str(round(i[j][k],12))
            txt.write(line+'\n')  
        
        line = ''+str(round(i[300],12))
        for j in range(301,306):		#indices from 300 to 305
            line+=' ; '+str(round(i[j],12))
        txt.write(line+'\n')
    txt.close()  
    print "%s[Done]%s" % (GREEN, NO_COLOR)
