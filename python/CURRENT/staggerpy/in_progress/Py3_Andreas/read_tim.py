#!/anaconda/bin/which 
# coding: utf-8

###################################################################
##    AUTHOR: Louis Manchon                                      ##
##    ORGN: Ãrhus University, Fysik og Astronomi                 ##
##    FILE: read_tim.py                                          ##
##    DATE: June 2016                                            ##
###################################################################

from const import *
import numpy as np
from scipy import interpolate
import math, h5py, os, struct


def read_tim(name, endian = '>'):
		"""
		Read mesh file

		Error : 

		Raise IOError if value from .dx differ from those of .msh.
		"""

		f = open(name,'rb') 

		for i in range(100):
			tvoid, dtvoid = np.fromfile(f, dtype = endian + 'f4', count = 2) 
			print('tvoid:', tvoid, 'dtvoid:', dtvoid)
			seedvoid, isnap = np.fromfile(f, dtype = endian + 'i4', count = 2)
			print('seedvoid:', seedvoid, 'isnap:', isnap) 


def edit_tim(name, endian = '>'):
		"""
		Read mesh file

		Error : 

		Raise IOError if value from .dx differ from those of .msh.
		"""

		f = open(name,'rb') 
		fileobj = open(name[:-4] +'_2.tim', mode='wb')

		np.fromfile(f, dtype = endian + 'f4', count = 2).tofile(fileobj) 
		np.fromfile(f, dtype = endian + 'i4', count = 2)
		np.array([-77, 94], dtype=np.int32).tofile(fileobj) 

		for i in range(94):
			np.fromfile(f, dtype = endian + 'f4', count = 2).tofile(fileobj)
			np.fromfile(f, dtype = endian + 'i4', count = 2).tofile(fileobj)
			

		fileobj.close()

