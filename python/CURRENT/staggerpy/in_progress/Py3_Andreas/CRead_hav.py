#!/anaconda/bin/python
# coding: utf-8

"""
@AUTHOR: Louis Manchon                             
@ORGN: Ãrhus University, Stellar Astrophysic Center
@FILE: CRead_hav.py                                   
@DATE: June 2016                                   
"""

__metaclass__ = type

class CRead_hav:
	"""
	Read the ASCII file ``.hav``.
	"""
	
	def __init__(self, file):
		"""
		Parameter
		---------
		file : string
			Name of the ``.dat`` file.
		"""

		self.file_name = file 
		self.read_hav()

	def read_hav(self):
		self.file = open(self.file_name, 'r')
		line = self.file.readline()
		line = self.file.readline()
		self.isnap = int(line.split()[2][:-1])
		line = self.file.readline()
		self.ndep = int(line.split()[2][:-1])
		line = self.file.readline()
		self.t = float(line.split()[2][:-1])
		line = self.file.readline()
		self.dt = float(line.split()[2][:-1])
		line = self.file.readline()
		self.nghost = float(line.split()[2][:-1])
		line = self.file.readline()
		self.uur = float(line.split()[2][:-1])
		line = self.file.readline()
		self.uul = float(line.split()[2][:-1])
		line = self.file.readline()
		self.uut = float(line.split()[2][:-1])
		line = self.file.readline()
		self.teff = float(line.split()[2][:-1])
		line = self.file.readline()
		self.gy = float(line.split()[2][:-1])
		line = self.file.readline()
		self.lnrbot = float(line.split()[2][:-1])
		line = self.file.readline()
		self.eebot = float(line.split()[2][:-1])
		line = self.file.readline()
		self.lnravbot = float(line.split()[2][:-1])
		line = self.file.readline()
		self.eeavbot = float(line.split()[2][:-1])
		line = self.file.readline()
		self.uxcm = float(line.split()[2][:-1])
		line = self.file.readline()
		self.uycm = float(line.split()[2][:-1])
		line = self.file.readline()
		self.uzcm = float(line.split()[2][:-1])

		line = self.file.readline()
		line = self.file.readline()

		self.ym = [] ; self.ymdn = [] ; self.rho = [] ; self.e = [] ; self.lnr = [] ; self.ee = [] ; self.tt = []
		self.P = [] ; self.rhomin = [] ; self.rhomax = [] ; self.eemin = [] ; self.eemax = [] ; self.ttmin = []
		self.ttmax = [] ; self.Pmin = [] ; self.Pmax = [] ; self.Pel = [] ; self.alphaR = [] ; self.alphaP = []
		self.alpha = [] ; self.px = [] ; self.py = [] ; self.pz = [] ; self.ux = [] ; self.uy = [] ; self.uz = []
		self.pyup = [] ; self.pydn = [] ; self.ffpyup = [] ; self.ffpydn = [] ; self.fvisc = [] ; self.fkin = []
		self.fconv = [] ; self.fei = [] ; self.facous = [] ; self.frad = [] ; self.qq = [] ; self.qqr = []

		for i in range(self.ndep):
			line = self.file.readline()
			words = line.split()
			self.ym.append(float(words[1]))
			self.ymdn.append(float(words[2]))
			self.rho.append(float(words[3]))
			self.e.append(float(words[4]))
			self.lnr.append(float(words[5]))
			self.ee.append(float(words[6]))
			self.tt.append(float(words[7]))
			self.P.append(float(words[8]))

		line = self.file.readline()
		for i in range(self.ndep):
			line = self.file.readline()
			words = line.split()
			self.rhomin.append(float(words[1]))
			self.rhomax.append(float(words[2]))
			self.eemin.append(float(words[3]))
			self.eemax.append(float(words[4]))
			self.ttmin.append(float(words[5]))
			self.ttmax.append(float(words[6]))
			self.Pmin.append(float(words[7]))
			self.Pmax.append(float(words[8]))

		line = self.file.readline()
		for i in range(self.ndep):
			line = self.file.readline()
			words = line.split()
			self.Pel.append(float(words[1]))
			self.alphaR.append(float(words[2]))
			self.alphaP.append(float(words[3]))
			self.alpha.append(float(words[4]))
		
		line = self.file.readline()
		for i in range(self.ndep):
			line = self.file.readline()
			words = line.split()	
			self.px.append(float(words[1]))
			self.py.append(float(words[2]))
			self.pz.append(float(words[3]))
			self.ux.append(float(words[4]))
			self.uy.append(float(words[5]))
			self.uz.append(float(words[6]))
			self.pyup.append(float(words[7]))
			self.pydn.append(float(words[8]))
			self.ffpyup.append(float(words[9]))
			self.ffpydn.append(float(words[10]))

		line = self.file.readline()
		for i in range(self.ndep):
			line = self.file.readline()
			words = line.split()
			self.fvisc.append(float(words[1]))
			self.fkin.append(float(words[2]))
			self.fconv.append(float(words[3]))
			self.fei.append(float(words[4]))
			self.facous.append(float(words[5]))
			self.frad.append(float(words[6]))
			self.qq.append(float(words[7]))
			self.qqr.append(float(words[8]))
			
