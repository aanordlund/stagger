#__________________________________________________________________________________________________
#Extract frequencies: 

import numpy.fft as fft
data
spectrum = fft.fft(data)
freq = fft.fftfreq(len(spectrum))
plot(freq, abs(spectrum))


#__________________________________________________________________________________________________
#Spectre

def spect(i, l):
    data = l[i]
    spectrum = fft.fft(data)
    freq = fft.fftfreq(len(spectrum))
    plot(freq, abs(spectrum))

def spect(l):
    data = l
    spectrum = fft.fft(data)
    freq = fft.fftfreq(len(spectrum))
    plot(freq, abs(spectrum))


def spect_ir_samp(x, y, num = 50):
	x_data = np.asfarray( x, dtype='float64' )
	y_data = np.asfarray( y, dtype='float64' )
	f = np.linspace(0.01, 1, num = num)
	pgram = signal.lombscargle(x_data, y_data, f)
	plt.plot(f, np.sqrt(4*(pgram/x_data.shape[0])))

#__________________________________________________________________________________________________
#Peak detection

import numpy as np
from detect_peaks import detect_peaks
cb = np.array([-0.010223, ... ])
indexes = detect_peaks(cb, mph=0.04, mpd=100)



def getl_havn(sims, vars, mean = False):
	"""
	mean			boolean or list
					default False
					if True : compute the average for all the variables i vars
					if list of bool (for instance [0, 1, 1, 1, 0, ...]) : compute only the needed averages
	"""
	list_var = []
	for i in vars:
		list_var.append([])
	for i in sims:
		os.chdir(path_to_stagger_grid+'/'+i)
		mdl = CModelS('all_'+i+'v05.havn.hdf5')
		for i in list_var:
			i.append([])
		j=0
		while 'md/'+i+'_'+str(j) in mdl.file:
			for i in range(len(var)):
				if type(mean) == list:
					for k in mean :
						if k :
							list_var[i][-1].append(mean(mdl.get_havn(vars[i], j)))
						else : 
							list_var[i][-1].append(mdl.get_havn(vars[i], j))
				elif mean:
					list_var[i][-1].append(mean(mdl.get_havn(vars[i], j)))
				else:
					 list_var[i][-1].append(mdl.get_havn(vars[i], j))
			j+=1
		print i 
		os.chdir('..')



#__________________________________________________________________________________________________
#read direct-output binary file

rho=[]
fin = open("t45g25p0505_00000.dat", "rb")
for i in range(240):
    print "i = ", i
    rho.append([])
    for j in range(240):
        rho[-1].append([])
        for k in range(240):
            rho[-1][-1].append([])
            rho[-1][-1][-1].append(struct.unpack('<f', fin.read(4)))



struct.unpack('>240f', fin.read(4*240))



import time  

start_time = time.time()
rho=[]
fin = open("t45g25p0505_00000.dat", "rb")
for i in range(240):
    print "i = ", i
    rho.append([])
    for j in range(240):
        rho[-1].append(list(struct.unpack('>240f', fin.read(4*240))))
fin.close()
print time.time() - start_time



import time  

start_time = time.time()
rho=[]
k = 240*240
fmt = '>'+str(k)+'f'
fin = open("t45g25p0505_00000.dat", "rb")
for i in range(240):
    print "i = ", i
    rho.append(np.reshape(struct.unpack(fmt, fin.read(4*k)), (240,240)))
fin.close()
print time.time() - start_time

#__________________________________________________________________________________________________
#Plot 2D

plt.imshow(x,vmin = -15, vmax=sing, cmap=cm.jet, interpolation='nearest')
filename = 'diffus_'+str('%04d' %(k/100))+'.png'
plt.savefig(filename)
clf()

#__________________________________________________________________________________________________
#Change color cycle

from matplotlib import colors
from cycler import cycler
mpl.rcParams['axes.color_cycle'] = ['coral', 'limegreen', 'c']	


#__________________________________________________________________________________________________
# Linear interpolation
# Find nearest grid point
idx = grid[:,0,0].searchsorted(value)
upper = grid[idx,0,0]
lower = grid[idx - 1, 0, 0]
s = (value - lower) / (upper - lower)
result = (1-s) * data[idx - 1, :, :] + s * data[idx, :, :]



cube = np.transpose(y[:, None, None] * zx, (1, 0, 2))

plane = np.transpose(x * y[:, None], (1, 0))





x = np.ones((240))
y = np.linspace(-5., 5., 101)
plane = np.transpose(x * y[:, None], (1, 0))
idx = ltaur[5:235, 5:235, 0].flatten().searchsorted(plane.flatten())


#__________________________________________________________________________________________________
# Linear interpolation of a phisical quantity.
#How to get a slice at a given depth ? 
depth = 0.
idy = mdl.ym.searchsorted(depth)
ttu = mdl.tt[:, idy, :]
ttd = mdl.tt[:, idy + 1, :]
s = - ttd / (ttu - ttd)
ttn = (1 - s) * ttu + s * ttd

#__________________________________________________________________________________________________
#Plots with correct axis

plt.imshow(inter(-5.), cmap = plt.get_cmap('gist_heat_r'), interpolation='bicubic', extent = [mdl.xm[0], mdl.xm[-1], mdl.zm[0], mdl.zm[-1]])
plt.colorbar()

#__________________________________________________________________________________________________
#Subplots intensity
 sim = 't45g25p05'
os.chdir(sim)
mdl = CSnapsh(sim+'05_00000', sim, read = True)
plt.subplot(2, 2, 1)
plt.imshow(mdl.tt[:, 0, :], cmap = plt.get_cmap('gist_heat'), interpolation='nearest', extent = [mdl.xm[0], mdl.xm[-1], mdl.zm[0], mdl.zm[-1]])
plt.title(r"$T_{\rm eff} = 4~500 K$, $\log g = 2.5$, $\rm [Fe/H] = +0.5$", fontsize = 15)
plt.colorbar()
os.chdir('..')
del mdl

sim = 't5777g44m00'
os.chdir(sim)
mdl = CSnapsh(sim+'05_00000', sim, read = True)
plt.subplot(2, 2, 2)
plt.imshow(mdl.tt[:, 0, :], cmap = plt.get_cmap('gist_heat'), interpolation='nearest', extent = [mdl.xm[0], mdl.xm[-1], mdl.zm[0], mdl.zm[-1]])
plt.title(r"$T_{\rm eff} = 5~777 K$, $\log g = 4.4$, $\rm [Fe/H] = +0.$", fontsize = 15)
plt.colorbar()
os.chdir('..')
del mdl

sim = 't65g45m30'
os.chdir(sim)
mdl = CSnapsh(sim+'05_00000', sim, read = True)
plt.subplot(2, 2, 3)
plt.imshow(mdl.tt[:, 0, :], cmap = plt.get_cmap('gist_heat'), interpolation='nearest', extent = [mdl.xm[0], mdl.xm[-1], mdl.zm[0], mdl.zm[-1]])
plt.title(r"$T_{\rm eff} = 6~500 K$, $\log g = 4.5$, $\rm [Fe/H] = -3.$", fontsize = 15)
cbar = plt.colorbar()
cbar.set_label(r"Emergent intensity $I \times 10^{12}~\lbrack\mbox{erg}\cdot\mbox{s}^{-1}\cdot\mbox{cm}^{-2}\rbrack$", labelpad=20, y=1.)
plt.xlabel(r"$\textnormal{Cube size in X-direction } \times 10^8~\lbrack\mbox{cm}\rbrack$", fontsize = 16)
plt.ylabel(r"$\textnormal{Cube size in Y-direction } \times 10^8~\lbrack\mbox{cm}\rbrack$", fontsize = 16)
os.chdir('..')
del mdl

sim = 't55g30m10'
os.chdir(sim)
mdl = CSnapsh(sim+'05_00000', sim, read = True)
plt.subplot(2, 2, 4)
plt.imshow(mdl.tt[:, 0, :], cmap = plt.get_cmap('gist_heat'), interpolation='nearest', extent = [mdl.xm[0], mdl.xm[-1], mdl.zm[0], mdl.zm[-1]])
plt.title(r"$T_{\rm eff} = 5~500 K$, $\log g = 3.$, $\rm [Fe/H] = -1.$", fontsize = 15)
plt.colorbar()
os.chdir('..')
del mdl

#__________________________________________________________________________________________________
#Subplots Velocity field

sim = 't45g25p05'
os.chdir(sim)
mdl = CSnapsh(sim+'05_00000', sim, read = True)
plt.subplot(2, 2, 1)
plt.imshow(np.transpose(mdl.get_vel_cube(1)[:, :, 100], (1, 0)), cmap = plt.get_cmap('seismic'), interpolation='bicubic', extent = [mdl.xm[0], mdl.xm[-1], mdl.ym[-1], mdl.ym[0]])
plt.title(r"$T_{\rm eff} = 4~500$, $\log g = 2.5$, $\rm [Fe/H] = +0.5$")
plt.colorbar()
os.chdir('..')
del mdl

sim = 't5777g44m00'
os.chdir(sim)
mdl = CSnapsh(sim+'05_00000', sim, read = True)
plt.subplot(2, 2, 2)
plt.imshow(np.transpose(mdl.get_vel_cube(1)[:, :, 100], (1, 0)), vmin = -1., vmax = -1., cmap = plt.get_cmap('seismic'), interpolation='bicubic', extent = [mdl.xm[0], mdl.xm[-1], mdl.ym[-1], mdl.ym[0]])
plt.title(r"$T_{\rm eff} = 5~777$, $\log g = 4.4$, $\rm [Fe/H] = +0.$")
plt.colorbar()
os.chdir('..')
del mdl

sim = 't65g45m30'
os.chdir(sim)
mdl = CSnapsh(sim+'05_00000', sim, read = True)
plt.subplot(2, 2, 3)
plt.imshow(np.transpose(mdl.get_vel_cube(1)[:, :, 100], (1, 0)), cmap = plt.get_cmap('seismic'), interpolation='bicubic', extent = [mdl.xm[0], mdl.xm[-1], mdl.ym[-1], mdl.ym[0]])
plt.title(r"$T_{\rm eff} = 6~500$, $\log g = 4.5$, $\rm [Fe/H] = -3.$")
plt.colorbar()
os.chdir('..')
del mdl

sim = 't55g30m10'
os.chdir(sim)
mdl = CSnapsh(sim+'05_00000', sim, read = True)
plt.subplot(2, 2, 4)
plt.imshow(np.transpose(mdl.get_vel_cube(1)[:, :, 100], (1, 0)), cmap = plt.get_cmap('seismic'), interpolation='bicubic', extent = [mdl.xm[0], mdl.xm[-1], mdl.ym[-1], mdl.ym[0]])
plt.title(r"$T_{\rm eff} = 5~500$, $\log g = 3.$, $\rm [Fe/H] = -1.$")
plt.colorbar()
os.chdir('..')
del mdl
