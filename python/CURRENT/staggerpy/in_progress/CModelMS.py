#!/usr/bin/env python
# coding: utf-8

###################################################################
##    AUTHOR: Louis Manchon                                      ##
##    ORGN: Århus University, Fysik og Astronomi                 ##
##    FILE: CModelMS.py                                          ##
##    DATE: May 2016                                             ##
###################################################################

from const import *
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import math, h5py, os



class CModelMS:
	"""
	Class that process data averaged files
	"""
	
	def __init__(self, file):
		"""
		Inputs
		file            string
						name of the *.hdf5 file
		
		For more details about the variables, see var.pdf or take a look to the attribute 'legend'.
		"""
		self.filename=file
		self.file = h5py.File(file, "r+") 
		if len(self.file.keys()) == 1 :
			self.model = self.file[self.file.keys()[0]]
		self.len = len(self.model.keys())

		self.md = []
		for i in range(self.len-3):
			self.md.append(self.model[self.file.attrs['model']+'_'+str(i)])
		self.md.append(self.model[self.file.attrs['model']+'-mean'])
		self.md.append(self.model['tmax'])
		self.md.append(self.model['tmin'])


		plt.rc('figure', figsize = [12,9])
		plt.rc('text', usetex=True)
		plt.rc('font', size= 20)
		plt.rc('savefig', dpi = 500)


	def __getitem__(self, var):
		i, v = var.split('/')
		a = self.md[int(i)][v][:]
		if len(a) == 1:
			return a[0]
		else : 
			return a

	def __len__(self):
		"""
		If the file is a all* file, return the nuber of snapshots stored in the files plus mean*, tmin and tmax.
		If it is a single snapshot, return the number of variables.
		"""
		return self.len

	def __iter__(self):
		"""
		You have to enter the full path in the file.
		"""
		for grp in self.model.iteritems():
	  		yield grp[1]


	def get_havn(self, var, snapshot = None, path=False):
		"""
		Extract values from *havn.hdf5 file. Use it if you want to extract many values at once.
		Inputs : 
			var 		string
						name of the variable you want. If you enter the full path, set path = True

			snapshot 	string or int or list of int
						name of the snapshot you want to study. You can extract the value of variable in several snapshots at once. 
						For that, enter a list containing the indices of the snapshots you want. 
						If want to study the the mean model or tmin or tmax, enter 'mean', or 'tmin' or 'tmax'. Default: None.

			path 		bool
						True if the path is given in var. Default: False.
		"""
		if f.attrs['average'] != 'havn':
			raise ValueError('Not an havn file.') 
		else : 
			if path:
				path_to = var
			elif snapshot == None:
				path_to = self.model.keys()[0]
			elif snapshot == 'tmax': 
				path_to = snapshot
			elif snapshot == 'tmin':
				path_to = snapshot
			elif snapshot == 'mean':
				path_to = self.model.keys()[0]
			elif type(snapshot) == int:
				path_to = self.file.attrs['name'] + '_' + str(snapshot)
			elif snapshot == 'all':
				if not path:
					path_to = self.file.attrs['name'] + '_'
				else :
					raise ValueError("If the argument 'snapshot' is set to all, 'path' should be False.")
			else : 
				raise ValueError("The argument 'snapshot' could take the value:\nNone, 'tmax', 'tmin', 'mean', or a nonnegative integer.")

			l = []

			if path:
				if len(self.model[path_to]) > 1:
					for i in self.model[path_to]:
						l.append(i)
				else : 
					l = self.file[path_to][0]
			elif snapshot == 'all':
				if type(var) == str:
					for i in range(self.len-3):
						path_toto = path_to+str(i)
						if len(self.model[path_toto + '/' + var]) > 1:
							l.append(self.model[path_toto + '/' + var][:])
						else : 
							l.append(self.model[path_toto + '/' + var][0])
				elif type(var) == list: 
					for k in var:
						l.append([])
						grp = self.model[path_to+str(0)]
						if len(grp[k]) > 1:
							l[-1].append(grp[k][:])
						else : 
							l[-1].append(grp[k][0])
					lenvar = len(var)
					for i in range(self.len-3):
						for k in range(lenvar):
							grp = self.model[path_to+str(i)]
							if len(grp[var[k]]) > 1:
								l[k].append(grp[var[k]][:])
							else : 
								l[k].append(grp[var[k]][0])
				else :
					raise ValueError("The argument 'var' must be a string or a list of string.")
			elif type(var) == str:
				if len(self.model[path_to + '/' + var]) > 1:
					l.append(self.model[path_to + '/' + var][:])
				else : 
					l.append(self.model[path_to + '/' + var][0])
			elif type(var) == list: 
				for k in var:
					l.append([])
					if len(self.model[path_to + '/' + k]) > 1:
						l[-1].append(self.model[path_to + '/' + k][:])
					else : 
						l[-1].append(self.model[path_to + '/' + k][0])
			else :
				raise ValueError("The argument 'var' must be a string or a list of string.")
			return l 

	def mean(self, a):
		"""
		Compute the mean value of an array.
		Input:
			a 		list or array
		"""
		m = 0
		for i in a:
			m +=i
		return m/len(a)

	def close(self):
		self.file.close()

	def plot_hist2d(self, var1, var2, lim1=None, nbin1=256, lim2=None, nbin2=256,
		axis=None, bins=None, ctable='Normalize', beta=1., reverse=False):

		"""
		Plot the 2D histogram of two array.
		Inputs: 
			var1				nD array

			var2				nD array

			lim1				tuple (pair) of float
								Min and max values of var1 you want to plot.
								Default: lim1 = None (<=> all).

			nbin1 				int
								Number of bins.
								Default: nbin1 = 256.

			lim2 				tuple (pair) of float
								Min and max values of var2 you want to plot.
								Default: lim2 = None (<=> all).

			nbin2  				int
								Number of bins.

			axis				tuple of float (4 elements)
								If want not to display the axis, set axis = 'off'.
								Default: axis = None. 

			bins				tuple (pair) of int
								Default: bins = None.

			ctable				string
								Name of the color table. Default: ctable = 'Normalize'.

			beta				float
								Enhanced the contrast. Default: beta = 1. 

			reverse				bool
								Reverse the values of var2. Deflaut: reverse = False.

		"""

		if lim1 == None and lim2 == None:
			h1=var1
			h2=var2
			if bins == None:
				bins = [(max(var1) - min(var1)) / (nbin1-1)+1, (max(var2) - min(var2)) / (nbin2-1)+1]
		elif lim1 != None and lim2 != None:
			h1=[]
			h2=[]
			if bins == None:
				bins = [(lim1[1] - lim1[0]) / (nbin1-1)+1, (lim2[1] - lim2[0]) / (nbin2-1)+1]
			for i in range(len(h1)):
				h1.append((where(var1, lim1[0], lim1[1])-lim1[0])/(lim1[1]-lim1[0])*nbin1)
				h2.append((where(var2, lim2[0], lim2[1])-lim2[0])/(lim2[1]-lim2[0])*nbin2)
		elif lim1 == None : 
			h1=var1
			h2=[]
			if bins == None:
				bins = [(max(var1) - min(var1)) / (nbin1-1)+1, (lim2[1] - lim2[0]) / (nbin2-1)+1]
			for i in range(len(h1)):
				h2.append((where(var2, lim2[0], lim2[1])-lim2[0])/(lim2[1]-lim2[0])*nbin2)
		elif lim2 == None : 
			h2=var2
			h1=[]
			if bins == None:
				bins = [(lim1[1] - lim1[0]) / (nbin1-1)+1, (max(h2) - min(h2)) / (nbin2-1)+1]
			for i in range(len(h1)):
				h1.append((where(var1, lim1[0], lim1[1])-lim1[0])/(lim1[1]-lim1[0])*nbin1)
		else : 
			raise ValueError("The arguments 'lim1' and 'lim2' must be None or a list of 2 floats.")

		if axis == None : 
			pass
		else : 
			try :
				plt.axis(axis)
			except : 
				raise ValueError("If stated, the argument 'axis' must be a list of 4 elements or 'off'.")

		if beta != 1.:
			h2 = [i**beta for i in h2]

		if reverse :
			h2 =[max(h2) - i for i in h2]

		if ctable == 'Normalize':
			plt.hist2d(h1, h2, bins=bins, norm=Normalize())
		elif ctable == 'LogNorm':
			plt.hist2d(h1, h2, bins=bins, norm=LogNorm())
		elif ctable == 'BoundaryNorm':
			plt.hist2d(h1, h2, bins=bins, norm=BoundaryNorm())

		else : 
			raise ValueError("The argument 'ctable' can be: 'LogNorm', 'BoundaryNorm' or 'Normalize'.")


	def where(a, min, max):
		"""
		Return the indices of the elements of a that stand between min and max.
		"""
		for i in range(len(a)):
			if a[i] <= max and a[i] >= min :
				yield a[i]



