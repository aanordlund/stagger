#!/usr/bin/env python
# coding: utf-8

###################################################################
##    AUTHOR: Louis Manchon                                      ##
##    ORGN: Århus University, Fysik og Astronomi                 ##
##    FILE: CMeanSM.py                                           ##
##    DATE: May 2016                                             ##
###################################################################

from operator import itemgetter
from const import *
from scipy import interpolate
import numpy as np
import matplotlib.pyplot as plt
import fnmatch
import math
import os



class CMeanSM:
	"""
	Class that process data of a mean 3D model
	"""
	
	def __init__(self, file, read=True):
		"""
		Inputs
		file            string
						name of the file
						
		read            boolean
						if False, doesn't read the file

		Name of the variables are stored in var
						
		
		"""
		self.file=file

		if os.path.isfile(self.file): #check if the file exists
			self.path=os.getcwd()
			os.chdir(self.path)
			
		else : 
			raise IOError('*.txt file does not exists') 

		plt.rc('figure', figsize = [12,9])
		plt.rc('text', usetex=True)
		plt.rc('font', size= 20)
		plt.rc('savefig', dpi = 500)

	def fill_mdl(self, mdl):
		"""
		Fill an array with the data of a mean model. 
		Usually, it is used internally
		"""
		for j in self.words[1:]:           #store line 0
			mdl.append(j)    
		self.line=self.data.readline()        
		self.words=self.line.split(';')  

		for j in self.words:               #store line 1
			mdl.append(float(j))    
		self.line=self.data.readline()        
		self.words=self.line.split(';') 

		for j in self.words:               #store line 2
			mdl.append(j)      
		self.line=self.data.readline()        
		self.words=self.line.split(';') 

		for j in self.words:               #store line 3
			mdl.append(float(j))      
		self.line=self.data.readline()        
		self.words=self.line.split(';')       
		
		mdl.append([])
		for j in self.words:             #store line 4
			mdl[-1].append(float(j))
		self.line=self.data.readline()
		self.words=self.line.split(';')
		
		mdl.append([])
		for j in self.words:             #store line 5
			mdl[-1].append(float(j))
		self.line=self.data.readline()
		self.words=self.line.split(';')
			
		mdl.append(float(self.words[0]))   #store line 6    
		self.line=self.data.readline()  
		self.words=self.line.split(';')   
			
		mdl.append([])
		for j in self.words:             #store line 7
			mdl[-1].append(float(j))
		self.line=self.data.readline()
		self.words=self.line.split(';')
	
		for i in range(0, 2):       #store lines from 8 to 9
			for j in self.words:     
				mdl.append(float(j))      
			self.line=self.data.readline()        
			self.words=self.line.split(';') 
			
		for i in range(0, 181):
			mdl.append([])
			for j in self.words:             #store line from 10 to 190
				mdl[-1].append(float(j))
			self.line=self.data.readline()
			self.words=self.line.split(';')
		
		for i in range(0, 3):       #store lines from 191 to 193
			for j in self.words:     
				mdl.append(float(j))       
			self.line=self.data.readline()        
			self.words=self.line.split(';') 
		
		mdl.append(float(self.words[0]))   #store line 194   
		self.line=self.data.readline()  
		self.words=self.line.split(';')   
		
		mdl.append([])
		for j in self.words:             #store line 195
				mdl[-1].append(float(j))
		self.line=self.data.readline()
		self.words=self.line.split(';')   
			
		for i in range(0, 3):       #store lines from 196 to 198
			for j in self.words:     
				mdl.append(float(j))  
			self.line=self.data.readline()        
			self.words=self.line.split(';') 
		
		for i in range(0, 7):
			mdl.append([])
			for j in self.words:             #store line from 199 to 204
				mdl[-1].append(float(j))
			self.line=self.data.readline()
			self.words=self.line.split(';')
		for j in self.words:     
			mdl.append(float(j))       
		self.line=self.data.readline()         


 
	def read_mmd(self, mdl):
		"""
		Read data written in mmd_*.txt
		Inputs
		mdl            string
					   name of the model. Should be of the form "txxgyymzz"
					   where xx : temperature ; yy : log g ; zz : metallicity
		"""
		if os.path.isfile(self.file) :
			print "Reading...%s" % self.file,

			self.mdl=[]

			self.data=open(self.file, 'r')
			self.line=self.data.readline()
			while self.line :
				self.words=self.line.split(' ; ')
				if self.words[0] == '#':
					if self.words[1] == mdl:
						self.fill_mdl(self.mdl)
						break 
				self.line=self.data.readline()

			self.data.close()
			print "%s[Done]%s" % (GREEN, NO_COLOR) 
		else : 
			raise IOError("file"+self.file+" does not exist.")
			self.read_stag=True     #need to read stagger_*.txt 


	def read_all_mmd(self, mdl='all'):
		"""
		Read data written in mmd_*.txt
		Inputs
		mdl            list of string or string
					   list of the model. Should be of the form "txxgyymzz"
					   where xx : temperature ; yy : log g ; zz : metallicity.
					   default : "all"
		"""
		if os.path.isfile(self.file) :
			print "Reading...%s" % self.file,

			self.list_mdl=[]

			self.data=open(self.file, 'r')
			self.line=self.data.readline()
			if mdl == 'all' : 
				while self.line :
					self.words=self.line.split(' ; ')
					self.list_mdl.append([])
					self.fill_mdl(self.list_mdl[-1])
			else :
				for i in range(0, len(mdl)):
					self.list_mdl.append([])
				k = 1
				while self.line :
					self.words=self.line.split(' ; ')
					if self.words[0] == '#':
						if (self.words[1] in mdl ):
							self.fill_mdl(self.list_mdl[mdl.index(self.words[1])])
							if k == len(mdl): 
								break
							k+=1
						else : 
							self.line=self.data.readline()
						self.words=self.line.split(' ; ')
					else : 
						self.line=self.data.readline()

			self.data.close()
			print "%s[Done]%s" % (GREEN, NO_COLOR) 
		else : 
			raise IOError("file"+self.file+" does not exist.")
			self.read_stag=True     #need to read stagger_*.txt 

	def plot_mmd(self, x, y, modlim = None, style='-', color='', linewidth=1):
		"""
		plot column y against column x
		Inputs
		x               int
						index of the array in self.mdl

		y               int
						index of the array in self.mfl

		modlim          tuple of int
						limit of the subarray you want to plot
						default : None (plot the entire array)

		style           string
						style of the curve
						default : '-'

		color           string
						color of the curve
						default : ''

		linewidth       string
						thicckness
						default : 1
		"""

		xlabel=var[x]
		ylabel=var[y]

		if modlim == None :
			plt.plot(self.mdl[x], self.mdl[y], style+color, linewidth = linewidth)
		else :
			plt.plot(self.mdl[x][modlim[0]:modlim[1]], self.mdl[y][modlim[0]:modlim[1]], style+color, linewidth = linewidth)

		plt.xlabel(xlabel, fontsize=20)
		plt.ylabel(ylabel, fontsize=20)


	def plot_all_mmd(self, x, y, mdl='all', modlim = None, style='-', color='', linewidth=1):
		"""
		plot column y against column x
		Inputs
		x               int
						index of the array in self.mdl

		y               int
						index of the array in self.mfl

		mdl             list of string
						list of model you want to plot
						default : 'all'. If 'all', plot all the models store in
						self.list_mdl

		modlim          tuple of int
						limit of the subarray you want to plot
						default : None (plot the entire array)

		style           string
						style of the curve
						default : '-'

		color           string
						color of the curve
						default : ''

		linewidth       string
						thicckness
						default : 1
		"""

		
		xlabel=var[x]
		ylabel=var[y]

		if mdl=='all':
			for i in self.list_mdl:
				if modlim == None :
					plt.plot(i[x], i[y], style+color, linewidth = linewidth)
				else :
					plt.plot(i[x][modlim[0]:modlim[1]], i[y][modlim[0]:modlim[1]], style+color, linewidth = linewidth)
		else : 
			for i in self.list_mdl:
				if i[0] in mdl :
					if modlim == None :
						plt.plot(i[x], i[y], style+color, linewidth = linewidth)
					else :
						plt.plot(i[x][modlim[0]:modlim[1]], i[y][modlim[0]:modlim[1]], style+color, linewidth = linewidth)
					mdl.remove(i[0])


		plt.xlabel(xlabel, fontsize=20)
		plt.ylabel(ylabel, fontsize=20)



	def trilin_interp(self, x, y, z, x0, y0, z0, mdl):
		"""
		Compute a value of the model txxgyymzz by triliear interpolation.
		Usually, it is used internally.
		Inputs:
		x               float
						Teff

		y               float
						log g

		z 				float
						metallicity

		x0              float
						Teff0 (upper limit in the cube)

		y0              float
						log g0 (upper limit in the cube)

		z0 				float
						metallicity0 (upper limit in the cube)

		mdl 			values at the edge of the cube
		"""

		#mdl in an array of float
		if z < -1 : 
			xd = (x-x0)/500; yd = 2*(y-y0); zd = z-z0; 
			xxd = 1- xd; yyd = 1- yd; 
		else :
			xd = (x-x0)/500; yd = 2*(y-y0); zd = 2*(z-z0);
			xxd = 1- xd; yyd = 1- yd;
		c00 = mdl[0]*xxd+mdl[1]*xd
		c01 = mdl[3]*xxd+mdl[4]*xd
		c10 = mdl[2]*xxd+mdl[6]*xd
		c11 = mdl[5]*xxd+mdl[7]*xd

		c0 = c00*yyd + c10*yd
		c1 = c01*yyd + c11*yd

		return c0*(1-zd)+c1*zd



	def trilin_interp_arr(self, x, y, z, x0, y0, z0, list_mdl, index):
		#list_mdl is an array of models
		"""
		Compute a value of the model txxgyymzz by triliear interpolation.
		Usually, it is used internally.
		Inputs:
		x               float
						Teff

		y               float
						log g

		z 				float
						metallicity

		x0              float
						Teff0 (upper limit in the cube)

		y0              float
						log g0 (upper limit in the cube)

		z0 				float
						metallicity0 (upper limit in the cube)

		list_mdl 		array of models
						models at the edge of the cube

		index 			int
						index of the data we are interpolated (see var)
		"""
		n = len(list_mdl[0][index])
		array=[]
		for i in range(0, n):
			value = [list_mdl[0][index][i], list_mdl[1][index][i], list_mdl[2][index][i], list_mdl[3][index][i], list_mdl[4][index][i], list_mdl[5][index][i], list_mdl[6][index][i], list_mdl[7][index][i]]
			array.append(self.trilin_interp(x, y, z, x0, y0, z0, value))

		return array


	def trilin_interp_mmd(self, teff, logg, feh):
		"""
		Compute a value of the model txxgyymzz by triliear interpolation
		Inputs:
		teff               float

		logg               float

		feh 				float


		 	mdl[5]----------mdl[7]
		    /|			    /|
		   / |			   / |
		  /	 |			  /	 |
		 /	 |			 /	 |
		mdl[3]---------mdl[4]|
		|	mdl[2]------|---mdl[6]
		|	/			|	/
		|  /			|  /	
		| /				| /		
		|/				|/		
		mdl[0]---------mdl[1]
		"""


		if (teff > 3999 and teff < 7001) and (logg > 1.49 and logg < 5.01) and (feh > -4.01 and feh < 0.51) :

			t = int(teff/1000)*1000 + 500
			if t < teff:
				teff0 = t
				teff1 = t + 500
			else : 
				teff0 = t - 500
				teff1 = t

			lg = int(logg) + 0.5
			if lg < logg : 
				logg0 = lg
				logg1 = lg + 0.5
			else : 
				logg0 = lg - 0.5
				logg1 = lg 

			if feh < -1.0:
				f = int(feh)
				feh0 = f
				feh1 = f + 1.0
			else : 
				f = int(feh) + 0.5
				if f < feh:
					feh0 = f
					feh1 = f + 0.5
				else : 
					feh0 = f - 0.5
					feh1 = f
		else : 
			raise ValueError('teff should lies between 4000 and 7000. \nlogg should lies between 1.5 and 5.0. \nfeh should lies between -4.0 and 0.5.')
		
		mdl=['t'+str(int(teff0/100))+'g'+str(int(logg0*10))+'m%02d' % (int(feh0*10)), 
		't'+str(int(teff1/100))+'g'+str(int(logg0*10))+'m%02d' % (int(feh0*10)),
		't'+str(int(teff0/100))+'g'+str(int(logg1*10))+'m%02d' % (int(feh0*10)),
		't'+str(int(teff0/100))+'g'+str(int(logg0*10))+'m%02d' % (int(feh1*10)),
		't'+str(int(teff1/100))+'g'+str(int(logg0*10))+'m%02d' % (int(feh1*10)),
		't'+str(int(teff0/100))+'g'+str(int(logg1*10))+'m%02d' % (int(feh1*10)),
		't'+str(int(teff1/100))+'g'+str(int(logg1*10))+'m%02d' % (int(feh0*10)),
		't'+str(int(teff1/100))+'g'+str(int(logg1*10))+'m%02d' % (int(feh1*10))]

		self.interp_mdl=[]
		self.interp_mdl.append('t'+str(int(teff))+'g'+str(int(logg*10))+'m'+str(int(feh*100)))
		self.interp_mdl.append('')
		self.interp_mdl.append('')
		self.interp_mdl.append('')
		self.interp_mdl.append(None)
		self.interp_mdl.append(None)
		self.interp_mdl.append(None)
		self.interp_mdl.append(teff)
		self.interp_mdl.append(teff)
		self.interp_mdl.append(logg)
		self.interp_mdl.append(feh)

		self.read_all_mmd(mdl = mdl)

		for i in range(11, 28):
			self.interp_mdl.append(None)

		self.interp_mdl.append(self.trilin_interp_arr(teff, logg, feh, teff0, logg0, feh0, self.list_mdl, 28))
		self.interp_mdl.append(self.trilin_interp_arr(teff, logg, feh, teff0, logg0, feh0, self.list_mdl, 29))
		self.interp_mdl.append(None)
		self.interp_mdl.append(self.trilin_interp_arr(teff, logg, feh, teff0, logg0, feh0, self.list_mdl, 31))

		for i in range(32, 51):
			self.interp_mdl.append(None)

		for i in range(51, 232):
			self.interp_mdl.append(self.trilin_interp_arr(teff, logg, feh, teff0, logg0, feh0, self.list_mdl, i))

		for i in range(232, 263):
			self.interp_mdl.append(None)

		self.interp_mdl.append(self.trilin_interp_arr(teff, logg, feh, teff0, logg0, feh0, self.list_mdl, 263))

		for i in range(264, 293):
			self.interp_mdl.append(None)

		for i in range(293, 300):
			self.interp_mdl.append(self.trilin_interp_arr(teff, logg, feh, teff0, logg0, feh0, self.list_mdl, i))

		for i in range(300, 306):
			self.interp_mdl.append(None)

	def plot_mmd_interp(self, x, y, modlim = None, style='-', color='', linewidth=1, lattice = False):
			"""
			plot column y against column x of the interpolated model
			Inputs
			x               int
							index of the array in self.mdl
	
			y               int
							index of the array in self.mfl
	
			modlim          tuple of int
							limit of the subarray you want to plot
							default : None (plot the entire array)
	
			style           string
							style of the curve
							default : '-'
	
			color           string
							color of the curve
							default : ''
	
			linewidth       string
							thicckness
							default : 1

			lattice			boolean
							if True, plot the correspondig self.list_mdl values
							default : False
			"""
	
			xlabel=var[x]
			ylabel=var[y]
	
			if modlim == None :
				plt.plot(self.interp_mdl[x], self.interp_mdl[y], style+color, linewidth = linewidth)
			else :
				plt.plot(self.interp_mdl[x][modlim[0]:modlim[1]], self.interp_mdl[y][modlim[0]:modlim[1]], style+color, linewidth = linewidth)
			if lattice : 
				self.plot_all_mmd(x, y, modlim = modlim, style='--')

			plt.xlabel(xlabel, fontsize=20)
			plt.ylabel(ylabel, fontsize=20)



