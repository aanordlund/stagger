#README#

This README describes the structure of the ``stagger/python`` directory which contains various versions of the ``staggerpy`` package for the analysis of Stagger stellar-surface convection simulations. In the various ``staggerpy`` subfolders under this directory, you will find a ``README.md`` describing how to install ``staggerpy``.

The ``stagger/python`` directories includes the following subdirectories:
- ``doc`` :	contains main documentation and description of the code; the ``staggerpy.pdf`` document therein describes the latest STABLE release;

- ``STABLE`` : last stable release (only python-2.7 compatible);

- ``LEGACY`` : previous stable releases (only python-2.7 compatible);

- ``CURRENT`` : developement version (python-2.7/-3.5 compatible).

