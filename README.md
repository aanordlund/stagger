README
======

## Stagger repository: introduction
This is a brief guide to the `stagger` repository and its contents. Here below is a list of the main directories in the repository:

- `src`                 : main code (see `src/README` for additional info) 
- `src-simple`          : simple demo code, to illustrate the principles. Still, subroutines developed to work here should work with little or no change under `src`

- `src/OS`              : OS dependent files
- `src/OS/hosts`        : host dependent files
- `src/INITIAL`         : initial value functions and programs
- `src/FORCING`         : forcing functions
- `src/COOLING`         : cooling functions
- `src/EOS`             : equation of state routines

- `src/EXPERIMENTS`     : files for specific experiments, e.g. makefiles, input files, etc.

- `src/TESTS`           : tests; input data, scripts, IDL procedures for tests

- `idl`                 : IDL routines for analysis of Stagger data
- `python`              : Python routines
- `run`                : directory to run in, e.g. checked out with
- `docs`                : basic documentation

## Git

The official repository is now maintained exclusively through `git`. A simple user guide to the `git` version control system is available as a markdown file

	docs/git-howto.md

as well as on PDF:

	docs/git-howto.pdf

## Git commit codes
When committing changes to this repository, please use the following three-letter codes at the beginning of your commit messages:

* ADD: new file
* ORG: organisational, no changes to functionality
* DEV: code development (incl. additions and deletions)
* BUG: fixes to bugs with significant impact on previous work 
* FIX: fixes to bugs during development, with little impact on previous work
* SYN: corrections to syntax errors, typos, and misspellings
* CLN: code tidying-up
* OPT: optimisation
* DBG: debugging
* DIA: affects only diagnostics
* MIN: minimal impact on results
* DOC: documentation
* VIS: visualisation

## Compiling
To compile a default version of the code do

	make

or

	make PDE=pde_call

To compile a specific experiment (see below for experiment definitions) do:

	make EXPERIMENT=padoan

To work extensively with a certain experiment in a directory, edit the 
Makefile, changing "EXPERIMENT=pde" into "EXPERIMENT=whatever", allowing
subsequent compilations with "make".  Do NOT commit the Makefile.

To run a demo, do:

	make demo

To set options different from the defaults

	$EDITOR OS/$MACHTYPE.mkf

to set machine type specific options (if $MACHTYPE is not
defined, set it to `uname -s`).  To set host specific options

	$EDITOR OS/hosts/$HOST.mkf


## Snapshots: convert to new format
To convert an old convection code `.sav` file to new `.dat` format, do:

	vi init_mesh.pro for fin, fout names
	idl
	@init_mesh  ;(or modifications thereof)

## Tables:
To make a new EOS/opacity binning table, do:

	cd stagger-code/src/EOS/UMA directory
	vi cparam.inc for dimensions

You may need to edit `Makefile` to include `etime.o` among dependencies for `tabcmp.x`. This requires to copy `etime.c` or `etime.o` to `UMA` directory

	ln -s ../../params.mod .

Either:

	make install   puts executables in ~/bin
	rehash
  
which installs executables in `~/bin`, or 

	make gibin.x
	make cnoin.x
	make tabcmp.x
	make tabinv.x

which puts executables in current directory.

### Input tables
In `EOS/UMA`:
- `cnoin.x` makes `cnoin.dat -> cno.dat`
- `gibin.x` makes `gibin.dat -> odf.dat`
- `subs.dat` contains information about continuous opacities cross sections

To make a table: cd to run directory (containing snapshot and mesh file), then do:

	ln -s stagger-code_dir input   

(input tables are hard-linked to `input/EOS/UMA`)

	mv input/EOS/UMA/tab*.x .
	cp input/EOS/UMA/tab*.in .
	vi tab*.in for file names
	./tabcmp.x < tabcmp.in >! tabcmp.log
	./tabinv.x < tabinv.in >! tabinv.log

## Make
On SGI: replace `make` by `smake`.  On IBMs & SUNs: replace "make" by "gmake"
(GNUs make).  If gmake is not available, and the "sinclude" statements
in the src/Makefile don't work, replace them with "include" statements,
and make empty files (e.g. with "touch") where "make" is looking for
a file and does not find one.

Don't check in the edited Makefile on the main branch.  Either just keep
it, merging new changes from the repository, or else check it in a branch.

On sparc: empty modules need a dummy variable
On SGI: empty keywords in `EXPERIMENTS/*.mkf` need to be filled or have 
dependence removed from Makefile.dep

On SGI: ln -s OS/IRIX64.{f,F}90 and compile separately

	  f90 -all options -c -o OS/IRIX64.o OS/IRIX64.F90

On Sun: ln -s OS/sparc.{f,F}90 and compile separately

	  f90 -all options -c -o OS/sparc.o OS/sparc.F90

## EXPERIMENTS

To set up a combination of specific file for an experiment

	setenv EXPERIMENT myexperiment
	$EDITOR EXPERIMENTS/$EXPERIMENT

see for example

	EXPERIMENTS/torgny.mkf
	EXPERIMENTS/turbulence.mkf

Experiments are defined by

	INITIAL/$(INITIAL).f90
	FORCING/$(FORCING).f90
	FORCING/$(EXPLODE).f90
	COOLING/$(COOLING).f90

and are furthermore controlled by input parameters, as exemplified in

	TESTS/*.in

and

	EXPERIMENTS/torgny.in

Compilation based on the EXPERIMENT environment variable is with

	make -e

or else, setting the variable explicitly, with

	make EXPERIMENT=turbulence

For potential BC: one needs to compile `UTILTIES/fftpack.f` separately with all options and add it to the `.mkf` file:

	FFT = UTILITIES/fft2d.o UTILITIES/fftpack.o

## INPUTS

	params.f90:  namelist /io/iread,iwrite,nstep,nsnap,tstop,tsnap,tscr,nscr,file,from
	params.f90:  namelist /run/t,dt,Cdt,timeorder,iseed,idbg,do_trace
	params.f90:  namelist /grid/mx,my,mz,sx,sy,sz,lb,ub,omega
	params.f90:  namelist /vars/do_density,do_momenta,do_energy,do_mhd,do_pscalar
	params.f90:  namelist /pde/do_loginterp,do_2nddiv,do_dissipation,gamma,csound, &
	                nu1,nu2,nu3,nur,nuB,poff,t_Bgrowth,cmax,Csmag
	
	params_mesh.f90:  namelist /io/iread,iwrite,nstep,nsnap,nscr,file,from
	params_mesh.f90:  namelist /run/t,dt,Cdt,timeorder,iseed,idbg,do_trace
	params_mesh.f90:  namelist /grid/mx,my,mz,sx,sy,sz,lb,ub
	params_mesh.f90:  namelist /vars/do_density,do_momenta,do_energy,do_mhd,do_pscalar
	params_mesh.f90:  namelist /pde/do_loginterp,do_2nddiv,gamma,csound,nu1,nu2,nur,nuB,poff,t_Bgrowth,cmax
	
	params_omp.f90:  namelist /io/iread,iwrite,nstep,nsnap,nscr,file,from
	params_omp.f90:  namelist /run/t,dt,Cdt,timeorder,iseed,idbg,do_trace
	params_omp.f90:  namelist /grid/mx,my,mz,sx,sy,sz,lb,ub,omega
	params_omp.f90:  namelist /vars/do_density,do_momenta,do_energy,do_mhd,do_pscalar
	params_omp.f90:  namelist /pde/do_loginterp,do_2nddiv,do_dissipation,gamma,csound, &
	                nu1,nu2,nur,nuB,poff,t_Bgrowth,cmax,Csmag
	
	params_omp_IRIX64.f90:  namelist /io/iread,iwrite,nstep,nsnap,nscr,file,from
	params_omp_IRIX64.f90:  namelist /run/t,dt,Cdt,timeorder,iseed,idbg,do_trace
	params_omp_IRIX64.f90:  namelist /grid/mx,my,mz,sx,sy,sz,lb,ub,omega
	params_omp_IRIX64.f90:  namelist /vars/do_density,do_momenta,do_energy,do_mhd,do_pscalar
	params_omp_IRIX64.f90:  namelist /pde/do_loginterp,do_2nddiv,do_dissipation,gamma,csound, &
	                nu1,nu2,nur,nuB,poff,t_Bgrowth,cmax
	
	particles.f90:  namelist /part/ do_particles
	particles_fraction.f90:  namelist /part/ do_particles, fraction
	
	BOUNDARIES/alfvenwave.f90:  namelist /bdry/ u0,type
	BOUNDARIES/corona.f90:  namelist /bdry/ t_Bbdry
	BOUNDARIES/emerging_flux.f90:  namelist /bdry/ v_ampl, B_ampl, y_tube, U_tube, tubefile
	BOUNDARIES/open_y_reversed.f90:  namelist /bdry/ lb, ub, t_Bbdry, Bscale
	BOUNDARIES/open_y_reversed_mesh.f90:  namelist /bdry/ lb, ub, t_Bbdry, Bscale
	BOUNDARIES/stellar-atm_bdry.f90:  namelist /bdry/ lb, ub, t_bdry, t_Bbdry, Bscale, lb_eampl, htop, debug
	
	COOLING/ISM-cooling-Rosen-etal.f90:  namelist /cool/do_cool,do_table,do_power,Tlower,ma,mb,coolH2file
	COOLING/ISM-cooling.f90:  namelist /cool/do_cool,do_table,do_power,Tlower,ma,mb,mc,coolH2file
	COOLING/ISM.f90:  namelist /cool/do_cool,coolH2file
	COOLING/corona_cooling.f90:  namelist /cool/do_cool,do_chromos,do_corona,do_conduction,do_damp, &
	COOLING/global_disk_cooling.f90:  namelist /cool/do_cool,dedt0,t_cool,rho_cool,ee_cool,p_cool
	COOLING/newton2_cooling.f90:  namelist /cool/do_cool,t_cool,ee_cool,r_cool
	COOLING/newton_cooling.f90:  namelist /cool/do_cool,t_cool,ee_cool
	COOLING/nocooling.f90:  namelist /cool/do_cool
	
	COOLING/radiation_lookup.f90:  namelist /cool/do_cool, nmu, nphi, dphi, form_factor, dtaumin, dtaumax, intfile
	COOLING/radiation_lookup_spline_dtau.f90:  namelist /cool/do_cool, nmu, nphi, dphi, form_factor, dtaumin, dtaumax, intfile
	COOLING/radiation_simple.f90:  namelist /cool/do_cool, nmu, nphi, dphi, form_factor, intfile
	COOLING/radiation_vertical.f90:  namelist /cool/do_cool, form_factor, intfile
	COOLING/table.f90:  namelist /cool/do_cool, do_heat, do_table, do_power, Tlower, tablefile
	
	EOS/H+He-table-read.f90:  namelist /eqofst/ do_ionization, ymin, do_table, tablefile
	EOS/H+He-table.f90:    namelist /eqofst/ do_ionization, ymin, do_table, eezero, ma, mb
	EOS/H+He.f90:    namelist /eqofst/ do_ionization, ymin
	EOS/H+He.f90:    namelist /eqofst/ do_ionization, ymin
	EOS/eos_lookup.f90:  namelist /eqofst/ do_eos, do_ionization, do_table, tablefile
	EOS/ideal-gas.f90:    namelist /eqofst/ do_ionization
	EOS/pure-hydrogen.f90:    namelist /eqofst/ do_ionization
	
	FORCING/alexei.f90:    namelist /force/ do_force, input, mach, calib, t_damp
	FORCING/constant-gravity.f90:  namelist /force/ do_force, gx, gy, gz, ay, ky, ny, t_friction
	FORCING/disk_simple_gravity.f90:  namelist /force/grav,soft
	FORCING/fake-gravity.f90:  namelist /force/ do_force, gx, gy, gz, ay, ky, ny
	FORCING/global_disk.f90:    namelist /force do_selfg,isolated,do_friction,grav,soft,M_sun,r0,r1,r2,r3, &
	        omega,rho0,ee0,rho_friction,fbdry,frho,rho_lim,e_lim,p_sigma,p_temp
	FORCING/noexplode.f90:  namelist /expl/do_expl
	FORCING/noforce.f90:  namelist /force/ do_force
	FORCING/planetary_disk.f90:    namelist /force/do_force,M_sun,r0,r1,eps,rho_min,rho_max
	FORCING/planetary_disk2.f90:    namelist /force/do_selfg,grav,M_sun,r0,r1,r2,r3,omega,soft,rho0,ee0,ee1
	FORCING/selfgravity.f90:  namelist /selfg/ grav,soft,isolated
	FORCING/sn.f90:  namelist /expl/ &
	FORCING/stars.f90:  namelist /expl/ &
	FORCING/turbulence.f90:    namelist /force/ do_force, do_helmh, a_helmh, k1, k2, ampl_turb, mach, calib, t_turn, pk
	
	INITIAL/alexei-init.f90:  namelist /init/do_init,r0,e0,d0,b0
	INITIAL/central.f90:  namelist /init/do_init,do_test,r0,r1,rho0,ee0,d0,B0,w0,eps2,pow,pow2
	INITIAL/disk_init.f90:  namelist /init/rho0,ee0,ux0,uy0,hscale
	INITIAL/generic.f90:  namelist /init/do_init,do_test,r0,e0,d0,b0,uy0
	INITIAL/planetary_disk_init.f90:  namelist /init/do_init,do_test,rho0,ee0,ee1,e1,d0,B0,hz,w0,delta1,delta2,delta3,xy0,xy1,delta4,r4,w4,a4
	INITIAL/planetary_disk_init.f90:  namelist /test/ampl,k
	INITIAL/shock-init.f90:  namelist /shock/type,r1,r2,e0,hm1,hm2,g1,g2,smoothfac
	INITIAL/stellaratm-init.f90:  namelist /init/do_init
	INITIAL/taxidis.f90:  namelist /init/do_init,do_test,r0,r1,rho0,ee0,d0,B0,w0
	INITIAL/taxidis2.f90:  namelist /init/do_init,do_test,r0,r1,rho0,rho1,ee0,d0,B0,w0,w1
	INITIAL/turbulence-init.f90:  namelist /init/do_init,do_test,test,r0,e0,d0,b0
	INITIAL/turbulence_tests.f90:  namelist /xshear/ux0,uz0,ux1,bz0,ampl,k
	INITIAL/waves-init.f90:  namelist /wave/type,k,ampl,b0,e0,r0
	
	STAGGER/quench.f90:  namelist /quench/ do_quench, qmax, qlim

------------------------------------------------------------------------
## CVS
***IMPORTANT!*** 
The CVS Stagger repository is no longer maintained. Instructions how to use it are provided here below only as historical record, but should be regarded as 'legacy'.

	cd somewhere
	cvs co stagger-code

### CVS branches
CVS branches should generally be avoided, but may be useful when 
developing new things, to not disturb colleagues working with a
stable version of the code.  To make tranparent which files are
involved in a branch, it is a good idea to list them in a file
with a .rev extension.  See for example TESTS/mesh-test-1.rev.
One may then switch between the branch and the main trunk by doing

	cvs update -r mesh-test-1 `cat TESTS/mesh-test-1.rev`
	cvs update -A             `cat TESTS/mesh-test-1.rev`

Quench branch:

	cvs update -r quench `cat TESTS/quench.rev`

OMP-fixed branch:

	cvs update -r omp-fixed `cat TESTS/omp-fixed.rev`