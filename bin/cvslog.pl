#!/usr/bin/perl

# Filter to produce a single line per CVS revision from "cvs log" output

while (<>) {
  if (/Working file: (.*)/) {$file = $1;}
  elsif (/^--------+$/) {
    $_ = <STDIN>;
    ($dum,$rev) = split(/ +/);
    $_ = <STDIN>;
    ($dum,$date,$time,$dum,$au) = split(/ +/);
    $au =~ s/;//;
    $_ = <STDIN>; chop;
    if (/^ *$/) {$_ = <STDIN>; chop;}
    if (/^branches:/) {$_ = <STDIN>; chop;}
    $log = $_;
    write;
  }
}

format STDOUT =
@<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<@>>>>>>>>>>> @<<<<<<<<< @<<<<<<< @>>>>>>> @<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
$file                                 ,$rev        ,$date     ,$time   ,$au     ,$log
.
