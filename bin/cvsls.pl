#!/usr/bin/perl

# $Id: cvsls.pl,v 1.1 2014/09/17 21:31:23 aake Exp $

#  Filter the output of "cvs stat" to a compact, one-line-per-file format

# open (STDIN,"cvs stat|");

$mo{'Jan'} = '01';
$mo{'Feb'} = '02';
$mo{'Mar'} = '03';
$mo{'Apr'} = '04';
$mo{'May'} = '05';
$mo{'Jun'} = '06';
$mo{'Jul'} = '07';
$mo{'Aug'} = '08';
$mo{'Sep'} = '09';
$mo{'Oct'} = '10';
$mo{'Nov'} = '11';
$mo{'Dec'} = '12';

while (<>) {
  chop; split;
  if    (/^File: ([^ ]*) *\t*Status: (.*)/) {$file=$1; $stat=$2;}
  elsif (/Working revision:/   ) {
    ($dum,$wrev,$date) = split(/\t/);
    $_ = $date;
    ($wday,$mon,$day,$time,$year) = split(/ +/);
    $day = "0$day" if ($day < 10);
    $mon = $mo{$mon};
    $date = "$year-$mon-$day $time";
  }
  elsif (/Repository revision:/) {($dum,$rrev,$name) = split(/\t/);}
  elsif (/Sticky Tag:\t*(.*)/) {$tag=$1;}
  elsif (/Sticky Options:/) {write ;}
}

format STDOUT = 
@<<<<<<<<<<<<<<<<<<<<<<<<<<@<<<<<<<<<<<<<@>>>>>>>>@>>>>>>>>@>>>>>>>>>>>>>>>>>>>>  @<<<<<<<<<<<<<<<
$file                     ,$tag          ,$wrev   ,$rrev   ,$date                 ,$stat
.

#  Example input:
#===================================================================
#File: .cvsignore       	Status: Up-to-date
#
#   Working revision:	1.5	Fri Jun  9 13:54:04 2000
#   Repository revision:	1.5	/astro/home/aake/cvs/astro2000/f90/MHD2000/.cvsignore,v
#   Sticky Tag:		(none)
#   Sticky Date:		(none)
#   Sticky Options:	(none)
