! $Id: grid.f90,v 1.24 2009/10/07 12:40:01 aake Exp $
!*******************************************************************************
MODULE grid_m
  implicit none
  integer                             :: mpi_size=1, mpi_rank=0                 ! number of MPI nodes, and our number
  integer                             :: mpi_x=0, mpi_y=0, mpi_z=0              ! our coordinates in the MPI cubical arrangement
  integer                             :: mpi_mx=0, mpi_my=0, mpi_mz=0           ! the dimensions of the MPI processor cube
  integer                             :: mx, my, mz                             ! the dimensions of the MPI sub-domains
  integer                             :: mw, mvar                               ! words per variable, number of variables
  integer                             :: recl, word                             ! fortan file record length
  integer                             :: lb, ub                                 ! indices of lower and upper boundaries
  integer                             :: stdin, stdout, verbose                 ! units for text in and out, level of noise
  logical                             :: master                                 ! are we the MPI master (mpi_rank 0) ? 
  logical                             :: do_dump, do_compare                    ! dump variables to disk, or compare with disk
  real                                :: sx, sy, sz                             ! physical size of the whole domain
  real                                :: dx, dy, dz, dsmin, dsmax               ! cell sizes, with min and max
  real                                :: xmin, xmax, ymin, ymax, zmin, zmax     ! domain bounds
  real                                :: xmin_mpi, xmax_mpi, ymin_mpi, ymax_mpi, zmin_mpi, zmax_mpi  ! ditto for MPI sub-domain
  real                                :: gamma                                  ! gas gamma
  real, parameter                     :: pi=3.1415926536
  real, allocatable, dimension(:)     :: xm, ym, zm                             ! mesh coordinates
  real, allocatable, dimension(:,:)   :: r_upper, r_lower, e_upper, e_lower     ! density and energy boundary values
  real, allocatable, dimension(:,:,:) :: r,px,py,pz,e,Bx,By,Bz,Ux,Uy,Uz, &      ! variables and their time derivatives
                                         drdt,dedt,dpxdt,dpydt,dpzdt, &
					 dBxdt,dBydt,dBzdt,x,y,z,scratch
  character(len=80) name, dir, info                                             ! output base name (.dat and .txt added)
  character(len=79) hl                                                          ! horizontal line
END MODULE

!*******************************************************************************
SUBROUTINE init_grid
  USE grid_m
!-------------------------------------------------------------------------------
! defines periodic grids that go from 0 to s[xyz]-d[xyz]
!-------------------------------------------------------------------------------
  implicit none
  integer i
  namelist /grid/ mx, my, mz, sx, sy, sz, lb, mpi_mx, mpi_my, mpi_mz, word
  character(len=80):: id='$Id: grid.f90,v 1.24 2009/10/07 12:40:01 aake Exp $'
!...............................................................................
  call print_id (id)
  info = ' '
  word = 4                                                                      ! for compilers with recl in bytes
  rewind(stdin); read(stdin,grid)
  if (master) write (2,grid)
  if (mz==1) lb=1                                                               ! no boundary layers in 1-D
  ub = mz-lb+1                                                                  ! upper boundary of full domain
  dx = sx/mx
  dy = sy/my
  dz = sz/(ub-lb)
  if (lb==1) dz = sz/mz                                                         ! lb=1 in periodic case
  dsmin = min(dx,dy,dz)
  dsmax = max(dx,dy,dz)
  call init_mpi_grid                                                            ! might modify the local grid
  ub = mz-lb+1                                                                  ! upper boundary of sub-domain
  if (master) then
    write (stdout,*) hl
    write (stdout,grid)
  end if
  call allocate_arrays
  call init_stagger
END SUBROUTINE

!-------------------------------------------------------------------------------
SUBROUTINE allocate_arrays
  USE grid_m
  implicit none
  integer i
!...............................................................................
  mw=mx*my*mz
  recl=word*mw
  allocate (r(mx,my,mz), e(mx,my,mz))
  allocate (px(mx,my,mz), py(mx,my,mz), pz(mx,my,mz))
  allocate (Ux(mx,my,mz), Uy(mx,my,mz), Uz(mx,my,mz))
  allocate (Bx(mx,my,mz), By(mx,my,mz), Bz(mx,my,mz))
  allocate (drdt(mx,my,mz), dedt(mx,my,mz))
  allocate (dpxdt(mx,my,mz), dpydt(mx,my,mz), dpzdt(mx,my,mz))
  allocate (dBxdt(mx,my,mz), dBydt(mx,my,mz), dBzdt(mx,my,mz))

  allocate (xm(mx),ym(my),zm(mz))
  xm = dx*(/ ((i-1 )+mpi_x*mx, i=1,mx) /) + xmin
  ym = dy*(/ ((i-1 )+mpi_y*my, i=1,my) /) + ymin
  zm = dz*(/ ((i-lb)+mpi_z*mz, i=1,mz) /) + zmin
  allocate (x(mx,my,mz),y(mx,my,mz),z(mx,my,mz))
  x=spread(spread(xm,dim=2,ncopies=my),dim=3,ncopies=mz)
  y=spread(spread(ym,dim=1,ncopies=mx),dim=3,ncopies=mz)
  z=spread(spread(zm,dim=1,ncopies=mx),dim=2,ncopies=my)
END subroutine

!-------------------------------------------------------------------------------
SUBROUTINE deallocate_arrays
  USE grid_m
  implicit none
!...............................................................................
  deallocate (r, e)
  deallocate (px, py, pz)
  deallocate (Bx, By, Bz)
  deallocate (drdt, dedt)
  deallocate (dpxdt, dpydt, dpzdt)
  deallocate (dBxdt, dBydt, dBzdt)
END subroutine
!*******************************************************************************
