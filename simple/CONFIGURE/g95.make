# $Id: g95.make,v 1.3 2009/10/07 11:13:53 aake Exp $

# --- MPI or not ---------------------------------------------------------------
ifeq ($(MPI),MPI)
  FC = mpif90
else
  FC = g95
endif

LD = $(FC)

# --- Common compiler flags ----------------------------------------------------
FFLAGS = -O -msse3

