# $Id: ifort.make,v 1.3 2009/09/18 08:45:19 aake Exp $

ifeq ($(MPI),MPI)
  FC = mpif90
else
  FC = ifort
endif

LD = $(FC) 

# For debugging -- does not reduce the speed, so can be left on
DEBUG = -g -traceback
#DEBUG = -g -traceback -C

FFLAGS = -O1 -xW -vec-report0 -assume byterecl
#FFLAGS = -O0 -assume byterecl
