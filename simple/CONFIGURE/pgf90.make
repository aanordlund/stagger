# $Id: pgf90.make,v 1.3 2009/10/07 11:14:01 aake Exp $

FC = pgf90
LD = $(FC)

ifeq ($(MPI),MPI)
  LIBS += -lmpi
endif

# For automatic OpenMP parallelization (not efficient)
#PAR = -Mconcur -mp=align -mp=bind -mp=numa

FFLAGS = -fastsse $(PAR)
