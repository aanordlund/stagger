#!/bin/sh
# 
# @ job_name         = kgjet
# @ notification     = never
# @ output           = log_$(jobid).txt
# @ error            = log_$(jobid).txt
# @ class            = astro
# @ job_type         = parallel
# @ node_usage       = not_shared
# @ wall_clock_limit = 240:00:00
# @ node             = 12
# @ tasks_per_node   = 4
# @ resources        = ConsumableCpus(1) ConsumableMemory(2gb) InfiniBand2(1)
# @ queue

ln -sf $LOADL_STEP_OUT output.txt

# write host list to hosts.txt
/opt/ibmll/LoadL/full/bin/ll_get_machine_list $LOADL_STEP_ID > hosts.txt

# run with MPI; remember to set the -np argument to (node)*(tasks_per_node)
#cp -p ../kg_jet_MPI.x ./
mpirun -np 48 -hostfile hosts.txt ./kg_jet_MPI.x
