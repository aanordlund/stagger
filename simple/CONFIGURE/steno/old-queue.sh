#!/bin/sh
# 
# @ job_name         = run1
# @ notification     = never
# @ output           = $(jobid).log
# @ error            = $(jobid).log
# @ class            = old
# @ job_type         = parallel
# @ node_usage       = not_shared
# @ wall_clock_limit = 12:00:00
# @ node             = 1
# @ tasks_per_node   = 4
# @ resources        = ConsumableCpus(1) ConsumableMemory(1gb) InfiniBand1(1)
# @ queue

# write host list to hosts.txt
/opt/ibmll/LoadL/full/bin/ll_get_machine_list $LOADL_STEP_ID > hosts.txt

# execute using mpirun; -np argument = tasks_per_node * node
mpirun -np 4 -hostfile hosts.txt ./starformation_Mpi.x
