#!/bin/sh
# 
# @ job_name         = run1
# @ notification     = never
# @ output           = $(jobid).log
# @ error            = $(jobid).log
# @ class            = astro
# @ wall_clock_limit = 12:00:00
# @ resources        = ConsumableCpus(1) ConsumableMemory(2gb)
# @ queue

# execute
./kg_jet.x
