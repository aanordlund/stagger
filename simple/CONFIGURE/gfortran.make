# $Id: gfortran.make,v 1.4 2009/10/06 21:17:15 aake Exp $

# --- No MPI -------------------------------------------------------------------
FC = gfortran
LD = $(FC)

# --- Common compiler flags ----------------------------------------------------
FFLAGS = -O
DEBUG = -g
