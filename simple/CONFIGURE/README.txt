
------------------------------------------------------------------------

To change default compiler, either 

1) Edit the CONFIGURE/Makefile, or

2) Create a directory and file CONFIGURE/$(HOST)/Makefile, where $(HOST)
is your machine name and set a new value of COMPILER in that file.

------------------------------------------------------------------------

To compile without MPI, do

make MPI=NOMPI  ...

(which simply uses routines from the ./NOMPI directory instead of the
MPI directory).

------------------------------------------------------------------------

To compile with MPI you need to find the command name and options for
the local MPI compiler.   If, for example, it is "mpif90" and the real
compiler (behind mpif90) is ifort, then use

make COMPILER=ifort MPI=MPI ...

Compiler commands like "mpif90" are typically just scripts (or 
C-programs) that call a real compiler, adding the options to find
include files and to link with certain libraries.

To handle more general cases, please note that all COMPILER=xxxx 
does is to include a CONFIGURE/$(COMPILER).make file.   Take a
look inside CONFIGURE/ifort.make, and you will see how it chooses
to use "mpif90" when MPI=MPI is used, and "ifort" otherwise.  

It is likely that a local MPI-configuration uses "mpif90", or some similar
"frontend", which takes care of adding include files and link linraries --
that is certainly the best.  

If a local MPI-implementation with 'mpif90' exists, but it uses for example
"gfortran" instead of "ifort" as the 'real' compiler, then add lines to
gfortran.make by copying and editing slightly the corresponding lines from
ifort.make.

But, compiling for MPI can also be done by explicitly adding options for 
finding include files, and for linking against MPI-libraries.  If this is
the case, then edit the $(COMPILER).make file for that compiler to look 
something like this (just an example):

ifeq ($(MPI),MPI)
  INCLUDE = -I/opt/MPI/include
  LIBS = -L/opt/MPI/lib -lmpi
else
  INCLUDE = 
  LIBS =
endif

FC = gfortran $(INCLUDE)
LD = gfortran $(LIBS)

So, in the case of MPI this adds the necessary options (documented 
locally) to the compile and link commands.

------------------------------------------------------------------------
