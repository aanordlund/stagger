! $Id: support.f90,v 1.10 2009/10/07 10:20:28 aake Exp $
!*******************************************************************************
MODULE mpi_m
  implicit none
  include 'mpif.h'
  integer               :: new_com
  integer               :: mpi_xup, mpi_xdn, mpi_yup, mpi_ydn, mpi_zdn, mpi_zup
  integer, dimension(3) :: mpi_coords, mpi_dims
  logical               :: mpi_reorder, mpi_periodic(3)
  integer               :: status(MPI_STATUS_SIZE)
  integer               :: request1, request2, request3, request4
  integer               :: mpi_err
  integer               :: handle, filetype
  integer               :: verbose=0
END MODULE mpi_m

!*******************************************************************************
SUBROUTINE init_mpi
!
!  Run the Toy Code under MPI.  All we do is modify the stagger operators
!  so they use MPI to communicate overlap zones.  There is no change needed in
!  the code, only in initialization, driver and (possibly) boundary routines.
!  There, changes are minimized by just changing the scales and coordinates,
!  assuming that the MPI-boxes have the sizes given in the input param file.
!
!-------------------------------------------------------------------------------
  USE grid_m
  USE mpi_m
  implicit none
  character(len=80):: id='$Id: support.f90,v 1.10 2009/10/07 10:20:28 aake Exp $'
!...............................................................................
  call print_id (id)
!-------------------------------------------------------------------------------
!  Start up MPI and get number of nodes and our node (mpi_rank)
!-------------------------------------------------------------------------------
  call MPI_INIT (mpi_err)
  call MPI_COMM_SIZE (mpi_comm_world, mpi_size, mpi_err)
  call MPI_COMM_RANK (mpi_comm_world, mpi_rank, mpi_err)
  master = mpi_rank==0
  lb = 1                                                                        ! change in start.f90 if non-periodic
END SUBROUTINE

!*******************************************************************************
SUBROUTINE init_mpi_grid
!
!  Run the Toy Code under MPI.  All we do is modify the stagger operators
!  so they use MPI to communicate overlap zones.  There is no change needed in
!  the code, only in initialization, driver and (possibly) boundary routines.
!  There, changes are minimized by just changing the scales and coordinates,
!  assuming that the MPI-boxes have the sizes given in the input param file.
!
!-------------------------------------------------------------------------------
  USE grid_m
  USE mpi_m
  implicit none
  real power
  integer iostatus, rank
  character(len=120) line
!-------------------------------------------------------------------------------
!  Compute suitable MPI dimensions
!-------------------------------------------------------------------------------
  if (mpi_mx == 0) then
    power = 3.
  1 if (mpi_my >= 1) power = power-1
    if (mpi_mz >= 1) power = power-1
    mpi_mx = (real(mpi_size/(max(mpi_my,1)*max(mpi_mz,1))))**(1./power+0.001)            ! search for a suitable mpi_mx
    do while (mod(mpi_size,mpi_mx) > 0)
      mpi_mx = mpi_mx-1
    end do
    if (mod(mx,mpi_mx) > 0) then
      if (mpi_my==0 .and. mod(my,mpi_mx) == 0) then
        mpi_my = mpi_mx
        if (master) print*,'first divisor; mpi_my =',mpi_my
        goto 1
      else
        if (mpi_mz==0 .and. mod(mz,mpi_mx) == 0) then
          mpi_mz = mpi_mx
          if (master) print*,'first divisor; mpi_mz =',mpi_mz
          goto 1
        else
          if (master) print*,'first divisor failed'
          stop
        end if
      end if
    end if
    if (master) print*,'first divisor; mpi_mx =',mpi_mx
  end if
  if (mpi_my == 0) then
    power = 2.
  2 if (mpi_mz >= 1) power = power-1
    mpi_my = (real(mpi_size/(mpi_mx*max(mpi_mz,1))))**(1./power+0.001)     ! search for a suitable mpi_my
    do while (mod(mpi_size,mpi_my) > 0)
      mpi_my = mpi_my-1
    end do
    if (mod(my,mpi_my) > 0) then
      if (mpi_mz==0 .and. mod(mz,mpi_my) == 0) then
        mpi_mz = mpi_my
        mpi_my = 1
        if (master) print*,'second divisor; mpi_mz =',mpi_mz
        goto 2
      else
        if (master) print*,'second divisor failed'
        stop
      end if
    end if
  end if
  mpi_mz = mpi_size/(mpi_mx*mpi_my)                        ! let the z-dir take the rest
  if (mpi_mx*mpi_my*mpi_mz .ne. mpi_size &
    .or. mod(mx,mpi_mx) > 0 .or. mod(mpi_size,mpi_mx) > 0 &
    .or. mod(my,mpi_my) > 0 .or. mod(mpi_size,mpi_my) > 0 &
    .or. mod(mz,mpi_mz) > 0 .or. mod(mpi_size,mpi_mz) > 0 &
    ) then
    if (mpi_rank==0) then
      print *,'mpi_size is not compatible with the problem size'
      print *,'mpi_size, my, mz =',mpi_size,my,mz
      print *,'mpi_mx, mpi_my, mpi_mz =',mpi_mx,mpi_my,mpi_mz
    end if
    call shut_down ('error')
  end if
  mx = mx/mpi_mx                            ! reduce grid for MPI
  my = my/mpi_my                            ! reduce grid for MPI
  mz = mz/mpi_mz                            ! reduce grid for MPI

!-------------------------------------------------------------------------------
!  Compute mpi dimensions, our place in the node space, and our neighbors
!-------------------------------------------------------------------------------
  mpi_dims(1) = mpi_mx; mpi_dims(2) = mpi_my; mpi_dims(3) = mpi_mz
  if (mpi_mx*mpi_my*mpi_mz .ne. mpi_size) then
    print *, 'inconsistent mpi_dims', mpi_dims, mpi_size
    call MPI_FINALIZE (mpi_err)
    stop
  end if
  mpi_reorder = .false.; mpi_periodic = .true.
  CALL MPI_CART_CREATE (mpi_comm_world, 3, mpi_dims, mpi_periodic, mpi_reorder, new_com,  mpi_err)
  CALL MPI_CART_COORDS (new_com, mpi_rank, 3, mpi_coords, mpi_err)
  CALL MPI_CART_SHIFT  (new_com, 0, 1, mpi_xdn, mpi_xup, mpi_err)
  CALL MPI_CART_SHIFT  (new_com, 1, 1, mpi_ydn, mpi_yup, mpi_err)
  CALL MPI_CART_SHIFT  (new_com, 2, 1, mpi_zdn, mpi_zup, mpi_err)
!...............................................................................
  mpi_x = mpi_coords(1)
  mpi_y = mpi_coords(2)
  mpi_z = mpi_coords(3)
END

!*******************************************************************************
SUBROUTINE barrier_mpi
  USE grid_m
  USE mpi_m
  implicit none
!...............................................................................
  call MPI_BARRIER (mpi_comm_world, mpi_err)
END

!*******************************************************************************
SUBROUTINE end_mpi
  USE grid_m
  USE mpi_m
  implicit none
!...............................................................................
  call MPI_BARRIER (mpi_comm_world, mpi_err)
  call MPI_FINALIZE (mpi_err)
END

!*******************************************************************************
SUBROUTINE mpi_send_x (f, g, mg, h, mh)
!
!  Send the boundary layers from one domain over to the neighboring domains
!  and recieve the corresponding values into temporary arrays.
!
!  g is the RHS part of the f array, so is to be used on the LHS 
!  h is the LHS part of the f array, so is to be used on the RHS
!
!-------------------------------------------------------------------------------
  USE grid_m
  USE mpi_m
  implicit none
!...............................................................................
  integer mg, mh
  real, dimension(mx,my,mz):: f
  real, dimension(mg,my,mz):: g, gsend
  real, dimension(mh,my,mz):: h, hsend
!-------------------------------------------------------------------------------
!  Single cpu runs, just wrap the array (assumes periodic y-conditions)
!-------------------------------------------------------------------------------
  if (mpi_mx == 1) then
    if (mh > 0) h(1:mh,:,:) = f(1:mh,:,:)
    if (mg > 0) g(1:mg,:,:) = f(mx-mg+1:mx,:,:)
  else
!-------------------------------------------------------------------------------
!  Send up and down
!-------------------------------------------------------------------------------
    if (mh > 0) then
      hsend = f(1:mh,:,:)
      call MPI_ISEND (hsend, mh*my*mz, MPI_REAL, mpi_xdn, 2*mpi_rank, mpi_comm_world, request1, mpi_err)
      call MPI_IRECV (h    , mh*my*mz, MPI_REAL, mpi_xup, 2*mpi_xup , mpi_comm_world, request3, mpi_err)
    end if
    if (mg > 0) then
      gsend = f(mx-mg+1:mx,:,:)
      call MPI_ISEND (gsend, mg*my*mz, MPI_REAL, mpi_xup, 2*mpi_rank+1, mpi_comm_world, request2, mpi_err)
      call MPI_IRECV (g    , mg*my*mz, MPI_REAL, mpi_xdn, 2*mpi_xdn +1, mpi_comm_world, request4, mpi_err)
    end if
!-------------------------------------------------------------------------------
!  Wait as necessary (noop on many MPI implementations)
!-------------------------------------------------------------------------------
    if (mh > 0) then
      call MPI_WAIT (request1, status, mpi_err)
      call MPI_WAIT (request3, status, mpi_err)
    end if
    if (mg > 0) then
      call MPI_WAIT (request2, status, mpi_err)
      call MPI_WAIT (request4, status, mpi_err)
    end if
  end if
END

!*******************************************************************************
SUBROUTINE mpi_send_y (f, g, mg, h, mh)
!
!  Send the boundary layers from one domain over to the neighboring domains
!  and recieve the corresponding values into temporary arrays.
!
!-------------------------------------------------------------------------------
  USE grid_m
  USE mpi_m
  implicit none
!...............................................................................
  integer mg, mh
  real, dimension(mx,my,mz):: f
  real, dimension(mx,mg,mz):: g, gsend
  real, dimension(mx,mh,mz):: h, hsend
!-------------------------------------------------------------------------------
!  Single cpu runs, just wrap the array (assumes periodic y-conditions)
!-------------------------------------------------------------------------------
  if (mpi_my == 1) then
    if (mh > 0) h(:,1:mh,:) = f(:,1:mh,:)
    if (mg > 0) g(:,1:mg,:) = f(:,my-mg+1:my,:)
  else
!-------------------------------------------------------------------------------
!  Send up and down
!-------------------------------------------------------------------------------
    if (mh > 0) then
      hsend = f(:,      1:mh,:)
      call MPI_ISEND (hsend, mx*mh*mz, MPI_REAL, mpi_ydn, 2*mpi_rank, mpi_comm_world, request1, mpi_err)
      call MPI_IRECV (h    , mx*mh*mz, MPI_REAL, mpi_yup, 2*mpi_yup , mpi_comm_world, request3, mpi_err)
    end if
    if (mg > 0) then
      gsend = f(:,my-mg+1:my,:)
      call MPI_ISEND (gsend, mx*mg*mz, MPI_REAL, mpi_yup, 2*mpi_rank+1, mpi_comm_world, request2, mpi_err)
      call MPI_IRECV (g    , mx*mg*mz, MPI_REAL, mpi_ydn, 2*mpi_ydn +1, mpi_comm_world, request4, mpi_err)
    end if
!-------------------------------------------------------------------------------
!  Wait as necessary (noop on many MPI implementations)
!-------------------------------------------------------------------------------
    if (mh > 0) then
      call MPI_WAIT (request1, status, mpi_err)
      call MPI_WAIT (request3, status, mpi_err)
    end if
    if (mg > 0) then
      call MPI_WAIT (request2, status, mpi_err)
      call MPI_WAIT (request4, status, mpi_err)
    end if
  end if
END

!*******************************************************************************
SUBROUTINE mpi_send_z (f, g, mg, h, mh)
!
!  Send the boundary layers from one domain over to the neighboring domains
!  and recieve the corresponding values into temporary arrays.
!
!-------------------------------------------------------------------------------
  USE grid_m
  USE mpi_m
  implicit none
!...............................................................................
  integer mg, mh
  real, dimension(mx,my,mz):: f
  real, dimension(mx,my,mg):: g, gsend
  real, dimension(mx,my,mh):: h, hsend
!-------------------------------------------------------------------------------
!  For single cpu runs, just wrap the array (assumes periodic y-conditions)
!-------------------------------------------------------------------------------
  if (mpi_mz == 1) then
    if (mh > 0) h(:,:,1:mh) = f(:,:,1:mh)
    if (mg > 0) g(:,:,1:mg) = f(:,:,mz-mg+1:mz)
  else
!-------------------------------------------------------------------------------
!  Ship the low index layer to the next one down, receive same from next one up
!-------------------------------------------------------------------------------
    if (mh > 0) then
      hsend(:,:,1:mh) = f(:,:,1:mh)
      call MPI_ISEND (hsend, mx*my*mh, MPI_REAL, mpi_zdn, 2*mpi_rank, mpi_comm_world, request1, mpi_err)
      call MPI_IRECV (h    , mx*my*mh, MPI_REAL, mpi_zup, 2*mpi_zup , mpi_comm_world, request3, mpi_err)
    end if
!-------------------------------------------------------------------------------
!  Ship the high index layer to the next one up, receive same from next one down
!-------------------------------------------------------------------------------
    if (mg > 0) then
      gsend(:,:,1:mg) = f(:,:,mz-mg+1:mz)
      call MPI_ISEND (gsend, mx*my*mg, MPI_REAL, mpi_zup, 2*mpi_rank+1, mpi_comm_world, request2, mpi_err)
      call MPI_IRECV (g    , mx*my*mg, MPI_REAL, mpi_zdn, 2*mpi_zdn +1, mpi_comm_world, request4, mpi_err)
    end if
!-------------------------------------------------------------------------------
!  Wait as necessary (noop on many MPI implementations)
!-------------------------------------------------------------------------------
    if (mh > 0) then
      call MPI_WAIT (request1, status, mpi_err)
      call MPI_WAIT (request3, status, mpi_err)
    end if
    if (mg > 0) then
      call MPI_WAIT (request2, status, mpi_err)
      call MPI_WAIT (request4, status, mpi_err)
    end if
  end if
END

!***********************************************************************
REAL FUNCTION fmaxval (f) RESULT(fmax)
  USE mpi_m
  USE grid_m, only: mx,my,mz
  implicit none
  real, dimension(mx,my,mz):: f
  call MPI_ALLREDUCE(maxval(f),fmax,1,MPI_REAL,MPI_MAX,mpi_comm_world,mpi_err)
END FUNCTION

!***********************************************************************
REAL FUNCTION fminval (f) RESULT(fmin)
  USE mpi_m
  USE grid_m, only: mx,my,mz
  implicit none
  real, dimension(mx,my,mz):: f
  call MPI_ALLREDUCE(minval(f),fmin,1,MPI_REAL,MPI_MIN,mpi_comm_world,mpi_err)
END FUNCTION

!***********************************************************************
REAL FUNCTION faver (f)
  USE mpi_m
  USE grid_m, only: mx,my,mz,mpi_size
  implicit none
  real, dimension(mx,my,mz):: f
  call MPI_ALLREDUCE(sum(f),faver,1,MPI_REAL,MPI_SUM,mpi_comm_world,mpi_err)
  faver = faver/mpi_size/(real(mx)*real(my)*real(mz))
END FUNCTION

!***********************************************************************
REAL FUNCTION fsum (f)
  USE mpi_m
  implicit none
  real f
  call MPI_ALLREDUCE(f,fsum,1,MPI_REAL,MPI_SUM,mpi_comm_world,mpi_err)
END FUNCTION

!***********************************************************************
SUBROUTINE transpose (f1, f2)
  USE mpi_m
  USE grid_m, only: mx, my, mz, mpi_my, mpi_rank
  implicit none
  real, dimension(mx,my,mz):: f1, f2
  intent(in)  :: f1
  intent(out) :: f2
  integer i1, i2, ix, iy, iz, jx
  integer req1, req2, tag1, tag2
  real, allocatable, dimension(:,:,:) :: a1, a2
!-----------------------------------------------------------------------
  allocate (a1(my,my,mz), a2(my,my,mz))

  do i1=0,mpi_my-1
    jx = i1*my
    do iz=1,mz; do iy=1,my; do ix=1,my
      a1(ix,iy,iz) = f1(ix+jx,iy,iz)
    end do; end do; end do
    i2 = mpi_rank
    tag1 = i1+10*i2
    tag2 = i2+10*i1
    if (i2 == i1) then
      do iz=1,mz
        a2(:,:,iz) = a1(:,:,iz)
      end do
    else
      call MPI_ISEND (a1, my*my*mz, MPI_REAL, i1, tag1, MPI_COMM_WORLD, req1, mpi_err)
      call MPI_IRECV (a2, my*my*mz, MPI_REAL, i1, tag2, MPI_COMM_WORLD, req2, mpi_err)
      call MPI_WAIT (req1, status, mpi_err)
      call MPI_WAIT (req2, status, mpi_err)
    end if
    do iz=1,mz; do iy=1,my; do ix=1,my
      f2(ix+jx,iy,iz) = a2(iy,ix,iz)
    end do; end do; end do
  end do

  deallocate (a1, a2)
END SUBROUTINE

!*******************************************************************************
SUBROUTINE trace (string)
  USE grid_m, only: mpi_rank
  USE mpi_m
  implicit none
  character(len=*):: string
!...............................................................................
  if (verbose > 0) print*,'TRACE: '//string, mpi_rank
  call MPI_BARRIER (mpi_comm_world, mpi_err)
END SUBROUTINE

!***********************************************************************
MODULE permute_m
  USE mpi_m
  implicit none
  real, allocatable, dimension(:,:,:,:):: snd                           ! send buffer
  real, allocatable, dimension(:,:,:):: rcv                             ! recieve buffer
  integer, allocatable, dimension(:):: sndrq                            ! send requests
  integer, allocatable, dimension(:,:):: snd_status                     ! send status
  integer nx, ny, nz, m, nreq, ierr
  logical, save:: first_time=.true., debug_permute=.false.
END MODULE

!***********************************************************************
SUBROUTINE init_permute
  USE grid_m
  USE mpi_m
  USE permute_m
  implicit none
  character(len=72):: id="$Id: support.f90,v 1.10 2009/10/07 10:20:28 aake Exp $"
!-----------------------------------------------------------------------
  first_time = .false.
  m = min(mx,my,mz)                                                     ! smallest dimension
  nx = mx/m                                                             ! number of boxes in x
  ny = my/m                                                             ! number of boxes in y
  nz = mz/m                                                             ! number of boxes in z
  allocate (sndrq(nx*ny*nz))                                            ! send reqs
  allocate (snd(m,m,m,nx*ny*nz))                                        ! send buffer
  allocate (snd_status(MPI_STATUS_SIZE,nx*ny*nz))                       ! status buffer
  allocate (rcv(m,m,m))                                                 ! recv buffer
END SUBROUTINE

!***********************************************************************
SUBROUTINE permute (f1, f2)
  USE grid_m, only: mx, my, mz, mpi_mx, mpi_my, mpi_mz, mpi_x, mpi_y, mpi_z
  USE permute_m
  USE mpi_m
  implicit none
  real, dimension(mx,my,mz):: f1, f2
  intent(in)  :: f1
  intent(out) :: f2
  integer ix, iy, iz, jx, jy, jz, kx, ky, kz, coord(3), tag, rank, req
!-----------------------------------------------------------------------
  if (first_time) call init_permute                                     ! initialize
  nreq = 0                                                              ! reset request counter
  kx = mpi_x*mx                                                         ! global x-index of domain
  ky = mpi_y*my                                                         ! global y-index of domain
  kz = mpi_z*mz                                                         ! global z-index of domain

  do jz=0,mz-1,m; do jy=0,my-1,m; do jx=0,mx-1,m                        ! SEND
    coord = (/ (jy+ky)/mx, (jz+kz)/my, (jx+kx)/mz /)                    ! receiver MPI-coordinates
    call MPI_CART_RANK (new_com, coord, rank, ierr)                     ! who to send to
    tag = (jx+kx)/m + (my*mpi_my)*((jy+ky)/m + (mz*mpi_mz)*(jz+kz)/m)   ! package tag
    nreq = nreq+1                                                       ! increment request counter
    do iz=1,m; do iy=1,m; do ix=1,m                                     ! copy to ..
      snd(iy,iz,ix,nreq) = f1(ix+jx,iy+jy,iz+jz)                        ! .. send buffer
    end do; end do; end do
    call MPI_ISSEND (snd(1,1,1,nreq), m*m*m, MPI_REAL, rank, tag, &     ! start send to her ..
                     new_com, sndrq(nreq), ierr)                        ! .. saving rq
  end do; end do; end do

  call MPI_BARRIER (new_com, ierr)                                      ! make sure all sent

  do jz=0,mz-1,m; do jy=0,my-1,m; do jx=0,mx-1,m                        ! RECIEVE
    coord = (/ (jz+kz)/mx, (jx+kx)/my, (jy+ky)/mz /)                    ! sender MPI-coordinates
    call MPI_CART_RANK (new_com, coord, rank, ierr)                     ! who to rcv from
    tag = (jz+kz)/m + (my*mpi_my)*((jx+kx)/m + (mz*mpi_mz)*(jy+ky)/m)   ! package tag
    call MPI_IRECV (rcv, m*m*m, MPI_REAL, rank, tag, new_com, req, ierr)! recv from him ..
    call MPI_WAIT (req, status, ierr)                                   ! wait for completion
    do iz=1,m; do iy=1,m; do ix=1,m
      f2(ix+jx,iy+jy,iz+jz) = rcv(ix,iy,iz)                             ! put in place
    end do; end do; end do
  end do; end do; end do
  call MPI_WAITALL (nreq, sndrq, snd_status, ierr)                      ! wait on sndrq, to free sndrq
END SUBROUTINE

!***********************************************************************
SUBROUTINE test_permute
  USE grid_m, only: mx, my, mz, mpi_x, mpi_y, mpi_z, mpi_rank
  USE permute_m
  implicit none
  real, allocatable, dimension(:,:,:):: fft1, fft2
  integer ix, iy, iz, jx, jy, jz
  real tmp
  logical ok
  integer, parameter:: mprint=10
  integer nprint
!-----------------------------------------------------------------------
  allocate (fft1(mx,my,mz),fft2(mx,my,mz))
  nprint = 0
  do iz=1,mz
    jz = iz+mpi_z*mz
    do iy=1,my
      jy = iy+mpi_y*my
      do ix=1,mx
        jx = ix+mpi_x*mx
        fft1(ix,iy,iz) = jz+1000*(jy+1000*jx)
        fft2(ix,iy,iz) = jz+1000*(jy+1000*jx)
      end do
    end do
  end do
  call permute (fft2, fft2)
  ok = .true.
  do iz=1,mz
    jz = iz+mpi_z*mz
    do iy=1,my
      jy = iy+mpi_y*my
      do ix=1,mx
        jx = ix+mpi_x*mx
        tmp = jy+1000*(jx+1000*jz)
        if (abs(tmp-fft2(ix,iy,iz)) > 1e-2) then
          if (nprint < mprint) print 1, mpi_rank, ix, iy, iz, tmp, fft2(ix,iy,iz)
          nprint = nprint+1
          ok = .false.
        end if
      end do
    end do
  end do
1 format(i6,3i4,2f10.0)
  deallocate (fft1, fft2)
END SUBROUTINE

!*******************************************************************************
SUBROUTINE file_open (file, mode)
  USE grid_m, ONLY: mx, my, mz, mpi_mx, mpi_my, mpi_mz, mpi_x, mpi_y, mpi_z, mpi_rank
  USE mpi_m
  implicit none
  integer mode
  character(len=*) file
  integer, dimension(3):: size, subsize, starts
!
  CALL MPI_FILE_OPEN (mpi_comm_world, file, MPI_MODE_CREATE + MPI_MODE_RDWR, MPI_INFO_NULL, handle, mpi_err)    
  size = (/mx*mpi_mx, my*mpi_my, mz*mpi_mz/)
  subsize = (/mx, my, mz/)
  starts = (/mpi_x*mx, mpi_y*my, mpi_z*mz/)
  if (verbose > 0 .and. mpi_rank==0) print'(i5,3(2x,3i7))',mpi_rank,size,subsize,starts
  if (verbose > 1 .and. mpi_rank >0) print'(i5,3(2x,3i7))',mpi_rank,size,subsize,starts
  CALL MPI_TYPE_CREATE_SUBARRAY (3, size, subsize, starts, MPI_ORDER_FORTRAN, MPI_REAL, filetype, mpi_err)   
  CALL MPI_TYPE_COMMIT (filetype, mpi_err)
END

!*******************************************************************************
SUBROUTINE file_openw (file)
  USE mpi_m
  USE grid_m, only: mpi_rank
  implicit none
  character(len=*) file
!
  if ((verbose > 0 .and. mpi_rank==0) .or. (verbose > 1)) then
    print'(1x,a,2i8)','file_openw: '//trim(file),mpi_rank
  end if 
  call file_open (file, MPI_MODE_CREATE + MPI_MODE_RDWR)
END

!*******************************************************************************
SUBROUTINE file_openr (file)
  USE mpi_m
  USE grid_m, only: mpi_rank
  implicit none
  character(len=*) file
!
  if ((verbose > 0 .and. mpi_rank==0) .or. (verbose > 1)) then
    print'(1x,a,2i8)','file_openr: '//trim(file),mpi_rank,mpi_err
  end if 
  call file_open (file, MPI_MODE_RDWR)
END

!*******************************************************************************
SUBROUTINE file_write (rec,f)
  USE grid_m, ONLY: mx, my, mz, mpi_mx, mpi_my, mpi_mz, mpi_x, mpi_y, mpi_z, mpi_rank
  USE mpi_m
  implicit none
  real, dimension(mx,my,mz):: f
  integer rec,count
  integer(kind=MPI_OFFSET_KIND) pos
!
  pos = 4*rec*mx*my*mz
  pos = pos*mpi_mx*mpi_my*mpi_mz
  CALL MPI_FILE_SET_VIEW (handle, pos, MPI_REAL, filetype, 'native', MPI_INFO_NULL, mpi_err)   
  count = mx*my*mz
  if ((verbose > 0 .and. mpi_rank==0) .or. (verbose > 1)) then
    print'(1x,a,4i9,5e12.3)','mpi_file_write_all:',mpi_rank,count,rec,pos,f(1:3,1,1)
  end if
  CALL MPI_FILE_WRITE_ALL(handle, f , count, MPI_REAL, status, mpi_err)
END

!*******************************************************************************
SUBROUTINE file_read (rec,f)
  USE grid_m, ONLY: mx, my, mz, mpi_mx, mpi_my, mpi_mz, mpi_x, mpi_y, mpi_z, mpi_rank
  USE mpi_m
  implicit none
  real, dimension(mx,my,mz):: f
  integer rec, count
  integer(kind=MPI_OFFSET_KIND) pos
!
  pos = 4*rec*mx*my*mz
  pos = pos*mpi_mx*mpi_my*mpi_mz
  count = mx*my*mz
  !if ((verbose > 0 .and. mpi_rank==0) .or. (verbose > 1)) then
    !print'(1x,a,4i8,5e12.3)','mpi_file_read_all:',mpi_rank,count,rec,pos
  !end if
  CALL MPI_FILE_SET_VIEW (handle, pos, MPI_REAL, filetype, 'native', MPI_INFO_NULL, mpi_err)   
  CALL MPI_FILE_READ_ALL(handle, f , count, MPI_REAL, status, mpi_err)
  if ((verbose > 0 .and. mpi_rank==0) .or. (verbose > 1)) then
    print'(1x,a,4i8,5e12.3)','mpi_file_read_all:',mpi_rank,count,rec,pos,f(1:3,1,1)
  end if
END

!*******************************************************************************
SUBROUTINE file_close ()
  USE mpi_m
  implicit none
  CALL MPI_FILE_CLOSE(handle, mpi_err)
END

!*******************************************************************************
FUNCTION flag(file)                                                             ! detect presence of file.flag
  USE mpi_m                                                                     ! this is used e.g. to stop/restart
  USE grid_m, only: master, dir
  implicit none
  character(len=*) file
  logical flag
!-------------------------------------------------------------------------------
  if (master) then
    inquire (file=trim(dir)//trim(file)//'.flag',exist=flag)
    if (flag) then
      open (1,file=trim(dir)//trim(file)//'.flag',status='old')
      close (1,status='delete')
    end if
  end if
  call MPI_BCAST (flag,1,MPI_LOGICAL,0,mpi_comm_world,mpi_err)
END
