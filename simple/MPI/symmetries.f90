! $Id: symmetries.f90,v 1.1 2009/10/07 10:33:00 aake Exp $

! Boundary conditions.  The indices are assumed to be arranged like so:
!
!                                lb           ub=m-lb+1
! centers:    1   2   3   4   5   6 ...       -5  -4  -3  -2  -1   0
! faces  :  1   2   3   4   5   6   ... -6  -5  -4  -3  -2  -1   0 
!
! For face centered variables, the index ranges 1:lb and ub+1:my are
! external.  For cell centered variables, the ranges are 1:lb-1 and ub+1:my.
!
! Valid points for plotting:
!
!                 F90                   IDL                  total
! center:  lb  :ub = g+1:m-g    lb-1:m-lb = g  :m-g-1   ub-lb+1 = m-2*g
!   face:  lb+1:ub = g+2:m-g    lb  :m-lb = g+1:m-g-1   ub-lb   = m-2*g-1
!
!-----------------------------------------------------------------------
SUBROUTINE constant_center_lower(f)
  USE grid_m
  implicit none
  real, dimension(mx,my,mz):: f
  integer:: iy,iz

  if (mpi_z > 0) return

  do iz=1,lb-1
    do iy=1,my
      f(:,iy,iz) = f(:,iy,lb)
    end do
  end do
END

!-----------------------------------------------------------------------
SUBROUTINE constant_center_upper(f)
  USE grid_m
  implicit none
  real, dimension(mx,my,mz):: f
  integer:: iy,iz

  if (mpi_z < mpi_mz-1) return

  do iz=ub+1,mz
    do iy=1,my
      f(:,iy,iz) = f(:,iy,ub)
    end do
  end do
END

!-----------------------------------------------------------------------
SUBROUTINE constant_center(f)
  USE grid_m
  implicit none
  real, dimension(mx,my,mz):: f

  call constant_center_lower(f)
  call constant_center_upper(f)
END

!-----------------------------------------------------------------------
SUBROUTINE symmetric_center_lower(f)
  USE grid_m
  implicit none
  real, dimension(mx,my,mz):: f
  integer:: iy,iz
  character(len=80):: id='$Id: symmetries.f90,v 1.1 2009/10/07 10:33:00 aake Exp $'
!.......................................................................
  call print_id (id)

  if (mpi_z > 0) return

  do iz=1,lb-1
    do iy=1,my
      f(:,iy,iz) = f(:,iy,lb+(lb-iz))
    end do
  end do
END

!-----------------------------------------------------------------------
SUBROUTINE symmetric_center_upper(f)
  USE grid_m
  implicit none
  real, dimension(mx,my,mz):: f
  integer:: iy,iz

  if (mpi_z < mpi_mz-1) return

  do iz=ub+1,mz
    do iy=1,my
      f(:,iy,iz) = f(:,iy,ub-(iz-ub))
    end do
  end do
END

!-----------------------------------------------------------------------
SUBROUTINE symmetric_center(f)
  USE grid_m
  implicit none
  real, dimension(mx,my,mz):: f

  call symmetric_center_lower(f)
  call symmetric_center_upper(f)
END

!-----------------------------------------------------------------------
SUBROUTINE symmetric_face_lower(f)
  USE grid_m
  implicit none
  real, dimension(mx,my,mz):: f
  integer:: iy,iz

  if (mpi_z > 0) return

  do iz=1,lb
    do iy=1,my
      f(:,iy,iz) = f(:,iy,lb+1+(lb-iz))
    end do
  end do
END

!-----------------------------------------------------------------------
SUBROUTINE symmetric_face_upper(f)
  USE grid_m
  implicit none
  real, dimension(mx,my,mz):: f
  integer:: iy,iz

  if (mpi_z < mpi_mz-1) return

  do iz=ub+1,mz
    do iy=1,my
      f(:,iy,iz) = f(:,iy,ub+1-(iz-ub))
    end do
  end do
END

!-----------------------------------------------------------------------
SUBROUTINE symmetric_face(f)
  USE grid_m
  implicit none
  real, dimension(mx,my,mz):: f

  call symmetric_face_lower(f)
  call symmetric_face_upper(f)
END

!-----------------------------------------------------------------------
SUBROUTINE antisymmetric_center_lower(f)
  USE grid_m
  implicit none
  real, dimension(mx,my,mz):: f
  integer:: iy,iz

  if (mpi_z > 0) return

  do iz=1,lb-1
    do iy=1,my
      f(:,iy,iz) = -f(:,iy,lb+(lb-iz))
    end do
    f(:,iy,lb) = 0.
  end do
END

!-----------------------------------------------------------------------
SUBROUTINE antisymmetric_center_upper(f)
  USE grid_m
  implicit none
  real, dimension(mx,my,mz):: f
  integer:: iy,iz

  if (mpi_z < mpi_mz-1) return

  do iz=ub+1,mz
    do iy=1,my
      f(:,iy,iz) = -f(:,iy,ub-(iz-ub))
    end do
    f(:,iy,ub) = 0.
  end do
END

!-----------------------------------------------------------------------
SUBROUTINE antisymmetric_center(f)
  USE grid_m
  implicit none
  real, dimension(mx,my,mz):: f

  call antisymmetric_center_lower(f)
  call antisymmetric_center_upper(f)
END

!-----------------------------------------------------------------------
SUBROUTINE antisymmetric_face_lower(f)
  USE grid_m
  implicit none
  real, dimension(mx,my,mz):: f
  integer:: iy,iz

  if (mpi_z > 0) return

  do iz=1,lb
    do iy=1,my
      f(:,iy,iz) = -f(:,iy,lb+1+(lb-iz))
    end do
  end do
END

!-----------------------------------------------------------------------
SUBROUTINE antisymmetric_face_upper(f)
  USE grid_m
  implicit none
  real, dimension(mx,my,mz):: f
  integer:: iy,iz

  if (mpi_z < mpi_mz-1) return

  do iz=ub+1,mz
    do iy=1,my
      f(:,iy,iz) = -f(:,iy,ub+1-(iz-ub))
    end do
  end do
END

!-----------------------------------------------------------------------
SUBROUTINE antisymmetric_face(f)
  USE grid_m
  implicit none
  real, dimension(mx,my,mz):: f

  call antisymmetric_face_lower(f)
  call antisymmetric_face_upper(f)
END

!-----------------------------------------------------------------------
SUBROUTINE derivative_face_lower (f, df)
  USE grid_m
  implicit none
  real, dimension(mx,my,mz):: f, df
  integer:: iy,iz

  if (mpi_z > 0) return

  do iz=1,lb
    do iy=1,my
      f(:,iy,iz) = f(:,iy,lb+1) - df(:,iy,lb)*(zm(lb)-zm(iz))
    end do
  end do
END

!-----------------------------------------------------------------------
SUBROUTINE derivative_face_upper (f, df)
  USE grid_m
  implicit none
  real, dimension(mx,my,mz):: f, df
  integer:: iy,iz

  if (mpi_z < mpi_mz-1) return

  do iz=ub+1,mz
    do iy=1,my
      f(:,iy,iz) = f(:,iy,ub) + df(:,iy,ub)*(zm(iz)-zm(ub))
    end do
  end do
END

!-----------------------------------------------------------------------
SUBROUTINE derivative_face (f, df)
  USE grid_m
  implicit none
  real, dimension(mx,my,mz):: f, df

  call derivative_face_lower (f, df)
  call derivative_face_upper (f, df)
END

!-----------------------------------------------------------------------
SUBROUTINE extrapolate_center_lower(f)
  USE grid_m
  implicit none
  real, dimension(mx,my,mz):: f
  integer:: iy,iz

  if (mpi_z > 0) return

  do iz=1,lb-1
    do iy=1,my
      f(:,iy,iz) = 2.*f(:,iy,lb)-f(:,iy,lb+(lb-iz))
    end do
  end do
END

!-----------------------------------------------------------------------
SUBROUTINE extrapolate_center_upper(f)
  USE grid_m
  implicit none
  real, dimension(mx,my,mz):: f
  integer:: iy,iz

  if (mpi_z < mpi_mz-1) return

  do iz=ub+1,mz
    do iy=1,my
      f(:,iy,iz) = 2.*f(:,iy,ub)-f(:,iy,ub-(iz-ub))
    end do
  end do
END

!-----------------------------------------------------------------------
SUBROUTINE extrapolate_center(f)
  USE grid_m
  implicit none
  real, dimension(mx,my,mz):: f

  call extrapolate_center_lower(f)
  call extrapolate_center_upper(f)
END

!-----------------------------------------------------------------------
SUBROUTINE extrapolate_constrained_center_lower(f)
  USE grid_m
  implicit none
  real, dimension(mx,my,mz):: f
  integer:: iy,iz
  real p

  if (mpi_z == 0) then
    do iz=1,lb-1
    do iy=1,my
      p = 1.-((lb-iz)/(lb-3.))**2
      f(:,iy,iz) = p*(f(:,iy,lb)-f(:,iy,lb+(lb-iz)))+f(:,iy,lb)
    end do
    end do
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE extrapolate_constrained_center_upper(f)
  USE grid_m
  implicit none
  real, dimension(mx,my,mz):: f
  integer:: iy,iz
  real p

  if (mpi_z == mpi_mz-1) then
    do iz=ub+1,mz
    do iy=1,my
      p = 1.-((ub-iz)/real(mz-ub-2))**2
      f(:,iy,iz) = p*(f(:,iy,ub)-f(:,iy,ub+(ub-iz)))+f(:,iy,ub)
    end do
    end do
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE extrapolate_constrained_center(f)
  USE grid_m
  implicit none
  real, dimension(mx,my,mz):: f

  call extrapolate_constrained_center_lower(f)
  call extrapolate_constrained_center_upper(f)
END


!-----------------------------------------------------------------------
SUBROUTINE extrapolate_center_log_lower(f)
  USE grid_m
  implicit none
  real, dimension(mx,my,mz):: f
  integer:: iy,iz

  if (mpi_z > 0) return

  do iz=1,lb-1
    do iy=1,my
      f(:,iy,iz) = exp(2.*alog(f(:,iy,lb))-alog(f(:,iy,lb+(lb-iz))))
    end do
  end do
END

!-----------------------------------------------------------------------
SUBROUTINE extrapolate_center_log_upper(f)
  USE grid_m
  implicit none
  real, dimension(mx,my,mz):: f
  integer:: iy,iz

  if (mpi_z < mpi_mz-1) return

  do iz=ub+1,mz
    do iy=1,my
      f(:,iy,iz) = exp(2.*alog(f(:,iy,ub))-alog(f(:,iy,ub-(iz-ub))))
    end do
  end do
END

!-----------------------------------------------------------------------
SUBROUTINE extrapolate_center_log(f)
  USE grid_m
  implicit none
  real, dimension(mx,my,mz):: f

  call extrapolate_center_log_lower(f)
  call extrapolate_center_log_upper(f)
END

!-----------------------------------------------------------------------
SUBROUTINE vanishing_face_lower(f)
  USE grid_m
  implicit none
  real, dimension(mx,my,mz):: f
  integer:: iy,iz

  if (mpi_z > 0) return

  do iz=1,lb
    do iy=1,my
      f(:,iy,iz) = (iy-1)/float(lb)*f(:,iy,lb+1)
    end do
  end do
END

!-----------------------------------------------------------------------
SUBROUTINE vanishing_face_upper(f)
  USE grid_m
  implicit none
  real, dimension(mx,my,mz):: f
  integer:: iy,iz

  if (mpi_z < mpi_mz-1) return

  do iz=ub+1,mz
    do iy=1,my
      f(:,iy,iz) = (mz-iz)/float(mz-ub)*f(:,iy,ub)
    end do
  end do
END

!-----------------------------------------------------------------------
SUBROUTINE vanishing_face(f)
  USE grid_m
  implicit none
  real, dimension(mx,my,mz):: f

  call vanishing_face_lower(f)
  call vanishing_face_upper(f)
END

!-----------------------------------------------------------------------
SUBROUTINE vanishing_center_lower(f)
  USE grid_m
  implicit none
  real, dimension(mx,my,mz):: f
  integer:: iy,iz

  if (mpi_z > 0) return

  do iz=1,lb-1
    do iy=1,my
      f(:,iy,iz) = iz/float(lb)*f(:,iy,lb)
    end do
  end do
END

!-----------------------------------------------------------------------
SUBROUTINE vanishing_center_upper(f)
  USE grid_m
  implicit none
  real, dimension(mx,my,mz):: f
  integer:: iy,iz

  if (mpi_z < mpi_mz-1) return

  do iz=ub+1,mz
    do iy=1,my
      f(:,iy,iz) = (mz+1-iz)/float(mz+1-ub)*f(:,iy,ub)
    end do
  end do
END

!-----------------------------------------------------------------------
SUBROUTINE vanishing_center(f)
  USE grid_m
  implicit none
  real, dimension(mx,my,mz):: f

  call vanishing_center_lower(f)
  call vanishing_center_upper(f)
END

!-----------------------------------------------------------------------
SUBROUTINE mixed_center_lower(f,fl)
  USE grid_m
  implicit none
  real, dimension(mx,my,mz):: f
  integer:: iy,iz
  real fl

  if (mpi_z > 0) return

  do iz=1,lb-1
    do iy=1,my
      f(:,iy,iz) = (1.-(zm(lb)-zm(iz))/fl)*f(:,iy,lb)
    end do
  end do
END

!-----------------------------------------------------------------------
SUBROUTINE mixed_center_upper(f,fl)
  USE grid_m
  implicit none
  real, dimension(mx,my,mz):: f
  integer:: iy,iz
  real fl

  if (mpi_z < mpi_mz-1) return

  do iz=ub+1,mz
    do iy=1,my
      f(:,iy,iz) = (1.-(zm(iz)-zm(ub))/fl)*f(:,iy,ub)
    end do
  end do
END

!-----------------------------------------------------------------------
SUBROUTINE mixed_center(f,fl)
  USE grid_m
  implicit none
  real, dimension(mx,my,mz):: f
  real fl

  call mixed_center_lower(f,fl)
  call mixed_center_upper(f,fl)
END

!-----------------------------------------------------------------------
SUBROUTINE extrapolate_constrained_face_lower(f)
  USE grid_m
  implicit none
  real, dimension(mx,my,mz):: f
  real:: flb 
  integer:: ix,iy,iz
  real p

  if (mpi_z == 0) then
    do iy=1,my
    do ix=1,mx
      flb = 2./8.*(15.*f(ix,iy,lb+1)-10.*f(ix,iy,lb+2)+3.*f(ix,iy,lb+3))
      do iz=1,lb
        p = 1.-((lb+1.-iz)/real(lb))**2
        f(ix,iy,iz) = p*(flb-f(ix,iz,lb+1+(lb-iz)))
      end do
    end do
    end do
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE extrapolate_constrained_face_upper(f)
  USE grid_m
  implicit none
  real, dimension(mx,my,mz):: f
  real fub
  integer:: ix,iy,iz
  real p

  if (mpi_z == mpi_mz-1) then
    do iy=1,my
    do ix=1,mx
      fub = 2./8.*(15.*f(ix,iy,ub)-10.*f(ix,iy,ub-1)+3.*f(ix,iy,ub-2))
      do iz=ub+1,mz
	p = 1.-((iz-ub)/real(mz-ub))**2
        f(ix,iy,iz) = p*(fub-f(ix,iz,ub+1-(iz-ub)))
      end do
    end do
    end do
  end if
END

!-----------------------------------------------------------------------
SUBROUTINE extrapolate_face_constrained(f)
  USE grid_m
  implicit none
  real, dimension(mx,my,mz):: f

  call extrapolate_constrained_face_lower(f)
  call extrapolate_constrained_face_upper(f)
END

!-----------------------------------------------------------------------
SUBROUTINE extrapolate_face_lower(f)
  USE grid_m
  implicit none
  real, dimension(mx,my,mz):: f
  real:: flb 
  integer:: ix,iy,iz

  if (mpi_z > 0) return

  do iy=1,my
    do ix=1,mx
      flb = 2./8.*(15.*f(ix,iy,lb+1)-10.*f(ix,iy,lb+2)+3.*f(ix,iy,lb+3))
      do iz=1,lb
        f(ix,iy,iz) = flb-f(ix,iy,lb+1+(lb-iy))
      end do
    end do
  end do
END

!-----------------------------------------------------------------------
SUBROUTINE extrapolate_face_upper(f)
  USE grid_m
  implicit none
  real, dimension(mx,my,mz):: f
  real fub
  integer:: ix,iy,iz

  if (mpi_z < mpi_mz-1) return

  do iy=1,my
    do ix=1,mx
      fub = 2./8.*(15.*f(ix,iy,ub)-10.*f(ix,iy,ub-1)+3.*f(ix,iy,ub-2))
      do iz=ub+1,mz
        f(ix,iy,iz) = fub-f(ix,iy,ub+1-(iz-ub))
      end do
    end do
  end do
END

!-----------------------------------------------------------------------
SUBROUTINE extrapolate_face(f)
  USE grid_m
  implicit none
  real, dimension(mx,my,mz):: f

  call extrapolate_face_lower(f)
  call extrapolate_face_upper(f)
END

!-----------------------------------------------------------------------
SUBROUTINE extrapolate_face_value_lower(f,b)
  USE grid_m
  implicit none
  real, dimension(mx,my,mz):: f
  real, dimension(mx,my):: b
  integer:: ix,iy,iz

  if (mpi_z > 0) return

  do iz=1,lb
    do iy=1,my
      f(:,iy,iz) = 2.*b(:,iy)-f(:,iy,lb+1+(lb-iz))
    end do
  end do
END

!-----------------------------------------------------------------------
SUBROUTINE extrapolate_face_value_upper(f,b)
  USE grid_m
  implicit none
  real, dimension(mx,my,mz):: f
  real, dimension(mx,my):: b
  integer:: ix,iy,iz

  if (mpi_z < mpi_mz-1) return

  do iz=ub+1,mz
    do iy=1,my
      f(:,iy,iz) = 2.*b(:,iy)-f(:,iz,ub+1-(iz-ub))
    end do
  end do
END

!-----------------------------------------------------------------------
SUBROUTINE extrapolate_face_value(f,flb,fub)
  USE grid_m
  implicit none
  real, dimension(mx,my,mz):: f
  real, dimension(mx,my):: flb,fub

  call extrapolate_face_value_lower(f,flb)
  call extrapolate_face_value_upper(f,fub)
END


!-----------------------------------------------------------------------
SUBROUTINE value_face_lower(f,fl)
  USE grid_m
  implicit none
  real, dimension(mx,my,mz):: f
  real, dimension(mx,my):: fl
  integer:: iy,iz

  if (mpi_z > 0) return

  do iz=1,lb
    do iy=1,my
      f(:,iy,iz) = fl(:,iy) + (lb+0.5-iz)*2.*(fl(:,iy)-f(:,iy,lb+1))
    end do
  end do
END

!-----------------------------------------------------------------------
SUBROUTINE value_face_upper(f,fu)
  USE grid_m
  implicit none
  real, dimension(mx,my,mz):: f
  real, dimension(mx,my):: fu
  integer:: iy,iz

  if (mpi_z < mpi_mz-1) return

  do iz=ub+1,mz
    do iy=1,my
      f(:,iy,iz) = fu(:,iy) + (iz-ub-0.5)*2.*(fu(:,iy)-f(:,iy,ub))
    end do
  end do
END

!-----------------------------------------------------------------------
SUBROUTINE value_face(f,fl,fu)
  USE grid_m
  implicit none
  real, dimension(mx,my,mz):: f
  real, dimension(mx,my):: fl,fu

  call value_face_lower(f,fl)
  call value_face_upper(f,fu)
END

!-----------------------------------------------------------------------
SUBROUTINE boundary_test
  USE grid_m
  real, allocatable, dimension(:,:,:):: f
  integer  :: iy
  mx=15
  my=mx
  mz=mx
  lb=6
  ub=mz-5
  allocate (f(mx,my,mz))

  do iz=1,mz
    f(:,:,iz)=cos((iz-lb)*2.*pi/(ub-lb))
  end do

  call symmetric_center(f)
  print '(a/15f8.4)','s_c',f(1,1,:)  
  call antisymmetric_center(f)
  print '(a/15f8.4)','a_c',f(1,1,:)  
  call symmetric_face(f)
  print '(a/15f8.4)','s_f',f(1,1,:)  
  call antisymmetric_face(f)
  print '(a/15f8.4)','a_f',f(1,1,:)
  
END
