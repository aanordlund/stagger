! $Id: stagger.f90,v 1.8 2009/10/07 10:20:58 aake Exp $

MODULE stagger_m
  real, dimension(:), allocatable:: dizdzdn, dizdzup
  logical do_stretch
CONTAINS

!***********************************************************************
FUNCTION xup(f) RESULT(fp)
!
!  f is centered on (i-.5,j,k), fifth order stagger
!
  use grid_m, only: mx, my, mz
  implicit none
  integer i, j, k
  real a, b, c
  real, dimension(mx,my,mz):: f, fp
  real, dimension(2,my,mz):: g
  real, dimension(3,my,mz):: h
!-----------------------------------------------------------------------
  if (mx.le.5) then
    fp = f
    return
  end if
  c = 3./256.
  b = -25./256.
  a = .5-b-c
!
  call mpi_send_x (f, g, 2, h, 3)
  do k=1,mz
   do j=1,my
    fp(mx-2,j,k) = ( &
                   a*(f(mx-2,j,k)+f(mx-1,j,k)) &
                 + b*(f(mx-3,j,k)+f(mx  ,j,k)) &
                 + c*(f(mx-4,j,k)+h(1   ,j,k)))
    fp(mx-1,j,k) = ( &
                   a*(f(mx-1,j,k)+f(mx  ,j,k)) &
                 + b*(f(mx-2,j,k)+h(1   ,j,k)) &
                 + c*(f(mx-3,j,k)+h(2   ,j,k)))
    fp(mx  ,j,k) = ( &
                   a*(f(mx  ,j,k)+h(1   ,j,k)) &
                 + b*(f(mx-1,j,k)+h(2   ,j,k)) &
                 + c*(f(mx-2,j,k)+h(3   ,j,k)))
    fp(1   ,j,k) = ( &
                   a*(f(1   ,j,k)+f(2   ,j,k)) &
                 + b*(g(2   ,j,k)+f(3   ,j,k)) &
                 + c*(g(1   ,j,k)+f(4   ,j,k)))
    fp(2   ,j,k) = ( &
                   a*(f(2   ,j,k)+f(3   ,j,k)) &
                 + b*(f(1   ,j,k)+f(4   ,j,k)) &
                 + c*(g(2   ,j,k)+f(5   ,j,k)))
   end do
   do j=1,my
    do i=3,mx-3
      fp(i  ,j,k) = ( &
                    a*(f(i  ,j,k)+f(i+1 ,j,k)) &
                  + b*(f(i-1,j,k)+f(i+2 ,j,k)) &
                  + c*(f(i-2,j,k)+f(i+3 ,j,k)))
    end do
   end do
  end do
END FUNCTION

!***********************************************************************
FUNCTION xup1(f) RESULT(fp)
!
!  f is centered on (i-.5,j,k), first order stagger
!
  use grid_m, only: mx, my, mz
  implicit none
  integer i, j, k
  real, dimension(mx,my,mz):: f, fp
  real, dimension(1,my,mz):: h
!-----------------------------------------------------------------------
  if (mx.le.5) then
    fp = f
    return
  end if
!
  call mpi_send_x (f, h, 0, h, 1)
  do k=1,mz
   do j=1,my
    fp(mx  ,j,k) =  ( &
                    0.5*(f(mx,j,k)+h(  1,j,k)))
   end do
   do j=1,my
    do i=1,mx-1
      fp(i  ,j,k) = ( &
                    0.5*(f(i ,j,k)+f(i+1,j,k)))
    end do
   end do
  end do
END FUNCTION

!***********************************************************************
FUNCTION xdn(f) RESULT(fp)
!
!  f is centered on (i,j,k), fifth order stagger
!
  use grid_m, only: mx, my, mz
  implicit none
  integer i, j, k
  real a, b, c
  real, dimension(mx,my,mz):: f, fp
  real, dimension(3,my,mz):: g
  real, dimension(2,my,mz):: h
!-----------------------------------------------------------------------
  if (mx.le.5) then
    fp = f
    return
  end if
  c = 3./256.
  b = -25./256.
  a = .5-b-c
!
  call mpi_send_x (f, g, 3, h, 2)
  do k=1,mz
   do j=1,my
    fp(mx-1,j,k) = ( &
                   a*(f(mx-2,j,k)+f(mx-1,j,k)) &
                 + b*(f(mx-3,j,k)+f(mx  ,j,k)) &
                 + c*(f(mx-4,j,k)+h(1   ,j,k)))
    fp(mx  ,j,k) = ( &
                   a*(f(mx-1,j,k)+f(mx  ,j,k)) &
                 + b*(f(mx-2,j,k)+h(1   ,j,k)) &
                 + c*(f(mx-3,j,k)+h(2   ,j,k)))
    fp(1  ,j,k) = ( &
                   a*(g(3   ,j,k)+f(1   ,j,k)) &
                 + b*(g(2   ,j,k)+f(2   ,j,k)) &
                 + c*(g(1   ,j,k)+f(3   ,j,k)))
    fp(2  ,j,k) = ( &
                   a*(f(1   ,j,k)+f(2   ,j,k)) &
                 + b*(g(3   ,j,k)+f(3   ,j,k)) &
                 + c*(g(2   ,j,k)+f(4   ,j,k)))
    fp(3  ,j,k) = ( &
                   a*(f(2   ,j,k)+f(3   ,j,k)) &
                 + b*(f(1   ,j,k)+f(4   ,j,k)) &
                 + c*(g(3   ,j,k)+f(5   ,j,k)))
   end do
   do j=1,my
    do i=4,mx-2
      fp(i,j,k) = ( &
                   a*(f(i   ,j,k)+f(i-1 ,j,k)) &
                 + b*(f(i+1 ,j,k)+f(i-2 ,j,k)) &
                 + c*(f(i+2 ,j,k)+f(i-3 ,j,k)))
    end do
   end do
  end do
END FUNCTION

!***********************************************************************
FUNCTION xdn1(f) RESULT(fp)
!
!  f is centered on (i,j,k), first order stagger
!
  use grid_m, only: mx, my, mz
  implicit none
  integer i, j, k
  real, dimension(mx,my,mz):: f, fp
  real, dimension(1,my,mz):: g
!-----------------------------------------------------------------------
  if (mx.le.5) then
    fp = f
    return
  end if
!
  call mpi_send_x (f, g, 1, g, 0)
  do k=1,mz
   do j=1,my
    fp(1  ,j,k) = ( &
                   0.5*(g(1,j,k)+f(1   ,j,k)))
   end do
   do j=1,my
    do i=2,mx
      fp(i,j,k) = ( &
                   0.5*(f(i,j,k)+f(i-1,j,k)))
    end do
   end do
  end do
END FUNCTION

!***********************************************************************
FUNCTION ddxup(f) RESULT(fp)
  use grid_m, only: mx, my, mz, dx
  implicit none
  integer i, j, k
  real a, b, c
  real, dimension(mx,my,mz):: f, fp
  real, dimension(2,my,mz):: g
  real, dimension(3,my,mz):: h
!-----------------------------------------------------------------------
  if (mx.le.5) then
    fp = 0.
    return
  end if
!
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)/dx
  b = b/dx
  c = c/dx

  call mpi_send_x (f, g, 2, h, 3)
  do k=1,mz
   do j=1,my
    fp(mx-2,j,k) = ( &
                 + c*(h(1   ,j,k)-f(mx-4,j,k)) &
                 + b*(f(mx  ,j,k)-f(mx-3,j,k)) &
                 + a*(f(mx-1,j,k)-f(mx-2,j,k)))
    fp(mx-1,j,k) = ( &
                 + c*(h(2   ,j,k)-f(mx-3,j,k)) &
                 + b*(h(1   ,j,k)-f(mx-2,j,k)) &
                 + a*(f(mx  ,j,k)-f(mx-1,j,k)))
    fp(mx  ,j,k) = ( &
                 + c*(h(3   ,j,k)-f(mx-2,j,k)) &
                 + b*(h(2   ,j,k)-f(mx-1,j,k)) &
                 + a*(h(1   ,j,k)-f(mx  ,j,k)))
    fp(1  ,j,k) = ( &
                 + c*(f(4   ,j,k)-g(1   ,j,k)) &
                 + b*(f(3   ,j,k)-g(2   ,j,k)) &
                 + a*(f(2   ,j,k)-f(1   ,j,k)))
    fp(2  ,j,k) = ( &
                 + c*(f(5   ,j,k)-g(2   ,j,k)) &
                 + b*(f(4   ,j,k)-f(1   ,j,k)) &
                 + a*(f(3   ,j,k)-f(2   ,j,k)))
   end do
   do j=1,my
    do i=3,mx-3
      fp(i  ,j,k) = ( &
                 + c*(f(i+3 ,j,k)-f(i-2 ,j,k)) &
                 + b*(f(i+2 ,j,k)-f(i-1 ,j,k)) &
                 + a*(f(i+1 ,j,k)-f(i   ,j,k)))
    end do
  end do
  end do
END FUNCTION

!***********************************************************************
FUNCTION ddxup1(f) RESULT(fp)
  use grid_m, only: mx, my, mz, dx
  implicit none
  real a
  integer i, j, k
  real, dimension(mx,my,mz):: f, fp
  real, dimension(1,my,mz):: g
!-----------------------------------------------------------------------
  if (mx.le.5) then
    fp = 0.
    return
  end if
  a = 1./dx

  call mpi_send_x (f, g, 0, g, 1)
  do k=1,mz
   do j=1,my
    fp(mx  ,j,k) = ( &
                   a*(g(1  ,j,k)-f(mx,j,k)))
   end do
   do j=1,my
    do i=1,mx-1
      fp(i  ,j,k) = ( &
                   a*(f(i+1,j,k)-f(i ,j,k)))
    end do
  end do
  end do
END FUNCTION

!***********************************************************************
FUNCTION ddxdn(f) RESULT(fp)
  use grid_m, only: mx, my, mz, dx
  implicit none
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f, fp
  real, dimension(3,my,mz):: g
  real, dimension(2,my,mz):: h
!-----------------------------------------------------------------------
  if (mx.le.5) then
    fp = 0.
    return
  end if
!
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)/dx
  a1st = 1./dx
  b = b/dx
  c = c/dx

  call mpi_send_x (f, g, 3, h, 2)
  do k=1,mz
   do j=1,my
    fp(mx-1,j,k) = ( &
                 + c*(h(1   ,j,k)-f(mx-4,j,k)) &
                 + b*(f(mx  ,j,k)-f(mx-3,j,k)) &
                 + a*(f(mx-1,j,k)-f(mx-2,j,k)))
    fp(mx  ,j,k) = ( &
                 + c*(h(2   ,j,k)-f(mx-3,j,k)) &
                 + b*(h(1   ,j,k)-f(mx-2,j,k)) &
                 + a*(f(mx  ,j,k)-f(mx-1,j,k)))
    fp(1   ,j,k) = ( &
                 + c*(f(3   ,j,k)-g(1   ,j,k)) &
                 + b*(f(2   ,j,k)-g(2   ,j,k)) &
                 + a*(f(1   ,j,k)-g(3   ,j,k)))
    fp(2  ,j,k) = ( &
                 + c*(f(4   ,j,k)-g(2   ,j,k)) &
                 + b*(f(3   ,j,k)-g(3   ,j,k)) &
                 + a*(f(2   ,j,k)-f(1   ,j,k)))
    fp(3  ,j,k) = ( &
                 + c*(f(5   ,j,k)-g(3   ,j,k)) &
                 + b*(f(4   ,j,k)-f(1   ,j,k)) &
                 + a*(f(3   ,j,k)-f(2   ,j,k)))
   end do
   do j=1,my
    do i=4,mx-2
      fp(i,j,k) = ( &
                 + c*(f(i+2 ,j,k)-f(i-3 ,j,k)) &
                 + b*(f(i+1 ,j,k)-f(i-2 ,j,k)) &
                 + a*(f(i   ,j,k)-f(i-1 ,j,k)))
    end do
   end do
  end do
END FUNCTION

!***********************************************************************
FUNCTION ddxdn1(f) RESULT(fp)
  use grid_m, only: mx, my, mz, dx
  implicit none
  real a
  integer i, j, k
  real, dimension(mx,my,mz):: f, fp
  real, dimension(1,my,mz):: g
!-----------------------------------------------------------------------
  if (mx.le.5) then
    fp = 0.
    return
  end if
  a = 1./dx

  call mpi_send_x (f, g, 1, g, 0)
  do k=1,mz
   do j=1,my
    fp(1   ,j,k) = ( &
                   a*(f(1,j,k)-g(1  ,j,k)))
   end do
   do j=1,my
    do i=2,mx
      fp(i,j,k) = ( &
                   a*(f(i,j,k)-f(i-1,j,k)))
    end do
   end do
  end do
END FUNCTION
!***********************************************************************
FUNCTION yup(f) RESULT(fp)
!
!  f is centered on (i,j-.5,k), fifth order stagger
!
  use grid_m, only: mx, my, mz
  implicit none
  real a, b, c
  integer i, j, k
  real, intent(in) , dimension(mx,my,mz):: f
  real,              dimension(mx,my,mz):: fp
  real, dimension(mx,2,mz):: g
  real, dimension(mx,3,mz):: h
!-----------------------------------------------------------------------
  if (my .lt. 5) then
    do k=1,mz
      fp(:,:,k) = f(:,:,k)
    end do
    return
  end if
  c = 3./256.
  b = -25./256.
  a = .5-b-c

  call mpi_send_y (f, g, 2, h, 3)
  do k=1,mz
    do j=3,my-3
     do i=1,mx
      fp(i,j  ,k) = ( &
                     a*(f(i,j   ,k)+f(i,j+1 ,k)) + &
                     b*(f(i,j-1 ,k)+f(i,j+2 ,k)) + &
                     c*(f(i,j-2 ,k)+f(i,j+3 ,k)))
     end do
    end do
    do i=1,mx
      fp(i,my-2,k) = ( &
                     a*(f(i,my-2,k)+f(i,my-1,k)) + &
                     b*(f(i,my-3,k)+f(i,my  ,k)) + &
                     c*(f(i,my-4,k)+h(i,1   ,k)))
      fp(i,my-1,k) = ( &
                     a*(f(i,my-1,k)+f(i,my  ,k)) + &
                     b*(f(i,my-2,k)+h(i,1   ,k)) + &
                     c*(f(i,my-3,k)+h(i,2   ,k)))
      fp(i,my  ,k) = ( &
                     a*(f(i,my  ,k)+h(i,1   ,k)) + &
                     b*(f(i,my-1,k)+h(i,2   ,k)) + &
                     c*(f(i,my-2,k)+h(i,3   ,k)))
      fp(i,1   ,k) = ( &
                     a*(f(i,1   ,k)+f(i,2   ,k)) + &
                     b*(g(i,2   ,k)+f(i,3   ,k)) + &
                     c*(g(i,1   ,k)+f(i,4   ,k)))
      fp(i,2   ,k) = ( &
                     a*(f(i,2   ,k)+f(i,3   ,k)) + &
                     b*(f(i,1   ,k)+f(i,4   ,k)) + &
                     c*(g(i,2   ,k)+f(i,5   ,k)))
    end do
  end do
END FUNCTION

!***********************************************************************
FUNCTION yup1(f) RESULT(fp)
!
!  f is centered on (i,j-.5,k), first order stagger
!
  use grid_m, only: mx, my, mz
  implicit none
  integer i, j, k
  real, intent(in) , dimension(mx,my,mz):: f
  real,              dimension(mx,my,mz):: fp
  real, dimension(mx,1,mz):: h
!-----------------------------------------------------------------------
  if (my .lt. 5) then
    do k=1,mz
      fp(:,:,k) = f(:,:,k)
    end do
    return
  end if

  call mpi_send_y (f, h, 0, h, 1)
  do k=1,mz
    do j=1,my-1
      do i=1,mx
        fp(i,j  ,k) = ( &
                     0.5*(f(i,j   ,k)+f(i,j+1 ,k)))
      end do
    end do
    do i=1,mx
      fp(i,my,k) = ( &
                     0.5*(h(i,1,k)+f(i,my,k)))
    end do
  end do
END FUNCTION

!***********************************************************************
FUNCTION ydn(f) RESULT(fp)
!
!  f is centered on (i,j,k), fifth order stagger
!
  use grid_m, only: mx, my, mz
  implicit none
  real a, b, c
  integer i, j, k
  real, dimension(mx,my,mz):: f, fp
  real, dimension(mx,3,mz):: g
  real, dimension(mx,2,mz):: h
!-----------------------------------------------------------------------
  if (my .lt. 5) then
    do k=1,mz
      fp(:,:,k) = f(:,:,k)
    end do
    return
  end if

  c = 3./256.
  b = -25./256.
  a = .5-b-c

  call mpi_send_y (f, g, 3, h, 2)
  do k=1,mz
    do j=4,my-2
     do i=1,mx
      fp(i,j,k) = ( &
                     a*(f(i,j   ,k)+f(i,j-1 ,k)) + &
                     b*(f(i,j+1 ,k)+f(i,j-2 ,k)) + &
                     c*(f(i,j+2 ,k)+f(i,j-3 ,k)))
     end do
    end do
    do i=1,mx
      fp(i,my-1,k) = ( &
                     a*(f(i,my-2,k)+f(i,my-1,k)) + &
                     b*(f(i,my-3,k)+f(i,my  ,k)) + &
                     c*(f(i,my-4,k)+h(i,1   ,k)))
      fp(i,my  ,k) = ( &
                     a*(f(i,my-1,k)+f(i,my  ,k)) + &
                     b*(f(i,my-2,k)+h(i,1   ,k)) + &
                     c*(f(i,my-3,k)+h(i,2   ,k)))
      fp(i,1   ,k) = ( &
                     a*(g(i,3   ,k)+f(i,1   ,k)) + &
                     b*(g(i,2   ,k)+f(i,2   ,k)) + &
                     c*(g(i,1   ,k)+f(i,3   ,k)))
      fp(i,2   ,k) = ( &
                     a*(f(i,1   ,k)+f(i,2   ,k)) + &
                     b*(g(i,3   ,k)+f(i,3   ,k)) + &
                     c*(g(i,2   ,k)+f(i,4   ,k)))
      fp(i,3   ,k) = ( &
                     a*(f(i,2   ,k)+f(i,3   ,k)) + &
                     b*(f(i,1   ,k)+f(i,4   ,k)) + &
                     c*(g(i,3   ,k)+f(i,5   ,k)))
    end do
  end do
END FUNCTION

!***********************************************************************
FUNCTION ydn1(f) RESULT(fp)
!
!  f is centered on (i,j,k), first order stagger
!
  use grid_m, only: mx, my, mz
  implicit none
  integer i, j, k
  real, dimension(mx,my,mz):: f, fp
  real, dimension(mx,1,mz):: g
!-----------------------------------------------------------------------
  if (my .lt. 5) then
    do k=1,mz
      fp(:,:,k) = f(:,:,k)
    end do
    return
  end if

  call mpi_send_y (f, g, 1, g, 0)
  do k=1,mz
    do j=2,my
     do i=1,mx
      fp(i,j,k) = ( &
                     0.5*(f(i,j   ,k)+f(i,j-1 ,k)))
     end do
    end do
    do i=1,mx
      fp(i,1   ,k) = ( &
                     0.5*(g(i,1   ,k)+f(i,1   ,k)))
    end do
  end do
END FUNCTION

!***********************************************************************
FUNCTION ddyup(f) RESULT(fp)
  use grid_m, only: mx, my, mz, dy
  implicit none
  real a, b, c
  integer i, j, k
  real, dimension(mx,my,mz):: f, fp
  real, dimension(mx,2,mz):: g
  real, dimension(mx,3,mz):: h
!-----------------------------------------------------------------------
  if (my .lt. 3) then
    fp = 0.
    return
  end if
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)/dy
  b = b/dy
  c = c/dy

  call mpi_send_y (f, g, 2, h, 3)
  do k=1,mz
    do i=1,mx
      fp(i,my-2,k) = ( &
                     c*(h(i,1   ,k)-f(i,my-4,k)) + &
                     b*(f(i,my  ,k)-f(i,my-3,k)) + &
                     a*(f(i,my-1,k)-f(i,my-2,k)) )
      fp(i,my-1,k) = ( &
                     c*(h(i,2   ,k)-f(i,my-3,k)) + &
                     b*(h(i,1   ,k)-f(i,my-2,k)) + &
                     a*(f(i,my  ,k)-f(i,my-1,k)) )
      fp(i,my  ,k) = ( &
                     c*(h(i,3   ,k)-f(i,my-2,k)) + &
                     b*(h(i,2   ,k)-f(i,my-1,k)) + &
                     a*(h(i,1   ,k)-f(i,my  ,k)) )
      fp(i,1   ,k) = ( &
                     c*(f(i,4   ,k)-g(i,1   ,k)) + &
                     b*(f(i,3   ,k)-g(i,2   ,k)) + &
                     a*(f(i,2   ,k)-f(i,1   ,k)) )
      fp(i,2   ,k) = ( &
                     c*(f(i,5   ,k)-g(i,2   ,k)) + &
                     b*(f(i,4   ,k)-f(i,1   ,k)) + &
                     a*(f(i,3   ,k)-f(i,2   ,k)) )
    end do
    do j=3,my-3
     do i=1,mx
      fp(i,j  ,k) = ( &
                     c*(f(i,j+3 ,k)-f(i,j-2 ,k)) + &
                     b*(f(i,j+2 ,k)-f(i,j-1 ,k)) + &
                     a*(f(i,j+1 ,k)-f(i,j   ,k)) )
     end do
    end do
  end do
END FUNCTION

!***********************************************************************
FUNCTION ddyup1(f) RESULT(fp)
  use grid_m, only: mx, my, mz, dy
  implicit none
  real a
  integer i, j, k
  real, dimension(mx,my,mz):: f, fp
  real, dimension(mx,1,mz):: g
!-----------------------------------------------------------------------
  if (my .lt. 3) then
    fp = 0.
    return
  end if
  a = 1./dy

  call mpi_send_y (f, g, 0, g, 1)
  do k=1,mz
    do i=1,mx
      fp(i,my,k) = ( &
                     a*(g(i,1  ,k)-f(i,my,k)))
    end do
    do j=1,my-1
      do i=1,mx
        fp(i,j  ,k) = ( &
                     a*(f(i,j+1 ,k)-f(i,j   ,k)))
     end do
    end do
  end do
END FUNCTION

!***********************************************************************
FUNCTION ddydn(f) RESULT(fp)
  use grid_m, only: mx, my, mz, dy
  implicit none
  real a, b, c
  integer i, j, k
  real, dimension(mx,my,mz):: f, fp
  real, dimension(mx,3,mz):: g
  real, dimension(mx,2,mz):: h
!-----------------------------------------------------------------------
  if (my .lt. 3) then
    fp = 0.
    return
  end if
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)/dy
  b = b/dy
  c = c/dy

  call mpi_send_y (f, g, 3, h, 2)
  do k=1,mz
    do i=1,mx
      fp(i,my-1,k) = ( &
                     c*(h(i,1   ,k)-f(i,my-4,k)) + &
                     b*(f(i,my  ,k)-f(i,my-3,k)) + &
                     a*(f(i,my-1,k)-f(i,my-2,k)) ) 
      fp(i,my  ,k) = ( &
                     c*(h(i,2   ,k)-f(i,my-3,k)) + &
                     b*(h(i,1   ,k)-f(i,my-2,k)) + &
                     a*(f(i,my  ,k)-f(i,my-1,k)) )
      fp(i,1  ,k) = ( &
                     c*(f(i,3   ,k)-g(i,1   ,k)) + &
                     b*(f(i,2   ,k)-g(i,2   ,k)) + &
                     a*(f(i,1   ,k)-g(i,3   ,k)) )
      fp(i,2   ,k) = ( &
                     c*(f(i,4   ,k)-g(i,2   ,k)) + &
                     b*(f(i,3   ,k)-g(i,3   ,k)) + &
                     a*(f(i,2   ,k)-f(i,1   ,k)) )
      fp(i,3   ,k) = ( &
                     c*(f(i,5   ,k)-g(i,3   ,k)) + &
                     b*(f(i,4   ,k)-f(i,1   ,k)) + &
                     a*(f(i,3   ,k)-f(i,2   ,k)) )
    end do
    do j=4,my-2
     do i=1,mx
      fp(i,j ,k) = ( &
                     c*(f(i,j+2 ,k)-f(i,j-3 ,k)) + &
                     b*(f(i,j+1 ,k)-f(i,j-2 ,k)) + &
                     a*(f(i,j   ,k)-f(i,j-1 ,k)) )
     end do
    end do
  end do
END FUNCTION

!***********************************************************************
FUNCTION ddydn1(f) RESULT(fp)
  use grid_m, only: mx, my, mz, dy
  implicit none
  real a
  integer i, j, k
  real, dimension(mx,my,mz):: f, fp
  real, dimension(mx,1,mz):: g
!-----------------------------------------------------------------------
  if (my .lt. 3) then
    fp = 0.
    return
  end if
  a = 1./dy

  call mpi_send_y (f, g, 1, g, 0)
  do k=1,mz
    do i=1,mx
      fp(i,1  ,k) = ( &
                     a*(f(i,1   ,k)-g(i,1   ,k)))
    end do
    do j=2,my
     do i=1,mx
      fp(i,j ,k) = ( &
                     a*(f(i,j   ,k)-f(i,j-1 ,k)))
     end do
    end do
  end do
END FUNCTION
!***********************************************************************
FUNCTION zup(f) RESULT(fp)
!
!  f is centered on (i,j,k-.5), fifth order stagger
!
  use grid_m, only: mx, my, mz
  implicit none
  real a, b, c
  integer i, j, k
  real, intent(in) , dimension(mx,my,mz):: f
  real,              dimension(mx,my,mz):: fp
  real,              dimension(mx,my, 2):: g
  real,              dimension(mx,my, 3):: h
!-----------------------------------------------------------------------
  if (mz .lt. 5) then
    do k=1,mz
      fp(:,:,k) = f(:,:,k)
    end do
    return
  end if
  c = 3./256.
  b = -25./256.
  a = .5-b-c

  call mpi_send_z (f, g, 2, h, 3)
  do k=3,mz-3
    do j=1,my
      do i=1,mx
        fp(i,j,k) = ( &
                     a*(f(i,j,k   )+f(i,j,k+1 )) + &
                     b*(f(i,j,k-1 )+f(i,j,k+2 )) + &
                     c*(f(i,j,k-2 )+f(i,j,k+3 )))
     end do
    end do
  end do
   do j=1,my
    do i=1,mx
      fp(i,j,mz-2) = ( &
                     a*(f(i,j,mz-2)+f(i,j,mz-1)) + &
                     b*(f(i,j,mz-3)+f(i,j,mz  )) + &
                     c*(f(i,j,mz-4)+h(i,j,1   )))
      fp(i,j,mz-1) = ( &
                     a*(f(i,j,mz-1)+f(i,j,mz  )) + &
                     b*(f(i,j,mz-2)+h(i,j,1   )) + &
                     c*(f(i,j,mz-3)+h(i,j,2   )))
      fp(i,j,mz  ) = ( &
                     a*(f(i,j,mz  )+h(i,j,1   )) + &
                     b*(f(i,j,mz-1)+h(i,j,2   )) + &
                     c*(f(i,j,mz-2)+h(i,j,3   )))
    end do
   end do
   do j=1,my
    do i=1,mx
      fp(i,j,1   ) = ( &
                     a*(f(i,j,1   )+f(i,j,2   )) + &
                     b*(g(i,j,2   )+f(i,j,3   )) + &
                     c*(g(i,j,1   )+f(i,j,4   )))
      fp(i,j,2   ) = ( &
                     a*(f(i,j,2   )+f(i,j,3   )) + &
                     b*(f(i,j,1   )+f(i,j,4   )) + &
                     c*(g(i,j,2   )+f(i,j,5   )))
    end do
   end do
END FUNCTION

!***********************************************************************
FUNCTION zup1(f) RESULT(fp)
!
!  f is centered on (i,j,k-.5), first order stagger
!
  use grid_m, only: mx, my, mz
  implicit none
  integer i, j, k
  real, intent(in) , dimension(mx,my,mz):: f
  real,              dimension(mx,my,mz):: fp
  real,              dimension(mx,my,1 ):: g, h
!-----------------------------------------------------------------------
  if (mz .lt. 5) then
    do k=1,mz
      fp(:,:,k) = f(:,:,k)
    end do
    return
  end if
!
  call mpi_send_z (f, g, 0, h, 1)
  do k=1,mz-1
    do j=1,my
      do i=1,mx
        fp(i,j,k  ) = ( &
                     0.5*(f(i,j,k   )+f(i,j,k+1 )))
      end do
    end do
  end do
   do j=1,my
    do i=1,mx
      fp(i,j,mz) = ( &
                     0.5*(h(i,j,1)+f(i,j,mz)))
    end do
   end do
END FUNCTION

!***********************************************************************
FUNCTION zdn(f) RESULT(fp)
!
!  f is centered on (i,j), fifth order stagger
!
  use grid_m, only: mx, my, mz
  implicit none
  real a, b, c
  integer i, j, k
  real, dimension(mx,my,mz):: f, fp
  real, dimension(mx,my, 3):: g
  real, dimension(mx,my, 2):: h
!-----------------------------------------------------------------------
  if (mz .lt. 5) then
    do k=1,mz
      fp(:,:,k) = f(:,:,k)
    end do
    return
  end if
  c = 3./256.
  b = -25./256.
  a = .5-b-c

  call mpi_send_z (f, g, 3, h, 2)
  do k=4,mz-2
    do j=1,my
     do i=1,mx
      fp(i,j,k) = ( &
                     a*(f(i,j,k   )+f(i,j,k-1 )) + &
                     b*(f(i,j,k+1 )+f(i,j,k-2 )) + &
                     c*(f(i,j,k+2 )+f(i,j,k-3 )))
     end do
    end do
  end do
   do j=1,my
    do i=1,mx
      fp(i,j,mz-1) = ( &
                     a*(f(i,j,mz-2)+f(i,j,mz-1)) + &
                     b*(f(i,j,mz-3)+f(i,j,mz  )) + &
                     c*(f(i,j,mz-4)+h(i,j,1   )))
      fp(i,j,mz  ) = ( &
                     a*(f(i,j,mz-1)+f(i,j,mz  )) + &
                     b*(f(i,j,mz-2)+h(i,j,1   )) + &
                     c*(f(i,j,mz-3)+h(i,j,2   )))
    end do
   end do
   do j=1,my
    do i=1,mx
      fp(i,j,1   ) = ( &
                     a*(g(i,j,3   )+f(i,j,1   )) + &
                     b*(g(i,j,2   )+f(i,j,2   )) + &
                     c*(g(i,j,1   )+f(i,j,3   )))
      fp(i,j,2   ) = ( &
                     a*(f(i,j,1   )+f(i,j,2   )) + &
                     b*(g(i,j,3   )+f(i,j,3   )) + &
                     c*(g(i,j,2   )+f(i,j,4   )))
      fp(i,j,3   ) = ( &
                     a*(f(i,j,2   )+f(i,j,3   )) + &
                     b*(f(i,j,1   )+f(i,j,4   )) + &
                     c*(g(i,j,3   )+f(i,j,5   )))
    end do
   end do
END FUNCTION

!***********************************************************************
FUNCTION zdn1(f) RESULT(fp)
!
!  f is centered on (i,j), first order stagger
!
  use grid_m, only: mx, my, mz
  implicit none
  integer i, j, k
  real, dimension(mx,my,mz):: f, fp
  real, dimension(mx,my,1 ):: g, h
!-----------------------------------------------------------------------
  if (mz .lt. 5) then
    do k=1,mz
      fp(:,:,k) = f(:,:,k)
    end do
    return
  end if
!
  call mpi_send_z (f, g, 1, h, 0)
  do k=2,mz
    do j=1,my
     do i=1,mx
      fp(i,j,k) = ( &
                     0.5*(f(i,j,k   )+f(i,j,k-1 )))
     end do
    end do
  end do
   do j=1,my
    do i=1,mx
      fp(i,j,1   ) = ( &
                     0.5*(g(i,j,1   )+f(i,j,1   )))
    end do
   end do
END FUNCTION
!***********************************************************************
FUNCTION ddzup(f) RESULT(fp)
!
!  f is centered on (i,j,k-.5), fifth order stagger
!
  use grid_m, only: mx, my, mz, dz
  implicit none
  real a, b, c
  integer i, j, k
  real, intent(in) , dimension(mx,my,mz):: f
  real,              dimension(mx,my,mz):: fp
  real,              dimension(mx,my, 2):: g
  real,              dimension(mx,my, 3):: h
!-----------------------------------------------------------------------
  if (mz .lt. 5) then
    do k=1,mz
      fp(:,:,k) = f(:,:,k)
    end do
    return
  end if
!
  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)
  if (.not. do_stretch) then
    a = a/dz
    b = b/dz
    c = c/dz
  end if
!
  call mpi_send_z (f, g, 2, h, 3)
  do k=3,mz-3
    do j=1,my
      do i=1,mx
        fp(i,j,k) = ( &
                     c*(f(i,j,k+3 )-f(i,j,k-2 )) + &
                     b*(f(i,j,k+2 )-f(i,j,k-1 )) + &
                     a*(f(i,j,k+1 )-f(i,j,k   )))
     end do
    end do
  end do

   do j=1,my
    do i=1,mx
      fp(i,j,mz-2) = ( &
                     c*(h(i,j,1   )-f(i,j,mz-4)) + &
                     b*(f(i,j,mz  )-f(i,j,mz-3)) + &
                     a*(f(i,j,mz-1)-f(i,j,mz-2)))
      fp(i,j,mz-1) = ( &
                     c*(h(i,j,2   )-f(i,j,mz-3)) + &
                     b*(h(i,j,1   )-f(i,j,mz-2)) + &
                     a*(f(i,j,mz  )-f(i,j,mz-1)))
      fp(i,j,mz  ) = ( &
                     c*(h(i,j,3   )-f(i,j,mz-2)) + &
                     b*(h(i,j,2   )-f(i,j,mz-1)) + &
                     a*(h(i,j,1   )-f(i,j,mz  )))
    end do
   end do
   do j=1,my
    do i=1,mx
      fp(i,j,1   ) = ( &
                     c*(f(i,j,4   )-g(i,j,1   )) + &
                     b*(f(i,j,3   )-g(i,j,2   )) + &
                     a*(f(i,j,2   )-f(i,j,1   )))
      fp(i,j,2   ) = ( &
                     c*(f(i,j,5   )-g(i,j,2   )) + &
                     b*(f(i,j,4   )-f(i,j,1   )) + &
                     a*(f(i,j,3   )-f(i,j,2   )))
    end do
   end do
!
   if (do_stretch) then
     do k=1,mz
       fp(:,:,k) = fp(:,:,k)*dizdzup(k)
     end do
   end if
END FUNCTION

!***********************************************************************
FUNCTION ddzup1(f) RESULT(fp)
!
!  f is centered on (i,j,k-.5), first order stagger
!
  use grid_m, only: mx, my, mz, dz
  implicit none
  real a
  integer i, j, k
  real, intent(in) , dimension(mx,my,mz):: f
  real,              dimension(mx,my,mz):: fp
  real,              dimension(mx,my, 1):: g, h
!-----------------------------------------------------------------------
  if (mz .lt. 5) then
    do k=1,mz
      fp(:,:,k) = f(:,:,k)
    end do
    return
  end if
!
  a = 1./dz
  if (do_stretch) a = 1.
!
  call mpi_send_z (f, g, 0, h, 1)
  do k=1,mz-1
    do j=1,my
      do i=1,mx
        fp(i,j,k  ) = ( &
                     a*(f(i,j,k+1)-f(i,j,k)))
      end do
    end do
  end do
   do j=1,my
    do i=1,mx
      fp(i,j,mz) = ( &
                     a*(h(i,j,1)-f(i,j,mz)))
    end do
   end do
!
   if (do_stretch) then
     do k=1,mz
       fp(:,:,k) = fp(:,:,k)*dizdzdn(k)
     end do
   end if
END FUNCTION

!***********************************************************************
FUNCTION ddzdn(f) RESULT(fp)
!
!  f is centered on (i,j), fifth order stagger
!
  use grid_m, only: mx, my, mz, dz
  implicit none
  real a, b, c, a1st
  integer i, j, k
  real, dimension(mx,my,mz):: f, fp
  real, dimension(mx,my, 3):: g
  real, dimension(mx,my, 2):: h
!-----------------------------------------------------------------------
  if (mz .lt. 5) then
    do k=1,mz
      fp(:,:,k) = f(:,:,k)
    end do
    return
  end if

  c = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3))
  b = (-1.-120.*c)/24.
  a = (1.-3.*b-5.*c)
  if (.not. do_stretch) then
    a = a/dz
    b = b/dz
    c = c/dz
  end if

  call mpi_send_z (f, g, 3, h, 2)
  do k=4,mz-2
    do j=1,my
     do i=1,mx
      fp(i,j,k) = ( &
                     c*(f(i,j,k+2 )-f(i,j,k-3 )) + &
                     b*(f(i,j,k+1 )-f(i,j,k-2 )) + &
                     a*(f(i,j,k   )-f(i,j,k-1 )))
     end do
    end do
  end do
   do j=1,my
    do i=1,mx
      fp(i,j,mz-1) = ( &
                     c*(h(i,j,1   )-f(i,j,mz-4)) + &
                     b*(f(i,j,mz  )-f(i,j,mz-3)) + &
                     a*(f(i,j,mz-1)-f(i,j,mz-2)))
      fp(i,j,mz  ) = ( &
                     c*(h(i,j,2   )-f(i,j,mz-3)) + &
                     b*(h(i,j,1   )-f(i,j,mz-2)) + &
                     a*(f(i,j,mz  )-f(i,j,mz-1)))
    end do
   end do
   do j=1,my
    do i=1,mx
      fp(i,j,1   ) = ( &
                     c*(f(i,j,3   )-g(i,j,1   )) + &
                     b*(f(i,j,2   )-g(i,j,2   )) + &
                     a*(f(i,j,1   )-g(i,j,3   )))
      fp(i,j,2   ) = ( &
                     c*(f(i,j,4   )-g(i,j,2   )) + &
                     b*(f(i,j,3   )-g(i,j,3   )) + &
                     a*(f(i,j,2   )-f(i,j,1   )))
      fp(i,j,3   ) = ( &
                     c*(f(i,j,5   )-g(i,j,3   )) + &
                     b*(f(i,j,4   )-f(i,j,1   )) + &
                     a*(f(i,j,3   )-f(i,j,2   )))
    end do
   end do
!
   if (do_stretch) then
     do k=1,mz
       fp(:,:,k) = fp(:,:,k)*dizdzdn(k)
     end do
   end if
END FUNCTION

!***********************************************************************
FUNCTION ddzdn1(f) RESULT(fp)
!
!  f is centered on (i,j), first order stagger
!
  use grid_m, only: mx, my, mz, dz
  implicit none
  real a
  integer i, j, k
  real, dimension(mx,my,mz):: f, fp
  real, dimension(mx,my, 1):: g, h
!-----------------------------------------------------------------------
  if (mz .lt. 5) then
    do k=1,mz
      fp(:,:,k) = f(:,:,k)
    end do
    return
  end if
!
  a = 1./dz
  if (do_stretch) a = 1.
!
  call mpi_send_z (f, g, 1, h, 0)
  do k=2,mz
    do j=1,my
     do i=1,mx
      fp(i,j,k) = ( &
                     a*(f(i,j,k)-f(i,j,k-1)))
     end do
    end do
  end do
   do j=1,my
    do i=1,mx
      fp(i,j,1   ) = ( &
                     a*(f(i,j,1)-g(i,j,1  )))
    end do
   end do
!
   if (do_stretch) then
     do k=1,mz
       fp(:,:,k) = fp(:,:,k)*dizdzdn(k)
     end do
   end if
END FUNCTION

!***********************************************************************
FUNCTION smooth3(f) RESULT(fp)
!
!  Three point smooth
!
  USE grid_m, only: mx, my, mz, scratch
  implicit none
  logical debug
  integer i,j,k
  real, dimension(mx,my,mz):: f, fp
  real, dimension(0:mx+1):: fx
  real, dimension(0:my+1):: fy
  real c3
  real, dimension(1,my,mz):: gx, hx
  real, dimension(mx,1,mz):: gy, hy
  real, dimension(mx,my,1):: gz, hz
!-----------------------------------------------------------------------
  c3=1./3.

  call mpi_send_x (f, gx, 1, hx, 1)
  allocate (scratch(mx,my,mz))
  do k=1,mz
   do j=1,my
    do i=1,mx
      fx(i)=f(i,j,k)
    end do
    fx(0)=gx(1,j,k)
    fx(mx+1)=hx(1,j,k)
    do i=1,mx
      scratch(i,j,k)=c3*(fx(i-1)+fx(i)+fx(i+1))
    end do
   end do
  end do

  call mpi_send_y (scratch, gy, 1, hy, 1)
  do k=1,mz
   do i=1,mx
    do j=1,my
      fy(j)=scratch(i,j,k)
    end do
    fy(0)=gy(i,1,k)
    fy(my+1)=hy(i,1,k)
    do j=1,my
      scratch(i,j,k)=c3*(fy(j-1)+fy(j)+fy(j+1))
    end do
   end do
  end do

  call mpi_send_z (scratch, gz, 1, hz, 1)
  do k=2,mz-1
   do j=1,my
    do i=1,mx
      fp(i,j,k)=c3*(scratch(i,j,k)+scratch(i,j,k-1)+scratch(i,j,k+1))
    end do
   end do
  end do
  if (mz > 1) then
   do j=1,my
    do i=1,mx
      fp(i,j,1)=c3*(scratch(i,j,1)+gz(i,j,1)+scratch(i,j,2))
    end do
   end do
   do j=1,my
    do i=1,mx
      fp(i,j,mz)=c3*(scratch(i,j,mz)+scratch(i,j,mz-1)+hz(i,j,1))
    end do
   end do
  end if
  deallocate (scratch)
END FUNCTION

!***********************************************************************
FUNCTION smooth3t(f) RESULT(fp)
!
!  Three point smooth
!
  USE grid_m, only: mx, my, mz, scratch
  implicit none
  integer i,j,k
  real, dimension(mx,my,mz):: f, fp
  real, dimension(0:mx+1):: fx
  real, dimension(0:my+1):: fy
  real, dimension(mx,my,1):: gz, hz
  real c3
  real, dimension(1,my,mz):: gx, hx
  real, dimension(mx,1,mz):: gy, hy
!-----------------------------------------------------------------------
  c3=1./3.

  call mpi_send_x (f, gx, 1, hx, 1)
  allocate (scratch(mx,my,mz))

  do k=1,mz
   do j=1,my
    do i=1,mx
      fx(i)=f(i,j,k)
    end do
    fx(0)=gx(1,j,k)
    fx(mx+1)=hx(1,j,k)
    do i=1,mx
      scratch(i,j,k)=c3*(fx(i-1)+fx(i)+fx(i+1))
    end do
   end do
  end do

  call mpi_send_y (scratch, gy, 1, hy, 1)

  do k=1,mz
   do i=1,mx
    do j=1,my
      fy(j)=scratch(i,j,k)
    end do
    fy(0)=gy(i,1,k)
    fy(my+1)=hy(i,1,k)
    do j=1,my
      scratch(i,j,k)=c3*(fy(j-1)+fy(j)+fy(j+1))
    end do
   end do
  end do

  call mpi_send_z (scratch(:,:,1:mz), gz, 1, hz, 1)

  do k=2,mz-1
   do j=1,my
    do i=1,mx
      fp(i,j,k)=c3*(scratch(i,j,k)+scratch(i,j,k-1)+scratch(i,j,k+1))
    end do
   end do
  end do
  if (mz > 1) then
   do j=1,my
    do i=1,mx
      fp(i,j,1)=c3*(scratch(i,j,1)+gz(i,j,1)+scratch(i,j,2))
    end do
   end do
   do j=1,my
    do i=1,mx
      fp(i,j,mz)=c3*(scratch(i,j,mz)+scratch(i,j,mz-1)+hz(i,j,1))
    end do
   end do
  end if
  deallocate (scratch)
END FUNCTION

!***********************************************************************
FUNCTION max3(f) RESULT(fp)
!
!  Three point max
!
  USE grid_m, only: mx, my, mz, scratch
  implicit none
  logical debug
  integer i,j,k,km1,kp1
  real, dimension(mx,my,mz):: f, fp
  real, dimension(0:mx+1):: fx
  real, dimension(0:my+1):: fy
  real, dimension(mx,my,1):: gz, hz
  real, dimension(1,my,mz):: gx, hx
  real, dimension(mx,1,mz):: gy, hy
!-----------------------------------------------------------------------
  call mpi_send_x (f, gx, 1, hx, 1)
  allocate (scratch(mx,my,mz))

  do k=1,mz
   do j=1,my
    do i=1,mx
      fx(i)=f(i,j,k)
    end do
    fx(0)=gx(1,j,k)
    fx(mx+1)=hx(1,j,k)
    do i=1,mx
      scratch(i,j,k)=amax1(fx(i-1),fx(i),fx(i+1))
    end do
   end do
  end do

  call mpi_send_y (scratch, gy, 1, hy, 1)

  do k=1,mz
   do i=1,mx
    do j=1,my
      fy(j)=scratch(i,j,k)
    end do
    fy(0)=gy(i,1,k)
    fy(my+1)=hy(i,1,k)
    do j=1,my
      scratch(i,j,k)=amax1(fy(j-1),fy(j),fy(j+1))
    end do
   end do
  end do
 
  call mpi_send_z (scratch, gz, 1, hz, 1)

  do k=2,mz-1
   do j=1,my
    do i=1,mx
      fp(i,j,k)=amax1(scratch(i,j,k),scratch(i,j,k-1),scratch(i,j,k+1))
    end do
   end do
  end do
  if (mz > 1) then
   do j=1,my
    do i=1,mx
      fp(i,j,1)=amax1(scratch(i,j,1),gz(i,j,1),scratch(i,j,2))
    end do
   end do
   do j=1,my
    do i=1,mx
      fp(i,j,mz)=amax1(scratch(i,j,mz),hz(i,j,1),scratch(i,j,mz-1))
    end do
   end do
  end if
  deallocate (scratch)
END FUNCTION

!***********************************************************************
FUNCTION max5(f) RESULT(fp)
!
!  Three point max
!
  USE grid_m, only: mx, my, mz, scratch
  implicit none
  integer i,j,k
  real, dimension(mx,my,mz), intent(in) :: f
  real, dimension(mx,my,mz):: fp
  real, dimension(-1:mx+2):: fx
  real, dimension(-1:my+2):: fy
  real, dimension(mx,my,2):: gz, hz
  real, dimension(2,my,mz):: gx, hx
  real, dimension(mx,2,mz):: gy, hy
!-----------------------------------------------------------------------
  call mpi_send_x (f, gx, 2, hx, 2)
  allocate (scratch(mx,my,mz))

  do k=1,mz
   do j=1,my
    do i=1,mx
      fx(i)=f(i,j,k)
    end do
    fx(  -1)=gx(1,j,k)
    fx(   0)=gx(2,j,k)
    fx(mx+1)=hx(1,j,k)
    fx(mx+2)=hx(2,j,k)
    do i=1,mx
      scratch(i,j,k)=amax1(fx(i-2),fx(i-1),fx(i),fx(i+1),fx(i+2))
    end do
   end do
  end do

  call mpi_send_y (scratch, gy, 2, hy, 2)

  do k=1,mz
   if (my.gt.1) then
     do i=1,mx
      do j=1,my
        fy(j)=scratch(i,j,k)
      end do
      fy(  -1)=gy(i,1,k)
      fy(   0)=gy(i,2,k)
      fy(my+1)=hy(i,1,k)
      fy(my+2)=hy(i,2,k)
      do j=1,my
        scratch(i,j,k)=amax1(fy(j-2),fy(j-1),fy(j),fy(j+1),fy(j+2))
      end do
     end do
    end if
  end do

  call mpi_send_z (scratch, gz, 2, hz, 2)

  do k=3,mz-2
   do j=1,my
    do i=1,mx
      fp(i,j,k)=amax1(scratch(i,j,k-2),scratch(i,j,k-1),scratch(i,j,k),scratch(i,j,k+1),scratch(i,j,k+2))
    end do
   end do
  end do
  if (mz > 1) then
   do j=1,my
    do i=1,mx
      fp(i,j,1)=amax1(gz(i,j,1),gz(i,j,2),scratch(i,j,1),scratch(i,j,2),scratch(i,j,3))
      fp(i,j,2)=amax1(gz(i,j,2),scratch(i,j,1),scratch(i,j,2),scratch(i,j,3),scratch(i,j,4))
    end do
   end do
   do j=1,my
    do i=1,mx
      fp(i,j,mz-1)=amax1(scratch(i,j,mz-3),scratch(i,j,mz-2),scratch(i,j,mz-1),scratch(i,j,mz),hz(i,j,1))
      fp(i,j,mz  )=amax1(scratch(i,j,mz-2),scratch(i,j,mz-1),scratch(i,j,mz  ),hz(i,j,1 ),hz(i,j,2))
    end do
   end do
  end if
  deallocate (scratch)
END FUNCTION

!*******************************************************************************
END MODULE

!*******************************************************************************
SUBROUTINE init_stagger
  USE grid_m
  USE stagger_m
  implicit none
  real kx, ky, kz
  logical, save:: test=.false., first=.true.
  integer ix, iy, iz, iz0, iz1, order, mz1
  real, allocatable, dimension(:,:,:):: u
  real, allocatable, dimension(:):: sm, disdsdn, disdsup
  namelist /stagger/ order, test, do_stretch
  character(len=80):: id='$Id: stagger.f90,v 1.8 2009/10/07 10:20:58 aake Exp $'
!.......................................................................
  call print_id (id)

  if (.not.first) return
  first = .false.

  order = 6
  test = .false.
  do_stretch = .false.
  rewind(stdin); read(stdin,stagger)
  if (stdout>0) then; write(*,*)hl; write (*,stagger); endif
  if (master) write(2,stagger)

  if (do_stretch) then                                                  ! if stretched mesh
    read (1) mz1                                                        ! global size
    if (mz1 /= mz*mpi_mz) then
      if (master) print *,'inconsistent stretched mesh',mz1,mz,mpi_mz
      call shut_down ('error')
    end if
    allocate (sm(mz1), disdsdn(mz1), disdsup(mz1))                      ! global size arrays
    open (1, file=trim(name)//'.mesh', status='old')                    ! mesh file
    read (1,*) sm                                                       ! stretched mesh
    read (1,*) disdsdn                                                  ! stretched mesh dn-coefficient
    read (1,*) disdsup                                                  ! stretched mesh up-coefficient
    close (1)
    allocate (dizdzdn(mz1), dizdzup(mz))                                ! local arrays
    iz0 = mpi_z*mz                                                      ! first index
    iz1 = iz0+mz-1                                                      ! last index
    zm = sm(iz0:iz1)                                                    ! pick up local mesh
    dizdzdn = disdsdn(iz0:iz1)                                          ! pick up local mesh dn-coefficient
    dizdzup = disdsup(iz0:iz1)                                          ! pick up local mesh up-coefficient
    deallocate (sm, disdsdn, disdsup)                                   ! global size arrays
  end if

  if (.not.test) return

  allocate (u(mx,my,mz))

  kx = 2.*pi/sx; ky = 2.*pi/sy; kz = 2.*pi/sz
  do iz=1,mz; do iy=1,my; do ix=1,mx
    u(ix,iy,iz)  =    sin(kx*xm(ix))*sin(ky*ym(iy))*sin(kz*zm(iz))
    bx(ix,iy,iz) = kx*cos(kx*xm(ix))*sin(ky*ym(iy))*sin(kz*zm(iz))
    by(ix,iy,iz) = ky*sin(kx*xm(ix))*cos(ky*ym(iy))*sin(kz*zm(iz))
    bz(ix,iy,iz) = kz*sin(kx*xm(ix))*sin(ky*ym(iy))*cos(kz*zm(iz))
  end do; end do; end do

  ux = xup(ddxdn(u)) ; print*,' x',mpi_rank,maxval(abs(ux-bx))
  uy = yup(ddydn(u)) ; print*,' y',mpi_rank,maxval(abs(uy-by))
  uz = zup(ddzdn(u)) ; print*,' z',mpi_rank,maxval(abs(uz-bz))
  ux = xdn(ddxup(u)) ; print*,' x',mpi_rank,maxval(abs(ux-bx))
  uy = ydn(ddyup(u)) ; print*,' y',mpi_rank,maxval(abs(uy-by))
  uz = zdn(ddzup(u)) ; print*,' z',mpi_rank,maxval(abs(uz-bz))

  ux = xup1(ddxdn(u)); print*,'x1',mpi_rank,maxval(abs(ux-bx))
  uy = yup1(ddydn(u)); print*,'y1',mpi_rank,maxval(abs(uy-by))
  uz = zup1(ddzdn(u)); print*,'z1',mpi_rank,maxval(abs(uz-bz))
  ux = xdn1(ddxup(u)); print*,'x1',mpi_rank,maxval(abs(ux-bx))
  uy = ydn1(ddyup(u)); print*,'y1',mpi_rank,maxval(abs(uy-by))
  uz = zdn1(ddzup(u)); print*,'z1',mpi_rank,maxval(abs(uz-bz))

  ux = xup(ddxdn1(u)); print*,'x2',mpi_rank,maxval(abs(ux-bx))
  uy = yup(ddydn1(u)); print*,'y2',mpi_rank,maxval(abs(uy-by))
  uz = zup(ddzdn1(u)); print*,'z2',mpi_rank,maxval(abs(uz-bz))
  ux = xdn(ddxup1(u)); print*,'x2',mpi_rank,maxval(abs(ux-bx))
  uy = ydn(ddyup1(u)); print*,'y2',mpi_rank,maxval(abs(uy-by))
  uz = zdn(ddzup1(u)); print*,'z2',mpi_rank,maxval(abs(uz-bz))

  deallocate(u)
END SUBROUTINE
!*******************************************************************************
