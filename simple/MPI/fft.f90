! $Id: fft.f90,v 1.2 2009/10/07 10:54:31 aake Exp $
!----------------------------------------------------------------------
!
!  Three-dimensional Fourier transform routines, based on FFTPACK.
!  FFTPACK is transportable, and allows arbitrary dimensions (speed
!  will vary, of course).
!
!  CALL fft3df (f, ft, mx, my, mz)              ! forward transform
!  CALL fft3db (ft, f, mx, my, mz)              ! backward transform
!  CALL fft3d_k2 (k2, dx, dy, dz, mx, my, mz)   ! compute k2
!
!******************************************************************************
MODULE reshape_m
  USE mpi_m, only: mpi_comm_world, MPI_REAL, MPI_ANY_SOURCE, MPI_STATUS_SIZE
  USE grid_m , only: mx,my,mz,master,hl,mpi_mx,mpi_my,mpi_mz, &
                     mpi_rank,mpi_x,mpi_y,mpi_z
  implicit none
  logical :: first_time=.true.
  integer ix2, iy2, iz2, com3d
  integer com_x, com_y, com_z, err                                             ! communicators
  integer kx1, kx2, ky1, ky2, kz1, kz2, rank, req
  integer, dimension(3):: geom_x, geom_y, geom_z, geom_g, geom_l               ! geometries
  integer, dimension(3):: dim_x, dim_y, dim_z, dim_g, dim_l                    ! dimensions
  integer:: verbose=0
CONTAINS

!******************************************************************************
SUBROUTINE init_reshape
  implicit none
  logical periodic(3), reord
  namelist /reshape/ geom_x, geom_y, geom_z
!..............................................................................
  if (.not. first_time) return
  first_time = .false.
  rewind (10); read(10,reshape)
  if (master) then; write(*,*)hl;  write(*,reshape); endif
  periodic = .true.
  reord = .false.
  dim_l = (/ mx, my, mz /)
  geom_g = (/ mpi_mx, mpi_my, mpi_mz /)
  dim_g = dim_l*geom_g
  dim_x = dim_g/geom_x
  dim_y = dim_g/geom_y
  dim_z = dim_g/geom_z
  if (verbose > 0 .and. master) print*,'dim_l =',dim_l
  if (verbose > 0 .and. master) print*,'dim_g =',dim_g
  if (verbose > 0 .and. master) print*,'dim_x =',dim_x
  if (verbose > 0 .and. master) print*,'dim_y =',dim_y
  if (verbose > 0 .and. master) print*,'dim_z =',dim_z
  if (verbose > 2) print*,'rank,dim_x',mpi_rank,dim_x
  if (verbose > 2) print*,'rank,dim_y',mpi_rank,dim_y
  if (verbose > 2) print*,'rank,dim_z',mpi_rank,dim_z
  CALL MPI_CART_CREATE (mpi_comm_world, 3, geom_g, periodic, reord, com3d, err)
  call MPI_CART_CREATE (mpi_comm_world, 3, geom_x, periodic, reord, com_x, err)
  call MPI_CART_CREATE (mpi_comm_world, 3, geom_y, periodic, reord, com_y, err)
  call MPI_CART_CREATE (mpi_comm_world, 3, geom_z, periodic, reord, com_z, err)

  call test_reshape
END SUBROUTINE

!******************************************************************************
SUBROUTINE test_reshape
  implicit none
  integer i,j,k
  real, dimension(:,:,:), allocatable:: a, b
!..............................................................................
  allocate (a(dim_l(1),dim_l(2),dim_l(3)))
  do k=1,dim_l(3)
  do j=1,dim_l(2)
  do i=1,dim_l(1)
    a(i,j,k)=i-1+(j-1)*dim_l(1)+(k-1)*dim_l(1)*dim_l(2)
  end do
  end do
  end do
  if (verbose > 2) then
    do k=1,5; do j=1,5; print'(3i3,5f6.0)',mpi_rank,j,k,a(1:5,j,k); end do; end do
  end if
!
  allocate (b(dim_x(1),dim_x(2),dim_x(3)))
  call reshape_x (a, b)
  call deshape_x (b, a)
  deallocate (b)
  if (verbose > 1) print*,mpi_rank,'reshape_x done'
  if (verbose > 2) then
    do k=1,5; do j=1,5; print'(3i3,5f6.0)',mpi_rank,j,k,a(1:5,j,k); end do; end do
  end if
!
  allocate (b(dim_y(1),dim_y(2),dim_y(3)))
  call reshape_y (a, b)
  call deshape_y (b, a)
  deallocate (b)
  if (verbose > 1) print*,mpi_rank,'reshape_y done'
  if (verbose > 2) then
    do k=1,5; do j=1,5; print'(3i3,5f6.0)',mpi_rank,j,k,a(1:5,j,k); end do; end do
  end if
!
  allocate (b(dim_z(1),dim_z(2),dim_z(3)))
  call reshape_z (a, b)
  call deshape_z (b, a)
  deallocate (b)
  if (verbose > 1) print*,mpi_rank,'reshape_z done'
  if (verbose > 2) then
    do k=1,5; do j=1,5; print'(3i3,5f6.0)',mpi_rank,j,k,a(1:5,j,k); end do; end do
  end if
!
  if (master .and. verbose > 0) print*,'test_reshape done'
  deallocate (a)
END SUBROUTINE

!******************************************************************************
SUBROUTINE reshape_x (a, b)
  implicit none
  real, dimension(mx,my,mz):: a
  real, dimension(dim_x(1),dim_x(2),dim_x(3)):: b
  real, dimension(:,:,:,:,:), allocatable:: c
  integer dim(5), pos(3), coord2d(3), code
!------------------------------------------------------------------------------
!  Send loop, non-blocking
!------------------------------------------------------------------------------
  if (first_time) call init_reshape
  if (all(dim_x == dim_l)) then
    if (verbose > 1 .or. (master .and. (verbose > 0))) print*,mpi_rank,'x-shortcut'
    b = a
    return
  end if
  geom_l = geom_x/geom_g
  dim = (/ dim_l(1), dim_x(2), dim_x(3), geom_l(2), geom_l(3) /)               ! buffer dimensions
  if (master) then
    if (verbose > 1) print*,'x:geom_l',geom_l
    if (verbose > 0) print*,'x:buffer dim',dim
    if (verbose > 1) print*,'x:buffer size',product(dim),product(dim_l)
  end if
  allocate (c(dim(1),dim(2),dim(3),dim(4),dim(5)))                             ! send buffer
  do iy2=1,geom_l(2)                                                           ! loop over blocks
  do iz2=1,geom_l(3)                                                           ! loop over blocks
    pos = (/ 0, iy2-1, iz2-1 /)                                                ! block coordinates
    code = mpi_x                                                               ! where to put it
    coord2d = (/ 0, iy2-1 + mpi_y*geom_l(2), iz2-1 + mpi_z*geom_l(3) /)        ! 2D coordinates
    call send_block (1, com_x, pos, code, coord2d, dim_l, a, dim(1:3), &
                     c(1,1,1,iy2,iz2))                                         ! send buffer
  end do
  end do
!------------------------------------------------------------------------------
!  Recv loop
!------------------------------------------------------------------------------
  do ix2=1,geom_g(1)
    pos = (/ ix2-1, 0, 0 /)
    code = ix2-1
    call recv_block (1, com_x, pos, code, dim_x, b, dim(1:3))                  ! get the block
  end do
!------------------------------------------------------------------------------
!  Synchronize, deallocate
!------------------------------------------------------------------------------
  call barrier_mpi
  deallocate (c)
END SUBROUTINE

!******************************************************************************
SUBROUTINE reshape_y (a, b)
  implicit none
  real, dimension(mx,my,mz):: a
  real, dimension(dim_y(1),dim_y(2),dim_y(3)):: b
  real, dimension(:,:,:,:,:), allocatable:: c
  integer dim(5), pos(3), coord2d(3), code
!------------------------------------------------------------------------------
!  Send loop, non-blocking
!------------------------------------------------------------------------------
  if (first_time) call init_reshape
  if (all(dim_y == dim_l)) then
    if (verbose > 1 .or. (master .and. (verbose > 0))) print*,mpi_rank,'y-shortcut'
    b = a
    return
  end if
  geom_l = geom_y/geom_g
  dim = (/ dim_y(1), dim_l(2), dim_y(3), geom_l(1), geom_l(3) /)               ! buffer dimensions
  if (master) then
    if (verbose > 1) print*,'y:geom_l',geom_l
    if (verbose > 0) print*,'y:buffer dim',dim
    if (verbose > 1) print*,'y:buffer size',product(dim),product(dim_l)
  end if
  allocate (c(dim(1),dim(2),dim(3),dim(4),dim(5)))                             ! send buffer
  do iz2=1,geom_l(3)                                                           ! loop over blocks
  do ix2=1,geom_l(1)                                                           ! loop over blocks
    pos = (/ ix2-1, 0, iz2-1 /)                                                ! block coordinates
    code = mpi_y
    coord2d = (/ ix2-1 + mpi_x*geom_l(1),     0, iz2-1 + mpi_z*geom_l(3) /)    ! 2D coordinates
    call send_block (2, com_y, pos, code, coord2d, dim_l, a, dim(1:3), &
                     c(1,1,1,ix2,iz2))                                         ! send buffer
  end do
  end do
!------------------------------------------------------------------------------
!  Recv loop
!------------------------------------------------------------------------------
  do iy2=1,geom_g(2)
    pos = (/ 0, iy2-1, 0 /)
    code = iy2-1
    call recv_block (2, com_y, pos, code, dim_y, b, dim(1:3))                 ! get the block
  end do
!------------------------------------------------------------------------------
!  Synchronize, deallocate
!------------------------------------------------------------------------------
  call barrier_mpi
  deallocate (c)
END SUBROUTINE

!******************************************************************************
SUBROUTINE reshape_z (a, b)
  implicit none
  real, dimension(mx,my,mz):: a
  real, dimension(dim_z(1),dim_z(2),dim_z(3)):: b
  real, dimension(:,:,:,:,:), allocatable:: c
  integer dim(5), pos(3), coord2d(3), code
!------------------------------------------------------------------------------
!  Send loop, non-blocking
!------------------------------------------------------------------------------
  if (first_time) call init_reshape
  if (all(dim_z == dim_l)) then
    if (verbose > 1 .or. (master .and. (verbose > 0))) print*,mpi_rank,'z-shortcut'
    b = a
    return
  end if
  geom_l = geom_z/geom_g
  dim = (/ dim_z(1), dim_z(2), dim_l(3), geom_l(1), geom_l(2) /)               ! buffer dimensions
  if (master) then
    if (verbose > 1) print*,'z:geom_l',geom_l
    if (verbose > 0) print*,'z:buffer dim',dim
    if (verbose > 1) print*,'z:buffer size',product(dim),product(dim_l)
  end if
  allocate (c(dim(1),dim(2),dim(3),dim(4),dim(5)))                             ! send buffer
  do ix2=1,geom_l(1)                                                           ! loop over blocks
  do iy2=1,geom_l(2)                                                           ! loop over blocks
    pos = (/ ix2-1, iy2-1, 0 /)                                                ! block coordinates
    coord2d = (/ ix2-1 + mpi_x*geom_l(1), iy2-1 + mpi_y*geom_l(2), 0     /)    ! 2D coordinates
    code = mpi_z
    call send_block (3, com_z, pos, code, coord2d, dim_l, a, dim(1:3), &
                     c(1,1,1,ix2,iy2))                                         ! send buffer
  end do
  end do
!------------------------------------------------------------------------------
!  Recv loop
!------------------------------------------------------------------------------
  do iz2=1,geom_g(3)
    pos = (/ 0, 0, iz2-1 /)
    code = iz2-1
    call recv_block (3, com_z, pos, code, dim_z, b, dim(1:3))                        ! get the block
  end do
!------------------------------------------------------------------------------
!  Synchronize, deallocate
!------------------------------------------------------------------------------
  call barrier_mpi
  deallocate (c)
END SUBROUTINE

!*******************************************************************************
SUBROUTINE send_block (i, com, pos, code, coord, dim1, a, dim2, c)
  implicit none
  integer i, com, code
  integer, dimension(3):: pos, dim1, dim2, coord
  real, dimension(dim1(1),dim1(2),dim1(3)):: a
  real, dimension(dim2(1),dim2(2),dim2(3)):: c
!------------------------------------------------------------------------------
!  Find partner, prepare block, send
!------------------------------------------------------------------------------
  kx1 = 1 + pos(1)*dim2(1); kx2 = kx1 + dim2(1) - 1
  ky1 = 1 + pos(2)*dim2(2); ky2 = ky1 + dim2(2) - 1
  kz1 = 1 + pos(3)*dim2(3); kz2 = kz1 + dim2(3) - 1
  if (verbose > 1) print '(i4,2(a,3i4))',mpi_rank,' pos',pos,' dim2',dim2
  c = a(kx1:kx2,ky1:ky2,kz1:kz2)
  call MPI_CART_RANK (com, coord, rank, err)                                   ! rank to send to
  if (verbose > 1) print '(3(i4,a),3i4,a,2i6)',mpi_rank,' sending',code,' to', &
                                          rank,' at',coord,' for',code,i
  call MPI_ISEND (c, product(dim2), MPI_REAL, rank, code, com, req, err)       ! send
END SUBROUTINE

!*******************************************************************************
SUBROUTINE recv_block (i, com2d, pos, code, dim2d, b, dim)
  implicit none
  integer i, com2d, dst, src, code
  integer, dimension(3):: dim2d, dim, pos
  real, dimension(dim2d(1),dim2d(2),dim2d(3)):: b
  real, dimension(:,:,:), allocatable:: d
  integer status(MPI_STATUS_SIZE)
!------------------------------------------------------------------------------
!  Receive block, plug it in
!------------------------------------------------------------------------------
  allocate (d(dim(1),dim(2),dim(3)))                                           ! recv buffer
  src = MPI_ANY_SOURCE
  if (verbose > 1) print '(2(i3,a),3i4,i6)',mpi_rank,' receiving',code, &
                                            ' for',pos,i
  call MPI_IRECV (d, product(dim), MPI_REAL, src, code, com2d, req, err)       ! request the data
  call MPI_WAIT (req, status, err)                                             ! get it!
  if (verbose > 1) print '(3(i3,a),3i4,i6)',mpi_rank,' received',i,' from', &
                                             status(1),' for',pos,i
  kx1 = 1 + pos(1)*dim(1); kx2 = kx1 + dim(1) - 1                              ! plug it in
  ky1 = 1 + pos(2)*dim(2); ky2 = ky1 + dim(2) - 1                              ! plug it in
  kz1 = 1 + pos(3)*dim(3); kz2 = kz1 + dim(3) - 1                              ! plug it in
  b(kx1:kx2,ky1:ky2,kz1:kz2) = d
  deallocate (d)
END SUBROUTINE

!******************************************************************************
SUBROUTINE deshape_x (a, b)
  implicit none
  real, dimension(dim_x(1),dim_x(2),dim_x(3)):: a
  real, dimension(dim_l(1),dim_l(2),dim_l(3)):: b
  real, dimension(:,:,:,:), allocatable:: c
  integer dim(4), pos(3), coord2d(3), coord3d(3), code, save(3)
!------------------------------------------------------------------------------
!  Send loop, non-blocking
!------------------------------------------------------------------------------
  if (first_time) call init_reshape
  if (all(dim_x == dim_l)) then
    if (verbose > 1 .or. (master .and. (verbose > 0))) print*,mpi_rank,'x-deshape-shortcut'
    b = a
    return
  end if
  geom_l = geom_x/geom_g
  dim = (/ dim_l(1), dim_x(2), dim_x(3), geom_g(1) /)
  if (master) then
    if (verbose > 1) print*,'x:geom_l',geom_l
    if (verbose > 0) print*,'x:buffer dim',dim
    if (verbose > 1) print*,'x:buffer size',product(dim),product(dim_l)
  end if
  allocate (c(dim(1),dim(2),dim(3),dim(4)))                                    ! send buffer
  CALL MPI_CART_COORDS (com_x, mpi_rank, 3, coord2d, err)                      ! get 2D-coords
  save = coord2d
  coord3d(2:3) = coord2d(2:3)/geom_l(2:3)
  coord2d = coord2d-coord3d*geom_l
  do ix2=1,geom_g(1)
    coord3d(1) = ix2-1
    !print'(a,i3,3(3i4,1x))','rank,coor2d',mpi_rank,save,geom_l,coord2d
    code = coord2d(2) + coord2d(3)*dim(2)                                      ! where to put it
    pos = (/ ix2-1, 0, 0 /)
    call send_block (1, com3d, pos, code, coord3d, dim_x, a, dim(1:3), &
                     c(1,1,1,ix2))
  end do
!------------------------------------------------------------------------------
!  Recv loop
!------------------------------------------------------------------------------
  do iz2=1,geom_l(3)
  do iy2=1,geom_l(2)
    pos = (/ 0, iy2-1, iz2-1 /)
    code = (iy2-1) + (iz2-1)*dim(2)
    call recv_block (1, com3d, pos, code, dim_l, b, dim(1:3))                        ! get the block
  end do
  end do
!------------------------------------------------------------------------------
!  Synchronize, deallocate
!------------------------------------------------------------------------------
  call barrier_mpi
  deallocate (c)
END SUBROUTINE

!******************************************************************************
SUBROUTINE deshape_y (a, b)
  implicit none
  real, dimension(dim_y(1),dim_y(2),dim_y(3)):: a
  real, dimension(dim_l(1),dim_l(2),dim_l(3)):: b
  real, dimension(:,:,:,:), allocatable:: c
  integer dim(4), pos(3), coord2d(3), coord3d(3), code, save(3)
!------------------------------------------------------------------------------
!  Send loop, non-blocking
!------------------------------------------------------------------------------
  if (first_time) call init_reshape
  if (all(dim_y == dim_l)) then
    if (verbose > 1 .or. (master .and. (verbose > 0))) print*,mpi_rank,'y-deshape-shortcut'
    b = a
    return
  end if
  geom_l = geom_y/geom_g
  dim = (/ dim_y(1), dim_l(2), dim_y(3), geom_g(2) /)
  if (master) then
    if (verbose > 1) print*,'y:geom_l',geom_l
    if (verbose > 0) print*,'y:buffer dim',dim
    if (verbose > 1) print*,'y:buffer size',product(dim),product(dim_l)
  end if
  allocate (c(dim(1),dim(2),dim(3),dim(4)))                                    ! send buffer
  CALL MPI_CART_COORDS (com_y, mpi_rank, 3, coord2d, err)                      ! get 2D-coords
  save = coord2d
  coord3d = coord2d/max(geom_l,1)
  coord2d = coord2d-coord3d*geom_l
  do iy2=1,geom_g(2)
    coord3d(2) = iy2-1
    !print'(a,i3,3(3i4,1x))','rank,coor2d',mpi_rank,save,geom_l,coord2d
    code = coord2d(1) + coord2d(3)*dim(1)                                      ! where to put it
    pos = (/ 0, iy2-1, 0 /)
    call send_block (2, com3d, pos, code, coord3d, dim_y, a, dim(1:3), &
                     c(1,1,1,iy2))
  end do
!------------------------------------------------------------------------------
!  Recv loop
!------------------------------------------------------------------------------
  do iz2=1,geom_l(3)
  do ix2=1,geom_l(1)
    pos = (/ ix2-1, 0, iz2-1 /)
    code = (ix2-1) + (iz2-1)*dim(1)
    call recv_block (2, com3d, pos, code, dim_l, b, dim(1:3))                        ! get the block
  end do
  end do
!------------------------------------------------------------------------------
!  Synchronize, deallocate
!------------------------------------------------------------------------------
  call barrier_mpi
  deallocate (c)
END SUBROUTINE

!******************************************************************************
SUBROUTINE deshape_z (a, b)
  implicit none
  real, dimension(dim_z(1),dim_z(2),dim_z(3)):: a
  real, dimension(dim_l(1),dim_l(2),dim_l(3)):: b
  real, dimension(:,:,:,:), allocatable:: c
  integer dim(4), pos(3), coord2d(3), coord3d(3), code, save(3)
!------------------------------------------------------------------------------
!  Send loop, non-blocking
!------------------------------------------------------------------------------
  if (first_time) call init_reshape
  if (all(dim_z == dim_l)) then
    if (verbose > 1 .or. (master .and. (verbose > 0))) print*,mpi_rank,'z-deshape-shortcut'
    b = a
    return
  end if
  geom_l = geom_z/geom_g
  dim = (/ dim_z(1), dim_z(2), dim_l(3), geom_g(3) /)
  if (master) then
    if (verbose > 1) print*,'z:geom_l',geom_l
    if (verbose > 0) print*,'z:buffer dim',dim
    if (verbose > 1) print*,'z:buffer size',product(dim),product(dim_l)
  end if
  allocate (c(dim(1),dim(2),dim(3),dim(4)))                                    ! send buffer
  CALL MPI_CART_COORDS (com_z, mpi_rank, 3, coord2d, err)                      ! get 2D-coords
  save = coord2d
  coord3d(1:2) = coord2d(1:2)/geom_l(1:2)
  coord2d = coord2d-coord3d*geom_l
  do iz2=1,geom_g(3)
    coord3d(3) = iz2-1
    !print'(a,i3,3(3i4,1x))','rank,coor2d',mpi_rank,save,geom_l,coord2d
    code = coord2d(1) + coord2d(2)*dim(1)                                      ! where to put it
    pos = (/ 0, 0, iz2-1 /)
    call send_block (3, com3d, pos, code, coord3d, dim_z, a, dim(1:3), &
                     c(1,1,1,iz2))
  end do
!------------------------------------------------------------------------------
!  Recv loop
!------------------------------------------------------------------------------
  do iy2=1,geom_l(2)
  do ix2=1,geom_l(1)
    pos = (/ ix2-1, iy2-1, 0 /)
    code = (ix2-1) + (iy2-1)*dim(1)
    call recv_block (3, com3d, pos, code, dim_l, b, dim(1:3))                        ! get the block
  end do
  end do
!------------------------------------------------------------------------------
!  Synchronize, deallocate
!------------------------------------------------------------------------------
  call barrier_mpi
  deallocate (c)
END SUBROUTINE
END MODULE

!**********************************************************************
SUBROUTINE fftpack_test
!
!  This test demonstrates that the coeffs returned are:
!
!   [a_0, +a_1/2., +b_1/2., ..., b_{ny-1}/2.,  a_ny]
!
!  Thus, the derivive is formed by setting the tranform to
!
!   [ 0., -b_1/2, +a_1/2., ...., a_{ny-1}/2.,  0.  ]
!
  real, allocatable, dimension(:):: f,ft,w
  integer m,lw,i,k
  real a,pi
!----------------------------------------------------------------------
  m=8
  pi=4.*atan(1.)
  lw=2*m+15
  allocate (f(m),ft(m),w(lw))

  a = 2.*pi/m
  f = 0.
  do k=0,m/2
    do i=1,m
      f(i) = f(i) + 2.*(k+1)*cos((i-1)*k*a) + 2.*(k+m)*sin((i-1)*k*a)
    end do
  end do
  call srffti (m,w)
  print '(a,8f8.4)','fft_test  f:', f
  ft = f
  call srfftf (m,ft,w)
  print '(a,8f8.4)','fft_test ft:', ft/m

  m = 7
  a = 2.*pi/m
  f = 0.
  do k=0,m/2
    do i=1,m
      f(i) = f(i) + 2.*(k+1)*cos((i-1)*k*a) + 2.*(k+m)*sin((i-1)*k*a)
    end do
  end do
  call srffti (m,w)
  print '(a,8f8.4)','fft_test  f:', f
  ft = f
  call srfftf (m,ft,w)
  print '(a,8f8.4)','fft_test ft:', ft

  deallocate (f,ft,w)
END

!**********************************************************************
SUBROUTINE fft_k2 (k2,dx1,dy1,dz1,mx1,my1,mz1)
  USE grid_m
  implicit none
  real, dimension(mx,my,mz):: k2
  real dx1, dy1, dz1, kx, ky, kz
  integer i, j, jj, k, kk, mx1, my1, mz1
!----------------------------------------------------------------------
  do k=1,mz
    kk=k+mpi_z*mz
    kz=2.*pi/sz*(kk/2)
    do j=1,my
      jj=j+mpi_y*my
      ky=2.*pi/sy*(jj/2)
      do i=1,mx
        kx=2.*pi/sx*(i/2)
        k2(i,j,k)=kx**2+ky**2+kz**2
      end do
    end do
  end do
END SUBROUTINE

!**********************************************************************
SUBROUTINE fft_f (r,rr,mx1,my1,mz1)
  USE grid_m, only: mx, my, mz
  implicit none
  integer i,j,k,mx1,my1,mz1
  integer, parameter:: lw=1024, lwx=2*lw+15
  real, dimension(mx,my,mz):: r, rr
  real, dimension(:,:,:), allocatable:: scratch
  real c, wx(lwx), fx(mx), fy(my), fz(mz,mx)
!----------------------------------------------------------------------
  call fft_test
  if (mx.gt.lw .or. my.gt.lw .or. mz.gt.lw) then
    print *,'fft_f: dim too small',mx,my,mz,lw
    stop
  end if
  call srffti (mx,wx)

  allocate (scratch(mx,my,mz))
  do k=1,mz
   do j=1,my
    fx(:)=r(:,j,k)
    call srfftf (mx,fx,wx)
    c=1./mx
    scratch(:,j,k)=c*fx(:)
   end do
  end do

  call permute (scratch, rr)
  do k=1,mz
   do j=1,my
    fx(:)=rr(:,j,k)
    call srfftf (mx,fx,wx)
    c=1./mx
    scratch(:,j,k)=c*fx(:)
   end do
  end do

  call permute (scratch, rr)
  do k=1,mz
   do j=1,my
    fx(:)=rr(:,j,k)
    call srfftf (mx,fx,wx)
    c=1./mx
    scratch(:,j,k)=c*fx(:)
   end do
  end do

  call permute (scratch, rr)
  deallocate (scratch)
END SUBROUTINE

!**********************************************************************
SUBROUTINE fft_r (rr,r,mx1,my1,mz1)
  USE grid_m, only: mx, my, mz
  implicit none
  integer i,j,k,mx1,my1,mz1
  integer, parameter:: lw=1024, lwx=2*lw+15
  real, dimension(mx,my,mz):: r, rr
  real, dimension(:,:,:), allocatable:: scratch
  real wx(lwx), fx(mx), fy(my), fz(mz,mx)
!----------------------------------------------------------------------

  if (mx.gt.lw .or. my.gt.lw .or. mz.gt.lw) then
    print *,'fft_r: dim too small',mx,my,mz,lw
    stop
  end if
  call srffti (mx,wx)

  allocate (scratch(mx,my,mz))
  call permute (rr, scratch)
  do k=1,mz
   do j=1,my
    fx=scratch(:,j,k)
    call srfftb (mx,fx,wx)
    r(:,j,k)=fx
   end do
  end do

  call permute (r, scratch)
  do k=1,mz
   do j=1,my
    fx=scratch(:,j,k)
    call srfftb (mx,fx,wx)
    r(:,j,k)=fx
   end do
  end do

  call permute (r, scratch)
  do k=1,mz
   do j=1,my
    fx=scratch(:,j,k)
    call srfftb (mx,fx,wx)
    r(:,j,k)=fx
   end do
  end do

  deallocate (scratch)
END SUBROUTINE

!**********************************************************************
SUBROUTINE fft_test
  USE grid_m
  implicit none
  integer ix, iy, iz, seed2
  real random
  logical, save:: first_time=.true.
  real, allocatable, dimension(:,:,:):: fft1, fft2, fft3
!----------------------------------------------------------------------
  if (.not. first_time) return
  first_time = .false.

  if (mpi_mx > 1) then
    if (master) print *,'WARNING, fft3d_mpi2.f90: cannot handle MPI in x-direction; skipping tests'
    return
  end if

  if (master) call fftpack_test
  call test_permute

  allocate (fft1(mx,my,mz), fft2(mx,my,mz), fft3(mx,my,mz))
  seed2 = 0
  do iz=1,mz
  do iy=1,my
  do ix=1,mx
    !fft1(ix,iy,iz) = cos(2.*pi*(x(ix)-xmin)/sx) + 2.*cos(2.*pi*(y(iy)-ymin)/sy) + 3.*cos(2.*pi*(z(iz)-zmin)/sz)
    fft1(ix,iy,iz) = random(seed2)
  end do
  end do
  end do

  call fft_f (fft1, fft2, mx, my, mz)
  if (master) print '(a,8f10.3)', 'fft_test:', fft2(1:2,1:2,1:2)

  call fft_r (fft2, fft3, mx, my, mz)
  if (maxval(abs(fft1-fft3)) > 1e-6) print*,mpi_rank,': fft_test WARNING:',maxval(abs(fft1-fft3))

  deallocate (fft1, fft2, fft3)
END
