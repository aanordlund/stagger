! $Id: start.f90,v 1.12 2009/10/07 12:23:06 aake Exp $
!***********************************************************************
MODULE start_m
  USE grid_m
  USE stagger_m
  USE time_m
  USE io_m
  implicit none
  real a0, a1, b0
  integer k
  character(len=40):: case
CONTAINS

!-----------------------------------------------------------------------
SUBROUTINE stationary                                                   ! simple 1-D shock
  implicit none
  mx=100; my=100; mz=100                                                ! dimensions
  call init_grid                                                        ! initialize grid
  r=a0; e=a1/(gamma*(gamma-1.))                                         ! sound speed = sqrt(a1/a0)
  px=0.; py=0.; pz=0.                                                   ! zero velocity
  bx=0.; by=0.; bz=b0                                                   ! constant Bz
  nsnap=50; tsnap=.004                                                  ! snap shots
END SUBROUTINE
!-----------------------------------------------------------------------
SUBROUTINE Shock_1D                                                     ! simple 1-D shock
  implicit none
  mx=500; my=1; mz=1                                                    ! dimensions
  call init_grid                                                        ! initialize grid
  r=1.                                                                  ! constant density
  px=0.; py=0.; pz=0.                                                   ! zero velocity
  e(1:11*mx/20,:,:)=1.                                                  ! left state
  e(11*mx/20+1:mx,:,:)=a1                                               ! right state
  bx=0.; by=0.; bz=0.                                                   ! zero magnetic fiels
  e = exp(smooth3(smooth3(alog(e))))                                    ! smooth log pressure
  nsnap=50; tsnap=.004                                                  ! snap shots
END SUBROUTINE
!-----------------------------------------------------------------------
SUBROUTINE Alfven                                                       ! propagating Alfven wave
  implicit none
  mx=100; my=1; mz=1;                                                   ! dimensions
  call init_grid                                                        ! initialize grid
  r=1.; px=0.; pz=0.; e=1.                                              ! constant density
  print*,allocated(py),size(py),sx
  print*,allocated(x),size(x),sx
  py=a1*sin(k*2.*pi*x/sx)                                               ! sinusoidal Uy
  bx=b0; by=-py; bz=0.                                                  ! By 180 degrees out-of-phase
  nsnap=100; tsnap=.01                                                  ! snap shots, duration=1
END SUBROUTINE
!-----------------------------------------------------------------------
SUBROUTINE Alfven_stand                                                 ! standing Alfven wave
  implicit none
  mx=100; my=1; mz=1                                                    ! grid dimensions
  call init_grid                                                        ! initialize grid
  r=1.; px=0.; pz=0.; e=1.                                              ! constant density
  py=a1*sin(k*2.*pi*x/sx)                                               ! sinusoidal Uy
  bx=b0; by=0.; bz=0.                                                   ! constant Bx
  nsnap=100; tsnap=.01                                                  ! snap shots, duration=1
END SUBROUTINE
!-----------------------------------------------------------------------
SUBROUTINE sound_wave                                                   ! 2-D sound wave
  implicit none
  mx=32; my=32; mz=1                                                    ! 2-D grid dimensions
  call init_grid                                                        ! initialize grid
  r=1.; py=0.; pz=0.                                                    ! constant initial density
  px=a1*xdn(sin(k*2.*pi*x/sx))*cos(2.*pi*y/sy)                          ! x-velocity
  e=r/(gamma*(gamma-1.))                                                ! pressure
  bx=0.; by=0.; bz=0.                                                   ! zero magnetic field
  nsnap=25; tsnap=.02                                                   ! snap shots, duration=1
END SUBROUTINE
!-----------------------------------------------------------------------
SUBROUTINE fast_wave                                                    ! fast mode wave
  implicit none
  mx=100; my=1; mz=1                                                    ! grid dimensions
  call init_grid                                                        ! initialize grid
  r=1.; py=0.; pz=0.; e=1.                                              ! constant density
  px=a1*xdn(sin(k*2.*pi*x/sx))*cos(2.*pi*y/sy)                          ! sinusoidal Ux
  bx=0.; by=0.; bz=b0                                                   ! constant initial Bz
  nsnap=100; tsnap=.01                                                  ! snap shots, duration=1
END SUBROUTINE
!***********************************************************************
SUBROUTINE reconnection_2d                                              ! magnetic 2-D reconnection
  implicit none
  mx=100; my=100; mz=1                                                  ! grid dimensions
  call init_grid                                                        ! initialize grid
  r=1.; py=0.; pz=0.                                                    ! constant density
!  px=0.0001*sin(y*pi*2/sy)*xdn(sin(x*2*pi/sx))                          ! y-dependent x-convergence
  px=0.
  bx=0.01*sin(y*pi*2/sy)                                                ! weak x-component
  by=sign(abs(sin(x*2*pi/sx))**.3,sin(x*2*pi/sx)) ! strong y-component
  bz=0.                                                                 ! no z-component
  e=3./2.*((.5+.1)-.5*(bx**2+by**2))                                    ! constant total pressure
  nsnap=50; tsnap=.1                                                    ! snap shots, duration=1
END SUBROUTINE
!***********************************************************************
SUBROUTINE Brio_Wu                                                      ! Brio & Wu, JCP 75, 400 (1988)
  implicit none
  integer i0, i1
!.......................................................................
  mx=1600; my=1; mz=1                                                   ! grid dimensions
  call init_grid                                                        ! initialize grid
  i0=(19*mx)/100+1                                                      ! left to right
  i1=i0+(60*mx)/100                                                     ! right to left
  gamma=2.                                                              ! gas gamma
  r=1.                                                                  ! density, left
  e=1.                                                                  ! pressure, left
  bx=0.75                                                               ! B_x
  by=1.                                                                 ! B_y, left
  r(i0:i1,:,:)=0.125                                                    ! density, right
  e  (i0:i1,:,:)=0.1                                                    ! pressure, right
  by (i0:i1,:,:)=-1.                                                    ! B_y, right
! r=exp(smooth3(alog(r)))                                               ! optional smoothing
! e  =exp(smooth3(alog(e  )))
  nsnap=18; tsnap=.004                                                  ! snap shots
END SUBROUTINE
!***********************************************************************
SUBROUTINE Kim_etal_1a                                                  ! Kim et al. Apj 514, 506 (1999)
  implicit none
  integer i0, i1
!.......................................................................
  mx=1024; my=1; mz=1                                                   ! grid dimensions
  call init_grid                                                        ! initialize grid
  i0=(19*mx)/100+1                                                      ! left to right
  i1=i0+(60*mx)/100                                                     ! right to left
  gamma=1.000001                                                        ! gas gamma
  r=1.                                                                  ! density, left
  e=1.                                                                  ! pressure, left
  bx=3./sqrt(4.*pi)                                                     ! B_x
  by=5./sqrt(4.*pi)                                                     ! B_y, left
  r(i0:i1,:,:)=0.1                                                      ! density, right
  e  (i0:i1,:,:)=0.1                                                    ! pressure, right
  by (i0:i1,:,:)=2./sqrt(4.*pi)                                         ! B_y, right
  r=exp(smooth3(alog(r)))                                               ! optional smoothing
  e  =exp(smooth3(alog(e  )))
  e=r**gamma/(gamma*(gamma-1.))                                         ! isothermal
  nsnap=18; tsnap=.004                                                  ! snap shots
END SUBROUTINE
!***********************************************************************
SUBROUTINE Orszag_Tang                                                  ! Orszag Tang vortex
  implicit none                                                         ! cf. Athena web
  real pg
!.......................................................................
  mx=128; my=128; mz=1                                                  ! grid dimensions
  call init_grid                                                        ! intialize grid
  B0=1./sqrt(4.*pi)                                                     ! normalized as Athena
  r=25./(36.*pi)                                                        ! density
  px=-sin(2.*pi*y/sy)*r                                                 ! Ux ampl=1.
  py= sin(2.*pi*x/sx)*r                                                 ! Uy ampl=1.
  bx=-b0*sin(2.*pi*y/sy)                                                ! Bx
  by= b0*sin(4.*pi*x/sx)                                                ! By
  pz=0.; bz=0.                                                          ! no z-components
  pg=5./(12.*pi); e=pg/(gamma-1.)                                       ! Pgas -> internal energy
  print*,'average beta =',pg/(0.5*b0**2)                                ! should be 10./3.
  nsnap=50; tsnap=.01                                                   ! snap shots, duration=0.5
END SUBROUTINE
!-----------------------------------------------------------------------
SUBROUTINE wave_leaking                                                 ! propagating Alfven wave
  implicit none
  integer i0, i1, i
  real beta, pg
!.......................................................................
  beta = 0.1                                                            ! plasma beta in channel
  mx=100; my=100; mz=1;                                                 ! dimensions
  call init_grid                                                        ! initialize grid
  i0=(24*mx)/100+1                                                      ! left to right
  i1=i0+(50*mx)/100                                                     ! right to left
  r=.1; py=0.; pz=0.; e=.1                                              ! left state
  r(i0:i1,:,:) = 1.                                                     ! right state density
  e(i0:i1,:,:)   = 1.                                                   ! right state internal energy
  do i=1,mx/100
    r=exp(smooth3(smooth3(alog(r))))                                    ! optional smoothing
    e  =exp(smooth3(smooth3(alog(e  ))))                                ! in the log
  enddo
  pg = (gamma-1)*e(mx/2,1,1)*(1.+1./beta)
  px=a1*sin(k*2.*pi*y/sy)*exp(-((x-x(mx/2,1,1))/& 
     (x(i1,1,1)-x(i0,1,1)))**2/.01)                                     ! sinusoidal Uy
  bx=-px; by=sqrt((pg-(gamma-1.)*e)*2.); bz=0.                          ! By 180 degrees out-of-phase
  nsnap=100; tsnap=.01                                                  ! snap shots, duration=1
END SUBROUTINE
!-----------------------------------------------------------------------
END MODULE

!***********************************************************************
SUBROUTINE input (inputfile)
  implicit none
  character(len=80) :: inputfile
!.......................................................................
  inputfile = 'EXPERIMENT/tests/input.txt'
END SUBROUTINE

!***********************************************************************
SUBROUTINE init_start                                                   ! choose among a number of 
  USE start_m                                                           ! built-in initial conditions
  USE grid_m
  USE time_m, only: iread
  implicit none
  integer i
  namelist /init/ iread, a0, a1, k, b0, case
!.......................................................................
  iread = 1                                                             ! record to read from
  k = 2                                                                 ! default wave number
  a0 = 1.                                                               ! background value
  a1 = 0.01                                                             ! perturbation amplitude
  b0 = 2.*pi                                                            ! time period = 1
  gamma=5./3.                                                           ! default gamma
  dt = 1.                                                               ! just needs to be non-zero
  sx = 1.                                                               ! unit cube for tests
  sy = 1.                                                               ! unit cube for tests
  sz = 1.                                                               ! unit cube for tests
  case = '1D_shock'                                                     ! default test
  rewind (stdin)                                                        ! arbitrary order of namelists 
  rewind(stdin); read  (stdin,init)                                                    ! read namelist parameters
  if (stdout>0) then; write(stdout,*) hl; write(stdout,init); endif     ! echo to standard output

  select case(trim(case))
  case  ('1D_shock')         ; call shock_1D
  case  ('Alfven')           ; call Alfven
  case  ('Alfven_stand')     ; call Alfven_stand
  case  ('fast_wave')        ; call fast_wave
  case  ('sound_wave')       ; call sound_wave
  case  ('2D_reconnection')  ; call reconnection_2d
  case  ('Brio_Wu')          ; call Brio_Wu
  case  ('Kim_etal_1a')      ; call Kim_etal_1a
  case  ('Orszag_Tang')      ; call Orszag_Tang
  case  ('wave_leaking')     ; call wave_leaking
  case  ('braiding')         ; call stationary
  case  ('ABC_flow')         ; call stationary
  case  default              ; call read_snapshot
  end select
  deallocate (x,y,z)

  if (master) write (2,init)                                            ! save possibly modified params

  if (mpi_rank>0) return
  print *,'r, px, py, pz'                                               ! sanity tests -- check printout!
  print *,'  maxvalues',maxval(r),maxval(px),maxval(py),maxval(pz)
  print *,'  minvalues',minval(r),minval(px),minval(py),minval(pz)
  print *,'e, bx, by, bz'
  print *,'  maxvalues',maxval(e),maxval(bx),maxval(by),maxval(bz)
  print *,'  minvalues',minval(e),minval(bx),minval(by),minval(bz)
END SUBROUTINE

