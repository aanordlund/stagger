! $Id: force.f90,v 1.6 2009/10/06 23:10:11 aake Exp $
!***********************************************************************
MODULE force_m
  USE grid_m
  real a, b, c, d
  integer i, seed
  character(len=40) type
CONTAINS
!-----------------------------------------------------------------------
SUBROUTINE abc_flow
  implicit none
  integer ix, iy, iz
  do iz=1,mz; do iy=1,my; do ix=1,mx
    dpxdt(ix,iy,iz) = dpxdt(ix,iy,iz) + (- A*cos(2.*pi*ym(iy)) + C*sin(2.*pi*zm(iz)) - ux(ix,iy,iz))*d/dx
    dpydt(ix,iy,iz) = dpydt(ix,iy,iz) + (- C*cos(2.*pi*zm(iz)) + B*sin(2.*pi*xm(ix)) - uy(ix,iy,iz))*d/dy
    dpzdt(ix,iy,iz) = dpzdt(ix,iy,iz) + (- B*cos(2.*pi*xm(ix)) + A*sin(2.*pi*ym(iy)) - uz(ix,iy,iz))*d/dz
  end do; end do; end do
END SUBROUTINE
!-----------------------------------------------------------------------
SUBROUTINE abc_force
  implicit none
  integer ix, iy, iz
  do iz=1,mz; do iy=1,my; do ix=1,mx
    dpxdt(ix,iy,iz) = dpxdt(ix,iy,iz) - A*cos(2.*pi*ym(iy)) + C*sin(2.*pi*zm(iz))
    dpydt(ix,iy,iz) = dpydt(ix,iy,iz) - C*cos(2.*pi*zm(iz)) + B*sin(2.*pi*xm(ix))
    dpzdt(ix,iy,iz) = dpzdt(ix,iy,iz) - B*cos(2.*pi*xm(ix)) + A*sin(2.*pi*ym(iy))
  end do; end do; end do
END SUBROUTINE
!-----------------------------------------------------------------------
SUBROUTINE braiding
  implicit none
  integer i1, i2, j, m, ix, iy
  real w, wz
  m = 2.5*c
  do j=-m,m
    i1 = i+j
    i2 = mz-i+1+j
    w=exp(-(j/c)**2)
    wz=exp(-((j-.5)/c)**2)
    do iy=1,my
    do ix=1,mx
      dpxdt(ix,iy,i1) = dpxdt(ix,iy,i1) + (a*(sin(2.*pi*ym(iy))+sin(4.*pi*ym(iy))) &
                                           - ux(ix,iy,i1))*b*w
      dpydt(ix,iy,i1) = dpydt(ix,iy,i1) - uy(ix,iy,i1)*b*w
      dpzdt(ix,iy,i1) = dpzdt(ix,iy,i1) - uz(ix,iy,i1)*b*wz
      dpydt(ix,iy,i2) = dpydt(ix,iy,i2) + (a*(sin(2.*pi*xm(ix))+cos(4.*pi*xm(ix))) &
                                           - uy(ix,iy,i2))*b*w
      dpxdt(ix,iy,i2) = dpxdt(ix,iy,i2) - ux(ix,iy,i2)*b*w
      dpzdt(ix,iy,i2) = dpzdt(ix,iy,i2) - uz(ix,iy,i2)*b*wz
    end do
    end do
  end do
END SUBROUTINE
!-----------------------------------------------------------------------
END MODULE

!***********************************************************************
SUBROUTINE init_force
  USE force_m
  USE grid_m, only: stdin
  implicit none
  namelist /force/ a, b, c, d, i, type
!.......................................................................
  type = 'xxx'
  a = 1. 
  b = 1.
  c = 1.
  d = 1.
  i = mz/2+1
  rewind(stdin); read(stdin,force); 
  if (stdout>0) then; write(stdout,force); write(stdout,*) hl; end if
  if (master) write(2,force)
END SUBROUTINE
!-----------------------------------------------------------------------
SUBROUTINE forcing (xdnr, ydrn, zdnr)
  USE force_m
  USE grid_m
  implicit none
  real, dimension(mx,my,mz):: xdnr, ydrn, zdnr
!.......................................................................
  select case (trim(type))
  case ('ABC_force')        ; call abc_force
  case ('ABC_flow')         ; call abc_flow 
  case ('braiding')         ; call braiding
  case default              ; return
  end select
END SUBROUTINE
!***********************************************************************
