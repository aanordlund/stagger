! $Id: boundaries.f90,v 1.2 2009/09/24 20:41:47 aake Exp $

!-------------------------------------------------------------------------------
SUBROUTINE density_boundaries (r)
  USE grid_m, only: mx, my, mz, lb, ub, r_lower, mpi_y, mpi_my
  implicit none
  real, dimension(mx,my,mz):: r
END

!-------------------------------------------------------------------------------
SUBROUTINE velocity_boundaries (px, py, pz, xdnr, ydnr, zdnr, Ux, Uy, Uz)
  USE grid_m, only: mx, my, mz, lb, ub, mpi_y, mpi_my
  implicit none
  real, dimension(mx,my,mz):: px, py, pz, xdnr, ydnr, zdnr, Ux, Uy, Uz
END

!-------------------------------------------------------------------------------
SUBROUTINE magnetic_field_boundaries (Bx, By, Bz, Ux, Uy, Uz)
  USE grid_m, only: mx, my, mz, lb, ub, mpi_y, mpi_my
  implicit none
  real, dimension(mx,my,mz):: Bx, By, Bz, Ux, Uy, Uz
END
!-------------------------------------------------------------------------------
SUBROUTINE electric_field_boundaries (Ex, Ey, Ez)
  USE grid_m, only: mx, my, mz, lb, ub, mpi_y, mpi_my
  implicit none
  real, dimension(mx,my,mz):: Ex, Ey, Ez
END

!-------------------------------------------------------------------------------
SUBROUTINE energy_boundaries (e, r, Ux, Uy, Uz)
  USE grid_m, only: mx, my, mz, lb, ub, e_lower, e_upper, mpi_y, mpi_my
  implicit none
  real, dimension(mx,my,mz):: e, r, Ux, Uy, Uz
END

!-------------------------------------------------------------------------------
SUBROUTINE ddt_boundaries (drdt, dpxdt, dpydt, dpzdt, dedt, dBxdt, dBydt, dBzdt)
  USE grid_m, only: mx, my, mz, lb, ub, mpi_y, mpi_my
  implicit none
  real, dimension(mx,my,mz):: drdt, dpxdt, dpydt, dpzdt, dedt, dBxdt, dBydt, dBzdt
END
