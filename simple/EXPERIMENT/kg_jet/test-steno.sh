#!/bin/sh
# 
# @ job_name         = kgjet-test
# @ notification     = never
# @ output           = test/output_$(jobid).log
# @ error            = test/output_$(jobid).log
# @ class            = astro
# @ job_type         = parallel
# @ node_usage       = not_shared
# @ wall_clock_limit = 2:00:00
# @ node             = 1
# @ tasks_per_node   = 4
# @ resources        = ConsumableCpus(1) ConsumableMemory(2gb) InfiniBand2(1)
# @ queue

# make sure the test directory exists
mkdir -p test

cat <<END >test/input.txt
 &redirect /
 &strat    /
 &B_field  / 
 &B_tube   /
 &force    /
 &stagger  /
 &mhd      /
 &time     /
 &grid     mx=80 my=80 mz=160 /
 &out      tsnap=0.01 nsnap=4 /
END

# write host list to hosts.txt
/opt/ibmll/LoadL/full/bin/ll_get_machine_list $LOADL_STEP_ID > hosts.txt

# run with MPI; remember to set the -np argument to (node)*(tasks_per_node)
mpirun -np 4 -hostfile hosts.txt ./kg_jet_MPI.x test/input.txt
