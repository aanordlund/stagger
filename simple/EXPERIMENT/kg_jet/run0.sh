#!/bin/sh
# 
# @ job_name         = kgjet
# @ notification     = never
# @ output           = run0/output_$(jobid).log
# @ error            = run0/output_$(jobid).log
# @ class            = astro
# @ job_type         = parallel
# @ node_usage       = not_shared
# @ wall_clock_limit = 240:00:00
# @ node             = 12
# @ tasks_per_node   = 4
# @ resources        = ConsumableCpus(1) ConsumableMemory(2gb) InfiniBand2(1)
# @ queue

# output directory
dir=run0

# make sure the test directory exists
mkdir -p $dir

# create input file
cat << END > $dir/input.txt
 &redirect /
 &strat    /
 &B_field  / 
 &B_tube   /
 &force    /
 &stagger  /
 &mhd      /
 &time     /
 &grid     /
 &out      iread=39 tsnap=1 nsnap=200 /
END

# write host list to hosts.txt
/opt/ibmll/LoadL/full/bin/ll_get_machine_list $LOADL_STEP_ID > hosts.txt

# run with MPI; remember to set the -np argument to (node)*(tasks_per_node)
cp -p ./kg_jet_MPI.x $dir/
mpirun -np 48 -hostfile hosts.txt $dir/kg_jet_MPI.x $dir/input.txt
