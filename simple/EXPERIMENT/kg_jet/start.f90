! $Id: start.f90,v 1.11 2009/11/09 23:29:48 aake Exp $
!***********************************************************************
SUBROUTINE input (inputfile)
  implicit none
  character(len=80) :: inputfile
!.......................................................................
  inputfile = 'EXPERIMENT/kg_jet/input.txt'
END SUBROUTINE

!***********************************************************************
SUBROUTINE init_start
  USE grid_m
  USE time_m, only: iread
  USE stagger_m
  implicit none
  integer i
  real :: nablst, nnst                                                  ! dlnT/dlnP 
  real :: p1, p2, r1, r2, h1                                            ! temp variables for hydrostadic model
  real :: z0, ztr, zb, z1, z2, T0, T1, h0, r0, p0
  real :: bxconst, byconst, bzconst
  real :: D0, B0, db, lamb
  real, dimension(:,:,:), allocatable :: scr2, scr3, p_gas              ! scratch variables
  namelist /strat/ z0,  ztr, z1, z2, T0, T1, h0, r0, p0
  namelist /B_field/ bxconst, byconst, bzconst
  namelist /B_tube/ D0, B0, db, zb, lamb
!.......................................................................

! stratification model parameters
  z0  =  22.                                                            ! position of photosphere
  ztr =  34.                                                            ! top position of temp minimum
  z1  =  46.                                                            ! position of corona
  z2  = 193.                                                            ! top of model
  T0  =   1.                                                            ! temp at photosphere
  T1  = 200.                                                            ! temp at corona --- isothermal
  h0  =   1.                                                            ! pressure scale height at photosphere 
  r0  =   1.                                                            ! density at photosphere
  p0  =   1.                                                            ! pressure at photosphere
  rewind(stdin); read(stdin,strat)                                      ! read namelist parameters
  if (stdout>0) then; write(stdout,*) hl; write(*,strat); endif         ! echo to standard output
  if (master) write(2,strat)

! background constant magnetic field
  bxconst =  0.008                                                      ! B is in the x-z plane, y is across
  byconst =  0.
  bzconst = -0.00373046
  rewind(stdin); read(stdin,B_field)                                    ! read namelist parameters
  if (stdout>0) then; write(stdout,*) hl; write(*,B_field); endif       ! echo to standard output
  if (master) write(2,B_field)

! flux tube parameters
  D0   =  2.5                                                           ! tube diameter (exponential)
  B0   =  2.9                                                           ! peak B strength
  dB   =   .4                                                           ! Twist parameter
  zb   = 12.                                                            ! height from the base of the box for the tube axis
  lamb = 20.                                                            ! density depletion length scale
  rewind(stdin); read(stdin,B_tube)                                     ! read namelist parameters
  if (stdout>0) then; write(stdout,*) hl; write(*,B_tube); endif        ! echo to standard output
  if (master) write(2,B_tube)

  gamma = 5./3.                                                         ! gas gamma
  nablst = (gamma-1.)/gamma                                             ! dlnT/dlnP 
  nnst = 1./nablst          

  sx = 225                                                              ! size along the tube
  sy = 200                                                              ! size across the tube
  sz = z2                                                               ! total height
  xmin = -sx/2.
  ymin = -sy/2.
  zmin = 0.
  mx = 320                                                              ! approx Klaus' dimension
  my = 320                                                              ! approx Klaus' dimension
  mz = 360                                                              ! approx Klaus' dimension
  lb = 6                                                                ! boundary zones
  call init_grid

! density and pressure in the convection zone:
  allocate (p_gas(mx,my,mz))
  where (z .le. z0)
    r     = (1.-(z-z0)/(h0*nnst))**(nnst-1.) * r0                       ! polytrope
    p_gas = (1.-(z-z0)/(h0*nnst))**nnst * p0                            ! polytrope
  endwhere

! density and pressure in temperature minumum
  where (z .gt. z0 .and. z .le. ztr)
    p_gas = p0*exp(-(z-z0)/h0)                                          ! exponential
    r     = p_gas/p0*r0                                                 ! exponential
  endwhere

! density and pressure in transition region
  p1 = p0*exp(-(ztr-z0)/h0)
  r1 = p1/p0*r0
  where (z .gt. ztr .and. z .le. z1)
    p_gas = exp((z1-ztr)/h0/alog(T1/T0)* &                              ! polytrope
            ((T1/T0)**(-(z-ztr)/(z1-ztr))-1.))*p1                       ! polytrope
    r     = (p_gas/p1)/((T1/T0)**((z-ztr)/(z1-ztr)))*r1
  endwhere

! pressure and density at the top of the transition region
  p2 = exp((z1-ztr)/h0/alog(T1/T0)*(T0/T1-1.))*p1
  r2 = (p2/p1)/(T1/T0)*r1
  h1 = h0*T1/T0

! pressure and density in the corona
  where (z .gt. z1)
    r     = r2*exp(-(z-z1)/h1)                                          ! exponential
    p_gas = p2*exp(-(z-z1)/h1)                                          ! exponential
  endwhere

  allocate (scr2(mx,my,mz), scr3(mx,my,mz))

! twisted magnetic field and additional corrections to the thermal profile.
  p0 = (1.-(zb-z0)/h0*nablst**nnst)*p0
  !p0 = (1.-(zb-z0)/h0*(gamma-1.)/gamma)**(gamma/(gamma-1.))*p0
  scr2 = (z-zb)**2 + y**2                                               ! radius of tube
  scr3 = B0*exp(-scr2/D0**2)                                            ! tube aligned field (z-dir)

  bx = scr3                                                             ! Bz = Bz(x,y)
  scr3 = scr3*db                                                        ! twist parameter
  bz = -y*scr3                                                          ! x is zero in the middle
  by = (z-zb)*scr3                                                      ! Bx = Bx(x,y)

! uniform magnetic field
  bx = bx + bxconst
  by = by + byconst
  bz = bz + bzconst

! density perturbation
  scr3  = B0**2./2.*exp(-2*scr2/D0**2)* &                               ! Magnetic pressure
          (1.+scr2*db**2-db**2*D0**2/2.)
  p_gas = p_gas - scr3                                                  ! change in pressure due to B pressure
  r     = r*(1.-scr3/p_gas*exp(-x**2/lamb**2))                          ! density perturbation along tube

  e = 1./(gamma-1)*p_gas
  deallocate (scr2, p_gas)

! Shift variables to their correct positions

  bx = xdn(bx)
  by = ydn(by)
  bz = zdn(bz)
  
! save boundary values
  allocate (r_upper(mx,my), r_lower(mx,my))
  allocate (e_upper(mx,my), e_lower(mx,my)) 
  r_upper = r(:,:,ub)
  r_lower = r(:,:,lb)
  e_upper = e(:,:,ub)
  e_lower = e(:,:,lb)
   
END subroutine
