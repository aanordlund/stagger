! $Id: force.f90,v 1.3 2009/09/25 03:06:04 aake Exp $
!***********************************************************************
MODULE force_m
  real grav
END MODULE

!***********************************************************************
SUBROUTINE init_force
  USE force_m
  USE grid_m, only: stdin, stdout, hl, master
  implicit none
  namelist /force/ grav
!.......................................................................
  grav = 1.
  rewind(stdin); read(stdin,force); 
  if (stdout>0) then; write(stdout,*) hl; write(stdout,force); end if
  if (master) write(2,force)
END SUBROUTINE
!-----------------------------------------------------------------------
SUBROUTINE forcing (xdnr, ydnr, zdnr)
  USE grid_m
  USE force_m
  implicit none
  real, dimension(mx,my,mz):: xdnr, ydnr, zdnr
!.......................................................................
  dpzdt = dpzdt - grav*zdnr
END SUBROUTINE
!***********************************************************************
