! $Id: boundaries.f90,v 1.4 2009/09/25 02:37:59 aake Exp $

!-------------------------------------------------------------------------------
SUBROUTINE density_boundaries (r)
  USE grid_m, only: mx, my, mz, lb, ub, r_lower, mpi_z, mpi_mz
  implicit none
  real, dimension(mx,my,mz):: r
!...............................................................................
  if (mpi_z==0) r(:,:,lb) = r_lower
  call extrapolate_center_log_lower (r)
  call extrapolate_center_log_upper (r)
END

!-------------------------------------------------------------------------------
SUBROUTINE velocity_boundaries (px, py, pz, xdnr, ydnr, zdnr, Ux, Uy, Uz)
  USE grid_m, only: mx, my, mz, lb, ub, mpi_z, mpi_mz
  implicit none
  real, dimension(mx,my,mz):: px, py, pz, xdnr, ydnr, zdnr, Ux, Uy, Uz
!...............................................................................
  call symmetric_center_upper (Ux)
  call symmetric_center_upper (Uy)
  if (mpi_z==mpi_mz-1) px(:,:,ub+1:mz) = xdnr(:,:,ub+1:mz)*Ux(:,:,ub+1:mz)
  if (mpi_z==mpi_mz-1) py(:,:,ub+1:mz) = ydnr(:,:,ub+1:mz)*Uy(:,:,ub+1:mz)
  call antisymmetric_face_upper (Uz)
  if (mpi_z==mpi_mz-1) pz(:,:,ub+1:mz) = zdnr(:,:,ub+1:mz)*Uz(:,:,ub+1:mz)
!...............................................................................
  call symmetric_center_lower (Ux)
  call symmetric_center_lower (Uy)
  if (mpi_z==0) px(:,:,1:lb-1) = xdnr(:,:,1:lb-1)*Ux(:,:,1:lb-1)
  if (mpi_z==0) py(:,:,1:lb-1) = ydnr(:,:,1:lb-1)*Uy(:,:,1:lb-1)
  call symmetric_face_lower (Uz)
  if (mpi_z==0) pz(:,:,1:lb) = zdnr(:,:,1:lb)*Uz(:,:,1:lb)
END

!-------------------------------------------------------------------------------
SUBROUTINE magnetic_field_boundaries (Bx, By, Bz, Ux, Uy, Uz)
  USE grid_m, only: mx, my, mz, lb, ub, mpi_z, mpi_mz
  implicit none
  real, dimension(mx,my,mz):: Bx, By, Bz, Ux, Uy, Uz
END
!-------------------------------------------------------------------------------
SUBROUTINE electric_field_boundaries (Ex, Ey, Ez)
  USE grid_m, only: mx, my, mz, lb, ub, mpi_z, mpi_mz
  implicit none
  real, dimension(mx,my,mz):: Ex, Ey, Ez
!...............................................................................
  call antisymmetric_face_lower (Ex)
  call antisymmetric_face_lower (Ey)
  call symmetric_center_lower (Ez)
!...............................................................................
  call antisymmetric_face_upper (Ex)
  call antisymmetric_face_upper (Ey)
  call symmetric_center_upper (Ez)
END

!-------------------------------------------------------------------------------
SUBROUTINE energy_boundaries (e, r, Ux, Uy, Uz)
  USE grid_m, only: mx, my, mz, lb, ub, e_lower, e_upper, mpi_z, mpi_mz
  implicit none
  real, dimension(mx,my,mz):: e, r, Ux, Uy, Uz
!...............................................................................
  if (mpi_z==0) e(:,:,lb) = e_lower
  call extrapolate_center_log_lower (e)
  call extrapolate_center_log_upper (e)
END

!-------------------------------------------------------------------------------
SUBROUTINE ddt_boundaries (drdt,dpxdt,dpydt,dpzdt,dedt,dBxdt,dBydt,dBzdt)
  USE grid_m, only: mx, my, mz, lb, ub, mpi_z, mpi_mz
  implicit none
  real, dimension(mx,my,mz):: drdt,dpxdt,dpydt,dpzdt,dedt,dBxdt,dBydt,dBzdt
!...............................................................................
  call symmetric_face_upper (dpxdt)                                             ! avoid crap outside
  call symmetric_face_upper (dpydt)                                             ! avoid crap outside
  call antisymmetric_face_upper (dpzdt)                                         ! impenetrable boundary
  call symmetric_center_upper (dBxdt)                                           ! avoid crap outside
  call symmetric_center_upper (dBydt)                                           ! avoid crap outside
  if (mpi_z == mpi_mz-1) then
    drdt (:,:,ub+1:mz) = 0.                                                     ! avoid crap outside boundary
    dedt (:,:,ub+1:mz) = 0.                                                     ! avoid crap outside boundary
  end if
!...............................................................................
  call symmetric_face_lower (dpxdt)                                             ! avoid crap outside
  call symmetric_face_lower (dpydt)                                             ! avoid crap outside
  call symmetric_face_lower (dpzdt)                                             ! allow flow through the bdry
  call symmetric_center_lower (dBxdt)                                           ! avoid crap outside
  call symmetric_center_lower (dBydt)                                           ! avoid crap outside
  if (mpi_z == 0) then
    drdt (:,:,1:lb) = 0.                                                        ! constant boundary value
    dedt (:,:,1:lb) = 0.                                                        ! constant boundary value
  end if
END
