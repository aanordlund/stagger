! $Id: mhd.f90,v 1.14 2009/10/07 10:31:19 aake Exp $
!***********************************************************************
MODULE mhd_m
  real   :: nu0, nu1, nu2, nue, nuB, nuS
END MODULE

!***********************************************************************
SUBROUTINE init_mhd
  USE mhd_m
  USE grid_m
  implicit none
  namelist /mhd/ nu0, nu1, nu2, nue, nuB, nuS, gamma
!.......................................................................
  nu0=0.007; nu1=0.007; nu2=1.0; nue=1.; nuB=0.5; nuS=1.
  rewind(stdin); read (stdin,mhd)
  if (stdout>0) then; write(stdout,*) hl; write(stdout,mhd); end if
  if (master) write(2,mhd)
END SUBROUTINE init_mhd

!-----------------------------------------------------------------------
SUBROUTINE pde
  USE grid_m
  USE stagger_m
  USE mhd_m
  USE time_m, only: Cu, Cv, Cn
  implicit none
  integer nchunk, maxchunk
  logical,save:: first_time=.true.
  real fmaxval, gb
  real, allocatable, dimension(:,:,:):: lnr,U,pp,cA, &                  ! temporary arrays
    Sxx,Syy,Szz,Sxy,Syz,Szx,Txx,Tyy,Tzz,Txy,Tyz,Tzx, &
    divU,nu,eta,jx,jy,jz,Ex,Ey,Ez,lnee,lnu,xdnr,ydnr,zdnr
  character(len=80):: id='$Id: mhd.f90,v 1.14 2009/10/07 10:31:19 aake Exp $'
!.......................................................................
  call print_id (id)

!-----------------------------------------------------------------------
!  Mass conservation, cell centered
!-----------------------------------------------------------------------
  drdt = drdt - ddxup(px) - ddyup(py) - ddzup(pz)

!-----------------------------------------------------------------------
!  Velocities
!-----------------------------------------------------------------------
  nchunk=16; maxchunk=16; maxchunk=max(nchunk,maxchunk)
  allocate (lnr(mx,my,mz),U(mx,my,mz),xdnr(mx,my,mz),ydnr(mx,my,mz),zdnr(mx,my,mz))
  nchunk=nchunk+5; maxchunk=max(nchunk,maxchunk)
    call density_boundaries (r)
  lnr = alog(r)
  xdnr = exp(xdn(lnr))
  ydnr = exp(ydn(lnr))
  zdnr = exp(zdn(lnr))
  Ux = px/xdnr
  Uy = py/ydnr
  Uz = pz/zdnr
    call velocity_boundaries (px, py, pz, xdnr, ydnr, zdnr,Ux, Uy, Uz)
  U  = sqrt(xup(Ux)**2 + yup(Uy)**2  + zup(Uz)**2)
  call forcing (xdnr, ydnr, zdnr)
  deallocate (xdnr,ydnr,zdnr)

!-----------------------------------------------------------------------
!  Equation of state 
!-----------------------------------------------------------------------
  allocate (pp(mx,my,mz),cA(mx,my,mz))
  nchunk=nchunk+2; maxchunk=max(nchunk,maxchunk)
    call energy_boundaries (e, r, Ux, Uy, Uz)
  pp = (gamma-1.)*e
  cA = sqrt((xup(Bx)**2 + yup(By)**2  + zup(Bz)**2 + pp*gamma)/r)

!-----------------------------------------------------------------------
!  Numerical diffusion and resistivity, Richtmyer & Morton type
!-----------------------------------------------------------------------
  allocate (divU(mx,my,mz),nu(mx,my,mz),eta(mx,my,mz))
  nchunk=nchunk+3; maxchunk=max(nchunk,maxchunk)
  divU = ddxup(Ux) + ddyup(Uy) + ddzup(Uz)
  nu  = nu0*dsmax + nu1*dsmax*(U+cA) + nu2*dsmax**2*smooth3(max3(max(-divU,0.0)))
  eta = nuB*nu

!-----------------------------------------------------------------------
!  Courant numbers based on wave speed, viscous and resistive diffusion
!-----------------------------------------------------------------------
  Cu = fmaxval(U+cA)
  Cv = 6.2*3.*fmaxval(nu )/2.*0.4/0.6
  Cn = 6.2*3.*fmaxval(eta)/2.*0.4/0.6
  deallocate (U,cA)
  nchunk=nchunk-2; maxchunk=max(nchunk,maxchunk)

!-----------------------------------------------------------------------
!  Viscous stress, diagonal and off-diagonal elements
!-----------------------------------------------------------------------
  allocate (Sxx(mx,my,mz),Syy(mx,my,mz),Szz(mx,my,mz))
  allocate (Txx(mx,my,mz),Tyy(mx,my,mz),Tzz(mx,my,mz))
  allocate (Sxy(mx,my,mz),Syz(mx,my,mz),Szx(mx,my,mz))
  allocate (Txy(mx,my,mz),Tyz(mx,my,mz),Tzx(mx,my,mz))
  nchunk=nchunk+12; maxchunk=max(nchunk,maxchunk)
  Sxx = ddxup(Ux)
  Syy = ddyup(Uy)
  Szz = ddzup(Uz)
  Txx = - (2.*nuS)*nu*r*Sxx - 2.*(1.-nuS)*nu*r*divU
  Tyy = - (2.*nuS)*nu*r*Syy - 2.*(1.-nuS)*nu*r*divU
  Tzz = - (2.*nuS)*nu*r*Szz - 2.*(1.-nuS)*nu*r*divU
  Sxy = 0.5*(ddxdn(Uy)+ddydn(Ux))
  Szx = 0.5*(ddzdn(Ux)+ddxdn(Uz))
  Syz = 0.5*(ddydn(Uz)+ddzdn(Uy))
  allocate (lnu(mx,my,mz))
  lnu = alog(nu*r)
  Txy = - (2.*nuS)*exp(xdn1(ydn1(lnu)))*Sxy
  Tzx = - (2.*nuS)*exp(zdn1(xdn1(lnu)))*Szx
  Tyz = - (2.*nuS)*exp(ydn1(zdn1(lnu)))*Syz
  deallocate (nu)

!-----------------------------------------------------------------------
!  Electric current j = curl(B), edge centered
!-----------------------------------------------------------------------
  allocate (jx(mx,my,mz),jy(mx,my,mz),jz(mx,my,mz))
  nchunk=nchunk+4; maxchunk=max(nchunk,maxchunk)
    call magnetic_field_boundaries (Bx, By, Bz, Ux, Uy, Uz)
  jx = ddydn(Bz) - ddzdn(By)
  jy = ddzdn(Bx) - ddxdn(Bz)
  jz = ddxdn(By) - ddydn(Bx)

!-----------------------------------------------------------------------
!  Electric field   E = eta j - (u x B), edge centered
!-----------------------------------------------------------------------
  allocate (Ex(mx,my,mz),Ey(mx,my,mz),Ez(mx,my,mz))
  nchunk=nchunk+3; maxchunk=max(nchunk,maxchunk)
  Ex = ydn(zdn(eta))*jx
  Ey = zdn(xdn(eta))*jy
  Ez = xdn(ydn(eta))*jz
  dedt = dedt + zup1(yup1(jx*Ex) + xup1(jy*Ey)) + xup1(yup1(jz*Ez))
  Ex = Ex - zdn(Uy)*ydn(Bz) + ydn(Uz)*zdn(By)
  Ey = Ey - xdn(Uz)*zdn(Bx) + zdn(Ux)*xdn(Bz)
  Ez = Ez - ydn(Ux)*xdn(By) + xdn(Uy)*ydn(Bx)
  deallocate (eta)

!-----------------------------------------------------------------------
!  Magnetic field's time derivative, dBdt = - curl(E), face centered
!-----------------------------------------------------------------------
    call electric_field_boundaries (Ex, Ey, Ez)
  dBxdt = dBxdt + ddzup(Ey) - ddyup(Ez)
  dBydt = dBydt + ddxup(Ez) - ddzup(Ex)
  dBzdt = dBzdt + ddyup(Ex) - ddxup(Ey)
  deallocate (Ex,Ey,Ez)
  nchunk=nchunk-3; maxchunk=max(nchunk,maxchunk)

!-----------------------------------------------------------------------
!  Energy eq, with P*dV work, difffusion, kinetic and Joule dissipation
!-----------------------------------------------------------------------
  allocate (lnee(mx,my,mz))
  nchunk=nchunk+1; maxchunk=max(nchunk,maxchunk)
  lnee = alog(e/r)
  dedt = dedt - pp*divU &
       - ddxup (exp(xdn(lnee))*(px - nue*exp(xdn(lnu))*ddxdn(lnee))) &
       - ddyup (exp(ydn(lnee))*(py - nue*exp(ydn(lnu))*ddydn(lnee))) &
       - ddzup (exp(zdn(lnee))*(pz - nue*exp(zdn(lnu))*ddzdn(lnee))) &
       - (Txx*Sxx + Tyy*Syy + Tzz*Szz) &
       - 2.*(xup1(yup1(Txy*Sxy) + zup1(Tzx*Szx)) + yup1(zup1(Tyz*Syz)))
  deallocate (Sxx,Syy,Szz,Sxy,Syz,Szx,lnee,lnu,divU)
  nchunk=nchunk-8; maxchunk=max(nchunk,maxchunk)

!-----------------------------------------------------------------------
!  Pressure and Reynolds stress
!-----------------------------------------------------------------------
  Txx = Txx + pp + r*xup(Ux)**2
  Tyy = Tyy + pp + r*yup(Uy)**2
  Tzz = Tzz + pp + r*zup(Uz)**2
  Txy = Txy + exp(xdn1(ydn1(lnr)))*ydn(Ux)*xdn(Uy)
  Tzx = Tzx + exp(xdn1(zdn1(lnr)))*zdn(Ux)*xdn(Uz)
  Tyz = Tyz + exp(ydn1(zdn1(lnr)))*zdn(Uy)*ydn(Uz)
  deallocate (pp,lnr)
  nchunk=nchunk-5; maxchunk=max(nchunk,maxchunk)

!-----------------------------------------------------------------------
!  Equations of motion, face centred, with Lorentz force jxB
!-----------------------------------------------------------------------
  dpxdt = dpxdt - ddxdn(Txx) - ddyup(Txy) - ddzup(Tzx) &
        + zup(jy*xdn(Bz)) - yup(jz*xdn(By))
  dpydt = dpydt - ddydn(Tyy) - ddzup(Tyz) - ddxup(Txy) &
        + xup(jz*ydn(Bx)) - zup(jx*ydn(Bz))
  dpzdt = dpzdt - ddzdn(Tzz) - ddyup(Tyz) - ddxup(Tzx) &
        + yup(jx*zdn(By)) - xup(jy*zdn(Bx))
  deallocate (Txx,Tyy,Tzz,Txy,Tyz,Tzx,jx,jy,jz)
  nchunk=nchunk-9; maxchunk=max(nchunk,maxchunk)

    call ddt_boundaries (drdt, dpxdt, dpydt, dpzdt, dedt, dBxdt, dBydt, dBzdt)

  if (master .and. first_time) then
    gb = maxchunk*real(mx)*real(my)*real(mz)*4e-9
    print '(1x,a,f7.3,a)','Estimated memory per process = ',gb,' GB'
    gb = gb*mpi_size
    print '(1x,a,f7.1,a)','Estimated total memory       = ',gb,' GB'
    first_time = .false.
  end if
END
!***********************************************************************
