! $Id: io.f90,v 1.19 2009/10/09 03:15:02 aake Exp $
!***********************************************************************
MODULE io_m
END MODULE

!***********************************************************************
SUBROUTINE init_io
  USE io_m
  USE time_m
  USE grid_m
  implicit none
  character(len=80) inputfile, arg, compare
  namelist /redirect/ inputfile
  logical file_exists
  integer i, i0, i1
  character(len=80):: id='$Id: io.f90,v 1.19 2009/10/09 03:15:02 aake Exp $'
!.......................................................................
  call print_id (id)
  stdin = 10
  stdout = -1
  if (master) stdout = 6
  hl = '----------------------------------------'// &
       '----------------------------------------'

  call input (inputfile)                                                        ! EXPERIMENT/input.txt
  inquire (file='input.txt',exist=file_exists)
  if (file_exists) &
    inputfile = './input.txt'                                                   ! input.txt (for production)
  call getarg (1, arg)                                                          ! command argument (for alternatives)
  if (arg /= ' ') inputfile = arg
  i = index(inputfile,'/',back=.true.)                                          ! search for dir
  if (i > 0) then
    dir = inputfile(1:i)
  else
    dir = './'
  end if
  name = 'snapshot'

  if (master) then 
    write(stdout,*) hl
    write(stdout,*) 'Running the Simple Stagger Code...'
    write(stdout,'(1x,a,a)') 'Reading namelist data from ',inputfile
    write(stdout,'(1x,a,a)') 'Writing output to directory ',dir
  endif

  open (stdin,file=inputfile,status='old')                                      ! read namelist inputs

  compare = inputfile
  rewind(stdin); read (stdin,redirect)
  if (inputfile /= compare) then
    i0 = index(inputfile,'/',back=.true.)                                       ! search for /
    i1 = index(inputfile,'.',back=.true.)-1                                     ! search for .
    if (i0 <= 0) i0 = 1                                                         ! use all of it
    if (i1 <= 0) i1 = index(inputfile,' ')                                      ! use all of it
    name = inputfile(i0:i1)                                                     ! use file from file.ext
    inputfile = trim(dir)//inputfile                                            ! prepend directory
    if (master) write(stdout,*) 'Input redirected to '//trim(inputfile)
    close (stdin)
    open (stdin,file=inputfile,status='old')
  end if

  if (master) & 
    open (2,file=trim(dir)//'tmp.nml', form='formatted', status='unknown')      ! record namelist inputs

  mvar = 8                                                              ! number of variables
  nsnap = -1                                                            ! reset in timestep
  tsnap = -1                                                            ! reset in timestep
END SUBROUTINE

!***********************************************************************
SUBROUTINE end_io
  USE grid_m, only: master
  implicit none
  if (master) close (2, status='delete')
END SUBROUTINE

!***********************************************************************
SUBROUTINE write_snapshot
  USE io_m
  USE grid_m
  USE time_m
  implicit none
  integer mv
  character(len=8) csnap
  character(len=100) line
  logical flag
  namelist /state/ mx, my, mz, mv, sx, sy, sz, t, dt
!.......................................................................
  if (t < tnext .and. &                                                 ! snap shot after time tsnap
      .not.flag('snapshot')) return                                     ! or 'touch snapshot.flag'
!
  info = trim(info)//'F'
  write(csnap,'(i8)') iwrite
  info = trim(info)//adjustl(trim(csnap))                               ! append Fxx to printout
!
  if (master) then
    open (1,file=trim(name)//'.nml', &                                  ! open .nml file
      status='unknown', form='formatted')                               ! formatted
    mv = mvar
    mx = mx*mpi_mx; my=my*mpi_my; mz=mz*mpi_mz                          ! total size
    write (1,state)                                                     ! namelist -> grid size
    mx = mx/mpi_mx; my=my/mpi_my; mz=mz/mpi_mz                          ! local size
    rewind (2)                                                          ! rewind tmp.in
    do while (.true.)                                                   ! loop over lines
      read (2,'(a)',end=1) line                                         ! copy input namelists
      write (1,'(a)') trim(line)
    end do
  1 close (1)
!
    open (1,file=trim(name)//'.times', &                                ! open .times file
      status='unknown', form='formatted', position='append')            ! append
    write (1,'(i6,1p,2e15.6)') iwrite,t,dt                              ! table with time and time step
    close (1)
  end if
!
  call file_openw (trim(name)//'.dat')
  call file_write (iwrite*mvar+0,r )                                    ! density
  call file_write (iwrite*mvar+1,px)                                    ! x-momentum
  call file_write (iwrite*mvar+2,py)                                    ! y-momentum
  call file_write (iwrite*mvar+3,pz)                                    ! z-momentum
  call file_write (iwrite*mvar+4,e )                                    ! energy
  call file_write (iwrite*mvar+5,bx)                                    ! Bx
  call file_write (iwrite*mvar+6,by)                                    ! By
  call file_write (iwrite*mvar+7,bz)                                    ! Bz
  call file_close ()
  call barrier_mpi
!
  tnext = tnext + tsnap                                                 ! next snapshot time
  iwrite = iwrite + 1                                                   ! increment snap shot number
END SUBROUTINE

!***********************************************************************
SUBROUTINE read_snapshot
  USE io_m
  USE grid_m
  USE time_m
  implicit none
  integer i, it1, mv
  real wallclock, wc0, t1, dt1
  namelist /state/ mx, my, mz, mv, sx, sy, sz, t, dt
!.......................................................................
  open (1,file=trim(name)//'.times',status='old', form='formatted')     ! open times file
  do while (.true.)                                                     ! scan through
    read (1,'(i6,2e15.6)',end=1) it1, t1, dt1                           ! read old info
    if (it1 == iread) then; t=t1; dt=dt1; endif                         ! use the LAST match
  end do
1 close (1)                                                             ! close file
  tnext = t + tsnap                                                     ! snapshot time
  tend  = t + nsnap*tsnap                                               ! ending time
!
  wc0 = wallclock()
  call file_openr (trim(name)//'.dat')
  call file_read (iread*mvar+0,r )                                      ! density
  call file_read (iread*mvar+1,px)                                      ! x-momentum
  call file_read (iread*mvar+2,py)                                      ! y-momentum
  call file_read (iread*mvar+3,pz)                                      ! z-momentum
  call file_read (iread*mvar+4,e )                                      ! energy
  call file_read (iread*mvar+5,bx)                                      ! Bx
  call file_read (iread*mvar+6,by)                                      ! By
  call file_read (iread*mvar+7,bz)                                      ! Bz
  call file_close ()
  if (master) then
    print'(a,i4,a,1pe10.3)',' recovered snapshot nr',iread,', t =',t
    print'(a,f5.1,a)',' wallclock time',wallclock()-wc0,' sec'
  end if
END SUBROUTINE

!***********************************************************************
REAL FUNCTION wallclock()                                               ! wall clock timer
  implicit none
  integer, save:: count, count_rate=0, count_max
  real, save:: previous=0., offset=0.
!.......................................................................
  if (count_rate == 0) then
    call system_clock(count=count, count_rate=count_rate, count_max=count_max)
    offset = -count/real(count_rate)
  else
    call system_clock(count=count)
  end if
  wallclock = count/real(count_rate) + offset
  if (wallclock < previous) then                                        ! handle wrap-around
    offset = offset + count_max/real(count_rate)                        ! add wrap-around offset
    wallclock = count/real(count_rate) + offset
  end if
  previous = wallclock
END

!***********************************************************************
LOGICAL FUNCTION thesame (label, rec, f, g, ndif)
  USE grid_m
  implicit none
  real, dimension(mx,my,mz):: f, g
  integer ix,iy,iz,maxprint,ndif,rec
  character(len=*) label
!
  thesame = .true.
  maxprint = 10
  ndif = 0
  do iz=1,mz
  do iy=1,my
  do ix=1,mx
    if (f(ix,iy,iz) .ne. g(ix,iy,iz)) then
      if (ndif < maxprint) then
        thesame = thesame .and. .false.
        print 1,'difference:',label,mpi_rank,rec,ix,iy,iz,f(ix,iy,iz),g(ix,iy,iz)
1       format(2(1x,a),i3,4i5,2(1pg16.8))
      end if
      ndif = ndif + 1
    end if
  end do
  end do
  end do
END

!***********************************************************************
SUBROUTINE dump (f,label,dumpfile,rec0)
  USE grid_m
  implicit none
  real, dimension(mx,my,mz):: f
  character(len=*) label,dumpfile
  integer rec0, rec, ndif
  integer, parameter:: mfiles=30
  integer, save:: nfiles=0, ifile, irec(mfiles)
  character(len=80), save:: files(mfiles), fname
  logical file_exists, ok, thesame

  if (.not. (do_dump.or.do_compare)) return                             ! faster than file_exists()
  rec = rec0

  do ifile=1,nfiles
    if (trim(dumpfile) == trim(files(ifile))) then
      goto 1
    end if
  end do
  nfiles = nfiles+1
  if (nfiles > mfiles) then
    if (master) print *,'dumpn: nfiles, mfiles =', nfiles, mfiles
    call shut_down ('error')
  end if
  ifile = nfiles
  files(ifile) = dumpfile
  irec(ifile) = -1
  if (master) print*,'dumpn: ifile, file =', ifile, trim(files(ifile))

1 continue
  if (rec == 0) then
    irec(ifile)=0
  else
    irec(ifile) = irec(ifile)+1
  end if
  rec = irec(ifile)

  fname = dumpfile
  inquire (file=fname,exist=file_exists)
  if (file_exists) then
    if (do_compare) then
      allocate (scratch(mx,my,mz))
      call file_openr (fname)
      call file_read (rec,scratch)
      call file_close
      ok = thesame(label,rec,f,scratch,ndif)
      if (ok) then
        print 2,'compare:',label,trim(fname),rec,mpi_rank,ok
      else
        print 2,'compare:',label,trim(fname),rec,mpi_rank,ok,ndif/float(mw)
      end if
2     format(3(1x,a),2i5,l5,f8.3)
      deallocate(scratch)
    else
      if (master) print 2,'dump:',label,trim(fname),rec,mpi_rank
      call file_openr (fname)
      call file_write (rec,f)
      call file_close
    end if
  end if
END

!***********************************************************************
SUBROUTINE print_id (id)
  USE grid_m
  implicit none
  character(len=80) id
  if (master .and. id /= ' ') then
    print *, trim(id)
    id = ' '
  end if
END
