! $Id: timestep.f90,v 1.19 2009/10/07 12:40:01 aake Exp $
!***********************************************************************
MODULE time_m
  USE grid_m
  real    :: Cu, Cv, Cn, Cr, Ce                                         ! Courant umbers
  real    :: alpha(3), beta(3)                                          ! time step coefficients
  real    :: tsnap                                                      ! time between snapshots
  real    :: t, dt, Cdt                                                 ! time, time step, Courant
  real    :: tend, tnext                                                ! snapshot interval and time
  integer :: nprint, it, nsnap, iread, iwrite, timeorder                ! snapshot number, total
END MODULE

!***********************************************************************
SUBROUTINE init_timestep
  USE time_m
  USE grid_m, only: hl
  implicit none
  integer i
  namelist /time/ Cdt, t, dt, nprint, timeorder
  namelist /out/ iread, iwrite, tsnap, nsnap, verbose, name, &          ! output control namelist
                 do_dump, do_compare
  character(len=80):: id='$Id: timestep.f90,v 1.19 2009/10/07 12:40:01 aake Exp $'
!.......................................................................
  call print_id (id)
  Cdt       = 0.4                                                       ! Courant condition
  t         = 0.                                                        ! reset time
  dt        = Cdt*min(dx,dy,dz)                                         ! default time step
  it        = 0                                                         ! reset time step counter
  timeorder = 3                                                         ! default 3rd order time steps
  nprint= min(100,max(1,ceiling(1./(3e-6*mx*my*mz))))                   ! cadence of printing

  rewind(stdin); read (stdin,time)                                      ! time step control namelist
  if (stdout>0) then; write(*,*) hl; write (*,time); end if 
  if (master) write (2,time)
 
  if (timeorder == 3) then                                              ! third order time steps
    alpha = (/     0., -0.641874, -1.31021 /)
    beta  = (/0.46173,  0.924087, 0.390614 /)
  else                                                                  ! first order time steps
    alpha = (/ 0.   ,  0.   , 0. /)
    beta  = (/ 1.   ,  0.   , 0. /)
  end if
 
  iread = -1                                                            ! input snapshot
  iwrite = -1                                                           ! output snapshot
  if (nsnap < 0) nsnap = 1                                              ! number of snapshots
  if (tsnap < 0) tsnap = 0.                                             ! stop after one timestep
  verbose = 0                                                           ! level of diagnostics
  do_dump = .false.                                                     ! dump for debugging
  do_compare = .false.                                                  ! compare with dump

  rewind (stdin); read  (stdin,out)                                     ! output control namelist
  if (iwrite == -1) iwrite = iread+1                                    ! next slot for snapshot
  if (master) write (2,out)

  tnext = t                                                             ! snapshot time
  if (iread >= 0) call read_snapshot                                    ! read old snapshot
  tend  = t + nsnap*tsnap                                               ! ending time

  if ((.not.master) .and. (verbose < 3)) stdout = -1                    ! disable stdout on MPI slaves
  if (stdout>0) then; write(stdout,*) hl; write (stdout,out); endif     ! write to log

  name = trim(dir)//trim(name)                                          ! .. prepend input directory 
  if (master) print*,'Output file: '//trim(name)//'.dat'
END SUBROUTINE

!***********************************************************************
SUBROUTINE timestep
!
!  3rd order, 2-N storage Runge-Kutta.  Advances the eight MHD variables
!  forward in time, given time derivatives computed in the subroutine pde
!
  USE time_m
  USE grid_m
  implicit none
!.......................................................................
  integer n
 
  do n=1,timeorder                                                      ! normally three Runge-Kutta stages
    drdt  = alpha(n)*drdt
    dpxdt = alpha(n)*dpxdt
    dpydt = alpha(n)*dpydt
    dpzdt = alpha(n)*dpzdt
    dedt  = alpha(n)*dedt
    dBxdt = alpha(n)*dBxdt
    dBydt = alpha(n)*dBydt
    dBzdt = alpha(n)*dBzdt

    call pde                                                            ! get the dfdt's
    if (n .eq.1) call Courant                                           ! set dt in the first stage
    
    r   = r   + (dt*beta(n))*drdt
    px  = px  + (dt*beta(n))*dpxdt
    py  = py  + (dt*beta(n))*dpydt
    pz  = pz  + (dt*beta(n))*dpzdt
    e   = e   + (dt*beta(n))*dedt
    Bx  = Bx  + (dt*beta(n))*dBxdt
    By  = By  + (dt*beta(n))*dBydt
    Bz  = Bz  + (dt*beta(n))*dBzdt
  end do
  call dump(drdt ,'drdt ','dfdt.dmp',0)
  call dump(dpxdt,'dpxdt','dfdt.dmp',1)
  call dump(dpydt,'dpydt','dfdt.dmp',1)
  call dump(dpzdt,'dpzdt','dfdt.dmp',1)
  call dump(dedt ,'dedt ','dfdt.dmp',1)
  call dump(dBxdt,'dBxdt','dfdt.dmp',1)
  call dump(dBydt,'dBydt','dfdt.dmp',1)
  call dump(dBzdt,'dBzdt','dfdt.dmp',1)
END

!***********************************************************************
SUBROUTINE Courant                                                      ! Courant condition
  USE grid_m
  USE time_m
  !USE io_m
  implicit none
  real fmaxval, wallclock, wc1, rem
  real, save:: wc0=0., wc2=0., t0=-1.
  integer, save:: iprint=0
  character(len=1):: unit
!.......................................................................
  Cr = dt*fmaxval(abs(drdt/r))                                          ! density change
  Ce = dt*fmaxval(abs(dedt/e))                                          ! energy change
  Cu = Cu*dt/dsmin                                                      ! wave speed
  Cv = Cv*dt/dsmin**2                                                   ! viscouse diffusion speed
  Cn = Cn*dt/dsmin**2                                                   ! magnetic diffusion speed

  if (master .and. &                                                    ! only on MPI master
     (mod(it,nprint)==0 .or. &                                          ! only every nprint step
      trim(info) /= '') ) then                                          ! or if there is info
    wc1 = wallclock()                                                   ! wall clock
    if (wc2==0.) wc2 = wc1                                              ! save initial
    if (t0 < 0.) t0 = t
    rem = (wc1-wc2)*(tend-t)/max(t-t0,dt)                               ! remaining time
    unit = 's'                                                          ! in seconds
    if (rem > 100.) then
      rem = rem/60.                                                     ! in minutes
      unit = 'm'
      if (rem > 100.) then
        rem = rem/60.                                                   ! in hours
        unit = 'h'
        if (rem >  48.) then
          rem = rem/24.                                                 ! in days
          unit = 'd'
        end if
      end if
    end if

    if (it == 0) &
      print'(/a)','   <-- time info -->        <--------- Courant numbers --------->    <-- speed -->'
    if (mod(iprint,50) == 0) &                                          ! header every 50 lines
      print'(a)','    it   t      dt          waves   visc    resist  dens    energ    musp  remains'
    print'(i6,f8.3,f9.5,2x,5f8.3,f8.2,f6.1,a1,2x,a)',it,t,dt,Cu,Cv,Cn,Cr,Ce, &
      (wc1-wc0)*1e6/(mx*my*mz)/nprint,rem,unit,trim(info)
    info = ''
    wc0 = wc1                                                           ! remember wall clock
    iprint = iprint+1                                                   ! count printed lines
  endif
  dt = dt*Cdt/max(Cu,Cv,Cn,Cr,Ce)                                       ! set new time step
END SUBROUTINE
!***********************************************************************
