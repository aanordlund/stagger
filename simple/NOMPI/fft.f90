! $Id: fft.f90,v 1.3 2009/10/07 15:48:36 aake Exp $
!----------------------------------------------------------------------
!  Three-dimensional Fourier transform routines, based on FFTPACK.
!  FFTPACK is transportable, and allows arbitrary dimensions (speed
!  will vary, of course).
!
!  CALL fft_f (f, ft, mx, my, mz)              ! forward transform
!  CALL fft_r (ft, f, mx, my, mz)              ! backward transform
!  CALL fft_k2 (k2, dx, dy, dz, mx, my, mz)    ! compute k2
!
!**********************************************************************
SUBROUTINE fft_k2 (k2,dx1,dy1,dz1,mx1,my1,mz1)
  USE grid_m
  implicit none
  real, dimension(mx,my,mz):: k2
  real dx1, dy1, dz1, kx, ky, kz
  integer i, j, k, mx1, my1, mz1
!----------------------------------------------------------------------
  do k=1,mz
    kz=2.*pi/sz*(k/2)
    do j=1,my
      ky=2.*pi/sy*(j/2)
      do i=1,mx
        kx=2.*pi/sx*(i/2)
        k2(i,j,k)=kx**2+ky**2+kz**2
      end do
    end do
  end do
END SUBROUTINE

!**********************************************************************
SUBROUTINE fft_f (r,rr,mx1,my1,mz1)
  USE grid_m, only: mx, my, mz, pi
  implicit none
  integer i,j,k,mx1,my1,mz1
  integer, parameter:: lw=1024, lwx=2*lw+15, lwy=2*lw+15, lwz=2*lw+15
  real, dimension(mx,my,mz):: r, rr
  real c, wx(lwx), wy(lwy), wz(lwz), fx(mx), fy(my), fz(mz,mx)
!----------------------------------------------------------------------
  call srffti (mx,wx)
  call srffti (my,wy)
  call srffti (mz,wz)

  do k=1,mz
   do j=1,my
    fx(:)=r(:,j,k)
    call srfftf (mx,fx,wx)
    c=1./mx
    rr(:,j,k)=c*fx(:)
   end do
   do i=1,mx
    fy=rr(i,:,k)
    call srfftf (my,fy,wy)
    c=1./my
    rr(i,:,k)=c*fy
   end do
  end do

  do j=1,my
    do k=1,mz
      fz(k,:)=rr(:,j,k)
    end do
    do i=1,mx
      call srfftf (mz,fz(:,i),wz)
    end do
    c=1./mz
    do k=1,mz
      rr(:,j,k)=c*fz(k,:)
    end do
  end do
END SUBROUTINE

!**********************************************************************
SUBROUTINE fft_r (rr,r,mx1,my1,mz1)
  USE grid_m, only: mx, my, mz
  implicit none
  integer i,j,k,mx1,my1,mz1
  integer, parameter:: lw=1024, lwx=2*lw+15, lwy=2*lw+15, lwz=2*lw+15
  real, dimension(mx,my,mz):: r, rr
  real wx(lwx), wy(lwy), wz(lwz), fx(mx), fy(my), fz(mz,mx)
!----------------------------------------------------------------------
  call srffti (mx,wx)
  call srffti (my,wy)
  call srffti (mz,wz)

  do j=1,my
    do k=1,mz
      fz(k,:)=rr(:,j,k)
    end do
    do i=1,mx
      call srfftb (mz,fz(:,i),wz)
    end do
    do k=1,mz
      r(:,j,k)=fz(k,:)
    end do
  end do

  do k=1,mz
   do i=1,mx
    fy=r(i,:,k)
    call srfftb (my,fy,wy)
    r(i,:,k)=fy
   end do
   do j=1,my
    fx=r(:,j,k)
    call srfftb (mx,fx,wx)
    r(:,j,k)=fx
   end do
  end do
END SUBROUTINE
