! $Id: stagger.f90,v 1.7 2009/11/09 23:29:27 aake Exp $
!***********************************************************************
MODULE stagger_m
  USE grid_m

  real a, b, c
  real ax1, ay1, az1
  real ax3, ay3, az3
  real bx3, by3, bz3
  real cx3, cy3, cz3
  integer order
  logical do_stretch
CONTAINS
!***********************************************************************
  function xup (f)
!
!  f is centered on (i-.5,j,k), fifth order stagger
!
  real, dimension(mx,my,mz):: f, xup
!-----------------------------------------------------------------------
!
  do k=1,mz
   do j=1,my
    xup(mx-2,j,k) = ( &
                   a*(f(mx-2,j,k)+f(mx-1,j,k)) &
                 + b*(f(mx-3,j,k)+f(mx  ,j,k)) &
                 + c*(f(mx-4,j,k)+f(1   ,j,k)))
    xup(mx-1,j,k) = ( &
                   a*(f(mx-1,j,k)+f(mx  ,j,k)) &
                 + b*(f(mx-2,j,k)+f(1   ,j,k)) &
                 + c*(f(mx-3,j,k)+f(2   ,j,k)))
    xup(mx  ,j,k) = ( &
                   a*(f(mx  ,j,k)+f(1   ,j,k)) &
                 + b*(f(mx-1,j,k)+f(2   ,j,k)) &
                 + c*(f(mx-2,j,k)+f(3   ,j,k)))
    xup(1   ,j,k) = ( &
                   a*(f(1   ,j,k)+f(2   ,j,k)) &
                 + b*(f(mx  ,j,k)+f(3   ,j,k)) &
                 + c*(f(mx-1,j,k)+f(4   ,j,k)))
    xup(2   ,j,k) = ( &
                   a*(f(2   ,j,k)+f(3   ,j,k)) &
                 + b*(f(1   ,j,k)+f(4   ,j,k)) &
                 + c*(f(mx  ,j,k)+f(5   ,j,k)))
   end do
   do j=1,my
    do i=3,mx-3
      xup(i  ,j,k) = ( &
                    a*(f(i  ,j,k)+f(i+1 ,j,k)) &
                  + b*(f(i-1,j,k)+f(i+2 ,j,k)) &
                  + c*(f(i-2,j,k)+f(i+3 ,j,k)))
    end do
   end do
  end do
!
  end function

!***********************************************************************
  function xup1 (f)
!
!  f is centered on (i,j,k), fifth order stagger
!
  real, dimension(mx,my,mz):: f, xup1
!-----------------------------------------------------------------------
!
  do k=1,mz
  do j=1,my
    xup1(mx ,j,k) = 0.5*(f(mx  ,j,k)+f(1   ,j,k))
  end do
  do j=1,my
    do i=1,mx-1
      xup1(i,j,k) = 0.5*(f(i   ,j,k)+f(i+1 ,j,k))
    end do
  end do
  end do
!
  end function

!***********************************************************************
  function yup (f)
!
!  f is centered on (i,j-.5,k), fifth order stagger
!
  real, dimension(mx,my,mz):: f, yup
!-----------------------------------------------------------------------
!
  if (my.eq.1) then
    yup = f
    return
  end if
!
  do k=1,mz
    do i=1,mx
      yup(i,my-2,k) = ( &
                     a*(f(i,my-2,k)+f(i,my-1,k)) + &
                     b*(f(i,my-3,k)+f(i,my  ,k)) + &
                     c*(f(i,my-4,k)+f(i,1   ,k)))
      yup(i,my-1,k) = ( &
                     a*(f(i,my-1,k)+f(i,my  ,k)) + &
                     b*(f(i,my-2,k)+f(i,1   ,k)) + &
                     c*(f(i,my-3,k)+f(i,2   ,k)))
      yup(i,my  ,k) = ( &
                     a*(f(i,my  ,k)+f(i,1   ,k)) + &
                     b*(f(i,my-1,k)+f(i,2   ,k)) + &
                     c*(f(i,my-2,k)+f(i,3   ,k)))
      yup(i,1   ,k) = ( &
                     a*(f(i,1   ,k)+f(i,2   ,k)) + &
                     b*(f(i,my  ,k)+f(i,3   ,k)) + &
                     c*(f(i,my-1,k)+f(i,4   ,k)))
      yup(i,2   ,k) = ( &
                     a*(f(i,2   ,k)+f(i,3   ,k)) + &
                     b*(f(i,1   ,k)+f(i,4   ,k)) + &
                     c*(f(i,my  ,k)+f(i,5   ,k)))
    end do
    do j=3,my-3
     do i=1,mx
      yup(i,j  ,k) = ( &
                     a*(f(i,j   ,k)+f(i,j+1 ,k)) + &
                     b*(f(i,j-1 ,k)+f(i,j+2 ,k)) + &
                     c*(f(i,j-2 ,k)+f(i,j+3 ,k)))
     end do
    end do
  end do
!
  end function

!***********************************************************************
  function yup1 (f)
!
!  f is centered on (i,j,k), fifth order stagger
!
  real, dimension(mx,my,mz):: f, yup1
!-----------------------------------------------------------------------
!
  if (my.eq.1) then
    yup1 = f
    return
  end if
!
  do k=1,mz
    do i=1,mx
      yup1(i,my,k) = 0.5*(f(i,my  ,k)+f(i,1   ,k))
    end do
    do j=1,my-1
     do i=1,mx
      yup1(i,j,k) = 0.5*(f(i,j   ,k)+f(i,j+1 ,k))
     end do
    end do
  end do
!
  end function

!***********************************************************************
  function zup (f)
!
!  f is centered on (i,j,k-.5), fifth order stagger
!
  real, dimension(mx,my,mz):: f, zup
!-----------------------------------------------------------------------
!
  if (mz.eq.1) then
    zup = f
    return
  end if
!
  do k=1,mz
    km2=mod(k-3+mz,mz)+1
    km1=mod(k-2+mz,mz)+1
    kp1=mod(k     ,mz)+1
    kp2=mod(k+1   ,mz)+1
    kp3=mod(k+2   ,mz)+1
    do j=1,my
      do i=1,mx
        zup(i,j,k) = ( &
                     a*(f(i,j,k  )+f(i,j,kp1)) + &
                     b*(f(i,j,km1)+f(i,j,kp2)) + &
                     c*(f(i,j,km2)+f(i,j,kp3)))
      end do
    end do
  end do
!
  end function
!***********************************************************************
  function zup1 (f)
!
!  f is centered on (i,j,k), fifth order stagger
!
  real, dimension(mx,my,mz):: f, zup1
!-----------------------------------------------------------------------
!
  if (mz.eq.1) then
    zup1 = f
    return
  end if
!
  do k=1,mz
    kp1=mod(k     ,mz)+1
    do j=1,my
      do i=1,mx
        zup1(i,j,k) = 0.5*(f(i,j,k  )+f(i,j,kp1))
      end do
    end do
  end do
!
  end function

!***********************************************************************
  function xdn (f)
!
!  f is centered on (i,j,k), fifth order stagger
!
  real, dimension(mx,my,mz):: f, xdn
!-----------------------------------------------------------------------
!
  do k=1,mz
  do j=1,my
    xdn(mx-1,j,k) = ( &
                   a*(f(mx-2,j,k)+f(mx-1,j,k)) &
                 + b*(f(mx-3,j,k)+f(mx  ,j,k)) &
                 + c*(f(mx-4,j,k)+f(1   ,j,k)))
    xdn(mx  ,j,k) = ( &
                   a*(f(mx-1,j,k)+f(mx  ,j,k)) &
                 + b*(f(mx-2,j,k)+f(1   ,j,k)) &
                 + c*(f(mx-3,j,k)+f(2   ,j,k)))
    xdn(1  ,j,k) = ( &
                   a*(f(mx  ,j,k)+f(1   ,j,k)) &
                 + b*(f(mx-1,j,k)+f(2   ,j,k)) &
                 + c*(f(mx-2,j,k)+f(3   ,j,k)))
    xdn(2  ,j,k) = ( &
                   a*(f(1   ,j,k)+f(2   ,j,k)) &
                 + b*(f(mx  ,j,k)+f(3   ,j,k)) &
                 + c*(f(mx-1,j,k)+f(4   ,j,k)))
    xdn(3  ,j,k) = ( &
                   a*(f(2   ,j,k)+f(3   ,j,k)) &
                 + b*(f(1   ,j,k)+f(4   ,j,k)) &
                 + c*(f(mx  ,j,k)+f(5   ,j,k)))
  end do
  do j=1,my
    do i=3,mx-3
      xdn(i+1,j,k) = ( &
                   a*(f(i   ,j,k)+f(i+1 ,j,k)) &
                 + b*(f(i-1 ,j,k)+f(i+2 ,j,k)) &
                 + c*(f(i-2 ,j,k)+f(i+3 ,j,k)))
    end do
  end do
  end do
!
  end function

!***********************************************************************
  function xdn1 (f)
!
!  f is centered on (i,j,k), fifth order stagger
!
  real, dimension(mx,my,mz):: f, xdn1
!-----------------------------------------------------------------------
!
  do k=1,mz
  do j=1,my
    xdn1(1  ,j,k) = 0.5*(f(mx  ,j,k)+f(1   ,j,k))
  end do
  do j=1,my
    do i=1,mx-1
      xdn1(i+1,j,k) = 0.5*(f(i   ,j,k)+f(i+1 ,j,k))
    end do
  end do
  end do
!
  end function

!***********************************************************************
  function ydn (f)
!
!  f is centered on (i,j,k), fifth order stagger
!
  real, dimension(mx,my,mz):: f, ydn
!-----------------------------------------------------------------------
!
  if (my.eq.1) then
    ydn = f
    return
  end if
!
  do k=1,mz
    do i=1,mx
      ydn(i,my-1,k) = ( &
                     a*(f(i,my-2,k)+f(i,my-1,k)) + &
                     b*(f(i,my-3,k)+f(i,my  ,k)) + &
                     c*(f(i,my-4,k)+f(i,1   ,k)))
      ydn(i,my  ,k) = ( &
                     a*(f(i,my-1,k)+f(i,my  ,k)) + &
                     b*(f(i,my-2,k)+f(i,1   ,k)) + &
                     c*(f(i,my-3,k)+f(i,2   ,k)))
      ydn(i,1   ,k) = ( &
                     a*(f(i,my  ,k)+f(i,1   ,k)) + &
                     b*(f(i,my-1,k)+f(i,2   ,k)) + &
                     c*(f(i,my-2,k)+f(i,3   ,k)))
      ydn(i,2   ,k) = ( &
                     a*(f(i,1   ,k)+f(i,2   ,k)) + &
                     b*(f(i,my  ,k)+f(i,3   ,k)) + &
                     c*(f(i,my-1,k)+f(i,4   ,k)))
      ydn(i,3   ,k) = ( &
                     a*(f(i,2   ,k)+f(i,3   ,k)) + &
                     b*(f(i,1   ,k)+f(i,4   ,k)) + &
                     c*(f(i,my  ,k)+f(i,5   ,k)))
    end do
    do j=3,my-3
     do i=1,mx
      ydn(i,j+1,k) = ( &
                     a*(f(i,j   ,k)+f(i,j+1 ,k)) + &
                     b*(f(i,j-1 ,k)+f(i,j+2 ,k)) + &
                     c*(f(i,j-2 ,k)+f(i,j+3 ,k)))
     end do
    end do
  end do
!
  end function

!***********************************************************************
  function ydn1 (f)
!
!  f is centered on (i,j,k), fifth order stagger
!
  real, dimension(mx,my,mz):: f, ydn1
!-----------------------------------------------------------------------
!
  if (my.eq.1) then
    ydn1 = f
    return
  end if
!
  do k=1,mz
    do i=1,mx
      ydn1(i,1   ,k) = 0.5*(f(i,my  ,k)+f(i,1   ,k))
    end do
    do j=1,my-1
     do i=1,mx
      ydn1(i,j+1,k) = 0.5*(f(i,j   ,k)+f(i,j+1 ,k))
     end do
    end do
  end do
!
  end function

!***********************************************************************
  function zdn (f)
!
!  f is centered on (i,j,k), fifth order stagger
!
  real, dimension(mx,my,mz):: f, zdn
!-----------------------------------------------------------------------
!
  if (mz.eq.1) then
    zdn = f
    return
  end if
!
  do k=1,mz
    km3=mod(k-4+mz,mz)+1
    km2=mod(k-3+mz,mz)+1
    km1=mod(k-2+mz,mz)+1
    kp1=mod(k     ,mz)+1
    kp2=mod(k+1   ,mz)+1
    do j=1,my
      do i=1,mx
        zdn(i,j,k) = ( &
                     a*(f(i,j,k  )+f(i,j,km1)) + &
                     b*(f(i,j,kp1)+f(i,j,km2)) + &
                     c*(f(i,j,kp2)+f(i,j,km3)))
      end do
    end do
  end do
!
  end function

!***********************************************************************
  function zdn1 (f)
!
!  f is centered on (i,j,k), fifth order stagger
!
  real, dimension(mx,my,mz):: f, zdn1
!-----------------------------------------------------------------------
!
  if (mz.eq.1) then
    zdn1 = f
    return
  end if
!
  do k=1,mz
    km1=mod(k-2+mz,mz)+1
    do j=1,my
      do i=1,mx
        zdn1(i,j,k) = 0.5*(f(i,j,k  )+f(i,j,km1))
      end do
    end do
  end do
!
  end function

!***********************************************************************
  function ddxup (f)
  real, dimension(mx,my,mz):: f, ddxup
!-----------------------------------------------------------------------
!
  do k=1,mz
  do j=1,my
    ddxup(mx-2,j,k) = ( &
                   ax3*(f(mx-1,j,k)-f(mx-2,j,k)) &
                 + bx3*(f(mx  ,j,k)-f(mx-3,j,k)) &
                 + cx3*(f(1   ,j,k)-f(mx-4,j,k)))
    ddxup(mx-1,j,k) = ( &
                   ax3*(f(mx  ,j,k)-f(mx-1,j,k)) &
                 + bx3*(f(1   ,j,k)-f(mx-2,j,k)) &
                 + cx3*(f(2   ,j,k)-f(mx-3,j,k)))
    ddxup(mx  ,j,k) = ( &
                   ax3*(f(1   ,j,k)-f(mx  ,j,k)) &
                 + bx3*(f(2   ,j,k)-f(mx-1,j,k)) &
                 + cx3*(f(3   ,j,k)-f(mx-2,j,k)))
    ddxup(1  ,j,k) = ( &
                   ax3*(f(2   ,j,k)-f(1   ,j,k)) &
                 + bx3*(f(3   ,j,k)-f(mx  ,j,k)) &
                 + cx3*(f(4   ,j,k)-f(mx-1,j,k)))
    ddxup(2  ,j,k) = ( &
                   ax3*(f(3   ,j,k)-f(2  ,j,k)) &
                 + bx3*(f(4   ,j,k)-f(1  ,j,k)) &
                 + cx3*(f(5   ,j,k)-f(mx ,j,k)))
  end do
  do j=1,my
    do i=3,mx-3
      ddxup(i  ,j,k) = ( &
                   ax3*(f(i+1 ,j,k)-f(i   ,j,k)) &
                 + bx3*(f(i+2 ,j,k)-f(i-1 ,j,k)) &
                 + cx3*(f(i+3 ,j,k)-f(i-2 ,j,k)))
    end do
  end do
  end do
!
  end function

!***********************************************************************
  function ddxup1 (f)
  real, dimension(mx,my,mz):: f, ddxup1
!-----------------------------------------------------------------------
!
  do k=1,mz
  do j=1,my
    ddxup1(mx  ,j,k) = ax1*(f(1   ,j,k)-f(mx  ,j,k))
  end do
  do j=1,my
    do i=1,mx-1
      ddxup1(i  ,j,k) = ax1*(f(i+1 ,j,k)-f(i   ,j,k))
    end do
  end do
  end do
!
  end function

!***********************************************************************
  function ddyup (f)
  real, dimension(mx,my,mz):: f, ddyup
!-----------------------------------------------------------------------
!
  if (my.eq.1) then
    ddyup = 0.
    return
  end if
!
  do k=1,mz
    do i=1,mx
      ddyup(i,my-2,k) = ( &
                     ay3*(f(i,my-1,k)-f(i,my-2,k)) + &
                     by3*(f(i,my  ,k)-f(i,my-3,k)) + &
                     cy3*(f(i,1   ,k)-f(i,my-4,k)))
      ddyup(i,my-1,k) = ( &
                     ay3*(f(i,my  ,k)-f(i,my-1,k)) + &
                     by3*(f(i,1   ,k)-f(i,my-2,k)) + &
                     cy3*(f(i,2   ,k)-f(i,my-3,k)))
      ddyup(i,my  ,k) = ( &
                     ay3*(f(i,1   ,k)-f(i,my  ,k)) + &
                     by3*(f(i,2   ,k)-f(i,my-1,k)) + &
                     cy3*(f(i,3   ,k)-f(i,my-2,k)))
      ddyup(i,1   ,k) = ( &
                     ay3*(f(i,2   ,k)-f(i,1   ,k)) + &
                     by3*(f(i,3   ,k)-f(i,my  ,k)) + &
                     cy3*(f(i,4   ,k)-f(i,my-1,k)))
      ddyup(i,2   ,k) = ( &
                     ay3*(f(i,3   ,k)-f(i,2   ,k)) + &
                     by3*(f(i,4   ,k)-f(i,1   ,k)) + &
                     cy3*(f(i,5   ,k)-f(i,my  ,k)))
    end do
    do j=3,my-3
     do i=1,mx
      ddyup(i,j  ,k) = ( &
                     ay3*(f(i,j+1 ,k)-f(i,j   ,k)) + &
                     by3*(f(i,j+2 ,k)-f(i,j-1 ,k)) + &
                     cy3*(f(i,j+3 ,k)-f(i,j-2 ,k)))
     end do
    end do
  end do
!
  end function

!***********************************************************************
  function ddyup1 (f)
  real, dimension(mx,my,mz):: f, ddyup1
!-----------------------------------------------------------------------
!
  if (my.eq.1) then
    ddyup1 = 0.
    return
  end if
!
  do k=1,mz
    do i=1,mx
      ddyup1(i,my  ,k) = ay1*(f(i,1   ,k)-f(i,my  ,k))
    end do
    do j=1,my-1
     do i=1,mx
      ddyup1(i,j  ,k) = ay1*(f(i,j+1 ,k)-f(i,j   ,k))
     end do
    end do
  end do
!
  end function

!***********************************************************************
  function ddzup (f)
  real, dimension(mx,my,mz):: f, ddzup
!-----------------------------------------------------------------------
!
  if (mz.eq.1) then
    ddzup = 0.
    return
  end if
!
  do k=1,mz
    km2=mod(k-3+mz,mz)+1
    km1=mod(k-2+mz,mz)+1
    kp1=mod(k     ,mz)+1
    kp2=mod(k+1   ,mz)+1
    kp3=mod(k+2   ,mz)+1
    do j=1,my
      do i=1,mx
        ddzup(i,j,k) = ( &
                       az3*(f(i,j,kp1)-f(i,j,k  )) + &
                       bz3*(f(i,j,kp2)-f(i,j,km1)) + &
                       cz3*(f(i,j,kp3)-f(i,j,km2)))
      end do
    end do
  end do
!
  end function

!***********************************************************************
  function ddzup1 (f)
  real, dimension(mx,my,mz):: f, ddzup1
!-----------------------------------------------------------------------
!
  if (mz.eq.1) then
    ddzup1 = 0.
    return
  end if
!
  do k=1,mz
    kp1=mod(k     ,mz)+1
    do j=1,my
      do i=1,mx
        ddzup1(i,j,k) = az1*(f(i,j,kp1)-f(i,j,k  ))
      end do
    end do
  end do
!
  end function

!***********************************************************************
  function ddxdn (f)
  real, dimension(mx,my,mz):: f, ddxdn
!-----------------------------------------------------------------------
!
  do k=1,mz
   do j=1,my
    ddxdn(mx-1,j,k) = ( &
                   ax3*(f(mx-1,j,k)-f(mx-2,j,k)) &
                 + bx3*(f(mx  ,j,k)-f(mx-3,j,k)) &
                 + cx3*(f(1   ,j,k)-f(mx-4,j,k)))
    ddxdn(mx  ,j,k) = ( &
                   ax3*(f(mx  ,j,k)-f(mx-1,j,k)) &
                 + bx3*(f(1   ,j,k)-f(mx-2,j,k)) &
                 + cx3*(f(2   ,j,k)-f(mx-3,j,k)))
    ddxdn(1   ,j,k) = ( &
                   ax3*(f(1   ,j,k)-f(mx  ,j,k)) &
                 + bx3*(f(2   ,j,k)-f(mx-1,j,k)) &
                 + cx3*(f(3   ,j,k)-f(mx-2,j,k)))
    ddxdn(2  ,j,k) = ( &
                   ax3*(f(2   ,j,k)-f(1   ,j,k)) &
                 + bx3*(f(3   ,j,k)-f(mx  ,j,k)) &
                 + cx3*(f(4   ,j,k)-f(mx-1,j,k)))
    ddxdn(3  ,j,k) = ( &
                   ax3*(f(3   ,j,k)-f(2   ,j,k)) &
                 + bx3*(f(4   ,j,k)-f(1   ,j,k)) &
                 + cx3*(f(5   ,j,k)-f(mx  ,j,k)))
   end do
   do j=1,my
     do i=3,mx-3
      ddxdn(i+1,j,k) = ( &
                   ax3*(f(i+1 ,j,k)-f(i   ,j,k)) &
                 + bx3*(f(i+2 ,j,k)-f(i-1 ,j,k)) &
                 + cx3*(f(i+3 ,j,k)-f(i-2 ,j,k)))
    end do
   end do
  end do
!
  end function

!***********************************************************************
  function ddxdn1 (f)
  real, dimension(mx,my,mz):: f, ddxdn1
!-----------------------------------------------------------------------
!
  do k=1,mz
   do j=1,my
    ddxdn1(1,j,k) = ax1*(f(1,j,k)-f(mx,j,k))
   end do
   do j=1,my
     do i=1,mx-1
      ddxdn1(i+1,j,k) = ax1*(f(i+1,j,k)-f(i,j,k))
    end do
   end do
  end do
!
  end function

!***********************************************************************
  function ddydn (f)
  real, dimension(mx,my,mz):: f, ddydn
!-----------------------------------------------------------------------
!
  if (my.eq.1) then
    ddydn = 0.
    return
  end if
!
  do k=1,mz
    do i=1,mx
      ddydn(i,my-1,k) = ( &
                     ay3*(f(i,my-1,k)-f(i,my-2,k)) + &
                     by3*(f(i,my  ,k)-f(i,my-3,k)) + &
                     cy3*(f(i,1   ,k)-f(i,my-4,k)))
      ddydn(i,my  ,k) = ( &
                     ay3*(f(i,my  ,k)-f(i,my-1,k)) + &
                     by3*(f(i,1   ,k)-f(i,my-2,k)) + &
                     cy3*(f(i,2   ,k)-f(i,my-3,k)))
      ddydn(i,1  ,k) = ( &
                     ay3*(f(i,1   ,k)-f(i,my  ,k)) + &
                     by3*(f(i,2   ,k)-f(i,my-1,k)) + &
                     cy3*(f(i,3   ,k)-f(i,my-2,k)))
      ddydn(i,2   ,k) = ( &
                     ay3*(f(i,2   ,k)-f(i,1   ,k)) + &
                     by3*(f(i,3   ,k)-f(i,my  ,k)) + &
                     cy3*(f(i,4   ,k)-f(i,my-1,k)))
      ddydn(i,3   ,k) = ( &
                     ay3*(f(i,3   ,k)-f(i,2   ,k)) + &
                     by3*(f(i,4   ,k)-f(i,1   ,k)) + &
                     cy3*(f(i,5   ,k)-f(i,my  ,k)))
    end do
    do j=3,my-3
     do i=1,mx
      ddydn(i,j+1 ,k) = ( &
                     ay3*(f(i,j+1 ,k)-f(i,j   ,k)) + &
                     by3*(f(i,j+2 ,k)-f(i,j-1 ,k)) + &
                     cy3*(f(i,j+3 ,k)-f(i,j-2 ,k)))
     end do
    end do
  end do
!
  end function

!***********************************************************************
  function ddydn1 (f)
  real, dimension(mx,my,mz):: f, ddydn1
!-----------------------------------------------------------------------
!
  if (my.eq.1) then
    ddydn1 = 0.
    return
  end if
!
  do k=1,mz
    do i=1,mx
      ddydn1(i,1  ,k) = ay1*(f(i,1   ,k)-f(i,my  ,k))
    end do
    do j=1,my-1
     do i=1,mx
      ddydn1(i,j+1 ,k) = ay1*(f(i,j+1 ,k)-f(i,j   ,k))
     end do
    end do
  end do
!
  end function

!***********************************************************************
  function ddzdn (f)
  real, dimension(mx,my,mz):: f, ddzdn
!-----------------------------------------------------------------------
!
  if (mz.eq.1) then
    ddzdn = 0.
    return
  end if
!
  do k=1,mz
    km3=mod(k-4+mz,mz)+1
    km2=mod(k-3+mz,mz)+1
    km1=mod(k-2+mz,mz)+1
    kp1=mod(k     ,mz)+1
    kp2=mod(k+1   ,mz)+1
    do j=1,my
      do i=1,mx
        ddzdn(i,j,k) = ( &
                       az3*(f(i,j,k  )-f(i,j,km1)) + &
                       bz3*(f(i,j,kp1)-f(i,j,km2)) + &
                       cz3*(f(i,j,kp2)-f(i,j,km3)))
      end do
    end do
  end do
!
  end function

!***********************************************************************
  function ddzdn1 (f)
  real, dimension(mx,my,mz):: f, ddzdn1
!-----------------------------------------------------------------------
!
  if (mz.eq.1) then
    ddzdn1 = 0.
    return
  end if
!
  do k=1,mz
    km1=mod(k-2+mz,mz)+1
    do j=1,my
      do i=1,mx
        ddzdn1(i,j,k) = az1*(f(i,j,k  )-f(i,j,km1))
      end do
    end do
  end do
!
  end function

!***********************************************************************
  function smooth3 (f)
!
!  Three point max
!
  real, dimension(mx,my,mz):: f, smooth3
  real, dimension(mx,my):: tmp
  integer ix,iy,iz,izm1,izp1,iym1,iyp1
!-----------------------------------------------------------------------
!
  if (mz.eq.1) then
    smooth3 = f
  else
    do iz=1,mz
      izm1 = iz-1 ; if (iz.eq.1 ) izm1=mz
      izp1 = iz+1 ; if (iz.eq.mz) izp1=1
      do ix=1,mx
      do iy=1,my
        smooth3(ix,iy,iz) = 0.3333333*(f(ix,iy,izm1)+f(ix,iy,iz)+f(ix,iy,izp1))
      end do
      end do
    end do
  end if
  do iz=1,mz
    if (my.eq.1) then
      tmp = smooth3(:,:,iz)
    else
      do iy=1,my
        iym1 = iy-1 ; if (iy.eq.1 ) iym1=my
        iyp1 = iy+1 ; if (iy.eq.my) iyp1=1
        do ix=1,mx
          tmp(ix,iy) = 0.3333333*(smooth3(ix,iym1,iz)+smooth3(ix,iy,iz)+smooth3(ix,iyp1,iz))
        end do
      end do
    end if
    do iy=1,my
      smooth3( 1,iy,iz) = 0.3333333*(tmp(mx  ,iy)+tmp(1 ,iy)+tmp(2,iy))
      smooth3(mx,iy,iz) = 0.3333333*(tmp(mx-1,iy)+tmp(mx,iy)+tmp(1,iy))
      do ix=2,mx-1
        smooth3(ix,iy,iz) = 0.3333333*(tmp(ix-1,iy)+tmp(ix,iy)+tmp(ix+1,iy))
      enddo
    enddo
  enddo
  end function

!***********************************************************************
  function max3 (f)
!
!  Three point max
!
  real, dimension(mx,my,mz):: f, max3
  real, dimension(mx,my):: tmp
  integer ix,iy,iz,izm1,izp1,iym1,iyp1
!-----------------------------------------------------------------------
!
  if (mz.eq.1) then
    max3 = max(f,0.)
  else
    do iz=1,mz
      izm1 = iz-1 ; if (iz.eq.1 ) izm1=mz
      izp1 = iz+1 ; if (iz.eq.mz) izp1=1
      do ix=1,mx
      do iy=1,my
        max3(ix,iy,iz) = max(f(ix,iy,izm1),f(ix,iy,iz),f(ix,iy,izp1),0.)
      end do
      end do
    end do
  end if
  do iz=1,mz
    if (my.eq.1) then
      tmp = max3(:,:,iz)
    else
      do iy=1,my
        iym1 = iy-1 ; if (iy.eq.1 ) iym1=my
        iyp1 = iy+1 ; if (iy.eq.my) iyp1=1
        do ix=1,mx
          tmp(ix,iy) = max(max3(ix,iym1,iz),max3(ix,iy,iz),max3(ix,iyp1,iz))
        end do
      end do
    end if
    do iy=1,my
      max3( 1,iy,iz) = max(tmp(mx  ,iy),tmp(1 ,iy),tmp(2,iy))
      max3(mx,iy,iz) = max(tmp(mx-1,iy),tmp(mx,iy),tmp(1,iy))
      do ix=2,mx-1
        max3(ix,iy,iz) = max(tmp(ix-1,iy),tmp(ix,iy),tmp(ix+1,iy))
      enddo
    enddo
  enddo
  end function

!***********************************************************************
  function max5 (f)
!
!  Three point max
!
  real, dimension(mx,my,mz):: f, max5
  real, dimension(mx,my):: tmp
  integer ix,iy,iz,izm2,izm1,izp1,izp2,iym2,iym1,iyp1,iyp2
!-----------------------------------------------------------------------
!
  if (mz.eq.1) then
    max5 = max(f,0.)
  else
    do iz=1,mz
      izm2 = iz-2 ; if (iz.eq.1 ) izm2=mz-1 ; if (iz.eq.2) izm2=mz
      izm1 = iz-1 ; if (iz.eq.1 ) izm1=mz
      izp1 = iz+1 ; if (iz.eq.mz) izp1=1
      izp2 = iz+2 ; if (iz.eq.mz) izp2=2    ; if (iz.eq.mz-1) izp2=1
      do ix=1,mx
      do iy=1,my
        max5(ix,iy,iz) = max(f(ix,iy,izm2), &
                             f(ix,iy,izm1), &
                             f(ix,iy,iz  ), &
                             f(ix,iy,izp1), &
                             f(ix,iy,izp2),0.)
      end do
      end do
    end do
  end if
  do iz=1,mz
    if (my.eq.1) then
      tmp = max5(:,:,iz)
    else
      do iy=1,my
        iym2 = iy-2 ; if (iy.eq.1 ) iym2=my-1 ; if (iy.eq.2) iym2=my
        iym1 = iy-1 ; if (iy.eq.1 ) iym1=my
        iyp1 = iy+1 ; if (iy.eq.my) iyp1=1
        iyp2 = iy+2 ; if (iy.eq.my) iyp2=2    ; if (iy.eq.my-1) iyp2=1
        do ix=1,mx
          tmp(ix,iy) = max(max5(ix,iym2,iz), &
                           max5(ix,iym1,iz), &
                           max5(ix,iy  ,iz), &
                           max5(ix,iyp1,iz), &
                           max5(ix,iyp2,iz))
        end do
      end do
    end if
    do iy=1,my
      max5( 2  ,iy,iz) = max(tmp(mx  ,iy),tmp(1   ,iy),tmp(2   ,iy),tmp(3 ,iy),tmp(4,iy))
      max5( 1  ,iy,iz) = max(tmp(mx-1,iy),tmp(mx  ,iy),tmp(1   ,iy),tmp(2 ,iy),tmp(3,iy))
      max5(mx  ,iy,iz) = max(tmp(mx-2,iy),tmp(mx-1,iy),tmp(mx  ,iy),tmp(1 ,iy),tmp(2,iy))
      max5(mx-1,iy,iz) = max(tmp(mx-3,iy),tmp(mx-2,iy),tmp(mx-1,iy),tmp(mx,iy),tmp(1,iy))
      do ix=3,mx-2
        max5(ix,iy,iz) = max(tmp(ix-2,iy), &
                             tmp(ix-1,iy), &
                             tmp(ix  ,iy), &
                             tmp(ix+1,iy), &
                             tmp(ix+2,iy))
      enddo
    enddo
  enddo
  end function

!***********************************************************************
  function smooth3w (f)
!
!  Three point max
!
  real, dimension(mx,my,mz):: f, smooth3w
  real, dimension(mx,my):: tmp
  integer ix,iy,iz,izm1,izp1,iym1,iyp1
!-----------------------------------------------------------------------
!
  if (mz.eq.1) then
    smooth3w = f
  else
    do iz=1,mz
      izm1 = iz-1 ; if (iz.eq.1 ) izm1=mz
      izp1 = iz+1 ; if (iz.eq.mz) izp1=1
      do ix=1,mx
      do iy=1,my
        smooth3w(ix,iy,iz) = 0.05*(f(ix,iy,izm1)+18*f(ix,iy,iz)+f(ix,iy,izp1))
      end do
      end do
    end do
  end if
  do iz=1,mz
    if (my.eq.1) then
      tmp = smooth3w(:,:,iz)
    else
      do iy=1,my
        iym1 = iy-1 ; if (iy.eq.1 ) iym1=my
        iyp1 = iy+1 ; if (iy.eq.my) iyp1=1
        do ix=1,mx
          tmp(ix,iy) = 0.05*(smooth3w(ix,iym1,iz)+18*smooth3w(ix,iy,iz)+smooth3w(ix,iyp1,iz))
        end do
      end do
    end if
    do iy=1,my
      smooth3w( 1,iy,iz) = 0.05*tmp(mx  ,iy)+0.9*tmp(1 ,iy)+0.05*tmp(2,iy)
      smooth3w(mx,iy,iz) = 0.05*tmp(mx-1,iy)+0.9*tmp(mx,iy)+0.05*tmp(1,iy)
      do ix=2,mx-1
        smooth3w(ix,iy,iz) = 0.05*tmp(ix-1,iy)+0.9*tmp(ix,iy)+0.05*tmp(ix+1,iy)
      enddo
    enddo
  enddo
  end function
!***********************************************************************
END MODULE

!***********************************************************************
SUBROUTINE init_stagger
  USE stagger_m
  USE grid_m
  implicit none
  logical test
  logical, save:: first=.true.
  integer ix, iy, iz
  real kx, ky, kz, fmaxval
  real, allocatable, dimension(:,:,:):: u
  namelist /stagger/ order, test, do_stretch
  character(len=80):: id='$Id: stagger.f90,v 1.7 2009/11/09 23:29:27 aake Exp $'
!.......................................................................
  call print_id (id)

  if (.not.first) return
  first = .false.

  order = 6
  test = .false.
  do_stretch = .false.
  rewind(stdin); read(stdin,stagger)
  if (stdout>0) then; write(*,*)hl; write (*,stagger); endif
  if (master) write(2,stagger)

  if (order == 6) then
    c = 3./256.
    b = -25./256.
    a = .5-b-c

    cx3 = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3.))
    bx3 = (-1.-120.*cx3)/24.
    ax3 = (1.-3.*bx3-5.*cx3)/dx
    bx3 = bx3/dx
    cx3 = cx3/dx

    cy3 = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3.))
    by3 = (-1.-120.*cy3)/24.
    ay3 = (1.-3.*by3-5.*cy3)/dy
    by3 = by3/dy
    cy3 = cy3/dy

    cz3 = (-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3.))
    bz3 = (-1.-120.*cz3)/24.
    az3 = (1.-3.*bz3-5.*cz3)/dz
    bz3 = bz3/dz
    cz3 = cz3/dz

  else if (order == 4) then
    c = 0.
    b = -1./16.
    a = .5-b-c

    cx3 = 0.
    bx3 = -1./24.
    ax3 = (1.- 3.*bx3)/dx
    bx3 = bx3/dx

    cy3 = 0.
    by3 = -1./24.
    ay3 = (1. - 3.*by3)/dy
    by3 = by3/dy

    cz3 = 0.
    bz3 = -1./24.
    az3 = (1. - 3.*bz3)/dz

  else if (order == 2) then
    c = 0.
    b = 0.
    a = .5

    cx3 = 0.
    bx3 = 0.
    ax3 = 1./dx

    cy3 = 0.
    by3 = 0.
    ay3 = 1./dy

    cz3 = 0.
    bz3 = 0.
    az3 = 1./dz

  else
     write(*,*)  'you can only choose the order of stagger to be 2, 4, or 6'
     stop

  endif

  ax1 = 1./dx
  ay1 = 1./dy
  az1 = 1./dz

  if (.not. test) return

  allocate (u(mx,my,mz))

  kx = 2.*pi/sx; ky = 2.*pi/sy; kz = 2.*pi/sz
  do iz=1,mz; do iy=1,my; do ix=1,mx
    u(ix,iy,iz)  =    sin(kx*xm(ix))*sin(ky*ym(iy))*sin(kz*zm(iz))
    bx(ix,iy,iz) = kx*cos(kx*xm(ix))*sin(ky*ym(iy))*sin(kz*zm(iz))
    by(ix,iy,iz) = ky*sin(kx*xm(ix))*cos(ky*ym(iy))*sin(kz*zm(iz))
    bz(ix,iy,iz) = kz*sin(kx*xm(ix))*sin(ky*ym(iy))*cos(kz*zm(iz))
  end do; end do; end do

  ux = xup(ddxdn(u)) ; print*,' x',mpi_rank,fmaxval(abs(ux-bx))
  uy = yup(ddydn(u)) ; print*,' y',mpi_rank,fmaxval(abs(uy-by))
  uz = zup(ddzdn(u)) ; print*,' z',mpi_rank,fmaxval(abs(uz-bz))
  ux = xdn(ddxup(u)) ; print*,' x',mpi_rank,fmaxval(abs(ux-bx))
  uy = ydn(ddyup(u)) ; print*,' y',mpi_rank,fmaxval(abs(uy-by))
  uz = zdn(ddzup(u)) ; print*,' z',mpi_rank,fmaxval(abs(uz-bz))

  ux = xup1(ddxdn(u)); print*,'x1',mpi_rank,fmaxval(abs(ux-bx))
  uy = yup1(ddydn(u)); print*,'y1',mpi_rank,fmaxval(abs(uy-by))
  uz = zup1(ddzdn(u)); print*,'z1',mpi_rank,fmaxval(abs(uz-bz))
  ux = xdn1(ddxup(u)); print*,'x1',mpi_rank,fmaxval(abs(ux-bx))
  uy = ydn1(ddyup(u)); print*,'y1',mpi_rank,fmaxval(abs(uy-by))
  uz = zdn1(ddzup(u)); print*,'z1',mpi_rank,fmaxval(abs(uz-bz))

  ux = xup(ddxdn1(u)); print*,'x2',mpi_rank,fmaxval(abs(ux-bx))
  uy = yup(ddydn1(u)); print*,'y2',mpi_rank,fmaxval(abs(uy-by))
  uz = zup(ddzdn1(u)); print*,'z2',mpi_rank,fmaxval(abs(uz-bz))
  ux = xdn(ddxup1(u)); print*,'x2',mpi_rank,fmaxval(abs(ux-bx))
  uy = ydn(ddyup1(u)); print*,'y2',mpi_rank,fmaxval(abs(uy-by))
  uz = zdn(ddzup1(u)); print*,'z2',mpi_rank,fmaxval(abs(uz-bz))

  deallocate(u)
END SUBROUTINE
!*******************************************************************************
