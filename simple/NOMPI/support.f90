! $Id: support.f90,v 1.12 2009/10/07 10:07:42 aake Exp $
!*******************************************************************************
SUBROUTINE init_mpi
  USE grid_m
  implicit none
  character(len=80):: id='$Id: support.f90,v 1.12 2009/10/07 10:07:42 aake Exp $'
!...............................................................................
  call print_id (id)
  mpi_rank = 0
  master = .true.
  lb = 1                                                                        ! change in start.f90 if non-periodic
END

!*******************************************************************************
SUBROUTINE end_mpi
END

!*******************************************************************************
SUBROUTINE barrier_mpi
END

!*******************************************************************************
SUBROUTINE init_mpi_grid
  USE grid_m
  implicit none
!...............................................................................
  xmin_mpi = xmin; ymin_mpi=ymin; zmin_mpi=zmin
  xmax_mpi = xmax; ymax_mpi=ymax; zmax_mpi=zmax
  mpi_x = 0
  mpi_y = 0
  mpi_z = 0
  mpi_mx = 1
  mpi_my = 1
  mpi_mz = 1
END

!*******************************************************************************
REAL FUNCTION fmaxval (f)
  USE grid_m
  implicit none
!...............................................................................
  real f(mx,my,mz)
  fmaxval = maxval(f(:,:,lb:ub))
END FUNCTION

!*******************************************************************************
REAL FUNCTION fminval (f)
  USE grid_m
  implicit none
!...............................................................................
  real f(mx,my,mz)
  fminval = minval(f)
END FUNCTION

!*******************************************************************************
REAL FUNCTION faver (f)
  USE grid_m
  implicit none
  real f(mx,my,mz)
  real(kind=8):: ysum, zsum
  integer iy, iz
!...............................................................................
  zsum = 0.
  do iz=1,mz
    ysum = 0.
    do iy=1,my
      ysum = ysum+sum(f(:,iy,iz))
    end do
    zsum = zsum+ysum
  end do
  faver = zsum/(real(mx)*real(my)*real(mz))
END FUNCTION

!*******************************************************************************
REAL FUNCTION fsum (f)
  implicit none
  real f
  fsum = f
END FUNCTION

!*******************************************************************************
SUBROUTINE file_open (file, mode)
  USE grid_m
  implicit none
  character(len=*) file, mode
!
  open (1,file=file,access='direct',form='unformatted',status=mode,recl=recl)
END

!*******************************************************************************
SUBROUTINE file_openw (file)
  implicit none
  character(len=*) file
!
  call file_open (file, 'unknown')
END

!*******************************************************************************
SUBROUTINE file_openr (file)
  implicit none
  character(len=*) file
!
  call file_open (file, 'old')
END

!*******************************************************************************
SUBROUTINE file_write (rec, f)
  USE grid_m
  implicit none
  real, dimension(mx,my,mz):: f
  integer rec
!
  write (1,rec=rec+1) f
END

!*******************************************************************************
SUBROUTINE file_read (rec, f)
  USE grid_m
  implicit none
  real, dimension(mx,my,mz):: f
  integer rec
!
  read (1,rec=rec+1) f
END

!*******************************************************************************
SUBROUTINE file_close ()
  close (1)
END

!*******************************************************************************
FUNCTION flag(file)                                                             ! detect 'dir/file.flag file
  USE grid_m, only: master, dir
  implicit none
  character(len=*) file
  logical flag
!-------------------------------------------------------------------------------
  inquire (file=trim(dir)//trim(file)//'.flag',exist=flag)
  if (flag) then
    open (1,file=trim(dir)//trim(file)//'.flag',status='old')
    close (1,status='delete')
  end if
END
