! $Id: mhd_advect.f90,v 1.15 2011/08/23 23:07:44 aake Exp $
!*******************************************************************************
MODULE mhd_m
  real    :: nu0, nu1, nu2, nu3, nur, nue, nuB, nuS
END MODULE

!*******************************************************************************
SUBROUTINE init_mhd
  USE mhd_m
  USE grid_m
  implicit none
  namelist /mhd/ nu0, nu1, nu2, nu3, nur, nue, nuB, nuS, gamma
!...............................................................................
  nu0=0.007; nu1=0.050; nu2=1.2; nu3=0.; nur=.0; nue=1.; nuB=1.0; nuS=1.
  rewind(stdin); read (stdin,mhd)
  if (stdout > 0) then; write(*,*) hl; write(*,mhd); end if
  if (master) write(2,mhd)
END SUBROUTINE

!*******************************************************************************
SUBROUTINE pde
  USE grid_m, rho=>r, drhodt=>drdt
  USE stagger_m
  USE mhd_m
  USE time_m, only: Cu, Cv, Cn
  implicit none
  integer nchunk, maxchunk
  logical,save:: first_time=.true.
  real fmaxval, gb
  real, allocatable, dimension(:,:,:):: lnr,xdnrho,ydnrho,zdnrho,U,pp,cA, &     ! temporary arrays
    Sxx,Syy,Szz,Sxy,Syz,Szx,Txx,Tyy,Tzz,Txy,Tyz,Tzx,qx,qy,qz,Vx,Vy,Vz, &
    ee,divU,nu,eta,jx,jy,jz,Ex,Ey,Ez,lnu,Uxc,Uyc,Uzc,Vxc,Vyc,Vzc
  character(len=80):: id='$Id: mhd_advect.f90,v 1.15 2011/08/23 23:07:44 aake Exp $'
!...............................................................................
  call print_id (id)

!-------------------------------------------------------------------------------
!  Density interpolations
!-------------------------------------------------------------------------------
  nchunk=16; maxchunk=16; maxchunk=max(nchunk,maxchunk)
  allocate(lnr(mx,my,mz),xdnrho(mx,my,mz),ydnrho(mx,my,mz),zdnrho(mx,my,mz))
  nchunk=nchunk+4; maxchunk=max(nchunk,maxchunk)
    call density_boundaries (rho)
  lnr = alog(rho)
  xdnrho = exp(xdn(lnr))
  ydnrho = exp(ydn(lnr))
  zdnrho = exp(zdn(lnr))

!-------------------------------------------------------------------------------
!  Velocities
!-------------------------------------------------------------------------------
  allocate(U(mx,my,mz),Uxc(mx,my,mz),Uyc(mx,my,mz),Uzc(mx,my,mz))
  nchunk=nchunk+4; maxchunk=max(nchunk,maxchunk)
  Ux = px/xdnrho
  Uy = py/ydnrho
  Uz = pz/zdnrho
    call velocity_boundaries (px, py, pz, xdnrho, ydnrho, zdnrho,Ux, Uy, Uz)
  Uxc = xup(Ux)
  Uyc = yup(Uy)
  Uzc = zup(Uz)
  U  = sqrt(Uxc**2 + Uyc**2  + Uzc**2)
  call forcing (xdnrho, ydnrho, zdnrho)
  
!-------------------------------------------------------------------------------
!  Equation of state 
!-------------------------------------------------------------------------------
  allocate(pp(mx,my,mz),cA(mx,my,mz))
  nchunk=nchunk+2; maxchunk=max(nchunk,maxchunk)
    call energy_boundaries (e, rho, Ux, Uy, Uz)
  if (gamma==1) then
    pp = rho
  else
    pp = (gamma-1.)*e
  end if
    call magnetic_field_boundaries (Bx, By, Bz, Ux, Uy, Uz)
  cA = sqrt((xup(Bx)**2 + yup(By)**2  + zup(Bz)**2 + pp*gamma)/rho)

!-------------------------------------------------------------------------------
!  Numerical diffusion and resistivity, Richtmyer & Morton type
!-------------------------------------------------------------------------------
  allocate(divU(mx,my,mz),nu(mx,my,mz),eta(mx,my,mz))
  allocate(Sxx(mx,my,mz),Syy(mx,my,mz),Szz(mx,my,mz))
  nchunk=nchunk+6; maxchunk=max(nchunk,maxchunk)
  Sxx = ddxup(Ux)
  Syy = ddyup(Uy)
  Szz = ddzup(Uz)
  divU = Sxx+Syy+Szz
  nu  = nu0*dsmax + nu1*dsmax*(U+cA) + nu2*dsmax**2*smooth3(max3(max(-divU,0.0)))
  if (nu3 > 0) nu = nu + &
   nu3*dsmax**3*(xup1(ddxdn(lnr)**2)+yup1(ddydn(lnr)**2)+zup1(ddzdn(lnr)**2))
  eta = nuB*nu

!-------------------------------------------------------------------------------
!  Courant numbers based on wave speed, viscous and resistive diffusion
!-------------------------------------------------------------------------------
  Cu = fmaxval(U+cA)
  Cv = 6.2*3.*fmaxval(nu )/2.*0.4/0.6
  Cn = 6.2*3.*fmaxval(eta)/2.*0.4/0.6
  deallocate(U,cA)
  nchunk=nchunk-2; maxchunk=max(nchunk,maxchunk)

!-------------------------------------------------------------------------------
!  Mass conservation, cell centered
!-------------------------------------------------------------------------------
  allocate(Vx(mx,my,mz),Vy(mx,my,mz),Vz(mx,my,mz))
  allocate(Vxc(mx,my,mz),Vyc(mx,my,mz),Vzc(mx,my,mz))
  allocate(lnu(mx,my,mz))
  nchunk=nchunk+10; maxchunk=max(nchunk,maxchunk)
  lnu = alog(nu)
  if (nur > 0.) then
    allocate(qx(mx,my,mz),qy(mx,my,mz),qz(mx,my,mz))
    qx = px - nur*exp(xdn(lnu))*ddxdn(rho); Vx = qx/xdnrho; Vxc = xup(Vx)
    qy = py - nur*exp(ydn(lnu))*ddydn(rho); Vy = qy/ydnrho; Vyc = yup(Vy)
    qz = pz - nur*exp(zdn(lnu))*ddzdn(rho); Vz = qz/zdnrho; Vzc = zup(Vz)
    drhodt = drhodt - ddxup(qx) - ddyup(qy) - ddzup(qz)
    deallocate(qx,qy,qz)
  else
    Vx = Ux; Vxc = Uxc
    Vy = Uy; Vyc = Uyc
    Vz = Uz; Vzc = Uzc
    drhodt = drhodt - ddxup(px) - ddyup(py) - ddzup(pz)
  end if
  deallocate(xdnrho,ydnrho,zdnrho)
  nchunk=nchunk-6; maxchunk=max(nchunk,maxchunk)

!-------------------------------------------------------------------------------
!  Viscous stress, diagonal and off-diagonal elements
!-------------------------------------------------------------------------------
  allocate(Sxy(mx,my,mz),Syz(mx,my,mz),Szx(mx,my,mz))
  allocate(Txx(mx,my,mz),Tyy(mx,my,mz),Tzz(mx,my,mz))
  allocate(Txy(mx,my,mz),Tyz(mx,my,mz),Tzx(mx,my,mz))
  nchunk=nchunk+9; maxchunk=max(nchunk,maxchunk)
  Txx = - nuS*nu*rho*Sxx - (1.-nuS)*nu*rho*divU
  Tyy = - nuS*nu*rho*Syy - (1.-nuS)*nu*rho*divU
  Tzz = - nuS*nu*rho*Szz - (1.-nuS)*nu*rho*divU
  Sxy = 0.5*(ddxdn(Uy)+ddydn(Ux))
  Syz = 0.5*(ddydn(Uz)+ddzdn(Uy))
  Szx = 0.5*(ddzdn(Ux)+ddxdn(Uz))
  lnu = lnu + lnr
  Txy = - nuS*exp(xdn(ydn(lnu)))*Sxy
  Tyz = - nuS*exp(ydn(zdn(lnu)))*Syz
  Tzx = - nuS*exp(zdn(xdn(lnu)))*Szx
  deallocate(nu)
  nchunk=nchunk-1; maxchunk=max(nchunk,maxchunk)
 
!-------------------------------------------------------------------------------
!  Electric current j = curl(B), edge centered
!-------------------------------------------------------------------------------
  allocate(jx(mx,my,mz),jy(mx,my,mz),jz(mx,my,mz))
  nchunk=nchunk+3; maxchunk=max(nchunk,maxchunk)
  jx = ddydn(Bz) - ddzdn(By)
  jy = ddzdn(Bx) - ddxdn(Bz)
  jz = ddxdn(By) - ddydn(Bx)

!-------------------------------------------------------------------------------
!  Lorentz force
!-------------------------------------------------------------------------------
  dpxdt = dpxdt + zup(jy*xdn(Bz)) - yup(jz*xdn(By))
  dpydt = dpydt + xup(jz*ydn(Bx)) - zup(jx*ydn(Bz))
  dpzdt = dpzdt + yup(jx*zdn(By)) - xup(jy*zdn(Bx))

!-------------------------------------------------------------------------------
!  Electric field   E = eta j - (u x B), edge centered
!-------------------------------------------------------------------------------
  allocate(Ex(mx,my,mz),Ey(mx,my,mz),Ez(mx,my,mz))
  nchunk=nchunk+3; maxchunk=max(nchunk,maxchunk)
  Ex = ydn(zdn(eta))*jx
  Ey = zdn(xdn(eta))*jy
  Ez = xdn(ydn(eta))*jz
  if (gamma .ne. 1.) then
    dedt = dedt + zup(yup(jx*Ex) + xup(jy*Ey)) + xup(yup(jz*Ez))
  end if
  Ex = Ex - zdn(Vy)*ydn(Bz) + ydn(Vz)*zdn(By)
  Ey = Ey - xdn(Vz)*zdn(Bx) + zdn(Vx)*xdn(Bz)
  Ez = Ez - ydn(Vx)*xdn(By) + xdn(Vy)*ydn(Bx)
  deallocate(jx,jy,jz,eta)
  nchunk=nchunk-4; maxchunk=max(nchunk,maxchunk)

!-------------------------------------------------------------------------------
!  Magnetic field's time derivative, dBdt = - curl(E), face centered
!-------------------------------------------------------------------------------
    call electric_field_boundaries (Ex, Ey, Ez)
  dBxdt = dBxdt + ddzup(Ey) - ddyup(Ez)
  dBydt = dBydt + ddxup(Ez) - ddzup(Ex)
  dBzdt = dBzdt + ddyup(Ex) - ddxup(Ey)
  deallocate(Ex,Ey,Ez)
  nchunk=nchunk-3; maxchunk=max(nchunk,maxchunk)

!-------------------------------------------------------------------------------
!  Energy eq, with P*dV work, difffusion, kinetic and Joule dissipation
!-------------------------------------------------------------------------------
  if (gamma .ne. 1) then
    allocate(ee(mx,my,mz))
    nchunk=nchunk+1; maxchunk=max(nchunk,maxchunk)
    ee = e/rho
    dedt = dedt - pp*divU &
         - ddxup (xdn(ee)*px - nue*exp(xdn(lnu))*ddxdn(ee)) &
         - ddyup (ydn(ee)*py - nue*exp(ydn(lnu))*ddydn(ee)) &
         - ddzup (zdn(ee)*pz - nue*exp(zdn(lnu))*ddzdn(ee)) &
         - (Txx*Sxx + Tyy*Syy + Tzz*Szz) &
         - 2.*(xup1(yup1(Txy*Sxy) + zup1(Tzx*Szx)) + yup1(zup1(Tyz*Syz)))
    deallocate(ee)
    nchunk=nchunk-1; maxchunk=max(nchunk,maxchunk)
  end if
  deallocate(Sxx,Syy,Szz,Sxy,Syz,Szx,lnu,divU)
  nchunk=nchunk-8; maxchunk=max(nchunk,maxchunk)
!-------------------------------------------------------------------------------
!  Pressure and Reynolds stress
!-------------------------------------------------------------------------------
  Txx = Txx + pp + rho*Uxc*Vxc
  Tyy = Tyy + pp + rho*Uyc*Vyc
  Tzz = Tzz + pp + rho*Uzc*Vzc
  Txy = Txy + exp(xdn(ydn(lnr)))*ydn(Ux)*xdn(Vy)
  Tyz = Tyz + exp(ydn(zdn(lnr)))*zdn(Uy)*ydn(Vz)
  Tzx = Tzx + exp(xdn(zdn(lnr)))*zdn(Ux)*xdn(Vz)
  deallocate(pp,Uxc,Uyc,Uzc,Vxc,Vyc,Vzc,Vx,Vy,Vz,lnr)
  nchunk=nchunk-11; maxchunk=max(nchunk,maxchunk)

!-------------------------------------------------------------------------------
!  Equations of motion, face centred
!-------------------------------------------------------------------------------
  dpxdt = dpxdt - ddxdn(Txx) - ddyup(Txy) - ddzup(Tzx)
  dpydt = dpydt - ddydn(Tyy) - ddzup(Tyz) - ddxup(Txy)
  dpzdt = dpzdt - ddzdn(Tzz) - ddxup(Tzx) - ddyup(Tyz)
  deallocate(Txx,Tyy,Tzz,Txy,Tyz,Tzx)
  nchunk=nchunk-6; maxchunk=max(nchunk,maxchunk)

  call ddt_boundaries (drhodt, dpxdt, dpydt, dpzdt, dedt, dBxdt, dBydt, dBzdt)

  if (master .and. first_time) then
    gb = maxchunk*real(mx)*real(my)*real(mz)*4e-9
    print '(1x,a,f7.3,a)','Estimated memory per process = ',gb,' GB'
    gb = gb*mpi_size
    print '(1x,a,f7.3,a)','Estimated total memory       = ',gb,' GB'
    gb = 8*mpi_size*real(mx)*real(my)*real(mz)*4e-9
    print '(1x,a,f7.3,a)','Estimated snapshot size      = ',gb,' GB'
    first_time = .false.
  end if
END
