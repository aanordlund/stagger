! $Id: main.f90,v 1.10 2009/10/07 10:20:27 aake Exp $
!*******************************************************************************
PROGRAM simple
!
!  Main program for the simple MHD staggered mesh code
!
!-------------------------------------------------------------------------------
  USE time_m                                                                    ! time step parameters
  implicit none                                                                 ! keep us straight
  real wc0, wc1, wallclock                                                      ! wall clock times
  logical flag                                                                  ! detects 'stop.flag' file
!...............................................................................
  call init_mpi                                                                 ! initialize MPI
  call init_io                                                                  ! initialize input/output
  call init_start                                                               ! initialize starting values
  call init_mhd                                                                 ! initialize MHD routine
  call init_force                                                               ! initialize forcing
  call init_timestep                                                            ! initialize time stepping
  if (master) print*,hl                                                         ! horizontal line

  wc0 = wallclock()                                                             ! initial wall clock time
  it = 0                                                                        ! reset counter
  do while (t < tend+1.5*dt .and. .not.flag('stop'))                            ! loop beyond tend or file stop.flag touched
    call write_snapshot                                                         ! write snapshot?
    call timestep                                                               ! time step
    t  = t + dt                                                                 ! update the time
    it = it + 1                                                                 ! count timesteps
  end do
  wc1 = wallclock()                                                             ! final wall clock time

  call deallocate_arrays                                                        ! avoids g95 complaints
  if (master) print '(a,2(f5.2,a))', &                                          ! suummary line
    'done:', (wc1-wc0)/3600., ' hours,', (wc1-wc0)*1e6 &                        ! wall clock hours
    /(real(it)*real(mx)*real(my)*real(mz)), &                                   ! CPU-microsec per zone-update
    ' CPU-musec/zone-update'
  call shut_down ('normal')
END PROGRAM

!*******************************************************************************
SUBROUTINE shut_down (text)
  USE grid_m, only: master
  implicit none
  character(len=*) text
!...............................................................................
  if (master) print*,'shut down: '//text
  call end_io                                                                   ! close and clean up
  call end_mpi                                                                  ! close down MPI
  stop
END SUBROUTINE
