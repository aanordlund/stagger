#! /bin/csh
#PBS -N ifc_test -l ncpus=4,cput=1:00:00,walltime=1:00:00,mem=4gb,arch=linux-ia64 -q parlinux -S /bin/csh -j oe

cd $PBS_O_WORKDIR
set verbose

echo 250,250,250 > in

setenv OMP_NUM_THREADS 1
dplace -x6 ifc7_omp_test.x < in
dplace -x6 ifc8_omp_test.x < in

setenv OMP_NUM_THREADS 4
dplace -x6 ifc7_omp_test.x < in
dplace -x6 ifc8_omp_test.x < in

dplace -x6 ifc7_test.x < in
dplace -x6 ifc8_test.x < in

echo 250,250,2500 > in

setenv OMP_NUM_THREADS 1
dplace -x6 ifc7_omp_test.x < in
dplace -x6 ifc8_omp_test.x < in

setenv OMP_NUM_THREADS 4
dplace -x6 ifc7_omp_test.x < in
dplace -x6 ifc8_omp_test.x < in

dplace -x6 ifc7_test.x < in
dplace -x6 ifc8_test.x < in
