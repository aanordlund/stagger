! $Id: flush_iostat.f90,v 1.2 2003/05/05 08:06:02 aake Exp $

! sander:stagger-code/bugs> f90 -version
! MIPSpro Compilers: Version 7.3.1.3m
!
! Two bugs are demontrated by the program below:
!
! 1) Flushing a missing unit should produce a non-zero iostat
! 2) Flushing 102 should be OK -- previously, and according to "man flush"
!    unit 102 is stderr.
!
! No, actually a more careful reading of "man flush" shows that on IRIX
! systems the iostat argument is mandatory.  So, only the first point is
! actually a bug (which may be safely ignored).  /EOS

  print *,'flush with iostat'
  do i=1,10
    call flush(101,io1)
    call flush(102,io2)
    print *,i,io1,io2
  end do

  print *,'flush missing unit with iostat'
  do i=1,10
    call flush(101,io1)
    call flush(111,io2)
    print *,i,io1,io2
  end do

  print *,'flush 101 without iostat'
  do i=1,10
    call flush(101)
    print *,i
  end do

  print *,'flush 101+102 without iostat'
  do i=1,10
    call flush(101)
    call flush(102)
    print *,i
  end do

  end
