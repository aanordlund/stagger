# $Id: mpi.csh,v 1.1 2014/02/24 13:11:12 bob Exp $

# -----------------------------------------------------------------------------
# Directory and environment setup
# -----------------------------------------------------------------------------
set echo
cd $PBS_O_WORKDIR			# working directory
umask 026				# allow group to read
unlimit stack				# max stack size
limit core 0				# no core dump
limit
echo $NCPUS
#setenv KMP_STACKSIZE 10G		# slave CPU stack size
#setenv KMP_LIBRARY throughput		# default, for shared environment
#setenv KMP_BLOCKTIME 0.3s		# near optimum, for throughput mode
#setenv KMP_LIBRARY turnaround		# for minimum wall clock time batch
#setenv F_UFMTENDIAN big  		# big endian byte order (SLOW!!)
#setenv F_UFMTENDIAN big:10,12		# big endian old model(10) and table(12)
#setenv F_UFMTENDIAN big:11   		# big endian new model(11)
setenv MPI_BUFS_PER_HOST 96
setenv MPI_BUFS_PER_PROC 64
setenv MPI_DISPLAY_SETTINGS

module load ~dpbarker/public/supersmith/modules/logstat.1.1

printenv
module list

# -----------------------------------------------------------------------------
# Execution.  This assumes that the $EXPERIMENT.x file has been installed ("make intall")
# to ~/bin/$MACHTYPE (to allow running under multiple architechtures).  The executable is
# copied to the run dir, to keep it with the data, and to allow the original file to be
# updated without killing running jobs.  
# -----------------------------------------------------------------------------
set x = mconv_ifort_opt_mpi.x		# edit this for specific case 
\rm $x
ln -s ~/bin/$x .			# link local copy to ~/bin
set c = "$1"				# number of cpus 
set name=96x30Mm_4032_t000
cp input.txt $name.in			# preserve input file
cat $name.in >>! $name.ins
touch $name.log                         # create log if doesn't exist
date >> $name.log
set d = `date +%D`
echo '96x30Mm' >> $name.log		# experiment
ls -lL $x | cut -d ' ' -f 5-10 >> $name.log       #executable name and date
echo 'NCPUS/node= ' $NCPUS >> $name.log	# number cpus/node
echo 'Ncores= ' $c >> $name.log		# number cores
cat $PBS_NODEFILE >> $name.log
#mpiexec -np $c dplace -s1 -e -c0,1,4,5,2,3,6,7 ./$x $name.in >>& $name.log     # run, with output appended 
#mpiexec -np $c dplace -s1 ./$x $name.in >>& $name.log     # run, with output appended 
mpiexec -np $c ./$x $name.in >>& $name.log     # run, with output appended 
if ($? != 0) then 
   sleep 1
   date >> $name.log
   mpiexec -np $c ./$x $name.in >>& $name.log     # run, with output appended 
endif
date >> $name.log
#setenv LOGSTAT_EXE ./$x
#logstat_runner mpiexec -np $c ./$x $name.in >>& $name.log     # run, with output appended 
#qsub job.csh

