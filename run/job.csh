#! /bin/csh
#PBS -S /bin/csh -j oe -m be -N 96x30-64512
##PBS -l nodes=1008:ppn=32:xe,walltime=24:00:00
##PBS -l nodes=1008:ppn=32:xe,walltime=00:30:00
#PBS -l nodes=2018:ppn=32:xe,walltime=24:00:00
##PBS -l nodes=2016:ppn=32:xe,walltime=00:30:00

#module load comp/intel/11.0.083_64

#~/bin/jobcontrol.csh mpi.csh 32256
~/bin/jobcontrol.csh mpi.csh 64512

